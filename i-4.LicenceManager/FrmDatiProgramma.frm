VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form FrmDatiProgramma 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Licence Maker"
   ClientHeight    =   8070
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   9435
   Icon            =   "FrmDatiProgramma.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8070
   ScaleWidth      =   9435
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog Cmdl 
      Left            =   8400
      Top             =   3780
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame FraDati 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Dati per creazione licenze"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   7875
      Left            =   90
      TabIndex        =   12
      Top             =   135
      Width           =   9330
      Begin VB.Frame FraLicenza 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Licenza :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   2445
         Left            =   90
         TabIndex        =   25
         Top             =   4710
         Width           =   9075
         Begin RichTextLib.RichTextBox RtbCriptato 
            Height          =   1935
            Left            =   135
            TabIndex        =   26
            Top             =   315
            Width           =   8745
            _ExtentX        =   15425
            _ExtentY        =   3413
            _Version        =   393217
            BackColor       =   12648384
            BorderStyle     =   0
            ReadOnly        =   -1  'True
            ScrollBars      =   2
            TextRTF         =   $"FrmDatiProgramma.frx":0442
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FRaDatiSoc 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Dati societ�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   2145
         Left            =   135
         TabIndex        =   20
         Top             =   270
         Width           =   9015
         Begin VB.TextBox TxtLocalita 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   360
            Left            =   1710
            MaxLength       =   54
            TabIndex        =   3
            Top             =   1575
            Width           =   7125
         End
         Begin VB.TextBox txtCap 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   360
            Left            =   1710
            MaxLength       =   6
            TabIndex        =   2
            Top             =   1170
            Width           =   7110
         End
         Begin VB.TextBox TxtIndirizzo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   360
            Left            =   1710
            MaxLength       =   60
            TabIndex        =   1
            Top             =   765
            Width           =   7110
         End
         Begin VB.TextBox TxtNomesoc 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   360
            Left            =   1710
            MaxLength       =   60
            TabIndex        =   0
            Top             =   360
            Width           =   7110
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "Localit� :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   240
            Index           =   9
            Left            =   810
            TabIndex        =   24
            Top             =   1635
            Width           =   825
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "C.A.P."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   240
            Index           =   8
            Left            =   1050
            TabIndex        =   23
            Top             =   1230
            Width           =   555
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "Indirizzo :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   240
            Index           =   7
            Left            =   750
            TabIndex        =   22
            Top             =   795
            Width           =   900
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "Nome societ� :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   240
            Index           =   1
            Left            =   330
            TabIndex        =   21
            Top             =   345
            Width           =   1320
         End
      End
      Begin VB.CommandButton CmdAvviaCotrLic 
         Caption         =   "Avvia controllo licenza"
         Height          =   465
         Left            =   5880
         TabIndex        =   19
         Top             =   7260
         Width           =   3300
      End
      Begin VB.Frame FRaCrypta 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Dati da criptare"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   2265
         Left            =   90
         TabIndex        =   10
         Top             =   2430
         Width           =   9075
         Begin VB.TextBox TxtNumProgramma 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   360
            Left            =   1650
            MaxLength       =   2
            TabIndex        =   4
            Top             =   390
            Width           =   2295
         End
         Begin VB.TextBox TxtNumId 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   360
            Left            =   1650
            MaxLength       =   3
            TabIndex        =   9
            Top             =   1290
            Width           =   2295
         End
         Begin VB.TextBox TxtNumUt 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   360
            Left            =   1650
            MaxLength       =   3
            TabIndex        =   8
            Top             =   840
            Width           =   2295
         End
         Begin VB.TextBox TxtNomeMacchina 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   360
            Left            =   1650
            MaxLength       =   24
            TabIndex        =   5
            Top             =   1725
            Width           =   7245
         End
         Begin MSMask.MaskEdBox MskDataInizio 
            Height          =   375
            Left            =   6600
            TabIndex        =   6
            Top             =   390
            Width           =   2265
            _ExtentX        =   3995
            _ExtentY        =   661
            _Version        =   393216
            Appearance      =   0
            BackColor       =   16777215
            ForeColor       =   0
            AutoTab         =   -1  'True
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "dd/mm/yyyy"
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox MskDataFine 
            Height          =   375
            Left            =   6600
            TabIndex        =   7
            Top             =   840
            Width           =   2265
            _ExtentX        =   3995
            _ExtentY        =   661
            _Version        =   393216
            Appearance      =   0
            BackColor       =   16777215
            ForeColor       =   0
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "dd/mm/yyyy"
            Mask            =   "##/##/####"
            PromptChar      =   "_"
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "Nome prodotto :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   240
            Index           =   0
            Left            =   120
            TabIndex        =   18
            Top             =   420
            Width           =   1440
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "N�ID:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   240
            Index           =   2
            Left            =   990
            TabIndex        =   17
            Top             =   1350
            Width           =   570
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "N�Utenze:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   240
            Index           =   3
            Left            =   600
            TabIndex        =   16
            Top             =   900
            Width           =   945
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "Data fine(dd/mm/yyyy):"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   240
            Index           =   4
            Left            =   4410
            TabIndex        =   15
            Top             =   885
            Width           =   2115
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "Data inizio(dd/mm/yyyy):"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   240
            Index           =   5
            Left            =   4350
            TabIndex        =   14
            Top             =   420
            Width           =   2190
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "KEY :"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   240
            Index           =   6
            Left            =   1065
            TabIndex        =   13
            Top             =   1770
            Width           =   495
         End
      End
      Begin VB.CommandButton CmdCripta 
         Caption         =   "Avvia creazione licenza"
         Height          =   465
         Left            =   90
         TabIndex        =   11
         Top             =   7260
         Width           =   3300
      End
   End
   Begin VB.Menu MnuLicense 
      Caption         =   "&Licence"
      Begin VB.Menu MnuOpen 
         Caption         =   "&Open Licence"
         Shortcut        =   ^O
      End
      Begin VB.Menu MnuSave 
         Caption         =   "&Save Licence"
         Shortcut        =   ^S
      End
   End
End
Attribute VB_Name = "FrmDatiProgramma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CmdAvviaCotrLic_Click()
  Dim Pos1 As Integer
  Dim Pos2 As Integer
  Dim Pos3 As Integer
  Dim NameMachine As String
  
  Dim Bool As Boolean
  
  If RtbCriptato.Text <> "" Then
    Pos2 = InStr(RtbCriptato.Text, "H0002 : ") + Len("H0002 : #")
    Pos1 = InStr(RtbCriptato.Text, "H0001 : ") + Len("H0001 : #")
    Pos3 = InStr(Pos1, RtbCriptato.Text, vbCrLf)
    NameMachine = Mid(RtbCriptato.Text, Pos1, (Pos3 - Pos1) - 1)
    Call CreaTabLunghezza
    Call CreaTabDinamica(NameMachine)
    DecodificaLicenza Mid(RtbCriptato.Text, Pos2, 52), NameMachine
  Else
    MsgBox "Prima di verificare la correttezza della licenza aprirla o crearla!", vbInformation, "Attention Please"
    Exit Sub
  End If
End Sub

Private Sub CmdCripta_Click()
  If IsDate(MskDataInizio.Text) = False Or IsDate(MskDataFine.Text) = False Then
    MsgBox "Controllare attentamente le date immesse!", vbExclamation, "Attention"
    Exit Sub
  Else
    If CDate(MskDataInizio.Text) > CDate(MskDataFine) Then
      MsgBox "Attenzione la data di istallazione deve essere inferiore  quella di disinstallazione!", vbExclamation, "attention"
      Exit Sub
    End If
  End If
  Call CreaTabDinamica(TxtNomeMacchina)
  Call CriptaLicenza(TxtNomeMacchina, TxtNumProgramma, MskDataInizio, MskDataFine, TxtNumUt, TxtNumId)
  TabellaDynamic = ""
End Sub

Private Sub Form_Load()
  Call main
End Sub

Private Sub MnuOpen_Click()
  Cmdl.Filter = "*.txt (file di testo)|*.txt"
  Cmdl.ShowOpen
  If Cmdl.FileName <> "" Then
    txtCap = ""
    TxtIndirizzo = ""
    TxtLocalita = ""
    TxtNomeMacchina = ""
    TxtNomesoc = ""
    TxtNumId = ""
    TxtNumProgramma = ""
    TxtNumUt = ""
    MskDataFine.Text = "__/__/____"
    MskDataInizio.Text = "__/__/____"
    RtbCriptato.LoadFile (Cmdl.FileName)
  End If
End Sub

Private Sub MnuSave_Click()
  Dim NomeFile As String
  Dim MyDirectory As String
  Dim Percorso As String
  If RtbCriptato.Text <> "" Then
    Cmdl.Filter = "*.txt (file di testo)|*.txt"
    Cmdl.ShowSave
    NomeFile = Cmdl.FileTitle
    If Cmdl.FileName <> "" Then
      Percorso = Mid(Cmdl.FileName, 1, InStr(Cmdl.FileName, Cmdl.FileTitle) - 1)
      MyDirectory = Dir(Percorso, vbDirectory)
      Do While MyDirectory <> ""
        If MyDirectory = NomeFile Then
          If MsgBox("Sovrascrivere il file esistente!", vbExclamation Or vbYesNo, "Attention please") = vbNo Then
            Exit Sub
          Else
            Exit Do
          End If
        End If
        MyDirectory = Dir
      Loop
      RtbCriptato.SaveFile Cmdl.FileName, rtfText
    Else
      MsgBox "Inserire il nome del file su cui salvare la licenza!", vbInformation, "Attention please"
    End If
  Else
    MsgBox "Creare o aprire prima una licenza e poi salvarla!", vbInformation, "Attention please"
    Exit Sub
  End If
End Sub

Private Sub txtCap_Change()
  SoloNumeri txtCap
End Sub

Private Sub TxtNumId_Change()
  SoloNumeri TxtNumId
End Sub

Private Sub TxtNumProgramma_Change()
  SoloNumeri TxtNumProgramma
End Sub

Private Sub TxtNumUt_Change()
  SoloNumeri TxtNumUt
End Sub
