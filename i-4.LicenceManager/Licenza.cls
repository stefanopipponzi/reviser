VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Licenza"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Lic_NomePC As String
Public Lic_DataInizio As String
Public Lic_DataFine As String
Public Lic_NumProdotto As String
Public Lic_NumUtenza As String
Public Lic_NumID As String

Public Sub Init_DLL()
  Set VarDLL = Me
  Call CreaTabLunghezza
End Sub
  
Public Sub CriptaLicenza(NomeSoc As String, IndirizzoSoc As String, CapSoc As String, LocSoc As String, NomeMacchina As String, NumProdotto As String, DataInizio As String, DatFine As String, NumUt As String, NumUt2 As String, AppUser() As String)
  Dim SingValCriptato As String
  Dim Contatore As Byte
  Dim Contatore_due As Byte
  Dim ValoreAggEsa As Variant
  Dim ValoreAgg As Variant
  Dim Testodascrivere As String
  Dim PosCarat1 As Byte
  Dim PosCarat2 As Byte
  
  Dim LunghNome As Variant
  Dim LunghNomeProg As String
  Dim LunghNumUt1 As String
  Dim LunghNumUt2 As String
  
  Dim NumProgCriptato As Variant
  Dim DataInizioCriptato As Variant
  Dim DatafineCriptato As Variant
  Dim NumUtenza1 As Variant
  Dim NumUtenza2 As Variant
  
 
  
  Call CreaTabDinamica(NomeMacchina)
  ReDim AppUser(4)
  
  CaratMacCripta = ""
  CaratMacCriptaFinale = ""
  LunghNome = Len(NomeMacchina)
 
  For Contatore = 1 To Len(NomeMacchina)
    SingValCriptato = Mid(NomeMacchina, Contatore, 1)
    PosCarat1 = InStr(TabellaOne, SingValCriptato)
    If Contatore <> 1 Then
      PosCarat1 = PosCarat1 + Contatore
    End If
    
    If PosCarat1 > 62 Then
      PosCarat1 = PosCarat1 - 62
    End If
    '*************************************************************
    SingValCriptato = Mid(TabellaDynamic, PosCarat1, 1)
    CaratMacCripta = CaratMacCripta & SingValCriptato
  Next Contatore
  '**************************************************************************
  
  For Contatore = 1 To Len(CaratMacCripta)
    If LunghNome - Contatore <> 0 Then
      CaratMacCriptaFinale = CaratMacCriptaFinale & Mid(CaratMacCripta, LunghNome - Contatore, 1)
    Else
      CaratMacCriptaFinale = CaratMacCriptaFinale & Mid(CaratMacCripta, LunghNome, 1)
    End If
  Next Contatore
  
  DataInizioCriptato = DataInizio
  DataInizioCriptato = Mid(DataInizioCriptato, 1, 1) & Mid(DataInizioCriptato, 2, 1) & Mid(DataInizioCriptato, 4, 1) & Mid(DataInizioCriptato, 5, 1) & Mid(DataInizioCriptato, 7, 1) & Mid(DataInizioCriptato, 8, 1) & Mid(DataInizioCriptato, 9, 1) & Mid(DataInizioCriptato, 10, 1)
  DataInizioCriptato = CriptoAll(DataInizioCriptato)
  DatafineCriptato = DatFine
  DatafineCriptato = Mid(DatafineCriptato, 1, 1) & Mid(DatafineCriptato, 2, 1) & Mid(DatafineCriptato, 4, 1) & Mid(DatafineCriptato, 5, 1) & Mid(DatafineCriptato, 7, 1) & Mid(DatafineCriptato, 8, 1) & Mid(DatafineCriptato, 9, 1) & Mid(DatafineCriptato, 10, 1)
  DatafineCriptato = CriptoAll(DatafineCriptato)
  NumProgCriptato = CriptoAll(NumProdotto)
  NumUt = CriptoAll(NumUt)
  NumUt2 = CriptoAll(NumUt2)
  
  LunghNome = LunghCriptata(CaratMacCriptaFinale)
  LunghNomeProg = LunghCriptata(NumProgCriptato)
  LunghNumUt1 = LunghCriptata(NumUt)
  LunghNumUt2 = LunghCriptata(NumUt2)
  '***********************************************************************
    
  CaratMacCriptaFinale = AggValoriMancanti(CaratMacCriptaFinale, LunghNome, 24)
  NumProgCriptato = AggValoriMancanti(NumProgCriptato, LunghNomeProg, 2)
  NumUt = AggValoriMancanti(NumUt, LunghNumUt1, 3)
  NumUt2 = AggValoriMancanti(NumUt2, LunghNumUt2, 3)
  '***********************************************************************
  
  Call CreaTestoFinale(CaratMacCriptaFinale, CStr(NumProgCriptato), CStr(DataInizioCriptato), CStr(DatafineCriptato), NumUt, NumUt2, CStr(LunghNome), LunghNomeProg, LunghNumUt1, LunghNumUt2)
  
  CaratMacCriptaFinale = ""
  For Contatore = 1 To UBound(TestoCriptatoFinale)
    CaratMacCriptaFinale = CaratMacCriptaFinale & TestoCriptatoFinale(Contatore)
  Next Contatore
  
  
  AppUser(0) = "I0001 : #" & NomeSoc & "#" & vbCrLf
  AppUser(1) = Testodascrivere & "I0002 : #" & IndirizzoSoc & "#" & vbCrLf
  AppUser(2) = Testodascrivere & "I0003 : #" & LocSoc & "-" & CapSoc & "#" & vbCrLf
  AppUser(3) = Testodascrivere & "H0001 : #" & NomeMacchina & "#" & vbCrLf
  AppUser(4) = Testodascrivere & "H0002 : #" & CaratMacCriptaFinale & "#"

End Sub

Public Function DecodificaLicenza(TestoCriptato As String, NomeMacchinaVero As String) As Boolean
  Dim TuttoOk As Boolean
  
  Dim LunghCriptaMachine As Integer
  Dim LunghNameMachine As String
  Dim LunghNameProgr As String
  Dim LunghNumUt1 As String
  Dim LunghNumUt2 As String
  
  Dim NomeMachineCrip As Variant
  Dim NomeProgrammaCrip As Variant
  Dim DataInizioCrip As Variant
  Dim DataFineCrip As Variant
  Dim NumUt1Crip As Variant
  Dim NumUt2Crip As Variant
  
  Dim NomeMachineCod As Variant
  Dim NomeMachineCodFin As Variant
  Dim NomeProgrammaCod As Variant
  Dim DataInizioCod As Variant
  Dim DataFineCod As Variant
  Dim NumUt1Cod As Variant
  Dim NumUt2Cod As Variant
  
  Dim Contatore As Integer
  Dim ContatoreDue As Integer
  Dim SingValCriptato As String
  
  Dim DataInizioAF As Date
  Dim DataFineAF As Date
  
  If Trim(TabellaDynamic) = "" Then
    Call CreaTabDinamica(NomeMacchinaVero)
  End If
  
  ReDim TestoCriptatoFinale(52)
  For Contatore = 1 To Len(TestoCriptato)
    TestoCriptatoFinale(Contatore) = Mid(TestoCriptato, Contatore, 1)
  Next Contatore

  LunghNameMachine = TestoCriptatoFinale(10)
  LunghNameProgr = TestoCriptatoFinale(13)
  LunghNumUt1 = TestoCriptatoFinale(23)
  LunghNumUt2 = TestoCriptatoFinale(33)
  '*******************************************************************
  
  NomeMachineCrip = TestoCriptatoFinale(36) & TestoCriptatoFinale(27) & TestoCriptatoFinale(1) & _
                    TestoCriptatoFinale(9) & TestoCriptatoFinale(32) & TestoCriptatoFinale(21) & _
                    TestoCriptatoFinale(14) & TestoCriptatoFinale(2) & TestoCriptatoFinale(24) & _
                    TestoCriptatoFinale(43) & TestoCriptatoFinale(40) & TestoCriptatoFinale(6) & _
                    TestoCriptatoFinale(17) & TestoCriptatoFinale(15) & TestoCriptatoFinale(29) & _
                    TestoCriptatoFinale(19) & TestoCriptatoFinale(45) & TestoCriptatoFinale(48) & _
                    TestoCriptatoFinale(46) & TestoCriptatoFinale(49) & TestoCriptatoFinale(52) & _
                    TestoCriptatoFinale(51) & TestoCriptatoFinale(47) & TestoCriptatoFinale(50)
                    
  NomeProgrammaCrip = TestoCriptatoFinale(44) & TestoCriptatoFinale(20)
  
  NumUt1Crip = TestoCriptatoFinale(25) & TestoCriptatoFinale(35) & TestoCriptatoFinale(41)
  NumUt2Crip = TestoCriptatoFinale(28) & TestoCriptatoFinale(30) & TestoCriptatoFinale(7)
  DataInizioCrip = TestoCriptatoFinale(12) & TestoCriptatoFinale(5) & TestoCriptatoFinale(3) & TestoCriptatoFinale(31) & TestoCriptatoFinale(38) & TestoCriptatoFinale(34) & TestoCriptatoFinale(22) & TestoCriptatoFinale(39)
  DataFineCrip = TestoCriptatoFinale(18) & TestoCriptatoFinale(11) & TestoCriptatoFinale(42) & TestoCriptatoFinale(37) & TestoCriptatoFinale(26) & TestoCriptatoFinale(8) & TestoCriptatoFinale(4) & TestoCriptatoFinale(16)
  '**************************************************************************
  
  NomeMachineCrip = DalPrimo_ultimo_NameMachine(CStr(NomeMachineCrip), LunghNameMachine)
  '********************************************************************
  
  NomeMachineCod = DecriptaAll(NomeMachineCrip, LunghNameMachine)
  NomeProgrammaCod = DecriptaAll(NomeProgrammaCrip, LunghNameProgr)
  DataInizioCod = DecriptaAll(DataInizioCrip, "H")
  DataInizioCod = Mid(DataInizioCod, 1, 2) & "/" & Mid(DataInizioCod, 3, 2) & "/" & Mid(DataInizioCod, 5)
  DataFineCod = DecriptaAll(DataFineCrip, "H")
  DataFineCod = Mid(DataFineCod, 1, 2) & "/" & Mid(DataFineCod, 3, 2) & "/" & Mid(DataFineCod, 5)
  NumUt1Cod = DecriptaAll(NumUt1Crip, LunghNumUt1)
  NumUt2Cod = DecriptaAll(NumUt2Crip, LunghNumUt2)
  
  
  TuttoOk = False
  For Contatore = 1 To 1
    If NomeMachineCod = NomeMacchinaVero Then
      TuttoOk = True
      If IsNumeric(NomeProgrammaCod) = True Then
        TuttoOk = True
        If IsNumeric(NumUt1Cod) = True Then
          TuttoOk = True
'          If IsNumeric(NumUt2Cod) = True Then
'            TuttoOk = True
            If IsDate(DataInizioCod) = True Then
              TuttoOk = True
              If IsDate(DataFineCod) = True Then
                TuttoOk = True
              Else
                TuttoOk = False
                Exit For
              End If
            Else
              TuttoOk = False
              Exit For
            End If
'          Else
'            TuttoOk = False
'            Exit For
'          End If
        Else
          TuttoOk = False
          Exit For
        End If
      Else
        TuttoOk = False
        Exit For
      End If
    Else
      TuttoOk = False
      Exit For
    End If
  Next Contatore
  '******************************************************
  If TuttoOk = True Then
    DecodificaLicenza = TuttoOk
  Else
    DecodificaLicenza = TuttoOk
  End If
  
  'AC
  
  DataInizioAF = DateSerial(Int(Mid(DataInizioCod, 7, 4)), Int(Mid(DataInizioCod, 4, 2)), Int(Mid(DataInizioCod, 1, 2)))
  DataFineAF = DateSerial(Int(Mid(DataFineCod, 7, 4)), Int(Mid(DataFineCod, 4, 2)), Int(Mid(DataFineCod, 1, 2)))
    
  'VarDLL.Lic_DataFine = DataFineCod
  'VarDLL.Lic_DataInizio = DataInizioCod
  VarDLL.Lic_DataFine = DataFineAF
  VarDLL.Lic_DataInizio = DataInizioAF
  
  VarDLL.Lic_NomePC = NomeMachineCod
  VarDLL.Lic_NumID = NumUt2Cod
  VarDLL.Lic_NumProdotto = NomeProgrammaCod
  VarDLL.Lic_NumUtenza = NumUt1Cod
  
End Function
