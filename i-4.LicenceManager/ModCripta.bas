Attribute VB_Name = "ModCripta"
Option Explicit
Global CaratMacCripta As String
Global CaratMacDecod As String
Global CaratMacCriptaFinale As String
Global TestoCriptatoFinale() As String
Global TabellaOne As String
Global TabellaTwo As String
Global TabellaDynamic As String


Public VarDLL As New Licenza

Global MyCodAscii() As CodiceAscii
Public Type CodiceAscii
  IdAscii As Integer
  ValAscii As String
End Type

Public MyCostLettere() As CostLettere
Public Type CostLettere
  IdLettera As Integer
  ValLettera As String
End Type

Public Sub CreaTabLunghezza()
  Dim AppCodAscii As Integer
  Dim AppCodLettere As Integer
  
  'CREAZIONE COSTANTI LETTERE
  AppCodLettere = 1
  ReDim MyCostLettere(24)
  For AppCodAscii = 65 To 90
    If AppCodLettere > 24 Then Exit For
    MyCostLettere(AppCodLettere).IdLettera = AppCodLettere
    MyCostLettere(AppCodLettere).ValLettera = Chr(AppCodAscii)
    AppCodLettere = AppCodLettere + 1
  Next AppCodAscii
  '************************************************
End Sub
Public Sub Main()
  Call CreaTabLunghezza
End Sub

Public Function CriptoAll(TestoOrig As Variant)
  Dim Contatore As Integer
  Dim PosOne As Byte
  Dim SingleValore As String
  CriptoAll = ""
  For Contatore = 1 To Len(TestoOrig)
      SingleValore = Mid(TestoOrig, Contatore, 1)
      PosOne = InStr(TabellaOne, SingleValore)
      If Contatore <> 1 Then
        PosOne = PosOne + Contatore
      End If
      If PosOne > 62 Then
        PosOne = PosOne - 62
      End If
      SingleValore = Mid(TabellaDynamic, PosOne, 1)
      CriptoAll = CriptoAll & SingleValore
  Next Contatore
End Function

Public Function LunghCriptata(TestoCriptato As Variant) As String
  Dim Contatore As Integer
  Dim LunghNome As Byte
  
  LunghNome = Len(TestoCriptato)
  For Contatore = 1 To UBound(MyCostLettere)
    If LunghNome = MyCostLettere(Contatore).IdLettera Then
      LunghCriptata = MyCostLettere(Contatore).ValLettera
      Exit For
    End If
  Next Contatore
End Function

Public Function AggValoriMancanti(TestoCriptato As Variant, LunghVal As Variant, NumLunghMax As Byte) As String
  Dim Contatore As Integer
  Dim Contatore2 As Integer
  Dim LunghNome As Byte
  Dim TestoRandom As String * 1
  Dim NumRandom As Integer
  Select Case LunghVal
    Case "A"
      LunghNome = 1
    Case "B"
      LunghNome = 2
    Case "C"
      LunghNome = 3
    Case "D"
      LunghNome = 4
    Case "E"
      LunghNome = 5
    Case "F"
      LunghNome = 6
    Case "G"
      LunghNome = 7
    Case "H"
      LunghNome = 8
    Case "I"
      LunghNome = 9
    Case "J"
      LunghNome = 10
    Case "K"
      LunghNome = 11
    Case "L"
      LunghNome = 12
    Case "M"
      LunghNome = 13
    Case "N"
      LunghNome = 14
    Case "O"
      LunghNome = 15
    Case "P"
      LunghNome = 16
    Case "Q"
     LunghNome = 17
    Case "R"
      LunghNome = 18
    Case "S"
      LunghNome = 19
    Case "T"
      LunghNome = 20
    Case "U"
      LunghNome = 21
    Case "V"
      LunghNome = 22
    Case "Z"
      LunghNome = 23
    Case "X"
      LunghNome = 24
   Case Else
       '  Stop
         
  End Select
  For Contatore = LunghNome + 1 To NumLunghMax
    Randomize
    NumRandom = Int((62 * Rnd) + 1)
    If NumRandom > 62 Then
      NumRandom = NumRandom - 62
    End If
    TestoRandom = Mid(TabellaDynamic, NumRandom, 1)
    TestoCriptato = TestoCriptato & TestoRandom
  Next Contatore
  AggValoriMancanti = TestoCriptato

End Function

Public Sub CreaTestoFinale(NameMachine As String, NameProgramma As String, DataIni As String, DataFin As String, NumUtenza1 As String, NumUtenza2 As String, LunghMachine As String, LunghNameProg As String, LunghNumUt1 As String, LunghNumUt2 As String)
  Dim Contatore As Integer
  
  ReDim TestoCriptatoFinale(52)
  For Contatore = 1 To UBound(TestoCriptatoFinale)
    Select Case Contatore
      Case 1
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 3, 1)
      Case 2
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 8, 1)
      Case 3
        TestoCriptatoFinale(Contatore) = Mid(DataIni, 3, 1)
      Case 4
        TestoCriptatoFinale(Contatore) = Mid(DataFin, 7, 1)
      Case 5
        TestoCriptatoFinale(Contatore) = Mid(DataIni, 2, 1)
      Case 6
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 12, 1)
      Case 7
        TestoCriptatoFinale(Contatore) = Mid(NumUtenza2, 3, 1)
      Case 8
        TestoCriptatoFinale(Contatore) = Mid(DataFin, 6, 1)
      Case 9
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 4, 1)
      Case 10
        TestoCriptatoFinale(Contatore) = LunghMachine
      Case 11
        TestoCriptatoFinale(Contatore) = Mid(DataFin, 2, 1)
      Case 12
        TestoCriptatoFinale(Contatore) = Mid(DataIni, 1, 1)
      Case 13
        TestoCriptatoFinale(Contatore) = LunghNameProg
      Case 14
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 7, 1)
      Case 15
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 14, 1)
      Case 16
        TestoCriptatoFinale(Contatore) = Mid(DataFin, 8, 1)
      Case 17
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 13, 1)
      Case 18
        TestoCriptatoFinale(Contatore) = Mid(DataFin, 1, 1)
      Case 19
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 16, 1)
      Case 20
        TestoCriptatoFinale(Contatore) = Mid(NameProgramma, 2, 1)
      Case 21
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 6, 1)
      Case 22
        TestoCriptatoFinale(Contatore) = Mid(DataIni, 7, 1)
      Case 23
        TestoCriptatoFinale(Contatore) = LunghNumUt1
      Case 24
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 9, 1)
      Case 25
        TestoCriptatoFinale(Contatore) = Mid(NumUtenza1, 1, 1)
      Case 26
        TestoCriptatoFinale(Contatore) = Mid(DataFin, 5, 1)
      Case 27
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 2, 1)
      Case 28
        TestoCriptatoFinale(Contatore) = Mid(NumUtenza2, 1, 1)
      Case 29
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 15, 1)
      Case 30
        TestoCriptatoFinale(Contatore) = Mid(NumUtenza2, 2, 1)
      Case 31
        TestoCriptatoFinale(Contatore) = Mid(DataIni, 4, 1)
      Case 32
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 5, 1)
      Case 33
        TestoCriptatoFinale(Contatore) = LunghNumUt2
      Case 34
        TestoCriptatoFinale(Contatore) = Mid(DataIni, 6, 1)
      Case 35
        TestoCriptatoFinale(Contatore) = Mid(NumUtenza1, 2, 1)
      Case 36
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 1, 1)
      Case 37
        TestoCriptatoFinale(Contatore) = Mid(DataFin, 4, 1)
      Case 38
        TestoCriptatoFinale(Contatore) = Mid(DataIni, 5, 1)
      Case 39
        TestoCriptatoFinale(Contatore) = Mid(DataIni, 8, 1)
      Case 40
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 11, 1)
      Case 41
        TestoCriptatoFinale(Contatore) = Mid(NumUtenza1, 3, 1)
      Case 42
        TestoCriptatoFinale(Contatore) = Mid(DataFin, 3, 1)
      Case 43
        TestoCriptatoFinale(Contatore) = Mid(NameMachine, 10, 1)
      Case 44
        TestoCriptatoFinale(Contatore) = Mid(NameProgramma, 1, 1)
      Case 45
         TestoCriptatoFinale(Contatore) = Mid(NameMachine, 17, 1)
      Case 46
         TestoCriptatoFinale(Contatore) = Mid(NameMachine, 19, 1)
      Case 47
         TestoCriptatoFinale(Contatore) = Mid(NameMachine, 23, 1)
      Case 48
         TestoCriptatoFinale(Contatore) = Mid(NameMachine, 18, 1)
      Case 49
         TestoCriptatoFinale(Contatore) = Mid(NameMachine, 20, 1)
      Case 50
         TestoCriptatoFinale(Contatore) = Mid(NameMachine, 24, 1)
      Case 51
         TestoCriptatoFinale(Contatore) = Mid(NameMachine, 22, 1)
      Case 52
         TestoCriptatoFinale(Contatore) = Mid(NameMachine, 21, 1)
      
    End Select
  Next Contatore

End Sub

Public Function DecriptaAll(Testo_da_cod As Variant, LunghUtile As String)
  Dim Contatore As Integer
  Dim Contatore2 As Integer
  Dim ContLungh As Byte
  Dim SingleCarat As String
  Dim PosCarat As Variant
  
  Select Case LunghUtile
    Case "A"
      ContLungh = 1
    Case "B"
      ContLungh = 2
    Case "C"
      ContLungh = 3
    Case "D"
      ContLungh = 4
    Case "E"
      ContLungh = 5
    Case "F"
      ContLungh = 6
    Case "G"
      ContLungh = 7
    Case "H"
      ContLungh = 8
    Case "I"
      ContLungh = 9
    Case "J"
      ContLungh = 10
    Case "K"
      ContLungh = 11
    Case "L"
      ContLungh = 12
    Case "M"
      ContLungh = 13
    Case "N"
      ContLungh = 14
    Case "O"
      ContLungh = 15
    Case "P"
      ContLungh = 16
    Case "Q"
      ContLungh = 17
    Case "R"
      ContLungh = 18
    Case "S"
      ContLungh = 19
    Case "T"
      ContLungh = 20
    Case "U"
      ContLungh = 21
    Case "V"
      ContLungh = 22
    Case "Z"
      ContLungh = 23
    Case "X"
      ContLungh = 24
   Case Else
        ' Stop
  End Select
  
  DecriptaAll = ""
  For Contatore = 1 To ContLungh
    SingleCarat = Mid(Testo_da_cod, Contatore, 1)
    PosCarat = InStr(TabellaDynamic, SingleCarat)
    If Contatore <> 1 Then
      PosCarat = PosCarat - Contatore
      If PosCarat = 0 Then
        PosCarat = 62 - PosCarat
      End If
      If PosCarat < 0 Then
        PosCarat = 62 + PosCarat
      End If
    End If
    SingleCarat = Mid(TabellaOne, PosCarat, 1)
    DecriptaAll = DecriptaAll & SingleCarat
  Next Contatore
End Function
''''''Public Sub GetDatiSocieta(RtbApp As RichTextBox)
''''''  Dim PosVal As Integer
''''''  Dim PosVal2 As Integer
''''''  Dim PosVal3 As Integer
''''''
''''''  'PRENDO NOME SOCIETA'
''''''  PosVal = InStr(RtbApp.Text, "I0001 : #") + Len("I0001 : #")
''''''  PosVal2 = InStr(PosVal, RtbApp.Text, vbCrLf) - 1
''''''  FrmDatiProgramma.TxtNomesoc = Mid(RtbApp.Text, PosVal, PosVal2 - PosVal)
''''''
''''''  'PRENDO INDIRIZZO SOCIETA'
''''''  PosVal = InStr(RtbApp.Text, "I0002 : #") + Len("I0002 : #")
''''''  PosVal2 = InStr(PosVal, RtbApp.Text, vbCrLf) - 1
''''''  FrmDatiProgramma.TxtIndirizzo = Mid(RtbApp.Text, PosVal, PosVal2 - PosVal)
''''''
''''''  'PRENDO CAP SOCIETA'
''''''  PosVal = InStr(RtbApp.Text, "I0003 : #") + Len("I0003 : #")
''''''  PosVal2 = InStr(PosVal, RtbApp.Text, "-") + 1
''''''  PosVal = InStr(PosVal2, RtbApp, "#")
''''''  FrmDatiProgramma.txtCap = Mid(RtbApp.Text, PosVal2, (PosVal - PosVal2) - 1)
''''''
''''''  'PRENDO IL NOME DELLA LOCALITA' DELLA CITTA'
''''''  PosVal = InStr(RtbApp.Text, "I0003 : #") + Len("I0003 : #")
''''''  PosVal2 = InStr(PosVal, RtbApp.Text, "-")
''''''  FrmDatiProgramma.TxtLocalita = Mid(RtbApp.Text, PosVal, PosVal2 - PosVal)
''''''
''''''End Sub
Public Sub SoloNumeri(Testo As TextBox)
    If Len(Testo) <> 0 Then
        If Len(Testo) <> 1 Then
            If IsNumeric(Testo) = False Then
                Testo = Mid(Testo, 1, Len(Testo) - 1)
            Else
                Testo = Testo
            End If
        Else
            If IsNumeric(Testo) = False Then
                Testo = ""
            Else
                Testo = Testo
            End If
        End If
    End If
    If Len(Testo) <> 0 Then
      Testo.SelStart = Len(Testo)
    End If
End Sub

Public Sub CreaTabDinamica(Chiave As String)
  Dim K0 As Integer
  Dim K1 As Integer
  Dim K2 As Integer
  Dim ValAscii  As Integer
  Dim CharSingle As String
  
  TabellaOne = "5ApBhCDEyF6GoHIx4JdKz0cLweMf7vnr1Ngb8OskumP9q3QRlSTiUVWajXYZt2"
  TabellaTwo = TabellaOne
  TabellaDynamic = ""
  K1 = 0
  For K0 = 0 To 62
    K1 = K1 + 1
    'Quando supero la lunghezza della chiave il valore di K1 � 1
    If K1 > Len(Chiave) Then K1 = 1
    '***********************************************************
    CharSingle = Mid(Chiave, K1, 1)
    ValAscii = Asc(CharSingle)
    Do While ValAscii > 62
      ValAscii = ValAscii - 62
    Loop
    For ValAscii = ValAscii To 62
      CharSingle = Mid(TabellaTwo, ValAscii, 1)
      If CharSingle <> " " Then
        TabellaDynamic = TabellaDynamic & CharSingle
        Mid(TabellaTwo, ValAscii, 1) = " "
        Exit For
      End If
    Next ValAscii
    If CharSingle = " " Then
      For ValAscii = 1 To 62
        CharSingle = Mid(TabellaTwo, ValAscii, 1)
        If CharSingle <> " " Then
          TabellaDynamic = TabellaDynamic & CharSingle
          Mid(TabellaTwo, ValAscii, 1) = " "
          Exit For
        End If
      Next ValAscii
    End If
  Next K0
End Sub
Public Function DalPrimo_ultimo_NameMachine(NomeMacCriptato As String, LunghVal As String)
  Dim Contatore As Integer
  Dim LunghNome As Byte
  
  Select Case LunghVal
    Case "A"
      LunghNome = 1
    Case "B"
      LunghNome = 2
    Case "C"
      LunghNome = 3
    Case "D"
      LunghNome = 4
    Case "E"
      LunghNome = 5
    Case "F"
      LunghNome = 6
    Case "G"
      LunghNome = 7
    Case "H"
      LunghNome = 8
    Case "I"
      LunghNome = 9
    Case "J"
      LunghNome = 10
    Case "K"
      LunghNome = 11
    Case "L"
      LunghNome = 12
    Case "M"
      LunghNome = 13
    Case "N"
      LunghNome = 14
    Case "O"
      LunghNome = 15
    Case "P"
      LunghNome = 16
    Case "Q"
      LunghNome = 17
    Case "R"
      LunghNome = 18
    Case "S"
      LunghNome = 19
    Case "T"
      LunghNome = 20
    Case "U"
      LunghNome = 21
    Case "V"
      LunghNome = 22
    Case "Z"
      LunghNome = 23
    Case "X"
      LunghNome = 24
     Case Else
         'Stop
  End Select
  
  'Per mettere il nome della macchina dal primo all'ultimo
  DalPrimo_ultimo_NameMachine = ""
  For Contatore = LunghNome To 1 Step -1
    If Contatore <> 1 Then
      DalPrimo_ultimo_NameMachine = DalPrimo_ultimo_NameMachine & Mid(NomeMacCriptato, Contatore - 1, 1)
    Else
      DalPrimo_ultimo_NameMachine = DalPrimo_ultimo_NameMachine & Mid(NomeMacCriptato, LunghNome, 1)
    End If
  Next Contatore
  '***********************************************
End Function

