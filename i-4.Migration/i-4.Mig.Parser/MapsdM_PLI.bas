Attribute VB_Name = "MapsdM_PLI"
Option Explicit
Option Compare Text
'''''''''''''''''''''''''''''''''''''''''''''
' - SQ [01] (15-12-08)
'   TMP: Gestione variabili "locali"...
'''''''''''''''''''''''''''''''''''''''''''''
Public procName As String
'Ac
Public ProcIndex As Integer 'aggiorniamo il prefisso per le variabili
'T ON condition
Dim Oncondition As String
Dim OnErrore As String

Public Type t_Entry
  Name As String
  external As Boolean
  parameters As String
  returns As String
  variable As Boolean
  options As String
End Type
Dim entries() As t_Entry

Private Type t_Files
  Name As String
  io As String
  'T 25-05-2009
  endcondition As String
  stkey As String
  recsize As Long
  '
End Type
Dim arrFiles() As t_Files

'gloria: collection proc, mi serve per conversione
Private Type t_Proc
  Name As String
  parameters() As String
  Return As String
End Type



Dim arrProc() As t_Proc
Dim countProc As Integer
'************************
Dim i As Integer
Dim y As Long
Dim varying As Boolean, IsStatic As Boolean, IsExternal As Boolean, booEliminaRiga As Boolean
Dim Occurs As Double, Tipo_tipo As String, Valore As String, Picture As String
Dim fieldName As String, Tipo As String, Segno As String, Usage As String, wIndexed As String
Dim builtins() As String
Dim Builtins_F As Collection
'silvia 09-10-2008
Dim ColRedefines As Collection
Dim entryIndex As Integer, builtinIndex As Integer
Dim oldLevel As Integer
Dim CurrentTag As String
'silvia 26-05-2008
Global GblCodiceDCL As String

'silvia 01-10-2008
Const migrationPath = "Output-prj\Languages\Pli2Cobol"
Const templatePath = "Input-prj\Languages\PLi2Cobol"

Public tagValues As Collection
'silvia 6/10/2008
Public DCLFile As Collection
Public operator As String
Dim indent  As String
Dim RTB As RichTextBox
'silvia: gestione  chiusura PERFORM, EVALUATE, IF...
'Dim ContPerform As Integer, ContEvaluate As Integer, ContIf As Integer, ContEndIF As Integer,
Dim ContWhen As Integer
Dim ArrEnd() As String, ContEnd As Integer
Dim ParamLinkage As String
Dim ProcMain As Boolean
Dim ArrFD() As String
'T collection per aggiungere copy di procedure alla fine
Dim stCopy_P As Collection
Private StRedefines


'silvia 01-10-2008
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub convertPGM(language As String)
  On Error GoTo fileErr
  Set tagValues = New Collection
  Select Case language
    Case "COBOL"
      Pli2cobol = True
      'INIT tags
      tagValues.Add "", "PROCEDURE-DIVISION"
      tagValues.Add "", "WORKING-STORAGE"
      tagValues.Add "", "DATA-REDEFINES"
      'silvia 5/9/2008: Tag per File Definition
      tagValues.Add "", "ASSIGN"
      tagValues.Add "", "FD"
      tagValues.Add "", "FILE-STATUS"
      tagValues.Add "", "LABEL-DEFINITION"
      
      'silvia 10/9/2008
      tagValues.Add "", "LABELS-TEMPLATE"
      'silvia 06/10/2008
      tagValues.Add "", "COMMENT"
      tagValues.Add "", "POPEN"
      tagValues.Add "", "PCLOSE"
      tagValues.Add "", "ON-ERROR"
      tagValues.Add "", "ON-ENDFILE"
      
      tagValues.Add "", "LINKAGE"
    ''  tagValues.Add "", "ON-CONDITION"
      tagValues.Add "", "COPY-P"
      '...
'      DataLevel = 0
'      listIndex = 0
      
      'INIT template
      
      Set RTB = MapsdF_Parser.rText
      On Error GoTo fileErr
      ''RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\COPY-WK"
      On Error GoTo catch
      CurrentTag = "WORKING-STORAGE"
      
      deleteSegnalazioni GbIdOggetto

      parsePLI
      
      'silvia 08-10-2008 dopo il parsing scelgo il template giusto
      If tagValues.item("PROCEDURE-DIVISION") <> "" And tagValues.item("WORKING-STORAGE") <> "" Then
        RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\MAIN"
        changeTag RTB, "<PGM-NAME>", GbNomePgm
      ElseIf tagValues.item("PROCEDURE-DIVISION") = "" And tagValues.item("WORKING-STORAGE") <> "" Then
        RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\COPY-WK"
        'GbNomePgm = GbNomePgm & "W"
        changeTag RTB, "<COPY-NAME>", GbNomePgm
      ElseIf tagValues.item("PROCEDURE-DIVISION") <> "" And tagValues.item("WORKING-STORAGE") = "" Then
        RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\COPY-PR"
        changeTag RTB, "<COPY-NAME>", GbNomePgm
        'GbNomePgm = GbNomePgm & "P"
      End If
      CurrentTag = "POPEN"
      For i = 1 To UBound(arrFiles)
        If arrFiles(i).io <> "XXX" Then
          tagWrite indent & "OPEN " & arrFiles(i).io & " " & arrFiles(i).Name
        End If
      Next
            
      writeTag "WORKING-STORAGE"
      writeTag "DATA-REDEFINES"
      writeTag "PROCEDURE-DIVISION"
      writeTag "ASSIGN"
      writeTag "FD"
      writeTag "FILE-STATUS"
      writeTag "DATA-REDEFINES"
      writeTag "LABEL-DEFINITION"
      writeTag "LABELS-TEMPLATE"
      writeTag "COMMENT"
      writeTag "POPEN"
      writeTag "PCLOSE"
      writeTag "ON-ERROR"
      writeTag "ON-ENDFILE"
      writeTag "LINKAGE"
  ''    writeTag "ON-CONDITION"
      writeTag "COPY-P"
      cleanTags RTB
      'silvia gestione procedure division using...
      If ParamLinkage <> "" Then
        If Len(ParamLinkage) > 40 Then
           ParamLinkage = Replace(ParamLinkage, ",", "," & vbCrLf & Space(31))
        End If
        RTB.Text = Replace(RTB.Text, "PROCEDURE DIVISION", "PROCEDURE DIVISION USING " & ParamLinkage)
      End If
      Pli2cobol = False
      On Error GoTo fileErr
      
      If InStr(RTB.fileName, "MAIN") Then
        RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm & ".CBL", 1
      Else
        RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm, 1
      End If
    Case Else
      MsgBox "TMP: not supported conversion", vbInformation, "i-4.Migration"
  End Select
  
  Exit Sub
fileErr:
  If err.Number = 75 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath
    Resume
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
  End If
  Exit Sub
catch:
  m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
End Sub

'silvia 01-10-2008
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''Public Sub convertINC(language As String)
'''  On Error GoTo fileErr
'''  Set tagValues = New Collection
'''  Select Case language
'''    Case "COBOL"
'''      Pli2cobol = True
'''      'INIT tags
'''      tagValues.Add "", "COPY-NAME"
'''      tagValues.Add "", "WORKING-STORAGE"
'''
'''      Set RTB = MapsdF_Parser.rText
'''      On Error GoTo fileErr
'''      RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\COPY-WK"
'''      On Error GoTo catch
'''      CurrentTag = "WORKING-STORAGE"
'''
'''      changeTag RTB, "<COPY-NAME>", GbNomePgm
'''
'''      parsePgm_I
'''
'''      writeTag "COPY-NAME"
'''      writeTag "WORKING-STORAGE"
'''      cleanTags RTB
'''
'''      On Error GoTo fileErr
'''
'''      RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm, 1
'''    Case Else
'''      MsgBox "TMP: not supported conversion", vbInformation, "i-4.Migration"
'''  End Select
'''
'''  Exit Sub
'''fileErr:
'''  If err.Number = 52 Then
'''    'Directory non esistente!?
'''    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
'''    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\" & migrationPath
'''    Resume
'''  ElseIf err.Number = 75 Then
'''    MsgBox "Template file '" & m_Fun.FnPathPrj & "\" & templatePath & "\COPY-WK' not found.", vbExclamation, Parser.PsNomeProdotto
'''  Else
'''    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.Description, False
'''  End If
'''  Exit Sub
'''catch:
'''  m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.Description, False
'''End Sub

Private Sub tagWrite(line As String)
  Dim tagValue As String
  'SILVIA 17/09/2008
  Dim stSpace As String
  Dim newline As String
  Dim previousTag As String
  Dim lines() As String
  
  On Error GoTo catch
  'silvia
  
  ReDim lines(0)
  lines = Split(line, vbCrLf)
  If UBound(lines) >= 0 Then
    If Len(lines(0)) < 127 Then
      stSpace = Space(127 - Len(lines(0)))
    Else
      stSpace = ""
    End If
  
    If UBound(lines) > 0 Then
      newline = lines(0) & stSpace & Mid(line, Len(lines(0)) + 1) & vbCrLf
    Else
      newline = line & stSpace & vbCrLf
    End If
    tagValue = tagValues.item(CurrentTag)
    
    tagValues.Remove CurrentTag
    'SILVIA 12/9/2008
    tagValues.Add tagValue & newline, CurrentTag
    
    'tagValues.Add tagValue & line & vbCrLf, CurrentTag
  End If
  Exit Sub
catch:
  If err.Number = 5 Then '424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    tagValues.Add "", CurrentTag
    Resume
  End If
End Sub

Private Sub writeTag(Tag As String)
  Dim tagValue As String
  
  On Error GoTo catch
  
  tagValue = tagValues.item(Tag)
  If InStr(tagValue, "VSAM-KEY") Then
    For i = 1 To UBound(arrFiles)
      If InStr(tagValue, "<" & arrFiles(i).Name & ">") Then
        tagValue = Replace(tagValue, "<" & arrFiles(i).Name & ">", arrFiles(i).stkey)
      End If
    Next i
    tagValue = Replace(tagValue, "VSAM-KEY-", "")
  End If
  changeTag RTB, IIf(tagValue = "", vbCrLf & "<" & Tag & ">", "<" & Tag & ">"), tagValue
  'attenzione: era "CurrentTag"!
  tagValues.Remove Tag
  tagValues.Add "", Tag
  
  Exit Sub
catch:
  If err.Number = 424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    'tagValues.Add "", Tag
    'Resume
  End If
End Sub

Sub getDCL(statement As String)
  Dim Index As Long, wNumPar As Long
  Dim token As String
  Dim i As Integer
  Dim level As Integer
  Dim includeName As String
  Dim spytoken As String
  Dim originstatement As String
  
  On Error GoTo ErrMsg
  ''''''''''''''''''''''''''''''
  'iterazione array attributes:
  ''''''''''''''''''''''''''''''
  'SQ 29-10-07 - il default � EXTERNAL!(?)
  'IsExternal = False
  IsExternal = True
  operator = "DCL"
  Occurs = 0
  
  originstatement = statement 'PLItoCBL
  'SQ 14-12-06 (tolta inizializzazione per utilizzarlo come "flag")
  'level = 1
  
  ''''''''''''''''''
  ' LIVELLO/NOME
  ''''''''''''''''''
  token = nexttoken(statement)
  If IsNumeric(token) Then
    'AC 14/05/10________________
    spytoken = nexttoken(statement)
    If spytoken = "+" Or spytoken = ":" Or spytoken = "/" Or spytoken = "-" Then
      statement = spytoken & " " & statement
      Exit Sub
    Else
      statement = spytoken & " " & statement
    End If
    level = token
    token = nexttoken(statement)  'Non usare il New! Mi serve la virgola!
  End If
  'VIRGOLA: altro elemento!
  If Right(token, 1) = "," Then
    token = Left(token, Len(token) - 1)
    statement = ", " & statement  'isolo la virgola per il case
  End If

  'ocio alla virgola!
  fieldName = token  'CONSIDERARE CASO (A, B, C)... (BASTA CONTROLLARE SE DENTRO FieldName c'e' parentesi...)
  If Pli2cobol Then fieldName = Replace(fieldName, "_", "-")
  If (InStr(ParamLinkage, fieldName) And ParamLinkage <> "") Or (CurrentTag = "LINKAGE" And level > 1) Then
    CurrentTag = "LINKAGE"
  Else
    CurrentTag = "WORKING-STORAGE"
  End If
  If InStr(fieldName, ",") Then
    fieldName = Replace(Replace(fieldName, "(", ""), ")", "")
  End If
  fieldName = Trim(CheckReservedCBL(fieldName))
  
  token = nexttoken(statement) 'Non usare il New! Mi serve la virgola!
  If token <> "," And Right(token, 1) = "," Then
    token = Left(token, Len(token) - 1)
    statement = ", " & statement  'isolo la virgola per il case
  End If
  
  ' Controllo ARRAY
  If (Left(token, 1) = "(") Then
    If Right(token, 1) = ")" Then
      token = Replace(Replace(token, "(", ""), ")", "")
      If InStr(token, ":") Then
        ' Mauro: Gestisco gli Occurs
        Dim ParOccurs() As String, NumOccurs() As Integer
        Dim a As Integer
        ParOccurs() = Split(token, ":")
        ' Caso Semplice: (a:b)
        If UBound(ParOccurs) = 1 Then
          ReDim NumOccurs(1)
          NumOccurs(0) = IIf(IsNumeric(ParOccurs(0)), ParOccurs(0), getFieldValue(ParOccurs(0)))
          NumOccurs(1) = IIf(IsNumeric(ParOccurs(1)), ParOccurs(1), getFieldValue(ParOccurs(1)))
          Occurs = CInt(NumOccurs(1) - NumOccurs(0) + 1)
        Else
        ' Casi Complessi: (a:b,c:d)...
        '                 (a,b:c)...
        '                 (a:b,c,d)...
          addSegnalazione "(" & token & ")" & statement, "P00", "(" & token & ")" & statement
        End If
      ElseIf InStr(token, ",") Then
        ' TMP: da gestire
        Occurs = Left(token, InStrRev(token, ",") - 1)
      ElseIf IsNumeric(token) Then
        Occurs = token
      Else
        ' TMP: da gestire decisamente meglio
        ' Bisognerebbe risalire alla variabile e mettere il suo valore reale
        'Occurs = token
        Occurs = 0
        'AC
        If Pli2cobol Then
          tagWrite "#" & Space(5) & "*" & originstatement
        End If
      End If
    Else
      MsgBox "tmp:DCL??" & token & "-" & statement
    End If
    token = nexttoken(statement)
  End If
  ''''''''''''''''''''''''''
  ' ATTRIBUTES
  ''''''''''''''''''''''''''
  While Len(statement) Or Len(token) '-PER EXTERNAL...
    Select Case token
      Case "INTERNAL", "INT", "EXTERNAL", "EXT"
        'SQ 29-10-07 - il default � EXTERNAL!(?)
        'If (Left(token, 3) = "EXT") Then
          IsExternal = Left(token, 3) = "EXT"
        'End If
      Case "FILE", "RECORD"
         'PER ORA NON GESTISCO IL FILE SYSPRINT
        If fieldName <> "SYSPRINT" And fieldName <> "SYSIN" And fieldName <> "SYSOUT" Then
          ReDim Preserve arrFiles(UBound(arrFiles) + 1)
          If InStr(statement, "RECSIZE") Then
            spytoken = nexttoken(Mid(statement, InStr(statement, "RECSIZE") + 7))
            spytoken = Replace(spytoken, "(", "")
            spytoken = Replace(spytoken, ")", "")
            arrFiles(UBound(arrFiles)).recsize = IIf(IsNumeric(spytoken), spytoken, 0)
          End If
          arrFiles(UBound(arrFiles)).Name = fieldName
          ReDim Preserve ArrFD(UBound(arrFiles) + 1)
          ArrFD(UBound(arrFiles)) = fieldName
          parseFile IIf(token = "FILE", statement, "RECORD " & statement)
          Exit Sub
        Else
          If Pli2cobol Then statement = ""
        End If
      Case "POINTER", "PTR"
        If Pli2cobol Then
          If InStr(fieldName, ",") Then
            For i = 0 To UBound(Split(fieldName, ",")) - 1
              If (InStr(ParamLinkage, Split(fieldName, ",")(i)) And ParamLinkage <> "") Then CurrentTag = "LINKAGE"
              tagWrite Space(7) & "01 " & Trim(Split(fieldName, ",")(i)) & " IS POINTER."
            Next i
          Else
          tagWrite Space(7) & "01 " & fieldName & " IS POINTER."
          End If
          statement = ""
          Exit Sub
        End If
      Case "LABEL"
        '
      Case "BUILTIN"
       statement = ""
        
      Case "ENTRY", "RETURNS"
        'Se ho RETURNS/OPTIONS prima della ENTRY me li perdo...
        parseEntry token, statement
      Case "OPTIONS"
        parseEntry token, statement
      Case "%INCLUDE"
        'MIGRAZIONE
        If Pli2cobol Then
          includeName = nexttoken(statement)
          If includeName = "SYSLIB" Then
            includeName = Replace(Replace(nexttoken(statement), ")", ""), "(", "")
          End If
          If InStr(includeName, ";") Then includeName = Left(includeName, InStr(includeName, ";") - 1)
          tagWrite indent & "COPY " & includeName & "."
          ' Ripulisco la virgola, senn� mi riscrive il vecchio campo
          If Left(statement, 1) = "," Then statement = Trim(Mid(statement, 2))
        Else
          'silvia 08-10-2008
          getInclude statement
        End If
        'statement = ""
      Case "%"
        'SQ 6-11-07 - "% INCLUDE" staccati!
        token = nexttoken(statement)
        'silvia 08-10-2008
        If token = "INCLUDE" And Pli2cobol = False Then getInclude statement
        'MIGRAZIONE
        If token = "INCLUDE" And Pli2cobol Then tagWrite indent & "COPY " & statement & "."
      Case ","
        'SQ 14-12-06 (tolta inizializzazione per utilizzarlo come "flag")
        If level Then
          'Caso "semplice": DCL 1 ciccio,...
          '''''''''''''''''''''''''
          'DATI:
          '''''''''''''''''''''''''
          statement = token & " " & statement 'Non passarla direttamente: "per copia"!
          parseData level, fieldName, statement
        Else
          'Caso "complesso": DCL miste separate da ","
          getDCL statement
          Exit Sub
        End If
     Case "ALIGNED", "UNALIGNED", "UNAL"
        'If Not Pli2cobol Then
        '  statement = token & " " & statement
        'End If
     Case "LIKE"
        If Pli2cobol Then
          token = nexttoken(statement)
          tagWrite indent & "COPY " & token & " REPLACING ==" & token & "== BY ==" & fieldName & "."
          If statement <> "" Then tagWrite "#     *" & statement
          Exit Sub
        End If
      Case Else
        '''''''''''''''''''''''''
        'DATI:
        '''''''''''''''''''''''''
        If IsNumeric(token) Then
          level = token
        Else
          statement = token & " " & statement 'Non passarla direttamente: "per copia"!
          parseData level, fieldName, statement
        End If
    End Select
    'PER EXTERNAL...
    token = nexttoken(statement)
    If token <> "," And Right(token, 1) = "," Then
      token = Left(token, Len(token) - 1)
      statement = ", " & statement  'isolo la virgola per il case
    End If
  Wend
  Exit Sub
ErrMsg:
  addSegnalazione Left(statement, 50), "P00", Left(statement, 50)
  'sq:
  'MsgBox Err.Description
  'Resume
  'Exit Sub
  'Select Case Err.Number
  '  Case Else
  '    Parser.PsFinestra.ListItems.Add , , Err.Description
  '    SegnalaIstruzione wrec, gbidoggetto
  '    m_Fun.WriteLog Err.Description & "  IdObj: " & gbidoggetto, "PARSER PLI", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
  'End Select
End Sub

Sub parseData(level As Integer, ByVal token As String, statement As String)
  Dim Interi As Integer, Decimali As Integer, Digit As Integer, Ordinale As Integer
  Dim Lunghezza As String 'attenzione: lunghezza variabile
  Dim tokenNeeded As Boolean
  Dim cobolField As cblData
  Dim newData As Boolean, endData As Boolean
  Dim nomeRed As String, sign As Boolean
  Dim parOpen As Integer, parClose As Integer
  Dim stoccurs As String, stdepend As String, pos As String, values As String
  'INIT:
  newData = True
  nomeRed = ""
  Tipo = ""
  Tipo_tipo = ""
  Valore = ""
  'SILVIA 08-10-2008
  ''Occurs = ""
  varying = False
  IsStatic = False
  sign = False
  Lunghezza = 0
  stdepend = ""
  
  'MsgBox "DATI: " & FieldName & "--" & token & "--" & wVar(i)
  While Len(statement) And Not endData
    tokenNeeded = True
    
    If newData Then
      If IsNumeric(token) Then
        level = token
        token = nexttoken(statement)
        'SQ - 9-03-07 VIRGOLA
        If Right(token, 1) = "," Then
          token = Left(token, Len(token) - 1)
          statement = ", " & statement  'isolo la virgola per il case
        End If
      End If
      
      'SQ 14-12-06 (tolta inizializzazione level=1 nel getDCL)
      If level = 0 Then level = 1
      '''''''''''''''''''''''''''''''''''''''
      ' SQ 26-10-07: gestione casi:
      ' 1 ciccio,
      '   %INCLUDE bluto
      ' -> non verificati casi con DCL che
      '    prosegue sotto la include
      ' SQ 06-11-07 - "% INCLUDE" staccati!
      ''''''''''''''''''''''''''''''''''''''
      If token = "%INCLUDE" Or token = "%" Then
        If token = "%" Then
          token = "%" & nexttoken(statement)
        End If
        'Ripristino e proseguo fuori!
        statement = token & " " & statement
        Exit Sub
      Else
        fieldName = token
      End If
      token = nexttoken(statement)
      ' Mauro 29/10/2008
      If Right(token, 1) = "," Then
        token = Left(token, Len(token) - 1)
        statement = ", " & statement  'isolo la virgola per il case
      End If
      newData = False
    End If
    
    'fieldName = Replace(fieldName, "_", "-")
    'If token <> "," And Right(token, 1) = "," Then
    '  token = Left(token, Len(token) - 1)
    '  statement = ", " & statement
    'End If
    
    'SILVIA 07-10-2008
    If InStr(token, "PIC'9") Then
      statement = "PIC" & " " & Mid(token, 4) & " " & statement
      token = nexttoken(statement)
    End If
    
    Select Case token
      'new
      Case "VARYING", "VAR"
        varying = True
        If statement = token Then
            statement = ""
        End If
      'new
      Case "STATIC" ', "AUTOMATIC", "CONTROLLED"
        IsStatic = True
      Case "FIXED", "FLOAT"
        'vanno con DECIMAL/BINARY
        'ATTENZIONE! PUO' ESSERE ANCHE "BIN FIXED(15)"
        Tipo_tipo = token
'spli
        If Len(Tipo) = 0 Then
          Tipo = Switch(token = "FIXED", "DEC", token = "FLOAT", Tipo = "BIN")
        End If
'spli
        token = nexttoken(statement)
        If Left(token, 1) = "(" Then
          Lunghezza = Trim(Mid(token, 2, Len(token) - 2))
        Else
          tokenNeeded = False
          Lunghezza = 1
        End If
        If Tipo = "BIN" Then
           sign = True
        End If
      Case "BASED", "DEF"
        If InStr(statement, "ADDR(") Then
          token = nexttoken(statement)
          'nomeRed = token 'da analizzare...
          'SILVIA 09-10-2008
          If Left(token, 1) = "(" Then
            token = Mid(token, 2)
          End If
          token = Mid(token, IIf(InStr(token, "ADDR"), InStr(token, "ADDR") + 4, 1))
          nomeRed = Replace(token, "(", "")
          nomeRed = Replace(nomeRed, ")", "")
          If InStr(nomeRed, ".") Then
            nomeRed = Mid(nomeRed, InStr(nomeRed, ".") + 1)
          End If
          nomeRed = Trim(nomeRed)
          If Right(nomeRed, 1) = "," Then
            nomeRed = Trim(Left(nomeRed, Len(nomeRed) - 1))
            tokenNeeded = False
            token = ","
          End If
          '' silvia: DA ANALIZZARE IL CASO DEL TIPO :
          'DCL VAR_A CHAR(96) BASED(ADDR(TLPR001A.TLPTELO));
          On Error Resume Next  ' Mi serve per evitare duplicati nella collection
          ColRedefines.Add nomeRed, nomeRed
        End If
        On Error GoTo 0
      'T 22-06-2009
      Case "POS"
        pos = nexttoken(statement)
        pos = Replace(Replace(pos, "(", ""), ")", "")
        ' Mauro 03/09/2009 : Se faccio una Remove su un campo che non c'� nella Collection, va in errore
        On Error Resume Next
        ColRedefines.Remove (nomeRed)
        ColRedefines.Add nomeRed & " , " & pos
        On Error GoTo 0
        nomeRed = nomeRed & " , " & pos
        
      Case "ALIGNED", "UNALIGNED", "UNAL"
        'MsgBox "########ALIGNED/UNALIGNED###### "
      Case "INIT"
        Valore = getINIT(statement)
        
 
      Case "ENTRY", "RETURNS"
        'Se ho RETURNS/OPTIONS prima della ENTRY me li perdo...
        parseEntry token, statement
      Case "POINTER", "PTR"
        Tipo = token
        'MsgBox "##POINTER##"
        'attenzione:
        'POINTER STATIC...
      Case "PIC", "PICTURE", "PIC'", "PICTURE'"
        'ES: PIC'(8)9'
        If Right(token, 1) = "'" Then
          'SQ: schifo... andrebbe bene che nextToken_new/new, ma mi mangierebbe le virgole...
          'Sistemarli una volta per tutte!
          statement = "'" & statement 'restore
        End If
        'token = Replace(nextToken_newnew(statement), "'", "")
        token = Replace(nexttoken(statement), "'", "")
        'spli
        'silvia 14-5-2008 ci vuole la left per i casi del tipo (10)9V(2)9
        'If Right(token, 1) = "(" Then
        token = Replace(token, ",", "")
        If InStr(token, "S") Then
          token = Replace(token, "S", "")
          sign = True
        End If
        Picture = token
        parOpen = 1
        parClose = 1
        Lunghezza = 0
        If Left(token, 1) = "(" Then
          Do While InStr(parClose, token, "(")
            parOpen = InStr(parClose, token, "(")
            parClose = InStr(parOpen, token, ")")
            Lunghezza = Int(Lunghezza) + Int(Mid(token, parOpen + 1, parClose - (parOpen + 1)))
            Tipo = Mid(token, parClose + 1, 1)
            Picture = Tipo & "(" & Lunghezza & ")" & Mid(token, parClose + 2, InStr(parClose, token & "(", "("))
          Loop
          token = Replace(token, ".", "")
          Lunghezza = Int(Lunghezza) + Len(Trim(Mid(token, parClose + 2)))
        Else
          token = Replace(token, ".", "")
          Lunghezza = Len(token)
        End If
        'silvia 14-5-2008 per i casi del tipo (10)9V(2)9
        If InStr(token, "V") Then
          Lunghezza = Lunghezza - 1
          'gestire i decimali
          token = Mid(token, InStr(token, "V") + 1)
          If Left(token, 1) = "(" Then
            Decimali = Mid(token, 2, InStr(token, ")") - 2)
          Else
            Decimali = Len(Trim((Mid(Replace(token, ")", ""), 1))))
          End If
          Lunghezza = Lunghezza & "," & Decimali
        End If
        'silvia 14-5-2008
        Tipo = Right(token, 1)
        ' Tipo = token
      Case "CHAR", "CHARACTER"
        Tipo = token
        token = nexttoken(statement)
        'SQ - 16-10-06
        If Left(token, 1) = "(" Then
          Lunghezza = Trim(Mid(token, 2, Len(token) - 2))
          If Not IsNumeric(Lunghezza) Then
            stdepend = Lunghezza
            Lunghezza = 0
          End If
        Else
          'SQ 14-12-06: E' CHAR e basta: default = 1?!
          Lunghezza = 1
          tokenNeeded = False
        End If
        'ANCHE VARYING...
      Case "DECIMAL", "DEC"
        Tipo = token
       'spli
        token = nexttoken(statement)
        If Left(token, 1) = "(" Then
          Lunghezza = Trim(Mid(token, 2, Len(token) - 2))
        Else
          tokenNeeded = False
          Lunghezza = 1
        End If
      Case "BIN", "BINARY"
        Tipo = token
        token = nexttoken(statement)
        If token = "FIXED" Or token = "FLOAT" Then
          Tipo_tipo = token
          token = nexttoken(statement)
          sign = True
        End If
        'posso avere FIXED/FLOAD | (...) | INIT ""
        If Left(token, 1) = "(" And Right(token, 1) = ")" Then
          Lunghezza = Trim(Mid(token, 2, Len(token) - 2))
          If InStr(Lunghezza, ",") > 0 Then
            Lunghezza = Left(Lunghezza, InStr(Lunghezza, ",") - 1)
            'AC TODO gestire parte decimale
          End If
        Else
          tokenNeeded = False
        End If
      Case "BIT"
        Tipo = token
        token = nexttoken(statement)
        If Left(token, 1) = "(" Then
          Lunghezza = Trim(Mid(token, 2, Len(token) - 2))
        ElseIf IsNumeric(token) Then
          Lunghezza = CInt(token)
        Else ' caso INIT o ','
          statement = token & " " & statement
          Lunghezza = 1
        End If
        Lunghezza = Int((Lunghezza) / 8)
        If Lunghezza = 0 Then Lunghezza = 1
        If token = Trim(statement) Then statement = ""
      Case "SYSTEM"
        '
      Case "INTERNAL", "INT", "EXTERNAL", "EXT"
        'SQ 29-10-07 - il default � EXTERNAL!(?)
        'If (Left(token, 3) = "EXT") Then
          IsExternal = Left(token, 3) = "EXT"
        'End If
      'new
      Case "CONDITION"
        'MsgBox "CONDITION... gestire"
      Case "SIGNED"
        sign = True
      Case "UNSIGNED"
        sign = False
      Case "LOW" 'AC 15/04/10
        '
      Case Else
        'FARE CASE
        If token = "," Then
          cobolField = getCobolData(fieldName, Tipo, Lunghezza, sign, varying)
          ' Mauro 10/12/2007 : gestione tipo campi di gruppo
          If oldLevel < level Then
            DataItem(Idx).DatString = "A"
          End If
          oldLevel = level
          'Dati:
          Idx = Idx + 1
          If level = 1 Then
            Ordinale = 1
          Else
            Ordinale = Ordinale + 1
          End If
          DataItem(Idx).DatLev = level
          DataItem(Idx).DatNomeFld = cobolField.Name
          'SQ [01]
          DataItem(Idx).procName = procName
          DataItem(Idx).DatOrdinale = Ordinale
          DataItem(Idx).DatString = cobolField.dataType(0)
          DataItem(Idx).DatVal = Valore
          DataItem(Idx).DatInt = cobolField.dataLen
          DataItem(Idx).DatDec = cobolField.dataDec
          DataItem(Idx).DatNomeRed = nomeRed
          'DataItem(y).DatOccurs = occurs
          'If Occurs <> "" Then
            DataItem(Idx).DatOccurs = Occurs
          'End If
          DataItem(Idx).DatDigit = cobolField.bytes
          Select Case cobolField.dataType(1)
            Case "COMP"
              DataItem(Idx).DatUsage = "BNR"
            Case "COMP-3"
              DataItem(Idx).DatUsage = "PCK"
            Case "BIT", "PTR", "VAR"
              DataItem(Idx).DatUsage = cobolField.dataType(1)
            Case Else
              DataItem(Idx).DatUsage = ""
          End Select
          DataItem(Idx).DatSign = IIf(sign = True, "S", " ")
          'silvia
          DataItem(Idx).idCopy = 0
          If Pli2cobol Then getData level, sign, CLng(Lunghezza), CLng(cobolField.dataDec), cobolField.dataType, nomeRed, stdepend

          'INIT:
          newData = True
          nomeRed = ""
          Tipo = ""
          Tipo_tipo = ""
          Valore = ""
          Occurs = 0
          varying = False
          IsStatic = False
          sign = False
          level = 1
          Lunghezza = 0
          'silvia 06-10-2008
        Else
          'SQ: perch� era cos�? c'� la parentesi attaccata?!
          'If (Left(token, 3) = "PIC" Or Left(token, 7) = "PICTURE") Then
          If Left(token, 4) = "PIC(" Or Left(token, 8) = "PICTURE(" Or _
             Left(token, 3) = "PIC" Or Left(token, 7) = "PICTURE" Then
            tokenNeeded = False
            'scorrettezza sullo stream... ripristino
            statement = Mid(token, InStr(token, "'")) & statement
            token = "PIC"
          Else
            'spli: provo, tanto � comunque un caso anomalo
            If Left(token, 1) = "(" Then
              'Occurs = Trim(Mid(token, 2, IIf(InStr(token, ",") > 0, InStr(token, ",") - 2, Len(token) - 2)))
              stoccurs = Replace(Replace(token, "(", ""), ")", "")
              If IsNumeric(stoccurs) Then
                Occurs = CInt(stoccurs)
              Else
                'silvia 16-03-2009: gestione caso (0:9) oppure (-3:6)
                If InStr(stoccurs, ":") Then
                  ' Mauro: Gestisco gli Occurs
                  ''Occurs = CInt(Split(stoccurs, ":")(1) - Split(stoccurs, ":")(0) + 1)
                  Dim ParOccurs() As String, NumOccurs() As Integer
                  Dim a As Integer
                  ParOccurs() = Split(stoccurs, ":")
                  ' Caso Semplice: (a:b)
                  If UBound(ParOccurs) = 1 Then
                    ReDim NumOccurs(1)
                    NumOccurs(0) = IIf(IsNumeric(ParOccurs(0)), ParOccurs(0), getFieldValue(ParOccurs(0)))
                    NumOccurs(1) = IIf(IsNumeric(ParOccurs(1)), ParOccurs(1), getFieldValue(ParOccurs(1)))
                    Occurs = CInt(NumOccurs(1) - NumOccurs(0) + 1)
                  Else
                  ' Casi Complessi: (a:b,c:d)...
                  '                 (a,b:c)...
                  '                 (a:b,c,d)...
                    addSegnalazione "(" & stoccurs & ")" & statement, "P00", "(" & stoccurs & ")" & statement
                  End If
                End If
              End If
            Else
              'MsgBox "+++++ COS'E'?" & token
            End If
          End If
        End If
    End Select
    If tokenNeeded Then
      token = nexttoken(statement)  'Non usare il "new"... la virgola serve!
      'Schifezzina per separare la virgola...
      If token <> "," And Right(token, 1) = "," Then
        token = Left(token, Len(token) - 1)
        statement = ", " & statement
      End If
    '  If statement = "" And token <> "" Then
    '    statement = token
    '  End If
    End If
  Wend
  '
  cobolField = getCobolData(fieldName, Tipo, Lunghezza, sign, varying)
  ' Mauro 10/12/2007 : gestione tipo campi di gruppo
  If oldLevel < level Then
    DataItem(Idx).DatString = "A"
  End If
  oldLevel = level
  'Dati:
  Idx = Idx + 1
  If level = 1 Then
    Ordinale = 1
  Else
    Ordinale = Ordinale + 1
  End If
  DataItem(Idx).DatLev = level
  DataItem(Idx).DatNomeFld = cobolField.Name
  'SQ [01]
  DataItem(Idx).procName = procName
  DataItem(Idx).DatOrdinale = Ordinale
  DataItem(Idx).DatString = cobolField.dataType(0)
  DataItem(Idx).DatVal = Valore
  DataItem(Idx).DatInt = cobolField.dataLen
  DataItem(Idx).DatDec = cobolField.dataDec
  DataItem(Idx).DatNomeRed = nomeRed
  'If Occurs <> "" Then
    DataItem(Idx).DatOccurs = Occurs
  'End If
  'spli
  DataItem(Idx).DatDigit = cobolField.bytes
  Select Case cobolField.dataType(1)
    Case "COMP"
      DataItem(Idx).DatUsage = "BNR"
    Case "COMP-3"
      DataItem(Idx).DatUsage = "PCK"
    Case "BIT", "PTR", "VAR"
      DataItem(Idx).DatUsage = cobolField.dataType(1)
    Case Else
      DataItem(Idx).DatUsage = ""
  End Select
  DataItem(Idx).DatSign = IIf(sign = True, "S", " ")
  'silvia
  DataItem(Idx).idCopy = 0

  'silvia 06-10-2008
  If Pli2cobol Then
      getData level, sign, CLng(Lunghezza), CLng(cobolField.dataDec), cobolField.dataType, nomeRed, stdepend
  End If
  'Exit Sub
'catch:
'  If Pli2cobol Then writeLog "W", "Field " & fieldName & ": Warning on byte length AND Redefines."
'  Resume Next
End Sub
Private Function CheckReservedCBL(source As String) As String
Dim rs As Recordset
CheckReservedCBL = source
If Not InStr(source, ",") > 0 And Not InStr(source, "'") > 0 Then
  Set rs = m_Fun.Open_Recordset("select CBL_Translation from psPLI_WordsTranslation where PLI_Word = '" & UCase(Trim(source)) & "'")
  If Not rs.EOF Then
    CheckReservedCBL = rs!CBL_Translation
  End If
End If
End Function


'silvia 6-10-2008
Sub getData(level As Integer, sign As Boolean, Lunghezza As Long, Decimali As Long, Tipo() As String, Based As String, Optional stdepend As String)
  Dim previousTag As String
  Dim statement As String
  Dim Segno As String, cblPic As String
  Dim i As Integer
  Dim values() As String
  Dim Fields() As String
  Dim Occurs1 As Long, Occurs2 As Long
  Dim stvalore As String, stOccursInit As String, stringavalore As String
  'On Error GoTo ErrorHandler
  'backup
  previousTag = CurrentTag
  fieldName = Replace(fieldName, "_", "-")
  If (InStr(ParamLinkage, " " & fieldName) And ParamLinkage <> "") Or (InStr(Based, ParamLinkage) And ParamLinkage <> "") Or (CurrentTag = "LINKAGE" And level > 1) Then
     CurrentTag = "LINKAGE"
  Else
     CurrentTag = "WORKING-STORAGE"
  End If
  'AC 04/01/2011
  ' PAROLE RISERVATE PLI
 ' fieldName = CheckReservedCBL(fieldName)
  
  'AC ----- gestione variabili da locali(PLI)  a globali(COBOL)
  If level = 1 Then
    fieldName = prefix(fieldName)
  End If
  '--------------------:-);-):D---------------------
  
  
  'T gestione "DEPENDING ONw
  If Len(stdepend) Then
    stdepend = Replace(stdepend, "_", "-")
    tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & "."
    tagWrite Space(7) + Space(level) & format(level + 1, "00") & " FILLER PIC " & Tipo(0) & "  OCCURS 1 TO 9999 " & vbCrLf _
             & Space(7) + Space(level) & "   DEPENDING ON " & stdepend & "."
    Exit Sub
  End If
  
  'silvia gestione VALUE
  stvalore = IIf(Len(Valore), IIf(Trim(Valore) = "", " VALUE  SPACE", IIf(Tipo(0) = "9", " VALUE " & Valore, " VALUE '" & Valore & "'")), "")
  If Len(stvalore) > 50 And Occurs = 0 Then
    stvalore = vbCrLf & Space(10) & stvalore
    stvalore = Replace(stvalore, " VALUE ", vbCrLf & Space(10) & "VALUE ")
  End If
  ' silvia 08-10-2008 : gestione redefines
  If Len(Based) Then
    If InStr(Based, ",") Then
      If Not IsNumeric(Based) Then
        ' Mauro: Pezza Temporanea
        tagWrite Space(7) & "01" & " " & "FILLER." & vbCrLf _
               & Space(9) & format(level + 1, "00") & " FILLER  PIC X(0)."
        tagWrite Space(9) & format(level + 1, "00") & " " & fieldName & Space(35 - Len(fieldName)) & " PIC " & IIf(sign, "S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza & ")") & stvalore & "."
        writeLog "E", "getData: error on BASED property!"
      Else
        tagWrite Space(7) & "01" & " " & "FILLER." & vbCrLf _
               & Space(9) & format(level + 1, "00") & " FILLER  PIC X(" & CInt(Mid(Based, InStr(Based, ", ") + 2)) - 1 & ")."
        tagWrite Space(9) & format(level + 1, "00") & " " & fieldName & Space(35 - Len(fieldName)) & " PIC " & IIf(sign, "S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza & ")") & stvalore & "."
      End If
    Else
      If Len(Based) < 10 Then
        tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & Space(10 - Len(Based)) & IIf(Occurs > 0, " OCCURS " & Occurs, "") & IIf(Lunghezza > 0, " PIC " & IIf(sign, "S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")"), "") & vbCrLf & Space(7) + Space(level + 2) & "REDEFINES " & Replace(Based, "_", "-") & "."
      Else
        tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & Space(5) & IIf(Occurs > 0, " OCCURS " & Occurs, "") & IIf(Lunghezza > 0, " PIC " & IIf(sign, "S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")"), "") & " REDEFINES " & vbCrLf & _
               Space(12) & Replace(Based, "_", "-") & "."
       End If
      level = level + 2
    End If
  Else
    If Lunghezza > 0 Then
      If Occurs = 0 Then
        If Left(fieldName, 1) = "(" And Right(fieldName, 1) = ")" Then
          fieldName = Replace(Replace(fieldName, ")", ""), "(", "")
          Fields = Split(fieldName, ",")
          For i = 0 To UBound(Fields)
            If Len(Fields(i)) < 20 Then
              tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & Fields(i) & Space(20 - Len(Fields(i))) & " PIC " & IIf(sign, "S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza & ")") & stvalore & "."
            Else
              tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & Fields(i) & Space(35 - Len(Fields(i))) & " PIC " & IIf(sign, "S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza & ")") & stvalore & "."
            End If
          Next i
        Else
          'LIVELLO 88 PER IL TIPO BIT DI LIVELLO 1
          If Tipo(1) = "BIT" Then
             tagWrite Space(7) & "77" & " " & "SW-" & fieldName & Space(20 - Len(fieldName)) & " PIC " & Tipo(0) & "(" & Lunghezza & ")."
             tagWrite Space(9) & "88" & " " & fieldName & Space(20 - Len(fieldName)) & stvalore & "."
          Else
            If Len(fieldName) < 20 Then
              tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & Space(20 - Len(fieldName)) & " PIC " & IIf(sign, "S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "") & IIf(Len(Tipo(1)), " " & Tipo(1), "")) & stvalore & "."
            Else
              tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & Space(35 - Len(fieldName)) & " PIC " & IIf(sign, " S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "") & IIf(Len(Tipo(1)), " " & Tipo(1), "")) & IIf(Len(Valore), vbCrLf & Space(10) & stvalore, "") & "."
            End If
          End If
        End If
      Else
        If Len(Valore) > 0 Then
          If InStr(Valore, ",") Then
            values = Split(Valore, ",")
            tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & "-TAB."
            level = level + 2
            If UBound(values) = Occurs - 1 Then
              For i = 0 To Occurs - 1
              stringavalore = values(i)
              stringavalore = IIf(Len(stringavalore) > 61, Space(10) & Mid(stringavalore, 1, 62) & vbCrLf & Space(6) & "-" & Space(3) & "'" & Mid(stringavalore, 63), stringavalore)
              'GLORIA: se pi� lungo di colonna 68 vado a capo
               If Len(stringavalore) < 61 Then
               'If Len(Space(7) + Space(level - 1) & format(level, "00") & " FILLER" & " PIC " & IIf(sign, " S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "")) & " VALUE " & values(i) & ".") <= 60 Then
                 tagWrite Space(7) + Space(level - 1) & format(level, "00") & " FILLER" & " PIC " & IIf(sign, " S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "")) & " VALUE " & vbCrLf & Space(11) & values(i) & "."
               Else
                 tagWrite Space(7) + Space(level - 1) & format(level, "00") & " FILLER" & " PIC " & IIf(sign, " S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "")) & vbCrLf & Space(11) & " VALUE " & vbCrLf & stringavalore & "."
               End If
             Next i
            Else
              For i = 0 To UBound(values) - 1
              'GLORIA: se pi� lungo di colonna 68 vado a capo
                If Len(Space(7) + Space(level - 1) & format(level, "00") & " FILLER" & " PIC " & IIf(sign, " S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "")) & " VALUE " & values(i) & ".") <= 60 Then
                  tagWrite Space(7) + Space(level - 1) & format(level, "00") & " FILLER" & " PIC " & IIf(sign, " S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "")) & " VALUE " & values(i) & "."
                Else
                  tagWrite Space(7) + Space(level - 1) & format(level, "00") & " FILLER" & " PIC " & IIf(sign, " S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "")) & vbCrLf & Space(11) & " VALUE " & vbCrLf & Space(7) + Space(level - 1) & values(i) & "."
                End If
              Next i
              'T 14-12-2010 caso di init mista con valori e occurs
              stOccursInit = Trim(values(UBound(values)))
              If stOccursInit <> "" Then
                 stOccursInit = Replace(stOccursInit, "(", "")
                 If UBound(Split(stOccursInit, ")")) Then
                   Occurs1 = Split(stOccursInit, ")")(0)
                   If UBound(Split(stOccursInit, ")")) = 2 Then
                     stvalore = Trim(Split(stOccursInit, ")")(2))
                     If Left(stvalore, 1) = "'" Then
                        Tipo(0) = "X"
                     ElseIf IsNumeric(stvalore) Then
                      Tipo(0) = "9"
                     End If
                     tagWrite Space(7) + Space(level - 1) & format(level, "00") & " FILLER OCCURS " & Occurs1 & " PIC " & Tipo(0) & "(" & Trim(Split(stOccursInit, ")")(1)) & ")  VALUE " & vbCrLf & Space(7) + Space(level - 1) & stvalore & "."
                   End If
                 End If
              End If
            End If
            level = level - 2
            tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & " OCCURS " & Occurs & IIf(Lunghezza > 0, " PIC " & IIf(sign, "S", "") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza - Decimali & ")"), "") & vbCrLf & Space(7) + Space(level + 2) & "REDEFINES " & fieldName & "-TAB."
          Else
            tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & " OCCURS " & Occurs & " PIC " & IIf(sign, "S", " ") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "") & IIf(Len(Tipo(1)), " " & Tipo(1), "")) & stvalore & "."
          End If
        Else
          If InStr(Occurs, ",") Then
            Occurs1 = Left(Occurs, InStr(Occurs, ",") - 1)
            Occurs2 = Mid(Occurs, InStr(Occurs, ",") + 1)
            tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & " OCCURS " & Occurs1 & "."
            tagWrite Space(7) + Space(level) & format(level + 1, "00") & " FILL-" & fieldName & " OCCURS " & Occurs2 & "."
            tagWrite Space(7) + Space(level + 1) & format(level + 2, "00") & " FILLER PIC " & IIf(sign, "S", " ") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "") & IIf(Len(Tipo(1)), " " & Tipo(1), "")) & stvalore & "."
          Else
            tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & " OCCURS " & Occurs & " PIC " & IIf(sign, "S", " ") & IIf(Len(Picture), Picture, Tipo(0) & "(" & Lunghezza & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "") & IIf(Len(Tipo(1)), " " & Tipo(1), "")) & stvalore & "."
          End If
          'level = level + 2
          'For i = 1 To Occurs
          '  tagWrite Space(7) + Space(level - 1) & format(level, "00") & " FILLER" & " PIC " & IIf(sign = True, "S", " ") & Tipo(0) & "(" & Lunghezza & ")" & IIf(Decimali > 0, "V" & Tipo(0) & "(" & Decimali & ")", "")
          'Next i
        End If
      End If
    Else
      If Lunghezza = 0 And Tipo(0) = "X" Then
        tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & IIf(Occurs > 0, " OCCURS " & Occurs, "") & "."
      Else
        If Len(fieldName) < 20 Then
          tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & Space(20 - Len(fieldName)) & IIf(Occurs > 0, " OCCURS " & Occurs, "") & IIf(Tipo(0) = "POINTER", "", " PIC ") & IIf(sign, "S", "") & IIf(Len(Picture), Picture, Tipo(0)) & stvalore & "."
        Else
          tagWrite Space(7) + Space(level - 1) & format(level, "00") & " " & fieldName & Space(35 - Len(fieldName)) & IIf(Occurs > 0, " OCCURS " & Occurs, "") & IIf(Tipo(0) = "POINTER", "", " PIC ") & IIf(sign, "S", "") & IIf(Len(Picture), Picture, Tipo(0)) & stvalore & "."
        End If
      End If
    End If
  End If
  
  Picture = ""
  'T
  '''CurrentTag = previousTag
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parseEntry(token As String, statement As String)
  Dim environment As String, entry As String
  
  'SQ - PERICOLO!!!! DA VERIFICARE:
  'HO gi� parserato lo statement nel getDCL...
  entries(entryIndex).external = IsExternal
  'Fare stessa cosa per le OPTIONS (se servono!)
  
  While Len(token)
    'tokenNeeded = True
    Select Case token
      Case "ENTRY"
        'inizio
        entries(entryIndex).Name = fieldName
      Case "RETURNS"
        token = nexttoken(statement)
        entries(entryIndex).returns = Mid(token, 2, Len(token) - 2)
      Case "OPTIONS"
        token = nexttoken(statement)
        entries(entryIndex).options = Mid(token, 2, Len(token) - 2)
      Case "VARIABLE"
        entries(entryIndex).variable = True
      Case "INTERNAL", "INT", "EXTERNAL", "EXT"
        entries(entryIndex).external = Left(token, 3) = "EXT"
      Case "REDUCIBLE", "RED", "IRREDUCIBLE", "IRRED"
      Case ","
        'Fine ENTRY: analisi resto della DCL
        entryIndex = entryIndex + 1
        ' Mauro 10/12/2007 : mi serve per gestire i campi di gruppo
        oldLevel = 1
        getDCL statement
        Exit Sub
      Case Else
        If (Left(token, 1) = "(") Then
          entry = Mid(token, 2, Len(token) - 2)
          '???
        Else
          'MsgBox "Cos'e'? --- " & token
        End If
    End Select
    token = nexttoken(statement)
    If token <> "," And Right(token, 1) = "," Then
      token = Left(token, Len(token) - 1)
      statement = ", " & statement  'isolo la virgola per il case
    End If
  Wend
  ''''''''''''''''''''''''''''''
  ' Ho solo il nome...
  ' servir� una struttura con tutte le info necessarie...
  ''''''''''''''''''''''''''''''
  entryIndex = entryIndex + 1
  
  'MsgBox "ENTRY: " & vbCrLf & _
  '        " - entries: " & entries & vbCrLf & _
  '        " - results: " & results & vbCrLf & _
  '        " - options: " & options & vbCrLf & _
  '        " - environment: " & environment & vbCrLf & _
  '        " - external: " & External
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''
' Allestisce la PsPgm con i parametri di input
' della proc "MAIN"
''''''''''''''''''''''''''''''''''''''''''''''''
Sub parsePROC(statement As String, label As String)
  Dim token As String
  Dim rs As Recordset
  Dim parameters As String
  
  token = nexttoken(statement)
  statement = token & " " & statement
  If token = "=" Then
    Exit Sub
  End If
  procName = label
  
  ProcIndex = ProcIndex + 1
  If statement = "" Then
    ' PROC senza parametri
    If Pli2cobol Then
       'TMP insert in psPgm_MACRO
      'Parser.PsConnection.Execute "INSERT INTO PsPgm_MACRO (IdPgm,Riga,Macro) VALUES (" & GbIdOggetto & "," & GbOldNumRec & ",'" & label & "')"
      indent = Space(11)
      tagWrite indent & "."
      tagWrite Space(7) & Replace(label, "_", "-") & " SECTION."
      ProcMain = False
      Exit Sub
    End If
  End If
  token = nexttoken(statement)
  If Left(token, 1) = "(" Then
    parameters = token
    parameters = Replace(parameters, "(", "")
    parameters = Replace(parameters, ")", "")
    'PROC con parametri (MAIN PROGRAM o PROC normale)
    token = nexttoken(statement)
    If token = "OPTIONS" Then
      CurrentTag = "COMMENT"
      token = nexttoken(statement)
      'If token = "(MAIN)" Then
      If InStr(token, "(MAIN") Then
        ProcMain = True
        ' ---> MAIN PROC con parametri
        'Else
        If Pli2cobol Then
          ParamLinkage = parameters
        Else
          Parser.PsConnection.Execute "INSERT INTO PsPgm (IdOggetto,[parameters],comando,ProcName) VALUES (" & GbIdOggetto & ",'" & parameters & "','USING','" & procName & "')"
        End If
      End If
    Else
      If Pli2cobol Then
        indent = Space(11)
        tagWrite indent & "."
        tagWrite Space(7) & Replace(label, "_", "-") & "."
        ProcMain = False
      End If
      ' ---> PROC con parametri
    End If
  ElseIf token = "OPTIONS" Then
    '---> MAIN PROC senza parametri
    token = nexttoken(statement)
    'If token = "(MAIN)" Then
    If InStr(token, "(MAIN") Then
      CurrentTag = "COMMENT"
      If Pli2cobol Then
        ProcMain = True
      Else
        'T 10-07-2009
        Parser.PsConnection.Execute "INSERT INTO PsPgm (IdOggetto,[parameters],comando,ProcName ) VALUES (" & GbIdOggetto & ",'" & parameters & "','USING','" & procName & "')"
        ' Mauro 04/11/2010 : Non posso fare la INSERT secca. Prima devo controllare che non ci sia gi� il record
        'Parser.PsConnection.Execute "INSERT INTO PsLoad (Source,Load) VALUES ('" & NameFromId(GbIdOggetto) & "','" & procName & "')"
        ' Devo aggiungere il record solo se il nome "fisico" � diverso dal nome "logico"
        If NameFromId(GbIdOggetto) <> procName Then
          Set rs = m_Fun.Open_Recordset("select * from psload where source = '" & NameFromId(GbIdOggetto) & "' and load = '" & procName & "'")
          If rs.RecordCount = 0 Then
            rs.AddNew
            rs!source = NameFromId(GbIdOggetto)
            rs!load = procName
            rs.Update
          End If
          rs.Close
        End If
      End If
    End If
  ElseIf token = "RETURNS" Then
    tagWrite Space(7) & Replace(label, "_", "-") & "."
    ProcMain = False
    tagWrite "#     *" & token & statement
  'AC 03/01/2011  'Buonanno a tutti
  'PROC REORDER, veniva ignorata dal parser, nessuna gestione del REORDER (commento), ma ramo solo per intercettare proc
  ElseIf token = "REORDER" Then
    tagWrite Space(11) & "."
    tagWrite Space(7) & Replace(label, "_", "-") & "."
    ProcMain = False
    tagWrite "#     *" & token & statement
  End If
  'SQ [01]
  'TMP insert in psPgm_MACRO
  If Not Pli2cobol Then
  '   Parser.PsConnection.Execute "INSERT INTO PsPgm_MACRO (IdPgm,Riga,Macro,Parametri) VALUES (" & GbIdOggetto & "," & GbOldNumRec & ",'" & label & "','" & Left(parameters, 70) & "')"
  End If
End Sub
Public Function NameFromId(pId As Long) As String
  Dim rs As New Recordset
  Set rs = m_Fun.Open_Recordset("select Nome from Bs_Oggetti  where idoggetto = " & pId)
  If Not rs.EOF Then
    NameFromId = rs!nome
  Else
    NameFromId = ""
  End If
End Function
Sub parseFile(statement As String)
  Dim environment As String
  Dim token As String, previousTag As String
  previousTag = CurrentTag
  If Pli2cobol Then
    If InStr(statement, "KEYED") Then
       getSDefinition "INDEXED"
    Else
       getSDefinition "SEQUENTIAL"
    End If
  End If

  'debug
  'MsgBox "DCL FILE: " & inputString
  'Dim tokenNeeded As Boolean
  token = nexttoken(statement)
  'MsgBox "DATI: " & FieldName & "--" & wWord & "--" & wVar(i)
  While Len(token)
    'tokenNeeded = True
    Select Case token
      Case "VARIABLE"
        'MsgBox "----variable!"
      Case "STREAM"
        If Pli2cobol Then
          getSDefinition "LINE SEQUENTIAL"
          'getFDefinition
        End If
'      Case "RECORD"
'        If Pli2cobol Then
'          If InStr(statement, "KEYED") Then
'             getSDefinition "INDEXED"
'          Else
'             getSDefinition "SEQUENTIAL"
'          End If
          'getFDefinition
'        End If
      Case "INPUT", "OUTPUT"
        arrFiles(UBound(arrFiles)).io = token
      Case "UPDATE"
      Case "SEQUENTIAL", "SEQL", "DIRECT" ', "TRANSIENT" ?
      Case "BUFFERED", "UNBUFFERED"
      Case "EXTERNAL", "INTERNAL"
      Case "ENVIRONMENT", "ENV"
        token = nexttoken(statement)
        environment = Mid(token, 2, Len(token) - 2)
        'MsgBox "environment: " & environment
      Case "PRINT"
        If Pli2cobol Then
          getSDefinition "SEQUENTIAL"
          'getFDefinition
        End If
      Case ","
        getDCL statement
        Exit Sub
      Case Else
        'MsgBox "Cos'e'? --- " & token
    End Select
    token = nexttoken(statement)
    If token <> "," And Right(token, 1) = "," Then
      token = Left(token, Len(token) - 1)
      statement = ", " & statement  'isolo la virgola per il case
    End If
  Wend
  
  CurrentTag = previousTag
End Sub

Sub getSDefinition(storganization As String)
  Dim statement As String
  
  CurrentTag = "ASSIGN"
  'gloria: indent. non prima di colonna 8
'  statement = Space(7 - Len(operator)) & "SELECT " & fieldName & " ASSIGN TO " & "S-" & fieldName & vbCrLf
  statement = Space(7) & "SELECT " & fieldName & " ASSIGN TO " & "F-" & fieldName & vbCrLf
  statement = statement & indent & "ORGANIZATION IS " & storganization & vbCrLf
  If storganization = "INDEXED" Then
     statement = statement & indent & "ACCESS DYNAMIC " & vbCrLf _
                 & indent & "RECORD KEY VSAM-KEY-" & "<" & fieldName & ">" & vbCrLf
  End If
  statement = statement & indent & "FILE STATUS IS FS-" & fieldName & "."
  'silvia RSI
  tagWrite statement
End Sub

Function getFDefinition(fileName As String, recname As String, Optional stkey As String, Optional io As String) As String
  Dim statement As String
  Dim i As Long, recsize As Long
  Dim rs As Recordset
  
  If io = "" Then
    io = "INPUT"
  End If
  For i = 1 To UBound(arrFiles)
    If arrFiles(i).Name = fileName Then
      recsize = arrFiles(i).recsize
      arrFiles(i).stkey = stkey
      
      getFDefinition = IIf(arrFiles(i).endcondition <> "", arrFiles(i).endcondition, "")
      Exit For
    End If
  Next i
  For i = 1 To UBound(ArrFD)
    If ArrFD(i) = fileName Then
      Do While i < UBound(ArrFD)
        ArrFD(i) = ArrFD(i + 1)
        i = i + 1
      Loop
      ReDim Preserve ArrFD(UBound(ArrFD) - 1)
      Set rs = m_Fun.Open_Recordset("select * from PsData_Cmp where " & _
                                    "IdOggetto = " & GbIdOggetto & " AND Nome = " & "'" & Trim(recname) & "' AND Livello = 1 ")
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from PsData_Cmp where " & _
                                      "Nome = " & "'" & Trim(recname) & "' AND Livello = 1 ")
      End If
      CurrentTag = "FD"
      statement = Space(7) & "FD  " & fileName & "." & vbCrLf
      statement = statement & Space(7) & "01 " & fileName & "-REC PIC X(" & IIf(rs.RecordCount, rs!byte & ").", IIf(recsize = 0, 10, recsize) & ").") & IIf(recsize = 0, Space(50) & "BPHX: Change length of RECORD", "")
      rs.Close
      
      tagWrite statement
      CurrentTag = "FILE-STATUS"
      statement = Space(7) & "01 FS-" & fileName & Space(20 - Len(fileName & "   ")) & " PIC X(02) VALUE '00'." & vbCrLf & _
                  Space(9) & "88 " & fileName & "-OK" & Space(20 - Len(fileName & "   ")) & " VALUE '00' '04'." & vbCrLf & _
                  Space(9) & "88 " & fileName & "-EOF" & Space(20 - Len(fileName & "    ")) & " VALUE '10'."
      tagWrite statement
      Exit Function
    End If
  Next i
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'silvia 08-10-2008
Sub parsePLI()
  Dim nFCbl As Long, i As Long, j As Long
  Dim statement As String
  Dim token As String, line As String
  Dim multilinea As Integer, label As Boolean
  Dim TotDigit As Long
  Dim previousTag As String
  Dim nextTkn As String, strLabel As String
  Dim includeName As String
  Dim flgSelect As Boolean
  Dim oncond As String
  Dim appoline2 As String
  Dim appoStatement As String
  Dim StatementArray() As String
         
  On Error GoTo errorHandler
  'silvia 06-10-2008
  indent = Space(11)
  CurrentTag = "COMMENT"
  previousTag = CurrentTag

  'INIT:
  booEliminaRiga = False
  GbNumRec = 0
  ReDim entries(100)
  entryIndex = 0
  ReDim builtins(100)
  builtinIndex = 0
  ReDim CmpIstruzione(0)
  ReDim CampiMove(0)
  'SQ 15-11-06
  ReDim DataValues(0)
  Idx = 0
  SwNomeCall = True
  ReDim arrFiles(0)
  ContWhen = 0

  ReDim ArrEnd(0)
  ContEnd = 0
  ParamLinkage = ""
  Set ColRedefines = New Collection
  Set Builtins_F = New Collection
  Set stCopy_P = New Collection
  ' Mauro 23/07/2008 : E se il file non c'�? Non usciva mai!!!
  If Len(Dir(GbFileInput)) = 0 Then
    MsgBox "This file is not avaliable...", vbExclamation, Parser.PsNomeProdotto
    Exit Sub
  End If

  'gloria: prima di tutto scorro il codice e identifico tutte le proc
  countProc = 0
  ReDim arrProc(0)
  ProcIndex = 0
  flgSelect = False
  '''''''''''''''''''''''''''''''''''
  If Pli2cobol Then
    '''''''''''''''''''''''''''''''''''
    ' SQ issue
    ' Giro preliminare per ottenere le "FUNZIONI": PROC + RETURN
    ' --> ma se RETURN � su un'altra riga????!!!
    '''''''''''''''''''''''''''''''''''
    nFCbl = FreeFile
    Open GbFileInput For Input As nFCbl
    Pli2cobol = False
    While Not EOF(nFCbl)
      Line Input #nFCbl, line
      RipulisciRiga line
       
      If Len(Trim(line)) Then
        If InStr(line, "PROC") And InStr(line, "MAIN") = 0 Then
          If Right(line, 1) = ";" Then
            statement = line
            checkProc statement
          Else
            statement = line
            While Not Right(Trim(line), 1) = ";" And Not EOF(nFCbl)
              Line Input #nFCbl, line
              statement = statement & line
            Wend
            statement = statement & line
            checkProc statement
          End If
        End If
        'T: gestione proc con parametri
        If InStr(line, "PROC") > 0 And InStr(line, "(") > 0 And InStr(line, "OPTIONS") = 0 And InStr(line, "RETURN") = 0 Then
          statement = line
          checkProc statement
          'AC 23/12/10
          If statement = "PROC NOT IN ONE LINE" Then
            appoline2 = line
            While Not InStr(line, ")") > 0
              Line Input #nFCbl, line
              appoline2 = appoline2 & Trim(line)
            Wend
            checkProc appoline2
          End If
        End If
      End If
    Wend
    Pli2cobol = True
    Close #nFCbl
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''''
  ' Giro "vero" di parsing
  ''''''''''''''''''''''''''''''''''''''''''''''''
  CurrentTag = "COMMENT"
  line = ""
  nFCbl = FreeFile
  Open GbFileInput For Input As nFCbl
  While Not EOF(nFCbl)
    'SQ 10-02-06 (potenziale loop - es.: non e' un PLI!)
    While Len(Trim(line)) = 0 And Not EOF(nFCbl)
      Line Input #nFCbl, line
      operator = ""
      GbNumRec = GbNumRec + 1
      RipulisciRiga line
    Wend
    statement = line
    GbOldNumRec = GbNumRec  'linea iniziale statement
    If label Then
      GbOldNumRec = GbOldNumRec - multilinea
    End If
          
    'stefano: caso di riga di commenti, quindi tutta pulita?????
    If line = "" Then
      token = ""
    ElseIf Right(line, 1) = ":" And InStr(Trim(Mid(line, 1, Len(line) - 1)), " ") = 0 Then
      'SQ 14-11-06: LABEL: "CICCIO:" ==> NON ho il ";"!
      '''''''''''''
      'LABEL
      '''''''''''''
      tagWrite indent & "."
      tagWrite Space(7) & Replace(Replace(line, "_", "-"), ":", ".")
      line = ""
      token = ""
      Line Input #nFCbl, line
      GbNumRec = GbNumRec + 1
      multilinea = multilinea + 1
      RipulisciRiga line
      statement = line
      GbOldNumRec = GbNumRec  'linea iniziale statement
      If label Then
        GbOldNumRec = GbOldNumRec - multilinea
      End If
      token = nextToken_newnew(statement)
    Else
      j = 0
      i = 1
      Do
        i = InStr(i, line, "'")
        If i = 0 Then Exit Do
        i = i + 1
        j = Switch(j = 0, 1, j = 1, 0)
      Loop
      multilinea = 0
      
      '''''''''''''''''''''''''''
      ' getStatement:
      '''''''''''''''''''''''''''
      If Not Left(line, 5) = "WHEN " And Not Left(Trim(line), 5) = "OTHER" And Not Left(Trim(line), 9) = "OTHERWISE" Then
        Do While (Right(line, 1) <> ";" Or j = 1) And Not EOF(nFCbl)
          Line Input #nFCbl, line
          GbNumRec = GbNumRec + 1
          multilinea = multilinea + 1
          RipulisciRiga line
          If statement = "" Then
            GbOldNumRec = GbNumRec
          End If
          If Left(statement, 5) = "EXEC " Then
            statement = statement & vbCrLf & indent & " " & line
          Else
            statement = statement & " " & line
          End If
          i = 1
          Do
            i = InStr(i, line, "'")
            If i = 0 Then Exit Do
            i = i + 1
            j = Switch(j = 0, 1, j = 1, 0)
          Loop
        Loop
      End If
      
      While Right(statement, 1) = ";"
        statement = RTrim(Left(statement, Len(statement) - 1))
      Wend
      statement = Replace(statement, ":", ": ")
      'gloria: se "=,+,/" � senza spazio non lo intercetta (il "-" non si pu� gestire perch� usato come separatore nei nomi composti
      ' e "*" "/" per i commenti
      If CurrentTag <> "WORKING-STORAGE" Then
        statement = Replace(statement, "=", " = ")
        statement = Replace(statement, "< =", "<=")
        statement = Replace(statement, "> =", ">=")
        statement = Replace(statement, "^", " NOT ")
      End If
      token = nextToken_newnew(statement)
    End If
    
    label = False
    If Len(indent) < 11 Then indent = Space(11)
    If Left(token, 2) = "/*" Then
      token = nextToken_newnew(statement)
    End If
    
    ''''''''''''''''''''''''''''
    ' STATEMENT parsting
    ''''''''''''''''''''''''''''
    Select Case Trim(token)
      Case "DCL", "DECLARE"
        CurrentTag = "WORKING-STORAGE"
        previousTag = CurrentTag
        CurrentTag = IIf(previousTag = "LINKAGE", previousTag, "WORKING-STORAGE")
        'gloria: gestione fetch
        If Pli2cobol Then
          If InStr(statement, "ENTRY") > 0 Then
            'AC commentiamo ENTRY
            tagWrite "#" & Space(5) & "*" & statement
            'Lascio cmq la vecchia gestione
            token = nexttoken(statement)
            tagWrite Space(7) & "01" & " " & Trim(token) & Space(20 - Len(Trim(token))) & " PIC X(8) " & "VALUES '" & Trim(token) & "'."
            'AC 04/01/2011  forse ci serve tutto lo statement
            statement = token & " " & statement
          End If
        End If
        '*******
        ' Mauro 10/12/2007 : mi serve per gestire i campi di gruppo
        oldLevel = 1
        getDCL statement
        line = ""
        CurrentTag = "PROCEDURE-DIVISION"
      Case "%INCLUDE"
        ' ATTENZIONE: forse vanno divise in Working e Procedure
        'MIGRAZIONE
        If Pli2cobol Then
          CurrentTag = "WORKING-STORAGE"
          statement = Replace(statement, "SYSLIB", "")
          statement = Replace(statement, "(", "")
          statement = Replace(statement, ")", "")
          includeName = nexttoken(statement)
          If InStr(includeName, ";") Then includeName = Left(includeName, InStr(includeName, ";") - 1)
          tagWrite indent & "COPY " & includeName & "."
          ' Ripulisco la virgola, senn� mi riscrive il vecchio campo
          If Left(statement, 1) = "," Then statement = Trim(Mid(statement, 2))
        Else
          'silvia 08-10-2008
          getInclude statement
        End If
        line = ""
      Case "%"
        token = nexttoken(statement)
        Select Case token
          Case "INCLUDE"
            ' ATTENZIONE: forse vanno divise in Working e Procedure
            If Pli2cobol Then
              CurrentTag = "WORKING-STORAGE"
              statement = Replace(statement, "SYSLIB", "")
              includeName = nexttoken(statement)
              If Right(includeName, 1) = ";" Then includeName = Left(includeName, Len(includeName) - 1)
              tagWrite indent & "COPY " & includeName & "."
              ' Ripulisco la virgola, senn� mi riscrive il vecchio campo
              If Left(statement, 1) = "," Then statement = Trim(Mid(statement, 2))
            Else
              'silvia 08-10-2008
              getInclude statement
            End If
            line = ""
          Case Else
            If Pli2cobol Then tagWrite "#     *" & " " & token & " " & statement
        End Select
        line = ""
      Case "PROC", "PROCEDURE"
        'SQ 27-03-07 (gestione "completa" label... (vedi sotto ex gestione PROC...)
        'Perdeva istruzioni tipo: LABEL1: CALL ...
        CurrentTag = "PROCEDURE-DIVISION"
        parsePROC statement, strLabel
        line = ""
        strLabel = ""
      Case "ON"
        oncond = ""
        If Pli2cobol Then
          CurrentTag = "PROCEDURE-DIVISION"
          token = nexttoken(statement)
          Select Case token
            Case "ENDPAGE"
              CurrentTag = "PROCEDURE-DIVISION"
              If InStr(statement, "BEGIN") And OnErrore = "" Then
                parsePROC "", "ON-ENDPAGE"
                statement = ""
                ContEnd = ContEnd + 1
                ReDim Preserve ArrEnd(ContEnd)
                ArrEnd(ContEnd) = "ON-ENDPAGE-EX." & vbCrLf & Space(11) & "EXIT." & vbCrLf
                line = ""
              End If
            Case Else
              If InStr(statement, "BEGIN") Then
                While Trim(line) <> "END;" And Not EOF(nFCbl)
                  Line Input #nFCbl, line
                  GbNumRec = GbNumRec + 1
                  RipulisciRiga line
                  oncond = IIf(Len(oncond), oncond & vbCrLf & "#     *" & line, line)
                Wend
                Line Input #nFCbl, line
                GbNumRec = GbNumRec + 1
                RipulisciRiga line
                If InStr(line, "END;") Then
                  While Trim(line) = "END;"
                    Line Input #nFCbl, line
                    GbNumRec = GbNumRec + 1
                    RipulisciRiga line
                    oncond = IIf(Len(oncond), oncond & vbCrLf & "#     *" & line, line)
                  Wend
                End If
                tagWrite "#     *" & "ON " & token & vbCrLf & "#     *" & oncond
              Else
                tagWrite "#     *" & line
                line = ""
              End If
          End Select
        End If
      Case "PUT"
        CurrentTag = "PROCEDURE-DIVISION"
        If Pli2cobol Then getPUT (statement)
        line = ""
      Case "DO"
        CurrentTag = "PROCEDURE-DIVISION"
        If Pli2cobol Then line = getDO(statement)
      Case "SELECT"
        flgSelect = True
        If Pli2cobol Then
          CurrentTag = "PROCEDURE-DIVISION"
          If InStr(statement, "END-IF") Then
            getSELECT IIf(Len(Left(statement, InStr(statement, "END-IF") - 1)), Left(statement, InStr(statement, "END-IF") - 1), "TRUE")
            line = Mid(statement, InStr(statement, "END-IF") + 6) & ";"
          Else
            getSELECT IIf(Len(statement), statement, "TRUE")
            line = IIf(InStr(statement, "WHEN"), statement, "")
          End If
        End If
      Case "WHEN"
        CurrentTag = "PROCEDURE-DIVISION"
        'gloria: verifico sempre che non sia prima di colonna 11
        If flgSelect Then
          flgSelect = False
        Else
          indent = IIf(Len(indent) - 2 >= 11, Space(Len(indent) - 2), Space(11))
        End If
        If Pli2cobol Then line = getWHEN(statement) & ";"
        indent = indent & "  "
        line = IIf(Trim(line) <> ";", indent & line, "")
      Case "OTHER", "OTHERWISE"
        CurrentTag = "PROCEDURE-DIVISION"
        If Pli2cobol Then
          line = IIf(Len(statement), statement, "CONTINUE") & ";"
          indent = IIf(Len(indent) - 2 >= 11, Space(Len(indent) - 2), Space(11))
          tagWrite indent & "WHEN OTHER"
          indent = Space(Len(indent) + 2)
        End If
      Case "CALL"
        CurrentTag = "PROCEDURE-DIVISION"
        If Pli2cobol Then
          If InStr(statement, "PLIDUMP") Then
            tagWrite indent & "CALL 'CBLDUMP'  USING 'PLIDUMP'  '" & GbNomePgm & "' ."
            line = IIf(Right(statement, 4) = " END", "END;", "")
            statement = ""
          ElseIf InStr(statement, "PLIRETC") Then
            statement = Replace(statement, "PLIRETC", "")
            token = nextToken_new(statement)
            token = Replace(Replace(token, "(", ""), ")", "")
            tagWrite indent & "MOVE " & token & " TO RETURN-CODE"
            If InStr(statement, "END-IF") Then
              line = Mid(statement, InStr(statement, "END-IF")) & ";"
            Else
              line = ""
            End If
            statement = ""
          'silvia gestione chiusura di end-if
          ElseIf InStr(statement, "END-IF") Then
            getPERFORM Left(statement, InStr(statement, "END-IF") - 1)
            line = Mid(statement, InStr(statement, "END-IF")) & ";"
          ElseIf Len(statement) Then getPERFORM statement
            line = ""
          End If
        End If
      Case "SNAP"
        line = statement & ";"
      Case "SIGNAL"
        If Pli2cobol Then
          If statement = "ERROR" Then
            tagWrite indent & "PERFORM ON-ERROR THRU ON-ERROR-EX"
          End If
          If InStr(statement, "ENDPAGE") Then
            tagWrite indent & "PERFORM ON-ENDPAGE THRU ON-ENDPAGE-EX"
          End If
        End If
        line = ""
      Case "OPEN"
        CurrentTag = "PROCEDURE-DIVISION"
        If Pli2cobol Then getOPEN statement
        line = ""
      Case "CLOSE"
        CurrentTag = "PROCEDURE-DIVISION"
        If Pli2cobol Then getCLOSE statement
        line = ""
      Case "READ"
        CurrentTag = "PROCEDURE-DIVISION"
        If Pli2cobol Then
          If InStr(statement, "END-IF") Then
            getREAD Left(statement, InStr(statement, "END-IF") - 1)
            line = Mid(statement, InStr(statement, "END-IF")) & ";"
          Else
            getREAD statement
            If Oncondition <> "" Then
              tagWrite indent & " AT END "
              line = Oncondition & ";"
            Else
              line = ""
            End If
          End If
        End If
      Case "WRITE", "REWRITE"
        CurrentTag = "PROCEDURE-DIVISION"
        If Pli2cobol Then
          If InStr(statement, "END-IF") Then
            getWRITE token, Left(statement, InStr(statement, "END-IF") - 1)
            line = Mid(statement, InStr(statement, "END-IF")) & ";"
          Else
            getWRITE token, statement
            line = ""
          End If
        End If
      Case "END"
        CurrentTag = "PROCEDURE-DIVISION"
        If Pli2cobol Then
          If statement = "" Then
            If Not EOF(nFCbl) Then
              Line Input #nFCbl, line
            End If
            RipulisciRiga line
            If InStr(line, "CEIL") > 0 And ((InStr(line, " + ") Or InStr(line, "*") Or InStr(line, " - ") Or InStr(line, "/")) And InStr(line, "||") = 0 And InStr(line, "'*'") = 0) Then
              statement = line
              token = nextToken_newnew(statement)
              getCeil token, Replace(statement, "=", "")
              line = ""
            Else
              If InStr(line, "ELSE") = 0 Then
                getEND (statement) ''& ";"
              End If
            End If
          Else
            If InStr(statement, "END-IF") Then
              line = getEND(Left(statement, InStr(statement, "END-IF") - 1))
            Else
              line = getEND(statement)
            End If
          End If
        End If
      Case "IF"
        CurrentTag = "PROCEDURE-DIVISION"
        If Pli2cobol Then
          If Right(statement, 2) = "DO" Then
            'gloria: se ho il DO come istruzione dopo il then lo devo bypassare ???
            Line Input #nFCbl, line
            RipulisciRiga line
            GbNumRec = GbNumRec + 1
            line = getIF(Left(statement, Len(statement) - 2) & line)
          Else
            If InStr(statement, " SQL") Then
              line = getIF(statement) & " END-EXEC " & " END-IF"
            Else
              line = getIF(statement) & " END-IF"
            End If
          End If
        End If
      Case "END-IF"
        'silvia: "END-IF" non � una istruzione PLI ma viene aggunta dopo la IF per gestire la chiusura
        If Pli2cobol Then
          If InStr(statement, "ELSE") Then
            If InStr(Mid(line, 5), "END-IF") Then
              line = statement & ";"
              getEND ("")
            Else
              line = statement & ";"
            End If
          Else
            line = statement & IIf(Len(statement), ";", "")
            getEND ("")
          End If
        End If
      Case "THEN", "ELSE"
        CurrentTag = "PROCEDURE-DIVISION"
        tagWrite Space(Len(indent) - 2) & token
        indent = indent & "  "
        If token = "ELSE" Then
          line = IIf(Trim(statement) = "DO", "", statement & " END-IF;")
         'gloria: verifico sempre che non sia prima di colonna 11
          indent = IIf(Len(indent) - 2 >= 11, Space(Len(indent) - 2), Space(11))
        ElseIf UCase(token) = "THEN" Then
          line = IIf(Trim(statement) = "DO", "DO THEN", statement & ";")
          'gloria: verifico sempre che non sia prima di colonna 11
          indent = IIf(Len(indent) - 2 >= 11, Space(Len(indent) - 2), Space(11))
        ElseIf Len(statement) Then
          line = statement & ";"
        Else
          line = ""
        End If
      Case "NOT", "AND"
        line = token
        token = nextToken_newnew(statement)
        tagWrite indent & line & " " & token
        If Len(statement) Then
          line = statement & ";"
        Else
          line = ""
        End If
      Case "SUBSTR"
         If Pli2cobol Then
          line = "(" & getSUBSTR(Mid(statement, 1, InStr(statement, ")"))) & ")" & Mid(statement, InStr(statement, ")") + 1) & ";"
        End If
      Case "TRANSLATE"
        If Pli2cobol Then
          getTRANSLATE statement, ""
        End If
      Case "RETURN"
        'GLORIA: il return � RET-nome procedura
        If Pli2cobol Then
          If Len(statement) Then
            If Left(statement, 1) = "(" And Right(statement, 1) = ")" And InStr(2, statement, "(") = 0 Then
              line = "RET-" & procName & " = " & Mid(statement, 2, Len(statement) - 2) & ";"
            Else
              line = "RET-" & procName & " = " & statement
            End If
          Else
            tagWrite indent & "GOBACK"
            line = ""
          End If
        End If
      Case "LEAVE"
        If Pli2cobol Then
          tagWrite indent & " EXIT PERFORM "
        End If
        line = statement & ";"
      Case "GO", "GOTO"
        If Pli2cobol Then
          tagWrite indent & Replace(token, "GOTO", "GO TO") & statement
        End If
        line = ""
      Case "VERIFY"
      Case "FETCH"
        If Pli2cobol Then
          'AC
          appoStatement = statement
          '-----
          token = nexttoken(statement)
          If InStr(statement, "TITLE") > 0 Then
            statement = Replace(Replace(statement, "TITLE(", ""), ")", "")
            ' devo valorizzare la variabile con il title che trovo tra parentesi
            tagWrite Space(7) & "MOVE " & Trim(statement) & " INTO " & Trim(token)
          End If
          line = ""
          'AC 05/01/2011 ricostuisco lo statement in modo che non rimanga monco dopo questo ramo
          statement = appoStatement
        End If
      Case "EXEC"
        token = nexttoken(statement)
        token = Replace(token, vbCrLf, "")
        Select Case token
          Case "DLI"
          Case "SQL"
            If InStr(statement, "INCLUDE") Then
              CurrentTag = "WORKING-STORAGE"
            Else
              CurrentTag = "PROCEDURE-DIVISION"
            End If
            If InStr(statement, " END-EXEC") Then
              statement = Left(statement, InStr(statement, " END-EXEC") + 9)
            End If
            tagWrite indent & "EXEC " & statement & vbCrLf & IIf(InStr(statement, " END-EXEC"), "", indent & "END-EXEC")
          Case "CICS"
            tagWrite statement & " END-EXEC."
            line = ""
        End Select
        line = ""
                
      Case Else
        'stefano:  tanto per non dare errore: da rivedere, perch� cos� non d� pi� errore in PPP3
        If Len(line) = 0 Then
          If IsNumeric(token) And InStr(token, "(") = 0 And InStr(token, ")") = 0 And UCase(Mid(statement, 1, 5)) <> "ELSE " Then
           'silvia: imposto un oldlevel al massimo
            m_Fun.writeLog "parsePgm_I: INCLUDE#" & GbNomePgm & ", riga:  " & GbOldNumRec
            oldLevel = 100
            statement = token & " " & statement
            getDCL statement
          End If
        'AC 31/10/2008  commentiamo (pluralis maiestatis) la gestione della LABEL
        ' proviamo ad andare avanti senza azzerare la linea
        ElseIf Right(token, 1) = ":" And InStr(token, " ") = 0 Or Left(Trim(statement), 1) = ":" Then
          If Right(token, 1) = ":" Then
            line = Trim(statement) & ";"
            strLabel = Left(token, Len(token) - 1)
            If InStr(statement, "MAIN") = 0 And InStr(line, "PROC;") = 0 And InStr(line, "PROC(") = 0 Then
              If InStr(line, "PROC") = 0 And InStr(line, "REORDER") = 0 Then
                tagWrite Space(11) & "." & vbCrLf & Space(7) & strLabel & "."
              End If
              'strLabel = "" AC secondo me ci serve
              indent = Space(11)
            End If
          ElseIf Left(Trim(statement), 1) = ":" And InStr(statement, "OPTION") = 0 Then
            strLabel = token
            tagWrite Space(11) & "." & vbCrLf & Space(7) & strLabel & "."
            strLabel = ""
            statement = Trim(Right(statement, Len(statement) - 1))
          End If
          line = statement & ";"
          'silvia 08-10-2008
        ElseIf IsNumeric(token) And InStr(token, "(") = 0 And InStr(token, ")") = 0 And UCase(Mid(statement, 1, 5)) <> "ELSE " Then
          'silvia: imposto un oldlevel al massimo
          m_Fun.writeLog "parsePgm_I: INCLUDE#" & GbNomePgm & ", riga:  " & GbOldNumRec
          oldLevel = 100
          statement = token & " " & statement
          getDCL statement
          line = ""
        Else
          'T
          If Left(statement, 1) = "(" Then
            token = token & nextToken_newnew(statement)
          End If
          nextTkn = nextToken_newnew(statement)
          If Left(nextTkn, 1) = "." Then
            token = token & nextTkn
            nextTkn = nextToken_newnew(statement)
          End If
          If nextTkn = "=" Then
            'silvia 10-12-2008
            ' � una MOVE
            If Pli2cobol Then
              'silvia gestione chiusura di end-if
              If InStr(statement, "END-IF") Then
                If (InStr(statement, "ELSE") And InStr(statement, " DO ")) Then
                  line = Mid(statement, InStr(statement, "END-IF")) ''& ";"
                Else
                  line = Mid(statement, InStr(statement, "END-IF")) & ";"
                End If
                statement = Left(statement, InStr(statement, "END-IF") - 1)
              Else
                'TODO scrivere il token ? commentato
                line = ""
              End If
              If (InStr(statement, " + ") Or InStr(statement, "*") Or InStr(statement, " - ") Or InStr(statement, "/")) And InStr(statement, "||") = 0 And InStr(statement, "'*'") = 0 And InStr(statement, "'") = 0 Then
                CurrentTag = "PROCEDURE-DIVISION"
                'prima di tutto controllo che non venga richiamata la CEIL
                If InStr(line, "CEIL") > 0 Or InStr(statement, "CEIL") > 0 Then
                  getCeil token, statement
                Else
                  'getCompute token, IIf(InStr(1, statement, "END-IF") > 0, Mid(statement, 1, InStr(statement, "END-IF") - 1), statement)
                  If InStr(1, statement, "END-IF") > 0 Then
                    'indent = Space(Len(indent) - 3)
                    getCompute token, Mid(statement, 1, InStr(statement, "END-IF") - 1)
                    'T
                    line = line & ";"
                  Else
                    getCompute token, statement
                  End If
                End If
              ElseIf InStr(statement, "||") Then
                CurrentTag = "PROCEDURE-DIVISION"
                If InStr(1, statement, "END-IF") > 0 Then
                  getString token, Mid(statement, 1, InStr(statement, "END-IF") - 1)
                Else
                  getString token, statement
                End If
              ElseIf InStr(statement, "TRANSLATE(") Then
                CurrentTag = "PROCEDURE-DIVISION"
                If InStr(1, statement, "END-IF") > 0 Then
                  getTRANSLATE Mid(statement, 11, InStr(statement, "END-IF") - 1), token
                Else
                  getTRANSLATE Mid(statement, 11), token
                End If
                tagWrite ""
              ElseIf InStr(statement, "CEIL") Then
                getCeil token, statement
                line = ""
              Else
                'gloria: vedo se � una proc
                If isPROC(statement) Then
                  'da fare
                  statement = getProc(token, statement)
                  line = getProc(token, statement)
                Else
                  If InStr(1, statement, "END-IF") > 0 Then
                    getMove token, Mid(statement, 1, InStr(statement, "END-IF") - 1)
                    'T
                    line = line & ";"
                  Else
                    If InStr(statement, ";") Then
                      getMove token, Left(statement, InStr(statement, ";") - 1)
                      line = Mid(statement, InStr(statement, ";") + 1) & ";"
                    Else
                      getMove token, statement
                    End If
                  End If
                End If
              End If
            End If
          Else
            'gloria: 26/05/2009: se non coverto riporto istruzione pari pari con commento
            If Pli2cobol And OnErrore = "" Then
              If InStr(1, line, vbCrLf) > 0 Then
                StatementArray = Split(line, vbCrLf)
                For i = 0 To UBound(StatementArray)
                  tagWrite "#     *" & " " & StatementArray(i)
                Next i
              Else
                tagWrite IIf(Trim(line) <> ";", "#     *" & " " & line, "")
              End If
              line = ""
            End If
          End If
        End If
    End Select
  Wend
  Close #nFCbl
  
  completeData (GbFileInput)
  
  'silvia 09-10-2008: mi serve la lunghezza totale del livello 01 per la FD del file.
  TotDigit = DataItem(1).DatDigit
  
  Exit Sub
errorHandler:
  writeLog "E", "Unexpected Error: " & err.description
  'Resume Next
End Sub
Private Sub getPUT(statement As String)
  Dim param As String, fileName As String
   Dim token As String, stdisplay As String, stresto As String
   tagWrite "  #   *" & statement
   statement = Replace(statement, "_", "-")
   statement = Replace(statement, "(SYSPRINT)", "")
   statement = Replace(statement, "SKIP", "")
   statement = Trim(Replace(statement, "LIST", ""))
   statement = Trim(Replace(statement, "EDIT", ""))
   If InStr(statement, "FILE") Then
      token = nextToken_newnew(statement)
      Select Case token
        Case "FILE"
          fileName = Replace(nextToken_newnew(statement), "_", "-")
          fileName = Replace(Replace(fileName, ")", ""), "(", "")
          fileName = Trim(CheckReservedCBL(fileName))
          param = nextToken_newnew(statement)
          param = Replace(Replace(param, ")", ""), "(", "")
              getFDefinition fileName, param, , "OUTPUT"
              CurrentTag = "PROCEDURE-DIVISION"
              statement = indent & "WRITE " & fileName & "-REC FROM " & param
        Case Else
          statement = "#     *" & token & " " & statement
      End Select
      tagWrite statement
      Exit Sub
   End If
   'Do While statement <> ""
     'token = nextToken_new(statement)
     'Select Case token
     'Case "SKIP"
     
'     Case "EDIT", "LIST", "FILE"
'       If Left(statement, 1) = "(" Then
'          stdisplay = Mid(statement, 2, InStr(statement, ")") - 2)
'          stresto = Mid(statement, InStr(statement, ")"))
'          Exit Do
'       End If
'     End Select
   'Loop
      If Left(statement, 1) = "(" Then
         stdisplay = Mid(statement, 2, InStr(statement, ")") - 2)
         stresto = Mid(statement, InStr(statement, ")"))
         If stresto = ")" Then stresto = ""
      End If
  
   CurrentTag = "PROCEDURE-DIVISION"
  '' statement = Replace(statement, "SKIP", "")
'''   statement = Replace(statement, "EDIT", "")
'''   statement = Replace(statement, "LIST", "")
'''   statement = Replace(statement, "FILE", "")
'''   statement = Replace(statement, "SYSPRINT", "")
'''   statement = Replace(statement, "()", "")
'''   statement = Replace(statement, "(A)", "")
'''   statement = Mid(statement, InStr(statement, "(") + 1)
'''  ' statement = IIf(InStr(statement, "("), Right(statement, InStr(statement, "(") - 1), statement)
'''   statement = Trim(statement)
'''   statement = IIf(Right(statement, 1) = ")", Left(statement, InStr(statement, ")") - 1), statement)
   If Len(stdisplay) > 50 Then
     stdisplay = Replace(stdisplay, "||", vbCrLf & indent & " ")
     stdisplay = Replace(stdisplay, ")(", ")" & vbCrLf & indent & "(")
     stdisplay = Replace(stdisplay, ") (", ")" & vbCrLf & indent & "(")
     stdisplay = Replace(stdisplay, ", ", "," & vbCrLf & indent)
   Else
     'statement = Mid(statement, InStr(1, statement, "(") + 1)
     'statement = Left(statement, Len(statement) - 1)
     stdisplay = Replace(stdisplay, "||", " ")
     stdisplay = Replace(stdisplay, ",", " ")
   End If
''   statement = Replace(Replace(statement, "(", ""), ")", "")
   stdisplay = Replace(stdisplay, ",", " ")
   
   tagWrite indent & "DISPLAY " & vbCrLf & indent & stdisplay
   If Len(stresto) Then tagWrite "  #   *" & indent & stresto

End Sub


Private Sub getOPEN(parameters As String)
  'ASSESSMENT...
  Dim statement As String, token As String
  Dim param As String, i As Integer
  Dim typefile As String
'  CurrentTag = "POPEN"
  token = nextToken_newnew(parameters)
  Select Case token
    Case "FILE"
      parameters = Replace(Replace(parameters, ")", ""), "(", "")
      If InStr(parameters, ",") Then
        ' Loop per l'elenco di files
      Else
        For i = 1 To UBound(arrFiles)
          If arrFiles(i).Name = parameters Then
            typefile = arrFiles(i).io
'''            Do While i < UBound(arrFiles)
'''              arrFiles(i) = arrFiles(i + 1)
'''              i = i + 1
'''            Loop
'''            ReDim Preserve arrFiles(UBound(arrFiles) - 1)
            If typefile = "" Then
              typefile = "INPUT"
            End If
            statement = IIf(typefile <> "XXX", indent & "OPEN " & typefile & " " & parameters, "")
            tagWrite statement
'            If arrFiles(i).io <> "XXX" Then
'              CurrentTag = "PCLOSE"
'              tagWrite Space(11) & "CLOSE " & arrFiles(i).Name & "."
'            End If
            arrFiles(i).io = "XXX"
            Exit For
          End If
        Next i
      End If
    Case Else
      statement = "#     *" & " OPEN " & token & " " & parameters
      tagWrite statement
  End Select
  
End Sub

Private Sub getCLOSE(parameters As String)
  'ASSESSMENT...
  Dim statement As String, token As String
  Dim param As String
  Dim i As Integer
  Dim typefile As String
  
  
  token = nextToken_newnew(parameters)
  Select Case token
    Case "FILE"
      parameters = Replace(Replace(parameters, ")", ""), "(", "")
      If InStr(parameters, ",") Then
        ' Loop per l'elenco di files
      Else
        For i = 1 To UBound(arrFiles)
          If InStr(parameters, arrFiles(i).Name) And arrFiles(i).io <> "XXX" Then
        statement = indent & "CLOSE " & parameters & "."
          End If
        Next
      End If
    Case Else
      statement = "#     *" & " CLOSE " & token & " " & parameters
  End Select
  If parameters <> "SYSPRINT" Then
    tagWrite statement
  End If
End Sub
'SILVIA
Private Sub getPERFORM(parameters As String)
  'AC  --------------------ATTENZIONE !!!!!! ------------
  ' prefisso gestito attraverso la funzione Prefix ed il secondo parametro(opzionale) a true
  ' nelle funzioni VarDepend e CheckParameter
  
  Dim statement As String, token As String, stperform As String
  Dim i As Integer, j As Integer, Z As Integer
  Dim spparameters() As String
    stperform = nextToken_newnew(parameters)
    stperform = Replace(stperform, "_", "-")
    'RAMO PERFORM
     For i = 0 To UBound(arrProc)
       If arrProc(i).Name = stperform Then
          If parameters <> "" Then
            If Not Right(parameters, 1) = ")" Then
              'abbiamo tirato su qualcosa di troppo
              ' togliamo ci� che sta a destra dell'ultima parentesi chiusa
              parameters = Left(parameters, InStrRev(parameters, ")"))
            End If
            parameters = Mid(parameters, 2)
            parameters = Left(parameters, Len(parameters) - 1)
            If Left(Trim(parameters), 6) = "SUBSTR" Then
              parameters = Replace(parameters, "SUBSTR", "")
              token = nextToken_newnew(parameters)
              token = getSUBSTR(token)
              If Trim(parameters) <> "" Then
               parameters = token & ", " & parameters
              Else
               parameters = token
              End If
            End If
            parameters = Replace(parameters, " END", " ")
            If InStr(parameters, ",") Then
               spparameters = Split(parameters, ",")
               For j = 0 To UBound(spparameters)
                 If InStr(spparameters(j), ".") > 0 Then
                    spparameters(j) = varDepend(spparameters(j), True)
                 ElseIf IsDataLocal(ProcIndex, spparameters(j)) Then
                    spparameters(j) = prefix(spparameters(j))  'local
                 Else
                    spparameters(j) = prefix(spparameters(j), 1) ' main
                 End If
                 'AC 23/12/10  gestione errato riconoscimento del numero di parametri
                 '(dichiarazione e chiamata non coincidono)
                 If UBound(arrProc(i).parameters) >= j Then
                 'AC l'indice dei prefissi delle dcl parte da 1 per il main, ArrProc(0) contiene la prima proc non main
                 ' per questo sommo 2 all'indice per allineare
                  tagWrite indent & "MOVE " & spparameters(j) & " TO " & prefix(arrProc(i).parameters(j), i + 2)
                 Else
                  writeLog "E", "Procedure Declaration and Procedure Call: parameters number mismatch!"
                  Exit For
                 End If
               Next
            Else
              If InStr(parameters, ".") > 0 Then
                 parameters = varDepend(parameters)
              ElseIf IsDataLocal(ProcIndex, parameters) Then
                 parameters = prefix(parameters) ' local
              Else
                 parameters = prefix(parameters, 1) ' main
              End If
              tagWrite indent & "MOVE " & parameters & " TO " & prefix(arrProc(i).parameters(0), i + 2)
            End If
          End If
          'tagWrite indent & "PERFORM " & stperform & IIf(Len(indent & "PERFORM " & stperform & " THRU " & stperform & "-EX") > 65, vbCrLf & indent & " THRU ", " THRU ") & stperform & "-EX"
          tagWrite indent & "PERFORM " & stperform '''& IIf(Len(indent & "PERFORM " & stperform & " THRU " & stperform & "-EX") > 65, vbCrLf & indent & " THRU ", " THRU ") & stperform & "-EX"
          Exit Sub
       End If
     Next
     
     'RAMO CALL  USING
     'Se non la trovo tra le proc provo tra le entri
     For i = 0 To UBound(entries)
       If entries(i).Name = stperform Then
        If Left(parameters, 1) = "(" And Right(parameters, 1) = ")" Then
         Dim parametri() As String
         parametri = GetParametersFromString(parameters)
         parameters = ""
         For Z = 0 To UBound(parametri)
           If Not parametri(Z) = "" Then
             parametri(Z) = CheckParameter(parametri(Z))
             parameters = parameters & " " & parametri(Z)
           End If
         Next
        End If
        statement = indent & "CALL  " & stperform & IIf(parameters = "", "", " USING" & parameters)
        tagWrite statement
        Exit Sub
      End If
     Next
     
     'DEFAULT -> perform senza che siano noti i nomi dei parametri su cui fare move
     If parameters <> "" Then
      If Not Right(parameters, 1) = ")" Then
        'abbiamo tirato su qualcosa di troppo
        ' togliamo ci� che sta a destra dell'ultima parentesi chiusa
        parameters = Left(parameters, InStrRev(parameters, ")"))
      End If
      parameters = Mid(parameters, 2)
      parameters = Left(parameters, Len(parameters) - 1)
      'token = nextToken_newnew(parameters)
      If Left(Trim(parameters), 6) = "SUBSTR" Then
         parameters = Replace(parameters, "SUBSTR", "")
         token = nextToken_newnew(parameters)
         token = getSUBSTR(token)
         If Trim(parameters) <> "" Then
          parameters = token & ", " & parameters
         Else
          parameters = token
         End If
      End If
            
       parameters = Replace(parameters, " END", " ")
       If InStr(parameters, ",") Then
          spparameters = Split(parameters, ",")
          For j = 0 To UBound(spparameters)
            If InStr(spparameters(j), ".") > 0 Then
               spparameters(j) = varDepend(spparameters(j), True)
            Else
               spparameters(j) = prefix(spparameters(j))
            End If
            tagWrite "#     * MOVE " & spparameters(j) & " TO the name of original proc parameter (is unknown)"
          Next
       Else
        If InStr(parameters, ".") > 0 Then
           parameters = varDepend(parameters)
        End If
        tagWrite "#     * MOVE " & parameters & " TO the name of original proc parameter (is unknown)"
      End If
    End If
    
    tagWrite indent & "PERFORM " & stperform '''& IIf(Len(indent & "PERFORM " & stperform & " THRU " & stperform & "-EX") > 65, vbCrLf & indent & " THRU ", " THRU ") & stperform & "-EX"
    Exit Sub
End Sub
Private Function CheckParameter(parametro As String, Optional IsPrefix As Boolean = False) As String
 If Left(parametro, 6) = "SUBSTR" Then
  CheckParameter = getSUBSTR(Mid(parametro, 7))
 Else
  CheckParameter = parametro
 End If
 If IsPrefix Then
  parametro = prefix(parametro)
 End If
End Function
Private Function GetParametersFromString(parameters As String, Optional IsToCheckLogic As Boolean = False) As String()
parameters = Trim(parameters)
If Left(parameters, 1) = "(" And Right(parameters, 1) = ")" Then
 parameters = Mid(parameters, 2, Len(parameters) - 2)
 Dim parametri() As String, appoparam() As String, k As Integer
 ' se parametri separati da virgola splitto
 ' poi controllo che virgole non fossero all'interno di parentesi e ricostruisco parametro
 ' ATTENZIONE
 ' Funziona se parametri elaborati da funzioni con UN SOLO livello di parentesi
 'Per ora � gestita solo SUBSTR
 
 If InStr(parameters, ",") > 0 Then ' ho pi� parametri separati da virgola
   parametri = Split(parameters, ",")
 Else
   ReDim parametri(0)
   parametri(0) = parameters
 End If
 If UBound(parametri) > 0 Then
   i = 0
   While i <= UBound(parametri)
     If InStr(parametri(i), "(") > 0 And Not InStr(parametri(i), ")") > 0 Then ' controllo che non siano stati splittati campi di una funzione (un solo livello di parentesi)
       k = i + 1
       While Not InStr(parametri(k), ")") > 0 And Not k > UBound(parametri)
        parametri(i) = parametri(i) & "," & parametri(k)
        parametri(k) = ""
        k = k + 1
       Wend
       If InStr(parametri(k), ")") > 0 Then
         parametri(i) = parametri(i) & "," & parametri(k)
         parametri(k) = ""
         i = k
       Else
         'segnalazione
       End If
     ' se parametri concatenati da operatori logici devono essere raggruppati in un unico parametro
     ElseIf IsToCheckLogic Then
      If parametri(i) = ">" Or parametri(i) = "<" Or parametri(i) = ">=" Or parametri(i) = "<=" Then
        If i - 1 >= 0 Then
          parametri(i - 1) = parametri(i - 1) & " " & parametri(i)
          If i + 1 <= UBound(parametri) Then
            parametri(i - 1) = parametri(i - 1) & " " & parametri(i + 1)
          End If
        End If
        i = i + 1
      End If
     End If
     
     i = i + 1
   Wend
 End If
 If IsToCheckLogic Then ' altro giro per operatori logici
  i = 0
  While i <= UBound(parametri)
    If parametri(i) = "&" Or parametri(i) = "�" Or parametri(i) = "|" Then
      If i - 1 >= 0 Then
          parametri(i - 1) = parametri(i - 1) & " " & parametri(i)
          If i + 1 <= UBound(parametri) Then
            parametri(i - 1) = parametri(i - 1) & " " & parametri(i + 1)
          End If
        End If
        i = i + 1
    End If
    i = i + 1
  Wend
 End If
 parameters = ""
 ReDim appoparam(0)
 For i = 0 To UBound(parametri)
   If Not parametri(i) = "" Then
     parametri(i) = CheckParameter(parametri(i))
     ReDim Preserve appoparam(UBound(appoparam) + 1)
     appoparam(UBound(appoparam)) = parametri(i)
   End If
 Next
End If
GetParametersFromString = appoparam
End Function

'SILVIA
Private Sub getSELECT(parameters As String)
  Dim statement As String, token As String, param As String
  param = IIf(Left(parameters, 1) = "(", Mid(Trim(parameters), 2, Len(parameters) - 2), parameters)
  token = nextToken_newnew(param)
  If token = "SUBSTR" Then
     token = nextToken_newnew(param)
     token = getSUBSTR(token)
     ''param = token & "," & param
  End If
  token = Replace(token, "_", "-")
  If Left(token, 1) <> "'" And InStr(token, ".") Then
    token = varDepend(token)
  End If
  ContEnd = ContEnd + 1
  ReDim Preserve ArrEnd(ContEnd)
  ArrEnd(ContEnd) = "END-EVALUATE"
  If IsDataLocal(ProcIndex, token) Then
    statement = indent & "EVALUATE " & prefix(token)
  Else
    statement = indent & "EVALUATE " & prefix(token, 1)
  End If
  token = nextToken_newnew(param)
  'indent = indent & "   "
  tagWrite statement
End Sub
'SILVIA
Private Function getWHEN(parameters As String) As String
  Dim statement As String, token As String, param As String, IsTerminated As Boolean
  Dim instruction As String
  
  Dim cases() As String
  ReDim cases(0)
  
  parameters = Replace(parameters, "_", "-")
  '____________________________________
  If Right(parameters, 2) = "DO" Then
    parameters = Left(parameters, Len(parameters) - 2)
    IsTerminated = True
  ElseIf Right(parameters, 3) = "DO;" Then
    parameters = Left(parameters, Len(parameters) - 3)
    IsTerminated = True
  Else
    ' lasciamo cos� per ora
  End If
  If Not Left(parameters, 1) = "(" And Not Right(parameters, 1) = ")" Then
    parameters = "(" & parameters & ")"
  ElseIf Left(parameters, 1) = "(" And InStr(parameters, ")") > 0 Then
    instruction = Trim(Right(parameters, Len(parameters) - InStrRev(parameters, ")")))
    parameters = Left(parameters, InStrRev(parameters, ")"))
  End If
  ReDim cases(0)
  cases = GetParametersFromString(parameters, True)
  '_______________________________
  statement = ""
  
  For i = 0 To UBound(cases)
    If Not cases(i) = "" Then
        statement = statement & " " & vbCrLf & indent & "WHEN " & cases(i)
    End If
  Next i
  tagWrite statement

 If IsTerminated Then
  ContWhen = ContWhen + 1
 End If
 If parameters = "" And Not instruction = "" Then
  parameters = instruction
 End If
 getWHEN = IIf(IsTerminated, "", parameters)
End Function

Private Function getDO(parameters As String) As String
  Dim statement As String, token As String
  Dim stvarying As String, stfrom As String, stparam() As String, stwhile As String
  Dim lenstring As Long

  If InStr(parameters, " TO ") Then
    stparam = Split(parameters, " TO ")
    stvarying = stparam(0)
    stfrom = stparam(1)
    If InStr(stfrom, " WHILE") = 0 Then
      statement = " PERFORM VARYING " & Replace(stvarying, "=", "FROM") & " BY 1 UNTIL " & Mid(stvarying, 1, InStr(stvarying, "=") + 1) & " " & stfrom
      If Len(statement) > 60 Then
        statement = Replace(statement, " UNTIL ", vbCrLf & indent & " UNTIL ")
      End If
      ContEnd = ContEnd + 1
      ReDim Preserve ArrEnd(ContEnd)
      ArrEnd(ContEnd) = "END-PERFORM"
      getDO = ""
    Else
      stwhile = Mid(stfrom, InStr(stfrom, "WHILE"))
      ''stwhile = "WHILE"
      stfrom = Trim(Mid(stfrom, InStr(stfrom, "WHILE") + 5))
      stfrom = IIf(Left(stfrom, 1) = "(", Mid(stfrom, 2), stfrom)
      stfrom = IIf(Right(stfrom, 1) = ")", Left(stfrom, Len(stfrom) - 1), stfrom)
      Do While stfrom <> ""
        token = nexttoken(stfrom)
        If InStr(token, ".") Then
          token = varDepend(token)
        End If
        stwhile = stwhile & " " & token
      Loop
      stwhile = Replace(stwhile, "WHILE", "OR ")
      stwhile = Replace(stwhile, " �", " NOT ")
      stwhile = Replace(stwhile, " | ", " OR ")
      stwhile = Replace(stwhile, "-", "-")
      statement = " PERFORM VARYING " & Replace(stvarying, "=", "FROM") & " BY 1 UNTIL " & stvarying & " " & " " & stwhile
      If Len(statement) > 60 Then
        statement = Replace(statement, " AND ", vbCrLf & indent & " AND ")
        statement = Replace(statement, " OR ", vbCrLf & indent & " OR ")
        statement = Replace(statement, " NOT ", vbCrLf & indent & " NOT ")
      End If
      ContEnd = ContEnd + 1
      ReDim Preserve ArrEnd(ContEnd)
      ArrEnd(ContEnd) = "END-PERFORM"
      getDO = ""
    End If
    'tagWrite indent & "   " & statement
  Else
    token = nextToken_newnew(parameters)
    Select Case UCase(token)
      Case "WHILE"
        'token = nextToken_newnew(parameters)
        statement = "PERFORM UNTIL NOT "
        parameters = Replace(parameters, " & ", " AND ")
        parameters = Replace(parameters, " | ", " OR ")
        parameters = Replace(parameters, " �", " NOT ")
        parameters = Replace(parameters, "_", "-")
        parameters = Replace(parameters, "'B", "'")
        statement = statement & " " & parameters
        If Len(statement) > 60 Then
          statement = Replace(statement, " AND ", vbCrLf & indent & " AND ")
          statement = Replace(statement, " OR ", vbCrLf & indent & " OR ")
          statement = Replace(statement, " NOT ", vbCrLf & indent & " NOT ")
        End If
        ContEnd = ContEnd + 1
        ReDim Preserve ArrEnd(ContEnd)
        ArrEnd(ContEnd) = "END-PERFORM"
        getDO = ""
      Case "UNTIL"
        statement = "PERFORM UNTIL "
        parameters = Replace(parameters, " & ", " AND ")
        parameters = Replace(parameters, " | ", " OR ")
        parameters = Replace(parameters, " �", " NOT ")
        parameters = Replace(parameters, "_", "-")
        parameters = Replace(parameters, "'B", "'")
        statement = statement & " " & parameters
        If Len(statement) > 60 Then
          statement = Replace(statement, " AND ", vbCrLf & indent & " AND ")
          statement = Replace(statement, " OR ", vbCrLf & indent & " OR ")
          statement = Replace(statement, " NOT ", vbCrLf & indent & " NOT ")
        End If
        ContEnd = ContEnd + 1
        ReDim Preserve ArrEnd(ContEnd)
        ArrEnd(ContEnd) = "END-PERFORM"
        getDO = ""
      Case ""
        getDO = ""
    Case "THEN"
        getDO = "THEN " & parameters & ";"
      Case Else
        statement = "#     *" & " DO " & token & " " & parameters
        getDO = ""
     End Select
  End If
  
  If statement <> "" Then
    tagWrite indent & statement
    indent = indent & "  "
  End If
End Function

Private Sub getREAD(parameters As String)
  'ASSESSMENT...
  Dim statement As String, token As String
  Dim param As String, fileName As String, recname As String, stkey As String
  token = nextToken_newnew(parameters)
  Select Case token
    Case "FILE"
      fileName = Replace(nextToken_newnew(parameters), "_", "-")
      fileName = Replace(Replace(fileName, ")", ""), "(", "")
      fileName = CheckReservedCBL(fileName)
      token = nextToken_newnew(parameters)
      Select Case token
        Case "INTO"
          If InStr(parameters, "KEY") Then
            recname = nextToken_newnew(parameters)
            recname = Replace(Replace(recname, ")", ""), "(", "")
            parameters = Replace(parameters, "KEY(", "")
            stkey = Replace(Replace(parameters, ")", ""), "(", "")
            Oncondition = getFDefinition(fileName, recname, stkey)
            statement = indent & "READ " & fileName & " INTO " & recname & vbCrLf
            statement = statement & indent & "   INVALID KEY "
          Else
            recname = Replace(Replace(parameters, ")", ""), "(", "")
            recname = Replace(recname, "_", "-")
            recname = CheckReservedCBL(recname)
            Oncondition = getFDefinition(fileName, recname)
            statement = indent & "READ " & fileName & " INTO " & recname ''& vbCrLf
          End If
        Case Else
          statement = "#     *" & " READ " & fileName & " " & token & " " & parameters
          Oncondition = getFDefinition(fileName, recname, stkey)
      End Select
    Case Else
      statement = "#     *" & " READ " & token & " " & parameters
  End Select
  CurrentTag = "PROCEDURE-DIVISION"
  tagWrite statement
End Sub

Private Sub getWRITE(stwrite As String, parameters As String)
  'ASSESSMENT...
  Dim statement As String, token As String
  Dim param As String, fileName As String
  
  token = nextToken_newnew(parameters)
  Select Case token
    Case "FILE"
      fileName = Replace(nextToken_newnew(parameters), "_", "-")
      fileName = Replace(Replace(fileName, ")", ""), "(", "")
      fileName = Trim(CheckReservedCBL(fileName))
      token = nextToken_newnew(parameters)
      Select Case token
        Case "FROM"
          parameters = Replace(Replace(parameters, ")", ""), "(", "")
          parameters = Replace(parameters, "_", "-")
          getFDefinition fileName, parameters, , "OUTPUT"
          CurrentTag = "PROCEDURE-DIVISION"
          statement = indent & stwrite & " " & fileName & "-REC FROM " & parameters
        Case Else
      End Select
    Case Else
      statement = "#     *" & stwrite & " " & token & " " & parameters
  End Select
  tagWrite statement
End Sub

''''''''''''''''''''''''''''''''''''''''''''''
' END: fine programma
''''''''''''''''''''''''''''''''''''''''''''''
Private Function getEND(statement As String) As String
  On Error GoTo catch
  
  Select Case statement
    Case GbNomePgm
      statement = indent & "GOBACK."
      statement = "      *" & vbCrLf & statement
      getEND = ""
    Case "ELSE", "ELSE DO", "END"
      getEND = statement & ";"
      Exit Function
    Case ""
      If ContWhen > 0 Then
        ContWhen = ContWhen - 1
        Exit Function
      End If
      If ContEnd > 0 Then
         indent = IIf(Len(indent) > 11, Space(Len(indent) - 2), Space(11))
         statement = indent & ArrEnd(ContEnd)
         tagWrite statement
         ContEnd = ContEnd - 1
         ReDim Preserve ArrEnd(ContEnd)
         getEND = ""
      End If
    Case Else
      If ContWhen > 0 Then
        ContWhen = ContWhen - 1
        Exit Function
      End If
      If ContEnd > 0 Then
        indent = IIf(Len(indent) > 11, Space(Len(indent) - 2), Space(11))
        'statement = indent & ArrEnd(ContEnd) & vbCrLf
        tagWrite indent & ArrEnd(ContEnd)
        ContEnd = ContEnd - 1
        ReDim Preserve ArrEnd(ContEnd)
        'getEND = ""
      End If
      
      If InStr(statement, " ") Then
        getEND = statement & ";"
        Exit Function
      '    End If
      '  End If
      Else
       ' indent = Space(11)
       ' statement = indent & "." ''& vbCrLf & Space(7) & Replace(statement, "_", "-") & "-EX." & vbCrLf
        'statement = statement & indent & "EXIT."
        'tagWrite statement
        getEND = ""
       'statement = indent & statement
      End If
  End Select
  Exit Function
catch:
  writeLog "E", "Unexpecte Error: " & err.description
End Function

Private Function getIF(parameters As String) As String
  Dim token As String, newtoken As String, statement As String, sverify As String
  
  On Error GoTo catch
  statement = "IF"
  token = nextToken_newnew(parameters)
  If token = "VERIFY" Then
   statement = statement & " " & getVerify(sverify, Left(parameters, InStr(parameters, ")")))
   parameters = Mid(parameters, InStr(parameters, ")") + 1)
   token = ""
   token = nextToken_newnew(parameters)
  End If
  If token = "SUBSTR" Then
    token = nextToken_newnew(parameters)
    token = getSUBSTR(token)
  End If

  If Left(token, 1) = "(" And Right(token, 1) = ")" Then
    token = Mid(token, 2)
    newtoken = nextToken_newnew(token)
    statement = statement & " ( "
    Do Until newtoken = ")" Or token = ""
      newtoken = Replace(newtoken, "_", "-")
      If InStr(newtoken, ".") Then
        newtoken = varDepend(newtoken)
      End If
      If Left(newtoken, 1) = "'" And Len(newtoken) > 2 And Trim(Mid(newtoken, 2)) = "'" Then
        token = "SPACES"
      End If
      If newtoken = "SUBSTR" Then
        newtoken = getSUBSTR(token)
        token = ""
      End If
      statement = statement & " " & newtoken
      newtoken = nextToken_newnew(token)
    Loop
    statement = statement & " " & newtoken
    token = nextToken_newnew(parameters)
  '  statement = statement & " ) "
  End If
  Do Until token = "THEN" Or parameters = ""
    If Left(token, 1) = "(" And Right(token, 1) = ")" Then
      token = Mid(token, 2)
      newtoken = nextToken_newnew(token)
      statement = statement & " ( "
      Do Until newtoken = ")" Or token = ""
        newtoken = Replace(newtoken, "_", "-")
        If InStr(newtoken, ".") Then
          newtoken = varDepend(newtoken)
        End If
        If Left(newtoken, 1) = "'" And Len(newtoken) > 2 And Trim(Mid(newtoken, 2)) = "'" Then
          token = "SPACES"
        End If
        If newtoken = "SUBSTR" Then
          newtoken = getSUBSTR(token)
          token = ""
        End If
        statement = statement & " " & newtoken
        newtoken = nextToken_newnew(token)
      Loop
      If Right(newtoken, 1) = ")" Then
        newtoken = Left(newtoken, Len(newtoken) - 1)
        newtoken = Replace(newtoken, "_", "-")
        If InStr(newtoken, ".") Then
          newtoken = varDepend(newtoken)
        End If
        newtoken = newtoken & ")"
      End If
      statement = statement & " " & newtoken
      token = nextToken_newnew(parameters)
    End If
    If token = "THEN" Then Exit Do
    token = Replace(token, "_", "-")
    If InStr(token, ".") Then
      token = varDepend(token)
    ElseIf IsDataLocal(ProcIndex, token) Then
      token = prefix(token)
    'Else
      'token = prefix(token, 1) ' main
    End If
    If Left(token, 1) = "'" And Len(token) > 2 And Trim(Mid(token, 2)) = "'" Then
      token = "SPACES"
    End If
    statement = statement & " " & token
    token = nextToken_newnew(parameters)
    If token = "VERIFY" Then
      statement = statement & " " & getVerify(sverify, Left(parameters, InStr(parameters, ")")))
      parameters = Mid(parameters, InStr(parameters, ")") + 1)
      token = ""
      token = nextToken_newnew(parameters)
    End If
  Loop
  statement = Replace(statement, " �", " NOT ")
  statement = Replace(statement, " | ", " OR ")
  statement = Replace(statement, " & ", " AND ")
  statement = indent & statement ''& " THEN"
  If Len(statement) > 60 Then
    statement = Replace(statement, " AND ", vbCrLf & indent & " AND ")
    statement = Replace(statement, " OR ", vbCrLf & indent & " OR ")
    '''statement = Mid(statement, 1, InStr(50, statement, " ")) & vbCrLf & indent & "   " & Mid(statement, InStr(50, statement, " "))
  End If
  indent = indent & "   "
  'If Len(parameters) Then
  getIF = indent & parameters   ''' & ";"
  'End If
  ContEnd = ContEnd + 1
  ReDim Preserve ArrEnd(ContEnd)
  ArrEnd(ContEnd) = "END-IF"
  tagWrite statement

  Exit Function
catch:
  writeLog "E", "Unexpected Error: " & err.description
End Function
'T
Private Function getVerify(param As String, statement As String) As String
  Dim token As String
  On Error GoTo catch
  
  statement = Replace(Replace(statement, "(", ""), ")", "")
  CurrentTag = "WORKING-STORAGE"
  
  If InStr(tagValues("WORKING-STORAGE"), "COPY VERIFYW") = 0 Then
    tagWrite Space(11) & "COPY VERIFYW."
  End If
  
  token = nextToken_newnew(statement)
  If InStr(token, ".") Then
    token = varDepend(token)
  End If

  If Len(param) = 0 Then
    param = token & "-POS"
    tagWrite Space(7) & "01 " & param & " PIC 9(4) COMP."
  End If

  CurrentTag = "PROCEDURE-DIVISION"
  tagWrite indent & "INITIALIZE AREA-VERIFY"
  
  tagWrite indent & "MOVE " & token & " TO  REV-PAR-X"
  tagWrite indent & "MOVE LENGTH OF " & token & " TO  REV-PAR-X-L"
  
  token = nextToken_newnew(statement)
  tagWrite indent & "MOVE " & token & " TO  REV-PAR-Y"
  tagWrite indent & "MOVE LENGTH OF " & token & " TO  REV-PAR-Y-L"
             
  tagWrite indent & "PERFORM VERIFY THRU END-VERIFY"
  'If Len(param) Then
    tagWrite indent & "MOVE REV-POS TO " & param
  'Else
  '  param = "REV-POS"
  'End If
  getVerify = param
  If InStr(tagValues("COPY-P"), "COPY VERIFYP") = 0 Then
    CurrentTag = "COPY-P"
    tagWrite "COPY VERIFYP."
    CurrentTag = "PROCEDURE-DIVISION"
  End If
  param = ""
  Exit Function
catch:
  If err.Number = 5 Then '424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    Resume
  End If
End Function
'silvia
Private Sub getCompute(destination As String, source As String)
  Dim stcompute() As String, token As String, stsource As String, stappo As String, lenstring As Long
  destination = Replace(destination, "_", "-")
  'SILVIA 18-12-2008 GESTIONE "SUBSTR"
  If InStr(destination, "SUBSTR") Then
    destination = getSUBSTR(Replace(destination, "SUBSTR", ""))
  End If
  If Mid(destination, 1, 1) = "(" Then
    destination = Mid(destination, 2, Len(destination) - 2)
  End If
  
  If Left(destination, 1) <> "'" And InStr(destination, ".") Then
    destination = varDepend(destination)
  End If
  source = Replace(source, "_", "-")
  token = nextToken_newnew(source)
  While Not token = ""
    If token = "SUBSTR" Then
      token = nextToken_newnew(source)
      token = getSUBSTR(token)
    End If
    If Left(token, 1) <> "'" And InStr(token, ".") Then
      token = varDepend(token)
    End If
    stsource = stsource & " " & token
    token = nextToken_newnew(source)
  Wend
  lenstring = Len(indent & stsource)
  If lenstring > 60 Then
    stsource = Replace(stsource, " + ", " + " & vbCrLf & indent)
    stsource = Replace(stsource, " - ", " - " & vbCrLf & indent)
  End If
  tagWrite indent & "COMPUTE " & destination & IIf(Len(indent & "COMPUTE " & destination & " = " & stsource) > 65, " = " & vbCrLf & indent, " = ") & stsource
End Sub

Private Sub getString(destination As String, source As String)
  Dim ststring() As String
  Dim i As Integer, j As Integer
  Dim lenstring As Long
  Dim ststringw As String, stfiller As String, stfiller1 As String
  Dim splitReplace() As String, splitSource() As String
  
  destination = Replace(destination, "_", "-")
  If Left(destination, 1) <> "'" And InStr(destination, ".") Then
    If InStr(destination, "SUBSTR") Then
      destination = getSUBSTR(Replace(destination, "SUBSTR", ""))
    End If
    destination = varDepend(destination)
  End If
  If InStr(source, "|| (") Then
  
  End If
  ststring = Split(source, "||")
  source = ""
  For i = 0 To UBound(ststring)
    If Left(ststring(i), 1) <> "'" Then
      ststring(i) = Replace(ststring(i), "_", "-")
      If InStr(ststring(i), "SUBSTR") Then
        ststring(i) = getSUBSTR(Replace(ststring(i), "SUBSTR", ""))
      End If
      If InStr(ststring(i), ".") And InStr(ststring(i), "'") = 0 Then
        ststring(i) = varDepend(ststring(i))
      End If
    End If
    If Left(Trim(ststring(i)), 1) = "(" And InStr(Trim(ststring(i)), ")") Then
      If IsNumeric(Mid(Trim(ststring(i)), 2, InStr(Trim(ststring(i)), ")") - 2)) Then
        stfiller = String(Mid(Trim(ststring(i)), 2, InStr(Trim(ststring(i)), ")") - 2), IIf(Trim(Mid(ststring(i), InStr(Trim(ststring(i)), ")") + 2)) = "' '", Space(1), Replace(Trim(Mid(ststring(i), InStr(Trim(ststring(i)), ")") + 2)), "'", "")))
        ststring(i) = "'" & stfiller & "'"
      End If
    ElseIf InStr(ststring(i), "REPEAT") > 0 Then
      stfiller = Replace(Replace(Replace(Replace(ststring(i), "'", ""), "(", ""), ")", ""), "REPEAT", "")
      splitReplace() = Split(stfiller, ",")
      stfiller = ""
      stfiller1 = ""
      If Len(Trim(splitReplace(0))) < 2 Then
        stfiller = String(CLng(splitReplace(1)), splitReplace(0))
      Else
        For j = 1 To CInt(Trim(splitReplace(1)))
          If UBound(Split(stfiller, vbCrLf)) <= 0 Then
             lenstring = Len(stfiller)
             stfiller = stfiller & IIf(lenstring + Len(indent) > 58, "'" & vbCrLf & indent & "'" & Trim(splitReplace(0)), Trim(splitReplace(0)))
          Else
             stfiller1 = stfiller1 & Trim(splitReplace(0))
             lenstring = Len(stfiller1) + Len(indent)
             If lenstring > 58 Then
               stfiller1 = ""
             End If
             stfiller = stfiller & IIf(lenstring > 58, "'" & vbCrLf & indent & "'" & Trim(splitReplace(0)), Trim(splitReplace(0)))
          End If
         ''  stfiller = stfiller & Trim(splitReplace(0))
        Next
      End If
      ststring(i) = "'" & stfiller & "'"
    End If
    source = source & " " & ststring(i)
    If UBound(Split(source, vbCrLf)) = 0 Then
      If Len(source) > 50 Then
        source = source & vbCrLf & indent & "        "
        lenstring = Len(source)
      End If
    Else
      If (Len(source) - lenstring) > 50 Then
        source = source & vbCrLf & indent & "        "
        lenstring = Len(source)
      End If
    End If
  Next i
  ststringw = indent & "STRING " & source & vbCrLf & indent & "DELIMITED BY SPACE INTO " & vbCrLf & indent & destination
  tagWrite ststringw
End Sub

Private Function IsBuiltFunction(source As String) As Boolean
  Dim rs As Recordset
  
  Set rs = m_Fun.Open_Recordset("select BIFuncName from PsPLI_Builtinfunctions where BIFuncName ='" & UCase(source) & "'")
  If Not rs.EOF Then
    IsBuiltFunction = True
  End If
  rs.Close
End Function

Private Function IsEntry(source As String) As Boolean
  Dim i As Integer
  
  For i = 0 To UBound(entries)
    If entries(i).Name = source Then
      Exit For
    End If
  Next i
  If i <= UBound(entries) Then
    IsEntry = True
  End If
End Function

Function IsDataLocal(Index As Integer, var As String) As Boolean
  Dim rs As Recordset
  
  If Index = 1 Then
    IsDataLocal = False
  Else
    If UBound(arrProc) >= Index - 2 Then
      Set rs = m_Fun.Open_Recordset("select Nome from PsData_Cmp where " & _
                                    "proc = '" & Replace(arrProc(Index - 2).Name, "-", "_") & "' and nome = '" & _
                                    Replace(Replace(var, "'", "''"), "-", "_") & "'")
      If Not rs.EOF Then
        IsDataLocal = True
      End If
      rs.Close
    End If
  End If
End Function

Private Sub getMove(destination As String, source As String)
  Dim stfiller As String, parametri() As String, token As String, appoSource As String
  
  'AC   -------------  ATTENZIONE PREGO !!!!!!!! -----------
  ' il prefisso � gestito tramite la finzione prefix e l'aggiunto del parametro TRUE
  ' nella chiamata alla funzione VarDepend  (che di default ha quel booleano a false)
  ' prefix con il parametro opzionale = 1 forza il prefisso a P1 (main) diversamente
  ' il prefisso � costruito con l'indice della proc che contiene la variabile locale
  ' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  CurrentTag = "PROCEDURE-DIVISION"
  destination = Replace(destination, "_", "-")
  'SILVIA 18-12-2008 GESTIONE "SUBSTR"
  
  If InStr(destination, "SUBSTR") Then
    destination = getSUBSTR(Replace(destination, "SUBSTR", ""))
  End If
  If Mid(destination, 1, 1) = "(" Then
    destination = Mid(destination, 2, Len(destination) - 2)
  End If
  If Left(destination, 1) <> "'" And InStr(destination, ".") Then
    destination = varDepend(destination, True)
  ElseIf IsDataLocal(ProcIndex, destination) Then
    destination = prefix(destination)    'local
  Else
    destination = prefix(destination, 1) 'MAIN
  End If
  source = Replace(source, "'1'B", "1")
  source = Replace(source, "'0'B", "0")
  source = Replace(source, vbCrLf, "")
  source = Replace(Trim(source), "''", "SPACES")
  
  'AC 10/01/2011
  'controlliamo se source � risultato di chimata a funzione ed isoliamo i parametri
  appoSource = source
  If source <> "" Then
    token = nexttoken(source)
    'ricostruiamo
    If token = "HIGH" Then
      source = "HIGH-VALUE"
    ElseIf Left(Trim(source), 1) = "(" And Right(Trim(source), 1) = ")" Then
      Dim funz As String
      parametri = GetParametersFromString(source)
      funz = token
      If Not IsEntry(funz) Then
        'ricostruiamo
        funz = ""
        source = appoSource
      End If
      If IsBuiltFunction(token) Then
        source = token & source
      End If
    Else
      'ricostruiamo
      source = appoSource
    End If
  
    If Left(source, 1) <> "'" Then
      source = Replace(source, "_", "-")
      If InStr(source, "SUBSTR") Then
         source = getSUBSTR(Replace(source, "SUBSTR", ""))
      End If
      'AC aggiunta gestione prefisso
      If InStr(source, ".") Then
        source = varDepend(source, True)
      ElseIf IsDataLocal(ProcIndex, source) Then
        source = prefix(source)
      Else
        source = prefix(source, 1)
      End If
      If InStr(source, "BY NAME") Or InStr(source, "BYNAME") Then
        source = Trim(Replace(Replace(source, "BY NAME", ""), "BYNAME", ""))
        If Right(source, 1) = "," Then
          source = Left(source, Len(source) - 1)
        End If
        source = "CORR " & source
      End If
    End If
  
    destination = IIf(InStr(source, "'B "), "SW-" & destination, destination)
    'GLORIA: se � una move per l'output di una proc potrebbero rimanere le parentesi a inizio e fine
    If Left(Trim(source), 1) = "(" And Right(Trim(source), 1) = ")" And InStr(2, Trim(source), "(") = 0 Then
      source = Trim(Replace(Replace(source, "(", " "), ")", " "))
    End If
  
    If Left(source, 1) = "(" And InStr(source, ")") > 0 Then
      If IsNumeric(Mid(Trim(source), 2, InStr(Trim(source), ")") - 2)) Then
        stfiller = String(Mid(source, 2, InStr(source, ")") - 2), IIf(Mid(source, InStr(source, ")") + 2) = "' '", Space(1), Replace(Mid(source, InStr(source, ")") + 2), "'", "")))
        source = "'" & stfiller & "'"
      End If
    End If
  End If
  
  If Trim(source) = "''" Then
    tagWrite indent & "INITIALIZE  " & destination
  ElseIf Left(source, 1) = "'" And Len(source) > 2 And Trim(Mid(source, 2)) = "'" Then
    tagWrite indent & "MOVE SPACES TO " & destination
  'AC 10/01/2011
  ElseIf Not funz = "" Then
    Dim CallMove As String
    CallMove = "CALL " & funz & " USING"
    For i = 1 To UBound(parametri)
      '__________________________________
      'AC gestiamo prefix per i parametri
      '----------------------------------
      If Left(parametri(i), 1) <> "'" And InStr(parametri(i), ".") Then
        parametri(i) = varDepend(parametri(i), True)
      ElseIf IsDataLocal(ProcIndex, parametri(i)) Then
        parametri(i) = prefix(parametri(i))
      Else
        parametri(i) = prefix(parametri(i), 1)
      End If
      
      CallMove = CallMove & " " & parametri(i)
    Next i
    
    tagWrite indent & CallMove & " RETURNING " & destination
  Else
    If Len(indent & "   MOVE " & source & " TO " & destination) > 60 Then
      tagWrite indent & "   MOVE " & source & vbCrLf & indent & " TO " & destination
    Else
      tagWrite indent & "   MOVE " & source & " TO " & destination
    End If
  End If
End Sub

Function prefix(source As String, Optional forcedprefix As Integer = -1) As String
  'AC  eliminare queste righe se si vuole prefisso anche per variabili dentro main
  If ProcIndex = 1 Or forcedprefix = 1 Then
    prefix = source
    Exit Function
  End If
  '---------- NO P1---------------------------------------
  
  If Len(source) >= 4 Then
    If Left(source, 4) = "RET-" Then
      prefix = source
      Exit Function
    End If
  End If
  If Not IsNumeric(source) And Not Left(source, 1) = "'" Then
    If forcedprefix = -1 Then
      prefix = "P" & ProcIndex & "-" & source
    Else
      prefix = "P" & forcedprefix & "-" & source
    End If
  Else
    prefix = source
  End If
End Function

Private Function getSUBSTR(FIELD As String) As String
  Dim token As String, numAperte As Integer, numChiuse As Integer
  
  numAperte = Len(FIELD) - Len(Replace(FIELD, "(", "", , , vbTextCompare))
  numChiuse = Len(FIELD) - Len(Replace(FIELD, ")", "", , , vbTextCompare))
  If Not numAperte = numChiuse Then
    getSUBSTR = "SUBSTR" & FIELD
    Exit Function
  End If
  FIELD = Replace(FIELD, "(", "")
  token = nextToken_newnew(FIELD)
  If Left(token, 1) <> "'" And InStr(token, ".") Then
    token = varDepend(token)
  End If
  If Not InStr(FIELD, ",") > 0 Then 'caso in cui manca lunghezza (deve diventare start: )
    FIELD = Left(FIELD, Len(FIELD) - 1) & "," & ")"
  End If
  getSUBSTR = token & "(" & Trim(Replace(FIELD, ",", ":"))
End Function

Private Sub getTRANSLATE(FIELD As String, stMove As String)
  Dim token As String
  Dim stfield As String
  ' INSPECT FIELD REPLACING ALL ' ' BY '0'
  
  stfield = nextToken_newnew(FIELD)
  If Left(stfield, 1) <> "'" And InStr(stfield, ".") Then
    stfield = varDepend(stfield)
  End If
  token = nextToken_newnew(FIELD)
  tagWrite indent & "INSPECT " & stfield & " REPLACING ALL " & Replace(FIELD, ")", "") & " BY " & token
  If stMove <> "" Then
    tagWrite indent & "MOVE " & stfield & " TO " & stMove
  End If
End Sub

Private Function varDepend(FIELD As String, Optional IsPrefix As Boolean = False) As String
  Dim punto As Integer
  Dim Parent As String

  punto = InStr(FIELD, ".")
  Parent = Left(FIELD, punto - 1)
  FIELD = Mid(FIELD, punto + 1)
  'AC
  varDepend = CheckReservedCBL(FIELD) & " OF " & CheckReservedCBL(Parent)
  If IsPrefix Then
    If IsDataLocal(ProcIndex, FIELD) Then
      varDepend = prefix(FIELD) & " OF " & prefix(Parent)      'local -> ProcIndex
    Else
      varDepend = prefix(FIELD, 1) & " OF " & prefix(Parent, 1) 'MAIN -> P1
    End If
  End If
End Function

Sub UpdateCampiMoved(NomeCampo As String, pIdOggetto As Long)
  Dim FIELD As String, area As String, rs As Recordset
  
  FIELD = Right(NomeCampo, Len(NomeCampo) - InStr(NomeCampo, "."))
  area = Left(NomeCampo, InStr(NomeCampo, ".") - 1)
  Set rs = m_Fun.Open_Recordset("Select IDArea from PsData_cmp where " & _
                                "(IdOggetto = " & pIdOggetto & " or IdOggetto in (Select IdOggettoR from PsRel_Obj where " & _
                                "IdOggettoC = " & pIdOggetto & " and (Relazione = 'CPY' or Relazione = 'INC'))) and Nome = '" & _
                                area & "' and ordinale = 1")
  If Not rs.EOF Then
    Parser.PsConnection.Execute "Update PsData_Cmp Set Moved = true where " & _
                                "Nome = '" & FIELD & "' and IdArea = " & rs!idArea & " and (IdOggetto = " & GbIdOggetto & " or IdOggetto in " & _
                                "(Select IdOggettoR from PsRel_Obj where IdOggettoC = " & _
                                GbIdOggetto & " and (Relazione = 'CPY' or Relazione = 'INC')))"
  End If
  rs.Close
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Parsing I livello
' ...
'  analizza DCL di tipo campo e riempie la lista DataItem (dimensionata su max 20000 elementi)
' Il campo Idx � il counter della lista (numero elementi validi)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parsePgm_I()
  Dim nFCbl As Long, i As Long, j As Long, k As Long
  Dim wRec As String, statement As String
  Dim token As String, line As String
  Dim manageMove As String  'Fare globale Parser
  Dim multilinea As Integer, label As Boolean, IsSubstr As Boolean, pStart As String
  ' Mauro 17/04/2008
  Dim is_IF As Boolean
  Dim strLabel As String
  On Error GoTo parseErr
  
  'Non leggiamo tutte le volte!
  manageMove = m_Fun.LeggiParam("IMSDC-MOVEYN") = "YES"
  
  resetRepository
  
  'Init flag per riconoscimento tipo programma... (flag da sistemare - perlomeno rinominare...)
  SwTpCX = False
  SwTpIMS = False
  SwDli = False
  SwSql = False
  SwBatch = False
  SwVsam = False
  SwMapping = False
  
  'INIT:
  booEliminaRiga = False
  GbNumRec = 0
  ReDim entries(100)
  entryIndex = 0
  ReDim builtins(100)
  builtinIndex = 0
  ReDim CmpIstruzione(0)
  ReDim CampiMove(0)
  ReDim CampiMoveCpy(0)
  'SQ 15-11-06
  ReDim DataValues(0)
  Idx = 0
  SwNomeCall = True
  ReDim arrFiles(0)
  'SQ [01]
  procName = ""
  
  ' Mauro 23/07/2008 : E se il file non c'�? Non usciva mai!!!
  If Len(Dir(GbFileInput)) = 0 Then
    MsgBox "This file is not avaliable...", vbExclamation, Parser.PsNomeProdotto
    Exit Sub
  End If

  nFCbl = FreeFile
  Open GbFileInput For Input As nFCbl
  While Not EOF(nFCbl)
    'SQ 10-02-06 (potenziale loop - es.: non e' un PLI!)
    While Len(line) = 0 And Not EOF(nFCbl)
      Line Input #nFCbl, line
      operator = ""
      GbNumRec = GbNumRec + 1
      RipulisciRiga line
    Wend
    'SQ 21-11-06 - GESTIONE DIRETTIVE:
    'BBV "-INC PSTAMP"... chiarire se sono direttive,
    'comunque non terminano col ";"!
    'TMP: FARE routine dedicata
    'AC 10/12/2007 gestito EXEC dentro IF,ELSE,WHEN come per CALL
    token = ""
    statement = ""
    Do While Not EOF(nFCbl)
      statement = line
      'SILVIA PROVIAMO CON IL nexttoten_newnew per tenere l'apice singolo
      ''token = nextToken(statement)
      token = nextToken_newnew(statement)
      If token = "INC" Or Len(line) = 0 Or _
         (token = "IF" And Not InStr(line, " CALL ") > 0 And Not InStr(line, " EXEC ") > 0) Or _
         (token = "ELSE" And Not InStr(line, " CALL ") > 0 And Not InStr(line, " EXEC ") > 0 And Not InStr(line, " = ") > 0) Or _
         (token = "THEN" And Not InStr(line, " CALL ") > 0 And Not InStr(line, " EXEC ") > 0) Or _
         (token = "WHEN" And Not InStr(line, " CALL ") > 0 And Not InStr(line, " EXEC ") > 0 And Not InStr(line, " = ") > 0) Or _
         (token = "OTHERWISE" And Not InStr(line, " CALL ") > 0 And Not InStr(line, " EXEC ") > 0 And Not InStr(line, " = ") > 0) Then
        Line Input #nFCbl, line
        GbNumRec = GbNumRec + 1
        RipulisciRiga line
      ElseIf token = "IF" Or token = "ELSE" Or token = "THEN" Or token = "WHEN" Or token = "OTHERWISE" Then
        ' sotto considerando i casi in cui call sulla stessa linea di questi identificatori
        RipulisciRiga line
        If InStr(line, " CALL ") > 0 Then
          line = Right(line, Len(line) - InStr(line, " CALL "))
        ElseIf InStr(line, " EXEC ") > 0 Then
          line = Right(line, Len(line) - InStr(line, " EXEC "))
        ' Mauro 19/10/2009 : Prendo i valori della MOVE dopo la parola riservata
        ElseIf InStr(line, " = ") > 0 And (token = "ELSE" Or token = "WHEN" Or token = "OTHERWISE") Then
          Dim firstSpace As Integer
          firstSpace = InStrRev(line, " ", InStr(line, " = ") - 2)
          line = Trim(Right(line, Len(line) - firstSpace))
        Else
          Line Input #nFCbl, line
          GbNumRec = GbNumRec + 1
          RipulisciRiga line
        End If
      Else
        Exit Do
      End If
    Loop
      
    statement = line
    GbOldNumRec = GbNumRec  'linea iniziale statement
    If label Then
      GbOldNumRec = GbOldNumRec - multilinea
    End If
        
    'stefano: caso di riga di commenti, quindi tutta pulita?????
    If line = "" Then
    'SQ 14-11-06: LABEL: "CICCIO:" ==> NON ho il ";"!
    ElseIf Right(line, 1) = ":" And InStr(Trim(Mid(line, 1, Len(line) - 1)), " ") = 0 Then
      'LABEL!
      'nop
    Else
      'spli
      'Do While Right(line, 1) <> ";" And Not EOF(nFCbl) And j = 1
      j = 0
      i = 1
      Do
        i = InStr(i, line, "'")
        If i = 0 Then Exit Do
        i = i + 1
        j = Switch(j = 0, 1, j = 1, 0)
      Loop
      multilinea = 0
      Do While (Right(line, 1) <> ";" Or j = 1) And Not EOF(nFCbl)
        Line Input #nFCbl, line
        GbNumRec = GbNumRec + 1
        multilinea = multilinea + 1
        RipulisciRiga line
        ' Mauro 22/04/2008
        If Right(line, 4) = "THEN" Or InStr(line, "THEN ") Then
          GbOldNumRec = GbNumRec
          'statement = LTrim(Mid(line, 5))
          statement = LTrim(Mid(line, InStr(line, "THEN") + 4))
        Else
          If statement = "" Then
            GbOldNumRec = GbNumRec
          End If
          statement = statement & " " & line
        End If
        
        i = 1
        Do
          i = InStr(i, line, "'")
          If i = 0 Then Exit Do
          i = i + 1
          j = Switch(j = 0, 1, j = 1, 0)
        Loop
      Loop
      'Pulizia ";"
      'split: non � colpa mia, doppio ";" in una decina di include...
      'If Right(statement, 1) = ";" Then statement = Left(statement, Len(statement) - 1)
      While Right(statement, 1) = ";"
        statement = RTrim(Left(statement, Len(statement) - 1))
      Wend
      'SILVIA PROVIAMO CON IL nexttoten_newnew per tenere l'apice singolo
      'token = nextToken(statement)
      token = nextToken_newnew(statement)
    End If
    'SQ - 14-11-06:
    'TMP: COLONNA 1: COS'E'?
    'If token = "0" Or token = "1" Or token = "-" Then
    '  token = nextToken(statement)
    'End If
    label = False
    
    Select Case token
      Case "SUBSTR"
        'If Left(line, 1) = "(" And Right(line, 1) = ")" Then
        If Left(line, 6) = "SUBSTR" Then
          IsSubstr = True
          token = nexttoken(line)
        Else
          line = ""
        End If
      Case "DCL", "DECLARE"
        ' Mauro 10/12/2007 : mi serve per gestire i campi di gruppo
        oldLevel = 1
        getDCL statement
        line = ""
      Case "%"
        token = nexttoken(statement)
        If token = "INCLUDE" Then
          getInclude statement
        Else
          '...
        End If
        line = ""
      Case "%INCLUDE"
        getInclude statement
        'ATTENZIONE A CASI CON type(...)... vedi sintassi IBM
        line = ""
      Case "EXEC"
        'GbOldNumRec = GbNumRec
        token = nexttoken(statement)
        Select Case token
          Case "DLI"
            'tmp:
            MapsdM_COBOL.getEXEC_DLI statement
          Case "SQL"
            'tmp:
            MapsdM_COBOL.getEXEC_SQL statement
          Case "CICS"
            'tmp:
            MapsdM_COBOL.getEXEC_CICS statement
        End Select
        line = ""
      Case "CALL"
        getCALL statement
        line = ""
      Case "WHEN"
        'Buttiamo via la condizione e teniamo lo statement!
        token = nexttoken(statement)
        line = statement & ";"  'Ritorna in circolo!
      'SQ 27-03-07 (gestione "completa" label... (vedi sotto ex gestione PROC...)
      'Perdeva istruzioni tipo: LABEL1: CALL ...
      Case "PROC", "PROCEDURE"
        parsePROC statement, strLabel
        line = ""
        strLabel = ""
      Case "END"
        If procName = statement Then
          procName = GbNomePgm
        End If
        line = ""
        statement = ""
      Case Else
        'stefano:  tanto per non dare errore: da rivedere, perch� cos� non d� pi� errore in PPP3
        If line = "" Then
          If IsNumeric(token) And InStr(token, "(") = 0 And InStr(token, ")") = 0 And UCase(Mid(statement, 1, 5)) <> "ELSE " Then
           'silvia: imposto un oldlevel al massimo
            m_Fun.writeLog "parsePgm_I: INCLUDE#" & GbNomePgm & ", riga:  " & GbOldNumRec
            oldLevel = 100
            statement = token & " " & statement
            getDCL statement
          End If
        'AC 31/10/2008  commentiamo (pluralis maiestatis) la gestione della LABEL
        ' proviamo ad andare avanti senza azzerare la linea
        ElseIf Right(token, 1) = ":" And InStr(token, " ") = 0 Or Left(Trim(statement), 1) = ":" Then
          If Right(token, 1) = ":" Then
            line = statement & ";"
            strLabel = Left(token, Len(token) - 1)
          ElseIf Left(Trim(statement), 1) = ":" Then
            strLabel = token
            statement = Trim(Right(statement, Len(statement) - 1))
            line = statement & ";"
          End If
'''          'LABEL: (controllo spazi interni al token per essere sicuri che sia una label...
'''          'ElseIf Right(token, 1) = ":" And InStr(Trim(Left(line, Len(line) - 1)), " ") = 0 Then
'''          ElseIf Right(token, 1) = ":" And InStr(token, " ") = 0 Then
'''            label = True
'''            ' ==> rientra in circolo: aggiunto case "PROC" nel "case"
'''            line = statement & " ;"
'''            'SQ tmp:
'''            If InStr(statement, "CALL ") Then
'''              Debug.Print "Pgm: " & GbNomePgm & "-" & GbIdOggetto & ":" & GbOldNumRec & " #" & statement
'''            End If
        ' Mauro 20/11/2009 : Token = Label:PROC - Devo separarli!!!!
        ElseIf InStr(token, ":") Then
          '� una stringa
          If Left(token, 1) = "'" And Right(token, 1) = "'" Then
            line = ""
          Else
            strLabel = Left(token, InStr(token, ":") - 1)
            line = Mid(token, InStr(token, ":") + 1) & " " & statement & ";"
          End If
          'silvia 08-10-2008
        ElseIf IsNumeric(token) And InStr(token, "(") = 0 And InStr(token, ")") = 0 And UCase(Mid(statement, 1, 5)) <> "ELSE " Then
          'silvia: imposto un oldlevel al massimo
          m_Fun.writeLog "parsePgm_I: INCLUDE#" & GbNomePgm & ", riga:  " & GbOldNumRec
          oldLevel = 100
          statement = token & " " & statement
          getDCL statement
          line = ""
        Else
          If manageMove Then
            Dim lValue As String
            Dim rValue As String
            'ASSEGNAMENTO:
            If Left(statement, 1) = "=" Then
              If Not IsSubstr Then
                lValue = token
                pStart = 0
              Else
                IsSubstr = False
                lValue = getPositionalPLI(token, pStart)
              End If
              rValue = Trim(Mid(statement, 2))
              getPliPositionalMove lValue, rValue, pStart, CampiMove
            End If
          End If
          'RESET statement:
          line = ""
        End If
        'SQ: portato nelle singole IF: non ci vuole in caso di LABEL
        'line = ""
    End Select
  Wend
  Close #nFCbl
  
  'DATA:
  updateData
    
  ' parameter move
  If manageMove Then
    For i = 1 To UBound(CampiMove)
      'stefano: pezza da rimettere a posto da parte di Matteo...
      CampiMove(i).NomeCampo = Replace(CampiMove(i).NomeCampo, "'", "")
      If Not InStr(CampiMove(i).NomeCampo, ".") > 0 Then
        Parser.PsConnection.Execute "Update PsData_Cmp Set Moved = true where Nome = '" & CampiMove(i).NomeCampo & "' and (IdOggetto = " & GbIdOggetto & " or IdOggetto in (Select IdOggettoR from PsRel_Obj where IdOggettoC = " & GbIdOggetto & " and Relazione = 'CPY') )"
      Else
        UpdateCampiMoved CampiMove(i).NomeCampo, GbIdOggetto
      End If
    Next i
     
    'AC  scrittura tabelle  campimoved/valori
    Dim response As String
    Dim retvalue As Integer
    Dim IdOggettoCmp As Long
    Dim idArea As Integer
    Dim Ordinale As Integer
    For i = 1 To UBound(CmpIstruzione) ' alla fine la struttura mi serve solo
                                       ' per fissare le istruzioni non risolte
      ' verifichiamo se istruzione risolta
      If CmpIstruzione(i).Risolta = 0 Then
        response = risolviIstruzioneByMove(CmpIstruzione(i).CampoIstruzione, retvalue, CmpIstruzione(i).LastSerchIndex)
        If Len(response) Then
        ' update Psdli_Istruzioni, delete segnalazione
          'spli: C'ERA UN APICE DI TROPPO NELLA QUERY E GLI APICI IN response, COME FACEVA A FUNZIONARE?
          response = Replace(response, "'", "")
          Parser.PsConnection.Execute "Update PsDLI_Istruzioni Set Istruzione = '" & response & _
                                      "' where IdOggetto =" & CmpIstruzione(i).Oggetto & " and Riga = " & _
                                      CmpIstruzione(i).Riga
          Parser.PsConnection.Execute "Delete from Bs_Segnalazioni where IdOggetto = " & _
                                      CmpIstruzione(i).Oggetto & " and Riga = " & CmpIstruzione(i).Riga & _
                                      " and (Codice = 'H09' Or Codice = 'F09')"
        End If
      End If
    Next i
    For j = 1 To UBound(CampiMove)
      ' aggiornamento tabella campi  PsData_CmpMoved
      ' AreaFromCampo GbIdOggetto, CampiMove(j).NomeCampo, IdOggettoCmp, IdArea, Ordinale
      'If Not idArea = -1 Then
      For k = 1 To CampiMove(j).Valori.count
        'AC 18/06/10 accantoniamo per il momento  ,destSTART ---> ,'" & CampiMove(j).Posizioni.item(k) & "'
        Parser.PsConnection.Execute "Insert Into PsData_CmpMoved (IdPgm,Nome,valore,destSTART) Values (" _
                                    & GbIdOggetto & ",'" & CampiMove(j).NomeCampo & "','" & Replace(Left(CampiMove(j).Valori.item(k), 255), "'", "''") & "','" & CampiMove(j).Posizioni.item(k) & "')"
      Next k
      'End If
    Next j
  End If
    
  Dim rs As Recordset
  Set rs = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  rs!DtParsing = Now
  rs!ErrLevel = GbErrLevel
  rs!parsinglevel = 1
  rs!Cics = SwTpCX
  rs!Batch = SwBatch
  rs!DLI = SwDli 'sarebbe IMS!
  rs!WithSQL = SwSql
  rs!VSam = SwVsam
  rs!mapping = SwMapping
  rs.Update
  ' Mauro 28/09/2009 : Aggiornamento Flag DLI per i pgm che richiamano l'Include appena parserata
  If rs!Tipo = "INC" And SwDli Then
    Dim rsRel As Recordset
    Set rsRel = m_Fun.Open_Recordset("select idoggettoC from psrel_obj where idoggettoR = " & rs!idOggetto)
    Do Until rsRel.EOF
      Parser.PsConnection.Execute "update bs_oggetti set dli = True where idoggetto = " & rsRel!IdOggettoC & " and dli = false"
      rsRel.MoveNext
    Loop
    rsRel.Close
  End If
  rs.Close

  Exit Sub
parseErr:    'Gestito a livello superiore...
  If err.Number = 123 Then
    'QUESTO E' PER IL COBOL. SISTEMARE...
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #nFCbl, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'gestire
      'GbErrLevel = "P-0"
      GbOldNumRec = GbNumRec
      addSegnalazione line, "P00", line
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
    m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
  Else
    addSegnalazione line, "P00", line
    Resume Next
  End If
End Sub

Function getPositionalPLI(token As String, pStart As String) As String
  Dim appotoken As String
  
  appotoken = token
  If Left(appotoken, 1) = "(" And Right(appotoken, 1) = ")" Then
    If InStr(appotoken, ",") > 0 Then
      getPositionalPLI = Mid(appotoken, 2, InStr(appotoken, ",") - 2)
      appotoken = Right(appotoken, Len(appotoken) - InStr(appotoken, ","))
      If InStr(appotoken, ",") > 0 Then
        pStart = Left(appotoken, InStr(appotoken, ",") - 1)
      End If
    End If
  Else
    pStart = 0
    getPositionalPLI = token
  End If
End Function

Sub getPliMove(campo As String, variable As String, ByRef pcampiMove() As UtCampiMove)
  Dim i As Integer, j As Integer

  For i = 1 To UBound(pcampiMove)
    If pcampiMove(i).NomeCampo = campo Then
      Exit For
    End If
  Next i
  If i = UBound(pcampiMove) + 1 Then ' campo nuovo
    ReDim Preserve pcampiMove(UBound(pcampiMove) + 1)
    pcampiMove(UBound(pcampiMove)).NomeCampo = Replace(campo, ",", "")
    Set pcampiMove(UBound(pcampiMove)).Valori = New Collection
    Set pcampiMove(UBound(pcampiMove)).Posizioni = New Collection
    pcampiMove(UBound(pcampiMove)).Valori.Add variable
  Else
    For j = 1 To pcampiMove(i).Valori.count
      If pcampiMove(i).Valori.item(j) = variable Then
        Exit For
      End If
    Next j
    If j = pcampiMove(i).Valori.count + 1 Then
      pcampiMove(i).Valori.Add variable
    End If
  End If
End Sub

'AC 11/06/10 duplico perch� purtroppo utilizzata anche al di fuori del PLI
Sub getPliPositionalMove(campo As String, variable As String, pStart As String, ByRef pcampiMove() As UtCampiMove)
  Dim i As Integer, j As Integer

  For i = 1 To UBound(pcampiMove)
    If pcampiMove(i).NomeCampo = campo Then
      Exit For
    End If
  Next i
  If i = UBound(pcampiMove) + 1 Then ' campo nuovo
    ReDim Preserve pcampiMove(UBound(pcampiMove) + 1)
    pcampiMove(UBound(pcampiMove)).NomeCampo = Replace(campo, ",", "")
    Set pcampiMove(UBound(pcampiMove)).Valori = New Collection
    Set pcampiMove(UBound(pcampiMove)).Posizioni = New Collection
    pcampiMove(UBound(pcampiMove)).Valori.Add variable
    pcampiMove(UBound(pcampiMove)).Posizioni.Add pStart
  Else
    For j = 1 To pcampiMove(i).Valori.count
      If pcampiMove(i).Valori.item(j) = variable Then
        Exit For
      End If
    Next j
    If j = pcampiMove(i).Valori.count + 1 Then
      pcampiMove(i).Valori.Add variable
    End If
    pcampiMove(i).Posizioni.Add pStart
  End If
End Sub

Function getEntryIndex(program As String) As Integer
  Dim i As Integer, j As Integer, arrProg() As String

  getEntryIndex = -1
  For i = 0 To entryIndex
    If Left(entries(i).Name, 1) = "(" And Right(entries(i).Name, 1) = ")" Then
      arrProg = Split(Mid(entries(i).Name, 2, Len(entries(i).Name) - 2), ",")
      For j = 0 To UBound(arrProg)
        If program = arrProg(j) Then
          getEntryIndex = i
          Exit Function
        End If
      Next j
    ElseIf program = entries(i).Name Then
      getEntryIndex = i
      Exit For
    End If
  Next i
End Function

''''''''''''''''''''''''''''''''''''''''''''
' In PsCall solo routine esterne!
''''''''''''''''''''''''''''''''''''''''''''
Private Sub getCALL(statement As String)
  Dim program As String, token As String
  Dim rs As Recordset, entryIndex As Integer
  ''''''''''''''''''''''''''''''
  ' Modulo Chiamato:
  ''''''''''''''''''''''''''''''
  'SwNomeCall = Left(statement, 1) = "'"
  program = nexttoken(statement)
  'SQ 14-11-06
  statement = nexttoken(statement)
  ' Mauro 29/09/2009: Non posso fare la replace di tutte le parentesi -> ...(BINARY(4, 31), 'GHN ', PCBPOINT, IMSIO, SSA_GIROOT)
  'statement = Replace(Replace(statement, ")", ""), "(", "") 'pulizia parentesi
  'statement = Replace(statement, ",", " ") 'argomenti separati da virgole...(meglio usare ciclo di nextToken_tmp...)
  If Len(statement) Then
    statement = Mid(statement, 2, Len(statement) - 2) 'pulizia parentesi
  End If
  Dim stat As String
  stat = ""
  Do Until statement = ""
    stat = stat & IIf(Len(stat), " ", "") & nextToken_newnew(statement)
  Loop
  statement = Trim(stat)
  ''''''''''''''''''''''''''''''''''
  ' ISPF!
  ' tmp:
  ''''''''''''''''''''''''''''''''''
  If program = "ISPLINK" Then
    parseISPLINK program, statement
    Exit Sub
  End If
  ''''''''''''''''''''''''''''''''''''''
  ' Relazioni/Segnalazioni
  ' (solo se modulo ESTERNO)
  ''''''''''''''''''''''''''''''''''''''
  IsExternal = False
  entryIndex = getEntryIndex(program)
  If program = "PLITDLI" Then ' AC : Matteo se ne � lavato le mani, io ho agito d'istinto
    IsExternal = True
  ElseIf entryIndex > -1 Then
    IsExternal = entries(entryIndex).external
  End If
  
  'SQ perch� � commentato? (verificare se ci vuole...)
'  If Not IsExternal Then
'    For i = 0 To builtinIndex
'      If builtins(builtinIndex) = program Then
'        IsExternal = False
'        Exit For
'      End If
'    Next
'  End If

  If IsExternal Then
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Inserimento istruzione in PsCall: SOLO LE ROUTINE ESTERNE!!!
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set rs = m_Fun.Open_Recordset("select * from PsCall")
    rs.AddNew
    rs!idOggetto = GbIdOggetto
    rs!Riga = GbOldNumRec
    rs!programma = program
    rs!parametri = statement
    rs!linee = GbNumRec - GbOldNumRec + 1
    rs.Update
    rs.Close
    MapsdM_Parser.checkRelations program, "CALL"
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Relazione programma/PSB
    ' (riconosce le istruzioni e le inserisce nella PsDli_Istruzioni
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If isDliCall Then 'settato dalla checkRelation
      'Preparo i parametri come arrivassero dal COBOL
      token = nexttoken(statement)  'COUNT
      'AC 14/02/08
      If token = "PCB" Or token = "TERM" Then
        statement = token & " " & statement
      End If
      ' Mauro 29/09/2009: Casi estremi tipo "...(BINARY(4, 31), 'GHN ', PCBPOINT, IMSIO, SSA_GIROOT)"
      If Left(statement, 1) = "(" Then
        token = token & nexttoken(statement)
      End If
      MapsdM_IMS.checkPSB statement
    End If
    
  End If
    
  Exit Sub
callErr:
  'gestire...
  If err.Number = -2147217887 Then  'chiave duplicata...
    '2 CALL su una linea...
  Else
    err.Raise 124, "getCall", "syntax error on " & statement
    Resume
  End If
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''
' &INCLUDE name | dataset(membro)
''
' ATTENZIONE:
' se il membro era stato riconosciuto PLI, viene riclassificato...
' -Segnalazione NOCOPY... SPECIFICARNE UNA PLI!!!!!!!!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getInclude(statement As String) As String
  Dim includeName As String
  'silvia 26-05-2008
  Dim idIncludeID As Long
  
  includeName = nexttoken(statement)
  If InStr(includeName, ")") Then
    includeName = Trim(Replace(Replace(includeName, "(", ""), ")", ""))
  End If
  
  If Right(includeName, 1) = ";" Then includeName = Left(includeName, Len(includeName) - 1)
  'IRIS-DB: to support cases where the include is embedded in a complex structure
  If Right(includeName, 2) = ";," Then includeName = Left(includeName, Len(includeName) - 2)
  'SQ 2-11-06
  MapsdM_Parser.checkRelations includeName, "PLI-INCLUDE"
  'SQ [01]
  'TMP:
  Parser.PsConnection.Execute "INSERT INTO PsPgm_Copy (IdPgm,Riga,COPY,ReplacingVar) VALUES (" & GbIdOggetto & "," & GbOldNumRec & ",'" & includeName & "','" & procName & "')"
'  Set TbOggetti = m_Fun.Open_Recordset("select idOggetto,tipo from Bs_oggetti where tipo = 'INC' and nome = '" & includeName & "'")
'  If TbOggetti.RecordCount = 0 Then
'    'Cerco fra in PGM (riconoscimento "quasi" errato)
'    Set TbOggetti = m_Fun.Open_Recordset("select idOggetto,tipo from Bs_oggetti where tipo = 'PLI' and nome = '" & includeName & "'")
'  End If
'
'  If TbOggetti.RecordCount Then
'    If TbOggetti!Tipo = "PLI" Then
'      TbOggetti!Tipo = "INC"
'      TbOggetti.Update
'    End If
'    AggiungiRelazione TbOggetti!idOggetto, "COPY", includeName, includeName
'  Else
'    If Not ExistObjSyst(includeName, "CPY") Then
'      addSegnalazione includeName, "NOCOPY", includeName
'    End If
'  End If
'  TbOggetti.Close

  '''''''
  'silvia 26-05-2008
  idIncludeID = getIncludeID(includeName)
  If idIncludeID <> 0 Then
    DataItem(Idx).idCopy = idIncludeID
    DataItem(Idx).DatDigit = getCopyDigit(idIncludeID)
    DataItem(Idx).DatString = "A"
  Else
    DataItem(Idx).idCopy = 0
  End If
  '''''
End Function

'silvia 26-05-2008
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ritorna l'ID della INCLUDE se "interna" (non livello 01);
' Ritorna 0 altrimenti
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getIncludeID(copyname As String) As Long
  Dim r As Recordset
  Dim idOggetto As Long
  
  Set r = Open_Recordset("Select idOggetto From BS_Oggetti Where " & _
                         "Nome = '" & Replace(copyname, "'", "") & "' And Tipo = 'INC'")
  If r.RecordCount Then
    idOggetto = r!idOggetto
    Set r = Open_Recordset("Select idArea,ordinale,livello From PsData_Cmp Where " & _
                           "idOggetto = " & idOggetto & " ORDER BY idArea,ordinale")
    If r.RecordCount Then
      'Livello 01? (copy "esterna"?)
      If r!livello = 1 Then idOggetto = 0
    Else
      'Parser.PsFinestra.ListItems.Add , , "There is no parser of INCLUDE : " & copyname & " - ID: " & idOggetto
      idOggetto = 0
    End If
  End If
  r.Close
  getIncludeID = idOggetto
End Function

Sub RipulisciRiga(ByRef line As String)
  Dim InizioCommento As Double
  Dim FineCommento As Double
  
  ' Mauro 17/10/2008
  ' Su MainFrame "dovrebbe" essere corretto troncare a colonna 72, ma su Unix non ci sono limiti
  'SQ 14-12-2010 chiarire il motivo della IF; in ogni caso cos� non pu� funzionare
  If Pli2cobol And False Then
    line = Trim(Mid(line, 2))
  Else
    line = Trim(Mid(line, 2, 71))
  End If
  If Not booEliminaRiga Then
'    spli: pi� commenti sulla stessa riga da gestire
    Do While InStr(line, "/*")
      InizioCommento = InStr(1, line, "/*")
      'silvia 06-10-2008
      FineCommento = InStr(InizioCommento + 2, line, "*/")
      If FineCommento = 0 Then
        booEliminaRiga = True
        FineCommento = Len(line) + 1
      End If
      ' Mauro 10/02/2010 : TEMPORANEO!!! Se ho un /* in una stringa...
      If InizioCommento > 1 Then
        If Mid(line, InizioCommento - 1, 1) = "'" Then
          booEliminaRiga = False
          InizioCommento = 0
          FineCommento = 0
          Exit Do
        End If
      End If
      'silvia 08-10-2008
      If InizioCommento > 0 Then
        'SQ 13-12-2010 - tmp/check
        If Pli2cobol Then
          getComment Mid(line, InizioCommento + 1)
        End If  'sq
        'Else
          line = Trim(Mid(line, 1, InizioCommento - 1)) & Trim(Mid(line, FineCommento + 2))
        'End If
      End If
    Loop
  Else
    If InStr(line, "*/") Then
      If Pli2cobol Then getComment Mid("*" & line, InizioCommento + 1)
      line = Trim(Mid(line, InStr(line, "*/") + 2))
      booEliminaRiga = False
    Else
      'gloria: se entro qui significa che ho aperto un commento e non l'ho chiuso quindi � una riga di commento non devo cancellarla
      line = "*" & line
    End If
    'silvia 06-10-2008
    If Mid(line, 1, 1) = "*" Then
      If Pli2cobol Then
        getComment line
      End If
      line = ""
    End If
  End If
   
End Sub

Public Function DoubleApice(ByVal Pbv_StrSource As String) As String
  'On Error GoTo errorHandler
  Dim IntPos As Integer, IntStart As Integer
   
  IntStart = 1
  IntPos = InStr(IntStart, Pbv_StrSource, "'", vbTextCompare)
  While IntPos > 0
    Pbv_StrSource = Left(Pbv_StrSource, IntPos) & "'" & Right(Pbv_StrSource, Len(Pbv_StrSource) - IntPos)
    IntStart = IntPos + 2
    IntPos = InStr(IntStart, Pbv_StrSource, "'", vbTextCompare)
  Wend
  DoubleApice = Pbv_StrSource
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Parte del Parser.AggiornaDati:
' Portarlo nel parser e utilizzarlo da AggiornaDati
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub updateData()
  Dim idArea As Long, idArea1 As Long, i As Long, j As Integer
  Dim rsArea As Recordset, rsCmp As Recordset, ArrDatNom() As String
   
  On Error GoTo catch
  
  ''''''''''''''''''''''''''''''''''''''''''
  ' COBOL!
  ''''''''''''''''''''''''''''''''''''''''''
  '''CaricaLstCampi GbFileInput, GbIdOggetto
  completeData ("ID " & CStr(GbIdOggetto))
  ''''''''''''''''''''''''''''''''''''''''''
  ' Allestimento Tabelle
  ''''''''''''''''''''''''''''''''''''''''''
  Set rsCmp = m_Fun.Open_Recordset("select * from PsData_cmp where idoggetto = " & GbIdOggetto)
  Set rsArea = m_Fun.Open_Recordset("Select * from PsData_Area where idoggetto = " & GbIdOggetto)
  For i = 1 To Idx
    If Left(DataItem(i).DatNomeFld, 1) = "(" And Right(DataItem(i).DatNomeFld, 1) = ")" Then
      ArrDatNom = Split(Mid(DataItem(i).DatNomeFld, 2, Len(DataItem(i).DatNomeFld) - 2), ",")
    Else
      ReDim ArrDatNom(0)
      ArrDatNom(0) = DataItem(i).DatNomeFld
    End If
    
    For j = 0 To UBound(ArrDatNom)
      If (Not Trim(ArrDatNom(j)) = "" And Not InStr(ArrDatNom(j), ",") > 0 And Not InStr(ArrDatNom(j), ";") > 0) Then
        If DataItem(i).DatLev = 1 Or DataItem(i).DatLev = 77 Or idArea = 0 Then
          idArea = idArea + 1
          rsArea.AddNew
          rsArea!idOggetto = GbIdOggetto
          rsArea!idArea = idArea
          rsArea!nome = Trim(ArrDatNom(j))
          'SQ 11-03-06
          rsArea!livello = DataItem(i).DatLev
          rsArea!isLinkage = DataItem(i).isLinkage
          rsArea.Update
          idArea1 = idArea
        End If
        'SQ
        'If DataItem(i).DatNomeRed = "" Then DataItem(i).DatNomeRed = " "
        'If DataItem(i).DatString = "" Then DataItem(i).DatString = " "
        'If DataItem(i).DatSign = "" Then DataItem(i).DatSign = " "
        'If DataItem(i).DatVal = "" Then DataItem(i).DatVal = " "
        
        rsCmp.AddNew
        rsCmp!codice = format(GbIdOggetto, "00000000") & "/" & format(idArea, "0000") & "/" & format(DataItem(i).DatOrdinale, "0000")
        rsCmp!idArea = idArea1
        rsCmp!idOggetto = GbIdOggetto
        rsCmp!nome = Trim(ArrDatNom(j))
        rsCmp!nomeRed = DataItem(i).DatNomeRed
        rsCmp!Ordinale = DataItem(i).DatOrdinale
        rsCmp!livello = DataItem(i).DatLev
        rsCmp!Tipo = DataItem(i).DatString
        rsCmp!Posizione = DataItem(i).DatPos
        rsCmp!byte = DataItem(i).DatDigit
        rsCmp!Occurs = DataItem(i).DatOccurs
        rsCmp!Lunghezza = "0" & DataItem(i).DatInt  'TMP
        rsCmp!Decimali = DataItem(i).DatDec
        rsCmp!Picture = DataItem(i).DatPicture
        rsCmp!Segno = DataItem(i).DatSign
        rsCmp!Usage = DataItem(i).DatUsage
        rsCmp!idCopy = DataItem(i).idCopy
        rsCmp!Valore = Left(DataItem(i).DatVal, 250)
        'SQ [01]
        rsCmp!proc = DataItem(i).procName
        rsCmp.Update
        
        '''''''''''''''''''''''''''''''''''''''''''''''''
        ' Refresh segnalazioni campi programmi chiamanti
        '''''''''''''''''''''''''''''''''''''''''''''''''
        If GbTipoInPars = "CPY" And Len(rsCmp!Valore) Then  'SQ SPECIFICO COBOL... GENERALIZZARE
          updateSegnalazioni rsCmp!nome, GbTipoInPars
        End If
      Else
         m_Fun.writeLog "IDProgram: " & GbIdOggetto & " #PLI-updateData: '" & Trim(ArrDatNom(j)) & "' is not a valid field."
      End If
    Next j
  Next i
  rsCmp.Close
  rsArea.Close
  Exit Sub
catch:
  'tmp
  'MsgBox "tmp: " & Err.Description
  m_Fun.writeLog "IDProgram: " & GbIdOggetto & " #PLI-updateData: " & err.description
  Resume Next
  ''On Error Resume Next
  ''rsCmp.Close
  ''rsArea.Close
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''
' Delete tabelle allestite dal parse (I)
''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub resetRepository()
  On Error GoTo dbErr
  
  MapsdM_Parser.deleteDataRelations
  
  Exit Sub
dbErr:
  m_Fun.Show_MsgBoxError "RC0002", , "resetRepository", err.source, err.description, False
End Sub

'TMP: completare... utilizzando un "pliData"
Private Function getCobolData(Name As String, pliType As String, pliLen As String, Segno As Boolean, Optional isVarying As Boolean = False) As MapsdM_EASYTRIEVE.cblData
  Dim cblField As cblData
  '
  cblField.Name = Name
  '
  If Not IsNumeric(pliLen) Then
    pliLen = "0"
    'Gestire!
    m_Fun.writeLog "#PLI data: " & Name & " -Len: " & pliLen & " #Id: " & GbIdOggetto
  End If
  '
  'spli: ho aggiunto solo quelli che avevo nei programmi bbv
  'cblField.dataType(0) = Switch(pliType = "CHAR" Or pliType = "CHARACTER", "X",
  '                              pliType = "N" Or pliType = "9", "9",
  '                              pliType = "B", "9",
  '                              pliType = "DEC" Or pliType = "DECIMAL", "9",
  '                              pliType = "BIN" Or pliType = "BINARY", "9",
  '                                 True, "X")
  'cblField.dataType(1) = Switch(pliType = "BIN" Or pliType = "BINARY" Or pliType = "FLOAT", "BNR",
  '                              pliType = "P" Or pliType = "DEC" Or pliType = "DECIMAL", "PCK",
  '                                 True, "ZND")
  'spli
  'cblField.bytes = cblField.dataLen 'TMP! non sono bytes
  Select Case pliType
    Case "BINARY", "BIN", "FLOAT" ' Verificare FLOAT
      cblField.dataType(0) = "9"
      cblField.dataType(1) = "COMP"
      cblField.bytes = bytesBinary(Segno, pliLen)
      cblField.dataLen = cblField.bytes * 2 'TMP!
      cblField.dataDec = 0
      pliLen = cblField.dataLen
    Case "DEC", "DECIMAL", "P" ' Verificare P
      cblField.dataType(0) = "9"
      cblField.dataType(1) = "COMP-3"
      Dim Virgola As Integer
      Dim Lunghezza As Long
      Virgola = InStr(pliLen, ",")
      If Virgola > 0 Then
        Lunghezza = Mid(pliLen, 1, Virgola - 1)
        cblField.dataDec = Mid(pliLen, Virgola + 1)
        cblField.dataLen = Lunghezza - cblField.dataDec
      Else
        cblField.dataLen = pliLen
        cblField.dataDec = 0
      End If
      ' Mauro 18/01/2008 : datalen � solo la parte intera. Copiato da CensCampi per COMP-3
      'cblField.bytes = Round(cblField.dataLen / 2 + 0.6)
      cblField.bytes = Int((Int(cblField.dataLen) + Int(cblField.dataDec)) / 2) + 1
    Case "CHAR", "CHARACTER"
      cblField.dataType(0) = "X"
      'AC 09/06/10
      If isVarying Then
        cblField.dataType(1) = "VAR"
      End If
      'cblField.dataType(1) = "ZND"
      cblField.dataLen = IIf(IsNumeric(pliLen), pliLen, 0)
      cblField.dataDec = 0
      cblField.bytes = cblField.dataLen
    Case "N", "9", "B" ' Verificare B
      cblField.dataType(0) = "9"
      'cblField.dataType(1) = "ZND"
      If InStr(pliLen, ",") Then
        cblField.dataDec = Mid(pliLen, InStr(pliLen, ",") + 1)
        cblField.dataLen = Left(pliLen, InStr(pliLen, ",") - 1)
        cblField.dataLen = cblField.dataLen - cblField.dataDec
      Else
        cblField.dataLen = pliLen
        cblField.dataDec = 0
      End If
      cblField.bytes = cblField.dataLen
    Case "PTR", "POINTER"
      ' Mauro 22/09/209: Gestione POINTER
      'cblField.dataType(0) = "POINTER"
      cblField.dataType(0) = "9"
      cblField.dataType(1) = "PTR"
      'cblField.dataLen = pliLen
      cblField.dataLen = 8
      cblField.dataDec = 0
      'cblField.bytes = cblField.dataLen
      cblField.bytes = 4
    Case "BIT"
      cblField.dataType(0) = "X"
      cblField.dataType(1) = "BIT"
      cblField.dataLen = pliLen
      cblField.dataDec = 0
      cblField.bytes = cblField.dataLen
    Case Else
      cblField.dataType(0) = "X"
      'cblField.dataType(1) = "ZND"
      cblField.dataLen = pliLen
      cblField.dataDec = 0
      cblField.bytes = cblField.dataLen
  End Select
 
  'cblField.bytes = Switch(cblField.dataType(1) = "PCK", Round(cblField.dataLen / 2 + 0.6),
  '                        cblField.dataType(1) = "BNR", IIf(cblField.dataLen > 5, 4, 2),
  '                           True, cblField.dataLen)
  
  ' Livello
  'cblField.isGroup = False
  'cblField.level = format(CurrentLevel, "00")
  getCobolData = cblField
End Function

'Private Function bytesBinary(Segno As Boolean, Lunghezza As String) As String
'  If Segno Then ' CON SEGNO
'    Select Case True
'      Case Lunghezza <= 7
'        bytesBinary = 1
'      Case Lunghezza > 7 And Lunghezza <= 15
'        bytesBinary = 2
'      Case Lunghezza > 15 And Lunghezza <= 31
'        bytesBinary = 3
'      Case Lunghezza > 31 And Lunghezza <= 63
'        bytesBinary = 4
'    End Select
'  Else ' SENZA SEGNO
'    Select Case True
'      Case Lunghezza <= 8
'        bytesBinary = 1
'      Case Lunghezza > 8 And Lunghezza <= 16
'        bytesBinary = 2
'      Case Lunghezza > 16 And Lunghezza <= 32
'        bytesBinary = 3
'      Case Lunghezza > 32 And Lunghezza <= 64
'        bytesBinary = 4
'    End Select
'  End If
'End Function
Private Function bytesBinary(Segno As Boolean, Lunghezza As String) As String
  If Segno Then ' CON SEGNO
    Lunghezza = Lunghezza + 1
  End If
  bytesBinary = Int(Lunghezza / 8) + IIf((Lunghezza Mod 8) > 0, 1, 0)
End Function

Private Function getINIT(statement As String) As String
  Dim token As String
  
  statement = Mid(statement, 2)
  statement = Replace(statement, "''", "' '")
  Do While Len(statement)
    'SQ problema casi INIT('('): nextToken mangia gli apici e sotto si incasina...
    'proviamo con il "_new" che li mantiene sperando di non fare casino
    'token = nextToken(statement)
    token = nextToken_new(statement)  'mantiene gli apici
    'Caso semplice: INIT(250),
    If Right(token, 1) = "," Then
      token = Left(token, Len(token) - 1)
      statement = ", " & statement
    End If
    If Left(token, 1) = "(" Then
      'Posso avere anche forme di questo tipo: INIT((6)' ')
      getINIT = getINIT & token
      token = nexttoken(statement)  'Sarebbe da esplodere...
      'Posso avere anche forme di questo tipo: INIT((6)(4)' ')
      If Left(token, 1) = "(" Then
        getINIT = getINIT & token
        token = nexttoken(statement)  'Sarebbe da esplodere...
      End If
    Else
      If Right(token, 1) = ")" And Len(token) > 1 Then 'Pezza: "and" per i casi (')'): nextToken mi torna senza apici, quindi ")"!!!
        token = Left(token, Len(token) - 1)
        statement = ")" & statement
      End If
    End If
    If Left(token, 1) = "'" Then
      If Right(token, 1) = "'" Then
        token = Mid(token, 2, Len(token) - 2)
      Else
        m_Fun.writeLog "!!!parseData syntax error: " & GbIdOggetto & " -token: " & token
      End If
    End If
    'AC LOW
    getINIT = getINIT & token
    If token = "LOW" Then
        token = nexttoken(statement)
        If Left(token, 1) = "(" And Right(token, 1) = ")" Then
            getINIT = "'" & Space(CInt(Mid(token, 2, Len(token) - 2))) & "'"
        End If
        
    End If
    'devo avere la tonda chiusa
    If Left(statement, 1) = ")" Then
      statement = Trim(Mid(statement, 2))
      Exit Do
    Else
      'Casi di OCCURS! lista di N valori
      If Left(statement, 1) = "," Then
        statement = Trim(Mid(statement, 2))
        ' silvia 08-10-2008
        If Pli2cobol And Occurs > 0 Then
          getINIT = "'" & getINIT & "'" & "," & Mid(statement, 1, Len(statement) - 1)
          ' Mauro 30/10/2008
          statement = ""
          Exit Do
        End If
        'altra iterazione
        'SQ - RAPINA:
        getINIT = ""
      ' Inizializzazione per BIT
      ElseIf statement = "B)" Then
        statement = ""
        Exit Do
      Else
        'Caso che non si deve verificare
        m_Fun.writeLog "!!!parseData syntax error: " & GbIdOggetto
        Exit Do
      End If
    End If
  Loop
End Function

Sub completeData(file As String)
  '********************************************************
  'Calcolo delle lunghezze dei livelli superiori
  '********************************************************
  'Dim line As String, istruzione As String, copyname As String
  Dim TabLen(99) As Double, TabPos(50, 3) As Double
  Dim ILen As Double, IPos As Double ', wpos As Double
  Dim I01 As Double
  Dim SavLev As Integer, k As Integer
  'Dim SwEndTable As Boolean, isLinkage As Boolean
  'Dim wLiv01 As Long, wLivPrec As Long, wIdArea As Long
  Dim Start As Long
  Dim BolBit As Boolean
  Dim LenBit As Integer
  
  On Error GoTo errorHandler
  LenBit = 0
  BolBit = False
  
  SavLev = Val(DataItem(Idx).DatLev)
  ILen = SavLev
  TabLen(ILen) = DataItem(Idx).DatDigit
  Start = Idx - 1
  If SavLev = 88 Then
    For Start = Start To 2 Step -1
''      If Val(DataItem(Start).DatLev) <> 88 Then
        If Val(DataItem(Start).DatLev) = 1 Then
          Start = Start - 1
          SavLev = Val(DataItem(Start).DatLev)
          If SavLev < 100 Then
            ILen = SavLev
          Else
            m_Fun.writeLog "Impossible to complete Data Structure Length for PGM: " & file
          End If
          TabLen(ILen) = DataItem(Start).DatDigit
        Else
          SavLev = Val(DataItem(Start).DatLev)
          ILen = SavLev
        End If
        Exit For
''      End If
    Next Start
  End If
  
  For Start = Start To 1 Step -1
    If Val(DataItem(Start).DatLev) = SavLev Then
''      If Val(DataItem(Start).DatLev) <> 88 Then
        
    '' tilvia bit
        If DataItem(Start).DatUsage = "BIT" Then
          If LenBit < 8 And DataItem(Start).DatOccurs = 0 Then LenBit = LenBit + 1
          If BolBit And LenBit = 8 Then
             TabLen(ILen) = TabLen(ILen) + DataItem(Start).DatDigit
             LenBit = 1
          Else
            BolBit = True
          End If
        Else
          BolBit = False
          LenBit = 0
          If DataItem(Start).DatOccurs = 0 Then
            TabLen(ILen) = TabLen(ILen) + DataItem(Start).DatDigit
          End If
        End If
        If DataItem(Start).DatOccurs <> 0 Then
          If BolBit Then
            LenBit = LenBit + Int(DataItem(Start).DatOccurs) - 2
          Else
            TabLen(ILen) = TabLen(ILen) + (DataItem(Start).DatDigit * DataItem(Start).DatOccurs)
          End If
        End If
    
''      End If
    Else
      If Val(DataItem(Start).DatLev) > SavLev Then
        SavLev = Val(DataItem(Start).DatLev)
        'ILen = ILen - 1
        ILen = SavLev
        If ILen < 0 Then
          ILen = 0
        End If
        
    '' tilvia bit
        If DataItem(Start).DatUsage = "BIT" Then
           If BolBit And LenBit = 8 Then
             TabLen(ILen) = DataItem(Start).DatDigit
             LenBit = 1
           Else
             BolBit = True
           End If
           If LenBit < 8 Then LenBit = LenBit + 1
        Else
          BolBit = False
          LenBit = 0
          If DataItem(Start).DatOccurs = 0 Then
            TabLen(ILen) = DataItem(Start).DatDigit
          End If
        End If
        If DataItem(Start).DatOccurs <> 0 Then
          If BolBit Then
            LenBit = LenBit + Int(DataItem(Start).DatOccurs) - 2
          Else
            TabLen(ILen) = (DataItem(Start).DatDigit * DataItem(Start).DatOccurs)
          End If
        End If

      Else
        SavLev = Val(DataItem(Start).DatLev)
        ' Mauro 20/01/2009: MODIFICA SOLO I CAMPI DI GRUPPO
        If DataItem(Start).DatDigit = 0 Then
          If DataItem(Start).DatOccurs = 0 Then
            DataItem(Start).DatDigit = TabLen(ILen)
          Else
            DataItem(Start).DatDigit = TabLen(ILen) * DataItem(Start).DatOccurs
          End If
        End If
        'ILen = ILen + 1
        ILen = SavLev
        If SavLev = 1 Then
          TabLen(ILen) = 0
        Else
          TabLen(ILen) = TabLen(ILen) + DataItem(Start).DatDigit
        End If
        For k = ILen + 1 To 99
          TabLen(k) = 0
        Next k
      End If
    End If

    If Trim(DataItem(Start).DatNomeRed & "") <> "" Then
      'SG : TabLen(ILen) = TabLen(ILen) - DataItem(Start).DatDigit
      TabLen(ILen) = Abs(TabLen(ILen) - DataItem(Start).DatDigit)
    End If
  Next Start
  
  ' ****************************************
  ' * Calcolo Scostamenti dei campi *
  ' ****************************************
  I01 = 1
  BolBit = False
  LenBit = 0
  SavLev = Val(DataItem(2).DatLev)
  ILen = 1
  TabLen(ILen) = DataItem(2).DatDigit
  IPos = 1
  If DataItem(1).DatLev = "" Then
    TabPos(IPos, 1) = 0
  Else
    TabPos(IPos, 1) = DataItem(1).DatLev
  End If
  TabPos(IPos, 2) = 1
  TabPos(IPos, 3) = DataItem(1).DatDigit
  DataItem(1).DatPos = 1
  For Start = 2 To Idx
''    If Val(DataItem(Start).DatLev) = 88 Then
''      DataItem(Start).DatPos = DataItem(Start - 1).DatPos
''    Else
      If Val(DataItem(Start).DatLev) = 1 Then
        If DataItem(I01).DatDigit = 0 Then
          DataItem(I01).DatDigit = TabLen(ILen)
        End If
        TabLen(ILen) = 0
        I01 = Start
        IPos = 1
        TabPos(IPos, 1) = DataItem(Start).DatLev
        TabPos(IPos, 2) = 1
        DataItem(Start).DatPos = 1
        TabPos(IPos, 3) = DataItem(Start).DatDigit
        SavLev = Val(DataItem(2).DatLev)
      Else
        If Val(DataItem(Start).DatLev) = SavLev Then
          If Trim(DataItem(Start).DatNomeRed & "") = "" Then
          'T 02092010 gestione occurs
          'T 20092010 campi BIT
            If DataItem(Start).DatUsage = "BIT" Then
              If BolBit And LenBit < 8 Then
                DataItem(Start).DatDigit = 0
              End If
            Else
              BolBit = False
            End If
            If DataItem(Start).DatOccurs <> 0 Then
              If BolBit Then
              Else
                TabLen(ILen) = TabLen(ILen) + (DataItem(Start).DatDigit * DataItem(Start).DatOccurs)
              End If
            Else
              TabLen(ILen) = TabLen(ILen) + DataItem(Start).DatDigit
            End If
          End If
        End If
        If LenBit = 8 Then
          LenBit = 0
          BolBit = False
          TabPos(IPos, 3) = 1
        End If
        If DataItem(Start).DatLev = TabPos(IPos, 1) Then
          If DataItem(Start).DatNomeRed <> "" Then
            DataItem(Start).DatPos = TabPos(IPos, 2)
          Else
            'T 02092010 gestione occurs
            'T 20092010 campi BIT
            ''TabPos(IPos, 2) = TabPos(IPos, 2) + TabPos(IPos, 3)
            If DataItem(Start).DatUsage = "BIT" Then
              If Not BolBit Then
                TabPos(IPos, 2) = TabPos(IPos, 2) + TabPos(IPos, 3)
                BolBit = True
              Else
               '' DataItem(Start).DatDigit = 0
              End If
              If LenBit < 8 Then LenBit = LenBit + 1
            Else
               TabPos(IPos, 2) = TabPos(IPos, 2) + TabPos(IPos, 3)
               BolBit = False
            End If
            If DataItem(Start).DatOccurs <> 0 Then
              If BolBit Then
                 LenBit = LenBit + Int(DataItem(Start).DatOccurs) - 1
              Else
                TabPos(IPos, 3) = (DataItem(Start).DatDigit * DataItem(Start).DatOccurs)
              End If
            Else
              TabPos(IPos, 3) = DataItem(Start).DatDigit
            End If
            DataItem(Start).DatPos = TabPos(IPos, 2)
          End If
        Else
            If DataItem(Start).DatUsage = "BIT" Then
              If Not BolBit Then
                 BolBit = True
              End If
              LenBit = LenBit + 1
            End If
          
          If DataItem(Start).DatLev > DataItem(Start - 1).DatLev Then
            IPos = IPos + 1
            TabPos(IPos, 1) = DataItem(Start).DatLev
            TabPos(IPos, 2) = TabPos(IPos - 1, 2)
            TabPos(IPos, 3) = DataItem(Start).DatDigit
            DataItem(Start).DatPos = TabPos(IPos, 2)
          Else
            For IPos = IPos To 1 Step -1
              If DataItem(Start).DatLev = TabPos(IPos, 1) Then Exit For
            Next IPos
            If DataItem(Start).DatNomeRed <> "" Then
              DataItem(Start).DatPos = TabPos(IPos, 2)
            Else
              TabPos(IPos, 2) = TabPos(IPos, 2) + TabPos(IPos, 3)
              TabPos(IPos, 3) = DataItem(Start).DatDigit
              DataItem(Start).DatPos = TabPos(IPos, 2)
            End If
          End If
        End If
      End If
''    End If
  Next Start
  If DataItem(I01).DatDigit = 0 Then
    DataItem(I01).DatDigit = TabLen(ILen)
  End If
  Exit Sub
errorHandler:
  writeLog "E", "Unexpected Error: " & err.description
End Sub

'SILVIA 02-10-2008
''''''''''''''''''''''''''''''''''''''''''''''
' Scrittura Commenti
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getComment(statement As String)
  On Error GoTo catch
  tagWrite Space(6) & statement

  Exit Sub
catch:
  writeLog "E", "parsePgm_I: error on getComment!"
End Sub

Private Sub getSECTION(procName As String)
  Dim i As Long
  
  'tagWrite Space(11) & "." & vbCrLf &
  tagWrite Space(6) & "*--------" & vbCrLf & _
           Space(7) & procName & " SECTION." & vbCrLf & _
           Space(6) & "*--------"
End Sub

Private Sub getEND_SECTION(procName As String)
  tagWrite Space(7) & procName & "-EX." & vbCrLf & _
           Space(12) & "EXIT."
End Sub

Private Sub writeLog(typeMsg As String, message As String)
  Dim rs As Recordset
  
  Set rs = m_Fun.Open_Recordset("SELECT * FROM MgPLI_segnalazioni WHERE IdOggetto= " & GbIdOggetto)
  rs.AddNew
  rs!idOggetto = GbIdOggetto
  rs!Riga = GbNumRec
  rs!tipoMsg = typeMsg
  rs!operatore = operator
  rs!descrizione = message
  rs.Update
  rs.Close
End Sub

Sub deleteSegnalazioni(idOggetto As Long)
  Parser.PsConnection.Execute "Delete * from MgPLI_segnalazioni where IdOggetto = " & idOggetto
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 17-12-08
' TMP INCAPSULAMENTO REV_ & PROC
'''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub patchREV_(fileName As String, outputDir As String, Optional options As String = "000")
  Dim rs As Recordset
  Dim FD As Long, fdOUT As Long
  Dim line As String, token As String, execStatement As String, outputFile As String
  Dim isLabel As Boolean
  Dim lineNumber As Long
  Dim statement As String
  Dim CurrentPROC As String
  Dim IsWronglen As Boolean
  Dim colRevLine As Collection, apporev As String, appoproc As String, curCollLine As String
  Dim isErrorGetRev As Boolean
  Dim listREV_X() As String
  Dim label As String
  
  ' Mauro 25/03/2009
  Dim listDCLREV() As String
  Dim isMain As Boolean
  Dim procNumbers As Integer
  
  On Error GoTo parseErr
  
  Set colRevLine = New Collection
  
  Dim member As String
  
  ''''''''''''''''''
  'SQ "selettori"
  ''''''''''''''''''
  Dim movePROC As Boolean
  Dim delX As Boolean
  Dim changeLen As Boolean
 
  If Left(options, 1) = "1" Then
    movePROC = True
  End If
  If Mid(options, 2, 1) = "1" Then
    delX = True
  End If
  If Mid(options, 3, 1) = "1" Then
    changeLen = True
  End If
    
  'init
  ReDim listREV_X(0)
  ReDim listDCLREV(0)
  procNumbers = 0
  'SQ----no!!------------On Error GoTo catch
  member = Mid(fileName, InStrRev("\" & fileName, "\"))
  '''''''''''''''''''''''''''''''''''''''''
  ' DB REV_
  '''''''''''''''''''''''''''''''''''''''''
  Dim revList As New Collection
  Set rs = m_Fun.Open_Recordset("select idoggetto from Bs_oggetti where nome='" & member & "'")
  If rs.EOF Then
    m_Fun.writeLog "_90_" & "File not found: " & member
    rs.Close
    Exit Sub
  Else
    Dim idOggetto As Long
    idOggetto = rs!idOggetto
  End If
  rs.Close
  
  FD = FreeFile
  Open fileName For Input As FD
  'output:
  fdOUT = FreeFile
  outputFile = outputDir & "\" & member
  Open outputFile For Output As fdOUT
  
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    Line Input #FD, line
    lineNumber = lineNumber + 1
    IsWronglen = False
    '
    statement = line
    token = nexttoken(statement)
    ''''''''''''''''''''''''''''''
    ' CONTROLLO DCL
    ''''''''''''''''''''''''''''''
    If token = "DCL" Then
      token = nexttoken(statement)
      If Left(token, 4) = "REV_" And Left(token, 7) <> "REV_LNK" And token <> "REV_PTR" Then
        ReDim Preserve listDCLREV(UBound(listDCLREV) + 1)
        If isMain Then
          listDCLREV(UBound(listDCLREV)) = token & "|MAIN"
        Else
          listDCLREV(UBound(listDCLREV)) = token & "|" & CurrentPROC
        End If
        ''''''''''''''''''''''''''''''''''''
        ' DCL REV_
        ''''''''''''''''''''''''''''''''''''
        '(verificare LEN!)
        Dim campo As String
        Dim lenCampo As Integer
        Dim lenField As String, oldlenField As String
        'NOME CAMPO ORIGINALE
        campo = Mid(token, 5)
        'LUNGHEZZA CAMPO INCAPSULATO:
        token = nexttoken(statement)  'CHAR
        lenField = Replace(Replace(nexttoken(statement), ")", ""), "(", "")
        
        'INFO DI RIFERIMENTO: REV_REPORT_
        Set rs = m_Fun.Open_Recordset("select * from REV_REPORT_ where id=" & idOggetto & " AND nome='" & campo & "' ORDER BY EXTRA DESC")
        If rs.EOF Then
          'Potrebbe essere presente con il "$" davanti: (P.S. dovrei controllare anche nella riga successiva...)
          If InStr(statement, "$" & campo) Then
            rs.Close 'serve?
            Set rs = m_Fun.Open_Recordset("select * from REV_REPORT_ where id=" & idOggetto & " AND nome='$" & campo & "' ORDER BY EXTRA DESC")
          End If
          If rs.EOF Then
            'FIELD NOT FOUND
            m_Fun.writeLog "_91_" & "FIELD not FOUND. ID: " & idOggetto & ",PGM: " & member & ",FLD: " & campo
          End If
          '
          Print #fdOUT, line
        Else
          'GESTIONE "X" (esclusione):
          If delX And rs!Tipo = "X" Then
            'UTILIZZO CAMPO ORIGINALE:
            'a) ELIMINO REV_
            'b) MEMORIZZO CAMPO ORIGINALE per utilizzo nelle istruzioni
            'tmp:
            'Print #fdOUT, line & "#X#"
            'CONTROLLO SE VA SU DUE RIGHE:
            If InStr(line, ";") = 0 Then
              'VA A CAPO: riga da eliminare
              Line Input #FD, line
              lineNumber = lineNumber + 1
              'verifica di sicurezza
              If InStr(line, "BASED") = 0 Then
                m_Fun.writeLog "_88_" & "##ERRORE delX:" & " ID: " & idOggetto & ",PGM: " & member & ",LINE: " & line
              End If
            End If
            listREV_X(UBound(listREV_X)) = campo
            ReDim Preserve listREV_X(UBound(listREV_X) + 1)
          Else
            'VERIFICA LUNGHEZZA: (modByte indica che la lunghezza � "affidabile"!
            If rs!mod_Byte And lenField <> rs!byte Then
              m_Fun.writeLog "_94_" & "WRONG LEN. ID: " & idOggetto & ",PGM: " & member & ",FLD: " & campo & " (old: " & lenField & ",new: " & rs!byte & ")"
              If changeLen Then
                'AC
                IsWronglen = True
                oldlenField = lenField
                lenField = rs!byte
                line = Replace(line, "(" & oldlenField & ")", "(" & lenField & ")")
                'Controllo lunghezza: non deve oltrepassare colonna 72!
                Dim diffLen As Integer
                diffLen = Len(lenField) - Len(oldlenField)
                If diffLen Then
                  If Len(line) > 72 Then
                    'MANGIO BIANCHI PRIMA DI /*BPHX*/
                    line = Replace(line, Space(diffLen) & "/*BPHX*/", "/*BPHX*/")
                    'verifica ulteriore (nel caso non ci fossere bianchi sufficienti:
                    If Len(line) > 72 Then
                      m_Fun.writeLog "_87_" & "###ERROR LEN SUBSTITUTION. ID: " & idOggetto & ",PGM: " & member & ",FLD: " & campo & " (old: " & oldlenField & ",new: " & lenField & ") - OLTRE COL 72!!!"
                    End If
                  End If
                End If
              End If
            Else
              'OK!
            End If
            If Len(rs!proc & "") Then
              'Se PROC = CURRENT_PROC: OK
              If rs!proc = CurrentPROC Then
                Print #fdOUT, line
                If IsWronglen Then
                  m_Fun.writeLog "_81_" & "LEN SUBSTITUTION. ID: " & idOggetto & ",PGM: " & member & ",FLD: " & campo & " (old: " & oldlenField & ",new: " & lenField & ")"
                End If
              Else
                'VERIFICA DI SICUREZZA:
                If Len(CurrentPROC) = 0 Then
                  m_Fun.writeLog "_92_" & "currPROC not FOUND. ID: " & idOggetto & ",PGM: " & member & " - EXIT - "
                  Exit Sub
                End If
                '''''''''''''''''''''''''''''''''
                ' REV_ DA SPOSTARE:
                '''''''''''''''''''''''''''''''''
                If movePROC Then
                  apporev = ""
                  'SQ ??????????????????????
                  ''''''''''''''''''''''CurrentPROC = rs!PROC
                  apporev = colRevLine.item(rs!proc)
                  colRevLine.Remove rs!proc
                  colRevLine.Add apporev & line & vbCrLf, rs!proc
                  If IsWronglen Then
                    m_Fun.writeLog "_82_" & "LEN SUBSTITUTION (DIFFERENT PROC). ID: " & idOggetto & ",PGM: " & member & ",FLD: " & campo & " (old: " & oldlenField & ",new: " & lenField & ")"
                  End If
                Else
                  'debug:
                  'Print #fdOUT, line & "#" & rs!proc & "#" & CurrentPROC
                  Print #fdOUT, line
                End If
              End If
            Else
              m_Fun.writeLog "_93_" & "PROC not FOUND. ID: " & idOggetto & ",PGM: " & member & ",FLD: " & campo
              'DEBUG
              'Print #fdOUT, line & " #!VERIF!#"
              Print #fdOUT, line
            End If
          End If
        End If
        rs.Close
      Else
        Print #fdOUT, line
      End If
    Else
      ''''''''''''''''''''''''''''''
      ' CONTROLLO "REV_" "X" da eliminare:
      ''''''''''''''''''''''''''''''
      If delX And InStr(line, "REV_") > 0 Then
        '(di sicuro non � una PROC)
        'Ignoro il campo REV_LNKDB_AREA
        If InStr(line, "REV_LNKDB_AREA") = 0 Then
        Dim i As Integer
          For i = 1 To UBound(listDCLREV) - 1
            If InStr(line, Left(listDCLREV(i), InStr(listDCLREV(i), "|") - 1)) Then
              Exit For
            End If
          Next
          If i > UBound(listDCLREV) Then
            m_Fun.writeLog "_1000_" & "ID: " & idOggetto & ",PGM: " & member & ",LINE: " & line & " - DCL NON TROVATA!"
          End If
        
          For i = 0 To UBound(listREV_X) - 1
            If InStr(line, listREV_X(i)) Then
              Exit For
            End If
          Next
          If Len(listREV_X(i)) Then
            'Campo da eliminare:
            m_Fun.writeLog "_89_" & "ID: " & idOggetto & ",PGM: " & member & ",FLD: " & listREV_X(i) & ",LINE: " & line & " - REV_X ELIMINATO!"
            'REV_ da eliminare!
            line = Replace(line, "REV_", "")
            '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            'ATTENZIONE: cos� mi perdo i campi che iniziano con "$"!!!!!!!!!!!!!
            '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            ' -> controllare "a mano"... sono pochissimi
          End If
        End If
        Print #fdOUT, line
      Else
        ''''''''''''''''''''''''''''''
        ' CONTROLLO PROC
        ''''''''''''''''''''''''''''''
        Print #fdOUT, line '(comunque scrivo)
        ' Mauro 26/03/2009
        'If InStr(statement, "PROC") Then
        If InStr(line, "PROC") Then
          label = token
          If Right(label, 1) = ":" Then
            ': attaccati al nome. OK
            label = Left(label, Len(label) - 1)
          ElseIf InStr(token, ":") Then
            ':PROC attaccati al nome. OK
            label = Left(label, InStr(token, ":") - 1)
          Else
            If Left(statement, 1) <> ":" Then
              '"falsa" PROC (es: /* PROCEDURA BLA.... */
              label = ""
            End If
          End If
          If Len(label) Then
            ''''''''''''''''''''''''''''''''
            ' PROC:
            ''''''''''''''''''''''''''''''''
            procNumbers = procNumbers + 1 'per verifica finale
            'PROC
            CurrentPROC = label
            ' Mauro 25/03/2009
            If InStr(UCase(statement), "MAIN") Or procNumbers = 1 Then
              isMain = True
            Else
              isMain = False
            End If
            For i = UBound(listDCLREV) To 1 Step -1
              If Not isMain Then
                If Mid(listDCLREV(i), InStr(listDCLREV(i), "|") + 1) <> "MAIN" Then
                  ReDim Preserve listDCLREV(i - 1)
                End If
              End If
            Next i
            'INSERIMENTO REV_:
            If movePROC Then
              'Controllo se ci sono REV_ da inserire:
              isErrorGetRev = False
              statement = colRevLine.item(CurrentPROC)
              If Not isErrorGetRev Then
                'VERIFICA: e' finito lo statement "PROC"? (potrebbe essere su pi� righe)
                If Right(RTrim(Left(line, 72)), 1) <> ";" Then
                  'Potrebbe esserci commento in fondo. Controllo scrauso: braso dall'inizio del commento in poi
                  line = Left(line, InStr(line & "/*", "/*") - 1)
                  If Right(RTrim(Left(line, 72)), 1) <> ";" Then
                    'per ora solo messaggio... vediamo di trovare il caso, poi, lo gestiamo!
                    m_Fun.writeLog "_84_" & "##ERRORE INSERT##:" & " ID: " & idOggetto & ",PGM: " & member & ",PROC: " & CurrentPROC
                  End If
                End If
                Print #fdOUT, vbCrLf & statement
                m_Fun.writeLog "_83_" & "REV INSERT IN PROC:" & CurrentPROC & ". ID: " & idOggetto & ",PGM: " & member & ",FLD: " & campo & " (old: " & oldlenField & ",new: " & lenField & ")"
              End If
              'SQ
              'If Not isErrorGetRev Then
              '  m_Fun.writeLog "_84b_" & "REV INSERT IN PROC:" & CurrentPROC & ". ID: " & idOggetto & ",PGM: " & member & ",FLD: " & campo & " (old: " & oldlenField & ",new: " & lenField & ")"
              'End If
            End If
          End If
        End If
      End If
    End If
  Wend
  Close FD
  Close fdOUT
  
  '''''''''''''''''''''''''''''
  ' VERIFICHE
  '''''''''''''''''''''''''''''
  'procNumbers:
  Exit Sub
  
parseErr:   'Gestito a livello superiore...
  If err.Number = 123 Then
    'Riga che non interessa: da riscrivere
    token = ""
    Resume Next
  ElseIf err.Number = 5 Then '424 Then
    'SQ
    'colRevLine.Add "", CurrentPROC
    isErrorGetRev = True
    Resume Next
  Else
    m_Fun.writeLog "_99_" & "UNEXPECTED ERROR! " & err.description
    On Error Resume Next
    Close FD
    Close fdOUT
  End If
End Sub
'gloria: identifica tutte le proc/function all'interno del codice e le salva in ArrayProc
Private Sub checkProc(line As String)
   Dim statement As String, token As String
   Dim i As Integer, n As Integer, IniPar As Integer, EndPar As Integer
   Dim afterProc As String, BeforeProc As String, sParameters As String
   
   i = InStr(line, "PROC")
   
   ReDim Preserve arrProc(countProc)
   'divido tra prima e dopo proc
   BeforeProc = Left(line, i - 1)
   afterProc = Right(line, Len(line) - (i + 3))
   
   'prima del ": PROC" ho il nome
   i = InStr(BeforeProc, ":")
   
   If i = 0 Then Exit Sub
   
   BeforeProc = Left(BeforeProc, i - 1)
   arrProc(countProc).Name = Replace(Trim(BeforeProc), "_", "-")
   
   'dopo il "PROC" posso avere "(parametri input) RETURN(tipodato ritorno)
   'oppure solo RETURN(tipodato ritorno)o solo parametri
   
   '-> cerco tra PROC e RETURN se c'� qualcosa
   i = InStr(afterProc, "RETURNS")
   'T
   If i > 0 Then
     afterProc = Left(afterProc, i - 1)
   End If
   If InStr(afterProc, "(") Then ' se trovo una parentesi ci sono i parametri
      'posso avere 1 solo parametro -> non avr� la ","
      'oppure pi� parametri separati da ","
     IniPar = InStr(afterProc, "(")
     EndPar = InStr(afterProc, ")")
     'SQ issue
     'SQ tmp:
    If EndPar = 0 Then
      'AC  23/12/10 occorre trovare la parentesi chiusa sulle linee successive
      line = "PROC NOT IN ONE LINE"
      Exit Sub
      'vecchia gestione proseguiva da qui
      EndPar = Len(afterProc)
    End If
    
    
     sParameters = Mid(afterProc, IniPar + 1, EndPar - (IniPar + 1))
      
     sParameters = Replace(sParameters, "_", "-")
     arrProc(countProc).parameters = Split(sParameters, ",")
     
     
     'abbiamo scelto un nome standard perch� il valore non viene passato sempre in una sola variabile
     arrProc(countProc).Return = IIf(i > 0, "RET-" & arrProc(countProc).Name, "")

   Else
     ReDim arrProc(countProc).parameters(0)
     arrProc(countProc).parameters(0) = ""
   End If
   
   countProc = countProc + 1
   ' quello che c'� dopo return non mi interessa
End Sub
Private Function isPROC(Name As String) As Boolean
   Dim i  As Integer
   
   isPROC = False
   
   For i = 0 To countProc - 1
      If InStr(Name, Replace(arrProc(i).Name, "_", "-")) > 0 And InStr(Name, "END " & Replace(arrProc(i).Name, "_", "-")) = 0 Then
         isPROC = True
         Exit For
      End If
   Next
   
End Function

Private Function getProc(destination As String, statement As String) As String
      
  Dim i As Integer, n As Integer, ProcIndex As Integer
  Dim tempParameter As String, ArrayParameter() As String
  
  ProcIndex = -1
  'prima di tutto identifico la proc e i paraemtri
  For i = 0 To countProc - 1
   If InStr(statement, Replace(arrProc(i).Name, "_", "-")) > 0 Then
      ProcIndex = i
      Exit For
   End If
  Next
       
  ' se nella Proc ho dei parametri in ingresso devo prima fare le MOVE
  If arrProc(ProcIndex).parameters(0) <> "" Then
     'devo prendere il paramentro dallo statement
      tempParameter = Mid(statement, InStr(1, statement, "(") + 1, Len(statement) - InStr(1, statement, "(") - 1)

      'devo pulire il parametro da eventuali conversioni di tipo dato da number to char
      Dim startChar As Integer, startComma As Integer
      
      If InStr(tempParameter, "CHAR(") > 0 Then
         startChar = InStr(tempParameter, "CHAR")
         For i = startChar + 5 To Len(tempParameter)
           If Mid(tempParameter, i, 1) = ")" Then
             If i < Len(tempParameter) Then
               tempParameter = Mid(tempParameter, startChar + 5, i - (startChar + 5)) & Mid(tempParameter, i, Len(tempParameter) - (i + 1))
             Else
               tempParameter = Mid(tempParameter, startChar + 5, i - (startChar + 5))
             End If
             'ora devo togliere la virgola del char
             startComma = InStr(tempParameter, ",")
             For n = startComma To Len(tempParameter)
               If Mid(tempParameter, n, 1) = "," Then
                  tempParameter = Left(tempParameter, startComma - 1) & Mid(tempParameter, startComma + 1, Len(tempParameter) - (startComma + 1))
                  Exit For
               End If
             Next
             Exit For
           End If
         Next
      End If
      
      'prima della MOVE devo controllare se campi composti (pippo.pluto)
      ' ma anche dopo..
      If InStr(destination, ".") > 0 Then
         destination = varDepend(destination)
      End If
      
      If UBound(arrProc(ProcIndex).parameters) > 0 Then
         ArrayParameter = Split(tempParameter, ",")
         
         For n = 0 To UBound(arrProc(ProcIndex).parameters)
           If InStr(arrProc(ProcIndex).parameters(n), ".") > 0 Then
             arrProc(ProcIndex).parameters(n) = varDepend(arrProc(ProcIndex).parameters(n))
           End If
           If InStr(ArrayParameter(n), "SUBSTR") > 0 Then
             If Left(ArrayParameter(n), 1) = "(" Then
                'tolgo parentesi esterne
                ArrayParameter(n) = Mid(2, ArrayParameter(n), Len(ArrayParameter(n)) - 2)
             End If
             'tolgo la SUBSTR
             ArrayParameter(n) = Replace(ArrayParameter(n), "SUBSTR", "")
             ArrayParameter(n) = getSUBSTR(ArrayParameter(n))
           End If
           'poi la vardepend
           If InStr(ArrayParameter(n), ".") > 0 Then
             ArrayParameter(n) = varDepend(ArrayParameter(n))
           End If

           tagWrite indent & "MOVE " & ArrayParameter(n) & " TO " & arrProc(ProcIndex).parameters(n)
         Next n
      Else
         If InStr(tempParameter, "SUBSTR") > 0 Then
             If Left(tempParameter, 1) = "(" Then
                'tolgo parentesi esterne
                tempParameter = Mid(2, tempParameter, Len(tempParameter) - 2)
             End If
             'tolgo la SUBSTR
             tempParameter = Replace(tempParameter, "SUBSTR", "")
             tempParameter = getSUBSTR(tempParameter)
           End If
           If InStr(tempParameter, ".") > 0 Then
              tempParameter = varDepend(tempParameter)
           End If
         'prima della move verifico che il parametro nn sia il risultato di un'altra proc
         tagWrite indent & "MOVE " & tempParameter & " TO " & arrProc(ProcIndex).parameters(0)
      End If
      
      For i = Len(statement) To 1 Step -1
         If Mid(statement, i, 1) = ")" Then
            getProc = Right(statement, Len(statement) - i) & ";"
            Exit For
         End If
      Next
      
  End If
'  End If
  'altrimenti devo fare la perform e poi fare la MOVE dell'OUTPUT (che sar� la variabile RETURN della Proc)
  '-> MOVE RETURN to Destination
  If ProcIndex > -1 Then
   tagWrite indent & "PERFORM " & arrProc(ProcIndex).Name & " THRU " & arrProc(ProcIndex).Name & "-EX"
   
   If arrProc(ProcIndex).Return <> "" Then tagWrite indent & "MOVE " & arrProc(ProcIndex).Return & " TO " & destination
  End If
End Function
Private Sub getCeil(destination As String, source As String)
   
   source = Replace(Replace(Replace(Replace(source, "CEIL", ""), "(", ""), ")", ""), ";", "")
   tagWrite indent & "COMPUTE " & destination & " ROUNDED = " & Trim(source)
   
End Sub
