Attribute VB_Name = "MapsdM_ASM"
Option Explicit
Option Compare Text
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' - rel. 2.0 (SQ - 28-02-10) Utilizzo puntatori (ASM2CBLL/")
' - rel. 2.1 (SQ - 11-03-10) Gestione "BR" (BR-LABELS, EVALUATE, etc..)
' - rel. 2.2 (SQ - 17-03-10) Gestione ADDRESS x PARAGRAFI!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Const migrationPath = "Output-prj\Languages\Asm2Cobol"
Const templatePath = "Input-prj\Languages\Asm2Cobol"

Const TILDE = "~"

Type operand
  base As String
  Displace As String
  displacePlus As String
  Index As String
  len As Integer
  value As String
  format As String
  iType As String
End Type

Type instruction
  operator As String
  loperand As operand
  roperand As operand
End Type

Type asmData
  Name As String
  dupFactor As String
  Type As String
  modifier As String
  modifierValue As String
  value As String
  level As String
End Type

'''''''''''''''''''''''''''''''
'SQ 22-08-08 (si introduce un po' di logica... piano piano!)
'''''''''''''''''''''''''''''''
Type regPtr
  register As String
  FIELD As String
End Type

Type Paragraphs
  Name As String
  regPtrs() As regPtr
End Type

Dim Paragraphs() As Paragraphs  'fare collection...
Dim I_parag As Integer
Dim I_parag_Tot As Long

Dim lastInstruction As instruction

Dim cblBytes As Integer
Dim indent As String
Dim isDSECT As Boolean
'SQ - Attenzione alla RAM... valutare...
Dim dataList(10000) As asmData
Dim listIndex As Integer
'tmp
Dim packedList(1000) As String
Dim packedIndex As Integer
Dim addedList(1000) As String
Dim missList(1000) As String
Dim missIndex As Integer
Dim addedIndex As Integer
'SQ - vedi EZT
Dim CurrentTag As String

Dim tagValues As Collection
Dim RTB As RichTextBox

''''''''''''''''''''''''
' 25-08-08
' Log sul pannello:
''''''''''''''''''''''''
Global lswLog As ListView
''''''''''''''''''''''''
' 28-08-08
' Operatore ASM:
''''''''''''''''''''''''
Public operator As String
Public Label_EX As Collection
Public Label_EXECUTE As Collection
Public Label_Def As Collection
Public Fields_Minus_Def As Collection

'silvia 4/9/2008
Public DFile As Collection
'''''''''''''''''
' collection per gestione template
'silvia 10/9/2008
Public Label_Temp As Collection

' Gestione "LABEL AUX"
'5-09-08
'''''''''''''''''
Public LabelAux As String
Public NameAux As String
'SQ 21-01-10
Dim missingStatus As Boolean
' Mauro 17/03/2010 : Lista dei Paragrafi per gestione BAL
Public arrCampi() As String
' Mauro 28/10/2010 : Lista delle Label per gestione "MOVEIT"
Public arrMoveItBAL() As String

''''''''''''''''''''''''''''''''''''''''''''''
' Attenzione alle macro!!!!!
' Andrebbero riconosciute...
' SQ 20-08-08:
' - Gestione LTR
' - Gestione Branch "Aritmetici"
' - Gestione CVD
' - Gestione BCT, BCTR
' - Gestione Commenti
''''''''''''''''''''''''''''''''''''''''''''''
Public Sub parseASM()
  Dim fdIN As Long
  Dim Line As String, token As String, statement As String
  Dim label As String, parameters As String
  Dim a As Integer, i As Integer
  Dim lastChar As Boolean
  
  On Error GoTo errASM
  
  '''''''''''''''''''''''''''''''''''''
  ' GESTIONE DATI/PARAGRAFI (parsing I)
  '''''''''''''''''''''''''''''''''''''
  If Asm2cobol Then parseDataASM
  
  '''''''''''''''''''''''''''''''''''''
  ' GESTIONE ISTRUZIONI (parsing II)
  '''''''''''''''''''''''''''''''''''''
  'init
  I_parag_Tot = I_parag
  GbNumRec = 0
  indent = Space(11)
  packedIndex = 0
  addedIndex = 0
  missIndex = 0
  I_parag = -1
  Set Label_EX = New Collection
  'silvia 10/9/2008
  Set Label_Temp = New Collection
  Set Fields_Minus_Def = New Collection
  LabelAux = ""
  NameAux = ""
  missingStatus = False
  ReDim arrMoveItBAL(0)
  ' Mauro 17/03/2010 : Carica Lista Paragrafi per Gestione BAL
  caricaListaCampi

  fdIN = FreeFile
  Open GbFileInput For Input As fdIN
     
  While Not EOF(fdIN)
    Line Input #fdIN, Line
    GbNumRec = GbNumRec + 1
    GbOldNumRec = GbNumRec
    If Left(Line, 1) <> "*" And Left(Line, 2) <> ".*" Then
      Line = Left(Line, 72)
      'SQ
      'statement = Left(line, 15) & RTrim(Mid(line, 16, 56))
      statement = RTrim(Left(Line, 15) & RTrim(Mid(Line, 16, 56)))

      'While Not EOF(fdIN) And ((Len(line) >= 72 And Right(line, 1) <> " ") Or Left(statement, 1) = "*")
      While Not EOF(fdIN) And (Len(Line) >= 72 And Right(Line, 1) <> " ")
        ' Mauro 04/12/2008 : Se la stringa continua alla riga successiva, a colonna 72 c'� un "*"
        lastChar = False
        If Len(Line) >= 72 Then
          If Mid(Line, 72, 1) = "*" Or _
             Mid(Line, 72, 1) = "-" Then
            lastChar = True
          End If
        End If
        Line Input #fdIN, Line
        GbNumRec = GbNumRec + 1
        Line = Left(Line, 72)
        'If Left(line, 1) <> "*" Then
        'I primi 15 byte devono essere space:
        If Left(Line, 15) = Space(15) Then
          'Byte 16 space: commento statement precedente!
          ' Mauro 04/12/2008 : Se la stringa continua alla riga successiva, a colonna 72 c'� un "*" o un "-"
          'If Mid(line, 16, 1) <> " " Then
          If Mid(Line, 16, 1) <> " " Or lastChar Then
            'statement = statement & Trim(Left(LTrim(line), InStr(LTrim(Left(line, 72)) & " ", " ")))
            statement = statement & " " & IIf(Asm2cobol, vbCrLf, "") & Trim(Mid(Line, 16, 56))
          Else
            writeLog "W", "##parseASM-TMP: " & GbIdOggetto & "TMP: Comment continuation!"
          End If
        Else
          writeLog "W", "##parseASM-TMP: " & GbIdOggetto & ": Syntax error on continuation line!"
        End If
      Wend
      'Eliminazione commenti finali (basta uno space dopo colonna 16 - non gestisco apici interni ai literals...)
      'a = InStr(16, statement & " ", " ") 'Aggiungo space in fondo per avere "a" significativo
      'apice?
    Else
      'SQ 28-08-08
      If Asm2cobol Then getComment Left(Line, 72)
      statement = ""
    End If

    '''''''''''''''''''''''''''''''''
    ' Analisi statement:
    '''''''''''''''''''''''''''''''''
    If Len(statement) Then
      ''''''''''''''''''''''''''''''
      ' Label
      ''''''''''''''''''''''''''''''
      If Left(statement, 1) <> " " Then
        label = nextToken_ASM(statement)
      'SQ 27-11-06
      Else
        label = ""
      End If
        
      operator = nextToken_ASM(statement)
      parameters = nextToken_ASM(statement) 'P.S.: tronca... farne uno dedicato!
      
      If Asm2cobol Then
        If Len(label) And operator <> "DC" And operator <> "DS" Then
          getLABEL label, parameters 'deve essere diversa dal nome del programma!
          'SQ lo fa il primo livello
          'setParagraph label
          I_parag = I_parag + 1
        ElseIf operator = "DS" And (parameters = "0H" Or parameters = "0F") Then
          getLABEL label, parameters 'deve essere diversa dal nome del programma!
          'SQ lo fa il primo livello
          'setParagraph label
          I_parag = I_parag + 1
        End If
      End If
      
      Select Case operator
        Case "C", "CDS", "CH", "CL", "CLC", "CLCL", "CLI", "CLM", "CLR", "CP", "CR", "CS"
          If Asm2cobol Then getCOMPARE parameters
        Case "CVD", "CVB"
          If Asm2cobol Then getCV parameters
        Case "B", "BAL", "BALR", "BC", "BCR", "BCT", "BCTR", "BE", "BER", "BH", "BHR", "BL", "BLR", "BM", "BMR", "BNE", _
             "BNER", "BNH", "BNHR", "BNL", "BNLR", "BNM", "BNMR", "BNO", "BNOR", "BNP", _
             "BNPR", "BNZ", "BNZR", "BNZR", "BO", "BOR", "BP", "BPR", "BR", "BZ", "BZR"
          If Asm2cobol Then getBRANCH parameters
        Case "DSECT", "CSECT"
          isDSECT = operator = "DSECT"
        Case "ENTRY"
          If Asm2cobol Then getENTRY parameters
        Case "EXEC", "EXECUTE" 'SQ 13-02-06,05-07-07
          If Not Asm2cobol Then
            Select Case parameters
              Case "DLI"
                'tmp:
                'MapsdM_COBOL.getEXEC_DLI statement
              Case "SQL"
                'tmp:
                'MapsdM_COBOL.getEXEC_SQL statement
              Case "CICS"
                'tmp:
                MapsdM_COBOL.getEXEC_CICS statement
            End Select
          Else
            If parameters = "CICS" Then
              If InStr(statement, vbCrLf) Then
                statement = Replace(statement, vbCrLf, vbCrLf & Space(Len(indent) + Len(operator) + Len(parameters) + 2))
              End If
              statement = operator & Space(Len(indent) - Len(operator)) & operator & " " & parameters & statement
              statement = statement & vbCrLf & Space(Len(indent)) & "END-EXEC"
              tagWrite statement
            End If
          End If
        Case "MVC", "MVCIN", "MVCL", "MVI", "MVN", "MVO", "MVZ"
          If Asm2cobol Then getMove parameters
        Case "ED", "EDMK"
          If Asm2cobol Then getEDIT parameters
        Case "EX"
          If Asm2cobol Then getEX parameters
        Case "OPEN"
          If Asm2cobol Then getOPEN parameters
        Case "CLOSE"
          If Asm2cobol Then getCLOSE parameters
        Case "PACK", "UNPK"
          If Asm2cobol Then getPACK_UNPK parameters
        Case "CALL"
          getCALL parameters
        Case "COPY"
          getCOPY parameters
        'parziale
        Case "L", "LA", "LCR", "LH", "LM", "LNR", "LPR", "LR", "LTR"
          If Asm2cobol Then getLOAD parameters
        'Parziale:
        Case "ST", "STC", "STCM", "STH", "STM"
          If Asm2cobol Then getSTORE parameters
        Case "IC", "ICM"
          If Asm2cobol Then getINSERT parameters
        Case "TM"
          If Asm2cobol Then getTM parameters
        Case "TR"
          If Asm2cobol Then getTR parameters
        Case "TRT"
          If Asm2cobol Then getTRT parameters
        Case "A", "AH", "AL", "ALR", "AP", "AR", _
             "S", "SH", "SL", "SLR", "SP", "SR", _
             "M", "MH", "MP", "MR", _
             "D", "DP", "DR"
          If Asm2cobol Then getARITHMETIC parameters
        Case "SLA", "SLDA", "SLDL", "SLL", "SRA", "SRDA", "SRDL", "SRL", "SRP"
          If Asm2cobol Then getSHIFT parameters
        Case "O", "OC", "OI", "OR", "X", "XC", "XI", "XR", "N", "NC", "NI", "NR"
          If Asm2cobol Then getLOGICAL parameters
        Case "GET", "PUT"
          If Asm2cobol Then getGET parameters
        Case "EQU"
          'SQ gestire...
          If Asm2cobol Then getEQU label, parameters
        Case "SVC"
          'SQ gestire...
          If Asm2cobol Then getSVC label, parameters
        Case "WTO"
          'SQ gestire...
          If Asm2cobol Then getWTO label, parameters
        Case "ZAP" ', "AP" : Ma non viene gestita nella getARITHMETIC questa?!?!?
          If Asm2cobol Then getZAP parameters
        Case "NOP", "CNOP", "NOPR"
          If Asm2cobol Then tagWrite "#" & operator & Space(6 - (Len(operator) + 1)) & "*" & "PARAM: " & parameters
        Case "RETUR", "RETURN" 'tmp: chiarire lunghezza nomi macro...
          If Asm2cobol Then getRETURN parameters
        Case "END"
          If Asm2cobol Then getEND label, parameters
        '''''''''''''''''''''''''''
        ' ISTRUZIONI NON GESTITE (con log...)
        '''''''''''''''''''''''''''
        Case "USING", "SAVE", "BAS", "BASM", "BASR", _
             "BASSM", "BSM", "BXH", "BXLE", "DROP", "LINK"
          If Asm2cobol Then tagWrite "#" & operator & Space(6 - (Len(operator) + 1)) & "*" & "PARAM: " & parameters
        '''''''''''''''''''''''''''
        ' ISTRUZIONI NON GESTITE (senza log...)
        '''''''''''''''''''''''''''
        Case "AMODE", "RMODE", "LTORG", "TITLE", "PRINT", "SPACE", "START", "EJECT", "SPACE" '...
          'NOP
        ''''''''''''''''''''
        ' MACRO:
        ''''''''''''''''''''
        Case "MACRO", "MEND", "MEXIT", "MNOTE", "AIF", "AO", "AGO", "ANOP", "AEJECT", "ASPACE", _
             "ACTR", "SET"  ',...
          'NON GESTITE...
        'Mauro 04/05/2010
        Case "LCLA", "LCLB", "LCLC"
          
        Case "GBLA", "GBLB", "GBLC"
        
        Case "SETA", "SETAF", "SETB", "SETC", "SETCF"
        
        ' MACRO
        Case "FREEMAIN", "DEVTYPE", "EXTRACT", "FREEPOOL", "CAMLST", "OBTAIN"
        '''''''''''''''''''''''
        ' Parse I
        '''''''''''''''''''''''
        Case "ORG", "DS", "DC", "ACB", "RPL", "DCB"
        '''''''''''''''''''''''
        Case Else
          ''''''''''
          ' MACRO
          ''''''''''
          Select Case operator
            Case "COPY"
              getCOPY parameters
            Case Else
              getMacro parameters
          End Select
          statement = ""
      End Select
    Else
      label = ""
      operator = ""
    End If
    'tmp
    lastInstruction.operator = operator
    ''''''''''''''''''''''''''''''''''
    ' Gestione LABEL "ausiliarie"
    ' -> inserimento LABEL-EX, ecc...
    ''''''''''''''''''''''''''''''''''
    checkLabelAux
  Wend
   
  GbNumRec = 0
  ' Mauro 25/08/2008
  For a = 0 To missIndex - 1
    getAddedName missList(a)
  Next a
  
  'TMP
  If Asm2cobol Then
    ' Mauro 05/09/2008: TMP!!!
    CurrentTag = "LABEL-DEFINITION"
    For i = 1 To Label_Def.count
      tagWrite Space(7) & "01 " & Label_Def.item(i) & " PIC X(4)."
    Next i
    ''tagWrite vbCrLf & vbCrLf & "*********************************"
    CurrentTag = "WORKING-STORAGE"
    For i = 0 To packedIndex - 1
      tagWrite packedList(i)
    Next i
    ' Mauro 12/009/2008: Gestione Campi con spiazzamento "-"
    For i = 1 To Fields_Minus_Def.count
      tagWrite Fields_Minus_Def.item(i)
    Next i
    'SILVIA 10/9/2008
    CurrentTag = "LABELS-TEMPLATE"
    For i = 1 To Label_Temp.count
      AddTemplate (Label_Temp.item(i))
    Next i
    ' Mauro 29/08/2008: Gestione Label mancanti(Per ora le mettiamo tutte in fondo)
    ''tagWrite vbCrLf & vbCrLf & "*********************************"
    'CurrentTag = "LABELS-EX"
    'For i = 1 To Label_EX.count
    '  tagWrite Space(7) & Label_EX(i) & "."
    'Next i
  End If
  
  Close fdIN
  ''Close fdOUT
  
  If Not Asm2cobol Then
    'SQ GESTIRE!!!!
    SwTpIMS = False
    SwDli = False
    SwSql = False
    SwBatch = False
    SwVsam = False
    SwMapping = False

    Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
    TbOggetti!DtParsing = Now
    TbOggetti!parsinglevel = 1
    TbOggetti!ErrLevel = GbErrLevel
    TbOggetti!Cics = SwTpCX
    TbOggetti!Ims = SwTpIMS
    TbOggetti!Batch = SwBatch
    TbOggetti!DLI = SwDli
    TbOggetti!WithSQL = SwSql
    TbOggetti!VSam = SwVsam
    TbOggetti!mapping = SwMapping
    TbOggetti.Update
    TbOggetti.Close
  End If
  Exit Sub
errASM:
  'tmp:
  If err.Number = 123 Then  'nextToken: gestire...
    'tmp: nextToken...
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "#parseASM: " & GbIdOggetto & "-" & err.description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
  End If
  writeLog "E", "#parseASM: " & GbIdOggetto & "-" & err.description
  Close fdIN
  ''Close fdOUT
End Sub

Private Sub parseDataASM()
  Dim fdIN As Long
  Dim Line As String, statement As String
  Dim label As String, parameters As String
  Dim dsLevel As Integer, dsLevBytes As Integer
  Dim dsORG As String
  Dim arrVar() As String, i As Integer, token As String
  Dim lastChar As Boolean
  
  On Error GoTo errASM

  'INIT
  I_parag_Tot = 0
  GbNumRec = 0
  dsLevel = 1
  indent = Space(11)
  listIndex = 0
  packedIndex = 0
  addedIndex = 0
  missIndex = 0
  I_parag = -1
  NameAux = ""
  ReDim arrVar(0)
  For i = 0 To 10000
    dataList(i).Name = ""
    dataList(i).dupFactor = ""
    dataList(i).Type = ""
    dataList(i).modifier = ""
    dataList(i).modifierValue = ""
    dataList(i).value = ""
    dataList(i).level = ""
  Next i
  Set Label_EX = New Collection
  Set Label_EXECUTE = New Collection
  Set Label_Def = New Collection
  'silvia 4/9/2008
  Set DFile = New Collection
  
  fdIN = FreeFile
  Open GbFileInput For Input As fdIN
     
  While Not EOF(fdIN)
    Line Input #fdIN, Line
    GbNumRec = GbNumRec + 1
    GbOldNumRec = GbNumRec
    If Left(Line, 1) <> "*" Then
      Line = Left(Line, 72)
      statement = Left(Line, 15) & RTrim(Mid(Line, 16, 56))
      'While Not EOF(fdIN) And ((Len(line) >= 72 And Right(line, 1) <> " ") Or Left(statement, 1) = "*")
      While Not EOF(fdIN) And (Len(Line) >= 72 And Right(Line, 1) <> " ")
        ' Mauro 04/12/2008 : Se la stringa continua alla riga successiva, a colonna 72 c'� un "*"
        lastChar = False
        If Len(Line) >= 72 Then
          If Mid(Line, 72, 1) = "*" Or Mid(Line, 72, 1) = "-" Then
            lastChar = True
          End If
        End If
        
        Line Input #fdIN, Line
        GbNumRec = GbNumRec + 1
        Line = Left(Line, 72)
        'If Left(line, 1) <> "*" Then
        'I primi 15 byte devono essere space:
        If Left(Line, 15) = Space(15) Then
          'Byte 16 space: commento statement precedente!
          ' Mauro 04/12/2008 : Se la stringa continua alla riga successiva, a colonna 72 c'� un "*" o "-"
          'If Mid(line, 16, 1) <> " " Then
          If Mid(Line, 16, 1) <> " " Or lastChar Then
            'statement = statement & Trim(Left(LTrim(line), InStr(LTrim(Left(line, 72)) & " ", " ")))
            statement = statement & RTrim(Mid(Line, 16, 56))
''          Else
''            writeLog "W", "##parseASM-TMP: " & GbIdOggetto & "TMP: Comment continuation!"
          End If
''        Else
''          writeLog "W", "##parseASM-TMP: " & GbIdOggetto & ": Syntax error on continuation line!"
        End If
      Wend
      'Eliminazione commenti finali (basta uno space dopo colonna 16 - non gestisco apici interni ai literals...)
      'a = InStr(16, statement & " ", " ") 'Aggiungo space in fondo per avere "a" significativo
      'apice?
''    Else
''      'SQ 28-08-08
''      If Asm2cobol Then getComment Left(line, 72)
''      statement = ""
    End If

    '''''''''''''''''''''''''''''''''
    ' Analisi statement:
    '''''''''''''''''''''''''''''''''
    If Len(statement) Then
      ''''''''''''''''''''''''''''''
      ' Label
      ''''''''''''''''''''''''''''''
      If Left(statement, 1) <> " " Then
        label = nextToken_ASM(statement)
      'SQ 27-11-06
      Else
        label = ""
      End If
        
      operator = nextToken_ASM(statement)
      parameters = nextToken_ASM(statement) 'P.S.: tronca... farne uno dedicato!
      
      If Asm2cobol Then
        If Len(label) And operator <> "DS" And operator <> "DC" Then
          setParagraph label
        ElseIf operator = "DS" And (parameters = "0H" Or parameters = "0F") Then
          setParagraph label
        End If
      End If
      
      Select Case operator
        Case "ORG"
          dsORG = parameters
        Case "DS", "DC"
          If Asm2cobol Or _
             (Asm2seqfiles And operator = "DC") Then
            If operator <> "DS" Or (parameters <> "0H" And parameters <> "0F") Then
              label = Replace(label, "_", "-")
  
              ReDim arrVar(0)
              token = nextToken_JCL(parameters)
              ReDim Preserve arrVar(UBound(arrVar) + 1)
              Do While parameters <> ""
                If token = "," Then
                  ReDim Preserve arrVar(UBound(arrVar) + 1)
                Else
                  arrVar(UBound(arrVar)) = arrVar(UBound(arrVar)) & token
                End If
                token = nextToken_JCL(parameters)
              Loop
              arrVar(UBound(arrVar)) = arrVar(UBound(arrVar)) & token
              If UBound(arrVar) > 1 Then
                If Len(label) Then
                  getData label, "0X", dsLevel, dsLevBytes, dsORG
                  NameAux = label
                  dsLevel = 1
                End If
                For i = 1 To UBound(arrVar)
                  getData "", arrVar(i), dsLevel, dsLevBytes, dsORG
                Next i
              Else
                getData label, arrVar(1), dsLevel, dsLevBytes, dsORG
              End If
''''          ElseIf Asm2seqfiles And operator = "DC" Then
''''            ' Mauro 08/02/2011
''''            Dim initStrng As Integer, endStrng As Integer
''''            Dim wStrng As String
''''            ' Cerco la stringa da concatenare per il file sequenziale
''''            token = nextToken_JCL(parameters)
''''            Select Case token
''''              Case "C", "X"
''''                initStrng = InStr(parameters, "'")
''''                endStrng = InStr(initStrng + 1, parameters, "'")
''''                If initStrng > 0 And endStrng > 0 Then
''''                  wStrng = Mid(parameters, initStrng + 1, endStrng - initStrng - 1)
''''                Else
''''                  wStrng = ""
''''                End If
''''                RTB.Text = RTB.Text & wStrng
''''              Case "H"
''''              Case "F"
''''              Case Else
''''            End Select
            End If
          End If
        Case "EX"
          If Asm2cobol Then initEX parameters
          operator = ""
        'silvia 4/9/2008
        Case "ACB", "RPL", "DCB"
          If Asm2cobol Then getFileDefinition label, parameters
        Case Else
          'nop
      End Select
      operator = ""
    Else
      label = ""
      operator = ""
    End If
    'tmp
    lastInstruction.operator = operator
  Wend
     
  Close fdIN
  Exit Sub
errASM:
  'tmp:
  If err.Number = 123 Then  'nextToken: gestire...
    'tmp: nextToken...
    'super tmp!!!
    parameters = Mid(parameters, 2)
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "#parseASM: " & GbIdOggetto & "-" & err.description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
  End If
  writeLog "E", "#parseASM: " & GbIdOggetto & "-" & err.description
  Close fdIN
  ''Close fdOUT
End Sub
''''''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''''''
Private Sub getCALL(parameters As String)
  Dim module As String
  Dim arguments As String
  Dim statement As String
  
  On Error GoTo nextTokenErr
  
  'Called Module
  SwNomeCall = True
  module = nextToken_tmp(parameters)
  
  ' Mauro 14/10/2009: TEMP!!!!!!!!!!!!! Manca il conteggio delle linee e la gestione dei parametri
  '''''''''''''''''''''''''''''''''''''''''''''''''
  ' Inserimento istruzione in PsCall: (trasformare in execute INSERT)
  '''''''''''''''''''''''''''''''''''''''''''''''''
  'SQ tmp!!
  If Not Asm2cobol Then
  Dim rsCall As Recordset
  Set rsCall = m_Fun.Open_Recordset("select * from PsCall")
  rsCall.AddNew
  rsCall!idOggetto = GbIdOggetto
  rsCall!Riga = GbOldNumRec
  rsCall!programma = module  'attenzione: puo' contenere apici...
  rsCall!parametri = Replace(Replace(nexttoken(parameters), ")", ""), "(", "")
  rsCall!linee = 1
  rsCall.Update
  rsCall.Close
 

  'ASSESSMENT:
  ''''''''''''''''''''''''''''''''''''''
  ' Relazioni/Segnalazioni
  ''''''''''''''''''''''''''''''''''''''
  MapsdM_Parser.checkRelations module, "CALL"
End If 'SQ tmp
  
  
  If Not Asm2cobol Then Exit Sub
  
  ''''''''''''''
  'MIGRAZIONE:
  ''''''''''''''
  statement = indent & "CALL '" & module & "'"
  
  'Parameters
  If Len(parameters) Then
    arguments = Replace(Replace(nexttoken(parameters), ")", ""), "(", "")
    'statement = statement & " USING" & vbCrLf & indent & "     " & arguments
    statement = statement & " USING " & arguments
  End If
    
  tagWrite statement
  
  Exit Sub
nextTokenErr:
  'tmp
  MsgBox err.description
End Sub
''''''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''''''
Private Sub getCOPY(parameters As String)
  
  'ASSESSMENT...
  MapsdM_Parser.checkRelations parameters, "COPY-ASM"
  
  If Not Asm2cobol Then Exit Sub
  
  'MIGRAZIONE
  tagWrite indent & "COPY " & parameters & "."
  
End Sub
''''''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''''''
Private Sub getMacro(parameters As String)
  Dim token As String, statement As String, attributes() As String, macroName As String, relType As String
    
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Gestione "vecchie" macro di sistema.
  ' Gestire bene con i "system object"!!!!!!!!
  ' - DFHPC (CICS macro)
  '    TYPE=LINK
  '    TYPE=XCTL
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Select Case operator
    Case "DFHPC"
      statement = parameters
      While Len(statement)
        token = nextToken_tmp(statement)
        attributes = Split(token, "=")
        If attributes(0) = "TYPE" Then
          relType = attributes(1)
        ElseIf attributes(0) = "PROGRAM" Then
          macroName = attributes(1)
        End If
      Wend
    Case Else
      macroName = operator
      relType = "MAC"
  End Select
  
  'ASSESSMENT:
  SwNomeCall = True
  MapsdM_Parser.checkRelations macroName, relType
  
  If Not Asm2cobol Then Exit Sub
  
  'MIGRAZIONE
  'FARE SELECT CASE
  If operator <> "MODE" Then
    tagWrite "*MACRO*" & operator & " #" & parameters & "#"
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 20-08-08:
' LTR: Load and Test Register [12] R1,R2
' LA:  Load Address           [41] R1,D2(X2,B2)
' L:   Load                   [58] R1,D2(X2,B2)
' LH:  Load Halfword          [48] R1,D2(X2,B2)
' LR:  Load Register          [18] R1,R2
' LCR,LM,LNR,LPR : fare...
''
'SQ 21-01-10: separti L,LH e LA... ma ancora da gestire bene...
'''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getLOAD(parameters As String)
  Dim param As String
  Dim loperand As operand, roperand As operand
  Dim statement As String
  Dim cblFrom As String, cblTo As String
  Dim groupField As String, disp As Integer
  
  param = parameters
  
  Select Case operator
    Case "LA"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'LA: The actual value of operand-2 (x2+b2+d2) is loaded into the register specified by R1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' a) R1,6:            MOVE 6 TO I-1
      ' b) R1,6(R1)         COMPUTE R1 = R1 + 6
      ' c) R1,FIELD1        a) "virtual association R1:FIELD1"
      '                     b) MOVE 0 TO I-1
      ' d) R1,FIELD1+6      a) "virtual association R1:FIELD1"
      '                     b) MOVE 6 TO I-1
      ' e) R1,FIELD1-6       ahaa
      ' f) R1,FIELD1(R1)    FARE!!!
      ' g) R1,R2  (NO!!)    a) "virtual association R1:FIELD<-R2"
      '                     b) MOVE I-2 TO I-1 (verificare!)
      ' e) R1,=F'6'  => FINIRE DI GESTIRE! (non confondere con "a")!
      '
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      cblTo = "I-" & Replace(loperand.value, "_", "-")
      If Len(roperand.base) = 0 Then
        If Len(roperand.value) Then
          'e) - IMMEDIATE: verificare...
          cblFrom = Replace(roperand.value, "_", "-")
          'SQ 2.0 TMP: era sotto... portato nelle singole IF e modificato solo dove sicuro...
          statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
          Dim isTrovato As Boolean, i As Integer
          isTrovato = False
          For i = 0 To listIndex
            If dataList(i).Name = cblFrom Then
              isTrovato = True
              Exit For
            End If
          Next i
          If Not isTrovato Then
            getData cblFrom, "CL" & roperand.len, 1, 0, ""
          End If
        Else
          'a),c),d)
          If IsNumeric(roperand.Displace) Then
            'a)
            cblFrom = Replace(roperand.Displace, "_", "-")
            statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
          Else
            'SQ 2.0
            'tagWrite "LA!   *" & loperand.value & " -> " & roperand.displace
            'setRegisters loperand.value, roperand.displace
            'c),d),e)
            cblFrom = Replace(roperand.Displace, "_", "-")
            If InStr(cblFrom, ".") Then
              groupField = Left(cblFrom, InStr(cblFrom, ".") - 1)
              cblFrom = Mid(cblFrom, InStr(cblFrom, ".") + 1)
              If InStr(cblFrom, ")") Then
                disp = InStr(cblFrom, "(")
                cblFrom = Left(cblFrom, disp - 1) & " OF " & groupField & Mid(cblFrom, disp)
              Else
                cblFrom = cblFrom & " OF " & groupField
              End If
            End If
            cblTo = "PTR-" & Replace(loperand.value, "_", "-")
            statement = operator & Space(Len(indent) - Len(operator)) & "SET " & cblTo & " TO ADDRESS OF " & cblFrom
            'colonna 72: parametri originali
            statement = statement & Space(72 - Len(statement)) & param
            If Len(roperand.displacePlus) Then
              'd)
              'spiazzamento:
              statement = statement & vbCrLf & _
                          operator & Space(Len(indent) - Len(operator)) & "ADD " & Replace(roperand.displacePlus, "_", "-") & _
                          " TO " & "I-" & Replace(loperand.value, "_", "-")
            End If
          End If
        End If

      Else
        If IsNumeric(roperand.Displace) Then  'in realt� serve un "isRegister" per gestire gli "EQU"...
          'b)
          'SQ 2.0
          ''PASSAGGIO PUNTAMENTO...
          ''If roperand.base <> loperand.value Then
          ''  cblFrom = getRegisters(roperand.base)
          ''  tagWrite "LA!   *" & loperand.value & " -> " & cblFrom
          ''  setRegisters loperand.value, cblFrom
          ''End If
          ''statement = operator & Space(Len(indent) - Len(operator)) & "COMPUTE " & cblTo & " = I-" & roperand.base & " + " & roperand.displace
          statement = operator & Space(Len(indent) - Len(operator)) & "COMPUTE " & cblTo & " = " & IIf(Len(roperand.Index), "I-" & roperand.Index & " + ", "") & "I-" & roperand.base & " + " & roperand.Displace
        Else
          'f)
          'SQ 2.0
          'tagWrite "LA!   *" & loperand.value & " -> " & roperand.displace
          'setRegisters loperand.value, roperand.displace
          statement = operator & Space(Len(indent) - Len(operator)) & "ADD " & roperand.Displace & " TO " & cblTo
        End If
        
        'colonna 72: parametri originali
        statement = statement & Space(72 - Len(statement)) & param
      End If
      
      tagWrite statement
    Case "L", "LH"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'L: The four bytes at the storage address specified by operand-2 (x2+b2+d2) is loaded into the register specified by operand-1 (r1).
      'LH: The two bytes (halfword) located at the storage address specified by operand-2 (x2+b2+d2) is loaded into the rightmost two bytes (bits 16-31) of the register specified by operand-1 (r1).
      '!!!!!!!!!!!!!!!!!gestire: the left-most 16 bit contengono il segno!!!!!!!!!!!!!!!
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      ' Mauro 17/03/2010 : Nuova Gestione per "Branch And Link(BAL)"
      If isParagraph(roperand.Displace) And operator = "L" Then
        ' Gestione post-BAL
        cblTo = "BR-LABEL(" & loperand.value & ")"
        cblFrom = getOperand_CBL(roperand, True) & "-L"
        
        statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
        statement = statement & Space(72 - Len(statement)) & param
      Else
        ' Gestione Classica
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' a) R1,6:            MOVE 6 TO I-1
        ' b) R1,6(R1)         COMPUTE R1 = R1 + 6
        ' c) R1,FIELD1        a) "virtual association R1:FIELD1"
        '                     b) MOVE 0 TO I-1
        ' d) R1,FIELD1+6      a) "virtual association R1:FIELD1"
        '                     b) MOVE 6 TO I-1
        ' e) R1,FIELD1-6       ahaa
        ' f) R1,FIELD1(R1)    FARE!!!
        ' g) R1,R2  (NO!!)    a) "virtual association R1:FIELD<-R2"
        '                     b) MOVE I-2 TO I-1 (verificare!)
        ' e) R1,=F'6'  => FINIRE DI GESTIRE! (non confondere con "a")!
        '
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        cblTo = "R-" & loperand.value & IIf(operator = "L", "(1:4)", "(3:2)")
        cblFrom = getOperand_CBL(roperand, True)
        
        'SQ tmp: left most 16bit: segno!
        If operator = "LH" Then
          cblFrom = IIf(Right(cblFrom, 2) = ":)", Replace(cblFrom, ":)", ":2)"), cblFrom & "(1:2)")
          statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
          statement = statement & Space(72 - Len(statement)) & param
          statement = statement & vbCrLf & Space(Len(indent)) & "MOVE LOW-VALUE TO " & Replace(cblTo, "(3:2)", "(1:2)")
        Else
          cblFrom = IIf(Right(cblFrom, 2) = ":)", Replace(cblFrom, ":)", ":4)"), cblFrom & "(1:4)")
          statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
          statement = statement & Space(72 - Len(statement)) & param
        End If      'colonna 72: parametri originali
            
        'SQ 2.0
        If roperand.iType = "REG" Then
          statement = operator & Space(Len(indent) - Len(operator)) & "SET ADDRESS OF AREA-" & roperand.base & " TO PTR-" & roperand.base & vbCrLf & statement
        End If
      End If
      tagWrite statement
    Case "LR", "LTR"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'Operand-2 (r2) is put into operand-1 (r1). The sign and magnitude of operand-2 (r2),
      'treated as a 32-bit signed binary integer are indicated in the condition code.
      ''
      ' MOVE I-2 TO I-1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "R2"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'Load:
      statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & getOperand_CBL(roperand) & " TO " & getOperand_CBL(loperand) & vbCrLf
      
      If operator = "LTR" Then
        'Test:
        statement = statement & indent & "IF " & getOperand_CBL(loperand) & " = 0 MOVE 0 TO COND-CODE" & vbCrLf & indent & "ELSE IF " & getOperand_CBL(loperand) & " < 0 MOVE 1 TO COND-CODE" & vbCrLf & indent & "ELSE MOVE 2 TO COND-CODE."
      End If
      
      tagWrite statement
    Case "LM"
      'SQ 16-03-10 - tmp... completare
      'R1,R3,D2(B2)

      Dim moperand As asmData
      Dim midOperand As OptionButton
      Dim mmoperand As operand
      loperand.format = "R1"
      mmoperand.format = "R1"
      roperand.format = "D2(X2,B2)"
    
      getOperand parameters, loperand
      
      getOperand parameters, mmoperand
      getOperand parameters, roperand
      
      cblTo = "R-" & loperand.value & "(1:4)"
      cblFrom = getOperand_CBL(roperand, True)
      
      cblFrom = IIf(Right(cblFrom, 2) = ":)", Replace(cblFrom, ":)", ":4)"), cblFrom & "(1:4)")
      'SQ brutta pezza...
      cblFrom = Replace(cblFrom, "(1:4)(1:4)", "(1:4)")
      
      statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
      statement = statement & Space(72 - Len(statement)) & param
     '*
      loperand = mmoperand
      cblTo = "R-" & loperand.value & "(1:4)"
      cblFrom = Replace(cblFrom, "(1:4)", "(5:4)")
      
      statement = statement & vbCrLf & operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
      
      tagWrite statement
    Case "LCR", "LNR", "LPR"
      'tmp - per ora mi serve per il "BR"... EXIT ROUTINE!
      lastInstruction.loperand.Displace = nextToken_tmp(parameters)
      lastInstruction.roperand.Displace = parameters
      lastInstruction.operator = operator
  
      tagWrite "#" & operator & ":" & Space(6 - Len(operator) - 2) & "*" & "PARAM: " & param
  End Select
  
  'statement=...
  'tagwrite statement
End Sub
''''''''''''''''''''''''''''''''''''''
' Scrittura LABEL
' - gestione (parziale) "EXECUTE"
''''''''''''''''''''''''''''''''''''''
Private Sub getLABEL(label As String, operators As String)
  Dim i As Long
  
  label = Replace(label, "_", "-")
  If operator <> "DCB" And operator <> "ACB" And operator <> "RPL" And _
    operator <> "RMODE" And operator <> "AMODE" Then
      If operator <> "EQU" Or operators = "*" Then
        tagWrite Space(11) & "." & vbCrLf & _
                 Space(6) & "*--------" & vbCrLf & _
                 Space(7) & label & IIf(operator = "START" Or operator = "CSECT", "-" & operator, "") & "." & vbCrLf & _
                 Space(6) & "*--------"
        'SQ tmp:
        'tagWrite Space(11) & "DISPLAY '- " & label & "'"
        ' Cercare la Label nella collection x commento
        For i = 1 To Label_EXECUTE.count
          If Label_EXECUTE.item(i) = label Then
            'SQ 24-03-10
            'Gestione "OR" dei parametri:
            Dim tmpOperator As String
            Dim Tag As String
            Tag = "L-EX!"
            tagWrite Tag & Space(11 - Len(Tag)) & "CALL 'CBL_OR' USING X'<EX-value>',X-EX BY VALUE 1"
            
            'Gestione LABEL AUX
            LabelAux = label & "-EX"
            Exit For
          End If
        Next i
    End If
  End If
End Sub
''''''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''''''
'silvia 10/9/2008
Private Sub getOPEN(parameters As String)
  'ASSESSMENT...
  Dim statement As String, token As String
  Dim param As String
  parameters = Replace(Replace(parameters, ")", ""), "(", "")
  
  While Len(parameters)
    token = nextToken_tmp(parameters)
    param = nextToken_tmp(parameters)
    Select Case param
      Case "OUTPUT", "INPUT"
        statement = statement & indent & "OPEN " & param & " " & token
      Case ""
        statement = statement & indent & "OPEN INPUT" & " " & token
      Case "INOUT"
        statement = statement & indent & "OPEN I-O" & " " & token
    End Select
    statement = statement & vbCrLf
  Wend
  
  tagWrite Left(statement, Len(statement) - 2)
End Sub
''''''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''''''
Private Sub getCLOSE(parameters As String)
  'ASSESSMENT...
  tagWrite indent & "CLOSE " & Replace(Replace(parameters, ")", ""), "(", "")
End Sub
''''''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''''''
Private Sub getENTRY(parameters As String)
  Dim param() As String, i As Integer
  
  param() = Split(parameters, ",")
  For i = 0 To UBound(param)
    tagWrite indent & "ENTRY '" & param(i) & "'"
  Next i
End Sub
''''''''''''''''''''''''''''''''''''''
' SQ 28-08-08
' - Gestiti gli "ARITMETHIC"
' - BCT:  [46] R1,D2((X2,B2)
' - BCTR: [06] R1,R2
' (accorpare le parti comuni)...
''''''''''''''''''''''''''''''''''''''
Private Sub getBRANCH(parameters As String)
  Dim statement As String
  Dim condition As String, conditionField As String
  Dim loperand As operand, roperand As operand
  Dim param As String, label As String
  Dim token As String
  Dim i As Integer, isTrovato As Boolean, cont As Integer
  
  param = parameters
  
  'SQ tmp: le istruzioni aritmetiche non settano lo status... quindi gestione "warning"
  If missingStatus Then
    If Left(operator, 3) <> "BCT" Then
      writeLog "W", "Previous Aritmethical instruction without status setting. "
    End If
    missingStatus = False
  End If
  Select Case operator
    ''''''''''''''''''''''''''''''''''''
    ' UNCONDITIONAL Branch
    ''''''''''''''''''''''''''''''''''''
    Case "B"
      statement = IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & IIf(Len(conditionField), "   ", "") & "GO TO " & Replace(getLabelEx(parameters), "_", "-") & IIf(Len(conditionField), ".", "")
      roperand.format = "D2(X2,B2)"
      getOperand parameters, roperand
             
      If roperand.iType = "REG" Then
        label = getRegisters(roperand.base)
        If Len(label) Then
          If roperand.Displace Then
            'caso: 2(R3)
            ' => scamuffo... verificare
            label = label & "+" & roperand.Displace
          End If
          'statement = IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & IIf(Len(conditionField), "   ", "") & "GO TO " & getLabelEx(label) & IIf(Len(conditionField), ".", "")
          statement = "GO TO " & Replace(getLabelEx(label), "_", "-")
        Else
          writeLog "W", "tmp: to be completed..."
        End If
        'Linea vuota
        'statement = statement & vbCrLf & Space(6) & "*" & vbCrLf
      Else
        label = roperand.Displace
        'statement = IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & IIf(Len(conditionField), "   ", "") & "GO TO " & getLabelEx(label) & IIf(Len(conditionField), ".", "")
        statement = "GO TO " & Replace(getLabelEx(label), "_", "-")
      End If
      'Stampa indentata/taggata
      statement = operator & Space(Len(indent) - Len(operator)) & statement
      tagWrite statement & Space(72 - Len(statement)) & param
    Case "BC", "BCR"
      loperand.format = "R1"
      roperand.format = IIf(operator = "BC", "D2(X2,B2)", "R2")
      
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      '''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' MOVE R1 TO REV-MASK
      ' CALL 'CBL_AND' USING COND-CODE,REV-MASK BY VALUE 2
      ' IF REV-MASK > ZERO
      '    GO TO <operand2>
      '''''''''''''''''''''''''''''''''''''''''''''''''''''
      'forse qualche controllino ci vorrebbe!...
      statement = "MOVE " & loperand.value & " TO REV-MASK"
      statement = operator & Space(Len(indent) - Len(operator)) & statement
      statement = statement & Space(72 - Len(statement)) & param & vbCrLf
      statement = statement & indent & "CALL 'CBL_AND' USING COND-CODE,REV-MASK BY VALUE 2" & vbCrLf
      statement = statement & indent & "IF REV-MASK > ZERO" & vbCrLf
      If operator = "BCR" Then
        label = getRegisters(roperand.value)
      Else
        label = roperand.Displace
      End If
      statement = statement & indent & "   GO TO " & label & "."
      
      tagWrite statement
    Case "BR"
      roperand.format = "R2"
      
      getOperand parameters, roperand
      '''''''''''''''''''''''''''''''''''''''
      ' Recupero label e chiusura paragrafo!
      '''''''''''''''''''''''''''''''''''''''
      'SQ tmp
      If False And lastInstruction.operator = "L" And lastInstruction.loperand.Displace = parameters Then
        If Right(lastInstruction.roperand.Displace, 2) = "-4" Then
          tagWrite Space(11) & "." & vbCrLf & _
                  "BR     " & Replace(Replace(lastInstruction.roperand.Displace, "-4", "-EX."), "_", "-")
          Exit Sub
        End If
      Else
        'SQ 2.1 - isSafe = true
        label = Replace(getRegisters(roperand.value, True), "_", "-")
        If Len(label) Then
          'Punto di chiusura
          statement = Space(11) & "." & vbCrLf
          'Label di chiusura
          statement = statement & "BR     " & label & "-EX."
        Else
          'BR 14 => GOBACK!
          If roperand.value = 14 Then
            statement = "BR     " & "    GOBACK."
            statement = statement & Space(72 - Len(statement)) & param & vbCrLf
            'meglio che l'utente verifichi!
            writeLog "I", "'BR 14' replaced with 'GOBACK'"
          'SQ 2.1
          Else
            statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & roperand.value & " TO BR-IDX"
            statement = statement & Space(72 - Len(statement)) & param
            statement = statement & vbCrLf & operator & Space(Len(indent) - Len(operator)) & "PERFORM MANAGE-BRANCH."
            statement = statement & vbCrLf & operator & Space(7 - Len(operator)) & Replace(Paragraphs(I_parag).Name, "_", "-") & "-EX." & vbCrLf
          End If

        End If
        'Linea vuota
        statement = statement & vbCrLf & Space(6) & "*" & vbCrLf
        tagWrite statement
      End If
    ''''''''''''''''''''''''''''''''''''
    ' Branch on CounT
    ''''''''''''''''''''''''''''''''''''
    Case "BCT"
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
  
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      ''''''''''''''''''''''''''''''''''''''
      ' SUBTRACT 1 FROM I-<R1>
      ' IF I-1 = ZERO
      '    GO TO <D2(X2,B2)>
      ''''''''''''''''''''''''''''''''''''''
      conditionField = "I-" & loperand.value
      condition = "> ZERO"
      'TMP: completare
      parameters = roperand.Displace
      
      statement = "SUBTRACT 1 FROM I-" & loperand.value
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(indent) - Len(statement)) & param & vbCrLf
      
      statement = statement & IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & _
      IIf(Len(conditionField), "   ", "") & "GO TO " & Replace(getLabelEx(parameters), "_", "-") & IIf(Len(conditionField), ".", "")
       
      'Stampa indentata/taggata
      tagWrite Replace(operator & Space(Len(indent) - Len(operator)) & statement, vbCrLf, vbCrLf & operator & Space(Len(indent) - Len(operator)))

    ''''''''''''''''''''''''''''''''''''
    ' Branch on CounT Register
    ''''''''''''''''''''''''''''''''''''
    Case "BCTR"
      loperand.format = "R1"
      roperand.format = "R2"

      getOperand parameters, loperand
      getOperand parameters, roperand
      
      ''''''''''''''''''''''''''''''''''''''
      ' SUBTRACT 1 FROM I-<R1>
      ' IF I-1 = ZERO
      '    GO TO <R2>
      ''''''''''''''''''''''''''''''''''''''
      statement = "SUBTRACT 1 FROM I-" & loperand.value
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(indent) - Len(statement)) & param
      
      If roperand.value > 0 Then
        conditionField = "I-" & loperand.value
        condition = "> ZERO"
        'TMP: completare
        parameters = roperand.Displace
        
        statement = statement & vbCrLf
        
        statement = statement & IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & _
        IIf(Len(conditionField), "   ", "") & "GO TO " & Replace(getLabelEx(parameters), "_", "-") & IIf(Len(conditionField), ".", "")
      Else
        'If R2 is zero then R1 is decremented but a branch is never taken
      End If
       
      'Stampa indentata/taggata
      tagWrite Replace(operator & Space(Len(indent) - Len(operator)) & statement, vbCrLf, vbCrLf & operator & Space(Len(indent) - Len(operator)))
    ''''''''''''''''''''''''''''''''''''
    ' Branch After COMPARE Instruction
    ''''''''''''''''''''''''''''''''''''
    Case "BH", "BHR", "BL", "BLR", "BE", "BER", "BNH", "BNHR", "BNL", "BNLR", "BNE", "BNER"
      conditionField = Replace(Replace(operator, "B", "CC-"), "R", "")
      If InStr(conditionField, "N") Then
        conditionField = "NOT " & Replace(conditionField, "N", "")
      End If
      
      statement = IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & _
      IIf(Len(conditionField), "   ", "") & "GO TO " & Replace(getLabelEx(parameters), "_", "-") & IIf(Len(conditionField), ".", "")
       
      'Stampa indentata/taggata
      tagWrite Replace(operator & Space(Len(indent) - Len(operator)) & statement, vbCrLf, vbCrLf & operator & Space(Len(indent) - Len(operator)))

    ''''''''''''''''''''''''''''''''''''
    ' Branch After ARITHMETIC Instruction
    ''''''''''''''''''''''''''''''''''''
    Case "BM", "BMR", "BNM", "BNMR", "BNO", "BNOR", "BNP", "BNPR", "BNZ", "BNZR", "BNZR", "BO", "BOR", "BP", "BPR", "BZ", "BZR"
      conditionField = Replace(Replace(operator, "B", "CC-"), "R", "")
      
      If InStr(conditionField, "N") Then
        conditionField = "NOT " & Replace(conditionField, "N", "")
      End If
      
      roperand.format = "D2(X2,B2)"
      getOperand parameters, roperand

      If roperand.iType = "REG" Then
        label = getRegisters(roperand.base)
        If Len(label) Then
          If roperand.Displace Then
            'caso: 2(R3)
            ' => scamuffo... verificare
            label = label & "+" & roperand.Displace
          End If
          'statement = IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & IIf(Len(conditionField), "   ", "") & "GO TO " & getLabelEx(label) & IIf(Len(conditionField), ".", "")
          label = "GO TO " & Replace(getLabelEx(label), "_", "-")
        Else
          MsgBox "tmp: to complete..."
        End If
        'Linea vuota
        'statement = statement & vbCrLf & Space(6) & "*" & vbCrLf
      Else
        label = roperand.Displace
        'statement = IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & IIf(Len(conditionField), "   ", "") & "GO TO " & getLabelEx(label) & IIf(Len(conditionField), ".", "")
        label = "GO TO " & Replace(getLabelEx(label), "_", "-")
      End If
      
      statement = IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & _
                  IIf(Len(conditionField), "   ", "") & label & IIf(Len(conditionField), ".", "")
       
      'Stampa indentata/taggata
      tagWrite Replace(operator & Space(Len(indent) - Len(operator)) & statement, vbCrLf, vbCrLf & operator & Space(Len(indent) - Len(operator)))
      
''      roperand.format = "D2(X2,B2)"
''      getOperand parameters, roperand
''
''      If roperand.iType = "REG" Then
''        label = getRegisters(roperand.base)
''        If Len(label) Then
''          If roperand.displace Then
''            'caso: 2(R3)
''            ' => scamuffo... verificare
''            label = label & "+" & roperand.displace
''          End If
''          'statement = IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & IIf(Len(conditionField), "   ", "") & "GO TO " & getLabelEx(label) & IIf(Len(conditionField), ".", "")
''          label = "GO TO " & getLabelEx(label)
''        Else
''          MsgBox "tmp: to complete..."
''        End If
''        'Linea vuota
''        'statement = statement & vbCrLf & Space(6) & "*" & vbCrLf
''      Else
''        label = roperand.displace
''        'statement = IIf(Len(conditionField), "IF " & conditionField & " " & condition & vbCrLf, "") & IIf(Len(conditionField), "   ", "") & "GO TO " & getLabelEx(label) & IIf(Len(conditionField), ".", "")
''        label = "GO TO " & getLabelEx(label)
''      End If
''      'Stampa indentata/taggata
''      statement = operator & Space(Len(indent) - Len(operator)) & statement
''      tagWrite statement & Space(72 - Len(statement)) & param
    Case "BAL"
      '''''''''''''''''''''''''
      'Branch And Link
      '''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"

      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'Link: associazione registro<->label
      setRegisters loperand.value, roperand.Displace
      
      ' Mauro 28/10/2010
      isTrovato = False
      cont = 0
      Paragraphs(I_parag).Name = Replace(Paragraphs(I_parag).Name, "_", "-")
      For i = 0 To UBound(arrMoveItBAL)
        If Left(arrMoveItBAL(i), Len(Paragraphs(I_parag).Name)) = Paragraphs(I_parag).Name Then
          cont = cont + 1
        End If
      Next i
      ReDim Preserve arrMoveItBAL(UBound(arrMoveItBAL) + 1)
      arrMoveItBAL(UBound(arrMoveItBAL)) = Paragraphs(I_parag).Name & IIf(cont = 0, "", "-" & cont)
      
      'SQ 2.1
      statement = operator & Space(Len(indent) - Len(operator)) & "MOVE '" & Paragraphs(I_parag).Name & IIf(cont = 0, "", "-" & cont) & "' TO BR-LABEL(" & loperand.value & ")"
      statement = statement & Space(72 - Len(statement)) & param & vbCrLf
      'Branch
      roperand.Displace = Replace(roperand.Displace, "_", "-")
      statement = statement & operator & Space(Len(indent) - Len(operator)) & "PERFORM " & roperand.Displace & " THRU " & roperand.Displace & "-EX." & vbCrLf
      'SQ 2.1
      statement = statement & operator & Space(7 - Len(operator)) & Paragraphs(I_parag).Name & IIf(cont = 0, "", "-" & cont) & "-EX."
      On Error Resume Next
      Label_EX.Add roperand.Displace & "-EX", roperand.Displace & "-EX"
      On Error GoTo 0
      tagWrite statement
    Case "BALR"
      '''''''''''''''''''''''''
      'Branch And Link Register
      '''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "R2"

      getOperand parameters, loperand
      getOperand parameters, roperand
      
      If roperand.value = 0 Then
        'When the operand 2 value is zero, the link information is loaded without branching
        statement = "MOVE ZERO TO I-" & loperand.value
        statement = operator & Space(Len(indent) - Len(operator)) & statement
        statement = statement & Space(72 - Len(statement)) & param & vbCrLf
      Else
        statement = "PERFORM " & roperand.value & " THRU " & roperand.value & "-EX"
        statement = operator & Space(Len(indent) - Len(operator)) & statement
        statement = statement & Space(72 - Len(statement)) & param & vbCrLf
      End If
      
      statement = "#BALR *PARAM: " & param
      tagWrite statement
  End Select
End Sub

' Mauro 29/08/2008: Ricerca l'indirizzamento della GO TO
Private Function getLabelEx(parameters As String) As String
  Dim Segno As Integer
  Dim labelEx As String
  
  On Error Resume Next ' Mi serve per evitare duplicati nella collection
  Segno = InStr(parameters, "+")
  If Segno = 0 Then
    Segno = InStr(parameters, "-")
  End If
  
  If Segno Then
    labelEx = Paragraphs(I_parag).Name & "-P" & Mid(parameters, Segno + 1)
    Label_EX.Add labelEx, labelEx
  Else
    If Right(operator, 1) = "R" Then
      labelEx = Paragraphs(I_parag).Name & "-EX"
      Label_EX.Add labelEx, labelEx
    Else
      labelEx = parameters
    End If
  End If
  If UCase(labelEx) = "RETURN" Then
    labelEx = labelEx & "-P"
  End If
  getLabelEx = labelEx
End Function
''''''''''''''''''''''''''''''''''''''
' silvia 3/09/2008
''''''''''''''''''''''''''''''''''''''
Private Sub getFileDefinition(label As String, parameters As String)
  Dim asmdcb As Dcb
  Dim spparam() As String, attributes() As String
  Dim j As Integer
  Dim previousTag As String
  Dim statement As String
   
  previousTag = CurrentTag
  spparam = Split(parameters, ",")
  asmdcb.LRec = 80
  asmdcb.label = label
  For j = 0 To UBound(spparam)
    attributes = Split(spparam(j), "=")
    Select Case attributes(0)
      Case "DDNAME"
         asmdcb.ddname = attributes(1)
      Case "ACB"
         asmdcb.dcbname = attributes(1)
      Case "LREC", "AREALEN"
         asmdcb.LRec = attributes(1)
      Case "EODAD"
         asmdcb.Eodad = attributes(1)
    End Select
  Next
  
  Select Case operator
    Case "DCB"
      asmdcb.FRec = label & "-REC"
      asmdcb.dcbname = label
      CurrentTag = "ASSIGN"
      statement = operator & Space(7 - Len(operator)) & "SELECT " & asmdcb.dcbname & " ASSIGN TO " & asmdcb.ddname
      statement = statement & Space(72 - Len(statement)) & parameters & vbCrLf
      statement = statement & indent & "ORGANIZATION IS SEQUENTIAL " & vbCrLf
      statement = statement & indent & "FILE STATUS IS " & asmdcb.dcbname & "-ST."
      tagWrite statement
      
      
      CurrentTag = "FD"
      statement = operator & Space(7 - Len(operator)) & "FD  " & asmdcb.dcbname
      statement = statement & Space(72 - Len(statement)) & parameters & vbCrLf
      statement = statement & indent & "BLOCK CONTAINS 0 RECORDS." & vbCrLf
      statement = statement & indent & "01 " & asmdcb.FRec & Space(20 - Len(asmdcb.FRec)) & " PIC X(" & asmdcb.LRec & ")."
      tagWrite statement
      
      
      CurrentTag = "FILE-STATUS"
      statement = operator & Space(7 - Len(operator)) & "01 " & asmdcb.dcbname & "-ST" & Space(20 - Len(asmdcb.dcbname & "-ST")) & " PIC X(02) VALUE SPACES."
      statement = statement & Space(72 - Len(statement)) & parameters & vbCrLf
      tagWrite statement
      
      DFile.Add asmdcb, label
    Case "ACB"
      CurrentTag = "ASSIGN"
      asmdcb.dcbname = label
      asmdcb.FRec = asmdcb.dcbname & "-REC"
      statement = operator & Space(7 - Len(operator)) & "SELECT " & asmdcb.dcbname & " ASSIGN TO " & asmdcb.ddname
      statement = statement & Space(72 - Len(statement)) & parameters & vbCrLf
      statement = statement & indent & "ORGANIZATION IS SEQUENTIAL " & vbCrLf
      statement = statement & indent & "FILE STATUS IS " & asmdcb.dcbname & "-ST."
      tagWrite statement
    
''      changeTag RTB, "<ASSIGN(i)>", "SELECT " & label & " ASSIGN TO " & asmdcb.ddname & vbCrLf _
''                                  & "       ORGANIZATION IS SEQUENTIAL " & vbCrLf _
''                                  & "       FILE STATUS IS " & asmdcb.ddname & "-ST."   ' & vbCrLf _
''                                 ' & "       ACCESS MODE IS SEQUENTIAL."
    Case "RPL"
      asmdcb.FRec = asmdcb.dcbname & "-REC"
      CurrentTag = "FD"
      statement = operator & Space(7 - Len(operator)) & "FD  " & asmdcb.dcbname
      statement = statement & Space(72 - Len(statement)) & parameters & vbCrLf
      statement = statement & indent & "BLOCK CONTAINS 0 RECORDS." & vbCrLf
      statement = statement & indent & "01 " & asmdcb.FRec & Space(20 - Len(asmdcb.FRec)) & " PIC X(" & asmdcb.LRec & ")."
      tagWrite statement

''      changeTag RTB, "<FD(i)>", "FD  " & asmdcb.ddname & vbCrLf & _
''                                "   BLOCK CONTAINS 0 RECORDS." & vbCrLf & _
''                                "01 " & asmdcb.FRec & Space(30 - Len(asmdcb.ddname & "-REC")) & " PIC X(" & asmdcb.LRec & ")."
      
      CurrentTag = "FILE-STATUS"
      statement = operator & Space(7 - Len(operator)) & "01 " & asmdcb.dcbname & "-ST" & Space(20 - Len(asmdcb.dcbname & "-ST")) & " PIC X(02) VALUE SPACES."
      statement = statement & Space(72 - Len(statement)) & parameters & vbCrLf
      tagWrite statement
''      changeTag RTB, "<FILE-STATUS(i)>"7, "01 " & asmdcb.ddname & "-ST" & Space(30 - Len(asmdcb.ddname & "-ST")) & " PIC X(02) VALUE SPACES."
      DFile.Add asmdcb, label
  End Select
  CurrentTag = previousTag

End Sub
'SILVIA 4/9/(2008
Private Sub getGET(parameters As String)
  Dim param() As String
  Dim parametri As String
  Dim statement As String
  Dim loperand As operand, roperand As operand
  Dim cblFrom As String, cblTo As String, cblRead As String, cblEoF As String
  Dim j As Integer
  
  parametri = parameters
  Select Case operator
    Case "GET"
      param = Split(parameters, ",")
      If UBound(param) = 0 Then
        ' Caso:  GET FILE
        ' READ FILE
        ' MOVE FILE-REC TO RECORD (associato al registro R-1)
        For j = 1 To DFile.count
          If Trim(param(0)) = Trim(DFile.item(j).label) Then
            cblRead = param(0)
            cblFrom = DFile.item(j).FRec
            cblEoF = DFile.item(j).Eodad
          End If
        Next
        roperand.format = "R2"
        getOperand parameters, loperand
        getOperand 1, roperand
        setRegisters roperand.value, loperand.Displace
        'cblFrom = loperand.displace & "-REC"
        cblTo = getOperand_CBL(roperand)
        'statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo & vbCrLf
      Else
        If InStr(param(0), "(") > 0 Then
          param(0) = Replace(param(0), "(", "")
          param(0) = Replace(param(0), ")", "")
          loperand.format = "R1"
          getOperand param(0), loperand
         'Cerco il puntamento:
          cblFrom = getOperand_CBL(loperand)
        
        Else
          For j = 1 To DFile.count
            If Trim(param(0)) = Trim(DFile.item(j).label) Then
              cblRead = param(0)
              cblFrom = DFile.item(j).FRec
              cblEoF = DFile.item(j).Eodad
            End If
          Next
        End If
        If InStr(param(1), "(") > 0 Then
          param(1) = Replace(param(1), "(", "")
          param(1) = Replace(param(1), ")", "")
          roperand.format = "R2"
          getOperand param(0), loperand
          getOperand param(1), roperand
          setRegisters roperand.value, loperand.Displace
         'cblFrom = loperand.displace & "-REC"
          cblTo = getOperand_CBL(roperand)
        Else
          '''
          cblTo = param(1)
        End If
      End If
            
      statement = operator & Space(Len(indent) - Len(operator)) & "READ " & cblRead
      statement = statement & Space(72 - Len(statement)) & parametri & vbCrLf
      statement = statement & indent & "AT END GO TO " & cblEoF & "." & vbCrLf
      tagWrite statement
      statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo & vbCrLf
        
    Case "PUT"
      If InStr(parameters, "RPL") Then
        'CASO PUT    RPL=<LABEL-RPL>
        param = Split(parameters, "=")
        For j = 1 To DFile.count
          If Trim(param(1)) = Trim(DFile.item(j).label) Then
            statement = operator & Space(Len(indent) - Len(operator)) & "WRITE " & DFile.item(j).FRec
            Exit For
          End If
        Next
      Else
        param = Split(parameters, ",")
        If UBound(param) = 0 Then
          'CASO PUT    FILE-REC
          For j = 1 To DFile.count
            If Trim(param(0)) = Trim(DFile.item(j).dcbname) Then
              statement = operator & Space(Len(indent) - Len(operator)) & "WRITE " & DFile.item(j).FRec
              Exit For
            End If
          Next
        Else
          'CASO PUT    FILE-REC,W-RECORD
          For j = 1 To DFile.count
            If Trim(param(0)) = Trim(DFile.item(j).dcbname) Then
              statement = operator & Space(Len(indent) - Len(operator)) & "WRITE " & DFile.item(j).FRec & " FROM " & param(1)
              Exit For
            End If
          Next j
        End If
      End If
      statement = statement & Space(72 - Len(statement)) & parametri & vbCrLf
  End Select
  tagWrite statement
End Sub

''''''''''''''''''''''''''''''''''''''
' MVC, Move Characters, D1(L,B1),D2(B2)
' MVCIN, Move Characters Inverse,D1(L,B1),D2(B2)
' MVCL, Move Characters Long, R1,R2
' MVI, Move Immediate, D1(B1),I2
' MVN, Move Numerics, D1(L,B1),D2(B2)
' MVO, Move with Offset,D1(L1,B1),D2(L2,B2)
' MVZ, Move Zones,D1(L,B1),D2(B2)   - TMP: FARE!!!!!
''''''''''''''''''''''''''''''''''''''
Private Sub getMove(parameters As String)
  Dim statement As String
  Dim loperand As operand, roperand As operand
  Dim cblFrom As String, cblTo As String
  Dim param As String
  Dim i As Integer
  Dim groupField As String, disp As Integer
  
  param = parameters
  
  Select Case operator
    Case "MVC"
      '''''''''''''''''''''''
      'SQ - PROBLEMA!!!!!! MOVE COMP-A TO COMP-B(1:4)
      '                    E' SBAGLIATA! (NON RIMANE INALTERATA...)
      '                    COS� FUNZIA: MOVE COMP-A(1:4) TO COMP-B(1:4)
      '''''''''''''''''''''''
      loperand.format = "D1(L,B1)"
      roperand.format = "D2(B2)"
      
      getOperand parameters, loperand
      getOperand parameters, roperand
            
      cblFrom = getOperand_CBL(roperand)
      
      'SQ 16-3-10  - pezza: fare meglio il controllo sul tipo!
      'cblTo = getOperand_CBL(loperand, True)
      'SQ 16-3-10  - pezza: fare meglio il controllo su tipo/len
      If roperand.len <> loperand.len Then
        If roperand.len < loperand.len Then
          'meglio che l'utente verifichi!
          writeLog "I", "Length of FROM operand: " & roperand.len & " < Length of TO operand: " & loperand.len
        End If
        'TMP!!! funzioncina per i vari casi! (numerici, etc...)
        cblFrom = getOperand_CBL(roperand, roperand.iType <> "H" Or roperand.iType <> "H")
        cblTo = getOperand_CBL(loperand, roperand.iType <> "H" Or roperand.iType <> "H")
        'sistemazione "posizionamento"
        If loperand.len > 0 Then
          cblFrom = Replace(cblFrom, ":" & IIf(roperand.len, roperand.len, ""), ":" & loperand.len)
        End If
      Else
        cblFrom = getOperand_CBL(roperand)
        cblTo = getOperand_CBL(loperand)
      End If
      If InStr(cblTo, ".") Then
        groupField = Left(cblTo, InStr(cblTo, ".") - 1)
        cblTo = Mid(cblTo, InStr(cblTo, ".") + 1)
        If InStr(cblTo, ")") Then
          disp = InStr(cblTo, "(")
          cblTo = Left(cblTo, disp - 1) & " OF " & groupField & Mid(cblTo, disp)
        Else
          cblTo = cblTo & " OF " & groupField
        End If
      End If
      If InStr(cblFrom, ".") Then
        groupField = Left(cblFrom, InStr(cblFrom, ".") - 1)
        cblFrom = Mid(cblFrom, InStr(cblFrom, ".") + 1)
        If InStr(cblFrom, ")") Then
          disp = InStr(cblFrom, "(")
          cblFrom = Left(cblFrom, disp - 1) & " OF " & groupField & Mid(cblFrom, disp)
        Else
          cblFrom = cblFrom & " OF " & groupField
        End If
      End If
      If roperand.iType <> loperand.iType Then
        'sistemazione tipo di dato: CICCIO,=H'2000'  -> errore se CICCIO � pic(x)!!!
        If loperand.iType = "C" And roperand.iType = "H" Then
          If InStr(cblTo, ")") Then
            disp = Mid(cblTo, InStr(cblTo, "("))
            cblTo = Left(cblTo, disp - 1) & "-H" & Mid(cblTo, disp)
          Else
            cblTo = cblTo & "-H"
          End If
        Else
          DoEvents
        End If
      End If

      statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param
    Case "MVI"
      loperand.format = "D1(B1)"
      roperand.format = "I2"
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'SQ 26-08-08 (1 carattere solo!)
      loperand.len = 1
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand, True)
      
      statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param
    Case "MVN"
      'SILVIA 10/9/2008
      For i = 1 To Label_Temp.count
        If Label_Temp.item(i) = "REV-MVN" Then
          Exit For
        End If
      Next
      If i > Label_Temp.count Then
        Label_Temp.Add "REV-MVN", "REV-MVN"
      End If
      
      'TMP
      operator = operator & TILDE
      loperand.format = "D1(L,B1)"
      roperand.format = "D2(B2)"
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'Delimitazione campo:
      If loperand.len = 0 Then
        loperand.len = roperand.len
      End If
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand)
      
      statement = "MOVE " & cblFrom & " TO REV-TBL-R"
      statement = operator & Space(Len(indent) - Len(operator)) & statement
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param & vbCrLf
      
      statement = statement & indent & "MOVE " & cblTo & " TO REV-TBL-L" & vbCrLf
      statement = statement & indent & "MOVE " & loperand.len & " TO REV-LEN" & vbCrLf
      statement = statement & indent & "PERFORM REV-MVN" & vbCrLf
      statement = statement & indent & "MOVE REV-TBL-L(1:" & loperand.len & ") TO " & cblTo
    Case "MVO"
      'SILVIA 10/9/2008
      For i = 1 To Label_Temp.count
        If Label_Temp.item(i) = "REV-MVO" Then
          Exit For
        End If
      Next
      If i > Label_Temp.count Then
        Label_Temp.Add "REV-MVO", "REV-MVO"
      End If
    
      'TMP (a cosa serve L2?!!!)
      operator = operator & TILDE
      loperand.format = "D1(L1,B1)"
      roperand.format = "D2(L2,B2)"
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'Delimitazione campo:
      If loperand.len = 0 Then
        loperand.len = roperand.len
      End If
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand)
      '''''''''''''''''''''''''''''''''''''''
      ' MVO: es: es: x'CF',x'32' -> x'F2'...
      '
      '''''''''''''''''''''''''''''''''''''''
      statement = "MOVE " & cblFrom & " TO REV-TBL-R"
      statement = operator & Space(Len(indent) - Len(operator)) & statement
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param & vbCrLf
      
      statement = statement & indent & "MOVE " & cblTo & " TO REV-TBL-L" & vbCrLf
      statement = statement & indent & "MOVE " & loperand.len & " TO REV-L1" & vbCrLf
      statement = statement & indent & "MOVE " & roperand.len & " TO REV-L2" & vbCrLf
      statement = statement & indent & "PERFORM REV-MVO" & vbCrLf
      statement = statement & indent & "MOVE REV-TBL-L(1:" & loperand.len & ") TO " & cblTo & "(1:" & loperand.len & ")"
    Case "MVZ"
      'FARE!!!!!!!!!!
      loperand.format = "D1(L1,B1)"
      roperand.format = "D2(L2,B2)"
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'Delimitazione campo:
      If loperand.len = 0 Then
        loperand.len = roperand.len
      End If
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand, True)
      '''''''''''''''''''''''''''''''''''''''
      ' MVZ: es:...
      '''''''''''''''''''''''''''''''''''''''
      'Delimitazione campo:
      If roperand.len = 0 Then 'And Len(rOperand.iType) = 0 Then
        If loperand.len Then
          cblTo = cblTo & "(1:" & loperand.len & ")"
        Else
          'Lunghezza da calcolare?!
          getDataLen loperand
          'cblTo = cblTo & "(1:" & lOperand.len & ")"
          'pezza:
          cblTo = Replace(cblTo, "(1:0)", "")
          cblFrom = Replace(cblFrom, ":0)", ":" & loperand.len & ")")
        End If
      End If
      statement = "#" & operator & Space(Len(indent) - Len(operator) - 1) & "MOVE " & cblFrom & " TO " & cblTo
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param
    Case "MVCIN"
      loperand.format = "D1(L,B1)"
      roperand.format = "D2(B2)"
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'Delimitazione campo:
      If loperand.len = 0 Then
        loperand.len = roperand.len
      End If
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand, True)
      
      'Delimitazione campo:
      If roperand.len = 0 Then 'And Len(rOperand.iType) = 0 Then
        If loperand.len Then
          cblTo = cblTo & "(1:" & loperand.len & ")"
        Else
          'Lunghezza da calcolare?!
          getDataLen loperand
          'cblTo = cblTo & "(1:" & lOperand.len & ")"
          'pezza:ss
          cblTo = Replace(cblTo, "(1:0)", "")
          cblFrom = Replace(cblFrom, ":0)", ":" & loperand.len & ")")
        End If
      End If
      statement = "#" & operator & Space(Len(indent) - Len(operator) - 1) & "MOVE " & cblFrom & " TO " & cblTo
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param
    Case "MVCL"
      'The data string identified by operand-2 (r2) is moved to the storage location identified by operand-1 (r1).
      'Both operands are register-pairs.
      'The first register of each operand needs to be loaded with the storage addresses of the data strings.
      'The second register of each operands needs to be loaded with the length of each operand.
      'The data strings may be different length.
      'The high-order byte of the second register of operand-2 is treated as the padding character if the operands or different lengths.
      'The move proceeds from left to right, low storage to high storage and as each character is moved the length registers are decremented.

      loperand.format = "R1"
      roperand.format = "R2"

      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'Delimitazione campo:
      If loperand.len = 0 Then
        loperand.len = roperand.len
      End If
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand)
      
      'Gestione particolare:
      roperand.base = CInt(Replace(cblFrom, "I-", ""))
      loperand.base = CInt(Replace(cblTo, "I-", ""))
      cblFrom = Replace(cblFrom, "I-", "AREA-") & "(1:I-" & roperand.base + 1 & ")"
      cblTo = Replace(cblTo, "I-", "AREA-") & "(1:I-" & loperand.len + 1 & ")"

      If roperand.len = 0 Then 'And Len(rOperand.iType) = 0 Then
        If loperand.len Then
          cblTo = cblTo & "(1:" & loperand.len & ")"
        Else
          'Lunghezza da calcolare?!
          cblTo = cblTo & "(1:" & "<LEN_" & loperand.Displace & ">)"
        End If
      End If
      statement = "#" & operator & Space(Len(indent) - Len(operator) - 1) & "MOVE " & cblFrom & " TO " & cblTo
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param
  End Select
  
  'Delimitazione campo:
'  If rOperand.len = 0 Then 'And Len(rOperand.iType) = 0 Then
'    If lOperand.len Then
'      rOperand.len = lOperand.len
'    Else
'      'Lunghezza da calcolare?!
'      rOperand.len = "<LEN_" & lOperand.displace & ">"
'    End If
'  End If
  
  'SQ 2.0
  If loperand.iType = "REG" Then
    statement = operator & Space(Len(indent) - Len(operator)) & "SET ADDRESS OF AREA-" & loperand.base & " TO PTR-" & loperand.base & vbCrLf & statement
  End If
  If roperand.iType = "REG" Then
    statement = operator & Space(Len(indent) - Len(operator)) & "SET ADDRESS OF AREA-" & roperand.base & " TO PTR-" & roperand.base & vbCrLf & statement
  End If
  
  tagWrite statement
End Sub

''''''''''''''''''''''''''''''''''''''
' ED    Edit             [DE]    D1(L1,B1),D2(B2)
' EDMK  Edit and Mark    [DF]    D1(L1,B1),D2(B2)
'       -> The address of the first significant byte is put in general register 1.
'       -> TMP: FARE!!!!!
''''''''''''''''''''''''''''''''''''''
Private Sub getEDIT(parameters As String)
  Dim statement As String
  Dim loperand As operand, roperand As operand
  Dim cblFrom As String, cblTo As String
  Dim param As String
  Dim i As Integer
  param = parameters
  
  Select Case operator
    Case "ED"
       'SILVIA 10/9/2008
      For i = 1 To Label_Temp.count
        If Label_Temp.item(i) = "REV-ED" Then
          Exit For
        End If
      Next
      If i > Label_Temp.count Then
        Label_Temp.Add "REV-ED", "REV-ED"
      End If
   
      'TMP
      operator = operator & TILDE
      loperand.format = "D1(L1,B1)"
      roperand.format = "D2(B2)"
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'Delimitazione campo:
      If loperand.len = 0 Then
        loperand.len = roperand.len
      End If
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand)
      
      statement = "MOVE " & cblFrom & " TO REV-TBL-R"
      statement = operator & Space(Len(indent) - Len(operator)) & statement
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param & vbCrLf
      
      statement = statement & indent & "MOVE " & cblTo & " TO REV-TBL-L" & vbCrLf
      statement = statement & indent & "MOVE " & loperand.len & " TO REV-LEN" & vbCrLf
      statement = statement & indent & "PERFORM REV-ED" & vbCrLf
      statement = statement & indent & "MOVE REV-TBL-L(1:" & loperand.len & ") TO " & cblTo

    Case "EDMK"
      loperand.format = "D1(L1,B1)"
      roperand.format = "D2(B2)"
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand)
      
      statement = "#" & operator & Space(6 - (Len(operator) + 1)) & "*" & "PARAM: " & param
  End Select
  
  tagWrite statement
End Sub
''''''''''''''''''''''''''''''''''''''
' C, Compare
' CDS, Compare Double and Swap
' CH, Compare Halfword
' CR, Compare Register
' CL, Compare Logical
' CLC, Compare Logical Characters
' ...
''''''''''''''''''''''''''''''''''''''
Private Sub getCOMPARE(parameters As String)
  Dim statement As String, cmpString As String, cmpAddress As String, token As String
  Dim address() As String
  Dim base As String, Offset As String
  '
  Dim loperand As operand, roperand As operand
  Dim param As String
  Dim groupField As String, disp As Integer
  
  On Error GoTo nextTokenErr
  
  param = parameters
  ReDim address(0)
  
  Select Case operator
    Case "CLC"
      loperand.format = "D1(L,B1)"
      roperand.format = "D2(B2)"
      
      getOperand parameters, loperand
      getOperand parameters, roperand
      'SQ 16-3-10  - pezza: fare meglio il controllo su tipo/len
      If roperand.len <> loperand.len Then
        'TMP!!! funzioncina per i vari casi! (numerici, etc...)
        cmpString = getOperand_CBL(roperand, roperand.iType <> "H" Or roperand.iType <> "H")
        cmpAddress = getOperand_CBL(loperand, roperand.iType <> "H" Or roperand.iType <> "H")
        'sistemazione "posizionamento"
        cmpString = Replace(cmpString, ":" & IIf(roperand.len, roperand.len, ""), ":" & loperand.len)
      Else
        cmpString = getOperand_CBL(roperand)
        cmpAddress = getOperand_CBL(loperand)
      End If
      If roperand.iType <> loperand.iType Then
        'sistemazione tipo di dato: CICCIO,=H'2000'  -> errore se CICCIO � pic(x)!!!
        If loperand.iType = "C" And roperand.iType = "H" Then
          cmpAddress = cmpAddress & "-H"
        Else
          DoEvents
        End If
      End If
    Case "CLI"
      loperand.format = "D1(B1)"
      roperand.format = "I2"
      'SQ 26-08-08 (1 carattere solo!)
      loperand.len = 1
      
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      cmpString = getOperand_CBL(roperand)
      cmpAddress = getOperand_CBL(loperand)
      
    Case "C"
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"

  
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      cmpString = getOperand_CBL(roperand)
      cmpAddress = getOperand_CBL(loperand)
    Case "CH"
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
  
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      cmpString = getOperand_CBL(roperand)
      cmpAddress = getOperand_CBL(loperand)
    Case "CR"
      'SQ 29-06-10
      loperand.format = "R1"
      roperand.format = "R2"
  
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      cmpString = getOperand_CBL(roperand)
      cmpAddress = getOperand_CBL(loperand)
    Case "CP"
      'SQ tmp
      'D1(L1,B1),D2(L2,B2)
      loperand.format = "D1(L1,B1)"
      roperand.format = "D2(L2,B2)"
  
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      cmpString = getOperand_CBL(roperand)
      cmpAddress = getOperand_CBL(loperand)
      
      If roperand.iType <> loperand.iType Then
        'sistemazione tipo di dato: CICCIO,=H'2000'  -> errore se CICCIO � pic(x)!!!
        If loperand.iType <> "P" Then
          cmpAddress = cmpAddress & "-P"
        End If
        If roperand.iType <> "P" Then
          cmpString = cmpString & "-P"
        End If
      End If
    Case "CDS", "CL", "CLCL", "CLM", "CLR", "CS"
      operator = operator & "#"
      statement = operator & Space(Len(indent) - Len(operator)) & parameters
      tagWrite statement
      Exit Sub
  End Select
        
  'STATEMENT:
  If InStr(cmpAddress, ".") Then
    groupField = Left(cmpAddress, InStr(cmpAddress, ".") - 1)
    cmpAddress = Mid(cmpAddress, InStr(cmpAddress, ".") + 1)
    If InStr(cmpAddress, ")") Then
      disp = InStr(cmpAddress, "(")
      cmpAddress = Left(cmpAddress, disp - 1) & " OF " & groupField & Mid(cmpAddress, disp)
    Else
      cmpAddress = cmpAddress & " OF " & groupField
    End If
  End If
  statement = "IF " & cmpAddress & " = " & cmpString
  statement = operator & Space(Len(indent) - Len(operator)) & statement
  statement = statement & Space(72 - Len(statement)) & param & vbCrLf
  statement = statement & indent & "   MOVE 0 TO COND-CODE" & vbCrLf
  statement = statement & indent & "ELSE IF " & cmpAddress & " < " & cmpString & vbCrLf
  statement = statement & indent & "  MOVE 1 TO COND-CODE" & vbCrLf
  statement = statement & indent & "ELSE MOVE 2 TO COND-CODE."

  'SQ 2.0 - tmp... vedere bene quando...
  If Left(operator, 2) = "CL" Then
    If loperand.iType = "REG" Then
      statement = operator & Space(Len(indent) - Len(operator)) & "SET ADDRESS OF AREA-" & loperand.base & " TO PTR-" & loperand.base & vbCrLf & statement
    End If
    If roperand.iType = "REG" Then
      statement = operator & Space(Len(indent) - Len(operator)) & "SET ADDRESS OF AREA-" & roperand.base & " TO PTR-" & roperand.base & vbCrLf & statement
    End If
  End If
  'tagWrite Replace(operator & Space(Len(indent) - Len(operator)) & statement, vbCrLf, vbCrLf & operator & Space(Len(indent) - Len(operator)))
  tagWrite statement
  
  Exit Sub
nextTokenErr:
  'tmp
  MsgBox err.description
  Resume
End Sub
'''''''''''''''''''''''''''''''''''''''''
' Convert to Decimal: [4E] R1,D2(X2,B2)
' SQ 28-08-08
''''''''''''''''''''''''''''''''''''''
Private Sub getCV(parameters As String)
  Dim loperand As operand, roperand As operand
  Dim cblFrom As String, cblTo As String
  Dim statement As String, param As String
  
  param = parameters
  
  'auxField =  'CVD" 'in copy... S9(15) COMP-3
  
  'tmp
  'tagWrite "~CVD  *PARAM: " & parameters
  
  loperand.format = "R1"
  roperand.format = "D2(X2,B2)"

  
  getOperand parameters, loperand
  getOperand parameters, roperand
  
  'rovesciati
  cblFrom = getOperand_CBL(loperand)
  cblTo = getOperand_CBL(roperand, True)
  
  'fixed: rOperand.len = 8
  cblTo = Replace(cblTo, ":)", ":8)")
  
  If operator = "CVD" Then
    'CONVERT to DECIMAL
    ''''''''''''''''''''''''''''''
    ' -> MOVE I-1 TO REV-CVD
    '    MOVE REV-CVD TO ...
    ''''''''''''''''''''''''''''''
    statement = "MOVE " & cblFrom & " TO REV-" & operator
    statement = operator & Space(Len(indent) - Len(operator)) & statement
    statement = statement & Space(72 - Len(statement)) & param & vbCrLf
    statement = statement & indent & "MOVE REV-CVD TO " & cblTo
  Else  'CVB
    'CONVERT to BINARY
    ''''''''''''''''''''''''''''''
    ' ES: DOBLEPA,R1
    ' -> MOVE DOBLEPA TO REV-CVD(1:8)
    '    MOVE REV-CVD TO I-1
    ''''''''''''''''''''''''''''''
    '(from e to invertiti...)
    statement = "MOVE " & cblTo & " TO REV-CVD(1:8)"
    statement = operator & Space(Len(indent) - Len(operator)) & statement
    statement = statement & Space(72 - Len(statement)) & param & vbCrLf
    statement = statement & indent & "MOVE REV-CVD TO " & cblFrom
  End If
  
  tagWrite statement
  
End Sub

''''''''''''''''''''''''''''''''''''''
' ZAP, Zero Add Packed
''''''''''''''''''''''''''''''''''''''
Private Sub getZAP(parameters As String)
  Dim loperand As operand, roperand As operand
  Dim cblFrom As String, cblTo As String
  Dim statement As String, param As String
  
  On Error GoTo catch
  
  'tmp
  operator = operator & TILDE
  
  param = parameters
  
  loperand.format = "D1(L1,B1)"
  roperand.format = "D2(L2,B2)"
  
  getOperand parameters, loperand
  getOperand parameters, roperand
  
  cblFrom = getOperand_CBL(roperand, True)
  cblTo = getOperand_CBL(loperand)

  If loperand.iType = "P" Then
    statement = "MOVE " & cblFrom & " TO " & cblTo
    statement = operator & Space(Len(indent) - Len(operator)) & statement
    statement = statement & Space(72 - Len(statement)) & param & vbCrLf
  Else
    statement = "MOVE " & cblFrom & " TO REV-PACKED(" & 10 - roperand.len + 1 & ":" & roperand.len & ")"
    statement = operator & Space(Len(indent) - Len(operator)) & statement
    statement = statement & Space(72 - Len(statement)) & param & vbCrLf
    statement = statement & indent & "MOVE REV-PACKED(" & 10 - loperand.len + 1 & ":" & loperand.len & ") TO " & cblTo
  End If

  tagWrite statement
    
  Exit Sub
catch:
  writeLog "E", operator & ":" & parameters & "-" & err.description
End Sub
''''''''''''''''''''''''''''''''''''''
' PACK, Pack    [F2]  D1(L1,B1),D2(L2,B2)
' UNPK, Unpack
''''''''''''''''''''''''''''''''''''''
Private Sub getPACK_UNPK(parameters As String)
  Dim statement As String
  Dim loperand As operand, roperand As operand
  Dim cblFrom As String, cblTo As String
  Dim previousTag As String
  'debug:
  Dim tag72 As String, param As String
  Dim isTrovato As Boolean, i As Integer
  Dim groupField As String, disp As Integer
  
  tag72 = parameters
  param = parameters
  
  'backup
  previousTag = CurrentTag
  CurrentTag = "DATA-REDEFINES"
  
  loperand.format = "D1(L1,B1)"
  roperand.format = "D2(L2,B2)"
  
  getOperand parameters, loperand
  getOperand parameters, roperand
  
  Select Case operator
    Case "PACK"
      '''''''''''''''''''''''''''''''''''''''''''''''''''
      'La trasformazione necessaria la fa gi� il COBOL,
      'muovendo su un COMP-3
      'ATTENZIONE: per Microfocus ci vuole la
      '$SET HOSTNUMMOVE(2)
      'Nota: L1 e L2 MAX 16 byte
      '''''''''''''''''''''''''''''''''''''''''''''''''''
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand)
      ''''''''''''''''''''''''''''''''''''''''''
      'Es:
      '#PACK *PARAM: FHLZLE+23(9),FVSNR
      '#          MOVE FVSNR-P TO FHLZLE-24
      '''''''''''''''''''''''''''''''''''''''''
      
      ''''''''''''''''''''''''''''''''''''''''''
      'SQ 2-09-08
      ' Se il campo � "secco", faccio il giro
      ' del getAddedName, con eventuale redefine;
      ' altrimenti passo per il campo d'appoggio!
      ' P.S. potrei anche fare sempre cos�!!
      ''''''''''''''''''''''''''''''''''''''''''
      If InStr(cblTo, "(") And False Then
        'SQ: vecchio modo: sistemare o buttare!
        'Nuovo campo: sar� da aggiungere in Working!
        'Packed: "-P" finale
        cblTo = getAddedName(cblTo)
        'Nuovo campo: sar� da aggiungere in Working!
        'Numerico: "-<displace>" finale
        
        'pezza: quando vengono cos�:
        'MOVE WMANUEL(71:5)-71 TO PBEWUZ-P
        If InStr(cblFrom, "(" & CInt("0" & roperand.displacePlus) + 1) Then
          tag72 = Replace(Mid(cblFrom, InStr(cblFrom, ":") + 1), ")", "")
          cblFrom = Left(cblFrom, InStr(cblFrom, "(") - 1)
        End If
        
        'debug: per copia incolla!
        'SQ
        'tagWrite "@@     01 " & cblFrom & "-Z REDEFINES " & cblFrom & "."
        tagWrite "@@     01 FILLER REDEFINES " & cblFrom & "."
        
        cblFrom = cblFrom & "-" & CInt("0" & roperand.displacePlus) + 1
        
        'debug: per copia incolla!
        If IsNumeric(tag72) Then
          If CInt(tag72) > 1 Then
            tagWrite "@@       03 FILLER PIC X(" & tag72 - 1 & ")."
          End If
        End If
        tagWrite "@@       03 " & cblFrom & " PIC 9(" & tag72 & ")."
        
        If InStr(cblFrom, ".") Then
          groupField = Left(cblFrom, InStr(cblFrom, ".") - 1)
          cblFrom = Mid(cblFrom, InStr(cblFrom, ".") + 1)
          If InStr(cblFrom, ")") Then
            disp = InStr(cblFrom, "(")
            cblFrom = Left(cblFrom, disp - 1) & " OF " & groupField & Mid(cblFrom, disp)
          Else
            cblFrom = cblFrom & " OF " & groupField
          End If
        End If
        statement = "PACK" & Space(Len(indent) - 4) & "MOVE " & cblFrom & " TO " & cblTo
        
        statement = statement & Space(72 - Len(statement)) & param
      Else
        'SQ nuovo modo!
        'Es: DOBLE,9(3,R1)
        '->  MOVE PTR1(I-1 + 10:3) TO REV-PACKED
        '->  MOVE REV-PACKED(<len_rev-packed - len_doble>:3) TO DOBLE
        If Right(cblFrom, 1) = ")" Then
          cblFrom = Left(cblFrom, InStr(cblFrom, "(") - 1)
        End If
        cblFrom = cblFrom & "-9"
        If Trim(Right(param, 1)) = ")" Then
          cblFrom = cblFrom & IIf(roperand.displacePlus = "", 0, roperand.displacePlus) & roperand.len
        End If
        'IIf(roperand.displacePlus = "", "", roperand.displacePlus & roperand.len)
        If InStr(cblFrom, ".") Then
          groupField = Left(cblFrom, InStr(cblFrom, ".") - 1)
          cblFrom = Mid(cblFrom, InStr(cblFrom, ".") + 1)
          If InStr(cblFrom, ")") Then
            disp = InStr(cblFrom, "(")
            cblFrom = Left(cblFrom, disp - 1) & " OF " & groupField & Mid(cblFrom, disp)
          Else
            cblFrom = cblFrom & " OF " & groupField
          End If
        End If
        statement = "MOVE " & cblFrom & " TO REV-PACKED"
        statement = statement & Space(72 - Len(statement) - Len(indent)) & param & vbCrLf
        'If roperand.len Then
        '  statement = statement & indent & "MOVE REV-PACKED(" & 10 - loperand.len + 1 & ":" & loperand.len & ") TO " & cblTo & "(" & 10 - loperand.len + 1 & ":" & loperand.len & ")"
        'Else
          statement = statement & indent & "MOVE REV-PACKED TO " & cblTo
        'End If
        statement = operator & Space(Len(indent) - Len(operator)) & statement
        isTrovato = False
        For i = 0 To listIndex
          If dataList(i).Name = cblFrom Then
            isTrovato = True
            Exit For
          End If
        Next i
        If Not isTrovato Then
          getData cblFrom, "CL" & roperand.len, 1, 0, ""
        End If
      End If
    Case "UNPK"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'Es: (modo nuovo: campo d'appoggio... SQ 4-09-08)
      ' YY,XX
      'MOVE XX TO REV-PACKED(<lenREV-PACKED - lenXX + 1>:lenXX)
      'MOVE REV-PACKED TO REV-ZONED
      'MOVE REV-ZONED(<lenREV-ZONED - lenYY + 1>:lenYY) TO YY
      ''
      ' Miglioramento: se � gi� PACKED, non serve il primo passaggio...
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand)
      
      If roperand.iType = "P" Then
        statement = "MOVE " & cblFrom & " TO REV-ZONED"
        statement = operator & Space(Len(indent) - Len(operator)) & statement
        statement = statement & Space(72 - Len(statement)) & param & vbCrLf
      Else
        'SQ pulizia campo:
        statement = indent & "MOVE " & cblFrom & " TO REV-PACKED(" & 10 - roperand.len + 1 & ":" & roperand.len & ")"
        statement = statement & Space(72 - Len(statement)) & param & vbCrLf
        statement = operator & Space(Len(indent) - Len(operator)) & "MOVE ZERO TO REV-PACKED" & vbCrLf & statement
        statement = statement & indent & "MOVE REV-PACKED TO REV-ZONED" & vbCrLf
      End If
      statement = statement & indent & "MOVE REV-ZONED(" & 18 - loperand.len + 1 & ":" & loperand.len & ") TO " & cblTo
  End Select
  
  'restore
  CurrentTag = previousTag
  
  tagWrite statement
End Sub
''''''''''''''''''''''''''''''''''''''
' NI, aNd Immediate, D1(B1),I2
' OI, Or Immediate, D1(B1),I2
' XI, Xor Immediate, D1(B1),I2
''
' "O", "OC", "X", "XC",  "N", "NC": QUASI FATTI... verificare
' "OR", "XR",  "NR": OK
''
' Utilizzo CALL 'CBL_OR' ...
' - Settare il CONDcode
' -> UNIFICARE I CASE... TUTTI QUASI UGUALI!
''''''''''''''''''''''''''''''''''''''
Private Sub getLOGICAL(parameters As String)
  Dim statement As String, cblFrom As String, cblTo As String, cblLen As String
  Dim roperand As operand
  Dim loperand As operand
  Dim param As String
  
  On Error GoTo catch
  
  param = parameters
  
  Select Case operator
    Case "NI", "OI", "XI"
      loperand.format = "D1(B1)"
      roperand.format = "I2"
      getOperand parameters, loperand
      getOperand parameters, roperand
      'SQ old... non lo scrivevo in quei casi (non serve... o quasi)
'     Quando segue PACK/UNPCK quasi sempre "sistema" l'ultimo byte...
'      Select Case lastInstruction.operator
'        Case "PACK"
'          getOperand parameters, loperand
'          getOperand parameters, roperand
'
'          If roperand.value = "'0F'" Then
'            statement = "OI    *" & "PARAM: " & param
'          Else
'            statement = "#OI:  *" & "PARAM: " & param
'          End If
'        Case "UNPK"
'          If roperand.value = "'F0'" Then
'            statement = "OI    *" & "PARAM: " & param
'          Else
'            statement = "#OI:  *" & "PARAM: " & param
'          End If
'        Case Else
'          statement = "#" & operator & Space(8 - (Len(operator) + 1)) & "*" & "PARAM: " & parameters
   
      'SQ 26-08-08 (1 carattere solo!)
      loperand.len = 1
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand)
      cblLen = 1
      '''''''''''''''''''''''''''''''''''''''''''''
      ' OR FIELD1,X'80'  -> CALL 'CBL_OR' USING X'80',FIELD1 BY VALUE 1
      '''''''''''''''''''''''''''''''''''''''''''''
     
      statement = "CALL 'CBL_" & Switch(operator = "NI", "AND", operator = "OI", "OR", operator = "XI", "XOR") & "' USING " & cblFrom & "," & cblTo
      'colonna 72: parametri originali
      If Len(statement) < 72 Then
        statement = statement & Space(72 - Len(statement) - Len(indent)) & param
      Else
        statement = statement & " " & param
      End If
      statement = statement & vbCrLf & Space(20) & " BY VALUE " & cblLen
      statement = statement & vbCrLf & indent & "IF " & cblTo & " = LOW-VALUE MOVE ZERO TO COND-CODE"
      statement = statement & vbCrLf & indent & "ELSE MOVE 1 TO COND-CODE."
      statement = operator & Space(Len(indent) - Len(operator)) & statement
    Case "N", "O", "X"
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand)
      '''''''''''''''''''''''''''''''''''''''''''''
      ' OR FIELD1,X'80'  -> CALL 'CBL_OR' USING X'80',FIELD1 BY VALUE 1
      '''''''''''''''''''''''''''''''''''''''''''''

      statement = "CALL 'CBL_" & Switch(operator = "N", "AND", operator = "O", "OR", operator = "X", "XOR") & "' USING " & cblFrom & "," & cblTo
      'colonna 72: parametri originali
      If Len(statement) < 72 Then
        statement = statement & Space(72 - Len(statement)) & param
      Else
        statement = statement & " " & param
      End If
      statement = statement & vbCrLf & Space(20) & " BY VALUE " & loperand.len
      statement = statement & vbCrLf & indent & "IF " & cblTo & " = LOW-VALUE MOVE ZERO TO COND-CODE"
      statement = statement & vbCrLf & indent & "ELSE MOVE 1 TO COND-CODE."
      statement = operator & Space(Len(indent) - Len(operator)) & statement
    Case "NC", "OC", "XC"
      loperand.format = "D1(L,B1)"
      roperand.format = "D2(B2)"
      getOperand parameters, loperand
      getOperand parameters, roperand

      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand)
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' OR FIELD1,FIELD2  -> CALL 'CBL_OR' USING FIELD2,FIELD1 BY VALUE <len_of_field1> o
      '                                                                LENGTH OF FIELD1
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If loperand.len = 0 Then
        'Lunghezza da calcolare?!
        getDataLen loperand
      End If
      cblLen = IIf(loperand.len, loperand.len, " LENGTH OF " & loperand.Displace)
      
      statement = "CALL 'CBL_" & Switch(operator = "NC", "AND", operator = "OC", "OR", operator = "XC", "XOR") & "' USING " & cblFrom & "," & cblTo
      'colonna 72: parametri originali
      If Len(statement) < 72 Then
        statement = statement & Space(72 - Len(statement)) & param
      Else
        statement = statement & " " & param
      End If
      statement = statement & vbCrLf & Space(20) & " BY VALUE " & loperand.len
      statement = statement & vbCrLf & indent & "IF " & cblTo & " = LOW-VALUE MOVE ZERO TO COND-CODE"
      statement = statement & vbCrLf & indent & "ELSE MOVE 1 TO COND-CODE."
      statement = operator & Space(Len(indent) - Len(operator)) & statement
    Case "NR", "OR", "XR"
      loperand.format = "R1"
      roperand.format = "R2"
      getOperand parameters, loperand
      getOperand parameters, roperand
      '''''''''''''''''''''''''''''''''''''''''''''
      ' OR R1,R2  -> CALL 'CBL_OR' USING R-2,R-1 BY VALUE
      '''''''''''''''''''''''''''''''''''''''''''''
      cblFrom = "R-" & roperand.value
      cblTo = "R-" & loperand.value
      '
      loperand.len = 4
      
      statement = "CALL 'CBL_" & Switch(operator = "NR", "AND", operator = "OR", "OR", operator = "XR", "XOR") & "' USING " & cblFrom & "," & cblTo
      'colonna 72: parametri originali
      If Len(statement) < 72 Then
        statement = statement & Space(72 - Len(statement)) & param
      Else
        statement = statement & " " & param
      End If
      statement = statement & vbCrLf & Space(20) & " BY VALUE " & loperand.len
      statement = statement & vbCrLf & indent & "IF " & cblTo & " = LOW-VALUE MOVE ZERO TO COND-CODE"
      statement = statement & vbCrLf & indent & "ELSE MOVE 1 TO COND-CODE."
      statement = operator & Space(Len(indent) - Len(operator)) & statement
    
    Case Else
      statement = "#" & operator & Space(8 - (Len(operator) + 1)) & "*" & "PARAM: " & parameters
  End Select
  
  tagWrite statement
  
  Exit Sub
catch:
   'tmp
   writeLog "E", "Unexpected error: " & err.description
End Sub
''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''
Private Sub getData(label As String, parameters As String, dsLevel As Integer, dsLevBytes As Integer, dsORG As String)
  Dim char As String
  Dim i As Integer
  Dim FIELD As asmData
  Dim previousTag As String
  Dim statement As String
  Dim Segno As String, cblPic As String
  
  On Error GoTo errorHandler
  'backup
  previousTag = CurrentTag
  CurrentTag = "WORKING-STORAGE"
  
  FIELD.Name = label
  ''''''''''''''''''''
  ' ORG: REDEFINES:
  ' Aggiungo livello di gruppo e cerco di gerstirlo come un normale campo di gruppo
  ''''''''''''''''''''
  If Len(dsORG) Then
    'SQ 5-07-07 PEZZA: dsORG name lungo:
    If Len(dsORG) < 10 Then
      tagWrite Space(7) + Space(dsLevel - 1) & format(dsLevel, "00") & " " & dsORG & "-R" & Space(10 - Len(dsORG & "-R")) & " REDEFINES " & dsORG & "."
    Else
      tagWrite Space(7) + Space(dsLevel - 1) & format(dsLevel, "00") & " " & dsORG & "-R" & Space(40 - Len(dsORG & "-R")) & " REDEFINES " & vbCrLf & _
               Space(12) & dsORG & "."
    End If
    dsLevel = dsLevel + 2
    'TMP: SBAGLIA! NON RIESCO A GESTIRMI CON UNA SOLA VARIABILE!
    dsLevBytes = cblBytes 'Semplificato!!!!! lunghezza area precedente!
    dsORG = ""
  End If
  
  i = 1
  ''''''''''''''''''''''''''''
  ' Duplicator factor:
  ' - Costante numerica
  ' - Variabile fra parentesi
  ''''''''''''''''''''''''''''
  While IsNumeric(Mid(parameters, i, 1))
    FIELD.dupFactor = FIELD.dupFactor & Mid(parameters, i, 1)
    i = i + 1
  Wend
  If Mid(parameters, i, 1) = "(" Then
    FIELD.dupFactor = Mid(parameters, i + 1, InStr(parameters, ")") - i - 1)
    i = InStr(parameters, ")") + 1
  End If
  
  '''''''''''''''''''''
  ' Type:
  '''''''''''''''''''''
  FIELD.Type = Mid(parameters, i, 1)
  i = i + 1
  '''''''''''''''''''''
  ' Modifier:
  ' - L,S,E
  '''''''''''''''''''''
  Do While i <= Len(parameters)
    Select Case Mid(parameters, i, 1)
      Case "L"
        FIELD.modifier = FIELD.modifier & "L"
        i = i + 1
        If Mid(parameters, i, 1) = "(" Then
          'tmp: es. CL(VARIABLE)'VALUE'
          FIELD.modifierValue = Mid(parameters, i + 1, InStr(parameters, ")") - i - 1)
          i = InStr(parameters, ")") + 1
        End If
      Case "S"
        FIELD.modifier = FIELD.modifier & "S"
        i = i + 1
        'tmp
        MsgBox "Modifier: " & FIELD.modifier, vbExclamation
      Case "E"
        FIELD.modifier = FIELD.modifier & "E"
        i = i + 1
        'tmp
        writeLog "W", "Modifier: " & FIELD.modifier
      Case "'", "("     'Mauro, "("?
        Exit Do
      Case "1", "2", "3", "4", "5", "6", "7", "8", "9"
        While IsNumeric(Mid(parameters, i, 1))
          FIELD.modifierValue = FIELD.modifierValue & Mid(parameters, i, 1)
          i = i + 1
        Wend
      Case Else
        'tmp
        tagWrite "@#A   *" & parameters & "#"
        'restore
        CurrentTag = previousTag
        Exit Sub
    End Select
  Loop
  '''''''''''''''''''''
  ' Value:
  '''''''''''''''''''''
  FIELD.value = Mid(parameters, i)

  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Migrazione, sar� portato fuori...
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim cblType(2) As String, cblLen As String, cblLevel As String, cblName As String
  Dim cblvalue As String
  Dim isGroupField As Boolean
  
  ''''''''''''
  ' Nome
  ''''''''''''
  If Len(label) Then
    cblName = Replace(label, "_", "-")
    NameAux = ""
  Else
    cblLevel = format(dsLevel + 2, "00")
    cblName = "FILLER"
    If NameAux = "" Then
      For i = listIndex To 0 Step -1
        If dataList(i).Name <> "FILLER" And dataList(i).Name <> "" Then
          NameAux = dataList(i).Name
          cblLevel = format(dataList(i).level, "00")
          statement = Space(7) + Space(cblLevel - 1) & cblLevel & " " & dataList(i).Name & "-AUX."
          cblLevel = format(dataList(i).level + 2, "00")
          writeLog "I", "New field '" & dataList(i).Name & "-AUX' is added in Working-Storage."
          Exit For
        End If
      Next i
    Else
      statement = ""
    End If
  End If
  '''''''''''
  ' Tipo/Value
  '''''''''''
  cblvalue = FIELD.value
  Select Case FIELD.Type
    Case "C", "G"
      cblType(0) = "X"
      If Asm2seqfiles Then
        RTB.Text = RTB.Text & Replace(cblvalue, "'", "")
      End If
    Case "X"
      cblType(0) = "X"
      'Lunghezza
      If FIELD.modifier = "L" Then
        If Len(FIELD.modifierValue) Then
          cblLen = FIELD.modifierValue
        End If
      Else
        If Len(FIELD.value) Then
          cblLen = CInt(((Len(FIELD.value) - 2) / 2) + 0.1) 'apici
          FIELD.modifierValue = cblLen
        Else
          'a volte viene usato ModifierValue come lunghezza: chiarire
          FIELD.modifierValue = 1
          cblLen = 1
        End If
      End If
      cblBytes = "0" & cblLen
      'Valore
      If Len(FIELD.value) Then
        cblvalue = "X" & cblvalue
      End If
      If Asm2seqfiles Then
        writeLog "W", "##getData-TMP: IdObject:" & GbIdOggetto & " - TMP: Variable " & cblName & " Type 'X'!"
      End If
    Case "B"
      cblType(0) = "9"
      cblType(1) = "COMP"
      cblvalue = Replace(cblvalue, "'", "")
      'verificare
      If FIELD.modifier = "L" Then
        If Len(FIELD.modifierValue) Then
          cblBytes = FIELD.modifierValue
          cblLen = cblBytes * 2
        End If
      End If
      If Asm2seqfiles Then
        writeLog "W", "##getData-TMP: IdObject:" & GbIdOggetto & " - TMP: Variable " & cblName & " - Type 'B'!"
      End If
    Case "F"
      cblType(0) = "9"
      cblType(1) = "COMP"
      cblvalue = Replace(cblvalue, "'", "")
      cblLen = "9"
      cblBytes = "4"
      'a volte viene usato ModifierValue come lunghezza: chiarire
      FIELD.modifierValue = "4"
      If Asm2seqfiles Then
        writeLog "W", "##getData-TMP: IdObject:" & GbIdOggetto & " - TMP: Variable " & cblName & " - Type 'F'!"
      End If
    Case "H"
      cblType(0) = "9"
      cblType(1) = "COMP"
      cblvalue = Replace(cblvalue, "'", "")
      cblLen = "4"
      cblBytes = "2"
      'a volte viene usato ModifierValue come lunghezza: chiarire
      FIELD.modifierValue = "2"
      If Asm2seqfiles Then
        writeLog "W", "##getData-TMP: IdObject:" & GbIdOggetto & " - TMP: Variable " & cblName & " - Type 'H'!"
      End If
    Case "D"
      'SQ - gestire...
      'cblType(0) = "X----------------D"
      cblType(0) = "X"
      cblLen = "8"
      cblBytes = "8"
      FIELD.modifierValue = "8"
      If Asm2seqfiles Then
        writeLog "W", "##getData-TMP: IdObject:" & GbIdOggetto & " - TMP: Variable " & cblName & " - Type 'D'!"
      End If
    Case "P"
      Segno = "S"
      cblType(0) = "9"
      cblType(1) = "COMP-3"
      cblvalue = Replace(cblvalue, "'", "")
      'sistemare
      If FIELD.modifier = "L" Then
        If Len(FIELD.modifierValue) Then
          cblBytes = FIELD.modifierValue
          cblLen = (cblBytes * 2) - 1
        End If
      Else
        If Len(cblvalue) Then
          cblLen = Len(cblvalue)
          FIELD.modifierValue = cblLen
        Else
          'tmp
          cblLen = 1
        End If
      End If
      If Asm2seqfiles Then
        writeLog "W", "##getData-TMP: IdObject:" & GbIdOggetto & " - TMP: Variable " & cblName & " - Type 'P'!"
      End If
    Case "Z"
      cblType(0) = "X----------------Z"
      cblType(1) = "ZONED"
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' TMP: Tutte da gestire meglio
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If Asm2seqfiles Then
        writeLog "W", "##getData-TMP: IdObject:" & GbIdOggetto & " - TMP: Variable " & cblName & " - Type 'Z'!"
      End If
    Case "S", "Y"
      cblType(0) = "X"
      cblLen = "2"
      cblBytes = "2"
      FIELD.modifierValue = cblLen
      cblvalue = ""
      'cblName = FIELD.Type & "-" & FIELD.Name
      cblName = FIELD.Name
      If Asm2seqfiles Then
        writeLog "W", "##getData-TMP: IdObject:" & GbIdOggetto & " - TMP: Variable " & cblName & " - Type '" & FIELD.Type & "'!"
      End If
    Case "A", "E", "Q", "V"
      cblType(0) = "9"
      cblType(1) = "COMP"
      cblLen = "9"
      cblBytes = "4"
      FIELD.modifierValue = cblLen
      cblvalue = ""
      'cblName = FIELD.Type & "-" & FIELD.Name
      cblName = FIELD.Name
      If Asm2seqfiles Then
        writeLog "W", "##getData-TMP: IdObject:" & GbIdOggetto & " - TMP: Variable " & cblName & " - Type '" & FIELD.Type & "'!"
      End If
    Case "L"
      cblType(0) = "X"
      cblLen = "16"
      cblBytes = "16"
      FIELD.modifierValue = cblLen
      cblvalue = ""
      'cblName = FIELD.Type & "-" & FIELD.Name
      cblName = FIELD.Name
      If Asm2seqfiles Then
        writeLog "W", "##getData-TMP: IdObject:" & GbIdOggetto & " - TMP: Variable " & cblName & " - Type 'L'!"
      End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Case Else
      tagWrite "###############" & parameters & "###############"
  End Select
  ''''''''''''''
  ' Lunghezza:
  ''''''''''''''
  If FIELD.Type = "C" Then
    If FIELD.modifier = "L" Then
      If Len(FIELD.modifierValue) Then
        cblLen = FIELD.modifierValue
      End If
    Else
      If Len(FIELD.value) Then
        cblLen = Len(Replace(FIELD.value, "'", ""))
        'a volte viene usato ModifierValue come lunghezza: chiarire
        FIELD.modifierValue = cblLen
      Else
        'writeLog "W", label & ": lunghezza 0!"
        cblLen = "1"
        'a volte viene usato ModifierValue come lunghezza: chiarire
        FIELD.modifierValue = 1
      End If
    End If
    If Not IsNumeric(cblLen) Then
      'Es: CL(VARIABILE)
      'tmp
      cblLen = "0"
      writeLog "E", "Field " & FIELD.Name & ": Unsupported field length."
    Else
      If CLng(cblLen) < 32768 Then
        cblBytes = cblLen
      Else
        writeLog "W", "Field " & FIELD.Name & ": Warning on byte length."
        cblBytes = 32767
      End If
    End If
  Else
    If Len(cblLen) = 0 Then
      writeLog "W", label & ": length = 0"
      'TMP!!!
      cblLen = "0"
    End If
  End If
  ''''''''''''''
  ' Livello
  ''''''''''''''
  isDSECT = True
  If dsLevBytes = 0 Then
    If cblLevel = "" Then
      cblLevel = format(dsLevel, "00")
    End If
    If FIELD.dupFactor = "0" Then
      'If field.Type = "C" And InStr(field.modifier, "L") Then
        'Campo di gruppo!
        isGroupField = True
        dsLevBytes = CInt(FIELD.modifierValue)
        dsLevel = dsLevel + 2
      'Else
      '  isDSECT = False
      'End If
    Else
      isGroupField = False
    End If
  Else
    'SQ - Gestione sbagliata... se ritorno ad un livello superiore mi perdo il conto!
    If cblLevel = "" Then
      cblLevel = format(dsLevel, "00")
    End If
    If FIELD.dupFactor = "0" Then
      'If field.Type = "C" And InStr(field.modifier, "L") Then
        'Campo di gruppo!
        isGroupField = True
        'Attenzione: ASM � fetente: pu� "sforare" con i byte => livello 01!
        If CInt(FIELD.modifierValue) <= dsLevBytes Then
          dsLevBytes = CInt(FIELD.modifierValue)
          dsLevel = dsLevel + 2
        Else
          'AGGIUNTA FILLER PER PAREGGIARE!
          tagWrite Space(7) + Space(cblLevel - 1) & cblLevel & " FILLER     PIC X(" & dsLevBytes & ")." & Space(40) & "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
          dsLevBytes = 0
          dsLevel = 1
          cblLevel = "01"
        End If
      'Else
      '  isDSECT = False
      'End If
    Else
      isGroupField = False
      dsLevBytes = dsLevBytes - cblBytes  'Attenzione... ci vorrebbe "bytes!"
      If dsLevBytes <= 0 Then
        'ultimo elemento... reset
        dsLevel = 1
        If dsLevBytes < 0 Then
          'tmp
          'MsgBox "Field " & FIELD.Name & ": Warning on byte lenght.", vbInformation, "i-4.Migration"
          writeLog "W", "Field " & FIELD.Name & ": Warning on byte length."
          dsLevBytes = 0
        End If
      End If
    End If
  End If
    
  'Chiarire bene: DS in CSECT!
  'SQ tmp: chiarire!
  'non entrano mai gli elementi tipo:
  'GIGI DS C/F/H...
  If False And Len(parameters) = 1 Then
    isDSECT = False
  End If
  
  If isDSECT Then
    If label = "" And FIELD.dupFactor = "0" Then
    Else
      FIELD.level = dsLevel 'potrebbe fare da flag...
      If isGroupField Then
        statement = Space(7) + Space(cblLevel - 1) & cblLevel & " " & cblName & Space(15 - Len(cblName)) & "."
        statement = statement & Space(72 - Len(statement)) & IIf(Len(FIELD.dupFactor), "#DUP:" & FIELD.dupFactor & ":" & FIELD.modifierValue, "")
      Else
        'SQ 5-07-07
        'Gestione nomi pi� lunghi di 10: (si pu� gestire molto meglio; per ora ci accontentiamo...
        If Len(cblName) < 10 Then
          'silvia 8/9/2008
          If Len(FIELD.dupFactor) Or FIELD.dupFactor > "0" Then
            cblPic = Space(7) + Space(cblLevel - 1) & cblLevel & " " & cblName & "." & vbCrLf & _
                     Space(7) + Space(cblLevel + 1) & format(cblLevel + 2, "00") & " " & cblName & "-I" & Space(10 - Len(cblName)) & " PIC " & cblType(0) & "(" & cblLen & ") " & cblType(1)
            cblPic = cblPic & " OCCURS " & FIELD.dupFactor
          Else
            cblPic = Space(7) + Space(cblLevel - 1) & cblLevel & " " & cblName & Space(15 - Len(cblName)) & " PIC " & cblType(0) & "(" & cblLen & ") " & cblType(1)
          End If
          If Len(cblvalue) > 72 - (Len(Mid(cblPic, InStrRev(vbCrLf & cblPic, vbCrLf))) + 7) Then
            cblvalue = getCblValue(cblvalue, Len(Mid(cblPic, InStrRev(vbCrLf & cblPic, vbCrLf))) + 7)
          End If
          statement = IIf(Len(statement), statement & vbCrLf, "") & cblPic & IIf(Len(cblvalue), " VALUE " & cblvalue, "") & "." & Space(40) & IIf(Len(FIELD.dupFactor), "#DUP:" & FIELD.dupFactor & ":" & FIELD.modifierValue, "")
        Else
          'a capo:
          'silvia 8/9/2008
          If Len(FIELD.dupFactor) Or FIELD.dupFactor > "0" Then
            cblPic = Space(7) + Space(cblLevel - 1) & cblLevel & " " & cblName & "." & vbCrLf & _
                     Space(7) + Space(cblLevel + 1) & format(cblLevel + 2, "00") & " " & cblName & "-I" & vbCrLf & _
                     Space(20) & " PIC " & cblType(0) & "(" & cblLen & ") " & cblType(1)
            cblPic = cblPic & " OCCURS " & FIELD.dupFactor
          Else
            cblPic = Space(7) + Space(cblLevel - 1) & cblLevel & " " & cblName & vbCrLf & _
                    Space(20) & " PIC " & cblType(0) & "(" & cblLen & ") " & cblType(1)
          End If
          If Len(cblvalue) > 72 - (Len(Mid(cblPic, InStrRev(cblPic, vbCrLf))) + 7) Then   ' 7 � la len(" VALUE ")
            cblvalue = getCblValue(cblvalue, Len(Mid(cblPic, InStrRev(vbCrLf & cblPic, vbCrLf))) + 7)
          End If
          statement = IIf(Len(statement), statement & vbCrLf, "") & cblPic & IIf(Len(cblvalue), " VALUE " & cblvalue, "") & "." & Space(50) & IIf(Len(FIELD.dupFactor), "#DUP:" & FIELD.dupFactor & ":" & FIELD.modifierValue, "")
      End If
    End If
    tagWrite statement
    
    'TMP: GESTIONE LISTA DATI... DOVREMO FARE 2 GIRI...
    dataList(listIndex) = FIELD
    listIndex = listIndex + 1
    End If
  End If
  
  If operator = "DC" Then
    DoEvents
  ElseIf operator = "DS" Then
    DoEvents
  Else
    'mai
  End If
  
  'restore
  CurrentTag = previousTag
  Exit Sub
errorHandler:
  'tmp:
  If err.Number = 123 Then  'nextToken: gestire...
    'tmp: nextToken...
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "#getData: " & GbIdOggetto & "-" & err.description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
  End If
  writeLog "E", "#getData: " & GbIdOggetto & "-" & err.description
  ''Close fdOUT
End Sub
'''''''''
' Funzione per VALUE su pi� righe
' Silvia 8/9/2008
'''''''
Private Function getCblValue(cblvalue As String, cblpiclen As Long) As String
Dim CValue As String, CValue1 As String
Dim i As Integer

  CValue1 = Left(cblvalue, 72 - cblpiclen) & vbCrLf
  CValue = "'" & Mid(cblvalue, Len(CValue1) - 1)
  i = Len(cblvalue)
  While i > 1
    CValue1 = CValue1 & Space(6) & "-" & Space(4) & Left(CValue, 72 - 11) & vbCrLf
    CValue = "'" & Mid(CValue, 72 - 11 + 1)
    i = Len(CValue)
  Wend
  getCblValue = Left(CValue1, Len(CValue1) - 2)
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Operandi "binari"... ritorna il primo gi� convertito...
' Ps: tornare struttura, non stringa!
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function nextOperand(parameters As String) As String
  Dim statement As String, cmpString As String, cmpAddress As String, token As String
  Dim address() As String
  Dim base As String, Offset As String
  
  On Error GoTo nextTokenErr
  
  ReDim address(0)
  '''''''''''''''''''
  ' Analisi operandi
  '''''''''''''''''''
  token = nextToken_tmp(parameters)  'si ferma alla virgola o alla parentesi o all'apice:
  cmpAddress = token
  If Left(parameters, 1) = "(" Then
    token = nextToken_tmp(parameters)
    address = Split(Replace(Replace(token, "(", ""), ")", ""), ",")
    If UBound(address) <> 1 Then
      If Len(address(0)) Then
        'CHIARIRE: base o offset?!
        'Indirizzamento con il +?
        base = 1
        Offset = address(0)
        'verifica sotto... comune!
      Else
        writeLog "W", "getCompare: " & token
      End If
    Else
      base = address(1)
      Offset = address(0)
    End If
  ElseIf Left(parameters, 1) = "'" Then
    'Literal: SICURO???????????
    cmpAddress = cmpAddress & parameters
  End If
  
  'Indirizzamento con il +
  If InStr(cmpAddress, "+") Then
    base = Mid(cmpAddress, InStr(cmpAddress, "+") + 1)
    cmpAddress = Left(cmpAddress, InStr(cmpAddress, "+") - 1)
  End If
  
  'Register?
  If IsNumeric(cmpAddress) Then
    cmpAddress = "R-" & cmpAddress
  End If
  'Base+offset?
  If Len(base) Then
    cmpAddress = cmpAddress & "(" & base & ":" & Offset & ")"
  End If
  
  nextOperand = cmpAddress
  
  Exit Function
nextTokenErr:
  'tmp
  MsgBox err.description
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' SQ 20-08-08 (da fare meglio):
' - Formato R1,R2
' - Formato D2(X2,B2)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getOperand(parameters As String, asmOperand As operand)
  Dim statement As String, cmpString As String, token As String
  Dim address() As String
  Dim param As String
  Dim operandDup As Integer
  Dim i As Integer
  Dim Occurs As Integer
  
  param = parameters
  
  On Error GoTo nextTokenErr
  
  ReDim address(0)
  
  Select Case asmOperand.format
    Case "D1(L,B1)", "D2(L,B2)", "D1(L1,B1)", "D2(L2,B2)"
      If Left(parameters, 1) = "=" Then
        'IMMEDIATE like: forzo il formato...
        parameters = Mid(parameters, 2)
        asmOperand.format = "I" & Mid(asmOperand.format, 2, 1)
        If Left(parameters, 1) = "C" Then
          asmOperand.iType = "C"
          asmOperand.value = Mid(parameters, 2)
          asmOperand.len = Len(asmOperand.value) - 2
        ElseIf Left(parameters, 1) = "X" Then
          asmOperand.iType = "X"
          asmOperand.value = Mid(parameters, 2)
          asmOperand.len = (Len(asmOperand.value) - 2) / 2
        ElseIf Left(parameters, 1) = "P" Then
          asmOperand.iType = "P"
          If Mid(parameters, 2, 1) = "L" Then
            asmOperand.len = Mid(parameters, 3, InStr(parameters, "'") - 3)
            asmOperand.value = Replace(Mid(parameters, InStr(parameters, "'")), "'", "")
          Else
            asmOperand.value = Replace(Mid(parameters, 2), "'", "")
            asmOperand.len = Len(asmOperand.value)
          End If
        'ElseIf Left(parameters, 1) = "" Then...
        Else
          writeLog "W", "Type to be managed: " & param
        End If
      Else
        token = nextToken_tmp(parameters)  'si ferma alla virgola o alla parentesi o all'apice:
        asmOperand.Displace = Replace(token, "_", "-")
        If Left(parameters, 1) = "(" Then
          token = nextToken_tmp(parameters)
          address = Split(Replace(Replace(token, "(", ""), ")", ""), ",")
          If UBound(address) <> 1 Then
            If Len(address(0)) Then
              'SQ 5-07-07 Pezza: casi non numerici tipo: L'C02-256
              asmOperand.len = IIf(IsNumeric(address(0)), address(0), 0)
            Else
              writeLog "W", "getCompare: " & token
            End If
          Else
            'SQ 5-07-07 Pezza: casi non numerici tipo: L'C02-256
            asmOperand.len = IIf(IsNumeric(address(0)), address(0), 0)
            asmOperand.base = Replace(Replace(address(1), "R", ""), "-", "")
            'SQ 26-08-08 registro o campo: (giusto? verificare)
            If IsNumeric(asmOperand.base) Then 'tmp: gestire EQU
              'REGISTRO
              asmOperand.iType = "REG"
            Else
              'CAMPO
              'ok
            End If
          End If
        ElseIf Left(parameters, 1) = "'" Then
          'Literal: SICURO???????????
          'asmOperand.displace = asmOperand.displace & parameters
        Else
          'CAMPO - recupero info
          getDataInfo asmOperand
          'Controllo len:
          If asmOperand.len = 0 Then
            writeLog "W", "L Unknown: " & param
          End If
        End If
        'Indirizzamento con il +
        If InStr(asmOperand.Displace, "+") Then
          asmOperand.displacePlus = Mid(asmOperand.Displace, InStr(asmOperand.Displace, "+") + 1)
          asmOperand.Displace = Left(asmOperand.Displace, InStr(asmOperand.Displace, "+") - 1)
        'Mauro 12/09/2008: Gestione campi con spiazzamento "-"
        ElseIf InStr(asmOperand.Displace, "-") Then
          AddFields_Minus asmOperand
        End If
      End If
    'SQ! GESTIRE CASI: L'ECBTAB(R8)
    Case "D1(X1,B1)", "D2(X2,B2)"
      'tmp: copiato da D1(L,B1)
      If Left(parameters, 1) = "=" Then
        'IMMEDIATE like: forzo il formato...
        parameters = Mid(parameters, 2)
        asmOperand.format = "I" & Mid(asmOperand.format, 2, 1)
        If Left(parameters, 1) = "C" Then
          asmOperand.iType = "C"
          asmOperand.value = Mid(parameters, 2)
          asmOperand.len = Len(asmOperand.value) - 2
        ElseIf Left(parameters, 1) = "X" Then
          'SQ 27-08-08 GESTIONE  "=XL4'F'"
          'Farlo OVUNQUE!!!!!!!!!!!!!!!!!!!!!!
          asmOperand.iType = "X"
          If Mid(parameters, 2, 1) = "L" Then
            statement = Mid(parameters, 3)
            token = nextToken_tmp(statement)  'si ferma alla virgola o alla parentesi o all'apice:
            If IsNumeric(token) Then
              operandDup = token
            Else
              writeLog "E", "Unknown operand format: " & param
              operandDup = 1
            End If
          Else
            statement = Mid(parameters, 2)
            operandDup = 1
          End If
          asmOperand.value = Replace(statement, "'", "")
          asmOperand.len = (Len(asmOperand.value) + 0.9) / 2 'coppie: se dispari devo aggiungere 1!
          asmOperand.len = asmOperand.len * operandDup
        ElseIf Left(parameters, 1) = "P" Then
          asmOperand.iType = "P"
          asmOperand.value = Replace(Mid(parameters, 2), "'", "")
          asmOperand.len = Len(asmOperand.value)
        ElseIf Left(parameters, 1) = "F" Then
          asmOperand.iType = "F"
          asmOperand.value = Replace(Mid(parameters, 2), "'", "")
          asmOperand.len = Len(asmOperand.value)
        ElseIf Left(parameters, 1) = "H" Then
          asmOperand.iType = "H"
          asmOperand.value = Replace(Mid(parameters, 2), "'", "")
          asmOperand.len = Len(asmOperand.value)
        ElseIf Left(parameters, 1) = "A" Then
          asmOperand.iType = "A"
          asmOperand.value = "ADDR-" & Replace(Mid(parameters, 3), ")", "")
          asmOperand.len = 4
        Else
          writeLog "E", "Unknown operand type: " & param
        End If
      Else
        token = nextToken_tmp(parameters)  'si ferma alla virgola o alla parentesi o all'apice:
        asmOperand.Displace = token
        If Left(parameters, 1) = "(" Then
          token = nextToken_tmp(parameters)
          address = Split(Replace(Replace(token, "(", ""), ")", ""), ",")
          If UBound(address) = 0 Then
            asmOperand.base = Replace(Replace(address(0), "R", ""), "-", "")
            'SQ 26-08-08 registro o campo: (giusto? verificare)
            If IsNumeric(asmOperand.base) Or (Len(asmOperand.base) = 1 And asmOperand.base >= "A" And asmOperand.base <= "F") Then 'tmp: gestire EQU
              'REGISTRO
              asmOperand.iType = "REG"
            Else
              'CAMPO
              'ok
            End If
          Else
            'SQ: pezza. se non uso gli alias, mi perdo la roba! (es: "R3")
            If Left(address(0), 1) = "R" And Len(address(0)) >= 2 Then
              If IsNumeric(Mid(address(0), 2)) Then address(0) = Mid(address(0), 2)
            End If
            asmOperand.Index = IIf(IsNumeric(address(0)), address(0), 0)
            
            'SQ 5-07-07 Pezza: casi non numerici tipo: L'C02-256
            'sq (verificare: dovrebbe essere per forza un registro)
            asmOperand.base = Replace(Replace(address(1), "R", ""), "-", "")
          End If
        ElseIf Left(parameters, 1) = "'" Then
          'SQ Gestione LUNGHEZZA: L'CAMPO1(...)
          If token = "L" Then
            token = nextToken_tmp(parameters)
            asmOperand.Displace = getFieldLen(token)
            asmOperand.Displace = statement
          Else
            'ma de che stamo?
          End If
        End If
        'Indirizzamento con il +
        If InStr(asmOperand.Displace, "+") Then
          asmOperand.displacePlus = Mid(asmOperand.Displace, InStr(asmOperand.Displace, "+") + 1)
          asmOperand.Displace = Left(asmOperand.Displace, InStr(asmOperand.Displace, "+") - 1)
        'Mauro 12/09/2008: Gestione campi con spiazzamento "-"
        ElseIf InStr(asmOperand.Displace, "-") Then
          AddFields_Minus asmOperand
        End If
      End If
    Case "D1(B1)", "D2(B2)"
      If Left(parameters, 1) = "=" Then
        'IMMEDIATE like: forzo il formato... (SQ: SICURO? CHI SFRUTTA 'STA COSA?!)
        parameters = Mid(parameters, 2)
        asmOperand.format = "I" & Mid(asmOperand.format, 2, 1)
        ' Mauro24/03/2011
        Occurs = 0
        If IsNumeric(Left(parameters, 1)) Then
          i = 2
          Do While IsNumeric(Mid(parameters, i, 1))
            DoEvents
          Loop
          Occurs = Left(parameters, i - 1)
          parameters = Mid(parameters, i)
        End If
        ''''''''''''''''''''''
        If Left(parameters, 1) = "C" Then
          asmOperand.iType = "C"
          ' Mauro 10/09/2008
          If Mid(parameters, 2, 1) = "L" Then
            i = 3
            Do While IsNumeric(Mid(parameters, i, 1))
              asmOperand.len = asmOperand.len & Mid(parameters, i, 1)
              i = i + 1
            Loop
            asmOperand.value = Mid(parameters, i)
          Else
            If Occurs Then
              asmOperand.value = ""
              For i = 1 To Occurs
                asmOperand.value = asmOperand.value & Replace(Mid(parameters, 2), "'", "")
              Next i
              asmOperand.value = "'" & asmOperand.value & "'"
              asmOperand.len = Occurs
            Else
              asmOperand.value = Mid(parameters, 2)
              asmOperand.len = Len(asmOperand.value) - 2
            End If
          End If
        ElseIf Left(parameters, 1) = "X" Then
          asmOperand.iType = "X"
          If Occurs Then
            asmOperand.value = ""
            For i = 1 To Occurs
              asmOperand.value = asmOperand.value & Replace(Mid(parameters, 2), "'", "")
            Next i
            asmOperand.value = "'" & asmOperand.value & "'"
            asmOperand.len = Occurs
          Else
            asmOperand.value = Mid(parameters, 2)
            asmOperand.len = (Len(asmOperand.value) - 2) / 2
          End If
        ElseIf Left(parameters, 1) = "P" Then
          asmOperand.iType = "P"
          asmOperand.value = Replace(Mid(parameters, 2), "'", "")
          asmOperand.len = Len(asmOperand.value)
        ElseIf Left(parameters, 1) = "F" Then
          asmOperand.iType = "F"
          asmOperand.value = Replace(Mid(parameters, 2), "'", "")
          asmOperand.len = (Len(asmOperand.value) - 2) '/ 2
        ElseIf Left(parameters, 1) = "H" Then
          asmOperand.iType = "H"
          asmOperand.value = Replace(Mid(parameters, 2), "'", "")
          asmOperand.len = Len(asmOperand.value)
        ElseIf Left(parameters, 1) = "A" Then
          asmOperand.iType = "A"
          asmOperand.value = "ADDR-" & Replace(Mid(parameters, 3), ")", "")
          asmOperand.len = 4
        Else
          writeLog "W", "Unknown Operand type: " & parameters
        End If
      Else
        'token = nextToken_tmp(parameters)  'si ferma alla virgola o alla parentesi o all'apice:
        token = nextToken_Operand(parameters)  'si ferma alla virgola o alla parentesi o all'apice:
        asmOperand.Displace = Replace(token, "_", "-")
        If Left(parameters, 1) = "(" Then
          token = nextToken_tmp(parameters)
          If Len(token) Then
            asmOperand.base = Replace(Replace(Replace(Replace(token, "(", ""), ")", ""), "R", ""), "-", "")
            'SQ 22-08-08 registro o campo: (giusto? verificare)
            If IsNumeric(asmOperand.base) Then 'tmp: gestire EQU
              'REGISTRO
              asmOperand.iType = "REG"
            Else
              'CAMPO
              'ok
            End If
          Else
            writeLog "W", "getOperand: " & token
          End If
        ElseIf Left(parameters, 1) = "'" Then
          'Literal: SICURO???????????
          asmOperand.Displace = asmOperand.Displace & parameters
        Else
          'SQ 16-03-2010 - le info mi possono servire lo stesso... per� metterlo nel punto giusto!
          getDataInfo asmOperand
        End If
        'Indirizzamento con il +
        If InStr(asmOperand.Displace, "+") Then
          asmOperand.displacePlus = Mid(asmOperand.Displace, InStr(asmOperand.Displace, "+") + 1)
          asmOperand.Displace = Left(asmOperand.Displace, InStr(asmOperand.Displace, "+") - 1)
        'Mauro 12/09/2008: Gestione campi con spiazzamento "-"
        ElseIf InStr(asmOperand.Displace, "-") Then
          AddFields_Minus asmOperand
        End If
      End If
    Case "I1", "I2"
      If Left(parameters, 1) = "C" Then
        asmOperand.iType = "C"
        asmOperand.value = Mid(parameters, 2)
        asmOperand.len = Len(asmOperand.value) - 2
      ElseIf Left(parameters, 1) = "X" Then
        asmOperand.iType = "X"
        asmOperand.value = Mid(parameters, 2)
        asmOperand.len = (Len(asmOperand.value) - 2) / 2
        ElseIf Left(parameters, 1) = "P" Then
          asmOperand.iType = "P"
          asmOperand.value = Replace(Mid(parameters, 2), "'", "")
          asmOperand.len = Len(asmOperand.value)
        ElseIf Left(parameters, 1) = "F" Then
          asmOperand.iType = "F"
          asmOperand.value = Replace(Mid(parameters, 2), "'", "")
          asmOperand.len = (Len(asmOperand.value) - 2) '/ 2
        ElseIf Left(parameters, 1) = "H" Then
          asmOperand.iType = "H"
          asmOperand.value = Replace(Mid(parameters, 2), "'", "")
          asmOperand.len = (Len(asmOperand.value) - 2) '/ 2
      Else
        If IsNumeric(parameters) Then
          'ES: MOVE FIELD1,6   (non � il registro!)
          asmOperand.iType = "SQ-BIN" 'TMP!
          asmOperand.value = parameters
          asmOperand.len = -1 'non definita
        Else
          writeLog "E", "Unknown Operand type: " & parameters
        End If
      End If
    'SQ 20-08-08
    Case "R", "R1", "R2"
      'SQ verificare bene: DA DISTINGUERE CON GLI "IMMEDIATE!"
      asmOperand.iType = "REG"
      asmOperand.value = nextToken_tmp(parameters)
      'TMP: GESTIRE EQU!!!
      asmOperand.value = Replace(Replace(asmOperand.value, "R", ""), "-", "")
    Case Else
      token = nextToken_tmp(parameters)  'si ferma alla virgola o alla parentesi o all'apice:
      asmOperand.Displace = token
      If Left(parameters, 1) = "(" Then
        token = nextToken_tmp(parameters)
        address = Split(Replace(Replace(token, "(", ""), ")", ""), ",")
        If UBound(address) <> 1 Then
          If Len(address(0)) Then
            'CHIARIRE: asmOperand.index o asmOperand.len?!
            'Indirizzamento con il +?
            'asmOperand.index = 1
            asmOperand.len = address(0)
            'verifica sotto... comune!
          Else
            writeLog "W", "getCompare: " & token
          End If
        Else
          asmOperand.Index = address(1)
          asmOperand.len = address(0)
        End If
      ElseIf Left(parameters, 1) = "'" Then
        'Literal: SICURO???????????
        asmOperand.Displace = asmOperand.Displace & parameters
      End If
      
      'Indirizzamento con il +
      If InStr(asmOperand.Displace, "+") Then
        asmOperand.Index = Mid(asmOperand.Displace, InStr(asmOperand.Displace, "+") + 1)
        asmOperand.Displace = Left(asmOperand.Displace, InStr(asmOperand.Displace, "+") - 1)
      'Mauro 12/09/2008: Gestione campi con spiazzamento "-"
      ElseIf InStr(asmOperand.Displace, "-") Then
        AddFields_Minus asmOperand
      End If
  End Select
  
  If asmOperand.iType = "REG" And asmOperand.len = 0 Then
    asmOperand.len = 4
  End If
  Exit Sub
nextTokenErr:
  'tmp
  writeLog "E", "Unexpected error: " & err.description
  'Resume
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Formatta l'operando in "formato" Cobol
'' SQ 20-08-08 (da fare meglio):
' - Formato R1,R2
' - Formato D2(X2,B2)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getOperand_CBL(asmOperand As operand, Optional isDisplace As Boolean = False) As String
  Dim cblOperand As String
  Dim FIELD As String
  Dim dispField As String, lenField As Long
  Dim i As Integer, Displace As String
  Dim isPlus As Boolean
  
  On Error GoTo nextTokenErr
  
  ReDim address(0)
  
  Select Case asmOperand.format
    Case "D1(L,B1)", "D2(L,B2)", "D1(L1,B1)", "D2(L2,B2)"
      'Register?
      If IsNumeric(asmOperand.Displace) Then
        'SQ old:
        'If IsNumeric(asmOperand.base) Then
        '  'cblOperand = "R-" & asmOperand.displace
        '  cblOperand = "R-" & asmOperand.base
        'End If
        ''Base+offset?
        ''If Len(asmOperand.base) Then
        'If Len(asmOperand.displace) Then
        '  cblOperand = cblOperand & "(" & CInt(asmOperand.displace) + 1 & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
        'End If
        'REGISTER:
        If asmOperand.iType = "REG" Then
          'Cerco il puntamento:
          FIELD = getRegisters(asmOperand.base)
          FIELD = isLabel(FIELD)
          
          If Len(FIELD) Then
            cblOperand = FIELD & "(I-" & asmOperand.base
            If Len(asmOperand.Displace) Then
              cblOperand = cblOperand & " + " & CInt(asmOperand.Displace) + 1 & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
            Else
             MsgBox "tmp: getOperand_CBL - No displace?"
            End If
          Else
            cblOperand = "AREA-" & asmOperand.base
            If Len(asmOperand.Displace) Then
              'SQ: le aree
              cblOperand = cblOperand & "(1" & IIf(CInt(asmOperand.Displace), " + " & CInt(asmOperand.Displace), "") & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
            Else
             MsgBox "tmp: getOperand_CBL - No displace?"
            End If
          End If
        Else
          'Base+offset?
          If Len(asmOperand.Displace) Then
            'cblOperand = cblOperand & "(" & CInt(asmOperand.displace) + 1 & ":" & ")"
            cblOperand = cblOperand & "(1" & IIf(CInt(asmOperand.Displace), " + " & CInt(asmOperand.Displace), "") & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
          End If
        End If
      Else
        'Campo (o alternative?)
        cblOperand = Replace(asmOperand.Displace, "_", "-")
        cblOperand = isLabel(cblOperand) ' Per ora metto solo "L-" in testa, ma dovr� anche essere definito
        'SQ: il posizionamento non mi serve se non ho l'offset (L � stata ricavata proprio dal campo)
        If CInt("0" & asmOperand.displacePlus) Or isDisplace Then
          cblOperand = cblOperand & "(" & CInt("0" & asmOperand.displacePlus) + 1 & ":"
          If asmOperand.len Then
            cblOperand = cblOperand & asmOperand.len
          End If
          cblOperand = cblOperand & ")"
        End If
      End If
    Case "D1(X1,B1)", "D2(X2,B2)"
      'SQ (fare meglio) copiato quello sopra!
      If IsNumeric(asmOperand.Displace) Then
        'REGISTER:
        If asmOperand.iType = "REG" Then
          'Cerco il puntamento:
          FIELD = getRegisters(asmOperand.base)
          FIELD = isLabel(FIELD)
          'SQ 2.0
          If Len(FIELD) Then
            cblOperand = FIELD & "(I-" & asmOperand.base
            If Len(asmOperand.Displace) Then
              cblOperand = cblOperand & " + " & CInt(asmOperand.Displace) + 1 & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
            Else
             MsgBox "tmp: getOperand_CBL - No displace?"
            End If
          Else
            cblOperand = "AREA-" & asmOperand.base
            If Len(asmOperand.Displace) Then
              cblOperand = cblOperand & "(1" & IIf(CInt(asmOperand.Displace), " + " & CInt(asmOperand.Displace), "") & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
            Else
             MsgBox "tmp: getOperand_CBL - No displace?"
            End If
          End If
          
        Else
          'Base+offset?
          If Len(asmOperand.Displace) Then
            'cblOperand = cblOperand & "(" & CInt(asmOperand.displace) + 1 & ":" & ")"
            cblOperand = cblOperand & "(" & CInt(asmOperand.Displace) + 1 & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
          End If
          
        End If
      Else
        'Campo - o alternative?
        cblOperand = Replace(asmOperand.Displace, "_", "-")
        cblOperand = isLabel(cblOperand) ' Per ora metto solo "L-" in testa, ma dovr� anche essere definito
        If CInt("0" & asmOperand.displacePlus) Then
          cblOperand = cblOperand & "(" & CInt("0" & asmOperand.displacePlus) + 1
        End If
        'SQ: gestire l'index!!!!
        If Len(asmOperand.Index) Then
          cblOperand = cblOperand & asmOperand.len & " + " & CInt(asmOperand.Index) 'verificare tutto!
        End If
        ''''''''''''''''''''''''''''
        ' Gestione casi: FIELD1(R1)
        ' SQ 8-9-08
        ''''''''''''''''''''''''''''
        If Len(asmOperand.base) Then
          ' MAURO: MOLTO TEMPORANEO
          'cblOperand = cblOperand & " + I-" & asmOperand.base
          cblOperand = "I-" & asmOperand.base & "(" & cblOperand & ":)"
        End If
        If CInt("0" & asmOperand.displacePlus) Then cblOperand = cblOperand & ":" & ")"
      End If
    Case "D1(B1)", "D2(B2)"
      'Register?
      If IsNumeric(asmOperand.Displace) Then
        'SQ 22-08-08
        'REGISTER:
        If asmOperand.iType = "REG" Then
          'Cerco il puntamento:
          FIELD = getRegisters(asmOperand.base)
          FIELD = isLabel(FIELD)
          'se non lo trova, metto "PTR-"... sar� da sistemare a mano!
          'SQ 2.0
          ''cblOperand = IIf(Len(FIELD), FIELD, "PTR-" & asmOperand.base) & "(I-" & asmOperand.base
          ''If Len(asmOperand.displace) Then
          ''  cblOperand = cblOperand & " + " & CInt(asmOperand.displace) + 1 & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
          ''Else
          '' MsgBox "tmp: getOperand_CBL - No displace?"
          ''End If
          If Len(FIELD) Then
            cblOperand = FIELD & "(I-" & asmOperand.base
            If Len(asmOperand.Displace) Then
              cblOperand = cblOperand & " + " & CInt(asmOperand.Displace) + 1 & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
            Else
             MsgBox "tmp: getOperand_CBL - No displace?"
            End If
          Else
            cblOperand = "AREA-" & asmOperand.base
            If Len(asmOperand.Displace) Then
              cblOperand = cblOperand & "(1" & IIf(CInt(asmOperand.Displace), " + " & CInt(asmOperand.Displace), "") & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
            Else
             MsgBox "tmp: getOperand_CBL - No displace?"
            End If
          End If
        Else
          'Base+offset?
          'If Len(asmOperand.base) Then
          If Len(asmOperand.Displace) Then
            'cblOperand = cblOperand & "(" & CInt(asmOperand.displace) + 1 & ":" & ")"
            cblOperand = cblOperand & "(" & CInt(asmOperand.Displace) + 1 & ":" & IIf(asmOperand.len, asmOperand.len, "") & ")"
          End If
        End If
      Else
        'Campo - o alternative?
        If Left(asmOperand.Displace, 1) = "=" Then
        
        Else
          cblOperand = Replace(asmOperand.Displace, "_", "-")
          cblOperand = isLabel(cblOperand) ' Per ora metto solo "L-" in testa, ma dovr� anche essere definito
          If Not IsNumeric(asmOperand.displacePlus) And Len(asmOperand.displacePlus) > 0 Then
            If Left(asmOperand.displacePlus, 1) = "L" Then
              dispField = Mid(asmOperand.displacePlus, InStr(asmOperand.displacePlus, "'") + 1)
              isPlus = True
              i = InStr(dispField, "+")
              If i = 0 Then
                isPlus = False
                i = InStrRev(dispField, "-")
              End If
              Displace = Mid(dispField, i + 1)
              dispField = Left(dispField, i - 1)
              If isPlus Then
                lenField = getFieldLen(dispField) + Displace
              Else
                lenField = getFieldLen(dispField) - Displace
              End If
              cblOperand = cblOperand & "(" & CInt("0" & lenField) + 1 & ":"
              If asmOperand.len Then
                cblOperand = cblOperand & asmOperand.len
              End If
              cblOperand = cblOperand & ")"
            End If
          ElseIf CInt("0" & asmOperand.displacePlus) Or isDisplace Then
            cblOperand = cblOperand & "(" & CInt("0" & asmOperand.displacePlus) + 1 & ":"
            If asmOperand.len Then
              cblOperand = cblOperand & asmOperand.len
            End If
            cblOperand = cblOperand & ")"
          End If
        End If
      End If
    Case "I1", "I2"
      If Left(asmOperand.iType, 1) = "C" Then
        cblOperand = asmOperand.value
      ElseIf Left(asmOperand.iType, 1) = "X" Then
        cblOperand = "X" & asmOperand.value
      ElseIf Left(asmOperand.iType, 1) = "P" Then
        cblOperand = Replace(asmOperand.value, "'", "")
      ElseIf Left(asmOperand.iType, 1) = "H" Then
        cblOperand = Replace(asmOperand.value, "'", "")
      ElseIf Left(asmOperand.iType, 1) = "F" Then
        cblOperand = Replace(asmOperand.value, "'", "")
      ElseIf Left(asmOperand.iType, 1) = "A" Then
        'NON GESTITO:
        writeLog "W", "Address to be managed: " & asmOperand.value
        cblOperand = asmOperand.value
      ElseIf asmOperand.iType = "SQ-BIN" Then
        cblOperand = asmOperand.value
      Else
        writeLog "W", "Type to be managed: " & asmOperand.iType
      End If
    'SQ 20-08-08
    Case "R", "R1", "R2"
      'tmp: gestire EQU...
      'tmp: valutare se I-/R-/altro?
      cblOperand = "I-" & asmOperand.value
    Case Else
      writeLog "E", "Unknown input format: " & asmOperand.format
  End Select
  
  getOperand_CBL = cblOperand
  
  Exit Function
nextTokenErr:
  'tmp
  writeLog "E", "Unexpected error: " & err.description
End Function

' Mauro 04/09/2008
Private Function isLabel(cblOperand As String) As String
  Dim i As Long, prevTag As String
  
  On Error Resume Next
  isLabel = cblOperand
  For i = 0 To I_parag_Tot
    If cblOperand = Paragraphs(i).Name Then
      isLabel = "L-" & cblOperand
      Label_Def.Add "L-" & cblOperand, "L-" & cblOperand
      Exit For
    End If
  Next i
End Function

'''''''''''''''''''''''''''''''''''''
' Gestione campi Packed Aggiunti
' SQ 20-08-08 gestione campi "posizionali" (x:y)
'''''''''''''''''''''''''''''''''''''
Private Function getAddedName(dataName As String) As String
  Dim i As Integer, j As Integer
  
  On Error GoTo catch
  
  ' Mauro 01/09/2008
  If InStr(dataName, "(") > 0 And InStr(dataName, "I-") = 0 Then
    dataName = operandName(dataName)
  End If
  'Gi� stato aggiunto?
  For j = 0 To addedIndex - 1
    If dataName = addedList(j) Then
      'Gi� aggiunto!
      getAddedName = dataName & "-P"
      Exit Function
    End If
  Next j
  
  For i = 0 To listIndex - 1
    If dataName = dataList(i).Name Then
      'Se non � gi� Packed il campo � da aggiungere
      If dataList(i).Type <> "P" Then
        getAddedName = dataName & "-P"
        'SQ pezza: gestire
        If Len(dataList(i).modifierValue) = 0 Then
          writeLog "E", dataName & " - modifierValue=0!"
          'tmp:
          dataList(i).modifierValue = 999
        End If
          'tmp: scrivo tutto alla fine!
        packedList(packedIndex) = "#P" & Space(6) & Space(dataList(i).level - 1) & _
                      format(dataList(i).level, "00") & " " & getAddedName & " REDEFINES " & dataName & _
                      " PIC 9(" & (((dataList(i).modifierValue - 1) * 2) + 1) & ") COMP-3."
        packedIndex = packedIndex + 1
      Else
        getAddedName = dataName
      End If
      'Aggiorno lista:
      addedList(addedIndex) = dataName
      addedIndex = addedIndex + 1
      Exit For
    End If
  Next i
  If i > listIndex - 1 Then
    'writeLog "non trovato!: " & dataName
    'Sar� su una copy!?
    getAddedName = dataName & "-P"
    For j = 0 To missIndex - 1
      If dataName = missList(j) Then
        'Gi� aggiunto!
        Exit Function
      End If
    Next
    missList(missIndex) = dataName
    missIndex = missIndex + 1
  End If
  
  Exit Function
catch:
  writeLog "E", dataName & " - " & err.description
End Function

Private Function operandName(dataName As String) As String
  Dim openPar As Integer, closePar As Integer, duePunti As Integer
  
  openPar = InStr(dataName, "(")
  closePar = InStr(dataName, ")")
  duePunti = InStr(dataName, ":")
  operandName = Left(dataName, openPar - 1) & "-"
  operandName = operandName & Mid(dataName, openPar + 1, duePunti - openPar - 1)
  If duePunti + 1 < closePar Then
    operandName = operandName & "-" & Mid(dataName, duePunti + 1, closePar - duePunti - 1)
  End If
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ritorna la lunghezza di un campo
' input: nome campo
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getFieldLen(fieldName As String) As String
  Dim dummy As operand
  
  dummy.Displace = fieldName
  getDataLen dummy
  getFieldLen = dummy.len
  
End Function
Private Sub getDataLen(asmOperand As operand)
  Dim i As Integer
  
  For i = 0 To listIndex
    If asmOperand.Displace = dataList(i).Name Then
      asmOperand.len = "0" & dataList(i).modifierValue
      Exit For
    End If
  Next
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''
' Setta:
' - asmOperand.len
' - asmOperand.itype
''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getDataInfo(asmOperand As operand)
  Dim i As Integer
  
  For i = 0 To listIndex
    If asmOperand.Displace = dataList(i).Name Then
      asmOperand.len = "0" & dataList(i).modifierValue
      asmOperand.iType = dataList(i).Type
      Exit For
    End If
  Next i
End Sub
''''''''''''''''''''''''''
'
''''''''''''''''''''''''''
Private Sub getRETURN(parameters As String)
  tagWrite Space(6) & "*" & vbCrLf & _
           "RETURN     GOBACK." & vbCrLf & _
           Space(6) & "*" & Space(64) & "*" & vbCrLf & _
           Space(6) & String(66, "*")
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'SQ - ULTERIORE NEXTTOKEN: DA UNIRE ALL'UNICO... COME GLI ALTRI!!!
'- separatore: space
'- "literal": apice
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function nextToken_ASM(inputString As String)
  Dim level As Integer, i As Integer
  Dim currentChar As String
  
  'Mangio i bianchi davanti (primo giro...)
  inputString = LTrim(inputString)
  If Len(inputString) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "'"
        'Attenzione: gestire gli apici "innestati" ('')
        inputString = Mid(inputString, 2)
        i = InStr(inputString, "'")
        If i Then
          nextToken_ASM = Left(inputString, InStr(inputString, "'") - 1)
          inputString = Mid(inputString, InStr(inputString, "'") + 1)
        Else
          'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
          inputString = "''" & inputString
          err.Raise 123, "nextToken_ASM", "syntax error on " & inputString
          Exit Function
        End If
      Case Else
        '20-10-06: modificato...
        For i = 1 To Len(inputString)
          currentChar = Mid(inputString, i, 1)
          If currentChar = "'" Then
            If level = 0 Then
              level = level + 1 'inizio
            Else
              level = level - 1 'fine
            End If
          ElseIf currentChar = " " Then
            If level = 0 Then Exit For
          End If
          nextToken_ASM = nextToken_ASM & currentChar
        Next i
        inputString = Mid(inputString, i)
    End Select
  End If
End Function
'ritorna la parola successiva (space unico separatore)
'Considera come delimitatore di "token" le parentesi e gli apici singoli...
'Effetto Collaterale: mangia...
Function nextToken_Operand(inputString As String)
  Dim level As Integer, i As Integer, j As Integer
  Dim currentChar As String, inputStringCopy As String
  
  level = 0
  If (Len(inputString)) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "("
        i = 2
        level = 1
        nextToken_Operand = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "("
              level = level + 1
              nextToken_Operand = nextToken_Operand & currentChar
            Case ")"
              level = level - 1
              nextToken_Operand = nextToken_Operand & currentChar
            Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nextToken_Operand = nextToken_Operand & currentChar
              Else
                err.Raise 123, "nextToken_Operand", "syntax error on " & inputString
                Exit Function
              End If

          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
        'SQ 7-03-6: ATTENZIONE ALLA VIRGOLA DOPO la parentesi!!!
        'Es: COND=(0,NE),PARM=
        If Left(inputString, 1) = "," Then
          inputString = Trim(Mid(inputString, 2))
        End If
      Case "'"
        inputString = Mid(inputString, 2)
        i = InStr(inputString, "'")
        If i Then
          nextToken_Operand = Left(inputString, InStr(inputString, "'") - 1)
          inputString = Trim(Mid(inputString, InStr(inputString, "'") + 1))
        Else
          err.Raise 123, "nextToken_Operand", "syntax error on " & inputString
          Exit Function
        End If
      Case Else
        'Ricerca primo separatore: " " o ","
        For i = 1 To Len(inputString)
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "'"
              If level = 0 Then
                If Right(nextToken_Operand, 1) <> "L" Then
                  level = level + 1 'inizio
                End If
              Else
                level = level - 1 'fine
              End If
            Case " "
              If level = 0 Then
                nextToken_Operand = Left(inputString, i - 1)
                inputString = Trim(Mid(inputString, i + 1))
                Exit Function
              End If
            Case "("
              nextToken_Operand = Left(inputString, i - 1)
              inputString = Trim(Mid(inputString, i))
              'pezza: se ho pippo(...),... rimane una virgola iniziale:
              'If Left(inputString, 1) = "," Then inputString = Trim(Mid(inputString, 2))
              Exit Function
            Case ","
              nextToken_Operand = Left(inputString, i - 1)
              inputString = Trim(Mid(inputString, i + 1))
              Exit Function
            Case Else
              'avanza pure
          End Select
        Next i
        'nessun separatore:
        nextToken_Operand = inputString
        inputString = ""
    End Select
  End If
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub convertPGM(language As String)
  Set tagValues = New Collection
  Select Case language
    Case "COBOL"
      Asm2cobol = True
      'INIT tags
      tagValues.Add "", "PROCEDURE-DIVISION"
      tagValues.Add "", "WORKING-STORAGE"
      tagValues.Add "", "DATA-REDEFINES"
      'silvia 5/9/2008: Tag per File Definition
      tagValues.Add "", "ASSIGN"
      tagValues.Add "", "FD"
      tagValues.Add "", "FILE-STATUS"
      
      ' Mauro 29/08/2008: Tag per chiusure di perform
      tagValues.Add "", "LABELS-EX"
      tagValues.Add "", "LABEL-DEFINITION"
      
      'silvia 10/9/2008
      tagValues.Add "", "LABELS-TEMPLATE"
      '...
'      DataLevel = 0
'      listIndex = 0
      
      'INIT template
      Set RTB = MapsdF_Parser.rText
      On Error GoTo fileErr
      RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\Main"
      On Error GoTo catch
      CurrentTag = "PROCEDURE-DIVISION"

      changeTag RTB, "<PGM-NAME>", GbNomePgm

      deleteSegnalazioni GbIdOggetto
      parseASM

      writeTag "WORKING-STORAGE"
      writeTag "DATA-REDEFINES"
      writeTag "PROCEDURE-DIVISION"
      'silvia 5/9/2008: Tag per File Definition
      writeTag "ASSIGN"
      writeTag "FD"
      writeTag "FILE-STATUS"
      ' Mauro 29/08/2008: Tag per chiusure di perform
      writeTag "LABELS-EX"
      writeTag "LABEL-DEFINITION"
      'silvia 10/9/2008
      writeTag "LABELS-TEMPLATE"
      
      Dim wTestoBAL As String, i As Integer
      For i = 1 To UBound(arrMoveItBAL)
        wTestoBAL = Space(14) & "WHEN '" & arrMoveItBAL(i) & "'" & vbCrLf & _
                    Space(14) & "    GO TO " & arrMoveItBAL(i) & "-EX"
        changeTag RTB, "<MOVEIT(i)>", wTestoBAL
      Next i
      
      cleanTags RTB
      RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm, 1
      Asm2cobol = False
    Case "SEQFILES"
      Asm2seqfiles = True
      Set RTB = MapsdF_Parser.rText
      RTB.Text = ""
      On Error GoTo fileErr
      
      deleteSegnalazioni GbIdOggetto
      parseDataASM
      
      RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm & ".MOD", 1
      Asm2seqfiles = False
    Case Else
      MsgBox "TMP: not supported conversion", vbInformation, "i-4.Migration"
  End Select
  
  Exit Sub
fileErr:
  If err.Number = 52 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\" & migrationPath
    Resume
  ElseIf err.Number = 75 Then
    MsgBox "Template file '" & m_Fun.FnPathPrj & "\" & templatePath & "\Main' not found.", vbExclamation, Parser.PsNomeProdotto
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
  End If
  Exit Sub
catch:
  m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
End Sub

Private Sub tagWrite(Line As String)
  Dim tagValue As String
  'SILVIA 17/09/2008
  Dim stSpace As String
  Dim newline As String
  
  On Error GoTo catch
  If Len(Split(Line, vbCrLf)(0)) < 127 Then
    stSpace = Space(127 - Len(Split(Line, vbCrLf)(0)))
  Else
    stSpace = ""
  End If
  If UBound(Split(Line, vbCrLf)) > 0 Then
    newline = Split(Line, vbCrLf)(0) & stSpace & format(GbNumRec, "00000") & Mid(Line, Len(Split(Line, vbCrLf)(0)) + 1) & vbCrLf
  Else
    newline = Line & stSpace & format(GbNumRec, "00000") & vbCrLf
  End If
  tagValue = tagValues.item(CurrentTag)
  tagValues.Remove CurrentTag
  'SILVIA 12/9/2008
  tagValues.Add tagValue & newline, CurrentTag
  'tagValues.Add tagValue & line & vbCrLf, CurrentTag
  
  Exit Sub
catch:
  If err.Number = 5 Then '424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    tagValues.Add "", CurrentTag
    Resume
  End If
End Sub

Private Sub writeTag(Tag As String)
  Dim tagValue As String
  
  On Error GoTo catch
  
  tagValue = tagValues.item(Tag)
  changeTag RTB, "<" & Tag & ">", tagValue
  'attenzione: era "CurrentTag"!
  tagValues.Remove Tag
  tagValues.Add "", Tag
  
  Exit Sub
catch:
  If err.Number = 424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    'tagValues.Add "", Tag
    'Resume
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''
' Scrittura Commenti
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getComment(statement As String)
  On Error GoTo catch
  
  tagWrite Space(6) & statement
  
  Exit Sub
catch:
  writeLog "E", "parsePgm_I: error on getComment!"
End Sub
''''''''''''''''''''''''''''''''''''''''''''''
' Gestione EQU
' SQ: fare!!
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getEQU(label As String, statement As String)
  On Error GoTo catch
  
  If statement = "*" Then
    'butto
  Else
    'Gestione REGISTRI:
    If IsNumeric(statement) Then
      If CInt(statement) >= "0" And CInt(statement) <= "15" Then
        'REGISTRO:
        'per ora butto!...
      Else
        'gestire...
        tagWrite "#EQU  *" & label & "==" & statement
      End If
    Else
      tagWrite "#EQU  *" & label & "==" & statement
    End If
  End If
  Exit Sub
catch:
  writeLog "E", "parsePgm_I: error on getEQU!"
End Sub
''''''''''''''''''''''''''''''''''''''''''''''
' TMP
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getSVC(label As String, statement As String)
  On Error GoTo catch
  
  tagWrite vbCrLf & "#SVC  *" & statement & vbCrLf
  
  Exit Sub
catch:
  writeLog "E", "Unexpecte Error: " & err.description
End Sub
''''''''''''''''''''''''''''''''''''''''''''''
' Silvia 9/9/2008
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getWTO(label As String, parameters As String)
  Dim i As Integer
  Dim cblDisplay As String, cblvalue As String, cblWTO As String
  Dim param() As String
  Dim previousTag As String, statement As String

  On Error GoTo catch
  param = Split(parameters, ",")
  If InStr(param(0), "=") = 0 And Len(param(0)) + 2 > (72 - 12 - 6) Then
    cblvalue = getCblValue("'" & param(0) & "'", 12 + 6)
  Else
    cblvalue = "'" & param(0) & "'"
  End If
  For i = 1 To Label_Def.count
    If Label_Def.item(i) = "L-" & label Then
      Label_Def.Remove (i)
      Exit For
    End If
  Next i
  previousTag = CurrentTag
  CurrentTag = "LABEL-DEFINITION"
  cblWTO = Space(7) & "01 " & "L-" & label & "." & vbCrLf
  cblWTO = cblWTO & Space(9) & "03 " & "L-" & label & "-CMD" & " PIC X(8)." & vbCrLf
  cblWTO = cblWTO & Space(9) & "03 " & "L-" & label & "-TEXT" & " PIC X(" & Len(param(0)) & ")" & vbCrLf
  cblWTO = cblWTO & Space(12) & "VALUE " & cblvalue & "."
  tagWrite cblWTO
  CurrentTag = previousTag
  cblDisplay = "L-" & label & "-TEXT"
  tagWrite statement
  statement = operator & Space(Len(indent) - Len(operator)) & "DISPLAY " & cblDisplay & "."
  tagWrite statement
  
  Exit Sub
catch:
  writeLog "E", "Unexpecte Error: " & err.description
End Sub
''''''''''''''''''''''''''''''''''''''''''''''
' END: fine programma
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getEND(label As String, statement As String)
  On Error GoTo catch
  
  statement = "GOBACK."
  statement = operator & Space(Len(indent) - Len(operator)) & statement
  'colonna 72: parametri originali
  statement = statement & Space(72 - Len(statement)) & statement & vbCrLf
  statement = statement & "      *END-OF-PROGRAM*"
  statement = "      *" & vbCrLf & statement
  
  tagWrite statement
  Exit Sub
catch:
  writeLog "E", "Unexpecte Error: " & err.description
End Sub
''''''''''''''''''''''''''''''''''''''''''''''
' SQ 22-08-08
' Lista (ordinata) Paragrafi (labels)
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub setParagraph(label As String)
  
  I_parag = I_parag + 1
  ReDim Preserve Paragraphs(I_parag)
  
  Paragraphs(I_parag).Name = label
  ReDim Paragraphs(I_parag).regPtrs(0)
  
  'SQ 2-2-09 ("interazione utente" -> info su repository)
  initParagraph
  
End Sub

''''''''''''''''''''''''''''''''''''''''''''''
' SQ 22-08-08
' Associazioni REGISTRO|CAMPO per ogni PARAGRAFO:
''
' PARAGRAFO(i)
' - regX1 -> campoY1
' - regX2 -> campoY2
' - ...
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub setRegisters(register As String, FIELD As String)
  Dim i As Integer
  
  i = UBound(Paragraphs(I_parag).regPtrs)
  Paragraphs(I_parag).regPtrs(i).register = register
  
  Paragraphs(I_parag).regPtrs(i).FIELD = FIELD
  
  ReDim Preserve Paragraphs(I_parag).regPtrs(i + 1)
  
End Sub
''''''''''''''''''''''''''''''''''''''''''''''
' SQ 2-09-08
' Modifica "epocale":
' "interazione con l'utente"...
' Lavora sul paragrafo "corrente" (appena analizzato)
''
' Uso tabella dedicata su repository: MgASM_registers
''
' PARAGRAFO(i)
' - regX1 -> campoY1
' - regX2 -> campoY2
' - ...
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub initParagraph()
  Dim i As Integer
  Dim rs As Recordset
  
  On Error GoTo catch
  
  Set rs = m_Fun.Open_Recordset("SELECT register,field FROM MgASM_registers WHERE paragraph='" & Paragraphs(I_parag).Name & "'")
  While Not rs.EOF
    setRegisters rs!register & "", rs!FIELD & ""
    rs.MoveNext
  Wend
  rs.Close
  Exit Sub
catch:
  If err.Number = -2147217865 Then  'tabella non trovata
    writeLog "E", "Unexpected error: 'Repository Checking required'."
  Else
    writeLog "E", "Unexpected error: " & err.description
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Cerco l'ultimo puntamento (REG->CAMPO) nella struttura di paragrafi
' Non conosce la logica del programma, ma si avvicina molto!
' SQ 2.1: TROPPO RISCHIOSO CERCARE NEI PARAGRAFI PRECEDENTI... =>
'         posso decidere di non farlo (parametro nuovo)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getRegisters(register As String, Optional isSafe As Boolean = False) As String
  Dim i As Integer, j As Integer
  
  On Error GoTo catch
  'Cerco all'interno del paragrafo corrente, poi all'indietro:
  For i = I_parag To 0 Step -1
    For j = 0 To UBound(Paragraphs(i).regPtrs)
      If Paragraphs(i).regPtrs(j).register = register Then
        'trovato!
        getRegisters = Paragraphs(i).regPtrs(j).FIELD
        Exit Function
      End If
    Next
    'Non trovato nel paragrafo: si prosegue col precedente...
    'SQ 2.1
    If isSafe Then Exit Function
  Next
  'SQ
  'Non trovato! ritorna ""
  writeLog "W", "Values not found for register. Insert the association: " & Paragraphs(I_parag).Name & ":R-" & register
  Exit Function
catch:
  writeLog "E", "Unexpected error: " & err.description
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 25-08-08:
' ST:  Load and Test Register [50] R1,D2(X2,B2)
' STC: Load and Test Register [42] R1,D2(X2,B2)
' STH: Load and Test Register [40] R1,D2(X2,B2)
' STCM: da fare - Store Character under Mask [STCM  BE  R1,M3,D2(B2)]
' STM:  da fare - Store Multiple             [STM 90  R1,R3,D2(B2)]
'''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getSTORE(parameters As String)
  Dim param As String
  Dim loperand As operand, roperand As operand
  Dim statement As String
  Dim cblFrom As String, cblTo As String
  
  param = parameters
  
  Select Case operator
    Case "ST"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'The actual value of operand-2 (x2+b2+d2) is loaded into the register specified by R1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' ...
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Mauro 17/03/2010 : Nuove gestione per "Branch And Link(BAL)"
      If isParagraph(roperand.Displace) Then
        ' Gestione post-BAL
        cblFrom = "BR-LABEL(" & loperand.value & ")"
        cblTo = getOperand_CBL(roperand) & "-L"
        statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
        'colonna 72: parametri originali
        statement = statement & Space(72 - Len(statement)) & param
      Else
        ' Gestione Classica
        cblFrom = "R-" & loperand.value
        ' MAURO: MOLTO TEMPORANEO
        cblTo = getOperand_CBL(roperand)
  
        'R-x devo valorizzare solo 4 byte... (da fare nei vari getOperand...)
        cblTo = IIf(Right(cblTo, 2) = ":)", Replace(cblTo, ":)", ":4)"), cblTo & "(1:4)")
          
        statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
        'colonna 72: parametri originali
        statement = statement & Space(72 - Len(statement)) & param
''      End If
      
        'SQ 2.0
        If roperand.iType = "REG" Then
          statement = operator & Space(Len(indent) - Len(operator)) & "SET ADDRESS OF AREA-" & roperand.base & " TO PTR-" & roperand.base & vbCrLf & statement
        End If
      End If
      tagWrite statement
    Case "STH"
    'PROBLEMA!!!
    'ST!   *1 -> LONKBUS
    'STH        MOVE R-1(3:2) TO LONKBUS  (� un COMP)
    ' ---------> MOVE R-1(3:2) TO LONKBUS(1:2)

      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' The rightmost two bytes or halfword (bits 16-31) of R1
      ' are stored at the storage address specified by (X2+B2+D2).
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' c) R1,FIELD1        MOVE I-1 TO FIELD1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      cblFrom = "R-" & loperand.value & "(3:2)"
      If Len(roperand.base) = 0 Then
        If Len(roperand.value) Then
          'e) - IMMEDIATE: verificare...
          cblTo = roperand.value
        Else
          'a),c)
          If IsNumeric(roperand.Displace) Then
            'a)
            cblTo = roperand.Displace
          Else
            'c),...
            'SQ 2.0
            ''tagWrite "ST!   *" & loperand.value & " -> " & roperand.displace
            cblTo = getOperand_CBL(roperand)
            cblTo = cblTo & "(1:2)"
            '''setRegisters loperand.value, roperand.displace
                            'd)
            '''FARE cblTo = "0" & roperand.displacePlus
          End If
        End If
        statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
        'colonna 72: parametri originali
        statement = statement & Space(72 - Len(statement)) & param
      Else
        cblTo = getOperand_CBL(roperand, True)
        statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
        'colonna 72: parametri originali
        statement = statement & Space(72 - Len(statement)) & param
      End If
      
      tagWrite statement
    Case "STC"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'Bits 24-31 (one-byte) of R1 are stored at the storage address specified by (x2+b2+d2)
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' c) R1,FIELD1        MOVE R-1(4:1) TO FIELD1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      cblFrom = "R-" & loperand.value & "(4:1)"
      If Len(roperand.base) = 0 Then
        If Len(roperand.value) Then
          'e) - IMMEDIATE: verificare...
          cblTo = roperand.value
        Else
          'a),c)
          If IsNumeric(roperand.Displace) Then
            'a)
            cblTo = roperand.Displace
          Else
            'c),...
            'TMP:
            tagWrite "ST!   *" & loperand.value & " -> " & roperand.Displace
            cblTo = getOperand_CBL(roperand)
            '''setRegisters loperand.value, roperand.displace
                            'd)
            '''FARE cblTo = "0" & roperand.displacePlus
          End If
        End If
        statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
        'colonna 72: parametri originali
        statement = statement & Space(72 - Len(statement)) & param
      Else
        'b)
        cblTo = getOperand_CBL(roperand, True)
        statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & cblTo
        'colonna 72: parametri originalip
        statement = statement & Space(72 - Len(statement)) & param
      End If
      
      tagWrite statement
    Case "STCM", "STM"
      'Store Character under Mask [STCM  BE  R1,M3,D2(B2)]
      'Store Multiple             [STM 90  R1,R3,D2(B2)]
      tagWrite "#" & operator & ":" & Space(6 - Len(operator) - 2) & "*" & "PARAM: " & param
  End Select
  
  'statement=...
  'tagwrite statement
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 25-08-08:
' S:  Subtract                [5B] R1,D2(X2,B2)
' SH: Subtract Halfword       [4B] R1,D2(X2,B2)
' SR: Subtract Registers      [1B] R1,R2
' SL,SLR,SP: fare...
' A:  Add                     [5A] R1,D2(X2,B2)
' AH: Add Halfword            [4A] R1,D2(X2,B2)
' AR: Add Registers           [1A] R1,R2
' AL,ALR,AP: fare...
' M:  Multiply                [5C] R1,D2(X2,B2)
' MH: Multiply Halfword       [4C] R1,D2(X2,B2)
' MR: Multiply Registers      [1C] R1,R2
' MP: fare...
' D:  Divide                  [5D] R1,D2(X2,B2)
' DR: Divide Registers        [1D] R1,R2
' DP: fare...
''
' SETTARE IL RETURN-CODE!!!!!!!
' -VERIFICARE TUTTO
' -SEPARARE "H": considera solo i primi 4 byte!
''
' SQ: attenzione: status non settati!...
' Si potrebbe aggiungere solo in caso l'istruzione successiva sia un B*...
' Al momento, aggiunto warning e basta!
'''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getARITHMETIC(parameters As String)
  Dim param As String
  Dim loperand As operand, roperand As operand
  Dim statement As String
  Dim cblFrom As String, cblTo As String
  
  param = parameters
  
  'SQ warning in getBranch...
  missingStatus = True
  
  Select Case operator
    Case "S", "M", "A", "D"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'The actual value of operand-2 (x2+b2+d2) is loaded into the register specified by R1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' a) * R1,FIELD1   ... FIELD1 FROM I-1
      ' b) * R1,=F'1'    ... 1 FROM I-1
      ' c) * R1,6(R2)    MOVE PRT-2(I-2 + 7:) TO REV-X4
      '                  ... REV-B4 FROM I-1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      cblFrom = getOperand_CBL(loperand, True)
      cblTo = getOperand_CBL(roperand)
      If InStr(cblTo, "(") Then
        'MOVE POSIZIONALE: SERVE CAMPO D'APPOGGIO
        'Attenzione: servirebbe anche per i campi non numerici! (AGGIUNGERE CONTROLLO SUL TIPO DI DATO)
        ' c), d)
        statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblTo & " TO REV-X4"
        statement = statement & Space(72 - Len(statement)) & param
        Select Case Mid(operator, 1, 1)
          Case "A"
            statement = statement & vbCrLf & Space(Len(indent)) & "ADD REV-B4 TO " & cblFrom
          Case "S"
            statement = statement & vbCrLf & Space(Len(indent)) & "SUBTRACT REV-B4 FROM " & cblFrom
          Case "M"
            'SQ 20-01-10 lavora su 2 registri...!
            'tmpfare nextReg...
            Dim nextReg As String
            nextReg = Mid(cblFrom, InStr(cblFrom & "-", "-") + 1)
            nextReg = "I-" & CInt(nextReg) + 1
            statement = statement & vbCrLf & Space(Len(indent)) & "MULTIPLY " & nextReg & " BY REV-B4"
            statement = statement & vbCrLf & Space(Len(indent)) & "MOVE ZERO TO " & cblFrom
            statement = statement & vbCrLf & Space(Len(indent)) & "MOVE REV-B4 TO " & nextReg
          Case "D"
            statement = statement & vbCrLf & Space(Len(indent)) & "DIVIDE REV-B4 INTO " & cblFrom
        End Select
      Else
        ' a), b)
        Select Case Mid(operator, 1, 1)
          Case "A"
            statement = operator & Space(Len(indent) - Len(operator)) & "ADD " & cblTo & " TO " & cblFrom
          Case "S"
            statement = operator & Space(Len(indent) - Len(operator)) & "SUBTRACT " & cblTo & " FROM " & cblFrom
          Case "M"
            statement = operator & Space(Len(indent) - Len(operator)) & "MULTIPLY " & cblTo & " BY " & cblFrom
          Case "D"
            statement = operator & Space(Len(indent) - Len(operator)) & "DIVIDE " & cblTo & " INTO " & cblFrom
        End Select
        'colonna 72: parametri originali
        statement = statement & Space(72 - Len(statement)) & param
      End If
    Case "SH", "MH", "AH"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'The actual value of operand-2 (x2+b2+d2) is loaded into the register specified by R1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' a) * R1,FIELD1   ... FIELD1 FROM I-1
      ' b) * R1,=F'1'    ... 1 FROM I-1
      ' c) * R1,6(R2)       MOVE PRT-2(I-2 + 7:) TO REV-X2
      '                     ... REV-BB FROM I-1
      ' d) * R1,FIELD1+6    MOVE FIELD1(7:) TO REV-X2
      '                     ... REV-BB FROM I-1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      cblFrom = getOperand_CBL(loperand, True)
      cblTo = getOperand_CBL(roperand)
      If InStr(cblTo, "(") Then
        'MOVE POSIZIONALE: SERVE CAMPO D'APPOGGIO
        'Attenzione: servirebbe anche per i campi non numerici! (AGGIUNGERE CONTROLLO SUL TIPO DI DATO)
        ' c), d)
        statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblTo & " TO REV-X2"
        statement = statement & Space(72 - Len(statement)) & param
        Select Case Mid(operator, 1, 1)
          Case "A"
            statement = statement & vbCrLf & Space(Len(indent)) & "ADD REV-BB TO " & cblFrom
          Case "S"
            statement = statement & vbCrLf & Space(Len(indent)) & "SUBTRACT REV-BB FROM " & cblFrom
          Case "M"
            statement = statement & vbCrLf & Space(Len(indent)) & "MULTIPLY REV-BB BY " & cblFrom
        End Select
      Else
        Select Case Mid(operator, 1, 1)
          Case "A"
            statement = operator & Space(Len(indent) - Len(operator)) & "ADD " & cblTo & " TO " & cblFrom
          Case "S"
            statement = operator & Space(Len(indent) - Len(operator)) & "SUBTRACT " & cblTo & " FROM " & cblFrom
          Case "M"
            'OK: � un po' al contrario (MULTIPLY 6 BY I-4)
            statement = operator & Space(Len(indent) - Len(operator)) & "MULTIPLY " & cblTo & " BY " & cblFrom
        End Select
        'colonna 72: parametri originali
        statement = statement & Space(72 - Len(statement)) & param
      End If
    'TMP: SQ 20-01-10 - Aggiunte istruzioni "L"... si differiscono per il RETURN CODE!...
    Case "SR", "MR", "AR", "DR", "SLR", "MLR", "ALR", "DLR"
      'tmp, scamuffo:
      If Len(operator) = 3 Then operator = Replace(operator, "L", "")
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'The actual value of operand-2 (x2+b2+d2) is loaded into the register specified by R1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "R2"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' a) *R R1,R2    ... FROM I-2 FROM I-1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      cblFrom = "I-" & loperand.value
      cblTo = "I-" & roperand.value
      
      Select Case Mid(operator, 1, 1)
        Case "A"
          statement = operator & Space(Len(indent) - Len(operator)) & "ADD " & cblTo & " TO " & cblFrom
        Case "S"
          statement = operator & Space(Len(indent) - Len(operator)) & "SUBTRACT " & cblTo & " FROM " & cblFrom
        Case "M"
          'OK: � un po' al contrario (MULTIPLY 6 BY I-4)
          statement = operator & Space(Len(indent) - Len(operator)) & "MULTIPLY " & cblTo & " BY " & cblFrom
        Case "D"
          statement = operator & Space(Len(indent) - Len(operator)) & "DIVIDE " & cblTo & " INTO " & cblFrom
      End Select
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param
    Case "SP", "MP", "AP", "DP"
      operator = operator & TILDE
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      '...
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "D1(L1,B1)"
      roperand.format = "D2(L2,B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand

      cblFrom = getOperand_CBL(loperand)
      cblTo = getOperand_CBL(roperand)
      
      Dim FIELD As String
      If loperand.iType = "P" Then
        FIELD = cblTo
        Select Case Mid(operator, 1, 1)
          Case "A"
            statement = "ADD " & FIELD & " TO " & cblFrom
          Case "S"
            statement = "SUBTRACT " & FIELD & " FROM " & cblFrom
          Case "M"
            statement = "MULTIPLY " & FIELD & " BY " & cblFrom
          Case "D"
            statement = "DIVIDE " & FIELD & " INTO " & cblFrom
        End Select
        statement = operator & Space(Len(indent) - Len(operator)) & statement
        'colonna 72: parametri originali
        statement = statement & Space(72 - Len(statement)) & param
      Else
        FIELD = "REV-PACKED"
        
        statement = "MOVE " & cblFrom & " TO REV-PACKED(" & 10 - loperand.len + 1 & ":" & loperand.len & ")"
        statement = operator & Space(Len(indent) - Len(operator)) & statement
        statement = statement & Space(72 - Len(statement)) & param & vbCrLf
        Select Case Mid(operator, 1, 1)
          Case "A"
            statement = statement & indent & "ADD " & cblTo & " TO " & FIELD & vbCrLf
          Case "S"
            statement = statement & indent & "SUBTRACT " & cblTo & " FROM " & FIELD & vbCrLf
          Case "M"
            statement = statement & indent & "MULTIPLY " & cblTo & " BY " & FIELD & vbCrLf
          Case "D"
            statement = statement & indent & "DIVIDE " & cblTo & " INTO " & FIELD & vbCrLf
        End Select
        statement = statement & indent & "MOVE REV-PACKED(" & 10 - loperand.len + 1 & ":" & loperand.len & ") TO " & cblFrom
      End If
    
    Case Else
      statement = "#" & operator & Space(6 - (Len(operator) + 1)) & "*" & "PARAM: " & param
  End Select
  
  'SQ 2.0
  If roperand.iType = "REG" And Len(roperand.base) Then
    statement = operator & Space(Len(indent) - Len(operator)) & "SET ADDRESS OF AREA-" & roperand.base & " TO PTR-" & roperand.base & vbCrLf & statement
  End If
    
  tagWrite statement

End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 25-08-08:
' SLL: Shift Left Single Logical  [89] R1,D2(B2)
' SLA,SLDA,SLDL,SRA,SRDA,SRDL,SRL,SRP: fare...
' ...
' -> SETTARE IL RETURN-CODE!!!!!!!
' -> DISTINGUERE LOGICAL, DOUBLE, ETC...!!!!!!!!
'''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getSHIFT(parameters As String)
  Dim param As String
  Dim loperand As operand, roperand As operand
  Dim statement As String, cblOperator As String
  Dim cblFrom As String, cblTo As String
  
  param = parameters
    
  cblOperator = IIf(Left(operator, 2) = "SL", " * ", " / ")
  
  
  Select Case operator
      Case "SLA", "SRA", "SLDA", "SRDA"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'The actual value of operand-2 (x2+b2+d2) is loaded into the register specified by R1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' a) R1,2       COMPUTE I-1 = I-1 */ <2exp2>
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'TMP:
      operator = operator & TILDE
      cblTo = "I-" & loperand.value
      If IsNumeric(roperand.Displace) Then
        'a
        cblFrom = 2 ^ roperand.Displace
        
        statement = operator & Space(Len(indent) - Len(operator)) & "COMPUTE " & cblTo & " = " & cblTo & cblOperator & cblFrom
        'colonna 72: parametri originali
        statement = statement & Space(72 - Len(statement)) & param
      Else
        writeLog "W", "--tmp: getSHIFT: " & param
        statement = "#" & operator & Space(6 - (Len(operator) + 1)) & "*" & "PARAM: " & parameters
      End If
    Case "SLDL", "SRDL"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'The actual value of operand-2 (x2+b2+d2) is loaded into the register specified by R1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'SQ non va bene
      'cblFrom = getOperand_CBL(roperand)
      'SQ tmp!
      If roperand.iType = "REG" Then
        cblFrom = "I-" & roperand.base
      Else
        cblFrom = roperand.Displace
      End If
      cblTo = getOperand_CBL(loperand)
  
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' a) R1,3       COMPUTE I-1 = I-1 * (2 ** 3)
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      statement = operator & Space(Len(indent) - Len(operator)) & "COMPUTE " & cblTo & " = " & cblTo & cblOperator & " (2 ** " & cblFrom & ")"
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param & vbCrLf
      'SQ tmp... gestire ALIAS registro...
      cblTo = "I-" & loperand.value + 1
      statement = statement & operator & Space(Len(indent) - Len(operator)) & "COMPUTE " & cblTo & " = " & cblTo & cblOperator & " (2 ** " & cblFrom & ")"
      
    Case "SLL", "SRL"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'The actual value of operand-2 (x2+b2+d2) is loaded into the register specified by R1
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      loperand.format = "R1"
      roperand.format = "D2(B2)"
    
      getOperand parameters, loperand
      getOperand parameters, roperand
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' a) R1,2       COMPUTE I-1 = I-1 */ <2exp2>
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'SQ non va bene
      'cblFrom = getOperand_CBL(roperand)
      'SQ tmp!
      If roperand.iType = "REG" Then
        cblFrom = "I-" & roperand.base
      Else
        cblFrom = roperand.Displace
      End If
      cblTo = getOperand_CBL(loperand)
  
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' a) R1,3       COMPUTE I-1 = I-1 * (2 ** 3)
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      statement = operator & Space(Len(indent) - Len(operator)) & "COMPUTE " & cblTo & " = " & cblTo & cblOperator & " (2 ** " & cblFrom & ")"
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param
    Case "SRP"
      statement = "#" & operator & Space(6 - (Len(operator) + 1)) & "*" & "PARAM: " & parameters
  End Select
  
  tagWrite statement

End Sub
''''''''''''''''''''''''''''''''''''''
' IC, Insert Characters, R1,D2(X2,B2)
' ICM, Insert Characters under MASK, R1,M3,D2(B2) - fare
''''''''''''''''''''''''''''''''''''''
Private Sub getINSERT(parameters As String)
  Dim statement As String
  Dim loperand As operand, roperand As operand
  Dim cblFrom As String, cblTo As String
  Dim param As String
  
  param = parameters
  
  Select Case operator
    Case "IC"
      loperand.format = "R1"
      roperand.format = "D2(X2,B2)"
      getOperand parameters, loperand
      getOperand parameters, roperand
      
      'SQ 26-08-08 (1 carattere solo!)
      loperand.len = 1
      roperand.len = 1
      
      cblFrom = getOperand_CBL(roperand)
      cblTo = getOperand_CBL(loperand, True)
      ''''''''''''''''''''''''''''''''''''''''''
      ' a) R1,FIELD1 -> MOVE FIELD1 TO R-1(4:1)
      ' b) R1,FIELD1(R2) -> MOVE ...boh... TO R-1(4:1)
      ''''''''''''''''''''''''''''''''''''''''''
      statement = operator & Space(Len(indent) - Len(operator)) & "MOVE " & cblFrom & " TO " & Replace(cblTo, "I-", "R-") & "(4:1)"
    Case "ICM"
      statement = "#" & operator & Space(8 - (Len(operator) + 1)) & "*" & "PARAM: " & parameters
  End Select
   
  'colonna 72: parametri originali
  statement = statement & Space(72 - Len(statement)) & param
  
  tagWrite statement
End Sub
'''''''''''''''''''''''''''''''''''''''''''
' TM, Test under Mask, D1(B1),I2
''
' -> COND-CODE a: 00 se bit esplorati a 0
'                 11                    1
'                 01 misti
'''''''''''''''''''''''''''''''''''''''''''
Private Sub getTM(parameters As String)
  Dim statement As String
  Dim loperand As operand, roperand As operand
  Dim cblAddress As String, cblMask As String
  Dim param As String
  
  param = parameters
  
  loperand.format = "D1(B1)"
  roperand.format = "I2"
  
  loperand.len = 1  'fixed
  
  getOperand parameters, loperand
  getOperand parameters, roperand
  
  cblMask = getOperand_CBL(roperand)
  cblAddress = getOperand_CBL(loperand)
  ''''''''''''''''''''''''''''''''''''''''''
  ' ES: FIELD1,X'08'
  ' ->
  ' MOVE FIELD1 TO REV-MASK
  ' CALL 'CBL_AND' USING X'08',REV-MASK BY VALUE 1
  ' IF REV-MASK = X'08 SET CC-O TO TRUE
  ' ELSE IF REV-MASK = ZERO SET CC-Z TO TRUE
  ' ELSE SET CC-M TO TRUE
  ''''''''''''''''''''''''''''''''''''''''''
  If loperand.iType = "C" Then
    If loperand.len < 2 Then
      statement = "MOVE ZERO TO REV-MASK"
      statement = operator & Space(Len(indent) - Len(operator)) & statement
      'colonna 72: parametri originali
      statement = statement & Space(72 - Len(statement)) & param
      param = ""
      tagWrite statement
      statement = "MOVE " & cblAddress & "(2:1) TO REV-MASK-X"
    Else
      statement = "MOVE " & cblAddress & " TO REV-MASK-X"
    End If
  Else
    statement = "MOVE " & cblAddress & " TO REV-MASK"
  End If
  statement = operator & Space(Len(indent) - Len(operator)) & statement
  'colonna 72: parametri originali
  statement = statement & Space(72 - Len(statement)) & param & vbCrLf
  
  statement = statement & indent & "CALL 'CBL_AND' USING " & cblMask & ",REV-MASK BY VALUE 1" & vbCrLf
  statement = statement & indent & "IF REV-MASK = " & cblMask & " SET CC-O TO TRUE" & vbCrLf
  statement = statement & indent & "ELSE IF REV-MASK = ZERO SET CC-Z TO TRUE" & vbCrLf
  statement = statement & indent & "ELSE SET CC-M TO TRUE."
  
  tagWrite statement
End Sub
''''''''''''''''''''''''''''''''''''''
' TR, ...
''''''''''''''''''''''''''''''''''''''
Private Sub getTR(parameters As String)
  Dim statement As String
  Dim loperand As operand, roperand As operand
  Dim cblFrom As String, cblTo As String
  Dim param As String
  Dim i As Integer
  param = parameters
  
  'SILVIA 10/9/2008
  For i = 1 To Label_Temp.count
    If Label_Temp.item(i) = "REV-TR" Then
      Exit For
    End If
  Next
  If i > Label_Temp.count Then
    Label_Temp.Add "REV-TR", "REV-TR"
  End If
  
  loperand.format = "D1(L1,B1)"
  roperand.format = "D2(B2)"
  
  getOperand parameters, loperand
  getOperand parameters, roperand
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' a) 2(81,R6),TAB1 -> MOVE TAB1 TO REVTRT-TBL
  '                     MOVE PTR6(I-6 + 3:81) TO REV-TBL
  '                     MOVE 81 TO REVTRT-LEN
  '                     PERFORM REV-TRT
  '                     MOVE REV-TBL TO PTR6(I-6 + 3:81)
  ''
  ' -> aggiungere paragrafo!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  cblFrom = getOperand_CBL(roperand)
  cblTo = getOperand_CBL(loperand)
  statement = "MOVE " & cblFrom & " TO REV-TRT-TBL"
  statement = operator & Space(Len(indent) - Len(operator)) & statement
  'colonna 72: parametri originali
  statement = statement & Space(72 - Len(statement)) & param & vbCrLf
  
  statement = statement & indent & "MOVE " & cblTo & " TO REV-TBL" & vbCrLf
  statement = statement & indent & "MOVE " & loperand.len & " TO REV-LEN" & vbCrLf
  statement = statement & indent & "PERFORM REV-TR" & vbCrLf
  statement = statement & indent & "MOVE REV-TBL TO " & cblTo & vbCrLf
  
  tagWrite statement
End Sub
'''''''''''''''''''''''''''''''''''''''''
' TRT, Test under Mask, D1(L1,B1),D2(B2)
''
' Utilizzo di REV-TRT -> da inserire in automatico!
'''''''''''''''''''''''''''''''''''''''''
Private Sub getTRT(parameters As String)
  Dim statement As String
  Dim loperand As operand, roperand As operand
  Dim cblFrom As String, cblTo As String
  Dim param As String
  Dim i As Integer
  
  param = parameters
  'SILVIA 10/9/2008
  For i = 1 To Label_Temp.count
    If Label_Temp.item(i) = "REV-TRT" Then
      Exit For
    End If
  Next
  If i > Label_Temp.count Then
    Label_Temp.Add "REV-TRT", "REV-TRT"
  End If
  
  'TMP
  operator = operator & TILDE
  
  loperand.format = "D1(L1,B1)"
  roperand.format = "D2(B2)"
  
  getOperand parameters, loperand
  getOperand parameters, roperand
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' a) 2(81,R6),TAB1 -> MOVE TAB1 TO REVTRT-TBL
  '                     MOVE PTR6(I-6 + 3:81) TO REVTRT-IN
  '                     MOVE 81 TO REV-LEN
  '                     PERFORM REV-TRT
  ''
  ' -> aggiungere paragrafo!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  cblFrom = getOperand_CBL(roperand)
  cblTo = getOperand_CBL(loperand)
  statement = "MOVE " & cblFrom & " TO REV-TRT-TBL"
  statement = operator & Space(Len(indent) - Len(operator)) & statement
  'colonna 72: parametri originali
  statement = statement & Space(72 - Len(statement)) & param & vbCrLf
  
  statement = statement & indent & "MOVE " & cblTo & " TO REV-TBL" & vbCrLf
  statement = statement & indent & "MOVE " & loperand.len & " TO REV-LEN" & vbCrLf
  statement = statement & indent & "PERFORM REV-TRT"
  
  'SQ 2.0
  If loperand.iType = "REG" Then
    statement = operator & Space(Len(indent) - Len(operator)) & "SET ADDRESS OF AREA-" & loperand.base & " TO PTR-" & loperand.base & vbCrLf & statement
  End If
  If roperand.iType = "REG" Then
    statement = operator & Space(Len(indent) - Len(operator)) & "SET ADDRESS OF AREA-" & roperand.base & " TO PTR-" & roperand.base & vbCrLf & statement
  End If
  
  tagWrite statement
End Sub
''''''''''''''''''''''''''''''''''''''
' EX: EXecute   [44]  R1,D2(X2,B2)
''''''''''''''''''''''''''''''''''''''
Private Sub getEX(parameters As String)
  Dim statement As String
  Dim loperand As operand, roperand As operand
  Dim cblFrom As String, cblTo As String
  Dim param As String
  
  param = parameters
  
  loperand.format = "R1"
  roperand.format = "D2(X2,B2)"
  
  getOperand parameters, loperand
  getOperand parameters, roperand
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' ES: R1,LABEL1 -> MOVE I-1 TO I-EX
  '                  PERFORM LABEL1 THRU LABEL1-EX
  ''
  ' -> il paragrafo LABEL1 va modificato a mano!!!!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  cblFrom = getOperand_CBL(roperand)
  'pezza:
  cblFrom = Replace(cblFrom, "L-", "")
  
  cblTo = getOperand_CBL(loperand)
  
  statement = "MOVE " & cblTo & " TO I-EX"
  statement = operator & Space(Len(indent) - Len(operator)) & statement
  'colonna 72: parametri originali
  statement = statement & Space(72 - Len(statement)) & param & vbCrLf
  
  statement = statement & indent & "PERFORM " & cblFrom & " THRU " & cblFrom & "-EX" & vbCrLf
  
  tagWrite statement
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Gestione aggiunta Template per le istruzioni MVN, MVO, ED,TR, TRT
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Silvia 10/9/2008
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub AddTemplate(tempname As String)
  Dim previousTag As String
  Dim RtbTemp As RichTextBox
  Dim strTemplate As String
  On Error GoTo fileErr
  previousTag = CurrentTag
  Set RtbTemp = MapsdF_Parser.RTErr
  
  Select Case tempname
    Case "REV-MVN", "REV-MVO", "REV-TR", "REV-TRT"
      'sq prova...
      CurrentTag = "PROCEDURE-DIVISION"
      RtbTemp.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\" & tempname
      strTemplate = RtbTemp.Text
      tagWrite strTemplate
      CurrentTag = previousTag
    Case "REV-ED"
      CurrentTag = "WORKING-STORAGE"
      RtbTemp.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\REV-ED-WK"
      strTemplate = RtbTemp.Text
      tagWrite strTemplate
      CurrentTag = previousTag
  End Select
  RtbTemp.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\" & tempname
  strTemplate = RtbTemp.Text
  tagWrite strTemplate
Exit Sub
fileErr:
  If err.Number = 52 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\" & migrationPath
    Resume
  ElseIf err.Number = 75 Then
    MsgBox "Template file '" & m_Fun.FnPathPrj & "\" & templatePath & "\" & tempname & " not found.", vbExclamation, Parser.PsNomeProdotto
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
  End If
End Sub

' Gestione EX: allestimento lista "LABEL executate"
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub initEX(parameters As String)
  Dim loperand As operand, roperand As operand
  Dim label As String
  Dim param As String
  
  param = parameters
  
  loperand.format = "R1"
  roperand.format = "D2(X2,B2)"
  
  getOperand parameters, loperand
  getOperand parameters, roperand
  
  label = roperand.Displace 'getOperand_CBL(roperand)... evitiamo il getOperand_CBL... fa giri che non servono
  
  On Error Resume Next ' In caso inserisse un elemento gi� esistente
  Label_EXECUTE.Add label, label
End Sub
'''''''''''''''''''''''''''''''''''''
' Gestione LABEL "AUX":
' -> quelle necessarie alle istruzioni
'    ma non presenti sull'ASM...
' TMP
'''''''''''''''''''''''''''''''''''''
Private Sub checkLabelAux()
  If Len(LabelAux) Then
    tagWrite indent & "."
    tagWrite "       " & LabelAux & "."
    'reset
   LabelAux = ""
  End If
End Sub

Private Sub writeLog(typeMsg As String, message As String)
  'silvia 11/9/2008
  Dim rs As Recordset
  Set rs = m_Fun.Open_Recordset("SELECT * FROM MgASM_segnalazioni WHERE IdOggetto= " & GbIdOggetto)
  rs.AddNew
  rs!idOggetto = GbIdOggetto
  rs!Riga = GbNumRec
  rs!tipoMsg = typeMsg
  rs!operatore = operator
  rs!descrizione = message
  rs.Update
  rs.Close
  'lswLog.ListItems.Add , , "[" & typeMsg & "] [" & format(operator, "_____") & "] [" & format(GbNumRec, "00000") & "] - " & message
End Sub

'silvia 11/9/2008
Sub deleteSegnalazioni(idOggetto As Long)
  'Dim rs As Recordset
  Parser.PsConnection.Execute "Delete * from MgASM_segnalazioni where IdOggetto = " & idOggetto
End Sub

' Mauro 17/03/2010
Function isParagraph(NomeCampo As String) As Boolean
  Dim i As Integer
  
  isParagraph = False
  If Not IsNumeric(NomeCampo) Then
    For i = 0 To UBound(arrCampi)
      If NomeCampo = arrCampi(i) Then
        isParagraph = True
        Exit For
      End If
    Next i
  End If
End Function

' Mauro 17/03/2010: PRJ Japan - carica Lista Paragrafi per Gestione BAL
Sub caricaListaCampi()
  ReDim arrCampi(0)
  arrCampi(UBound(arrCampi)) = "BAW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "BBW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "BCW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "BDW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "BEW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "BFW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "CBW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "CCW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "CDW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "CEW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "DAW010"
  ReDim Preserve arrCampi(UBound(arrCampi) + 1)
  arrCampi(UBound(arrCampi)) = "DBW010"
End Sub

Sub AddFields_Minus(asmOperand As operand)
  Dim isTrovato As Boolean, a As Integer
  
  isTrovato = False
  For a = 0 To listIndex
    If asmOperand.Displace = dataList(a).Name Then
      isTrovato = True
      Exit For
    End If
  Next a
  If Not isTrovato Then
    On Error Resume Next
    Fields_Minus_Def.Add Space(7) & "01 " & asmOperand.Displace & Space(15 - Len(asmOperand.Displace)) & "PIC X(" & IIf(asmOperand.len > 0, asmOperand.len, "10") & ").", asmOperand.Displace
    On Error GoTo 0
  End If
End Sub
