Attribute VB_Name = "MapsdM_QUICKIMS"
Option Explicit
Option Compare Text

Dim reserverdWords() As Variant
Dim equOrdinale As Integer

Sub parsePgm_I()
  Dim FD As Long
  Dim Line As String, token As String, execStatement As String
  Dim rsOggetti As Recordset
  
  On Error GoTo parseErr
  
  ReDim CmpIstruzione(0)
  'ReDim assign_files(0) 'SELECT/ASSIGN
  ReDim CampiMove(0)
  
  '''''''''''''''''''''
  ' Reset tabelle
  '''''''''''''''''''''
  Parser.PsConnection.Execute "DELETE * FROM PsQUIK_Dati where idOggetto = " & GbIdOggetto
  
  
  'TMP: se mai ci sar� un altro progetto... portare fuori sta lista e caricarla al load!
  reserverdWords = Array("++INCLUDE", "READ", "WRITE", "GET", "SET", "DISPLAY", "TRACE", "PRINT", "REPORT", "PRINTCHAR", "HDR", "DOHEADERS", "TITLE1", "TITLE2", "OPEN", "CLOSE", "OPTION", "EQU", "PRINTHEX", "TABLSPEC", "ADD", "SUB", "DIVD", "GO", "GOTO", "ATEND", "MOVE", "MOVZON", "COPY", "CALL", "PERFORM", "EXEC", "IF", "ELSE")
  
  equOrdinale = 0 'init
  GbNumRec = 0    'init
  
  FD = FreeFile
  Open GbFileInput For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(Line) = 0 Then
      Line Input #FD, Line
      GbNumRec = GbNumRec + 1
    Else
      'Ho gia' una linea da analizzare (letta dalla getCall, per es.)
    End If
    
    If Left(Line, 1) <> "*" Then
      'LINEA VALIDA:
      'line = RTrim(Mid(line, 8, 65)) 'elimino etichette!
      'line = RTrim(line)
      
      'Eliminazione Commenti a met� linea:
      Line = Trim(Left(Line, InStr(Line & "/*", "/*") - 1))
      
      token = nextToken_new(Line) 'MANTIENE GLI APICI!!
      Select Case token
        Case "EQU"
          getEQU Line
          Line = ""
        Case "++INCLUDE"
          getInclude Line
          Line = ""
         Case "READ", "WRITE", "GET", "SET", "DISPLAY", "TRACE", "PRINT", "PRINT.", "REPORT", "PRINTCHAR", "HDR", "DOHEADERS", "DOHEADERS."
          Line = ""
        Case "TITLE1", "TITLE2" '?...
          Line = ""
        Case "OPEN", "CLOSE"
          Line = ""
        Case "OPTION", "PRINTHEX", "TABLSPEC"
          Line = ""
        Case "ADD", "SUB", "DIVD"
          Line = ""
        Case "GO", "GOTO", "ATEND"
          Line = ""
        Case "MOVE", "MOVZON"
          'tmp: ignoriamo MOVZON
          If token = "MOVZON" Then
            Line = ""
          Else
            getMove Line, FD, CampiMove
          End If
        Case "COPY"
          'getCOPY line 'COPY
          MsgBox token & "-" & Line
          Line = ""
        Case "CALL"
          getCALL Line, FD 'attenzione: modifica line
        Case "PERFORM"
          Line = ""
        Case "EXEC"
          execStatement = getEXEC(Line, FD)
          token = nexttoken(execStatement)
            Select Case token
              Case "DLI"
                  getEXEC_DLI execStatement
              Case "SQL"
                  getEXEC_SQL execStatement
              Case "CICS"
                  getEXEC_CICS execStatement
            End Select
          Line = ""
        Case "IF", "ELSE"
          Line = ""
        Case "ABEND", "END", "EXIT"
          Line = ""
        Case "/*"
          'getComment FD, line  '!Non � su pi� righe!?
          Line = ""
        Case ""
          'nop
        Case Else
          If IsNumeric(token) Then
            'Label!
          Else
            'Debug.Print token & " ## " & GbFileInput
            Line = ""
          End If
      End Select
    Else
      Line = ""
    End If
  Wend
  Close FD
  
  'SQ - gi� resettato...
  'Parser.PsConnection.Execute "Delete from PsData_CmpMoved where idpgm = " & GbIdOggetto
  Dim i As Integer
  For i = 1 To UBound(CampiMove)
    Parser.PsConnection.Execute "Update PsData_Cmp Set Moved = true where Nome = '" & CampiMove(i).NomeCampo & "' and (IdOggetto = " & GbIdOggetto & " or IdOggetto in (Select IdOggettoR from PsRel_Obj where IdOggettoC = " & GbIdOggetto & " and Relazione = 'CPY') )"
    'stefano: si, provare, da rifare con cura
    Dim rsMoved As Recordset
    'stefano: verifica se il campo � in copy
    Set rsMoved = m_Fun.Open_Recordset("Select * from PsData_Cmp where Nome = '" & CampiMove(i).NomeCampo & "' and IdOggetto in (Select IdOggettoR from PsRel_Obj where IdOggettoC = " & GbIdOggetto & " and Relazione = 'CPY')")
    ' rivedere cambiata tabella
    'If Not rsMoved.EOF Then
       'Parser.PsConnection.Execute "Insert into PsData_CmpMoved (IdOggetto,IdArea,Ordinale,IdPgm) values(" & rsMoved!idOggetto & ", " & rsMoved!idArea & ", " & rsMoved!Ordinale & ", " & GbIdOggetto & ")"
       'rsMoved.Close
    'End If
  Next
   
  'AC  scrittura tabelle  campimoved/valori
  Dim response As String
  Dim retvalue As Integer
  Dim IdOggettoCmp As Long
  Dim idArea As Integer
  Dim Ordinale As Integer
  For i = 1 To UBound(CmpIstruzione) ' alla fine la struttura mi serve solo
                                     ' per fissare le istruzioni non risolte
    'SQ - gi� resettato...
    'Parser.PsConnection.Execute "Delete from PsData_CmpMoved where IdPgm = " & GbIdOggetto
    ' verifichiamo se istruzione risolta
    If CmpIstruzione(i).Risolta = 0 Then
      response = risolviIstruzioneByMove(CmpIstruzione(i).CampoIstruzione, retvalue, CmpIstruzione(i).LastSerchIndex)
      If Len(response) Then
      ' update Psdli_Istruzioni, delete segnalazione
        Parser.PsConnection.Execute "Update PsDLI_Istruzioni Set Istruzione = '" & response _
                                & "' where IdOggetto =" & CmpIstruzione(i).Oggetto & "' and Riga =" _
                                & CmpIstruzione(i).Riga
        Parser.PsConnection.Execute "Delete from Bs_Segnalazioni where IdOggetto = " _
                                 & CmpIstruzione(i).Oggetto & " and Riga = " & CmpIstruzione(i).Riga _
                                 & " and ( Codice = 'H09' Or Codice = 'F09')"
      End If
    End If
  Next
  Dim j As Integer, k As Integer
  For j = 1 To UBound(CampiMove)
   ' aggiornamento tabella campi  PsData_CmpMoved
      ' AreaFromCampo GbIdOggetto, CampiMove(j).NomeCampo, IdOggettoCmp, IdArea, Ordinale
      'If Not idArea = -1 Then
        For k = 1 To CampiMove(j).Valori.count
          Parser.PsConnection.Execute "Insert Into PsData_CmpMoved (IdPgm,Nome,valore) Values (" _
                                    & GbIdOggetto & ",'" & CampiMove(j).NomeCampo & "','" & Replace(Left(CampiMove(j).Valori.item(k), 255), "'", "''") & "')"
        Next
      'End If
  Next
  
  Set rsOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  rsOggetti!DtParsing = Now
  rsOggetti!ErrLevel = GbErrLevel
  rsOggetti!parsinglevel = 1
  rsOggetti!Cics = SwTpCX
  rsOggetti!Batch = SwBatch
  rsOggetti!DLI = SwDli 'sarebbe IMS!
  rsOggetti!WithSQL = SwSql
  rsOggetti!VSam = SwVsam
  rsOggetti!mapping = SwMapping
  rsOggetti.Update
  rsOggetti.Close
  Exit Sub
parseErr:  'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(Line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, Line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(Line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, Line
        GbNumRec = GbNumRec + 1
        If Mid(Line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          GbOldNumRec = GbNumRec
          addSegnalazione Line, "P00", Line
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        GbOldNumRec = GbNumRec
        addSegnalazione Line, "P00", Line
        'm_Fun.WriteLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & Err.Description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    addSegnalazione Line, "P00", Line
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
    m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
  Else
    GbOldNumRec = GbNumRec
    addSegnalazione Line, "P00", Line
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Analizza le CALL.
' - Allestisce la PsCall (NUOVA!)
' - Invoca checkRelation
' Effetto collaterale: puo' valorizzare "line" per il giro successivo
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub getCALL(Line As String, FD As Long)
  Dim statement As String, program As String, token As String
  Dim endStatement As Boolean
  Dim linee As Integer
  
  On Error GoTo callErr
  
  GbOldNumRec = GbNumRec  'linea iniziale
  ''''''''''''''''''''''''''''''''''
  ' Becco tutte le linee di call...
  ''''''''''''''''''''''''''''''''''
  statement = Line
  If Mid(Line, Len(Line), 1) <> "." Then
    While Not EOF(FD) And Not endStatement
      Line Input #FD, Line
      GbNumRec = GbNumRec + 1
      If Left(Line, 1) <> "*" Then
        Line = Trim(Line)
        ' posso avere una linea vuota? (prob. no...)
        If Len(Line) Then
          token = nexttoken(Line)
          'Parola Riservata?
          'SQ 27-04-06: se mi arriva END-IF./END-CALL. con il punto sbaglia!
          If isReservedWord(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Or IsNumeric(token) Then  'LABLE NUMERICHE!
            endStatement = True
            'linea buona per il giro successivo!
            Line = Space(4) & token & " " & Line
            'SQ 4-05-06
            'Mi serve linee perch� nei casi di "isReservedWord", gbnumrec uno in pi�
            linee = GbNumRec - GbOldNumRec
          Else
            'linea buona:
            statement = statement & " " & token & " " & Line
            'SQ - Fine statement con Punto finale o ELSE!
            If Len(Line) Then
              If Mid(Line, Len(Line), 1) = "." Then
                endStatement = True
                linee = GbNumRec - GbOldNumRec + 1
                Line = "" 'e' il flag... per la nuova lettura
              ElseIf Right(" " & Line, 5) = " ELSE" Then
                endStatement = True
                linee = GbNumRec - GbOldNumRec
                Line = "" 'e' il flag... per la nuova lettura
                'Mangio l'ELSE finale
                statement = Trim(Left(statement, Len(statement) - 4))
              Else
                endStatement = False
              End If
            Else
              endStatement = Mid(token, Len(token), 1) = "."
              linee = GbNumRec - GbOldNumRec + 1
            End If
          End If
        End If
      End If
    Wend
  Else
    'SQ 14-02-06: se CALL su unica riga, rimane "line" sporca...
    'stefano: vediamo cos�
    Line = ""
    linee = 1
  End If
  
  ''''''''''''''''''''''''''''''
  ' Modulo Chiamato:
  ''''''''''''''''''''''''''''''
  program = Replace(nexttoken(statement), ".", "")
  
  '''''''''''''''''''''''''''''''''''''''''''''''''
  ' Inserimento istruzione in PsCall: (trasformare in execute INSERT)
  '''''''''''''''''''''''''''''''''''''''''''''''''
  Dim rsCall As Recordset
  Set rsCall = m_Fun.Open_Recordset("select * from PsCall")
  rsCall.AddNew
  rsCall!idOggetto = GbIdOggetto
  rsCall!Riga = GbOldNumRec
  rsCall!programma = program  'attenzione: puo' contenere apici...
  rsCall!parametri = statement
  rsCall!linee = linee
  rsCall.Update
  rsCall.Close
  
  ''''''''''''''''''''''''''''''''''''''
  ' Relazioni/Segnalazioni
  ''''''''''''''''''''''''''''''''''''''
  checkRelations program, "CALL"
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Relazione programma/PSB
  ' (riconosce le istruzioni e le inserisce nella PsDli_Istruzioni
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If isDliCall Then 'settato dalla checkRelation
    checkPSB statement
  End If
  
  Exit Sub
callErr:
  'gestire...
  If err.Number = -2147217887 Then  'chiave duplicata...
    '2 CALL su una linea...
  Else
    err.Raise 124, "getCall", "syntax error on " & statement
    'Resume
  End If
End Sub
''''''''''''''''''''''''''''''''''
' Mangia i commenti "multi-linea"
''''''''''''''''''''''''''''''''''
Private Sub getComment(FD As Long, Line As String)
  'TMP: non gestisce commenti innestati o "*/" dentro "literal"...
  'While InStr(line, "*/") = 0 And Not EOF(FD)
  '  Line Input #FD, line
  '  GbNumRec = GbNumRec + 1
  'Wend
  'If InStr(line, "*/") Then line = Mid(line, InStr(line, "*/") + 1)
End Sub
Private Function isReservedWord(word As String) As Boolean
  Dim i As Integer
  
  For i = 0 To UBound(reserverdWords)
    If word = reserverdWords(i) Then Exit For
  Next
  
  isReservedWord = i <= UBound(reserverdWords)
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' E' una COPIA! SE RIMANE UGUALE UTILIZZARE QUELLA DEL Parser!
'
' Rendere definitiva (X TUTTI I TIPI DI RELAZIONE)
' Gestisce: CALL/COPY
' - AggiungiRelazione (PSB!)
' - AggiungiSegnalazione
' Effetti collaterali:
' - setta idOggettoRel
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub checkRelations(item As String, relType As String)
  Dim rsOggetti As Recordset
  
  isDliCall = False
  
  'Da verificare
  itemValue = item
  
  Select Case relType
    Case "CALL"
      If item = "QJBTDLI" Then
        SwDli = True        'programma DLI
        isDliCall = True    'istruzione DLI: serve al chiamante (e basta)
      Else
        ''''''''''''''''''''''''''''''''''
        ' SQ 15-02-06
        ' Gestione PsLoad
        ' Sostituire OVUNQUE la query commentata con il blocco sotto
        ''''''''''''''''''''''''''''''''''
        'OLD:
        'Set rsOggetti = m_Fun.Open_Recordset("select * from Bs_oggetti where (tipo = 'CBL' or tipo = 'ASM' or tipo = 'PLI') and nome = '" & itemValue & "'")
        'NEW:
        Set rsOggetti = m_Fun.Open_Recordset("select idOggetto,tipo from bs_oggetti where nome = '" & itemValue & "' and tipo in ('CBL','ASM','PLI')")
        If rsOggetti.RecordCount = 0 Then
          'Nome LOAD diverso da nome SOURCE: aggiorniamo il recordset
          Set rsOggetti = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.load = '" & itemValue & "' and b.source=a.nome and a.tipo in ('CBL','ASM','PLI')")
        End If
        
        If rsOggetti.RecordCount Then
          If rsOggetti.RecordCount = 1 Then
            AggiungiRelazione rsOggetti!idOggetto, relType, itemValue, itemValue
          Else
            'verificare 'sta storia...
            While Not rsOggetti.EOF
              'Inserisce le relazioni come duplicate nella tabella
              AggiungiRelazione rsOggetti!idOggetto, relType, itemValue, itemValue, True
              rsOggetti.MoveNext
            Wend
          End If
        Else
           If Not ExistObjSyst(itemValue, "PGM") Then
              addSegnalazione itemValue, "NO" & relType, itemValue
           End If
        End If
        rsOggetti.Close
      End If
    Case "QUIK-INCLUDE"
      itemValue = item
      IdOggettoRel = 0  'init
      'SERVE UN TIPO SPECIFICO????? CHE OGGETTI SONO????
      Set rsOggetti = m_Fun.Open_Recordset("select idOggetto from Bs_oggetti where (tipo = 'QIM') and nome = '" & itemValue & "'")
      If rsOggetti.RecordCount Then
        IdOggettoRel = rsOggetti!idOggetto  'serve al getFD
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ: 09-02-06 CONTROLLARE...
        'If rsOggetti.RecordCount = 1 Then
        '  AggiungiRelazione idOggettoRel, "COPY", item, item
        'Else
          While Not rsOggetti.EOF
            'Inserisce le relazioni come duplicate nella tabella
            AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue ''''''', True
            rsOggetti.MoveNext
          Wend
        'End If
      Else
        If Not ExistObjSyst(item, "CPY") Then
          addSegnalazione item, "NO" & relType, itemValue
        End If
      End If
      rsOggetti.Close
    Case Else
      '...
  End Select
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Analizza i parametri delle CALL IMS
' - Gestione PSB
' - Riconoscimento istruzioni: inserimento in PsDli_Istruzioni
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub checkPSB(parametri As String)
  Dim istrParam As String, istruzione As String, psbParam As String, psb As String
  Dim isSchedule As Boolean
  Dim rsIstruzioni As Recordset, rsOggetti As Recordset
  Dim IdPsb As Long
  Dim moveResponse As String
  
  On Error GoTo tmp
   
  istruzione = Replace(nextToken_tmp(parametri), ".", "")
  'QUICK: costanti = C'...'
  If istruzione = "C" Then
    istruzione = Trim(nextToken_tmp(parametri))
  Else
    '''MsgBox "istruzione?: " & istruzione
  End If
  
  istrParam = istruzione 'mi serve una copia se non riesco a risolvere la variabile...
    
   '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Prepariamo la struttura con il campo istruzione che successivamente
  ' sar� associato ai possibili valori determinati dalle move
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ReDim Preserve CmpIstruzione(UBound(CmpIstruzione) + 1)
  CmpIstruzione(UBound(CmpIstruzione)).CampoIstruzione = istruzione
  CmpIstruzione(UBound(CmpIstruzione)).Oggetto = GbIdOggetto
  CmpIstruzione(UBound(CmpIstruzione)).Riga = GbOldNumRec
  CmpIstruzione(UBound(CmpIstruzione)).Risolta = 0
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'Se il parametro "istruzione" ha il nome esatto dell'istruzione, OK
  'altrimenti cerca il "VALUE":
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Select Case istruzione
    Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP"
    Case "DLET", "REPL", "ISRT"
    Case "PCB", "TERM"
    Case "AUTH", "GCMD", "ICMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG"
    Case "SETB", "SETO", "SETS", "SETU", "XRST"
    Case "SYNC", "CHKP", "CKPT", "APSB", "DPSB", "INIT", "INQY", "LOG", "ROLB", "ROLL", "ROLS"
    Case "OPEN", "CLSE"
    Case Else
      'VALUE:
      istruzione = getFieldValue(istruzione)
  End Select
  'Doppio controllo, ma almeno evitiamo il "PrendiValue" per i casi "sicuri"...
  Select Case istruzione
    Case "PCB"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
      isSchedule = True
    Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "DLET", "REPL", "ISRT"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "TERM"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "AUTH", "GCMD", "ICMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "SETB", "SETO", "SETS", "SETU", "XRST"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "SYNC", "CHKP", "CKPT", "APSB", "DPSB", "INIT", "INQY", "LOG", "ROLB", "ROLL", "ROLS"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "OPEN", "CLSE"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case ""
      'getFieldValue non ha trovato niente
      ' provo a cercare fra le move fin qui trovate
      moveResponse = risolviIstruzioneByMove(CmpIstruzione(UBound(CmpIstruzione)).CampoIstruzione, CmpIstruzione(UBound(CmpIstruzione)).LastSerchIndex, 0)
      If Not Len(moveResponse) Then
        addSegnalazione istrParam, "NOISTRDLIVALUE", istrParam
      Else
        istruzione = moveResponse
        CmpIstruzione(UBound(CmpIstruzione)).Risolta = 2
      End If
    Case Else
      moveResponse = risolviIstruzioneByMove(CmpIstruzione(UBound(CmpIstruzione)).CampoIstruzione, CmpIstruzione(UBound(CmpIstruzione)).LastSerchIndex, 0)
      If Not Len(moveResponse) Then
        addSegnalazione istruzione, "NOISTRDLI", istruzione
        istruzione = ""
      Else
        istruzione = moveResponse
        CmpIstruzione(UBound(CmpIstruzione)).Risolta = 2
      End If
      
  End Select
  
 
  ''''''''''''''''''''''''''''''''''''''''''''
  ' Inserimento PsDli_Istruzioni:
  ''''''''''''''''''''''''''''''''''''''''''''
  ' AC :Case "" e ricerca move senza successo scrive campo vuoto
  ' a fine parser cercher� fra le move successive
  
  Set rsIstruzioni = m_Fun.Open_Recordset("select IdOggetto,Riga,istruzione,statement,isCBL from PsDli_Istruzioni")
  rsIstruzioni.AddNew
  rsIstruzioni!idOggetto = GbIdOggetto
  rsIstruzioni!Riga = GbOldNumRec
  rsIstruzioni!istruzione = istruzione
  'Parametri "utili" (gia' puliti da tutto...compreso eventuale punto finale)
  If Len(parametri) Then
    rsIstruzioni!statement = Trim(IIf(Right(parametri, 1) = ".", Left(parametri, Len(parametri) - 1), parametri))
  Else
    rsIstruzioni!statement = ""
  End If
  rsIstruzioni!isCBL = itemValue = "QJBTDLI"
  
  'Chiudo in fondo... dopo il controllo del psb
  
  ''''''''''''''''''''''''''''''''''''''''''''
  ' Schedulazione (gestione PSB):
  ' Es:
  ' - "PCB G006-PSBNAME ADDRESS OF DLIUIB"
  ' - "PCB G006-PSBNAME DLIUIB"
  ''''''''''''''''''''''''''''''''''''''''''''
  If isSchedule Then
    psbParam = nexttoken(parametri)
    'gestire psbParam=""...
    If Left(psbParam, 1) = "'" Then 'Puo' essere anche il valore costante?
      psb = Mid(psbParam, 2, Len(psbParam) - 2)
    Else
      '?????
      If ActParsNoPsb Then
        '??????
        psb = GbParmRel.DLIStdPsbName
      Else
        '''''''''''''''''''''''''''''''''''''''''''''''
        ' - VALUE iniziale; se non c'�:
        ' - MOVE (la prima, ma le prenderei tutte!)
        '''''''''''''''''''''''''''''''''''''''''''''''
        psb = PrendiValue(psbParam)
        If psb = "SPACE" Or psb = "SPACES" Or psb = "None" Then
          psb = TrovaPsb(psbParam)
        End If
      End If
    End If
        
    If Len(psb) Then
      Set rsOggetti = m_Fun.Open_Recordset("select idOggetto from Bs_oggetti where tipo = 'PSB' and nome = '" & psb & "'")
      If rsOggetti.RecordCount = 0 Then
        addSegnalazione psb, "NOPSB", psb
      Else
        AggiungiRelazione rsOggetti!idOggetto, "PSB", psb, psb
      End If
      rsOggetti.Close
    Else
      addSegnalazione psbParam, "NOPSBVALUE", psbParam
    End If
    
  End If
  rsIstruzioni.Update
  rsIstruzioni.Close
  
  Exit Sub
tmp:
  'gestire...
  MsgBox err.description
  'Resume
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' COPIA...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getMove(Line As String, FD As Long, ByRef pcampiMove() As UtCampiMove)
  Dim statement As String, variable As String, token As String
  Dim endStatement As Boolean
  Dim linee As Integer
  Dim campo As String, campoof As String, campoend As String
  
  Dim i As Integer, j As Integer
  On Error GoTo callErr
  
  GbOldNumRec = GbNumRec  'linea iniziale
  ''''''''''''''''''''''''''''''''''
  ' Becco tutte le linee di call...
  ''''''''''''''''''''''''''''''''''
  statement = Line
  If Len(Line) = 0 Then Line = " "
  If Mid(Line, Len(Line), 1) <> "." Then
    While Not EOF(FD) And Not endStatement
      Line Input #FD, Line
      GbNumRec = GbNumRec + 1
      If Left(Line, 1) <> "*" Then
        'If Mid(line, 7, 1) <> "-" Then
          'Eliminazione Commenti a met� linea:
          Line = Trim(Left(Line, InStr(Line & "/*", "/*") - 1))
          ' posso avere una linea vuota? (prob. no...)
          If Len(Line) Then
            'Altro statement?
            'If isCobolLabel(line) Then 'LABEL!
            '  endStatement = True
            '  linee = GbNumRec - GbOldNumRec
            'Else
              token = nexttoken(Line)
              'Parola Cobol?
              'SQ 27-04-06: se mi arriva END-IF./END-CALL. con il punto sbaglia!
              If isReservedWord(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Or IsNumeric(token) Then  'LABLE NUMERICHE!
                endStatement = True
                'linea buona per il giro successivo!
                Line = Space(4) & token & " " & Line
                'SQ 4-05-06
                'Mi serve linee perch� nei casi di "isReservedWord", gbnumrec uno in pi�
                linee = GbNumRec - GbOldNumRec
              Else
                'linea buona:
                statement = statement & " " & token & " " & Line
                'SQ - Fine statement con Punto finale o ELSE!
                If Len(Line) Then
                  If Mid(Line, Len(Line), 1) = "." Then
                    endStatement = True
                    linee = GbNumRec - GbOldNumRec + 1
                    Line = "" 'e' il flag... per la nuova lettura
                  ElseIf Right(" " & Line, 5) = " ELSE" Then
                    endStatement = True
                    linee = GbNumRec - GbOldNumRec
                    Line = "" 'e' il flag... per la nuova lettura
                    'Mangio l'ELSE finale
                    statement = Trim(Left(statement, Len(statement) - 4))
                  Else
                    endStatement = False
                  End If
                Else
                  endStatement = Mid(token, Len(token), 1) = "."
                  linee = GbNumRec - GbOldNumRec + 1
                End If
              End If
            'End If
          End If
        'Else
        '  line = Trim(Mid(line, 8, 65))
        '  line = Mid(line, 2)
        '  statement = statement & " " & line
        'End If
      End If
    Wend
  Else
    'SQ 14-02-06: se CALL su unica riga, rimane "line" sporca...
    Line = ""
  End If
  
  '
  Dim bolCostant As Boolean, bolnumeric As Boolean
  If Left(statement, 2) = "C'" Then
    bolCostant = True
    statement = Mid(statement, 2)
  Else
    bolCostant = False
  End If
  variable = Replace(nexttoken(statement), ".", "")
  If variable = "ALL" Then
    If Left(statement, 1) = "'" Then
      bolCostant = True
    Else
      bolCostant = False
    End If
    variable = Replace(nexttoken(statement), ".", "")
  End If
  If IsNumeric(variable) Then
    bolnumeric = True
  Else
    bolnumeric = False
  End If
  If bolCostant Or bolnumeric Then
    variable = "'" & variable & "'"
  End If
  'tmp: indici! li eliminiamo... GESTIRE!
  If Left(statement, 1) = "(" Then token = nexttoken(statement)
  token = nexttoken(statement) 'serve solo a mangiare l'eventuale "TO"
  While Len(statement)
    campo = nexttoken(statement)
    'tmp: indici! li eliminiamo... GESTIRE!
    If Left(statement, 1) = "(" Then token = nexttoken(statement)
    campoof = nexttoken(statement)
    ' se successivo OF compongo campo
    ' altrimenti ricostruisco statement
    If campoof = "OF" Then
      campoend = nexttoken(statement)
      campo = campo & " OF " & campoend
    ElseIf Not campoof = "" Then ' ero gi� alla fine
      statement = campoof & " " & statement
    End If
      
      
    If Right(campo, 1) = "." Then
      campo = Left(campo, Len(campo) - 1)
    End If
   
       
    For i = 1 To UBound(pcampiMove)
      If pcampiMove(i).NomeCampo = campo Then
        Exit For
      End If
    Next
    If i = UBound(pcampiMove) + 1 Then ' campo nuovo
      ReDim Preserve pcampiMove(UBound(pcampiMove) + 1)
      pcampiMove(UBound(pcampiMove)).NomeCampo = Replace(campo, ",", "")
      Set pcampiMove(UBound(pcampiMove)).Valori = New Collection
      pcampiMove(UBound(pcampiMove)).Valori.Add variable
    Else
      For j = 1 To pcampiMove(i).Valori.count
        If pcampiMove(i).Valori.item(j) = variable Then
          Exit For
        End If
      Next
      If j = pcampiMove(i).Valori.count + 1 Then
        pcampiMove(i).Valori.Add variable
      End If
    End If
    If Trim(statement) = "." Then statement = ""
  Wend
  
  Exit Sub
callErr:
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(Line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, Line
      GbNumRec = GbNumRec + 1
      '...
    Else
      err.Raise 124, "getMove", "syntax error on " & statement
      'Resume
    End If
  End If
  'gestire...
'''  If Err.Number = -2147217887 Then  'chiave duplicata...
'''    '2 CALL su una linea...
'''  Else
'''    Err.Raise 124, "getMove", "syntax error on " & statement
'''    Resume
'''  End If
End Sub
''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''
Sub getInclude(parameters As String)
  Dim rs As Integer
  Dim token As String
  
  GbOldNumRec = GbNumRec  'linea iniziale
  
  token = nexttoken(parameters)
  
  'tmp
  If Len(parameters) Then MsgBox "TMP: getINCLUDE - " & parameters & "?"
  
  checkRelations token, "QUIK-INCLUDE"
  
'  Set TbOggetti = m_Fun.Open_Recordset("select * from bs_oggetti where tipo = 'CPY' and nome = '" & Wstr2 & "'")
'  If TbOggetti.RecordCount > 0 Then
'     If TbOggetti.RecordCount = 1 Then
'         AggiungiRelazione TbOggetti!idOggetto, "INCLUDE", Wstr2, Wstr2
'      Else
'        While Not TbOggetti.EOF
'            'Inswerisce le relazioni come duplicate nella tabella
'            AggiungiRelazione TbOggetti!idOggetto, "INCLUDE", Wstr2, Wstr2, True
'
'            TbOggetti.MoveNext
'        Wend
'      End If
'  Else
'     If Not ExistObjSyst(Wstr2, "CPY") Then
'         AggiungiSegnalazione Wstr2, "NOINCLUDE", Wstr2
'     End If
'  End If

End Sub
''''''''''''''''''''''''''''''''''
' Definizione Dati:
' Allestisce la PsQUIK_Dati
''''''''''''''''''''''''''''''''''
Private Sub getEQU(parameters As String)
  Dim rs As Recordset
  Dim token As String
  
  On Error GoTo dataErr

  equOrdinale = equOrdinale + 1
  
  Set rs = m_Fun.Open_Recordset("Select * from PsQUIK_Dati")
  rs.AddNew
  
  rs!idOggetto = GbIdOggetto
  rs!Ordinale = equOrdinale
  
  'Nome
  token = nextToken_tmp(parameters)
  rs!nome = token
  'Dichiarazione
  token = nextToken_tmp(parameters)
  rs!Tipo = Left(token, 3)
  rs!Posizione = Mid(token, 4)
  'Valore
  If Len(parameters) Then
    token = nextToken_tmp(parameters)
    If token = "C" Then
      token = nextToken_tmp(parameters)
     DoEvents
    End If
    rs!Valore = token
  End If
  
  rs.Update
  rs.Close
  Exit Sub
dataErr:
  'tmp
  MsgBox err.description

End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Parsing di II livello
' Analizza le istruzioni IMS che trova in PsDli_Istruzioni
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parsePgm_II(Optional pRiga As Long = -1, Optional pIdOggetto As Long = -1)
  Dim istruzione As String, parametri As String
  Dim rs As Recordset
  Dim rsCall As Recordset
  Dim OptWhereCond As String
  
  On Error GoTo parseErr
    
  ReDim idAree(0)
  ReDim ssaCache(0)
  ReDim wPcb(0)
  ReDim xPcb(0)
  ReDim fSegm(0)
   
  If pRiga = -1 Then
    OptWhereCond = ""
  Else
    OptWhereCond = " and Riga = " & pRiga
    GbIdOggetto = pIdOggetto
  End If
  'Naming Convention: nome segmento/nome area
  segmentNameParam = LeggiParam("SEGM-NAME-CRITERIA")
 
  'ATTENZIONE:
  'per ora non ho l'info "CALL/EXEC":
  
  ''''''''''''''''''''''''''''''''
  ' PSB Schedulati
  ' tmp: valorizza IdPsb (uno solo) come nel vecchio giro...)
  ''''''''''''''''''''''''''''''''
  getPsb_program
  ' Mauro 07-05-2007 : Spostato per gestione istruzioni in COPY
  gbCurEntryPCB = getENTRY(GbIdOggetto)
  
  ''''Set rs = m_Fun.Open_Recordset("Select istruzione,statement,Riga,valida,isExec from PsDLI_Istruzioni where idOggetto=" & GbIdOggetto)
  Set rs = m_Fun.Open_Recordset("Select isExec,istruzione,statement,Riga,valida from PsDLI_Istruzioni where idOggetto=" & GbIdOggetto & OptWhereCond)
  'Set rs = m_Fun.Open_Recordset("Select a.istruzione,a.statement,a.Riga from PsDLI_Istruzioni as a,PsCall as b where a.idOggetto=b.idOggetto and a.riga=b.riga and b.idOggetto=" & GbIdOggetto & " and (b.Programma='AIBTDLI' or b.Programma='CBLTDLI')")
  
  If rs.RecordCount Then
        
    If SwPsbAssociato Then
      IdPsb = psbPgm(0)
      'warning se piu' di uno:
      'AggiungiSegnalazione " ", "MANYRELPSB", " "
    Else
      addSegnalazione " ", "NORELPSB", " "
    End If
    '''''''SOLO CALL!!!!!!!!!!!!!!!!!!!!!!!
    ' ENTRY:
    ' gbCurEntryPCB: array globale con i PCB...
    ' (se globale perche' usiamo una function?)
    ''''''''''''''''''''''''''''''''
    'gbCurEntryPCB = CaricaEntry(GbIdOggetto)  'ci mette una vita!!!
    'Modificato... toglieva l'I-O e altre cose...
    ' Mauro 07-05-2007 : Spostato per gestione istruzioni in COPY
    'gbCurEntryPCB = getENTRY(GbIdOggetto)
  End If
  
  While Not rs.EOF
''    Set rsCall = m_Fun.Open_Recordset("Select Parametri from PsCall where IdOggetto = " & GbIdOggetto _
''                                       & " and Riga = " & rs!riga)
''    If Not rsCall.EOF Then
''      AggiornaCloniIstruzione rs, nextToken(rsCall!parametri)
''    End If
''    rsCall.Close
    
    istruzione = IIf(IsNull(rs!istruzione), "", rs!istruzione)
    parametri = rs!statement
    GbOldNumRec = rs!Riga
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Select Case istruzione
      Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP"
        If rs!isExec Then
          'parseGU_exec istruzione, parametri
        Else
          'parseGU_call istruzione, parametri
          getSegment istruzione, parametri
        End If
      Case "DLET", "REPL"
        'parseREPL_call istruzione, parametri
      Case "ISRT"
        'parseISRT_call istruzione, parametri
        getSegment istruzione, parametri
      Case "CHKP"
        'parseCHKP parametri
      Case "AUTH"
        'parseAUTH parametri
      Case "XRST"
        'parseXRST parametri
      Case "ICMD"
        'parseICMD parametri
      Case "CHNG"
        'parseCHNG parametri
      Case "INQY"
        parseINQY parametri
      Case "LOG", "ROLB", "ROLL", "ROLS"
        'TMP: riuso il vecchio giro... sostituendo man mano...
        'parseROLL istruzione, parametri
      Case "PCB", "TERM"  ',"SCHEDULE","SCHD"
         'parsePCB parametri
      Case "SYNC"
        rs!valida = True
        rs.Update
      Case "CKPT", "APSB", "DPSB", "INIT"
        rs!valida = True
        rs.Update
      Case "OPEN", "CLSE"
        'parseOPEN_CLOSE istruzione, parametri
      Case "GCMD", "RCMD", "GSCD", "GMSG", "PURG"
        'Fare bene...
        'parsePURG istruzione, parametri
      Case "SETB", "SETO", "SETS", "SETU"
        'Fare bene...
        'parseSETx istruzione, parametri
      Case Else
        'gestire...
    End Select
    
''''    'CHIARIRE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
''''    'If Not SwParsExec Then
''''      Dim vPam As Variant
''''      vPam = m_Fun.RetrieveParameterValues("PS_FORCE_DLET_REPL")
''''      If vPam(0) = "Yes" Then
''''         'SG : Risolve dlet e repl quando il rapporto GH* � uno a uno con lo stesso PCB
''''         Resolve_Single_GH_REPL_DLET
''''      End If
''''   'End If
   rs.MoveNext
  Wend
  rs.Close
  
  '''''''''''''''''
  '  'EXEC?
  '''''''''''''''''
  '  'Set rs = m_Fun.Open_Recordset("Select * from PsExec_SQ where idOggetto=" & GbIdOggetto & " AND type='DLI'")
  '
  '  'DliCercaEXEC wRec, FD
  
  ''''''''''''''''''''''''''''''''''''''''''''
  ' Update Bs_oggetti: data,livello,errorLevel
  ''''''''''''''''''''''''''''''''''''''''''''
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  TbOggetti!DtParsing = Now
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti!parsinglevel = 2
  TbOggetti.Update
  TbOggetti.Close
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Aggiornamento DBD in PsRel_Obj e SEGM in PsComObj (!?)
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  AggiornaRelIstrDli GbIdOggetto
  
  Exit Sub
parseErr:
  'gestire meglio...
  If err.Number = -2147467259 Then  'tmp - problema Alby... prj Perot
    'm_Fun.WriteLog "parseCbl_II: id#" & GbIdOggetto & "#" & Error & "#" & Err.Description
    addSegnalazione GbNomePgm & ": nested PSB", "I00", GbNomePgm & ": nested PSB"
  Else
    'MsgBox "II level parsing: " & Err.Description
    addSegnalazione rs!statement, "I00", rs!statement
    'Resume
  End If
End Sub

Private Sub parseGU(istruzione As String, parametri As String)
  Dim rs As Recordset, tb As Recordset, tb1 As Recordset, tb2 As Recordset, rsdbd As Recordset
  Dim i As Integer, k As Integer, y As Integer, T1 As Integer, T2 As Integer, T3 As Integer, T4 As Integer
  Dim dbdName As String, wNomePsb As String, ssaValue As String
  Dim isGSAM As Boolean, unqualified As Boolean
  Dim idField As Integer
  
  On Error GoTo parseErr
  
  ReDim gSSA(0)
  ReDim dbdPgm(0)
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
'''''  TabIstr.pcb = nextToken(parametri)
'''''  wDliRec = nextToken(parametri)
'''''  TabIstr.Istr = istruzione
'''''
'''''  '''''''''''''''''''''
'''''  ' PCB (number)
'''''  '''''''''''''''''''''
'''''  TabIstr.numPCB = getPCBnumber(TabIstr.pcb)
'''''  If isDC Then
'''''    parseRECEIVE istruzione, wDliRec
'''''    Exit Sub
'''''  Else
'''''    isDC = False
'''''    'se non trova il PCB? (No entry...)
'''''  End If
'''''
'''''  'SQ 27-04-06 -
'''''  'COMPLETAMENTE SQUALIFICATA?
'''''  unqualified = Len(parametri) = 0
'''''
'''''  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''  ' Gestione SSA
'''''  ' Valorizzo la TabIstrSSa
'''''  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''  Dim ssa
'''''  ReDim wDliSsa(0)
'''''  ReDim TabIstrSSa(0)
'''''  ReDim TabIstrSSa(0).NomeSegmento.value(0)
'''''  Dim ssaOk As Boolean
'''''  ssaOk = True
'''''  While Len(parametri)
'''''    i = i + 1
'''''    ReDim Preserve wDliSsa(UBound(wDliSsa) + 1)
'''''    wDliSsa(UBound(wDliSsa)) = nextToken(parametri)
'''''
'''''    'vedere 'sto blocco se serve...
'''''    Dim rsArea As Recordset, rsLink As Recordset, rsCmp As Recordset
'''''    Set rsArea = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & GbIdOggetto)
'''''    If rsArea.RecordCount = 0 Then
'''''      'Ricerca nelle COPY:
'''''      Set rsLink = m_Fun.Open_Recordset("Select IdOggettoR From PsRel_Obj Where IdOggettoC = " & GbIdOggetto & " And Relazione = 'CPY'")
'''''      Do While Not rsLink.EOF
'''''        Set rsCmp = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & rsLink!IdOggettoR)
'''''        If rsCmp.RecordCount Then
'''''           Set rsArea = rsCmp
'''''           Exit Do
'''''        End If
'''''        rsLink.MoveNext
'''''      Loop
'''''      rsLink.Close
'''''    End If
'''''
'''''    'Valorizza TabIstrSSa:
'''''    If rsArea.RecordCount Then
'''''      'SQ - tmp: mettere in linea...
'''''      'Non funge per indirizzo? (in testa?)
'''''      ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
'''''      TabIstrSSa(UBound(TabIstrSSa)).SSAName = wDliSsa(UBound(wDliSsa))
'''''      TabIstrSSa(UBound(TabIstrSSa)).idOggetto = rsArea!idOggetto
'''''      TabIstrSSa(UBound(TabIstrSSa)).idArea = rsArea!idArea
'''''      parseSSA TabIstrSSa(UBound(TabIstrSSa))
'''''    Else
'''''      ''''''''''''''''''''''''''''''''''''''''
'''''      ' SQ - 19-04-06
'''''      ' Nuova segnalazione (I02)
'''''      ''''''''''''''''''''''''''''''''''''''''
'''''      AggiungiSegnalazione wDliSsa(UBound(wDliSsa)), "NOSSAVALUE", wDliSsa(UBound(wDliSsa))
'''''      ssaOk = False
'''''    End If
'''''    rsArea.Close
'''''  Wend
'''''
'''''  ''''''''''''''''''''''''''''''''''''''''
'''''  ' SQ 20-04-06
'''''  ' Area Segmento - la associava a tutti!
'''''  '''''''''''''''''''''''''''''''''''''''''
'''''  If ssaOk And Not unqualified Then
'''''    TabIstrSSa(UBound(TabIstrSSa)).Struttura = wDliRec
'''''    If Len(TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0)) = 0 And Len(wDliRec) Then  'Solo sul Segmento finale...
'''''      ''''''''''''''''''''''''''''''''''''''''''''''
'''''      ' SQ 2-05-06
'''''      ' Naming Convention SEGMENTO/AREA
'''''      ''''''''''''''''''''''''''''''''''''''''''''''
'''''      Dim env As Collection 'area,idDbd
'''''      If Len(segmentNameParam) Then
'''''        Set env = New Collection  'resetto;
'''''        env.Add wDliRec
'''''        'SQ 5-06-06 - Come poteva funzionare?
'''''        env.Add 0 'wIdDBD(0)
'''''        'SQ MOVED/VALUE
'''''        TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0) = getEnvironmentParam(segmentNameParam, "IMS", env)
'''''      End If
'''''    End If
'''''    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''    'SQ - Cambiamento mostruoso (pezzo di analizzaCallDli_SQ)
'''''    ' TabIstrSSa -> TabIstr
'''''    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''    getTabIstr
'''''  End If
'''''  ''''''''''''''''''''''''''''''''
'''''  ' Gestione PSB/DBD:
'''''  ' Input: N PSB, PCB, M SEGMENTI
'''''  ''''''''''''''''''''''''''''''''
'''''  getInstructionPsbDbd (TabIstr.numPCB)
'''''
'''''  'SQ - 27-04-06
'''''  'If isGSAM Then
'''''  If TabIstr.DBD_TYPE = DBD_GSAM Then
'''''    getGSAMinfo (TabIstr.numPCB)  'wIDdbd...
'''''    'If TabIstr.DBD_TYPE <> DBD_GSAM Then
'''''    '  'Non ho la SSa:
'''''    '  AggiungiSegnalazione GbNomePgm, "NODECOD", GbNomePgm
'''''    '  Exit Sub
'''''    'End If
'''''  Else
'''''    ''''''''''''''SQ - 27-04-06 (sopra: serve cmq) getInstructionPsbDbd (TabIstr.numPCB)
'''''    ''''''''''''''''''''''''''''''''''''''''''
'''''    ' TabIstr.BlkIstr:
'''''    ''''''''''''''''''''''''''''''''''''''''''
'''''    If ssaOk And IsValid Then 'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
'''''      getTabIstrIds
'''''    Else
'''''      If Not unqualified Then
'''''        IsValid = False
'''''      End If
'''''    End If
'''''  End If
'''''
'''''  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''  'SQ 10-04-06
'''''  ' Se usciamo qui perdiamo tutti i settaggi per l'istruzione!
'''''  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''  If IsValid And Not unqualified Then 'SQ 27-04-06
'''''    ''''''''''''''''''''''''''''''''''''
'''''    'ITERAZIONE LIVELLI ISTRUZIONE:
'''''    ''
'''''    'AGGIORNAMENTO "PsDli_IstrDett_Liv"
'''''    ''''''''''''''''''''''''''''''''''''
'''''    Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
'''''    For k = 1 To UBound(TabIstr.BlkIstr)
'''''      '''''''''''''''''''''
'''''      'SEGMENTO VARIABILE?
'''''      '''''''''''''''''''''
'''''      If Left(TabIstr.BlkIstr(k).segment, 1) = "(" And Right(TabIstr.BlkIstr(k).segment, 1) = ")" Then
'''''        ReDim IdSegmenti(0)
'''''        '''''''''''''''''''''''''''''''''''''
'''''        ' Segmento variabile: gestione temp.
'''''        '''''''''''''''''''''''''''''''''''''
'''''      End If
'''''
'''''      Dim wNomeCmpLiv As String, wIdStr As String, wStrIdOggetto As String, wStrIdArea As Long
'''''
'''''      wIdStr = getIdArea(TabIstr.BlkIstr(k).Record)
'''''      wNomeCmpLiv = TabIstr.BlkIstr(k).Record
'''''
'''''      'ADESSO HO N SEGMENTI QUANTI SONO I DBD... (o ZERO!)
'''''     If SwParsExec Then
'''''      '...
'''''     Else
'''''      'SQ 28-11-05:
'''''      If TabIstr.BlkIstr(k).IdSegm Then
'''''        wStrIdOggetto = Val(Left(wIdStr, 6))
'''''        wStrIdArea = Val(Mid(wIdStr, 8, 4))
'''''        'SQ - 28-11-05
'''''        'Pezza provvisoria: capire perche' arriva cosi'...
'''''        If wStrIdOggetto <> 0 And wStrIdArea <> 0 Then
'''''          Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & TabIstr.BlkIstr(k).IdSegm _
'''''               & " and stridoggetto = " & wStrIdOggetto _
'''''               & " and stridarea = " & wStrIdArea _
'''''               & " and nomecmp = '" & wNomeCmpLiv & "'")
'''''
'''''          If tb2.RecordCount = 0 Then
'''''             tb2.AddNew
'''''          End If
'''''
'''''          tb2!IdSegmento = TabIstr.BlkIstr(k).IdSegm
'''''          tb2!stridoggetto = wStrIdOggetto
'''''          tb2!strIdArea = wStrIdArea
'''''          tb2!NomeCmp = wNomeCmpLiv
'''''          'AC
'''''          tb2.Update
'''''          tb2.Close
'''''        End If
'''''      End If
'''''     End If
'''''
'''''     If TabIstr.DBD_TYPE <> DBD_GSAM Then
'''''        If Len(IdSegmenti(0)) = 0 Then SwSegm = TabIstr.BlkIstr(k).segment
'''''     End If
'''''
'''''    Dim wNomeCmp As String
'''''     i = InStr(wIdStr, "-")
'''''     If i > 0 Then
'''''        wNomeCmp = Mid(wIdStr, i + 2)
'''''        wIdStr = Val(Mid(wIdStr, 1, i - 1))
'''''     Else
'''''        wNomeCmp = " "
'''''     End If
'''''
'''''     ''''''''''''''''''''''''''''''''''''''
'''''     'INSERIMENTO IN "PsDli_IstrDett_Liv":
'''''     ''''''''''''''''''''''''''''''''''''''
'''''     tb.AddNew
'''''     tb!idOggetto = GbIdOggetto
'''''     tb!riga = GbOldNumRec
'''''     tb!livello = k
'''''     ' tb!dtsegnalazione = Now
'''''
'''''    'SQ: Cos'� 'sta roba???
'''''    'dovremo aggiungere quei due campi nella TabIstr!!!!!!!!!!!!!!!!!!!!
'''''    For i = 0 To UBound(TabIstrSSa)
'''''      If Trim(TabIstrSSa(i).SSAName) = Trim(TabIstr.BlkIstr(k).SSAName) Then
'''''        tb!stridoggetto = TabIstrSSa(i).idOggetto
'''''        tb!strIdArea = TabIstrSSa(i).idArea
'''''        Exit For
'''''      End If
'''''    Next
'''''
'''''     'SQ-2: CHIARIRE: LI HO APPENA CALCOLATI SOPRA!?
'''''    ' y = InStr(wIdStr, "/")
'''''     'If y > 0 Then
'''''        'tb!strIdOggetto = Left(wIdStr, 6)
'''''        'tb!strIdArea = Mid(wIdStr, y + 1, 4) 'SOPRA USO 6 E QUI Y?!
'''''     'Else
'''''      ''SQ - controllare: era sbagliato! wStrIdOggetto > 0... ma � string!?
'''''        'If wStrIdArea > 0 And Len(wStrIdOggetto) Then
'''''           'tb!strIdOggetto = wStrIdOggetto
'''''           'tb!strIdArea = wStrIdArea
'''''        'End If
'''''     'End If
'''''
'''''
'''''     'SQ-2: attenzione: ne ho N!
'''''     'EVENTUALMENTE FARE TABELLA DEDICATA...
'''''     '(IN TEORIA NE BASTA 1 PER RECUPERARE IL NOME... POI SE SERVE HO LA TABELLA DBD/ISTRUZIONE)
'''''     If SwParsExec Then
'''''  ''''''      If Len(IdSegmenti(0)) Then
''''''...
'''''  ''''''         ' Gestione Segmenti variabili:
'''''     End If
'''''
'''''     If Not SwParsExec Then 'CALL
'''''        tb!IdSegmento_MOVED = TabIstr.BlkIstr(k).segment_moved
'''''        If TabIstr.BlkIstr(k).IdSegm Then
'''''           tb!IdSegmento = TabIstr.BlkIstr(k).IdSegm
'''''        Else
'''''          'Cerca l'id segmento nelle tabelle mg (id segmento associato da noi)
'''''          tb!IdSegmento = getIdSegmento_By_MgTable(TabIstr.BlkIstr(k).Record)
'''''          'SQ 9-05-06: nuova gestione segmenti... non serve pi�
''''''          If tb!IdSegmento = 0 Then
''''''             'se � un gsam associa l'unico segmento possibile legato a quel dbd
''''''             If TabIstr.DBD_TYPE = DBD_GSAM Then
''''''                'Usa wPcb!!!! modificare...
''''''                tb!IdSegmento = Assegna_IdSegmento_ByNomePCB_GSAM()
''''''             End If
''''''          End If
'''''
'''''          '''''''''''''''''''''''''''''''''''''''''''''
'''''          'SQ -10-04-06
'''''          'Segmento non trovata: istruzione INVALIDA!
'''''          '''''''''''''''''''''''''''''''''''''''''''''
'''''          If tb!IdSegmento Then
'''''             TabIstr.BlkIstr(k).IdSegm = tb!IdSegmento
'''''             TabIstr.BlkIstr(k).segment = getNomeSegmento(tb!IdSegmento)
'''''             If Len(TabIstr.BlkIstr(k).SegLen) = 0 Then
'''''               TabIstr.BlkIstr(k).SegLen = getLenSegmentoByIdSegmento(tb!IdSegmento)
'''''             End If
'''''            Else
'''''              IsValid = False
'''''            End If
'''''        End If
'''''     End If
'''''
'''''     'E' sempre vuoto! servirebbe una "getLenSegmentoByIdSegmento(tb!IdSegmento)",
'''''     'ma metterlo a posto a monte...
'''''     If IsNumeric(TabIstr.BlkIstr(k).SegLen) Then 'blank/null (numerico <- stringa) !
'''''      tb!SegLen = CInt(TabIstr.BlkIstr(k).SegLen)
'''''     Else
'''''      tb!SegLen = 0
'''''     End If
'''''     tb!NomeSSA = TabIstr.BlkIstr(k).SSAName   'togliere sti bianchi in fondo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
'''''     tb!DataArea = TabIstr.BlkIstr(k).Record
'''''
'''''     'SQ
''''''     If Len(TabIstr.BlkIstr(k).CodCom) Then
''''''      TabIstr.BlkIstr(k).Search = DecodificaCodCom(TabIstr.BlkIstr(k).CodCom)
''''''      If InStr(TabIstr.BlkIstr(k).Search, "Undefined") Then
''''''        TabIstr.BlkIstr(k).valida = False
''''''        'NUOVA SEGNALAZIONE
''''''      End If
''''''     End If
'''''
'''''     'COMMAND CODES
'''''     'Occhio: c'� la search gi� decodificata!
'''''     tb!condizione = Left(TabIstr.BlkIstr(k).CodCom, 100)
'''''     tb!ComCodes_moved = TabIstr.BlkIstr(k).CodCom_moved
'''''
'''''     'QUALIFICAZIONE (MULTIPLA)
'''''     tb!Qualificazione = TabIstr.BlkIstr(k).qualified
'''''     tb!Qualificazione_moved = TabIstr.BlkIstr(k).qualified_moved
'''''     tb!QualAndUnqual = TabIstr.BlkIstr(k).qual_unqual
'''''
'''''     tb!Correzione = False
'''''     tb!moved = TabIstr.BlkIstr(k).moved
'''''
'''''     'SQ - 27-05-05: aspettare la validita' delle chiavi per chiudere...
'''''
'''''     '''''''''''''''''''''''''''''''''''''''''''''''''
'''''     'GESTIONE CHIAVI:
'''''     'Cerca ID campo chiave
'''''     'Allestisce tabella record in PsDli_IstrDett_Key
'''''     'Setta la validita' dell'istruzione;
'''''     '''''''''''''''''''''''''''''''''''''''''''''''''
'''''     For y = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
'''''        'SQ-2: temporaneo - valutare caso di segmenti multipli....
'''''        'idField = CercaIdField(wIdSegm, TabIstr.BlkIstr(K).KeyDef(y).Key)
'''''        If TabIstr.BlkIstr(k).IdSegm Then
'''''           'piu' di un elemento: indeterminatezza
'''''           idField = CercaIdField(TabIstr.BlkIstr(k).IdSegm, TabIstr.BlkIstr(k).KeyDef(y).Key)
'''''        Else
'''''           idField = 0
'''''        End If
'''''
'''''        'SQ - 27-05-05
'''''        If idField = 0 Then
'''''          TabIstr.BlkIstr(k).valida = False 'istruzione sbagliata in ogni caso!
'''''          IsValid = False
'''''          '''''''''''''''''''''''''''''''''''''''
'''''          ' SQ 5-05-06 - Nuova segnalazione (I04)
'''''          '''''''''''''''''''''''''''''''''''''''
'''''          AggiungiSegnalazione TabIstr.BlkIstr(k).KeyDef(y).Key, "SSAKEYNOTFOUND", TabIstr.BlkIstr(k).KeyDef(y).Key
'''''        End If
'''''
'''''        'SQ: spostata qui... era fuori ciclo e settava solo la chiave 1!?
'''''        TabIstr.BlkIstr(k).KeyDef(y).xName = GbXName
'''''
'''''        Set tb1 = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Key")
'''''        tb1.AddNew
'''''        tb1!idOggetto = GbIdOggetto
'''''        tb1!riga = GbOldNumRec
'''''        tb1!livello = k
'''''        tb1!progressivo = y
'''''
'''''        tb1!idField = idField
'''''        tb1!idField_moved = TabIstr.BlkIstr(k).KeyDef(y).Key_moved
'''''        'SQ: aspettare il via di Ste per mettere true se idField negativo.
'''''        tb1!xName = TabIstr.BlkIstr(k).KeyDef(y).xName
'''''
'''''        tb1!operatore = TabIstr.BlkIstr(k).KeyDef(y).Op 'multiplo
'''''        tb1!operatore_moved = TabIstr.BlkIstr(k).KeyDef(y).Op_moved
'''''
'''''        'SQ: perche' se e' = ""??????????
'''''        If TabIstr.BlkIstr(k).KeyDef(y).Key <> "Nothing" Then
'''''          tb1!Valore = TabIstr.BlkIstr(k).KeyDef(y).KeyVal & ""
'''''          tb1!KeyValore = Left(TabIstr.BlkIstr(k).KeyDef(y).KeyVarField, 255)
'''''          'SQ: aggiungere colonna in DB!
'''''          tb1!KeyStart = TabIstr.BlkIstr(k).KeyDef(y).KeyStart
'''''        End If
'''''
'''''        tb1!KeyLen = TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ""
'''''
'''''        'SG : Se la lunghezza non � stata calcolata o calcolata male recupera l'effettiva lunghezza dal field dli
'''''        If Val(tb1!KeyLen) <= 0 Or Trim(tb1!KeyLen) = "" Then
'''''           tb1!KeyLen = Restituisci_DatiKey(tb1!idField, tb1!xName, "LUNGHEZZA")
'''''           TabIstr.BlkIstr(k).KeyDef(y).KeyLen = tb1!KeyLen
'''''
'''''           If Len(TabIstr.BlkIstr(k).SSAName) Then
'''''              TabIstr.BlkIstr(k).KeyDef(y).KeyVarField = TabIstr.BlkIstr(k).SSAName & "(" & TabIstr.BlkIstr(k).KeyDef(y).KeyStart & ":" & TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ")"
'''''              tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
'''''           End If
'''''        End If
'''''
'''''        If Len(TabIstr.BlkIstr(k).KeyDef(y).KeyBool) = 0 Then
'''''          tb1!Booleano = " "
'''''        Else
'''''          tb1!Booleano = TabIstr.BlkIstr(k).KeyDef(y).KeyBool
'''''        End If
'''''        tb1!booleano_moved = TabIstr.BlkIstr(k).KeyDef(y).KeyBool_moved
'''''        'SQ:
'''''        tb1!valida = TabIstr.BlkIstr(k).valida
'''''        tb1!Correzione = False
'''''        tb1.Update
'''''        tb1.Close
'''''    Next y
'''''
'''''    Dim wSsaPres As Boolean
'''''    '''''''''''''''wSsaPres = Trim(TabIstr.BlkIstr(k).SSAName) <> "<NONE>"
'''''
'''''    'Ancora il SEGMENTO????
'''''    'If Not isGSAM Then
'''''    '    If TabIstr.BlkIstr(k).IdSegm <= 0 Then TabIstr.BlkIstr(k).Valida = False
'''''    '    If Trim(TabIstr.BlkIstr(k).segment) = "" Then TabIstr.BlkIstr(k).Valida = False
'''''    'End If
'''''
'''''
'''''    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''    ' PsDli_istrDett_Dup: COS'E' 'STA ROBA?????
'''''    ' A COSA SERVE LA gSSA?????????????????????
'''''    ' Per la molteplicit�?????????????????????? (forse)
'''''    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''    Dim tbr As Recordset
'''''    Set tbr = m_Fun.Open_Recordset("Select * from PsDli_istrDett_Dup")
'''''    T4 = 0
'''''    wIdStr = ""
'''''
'''''    For T1 = 1 To UBound(gSSA)
'''''   '   For t2 = 1 To UBound(wDliSsa)
'''''       If k <= UBound(wDliSsa) Then
'''''         If gSSA(T1).Name = wDliSsa(k) Then
'''''            If UBound(gSSA(T1).Segm) > 1 Then
'''''               T4 = 0
'''''               For T3 = 1 To UBound(gSSA(T1).Segm)
'''''                 T4 = T4 + 1
'''''                 tbr.AddNew
'''''                    tbr!idOggetto = GbIdOggetto
'''''                    tbr!riga = GbOldNumRec
'''''                    tbr!livello = k
'''''                    tbr!numeropar = T4
'''''                    tbr!Tipo = "SG"
'''''                    tbr!Valore = gSSA(T1).Segm(T3)
'''''                 tbr.Update
'''''               Next T3
'''''            End If
'''''            If UBound(gSSA(T1).FIELD) > 1 Then
'''''               T4 = 0
'''''               For T3 = 1 To UBound(gSSA(T1).FIELD)
'''''                  T4 = T4 + 1
'''''                  tbr.AddNew
'''''                     tbr!idOggetto = GbIdOggetto
'''''                     tbr!riga = GbOldNumRec
'''''                     tbr!livello = k
'''''                     tbr!numeropar = T4
'''''                     tbr!Tipo = "FL"
'''''                     tbr!Valore = gSSA(T1).FIELD(T3)
'''''                  tbr.Update
'''''               Next T3
'''''            End If
'''''            If UBound(gSSA(T1).Op) > 1 Then
'''''               T4 = 0
'''''               For T3 = 1 To UBound(gSSA(T1).Op)
'''''                  T4 = T4 + 1
'''''                  tbr.AddNew
'''''                     tbr!idOggetto = GbIdOggetto
'''''                     tbr!riga = GbOldNumRec
'''''                     tbr!livello = k
'''''                     tbr!numeropar = T4
'''''                     tbr!Tipo = "OP"
'''''                     tbr!Valore = gSSA(T1).Op(T3)
'''''                  tbr.Update
'''''               Next T3
'''''            End If
'''''            If UBound(gSSA(T1).value) > 1 Then
'''''               T4 = 0
'''''               For T3 = 1 To UBound(gSSA(T1).value)
'''''                  T4 = T4 + 1
'''''                  tbr.AddNew
'''''                     tbr!idOggetto = GbIdOggetto
'''''                     tbr!riga = GbOldNumRec
'''''                     tbr!livello = k
'''''                     tbr!numeropar = T4
'''''                     tbr!Tipo = "VL"
'''''                     tbr!Valore = gSSA(T1).value(T3)
'''''                  tbr.Update
'''''               Next T3
'''''            End If
'''''            If UBound(gSSA(T1).CodCom) > 1 Then
'''''               T4 = 0
'''''               For T3 = 1 To UBound(gSSA(T1).CodCom)
'''''                  T4 = T4 + 1
'''''                  tbr.AddNew
'''''                     tbr!idOggetto = GbIdOggetto
'''''                     tbr!riga = GbOldNumRec
'''''                     tbr!livello = k
'''''                     tbr!numeropar = T4
'''''                     tbr!Tipo = "CC"
'''''                     tbr!Valore = DecodificaCodCom(gSSA(T1).CodCom(T3))
'''''                  tbr.Update
'''''               Next T3
'''''            End If
'''''            If UBound(gSSA(T1).Parentesi) > 1 Then
'''''               T4 = 0
'''''               wIdStr = getIdArea(gSSA(1).Name)
'''''               For T3 = 1 To UBound(gSSA(T1).Parentesi)
'''''                  T4 = T4 + 1
'''''                  If T4 > 2 Then
'''''                     T4 = T4
'''''                  End If
'''''                  tbr.AddNew
'''''                     tbr!idOggetto = GbIdOggetto
'''''                     tbr!riga = GbOldNumRec
'''''                     tbr!livello = k
'''''                     tbr!numeropar = T4
'''''                     tbr!Tipo = "PA"
'''''                     tbr!Valore = gSSA(T1).Parentesi(T3)
'''''                  tbr.Update
'''''               Next T3
'''''            End If
'''''         End If
'''''       End If
'''''  '    Next t2
'''''    Next T1
'''''    If Not TabIstr.BlkIstr(k).valida Then IsValid = False
'''''    tb.Update
'''''  Next k 'ITERAZIONE LIVELLI...
'''''  tb.Close
'''''
'''''  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''  ' GESTIONE SEGMENTI/AREE: ANCORA?!
'''''  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''  'Attenzione: wIdStr NON e' quello usato sopra per i singoli livelli... e' un'altra cosa...
'''''  'SQ-2: If InStr(wIdStr, "/") And wIdSegm > 0 Then
'''''  If InStr(wIdStr, "/") Then
'''''      wStrIdOggetto = Val(Left(wIdStr, 6))
'''''      wStrIdArea = Val(Mid$(wIdStr, 8, 4))
'''''      For y = 0 To UBound(IdSegmenti)
'''''        If Len(Trim(IdSegmenti(y))) Then
'''''          Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & IdSegmenti(y) _
'''''                & " and stridoggetto = " & wStrIdOggetto _
'''''                & " and stridarea = " & wStrIdArea _
'''''                & " and nomecmp = '" & TabIstr.BlkIstr(1).Record & "'")
'''''          If tb2.RecordCount = 0 Then
'''''            tb2.AddNew
'''''            tb2!IdSegmento = IdSegmenti(y)
'''''            tb2!stridoggetto = wStrIdOggetto
'''''            tb2!strIdArea = wStrIdArea
'''''            tb2!NomeCmp = TabIstr.BlkIstr(1).Record
'''''            tb2.Update
'''''            tb2.Close
'''''          End If
'''''        End If
'''''     Next y
'''''    End If
'''''  End If
'''''
'''''  ''''''''''''''''''''''''''''''''''''''''''''
'''''  ' UPDATE "PsDli_Istruzioni":
'''''  ''''''''''''''''''''''''''''''''''''''''''''
'''''  UpdatePsdliIstruzioni (False)
'''''
'''''  'SQ ??????????????????
'''''  Dim ssaPresent As Boolean
'''''  'ssaPresent = True
'''''  'SG: Controlla se c'� un livello di ssa
'''''  '...
'''''
'''''
'''''  ''''''''''''''''''''''''''''''''''''''''
'''''  ' Segnalazioni
'''''  ''''''''''''''''''''''''''''''''''''''''
'''''  'Portare nelle rispettive posizioni...!
'''''  If Not IsValid And ssaOk Then
'''''    If Len(TabIstr.DBD) And wIdDBD(0) = 0 Then  'NO DBD
'''''      AggiungiSegnalazione TabIstr.DBD, "NODBD", TabIstr.Istr
'''''    'ElseIf ubound(wIdDBD) Then   'DBD MULTIPLI
'''''    '  AggiungiSegnalazione dbdName, "DBDS", istruzione
'''''    ElseIf Trim(SwSegm) <> "" Then  'SQ ?
'''''      AggiungiSegnalazione SwSegm, "NOSEGM", TabIstr.Istr
'''''    Else
'''''     'Errore Generico (SEGMENTO!)
'''''     AggiungiSegnalazione "", "INVALIDDLIISTR", ""
'''''    End If
'''''  End If
  Exit Sub
parseErr:
  addSegnalazione parametri, "I00", parametri
End Sub

Private Sub getSegment(istruzione As String, parametri As String)
  Dim rs As Recordset
  Dim token As String, dbdName As String, wNomePsb As String, ssaValue As String
  Dim pcbNumber As Integer
  
  On Error GoTo parseErr


  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  'Pcb
  token = nextToken_tmp(parametri)
  If token = "C" Then
    TabIstr.pcb = nextToken_tmp(parametri)
  Else
    MsgBox "PCB???" & token
  End If
  
  'tmp
  Dim wDliRec As String
  wDliRec = nexttoken(parametri)
  
  '''''''''''''''''''''
  ' PCB (number)
  '''''''''''''''''''''
  TabIstr.numpcb = getPCBnumber(TabIstr.pcb)
  
  IsDC = False
  
  If IsDC Then
    parseRECEIVE istruzione, wDliRec
    Exit Sub
  End If


  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Gestione SSA
  ' Valorizzo la TabIstrSSa
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim ssa
  ReDim wDliSsa(0)
  ReDim gSSA(0)
  ReDim TabIstrSSa(0)
  ReDim TabIstrSSa(0).NomeSegmento.value(0)
  Dim ssaOk As Boolean
  ssaOk = True
  Dim i As Integer
  While Len(parametri)
    i = i + 1
    ReDim Preserve wDliSsa(UBound(wDliSsa) + 1)
    ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
    ReDim Preserve TabIstrSSa(UBound(gSSA) + 1)
    wDliSsa(UBound(wDliSsa)) = nexttoken(parametri)

    ''''''''AGGIUSTARE... PER ORA NON SERVE!
    Dim rsArea As Recordset, rsLink As Recordset, rsCmp As Recordset
    Set rsArea = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & GbIdOggetto)
    If rsArea.RecordCount = 0 Then
      'Ricerca nelle COPY:
      Set rsLink = m_Fun.Open_Recordset("Select IdOggettoR From PsRel_Obj Where IdOggettoC = " & GbIdOggetto & " And Relazione = 'CPY'")
      Do While Not rsLink.EOF
        Set rsCmp = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & rsLink!idOggettoR)
        If rsCmp.RecordCount Then
           Set rsArea = rsCmp
           Exit Do
        End If
        rsLink.MoveNext
      Loop
      rsLink.Close
    End If

    'Valorizza TabIstrSSa:
    If rsArea.RecordCount Then
      'SQ - tmp: mettere in linea...
      'Non funge per indirizzo? (in testa?)
      ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
      TabIstrSSa(UBound(TabIstrSSa)).SSAName = wDliSsa(UBound(wDliSsa))
      TabIstrSSa(UBound(TabIstrSSa)).idOggetto = rsArea!idOggetto
      TabIstrSSa(UBound(TabIstrSSa)).idArea = rsArea!idArea
      '''''parseSSA TabIstrSSa(UBound(TabIstrSSa))
    Else
      ''''''''''''''''''''''''''''''''''''''''
      ' SQ - 19-04-06
      ' Nuova segnalazione (I02)
      ''''''''''''''''''''''''''''''''''''''''
      ''''''''AggiungiSegnalazione wDliSsa(UBound(wDliSsa)), "NOSSAVALUE", wDliSsa(UBound(wDliSsa))
      ssaOk = False
    End If
    rsArea.Close
    
    '''''''''''''''''''''''''''''''''''''''''''''
    ' SPECIFICO QUIK: TABELLA (tmp?) PsQUIK_Dati
    ' Ci serve solo il SEGMENTO, per il momento...
    '''''''''''''''''''''''''''''''''''''''''''''
    Set rsArea = m_Fun.Open_Recordset("Select posizione,valore,ordinale From PsQUIK_Dati Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & GbIdOggetto)
    Dim Ordinale As Integer
    If rsArea.RecordCount Then
          'Primo elemento: campo di gruppo
          If Len(rsArea!Valore & "") = 0 Then
            Ordinale = rsArea!Ordinale
            Set rsArea = m_Fun.Open_Recordset("Select posizione,valore From PsQUIK_Dati Where ordinale > " & Ordinale & " And IdOggetto = " & GbIdOggetto & " order by ordinale")
            If rsArea.RecordCount = 0 Then
              MsgBox "ssa??? - Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & GbIdOggetto
            End If
          End If
          'Secondo elemento: inizio SSA... (ovviamente fa schifo, ma per ora � anche troppo!)
          TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.Name = Trim(Left(rsArea!Valore, 8))
          ReDim TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0)
          ReDim TabIstrSSa(UBound(TabIstrSSa)).CommandCode.value(0)
          ReDim TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0)
          ReDim TabIstrSSa(UBound(TabIstrSSa)).CommandCode.value(0)
    Else
      MsgBox "ssa??? - Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & GbIdOggetto
    End If
  Wend
  
  If UBound(TabIstrSSa) Then
    getTabIstr
  End If
  ''''''''''''''''''''''''''''''''
  ' Gestione PSB/DBD:
  ' Input: N PSB, PCB, M SEGMENTI
  ''''''''''''''''''''''''''''''''
  getInstructionPsbDbd (TabIstr.numpcb)
  
  'getTabIstr
  For i = 1 To UBound(TabIstr.BlkIstr)
    'SQ 9-05-06
    'If Len(TabIstr.BlkIstr(i).segment) Then
    '  getSegmentsId TabIstr.BlkIstr(i).segment
    'Else
    '  ReDim IdSegmenti(0)
    'End If
    'Valorizza IdSegmenti (tutti N segmenti: per ora non usato) e
    'le info relative al segmento del TabIstr.BlkIstr(i)
    getSegmentsId TabIstr.BlkIstr(i)
    
    ''''''''''''''''''''''''''''
    ' Ne trovo N e ne uso 1...
    ''''''''''''''''''''''''''''
    If Len(IdSegmenti(0)) Then ' � stringa!
      TabIstr.BlkIstr(i).idSegm = IdSegmenti(0)
    Else
      TabIstr.BlkIstr(i).SegLen = 0
      '''''''''''''''''''''''''''''''''''''
      ' Segnalazione
      '''''''''''''''''''''''''''''''''''''
    End If
    ''''''''''''''''''''''''''''''''''
    ' Nome Routine?!
    ''''''''''''''''''''''''''''''''''
    '''TabIstr.BlkIstr(i).NomeRout = CreaNomeRout(TabIstr.DBD)
  Next
  
  Exit Sub
parseErr:
  'tmp
  MsgBox err.description
End Sub
'3-04-06 Modificato per gli Alternate PCB
'Effetto Collaterale: setta isDC
Private Function getPCBnumber(pcbField As String) As Integer
  Dim pcb As Integer
  Dim rsPsb As Recordset
    
  '''''''''''''''''''''''''''''''''''''''''
  ' Numero PCB
  '''''''''''''''''''''''''''''''''''''''''
  pcb = CInt(pcbField)
  
  '''''''''''''''''''''''''''''''''''''''''
  ' isDC
  '''''''''''''''''''''''''''''''''''''''''
  IsDC = False
  If SwTpIMS Then
    If pcb = 0 Then
      'PCB-IO ==> IMS/DC
      IsDC = True
    Else
      'PCB-ALTERNATE ==> IMS/DC
      If pcb < (UBound(gbCurEntryPCB) + 1) Then
        Set rsPsb = m_Fun.Open_Recordset("Select TypePCB from PsDLI_Psb where IdOggetto = " & IdPsb & " and numPCB = " & getPCBnumber)
        If Not rsPsb.EOF Then
          If Trim(rsPsb!TypePcb) = "TP" Then
            IsDC = True
          End If
        End If
        rsPsb.Close
      End If
    End If
  End If
  
  getPCBnumber = pcb
  
End Function
