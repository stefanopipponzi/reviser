Attribute VB_Name = "MapsdM_VSM"
Option Explicit

Dim numLinee As Long

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Parsing di II livello
' Analizza le istruzioni IMS che trova in PsDli_Istruzioni
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseVSM_II(pIdOggetto As Long, isBatch As Boolean)
  Dim istruzione As String, parametri As String
  Dim rs As Recordset
  Dim rsCall As Recordset
  Dim OptWhereCond As String

  On Error GoTo parseErr
    
  ReDim idAree(0)
  ReDim ssaCache(0)
  ReDim wPcb(0)
  ReDim xPcb(0)
  ReDim fSegm(0)
  'SQ 28-02-07
  ReDim DataValues(0)
  'SQ 9-11-06
  ReDim CampiMove(0)
  GbIdOggetto = pIdOggetto
 
  'SQ CPY-ISTR
  If isBatch Then
    Set rs = m_Fun.Open_Recordset("Select * from PsIO_Batch where idOggetto=" & GbIdOggetto)
  Else 'CICS
     Set rs = m_Fun.Open_Recordset("Select * from PSExec where IdOggetto=" & GbIdOggetto)
   End If
  
  While Not rs.EOF
    istruzione = IIf(IsNull(rs!Comando), "", rs!Comando)
    parametri = rs!istruzione
    GbOldNumRec = rs!Riga
    'SQ CPY-ISTR
    IdOggettoRel = rs!idOggetto
    numLinee = rs!linee
    If isBatch Then
      Select Case istruzione
        Case "READ", "READNEXT"
          parseREAD_VSM istruzione, parametri
        Case "WRITE", "REWRITE"
          parseWRITE_VSM istruzione, parametri
        Case "DELETE"
          parseDELETE_VSM istruzione, parametri
        Case "START"
          parseSTART_VSM istruzione, parametri
        Case "OPEN"
          parseOPEN_VSM istruzione, parametri
        Case "CLOSE"
          parseCLOSE_VSM istruzione, parametri
        Case "START"
          MapsdM_IMS.parseCHKP parametri
        Case Else
          'gestire...
      End Select
    Else ' comandi CICS
      Select Case istruzione
        Case "READ", "READNEXT", "READPREV"
          If rs!isExec Then
            parseGU_exec istruzione, parametri
          Else
            parseGU_call istruzione, parametri
          End If
        Case "WRITE", "REWRITE"
          If rs!isExec Then
            parseREPL_exec istruzione, parametri
          Else
            parseREPL_call istruzione, parametri
          End If
        Case "STARTBR"
          If rs!isExec Then
            parseISRT_exec istruzione, parametri
          Else
            parseISRT_call istruzione, parametri
          End If
        Case "ENDBR"
          MapsdM_IMS.parseCHKP parametri
        Case "DELETE"
          parseAUTH parametri
        Case Else
          'gestire...
      End Select
    End If ' batch o cics
    rs.MoveNext
  Wend
  rs.Close
  
  
  ''''''''''''''''''''''''''''''''''''''''''''
  ' Update Bs_oggetti: data,livello,errorLevel
  ''''''''''''''''''''''''''''''''''''''''''''
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  TbOggetti!DtParsing = Now
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti!parsinglevel = 2
  TbOggetti.Update
  TbOggetti.Close



  'SQ - CPY-INSTR chiarire questa qui sotto:
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Aggiornamento DBD in PsRel_Obj e SEGM in PsComObj (!?)
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  AggiornaRelIstrDli GbIdOggetto
  
  
  Exit Sub
parseErr:
  'gestire meglio...
  If err.Number = -2147467259 Or err.Number = 3704 Then  'tmp - problema Alby... prj Perot
    'm_Fun.WriteLog "parseCbl_II: id#" & GbIdOggetto & "#" & Error & "#" & Err.Description
    addSegnalazione GbNomePgm & ": nested PSB", "I00", GbNomePgm & ": nested PSB"
  Else
    'MsgBox "II level parsing: " & Err.Description
    addSegnalazione rs!statement, "I00", rs!statement
    'Resume
  End If
End Sub
Public Sub clearVSMtable(pIdOggetto As Long)
  Parser.PsConnection.Execute "delete * from PsVSM_Istruzioni where idOggetto = " & pIdOggetto
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 13-09-07
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseREAD_VSM(istruzione As String, parametri As String)
  Dim statement As String, token As String, fileName As String
  Dim atEnd As String, notAtEnd As String
  Dim invalidKey As String, notInvalidKey As String
  Dim area As String
  Dim keyField As String
  Dim endStatement As Boolean
  Dim k As Long
  
  statement = Replace(parametri, "@", "@ ")
  fileName = nexttoken(statement)
  
  fileName = checkChiocciola(fileName)
  
  While Len(statement) And Not endStatement
    token = nexttoken(statement)
    
    Select Case token
      Case "NEXT"
        token = checkChiocciola(token)
        istruzione = istruzione + " " + token
      Case "INTO"
        token = checkChiocciola(token)
        token = nexttoken(statement)
        token = checkChiocciola(token)
        area = token
      Case "KEY"
        token = nexttoken(statement)
        If (token = "IS") Then
          token = checkChiocciola(token)
          token = nexttoken(statement)
          token = checkChiocciola(token)
          keyField = token
        End If
      Case "AT"
        token = nexttoken(statement)
        If (token = "END") Or (token = "END@") Then
          token = checkChiocciola(token)
          k = InStr(statement, "NOT AT END")
          If (k = 0) Then
            atEnd = statement
            atEnd = Replace(atEnd, "@ ", "@")
            endStatement = True
          Else
            atEnd = Left(statement, k - 1)
            atEnd = Replace(atEnd, "@ ", "@")
            notAtEnd = Mid(statement, k + 11)
            notAtEnd = Replace(notAtEnd, "@ ", "@")
            endStatement = True
          End If
        End If
      Case "INVALID"
        token = nexttoken(statement)
        If (token = "KEY") Or (token = "KEY@") Then
            invalidKey = statement
            invalidKey = Replace(invalidKey, "@ ", "@")
            endStatement = True
        End If
      Case "NOT"
        If (nexttoken(statement) = "INVALID") Then
          token = nexttoken(statement)
          If (token = "KEY") Or (token = "KEY@") Then
            invalidKey = statement
            invalidKey = Replace(invalidKey, "@ ", "@")
            endStatement = True
          End If
        End If
    End Select
  
  Wend
  
  Dim rIstr As Recordset
  Set rIstr = m_Fun.Open_Recordset("select * from PSVSM_ISTRUZIONI")
  rIstr.AddNew
  rIstr!idOggetto = IdOggettoRel
  rIstr!Riga = GbOldNumRec
  rIstr!isBatch = True
  rIstr!istruzione = istruzione
  rIstr!atEnd = atEnd
  rIstr!notAtEnd = notAtEnd
  rIstr!invalid_Key = invalidKey
  rIstr!notInvalid_Key = notInvalidKey
  rIstr!linee = numLinee
  rIstr!relRow = 1
  rIstr!fileName = fileName
  rIstr!area = area
  rIstr!keyField = keyField
  rIstr.Update
  rIstr.Close
  
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 13-09-07
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseWRITE_VSM(istruzione As String, parametri As String)
  Dim statement As String, token As String, fileName As String
  Dim invalidKey As String, notInvalidKey As String
  Dim atEnd As String, notAtEnd As String
  Dim area As String
  Dim keyField As String
  Dim endStatement As Boolean
  Dim k As Long
  
  statement = Replace(parametri, "@", "@ ")
  fileName = nexttoken(statement)
  
  fileName = checkChiocciola(fileName)
  
  While Len(statement) And Not endStatement
    token = nexttoken(statement)
    
    Select Case token
      Case "FROM"
        token = checkChiocciola(token)
        token = nexttoken(statement)
        token = checkChiocciola(token)
        area = token
      Case "INVALID"
        token = nexttoken(statement)
        If (token = "KEY") Or (token = "KEY@") Then
            invalidKey = statement
            invalidKey = Replace(invalidKey, "@ ", "@")
            endStatement = True
        End If
      Case "NOT"
        If (nexttoken(statement) = "INVALID") Then
          token = nexttoken(statement)
          If (token = "KEY") Or (token = "KEY@") Then
            invalidKey = statement
            invalidKey = Replace(invalidKey, "@ ", "@")
            endStatement = True
          End If
        End If
    End Select
  Wend
  
  Dim rIstr As Recordset
  Set rIstr = m_Fun.Open_Recordset("select * from PSVSM_ISTRUZIONI")
  rIstr.AddNew
  rIstr!idOggetto = IdOggettoRel
  rIstr!Riga = GbOldNumRec
  rIstr!isBatch = True
  rIstr!istruzione = istruzione
  rIstr!atEnd = atEnd
  rIstr!notAtEnd = notAtEnd
  rIstr!invalid_Key = invalidKey
  rIstr!notInvalid_Key = notInvalidKey
  rIstr!linee = numLinee
  rIstr!fileName = fileName
  rIstr!relRow = 1
  rIstr!area = area
  rIstr!keyField = keyField
  rIstr.Update
  rIstr.Close
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 14-09-07
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseDELETE_VSM(istruzione As String, parametri As String)
  Dim statement As String, token As String, fileName As String
  Dim invalidKey As String, notInvalidKey As String
  Dim atEnd As String, notAtEnd As String
  Dim area As String
  Dim keyField As String
  Dim endStatement As Boolean
  Dim k As Long
  
  statement = Replace(parametri, "@", "@ ")
  fileName = nexttoken(statement)
  
  fileName = checkChiocciola(fileName)
  
  While Len(statement) And Not endStatement
    token = nexttoken(statement)
    
    Select Case token
      Case "INVALID"
        token = nexttoken(statement)
        If (token = "KEY") Or (token = "KEY@") Then
            invalidKey = statement
            invalidKey = Replace(invalidKey, "@ ", "@")
            endStatement = True
        End If
      Case "NOT"
        If (nexttoken(statement) = "INVALID") Then
          token = nexttoken(statement)
          If (token = "KEY") Or (token = "KEY@") Then
            invalidKey = statement
            invalidKey = Replace(invalidKey, "@ ", "@")
            endStatement = True
          End If
        End If
    End Select
  Wend
  
  Dim rIstr As Recordset
  Set rIstr = m_Fun.Open_Recordset("select * from PSVSM_ISTRUZIONI")
  rIstr.AddNew
  rIstr!idOggetto = IdOggettoRel
  rIstr!Riga = GbOldNumRec
  rIstr!isBatch = True
  rIstr!istruzione = istruzione
  rIstr!atEnd = atEnd
  rIstr!notAtEnd = notAtEnd
  rIstr!invalid_Key = invalidKey
  rIstr!notInvalid_Key = notInvalidKey
  rIstr!linee = numLinee
  rIstr!relRow = 1
  rIstr!fileName = fileName
  rIstr!area = area
  rIstr!keyField = keyField
  rIstr.Update
  rIstr.Close
End Sub

Sub parseSTART_VSM(istruzione As String, parametri As String)
  Dim statement As String, token As String, fileName As String, operator As String
  Dim invalidKey As String, notInvalidKey As String
  Dim atEnd As String, notAtEnd As String
  Dim area As String
  Dim keyField As String
  Dim endStatement As Boolean
  Dim k As Long
  
  statement = Replace(parametri, "@", "@ ")
  fileName = nexttoken(statement)
  
  fileName = checkChiocciola(fileName)
  
  While Len(statement) And Not endStatement
    token = nexttoken(statement)
    
    Select Case token
      Case "KEY"
        token = nexttoken(statement)
        
        If (token = "IS") Then
          token = nexttoken(statement)
        End If
        
        token = checkChiocciola(token)
        
        Select Case token
          Case "=", ">", ">=", "<", "<="
            operator = token
            token = nexttoken(statement)
          Case "EQUAL"
            token = checkChiocciola(token)
            operator = token
            token = nexttoken(statement)
            If (token = "TO") Or (token = "TO@") Then
              token = checkChiocciola(token)
              operator = operator & " " & token
              token = nexttoken(statement)
            End If
          Case "GREATER"
            operator = token
            token = nexttoken(statement)
            If (token = "THAN") Or (token = "THAN@") Then
              token = checkChiocciola(token)
              operator = operator & " " & token
              token = nexttoken(statement)
              If (token = "OR") Or (token = "OR@") Then
                token = checkChiocciola(token)
                operator = operator & " " & token
                token = nexttoken(statement)
                If (token = "EQUAL") Or (token = "EQUAL@") Then
                  token = checkChiocciola(token)
                  operator = operator & " " & token
                  token = nexttoken(statement)
                  If (token = "TO") Or (token = "TO@") Then
                    token = checkChiocciola(token)
                    operator = operator & " " & token
                    token = nexttoken(statement)
                  End If
                End If
              End If
            Else
              token = checkChiocciola(token)
              operator = operator & " " & token
              token = nexttoken(statement)
            End If
          Case "NOT"
            operator = operator & token
            token = nexttoken(statement)
            If (token = "LESS") Or (token = "LESS@") Then
              token = checkChiocciola(token)
              operator = operator & " " & token
              token = nexttoken(statement)
              If (token = "THAN") Then
                operator = operator & " " & token
                token = nexttoken(statement)
              End If
            ElseIf (token = "<") Then
              operator = operator & " " & token
              token = nexttoken(statement)
            End If
          End Select
'        ElseIf (token = "GREATER") Then
'          operator = operator & token
'          token = nextToken(statement)
'          If (token = "THAN") Then
'            operator = operator & " " & token
'          End If
'        End If
'        token = checkChiocciola(token)
'        token = nextToken(statement)
'        token = checkChiocciola(token)
'        keyField = token
        token = checkChiocciola(token)
        keyField = token
      Case "INVALID"
        token = nexttoken(statement)
        If (token = "KEY") Or (token = "KEY@") Then
          invalidKey = statement
          invalidKey = Replace(invalidKey, "@ ", "@")
          endStatement = True
        End If
      Case "NOT"
        If (nexttoken(statement) = "INVALID") Then
          If (nexttoken(statement) = "KEY") Then
            invalidKey = statement
            endStatement = True
          End If
        End If
    End Select
  Wend
    
  Dim rIstr As Recordset
  Set rIstr = m_Fun.Open_Recordset("select * from PSVSM_ISTRUZIONI")
  rIstr.AddNew
  rIstr!idOggetto = IdOggettoRel
  rIstr!Riga = GbOldNumRec
  rIstr!isBatch = True
  rIstr!istruzione = istruzione
  rIstr!atEnd = atEnd
  rIstr!notAtEnd = notAtEnd
  rIstr!invalid_Key = invalidKey
  rIstr!notInvalid_Key = notInvalidKey
  rIstr!linee = numLinee
  rIstr!fileName = fileName
  rIstr!area = area
  rIstr!relRow = 1
  rIstr!keyField = keyField
  rIstr!operator = operator
  rIstr.Update
  rIstr.Close
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 18-09-07
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseOPEN_VSM(istruzione As String, parametri As String)
  Dim statement As String, token As String, fileName As String, operator As String
  Dim invalidKey As String, notInvalidKey As String
  Dim area As String
  Dim keyField As String
  Dim endInput As Boolean, endOutput As Boolean
  Dim k As Long, relRow As Long
  
  relRow = 0
  
  statement = Replace(parametri, "@", "@ ")
  operator = nexttoken(statement)
  
  token = nexttoken(statement)
  
  If (operator = "INPUT") Or (operator = "INPUT@") Then
    
    While Not endInput
      operator = checkChiocciola(operator)
      fileName = checkChiocciola(token)
      
      relRow = relRow + 1
      
      Dim rIstrIn As Recordset
      Set rIstrIn = m_Fun.Open_Recordset("select * from PSVSM_ISTRUZIONI")
      rIstrIn.AddNew
      rIstrIn!idOggetto = IdOggettoRel
      rIstrIn!Riga = GbOldNumRec
      rIstrIn!relRow = relRow
      rIstrIn!isBatch = True
      rIstrIn!istruzione = istruzione
      rIstrIn!fileName = fileName
      rIstrIn!operator = operator
      rIstrIn!linee = 1
      rIstrIn.Update
      rIstrIn.Close
      
      token = nexttoken(statement)
      If Len(statement) Then
        If (token = "OUTPUT") Or (token = "OUTPUT@") Then
          endInput = True
          operator = token
        End If
      ElseIf (token = "") Then
        endInput = True
      End If
    Wend
  End If
  
  If (operator = "OUTPUT") Or (operator = "OUTPUT@") Then
    
    While Not endOutput
      operator = checkChiocciola(operator)
      fileName = checkChiocciola(token)
      
      relRow = relRow + 1
      
      Dim rIstrOut As Recordset
      Set rIstrOut = m_Fun.Open_Recordset("select * from PSVSM_ISTRUZIONI")
      rIstrOut.AddNew
      rIstrOut!idOggetto = IdOggettoRel
      rIstrOut!Riga = GbOldNumRec
      rIstrOut!relRow = relRow
      rIstrOut!isBatch = True
      rIstrOut!istruzione = istruzione
      rIstrOut!fileName = fileName
      rIstrOut!operator = operator
      rIstrIn!linee = 1
      rIstrOut.Update
      rIstrOut.Close
      
      token = nexttoken(statement)
      
      If Not Len(statement) Then
        endOutput = True
      End If
    Wend
  End If
  
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 18-09-07
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseCLOSE_VSM(istruzione As String, parametri As String)
  Dim statement As String, token As String, fileName As String, operator As String
  Dim invalidKey As String, notInvalidKey As String
  Dim area As String
  Dim keyField As String
  Dim endInput As Boolean, endOutput As Boolean
  Dim k As Long, relRow As Long
  
  relRow = 0
  
  statement = Replace(parametri, "@", "@ ")

  While Len(statement)
    fileName = nexttoken(statement)
    fileName = checkChiocciola(fileName)
    relRow = relRow + 1
    
    Dim rIstr As Recordset
    Set rIstr = m_Fun.Open_Recordset("select * from PSVSM_ISTRUZIONI")
    rIstr.AddNew
    rIstr!idOggetto = IdOggettoRel
    rIstr!Riga = GbOldNumRec
    rIstr!relRow = relRow
    rIstr!isBatch = True
    rIstr!istruzione = istruzione
    rIstr!fileName = fileName
    rIstr!operator = operator
    rIstr!linee = numLinee
    rIstr.Update
    rIstr.Close
  Wend

End Sub
Private Function checkChiocciola(parametri As String) As String

  If (Right(parametri, 1) = "@") Then
    checkChiocciola = Replace(parametri, "@", "")
  Else
    checkChiocciola = parametri
  End If

End Function


