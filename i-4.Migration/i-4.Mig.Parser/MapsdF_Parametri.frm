VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form MapsdF_Parametri 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Parser Config"
   ClientHeight    =   6105
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7830
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6105
   ScaleWidth      =   7830
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   7830
      _ExtentX        =   13811
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Save"
            Object.ToolTipText     =   "Save"
            ImageKey        =   "Save"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Annull"
            Object.ToolTipText     =   "Cancel"
            ImageKey        =   "Delete"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Help"
            Object.ToolTipText     =   "Help"
            ImageKey        =   "Help"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5625
      Left            =   30
      TabIndex        =   0
      Top             =   450
      Width           =   5145
      _ExtentX        =   9075
      _ExtentY        =   9922
      _Version        =   393216
      TabOrientation  =   2
      Style           =   1
      Tabs            =   6
      Tab             =   2
      TabsPerRow      =   6
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Relations"
      TabPicture(0)   =   "MapsdF_Parametri.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "RelTv"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "IMS / DB"
      TabPicture(1)   =   "MapsdF_Parametri.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "IMS / DC"
      TabPicture(2)   =   "MapsdF_Parametri.frx":0038
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Frame3"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "RDBMS"
      TabPicture(3)   =   "MapsdF_Parametri.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
      TabCaption(4)   =   "Repository"
      TabPicture(4)   =   "MapsdF_Parametri.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).ControlCount=   0
      TabCaption(5)   =   "Tab 5"
      TabPicture(5)   =   "MapsdF_Parametri.frx":008C
      Tab(5).ControlEnabled=   0   'False
      Tab(5).ControlCount=   0
      Begin VB.Frame Frame3 
         Caption         =   "Standard PCB for IMS / DC"
         Height          =   2955
         Left            =   570
         TabIndex        =   7
         Top             =   210
         Width           =   3015
         Begin VB.Frame Frame6 
            Caption         =   "I/O Master PCB Name"
            Height          =   615
            Left            =   510
            TabIndex        =   13
            Top             =   2220
            Width           =   2205
            Begin VB.TextBox ImsStdMstPcb 
               Height          =   285
               Left            =   120
               TabIndex        =   14
               Top             =   240
               Width           =   1995
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "I/O Logical PCB Name"
            Height          =   615
            Left            =   510
            TabIndex        =   11
            Top             =   1560
            Width           =   2205
            Begin VB.TextBox ImsStdIoPcb 
               Height          =   285
               Left            =   90
               TabIndex        =   12
               Top             =   210
               Width           =   1995
            End
         End
         Begin VB.CheckBox ChkImsStdPcb 
            Caption         =   "Utilizzo di PCB standard nelle istruzioni  IMS ?"
            Height          =   495
            Left            =   210
            TabIndex        =   10
            Top             =   270
            Width           =   2745
         End
         Begin VB.Frame Frame4 
            Caption         =   "Alternate PCB Name"
            Height          =   615
            Left            =   510
            TabIndex        =   8
            Top             =   900
            Width           =   2205
            Begin VB.TextBox ImsStdAltPcb 
               Height          =   285
               Left            =   90
               TabIndex        =   9
               Top             =   210
               Width           =   1995
            End
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Standard PSB for IMS / DB"
         Height          =   1635
         Left            =   -74520
         TabIndex        =   3
         Top             =   120
         Width           =   2865
         Begin VB.Frame Frame2 
            Caption         =   "PSB Name"
            Height          =   615
            Left            =   510
            TabIndex        =   5
            Top             =   900
            Width           =   2205
            Begin VB.TextBox DLIStdPsbName 
               Height          =   285
               Left            =   90
               TabIndex        =   6
               Top             =   210
               Width           =   1995
            End
         End
         Begin VB.CheckBox ChkDliStdPsb 
            Caption         =   "Utilizzo di un PSB standard per le istruzioni  DLI ?"
            Height          =   495
            Left            =   270
            TabIndex        =   4
            Top             =   330
            Width           =   2385
         End
      End
      Begin MSComctlLib.TreeView RelTv 
         Height          =   5055
         Left            =   -74550
         TabIndex        =   2
         Top             =   120
         Width           =   3225
         _ExtentX        =   5689
         _ExtentY        =   8916
         _Version        =   393217
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   3210
      Top             =   2085
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MapsdF_Parametri.frx":00A8
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MapsdF_Parametri.frx":01BA
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MapsdF_Parametri.frx":02CC
            Key             =   "Help"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MapsdF_Parametri"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim SwCheck As Boolean

Private Sub ChkDliStdPsb_Click()
   If ChkDliStdPsb.Value = vbChecked Then
      Frame2.Visible = True
   Else
      Frame2.Visible = False
   End If

End Sub

Private Sub ChkImsStdPcb_Click()
   If ChkImsStdPcb.Value = vbChecked Then
      Frame4.Visible = True
      Frame5.Visible = True
      Frame6.Visible = True
   Else
      Frame4.Visible = False
      Frame5.Visible = False
      Frame6.Visible = False
   End If
End Sub

Private Sub RelTv_NodeCheck(ByVal Node As MSComctlLib.Node)
     Dim k As Integer
     Dim wKey As String
     Dim SwNck As Boolean
     
     If SwCheck Then Exit Sub
     SwCheck = True
     
     If Len(Node.key) = 2 Then
           wKey = Node.key
           SwNck = Node.Checked
           For k = 1 To RelTv.Nodes.Count
              If Len(RelTv.Nodes(k).key) > 2 Then
                 If RelTv.Nodes(k).Parent.key = wKey Then
                     RelTv.Nodes(k).Checked = SwNck
                 End If
              End If
           Next k
     End If
     
     SwCheck = False

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    On Error Resume Next
    Select Case Button.key
        Case "Save"
            AggiornaParametri
        Case "Annull"
            'Da fare: Aggiunge il codice per il pulsante 'Annull'.
            MsgBox "Aggiunge il codice per il pulsante 'Annull'."
        Case "Help"
            'Da fare: Aggiunge il codice per il pulsante 'Help'.
            MsgBox "Aggiunge il codice per il pulsante 'Help'."
    End Select
End Sub


Public Sub Resize()
  On Error Resume Next
  
  Me.Move Parser.PsLeft, Parser.PsTop, Parser.PsWidth, Parser.PsHeight
  SSTab1.Tab = 0
  SSTab1.Move 30, 450, (Me.ScaleWidth / 2) - 60, Me.ScaleHeight - 510
  SSTab1.Tab = 0
 RelTv.Move 390, 60, SSTab1.Width - 450, SSTab1.Height - 120
End Sub

Private Sub Form_Load()
  SSTab1.TabVisible(5) = False
  Resize
  ApriProgetto
  InitRelTv
End Sub

Private Sub InitRelTv()

  Dim k As Integer
  
  RelTv.Nodes.Clear
  
  RelTv.Nodes.Add , , "CB", "Cobol - Source and Copies"
     With RelTv.Nodes.Add("CB", tvwChild, "CB-COPY", "Copy")
       .Checked = GbParmRel.CB_Copy
     End With
     With RelTv.Nodes.Add("CB", tvwChild, "CB-CALLSTAT", "Call Static ")
       .Checked = GbParmRel.CB_CallStat
     End With
     With RelTv.Nodes.Add("CB", tvwChild, "CB-CALLDYN", "Call Dynamic")
       .Checked = GbParmRel.CB_CallDyn
     End With
     With RelTv.Nodes.Add("CB", tvwChild, "CB-LINKAGE", "Linkage Areas")
       .Checked = GbParmRel.CB_Linkage
     End With
 
  RelTv.Nodes.Add , , "AS", "Assembler - Source and Macro"
     With RelTv.Nodes.Add("AS", tvwChild, "AS-MACRO", "Macro")
       .Checked = GbParmRel.AS_Macro
     End With
     
  RelTv.Nodes.Add , , "BM", "BMS"
     With RelTv.Nodes.Add("BM", tvwChild, "BM-MAPSET", "Map and Mapset")
       .Checked = GbParmRel.BM_Mapset
     End With
     With RelTv.Nodes.Add("BM", tvwChild, "BM-COPY", "Map and Copy")
       .Checked = GbParmRel.BM_Copy
     End With
     
  RelTv.Nodes.Add , , "MF", "MFS"
     With RelTv.Nodes.Add("MF", tvwChild, "MF-MAPSET", "Map and MapArea-IN & MapArea-OUT")
       .Checked = GbParmRel.MF_Mapset
     End With
     With RelTv.Nodes.Add("MF", tvwChild, "MF-COPY", "Map and Copy")
       .Checked = GbParmRel.MF_Copy
     End With
     
  RelTv.Nodes.Add , , "JC", "Jcl"
     With RelTv.Nodes.Add("JC", tvwChild, "JC-PROC", "Call Procedures")
       .Checked = GbParmRel.JC_Proc
     End With
     With RelTv.Nodes.Add("JC", tvwChild, "JC-PROG", "Call Programs")
       .Checked = GbParmRel.JC_Prog
     End With
     With RelTv.Nodes.Add("JC", tvwChild, "JC-UTILITY", "Call Utility")
       .Checked = GbParmRel.JC_Utility
     End With
     With RelTv.Nodes.Add("JC", tvwChild, "JC-DLBL", "Define DD or DLBL")
       .Checked = GbParmRel.JC_Dlbl
     End With
     
  RelTv.Nodes.Add , , "CX", "CICS"
     With RelTv.Nodes.Add("CX", tvwChild, "CX-LINK", "LINK Program")
       .Checked = GbParmRel.CX_Link
     End With
     With RelTv.Nodes.Add("CX", tvwChild, "CX-XCTL", "XCTL Program")
       .Checked = GbParmRel.CX_XCTL
     End With
     With RelTv.Nodes.Add("CX", tvwChild, "CX-START-TR", "START Transid")
       .Checked = GbParmRel.CX_Start_TR
     End With
     With RelTv.Nodes.Add("CX", tvwChild, "CX-RETURN-TR", "RETURN Transid")
       .Checked = GbParmRel.CX_Return_TR
     End With
     With RelTv.Nodes.Add("CX", tvwChild, "CX-SEND", "SEND & RECEIVE MAP")
       .Checked = GbParmRel.CX_Send
     End With
     With RelTv.Nodes.Add("CX", tvwChild, "CX-COMMAREA", "Commarea")
       .Checked = GbParmRel.CX_Commarea
     End With
  
  RelTv.Nodes.Add , , "IM", "IMS / DC"
     With RelTv.Nodes.Add("IM", tvwChild, "IM-CALL", "Call Program")
       .Checked = GbParmRel.IMS_Call_Pgm
     End With
     With RelTv.Nodes.Add("IM", tvwChild, "IM-START-TR", "Call Transid")
       .Checked = GbParmRel.IMS_Call_TR
     End With
     With RelTv.Nodes.Add("IM", tvwChild, "IM-TRAN", "Program Transid")
       .Checked = GbParmRel.IMS_Transid
     End With
     With RelTv.Nodes.Add("IM", tvwChild, "IM-SEND", "PUT & GET MAP")
       .Checked = GbParmRel.IMS_Send
     End With
     With RelTv.Nodes.Add("IM", tvwChild, "IM-SPAAREA", "SPA Area")
       .Checked = GbParmRel.IMS_SPAarea
     End With
  
  RelTv.Nodes.Add , , "VS", "VSAM"
     With RelTv.Nodes.Add("VS", tvwChild, "VS-DECFILE", "Program & VSAM: Declare of Files")
       .Checked = GbParmRel.VS_DecFile
     End With
     With RelTv.Nodes.Add("VS", tvwChild, "VS-USEFILE", "Program & VSAM: Use of Files")
       .Checked = GbParmRel.VS_UseFile
     End With
     
  RelTv.Nodes.Add , , "RD", "RDBMS"
     With RelTv.Nodes.Add("RD", tvwChild, "RD-DECTABLE", "Program & RDBMS: Declare of Tables")
       .Checked = GbParmRel.RD_DecTable
     End With
     With RelTv.Nodes.Add("RD", tvwChild, "RD-USETABLE", "Program & RDBMS: Use of Tables")
       .Checked = GbParmRel.RD_UseTable
     End With
     
  RelTv.Nodes.Add , , "DL", "IMS / DB"
     With RelTv.Nodes.Add("DL", tvwChild, "DL-PGM-PSB", "Program & DLI: Use of PSBs and PCBs")
       .Checked = GbParmRel.DL_Pgm_Psb
     End With
     With RelTv.Nodes.Add("DL", tvwChild, "DL-DECDLI", "Program & DLI: Declare of DBs")
       .Checked = GbParmRel.DL_DecDli
     End With
     With RelTv.Nodes.Add("DL", tvwChild, "DL-USEDLI", "Program & DLI: Use of DBs, Segments and Fields")
       .Checked = GbParmRel.DL_UseDli
     End With
If GbParmRel.DliStdPsb Then
    ChkDliStdPsb.Value = vbChecked
Else
    ChkDliStdPsb.Value = vbUnchecked
End If
If GbParmRel.ImsStdPcb Then
     ChkImsStdPcb = vbChecked
Else
     ChkImsStdPcb = vbUnchecked
End If
DLIStdPsbName = GbParmRel.DLIStdPsbName
ImsStdAltPcb = GbParmRel.ImsStdAltPcbName
ImsStdIoPcb = GbParmRel.ImsStdIoPcbName
ImsStdMstPcb = GbParmRel.ImsStdMstPcbName

End Sub

Sub AggiornaParametri()

  GbParmRel.AS_Macro = RelTv.Nodes("AS-MACRO").Checked
  
  GbParmRel.CB_Copy = RelTv.Nodes("CB-COPY").Checked
  GbParmRel.CB_CallStat = RelTv.Nodes("CB-CALLSTAT").Checked
  GbParmRel.CB_CallDyn = RelTv.Nodes("CB-CALLDYN").Checked
  GbParmRel.CB_Linkage = RelTv.Nodes("CB-LINKAGE").Checked
  
  GbParmRel.BM_Copy = RelTv.Nodes("BM-COPY").Checked
  GbParmRel.BM_Mapset = RelTv.Nodes("BM-MAPSET").Checked
  
  GbParmRel.MF_Copy = RelTv.Nodes("MF-COPY").Checked
  GbParmRel.MF_Mapset = RelTv.Nodes("MF-MAPSET").Checked
  
  GbParmRel.JC_Dlbl = RelTv.Nodes("JC-DLBL").Checked
  GbParmRel.JC_Proc = RelTv.Nodes("JC-PROC").Checked
  GbParmRel.JC_Prog = RelTv.Nodes("JC-PROG").Checked
  GbParmRel.JC_Utility = RelTv.Nodes("JC-UTILITY").Checked
  
  GbParmRel.CX_Commarea = RelTv.Nodes("CX-COMMAREA").Checked
  GbParmRel.CX_Link = RelTv.Nodes("CX-LINK").Checked
  GbParmRel.CX_Return_TR = RelTv.Nodes("CX-RETURN-TR").Checked
  GbParmRel.CX_Send = RelTv.Nodes("CX-SEND").Checked
  GbParmRel.CX_Start_TR = RelTv.Nodes("CX-START-TR").Checked
  GbParmRel.CX_XCTL = RelTv.Nodes("CX-XCTL").Checked
  
  GbParmRel.IMS_SPAarea = RelTv.Nodes("IM-SPAAREA").Checked
  GbParmRel.IMS_Call_Pgm = RelTv.Nodes("IM-CALL").Checked
  GbParmRel.IMS_Call_TR = RelTv.Nodes("IM-START-TR").Checked
  GbParmRel.IMS_Transid = RelTv.Nodes("IM-TRAN").Checked
  GbParmRel.IMS_Send = RelTv.Nodes("IM-SEND").Checked
  
  GbParmRel.VS_DecFile = RelTv.Nodes("VS-DECFILE").Checked
  GbParmRel.VS_UseFile = RelTv.Nodes("VS-USEFILE").Checked
  
  GbParmRel.RD_DecTable = RelTv.Nodes("RD-DECTABLE").Checked
  GbParmRel.RD_UseTable = RelTv.Nodes("RD-USETABLE").Checked
  
  GbParmRel.DL_DecDli = RelTv.Nodes("DL-DECDLI").Checked
  GbParmRel.DL_Pgm_Psb = RelTv.Nodes("DL-PGM-PSB").Checked
  GbParmRel.DL_UseDli = RelTv.Nodes("DL-USEDLI").Checked
  
  GbParmRel.DliStdPsb = ChkDliStdPsb
  GbParmRel.DLIStdPsbName = DLIStdPsbName
  
  GbParmRel.ImsStdPcb = ChkImsStdPcb
  GbParmRel.ImsStdAltPcbName = ImsStdAltPcb
  GbParmRel.ImsStdIoPcbName = ImsStdIoPcb
  GbParmRel.ImsStdMstPcbName = ImsStdMstPcb
  
  AggiornaParmRel
  
End Sub
