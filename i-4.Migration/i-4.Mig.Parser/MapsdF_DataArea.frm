VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MapsdF_DataArea 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Query Repository"
   ClientHeight    =   6240
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9030
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6240
   ScaleWidth      =   9030
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Level 1"
      Height          =   6165
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   3435
      Begin MSComctlLib.ListView LwObj 
         Height          =   3435
         Left            =   90
         TabIndex        =   2
         Top             =   2610
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   6059
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Obj "
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Type"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.TreeView TwObj 
         Height          =   2295
         Left            =   90
         TabIndex        =   1
         Top             =   240
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   4048
         _Version        =   393217
         Indentation     =   529
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
      End
   End
End
Attribute VB_Name = "MapsdF_DataArea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub Resize()
  On Error Resume Next
  Me.Move m_Fun.FnLeft, m_Fun.FnTop, m_Fun.FnWidth, m_Fun.FnHeight
End Sub

Private Sub Form_Load()
  SetParent Me.hWnd, Parser.PsParent
  
  Resize
  
End Sub


Private Sub TreeView1_BeforeLabelEdit(Cancel As Integer)

End Sub
