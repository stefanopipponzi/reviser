Attribute VB_Name = "MapsdM_Campi"
Option Explicit
Option Compare Text

Global MigrNomeFile As String
Global BufMigr As String
Global RigaSucc As String

'SQ: da spostare in Parser:
Global DataValues() As Variant  'array di 2 stringhe

Dim nFile As Integer

'Type t_RelCpyLiv
'  idOggetto As Long
'  idArea As Long
'  idCopy As Long
'  NomeCopy As String
'  IdStruttura As Long
'End Type
'Public mRelCopyLivello() As t_RelCpyLiv

Type Dati
  DatTipo As String
  DatNull As String
  DatOrdinale As Integer
  DatNomeFld As String
  DatNomeRed As String
  DatNomeDepOn As String
  DatLev As String
  DatString As String
  DatSign As String
  DatVal As String
  DatUsage As String
  DatInt As String   'DatInt As Long - lunghezza variabile...
  DatPicture As String
  DatDec As Integer
  DatOccurs As Double
  DatOccursTo As Double
  DatDigit As Double
  DatPos As Double
  DatIndexed As String
  isLinkage As Boolean 'SQ
  idCopy As Long
  procName As String 'SQ [01]
End Type

Dim TipoTrk As String, StrInd As String, DataN1 As String, DataN2 As String, DataN4 As String
Dim livello As String, Tipo As String, Segno As String, Valore As String, Usage As String, wIndexed As String
Dim Interi As Long, Decimali As Long, Digit As Long
Dim Occurs As Double, OccursTo As Double
Dim OldLen As Long, OldDec As Long, olddigit As Long
Dim DatPicture As String
Dim OldSegno As String, OldTipo As String, OldNome As String

Dim SwEnd As Boolean
Dim Start As Long
Const maxFieldNumber = 60000
Const maxTabword = 6000
Dim Tabword(maxTabword) As String
Global DataItem(maxFieldNumber) As Dati  'SQ non bastavano 20000!
Global Idx As Long
Dim Glist As ListView
Dim wNumRec As Long
Dim wIdOgg As Long
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' 11-03-06
' Controlla se campi di linkage;
' Nuova colonna in PsData_Cmp: isLinkage (boolean)
' SQ: 17-10-06
' Gestisce COPY di tutti i livelli...
' ATTENZIONE: Non gestisce strutture che "continuano" i livelli interni di una copy.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub CaricaLstCampi(nomefile As String, wIdOggw As Long)
  Dim line As String, istruzione As String, copyname As String
  Dim TabLen(99) As Double, TabPos(50, 3) As Double
  Dim ILen As Double, IPos As Double, wpos As Double, I01 As Double
  Dim SavLev As Integer, k As Integer
  Dim SwEndTable As Boolean, isLinkage As Boolean
  Dim wLiv01 As Long, wLivPrec As Long, wIdArea As Long
  Dim idCopy As Long
  Dim areaEnded As Boolean
  
  'ReDim mRelCopyLivello(0)
  wIdOgg = wIdOggw
  Set Glist = Parser.PsFinestra
  
  MigrNomeFile = nomefile
  Idx = 0
  nFile = FreeFile
  wNumRec = 0
  Open MigrNomeFile For Input As #nFile
  'SQ 20-11-06
  On Error GoTo catch

  Do While Not EOF(nFile)
    DoEvents
    If Trim(RigaSucc) = "" Then
      Line Input #nFile, line
      wNumRec = wNumRec + 1
    Else
      line = RigaSucc
    End If
    RigaSucc = ""
    line = Space(6) & Mid(line & Space(72), 7, 66)
    If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
      
      'SQ 21-07-06
      'Problema: ' MOVE TO ' nella value!
      '=> basta "AFFINARE" QUESTA CONDIZIONE!
      'If InStr(line, "PROCEDURE ") > 0 Then
      
      If Mid(line, 8, 10) = "PROCEDURE " Then
        'If InStr(line, " DIVISION") > 0 Then
        Exit Do
        'End If
      ElseIf Mid(line, 8, 7) = "LINKAGE" Then
        If InStr(line, " SECTION") Then
          isLinkage = True
        End If
      ElseIf InStr(line, "REPORT-WRITER") > 0 Then
        If InStr(line, " SECTION.") > 0 Then
          Exit Do
        End If
        'SQ 21-07-06
        'Problema: ' MOVE TO ' nella value! (vedi sopra)
        'ElseIf InStr(line, " MOVE ") > 0 Then
        '  If InStr(line, " TO ") > 0 Then
        '    Exit Do
        '  End If
      'T GESTIONE "REMARKS"
      ElseIf InStr(line, " REMARKS.") > 0 Then
        Dim rsObj As Recordset
        Set rsObj = m_Fun.Open_Recordset("select tipo from bs_oggetti where " & _
                                         "idoggetto = " & wIdOggw)
        If Not rsObj!Tipo = "CPY" Then
          Do While Not InStr(line, "ENVIRONMENT") Or Not EOF(nFile)
            Line Input #nFile, line
            wNumRec = wNumRec + 1
            If InStr(line, "ENVIRONMENT") > 0 And InStr(line, " DIVISION.") > 0 Then
              Exit Do
            End If
          Loop
        End If
        rsObj.Close
      End If
    End If
    
    If InStr(line, " FILLER ") > 0 Then
      wLivPrec = -1
    End If
    '''''''''''''''''''''''''''''''''''''''''
    'Verifica se la riga � diversa da Spaces
    'SQ: utile questa funzione!
    '''''''''''''''''''''''''''''''''''''''''
    If VerRiga(line) Then
      '''''''''''''''''''''''''''''''''''
      'Verifica se la riga � un commento
      '''''''''''''''''''''''''''''''''''
      If Not VerCommento(line) Then
        'SQ - MODIFICATO VerCommento (vedi commenti...)
        If Len(RigaSucc) Then istruzione = ""
          'SQ: pezza temporanea
          'Va in abort se il VALUE va a capo...
          'If Mid(line, 7, 1) <> "-" Then
          '''''''''''''''''''''''''''''''''''''
          'Verifica se l'istruzione � terminata
          '''''''''''''''''''''''''''''''''''''
          If VerFineRiga(line, istruzione) Then
            '***************************************
            'Verifica se trattasi Declare Table
            '***************************************
            istruzione = Replace(istruzione, " IS ", " ")
            If Not VerDeclare(istruzione) Then
              '''''''''''''''''''''''''''''''''''''''
              ' E' una picture? (01-99)
              '''''''''''''''''''''''''''''''''''''''
              If VerValRiga(istruzione) Then
                '**********************************************
                'Separazione delle parole nella stringa letta,
                'Inserimento delle parole in un array (TabWord)
                '**********************************************
                If SeparaParole(istruzione, Tabword) Then
                  ''''''''''''''''''
                  'Censimento campi
                  ''''''''''''''''''
                  If Not CensCampi(Tabword) Then
                    istruzione = ""
   '                 Close #Nfile
   '                 Set Glist = Nothing
   '                 Exit Sub
                  Else
                    If livello = 1 Then
                      ILen = 0
                      wIdArea = wIdArea + 1
                    End If
                       
                    areaEnded = False
                    ILen = ILen + 1
                    Idx = Idx + 1
                    DataItem(Idx).DatTipo = "TRK"
                    DataItem(Idx).DatNull = ""
                    DataItem(Idx).DatOrdinale = ILen
                    DataItem(Idx).DatDec = Decimali
                    DataItem(Idx).DatDigit = Digit
                    DataItem(Idx).DatInt = Interi
                    DataItem(Idx).DatLev = format(livello, "00")
                    DataItem(Idx).DatNomeFld = DataN1
                    DataItem(Idx).DatNomeRed = DataN2
                    DataItem(Idx).DatNomeDepOn = DataN4
                    DataItem(Idx).DatOccurs = Occurs
                    DataItem(Idx).DatOccursTo = OccursTo
                    DataItem(Idx).DatSign = Segno
                    DataItem(Idx).DatString = Tipo
                    DataItem(Idx).DatUsage = Usage
                    DataItem(Idx).DatVal = Valore
                    DataItem(Idx).DatPicture = DatPicture
                    DataItem(Idx).DatIndexed = wIndexed
                    DataItem(Idx).isLinkage = isLinkage  'SQ
                    'stefano: azzerare!!! Visto che non c'� una redim...
                    DataItem(Idx).idCopy = 0
                    'SQ [01]
                    'DataItem(Idx).procName = procName
                    
                    istruzione = ""
                    
                    'SQ perch� guarda line? � proprio la linea o le ha appese?!
                    If InStr(line, " PIC") = 0 And InStr(line, " VALUE") = 0 Then
                      wLivPrec = livello
                    End If
                    
                    ''''''''''''''''''''''''''''''''''''''''
                    'SQ 11-05-07
                    ' Gestione PICTURE SU GRUPPO!
                    ' E' brutto qui... ma � un casino!
                    ''''''''''''''''''''''''''''''''''''''''
                    If Tipo = "A" And Usage <> "PTR" Then '!!!PTR hanno "A"!!!!!!!!!!!!!!!!!!!!!!
                      Dim groupLevel As Integer
                      Dim groupUsage As String
                      
                      groupUsage = Usage
                      groupLevel = livello
                    Else
                      'E' un gruppo dentro un gruppo!?
                      If livello <> 88 And livello > groupLevel And Usage = "ZND" And (Len(groupUsage) > 0 And groupUsage <> "ZND") Then
                        DataItem(Idx).DatUsage = groupUsage
                        'non abbiamo una funzioncina apposta!
                        If groupUsage = "PCK" Then
                          DataItem(Idx).DatDigit = Int(Digit / 2) + 1
                        ElseIf groupUsage = "BNR" Then
                          DataItem(Idx).DatDigit = Switch(Digit >= 1 And Digit <= 4, 2, Digit >= 5 And Digit <= 9, 4, True, 8)
                        End If
                      End If
                    End If
                  End If
                End If
              Else
                'SQ 06-03-2007 - Gestione nuova area
                If Not areaEnded Then
                  ''''''''''''''''''''''''''''''''''''
                  ' Gestione COPY (in struttura)
                  ''''''''''''''''''''''''''''''''''''
                  If InStr(line, " COPY ") Then
                    'SQ: controlla se la COPY appartiene alla struttura!
                    'SQ 06-03-2007
                    areaEnded = True  'Attenzione: casi 01 ciccio ; COPY A ; COPY B non sono gestiti!
                    copyname = LTrim(Replace(line, " COPY ", "")) & " "
                    copyname = Left(copyname, InStr(copyname, " "))  'cos'e' sta roba? un trim?
                    'SQ 10-02-06 - COPY FRA APICI!
                    copyname = Trim(Replace(Replace(copyname, ".", ""), "'", ""))
                    copyname = Replace(copyname, """", "")
                    'SQ: bello!!!!!!!!!!!!! riapre il file e lo riparsera per vedere il livello?!
                    'SQ Butto tutto via!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    'If CopyIsChildren(copyName) Then
                    '   ReDim Preserve mRelCopyLivello(UBound(mRelCopyLivello) + 1)
                    '   mRelCopyLivello(UBound(mRelCopyLivello)).idOggetto = GbIdOggetto
                    '   mRelCopyLivello(UBound(mRelCopyLivello)).idArea = Idx
                    '   'SQ l'ho appena cercato sopra nel CopyIsChildren!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    '   mRelCopyLivello(UBound(mRelCopyLivello)).idCopy = m_Fun.Restituisci_IdOggetto_Da_NomeCopy(copyName)
                    '   mRelCopyLivello(UBound(mRelCopyLivello)).NomeCopy = copyName
                    '   'SQ - 17-05-06
                    '   'sta gestione � ultralimita!!!!!!
                    '   '   => "idCopy" in PsData_Cmp!!!!
                    '   '      ==> mRelCopyLivello da buttare nel cesso!
                    '   DataItem(Idx).idCopy = mRelCopyLivello(UBound(mRelCopyLivello)).idCopy
                    'End If
                    idCopy = getCopyID(copyname)
                    If idCopy Then
                      ' stefano: ma perch� riusava getcopyid(copyname), quando lo ha appena estratto????
                      'DataItem(Idx).idCopy = getCopyID(copyname)
                      DataItem(Idx).idCopy = idCopy
                      ' stefano: per ho duplicato la lettura, non � bellissimo, ma non volevo
                      '  modificare i parametri della getcopyid
                      ' per ora funziona solo con la copy sotto il livello 01, ma era cos� anche per
                      ' l'idcopy prima
                      DataItem(Idx).DatDigit = getCopyDigit(idCopy)
                      DataItem(Idx).DatString = "A"
                    Else
                      'nuova struttura (il controllo non � dei pi� sicuri!)
                    End If
                  ElseIf InStr(line, " INCLUDE ") Then
                    areaEnded = True  'Attenzione: casi 01 ciccio ; COPY A ; COPY B non sono gestiti!
                    copyname = Trim(Mid(line, InStr(line, " SQL ") + 5))
                    copyname = LTrim(Replace(copyname, "INCLUDE ", "")) & " "
                    copyname = LTrim(Replace(copyname, "END-EXEC", "")) & " "
                    copyname = Left(copyname, InStr(copyname, " "))  'cos'e' sta roba? un trim?
                    'SQ 10-02-06 - COPY FRA APICI!
                    copyname = Trim(Replace(Replace(copyname, ".", ""), "'", ""))
                    copyname = Replace(copyname, """", "")
                    idCopy = getCopyID(copyname)
                    If idCopy Then
                      DataItem(Idx).idCopy = idCopy
                      DataItem(Idx).DatDigit = getCopyDigit(idCopy)
                      DataItem(Idx).DatString = "A"
                    Else
                      'nuova struttura (il controllo non � dei pi� sicuri!)
                    End If
                  ' Mauro 04/11/2010: Sono in una copy e non ci sono Picture = Copy di Procedure
                  '                   � inutile che continuo a ciclare qua dentro
                  ElseIf TypeFromId(wIdOggw) = "CPY" And Idx = 0 Then
                    Exit Do
                  End If
                End If
                istruzione = ""
              End If
            Else
              '***************************
              'Trattamento declare table
              '***************************
              SwEndTable = False
              Do While Not SwEndTable
                 CensColonne istruzione, SwEndTable
                 If livello = 1 Then ILen = 0
                 areaEnded = False
                 ILen = ILen + 1
                 Idx = Idx + 1
                 DataItem(Idx).DatTipo = "TBL"
                 DataItem(Idx).DatNull = StrInd
                 DataItem(Idx).DatOrdinale = ILen
                 DataItem(Idx).DatDec = Decimali
                 DataItem(Idx).DatDigit = Digit
                 DataItem(Idx).DatInt = Interi
                 DataItem(Idx).DatLev = format(livello, "00")
                 DataItem(Idx).DatNomeFld = DataN1
                 DataItem(Idx).DatNomeRed = DataN2
                 DataItem(Idx).DatNomeDepOn = DataN4
                 DataItem(Idx).DatOccurs = Occurs
                 DataItem(Idx).DatOccursTo = OccursTo
                 DataItem(Idx).DatSign = Segno
                 DataItem(Idx).DatString = Tipo
                 DataItem(Idx).DatUsage = Usage
                 DataItem(Idx).DatVal = Valore
                 DataItem(Idx).DatPicture = DatPicture
                 DataItem(Idx).DatIndexed = wIndexed
                 'SQ [01]
                 DataItem(Idx).procName = procName
              Loop
              istruzione = ""
            End If  'VerDeclare
          Else
            wLivPrec = -1
          End If  'VerFineRiga
''''        Else  'SQ tmp: VALUE su piu' righe...
''''          'Controllo di sicurezza:
''''          'SQ 18-07-06
''''          'Arriva con Idx ancora sul livello 01 (quindi DatVal ="") !
''''          'es: " 02  FILLER  PIC X(120) VALUE    'X "
''''          ' => IN CHE CASI FUNZIONAVA?
''''          If Left(DataItem(Idx).DatVal, 1) = "'" And Right(DataItem(Idx).DatVal, 1) <> "'" Then
''''            'ultimo "pezzo" del value?  SBAGLIATO!!!!
''''            If Mid(line, 72, 1) <> " " Then 'non esatto: potrei avere apice o punto!
''''              DataItem(Idx).DatVal = DataItem(Idx).DatVal & Trim(Mid(line, InStr(line, "'") + 1)) 'pulisco da trattino + apice
''''            Else
''''              line = Trim(Mid(line, InStr(line, "'") + 1))  'pulisco da trattino + apice
''''              'ultimo "pezzo"
''''              If Right(line, 1) = "." Then  'eventuale punto finale...
''''                line = Trim(Left(line, Len(line) - 1))
''''              End If
''''              DataItem(Idx).DatVal = DataItem(Idx).DatVal & line
''''            End If
''''          Else
''''            'Sempre pezza (quando entro nell'if sopra)?
''''            If Len(DataItem(Idx).DatVal) = 0 And InStr(istruzione, "VALUE") Then
''''              line = Mid(line, InStr(line, "'") + 1)  'pulisco da trattino + apice
''''              'ultimo "pezzo"
''''              If Right(line, 1) = "." Then  'eventuale punto finale...
''''                line = Trim(Left(line, Len(line) - 1))
''''              End If
''''              istruzione = istruzione & line
''''            End If
''''          End If
''''        End If
      Else
        wLivPrec = -1
      End If  'VerCommento
    End If  'verRiga
    
    ''''''''''''''''''''''''''''''''''''
    ' SQ  - 16-03-06
    ' Posso avere un miliardo di campi!
    ' ci fermiamo a "maxFieldsNumber" e segnaliamo
    ''''''''''''''''''''''''''''''''''''
    If Idx = maxFieldNumber Then
      m_Fun.writeLog "Parsing obj " & GbIdOggetto & ": too many fields declared in object (>" & maxFieldNumber & ")"
      Glist.ListItems.Add , , ("Parsing obj " & GbIdOggetto & ": too many fields declared in object (>" & maxFieldNumber & ")")
      Glist.Refresh
      Exit Do
    End If
  Loop
  Close #nFile
  RigaSucc = ""
  'SQ 20-11-06
  On Error GoTo 0

  If Idx = 0 Then
    Set Glist = Nothing
    Exit Sub
  End If

  '********************************************************
  'Calcolo delle lunghezze dei livelli superiori
  '********************************************************
  SavLev = Val(DataItem(Idx).DatLev)
  ILen = SavLev
  TabLen(ILen) = DataItem(Idx).DatDigit
  Start = Idx - 1
  If SavLev = 88 Then
    For Start = Start To 2 Step -1
      If Val(DataItem(Start).DatLev) <> 88 Then
        If Val(DataItem(Start).DatLev) = 1 Then
          Start = Start - 1
          SavLev = Val(DataItem(Start).DatLev)
          ILen = SavLev
          TabLen(ILen) = DataItem(Start).DatDigit
        Else
          SavLev = Val(DataItem(Start).DatLev)
          ILen = SavLev
        End If
        Exit For
      End If
    Next Start
  End If
  For Start = Start To 1 Step -1
    If Val(DataItem(Start).DatLev) = SavLev Then
      If Val(DataItem(Start).DatLev) <> 88 Then
        If DataItem(Start).DatOccurs = 0 Then
          TabLen(ILen) = TabLen(ILen) + DataItem(Start).DatDigit
        Else
          TabLen(ILen) = TabLen(ILen) + (DataItem(Start).DatDigit * DataItem(Start).DatOccurs)
        End If
      End If
    Else
      If Val(DataItem(Start).DatLev) > SavLev Then
        SavLev = Val(DataItem(Start).DatLev)
        'ILen = ILen - 1
        ILen = SavLev
        If ILen < 0 Then ILen = 0
        If DataItem(Start).DatOccurs = 0 Then
          TabLen(ILen) = DataItem(Start).DatDigit
        Else
          TabLen(ILen) = (DataItem(Start).DatDigit * DataItem(Start).DatOccurs)
        End If
      Else
        SavLev = Val(DataItem(Start).DatLev)
        If DataItem(Start).DatOccurs = 0 Then
          DataItem(Start).DatDigit = TabLen(ILen)
        Else
          DataItem(Start).DatDigit = TabLen(ILen) * DataItem(Start).DatOccurs
        End If
        'ILen = ILen + 1
        ILen = SavLev
        If SavLev = 1 Then
          TabLen(ILen) = 0
        Else
          TabLen(ILen) = TabLen(ILen) + DataItem(Start).DatDigit
        End If
        For k = ILen + 1 To 99
          TabLen(k) = 0
        Next k
      End If
    End If
    ' Mauro 23-02-2007: Aggiunto il controllo per il livello 88, altrimenti ogni volta lo cancella
    If Trim(DataItem(Start).DatNomeRed & "") <> "" And DataItem(Start).DatLev <> 88 Then
      'SG : TabLen(ILen) = TabLen(ILen) - DataItem(Start).DatDigit
      TabLen(ILen) = Abs(TabLen(ILen) - DataItem(Start).DatDigit)
    End If
  Next Start
  ' ****************************************
  ' * Calcolo Scostamenti dei campi *
  ' ****************************************
  I01 = 1
  SavLev = Val(DataItem(1).DatLev)
  ILen = 1
  TabLen(ILen) = DataItem(2).DatDigit
  IPos = 1
  TabPos(IPos, 1) = DataItem(1).DatLev
  TabPos(IPos, 2) = 1
  TabPos(IPos, 3) = DataItem(1).DatDigit
  DataItem(1).DatPos = 1
  For Start = 2 To Idx
      If Val(DataItem(Start).DatLev) = 88 Then
        DataItem(Start).DatPos = DataItem(Start - 1).DatPos
      Else
        If Val(DataItem(Start).DatLev) = 1 Or Val(DataItem(Start).DatLev) = 77 Then
          If DataItem(I01).DatDigit = 0 Then
            DataItem(I01).DatDigit = TabLen(ILen)
          End If
          TabLen(ILen) = 0
          I01 = Start
          IPos = 1
          TabPos(IPos, 1) = DataItem(Start).DatLev
          TabPos(IPos, 2) = 1
          DataItem(Start).DatPos = 1
          TabPos(IPos, 3) = DataItem(Start).DatDigit
          SavLev = Val(DataItem(2).DatLev)
        Else
          If Val(DataItem(Start).DatLev) = SavLev Then
            If Trim(DataItem(Start).DatNomeRed & "") = "" Then
              TabLen(ILen) = TabLen(ILen) + DataItem(Start).DatDigit
            End If
          End If
          If DataItem(Start).DatLev = TabPos(IPos, 1) Then
            If DataItem(Start).DatNomeRed <> "" Then
              DataItem(Start).DatPos = TabPos(IPos, 2)
            Else
              TabPos(IPos, 2) = TabPos(IPos, 2) + TabPos(IPos, 3)
              'T 9-4-2009
              'TabPos(IPos, 3) = IIf(DataItem(Start).DatOccurs > 0, DataItem(Start).DatDigit * DataItem(Start).DatOccurs, DataItem(Start).DatDigit)
              TabPos(IPos, 3) = DataItem(Start).DatDigit
              DataItem(Start).DatPos = TabPos(IPos, 2)
            End If
          Else
            If DataItem(Start).DatLev > DataItem(Start - 1).DatLev Then
              IPos = IPos + 1
              TabPos(IPos, 1) = DataItem(Start).DatLev
              TabPos(IPos, 2) = TabPos(IPos - 1, 2)
              TabPos(IPos, 3) = DataItem(Start).DatDigit
              DataItem(Start).DatPos = TabPos(IPos, 2)
              SavLev = DataItem(Start).DatLev
            Else
              For IPos = IPos To 1 Step -1
                If DataItem(Start).DatLev = TabPos(IPos, 1) Then Exit For
              Next IPos
              If DataItem(Start).DatNomeRed <> "" Then
                DataItem(Start).DatPos = TabPos(IPos, 2)
              Else
                TabPos(IPos, 2) = TabPos(IPos, 2) + TabPos(IPos, 3)
                TabPos(IPos, 3) = DataItem(Start).DatDigit
                DataItem(Start).DatPos = TabPos(IPos, 2)
              End If
            End If
          End If
        End If
      End If
  Next Start
  If DataItem(I01).DatDigit = 0 Then
    DataItem(I01).DatDigit = TabLen(ILen)
  End If
  '********************************
  'Caricamento della lista a video
  '********************************
  Set Glist = Nothing
  Exit Sub
catch:
  'Rimane il file aperto... poi casini indescrivibili!
  'Continuo a gestire dal chiamante... qui chiudo solo il file:
  Close #nFile
  err.Raise err.Number, , err.description
End Sub

Function CensColonne(istruzione, SwEndTable)
  Dim y As Integer
  
  livello = ""
  DataN1 = ""
  DataN2 = ""
  DataN4 = ""
  Segno = ""
  Tipo = ""
  Interi = 0
  Decimali = 0
  Occurs = 0
  OccursTo = 0
  Digit = 0
  Usage = ""
  Valore = ""
  wIndexed = ""
  DatPicture = ""
  
  y = InStr(istruzione, "DECLARE")
  If y > 0 Then
    istruzione = Mid(istruzione, y + 7)
    y = InStr(istruzione, "TABLE")
    If y > 0 Then
      livello = "01"
      DataN1 = Trim(Mid(istruzione, 1, y - 1))
      DataN2 = ""
      DataN4 = ""
      Segno = ""
      StrInd = ""
      Tipo = "X"
      Interi = 0
      Decimali = 0
      Occurs = 0
      OccursTo = 0
      Digit = 0
      Usage = "ZND"
      Valore = ""
      DatPicture = ""
      istruzione = Mid(istruzione, y + 5)
      istruzione = LTrim(istruzione)
      Exit Function
    End If
  End If
  If Mid(istruzione, 1, 1) = "(" Then
    istruzione = Trim(Mid(istruzione, 2))
  End If
  y = InStr(istruzione, " ")
  If y > 0 Then
    livello = "10"
    DataN1 = Mid(istruzione, 1, y - 1)
    istruzione = Trim(Mid(istruzione, y))
    If Mid(istruzione, 1, 4) = "CHAR" Then
      Tipo = "X"
      Segno = "N"
      istruzione = Trim(Mid(istruzione, 5))
      CalcLen istruzione
      VerNull istruzione
      Usage = "ZND"
      Digit = Interi + Decimali
    End If
    If Mid(istruzione, 1, 7) = "VARCHAR" Then
      Tipo = "X"
      Segno = "N"
      istruzione = Trim(Mid(istruzione, 8))
      CalcLen istruzione
      VerNull istruzione
      Usage = "ZND"
      Digit = Interi + Decimali
    End If
    If Mid(istruzione, 1, 7) = "DECIMAL" Then
      Tipo = "9"
      Segno = "S"
      istruzione = Trim(Mid(istruzione, 8))
      CalcLen istruzione
      VerNull istruzione
      Usage = "PCK"
      Digit = Interi + Decimali
      Digit = Int(Digit / 2) + 1
    End If
    If Mid(istruzione, 1, 4) = "DATE" Then
      Tipo = "X"
      Segno = "N"
      istruzione = Trim(Mid(istruzione, 5))
      Interi = 10
      Decimali = 0
      VerNull istruzione
      Usage = "ZND"
      Digit = Interi + Decimali
    End If
    If Mid(istruzione, 1, 7) = "INTEGER" Then
      Tipo = "9"
      Segno = "S"
      istruzione = Trim(Mid(istruzione, 8))
      Interi = 9
      Decimali = 0
      VerNull istruzione
      Usage = "BNR"
      Digit = 4
    End If
    If Mid(istruzione, 1, 8) = "SMALLINT" Then
      Tipo = "9"
      Segno = "S"
      istruzione = Trim(Mid(istruzione, 9))
      Interi = 4
      Decimali = 0
      VerNull istruzione
      Usage = "BNR"
      Digit = 2
    End If
    If Mid(istruzione, 1, 1) = ")" Then
      istruzione = Trim(Mid(istruzione, 2))
    End If
    If Mid(istruzione, 1, 8) = "END-EXEC" Then
      SwEndTable = True
      Exit Function
    End If
  End If
End Function

Function VerNull(istruzione)
  StrInd = ""
  If Mid(istruzione, 1, 1) = "," Then
    istruzione = Trim(Mid(istruzione, 2))
    Exit Function
  End If
  If Mid(istruzione, 1, 3) = "NOT" Then
    StrInd = StrInd & "N"
    istruzione = Trim(Mid(istruzione, 4))
  End If
  If Mid(istruzione, 1, 4) = "NULL" Then
    StrInd = StrInd & "N"
    istruzione = Trim(Mid(istruzione, 5))
  End If
  If Mid(istruzione, 1, 1) = "," Then
    istruzione = Trim(Mid(istruzione, 2))
    Exit Function
  End If
  If Mid(istruzione, 1, 4) = "WITH" Then
    istruzione = Trim(Mid(istruzione, 5))
    StrInd = StrInd & "W"
  End If
  If Mid(istruzione, 1, 7) = "DEFAULT" Then
    istruzione = Trim(Mid(istruzione, 8))
    StrInd = StrInd & "D"
  End If
End Function

Function CalcLen(istruzione)
  Dim SwLen As Boolean
  Dim StrIntDec As String
  Dim Z As Integer
  
  SwLen = False
  Z = 1
  StrIntDec = ""
  Do While SwLen = False
    If Mid(istruzione, Z, 1) = "(" Then
      istruzione = Trim(Mid(istruzione, Z + 1))
    End If
    If Mid(istruzione, Z, 1) = ")" Then
      istruzione = Trim(Mid(istruzione, Z + 1))
      Z = InStr(StrIntDec, ",")
      If Z > 0 Then
        Interi = Val(Mid(StrIntDec, 1, Z - 1))
        Decimali = Val(Mid(StrIntDec, Z + 1))
      Else
        Interi = Val(Trim(StrIntDec))
        Decimali = 0
      End If
      SwLen = True
      Exit Do
    End If
    If Mid(istruzione, Z, 1) <> " " Then
      StrIntDec = StrIntDec & Trim(Mid(istruzione, Z, 1))
    End If
    Z = Z + 1
  Loop
End Function

Function VerDeclare(istruzione) As Boolean
  Dim posEXEC As Integer, posSQL As Integer, posTABLE As Integer
  
  posEXEC = InStr(istruzione, "EXEC ")
  posSQL = InStr(istruzione, " SQL")
  posTABLE = InStr(istruzione, "TABLE")
  If posEXEC > 0 And posSQL > 0 And posTABLE > 0 Then
    If posSQL > posEXEC And posTABLE > posSQL Then
      VerDeclare = True
    Else
      VerDeclare = False
    End If
  Else
    VerDeclare = False
  End If
End Function

Function VerValRiga(istruzione) As Boolean
  Dim k As Integer
  Dim firstWord As String

  k = InStr(istruzione, " ")
  If k > 3 Then
    VerValRiga = False
    Exit Function
  End If
  k = InStr(LTrim(istruzione), " ")
  firstWord = (Mid(LTrim(istruzione), 1, k - 1))
  firstWord = Replace(firstWord, "/", 0)
  
  If Len(firstWord) > 2 Then
    VerValRiga = False
    Exit Function
  End If
  
  If Val(firstWord) > 0 And Val(firstWord) < 99 Then
    VerValRiga = True
  Else
    VerValRiga = False
  End If
End Function

Function VerRiga(TextLine) As Boolean
  If Trim(TextLine) <> "" Then
     VerRiga = True
  Else
     VerRiga = False
  End If
End Function

Function SeparaParole(istruzione, Tabword) As Boolean
  Dim Ind As Integer
  Dim Parola As String
  Dim SwValue As Boolean
  Dim wIndSta As Long
  Dim CntApc As Integer
  
  SwValue = False
  
  '**********************************************
  'Separazione delle parole nella stringa letta,
  'Inserimento delle parole in un array (TabWord)
  '**********************************************
  ' Va inizializzato sto coso, senn� si incarta tutto!!!
  ' Che poi, senza fare polemica, ma... farlo dinamico faceva schifo?!?!?!
  For wIndSta = 1 To UBound(Tabword)
    Tabword(wIndSta) = ""
  Next wIndSta
  
  Ind = 0
  wIndSta = 0
  Parola = ""
  CntApc = 0
  istruzione = Replace(istruzione, "X (", "X(")
  For wIndSta = 1 To Len(istruzione)
    If Mid(istruzione, wIndSta, 1) = "'" Then
      CntApc = CntApc + 1
    End If
    If Mid(istruzione, wIndSta, 1) <> " " Or CntApc > 0 Then
      Parola = Parola & Mid(istruzione, wIndSta, 1)
      If CntApc = 2 Then
        CntApc = 0
      End If
    Else
       If Trim(Parola) <> "" Then
          ' Mauro 11/06/2007 : Non deve fare la replace sul valore del campo
          If Not SwValue Then
            Parola = Replace(Parola, ":", "")
          End If
          Ind = Ind + 1
          Tabword(Ind) = Parola
          If Not SwValue Then
             Tabword(Ind) = UCase(Tabword(Ind))
          End If
          If Mid$(Tabword(Ind), 1, 6) = "VALUE'" Then
             Tabword(Ind + 1) = Mid$(Tabword(Ind), 6)
             Tabword(Ind) = Mid$(Tabword(Ind), 1, 5)
             Ind = Ind + 1
             SwValue = True
          End If
          If Mid$(Tabword(Ind), 1, 7) = "VALUES'" Then
             Tabword(Ind + 1) = Mid$(Tabword(Ind), 7)
             Tabword(Ind) = Mid$(Tabword(Ind), 1, 6)
             Ind = Ind + 1
             SwValue = True
          End If
             
          If Tabword(Ind) = "VALUE" Or Tabword(Ind) = "VALUES" Then SwValue = True
          If Ind = 2 And Tabword(Ind) = "PIC" Then
            Tabword(Ind) = "FILLER"
            Ind = Ind + 1
            Tabword(Ind) = "PIC"
            
          End If
       End If
       Parola = ""
    End If
  Next wIndSta
  If Trim(Parola) <> "" Then
     Ind = Ind + 1
     Tabword(Ind) = Parola
  End If
  Tabword(Ind + 1) = ""
  If Tabword(Ind) <> "" Then
     SeparaParole = True
  Else
     SeparaParole = False
  End If
End Function

Function CensCampi(Tabword() As String) As Boolean
  '*****************************
  'Ricerca del livello DataItem
  '*****************************
  Dim y As Integer, k As Integer, i As Integer
  Dim indexedStatement As Boolean

  livello = ""
  DataN1 = ""
  DataN2 = ""
  DataN4 = ""
  Segno = ""
  Tipo = ""
  Interi = 0
  Decimali = 0
  Occurs = 0
  OccursTo = 0
  Digit = 0
  Usage = ""
  Valore = ""
  DatPicture = ""
  'wCOPY_IDMS = False
  
  '''''''''''''''''
  ' Livello:
  '''''''''''''''''
  For y = 1 To 99
    If InStr(Tabword(1) & " ", format(y, "00 ")) > 0 Then
     livello = format(y, "00")
     Exit For
    End If
  Next y
  If livello = "" Then
    For y = 1 To 99
      If InStr(Tabword(1) & " ", format(y, "#0 ")) > 0 Then
       livello = format(y, "00")
       Exit For
      End If
    Next y
  End If
  'Livello inesistente
  If livello = "" Then
    AggSegn " ", "NOLIVELLO"
    CensCampi = False
    Exit Function
  End If
  
  '''''''''''''''''''''''''''''''
  ' Nome campo
  '''''''''''''''''''''''''''''''
  Select Case Tabword(2)
    Case "REDEFINES", "PIC", "PICTURE", "VALUE", "VALUES"
      For k = 2 To maxTabword
        If Tabword(k) = "" Then
          Tabword(k + 1) = ""
          Exit For
        End If
      Next k
      For y = k To 3 Step -1
        Tabword(y) = Tabword(y - 1)
      Next y
      Tabword(2) = "FILLER"
  End Select

  DataN1 = ""
  y = InStr(Tabword(2), ".")
  If y > 0 Then
    DataN1 = Mid(Tabword(2), 1, y - 1)
    SwEnd = True
  Else
    DataN1 = Tabword(2)
    SwEnd = False
  End If
  If Val(livello) = 88 Then
    DataN2 = OldNome
  Else
    OldNome = DataN1
  End If
  'Nome Campo inesistente
  If DataN1 = "" Then
    AggSegn " ", "NONOME"
    CensCampi = False
    Exit Function
  End If
  
  '*********************
  'Ricerca dichiarative
  '*********************
  Usage = "ZND"
  Tipo = "A"
  For Start = 3 To maxTabword
    If Tabword(Start) = "" Then Exit For
    If Not SwEnd Then
      Select Case Tabword(Start)
        Case "ASCENDING"
          Start = Start + 1
          If Tabword(Start) = "KEY" Then
            Start = Start + 1
            If Tabword(Start) = "IS" Then
              Start = Start + 1
            End If
          End If
        Case "OCCURS"
          Start = Start + 1
          If Not VerOccurs() Then
            AggSegn Tabword(Start), "OCCURS"
            CensCampi = False
            Exit Function
          End If
        Case "TO"
          Start = Start + 1
          If Not VerOccursTo() Then
            AggSegn Tabword(Start), "OCCURS TO"
            CensCampi = False
            Exit Function
          End If
        Case "REDEFINES"
          Start = Start + 1
          If Not VerRedefine() Then
            AggSegn Tabword(Start), "REDEFINE"
            CensCampi = False
            Exit Function
          End If
        Case "RENAMES"
          Start = Start + 1
          If Not VerRedefine() Then
            AggSegn Tabword(Start), "RENAMES"
            CensCampi = False
            Exit Function
          End If
        Case "THROUGH", "THRU"
          Start = Start + 1
        Case "LEADING", "TRAILING", "GLOBAL", "GLOBAL.", "SEPARATE", "SEPARATE.", "SIGN", "IS", "CHARACTER", "CHARACTER.", "DISPLAY", "DISPLAY."
        Case "BLANK"
          Start = Start + 1
          If Tabword(Start) = "WHEN" Then
            Start = Start + 1
          End If
        Case "USAGE"
          If Not VerUsage() Then
            AggSegn Tabword(Start), "USAGE"
            CensCampi = False
            Exit Function
          End If
        Case "IS", "ARE"
          If Not VerUsage() Then
            AggSegn Tabword(Start), "USAGE IS"
            CensCampi = False
            Exit Function
          End If
        Case "VALUE", "VALUES", "value", "values"
          If livello = 88 Then
            Interi = OldLen
            Decimali = OldDec
            Segno = OldSegno
            Tipo = OldTipo
            Digit = olddigit
          End If
          Start = Start + 1
          If Not VerValue() Then
            CensCampi = False
            AggSegn Tabword(Start), "VALUE"
            Exit Function
          End If
        Case "REPRESENTS"
          Start = Start + 1
          Valore = "#" & Tabword(Start) & "#"
        Case "PIC", "PICTURE", "pic"
          Start = Start + 1
          If Not VerTipo() Then
            AggSegn Tabword(Start), "PICTURE"
            CensCampi = False
            Exit Function
          Else
            If Not VerLen() Then
              CensCampi = False
              AggSegn Tabword(Start), "LUNGHEZZA"
              Exit Function
            End If
            If Val(livello) = 88 Then
              Digit = olddigit
            Else
              Digit = Interi + Decimali
              ' Mauro 18/01/2008 : posso avere gi� passato la clausola COMP-3
              If Usage = "PCK" Then
                Digit = Int(Digit / 2) + 1
              End If
              olddigit = Digit
            End If
          End If
        Case "COMP.", "COMPUTATIONAL.", "COMP", "COMPUTATIONAL", "BINARY", "BINARY."
          'SQ - casi 01 CICCIO COMP S9(...) li sbaglia! VerComp entra con Digit=0 e restituisce l'ultimo!
          If Digit Then '� una pezza! il problema � per tutti!
          'IRIS-DB aggiunto solo questo commento per forzare una modifica qualsiasi
            If Not VerComp Then
              AggSegn Tabword(Start), "COMP"
              CensCampi = False
              Exit Function
            End If
          Else
            'lo butto in fondo! (non c'� gi� un maxInd?! allora farlo!)
            For i = Start + 1 To maxTabword
            'IRIS-DB ma cavolo con il punto che c'era nel mezzo tutto il discorso saltava perch� poi settava swEnd e non arrivava al COMP buttato in fondo
            'IRIS-DB poi perch� solo COMP? Tutte le clausole possono essere spostate in fondo. E' casuale, comunque per ora lascio cos�, abbiamo trovato solo casi con COMP
              Tabword(i) = Replace(Tabword(i), ".", "") 'IRIS-DB
              If Tabword(i) = "" Then
                'IRIS-DB Tabword(i) = Tabword(Start)
                Tabword(i) = Tabword(Start) & "." 'IRIS-DB
                Exit For
              End If
            Next i
          End If
        Case "COMP-X.", "COMPUTATIONAL-X.", "COMP-X", "COMPUTATIONAL-X"
          If Not VerCompX Then
            AggSegn Tabword(Start), "COMP-X"
            CensCampi = False
            Exit Function
          End If
        Case "COMP-5.", "COMPUTATIONAL-5.", "COMP-5", "COMPUTATIONAL-5"
          If Not VerComp5 Then
            AggSegn Tabword(Start), "COMP-5"
            CensCampi = False
            Exit Function
          End If
        Case "COMP-4.", "COMPUTATIONAL-4.", "COMP-4", "COMPUTATIONAL-4"
          If Not VerComp4 Then
            AggSegn Tabword(Start), "COMP-4"
            CensCampi = False
            Exit Function
          End If
        Case "COMP-2.", "COMPUTATIONAL-2.", "COMP-2", "COMPUTATIONAL-2"
          If Not VerComp4 Then
            AggSegn Tabword(Start), "COMP-2"
            CensCampi = False
            Exit Function
          End If
        Case "COMP-3.", "COMPUTATIONAL-3.", "COMP-3", "COMPUTATIONAL-3"
          If Not VerComp3 Then
            AggSegn Tabword(Start), "COMP-3"
            CensCampi = False
            Exit Function
          End If
        Case "COMP-1.", "COMPUTATIONAL-1.", "COMP-1", "COMPUTATIONAL-1"
          If Not VerPointer Then
            AggSegn Tabword(Start), "COMP-1"
            CensCampi = False
            Exit Function
          End If
        Case "POINTER.", "POINTER"
          If Not VerPointer Then
            AggSegn Tabword(Start), "POINTER"
            CensCampi = False
            Exit Function
          End If
        Case "TIMES.", "TIMES"
          If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
            SwEnd = True
          Else
            SwEnd = False
          End If
          CensCampi = True
        Case "DEPENDING"
          If Trim(Tabword(Start + 1)) <> "ON" Then
            Start = Start + 1  ' skippo campo depending on
          End If
          If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
            SwEnd = True
          Else
            SwEnd = False
          End If
          CensCampi = True
        Case "SYNC", "SYNCHRONIZED", "JUSTIFIED", "RIGHT", "LEFT", "JUST", "EXTERNAL"
          If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
            SwEnd = True
          Else
            SwEnd = False
          End If
          CensCampi = True
        Case "SYNC.", "SYNCHRONIZED.", "JUSTIFIED.", "RIGHT.", "LEFT.", "JUST.", "EXTERNAL."
          SwEnd = True
          CensCampi = True
        Case "INDEXED"
          Start = Start + 1
          If Tabword(Start) = "BY" Then
            Start = Start + 1
          End If
          wIndexed = Tabword(Start)
          k = InStr(wIndexed, ".")
          If k Then
            wIndexed = Left(wIndexed, k - 1)
            SwEnd = True
            CensCampi = True
          Else
            SwEnd = False
            'SQ - posso avere N indici: tengo l'ultimo!
            indexedStatement = True
          End If
        Case "ON"
          Start = Start + 1 ' skippo campo depending on
          If VerOn = False Then
            AggSegn Tabword(Start), "ON"
            CensCampi = False
            Exit Function
          End If
        Case "OF"
          Start = Start + 1
'        Case "IDMS"
'          Start = Start + 1
'          If DataN1 = "COPY" Then
'            wCOPY_IDMS = True
'            DataN1 = ""
'            GbNumRec = wNumRec
'            If Trim(Tabword(Start)) = "RECORD" Then
'              Start = Start + 1
'            End If
'            getCOPY Trim(Tabword(Start))
'            Exit Function
'          End If
        Case Else
          'SQ
          If Not indexedStatement Then
            If Trim(Tabword(Start)) = "." Then
              SwEnd = True
              CensCampi = True
            Else
              If Tabword(Start - 1) = "THRU" Then
                If InStr(Tabword(Start), ".") > 0 Then
                  SwEnd = True
                  CensCampi = True
                End If
              Else
                AggSegn Tabword(Start), "NOCLAUSOLA"
                CensCampi = False
             End If
           End If
         Else
           indexedStatement = False
           wIndexed = Tabword(Start)
           k = InStr(wIndexed, ".")
           If k Then
             wIndexed = Left(wIndexed, k - 1)
             SwEnd = True
             CensCampi = True
           Else
             SwEnd = False
             indexedStatement = True
           End If
         End If
      End Select
    Else
      Exit For
    End If
  Next Start
  CensCampi = True
End Function

Function VerOn() As Boolean
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    SwEnd = True
  Else
    SwEnd = False
  End If
  If SwEnd = True Then
    DataN4 = Mid(Tabword(Start), 1, Len(RTrim(Tabword(Start))) - 1)
  Else
    DataN4 = Tabword(Start)
  End If
  VerOn = True
End Function

Function VerComp() As Boolean
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    SwEnd = True
  Else
    SwEnd = False
  End If
  If Tipo = "X" Then
    VerComp = False
  Else
    Usage = "BNR"
    If Digit >= 1 And Digit <= 4 Then
     Digit = 2
    End If
    If Digit >= 5 And Digit <= 9 Then
     Digit = 4
    End If
    If Digit > 9 Then
     Digit = 8
    End If
    olddigit = Digit
    VerComp = True
  End If
End Function

Function VerCompX() As Boolean
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    SwEnd = True
  Else
    SwEnd = False
  End If
  Segno = "N"
  Usage = "BNR"
  olddigit = Digit
  VerCompX = True
End Function

Function VerComp5() As Boolean
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    SwEnd = True
  Else
    SwEnd = False
  End If
  If Tipo = "X" Then
    Segno = "N"
  End If
  Usage = "BNR"
  olddigit = Digit
  VerComp5 = True
End Function

Function VerComp4() As Boolean
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    SwEnd = True
  Else
    SwEnd = False
  End If
  If Tipo = "X" Then
    VerComp4 = False
    Exit Function
  End If
  Usage = "BNR"
  If Segno = "S" Then
    Select Case Digit
      Case 1, 2
        Digit = 1
      Case 3, 4
        Digit = 2
      Case 5, 6
        Digit = 3
      Case 7, 8, 9
        Digit = 4
      Case 10, 11
        Digit = 5
      Case 12, 13, 14
        Digit = 6
      Case 15, 16
        Digit = 7
      Case 17, 18
        Digit = 8
    End Select
  Else
    Select Case Digit
      Case 1, 2
        Digit = 1
      Case 3, 4
        Digit = 2
      Case 5, 6, 7
        Digit = 3
      Case 8, 9
        Digit = 4
      Case 10, 11, 12
        Digit = 5
      Case 13, 14
        Digit = 6
      Case 15, 16
        Digit = 7
      Case 17, 18
        Digit = 8
    End Select
  End If
  olddigit = Digit
  VerComp4 = True
End Function

Function VerComp3() As Boolean
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    SwEnd = True
  Else
    SwEnd = False
  End If
  If Tipo = "X" Then
    VerComp3 = False
    Exit Function
  End If
  Usage = "PCK"
  Digit = Int(Digit / 2) + 1
  VerComp3 = True
End Function

Function VerPointer() As Boolean
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    SwEnd = True
  Else
    SwEnd = False
  End If
  Usage = "PTR"
  Digit = 4
  Interi = 8
  Decimali = 0
  VerPointer = True
End Function

Function VerRedefine() As Boolean
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    SwEnd = True
    DataN2 = Mid(Tabword(Start), 1, Len(RTrim(Tabword(Start))) - 1)
    VerRedefine = True
  Else
    SwEnd = False
    DataN2 = Tabword(Start)
    VerRedefine = True
  End If
End Function

Function VerUsage() As Boolean
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    SwEnd = True
  Else
    SwEnd = False
  End If
  VerUsage = True
End Function

Function VerIs() As Boolean
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    SwEnd = True
  Else
    SwEnd = False
  End If
  VerIs = True
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function VerFineRiga(TextLine As String, istruzione As String) As Boolean
  Dim firstWord As String, k As Integer
  Dim prevLine As String
  
  'SQ 18-07-06: gestione VALUE su pi� righe
  If InStr(istruzione, " VALUE ") And Mid(TextLine, 7, 1) = "-" Then
    TextLine = Mid(TextLine, 8, 64)
    TextLine = Mid(TextLine, InStr(TextLine, "'") + 1)  'pulisco da trattino + apice
    istruzione = istruzione & TextLine
  End If
  
  '*******************************************
  'Verifica se l'istruzione � terminata
  '*******************************************
  If (InStr(istruzione, " PIC ") Or InStr(istruzione, " PICTURE ")) Then
    If (InStr(TextLine, " PIC ") Or InStr(TextLine, " PICTURE ")) Then
      'SQ
      'problema!!!! se sono nella value tiro su PICTURE che non c'entrano niente!!!!
      If Mid(Trim(istruzione), Len(Trim(istruzione)) - 1) <> "." Then 'esiste la "right"
        istruzione = RTrim(istruzione) & ". "
      End If
      VerFineRiga = True
      RigaSucc = TextLine
      If Not VerificaIstruzione(istruzione) Then
        istruzione = ""
        VerFineRiga = False
      End If
      Exit Function
    End If
  End If

  If Left(LTrim(istruzione), 5) = "EXEC " Then
    If InStr(TextLine, "END-EXEC") > 0 Then
      istruzione = ""
    End If
    VerFineRiga = False
    Exit Function
  End If

  firstWord = LTrim(TextLine)
  k = InStr(firstWord, " ")
  If k > 0 Then firstWord = Left(firstWord, k - 1)
  firstWord = Replace(firstWord, "/", 0)
  
  'SQ il punto non � obbligatorio alla fine di una struttura dati?!
  If Len(firstWord) < 3 Then
    'SQ Phoenix - Pezza per VALUE su livelli 88...
    If Left(Trim(istruzione), 2) = "88" Then
      DoEvents
    Else
      If Val(firstWord) > 1 And Val(firstWord) < 66 Then
        If Trim(istruzione) <> "" Then
          ' Mauro 08/08/2007 : Pezza paurosa!
          'Se l'istruzione � COPY con una replacing, si incartava mani e piedi
          If InStr(istruzione, "REPLACING") And InStr(TextLine, "BY") Then
            'istruzione = istruzione & " " & Trim(Textline)
          Else
            ''''''''''''''''''''''''''''''''''''''
            ' SBAGLIA LE OCCURS!!!!!!!!!!!
            ' ES. RIGA: "50 TIMES"
            ''''''''''''''''''''''''''''''''''''''
            'pezza temporanea... sarebbe da cambiare tutto...
            istruzione = RTrim(istruzione)
            If Mid(istruzione, InStrRev(istruzione, " ") + 1) <> "OCCURS" Then 'ultima parola
              istruzione = istruzione & ". "
              VerFineRiga = True
              RigaSucc = TextLine
              If Not VerificaIstruzione(istruzione) Then
                istruzione = ""
                VerFineRiga = False
              End If
              Exit Function
            Else
              istruzione = istruzione & " "
            End If
          End If
        End If
      End If
    End If
  End If
   
  If InStr(Left(TextLine, 72), ".") Then
    If Mid(TextLine, Len(RTrim(Left(TextLine, 72))), 1) = "." Then
      istruzione = istruzione & Mid(TextLine & Space$(65), 8, 65)
      VerFineRiga = True
      If Not VerificaIstruzione(istruzione) Then
        istruzione = ""
        VerFineRiga = False
      End If
      Exit Function
    End If
  ElseIf InStr(Left(TextLine, 72), "END-EXEC") Then
    istruzione = istruzione & Mid(TextLine & Space$(65), 8, 65)
    VerFineRiga = True
    If Not VerificaIstruzione(istruzione) Then
      istruzione = ""
      VerFineRiga = False
    End If
    Exit Function
  End If
  
  istruzione = istruzione & Mid(TextLine & Space$(65), 8, 65)
  VerFineRiga = False
End Function

Function VerificaIstruzione(Stringa As String) As Boolean
  Dim wStr As String, k As Integer
  Dim Word1 As String, Word2 As String, Word3 As String
  
  wStr = LTrim(Stringa)
  k = InStr(wStr, " ")
  Word1 = Mid$(wStr, 1, k - 1)
  Word2 = LTrim(Mid$(wStr, k + 1))
  k = InStr(Word2, " ")
  If k > 0 Then
    Word3 = LTrim(Mid$(Word2, k + 1))
    Word2 = Mid$(Word2, 1, k - 1)
  End If
  k = InStr(Word3, " ")
  If k > 0 Then
    Word3 = Mid$(Word3, 1, k - 1)
  End If
  
  If Word2 = "LINE" Or Word3 = "LINE" Then
    VerificaIstruzione = False
    Exit Function
  End If
  If Word2 = "COLUMN" Then
    VerificaIstruzione = False
    Exit Function
  End If
  If InStr(Stringa, " REPORT ") > 0 Then
    If InStr(Stringa, " TYPE ") > 0 Then
      VerificaIstruzione = False
      Exit Function
    End If
  End If
  VerificaIstruzione = True
End Function

Function VerValue() As Boolean
  Dim y As Integer
  Const lastIndex = 380 'SQ: a cosa serve?! non esco sempre se ho uno space?
  
  Valore = ""
  SwEnd = False

  For y = Start To lastIndex
    Start = y
    'SQ - non e' che arrivino gia' trimmati?
    'If Mid(Tabword(y), Len(RTrim(Tabword(y))), 1) = "." Then
    '   SwEnd = True
    'End If
    SwEnd = Right(RTrim(Tabword(y)), 1) = "."

    If Trim(Tabword(y)) = "PIC" Or Trim(Tabword(y)) = "PICTURE" Then
      SwEnd = True
      Start = Start - 1
    End If

    If Trim(Valore) = "" Then
      If SwEnd Then
        Valore = Left(Tabword(y), Len(RTrim(Tabword(y))) - 1)
        y = lastIndex
      Else
        Valore = Tabword(y)
      End If
    Else
      If SwEnd Then
        If Left(Trim(Tabword(y)), 3) <> "PIC" Then
          'SQ - apparte il fatto che sta routine � orrenda...
          'gestione VALUE di questo tipo: " PIC 9(5) VALUE +4 COMP."
          If Left(Tabword(y), 4) <> "COMP" Then
            Valore = Valore & " " & Left(Tabword(y), Len(RTrim(Tabword(y))) - 1)
          Else
            'Proviamo farlo rientrare in carreggiata:
            Start = Start - 1
            SwEnd = False
          End If
        Else
          SwEnd = False
        End If
        y = lastIndex
      Else
        Valore = Valore & Space(1) & Tabword(y)
      End If
    End If
  Next y

  VerValue = Len(Trim(Valore))
End Function
'**********************************
'Verifica se la riga � un commento
'**********************************
Function VerCommento(TextLine) As Boolean
  If Mid(TextLine, 7, 1) = "*" Or Mid(TextLine, 7, 1) = "/" Then
    VerCommento = True
  ElseIf Trim(Mid$(TextLine, 8, 50)) = "EJECT" Then
    VerCommento = True
  ElseIf Mid$(Trim(Mid$(TextLine, 8, 70)), 1, 4) = "SKIP" Then
    VerCommento = True
  ElseIf InStr(TextLine, "COPY") > 0 Then
  ''''''''''''''''''''''''''''''''''''''''''''
  ' SQ - 10-03-06 ??????????????????????
  ' 01 CICCIO. COPY BLUTO.  (li butta via!)
  ''''''''''''''''''''''''''''''''''''''''''''
'  If InStr(Textline, "COPY") > 0 Then
'    If InStr(Textline, ".") > 0 Then
'      If Left(LTrim(Mid(Textline, 8, 50)), 2) = "01" Then
'        VerCommento = True
'        Exit Function
'      End If
'    End If
'  End If
    If InStr(TextLine, ".") > 0 Then
      Dim statementS() As String
      statementS = Split(TextLine, ".")
      If UBound(statementS) = 2 Then
        TextLine = statementS(0) & "."
        statementS(1) = Trim(statementS(1))
        If Left(statementS(1), 4) = "COPY" Then
          RigaSucc = Space(8) & statementS(1) & "."
          ''''''''''''''''''''''''
          '
          ''''''''''''''''''''''''
          GbNumRec = wNumRec
          MapsdM_COBOL.getCOPY (Mid(statementS(1), 5))  'devo mangiare COPY per getCopy
        End If
      End If
    End If
  End If

End Function
Function VerTipo() As Boolean
  '****************************************
  'Verifica la tipologia del DataItem e segno
  '****************************************
  Segno = ""
  Tipo = ""
  
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
     SwEnd = True
  Else
     SwEnd = False
  End If
  If Mid(Tabword(Start), 1, 1) = "S" Or Mid(Tabword(Start), 1, 1) = "-" Or Mid(Tabword(Start), 1, 1) = "+" Then
    Segno = Mid(Tabword(Start), 1, 1)
    ' Mauro 28-02-2007: Potrei avere il caso "SV9(n)"
    If UCase(Mid(Tabword(Start), 2, 1)) = "V" Or UCase(Mid(Tabword(Start), 2, 1)) = "," Then
      Tipo = Mid(Tabword(Start), 3, 1)
    Else
      Tipo = Mid(Tabword(Start), 2, 1)
    End If
    'Tipo = "9"
  Else
   Segno = "N"
   Select Case Mid(Tabword(Start), 1, 1)
     Case "9", "V", ".", ","
       Tipo = "9"
     Case "X", "Z", "B", "A", "*", "x", "a", "z", "b", "$"
       Tipo = "X"
     Case Else
       VerTipo = False
       Exit Function
     End Select
  End If
  VerTipo = True
  OldSegno = Segno
  OldTipo = Tipo
End Function
Function VerLen() As Boolean
  Dim y As Integer
  Dim SwDec As Boolean
  
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
     SwEnd = True
  Else
     SwEnd = False
  End If
  '********************************
  'Verifica la lunghezza del DataItem
  '********************************
  Interi = 0
  Decimali = 0
  SwDec = False
  DatPicture = Trim(Tabword(Start))
  If Len(DatPicture) > 0 Then
     If Mid$(DatPicture, Len(DatPicture), 1) = "." Then
        Mid$(DatPicture, Len(DatPicture), 1) = " "
        DatPicture = Trim(DatPicture)
     End If
  End If
  For y = 1 To Len(Tabword(Start))
    Select Case Mid(Tabword(Start), y, 1)
           Case "S", "s"
           Case "9", "0"
              If SwDec = False Then
                 Interi = Interi + 1
              Else
                 Decimali = Decimali + 1
              End If
           Case "X", "A", "B", "x", "a", "b", "$"
              Interi = Interi + 1
           Case "V", "v"
              SwDec = True
           Case "("
              If SwDec = False Then
                 Interi = Interi - 1
                 Interi = Interi + Val(Mid(Tabword(Start), y + 1, InStr(Tabword(Start), ")") - 1))
                 y = InStr(y + 1, Tabword(Start), ")")
              Else
                 Decimali = Decimali - 1
                 Decimali = Decimali + Val(Mid(Tabword(Start), y + 1, InStr(Tabword(Start), ")") - 1))
                 y = InStr(y + 1, Tabword(Start), ")")
              End If
           Case ")"
           Case ","
              SwDec = True
              Usage = "EDT"
           Case "/"
           Case "."
              If y = Len(RTrim(Tabword(Start))) Then
              Else
                 Interi = Interi + 1
                 Usage = "EDT"
              End If
           Case "-", "+"
              Interi = Interi + 1
              Usage = "EDT"
           Case "Z", "*"
              If SwDec = False Then
                  Interi = Interi + 1
              Else
                  Decimali = Decimali + 1
              End If
              Usage = "EDT"
           Case Else
              If SwDec Then
                 Decimali = Val(Mid(Tabword(Start), y, 1))
              Else
                VerLen = False
                Exit Function
              End If
    End Select
  Next y
  VerLen = True
  OldLen = Interi
  OldDec = Decimali

End Function
Function VerOccurs() As Boolean
  '*****************************
  'Verifica la occurs del DataItem
  '*****************************
  Occurs = 0
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    'SQ - problema "language settings:"
    '     es: 18. => ok per noi; ko per francesi (per es.): hanno la virgola...
    Tabword(Start) = Left(Tabword(Start), Len(Tabword(Start)) - 1)
    SwEnd = True
  Else
    SwEnd = False
  End If
  If Not IsNumeric(Tabword(Start)) Then
     VerOccurs = False
  Else
  'If Val(Tabword(Start)) = 0 Then
  '   VerOccurs = False
  '   Exit Function
  'Else
     Occurs = Val(Tabword(Start))
     VerOccurs = True
  End If
End Function
Function VerOccursTo() As Boolean
  '*****************************
  'Verifica la occurs del DataItem
  '*****************************
  OccursTo = 0
  If Mid(Tabword(Start), Len(RTrim(Tabword(Start))), 1) = "." Then
    'SQ - vedi VerOccurs() se da lo stesso problema con il francese...
     SwEnd = True
  Else
     SwEnd = False
  End If
  If Val(Tabword(Start)) = 0 Then
     VerOccursTo = False
     Exit Function
  Else
     OccursTo = Val(Tabword(Start))
  End If
  VerOccursTo = True
End Function

Sub AggSegn(wStringa, wtipo)
  Dim wVar1 As String, wVar2 As String, wCodice As String, wPeso As String
  Dim xPeso As String
  Dim TbSegnalazioni As Recordset
  Dim rs As Recordset
  
  Glist.ListItems.Add , , (MigrNomeFile & ": errore nella definizione (" & wtipo & ") " & Tabword(Start))
  Glist.Refresh
  
  Set TbSegnalazioni = m_Fun.Open_Recordset("select * from Bs_Segnalazioni where idoggetto = " & wIdOgg)
  wVar1 = Space$(1)
  wVar2 = Space$(1)
  Select Case wtipo
   Case "NOCLAUSOLA"
      wVar1 = wStringa
      wCodice = "L08"
      wPeso = "E"
   Case "NOLIVELLO"
      wVar1 = wStringa
      wCodice = "L09"
      wPeso = "E"
   Case "NONOME"
      wVar1 = wStringa
      wCodice = "L10"
      wPeso = "E"
   Case "OCCURS", "OCCURS TO"
      wVar1 = wStringa
      wCodice = "L11"
      wPeso = "E"
   Case "REDEFINE"
      wVar1 = wStringa
      wCodice = "L12"
      wPeso = "E"
   Case "USAGE", "USAGE IS"
      wVar1 = wStringa
      wCodice = "L13"
      wPeso = "E"
   Case "VALUE"
      wVar1 = wStringa
      wCodice = "L14"
      wPeso = "I"
   Case "PICTURE"
      wVar1 = wStringa
      wCodice = "L15"
      wPeso = "E"
   Case "LUNGHEZZA"
      wVar1 = wStringa
      wCodice = "L16"
      wPeso = "E"
   Case "COMP"
      wVar1 = wStringa
      wCodice = "L17"
      wPeso = "E"
   Case "COMP-3"
      wVar1 = wStringa
      wCodice = "L18"
      wPeso = "E"
   Case "POINTER"
      wVar1 = wStringa
      wCodice = "L21"
      wPeso = "E"
   Case "ON"
      wVar1 = wStringa
      wCodice = "L19"
      wPeso = "E"
  End Select
  On Error Resume Next
  
  If PsHideIgnore Then
    Set rs = m_Fun.Open_Recordset( _
      "select * from Bs_Segnalazioni_Ignore where code='" & wCodice & "' and var1 = 'ALL'")
    If rs.EOF Then
      rs.Close
      Set rs = m_Fun.Open_Recordset( _
        "select * from Bs_Segnalazioni_Ignore where code='" & wCodice & "' and var1='" & Replace(wVar1, "'", "''") & "'")
    End If
    If Not rs.EOF Then
      rs.Close
      TbSegnalazioni.Close
      Exit Sub
    End If
    rs.Close
  End If
  TbSegnalazioni.AddNew
  TbSegnalazioni!idOggetto = wIdOgg
  TbSegnalazioni!Riga = wNumRec
  TbSegnalazioni!codice = wCodice
  TbSegnalazioni!Origine = "PS"
  TbSegnalazioni!data_segnalazione = Now
  TbSegnalazioni!Var1 = wVar1
  TbSegnalazioni!var2 = wVar2
  TbSegnalazioni!gravita = wPeso
  TbSegnalazioni!Tipologia = "DATI"
  TbSegnalazioni.Update
  
  TbSegnalazioni.Close
  
  Parser.SettaErrLevel wPeso, "DATI"
End Sub

Public Function CopyIsChildren(wNomeCopy As String) As Boolean
  'SG : Questa routine controlla che
  'effettivamente la copy deve cominciare con un livello che non sia 01
  'Solo in questo caso la copy fa parte veramente di un livello esterno superiore
   Dim rs As Recordset
   Dim wPath As String
   Dim sLine As String
   Dim fLen As Long
   Dim nFile As Long
   Dim firstWord As String
   
  Set rs = m_Fun.Open_Recordset("Select IdOggetto From BS_Oggetti Where Nome = '" & wNomeCopy & "' And Tipo = 'CPY'")
  If rs.RecordCount Then
    wPath = m_Fun.Restituisci_Directory_Da_IdOggetto(rs!idOggetto) & "\" & m_Fun.Restituisci_NomeOgg_Da_IdOggetto(rs!idOggetto)
    'SQ: perche' apre il file? se la conosco gia'...
    nFile = FreeFile
    Open wPath For Binary As #nFile
    fLen = FileLen(wPath)
    While Loc(nFile) < fLen
       Line Input #nFile, sLine
       If Mid(sLine, 7, 1) <> "*" Then
          sLine = Mid(sLine, 1, 72)
          sLine = LTrim(Mid(sLine, 8))
          firstWord = Mid(sLine, 1, InStr(1, sLine, " "))
          If IsNumeric(firstWord) Then
             If Val(firstWord) = 1 Then
                CopyIsChildren = False
             Else
                CopyIsChildren = True
             End If
             Close #nFile 'ALE 4/11/2005
             Exit Function
          End If
       End If
    Wend

   Close #nFile
  End If
  rs.Close
End Function
''''''''''''''''''''''''''''''''''''''''''
' Buttare via il PrendiValue
''''''''''''''''''''''''''''''''''''''''''
Function getFieldValue(fieldName As String) As String
  Dim rs As Recordset
  
  'IRIS-DB: provo a gestire le costanti che venivano trasformate in variabili precedentemente
  If InStr(fieldName, "'") > 0 Then
    getFieldValue = Replace(fieldName, "'", "")
    Exit Function
  End If
  'SQ: TMP
  If True Then
    getFieldValue = getFieldValue_new(fieldName)
    Exit Function
  End If
  
  Set rs = m_Fun.Open_Recordset("SELECT valore From PsData_Cmp Where idOggetto = " & GbIdOggetto & " And nome = '" & fieldName & "'" & _
                                " UNION " & _
                                "SELECT a.valore From PsData_Cmp as a,PsRel_Obj as b" & _
                                "  where b.idOggettoC = " & GbIdOggetto & " and b.relazione='CPY' And a.IdOggetto=b.IdOggettoR and a.nome = '" & fieldName & "'")
  If rs.RecordCount Then
    Select Case Trim(rs!Valore) 'L'ANALISI DEI DATI APPENDE UNO SPACE IN FONDO!!!!!!!!!!!
      Case "SPACE", "SPACES"
        getFieldValue = ""
      Case "ZERO", "ZEROES", "ZEROS"
        getFieldValue = "0"
      Case Else
        getFieldValue = Replace(rs!Valore, "'", "")
        getFieldValue = Trim(Replace(getFieldValue, """", ""))
    End Select
  End If
  rs.Close
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Dovr� sostituire il getFieldValue;
' Gestione REPLACING nelle COPY;
' Ottimizzazione ricerca: utilizzo "cache" (DataValues)
' Per i campi interni all'oggetto stesso, la DataValues
' DOVRA' essere valorizzata direttamente all'analisi dei
' campi... � lo stesso giro!
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function getFieldValue_new(ByVal fieldName As String) As String
  Dim rs As Recordset, rsCopy As Recordset
  Dim i As Long
  Dim dataField(2) As String
  Dim PIPPO()  As String
  Dim relatedType As String
  Dim IsCampoFound As Boolean
  
  On Error GoTo catch
  
  'Campo gi� analizzato?:
  ' stefano: datavalues a volte � vuoto... da rivedere
  For i = 1 To UBound(DataValues)
    If fieldName = DataValues(i)(0) Then
      getFieldValue_new = DataValues(i)(1)
      Exit Function
    End If
  Next i
  
  'SQ 22-11-06 (Da estendere?)
  'PLI: HO L'ARRAY IN MEMORIA (E NON ANCORA IN TABELLA!)
  If i > UBound(DataValues) And (GbTipoInPars = "PLI" Or GbTipoInPars = "INC") Then
  'IRIS-DB: workaround veloce: non gestisce i campi referenziati alla struttura con il punto
  '   per ora rimossa la parte dell'area, ovviamente non funzionerebbe con campi con lo stesso nome
  '   in aree diverse, ma per svilupparlo bene ci vuole troppo tempo, manca tutta la gestione dell'accesso alla copy
    fieldName = Right(fieldName, Len(fieldName) - (InStr(fieldName, ".")))
    relatedType = "INC"
    For i = 0 To Idx
      If DataItem(i).DatNomeFld = fieldName Then
        getFieldValue_new = DataItem(i).DatVal
        Exit For
      End If
    Next i
    'Deve entrare sotto:
    If i > Idx Then i = UBound(DataValues) + 1
  Else
    'COBOL/CPY...
    relatedType = "CPY"
  End If
  
  If i > UBound(DataValues) Then
    dataField(0) = fieldName
    ReDim Preserve DataValues(UBound(DataValues) + 1)
    
    Set rs = m_Fun.Open_Recordset( _
      "SELECT valore FROM PsData_Cmp WHERE idOggetto=" & GbIdOggetto & " And nome='" & fieldName & "'")
    If rs.EOF Then
      'ricerca in COPY:
      'SQ 12-10-2007
      'Gestione istruzioni in COPY: devo cercare sui programmi!
      Dim idpgm As Long
      If GbTipoInPars = "CPY" Then  'farlo anche per le "INC"
        'Cpy: ricerca nei programmi!
        Set rsCopy = m_Fun.Open_Recordset( _
          "SELECT idOggettoR,nomeComponente,idOggettoC as idPgm From PsRel_Obj WHERE relazione='" & relatedType & "' And idOggettoR=" & GbIdOggetto & " ORDER BY idOggettoC")
      Else
        Set rsCopy = m_Fun.Open_Recordset( _
          "SELECT idOggettoR,nomeComponente," & GbIdOggetto & " as idPgm From PsRel_Obj WHERE relazione='" & relatedType & "' And idOggettoC=" & GbIdOggetto & " ORDER BY idOggettoC")
      End If
      While Not rsCopy.EOF And Not IsCampoFound
        'Replacing?
        rs.Close
        Set rs = m_Fun.Open_Recordset( _
          "SELECT replacingVar,replacingValue FROM PsPgm_Copy WHERE idPgm=" & rsCopy!idpgm & " And copy='" & rsCopy!nomecomponente & "'")
        If rs.RecordCount Then
          'REPLACING!
          Dim replacingValue As String, replacingVar As String
          replacingValue = rs!replacingValue & ""
          replacingVar = rs!replacingVar & ""
        Else
          replacingValue = ""
          replacingVar = ""
        End If
        rs.Close
        If Len(replacingVar) Then
          'Non filtro sul nome: lo calcolo sotto!
          Set rs = m_Fun.Open_Recordset("SELECT nome,valore FROM PsData_Cmp WHERE idOggetto=" & rsCopy!idOggettoR)
          Do While Not rs.EOF
            If Replace(rs!nome, replacingVar, replacingValue) = fieldName Then
              'Trovato il campo!
              Exit Do
            End If
            rs.MoveNext
          Loop
        Else
          'SQ 12-10-2007
          'Gestione istruzioni in COPY: devo cercare sui programmi!
          If GbTipoInPars = "CPY" Then  'farlo anche per le "INC"
            'Cpy: ricerca nei programmi!
            Set rs = m_Fun.Open_Recordset( _
              "SELECT valore FROM PsData_Cmp WHERE idOggetto=" & rsCopy!idpgm & " And nome='" & fieldName & "'")
          Else
            Set rs = m_Fun.Open_Recordset( _
              "SELECT valore FROM PsData_Cmp WHERE idOggetto=" & rsCopy!idOggettoR & " And nome='" & fieldName & "'")
          End If
        End If
        'Se trovo un rs esce dal ciclo.
        If Not rs.EOF Then
          IsCampoFound = True
        End If
        rsCopy.MoveNext
      Wend
      rsCopy.Close
    End If
    'Ac 03/10/07
    If rs.EOF Then
    ' cerco nella PsData_Cmp utilizzando la copy di secondo livello
      rs.Close
      Set rs = m_Fun.Open_Recordset( _
      "SELECT a.valore From PsData_Cmp as a,PsRel_Obj as b,PsRel_Obj as c " & _
      " WHERE b.idOggettoC=" & GbIdOggetto & " and b.relazione='" & relatedType & "' and b.IdOggettoR = c.IdOggettoC And a.IdOggetto=c.IdOggettoR and a.nome='" & fieldName & "'")
    End If
    
    ' Mauro 14/01/2009
    If rs.EOF Then
      ' cerco nella PsData_Cmp utilizzando la copy di working
      rs.Close
      Set rs = m_Fun.Open_Recordset( _
      "SELECT a.valore From PsData_Cmp as a,PsRel_Obj as b,PsRel_Obj as c " & _
      " WHERE b.idOggettoR=" & GbIdOggetto & " and b.relazione='" & relatedType & "' and b.IdOggettoC = c.IdOggettoC And a.IdOggetto=c.IdOggettoR and a.nome='" & fieldName & "'")
    End If
    
    If Not rs.EOF Then
      getFieldValue_new = Trim(rs!Valore) 'L'ANALISI DEI DATI APPENDE UNO SPACE IN FONDO!!!!!!!!!!!
    End If
    rs.Close
  End If
  
  Select Case getFieldValue_new
    Case "SPACE", "SPACES"
      getFieldValue_new = ""
    Case "ZERO", "ZEROES", "ZEROS"
      getFieldValue_new = "0"
    Case ""
      'nop
      'IRIS-DB: bisognerebbe fare molto meglio: se un campo ha sottolivelli o sopralivelli assume il valore di quelli (deve combinare o splittare)
      Set rs = m_Fun.Open_Recordset( _
      "SELECT IdArea, Livello FROM PsData_Cmp WHERE idOggetto=" & GbIdOggetto & " And nome='" & fieldName & "'")
      Dim idArea, livello As Integer
      Dim FlagOk As Boolean
      If Not rs.EOF Then
        idArea = rs!idArea
        livello = rs!livello
        FlagOk = False
        rs.Close
        Set rs = m_Fun.Open_Recordset( _
        "SELECT Nome, Livello, Tipo, Valore, NomeRed, Occurs From PsData_Cmp " & _
        "WHERE idOggetto=" & GbIdOggetto & " and IdArea = " & idArea & " order by Ordinale")
        Do While Not rs.EOF
          If rs!nome = fieldName Then
            FlagOk = True
          Else
            If FlagOk Then
              If rs!livello <= livello Then
                Exit Do
              End If
              'IRIS-DB: per ora solo per gli alfanumerici, e scarta occurs e redefines
              If rs!Tipo <> "X" Or Trim(rs!nomeRed) <> "" Or rs!Occurs > 0 Then
                getFieldValue_new = ""
                Exit Do
              End If
              'IRIS-DB: per ora non gestisce lunghezze (dovrebbe paddare a spaces in caso di lunghezza valore inferiore della lunghezza del campo)
              getFieldValue_new = getFieldValue_new & Trim(Replace(Replace(rs!Valore, "'", ""), """", ""))
            End If
          End If
          rs.MoveNext
        Loop
        rs.Close
      Else
        rs.Close
      End If
    Case Else
      getFieldValue_new = Replace(getFieldValue_new, "'", "")
      getFieldValue_new = Trim(Replace(getFieldValue_new, """", ""))
  End Select
  
  'Aggiornamento "cache":
  dataField(1) = getFieldValue_new
  DataValues(UBound(DataValues)) = dataField
  
  Exit Function
catch:
  m_Fun.writeLog "###getFieldValue: " & err.description
  Exit Function
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ritorna l'ID della COPY se "interna" (non livello 01);
' Ritorna 0 altrimenti
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getCopyID(copyname As String) As Long
  Dim r As Recordset
  Dim idOggetto As Long
  
  Set r = Open_Recordset("Select idOggetto From BS_Oggetti Where Nome = '" & copyname & "' And Tipo = 'CPY'")
  If r.RecordCount Then
    idOggetto = r!idOggetto
    Set r = Open_Recordset("Select idArea,ordinale,livello From PsData_Cmp Where idOggetto=" & idOggetto & " ORDER BY idArea,ordinale")
    If r.RecordCount Then
      'Livello 01? (copy "esterna"?)
      If r!livello = 1 Then idOggetto = 0
    Else
      'Parser.PsFinestra.ListItems.Add , , "There is no parser of COPY : " & copyname & " - ID: " & idOggetto
      idOggetto = 0
    End If
  End If
  r.Close
  getCopyID = idOggetto
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ritorna il numero di bytes della COPY se "interna" (non livello 01);
' Ritorna 0 altrimenti
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getCopyDigit(idCopy As Long) As Long
  Dim r As Recordset, Digit As Long, livello As Long
  
  Set r = Open_Recordset("Select idArea,ordinale,livello From PsData_Cmp " & _
                         "Where idOggetto=" & idCopy & " ORDER BY idArea,ordinale")
  If r.RecordCount Then
    If r!livello > 1 Then
      livello = r!livello
      Set r = Open_Recordset("Select sum(byte) as somma From PsData_Cmp Where " & _
                             "idOggetto=" & idCopy & " and livello = " & livello)
      If r.RecordCount Then
        Digit = r!somma
      End If
    End If
  End If
  r.Close
  getCopyDigit = Digit
End Function

Public Function TypeFromId(pId As Long) As String
  Dim rs As New Recordset
  
  Set rs = m_Fun.Open_Recordset("select tipo from Bs_Oggetti where idoggetto = " & pId)
  If Not rs.EOF Then
    TypeFromId = rs!Tipo
  Else
    TypeFromId = ""
  End If
End Function
