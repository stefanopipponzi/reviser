Attribute VB_Name = "MapsdM_TSO"
Option Explicit
Option Compare Text
'''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''
Public Sub parseISPF()
  Dim FD As Long
  Dim line As String, token As String, execStatement As String, statement As String
  Dim rsOggetti As Recordset
  
  On Error GoTo parseErr
  
  ReDim CmpIstruzione(0)
  'ReDim assign_files(0) 'SELECT/ASSIGN
  ReDim CampiMove(0)
  
  ''''''''''''''''''''''''''''''
  ' Init
  ''''''''''''''''''''''''''''''
  'reserverdWords = ...
  
  GbNumRec = 0    'init
  ''idArea = 0
  
  FD = FreeFile
  Open GbFileInput For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    
    'Costruzione statement: gestione commenti+multilinea
    statement = getStatement(FD, line)
        
    'LINEA VALIDA:
    token = nextToken_new(statement) 'MANTIENE GLI APICI!!
    Select Case token
      Case ")ATTR"
        line = ""
      Case ")BODY"
        line = ""
      Case ")MODEL"
        line = ""
      Case ")INIT"
        line = ""
      Case ")END"
        line = ""
      Case ""
        'nop
      Case Else
        line = ""
    End Select
  Wend
  Close FD

  Set rsOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  rsOggetti!DtParsing = Now
  rsOggetti!ErrLevel = GbErrLevel
  rsOggetti!parsinglevel = 1
  rsOggetti!Cics = SwTpCX
  rsOggetti!Batch = SwBatch
  rsOggetti!DLI = SwDli 'sarebbe IMS!
  rsOggetti!WithSQL = SwSql
  rsOggetti!VSam = SwVsam
  rsOggetti!mapping = SwMapping
  rsOggetti.Update
  rsOggetti.Close
  Exit Sub
parseErr:  'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          GbOldNumRec = GbNumRec
          addSegnalazione line, "P00", line
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        GbOldNumRec = GbNumRec
        addSegnalazione line, "P00", line
        'm_Fun.WriteLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & Err.Description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "parseISPF: id#" & GbIdOggetto & ", ###" & err.Description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
    m_Fun.writeLog "parseISPF: id#" & GbIdOggetto & ", ###" & err.Description
  Else
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''
Public Sub parseCLIST()
  Dim FD As Long
  Dim line As String, token As String, execStatement As String, statement As String, parameters As String
  Dim rsOggetti As Recordset
  
  On Error GoTo parseErr
  
  ReDim CmpIstruzione(0)
  'ReDim assign_files(0) 'SELECT/ASSIGN
  ReDim CampiMove(0)
  
  ''''''''''''''''''''''''''''''
  ' Init
  ''''''''''''''''''''''''''''''
  'reserverdWords = ...
  
  GbNumRec = 0    'init
  ''idArea = 0
  
  FD = FreeFile
  Open GbFileInput For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    
    'Costruzione statement: gestione commenti+multilinea
    statement = getStatement(FD, line)
        
    'LINEA VALIDA:
    token = nextToken_new(statement) 'MANTIENE GLI APICI!!
    Select Case token
      Case "CALL"
        getCALL statement
      Case "ISPEXEC"
        parseISPEXEC token, statement
        line = ""
      Case "ISPEXEC"
        line = ""
      Case "ISPEXEC"
        line = ""
      Case "ISPEXEC"
        line = ""
      Case ""
        'nop
      Case Else
        line = ""
    End Select
  Wend
  Close FD

  Set rsOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  rsOggetti!DtParsing = Now
  rsOggetti!ErrLevel = GbErrLevel
  rsOggetti!parsinglevel = 1
  rsOggetti!Cics = SwTpCX
  rsOggetti!Batch = SwBatch
  rsOggetti!DLI = SwDli 'sarebbe IMS!
  rsOggetti!WithSQL = SwSql
  rsOggetti!VSam = SwVsam
  rsOggetti!mapping = SwMapping
  rsOggetti.Update
  rsOggetti.Close
  Exit Sub
parseErr:  'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          GbOldNumRec = GbNumRec
          addSegnalazione line, "P00", line
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        GbOldNumRec = GbNumRec
        addSegnalazione line, "P00", line
        'm_Fun.WriteLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & Err.Description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "parseCLIST: id#" & GbIdOggetto & ", ###" & err.Description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
    m_Fun.writeLog "parseCLIST: id#" & GbIdOggetto & ", ###" & err.Description
  Else
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub parseISPLINK(program As String, statement As String)
  Dim line As String, token As String, service As String
  Dim rs As Recordset
  
  On Error GoTo catch
  
  service = nexttoken(statement)

  Set rs = m_Fun.Open_Recordset("select * from PsISPF_Call")
  rs.AddNew
  rs!idOggetto = GbIdOggetto
  rs!Riga = GbOldNumRec
  rs!programma = program
  rs!service = service
  rs!parametri = Trim(statement)
  rs!linee = GbNumRec - GbOldNumRec + 1
  rs.Update
  rs.Close
  
  Select Case service
    Case "TBOPEN", "TBCLOSE" '...
    Case "CONTROL"  '...
    Case "DISPLAY"  '...
      parseDISPLAY statement
    Case "TBDISPL"  '...
      parseTBDISP statement
  End Select
  
  Exit Sub
catch:
  MsgBox "tmp: " & err.Description
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Unificare con ISPLINK... vedere alla fine...
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub parseISPEXEC(program As String, statement As String)
  Dim line As String, token As String, service As String
  Dim rs As Recordset
  
  On Error GoTo catch
  
  service = nexttoken(statement)

  Set rs = m_Fun.Open_Recordset("select * from PsISPF_Call")
  rs.AddNew
  rs!idOggetto = GbIdOggetto
  rs!Riga = GbOldNumRec
  rs!programma = program
  rs!service = service
  rs!parametri = Trim(statement)
  rs!linee = GbNumRec - GbOldNumRec + 1
  rs.Update
  rs.Close
  
  Select Case service
    Case "DISPLAY"  '...
      parseDISPLAY statement
    Case "TBDISP"
      parseTBDISP statement
    Case "TBOPEN", "TBCLOSE" '...
    Case "CONTROL"  '...
    
      
  End Select
  
  Exit Sub
catch:
  MsgBox "tmp: " & err.Description
End Sub
Private Function getStatement(FD As Long, alreadyReadLine As String) As String
  Dim line As String
  
  'On Error GoTo errToken
  
  If Len(alreadyReadLine) Then
    'gia' letta dall'istruzione precedente
    line = alreadyReadLine
    alreadyReadLine = "" 'pulisco per i giri dopo
  Else
    Line Input #FD, line
    GbNumRec = GbNumRec + 1
    line = Trim(Left(line, 72))
    If EOF(FD) Then
      'Ultima linea:
      getStatement = line
    End If
    GbOldNumRec = GbNumRec
  End If
  
  Do While Not EOF(FD)
    If Left(line, 1) <> "*" Then
      getStatement = getStatement & line
      'CONTINUATIONS:
      If Right(line, 1) = "+" Or Right(line, 1) = "-" Then
        getStatement = Left(getStatement, Len(getStatement) - 1)
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        line = RTrim(Left(line, 72))
        If Right(line, 1) = "+" Then
          line = Trim(line)
        End If
        'getStatement = getStatement & line
      Else
        'Attenzione: gestire il "." come fine statement!
        'Es.: DEFINE <...>   . *COMMENTI....
        'Provvisoria:
        Dim endStatement As Integer
        endStatement = InStr(getStatement, ".")
        If endStatement Then
          'Non lo chiamo sempre per far prima!
          '''splitStatement getStatement
          'line = Trim(Mid(getStatement, endStatement + 1))  'appoggio
          'If Left(line, 1) = "*" Then
          '  getStatement = Trim(Left(getStatement, endStatement - 1))
          'End If
        End If
        Exit Do
      End If
    Else
      'COMMENTS:
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      line = Trim(Left(line, 72))
    End If
  Loop
End Function
'
Private Sub parseDISPLAY(statement As String)
  Dim panel As String
    
  panel = nexttoken(statement)
  If panel = "PANEL" Then 'ISPEXEC
    panel = nexttoken(statement)
    panel = Replace(Replace(panel, "(", ""), ")", "")
  End If
  
  MapsdM_Parser.checkRelations panel, "PANEL"
  
End Sub
Private Sub getCALL(parameters As String)
  Dim module As String
  Dim arguments As String
  Dim statement As String
  
  On Error GoTo nextTokenErr
  
  'Called Module
  SwNomeCall = True
  module = nextToken_tmp(parameters)
  
  ' Mauro 14/10/2009: TEMP!!!!!!!!!!!!! Manca il conteggio delle linee e la gestione dei parametri
  '''''''''''''''''''''''''''''''''''''''''''''''''
  ' Inserimento istruzione in PsCall: (trasformare in execute INSERT)
  '''''''''''''''''''''''''''''''''''''''''''''''''
  Dim rsCall As Recordset
  Set rsCall = m_Fun.Open_Recordset("select * from PsCall where IdOggetto = " & GbIdOggetto & " and riga = " & GbOldNumRec _
                                    & " and  Programma = '" & module & "' and PArametri = '" & Replace(Replace(nexttoken(parameters), ")", ""), "(", "") _
                                    & "' and linee = 1")
  If rsCall.EOF Then
    rsCall.AddNew
    rsCall!idOggetto = GbIdOggetto
    rsCall!Riga = GbOldNumRec
    rsCall!programma = module  'attenzione: puo' contenere apici...
    rsCall!parametri = Replace(Replace(nexttoken(parameters), ")", ""), "(", "")
    rsCall!linee = 1
    rsCall.Update
    rsCall.Close
  End If

  'ASSESSMENT:
  ''''''''''''''''''''''''''''''''''''''
  ' Relazioni/Segnalazioni
  ''''''''''''''''''''''''''''''''''''''
  MapsdM_Parser.checkRelations module, "CALL"
  
  
  ''''''''''''''
  'MIGRAZIONE:
  ''''''''''''''
  'statement = indent & "CALL '" & module & "'"
  
  'Parameters
  If Len(parameters) Then
    arguments = Replace(Replace(nexttoken(parameters), ")", ""), "(", "")
    'statement = statement & " USING" & vbCrLf & indent & "     " & arguments
    statement = statement & " USING " & arguments
  End If
    
  'tagWrite statement
  
  Exit Sub
nextTokenErr:
  'tmp
  MsgBox err.Description
End Sub
'
Private Sub parseTBDISP(statement As String)
  Dim panel As String, table As String
    
  table = Trim(nexttoken(statement))  'fra apici
  'TMP:
  AggiungiComponente table, GbNomePgm, "ISP", "TABLE"
  
  If Len(statement) Then
    panel = Trim(nexttoken(statement))  'fra apici
    
    'If panel = "PANEL" Then 'ISPEXEC
    '  panel = nextToken(statement)
    '  panel = Replace(Replace(panel, "(", ""), ")", "")
    'End If
    
    MapsdM_Parser.checkRelations panel, "PANEL"
  Else
    'OK: parametro opzionale (SCROLL...)
  End If
End Sub
