Attribute VB_Name = "MapsdM_IMS"
Option Explicit
Option Compare Text

Global ssaCache() As t_ssa
Global TabIstrSSa() As t_ssa  'info dei vari livelli di SSA di una istruzione
Global IsDC As Boolean 'singola istruzione
Global SwSegm As String '????
Global wDliSsa() As String
Global itemValue As String 'CBLTDLI/AIBTDLI/...
Global psbInstr() As Long
Global psbPgm() As Long
Global dbdPgm() As Long 'cambiarci nome...
Global IsValid As Boolean
Global IdPsb As Long
Global gbCurEntryPCB() As String 'PCB Definiti nella ENTRY USING PCB
Global wIdDBD() As Long
Global IdSegmenti() As String
Global gSSA() As DefSSA
Global IndSSA As Integer
Global TabIstr As istrDli
Global GbXName  As Boolean

Dim wNomeCmp As String, wDliRec As String
Dim MaxSeg As Integer

Type DefSSA
  Name As String
  Segm() As String
  CodCom() As String
  parentesi() As String
  FIELD() As String
  Op() As String
  value() As String
  Length As Integer
End Type

Type BlkIstrKey
  Key As String
  Key_moved As Boolean
  xName As Boolean
  NameAlt As String
  Op As String
  Op_moved As Boolean
  KeyVal As String
  KeyLen As String
  KeyBool As String
  KeyBool_moved As Boolean
  KeyNum As Integer
  KeyVarField As String
  KeyStart As Long
End Type

Type blkIstrDli
  SSAName As String
  moved As Boolean
  SsaLen As Integer
  Segment As String
  Varsegment As String
  'Mauro 19-03-2007
  Segmenti As String
  segment_moved As Boolean
  Tavola As String
  IdSegm As Double
  'Mauro 19-03-2007
  IdSegmenti As String
  Record As String
  SegLen As String
  'Mauro 19-03-2007
  SegmentiLen As String
  CodCom As String
  CodCom_moved As Boolean
  qualified As Boolean
  qualified_moved As Boolean
  qual_unqual As Boolean
  Search As String
  NomeRout As String
  valida As Boolean
  KeyDef() As BlkIstrKey
  AreaLen As Integer
End Type

Type istrDli
  DBD As String
  DBD_TYPE As e_DBDType
  IstrType As e_IstrType
  pcb As String
  numpcb As Long
  istr As String
  psbName As String
  psbId As Double
  keyFeed As String
  keyFeedLen As String 'IRIS-DB
  BlkIstr() As blkIstrDli
  ' Mauro 21/04/2008 :  Gestione KEYS
  KeyArg As String
  ' Mauro 22/01/2009 :  PCB Multipli
  Multi_NumPCB  As String
End Type

'Template per i PCB
Type t_PcbStructDett
  wPicture As String
  wtipo As String
  wLunghezza As String
  wSegno As String
  wUsage As String
End Type

Type t_PcbStruct
  FixedLength As Long
  Details() As t_PcbStructDett
End Type

Type t_PcbTemplate
  IO_Pcb As t_PcbStruct
  ALT_Pcb As t_PcbStruct
  STD_Pcb As t_PcbStruct
End Type

Enum e_Recfm_gisam
  REC_FIXED_LENGTH = 0 ^ 2 'F
  REC_FIXED_LENGTH_AND_BLOCKED = 1 ^ 2 'FB
  REC_VARIABILE_LENGTH = 2 ^ 2 'V
  REC_VARIABILE_LENGTH_AND_BLOCKED = 3 ^ 2 'VB
  REC_UNDEFINED_LENGTH = 4 ^ 2 'U
  REC_UNKNOWED_LENGTH = 5 ^ 2
End Enum

Enum e_DBDType
  DBD_GSAM = 0 ^ 2
  DBD_HDAM = 1 ^ 2
  DBD_HIDAM = 2 ^ 2
  DBD_INDEX = 3 ^ 2
  DBD_LOGIC = 4 ^ 2
  DBD_UNKNOWN_TYPE = 5 ^ 2
End Enum

Enum e_IstrType
  IMSDB_ISTRUCTION = 0 ^ 2
  IMSDC_ISTRUCTION = 1 ^ 2
  UNKNOWED_ISTRUCTION = 2 ^ 2
End Enum

Type DefPcb
  Name As String
  dbdname As String
  TypePcb As String
  segName() As String
End Type

Global m_Menu As New MapsdC_Menu
Global colResWord As Collection
Global noparsercloni As Boolean
Global GbErrLevelRel As String
' AC � una pezza brutta brutta
Global GbappoIdPgm As Long

Sub ParseSegmentExec(ByRef parametri As String, prog As Integer)
  ' AC
  ' legge tutto fino a segmento successivo o fine
  Dim strParametro As String
  Dim wDliRec As String
  Dim opLogic As String, strChiave As String
  Dim progKey As Integer
  Dim qual As Boolean, isLastLevel As Boolean
  Dim posAND As Integer, posOR As Integer
  Dim posANDlett As Integer, posANDcomm As Integer, posORlett As Integer, posOrpipe As Integer
  ' Mauro 21/04/2008 :  Gestione KEYS - keyarg portato nella struttura TabIstr
  'Dim keyfeedarg As String, keyarg as string, afterFirst As String
  Dim keyfeedarg As String, afterFirst As String
  Dim segvalues() As String
  
  opLogic = ""
  progKey = 0
  qual = False
  isLastLevel = True
  ReDim Preserve TabIstr.BlkIstr(prog).KeyDef(0)
  TabIstr.BlkIstr(prog).valida = True  'init
 
  If Len(parametri) Then
    'SQ 12-02-08 CON LO SPAZIO NON RISOLVEVA NULLA!
    TabIstr.BlkIstr(prog).Segment = Trim(EliminaParentesi(Replace(nexttoken(parametri), "'", "")))
    If Left(TabIstr.BlkIstr(prog).Segment, 1) = "(" And Right(TabIstr.BlkIstr(prog).Segment, 1) = ")" Then
      TabIstr.BlkIstr(prog).Varsegment = EliminaParentesi(TabIstr.BlkIstr(prog).Segment)
      TabIstr.BlkIstr(prog).Segment = EliminaParentesi(TabIstr.BlkIstr(prog).Segment)
      segvalues() = MapsdM_Parser.getFieldValues(TabIstr.BlkIstr(prog).Segment)
      If UBound(segvalues) Then
        TabIstr.BlkIstr(prog).Segment = segvalues(1)
      Else
        addSegnalazione TabIstr.BlkIstr(prog).Segment, "NOSEGVALUE", TabIstr.BlkIstr(prog).Segment
        TabIstr.BlkIstr(prog).Segment = ""
      End If
    End If
  Else
    TabIstr.BlkIstr(prog).valida = False
    Exit Sub
  End If
  '*************
  'TabIstr.BlkIstr(k).moved = TabIstrSSa(k).moved
  'TabIstr.BlkIstr(k).CodCom = TabIstrSSa(k).CommandCode
  '*****************************************
  Dim posop As Integer, lenop As Integer, lencutand As Integer, lencutor As Integer
  
  Do While Len(parametri)
    strParametro = EliminaParentesi(nexttoken(parametri))
    Select Case strParametro
      Case "INTO", "FROM"
        TabIstr.BlkIstr(prog).Record = EliminaParentesi(nexttoken(parametri)) 'wDliRec
        'AC 03/01/08 e 21/01/08
        If TabIstr.BlkIstr(prog).CodCom = "*F" Then
          TabIstr.BlkIstr(prog).CodCom = "*DF"
        ElseIf TabIstr.BlkIstr(prog).CodCom = "*P" Then
          TabIstr.BlkIstr(prog).CodCom = "*DP"
        ElseIf TabIstr.BlkIstr(prog).CodCom = "*L" Then
          TabIstr.BlkIstr(prog).CodCom = "*DL"
        ElseIf TabIstr.BlkIstr(prog).CodCom = "*C" Then
          TabIstr.BlkIstr(prog).CodCom = "*DC"
        Else
          TabIstr.BlkIstr(prog).CodCom = "*D"
        End If
      Case "SEGLENGTH"
        TabIstr.BlkIstr(prog).SegLen = EliminaParentesi(nexttoken(parametri))
      Case "WHERE"
        qual = True
        progKey = progKey + 1
        ReDim Preserve TabIstr.BlkIstr(prog).KeyDef(progKey)
        ' nel prossimo token info su chiavi
        strChiave = EliminaParentesi(nexttoken(parametri))
        '....
        'AC 21/01/08 considero casi misti e prendo per ogni tipo il primo trovato
        posANDlett = InStr(strChiave, " AND ")
        posORlett = InStr(strChiave, " OR ")
        posANDcomm = InStr(strChiave, " & ")
        posOrpipe = InStr(strChiave, " | ")
        If posANDlett > 0 And posANDcomm > 0 Then
          posAND = IIf(posANDlett < posANDcomm, posANDlett, posANDcomm)
          lencutand = IIf(posANDlett < posANDcomm, 4, 2)
        ElseIf posANDlett = 0 Then
          posAND = posANDcomm
          lencutand = 2
        Else
          posAND = posANDlett
          lencutand = 4
        End If
        If posORlett > 0 And posOrpipe > 0 Then
          posOR = IIf(posORlett < posOrpipe, posORlett, posOrpipe)
          lencutor = IIf(posORlett < posOrpipe, 3, 2)
        ElseIf posORlett = 0 Then
          posOR = posOrpipe
          lencutor = 2
        Else
          posOR = posORlett
          lencutor = 3
        End If
        If posAND > 0 And (posAND < posOR Or Not posOR > 0) Then
          ' Mauro 07/10/2008: Se nel I livello c'era un operatore, brasava tutti gli altri livelli
          'parametri = "WHERE (" & Right(strChiave, Len(strChiave) - posAND - lencutand) & ")"
          parametri = "WHERE (" & Right(strChiave, Len(strChiave) - posAND - lencutand) & ") " & parametri
          strChiave = Left(strChiave, posAND - 1)
          TabIstr.BlkIstr(prog).KeyDef(progKey).KeyBool = "&"
        ElseIf posOR > 0 And (posOR < posAND Or Not posAND > 0) Then
          ' Mauro 07/10/2008: Se nel I livello c'era un operatore, brasava tutti gli altri livelli
          'parametri = "WHERE (" & Right(strChiave, Len(strChiave) - posOR - lencutor) & ")"
          parametri = "WHERE (" & Right(strChiave, Len(strChiave) - posOR - lencutor) & ") " & parametri
          strChiave = Left(strChiave, posOR - 1)
          'IRIS-DB TabIstr.BlkIstr(prog).KeyDef(progKey).KeyBool = "!"
          TabIstr.BlkIstr(prog).KeyDef(progKey).KeyBool = "|" 'IRIS-DB
        End If
        TabIstr.BlkIstr(prog).KeyDef(progKey).Op = ControllaOperatoreInstr(strChiave, posop, lenop)
        TabIstr.BlkIstr(prog).KeyDef(progKey).Key = Trim(Left(strChiave, posop - 1))
        TabIstr.BlkIstr(prog).KeyDef(progKey).KeyVal = Mid(strChiave, posop + lenop, Len(strChiave) - (posop))
        ' TabIstr.BlkIstr(prog).KeyDef(progKey).KeyBool a chi si abbina operatore ?
      Case "FIELDLENGTH"
        TabIstr.BlkIstr(prog).KeyDef(progKey).KeyLen = EliminaParentesi(nexttoken(parametri))
        Dim KeyLen() As String 'IRIS-DB
        Dim i As Long 'IRIS-DB
        KeyLen = Split(TabIstr.BlkIstr(prog).KeyDef(progKey).KeyLen, ",") 'IRIS-DB
        If UBound(KeyLen) > 0 Then 'IRIS-DB
          For i = 0 To UBound(KeyLen) 'IRIS-DB
            TabIstr.BlkIstr(prog).KeyDef(i + 1).KeyLen = KeyLen(i) 'IRIS-DB
          Next i 'IRIS-DB
        End If 'IRIS-DB
      Case "SEGMENT"
        ' la stringa torna in pasto al chiamante, la stringa originale
        ' devo riattaccare il token 'SEGMENT'
        parametri = "SEGMENT " & parametri
        isLastLevel = False
        'SQ/Alpis treno
        Exit Do
      Case "AND", "OR"
        opLogic = strParametro
      Case "SETPARENT"
        If TabIstr.BlkIstr(prog).CodCom = "" Then
          TabIstr.BlkIstr(prog).CodCom = "*P"
        Else
          TabIstr.BlkIstr(prog).CodCom = TabIstr.BlkIstr(prog).CodCom & "P"
        End If
      'AC 03/01/08
      Case "FIRST"  ' siamo gi� su livello successivo, gestito da routine exec delle istruzioni
        afterFirst = nexttoken(parametri)
        If afterFirst = "SEGMENT" Then
          parametri = "FIRST " & afterFirst & " " & parametri
          isLastLevel = False
          Exit Do
        Else
          parametri = Trim(afterFirst & " " & parametri)
          If TabIstr.BlkIstr(prog).CodCom = "" Then
            TabIstr.BlkIstr(prog).CodCom = "*F"
          Else
            TabIstr.BlkIstr(prog).CodCom = TabIstr.BlkIstr(prog).CodCom & "F"
          End If
        End If
      Case "LAST"
'IRIS-DB non so per quale motivo religioso LAST fosse gestito diverso da FIRST, ma vabbe...
'        If TabIstr.BlkIstr(prog).CodCom = "" Then
'          TabIstr.BlkIstr(prog).CodCom = "*L"
'        Else
'          TabIstr.BlkIstr(prog).CodCom = TabIstr.BlkIstr(prog).CodCom & "L"
'        End If
        afterFirst = nexttoken(parametri)
        If afterFirst = "SEGMENT" Then
          parametri = "LAST " & afterFirst & " " & parametri
          isLastLevel = False
          Exit Do
        Else
          parametri = Trim(afterFirst & " " & parametri)
          If TabIstr.BlkIstr(prog).CodCom = "" Then
            TabIstr.BlkIstr(prog).CodCom = "*L"
          Else
            TabIstr.BlkIstr(prog).CodCom = TabIstr.BlkIstr(prog).CodCom & "L"
          End If
        End If
      Case "CURRENT" 'IRIS-DB
        afterFirst = nexttoken(parametri)
        If afterFirst = "SEGMENT" Then
          parametri = "CURRENT " & afterFirst & " " & parametri
          isLastLevel = False
          Exit Do
        Else
          parametri = Trim(afterFirst & " " & parametri)
          If TabIstr.BlkIstr(prog).CodCom = "" Then
            TabIstr.BlkIstr(prog).CodCom = "*V"
          Else
            TabIstr.BlkIstr(prog).CodCom = TabIstr.BlkIstr(prog).CodCom & "V"
          End If
        End If
      Case "KEYFEEDBACK"
        TabIstr.keyFeed = EliminaParentesi(nexttoken(parametri))
      Case "FEEDBACKLEN" 'IRIS-DB
        TabIstr.keyFeedLen = EliminaParentesi(nexttoken(parametri))
      Case "KEYS"
      'IRIS-DB: mod�ficato per usare la chiave primaria direttamente quando root invece del command code, altrimenti si perde tutti gli eventuali livelli successivi
'''        TabIstr.KeyArg = EliminaParentesi(nexttoken(parametri))
'''        If TabIstr.BlkIstr(prog).CodCom = "" Then
'''          TabIstr.BlkIstr(prog).CodCom = "*C"
'''        Else
'''          TabIstr.BlkIstr(prog).CodCom = TabIstr.BlkIstr(prog).CodCom & "C"
'''        End If
'''        qual = True
'''        progKey = progKey + 1
'''        ReDim Preserve TabIstr.BlkIstr(prog).KeyDef(progKey)
'''        ' nel prossimo token info su chiavi
'''        strChiave = EliminaParentesi(nexttoken(parametri))
        qual = True
        progKey = progKey + 1
        ReDim Preserve TabIstr.BlkIstr(prog).KeyDef(progKey)
        strChiave = EliminaParentesi(nexttoken(parametri))
        TabIstr.BlkIstr(prog).KeyDef(progKey).Op = "EQ"
        TabIstr.BlkIstr(prog).KeyDef(progKey).Key = GetNomeKey(TabIstr.BlkIstr(prog).Segment, TabIstr.DBD)
        If TabIstr.BlkIstr(prog).KeyDef(progKey).Key = "CONCAT" Then
          Dim parentSeg As String
          Dim KeyParentLen As Integer
          Dim KeyCurrentLen As Integer
          parentSeg = GetParentSeg(TabIstr.BlkIstr(prog).Segment, TabIstr.DBD)
          If parentSeg = "CONCAT" Then
          'IRIS-DB: trying to get the no key segments too; supporting only a specific case of DSHS, to be finalized later
            parentSeg = GetParentSegUncond(TabIstr.BlkIstr(prog).Segment, TabIstr.DBD)
            Dim parentSeg2 As String
            parentSeg2 = GetParentSeg(parentSeg, TabIstr.DBD)
            If parentSeg2 <> "" Then
              ReDim Preserve TabIstr.BlkIstr(2)
              ReDim Preserve TabIstr.BlkIstr(2).KeyDef(1)
              ReDim Preserve TabIstr.BlkIstr(3)
              ReDim Preserve TabIstr.BlkIstr(3).KeyDef(progKey)
              TabIstr.BlkIstr(3) = TabIstr.BlkIstr(1)
              TabIstr.BlkIstr(3).qualified = False
              TabIstr.BlkIstr(1).Segment = parentSeg2
              TabIstr.BlkIstr(1).KeyDef(1).Key = GetNomeKey(TabIstr.BlkIstr(1).Segment, TabIstr.DBD)
              TabIstr.BlkIstr(1).KeyDef(1).Op = "EQ"
              TabIstr.BlkIstr(2).Segment = parentSeg
              TabIstr.BlkIstr(2).KeyDef(1).Key = GetNomeKeyNew(TabIstr.BlkIstr(2).Segment, TabIstr.DBD)
              TabIstr.BlkIstr(2).KeyDef(1).Op = "EQ"
              TabIstr.BlkIstr(1).qualified = True
              TabIstr.BlkIstr(2).qualified = True
              KeyParentLen = GetKeyLen(TabIstr.BlkIstr(1).Segment, TabIstr.DBD)
              KeyCurrentLen = GetKeyLen(TabIstr.BlkIstr(2).Segment, TabIstr.DBD)
              TabIstr.BlkIstr(1).KeyDef(1).KeyVal = strChiave & "(1:" & KeyParentLen & ")"
              TabIstr.BlkIstr(2).KeyDef(1).KeyVal = strChiave & "(" & KeyParentLen + 1 & ":" & KeyCurrentLen & ")"
              TabIstr.BlkIstr(1).Record = ""
              TabIstr.BlkIstr(2).Record = ""
              TabIstr.BlkIstr(1).valida = True
              TabIstr.BlkIstr(2).valida = True
              'prog = 3
              'qual = False
              ReDim Preserve TabIstr.BlkIstr(3).KeyDef(0)
            'IRIS-DB to manage better later
              If TabIstr.BlkIstr(1).CodCom = "*D" Then
                TabIstr.BlkIstr(1).CodCom = ""
              Else
                TabIstr.BlkIstr(1).CodCom = Replace(TabIstr.BlkIstr(1).CodCom, "D", "")
              End If
              If TabIstr.BlkIstr(2).CodCom = "*D" Then
                TabIstr.BlkIstr(2).CodCom = ""
              Else
                TabIstr.BlkIstr(2).CodCom = Replace(TabIstr.BlkIstr(2).CodCom, "D", "")
              End If
              If TabIstr.BlkIstr(3).CodCom = "*D" Then
                TabIstr.BlkIstr(3).CodCom = ""
              Else
                TabIstr.BlkIstr(3).CodCom = Replace(TabIstr.BlkIstr(3).CodCom, "D", "")
              End If
            Else
              If parentSeg = "CONCAT" Then
                If TabIstr.BlkIstr(prog).CodCom = "" Then
                  TabIstr.BlkIstr(prog).CodCom = "*C"
                Else
                  TabIstr.BlkIstr(prog).CodCom = TabIstr.BlkIstr(prog).CodCom & "C"
                End If
                TabIstr.BlkIstr(prog).KeyDef(progKey).KeyVal = strChiave
              End If
            End If
          Else
          'IRIS-DB: for now it supports only 2 levels of KEYS and assumes CLONES are identical in terms of segments and fields!!!
            If prog = 1 Then
              ReDim Preserve TabIstr.BlkIstr(2)
              ReDim Preserve TabIstr.BlkIstr(2).KeyDef(progKey)
              TabIstr.BlkIstr(2) = TabIstr.BlkIstr(1)
              TabIstr.BlkIstr(1).Segment = parentSeg
              TabIstr.BlkIstr(1).KeyDef(1).Key = GetNomeKey(TabIstr.BlkIstr(1).Segment, TabIstr.DBD)
              TabIstr.BlkIstr(1).KeyDef(1).Op = "EQ"
              TabIstr.BlkIstr(2).KeyDef(progKey).Key = GetNomeKeyNew(TabIstr.BlkIstr(2).Segment, TabIstr.DBD)
              TabIstr.BlkIstr(1).qualified = True
              TabIstr.BlkIstr(2).qualified = True
            Else
              ReDim Preserve TabIstr.BlkIstr(1).KeyDef(1)
              TabIstr.BlkIstr(1).Segment = parentSeg
              TabIstr.BlkIstr(1).KeyDef(1).Key = GetNomeKey(TabIstr.BlkIstr(1).Segment, TabIstr.DBD)
              TabIstr.BlkIstr(1).KeyDef(1).Op = "EQ"
              TabIstr.BlkIstr(2).KeyDef(progKey).Key = GetNomeKeyNew(TabIstr.BlkIstr(2).Segment, TabIstr.DBD)
              TabIstr.BlkIstr(1).qualified = True
              TabIstr.BlkIstr(2).qualified = True
            End If
            KeyParentLen = GetKeyLen(TabIstr.BlkIstr(1).Segment, TabIstr.DBD)
            KeyCurrentLen = GetKeyLen(TabIstr.BlkIstr(2).Segment, TabIstr.DBD)
            TabIstr.BlkIstr(1).KeyDef(1).KeyVal = strChiave & "(1:" & KeyParentLen & ")"
            TabIstr.BlkIstr(2).KeyDef(1).KeyVal = strChiave & "(" & KeyParentLen + 1 & ":" & KeyCurrentLen & ")"
            TabIstr.BlkIstr(1).Record = ""
            'IRIS-DB to manage better later
            If TabIstr.BlkIstr(2).CodCom = "*D" Then
              TabIstr.BlkIstr(2).CodCom = ""
            Else
              TabIstr.BlkIstr(2).CodCom = Replace(TabIstr.BlkIstr(2).CodCom, "D", "")
            End If
          End If
        Else
          TabIstr.BlkIstr(prog).KeyDef(progKey).KeyVal = strChiave
        End If
        If TabIstr.BlkIstr(prog).KeyDef(progKey).Key = "" Then
          addSegnalazione "KEYS", "I00", "missing Sequence Unique in segm: " & TabIstr.BlkIstr(prog).Segment
        End If
      Case "KEYLENGTH" 'IRIS-DB
        TabIstr.BlkIstr(prog).KeyDef(progKey).KeyLen = EliminaParentesi(nexttoken(parametri))
        KeyLen = Split(TabIstr.BlkIstr(prog).KeyDef(progKey).KeyLen, ",")
        If UBound(KeyLen) > 0 Then
          For i = 0 To UBound(KeyLen)
            TabIstr.BlkIstr(prog).KeyDef(i + 1).KeyLen = KeyLen(i)
          Next i
        End If
      Case "VARIABLE" 'IRIS-DB per ora semplicemente ignorato, tanto lo sappiamo dal DBD e all'incapsulatore non interessa
      Case Else
        'AC entra spesso qui, da rivedere il riconoscimento del syntax error
        addSegnalazione strParametro, "I00", strParametro
        parametri = ""
        Exit Do
    End Select
    ' riallineo
  Loop
  ' il command code "*D" per la INTO solo se non siamo sull'ultimo livello
  If isLastLevel And TabIstr.BlkIstr(prog).CodCom = "*D" Then
    TabIstr.BlkIstr(prog).CodCom = ""
  ElseIf isLastLevel Then ' se il D insieme ad altri (tipo F o L) tolgo solo il d
    TabIstr.BlkIstr(prog).CodCom = Replace(TabIstr.BlkIstr(prog).CodCom, "D", "")
  End If
  ' se nessuna where segmento squalificato
  TabIstr.BlkIstr(prog).qualified = qual
End Sub
Public Function GetNomeKey(wNomeSegm, dbdname) 'IRIS-DB
  Dim tb As Recordset
  On Error GoTo err
  Set tb = m_Fun.Open_Recordset("select a.nome, b.parent from PsDli_field as a, PsDli_segmenti as b, bs_oggetti as c " & _
          "where a.idsegmento = b.idsegmento and a.seq = true and a.unique = true and b.nome = '" & wNomeSegm & _
           "' and c.nome = '" & dbdname & "' and b.idoggetto = c.idoggetto")
  If tb.RecordCount > 0 Then
    If tb!Parent = "0" Or tb!Parent = "" Then
      GetNomeKey = tb!nome
    Else
      GetNomeKey = "CONCAT"
    End If
  Else
    GetNomeKey = "CONCAT"
  End If
  tb.Close
  Exit Function
err:
  m_Fun.writeLog "GetNomeKey: " & err.description
End Function
Public Function GetNomeKeyNew(wNomeSegm, dbdname) 'IRIS-DB
  Dim tb As Recordset
  On Error GoTo err
  Set tb = m_Fun.Open_Recordset("select a.nome, b.parent from PsDli_field as a, PsDli_segmenti as b, bs_oggetti as c " & _
           " where a.idsegmento = b.idsegmento and a.seq = true and a.unique = true and b.nome = '" & wNomeSegm & _
           "' and c.nome = '" & dbdname & "' and b.idoggetto = c.idoggetto")
  If tb.RecordCount > 0 Then
    GetNomeKeyNew = tb!nome
  Else
    GetNomeKeyNew = "CONCAT"
  End If
  tb.Close
  Exit Function
err:
  m_Fun.writeLog "GetNomeKeyNew: " & err.description
End Function
Public Function GetParentSeg(wNomeSegm, dbdname) 'IRIS-DB
  Dim tb As Recordset
  On Error GoTo err
  Set tb = m_Fun.Open_Recordset("select a.nome, b.parent from PsDli_field as a, PsDli_segmenti as b, bs_oggetti as c" & _
         " where a.idsegmento = b.idsegmento and a.seq = true and a.unique = true and b.nome = '" & wNomeSegm & _
         "' and c.nome = '" & dbdname & "' and b.idoggetto = c.idoggetto")
  If tb.RecordCount > 0 Then
    If tb!Parent = "0" Or tb!Parent = "" Then
      GetParentSeg = ""
    Else
      GetParentSeg = tb!Parent
    End If
  Else
    GetParentSeg = "CONCAT"
  End If
  tb.Close
  Exit Function
err:
  m_Fun.writeLog "GetParentSeg: " & err.description
End Function
Public Function GetParentSegUncond(wNomeSegm, dbdname) 'IRIS-DB
  Dim tb As Recordset
  On Error GoTo err
  Set tb = m_Fun.Open_Recordset("select parent from PsDli_segmenti as a, bs_oggetti as b where a.nome = '" & _
           wNomeSegm & "' and b.nome = '" & dbdname & "' and a.idoggetto = b.idoggetto")
  If tb.RecordCount > 0 Then
    If tb!Parent = "0" Or tb!Parent = "" Then
      GetParentSegUncond = ""
    Else
      GetParentSegUncond = tb!Parent
    End If
  Else
    GetParentSegUncond = "CONCAT"
  End If
  tb.Close
  Exit Function
err:
  m_Fun.writeLog "GetParentSegUncond: " & err.description
End Function
Public Function GetKeyLen(wNomeSegm, dbdname) 'IRIS-DB
  Dim tb As Recordset
  On Error GoTo err
  Set tb = m_Fun.Open_Recordset("select a.Lunghezza from PsDli_field as a, PsDli_segmenti as b, bsoggetti as c " & _
         " where a.idsegmento = b.idsegmento and a.seq = true and a.unique = true and b.nome = '" & wNomeSegm & _
           "' and c.nome = '" & dbdname & "' and b.idoggetto = c.idoggetto")
  Set tb = m_Fun.Open_Recordset("select a.nome, b.parent from PsDli_field as a, PsDli_segmenti as b, bs_oggetti as c " & _
           " where a.idsegmento = b.idsegmento and a.seq = true and a.unique = true and b.nome = '" & wNomeSegm & _
           "' and c.nome = '" & dbdname & "' and b.idoggetto = c.idoggetto")
  If tb.RecordCount > 0 Then
    GetKeyLen = tb!Lunghezza
  Else
    GetKeyLen = 0
  End If
  tb.Close
  Exit Function
err:
  m_Fun.writeLog "GetKeyLen: " & err.description
End Function

''''''''''''''''''''''''''''''''
'  Pulizia Tabelle, ErrorLevel
''''''''''''''''''''''''''''''''
Sub DliInitParser()
  On Error GoTo err
  
  'SQ ATTENZIONE!!! � utilizzato anche dal livello I !?
  resetRepository ("parsedue")
   ''''''''''''''''''''''''''''''''''''''
  ' 28-04-06
  ' Relazioni aggiunte dal livello II
  ''''''''''''''''''''''''''''''''''''''
  Parser.PsConnection.Execute "delete * from PsRel_Obj where idoggettoC = " & GbIdOggetto & " AND Utilizzo='PBS-INHE'"
  Parser.PsConnection.Execute "delete * from PsRel_Obj where idoggettoC = " & GbIdOggetto & " AND Utilizzo='IMS-DBD'"
  Parser.PsConnection.Execute "delete * from PsCom_Obj where idoggettoc = " & GbIdOggetto & " and relazione = 'SGM' "
  
  Parser.AggErrLevel GbIdOggetto  'SQ ?!
  Exit Sub
err:
  If err.Number = -2147467259 Then
    Resume Next
  Else
    m_Fun.writeLog "DliInitParser: " & err.description
  End If
End Sub

Public Sub parseCHKP_exec(istruzione As String, parametri As String, idpgm As Long, idOggetto As Long, Riga As Long)
  Dim tb As Recordset
  Dim idArea As String, areechkp() As String
  Dim rs As Recordset
  'AC 27/05/08 ' potremmo avere anche una variabile con la lunghezza delle aree
  Dim areelenchkp() As String, campoaree As String, i As Integer
  
  On Error GoTo parseErr
  
  ReDim areechkp(0)
  ReDim areelenchkp(0)
  ReDim TabIstr.BlkIstr(0)

  Dim strParametro   As String
  Dim intProgSeg As Integer
  
  intProgSeg = 0
  If idpgm = -1 Then idpgm = idOggetto
  
  While Len(parametri)
    strParametro = nexttoken(parametri)
    'AC 27/05/08 parole chiave: ID, AREAn,LENGTHn
    Select Case Len(strParametro)
      Case 2
        If strParametro = "ID" Then
          idArea = EliminaParentesi(nexttoken(parametri))
        End If
      Case 5
        If Left(strParametro, 4) = "AREA" Then
          ReDim Preserve areechkp(UBound(areechkp) + 1)
          areechkp(UBound(areechkp)) = EliminaParentesi(nexttoken(parametri))
        End If
      Case 7
        If Left(strParametro, 6) = "LENGTH" Then
          ReDim Preserve areelenchkp(UBound(areelenchkp) + 1)
          areelenchkp(UBound(areelenchkp)) = EliminaParentesi(nexttoken(parametri))
        End If
    End Select
'    Select Case strParametro
'      Case "ID"
'        idArea = EliminaParentesi(nextToken(parametri))
'      Case Else ' solo per SYMCHKP
'        ' incrementa blkIstrDli
'        If Left(strParametro, 4) = "AREA" And Len(strParametro) = 5 Then
'          ReDim Preserve areechkp(UBound(areechkp) + 1)
'          areechkp(UBound(areechkp)) = EliminaParentesi(nextToken(parametri))
'        End If
'    End Select
  Wend
  'AC 29/05/08 Ho mantenuto gli array anche se poi creiamo stringa unica (; come separatore)
  Set tb = m_Fun.Open_Recordset("select MapArea,MapName,Mapset from psdli_Istruzioni where IdPgm = " & idpgm & _
                                " and Idoggetto = " & idOggetto & " and riga = " & Riga)
  If Not tb.EOF Then
    tb!Maparea = idArea
    For i = 1 To UBound(areechkp)
      campoaree = campoaree & ";" & areechkp(i)
    Next
    If Len(campoaree) Then
      campoaree = Right(campoaree, Len(campoaree) - 1)
    End If
    tb!mapName = campoaree
    campoaree = ""
    For i = 1 To UBound(areelenchkp)
      campoaree = campoaree & ";" & areelenchkp(i)
    Next
    If Len(campoaree) Then
      campoaree = Right(campoaree, Len(campoaree) - 1)
    End If
    tb!mapSet = campoaree
    tb.Update
  End If
  tb.Close
  TabIstr.istr = istruzione
  Exit Sub
parseErr:

End Sub

Public Sub parseXRST_exec(istruzione As String, parametri As String, idpgm As Long, idOggetto As Long, Riga As Long)
  Dim tb As Recordset
  Dim idArea As String, maxLength As String, areechkp() As String
  Dim areelenchkp() As String, campoaree As String, i As Integer
    
  'AC 27/05/08' costruita sulla falsariga della CHKP_exec
  On Error GoTo parseErr
  
  ReDim areechkp(0)
  ReDim areelenchkp(0)
  ReDim TabIstr.BlkIstr(0)
  
  Dim strParametro   As String
  Dim intProgSeg As Integer
  
  intProgSeg = 0
  If idpgm = -1 Then idpgm = idOggetto
  While Len(parametri)
    strParametro = nexttoken(parametri)
    'AC 27/05/08 parole chiave: MAXLENGTH,ID, AREAn,LENGTHn
    Select Case Len(strParametro)
      Case 2
        If strParametro = "ID" Then
          idArea = EliminaParentesi(nexttoken(parametri))
        End If
      Case 5
        If Left(strParametro, 4) = "AREA" Then
          ReDim Preserve areechkp(UBound(areechkp) + 1)
          areechkp(UBound(areechkp)) = EliminaParentesi(nexttoken(parametri))
        End If
      Case 7
        If Left(strParametro, 6) = "LENGTH" Then
          ReDim Preserve areelenchkp(UBound(areelenchkp) + 1)
          areelenchkp(UBound(areelenchkp)) = EliminaParentesi(nexttoken(parametri))
        End If
      Case 9
        If strParametro = "MAXLENGTH" Then
          maxLength = EliminaParentesi(nexttoken(parametri))
        End If
    End Select
  Wend
  'AC 29/05/08 Ho mantenuto gli array anche se poi creiamo stringa unica (; come separatore)
  Set tb = m_Fun.Open_Recordset("select MapArea,MapName,Mapset from psdli_Istruzioni where IdPgm = " & idpgm & _
                                " and Idoggetto = " & idOggetto & " and riga = " & Riga)
  If Not tb.EOF Then
    tb!Maparea = idArea & ";" & maxLength  ' sullo stesso campo ID ; MAXLENGTH
    For i = 1 To UBound(areechkp)
      campoaree = campoaree & ";" & areechkp(i)
    Next
    If Len(campoaree) Then
      campoaree = Right(campoaree, Len(campoaree) - 1)
    End If
    tb!mapName = campoaree
    campoaree = ""
    For i = 1 To UBound(areelenchkp)
      campoaree = campoaree & ";" & areelenchkp(i)
    Next
    If Len(campoaree) Then
      campoaree = Right(campoaree, Len(campoaree) - 1)
    End If
    tb!mapSet = campoaree
    tb.Update
  End If
  tb.Close
  TabIstr.istr = istruzione
  Exit Sub
parseErr:
  
End Sub

Public Sub parseGU_exec(istruzione As String, parametri As String)
  Dim rs As Recordset, tb As Recordset, tb1 As Recordset, tb2 As Recordset, rsdbd As Recordset
  Dim i As Integer, k As Integer, y As Integer, T1 As Integer, T2 As Integer, T3 As Integer, T4 As Integer
  Dim dbdname As String, wNomePsb As String, ssaValue As String
  Dim isGSAM As Boolean, unqualified As Boolean
  Dim idField As Integer, isComFirst As Boolean, isComLast As Boolean, isComCurrent As Boolean 'IRIS-DB
  Dim keyfedarg As String, whereParam As String, nameSeg As String, cleanparam
  Dim intoParam As String, fromParam As String
  Dim seglenParam As String
  On Error GoTo parseErr
  
  ReDim gSSA(0)
  ReDim dbdPgm(0)
  ReDim TabIstr.BlkIstr(0)
  
  'Ac
  Dim strParametro   As String
  Dim intProgSeg As Integer
  
  intProgSeg = 0
  SwSegm = "" 'IRIS-DB
  While Len(parametri)
    strParametro = nexttoken(parametri)
    Select Case strParametro
      Case "WHERE"
        If intProgSeg = 0 Then
          whereParam = strParametro & " " & nexttoken(parametri) & " "
        End If
      Case "INTO"
        If intProgSeg = 0 Then
          intoParam = strParametro & " " & nexttoken(parametri) & " "
        End If
      Case "SEGLENGTH" 'IRIS-DB
        If intProgSeg = 0 Then
          seglenParam = strParametro & " " & nexttoken(parametri) & " "
        End If
      Case "FROM"
        If intProgSeg = 0 Then
          fromParam = strParametro & " " & nexttoken(parametri) & " "
        End If
      Case "FIRST"
        isComFirst = True
      Case "LAST"
        isComLast = True
      Case "CURRENT"
        isComCurrent = True
      Case "KEYFEEDBACK"
        TabIstr.keyFeed = EliminaParentesi(nexttoken(parametri))
      Case "FEEDBACKLEN" 'IRIS-DB
        TabIstr.keyFeedLen = EliminaParentesi(nexttoken(parametri))
      Case "PCB"
        TabIstr.pcb = EliminaParentesi(nexttoken(parametri))
        TabIstr.numpcb = getPCBnumber(TabIstr.pcb, True)
      Case "SEGMENT"
        ' incrementa blkIstrDli
        intProgSeg = intProgSeg + 1
        ReDim Preserve TabIstr.BlkIstr(intProgSeg)
        If isComFirst Then
          TabIstr.BlkIstr(intProgSeg).CodCom = "*F"
        End If
        If isComLast Then
          TabIstr.BlkIstr(intProgSeg).CodCom = "*L"
        End If
        If isComCurrent Then
          TabIstr.BlkIstr(intProgSeg).CodCom = "*V"
        End If
        If whereParam = "" And intoParam = "" And fromParam = "" And seglenParam = "" Then
          ParseSegmentExec parametri, intProgSeg
        Else
          nameSeg = nexttoken(parametri)
          parametri = nameSeg & " " & intoParam & seglenParam & fromParam & whereParam & parametri
          ParseSegmentExec parametri, intProgSeg
          whereParam = ""
          intoParam = ""
          seglenParam = ""
        End If
        isComLast = False
        isComFirst = False
        isComCurrent = False
      Case "USING" 'IRIS-DB stranamente dava errore in compresenza della LAST o di altro CC; probabilmente di solito lo scarta prima
      Case "VARIABLE" 'IRIS-DB per ora semplicemente ignorato, tanto lo sappiamo dal DBD e all'incapsulatore non interessa
      Case Else
        addSegnalazione strParametro, "I00", strParametro
    End Select
  Wend
  
  ' Mauro 23/04/2008 : Istruzione completamente squalificata
  ' aggiungiamo un livello per inserire l'area di I/O; adeguamento alla CALL
  If intProgSeg = 0 Then
    ReDim TabIstr.BlkIstr(1)
    TabIstr.BlkIstr(1).valida = True ' init
    ' Faccio la ReDim anche sulla KeyDef senn� mi va in errore quando salva la IstrDett_Liv
    ReDim Preserve TabIstr.BlkIstr(1).KeyDef(0)
    TabIstr.BlkIstr(1).Record = Trim(Replace(intoParam, "INTO", ""))
    If Trim(seglenParam) <> "" Then
      TabIstr.BlkIstr(intProgSeg).SegLen = Trim(Replace(seglenParam, "SEGLENGTH", ""))
    End If
  End If
  
  TabIstr.istr = istruzione
    
  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  ' **** TabIstr.pcb = nextToken(parametri)
  
  ' ***** sposto nel parser segmento wDliRec = nextToken(parametri)
       
'  Dim ssa
'  ReDim wDliSsa(0)
'  ReDim TabIstrSSa(0)
'  ReDim TabIstrSSa(0).NomeSegmento.value(0)
'  Dim ssaOk As Boolean
'  ssaOk = True
   
  ''''''''''''''''''''''''''''''''''''''''
  ' SQ 20-04-06
  ' Area Segmento - la associava a tutti!
  '''''''''''''''''''''''''''''''''''''''''
'  If Not unqualified Then
'    TabIstrSSa(UBound(TabIstrSSa)).Struttura = wDliRec
'    If Len(TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0)) = 0 And Len(wDliRec) Then  'Solo sul Segmento finale...
'      ''''''''''''''''''''''''''''''''''''''''''''''
'      ' SQ 2-05-06
'      ' Naming Convention SEGMENTO/AREA
'      ''''''''''''''''''''''''''''''''''''''''''''''
'      Dim env As Collection 'area,idDbd
'      If Len(segmentNameParam) Then
'        Set env = New Collection  'resetto;
'        env.Add wDliRec
'        'SQ 5-06-06 - Come poteva funzionare?
'        env.Add 0 'wIdDBD(0)
'        'SQ MOVED/VALUE
'        TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0) = getEnvironmentParam(segmentNameParam, "IMS", env)
'      End If
'    End If
'    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    'SQ - Cambiamento mostruoso (pezzo di analizzaCallDli_SQ)
'    ' TabIstrSSa -> TabIstr
'    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    'getTabIstr
'  End If
    
  ''''''''''''''''''''''''''''''''
  ' Gestione PSB/DBD:
  ' Input: N PSB, PCB, M SEGMENTI
  ''''''''''''''''''''''''''''''''
  getInstructionPsbDbd (TabIstr.numpcb)
  
  'SQ - 27-04-06
  'If isGSAM Then
  If TabIstr.DBD_TYPE = DBD_GSAM Then
    getGSAMinfo (TabIstr.numpcb)  'wIDdbd...
  Else
    ''''''''''''''SQ - 27-04-06 (sopra: serve cmq) getInstructionPsbDbd (TabIstr.numPCB)
    ''''''''''''''''''''''''''''''''''''''''''
    ' TabIstr.BlkIstr:
    ''''''''''''''''''''''''''''''''''''''''''
    If IsValid Then  'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
      getTabIstrIds
    Else
      If Not unqualified Then IsValid = False
    End If
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'SQ 10-04-06
  ' Se usciamo qui perdiamo tutti i settaggi per l'istruzione!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If IsValid And Not unqualified Then 'SQ 27-04-06
    ''''''''''''''''''''''''''''''''''''
    'ITERAZIONE LIVELLI ISTRUZIONE:
    ''
    'AGGIORNAMENTO "PsDli_IstrDett_Liv"
    ''''''''''''''''''''''''''''''''''''
    Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
    For k = 1 To UBound(TabIstr.BlkIstr)
      '''''''''''''''''''''
      'SEGMENTO VARIABILE?
      '''''''''''''''''''''
      If Left(TabIstr.BlkIstr(k).Segment, 1) = "(" And Right(TabIstr.BlkIstr(k).Segment, 1) = ")" Then
        ReDim IdSegmenti(0)
        '''''''''''''''''''''''''''''''''''''
        ' Segmento variabile: gestione temp.
        '''''''''''''''''''''''''''''''''''''
      End If
      
      'stefano: spero di avere ragione
      'Dim wNomeCmpLiv As String, wIdStr As String, wStrIdOggetto As String, wStrIdArea As Long
      Dim wNomeCmpLiv As String, wIdStr As String, wStrIdOggetto As Long, wStrIdArea As Long
      
      wIdStr = getIdArea(TabIstr.BlkIstr(k).Record)
      wNomeCmpLiv = TabIstr.BlkIstr(k).Record
      
      'ADESSO HO N SEGMENTI QUANTI SONO I DBD... (o ZERO!)
      'SQ 28-11-05:
      'Mauro 19-03-2007 Multi-Segm
      'If TabIstr.BlkIstr(k).IdSegm Then
      If Len(TabIstr.BlkIstr(k).IdSegmenti) Then
        Dim IdSegmentiD() As String
        Dim a As Integer
        IdSegmentiD() = Split(TabIstr.BlkIstr(k).IdSegmenti, ";")
        For a = 0 To UBound(IdSegmentiD())
          wStrIdOggetto = Val(Left(wIdStr, 6))
          wStrIdArea = Val(Mid(wIdStr, 8, 4))
          'SQ - 28-11-05
          'Pezza provvisoria: capire perche' arriva cosi'...
          If wStrIdOggetto <> 0 And wStrIdArea <> 0 Then
            Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where " & _
                                           "idsegmento = " & IdSegmentiD(a) & " and stridoggetto = " & wStrIdOggetto & _
                                           " and stridarea = " & wStrIdArea & " and nomecmp = '" & wNomeCmpLiv & "'")
            If tb2.RecordCount = 0 Then
              tb2.AddNew
            End If
            tb2!idSegmento = IdSegmentiD(a)
            tb2!stridoggetto = wStrIdOggetto
            tb2!strIdArea = wStrIdArea
            tb2!nomeCmp = wNomeCmpLiv
            'AC
            tb2.Update
            tb2.Close
          End If
        Next a
      End If
         
     If TabIstr.DBD_TYPE <> DBD_GSAM Then
       If Len(IdSegmenti(0)) = 0 Then SwSegm = TabIstr.BlkIstr(k).Segment
     End If
         
     Dim wNomeCmp As String
     i = InStr(wIdStr, "-")
     If i > 0 Then
       wNomeCmp = Mid(wIdStr, i + 2)
       wIdStr = Val(Mid(wIdStr, 1, i - 1))
     Else
       wNomeCmp = " "
     End If
    
     ''''''''''''''''''''''''''''''''''''''
     'INSERIMENTO IN "PsDli_IstrDett_Liv":
     ''''''''''''''''''''''''''''''''''''''
     tb.AddNew
     'SQ COPY-ISTR
     'tb!idOggetto = GbIdOggetto
     tb!idOggetto = IdOggettoRel
     tb!idpgm = GbIdOggetto

     tb!Riga = GbOldNumRec
     tb!livello = k
     If Not Len(TabIstr.BlkIstr(k).Varsegment) = 0 Then
       tb!VariabileSegmento = TabIstr.BlkIstr(k).Varsegment
     End If
     
     ' tb!dtsegnalazione = Now
    
    'SQ: Cos'� 'sta roba???
    'dovremo aggiungere quei due campi nella TabIstr!!!!!!!!!!!!!!!!!!!!
'    For i = 0 To UBound(TabIstrSSa)
'      If Trim(TabIstrSSa(i).SSAName) = Trim(TabIstr.BlkIstr(k).SSAName) Then
'        tb!stridoggetto = TabIstrSSa(i).idOggetto
'        tb!strIdArea = TabIstrSSa(i).idArea
'        Exit For
'      End If
'    Next
    
     'SQ-2: CHIARIRE: LI HO APPENA CALCOLATI SOPRA!?
    ' y = InStr(wIdStr, "/")
     'If y > 0 Then
        'tb!strIdOggetto = Left(wIdStr, 6)
        'tb!strIdArea = Mid(wIdStr, y + 1, 4) 'SOPRA USO 6 E QUI Y?!
     'Else
      ''SQ - controllare: era sbagliato! wStrIdOggetto > 0... ma � string!?
        'If wStrIdArea > 0 And Len(wStrIdOggetto) Then
           'tb!strIdOggetto = wStrIdOggetto
           'tb!strIdArea = wStrIdArea
        'End If
     'End If


     'SQ-2: attenzione: ne ho N!
     'EVENTUALMENTE FARE TABELLA DEDICATA...
     '(IN TEORIA NE BASTA 1 PER RECUPERARE IL NOME... POI SE SERVE HO LA TABELLA DBD/ISTRUZIONE)
          
      'Mauro 19-03-2007 Multi-Segm
      'If TabIstr.BlkIstr(k).IdSegm = 0 Then
      If Len(TabIstr.BlkIstr(k).IdSegmenti) = 0 Then
        'Scamuffo I/O AREA -> SEGMENTO (Ha senso solo sull'ultimo livello - volendo si pu� rendere opzionale)
        If k = UBound(TabIstr.BlkIstr) Then
          getSegment_byArea TabIstr.BlkIstr(k)
        End If
      End If
      'If TabIstr.BlkIstr(k).IdSegm Then
      If Len(TabIstr.BlkIstr(k).IdSegmenti) Then
        tb!idSegmento = TabIstr.BlkIstr(k).IdSegm
        tb!IdSegmentiD = TabIstr.BlkIstr(k).IdSegmenti
      Else
        'Cerca l'id segmento nelle tabelle mg (id segmento associato da noi)
        tb!idSegmento = getIdSegmento_By_MgTable(TabIstr.BlkIstr(k).Record)
        tb!IdSegmentiD = tb!idSegmento
        'SQ 9-05-06: nuova gestione segmenti... non serve pi�
'          If tb!IdSegmento = 0 Then
'             'se � un gsam associa l'unico segmento possibile legato a quel dbd
'             If TabIstr.DBD_TYPE = DBD_GSAM Then
'                'Usa wPcb!!!! modificare...
'                tb!IdSegmento = Assegna_IdSegmento_ByNomePCB_GSAM()
'             End If
'          End If
          
          '''''''''''''''''''''''''''''''''''''''''''''
          'SQ -10-04-06
          'Segmento non trovata: istruzione INVALIDA!
          '''''''''''''''''''''''''''''''''''''''''''''
        If tb!idSegmento Then
          TabIstr.BlkIstr(k).IdSegm = tb!idSegmento
          TabIstr.BlkIstr(k).IdSegmenti = tb!idSegmento
          TabIstr.BlkIstr(k).Segment = getNomeSegmento(tb!idSegmento)
          TabIstr.BlkIstr(k).Segmenti = TabIstr.BlkIstr(k).Segment
          If Len(TabIstr.BlkIstr(k).SegLen) = 0 Then
            TabIstr.BlkIstr(k).SegLen = getLenSegmentoByIdSegmento(tb!idSegmento)
            TabIstr.BlkIstr(k).SegmentiLen = TabIstr.BlkIstr(k).SegLen
          End If
        Else
          ' Mauro 29/04/2008 : Istruzione completamente squalificata
          ' aggiungiamo un livello per inserire l'area di I/O; adeguamento alla CALL
          If intProgSeg = 0 Then
            TabIstr.BlkIstr(k).valida = True
          Else
            IsValid = False
          End If
        End If
      End If

     'E' sempre vuoto! servirebbe una "getLenSegmentoByIdSegmento(tb!IdSegmento)",
     'ma metterlo a posto a monte...
     'Mauro 19-03-2007 Multi-Segm
     'If IsNumeric(TabIstr.BlkIstr(k).SegLen) Then 'blank/null (numerico <- stringa) !
     If Len(TabIstr.BlkIstr(k).SegmentiLen) Then
       If TabIstr.BlkIstr(k).SegLen Then
         tb!SegLen = CInt(TabIstr.BlkIstr(k).SegLen)
       Else
         'Imposto il primo valore tra quelli multipli
         Dim SegmentLen() As String
         SegmentLen() = Split(TabIstr.BlkIstr(k).SegmentiLen, ";")
         tb!SegLen = SegmentLen(0)
       End If
       tb!SegmentiLend = TabIstr.BlkIstr(k).SegmentiLen
     ElseIf Len(TabIstr.BlkIstr(k).SegLen) Then
       tb!SegLen = TabIstr.BlkIstr(k).SegLen
       tb!SegmentiLend = TabIstr.BlkIstr(k).SegLen
     Else
       tb!SegLen = 0
       tb!SegmentiLend = 0
     End If
     tb!NomeSSA = TabIstr.BlkIstr(k).SSAName   'togliere sti bianchi in fondo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     tb!dataarea = TabIstr.BlkIstr(k).Record
     
     If Len(TabIstr.BlkIstr(k).CodCom) Then
      TabIstr.BlkIstr(k).Search = DecodificaCodCom(TabIstr.BlkIstr(k).CodCom)
      If InStr(TabIstr.BlkIstr(k).Search, "Undefined") Then
        TabIstr.BlkIstr(k).valida = False
        'NUOVA SEGNALAZIONE
      End If
     End If
        
     'Command code
     tb!condizione = Replace(TabIstr.BlkIstr(k).CodCom, "-", "")
     tb!Qualificazione = TabIstr.BlkIstr(k).qualified
     tb!Correzione = False
     tb!moved = TabIstr.BlkIstr(k).moved
     
     'SQ - 27-05-05: aspettare la validita' delle chiavi per chiudere...
    
     '''''''''''''''''''''''''''''''''''''''''''''''''
     'GESTIONE CHIAVI:
     'Cerca ID campo chiave
     'Allestisce tabella record in PsDli_IstrDett_Key
     'Setta la validita' dell'istruzione;
     '''''''''''''''''''''''''''''''''''''''''''''''''
     For y = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
       'SQ-2: temporaneo - valutare caso di segmenti multipli....
       'idField = CercaIdField(wIdSegm, TabIstr.BlkIstr(K).KeyDef(y).Key)
       If TabIstr.BlkIstr(k).IdSegm Then
         'piu' di un elemento: indeterminatezza
         'Mauro 19-03-2007 Multi-Segm: Sarebbe da gestire per Multi-Segmenti
         'IRIS-DB idField = CercaIdField(TabIstr.BlkIstr(k).idSegm, TabIstr.BlkIstr(k).KeyDef(y).Key)
         idField = CercaIdField(TabIstr.BlkIstr(k).IdSegm, TabIstr.BlkIstr(k).KeyDef(y).Key, TabIstr.DBD_TYPE)  'IRIS-DB
       Else
         idField = 0
       End If
        
       'SQ - 27-05-05
       If idField = 0 And TabIstr.BlkIstr(k).KeyDef(y).Key <> "CONCAT" Then 'IRIS-DB brutto ma veloce
         TabIstr.BlkIstr(k).valida = False 'istruzione sbagliata in ogni caso!
         IsValid = False
         '''''''''''''''''''''''''''''''''''''''
         ' SQ 5-05-06 - Nuova segnalazione (I04)
         '''''''''''''''''''''''''''''''''''''''
         addSegnalazione TabIstr.BlkIstr(k).KeyDef(y).Key, "SSAKEYNOTFOUND", TabIstr.BlkIstr(k).KeyDef(y).Key
       End If
  
       'SQ: spostata qui... era fuori ciclo e settava solo la chiave 1!?
       TabIstr.BlkIstr(k).KeyDef(y).xName = GbXName
        
       Set tb1 = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Key")
       tb1.AddNew
       'SQ COPY-ISTR
       'tb1!idOggetto = GbIdOggetto
       tb1!idOggetto = IdOggettoRel
       tb1!idpgm = GbIdOggetto
       tb1!Riga = GbOldNumRec
       tb1!livello = k
       tb1!progressivo = y
       
       tb1!idField = idField
       tb1!idField_moved = TabIstr.BlkIstr(k).KeyDef(y).Key_moved
       'SQ: aspettare il via di Ste per mettere true se idField negativo.
       tb1!xName = TabIstr.BlkIstr(k).KeyDef(y).xName
  
       '''''SONO IN DEBUG: TOGLIERE'''''
       'QUANTI CONTROLLI FACCIAMO? E' GIA' STATO FATTO ALL'INIZIO
       Select Case TabIstr.BlkIstr(k).KeyDef(y).Op
         Case "Nothing"
           tb1!operatore = ""
         Case "SPACE", "SPACES", "SPA"
           tb1!operatore = "SP"
         Case Else
           If Len(TabIstr.BlkIstr(k).KeyDef(y).Op) = 0 Then
             tb1!operatore = " "
           Else
             tb1!operatore = Mid(TabIstr.BlkIstr(k).KeyDef(y).Op, 1, 2) 'tronca a 2 perch� l'operatore � di 2
           End If
       End Select
       
       'SQ: perche' se e' = ""??????????
       If TabIstr.BlkIstr(k).KeyDef(y).Key <> "Nothing" Then
         tb1!Valore = Trim(TabIstr.BlkIstr(k).KeyDef(y).KeyVal & "")
         tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
         'SQ: aggiungere colonna in DB!
         tb1!KeyStart = TabIstr.BlkIstr(k).KeyDef(y).KeyStart
       End If
       tb1!KeyLen = TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ""
        
       'SG : Se la lunghezza non � stata calcolata o calcolata male recupera l'effettiva lunghezza dal field dli
       If Val(tb1!KeyLen) <= 0 Or Trim(tb1!KeyLen) = "" Then
         'AC eliminato
'         tb1!KeyLen = Restituisci_DatiKey(tb1!idField, tb1!xName, "LUNGHEZZA")
'         TabIstr.BlkIstr(k).KeyDef(y).KeyLen = tb1!KeyLen
'
'         If Len(TabIstr.BlkIstr(k).SSAName) Then
'           TabIstr.BlkIstr(k).KeyDef(y).KeyVarField = TabIstr.BlkIstr(k).SSAName & "(" & TabIstr.BlkIstr(k).KeyDef(y).KeyStart & ":" & TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ")"
'           tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
'         End If
       End If
        
       If Len(TabIstr.BlkIstr(k).KeyDef(y).KeyBool) = 0 Then
         tb1!Booleano = " "
       Else
         tb1!Booleano = TabIstr.BlkIstr(k).KeyDef(y).KeyBool
       End If
       'SQ:
       tb1!valida = TabIstr.BlkIstr(k).valida
       tb1!Correzione = False
       tb1.Update
       tb1.Close
    Next y
    
    Dim wSsaPres As Boolean
    '''''''''''''''wSsaPres = Trim(TabIstr.BlkIstr(k).SSAName) <> "<NONE>"
    
    'Ancora il SEGMENTO????
    'If Not isGSAM Then
    '    If TabIstr.BlkIstr(k).IdSegm <= 0 Then TabIstr.BlkIstr(k).Valida = False
    '    If Trim(TabIstr.BlkIstr(k).segment) = "" Then TabIstr.BlkIstr(k).Valida = False
    'End If
   
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' PsDli_istrDett_Dup: COS'E' 'STA ROBA?????
    ' A COSA SERVE LA gSSA?????????????????????
    ' Per la molteplicit�?????????????????????? (forse)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim tbr As Recordset
    Set tbr = m_Fun.Open_Recordset("Select * from PsDli_istrDett_Dup")
    T4 = 0
    wIdStr = ""
    
    For T1 = 1 To UBound(gSSA)
   '   For t2 = 1 To UBound(wDliSsa)
       If k <= UBound(wDliSsa) Then
         If gSSA(T1).Name = wDliSsa(k) Then
            If UBound(gSSA(T1).Segm) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).Segm)
                 T4 = T4 + 1
                 tbr.AddNew
                    'SQ COPY-ISTR
                    'tbr!idOggetto = GbIdOggetto
                    tbr!idOggetto = IdOggettoRel
                    tbr!idpgm = GbIdOggetto
                    tbr!Riga = GbOldNumRec
                    tbr!livello = k
                    tbr!numeropar = T4
                    tbr!Tipo = "SG"
                    tbr!Valore = gSSA(T1).Segm(T3)
                 tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).FIELD) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).FIELD)
                  T4 = T4 + 1
                  tbr.AddNew
                     'SQ COPY-ISTR
                     'tbr!idOggetto = GbIdOggetto
                     tbr!idOggetto = IdOggettoRel
                     tbr!idpgm = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "FL"
                     tbr!Valore = gSSA(T1).FIELD(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).Op) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).Op)
                  T4 = T4 + 1
                  tbr.AddNew
                     'SQ COPY-ISTR
                     'tbr!idOggetto = GbIdOggetto
                     tbr!idOggetto = IdOggettoRel
                     tbr!idpgm = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "OP"
                     tbr!Valore = gSSA(T1).Op(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).value) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).value)
                  T4 = T4 + 1
                  tbr.AddNew
                     'SQ COPY-ISTR
                     'tbr!idOggetto = GbIdOggetto
                     tbr!idOggetto = IdOggettoRel
                     tbr!idpgm = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "VL"
                     tbr!Valore = gSSA(T1).value(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).CodCom) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).CodCom)
                  T4 = T4 + 1
                  tbr.AddNew
                     'SQ COPY-ISTR
                     'tbr!idOggetto = GbIdOggetto
                     tbr!idOggetto = IdOggettoRel
                     tbr!idpgm = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "CC"
                     tbr!Valore = DecodificaCodCom(gSSA(T1).CodCom(T3))
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).parentesi) > 1 Then
               T4 = 0
               wIdStr = getIdArea(gSSA(1).Name)
               For T3 = 1 To UBound(gSSA(T1).parentesi)
                  T4 = T4 + 1
                  If T4 > 2 Then
                     T4 = T4
                  End If
                  tbr.AddNew
                     'SQ COPY-ISTR
                     'tbr!idOggetto = GbIdOggetto
                     tbr!idOggetto = IdOggettoRel
                     tbr!idpgm = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "PA"
                     tbr!Valore = gSSA(T1).parentesi(T3)
                  tbr.Update
               Next T3
            End If
         End If
       End If
  '    Next t2
    Next T1
    If Not TabIstr.BlkIstr(k).valida Then IsValid = False
    tb.Update
  Next k 'ITERAZIONE LIVELLI...
  tb.Close
  
  ' Mauro 23/04/2008 : Istruzione completamente squalificata
  ' aggiungiamo un livello per inserire l'area di I/O; adeguamento alla CALL
  If intProgSeg = 0 Then
    intProgSeg = 1
    ReDim TabIstr.BlkIstr(intProgSeg)
    ' Faccio la ReDim anche sulla KeyDef senn� mi va in errore quando salva la IstrDett_Liv
    ReDim Preserve TabIstr.BlkIstr(intProgSeg).KeyDef(0)
    TabIstr.BlkIstr(intProgSeg).Record = Trim(Replace(intoParam, "INTO", ""))
    If Trim(seglenParam) <> "" Then
      TabIstr.BlkIstr(intProgSeg).SegLen = Trim(Replace(seglenParam, "SEGLENGTH", ""))
    End If
  End If
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' GESTIONE SEGMENTI/AREE: ANCORA?!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'Attenzione: wIdStr NON e' quello usato sopra per i singoli livelli... e' un'altra cosa...
  'SQ-2: If InStr(wIdStr, "/") And wIdSegm > 0 Then
  If InStr(wIdStr, "/") Then
      wStrIdOggetto = Val(Left(wIdStr, 6))
      wStrIdArea = Val(Mid$(wIdStr, 8, 4))
      For y = 0 To UBound(IdSegmenti)
        If Len(Trim(IdSegmenti(y))) Then
          Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & IdSegmenti(y) _
                & " and stridoggetto = " & wStrIdOggetto _
                & " and stridarea = " & wStrIdArea _
                & " and nomecmp = '" & TabIstr.BlkIstr(1).Record & "'")
          If tb2.RecordCount = 0 Then
            tb2.AddNew
            tb2!idSegmento = IdSegmenti(y)
            tb2!stridoggetto = wStrIdOggetto
            tb2!strIdArea = wStrIdArea
            tb2!nomeCmp = TabIstr.BlkIstr(1).Record
            tb2.Update
            tb2.Close
          End If
        End If
     Next y
    End If
  End If

  ''''''''''''''''''''''''''''''''''''''''''''
  ' UPDATE "PsDli_Istruzioni":
  ''''''''''''''''''''''''''''''''''''''''''''
  UpdatePsdliIstruzioni (IsDC)
  
  'SQ ??????????????????
  Dim ssaPresent As Boolean
  'ssaPresent = True
  'SG: Controlla se c'� un livello di ssa
  '...
  
  ''''''''''''''''''''''''''''''''''''''''
  ' Segnalazioni
  ''''''''''''''''''''''''''''''''''''''''
  'Portare nelle rispettive posizioni...!
  If Not IsValid Then
    If Len(TabIstr.DBD) And wIdDBD(0) = 0 Then  'NO DBD
      addSegnalazione TabIstr.DBD, "NODBD", TabIstr.istr
    'ElseIf ubound(wIdDBD) Then   'DBD MULTIPLI
    '  AggiungiSegnalazione dbdName, "DBDS", istruzione
    ElseIf Trim(SwSegm) <> "" Then  'SQ ?
      addSegnalazione SwSegm, "NOSEGM", TabIstr.istr
    Else
     'Errore Generico (SEGMENTO!)
     addSegnalazione "", "INVALIDDLIISTR", ""
    End If
  End If
  Exit Sub
parseErr:
  addSegnalazione parametri, "I00", parametri
End Sub

Public Sub parseREPL_exec(istruzione As String, parametri As String)
  Dim rs As Recordset, tb As Recordset, tb1 As Recordset, tb2 As Recordset, rsdbd As Recordset
  Dim i As Integer, k As Integer, y As Integer, T1 As Integer, T2 As Integer, T3 As Integer, T4 As Integer
  Dim dbdname As String, wNomePsb As String, ssaValue As String
  Dim isGSAM As Boolean, unqualified As Boolean
  Dim idField As Integer
  'Ac
  Dim strParametro   As String
  Dim intProgSeg As Integer
  ' Mauro
  Dim fromParam As String, whereParam As String, intoParam As String, nameSeg As String
  
  On Error GoTo parseErr
  
  ReDim gSSA(0)
  ReDim dbdPgm(0)
  ReDim TabIstr.BlkIstr(0)
  
  intProgSeg = 0
  
  ' Mauro 17/04/2009 :  Devo gestire le opzioni se vengono prima di SEGMENT nelle REPL
  If istruzione = "REPL" Then
    While Len(parametri)
      strParametro = nexttoken(parametri)
      Select Case strParametro
        Case "PCB"
          TabIstr.pcb = EliminaParentesi(nexttoken(parametri))
          TabIstr.numpcb = getPCBnumber(TabIstr.pcb, True)
        Case "FROM"
          If intProgSeg = 0 Then
            fromParam = strParametro & " " & nexttoken(parametri) & " "
          End If
        Case "WHERE"
          If intProgSeg = 0 Then
            whereParam = strParametro & " " & nexttoken(parametri) & " "
          End If
        Case "INTO"
          If intProgSeg = 0 Then
            intoParam = strParametro & " " & nexttoken(parametri) & " "
          End If
        Case "SEGMENT"
          ' incrementa blkIstrDli
          intProgSeg = intProgSeg + 1
          ReDim Preserve TabIstr.BlkIstr(intProgSeg)
          If whereParam = "" And intoParam = "" And fromParam = "" Then
            ParseSegmentExec parametri, intProgSeg
          Else
            nameSeg = nexttoken(parametri)
            parametri = nameSeg & " " & intoParam & fromParam & whereParam & parametri
            ParseSegmentExec parametri, intProgSeg
            whereParam = ""
            intoParam = ""
            fromParam = ""
          End If
        Case "VARIABLE" 'IRIS-DB per ora semplicemente ignorato, tanto lo sappiamo dal DBD e all'incapsulatore non interessa
        Case Else
          addSegnalazione strParametro, "I00", strParametro
      End Select
    Wend
  ElseIf istruzione = "DLET" Then
    While Len(parametri)
      strParametro = nexttoken(parametri)
      Select Case strParametro
        Case "PCB"
          TabIstr.pcb = EliminaParentesi(nexttoken(parametri))
          TabIstr.numpcb = getPCBnumber(TabIstr.pcb, True)
        Case "SEGMENT"
          ' incrementa blkIstrDli
          intProgSeg = intProgSeg + 1
          ReDim Preserve TabIstr.BlkIstr(intProgSeg)
          ParseSegmentExec parametri, intProgSeg
        Case "VARIABLE" 'IRIS-DB per ora semplicemente ignorato, tanto lo sappiamo dal DBD e all'incapsulatore non interessa
      End Select
    Wend
  End If
  TabIstr.istr = istruzione
    
  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  ' **** TabIstr.pcb = nextToken(parametri)
  
  ' ***** sposto nel parser segmento wDliRec = nextToken(parametri)
       
'  Dim ssa
'  ReDim wDliSsa(0)
'  ReDim TabIstrSSa(0)
'  ReDim TabIstrSSa(0).NomeSegmento.value(0)
'  Dim ssaOk As Boolean
'  ssaOk = True
 
  ''''''''''''''''''''''''''''''''''''''''
  ' SQ 20-04-06
  ' Area Segmento - la associava a tutti!
  '''''''''''''''''''''''''''''''''''''''''
'  If Not unqualified Then
'    TabIstrSSa(UBound(TabIstrSSa)).Struttura = wDliRec
'    If Len(TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0)) = 0 And Len(wDliRec) Then  'Solo sul Segmento finale...
'      ''''''''''''''''''''''''''''''''''''''''''''''
'      ' SQ 2-05-06
'      ' Naming Convention SEGMENTO/AREA
'      ''''''''''''''''''''''''''''''''''''''''''''''
'      Dim env As Collection 'area,idDbd
'      If Len(segmentNameParam) Then
'        Set env = New Collection  'resetto;
'        env.Add wDliRec
'        'SQ 5-06-06 - Come poteva funzionare?
'        env.Add 0 'wIdDBD(0)
'        'SQ MOVED/VALUE
'        TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0) = getEnvironmentParam(segmentNameParam, "IMS", env)
'      End If
'    End If
'    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    'SQ - Cambiamento mostruoso (pezzo di analizzaCallDli_SQ)
'    ' TabIstrSSa -> TabIstr
'    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    'getTabIstr
'  End If
  
  ''''''''''''''''''''''''''''''''
  ' Gestione PSB/DBD:
  ' Input: N PSB, PCB, M SEGMENTI
  ''''''''''''''''''''''''''''''''
  getInstructionPsbDbd (TabIstr.numpcb)
  
  'SQ - 27-04-06
  'If isGSAM Then
  If TabIstr.DBD_TYPE = DBD_GSAM Then
    getGSAMinfo (TabIstr.numpcb)  'wIDdbd...
  Else
    ''''''''''''''SQ - 27-04-06 (sopra: serve cmq) getInstructionPsbDbd (TabIstr.numPCB)
    ''''''''''''''''''''''''''''''''''''''''''
    ' TabIstr.BlkIstr:
    ''''''''''''''''''''''''''''''''''''''''''
    If IsValid Then  'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
      getTabIstrIds
    Else
      If Not unqualified Then
        IsValid = False
      End If
    End If
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'SQ 10-04-06
  ' Se usciamo qui perdiamo tutti i settaggi per l'istruzione!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If IsValid And Not unqualified Then 'SQ 27-04-06
    ''''''''''''''''''''''''''''''''''''
    'ITERAZIONE LIVELLI ISTRUZIONE:
    ''
    'AGGIORNAMENTO "PsDli_IstrDett_Liv"
    ''''''''''''''''''''''''''''''''''''
    Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
    For k = 1 To UBound(TabIstr.BlkIstr)
      '''''''''''''''''''''
      'SEGMENTO VARIABILE?
      '''''''''''''''''''''
      If Left(TabIstr.BlkIstr(k).Segment, 1) = "(" And Right(TabIstr.BlkIstr(k).Segment, 1) = ")" Then
        ReDim IdSegmenti(0)
        '''''''''''''''''''''''''''''''''''''
        ' Segmento variabile: gestione temp.
        '''''''''''''''''''''''''''''''''''''
      End If
      
      'Dim wNomeCmpLiv As String, wIdStr As String, wStrIdOggetto As String, wStrIdArea As Long
      Dim wNomeCmpLiv As String, wIdStr As String, wStrIdOggetto As Long, wStrIdArea As Long
      
      wIdStr = getIdArea(TabIstr.BlkIstr(k).Record)
      wNomeCmpLiv = TabIstr.BlkIstr(k).Record
      
      'ADESSO HO N SEGMENTI QUANTI SONO I DBD... (o ZERO!)
      'SQ 28-11-05:
      'Mauro 19-03-2007 Multi-Segm
      'If TabIstr.BlkIstr(k).IdSegm Then
      If Len(TabIstr.BlkIstr(k).IdSegmenti) Then
        Dim IdSegmentiD() As String
        Dim a As Integer
        IdSegmentiD() = Split(TabIstr.BlkIstr(k).IdSegmenti, ";")
        For a = 0 To UBound(IdSegmentiD())
          wStrIdOggetto = Val(Left(wIdStr, 6))
          wStrIdArea = Val(Mid(wIdStr, 8, 4))
          'SQ - 28-11-05
          'Pezza provvisoria: capire perche' arriva cosi'...
          If wStrIdOggetto <> 0 And wStrIdArea <> 0 Then
            Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & IdSegmentiD(a) _
                 & " and stridoggetto = " & wStrIdOggetto _
                 & " and stridarea = " & wStrIdArea _
                 & " and nomecmp = '" & wNomeCmpLiv & "'")
                      
            If tb2.RecordCount = 0 Then
               tb2.AddNew
            End If
            
            tb2!idSegmento = IdSegmentiD(a)
            tb2!stridoggetto = wStrIdOggetto
            tb2!strIdArea = wStrIdArea
            tb2!nomeCmp = wNomeCmpLiv
            'AC
            tb2.Update
            tb2.Close
          End If
        Next a
     End If
    
     If TabIstr.DBD_TYPE <> DBD_GSAM Then
       If Len(IdSegmenti(0)) = 0 Then SwSegm = TabIstr.BlkIstr(k).Segment
     End If
     
     
    
    Dim wNomeCmp As String
     i = InStr(wIdStr, "-")
     If i > 0 Then
        wNomeCmp = Mid(wIdStr, i + 2)
        wIdStr = Val(Mid(wIdStr, 1, i - 1))
     Else
        wNomeCmp = " "
     End If
    
     ''''''''''''''''''''''''''''''''''''''
     'INSERIMENTO IN "PsDli_IstrDett_Liv":
     ''''''''''''''''''''''''''''''''''''''
     tb.AddNew
     'SQ COPY-ISTR
     'tb!idOggetto = GbIdOggetto
     tb!idOggetto = IdOggettoRel
     tb!idpgm = GbIdOggetto
     tb!Riga = GbOldNumRec
     tb!livello = k
     ' tb!dtsegnalazione = Now
    
    'SQ: Cos'� 'sta roba???
    'dovremo aggiungere quei due campi nella TabIstr!!!!!!!!!!!!!!!!!!!!
'    For i = 0 To UBound(TabIstrSSa)
'      If Trim(TabIstrSSa(i).SSAName) = Trim(TabIstr.BlkIstr(k).SSAName) Then
'        tb!stridoggetto = TabIstrSSa(i).idOggetto
'        tb!strIdArea = TabIstrSSa(i).idArea
'        Exit For
'      End If
'    Next
    
     'SQ-2: CHIARIRE: LI HO APPENA CALCOLATI SOPRA!?
    ' y = InStr(wIdStr, "/")
     'If y > 0 Then
        'tb!strIdOggetto = Left(wIdStr, 6)
        'tb!strIdArea = Mid(wIdStr, y + 1, 4) 'SOPRA USO 6 E QUI Y?!
     'Else
      ''SQ - controllare: era sbagliato! wStrIdOggetto > 0... ma � string!?
        'If wStrIdArea > 0 And Len(wStrIdOggetto) Then
           'tb!strIdOggetto = wStrIdOggetto
           'tb!strIdArea = wStrIdArea
        'End If
     'End If


     'SQ-2: attenzione: ne ho N!
     'EVENTUALMENTE FARE TABELLA DEDICATA...
     '(IN TEORIA NE BASTA 1 PER RECUPERARE IL NOME... POI SE SERVE HO LA TABELLA DBD/ISTRUZIONE)
          
        'Mauro 19-03-2007 Multi-Segm
        'If TabIstr.BlkIstr(k).IdSegm = 0 Then
        If Len(TabIstr.BlkIstr(k).IdSegmenti) = 0 Then
          'Scamuffo I/O AREA -> SEGMENTO (Ha senso solo sull'ultimo livello - volendo si pu� rendere opzionale)
          If k = UBound(TabIstr.BlkIstr) Then
            getSegment_byArea TabIstr.BlkIstr(k)
          End If
        End If
        'If TabIstr.BlkIstr(k).IdSegm Then
        If Len(TabIstr.BlkIstr(k).IdSegmenti) Then
           tb!idSegmento = TabIstr.BlkIstr(k).IdSegm
           tb!IdSegmentiD = TabIstr.BlkIstr(k).IdSegmenti
        Else
          'Cerca l'id segmento nelle tabelle mg (id segmento associato da noi)
          tb!idSegmento = getIdSegmento_By_MgTable(TabIstr.BlkIstr(k).Record)
          tb!IdSegmentiD = tb!idSegmento
          'SQ 9-05-06: nuova gestione segmenti... non serve pi�
'          If tb!IdSegmento = 0 Then
'             'se � un gsam associa l'unico segmento possibile legato a quel dbd
'             If TabIstr.DBD_TYPE = DBD_GSAM Then
'                'Usa wPcb!!!! modificare...
'                tb!IdSegmento = Assegna_IdSegmento_ByNomePCB_GSAM()
'             End If
'          End If
          
          '''''''''''''''''''''''''''''''''''''''''''''
          'SQ -10-04-06
          'Segmento non trovata: istruzione INVALIDA!
          '''''''''''''''''''''''''''''''''''''''''''''
          If tb!idSegmento Then
            TabIstr.BlkIstr(k).IdSegm = tb!idSegmento
            TabIstr.BlkIstr(k).IdSegmenti = tb!idSegmento
            TabIstr.BlkIstr(k).Segment = getNomeSegmento(tb!idSegmento)
            TabIstr.BlkIstr(k).Segmenti = TabIstr.BlkIstr(k).Segment
            If Len(TabIstr.BlkIstr(k).SegLen) = 0 Then
              TabIstr.BlkIstr(k).SegLen = getLenSegmentoByIdSegmento(tb!idSegmento)
              TabIstr.BlkIstr(k).SegmentiLen = TabIstr.BlkIstr(k).SegLen
             End If
            Else
              IsValid = False
            End If
      
     End If
     
     'E' sempre vuoto! servirebbe una "getLenSegmentoByIdSegmento(tb!IdSegmento)",
     'ma metterlo a posto a monte...
     'Mauro 19-03-2007 Multi-Segm
     'If IsNumeric(TabIstr.BlkIstr(k).SegLen) Then 'blank/null (numerico <- stringa) !
     If Len(TabIstr.BlkIstr(k).SegmentiLen) Then
       If TabIstr.BlkIstr(k).SegLen Then
         tb!SegLen = CInt(TabIstr.BlkIstr(k).SegLen)
       Else
         'Imposto il primo valore tra quelli multipli
         Dim SegmentLen() As String
         SegmentLen() = Split(TabIstr.BlkIstr(k).SegmentiLen, ";")
         tb!SegLen = SegmentLen(0)
       End If
       tb!SegmentiLend = TabIstr.BlkIstr(k).SegmentiLen
     Else
       tb!SegLen = 0
       tb!SegmentiLend = 0
     End If
     tb!NomeSSA = TabIstr.BlkIstr(k).SSAName   'togliere sti bianchi in fondo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     tb!dataarea = TabIstr.BlkIstr(k).Record
     
     If Len(TabIstr.BlkIstr(k).CodCom) Then
         TabIstr.BlkIstr(k).Search = DecodificaCodCom(TabIstr.BlkIstr(k).CodCom)
         If InStr(TabIstr.BlkIstr(k).Search, "Undefined") Then
           TabIstr.BlkIstr(k).valida = False
           'NUOVA SEGNALAZIONE
         End If
     End If
        
     'Command code
     tb!condizione = Replace(TabIstr.BlkIstr(k).CodCom, "-", "")
     tb!Qualificazione = TabIstr.BlkIstr(k).qualified
     tb!Correzione = False
     tb!moved = TabIstr.BlkIstr(k).moved
     
     'SQ - 27-05-05: aspettare la validita' delle chiavi per chiudere...
    
     '''''''''''''''''''''''''''''''''''''''''''''''''
     'GESTIONE CHIAVI:
     'Cerca ID campo chiave
     'Allestisce tabella record in PsDli_IstrDett_Key
     'Setta la validita' dell'istruzione;
     '''''''''''''''''''''''''''''''''''''''''''''''''
     For y = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
        'SQ-2: temporaneo - valutare caso di segmenti multipli....
        'idField = CercaIdField(wIdSegm, TabIstr.BlkIstr(K).KeyDef(y).Key)
        If TabIstr.BlkIstr(k).IdSegm Then
           'piu' di un elemento: indeterminatezza
           'Mauro 19-03-2007 Multi-Segm: Sarebbe da gestire per Multi-Segmenti
           idField = CercaIdField(TabIstr.BlkIstr(k).IdSegm, TabIstr.BlkIstr(k).KeyDef(y).Key, TabIstr.DBD_TYPE)
        Else
           idField = 0
        End If
        
        'SQ - 27-05-05
        If idField = 0 Then
          TabIstr.BlkIstr(k).valida = False 'istruzione sbagliata in ogni caso!
          IsValid = False
          '''''''''''''''''''''''''''''''''''''''
          ' SQ 5-05-06 - Nuova segnalazione (I04)
          '''''''''''''''''''''''''''''''''''''''
          addSegnalazione TabIstr.BlkIstr(k).KeyDef(y).Key, "SSAKEYNOTFOUND", TabIstr.BlkIstr(k).KeyDef(y).Key
        End If
  
        'SQ: spostata qui... era fuori ciclo e settava solo la chiave 1!?
        TabIstr.BlkIstr(k).KeyDef(y).xName = GbXName
        
        Set tb1 = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Key")
        tb1.AddNew
        'SQ COPY-ISTR
        'tb1!idOggetto = GbIdOggetto
        tb1!idOggetto = IdOggettoRel
        tb1!idpgm = GbIdOggetto
        tb1!Riga = GbOldNumRec
        tb1!livello = k
        tb1!progressivo = y
        
        tb1!idField = idField
        tb1!idField_moved = TabIstr.BlkIstr(k).KeyDef(y).Key_moved
        'SQ: aspettare il via di Ste per mettere true se idField negativo.
        tb1!xName = TabIstr.BlkIstr(k).KeyDef(y).xName
    
        '''''SONO IN DEBUG: TOGLIERE'''''
        'QUANTI CONTROLLI FACCIAMO? E' GIA' STATO FATTO ALL'INIZIO
        Select Case TabIstr.BlkIstr(k).KeyDef(y).Op
          Case "Nothing"
            tb1!operatore = ""
          Case "SPACE", "SPACES", "SPA"
            tb1!operatore = "SP"
          Case Else
            If Len(TabIstr.BlkIstr(k).KeyDef(y).Op) = 0 Then
              tb1!operatore = " "
            Else
              tb1!operatore = Mid(TabIstr.BlkIstr(k).KeyDef(y).Op, 1, 2) 'tronca a 2 perch� l'operatore � di 2
            End If
        End Select
        
        'SQ: perche' se e' = ""??????????
        If TabIstr.BlkIstr(k).KeyDef(y).Key <> "Nothing" Then
          tb1!Valore = Trim(TabIstr.BlkIstr(k).KeyDef(y).KeyVal & "")
          tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
          'SQ: aggiungere colonna in DB!
          tb1!KeyStart = TabIstr.BlkIstr(k).KeyDef(y).KeyStart
        End If
  
        tb1!KeyLen = TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ""
        
        'SG : Se la lunghezza non � stata calcolata o calcolata male recupera l'effettiva lunghezza dal field dli
        If Val(tb1!KeyLen) <= 0 Or Trim(tb1!KeyLen) = "" Then
           tb1!KeyLen = Restituisci_DatiKey(tb1!idField, tb1!xName, "LUNGHEZZA")
           TabIstr.BlkIstr(k).KeyDef(y).KeyLen = tb1!KeyLen
           
           If Len(TabIstr.BlkIstr(k).SSAName) Then
              TabIstr.BlkIstr(k).KeyDef(y).KeyVarField = TabIstr.BlkIstr(k).SSAName & "(" & TabIstr.BlkIstr(k).KeyDef(y).KeyStart & ":" & TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ")"
              tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
           End If
        End If
        
        If Len(TabIstr.BlkIstr(k).KeyDef(y).KeyBool) = 0 Then
          tb1!Booleano = " "
        Else
          tb1!Booleano = TabIstr.BlkIstr(k).KeyDef(y).KeyBool
        End If
        'SQ:
        tb1!valida = TabIstr.BlkIstr(k).valida
        tb1!Correzione = False
        tb1.Update
        tb1.Close
    Next y
    
    Dim wSsaPres As Boolean
    '''''''''''''''wSsaPres = Trim(TabIstr.BlkIstr(k).SSAName) <> "<NONE>"
    
    'Ancora il SEGMENTO????
    'If Not isGSAM Then
    '    If TabIstr.BlkIstr(k).IdSegm <= 0 Then TabIstr.BlkIstr(k).Valida = False
    '    If Trim(TabIstr.BlkIstr(k).segment) = "" Then TabIstr.BlkIstr(k).Valida = False
    'End If
      
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' PsDli_istrDett_Dup: COS'E' 'STA ROBA?????
    ' A COSA SERVE LA gSSA?????????????????????
    ' Per la molteplicit�?????????????????????? (forse)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim tbr As Recordset
    Set tbr = m_Fun.Open_Recordset("Select * from PsDli_istrDett_Dup")
    T4 = 0
    wIdStr = ""
    
    For T1 = 1 To UBound(gSSA)
   '   For t2 = 1 To UBound(wDliSsa)
       If k <= UBound(wDliSsa) Then
         If gSSA(T1).Name = wDliSsa(k) Then
            If UBound(gSSA(T1).Segm) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).Segm)
                 T4 = T4 + 1
                 tbr.AddNew
                    'SQ COPY-ISTR
                    'tbr!idOggetto = GbIdOggetto
                    tbr!idOggetto = IdOggettoRel
                    tbr!idpgm = GbIdOggetto
                    tbr!Riga = GbOldNumRec
                    tbr!livello = k
                    tbr!numeropar = T4
                    tbr!Tipo = "SG"
                    tbr!Valore = gSSA(T1).Segm(T3)
                 tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).FIELD) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).FIELD)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "FL"
                     tbr!Valore = gSSA(T1).FIELD(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).Op) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).Op)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "OP"
                     tbr!Valore = gSSA(T1).Op(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).value) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).value)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "VL"
                     tbr!Valore = gSSA(T1).value(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).CodCom) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).CodCom)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "CC"
                     tbr!Valore = DecodificaCodCom(gSSA(T1).CodCom(T3))
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).parentesi) > 1 Then
               T4 = 0
               wIdStr = getIdArea(gSSA(1).Name)
               For T3 = 1 To UBound(gSSA(T1).parentesi)
                  T4 = T4 + 1
                  If T4 > 2 Then
                     T4 = T4
                  End If
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "PA"
                     tbr!Valore = gSSA(T1).parentesi(T3)
                  tbr.Update
               Next T3
            End If
         End If
       End If
  '    Next t2
    Next T1
    If Not TabIstr.BlkIstr(k).valida Then IsValid = False
    tb.Update
  Next k 'ITERAZIONE LIVELLI...
  tb.Close
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' GESTIONE SEGMENTI/AREE: ANCORA?!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'Attenzione: wIdStr NON e' quello usato sopra per i singoli livelli... e' un'altra cosa...
  'SQ-2: If InStr(wIdStr, "/") And wIdSegm > 0 Then
  If InStr(wIdStr, "/") Then
      wStrIdOggetto = Val(Left(wIdStr, 6))
      wStrIdArea = Val(Mid$(wIdStr, 8, 4))
      For y = 0 To UBound(IdSegmenti)
        If Len(Trim(IdSegmenti(y))) Then
          Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & IdSegmenti(y) _
                & " and stridoggetto = " & wStrIdOggetto _
                & " and stridarea = " & wStrIdArea _
                & " and nomecmp = '" & TabIstr.BlkIstr(1).Record & "'")
          If tb2.RecordCount = 0 Then
            tb2.AddNew
            tb2!idSegmento = IdSegmenti(y)
            tb2!stridoggetto = wStrIdOggetto
            tb2!strIdArea = wStrIdArea
            tb2!nomeCmp = TabIstr.BlkIstr(1).Record
            tb2.Update
            tb2.Close
          End If
        End If
     Next y
    End If
  End If

  ''''''''''''''''''''''''''''''''''''''''''''
  ' UPDATE "PsDli_Istruzioni":
  ''''''''''''''''''''''''''''''''''''''''''''
  UpdatePsdliIstruzioni (IsDC)
  
  'SQ ??????????????????
  Dim ssaPresent As Boolean
  'ssaPresent = True
  'SG: Controlla se c'� un livello di ssa
  '...
    
  ''''''''''''''''''''''''''''''''''''''''
  ' Segnalazioni
  ''''''''''''''''''''''''''''''''''''''''
  'Portare nelle rispettive posizioni...!
  If Not IsValid Then
    If Len(TabIstr.DBD) And wIdDBD(0) = 0 Then  'NO DBD
      addSegnalazione TabIstr.DBD, "NODBD", TabIstr.istr
    'ElseIf ubound(wIdDBD) Then   'DBD MULTIPLI
    '  AggiungiSegnalazione dbdName, "DBDS", istruzione
    ElseIf Trim(SwSegm) <> "" Then  'SQ ?
      addSegnalazione SwSegm, "NOSEGM", TabIstr.istr
    Else
     'Errore Generico (SEGMENTO!)
     addSegnalazione "", "INVALIDDLIISTR", ""
    End If
  End If
  Exit Sub
parseErr:
  addSegnalazione parametri, "I00", parametri
End Sub

Public Sub parseISRT_exec(istruzione As String, parametri As String)
  Dim rs As Recordset, tb As Recordset, tb1 As Recordset, tb2 As Recordset, rsdbd As Recordset
  Dim i As Integer, k As Integer, y As Integer, T1 As Integer, T2 As Integer, T3 As Integer, T4 As Integer
  Dim dbdname As String, wNomePsb As String, ssaValue As String
  Dim isGSAM As Boolean, unqualified As Boolean
  Dim idField As Integer, isComFirst As Boolean, whereParam As String, nameSeg As String
  Dim intoParam As String, fromParam As String
  Dim isComCurrent 'IRIS-DB
   
  On Error GoTo parseErr
  
  ReDim gSSA(0)
  ReDim dbdPgm(0)
  ReDim TabIstr.BlkIstr(0)
  
  'Ac
  
  Dim strParametro   As String
  Dim intProgSeg As Integer
  
  intProgSeg = 0
  
  While Len(parametri)
    strParametro = nexttoken(parametri)
    Select Case strParametro
      Case "WHERE"
        If intProgSeg = 0 Then
          whereParam = strParametro & " " & nexttoken(parametri) & " "
        End If
      Case "INTO"
        If intProgSeg = 0 Then
          intoParam = strParametro & " " & nexttoken(parametri) & " "
        End If
      Case "FROM"
        If intProgSeg = 0 Then
          fromParam = strParametro & " " & nexttoken(parametri) & " "
        End If
      Case "FIRST"
        isComFirst = True
      Case "CURRENT"
        isComCurrent = True
      Case "PCB"
        TabIstr.pcb = EliminaParentesi(nexttoken(parametri))
        TabIstr.numpcb = getPCBnumber(TabIstr.pcb, True)
      Case "SEGMENT"
        ' incrementa blkIstrDli
        intProgSeg = intProgSeg + 1
        ReDim Preserve TabIstr.BlkIstr(intProgSeg)
        If isComFirst Then
          TabIstr.BlkIstr(intProgSeg).CodCom = "*F"
        End If
        If isComCurrent Then
          TabIstr.BlkIstr(intProgSeg).CodCom = "*V"
        End If
        If whereParam = "" And intoParam = "" And fromParam = "" Then
          ParseSegmentExec parametri, intProgSeg
        Else
          nameSeg = nexttoken(parametri)
          parametri = nameSeg & " " & intoParam & fromParam & whereParam & parametri
          ParseSegmentExec parametri, intProgSeg
          whereParam = ""
          intoParam = ""
          fromParam = ""
        End If
        isComFirst = False
        isComCurrent = False
      Case "VARIABLE" 'IRIS-DB per ora semplicemente ignorato, tanto lo sappiamo dal DBD e all'incapsulatore non interessa
      Case Else
        addSegnalazione strParametro, "I00", strParametro
    End Select
  Wend
  
  TabIstr.istr = istruzione
    
  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  ' **** TabIstr.pcb = nextToken(parametri)
  
  ' ***** sposto nel parser segmento wDliRec = nextToken(parametri)
       
'  Dim ssa
'  ReDim wDliSsa(0)
'  ReDim TabIstrSSa(0)
'  ReDim TabIstrSSa(0).NomeSegmento.value(0)
'  Dim ssaOk As Boolean
'  ssaOk = True
  
  
  ''''''''''''''''''''''''''''''''''''''''
  ' SQ 20-04-06
  ' Area Segmento - la associava a tutti!
  '''''''''''''''''''''''''''''''''''''''''
'  If Not unqualified Then
'    TabIstrSSa(UBound(TabIstrSSa)).Struttura = wDliRec
'    If Len(TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0)) = 0 And Len(wDliRec) Then  'Solo sul Segmento finale...
'      ''''''''''''''''''''''''''''''''''''''''''''''
'      ' SQ 2-05-06
'      ' Naming Convention SEGMENTO/AREA
'      ''''''''''''''''''''''''''''''''''''''''''''''
'      Dim env As Collection 'area,idDbd
'      If Len(segmentNameParam) Then
'        Set env = New Collection  'resetto;
'        env.Add wDliRec
'        'SQ 5-06-06 - Come poteva funzionare?
'        env.Add 0 'wIdDBD(0)
'        'SQ MOVED/VALUE
'        TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0) = getEnvironmentParam(segmentNameParam, "IMS", env)
'      End If
'    End If
'    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    'SQ - Cambiamento mostruoso (pezzo di analizzaCallDli_SQ)
'    ' TabIstrSSa -> TabIstr
'    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'    'getTabIstr
'  End If
  
  
  ''''''''''''''''''''''''''''''''
  ' Gestione PSB/DBD:
  ' Input: N PSB, PCB, M SEGMENTI
  ''''''''''''''''''''''''''''''''
  getInstructionPsbDbd (TabIstr.numpcb)
  
  'SQ - 27-04-06
  'If isGSAM Then
  If TabIstr.DBD_TYPE = DBD_GSAM Then
    getGSAMinfo (TabIstr.numpcb)  'wIDdbd...
  Else
    ''''''''''''''SQ - 27-04-06 (sopra: serve cmq) getInstructionPsbDbd (TabIstr.numPCB)
    ''''''''''''''''''''''''''''''''''''''''''
    ' TabIstr.BlkIstr:
    ''''''''''''''''''''''''''''''''''''''''''
    If IsValid Then  'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
      getTabIstrIds
    Else
      If Not unqualified Then
        IsValid = False
      End If
    End If
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'SQ 10-04-06
  ' Se usciamo qui perdiamo tutti i settaggi per l'istruzione!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If IsValid And Not unqualified Then 'SQ 27-04-06
    ''''''''''''''''''''''''''''''''''''
    'ITERAZIONE LIVELLI ISTRUZIONE:
    ''
    'AGGIORNAMENTO "PsDli_IstrDett_Liv"
    ''''''''''''''''''''''''''''''''''''
    Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
    For k = 1 To UBound(TabIstr.BlkIstr)
      '''''''''''''''''''''
      'SEGMENTO VARIABILE?
      '''''''''''''''''''''
      If Left(TabIstr.BlkIstr(k).Segment, 1) = "(" And Right(TabIstr.BlkIstr(k).Segment, 1) = ")" Then
        ReDim IdSegmenti(0)
        '''''''''''''''''''''''''''''''''''''
        ' Segmento variabile: gestione temp.
        '''''''''''''''''''''''''''''''''''''
      End If
      
      'Dim wNomeCmpLiv As String, wIdStr As String, wStrIdOggetto As String, wStrIdArea As Long
      Dim wNomeCmpLiv As String, wIdStr As String, wStrIdOggetto As Long, wStrIdArea As Long
      
      wIdStr = getIdArea(TabIstr.BlkIstr(k).Record)
      wNomeCmpLiv = TabIstr.BlkIstr(k).Record
      
      'ADESSO HO N SEGMENTI QUANTI SONO I DBD... (o ZERO!)
      
     
      'SQ 28-11-05:
      'Mauro 19-03-2007 Multi-Segm
      'If TabIstr.BlkIstr(k).IdSegm Then
      If Len(TabIstr.BlkIstr(k).IdSegmenti) Then
        Dim IdSegmentiD() As String
        Dim a As Integer
        IdSegmentiD() = Split(TabIstr.BlkIstr(k).IdSegmenti, ";")
        For a = 0 To UBound(IdSegmentiD())
          wStrIdOggetto = Val(Left(wIdStr, 6))
          wStrIdArea = Val(Mid(wIdStr, 8, 4))
          'SQ - 28-11-05
          'Pezza provvisoria: capire perche' arriva cosi'...
          If wStrIdOggetto <> 0 And wStrIdArea <> 0 Then
            Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & IdSegmentiD(a) _
                 & " and stridoggetto = " & wStrIdOggetto _
                 & " and stridarea = " & wStrIdArea _
                 & " and nomecmp = '" & wNomeCmpLiv & "'")
                      
            If tb2.RecordCount = 0 Then
               tb2.AddNew
            End If
            
            tb2!idSegmento = IdSegmentiD(a)
            tb2!stridoggetto = wStrIdOggetto
            tb2!strIdArea = wStrIdArea
            tb2!nomeCmp = wNomeCmpLiv
            'AC
            tb2.Update
            tb2.Close
          End If
        Next a
      End If
         
     If TabIstr.DBD_TYPE <> DBD_GSAM Then
       If Len(IdSegmenti(0)) = 0 Then SwSegm = TabIstr.BlkIstr(k).Segment
     End If
    
     Dim wNomeCmp As String
     i = InStr(wIdStr, "-")
     If i > 0 Then
        wNomeCmp = Mid(wIdStr, i + 2)
        wIdStr = Val(Mid(wIdStr, 1, i - 1))
     Else
        wNomeCmp = " "
     End If
    
     ''''''''''''''''''''''''''''''''''''''
     'INSERIMENTO IN "PsDli_IstrDett_Liv":
     ''''''''''''''''''''''''''''''''''''''
     tb.AddNew
     'SQ COPY-ISTR
     'tb!idOggetto = GbIdOggetto
     tb!idOggetto = IdOggettoRel
     tb!idpgm = GbIdOggetto
     tb!Riga = GbOldNumRec
     tb!livello = k
     If Not Len(TabIstr.BlkIstr(k).Varsegment) = 0 Then
      tb!VariabileSegmento = TabIstr.BlkIstr(k).Varsegment
     End If
     ' tb!dtsegnalazione = Now
    
    'SQ: Cos'� 'sta roba???
    'dovremo aggiungere quei due campi nella TabIstr!!!!!!!!!!!!!!!!!!!!
'    For i = 0 To UBound(TabIstrSSa)
'      If Trim(TabIstrSSa(i).SSAName) = Trim(TabIstr.BlkIstr(k).SSAName) Then
'        tb!stridoggetto = TabIstrSSa(i).idOggetto
'        tb!strIdArea = TabIstrSSa(i).idArea
'        Exit For
'      End If
'    Next
    
     'SQ-2: CHIARIRE: LI HO APPENA CALCOLATI SOPRA!?
    ' y = InStr(wIdStr, "/")
     'If y > 0 Then
        'tb!strIdOggetto = Left(wIdStr, 6)
        'tb!strIdArea = Mid(wIdStr, y + 1, 4) 'SOPRA USO 6 E QUI Y?!
     'Else
      ''SQ - controllare: era sbagliato! wStrIdOggetto > 0... ma � string!?
        'If wStrIdArea > 0 And Len(wStrIdOggetto) Then
           'tb!strIdOggetto = wStrIdOggetto
           'tb!strIdArea = wStrIdArea
        'End If
     'End If


     'SQ-2: attenzione: ne ho N!
     'EVENTUALMENTE FARE TABELLA DEDICATA...
     '(IN TEORIA NE BASTA 1 PER RECUPERARE IL NOME... POI SE SERVE HO LA TABELLA DBD/ISTRUZIONE)
        'Mauro 19-03-2007 Multi-Segm
        'If TabIstr.BlkIstr(k).IdSegm = 0 Then
        If Len(TabIstr.BlkIstr(k).IdSegmenti) = 0 Then
          'Scamuffo I/O AREA -> SEGMENTO (Ha senso solo sull'ultimo livello - volendo si pu� rendere opzionale)
          If k = UBound(TabIstr.BlkIstr) Then
            getSegment_byArea TabIstr.BlkIstr(k)
          End If
        End If
        'If TabIstr.BlkIstr(k).IdSegm Then
        If Len(TabIstr.BlkIstr(k).IdSegmenti) Then
           tb!idSegmento = TabIstr.BlkIstr(k).IdSegm
           tb!IdSegmentiD = TabIstr.BlkIstr(k).IdSegmenti
        Else
          'Cerca l'id segmento nelle tabelle mg (id segmento associato da noi)
          tb!idSegmento = getIdSegmento_By_MgTable(TabIstr.BlkIstr(k).Record)
          tb!IdSegmentiD = tb!idSegmento
          'SQ 9-05-06: nuova gestione segmenti... non serve pi�
'          If tb!IdSegmento = 0 Then
'             'se � un gsam associa l'unico segmento possibile legato a quel dbd
'             If TabIstr.DBD_TYPE = DBD_GSAM Then
'                'Usa wPcb!!!! modificare...
'                tb!IdSegmento = Assegna_IdSegmento_ByNomePCB_GSAM()
'             End If
'          End If
          
          '''''''''''''''''''''''''''''''''''''''''''''
          'SQ -10-04-06
          'Segmento non trovata: istruzione INVALIDA!
          '''''''''''''''''''''''''''''''''''''''''''''
          If tb!idSegmento Then
            TabIstr.BlkIstr(k).IdSegm = tb!idSegmento
            TabIstr.BlkIstr(k).IdSegmenti = tb!idSegmento
            TabIstr.BlkIstr(k).Segment = getNomeSegmento(tb!idSegmento)
            TabIstr.BlkIstr(k).Segmenti = TabIstr.BlkIstr(k).Segment
            If Len(TabIstr.BlkIstr(k).SegLen) = 0 Then
              TabIstr.BlkIstr(k).SegLen = getLenSegmentoByIdSegmento(tb!idSegmento)
              TabIstr.BlkIstr(k).SegmentiLen = TabIstr.BlkIstr(k).SegLen
             End If
            Else
              IsValid = False
            End If
      
     End If
     
     'E' sempre vuoto! servirebbe una "getLenSegmentoByIdSegmento(tb!IdSegmento)",
     'ma metterlo a posto a monte...
     'Mauro 19-03-2007 Multi-Segm
     'If IsNumeric(TabIstr.BlkIstr(k).SegLen) Then 'blank/null (numerico <- stringa) !
     If Len(TabIstr.BlkIstr(k).SegmentiLen) Then
       If TabIstr.BlkIstr(k).SegLen Then
         tb!SegLen = CInt(TabIstr.BlkIstr(k).SegLen)
       Else
         'Imposto il primo valore tra quelli multipli
         Dim SegmentLen() As String
         SegmentLen() = Split(TabIstr.BlkIstr(k).SegmentiLen, ";")
         tb!SegLen = SegmentLen(0)
       End If
       tb!SegmentiLend = TabIstr.BlkIstr(k).SegmentiLen
     ElseIf TabIstr.BlkIstr(k).SegLen Then
      tb!SegLen = CInt(TabIstr.BlkIstr(k).SegLen)
      tb!SegmentiLend = CInt(TabIstr.BlkIstr(k).SegLen)
     Else
       tb!SegLen = 0
       tb!SegmentiLend = 0
     End If
     tb!NomeSSA = TabIstr.BlkIstr(k).SSAName   'togliere sti bianchi in fondo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     tb!dataarea = TabIstr.BlkIstr(k).Record
     
     If Len(TabIstr.BlkIstr(k).CodCom) Then
         TabIstr.BlkIstr(k).Search = DecodificaCodCom(TabIstr.BlkIstr(k).CodCom)
         If InStr(TabIstr.BlkIstr(k).Search, "Undefined") Then
           TabIstr.BlkIstr(k).valida = False
           'NUOVA SEGNALAZIONE
         End If
     End If
        
     'Command code
     tb!condizione = Replace(TabIstr.BlkIstr(k).CodCom, "-", "")
     tb!Qualificazione = TabIstr.BlkIstr(k).qualified
     tb!Correzione = False
     tb!moved = TabIstr.BlkIstr(k).moved
     
     'SQ - 27-05-05: aspettare la validita' delle chiavi per chiudere...
    
     '''''''''''''''''''''''''''''''''''''''''''''''''
     'GESTIONE CHIAVI:
     'Cerca ID campo chiave
     'Allestisce tabella record in PsDli_IstrDett_Key
     'Setta la validita' dell'istruzione;
     '''''''''''''''''''''''''''''''''''''''''''''''''
     For y = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
        'SQ-2: temporaneo - valutare caso di segmenti multipli....
        'idField = CercaIdField(wIdSegm, TabIstr.BlkIstr(K).KeyDef(y).Key)
        If TabIstr.BlkIstr(k).IdSegm Then
           'piu' di un elemento: indeterminatezza
           'Mauro 19-03-2007 Multi-Segm: Sarebbe da gestire per Multi-Segmenti
           idField = CercaIdField(TabIstr.BlkIstr(k).IdSegm, TabIstr.BlkIstr(k).KeyDef(y).Key, TabIstr.DBD_TYPE)
        Else
           idField = 0
        End If
        
        'SQ - 27-05-05
        If idField = 0 Then
          TabIstr.BlkIstr(k).valida = False 'istruzione sbagliata in ogni caso!
          IsValid = False
          '''''''''''''''''''''''''''''''''''''''
          ' SQ 5-05-06 - Nuova segnalazione (I04)
          '''''''''''''''''''''''''''''''''''''''
          addSegnalazione TabIstr.BlkIstr(k).KeyDef(y).Key, "SSAKEYNOTFOUND", TabIstr.BlkIstr(k).KeyDef(y).Key
        End If
  
        'SQ: spostata qui... era fuori ciclo e settava solo la chiave 1!?
        TabIstr.BlkIstr(k).KeyDef(y).xName = GbXName
        
        Set tb1 = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Key")
        tb1.AddNew
        'SQ COPY-ISTR
        'tb1!idOggetto = GbIdOggetto
        tb1!idOggetto = IdOggettoRel
        tb1!idpgm = GbIdOggetto
        tb1!Riga = GbOldNumRec
        tb1!livello = k
        tb1!progressivo = y
        
        tb1!idField = idField
        tb1!idField_moved = TabIstr.BlkIstr(k).KeyDef(y).Key_moved
        'SQ: aspettare il via di Ste per mettere true se idField negativo.
        tb1!xName = TabIstr.BlkIstr(k).KeyDef(y).xName
  
  
        '''''SONO IN DEBUG: TOGLIERE'''''
        'QUANTI CONTROLLI FACCIAMO? E' GIA' STATO FATTO ALL'INIZIO
        Select Case TabIstr.BlkIstr(k).KeyDef(y).Op
          Case "Nothing"
            tb1!operatore = ""
          Case "SPACE", "SPACES", "SPA"
            tb1!operatore = "SP"
          Case Else
            If Len(TabIstr.BlkIstr(k).KeyDef(y).Op) = 0 Then
              tb1!operatore = " "
            Else
              tb1!operatore = Mid(TabIstr.BlkIstr(k).KeyDef(y).Op, 1, 2) 'tronca a 2 perch� l'operatore � di 2
            End If
        End Select
        
        'SQ: perche' se e' = ""??????????
        If TabIstr.BlkIstr(k).KeyDef(y).Key <> "Nothing" Then
          tb1!Valore = Trim(TabIstr.BlkIstr(k).KeyDef(y).KeyVal & "")
          tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
          'SQ: aggiungere colonna in DB!
          tb1!KeyStart = TabIstr.BlkIstr(k).KeyDef(y).KeyStart
        End If
  
        tb1!KeyLen = TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ""
        
        'SG : Se la lunghezza non � stata calcolata o calcolata male recupera l'effettiva lunghezza dal field dli
        
        'ac COMMENTATO
''        If Val(tb1!KeyLen) <= 0 Or Trim(tb1!KeyLen) = "" Then
''           tb1!KeyLen = Restituisci_DatiKey(tb1!idField, tb1!xName, "LUNGHEZZA")
''           TabIstr.BlkIstr(k).KeyDef(y).KeyLen = tb1!KeyLen
''
''           If Len(TabIstr.BlkIstr(k).SSAName) Then
''              TabIstr.BlkIstr(k).KeyDef(y).KeyVarField = TabIstr.BlkIstr(k).SSAName & "(" & TabIstr.BlkIstr(k).KeyDef(y).KeyStart & ":" & TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ")"
''              tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
''           End If
''        End If
        'ac
        tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
        
        
        If Len(TabIstr.BlkIstr(k).KeyDef(y).KeyBool) = 0 Then
          tb1!Booleano = " "
        Else
          tb1!Booleano = TabIstr.BlkIstr(k).KeyDef(y).KeyBool
        End If
        'SQ:
        tb1!valida = TabIstr.BlkIstr(k).valida
        tb1!Correzione = False
        tb1.Update
        tb1.Close
    Next y
    
    Dim wSsaPres As Boolean
    '''''''''''''''wSsaPres = Trim(TabIstr.BlkIstr(k).SSAName) <> "<NONE>"
    
    'Ancora il SEGMENTO????
    'If Not isGSAM Then
    '    If TabIstr.BlkIstr(k).IdSegm <= 0 Then TabIstr.BlkIstr(k).Valida = False
    '    If Trim(TabIstr.BlkIstr(k).segment) = "" Then TabIstr.BlkIstr(k).Valida = False
    'End If
    
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' PsDli_istrDett_Dup: COS'E' 'STA ROBA?????
    ' A COSA SERVE LA gSSA?????????????????????
    ' Per la molteplicit�?????????????????????? (forse)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim tbr As Recordset
    Set tbr = m_Fun.Open_Recordset("Select * from PsDli_istrDett_Dup")
    T4 = 0
    wIdStr = ""
    
    For T1 = 1 To UBound(gSSA)
   '   For t2 = 1 To UBound(wDliSsa)
       If k <= UBound(wDliSsa) Then
         If gSSA(T1).Name = wDliSsa(k) Then
            If UBound(gSSA(T1).Segm) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).Segm)
                 T4 = T4 + 1
                 tbr.AddNew
                    'SQ COPY-ISTR
                    'tbr!idOggetto = GbIdOggetto
                    tbr!idOggetto = IdOggettoRel
                    tbr!idpgm = GbIdOggetto
                    tbr!Riga = GbOldNumRec
                    tbr!livello = k
                    tbr!numeropar = T4
                    tbr!Tipo = "SG"
                    tbr!Valore = gSSA(T1).Segm(T3)
                 tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).FIELD) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).FIELD)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "FL"
                     tbr!Valore = gSSA(T1).FIELD(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).Op) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).Op)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "OP"
                     tbr!Valore = gSSA(T1).Op(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).value) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).value)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "VL"
                     tbr!Valore = gSSA(T1).value(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).CodCom) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).CodCom)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "CC"
                     tbr!Valore = DecodificaCodCom(gSSA(T1).CodCom(T3))
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).parentesi) > 1 Then
               T4 = 0
               wIdStr = getIdArea(gSSA(1).Name)
               For T3 = 1 To UBound(gSSA(T1).parentesi)
                  T4 = T4 + 1
                  If T4 > 2 Then
                     T4 = T4
                  End If
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "PA"
                     tbr!Valore = gSSA(T1).parentesi(T3)
                  tbr.Update
               Next T3
            End If
         End If
       End If
  '    Next t2
    Next T1
    If Not TabIstr.BlkIstr(k).valida Then IsValid = False
    tb.Update
  Next k 'ITERAZIONE LIVELLI...
  tb.Close
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' GESTIONE SEGMENTI/AREE: ANCORA?!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'Attenzione: wIdStr NON e' quello usato sopra per i singoli livelli... e' un'altra cosa...
  'SQ-2: If InStr(wIdStr, "/") And wIdSegm > 0 Then
  If InStr(wIdStr, "/") Then
      wStrIdOggetto = Val(Left(wIdStr, 6))
      wStrIdArea = Val(Mid$(wIdStr, 8, 4))
      For y = 0 To UBound(IdSegmenti)
        If Len(Trim(IdSegmenti(y))) Then
          Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & IdSegmenti(y) _
                & " and stridoggetto = " & wStrIdOggetto _
                & " and stridarea = " & wStrIdArea _
                & " and nomecmp = '" & TabIstr.BlkIstr(1).Record & "'")
          If tb2.RecordCount = 0 Then
            tb2.AddNew
            tb2!idSegmento = IdSegmenti(y)
            tb2!stridoggetto = wStrIdOggetto
            tb2!strIdArea = wStrIdArea
            tb2!nomeCmp = TabIstr.BlkIstr(1).Record
            tb2.Update
            tb2.Close
          End If
        End If
     Next y
    End If
  End If

  ''''''''''''''''''''''''''''''''''''''''''''
  ' UPDATE "PsDli_Istruzioni":
  ''''''''''''''''''''''''''''''''''''''''''''
  UpdatePsdliIstruzioni (IsDC)
  
  'SQ ??????????????????
  Dim ssaPresent As Boolean
  'ssaPresent = True
  'SG: Controlla se c'� un livello di ssa
  '...
  
  
  ''''''''''''''''''''''''''''''''''''''''
  ' Segnalazioni
  ''''''''''''''''''''''''''''''''''''''''
  'Portare nelle rispettive posizioni...!
  If Not IsValid Then
    If Len(TabIstr.DBD) And wIdDBD(0) = 0 Then  'NO DBD
      addSegnalazione TabIstr.DBD, "NODBD", TabIstr.istr
    'ElseIf ubound(wIdDBD) Then   'DBD MULTIPLI
    '  AggiungiSegnalazione dbdName, "DBDS", istruzione
    ElseIf Trim(SwSegm) <> "" Then  'SQ ?
      addSegnalazione SwSegm, "NOSEGM", TabIstr.istr
    Else
     'Errore Generico (SEGMENTO!)
     addSegnalazione "", "INVALIDDLIISTR", ""
    End If
  End If
  Exit Sub
parseErr:
  addSegnalazione parametri, "I00", parametri
End Sub
Public Function ChecktokenArray(param As String) As String
    Dim token As String
        ChecktokenArray = ""
        token = nextToken_new(param)
        If Left(token, 1) = "(" And Right(token, 1) = ")" Then
            ChecktokenArray = token
        Else ' ripristino parametri
          If token = "OF" Then 'IRIS-DB
            ChecktokenArray = " " & token & " " & nextToken_new(param) 'IRIS-DB
          Else 'IRIS-DB
            If Len(param) Then
                param = token & " " & param
            Else
                param = token
            End If
          End If 'IRIS-DB
        End If
End Function

Function CheckConcatenation(pparametri As String) As String
  Dim token As String, lostToken As String, tokenConcat As String
  
  If pparametri = "" Then
    Exit Function
  End If
  token = nextToken_new(pparametri)
  lostToken = token
  While token = "!!"
    lostToken = ""
    If Len(pparametri) Then
      token = nextToken_new(pparametri)
      CheckConcatenation = CheckConcatenation & " " & "!!" & " " & token
    End If
    If Len(pparametri) Then
      token = nextToken_new(pparametri)
      lostToken = token
    End If
  Wend
  If Not lostToken = "" Then
    pparametri = lostToken & " " & pparametri
  End If
End Function

Function GetProcNameFromIstr(pPgm As Long, pOggetto As Long, pRiga As Long) As String
  Dim rsProc As Recordset
  
  Set rsProc = m_Fun.Open_Recordset("Select ProcName From PsDli_Istruzioni Where " & _
                                    "IdPgm = " & pPgm & " and idOggetto = " & pOggetto & " and riga = " & pRiga)
  If Not rsProc.EOF Then
    GetProcNameFromIstr = rsProc!procName
  End If
  rsProc.Close
End Function

Function isMAinProc(procName As String, pOggetto As Long) As Boolean
  Dim rsProc As Recordset
  
  Set rsProc = m_Fun.Open_Recordset("Select ProcName From PsPgm Where idOggetto = " & pOggetto & " and Procname ='" & procName & "'")
  If Not rsProc.EOF Then
    isMAinProc = True
  End If
  rsProc.Close
End Function

Function GetProcFromIdCopy(idCopy As Long, idpgm As Long) As String
  Dim rsProc As Recordset
  
  Set rsProc = m_Fun.Open_Recordset("Select b.ReplacingVar from Bs_Oggetti as a,PsPgm_Copy as b where b.IdPgm = " & _
                                    idpgm & " and copy = a.Nome and a.IdOggetto = " & idCopy)
  If Not rsProc.EOF Then
    GetProcFromIdCopy = rsProc!replacingVar
  End If
  rsProc.Close
End Function

Public Sub parseGU_call(istruzione As String, parametri As String)
  Dim rs As Recordset, tb As Recordset, tb1 As Recordset, tb2 As Recordset
  Dim i As Integer, k As Integer, y As Integer, T1 As Integer, T3 As Integer, T4 As Integer
  Dim wNomePsb As String, ssaValue As String
  Dim isGSAM As Boolean, unqualified As Boolean
  Dim idField As Integer
  Dim obsolete As Boolean
  Dim ssa As String, procIstr As String, rsAreaMain As Recordset
  Dim ssaOk As Boolean
  Dim rsArea As Recordset, rsLink As Recordset, rsCmp As Recordset
  Dim env As Collection 'area,idDbd
  Dim wNomeCmpLiv As String, wIdStr As String, wStrIdOggetto As Long, wStrIdArea As Long
  Dim IdSegmentiD() As String
  Dim a As Integer
  Dim wNomeCmp As String
  Dim SegmentLen() As String
  Dim wSsaPres As Boolean
  Dim tbr As Recordset
  Dim ssaPresent As Boolean
  Dim rsPSB As Recordset 'IRIS-DB
  Dim PCBNum As Integer 'IRIS-DB
  On Error GoTo parseErr
  
  ReDim gSSA(0)
  ReDim dbdPgm(0)
  ReDim TabIstr.BlkIstr(0)
  'SQ 17-11-06
  SwSegm = "" 'init
  
  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  wDliRec = nexttoken(parametri)
  'AC 12/02/10 Controllo che l'area non sia elemento di un array
  
  wDliRec = wDliRec & ChecktokenArray(parametri)
  TabIstr.istr = istruzione
    
'''''  Debug.Print "parametri: " & parametri
'''''  Debug.Print "istruzione: " & istruzione
  
  '''''''''''''''''''''
  ' PCB (number)
  '''''''''''''''''''''
  TabIstr.numpcb = getPCBnumber(TabIstr.pcb)
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''  ' Mauro 21/01/2009 : TMP!!!!!!!!!!!!!!!! - PCB Multipli
''  If TabIstr.numpcb = 0 Then
''    Dim rsMoved As Recordset, numpcb As Integer
''    TabIstr.Multi_NumPCB = ""
''    Set rsMoved = m_Fun.Open_Recordset("select valore from psdata_cmpmoved where " & _
''                                       "idpgm = " & GbIdOggetto & " and nome = '" & TabIstr.pcb & "' " & _
''                                       "order by valore")
''    Do Until rsMoved.EOF
''      If Len(rsMoved!Valore) Then
''        numpcb = getPCBnumber(rsMoved!Valore)
''        TabIstr.Multi_NumPCB = TabIstr.Multi_NumPCB & IIf(Len(TabIstr.Multi_NumPCB), ",", "") & numpcb
''      End If
''      rsMoved.MoveNext
''    Loop
''    rsMoved.Close
''  End If
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'IRIS-DB start - non uso la funzione CMPAT perch� mi sembra incoerente con quello che ha fatto il parser di primo livello - da rivedere con programmi CICS
'''''  If psbPgm(0) > 0 Then
'''''    PCBNum = IIf(IsCMPAT(CStr(psbPgm(0)), GbIdOggetto) = True, 1, 0)
  If psbPgm(0) > 0 Then
    Set rsPSB = m_Fun.Open_Recordset("Select TypePCB From PsDLI_Psb Where idoggetto = " & psbPgm(0) & " and NumPcb = " & TabIstr.numpcb)
    If rsPSB.RecordCount Then
      If rsPSB!TypePcb = "TP" Then
        IsDC = True
      End If
    Else
      If TabIstr.numpcb = 1 Then
        IsDC = True
      Else
        m_Fun.writeLog "parseGU/REPL_call - PCB not found in PSB table Idogg(" & GbIdOggetto & ") Riga(" & GbOldNumRec & ") Idpsb(" & psbPgm(0) & ")"
      End If
    End If
    rsPSB.Close
  Else
    m_Fun.writeLog "parseGU/REPL_call - missing PSB - not checking PCB type Idogg(" & GbIdOggetto & ") Riga(" & GbOldNumRec & ") Idpsb(" & psbPgm(0) & ")"
  End If
  If Len(TabIstr.pcb) > 0 And Not IsDC Then 'IRIS-DB
  'IRIS-DB forzatura per i programmi chiamati (Fedex ne � piena)
    If m_Fun.FnNomeDB = "LSSIprj.mty" Then 'IRIS-DB forzato solo per Fedex
      If Left(TabIstr.pcb, 3) = "ALT" Then IsDC = True
      If Left(TabIstr.pcb, 3) = "EXP" Then IsDC = True
      If Left(TabIstr.pcb, 2) = "IO" Then IsDC = True
      If Mid(TabIstr.pcb, 3, 1) = "T" And Not Mid(TabIstr.pcb, 4, 1) = "-" Then IsDC = True
      If Left(TabIstr.pcb, 6) = "LZQ179" Then IsDC = True
    End If
  End If
  'IRIS-DB end
  'IRIS-DB start - ottiene il DBD prima (sarebbe da rivalutare tutto l'ordine del parsing
  If TabIstr.numpcb > 0 And psbPgm(0) > 0 And Not IsDC Then
    getDbdfromPsb TabIstr.numpcb, psbPgm(0)
  End If
  'IRIS-DB end
  If IsDC Then
    parseRECEIVE istruzione, wDliRec
    Exit Sub
  End If
  
  'SQ 27-04-06 -
  'COMPLETAMENTE SQUALIFICATA?
  unqualified = Len(parametri) = 0
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Gestione SSA
  ' Valorizzo la TabIstrSSa
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

  ReDim wDliSsa(0)
  ReDim TabIstrSSa(0)
  ReDim TabIstrSSa(0).NomeSegmento.value(0)
  ssaOk = True
  While Len(parametri)
    i = i + 1
    ReDim Preserve wDliSsa(UBound(wDliSsa) + 1)
    'AC 26/02/10
    wDliSsa(UBound(wDliSsa)) = nextToken_new(parametri) & CheckConcatenation(parametri)

    If GbTipoInPars = "PLI" Then
      procIstr = GetProcNameFromIstr(GbIdOggetto, IdOggettoRel, GbOldNumRec)
    End If
    
    'stefano: parsa le aree costanti; per ora solo l'SSA, ci sono anche casi sulla funzione da implementare...
    'SQ PULIRE TUTTA STA ROBA!!! --> UTILIZZARE "findDataCmp"
    If InStr(wDliSsa(UBound(wDliSsa)), "'") = 0 Then
      'vedere 'sto blocco se serve...
      Set rsArea = m_Fun.Open_Recordset("Select IdOggetto,IdArea,Proc From PsData_Cmp Where " & _
                                        "Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & GbIdOggetto)
      If Not rsArea.EOF And GbTipoInPars = "PLI" Then  'controllo proc
        'IRIS-DB: not sure how was determined this type of check years ago, it does not look logical
        '         as any PROC can contain the SSA even not being the main of the task - i.e. the one with the ENTRY DLITCBL
        '         for now I'm changing it for L&G as in this case we have only PROCs and no Main programs at all
        '         The research in the copybooks/includes has to be done indipendently if a program is main or sub
'        If Not rsArea!proc = procIstr Then
'          If isMAinProc(rsArea!proc, GbIdOggetto) Then
'            Set rsAreaMain = rsArea
'          End If
'         ' riportiamo il record count a zero
'          Set rsArea = m_Fun.Open_Recordset("Select IdOggetto From PsData_Cmp Where Nome = 'XXXXX' And IdOggetto = -100")
'        End If
        Set rsAreaMain = rsArea
      End If
      
      If rsArea.RecordCount = 0 Then
        'Ricerca nelle COPY:
        Set rsLink = m_Fun.Open_Recordset("Select IdOggettoR From PsRel_Obj Where " & _
                                          "IdOggettoC = " & GbIdOggetto & " And (Relazione = 'CPY' or Relazione = 'INC')")
        Do While Not rsLink.EOF
          Set rsCmp = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp Where " & _
                                           "Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & rsLink!idOggettoR)
          If rsCmp.RecordCount Then
        'IRIS-DB: not sure how was determined this type of check years ago, it does not look logical
        '         as any PROC can contain the SSA even not being the main of the task - i.e. the one with the ENTRY DLITCBL
        '         for now I'm changing it for L&G as in this case we have only PROCs and no Main programs at all
        '         The research in the copybooks/includes has to be done indipendently if a program is main or sub
'''            If GbTipoInPars = "PLI" Then  ' >-----------   PLI
'''              ' nel caso del PLI controllo il nome della proc
'''              If GetProcFromIdCopy(rsCmp!idOggetto, GbIdOggetto) = procIstr Then
'''                Set rsArea = rsCmp
'''                Exit Do
'''              Else
'''                If isMAinProc(GetProcFromIdCopy(rsCmp!idOggetto, GbIdOggetto), GbIdOggetto) Then
'''                  Set rsAreaMain = rsCmp
'''                End If
'''              End If
'''            Else                          ' >-----------   COBOL
              Set rsArea = rsCmp
              Exit Do
'''            End If
          End If
          rsLink.MoveNext
        Loop
        ''''''''''''''''''''''''''''''''''''''''''''''''
        ' COPY IN COPY (non si finisce mai!)
        ' SQ 10-09-09 (vedere se si pu� fare meglio...)
        ''''''''''''''''''''''''''''''''''''''''''''''''
        If rsArea.RecordCount = 0 Then
          If rsLink.RecordCount Then
            rsLink.MoveFirst  'Altro giro:
            Do While Not rsLink.EOF
              Set rs = m_Fun.Open_Recordset("Select IdOggettoR From PsRel_Obj Where " & _
                                            "IdOggettoC = " & rsLink!idOggettoR & " And (Relazione = 'CPY' or Relazione = 'INC')")
              Do While Not rs.EOF
                Set rsCmp = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp " & _
                                                 "Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & rs!idOggettoR)
                If rsCmp.RecordCount Then
        'IRIS-DB: not sure how was determined this type of check years ago, it does not look logical
        '         as any PROC can contain the SSA even not being the main of the task - i.e. the one with the ENTRY DLITCBL
        '         for now I'm changing it for L&G as in this case we have only PROCs and no Main programs at all
        '         The research in the copybooks/includes has to be done indipendently if a program is main or sub
'''                  If GbTipoInPars = "PLI" Then  ' >-----------   PLI
'''                    ' nel caso del PLI controllo il nome della proc
'''                    If GetProcFromIdCopy(rsCmp!idOggetto, GbIdOggetto) = procIstr Then
'''                      Set rsArea = rsCmp
'''                      Exit Do
'''                    Else
'''                      If isMAinProc(GetProcFromIdCopy(rsCmp!idOggetto, GbIdOggetto), GbIdOggetto) Then
'''                        Set rsAreaMain = rsCmp
'''                      End If
'''                    End If
'''                  Else
                    Set rsArea = rsCmp
                    Exit Do
'''                  End If
                End If
                rs.MoveNext
              Loop
              rs.Close
              rsLink.MoveNext
            Loop
          End If
        End If
        rsLink.Close
      End If
      
      If GbTipoInPars = "PLI" And rsArea.RecordCount = 0 Then  ' >----------- PLI
        If Not rsAreaMain Is Nothing Then
          Set rsArea = rsAreaMain
        End If
      End If
      'Valorizza TabIstrSSa:
      If rsArea.RecordCount Then
        'SQ - tmp: mettere in linea...
        'Non funge per indirizzo? (in testa?)
        ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
        TabIstrSSa(UBound(TabIstrSSa)).SSAName = wDliSsa(UBound(wDliSsa))
        TabIstrSSa(UBound(TabIstrSSa)).idOggetto = rsArea!idOggetto
        TabIstrSSa(UBound(TabIstrSSa)).idArea = rsArea!idArea
        parseSSA TabIstrSSa(UBound(TabIstrSSa))
      Else
        ''''''''''''''''''''''''''''''''''''''''
        ' SQ - 19-04-06
        ' Nuova segnalazione (I02)
        ''''''''''''''''''''''''''''''''''''''''
        addSegnalazione wDliSsa(UBound(wDliSsa)), "NOSSAVALUE", wDliSsa(UBound(wDliSsa))
        ssaOk = False
      End If
      rsArea.Close
    Else
      ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
      TabIstrSSa(UBound(TabIstrSSa)).SSAName = wDliSsa(UBound(wDliSsa))
      TabIstrSSa(UBound(TabIstrSSa)).idOggetto = 0 'fittizio
      TabIstrSSa(UBound(TabIstrSSa)).idArea = 0 'fittizio
      parseSSA TabIstrSSa(UBound(TabIstrSSa))
    End If
  Wend
  
  ''''''''''''''''''''''''''''''''''''''''
  ' SQ 20-04-06
  ' Area Segmento - la associava a tutti!
  '''''''''''''''''''''''''''''''''''''''''
  'stefano: oggi - parsa l'area anche se l'SSA non � ok o se la gn � totalmente squalificata
  '   ATTENZIONE: CAMBIO IMPORTANTE!!!!!!
  'If ssaOk And Not unqualified Then
  TabIstrSSa(UBound(TabIstrSSa)).Struttura = wDliRec
  If Len(TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0)) = 0 And Len(wDliRec) Then  'Solo sul Segmento finale...
    ''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 2-05-06
    ' Naming Convention SEGMENTO/AREA
    ''''''''''''''''''''''''''''''''''''''''''''''
    If Len(segmentNameParam) Then
      Set env = New Collection  'resetto;
      env.Add wDliRec
      'SQ 5-06-06 - Come poteva funzionare?
      env.Add 0 'wIdDBD(0)
      'SQ MOVED/VALUE
      TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0) = getEnvironmentParam(segmentNameParam, "IMS", env)
    End If
  End If
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'SQ - Cambiamento mostruoso (pezzo di analizzaCallDli_SQ)
  ' TabIstrSSa -> TabIstr
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  getTabIstr
'   End If
  ''''''''''''''''''''''''''''''''
  ' Gestione PSB/DBD:
  ' Input: N PSB, PCB, M SEGMENTI
  ''''''''''''''''''''''''''''''''
  getInstructionPsbDbd (TabIstr.numpcb)
  obsolete = getObsolete(TabIstr.DBD)
  
  'SQ - 27-04-06
  isGSAM = TabIstr.DBD_TYPE = DBD_GSAM
  ' non mi piace molto che sia separata, comunque...
  If isGSAM Then
    getGSAMinfo (TabIstr.numpcb)  'wIDdbd...
    'If TabIstr.DBD_TYPE <> DBD_GSAM Then
    '  'Non ho la SSa:
    '  AggiungiSegnalazione GbNomePgm, "NODECOD", GbNomePgm
    '  Exit Sub
    'End If
  Else
    ''''''''''''''SQ - 27-04-06 (sopra: serve cmq) getInstructionPsbDbd (TabIstr.numPCB)
    ''''''''''''''''''''''''''''''''''''''''''
    ' TabIstr.BlkIstr:
    ''''''''''''''''''''''''''''''''''''''''''
    If ssaOk And IsValid Then 'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
      getTabIstrIds
    Else
      If Not unqualified Then IsValid = False
    End If
  End If
  
  'SQ 19-11-06
  'If IsValid And Not unqualified Then
  ' non mi piace molto che sia separata, comunque...
  'stefano: oggi - parsa l'area anche se l'SSA non � ok o se la gn � totalmente squalificata
  '   ATTENZIONE: CAMBIO IMPORTANTE!!!!!!
  'If (isValid And Not unqualified) Or isGSAM Then
    ''''''''''''''''''''''''''''''''''''
    'ITERAZIONE LIVELLI ISTRUZIONE:
    ''
    'AGGIORNAMENTO "PsDli_IstrDett_Liv"
    ''''''''''''''''''''''''''''''''''''
    Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
    For k = 1 To UBound(TabIstr.BlkIstr)
      '''''''''''''''''''''
      'SEGMENTO VARIABILE?
      '''''''''''''''''''''
      If Left(TabIstr.BlkIstr(k).Segment, 1) = "(" And Right(TabIstr.BlkIstr(k).Segment, 1) = ")" Then
        ReDim IdSegmenti(0)
        '''''''''''''''''''''''''''''''''''''
        ' Segmento variabile: gestione temp.
        '''''''''''''''''''''''''''''''''''''
      End If
      ' AC
      If Len(TabIstr.BlkIstr(k).Segment) = 0 Then
        ReDim IdSegmenti(0)
      End If
      
      wIdStr = getIdArea(TabIstr.BlkIstr(k).Record)
      wNomeCmpLiv = TabIstr.BlkIstr(k).Record
      
     'ADESSO HO N SEGMENTI QUANTI SONO I DBD... (o ZERO!)
     If SwParsExec Then
      '...
     Else
       'SQ 28-11-05:
       'Mauro 19-03-2007 Multi-Segm
       'If TabIstr.BlkIstr(k).IdSegm Then
       If Len(TabIstr.BlkIstr(k).IdSegmenti) Then
         IdSegmentiD() = Split(TabIstr.BlkIstr(k).IdSegmenti, ";")
         For a = 0 To UBound(IdSegmentiD())
           wStrIdOggetto = Val(Left(wIdStr, 6))
           wStrIdArea = Val(Mid(wIdStr, 8, 4))
           'SQ - 28-11-05
           'Pezza provvisoria: capire perche' arriva cosi'...
           If wStrIdOggetto <> 0 And wStrIdArea <> 0 Then
             Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & IdSegmentiD(a) & _
                                            " and stridoggetto = " & wStrIdOggetto & _
                                            " and stridarea = " & wStrIdArea & _
                                            " and nomecmp = '" & wNomeCmpLiv & "'")
             If tb2.RecordCount = 0 Then
               tb2.AddNew
             End If
             tb2!idSegmento = IdSegmentiD(a)
             tb2!stridoggetto = wStrIdOggetto
             tb2!strIdArea = wStrIdArea
             tb2!nomeCmp = wNomeCmpLiv
             'AC
             tb2.Update
             tb2.Close
           End If
         Next a
       End If
     End If
    
     If TabIstr.DBD_TYPE <> DBD_GSAM Then
       If Len(IdSegmenti(0)) = 0 Then SwSegm = TabIstr.BlkIstr(k).Segment
     End If
          
     i = InStr(wIdStr, "-")
     If i > 0 Then
       wNomeCmp = Mid(wIdStr, i + 2)
       wIdStr = Val(Mid(wIdStr, 1, i - 1))
     Else
       wNomeCmp = " "
     End If
    
     ''''''''''''''''''''''''''''''''''''''
     'INSERIMENTO IN "PsDli_IstrDett_Liv":
     ''''''''''''''''''''''''''''''''''''''
     tb.AddNew
     'SQ COPY-ISTR
     'tb!idOggetto = GbIdOggetto
     tb!idOggetto = IdOggettoRel
     tb!idpgm = GbIdOggetto
     tb!Riga = GbOldNumRec
     tb!livello = k
     'SQ 17-10-07
     'Pippo ha modificato la gestione delle GN compl. sq. aggiungendo comunque un livello?
     ' => cambiare 'sta gestione!... al momento NON deve segnalare l'istruzione come invalida!
     If unqualified Then
       TabIstr.BlkIstr(k).valida = True
     End If
     tb!valida = TabIstr.BlkIstr(k).valida
     ' tb!dtsegnalazione = Now
    
    'SQ: Cos'� 'sta roba???
    'dovremo aggiungere quei due campi nella TabIstr!!!!!!!!!!!!!!!!!!!!
    For i = 0 To UBound(TabIstrSSa)
      If Trim(TabIstrSSa(i).SSAName) = Trim(TabIstr.BlkIstr(k).SSAName) Then  'SQ?? devono essere allineati i due array! (e partire entrambi da 1!)
        tb!stridoggetto = TabIstrSSa(i).idOggetto
        tb!strIdArea = TabIstrSSa(i).idArea
        'stefano: per i GSAM la precedenti if � comunque vera, anche se entrambi i valori sono vuoti!!
        ' Per ora ho messo una pezza, ma perch� non caricare direttamente dalla ricerca per la psdlirel_segaree?
        ' ed inserire la lunghezza dalla funzione apposita? Perch� non riporta anche l'ordinale?
        If isGSAM Then
          tb!stridoggetto = wStrIdOggetto
          tb!strIdArea = wStrIdArea
        End If
        Set rsArea = m_Fun.Open_Recordset("Select Byte From PsData_Cmp Where IdOggetto = " & tb!stridoggetto & " and idarea = " & tb!strIdArea & " and nome = '" & wNomeCmpLiv & "'")
        If rsArea.EOF Then
          tb!AreaLen = Null 'forse sarebbe meglio zero, cos� saprei che l'analisi � stata fatta
        Else
          tb!AreaLen = rsArea!byte
        End If
        rsArea.Close
        Exit For
      End If
    Next i
    
     'SQ-2: CHIARIRE: LI HO APPENA CALCOLATI SOPRA!?
    ' y = InStr(wIdStr, "/")
     'If y > 0 Then
        'tb!strIdOggetto = Left(wIdStr, 6)
        'tb!strIdArea = Mid(wIdStr, y + 1, 4) 'SOPRA USO 6 E QUI Y?!
     'Else
      ''SQ - controllare: era sbagliato! wStrIdOggetto > 0... ma � string!?
        'If wStrIdArea > 0 And Len(wStrIdOggetto) Then
           'tb!strIdOggetto = wStrIdOggetto
           'tb!strIdArea = wStrIdArea
        'End If
     'End If
     
      tb!IdSegmento_moved = TabIstr.BlkIstr(k).segment_moved
      'SQ 30-01-07 - Modifica al EX getIdSegmento_By_MgTable...
      'Mauro 19-03-2007 Multi-Segm
      'If TabIstr.BlkIstr(k).IdSegm = 0 Then
      If Len(TabIstr.BlkIstr(k).IdSegmenti) = 0 Then
        'Scamuffo I/O AREA -> SEGMENTO (Ha senso solo sull'ultimo livello - volendo si pu� rendere opzionale)
        If k = UBound(TabIstr.BlkIstr) Then
          getSegment_byArea TabIstr.BlkIstr(k)
        End If
      End If
      'If TabIstr.BlkIstr(k).IdSegm Then
      If Len(TabIstr.BlkIstr(k).IdSegmenti) Then
        tb!idSegmento = TabIstr.BlkIstr(k).IdSegm
        tb!IdSegmentiD = TabIstr.BlkIstr(k).IdSegmenti
      ElseIf Len(TabIstr.BlkIstr(k).IdSegm) Then
        tb!idSegmento = TabIstr.BlkIstr(k).IdSegm
        tb!IdSegmentiD = TabIstr.BlkIstr(k).IdSegm
      Else
        'Segmento non trovato: istruzione INVALIDA!
        'stefano: gestione squalificata e obsoleta
        If Not unqualified And Not obsolete Then
          IsValid = False
          addSegnalazione TabIstr.BlkIstr(k).Segment, "NOSEGM", TabIstr.istr
        End If
      End If
     
     'E' sempre vuoto! servirebbe una "getLenSegmentoByIdSegmento(tb!IdSegmento)",
     'ma metterlo a posto a monte...
     'Mauro 19-03-2007 Multi-Segm
     'If IsNumeric(TabIstr.BlkIstr(k).SegLen) Then 'blank/null (numerico <- stringa) !
     If Len(TabIstr.BlkIstr(k).SegmentiLen) Then
       If TabIstr.BlkIstr(k).SegLen Then
         tb!SegLen = CInt(TabIstr.BlkIstr(k).SegLen)
       Else
         'Imposto il primo valore tra quelli multipli
         SegmentLen() = Split(TabIstr.BlkIstr(k).SegmentiLen, ";")
         tb!SegLen = SegmentLen(0)
       End If
       tb!SegmentiLend = TabIstr.BlkIstr(k).SegmentiLen
     Else
       tb!SegLen = 0
       tb!SegmentiLend = 0
     End If
     tb!NomeSSA = TabIstr.BlkIstr(k).SSAName   'togliere sti bianchi in fondo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     tb!dataarea = TabIstr.BlkIstr(k).Record
     
     'SQ
'     If Len(TabIstr.BlkIstr(k).CodCom) Then
'      TabIstr.BlkIstr(k).Search = DecodificaCodCom(TabIstr.BlkIstr(k).CodCom)
'      If InStr(TabIstr.BlkIstr(k).Search, "Undefined") Then
'        TabIstr.BlkIstr(k).valida = False
'        'NUOVA SEGNALAZIONE
'      End If
'     End If
        
     'COMMAND CODES
     'Occhio: c'� la search gi� decodificata!
     tb!condizione = Left(TabIstr.BlkIstr(k).CodCom, 100)
     tb!comcodes_moved = TabIstr.BlkIstr(k).CodCom_moved
     
     'QUALIFICAZIONE (MULTIPLA)
     tb!Qualificazione = TabIstr.BlkIstr(k).qualified
     tb!Qualificazione_moved = TabIstr.BlkIstr(k).qualified_moved
     tb!Qualandunqual = TabIstr.BlkIstr(k).qual_unqual
     
     tb!Correzione = False
     tb!moved = TabIstr.BlkIstr(k).moved
     
     'SQ - 27-05-05: aspettare la validita' delle chiavi per chiudere...
    
     '''''''''''''''''''''''''''''''''''''''''''''''''
     'GESTIONE CHIAVI:
     'Cerca ID campo chiave
     'Allestisce tabella record in PsDli_IstrDett_Key
     'Setta la validita' dell'istruzione;
     '''''''''''''''''''''''''''''''''''''''''''''''''
     'stefano: gestione squalificata
     If Not unqualified Then
       For y = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
         'SQ-2: temporaneo - valutare caso di segmenti multipli....
         'idField = CercaIdField(wIdSegm, TabIstr.BlkIstr(K).KeyDef(y).Key)
         If TabIstr.BlkIstr(k).IdSegm Then
           'piu' di un elemento: indeterminatezza
           'Mauro 19-03-2007 Multi-Segm: Sarebbe da gestire per Multi-Segmenti
           idField = CercaIdField(TabIstr.BlkIstr(k).IdSegm, TabIstr.BlkIstr(k).KeyDef(y).Key, TabIstr.DBD_TYPE) 'IRIS-DB aggiunto DBD_TYPE per i logici
         Else
           idField = 0
         End If
          
         'SQ - 27-05-05
         If idField = 0 Then
           TabIstr.BlkIstr(k).valida = False 'istruzione sbagliata in ogni caso!
           IsValid = False
           '''''''''''''''''''''''''''''''''''''''
           ' SQ 5-05-06 - Nuova segnalazione (I04)
           '''''''''''''''''''''''''''''''''''''''
           addSegnalazione TabIstr.BlkIstr(k).KeyDef(y).Key, "SSAKEYNOTFOUND", TabIstr.BlkIstr(k).KeyDef(y).Key
         End If
    
         'SQ: spostata qui... era fuori ciclo e settava solo la chiave 1!?
         TabIstr.BlkIstr(k).KeyDef(y).xName = GbXName
          
         Set tb1 = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Key")
         tb1.AddNew
         'SQ COPY-ISTR
         'tb1!idOggetto = GbIdOggetto
         tb1!idOggetto = IdOggettoRel
         tb1!idpgm = GbIdOggetto
         tb1!Riga = GbOldNumRec
         tb1!livello = k
         tb1!progressivo = y
         
         tb1!idField = idField
         tb1!idField_moved = TabIstr.BlkIstr(k).KeyDef(y).Key_moved
         'SQ: aspettare il via di Ste per mettere true se idField negativo.
         tb1!xName = TabIstr.BlkIstr(k).KeyDef(y).xName
         
         tb1!operatore = TabIstr.BlkIstr(k).KeyDef(y).Op 'multiplo
         tb1!operatore_moved = TabIstr.BlkIstr(k).KeyDef(y).Op_moved
         
         'SQ: perche' se e' = ""??????????
         If TabIstr.BlkIstr(k).KeyDef(y).Key <> "Nothing" Then
           tb1!Valore = Trim(TabIstr.BlkIstr(k).KeyDef(y).KeyVal & "")
           tb1!KeyValore = Left(TabIstr.BlkIstr(k).KeyDef(y).KeyVarField, 255)
           'SQ: aggiungere colonna in DB!
           tb1!KeyStart = TabIstr.BlkIstr(k).KeyDef(y).KeyStart
         End If
   
         tb1!KeyLen = TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ""
         
         'SG : Se la lunghezza non � stata calcolata o calcolata male recupera l'effettiva lunghezza dal field dli
         If Val(tb1!KeyLen) <= 0 Or Trim(tb1!KeyLen) = "" Then
           tb1!KeyLen = Restituisci_DatiKey(tb1!idField, tb1!xName, "LUNGHEZZA")
           TabIstr.BlkIstr(k).KeyDef(y).KeyLen = tb1!KeyLen
    
           If Len(TabIstr.BlkIstr(k).SSAName) Then
             TabIstr.BlkIstr(k).KeyDef(y).KeyVarField = TabIstr.BlkIstr(k).SSAName & "(" & TabIstr.BlkIstr(k).KeyDef(y).KeyStart & ":" & TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ")"
             tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
           End If
         End If
          
         If Len(TabIstr.BlkIstr(k).KeyDef(y).KeyBool) = 0 Then
           tb1!Booleano = " "
         Else
           tb1!Booleano = TabIstr.BlkIstr(k).KeyDef(y).KeyBool
         End If
         tb1!booleano_moved = TabIstr.BlkIstr(k).KeyDef(y).KeyBool_moved
         'SQ: questa � la validita del livello! non della chiave...
         tb1!valida = TabIstr.BlkIstr(k).valida
         tb1!Correzione = False
         tb1.Update
         tb1.Close
       Next y
     End If
    
     '''''''''''''''wSsaPres = Trim(TabIstr.BlkIstr(k).SSAName) <> "<NONE>"
    
     'Ancora il SEGMENTO????
     'If Not isGSAM Then
     '  If TabIstr.BlkIstr(k).IdSegm <= 0 Then TabIstr.BlkIstr(k).Valida = False
     '  If Trim(TabIstr.BlkIstr(k).segment) = "" Then TabIstr.BlkIstr(k).Valida = False
     'End If
    
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' PsDli_istrDett_Dup: COS'E' 'STA ROBA?????
    ' A COSA SERVE LA gSSA?????????????????????
    ' Per la molteplicit�?????????????????????? (forse)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set tbr = m_Fun.Open_Recordset("Select * from PsDli_istrDett_Dup")
    T4 = 0
    wIdStr = ""
    
    For T1 = 1 To UBound(gSSA)
       If k <= UBound(wDliSsa) Then
         If gSSA(T1).Name = wDliSsa(k) Then
            If UBound(gSSA(T1).Segm) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).Segm)
                 T4 = T4 + 1
                 tbr.AddNew
                  'SQ COPY-ISTR
                  'tbr!idOggetto = GbIdOggetto
                  tbr!idOggetto = IdOggettoRel
                  tbr!idpgm = GbIdOggetto
                  tbr!Riga = GbOldNumRec
                  tbr!livello = k
                  tbr!numeropar = T4
                  tbr!Tipo = "SG"
                  tbr!Valore = gSSA(T1).Segm(T3)
                 tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).FIELD) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).FIELD)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "FL"
                     tbr!Valore = gSSA(T1).FIELD(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).Op) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).Op)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "OP"
                     tbr!Valore = gSSA(T1).Op(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).value) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).value)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "VL"
                     tbr!Valore = gSSA(T1).value(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).CodCom) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).CodCom)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "CC"
                     tbr!Valore = DecodificaCodCom(gSSA(T1).CodCom(T3))
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).parentesi) > 1 Then
               T4 = 0
               wIdStr = getIdArea(gSSA(1).Name)
               For T3 = 1 To UBound(gSSA(T1).parentesi)
                  T4 = T4 + 1
                  If T4 > 2 Then
                     T4 = T4
                  End If
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "PA"
                     tbr!Valore = gSSA(T1).parentesi(T3)
                  tbr.Update
               Next T3
            End If
         End If
       End If
    Next T1
    If Not TabIstr.BlkIstr(k).valida Then IsValid = False
    tb.Update
  Next k 'ITERAZIONE LIVELLI...
  tb.Close
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' GESTIONE SEGMENTI/AREE: ANCORA?!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'Attenzione: wIdStr NON e' quello usato sopra per i singoli livelli... e' un'altra cosa...
  'SQ-2: If InStr(wIdStr, "/") And wIdSegm > 0 Then
  If InStr(wIdStr, "/") Then
      wStrIdOggetto = Val(Left(wIdStr, 6))
      wStrIdArea = Val(Mid$(wIdStr, 8, 4))
      For y = 0 To UBound(IdSegmenti)
        If Len(Trim(IdSegmenti(y))) Then
          Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & IdSegmenti(y) _
                & " and stridoggetto = " & wStrIdOggetto _
                & " and stridarea = " & wStrIdArea _
                & " and nomecmp = '" & TabIstr.BlkIstr(1).Record & "'")
          If tb2.RecordCount = 0 Then
            tb2.AddNew
            tb2!idSegmento = IdSegmenti(y)
            tb2!stridoggetto = wStrIdOggetto
            tb2!strIdArea = wStrIdArea
            tb2!nomeCmp = TabIstr.BlkIstr(1).Record
            tb2.Update
            tb2.Close
          End If
        End If
     Next y
    End If
  'stefano: oggi - parsa l'area anche se l'SSA non � ok o se la gn � totalmente squalificata
  '   ATTENZIONE: CAMBIO IMPORTANTE!!!!!!
  'End If

  ''''''''''''''''''''''''''''''''''''''''''''
  ' UPDATE "PsDli_Istruzioni":
  ''''''''''''''''''''''''''''''''''''''''''''
  ' Mauro 22/01/2009: PCB Multipli
''  If TabIstr.numpcb = 0 Then
''    Dim splitpcb() As Integer
''    splitpcb() = Split(TabIstr.Multi_NumPCB, ",")
''    For i = 0 To UBound(splitpcb)
''      TabIstr.numpcb = splitpcb(i)
''      UpdatePsdliIstruzioni (False)
''    Next i
''  Else
    UpdatePsdliIstruzioni False
''  End If
  
  'SQ ??????????????????
  'ssaPresent = True
  'SG: Controlla se c'� un livello di ssa
  '...
  
  ''''''''''''''''''''''''''''''''''''''''
  ' Segnalazioni
  ''''''''''''''''''''''''''''''''''''''''
  'Portare nelle rispettive posizioni...!
  If Not IsValid And ssaOk Then
    If Len(TabIstr.DBD) And wIdDBD(0) = 0 Then  'NO DBD
      addSegnalazione TabIstr.DBD, "NODBD", TabIstr.istr
    ElseIf Trim(SwSegm) <> "" Then  'SQ ?
      addSegnalazione SwSegm, "NOSEGM", TabIstr.istr
    Else
     'Errore Generico (SEGMENTO!)
     addSegnalazione "", "INVALIDDLIISTR", ""
    End If
  End If
  Exit Sub
parseErr:
  addSegnalazione parametri, "I00", parametri
  'Resume Next
End Sub

Public Sub UpdatePsdliIstruzioni(pIsdc As Boolean, Optional isinfopcb As Boolean = True)
  Dim rs As Recordset, rsdbd As Recordset
  
  ''''''''''''''''''''''''''''''''''''''''''''
  ' UPDATE "PsDli_Istruzioni":
  ''''''''''''''''''''''''''''''''''''''''''''
  'SQ - CPY-ISTR
  'Set rs = m_Fun.Open_Recordset("select * from PsDli_Istruzioni where idoggetto = " & GbIdOggetto & " and riga = " & GbOldNumRec)
  ' Mauro 21/11/2007 : e se GbappoIdPgm � 0?!?! La query va a quel paese!!
  'If GbappoIdPgm = -1 Then
  If GbappoIdPgm < 1 Then
    Set rs = m_Fun.Open_Recordset("select * from PsDli_Istruzioni where " & _
             "idPgm=" & GbIdOggetto & " and idOggetto = " & IdOggettoRel & " and riga = " & GbOldNumRec)
  Else
    Set rs = m_Fun.Open_Recordset("select * from PsDli_Istruzioni where " & _
             "idPgm=" & GbappoIdPgm & " and idOggetto = " & IdOggettoRel & " and riga = " & GbOldNumRec)
  End If
  'virgilio 8/3/2007
  If Not rs.EOF Then
    rs!valida = IsValid 'IsValid TRUE se validi TUTTI i singoli livelli...
    'stefano: spostato sotto, perch� se l'istruzione � obsoleta, NON bisogna segnalare errore
    ' sarebbe meglio anche evitare tutto il parser dei livelli, ma una parte serve per estrarre
    ' il nome DBD.
'    If Not isValid Then
'      addSegnalazione "", "INVALIDDLIISTR", ""
'    End If
    If isinfopcb Then
      rs!numpcb = TabIstr.pcb & ""
      rs!ordpcb = TabIstr.numpcb
    End If
    If Not TabIstr.keyFeed = "" Then
      'rs!AreaFeed = TabIstr.keyFeed
      rs!keyfeedback = TabIstr.keyFeed
    End If
    If Not TabIstr.keyFeedLen = "" Then
      rs!keyfeedbacklen = TabIstr.keyFeedLen
    End If
    rs!Imsdb = Not (pIsdc)
    rs!Imsdc = pIsdc
    rs!obsolete = False
    'AC 13/03/2008  TO_MIGRATE
    'Default di to_migrate in checkPSB,resetRepository(parsedue) e UpdatePsDliIstruzioni
    ' Settaggio attuale: default = true, diventa false solo se il dbd � stato trovato ma non � da migrare
    rs!to_migrate = True
    ' AC ******* setto obsolete e to_migrate copiandoli da PsDli_DBD
    If Len(TabIstr.DBD) Then
      If TabIstr.DBD = "TO_MIGRATE" Then
         rs!obsolete = False
         rs!to_migrate = True
         TabIstr.DBD = ""
      Else
         Set rsdbd = m_Fun.Open_Recordset("select * from PsDli_DBD where dbd_name = '" & TabIstr.DBD & "'")
         If Not rsdbd.EOF Then
           rs!obsolete = rsdbd!obsolete
           rs!to_migrate = rsdbd!to_migrate
           'stefano: visto che � obsoleta, la setta anche come valida in modo da non inquinare i report
           '  sulla validit�; l'obsolescenza � un'informazione superiore alla validit�
           If rsdbd!obsolete Then
             rs!valida = True
             IsValid = True
          End If
         End If
         rsdbd.Close
      End If
    End If
    ' Mauro 21/04/2008 : Gestione KEYS
    On Error Resume Next
    If Not TabIstr.KeyArg = "" Then
      rs!Keys = TabIstr.KeyArg
    End If
    On Error GoTo 0
    '********************
    rs.Update
    If Not IsValid Then
      addSegnalazione "", "INVALIDDLIISTR", ""
    End If
  Else
    Stop 'virgilio
    Debug.Print GbIdOggetto 'virgilio
  End If
  rs.Close
End Sub

Sub dbsObsoleteFilter(arraydbd() As Long)
  Dim i As Integer, j As Integer, rs As Recordset, appoDbd() As Long
  Dim firstdbd As Long, nomeDBD As String
  firstdbd = arraydbd(0)
  ReDim appoDbd(UBound(arraydbd))
  For i = 0 To UBound(arraydbd)
    'IRIS-DB INSENSATO!!!! Restituiva anche l'estensione e poi cercava il nome... nomeDBD = m_Fun.Restituisci_NomeOgg_Da_IdOggetto(arraydbd(i))
    'IRIS-DB If Not nomeDBD = "" Then
      Set rs = m_Fun.Open_Recordset("select dbd_name from psDLI_DBD, bs_oggetti where idoggetto = " & arraydbd(i) _
                & " and to_migrate = true and obsolete = false And dbd_name = nome")
      If Not rs.EOF Then
        j = j + 1
        appoDbd(i) = arraydbd(i)
      Else
        appoDbd(i) = -1
      End If
'IRIS-DB    Else
'IRIS-DB      appoDbd(i) = -1
'IRIS-DB    End If
    rs.Close
  Next i
  If j > 0 Then ' almeno uno non � obsoleto
    ReDim arraydbd(j - 1)
    j = 0
    For i = 0 To UBound(appoDbd)
      If Not appoDbd(i) = -1 Then
        arraydbd(j) = appoDbd(i)
        j = j + 1
      End If
    Next i
  Else ' tutti obsoleti tengo il primo
    ReDim arraydbd(0)
    arraydbd(0) = firstdbd
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''
' GESTIONE PSB/DBD:
' - N PSB, PCB, M segmenti
''''''''''''''''''''''''''''''''''''''''''
Sub getInstructionPsbDbd(pcbNumber As Integer)
  Dim rs As Recordset, tb As Recordset, rsdbd As Recordset
  Dim segmentsQuery As String, psbString As String, dbdWhereCond As String
  Dim i As Integer, namedb As String
  Dim CMPAT_Yes As Boolean
  Dim overrideDBD As Long
  
  ''''''''''''''''''''''''''''''''''''
  'SQ 29-04-08
  ' psbPgm � a livello di programma:
  ' non va modificato!
  ''''''''''''''''''''''''''''''''''''
  Dim psbInstr() As Long
  psbInstr = psbPgm
   
  If Not IsDC Then

'IRIS-DB tabella di override per risolvere i casi di PSB mancante e/o PCB dinamico
    Set rs = m_Fun.Open_Recordset("select idDBD, PCBNum from IRIS_PGM_DBD where IdPgm = " & GbIdOggetto & " and Riga = " & GbOldNumRec)
    If Not rs.EOF Then
      If rs!PCBNum > 0 Then pcbNumber = rs!PCBNum
      If rs!idDBD > 0 Then overrideDBD = rs!idDBD
    End If
    rs.Close
    
    ''''''''''''''''''''''''''''''''
    ' Lista PSB + PCB ==> lista DBD
    ''''''''''''''''''''''''''''''''
    For i = 0 To UBound(psbInstr)
      psbString = psbString & IIf(Len(psbString), ",", "") & psbInstr(i)
    Next i
    'psbString = Left(psbString, Len(psbString) - 1) 'ultima virgola superflua
    
    ' Mauro 04/11/2009: Se il PGM � un CICS, CMPAT va ignorato!!
    CMPAT_Yes = IsCMPAT(psbString, GbIdOggetto)
    If Not CMPAT_Yes Then pcbNumber = pcbNumber + 1
    
    '! solo DBD di tipo DB: attenzione ai TP
    'Set rs = m_Fun.Open_Recordset("select DbdName from PsDLI_Psb where idOggetto IN (" & psbString & ") and NumPcb = " & pcbNumber & " and typePcb='DB'")
    Set rs = m_Fun.Open_Recordset("select distinct b.idOggetto as IdDbd from PsDLI_Psb as a,bs_oggetti as b where " & _
                                  "a.idOggetto IN (" & psbString & ") and NumPcb = " & pcbNumber & " and typePcb = 'DB' and " & _
                                  "a.dbdName = b.nome and b.tipo = 'DBD'")
    If rs.RecordCount Then
      dbdWhereCond = "("
      ReDim wIdDBD(rs.RecordCount - 1)
      For i = 0 To UBound(wIdDBD)
        wIdDBD(i) = rs!idDBD
        dbdWhereCond = dbdWhereCond & "idOggetto = " & rs!idDBD
        If i = UBound(wIdDBD) Then  'ultimo giro
          dbdWhereCond = dbdWhereCond & ")"
        Else
          dbdWhereCond = dbdWhereCond & " OR "
        End If
        rs.MoveNext
      Next i
    Else
      'NESSUN DBD! (a meno di GSAM...)
      ReDim wIdDBD(0)
      If overrideDBD > 0 Then wIdDBD(0) = overrideDBD 'IRIS-DB
    End If
    'SQ
    rs.Close
    IsValid = True 'IRIS-DB
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' FILTRO SEGMENTI
    ' (VERIFICARE CHE NOME SEGMENTO SIA "vero" non variabile/space!!!)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'If Not wIdDBD(0) = 0 And UBound(TabIstr.BlkIstr) Then 'Almeno un DBD e un segmento:
    'IRIS-DB If wIdDBD(0) > 0 And UBound(TabIstr.BlkIstr) And Len(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Segment) Then 'Almeno un DBD e un segmento (valido):
    If wIdDBD(0) > 0 And UBound(TabIstr.BlkIstr) > 0 And Len(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Segment) > 1 Then 'Almeno un DBD e un segmento (valido): 'IRIS-DB checka che non ci sia solo la virgola...
      'Preparazione query differenziata:
      'SQ 2-05-07
      'If UBound(TabIstr.BlkIstr) Then
      If UBound(TabIstr.BlkIstr) = 1 And Len(TabIstr.BlkIstr(1).Segment) And Len(dbdWhereCond) = 0 Then
        'IRIS-DB molto rischioso, ma necessario... segmentsQuery = "select IdOggetto from PsDli_Segmenti where nome = '" & Replace(TabIstr.BlkIstr(1).Segment, "'", "") & "'"
        'IRIS-DB: non ne sono affatto sicuro, ma sembrerebbe ovvio
        If TabIstr.BlkIstr(1).Segmenti = "" Then TabIstr.BlkIstr(1).Segmenti = TabIstr.BlkIstr(1).Segment 'IRIS-DB
        segmentsQuery = "select IdOggetto from PsDli_Segmenti where nome in ('" & Replace(Replace(TabIstr.BlkIstr(1).Segmenti, "'", ""), ";", "','") & "')" 'IRIS-DB usa tutti i segmenti
        'Se non ho PSB o PCB non filtro i DBD: su tutti!
      Else
        'filtro su tutti i nomi di segmento dell'istruzione...
        'es:
        'select idOggetto from
        '  (SELECT IdOggetto, Count(*) AS occurs
        '  From psDLI_Segmenti
        '  WHERE (Nome='XXX' Or Nome='YYY')
        '  GROUP BY IdOggetto)
        'Where Occurs = 2
        segmentsQuery = "select IdOggetto from " & _
                        "(select idOggetto,count(*) as occurs from psDLI_Segmenti where ("
        Dim irisCount As Integer 'IRIS-DB
        irisCount = 0 'IRIS-DB
        For i = 1 To UBound(TabIstr.BlkIstr)
          If Not InStr(TabIstr.BlkIstr(i).Segment, ",") > 0 Then 'IRIS-DB: controlla che il segmento non sia ",", a quanto pare arriva qui con "," quando ssa multipla
            irisCount = irisCount + 1 'IRIS-DB
            'IRIS-DB molto rischioso, ma necessario... segmentsQuery = segmentsQuery & "nome = '" & Replace(TabIstr.BlkIstr(i).Segment, "'", "") & "'"
            'IRIS-DB devo tornare indietro... segmentsQuery = segmentsQuery & "nome in ('" & Replace(Replace(TabIstr.BlkIstr(1).Segmenti, "'", ""), ";", "','") & "')" 'IRIS-DB usa tutti i segmenti
            'IRIS-DB: non ne sono affatto sicuro, ma sembrerebbe ovvio
            If TabIstr.BlkIstr(i).Segmenti = "" Then TabIstr.BlkIstr(i).Segmenti = TabIstr.BlkIstr(i).Segment 'IRIS-DB
            segmentsQuery = segmentsQuery & "nome in ('" & Replace(Replace(TabIstr.BlkIstr(i).Segmenti, "'", ""), ";", "','") & "')" 'IRIS-DB usa tutti i segmenti
            If i = UBound(TabIstr.BlkIstr) Then
              segmentsQuery = segmentsQuery & ")"
            Else
              segmentsQuery = segmentsQuery & " OR "
            End If
          Else 'IRIS-DB
            IsValid = False 'IRIS-DB
          End If
        Next i
        segmentsQuery = segmentsQuery & " group by idOggetto)" & _
                          " where occurs >=" & irisCount 'IRIS-DB
                          'IRIS-DB " where occurs =" & UBound(TabIstr.BlkIstr)
        segmentsQuery = segmentsQuery & " AND " & dbdWhereCond
      End If
      'SQ 2-05-07
      'Else
      '  'NO FILTRO SUI SEGMENTI:
      '  segmentsQuery = "select idOggetto from PsDLI_Psb where idOggetto IN (" & psbString & ") and NumPcb = " & pcbNumber
      'End If
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' DBD:
      ' - Set wIdDBD()
      ' - Insert in PsDli_IstrDBD
      ' Ho la query finale, differenziata a seconda dei casi:
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Set tb = m_Fun.Open_Recordset(segmentsQuery)
      If tb.RecordCount Then
        ReDim wIdDBD(tb.RecordCount - 1)
        For i = 0 To tb.RecordCount - 1
          wIdDBD(i) = tb!idOggetto
          tb.MoveNext
        Next i
      ElseIf Len(dbdWhereCond) Then
        'tolgo il filtro sui PSB!!!!!!!! (se ne ho di variabili mi frega...)
        'integrare meglio sopra...
        ReDim psbInstr(0)
        segmentsQuery = Left(segmentsQuery, InStr(segmentsQuery, " AND "))
        Set tb = m_Fun.Open_Recordset(segmentsQuery)
        If tb.RecordCount Then
          ReDim wIdDBD(tb.RecordCount - 1)
          For i = 0 To tb.RecordCount - 1
            wIdDBD(i) = tb!idOggetto
            tb.MoveNext
          Next i
        Else
          ' nessun dbd coerente con i segmenti
          ReDim wIdDBD(0)
        End If
      End If
      tb.Close
    End If
    'AC 20/04/2007
    
''''''    'stefano: ricerca con nome segmento!!!!!!!!!!!!!!!!!!
''''''    ' LA SEGNALAZIONE SUL PCB MANCANTE LA LASCIO LO STESSO....
''''''    If UBound(wIdDBD) = 0 And UBound(TabIstr.BlkIstr) > 0 Then
''''''       If wIdDBD(0) = 0 Then
''''''         Set tb = m_Fun.Open_Recordset("select distinct a.idOggetto from PsDLI_segmenti as a, Bs_Oggetti as b where a.nome = '" & TabIstr.BlkIstr(1).segment & "' and a.idoggetto = b.idoggetto and b.tipo_dbd <> 'IND'")
''''''         If Not tb.EOF Then
''''''           ReDim wIdDBD(tb.RecordCount - 1)
''''''           For i = 0 To UBound(wIdDBD)
''''''             wIdDBD(i) = tb!idOggetto
''''''             tb.MoveNext
''''''           Next
''''''         End If
''''''       End If
''''''    End If

''''''virgilio 7/3/2007 inizio
''''''questa pezza messa insieme a Stefano permette di analizzare le istruzioni che hanno problemi
''''''di tipo: manca il PSB, molteplicit� di segmenti su pi� DBD, etc.
''''''CONTROINDICAZIONI: il problema, sistemato mettendo la PRESERVE sulla REDIM, � che viene
''''''                   sempre preso il primo DBD in cui trova il segmento, e questo siginifica
''''''                   che non � detto che sia quello giusto, la cosa buona � che prende il numPCB giusto

    '''''''''''''''''''''''''''''''''''''''''''''''
    ' DBD not found con PSB + PCB
    ' ==> UTILIZZO SEGMENTI (ed eventuali CHIAVI!)
    '''''''''''''''''''''''''''''''''''''''''''''''
    If UBound(wIdDBD) = 0 And UBound(TabIstr.BlkIstr) > 0 Then
      Dim flgDBDOk As Boolean
      i = 0
      If wIdDBD(0) = 0 Then
      'IRIS-DB: cambio importante: accetta anche DBD indice, in quanto in Fedex usano pesantemente accessi sull'indice Fisico che contiene svariati campi
      'IRIS-DB: Da vedere come migrare l'istruzione poi
        'IRIS-DB Set tb = m_Fun.Open_Recordset("select distinct a.idOggetto, idsegmento from PsDLI_segmenti as a, Bs_Oggetti as b where " & _
        'IRIS-DB                              "a.nome = '" & TabIstr.BlkIstr(1).Segment & "' and a.idoggetto = b.idoggetto and " & _
                             'IRIS-DB         "b.tipo_dbd <> 'IND'")
        'IRIS-DB aggiunta verifica sul PSB per ridurre al minimo i doppi inutili; per ora limitata al primo PSB (non considero il caso con pi� di uno)
         If UBound(psbInstr) >= 0 And psbInstr(0) > 0 Then
           Set tb = m_Fun.Open_Recordset("select distinct a.idOggetto, idsegmento from PsDLI_segmenti as a, Bs_Oggetti as b, PsDLI_Psb as c where " & _
                                      "a.nome = '" & TabIstr.BlkIstr(1).Segment & "' and a.idoggetto = b.idoggetto and b.nome = c.dbdname and c.idoggetto = " & psbInstr(0))
         Else
           Set tb = m_Fun.Open_Recordset("select distinct a.idOggetto, idsegmento from PsDLI_segmenti as a, Bs_Oggetti as b where " & _
                                      "a.nome = '" & TabIstr.BlkIstr(1).Segment & "' and a.idoggetto = b.idoggetto")
         End If
        If tb.EOF Then  'AC 11/03/2008 mi serve swsegm valorizzato per segnalazione di segmento mancante
          SwSegm = TabIstr.BlkIstr(1).Segment
        End If
        While Not tb.EOF
          flgDBDOk = True
          If UBound(TabIstr.BlkIstr(1).KeyDef) > 0 Then
            'stefano: ancora modifiche del cavolo: ma perch� la keydef viene definita anche sulle squalificate?
            If TabIstr.BlkIstr(1).KeyDef(1).Key <> "" And TabIstr.BlkIstr(1).KeyDef(1).Key <> "CONCAT" Then 'IRIS-DB lo cambier�
              Dim tb1, tb2 As Recordset
              Set tb2 = m_Fun.Open_Recordset("select * from PsDLI_field where " & _
                                             "nome = '" & TabIstr.BlkIstr(1).KeyDef(1).Key & "' and idsegmento = " & tb!idSegmento)
              If tb2.EOF Then
                Set tb2 = m_Fun.Open_Recordset("select * from PsDLI_xdfield where " & _
                                               "nome = '" & TabIstr.BlkIstr(1).KeyDef(1).Key & "' and idsegmento = " & tb!idSegmento)
              End If
              If tb2.EOF Then
                flgDBDOk = False
              End If
              tb2.Close
            End If
          End If
          If UBound(TabIstr.BlkIstr) > 1 And flgDBDOk Then
            Set tb1 = m_Fun.Open_Recordset("select distinct a.idOggetto, idsegmento from PsDLI_segmenti as a, Bs_Oggetti as b where " & _
                                           "a.nome = '" & TabIstr.BlkIstr(2).Segment & "' and a.idoggetto = b.idoggetto and " & _
                                           "b.tipo_dbd <> 'IND'")
            If Not tb1.EOF Then
              flgDBDOk = True
            Else
              flgDBDOk = False
            End If
            tb1.Close
          End If
          If flgDBDOk Then
            ''''''''''''''''''''''''
            ' DBD trovato!
            ''''''''''''''''''''''''
            ReDim Preserve wIdDBD(i)
            wIdDBD(i) = tb!idOggetto
            i = i + 1
          End If
          tb.MoveNext
        Wend
        tb.Close
      End If
    End If
    
    Dim wPsb As Recordset
    If wIdDBD(0) Then
      '''''''''''''''''''''''''''''''
      ' DBD trovato: scrittura DB...
      '''''''''''''''''''''''''''''''
      'ANCHE SE NE HO N, PRENDO IL PRIMO... POI SEGNALO
      'TMP: trovare psbInstr (filtrato con i DBD OK) e usare quello...
      
      'SQ: chi usa questi? potenzialmente sbagliati!
      TabIstr.psbId = psbInstr(0)
      TabIstr.psbName = m_Fun.Restituisci_NomeOgg_Da_IdOggetto(psbInstr(0))
      
      If UBound(wIdDBD) Then
        dbsObsoleteFilter wIdDBD()
      End If
      
      Set rs = m_Fun.Open_Recordset("select Nome,Tipo_DBD from Bs_oggetti where IdOggetto = " & wIdDBD(0))
      TabIstr.DBD_TYPE = getDBDType(rs!nome, rs)  'rs opzionale...
      TabIstr.DBD = rs!nome
      rs.Close
      
      'Inserimento in Tabella PsDli_IstrDBD
      'SQ CPY-ISTR
      Set rsdbd = m_Fun.Open_Recordset("select * from PsDli_IstrDBD")
      For i = 0 To UBound(wIdDBD)
         rsdbd.AddNew
         rsdbd!idpgm = GbIdOggetto
         rsdbd!idOggetto = IdOggettoRel
         rsdbd!Riga = GbOldNumRec
         rsdbd!idDBD = wIdDBD(i)
         rsdbd.Update
         If pcbNumber = 0 Then 'log con possibili pcb a partire da PSB + DBD
            namedb = getNomeDBD(wIdDBD(i))
            Set rs = m_Fun.Open_Recordset("select Idoggetto,NumPcb from PsDLI_PSB where DBdName = '" & _
            namedb & "' and  IdOggetto in (" & psbString & ")")
            While Not rs.EOF
              'IRIS-DB: removed for cleaning the log from informational messages
              'IRIS-DB: m_Fun.writeLog "Number pcb suggested for programID " & GbIdOggetto & ", instruction row " & GbOldNumRec & " - IdPSB: " & rs!idOggetto & " DBD: " & namedb &
              'IRIS-DB: " ---->  PCB NUMBER = " & rs!numpcb
              rs.MoveNext
            Wend
            rs.Close
         End If
         If psbString = "0" Then 'log con possibili psb a partire da PCB + DBD
            namedb = getNomeDBD(wIdDBD(i))
            Set rs = m_Fun.Open_Recordset("select Idoggetto from PsDLI_PSB where DBdName = '" & _
            namedb & "' and  Numpcb = " & pcbNumber)
            While Not rs.EOF
              'IRIS-DB: removed for cleaning the log from informational messages
                'IRIS-DB m_Fun.writeLog "Id PSB suggested for programId " & GbIdOggetto & " from instruction on row " & GbOldNumRec & " - PCB NUMBER: " & pcbNumber & " DBD: " & namedb &
                'IRIS-DB " ----> IdPSB  = " & rs!idOggetto
                rs.MoveNext
            Wend
            rs.Close
         End If
      Next i
      rsdbd.Close
      'IRIS-DB IsValid = True
    Else
      ''''''''''''''''''''''''''''
      ' SQ 10-04-06
      ' GESTIONE GSAM
      ''''''''''''''''''''''''''''
      Set rs = m_Fun.Open_Recordset("select b.idOggetto as IdDbd,A.dbdName as dbdName from PsDLI_Psb as a,bs_oggetti as b where a.idOggetto IN (" & psbString & ") and NumPcb = " & pcbNumber & " and typePcb='GSAM' and a.dbdName=b.nome and b.tipo='DBD'")
      If rs.RecordCount Then
        TabIstr.DBD = rs!dbdname
        TabIstr.DBD_TYPE = DBD_GSAM
        ReDim wIdDBD(0)
        wIdDBD(0) = rs!idDBD
        dbdWhereCond = "(idOggetto = " & rs!idDBD & ")"
        
        'SQ CPY-ISTR
        'Inserimento in Tabella PsDli_IstrDBD
        Set rsdbd = m_Fun.Open_Recordset("select * from PsDli_IstrDBD")
        rsdbd.AddNew
        rsdbd!idpgm = GbIdOggetto
        rsdbd!idOggetto = IdOggettoRel
        rsdbd!Riga = GbOldNumRec
        rsdbd!idDBD = wIdDBD(0)
        rsdbd.Update
        rsdbd.Close
      Else
        IsValid = False
        'SQ - 23-04-06
        Set rs = m_Fun.Open_Recordset("select DbdName from PsDLI_Psb where idOggetto IN (" & psbString & ") and NumPcb = " & pcbNumber & " and typePcb='DB'")
        If rs.RecordCount Then
          TabIstr.DBD = rs!dbdname
          ' AC 13/03/2008 rivalorizzo wIdDBD(0) con l'Idoggetto del db
          ' in questo modo non ho la segnalazione (errata) di db mancante
          ' da questo punto in poi wIdDBD(0) � usato solo nella if del filtro dei PSB
          ' rivalorizzarlo non dovrebbe introdurre effeti collaterali
          Set tb = m_Fun.Open_Recordset("select IdOggetto from BS_Oggetti where nome = '" & TabIstr.DBD & "'")
          If Not tb.EOF Then
            wIdDBD(0) = tb!idOggetto
          End If
          tb.Close
        Else
          TabIstr.DBD = ""
        End If
        'SQ 28-04-06
        TabIstr.DBD_TYPE = DBD_UNKNOWN_TYPE
        rs.Close
        '''AggiungiSegnalazione TabIstr.DBD, "NODBD", TabIstr.DBD
      End If
    End If
  Else
    '''''''''''''''''''''''''
    ' IMS-DC:
    ' - PSB: quelli del PGM
    ' - NO DBD/SEGM
    '''''''''''''''''''''''''
    ReDim wIdDBD(0)
    ' Ma serve ?
'    If UBound(psbInstr) Then
'      AggiungiSegnalazione " ", "MANYRELPSB", " "
'      'ATTENZIONE: l'istruzione pero' e' valida?!
'    End If
  End If
  
  '''''''''''''''''''''''''''''''''''''''''''''''
  ' PSB: eliminazione incoerenti e scrittura DB
  '''''''''''''''''''''''''''''''''''''''''''''''
  If SwPsbAssociato Then
    '''''''''''''''''''''''''''''''''''''''''''
    ' SQ 26-05-08 !!TMP!!
    ''
    ' BISOGNA FARE FILTRI "INTELLIGENTI" o PILOTATI da flag:
    ' Per sapere se e chi devo buttare via, devo decidere di chi "fidarmi":
    ' del PSB o del PCB?, del SEGMENTO? (se sono valori costanti, per es., sono sicuri!)
    ' ==> primo passo: se ho dedotto il DBD dal segmento (quindi PSB+PCB sbagliati),
    '     mi fido del DBD (e del PCB) e butto il PSB
    ' ==> UNIFORMARE COL PEZZO SOPRA...
    ''
    ' Aggiunto anche pcbNumber > 0 (era dentro)
    '''''''''''''''''''''''''''''''''''''''''''
    'If (UBound(psbInstr) > UBound(wIdDBD)) And wIdDBD(0) Then
    If (flgDBDOk Or (UBound(psbInstr) > UBound(wIdDBD))) And wIdDBD(0) And pcbNumber > 0 Then
      'Filtro la psbInstr con i DBD rimasti...
      'SQ 26-05-08
      'Non va bene la dbdWhereCond... bisogna ricostruirla! (se ho tirato su il DBD utilizzando il segmento, per esempio)
      dbdWhereCond = "("
      For i = 0 To UBound(wIdDBD)
        dbdWhereCond = dbdWhereCond & "dbd.idOggetto = " & wIdDBD(i)
        If i = UBound(wIdDBD) Then  'ultimo giro
          dbdWhereCond = dbdWhereCond & ")"
        Else
          dbdWhereCond = dbdWhereCond & " OR "
        End If
      Next
      
      For i = 0 To UBound(psbInstr)
        'stefano: da rivedere
        'SQ "magazine" (rivista) (ricalcolato sopra: se sono qui � perch� ho almeno un DBD
        'If Len(dbdWhereCond) Then
        Set tb = m_Fun.Open_Recordset("select psb.idOggetto from PsDli_Psb as psb, bs_oggetti as dbd where " & _
                                      "psb.IdOggetto = " & psbInstr(i) & " AND psb.numPcb = " & pcbNumber & " AND " & _
                                      "(psb.dbdName = dbd.nome and dbd.tipo = 'DBD') and " & dbdWhereCond)
        If tb.RecordCount = 0 Then
          'da scartare!
          psbInstr(i) = 0
        End If
        tb.Close
        'ElseIf Not pcbNumber = 0 Then
        '  psbInstr(i) = 0
        '  m_Fun.WriteLog "SP-dbbwherecond vuoto " & GbIdOggetto & "-" & GbOldNumRec
        'End If
      Next i
    End If
    
    For i = 0 To UBound(psbInstr)
      If psbInstr(i) Then
        Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrPSB where " & _
                                      "Idpgm=" & GbIdOggetto & " and IdOggetto=" & IdOggettoRel & " AND " & _
                                      "Riga=" & GbOldNumRec & " AND IdPSB=" & psbInstr(i))
        'lo faccio da dentro per evitare chiavi duplicate:
        If tb.RecordCount = 0 Then
          tb.AddNew
          tb!idpgm = GbIdOggetto
          tb!idOggetto = IdOggettoRel
          tb!Riga = GbOldNumRec
          tb!IdPsb = psbInstr(i)
          tb.Update
        End If
        tb.Close
      End If
    Next i
  End If
End Sub
Sub getDbdfromPsb(pcbNumber As Long, psbId As Long) 'IRIS-DB
  Dim rs As Recordset
  Set rs = m_Fun.Open_Recordset("select DBDName from Psdli_PSB where IdOggetto = " & psbId & " and Numpcb = " & pcbNumber)
  If Not rs.EOF Then
    TabIstr.DBD = rs!dbdname
  End If
  rs.Close
    
 End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' (Parsing II livello)
' Valorizza la variabile globale psbPgm()
' a) IMS-DC
' Nome PSB=Nome PGM
' b) Altri:
' Ricava il PSB dalla PsRel_obj:
'  se non esiste, cerca quello del chiamante...
' Effetti collaterali:
' - Aggiorna la PsRel_obj (NON PIU')!!!!!!!!!!!!!!!!!!!!!!!!!!
' - pulisce le mancanze dalla bs_segnalazioni (F07... esiste ancora?)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function FindPSBTwoLevelAndMore(pIdObject As Long) As Collection
  Dim rs As New Recordset
  Dim rsCaller As Recordset
  Dim StrAllIdPsb As String
  Dim StrPsb As String
  Dim ColIdPsb As New Collection
  Dim appoColIdPsb As Collection
  Dim i As Integer
  Dim items As Integer

  Set FindPSBTwoLevelAndMore = ColIdPsb
  Set rs = m_Fun.Open_Recordset("select * from PsRel_obj where " & _
                                "idoggettor = " & pIdObject & " AND relazione = 'CAL' " & _
                                "and idoggettor <> idoggettoc")
  If rs.EOF Then Exit Function
  
  While Not rs.EOF
    Set rsCaller = m_Fun.Open_Recordset( _
           "select b.idOggettoR as IdPsb,a.idOggettoR as IdParent,b.NomeComponente as Nome from PsRel_obj as a,PsRel_obj as b " & _
           "where a.idoggettor = " & rs!IdOggettoC & " AND a.relazione = 'CAL' and " & _
           "a.idOggettoC = b.idOggettoC and b.relazione = 'PSB'")
    ' Mauro 01/02/2008 : Se fa pi� di un giro, va in chiave duplicata
    'While Not rsCaller.EOF
    If rsCaller.RecordCount Then
      ColIdPsb.Add (rsCaller!IdPsb)
      'IRIS-DB: non inserisce la relazione per i PSB "ereditati", perch� fa pi� casino che altro
      'SCRIVI RELAZIONE PER CHIAMANTE E IL CHIAMATO - Livello 0 e 1
'''''      rs.AddNew ' Liv 0
'''''      rs!IdOggettoC = pIdObject
'''''      rs!idOggettoR = rsCaller!IdPsb
'''''      rs!relazione = "PSB"
'''''      rs!nomecomponente = PsbNameFromId(rsCaller!IdPsb)
'''''      rs!utilizzo = "PSB-INHE"
'''''      'rs.Update
'''''      rs.AddNew  ' Liv 1
'''''      rs!IdOggettoC = rsCaller!IdParent
'''''      rs!idOggettoR = rsCaller!IdPsb
'''''      rs!relazione = "PSB"
'''''      rs!nomecomponente = PsbNameFromId(rsCaller!IdPsb)
'''''      rs!utilizzo = "PSB-INHE"
'''''      rs.Update
      'rsCaller.MoveNext
    'Wend
    End If
    rsCaller.Close
    rs.MoveNext
  Wend
  
  Set FindPSBTwoLevelAndMore = ColIdPsb
  If ColIdPsb.count = 0 Then
    ' riparto dai chiamanti cercando i psb fra i chiamanti di livello superiore
    rs.MoveFirst
    While Not rs.EOF
      Set appoColIdPsb = FindPSBTwoLevelAndMore(rs!IdOggettoC)
      items = appoColIdPsb.count
      For i = 1 To items
        ColIdPsb.Add (appoColIdPsb.item(i))
      Next i
      rs.MoveNext
    Wend
    'SCRIVI RELAZIONE PER CHIAMATO - Livello 0  rs!idOggettoR (per livello 1 e 2 scrittura dentro funzione)
    'IRIS-DB: non inserisce la relazione per i PSB "ereditati", perch� fa pi� casino che altro
''''    For i = 1 To ColIdPsb.count
''''      rs.AddNew ' Liv 0
''''      rs!IdOggettoC = pIdObject
''''      rs!idOggettoR = ColIdPsb.item(i)
''''      rs!relazione = "PSB"
''''      rs!nomecomponente = PsbNameFromId(ColIdPsb.item(i))
''''      rs!utilizzo = "PSB-INHE"
''''      rs.Update
''''    Next i
    Set FindPSBTwoLevelAndMore = ColIdPsb
  End If
  rs.Close
End Function
Public Function PsbNameFromId(pIdPsb As Long) As String
  Dim rs As New Recordset
  Set rs = m_Fun.Open_Recordset("select Nome from Bs_Oggetti  where idoggetto = " & pIdPsb & " AND tipo = 'PSB'")
  If Not rs.EOF Then
    PsbNameFromId = rs!nome
  Else
    PsbNameFromId = ""
  End If
End Function

Public Sub getPsb_program()
  Dim rs As Recordset, rsRelObj As Recordset, rsOggetti As Recordset
  Dim i As Integer
  Dim ColIdPsb As New Collection
  
  SwPsbAssociato = False
  ReDim psbPgm(0)
  'IRIS-DB prendeva il PSB del programma precedente se non assegnato in questo...
  IdPsb = 0 'IRIS-DB
  'IRIS-DB: controlla nella tabella custom se ci sono PSB associati, nel qual caso prende quelli
  Set rs = m_Fun.Open_Recordset("Select IdPSB from IRIS_PGM_PSB where idpgm = " & GbIdOggetto)
  If rs.RecordCount Then
    SwPsbAssociato = True
    ReDim psbPgm(rs.RecordCount - 1)
    'aggiorna anche la tabella psrel_obj per forzare i PSB custom
    m_Fun.FnConnection.Execute "delete from PsRel_Obj Where idoggettoc = " & GbIdOggetto & " and relazione = 'PSB' "
    For i = 0 To rs.RecordCount - 1
      psbPgm(i) = rs!IdPsb
      Set rsOggetti = m_Fun.Open_Recordset("Select nome from bs_oggetti where idOggetto = " & GbIdOggetto)
      m_Fun.FnConnection.Execute "insert into PsRel_Obj (idoggettoc,idoggettor,relazione,nomecomponente,utilizzo) values(" _
       & GbIdOggetto & ", " & psbPgm(i) & ", 'PSB','" & rsOggetti!nome & "','PSB-PGM')"
      rsOggetti.Close
      rs.MoveNext
    Next
    rs.Close
    Exit Sub
  End If
  rs.Close
  'IRIS-DB end
    
  Set rs = m_Fun.Open_Recordset("Select IdOggettoR from PsRel_Obj where idoggettoc = " & GbIdOggetto & " and relazione = 'PSB' ")
  If rs.RecordCount Then
    SwPsbAssociato = True
    ReDim psbPgm(rs.RecordCount - 1)
    For i = 0 To rs.RecordCount - 1
      psbPgm(i) = rs!idOggettoR
      rs.MoveNext
    Next
'IRIS-DB: non inserisce la relazione per i PSB "ereditati", perch� fa pi� casino che altro; usiamo i PSB dummy piuttosto
''''  Else
''''    'SCHEDULATO DAI CHIAMANTI?
''''    Set rs = m_Fun.Open_Recordset("select distinct b.idOggettoR from PsRel_obj as a,PsRel_obj as b " & _
''''                                  "where a.idoggettor = " & GbIdOggetto & " AND a.relazione = 'CAL' and " & _
''''                                        "a.idOggettoC = b.idOggettoC and b.relazione = 'PSB'")
''''    If rs.RecordCount Then
''''      SwPsbAssociato = True
''''      Set rsRelObj = m_Fun.Open_Recordset("Select * from PsRel_Obj")
''''      ReDim psbPgm(rs.RecordCount - 1)
''''      For i = 0 To rs.RecordCount - 1
''''        psbPgm(i) = rs!idOggettoR
''''        '''''''''''''''''''''''''''
''''        ' Aggiunta in PsRel_Obj
''''        '''''''''''''''''''''''''''
''''        rsRelObj.AddNew
''''        rsRelObj!IdOggettoC = GbIdOggetto
''''        rsRelObj!idOggettoR = psbPgm(i)
''''        rsRelObj!relazione = "PSB"
''''        rsRelObj!nomecomponente = PsbNameFromId(psbPgm(i))
''''        rsRelObj!utilizzo = "PSB-INHE"
''''
''''        rs.MoveNext
''''      Next i
''''      rsRelObj.Update
''''      rsRelObj.Close
''''    Else
''''      Set ColIdPsb = FindPSBTwoLevelAndMore(GbIdOggetto)
''''      ReDim psbPgm(ColIdPsb.count)
''''      For i = 0 To ColIdPsb.count - 1
''''        psbPgm(i) = ColIdPsb.item(i + 1)
''''      Next i
''''    End If
  End If
  rs.Close
  
  If Not SwPsbAssociato Then
    '''''''''''''''''''''''''''''''''''''''''''''
    ' SQ - 14-04-06
    ' Pezza: prendiamo quelli con lo stesso nome!
    ' (come per imsDc!)
    '''''''''''''''''''''''''''''''''''''''''''''
    Set rs = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where nome = '" & GbNomePgm & "' and tipo = 'PSB'")
    If rs.RecordCount = 0 Then
      Set rs = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.source = '" & GbNomePgm & "' and b.load=a.nome and a.tipo = 'PSB'")
    End If
    If rs.RecordCount Then
      psbPgm(0) = rs!idOggetto
      SwPsbAssociato = True
      '''''''''''''''''''''''''''
      ' Aggiunta in PsRel_Obj
      '''''''''''''''''''''''''''
      Set rsRelObj = m_Fun.Open_Recordset("Select * from PsRel_Obj where idOggettoC = " & GbIdOggetto & " and idOggettoR = " & psbPgm(0) & " and relazione = 'PSB'")
      If rsRelObj.EOF Then
        rsRelObj.AddNew
        rsRelObj!IdOggettoC = GbIdOggetto
        rsRelObj!idOggettoR = psbPgm(0)
        rsRelObj!relazione = "PSB"
        rsRelObj!nomecomponente = GbNomePgm
        rsRelObj!utilizzo = "PSB-PGM"
        rsRelObj.Update
      End If
      rsRelObj.Close
    End If
    rs.Close
  End If
  '...
  '        TbRela!nomeComponente = "ITERPSB"
  '...
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''
'3-04-06 Modificato per gli Alternate PCB
'Effetto Collaterale: setta isDC
' SQ 16-10-07 - IMPORTANTISSIMO
' Gestione (tmp o definitiva?) INFO CORRECTOR
' -> Nuova colonna in PsDli_Istruzioni: OrdPCB_MG
'''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getPCBnumber(pcbField As String, Optional isExec = False) As Integer
  Dim rs As Recordset, rsCmp As Recordset, rsPCB As Recordset
  Dim pcb As Integer
  
  IsDC = False  'init
  
  '''''''''''''''
  'SQ 16-10-07
  '''''''''''''''
  On Error GoTo ok_tmp  'tmp per vecchi prj
  'Esiste un VALORE "di Corrector"?
  Set rs = m_Fun.Open_Recordset("SELECT OrdPCB_MG FROM PsDli_Istruzioni WHERE " & _
                                " IdPgm = " & GbIdOggetto & " AND idOggetto = " & IdOggettoRel & " AND " & _
                                "riga=" & GbOldNumRec & " AND OrdPCB_MG > 0")
  If rs.RecordCount Then
    getPCBnumber = rs!OrdPCB_MG
  End If
  rs.Close
ok_tmp:
  'IRIS-DB: estrae il PCB "vero" in caso di PCB dinamico

  Set rs = m_Fun.Open_Recordset("SELECT TargPCB FROM IRIS_DYN_PCB WHERE " & _
                                " IdPgm = " & GbIdOggetto & " AND OrigPCB = '" & pcbField & "'")
  If rs.RecordCount Then
    pcbField = rs!TargPCB
  End If
  rs.Close
'end IRIS

'IRIS-DB: estrae il numero PCH dalla tabella per i sottoprogrammi
  Set rs = m_Fun.Open_Recordset("SELECT PCBNumber FROM IRIS_SUB_PCB WHERE " & _
                                " IdPgm = " & GbIdOggetto & " AND PCBName = '" & pcbField & "'")
  If rs.RecordCount Then
    getPCBnumber = rs!pcbNumber
  End If
  rs.Close
'end IRIS
  If getPCBnumber = 0 Then
    If Not isExec Then
      For pcb = 0 To UBound(gbCurEntryPCB)
        If gbCurEntryPCB(pcb) = pcbField Then
          getPCBnumber = pcb + 1
          Exit For
        End If
      Next pcb
      'VC: Gestione casi Area PCB in sottolivello (es: 01 PCB-IO. 03 PSB-MSG... e viene usato PSB-MSG!)
      If getPCBnumber = 0 Then
        For pcb = 0 To UBound(gbCurEntryPCB)
         Set rs = m_Fun.Open_Recordset("Select idCopy from PsData_Cmp where IdOggetto=" & GbIdOggetto & " and nome='" & gbCurEntryPCB(pcb) & "'")
         If Not rs.EOF Then
           If rs!idCopy > 0 Then
             Set rsCmp = m_Fun.Open_Recordset("Select Nome from PsData_Cmp where IdOggetto=" & rs!idCopy & " order by IdArea,Ordinale")
             If Not rsCmp.EOF Then
               If rsCmp!nome = pcbField Then
                 getPCBnumber = pcb + 1
                 rsCmp.Close
                 rs.Close
                 Exit For
               End If
             End If
             rsCmp.Close
           Else
             'SQ - bisogna prendere il record successivo (dell'area stessa)!
             '...
           End If
         End If
         rs.Close
        Next pcb
      End If
      ' Mauro 11-05-2007 : Ricerca PCB Number solo per Perot
      If getPCBnumber = 0 And (m_Fun.FnNomeDB = "Prj-Perot.mty" Or m_Fun.FnNomeDB = "Prj-Perot-POC.mty") Then
        Set rsPCB = m_Fun.Open_Recordset("select ordinale from PsData_Cmp where nome = '" & pcbField & "-PTR' and idoggetto = " & GbIdOggetto)
        If Not rsPCB.EOF Then
          getPCBnumber = rsPCB!Ordinale - 1
        End If
        rsPCB.Close
      End If
      ' IRIS-DB: questo � per CSX, ma lo faccio per tutti - � un po' forzato, ci sono molti standard un po' misti
      If getPCBnumber = 0 Then
        Dim PCBPieces() As String
        Dim idArea As Integer
        PCBPieces = Split(pcbField, "-")
        Set rsPCB = m_Fun.Open_Recordset("select ordinale, IdArea from PsData_Cmp where " & _
        " (nome like '%" & PCBPieces(0) & "%-PTR' or nome like '%PTR%" & PCBPieces(0) & "%' or nome like '%" & Left(PCBPieces(0), 3) & "%-PTR' or nome like '%PTR%" & Left(PCBPieces(0), 3) & "%') " & _
        " and nome not like '%TWA%' and idoggetto = " & GbIdOggetto)
        If Not rsPCB.EOF Then
          getPCBnumber = rsPCB!Ordinale - 1
          idArea = rsPCB!idArea
          rsPCB.Close
       'IRIS-DB subtracts the redefines
          Set rsPCB = m_Fun.Open_Recordset("select count(*) as NumRed from PsData_Cmp where NomeRed <> '' and IdArea = " & idArea & " and ordinale <= " & getPCBnumber & " and idoggetto = " & GbIdOggetto)
          If Not rsPCB.EOF Then
            getPCBnumber = getPCBnumber - rsPCB!numred
          End If
        End If
        rsPCB.Close
      End If
      ' IRIS-DB: ancora ZERO, ulteriore analisi "a tentoni": invece di cercare tutti quelli che iniziano o finiscono per PCB, mi posiziono sotto l'area dei puntatori
      If getPCBnumber = 0 Then
        Set rsPCB = m_Fun.Open_Recordset("select idArea from PsData_Cmp where nome = 'PCB-ADDRESS-LIST' and idoggetto = " & GbIdOggetto)
        If Not rsPCB.EOF Then
          idArea = rsPCB!idArea
          rsPCB.Close
          Set rsPCB = m_Fun.Open_Recordset("select idArea from PsData_Cmp where nome = '" & pcbField & "' and idoggetto = " & GbIdOggetto)
          If Not rsPCB.EOF Then
            getPCBnumber = rsPCB!idArea - idArea
          End If
        End If
        rsPCB.Close
      End If
    Else ' exec
      If Not IsNumeric(pcbField) Then
        getPCBnumber = "0" & Replace(MapsdM_Campi.getFieldValue(pcbField), "+", "")
      Else
        getPCBnumber = pcbField
      End If
      'SQ - portato sotto: pezzo comune a tutti
      'If getPCBnumber = 0 Then
      ' getPCBnumber = -1
      ' addSegnalazione pcbField, "UNKPCB", pcbField
      '  Exit Function
      'End If
    End If
  End If
  
  '''''''''''''''''''''''''''''''''''''''''
  ' isDC
  '''''''''''''''''''''''''''''''''''''''''
  If getPCBnumber > 0 Then
    If SwTpIMS Then
      'SQ 16-10-07
      'If pcb = 0 Then
      'IRIS-DB purtroppo rimossa, perch� ci sono chiamati che non riescono ad associare il PSB, ma hanno il PCB tipo DB per primo e i PCB tipo TP dopo...
      'IRIS-DB If getPCBnumber = 1 Then
        'PCB-IO ==> IMS/DC
       'IRIS-DB IsDC = True
      'IRIS-DB Else
        'PCB-ALTERNATE ==> IMS/DC
        'SQ
        'If pcb < (UBound(gbCurEntryPCB) + 1) Or isExec Then
          Set rs = m_Fun.Open_Recordset("Select TypePCB from PsDLI_Psb where IdOggetto = " & IdPsb & " and numPCB = " & getPCBnumber)
          If Not rs.EOF Then
            If Trim(rs!TypePcb) = "TP" Then IsDC = True
          Else 'IRIS-DB
            If getPCBnumber = 1 And IdPsb Then IsDC = True 'IRIS-DB buco dopo buco: non carica il PCB 1 come IO-PCB quando c'� CMPAT = "Y"
          End If
          rs.Close
        'End If
      'IRIS-DB End If
    End If
  Else
    '''''''''''''''''''''''''
    'segnalazione UnknownPcb
    '''''''''''''''''''''''''
    addSegnalazione pcbField, "UNKPCB", pcbField
  End If
End Function

''''''''''''''''''''''''''''''''''''''
' Analizza l'inputStream CHECKPOINT
' => inserisce l'istruzione nel DB
''''''''''''''''''''''''''''''''''''''
Sub parseDliChkp(statement As String, inputStream As String)
  Dim rs As Recordset
  Dim token As String
  ''''''''''''''''
  ' PARSING:
  ''''''''''''''''
  token = nexttoken(inputStream)
  If token = "ID" Then
    token = nexttoken(inputStream)
  End If
  Set rs = m_Fun.Open_Recordset("select * from PsDli_Istruzioni where idoggetto = " & GbIdOggetto & " and riga = " & GbOldNumRec)
  If rs.RecordCount = 0 Then
    rs.AddNew
  End If
  If (Left(token, 1) = "(") Then  'elimino le parentesi esterne: non fare replace che ce ne possono essere interne...
    token = Mid(Left(token, Len(token) - 1), 2)
  End If
  '''''''''''''''''''''
  ' INSERIMENTO IN DB:
  '''''''''''''''''''''
  'SQ COPY-ISTR
  'rs!idOggetto = GbIdOggetto
  rs!idOggetto = IdOggettoRel
  rs!idpgm = GbIdOggetto
  rs!Riga = GbOldNumRec
  rs!valida = True
  'rs!DtSegnalazione = Now
  rs!statement = statement
  rs!istruzione = "CHKP"
  rs!numpcb = token 'campo rapinato...
  rs.Update
  rs.Close
End Sub

''''''''''''''''''''''''''''''''''''''
' Analizza lo inputStream QUERY
' => inserisce l'istruzione nel DB
''''''''''''''''''''''''''''''''''''''
Sub parseDliQuery(statement As String, inputStream As String)
  Dim rs As Recordset
  Dim token As String
  ''''''''''''''''
  ' PARSING:
  ''''''''''''''''
  token = nexttoken(inputStream)
  If token = "USING" Then
    token = nexttoken(inputStream)
  End If
  'vado avanti cosi' perche' non ho guardato la sintassi... (opzionali?!)
  If token = "PCB" Then
    token = nexttoken(inputStream)
    Set rs = m_Fun.Open_Recordset("select * from PsDli_Istruzioni where idoggetto = " & GbIdOggetto & " and riga = " & GbOldNumRec)
    If rs.RecordCount = 0 Then
      rs.AddNew
    End If
    If (Left(token, 1) = "(") Then  'elimino le parentesi esterne: non fare replace che ce ne possono essere interne...
      token = Mid(Left(token, Len(token) - 1), 2)
    End If
    '''''''''''''''''''''
    ' INSERIMENTO IN DB:
    '''''''''''''''''''''
    'SQ COPY-ISTR
    'rs!idOggetto = GbIdOggetto
    rs!idOggetto = IdOggettoRel
    rs!idpgm = GbIdOggetto
    rs!Riga = GbOldNumRec
    rs!valida = True
    'rs!DtSegnalazione = Now
    rs!statement = statement
    rs!istruzione = "QUERY"
    rs!numpcb = token 'campo rapinato...
    rs.Update
    rs.Close
  Else
    'tmp:
    MsgBox "parseDliQuery: boh?"
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''
' Riconoscimento CMPAT per PCB-IO:
' numPcb parte da 2
' Per PSB "IMS" il CMPAT=YES � automatico!
''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parsePSB()
  Dim rs As Recordset
  Dim FD As Long
  Dim wRecIn As String, wRec As String, wNomeDBD As String, wStr As String, firstWord As String
  Dim k1 As Integer
  Dim wSql As String, wSegmento As String, wParent As String, wProcSeq As String, wKeyLen As String, wType As String
  Dim pcbNumber As Integer, senSegNumber As Integer
  Dim pcbStruct() As PcbStr
  Dim ProcOpt As String
  
  ReDim pcbStruct(0)
  pcbNumber = 0
  
  GbNumRec = 0
  FD = FreeFile
  Open GbFileInput For Input As FD
  While Not EOF(FD)
    wRec = ""
    Line Input #FD, wRecIn
    GbNumRec = GbNumRec + 1
    If Not EOF(FD) Then
      While Mid(wRecIn & Space(72), 72, 1) <> " " And Not EOF(FD)
        'SQ - 16-10-06: Mi serve l'incolonnamento!
        'wRec = wRec & Trim(Left(wRecIn, 71))
        wRec = wRec & Left(wRecIn, 71)
        Line Input #FD, wRecIn
        GbNumRec = GbNumRec + 1
      Wend
      'SQ - 16-10-06: Mi serve l'incolonnamento!
      'wRec = wRec & LTrim(Left(wRecIn & Space(72), 71))
      wRec = wRec & Left(wRecIn & Space(72), 71)
    
      If Left(wRec, 1) <> "*" Then
        'SQ 16-10-06
        'Alcuni PSB hanno la LABEL
        'Non ho guardato il manuale per chiarire l'"incolonnamento":
        'facciamo finta (dovrebbe essere vero!) che a colonna 1 sia una label:
        If Left(wRec, 1) <> " " Then
          'LABEL: ci servir�?
          firstWord = nexttoken(wRec)
        End If
        
        'firstWord = wRec
        'K1 = InStr(firstWord, " ")
        'firstWord = Trim(Left(firstWord, K1))
        firstWord = nexttoken(wRec)
        Select Case firstWord
          Case "PCB"
            'SQ 16-02-06
            GbOldNumRec = GbNumRec
            pcbNumber = pcbNumber + 1
            ReDim Preserve pcbStruct(pcbNumber)
            senSegNumber = 0
            ReDim Preserve pcbStruct(pcbNumber).SenSeg(senSegNumber)
            
            wType = TrovaParametro(wRec, "TYPE")
            'SQ - 5-12-05
            'ES.: PCB     TYPE=GSAM,NAME=CVDACTGB
            'I GSAM non hanno DBDNAME...
            'VERIFICARE GLI ALTRI CASI...
            If wType = "GSAM" Then
              wNomeDBD = TrovaParametro(wRec, "NAME")
            Else
              wNomeDBD = TrovaParametro(wRec, "DBDNAME")
              'VIRGILIO 13-02-2006
              'gestito il nome DBD per eventi strani lo chiamano con NAME e NON DBDNAME
              'Fare un "case" con il ventaglio completo di situazioni...
              If UCase(wNomeDBD) = "NONE" Then
                wNomeDBD = TrovaParametro(wRec, "NAME")
              End If
            End If
            
            wProcSeq = TrovaParametro(wRec, "PROCSEQ")
            wKeyLen = TrovaParametro(wRec, "KEYLEN")
            ProcOpt = TrovaParametro(wRec, "PROCOPT")
            
            pcbStruct(pcbNumber).dbdname = wNomeDBD
            pcbStruct(pcbNumber).KeyLen = Val(wKeyLen)
            pcbStruct(pcbNumber).ProcSeq = wProcSeq
            pcbStruct(pcbNumber).ProcOpt = ProcOpt
            pcbStruct(pcbNumber).Type = wType
            ' perch� 'sto trattamento speciale per i GSAM???
            If wType = "DB" Or wType = "GSAM" Then
              Set rs = m_Fun.Open_Recordset("SELECT * FROM BS_OGGETTI WHERE NOME = '" & pcbStruct(pcbNumber).dbdname & "' AND TIPO = 'dbd'")
              If rs.RecordCount = 0 Then
                addSegnalazione pcbStruct(pcbNumber).dbdname, "NODBD", pcbStruct(pcbNumber).dbdname
              End If
              rs.Close
            End If
          Case "SENSEG"
            'SQ 16-02-06
            senSegNumber = senSegNumber + 1
            ReDim Preserve pcbStruct(pcbNumber).SenSeg(senSegNumber)
            wSegmento = TrovaParametro(wRec, "NAME")
            wParent = TrovaParametro(wRec, "PARENT")
            pcbStruct(pcbNumber).SenSeg(senSegNumber).Segmento = wSegmento
            pcbStruct(pcbNumber).SenSeg(senSegNumber).Parent = wParent
          Case "PSBGEN"
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'CMPAT=YES:
            ' Utilizza il PCB-IO => il primo PCB e' il numero 2...
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim withPCBIO As Boolean
            withPCBIO = InStr(wRec, "CMPAT=YES")
            'SQ 20-04-06
            If Not withPCBIO Then
              'controllo se � IMS
              'IRIS-DB ripristinato controllo con il tipo TP, che sembra corretto
              ' IRIS-DB Set rs = m_Fun.Open_Recordset("select * from IMS_Catalog where PSB='" & GbNomePgm & "'") ' and pgmType='TP'")
              Set rs = m_Fun.Open_Recordset("select * from IMS_Catalog where PSB='" & GbNomePgm & "' and pgmType='TP'") 'IRIS-DB
              withPCBIO = Not rs.EOF
              rs.Close
            End If
        End Select
      End If
    End If
  Wend
  
  Close #FD
  On Error Resume Next
  
  Parser.PsConnection.Execute "delete * from psdli_psb where idoggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "delete * from PsDli_PSBSEG where idoggetto = " & GbIdOggetto
  
  Set TbPSB = m_Fun.Open_Recordset("select * from PsDli_PSB where idoggetto = " & GbIdOggetto)
  Set TbPSBSEG = m_Fun.Open_Recordset("select * from PsDli_PSBSEG where idoggetto = " & GbIdOggetto)
  For pcbNumber = 1 To UBound(pcbStruct)
    TbPSB.AddNew
    TbPSB!idOggetto = GbIdOggetto
    TbPSB!numpcb = IIf(withPCBIO, pcbNumber + 1, pcbNumber)
    TbPSB!dbdname = pcbStruct(pcbNumber).dbdname
    TbPSB!TypePcb = pcbStruct(pcbNumber).Type
    TbPSB!ProcSeq = pcbStruct(pcbNumber).ProcSeq
    TbPSB!ProcOpt = pcbStruct(pcbNumber).ProcOpt
    TbPSB!KeyLen = pcbStruct(pcbNumber).KeyLen
    TbPSB.Update
    
    For senSegNumber = 1 To UBound(pcbStruct(pcbNumber).SenSeg)
      TbPSBSEG.AddNew
      TbPSBSEG!idOggetto = GbIdOggetto
      TbPSBSEG!numpcb = IIf(withPCBIO, pcbNumber + 1, pcbNumber)
      TbPSBSEG!Ordinale = senSegNumber
      TbPSBSEG!Segmento = pcbStruct(pcbNumber).SenSeg(senSegNumber).Segmento
      If UCase(pcbStruct(pcbNumber).SenSeg(senSegNumber).Parent) = "NONE" Then
        pcbStruct(pcbNumber).SenSeg(senSegNumber).Parent = "0"
      End If
      TbPSBSEG!Parent = pcbStruct(pcbNumber).SenSeg(senSegNumber).Parent
      TbPSBSEG.Update
    Next
  Next
  'IRIS-DB inserisce anche il PCB di IO, non c'� motivo di farlo estrarre ogni volta che si analizzano le istruzioni
  If withPCBIO Then
    TbPSB.AddNew
    TbPSB!idOggetto = GbIdOggetto
    TbPSB!numpcb = 1
    TbPSB!dbdname = "None"
    TbPSB!TypePcb = "TP"
    TbPSB!ProcSeq = "None"
    TbPSB!ProcOpt = "None"
    TbPSB!KeyLen = 0
    TbPSB.Update
  End If
  TbPSB.Close
  TbPSBSEG.Close
  
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  TbOggetti!DtParsing = Now
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti!parsinglevel = 1
  TbOggetti.Update
  TbOggetti.Close
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 24-01-07
' Gestione DBD Logici: ci attribuiamo i FIELD "fisici"
''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub parseDBD()
  Dim FD As Long
  Dim line As String, wRecIn As String, wRec As String, DBDType As String, nomeDBD As String, wStr As String, firstWord As String
  Dim wSegmento As String, wSegmOrd As String, wfield As String, wIndex As String, WLchld As String, wSqlExec As String
  Dim rsSegmenti As Recordset, TbField As Recordset, TbXDField As Recordset, TbRelDli As Recordset, rs As Recordset
  Dim segmSource As SEGM_SOURCE
  Dim sourceFields() As String
  Dim i As Integer
  ' Mauro 02-04-2007
  Dim WSegChld  As String
  Dim WPair As String
  Dim NameLChld As String
  Dim rsLChld As Recordset, maxId As Long
    
  On Error GoTo catch
  'init
  MapsdM_Parser.resetRepository "DBD"
  ReDim GbSegmenti(0)
  GbIndSeg = 0
  
  FD = FreeFile
  Open GbFileInput For Input As FD
  While Not EOF(FD)
    wRec = ""
    Line Input #FD, line
    GbNumRec = GbNumRec + 1
    'SQ -  gestione LABEL:
    'Verificare bene l'incolonnamento...
    Dim isLabel As Boolean
    isLabel = False
    If Len(line) Then
      isLabel = Left(line, 1) <> " "
    End If
    If Not EOF(FD) Then
      While Mid(line & Space(72), 72, 1) <> " " And Not EOF(FD)
        wRec = wRec & Trim(Left(line, 71))
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
      Wend
      wRec = wRec & LTrim(Left(line & Space(72), 71))
      
      If Left(wRec, 1) <> "*" Then
        'SQ 3-11-06
        If isLabel Then
          'Mangio la LABEL
          wStr = nexttoken(wRec)
        End If
        firstWord = Trim(Left(wRec, InStr(wRec, " ")))
        Select Case firstWord
          Case "DBD"
            nomeDBD = TrovaParametro(wRec, "NAME")
            DBDType = TrovaParametro(wRec, "ACCESS")
            If Left(DBDType, 1) = "(" Then DBDType = Mid(DBDType, 2)
            If InStr(DBDType, ",") > 0 Then DBDType = Left(DBDType, InStr(DBDType, ",") - 1)
            DBDType = Left(Trim(DBDType), 3)
            'Serve?
            If DBDType = "GIS" Then DBDType = "GSA"
          Case "SEGM"
            GbIndSeg = GbIndSeg + 1
            ReDim Preserve GbSegmenti(GbIndSeg)
            GbIndField = 0
            ReDim Preserve GbSegmenti(GbIndSeg).FIELD(GbIndField)
            '
            GbSegmenti(GbIndSeg).nome = TrovaParametro(wRec, "NAME")
            
            'wStr = TrovaParametro(wRec, "PARENT")
            wStr = findParameter(wRec, "PARENT")
            If Len(wStr) Then
              If Left(wStr, 1) = "(" Then
                'SQ 14-12-06 - Gestione: PARENT=((S138),(S360,P,P038))
                wStr = Mid(Left(wStr, Len(wStr) - 1), 2) 'Eliminazione parentesi
              End If
              Dim token As String
              token = nexttoken(wStr)
              If Left(token, 1) = "(" Then
                'SQ 14-12-06 - Gestione: PARENT=((S138),(S360,P,P038))
                token = Mid(Left(token, Len(token) - 1), 2) 'Eliminazione parentesi
                'PARENT=((S138,DBLE),(S360,P,P038))
                sourceFields = Split(token, ",")
                'TMP: BUTTIAMO VIA GLI ATTRIBUTI DEL PADRE FISICO... GESTIRE!
                token = sourceFields(0)
              End If
              GbSegmenti(GbIndSeg).Parent = token
              If Left(wStr, 1) = "," Then
                'LParent!
                wStr = Mid(wStr, 2)
                token = nexttoken(wStr)
                If Left(token, 1) = "(" Then
                  token = Mid(Left(token, Len(token) - 1), 2)
                  'E' da splittare!... checkRelation!
                  sourceFields = Split(token, ",")
                  If UBound(sourceFields) = 2 Then
                    segmSource.Segment = sourceFields(0)
                    ' Mauro 05/10/2009
                    segmSource.Type = Left(Trim(sourceFields(1)), 3)
                    segmSource.DBD = sourceFields(2)
                    GbSegmenti(GbIndSeg).LParent = segmSource
                    'AUDITING:
                    MapsdM_Parser.checkRelations segmSource.DBD, "DBD-REL"
                  Else
                    'TMP: Possibile?!
                    m_Fun.writeLog "#!! parseDBD " & GbIdOggetto & ": SOURCE=" & token
                  End If
                End If
              End If
            Else
              'DEFAULT: PARENT=0
              GbSegmenti(GbIndSeg).Parent = "0"
            End If
             '
            GbSegmenti(GbIndSeg).Pointer = TrovaParametro(wRec, "PTR")
            wStr = TrovaParametro(wRec, "BYTES")
            ' Mauro 20/11/2007: Posso avere le parentesi anche se il valore � uno solo
            'If Left(wStr, 1) <> "(" Then
            '  GbSegmenti(GbIndSeg).MaxLen = Val(wStr)
            '  GbSegmenti(GbIndSeg).MinLen = 0
            'End If
            wStr = Replace(wStr, "(", "")
            wStr = Replace(wStr, ")", "")
            If InStr(wStr, ",") Then
              GbSegmenti(GbIndSeg).MaxLen = Val(Mid(wStr, 1, InStr(wStr, ",") - 1))
              GbSegmenti(GbIndSeg).MinLen = Val(Mid(wStr, InStr(wStr, ",") + 1))
            Else
              GbSegmenti(GbIndSeg).MaxLen = Val(wStr)
              GbSegmenti(GbIndSeg).MinLen = 0
            End If
            
            wStr = TrovaParametro(wRec, "RULES")
            If Left(wStr, 1) <> "(" Then
              GbSegmenti(GbIndSeg).Rules = wStr
            Else
              'SQ 14-12-2006: li teniamo tutti. Es.: PPP,LAST
              ' -> CONTROLLARE TUTTI I PUNTI CHE UTILIZZANO "rules"
              GbSegmenti(GbIndSeg).Rules = Mid(Left(wStr, Len(wStr) - 1), 2)
              'If InStr(wStr, "LAST") > 0 Then
              '   GbSegmenti(GbIndSeg).Rules = "LAST"
              ' Else
              '   If InStr(wStr, "FIRST") > 0 Then
              '     GbSegmenti(GbIndSeg).Rules = "FIRST"
              '   End If
              'End If
            End If
            wStr = TrovaParametro(wRec, "COMPRTN")
            GbSegmenti(GbIndSeg).CompRtn = wStr
            '''''''''''''''''''''''''''
            ' GESTIONE LOGICI
            ' SQ 15-12-06
            '''''''''''''''''''''''''''
            If DBDType = "LOG" Then
              'SOURCE
              wStr = findParameter(wRec, "SOURCE")
              'parentesi obbligatorie?
              If Len(wStr) Then
                'ES: SOURCE=((S099,,P013),(S078,,P013))
                If Left(wStr, 1) = "(" Then
                  'SQ 14-12-06 - Gestione: SOURCE=((S138),(S360,P,P038))
                  wStr = Mid(Left(wStr, Len(wStr) - 1), 2) 'Eliminazione parentesi
                End If
                For i = 0 To 1
                  token = nexttoken(wStr)
                  If Left(token, 1) = "(" Then
                    'SQ 14-12-06 - Gestione: PARENT=((S138),(S360,P,P038))
                    token = Mid(Left(token, Len(token) - 1), 2) 'Eliminazione parentesi
                  End If
                  sourceFields = Split(token, ",")
                  If UBound(sourceFields) = 2 Then
                    segmSource.Segment = sourceFields(0)
                    segmSource.Type = sourceFields(1)
                    segmSource.DBD = sourceFields(2)
                    GbSegmenti(GbIndSeg).source(i) = segmSource
                  Else
                    'TMP: Possibile?!
                    m_Fun.writeLog "#!! parseDBD " & GbIdOggetto & ": SOURCE=" & token
                  End If
                  If Left(wStr, 1) = "," Then
                    wStr = Trim(Mid(wStr, 2))
                  Else
                    'SOLO UN SOURCE!
                    Exit For
                  End If
                Next i
              End If
            End If
          Case "FIELD"
            wStr = TrovaParametro(wRec, "NAME")
            GbIndField = GbIndField + 1
            ReDim Preserve GbSegmenti(GbIndSeg).FIELD(GbIndField)
            If Left(wStr, 1) = "(" Then
              'SILVIA 5-3-2008
              'GbSegmenti(GbIndSeg).FIELD(GbIndField).Seq = False
              'GbSegmenti(GbIndSeg).FIELD(GbIndField).Primario = True
              If InStr(wStr, ",SEQ") > 0 Then
                GbSegmenti(GbIndSeg).FIELD(GbIndField).Seq = True
              End If
              ' Mauro 09/09/2010 : UNIQUE � il default, con o senza opzione "U"
              'If InStr(wStr, ",U") > 0 Then
                GbSegmenti(GbIndSeg).FIELD(GbIndField).Unique = True
              'End If
              If InStr(wStr, ",M") > 0 Then
                GbSegmenti(GbIndSeg).FIELD(GbIndField).Unique = False
              End If
              i = InStr(wStr, ",")
              If i > 0 Then wStr = Mid$(wStr, 1, i - 1)
              wStr = Mid$(wStr, 2)
              i = InStr(wStr, ")")
              If i > 0 Then wStr = Mid$(wStr, 1, i - 1)
              'GbSegmenti(GbIndSeg).FIELD(GbIndField).nome = wStr
            'Else
              'GbSegmenti(GbIndSeg).FIELD(GbIndField).nome = wStr
            End If
            GbSegmenti(GbIndSeg).FIELD(GbIndField).nome = wStr

            GbSegmenti(GbIndSeg).FIELD(GbIndField).Tipo = "C"
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Posizione = Val(TrovaParametro(wRec, "START"))
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Lunghezza = Val(TrovaParametro(wRec, "BYTES"))
            'cambiati in stringa vuota... verificare...
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_DbdNome = ""
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Index = False
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_NullVal = ""
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Pointer = ""
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Punseq = ""
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Search = ""
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_DData = ""
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Segmento = ""
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_SubSeq = ""
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_xNome = ""
            'MAURO: 08-09-05
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Lchild = WLchld
            'SP 8/9 pulire...
            WLchld = ""
          'SQ-5:
          Case "LCHILD", "PFXCHILD" 'IRIS-DB syntax for DEDB
            wIndex = TrovaParametro(wRec, "PTR")  'wIndex: chi lo usa?
            'SQ: puo' essere POINTER!!!!!!!!!!!!!!!!!!!
            If UCase(wIndex) = "NONE" Then
              wIndex = TrovaParametro(wRec, "POINTER")
            End If
            NameLChld = TrovaParametro(wRec, "NAME")
            i = InStr(NameLChld, ",")
            If i Then
              '(ciccio,ciaccio)
              WLchld = Trim(Replace(Mid(NameLChld, i + 1), ")", ""))
              WSegChld = Trim(Replace(Mid(NameLChld, 1, i - 1), "(", ""))
            Else
              'ciaccio
              'WLchld = TrovaParametro(wRec, "NAME")
              WLchld = NameLChld
              WSegChld = ""
            End If
            WPair = TrovaParametro(wRec, "PAIR")
            ' Mauro 02-04-2007 Inserimento in PSDLI_LChild
            Set rsLChld = m_Fun.Open_Recordset("Select * from PSDLI_LChild where " & _
                                               "dbdName = '" & nomeDBD & "' and " & _
                                               "NomeSegmento = '" & GbSegmenti(GbIndSeg).nome & "' and " & _
                                               "lc = '" & WSegChld & "' and " & _
                                               "lc_dbd = '" & WLchld & "'")
            If rsLChld.EOF Then
              rsLChld.AddNew
              rsLChld!dbdname = nomeDBD
              rsLChld!NomeSegmento = GbSegmenti(GbIndSeg).nome
              rsLChld!lc = WSegChld
              rsLChld!lc_dbd = WLchld
            End If
            rsLChld!ptr = IIf(UCase(wIndex) = "NONE", "", wIndex)
            rsLChld!pair = IIf(UCase(WPair) = "NONE", "", WPair)
            rsLChld.Update
            rsLChld.Close
            
          Case "XDFLD", "PFXXDFLD"
            GbIndField = GbIndField + 1
            ReDim Preserve GbSegmenti(GbIndSeg).FIELD(GbIndField)
            GbSegmenti(GbIndSeg).FIELD(GbIndField).nome = TrovaParametro(wRec, "NAME")
            'SEGMENT:
            wSegmento = TrovaParametro(wRec, "SEGMENT")
            If Len(wSegmento) = 0 Or UCase(wSegmento) = "NONE" Then wSegmento = GbSegmenti(GbIndSeg).nome
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Segmento = wSegmento
            'SRCH:
            wfield = TrovaParametro(wRec, "SRCH")
            'elimino eventuali parentesi
            If Left$(wfield, 1) = "(" Then
              wfield = Mid$(wfield, 2, Len(wfield) - 2)
            End If
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Search = Replace(wfield, " ", "")
            'DDATA
             wfield = TrovaParametro(wRec, "DDATA")
            'elimino eventuali parentesi
            If Left$(wfield, 1) = "(" Then
              wfield = Mid$(wfield, 2, Len(wfield) - 2)
            End If
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_DData = Replace(wfield, " ", "")
            'SUBSEQ:
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_SubSeq = TrovaParametro(wRec, "SUBSEQ")
            'SQ: 03-05-05
            'EXTRTN
            GbSegmenti(GbIndSeg).FIELD(GbIndField).XD_Extrtn = TrovaParametro(wRec, "EXTRTN")
            'MAURO: 08-09-05
            'LCHILD
            GbSegmenti(GbIndSeg).FIELD(GbIndField).Lchild = WLchld
            'SP 8/9 pulire...
            WLchld = ""
        End Select
      End If
    End If
  Wend
  Close FD
    
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' AGGIORNAMENTO TABELLE:
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'SQ 15-12-06
  'Aggiungiunta elemento alla PsDBD!
  Set rs = m_Fun.Open_Recordset("select * from PsDli_DBD where dbd_name = '" & GbNomePgm & "'")
  If rs.EOF Then
    rs.AddNew
    rs!dbd_name = GbNomePgm
    rs.Update
  End If
  rs.Close
  
  Set rs = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  rs!DtParsing = Now
  rs!parsinglevel = 1
  rs!Tipo_DBD = Left(DBDType, 3)
  rs.Update
  rs.Close
  
  Set TbField = m_Fun.Open_Recordset("Select * from PsDli_Field")
  Set TbXDField = m_Fun.Open_Recordset("Select * from PsDli_XDField")
  Set TbRelDli = m_Fun.Open_Recordset("Select * from PsRel_DliSeg")
  Set rsSegmenti = m_Fun.Open_Recordset("Select Max(IdSegmento) as maxId from PsDli_Segmenti")
  If Not rsSegmenti.EOF Then
    maxId = "0" & rsSegmenti!maxId
  End If
  rsSegmenti.Close
  Set rsSegmenti = m_Fun.Open_Recordset("Select * from PsDli_Segmenti")
  For GbIndSeg = 1 To UBound(GbSegmenti)
    rsSegmenti.AddNew
    rsSegmenti!idOggetto = GbIdOggetto
    rsSegmenti!IdSegmentoOrigine = 0
    rsSegmenti!nome = GbSegmenti(GbIndSeg).nome
    rsSegmenti!Parent = GbSegmenti(GbIndSeg).Parent
    rsSegmenti!LParent = GbSegmenti(GbIndSeg).LParent.Segment
    rsSegmenti!Ltype = GbSegmenti(GbIndSeg).LParent.Type
    rsSegmenti!LDBD = GbSegmenti(GbIndSeg).LParent.DBD
    rsSegmenti!MaxLen = GbSegmenti(GbIndSeg).MaxLen
    rsSegmenti!MinLen = GbSegmenti(GbIndSeg).MinLen
    rsSegmenti!Pointer = GbSegmenti(GbIndSeg).Pointer
    rsSegmenti!Rules = GbSegmenti(GbIndSeg).Rules
    rsSegmenti!CompRtn = GbSegmenti(GbIndSeg).CompRtn
    If rsSegmenti!idSegmento = 0 Then 'SQ gestione TMP per cambio tipo di dato: da contatore a long
      rsSegmenti!idSegmento = maxId + GbIndSeg
    End If
    rsSegmenti.Update
    'ottengo adesso il contatore automatico
    GbSegmenti(GbIndSeg).idSegmento = rsSegmenti!idSegmento
    TbRelDli.AddNew
    TbRelDli!idOggetto = GbIdOggetto
    TbRelDli!Father = GbSegmenti(GbIndSeg).Parent
    TbRelDli!Child = GbSegmenti(GbIndSeg).nome
    TbRelDli!relazione = True
    TbRelDli.Update
    'SQ 15-12-06 - Segmenti logici
    If DBDType = "LOG" Then
      Set rs = m_Fun.Open_Recordset("select * from PsDLI_Segmenti_SOURCE")
      For i = 0 To 1
        If Len(GbSegmenti(GbIndSeg).source(i).Segment) Then
          rs.AddNew
          rs!idSegmento = GbSegmenti(GbIndSeg).idSegmento
          rs!Ordinale = i + 1
          rs!Segmento = GbSegmenti(GbIndSeg).source(i).Segment
          rs!Tipo = GbSegmenti(GbIndSeg).source(i).Type
          rs!DBD = GbSegmenti(GbIndSeg).source(i).DBD
          rs.Update
        End If
      Next
      rs.Close
      '''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Eredito i FIELD "fisici"
      ' (solo del primo source)
      ' (per ora � significativo l'ordine del parsing...
      '''''''''''''''''''''''''''''''''''''''''''''''''''
      Set rs = m_Fun.Open_Recordset("select a.* from PsDli_Field as a,Bs_Oggetti as b,PsDli_Segmenti as c where " & _
                                    "b.nome='" & GbSegmenti(GbIndSeg).source(0).DBD & "' AND b.tipo='DBD' AND " & _
                                    "c.nome='" & GbSegmenti(GbIndSeg).source(0).Segment & "' AND b.idOggetto=c.idOggetto AND " & _
                                    "a.idSegmento=c.idSegmento")
      If rs.RecordCount Then
        While Not rs.EOF
          TbField.AddNew  'aperto sopra, fuori ciclo
          For i = 1 To rs.Fields.count - 1
            If Not IsNull(rs.Fields.item(i)) Then
              TbField.Fields.item(i) = rs.Fields.item(i)
            End If
          Next
          TbField!idSegmento = GbSegmenti(GbIndSeg).idSegmento
          TbField.Update
          rs.MoveNext
        Wend
      Else
        'Messaggio informativo sulla Bs_Messaggi!!!
      End If
      rs.Close
            '''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Eredito i FIELD "fisici"
      ' (solo del primo source)
      ' (per ora � significativo l'ordine del parsing...
      '''''''''''''''''''''''''''''''''''''''''''''''''''
      Set rs = m_Fun.Open_Recordset("select a.* from PsDli_XDField as a,Bs_Oggetti as b,PsDli_Segmenti as c where " & _
                                    "b.nome='" & GbSegmenti(GbIndSeg).source(0).DBD & "' AND b.tipo='DBD' AND " & _
                                    "c.nome='" & GbSegmenti(GbIndSeg).source(0).Segment & "' AND b.idOggetto=c.idOggetto " & _
                                    "AND a.idSegmento=c.idSegmento")
      If rs.RecordCount Then
        While Not rs.EOF
          TbXDField.AddNew  'aperto sopra, fuori ciclo
          For i = 1 To rs.Fields.count - 1
            If Not IsNull(rs.Fields.item(i)) Then
              TbXDField.Fields.item(i) = rs.Fields.item(i)
            End If
          Next
          TbXDField!IdXDField = getId("IdXDField", "PsDLI_XDField")
          TbXDField!idSegmento = GbSegmenti(GbIndSeg).idSegmento
          TbXDField.Update
          rs.MoveNext
        Wend
      Else
        'Messaggio informativo sulla Bs_Messaggi!!!
      End If
      rs.Close
    End If
     
    For GbIndField = 1 To UBound(GbSegmenti(GbIndSeg).FIELD)
      If Len(GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Search) = 0 Then
        'FIELD:
        TbField.AddNew
        TbField!idSegmento = GbSegmenti(GbIndSeg).idSegmento
        TbField!nome = GbSegmenti(GbIndSeg).FIELD(GbIndField).nome
        TbField!Tipo = GbSegmenti(GbIndSeg).FIELD(GbIndField).Tipo
        TbField!Posizione = GbSegmenti(GbIndSeg).FIELD(GbIndField).Posizione
        TbField!Lunghezza = GbSegmenti(GbIndSeg).FIELD(GbIndField).Lunghezza
        TbField!Seq = GbSegmenti(GbIndSeg).FIELD(GbIndField).Seq
        TbField!Unique = GbSegmenti(GbIndSeg).FIELD(GbIndField).Unique
        TbField!Primario = GbSegmenti(GbIndSeg).FIELD(GbIndField).Primario
        TbField!Ptr_Punseq = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Punseq
        TbField!Ptr_DbdNome = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_DbdNome
        TbField!Ptr_Pointer = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Pointer
        TbField!Ptr_xNome = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_xNome
        TbField!Ptr_Segmento = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Segmento
        TbField!Ptr_Search = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Search
        TbField!Ptr_NullVal = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_NullVal
        TbField!Ptr_Index = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Index
        TbField.Update
      Else
        'XDFLD:
        'ATTENZIONE: STIAMO CONSIDERANDO SOLO CASI INTRA-SEGMENTO...
        TbXDField.AddNew
        TbXDField!IdXDField = getId("IdXDField", "PsDLI_XDField") 'ritorna -1 in caso d'errore
        TbXDField!idSegmento = GbSegmenti(GbIndSeg).idSegmento  'ATTENZIONE: SOLO SRCH INTRA-SEGMENTO!!!!!
        TbXDField!nome = GbSegmenti(GbIndSeg).FIELD(GbIndField).nome
        TbXDField!srchFields = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Search
        TbXDField!DDataFields = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_DData
        TbXDField!extRtn = GbSegmenti(GbIndSeg).FIELD(GbIndField).XD_Extrtn
        TbXDField!Lchild = GbSegmenti(GbIndSeg).FIELD(GbIndField).Lchild
        TbXDField!SubSeq = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_SubSeq
        ' Mauro 07/04/2008
        On Error Resume Next
        TbXDField!Segmento = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Segmento
        On Error GoTo 0
        TbXDField.Update
      End If
    Next GbIndField
  Next GbIndSeg
  TbXDField.Close
  rsSegmenti.Close
  TbField.Close
  TbRelDli.Close
  
  'BELLO!!!!!!!!! RIFA TUTTO IL PARSING!!!!!!!!!!!
  'STO ROBO FARLO DIRETTAMENTE SOPRA!!!!!!!!!!!!!!
  If DBDType = "GSA" Then
    ParserDBD_GISAM
  End If
  Exit Sub
catch:
'  If Err.Number = 123 Then
'  else
'  endif
  GbOldNumRec = GbNumRec
  'If wRec <> "" Then
    addSegnalazione wRec, "P00", wRec
  'Else
    'addSegnalazione path, "P00", path
  'End If
End Sub

Public Sub ParserDBD_GISAM()
   Dim nFDbd As Variant
   Dim wRecIn As String
   Dim wRec As String
   Dim TipoDbd As String
   Dim nomeDBD As String
   Dim wStr As String
   Dim firstWord As String
   Dim k As Integer
   
   'Dati del dbd gisam
   Dim wRec1 As String, wRec2 As String
   Dim wRecFm As e_Recfm_gisam
   Dim wDD1 As String
   Dim wApprecFm As String
   
   Dim wSegmento As String, wSegmOrd As String, wfield As String, wIndex As String
   
   Dim TbSegmenti As Recordset, TbField As Recordset, TbXDField As Recordset
   Dim TbRelDli As Recordset, TbOggetti As Recordset
   Dim wSqlExec As String
   
   ReDim GbSegmenti(0)
   GbIndSeg = 0
   
   nFDbd = FreeFile
   Open GbFileInput For Input As nFDbd
   While Not EOF(nFDbd)
      wRec = ""
      Line Input #nFDbd, wRecIn
      GbNumRec = GbNumRec + 1
      If Not EOF(nFDbd) Then
        While Mid(wRecIn & Space$(72), 72, 1) <> " " And Not EOF(nFDbd)
           wRec = wRec & Trim(Mid$(wRecIn, 1, 71))
           Line Input #nFDbd, wRecIn
           GbNumRec = GbNumRec + 1
        Wend
        wRec = wRec & LTrim(Mid$(wRecIn & Space$(72), 1, 71))
      
        If Left(wRec, 1) <> "*" Then
           firstWord = wRec
           k = InStr(firstWord, " ")
           firstWord = Trim(Mid$(firstWord, 1, k))
           Select Case firstWord
              Case "DBD"
                nomeDBD = TrovaParametro(wRec, "NAME")
                TipoDbd = "GSAM"
            
              Case "DATASET"
                If InStr(1, wRec, "DD1=") > 0 Then
                  wDD1 = TrovaParametro(wRec, "DD1")
                End If
                
                If InStr(1, wRec, "RECORD=") > 0 Then
                  wRec1 = TrovaParametro(wRec, "RECORD")
                  
                  'GS:Elimina rec2 se presente : Non serve per ora
                End If
                
                If InStr(1, wRec, "RECFM=") > 0 Then
                  wApprecFm = TrovaParametro(wRec, "RECFM")
                  
                  If wApprecFm = "F" Then wRecFm = REC_FIXED_LENGTH
                  If wApprecFm = "FB" Then wRecFm = REC_FIXED_LENGTH_AND_BLOCKED
                  If wApprecFm = "V" Then wRecFm = REC_VARIABILE_LENGTH
                  If wApprecFm = "VB" Then wRecFm = REC_VARIABILE_LENGTH_AND_BLOCKED
                  If wApprecFm = "U" Then wRecFm = REC_UNDEFINED_LENGTH
                End If
             
           End Select
        End If
      End If
   Wend

   Close nFDbd
   
   'GS: crea un segmento Virtuale per permettere comunque l'associazione delle aree
   ReDim Preserve GbSegmenti(1)
   
   GbSegmenti(1).nome = "GS_" & nomeDBD 'Dovr� essere variabile
   
   Set TbSegmenti = m_Fun.Open_Recordset("Select MAX (IdSegmento) from PsDli_Segmenti")
   
   GbSegmenti(1).idSegmento = TbSegmenti.Fields(0).value + 1
   GbSegmenti(1).CompRtn = "None"
   GbSegmenti(1).Parent = 0
   GbSegmenti(1).Pointer = "None"
   GbSegmenti(1).Rules = "None"
    'SQ: Nessuna lunghezza specificata...
    
   GbSegmenti(1).MaxLen = IIf(Len(wRec1), wRec1, -1)
   
   ReDim Preserve GbSegmenti(1).FIELD(0)
   ReDim Preserve GbSegmenti(1).FIELD(1)
   GbSegmenti(1).FIELD(1).nome = "GS_FLD_" & nomeDBD
   GbSegmenti(1).FIELD(1).Ptr_DbdNome = nomeDBD
   GbSegmenti(1).FIELD(1).Tipo = "C"
    'SQ: wRec1 contiene tutte le lunghezze del mondo?
   GbSegmenti(1).FIELD(1).Lunghezza = IIf(Len(wRec1), wRec1, -1)
   GbSegmenti(1).FIELD(1).Posizione = 1
   GbSegmenti(1).FIELD(1).Primario = True
   GbSegmenti(1).FIELD(1).Unique = True
   
   'GS : Aggiunge field,relazioni e Segmento
   Set TbField = m_Fun.Open_Recordset("Select * from PsDli_Field")
   Set TbRelDli = m_Fun.Open_Recordset("Select * from PsRel_DliSeg")
   Set TbSegmenti = m_Fun.Open_Recordset("Select * from PsDli_Segmenti ")
   
   For GbIndSeg = 1 To UBound(GbSegmenti)
      TbSegmenti.AddNew
      ''''''''''''''''''''
      TbSegmenti!idSegmento = GbSegmenti(GbIndSeg).idSegmento
      ''''''''''''''''''''
      TbSegmenti!idOggetto = GbIdOggetto
      TbSegmenti!IdSegmentoOrigine = 0
      TbSegmenti!nome = GbSegmenti(GbIndSeg).nome
      TbSegmenti!Parent = GbSegmenti(GbIndSeg).Parent
      TbSegmenti!MaxLen = GbSegmenti(GbIndSeg).MaxLen
      TbSegmenti!MinLen = GbSegmenti(GbIndSeg).MinLen
      TbSegmenti!Pointer = GbSegmenti(GbIndSeg).Pointer
      TbSegmenti!Rules = GbSegmenti(GbIndSeg).Rules
      TbSegmenti!CompRtn = GbSegmenti(GbIndSeg).CompRtn
      TbSegmenti.Update
      
      GbSegmenti(GbIndSeg).idSegmento = TbSegmenti!idSegmento
      TbRelDli.AddNew
      TbRelDli!idOggetto = GbIdOggetto
      TbRelDli!Father = GbSegmenti(GbIndSeg).Parent
      TbRelDli!Child = GbSegmenti(GbIndSeg).nome
      TbRelDli!relazione = True
      TbRelDli.Update

      For GbIndField = 1 To UBound(GbSegmenti(GbIndSeg).FIELD)
        If Len(GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Search) = 0 Then
         'FIELD:
         TbField.AddNew
         TbField!idSegmento = GbSegmenti(GbIndSeg).idSegmento
         TbField!nome = GbSegmenti(GbIndSeg).FIELD(GbIndField).nome
         TbField!Tipo = GbSegmenti(GbIndSeg).FIELD(GbIndField).Tipo
         TbField!Posizione = GbSegmenti(GbIndSeg).FIELD(GbIndField).Posizione
         TbField!Lunghezza = GbSegmenti(GbIndSeg).FIELD(GbIndField).Lunghezza
         TbField!Seq = GbSegmenti(GbIndSeg).FIELD(GbIndField).Seq
         TbField!Unique = GbSegmenti(GbIndSeg).FIELD(GbIndField).Unique
         TbField!Primario = GbSegmenti(GbIndSeg).FIELD(GbIndField).Primario
         TbField!Ptr_Punseq = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Punseq
         TbField!Ptr_DbdNome = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_DbdNome
         TbField!Ptr_Pointer = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Pointer
         TbField!Ptr_xNome = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_xNome
         TbField!Ptr_Segmento = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Segmento
         TbField!Ptr_Search = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Search
         TbField!Ptr_SubSeq = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_SubSeq
         TbField!Ptr_NullVal = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_NullVal
         TbField!Ptr_Index = GbSegmenti(GbIndSeg).FIELD(GbIndField).Ptr_Index
         TbField.Update
        End If
      Next GbIndField
   Next GbIndSeg
   
   TbSegmenti.Close
   TbField.Close
  
   TbRelDli.Close
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' E' un "cercaMOVE":
' - riapre il file (usa rText) e cerca nelle MOVE...
' !! Prende solo il primo: PERCHE???????????????
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Function TrovaPsb(NomePCB) As String
  Dim vStart As Variant, vPos As Variant, vEnd As Variant, vLen As Variant
  Dim k As Integer
  
  Dim wStrDef As String, wStrMove As String, wStrRel As String, wStrDefRel As String

  If Len(MapsdF_Parser.rText.Text) = 0 Then
    MapsdF_Parser.rText.LoadFile GbFileInput
  End If
  
  vEnd = Len(MapsdF_Parser.rText.Text)
  'vStart = 0
  'TrovaPsb = ""
  vPos = MapsdF_Parser.rText.Find(NomePCB, vStart, vEnd)
  While vPos > 0
   wStrDef = Mid$(MapsdF_Parser.rText.Text, vPos - 30, 100)
   k = InStr(wStrDef, vbCrLf)
   If k > 0 And k < 20 Then
      wStrDef = Mid$(wStrDef, k + 2)
   End If
   k = InStr(wStrDef, vbCrLf)
   If k > 0 And k > 25 Then
      wStrDef = Mid$(wStrDef, 1, k - 2)
   End If
   If InStr(wStrDef, " MOVE ") > 0 Then
     k = InStr(wStrDef, " MOVE ")
     wStrDef = Trim(Mid$(wStrDef, k + 6))
     k = InStr(wStrDef, " ")
     wStrDef = Trim(Mid$(wStrDef, 1, k - 1))
     If InStr(wStrDef, "'") = 0 Then
      TrovaPsb = PrendiValue(wStrDef)
     Else
      TrovaPsb = Replace(wStrDef, "'", "")
     End If
     If TrovaPsb <> "SPACE" And TrovaPsb <> "SPACES" And UCase(TrovaPsb) <> "NONE" Then
         Exit Function
     End If
   End If
   
   vStart = vPos + 1
   vPos = MapsdF_Parser.rText.Find(NomePCB, vStart, vEnd)
  Wend
End Function
Sub parseCHKP(parametri As String)
  Dim i As Integer
  Dim wDliRec As String
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  TabIstr.istr = "CHKP"
  TabIstr.psbId = 0
  TabIstr.psbName = " "
  'stefano: pezza per colpa di qualcun altro
  TabIstr.DBD_TYPE = DBD_UNKNOWN_TYPE
  TabIstr.DBD = ""

  'If TabIstr.NumPcb = 1 Then
    '''''''''''''''''''
    'PCB-IO: IMS-DC
    '''''''''''''''''''
    IsDC = True
    'MsgBox "tmp: PCB-IO: IMS-DC!"
    'tmp:
    'parseRECEIVE
    'Exit Sub
  'Else
    '
  'End If
  Dim tb As Recordset
  'stefano: forzato, altrimenti � casuale, quello dell'istruzione precedente
  IsValid = True
  
  UpdatePsdliIstruzioni (IsDC)
  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  
  While Len(parametri)
    wDliRec = nexttoken(parametri)
    If Len(wDliRec) Then
      i = i + 1
      tb.AddNew
      'SQ COPY-ISTR
      'tb!idOggetto = GbIdOggetto
      tb!idOggetto = IdOggettoRel
      tb!idpgm = GbIdOggetto

      tb!Riga = GbOldNumRec
      tb!livello = i
      tb!NomeSSA = ""
      tb!dataarea = wDliRec
      tb.Update
    End If
  Wend
  tb.Close
End Sub
Sub parseXRST(parametri As String)
  Dim wDliRec As String
  Dim i As Integer
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  TabIstr.istr = "XRST"
  TabIstr.psbId = 0
  TabIstr.psbName = " "
  'stefano: pezza per colpa di qualcun altro
  TabIstr.DBD_TYPE = DBD_UNKNOWN_TYPE
  TabIstr.DBD = ""
  'If TabIstr.NumPcb = 1 Then
    '''''''''''''''''''
    'PCB-IO: IMS-DC
    '''''''''''''''''''
    IsDC = True
    'MsgBox "tmp: PCB-IO: IMS-DC!"
    'tmp:
    'parseRECEIVE
    'Exit Sub
  'Else
    '
  'End If
  Dim tb As Recordset
  'stefano: forzato, altrimenti � casuale, quello dell'istruzione precedente
  IsValid = True
  UpdatePsdliIstruzioni (IsDC)

  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  While Len(parametri)
    wDliRec = nexttoken(parametri)
    If Len(wDliRec) Then
      i = i + 1
      tb.AddNew
      'SQ COPY-ISTR
      'tb!idOggetto = GbIdOggetto
      tb!idOggetto = IdOggettoRel
      tb!idpgm = GbIdOggetto
      tb!Riga = GbOldNumRec
      tb!livello = i
      tb!NomeSSA = ""
      tb!dataarea = wDliRec
      tb.Update
    End If
  Wend
  tb.Close
End Sub
'IRIS-DB Sub parseICMD(parametri As String)
Sub parseCMD(istruzione As String, parametri As String)
  Dim wDliRec As String
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  wDliRec = nexttoken(parametri)
'IRIS-DB  TabIstr.istr = "ICMD"
  TabIstr.istr = istruzione 'IRIS-DB
  TabIstr.psbId = 0
  TabIstr.psbName = " "
    
   'If TabIstr.NumPcb = 1 Then
    '''''''''''''''''''
    'PCB-IO: IMS-DC
    '''''''''''''''''''
    IsDC = True
    'MsgBox "tmp: PCB-IO: IMS-DC!"
    'tmp:
    'parseRECEIVE
    'Exit Sub
  'Else
    '
  'End If
  
  Dim tb As Recordset
  
  Set tb = m_Fun.Open_Recordset("select * from PsDli_Istruzioni where IdOggetto = " & GbIdOggetto & " and riga = " & GbOldNumRec)
  If Not tb.EOF Then
    tb!numpcb = TabIstr.pcb
    tb!Imsdc = IsDC
    tb!Imsdb = Not (IsDC)
    tb!valida = True
    tb.Update
  End If
  tb.Close
  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
    
  If Len(wDliRec) Then
    tb.AddNew
    'SQ COPY-ISTR
    'tb!idOggetto = GbIdOggetto
    tb!idOggetto = IdOggettoRel
    tb!idpgm = GbIdOggetto
    tb!Riga = GbOldNumRec
    tb!livello = 1
    tb!NomeSSA = ""
    tb!dataarea = wDliRec
    tb.Update
  End If
  tb.Close
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Da dettagliare...
' - Anche per: "GCMD", "RCMD", "GSCD", "GMSG"
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parsePURG(istruzione As String, parametri As String)
  Dim wDliRec As String
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  wDliRec = nexttoken(parametri)
  TabIstr.istr = istruzione
  TabIstr.psbId = 0
  TabIstr.psbName = " "
  
  IsDC = True
  
  Dim tb As Recordset
  IsValid = True
  UpdatePsdliIstruzioni (IsDC)
  
  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  If Len(wDliRec) Then
    tb.AddNew
    'SQ COPY-ISTR
    'tb!idOggetto = GbIdOggetto
    tb!idOggetto = IdOggettoRel
    tb!idpgm = GbIdOggetto
    tb!Riga = GbOldNumRec
    tb!livello = 3
    tb!NomeSSA = ""
    tb!dataarea = wDliRec
    tb.Update
  Else
    'segnalazione istruzione "sbagliata"
  End If
  tb.Close
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Da dettagliare...
' - SETU,SETO,SETS,SETB
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseSETx(istruzione As String, parametri As String)
  Dim wDliRec As String
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  wDliRec = nexttoken(parametri)
  TabIstr.istr = istruzione
  TabIstr.psbId = 0
  TabIstr.psbName = " "
  
  IsDC = True
  
  Dim tb As Recordset
  
    UpdatePsdliIstruzioni (IsDC)
  
  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  If Len(wDliRec) Then
    tb.AddNew
    'SQ COPY-ISTR
    'tb!idOggetto = GbIdOggetto
    tb!idOggetto = IdOggettoRel
    tb!idpgm = GbIdOggetto
    tb!Riga = GbOldNumRec
    tb!livello = 3
    tb!NomeSSA = ""
    tb!dataarea = wDliRec
    tb.Update
  Else
    'segnalazione istruzione "sbagliata"
  End If
  tb.Close
End Sub

Sub parseAUTH(parametri As String)
  Dim wDliRec As String
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  wDliRec = nexttoken(parametri)
  TabIstr.istr = "AUTH"
  TabIstr.psbId = 0
  TabIstr.psbName = " "
  
  'If TabIstr.NumPcb = 1 Then
    '''''''''''''''''''
    'PCB-IO: IMS-DC
    '''''''''''''''''''
    IsDC = True
    'MsgBox "tmp: PCB-IO: IMS-DC!"
    'tmp:
    'parseRECEIVE
    'Exit Sub
  'Else
    '
  'End If
  
  Dim tb As Recordset
  
  UpdatePsdliIstruzioni (IsDC)
  
  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  If Len(wDliRec) Then
    tb.AddNew
    'SQ COPY-ISTR
    'tb!idOggetto = GbIdOggetto
    tb!idOggetto = IdOggettoRel
    tb!idpgm = GbIdOggetto
    tb!Riga = GbOldNumRec
    tb!livello = 3
    tb!NomeSSA = ""
    tb!dataarea = wDliRec
    tb.Update
  Else
    'segnalazione istruzione "sbagliata"
  End If
  tb.Close
End Sub
Sub parseCHNG(parametri As String)
  Dim wDliRec As String
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)
  Dim restOfCall As String 'IRIS-DB
  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  wDliRec = nexttoken(parametri)
  TabIstr.numpcb = getPCBnumber(TabIstr.pcb) 'IRIS-DB da mettere in tutte le istruzioni, altrimenti ci rimane quello dell'istruzione DB precedente...
  
  IsValid = True 'IRIS-DB
 
 'IRIS-DB gestione della parentesi - start
  restOfCall = nexttoken(parametri)
  If InStr(restOfCall, "(") Then
    wDliRec = wDliRec & restOfCall
  End If
 'IRIS-DB - end
  
  'IRIS-DB: Area is mandatory in CHNG command
  If Len(wDliRec) = 0 Then
    IsValid = False
    addSegnalazione TabIstr.istr, "NODECOD", TabIstr.istr
  End If
  TabIstr.istr = "CHNG"
  TabIstr.psbId = 0
  TabIstr.psbName = " "

  'If TabIstr.NumPcb = 1 Then
    '''''''''''''''''''
    'PCB-IO: IMS-DC
    '''''''''''''''''''
    IsDC = True
    'MsgBox "tmp: PCB-IO: IMS-DC!"
    'tmp:
    'parseRECEIVE
    'Exit Sub
  'Else
    '
  'End If
  
  Dim tb As Recordset
  'IRIS-DB IsValid = True
  UpdatePsdliIstruzioni (IsDC)
  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  If Len(wDliRec) Then
    tb.AddNew
    'SQ COPY-ISTR
    'tb!idOggetto = GbIdOggetto
    tb!idOggetto = IdOggettoRel
    tb!idpgm = GbIdOggetto
    tb!Riga = GbOldNumRec
    tb!livello = 1
    tb!NomeSSA = ""
    tb!dataarea = wDliRec
    tb.Update
  End If
  tb.Close
End Sub
Sub parseINQY(parametri As String)
  Dim wDliRec As String
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  wDliRec = nexttoken(parametri)
  TabIstr.istr = "INQY"
  TabIstr.psbId = 0
  TabIstr.psbName = " "

  'If TabIstr.NumPcb = 1 Then
    '''''''''''''''''''
    'PCB-IO: IMS-DC
    '''''''''''''''''''
    IsDC = True
    'MsgBox "tmp: PCB-IO: IMS-DC!"
    'tmp:
    'parseRECEIVE
    'Exit Sub
  'Else
    '
  'End If
  
  Dim tb As Recordset
  UpdatePsdliIstruzioni (IsDC)
  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  If Len(TabIstr.pcb) Then
    tb.AddNew
    'SQ COPY-ISTR
    'tb!idOggetto = GbIdOggetto
    tb!idOggetto = IdOggettoRel
    tb!idpgm = GbIdOggetto
    tb!Riga = GbOldNumRec
    tb!livello = 1
    tb!NomeSSA = ""
    tb!dataarea = TabIstr.pcb
    tb.Update
  End If
  
  If Len(wDliRec) Then
    tb.AddNew
    'SQ COPY-ISTR
    'tb!idOggetto = GbIdOggetto
    tb!idOggetto = IdOggettoRel
    tb!idpgm = GbIdOggetto
    tb!Riga = GbOldNumRec
    tb!livello = 2
    tb!NomeSSA = ""
    tb!dataarea = wDliRec
    tb.Update
  End If
  tb.Close
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Rifatta...
' Legge la tabella PsIMS_entry (valorizzata dal I livello);
' Se non c'e' la ENTRY cerca nei chiamanti;
' se non trova niente, prende la USING...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function getENTRY(idOggetto As Long) As String()
  Dim AppPcb() As String, entries() As String
  Dim rs As Recordset
  Dim rsCaller As Recordset
  Dim CallerOK As Boolean
  Dim sApp() As String
  Dim i As Long
   
  Set rs = m_Fun.Open_Recordset("select * from PsIMS_entry where IdOggetto = " & idOggetto & " order by numPCB")
  If rs.RecordCount = 0 Then
    ReDim entries(0)
    CallerOK = False
    'stefano: non c'entra nulla con il discorso del nome dei pcb: non pu� prendere quelli del
    ' chiamante, ma i nomi dei pcb vanno presi comunque nel chiamato, poi devono essere
    ' incrociati con quelli del chiamante, per associarli alla corretta posizione nel psb
    ' per ora asterisco, poi vediamo...
    ' SQ: grazie al pistello!...
'    Set rsCaller = m_Fun.Open_Recordset("select * from PsRel_Obj where  Utilizzo ='CALL' and IdOggettoR = " & idOggetto)
'    Do Until rsCaller.EOF Or CallerOK
'      entries = getEntry(rsCaller!IdOggettoC)
'      If UBound(entries) Then
'          CallerOK = True
'      End If
'      rsCaller.MoveNext
'    Loop
    '''''''''''''''''''''''''''''''''''
    ' SQ
    ' Prende le aree dalla linkage!
    '''''''''''''''''''''''''''''''''''
    'stefano: bisogna aggiungere l'esclusione delle aree che non sono pcb, come ad esempio
    ' i parametri aggiuntivi provenienti da JCL
    If Not CallerOK Then
      Set rs = m_Fun.Open_Recordset("select Parameters from PsPgm where IdOggetto = " & idOggetto)
      If rs.EOF Then
        'SQ 1-10-07
        'Gestione ENTRY in COPY (verificare se Utilizzo � 'INC' per il PLI...
        rs.Close
        Set rs = m_Fun.Open_Recordset( _
          "SELECT a.Parameters FROM PsPgm as a,PsRel_Obj as b WHERE Relazione='CPY' AND " & _
          " IdOggettoC = " & idOggetto & " AND a.idoggetto=b.idoggettoR")
        If rs.EOF Then
          'GESTIONE ENTRY in COPY dentro COPY (drogati: vedi ATOS!!!!!!!!!!)
          rs.Close
          Set rs = m_Fun.Open_Recordset("SELECT a.Parameters FROM PsPgm as a,PsRel_Obj as b ,PsRel_Obj as c WHERE b.Relazione='CPY' AND b.IdOggettoC=" & idOggetto & " AND b.idoggettoR=c.idoggettoC AND c.Relazione='CPY' AND a.idoggetto=c.idoggettoR")
        End If
      End If
      If Not rs.EOF Then
        entries = Split(rs!parameters & "", ",")
      End If
    End If
    rs.Close
  Else
    Do Until rs.EOF
      ReDim Preserve entries(i)
      entries(i) = rs!pcb
      rs.MoveNext
      i = i + 1
    Loop
  End If
  getENTRY = entries
End Function
'''''''''''''''''''''''''
'GU-GN/IMSDC
'''''''''''''''''''''''''
Public Sub parseRECEIVE(istruzione As String, ioArea As String)
  Dim rs As Recordset
  
  ''''''''''''''''''''''''''''''''
  ' Gestione PSB: quelli del PGM:
  ''''''''''''''''''''''''''''''''
  getInstructionPsbDbd (TabIstr.numpcb)
  
  'Validit�:
  IsValid = SwPsbAssociato
  If Len(ioArea) = 0 Then
    IsValid = False
    addSegnalazione TabIstr.istr, "NODECOD", TabIstr.istr
  End If
  
  UpdatePsdliIstruzioni True
  
  'UPDATE "PsDli_Istruzioni":
  ' AC tratto a parte i casi non gestiti
  Set rs = m_Fun.Open_Recordset("select * from PsDli_Istruzioni where idoggetto = " & GbIdOggetto & " and riga = " & GbOldNumRec)
  rs!ordpcb = 1
  rs!Maparea = ioArea
  'IRIS-DB molto brutto, ma va bene...
  rs!valida = Len(ioArea) > 0 'IRIS-DB
  rs.Update
  rs.Close
  
  'UPDATE "PsDli_IstrDett_liv":
  Set rs = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  rs.AddNew
  'SQ COPY-ISTR
  'rs!idOggetto = GbIdOggetto
  rs!idOggetto = IdOggettoRel
  rs!idpgm = GbIdOggetto
  rs!Riga = GbOldNumRec
  rs!livello = 1
  rs!dataarea = ioArea
  rs!valida = Len(ioArea) > 0
  rs.Update
  rs.Close
'      If Trim(tb!istruzione) = "ISRT" Then
'         wMapArea = kIstr(UBound(kIstr) - 1)
'         wMapName = kIstr(UBound(kIstr))
'      End If
'      If Trim(wMapName) <> "" Then
'         tb!MapName = wMapName
'         tb!MapSet = wMapName 'SG : Deve vedere se c'� un mapset
'      Else
'         tb!MapName = "[*MAP-NOT-FOUND*]"
'         tb!MapSet = ""
'      End If

End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 9-11-06
' Gestione MOVE: segnalazioni "multiple"
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub parseSEND(istruzione As String, parametri As String)
  Dim rs As Recordset
  Dim ioArea As String, mapName() As String, mapNameVar As String
  Dim i As Integer
  
  ReDim mapName(0)  'init
  
  ''''''''''''''''''''''''''''''''
  ' Gestione PSB: quelli del PGM:
  ''''''''''''''''''''''''''''''''
  getInstructionPsbDbd (TabIstr.numpcb)
  
  '''''''''''''''''''''''''
  ' IO-AREA
  '''''''''''''''''''''''''
  ioArea = nexttoken(parametri)
  
  'Validit�:
  IsValid = SwPsbAssociato
  If Len(ioArea) = 0 Then
    IsValid = False
    addSegnalazione TabIstr.istr, "NODECOD", TabIstr.istr
  End If
  
  '''''''''''''''''''''''''
  ' MODNAME: MAPPA
  '''''''''''''''''''''''''
  If Len(parametri) Then
    mapNameVar = nexttoken(parametri)  'variabile
    'IRIS-DB gestione della OF - start
    If mapNameVar = "OF" Then
      ioArea = ioArea & " OF " & nexttoken(parametri)
      If Len(parametri) Then
        mapNameVar = nexttoken(parametri)
      Else
        mapNameVar = ""
      End If
    End If
    'SQ
    'mapName = getFieldValue(mapNameVar)
    'VALUE + MOVE:
    If Len(mapNameVar) Then
    'IRIS-DB - end
      mapName = getFieldValues(mapNameVar)
      If UBound(mapName) Or Len(mapName(0)) Then
        For i = 1 To UBound(mapName)
          MapsdM_Parser.checkRelations mapName(i), "MFS"
          'IsValid = False
        Next
      Else
        MapsdM_Parser.checkRelations mapNameVar, "MFS"
        'IsValid = False
      End If
    End If
  End If
    
  'UPDATE "PsDli_Istruzioni":
  UpdatePsdliIstruzioni (True)
  'Ac tratto a parte i casi non gestiti
  Set rs = m_Fun.Open_Recordset("select * from PsDli_Istruzioni where idoggetto = " & GbIdOggetto & " and riga = " & GbOldNumRec)
  rs!ordpcb = 1
  rs!Maparea = ioArea
  rs!mapName = mapNameVar
  'rs!valida = Len(ioArea) > 0
  'IRIS-DB ripristinata precedente istruzione
  rs!valida = Len(ioArea) > 0 'IRIS-DB
  rs.Update
  rs.Close
  
  'UPDATE "PsDli_IstrDett_liv":
  Set rs = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  rs.AddNew
  'SQ COPY-ISTR
  'rs!idOggetto = GbIdOggetto
  rs!idOggetto = IdOggettoRel
  rs!idpgm = GbIdOggetto
  rs!Riga = GbOldNumRec
  rs!livello = 1
  rs!dataarea = ioArea
  rs!valida = Len(ioArea) > 0
  rs.Update
  rs.Close
'      If Trim(tb!istruzione) = "ISRT" Then
'         wMapArea = kIstr(UBound(kIstr) - 1)
'         wMapName = kIstr(UBound(kIstr))
'      End If
'      If Trim(wMapName) <> "" Then
'         tb!MapName = wMapName
'         tb!MapSet = wMapName 'SG : Deve vedere se c'� un mapset
'      Else
'         tb!MapName = "[*MAP-NOT-FOUND*]"
'         tb!MapSet = ""
'      End If

  ''''''''''''''''''''''''''''''''''''''''
  ' Segnalazioni
  ''''''''''''''''''''''''''''''''''''''''
  If Len(ioArea) = 0 Then
     addSegnalazione TabIstr.istr, "NODECOD", TabIstr.istr
  End If
  'PSB:?
  
End Sub
Function risolviIstruzioneByMove(pCampoIstruzione As String, ByRef pLastIndex As Integer, Start As Integer) As String
  Dim j As Integer, k As Integer
  Dim findCampo As Boolean
  Dim itemValue As String
  
  risolviIstruzioneByMove = ""
  pLastIndex = UBound(CampiMove)
  
  For j = Start To UBound(CampiMove)
    If findCampo Then Exit For
    If CampiMove(j).NomeCampo = pCampoIstruzione Then
      findCampo = True ' campo univoco, trovato una volta finite ricerche
      ' controllo lista valori - al primo buono esco
      For k = 1 To CampiMove(j).Valori.count
        'Mauro 07-03-2007
        ' Se il valore � una variabile, mi ricavo nuovamente il valore
        'If Left(CampiMove(j).Valori.item(k), 1) <> "'" Then
        '  itemValue = getFieldValue(CampiMove(j).Valori.item(k))
        'Else
        '  itemValue = Trim(Replace(CampiMove(j).Valori.item(k), "'", ""))
        'End If
        itemValue = getIstrValue(CampiMove(j).Valori.item(k))
        Select Case itemValue
          Case "PCB", "GU", "GHU", "GN", "GHN", "GNP", "GHNP", "DLET", "REPL", "ISRT", "TERM"
            risolviIstruzioneByMove = itemValue
            Exit For
        End Select
      Next k
      If risolviIstruzioneByMove <> itemValue Then
        ' controllo lista valori - al primo buono esco
        For k = 1 To CampiMove(j).Valori.count
          'Mauro 07-03-2007
          ' Se il valore � una variabile, mi ricavo nuovamente il valore
          'If Left(CampiMove(j).Valori.item(k), 1) <> "'" Then
          '  itemValue = getFieldValue(CampiMove(j).Valori.item(k))
          'Else
          '  itemValue = Trim(Replace(CampiMove(j).Valori.item(k), "'", ""))
          'End If
          itemValue = getIstrValue(CampiMove(j).Valori.item(k))
          Select Case itemValue 'IRIS-DB changed ICMD to CMD and added POS
            Case "PCB", "GU", "GHU", "GN", "GHN", "GNP", "GHNP", "DLET", "REPL", "ISRT", "POS", _
                 "TERM", "AUTH", "GCMD", "CMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG", _
                 "SETB", "SETO", "SETS", "SETU", "XRST", "SYNC", "CHKP", "CKPT", "APSB", _
                 "DPSB", "INIT", "INQY", "LOG", "ROLB", "ROLL", "ROLS", "OPEN", "CLSE"
              risolviIstruzioneByMove = itemValue
              Exit For
          End Select
        Next k
      End If
    End If
  Next j
  If risolviIstruzioneByMove = "" Then
    risolviIstruzioneByMove = risolviIstruzioneByMoveCopyprimo(pCampoIstruzione, GbIdOggetto)
  End If
End Function

Function risolviIstruzioneByMoveCopyprimo(pCampoIstruzione As String, idOggetto As Long) As String
  Dim rs As Recordset, i As Integer, j As Integer, k As Integer, findCampo As Boolean, itemValue As String
  Dim nome As String, Valore As String, deststart As String
  
  On Error GoTo err
  
  If UBound(CampiMoveCpy) = 0 Then
    Set rs = m_Fun.Open_Recordset("SELECT a.* FROM PsData_CmpMoved AS a, PsRel_Obj AS b WHERE " & _
                                  "b.idOggettoC= " & idOggetto & " And b.relazione = 'CPY' And a.IdPgm = b.IdOggettoR")
    While Not rs.EOF
      nome = rs!nome
      Valore = rs!Valore
      deststart = rs!deststart & ""
      For i = 1 To UBound(CampiMoveCpy)
        If CampiMoveCpy(i).NomeCampo = nome Then
          Exit For
        End If
      Next i
      If i = UBound(CampiMoveCpy) + 1 Then ' campo nuovo
        ReDim Preserve CampiMoveCpy(UBound(CampiMoveCpy) + 1)
        CampiMoveCpy(UBound(CampiMoveCpy)).NomeCampo = Replace(nome, ",", "")
        Set CampiMoveCpy(UBound(CampiMoveCpy)).Valori = New Collection
        CampiMoveCpy(UBound(CampiMoveCpy)).Valori.Add Valore
        Set CampiMoveCpy(UBound(CampiMoveCpy)).Posizioni = New Collection
        CampiMoveCpy(UBound(CampiMoveCpy)).Posizioni.Add deststart
      Else
        For j = 1 To CampiMoveCpy(i).Valori.count
          If CampiMoveCpy(i).Valori.item(j) = Valore Then
            Exit For
          End If
        Next j
        If j = CampiMoveCpy(i).Valori.count + 1 Then
          CampiMoveCpy(i).Valori.Add Valore
          CampiMoveCpy(i).Posizioni.Add deststart
        End If
      End If
      rs.MoveNext
    Wend
    rs.Close
  End If
  If UBound(CampiMoveCpy) = 0 Then
    Set rs = m_Fun.Open_Recordset("SELECT a.* From PsData_Cmpmoved as a,PsRel_Obj as b,PsRel_Obj as c where " & _
                                  "b.idOggettoR=" & GbIdOggetto & " and b.relazione='CPY' and " & _
                                  "b.IdOggettoC = c.IdOggettoC And a.Idpgm=c.IdOggettoR and a.nome='" & pCampoIstruzione & "'")
    While Not rs.EOF
      nome = rs!nome
      Valore = rs!Valore
      deststart = rs!deststart & ""
      For i = 1 To UBound(CampiMoveCpy)
        If CampiMoveCpy(i).NomeCampo = nome Then
          Exit For
        End If
      Next i
      If i = UBound(CampiMoveCpy) + 1 Then ' campo nuovo
        ReDim Preserve CampiMoveCpy(UBound(CampiMoveCpy) + 1)
        CampiMoveCpy(UBound(CampiMoveCpy)).NomeCampo = Replace(nome, ",", "")
        Set CampiMoveCpy(UBound(CampiMoveCpy)).Valori = New Collection
        CampiMoveCpy(UBound(CampiMoveCpy)).Valori.Add Valore
        Set CampiMoveCpy(UBound(CampiMoveCpy)).Posizioni = New Collection
        CampiMoveCpy(UBound(CampiMoveCpy)).Posizioni.Add deststart
      Else
        For j = 1 To CampiMoveCpy(i).Valori.count
          If CampiMoveCpy(i).Valori.item(j) = Valore Then
            Exit For
          End If
        Next j
        If j = CampiMoveCpy(i).Valori.count + 1 Then
          CampiMoveCpy(i).Valori.Add Valore
          CampiMoveCpy(i).Posizioni.Add deststart
        End If
      End If
      rs.MoveNext
    Wend
    rs.Close
  End If
  For j = 1 To UBound(CampiMoveCpy)
    If findCampo Then Exit For
    If CampiMoveCpy(j).NomeCampo = pCampoIstruzione Then
     findCampo = True ' campo univoco, trovato una volta finite ricerche
    ' controllo lista valori - al primo buono esco
     For k = 1 To CampiMoveCpy(j).Valori.count
       'Mauro 07-03-2007
       ' Se il valore � una variabile, mi ricavo nuovamente il valore
       If Left(CampiMoveCpy(j).Valori.item(k), 1) <> "'" Then
         itemValue = getFieldValue(CampiMoveCpy(j).Valori.item(k))
       Else
         itemValue = Trim(Replace(CampiMoveCpy(j).Valori.item(k), "'", ""))
       End If
       Select Case itemValue  'IRIS-DB changed ICMD to CMD and added POS
         Case "PCB", "GU", "GHU", "GN", "GHN", "GNP", "GHNP", "DLET", "REPL", "ISRT", "POS", _
              "TERM", "AUTH", "GCMD", "CMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG", _
              "SETB", "SETO", "SETS", "SETU", "XRST", "SYNC", "CHKP", "CKPT", "APSB", _
              "DPSB", "INIT", "INQY", "LOG", "ROLB", "ROLL", "ROLS", "OPEN", "CLSE"
           risolviIstruzioneByMoveCopyprimo = itemValue
           Exit For
       End Select
     Next
    End If
  Next
Exit Function
err:
  GbOldNumRec = 0
  addSegnalazione pCampoIstruzione, "P00", pCampoIstruzione
End Function
Sub AggiungiObjUnk(wNomeOc, WnomeOr, wRel, wNomeComp)
  Dim rs As Recordset
'SQ - 29-11-05
On Error GoTo errTmp

  Set rs = m_Fun.Open_Recordset("select * from PsRel_ObjUnk where IdOggettoC = " & wNomeOc & " and NomeOggettoR = '" & WnomeOr & "' and relazione = '" & wRel & "'")
  If rs.RecordCount = 0 Then
    Select Case wRel
     
     Case "MFS"
      rs.AddNew
      rs!IdOggettoC = wNomeOc
      rs!nomeOggettoR = WnomeOr
      rs!nomecomponente = wNomeComp
      rs!relazione = wRel
      rs!utilizzo = "MFS"
      rs.Update
   End Select
  End If
  rs.Close
Exit Sub
errTmp:
'SQ - gestire...
  'MsgBox Err.Number
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Analizza i parametri delle CALL IMS
' - Gestione PSB
' - Riconoscimento istruzioni: inserimento in PsDli_Istruzioni
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub checkPSB(parametri As String)
  Dim istrParam As String, istruzione As String, psbParam As String, psb As String
  Dim isSchedule As Boolean
  Dim rsIstruzioni As Recordset, rs As Recordset
  Dim IdPsb As Long
  Dim moveResponse As String
  
  'On Error GoTo tmp
   
  'Mauro 19/01/2009
  'istruzione = Replace(nextToken(parametri), ".", "")
  'IRIS-DB: quanto sotto ha senso solo per il COBOL, non ad esempio per il PL/I, dove il punto
  '   � separatore di livello (come la OF del COBOL);
  '   Forse sarebbe pi� sicuro escluderlo solo per il PL/I ma preferisco rischiare
  If GbTipoInPars = "CBL" Or GbTipoInPars = "CPY" Then 'IRIS-DB
    istruzione = Trim(Replace(nexttoken(parametri), ".", ""))
  Else
    istruzione = Trim(nexttoken(parametri))
  End If 'IRIS-SB
 
  istrParam = istruzione 'mi serve una copia se non riesco a risolvere la variabile...
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Prepariamo la struttura con il campo istruzione che successivamente
  ' sar� associato ai possibili valori determinati dalle move
  
  ReDim Preserve CmpIstruzione(UBound(CmpIstruzione) + 1)
  CmpIstruzione(UBound(CmpIstruzione)).CampoIstruzione = istruzione
  CmpIstruzione(UBound(CmpIstruzione)).Oggetto = GbIdOggetto
  CmpIstruzione(UBound(CmpIstruzione)).Riga = GbOldNumRec
  CmpIstruzione(UBound(CmpIstruzione)).Risolta = 0
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'Se il parametro "istruzione" ha il nome esatto dell'istruzione, OK
  'altrimenti cerca il "VALUE":
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Select Case istruzione
    Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP", "POS" 'IRIS-DB
    Case "DLET", "REPL", "ISRT"
    Case "PCB", "TERM"
'IRIS-DB    Case "AUTH", "GCMD", "ICMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG"
    Case "AUTH", "GCMD", "CMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG"  'IRIS-DB
    Case "SETB", "SETO", "SETS", "SETU", "XRST"
    Case "SYNC", "CHKP", "CKPT", "APSB", "DPSB", "INIT", "INQY", "LOG", "ROLB", "ROLL", "ROLS"
    Case "OPEN", "CLSE"
    Case Else
      'VALUE:
      istruzione = getFieldValue(istruzione)
      'Gestione "PARM-COUNT": primo parametro opzionale!
      If IsNumeric(istruzione) Then
        checkPSB (parametri)
        Exit Sub
      End If
  End Select
  
  'Doppio controllo, ma almeno evitiamo il "PrendiValue" per i casi "sicuri"...
  Select Case istruzione
    Case "PCB"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
      isSchedule = True
    Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP", "POS" 'IRIS-DB
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "DLET", "REPL", "ISRT"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "TERM"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
'IRIS-DB    Case "AUTH", "GCMD", "ICMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG"
    Case "AUTH", "GCMD", "CMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG" 'IRIS-DB
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "SETB", "SETO", "SETS", "SETU", "XRST"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "SYNC", "CHKP", "CKPT", "APSB", "DPSB", "INIT", "INQY", "LOG", "ROLB", "ROLL", "ROLS"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "OPEN", "CLSE"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case ""
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' SQ 8-0307
      ' Gestione "PARM-COUNT": primo parametro opzionale!
      ' Controlliamo il tipo di dato:
      ' Lo facciamo solo qui per ottimizzare: isNumeric fatto
      ' sopra non ha funzionato perch� � tornato evidentemente: ""
      ' Per correttezza andrebbe in testa a tutto senza condizionamenti...
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Set rs = findDataCmp(GbIdOggetto, istrParam)
      If rs.RecordCount Then
        If rs!Tipo = "9" Then
          'rientro!
          checkPSB (parametri)
          rs.Close
          Exit Sub
        End If
      End If
      rs.Close
      'getFieldValue non ha trovato niente
      ' provo a cercare fra le move fin qui trovate
      moveResponse = risolviIstruzioneByMove(CmpIstruzione(UBound(CmpIstruzione)).CampoIstruzione, CmpIstruzione(UBound(CmpIstruzione)).LastSerchIndex, 0)
      If Len(moveResponse) = 0 Then
        addSegnalazione istrParam, "NOISTRDLIVALUE", istrParam
      Else
        istruzione = moveResponse
        CmpIstruzione(UBound(CmpIstruzione)).Risolta = 2
      End If
    Case Else
      moveResponse = risolviIstruzioneByMove(CmpIstruzione(UBound(CmpIstruzione)).CampoIstruzione, CmpIstruzione(UBound(CmpIstruzione)).LastSerchIndex, 0)
      If Not Len(moveResponse) Then
        addSegnalazione istruzione, "NOISTRDLI", istruzione
        istruzione = ""
      Else
        istruzione = moveResponse
        CmpIstruzione(UBound(CmpIstruzione)).Risolta = 2
      End If
  End Select
 
  ''''''''''''''''''''''''''''''''''''''''''''
  ' Inserimento PsDli_Istruzioni:
  ''''''''''''''''''''''''''''''''''''''''''''
  ' AC :Case "" e ricerca move senza successo scrive campo vuoto
  ' a fine parser cercher� fra le move successive
  'SQ - serve per le insert tirar su le singole colonne?
  'SQ CPY-ISTR
  'Set rsIstruzioni = m_Fun.Open_Recordset("select IdOggetto,Riga,istruzione,statement,isCBL from PsDli_Istruzioni")
  Set rsIstruzioni = m_Fun.Open_Recordset("select * from PsDli_Istruzioni")
  rsIstruzioni.AddNew
  'SQ CPY-ISTR
  'TMP - Sistemarlo per tutti i tipi di "CPY" e per tutti i linguaggi...
  rsIstruzioni!idpgm = IIf(GbTipoInPars = "CPY" Or GbTipoInPars = "INC", 0, GbIdOggetto)
  rsIstruzioni!idOggetto = GbIdOggetto
  rsIstruzioni!Riga = GbOldNumRec
  rsIstruzioni!istruzione = istruzione
  'Parametri "utili" (gia' puliti da tutto...compreso eventuale punto finale)
  If Len(parametri) Then
    rsIstruzioni!statement = IIf(Right(parametri, 1) = ".", Left(parametri, Len(parametri) - 1), parametri)
  Else
    rsIstruzioni!statement = ""
  End If
  rsIstruzioni!isCBL = itemValue = "CBLTDLI"
  rsIstruzioni!isCBL = itemValue = "DBINTDLI"
  
  'AC 05/10/2010
  rsIstruzioni!procName = procName
  
  'Chiudo in fondo... dopo il controllo del psb
  
  ''''''''''''''''''''''''''''''''''''''''''''
  ' Schedulazione (gestione PSB):
  ' Es:
  ' - "PCB G006-PSBNAME ADDRESS OF DLIUIB"
  ' - "PCB G006-PSBNAME DLIUIB"
  ''''''''''''''''''''''''''''''''''''''''''''
  If isSchedule Then
    Dim IsValid As Boolean
    
    psbParam = nexttoken(parametri)
    'gestire psbParam=""...
    If Left(psbParam, 1) = "'" Then 'Puo' essere anche il valore costante?
      psb = Mid(psbParam, 2, Len(psbParam) - 2)
    Else
      '?????
      If ActParsNoPsb Then
        '??????
        psb = GbParmRel.DLIStdPsbName
      Else
        '''''''''''''''''''''''''''''''''''''''''''''''
        ' - VALUE iniziale; se non c'�:
        ' - MOVE (la prima, ma le prenderei tutte!)
        '''''''''''''''''''''''''''''''''''''''''''''''
        'psb = PrendiValue(psbParam)
        psb = getFieldValue(psbParam)
        If psb = "SPACE" Or psb = "SPACES" Or UCase(psb) = "NONE" Then
          psb = TrovaPsb(psbParam)
        End If
      End If
    End If
        
    If Len(psb) Then
      Set rs = m_Fun.Open_Recordset("select idOggetto from Bs_oggetti where tipo = 'PSB' and nome = '" & psb & "'")
      If rs.RecordCount = 0 Then
        addSegnalazione psb, "NOPSB", psb
      Else
        'SQ 16-05-07
        IsValid = True
        AggiungiRelazione rs!idOggetto, "PSB", psb, psb
      End If
      rs.Close
    Else
      'P05: nuovo warning: "No values found for field: %VAR1%"
      addSegnalazione psbParam, "NOPSBVALUE", psbParam
    End If
    'SQ 16-05-07
    rsIstruzioni!valida = IsValid
    rsIstruzioni!Imsdb = True
    rsIstruzioni!Imsdc = False
    rsIstruzioni!obsolete = False
    'AC 13/03/2008  TO_MIGRATE
    'Default di to_migrate in checkPSB,resetRepository(parsedue) e UpdatePsDliIstruzioni
    ' Settaggio attuale: default = true, diventa false solo se il dbd � stato trovato ma non � da migrare
    rsIstruzioni!to_migrate = True
  End If
  If istruzione = "TERM" Then
    rsIstruzioni!Imsdb = True
    rsIstruzioni!Imsdc = False
    rsIstruzioni!obsolete = False
    rsIstruzioni!to_migrate = True
    rsIstruzioni!valida = True
  End If
  rsIstruzioni.Update
  rsIstruzioni.Close
  
  Exit Sub
tmp:
  'gestire...
  'MsgBox Err.Description
  'Err.Raise 789
  'Resume
End Sub
Sub getGSAMinfo(pcbNumber As Integer)
  Dim psbString As String
  Dim i As Integer
  Dim rs As Recordset
  
  IsValid = True 'init
  ReDim wIdDBD(0)
  
  For i = 0 To UBound(psbPgm)
    psbString = psbString & psbPgm(i) & ","
  Next
  psbString = Left(psbString, Len(psbString) - 1) 'ultima virgola superflua
  
  Set rs = m_Fun.Open_Recordset("select b.idOggetto as IdDbd,a.dbdName as nameDbd,a.typePCB as typeDbd from PsDLI_Psb as a,bs_oggetti as b where a.idOggetto IN (" & psbString & ") and NumPcb = " & pcbNumber & " and typePcb='GSAM' and a.dbdName=b.nome and b.tipo='DBD' ")
  If rs.RecordCount Then
    If rs.RecordCount = 1 Then
      TabIstr.DBD = rs!nameDbd
      TabIstr.DBD_TYPE = IIf(rs!typeDbd = "GSAM", DBD_GSAM, DBD_HIDAM)  'mi interessa solo se GSMA
      wIdDBD(0) = rs!idDBD
            
      'Inserimento in Tabella PsDli_IstrDBD
      'SQ 8-5-06
      'Gi� fatto nel getInstructionsPSBDBD... pulire la situazione...
      'Set rs = m_Fun.Open_Recordset("select * from PsDli_IstrDBD")
      'SQ CPY-ISTR
      Set rs = m_Fun.Open_Recordset("select * from PsDli_IstrDBD " & _
                                    "where idpgm = " & GbIdOggetto & " and " & _
                                    "idoggetto = " & IdOggettoRel & " and " & _
                                    "riga = " & GbOldNumRec & " And idDBD = " & wIdDBD(0))
      If rs.EOF Then
        rs.AddNew
        rs!idpgm = GbIdOggetto
        rs!idOggetto = IdOggettoRel
        rs!Riga = GbOldNumRec
        rs!idDBD = wIdDBD(0)
        rs.Update
        rs.Close
      End If
      
      Set rs = m_Fun.Open_Recordset("Select IdSegmento,nome,maxlen From PsDLI_segmenti Where IdOggetto = " & wIdDBD(0))
      If rs.RecordCount Then
        ReDim TabIstr.BlkIstr(1)
        ReDim TabIstr.BlkIstr(1).KeyDef(0)
        TabIstr.BlkIstr(1).IdSegm = rs!idSegmento
        'stefano
        'SQ - perch� non prendere la colonna giusta?
        TabIstr.BlkIstr(1).IdSegmenti = rs!idSegmento
        TabIstr.BlkIstr(1).Segment = rs!nome
        'l'ho aggiunto, per i GSAM non lo faceva nel giro principale
        TabIstr.BlkIstr(1).SegLen = rs!MaxLen
        TabIstr.BlkIstr(1).Record = wDliRec 'o <none>"?
        TabIstr.BlkIstr(1).valida = True  'serve al resto del marasma...
      Else
        IsValid = False
        'segnalazione!!!!!!!!!!!!!
      End If
      rs.Close
    Else
      IsValid = False
      'gestire...
      'sono tutti GSAM? tutti lo stesso GSAM?
      rs.Close
    End If
  Else
    IsValid = False
    rs.Close
  End If
  
  '''''''''''''''''''''''''''''''''''''''''''''''
  'Inserimento in Tabella
  ' (psbPgm e' da filtrare con i DBD rimasti...)
  '''''''''''''''''''''''''''''''''''''''''''''''
  If SwPsbAssociato Then
    For i = 0 To UBound(psbPgm)
      'lo faccio da dentro per evitare chiavi duplicate:
      Set rs = m_Fun.Open_Recordset("select * from PsDli_IstrPSB where Idpgm=" & GbIdOggetto & " and IdOggetto=" & IdOggettoRel & " AND Riga=" & GbOldNumRec & " AND IdPSB=" & psbPgm(i))
      If rs.RecordCount = 0 Then
        rs.AddNew
        rs!idpgm = GbIdOggetto
        rs!idOggetto = IdOggettoRel
        rs!Riga = GbOldNumRec
        rs!IdPsb = psbPgm(i)
        rs.Update
      End If
      rs.Close
    Next
  End If
End Sub
Public Sub parseOPEN_CLOSE(istruzione As String, parametri As String)
  Dim rs As Recordset
  Dim i As Integer
  Dim rsdbd As Recordset
  '''''''''''''''''''''
  ' PCB (number)
  '''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  TabIstr.numpcb = getPCBnumber(TabIstr.pcb)  'setta isDC
  If TabIstr.numpcb Then
  'stefano: forzato, DA RIVEDERE!!!!
    IsValid = True
    '''''''''''''''''''
    'PCB-IO: IMS-DC
    '''''''''''''''''''
    '''''''''isDC = SwTpIMS And (TabIstr.numPCB = 1)
    
    getGSAMinfo (TabIstr.numpcb)  'wIDdbd...
    If TabIstr.DBD_TYPE = DBD_GSAM Then
      ' DBD: inserito dentro getGSAMinfo
'''''      Set rs = m_Fun.Open_Recordset("select * from PsDli_IstrDBD")
'''''      For i = 0 To UBound(wIdDBD)
'''''         rs.AddNew
'''''         rs!idOggetto = GbIdOggetto
'''''         rs!riga = GbOldNumRec
'''''         rs!idDbd = wIdDBD(i)
'''''         rs.Update
'''''      Next i
'''''      rs.Close
    Else
      IsValid = False
      'NE SERVE UNA NUOVA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      addSegnalazione GbNomePgm, "NODECOD", GbNomePgm
      Exit Sub
    End If
  Else
    IsValid = False
  End If
  '''''''''''''''''''''''''''''''''''''''''''''''
  ' PSB: Inserimento in Tabella
  '''''''''''''''''''''''''''''''''''''''''''''''
  If SwPsbAssociato Then
    For i = 0 To UBound(psbPgm)
      'lo faccio da dentro per evitare chiavi duplicate:
      Set rs = m_Fun.Open_Recordset("select * from PsDli_IstrPSB where Idpgm=" & GbIdOggetto & " and IdOggetto=" & IdOggettoRel & " AND Riga=" & GbOldNumRec & " AND IdPSB=" & psbPgm(0))
      If rs.RecordCount = 0 Then
        rs.AddNew
        rs!idpgm = GbIdOggetto
        rs!idOggetto = IdOggettoRel
        rs!Riga = GbOldNumRec
        rs!IdPsb = psbPgm(0)
        rs.Update
      End If
      rs.Close
    Next
  End If

  ''''''''''''''''''''''''''''''''''''''''''''
  ' UPDATE "PsDli_Istruzioni":
  ''''''''''''''''''''''''''''''''''''''''''''
    UpdatePsdliIstruzioni (IsDC)

End Sub
Public Sub parseREPL_call(istruzione As String, parametri As String)
  Dim rs As Recordset, tb As Recordset, tb1 As Recordset, tb2 As Recordset, rsdbd As Recordset
  Dim i As Integer, k As Integer, y As Integer
  Dim dbdname As String, ssaValue As String ', ssaField As String
  Dim isGSAM As Boolean, unqualified As Boolean, segmentNotFound As Boolean
  Dim idField As Integer
  Dim obsolete As Boolean
  Dim rsPSB As Recordset 'IRIS-DB
  Dim PCBNum As Integer 'IRIS-DB
  
  On Error GoTo parseErr
  
  ReDim gSSA(0) 'A cosa serve?
  ReDim dbdPgm(0)
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  TabIstr.istr = istruzione
    
  '''''''''''''''''''''
  ' PCB (number)
  '''''''''''''''''''''
  TabIstr.numpcb = getPCBnumber(TabIstr.pcb)  'setta isDC
'IRIS-DB start - non uso la funzione CMPAT perch� mi sembra incoerente con quello che ha fatto il parser di primo livello - da rivedere con programmi CICS
'''''  If psbPgm(0) > 0 Then
'''''    PCBNum = IIf(IsCMPAT(CStr(psbPgm(0)), GbIdOggetto) = True, 1, 0)
  If psbPgm(0) > 0 Then
    Set rsPSB = m_Fun.Open_Recordset("Select TypePCB From PsDLI_Psb Where idoggetto = " & psbPgm(0) & " and NumPcb = " & TabIstr.numpcb)
    If rsPSB.RecordCount Then
      If rsPSB!TypePcb = "TP" Then
        IsDC = True
      End If
    Else
      If TabIstr.numpcb = 1 Then
        IsDC = True
      Else
        m_Fun.writeLog "parseGU/REPL_call - PCB not found in PSB table Idogg(" & GbIdOggetto & ") Riga(" & GbOldNumRec & ") Idpsb(" & psbPgm(0) & ")"
      End If
    End If
    rsPSB.Close
  Else
    m_Fun.writeLog "parseGU/REPL_call - missing PSB - not checking PCB type Idogg(" & GbIdOggetto & ") Riga(" & GbOldNumRec & ") Idpsb(" & psbPgm(0) & ")"
  End If
  If Len(TabIstr.pcb) > 0 And Not IsDC Then 'IRIS-DB
  'IRIS-DB forzatura per i programmi chiamati (Fedex ne � piena)
    If m_Fun.FnNomeDB = "LSSIprj.mty" Then 'IRIS-DB forzato solo per Fedex
      If Left(TabIstr.pcb, 3) = "ALT" Then IsDC = True
      If Left(TabIstr.pcb, 3) = "EXP" Then IsDC = True
      If Left(TabIstr.pcb, 2) = "IO" Then IsDC = True
      If Mid(TabIstr.pcb, 3, 1) = "T" And Not Mid(TabIstr.pcb, 4, 1) = "-" Then IsDC = True
      If Left(TabIstr.pcb, 6) = "LZQ179" Then IsDC = True
    End If
  End If
  'IRIS-DB end
  'IRIS-DB start - ottiene il DBD prima (sarebbe da rivalutare tutto l'ordine del parsing
  If TabIstr.numpcb > 0 And psbPgm(0) > 0 And Not IsDC Then
    getDbdfromPsb TabIstr.numpcb, psbPgm(0)
  End If
  'IRIS-DB end
  If IsDC Then
    '''''''''''''''''''
    'PCB-IO: IMS-DC
    '''''''''''''''''''
    parseSEND istruzione, parametri
    Exit Sub
  Else
    wDliRec = nexttoken(parametri)
    'AC 12/02/10 Controllo che l'area non sia elemento di un array
  
    wDliRec = wDliRec & ChecktokenArray(parametri)
  End If
  
  'COMPLETAMENTE SQUALIFICATA?
  'unqualified = Len(parametri) = 0
  unqualified = False
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Gestione SSA
  ' Valorizzo la TabIstrSSa
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ReDim TabIstrSSa(0)
  ReDim TabIstrSSa(0).NomeSegmento.value(0)
  ReDim wDliSsa(0)
  Dim ssaOk As Boolean
  ssaOk = True
  While Len(parametri)
    i = i + 1
    'SSA nome field:
    ReDim Preserve wDliSsa(UBound(wDliSsa) + 1)
    'AC 26/02/10
    wDliSsa(UBound(wDliSsa)) = nextToken_new(parametri) & CheckConcatenation(parametri)
    'stefano: parsa le aree costanti; per ora solo l'SSA, ci sono anche casi sulla funzione da implementare...
    If InStr(wDliSsa(UBound(wDliSsa)), "'") = 0 Then
    'SSA id field:
      Dim rsArea As Recordset, rsLink As Recordset, rsCmp As Recordset
      Set rsArea = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & GbIdOggetto)
      If rsArea.RecordCount = 0 Then
        'Ricerca nelle COPY:
        Set rsLink = m_Fun.Open_Recordset("Select IdOggettoR From PsRel_Obj Where IdOggettoC = " & GbIdOggetto & " And (Relazione = 'CPY' or Relazione = 'INC')")
        Do While Not rsLink.EOF
          Set rsCmp = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & rsLink!idOggettoR)
          If rsCmp.RecordCount Then
             Set rsArea = rsCmp
             Exit Do
          End If
          rsLink.MoveNext
        Loop
        ''''''''''''''''''''''''''''''''''''''''''''''''
        ' COPY IN COPY (non si finisce mai!)
        ' SQ 10-09-09 (vedere se si pu� fare meglio...)
        ''''''''''''''''''''''''''''''''''''''''''''''''
        If rsLink.RecordCount Then
          rsLink.MoveFirst  'Altro giro:
          Do While Not rsLink.EOF
            Set rs = m_Fun.Open_Recordset("Select IdOggettoR From PsRel_Obj Where IdOggettoC = " & rsLink!idOggettoR & " And (Relazione = 'CPY' or Relazione = 'INC')")
            Do While Not rs.EOF
              Set rsCmp = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & rs!idOggettoR)
              If rsCmp.RecordCount Then
                Set rsArea = rsCmp
                Exit Do
              End If
              rs.MoveNext
            Loop
            rs.Close
            rsLink.MoveNext
          Loop
        End If
        rsLink.Close
      End If
    
      If rsArea.RecordCount Then
        ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
        TabIstrSSa(UBound(TabIstrSSa)).SSAName = wDliSsa(UBound(wDliSsa))
        TabIstrSSa(UBound(TabIstrSSa)).idOggetto = rsArea!idOggetto
        TabIstrSSa(UBound(TabIstrSSa)).idArea = rsArea!idArea
        'Valorizza TabIstrSSa:
        parseSSA TabIstrSSa(UBound(TabIstrSSa))
      Else
       ''''''''''''''''''''''''''''''''''''''''
       ' SQ - 19-04-06
       ' Nuova segnalazione (I02)
       ''''''''''''''''''''''''''''''''''''''''
        addSegnalazione wDliSsa(UBound(wDliSsa)), "NOSSAVALUE", wDliSsa(UBound(wDliSsa))
        ssaOk = False
      End If
      rsArea.Close
    Else
      ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
      TabIstrSSa(UBound(TabIstrSSa)).SSAName = wDliSsa(UBound(wDliSsa))
      TabIstrSSa(UBound(TabIstrSSa)).idOggetto = 0 'fittizio
      TabIstrSSa(UBound(TabIstrSSa)).idArea = 0 'fittizio
      parseSSA TabIstrSSa(UBound(TabIstrSSa))
    End If
     
  Wend
  
  ''''''''''''''''''''''''''''''''''''''''
  ' SQ 20-04-06
  ' Area Segmento - la associava a tutti!
  '''''''''''''''''''''''''''''''''''''''''
  ReDim wIdDBD(0) 'IRIS-DB
  wIdDBD(0) = 0 'IRIS-DB
  If Not unqualified Then 'SQ variabile forzata all'inizio... sistemare bene il giro!
    TabIstrSSa(UBound(TabIstrSSa)).Struttura = wDliRec
    If Len(TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0)) = 0 And Len(wDliRec) Then  'Solo sul Segmento finale...
      ''''''''''''''''''''''''''''''''''''''''''''''
      ' SQ 2-05-06
      ' Naming Convention SEGMENTO/AREA
      ''''''''''''''''''''''''''''''''''''''''''''''
      Dim env As Collection 'area,idDbd
      If Len(segmentNameParam) Then
        Set env = New Collection  'resetto;
        env.Add wDliRec
        'SQ 5-06-06 - Come poteva funzionare?
'IRIS-DB: non so perch� era tolto, provo a rimetterlo. Ho capito: la valorizzazione la fa sotto... per cui becca sempre il DBD della precedente istruzione! Difficile da aggiustare perch� per il DBD usa il segmento e viceversa...
        'IRIS-DB env.Add 0 'wIdDBD(0)
        'IRIS-DB questa � veramente una pezza, ma per non farla bisognerebbe riscrivere il parser da zero in maniera pi� logica
        'IRIS-DB per ora lo metto solo sulla REPL (cio� ISRT, REPL, DLET), sulla GU non dovrebbe servire mai tranne in quelle completamente squalificate, ma vabbe
        getInstructionPsbDbd (TabIstr.numpcb) 'IRIS-DB incredibile ma vero, lo fa due volte, perch� � un cane che si morde la coda: per tirare fuori il segmento usa anche il dbd, e per tirare fuori il dbd usa anche il segmento - DA RIFARE!!!!!
        env.Add wIdDBD(0) 'IRIS-DB
        If wIdDBD(0) = 0 Then 'IRIS-DB
          m_Fun.writeLog "parseREPL_call - Attention! idDBD not set while searching the segment by Area Name - Idogg(" & GbIdOggetto & ") Riga(" & GbOldNumRec & ")" 'IRIS-DB
        End If 'IRIS-DB
        'SQ MOVED/VALUE
        'TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0) = getEnvironmentParam(segmentNameParam, "IMS", env)
        Dim segmentName As String
        'buttare la var sopra--^
        TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0) = getEnvironmentParam(segmentNameParam, "IMS", env)
      End If
    End If
    
    ''''''''''''''''''''''''''''''''''''''''''
    ' TabIstrSSa -> TabIstr
    ''''''''''''''''''''''''''''''''''''''''''
    getTabIstr
  End If
  
'commento provvisorio AC 18/07/07
  
'  'stefano: forzatura per DLET/REPL senza PSB
'  'lasciato comunque anche nel ciclo, nel ciclo serve solo per la ISRT
'  If (TabIstr.Istr = "REPL" Or TabIstr.Istr = "DLET") And UBound(TabIstr.BlkIstr) = 1 Then
'    'SQ: idSegmento non l'ha ancora valorizzato nessuno, in questo punto!
'    If Len(TabIstr.BlkIstr(1).idSegmenti) = 0 Then
'     'Scamuffo I/O AREA -> SEGMENTO (Ha senso solo sull'ultimo livello - volendo si pu� rendere opzionale)
'       getSegment_byArea TabIstr.BlkIstr(1)
'       If TabIstr.BlkIstr(1).idsegm <> 0 Then
'          isValid = True
'          'unqualified = True
'       End If
'     End If
'  End If
  
  'IRIS-DB: ripristinato in quanto utile - attenzione, solo per FEDEX per ora!!!!
  'If m_Fun.FnNomeDB = "LSSIprj.mty" And (TabIstr.istr = "REPL" Or TabIstr.istr = "DLET" Or TabIstr.istr = "ISRT") And UBound(TabIstr.BlkIstr) = 1 Then 'IRIS-DB aggiunto anche ISRT
  'IRIS-DB: rischio e lo rimetto per tutti
  If (TabIstr.istr = "REPL" Or TabIstr.istr = "DLET" Or TabIstr.istr = "ISRT") And UBound(TabIstr.BlkIstr) = 1 Then 'IRIS-DB aggiunto anche ISRT
    If Len(TabIstr.BlkIstr(1).IdSegmenti) = 0 Then
       If wIdDBD(0) = 0 Then 'IRIS-DB e vai con la terza volta!!!
        getInstructionPsbDbd (TabIstr.numpcb)
       End If 'IRIS-DB
       getSegment_byArea_IRIS TabIstr.BlkIstr(1)
       If TabIstr.BlkIstr(1).IdSegm <> 0 Then
          IsValid = True
          unqualified = True
       End If
     End If
  End If
 

  ''''''''''''''''''''''''''''''''
  ' Gestione PSB/DBD:
  ' Input: N PSB, PCB, M SEGMENTI
  ''''''''''''''''''''''''''''''''
  If wIdDBD(0) = 0 Then 'IRIS-DB
    getInstructionPsbDbd (TabIstr.numpcb)
  End If 'IRIS-DB
  'SQ 17-10-07 (copiato dalla GU)
  obsolete = getObsolete(TabIstr.DBD)
  
  ''''''''''''''''''''''''''''''''''''''''''
  ' TabIstr.BlkIstr:
  ''''''''''''''''''''''''''''''''''''''''''
  'isGSAM = False
  'If TabIstr.DBD_TYPE = DBD_GSAM Then
  '   isGSAM = True
  'End If
  ''If ssaOk Then 'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
  'If ssaOk Or isGSAM Then 'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
  '  getTabIstrIds
  'Else
  ' IsValid = False
  'End If
  
  isGSAM = TabIstr.DBD_TYPE = DBD_GSAM
  If isGSAM Then
    getGSAMinfo (TabIstr.numpcb)  'wIDdbd...
  Else
    ''''''''''''''''''''''''''''''''''''''''''
    ' TabIstr.BlkIstr:
    ''''''''''''''''''''''''''''''''''''''''''
    If ssaOk And IsValid Then 'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
      getTabIstrIds
    Else
      If Not unqualified Then
        IsValid = False
      End If
    End If
  End If
  
  'SQ 17-10-07 ATTENZIONE!!! devo scrivere ugualmente! VERIFICARE BENE...
  If True Or IsValid Or isGSAM Then
    ''''''''''''''''''''''''''''''''''''
    'ITERAZIONE LIVELLI ISTRUZIONE:
    ''
    'AGGIORNAMENTO "PsDli_IstrDett_Liv"
    ''''''''''''''''''''''''''''''''''''
    Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
    For k = 1 To UBound(TabIstr.BlkIstr)
      '''''''''''''''''''''
      'SEGMENTO VARIABILE?
      '''''''''''''''''''''
      If Left(TabIstr.BlkIstr(k).Segment, 1) = "(" And Right(TabIstr.BlkIstr(k).Segment, 1) = ")" Then
        ReDim IdSegmenti(0)
        '''''''''''''''''''''''''''''''''''''
        ' Segmento variabile: gestione temp.
        '''''''''''''''''''''''''''''''''''''
      End If
      
      'SQ 17-10-07 (come per GU)
      If Len(TabIstr.BlkIstr(k).Segment) = 0 Then
        ReDim IdSegmenti(0)
      End If
      
      ''''''''''''''''''''''''''''''''''''''
      'INSERIMENTO IN "PsDli_IstrDett_Liv":
      ''''''''''''''''''''''''''''''''''''''
      tb.AddNew
      'SQ COPY-ISTR
      'tb!idOggetto = GbIdOggetto
      tb!idOggetto = IdOggettoRel
      tb!idpgm = GbIdOggetto
      tb!Riga = GbOldNumRec
      tb!livello = k
      tb!valida = TabIstr.BlkIstr(k).valida
      
      'SEGMENTO:
      tb!IdSegmento_moved = TabIstr.BlkIstr(k).segment_moved
      
      '''''''''''''''''''''''''''''''''''''''''''''
      ' SQ IMP!
      ' verificare... perch� � stato commentato?
      '''''''''''''''''''''''''''''''''''''''''''''
      'AC commentato
'      If Len(TabIstr.BlkIstr(k).idSegmenti) = 0 Then
'        'Scamuffo I/O AREA -> SEGMENTO (Ha senso solo sull'ultimo livello - volendo si pu� rendere opzionale)
'        If k = UBound(TabIstr.BlkIstr) Then
'          getSegment_byArea TabIstr.BlkIstr(k)
'        End If
'      End If
      ' fine commento
      
      '''''''''''''''''''''''''''''''''''''''''''''
      ' SQ IMP!
      ' verificare... perch� � stato commentato?
      '''''''''''''''''''''''''''''''''''''''''''''
      'AC commentato
'      If Len(TabIstr.BlkIstr(k).idSegmenti) Then
'        tb!IdSegmento = TabIstr.BlkIstr(k).idsegm
'        tb!IdSegmentid = TabIstr.BlkIstr(k).idSegmenti
'      Else
'        'Segmento non trovato: istruzione INVALIDA!
'        isValid = False
'        segmentNotFound = True
'        addSegnalazione TabIstr.BlkIstr(k).segment, "NOSEGM", TabIstr.Istr
'      End If
     ' fine commento
     
     'If Len(TabIstr.DBD) And (TabIstr.istr = "REPL" Or TabIstr.istr = "DLET") Then
      'TabIstr.BlkIstr(k).valida = True
     'End If
     
     'AC 19/07/07
     If TabIstr.BlkIstr(k).IdSegm Then
      tb!idSegmento = TabIstr.BlkIstr(k).IdSegm
      tb!IdSegmentiD = IIf(TabIstr.BlkIstr(k).IdSegmenti <> "", TabIstr.BlkIstr(k).IdSegmenti, TabIstr.BlkIstr(k).IdSegm) 'IRIS-DB
      ' 30/10/2007
      TabIstr.BlkIstr(k).valida = True
      IsValid = True
     Else
        'Segmento non trovato: istruzione INVALIDA!
        If Not unqualified And Not obsolete Then
          segmentNotFound = True
          'AC 26/07/10
          If Len(TabIstr.DBD) And (TabIstr.istr = "REPL" Or TabIstr.istr = "DLET") Then
            IsValid = True
            TabIstr.BlkIstr(k).valida = True
          Else
            IsValid = False
            addSegnalazione TabIstr.BlkIstr(k).Segment, "NOSEGM", TabIstr.istr 'IRIS-DB
          End If
          
'IRIS-DB: spostato sopra          addSegnalazione TabIstr.BlkIstr(k).Segment, "NOSEGM", TabIstr.istr
        End If
     End If
     
     If Len(TabIstr.BlkIstr(k).SegmentiLen) Then
       If TabIstr.BlkIstr(k).SegLen Then
         tb!SegLen = CInt(TabIstr.BlkIstr(k).SegLen)
       Else
         'Imposto il primo valore tra quelli multipli
         Dim SegmentLen() As String
         SegmentLen() = Split(TabIstr.BlkIstr(k).SegmentiLen, ";")
         tb!SegLen = SegmentLen(0)
       End If
       tb!SegmentiLend = TabIstr.BlkIstr(k).SegmentiLen
     ElseIf Len(TabIstr.BlkIstr(k).SegLen) Then
      tb!SegLen = CInt(TabIstr.BlkIstr(k).SegLen)
      tb!SegmentiLend = CInt(TabIstr.BlkIstr(k).SegLen)
     Else
       tb!SegLen = 0
       tb!SegmentiLend = 0
     End If
     
      tb!NomeSSA = TabIstr.BlkIstr(k).SSAName
      
      tb!dataarea = TabIstr.BlkIstr(k).Record
    
      'COMMAND CODES
      'Occhio: c'� la search gi� decodificata...
      tb!condizione = Left(TabIstr.BlkIstr(k).CodCom, 100)
      
      tb!comcodes_moved = TabIstr.BlkIstr(k).CodCom_moved
       
      'QUALIFICAZIONE (MULTIPLA)
      tb!Qualificazione = TabIstr.BlkIstr(k).qualified
      tb!Qualificazione_moved = TabIstr.BlkIstr(k).qualified_moved
      tb!Qualandunqual = TabIstr.BlkIstr(k).qual_unqual
  
      tb!Correzione = False
      'stefano: mancava...  ??COS'E'??
      If istruzione = "ISRT" Then
        For i = 0 To UBound(TabIstrSSa)
          If Trim(TabIstrSSa(i).SSAName) = Trim(TabIstr.BlkIstr(k).SSAName) Then
            tb!stridoggetto = TabIstrSSa(i).idOggetto
            tb!strIdArea = TabIstrSSa(i).idArea
        'stefano: per i GSAM la precedenti if � comunque vera, anche se entrambi i valori sono vuoti!!
        ' Per ora ho messo una pezza, ma perch� non caricare direttamente dalla ricerca per la psdlirel_segaree?
        ' ed inserire la lunghezza dalla funzione apposita? Perch� non riporta anche l'ordinale?
            If isGSAM Then
            'purtroppo devo fare cos�, altrimenti lo rifaccio da capo...
               Set rsArea = m_Fun.Open_Recordset("Select IdOggetto,IdArea,Byte From PsData_Cmp Where Nome = '" & TabIstr.BlkIstr(k).Record & "' And IdOggetto = " & GbIdOggetto)
               If rsArea.RecordCount = 0 Then
                  'Ricerca nelle COPY:
                  Set rsLink = m_Fun.Open_Recordset("Select IdOggettoR From PsRel_Obj Where IdOggettoC = " & GbIdOggetto & " And (Relazione = 'CPY' or Relazione = 'INC')")
                  Do While Not rsLink.EOF
                    Set rsCmp = m_Fun.Open_Recordset("Select IdOggetto,IdArea,Byte From PsData_Cmp Where Nome = '" & TabIstr.BlkIstr(k).Record & "' And IdOggetto = " & rsLink!idOggettoR)
                    If rsCmp.RecordCount Then
                       Set rsArea = rsCmp
                       Exit Do
                    End If
                    rsCmp.Close
                    rsLink.MoveNext
                  Loop
                  rsLink.Close
               Else
                  tb!stridoggetto = rsArea!idOggetto
                  tb!strIdArea = rsArea!idArea
                  tb!AreaLen = rsArea!byte
               End If
               rsArea.Close
            End If
            Exit For
          End If
        Next
      End If
    
      'SQ - 27-05-05: aspettare la validita' delle chiavi per chiudere...
      If TabIstr.BlkIstr(k).qualified Then
        '''''''''''''''''''''''''''''''''''''''''''''''''
        'GESTIONE CHIAVI:
        'Cerca ID campo chiave
        'Allestisce tabella record in PsDli_IstrDett_Key
        'Setta la validita' dell'istruzione;
        '''''''''''''''''''''''''''''''''''''''''''''''''
        For y = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
          'SQ-2: temporaneo - valutare caso di segmenti multipli...
          'idField = CercaIdField(wIdSegm, TabIstr.BlkIstr(K).KeyDef(y).Key)
          If TabIstr.BlkIstr(k).IdSegm Then
             'piu' di un elemento: indeterminatezza
             'Mauro 19-03-2007 Multi-Segm: Sarebbe da gestire per Multi-Segmenti
             idField = CercaIdField(TabIstr.BlkIstr(k).IdSegm, TabIstr.BlkIstr(k).KeyDef(y).Key, TabIstr.DBD_TYPE) 'IRIS-DB aggiunto DBD_TYPE per i logici
          Else
             idField = 0
          End If
          
          'SQ - 27-05-05
          If idField = 0 Then
            TabIstr.BlkIstr(k).valida = False 'istruzione sbagliata in ogni caso!
            IsValid = False
            '''''''''''''''''''''''''''''''''''''''
            ' SQ 5-05-06 - Nuova segnalazione (I04)
            '''''''''''''''''''''''''''''''''''''''
            addSegnalazione TabIstr.BlkIstr(k).KeyDef(y).Key, "SSAKEYNOTFOUND", TabIstr.BlkIstr(k).KeyDef(y).Key
          End If
    
          'SQ: spostata qui... era fuori ciclo e settava solo la chiave 1!?
          TabIstr.BlkIstr(k).KeyDef(y).xName = GbXName
          
          Set tb1 = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Key")
          tb1.AddNew
          'SQ COPY-ISTR
          'tb1!idOggetto = GbIdOggetto
          tb1!idOggetto = IdOggettoRel
          tb1!idpgm = GbIdOggetto
          tb1!Riga = GbOldNumRec
          tb1!livello = k
          tb1!progressivo = y
          
          tb1!idField = idField
          tb1!idField_moved = TabIstr.BlkIstr(k).KeyDef(y).Key_moved
          'SQ: aspettare il via di Ste per mettere true se idField negativo.
          tb1!xName = TabIstr.BlkIstr(k).KeyDef(y).xName
    
          'OPERATORE
          tb1!operatore = TabIstr.BlkIstr(k).KeyDef(y).Op 'multiplo
          tb1!operatore_moved = TabIstr.BlkIstr(k).KeyDef(y).Op_moved
          
          'SQ: perche' se e' = ""??????????
          If TabIstr.BlkIstr(k).KeyDef(y).Key <> "Nothing" Then
            tb1!Valore = Trim(TabIstr.BlkIstr(k).KeyDef(y).KeyVal & "")
            tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
            'SQ: aggiungere colonna in DB!
            tb1!KeyStart = TabIstr.BlkIstr(k).KeyDef(y).KeyStart
          End If
          tb1!KeyLen = TabIstr.BlkIstr(k).KeyDef(y).KeyLen
          
          'SG : Se la lunghezza non � stata calcolata o calcolata male recupera l'effettiva lunghezza dal field dli
          If Val(tb1!KeyLen) <= 0 Or Trim(tb1!KeyLen) = "" Then
             tb1!KeyLen = Restituisci_DatiKey(tb1!idField, tb1!xName, "LUNGHEZZA")
             TabIstr.BlkIstr(k).KeyDef(y).KeyLen = tb1!KeyLen
             
             If Trim(UCase(TabIstr.BlkIstr(k).SSAName)) <> "" Then
                TabIstr.BlkIstr(k).KeyDef(y).KeyVarField = TabIstr.BlkIstr(k).SSAName & "(" & TabIstr.BlkIstr(k).KeyDef(y).KeyStart & ":" & TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ")"
                tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
             End If
          End If
          
          'OPERATORE LOGICO ("BOOLEANO")
          If Len(TabIstr.BlkIstr(k).KeyDef(y).KeyBool) = 0 Then
            tb1!Booleano = " "
          Else
            tb1!Booleano = TabIstr.BlkIstr(k).KeyDef(y).KeyBool
          End If
          tb1!booleano_moved = TabIstr.BlkIstr(k).KeyDef(y).KeyBool_moved
                  
          tb1!valida = TabIstr.BlkIstr(k).valida
          
          tb1!Correzione = False
          tb1.Update
          tb1.Close
        Next y
      End If
      
      tb!moved = TabIstr.BlkIstr(k).moved
      If Not TabIstr.BlkIstr(k).valida Then IsValid = False
      
      tb.Update
    Next k 'ITERAZIONE LIVELLI...
    tb.Close
  Else
    'Manca PSB/DBD
  End If
  
  ''''''''''''''''''''''''''''''''''''''''
  ' UPDATE "PsDli_Istruzioni":
  ''''''''''''''''''''''''''''''''''''''''
  UpdatePsdliIstruzioni (IsDC)
  
  ''''''''''''''''''''''''''''''''''''''''
  ' Segnalazioni
  ''''''''''''''''''''''''''''''''''''''''
  'Da eliminare : portare nelle rispettive posizioni...
  If Not IsValid And ssaOk Then
    If Len(TabIstr.DBD) And wIdDBD(0) = 0 Then  'NO DBD
      addSegnalazione dbdname, "NODBD", TabIstr.istr
    ElseIf Not segmentNotFound Then
      'SQ: qui si entra per esclusione!!!!!!!!!
      addSegnalazione "", "INVALIDDLIISTR", ""
    End If
     '''''AggiungiSegnalazione TabIstr.Istr, "NODECOD", TabIstr.Istr
  End If
  Exit Sub
parseErr:
  addSegnalazione parametri, "I00", parametri
  'Resume
End Sub
Public Sub parseISRT_call(istruzione As String, parametri As String)
  parseREPL_call istruzione, parametri
End Sub
Public Sub parseROLL(istruzione As String, parametri As String)
'  Dim rsIstruzioni As Recordset
'  Set rsIstruzioni = m_Fun.Open_Recordset("select * from PsDli_Istruzioni where idoggetto = " & GbIdOggetto & " and riga = " & GbOldNumRec)
'  If rsIstruzioni.RecordCount = 0 Then
'     rsIstruzioni.AddNew
'  End If
'  rsIstruzioni!IdOggetto = GbIdOggetto
'  rsIstruzioni!riga = GbOldNumRec
'  rsIstruzioni!Valida = True
'  rsIstruzioni!istruzione = "ROLL"
'  rsIstruzioni!statement = Trim(Mid(statement, 8))
'  'rsIstruzioni!ImsDC = True
'  'rsIstruzioni!ImsDB = False
'  rsIstruzioni.Update
'  rsIstruzioni.Close
  Dim i As Integer
  Dim wDliRec As String
  Dim rsdbd As Recordset
  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  wDliRec = nexttoken(parametri)
  'AC 12/02/10 Controllo che l'area non sia elemento di un array
  
  wDliRec = wDliRec & ChecktokenArray(parametri)
  TabIstr.istr = istruzione
  TabIstr.psbId = 0
  TabIstr.psbName = " "
  IsValid = True 'IRIS-DB

  Dim tb As Recordset
  If Len(TabIstr.pcb) > 0 Then 'IRIS-DB: gestisce anche la ROLB della EXEC DLI in questo modo
    TabIstr.numpcb = getPCBnumber(TabIstr.pcb)
  End If
  'IRIS-DB UpdatePsdliIstruzioni (IsDC)
  UpdatePsdliIstruzioni (False)
  
  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  If Len(wDliRec) Then
    tb.AddNew
    'SQ COPY-ISTR
    'tb!idOggetto = GbIdOggetto
    tb!idOggetto = IdOggettoRel
    tb!idpgm = GbIdOggetto
    tb!Riga = GbOldNumRec
    tb!livello = 1
    tb!NomeSSA = ""
    tb!dataarea = wDliRec
    tb.Update
  Else
    'segnalazione istruzione "sbagliata"
  End If
  tb.Close

End Sub
Sub updatePsPgmEntry(idOggetto As Long, NumRec As Long, nlinee As Integer, param As String)
  m_Fun.FnConnection.Execute "delete from PsPgm Where comando = 'ENTRY' and IdOggetto =" & GbIdOggetto
  m_Fun.FnConnection.Execute "insert into PsPgm (IdOggetto,riga,linee,[parameters],comando) values(" _
  & idOggetto & ", " & NumRec & ", " & nlinee & ",'" & param & "','ENTRY')"
End Sub
Public Sub parseEntry(line As String, FD As Long)
  Dim statement As String, program As String, token As String
  Dim endStatement As Boolean
  Dim rs As New Recordset
  Dim numpcb As Integer, copyname As String, path As String, FDC As Integer
  Dim linee As Integer, parameters As String
  On Error GoTo entryErr

  statement = line
  If Right(statement, 1) = "." Then endStatement = True
  GbOldNumRec = GbNumRec
  Do Until Not EOF(FD) And endStatement
    Line Input #FD, line
    GbNumRec = GbNumRec + 1
    If Mid(line, 7, 1) <> "*" Then
      line = Trim(Mid(line, 8, 65))
      If Len(line) Then
' AC 12/02/07  sostituito il riconoscimento di fine statement con
'              quello della getcall

'          statement = statement & " " & line
'          If InStr(1, line, ".") > 0 Then
'             endStatement = True
'             Exit Do
'          End If
        'Altro statement?
          If isCobolLabel(line) Then 'LABEL!
            endStatement = True
            linee = GbNumRec - GbOldNumRec
          Else
            token = nexttoken(line)
            'Parola Cobol?
            'SQ 27-04-06: se mi arriva END-IF./END-CALL. con il punto sbaglia!
            If token <> "USING" And MapsdM_COBOL.isReservedWord(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Then
              'IRIS-DB: removed as fixed the parser to accept ENTRY without a point, even not correct for COBOL manual
              ' m_Fun.writeLog "parseEntry - missing dot - idoggetto (" & GbIdOggetto & ") - riga (" & GbOldNumRec & ")" 'IRIS-DB deve segnalare una ENTRY senza punto
              endStatement = True
              'linea buona per il giro successivo!
              line = Space(8) & token & " " & line
              'SQ 4-05-06
              'Mi serve linee perch� nei casi di "isReservedWord", gbnumrec uno in pi�
              linee = GbNumRec - GbOldNumRec
              If token = "COPY" Then
                MapsdM_COBOL.isCopyUsing = True
              End If
            Else
              'linea buona:
              statement = statement & " " & token & " " & line
              'SQ - Fine statement con Punto finale o ELSE!
              If Len(line) Then
                If Mid(line, Len(line), 1) = "." Then
                  endStatement = True
                  linee = GbNumRec - GbOldNumRec + 1
                  line = "" 'e' il flag... per la nuova lettura
                ElseIf Right(" " & line, 5) = " ELSE" Then
                  endStatement = True
                  linee = GbNumRec - GbOldNumRec
                  line = "" 'e' il flag... per la nuova lettura
                  'Mangio l'ELSE finale
                  statement = Trim(Left(statement, Len(statement) - 4))
                Else
                  endStatement = False
                End If
              Else
                endStatement = Mid(token, Len(token), 1) = "."
                linee = GbNumRec - GbOldNumRec + 1
              End If
            End If
          End If
        End If
    End If
  Loop
  
  statement = Replace(statement, ".", "")
  statement = Replace(statement, ",", "")
  token = nexttoken(statement)
  If token = "DLITCBL" Then
  'stefano: visto che tutti forzano
    SwDli = True
    token = nexttoken(statement)  'USING
    If token = "USING" Then
      Set rs = m_Fun.Open_Recordset("select * from PsIMS_entry")
      token = nexttoken(statement)
      'parameters = parameters & "," & token
      While Len(token)
        numpcb = numpcb + 1
        rs.AddNew
        rs!idOggetto = GbIdOggetto
        rs!numpcb = numpcb
        rs!pcb = token
        rs.Update
        parameters = parameters & "," & token
        token = nexttoken(statement)
      Wend
      rs.Close
      parameters = parameters & "," & token
      parameters = Right(parameters, Len(parameters) - 1)
      If Right(parameters, 1) = "," Then
        parameters = Left(parameters, Len(parameters) - 1)
      End If
      updatePsPgmEntry GbIdOggetto, GbOldNumRec, linee, parameters
    ElseIf token = "COPY" Then
      getCOPY (statement)

      ' apertura file e richiamo
    
      Set rs = m_Fun.Open_Recordset("select * from BS_oggetti where IdOggetto = " & IdOggettoRel & " and tipo = 'CPY'")
      If Not rs.EOF Then
        If Trim(rs!estensione) = "" Then
          path = m_Fun.FnPathDef & "\" & Mid(rs!Directory_Input, 2) & "\" & rs!nome
        Else
          path = m_Fun.FnPathDef & "\" & Mid(rs!Directory_Input, 2) & "\" & rs!nome & "." & rs!estensione
        End If
        endStatement = False
        rs.Close
        FDC = FreeFile
        Open path For Input As FDC
        statement = ""
       Do Until Not EOF(FDC) And endStatement
        Line Input #FDC, line
        If Mid(line, 7, 1) <> "*" Then
         line = Trim(Mid(line, 8, 65))
         If Len(line) Then
            statement = statement & " " & line
            If InStr(1, line, ".") > 0 Then
               endStatement = True
               Exit Do
            End If
         End If
       End If
      Loop
      token = nexttoken(statement) ' mangia using
      Set rs = m_Fun.Open_Recordset("select * from PsIMS_entry")
      token = nexttoken(statement)
      While Len(token)
        numpcb = numpcb + 1
        rs.AddNew
        rs!idOggetto = GbIdOggetto
        rs!numpcb = numpcb
        rs!pcb = token
        rs.Update
        token = nexttoken(statement)
      Wend
      rs.Close
      Else
        rs.Close
      End If
      
      
    End If
  Else
    line = ""
  End If
  Exit Sub
entryErr:
  'gestione errore
End Sub
'''''''''''''''''''''''''''''''''''''
' Istruzione di SCHEDULE (CALL)
' Fare meglio...
'''''''''''''''''''''''''''''''''''''
Sub parsePCB(parametri As String)
  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.psbName = nexttoken(parametri)
  
  UpdatePsdliIstruzioni (False) 'IRIS-DB
  'SQ 16-05-07
  'UpdatePsdliIstruzioni (isDC)
  'primo livello
End Sub
'''''''''''''''''''''''''''''''''''''
' Fare meglio...
'''''''''''''''''''''''''''''''''''''
Sub parseTERM(parametri As String)
  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  'TabIstr.psbName = nextToken(parametri)
  
  IsValid = True
  
  'AC (rimangno sporche)
'  TabIstr.pcb = ""
'  TabIstr.numpcb = 0
  
  UpdatePsdliIstruzioni False, False 'Chiarire isDC/isDB!
End Sub

Function CheckEOf(mainpointer As Integer, copypointer As Integer, newpointer As Integer, checktype As String) As Boolean
  If checktype = "M" Then
    If EOF(mainpointer) Then
      CheckEOf = False
    Else
      CheckEOf = True
      newpointer = mainpointer
    End If
  Else '"C"
    If EOF(copypointer) Then
      Close #copypointer
      If EOF(mainpointer) Then
        CheckEOf = False
      Else
        CheckEOf = True
        newpointer = mainpointer
        checktype = "M"
      End If
    Else
      CheckEOf = True
      newpointer = copypointer
    End If
  End If
End Function
Function TrovaParametroArray(wStringa As String, Parola() As String) As String
   
   Dim wParm As String
   Dim Valore As String
   Dim k1 As Integer, Index As Integer, k As Integer
   Dim parentesi As Integer
   TrovaParametroArray = "None"
   
   Index = 1
   
   While UCase(TrovaParametroArray) = "NONE" And Index <= UBound(Parola)
    wParm = Parola(Index)
    k1 = InStr(wStringa, wParm)
    If Not k1 = 0 Then
      Valore = Mid$(wStringa, k1 + Len(wParm))
      Select Case Mid(Valore, 1, 1)
          Case "("
           parentesi = 1
           For k = 2 To Len(Valore)
            Select Case Mid(Valore, k, 1)
              Case "("
                parentesi = parentesi + 1
              Case ")"
                parentesi = parentesi - 1
                If parentesi = 0 Then
                  Exit For
                End If
            End Select
           Next
           Valore = Left(Valore, k)
          Case Else
              k1 = InStr(Valore, " ")
              If k1 > 0 Then
                 Valore = Mid$(Valore, 1, k1 - 1)
              End If
              k1 = InStr(Valore, ",")
              If k1 > 0 Then
                 Valore = Mid$(Valore, 1, k1 - 1)
              End If
      End Select
      
      k1 = InStr(Valore, "$")
      While k1 > 0
         Mid$(Valore, k1, 1) = Space$(1)
         k1 = InStr(Valore, "$")
      Wend
      If Right(Valore, 1) = "'" Then
          Valore = Left(Valore, Len(Valore) - 1)
      End If
      TrovaParametroArray = Trim(Valore)
    End If
    Index = Index + 1
   Wend
End Function
Private Function checkField(campo As String) As String
checkField = campo
Dim appocampo As String, i As Integer
If Left(campo, 1) = "#" Then
  If IsNumeric(Right(campo, Len(campo) - 1)) Then
    checkField = campo
  Else
    appocampo = Replace(campo, "#", "")
  For i = 1 To colResWord.count
    If appocampo = colResWord.item(i) Then
      appocampo = appocampo & "RW"
      Exit For
    End If
  Next
  checkField = appocampo
  End If
Else
  appocampo = Replace(campo, "#", "")
  For i = 1 To colResWord.count
    If appocampo = colResWord.item(i) Then
      appocampo = appocampo & "RW"
      Exit For
    End If
  Next
  checkField = appocampo
End If
End Function

'''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''
Function duplicatedFMT(pIdOggetto As Long, pFMTName As String) As Boolean
  Dim rs As Recordset
  
  Set rs = m_Fun.Open_Recordset("SELECT * FROM PsDli_MFS where idOggetto = " & _
                                 pIdOggetto & " and FMTname = '" & pFMTName & "'")
  If Not rs.EOF Then
    duplicatedFMT = True
  Else
    duplicatedFMT = False
  End If
  rs.Close
End Function

Sub parseMFS()
  Dim nFBms As Integer
  Dim wRecIn As String, wRec As String
  Dim wCmp() As t_Cmp_Bms
  Dim aCmp As t_Cmp_Bms
  Dim wNomeMapSet As String, wNomeMap As String, wStr As String, firstWord As String
  Dim MacroCmd As String, wLabel As String
  Dim k1 As Integer
  Dim rs As Recordset, rsMappa As Recordset
  Dim i As Integer
  Dim linePFK As String, contPFK As String, pValore As String, pTasto As String
  Dim j As Integer
  Dim nFMfs As Variant
  Dim m_DllFunctions As New MaFndC_Funzioni
  Dim s() As String
  Dim sezioneDiv As Boolean, sezioneDev As Boolean, sezioneDpage As Boolean, sezioneMsg As Boolean
  Dim FirstDPage As Boolean, CycleFMTEnd As Boolean, divdevsalvato As Boolean
  Dim pFMTName As String
  Dim pDevType As String, pFeat As String, pDSCA As String, pSysMsg As String
  Dim pDivType As String
  Dim pCursor As String, pFill As String
  Dim MSGName As String, pMsgType As String, pSor As String, pMSGFill As String, pNxt As String
  Dim Campi As Collection, InsiemiDuplicati As Collection
  Dim ItemDuplicato As String
  Dim PrimoDuplicato As Boolean
  Dim var2segnalazione As String
  Dim nomemappa As String
  Dim Ordinale As Integer
  Dim PMSGName As String
  Dim pnomeCopy As String
  Dim copyFMT As Boolean  ' true quando trovo fmt, diventa false quando arrivo a fmtend
  Dim copyMSG As Boolean  ' true quando trovo msg, diventa false quando arrivo a msgend
  Dim FirstLPage As Boolean, CycleMSGEnd As Boolean
  Dim retFunCopy As Integer, progressivoinit As Integer, Idx As Integer
  Dim InitValue As String
  Dim Occurs As Integer
  'AC 19/7/2006
  Dim bolNewLine As Boolean, wRecInFunz As String, WRecCampo As String
  Dim posaperta As Integer, poschiusa As Integer
  Dim PfkField As String
  'AC 01/08/2006
  Dim orddpage As Integer, ordlpage As Integer, addCampo As Boolean, OrdinaleDo As Integer
  Dim sorlpage As String, sorprogLpage() As String, k As Integer
  Dim Idprimoapice As Integer, Idsecondoapice As Integer, extraapici As Integer
  Dim fpointer As Integer, checktype As String, cpypoint As Integer, copypath As String
  'AC 16/10/06
  Dim DpageName As String, ArrLAlias() As String, ArrPAlias() As String, campodpage As Collection
  Dim Do_Suff As String, campoSuff As Collection, WrecParam As String, DoParam() As String
  Dim Interlinea As String, OffsetRiga As String
  Dim ordinaleDFLD As Integer
  Dim emptyFMT As Boolean, nosegnFMT As Boolean, campoFMT As Collection, AggiornaMSG As Boolean
  Dim ColMSGUpdate As Collection, charcutpos As String, fieldtruncate1 As String, fieldtruncate2 As String
  Dim ppage As String, openmsg As Boolean, rsDict As Recordset
  Dim spcampolen As String, splindex As Integer, spattributo As String, spNomeCampo As String
  Dim lensplit As Integer, colsplit As Integer, rigasplit As Integer, mapping As Boolean, totallen As Integer, progMFLD As Integer
  Dim ArrAttAlias() As String, Equordinato As Boolean, kk As String
  Dim rsDupField As Recordset 'IRIS-DB
  Dim cnt1 As Integer 'IRIS-DB
  Dim origName As String 'IRIS-DB
  Dim currName As String 'IRIS-DB
  ReDim Preserve mEqu(0)
  ReDim Preserve sorprogLpage(0)
  
  ReDim Preserve ArrLAlias(3)
  ArrLAlias(1) = "LTH="
  ArrLAlias(2) = "LENGTH="
  ArrLAlias(3) = "L."
  ReDim Preserve ArrPAlias(3)
  ArrPAlias(1) = "POS="
  ArrPAlias(2) = "P."
  ArrPAlias(3) = "POS"
  ReDim Preserve ArrAttAlias(2)
  ArrAttAlias(1) = "ATTR="
  ArrAttAlias(2) = "A."
  progMFLD = 1
      
  Set colResWord = New Collection
  Set rsDict = m_Fun.Open_Recordset_Sys("select Parola from Sys_WordRes where Parola " _
  & "like '%[FLIOA]' or Parola like 'I-%'  or Parola like 'O-%' ")
  While Not rsDict.EOF
    If Left(rsDict!Parola, 2) = "I-" Or Left(rsDict!Parola, 2) = "O-" Then
      colResWord.Add Right(rsDict!Parola, Len(rsDict!Parola) - 2), rsDict!Parola
    Else
      colResWord.Add Left(rsDict!Parola, Len(rsDict!Parola) - 1), rsDict!Parola
    End If
    rsDict.MoveNext
  Wend
  
  m_Fun.FnConnection.Execute "delete from PsDLI_MFS Where IdOggetto =" & GbIdOggetto
  'm_Fun.FnConnection.Execute "Delete from PsDLI_MFSCmp Where IdOggetto =" & GbIdOggetto
  
  bolNewLine = True
  progressivoinit = 1
  Occurs = 1
  GbNumRec = 0
  
  AggiornaMSG = False
    
  On Error GoTo error
  
  m_Fun.FnConnection.Execute "delete from PsDLI_MFSCmp Where IdOggetto =" & GbIdOggetto
  m_Fun.FnConnection.Execute "delete from PsDLI_MFSmsgCmp Where IdOggetto =" & GbIdOggetto
  
  Set rs = m_Fun.Open_Recordset("select Mapping from BS_Oggetti where IdOggetto =" & GbIdOggetto)
  If Not rs.EOF Then
    mapping = rs!mapping
  End If
   
  Set Campi = New Collection
  Set campodpage = New Collection
  Set InsiemiDuplicati = New Collection
  Set campoSuff = New Collection
  Set campoFMT = New Collection
  Set ColMSGUpdate = New Collection
  
  '****************************  <8 **********************
  
  'AC letura parametro per troncare i campi di lunghezza 8
'  charcutpos = RetrieveParameterValues("IMSDC-FIELDMAP-CUT")(0)
'  If IsNumeric(charcutpos) Then
'    If CInt(charcutpos) > 8 Or CInt(charcutpos) < 1 Then
'       MsgBox ("Parameter IMSDC-FIELDMAP-CUT must be in 1-8 range")
'       Exit Sub
'    Exit Sub
'    End If
'  Else
'    MsgBox ("Set IMSDC parameter IMSDC-FIELDMAP-CUT with a numeric value")
'    Exit Sub
'  End If
  
  '*********************************************************
  
  'Da raffinare per ora mi interessano solo i nomi campi
  nFBms = FreeFile
  Open GbFileInput For Input As nFBms
  fpointer = nFBms
  checktype = "M"
  openmsg = False 'IRIS-DB
  origName = "" 'IRIS-DB
  
  'While Not EOF(nFBms)
  While CheckEOf(nFBms, cpypoint, fpointer, checktype) Or openmsg 'IRIS-DB - se ci sono messaggi aperti, deve ancora elaborare il messaggio; andrebbe esteso ad altri casi di fine file senza righe vuote in fondo
    wRec = ""
    If bolNewLine And Not EOF(nFBms) Then
      Line Input #fpointer, wRecIn
      'SQ 9-08-06
      If checktype = "M" Then
        GbNumRec = GbNumRec + 1
        GbOldNumRec = GbNumRec
      End If
    Else
      bolNewLine = True
    End If
    
    If Left(wRecIn, 1) <> "*" Then
      ''''''''''''''''''''''''''''
      ' costruzione Statement
      ' SQ 9-08-06
      ''''''''''''''''''''''''''''
      wRecIn = Left(wRecIn, 72)
      wRec = Left(wRecIn, 71)
      'IRIS-DB non so onestamente perch� non usi il carattere a colonna 72 per capire se il campo va a capo, ma vabbe per ora lascio cos�
      While CheckEOf(nFBms, cpypoint, fpointer, checktype) And (Len(wRecIn) >= 72 And Right(wRecIn, 1) <> " " Or Left(wRecIn, 1) = "*")
        Line Input #fpointer, wRecIn
        If checktype = "M" Then
          GbNumRec = GbNumRec + 1
          GbOldNumRec = GbNumRec
        End If
        If Left(wRecIn, 1) <> "*" Then
          'SQ - non so se posso avere commento in mezzo... male non fa!
          'IRIS-DB non deve trimmare i literal
          If Right(Trim(wRec), 1) = "," Then
            wRec = Trim(wRec) & Mid(wRecIn, 16, 56)
          Else
            wRec = wRec & Mid(wRecIn, 16, 56)
          End If
          wRecIn = Left(wRecIn, 72)
        End If
      Wend
    
      'SQ - Modificati tutti i wRecIn in wRec!
      firstWord = Trim(wRec)
      k1 = InStr(firstWord, " ")
      firstWord = Trim(Left(firstWord, k1))
      
      'SQ - Semplificare l'analisi tenendo conto delle colonne!
      'IRIS-DB condivido, gestione squallida... If k1 > 0 Or InStr(wRec, " FMTEND") > 0 Or InStr(wRec, " DPAGE") > 0 Or InStr(wRec, " LPAGE") > 0 Or InStr(wRec, " MSGEND") > 0 Or InStr(wRec, " ENDDO") > 0 Or InStr(wRec, " PFK=") > 0 Then 'AC gestione squallida mi assumo le mie resp
      If k1 > 0 Or InStr(wRec, " DIV") > 0 Or InStr(wRec, " FMTEND") > 0 Or InStr(wRec, " DPAGE") > 0 Or InStr(wRec, " LPAGE") > 0 Or InStr(wRec, " MSGEND") > 0 Or InStr(wRec, " ENDDO") > 0 Or InStr(wRec, " PFK=") > 0 Then 'IRIS-DB
        If InStr(wRec, " DFLD ") > 0 Or InStr(wRec, "DFLD ") > 0 Then
          MacroCmd = "DFLD"
        ElseIf InStr(wRec, " MFLD ") > 0 Then
          MacroCmd = "MFLD"
        ElseIf InStr(wRec, " FMT ") > 0 Or (InStr(wRec, " FMT") > 0 And Not InStr(wRec, " FMTEND") > 0) Then
          MacroCmd = "FMT"
         ElseIf (InStr(wRec, " FMTEND ") > 0 Or InStr(wRec, " FMTEND") > 0) Then
          MacroCmd = "FMTEND"
         ElseIf InStr(wRec, " MSG ") > 0 Then
          MacroCmd = "MSG"
        ElseIf InStr(wRec, " MSGEND ") > 0 Or (InStr(wRec, " MSGEND") > 0 And Trim(wRec) = "MSGEND") Then
          MacroCmd = "MSGEND"
         ElseIf InStr(wRec, " COPY ") > 0 Then
          MacroCmd = "COPY"
         ElseIf InStr(wRec, " DPAGE ") > 0 Or InStr(wRec, " DPAGE") > 0 Then
          MacroCmd = "DPAGE"
        ElseIf InStr(wRec, " LPAGE ") > 0 Or InStr(wRec, " LPAGE") > 0 Then
          MacroCmd = "LPAGE"
        ElseIf InStr(wRec, " DEV ") > 0 Or InStr(wRec, " DEV") > 0 Or InStr(wRec, "DEV ") > 0 Then
          MacroCmd = "DEV"
        ElseIf InStr(wRec, " DIV ") > 0 Or InStr(wRec, " DIV") > 0 Then
          MacroCmd = "DIV"
        ElseIf InStr(wRec, " EQU ") > 0 Or InStr(wRec, " EQU") > 0 Then
          MacroCmd = "EQU"
        ElseIf InStr(wRec, "PFK=") > 0 Then 'SQ - Spostato qui sotto: ma non � un parametro del DEV?????!!!!!!
          MacroCmd = "PFK"
        ElseIf InStr(wRec, " DO ") > 0 Then
          MacroCmd = "DO"
         ElseIf InStr(wRec, " ENDDO ") > 0 Or InStr(wRec, " ENDDO") > 0 Then
          MacroCmd = "ENDDO"
       Else
          MacroCmd = ""
       End If
    Else
       MacroCmd = ""
    End If
    
    Select Case MacroCmd
      Case "EQU"
        s = Split(m_DllFunctions.Elimina_Space_Da_Riga(wRec), " ")
        ReDim Preserve mEqu(UBound(mEqu) + 1)
        mEqu(UBound(mEqu)).cmd = s(2)
        mEqu(UBound(mEqu)).Alias = s(0)
        Select Case mEqu(UBound(mEqu)).cmd
          Case "POS=", "'POS='"
            ReDim Preserve ArrPAlias(UBound(ArrPAlias) + 1)
            ArrPAlias(UBound(ArrPAlias)) = mEqu(UBound(mEqu)).Alias
          Case "LTH=", "'LTH='"
            ReDim Preserve ArrLAlias(UBound(ArrLAlias) + 1)
            ArrLAlias(UBound(ArrLAlias)) = mEqu(UBound(mEqu)).Alias
          Case "ATTR=", "'ATTR='"
            ReDim Preserve ArrAttAlias(UBound(ArrAttAlias) + 1)
            ArrAttAlias(UBound(ArrAttAlias)) = mEqu(UBound(mEqu)).Alias
        End Select
    
      Case "DO"
        Interlinea = 1
        OffsetRiga = 0
        If Len(Trim(wRec)) > 50 Then
          wRec = Left(wRec, 50)
        End If
        If IsNumeric(Trim(Mid(Trim(wRec), 3, Len(wRec) - 2))) And Not InStr(1, Trim(Mid(Trim(wRec), 3, Len(wRec) - 2)), ",") > 0 Then
          Occurs = Trim(Mid(Trim(wRec), 3, Len(wRec) - 2))  ' TO CHECK
        ElseIf IsNumeric(Trim(Mid(Trim(wRec), 3, InStr(3, Trim(wRec), ",") - 3))) Then
          wRec = Trim(wRec)
          WrecParam = Trim(Right(wRec, Len(wRec) - 2))
          DoParam = Split(WrecParam, ",")
          'Occurs = Trim(Mid(Trim(wRec), 3, InStr(3, Trim(wRec), ",") - 3))
          Occurs = DoParam(0)
          If UBound(DoParam) > 0 Then
            If IsNumeric(DoParam(1)) Then
              Interlinea = DoParam(1)
              If UBound(DoParam) > 1 Then
                If IsNumeric(DoParam(2)) Then
                  OffsetRiga = DoParam(2)
                End If
              End If
            Else
              If IsNumeric(DoParam(2)) Then
                OffsetRiga = DoParam(2)
              End If
            End If
          End If
        End If
        OrdinaleDo = OrdinaleDo + 1
        Do_Suff = findParameter(wRec, "SUF")
        If Do_Suff = "" Then Do_Suff = "01"
      Case "ENDDO"
        Occurs = 1
        Do_Suff = ""
      Case "DEV"
        'SQ - spostati
        'orddpage = 0
        'ordlpage = 0
        ReDim Preserve sorprogLpage(0)
        pDevType = findParameter(wRec, "TYPE")
        i = InStr(wRec, "PFK=")
        If i Then
          getPFK Mid(wRec, i), pDevType, pFMTName
        End If
        sezioneDev = True
        
        pFeat = findParameter(wRec, "FEAT")
        pDSCA = findParameter(wRec, "DSCA")
        pSysMsg = findParameter(wRec, "SYSMSG")
        ppage = findParameter(wRec, "PAGE")
        
        '******** PFK
       
        PfkField = ""
        linePFK = findParameter(wRec, "PFK")
        'linePFK = Analizza_PFK(wRec)
        If Len(linePFK) Then
          linePFK = Mid(linePFK, 2, Len(linePFK) - 2)
          m_Fun.FnConnection.Execute "DELETE FROM PsDLI_MFSPfk WHERE FMTNAme ='" & pFMTName & "'"
          
          PfkField = Left(linePFK, InStr(linePFK, ",") - 1)
          
          linePFK = Right(linePFK, Len(linePFK) - (Len(PfkField) + 1))
          Set rs = m_Fun.Open_Recordset("Select * From PsDLI_MFSPfk")
          linePFK = Trim(linePFK)
          If Not Left(linePFK, 1) = "'" Then ' coppie tasto valore
            While InStr(1, linePFK, ",") > 0
                  If CoppiaTastoValore(Mid(linePFK, 1, InStr(1, linePFK, ",") - 1), pTasto, pValore) Then
                    rs.AddNew
                    rs!idOggetto = GbIdOggetto
                    rs!fmtName = pFMTName
                    If pFMTName = "" Then
                      emptyFMT = True
                    End If
                    rs!Dev = pDevType
                    rs!tasto = pTasto
                    rs!Valore = Trim(pValore)
                    rs.Update
                  End If
                  linePFK = Mid(linePFK, InStr(1, linePFK, ",") + 1, Len(linePFK) - InStr(1, linePFK, ","))
            Wend
            If CoppiaTastoValore(linePFK, pTasto, pValore) Then
              rs.AddNew
              rs!idOggetto = GbIdOggetto
              rs!fmtName = pFMTName
              If pFMTName = "" Then
                emptyFMT = True
              End If
              rs!Dev = pDevType
              rs!tasto = pTasto
              rs!Valore = Trim(pValore)
              rs.Update
            End If
          Else   ' singoli
            Dim progressivo As Integer
            progressivo = 0
            While (InStr(1, linePFK, ",")) > 0
                  If Not linePFK = "," Then
                    progressivo = progressivo + 1
                    rs.AddNew
                    rs!idOggetto = GbIdOggetto
                    rs!fmtName = pFMTName
                    If pFMTName = "" Then
                      emptyFMT = True
                    End If
                    rs!Dev = pDevType
                    rs!tasto = progressivo
                    rs!Valore = Trim(pulisciCommentiPFK(Left(linePFK, InStr(1, linePFK, ",") - 1)))
                    rs.Update
                  End If
                  linePFK = Mid(linePFK, InStr(1, linePFK, ",") + 1, Len(linePFK) - InStr(1, linePFK, ","))
            Wend
            If Not linePFK = "" Then
              progressivo = progressivo + 1
              rs.AddNew
              rs!idOggetto = GbIdOggetto
              rs!fmtName = pFMTName
              If pFMTName = "" Then
                emptyFMT = True
              End If
              rs!Dev = pDevType
              rs!tasto = progressivo
              rs!Valore = Trim(pulisciCommentiPFK(linePFK))
              rs.Update
            End If
          End If
          rs.Close
        End If
      Case "DIV"
        sezioneDiv = True
        sezioneDev = False
        pDivType = findParameter(wRec, "TYPE")
      Case "DPAGE"
        orddpage = orddpage + 1
        If InStr(1, wRec, "DPAGE ") Then
        DpageName = Trim(Left(wRec, InStr(1, wRec, "DPAGE ") - 1))
        Else
          DpageName = Trim(Left(wRec, InStr(1, wRec, "DPAGE") - 1))
        End If
        pCursor = findParameter(wRec, "CURSOR")
        pFill = findParameter(wRec, "FILL")
        If Not FirstDPage Then
          FirstDPage = True
          sezioneDpage = True
          pCursor = findParameter(wRec, "CURSOR")
          pFill = findParameter(wRec, "FILL")
          If sezioneDiv Then
            sezioneDiv = False
                '***** ex case fmtend
                    
            Set rs = m_Fun.Open_Recordset("select * from PsDLI_MFS")
            rs.AddNew
            rs!idOggetto = GbIdOggetto
            rs!fmtName = pFMTName
            If pFMTName = "" Then
              emptyFMT = True
            End If
            rs!DevType = pDevType
            rs!Feat = pFeat
            rs!Page = ppage
            rs!DSCA = pDSCA
            rs!Sysmsg = pSysMsg
            rs!DivType = pDivType
            rs!Cursor = pCursor
            rs!Fill = pFill
            rs.Update
            rs.Close
            'pDevType = ""
            pFeat = ""
            ppage = ""
            pDSCA = ""
            pSysMsg = ""
            pDivType = ""
            pCursor = ""
            pFill = ""
          End If
        Else
          'CycleFMTEnd = True
          'While Not EOF(nFMfs) And CycleFMTEnd
            'Line Input #nFMfs, wRecIn
            'If InStr(wRecIn, "FMTEND") > 0 Then
              'CycleFMTEnd = False
            'End If
          'Wend
        End If
       Case "LPAGE"
        sorlpage = findParameter(wRec, "SOR")
        'SQ
        For k = 1 To UBound(sorprogLpage)
          If sorprogLpage(k) = sorlpage Then
            ordlpage = k
            Exit For
          End If
        Next
        If k > UBound(sorprogLpage) Then
          ordlpage = UBound(sorprogLpage) + 1
          ReDim Preserve sorprogLpage(UBound(sorprogLpage) + 1)
          sorprogLpage(UBound(sorprogLpage)) = IIf(sorlpage = "", "NNNN", sorlpage)
        End If
        Set rs = m_Fun.Open_Recordset("SELECT * FROM PsMFS_MSG_LPAGE where idOggetto =" _
                                      & GbIdOggetto & " and msg = '" & PMSGName & "' and ordinale =" _
                                      & ordlpage)
        If rs.EOF Then
          rs.AddNew
          rs!idOggetto = GbIdOggetto
          rs!msg = PMSGName
          rs!Ordinale = ordlpage
          rs!SOR = EliminaParentesi(sorprogLpage(UBound(sorprogLpage)))
          
          '''''''''''''''''''''''''''''''''''''
          ' Gestione COND per il "MultiPaging"
          ' SQ 8-09-06 - brutto... completare...
          '''''''''''''''''''''''''''''''''''''
          wStr = findParameter(wRec, "COND")
          If Len(wStr) Then
            'wStr = Replace(Replace(wStr, ")", ""), "(", "")
            wStr = Mid(wStr, 2, Len(wStr) - 2)
            Dim condParam() As String
            condParam = Split(wStr, ",")
            If UBound(condParam) = 2 Then
              rs!COND_Field = Trim(condParam(0))
              rs!COND_Op = Trim(condParam(1))
              rs!COND_Value = Trim(condParam(2))
            Else
              m_Fun.writeLog "parseMFS - " & sorlpage & " - incorrect COND: " & wStr
            End If
          End If
          rs.Update
        End If
        rs.Close
       Case "FMTEND"
         copyFMT = False
       Case "FMT"
        If Not Equordinato Then
          j = 1
          While j > 0
            j = 0
            For i = 1 To UBound(mEqu) - 1
        ' Confronto gli elementi a coppie 1-2, 2-3, 3-4 ecc...
              If Len(mEqu(i).Alias) < Len(mEqu(i + 1).Alias) Then
                j = j + 1
                ' Se non sono in sequenza
                ' Scambio il primo valore col successivo
                kk = mEqu(i + 1).Alias
                mEqu(i + 1).Alias = mEqu(i).Alias
                mEqu(i).Alias = kk
                kk = mEqu(i + 1).cmd
                mEqu(i + 1).cmd = mEqu(i).cmd
                mEqu(i).cmd = kk
              End If
            Next i
          Wend
          Equordinato = True
        End If
        FirstDPage = True
        'SQ
        orddpage = 0
          If Trim(wLabel) <> "" Then
            wNomeMapSet = wLabel
          End If
          AggiungiComponente wNomeMapSet, wNomeMapSet, "MPS", "MAPMFS"
          pFMTName = firstWord
          If duplicatedFMT(GbIdOggetto, pFMTName) Then
          
            If AggiornaMSG Then ' se ci sono msg sopra che facevano riferimento a questo blocco fmt li elimino
              For i = 1 To ColMSGUpdate.count
                m_Fun.FnConnection.Execute "Delete PsDLI_MFSmsgCmp  where " _
                & " IdOggetto =" & GbIdOggetto & " and MSGName ='" & ColMSGUpdate.item(i) & "'"
                m_Fun.FnConnection.Execute "Delete PsDLI_MFSmsg  where " _
                & " IdOggetto =" & GbIdOggetto & " and MSGName ='" & ColMSGUpdate.item(i) & "'"
              Next
            End If
            Exit Sub
          End If
          If AggiornaMSG Then
            For i = 1 To ColMSGUpdate.count
              m_Fun.FnConnection.Execute "Update PsDLI_MFSmsgCmp set FMTName = '" & pFMTName & "' where " _
              & " IdOggetto =" & GbIdOggetto & " and MSGName ='" & ColMSGUpdate.item(i) & "'"
              m_Fun.FnConnection.Execute "Update PsDLI_MFSmsg set FMTName = '" & pFMTName & "' where " _
              & " IdOggetto =" & GbIdOggetto & " and MSGName ='" & ColMSGUpdate.item(i) & "'"
            Next
            Set ColMSGUpdate = New Collection
          End If
          
          copyFMT = True
          OrdinaleDo = 0
       Case "MSG"
        If openmsg Then
          copyMSG = False
          'm_Fun.FnConnection.Execute "delete from PsDLI_mfsMSG Where IdOggetto =" & GbIdOggetto & " and MSGName ='" & PMSGName & "'"
          Set rs = m_Fun.Open_Recordset("select * from PsDLI_mfsMSG")
          rs.AddNew
          rs!fmtName = pFMTName
          If pFMTName = "" Then
            emptyFMT = True
            AggiornaMSG = True
            ColMSGUpdate.Add PMSGName
          End If
          rs!idOggetto = GbIdOggetto
          rs!MSGName = PMSGName
          rs!DevType = pMsgType
          rs!SOR = pSor
          rs!Fill = pMSGFill
          rs!Nxt = pNxt
          rs.Update
          rs.Close
          If pMsgType = "OUTPUT" Then
            Parser.updateRelations PMSGName, GbTipoInPars
          End If
          MSGName = ""
          pMsgType = ""
          pSor = ""
          pMSGFill = ""
          pNxt = ""
        
        
        End If
        'SQ
        ordlpage = 0
        FirstLPage = False
        PMSGName = firstWord
        copyMSG = True
        Ordinale = 0
        OrdinaleDo = 0
        openmsg = True
        
        'SQ - brasare tutto a inizio parse!
        m_Fun.FnConnection.Execute "DELETE FROM PsDLI_mfsMSG WHERE IdOggetto =" & GbIdOggetto & " and MSGName ='" & PMSGName & "'"
        m_Fun.FnConnection.Execute "DELETE FROM PsMFS_MSG_LPAGE WHERE IdOggetto =" & GbIdOggetto & " and MSG ='" & PMSGName & "'"

        ReDim Preserve sorprogLpage(0)
        pMsgType = findParameter(wRec, "TYPE")
        wNomeMap = wLabel
        MSGName = wNomeMap
        If pMsgType = "INPUT" Then
          AggiungiComponente wNomeMapSet, wNomeMap, "MAP", "MAPIN"
        ElseIf pMsgType = "OUTPUT" Then
          AggiungiComponente wNomeMapSet, wNomeMap, "MAP", "MAPOUT"
        End If
        pSor = findParameter(wRec, "SOR")
        pMSGFill = findParameter(wRec, "FILL")
        pNxt = findParameter(wRec, "NXT")
        If Not pSor = "" And Not pFMTName = "" Then
          If Not InStr(pSor, pFMTName) > 0 Then
          ' sto facendo riferimento ad una FMT che non � quella immediatamente precedente
            If Left(pSor, 1) = "(" And Right(pSor, 1) = ")" Then
              If InStr(pSor, ",") > 0 Then
                pFMTName = Mid(pSor, 2, InStr(pSor, ",") - 2)
              Else
                pFMTName = Mid(pSor, 2, Len(pSor) - 2)
              End If
            Else
              If InStr(pSor, ",") > 0 Then
                pFMTName = Left(pSor, InStr(pSor, ",") - 1)
              Else
                pFMTName = pSor
              End If
            End If
          End If
        End If
        
       Case "MSGEND"
          copyMSG = False
          openmsg = False
          'm_Fun.FnConnection.Execute "delete from PsDLI_mfsMSG Where IdOggetto =" & GbIdOggetto & " and MSGName ='" & PMSGName & "'"
          Set rs = m_Fun.Open_Recordset("select * from PsDLI_mfsMSG")
          rs.AddNew
          rs!fmtName = pFMTName
          If pFMTName = "" Then
            emptyFMT = True
            AggiornaMSG = True
            ColMSGUpdate.Add PMSGName
          End If
          rs!idOggetto = GbIdOggetto
          rs!MSGName = PMSGName
          rs!DevType = pMsgType
          rs!SOR = pSor
          rs!Fill = pMSGFill
          rs!Nxt = pNxt
          rs.Update
          rs.Close
          If pMsgType = "OUTPUT" Then
            Parser.updateRelations PMSGName, GbTipoInPars
          End If
          MSGName = ""
          pMsgType = ""
          pSor = ""
          pMSGFill = ""
          pNxt = ""
       Case "COPY"
          pnomeCopy = Trim(Mid(wRec, InStr(wRec, "COPY") + 4, 10))
'         If copyFMT Then  'la copy deve essere esplosa perch� contiene i campi
'              retFunCopy = writeCampiCopyMFS(pnomeCopy, Campi, pDevType)
'         ElseIf copyMSG Then
'              retFunCopy = writeCampiCopymsgMFS(pnomeCopy, PMSGName, Ordinale)
'         End If
          If checkCopyMFS(pnomeCopy, copypath) Then
            checktype = "C"
            cpypoint = FreeFile
            Open copypath For Input As cpypoint
            fpointer = cpypoint
          End If
        Case "DFLD"
           ordinaleDFLD = ordinaleDFLD + 1
          'E' un campo, recupera le informazioni
          If Not (firstWord = "DFLD") Then
            aCmp.nomeCmp = firstWord
            'IRIS-DB: checks if same name used in the same MFS in another FMT
            origName = aCmp.nomeCmp
            If Not aCmp.nomeCmp = "" And Not InStr(aCmp.nomeCmp, "LTH=") > 0 Then
              Set rsDupField = m_Fun.Open_Recordset("SELECT * FROM PsDLI_MFScmp where idOggetto = " & GbIdOggetto & " and Nome = '" & Trim(aCmp.nomeCmp) & "'") 'IRIS-DB
              If Not rsDupField.EOF Then 'IRIS-DB
                cnt1 = rsDupField.RecordCount + 1 'IRIS-DB
                'IRIS-DB: deve ciclare fino a che non trova un nome che non esista gi�
                rsDupField.Close 'IRIS-DB
                currName = aCmp.nomeCmp 'IRIS-DB
                aCmp.nomeCmp = Trim(aCmp.nomeCmp) & CStr(cnt1) 'IRIS-DB
                Set rsDupField = m_Fun.Open_Recordset("SELECT * FROM PsDLI_MFScmp where idOggetto = " & GbIdOggetto & " and Nome = '" & Trim(aCmp.nomeCmp) & "'") 'IRIS-DB
                While Not rsDupField.EOF
                  cnt1 = cnt1 + 1 'IRIS-DB
                  aCmp.nomeCmp = currName & CStr(cnt1) 'IRIS-DB
                  Set rsDupField = m_Fun.Open_Recordset("SELECT * FROM PsDLI_MFScmp where idOggetto = " & GbIdOggetto & " and Nome = '" & Trim(aCmp.nomeCmp) & "'") 'IRIS-DB
                Wend
              End If 'IRIS-DB
              rsDupField.Close 'IRIS-DB
            End If 'IRIS-DB
          Else
            'NON HA LABEL
            aCmp.nomeCmp = "#" & format(progressivoinit, "0000")
            progressivoinit = progressivoinit + 1
            
            'VALORE INIZIALE: SBAGLIA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            'POSSO AVERE VIRGOLE NEL VALORE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            'AC 18/09/06
            extraapici = 0
            Idprimoapice = InStr(InStr(1, wRec, "DFLD") + 4, wRec, "'")
            Idsecondoapice = InStr(Idprimoapice + 1, wRec, "'")
            While Mid(wRec, Idsecondoapice, 2) = "''"
              extraapici = extraapici + 1
              Idsecondoapice = InStr(Idsecondoapice + 2, wRec, "'")
            Wend
            '*******
            Idx = InStr(Idsecondoapice + 1, wRec, ",")
            'AC oppure fino ad x
            If Idx > 0 Then
              InitValue = Mid(wRec, Idprimoapice, Idx - Idprimoapice)
              ' Cancello da stringa valore iniziale per evitare ambiguit�
              ' con ricerca degli altri campi
            ElseIf InStr(16, wRec, "X") > 0 Then
              InitValue = Mid(wRec, 16, InStr(16, wRec, "X") - 16)
            End If
            wRec = Replace(wRec, InitValue, "")
            'InitValue = Replace(InitValue, "'", "")
            InitValue = Mid(InitValue, 2, Len(InitValue) - 2)
          End If
          'POSIZIONE:
          Dim pos As String
          pos = "None"
          'SQ - Ho costruito all'inizio l'intero statement!
          ' => pulire qui sotto...
          Do While UCase(pos) = "NONE"   ' AC 06-06-20  pos pu� essere su righe sotto
            'pos = Trim(Replace(Replace(TrovaParametro(Replace(Replace(wRec, "P.", "POS="), "P(", "POS=("), "POS"), ")", ""), "(", ""))
            ' Se ho una riga con 1 o 2 caratteri (ES: "*"), la RIGHT andava in errore
            If InStr(1, wRec, "DFLD") < 3 Then
              pos = Trim(Replace(Replace(TrovaParametroArray(Right(wRec, Len(wRec)), ArrPAlias), ")", ""), "(", ""))
            Else
              pos = Trim(Replace(Replace(TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "DFLD") - 3), ArrPAlias), ")", ""), "(", ""))
            End If
            If UCase(pos) <> "NONE" And pos <> "" Then
               If IsNumeric(Mid(pos, InStr(pos, ",") + 1)) Then
                aCmp.PosX = Mid(pos, InStr(pos, ",") + 1)
               End If
               If IsNumeric(Left(pos, InStr(pos, ",") - 1)) Then
                aCmp.PosY = Left(pos, InStr(pos, ",") - 1)
               End If
            Else
              'SQ Non deve pi� entrare qui!
              If Not EOF(fpointer) Then
                Line Input #fpointer, wRecIn
                If checktype = "M" Then
                  GbNumRec = GbNumRec + 1
                  GbOldNumRec = GbNumRec
                End If
              Else
                Exit Do
              End If
              'Aggiungo questo per sicurezza
              wRec = wRecIn
            End If
            ' leggo un altra riga
          Loop
          
          'LUNGHEZZA:
          'pos = TrovaParametro(Replace(Replace(wRec, "L.", "LENGTH="), "LTH=", "LENGTH="), "LENGTH")
          pos = TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "DFLD") - 3), ArrLAlias)
          If UCase(pos) <> "NONE" Then
            aCmp.Length = pos
          Else
            aCmp.Length = Len(InitValue) - extraapici
          End If
                    
          If Len(aCmp.nomeCmp) = 8 Or Len(aCmp.nomeCmp) = 7 Then
            Campi.Add aCmp.nomeCmp
            campodpage.Add orddpage
            campoSuff.Add Do_Suff
            campoFMT.Add pFMTName
          End If
          '*********** <8 **********
'          If Len(aCmp.NomeCmp) = 8 Then
'            aCmp.NomeCmp = Left(aCmp.NomeCmp, CInt(charcutpos) - 1) & Right(aCmp.NomeCmp, 8 - CInt(charcutpos))
'          End If
          '****************************
                    
          Set rs = m_Fun.Open_Recordset("Select * From PsDLI_MFSCmp")
'IRIS-DB inizio - non splitta pi�
''''          If aCmp.Length > 80 And Not InStr(pDevType, "3270P") > 0 And Not mapping Then
''''            spcampolen = aCmp.Length
''''            colsplit = aCmp.PosX
''''            rigasplit = aCmp.PosY
''''            splindex = 1
''''            totallen = spcampolen
''''
''''            If UBound(mEqu) Then
''''              For i = 1 To UBound(mEqu)
''''                If InStr(wRec, mEqu(i).Alias) Then
''''                  If UCase(TrovaParametro(mEqu(i).cmd, "ATTR")) <> "NONE" And Not (mEqu(i).cmd = "'ATTR='") Then
''''                    rs!Attributo = ControllaAttributo(TrovaParametro(mEqu(i).cmd, "ATTR"))
''''                    Exit For
''''                  End If
''''                End If
''''              Next i
''''              If i > UBound(mEqu) Then
''''                spattributo = ControllaAttributo(TrovaParametro(wRec, "ATTR"))
''''              End If
''''            Else
''''              spattributo = ControllaAttributo(TrovaParametro(wRec, "ATTR"))
''''            End If
''''
''''            While totallen > 0
''''              spNomeCampo = checkField(aCmp.nomeCmp) & "SP" & splindex
''''              If CInt(totallen) > 79 Then
''''                lensplit = 81 - colsplit
''''              Else
''''                lensplit = totallen
''''              End If
''''
''''             totallen = totallen - lensplit
''''
''''             '*************** SALVATAGGIO
''''             rs.AddNew
''''            rs!FMTName = pFMTName
''''            If pFMTName = "" Then
''''              emptyFMT = True
''''            End If
''''            rs!idOggetto = GbIdOggetto
''''            rs!nome = checkField(spNomeCampo)
''''            ' AC sostituisce FMT type
''''            rs!DevType = pDevType
''''            rs!Do_Suff = Do_Suff
''''            rs!Riga = rigasplit
''''            rs!Colonna = colsplit
''''            rs!Lunghezza = lensplit
''''
''''            'rs!InitValue = InitValue
''''            rs!Occurs = Occurs
''''            rs!OrdinaleCmp = ordinaleDFLD
'''''            If Occurs > 1 Then
'''''              rs!Interlinea = Interlinea
'''''              rs!OffsetRiga = OffsetRiga
'''''              rs!OrdinaleDo = OrdinaleDo
'''''            End If
''''            If orddpage = 0 Then orddpage = 1
''''            rs!DPage = orddpage
''''            rs!DpageName = DpageName
''''            rs.Update
''''
''''
''''
''''             ordinaleDFLD = ordinaleDFLD + 1
''''             colsplit = 1
''''             splindex = splindex + 1
''''             rigasplit = rigasplit + 1
''''            Wend
''''            ordinaleDFLD = ordinaleDFLD - 1
''''            rs.Close
''''          Else
            rs.AddNew
            rs!fmtName = pFMTName
            If pFMTName = "" Then
              emptyFMT = True
            End If
            rs!idOggetto = GbIdOggetto
            rs!nome = checkField(aCmp.nomeCmp)
            ' AC sostituisce FMT type
            rs!DevType = pDevType
            rs!Do_Suff = Do_Suff
            rs!Riga = aCmp.PosY
            rs!Colonna = aCmp.PosX
            rs!Lunghezza = aCmp.Length
            'SQ tmp: max len value?
            rs!InitValue = InitValue
            rs!Occurs = Occurs
            rs!OrdinaleCmp = ordinaleDFLD
            If Occurs > 1 Then
              rs!Interlinea = Interlinea
              rs!OffsetRiga = OffsetRiga
              rs!OrdinaleDo = OrdinaleDo
            End If
            If orddpage = 0 Then orddpage = 1
            rs!DPage = orddpage
            rs!DpageName = DpageName
          'SQ 3-05-06 - mettere a posto 'sta roba...
          'Usa solo le costanti e non gli statement normali!
          If UBound(mEqu) Then
            For i = 1 To UBound(mEqu)
              If InStr(aCmp.nomeCmp, mEqu(i).Alias) Then wRec = Replace(wRec, aCmp.nomeCmp, "")
              If InStr(wRec, mEqu(i).Alias) Then
                If UCase(TrovaParametro(mEqu(i).cmd, "ATTR")) <> "NONE" And Not (mEqu(i).cmd = "'ATTR='") Then
                  rs!Attributo = ControllaAttributo(TrovaParametro(mEqu(i).cmd, "ATTR"))
                  Exit For
                End If
              End If
            Next
            If i > UBound(mEqu) Then
              rs!Attributo = ControllaAttributo(TrovaParametroArray(wRec, ArrAttAlias))
            End If
          Else
            rs!Attributo = ControllaAttributo(TrovaParametroArray(wRec, ArrAttAlias))
          End If
          rs!origName = origName 'IRIS-DB
          rs.Update
          rs.Close
          origName = "" 'IRIS-DB
          InitValue = ""
'''''      End If
'IRIS-DB fine
      
      '**** DA parseMFS ****
        sezioneDpage = False
          If sezioneDiv Then
            sezioneDiv = False
                  '***** ex case fmtend
                  
            Set rs = m_Fun.Open_Recordset("select * from PsDLI_MFS")
            rs.AddNew
            rs!idOggetto = GbIdOggetto
            rs!fmtName = pFMTName
            If pFMTName = "" Then
              emptyFMT = True
            End If
            rs!DevType = pDevType
            rs!Feat = pFeat
            rs!Page = ppage
            rs!DSCA = pDSCA
            rs!Sysmsg = pSysMsg
            rs!DivType = pDivType
            rs!Cursor = pCursor
            rs!Fill = pFill
            rs.Update
            rs.Close
            'pDevType = ""
            pFeat = ""
            ppage = ""
            pDSCA = ""
            pSysMsg = ""
            pDivType = ""
            pCursor = ""
            pFill = ""
          End If
    Case "MFLD"
      sezioneMsg = False
      aCmp.Length = 0 'IRIS-DB prendeva quello del precedente come default, almeno inizializza (da rivedere anche per altri campi)
      'Mauro - 10-03-06
      'ha solo la lunghezza...
      If InStr(wRec, ",") Then
        aCmp.nomeCmp = Trim(Mid(wRec, InStr(wRec, "MFLD") + 4, InStr(wRec, ",") - InStr(wRec, "MFLD") - 4))
        'IRIS-DB: rimessa: la usa come FLAG!!!!! aCmp.nomeCmp = Replace(aCmp.nomeCmp, "(", "") 'IRIS-DB: rimuove la parentesi; ad essere sinceri il parser delle MFS andrebbe rifatto da zero, non � complicato
       'IRIS-DB: checks if same name used in the same MFS in another FMT
        If Not aCmp.nomeCmp = "" And Not InStr(aCmp.nomeCmp, "LTH=") > 0 Then
          Set rsDupField = m_Fun.Open_Recordset("SELECT * FROM PsDLI_MFSCmp where idOggetto = " & GbIdOggetto & " and FMTName = '" & pFMTName & "' and OrigName = '" & Trim(aCmp.nomeCmp) & "'")
          If Not rsDupField.EOF Then
            aCmp.nomeCmp = rsDupField!nome
          End If 'IRIS-DB
          rsDupField.Close 'IRIS-DB
        End If 'IRIS-DB
        'IRIS-DB: pezza a colori per l'inconsistenza del parser MFS - non funziona quando manca LTH, troppo tempo per riscriverlo
        If InStr(wRec, "'") > 0 And Not InStr(wRec, "LTH") > 0 Then
          Set rsDupField = m_Fun.Open_Recordset("SELECT * FROM PsDLI_MFSCmp where idOggetto = " & GbIdOggetto & " and FMTName = '" & pFMTName & "' and Nome = '" & Trim(Replace(aCmp.nomeCmp, "(", "")) & "'")
          If Not rsDupField.EOF Then
            wRec = RTrim(wRec) & ",LTH=" & rsDupField!Lunghezza
          End If 'IRIS-DB
          rsDupField.Close 'IRIS-DB
        End If
        WRecCampo = wRec  ' su WRecCampo successivamente controllo
                            ' che campo non sia fra parentesi ed abbia value
        'If TrovaParametro(Replace(wRec, "L.", "LTH="), "LTH") <> "none" Then
        If UCase(TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "MFLD") - 3), ArrLAlias)) <> "NONE" Or InStr(wRec, "'") > 0 Then
          aCmp.Length = TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "MFLD") - 3), ArrLAlias)
        Else   'campo generato da funzione
          wRecInFunz = wRec
            
          Line Input #fpointer, wRecIn
          If checktype = "M" Then
            GbNumRec = GbNumRec + 1
            GbOldNumRec = GbNumRec
          End If
          If InStr(wRecIn, " MFLD ") > 0 Then
          ' ho sconfinato nel campo successivo
            ' non leggo nel  giro successivo
            bolNewLine = False
            ' continuo con la linea vecchia
            wRec = wRecInFunz
          Else
            wRec = wRecIn
          End If
          
          'SQ ??? legge e basta? non conta righe, non trimma niente? boh...
          'Aggiungo questo, ma... boh?!
          
          ' cerco nella riga successiva se non � un altro MFLD
          If UCase(TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "MFLD") - 3), ArrLAlias)) <> "NONE" And Not InStr(wRec, " MFLD ") > 0 Then
            aCmp.Length = TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "MFLD") - 3), ArrLAlias)
          ElseIf UCase(TrovaParametro(Replace(wRec, "A.", "ATTR="), "ATTR")) <> "NONE" Then
            aCmp.attrib = TrovaParametro(Replace(wRec, "A.", "ATTR="), "ATTR")
            If UCase(TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "MFLD") - 3), ArrLAlias)) <> "NONE" Then
              aCmp.Length = TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "MFLD") - 3), ArrLAlias)
            Else
              Line Input #fpointer, wRecIn
              If checktype = "M" Then
                GbNumRec = GbNumRec + 1
                GbOldNumRec = GbNumRec
              End If
              'SQ ??? legge e basta? non conta righe, non trimma niente? boh...
              'Aggiungo questo, ma... boh?!
              wRec = wRecIn
              If UCase(TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "MFLD") - 3), ArrLAlias)) <> "NONE" Then
                aCmp.Length = TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "MFLD") - 3), ArrLAlias)
              End If
            End If
          Else
            ' gestiamo il caso in cui prima attributo
            
            bolNewLine = False
            'AC 17/7/2006 prima la stessa gestione se non trovava length
            wRecInFunz = Trim(Mid(wRecInFunz, InStr(wRecInFunz, "MFLD") + 4, Len(wRecInFunz) - InStr(wRecInFunz, "MFLD") - 4))
''''            If InStr(wRecInFunz, "    ") Then
''''              wRecInFunz = Trim(Mid(wRecInFunz, 1, InStr(wRecInFunz, "    ")))
''''            End If
            'Ricerca di campi definiti da funzioni
            If Left(wRecInFunz, 1) = "(" Then  'assumiamo che campo sia definito da funzione nel formato (campo,funzione)
              If Right(wRecInFunz, 1) = ")" Then
                wRecInFunz = Mid(wRecInFunz, 1, Len(wRecInFunz) - 1)
              End If
              aCmp.nomeCmp = Mid(wRecInFunz, 2, InStr(wRecInFunz, ",") - 2)
             'IRIS-DB: checks if same name used in the same MFS in another FMT
              If Not aCmp.nomeCmp = "" And Not InStr(aCmp.nomeCmp, "LTH=") > 0 Then
                Set rsDupField = m_Fun.Open_Recordset("SELECT * FROM PsDLI_MFSCmp where idOggetto = " & GbIdOggetto & " and FMTName = '" & pFMTName & "' and OrigName = '" & Trim(aCmp.nomeCmp) & "'")
                If Not rsDupField.EOF Then
                  aCmp.nomeCmp = rsDupField!nome
                End If 'IRIS-DB
                rsDupField.Close 'IRIS-DB
              End If
              aCmp.Function = Mid(wRecInFunz, InStr(wRecInFunz, ",") + 1, Len(wRecInFunz) - InStr(wRecInFunz, ","))
              aCmp.Function = Replace(aCmp.Function, "'", "")
              m_Fun.FnConnection.Execute "update PsDLI_MFSCmp set Funzione = '" & aCmp.Function _
                                      & "' where Nome = '" & checkField(aCmp.nomeCmp) & "'  and FMTName = '" & pFMTName & "'"
            End If
          End If
        End If
        If Left(aCmp.nomeCmp, 1) = "(" Then
          aCmp.nomeCmp = Right(aCmp.nomeCmp, Len(aCmp.nomeCmp) - 1)
        'IRIS-DB: checks if same name used in the same MFS in another FMT
          If Not aCmp.nomeCmp = "" And Not InStr(aCmp.nomeCmp, "LTH=") > 0 Then
            Set rsDupField = m_Fun.Open_Recordset("SELECT * FROM PsDLI_MFSCmp where idOggetto = " & GbIdOggetto & " and FMTName = '" & pFMTName & "' and OrigName = '" & Trim(aCmp.nomeCmp) & "'")
            If Not rsDupField.EOF Then
              aCmp.nomeCmp = rsDupField!nome
            End If 'IRIS-DB
            rsDupField.Close 'IRIS-DB
          End If 'IRIS-DB
          WRecCampo = Replace(WRecCampo, aCmp.nomeCmp & ",", "")
          posaperta = InStr(1, WRecCampo, "('")
          poschiusa = InStr(1, WRecCampo, "')")
          If posaperta > 0 And poschiusa > 0 Then ' stringa
            aCmp.INITIAL = "'" & Mid(WRecCampo, posaperta + 2, poschiusa - posaperta - 2) & "'"
          Else ' cerco un numerico
            posaperta = InStr(1, WRecCampo, "(")
            poschiusa = InStr(1, WRecCampo, ")")
            If posaperta > 0 And poschiusa > 0 Then ' numero
              aCmp.INITIAL = Mid(WRecCampo, posaperta + 1, poschiusa - posaperta - 1)
            End If
          End If
        End If
        For i = 1 To UBound(mEqu)
          If InStr(Len(aCmp.nomeCmp) + 1, Trim(Mid(Trim(wRec), 5, Len(wRec) - 4)), mEqu(i).Alias) Then
            If UCase(TrovaParametro(mEqu(i).cmd, "ATTR")) <> "NONE" And Not (mEqu(i).cmd = "'ATTR='") Then
              aCmp.attrib = ControllaAttributo(TrovaParametro(mEqu(i).cmd, "ATTR"))
            End If
          End If
        Next i
        If UCase(TrovaParametro(Replace(wRec, "A.", "ATTR="), "ATTR")) <> "NONE" Then
          aCmp.attrib = TrovaParametro(Replace(wRec, "A.", "ATTR="), "ATTR")
          ' rischioso - controllo i casi noti
          If InStr(aCmp.attrib, "'") > 0 Then
           aCmp.attrib = ""
          End If
        End If
        'IRIS-DB
        aCmp.Exit = ""
        If UCase(TrovaParametro(Replace(wRec, "E.", "EXIT="), "EXIT")) <> "NONE" Then
          aCmp.Exit = TrovaParametro(Replace(wRec, "E.", "EXIT="), "EXIT")
          m_Fun.FnConnection.Execute "update PsDLI_MFSCmp set Exit = '" & aCmp.Exit _
                 & "' where Nome = '" & checkField(aCmp.nomeCmp) & "'  and FMTName = '" & pFMTName & "'"
        End If
        'IRIS-DB
        aCmp.Fill = ""
        If UCase(TrovaParametro(Replace(wRec, "F.", "FILL="), "FILL")) <> "NONE" Then
          aCmp.Fill = TrovaParametro(Replace(wRec, "F.", "FILL="), "FILL")
        End If
        If aCmp.INITIAL = "" And Left(aCmp.nomeCmp, 1) = "'" Then
          aCmp.INITIAL = aCmp.nomeCmp
          aCmp.nomeCmp = ""
        End If
        
        '**********  <8 ***********
'        If Len(aCmp.NomeCmp) = 8 Then
'          aCmp.NomeCmp = Left(aCmp.NomeCmp, CInt(charcutpos) - 1) & Right(aCmp.NomeCmp, 8 - CInt(charcutpos))
'        End If
        '*************************
        
        'SQ - 23-08-06
        'Con LPAGE = 0 saltava tutto!: DPAGE parte da 1, LPAGE da 0... si perdevano tutte le MOVE!
        If ordlpage = 0 Then ordlpage = 1 'Posizionare questo controllo in un unico punto
          Set rs = m_Fun.Open_Recordset("Select * From PsDLI_MFSmsgCmp where FMTname ='" & pFMTName _
                                        & "' And Nome ='" & Replace(aCmp.nomeCmp, "'", "''") & "' And MSGName ='" & Replace(PMSGName, "'", "''") & "' and LPage = " & ordlpage & " and OrdinaleDo = " & OrdinaleDo)
          'IRIS-DB: non dovrebbe servire pi� visto che adesso il nome lo cambio alla fonte se � duplicato
''          If Not rs.EOF Then 'IRIS-DB: ci possono essere nomi duplicati in MFS; sarebbe molto meglio mettere l'ordinale in chiave, ma ci sarebbero tutte le ricerche da cambiare, per ora rendiamo il nome univoco
''            aCmp.nomeCmp = aCmp.nomeCmp & "1"
''          End If
'IRIS-DB        If rs.EOF Then
        'IRIS-DB - inizio  tolgo la gestione del limite a 80, visto che le BMS dovrebbero gestire anche campi > 80
''''          If aCmp.Length > 80 And (InStr(pDevType, "3270P") = 0 And InStr(pDevType, "SCS") = 0) And Not mapping Then
''''            spcampolen = aCmp.Length
''''            colsplit = findColFromFMTcmp(checkField(aCmp.nomeCmp) & "SP1", pFMTName)
''''            'rigasplit = aCmp.PosY
''''            splindex = 1
''''            totallen = spcampolen
''''
''''            While totallen > 0
''''
''''             spNomeCampo = checkField(aCmp.nomeCmp) & "SP" & splindex
''''             If CInt(totallen) > 79 Then
''''                 lensplit = 81 - colsplit
''''             Else
''''                 lensplit = totallen
''''             End If
''''
''''             totallen = totallen - lensplit
''''
''''             '*************** SALVATAGGIO
''''             rs.AddNew
''''             rs!FMTName = pFMTName
''''             If pFMTName = "" Then
''''               emptyFMT = True
''''             End If
''''             rs!idOggetto = GbIdOggetto
''''             rs!nome = checkField(spNomeCampo)
''''             rs!MSGName = PMSGName
''''             rs!Lunghezza = lensplit
''''             rs!value = aCmp.INITIAL
'''''             If Not aCmp.NomeCmp = PfkField Then
'''''               rs!Funzione = aCmp.Function
'''''             ElseIf Not aCmp.Function = "" Then
'''''               rs!value = "'" & aCmp.Function & "'"
'''''               rs!Lunghezza = Len(aCmp.Function)
'''''               rs!Funzione = ""
'''''             End If
''''             rs!Attributo = aCmp.attrib
''''             rs!Ordinale = Ordinale
''''             rs!Occurs = Occurs
''''             If Occurs > 1 Then
''''               rs!OrdinaleDo = OrdinaleDo
''''             End If
''''             rs!LPage = ordlpage
''''            ' tratto il caso di SCA e affini
'''''             If Not aCmp.INITIAL = "" And Not Left(aCmp.INITIAL, 1) = "'" And Not IsNumeric(aCmp.INITIAL) Then
'''''               rs!Funzione = aCmp.INITIAL
'''''               rs!value = ""
'''''               If aCmp.NomeCmp = "" Then
'''''                 rs!nome = aCmp.INITIAL
'''''               End If
'''''             End If
''''             rs.Update
''''             colsplit = 1
''''             splindex = splindex + 1
''''             rigasplit = rigasplit + 1
''''             Ordinale = Ordinale + 1
''''            Wend
''''
''''          Else
            rs.AddNew
            rs!fmtName = pFMTName
            If pFMTName = "" Then
              emptyFMT = True
            End If
            rs!idOggetto = GbIdOggetto
            rs!nome = checkField(aCmp.nomeCmp)
            rs!MSGName = PMSGName
            If Not aCmp.INITIAL = "" Then
              If Len(aCmp.INITIAL) - 2 >= CInt(aCmp.Length) Then
                rs!Lunghezza = Len(aCmp.INITIAL) - 2
              Else
                rs!Lunghezza = aCmp.Length
              End If
            Else
              rs!Lunghezza = aCmp.Length
            End If
            rs!value = aCmp.INITIAL
            If Not aCmp.nomeCmp = PfkField Then
              rs!Funzione = aCmp.Function
            ElseIf Not aCmp.Function = "" Then ' pkfield e function valorizzata
              rs!value = "'" & aCmp.Function & "'"
              If Len(aCmp.Function) > CInt(aCmp.Length) Then
                rs!Lunghezza = Len(aCmp.Function)
              Else
                rs!Lunghezza = aCmp.Length
              End If
              rs!Funzione = ""
            End If
            rs!Attributo = aCmp.attrib
            rs!Ordinale = Ordinale
            rs!Occurs = Occurs
            If Occurs > 1 Then
              rs!OrdinaleDo = OrdinaleDo
            End If
            rs!LPage = ordlpage
            ' tratto il caso di SCA e affini
            If Not aCmp.INITIAL = "" And Not Left(aCmp.INITIAL, 1) = "'" And Not IsNumeric(aCmp.INITIAL) Then
              rs!Funzione = aCmp.INITIAL
              rs!value = ""
              If aCmp.nomeCmp = "" Then
                rs!nome = aCmp.INITIAL
              End If
            End If
            If rs!nome = "" Or Left(rs!nome, 1) = "'" Then
              rs!nome = "#" & progMFLD
              progMFLD = progMFLD + 1
            End If
            rs!Exit = aCmp.Exit
            rs!Fill = aCmp.Fill
            rs.Update
            Ordinale = Ordinale + 1
''''          End If ' controllo lunghezza > 79
'IRIS-DB - fine
'IRIS-DB        End If ' not EOF
        rs.Close
        
        
        aCmp.Function = ""
        aCmp.attrib = ""
        aCmp.INITIAL = ""
        aCmp.Length = 0
      Else ' portrei avere solo campo fra apici o solo LTH=
        If InStr(5, wRec, "'") Then
          aCmp.nomeCmp = Trim(Mid(wRec, InStr(wRec, "MFLD") + 4, InStrRev(wRec, "'") - InStr(wRec, "MFLD") - 3))
        'IRIS-DB: checks if same name used in the same MFS in another FMT
          If Not aCmp.nomeCmp = "" And Not InStr(aCmp.nomeCmp, "LTH=") And Not InStr(aCmp.nomeCmp, ")") > 0 Then
            Set rsDupField = m_Fun.Open_Recordset("SELECT * FROM PsDLI_MFSCmp where idOggetto = " & GbIdOggetto & " and FMTName = '" & pFMTName & "' and OrigName = '" & Trim(aCmp.nomeCmp) & "'")
            If Not rsDupField.EOF Then
              aCmp.nomeCmp = rsDupField!nome
            End If 'IRIS-DB
            rsDupField.Close 'IRIS-DB
          End If 'IRIS-DB
          aCmp.Length = Len(aCmp.nomeCmp) - 2
          aCmp.INITIAL = aCmp.nomeCmp
          addCampo = True
        ElseIf InStr(5, wRec, "LTH=") Then
          aCmp.nomeCmp = Trim(Mid(wRec, InStr(wRec, "MFLD") + 4, InStrRev(wRec, "=") - InStr(wRec, "MFLD") - 2))
        'IRIS-DB: checks if same name used in the same MFS in another FMT
          If Not aCmp.nomeCmp = "" And Not InStr(aCmp.nomeCmp, "LTH=") > 0 Then
            Set rsDupField = m_Fun.Open_Recordset("SELECT * FROM PsDLI_MFSCmp where idOggetto = " & GbIdOggetto & " and FMTName = '" & pFMTName & "' and OrigName = '" & Trim(aCmp.nomeCmp) & "'")
            If Not rsDupField.EOF Then
              aCmp.nomeCmp = rsDupField!nome
            End If 'IRIS-DB
            rsDupField.Close 'IRIS-DB
          End If 'IRIS-DB
          If Not aCmp.nomeCmp = "" And Not InStr(aCmp.nomeCmp, "LTH=") > 0 Then
            aCmp.Length = Right(aCmp.nomeCmp, Len(aCmp.nomeCmp) - InStr(1, aCmp.nomeCmp, "="))
          Else ' nome campo davanti ad eticheta MFLD
            aCmp.nomeCmp = Trim(Left(wRec, InStr(wRec, "MFLD") - 1))
            If UCase(TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "MFLD") - 3), ArrLAlias)) <> "NONE" Then
              aCmp.Length = TrovaParametroArray(Right(wRec, Len(wRec) - InStr(1, wRec, "MFLD") - 3), ArrLAlias)
            End If
          End If
          addCampo = True
        End If
        If addCampo Then
          'SQ - 23-08-06
          'Con LPAGE = 0 saltava tutto!: DPAGE parte da 1, LPAGE da 0... si perdevano tutte le MOVE!
          If ordlpage = 0 Then ordlpage = 1 'Posizionare questo controllo in un unico punto
          addCampo = False
          Set rs = m_Fun.Open_Recordset("Select * From PsDLI_MFSmsgCmp where FMTname = '" & pFMTName & _
                                        "' And Nome ='" & checkField(Replace(aCmp.nomeCmp, "'", "''")) & "' And MSGName ='" & Replace(PMSGName, "'", "''") & "' and LPage = " & ordlpage & " and OrdinaleDo = " & OrdinaleDo)
          If rs.EOF Then
            If aCmp.Length > 80 And (InStr(pDevType, "3270P") = 0 And InStr(pDevType, "SCS") = 0) And Not mapping Then
              spcampolen = aCmp.Length
              colsplit = findColFromFMTcmp(checkField(aCmp.nomeCmp) & "SP1", pFMTName)
              'rigasplit = aCmp.PosY
              splindex = 1
              totallen = spcampolen
                          
              While totallen > 0
              
               spNomeCampo = checkField(aCmp.nomeCmp) & "SP" & splindex
               If CInt(totallen) > 79 Then
                   lensplit = 81 - colsplit
               Else
                   lensplit = totallen
               End If
              
               totallen = totallen - lensplit
              
               '*************** SALVATAGGIO
               rs.AddNew
               rs!fmtName = pFMTName
               If pFMTName = "" Then
                 emptyFMT = True
               End If
               rs!idOggetto = GbIdOggetto
               rs!nome = checkField(spNomeCampo)
               rs!MSGName = PMSGName
               rs!Lunghezza = lensplit
               rs!value = aCmp.INITIAL
  '             If Not aCmp.NomeCmp = PfkField Then
  '               rs!Funzione = aCmp.Function
  '             ElseIf Not aCmp.Function = "" Then
  '               rs!value = "'" & aCmp.Function & "'"
  '               rs!Lunghezza = Len(aCmp.Function)
  '               rs!Funzione = ""
  '             End If
               rs!Attributo = aCmp.attrib
               rs!Ordinale = Ordinale
               rs!Occurs = Occurs
               If Occurs > 1 Then
                 rs!OrdinaleDo = OrdinaleDo
               End If
               rs!LPage = ordlpage
              ' tratto il caso di SCA e affini
  '             If Not aCmp.INITIAL = "" And Not Left(aCmp.INITIAL, 1) = "'" And Not IsNumeric(aCmp.INITIAL) Then
  '               rs!Funzione = aCmp.INITIAL
  '               rs!value = ""
  '               If aCmp.NomeCmp = "" Then
  '                 rs!nome = aCmp.INITIAL
  '               End If
  '             End If
               rs.Update
               Ordinale = Ordinale + 1
               colsplit = 1
               splindex = splindex + 1
               rigasplit = rigasplit + 1
              Wend
          
            Else
              rs.AddNew
              rs!fmtName = pFMTName
              If pFMTName = "" Then
                emptyFMT = True
              End If
              rs!idOggetto = GbIdOggetto
              rs!nome = checkField(aCmp.nomeCmp)
              rs!MSGName = PMSGName
              rs!Lunghezza = aCmp.Length
              rs!Funzione = aCmp.Function
              rs!Attributo = aCmp.attrib
              rs!Ordinale = Ordinale
              rs!value = aCmp.INITIAL
              rs!Occurs = Occurs
              If Occurs > 1 Then
                rs!OrdinaleDo = OrdinaleDo
              End If
              rs!LPage = ordlpage
              If rs!nome = "" Or Left(rs!nome, 1) = "'" Then
                rs!nome = "#" & format(progMFLD, "000")
                progMFLD = progMFLD + 1
              End If
              rs.Update
            End If
            rs.Close
            Ordinale = Ordinale + 1
          End If
          aCmp.Function = ""
          aCmp.attrib = ""
          aCmp.INITIAL = ""
          aCmp.Length = 0
          addCampo = False
      End If
     End If
    Case "PFK"
       ' wRec = Left(Trim(wRec), InStr(1, Trim(wRec), "  "))
        If Len(wRec) > 71 Then wRec = Left(wRec, 71)
        If InStr(wRec, "*") > 0 And Not InStr(wRec, "'*'") > 0 Then
          wRec = Left(wRec, InStr(wRec, "*") - 1)
        End If
        'If Right(wRec, 1) = "X" Then wRec = Left(wRec, Len(wRec) - 1)
        While Not Right(wRec, 1) = ")"
           Line Input #fpointer, contPFK
           If checktype = "M" Then
            GbNumRec = GbNumRec + 1
            GbOldNumRec = GbNumRec
           End If
           contPFK = Trim(Left(contPFK, 71))
           If InStr(1, contPFK, "*") > 0 And Not InStr(1, contPFK, "'*'") > 0 Then
             contPFK = Left(contPFK, InStr(1, contPFK, "*") - 1)
           End If
           'wRec = wRec & Trim(Left(contPFK, InStr(1, Trim(contPFK), "  ")))
           wRec = wRec & contPFK
        Wend
        linePFK = Analizza_PFK(wRec)
        
        m_Fun.FnConnection.Execute "DELETE FROM PsDLI_MFSPfk WHERE FMTName = '" & pFMTName & "' "
        
        PfkField = Left(linePFK, InStr(linePFK, ",") - 1)
'         m_Fun.FnConnection.Execute "Update PsDLI_MFS Set PFKField = '" & PfkField _
'                                   & "' where IdOggetto = " & GbIdOggetto & " and DevType = '(3270,2)'"
          m_Fun.FnConnection.Execute "Update PsDLI_MFS Set PFKField = '" & PfkField _
                                    & "' where FMTName = '" & pFMTName & "' and DevType = '(3270,2)'"
        linePFK = Right(linePFK, Len(linePFK) - (Len(PfkField) + 1))
        Set rs = m_Fun.Open_Recordset("Select * From PsDLI_MFSPfk")
        linePFK = Trim(linePFK)
        If Not Left(linePFK, 1) = "'" Then ' coppie tasto valore
          While InStr(1, linePFK, ",") > 0
                If CoppiaTastoValore(Mid(linePFK, 1, InStr(1, linePFK, ",") - 1), pTasto, pValore) Then
                  rs.AddNew
                  rs!fmtName = pFMTName
                  If pFMTName = "" Then
                    emptyFMT = True
                  End If
                  rs!idOggetto = GbIdOggetto
                  rs!Dev = pDevType
                  rs!tasto = pTasto
                  rs!Valore = Trim(pValore)
                  rs.Update
                End If
                linePFK = Mid(linePFK, InStr(1, linePFK, ",") + 1, Len(linePFK) - InStr(1, linePFK, ","))
          Wend
          If CoppiaTastoValore(linePFK, pTasto, pValore) Then
            rs.AddNew
            rs!fmtName = pFMTName
            If pFMTName = "" Then
              emptyFMT = True
            End If
            rs!idOggetto = GbIdOggetto
            rs!Dev = pDevType
            rs!tasto = pTasto
            rs!Valore = Trim(pValore)
            rs.Update
          End If
        Else   ' singoli
          Dim progressivo2 As Integer
          progressivo2 = 0
          While (InStr(1, linePFK, ",")) > 0
                If Not linePFK = "," Then
                  progressivo2 = progressivo2 + 1
                  rs.AddNew
                  rs!idOggetto = GbIdOggetto
                  rs!Dev = pDevType
                  rs!tasto = progressivo2
                  rs!Valore = Trim(pulisciCommentiPFK(Left(linePFK, InStr(1, linePFK, ",") - 1)))
                  rs.Update
                End If
                linePFK = Mid(linePFK, InStr(1, linePFK, ",") + 1, Len(linePFK) - InStr(1, linePFK, ","))
          Wend
          If Not linePFK = "" Then
            progressivo2 = progressivo2 + 1
            rs.AddNew
            rs!idOggetto = GbIdOggetto
            rs!Dev = pDevType
            rs!tasto = progressivo2
            rs!Valore = Trim(pulisciCommentiPFK(linePFK))
            rs.Update
          End If
        End If
        rs.Close
     Case "MFLD"
        wRec = Trim(Mid(wRec, InStr(wRec, "MFLD") + 4, Len(wRec) - InStr(wRec, "MFLD") - 4))
        If InStr(wRec, "    ") Then
            wRec = Trim(Mid(wRec, 1, InStr(wRec, "    ")))
        End If
        'Ricerca di campi definiti da funzioni
        If Left(wRec, 1) = "(" Then  'assumiamo che campo sia definito da funzione nel formato (campo,funzione)
           If Right(wRec, 1) = ")" Then
                wRec = Mid(wRec, 1, Len(wRec) - 1)
           End If
            ' nel caso del campo che intercetta i tasti funzione considero il secondo campo come value
            If Not Mid(wRec, 2, InStr(wRec, ",") - 2) = PfkField Then
              m_Fun.FnConnection.Execute "update PsDLI_MFSCmp set Funzione = '" _
              & Mid(wRec, InStr(wRec, ",") + 1, Len(wRec) - InStr(wRec, ",")) _
              & "' where Nome = '" & Mid(wRec, 2, InStr(wRec, ",") - 2) & "'  and FMTname = '" & pFMTName & "'"
            Else
              m_Fun.FnConnection.Execute "update PsDLI_MFSCmp set Value = '" _
              & Mid(wRec, InStr(wRec, ","), Len(wRec) - InStr(wRec, ",") + 1) _
              & "' where Nome = '" & Mid(wRec, 2, InStr(wRec, ",") - 2) & "'  and FMTname = '" & pFMTName & "'"
            End If
        End If
      Case Else
        If sezioneDev Then
          If pDevType = "" Then
            pDevType = findParameter(wRec, "TYPE")
          End If
          If pFeat = "" Then
            pFeat = findParameter(wRec, "FEAT")
          End If
          If ppage = "" Then
            ppage = findParameter(wRec, "PAGE")
          End If
          If pDSCA = "" Then
            pDSCA = findParameter(wRec, "DSCA")
          End If
          If pSysMsg = "" Then
            pSysMsg = findParameter(wRec, "SYSMSG")
          End If
        ElseIf sezioneDiv Then
          If pDivType = "" Then
            pDivType = findParameter(wRec, "TYPE")
          End If
        ElseIf sezioneDpage Then
          If pCursor = "" Then
            pCursor = findParameter(wRec, "CURSOR")
          End If
          If pFill = "" Then
            pFill = findParameter(wRec, "FILL")
          End If
        ElseIf sezioneMsg Then
          If pMsgType = "" Then
            pMsgType = findParameter(wRec, "TYPE")
          End If
          If pSor = "" Then
            pSor = findParameter(wRec, "SOR")
          End If
          If pMSGFill = "" Then
            pMSGFill = findParameter(wRec, "FILL")
          End If
          If pNxt = "" Then
            pNxt = findParameter(wRec, "NXT")
          End If
        End If
      End Select
    End If
  Wend
  '************** <8 *************************************
'  i = 1
'  j = 1
'  While i <= Campi.count
'    PrimoDuplicato = True
'    While (j + i) <= Campi.count
'     If Len(Campi.item(i)) = 8 Then
'      fieldtruncate1 = Left(Campi.item(i), CInt(charcutpos) - 1) & Right(Campi.item(i), 8 - CInt(charcutpos))
'     Else
'      fieldtruncate1 = Campi.item(i)
'     End If
'     If Len(Campi.item(j + i)) = 8 Then
'      fieldtruncate2 = Left(Campi.item(j + i), CInt(charcutpos) - 1) & Right(Campi.item(j + i), 8 - CInt(charcutpos))
'     Else
'      fieldtruncate2 = Campi.item(j + i)
'     End If
'
'     If fieldtruncate1 = fieldtruncate2 And campodpage.item(i) = campodpage.item(j + i) And campoSuff.item(i) = campoSuff.item(j + i) And campoFMT.item(i) = campoFMT.item(j + i) Then
'         If PrimoDuplicato Then
'             ItemDuplicato = Campi.item(i) & "," & Campi.item(j + i)
'             PrimoDuplicato = False
'         Else
'             ItemDuplicato = ItemDuplicato & "," & Campi.item(j + i)
'         End If
'         Campi.Remove (j + i)
'         campodpage.Remove (j + i)
'         campoSuff.Remove (j + i)
'         j = j - 1
'     End If
'     j = j + 1
'    Wend
'    If Not ItemDuplicato = "" Then
'     InsiemiDuplicati.Add ItemDuplicato
'     ItemDuplicato = ""
'    End If
'    i = i + 1
'    j = 1
'  Wend
'
'  If InsiemiDuplicati.count > 0 Then
'    For i = 1 To InsiemiDuplicati.count
'       var2segnalazione = var2segnalazione & IIf(var2segnalazione = "", "", ";") & "[" & InsiemiDuplicati.item(i) & "]"
'    Next
'    Set rsMappa = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
'    If Not rsMappa.EOF Then
'      nomemappa = rsMappa!nome
'      rsMappa.Close
'    End If
'    addSegnalazione var2segnalazione, "DUPLICATEDNAMES", nomemappa
'  End If
  '********************************************
  
  Close nFBms
  
  m_Fun.FnConnection.Execute "Update PsDLI_MFS Set PFKField = '" & PfkField & _
                              "' where FMTname = '" & pFMTName & "' and DevType = '(3270,2)'"
  
  Set rs = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  rs!DtParsing = Now
  rs!ErrLevel = GbErrLevel
  rs!parsinglevel = 1
  rs.Update
  rs.Close
  
  'Controllo campi funzione con lunghezza valorizzata
  Set rs = m_Fun.Open_Recordset("select Lunghezza,Funzione from PSDLI_MFSmsgcmp where Funzione in " _
  & "('LTSEQ','LTNAME','TIME','DATE1','DATE2','DATE3','DATE4','LPAGENO','LTMSG') and IdOggetto =" & GbIdOggetto)
  
  While Not rs.EOF
    If IIf(IsNull(rs!Lunghezza), 0, rs!Lunghezza) = 0 Then
      Select Case rs!Funzione
        Case "LTSEQ"
          rs!Lunghezza = 5
          rs.Update
        Case "LTNAME"
          rs!Lunghezza = 8
          rs.Update
        Case "TIME"
          rs!Lunghezza = 8
          rs.Update
        Case "DATE1"
          rs!Lunghezza = 6
          rs.Update
        Case "DATE2"
          rs!Lunghezza = 8
          rs.Update
        Case "DATE3"
          rs!Lunghezza = 8
          rs.Update
        Case "DATE4"
          rs!Lunghezza = 8
          rs.Update
        Case "LPAGENO"
          rs!Lunghezza = 4
          rs.Update
        Case "LTMSG"
          rs!Lunghezza = 14
          rs.Update
      End Select
    End If
    rs.MoveNext
  Wend
  rs.Close
  Exit Sub
error:
  If emptyFMT And nosegnFMT Then
    m_Fun.writeLog "parseMFS - " & sorlpage & " - incorrect COND: " & wStr
    addSegnalazione wRecIn, "P00", "no FMT"
    nosegnFMT = True
  Else
    addSegnalazione wRecIn, "P00", wRecIn
  End If
  Resume Next
End Sub

Private Function findColFromFMTcmp(FIELD As String, pFMTName As String) As Integer
  Dim rs As Recordset
  Set rs = m_Fun.Open_Recordset("Select Colonna From PsDLI_MFSCmp where FMTname ='" & pFMTName _
                                      & "' And Nome ='" & Replace(FIELD, "'", "''") & "'")
  If Not rs.EOF Then
    findColFromFMTcmp = IIf(IsNull(rs!Colonna), 0, rs!Colonna)
  End If
End Function

Private Function DoubleApice(source As String) As String
  DoubleApice = Replace(source, "'", "''")
End Function
Function CorrectCCMove(Cmp() As dataCmp, CmpValues() As String, dataCmpStartValues() As String, CommandCode As SSA_ITEM) As Boolean
Dim posrif As Integer, i As Integer, arSplit() As String, arPosSplit() As String, j As Integer
Dim toRebuild As Boolean
  CorrectCCMove = True
  posrif = CommandCode.pos
  For i = 0 To UBound(Cmp)
    If Cmp(i).Posizione >= posrif And Cmp(i).Posizione < posrif + CommandCode.len Then
      arSplit() = Split(CmpValues(i), ",")
      arPosSplit() = Split(dataCmpStartValues(i), ",")
      toRebuild = False
      For j = 0 To UBound(arSplit)
        If arPosSplit(j) = "0" Then
          CorrectCCMove = False
          arPosSplit(j) = "1"
          toRebuild = True
          If CommandCode.len > Len(arSplit(j)) Then
            arSplit(j) = arSplit(j) & Space(CommandCode.len - Len(arSplit(j)))
          End If
        End If
      Next j
      If toRebuild Then
        CmpValues(i) = ""
        For j = 0 To UBound(arSplit)
          CmpValues(i) = CmpValues(i) & arSplit(j) & ","
        Next
        CmpValues(i) = Left(CmpValues(i), Len(CmpValues(i)) - 1)
        dataCmpStartValues(i) = ""
        For j = 0 To UBound(arPosSplit)
          dataCmpStartValues(i) = dataCmpStartValues(i) & arPosSplit(j) & ","
        Next
        dataCmpStartValues(i) = Left(dataCmpStartValues(i), Len(dataCmpStartValues(i)) - 1)
      End If
    End If
  Next i
End Function

Public Sub parseSSA(ssaStruct As t_ssa, Optional ptype = "")
  Dim Ordinale As Integer, codesIndex As Integer, valueIndex As Integer, keyLevel As Integer, redefineLevel As Integer
  Dim ssa As String
  Dim dataCmp() As dataCmp
  Dim dataCmpValues() As String
  Dim dataCmpStartValues() As String
  Dim manageMove As Boolean, j As Integer
  Dim ssamoved_alligned As Boolean
  On Error GoTo errSSA
  
  '''''''''''''''''''''''
  ' SSA gi� calcolata?
  '''''''''''''''''''''''
  Dim i As Integer
  For i = 0 To UBound(ssaCache)
    If (ssaCache(i).SSAName = ssaStruct.SSAName And ssaCache(i).idOggetto = ssaStruct.idOggetto) Then  'AC 08/10/10 aggiunta condizione su IdOggetto
      ssaStruct = ssaCache(i)
      Exit Sub
    End If
  Next i
  
  ReDim ssaStruct.Chiavi(0)
  ReDim ssaStruct.NomeSegmento.value(0)
  ReDim ssaStruct.Qualificazione.value(0)
  ReDim ssaStruct.CommandCode.value(0)
  
  'stefano: servono un po' di pezze, forse era meglio fare una funzione ad hoc...
  Dim SSAConstant As Boolean
  SSAConstant = False
  If InStr(ssaStruct.SSAName, "'") > 0 Then
    SSAConstant = True
  End If
  
  If Not SSAConstant Then
    manageMove = m_Fun.LeggiParam("IMSDC-MOVEYN") = "YES"
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 18-05-06
    ' Restituisce la struttura di campi con TUTTE le COPY esplose
    ' Es:  01 BLUTO.
    '         COPY GIORGIO.
    '         03 FIFFO ...
    '         COPY ALPREDO.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'SQ 18-06-07 - gestione aree a livello > 01
    'dataCmp = getDataCmp(ssaStruct.idOggetto, ssaStruct.idArea, ssaStruct.SSAName)
    dataCmp = getDataCmp(ssaStruct.idOggetto, ssaStruct.idArea, ssaStruct.SSAName, True)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 28-06-06
    ' Restituisce l'array relativo alla dataCmp con i valori (di move)
    ' Esterno alla struttura della dataCmp che mappa la PsData_Cmp
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If manageMove Then
      dataCmpValues = getDataCmpValues(dataCmp, dataCmpStartValues)
    End If
    'IRIS-DB start SSA mossa a livello 01
     'salva i nomi delle SSA multiple in una nuova tabella, da cui si potr� fare un import per poter passare solo i valori validi
    m_Fun.FnConnection.Execute "DELETE FROM IRIS_PsDLI_Istruzioni_MULTI_SSA WHERE idPgm = " & GbIdOggetto & " and idOggetto = " & GbIdOggetto & " and Riga = " & GbOldNumRec & _
                               " and NomeSSA = '" & ssaStruct.SSAName & "'"
    Dim rsRP As Recordset
    Set rsRP = m_Fun.Open_Recordset("Select * From IRIS_PsDLI_Istruzioni_MULTI_SSA")
    Dim rs As Recordset
    Set rs = m_Fun.Open_Recordset("SELECT valore From PsData_CmpMoved WHERE " & _
                        "idPgm=" & GbIdOggetto & " And Nome = '" & ssaStruct.SSAName & "'")
    i = 0
    While Not rs.EOF
      rsRP.AddNew
      rsRP!idpgm = GbIdOggetto
      rsRP!idOggetto = GbIdOggetto
      rsRP!Riga = GbOldNumRec
      rsRP!NomeSSA = ssaStruct.SSAName
      i = i + 1
      rsRP!Idx = i
      rsRP!Content = rs!Valore
      rsRP.Update
      rs.MoveNext
    Wend
    rsRP.Close
    rs.Close
'IRIS-DB end
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Costruisce l'SSA: da struttura COBOL a stringa (posizionale/IMS)
    ' (setta ssaStruct.moved)
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 28-02-07
    If UBound(dataCmp) = 0 And dataCmp(0).Lunghezza > 0 And dataCmp(0).moved Then
      '''''''''''''''''''''''''''''''''''''''''''
      'stefano 20-06-07
      'gestione ssa mosse a livello 01
      '''''''''''''''''''''''''''''''''''''''''''
      If Len(Trim(dataCmp(0).Valore)) And Not dataCmp(0).Valore = "SPACES" Then  'sbagliata: posso avere SPACE...
        Dim fieldName As String
        'Ho gi� dataCmpValues...
        Set rs = m_Fun.Open_Recordset("SELECT valore From PsData_CmpMoved WHERE " & _
                            "idPgm=" & GbIdOggetto & " And Nome='" & Replace(dataCmp(0).nome, "'", "") & "'")
        fieldName = ""
        If Not rs.EOF Then
          fieldName = rs!Valore
        End If
        rs.Close
        If Len(fieldName) And Not Left(fieldName, 1) = "'" Then
          Set rs = m_Fun.Open_Recordset("SELECT idoggetto, idarea, valore, lunghezza, tipo From PsData_Cmp " & _
                                  "Where idOggetto = " & GbIdOggetto & " And nome = '" & DoubleApice(fieldName) & "'" & _
                                  " UNION " & _
                                  "SELECT a.idoggetto, a.idarea, a.valore, a.lunghezza, a.tipo From PsData_Cmp as a,PsRel_Obj as b" & _
                                  " where b.idOggettoC = " & GbIdOggetto & " and b.relazione='CPY' And" & _
                                  " a.IdOggetto=b.IdOggettoR and a.nome = '" & DoubleApice(fieldName) & "'")
          If Not rs.EOF Then
            If Not (IsNull(rs!Valore) Or rs!Valore = "") Then
              dataCmp = getDataCmp(rs!idOggetto, rs!idArea, fieldName, True)
              If manageMove Then
                dataCmpValues = getDataCmpValues(dataCmp, dataCmpValues)
              End If
            End If
          End If
          rs.Close
        Else
           m_Fun.writeLog "!!SSA NON GESTITA - Istruzione: " & GbIdOggetto & "-" & GbOldNumRec & " -> " & dataCmpValues(0)
        End If
        ' Mauro 09-05-2007 : Value nel campo di gruppo (DROGATI!!!)
        If dataCmp(0).Tipo = "A" And Len(Trim(dataCmp(0).Valore)) > 0 Then
          ssa = formatSsaValue(IIf(IsNull(dataCmp(0).Valore), "@", Trim(dataCmp(0).Valore)), dataCmp(0).byte & "", dataCmp(0).Segno, dataCmp(0).nome)
        Else
          'CASO "NORMALE"
          ssa = getSSAstream(dataCmp, ssaStruct)
        End If
      Else
        'CASO "SPEZZAGAMBE"
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SSA PIC X... le move sono al livello 01 di SSA complete...
        ' la struttura delle SSA non corrisponde (posizione elementi diversa)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim ssaValues() As String
        ssaValues = Split(dataCmpValues(0), ",")
        If Len(dataCmpValues(0)) Then
          'ssa = ssaValues(0)
          ssa = formatSsaValue(IIf(IsNull(ssaValues(0)), "@", Trim(ssaValues(0))), dataCmp(0).byte & "", dataCmp(0).Segno, dataCmp(0).nome)
          'Altre potenziali SSA:
          dataCmpValues(0) = Mid(dataCmpValues(0), Len(ssaValues(0)) + 2)
          'SQ TMP!!!!!!!!! STO PRENDENDO SOLO IL PRIMO!!!! (INVENTARSI UN CICLO...)
          m_Fun.writeLog "!!SSA IGNORATA - Istruzione: " & GbIdOggetto & "-" & GbOldNumRec & " -> " & dataCmpValues(0)
          'dataCmpValues(0) = ""
          'IRIS-DB tolto perch� modificato con la lista delle SSA impattate e messo sopra
'''''          m_Fun.FnConnection.Execute "DELETE FROM IRIS_PsDLI_Istruzioni_MULTI_SSA WHERE idPgm = " & GbIdOggetto & " and idOggetto = " & GbIdOggetto & " and Riga = " & GbOldNumRec & _
'''''                                     " and NomeSSA = '" & ssaStruct.SSAName & "'"
'''''          i = 0
'''''          Dim rsRP As Recordset
'''''          Set rsRP = m_Fun.Open_Recordset("Select * From IRIS_PsDLI_Istruzioni_MULTI_SSA")
'''''          While Len(dataCmpValues(0))
'''''            Dim ssaContent As String
'''''            ssaContent = Left(dataCmpValues(0), Len(ssaValues(0)))
'''''            rsRP.AddNew
'''''            rsRP!idpgm = GbIdOggetto
'''''            rsRP!idOggetto = GbIdOggetto
'''''            rsRP!Riga = GbOldNumRec
'''''            rsRP!NomeSSA = ssaStruct.SSAName
'''''            i = i + 1
'''''            rsRP!Idx = i
'''''            rsRP!Content = ssaContent
'''''            rsRP.Update
'''''            dataCmpValues(0) = Mid(dataCmpValues(0), Len(ssaValues(0)) + 2)
'''''          Wend
'''''          rsRP.Close
        End If
      End If
    Else
      ' Mauro 09-05-2007 : Value nel campo di gruppo (DROGATI!!!)
      If dataCmp(0).Tipo = "A" And Len(Trim(dataCmp(0).Valore)) > 0 Then
        ssa = formatSsaValue(IIf(IsNull(dataCmp(0).Valore), "@", Trim(dataCmp(0).Valore)), dataCmp(0).byte & "", dataCmp(0).Segno, dataCmp(0).nome)
      Else
        'CASO "NORMALE"
        ssa = getSSAstream(dataCmp, ssaStruct)

        If Len(Trim(ssa)) = 0 Then
          ''''''''''''''''''''''''''''
          ' NON ANCORA TROVATO NIENTE
          '
          ' SQ 10-10-07
          ' Altra casistica: (es. Atos)
          ' ssa strutturata; move ssa-finta to ssa; ssa-finta strutturata con value:
          ' => dataCmp(0).value = "" e dataCmpValues(0).value = "PIENO!"
          '''''''''''''''''''''''''''''
          If dataCmp(0).Tipo = "A" And Len(Trim(dataCmpValues(0))) > 0 Then
            'serve il formatSsaValue?
            ssa = formatSsaValue(IIf(IsNull(dataCmpValues(0)), "@", Trim(dataCmpValues(0))), dataCmp(0).byte & "", dataCmp(0).Segno, dataCmp(0).nome)
          End If
        End If
      End If
    End If
  Else
    'stefano: imposto l'ssa costante
    ssa = Mid(ssaStruct.SSAName, 2, Len(ssaStruct.SSAName) - 2) 'matteo: se vuoi usare un sistema migliore, visto che sei esperto...
  End If
  
  'SQ 10-10-07
  'ssa � formattato! ma come fa a non esserci la trim qui?
  If Len(Trim(ssa)) Then
    'la ssaStruct sarebbe la TabIstrSSa(UBound(TabIstrSSa))
    
    'SQ - portati fuori if...
'    ReDim ssaStruct.Chiavi(0)
'    ReDim ssaStruct.NomeSegmento.value(0)
'    ReDim ssaStruct.Qualificazione.value(0)
'    ReDim ssaStruct.CommandCode.value(0)
    
    ssaStruct.ssaStream = ssa
    
    ''''''''''''''''''''''''''''''''''''''''''''
    ' ANALISI SSA (pulita)
    ''''''''''''''''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''
    ' Segmento: 1-8
    '''''''''''''''''''''''''
    ssaStruct.NomeSegmento.value(0) = Left(ssa, 8)  'att!! FARE IL TRIM DOPO...
    ssaStruct.NomeSegmento.pos = 1
    ssaStruct.NomeSegmento.len = 8
    If manageMove And Not SSAConstant Then
      checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.NomeSegmento
    End If
    
    ''''''''''''''''''''''''''
    ' Tipo SSA - Char (9:1):
    ' - bianco: squalificata
    ' - "(":    qualificata
    ' - "*":    command code
    ''''''''''''''''''''''''''
    Select Case Mid(ssa, 9, 1)
      Case "@"  'NO VALUE! VALORIZZATO EVIDENTEMENTE A RUN-TIME
        ssa = ""
        ssaStruct.valida = False
        'manageUnknownQualification(
      Case " "
        'SQUALIFICATA
        ssaStruct.Qualificatore = False
        ssaStruct.Qualificazione.pos = 9
        ssaStruct.Qualificazione.len = 1
        ssaStruct.Qualificazione.value(0) = " "
        'SQ 5-07-06 continuo l'analisi per qualificazioni a run-time...
        If Len(ssa) > 9 And Right(ssa, 1) = ")" Then
          'ATTENZIONE: forzatura!
          'La value potrebbe avere space o niente! (CONSIDERIAMO COMUNQUE LO SPACE!)
          ssaStruct.Qualificatore = True
          ssaStruct.Qualificazione.moved = True
          codesIndex = 10 'lo uso come "base"
        Else
          ssa = ""
          ssaStruct.valida = True
          ssaStruct.Qualificatore = False
        End If
      Case "("
        'QUALIFICATA
        codesIndex = 10 'lo uso come "base"
        ssaStruct.Qualificatore = True
        'SQ 22-10-07
        ssaStruct.Qualificazione.pos = 9
        ssaStruct.Qualificazione.len = 1
        ssaStruct.Qualificazione.value(0) = "("
      Case "*"
        'CommandCodes: fino a " " o "("
        ssaStruct.CommandCode.value(0) = "*"
        ssaStruct.CommandCode.pos = 9
        For codesIndex = 10 To Len(ssa)
          Select Case Mid(ssa, codesIndex, 1)
            Case "@"  'NO VALUE! VALORIZZATO EVIDENTEMENTE A RUN-TIME
              ssa = ""
              ssaStruct.valida = False
            Case " "
              'SQUALIFICATA: continuo l'analisi per qualificazioni a run-time...
              ssa = ""
              ''codesIndex = codesIndex + 1 'salto la parentesi
              
              ssaStruct.valida = True
              ssaStruct.Qualificatore = False
              'SQ - move/value
              ssaStruct.Qualificazione.pos = codesIndex
              ssaStruct.Qualificazione.len = 1
              ssaStruct.Qualificazione.value(0) = " "
            Case "("
              ssaStruct.Qualificatore = True
              'SQ - move/value
              ssaStruct.Qualificazione.pos = codesIndex
              ssaStruct.Qualificazione.len = 1
              ssaStruct.Qualificazione.value(0) = "("
              codesIndex = codesIndex + 1 'salto la parentesi
              Exit For
            Case Else
              ssaStruct.CommandCode.value(0) = ssaStruct.CommandCode.value(0) & Mid(ssa, codesIndex, 1)
          End Select
        Next codesIndex
      Case Else
        ''''''''''''''''''''''''''''''''''''''''
        ' SQ - 5-05-06
        ' Nuova segnalazione (I03)
        ''''''''''''''''''''''''''''''''''''''''
        addSegnalazione ssa, "SSAWRONG", ssa
        ssaStruct.valida = False 'IRIS-DB forza la dinamica mettendo istruzione non valida
        ReDim ssaStruct.Chiavi(0)
        Exit Sub
    End Select
    
    'MOVE/VALUE "QUALIFICAZIONE"
    If manageMove And Not SSAConstant Then
      checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.Qualificazione
    End If
    
    'MOVE/VALUE "COMMAND CODES"
    If ssaStruct.CommandCode.pos Then
      ssaStruct.CommandCode.len = Len(ssaStruct.CommandCode.value(0))
      'TMP
      
      'AC 19/09/08  Problema di ssa mosse con struttura diversa
      ' creazioni di combinazioni di command code utilizzando ssa che non hanno command code
      ' pezza: controllo che il primo command code della ssa mossa non sia "(" per tutte le ssa
      ssamoved_alligned = False
      For i = 0 To UBound(dataCmpValues)
        If Mid(dataCmpValues(0), ssaStruct.CommandCode.pos, 1) = "(" Then
            Exit For
        End If
      Next i
      If i = UBound(dataCmpValues) + 1 Then
        ssamoved_alligned = True
      End If
      If manageMove And Not SSAConstant And ssamoved_alligned Then
        If Not GbTipoInPars = "PLI" Then
        'IRIS-DB: non chiaro perch� fa una combinazione delle combinazioni, forse per il PLI... tolto
        'IRIS-DB  checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.CommandCode, True
          checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.CommandCode 'IRIS-DB
        Else ' devo riconoscere se move posizionali o meno (in questo caso corregger valori e posizioni
          Dim isToCombine As Boolean
          isToCombine = CorrectCCMove(dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.CommandCode)
          checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.CommandCode, isToCombine
        End If
      End If
      'Potrei avere SPACE sul c.c => Effetto collaterale sulla squalificazione:
      For i = 1 To UBound(ssaStruct.CommandCode.value)
        If InStr(ssaStruct.CommandCode.value(i), " ") Then
          ssaStruct.Qualificazione.moved = True
          ReDim ssaStruct.Qualificazione.value(UBound(ssaStruct.Qualificazione.value) + 1)
          ssaStruct.Qualificazione.value(UBound(ssaStruct.Qualificazione.value)) = " "
          Exit For
        End If
      Next i
      Dim ssaValue() As String
      Dim k As Integer
      k = 0
      For i = 0 To UBound(ssaStruct.CommandCode.value)
        If InStr(ssaStruct.CommandCode.value(i), " ") = 0 Then
          ReDim Preserve ssaValue(k)
          ssaValue(k) = ssaStruct.CommandCode.value(i)
          k = k + 1
        End If
      Next i
      ssaStruct.CommandCode.value = ssaValue
      If UBound(ssaStruct.CommandCode.value) = 0 Then
        ssaStruct.CommandCode.moved = False
      End If
    End If
    
    ''''''''''''''''''''''''''
    ' Analisi "QUALIFICA"
    ''''''''''''''''''''''''''
    ReDim ssaStruct.OpLogico(0) 'init
    ReDim ssaStruct.OpLogico(0).value(0) 'init
    keyLevel = 1  'init
    While Len(ssa)
      ReDim Preserve ssaStruct.Chiavi(keyLevel)
      
      ''''''''''''''''''''''''''''''''''''''''''''
      ' CHIAVE. Posizione relativa=(codesIndex:8)
      ''''''''''''''''''''''''''''''''''''''''''''
      ssaStruct.Chiavi(keyLevel).Key.pos = codesIndex
      ssaStruct.Chiavi(keyLevel).Key.len = 8
      ReDim Preserve ssaStruct.Chiavi(keyLevel).Key.value(0)
      ssaStruct.Chiavi(keyLevel).Key.value(0) = Mid(ssa, codesIndex, 8)   'TRIMMARE DOPO!
      'TMP
      If manageMove And Not SSAConstant Then
        checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.Chiavi(keyLevel).Key
      End If
      
      ''''''''''''''''''''''''''''''''''''''
      ' OPERATORE. Posizione relativa=(9:2)
      '''''''''''''''''''''''''''''''''''''
      'SQ - gestione MOVE/VALUES
      'ssaStruct.Chiavi(keyLevel).operatore = Mid(ssa, codesIndex + 8, 2)
      ssaStruct.Chiavi(keyLevel).operatore.pos = codesIndex + 8
      ssaStruct.Chiavi(keyLevel).operatore.len = 2
      ReDim ssaStruct.Chiavi(keyLevel).operatore.value(0)
      ssaStruct.Chiavi(keyLevel).operatore.value(0) = Mid(ssa, codesIndex + 8, 2)
      If GbTipoInPars = "PLI" Then 'normalizzazione
        Dim kk As Integer
        For kk = 0 To UBound(dataCmpStartValues)
          If IsNumeric(dataCmpStartValues(kk)) Then
            If CInt(dataCmpStartValues(kk)) + 1 - (codesIndex + 8) > 0 Then
              dataCmpStartValues(kk) = CInt(dataCmpStartValues(kk)) + 1 - (codesIndex + 8)
            End If
          End If
        Next
      End If
      If manageMove And Not SSAConstant Then
        'IRIS-DB: non chiaro perch� fa una combinazione delle combinazioni, forse per il PLI... tolto
        'IRIS-DB checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.Chiavi(keyLevel).operatore, True
        checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.Chiavi(keyLevel).operatore 'IRIS-DB
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''
      ' VALORE. Posizione relativa=(11:x). Termina con:
      ' + ")"
      ' + OPERATORE RELAZIONALE:
      '   - *,& (AND)
      '   - +,! (OR)
      '''''''''''''''''''''''''''''''''''''''''''''''''
      codesIndex = codesIndex + 10  'verificare
      For valueIndex = codesIndex To Len(ssa)
        Select Case Mid(ssa, valueIndex, 1)
'IRIS-DB          Case "+", "!" 'OR
          Case "+", "!", "|" 'OR 'IRIS-DB: not clear why it was using exclamation mark instead of pipe, so I'm leaving also that value
            ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico) + 1)
            ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0)
            'SQ - gestione MOVE/VALUES
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0) = "+"
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).pos = valueIndex
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).len = 1
            If manageMove And Not SSAConstant Then
              checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.OpLogico(UBound(ssaStruct.OpLogico))
            End If

            ssaStruct.Chiavi(keyLevel).Valore.KeyStart = codesIndex
            ssaStruct.Chiavi(keyLevel).Valore.Lunghezza = valueIndex - codesIndex
            ssaStruct.Chiavi(keyLevel).Valore.Valore = "'" & Mid(ssa, codesIndex, valueIndex - codesIndex) & "'"
            
            'aggiorno la "base" (uso codesIndex come "startValueIndex")
            codesIndex = valueIndex + 1
            keyLevel = keyLevel + 1
            Exit For
'IRIS-DB          Case "*", "&" 'AND
          Case "*", "&", "#" 'AND 'IRIS-DB per ora la considero come la normale AND, ma va rivista
            If Mid(ssa, valueIndex, 1) = "#" Then 'IRIS-DB
              addSegnalazione ssa, "IRIS#", ssa 'IRIS-DB
            End If 'IRIS-DB
            ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico) + 1)
            ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0)
            'SQ - gestione MOVE/VALUES
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0) = "*"
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).pos = valueIndex
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).len = 1
            If manageMove And Not SSAConstant Then
              checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.OpLogico(UBound(ssaStruct.OpLogico))
            End If
            
            ssaStruct.Chiavi(keyLevel).Valore.KeyStart = codesIndex
            ssaStruct.Chiavi(keyLevel).Valore.Lunghezza = valueIndex - codesIndex
            ssaStruct.Chiavi(keyLevel).Valore.Valore = "'" & Mid(ssa, codesIndex, valueIndex - codesIndex) & "'"
            'aggiorno la "base" (uso codesIndex come "startValueIndex")
            codesIndex = valueIndex + 1
            keyLevel = keyLevel + 1
            Exit For
          Case ")"
            'FINE!
            ssaStruct.Chiavi(keyLevel).Valore.KeyStart = codesIndex
            ssaStruct.Chiavi(keyLevel).Valore.Lunghezza = valueIndex - codesIndex '+ 1
            ssaStruct.Chiavi(keyLevel).Valore.Valore = "'" & Mid(ssa, codesIndex, valueIndex - codesIndex) & "'"
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Caso particolare!
            'Es.: "SPJROOT *--(PPJKEY  >=87                  &PPJKEY  <=87                  )"
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Len(ssa) > valueIndex And Right(ssa, 1) = ")" Then
              ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico) + 1)
              ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0)
              
              'SQ - gestione MOVE/VALUES
              ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0) = ")" '!!!
              ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).pos = valueIndex
              ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).len = 1
              If manageMove And Not SSAConstant Then
                checkMove_SSA dataCmp, dataCmpValues, dataCmpStartValues, ssaStruct.OpLogico(UBound(ssaStruct.OpLogico))
              End If
              ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).moved = True 'FORZATO!
              
              ssaStruct.Chiavi(keyLevel).Valore.KeyStart = codesIndex
              ssaStruct.Chiavi(keyLevel).Valore.Lunghezza = valueIndex - codesIndex
              ssaStruct.Chiavi(keyLevel).Valore.Valore = "'" & Mid(ssa, codesIndex, valueIndex - codesIndex) & "'"
              'aggiorno la "base" (uso codesIndex come "startValueIndex")
              codesIndex = valueIndex + 1
              keyLevel = keyLevel + 1
              Exit For
            Else
              ssaStruct.valida = True
              ssa = ""
            End If
          Case Else
            'NOP
            'Non mi serve il valore effettivo
        End Select
      Next valueIndex
    Wend
    
    'TMP
    If manageMove And Not SSAConstant Then
      checkMove dataCmp, ssaStruct
    End If
  Else
    ssaStruct.valida = False
    ''''''''''''''''''''''''''''''''''''''''
    ' SQ - 19-04-06
    ' Nuova segnalazione (I02)
    ''''''''''''''''''''''''''''''''''''''''
    addSegnalazione ssaStruct.SSAName, "NOSSAVALUE", ssaStruct.SSAName
  End If
  
  '''''''''''''''''''''''
  ' Update ssaCache
  '''''''''''''''''''''''
  ssaCache(UBound(ssaCache)) = ssaStruct
  ReDim Preserve ssaCache(UBound(ssaCache) + 1)
  Exit Sub
errSSA:
  ssaStruct.valida = False
  'Resume
  'CAMBIARE LA SEGNALAZIONE!
  addSegnalazione ssa, "SSAWRONG", ssa
End Sub
'SQ - Da buttare via: usare dappertutto la nuova getSegment_byArea
Public Function getIdSegmento_By_MgTable(wRecordArea As String) As Long
   Dim tb As Recordset
   m_Fun.writeLog "getIdSegmento_By_MgTable used, but it should not" & GbIdOggetto & "-" & GbOldNumRec
     
  Set tb = m_Fun.Open_Recordset("select * From MgData_Cmp Where Nome = '" & wRecordArea & "'")
  If tb.RecordCount = 1 Then
    getIdSegmento_By_MgTable = tb!idSegmento
  Else
    getIdSegmento_By_MgTable = 0
  End If
  tb.Close
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 30-01-07 Ex getIdSegmento_By_MgTable
' Ottiene il segmento a partire dall'IOarea associata al segmento
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub getSegment_byArea(istrDli As blkIstrDli)
  Dim rs As Recordset
  'IRIS-DB in realt� � corretto... m_Fun.writeLog "getSegment_byArea used, but it should not" & GbIdOggetto & "-" & GbOldNumRec
   
  Set rs = m_Fun.Open_Recordset("SELECT a.idSegmento as idSegmento,a.nome as nome,a.maxlen as segmLen FROM PsDli_Segmenti as a, MgData_Cmp as b " & _
                                "WHERE b.Nome='" & istrDli.Record & "' AND a.idSegmento=b.idSegmento")
  If rs.RecordCount = 1 Then
    'stefano:
    istrDli.IdSegmenti = rs!idSegmento
    istrDli.IdSegm = rs!idSegmento
    istrDli.Segment = rs!nome
    If Len(istrDli.SegLen) = 0 Then 'Chiarire questo controllo... posso avere la LEN nell'istruzione?! (per le EXEC)
      istrDli.SegLen = rs!segmLen
    End If
  Else
    istrDli.IdSegm = 0
    istrDli.IdSegmenti = ""
  End If
  rs.Close
End Sub

Public Sub getSegment_byArea_IRIS(istrDli As blkIstrDli) 'IRIS-DB
  Dim rs As Recordset
  Dim segmentName As String
  Dim s() As String
  Dim t() As String
  ' m_Fun.writeLog "getSegment_byArea_IRIS used, but it should not" & GbIdOggetto & "-" & GbOldNumRec
  'qualche forzatura
  s = Split(istrDli.Record, "-")
  If Len(s(0)) > 3 Then
    segmentName = s(0)
  Else
    segmentName = s(1)
  End If
  If Len(TabIstr.DBD) Then
    Set rs = m_Fun.Open_Recordset("SELECT idSegmento, Maxlen FROM PsDli_Segmenti as a, bs_oggetti as b " & _
                                 "WHERE a.Nome= '" & segmentName & "' and b.nome = '" & TabIstr.DBD & "' and a.idoggetto = b.idoggetto")
  Else
    Set rs = m_Fun.Open_Recordset("SELECT idSegmento, Maxlen FROM PsDli_Segmenti " & _
                                 "WHERE Nome= '" & segmentName & "'")
  End If
  If rs.RecordCount = 1 Then ' solo per uno altrimenti meglio che lo lasci a zero
    istrDli.IdSegmenti = rs!idSegmento
    istrDli.IdSegm = rs!idSegmento
    istrDli.Segment = segmentName
    istrDli.Segmenti = segmentName 'boh, mi sono rotto veramente il cazzo pdmc
    If Len(istrDli.SegLen) = 0 Then
      istrDli.SegLen = rs!MaxLen
    End If
  Else
    istrDli.IdSegm = 0
    istrDli.IdSegmenti = ""
  End If
  rs.Close
End Sub

Public Function extractInstructionDLI(wIstr As String, FlagIsVariabile As Boolean, Optional ByVal empiricMode As Boolean = False) As String
   Dim i As Long
   Dim j As Long
   Dim wFind As Boolean
   
   Select Case Trim(UCase(wIstr))
      Case "GU", "GHU", "GN", "GHN", "GHNP", "GNP", "ISRT", "DLET", "REPL", "OPEN", "CLSE", "REST", "CLOSE", "WRTE", "WRITE", "READ", "SYNC"
         FlagIsVariabile = False
         extractInstructionDLI = Mid(wIstr & Space(4), 1, 4)
      Case Else
         FlagIsVariabile = True
         
         'E' una variabile
         If empiricMode = True Then
            For i = 1 To UBound(gbIstrAlias)
               For j = 1 To UBound(gbIstrAlias(i).Alias)
                  If InStr(1, wIstr, gbIstrAlias(i).Alias(j)) > 0 Then
                     extractInstructionDLI = gbIstrAlias(i).Mnemonics
                     wFind = True
                     Exit For
                  End If
               Next j
               
               If wFind Then Exit For
            Next i
            
         Else
            extractInstructionDLI = wIstr
         End If
   End Select
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Funzionalita' principale: ottengo wIdDBD()
'Effetto collaterale:
' - IdPsb (? - giusto perche' viene usato sotto... ma non servirebbe)
' - TabIstr.PsbId
' - TabIstr.PSBName
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub cercaDbd()
   Dim tb As Recordset
   Dim tb1 As Recordset
   Dim tb2 As Recordset
   Dim tb3 As Recordset
   Dim i As Integer
   Dim dbdWhereCond As String
   Dim psbWhereCond As String
   Dim segmentsQuery As String
   
   'Preparo la query, differenziata...
   'VERIFICARE CHE NOME SEGMENTO NON SIA VARIABILE!!!...
   If UBound(TabIstr.BlkIstr) = 1 Then
      segmentsQuery = "select IdOggetto from PsDli_Segmenti where nome = '" & Replace(TabIstr.BlkIstr(1).Segment, "'", "") & "'"
   Else
      'filtro su tutti i nomi di segmento dell'istruzione...
      'es:
      'select idOggetto from
      '  (SELECT psDLI_Segmenti.IdOggetto, Count(*) AS occurs
      '  From psDLI_Segmenti
      '  WHERE (Nome='XXX' Or Nome='YYY')
      '  GROUP BY IdOggetto)
      'Where Occurs = 2
      segmentsQuery = "select IdOggetto from " & _
                        "(select idOggetto,count(*) as occurs from psDLI_Segmenti " & _
                        "where ("
      For i = 1 To UBound(TabIstr.BlkIstr)
         segmentsQuery = segmentsQuery & "nome = '" & Replace(TabIstr.BlkIstr(i).Segment, "'", "") & "'"
         If i = UBound(TabIstr.BlkIstr) Then
            segmentsQuery = segmentsQuery & ")"
         Else
            segmentsQuery = segmentsQuery & " OR "
         End If
      Next i
      segmentsQuery = segmentsQuery & _
                         " group by idOggetto)" & _
                         " where occurs =" & UBound(TabIstr.BlkIstr)
   End If

   'CERCO TUTTI I PSB SCHEDULATI PER IL PROGRAMMA:
   Set tb = m_Fun.Open_Recordset("select IdOggettoR,NomeComponente from PsRel_Obj where relazione = 'PSB' and idoggettoc = " & GbIdOggetto)
  'QUESTA QUERY VIENE FATTA N VOLTE... TENERE LA PRIMA...
   Select Case tb.RecordCount
    Case 0
      ''''''''''''''''''''''''
      'NESSUNA SCHEDULAZIONE:
      ''''''''''''''''''''''''
      TabIstr.psbId = 0
      TabIstr.psbName = "" 'Tb!nomeComponente
    Case 1
      '''''''''''
      'PSB NOTO:
      '''''''''''
      TabIstr.psbId = tb!idOggettoR
      'SQ-2: serve il IdPsb globale?
      IdPsb = tb!idOggettoR
      TabIstr.psbName = tb!nomecomponente
            
      'CERCO I DBD:
      ' a) PCB NOTO ==> DBD
      ' b) NO PCB   ==> TUTTI i DBD legati al PSB
      If IsNumeric(TabIstr.pcb) Then
         'Accesso diretto con PCB
         If SwBatch Then
            'PCB 1: IO-PCB...
            Set tb1 = m_Fun.Open_Recordset("select bs_oggetti.idOggetto from bs_oggetti,PsDli_Psb where bs_oggetti.nome=PsDli_Psb.DbdName and PsDli_Psb.IdOggetto = " & tb!idOggettoR & " and numPcb=" & TabIstr.pcb - 1)
         Else
            Set tb1 = m_Fun.Open_Recordset("select bs_oggetti.idOggetto from bs_oggetti,PsDli_Psb where bs_oggetti.nome=PsDli_Psb.DbdName and PsDli_Psb.IdOggetto = " & tb!idOggettoR & " and numPcb=" & TabIstr.pcb)
         End If
      Else
         'Tiro su tutti gli eventuali DBD
         Set tb1 = m_Fun.Open_Recordset("select distinct bs_oggetti.idOggetto from bs_oggetti,PsDli_Psb where bs_oggetti.nome=PsDli_Psb.DbdName and PsDli_Psb.IdOggetto = " & tb!idOggettoR)
          'perche' l'altro giro va su file?????????????????????????
      End If
      'ATTENZIONE: IN CASO DI ANOMALIE (DBD NON ESISTENTI - SECONDO tool) TB1 E' VUOTO!... GESTIRE!
      'SQ-2:
      If tb1.RecordCount = 0 Then
         Exit Sub
      End If
      'Costruisco il filtro (where condition) con gli IdDBD potenzialmente validi:
      dbdWhereCond = "("
      While Not tb1.EOF
         dbdWhereCond = dbdWhereCond & "idOggetto = " & tb1(0)
         tb1.MoveNext
         If tb1.EOF Then
            dbdWhereCond = dbdWhereCond & ")"
         Else
            dbdWhereCond = dbdWhereCond & " OR "
         End If
      Wend
      '"specializzo" la query...
      segmentsQuery = segmentsQuery & " AND " & dbdWhereCond
    Case Else
      '''''''''
      'N PSB
      '''''''''
      ' Come il caso precedente, ma fitro gli N PSB sulla query dei PCB...
      '''''''''
      'costruisco WHERE con tutti i PSB in OR...
      psbWhereCond = "("
      While Not tb.EOF
         psbWhereCond = psbWhereCond & "PsDli_Psb.IdOggetto = " & tb!idOggettoR
         tb.MoveNext
         If tb.EOF Then
            psbWhereCond = psbWhereCond & ")"
         Else
            psbWhereCond = psbWhereCond & " OR "
         End If
      Wend
      'CERCO I DBD:
      ' a) PCB NOTO ==> DBD
      ' b) NO PCB   ==> TUTTI i DBD legati al PCB
      If IsNumeric(TabIstr.pcb) Then
         'Accesso diretto con PCB
         If SwBatch Then
            'PCB 1: IO-PCB...
            Set tb1 = m_Fun.Open_Recordset("select distinct bs_oggetti.idOggetto,DbdName from bs_oggetti,PsDli_Psb where bs_oggetti.nome=PsDli_Psb.DbdName and " & psbWhereCond & " and numPcb=" & TabIstr.pcb - 1)
         Else
            Set tb1 = m_Fun.Open_Recordset("select distinct bs_oggetti.idOggetto,DbdName from bs_oggetti,PsDli_Psb where bs_oggetti.nome=PsDli_Psb.DbdName and " & psbWhereCond & " and numPcb=" & TabIstr.pcb)
         End If
      Else
         'Tiro su tutti gli eventuali DBD
         Set tb1 = m_Fun.Open_Recordset("select distinct bs_oggetti.idOggetto,DbdName from bs_oggetti,PsDli_Psb where bs_oggetti.nome=PsDli_Psb.DbdName and " & psbWhereCond)
          'perche' l'altro giro va su file?????????????????????????
      End If
      'ATTENZIONE: IN CASO DI ANOMALIE (DBD NON ESISTENTI - SECONDO tool) TB1 E' VUOTO!... GESTIRE!
      'SQ-2:
      If tb1.RecordCount = 0 Then
         Exit Sub
      End If
      'Costruisco il filtro (where condition) con gli IdDBD potenzialmente validi:
      dbdWhereCond = "("
      While Not tb1.EOF
         dbdWhereCond = dbdWhereCond & "idOggetto = " & tb1(0)
         tb1.MoveNext
         If tb1.EOF Then
            dbdWhereCond = dbdWhereCond & ")"
         Else
            dbdWhereCond = dbdWhereCond & " OR "
         End If
      Wend
      '"specializzo" la query...
      segmentsQuery = segmentsQuery & " AND " & dbdWhereCond
  End Select
  
  'Ho la query finale, differenziata a seconda dei casi:
  'Ottengo l'array di DBD!
  Set tb = m_Fun.Open_Recordset(segmentsQuery)
  If tb.RecordCount > 0 Then
   ReDim wIdDBD(tb.RecordCount - 1)
   For i = 0 To tb.RecordCount - 1
      wIdDBD(i) = tb!idOggetto
      tb.MoveNext
   Next i
  '25-05-05:
  ElseIf Len(dbdWhereCond) Then
    'tolgo il filtro sui PSB!!!!!!!! (se ne ho di variabili mi frega...)
    'integrare meglio sopra...
    segmentsQuery = Left(segmentsQuery, InStr(segmentsQuery, " AND "))
    Set tb = m_Fun.Open_Recordset(segmentsQuery)
    If tb.RecordCount > 0 Then
      ReDim wIdDBD(tb.RecordCount - 1)
      For i = 0 To tb.RecordCount - 1
         wIdDBD(i) = tb!idOggetto
         tb.MoveNext
      Next i
    End If
  End If
  tb.Close
End Sub

Function AnalizzaExecDli(NomeDb, IdPsb, istruzione, wIstr) As Boolean
Dim wTb As Recordset
Dim wTbR As Recordset

Dim k As Integer

Dim k3 As Integer

Dim SwT2 As Boolean
Dim SwW2 As Boolean
Dim SwX2 As Boolean

Dim wStr1 As String
Dim wWhere1 As String
Dim wWhere As String
Dim wLen As String
Dim wIdDb As Double
Dim Ind As Integer

  TabIstr.DBD = NomeDb
  TabIstr.psbId = IdPsb
  
  Set wTb = m_Fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & TabIstr.psbId)
  If wTb.RecordCount > 0 Then
     TabIstr.psbName = wTb!nome
  End If
  wTb.Close
  
  TabIstr.istr = wIstr

  If wIstr = "PCB" Or wIstr = "TERM" Or wIstr = "SCHD" Then
     AnalizzaExecDli = True
     Exit Function
  End If
  
  Ind = 0
'   Debug.Print Istruzione
  TabIstr.pcb = SelezionaParam(istruzione, "PCB")
  k = InStr(istruzione, " SEGMENT")
  If k > 0 Then
     istruzione = Mid$(istruzione, k)
  End If
  k = InStr(istruzione, "END-EXEC")
  If k = 0 Then k = Len(istruzione)
  
  istruzione = Trim(Mid$(istruzione, 1, k - 1))
  While Len(istruzione) > 0
      'chiarire perche' cerca segment anche sopra...
      k = InStr(istruzione, " SEGMENT")
      If k = 0 Then
        k = Len(istruzione) + 1
      End If
      wStr1 = Mid$(istruzione, 1, k - 1)
      istruzione = Trim(Mid$(istruzione, k))
      Ind = Ind + 1
      ReDim Preserve TabIstr.BlkIstr(Ind)
      TabIstr.BlkIstr(Ind).valida = True
      TabIstr.BlkIstr(Ind).Segment = SelezionaParam(wStr1, "SEGMENT")
      If ActParsNoPsb Then
         Set wTb = m_Fun.Open_Recordset("Select * from PsDli_Segmenti where nome = '" & TabIstr.BlkIstr(Ind).Segment & "'")
         If wTb.RecordCount > 0 Then
           wTb.MoveLast
           If wTb.RecordCount = 1 Then
              Set wTb = m_Fun.Open_Recordset("select * from bs_Oggetti where idoggetto = " & wTb!idOggetto)
              If wTb.RecordCount > 0 Then
                 NomeDb = wTb!nome
                 k = InStr(NomeDb, ".")
                 If k > 0 Then NomeDb = Mid$(NomeDb, 1, k - 1)
                 If NomeDb = "PDBX2DA" Or NomeDb = "PDBW2DA" Or NomeDb = "PDBT2DA" Then
'                    Set wTbR = m_fun.Open_Recordset("select * from psrel_obj where idoggettoc = " & GbIdOggetto & " and relazione = 'CPY' and nomecomponente = 'PSELG00'")
'                    If wTbR.RecordCount > 0 Then
'                       NomeDb = "PDBL2DA"
'                    End If
'                    Set wTbR = m_fun.Open_Recordset("select * from psrel_obj where idoggettoc = " & GbIdOggetto & " and relazione = 'CPY' and nomecomponente = 'PSEPU00'")
'                    If wTbR.RecordCount > 0 Then
'                       NomeDb = "PDBP2DA"
'                    End If
                    SwT2 = False
                    SwX2 = False
                    SwW2 = False
                    Set wTbR = m_Fun.Open_Recordset("select * from psrel_obj where idoggettoc = " & GbIdOggetto & " and relazione = 'CPY' and nomecomponente = 'KSEGTA1'")
                    If wTbR.RecordCount > 0 Then SwT2 = True
                    Set wTbR = m_Fun.Open_Recordset("select * from psrel_obj where idoggettoc = " & GbIdOggetto & " and relazione = 'CPY' and nomecomponente = 'PSEGTRAN'")
                    If wTbR.RecordCount > 0 Then SwW2 = True
                    Set wTbR = m_Fun.Open_Recordset("select * from psrel_obj where idoggettoc = " & GbIdOggetto & " and relazione = 'CPY' and nomecomponente = 'PREX010'")
                    If wTbR.RecordCount > 0 Then SwX2 = True
                    
                    If SwT2 Then
                       NomeDb = "PDBT2DA"
                    End If
                    If SwW2 Then
                       NomeDb = "PDBW2DA"
                    End If
                    If SwX2 Then
                       NomeDb = "PDBX2DA"
                    End If
                    If (SwT2 = True And SwX2 = True) Or _
                       (SwT2 = True And SwW2 = True) Or _
                       (SwX2 = True And SwW2 = True) Then
  '                     Stop
                    End If
                 End If
                 TabIstr.DBD = NomeDb
              End If
           End If
         End If
      End If
      TabIstr.BlkIstr(Ind).SegLen = SelezionaParam(wStr1, "SEGLENGTH")
      TabIstr.BlkIstr(Ind).Search = SelezionaParam(wStr1, "SEARCH")
      Select Case Trim(TabIstr.istr)
         Case "GU", "GN", "GNP"
            TabIstr.BlkIstr(Ind).Record = SelezionaParam(wStr1, "INTO")
         Case "ISRT", "REPL", "DLET"
            TabIstr.BlkIstr(Ind).Record = SelezionaParam(wStr1, "FROM")
      End Select
      wWhere = FormattaWhere(SelezionaParam(wStr1, "WHERE"))  'a cosa serve FormattaWhere?
      'cosa fa qui? (quante trim?)
      If Len(wWhere) Then
        k = InStr(wWhere, " ")
        If InStr(wWhere, "=") < k Then k = 0
         If k = 0 Then
           k = InStr(wWhere, "=")
           If k = 0 Then k = InStr(wWhere, ">")
           If k > 0 Then
               If Mid$(wWhere, k - 1, 1) < "A" Or Mid$(wWhere, k - 1, 1) > "Z" Then
                  If Mid$(wWhere, k - 1, 1) < "0" Or Mid$(wWhere, k - 1, 1) > "9" Then
                     k = k - 1
                  End If
               End If
               wWhere = Mid$(wWhere, 1, k - 1) & " " & Mid$(wWhere, k)
           End If
           k = InStr(wWhere, "=")
           If k = 0 Then k = InStr(wWhere, ">")
           If k > 0 Then
               If Mid$(wWhere, k + 1, 1) < "A" Or Mid$(wWhere, k + 1, 1) > "Z" Then
                  If Mid$(wWhere, k + 1, 1) < "0" Or Mid$(wWhere, k + 1, 1) > "9" Then
                     k = k + 1
                  End If
               End If
               wWhere = Mid$(wWhere, 1, k) & " " & Mid$(wWhere, k + 1)
           End If
           k = InStr(wWhere, " ")
        End If
        k3 = 0
        
        ReDim Preserve TabIstr.BlkIstr(Ind).KeyDef(0)
        wLen = SelezionaParam(wStr1, "FIELDLENGTH")
        'itero le varie condizioni in AND e OR:
        While Len(wWhere) > 0
          k3 = k3 + 1
          ReDim Preserve TabIstr.BlkIstr(Ind).KeyDef(k3)
          
          k = InStr(wWhere, " ")
          If k > 0 Then
            TabIstr.BlkIstr(Ind).KeyDef(k3).Key = Trim(Mid$(wWhere, 1, k - 1))
          Else
            TabIstr.BlkIstr(Ind).KeyDef(k3).Key = wWhere
          End If
          wWhere = Trim(Mid$(wWhere, k))
         
          k = InStr(wWhere, " ")
          TabIstr.BlkIstr(Ind).KeyDef(k3).Op = Trim(Mid$(wWhere, 1, k - 1))
          wWhere = Trim(Mid$(wWhere, k))
          
          k = InStr(wWhere, " ")
          If k = 0 Then k = Len(wWhere) + 1
          'SQ:
          wStr1 = Trim(Mid$(wWhere, 1, k - 1))
          wWhere = Trim(Mid$(wWhere, k))
          'VALORE DI CONFRONTO: PUO' ESSERE DI TIPO "ciccio OF ciaccio"
          TabIstr.BlkIstr(Ind).KeyDef(k3).KeyVal = wStr1
          k = InStr(wWhere, " ")
          If k > 0 Then
            wStr1 = Trim(Mid$(wWhere, 1, k - 1))
            wWhere = Trim(Mid$(wWhere, k))
            If wStr1 = "OF" Then
                'USARE GETTOKEN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                k = InStr(wWhere, " ")
                If k = 0 Then k = Len(wWhere) + 1
                wStr1 = Trim(Mid$(wWhere, 1, k - 1))
                wWhere = Trim(Mid$(wWhere, k))
                'SQ - SENTIRE COME SI GESTISCE QUESTO CAMPO
                TabIstr.BlkIstr(Ind).KeyDef(k3).KeyVal = TabIstr.BlkIstr(Ind).KeyDef(k3).KeyVal & " OF " & wStr1
                k = InStr(wWhere, " ")
                If k > 0 Then
                    wStr1 = Trim(Mid$(wWhere, 1, k - 1))
                    wWhere = Trim(Mid$(wWhere, k))
                    TabIstr.BlkIstr(Ind).KeyDef(k3).KeyBool = wStr1
                End If
            Else
                'SQ - operatore relazionale (controllare)
                'MsgBox "SQ-AnalizzaExecDli: trovato " & wStr1
                m_Fun.writeLog "SQ-AnalizzaExecDli: trovato " & wStr1
                TabIstr.BlkIstr(Ind).KeyDef(k3).KeyBool = wStr1
            End If
          End If
          'OF o OPERATORE RELAZIONALE
          'cosa centra 'sta parte qui???
          'K = InStr(wLen, ",")
          'If K = 0 Then K = Len(wLen) + 1
          'TabIstr.BlkIstr(Ind).KeyDef(k3).KeyLen = Trim(Mid$(wLen, 1, K - 1))
          'SQ - VERIFICARE!!!!!!!!!!!!!!!!!!!!
          TabIstr.BlkIstr(Ind).KeyDef(k3).KeyLen = wLen
          'wLen = Trim(Mid$(wLen, K + 1))
          '-------------------------------
          'If Len(wWhere) > 0 Then
          '  K = InStr(wWhere, " ")
          '  TabIstr.BlkIstr(Ind).KeyDef(k3).KeyBool = Trim(Mid$(wWhere, 1, K - 1))
          '  wWhere = Trim(Mid$(wWhere, K))
          'End If
        Wend
      Else
        'NON HO LA WHERE... posso avere delle KEYS ugualmente?! boh... controllare dopo...
        'IRIS-DB veramente la KEYS � in alternativa alla WHERE: o l'una o l'altra
        wStr1 = SelezionaParam(wStr1, "KEYS")
        If Len(wStr1) Then
          ReDim TabIstr.BlkIstr(Ind).KeyDef(1)
          TabIstr.BlkIstr(Ind).KeyDef(1).KeyVal = wStr1
          TabIstr.BlkIstr(Ind).KeyDef(1).KeyLen = SelezionaParam(wStr1, "KEYLENGTH")
        Else
          ReDim TabIstr.BlkIstr(Ind).KeyDef(0)
        End If
      End If
      TabIstr.BlkIstr(Ind).NomeRout = CreaNomeRout(TabIstr.DBD)
  Wend

  AnalizzaExecDli = True
  If UBound(TabIstr.BlkIstr) > MaxSeg Then
      MaxSeg = UBound(TabIstr.BlkIstr)
  '    MsgBox MaxSeg
  End If
End Function

Public Function getIdSegmento(wIdOggetto As Long, wNomeSeg As String) As Long
   Dim rs As Recordset
   
   Set rs = m_Fun.Open_Recordset("Select * From PsDli_segmenti Where IdOggetto = " & wIdOggetto & " And Nome = '" & wNomeSeg & "'")
   If rs.RecordCount = 1 Then
      getIdSegmento = rs!idSegmento
   End If
   
   rs.Close
End Function

Public Function getNomeSegmento(wIdSegmento As Long) As String
   Dim rs As Recordset
   
   Set rs = m_Fun.Open_Recordset("Select * From PsDli_segmenti Where IdSegmento = " & wIdSegmento)
   
   If rs.RecordCount = 1 Then
      getNomeSegmento = rs!nome
   End If
   
   rs.Close
End Function
Public Function getNomeDBD(wIdDBD As Long) As String
   Dim rs As Recordset
   Set rs = m_Fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & wIdDBD)
   If rs.RecordCount = 1 Then
      getNomeDBD = rs!nome
   End If
   rs.Close
End Function

Public Function getLenSegmento(wIdOggetto As Long, wNomeSeg As String) As Long
   Dim rs As Recordset
   
   Set rs = m_Fun.Open_Recordset("Select * From PsDli_segmenti Where IdOggetto = " & wIdOggetto & " And Nome = '" & wNomeSeg & "'")
   
   If rs.RecordCount = 1 Then
      getLenSegmento = rs!MaxLen
   End If
   
   rs.Close
End Function

Public Function getLenSegmentoByIdSegmento(wIdSegmento As Long) As Long
   Dim rs As Recordset
   
   Set rs = m_Fun.Open_Recordset("Select * From PsDli_segmenti Where IdSegmento = " & wIdSegmento)
   
   If rs.RecordCount = 1 Then
      getLenSegmentoByIdSegmento = rs!MaxLen
   End If
   
   rs.Close
End Function
'''''''''''''''''''''''''''''''''''''''''''''
' SQ - 21-04-06
'''''''''''''''''''''''''''''''''''''''''''''
Sub getTabIstr()
  Dim wDliRec As String, ssaValue As String, operator As String
  Dim wIdSegm As Long
  Dim i As Integer, Ind As Integer, k As Integer, j As Integer
  
  'stefano: ma perch� l'area la parsa solo se c'� l'SSA? SONO INDIPENDENTI, ci sono istruzioni
  ' senza SSA, ma l'area le GU e le insert ce l'hanno tutte...
  
  If UBound(wDliSsa) = 0 Then
    'DELETE/REPLACE (dovrebbero essere solo questi casi. verificare)
    ReDim TabIstr.BlkIstr(1)
    ReDim TabIstr.BlkIstr(1).KeyDef(0)
    ReDim TabIstr.BlkIstr(1).KeyDef(1)  '???
    TabIstr.BlkIstr(1).SSAName = ""
    'Con naming convention (per es.)
    TabIstr.BlkIstr(1).Segment = TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0)
    'Mauro 19-03-2007
    'AC k?!
    'For i = 0 To UBound(TabIstrSSa(k).NomeSegmento.value)
    For i = 0 To UBound(TabIstrSSa(0).NomeSegmento.value)
      If Len(Trim(TabIstrSSa(0).NomeSegmento.value(i))) Then
        'Controllo duplicati:
        If InStr(TabIstr.BlkIstr(1).Segmenti, TabIstrSSa(0).NomeSegmento.value(i)) = 0 Then
          TabIstr.BlkIstr(1).Segmenti = TabIstr.BlkIstr(1).Segmenti & IIf(i, ";", "") & TabIstrSSa(0).NomeSegmento.value(i)
        End If
      Else
        'Ci vuole la segnalazione!
        'addSegnalazione "", "UNKNOWNOPERATOR", Trim(TabIstrSSa(1).Chiavi(j).operatore.value(i))
      End If
    Next
    '''''''''''''''''
    TabIstr.BlkIstr(1).Record = TabIstrSSa(UBound(TabIstrSSa)).Struttura ' parametri
    'AC 10/10/07 in precedenza valida se len(record)
    TabIstr.BlkIstr(1).valida = Len(Trim(TabIstr.BlkIstr(1).Segmenti))
    'TabIstr.BlkIstr(1).NomeRout = CreaNomeRout(TabIstr.DBD)
    TabIstr.BlkIstr(1).qualified = TabIstrSSa(UBound(TabIstrSSa)).Qualificatore
  Else
    For Ind = 1 To UBound(wDliSsa)
      ReDim Preserve TabIstr.BlkIstr(Ind)
      ReDim Preserve TabIstr.BlkIstr(Ind).KeyDef(0)
      TabIstr.BlkIstr(Ind).valida = True  'init
      'Area di input: solo sull'ultimo livello (ovviamente)
      If Ind = UBound(wDliSsa) Then
         TabIstr.BlkIstr(Ind).Record = TabIstrSSa(UBound(TabIstrSSa)).Struttura ' parametri  'sarebbe wDliRec
      Else
         TabIstr.BlkIstr(Ind).Record = ""
      End If
      Dim SwElab As Boolean
      
      TabIstr.BlkIstr(Ind).SSAName = wDliSsa(Ind)
      'gSSA????????????????????????????????
      For IndSSA = 1 To UBound(gSSA)
        If wDliSsa(Ind) = gSSA(IndSSA).Name Then
           SwElab = True
           Exit For
        End If
      Next IndSSA
      If Not SwElab Then
        IndSSA = UBound(gSSA) + 1
        ReDim Preserve gSSA(IndSSA)
        gSSA(IndSSA).Name = wDliSsa(Ind)
        ReDim Preserve gSSA(IndSSA).parentesi(0)
        ReDim Preserve gSSA(IndSSA).CodCom(0)
        ReDim Preserve gSSA(IndSSA).FIELD(0)
        ReDim Preserve gSSA(IndSSA).Op(0)
        ReDim Preserve gSSA(IndSSA).Segm(0)
        ReDim Preserve gSSA(IndSSA).value(0)
      End If
    Next Ind
          
    For k = 1 To UBound(TabIstrSSa)
      ReDim Preserve TabIstr.BlkIstr(k).KeyDef(0)
      'AC
      TabIstr.BlkIstr(k).moved = TabIstrSSa(k).moved
      'If Len(TabIstrSSa(k).NomeSegmento.value(0)) Then
      If Len(Trim(TabIstrSSa(k).NomeSegmento.value(0))) Then
        TabIstr.BlkIstr(k).Segment = Trim(TabIstrSSa(k).NomeSegmento.value(0))
        'Mauro 19-03-2007
        ''''''''''''''''''''''''''''
        For i = 0 To UBound(TabIstrSSa(k).NomeSegmento.value)
          If Len(Trim(TabIstrSSa(k).NomeSegmento.value(i))) Then
            'Controllo duplicati:
            If InStr(TabIstr.BlkIstr(k).Segmenti, TabIstrSSa(k).NomeSegmento.value(i)) = 0 Then
              TabIstr.BlkIstr(k).Segmenti = TabIstr.BlkIstr(k).Segmenti & IIf(i, ";", "") & TabIstrSSa(k).NomeSegmento.value(i)
            End If
          Else
            'Ci vuole la segnalazione!
            'addSegnalazione "", "UNKNOWNOPERATOR", Trim(TabIstrSSa(k).Chiavi(j).operatore.value(i))
          End If
        Next i
        ''''''''''''''''''''''''''''
      Else
        TabIstr.BlkIstr(k).valida = False
      End If
      TabIstr.BlkIstr(k).segment_moved = TabIstrSSa(k).NomeSegmento.moved
      
      'QUALIFICAZIONE (MULTIPLA)
      TabIstr.BlkIstr(k).qualified = TabIstrSSa(k).Qualificatore 'scomparir�
      TabIstr.BlkIstr(k).qualified_moved = TabIstrSSa(k).Qualificazione.moved
      TabIstr.BlkIstr(k).qual_unqual = False  'init
      'stefano: da correggere... per ora � una pezza...  ???? chiarire ????
      If TabIstrSSa(k).Qualificazione.len Then
        For i = 0 To UBound(TabIstrSSa(k).Qualificazione.value) 'Deve partire da 0! (casi scamuffi...)
          If TabIstrSSa(k).Qualificazione.value(i) = IIf(TabIstr.BlkIstr(k).qualified, " ", "(") Then
            TabIstr.BlkIstr(k).qual_unqual = True
            Exit For
          End If
        Next i
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''
      ' Command Codes
      '''''''''''''''''''''''''''''''''''''''''''''''''
      For i = 0 To UBound(TabIstrSSa(k).CommandCode.value)
        TabIstr.BlkIstr(k).CodCom = TabIstr.BlkIstr(k).CodCom & IIf(i, ";", "") & TabIstrSSa(k).CommandCode.value(i)
      Next
      TabIstr.BlkIstr(k).CodCom_moved = TabIstrSSa(k).CommandCode.moved
'IRIS-DB quando il command code � "C" la chiave non c'� perch� usa la chiave concatenata, quindi non dovrebbe esserci neanche la psistrdett_key QWERTY2
      If Not InStr(TabIstr.BlkIstr(k).CodCom, "C") > 0 Then
        If TabIstrSSa(k).Qualificatore Then
          For j = 1 To UBound(TabIstrSSa(k).Chiavi)
            ReDim Preserve TabIstr.BlkIstr(k).KeyDef(j)
                    
            If Len(TabIstrSSa(k).Chiavi(j).Key.value(0)) Then
              'IRIS-DB send a message when multiple field has been found, as later it is ignored
              If UBound(TabIstrSSa(k).Chiavi(j).Key.value) > 0 Then 'IRIS-DB
                addSegnalazione "IRIS", "IRISFLD", Trim(TabIstrSSa(k).Chiavi(j).Key.value(1)) 'IRIS-DB
              End If 'IRIS-DB
              TabIstr.BlkIstr(k).KeyDef(j).Key = Trim(TabIstrSSa(k).Chiavi(j).Key.value(0))
              TabIstr.BlkIstr(k).KeyDef(j).Key_moved = TabIstrSSa(k).Chiavi(j).Key.moved
            Else
              TabIstr.BlkIstr(k).valida = False
              'Segnalazione!
            End If
            TabIstr.BlkIstr(k).KeyDef(j).KeyStart = TabIstrSSa(k).Chiavi(j).Valore.KeyStart
            TabIstr.BlkIstr(k).KeyDef(j).KeyLen = TabIstrSSa(k).Chiavi(j).Valore.Lunghezza
            TabIstr.BlkIstr(k).KeyDef(j).KeyVal = TabIstrSSa(k).Chiavi(j).Valore.Valore
            TabIstr.BlkIstr(k).KeyDef(j).KeyVarField = TabIstrSSa(k).ssaStream
            
            If UBound(TabIstrSSa(k).OpLogico) Then
              'SQ MOVE
              'TabIstr.BlkIstr(k).KeyDef(j - 1).KeyBool = TabIstrSSa(k).OpLogico(j - 1).value(0)
              For i = 0 To UBound(TabIstrSSa(k).OpLogico(j - 1).value)
                TabIstr.BlkIstr(k).KeyDef(j - 1).KeyBool = TabIstr.BlkIstr(k).KeyDef(j - 1).KeyBool & IIf(i, ";", "") & TabIstrSSa(k).OpLogico(j - 1).value(i)
              Next
              TabIstr.BlkIstr(k).KeyDef(j - 1).KeyBool_moved = TabIstrSSa(k).OpLogico(j - 1).moved
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''
            ' Controllo operatore
            '''''''''''''''''''''''''''''''''''''''''''''''''
            'SQ - move/value
            For i = 0 To UBound(TabIstrSSa(k).Chiavi(j).operatore.value)
              operator = getOperator(Trim(TabIstrSSa(k).Chiavi(j).operatore.value(i)))
              If Len(operator) Then
                'Controllo duplicati:
                If InStr(TabIstr.BlkIstr(k).KeyDef(j).Op, operator) = 0 Then
                  TabIstr.BlkIstr(k).KeyDef(j).Op = TabIstr.BlkIstr(k).KeyDef(j).Op & _
                                                    IIf(Len(TabIstr.BlkIstr(k).KeyDef(j).Op), ";", "") & operator
                End If
              Else
                'Ci vuole la segnalazione!
                addSegnalazione "", "UNKNOWNOPERATOR", Trim(TabIstrSSa(k).Chiavi(j).operatore.value(i))
              End If
            Next i
            'Istruzione invalida se ho 0 valori validi
            If Len(TabIstr.BlkIstr(k).KeyDef(j).Op) = 0 Then
              TabIstr.BlkIstr(k).valida = False
              'SQ - Questa dovrebbe essere una segnalazione diversa da quella sopra!
              addSegnalazione "", "UNKNOWNOPERATOR", Trim(TabIstr.BlkIstr(k).KeyDef(j).Op)
            End If
            TabIstr.BlkIstr(k).KeyDef(j).Op_moved = TabIstrSSa(k).Chiavi(j).operatore.moved
            gSSA(k).value(0) = TabIstrSSa(k).Chiavi(j).Valore.Valore
            gSSA(k).Segm(0) = TabIstr.BlkIstr(k).Segment
          Next j
        End If
      Else 'IRIS-DB
      'IRIS-DB: aggiunta gestione command code "C" a livello di parser invece che di generazione routines
      ' TO BE FINALIZED TO SUPPORT ALL LEVELS (L&G HAS UP TO 6 LEVES USED FOR A SINGLE "C")
''''        ReDim Preserve TabIstr.BlkIstr(k).KeyDef(1)
''''        TabIstr.BlkIstr(k).KeyDef(1).Op = "EQ"
''''        TabIstr.BlkIstr(k).KeyDef(1).Key = GetNomeKey(TabIstr.BlkIstr(k).Segment, TabIstr.DBD)
''''        If TabIstr.BlkIstr(k).KeyDef(1).Key = "CONCAT" Then
''''          Dim parentSeg As String
''''          Dim KeyParentLen As Integer
''''          Dim KeyCurrentLen As Integer
''''          parentSeg = GetParentSeg(TabIstr.BlkIstr(k).Segment, TabIstr.DBD)
''''          If parentSeg = "CONCAT" Then
''''          'IRIS-DB: trying to get the no key segments too; supporting only a specific case of DSHS, to be finalized later
''''            parentSeg = GetParentSegUncond(TabIstr.BlkIstr(k).Segment, TabIstr.DBD)
''''            Dim parentSeg2 As String
''''            parentSeg2 = GetParentSeg(parentSeg)
''''            If parentSeg2 <> "" Then
''''              ReDim Preserve TabIstr.BlkIstr(2)
''''              ReDim Preserve TabIstr.BlkIstr(2).KeyDef(1)
''''              ReDim Preserve TabIstr.BlkIstr(3)
''''              ReDim Preserve TabIstr.BlkIstr(3).KeyDef(1)
''''              TabIstr.BlkIstr(3) = TabIstr.BlkIstr(1)
''''              TabIstr.BlkIstr(3).qualified = False
''''              TabIstr.BlkIstr(1).Segment = parentSeg2
''''              TabIstr.BlkIstr(1).KeyDef(1).Key = GetNomeKey(TabIstr.BlkIstr(1).Segment, TabIstr.DBD)
''''              TabIstr.BlkIstr(1).KeyDef(1).Op = "EQ"
''''              TabIstr.BlkIstr(2).Segment = parentSeg
''''              TabIstr.BlkIstr(2).KeyDef(1).Key = GetNomeKeyNew(TabIstr.BlkIstr(2).Segment, TabIstr.DBD)
''''              TabIstr.BlkIstr(2).KeyDef(1).Op = "EQ"
''''              TabIstr.BlkIstr(1).qualified = True
''''              TabIstr.BlkIstr(2).qualified = True
''''              KeyParentLen = GetKeyLen(TabIstr.BlkIstr(1).Segment, TabIstr.DBD)
''''              KeyCurrentLen = GetKeyLen(TabIstr.BlkIstr(2).Segment, TabIstr.DBD)
''''              TabIstr.BlkIstr(1).KeyDef(1).KeyVal = TabIstrSSa(1).Chiavi(1).Valore.Valore & "(1:" & KeyParentLen & ")"
''''              TabIstr.BlkIstr(2).KeyDef(1).KeyVal = TabIstrSSa(1).Chiavi(1).Valore.Valore & "(" & KeyParentLen + 1 & ":" & KeyCurrentLen & ")"
''''              TabIstr.BlkIstr(1).Record = ""
''''              TabIstr.BlkIstr(2).Record = ""
''''              TabIstr.BlkIstr(1).valida = True
''''              TabIstr.BlkIstr(2).valida = True
''''              'prog = 3
''''              'qual = False
''''              ReDim Preserve TabIstr.BlkIstr(3).KeyDef(0)
''''            'IRIS-DB to manage better later
''''              If TabIstr.BlkIstr(1).CodCom = "*D" Then
''''                TabIstr.BlkIstr(1).CodCom = ""
''''              Else
''''                TabIstr.BlkIstr(1).CodCom = Replace(TabIstr.BlkIstr(1).CodCom, "D", "")
''''              End If
''''              If TabIstr.BlkIstr(2).CodCom = "*D" Then
''''                TabIstr.BlkIstr(2).CodCom = ""
''''              Else
''''                TabIstr.BlkIstr(2).CodCom = Replace(TabIstr.BlkIstr(2).CodCom, "D", "")
''''              End If
''''              If TabIstr.BlkIstr(3).CodCom = "*D" Then
''''                TabIstr.BlkIstr(3).CodCom = ""
''''              Else
''''                TabIstr.BlkIstr(3).CodCom = Replace(TabIstr.BlkIstr(3).CodCom, "D", "")
''''              End If
''''            Else
''''              If parentSeg = "CONCAT" Then
''''                If TabIstr.BlkIstr(k).CodCom = "" Then
''''                  TabIstr.BlkIstr(k).CodCom = "*C"
''''                Else
''''                  TabIstr.BlkIstr(k).CodCom = TabIstr.BlkIstr(k).CodCom & "C"
''''                End If
''''                TabIstr.BlkIstr(k).KeyDef(1).KeyVal = TabIstrSSa(1).Chiavi(1).Valore.Valore
''''              End If
''''            End If
''''          Else
''''          'IRIS-DB: for now it supports only 2 levels of KEYS!!!
''''            If k = 1 Then
''''              ReDim Preserve TabIstr.BlkIstr(2)
''''              ReDim Preserve TabIstr.BlkIstr(2).KeyDef(1)
''''              TabIstr.BlkIstr(2) = TabIstr.BlkIstr(1)
''''              TabIstr.BlkIstr(1).Segment = parentSeg
''''              TabIstr.BlkIstr(1).KeyDef(1).Key = GetNomeKey(TabIstr.BlkIstr(1).Segment, TabIstr.DBD)
''''              TabIstr.BlkIstr(1).KeyDef(1).Op = "EQ"
''''              TabIstr.BlkIstr(2).KeyDef(1).Key = GetNomeKeyNew(TabIstr.BlkIstr(2).Segment, TabIstr.DBD)
''''              TabIstr.BlkIstr(1).qualified = True
''''              TabIstr.BlkIstr(2).qualified = True
''''            Else
''''              ReDim Preserve TabIstr.BlkIstr(1).KeyDef(1)
''''              TabIstr.BlkIstr(1).Segment = parentSeg
''''              TabIstr.BlkIstr(1).KeyDef(1).Key = GetNomeKey(TabIstr.BlkIstr(1).Segment, TabIstr.DBD)
''''              TabIstr.BlkIstr(1).KeyDef(1).Op = "EQ"
''''              TabIstr.BlkIstr(2).KeyDef(1).Key = GetNomeKeyNew(TabIstr.BlkIstr(2).Segment, TabIstr.DBD)
''''              TabIstr.BlkIstr(1).qualified = True
''''              TabIstr.BlkIstr(2).qualified = True
''''            End If
''''            KeyParentLen = GetKeyLen(TabIstr.BlkIstr(1).Segment, TabIstr.DBD)
''''            KeyCurrentLen = GetKeyLen(TabIstr.BlkIstr(2).Segment, TabIstr.DBD)
''''            TabIstr.BlkIstr(1).KeyDef(1).KeyVal = TabIstrSSa(1).Chiavi(1).Valore.Valore & "(1:" & KeyParentLen & ")"
''''            TabIstr.BlkIstr(2).KeyDef(1).KeyVal = TabIstrSSa(1).Chiavi(1).Valore.Valore & "(" & KeyParentLen + 1 & ":" & KeyCurrentLen & ")"
''''            TabIstr.BlkIstr(1).Record = ""
''''            'IRIS-DB to manage better later
''''            If TabIstr.BlkIstr(2).CodCom = "*D" Then
''''              TabIstr.BlkIstr(2).CodCom = ""
''''            Else
''''              TabIstr.BlkIstr(2).CodCom = Replace(TabIstr.BlkIstr(2).CodCom, "D", "")
''''            End If
''''          End If
''''        Else
''''          TabIstr.BlkIstr(k).KeyDef(1).KeyVal = TabIstrSSa(1).Chiavi(1).Valore.Valore
''''        End If
''''        If TabIstr.BlkIstr(k).KeyDef(1).Key = "" Then
''''          addSegnalazione "Command code C", "I00", "missing Sequence Unique in segm: " & TabIstr.BlkIstr(k).Segment
''''        End If
        m_Fun.writeLog "Command code C not supported yet, to do manually in the IO routine! idpgm: " & GbIdOggetto & " riga: " & GbOldNumRec
      End If 'IRIS-DB
      'IRIS-DB: se ho errore sulla SSA, devo segnalare non valida, perch� cos� va sulla dinamica. Prima lo ignorava
      If TabIstr.BlkIstr(k).valida Then
        TabIstr.BlkIstr(k).valida = TabIstrSSa(k).valida
      End If
    Next k
  End If
End Sub

Function getOperator(operator As String) As String
  Select Case operator
    Case "=", "EQ", " =" 'IRIS-DB
      getOperator = "EQ"
    Case ">=", "=>", "GE"
      getOperator = "GE"
    Case "<=", "=<", "LE"
      getOperator = "LE"
    Case "<", "LT"
      getOperator = "LT"
    Case ">", "GT"
      getOperator = "GT"
    Case "!=", "=!", "^=", "=^", "NE"
      getOperator = "NE"
    Case Else
      getOperator = ""
  End Select
End Function
Function EliminaParentesi(source As String) As String
  Dim ret As String
  ret = source
  While Left(ret, 1) = "("
    ret = Right(ret, Len(ret) - 1)
    If Right(ret, 1) = ")" Then
        ret = Left(ret, Len(ret) - 1)
    End If
  Wend
  EliminaParentesi = ret
End Function
Function ControllaOperatoreInstr(source As String, ByRef pos As Integer, ByRef Length As Integer) As String
  'SQ ma come fanno ad esserci anche le forme "alfanumeriche"? Come fa a "parserare"?
  If InStr(source, ">=") > 0 Then
    ControllaOperatoreInstr = "GE"
    pos = InStr(source, ">=")
    Length = 2
  ElseIf InStr(source, "=>") > 0 Then
    ControllaOperatoreInstr = "GE"
    pos = InStr(source, "=>")
    Length = 2
  ElseIf InStr(source, "<=") > 0 Then
    ControllaOperatoreInstr = "LE"
    pos = InStr(source, "<=")
    Length = 2
  ElseIf InStr(source, "=<") > 0 Then
    ControllaOperatoreInstr = "LE"
    pos = InStr(source, "=<")
    Length = 2
  ElseIf InStr(source, "!=") > 0 Then
    ControllaOperatoreInstr = "NE"
    pos = InStr(source, "!=")
    Length = 2
  ElseIf InStr(source, "=!") > 0 Then
    ControllaOperatoreInstr = "NE"
    pos = InStr(source, "=!")
    Length = 2
  ElseIf InStr(source, "^=") > 0 Then
    ControllaOperatoreInstr = "NE"
    pos = InStr(source, "^=")
    Length = 2
  ElseIf InStr(source, "=^") > 0 Then
    ControllaOperatoreInstr = "NE"
    pos = InStr(source, "=^")
    Length = 2
   ElseIf InStr(source, "^<") > 0 Then
    ControllaOperatoreInstr = "GE"
    pos = InStr(source, "^<")
    Length = 2
  ElseIf InStr(source, "<^") > 0 Then
    ControllaOperatoreInstr = "GE"
    pos = InStr(source, "<^")
    Length = 2
   ElseIf InStr(source, "^>") > 0 Then
    ControllaOperatoreInstr = "LE"
    pos = InStr(source, "^>")
    Length = 2
  ElseIf InStr(source, ">^") > 0 Then
    ControllaOperatoreInstr = "LE"
    pos = InStr(source, ">^")
    Length = 2
  ElseIf InStr(source, Chr(172) & "=") > 0 Then
    ControllaOperatoreInstr = "NE"
    pos = InStr(source, Chr(172) & "=")
    Length = 2
  ElseIf InStr(source, "=" & Chr(172)) > 0 Then
    ControllaOperatoreInstr = "NE"
    pos = InStr(source, "=" & Chr(172))
    Length = 2
  ElseIf InStr(source, "=") > 0 Then
    ControllaOperatoreInstr = "EQ"
    pos = InStr(source, "=")
    Length = 1
  ElseIf InStr(source, ">") > 0 Then
    ControllaOperatoreInstr = "GT"
    pos = InStr(source, ">")
    Length = 1
  ElseIf InStr(source, "<") > 0 Then
    ControllaOperatoreInstr = "LT"
    pos = InStr(source, "<")
    Length = 1
  ElseIf InStr(source, "GE") > 0 Then
    ControllaOperatoreInstr = "GE"
    pos = InStr(source, "GE")
    Length = 2
  ElseIf InStr(source, "LE") > 0 Then
    ControllaOperatoreInstr = "LE"
    pos = InStr(source, "LE")
    Length = 2
  ElseIf InStr(source, "EQ") > 0 Then
    ControllaOperatoreInstr = "EQ"
    pos = InStr(source, "EQ")
    Length = 2
  ElseIf InStr(source, "LT") > 0 Then
    ControllaOperatoreInstr = "LT"
    pos = InStr(source, "LT")
    Length = 2
  ElseIf InStr(source, "GT") > 0 Then
    ControllaOperatoreInstr = "GT"
    pos = InStr(source, "GT")
    Length = 2
  ElseIf InStr(source, "GR") > 0 Then
    ControllaOperatoreInstr = "GT"
    pos = InStr(source, "GR")
    Length = 2
  ElseIf InStr(source, "NE") > 0 Then
    ControllaOperatoreInstr = "NE"
    pos = InStr(source, "NE")
    Length = 2
  End If
End Function
'''''''''''''''''''''''''''''''''''''''''''''
' Completa la getTabIstr con gli idSegmenti
' (serviva il calcolo dei DBD...)
' Effetto collaterale: setta la NomeRout
'''''''''''''''''''''''''''''''''''''''''''''
Public Sub getTabIstrIds()
  Dim k As Integer
  
  For k = 1 To UBound(TabIstr.BlkIstr)
    'SQ 9-05-06
    'If Len(TabIstr.BlkIstr(k).segment) Then
    '  getSegmentsId TabIstr.BlkIstr(k).segment
    'Else
    '  ReDim IdSegmenti(0)
    'End If
    'Valorizza IdSegmenti (tutti N segmenti: per ora non usato) e
    'le info relative al segmento del TabIstr.BlkIstr(k)
    getSegmentsId TabIstr.BlkIstr(k)
    
    ''''''''''''''''''''''''''''
    ' Ne trovo N e ne uso 1...
    ''''''''''''''''''''''''''''
    'Mauro 19-03-2007 Multi-Segm
'''''    If Len(IdSegmenti(0)) Then ' � stringa!
'''''      TabIstr.BlkIstr(k).IdSegm = IdSegmenti(0)
'''''    Else
'''''      TabIstr.BlkIstr(k).SegLen = 0
'''''      '''''''''''''''''''''''''''''''''''''
'''''      ' Segnalazione
'''''      '''''''''''''''''''''''''''''''''''''
'''''    End If
    If Len(IdSegmenti(0)) Then ' � stringa!
      TabIstr.BlkIstr(k).IdSegm = IdSegmenti(0)
    Else
      If Len(TabIstr.BlkIstr(k).IdSegmenti) Then
        Dim IdSegm() As String
        IdSegm() = Split(TabIstr.BlkIstr(k).IdSegmenti, ";")
        If Len(IdSegm(0)) Then TabIstr.BlkIstr(k).IdSegm = IdSegm(0)
      End If
      TabIstr.BlkIstr(k).SegLen = 0
      '''''''''''''''''''''''''''''''''''''
      ' Segnalazione
      '''''''''''''''''''''''''''''''''''''
    End If
  Next k
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Setta la IdSegmenti a partire da blkIstrDli.segment e wIdDBD
' Se DBD ha un unico segmento lo assegna comunque.
' Effetti collaterali:
' - valorizza blkIstrDli.segment e blkIstrDli.segLen
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Sub getSegmentsId(segmentName As String)
Sub getSegmentsId(blkIstrDli As blkIstrDli)
  Dim rs As Recordset
  Dim queryCondition As String
  Dim i As Integer
  Dim tb As Recordset 'IRIS-DB
  Dim idSegmento As Variant 'IRIS-DB
  Dim maxLength As Variant 'IRIS-DB
  
  If wIdDBD(0) Then
    queryCondition = "("
    For i = 0 To UBound(wIdDBD)
      queryCondition = queryCondition & "idOggetto=" & wIdDBD(i)
      If i = UBound(wIdDBD) Then
         queryCondition = queryCondition & ")"
      Else
         queryCondition = queryCondition & " OR "
      End If
    Next i
    '''''''''''''''''''''''''''''''''''''''''
    ' SQ 9-05-06
    ' DBD con unico segmento (non solo GSAM)
    '''''''''''''''''''''''''''''''''''''''''
    If Len(blkIstrDli.Segment) Then
      Set rs = m_Fun.Open_Recordset("select IdSegmento,maxLen from PsDli_segmenti where " & queryCondition & " and PsDli_segmenti.nome = '" & blkIstrDli.Segment & "' ")
      If rs.RecordCount Then
        '''''''''''''''''''''''''''''''''''''''''
        ' SQ 15-12-06
        ' GESTIONE LOGICI!
        ' -> Cambiamo il segmento sotto il naso?!
        '''''''''''''''''''''''''''''''''''''''''
        If TabIstr.DBD_TYPE = DBD_LOGIC Then
          'per ora no... lo facciamo solo per la chiave...
        End If
        'effetto collaterale (prendo il primo)
        blkIstrDli.SegLen = rs!MaxLen
        blkIstrDli.IdSegm = rs!idSegmento
        ' AC non deve restare vuota , da idSegmenti partono controlli successivi
        blkIstrDli.IdSegmenti = rs!idSegmento
        ReDim IdSegmenti(rs.RecordCount - 1)
        For i = 0 To rs.RecordCount - 1
          IdSegmenti(i) = rs!idSegmento
          rs.MoveNext
        Next i
      Else
        ReDim IdSegmenti(0)
      End If
      rs.Close
    Else
      ReDim IdSegmenti(0)
      'Non ho nome segmento: unico nel DBD?
      Set rs = m_Fun.Open_Recordset("select IdSegmento,nome,maxLen from PsDli_segmenti where " & queryCondition)
      If rs.RecordCount = 1 Then
        IdSegmenti(0) = rs!idSegmento
        'effetti collaterali:
        blkIstrDli.Segment = rs!nome
        blkIstrDli.SegLen = rs!MaxLen
      End If
      rs.Close
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Mauro 19-03-2007
    ' DBD con segmento multiplo
    '''''''''''''''''''''''''''''''''''''''''
    If Len(blkIstrDli.Segmenti) Then
      Dim Segmenti() As String
      Dim a As Integer
      Segmenti() = Split(blkIstrDli.Segmenti, ";")
      'AC 15/10/2007 ' duplicava segmento perch� idSegmenti non era vuoto
      blkIstrDli.SegmentiLen = ""
      blkIstrDli.IdSegmenti = ""
      '-----------
      For a = 0 To UBound(Segmenti())
        If Len(Trim(Segmenti(a))) Then
          Set rs = m_Fun.Open_Recordset("select IdSegmento,maxLen from PsDli_segmenti " & _
                                        "where " & queryCondition & " and nome = '" & Segmenti(a) & "' ")
          If rs.RecordCount Then
            '''''''''''''''''''''''''''''''''''''''''
            ' SQ 15-12-06
            ' GESTIONE LOGICI!
            ' -> Cambiamo il segmento sotto il naso?!
            '''''''''''''''''''''''''''''''''''''''''
            'IRIS-DB modifica per i logici - start
            idSegmento = rs!idSegmento
            maxLength = rs!MaxLen
            If TabIstr.DBD_TYPE = DBD_LOGIC Then
              'per ora no... lo facciamo solo per la chiave...
              'IRIS-DB: � ora di farlo...
              Set tb = m_Fun.Open_Recordset("select maxLen from PsDLI_Segmenti as b, PsDLI_Segmenti_SOURCE as c, bs_oggetti as d where c.idsegmento = " & idSegmento _
                       & " and c.segmento = b.nome and c.DBD = d.nome and d.idoggetto = b.idoggetto")
              If tb.RecordCount Then
                maxLength = tb!MaxLen
              Else
                maxLength = 0
              End If
              tb.Close
            End If
            blkIstrDli.SegmentiLen = IIf(Len(Trim(blkIstrDli.SegmentiLen)), blkIstrDli.SegmentiLen & ";" & maxLength, maxLength)
            blkIstrDli.IdSegmenti = IIf(Len(Trim(blkIstrDli.IdSegmenti)), blkIstrDli.IdSegmenti & ";" & idSegmento, idSegmento)
            'IRIS-DB - end
            ' Mauro 19-03-2007: al momento IdSegmenti non lo utilizzo
''''            ReDim IdSegmenti(rs.RecordCount - 1)
''''            For i = 0 To rs.RecordCount - 1
''''              IdSegmenti(i) = rs!IdSegmento
''''              rs.MoveNext
''''            Next
''''          Else
''''            ReDim IdSegmenti(0)
          End If
          rs.Close
        End If
      Next a
'''    Else
'''      ReDim IdSegmenti(0)
'''      'Non ho nome segmento: unico nel DBD?
'''      Set rs = m_Fun.Open_Recordset("select IdSegmento,nome,maxLen from PsDli_segmenti where " & queryCondition)
'''      If rs.RecordCount = 1 Then
'''        IdSegmenti(0) = rs!IdSegmento
'''        'effetti collaterali:
'''        blkIstrDli.Segmenti = rs!nome
'''        blkIstrDli.SegmentiLen = rs!MaxLen
'''      End If
'''      rs.Close
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Else
    ReDim IdSegmenti(0)
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' Utilizzata da parseSSA...
' Ritorno il valore del campo di lunghezza "precisa"
' I valori vengono "puliti" (SPACE,ZERO,SEGNO "+"...)
' Gestione null: arriva una "@" - Per ora SPACE = default
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function formatSsaValue(value As String, bytes As String, Segno As String, nome As String) As String
  Select Case value
    Case "@"
      formatSsaValue = " "
      'NO VALUE!
      'tmp...
      If Not nome = "" Then
        addSegnalazione nome, "NOSSAVALUE", nome
      End If
    Case "LOW-VALUE", "LOW-VALUES", "HIGH-VALUE", "HIGH-VALUES"
      formatSsaValue = "."
    Case "SPACE", "SPACES"
      formatSsaValue = " "
    Case "ZERO", "ZEROES"
      formatSsaValue = "0"
    Case Else
      If Left(value, 1) = "'" Then
        formatSsaValue = Replace(value, "'", "")
      Else
        If Left(value, 1) = """" Then 'IRIS-DB: gestisce anche il doppio apice perch� pu� essere usato anche lui come quote
          formatSsaValue = Replace(value, """", "")
        Else
          If Segno = "S" Then
            formatSsaValue = Replace(value, "+", "") 'il "+" � operatore relazionale... darebbe fastidio!
          Else
            formatSsaValue = value
          End If
        End If
      End If
  End Select
  'Lunghezza esatta:
  If Len(formatSsaValue) < bytes Then
    'LEADING:
    formatSsaValue = formatSsaValue & Space(bytes - Len(formatSsaValue))
  ElseIf Len(formatSsaValue) > CInt(bytes) Then
    'TRONCAMENTO:
    'SQ 17-10-07 (es: da MOVE...) importantissimo! verificare che non faccia danni
    formatSsaValue = Left(formatSsaValue, bytes)
  End If
End Function

Public Function CoppiaTastoValore(pwCoppia As String, ByRef tasto As String, ByRef Valore As String) As Boolean
    CoppiaTastoValore = False
    If InStr(2, pwCoppia, "=") > 0 Then
           tasto = Mid(pwCoppia, 1, InStr(2, pwCoppia, "=") - 1)
           Valore = Mid(pwCoppia, InStr(2, pwCoppia, "=") + 1, Len(pwCoppia) - InStr(2, pwCoppia, "="))
           If tasto = "'" Then Exit Function
           
'           If Left(Valore, 1) = "'" Then
'              Valore = Right(Valore, Len(Valore) - 1)
'           End If
'           If Right(Valore, 1) = "'" Then
'              Valore = Left(Valore, Len(Valore) - 1)
'           End If
           CoppiaTastoValore = True
    End If

End Function
Function pulisciCommentiPFK(source As String) As String
  If InStr(1, source, "*") > 0 Or InStr(1, source, "+") > 0 Then
    pulisciCommentiPFK = Right(source, Len(source) - InStr(1, source, "'") + 1)
  Else
    pulisciCommentiPFK = source
  End If
End Function
Private Function checkCopyMFS(wFileName As String, path As String) As Boolean
  Dim rs As Recordset, area As String, level As String
  
  Set rs = m_Fun.Open_Recordset("select * from BS_oggetti where Nome = '" & wFileName & "' and tipo in ('CMF','MFS','UNK')")
  If rs.EOF Then
    ' segnalazione
    GbOldNumRec = GbNumRec
    addSegnalazione "", "NOCOPYMAPFIELD", wFileName
    checkCopyMFS = False
  Else
    'RICLASSIFICAZIONE
      rs!Tipo = "CMF"
      MapsdM_Parser.getLevel_Area "CMF", area, level
      rs!Area_Appartenenza = area
      rs!livello1 = level
      rs.Update
      checkCopyMFS = True
    If Trim(rs!estensione) = "" Then
      path = m_Fun.FnPathDef & "\" & Mid(rs!Directory_Input, 2) & "\" & rs!nome
    Else
      path = m_Fun.FnPathDef & "\" & Mid(rs!Directory_Input, 2) & "\" & rs!nome & "." & rs!estensione
    End If
  End If
  rs.Close
End Function
Function writeCampiCopyMFS(wFileName As String, pCampi As Collection, PDev As String) As Integer
  Dim nFBms As Variant
  Dim path As String
  Dim wRec As String
  Dim wRecIn As String
  Dim firstWord As String
  Dim k1 As Integer
  Dim MacroCmd As String
  Dim aCmp As t_Cmp_Bms
  Dim i As Integer
  'return:
  ' 1 file find
  ' -1 file not find in project
  ' -2 file not find in directory
  Dim TbOggetti As New Recordset
  Dim rs As New Recordset
  Dim InsideDFLD As Boolean
  Dim Keepline As Boolean
  Dim wRecAppo As String
    
  InsideDFLD = False
  Keepline = False
  Set TbOggetti = m_Fun.Open_Recordset("select * from BS_oggetti where Nome = '" & wFileName & "' and (tipo = 'CMF' or tipo = 'MFS')")
  If TbOggetti.EOF Then
    ' segnalazione
    GbOldNumRec = GbNumRec
    addSegnalazione "", "NOCOPYMAPFIELD", wFileName
    writeCampiCopyMFS = -1
    Exit Function
  End If
    
    On Error GoTo parseErr
    
  If Trim(TbOggetti!estensione) = "" Then
      path = m_Fun.FnPathDef & "\" & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!nome
  Else
      path = m_Fun.FnPathDef & "\" & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!nome & "." & TbOggetti!estensione
  End If


    nFBms = FreeFile
    Open path For Input As nFBms
    While Not EOF(nFBms)
        wRec = ""
        If Not Keepline Then
            Line Input #nFBms, wRecIn
        Else
            Keepline = False
        End If
        
        If Mid$(wRecIn, 1, 1) <> "*" Then
            firstWord = Trim(wRecIn)
            k1 = InStr(firstWord, " ")
            firstWord = Trim(Mid$(firstWord, 1, k1))
         
            If k1 > 0 Then
                If InStr(1, wRecIn, " DFLD ") > 0 Then
                    MacroCmd = "DFLD"
                Else
                    MacroCmd = ""
                End If
            Else
                MacroCmd = ""
            End If
            Select Case MacroCmd
                Case "DFLD"
                   InsideDFLD = True
                   While InsideDFLD And Not EOF(nFBms)
                    Line Input #nFBms, wRecAppo
                    If Not InStr(1, wRecAppo, "*") > 0 And Not InStr(1, wRecAppo, " DFLD ") > 0 Then
                        wRecIn = wRecIn & wRecAppo
                    Else
                        InsideDFLD = False
                        If InStr(1, wRecAppo, " DFLD ") Then
                            Keepline = True
                        End If
                    End If
                   Wend
            'E' un campo, recupera le informazioni
                    If Not (firstWord = "DFLD") Then
                        aCmp.nomeCmp = firstWord
                        If UCase(TrovaParametro(Replace(wRecIn, "P.", "POS="), "POS")) <> "NONE" Then
                          aCmp.PosY = Mid(TrovaParametro(Replace(wRecIn, "P.", "POS="), "POS"), 2, 2)
                          aCmp.PosX = Mid(TrovaParametro(Replace(wRecIn, "P.", "POS="), "POS"), 5, 2)
                        End If
                        If UCase(TrovaParametro(Replace(wRecIn, "L.", "LENGTH="), "LENGTH")) <> "NONE" Then
                          aCmp.Length = TrovaParametro(Replace(wRecIn, "L.", "LENGTH="), "LENGTH")
                        ElseIf UCase(TrovaParametro(wRecIn, "LTH")) <> "NONE" Then
                          aCmp.Length = TrovaParametro(wRecIn, "LTH")
                        End If
            
                        If Len(aCmp.nomeCmp) = 8 Then
                          pCampi.Add aCmp.nomeCmp
                        End If
                        Set rs = m_Fun.Open_Recordset("Select * From PsDLI_MFSCmp")
                        rs.AddNew
                        rs!idOggetto = GbIdOggetto
                        rs!nome = aCmp.nomeCmp
                        rs!DevType = PDev
                        rs!Riga = aCmp.PosY
                        rs!Colonna = aCmp.PosX
                        rs!Lunghezza = aCmp.Length
                        For i = 1 To UBound(mEqu)
                            If InStr(1, wRecIn, mEqu(i).Alias) Or InStr(1, wRecIn, mEqu(i).cmd) Or InStr(1, wRecIn, Mid(mEqu(i).cmd, 2, Len(mEqu(i).cmd) - 2)) Then
                                If UCase(TrovaParametro(mEqu(i).cmd, "ATTR")) <> "NONE" And Not (mEqu(i).cmd = "'ATTR='") Then
                                    rs!Attributo = ControllaAttributo(TrovaParametro(mEqu(i).cmd, "ATTR"))
                                End If
                            End If
                        Next
                        rs.Update
                        rs.Close
                    End If
                End Select
        End If
    Wend
    Exit Function
parseErr:
    GbOldNumRec = GbNumRec
    If Not wRecIn = "" Then
        addSegnalazione wRecIn, "P00", wRecIn
    Else
        addSegnalazione path, "P00", path
    End If
End Function
Public Function writeCampiCopymsgMFS(wFileName As String, PMSGName As String, ByRef pOrdinale As Integer) As Integer
  Dim nFBms As Variant
  Dim path As String
  Dim wRec As String
  Dim wRecIn As String
  Dim firstWord As String
  Dim k1 As Integer
  Dim MacroCmd As String
  Dim aCmp As t_Cmp_Bms
  Dim i As Integer
  'return:
  ' 1 file find
  ' -1 file not find in project
  ' -2 file not find in directory
  Dim TbOggetti As New Recordset
  Dim rs As New Recordset

    Set TbOggetti = m_Fun.Open_Recordset("select * from BS_oggetti where Nome = '" & wFileName & "' and (tipo = 'CMF' or tipo ='MFS')")
    If TbOggetti.EOF Then
' segnalazione
        GbOldNumRec = GbNumRec
        addSegnalazione "", "NOCOPYMAPFIELD", wFileName
        writeCampiCopymsgMFS = -1
        Exit Function
    End If
    
    On Error GoTo parseErr
    If Trim(TbOggetti!estensione) = "" Then
        path = m_Fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!nome
    Else
        path = m_Fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!nome & "." & TbOggetti!estensione
    End If


    nFBms = FreeFile
    Open path For Input As nFBms
    While Not EOF(nFBms)
        wRec = ""
        Line Input #nFBms, wRecIn


        If Mid$(wRecIn, 1, 1) <> "*" Then
            firstWord = Trim(wRecIn)
            k1 = InStr(firstWord, " ")
            firstWord = Trim(Mid$(firstWord, 1, k1))
         
            If k1 > 0 Then
                If InStr(1, wRecIn, " MFLD ") > 0 Or InStr(1, wRecIn, "MFLD") > 0 Then
                    MacroCmd = "MFLD"
                Else
                    MacroCmd = ""
                End If
            Else
                MacroCmd = ""
            End If
            Select Case MacroCmd
                Case "MFLD"
                    aCmp.nomeCmp = Trim(Mid(wRecIn, InStr(1, wRecIn, "MFLD") + 4, InStr(1, wRecIn, ",") - InStr(1, wRecIn, "MFLD") - 4))
                    If UCase(TrovaParametro(Replace(wRecIn, "L.", "LTH="), "LTH")) <> "NONE" Then
                           aCmp.Length = TrovaParametro(Replace(wRecIn, "L.", "LTH="), "LTH")
                    Else   'campo generato da funzione
                         wRecIn = Trim(Mid(wRecIn, InStr(1, wRecIn, "MFLD") + 4, Len(wRecIn) - InStr(1, wRecIn, "MFLD") - 4))
                        If InStr(1, wRecIn, "    ") Then
                            wRecIn = Trim(Mid(wRecIn, 1, InStr(1, wRecIn, "    ")))
                        End If
                        'Ricerca di campi definiti da funzioni
                        If Left(wRecIn, 1) = "(" Then  'assumiamo che campo sia definito da funzione nel formato (campo,funzione)
                           If Right(wRecIn, 1) = ")" Then
                                wRecIn = Mid(wRecIn, 1, Len(wRecIn) - 1)
                           End If
                           aCmp.nomeCmp = Mid(wRecIn, 2, InStr(1, wRecIn, ",") - 2)
                           aCmp.Function = Mid(wRecIn, InStr(1, wRecIn, ",") + 1, Len(wRecIn) - InStr(1, wRecIn, ","))
                            m_Fun.FnConnection.Execute "update PsDLI_MFSCmp set Funzione = '" & aCmp.Function _
                            & "' where Nome = '" & aCmp.nomeCmp & "'  and IdOggetto = " & GbIdOggetto
                        End If
                    End If
                    For i = 1 To UBound(mEqu)
                        If InStr(Len(aCmp.nomeCmp) + 1, Mid(Trim(wRecIn), 5, Len(wRecIn) - 4), mEqu(i).Alias) Then
                            If UCase(TrovaParametro(mEqu(i).cmd, "ATTR")) <> "NONE" And Not (mEqu(i).cmd = "'ATTR='") Then
                                aCmp.attrib = ControllaAttributo(TrovaParametro(mEqu(i).cmd, "ATTR"))
                            End If
                        End If
                    Next
                    If UCase(TrovaParametro(Replace(wRecIn, "A.", "ATTR="), "ATTR")) <> "NONE" Then
                           aCmp.attrib = TrovaParametro(Replace(wRecIn, "A.", "ATTR="), "ATTR")
                    End If
                    'IRIS-DB gestione EXIT
                    If UCase(TrovaParametro(Replace(wRecIn, "E.", "EXIT="), "EXIT")) <> "NONE" Then
                      aCmp.Exit = TrovaParametro(Replace(wRecIn, "E.", "EXIT="), "EXIT")
                      m_Fun.FnConnection.Execute "update PsDLI_MFSCmp set Exit = '" & aCmp.Exit _
                      & "' where Nome = '" & aCmp.nomeCmp & "'  and IdOggetto = " & GbIdOggetto
                    End If
                    'IRIS-DB gestione FILL
                    If UCase(TrovaParametro(Replace(wRecIn, "F.", "FILL="), "FILL")) <> "NONE" Then
                      aCmp.Fill = TrovaParametro(Replace(wRecIn, "F.", "FILL="), "FILL")
                    End If
                    
                    Set rs = m_Fun.Open_Recordset("Select * From PsDLI_MFSmsgCmp")
                    rs.AddNew
                    rs!idOggetto = GbIdOggetto
                    rs!nome = aCmp.nomeCmp
                    rs!MSGName = PMSGName
                    rs!Lunghezza = aCmp.Length
                    rs!Funzione = aCmp.Function
                    rs!Attributo = aCmp.attrib
                    rs!Ordinale = pOrdinale
                    rs!Exit = aCmp.Exit
                    rs!Fill = aCmp.Fill
                    rs.Update
                    rs.Close
                    pOrdinale = pOrdinale + 1
                    aCmp.Function = ""
                    aCmp.attrib = ""
                End Select
        End If
    Wend
    Exit Function
parseErr:
    GbOldNumRec = GbNumRec
    If Not wRecIn = "" Then
        addSegnalazione wRecIn, "P00", wRecIn
    Else
        addSegnalazione path, "P00", path
    End If
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ritorna il recordset (PsDataCmp) relativo alla struttura di input:
' Esplode le copy.
''
'SQ 18-06-07 - gestione aree a livello > 01
'               -> parametro opzionale: relativePosition
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getDataCmp(idOggetto As Long, idArea As Long, nome As String, Optional relativePosition As Boolean = False) As dataCmp()
  Dim rs As Recordset
  Dim Ordinale As Integer, livello As Integer
  Dim dataCmp() As dataCmp
  Dim Posizione As Long, posizioneBase As Long
  
  On Error GoTo funErr
  
  ReDim dataCmp(0)
  getDataCmp = dataCmp 'IRIS-DB inizializzazione
  
  posizioneBase = 1 'init
  
  'SQ - 29-09-06: errore mortifero... non consideravo il livello...
  'SQ 18-06-07 - gestione aree a livello > 01
  Set rs = m_Fun.Open_Recordset("Select Ordinale,livello,posizione From PsData_Cmp Where idOggetto = " & idOggetto & " And IdArea = " & idArea & " And Nome = '" & DoubleApice(nome) & "'")
  If rs.RecordCount Then
    Ordinale = rs!Ordinale
    livello = rs!livello
    
    'SQ 18-06-07 - gestione aree a livello > 01
    If relativePosition Then
      If rs!Posizione > 1 Then
        Dim posizioneBaseRelativa
        posizioneBaseRelativa = rs!Posizione - 1 'Caso di sottogruppo!
      End If
    End If
    
    rs.Close
    Set rs = m_Fun.Open_Recordset("Select * From PsData_Cmp Where idOggetto = " & idOggetto & " And IdArea = " & idArea & " And ordinale >= " & Ordinale & " order by ordinale")
    Do While Not rs.EOF
      If rs!livello > livello Or rs!Ordinale = Ordinale Then
        ReDim Preserve dataCmp(Posizione)
        dataCmp(Posizione).idOggetto = idOggetto
        dataCmp(Posizione).idArea = idArea
        dataCmp(Posizione).Ordinale = rs!Ordinale
        dataCmp(Posizione).livello = rs!livello
        dataCmp(Posizione).Tipo = rs!Tipo
        dataCmp(Posizione).Segno = rs!Segno & ""
        dataCmp(Posizione).byte = rs!byte
        dataCmp(Posizione).Lunghezza = rs!Lunghezza
        dataCmp(Posizione).Valore = rs!Valore
        dataCmp(Posizione).nomeRed = rs!nomeRed & ""
        dataCmp(Posizione).nome = rs!nome
        'SQ 18-06-07 - gestione aree a livello > 01
        'dataCmp(Posizione).Posizione = rs!Posizione + posizioneBase - 1
        dataCmp(Posizione).Posizione = rs!Posizione + posizioneBase - 1 - posizioneBaseRelativa
        If rs!idCopy > 0 Then
          dataCmp(Posizione).idCopy = rs!idCopy
          addToDataCmp dataCmp, Posizione, livello
          'devo proseguire con il posizionamento: ho aggiunto campi di copy!
          posizioneBase = dataCmp(Posizione).Posizione + dataCmp(Posizione).byte
        End If
        Posizione = Posizione + 1
        rs.MoveNext
      Else
        'Altra AREA
        Exit Do
      End If
    Loop
  End If
  rs.Close
  getDataCmp = dataCmp
  Exit Function
funErr:
   m_Fun.writeLog "getdatacmp error idOggetto(" & idOggetto & ") - idArea(" & idArea & ") - Nome(" & nome & ")"
  'Resume
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function getFormatFieldValue(fieldName As String, bytes As Integer) As String
  Dim rs As Recordset
  
  'SQ 18-06-07 Aggiunto "Case" esterno
  'Sbagliava i casi pi� semplici: MOVE SPACE TO W-CICCIO (!?)
  'Attenzione: per PLI/EasyTrieve servono altri "case"?
  Select Case fieldName
    Case "SPACE", "SPACES"
      getFormatFieldValue = formatSsaValue("", CStr(bytes), "", "")
    Case "ZERO", "ZEROES"
      getFormatFieldValue = formatSsaValue("0", CStr(bytes), "", "")
    Case Else
      If UCase(Left(fieldName, 3)) = "LOW" Then
        If InStr(fieldName, "(") > 0 And Right(fieldName, 1) = ")" Then
          If IsNumeric(Mid(fieldName, InStr(fieldName, "(") + 1, Len(fieldName) - (InStr(fieldName, "(") + 1))) Then
            getFormatFieldValue = formatSsaValue("", Mid(fieldName, InStr(fieldName, "(") + 1, Len(fieldName) - (InStr(fieldName, "(") + 1)), "", "")
          Else
            getFormatFieldValue = formatSsaValue("", CStr(bytes), "", "")
          End If
        Else
          getFormatFieldValue = formatSsaValue("", CStr(bytes), "", "")
        End If
        
      'spli: pezza momentanea
      ElseIf InStr(fieldName, ")") > 0 Or InStr(fieldName, "/") Then
         m_Fun.writeLog "Warning: Parentesi nel nome del campo MOVED -" & GbIdOggetto & "-" & GbOldNumRec
         Exit Function
      'IRIS-DN: altra pezza, in PLI ci possono essere costanti concatenate con || che ovviamente fanno fallire la query
      ElseIf InStr(fieldName, "|") > 0 Or InStr(fieldName, "'") Then
         m_Fun.writeLog "Warning: pipe o apice nel nome del campo MOVED -" & GbIdOggetto & "-" & GbOldNumRec
         Exit Function
      End If
      
      Set rs = m_Fun.Open_Recordset("SELECT idoggetto, idarea, valore, lunghezza, tipo From PsData_Cmp " & _
                                "Where idOggetto = " & GbIdOggetto & " And nome = '" & fieldName & "'" & _
                                " UNION " & _
                                "SELECT a.idoggetto, a.idarea, a.valore, a.lunghezza, a.tipo From PsData_Cmp as a,PsRel_Obj as b" & _
                                " where b.idOggettoC = " & GbIdOggetto & " and b.relazione='CPY' And" & _
                                " a.IdOggetto=b.IdOggettoR and a.nome = '" & fieldName & "'")
      'SQ 10-10-07
      'duro e brigidino: insisto a testa bassa
      '=> copy dentro copy
      If rs.RecordCount = 0 Then
        'per ora solo qui... poi fare un flag: non vorrei ci mettesse troppo tempo...
        If m_Fun.FnNomeDB = "ampert.mty" Then
          Set rs = m_Fun.Open_Recordset( _
            "SELECT a.idoggetto, a.idarea, a.valore, a.lunghezza, a.tipo From PsData_Cmp as a,PsRel_Obj as b,PsRel_Obj as c" & _
            " where b.idOggettoC = " & GbIdOggetto & " and b.relazione='CPY' And" & _
            " a.IdOggetto=c.IdOggettoR and b.IdOggettoR=c.IdOggettoC and c.relazione='CPY' and a.nome = '" & fieldName & "'")
        End If
      End If
      
      If rs.RecordCount Then
        ' Mauro 27-02-2007: Controllo se � un campo di gruppo
        If rs!Tipo = "A" And rs!Lunghezza = 0 Then
          Dim i As Integer, redefineLevel As Integer
          Dim isRedefines As Boolean
          Dim ssa As String
          Dim arrCmp() As dataCmp
                
          arrCmp = getDataCmp(rs!idOggetto, rs!idArea, fieldName)
          
          While i <= UBound(arrCmp)
            If isRedefines Then
              If arrCmp(i).livello <= redefineLevel Then
                isRedefines = False
              Else
                'redefines: salto
                i = i + 1
              End If
            Else
              If Len(Trim(arrCmp(i).nomeRed)) Then
                isRedefines = True
                redefineLevel = arrCmp(i).livello
                'redefines: salto
                i = i + 1
              Else
                If arrCmp(i).Lunghezza Then
                  ssa = ssa & formatSsaValue(IIf(IsNull(arrCmp(i).Valore), "@", Trim(arrCmp(i).Valore)), arrCmp(i).byte & "", arrCmp(i).Segno, arrCmp(i).nome)
                Else
                  'campo di gruppo: ignoro
                End If
                i = i + 1
              End If
            End If
          Wend
          getFormatFieldValue = ssa
        Else
          Select Case Trim(rs!Valore) 'L'ANALISI DEI DATI APPENDE UNO SPACE IN FONDO!!!!!!!!!!!
            Case "SPACE", "SPACES"
              getFormatFieldValue = ""
            Case "ZERO", "ZEROES"
              getFormatFieldValue = "0"
            Case Else
              getFormatFieldValue = Replace(rs!Valore, "'", "")
              getFormatFieldValue = Trim(Replace(getFormatFieldValue, """", ""))
          End Select
          'SQ 17-10-07 anche per la IF
          'getFormatFieldValue = formatSsaValue(getFormatFieldValue, CStr(bytes), "", "")
        End If
        'SQ 17-10-07 era solo per la ELSE
        getFormatFieldValue = formatSsaValue(getFormatFieldValue, CStr(bytes), "", "")
      End If
      rs.Close
  End Select
End Function
Function GetLenCmp(Cmp As String) As Integer
Dim rs As Recordset
Set rs = m_Fun.Open_Recordset("SELECT Lunghezza From PsData_Cmp WHERE" & _
      " idOggetto=" & GbIdOggetto & " And Nome='" & Cmp & "'" _
    )
    If Not rs.EOF Then
      GetLenCmp = rs!Lunghezza
    End If
End Function
Function ReplaceConcat(Valore As String) As String
Dim appovalue As String, part As String, posconcat As Integer
  appovalue = Valore
  While Len(appovalue)
    posconcat = InStr(appovalue, "!!")
    If posconcat > 0 Then
      part = Trim(Left(appovalue, posconcat - 1))
      appovalue = Trim(Right(appovalue, Len(appovalue) - posconcat - 1))
    Else
      part = appovalue
      appovalue = ""
    End If
    If Left(part, 1) = "'" Then
      part = Mid(part, 2, Len(part) - 2)
    Else '
      part = Space(GetLenCmp(part))
    End If
    ReplaceConcat = ReplaceConcat & part
   
  Wend
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ritorna un array di lista-valori (separatore: ",")
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function getDataCmpValues(dataCmp() As dataCmp, dataStart() As String) As String()
'ac 12/09/07 dataStart per settare byte di destinazione da cui inizia la move
  Dim rs As Recordset
  Dim i As Integer
  Dim dataCmpValues() As String
  Dim Valore As String
  Dim idOggettoBak As Long, queryField As String

  'volevo utilizzare una matrice ma non so la sintassi!

  On Error GoTo funErr

  ReDim dataStart(UBound(dataCmp))
  ReDim dataCmpValues(UBound(dataCmp))
 
  For i = 0 To UBound(dataCmp)
    If GbTipoInPars = "PLI" And i > 0 Then
      queryField = " And (Nome = '" & Replace(dataCmp(i).nome, "'", "") & "' or nome = '" & Replace(dataCmp(0).nome, "'", "") & "." & Replace(dataCmp(i).nome, "'", "") & "')"
    Else
      queryField = " And Nome = '" & Replace(dataCmp(i).nome, "'", "") & "'"
    End If
    Set rs = m_Fun.Open_Recordset("SELECT valore, destSTART From PsData_CmpMoved WHERE " & _
                                  "idPgm = " & GbIdOggetto & queryField)
    'IRIS-DB prova ad usare la OF per il COBOL, svilupperemo anche per il PL/I pi� avanti
    If rs.EOF And GbTipoInPars = "CBL" And i > 0 Then
      queryField = " And Nome = '" & Replace(dataCmp(i).nome, "'", "") & " OF " & Replace(dataCmp(0).nome, "'", "") & "'"
      Set rs = m_Fun.Open_Recordset("SELECT valore, destSTART From PsData_CmpMoved WHERE " & _
                                  "idPgm = " & GbIdOggetto & queryField)
    End If

    dataCmp(i).moved = Not rs.EOF

    While Not rs.EOF
      'VARIABILE/COSTANTI (perfezionare...)
      If Left(rs!Valore, 1) = "'" Then
        If Not InStr(rs!Valore, "!!") > 0 Then
          Valore = Replace(rs!Valore, "'", "")
        Else
          Valore = ReplaceConcat(rs!Valore)
        End If
      Else
        Valore = getFormatFieldValue(rs!Valore, dataCmp(i).byte)
      End If
'Stefano RP: to review for excluding all spaces values
      If Len(Valore) Then dataCmpValues(i) = dataCmpValues(i) & Valore & ","
      If Not IsNull(rs!deststart) Then
         If Len(rs!deststart) Then
          dataStart(i) = dataStart(i) & rs!deststart & ","
         Else
          dataStart(i) = dataStart(i) & "1" & ","
         End If
      Else
        dataStart(i) = dataStart(i) & "1" & ","
      End If
      rs.MoveNext
    Wend
    rs.Close
    ' E se ho le MOVE in copy?!?!
    If GbTipoInPars = "PLI" And i > 0 Then
      queryField = " and (a.nome = '" & Replace(dataCmp(i).nome, "'", "") & "' or a.nome = ' " & Replace(dataCmp(0).nome, "'", "") & "." & Replace(dataCmp(i).nome, "'", "") & "')"
    Else
      queryField = " and a.nome = '" & Replace(dataCmp(i).nome, "'", "") & "'"
    End If
    
    Set rs = m_Fun.Open_Recordset("SELECT a.valore, a.deststart from PsData_CmpMoved AS a, PsRel_Obj AS b WHERE " & _
                                  "b.idOggettoC = " & GbIdOggetto & " And b.relazione = 'CPY' And " & _
                                  "a.IdPgm = b.IdOggettoR " & queryField)
    'VIRGILIO 1/3/2007 Ho inserito la Replace per le REPLACING tipo : =='WS'== BY ==WS==
    If Not dataCmp(i).moved Then
      dataCmp(i).moved = Not rs.EOF
    End If
    While Not rs.EOF
      'VARIABILE/COSTANTI (perfezionare...)
      If Left(rs!Valore, 1) = "'" Then
        If Not InStr(rs!Valore, "!!") > 0 Then
          Valore = Replace(rs!Valore, "'", "")
        Else
          Valore = ReplaceConcat(rs!Valore)
        End If
      Else
        Valore = getFormatFieldValue(rs!Valore, dataCmp(i).byte)
      End If
      If Len(Valore) Then dataCmpValues(i) = dataCmpValues(i) & Valore & ","
      If Not IsNull(rs!deststart) Then
        If Len(rs!deststart) Then
          dataStart(i) = dataStart(i) & rs!deststart & ","
        Else
          dataStart(i) = dataStart(i) & "1" & ","
        End If
      Else
        dataStart(i) = dataStart(i) & "1" & ","
      End If
      rs.MoveNext
    Wend
    rs.Close
    'ultima virgola
    If Len(dataCmpValues(i)) Then
      dataCmpValues(i) = Left(dataCmpValues(i), Len(dataCmpValues(i)) - 1)
    End If
    If Len(dataStart(i)) Then
      dataStart(i) = Left(dataStart(i), Len(dataStart(i)) - 1)
    End If
  Next i
  getDataCmpValues = dataCmpValues
  Exit Function
funErr:
  MsgBox "tmp: getDataCmpValues - gestire..."
  'Resume
End Function

'SQ 29-09-06
'Private Sub addToDataCmp(dataCmp() As dataCmp, Posizione As Integer)
Private Sub addToDataCmp(dataCmp() As dataCmp, Posizione As Long, livello As Integer)
  Dim rs As Recordset
  Dim codice As String
  Dim idOggetto_Copy As Long, posizioneBase As Long
  
  idOggetto_Copy = dataCmp(Posizione).idCopy
  posizioneBase = dataCmp(Posizione).Posizione + IIf(dataCmp(Posizione).Lunghezza, dataCmp(Posizione).byte, 0) 'le copy hanno posizionamenti relativi!
  
  'SQ 5-07-06
  'Posso avere copy con pi� tracciati, anche se ho lo 01 fuori...
  'Set rs = m_Fun.Open_Recordset("Select * From PsData_Cmp Where idOggetto = " & idOggetto_Copy & " order by ordinale")
  Set rs = m_Fun.Open_Recordset("Select * From PsData_Cmp Where idOggetto = " & idOggetto_Copy & " and idArea = 1 order by ordinale")
  Do While Not rs.EOF
    If rs!livello > livello Then
      Posizione = Posizione + 1
      ReDim Preserve dataCmp(Posizione)
      dataCmp(Posizione).idOggetto = idOggetto_Copy
      dataCmp(Posizione).idArea = rs!idArea
      dataCmp(Posizione).Ordinale = rs!Ordinale
      dataCmp(Posizione).livello = rs!livello
      dataCmp(Posizione).Segno = rs!Segno
      dataCmp(Posizione).byte = rs!byte
      dataCmp(Posizione).Lunghezza = rs!Lunghezza
      dataCmp(Posizione).Valore = rs!Valore
      dataCmp(Posizione).nomeRed = rs!nomeRed
      dataCmp(Posizione).nome = rs!nome
      dataCmp(Posizione).Posizione = rs!Posizione + posizioneBase - 1
      rs.MoveNext
    Else
      'Altra AREA
      Exit Do
    End If
  Loop
  rs.Close
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''
' "Appiattisco" la SSA: unica stringa posizionale
''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getSSAstream(dataCmp() As dataCmp, ssaStruct As t_ssa) As String
  Dim i As Integer, redefineLevel As Integer
  Dim isRedefines As Boolean
  Dim ssa As String
  
  While i <= UBound(dataCmp)
    'MOVE sulla SSA
    If dataCmp(i).moved Then
      ssaStruct.moved = True  'Sbagliato?!... prob. da togliere (fa tutto la checkMove)
    End If
    
    If isRedefines Then
      If dataCmp(i).livello <= redefineLevel Then
        isRedefines = False
      Else
        'redefines: salto
        i = i + 1
      End If
    Else
      If Len(Trim(dataCmp(i).nomeRed)) Then
        isRedefines = True
        redefineLevel = dataCmp(i).livello
        'redefines: salto
        i = i + 1
      Else
        If dataCmp(i).Lunghezza Then
          ssa = ssa & formatSsaValue(IIf(IsNull(dataCmp(i).Valore), "@", Trim(dataCmp(i).Valore)), dataCmp(i).byte & "", dataCmp(i).Segno, dataCmp(i).nome)
        Else
          'campo di gruppo: ignoro
        End If
        i = i + 1
      End If
    End If
  Wend
  getSSAstream = ssa
End Function
'
Private Sub checkMove(dataCmp() As dataCmp, ssaStruct As t_ssa)
  Dim i As Integer, j As Integer
  
  ssaStruct.moved = False 'init
  
  For i = 0 To UBound(dataCmp)
    'MOVE sulla SSA
    If dataCmp(i).moved Then
      'Scarto le MOVE sulle "VALUE"
      For j = 1 To UBound(ssaStruct.Chiavi)
        If dataCmp(i).Posizione >= ssaStruct.Chiavi(j).Valore.KeyStart Then
          If dataCmp(i).byte + dataCmp(i).Posizione <= ssaStruct.Chiavi(j).Valore.KeyStart + ssaStruct.Chiavi(j).Valore.Lunghezza Then
            Exit For
          End If
        End If
      Next
      If j > UBound(ssaStruct.Chiavi) Then
        ssaStruct.moved = True
        Exit For
      End If
    End If
  Next
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' checkMove_SSA
' E' una "checkMove" specifica per un determinato "ssa_item"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub checkMove_SSA(dataCmp() As dataCmp, dataCmpValues() As String, datastartpos() As String, ssaItem As SSA_ITEM, Optional combine As Boolean = False)
  Dim i As Integer, j As Integer, subFieldPos As Integer, subFieldLen As Integer, k As Integer, Z As Integer
  Dim values() As String, StartPos() As String
  Dim itemValue_init As String, itemValue_end As String
  Dim ValuesForCombination() As Collection
  Dim Combinazioni() As Integer
  Dim posfirstccode As Integer, normalizedpos As Integer, startindex As Integer
  
  On Error GoTo error
  ssaItem.moved = False 'init

  If combine Then
    ReDim ValuesForCombination(ssaItem.len - 1)
    ReDim Combinazioni(ssaItem.len - 1)
    For j = 0 To UBound(ValuesForCombination)
      Set ValuesForCombination(j) = New Collection
      ' prendo il primo valore preso dall'inizializzazione
      ' considero solo le posizioni interessate dalla move
      ValuesForCombination(j).Add Mid(ssaItem.value(0), j + 1, 1)
    Next
    For j = 0 To UBound(Combinazioni)
      Combinazioni(j) = 1
    Next
  End If

  For i = 0 To UBound(dataCmp)
    'MOVE sulla SSA
    If dataCmp(i).moved Then
      subFieldPos = 0 'init (usato come flag sotto...)
      'SQ
      'Campo interno a ssaItem (o coincidente)
      If (dataCmp(i).Posizione >= ssaItem.pos) And (dataCmp(i).byte + dataCmp(i).Posizione <= ssaItem.pos + ssaItem.len) Then
        ssaItem.moved = True
        subFieldPos = 1
        posfirstccode = 1
        subFieldLen = dataCmp(i).byte
        itemValue_init = Left(ssaItem.value(0), dataCmp(i).Posizione - ssaItem.pos)
        itemValue_end = Mid(ssaItem.value(0), subFieldPos + subFieldLen + 1)
      'parte finale campo dentro (intersezione a sinistra o compreso)
      ElseIf (dataCmp(i).Posizione < ssaItem.pos) And (dataCmp(i).byte + dataCmp(i).Posizione > ssaItem.pos) And (dataCmp(i).byte + dataCmp(i).Posizione <= ssaItem.pos + ssaItem.len) Then
        ssaItem.moved = True
        subFieldPos = ssaItem.pos - dataCmp(i).Posizione + 1
        subFieldLen = dataCmp(i).Posizione + dataCmp(i).byte - ssaItem.pos
        posfirstccode = ssaItem.pos - dataCmp(i).Posizione + 1
        itemValue_init = "" 'init
        itemValue_end = Right(ssaItem.value(0), ssaItem.len - subFieldLen)  'verificare...
      ElseIf (dataCmp(i).Posizione > ssaItem.pos) And (dataCmp(i).Posizione < ssaItem.pos + ssaItem.len) And (dataCmp(i).byte + dataCmp(i).Posizione >= ssaItem.pos + ssaItem.len) Then
        'parte iniziale campo dentro (intersezione a destra o compreso)
        ssaItem.moved = True
        subFieldPos = 1
        posfirstccode = 1
        'subFieldLen = ssaItem.pos + ssaItem.len - (1 + dataCmp(i).byte)
        subFieldLen = ssaItem.pos + ssaItem.len - dataCmp(i).Posizione + 1
        itemValue_init = "" 'init
        itemValue_end = Left(ssaItem.value(0), ssaItem.len - subFieldLen)  'verificare...
      'campo comprende ssa
      ElseIf (dataCmp(i).Posizione <= ssaItem.pos) And (dataCmp(i).byte + dataCmp(i).Posizione >= ssaItem.pos + ssaItem.len) Then
        ssaItem.moved = True
        subFieldPos = ssaItem.pos - dataCmp(i).Posizione + 1
        subFieldLen = ssaItem.len
        posfirstccode = ssaItem.pos - dataCmp(i).Posizione + 1
        itemValue_init = "" 'init
        itemValue_end = "" 'init
      End If

     If subFieldPos Then  'NON POSSO USARE QUESTO FLAG (ENTREREI PER TUTTI I CAMPI MOVED SUCCESSIVI A QUELLO VERO!!!)
     'Indice 0: valore della "PIC VALUE"
      values = Split(dataCmpValues(i), ",")
      StartPos = Split(datastartpos(i), ",")
      Dim ssaMovedValue As String
      If Not combine Then
        For j = 0 To UBound(values)
          'AC 18/06/10 pezza per PLI, risolve i casi in cui in values gi� sottostringa dell'SSA, ma il subFieldPos � relativo a SSA
          If Len(values(j)) < subFieldPos Then
            subFieldPos = 1
          End If
          If Len(values(j)) < subFieldLen Then
            subFieldLen = Len(values(j))
          End If
          '__________________________
          ssaMovedValue = itemValue_init & Mid(values(j), subFieldPos, subFieldLen) & itemValue_end
          'Lo tengo solo se diverso da quello iniziale
          'IRIS-DB If ssaMovedValue <> ssaItem.value(0) Then
           If IIf(ssaMovedValue = "#" Or ssaMovedValue = "&", "*", IIf(ssaMovedValue = "!" Or ssaMovedValue = "|", "+", ssaMovedValue)) <> ssaItem.value(0) Then 'IRIS-DB
            ReDim Preserve ssaItem.value(UBound(ssaItem.value) + 1)
            ssaItem.value(UBound(ssaItem.value)) = ssaMovedValue 'indice 0: value della PIC
          End If
        Next j
      Else  ' calcolo le combinazioni (commandcode) 'IRIS-DB: non � solo per il command code a quanto pare e comunque non ha senso fare tutte le combinazioni di ogni singolo byte...
        For j = 0 To UBound(values) ' per tutti i valori
'         For k = Len(itemValue_init) To Len(itemValue_init) + subFieldLen - 1  ' per tutte le posizioni interessate
'            ' scorro collection per vedere se elemento present
'            For Z = 1 To ValuesForCombination(k).count
'              If ValuesForCombination(k).item(Z) = Mid(values(j), k - Len(itemValue_init) + 1, 1) Then
'                Exit For
'              End If
'            Next
'            If Z = ValuesForCombination(k).count + 1 Then
'               ValuesForCombination(k).Add Mid(values(j), k - Len(itemValue_init) + 1, 1)
'            End If
'          Next
         ' AC modifica il primo agosto 2007 - mi svincolo da itemValue_init e ragiono
         ' sulle posizioni
          normalizedpos = dataCmp(i).Posizione - 1
          startindex = subFieldPos + normalizedpos - ssaItem.pos
          'AC luglio 2010 modifica per operatori PLI
          Dim oldstartindex As Integer
          Dim oldsubFieldLen As Integer
          oldstartindex = startindex
          oldsubFieldLen = subFieldLen
          '------
          If Not (StartPos(j) = "1" Or StartPos(j) = "") Then
            If IsNumeric(StartPos(j)) Then
              startindex = startindex + CInt(StartPos(j)) - 1
              subFieldLen = subFieldLen - (CInt(StartPos(j)) - 1)
            End If
          End If
          'AC luglio 2010 ripristino
          If subFieldLen < 0 Then
            startindex = oldstartindex
            subFieldLen = oldsubFieldLen
          End If
          'AC luglio 2010 pezza
          If posfirstccode > Len(values(j)) Then
            posfirstccode = 1
          End If
          '-----
          For k = startindex To startindex + subFieldLen - 1
            For Z = 1 To ValuesForCombination(k).count
              If ValuesForCombination(k).item(Z) = Mid(values(j), posfirstccode + k - startindex, 1) Then
                Exit For
              End If
            Next Z
            If Z = ValuesForCombination(k).count + 1 Then
               If Not Len(values(j)) < (posfirstccode + k - startindex) Then 'AC 09/07/10 (metteva nella combinazione "" perch� mid oltre lunghezza stringa))
                ValuesForCombination(k).Add Mid(values(j), posfirstccode + k - startindex, 1)
               End If
            End If
          Next k
        Next j
      End If
  
      'Se sono in un campo di gruppo devo controllare anche i sottocampi:
      'If dataCmp(i).Lunghezza Then
        'Exit For
      'End If
     End If
    End If
  Next i
  
  If combine Then
  ' ciclo su elementi e costruisco combinazioni sul sottoinsieme
  ' concatenendo le eventuali parti fisse iniziale e finale
    ssaMovedValue = ""
    While AggiornaCombinazioneSSAItem(Combinazioni, ValuesForCombination)
      For j = 0 To UBound(ValuesForCombination)
        ssaMovedValue = ssaMovedValue & ValuesForCombination(j).item(Combinazioni(j))
      Next
      ReDim Preserve ssaItem.value(UBound(ssaItem.value) + 1)
      ssaItem.value(UBound(ssaItem.value)) = ssaMovedValue 'indice 0: value della PIC
      ssaMovedValue = ""
    Wend
  End If
  Exit Sub
error:
  addSegnalazione "", "SSAWRONGMOVE", ""
  
End Sub
'
'Function CercaPcb(wNomPcb) As String
'...
'End Function

Function AggiornaCombinazioneSSAItem(ArrayCombinazioni() As Integer, ArrayCollection() As Collection) As Boolean
Dim lowelement As Integer, i As Integer, maxnum As Integer
Dim aggiornato As Boolean
  aggiornato = False
  For i = 0 To UBound(ArrayCombinazioni)
    lowelement = ArrayCombinazioni(i)
    lowelement = lowelement + 1
    maxnum = ArrayCollection(i).count
    If lowelement <= maxnum Then
      ArrayCombinazioni(i) = lowelement
      aggiornato = True
      Exit For
    Else
      ArrayCombinazioni(i) = 1
    End If
  Next
  AggiornaCombinazioneSSAItem = aggiornato
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ verificare...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getPFK(wRec As String, pDevType As String, pFMTName As String)
  Dim rs As Recordset
  Dim contPFK As String, linePFK As String, PfkField As String, pTasto As String, pValore As String
    
  'SQ
'  If Len(wRec) > 71 Then wRec = Left(wRec, 71)
'  If InStr(wRec, "*") > 0 And Not InStr(wRec, "'*'") > 0 Then
'    wRec = Left(wRec, InStr(wRec, "*") - 1)
'  End If
'  'If Right(wRec, 1) = "X" Then wRec = Left(wRec, Len(wRec) - 1)
'  While Not Right(wRec, 1) = ")"
'     Line Input #nFBms, contPFK
'     contPFK = Trim(Left(contPFK, 71))
'     If InStr(1, contPFK, "*") > 0 And Not InStr(1, contPFK, "'*'") > 0 Then
'       contPFK = Left(contPFK, InStr(1, contPFK, "*") - 1)
'     End If
'     'wRec = wRec & Trim(Left(contPFK, InStr(1, Trim(contPFK), "  ")))
'     wRec = wRec & contPFK
'  Wend
  linePFK = Analizza_PFK(wRec)
  
  m_Fun.FnConnection.Execute "DELETE FROM PsDLI_MFSPfk WHERE FMTName ='" & pFMTName & "'"
  
  PfkField = Left(linePFK, InStr(linePFK, ",") - 1)
  m_Fun.FnConnection.Execute "Update PsDLI_MFS Set PFKField = '" & PfkField _
                             & "' where FMTName ='" & pFMTName & "' and DevType = '(3270,2)'"
                             
  linePFK = Right(linePFK, Len(linePFK) - (Len(PfkField) + 1))
  Set rs = m_Fun.Open_Recordset("Select * From PsDLI_MFSPfk")
  linePFK = Trim(linePFK)
  If Not Left(linePFK, 1) = "'" Then
    'Coppie tasto valore
    While InStr(linePFK, ",")
      If CoppiaTastoValore(Left(linePFK, InStr(linePFK, ",") - 1), pTasto, pValore) Then
        rs.AddNew
        rs!idOggetto = GbIdOggetto
        rs!fmtName = pFMTName
        rs!Dev = pDevType
        rs!tasto = pTasto
        rs!Valore = Trim(pValore)
        rs.Update
      End If
      linePFK = Trim(Mid(linePFK, InStr(1, linePFK, ",") + 1, Len(linePFK) - InStr(1, linePFK, ",")))
    Wend
    If CoppiaTastoValore(linePFK, pTasto, pValore) Then
      rs.AddNew
      rs!idOggetto = GbIdOggetto
      rs!fmtName = pFMTName
      rs!Dev = pDevType
      rs!tasto = pTasto
      rs!Valore = Trim(pValore)
      rs.Update
    End If
  Else   ' singoli
    Dim progressivo As Integer
    progressivo = 0
    While (InStr(1, linePFK, ",")) > 0
          If Not linePFK = "," Then
            progressivo = progressivo + 1
            rs.AddNew
            rs!idOggetto = GbIdOggetto
            rs!fmtName = pFMTName
            rs!Dev = pDevType
            rs!tasto = progressivo
            rs!Valore = Trim(pulisciCommentiPFK(Left(linePFK, InStr(1, linePFK, ",") - 1)))
            rs.Update
          End If
          linePFK = Mid(linePFK, InStr(1, linePFK, ",") + 1, Len(linePFK) - InStr(1, linePFK, ","))
    Wend
    If Not linePFK = "" Then
      progressivo = progressivo + 1
      rs.AddNew
      rs!idOggetto = GbIdOggetto
      rs!fmtName = pFMTName
      rs!Dev = pDevType
      rs!tasto = progressivo
      rs!Valore = Trim(pulisciCommentiPFK(linePFK))
      rs.Update
    End If
  End If
  rs.Close
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Function getDBDType(dbdname As String, Optional rs As Recordset) As e_DBDType
  'Dim rs As Recordset
  Dim DBDType As String
  
  If rs Is Nothing Then
    Set rs = m_Fun.Open_Recordset("Select Tipo_DBD From BS_Oggetti Where Nome = '" & dbdname & "' And Tipo = 'DBD'")
    If rs.RecordCount Then
      DBDType = rs!Tipo_DBD & ""
    End If
    rs.Close
  Else
    DBDType = rs!Tipo_DBD & ""
  End If
      
  Select Case DBDType
    Case "GSA"
      getDBDType = DBD_GSAM
    Case "HID"
      getDBDType = DBD_HIDAM
    Case "HDA"
      getDBDType = DBD_HIDAM
    Case "IDX"
      getDBDType = DBD_INDEX
    Case "LOG"
      getDBDType = DBD_LOGIC
    Case Else
      getDBDType = DBD_UNKNOWN_TYPE
  End Select
End Function
Function getObsolete(dbdname As String) As Boolean
'''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim rsdbd As Recordset
  Set rsdbd = m_Fun.Open_Recordset("select * from PsDli_DBD where dbd_name = '" & dbdname & "'")
  If Not rsdbd.EOF Then
     getObsolete = rsdbd!obsolete
  Else
     getObsolete = False
  End If
      
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ - ex CercaIdField
' 23-01-07 - Gestione DBD logici...
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Function CercaIdField(wIdSegm, NomeFld, DBDType As e_DBDType) As Variant
  Dim tb As Recordset
  'Dim phIdSegmento As Long
  Dim wIdSegm2 As Long 'IRIS-DB
  
  On Error GoTo errdb
  
  GbXName = False
  NomeFld = Replace(NomeFld, "'", "")
  NomeFld = Replace(Replace(NomeFld, "(", ""), ")", "")
  
  'IRIS-DB start - aggiunta logici, per ora fa un loop con una join per prendere solo quelli che matchano
  If DBDType = DBD_LOGIC Then
    Set tb = m_Fun.Open_Recordset("select IdField from PsDli_field as a, PsDLI_Segmenti as b, PsDLI_Segmenti_SOURCE as c, bs_oggetti as d where c.idsegmento = " & wIdSegm & " and a.nome = '" & NomeFld & "'" _
             & " and c.segmento = b.nome and c.DBD = d.nome and d.idoggetto = b.idoggetto and b.idsegmento = a.idsegmento")
    If tb.RecordCount Then
      CercaIdField = tb!idField
    Else
      tb.Close
      Set tb = m_Fun.Open_Recordset("select IdXDField from PsDli_XDfield as a, PsDLI_Segmenti as b, PsDLI_Segmenti_SOURCE as c, bs_oggetti as d where c.idsegmento = " & wIdSegm & " and a.nome = '" & NomeFld & "'" _
               & " and c.segmento = b.nome and c.DBD = d.nome and d.idoggetto = b.idoggetto and b.idsegmento = a.idsegmento")
      If tb.RecordCount Then
        CercaIdField = tb!IdXDField * -1
      Else
        CercaIdField = 0
      End If
    End If
    tb.Close
    Exit Function
  End If
  'IRIS-DB end
  
  If Len(NomeFld) > 0 And wIdSegm Then
    Set tb = m_Fun.Open_Recordset("select IdField from PsDli_field where idsegmento = " & wIdSegm & " and nome = '" & NomeFld & "'")
    If tb.RecordCount > 0 Then
      If tb.RecordCount = 1 Then
         CercaIdField = tb!idField
      Else  'N idField!?
         CercaIdField = 0
      End If
      tb.Close
      Exit Function
    End If
    
    'SQ:
    'NUOVA GESTIONE XDFIELD: numeri negativi...
    Set tb = m_Fun.Open_Recordset("select IdXDField from PsDli_XDField where idsegmento = " & wIdSegm & " and nome = '" & NomeFld & "' ")
    If tb.RecordCount Then
      CercaIdField = tb!IdXDField * -1
      tb.Close
      Exit Function
    End If
    
    '??????QUESTO CI VA??????????
    Set tb = m_Fun.Open_Recordset("select * from PsDli_field where idsegmento = " & wIdSegm & " and ptr_xnome = '" & NomeFld & "' ")
    If tb.RecordCount > 0 Then
      tb.MoveLast
      If tb.RecordCount = 1 Then
         CercaIdField = tb!idField
         GbXName = True
      Else
         CercaIdField = 0
      End If
    Else
      CercaIdField = 0
    End If
  End If
  Exit Function
errdb:
  'writeLog...
  CercaIdField = 0
End Function
Function getIdField(wIdSegm, NomeFld) As Long
  Dim tb As Recordset
  'Dim phIdSegmento As Long
  
  On Error GoTo errdb
  
  GbXName = False
  NomeFld = Replace(NomeFld, "'", "")
  NomeFld = Replace(Replace(NomeFld, "(", ""), ")", "")
  
  If Len(NomeFld) > 0 And wIdSegm Then
    Set tb = m_Fun.Open_Recordset("select IdField from PsDli_field where idsegmento = " & wIdSegm & " and nome = '" & NomeFld & "'")
    If tb.RecordCount > 0 Then
      If tb.RecordCount = 1 Then
         getIdField = tb!idField
      Else  'N idField!?
         getIdField = 0
      End If
      tb.Close
      Exit Function
    End If
    
    'SQ:
    'NUOVA GESTIONE XDFIELD: numeri negativi...
    Set tb = m_Fun.Open_Recordset("select IdXDField from PsDli_XDField where idsegmento = " & wIdSegm & " and nome = '" & NomeFld & "' ")
    If tb.RecordCount Then
      getIdField = tb!IdXDField * -1
      tb.Close
      Exit Function
    End If
    
    '??????QUESTO CI VA??????????
    Set tb = m_Fun.Open_Recordset("select * from PsDli_field where idsegmento = " & wIdSegm & " and ptr_xnome = '" & NomeFld & "' ")
    If tb.RecordCount > 0 Then
      tb.MoveLast
      If tb.RecordCount = 1 Then
         getIdField = tb!idField
         GbXName = True
      Else
         getIdField = 0
      End If
    Else
      getIdField = 0
    End If
  End If
  Exit Function
errdb:
  'writeLog...
  getIdField = 0
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''
''''' Non rifare tutte le query!...
''''' Ma DEVE stare fuori ciclo: devo avere completato tutte le istruzioni!
'''''''''''''''''''''''''''''''''''''''''''''''''
''''Public Sub Resolve_Single_GH_REPL_DLET()
''''   Dim rs As Recordset, rsLiv As Recordset, rsKey As Recordset
''''   Dim wFlagValida As Boolean
''''   Dim rsAdd As Recordset
''''   Dim rsCurIstr As Recordset
''''   Dim numpcb As New Collection
''''   Dim riga As New Collection
''''   Dim i As Long
''''
''''  'SQ COPY-ISTR
''''  'Set rsCurIstr = m_Fun.Open_Recordset("select * From PsDli_Istruzioni Where IdPgm=" & GbIdOggetto & " AND IdOggetto=" & IdOggettoRel & " And (Istruzione = 'DLET' Or Istruzione = 'REPL') and not valida")
''''  Set rsCurIstr = m_Fun.Open_Recordset("select * From PsDli_Istruzioni Where " & _
''''                                       "IdPgm=" & GbIdOggetto & " AND " & _
''''                                       "(Istruzione = 'DLET' Or Istruzione = 'REPL') and not valida")
''''  While Not rsCurIstr.EOF
''''    'SQ init
''''    wFlagValida = False
''''    'SQ COPY-ISTR - Anche GHP - IdSegmento > 0
''''    'Set rs = m_Fun.Open_Recordset("Select * From PsDLi_Istruzioni Where IdPgm=" & GbIdOggetto & " AND IdOggetto=" & IdOggettoRel & " And NumPcb = '" & rsCurIstr!numpcb & "' And Istruzione in ('GHU','GHN','GHNP')")
''''    Set rs = m_Fun.Open_Recordset("Select * From PsDLi_Istruzioni Where " & _
''''                                  "IdPgm=" & GbIdOggetto & " AND " & _
''''                                  "NumPcb = '" & rsCurIstr!numpcb & "' And Istruzione in ('GHU','GHN','GHNP')")
''''    If rs.RecordCount = 1 Then
''''        'SQ COPY-ISTR
''''        'Set rsLiv = m_Fun.Open_Recordset("Select * From PsDli_IstrDett_liv Where IdPgm=" & GbIdOggetto & " AND IdOggetto=" & IdOggettoRel & " And Riga = " & rs!riga & " order By Livello")
''''        Set rsLiv = m_Fun.Open_Recordset("Select * From PsDli_IstrDett_liv Where " & _
''''                                         "IdPgm=" & GbIdOggetto & " And " & _
''''                                         "Riga = " & rs!riga & " order By Livello")
''''        If rsLiv.RecordCount > 0 Then
''''           rsLiv.MoveLast   'Ci serve l'ultimo livello
''''
''''           'SQ - Non deve brasare tutti i livelli!????!!!
''''           'm_Fun.FnConnection.Execute "delete * From PsDli_IstrDett_Liv Where IdPgm=" & GbIdOggetto & " AND IdOggetto=" & IdOggettoRel & " And Riga = " & rsCurIstr!riga
''''           m_Fun.FnConnection.Execute "delete * From PsDli_IstrDett_Liv Where " & _
''''                                      "IdPgm=" & GbIdOggetto & " AND IdOggetto=" & IdOggettoRel & " And Riga = " & rsCurIstr!riga
''''           'Aggiornamento/Aggiunta livello:
''''           'Set rsAdd = m_Fun.Open_Recordset("select * From PsDli_IstrDett_Liv Where IdPgm=" & GbIdOggetto & " AND IdOggetto=" & IdOggettoRel & " And Riga = " & rsCurIstr!riga & " And Livello = " & rsLiv!livello)
''''           Set rsAdd = m_Fun.Open_Recordset("select * From PsDli_IstrDett_Liv Where " & _
''''                                            "IdPgm=" & GbIdOggetto & " And " & _
''''                                            "Idoggetto=" & IdOggettoRel & " AND " & _
''''                                            "Riga = " & rsCurIstr!riga & " And Livello = " & rsLiv!livello)
''''           If rsAdd.RecordCount = 0 Then rsAdd.AddNew
''''           'SQ COPY-ISTR
''''           rsAdd!idpgm = rsLiv!idpgm
''''           rsAdd!idOggetto = rsLiv!idOggetto
''''           rsAdd!riga = rsCurIstr!riga
''''           rsAdd!livello = 1
''''           rsAdd!NomeSSA = "" 'rsLiv!NomeSSA & ""
''''           rsAdd!stridoggetto = rsLiv!stridoggetto
''''           rsAdd!strIdArea = rsLiv!strIdArea
''''           rsAdd!IdSegmento = rsLiv!IdSegmento
''''           rsAdd!VariabileSegmento = rsLiv!VariabileSegmento & ""
''''           rsAdd!condizione = rsLiv!condizione & ""
''''           rsAdd!SegLen = rsLiv!SegLen
''''           rsAdd!Qualificazione = rsLiv!Qualificazione
''''           'SQ 20-03-07
''''           'rsAdd!valida = wFlagValida
''''           rsAdd!valida = rsLiv!valida
''''           rsAdd!Correzione = True
''''           rsAdd.Update
''''           rsAdd.Close
''''           'SQ
''''           wFlagValida = rsLiv!valida
''''        End If
''''        rsLiv.Close
''''        'SQ 20-03-07
''''        'wFlagValida = true
''''     'ElseIf rs.RecordCount = 0 Then
''''     '   wFlagValida = False
''''     'ElseIf rs.RecordCount > 1 Then
''''     '   wFlagValida = False
''''     End If
''''     rs.Close
''''
''''     rsCurIstr!valida = wFlagValida
''''     rsCurIstr.Update
''''
''''     'SQ non c'� gi�?!
''''     'If Not wFlagValida Then
''''     '   addSegnalazione " ", "NOSEGM", " "
''''     '   GbErrLevel = "P-8"
''''     'End If
''''
''''     rsCurIstr.MoveNext
''''  Wend
''''  rsCurIstr.Close
''''
''''End Sub

'''''''''''''''''''''''''''''''''''''''''''''
' Non rifare tutte le query!...
' Ma DEVE stare fuori ciclo: devo avere completato tutte le istruzioni!
'''''''''''''''''''''''''''''''''''''''''''''
Public Sub resolve_Istr_DLET_REPL()
  Dim rs As Recordset, rsLiv As Recordset, rsKey As Recordset
  Dim wFlagValida As Boolean
  Dim rsadd As Recordset
  Dim rsCurIstr As Recordset
  Dim numpcb As New Collection
  Dim Riga As New Collection
  Dim i As Long
  Dim rsDataArea As Recordset
  Dim rsGHIstr As Recordset
  
  'SQ COPY-ISTR
  Set rsCurIstr = m_Fun.Open_Recordset("select * From PsDli_Istruzioni Where " & _
    "IdPgm=" & GbIdOggetto & " AND " & _
    "(Istruzione = 'DLET' Or Istruzione = 'REPL') and not valida")
  While Not rsCurIstr.EOF
    'SQ init
    wFlagValida = False
    ' Mi serve solo per trovare la DataArea
    Set rsDataArea = m_Fun.Open_Recordset("select DataArea from PsDli_IstrDett_Liv where " & _
      "IdPgm = " & rsCurIstr!idpgm & " AND " & _
      "IdOggetto = " & rsCurIstr!idOggetto & " AND " & _
      "Riga = " & rsCurIstr!Riga)
    ' Dovrebbe essercene solo 1
    If rsDataArea.RecordCount > 0 Then
      'SQ 17-10-07 - Non � meglio SOLO ISTRUZIONI VALIDE?!
      Set rs = m_Fun.Open_Recordset("Select distinct b.idsegmento From " & _
        "PsDLi_Istruzioni as a, PsDli_IstrDett_Liv as b,PsDLI_Istruzioni_Clone as c  Where " & _
        "a.IdPgm=" & GbIdOggetto & " AND b.dataarea = '" & rsDataArea!dataarea & "" & "' and " & _
        "a.idpgm = b.idpgm and a.idoggetto = b.idoggetto and a.riga = b.riga  and " & _
        "a.NumPcb = '" & rsCurIstr!numpcb & "' And (a.Istruzione in ('GHU','GHN','GHNP')" & _
        " or a.IdPgm = c.idpgm and a.IdOggetto = c.IdOggetto and a.Riga = c.Riga and c.istruzione in ('GHU','GHN','GHNP')) and not b.idsegmento = 0")
      'stefano: stavolta � meglio inserirlo!!!
      'If rs.RecordCount = 1 Then
      'SQ 17-10-07 questa � sbagliata! IdSegmento pu� essere 0!...
      'If rs.RecordCount = 1 And Len(rs!IdSegmento) > 0 Then
      If rs.RecordCount = 1 Then
        If "0" & rs!idSegmento > 0 Then
          'SQ COPY-ISTR
          Set rsGHIstr = m_Fun.Open_Recordset("select  b.idoggetto, b.riga, b.livello from PsDli_Istruzioni as a, PsDli_IstrDett_Liv as b,PsDLI_Istruzioni_Clone as c where " & _
            "a.IdPgm=" & GbIdOggetto & " AND b.dataarea = '" & rsDataArea!dataarea & "" & "' and " & _
            "a.idpgm = b.idpgm and a.idoggetto = b.idoggetto and a.riga = b.riga  and " & _
            "a.NumPcb = '" & rsCurIstr!numpcb & "' And (a.Istruzione in ('GHU','GHN','GHNP') or a.IdPgm = c.idpgm and a.IdOggetto = c.IdOggetto and a.Riga = c.Riga and c.istruzione in ('GHU','GHN','GHNP'))and " & _
                                      "b.idsegmento = " & rs!idSegmento)
          If rsGHIstr.RecordCount > 0 Then
            Set rsLiv = m_Fun.Open_Recordset("Select * From PsDli_IstrDett_liv Where " & _
                                             "IdPgm=" & GbIdOggetto & " And " & _
                                             "Idoggetto=" & rsGHIstr!idOggetto & " And " & _
                                             "Riga = " & rsGHIstr!Riga & " And " & _
                                             "livello = " & rsGHIstr!livello)
            If rsLiv.RecordCount > 0 Then
      
              'SQ - brasa tutti i livelli... siamo sicuri?
              m_Fun.FnConnection.Execute "delete * From PsDli_IstrDett_Liv Where " & _
                                         "IdPgm=" & GbIdOggetto & " And IdOggetto=" & rsCurIstr!idOggetto & " And " & _
                                         "Riga = " & rsCurIstr!Riga
              'Aggiornamento/Aggiunta livello:
              Set rsadd = m_Fun.Open_Recordset("select * From PsDli_IstrDett_Liv Where " & _
                                               "IdPgm=" & GbIdOggetto & " And " & _
                                               "Idoggetto=" & rsCurIstr!idOggetto & " And " & _
                                               "Riga = " & rsCurIstr!Riga)
              rsadd.AddNew
              'SQ COPY-ISTR
              rsadd!idpgm = GbIdOggetto
              rsadd!idOggetto = rsCurIstr!idOggetto
              rsadd!Riga = rsCurIstr!Riga
              rsadd!livello = 1
              rsadd!NomeSSA = "" 'rsLiv!NomeSSA & ""
              rsadd!dataarea = rsLiv!dataarea ' Mauro
              rsadd!stridoggetto = rsLiv!stridoggetto
              rsadd!strIdArea = rsLiv!strIdArea
              rsadd!AreaLen = rsLiv!AreaLen ' Mauro
              rsadd!idSegmento = rsLiv!idSegmento
              
              rsadd!IdSegmento_moved = rsLiv!IdSegmento_moved ' Mauro
              rsadd!IdSegmentiD = rsLiv!IdSegmentiD ' Mauro
              rsadd!VariabileSegmento = rsLiv!VariabileSegmento & ""
              rsadd!condizione = rsLiv!condizione & ""
              rsadd!SegLen = rsLiv!SegLen
              rsadd!SegmentiLend = rsLiv!SegmentiLend ' Mauro
              rsadd!Qualificazione = rsLiv!Qualificazione 'SQ ?!
              'rsAdd!Qualificazione_moved = rsLiv!Qualificazione_moved ' Mauro
              rsadd!valida = rsLiv!valida
              rsadd!Correzione = True
              'rsAdd!Qualandunqual = rsLiv!Qualandunqual ' Mauro
              rsadd.Update
              rsadd.Close
              ' Mauro 06/07/2009 - Oltre al segmento, aggiorno anche il DBD
              If Len(rsLiv!IdSegmentiD) > 0 Or rsLiv!idSegmento > 0 Then
                If rsLiv!idSegmento > 0 Then
                  Aggiorna_DBD rsLiv!idSegmento, GbIdOggetto, rsCurIstr!idOggetto, rsCurIstr!Riga
                Else
                  Aggiorna_DBD Left(rsLiv!IdSegmentiD, InStr(rsLiv!IdSegmentiD & ",", ",") - 1), _
                               GbIdOggetto, rsCurIstr!idOggetto, rsCurIstr!Riga
                End If
              End If
              'SQ
              wFlagValida = rsLiv!valida
            End If
            rsLiv.Close
          End If
          rsGHIstr.Close
        ElseIf rs.RecordCount > 1 Then
          ' ??????????????????????????????
          '   wFlagValida = False
        End If
      End If
      rs.Close
    End If
    rsDataArea.Close

    rsCurIstr!valida = wFlagValida
    If wFlagValida Then
      ClearSegnalazioneDLET_REPL rsCurIstr!idpgm, rsCurIstr!idOggetto, rsCurIstr!Riga
    End If
    rsCurIstr.Update

    rsCurIstr.MoveNext
  Wend
  rsCurIstr.Close
  
End Sub

Sub ClearSegnalazioneDLET_REPL(idpgm As Long, idOggetto As Long, Riga As Long)
  Dim rs As New Recordset, IdOggettoRel As Long
  'If IdPgm = IdOggetto Then
    'IdOggettoRel = 0
  'Else
    IdOggettoRel = idpgm
  'End If
  Set rs = m_Fun.Open_Recordset( _
        "Delete from Bs_Segnalazioni where idoggetto=" & idOggetto & " and IdOggettoRel = " & IdOggettoRel & " and riga=" _
        & Riga & " and (codice='F10' or codice = 'I01') ")
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ - CPY-INSTR
' - Cerca le cpy con IMS (COBOL, PLI)
' - Seleziona le istruzioni "base" (con idPgm=0) da PsDli_Istruzioni
' - Copia l'istruzione base con IdPgm corretto
' - Esegue il parsing dell'istruzione
''
' SQ 6-05-08: cambio gestione (x problema PCB_mg)
' - Il record sulla PsDli_Istruzioni non viene pi� cancellato (a inizio parsing II),
'   ma "resettato" (come per i casi "normali")
'   -> se esiste gi�, non lo clono!
' ATTENZIONE: SE RIFACCIO IL PARSING DI I LIVELLO DELLA COPY...
'  HO TUTTI RECORD POTENZIALMENTE SBALLATI!
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub manageCpyInstructions()
  Dim rs As Recordset, rsCopy As Recordset, rsIstruzioni As Recordset
  Dim i As Integer
  
  ''''''''''''''''''''''''''''''''''
  'SQ 29-05-08
  ''
  ' COPY INNESTATE (solo 2 livelli)
  ''''''''''''''''''''''''''''''''''
  Set rsCopy = m_Fun.Open_Recordset( _
    "SELECT idOggettoR FROM PsRel_Obj WHERE idOggettoC=" & GbIdOggetto & " AND relazione IN ('CPY','INC')" & _
    " UNION " & _
    "SELECT b.idoggettoR FROM PsRel_Obj AS A, PsRel_Obj AS B " & _
    " WHERE a.idOggettoC=" & GbIdOggetto & " AND a.relazione IN ('CPY','INC') AND " & _
    " A.idOggettoR=b.idOggettoC and b.relazione IN ('CPY','INC')")
  While Not rsCopy.EOF
    Set rsIstruzioni = m_Fun.Open_Recordset( _
      "SELECT * from PsDLI_Istruzioni WHERE idOggetto=" & rsCopy!idOggettoR & " AND idPgm=0")
    While Not rsIstruzioni.EOF
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Se il record non esiste (parsing II mai effettuato prima), CLONO
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Set rs = m_Fun.Open_Recordset("SELECT * FROM PsDLI_Istruzioni WHERE idPgm=" & GbIdOggetto & " AND idOggetto=" & rsIstruzioni!idOggetto & " AND riga=" & rsIstruzioni!Riga)
      If rs.EOF Then
        rs.AddNew
        'SQ pericoloso: dipendente dalla posizione delle colonne... meglio evitare!
        'rs.Fields.item(0) = GbIdOggetto
        'For i = 1 To rsIstruzioni.Fields.count - 1
        For i = 0 To rsIstruzioni.Fields.count - 1
          If rs.Fields.item(i).Name = "idPgm" Then
            rs.Fields.item(i) = GbIdOggetto
          Else
            '''''''''''''''''''''''''''''''''''''''
            ' GESTIONE PCB:
            ' IL "GIOCHINO" DELLE MG" SALTAVA PER LE COPY...
            ' SQ 6-05-08  IMPORTANTISSIMO! (VERIFICARE BENE)
            '''''''''''''''''''''''''''''''''''''''
            If rs.Fields.item(i).Name = "OrdPCB_mg" Then
              'Deve rimanere il vecchio... solo che ho gi� cancellato il record!
              DoEvents
            ElseIf Not IsNull(rsIstruzioni.Fields.item(i)) Then
              rs.Fields.item(i) = rsIstruzioni.Fields.item(i)
            End If
          End If
        Next
        rs.Update
      End If
      
      parseInstructions rs
      
      AggErrLevelRel IdOggettoRel
      
      Parser.PsConnection.Execute "Update Bs_Oggetti set Errlevel = '" & GbErrLevelRel & "' where IdOggetto = " & IdOggettoRel
      
      rs.Close
      rsIstruzioni.MoveNext
    Wend
    rsIstruzioni.Close
    rsCopy.MoveNext
  Wend
  rsCopy.Close
End Sub

Public Sub AggErrLevelRel(wIdObj As Long)
  Dim tb As Recordset
  
  GbErrLevelRel = ""
  Set tb = m_Fun.Open_Recordset("Select * from bs_segnalazioni where idoggettoRel = " & wIdObj)
  While Not tb.EOF
    SettaErrLevelREL tb!gravita, tb!Tipologia
    tb.MoveNext
  Wend
  tb.Close
End Sub

Sub SettaErrLevelREL(wPeso, wAmb)
  Dim k As Integer, xPeso As Integer
  Dim xAmb As String, wErrL1 As String, wErrL2 As String
  
  Select Case wPeso
    Case "0"
      xPeso = "0"
    Case "I"
      xPeso = "02"
    Case "W"
      xPeso = "04"
    Case "E"
      xPeso = "08"
    Case "S"
      xPeso = "12"
  End Select
  
  Select Case wAmb
    Case "DATI"
      xAmb = "D"
    Case Else
      xAmb = "P"
  End Select

  k = InStr(GbErrLevelRel, "-")
  If k = 0 Then
    wErrL1 = ""
    wErrL2 = "00"
  Else
    wErrL1 = Mid$(GbErrLevelRel, 1, k - 1)
    wErrL2 = Mid$(GbErrLevelRel, k + 1)
  End If

  If InStr(wErrL1, xAmb) = 0 Then
    wErrL1 = wErrL1 & xAmb
  End If

  If xPeso > wErrL2 Then wErrL2 = xPeso

  GbErrLevelRel = wErrL1 & "-" & wErrL2
End Sub

Function CheckClonePcbConsistency(ordpcb As Integer) As Boolean
Dim rs As Recordset, tb As Recordset, rsdbd As Recordset
Dim segmentsQuery As String, psbString As String, dbdWhereCond As String
Dim i As Integer, namedb As String
  
Dim psbInstr() As Long
 psbInstr = psbPgm
  
 For i = 0 To UBound(psbInstr)
   psbString = psbString & IIf(Len(psbString), ",", "") & psbInstr(i)
 Next i
 
 Set rs = m_Fun.Open_Recordset("select distinct b.idOggetto as IdDbd from PsDLI_Psb as a,bs_oggetti as b where " & _
                                  "a.idOggetto IN (" & psbString & ") and NumPcb = " & ordpcb & " and typePcb = 'DB' and " & _
                                  "a.dbdName = b.nome and b.tipo = 'DBD'")
 If rs.RecordCount Then
  dbdWhereCond = "("
  ReDim wIdDBD(rs.RecordCount - 1)
  For i = 0 To UBound(wIdDBD)
    wIdDBD(i) = rs!idDBD
    dbdWhereCond = dbdWhereCond & "idOggetto = " & rs!idDBD
    If i = UBound(wIdDBD) Then  'ultimo giro
      dbdWhereCond = dbdWhereCond & ")"
    Else
      dbdWhereCond = dbdWhereCond & " OR "
    End If
    rs.MoveNext
  Next i
 Else
  CheckClonePcbConsistency = False
  rs.Close
  Exit Function
 End If
 rs.Close
 If Len(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Segment) Then  'Almeno un segmento (valido):
      
      If UBound(TabIstr.BlkIstr) = 1 And Len(TabIstr.BlkIstr(1).Segment) And Len(dbdWhereCond) = 0 Then
        segmentsQuery = "select IdOggetto from PsDli_Segmenti where nome = '" & Replace(TabIstr.BlkIstr(1).Segment, "'", "") & "'"
        'Se non ho PSB o PCB non filtro i DBD: su tutti!
      Else
        'filtro su tutti i nomi di segmento dell'istruzione...
        'es:
        'select idOggetto from
        '  (SELECT IdOggetto, Count(*) AS occurs
        '  From psDLI_Segmenti
        '  WHERE (Nome='XXX' Or Nome='YYY')
        '  GROUP BY IdOggetto)
        'Where Occurs = 2
        segmentsQuery = "select IdOggetto from " & _
                        "(select idOggetto,count(*) as occurs from psDLI_Segmenti where ("
        For i = 1 To UBound(TabIstr.BlkIstr)
          segmentsQuery = segmentsQuery & "nome = '" & Replace(TabIstr.BlkIstr(i).Segment, "'", "") & "'"
          If i = UBound(TabIstr.BlkIstr) Then
            segmentsQuery = segmentsQuery & ")"
          Else
            segmentsQuery = segmentsQuery & " OR "
          End If
        Next i
        segmentsQuery = segmentsQuery & " group by idOggetto)" & _
                          " where occurs =" & UBound(TabIstr.BlkIstr)
        segmentsQuery = segmentsQuery & " AND " & dbdWhereCond
      End If
      Set tb = m_Fun.Open_Recordset(segmentsQuery)
      If Not tb.EOF Then
        CheckClonePcbConsistency = True
      End If
      tb.Close
  End If
End Function
Sub AggiornaCloniPcb(pRecord As Recordset)
Dim rs As Recordset, rscheck As Recordset, pcbvalue As String, i As Integer
  Set rs = m_Fun.Open_Recordset("Select distinct Valore from PsData_CmpMoved where " & _
                                " IdPgm = " & GbIdOggetto & " and Nome = '" & pRecord!numpcb & "'")
  While Not rs.EOF
    
    pcbvalue = getIstrValue(rs!Valore)
    If Not pcbvalue = pRecord!ordpcb And IsNumeric(pcbvalue) Then
      i = i + 1
      Set rscheck = m_Fun.Open_Recordset("Select Istruzione from PsDLI_Istruzioni_Clone where " & _
                                             "IdPgm = " & GbIdOggetto & " and IdOggetto =" & IdOggettoRel & " and " & _
                                             "Riga =" & pRecord!Riga & " and ordPcb = " & pcbvalue)
          If rscheck.EOF Then
            'SQ CPY-ISTR
            If CheckClonePcbConsistency(CInt(pcbvalue)) Then
              Parser.PsConnection.Execute "INSERT INTO PsDLI_Istruzioni_Clone (IdPgm,IdOggetto,Riga,IdClone,OrdPcb) " & _
                                        "VALUES (" & GbIdOggetto & "," & IdOggettoRel & "," & pRecord!Riga & "," & _
                                        i & "," & CInt(pcbvalue) & ")"
            End If
          Else
            i = i - 1
          End If
          rscheck.Close
    End If
    rs.MoveNext
  Wend
End Sub
Sub AggiornaCloniIstruzione(pRecordIstr As Recordset, campo As String, parametri As String)
  Dim rs As Recordset, rscheck As Recordset
  Dim i As Integer
  Dim istruzione As String
  Dim mancaistr As Boolean
  
  If noparsercloni Then
    noparsercloni = False
    Exit Sub
  End If

  Set rs = findDataCmp(GbIdOggetto, campo)
  If rs.RecordCount Then
    If rs!Tipo = "9" Then
      'stefano: GESTIONE CONTATORE PARAMETRI: ANDREBBE GESTITO CON UN FLAG SUL DB AL MOMENTO DEL
      '  PARSER DI PRIMO LIVELLO!!!!
      'rs.Close
      ' Mauro/SQ : 13/11/2009
      'AggiornaCloniIstruzione pRecordIstr, nextToken_new(parametri), parametri
      istruzione = nextToken_new(parametri)
      If Left(istruzione, 1) <> "'" Then
        AggiornaCloniIstruzione pRecordIstr, istruzione, parametri
      End If
      Exit Sub
    End If
  End If
  rs.Close
  
  mancaistr = False
  If pRecordIstr!istruzione = "" Then
    mancaistr = True
  End If
  Set rs = m_Fun.Open_Recordset("Select distinct Valore from PsData_CmpMoved where " & _
                                " IdPgm = " & GbIdOggetto & " and Nome = '" & campo & "'")
  If Not rs.EOF Then
    Parser.PsConnection.Execute "UPDATE PsDli_Istruzioni SET Istruzione_moved = true where " & _
                                "idPgm=" & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " and riga=" & pRecordIstr!Riga
  End If
  
  While Not rs.EOF
    'VARIABILE/COSTANTI (perfezionare...)
    'If Left(rs!Valore, 1) = "'" Then
    '  Istruzione = Trim(Replace(rs!Valore, "'", ""))
    'Else
    '  Istruzione = getFieldValue(rs!Valore)
    'End If
    istruzione = getIstrValue(rs!Valore)
  
    If istruzione <> pRecordIstr!istruzione And Len(istruzione) Then 'IRIS-DB
      ' update Cloni
      i = i + 1
      If mancaistr Then
        'SQ CPY-ISTR
        Parser.PsConnection.Execute "UPDATE PsDli_Istruzioni SET Istruzione ='" & istruzione & "' where " & _
                                    "idPgm=" & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " and riga=" & pRecordIstr!Riga
        pRecordIstr!istruzione = istruzione
        mancaistr = False
        i = 0
      Else
        ' virgilio 8/3/2007
        'Set rscheck = m_Fun.Open_Recordset("Select Istruzione from PsDLI_Istruzioni_Clone where " & _
        '                                   "IdOggetto =" & GbIdOggetto & " and Riga =" & pRecordIstr!riga & " and " & _
        '                                   "Istruzione ='" & istruzione & "'")
        Set rscheck = m_Fun.Open_Recordset("Select Istruzione from PsDLI_Istruzioni_Clone where " & _
                                           "IdPgm = " & GbIdOggetto & " and IdOggetto =" & IdOggettoRel & " and " & _
                                           "Riga =" & pRecordIstr!Riga & " and Istruzione ='" & istruzione & "'")
        If rscheck.EOF Then
          'SQ CPY-ISTR
          Parser.PsConnection.Execute "INSERT INTO PsDLI_Istruzioni_Clone (IdPgm,IdOggetto,Riga,IdClone,Istruzione) " & _
                                      "VALUES (" & GbIdOggetto & "," & IdOggettoRel & "," & pRecordIstr!Riga & "," & _
                                      i & ",'" & istruzione & "')"
        Else
          i = i - 1
        End If
        rscheck.Close
      End If
    End If
    rs.MoveNext
  Wend
  rs.Close
  
  ' E se ho delle MOVE in copy?!?
  Set rs = m_Fun.Open_Recordset("SELECT a.valore from PsData_CmpMoved AS a, PsRel_Obj AS b WHERE " & _
                                "b.idOggettoC = " & GbIdOggetto & " And b.relazione = 'CPY' And " & _
                                "a.IdPgm = b.IdOggettoR and a.nome = '" & campo & "'")
  If Not rs.EOF Then
    Parser.PsConnection.Execute "UPDATE PsDli_Istruzioni SET Istruzione_moved = true where " & _
                                "idPgm=" & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " and riga=" & pRecordIstr!Riga
  End If
  
  While Not rs.EOF
    'VARIABILE/COSTANTI (perfezionare...)
    If Left(rs!Valore, 1) = "'" Then
      istruzione = Trim(Replace(rs!Valore, "'", ""))
    Else
      istruzione = getFieldValue(rs!Valore)
    End If
  
    If istruzione <> pRecordIstr!istruzione Then
      ' update Cloni
      i = i + 1
      If mancaistr Then
        'SQ CPY-ISTR
        Parser.PsConnection.Execute "UPDATE PsDli_Istruzioni SET Istruzione ='" & istruzione & "' where " & _
                                    "idPgm=" & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " and riga=" & pRecordIstr!Riga
        pRecordIstr!istruzione = istruzione
        mancaistr = False
        i = 0
      Else
        ' virgilio 8/3/2007
        'Set rscheck = m_Fun.Open_Recordset("Select Istruzione from PsDLI_Istruzioni_Clone where " & _
        '                                   "IdOggetto =" & GbIdOggetto & " and Riga =" & pRecordIstr!riga & " and " & _
        '                                   "Istruzione ='" & istruzione & "'")
        Set rscheck = m_Fun.Open_Recordset("Select Istruzione from PsDLI_Istruzioni_Clone where " & _
                                           "IdPgm = " & GbIdOggetto & " and IdOggetto =" & IdOggettoRel & " and " & _
                                           "Riga =" & pRecordIstr!Riga & " and Istruzione ='" & istruzione & "'")
        If rscheck.EOF Then
          'SQ CPY-ISTR
          Parser.PsConnection.Execute "INSERT INTO PsDLI_Istruzioni_Clone (IdPgm,IdOggetto,Riga,IdClone,Istruzione) " & _
                                      "VALUES (" & GbIdOggetto & "," & IdOggettoRel & "," & pRecordIstr!Riga & "," & _
                                      i & ",'" & istruzione & "')"
        Else
          i = i - 1
        End If
        rscheck.Close
      End If
    End If
    rs.MoveNext
  Wend
  rs.Close
End Sub
'SQ - E' quello relativo al RIparsing di una singola istruzione (dal CORRECTOR!)
'Public Sub resetRepository_instruction(Optional IsCloniTodelete As Boolean = True)
'  On Error GoTo err
'
'  Parser.PsConnection.Execute "UPDATE PsDli_Istruzioni SET Valida = false,Correzione = false,numPcb = '',ordPCB = NULL," & _
'                              " ImsDc = false,ImsDB  = false,MapArea = '',MapName = '',MapSet = '',keyfeedBack = '' " & _
'                              "WHERE IdPgm=" & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " AND riga=" & GbOldNumRec
'  Parser.PsConnection.Execute "DELETE * from PsDli_IstrDett_Liv where IdPgm=" & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " AND riga=" & GbOldNumRec
'  Parser.PsConnection.Execute "DELETE * from PsDli_IstrDett_Key where IdPgm=" & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " AND riga=" & GbOldNumRec
'  Parser.PsConnection.Execute "DELETE * from PsDli_IstrDett_Dup where IdPgm=" & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " AND riga=" & GbOldNumRec
'  Parser.PsConnection.Execute "DELETE * from PsDLI_IstrPSB where idpgm = " & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " AND riga=" & GbOldNumRec 'TMP!!!
'  Parser.PsConnection.Execute "DELETE * from PsDLI_IstrDBD where idpgm = " & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " AND riga=" & GbOldNumRec 'TMP!!!
'  If IsCloniTodelete Then
'    Parser.PsConnection.Execute "DELETE * from PsDLI_Istruzioni_Clone where IdPgm=" & GbIdOggetto & " AND idOggetto=" & IdOggettoRel & " AND riga=" & GbOldNumRec
'  End If
'  'AC 13/07/07
'  ' ho fatto modifica che potrebbe avere implicazioni
''  Parser.PsConnection.Execute "DELETE * from Bs_Segnalazioni where idoggetto = " & GbIdOggetto & " and Riga = " & GbOldNumRec & _
''                              " AND (codice like 'I%%' or codice in ('F04','F06','F07','F08','F09','F10','F29','F30','H30','P12'))" 'TMP!!!
'  Parser.PsConnection.Execute "DELETE * from Bs_Segnalazioni where (idoggetto = " & GbIdOggetto & " or idoggettoRel = " & GbIdOggetto & ") and Riga = " & GbOldNumRec & _
'                              " AND (codice like 'I%%' or codice in ('F04','F06','F07','F08','F09','F10','F29','F30','H30','P12'))" 'TMP!!!
'Exit Sub:
'err:
'  If err.Number = -2147467259 Then  'cos'�?
'     Resume Next
'  End If
'End Sub
Public Sub resetRepository_instruction(pIdPgm As Long, pIdOggetto As Long, pRiga As Long, Optional IsCloniTodelete As Boolean = True)
  On Error GoTo err

  Parser.PsConnection.Execute "UPDATE PsDli_Istruzioni SET Valida = false,Correzione = false,numPcb = '',ordPCB = NULL," & _
                              " ImsDc = false,ImsDB  = false,MapArea = '',MapName = '',MapSet = '',keyfeedBack = '',Keys = NULL " & _
                              "WHERE IdPgm=" & pIdPgm & " AND idOggetto=" & pIdOggetto & " AND riga=" & pRiga
  Parser.PsConnection.Execute "DELETE * from PsDli_IstrDett_Liv where IdPgm=" & pIdPgm & " AND idOggetto=" & pIdOggetto & " AND riga=" & pRiga
  Parser.PsConnection.Execute "DELETE * from PsDli_IstrDett_Key where IdPgm=" & pIdPgm & " AND idOggetto=" & pIdOggetto & " AND riga=" & pRiga
  Parser.PsConnection.Execute "DELETE * from PsDli_IstrDett_Dup where IdPgm=" & pIdPgm & " AND idOggetto=" & pIdOggetto & " AND riga=" & pRiga
  Parser.PsConnection.Execute "DELETE * from PsDLI_IstrPSB where idpgm = " & pIdPgm & " AND idOggetto=" & pIdOggetto & " AND riga=" & pRiga 'TMP!!!
  Parser.PsConnection.Execute "DELETE * from PsDLI_IstrDBD where idpgm = " & pIdPgm & " AND idOggetto=" & pIdOggetto & " AND riga=" & pRiga 'TMP!!!
  If IsCloniTodelete Then
    Parser.PsConnection.Execute "DELETE * from PsDLI_Istruzioni_Clone where IdPgm=" & pIdPgm & " AND idOggetto=" & pIdOggetto & " AND riga=" & pRiga
  End If
  'AC 13/07/07
  ' ho fatto modifica che potrebbe avere implicazioni
'  Parser.PsConnection.Execute "DELETE * from Bs_Segnalazioni where idoggetto = " & GbIdOggetto & " and Riga = " & GbOldNumRec & _
'                              " AND (codice like 'I%%' or codice in ('F04','F06','F07','F08','F09','F10','F29','F30','H30','P12'))" 'TMP!!!
  Parser.PsConnection.Execute "DELETE * from Bs_Segnalazioni where (idoggetto = " & pIdOggetto & " or idoggettoRel = " & pIdOggetto & ") and Riga = " & pRiga & _
                              " AND (codice like 'I%%' or codice in ('F04','F06','F07','F08','F09','F10','F29','F30','H30','P12'))" 'TMP!!!
Exit Sub:
err:
  'If err.Number = -2147467259 Then  'SQL Server does not exist or access denied
  If err.Number = -2147217904 Then  'Nessun valore specificato per alcuni parametri necessari
    Parser.PsFinestra.ListItems.Add , , Now & " --> Please, check Repository for Missing Columns"
    Resume Next
  End If
End Sub

Public Sub resetRepository(caller As String)
  Dim idselected As String
  On Error GoTo err
  
  'SQ Il parsing di primo livello (nuovo) imposta gia' i valori "chiave"...
  'SQ - COPY-ISTR (IL PARSING DI II LIVELLO NON LO DOVRO' FARE PER LE "CPY"!...
  'Attenzione: elimino i record generati dal II livello: quelli clonati dal record della copy (idPgm = 0)
  idselected = "IdPgm"
  If GbTipoInPars = "CPY" Or GbTipoInPars = "INC" Then
    idselected = "IdOggetto"
  End If
  Select Case caller
    Case "parseuno"
      ' per la copy
      Parser.PsConnection.Execute "DELETE * from PsDli_Istruzioni where " & idselected & " = " & GbIdOggetto
       
    Case "parsedue"
      'AC 13/03/2008  TO_MIGRATE
      'Default di to_migrate in checkPSB,resetRepository(parsedue) e UpdatePsDliIstruzioni
      ' Settaggio attuale: default = true, diventa false solo se il dbd � stato trovato ma non � da migrare
      ''''''''''''''''''''''''''''''''
      ' IMPORTANTISSIMO! (Saltava la gesitone PCB_mg)!
      ' SQ 6-05-08
      ''''''''''''''''''''''''''''''''
      'Parser.PsConnection.Execute "DELETE * from PsDli_Istruzioni where idPgm = " & GbIdOggetto & " AND idOggetto <> idPgm"
      Parser.PsConnection.Execute "UPDATE PsDli_Istruzioni set Istruzione_moved=false,Valida=iif(istruzione = 'PCB',Valida,false),obsolete=false,to_migrate=true,Correzione=false,numPcb=''," & _
                                  " ordPCB = NULL, ImsDc = false, ImsDB  = false, MapArea = '', MapName = '',MapSet = '',keyfeedBack = '', Keys = NULL where IdPgm = " & GbIdOggetto
       'Qui ok idoggetto: in questo caso (sarebbe idPgm)
      Parser.PsConnection.Execute "DELETE * from Bs_Segnalazioni where idoggetto = " & GbIdOggetto & _
                              " AND (codice like 'I%%' or codice in ('F04','F06','F07','F08','F09','F10','F29','F30','H10','H30','P12'))" 'TMP!!!
  End Select
    
  ' valide per tutti i casi
  
  Parser.PsConnection.Execute "DELETE * from PsDli_IstrDett_Liv where " & idselected & " = " & GbIdOggetto
  Parser.PsConnection.Execute "DELETE * from PsDli_IstrDett_Key where " & idselected & " = " & GbIdOggetto
  Parser.PsConnection.Execute "DELETE * from PsDli_IstrDett_Dup where " & idselected & " = " & GbIdOggetto
  'SQ 11-01-08 (era where idoggetto)
  Parser.PsConnection.Execute "DELETE * from PsDLI_Istruzioni_Clone where " & idselected & " = " & GbIdOggetto
  Parser.PsConnection.Execute "DELETE * from PsDLI_IstrPSB where " & idselected & " = " & GbIdOggetto  'TMP!!!
  Parser.PsConnection.Execute "DELETE * from PsDLI_IstrDBD where " & idselected & " = " & GbIdOggetto  'TMP!!!
 
Exit Sub
err:
  'If err.Number = -2147467259 Then  'SQL Server does not exist or access denied
  If err.Number = -2147217904 Then  'Nessun valore specificato per alcuni parametri necessari
    Parser.PsFinestra.ListItems.Add , , Now & " --> Please, check Repository for Missing Columns"
    Resume Next
  End If
End Sub
  
Sub checkPsbIMS()
  Dim rs As Recordset, rsCat As Recordset, rsRelObj As Recordset

  Set rs = m_Fun.Open_Recordset("select PSB from IMS_Catalog where PSB = '" & GbNomePgm & "'")
  If rs.RecordCount Then
    'IMS-DC
    Set rs = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where nome = '" & GbNomePgm & "' and tipo = 'PSB'")
  Else
    'Nome SRC <> Nome LOAD?
    Set rs = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.source = '" & GbNomePgm & "' and b.load=a.nome and a.tipo = 'PSB'")
  End If
   
  If rs.RecordCount Then
    'psbPgm(0) = rs!idOggetto
    '''''''''''''''''''''''''''
    ' Aggiunta in PsRel_Obj
    '''''''''''''''''''''''''''
    Set rsRelObj = m_Fun.Open_Recordset("Select * from PsRel_Obj where idOggettoC = " & GbIdOggetto & " and idOggettoR = " & rs!idOggetto & " and relazione = 'PSB'")
    If rsRelObj.EOF Then
      rsRelObj.AddNew
      rsRelObj!IdOggettoC = GbIdOggetto
      rsRelObj!idOggettoR = rs!idOggetto
      rsRelObj!relazione = "PSB"
      rsRelObj!nomecomponente = GbNomePgm
      rsRelObj!utilizzo = "PSB-IMS"
      rsRelObj.Update
    End If
    rsRelObj.Close
  End If
  rs.Close
End Sub
'Blocco centrale di parseCbl_II... per
Public Sub parseInstructions(rs As Recordset)
  Dim rsCall As Recordset
  Dim istruzione As String, parametri As String
  
  'While Not rs.EOF
'    Set rsCall = m_Fun.Open_Recordset("Select Parametri from PsCall where IdOggetto = " & rs!idOggetto & " and Riga = " & rs!Riga)
'    'SQ - chiarire cosa fa...
'    If Not rsCall.EOF Then
'      '''''''''''''''''''''''''''''''AggiornaCloniIstruzione rs, nextToken(rsCall!parametri)
'    End If
'    rsCall.Close
    
    istruzione = IIf(IsNull(rs!istruzione), "", rs!istruzione)
    parametri = rs!statement
    GbOldNumRec = rs!Riga 'SQ - OCIO!!!
    'SQ - CPY-ISTR
    IdOggettoRel = rs!idOggetto
    GbSegnObjRel = IdOggettoRel
    ' Mauro 29/04/2008
    TabIstr.keyFeed = ""
    TabIstr.keyFeedLen = "" 'IRIS-DB
    TabIstr.KeyArg = ""
    
    ' Istruzioni in copy
    If rs!idpgm <> rs!idOggetto Then
      Set rsCall = m_Fun.Open_Recordset("Select Parametri from PsCall where " & _
                                        "IdOggetto = " & rs!idOggetto & " and Riga = " & rs!Riga)
      'SQ - chiarire cosa fa...
      If Not rsCall.EOF Then
        AggiornaCloniIstruzione rs, nexttoken(rsCall!parametri), rsCall!parametri
      End If
      rsCall.Close
    End If
    
    Select Case istruzione
      Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP", "POS" 'IRIS-DB
        If rs!isExec Then
          parseGU_exec istruzione, parametri
        Else
          parseGU_call istruzione, parametri
        End If
      Case "DLET", "REPL"
        If rs!isExec Then
          parseREPL_exec istruzione, parametri
        Else
          parseREPL_call istruzione, parametri
        End If
      Case "ISRT"
        If rs!isExec Then
          parseISRT_exec istruzione, parametri
        Else
          parseISRT_call istruzione, parametri
        End If
      Case "CHKP", "SYMCHKP"
        If rs!isExec Then
          parseCHKP_exec istruzione, parametri, rs!idpgm, rs!idOggetto, rs!Riga
          'SQ TMP!!!!! GESTIRE
          rs!Imsdb = True
          rs!valida = True
          rs.Update
        Else
        ' Mauro 12-04-2007 : Attenzione, c'� una routine omonima sul modulo EASYTRIEVE
          MapsdM_IMS.parseCHKP parametri
        End If
      Case "AUTH"
        parseAUTH parametri
      Case "XRST"
        ' Mauro 12-04-2007 : Attenzione, c'� una routine omonima sul modulo EASYTRIEVE
        If Not rs!isExec Then
          MapsdM_IMS.parseXRST parametri
        Else
        'AC 22/05/08 GESTIRE
          MapsdM_IMS.parseXRST_exec istruzione, parametri, rs!idpgm, rs!idOggetto, rs!Riga
          rs!Imsdb = True
          rs!valida = True
          rs.Update
        End If
'IRIS-DB      Case "ICMD"
'IRIS-DB        parseICMD parametri
      Case "CMD", "GCMD", "RCMD" 'IRIS-DB
        parseCMD istruzione, parametri 'IRIS-DB
      Case "CHNG"
        parseCHNG parametri
      Case "INQY"
        parseINQY parametri
      Case "LOG", "ROLB", "ROLL"
        'TMP: riuso il vecchio giro... sostituendo man mano...
        parseROLL istruzione, parametri
        rs!valida = True 'IRIS-DB
        rs!to_migrate = True 'IRIS-DB
        rs!Imsdb = True 'IRIS-DB
        rs!Imsdc = False 'IRIS-DB
        rs.Update 'IRIS-DB
    Case "PCB"  ',"SCHEDULE","SCHD"
      If Not rs!isExec Then
       MapsdM_IMS.parsePCB parametri
      Else
        'AC 22/05/08 GESTIRE
          rs!valida = True
          rs.Update
      End If
    Case "SCHEDULE", "SCHD", "SCHED" 'AC 27/07/10 era stata commentata (ripristinata come la gestione di SCHED dentro i programmi
      If Not rs!isExec Then
       MapsdM_IMS.parsePCB parametri
      Else
        rs!valida = True
        rs!to_migrate = True
        rs!Imsdb = True
        rs!Imsdc = False 'IRIS-DB
        rs.Update
      End If
    Case "TERM"
       MapsdM_IMS.parseTERM parametri
      Case "SYNC"
        rs!valida = True
        rs!Imsdb = True 'IRIS-DB
        rs!Imsdc = False 'IRIS-DB
        rs.Update
      'AC 22/05/08 --> aggiunte query,chkp,symchkp
      Case "CKPT", "APSB", "DPSB", "INIT", "QUERY"
        rs!valida = True
        rs.Update
      Case "CHKP", "SYMCHKP"
        If rs!isExec Then
          MapsdM_IMS.parseCHKP_exec istruzione, parametri, rs!idpgm, rs!idOggetto, rs!Riga
          rs!valida = True
          rs!Imsdb = True
          rs.Update
        Else
          rs!valida = True
          rs.Update
        End If
      Case "OPEN", "CLSE"
        parseOPEN_CLOSE istruzione, parametri
      'IRIS-DB Case "GCMD", "RCMD", "GSCD", "GMSG", "PURG"
      Case "GSCD", "GMSG", "PURG" 'IRIS-DB
        'Fare bene...
        parsePURG istruzione, parametri
      Case "SETB", "SETO", "SETS", "SETU", "ROLS" 'IRIS-DB aggiunta ROLS per coerenza con SETS
        'Fare bene...
        parseSETx istruzione, parametri
      Case Else
        'gestire...
    End Select
  '  rs.MoveNext
  'Wend
  'rs.Close
  '
End Sub

Public Sub erase_Istr_IMSDC_suLivelli_e_Key()
  Dim rs As Recordset
   
  Set rs = m_Fun.Open_Recordset("select * From psDli_Istruzioni Where ImsDC = true")
  While Not rs.EOF
    m_Fun.FnConnection.Execute "delete * from PsDli_istrdett_Key Where Idoggetto = " & rs!idOggetto & " And Riga = " & rs!Riga
    m_Fun.FnConnection.Execute "delete * from PsDli_istrdett_dup Where Idoggetto = " & rs!idOggetto & " And Riga = " & rs!Riga
    '? poi vedremo mi sembra che un record di corrispondenza ci debba essere sempre
    'm_Fun.FnConnection.Execute "delete * from PsDli_istrdett_liv Where Idoggetto = " & rs!idoggetto & " And Riga = " & rs!Riga
    rs.MoveNext
  Wend
  rs.Close
End Sub

Public Function is_GSAM(wNomeDBD As String) As Boolean
  'Restiuisce true se gsma
  Dim rs As Recordset
    
  is_GSAM = False
  Set rs = m_Fun.Open_Recordset("select * From BS_Oggetti Where Nome = '" & wNomeDBD & "' And Tipo = 'DBD' And Tipo_DBD = 'GSA'")
  If rs.RecordCount Then
    is_GSAM = True
  End If
  rs.Close
End Function
'NON USATO
'Public Function transcode_CommandCode(wCcode As Long) As String
'  Select Case wCcode
'    Case CCODE_V_LOCK_SEGM_POSITION
'      transcode_CommandCode = "V"
'    Case CCODE_C_FULL_CONCAT
'      transcode_CommandCode = "C"
'    Case CCODE_D_PATHCALL
'      transcode_CommandCode = "D"
'    Case CCODE_F_FIRST_SEGM_OCCURRENCY
'      transcode_CommandCode = "F"
'    Case CCODE_L_LAST
'      transcode_CommandCode = "L"
'    Case CCODE_N_NOT_SUBSTITUTE_AFTER_GH
'      transcode_CommandCode = "N"
'    Case CCODE_P_SET_PARENT_SEGM
'      transcode_CommandCode = "P"
'    Case CCODE_Q_EXCLUSIVE_ACCESS_SEGM
'      transcode_CommandCode = "Q"
'    Case CCODE_Q_EXCLUSIVE_ACCESS_SEGM
'      transcode_CommandCode = "U"
'    Case CCODE_NULL
'      transcode_CommandCode = "-"
'  End Select
'End Function

' Mauro 06/07/2009
Public Sub Aggiorna_DBD(IdSegm As Long, idpgm As Long, idOggetto As Long, Riga As Long)
  Dim rsadd As Recordset, rsdbd As Recordset
  
  Set rsdbd = m_Fun.Open_Recordset("Select idoggetto from psdli_segmenti where idsegmento = " & IdSegm)
  If rsdbd.RecordCount Then
    Parser.PsConnection.Execute "DELETE * from PsDLI_IstrDBD where " & _
                                "idpgm = " & idpgm & " and idoggetto = " & idOggetto & " and riga = " & Riga
    Set rsadd = m_Fun.Open_Recordset("select * From PsDli_IstrDbd Where " & _
                                     "IdPgm=" & idpgm & " And " & _
                                     "Idoggetto=" & idOggetto & " And " & _
                                     "Riga = " & Riga)
    rsadd.AddNew
    rsadd!idpgm = idpgm
    rsadd!idOggetto = idOggetto
    rsadd!Riga = Riga
    rsadd!idDBD = rsdbd!idOggetto
    
    rsadd.Update
    rsadd.Close
  End If
  rsdbd.Close
End Sub

' Mauro 04/11/2009
Public Function IsCMPAT(psb As String, idObj As Long) As Boolean
  Dim rs As Recordset, rsObj As Recordset
  
  Set rs = m_Fun.Open_Recordset("Select Min(NumPcb) From PsDli_Psb Where idOggetto in (" & psb & ")")
  If rs.Fields(0) > 1 Then
    Set rsObj = m_Fun.Open_Recordset("select cics from bs_oggetti where idoggetto = " & idObj & " and cics = true")
    IsCMPAT = rsObj.RecordCount = 0
    rsObj.Close
  Else
    IsCMPAT = True
  End If
  rs.Close
End Function

Public Function getIstrValue(Valore As String) As String
    If Left(Valore, 1) = "'" Then
      getIstrValue = Trim(Replace(Valore, "'", ""))
    Else
      Select Case UCase(Valore) 'IRIS-DB changed ICMD to CMD, added POS
        Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP", "POS", _
             "DLET", "REPL", "ISRT", _
             "PCB", "TERM", _
             "AUTH", "GCMD", "CMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG", _
             "SETB", "SETO", "SETS", "SETU", "XRST", _
             "SYNC", "CHKP", "CKPT", "APSB", "DPSB", "INIT", "INQY", "LOG", "ROLB", "ROLL", "ROLS", _
             "OPEN", "CLSE", _
             "FOR"
          getIstrValue = Trim(UCase(Valore))
        Case Else
          'VALUE:
          getIstrValue = getFieldValue(Valore)
      End Select
    End If
End Function
