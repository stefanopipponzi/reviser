Attribute VB_Name = "MapsdM_FORTE"
Option Explicit

Const migrationPath = "Output-prj\Languages\Forte2Java"
Const templatePath = "Input-prj\Languages\Forte2Java"
Dim CurrentTag As String
Dim RTB As RichTextBox, RTBImport As RichTextBox
Dim stBatchName As String
Dim StDataStep As String, SrProcName As String, StMacroName As String
Dim BolIsData As Boolean
Dim DataStep As Collection
Dim DataStep1 As Collection
Dim IntDataNum As Integer
Dim IntDataNum1 As Integer
Dim IntDatastep As Integer
Dim IntProcNum As Integer
Dim BolMultiData As Boolean
Dim booEliminaRiga As Boolean
Dim BolOutput As Boolean
Dim BolMerge As Boolean
Dim BolIsFile As Boolean
Dim BolIsMacro As Boolean
''Type DBDataSet
''  'DataSetname As String
''  fieldName() As String
''  fieldType() As String
''End Type
''Dim dataField() As DBDataSet

Dim ColImport As Collection
Dim ColClass As Collection
'''
Dim Countdst As Integer

Public Sub convertPGM()
  Dim previousTag As String
  Dim newriga As String
  Dim i As Long
  Set tagValues = New Collection
  
  tagValues.Add "", "import(i)"
  tagValues.Add "", "nomeimport(i)"
  tagValues.Add "", "attribute(i)"
  tagValues.Add "", "method(i)"
  tagValues.Add "", "istructions(i)"
'
  GbNomePgm = LCase(GbNomePgm)
  m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm
  
'  stBatchName = UCase(Chr(Asc(GbNomePgm))) & Right(GbNomePgm, Len(GbNomePgm) - 1)
'  changeTag RTB, "<BatchName>", UCase(stBatchName)
  Set RTB = MapsdF_Parser.rText
  Set RTBImport = MapsdF_Parser.rText1
  RTBImport.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\import"

  parsForte

  writeTag "class(i)", RTB
  
  cleanTags RTB

  On Error GoTo saveErr
  'RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & stBatchName & "Main.java", 1
 Exit Sub
saveErr:
  If err.Number = 75 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath
    Resume
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.Description, False
  End If
End Sub

Sub parsForte()
  Dim nFJava As Long, i As Long, j As Long, k As Long
  Dim wRec As String, statement As String
  Dim token As String, line As String
  Dim previousTag As String
  Dim nextTkn As String, stclass As String, stoldclass As String, stinherits As String, numFileTXT As Long
  Dim label As String, multilinea As Integer
  Dim stappo As String, stmetodo As String, stimplement As String, stparam As String, stparameters As String, sttipo As String
  Dim stvar As String

  On Error GoTo errorHandler
  BolIsData = False
  BolMultiData = False
  BolOutput = False
  BolMerge = False
  BolIsFile = False
  BolIsMacro = False
  IntDataNum = 0
  IntDataNum1 = 0
  IntDatastep = 0
  IntProcNum = 0
  GbNumRec = 0
  Set ColImport = New Collection
  Set ColClass = New Collection
  
  If Len(Dir(GbFileInput)) = 0 Then
    MsgBox "This file is not avaliable...", vbExclamation, Parser.PsNomeProdotto
    Exit Sub
  End If
  nFJava = FreeFile
  Open GbFileInput For Input As nFJava
  While Not EOF(nFJava)
  
    While Len(Trim(line)) = 0 And Not EOF(nFJava)
      Line Input #nFJava, line
      If InStr(line, "-- END METHOD DEFINITIONS") Then
        RTB.LoadFile m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm & "\" & stclass & ".java", 1
        cleanTags RTB
        RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm & "\" & stclass & ".java", 1
        Close #nFJava
        Exit Sub
      End If
      GbNumRec = GbNumRec + 1
      RipulisciRiga_FORTE line
    Wend
    statement = line
    If line = "" Then
      token = ""
    Else
'    multilinea = 0
'    Do While (Right(Trim(line), 1) <> ";") And Not EOF(nFJava)
'      Line Input #nFJava, line
'      GbNumRec = GbNumRec + 1
'      multilinea = multilinea + 1
'      RipulisciRiga_FORTE line
'      If statement = "" Then
'         GbOldNumRec = GbNumRec
'      End If
'      statement = statement & " " & line
'    Loop
      'While Right(statement, 1) = ";"
      '  statement = RTrim(Left(statement, Len(statement) - 1))
      'Wend
      If InStr(statement, ";") Then
        statement = Left(statement, InStr(statement, ";") - 1)
        line = Mid(statement, InStr(statement, ";") + 1) & ";"
      End If
      statement = Replace(statement, "^=", " ^= ")
      statement = Replace(statement, "=", " = ")
      statement = Replace(statement, "-", " - ")
      statement = Replace(statement, "+", " + ")
      statement = Replace(statement, "> =", " >=")
      statement = Replace(statement, "< =", " <=")
      'statement = Replace(statement, "*", " * ")
      token = nextToken_newnew(statement)
    End If
    'token = UCase(token)
    Select Case UCase(token)
      Case "BEGIN"
        token = nextToken_newnew(statement)
        If UCase(token) = "TOOL" Or UCase(token) = "C" Then
          line = ""
        Else
          tagWrite "// " & token & " " & statement
        End If
        line = ""
      Case "INCLUDES"
        '''ColImport.Add statement, statement
        tagWrite "import " & LCase(statement) & ".*;", "nomeimport(i)"
        line = ""
      Case "FORWARD"
        line = ""
      Case "CLASS"
        writeTag "nomeimport(i)", RTBImport
        RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\class"
        changeTag RTBImport, "<nomeimport(i)>", ""
        changeTag RTB, "<nomepack>", GbNomePgm
        changeTag RTB, "<import>", RTBImport.text   ''' m_Fun.FnPathPrj & "\" & templatePath & "\import"
      
        stclass = nextToken_newnew(statement)
        stclass = UCase(Chr(Asc(LCase(stclass)))) & Right(LCase(stclass), Len(stclass) - 1)
        changeTag RTB, "<nomeclass>", stclass
        stappo = nextToken_newnew(statement)
        line = ""
        If stappo = "inherits" Then
          stappo = nextToken_newnew(statement)
          stinherits = nextToken_newnew(statement)
          stinherits = LCase(Left(stinherits, 1)) & Right(stinherits, Len(stinherits) - 1)
          If InStr(stinherits, ".") Then
            stinherits = LCase(Left(stinherits, InStr(stinherits, ".") - 1)) & "." & UCase(Chr(Asc(LCase(Mid(stinherits, InStr(stinherits, ".") + 1))))) & LCase(Mid(stinherits, InStr(stinherits, ".") + 2))
          End If
          changeTag RTB, "<extends>", "extends " & stinherits
        End If
        ColClass.Add stclass & " + " & stinherits, stclass
        If InStr(statement, "implements") Then
          statement = Trim(Mid(statement, InStr(statement, "implements") + 10))
          stimplement = LCase(Left(statement, 1)) & Right(statement, Len(statement) - 1)
          If InStr(stimplement, ".") Then
            stimplement = LCase(Left(stimplement, InStr(stimplement, ".") - 1)) & "." & UCase(Chr(Asc(LCase(Mid(stimplement, InStr(stimplement, ".") + 1))))) & LCase(Mid(stimplement, InStr(stimplement, ".") + 2))
          End If
          changeTag RTB, "<implements>", stimplement
        Else
          changeTag RTB, "<implements>", ""
        End If
        RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm & "\" & stclass & ".java", 1
      Case "IMPLEMENTS"
        stappo = ColClass.item(ColClass.count)
        ColClass.Remove (ColClass.count)
        line = ""
      Case "METHOD"
        stparam = ""
        stparameters = ""
        stimplement = ""
        Do While (Trim(line) <> "begin") And Not EOF(nFJava)
          Line Input #nFJava, line
          statement = statement & " " & line
        Loop
        statement = Replace(Replace(statement, "(", " ( "), ")", " ) ")
        statement = Replace(statement, ":", " : ")
        statement = Replace(statement, "begin", "")
        stappo = nextToken_newnew(statement)
        stclass = Split(stappo, ".")(0)
        stclass = UCase(Chr(Asc(LCase(stclass)))) & Right(LCase(stclass), Len(stclass) - 1)
        stmetodo = LCase(Split(stappo, ".")(1))
        
        If stoldclass <> stclass And stoldclass <> "" Then
         ' RTB.LoadFile m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm & "\" & stoldclass & ".java", 1
          changeTag RTB, "<<method(i)>>", ""
          changeTag RTB, "<istructions(i)>", ""
          RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm & "\" & stoldclass & ".java", 1
           RTB.LoadFile m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm & "\" & stclass & ".java", 1
        Else
          If stoldclass = "" Then RTB.LoadFile m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm & "\" & stclass & ".java", 1
        End If
        stoldclass = stclass
        changeTag RTB, "<<method(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\method"
        changeTag RTB, "<nomemetodo>", stmetodo
        stappo = nextToken_newnew(statement)
        If stappo = ":" Then
          stimplement = nextToken_newnew(statement)
          If InStr(stimplement, ".") Then
            stimplement = LCase(Left(stimplement, InStr(stimplement, ".") - 1)) & "." & UCase(Chr(Asc(LCase(Mid(stimplement, InStr(stimplement, ".") + 1))))) & LCase(Mid(stimplement, InStr(stimplement, ".") + 2))
          End If
          changeTag RTB, "<void>", stimplement
          changeTag RTB, "<parameters>", ""
          changeTag RTB, "<returnvalue>", stimplement & " returnvalue = null;"
          changeTag RTB, "<return>", "return returnvalue;"
        Else
          If stappo <> "" Then
            changeTag RTB, "<void>", "void"
            stappo = Replace(Replace(stappo, "(", ""), ")", "")
            stappo = Replace(Replace(stappo, " input ", ""), " output ", "")
            If Trim(stappo) <> "" Then
              For i = 0 To UBound(Split(stappo, ","))
                stparam = Trim(Split(stappo, ",")(i))
                sttipo = nextToken_newnew(Trim(Split(stparam, ":")(1)))
                If InStr(sttipo, ".") Then
                  sttipo = LCase(Left(sttipo, InStr(sttipo, ".") - 1)) & "." & UCase(Chr(Asc(LCase(Mid(sttipo, InStr(sttipo, ".") + 1))))) & LCase(Mid(sttipo, InStr(sttipo, ".") + 2))
                End If
                stvar = Replace(Replace(LCase(Trim(Split(stparam, ":")(0))), "input ", ""), "output ", "")
                stparameters = stparameters & sttipo & " " & stvar & ","
              Next
              changeTag RTB, "<parameters>", Left(stparameters, Len(stparameters) - 1)
            End If
          Else
            changeTag RTB, "<void>", "void"
            changeTag RTB, "<parameters>", ""
          End If
          changeTag RTB, "<return>", ""
        End If
        If Len(statement) Then
          stappo = nextToken_newnew(statement)
          If stappo = ":" Then
            stimplement = nextToken_newnew(statement)
            If InStr(stimplement, ".") Then
              stimplement = LCase(Left(stimplement, InStr(stimplement, ".") - 1)) & "." & UCase(Chr(Asc(LCase(Mid(stimplement, InStr(stimplement, ".") + 1))))) & LCase(Mid(stimplement, InStr(stimplement, ".") + 2))
            End If
            changeTag RTB, "<void>", stimplement
            changeTag RTB, "<parameters>", ""
            changeTag RTB, "<returnvalue>", stimplement & " returnvalue = null;"
            changeTag RTB, "<return>", "return returnvalue;"
          End If
        End If
        changeTag RTB, "<returnvalue>", ""
        ''cleanTags RTB
        'RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm & "\" & stclass & ".java", 1
        'RTB.LoadFile m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm & "\" & stclass & ".java", 1
        CurrentTag = "istructions(i)"
        line = ""
      Case "END"
        If UCase(statement) = "METHOD" Then
          writeTag "istructions(i)", RTB
          changeTag RTB, "<istructions(i)>", ""
          'cleanTags RTB
          RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\project\" & GbNomePgm & "\" & stclass & ".java", 1
        End If
         line = ""
      Case "HAS"
        line = ""
        
      Case Else
        tagWrite "// " & token & " " & statement
        line = ""
    End Select
  Wend
  
  Close #nFJava
  
  Exit Sub
errorHandler:
  ''writeLog "E", "Unexpected Error: " & err.Description
    m_Fun.writeLog "Conversion Fort� to java: id#" & GbIdOggetto & ", ###" & err.Description
catch:
  Resume Next
End Sub

Private Sub RipulisciRiga_FORTE(ByRef line As String)
  Dim InizioCommento As Double
  Dim FineCommento As Double
  
  line = Trim(line)
  ' Su MainFrame "dovrebbe" essere corretto troncare a colonna 72, ma su Unix non ci sono limiti
  
  If InStr(1, line, "--") Or InStr(1, line, "//") Then
    'getComment line, CurrentTag
    line = ""
  Else
     InizioCommento = InStr(1, line, "/*")
     If InizioCommento > 0 Then
        FineCommento = InStr(InizioCommento + 2, line, "*/")
        If FineCommento = 0 Then
          FineCommento = InStr(InizioCommento + 2, line, "*")
        End If
        If FineCommento = 0 Then
          booEliminaRiga = True
          FineCommento = Len(line) + 1
        End If
        'getComment Mid(line, InizioCommento + 1), CurrentTag
        line = Trim(Mid(line, 1, InizioCommento - 1)) & Trim(Mid(line, FineCommento + 3))
      End If
  End If
  
   
End Sub

Private Sub tagWrite(line As String, Optional tagName As String)
  Dim tagValue As String
  
  On Error GoTo catch
  
  If Len(tagName) = 0 Then
    tagName = CurrentTag
  End If
  
  tagValue = tagValues.item(tagName)
  tagValues.Remove tagName
  tagValues.Add tagValue & line & vbCrLf, tagName
  
  Exit Sub
catch:
  If err.Number = 5 Then '424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    tagValues.Add "", CurrentTag
    Resume
  End If
End Sub

Private Sub writeTag(Tag As String, RTB As RichTextBox)
  Dim tagValue As String
  
  On Error GoTo catch
  
  tagValue = tagValues.item(Tag)
  changeTag RTB, "<" & Tag & ">", tagValue
  tagValues.Remove Tag
  tagValues.Add "", Tag
  
  Exit Sub
catch:
  If err.Number = 424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    'tagValues.Add "", Tag
    'Resume
  End If
End Sub


'SILVIA 02-10-2008
''''''''''''''''''''''''''''''''''''''''''''''
' Scrittura Commenti
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getComment(statement As String, Optional tagName As String)
  On Error GoTo catch
  tagWrite "// " & statement, tagName
  Exit Sub
catch:
  writeLog "E", "parsePgm_I: error on getComment!"
End Sub

