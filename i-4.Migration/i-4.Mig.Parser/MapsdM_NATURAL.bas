Attribute VB_Name = "MapsdM_NATURAL"
Option Explicit
Option Compare Text

Dim entryIndex As Integer, builtinIndex As Integer
Dim entries() As String, builtins() As String
Dim nFCbl As Long
'silvia 24-04-2008
Type NatData
  Nalevel As String
  NaSegno As String
  NaName As String
  isNaGroup As Boolean
  NaFieldtype As String
  Nabytes As Long
  NaLen As Long
  NaDec As Long
  'modifierValue As String
  Navalue As String
  NaRedefines As String
  NaOccurs As Integer
  DatPos As Integer
End Type
Dim dataList(10000) As NatData
Dim listIndex As Long
Dim Start As Long
Dim idArea As Long
Dim lOrdinale As Long

''
''''''''''''''''''''''''''''''''
' Parsing I livello
' ...
'  analizza solo le chiamate dei programmi Natural
''''''''''''''''''''''''''''''''
Sub parsePgm_I()
  Dim i As Long, j As Long
  Dim wRec As String, statement As String
  Dim token As String, line As String
  Dim isDEFINE As Boolean
  Dim manageMove As String  'Fare globale Parser
  
  Dim TabLen(99) As Double, TabPos(50, 3) As Double
  Dim ILen As Double, IPos As Double, wpos As Double, I01 As Double
  Dim SavLev As Integer, k As Integer
  On Error GoTo parseErr
  
  'Non leggiamo tutte le volte!
  manageMove = m_Fun.LeggiParam("IMSDC-MOVEYN") = "YES"
    
  GbNumRec = 0            'init
  ReDim entries(100)      'init
  entryIndex = 0          'init
  ReDim builtins(100)     'init
  builtinIndex = 0
  SwNomeCall = True
  ReDim DataValues(0)
  ReDim CampiMove(0)
  Idx = 0 'serve?
  listIndex = 0
  
  Parser.PsConnection.Execute "Delete * from PsData_Area where idoggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsData_Cmp where idoggetto = " & GbIdOggetto

  nFCbl = FreeFile
  Open GbFileInput For Input As nFCbl
  While Not EOF(nFCbl)
    'SQ:
'    While Len(line) = 0 And Not EOF(nFCbl)
'      Line Input #nFCbl, line
'      GbNumRec = GbNumRec + 1
'    Wend
'    statement = line
'    GbOldNumRec = GbNumRec  'linea iniziale statement
'    While Left(line, 1) = "*" And Not EOF(nFCbl)
'      Line Input #nFCbl, line
'      GbNumRec = GbNumRec + 1
'      RipulisciRiga line
'      statement = statement & " " & line
'    Wend
   
    While Len(line) = 0 And Not EOF(nFCbl)
      Line Input #nFCbl, line
      GbNumRec = GbNumRec + 1
      If Len(line) Then
        If Left(line, 1) = "*" Then
          line = ""
        Else
          'RipulisciRiga line
        End If
      End If
    Wend
    statement = line
    GbOldNumRec = GbNumRec
    
    token = nexttoken(statement)
    If token <> "" Then
      Select Case token
        Case "DEFINE"
          token = nexttoken(statement)
          If token = "DATA" Then
            isDEFINE = True
          End If
        Case "END-DEFINE"
          isDEFINE = False
        Case "INCLUDE"
          getInclude statement
        Case "FETCH"
          getFETCH statement
        Case "EXEC"
          token = nexttoken(statement)
          Select Case token
            Case "DLI"
              'getEXEC_DLI statement
            Case "SQL"
              'getEXEC_SQL statement
            Case "CICS"
              'getEXEC_CICS statement
          End Select
        Case "CALL", "CALLNAT"
          getCALL statement, token
        Case "LOCAL"
          token = nexttoken(statement)
          If token = "USING" Then
            getUSING statement
          End If
        Case "MOVE"
          If manageMove Then
            getMove statement
          End If
        Case "PARAMETER"
          token = nexttoken(statement)
          If token = "USING" Then
            getUSING statement
          End If
        Case "END-DEFINE"
           isDEFINE = False
           line = ""
       ' Case "FORMAT", "AT"
       '   line = ""
        Case Else
          'LABEL:
          'If Right(token, 1) = ":" Then
          '  token = nextToken(statement)
          '  If token = "PROC" Then
          '    parsePROC statement
          '  End If
          'Else
            If manageMove Then
              Dim lValue As String
              Dim rValue As String
              'ASSEGNAMENTO:
              If Left(statement, 2) = ":=" Then
                lValue = token
                rValue = Trim(Mid(statement, 3))
                getPliMove lValue, rValue, CampiMove  'SQ: Per il PLI!! se generico, renderlo tale!
              End If
            End If
          'End If
           If isDEFINE Then getDEFINE statement, token, True
          line = ""
      End Select
    End If
    line = ""
  Wend
  Close #nFCbl
  
  If listIndex > 1 Then

    '********************************************************
    'Calcolo delle lunghezze dei livelli superiori
    '********************************************************
    'listIndex = listIndex - 1
    SavLev = Val(dataList(listIndex).Nalevel)
    ILen = SavLev
    TabLen(ILen) = dataList(listIndex).Nabytes
    Start = listIndex - 1
    If SavLev = 88 Then
      For Start = Start To 2 Step -1
        If Val(dataList(Start).Nalevel) <> 88 Then
          If Val(dataList(Start).Nalevel) = 1 Then
            Start = Start - 1
            SavLev = Val(dataList(Start).Nalevel)
            ILen = SavLev
            TabLen(ILen) = dataList(Start).Nabytes
          Else
            SavLev = Val(dataList(Start).Nalevel)
            ILen = SavLev
          End If
          Exit For
        End If
      Next Start
    End If
    For Start = Start To 1 Step -1
        If Val(dataList(Start).Nalevel) = SavLev Then
           If Val(dataList(Start).Nalevel) <> 88 Then
              If dataList(Start).NaOccurs = 0 Then
                TabLen(ILen) = TabLen(ILen) + dataList(Start).Nabytes
              Else
                TabLen(ILen) = TabLen(ILen) + (dataList(Start).Nabytes * dataList(Start).NaOccurs)
              End If
           End If
        Else
          If Val(dataList(Start).Nalevel) > SavLev Then
            SavLev = Val(dataList(Start).Nalevel)
      '      ILen = ILen - 1
            ILen = SavLev
            If ILen < 0 Then
               ILen = 0
            End If
            If dataList(Start).NaOccurs = 0 Then
               TabLen(ILen) = dataList(Start).Nabytes
            Else
               TabLen(ILen) = (dataList(Start).Nabytes * dataList(Start).NaOccurs)
            End If
          Else
            SavLev = Val(dataList(Start).Nalevel)
            If dataList(Start).NaOccurs = 0 Then
              dataList(Start).Nabytes = TabLen(ILen)
            Else
              dataList(Start).Nabytes = TabLen(ILen) * dataList(Start).NaOccurs
            End If
          
    '        ILen = ILen + 1
            ILen = SavLev
            If SavLev = 1 Then
               TabLen(ILen) = 0
            Else
               TabLen(ILen) = TabLen(ILen) + dataList(Start).Nabytes
            End If
            For k = ILen + 1 To 99
               TabLen(k) = 0
            Next k
          End If
        End If
        ' Mauro 23-02-2007: Aggiunto il controllo per il livello 88, altrimenti ogni volta lo cancella
        If Trim(dataList(Start).NaRedefines & "") <> "" And dataList(Start).Nalevel <> 88 Then
            'SG : TabLen(ILen) = TabLen(ILen) - dataList(Start).Nabytes
            TabLen(ILen) = Abs(TabLen(ILen) - dataList(Start).Nabytes)
        End If
    Next Start
    ' ****************************************
    ' * Calcolo Scostamenti dei campi *
    ' ****************************************
    I01 = 1
    SavLev = Val(dataList(2).Nalevel)
    ILen = 1
    TabLen(ILen) = dataList(2).Nabytes
    IPos = 1
    TabPos(IPos, 1) = dataList(1).Nalevel
    TabPos(IPos, 2) = 1
    TabPos(IPos, 3) = dataList(1).Nabytes
    dataList(1).DatPos = 1
    For Start = 2 To listIndex
        If Val(dataList(Start).Nalevel) = 88 Then
          dataList(Start).DatPos = dataList(Start - 1).DatPos
        Else
          If Val(dataList(Start).Nalevel) = 1 Or Val(dataList(Start).Nalevel) = 77 Then
             If dataList(I01).Nabytes = 0 Then
                dataList(I01).Nabytes = TabLen(ILen)
             End If
             TabLen(ILen) = 0
             I01 = Start
             IPos = 1
             TabPos(IPos, 1) = dataList(Start).Nalevel
             TabPos(IPos, 2) = 1
             dataList(Start).DatPos = 1
             TabPos(IPos, 3) = dataList(Start).Nabytes
             SavLev = Val(dataList(1).Nalevel)
          Else
            If Val(dataList(Start).Nalevel) = SavLev Then
               If Trim(dataList(Start).NaRedefines & "") = "" Then
                 TabLen(ILen) = TabLen(ILen) + dataList(Start).Nabytes
               End If
            End If
            If dataList(Start).Nalevel = TabPos(IPos, 1) Then
              If dataList(Start).NaRedefines <> "" Then
                dataList(Start).DatPos = TabPos(IPos, 2)
              Else
                TabPos(IPos, 2) = TabPos(IPos, 2) + TabPos(IPos, 3)
                TabPos(IPos, 3) = dataList(Start).Nabytes
                dataList(Start).DatPos = TabPos(IPos, 2)
              End If
            Else
              If dataList(Start).Nalevel > dataList(Start - 1).Nalevel Then
                IPos = IPos + 1
                TabPos(IPos, 1) = dataList(Start).Nalevel
                TabPos(IPos, 2) = TabPos(IPos - 1, 2)
                TabPos(IPos, 3) = dataList(Start).Nabytes
                dataList(Start).DatPos = TabPos(IPos, 2)
              Else
                For IPos = IPos To 1 Step -1
                   If dataList(Start).Nalevel = TabPos(IPos, 1) Then Exit For
                Next IPos
                If dataList(Start).NaRedefines <> "" Then
                  dataList(Start).DatPos = TabPos(IPos, 2)
                Else
                  TabPos(IPos, 2) = TabPos(IPos, 2) + TabPos(IPos, 3)
                  TabPos(IPos, 3) = dataList(Start).Nabytes
                  dataList(Start).DatPos = TabPos(IPos, 2)
                End If
              End If
            End If
          End If
        End If
    Next Start
    If dataList(I01).Nabytes = 0 Then
       dataList(I01).Nabytes = TabLen(ILen)
    End If
  
    writeDataCmp dataList, listIndex + 1
    
    If manageMove Then
      For i = 1 To UBound(CampiMove)
         For j = 1 To CampiMove(i).Valori.count
           Parser.PsConnection.Execute "INSERT INTO PsData_CmpMoved (IdPgm,Nome,valore) Values (" & GbIdOggetto & ",'" & CampiMove(i).NomeCampo & "','" & Replace(Left(CampiMove(i).Valori.item(j), 255), "'", "''") & "')"
         Next
       'End If
      Next
    End If
  End If
  Exit Sub
parseErr:    'Gestito a livello superiore...
  If err.Number = 123 Then
    'QUESTO E' PER IL COBOL. SISTEMARE...
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(Trim(line), 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #nFCbl, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'gestire
      'GbErrLevel = "P-0"
      GbOldNumRec = GbNumRec
      addSegnalazione line, "P00", line
      'm_Fun.WriteLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & Err.Description
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.Description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
    m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.Description
  Else
    addSegnalazione line, "P00", line
    Resume Next
  End If

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''
' INCLUDE name | dataset(membro)
' Copia del PLI!!!! unificare?!
''
' ATTENZIONE:
' se il membro era stato riconosciuto viene riclassificato...
' -Segnalazione NOCOPY... SPECIFICARNE UNA NIN!!!!!!!!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getInclude(statement As String) As String
  Dim includeName As String
  
  includeName = nexttoken(statement)
  If InStr(includeName, ")") Then
    includeName = Trim(Replace(includeName, ")", "", 1))
  End If
  'SQ 6-12-06
  checkRelations includeName, "NAT-INCLUDE"
'  Set TbOggetti = m_Fun.Open_Recordset("select idOggetto,tipo from Bs_oggetti where tipo = 'NIN' and nome = '" & includeName & "'")
'  If TbOggetti.RecordCount = 0 Then
'    'Cerco fra in PGM (riconoscimento "quasi" errato)
'    Set TbOggetti = m_Fun.Open_Recordset("select idOggetto,tipo from Bs_oggetti where tipo = 'NAT' and nome = '" & includeName & "'")
'  End If
'
'  If TbOggetti.RecordCount Then
'    If TbOggetti!Tipo = "NAT" Then
'      TbOggetti!Tipo = "NIN"
'      TbOggetti.Update
'    End If
'    addRelazione TbOggetti!idOggetto, "COPY", includeName, includeName
'  Else
'    If Not ExistObjSyst(includeName, "CPY") Then
'       addSegnalazione includeName, "NOCOPY", includeName
'    End If
'  End If
'  TbOggetti.Close
End Function
Private Function getUSING(statement As String) As String
  Dim usingName As String
  
  usingName = nexttoken(statement)
  'If InStr(usingName, ")") Then
  '  usingName = Trim(Replace(usingName, ")", "", 1))
  'End If
  
  checkRelations usingName, "NAT-COPY"
End Function
'use: CALL | CALLNAT | NATFETCH
Private Sub getCALL(statement As String, use As String)
  Dim program As String, token As String
  ''''''''''''''''''''''''''''''
  ' Modulo Chiamato:
  ''''''''''''''''''''''''''''''
  SwNomeCall = Left(statement, 1) = "'"
  program = Trim(nexttoken(statement))  'SQ - Sbaglia: gli array si perdono l'indice! (Es: CHILDREN(I))
  If SwNomeCall Then program = "'" & program & "'"  'nextToken mangia gli "'"!
  statement = Replace(statement, ",", " ") 'argomenti separati da virgole...(meglio usare ciclo di nextToken_tmp...)
  statement = Replace(Replace(statement, ")", ""), "(", "") 'pulizia parentesi
  
  If False Then
    '''''''''''''''''''''''''''''''''''''''''''''''''
    ' Inserimento istruzione in PsCall: (trasformare in execute INSERT)
    '''''''''''''''''''''''''''''''''''''''''''''''''
    Dim rsCall As Recordset
    Set rsCall = m_Fun.Open_Recordset("select * from PsCall")
    rsCall.AddNew
    rsCall!idOggetto = GbIdOggetto
    rsCall!Riga = GbOldNumRec
    rsCall!programma = program
    rsCall!parametri = statement
    rsCall!linee = GbNumRec - GbOldNumRec + 1
    rsCall.Update
    rsCall.Close
  End If
  
  ''''''''''''''''''''''''''''''''''''''
  ' Relazioni/Segnalazioni
  ' (se non BUILTIN)
  ''''''''''''''''''''''''''''''''''''''
  Dim i As Integer
  
  MapsdM_NATURAL.checkRelations program, use
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Relazione programma/PSB
  ' (riconosce le istruzioni e le inserisce nella PsDli_Istruzioni
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If isDliCall Then 'settato dalla checkRelation
    'Preparo i parametri come arrivassero dal COBOL
    token = nexttoken(statement)  'COUNT
    MapsdM_IMS.checkPSB statement
  End If
  
  Exit Sub
callErr:
  'gestire...
  If err.Number = -2147217887 Then  'chiave duplicata...
    '2 CALL su una linea...
  Else
    err.Raise 124, "getCall", "syntax error on " & statement
    Resume
  End If

End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''
' Si possono eseguire programmi "Main" NATURAL
' con FETCH RETURN <nomePgm>
' Posso avere FETCH REPEAT (ma non so cosa faccia!)
'''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getFETCH(statement As String)
  Dim token As String
  
  On Error GoTo catch
  
  token = nextToken_new(statement)
  If token = "RETURN" Then
    getCALL statement, "NATFETCH"
  ElseIf token = "REPEAT" Then
    m_Fun.writeLog "idOggetto: " & GbIdOggetto & " FETCH REPEAT: " & token & statement & " ???!!!"
  Else
    'Default RETURN?
    getCALL token & " " & statement, "NATFETCH"
  End If
  
  Exit Sub
catch:
  'gestire...
  m_Fun.writeLog "##getFETCH-idOggetto: " & GbIdOggetto & "-" & err.Description
End Sub

'''''''''''''''''''''''''''''
' Provvisorio: gestione solo casi semplici
'''''''''''''''''''''''''''''
Private Sub getMove(statement As String)
  Dim lValue As String
  Dim rValue As String
  
  lValue = nexttoken(statement)
  rValue = nexttoken(statement) 'TO
  rValue = nexttoken(statement)
  getPliMove lValue, rValue, CampiMove  'SQ: Per il PLI!! se generico, renderlo tale!
  
End Sub
'TMP: � quello del Parser... da modificare ESTERNALIZZANDO!
Public Sub checkRelations(item As String, relType As String)
  Dim rs As Recordset
  Dim itemType As String, itemTypes As String, wrongTypes As String
  
  isDliCall = False
  
  Select Case relType
    Case "CALL"
      itemTypes = "'CBL','ASM','PLI','QIM'" 'EZT?!
      wrongTypes = "'CPY','INC','UNK'"
      itemType = GbTipoInPars '� sempre corretto? Allora usarlo in tutti i casi!!
      '(item: programma chiamato)
      'item puo' essere nome variabile...
      If SwNomeCall Then
        itemValue = Replace(item, "'", "")
      Else
        'Ottimizzazione: diamo per buone le seguenti variabili:
        ' Mauro 08/10/2009: Aggiunta SX000M0 per GRZ
        If item = "CBLTDLI" Or item = "AIBTDLI" Or item = "PLITDLI" Or _
           item = "PLIHSSR" Or item = "SX000M0" Or item = "DBINTDLI" Then
          itemValue = item
        Else
          ''''''''''''''''''''
          'SQ 19-12-06
          ' Prova solo per NATURAL... decidere se mettere in linea...
          ''''''''''''''''''''
          If GbTipoInPars = "NAT" Or GbTipoInPars = "NIN" Then
            Dim itemValues() As String
            'Non segue le "dependencies"...
            itemValues = getFieldValues(item)
            If UBound(itemValues) Then
              itemValue = Trim(Replace(itemValues(1), "'", "")) 'TMP: Teniamo solo il primo
            Else
              itemValue = ""
            End If
          Else
            itemValue = getFieldValue(item)
          End If
          If Len(itemValue) = 0 Then
            addSegnalazione item, "NO" & relType & "VALUE", item '"NOCALL": dinamico, per eventulmente accorpare questa routine...
            Exit Sub
          End If
        End If
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''
      ' Gestione "ALIAS" per prg d'interfaccia ad IMS:
      ' Es. Sungard: "INMCMIO"... prg centralizzato per
      ' invio mappe... i parametri sono gli stessi...
      ' SQ - ... implementare! Per ora scamuffo!
      '''''''''''''''''''''''''''''''''''''''''''''''''
      If itemValue = "INMCMIO" Then
        SwDli = True        'programma DLI
        isDliCall = True    'istruzione DLI: serve al chiamante (e basta)
      End If
      
      Select Case itemValue
        ' Mauro 08/10/2009: Aggiunta SX000M0 per GRZ
        Case "CBLTDLI", "AIBTDLI", "PLITDLI", "PLIHSSR", "SX000M0", "DBINTDLI"
          SwDli = True        'programma DLI
          isDliCall = True    'istruzione DLI: serve al chiamante (e basta)
        Case "ISPLINK"
          'Flag ISPF!
          '...
        Case Else
          ''''''''''''''''''''''''''''''''''
          ' Gestione PsLoad
          ''''''''''''''''''''''''''''''''''
          Set rs = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where nome = '" & itemValue & "' and tipo in (" & itemTypes & ")")
          If rs.RecordCount = 0 Then
            'Nome LOAD diverso da nome SOURCE: aggiorniamo il recordset
            Set rs = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.load = '" & itemValue & "' and b.source=a.nome and a.tipo in (" & itemTypes & ")")
          End If
          'RICLASSIFICAZIONE
          If rs.RecordCount = 0 Then
            Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ")")
            If rs.RecordCount Then
              rs!Tipo = itemType
              rs.Update
            End If
          End If
          If rs.RecordCount Then
            If rs.RecordCount = 1 Then
              addRelazione rs!idOggetto, relType, itemValue, itemValue
            Else
              'verificare 'sta storia...
              While Not rs.EOF
                'Inserisce le relazioni come duplicate nella tabella
                addRelazione rs!idOggetto, relType, itemValue, itemValue, True
                rs.MoveNext
              Wend
            End If
          Else
             If Not ExistObjSyst(itemValue, "PGM") Then
                addSegnalazione itemValue, "NO" & relType, itemValue
             End If
          End If
          rs.Close
      End Select
    Case "CALLNAT", "NATFETCH", "NATBATCH"
      itemType = GbTipoInPars '� sempre corretto? Allora usarlo in tutti i casi!!
      itemTypes = "'NAT'"
      wrongTypes = "'NIN','NCP','UNK'"
      IdOggettoRel = 0  'init
      '(item: programma chiamato)
      'item puo' essere nome variabile...
      If SwNomeCall Then
        itemValue = Replace(item, "'", "")
      Else
        'Non segue le "dependencies"...
        itemValues = getFieldValues(item)
        If UBound(itemValues) Then
          itemValue = Trim(Replace(itemValues(1), "'", "")) 'TMP: Teniamo solo il primo
        Else
          itemValue = ""
        End If
        If Len(itemValue) = 0 Then
          addSegnalazione item, "NO" & relType & "VALUE", item '"NOCALL": dinamico, per eventulmente accorpare questa routine...
          Exit Sub
        End If
      End If
      ''''''''''''''''''''''''''''''''''
      ' Gestione PsLoad
      ''''''''''''''''''''''''''''''''''
      Set rs = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where nome = '" & itemValue & "' and tipo in (" & itemTypes & ")")
      If rs.RecordCount = 0 Then
        'Nome LOAD diverso da nome SOURCE: aggiorniamo il recordset
        Set rs = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.load = '" & itemValue & "' and b.source=a.nome and a.tipo in (" & itemTypes & ")")
      End If
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ")")
        If rs.RecordCount Then
          rs!Tipo = itemType
          rs.Update
        End If
      End If
      If rs.RecordCount Then
        If rs.RecordCount = 1 Then
          addRelazione rs!idOggetto, relType, itemValue, itemValue
        Else
          'verificare 'sta storia...
          While Not rs.EOF
            'Inserisce le relazioni come duplicate nella tabella
            addRelazione rs!idOggetto, relType, itemValue, itemValue, True
            rs.MoveNext
          Wend
        End If
      Else
        If Not ExistObjSyst(itemValue, "PGM") Then
          addSegnalazione itemValue, "NO" & relType, itemValue
        End If
      End If
      rs.Close
    Case Else
      '...
  End Select
End Sub

'''''''
'silvia 24-04-2008
'''
Private Sub getDEFINE(statement As String, Optional token As String = "", Optional make As Boolean = False)
  Dim rs As Recordset
  Dim NatField As NatData
  Dim cblField As cblData
  Dim fieldName As String, strformat As String, strarray() As String
  Dim pos As Integer
  Dim i As Integer
  Dim livello As Integer
  Dim tmpbyte As Integer, tmplivello As Integer
  On Error GoTo catch
    
  NatField.isNaGroup = False
  NatField.NaLen = 0
  'level
  statement = token & " " & statement
  NatField.Nalevel = nexttoken(statement)
  'FieldName:
  fieldName = Replace(nexttoken(statement), "#", "")
  If fieldName = "REDEFINE" Then
    NatField.NaName = ""
    token = Replace(nexttoken(statement), "#", "")
    NatField.NaRedefines = token
    Line Input #nFCbl, statement
    GbNumRec = GbNumRec + 1
    token = nexttoken(statement)
    fieldName = Replace(nexttoken(statement), "#", "")
''  Else
''    NatField.NaName = fieldName
  End If
  ''
  NatField.NaName = fieldName
  If Len(statement) = 0 Then
      NatField.isNaGroup = True
      NatField.NaFieldtype = "A"
  Else
  ''If Len(statement) Then
    token = nexttoken(statement)
    If token = "VIEW" Or token = "" Or token = "/*" Then
      NatField.isNaGroup = True
      NatField.NaFieldtype = "A"
    Else
      token = Replace(token, "(", "")
      token = Trim(Replace(token, ")", ""))
      NatField.NaFieldtype = Mid(token, 1, 1)
      strformat = nexttoken(Mid(token, 2))
      If InStr(strformat, ".") = 0 And InStr(strformat, "/") = 0 Then
        If Len(strformat) = 0 Then
          NatField.Nabytes = 0
        Else
          NatField.Nabytes = strformat
        End If
      End If
     'gestione decimali
      If InStr(strformat, ".") Then
        NatField.Nabytes = Mid(strformat, 1, InStr(strformat, ".") - 1)
        NatField.NaDec = Mid(strformat, InStr(strformat, ".") + 1, IIf(InStr(strformat, "/"), InStr(strformat, "/") - InStr(strformat, ".") - 1, Len(strformat) - InStr(strformat, ".")))
      End If
      'gestione occurs
      If InStr(strformat, "/") Then
        NatField.Nabytes = Mid(strformat, 1, InStr(strformat, "/") - 1)
        strformat = Mid(strformat, InStr(strformat, "/") + 1)
        strarray = Split(strformat, ",")
        If UBound(strarray) > 0 Then
          NatField.NaOccurs = Mid(strarray(UBound(strarray)), InStr(strarray(UBound(strarray)), ":") + 1)
          tmpbyte = NatField.Nabytes
          tmplivello = NatField.Nalevel
          NatField.Nabytes = 0
          listIndex = listIndex + 1
          dataList(listIndex) = NatField
          NatField.NaRedefines = ""
          For i = UBound(strarray) - 1 To 0 Step -1
            NatField.Nalevel = tmplivello + 1
            NatField.NaOccurs = Mid(strarray(i), InStr(strarray(i), ":") + 1)
            If i = 0 Then
              NatField.Nabytes = tmpbyte
              NatField.NaLen = NatField.Nabytes
              NatField.NaName = "FILLER"
            Else
              NatField.Nabytes = 0
              NatField.NaLen = NatField.Nabytes
              NatField.NaName = "FILLER"
              listIndex = listIndex + 1
              dataList(listIndex) = NatField
            End If
          Next
        Else
          NatField.NaOccurs = Mid(strformat, InStr(strformat, ":") + 1)
        End If
      End If
      Select Case NatField.NaFieldtype
        Case "D"
          NatField.Nabytes = 4
          NatField.NaLen = 6
        Case "T"
          NatField.Nabytes = 7
          NatField.NaLen = 12
        Case "L"
          NatField.Nabytes = 1
          NatField.NaLen = NatField.Nabytes
        Case "P"
          NatField.NaLen = NatField.Nabytes
          NatField.Nabytes = Int(((NatField.NaLen + NatField.NaDec) / 2) + 1)
          NatField.NaSegno = "S"
        Case "B"
          Select Case NatField.Nabytes
            Case 1
              NatField.NaLen = 3
            Case 2
              NatField.NaLen = 5
            Case 3
              NatField.NaLen = 8
            Case 4
              NatField.NaLen = 10
          End Select
          NatField.NaSegno = "N"
        Case Else
          NatField.NaLen = NatField.Nabytes
          NatField.NaSegno = "N"
      End Select
    End If
    If token = "/*" Then
      statement = ""
    End If
    If Len(statement) Then
      token = nexttoken(statement)
      Select Case token
        Case "INIT", "COSTANT"
       '' token = nextToken(statement)
          If statement = "" Then
            While Len(statement) = 0 And Not EOF(nFCbl)
              Line Input #nFCbl, statement
              If Len(statement) Then
                If Left(statement, 1) = "*" Then
                  statement = ""
                End If
              End If
            Wend
          End If
          If InStr(statement, "<'") Then
            NatField.Navalue = Mid(Trim(statement), 3)
            NatField.Navalue = Mid(NatField.Navalue, 1, InStr(NatField.Navalue, "'>") - 1)
            If NatField.NaFieldtype = "L" Then
              If UCase(NatField.Navalue) = "TRUE" Then
                NatField.Navalue = 1
              Else
                NatField.Navalue = 0
              End If
            End If
          End If
          statement = ""
        Case Else
           'MsgBox "statement: " & statement
           '''m_Fun.WriteLog "statement: " & statement
      End Select
    End If
    token = nexttoken(statement)
    If token = "/*" Then
      statement = ""
    End If
    statement = ""
  End If
 
  listIndex = listIndex + 1
  dataList(listIndex) = NatField
 
  Exit Sub
catch:
  m_Fun.Show_MsgBoxError "RC0002", , "getDEFINE", err.source, err.Description, False
  Resume Next
End Sub

'silvia 28-04-2008
Private Function getCobolData(NaField As NatData) As cblData
  Dim cblField As cblData
  '
  cblField.Name = NaField.NaName
  '
  cblField.dataLen = NaField.NaLen
  cblField.dataDec = NaField.NaDec
  cblField.Segno = NaField.NaSegno
  cblField.Redefines = NaField.NaRedefines
  '
  cblField.dataType(0) = Switch(NaField.NaFieldtype = "A", "X", NaField.NaFieldtype = "N" Or NaField.NaFieldtype = "B" Or NaField.NaFieldtype = "P", "9", True, "X")
  cblField.dataType(1) = Switch(NaField.NaFieldtype = "B", "COMP", NaField.NaFieldtype = "P", "COMP-3", True, "")
  'cblField.dataType(1) = Switch(NaField.NaFieldtype = "P", "COMP-3", True, "")
  cblField.bytes = NaField.Nabytes
  cblField.isGroup = NaField.isNaGroup
  If NaField.isNaGroup Then
    cblField.dataLen = 0
    cblField.dataType(0) = "A"
  End If
  cblField.level = NaField.Nalevel
  getCobolData = cblField
End Function

Private Sub writeDataCmp(listCmp() As NatData, Index As Long)
Dim i As Long
Dim rsArea As Recordset
Dim rsCmp As Recordset
Dim cblField As cblData

  For i = 1 To Index - 1
    ' Se � un livelo 01, scrivo il record nella PsData_Area
    If CInt(listCmp(i).Nalevel) = "1" Then
      idArea = idArea + 1
      lOrdinale = 0
      Set rsArea = m_Fun.Open_Recordset("select * from PsData_Area where idoggetto = " & GbIdOggetto)
      rsArea.AddNew
      rsArea!idOggetto = GbIdOggetto
      rsArea!idArea = idArea
      rsArea!nome = listCmp(i).NaName
      rsArea!livello = listCmp(i).Nalevel
      
      rsArea.Update
      rsArea.Close
    End If
    ' Scrivo sempre il record nella PsData_Cmp
    Set rsCmp = m_Fun.Open_Recordset("select * from PsData_Cmp where idoggetto = " & GbIdOggetto)
    cblField = getCobolData(listCmp(i))
    rsCmp.AddNew
    
    rsCmp!idOggetto = GbIdOggetto
    rsCmp!idArea = idArea
    rsCmp!nome = cblField.Name
    lOrdinale = lOrdinale + 1
    rsCmp!Ordinale = lOrdinale
    rsCmp!codice = format(GbIdOggetto, "00000000") & "/" & format(idArea, "0000") & "/" & format(lOrdinale, "0000")
    rsCmp!livello = cblField.level
    rsCmp!Tipo = IIf(cblField.isGroup, "A", cblField.dataType(0))
    rsCmp!Posizione = listCmp(i).DatPos
    If IsNumeric(cblField.bytes) Then
      rsCmp!byte = cblField.bytes
    End If
    rsCmp!Occurs = listCmp(i).NaOccurs
    If IsNumeric(cblField.dataLen) Then
      rsCmp!Lunghezza = cblField.dataLen  '0 !!!!!!!!!!!!!!!
    End If
    If IsNumeric(cblField.dataDec) Then
      rsCmp!Decimali = cblField.dataDec  '0 !!!!!!!!!!!!!!!
    End If
''    If IsNumeric(cblField.dataLen) Then
''      rsCmp!Lunghezza = listCmp(i).NaLen   '0 !!!!!!!!!!!!!!!
''    End If
''    If IsNumeric(cblField.dataDec) Then
''      rsCmp!Decimali = listCmp(i).NaLen   '0 !!!!!!!!!!!!!!!
''    End If
    rsCmp!Segno = cblField.Segno
    rsCmp!Usage = Switch(cblField.dataType(1) = "COMP", "BNR", cblField.dataType(1) = "COMP-3", "PCK", True, "ZND")
    rsCmp!Valore = listCmp(i).Navalue
    rsCmp!nomeRed = listCmp(i).NaRedefines
    
    rsCmp.Update
    rsCmp.Close
  Next i
End Sub


