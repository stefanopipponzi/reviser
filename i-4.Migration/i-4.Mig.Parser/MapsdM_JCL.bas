Attribute VB_Name = "MapsdM_JCL"
Option Explicit
''''''''''''''''''
' GESTIONE VARIABILI:
' SET ...
' SQ - 18-10-06
''''''''''''''''''
Dim parameters As Collection
'spli
Dim GOModule As String
'Mauro 02/01/2008: Gestione PROC interne
Dim arrProc() As String
' Mauro 16/04/2010 : Contatore delle righe "effettive"
Dim LOCS As Long
Dim appoIdProc As Long

''''''''''''''''''''''''''''''''''''''''''''''''''''''
' NUOVA
' Gestione SYSIN
' Generalizzare ricerca PSB...
' Gestione IDCAMS
' !!!!Replicarla per i JOB
' 27-09-06: QUICKJOB => ESECUZIONE PROGRAMMI QUICKIMS
''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseJCL()
  Dim FD As Long, stepLines As Long
  Dim wRecIn As String, Name As String, statement As String, istruz As String, inputStream As String
  Dim k As Integer, rf_nStep As Integer
  Dim rs As Recordset, rsJCL As Recordset
  Dim rf_Step As String, rf_Program As String, rf_Parameters As String, rf_Utility As String
  Dim token As String, wDsnName As String, jclName As String, psbName As String
  Dim rsAlias As Recordset, rsObj As Recordset
  Dim SaveIdOggetto As Long
  
  On Error GoTo errJcl
  
  'init
  GbNumRec = 0
  LOCS = 0
  alreadyReadLine = ""
  ReDim arrProc(0)
  
  resetRepository
  
  '"Nested Parsing":
  If IdOggettoRel = GbIdOggetto Then
    'Oggetto "esterno"
    Set parameters = New Collection
    'Parametri settati dai chiamanti?!
    getCallerParameters GbIdOggetto ', GbTipoInPars
  Else
    'Oggetto "interno" (MEMBER):
    'Non devo resettare la Collection!
    GbIdOggetto = IdOggettoRel
  End If
  
  FD = FreeFile
  Open GbFileInput For Input As FD
  
  'spli
  GOModule = ""
  While Not EOF(FD) Or Len(Trim(alreadyReadLine)) > 0
    SwNomeCall = True
    'Ritorna intero statement (su unica linea), esclusi i "//"
    inputStream = getJclStatement(FD, alreadyReadLine)  'inputStream viene "mangiato" da nextToken...
    statement = inputStream
    token = nexttoken(statement)
    '5-12-06
    If Left(inputStream, 1) <> " " Then
      'LABEL
      Name = token
      istruz = nexttoken(statement)
    Else
      Select Case token
        Case "JOB", "PROC", "EXEC", "INCLUDE", "SET"
          istruz = token
          Name = ""
        Case "DD"
          istruz = "DD"
        Case "IF", "ENDIF"
          istruz = ""
          Name = token
        Case Else
          istruz = nexttoken(statement)
          Name = token
      End Select
    End If
    
    Select Case istruz
      Case "JOB", "PROC"
        'Attenzione...
        If Len(jclName) = 0 Then jclName = Name
        'tmp: solo PROC...
        If istruz = "PROC" Then
          getDefaultParameters statement, True
          ReDim Preserve arrProc(UBound(arrProc) + 1)
          arrProc(UBound(arrProc)) = token
        End If
      Case "EXEC"
        'Mauro 3-08-07
        'init:
        rf_Program = ""
        rf_Parameters = ""
        rf_Step = Name
        
        Set rsJCL = m_Fun.Open_Recordset("select * from PsJCL where " & _
                                         "idoggetto = " & GbIdOggetto & " and stepnumber = " & rf_nStep)
        If Not rsJCL.EOF Then
          rsJCL!linee = GbOldNumRec - stepLines
          rsJCL.Update
        End If
        rsJCL.Close
        
        stepLines = GbOldNumRec
        rf_nStep = rf_nStep + 1
        ''''''''''''''''''''''''''''''''''''
        ' GESTIONE EXEC
        ' -Valorizza rf_Step e rf_Program
        ''''''''''''''''''''''''''''''''''''
        parseEXEC_PGM statement, jclName, rf_Step, rf_Program, rf_Parameters
        ' UTILITY APPBUILDER
        If rf_Program = "HPSARCTL" Then
          rf_Utility = rf_Program
          ' ZZZZPARM
          token = nextToken_JCL(statement)
          Do Until token = "ZZZZPARM="
            If Left(statement, 1) = "," Then
              statement = Mid(statement, 2)
            End If
            token = nextToken_JCL(statement)
          Loop
          rf_Program = nextToken_JCL(statement)
          If Len(rf_Program) Then
            If InStr(rf_Program, "/") Then
              Dim Slash As Integer, Virgola As Integer
              Slash = InStr(rf_Program, "/")
              Virgola = InStr(Slash, rf_Program & ",", ",")
              rf_Program = Mid(rf_Program, Slash + 1, Virgola - Slash - 1)
            End If
            rf_Program = Replace(rf_Program, "'", "")
            updateFileRelations rf_Program, rf_nStep, rf_Utility
          End If
          If Len(statement) Then
            If Left(statement, 1) = "," Then rf_Parameters = Mid(statement, 2)
            statement = rf_Parameters
          Else
            rf_Parameters = ""
          End If
        End If
        ' UTILITY APPBUILDER
        If rf_Program = "HPSBATCH" Then
          rf_Utility = rf_Program
          ' ZZZZPARM
          token = nextToken_JCL(statement)
          rf_Program = nexttoken(statement)
          If Len(rf_Program) Then
            If InStr(rf_Program, ",") Then
              rf_Program = Left(rf_Program, InStr(rf_Program, ",") - 1)
            End If
            rf_Program = Replace(rf_Program, "'", "")
            If InStr(rf_Program, "&") Then
              Dim pgmName() As String, a As Long
              pgmName = getParameters(rf_Program)
              For a = 0 To UBound(pgmName)
                updateFileRelations pgmName(a), rf_nStep, rf_Utility
              Next a
            Else
              updateFileRelations rf_Program, rf_nStep, rf_Utility
            End If
          End If
          If Len(statement) Then
            If Left(statement, 1) = "," Then rf_Parameters = Mid(statement, 2)
            statement = rf_Parameters
          End If
        End If
        If rf_Program = "IKJEFT01" And Len(Trim(statement)) > 0 Then
          rf_Utility = rf_Program
          rf_Program = parseIKJEFT01(FD, statement)
          If Len(rf_Program) Then
            updateFileRelations rf_Program, rf_nStep, rf_Utility
          Else
            rf_Program = "IKJEFT01"
          End If
        End If
        If rf_Program = "NAT232BA" Or rf_Program = "NAT217BA" Then
          rf_Utility = rf_Program
        End If
        '''''''''''''''''''''''''''''''''
        ' SCHEDULAZIONE PSB
        '''''''''''''''''''''''''''''''''
        'SQ: verificare(portato sotto: checkSchedPSB mi cambia il rf_Program...)
        'psbName = checkSchedPSB(rf_Program, statement, rf_nStep)
        
        '''''''''''''''''''''''''''''''''
        ' SQ Madrid
        ' TABELLA PsJCL
        '''''''''''''''''''''''''''''''''
        'manca execType... di ritorno dal parseEXEC_PGM...
        Parser.PsConnection.Execute "INSERT INTO PsJCL (idOggetto,stepNumber,stepName,execName,[parameters]) " & _
                                     "VALUES (" & GbIdOggetto & "," & rf_nStep & ",'" & rf_Step & "','" & rf_Program & "','" & Replace(rf_Parameters, "'", "''") & "')"
        psbName = checkSchedPSB(rf_Program, statement, rf_nStep)
      Case "DD"
        ''''''''''''''''''''''''''''''''''''
        ' GESTIONE DATASET
        ''''''''''''''''''''''''''''''''''''
        If statement = "*" Then
          ''''''''''''''''''''''''
          ' INSTREAM:
          ''''''''''''''''''''''''
          SaveIdOggetto = GbIdOggetto
          ' Mauro 14/01/2011 : Ma che tocca f� per camp�?!?!
          If InStr(Parser.PsNomeDB, "TSystems") Then
            Set rsAlias = m_Fun.Open_Recordset("select * from alias_IKJEFT01 where nome = '" & rf_Program & "'")
            If rsAlias.RecordCount Then
              Set rsObj = m_Fun.Open_Recordset("select idoggetto from bs_oggetti where " & _
                                               "nome = '" & rf_Program & "' and tipo = 'PRC'")
              appoIdProc = rsObj!idOggetto
              rsObj.Close
              rf_Program = "IKJEFT01"
            End If
            rsAlias.Close
          End If
          
          Select Case rf_Program
            Case "IDCAMS"
              'modificare... tenuto il vecchio... solo rinominato
              parseIDCAMS FD
            Case "IKJEFT01", "IKJEFT1B", "TSOBATCH", "HPSVRUN1"
              'L'input che ci interessa e' in SYSTSIN:
              If Name = "SYSTSIN" Then
                'SQ - 21-03-06
                'Update rf_Program: per le relazioni file...
                'parseIKJEFT01 FD
                rf_Utility = rf_Program
                rf_Program = parseIKJEFT01(FD)
                If Len(rf_Program) Then
                  updateFileRelations rf_Program, rf_nStep, rf_Utility
                End If
              Else
                'l'INSTREAM...
                While Left(wRecIn, 2) <> "//" And Left(wRecIn, 2) <> "/*" And Left(wRecIn, 3) <> "//*" And Not EOF(FD) 'ATTENZIONE: PUO' ESSERE ANCHE: DD DATA,DLM=AA
                  Line Input #FD, wRecIn
                  CalcolaLOCSJcl wRecIn
                  GbNumRec = GbNumRec + 1
                  If Left(wRecIn, 2) = "//" Then
                    'statement nuovo
                    alreadyReadLine = wRecIn
                  End If
                Wend
                wRecIn = ""
              End If
              ' Mauro 14/01/2011 : Ma che tocca f� per camp�?!?!
              If InStr(Parser.PsNomeDB, "TSystems") Then
                GbIdOggetto = SaveIdOggetto
              End If
'''            Case "IKJEFT1B"
'''              'L'input che ci interessa e' in SYSTIN:
'''              If Name = "SYSTSIN" Then
'''                'SQ - 21-03-06
'''                rf_Program = parseIKJEFT1B(FD)
'''                If Len(rf_Program) Then
'''                  updateFileRelations rf_Program, rf_nStep, "IKJEFT1B"
'''                End If
'''              Else
'''                'l'INSTREAM...
'''                While Left(wRecIn, 2) <> "//" And Left(wRecIn, 2) <> "/*" And Left(wRecIn, 3) <> "//*" And Not EOF(FD) 'ATTENZIONE: PUO' ESSERE ANCHE: DD DATA,DLM=AA
'''                  Line Input #FD, wRecIn
'''                  GbNumRec = GbNumRec + 1
'''                  If Left(wRecIn, 2) = "//" Then
'''                    'statement nuovo
'''                    alreadyReadLine = wRecIn
'''                  End If
'''                Wend
'''                wRecIn = ""
'''              End If
            Case "NAT232BA", "NAT217BA"
              'L'input che ci interessa e' in CMSYNIN:
              If Name = "CMSYNIN" Then
                'SQ - 21-03-06
                'Update rf_Program: per le relazioni file...
                'parseIKJEFT01 FD
                rf_Utility = rf_Program
                rf_Program = parseNATBATCH(FD)
                If Len(rf_Program) Then
                  updateFileRelations rf_Program, rf_nStep, rf_Utility
                End If
              Else
                'l'INSTREAM...
                While Left(wRecIn, 2) <> "//" And Left(wRecIn, 2) <> "/*" And Left(wRecIn, 3) <> "//*" And Not EOF(FD) 'ATTENZIONE: PUO' ESSERE ANCHE: DD DATA,DLM=AA
                  Line Input #FD, wRecIn
                  CalcolaLOCSJcl wRecIn
                  GbNumRec = GbNumRec + 1
                  If Left(wRecIn, 2) = "//" Then
                    'statement nuovo
                    alreadyReadLine = wRecIn
                  End If
                Wend
                wRecIn = ""
              End If
              
            Case "DSNMTV01"
              'SQ 24-10-07 attenzione! Se ho il passaggio JCL->PROC, con il DSNMTV01 passato fra i parametri della PROC...
              ' ho DD <nomeStepProc>.DDITV02! e NON FUNZIA!
              If Name = "DDITV02" Or Right(Name, 8) = ".DDITV02" Then
                'Lettura INSTREAM (fare funzione comune!)
                While Left(wRecIn, 2) <> "//" And Left(wRecIn, 2) <> "/*" And Left(wRecIn, 3) <> "//*" And Not EOF(FD) 'ATTENZIONE: PUO' ESSERE ANCHE: DD DATA,DLM=AA
                  Line Input #FD, wRecIn
                  CalcolaLOCSJcl wRecIn
                  GbNumRec = GbNumRec + 1
                  If Left(wRecIn, 2) = "//" Then
                    'statement nuovo
                    alreadyReadLine = wRecIn
                  ElseIf Left(wRecIn, 2) <> "/*" And Left(wRecIn, 3) <> "//*" Then
                    statement = statement & " " & wRecIn
                  End If
                Wend
                parseDSNMTV01 statement, psbName, rf_nStep
                wRecIn = ""
              End If
            ''''''''''''''''''''''
            ' QUICKIMS
            ' SQ - 27-09-06
            ''''''''''''''''''''''
            Case "QUIKJOB"
              If Name = "SYSYIN" Then
                'tmp
                MsgBox "QUIKJOB"
                'rf_Program = parseQUICKJOB(FD)
              End If
            Case Else
              ''''''''''''''''''''''''
              ' INSTREAM:
              '''''''''''''''''''''''''
              
              '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
              ''
              'SQ - 4-11-2010 tmp
              ''
              ' Estrapolazione schede significative -> PGM (SAS,EZT,etc...)
              '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
              Dim newMember As String
              Dim FDnew As Long
              If Name = "SYSIN" Then
                If rf_Program = "SASESA" Or rf_Program = "SAS" Or rf_Program = "SASPIU" Or rf_Program = "SAS609" _
                                          Or rf_Program = "Y6SAS" Or rf_Program = "SASXA1" Then
                  On Error Resume Next
                  m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\sysin\" & rf_Program
                  On Error GoTo 0
                  newMember = m_Fun.FnPathPrj & "\Output-prj\sysin\" & rf_Program & "\" & GbNomePgm & "_" & rf_Step
                  
                  FDnew = FreeFile
                  Open newMember For Output As FDnew
                  On Error GoTo 0
                End If
              End If
              While Left(wRecIn, 2) <> "//" And Left(wRecIn, 2) <> "/*" And Left(wRecIn, 3) <> "//*" And Not EOF(FD) 'ATTENZIONE: PUO' ESSERE ANCHE: DD DATA,DLM=AA
                Line Input #FD, wRecIn
                
                'SQ
                On Error Resume Next
                If (FDnew) Then
                  Print #FDnew, wRecIn
                End If
                On Error GoTo 0
                
                CalcolaLOCSJcl wRecIn
                GbNumRec = GbNumRec + 1
                If Left(wRecIn, 2) = "//" Then
                  'statement nuovo
                  alreadyReadLine = wRecIn
                End If
              Wend
              wRecIn = ""
              
              'SQ
              On Error Resume Next
              If (FDnew) Then
                Close #FDnew
              End If
              On Error GoTo 0
          End Select
        Else
          ''''''''''''''''''''''''''''''''
          ' SKEDA su file
          ''''''''''''''''''''''''''''''''
          Select Case Name
            Case "SYSIN", "SYSTSIN", "SYSIPT"
              ''''''''''''''''''''''
              ' QUICKIMS/EASYTRIEVE
              ' SQ - 27-09-06
              ''''''''''''''''''''''
              If rf_Program = "QUIKJOB" Or rf_Program = "QUIKIMS" Or rf_Program = "EZTPA00" Or rf_Program = "SASESA" Or rf_Program = "SAS" Or rf_Program = "SASPIU" _
                                        Or rf_Program = "SAS609" Or rf_Program = "Y6SAS" Or rf_Program = "SASXA1" Then     '<-- SASESA (TILVIA)
                'MG 04/09/2007 : Se pi� SYSIN nello stesso STEP, rf_Program viene sovrascritto
                ''rf_Program = parseSYSIN_PGM(statement, psbName, rf_nStep, rf_Program)
                parseSYSIN_PGM statement, psbName, rf_nStep, rf_Program
                If rf_Program = "SASESA" Or rf_Program = "SAS" Or rf_Program = "SASPIU" Then ' <-- SASESA (TILVIA)
                  parseJclSked statement, Name, rf_Step, rf_nStep, rf_Program
                End If
                'If Len(rf_Program) Then
                '  updateFileRelations rf_Program, rf_nStep, "QUICKJOB"
                'End If
              Else
                ''''''''''''''''''''''''
                ' SCHEDA PARAMETRO:
                ''''''''''''''''''''''''
                Select Case statement
                  Case "DUMMY"
                    'Non ho la scheda
                  Case Else
                    '''''''''''''''''''''''''''''''''''''''''''''''
                    ' Controllo la presenza della scheda parametro:
                    '''''''''''''''''''''''''''''''''''''''''''''''
                    parseJclSked statement, Name, rf_Step, rf_nStep, rf_Program
                End Select
              End If
            Case "DDITV02"
              If rf_Program = "DSNMTV01" Then
                Dim skeda As String
                ''''''''''''''''''''''''''''''''''''''''''''
                ' Lettura/gestione skeda parametro generica:
                ' usarla negli altri casi!
                ''''''''''''''''''''''''''''''''''''''''''''
                skeda = getSKparam(statement, True)
                If Len(skeda) Then  'tutto OK
                  parseDSNMTV01 skeda, psbName, rf_nStep
                End If
              End If
            Case "STEPLIB", "JOBLIB", "PROCLIB" 'SQ 3-03-06
              'nop
'spli
            Case "SYSLIN"
            ' per ora escludo casi di pi� di un GO nella stesso file
            ' fa schifo ma non � colpa mia, a volte arriva con PGM= a volte senza
              If (rf_Program = "IEWL" Or rf_Program = "PGM=IEWL") And GOModule = "" Then
                If InStr(statement, "(") > 0 Then
                  GOModule = Mid(statement, InStr(statement, "(") + 1, InStr(statement, ")") - InStr(statement, "(") - 1)
                End If
              End If
              If (rf_Program = "DFSILNK0" Or rf_Program = "DFSILNK0") And GOModule = "" Then
                If InStr(statement, "(") > 0 Then
                  GOModule = Mid(statement, InStr(statement, "(") + 1, InStr(statement, ")") - InStr(statement, "(") - 1)
                  If GOModule = "PLITDLI" Or GOModule = "CBLTDLI" Or GOModule = "DBINTDLI" Then GOModule = ""
                End If
              End If
            Case Else
              ' Mauro 23/01/2009 : TMP!!!!! : SYSUT1, SYSUT2, etc...
              'If Left(Name, 3) = "SYS" Or (Name = "INPUT" And rf_Program = "FTP") Then
'              If (Name = "INPUT" And rf_Program = "FTP") Or _
'                 rf_Program = "NATBATCH" Or _
'                 (Name = "SYSUT1" And rf_Program = "ICEGENER") Then
                If InStr(SelezionaParam(statement, "DSN="), "(") > 0 And _
                   InStr(SelezionaParam(statement, "DSN="), "(+1)") = 0 And _
                   InStr(SelezionaParam(statement, "DSN="), "(+0)") = 0 And _
                   InStr(SelezionaParam(statement, "DSN="), "(0)") = 0 And _
                   InStr(SelezionaParam(statement, "DSN="), "(+2)") = 0 Then
                  ''''''''''''''''''''''
                  ' QUICKIMS/EASYTRIEVE
                  ' SQ - 27-09-06
                  ''''''''''''''''''''''
                  If rf_Program = "QUIKJOB" Or rf_Program = "QUIKIMS" Or rf_Program = "EZTPA00" Or rf_Program = "SASESA" _
                                            Or rf_Program = "SAS609" Or rf_Program = "Y6SAS" Or rf_Program = "SASXA1" Then  '<--- SASESA (TILVIA)
                    'MG 04/09/2007 : Se pi� SYSIN nello stesso STEP, rf_Program viene sovrascritto
                    ''rf_Program = parseSYSIN_PGM(statement, psbName, rf_nStep, rf_Program)
                    parseSYSIN_PGM statement, psbName, rf_nStep, rf_Program
                    'If Len(rf_Program) Th
                    '  updateFileRelations rf_Program, rf_nStep, "QUICKJOB"
                    'End If
                  Else
                    ''''''''''''''''''''''''
                    ' SCHEDA PARAMETRO:
                    ''''''''''''''''''''''''
                    Select Case statement
                      Case "DUMMY"
                        'Non ho la scheda
                      Case Else
                        '''''''''''''''''''''''''''''''''''''''''''''''
                        ' Controllo la presenza della scheda parametro:
                        '''''''''''''''''''''''''''''''''''''''''''''''
                        parseJclSked statement, Name, rf_Step, rf_nStep, rf_Program
                    End Select
                  End If
                Else
                  parseJclDD statement, Name, rf_Step, rf_nStep, rf_Program
                End If
'              Else
'                parseJclDD statement, Name, rf_Step, rf_nStep, rf_Program
'              End If
          End Select
        End If
      Case "INCLUDE"
        getInclude statement
''        'SCATTA LA RELAZIONE...
''        Name = nextToken(statement)
''        If Name = "MEMBER=" Then
''          'Es: INCLUDE MEMBER=(CICCIO)
''          Name = nextToken(statement)
''          Name = Trim(Replace(Replace(Name, "(", ""), ")", ""))
''        Else
''          'Es: INCLUDE MEMBER=CICCIO
''          Name = Mid(Name, InStr(Name, "=") + 1)
''        End If
''        Set rs = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where tipo = 'MBR' and nome = '" & Name & "'")
''        If rs.RecordCount Then
''          AggiungiRelazione rs!idOggetto, "INCLUDE_JCL", Name, Name 'utili i due parametri sempre uguali!...
''        Else
''          'If Not ExistObjSyst(name, "MEMBER") Then 'TABELLA ELEMENTI AGGIUNTI... DIREI CHE NON SERVE PER I MEBRI...
''           addSegnalazione Name, "NOMEMBER", Name  'IL TERZO PARAMETRO COS'E'?????????
''          'Else
''          ' AggiungiComponente jclName, jclName, "PRC", "SYSTEM"
''          'End If
''        End If
''        rs.Close
      Case "SET"
        parseSET statement
      Case Else
        '...
    End Select
  Wend
  Close #FD
   
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  TbOggetti!LOCS = LOCS
  TbOggetti!DtParsing = Now
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti!parsinglevel = 1
  TbOggetti.Update
  Exit Sub
errJcl:
  If err.Number = 123 Then 'Errore gestito: syntax error on nextToken
    'Probabilmente ho una istruzione su piu' righe: ...
    'Resume
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "parseJCL: id#" & GbIdOggetto & ", ###" & err.description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
    m_Fun.writeLog "parseJCL: id#" & GbIdOggetto & ", ###" & err.description
  Else
    GbOldNumRec = GbNumRec
    addSegnalazione statement, "P00", statement
    Resume Next
  End If
  Exit Sub
errPsJCL:
  'tmp: OK non ho la tabella... vecchie versioni
  Resume Next
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Gestione DATASET:
' - Inserimento in PsRel_File
' - Aggiornamento DMVSM_Archivio/DMVSM_Tracciati;
' - Inserimento in DMVSM_Tracciato, se conosco gia' il tracciato associato
'   (PGM gia' parserato)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseJclDD(statement As String, ddname As String, stepName As String, stepNumber As Integer, pgmName As String)
  Dim wStr1 As String, disp As String, dsn As String
  Dim i As Integer, a As Integer
  'Dim idArchivio As Long
  Dim VSAMName As String
  Dim rs As Recordset, rsTracciati As Recordset
  ' Mauro 06/08/2007 : cambio i flag con un campo Type
  'Dim isGDG As Boolean, isSEQ As Boolean
  Dim isGDG As Boolean
  Dim wFileType As String
  Dim rsVSAM As Recordset
  Dim parwStr1() As String
  Dim parDsn() As String
  Dim nFile As Integer
  Dim Ordinale As Integer
  Dim rsTracciato As Recordset
  
  On Error GoTo errdb

  disp = SelezionaParam(statement, "DISP=")
  dsn = SelezionaParam(statement, "DSN=")
  If Len(dsn) = 0 Then
    '''''''''''''''''''''''''''''''''
    'SQ 21-03-06
    ' Buttiamo via le SYSOUT!
    ''''''''''''''''''''''''''''''''
    wStr1 = SelezionaParam(statement, "SYSOUT=")
    If Len(wStr1) Then
      'dsn = Replace(wStr1, ",", "")
      Exit Sub
    Else
      'SQ - METTERE A POSTO TUTTO 'STO ROBO!!!
      'If InStr(statement, " DUMMY") > 0 Then wStr1 = "DUMMY"
      If InStr(statement, "DUMMY") > 0 Then wStr1 = "DUMMY"
      If InStr(statement, " * ") > 0 Then wStr1 = "*"
      If InStr(statement, "UNIT=") > 0 Then wStr1 = SelezionaParam(statement, "UNIT=")
      If Len(wStr1) Then
        dsn = wStr1
      Else
        'SQ tmp: K = InStr(statement, " ")
        'SQ tmp: wStr1 = Mid$(statement, 1, K - 1)
    '    addSegnalazione Trim(wStr1), "NODD", " "
        Exit Sub
      End If
    End If
  End If
  
  'SQ: e' il ddName? verificare...!
  Select Case dsn
    Case "*", "DUMMY", "SORTWK", "STEPLIB", "JOBLIB", "TASKLIB", "SYSDUMP", "SYSUDUMP"
      'nop
    Case Else
      ' Mauro 07/08/2007 : Il controllo sul parametro lo faccio dopo per risolverlo
      'If Left(dsn, 1) <> "&" And InStr(ddname, "SORTWK") = 0 Then 'InStr(dsn, ".")?
      If InStr(ddname, "SORTWK") = 0 Then 'InStr(dsn, ".")?
        ''''''''''''''''''''''''''''''''''''''''''''
        ' INSERIMENTO PsRel_file
        ''''''''''''''''''''''''''''''''''''''''''''
        i = InStr(dsn, "(")
        If i > 0 Then
          wStr1 = Mid(dsn, i + 1)
          If Trim(wStr1) = "" Then
            addSegnalazione statement & "", "P00", statement & ""
          Else
            wStr1 = Left(wStr1, InStr(wStr1, ")") - 1)
            '''''''''''''''''''''''''''''''''''''''''''''
            ' Mauro 07/08/2007 : controllo se il dsn � un parametro e in caso, lo risolvo
            If InStr(wStr1, "&") Then
              parwStr1 = getParameters(wStr1)
              If Len(parwStr1(0)) = 0 Then
                ' Mauro 07/08/2007 : Per ora niente segnalazione, poi vedremo
                'addSegnalazione dsn, "NOSKVALUE", dsn
                'Exit Sub
              Else
                wStr1 = parwStr1(0)
              End If
            End If
            ''''''''''''''''''''''''''''''''''
            If IsNumeric(wStr1) Then
              'GDG
              isGDG = True
              ' Mauro 06/08/2007 : cambio i flag con un campo Type
              'isSEQ = True
              wFileType = "Seq"
              dsn = Left(dsn, i - 1)
            Else
              'MEMBRO
              dsn = wStr1
            End If
          End If
        End If
        dsn = Replace(dsn, ",", "") '???????
        ' Mauro 07/08/2007 : controllo se il dsn � un parametro e in caso, lo risolvo
        If InStr(dsn, "&") Then
          parDsn = getParameters(dsn)
          If Len(parDsn(0)) = 0 Then
            parDsn(0) = dsn
            ' Mauro 07/08/2007 : Per ora niente segnalazione, poi vedremo
            'addSegnalazione dsn, "NOSKVALUE", dsn
            'Exit Sub
          End If
          For a = 0 To UBound(parDsn)
            Set rs = m_Fun.Open_Recordset("select * from PsRel_file where idoggetto = " & GbIdOggetto)
            If rs.RecordCount Then
              rs.MoveLast 'mi serve l'ultimo progressivo... (nFile)
              nFile = rs!nFile + 1
            Else
              nFile = 1
            End If
            rs.AddNew
            rs!idOggetto = GbIdOggetto
            rs!nFile = nFile
            rs!Type = "JCL"
            rs!nstep = stepNumber
            rs!Step = stepName
            rs!program = pgmName
            rs!dsnName = parDsn(a)
            rs!LINK = ddname
            rs!use = disp
            rs!Update = False 'era parametrico (sempre a false!)
            rs.Update
            rs.Close
            
            ''''''''''''''''''''''''''''''''''''''''''''
            ' AGGIORNAMENTO DMVSM_Archivio
            ''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''Aggiorna_Archivi GbIdOggetto, "JCL", stepNumber, nFile, statement
            'SQ 20-11-06 (portato sopra)
            'i = InStr(dsn, "(") 'GDG
            'If i Then
            '  dsn = Left(dsn, i - 1)
            '  isGDG = True
            '  isSEQ = True
            'End If
            Set rs = m_Fun.Open_Recordset("select * from DMVSM_Archivio where nome = '" & Replace(parDsn(a), "'", "") & "'")
            If rs.RecordCount = 0 Then
              rs.AddNew
              'rs!data_creazione = Now
              rs!idorigine = GbIdOggetto
              rs!Tag = "Generated by JCL"
              rs!nome = parDsn(a) 'serve?
              rs!VSam = "VSM_" & rs!idArchivio
              ' Mauro 06/08/2007 : cambio i flag con un campo Type
              'rs!Seq = isSEQ
              rs!Type = wFileType
              rs!gdg = isGDG
              rs!recform = "U"
              rs!recsize = 0
              rs!MinLen = 0
              rs!MaxLen = 0
              rs!KeyStart = 0
              rs!KeyLen = 0
              rs!data_creazione = Now
              Set rsVSAM = m_Fun.Open_Recordset("select * from DMVSM_NomiVSAM where NomeVSAM = '" & rs!VSam & "'")
              If rsVSAM.RecordCount = 0 Then
                rsVSAM.AddNew
                rsVSAM!NomeVSAM = rs!VSam & ""
                rsVSAM!Acronimo = rs!VSam & ""
                rsVSAM!to_migrate = True
                rsVSAM!obsolete = False
                rsVSAM.Update
              End If
              rsVSAM.Close
            Else
              'rs!data_modifica = Now
              ' Mauro 06/08/2007 : cambio i flag con un campo Type
              'rs!Seq = isSEQ
              If Len(wFileType) Then
                rs!Type = wFileType
              End If
              rs!gdg = isGDG
            End If
            ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
            'idArchivio = rs!idArchivio 'contatore automatico...
            VSAMName = rs!VSam & ""
            rs.Update
            rs.Close
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' AGGIORNAMENTO DMVSM_Tracciato
            ' (attenzione... passo di qui anche con le utility: non e' il massimo...
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Set rs = m_Fun.Open_Recordset("select IdOggetto,IdArea from PsRel_Tracciati where " & _
                                          "Program = '" & pgmName & "' and DDname = '" & ddname & "'")
            If rs.RecordCount Then
              'PGM gia' parserato: info tracciato gia' presenti...
              For i = 1 To rs.RecordCount
                'Parser.PsConnection.Execute "INSERT INTO DMVSM_Tracciato " & _
                '                              "VALUES(" & idArchivio & "," & i & "," & rs!idOggetto & "," & rs!idArea & ")"
                '''''''''''''''''''''''
                ' 23-02-06: c'era il problema di chiavi duplicate...
                ' A cosa serve l'Ordinale? vedere se sarebbe OK la chiave idArchivio|IdOggetto|idArea
                '''''''''''''''''''''''
                ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
                'Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " and idoggetto = " & rs!idOggetto & " and idarea = " & rs!idArea)
                Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                                       "VSAM = '" & VSAMName & "' and idoggetto = " & rs!idOggetto & " and " & _
                                                       "idarea = " & rs!idArea)
                If rsTracciato.RecordCount = 0 Then
                  'Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " order by ordinale ")
                  Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                                         "VSAM = '" & VSAMName & "' order by ordinale ")
                  If rsTracciato.RecordCount = 0 Then
                    Ordinale = 1
                  Else
                    rsTracciato.MoveLast
                    'Ordinale = rs.RecordCount + 1
                    Ordinale = rsTracciato!Ordinale + 1
                  End If
                  rsTracciato.AddNew
                  'rsTracciato!idArchivio = idArchivio
                  rsTracciato!VSam = VSAMName
                  rsTracciato!Ordinale = Ordinale
                  rsTracciato!idOggetto = rs!idOggetto
                  rsTracciato!idArea = rs!idArea
                  rsTracciato!Tag = "DD"
                  rsTracciato.Update
                  rsTracciato.Close
                End If
                rs.MoveNext
                'ATTENZIONE: non posso pulire il record "temporaneo" d'appoggio in PsRel_Tracciati,
                'perche' la stessa relazione puo' servire ad altre JCL!
              Next i
            End If
            rs.Close
          Next a
          'dsn = parDsn(0)
        Else
          Set rs = m_Fun.Open_Recordset("select * from PsRel_file where idoggetto = " & GbIdOggetto)
          If rs.RecordCount Then
            rs.MoveLast 'mi serve l'ultimo progressivo... (nFile)
            nFile = rs!nFile + 1
          Else
            nFile = 1
          End If
          rs.AddNew
          rs!idOggetto = GbIdOggetto
          rs!nFile = nFile
          rs!Type = "JCL"
          rs!nstep = stepNumber
          rs!Step = stepName
          rs!program = pgmName
          rs!dsnName = dsn
          rs!LINK = ddname
          rs!use = disp
          rs!Update = False 'era parametrico (sempre a false!)
          rs.Update
          rs.Close
          
          ''''''''''''''''''''''''''''''''''''''''''''
          ' AGGIORNAMENTO DMVSM_Archivio
          ''''''''''''''''''''''''''''''''''''''''''''
          ''''''''''''''Aggiorna_Archivi GbIdOggetto, "JCL", stepNumber, nFile, statement
          'SQ 20-11-06 (portato sopra)
          'i = InStr(dsn, "(") 'GDG
          'If i Then
          '  dsn = Left(dsn, i - 1)
          '  isGDG = True
          '  isSEQ = True
          'End If
          Set rs = m_Fun.Open_Recordset("select * from DMVSM_Archivio where nome = '" & dsn & "'")
          If rs.RecordCount = 0 Then
            rs.AddNew
            'rs!data_creazione = Now
            rs!idorigine = GbIdOggetto
            rs!Tag = "Generated by JCL"
            rs!nome = dsn 'serve?
            rs!VSam = "VSM_" & rs!idArchivio
            ' Mauro 06/08/2007 : cambio i flag con un campo Type
            'rs!Seq = isSEQ
            rs!Type = wFileType
            rs!gdg = isGDG
            rs!recform = "U"
            rs!recsize = 0
            rs!MinLen = 0
            rs!MaxLen = 0
            rs!KeyStart = 0
            rs!KeyLen = 0
            rs!data_creazione = Now
            Set rsVSAM = m_Fun.Open_Recordset("select * from DMVSM_NomiVSAM where NomeVSAM = '" & rs!VSam & "'")
            If rsVSAM.RecordCount = 0 Then
              rsVSAM.AddNew
              rsVSAM!NomeVSAM = rs!VSam & ""
              rsVSAM!Acronimo = rs!VSam & ""
              rsVSAM!to_migrate = True
              rsVSAM!obsolete = False
              rsVSAM.Update
            End If
            rsVSAM.Close
          Else
            'rs!data_modifica = Now
            ' Mauro 06/08/2007 : cambio i flag con un campo Type
            'rs!Seq = isSEQ
            If Len(wFileType) Then
              rs!Type = wFileType
            End If
            rs!gdg = isGDG
          End If
          ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
          'idArchivio = rs!idArchivio 'contatore automatico...
          VSAMName = rs!VSam & ""
          rs.Update
          rs.Close
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' AGGIORNAMENTO DMVSM_Tracciato
          ' (attenzione... passo di qui anche con le utility: non e' il massimo...
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          Set rs = m_Fun.Open_Recordset("select IdOggetto,IdArea from PsRel_Tracciati where " & _
                                        "Program='" & pgmName & "' and DDname = '" & ddname & "'")
          If rs.RecordCount Then
            'PGM gia' parserato: info tracciato gia' presenti...
            For i = 1 To rs.RecordCount
              'Parser.PsConnection.Execute "INSERT INTO DMVSM_Tracciato " & _
              '                              "VALUES(" & idArchivio & "," & i & "," & rs!idOggetto & "," & rs!idArea & ")"
              '''''''''''''''''''''''
              ' 23-02-06: c'era il problema di chiavi duplicate...
              ' A cosa serve l'Ordinale? vedere se sarebbe OK la chiave idArchivio|IdOggetto|idArea
              '''''''''''''''''''''''
              ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
              'Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " and idoggetto = " & rs!idOggetto & " and idarea = " & rs!idArea)
              Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                                     "VSAM = '" & VSAMName & "' and idoggetto = " & rs!idOggetto & " and idarea = " & rs!idArea)
              If rsTracciato.RecordCount = 0 Then
                'Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " order by ordinale ")
                Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                                       "VSAM = '" & VSAMName & "' order by ordinale ")
                If rsTracciato.RecordCount = 0 Then
                  Ordinale = 1
                Else
                  rsTracciato.MoveLast
                  'Ordinale = rs.RecordCount + 1
                  Ordinale = rsTracciato!Ordinale + 1
                End If
                rsTracciato.AddNew
                'rsTracciato!idArchivio = idArchivio
                rsTracciato!VSam = VSAMName
                rsTracciato!Ordinale = Ordinale
                rsTracciato!idOggetto = rs!idOggetto
                rsTracciato!idArea = rs!idArea
                rsTracciato!Tag = "DD"
                rsTracciato.Update
                rsTracciato.Close
              End If
              rs.MoveNext
              'ATTENZIONE: non posso pulire il record "temporaneo" d'appoggio in PsRel_Tracciati,
              'perche' la stessa relazione puo' servire ad altre JCL!
            Next
          End If
          rs.Close
        End If
      End If
  End Select
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'SQ: sto pezzo era in testa... verificare...
    'SQ If InStr(statement, " DLBL ") > 0 Then wSk = " DLBL "
  'wSk = " DD " '???
  'w_Update = False
  
'  k = InStr(statement, wSk)
'  If k > 30 Then
'  '   Stop
'     Exit Sub
'  End If
  'If Trim(wSk) = "DD" Then
     'wStr1 = Mid$(statement, 1, K - 1)
     'wStr1 = Replace(wStr1, "/", "")
     'wStr1 = Replace(wStr1, "/", "")
     'w_Link = Trim(wStr1)
     'wStr1 = SelezionaParam(statement, "DISP=")
     'If wStr1 <> "" Then
     '  disp = wStr1
     'End If
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Exit Sub
errdb:
  'gestire...
  m_Fun.writeLog "###parseJclDD - idOggetto: " & GbIdOggetto & " - " & err.description
  'Resume
End Sub

Sub parserJclDDPsbPgm(wRecord, w_RunProgram, w_PsbName, FD)
  Dim k As Integer
  
  Dim wNomePsb As String
  Dim wIdPgm As Variant
  Dim wIdPsb As Variant
  Dim wNomePgm As String
  Dim tb As Recordset
  Dim SaveIdOggetto As Variant
  Dim wRecIn As String
  Dim y  As Integer
  Dim wStr1 As String
  
  If w_RunProgram <> "DSNMTV01" Then Exit Sub
  
  wNomePsb = w_PsbName
  If InStr(wRecord, " DD ") = 0 Then Exit Sub
  If InStr(wRecord, "DDITV02 ") = 0 Then Exit Sub
  
  wIdPgm = 0
  wIdPsb = 0
  
  Line Input #FD, wRecIn
  CalcolaLOCSJcl wRecIn
  GbNumRec = GbNumRec + 1
  
  wStr1 = wRecIn
  k = InStr(wStr1, ",")
  While k > 0
    y = y + 1
    wStr1 = Mid$(wStr1, k + 1)
    If y = 8 Then
      wNomePgm = wStr1
    End If
    k = InStr(wStr1, ",")
  Wend
  
  k = InStr(wNomePgm, ",")
  If k > 0 Then wNomePgm = Mid$(wNomePgm, 1, k - 1)
  k = InStr(wNomePgm, " ")
  If k > 0 Then wNomePgm = Mid$(wNomePgm, 1, k - 1)
  wNomePgm = Trim(wNomePgm)
  
  w_RunProgram = wNomePgm
  
  Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where (tipo = 'CBL' or tipo = 'ASM' or tipo = 'PLI') and nome = '" & wNomePgm & "'")
  If tb.RecordCount Then
    AggiungiRelazione tb!idOggetto, "CALL", wNomePgm, wNomePgm
    wIdPgm = tb!idOggetto
  End If
  
  Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where tipo = 'PSB' and nome = '" & wNomePsb & "'")
  If tb.RecordCount > 0 Then
    wIdPsb = tb!idOggetto
  Else
    wIdPsb = -1
  End If
  
  If wIdPgm > 0 Then
    SaveIdOggetto = GbIdOggetto
    GbIdOggetto = wIdPgm
    If wIdPsb > 0 Then
      AggiungiRelazione wIdPsb, "PSB", wNomePsb, wNomePsb
      wStr1 = "delete * from bs_segnalazioni where idoggetto = " & wIdPgm & " and codice = 'F07' "
      Parser.PsConnection.Execute wStr1
    End If
    GbIdOggetto = SaveIdOggetto
  End If

End Sub

''''''''''''''''''''''''''''''''''''''''''
'MF 03/08/08
'Nuovo parsing
''''''''''''''''''''''''''''''''''''''''''
Sub parseIDCAMS(FD As Long, Optional controlCard As String = "INSTREAM")
  Dim k As Integer
  Dim Tba As Recordset, TbaAix As Recordset, TbChiavi As Recordset
  Dim wNomeArc As String, wRecF As String, wNomeArc2 As String
  Dim token As String
  Dim wKeyStart As Long, wKeyLen As Long, wMinLen As Long, wMaxLen As Long, wRecS As Long
  ' Mauro 06/08/2007 : cambio i flag con un campo Type
  'Dim wRRDS As Boolean, wKSDS As Boolean, wESDS As Boolean, wSEQ As Boolean, wGDG As Boolean
  Dim wFileType As String
  Dim wGDG As Boolean
  Dim statement As String, wStr1 As String, Wstr2 As String, wRec As String, SaveWstr As String
  Dim idArchivio As Long, idAix As Long, idKey As Long
  Dim continue1 As Boolean, continue2 As Boolean
  Dim nextDefine As Boolean
  Dim appo As String, appotoken As String
  Dim Relate As String
  Dim i As Long, j As Long, count As Long
  Dim rsVSAM As Recordset
  
  On Error GoTo errdb

  If controlCard = "INSTREAM" Then
    'SILVIA
    'While Left(wRec, 2) <> "//" And Left(wRec, 2) <> "/*" And Left(wRec, 3) <> "//*" And Not EOF(FD)
    While Left(wRec, 1) <> "/" And Not EOF(FD)
      Line Input #FD, wRec
      CalcolaLOCSJcl wRec
      GbNumRec = GbNumRec + 1
      If Left(wRec, 2) = "//" Then
        alreadyReadLine = wRec
      Else
        'SQ: LE LINEE TERMINANO CON "-" SE LO STATEMENT VA A CAPO...
        'SQ 5-12-07: colonna 72!
        wRec = Trim(Left(wRec, 72))
        'SILVIA
        If Left(wRec, 2) <> "/*" Then
          If Right(wRec, 1) = "-" Then
            wRec = Left(wRec, Len(wRec) - 1) & " "
          End If
          statement = statement & " " & wRec
        Else
          wRec = " " & wRec
          'If Right(wRec, 2) <> "*/" Then
          '  MsgBox "NOOOOOO!!!!!"
          'End If
        End If
      End If
    Wend
  Else
    statement = controlCard
  End If
  
  idAix = 1
  idKey = 1
  i = 1
  j = 1
  
  While Len(statement)
    continue1 = True
    continue2 = True
    wKeyStart = 0
    wKeyLen = 0
    nextDefine = False
    
    token = nextToken_new(statement)
    
    Select Case token
      Case "DEFINE"
        token = nextToken_new(statement)
        While Len(statement) And Not nextDefine
          Select Case token
            Case "GDG"  ' livello 2 sotto DEFINE
              If Left(statement, 1) = "(" Then
                statement = Trim(Mid$(statement, 2))
              End If
              While Len(statement) And continue1
                token = nexttoken(statement)
                Select Case token
                  Case "NAME"
                    'MF Questo dovr� essere inserito nella colonna Cluster
                    token = nexttoken(statement)
                    token = Replace(token, "(", "")
                    token = Replace(token, ")", "")
                    token = Trim(token)
                    wNomeArc = token
                    Set Tba = m_Fun.Open_Recordset("select * from dmvsm_archivio where nome = '" & token & "'")
                    If Tba.RecordCount = 0 Then
                      Tba.AddNew
                      Tba!data_creazione = Now
                      Tba!idorigine = GbIdOggetto
                      Tba!Tag = "Generated by IDCAMS"
                      Tba!VSam = "VSM_" & Tba!idArchivio
                      wFileType = ""
                      wGDG = True
                      wRecF = "U"
                      wRecS = 0
                      wKeyStart = 0
                      wKeyLen = 0
                      wMinLen = 0
                      wMaxLen = 0
                      Set rsVSAM = m_Fun.Open_Recordset("select * from DMVSM_NomiVSAM where NomeVSAM = '" & Tba!VSam & "'")
                      If rsVSAM.RecordCount = 0 Then
                        rsVSAM!NomeVSAM = Tba!VSam & ""
                        rsVSAM!Acronimo = Tba!VSam & ""
                        rsVSAM!to_migrate = True
                        rsVSAM!obsolete = False
                        rsVSAM.Update
                      End If
                      rsVSAM.Close
                    Else
                      'wRRDS = Tba!RRDS
                      'wESDS = Tba!ESDS
                      'wKSDS = Tba!KSDS
                      'wSEQ = Tba!Seq
                      wFileType = Tba!Type & ""
                      wGDG = Tba!gdg
                      wRecF = Tba!recform & ""
                      wRecS = "0" & Tba!recsize
                      wKeyStart = "0" & Tba!KeyStart
                      wKeyLen = "0" & Tba!KeyLen
                      wMinLen = "0" & Tba!MinLen
                      wMaxLen = "0" & Tba!MaxLen
                    End If
                    idArchivio = Tba!idArchivio
                End Select
              Wend
            Case "CL", "CLUSTER" ' livello 2 sotto DEFINE
              If Left(statement, 1) = "(" Then
                statement = Trim(Mid$(statement, 2))
              End If
              While Len(statement) And continue1
                token = nexttoken(statement)
                Select Case token
                  Case "NAME"
                    'MF Questo dovr� essere inserito nella colonna Cluster
                    token = nexttoken(statement)
                    token = Replace(Replace(token, ")", ""), "(", "")
                    token = Trim(Replace(token, "'", ""))
                    wNomeArc = token
                    Set Tba = m_Fun.Open_Recordset("select * from dmvsm_archivio where nome = '" & token & "'")
                    If Tba.RecordCount = 0 Then
                      Tba.AddNew
                      Tba!data_creazione = Now
                      Tba!idorigine = GbIdOggetto
                      Tba!Tag = "Generated by IDCAMS "
                      Tba!VSam = "VSM_" & Tba!idArchivio
                      ' Mauro 06/08/2007 : cambio i flag con un campo Type
                      'wRRDS = False
                      'wESDS = False
                      'wKSDS = False
                      'wSEQ = False
                      wFileType = ""
                      wGDG = False
                      wRecF = "U"
                      wRecS = 0
                      wKeyStart = 0
                      wKeyLen = 0
                      wMinLen = 0
                      wMaxLen = 0
                      Set rsVSAM = m_Fun.Open_Recordset("select * from DMVSM_NomiVSAM where NomeVSAM = '" & Tba!VSam & "'")
                      If rsVSAM.RecordCount = 0 Then
                        rsVSAM.AddNew
                        rsVSAM!NomeVSAM = Tba!VSam & ""
                        rsVSAM!Acronimo = Tba!VSam & ""
                        rsVSAM!to_migrate = True
                        rsVSAM!obsolete = False
                        rsVSAM.Update
                      End If
                      rsVSAM.Close
                    Else
                      ' Mauro 06/08/2007 : cambio i flag con un campo Type
                      'wRRDS = Tba!RRDS
                      'wESDS = Tba!ESDS
                      'wKSDS = Tba!KSDS
                      'wSEQ = Tba!Seq
                      wFileType = Tba!Type & ""
                      wGDG = Tba!gdg
                      wRecF = Tba!recform & ""
                      wRecS = "0" & Tba!recsize
                      wKeyStart = "0" & Tba!KeyStart
                      wKeyLen = "0" & Tba!KeyLen
                      wMinLen = "0" & Tba!MinLen
                      wMaxLen = "0" & Tba!MaxLen
                    End If
                    idArchivio = Tba!idArchivio
                  Case "NONINDEXED"
                    ' Mauro 06/08/2007 : cambio i flag con un campo Type
                    'wESDS = True
                    wFileType = "ESDS"
                  Case "INDEXED"
                    'wKSDS = True
                    wFileType = "KSDS"
                  Case "NUMBERED"
                    'wRRDS = True
                    wFileType = "RRDS"
                  Case "RECORDSIZE", "RECSZ"
                    'sq
                    token = nextToken_new(statement)
                    token = Replace(token, "(", "")
                    token = Replace(token, ")", "")
                    'silvia: gestisco anche il caso (nn,nn) non solo (nn nn)
                    token = Replace(token, ",", " ")
                    '
                    Wstr2 = token
                    token = nextToken_new(Wstr2)
                    wMinLen = token
                    token = nextToken_new(Wstr2)
                    wMaxLen = "0" & token
                  Case "RECORDFORMAT"
                    'Non gestito
                  Case "FIXBLK"
                    wRecF = "FB"
                  Case "FIXED"
                    wRecF = "F"
                  Case "VARBLK"
                    wRecF = "VB"
                  Case "VARIABILE"
                    wRecF = "V"
                  Case "KEYS"
                    token = nexttoken(statement)
                    token = Replace(token, "(", "")
                    token = Replace(token, ")", "")
                    wKeyLen = CInt(nexttoken(token))
                    wKeyStart = CInt(nexttoken(token))
      
                    Dim rsKeyP As Recordset
                    Set rsKeyP = m_Fun.Open_Recordset("select * from DMVSM_CHIAVI where idArchivio = " & idArchivio)
                    If rsKeyP.RecordCount = 0 Then
                      rsKeyP.AddNew
                      rsKeyP!idArchivio = idArchivio
                      rsKeyP!idKey = 1
                      rsKeyP!KeyStart = wKeyStart
                      rsKeyP!KeyLen = wKeyLen
                      rsKeyP!nome = "PKEY"
                      rsKeyP!data_creazione = Now
                      rsKeyP!data_modifica = Now
                      rsKeyP!Tag = "Generated by IDCAMS "
                      rsKeyP!primaria = True
                      rsKeyP.Update
                    Else
                    End If
                    rsKeyP.Close
                  Case "DATA"
                    continue2 = True
                    If Left(statement, 1) = "(" Then
                      statement = Trim(Mid(statement, 2))
                    End If
                    While Len(statement) And continue2
                      token = nexttoken(statement)
                      Select Case token
                        Case "NAME"
                          token = nexttoken(statement)
                          token = Replace(token, "(", "")
                          token = Replace(token, ")", "")
                          token = Trim(token)
                          wNomeArc2 = token
                        Case "RECORDSIZE", "RECSZ"
                          token = nextToken_new(statement)
                          token = Replace(token, "(", "")
                          token = Replace(token, ")", "")
                          'silvia: gestisco anche il caso (nn,nn) non solo (nn nn)
                          token = Replace(token, ",", " ")
                          '
                          Wstr2 = token
                          token = nextToken_new(Wstr2)
                          wMinLen = token
                          token = nextToken_new(Wstr2)
                          wMaxLen = "0" & token
                        Case "KEYS"
                          token = nexttoken(statement)
                          token = Replace(token, "(", "")
                          token = Replace(token, ")", "")
                          wKeyLen = CInt(nexttoken(token))
                          wKeyStart = CInt(nexttoken(token))
            
                          Dim rsKey2 As Recordset
                          Set rsKey2 = m_Fun.Open_Recordset("select * from DMVSM_CHIAVI where idArchivio = " & idArchivio)
                          If rsKey2.RecordCount = 0 Then
                            rsKey2.AddNew
                            rsKey2!idArchivio = idArchivio
                            rsKey2!idKey = 1
                            rsKey2!KeyStart = wKeyStart
                            rsKey2!KeyLen = wKeyLen
                            rsKey2!nome = "PKEY"
                            rsKey2!data_creazione = Now
                            rsKey2!data_modifica = Now
                            rsKey2!Tag = "Generated by IDCAMS "
                            rsKey2!primaria = True
                            rsKey2.Update
                          'Else
                          End If
                          rsKey2.Close
                        Case "INDEX"
                          'Non gestito al momento....mangio lo statement fino alla prossima DEFINE
                          token = nexttoken(statement)
                          token = Replace(token, "(", "")
                          token = Replace(token, ")", "")
                          appo = statement
                          appotoken = nexttoken(appo)
                          While appotoken <> "DEFINE" And appotoken <> "DELETE" And Len(appo)
                            appotoken = nexttoken(appo)
                          Wend
                          continue1 = False
                          continue2 = False
                          nextDefine = True
                        Case Else
                        'Tutti i casi non gestiti al momento
                        'silvia
                          appo = statement
                          appotoken = nexttoken(appo)
                          If appotoken = "DEFINE" Or appotoken = "DELETE" Then
                            continue1 = False
                            continue2 = False
                            nextDefine = True
                          End If
                        '
                      End Select
                    Wend
                  Case "CATALOG"
                    'Non gestito al momento....mangio lo statement fino alla prossima DEFINE
                    token = nexttoken(statement)
                    token = Replace(token, "(", "")
                    token = Replace(token, ")", "")
                    appo = statement
                    appotoken = nexttoken(appo)
                    While appotoken <> "DEFINE" And Len(appo)
                      appotoken = nexttoken(appo)
                    Wend
                    nextDefine = True
                  Case Else
                    'Tutti i casi non gestiti
                End Select
              Wend
              Tba!nome = wNomeArc
              Tba!data_modifica = Now
              ' Mauro 06/08/2007 : cambio i flag con un campo Type
              'Tba!ESDS = wESDS
              'Tba!KSDS = wKSDS
              'Tba!RRDS = wRRDS
              'Tba!Seq = wSEQ
              Tba!Type = wFileType
              Tba!gdg = wGDG
              Tba!recform = wRecF
              Tba!recsize = wRecS
              Tba!MinLen = wMinLen
              Tba!MaxLen = wMaxLen
              Tba!KeyStart = wKeyStart
              Tba!KeyLen = wKeyLen
              Tba.Update
              Tba.Close
              
              Set TbChiavi = m_Fun.Open_Recordset("select * from DMVSM_AIX where Relate = '" & wNomeArc & "'")
              'If TbChiavi.RecordCount > 0 Then
              count = TbChiavi.RecordCount
              While count > 0
                Set rsKey2 = m_Fun.Open_Recordset("select * from DMVSM_CHIAVI where idArchivio = " & idArchivio)
                For j = 1 To rsKey2.RecordCount
                  'If rsKey2!nome = wNomeArc Then
                  If rsKey2!nome = TbChiavi!nome Then
                    Exit For
                  End If
                  idKey = rsKey2!idKey
                  rsKey2.MoveNext
                Next
                If j > rsKey2.RecordCount Then
  '                If rsKey2.RecordCount Then
  '                  idKey = idKey + 1
  '                Else
                    'Prima chiave secondaria
                    idKey = idKey + 1
  '                End If
                  'Chiave non presente
                  rsKey2.AddNew
                  rsKey2!idArchivio = idArchivio
                  rsKey2!idKey = idKey
                  rsKey2!KeyStart = TbChiavi!KeyStart
                  rsKey2!KeyLen = TbChiavi!KeyLen
                  rsKey2!nome = TbChiavi!nome
                  rsKey2!data_creazione = Now
                  rsKey2!data_modifica = Now
                  rsKey2!Tag = "Generated by IDCAMS "
                  rsKey2!primaria = False
                  rsKey2.Update
                Else
                  'Chiave gi� presente
                  idAix = rsKey2!idKey
                End If
                rsKey2.Close
                TbChiavi.MoveNext
                count = count - 1
              Wend
            Case "AIX", "ALTERNATEINDEX" ' livello 2 sotto DEFINE
              If Left(statement, 1) = "(" Then
                statement = Trim(Mid(statement, 2))
              End If
              While Len(statement) And continue1
                token = nexttoken(statement)
                Select Case token
                  Case "NAME"
                    'MF Questo dovr� essere inserito nella colonna Cluster
                    token = nexttoken(statement)
                    token = Replace(token, "(", "")
                    token = Replace(token, ")", "")
                    token = Trim(token)
                    wNomeArc = token
                  Case "RELATE"
                    token = nexttoken(statement)
                    token = Replace(token, "(", "")
                    token = Replace(token, ")", "")
                    token = Trim(token)
                    Relate = token
                  Case "KEYS"
                    token = nexttoken(statement)
                    token = Replace(token, "(", "")
                    token = Replace(token, ")", "")
                    wKeyLen = CInt(nextToken_tmp(token))
                    wKeyStart = CInt(nexttoken(token))
                  Case "DATA"
                    token = nexttoken(statement)
                    token = Replace(token, "(", "")
                    token = Replace(token, ")", "")
                    appo = statement
                    appotoken = nexttoken(appo)
                    While appotoken <> "DEFINE" And appotoken <> "INDEX" And Len(appo)
                      appotoken = nexttoken(appo)
                    Wend
                    nextDefine = True
                  Case "INDEX"
                    token = nexttoken(statement)
                    token = Replace(token, "(", "")
                    token = Replace(token, ")", "")
                    appo = statement
                    appotoken = nexttoken(appo)
                    While appotoken <> "DEFINE" And Len(appo)
                      appotoken = nexttoken(appo)
                    Wend
                    continue1 = False
                    nextDefine = True
                  Case Else
                      'silvia
                       'token = nextToken(statement)
                      'Tutti i casi non gestiti
                  End Select
                Wend
              'Fine AIX: inserimento in db (se non esistente - chiave: nome VSAM+nome KEY)
              Set TbaAix = m_Fun.Open_Recordset("select * from DMVSM_AIX where NOME='" & wNomeArc & "' AND RELATE='" & Relate & "'")
              If TbaAix.RecordCount = 0 Then
                TbaAix.AddNew
                TbaAix!nome = wNomeArc
                TbaAix!KeyStart = wKeyStart
                TbaAix!KeyLen = wKeyLen
                TbaAix!Relate = Relate
                TbaAix.Update
              Else
                'se ho due definizioni? diverse?
                'ci penseremo...
              End If
              TbaAix.Close
              
              'Associazione chiave/vsam
              'select sul archivio con nome
              Set Tba = m_Fun.Open_Recordset("select idArchivio from DMVSM_ARCHIVIO where Nome = '" & Relate & "'")
              If Tba.RecordCount > 0 Then
                'se c'�, controlli chiavi...
                Dim rsKeyS As Recordset
                Set rsKeyS = m_Fun.Open_Recordset("select * from DMVSM_CHIAVI where idArchivio=" & Tba!idArchivio & " AND not primaria ORDER BY idKey") 'wNomeArc 'ORDER BY idKey
                For i = 1 To rsKeyS.RecordCount
                  If rsKeyS!nome = wNomeArc Then
                    Exit For
                  End If
                  rsKeyS.MoveNext
                Next
                If i > rsKeyS.RecordCount Then
                  If rsKeyS.RecordCount Then
                    idAix = i + 1
                  Else
                    'Prima chiave secondaria
                    idAix = idAix + 1
                  End If
                  'Chiave non presente
                  rsKeyS.AddNew
                  rsKeyS!idArchivio = Tba!idArchivio
                  rsKeyS!idKey = idAix
                  rsKeyS!KeyStart = wKeyStart
                  rsKeyS!KeyLen = wKeyLen
                  rsKeyS!nome = wNomeArc
                  rsKeyS!data_creazione = Now
                  rsKeyS!data_modifica = Now
                  rsKeyS!Tag = "Generated by IDCAMS"
                  rsKeyS!primaria = False
                  rsKeyS.Update
                Else
                  'Chiave gi� presente
                End If
                rsKeyS.Close
              End If
              Tba.Close
            Case "PATH"
              'Non gestito al momento....mangio lo statement fino alla prossima DEFINE
              appotoken = nexttoken(statement)
               appo = statement
              'appotoken = nextToken(statement)
              While appotoken <> "DEFINE" And appotoken <> "DELETE" And Len(appo)
                appotoken = nexttoken(appo)
                If appotoken <> "DEFINE" Then
                  token = nexttoken(statement)
                End If
              Wend
              nextDefine = True
            'SQ 5-12-07 pezza: si inluppa se non c'� l'else!
            Case Else
              '!?
              token = nextToken_new(statement)
          End Select
        Wend
      Case "DELETE"
        'Non gestito al momento....mangio lo statement fino alla prossima DEFINE
        appotoken = nexttoken(statement)
         appo = statement
        'appotoken = nextToken(statement)
        While appotoken <> "DEFINE" And appotoken <> "DELETE" And Len(appo)
          appotoken = nexttoken(appo)
          If appotoken <> "DEFINE" Then
            token = nexttoken(statement)
          End If
        Wend
      Case Else
        'SQ verificare...
        token = nextToken_new(statement)
      End Select
  Wend
  Exit Sub

errdb:
  If err.Number = 123 Then  'Errore gestito: nextToken syntax error
    'SQ pezza: s'inluppava!...
    If Left(statement, 1) = "'" Then
     statement = Mid(statement, 2)
    End If
    If Left(statement, 1) = "(" Then
      statement = Mid(statement, 2)
      appo = statement
    End If
    GbOldNumRec = GbNumRec
    addSegnalazione statement, "P00", statement
    Resume Next
  Else
  'gestire...
    m_Fun.writeLog "###parseIDCAMS - idOggetto: " & GbIdOggetto & " - " & err.description
  'Resume
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''' SQ - preso vecchio codice... fa schifo... rifare come parseIKJEFT01
'''''' SQ 20-11-06 - Mancava la chiamata in casi di SK su FILE!!!
'''''' aggiunto parametro "controlCard" (fa anche da flag)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''Sub parseIDCAMS(FD As Long, Optional controlCard As String = "INSTREAM")
'''''  Dim k As Integer
'''''  Dim Tba As Recordset
'''''  Dim wNomeArc As String, wRecF As String
'''''  Dim wKeyStart As Long, wKeyLen As Long, wMinLen As Long, wMaxLen As Long, wRecS As Long
'''''  ' Mauro 06/08/2007 : cambio i flag con un campo Type
'''''  'Dim wRRDS As Boolean, wKSDS As Boolean, wESDS As Boolean, wSEQ As Boolean, wGDG As Boolean
'''''  Dim wFileType As String
'''''  Dim wGDG As Boolean
'''''  Dim statement As String, wStr1 As String, Wstr2 As String, wRec As String, SaveWstr As String
'''''  Dim idArchivio As Long
'''''
'''''  If controlCard = "INSTREAM" Then
'''''    While Left(wRec, 1) <> "/" And Not EOF(FD)
'''''      Line Input #FD, wRec
'''''      GbNumRec = GbNumRec + 1
'''''      If Left(wRec, 2) = "//" Then
'''''        alreadyReadLine = wRec
'''''      Else
'''''        'SQ: LE LINEE TERMINANO CON "-" SE LO STATEMENT VA A CAPO...
'''''        wRec = Trim(wRec)
'''''        If Right(wRec, 1) = "-" Then
'''''          wRec = Left(wRec, Len(wRec) - 1) & " "
'''''        End If
'''''        statement = statement & " " & wRec
'''''      End If
'''''    Wend
'''''  Else
'''''    statement = controlCard
'''''  End If
'''''
'''''  If True Then
'''''    parsePROVA statement
'''''  End If
'''''
''''''''
''''''''  'ANALISI DEFINE:
''''''''  k = InStr(statement, " DEFINE ")
''''''''
''''''''  If k Then
''''''''    SaveWstr = statement
''''''''    statement = Trim(Mid(statement, k + 8))
''''''''    While Len(statement)
''''''''       k = InStr(statement, " DEFINE ")
''''''''       If k > 0 Then
''''''''           wStr1 = Mid$(statement, 1, k - 1)
''''''''           statement = Trim(Mid$(statement, k))
''''''''       Else
''''''''           wStr1 = statement
''''''''           statement = ""
''''''''      End If
''''''''      k = InStr(wStr1, "NAME")
''''''''      wStr1 = Trim(Mid$(wStr1, k + 4))
''''''''      k = InStr(wStr1, ")")
''''''''      If k > 0 Then
''''''''          wNomeArc = Mid$(wStr1, 1, k)
''''''''      Else
''''''''         k = InStr(wStr1, " ")
''''''''         wNomeArc = Mid$(wStr1, 1, k)
''''''''      End If
''''''''      wNomeArc = Replace(wNomeArc, "(", "")
''''''''      wNomeArc = Replace(wNomeArc, ")", "")
''''''''      wNomeArc = Trim(wNomeArc)
''''''''
''''''''      Set Tba = m_Fun.Open_Recordset("select * from dmvsm_archivio where nome = '" & wNomeArc & "' ")
''''''''      If Tba.RecordCount = 0 Then
''''''''        Tba.AddNew
''''''''        Tba!data_creazione = Now
''''''''        Tba!idorigine = GbIdOggetto
''''''''        Tba!Tag = "Generated by IDCAMS "
''''''''        ' Mauro 06/08/2007 : cambio i flag con un campo Type
''''''''        'wRRDS = False
''''''''        'wESDS = False
''''''''        'wKSDS = False
''''''''        'wSEQ = False
''''''''        wFileType = ""
''''''''        wGDG = False
''''''''        wRecF = "U"
''''''''        wRecS = 0
''''''''        wKeyStart = 0
''''''''        wKeyLen = 0
''''''''        wMinLen = 0
''''''''        wMaxLen = 0
''''''''      Else
''''''''        ' Mauro 06/08/2007 : cambio i flag con un campo Type
''''''''        'wRRDS = Tba!RRDS
''''''''        'wESDS = Tba!ESDS
''''''''        'wKSDS = Tba!KSDS
''''''''        'wSEQ = Tba!Seq
''''''''        wFileType = Tba!Type & ""
''''''''        wGDG = Tba!gdg
''''''''        wRecF = Tba!recform & ""
''''''''        wRecS = "0" & Tba!recsize
''''''''        wKeyStart = "0" & Tba!KeyStart
''''''''        wKeyLen = "0" & Tba!KeyLen
''''''''        wMinLen = "0" & Tba!MinLen
''''''''        wMaxLen = "0" & Tba!MaxLen
''''''''      End If
''''''''      idArchivio = Tba!idArchivio
''''''''
''''''''      ' Mauro 06/08/2007 : cambio i flag con un campo Type
''''''''      'If InStr(wStr1, " NONINDEXED ") > 0 Then wESDS = True
''''''''      'If InStr(wStr1, " INDEXED ") > 0 Then wKSDS = True
''''''''      'If InStr(wStr1, " NUMBERED ") > 0 Then wRRDS = True
''''''''      If InStr(wStr1, " NONINDEXED ") > 0 Then wFileType = "ESDS"
''''''''      If InStr(wStr1, " INDEXED ") > 0 Then wFileType = "KSDS"
''''''''      If InStr(wStr1, " NUMBERED ") > 0 Then wFileType = "RRDS"
''''''''
''''''''      k = InStr(wStr1, "RECORDSIZE")
''''''''      If k > 0 Then
''''''''         Wstr2 = Mid$(wStr1, k)
''''''''         k = InStr(Wstr2, ")")
''''''''         Wstr2 = Mid$(Wstr2, 1, k - 1)
''''''''         k = InStr(Wstr2, "(")
''''''''         If k > 0 Then Wstr2 = Mid$(Wstr2, k + 1)
''''''''         Wstr2 = Trim(Wstr2)
''''''''         k = InStr(Wstr2, " ")
''''''''         If k = 0 Then k = InStr(Wstr2, ",")
''''''''         wMinLen = Mid$(Wstr2, 1, k - 1)
''''''''         wMaxLen = Mid$(Wstr2, k + 1)
''''''''      End If
''''''''      k = InStr(wStr1, "RECORDFORMAT")
''''''''      If k > 0 Then
''''''''         Wstr2 = Mid$(wStr1, k)
''''''''         k = InStr(Wstr2, ")")
''''''''         Wstr2 = Mid$(Wstr2, 1, k - 1)
''''''''         k = InStr(Wstr2, "(")
''''''''         If k > 0 Then Wstr2 = Mid$(Wstr2, k + 1)
''''''''         Wstr2 = Trim(Wstr2)
''''''''         If InStr(Wstr2, "FIXBLK") > 0 Then wRecF = "FB"
''''''''         If InStr(Wstr2, "FIXED") > 0 Then wRecF = "F"
''''''''         If InStr(Wstr2, "VARBLK") > 0 Then wRecF = "VB"
''''''''         If InStr(Wstr2, "VARIABILE") > 0 Then wRecF = "V"
''''''''         k = InStr(Wstr2, "(")
''''''''         If k > 0 Then Wstr2 = Mid$(Wstr2, k + 1)
''''''''         wRecS = Wstr2
''''''''      End If
''''''''
''''''''      ''''''''''''''''''''''''''''''''''''''''''
''''''''      ' SQ - Gestione Chiavi
''''''''      ' Provvisoria (del cavolo!): ne tengo solo una...
''''''''      ''''''''''''''''''''''''''''''''''''''''''
''''''''      k = InStr(wStr1, "KEYS")
''''''''      If k Then
''''''''        wStr1 = Mid(wStr1, k)
''''''''        Wstr2 = nextToken(wStr1)
''''''''        Wstr2 = Replace(Replace(nextToken(wStr1), "(", ""), ")", "")
''''''''        wKeyLen = CInt(nextToken(Wstr2))
''''''''        wKeyStart = CInt(nextToken(Wstr2))
''''''''        Dim rsKey As Recordset
''''''''        'SQ tmp
''''''''        Set rsKey = m_Fun.Open_Recordset("select * from DMVSM_CHIAVI where idArchivio = " & idArchivio)
''''''''        If rsKey.RecordCount = 0 Then
''''''''          rsKey.AddNew
''''''''          rsKey!idArchivio = idArchivio
''''''''          rsKey!idKey = 1
''''''''          rsKey!KeyStart = wKeyStart
''''''''          rsKey!KeyLen = wKeyLen
''''''''          rsKey!nome = "PKEY" 'DEFAULT
''''''''          rsKey!data_creazione = Now
''''''''          rsKey!Tag = "Generated by IDCAMS "
''''''''          rsKey.Update
''''''''        Else
''''''''
''''''''        End If
''''''''        rsKey.Close
''''''''      End If
''''''''
''''''''      Tba!nome = wNomeArc
''''''''      Tba!data_modifica = Now
''''''''      ' Mauro 06/08/2007 : cambio i flag con un campo Type
''''''''      'Tba!ESDS = wESDS
''''''''      'Tba!KSDS = wKSDS
''''''''      'Tba!RRDS = wRRDS
''''''''      'Tba!Seq = wSEQ
''''''''      Tba!Type = wFileType
''''''''      Tba!gdg = wGDG
''''''''      Tba!recform = wRecF
''''''''      Tba!recsize = wRecS
''''''''      Tba!MinLen = wMinLen
''''''''      Tba!MaxLen = wMaxLen
''''''''      Tba!KeyStart = wKeyStart
''''''''      Tba!KeyLen = wKeyLen
''''''''      Tba.Update
''''''''      Tba.Close
''''''''
''''''''    Wend
''''''''  End If
'''''End Sub

'''''''''''''''''''''''''''''''''''''''''''''''
'Mf 07/08/07
'Spostato da MapsdM_Parser
'''''''''''''''''''''''''''''''''''''''''''''''
Sub ParserDefCluster(wRecord, nFile)
  Dim wStr As String
  Dim wStr1 As String
  Dim Wstr2 As String
  Dim wRec As String
  Dim SaveWstr As String
  Dim k As Integer
  Dim Tba As Recordset
  
  Dim wNomeArc As String
  
  Dim wKeyStart As Long
  Dim wKeyLen As Long
  Dim wMinLen As Long
  Dim wMaxLen As Long
  ' Mauro 06/08/2007 : cambio i flag con un campo Type
  'Dim wRRDS As Boolean
  'Dim wKSDS As Boolean
  'Dim wESDS As Boolean
  'Dim wSEQ As Boolean
  Dim filetype As String
  Dim wGDG As Boolean
  Dim wRecF As String
  Dim wRecS As Long
  Dim rsVSAM As Recordset
  
  If Mid$(wRecord, 1, 1) = "/" Then Exit Sub
   
  wStr = wRecord
  wRec = " "
  While Mid$(wRec, 1, 1) <> "/" And Not EOF(nFile)
    Line Input #nFile, wRec
    CalcolaLOCSJcl wRec
    GbNumRec = GbNumRec + 1
    wStr = wStr & wRec
  Wend
   
  If InStr(wStr, " DEFINE ") = 0 Then
    wRecord = wRec
    Exit Sub
  End If
  
  SaveWstr = wStr
        
  k = InStr(wStr, " DEFINE ")
  wStr = Trim(Mid$(wStr, k + 8))
  While Len(wStr) > 0
    k = InStr(wStr, " DEFINE")
    If k > 0 Then
      wStr1 = Mid$(wStr, 1, k - 1)
      wStr = Trim(Mid$(wStr, k))
    Else
      wStr1 = wStr
      wStr = ""
    End If
    k = InStr(wStr1, "NAME")
    wStr1 = Trim(Mid$(wStr1, k + 4))
    k = InStr(wStr1, ")")
    If k > 0 Then
      wNomeArc = Mid$(wStr1, 1, k)
    Else
      k = InStr(wStr1, " ")
      wNomeArc = Mid$(wStr1, 1, k)
    End If
    wNomeArc = Replace(wNomeArc, "(", "")
    wNomeArc = Replace(wNomeArc, ")", "")
    wNomeArc = Trim(wNomeArc)
     
    Set Tba = m_Fun.Open_Recordset("select * from dmvsm_archivio where nome = '" & wNomeArc & "' ")
    If Tba.RecordCount = 0 Then
      Tba.AddNew
      Tba!data_creazione = Now
      Tba!idorigine = GbIdOggetto
      Tba!Tag = "Generated by IDCAMS "
      Tba!VSam = "VSM_" & Tba!idArchivio
      ' Mauro 06/08/2007 : cambio i flag con un campo Type
      'wRRDS = False
      'wESDS = False
      'wKSDS = False
      'wSEQ = False
      filetype = ""
      wGDG = False
      wRecF = "U"
      wRecS = 0
      wKeyStart = 0
      wKeyLen = 0
      wMinLen = 0
      wMaxLen = 0
      Set rsVSAM = m_Fun.Open_Recordset("select * from DMVSM_NomiVSAM where NomeVSAM = '" & Tba!VSam & "'")
      If rsVSAM.RecordCount = 0 Then
        rsVSAM!NomeVSAM = Tba!VSam & ""
        rsVSAM!Acronimo = Tba!VSam & ""
        rsVSAM!to_migrate = True
        rsVSAM!obsolete = False
        rsVSAM.Update
      End If
      rsVSAM.Close
    Else
      ' Mauro 06/08/2007 : cambio i flag con un campo Type
      'wRRDS = Tba!RRDS
      'wESDS = Tba!ESDS
      'wKSDS = Tba!KSDS
      'wSEQ = Tba!Seq
      filetype = Tba!Type & ""
      wGDG = Tba!gdg
      wRecF = Tba!recform
      wRecS = Tba!recsize
      wKeyStart = Tba!KeyStart
      wKeyLen = Tba!KeyLen
      wMinLen = Tba!MinLen
      wMaxLen = Tba!MaxLen
    End If
     
    ' Mauro 06/08/2007 : cambio i flag con un campo Type
    'If InStr(wStr1, " NONINDEXED ") > 0 Then wESDS = True
    'If InStr(wStr1, " INDEXED ") > 0 Then wKSDS = True
    'If InStr(wStr1, " NUMBERED ") > 0 Then wRRDS = True
    If InStr(wStr1, " NONINDEXED ") > 0 Then filetype = "ESDS"
    If InStr(wStr1, " INDEXED ") > 0 Then filetype = "KSDS"
    If InStr(wStr1, " NUMBERED ") > 0 Then filetype = "RRDS"
    
    k = InStr(wStr1, "RECORDSIZE")
    If k > 0 Then
      Wstr2 = Mid$(wStr1, k)
      k = InStr(Wstr2, ")")
      Wstr2 = Mid$(Wstr2, 1, k - 1)
      k = InStr(Wstr2, "(")
      If k > 0 Then Wstr2 = Mid$(Wstr2, k + 1)
      Wstr2 = Trim(Wstr2)
      k = InStr(Wstr2, " ")
      If k = 0 Then k = InStr(Wstr2, ",")
      wMinLen = Mid$(Wstr2, 1, k - 1)
      wMaxLen = Mid$(Wstr2, k + 1)
    End If
    k = InStr(wStr1, "RECORDFORMAT")
    If k > 0 Then
      Wstr2 = Mid$(wStr1, k)
      k = InStr(Wstr2, ")")
      Wstr2 = Mid$(Wstr2, 1, k - 1)
      k = InStr(Wstr2, "(")
      If k > 0 Then Wstr2 = Mid$(Wstr2, k + 1)
      Wstr2 = Trim(Wstr2)
      If InStr(Wstr2, "FIXBLK") > 0 Then wRecF = "FB"
      If InStr(Wstr2, "FIXED") > 0 Then wRecF = "F"
      If InStr(Wstr2, "VARBLK") > 0 Then wRecF = "VB"
      If InStr(Wstr2, "VARIABLE") > 0 Then wRecF = "V"
      k = InStr(Wstr2, "(")
      If k > 0 Then Wstr2 = Mid$(Wstr2, k + 1)
      wRecF = Wstr2
    End If
     
    Tba!nome = wNomeArc
    Tba!data_modifica = Now
    ' Mauro 06/08/2007 : cambio i flag con un campo Type
    'Tba!ESDS = wESDS
    'Tba!KSDS = wKSDS
    'Tba!RRDS = wRRDS
    'Tba!Seq = wSEQ
    Tba!Type = filetype
    Tba!gdg = wGDG
    Tba!recform = wRecF
    Tba!recsize = wRecS
    Tba!MinLen = wMinLen
    Tba!MaxLen = wMaxLen
    Tba!KeyStart = wKeyStart
    Tba!KeyLen = wKeyLen
    Tba.Update

    Tba.Close
  Wend
  wRecord = wRec
End Sub

'SQ
'APPENDE LE ISTRUZIONI SU PIU' LINEE...
'mantenuto parte di vecchio codice
Private Function getJclStatement(file As Long, alreadyReadLine As String)
  Dim statement As String, Line As String, UltChar As String, lineErr As String
  
  On Error GoTo errToken
  
  If Len(alreadyReadLine) Then
    'gia' letta dall'istruzione precedente
    Line = alreadyReadLine
    alreadyReadLine = "" 'pulisco per i giri dopo
  Else
    Line Input #file, Line
    CalcolaLOCSJcl Line
    GbNumRec = GbNumRec + 1
  End If
  
  While (Left(Line, 2) <> "//" Or Mid(Line, 3, 1) = "*") And Not EOF(file)
    Line Input #file, Line
    CalcolaLOCSJcl Line
    GbNumRec = GbNumRec + 1
  Wend
  ' Mauro 25/03/2010 : Se arrivo a fine file e ho una riga commentata, tool la gestisce lo stesso
  If EOF(file) And (Left(Line, 2) <> "//" Or Mid(Line, 3, 1) = "*") Then
    Line = ""
  End If
  'Linea iniziale istruzione:
  GbOldNumRec = GbNumRec
  statement = RTrim(Mid(Line, 3, 70)) 'elimino il //
  Dim token As String, ifStatement As String
  ifStatement = statement 'serve una copia...
  token = nexttoken(ifStatement)
  Select Case token
    Case "IF"
      While nexttoken(ifStatement) <> "THEN" And (Len(ifStatement))
        'mangio il tutto...
        ifStatement = IIf(InStr(ifStatement, "THEN"), ifStatement, "")
      Wend
    Case "PARM"
      'MsgBox ""
    Case Else
      If Len(statement) = 0 Then
        UltChar = " "
      Else
        '''''''''''''''''''''''''''''''''''''''''
        'Attenzione: se ho ", commento" sbaglio!
        '''''''''''''''''''''''''''''''''''''''''
        'SQ - 20-10-06
        'If InStr(statement, ", ") Then
        If InStr(statement & " ", ", ") Then
          'taglio!
          statement = Left(statement, InStr(statement & " ", ", "))
        End If
        UltChar = Right(statement, 1)
      End If
      While UltChar = "," And Not EOF(file)
        'SQ 7-03-05
        Line = "//*"  'forzatura per entrare...
        While (Left(Line, 2) <> "//" Or Mid(Line, 3, 1) = "*") And Not EOF(file)
          Line Input #file, Line
          CalcolaLOCSJcl Line
          GbNumRec = GbNumRec + 1
        Wend
        Line = Trim(Mid(Line, 3, 70))
        If Len(Line) = 0 Then
          UltChar = " "
        Else
'''          '''''''''''''''''''''''''''''''''''''''''
'''          'Attenzione: se ho ", commento" sbaglio!
'''          '''''''''''''''''''''''''''''''''''''''''
'''          'SQ - 20-10-06
'''          'If InStr(line, ", ") Then
'''          If InStr(line & " ", ", ") Then
'''            'taglio!
'''            line = Left(line, InStr(line & " ", ", "))
'''          End If
'''          UltChar = Right(line, 1)
          ' Mauro 20/03/2008 : � stato rivisto tutto il giro
          '                    andava in errore con "commento, commento"
          Dim StatTmp As String
          StatTmp = Line
          Line = ""
          Do Until Left(StatTmp, 1) = " " Or Len(StatTmp) = 0
            token = nextToken_JCL(StatTmp)
            Line = Line & token
          Loop
          UltChar = Right(Line, 1)
        End If
        statement = statement & Line 'eliminare il "//" e gli space...
      Wend
  End Select
  'getJclStatement = Mid(statement, 3)
  
  '''''''''''''''''''''''''''''''''''''''
  ' SQ 17-03-06
  ' Sull'ultima linea dello statement di EXEC
  ' uno space alla fine dei parametri
  ' � il terminatore di riga! (poi commenti
  '''''''''''''''''''''''''''''''''''''''
  '...
  getJclStatement = statement
  Exit Function
errToken:
  If err.Number = 123 Then  'Errore gestito: nextToken syntax error
    statement = statement & " " & Line & StatTmp
    Line Input #file, lineErr
    CalcolaLOCSJcl lineErr
    GbNumRec = GbNumRec + 1
    Do Until Left(lineErr, 3) <> "//*"
      Line Input #file, lineErr
      CalcolaLOCSJcl lineErr
      GbNumRec = GbNumRec + 1
    Loop
    ifStatement = Mid(lineErr, 3, 70)
    statement = statement & " " & ifStatement
    StatTmp = StatTmp & " " & ifStatement
    Resume
  Else
    'gestire
    err.Raise err.Number, err.source, err.description
  End If
End Function

' Copiato da nextToken_NewNew
' Mauro 19/03/2008 : Ho bisogno di un NextToken che non mangia gli spazi
Function nextToken_JCL(inputString As String)
  Dim level, i, j As Integer
  Dim currentChar As String
  Dim isComment As Boolean
  
  'Mangio i bianchi e virgole davanti (primo giro...)
  inputString = LTrim(inputString)
  If (Len(inputString) > 0) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "("
        isComment = False
        i = 2
        level = 1
        nextToken_JCL = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "("
              If Not isComment Then
                level = level + 1
              End If
              nextToken_JCL = nextToken_JCL & currentChar
            Case ")"
              If Not isComment Then
                level = level - 1
              End If
              nextToken_JCL = nextToken_JCL & currentChar
            Case "'"
              isComment = Not isComment
              nextToken_JCL = nextToken_JCL & currentChar
            Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nextToken_JCL = nextToken_JCL & currentChar
              Else
                err.Raise 123, "nextToken_JCL", "syntax error on " & inputString
                Exit Function
              End If
          End Select
          i = i + 1
        Wend
        inputString = Mid(inputString, i)
      Case "'"
        'Ritorno gli apici
        ''''inputString = Mid(inputString, 2)
        i = InStr(2, inputString, "'")
        If i Then
          'nextToken_JCL = Left(inputString, InStr(inputString, "'") - 1)
          nextToken_JCL = Left(inputString, i)
          inputString = Mid(inputString, i + 1)
        Else
          ' Mauro 14/04/2010: Se ho gi� un'apice, aggiungendone 2, non cambia una fava
          If Left(inputString, 1) = "'" Then
            'inputString = "'" & inputString
          Else
            'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
            inputString = "''" & inputString
          End If
          err.Raise 123, "nextToken_JCL", "syntax error on " & inputString
          Exit Function
        End If
     Case Else
        'Ricerca primo separatore: " " o ","
        For i = 1 To Len(inputString)
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case " ", "'"
              nextToken_JCL = Left(inputString, i - 1)
              inputString = Mid(inputString, i)
              Exit Function
            Case "("
              nextToken_JCL = Left(inputString, i - 1)
              inputString = Mid(inputString, i)
              'pezza: se ho pippo(...),... rimane una virgola iniziale:
              'If Left(inputString, 1) = "," Then inputString = Mid(inputString, 2)
              Exit Function
            Case ","
              If i = 1 Then
                nextToken_JCL = Left(inputString, i)
                inputString = Mid(inputString, i + 1)
              Else
                nextToken_JCL = Left(inputString, i - 1)
                inputString = Mid(inputString, i)
              End If
              Exit Function
            Case Else
              'avanza pure
          End Select
        Next i
        'nessun separatore:
        nextToken_JCL = inputString
        inputString = ""
    End Select
  End If
End Function

Sub parseEXEC_PGM(statement As String, caller As String, w_Step As String, calledModule As String, parameters As String)
  Dim k As Integer
  Dim rsOggetti  As Recordset
  Dim pgmList() As String
  Dim i As Integer
  ' Mauro 02/01/2008: Gestione Proc interne
  Dim isTrovato As Boolean
  Dim a As Integer
  
  If Len(caller) = 0 Then
    caller = getNomeFromIdOggetto(GbIdOggetto)
  End If
  ' Mauro 02/08/2007 : Se ci sono spazi dopo il CalledModule, il resto � tutto commento
  'Gestione casi: EXEC GINO   **commento
  Dim st As String
  st = statement
  
  calledModule = nextToken_tmp(statement)
  
  If Mid(st & ",", Len(calledModule) + 1, 1) <> "," Then
    statement = ""
  End If
  
  ''''''''''''''''''''''''''''''''''''''''
  ' Gestione Variabili
  ''
  ' SQ 19-10-06
  ''''''''''''''''''''''''''''''''''''''''
  'spli
  calledModule = Replace(calledModule, "PGM=GO", "PGM=" & GOModule)
  
  'If InStr(calledModule, "&") Then solveParameters calledModule
  
  ReDim pgmList(0)
  If InStr(calledModule, "&") Then
    pgmList = getParameters(calledModule)
  Else
    pgmList(0) = calledModule
  End If
  
  'spli: attenzione...
  For i = 0 To UBound(pgmList)
    If Left(pgmList(i), 4) = "PGM=" Or pgmList(i) = "HPSVRUN1" Or pgmList(i) = "HPSARCTL" Then
      If InStr(pgmList(i), "&") Then
        'addSegnalazione calledModule, "NOPGMVALUE", caller
        addSegnalazione pgmList(i), "NOPGMVALUE", caller
      Else
        '''''''''''
        ' PGM/UTIL
        '''''''''''
        If pgmList(i) <> "HPSVRUN1" And pgmList(i) <> "HPSARCTL" Then
          pgmList(i) = Trim(Mid(pgmList(i), 5))
        End If
        'SQ 8-02-07
        calledModule = pgmList(i)
        ''''''''''''''''''''''''''''''''''''''''''''
        ' 2-02-06
        ' Gestione nomi VARIABILI
        ' ...introdurre "cattura" assegnamenti...
        ''''''''''''''''''''''''''''''''''''''''''''
        If InStr(pgmList(i), "&") Then
          'addSegnalazione calledModule, "NOPGMVALUE", caller
          addSegnalazione pgmList(i), "NOPGMVALUE", caller
        Else
          If ExistObjSyst(pgmList(i), "PGM") Then 'Presente in "psUte_ObjSys":
            'Aggiungi alla "PsCom_Obj"
            'AggiungiComponente calledModule, calledModule, "UTL", "SYSTEM"  'Non sapevo chi e quanti utilizzano un certo "componente"!
            'Soluzione temp:
            AggiungiComponente pgmList(i), caller, "UTL", "SYSTEM"
            'spli
            If pgmList(i) = "IKJEFT01" Then
              If InStr(statement, "%IEBUPD") > 0 Then
                ' funziona solo se c'� uno spazio tra %IEBUPD e il nome del programma
                GOModule = Mid(statement, InStr(statement, "%IEBUPD") + 8, Len(statement) - InStr(statement, "%IEBUPD") - 8)
              End If
            End If
          Else
            ''''''''''''''''''''''''''''''''''
            ' SQ 15-02-06
            ' Gestione PsLoad
            ' Sostituire OVUNQUE la query commentata con il blocco sotto
            ''''''''''''''''''''''''''''''''''
            'SQ - gestire con checkRelation!...
            Set rsOggetti = m_Fun.Open_Recordset("select idOggetto,tipo from bs_oggetti where " & _
                                                 "nome = '" & pgmList(i) & "' and tipo in ('CBL','ASM','PLI','EZT','FRT')")
            If rsOggetti.RecordCount = 0 Then
              'Nome LOAD diverso da nome SOURCE: aggiorniamo il recordset
              Set rsOggetti = m_Fun.Open_Recordset("select a.idOggetto,a.tipo from bs_oggetti as a,PsLoad as b where " & _
                                                   "b.load = '" & pgmList(i) & "' and b.source=a.nome and tipo in ('CBL','ASM','PLI','EZT','FRT')")
            End If
     
            If rsOggetti.RecordCount Then 'Perche' 1??????
              AggiungiRelazione rsOggetti!idOggetto, "PGM", caller, pgmList(i)
            Else
              'AggiungiSegnalazione calledModule, "NOCALL", caller
              addSegnalazione pgmList(i), "NOPGM", caller
            End If
          End If
        End If
      End If
    Else
      'spli: per ora escludo solo il valore vuoto
      If pgmList(i) <> "" Then
        ''''''''''''''''''''
        ' PROC:
        ' - <nome_proc>
        ' - PROC=<nome_proc>
        '''''''''''''''''''''
        ' Mauro 12/01/2009 : elimino "PROC=" senn� viene salvato sul DB assieme al nome della Procedure
        pgmList(i) = Replace(pgmList(i), "PROC=", "")
        calledModule = pgmList(i)
        ''''''''''''''''''''''''''''''''''''''''''''
        ' 2-02-06
        ' Gestione nomi VARIABILI
        ' ...introdurre "cattura" assegnamenti...
        ''''''''''''''''''''''''''''''''''''''''''''
        'Mauro 3-08-07
        'non lo faceva per i PGM (portato sotto)
        'parameters = statement
        If InStr(pgmList(i), "&") Then
          'AggiungiSegnalazione calledModule, "NOPRCVALUE", caller
          addSegnalazione pgmList(i), "NOPRCVALUE", caller
        Else
          'Attenzione: MBR==PROC
          'Quelli che per noi sono MEMBER (no instestazione JOB/PROC)
          'possono essere utilizzati con "INCLUDE" o "EXEC" (proc!)
          isTrovato = False
          If UBound(arrProc) Then
            For a = 1 To UBound(arrProc)
              If arrProc(a) = pgmList(i) Then
                isTrovato = True
                Exit For
              End If
            Next a
          End If
          If Not isTrovato Then
            Set rsOggetti = m_Fun.Open_Recordset("select * from bs_oggetti where nome = '" & pgmList(i) & "' and (tipo = 'PRC' or tipo = 'MBR')")
            If Not rsOggetti.EOF Then
              If rsOggetti!Tipo = "MBR" Then
                changeTypeObj rsOggetti!idOggetto, "PRC", "COMMAND", "PROCEDURE"
              End If
              AggiungiRelazione rsOggetti!idOggetto, "PRC", caller, pgmList(i)
            Else
              If Not ExistObjSyst(pgmList(i), "PRC") Then
                'AggiungiSegnalazione calledModule, "NOPRC", calledModule
                addSegnalazione pgmList(i), "NOPRC", pgmList(i)
              Else
                AggiungiComponente pgmList(i), pgmList(i), "PRC", "SYSTEM"
              End If
            End If
            rsOggetti.Close
          Else
            ' � una PROC interna
            addSegnalazione pgmList(i), "INT-PROC", pgmList(i)
          End If
        End If
        'stefano: a che serve a fine sub?
        'Set rsOggetti = m_Fun.Open_Recordset("select * from bs_oggetti where nome = '" & pgmList(i) & "' and (tipo = 'PRC' or tipo = 'MBR')")
        Set rsOggetti = m_Fun.Open_Recordset("select * from bs_oggetti where nome = '" & pgmList(i) & "' and tipo = 'PRC'")
      Else
        'SQ ?
      End If
    End If
    
  Next i
  'colpa di mauro
  parameters = statement
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Utility DSNMTV01 di esecuzione PGM + schedulazione PSB
' statement � il contenuto del dsn DDITV02...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseDSNMTV01(statement As String, psbName As String, stepNumber As Integer)
  Dim rs As Recordset
  Dim k As Integer, y As String
  Dim idpgm As Long
  Dim PGM As String
  Dim idOggettoOrig As Long
  Dim wRecIn As String
  Dim parameters() As String
  
  parameters = Split(statement, ",")
  If UBound(parameters) = 8 Then
    'nono parametro
    PGM = Trim(parameters(8))
  Else
    'segnalazione!
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''
  ' TMP: COPIATO (piccole modifiche) DA checkSchedPSB... farne una!!!
  ''''''''''''''''''''''''''''''''''''''''''''
  'AggiungiRelazione/Segnalazione lavora sul GbidOggetto, ma qui ho member che e' diverso...
  idOggettoOrig = GbIdOggetto

  ''''''''''''''''
  'Controllo PGM:
  ''''''''''''''''
  'Se ho valori variabili non me ne faccio niente...
  If InStr(PGM, "&") = 0 Then
    ''''''''''''''''''''''''''''''''''
    ' SQ 15-02-06
    ' Gestione PsLoad
    ''''''''''''''''''''''''''''''''''
    Parser.PsConnection.Execute "INSERT INTO PsJCL_Psb (idOggetto,stepNumber,Utility,PSB,PGM) " & _
                                "VALUES(" & GbIdOggetto & ",'" & stepNumber & "','DSNMTV01','" & psbName & "','" & PGM & "')"

    'OLD:
    'Set rs = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where nome = '" & pgm & "' and (tipo = 'CBL' or tipo = 'ASM' )")
    'NEW:
    Set rs = m_Fun.Open_Recordset("select idOggetto,tipo from bs_oggetti where nome = '" & PGM & "' and tipo in ('CBL','ASM','PLI')")
    If rs.RecordCount = 0 Then
      'Nome LOAD diverso da nome SOURCE: aggiorniamo il recordset
      Set rs = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.load = '" & PGM & "' and b.source=a.nome and a.tipo in ('CBL','ASM','PLI')")
    End If
    
    If rs.RecordCount Then  'rs potenzialmente diverso da quello sopra...
      'AggiungiRelazione rs!IdOggetto, "PGM", pgm, pgm
      AggiungiRelazione rs!idOggetto, "CALL", PGM, PGM
      'diventa lui...
      GbIdOggetto = rs!idOggetto
    Else
      ''''''''''''''''''''''''''''''''''''''''''''''
      ' UTILITY?
      ' Es: DFSUDMP0,DFSUCF00,DFSSBHD0,...
      ''''''''''''''''''''''''''''''''''''''''''''''
      If ExistObjSyst(PGM, "PGM") Then 'Presente in "psUte_ObjSys"
        'Aggiungi alla "PsCom_Obj"
        AggiungiComponente PGM, PGM, "UTL", "SYSTEM"
      Else
        'AggiungiSegnalazione PGM, "NOCALL", PGM
        addSegnalazione PGM, "NOCALL", PGM
      End If
    End If
    rs.Close
  End If
  ''''''''''''''''
  'Controllo PSB:
  ''''''''''''''''
  'Se ho valori variabili non me ne faccio niente...
  Dim IdPsb As Long
  If InStr(psbName, "&") = 0 Then
    Set rs = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where tipo = 'PSB' and nome = '" & psbName & "'")
      If rs.RecordCount Then
        IdPsb = rs!idOggetto
      Else
        'Restore
        GbIdOggetto = idOggettoOrig
        'AggiungiSegnalazione psbName, "NOPSB-JCL", psbName
        addSegnalazione psbName, "NOPSB-JCL", psbName
      End If
    rs.Close
  End If
  ''''''''''''''''''''''''''
  ' Relazione PGM/PSB
  ''''''''''''''''''''''''''
  If GbIdOggetto <> idOggettoOrig And IdPsb <> 0 Then 'tutto OK:
    AggiungiRelazione IdPsb, "PSB-JCL", GbNomePgm, psbName
  End If
  'Restore
  GbIdOggetto = idOggettoOrig

End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' Input: statement DD (JCL)
' Result: contenuto SK (scheda parametro su FILE)
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Function getSKparam(statement As String, Optional IsPGM As Boolean = False) As String
  Dim skName As String, skFile As String, Line As String
  Dim rs As Recordset
  Dim k As Integer, i As Integer
  Dim FD As Long
  Dim skNames() As String
 
  On Error GoTo errSK

  skName = SelezionaParam(statement, "DSN=")
  k = InStr(skName, ".")
  While k > 0
    skName = Right(skName, Len(skName) - k)
    k = InStr(skName, ".")
  Wend

  k = InStr(skName, "(")
  If k Then  'posso avere il membro definito tra parentesi
    skName = Mid(skName, k + 1)
    skName = Left(skName, InStr(skName, ")") - 1)
  End If
  
  If InStr(skName, "&") Then solveParameters skName
  skNames = Split(skName & "@", "@")
  For i = 0 To UBound(skNames) - 1

    If InStr(skNames(i), "&") = 0 Then
      'Cerco un PRM
      Set rs = m_Fun.Open_Recordset("select idOggetto,Nome,Directory_Input,Estensione,tipo,Area_Appartenenza,livello1 from BS_Oggetti where " & _
                                    "NOME = '" & skNames(i) & "' and tipo in ('PRM','ERL','SCP','REX','SAS')")
      If rs.RecordCount = 0 Then
        'Cerco un UNK
        Set rs = m_Fun.Open_Recordset("select idOggetto,Nome,Directory_Input,Estensione,tipo,Area_Appartenenza,livello1 from BS_Oggetti where " & _
                                      "NOME = '" & skNames(i) & "' and tipo ='UNK'")
        If rs.RecordCount Then
          '''''''''''''''''''
          ' "FORZATURA" TIPO
          ' prendiamo solo il primo
          '''''''''''''''''''
          rs!Tipo = "PRM"
          rs!Area_Appartenenza = "COMMAND"
          rs!livello1 = "SK-PARAM"
          rs.Update
          AggiungiRelazione rs!idOggetto, "PRM", GbNomePgm, skNames(i)
        Else
          'SKEDA non presente nel progetto
          addSegnalazione skNames(i), "NOSK", skNames(i)
          rs.Close
          Exit Function
        End If
      Else
        ' Ho trovato la Sk Param
        AggiungiRelazione rs!idOggetto, "PRM", GbNomePgm, skNames(i)
      End If
    Else
      ' Mauro 14/03/2008
      If IsPGM Then
        addSegnalazione skNames(i), "NOPGMVALUE", skNames(i)
      Else
        addSegnalazione skName, "NOSKVALUE", skNames(i)
      End If
    End If
    
    ''''''''''''''''''''''''''''''''''''''''''''''''
    ' Parsing Skeda Parametro
    ''''''''''''''''''''''''''''''''''''''''''''''''
    '''''Set TbOggetti = m_Fun.Open_Recordset("select Nome,Directory_Input,Estensione from BS_oggetti where idoggetto = " & rs!idOggetto)
    If Trim(rs!estensione) = "" Then
      skFile = m_Fun.FnPathDef & Mid(rs!Directory_Input, 2) & "\" & rs!nome
    Else
      skFile = m_Fun.FnPathDef & Mid(rs!Directory_Input, 2) & "\" & rs!nome & "." & rs!estensione
    End If
    rs.Close
  
    FD = FreeFile
    Open skFile For Input As FD
    While Not EOF(FD)
      Line Input #FD, Line
      CalcolaLOCSJcl Line
      getSKparam = getSKparam & " " & Left(Line, 72)
    Wend
    Close FD
  Next i
  
  Exit Function
errSK:
  'gestire...
  m_Fun.writeLog "getSKparam: id#" & GbIdOggetto & ", err: " & err.description & "###"
  'Resume
End Function
''''
'''Private Function parseIKJEFT1B(fileIn)
'''  'Per ora le trattiamo allo stesso modo...
'''  parseIKJEFT1B = parseIKJEFT01(fileIn)
'''End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Schedulazione BATCH dei PSB:
' a) UTILITY di sistema DFSRRC00 ("diretta",con "DSNMTV01")
' b) Modalit� (NON sicura) PARM=..."PSB=" "MBR="
''
' Ritorna il PSB e modifica "program" se pgm=DSNMTV01
''
' 15-02-06: gestione PsLoad
' 13-03-06: gestione DSNMTV01 - SCHEDULAZIONE diversa!
' 27-09-06: gestione QUIKJOB: modifica "program"...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function checkSchedPSB(program As String, statement As String, stepNumber As Integer) As String
  Dim rs As Recordset
  Dim i As Integer
  Dim token As String, psb As String, PGM As String, parameter As String
  Dim parameters() As String
  Dim idOggettoOrig As Long
  Dim ok As Boolean
  'spli
  Dim psbList() As String
  Dim pgmList() As String
  
  On Error GoTo errPsb
  
  ''''''''''''''''''''''''''''''
  ' Ricerca PGM,PSB
  ''''''''''''''''''''''''''''''
  Do While Len(statement)
'    token = nextToken_tmp(statement)
    'token = nextToken_newnew(statement)
    token = nextToken_Mauro(statement)
    Select Case Left(token, 4)
      Case "PSB="
        psb = Mid(token, 5)
        If Len(PGM) Then ok = True  'caso valido: devo avere sia MBR che PGM
      Case "MBR="
        PGM = Mid(token, 5)
        If Len(psb) Then ok = True  'caso valido: devo avere sia MBR che PGM
      Case "PARM"
        'Chiamata diretta all'utility DFSRRC00:
        'spli: boh, Matteo pensaci tu....
        'If token = "PARM=" And (program = "DFSRRC00" Or program = "BMPRRC00" Or program = "TOYRRC00") Then
        'If token = "PARM=" And (program = "DFSRRC00" Or program = "PGM=DFSRRC00" Or program = "BMPRRC00" Or program = "TOYRRC00") Then
        If token = "PARM=" And (program = "DFSRRC00" Or program = "PGM=DFSRRC00" _
           Or program = "BMPRRC00" Or program = "TOYRRC00" Or program = "FABHX034") Then
          'parameter = nextToken_tmp(statement)
          parameter = nextToken_newnew(statement)
          'SQ 6-09-07 LA MODIFICA SOPRA HA INTRODOTTO CASINI! MANTIENE GLI APICI!
          'If Left(parameter, 1) = "(" Then
          If Left(parameter, 1) = "(" Or Left(parameter, 1) = "'" Then
            parameter = Trim(Mid(parameter, 2, Len(parameter) - 2))
            If Left(parameter, 1) = "'" Then
              If Right(parameter, 1) = "'" Then
                parameter = Mid(parameter, 2, Len(parameter) - 2)
              End If
            End If
          End If
          parameters() = Split(parameter, ",")
          If UBound(parameters) >= 2 Then
            ok = True
            PGM = parameters(1)
            psb = parameters(2)
            'SQ tmp
            If program = "TOYRRC00" Then
              'Parametri di questo tipo: <PSBNAME>*L*
              If (InStr(psb, "*")) Then
                psb = Left(psb, InStr(psb, "*") - 1)
              End If
            End If
          End If
        End If
      Case ""
        'COMMENTO: statement inizia con space (e nextToken ritorna "")
        'Braso la riga
        statement = ""
    Case Else
      'trovato COMMENTO (".") o quello che cercavo (MBR,PSB)
      If token = "." Or (Len(PGM) > 0 And (Len(psb) > 0)) Then
        'Esco dal ciclo:
        statement = ""
      End If
    End Select
  Loop
  
  If ok Then
    'AggiungiRelazione/Segnalazione lavora sul GbidOggetto, ma qui ho member che e' diverso...
    'spli
    'idOggettoOrig = GbIdOggetto
  
    ''''''''''''''''
    'Controllo PGM:
    ''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''
    ' Gestione Variabili
    ''
    ' SQ 20-10-06
    ''''''''''''''''''''''''''''''''''''''''
    'spli
    If PGM = "GO" Then PGM = GOModule
    'If InStr(PGM, "&") Then solveParameters PGM
    ReDim pgmList(0)
    If InStr(PGM, "&") Then
    'stefano: grande forzatura!!! forse � un problema di load?
      If Len(GOModule) Then PGM = GOModule
      pgmList = getParameters(PGM)
    Else
      pgmList(0) = PGM
    End If
    
    ' Mauro 14/03/2008
    ReDim psbList(0)
    If InStr(psb, "&") Then
      psbList = getParameters(psb)
    Else
      psbList(0) = psb
    End If
    
    'spli: per ora cambio, anche perch� le tabelle venivano riempite con valori
    ' non utili (nomi di variabili); se non va, torno indietro
'    If InStr(PGM, "&") = 0 Then
'      '''''''''''''''''''''''''''''''''''''''''''
'      ' 13-03-06
'      ' DSNMTV01: proseguo l'analisi in parseJcl
'      ' 27-09-06: idem per QUIKIMS
'      ' 16-10-06:  idem per EZPLUS00 (Easytrieve)
'      '''''''''''''''''''''''''''''''''''''''''''
'      If PGM = "DSNMTV01" Or PGM = "QUIKIMS" Or PGM = "EZTPA00" Then
'        checkSchedPSB = psb
'        program = PGM
'        Exit Function
'      End If
'
'      ' Gestione PsLoad
'      Set rs = m_Fun.Open_Recordset("select idOggetto,tipo from bs_oggetti where nome = '" & PGM & "' and tipo in ('CBL','ASM','PLI')")
'      If rs.RecordCount = 0 Then
'        Set rs = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.load = '" & PGM & "' and b.source=a.nome and a.tipo in ('CBL','ASM','PLI')")
'      End If
'      If rs.RecordCount Then  'rs potenzialmente diverso da quello sopra...
'        ''''''''''''''''''''''''''''''''
'        '
'        ''''''''''''''''''''''''''''''''
'        'stefano: a che serve questa tabella? Chi la usa?
'        ' nei casi di psb variabile non pu� servire se non viene risolta la variabile con gli
'        ' N valori
'        Parser.PsConnection.Execute "INSERT INTO PsJCL_Psb (idOggetto,stepNumber,Utility,PSB,PGM) " & _
'                                    "VALUES(" & GbIdOggetto & ",'" & stepNumber & "','" & program & "','" & psb & "','" & PGM & "')"
'        'AggiungiRelazione rs!IdOggetto, "PGM", pgm, pgm
'        AggiungiRelazione rs!idOggetto, "CALL", PGM, PGM
'
'        'diventa lui...
'        GbIdOggetto = rs!idOggetto
'      Else
'        ''''''''''''''''''''''''''''''''''''''''''''''
'        ' UTILITY?
'        ' Es: DFSUDMP0,DFSUCF00,DFSSBHD0,DFSURDB0,DFSURGL0,...
'        ''''''''''''''''''''''''''''''''''''''''''''''
'        If ExistObjSyst(PGM, "PGM") Then 'Presente in "psUte_ObjSys"
'          'Aggiungi alla "PsCom_Obj"
'          AggiungiComponente PGM, program, "UTL", "SYSTEM"
'          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'          ' IGNORO IL PSB:
'          ' IPOTESI: A PARTE L'UTIL DSNMTV01, LE ALTRE NON SCHEDULANO.
'          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'          Exit Function
'        Else
'          ''''''''''''''''''''''''''''''''
'          '
'          ''''''''''''''''''''''''''''''''
'          Parser.PsConnection.Execute "INSERT INTO PsJCL_Psb (idOggetto,stepNumber,Utility,PSB,PGM) " & _
'                                    "VALUES(" & GbIdOggetto & ",'" & stepNumber & "','" & program & "','" & psb & "','" & PGM & "')"
'          addSegnalazione PGM, "NOCALL", PGM
'          'SQ: perdiamo la relazione!!!!!! gestire bene...
'          GbIdOggetto = 0
'        End If
'      End If
'      rs.Close
'    Else
'      addSegnalazione PGM, "NOPGMVALUE", PGM
'    End If

'spli: nuovo pezzo
    If Len(pgmList(0)) Then
      For i = 0 To UBound(pgmList)
      '''''''''''''''''''''''''''''''''''''''''''
      ' 13-03-06
      ' DSNMTV01: proseguo l'analisi in parseJcl
      ' 27-09-06: idem per QUIKIMS
      ' 16-10-06:  idem per EZPLUS00 (Easytrieve)
      '''''''''''''''''''''''''''''''''''''''''''
        If pgmList(i) = "DSNMTV01" Or pgmList(i) = "QUIKIMS" Or pgmList(i) = "EZTPA00" Or pgmList(i) = "SASESA" Then  '<--- SASESA (TILVIA)
          ' Mauro 14/03/2008
          'checkSchedPSB = psb
          checkSchedPSB = IIf(UBound(psbList) >= i, psbList(i), psbList(0))
          program = pgmList(i)
          'SQ Exit Function 'SQ errore! Non exit function!! ho un array da ciclare!!!
          Dim gotoNext As Boolean
          gotoNext = True
        Else
          gotoNext = False
        End If
        If Not gotoNext Then
        ' Gestione PsLoad
        Set rs = m_Fun.Open_Recordset("select idOggetto,tipo from bs_oggetti where nome = '" & pgmList(i) & "' and tipo in ('CBL','ASM','PLI','EZT')") 'SQ 2012
        If rs.RecordCount = 0 Then
          Set rs = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.load = '" & pgmList(i) & "' and b.source=a.nome and a.tipo in ('CBL','ASM','PLI','EZT')") 'SQ 2012
        End If
        If rs.RecordCount Then  'rs potenzialmente diverso da quello sopra...
          ''''''''''''''''''''''''''''''''
          '
          ''''''''''''''''''''''''''''''''
          'stefano: a che serve questa tabella? Chi la usa?
          ' nei casi di psb variabile non pu� servire se non viene risolta la variabile con gli
          ' N valori.
          ' L'ho cambiata, ho anche modificato la chiave, tanto nessuno la usa all'interno del tool, solo sulle print;
          'SPOSTATA DENTRO IL CICLO DEI PSB
'          Parser.PsConnection.Execute "INSERT INTO PsJCL_Psb (idOggetto,stepNumber,Utility,PSB,PGM) " & _
'                                      "VALUES(" & GbIdOggetto & ",'" & stepNumber & "','" & program & "','" & psb & "','" & pgmList(i) & "')"
          'AggiungiRelazione rs!IdOggetto, "PGM", pgm, pgm
          AggiungiRelazione rs!idOggetto, "CALL", pgmList(i), pgmList(i)
          'diventa lui...
          'spli
          'GbIdOggetto = rs!idOggetto
        Else
          ''''''''''''''''''''''''''''''''''''''''''''''
          ' UTILITY?
          ' Es: DFSUDMP0,DFSUCF00,DFSSBHD0,DFSURDB0,DFSURGL0,...
          ''''''''''''''''''''''''''''''''''''''''''''''
          If ExistObjSyst(pgmList(i), "PGM") Then 'Presente in "psUte_ObjSys"
            'Aggiungi alla "PsCom_Obj"
            AggiungiComponente pgmList(i), program, "UTL", "SYSTEM"
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' IGNORO IL PSB:
            ' IPOTESI: A PARTE L'UTIL DSNMTV01, LE ALTRE NON SCHEDULANO.
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Exit Function
          Else
            ''''''''''''''''''''''''''''''''
            '
            ''''''''''''''''''''''''''''''''
            'stefano: vedi commento sopra
'            Parser.PsConnection.Execute "INSERT INTO PsJCL_Psb (idOggetto,stepNumber,Utility,PSB,PGM) " & _
'                                      "VALUES(" & GbIdOggetto & ",'" & stepNumber & "','" & program & "','" & psb & "','" & PGM & "')"
            If (pgmList(i) <> "DUMMY" Or (pgmList(i) = "DUMMY" And UBound(pgmList) = 0)) And Not pgmList(i) = "" Then
              addSegnalazione pgmList(i), "NOCALL", pgmList(i)
            End If
            'SQ: perdiamo la relazione!!!!!! gestire bene...
            'spli: nooo, crea legami con idoggetto = 0
            '''''''''GbIdOggetto = 0
          End If
        End If
        rs.Close
        End If
      Next
    Else
      addSegnalazione PGM, "NOPGMVALUE", PGM
    End If
    
    ''''''''''''''''
    'Controllo PSB:
    ''''''''''''''''
    Dim IdPsb As Long
    
    ''''''''''''''''''''''''''''''''''''''''
    ' Gestione Variabili
    ''
    ' SQ 20-10-06
    ''''''''''''''''''''''''''''''''''''''''
    If Len(psbList(0)) Then
      For i = 0 To UBound(psbList)
      'spli
        IdPsb = 0
        'stefano: meglio segnalarlo se � uno solo
        'If Not psbList(i) = "DUMMY" And Not psbList(i) = "" Then
        If (psbList(i) <> "DUMMY" Or (psbList(i) = "DUMMY" And UBound(psbList) = 0)) And Not psbList(i) = "" Then
           Set rs = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where tipo = 'PSB' and nome = '" & psbList(i) & "'")
            If rs.RecordCount Then
              IdPsb = rs!idOggetto
            Else
            'Restore
            'spli
              'GbIdOggetto = idOggettoOrig
              addSegnalazione psbList(i), "NOPSB-JCL", GbNomePgm
            End If
          rs.Close
        End If
      
        ''''''''''''''''''''''''''
        ' Relazione PGM/PSB
        ''''''''''''''''''''''''''
        'spli
        'If GbIdOggetto <> idOggettoOrig And idPsb <> 0 Then 'tutto OK:
        ' Secondo il PM non serve fatta in questo modo
          'If idPsb <> 0 Then
          '   AggiungiRelazione idPsb, "PSB-JCL", GbNomePgm, psbList(i)
          'End If
        'Else
        '  If GbIdOggetto = 0 Then
            'NON HO IL PROGRAMMA... PERDO LA RELAZIONE!
            '...unkUnk
        '  End If
        'End If
'spli
        If Len(pgmList(0)) Then
        'spli: notare che se le due tabelle non coincidono, usa il programma alla posizione 1
        'non gestisce l'inverso: cio� psb costante e nome programma multiplo
           If Len(psbList(i)) Then
             Parser.PsConnection.Execute "INSERT INTO PsJCL_Psb (idOggetto,stepNumber,Utility,PSB,PGM) " & _
                   "VALUES(" & GbIdOggetto & ",'" & stepNumber & "','" & program & "','" & psbList(i) & "','" & IIf(UBound(pgmList) >= i, pgmList(i), pgmList(0)) & "')"
           End If
        ' Gestione PsLoad
          Set rs = m_Fun.Open_Recordset("select idOggetto,tipo from bs_oggetti where nome = '" & IIf(UBound(pgmList) >= i, pgmList(i), pgmList(0)) & "' and tipo in ('CBL','ASM','PLI')")
          If rs.RecordCount = 0 Then
            Set rs = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.load = '" & IIf(UBound(pgmList) >= i, pgmList(i), pgmList(0)) & "' and b.source=a.nome and a.tipo in ('CBL','ASM','PLI')")
          End If
          If rs.RecordCount Then  'rs potenzialmente diverso da quello sopra...
            idOggettoOrig = GbIdOggetto
            GbIdOggetto = rs!idOggetto
            'AggiungiRelazione IdPsb, "PSB-JCL", GbNomePgm, psbList(i)
            ' Mauro 11/03/2008 : Aggiunge la relazione anche se l'IdPsb � a 0
            If IdPsb = 0 Then
              addSegnalazione psbList(i), "NOPSB-JCL", GbNomePgm
            Else
              AggiungiRelazione IdPsb, "PSB-JCL", GbNomePgm, psbList(i)
            End If
            GbIdOggetto = idOggettoOrig
          End If
          rs.Close
        End If
      Next
    Else
      'Restore
      'GbIdOggetto = idOggettoOrig
    'spli: ma "i" che valore aveva fuori dal ciclo????
      'addSegnalazione elencoparam(i), "NOPSBVALUE", elencoparam(i)
      addSegnalazione psb, "NOPSBVALUE", psb
    End If
    'Restore
    'GbIdOggetto = idOggettoOrig
  End If

  Exit Function
errPsb:
  'gestire...
  If err.Number = 123 Then
    'Parentesi sbilanciata...
    If token = "PARM=" Then
      statement = ""
      Resume Next
    Else
      ' Non � ancora stato valorizzato idOggettoOrig
      'If idOggettoOrig Then
      '  GbIdOggetto = idOggettoOrig
      'End If
      err.Raise 234, "checkSchedPSB", "checkSchedPSB: syntax error on: " & statement
    End If
   
  'spli: sarebbe meglio leggerlo...
  ElseIf err.Number = -2147467259 Then
    Resume Next
  Else
    'GbIdOggetto = idOggettoOrig
    err.Raise 234, "checkSchedPSB", "checkSchedPSB: syntax error on: " & statement
  End If
End Function
'''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''
Sub parseJobDD(statement As String, ddname As String, stepName As String, stepNumber As Integer, pgmName As String)
  
  Dim wStr1 As String, disp As String, dsn As String, fileName As String
  Dim i As Integer
  Dim idArchivio As Long
  Dim rs As Recordset, rsTracciati As Recordset
  Dim rsVSAM As Recordset
  
  On Error GoTo errdb

  disp = SelezionaParam(statement, "DISP=")
  dsn = SelezionaParam(statement, "'")
  If Len(dsn) = 0 Then
       ' non ho il dsn ma solo il ddname vederec se vsam
  End If
  dsn = Replace(dsn, "'", "")
  Select Case dsn
    Case "SORTWK", "SORTWK1", "JOBLIB", "SYSDUMP", "SYSUDUMP"
      'nop
    Case Else
      'If Left(dsn, 1) <> "&" And InStr(ddname, "SORTWK") = 0 Then
      If InStr(dsn, "&") = 0 And InStr(ddname, "SORTWK") = 0 Then
        ''''''''''''''''''''''''''''''''''''''''''''
        ' INSERIMENTO PsRpsrel_file
        ''''''''''''''''''''''''''''''''''''''''''''
        i = InStr(dsn, "(")
        If i > 0 Then dsn = Left(dsn, i - 1)
        
        dsn = Replace(dsn, ",", "")
        Set rs = m_Fun.Open_Recordset("select * from psrel_file where idoggetto = " & GbIdOggetto)
        If rs.RecordCount Then
          Dim nFile As Integer
          rs.MoveLast 'mi serve l'ultimo progressivo... (nFile)
          nFile = rs!nFile + 1
        Else
          nFile = 1
        End If
        rs.AddNew
        rs!idOggetto = GbIdOggetto
        rs!nFile = nFile
        rs!Type = "JCL"
        rs!nstep = stepNumber
        rs!Step = stepName
        rs!program = "VSE"
        rs!dsnName = dsn
        rs!LINK = ddname
        rs!use = disp
        rs!Update = False 'cos'�???????????????
        rs.Update
        rs.Close
        ''''''''''''''''''''''''''''''''''''''''''''
        ' AGGIORNAMENTO DMVSM_Archivio
        ''''''''''''''''''''''''''''''''''''''''''''
        ' Mauro 06/08/2007 : cambio i flag con un campo Type
        'Dim isGDG As Boolean, isSEQ As Boolean, isKSDS As Boolean
        Dim isGDG As Boolean
        Dim wFileType As String
        
        i = InStr(statement, ",VSAM")
        If i > 0 Then
          'isKSDS = True
          wFileType = "KSDS"
        Else
          'isSEQ = True
          wFileType = "Seq"
        End If
        
        Set rs = m_Fun.Open_Recordset("select * from DMVSM_Archivio where nome = '" & dsn & "' ")
        If rs.RecordCount = 0 Then
          rs.AddNew
          'rs!data_creazione = Now
          rs!idorigine = GbIdOggetto
          rs!Tag = "Generated by JCL"
          rs!nome = dsn
          rs!VSam = "VSM_" & rs!idArchivio
          ' Mauro 06/08/2007 : cambio i flag con un campo Type
          'rs!KSDS = isKSDS
          'rs!Seq = isSEQ
          rs!Type = wFileType
          rs!gdg = isGDG
          rs!recform = "U"
          rs!recsize = 0
          rs!MinLen = 0
          rs!MaxLen = 0
          rs!KeyStart = 0
          rs!KeyLen = 0
          Set rsVSAM = m_Fun.Open_Recordset("select * from DMVSM_NomiVSAM where NomeVSAM = '" & rs!VSam & "'")
          If rsVSAM.RecordCount = 0 Then
            rsVSAM.AddNew
          End If
          rsVSAM!NomeVSAM = rs!VSam & ""
          rsVSAM!Acronimo = rs!VSam & ""
          rsVSAM!to_migrate = True
          rsVSAM!obsolete = False
          rsVSAM.Update
          rsVSAM.Close
        Else
          'rs!data_modifica = Now
          ' Mauro 06/08/2007 : cambio i flag con un campo Type
          'rs!KSDS = isKSDS
          'rs!Seq = isSEQ
          rs!Type = wFileType
          rs!gdg = isGDG
        End If
        idArchivio = rs!idArchivio 'contatore automatico...
        rs.Update
        rs.Close
      End If
  End Select
  Exit Sub
errdb:
  'gestire...
  MsgBox err.description
  'Resume
End Sub

Sub ParserJclProcPgm(wRecord, w_Step, wProgram)
  Dim k As Integer
  Dim wStr1 As String
  Dim wType As String
  Dim wNomeOgg As String
  Dim tb As Recordset
  
  k = InStr(wRecord, "EXEC ")
  
  If k = 0 Then Exit Sub
  If Mid(wRecord, k - 1, 1) <> "/" And Mid(wRecord, k - 1, 1) <> " " Then
    Exit Sub
  End If
  
  'SQ 27-02-06
  GbOldNumRec = GbNumRec
  wStr1 = LTrim(Mid(wRecord, k + 5))
  If GbTipoInPars = "JCL" Then
    wType = "PRC"
    w_Step = Trim(Mid$(wRecord, 1, k - 1))
    w_Step = Replace(w_Step, "/", "")
    w_Step = Replace(w_Step, "/", "")
  End If
  If GbTipoInPars = "JOB" Then
    wType = "PGM"
    w_Step = ""
  End If
  
  k = InStr(wStr1, ",")
  If k = 0 Then k = InStr(wStr1, " ")
  
  If k = 0 Or k > 9 Then
    k = InStr(wStr1, ",")
    If k = 0 Or k > 9 Then
      k = InStr(wStr1, "(")
    End If
  End If
  If k = 0 Then k = Len(wStr1) + 1
  wNomeOgg = Mid$(wStr1, 1, k - 1)
  k = InStr(wNomeOgg, ",")
  If k > 0 Then wNomeOgg = Mid$(wNomeOgg, 1, k - 1)
  k = InStr(wNomeOgg, "=")
  If k > 0 Then
    wStr1 = Mid$(wNomeOgg, 1, k - 1)
    wNomeOgg = Mid$(wNomeOgg, k + 1)
    Select Case wStr1
      Case "PROC"
        wType = "PRC"
      Case "PGM"
        wType = "PGM"
    End Select
  End If
  k = InStr(wNomeOgg, " ")
  If k > 0 Then wNomeOgg = Mid$(wNomeOgg, 1, k - 1)
  
  If Mid$(wNomeOgg, 1, 1) = "&" Then wNomeOgg = Mid$(wNomeOgg, 2)
  
  If wType = "PRC" Then
    ' Mauro 18/03/2008 : Devo ricercare anche tra gli UNKNOWN, e, in caso, cambiargli il tipo!!
    'Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where nome = '" & wNomeOgg & "' and (tipo = 'PRC' or tipo = 'JCL' )")
    Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where nome = '" & wNomeOgg & "' and tipo in ('PRC','JCL','UNK')")
    If tb.RecordCount > 0 Then
      If tb!Tipo = "UNK" Then
        tb!Tipo = "PRC"
        tb!Area_Appartenenza = "COMMAND"
        tb!livello1 = "PROCEDURE"
        tb.Update
        'Mauro (Madrid)
        updateTreeViewTable tb!idOggetto
        Carica_TreeView_Progetto Parser.PsObjTree
      End If
      AggiungiRelazione tb!idOggetto, "PRC", wNomeOgg, wNomeOgg
    Else
      If Not ExistObjSyst(wNomeOgg, "PRC") Then
        addSegnalazione wNomeOgg, "NOPRC", wNomeOgg
      Else
        AggiungiComponente wNomeOgg, wNomeOgg, "PRC", "SYSTEM"
      End If
    End If
  End If
  If wType = "PGM" Then
    wProgram = wNomeOgg
    If ExistObjSyst(wNomeOgg, "PGM") Then
      AggiungiComponente wNomeOgg, wNomeOgg, "UTL", "SYSTEM"
    Else
      Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where nome = '" & wNomeOgg & "' and (tipo = 'CBL' or tipo = 'ASM' )")
      If tb.RecordCount = 1 Then
        AggiungiRelazione tb!idOggetto, "PGM", wNomeOgg, wNomeOgg
      Else
        If wType = "PRC" Then
          addSegnalazione wNomeOgg, "NOPRC", wNomeOgg
        Else
          addSegnalazione wNomeOgg, "NOCALL", wNomeOgg
        End If
      End If
    End If
  End If
End Sub

Sub ParserJmbProcPgm(wRecord)

Dim k As Integer
Dim wStr1 As String
Dim wType As String
Dim wNomeOgg As String
Dim tb  As Recordset

k = InStr(UCase(wRecord), "EXECPGM")
If k = 0 Then Exit Sub
k = InStr(UCase(wRecord), "PGMNAME=")
wStr1 = LTrim(Mid$(wRecord, k + 8))
wType = "PGM"

k = InStr(wStr1, " ")
If k = 0 Or k > 11 Then
   k = InStr(wStr1, ",")
   If k = 0 Or k > 9 Then
      k = InStr(wStr1, "(")
   End If
End If
If k = 0 Then k = Len(wStr1) + 1
wNomeOgg = Mid$(wStr1, 1, k - 1)
k = InStr(wNomeOgg, ",")
If k > 0 Then wNomeOgg = Mid$(wNomeOgg, 1, k - 1)
k = InStr(wNomeOgg, "=")
If k > 0 Then
   wStr1 = Mid$(wNomeOgg, 1, k - 1)
   wNomeOgg = Mid$(wNomeOgg, k + 1)
   Select Case wStr1
      Case "PROC"
         wType = "PRC"
      Case "PGM"
         wType = "PGM"
   End Select
End If
k = InStr(wNomeOgg, " ")
If k > 0 Then wNomeOgg = Mid$(wNomeOgg, 1, k - 1)
wNomeOgg = Replace(wNomeOgg, "'", "")

If Mid$(wNomeOgg, 1, 1) = "&" Then wNomeOgg = Mid$(wNomeOgg, 2)

If wType = "PRC" Then
   Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where nome = '" & wNomeOgg & "' and tipo = 'PRC'")
   If tb.RecordCount = 1 Then
      AggiungiRelazione tb!idOggetto, "PRC", wNomeOgg, wNomeOgg
   Else
      If Not ExistObjSyst(wNomeOgg, "PRC") Then
         addSegnalazione wNomeOgg, "NOPRC", wNomeOgg
      Else
         AggiungiComponente wNomeOgg, wNomeOgg, "PRC", "SYSTEM"
      End If
   End If
End If
If wType = "PGM" Then
      If ExistObjSyst(wNomeOgg, "PGM") Then
           AggiungiComponente wNomeOgg, wNomeOgg, "UTL", "SYSTEM"
      Else
           Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where nome = '" & wNomeOgg & "' and (tipo = 'CBL' or tipo = 'ASM' )")
           If tb.RecordCount = 1 Then
              AggiungiRelazione tb!idOggetto, "PGM", wNomeOgg, wNomeOgg
           Else
              If wType = "PRC" Then
                 addSegnalazione wNomeOgg, "NOPRC", wNomeOgg
              Else
                 addSegnalazione wNomeOgg, "NOCALL", wNomeOgg
              End If
           End If
      End If
End If
End Sub

Sub ParserJclPsb(wRecord, w_RunProgram, w_PsbName)
  Dim k As Integer
  Dim wNomePsb As String
  Dim wIdPgm As Variant
  Dim IdPsb As Long
  Dim wNomePgm As String
  Dim tb As Recordset
  Dim SaveIdOggetto As Variant
  Dim wStr1 As String
  Dim wNomeUty As String
  Dim wLine As String
  
  Dim rs As Recordset
  Dim wApp As String
  Dim wApp2 As String
  Dim s() As String
  Dim sApp() As String
  Dim i As Long
  Dim wFormato As String
  
  Dim bPsb As Boolean
  Dim bPgm As Boolean
  Dim bUty As Boolean
  Dim bExec As Boolean
  Dim bParm As Boolean
  Dim bMbr As Boolean
  
  Dim wDBType As String
  
  Dim sFrm() As String

  wNomePgm = ""
  wNomePsb = ""
  wNomeUty = ""
  wDBType = ""
  
  'Debug.Print wRecord

  wLine = getTemplate_Of_JCLline(CStr(wRecord))

  If wLine = "|" Then Exit Sub
  If wLine = "" Then Exit Sub

  'Controlla se il template � presente nella tabella sys
  Set rs = m_Fun.Open_Recordset_Sys("Select * From sysObjectParamStr Where Template = '" & wLine & "'")
  If rs.RecordCount > 0 Then
   wFormato = rs!ParamStr
   
   wApp = Replace(wRecord, ",", " ")
   wApp = Replace(wApp, "'", " ")
   wApp = Replace(wApp, "=", " ")

   s = Split(wApp, " ")
   ReDim sApp(0)
   
   For i = 0 To UBound(s)
      If Trim(s(i)) <> "" Then
         If Mid(Trim(s(i)), 1, 2) <> "//" Then
            sApp(UBound(sApp)) = s(i)
            ReDim Preserve sApp(UBound(sApp) + 1)
         End If
      End If
   Next i
   
   ReDim Preserve sApp(UBound(sApp) - 1)
   
   s = Split(wFormato, " ")
   
   'Filtra sApp
   ReDim sFrm(0)
   Dim j As Long
   Dim bfind As Boolean
   Dim cc As Long
   
   For i = 0 To UBound(s)
      bfind = False
      For j = 0 To UBound(sApp)
         If s(i) = sApp(j) Then
            bfind = True
            Exit For
         End If
      Next j
      If bfind Then
         sFrm(i) = sApp(j)
         cc = j + 1
      Else
         If cc <= UBound(sApp) Then
            sFrm(i) = sApp(cc)
            cc = cc + 1
         End If
      End If
         
      ReDim Preserve sFrm(UBound(sFrm) + 1)
   Next i
   
   ReDim Preserve sFrm(UBound(sFrm) - 1)
   wApp = ""
   For i = 0 To UBound(s)
      Select Case UCase(Trim(s(i)))
         Case "EXEC"
            bExec = True
         Case "IGN"
            'Ignora
         Case "PGM"
            If bExec Then
               bUty = True
            End If
         Case "PARM", "PAM"
            bParm = True
         Case "PSB"
            bPsb = True
         Case "MBR"
           bMbr = True
         Case Else
            wApp = sFrm(i)
      End Select
      
      If s(i) = "<PRC>" Then
         If bExec Then
            wNomeUty = wApp
            
            bExec = False
            bParm = False
         End If
      End If
      If s(i) = "<DB>" Then
         If bParm Then
            wDBType = wApp
         End If
      End If
      If s(i) = "<PGM>" Then
         If bParm Or bMbr Then
            wNomePgm = wApp
         End If
      End If
      If s(i) = "<PSB>" Then
         If bParm Or bPsb Then
            wNomePsb = wApp
         End If
      End If
   Next i
              
   bExec = False
   bParm = False
  End If

  rs.Close

  wIdPgm = 0
  IdPsb = 0

  If wNomePgm = "" And wNomeUty = "" Then Exit Sub

  'SG : Controlla se esiste un programma che si chiama come la utility
  Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where (tipo = 'CBL' or tipo = 'ASM' or tipo = 'PLI') and nome = '" & wNomeUty & "'")
  If tb.RecordCount > 0 Then
        wNomePgm = wNomeUty
        wNomeUty = ""
  End If
  tb.Close
  
  w_RunProgram = wNomePgm
  Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where (tipo = 'CBL' or tipo = 'ASM' or tipo = 'PLI') and nome = '" & wNomePgm & "'")
  If tb.RecordCount > 0 Then
    AggiungiRelazione tb!idOggetto, "CALL", wNomePgm, wNomePgm
    wIdPgm = tb!idOggetto
  Else
    If Trim(wNomePgm) <> "" Then
      Set tb = m_Fun.Open_Recordset("select * from psute_objsys where tipo = 'PSY' and nome = '" & wNomeUty & "' ")
      If tb.RecordCount = 0 Then
          addSegnalazione wNomePgm, "NOCALL", wNomePgm
      End If
    End If
  End If
  
  'SQ 29-11-05
  'sbaglia...
  If Trim(wNomePsb) <> "" Then
    Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where tipo = 'PSB' and nome = '" & wNomePsb & "'")
    If tb.RecordCount > 0 Then
       IdPsb = tb!idOggetto
    Else
      If Trim(wNomePsb) <> "" Then
         Set tb = m_Fun.Open_Recordset("select * from PsUte_objsys where tipo = 'PSB' and nome = '" & wNomePsb & "' ")
         If tb.RecordCount = 0 Then
            addSegnalazione wNomePsb, "NOPSB", wNomePsb
         Else
             IdPsb = -1
         End If
      End If
    End If
  End If

  w_PsbName = wNomePsb

  If wIdPgm > 0 Then
   SaveIdOggetto = GbIdOggetto
   GbIdOggetto = wIdPgm
   If IdPsb > 0 Then
      AggiungiRelazione IdPsb, "PSB", wNomePsb, wNomePsb
      wStr1 = "delete * from bs_segnalazioni where idoggetto = " & wIdPgm & " and codice = 'F07' "
      Parser.PsConnection.Execute wStr1
   End If
   GbIdOggetto = SaveIdOggetto
  Else
   If IdPsb = 0 Then
      If Trim(wNomePgm) <> "" And Trim(wNomePsb) <> "" Then
          AggiungiObjUnkUnk wNomePgm, wNomePsb, "PSB", wNomePsb
      End If
   End If
  End If

End Sub

Sub ParserJclRunPgm(wRecord, w_RunProgram)
  Dim k As Integer
  
  Dim wNomePlan As String
  Dim wIdPgm As Variant
  Dim wIdPsb As Variant
  Dim wNomePgm As String
  Dim tb As Recordset
  Dim SaveIdOggetto As Variant

  k = InStr(wRecord, "RUN ")
  If k > 0 Then
  '   Debug.Print wRecord
     k = InStr(wRecord, " PROGRAM")
     If k = 0 Then Exit Sub
     wNomePgm = Mid$(wRecord, k + 8)
  End If
  If wNomePgm = "" Then Exit Sub
  wNomePlan = ""
  
  k = InStr(wNomePgm, " PLAN")
  If k > 0 Then
     wNomePlan = Mid$(wNomePgm, k + 5)
     wNomePgm = Mid$(wNomePgm, 1, k - 1)
  End If
  k = InStr(wNomePlan, ")")
  If k > 0 Then
     wNomePlan = Mid$(wNomePlan, 1, k - 1)
  End If
  k = InStr(wNomePlan, "(")
  If k > 0 Then
     wNomePlan = Mid$(wNomePlan, k + 1)
  End If
  
  wNomePlan = Trim(wNomePlan)
  wNomePgm = Trim(wNomePgm)

  If Mid$(wNomePgm, 1, 1) = "(" Then
    wNomePgm = Mid$(wNomePgm, 2)
  End If
  k = InStr(wNomePgm, " ")
  If k > 0 Then
     wNomePgm = Mid$(wNomePgm, 1, k - 1)
  End If
  k = InStr(wNomePgm, ")")
  If k > 0 Then
     wNomePgm = Mid$(wNomePgm, 1, k - 1)
  End If
  
  wIdPgm = 0
  w_RunProgram = wNomePgm
  
  Set tb = m_Fun.Open_Recordset("select * from bs_oggetti where tipo = 'CBL' and nome = '" & wNomePgm & "'")
  If tb.RecordCount = 1 Then
     AggiungiRelazione tb!idOggetto, "CALL", wNomePgm, wNomePgm
     wIdPgm = tb!idOggetto
  Else
     If Not ExistObjSyst(wNomePgm, "PGM") Then
        addSegnalazione wNomePgm, "NOCALL", wNomePgm
     End If
  End If
  If Trim(wNomePlan) <> "" Then
      AggiungiComponente wNomePgm, wNomePlan, "PLN", "PLAN"
  End If
End Sub

'''''''''''''''''''''''''''''''''
'ROBERTO
'SQ 28-05-07 Modifiche "pesanti"
'''''''''''''''''''''''''''''''''
Sub parseJclSked(statement As String, ddname As String, stepName As String, stepNumber As Integer, pgmName As String)
  Dim wSk As String, wStr1 As String, disp As String, dsn As String, skName As String, FileSkeda As String, library As String
  Dim k As Integer, FD As Integer, i As Integer
  Dim idOggetto As Long
  Dim tb As Recordset, rsSked As Recordset
  Dim wRecIn As String, wNomePlan As String, Line As String, token As String, SKriga As String
  Dim program As String, skList() As String, ccList() As String
  Dim skFound As Boolean
  Dim Slash As Integer, Virgola As Integer
  Dim Directory As String
  Dim a As Integer
  On Error GoTo errdb

  dsn = SelezionaParam(statement, "DSN=")
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' gestione DUMMY
  ' SQ 28-05-07
  ' tmp...da gestire! (soprattutto caso valorizzazione DUMMY in JCL!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If Len(dsn) Then
    ReDim ccList(0)
    If Left(dsn, 1) = "&" Then
      ccList = getParameters(dsn)
      If Len(ccList(0)) = 0 Then
        addSegnalazione dsn, "NOSKVALUE", dsn
        ccList(0) = dsn
      End If
    Else
      ccList(0) = dsn
    End If
  
    For a = 0 To UBound(ccList)
      dsn = ccList(a)
      'Ricerca LIBRERIA/MEMBRO
      If InStr(dsn, "(") Then
        library = Left(dsn, InStr(dsn, "(") - 1)
      Else
        library = dsn
      End If
      
      ' TEMP
      ' Mauro 13/03/2008: Non posso eliminare tutto quello che c'� prima del punto
      '                   Potrebbero essere delle variabili, se precedute da "&", es: "&var1.001"
      k = InStr(dsn, ".")
      While k > 0
        For i = k - 1 To 1 Step -1
          If Mid(dsn, i, 1) = "&" Then
            Exit For
          ElseIf Mid(dsn, i, 1) = "." Then
            i = 0
            Exit For
          End If
        Next i
        If i = 0 Then
          dsn = Right(dsn, Len(dsn) - k)
          k = InStr(dsn, ".")
        Else
          k = InStr(k + 1, dsn, ".")
        End If
      Wend
    
      k = InStr(dsn, "(")
      If k Then  'posso avere il membro definito tra parentesi
        dsn = Mid(dsn, k + 1)
        dsn = Left(dsn, InStr(dsn, ")") - 1)
      Else
        'ho il punto dentro le parentesi... rimane solo la chiusa
        dsn = Replace(dsn, ")", "")
        If pgmName = "HPSVRUN1" Then
          addSegnalazione dsn, "NOSKVALUE", dsn
        End If
        Exit Sub
      End If
      'dsn = Replace(Replace(dsn, "(", ""), ")", "")
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Tabella PsJCL_Sked
    ''
    ' SQ - 28-05-07
    ' Attenzione, se non risolveva la variabile non inseriva
    ' niente in tabella! (era sotto, questo pezzo)
    ' E' giusto che qui vada inserito il DSN letto e NON quello risolto...
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'SQ 20-11-06 - Chiave sbagliata: idOggetto+step (casi di file "concatentati")
    'Gestire!
    On Error GoTo tmpErr
    ' Mauro 21/01/2009 : Posso avere pi� di un valore sullo stesso step
  '  Parser.PsConnection.Execute "INSERT INTO PsJCL_Sked (idOggetto,stepNumber,DSN,Library,Member) " & _
  '                              "  VALUES(" & GbIdOggetto & ",'" & stepNumber & "','" & ddname & "','" & library & "','" & dsn & "')"
    Set rsSked = m_Fun.Open_Recordset("select * from PsJcl_Sked where " & _
                                      "idoggetto = " & GbIdOggetto & " and stepNumber = " & stepNumber)
    rsSked.AddNew
    rsSked!idOggetto = GbIdOggetto
    rsSked!stepNumber = stepNumber
    rsSked!Ordinale = rsSked.RecordCount + 1
    rsSked!dsn = ddname
    rsSked!library = library
    rsSked!member = dsn
    rsSked.Update
    rsSked.Close
    
    On Error GoTo 0
    
    'tmp: valutare gestione "allacciamento jcl"
    If dsn = "DUMMY" Then
      Exit Sub
    End If
    
    skName = dsn
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Gestione Molteplicit� valori
    ' SQ 28-05-07
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    '
    'If InStr(skName, "&") Then solveParameters skName
    'If InStr(skName, "&") Then
    '  addSegnalazione skName, "NOSKVALUE", skName
    '  Exit Sub
    'End If
    
    ReDim skList(0)
    If InStr(dsn, "&") Then
      skList = getParameters(dsn)
      If Len(skList(0)) = 0 Then
        addSegnalazione dsn, "NOSKVALUE", dsn
        Exit Sub
      End If
    Else
      skList(0) = dsn
    End If
    
    For k = 0 To UBound(skList)
      'SQ attenzione: ho i duplicati nell'array! usare collection...!
      skName = skList(k)
      skFound = False
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'SQ 19-02-07
      ' Gestione programmi SAS:
      ' utility SAS820/913 lanciano pgm SAS specificati in SYSIN
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If pgmName <> "SAS820" And pgmName <> "SAS913" And pgmName <> "SASITSV" And pgmName <> "SASESA" And pgmName <> "SAS" And pgmName <> "SASPIU" _
                             And pgmName <> "SAS609" And pgmName <> "Y6SAS" And pgmName <> "SASXA1" Then    'SQ: gestione "specifica" sotto...
        'SQ utilizzare checkRelation
        Directory = Mid(library, InStrRev("." & library, "."))
        Set tb = m_Fun.Open_Recordset("select * from BS_Oggetti where " & _
                                      "NOME = '" & Replace(skName, "'", "") & "' AND directory_input like '%" & Replace(Directory, "'", "") & "%' AND " & _
                                      "tipo IN ('PRM','UNK','SCP','ERL','REX','SAS')")
        If tb.RecordCount = 0 Then
          tb.Close
          Set tb = m_Fun.Open_Recordset("select * from BS_Oggetti where " & _
                                        "NOME = '" & Replace(skName, "'", "") & "' AND tipo IN ('PRM','UNK','SCP','ERL','REX','SAS')")
        End If
        If tb.RecordCount Then
          Do Until tb.EOF
            skFound = True
            '''''''''''''''''''''''''''''''''''''''
            ' CAMBIAMENTO TIPO
            ' varie politiche... da valutare i vari casi
            '''''''''''''''''''''''''''''''''''''''
            Select Case tb!Tipo
              'SQ tmp - riclassifico anche i SAS... che sono un po' "precari"!
              Case "UNK", "SAS"
                tb!Tipo = "PRM"
                tb!Area_Appartenenza = "COMMAND"
                tb!livello1 = "SK-PARAM"
                tb.Update
                'Mauro (Madrid)
                updateTreeViewTable tb!idOggetto
                Carica_TreeView_Progetto Parser.PsObjTree
              Case "PRM", "SCP", "ERL", "REX" ', "SAS"
                'nop
                'Va Bene, e gia stata riconosciuta
                'If ddname = "SYSIN" Then ' non serve rianalizzare la skeda
                'If ddname = "SYSIN" Or ddname = "SYSIPT" Then ' non serve rianalizzare la skeda
                  'SICURI?????
                  'BASTA GUARDARE IL LIVELLO DEL PARSING SUL DB... verificare...
                  'SQ ora � ciclico:
                  'Exit Sub
                  'skFound = False 'attenzione: forzo il flag... ma pericoloso!
                'End If
              Case Else
                'nop
            End Select
              
            idOggetto = tb!idOggetto
            AggiungiRelazione idOggetto, "PRM", GbNomePgm, skName
            If tb!Tipo <> "PRM" Then
              SKriga = statement & " /*"
              '''''''''''''''''''''''''''Exit Sub
            Else
              ''''''''''''''''''''''''''''''''''''''''''''''''
              ' Lettura FILE:
              ''''''''''''''''''''''''''''''''''''''''''''''''
              Set TbOggetti = m_Fun.Open_Recordset("select Nome,Directory_Input,Estensione from BS_oggetti where idoggetto = " & idOggetto)
              If Trim(TbOggetti!estensione) = "" Then
                FileSkeda = m_Fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!nome
              Else
                FileSkeda = m_Fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!nome & "." & TbOggetti!estensione
              End If
              TbOggetti.Close
          
              FD = FreeFile
              Open FileSkeda For Input As FD
              While Not EOF(FD)
                Line Input #FD, Line
                SKriga = SKriga & " " & Line
              Wend
              Close FD
          
              'Per avere un terminatore unico:
              SKriga = SKriga & " /*"
            End If
            tb.MoveNext
          Loop
        Else
          addSegnalazione skList(k), "NOSK", skList(k)
          'SQ ora � ciclico
          'tb.Close
          'Exit Sub
          skFound = False
        End If
        tb.Close
      Else
        skFound = True '� tutto impostato male... cmq imposto a true perch� deve entrare sotto a fare il checkRelation...
      End If
    
      If skFound Then
        Select Case pgmName
          Case "NAT232BA", "NAT217BA"
            While token <> "/*"
              Line = SKriga
              token = nexttoken(SKriga)
              ' Lo forzo per uscire dal ciclo
              If token = SKriga Then
                SKriga = "/*"
              End If
              Select Case token
                Case "EXEC", "RUN"
                  program = nexttoken(SKriga)  'PROGRAM
                  If Left(program, 1) = "(" Then program = Mid(program, 2, Len(program) - 2)  'PULIZIA PARENTESI
                Case Else
                  'nop
              End Select
            Wend
            token = ""
            If Len(program) Then
              MapsdM_Parser.checkRelations program, "CALL"
            End If
          
          Case "IKJEFT01", "IKJEFT1B", "TSOBATCH", "HPSVRUN1"
            'pgmName = ""
            wNomePlan = ""
            While token <> "/*"
              Line = SKriga
              token = nexttoken(SKriga)
              ' Lo forzo per uscire dal ciclo
              If token = SKriga Then
                SKriga = "/*"
              End If
              Select Case token
                ' Mauro 12/01/2009 :  Perch� "RUN"? Cos� si tira fuori il PLAN, non il PGM
                'Case "RUN"
                  'token = nextToken(SKriga)  'PROGRAM
                Case "PROGRAM", "PROG"
                  program = nexttoken(SKriga)  'PROGRAM
                  If Left(program, 1) = "(" Then program = Mid(program, 2, Len(program) - 2)  'PULIZIA PARENTESI
                Case "PLAN"
                  wNomePlan = nexttoken(SKriga)  'PROGRAM
                  If Left(wNomePlan, 1) = "(" Then wNomePlan = Mid(wNomePlan, 2, Len(wNomePlan) - 2)  'PULIZIA PARENTESI
                Case "PARM"
                  If program = "HPSBATCH" Then
                    program = nexttoken(SKriga)  'PROGRAM
                    If InStr(program, "/") Then
                      Slash = InStr(program, "/")
                      Virgola = InStr(Slash, program, ",")
                      program = Mid(program, Slash + 1, Virgola - Slash - 1)
                    Else
                      If Left(program, 1) = "(" Then program = Mid(program, 2, Len(program) - 2)  'PULIZIA PARENTESI
                    End If
                  End If
                Case Else
                  'nop
      '            dsn = nextToken(line)
      '            If Len(dsn) > 0 Then
      '              dsn = nextToken(line)
      '              dsn = Replace(dsn, "(", "")
      '              dsn = Replace(dsn, ")", "")
      '              pgmName = dsn
      '              token = "/*"
      '            End If
              End Select
            Wend
            token = ""
            If Len(program) Then
              'SQ 11-10-06
      '        Set tb = m_Fun.Open_Recordset("select idoggetto,Tipo, Nome from bs_oggetti where tipo = 'CBL' and nome = '" & pgmName & "'")
      '        If tb.RecordCount Then
      '          'OCIO SE SONO PIU' DI UNO... SEGNALAZIONE
      '          AggiungiRelazione tb!idOggetto, "CALL", pgmName, pgmName
      '          idOggetto = tb!idOggetto
      '        Else
      '          If Not ExistObjSyst(pgmName, "PGM") Then
      '            addSegnalazione pgmName, "NOCALL", pgmName
      '          End If
      '        End If
              MapsdM_Parser.checkRelations program, "CALL"
              If Len(wNomePlan) Then
                AggiungiComponente program, wNomePlan, "PLN", "PLAN"
              End If
      '        tb.Close
            End If
          Case "IDCAMS" 'SQ 20-11-06
            parseIDCAMS 0, SKriga
          Case "SAS820", "SAS913", "SASITSV", "SASXA1"
            MapsdM_Parser.checkRelations skName, "SAS-EXEC"
          Case Else
            '...
        End Select
      End If
    Next k
  
    Next a
  Else
    'tmp (usare token e gestione eccezione...)
    If Left(statement, 5) = "DUMMY" Then
      dsn = "DUMMY"
      On Error GoTo tmpErr
      ' Mauro 21/01/2009 : Posso avere pi� di un valore sullo stesso step
      '  Parser.PsConnection.Execute "INSERT INTO PsJCL_Sked (idOggetto,stepNumber,DSN,Library,Member) " & _
      '                              "  VALUES(" & GbIdOggetto & ",'" & stepNumber & "','" & ddname & "','" & library & "','" & dsn & "')"
      Set rsSked = m_Fun.Open_Recordset("select * from PsJcl_Sked where " & _
                                        "idoggetto = " & GbIdOggetto & " and stepNumber = " & stepNumber)
      rsSked.AddNew
      rsSked!idOggetto = GbIdOggetto
      rsSked!stepNumber = stepNumber
      rsSked!Ordinale = rsSked.RecordCount + 1
      rsSked!dsn = ddname
      rsSked!library = library
      rsSked!member = dsn
      rsSked.Update
      rsSked.Close
    Else
      '!? ...messaggio
      Exit Sub
    End If
  End If

  Exit Sub
errdb:
  If err.Number = 123 Then 'Errore gestito: syntax error on nextToken
    'Scheda parametro con parentesi/apici "sbilanciati"
    'LA IGNORIAMO...
  Else
    'gestire
    On Error Resume Next  'se ho problemi sul TbOggetti!Nome...
    m_Fun.writeLog "parseJclSked: id#" & GbIdOggetto & "#" & SKriga & "#" & err.description
    'Resume
  End If
  Exit Sub
tmpErr:
  m_Fun.writeLog "TMP parseJclSked: id#" & GbIdOggetto & "-" & SKriga & ": SYSIN concatenata!"
  Resume Next
End Sub

''''''''''''''''''''''''''''
' Jcl VSE
' Modifica Roby - 9-03-06
''''''''''''''''''''''''''''
Sub parseJOB()
  Dim nFJcl As Long
  Dim wRecIn As String, wRec As String, UltChar As String, wStr As String, ddname As String, dsn As String
  'Dim idArchivio As Long, wNumLink As Long, rf_nStep As Long, rf_IdOggetto As Long
  Dim wNumLink As Long, rf_nStep As Long, rf_IdOggetto As Long
  Dim VSAMName As String
  Dim firstWord As String
  Dim i As Integer
  Dim stepLines As Long
  Dim tb As Recordset, rs As Recordset, rsTracciato As Recordset, tbJCL As Recordset
  Dim rf_Type As String, rf_Step As String, rf_Program As String, rf_Link As String, rf_Dsnname As String, rf_Use As String
  Dim rf_Update As Boolean
  Dim rf_RunProgram As String, rf_Psbname As String
  Dim Ordinale As Integer, j As Integer
                
  rf_IdOggetto = GbIdOggetto
   
  rf_Type = "JCL"
  rf_nStep = 1
  ''migdal - PIPPO: METTERE IN LINEA????
  '''rf_nStep = 1
  ''rf_nStep = 0
  rf_Update = False
   
  nFJcl = FreeFile
  Open GbFileInput For Input As nFJcl
  ' Roberto 02-03-2006 inizio
  While Not EOF(nFJcl)
    Line Input #nFJcl, wRecIn
    CalcolaLOCSJcl wRecIn
    GbNumRec = GbNumRec + 1
    wRecIn = Mid$(wRecIn & Space$(72), 1, 72)
    While (Mid$(wRecIn, 1, 2) = "/*" Or Mid$(wRecIn, 1, 2) = "/&" Or Mid$(wRecIn, 1, 2) = "/." Or _
           Mid$(wRecIn, 1, 1) = "*" Or Mid$(wRecIn, 1, 2) = "/&") And InStr(wRecIn, "SLI") = 0 And Not EOF(nFJcl)
    Line Input #nFJcl, wRecIn
    CalcolaLOCSJcl wRecIn
    GbNumRec = GbNumRec + 1
  Wend
  wRec = Left(wRecIn & Space(72), 72)
  If Len(RTrim(wRecIn)) = 0 Then
    UltChar = " "
  Else
    UltChar = Mid(wRecIn, Len(RTrim(wRecIn)), 1)
  End If
  While UltChar = "," And Not EOF(nFJcl)
    Line Input #nFJcl, wRecIn
    CalcolaLOCSJcl wRecIn
    GbNumRec = GbNumRec + 1
    wRecIn = Mid$(wRecIn & Space$(72), 1, 72)
    If Len(RTrim(wRecIn)) = 0 Then
      UltChar = " "
    Else
      UltChar = Mid$(wRec, Len(RTrim(wRecIn)), 1)
    End If
    wRec = wRec & " " & Trim(Mid$(wRecIn, 1, 72))
  Wend
  
    Dim token As String, istruz As String
    Dim statement As String, pgmName As String, Name As String, inputStream As String
    Dim inputStream1 As String, Slitype As String
    
    GbOldNumRec = GbNumRec
    
    inputStream = wRec
    token = nexttoken(wRec)
    Select Case token
    Case "//"
      istruz = token
      Name = ""
    Case "GOTO", "IF", "ON", "CONTINUE", "CLOSE"
      istruz = "" ' non faccio niente
      Name = token
    Case "*"
      If InStr(wRecIn, "EOJ") > 0 Then
        istruz = "" ' non faccio niente
      End If
    Case "/INCLUDE"
      istruz = ""
      'Es: /INCLUDE CICCIO
      'Name = Mid(Name, InStr(Name, "=") + 1)
      Name = nexttoken(wRec)
      Set tb = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where tipo = 'MBR' and nome = '" & Name & "'")
      If tb.RecordCount Then
        AggiungiRelazione tb!idOggetto, "INCLUDE_JCL", Name, Name
      Else
        addSegnalazione Name, "NOMEMBER", Name
      End If
      tb.Close
    Case Else
      Name = token
    End Select
    
    If istruz <> "" Then
      inputStream1 = wRec
      token = nexttoken(wRec)
      Select Case token
        Case "$$", "$$SLI" 'SLI
          'migdal - PIPPO
          Name = token
          token = nexttoken(wRec)
          If Name = "$$SLI" Then
            Name = token
          Else
            Name = Mid(wRec, InStr(wRec, "=") + 1)
          End If
          '''''''''''''''
          ' TMP - migdal
          '''''''''''''''
          If InStr(wRec, "=") > 0 Then
            Slitype = Left(wRec, InStr(wRec, "=") - 1)
          Else
            Slitype = "STANDARD"
          End If
          Select Case Slitype
            Case "ICCF"
              Name = Trim(Replace(Replace(Name, "(", ""), ")", ""))
              Name = Left(Name, InStr(Name, ",") - 1)
            Case "MEM"
              Name = Left(Name, InStr(Name, ",") - 1)
            'migdal
            Case "STANDARD"
            Case Else
              Name = ""
          End Select
          If Len(Name) Then
            Set tb = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where tipo = 'MBR' and nome = '" & Name & "'")
            If tb.RecordCount Then
               AggiungiRelazione tb!idOggetto, "INCLUDE_JCL", Name, Name
            Else
              addSegnalazione Name, "NOMEMBER", Name
            End If
            tb.Close
          End If
          ' ciclo la scheda parametri SLI
          While ((Mid$(wRecIn, 1, 2) <> "/*" And Mid$(wRecIn, 1, 2) <> "//") And Not EOF(nFJcl))
             Line Input #nFJcl, wRecIn
             CalcolaLOCSJcl wRecIn
             GbNumRec = GbNumRec + 1
          Wend
      Case "JOB"
      ' Mauro 16/04/2008
      'Case "TLBL"
      Case "DLBL", "TLBL"
        rf_Link = ""
        token = nexttoken(wRec)
        ' Mauro 17/04/2008 :  Su consiglio di Robi. Se non trovo la virgola, salto tutto
        If InStr(token, ",") Then
          token = Trim(Left(token, InStr(token, ",") - 1))
          parseJobDD inputStream, token, "", 0, ""
        End If
      Case "EXEC"
        token = nexttoken(wRec)
        If InStr(token, ",") > 0 Then
          token = Trim(Left(token, InStr(token, ",") - 1))
        End If
        pgmName = token
        '''''''''''''''''''''''''''''''''''''''
        ' TMP: NATURAL/ADABAS (Migdal)
        '''''''''''''''''''''''''''''''''''''''
        If pgmName = "NATBATCH" Or pgmName = "NATBABIG" Then
          'controllo temp:
          If pgmName = "NATBABIG" Then
            m_Fun.writeLog "TMP: NATBABIG! - " & GbIdOggetto
          End If
          'SQ - Gestione tabella temporanea tmp_C7010
          'If True Then
          'T
          If InStr(UCase(m_Fun.FnNomeDB), "MIGDAL") Then
          
            AggiungiComponente pgmName, GbNomePgm, "UTL", "SYSTEM"
            'Nome programma:
            Set tbJCL = m_Fun.Open_Recordset("SELECT pgmName FROM tmp_C7010 WHERE jclName='" & GbNomePgm & "'")
            If tbJCL.RecordCount Then
              While Not tbJCL.EOF
                pgmName = tbJCL!pgmName
                MapsdM_NATURAL.checkRelations pgmName, "NATBATCH"
                'Set tb = m_Fun.Open_Recordset("SELECT idOggetto FROM bs_oggetti WHERE tipo = 'NAT' AND nome = '" & pgmName & "'")
                'If tb.RecordCount Then
                '  AggiungiRelazione tb!idOggetto, "PGM", pgmName, pgmName
                'Else
                '  addSegnalazione pgmName, "NOCALL", pgmName
                'End If
                'tb.Close
                tbJCL.MoveNext
              Wend
            Else
              addSegnalazione "!!!tmp_C7010", "NONATBATCH", "!!!tmp_C7010"
            End If
            tbJCL.Close
          'T
          Else
            'SQ - Gestione di PIPPO
            Dim pgmName1() As String
            Dim indNat As Integer
            Line Input #nFJcl, wRecIn
            CalcolaLOCSJcl wRecIn
            GbNumRec = GbNumRec + 1
            indNat = 0
            ReDim pgmName1(indNat)
            'SQ - mi serviva un flag!
            indNat = -1
            'SQ
            'While Left(wRecIn, 3) <> "FIN" And Left(wRecIn, 2) <> "/&" And Not EOF(nFJcl)
            While Left(wRecIn, 3) <> "FIN" And Left(wRecIn, 2) <> "/&" And Left(wRecIn, 2) <> "//" And Not EOF(nFJcl)
              'DA ADARUN IN GIU!
              If indNat <> -1 Then
                If Len(Trim(Left(wRecIn, 72))) <= 8 Then
                  If InStr(Left(wRecIn, 72), ",") = 0 And InStr(Left(wRecIn, 72), "=") = 0 And InStr(Left(wRecIn, 72), "/") = 0 Then
                    indNat = indNat + 1
                   ReDim Preserve pgmName1(indNat)
                    pgmName1(indNat) = Left(wRecIn & Space(8), 8)
                  End If
                End If
              Else
                If InStr(wRecIn, "ADARUN") Then
                  indNat = 0
                  '/*
                  Line Input #nFJcl, wRecIn
                  CalcolaLOCSJcl wRecIn
                  GbNumRec = GbNumRec + 1
                End If
              End If
              Line Input #nFJcl, wRecIn
              CalcolaLOCSJcl wRecIn
              If Not EOF(nFJcl) Then
                GbNumRec = GbNumRec + 1
              End If
            Wend
            For i = 1 To indNat
                pgmName = Trim(pgmName1(i))
                If pgmName <> "ADARUN" Then
                  If ExistObjSyst(pgmName, "PGM") Then 'Presente in "psUte_ObjSys":
                    'Aggiungi alla "PsCom_Obj"
                    'AggiungiComponente calledModule, calledModule, "UTL", "SYSTEM"  'Non sapevo chi e quanti utilizzano un certo "componente"!
                    'Soluzione temp:
                    AggiungiComponente pgmName, GbNomePgm, "UTL", "SYSTEM"
                  Else
                    'SQ - PEZZA... TIRIAMO SU DI TUTTO!
                    If Not IsNumeric(pgmName) Then
                      MapsdM_NATURAL.checkRelations pgmName, "NATBATCH"
                    End If
                    'Set tb = m_Fun.Open_Recordset("SELECT idOggetto FROM bs_oggetti WHERE tipo = 'NAT' AND nome = '" & pgmName & "'")
                    'If tb.RecordCount Then
                    '  AggiungiRelazione tb!idOggetto, "PGM", pgmName, pgmName
                    'Else
                    '  addSegnalazione pgmName, "NOCALL", pgmName
                    'End If
                    'tb.Close
                  End If
                End If
              Next
              'parziale: per ora lascio NATBATCH
              rf_nStep = rf_nStep + 1
              pgmName = "NATBATCH"
          'T
          End If
          ElseIf InStr(token, "REXX") Then
            pgmName = Right(pgmName, Len(pgmName) - InStr(pgmName, "="))
            addSegnalazione pgmName, "REXX", pgmName
            'migdal
            rf_nStep = rf_nStep + 1
          Else
            Set tb = m_Fun.Open_Recordset("select * from PsRel_file where idoggetto = " & GbIdOggetto & " and Program = 'VSE' ")
            For i = 1 To tb.RecordCount
              tb!program = pgmName
              ddname = tb!LINK
              dsn = tb!dsnName
              tb.Update
              ' rintraccio id archivio
              Set rs = m_Fun.Open_Recordset("select * from DMVSM_Archivio where nome = '" & dsn & "' ")
              If rs.RecordCount = 0 Then
                ' Non dovrei mai entrare qui
                tb.MoveNext
                Exit For
              End If
              ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
              'idArchivio = rs!idArchivio
              VSAMName = rs!VSam & ""
              rs.Close

            ' AGGIORNAMENTO DMVSM_Tracciato
              Set rs = m_Fun.Open_Recordset("select IdOggetto,IdArea from PsRel_Tracciati where " & _
                                    "Program='" & pgmName & "' and DDname = '" & ddname & "'")
              If rs.RecordCount Then
                'PGM gia' parserato: info tracciato gia' presenti...
                For j = 1 To rs.RecordCount
                  ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
                  'Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " and idoggetto = " & rs!idOggetto & " and idarea = " & rs!idArea)
                  Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                                         "VSAM = '" & VSAMName & "' and idoggetto = " & rs!idOggetto & " and idarea = " & rs!idArea)
                  If rsTracciato.RecordCount = 0 Then
                    'Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " order by ordinale ")
                    Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                                           "VSAM = '" & VSAMName & "' order by ordinale ")
                    If rsTracciato.RecordCount = 0 Then
                      Ordinale = 1
                    Else
                      rsTracciato.MoveLast
                      Ordinale = rsTracciato!Ordinale + 1
                    End If
                    rsTracciato.AddNew
                    'rsTracciato!idArchivio = idArchivio
                    rsTracciato!VSam = VSAMName
                    rsTracciato!Ordinale = Ordinale
                    rsTracciato!idOggetto = rs!idOggetto
                    rsTracciato!idArea = rs!idArea
                    rsTracciato!Tag = "DD"
                    rsTracciato.Update
                    rsTracciato.Close
                  End If
                rs.MoveNext
                Next
              End If
              rs.Close
              tb.MoveNext
            Next
            tb.Close
            
            If pgmName = "IDCAMS" Then
              Line Input #nFJcl, wRecIn
              CalcolaLOCSJcl wRecIn
              GbNumRec = GbNumRec + 1
              ParserDefCluster wRecIn, nFJcl
            End If
            
            SwNomeCall = True
            rf_Program = ""
            ParserJclProcPgm wRecIn, rf_Step, pgmName
            If InStr(wRecIn, "PROC") Then
              pgmName = Right(pgmName, Len(pgmName) - InStr(pgmName, "="))
            End If
            If Len(pgmName) Then
              'SQ MADRID: era commentato... perch�????????????????????????
              rf_nStep = rf_nStep + 1
              Parser.PsConnection.Execute _
                "UPDATE PsRel_file SET program='" & pgmName & "' WHERE " & _
                " idoggetto = " & GbIdOggetto & " and program='PGMJOB'"
            End If
            rf_RunProgram = ""
            ParserJclPsb wRecIn, rf_RunProgram, rf_Psbname
            ParserJclRunPgm wRecIn, rf_RunProgram
            If rf_RunProgram <> "" Then
              Aggiorna_RelFile rf_IdOggetto, rf_Type, rf_nStep, rf_Step, pgmName, rf_RunProgram
              rf_Program = rf_RunProgram
            End If
          End If
          ' ciclo la scheda parametri PGM
          If InStr(wRecIn, pgmName) And InStr(wRecIn, "PROC=") = 0 Then
            Line Input #nFJcl, wRecIn
            CalcolaLOCSJcl wRecIn
            GbNumRec = GbNumRec + 1
          End If
          While ((Left(wRecIn, 2) <> "/*" And Left(wRecIn, 2) <> "//") And Not EOF(nFJcl))
             Line Input #nFJcl, wRecIn
             CalcolaLOCSJcl wRecIn
             GbNumRec = GbNumRec + 1
          Wend
          '''''''''''''''''''
          ' Da testare
          '''''''''''''''''''
          Set tbJCL = m_Fun.Open_Recordset("select * from PsJCL where idoggetto = " & GbIdOggetto & " and stepnumber = " & rf_nStep)
          If Not tbJCL.EOF Then
            tbJCL!linee = GbOldNumRec - stepLines
            tbJCL.Update
          End If
          tbJCL.Close
          stepLines = GbOldNumRec
          '''''''''''''''''''''''''''''''''
          ' SQ Madrid
          ' TABELLA PsJCL
          '''''''''''''''''''''''''''''''''
          'manca execType... di ritorno dal parseEXEC_PGM...
          Parser.PsConnection.Execute "INSERT INTO PsJCL (idOggetto,stepNumber,stepName,execName) " & _
                                       "VALUES(" & GbIdOggetto & "," & rf_nStep & ",'" & rf_Step & "','" & pgmName & "')"
      Case "EXTENT"
      Case "ASSGN"
      Case "ON"
      Case "UPSI"
      Case Else
        'Attenzione... con istruz""
      End Select
    End If
' Roberto 02-03-2006 Fine
  Wend
  Close #nFJcl
   
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  TbOggetti!DtParsing = Now
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti!parsinglevel = 1
  TbOggetti.Update
  
  Exit Sub
errPsJCL:
'tmp: OK non ho la tabella... vecchie versioni
Resume Next
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''
' Gestione "SYSTIN" tipo "RUN PROGRAM, PLAN"
' - Relazione/Segnalazioni di CALL
' - Componente "PLAN"
' Ritorna il nome del programma invocato (o space)
''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function parseIKJEFT01(fileIn As Long, Optional statement As String) As String
  Dim tb As Recordset
  Dim wNomePlan As String, Line As String, pgmName As String, token As String
  Dim idOggetto As Long
  
  If Len(Trim(statement)) = 0 Then
    'COMPOSIZIONE INPUT: SYSTIN SU UNICA LINEA (terminante con "/*")
    ' Mauro 01/10/2008 : Potrebbe non esserci un asterisco a fine step
    'While Left(line, 2) <> "/*" And Left(line, 3) <> "//*" And Not EOF(fileIn) 'ATTENZIONE: PUO' ESSERE ANCHE: DD DATA,DLM=AA
    Line Input #fileIn, Line
    CalcolaLOCSJcl Line
    GbNumRec = GbNumRec + 1
    While Left(Line, 1) <> "/" And Not EOF(fileIn) 'ATTENZIONE: PUO' ESSERE ANCHE: DD DATA,DLM=AA
      statement = statement & " " & Line
      Line Input #fileIn, Line
      CalcolaLOCSJcl Line
      GbNumRec = GbNumRec + 1
    Wend
    If Left(Line, 2) <> "/*" And Left(Line, 3) <> "//*" Then
      alreadyReadLine = Line
    End If
  End If
  'Per avere un terminatore unico:
  statement = statement & " /*"
  
  Do While token <> "/*"
    token = nexttoken(statement)
    Select Case token
      ' Mauro 12/01/2009 :  Perch� "RUN"? Cos� si tira fuori il PLAN, non il PGM
      'Case "RUN"
      '  token = nextToken(statement)  'PROGRAM
      Case "PROGRAM", "PROG"
        pgmName = nexttoken(statement)  'PROGRAM
        If Left(pgmName, 1) = "(" Then pgmName = Mid(pgmName, 2, Len(pgmName) - 2)  'PULIZIA PARENTESI
      Case "PLAN"
        wNomePlan = nexttoken(statement)  'PROGRAM
        If Left(wNomePlan, 1) = "(" Then wNomePlan = Mid(wNomePlan, 2, Len(wNomePlan) - 2)  'PULIZIA PARENTESI
      Case "PARM"
        If pgmName = "HPSBATCH" Then
          pgmName = nexttoken(statement)  'PROGRAM
          If InStr(pgmName, "/") Then
            Dim Slash As Integer, Virgola As Integer
            Slash = InStr(pgmName, "/")
            Virgola = InStr(Slash, pgmName, ",")
            pgmName = Mid(pgmName, Slash + 1, Virgola - 1)
          Else
            If Left(pgmName, 1) = "(" Then pgmName = Mid(pgmName, 2, Len(pgmName) - 2)  'PULIZIA PARENTESI
          End If
        End If
      Case Else
      '...
    End Select
    ' Per qualche motivo, non riesce a uscire dal loop
    ' (Sbilanciamento delle parentesi, degli apici o altro...)
    If statement = "" And token = "" Then
      Exit Do
    End If
  Loop
  ''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Relazioni/Segnalazioni (CALL)
  ' Attenzione: 1) solo CBL
  '             2) giusto CALL o PGM?
  '             3) Se ho pi� pgm con lo stesso nome?
  ''''''''''''''''''''''''''''''''''''''''''''''''''
  If Len(pgmName) Then
    'Ritorno:
    parseIKJEFT01 = Replace(pgmName, "'", "")
    
    'SQ 11-10-06
'    Set tb = m_Fun.Open_Recordset("select * from BS_Oggetti where tipo = 'CBL' and nome = '" & pgmName & "'")
'    If tb.RecordCount Then
'       AggiungiRelazione tb!idOggetto, "CALL", pgmName, pgmName
'       idOggetto = tb!idOggetto
'    Else
'       If Not ExistObjSyst(pgmName, "PGM") Then
'          addSegnalazione pgmName, "NOCALL", pgmName
'       End If
'    End If
    If InStr(Parser.PsNomeDB, "TSystems") Then
      GbIdOggetto = appoIdProc
    End If

    MapsdM_Parser.checkRelations pgmName, "CALL"
    
    '''''''''''''''''''''''''''''''''
    ' Componenti: "PLAN"
    '''''''''''''''''''''''''''''''''
    If Len(wNomePlan) Then
      AggiungiComponente pgmName, wNomePlan, "PLN", "PLAN"
    End If
  'else
    'Non "RUN PROGRAM"...
    'Altri utilizzi dell'utility che non interessano (per ora)
  End If
End Function

Private Function parseNATBATCH(fileIn As Long, Optional statement As String) As String
  Dim tb As Recordset
  Dim wNomePlan As String, Line As String, pgmName As String, token As String
  Dim idOggetto As Long
  
  If Len(Trim(statement)) = 0 Then
    'COMPOSIZIONE INPUT: CMSYNIN SU UNICA LINEA (terminante con "/*")
    Line Input #fileIn, Line
    CalcolaLOCSJcl Line
    GbNumRec = GbNumRec + 1
    While Left(Line, 1) <> "/" And Not EOF(fileIn) 'ATTENZIONE: PUO' ESSERE ANCHE: DD DATA,DLM=AA
      statement = statement & " " & Line
      Line Input #fileIn, Line
      CalcolaLOCSJcl Line
      GbNumRec = GbNumRec + 1
    Wend
    If Left(Line, 2) <> "/*" And Left(Line, 3) <> "//*" Then
      alreadyReadLine = Line
    End If
  End If
  'Per avere un terminatore unico:
  statement = statement & " /*"
  
  Do While token <> "/*"
    token = nexttoken(statement)
    Select Case token
      Case "EXEC", "RUN"
        pgmName = nexttoken(statement)  'PROGRAM
        If Left(pgmName, 1) = "(" Then pgmName = Mid(pgmName, 2, Len(pgmName) - 2)  'PULIZIA PARENTESI
      Case Else
      '...
    End Select
    ' Per qualche motivo, non riesce a uscire dal loop
    ' (Sbilanciamento delle parentesi, degli apici o altro...)
    If statement = "" And token = "" Then
      Exit Do
    End If
  Loop
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Relazioni/Segnalazioni (CALL)
  ' Attenzione: 1) solo CBL
  '             2) giusto CALL o PGM?
  '             3) Se ho pi� pgm con lo stesso nome?
  ''''''''''''''''''''''''''''''''''''''''''''''''''
  If Len(pgmName) Then
    'Ritorno:
    parseNATBATCH = Replace(pgmName, "'", "")
    
    MapsdM_Parser.checkRelations pgmName, "CALL"
  End If
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''
' Gestione "SYSIN"...
' - Relazione/Segnalazioni di CALL
' Ritorna il nome del programma invocato (o space)
''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function parseSYSIN_PGM(statement As String, psbName As String, stepNumber As Integer, utility As String)
  Dim k As Integer, i As Integer
  Dim rs As Recordset
  Dim dsn As String, pgmName As String, token As String, library As String, pgmType As String, relType As String
  Dim idOggetto As Long, idOggettoOrig As Long
  Dim a As Integer
  
  Select Case utility
    Case "QUIKIMS", "QUIKJOB"
      pgmType = "QIM"
      relType = "QUIK-PGM"
    Case "EZTPA00"
      pgmType = "EZT"
      relType = "EZT-PGM" 'tmp
    'T
    Case "SASESA", "SAS", "SASPIU"
      pgmType = "SAS"
      relType = "SAS-PGM" 'tmp
    Case Else
      'non possibile
      'SQ: parole grosse!
      If (utility = "SAS609") Then  'aggiungere al case sopra
        pgmType = "SAS"
        relType = "SAS-PGM" 'tmp
      End If
      ' Ac per ora gestisco anche questa come eccezione
      If (utility = "Y6SAS") Then  'aggiungere al case sopra
        pgmType = "SAS"
        relType = "SAS-PGM" 'tmp
      End If
      If (utility = "SASXA1") Then  'aggiungere al case sopra
        pgmType = "SAS"
        relType = "SAS-PGM" 'tmp
      End If
  End Select
  
  dsn = SelezionaParam(statement, "DSN=")
  
  If Len(dsn) Then
    '''''''''''''''''''''
    '
    '''''''''''''''''''''
    If InStr(dsn, "(") Then
      library = Left(dsn, InStr(dsn, "(") - 1)
    Else
      library = dsn
    End If
    ' TEMP
    ' Mauro 13/03/2008: Non posso eliminare tutto quello che c'� prima del punto
    '                   Potrebbero essere delle variabili, se precedute da "&", es: "&var1.001"
    k = InStr(dsn, ".")
    While k > 0
      For i = k - 1 To 1 Step -1
        If Mid(dsn, i, 1) = "&" Then
          Exit For
        ElseIf Mid(dsn, i, 1) = "." Then
          i = 0
          Exit For
        End If
      Next i
      If i = 0 Then
        dsn = Right(dsn, Len(dsn) - k)
        k = InStr(dsn, ".")
      Else
        k = InStr(k + 1, dsn, ".")
      End If
    Wend
  
    k = InStr(dsn, "(")
    If k Then  'posso avere il membro definito tra parentesi
       dsn = Mid(dsn, k + 1)
       dsn = Left(dsn, InStr(dsn, ")") - 1)
    Else
      'ho il punto dentro le parentesi... rimane solo la chiusa
      dsn = Replace(dsn, ")", "")
    End If
    'dsn = Replace(Replace(dsn, "(", ""), ")", "")
  
    pgmName = dsn
    
    ''''''''''''''''''''''''''''''''''''''''''''
    ' TMP: COPIATO (piccole modifiche) DA checkSchedPSB... farne una!!!
    ''''''''''''''''''''''''''''''''''''''''''''
    'AggiungiRelazione/Segnalazione lavora sul GbidOggetto, ma qui ho member che e' diverso...
    idOggettoOrig = GbIdOggetto
  
    ''''''''''''''''''''
    'Controllo pgmName:
    ''''''''''''''''''''
    'SQ 25-12-07 Risoluzione VARIABILI
    Dim pgmList() As String
    ReDim pgmList(0)
    If InStr(dsn, "&") Then
      pgmList = getParameters(dsn)
    
      'SQ - per ora solo il primo teniamo!?
      If Len(pgmList(0)) Then
        pgmName = pgmList(0)
      Else
        addSegnalazione dsn, "NOPGMVALUE", dsn
      End If
    Else
      pgmList(0) = dsn
    End If
     
    For a = 0 To UBound(pgmList)
      pgmName = pgmList(a)
      'If Left(pgmName, 1) <> "&" Then
      If InStr(pgmName, "&") = 0 Then
        'Ritorno:
        ' Se ci sono delle parentesi, prendo solo quello che c'� dentro, eliminando il percorso!
        If InStr(pgmName, "(") Then
          pgmName = Mid(pgmName, InStr(pgmName, "(") + 1, InStr(pgmName, ")") - InStr(pgmName, "(") - 1)
        End If
        parseSYSIN_PGM = pgmName
        
        ''''''''''''''''''''''''''''''''''
        ' SQ 15-02-06
        ' Gestione PsLoad
        ''''''''''''''''''''''''''''''''''
        'SQ 25-05-07 Questa non c'entra niente?! � stata incollata per sbaglio?
        'Parser.PsConnection.Execute "INSERT INTO PsJCL_Psb (idOggetto,stepNumber,Utility,PSB,PGM) " & _
        '                            "VALUES(" & GbIdOggetto & ",'" & stepNumber & "','" & utility & "','" & psbName & "','" & pgmName & "')"
    
        'SQ 25-05-07
        'TMP NON HO TEMPO! AGGIUNGERE IL TIPO "QIM" NEL checkRelations e buttare via qui sotto!
        If pgmType = "QIM" Then
          Set rs = m_Fun.Open_Recordset("select idOggetto,tipo from bs_oggetti where nome = '" & pgmName & "' and tipo in ('" & pgmType & "')")
          If rs.RecordCount = 0 Then
            'Nome LOAD diverso da nome SOURCE: aggiorniamo il recordset
            Set rs = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.load = '" & pgmName & "' and b.source=a.nome and a.tipo in ('CBL','ASM','PLI')")
          End If
      
          If rs.RecordCount Then  'rs potenzialmente diverso da quello sopra...
            AggiungiRelazione rs!idOggetto, relType, pgmName, pgmName
            'diventa lui...
            GbIdOggetto = rs!idOggetto
          Else
            ''''''''''''''''''''''''''''''''''''''''''''''
            ' UTILITY?
            ' Es: DFSUDMP0,DFSUCF00,DFSSBHD0,...
            ''''''''''''''''''''''''''''''''''''''''''''''
            If ExistObjSyst(pgmName, "PGM") Then 'Presente in "psUte_ObjSys"
              'Aggiungi alla "PsCom_Obj"
              AggiungiComponente pgmName, pgmName, "UTL", "SYSTEM"
            Else
              addSegnalazione pgmName, "NO" & relType, pgmName
            End If
          End If
          rs.Close
        Else
          MapsdM_Parser.checkRelations pgmName, relType
        End If
      End If
    Next a
    '''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Controllo PSB:
    ' Solo se eseguito con DFSRRC00 (psbName fa da flag)
    '''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Se ho valori variabili non me ne faccio niente...
    Dim IdPsb As Long
    If Len(psbName) Then
      'If Left(psbName, 1) <> "&" Then
      If InStr(psbName, "&") = 0 Then
        Set rs = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where tipo = 'PSB' and nome = '" & psbName & "'")
          If rs.RecordCount Then
            IdPsb = rs!idOggetto
          Else
            'Restore
            GbIdOggetto = idOggettoOrig
            addSegnalazione psbName, "NOPSB-JCL", psbName
          End If
        rs.Close
      End If
      ''''''''''''''''''''''''''
      ' Relazione pgmName/PSB
      ''''''''''''''''''''''''''
      If GbIdOggetto <> idOggettoOrig And IdPsb <> 0 Then 'tutto OK:
        AggiungiRelazione IdPsb, "PSB-JCL", GbNomePgm, psbName
      End If
    End If
    'Restore
    GbIdOggetto = idOggettoOrig
  End If
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Aggiorna il nome del programma (es: IKJEFT01/IKJEFT1B/TSOBATCH) associato ad un file di DD
' Aggiorna la relazione pgm/tracciato (se parsing del pgm gi� effettuato)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub updateFileRelations(pgmName As String, stepNumber As Integer, utility As String)
  Dim rsRelFile As Recordset, rsRelTracciati As Recordset, rsTracciato As Recordset
  Dim Ordinale As Integer, i As Integer
  'Dim idArchivio As Long
  Dim VSAMName As String
  
  Set rsRelFile = m_Fun.Open_Recordset("select idOggetto,program,link,dsnName from PsRel_file where idoggetto = " & GbIdOggetto & _
                                        " and nstep = " & stepNumber & " and program = '" & utility & "'")
  While Not rsRelFile.EOF
    'update:
    rsRelFile!program = pgmName
    rsRelFile.Update
    
    '''''''''''''''''''''''
    ' idArchivio:
    '''''''''''''''''''''''
    'Set rsRelTracciati = m_Fun.Open_Recordset("select idArchivio from DMVSM_Archivio where nome = '" & rsRelFile!dsnName & "'")
    Set rsRelTracciati = m_Fun.Open_Recordset("select VSAM from DMVSM_Archivio where nome = '" & rsRelFile!dsnName & "'")
    If rsRelTracciati.RecordCount Then  'per sicurezza, ma appena inserito
      ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
      'idArchivio = rsRelTracciati!idArchivio
      VSAMName = rsRelTracciati!VSam & ""
    Else
      'segnalazione...
    End If
    rsRelTracciati.Close
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' AGGIORNAMENTO DMVSM_Tracciato
    ' (attenzione... passo di qui anche con le utility: non e' il massimo...
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set rsRelTracciati = m_Fun.Open_Recordset("select IdOggetto,IdArea from PsRel_Tracciati where " & _
                                              "Program='" & pgmName & "' and DDname = '" & rsRelFile!LINK & "'")
    If rsRelTracciati.RecordCount Then
      'PGM gia' parserato: info tracciato gia' presenti...
      For i = 1 To rsRelTracciati.RecordCount
        '''''''''''''''''''''''
        ' 23-02-06: c'era il problema di chiavi duplicate...
        ' A cosa serve l'Ordinale? vedere se sarebbe OK la chiave idArchivio|IdOggetto|idArea
        '''''''''''''''''''''''
        ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
        'Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " and idoggetto = " & rsRelTracciati!idOggetto & " and idarea = " & rsRelTracciati!idArea)
        Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                               "VSAM = '" & VSAMName & "' and idoggetto = " & rsRelTracciati!idOggetto & " and " & _
                                               "idarea = " & rsRelTracciati!idArea)
        If rsTracciato.RecordCount = 0 Then
          'Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " order by ordinale ")
          Set rsTracciato = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                                 "VSAM = '" & VSAMName & "' order by ordinale ")
          If rsTracciato.RecordCount = 0 Then
            Ordinale = 1
          Else
            rsTracciato.MoveLast
            'Ordinale = rs.RecordCount + 1
            Ordinale = rsTracciato!Ordinale + 1
          End If
          rsTracciato.AddNew
          'rsTracciato!idArchivio = idArchivio
          rsTracciato!VSam = VSAMName
          rsTracciato!Ordinale = Ordinale
          rsTracciato!idOggetto = rsRelTracciati!idOggetto
          rsTracciato!idArea = rsRelTracciati!idArea
          rsTracciato!Tag = "DD"
          rsTracciato.Update
          rsTracciato.Close
        End If
        rsRelTracciati.MoveNext
        'ATTENZIONE: non posso pulire il record "temporaneo" d'appoggio in PsRel_Tracciati,
        'perche' la stessa relazione puo' servire ad altre JCL!
      Next
    End If
    rsRelTracciati.Close
    rsRelFile.MoveNext
  Wend
  rsRelFile.Close
End Sub

Sub ParserJMB()
  Dim nFJcl As Variant
  Dim wRecIn As String
  Dim wRec As String
  
  nFJcl = FreeFile
  Open GbFileInput For Input As nFJcl
  While Not EOF(nFJcl)
    wRecIn = ""
    Line Input #nFJcl, wRecIn
    CalcolaLOCSJcl wRecIn
    GbNumRec = GbNumRec + 1
    wRecIn = Trim(wRecIn)
    While Mid$(wRecIn, 1, 1) = "#" And Not EOF(nFJcl)
       Line Input #nFJcl, wRecIn
       CalcolaLOCSJcl wRecIn
       wRecIn = Trim(wRecIn)
       GbNumRec = GbNumRec + 1
    Wend
    wRec = wRecIn
    SwNomeCall = True
    ParserJmbProcPgm wRec
'    ParserJclPsb wRec
'    ParserJclRunPgm wRec
  Wend
   
  Close #nFJcl
   
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  TbOggetti.MoveFirst
  TbOggetti!DtParsing = Now
  TbOggetti!parsinglevel = 1
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti.Update
  TbOggetti.Close
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Gestione PARAMETRI (VARIABILI)
' Syntax: //[name] SET symbolic-parameter=value
'         // [,symbolic-parameter=value]... [comments]
''
' SQ 18-10-06
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parseSET(statement As String)
  Dim token As String
  Dim pos As Integer
  Dim rs As Recordset
  
  On Error GoTo errSET
  
  'cleanComments statement
  
  'Posso avere una lista di assegnamenti separati da ","
  'While Len(statement)
    token = nextToken_tmp(statement)
    'Valori con parentesi: nextToken si ferma
    If Left(statement, 1) = "(" Then
      token = token & nextToken_tmp(statement)
    End If
    pos = InStr(token, "=")
    If pos Then
      setParameter Left(token, pos - 1), Mid(token, pos + 1), (IdOggettoRel = GbIdOggetto)
    Else
      ' Mauro 05/12/2008
      'MsgBox "tmp: parseSET - format unknown: " & statement
      m_Fun.writeLog "tmp: parseSET - format unknown: " & statement, "MapsdM_JCL"
    End If
  'Wend
  Exit Sub
errSET:
  'nextToken?
  'tmp
  m_Fun.writeLog err.description, "MapsdM_JCL"
  'Resume
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Gestione PARAMETRI di DEFAULT (VARIABILI)
' Syntax: //[name] PROC [parameter [comments]]
' Es:     //P4 PROC PARM5=OLD,PARM6='SYS1.LINKLIB(P4))',
'         // PARM7=SYSDA,PARM8=(CYL,(1),1))
''
' SQ 20-10-06
' Se uguale a parseSET, farne una sola!
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Private Sub getDefaultParameters(statement As String)
Private Sub getDefaultParameters(statement As String, Optional onDb As Boolean = False)
  Dim token As String
  Dim pos As Integer
  Dim rs As Recordset
  
  On Error GoTo errSET
  
  cleanComments statement
  
  'Posso avere una lista di assegnamenti separati da ","
  While Len(statement)
    token = nextToken_newnew(statement)
    'Valori con parentesi: nextToken si ferma
    If Left(statement, 1) = "(" Or Left(statement, 1) = "'" Then
      token = token & nextToken_newnew(statement)
    End If
    pos = InStr(token, "=")
    If pos Then
      setParameter Left(token, pos - 1), Mid(token, pos + 1), onDb
    Else
      ' Mauro 05/12/2008
      'MsgBox "tmp: parseSET - format unknown: " & token & "@" & statement
      m_Fun.writeLog "tmp: parseSET - format unknown: " & token & "@" & statement, "MapsdM_JCL"
      statement = ""
    End If
  Wend
  Exit Sub
errSET:
  'nextToken?
  m_Fun.writeLog err.description, "MapsdM_JCL"
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''
' Delete tabelle allestite dal parse (I)
''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub resetRepository()
  On Error GoTo dbErr
  
  Parser.PsConnection.Execute "Delete * from PsJCL where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsJCL_Psb where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsJCL_Sked where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsJCL_Parameters where idOggetto =" & GbIdOggetto
  
  Parser.PsConnection.Execute "Delete * from PsRel_File where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsRel_Obj where idoggettoc = " & GbIdOggetto
  Parser.PsConnection.Execute _
    "Delete * from PsRel_Obj where " & "NomeComponente = '" & GbNomePgm & "' and Utilizzo = 'PSB-JCL'"
  Parser.PsConnection.Execute "Delete * from PsRel_ObjUnk where idoggettoc = " & GbIdOggetto
  Parser.PsConnection.Execute _
    "Delete * from PsRel_ObjUnk where " & "NomeComponente = '" & GbNomePgm & "' and Utilizzo = 'PSB-JCL'"
  Exit Sub
dbErr:
  m_Fun.Show_MsgBoxError "RC0002", , "resetRepository", err.source, err.description, False
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' - Ricerca MEMBER incluso fra gli MBR e gli UNK
' - Eventuale riclassificazione (casi di UNK)
' - Eventuale parsing (se non gi� parserato): serve per i parametri!
' - Relazioni/Segnalazioni
''
' SQ 18-10-06
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getInclude(statement As String)
  Dim member As String, token As String
  Dim rs As Recordset
  Dim members() As String, i As Integer
  
  token = nextToken_new(statement)
  If token = "MEMBER=" Then
    'Es: INCLUDE MEMBER=(CICCIO)
    member = nexttoken(statement)
    member = Trim(Replace(Replace(member, "(", ""), ")", ""))
  Else
    'Es: INCLUDE MEMBER=CICCIO
    member = Mid(token, InStr(token, "=") + 1)
  End If
  
  '''''''''''''''''''''''''''''''''''''
  ' Gestione variabili:
  '''''''''''''''''''''''''''''''''''''
  If InStr(member, "&") Then solveParameters member
  members = Split(member & "@", "@")
  For i = 0 To UBound(members) - 1
    'attenzione: riparsera e rilancia (ricorsivamente!) parseJCL:
    'PERDO TUTTE LE VARIABILI GLOBALI!!!!!!!!!!!!!!!!!!!
    MapsdM_Parser.checkRelations members(i), "MEMBER"
  
    ''''''''''''''''''''''''''''''''''
    ' Parsing oggetto relazionato
    ''''''''''''''''''''''''''''''''''
    If IsParseNeeded Then '
      'ATTENZIONE: PARSING INNESTATI!
      'VARIABILI GLOBALI DA SALVARE!!
      Dim currentLine As Long
      Dim statementLine As Long
      Dim idOggetto As Long
      Dim itemName As String
      'backup:
      currentLine = GbNumRec
      statementLine = GbOldNumRec
      idOggetto = GbIdOggetto
      itemName = GbNomePgm
      'serve backup? GbFileInput
      '
      Set rs = m_Fun.Open_Recordset("select estensione,Directory_Input from BS_oggetti where idoggetto = " & IdOggettoRel)
      GbNomePgm = members(i)  'serve?
      'Parser.AggErrLevel GbIdOggetto 'CHIARIRE...
      GbFileInput = m_Fun.FnPathDef & Mid(rs!Directory_Input, 2) & "\" & members(i)
      If Len(rs!estensione) Then
        GbFileInput = m_Fun.FnPathDef & Mid(rs!Directory_Input, 2) & "\" & members(i) & "." & rs!estensione
      End If
      rs.Close
      
      'Parsing:
      parseJCL
      
      'restore:
      GbNumRec = currentLine
      GbOldNumRec = statementLine
      GbIdOggetto = idOggetto
      GbNomePgm = itemName
    Else
      'Devo recuperare i Parametri dal Repository:
      Set rs = m_Fun.Open_Recordset("select name,[value] from PsJcl_Parameters where idoggetto = " & IdOggettoRel)
      While Not rs.EOF
        setParameter rs!Name, rs!value
        rs.MoveNext
      Wend
      rs.Close
    End If
  Next i
  
End Sub

Private Sub setParameterFromExec(parameterString As String)
  Dim statement As String, element As String, parameterName As String, parameterValue As String
  
  statement = parameterString
  While Len(statement)
    element = nextToken_tmp(statement)
    If InStr(1, element, "=") > 0 Then
      parameterName = Left(element, InStr(1, element, "=") - 1)
      parameterValue = Right(element, Len(element) - InStr(1, element, "="))
      setParameter parameterName, parameterValue
    End If
  Wend
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Gestione "Parameters"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub setParameter(parameterName As String, parameterValue As String, Optional onDb As Boolean = False)
  Dim isKey As Boolean, oldparametervalue As String
  Dim rs As Recordset
  
  On Error GoTo errPar
  
  'Pulizia apici esterni
  If Left(parameterValue, 1) = "'" Then
    If Right(parameterValue, 1) = "'" Then
      parameterValue = Mid(Left(parameterValue, Len(parameterValue) - 1), 2)
    Else
      m_Fun.writeLog "###item: " & GbIdOggetto & " - Parameter: " & parameterName & " - Value??: " & parameterValue
    End If
  End If
  
  'SQ - Attenzione: NON considero le value a space: verificare se scelta migliore!
  If Len(Trim(Replace(parameterValue, "'", ""))) = 0 Then Exit Sub
  
  '"Nested Parsing":
  If onDb Then
    'Repository: (serve solo per JCL?) - Attenzione: taglio a 60 chars...
    Set rs = m_Fun.Open_Recordset("select * from psJCL_Parameters where " & _
                                  "idoggetto = " & GbIdOggetto & " and name = '" & parameterName & "' and stepnumber = " & GbNumRec)
    If rs.RecordCount = 0 Then
      rs.AddNew
      rs!idOggetto = GbIdOggetto
      rs!Name = parameterName
      rs!value = Left(Replace(parameterValue, "'", "''"), 60)
      rs!stepNumber = GbNumRec
      rs.Update
    End If
    rs.Close
    'Parser.PsConnection.Execute "INSERT INTO PsJCL_Parameters (IdOggetto,Name,[Value],StepNumber) VALUES(" & GbIdOggetto & ",'" & parameterName & "','" & Replace(Left(parameterValue, 60), "'", "''") & "'," & GbNumRec & ")"
  End If
  'Lista di parametri: corrisponde (pi� o meno) al valore corrente
  'Collection: per sapere se un elemento � gi� presente, devo fare i salti mortali!
  oldparametervalue = parameters.item(parameterName)
  parameters.Remove parameterName
  parameters.Add oldparametervalue & parameterValue & "@", parameterName
  Exit Sub
errPar:
  'Bello! complimenti alle Collection!
  If err.Number = 5 Then
    'oggetto non presente: OK
    Resume Next
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "setParameter", err.source, err.description, False
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Esplode i parametri all'interno della stringa di input ("template")
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub solveParameters(template As String)
  Dim templates() As String, parameter As String, value As String
  Dim i As Integer
  Dim parameterEnd As Integer
  Dim values() As String
  Dim CleanParm As New Collection
  
  On Error GoTo catch
  
  templates = Split(template, "&")
  If UBound(templates) = 1 Then
    parameter = templates(1)
    'Lista di parametri separati da "@"
    values = Split(parameters.item(parameter), "@")
    ' Ciclo per eliminare i doppioni
    For i = 0 To UBound(values)
      On Error Resume Next
      CleanParm.Add values(i), values(i)
    Next i
    On Error GoTo catch
    template = ""
    For i = 1 To CleanParm.count
      If Len(CleanParm.item(i)) Then
        template = template & IIf(Len(template), "@", "") & CleanParm.item(i)
      End If
    Next i
  Else
    For i = 1 To UBound(templates)
      parameterEnd = InStr(templates(i) & ".", ".")
      parameter = Left(templates(i), parameterEnd - 1)
      'Lista di parametri separati da "@"
      
      values = Split(parameters.item(parameter), "@")
      value = values(0)
      If Len(value) Then
        'Sostituzione variabile:
        template = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ".", ""), value)
      Else
        'Segnalazione
      End If
    Next i
  End If
  
  Exit Sub
catch:
  'Bello! complimenti alle Collection!
  If err.Number = 5 Then
    'oggetto non presente: OK
    ReDim values(0)
    Resume Next
  Else
    MsgBox err.description
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Esplode i parametri all'interno della stringa di input ("template")
' => ritorna l'array degli n valori
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getParameters(template As String) As String()
  Dim templates() As String, parameter As String, value As String
  Dim i As Integer, a As Integer
  Dim parameterEnd As Integer
  Dim parametersArr() As String
  Dim isParChiusa As Boolean, isParAperta As Boolean
  Dim rs As Recordset, rsParam As Recordset, rsRel As Recordset
  
  On Error GoTo catch
  
  'SQ 12-07-07 - pezza:
  'caso: &&VARIABILE -> mi arriva "". Chiarire bene.
  ' Mauro 22/06/2010: SBAGLIATO! Se � un file, � giusto il && in testa!
  'template = Replace(template, "&&", "&")
  ReDim parametersArr(0)
  If Left(template, 2) = "&&" Then
    parametersArr(0) = template
    getParameters = parametersArr
    Exit Function
  End If
  templates = Split(template, "&")
  If UBound(templates) Then
    Set rsRel = m_Fun.Open_Recordset("select * from psrel_obj where idoggettor = " & GbIdOggetto)
    If rsRel.RecordCount = 0 Then
      For i = 1 To UBound(templates)
        parameterEnd = InStr(templates(i) & ".", ".")
        isParAperta = False
        If parameterEnd > Len(templates(i)) Then
          isParAperta = True
          parameterEnd = InStr(templates(i) & "(", "(")
        End If
        isParChiusa = False
        If parameterEnd > Len(templates(i)) Then
          isParChiusa = True
          parameterEnd = InStr(templates(i) & ")", ")")
        End If
        parameter = Left(templates(i), parameterEnd - 1)
        Set rsParam = m_Fun.Open_Recordset("select value from psjcl_parameters where " & _
                                           "idoggetto = " & GbIdOggetto & " and name = '" & parameter & "'")
        If Not rsParam.EOF Then
          value = rsParam!value
        End If
        rsParam.Close
        If Len(value) = 0 Then
          Set rsParam = m_Fun.Open_Recordset("select parameters from psjcl where " & _
                                             "idoggetto = " & GbIdOggetto & " and parameters <> ''")
          If Not rsParam.EOF Then
            ' Dovrebbe esserci solo un record
            value = getJclParam(parameter, rsParam!parameters)
          End If
          rsParam.Close
        End If
        If i = 1 Then
          If Len(value) Then
            'Sostituzione variabile:
            If isParChiusa Then
              parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ")", ""), value)
            Else
              If isParAperta Then
                parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter, value)
              Else
                parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ".", ""), value)
              End If
            End If
            ReDim Preserve parametersArr(UBound(parametersArr) + 1)
          End If
        Else
          If Len(value) Then
            If UBound(parametersArr) = 0 Then
              'Sostituzione variabile:
              If isParChiusa Then
                parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ")", ""), value)
              Else
                If isParAperta Then
                  parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter, value)
                Else
                  parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ".", ""), value)
                End If
              End If
              ReDim Preserve parametersArr(UBound(parametersArr) + 1)
            Else
              'Sostituzione variabile:
              If isParChiusa Then
                parametersArr(UBound(parametersArr) - 1) = Replace(parametersArr(UBound(parametersArr) - 1), "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ")", ""), value & ")")
              Else
                If isParAperta Then
                  parametersArr(UBound(parametersArr) - 1) = Replace(parametersArr(UBound(parametersArr) - 1), "&" & parameter, value)
                Else
                  parametersArr(UBound(parametersArr) - 1) = Replace(parametersArr(UBound(parametersArr) - 1), "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ".", ""), value)
                End If
              End If
            End If
          End If
        End If
      Next i
    Else
      Set rs = m_Fun.Open_Recordset("select parameters from psjcl where " & _
                                    "idoggetto in (select idoggettoc from psrel_obj where " & _
                                    "idoggettor = " & GbIdOggetto & ") and " & _
                                    "execname = '" & getNomeFromIdOggetto(GbIdOggetto) & "' and parameters <> ''")
      If rs.RecordCount Then
        Do Until rs.EOF
          For i = 1 To UBound(templates)
            parameterEnd = InStr(templates(i) & ".", ".")
            isParAperta = False
            If parameterEnd > Len(templates(i)) Then
              isParAperta = True
              parameterEnd = InStr(templates(i) & "(", "(")
            End If
            isParChiusa = False
            If parameterEnd > Len(templates(i)) Then
              isParChiusa = True
              parameterEnd = InStr(templates(i) & ")", ")")
            End If
            parameter = Left(templates(i), parameterEnd - 1)
            value = getJclParam(parameter, rs!parameters)
            If Len(value) = 0 Then
              Set rsParam = m_Fun.Open_Recordset("select value from psjcl_parameters where " & _
                                                 "idoggetto = " & GbIdOggetto & " and name = '" & parameter & "'")
              If Not rsParam.EOF Then
                value = rsParam!value
              End If
              rsParam.Close
            End If
            If i = 1 Then
              If Len(value) Then
                'Sostituzione variabile:
                If isParChiusa Then
                  parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ")", ""), value)
                Else
                  If isParAperta Then
                    parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter, value)
                  Else
                    parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ".", ""), value)
                  End If
                End If
                ReDim Preserve parametersArr(UBound(parametersArr) + 1)
              End If
            Else
              If Len(value) Then
                If UBound(parametersArr) = 0 Then
                  'Sostituzione variabile:
                  If isParChiusa Then
                    parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ")", ""), value)
                  Else
                    If isParAperta Then
                      parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter, value)
                    Else
                      parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ".", ""), value)
                    End If
                  End If
                  ReDim Preserve parametersArr(UBound(parametersArr) + 1)
                Else
                  'Sostituzione variabile:
                  If isParChiusa Then
                    parametersArr(UBound(parametersArr) - 1) = Replace(parametersArr(UBound(parametersArr) - 1), "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ")", ""), value & ")")
                  Else
                    If isParAperta Then
                      parametersArr(UBound(parametersArr) - 1) = Replace(parametersArr(UBound(parametersArr) - 1), "&" & parameter, value)
                    Else
                      parametersArr(UBound(parametersArr) - 1) = Replace(parametersArr(UBound(parametersArr) - 1), "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ".", ""), value)
                    End If
                  End If
                End If
              End If
            End If
          Next i
          rs.MoveNext
        Loop
      Else
        For i = 1 To UBound(templates)
          parameterEnd = InStr(templates(i) & ".", ".")
          isParAperta = False
          If parameterEnd > Len(templates(i)) Then
            isParAperta = True
            parameterEnd = InStr(templates(i) & "(", "(")
          End If
          isParChiusa = False
          If parameterEnd > Len(templates(i)) Then
            isParChiusa = True
            parameterEnd = InStr(templates(i) & ")", ")")
          End If
          parameter = Left(templates(i), parameterEnd - 1)
          Set rsParam = m_Fun.Open_Recordset("select value from psjcl_parameters where " & _
                                             "idoggetto = " & GbIdOggetto & " and name = '" & parameter & "'")
          If Not rsParam.EOF Then
            value = rsParam!value
          End If
          rsParam.Close
          If Len(value) = 0 Then
            Set rsParam = m_Fun.Open_Recordset("select parameters from psjcl where " & _
                                               "idoggetto = " & GbIdOggetto & " and parameters <> ''")
            If Not rsParam.EOF Then
              ' Dovrebbe esserci solo un record
              value = getJclParam(parameter, rsParam!parameters)
            End If
            rsParam.Close
          End If
          If i = 1 Then
            If Len(value) Then
              'Sostituzione variabile:
              If isParChiusa Then
                parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ")", ""), value)
              Else
                If isParAperta Then
                  parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter, value)
                Else
                  parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ".", ""), value)
                End If
              End If
              ReDim Preserve parametersArr(UBound(parametersArr) + 1)
            End If
          Else
            If Len(value) Then
              If UBound(parametersArr) = 0 Then
                'Sostituzione variabile:
                If isParChiusa Then
                  parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ")", ""), value)
                Else
                  If isParAperta Then
                    parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter, value)
                  Else
                    parametersArr(UBound(parametersArr)) = Replace(template, "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ".", ""), value)
                  End If
                End If
                ReDim Preserve parametersArr(UBound(parametersArr) + 1)
              Else
                'Sostituzione variabile:
                If isParChiusa Then
                  parametersArr(UBound(parametersArr) - 1) = Replace(parametersArr(UBound(parametersArr) - 1), "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ")", ""), value & ")")
                Else
                  If isParAperta Then
                    parametersArr(UBound(parametersArr) - 1) = Replace(parametersArr(UBound(parametersArr) - 1), "&" & parameter, value)
                  Else
                    parametersArr(UBound(parametersArr) - 1) = Replace(parametersArr(UBound(parametersArr) - 1), "&" & parameter & IIf(parameterEnd <= Len(templates(i)), ".", ""), value)
                  End If
                End If
              End If
            End If
          End If
        Next i
      End If
      rs.Close
    End If
    rsRel.Close
  End If
  If UBound(parametersArr) > 0 Then
    ReDim Preserve parametersArr(UBound(parametersArr) - 1)
  End If
  getParameters = parametersArr
  Exit Function
catch:
  'Bello! complimenti alle Collection!
  If err.Number = 5 Then
    'oggetto non presente: OK
    Resume Next
  Else
    'gestire
    m_Fun.writeLog "#getParameters-idOggetto: " & GbIdOggetto & " - " & err.description
  End If
End Function

Function getJclParam(parameter As String, jclParam As String) As String
  Dim Start As Integer, i As Integer
  Dim char As String
  Dim isPar As Boolean, isApice As Boolean
  
  getJclParam = ""
  Start = InStr(jclParam, parameter & "=")
  If Start Then
    Start = InStr(Start, jclParam, "=") + 1
    For i = Start To Len(jclParam)
      char = Mid(jclParam, i, 1)
      If char = "," Then
        If Not isPar And Not isApice Then
          Exit For
        End If
      ElseIf char = "'" Then
        isApice = Not isApice
      ElseIf char = "(" Then
        isPar = True
      ElseIf char = ")" Then
        isPar = False
      End If
      getJclParam = getJclParam & char
    Next i
  End If
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Elimina i commenti: tutto ci� che segue il primo space
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub cleanComments(statement As String)
  Dim pos As Integer
  
  statement = nextToken_ASM(statement)
  'pos = InStr(statement & " ", " ")
  'Attenzione: space di un "literal"
  'if instr(statement
  'statement = Left(statement, InStrRev(statement & " ", " ") - 1) 'elimino eventuali commenti
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getCallerParameters(idOggetto As Long)
  Dim rs As Recordset, rsMember As Recordset
  
  On Error GoTo catch
    
  If idOggetto <> GbIdOggetto Then
    Set rs = m_Fun.Open_Recordset("SELECT name,[value] FROM PsJCL_Parameters as b WHERE idOggetto=" & idOggetto)
    While Not rs.EOF
      setParameter rs!Name, rs!value
      rs.MoveNext
    Wend
    rs.Close
        
    'MEMBER
    Set rsMember = m_Fun.Open_Recordset( _
      "SELECT idOggettoR FROM PsRel_Obj WHERE idOggettoC = " & idOggetto & " AND relazione='INC'")
    While Not rsMember.EOF
      Set rs = m_Fun.Open_Recordset("SELECT name,[value] FROM PsJCL_Parameters as b WHERE idOggetto=" & rsMember!idOggettoR)
      While Not rs.EOF
        setParameter rs!Name, rs!value
        rs.MoveNext
      Wend
      rs.Close
      rsMember.MoveNext
    Wend
    rsMember.Close
    
    ' Mauro 25/02/2009
    'parametri del chiamante
    Set rs = m_Fun.Open_Recordset("SELECT a.[parameters] FROM PsJCL as a, bs_oggetti as b WHERE " & _
                                  "a.ExecName = b.nome and b.idoggetto = " & idOggetto)
    While Not rs.EOF
     If Not IsNull(rs!parameters) And Not rs!parameters = "" Then
      getDefaultParameters (rs!parameters)
     End If
     rs.MoveNext
    Wend
    rs.Close
  Else
    'parametri del chiamante
    Set rs = m_Fun.Open_Recordset("SELECT [parameters] FROM PsJCL WHERE ExecName ='" & GbNomePgm & "'")
    While Not rs.EOF
     If Not IsNull(rs!parameters) And Not rs!parameters = "" Then
      getDefaultParameters rs!parameters
     End If
     rs.MoveNext
    Wend
    rs.Close
  End If
  
  'CALLER
  Dim i As Integer
  'Non posso utilizzare i recordset ricorsivamente... c'� un limite!
  'Tengo un chiamante... poi vediamo!
  Set rs = m_Fun.Open_Recordset("SELECT idOggettoC FROM PsRel_Obj WHERE idOggettoR = " & idOggetto)
  If Not rs.EOF Then
    getCallerParameters rs!IdOggettoC ', rs!Tipo
    rs.MoveNext
  End If
  rs.Close
  Exit Sub
catch:
  'Bello! complimenti alle Collection!
  If err.Number = 5 Then
    'oggetto non presente: OK
    Resume Next
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "setParameter", err.source, err.description, False
  End If
End Sub

' Mauro 16/04/2010 : Calcola le righe "effettive" della JCL/PROC
Private Sub CalcolaLOCSJcl(wRec As String)
  wRec = Left(wRec, 72)
  
  If Len(Trim(wRec)) = 0 Then
    ' Riga vuota
  Else
    If Left(wRec, 3) = "//*" Then
      ' Commento
    Else
      LOCS = LOCS + 1
    End If
  End If
End Sub

Sub changeTypeObj(wIdOggetto As Long, itemType As String, area As String, livello1 As String)
  Dim rs As Recordset
  
  GbNumRec = 0
  GbErrLevel = ""

  Set rs = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & wIdOggetto)
  'SQ 26-11-06
  rs!Area_Appartenenza = area
  rs!livello1 = livello1
  rs!Tipo = itemType
  rs!parsinglevel = 0
  rs!DtParsing = Now
  rs.Update
  
  RelInitParser rs!nome
  rs.Close
End Sub

Function getNomeFromIdOggetto(idObj As Long) As String
  Dim rs As Recordset
  
  getNomeFromIdOggetto = ""
  Set rs = m_Fun.Open_Recordset("Select nome from Bs_Oggetti where idoggetto = " & idObj)
  If rs.RecordCount Then
    getNomeFromIdOggetto = rs!nome
  End If
  rs.Close
End Function
