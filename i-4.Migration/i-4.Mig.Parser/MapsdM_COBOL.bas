Attribute VB_Name = "MapsdM_COBOL"
Option Explicit
Option Compare Text

Global manageIO As Boolean
Global isCopyUsing As Boolean
'SILVIA: AGGIUNGO POS DEL "."
Dim posDot As Long
'' Temporaneo per gestione COPY IDMS
'Global wCOPY_IDMS As Boolean

'Uniformare con isReservedWord...
'Ex SeWordCobol
Public Function isReservedWord(word As String) As Boolean
  Dim tb As Recordset
  Dim wStr As String
  Dim k As Integer
  
  'SQ: ma perch�, potrei avere parole con apici?!
  wStr = word
  If Left(wStr, 1) = "'" Then
    wStr = Mid$(wStr, 2, Len(wStr) - 2)
  End If
  k = InStr(wStr, ",")
  If k > 0 Then
    wStr = Left(wStr, k - 1)
  End If
  wStr = Trim(wStr)
  'SQ !
  If Left(wStr, 1) = "*" Then
    isReservedWord = True
    Exit Function
  End If
  If Left(wStr, 1) = "-" Then
    isReservedWord = True
    Exit Function
  End If
  Select Case Trim(wStr)
    Case "EXEC"
      isReservedWord = True
    Case Else
      'Non pu� fare la query tutte le volte!
      'Provare con una struttura in memoria
      If InStr(wStr, "'") Then
        isReservedWord = False
      Else
        Set tb = m_Fun.Open_Recordset_Sys("select * from SyS_WordRes where " & _
                                          "linguaggio = 'COBOL2' and parola = '" & wStr & "'")
        isReservedWord = tb.RecordCount
        tb.Close
      End If
  End Select
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 11-09-07
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function isInstruction(word As String) As Boolean
  Dim tb As Recordset
  Dim wStr As String
  Dim k As Integer
  
  wStr = word
  If Left(wStr, 1) = "'" Then
    wStr = Mid$(wStr, 2, Len(wStr) - 2)
  End If
  k = InStr(wStr, ",")
  If k > 0 Then
    wStr = Left(wStr, k - 1)
  End If
  wStr = Trim(wStr)
  If Mid$(wStr, 1, 1) = "*" Then
    isInstruction = True
    Exit Function
  End If
  If Left(wStr, 1) = "-" Then
    isInstruction = True
    Exit Function
  End If
  Select Case Trim(wStr)
    Case "EXEC"
      isInstruction = True
    Case Else
      'Non puo' fare la query tutte le volte!
      'Provare con una struttura in memoria
      Set tb = m_Fun.Open_Recordset_Sys("select * from SyS_WordRes where linguaggio = 'COBOL2' and parola = '" & wStr & "' and Istruzione ")
      isInstruction = tb.RecordCount
      tb.Close
  End Select
End Function

Private Sub parseEntryInCopy()
  Dim rs As Recordset, statement As String, path As String, endStatement As Boolean
  Dim FDC As Integer, line As String, token As String, numpcb As Integer
  ' apertura file e richiamo
    
  Set rs = m_Fun.Open_Recordset("select * from BS_oggetti where IdOggetto = " & IdOggettoRel & " and tipo = 'CPY'")
  If Not rs.EOF Then
    If Trim(rs!estensione) = "" Then
      path = m_Fun.FnPathDef & "\" & Mid(rs!Directory_Input, 2) & "\" & rs!nome
    Else
      path = m_Fun.FnPathDef & "\" & Mid(rs!Directory_Input, 2) & "\" & rs!nome & "." & rs!estensione
    End If
    endStatement = False
    FDC = FreeFile
    Open path For Input As FDC
    'SQ phoenix: a cosa serve not EOF(FDC)?
    'Do Until Not EOF(FDC) And endStatement
    Do While Not EOF(FDC) Or endStatement
      Line Input #FDC, line
      If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
        line = Trim(Mid(line, 8, 65))
        If Len(line) Then
         statement = statement & " " & line
         If InStr(1, line, ".") > 0 Then
            endStatement = True
            Exit Do
         End If
        End If
      End If
    Loop
    statement = Replace(statement, ".", "")
    statement = Replace(statement, ",", "")
    token = nexttoken(statement)
    If Not isCopyUsing Then
      If token = "ENTRY" Then
        token = nexttoken(statement)
        If token = "DLITCBL" Then
          token = nexttoken(statement)  'USING
          If token = "USING" Then
            Set rs = m_Fun.Open_Recordset("select * from PsIMS_entry")
            token = nexttoken(statement)
            While Len(token)
              numpcb = numpcb + 1
              rs.AddNew
              rs!idOggetto = GbIdOggetto
              rs!numpcb = numpcb
              rs!pcb = token
              rs.Update
              token = nexttoken(statement)
            Wend
            rs.Close
          End If
        End If
      End If
    Else
      If token = "USING" Then
        Set rs = m_Fun.Open_Recordset("select * from PsIMS_entry")
        token = nexttoken(statement)
        While Len(token)
          numpcb = numpcb + 1
          rs.AddNew
          rs!idOggetto = GbIdOggetto
          rs!numpcb = numpcb
          rs!pcb = token
          rs.Update
          token = nexttoken(statement)
        Wend
        rs.Close
      End If
    End If
    Close #FDC
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Rifatto TUTTO IL PARSING DI PRIMO LIVELLO:
' Proviamo a leggere ogni file SOLO una volta anzich� 800...
' riconoscere il tipo di statement e invocare SOLO la routine specifica
' IMP!!!!!!!!: ogni call viene scritta in tabella PsCall+PsDli_Istruzioni
' Parsing II, incapsulamento e tutto il possibile acceder� alla tabella
' senza rileggere il file.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub parsePgm_I()
  Dim FD As Long
  Dim line As String, token As String, execStatement As String
  Dim rsOggetti As Recordset
  Dim i As Integer, j As Integer, k As Integer, manageMove As Boolean
  Dim copyEntry As Boolean, isLabel As Boolean
  Dim rsRel As Recordset
  
  'Dim rsMoved As Recordset
  Dim response As String
  Dim retvalue As Integer
  'Dim IdOggettoCmp As Long
  'Dim idArea As Integer
  'Dim Ordinale As Integer
  
  Dim ProgramId As String, nomePgm As String, rsLoad As Recordset
  Dim rsUnk As Recordset, rsError As Recordset
  On Error GoTo parseErr
  
  ReDim CampiMove(0)
  ReDim CampiMoveCpy(0)
  ReDim DataValues(0)
  ReDim CmpIstruzione(0)
  'SQ 28-12-06
  ReDim DataValues(0)
  
  copyEntry = False
  
  'Non leggiamo tutte le volte!
  manageMove = m_Fun.LeggiParam("IMSDC-MOVEYN") = "YES"
  manageIO = m_Fun.LeggiParam("IMSDC-IOYN") = "YES"
  
  'Init flag per riconoscimento tipo programma... (flag da sistemare - perlomeno rinominare...)
  SwTpCX = False
  SwTpIMS = False
  SwDli = False
  SwSql = False
  SwBatch = False
  SwVsam = False
  SwMapping = False
  
  ReDim assign_files(0) 'SELECT/ASSIGN
  
  ' Mauro 23/07/2008 : E se il file non c'�? Non usciva mai!!!
  If Len(Dir(GbFileInput)) = 0 Then
    MsgBox "This file is not avaliable...", vbExclamation, Parser.PsNomeProdotto
    Exit Sub
  End If
  
  GbNumRec = 0
  FD = FreeFile
  Open GbFileInput For Input As FD
  ' Mauro 14/11/2007 : Se l'ultima riga comprende 2 istruzioni diverse, la seconda va persa
  'While Not EOF(FD)
  'IRIS: va in loop qui...
  While Not EOF(FD) Or Len(line)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      ''''''''''''''''''''''''''
      'Gestione LABEL
      ''''''''''''''''''''''''''
      isLabel = Mid(line, 8, 1) <> " "
    Else
      'Ho gia' una linea da analizzare (letta dalla getCall, per es.)
      isLabel = False
    End If
    
    If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
      'LINEA VALIDA
      line = RTrim(Mid(line, 8, 65)) 'elimino etichette!
      'token = nextToken(line)
      token = nextToken_new(line) 'MANTIENE GLI APICI!!
      Select Case token
        Case "PROCEDURE"
          token = nextToken_new(line)
          If token = "DIVISION" Then
            getUsingParameters line, FD
          End If
          copyEntry = True
          line = ""
        ' parameter move
        Case "AUTHOR.", "FILE-CONTROL."   ',... 'SQ 2-2-07 per non incasinare la gestione delle LABEL...
          line = ""
          copyEntry = False
        ' Mauro 22/03/2010 : Se il ProgramId � diverso dal nome esterno del Pgm, li salvo sulla PsLoad
        Case "PROGRAM-ID."
          Do Until EOF(FD) Or line <> ""
            Line Input #FD, line
            GbNumRec = GbNumRec + 1
            If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
              'LINEA VALIDA
              line = RTrim(Mid(line, 8, 65)) 'elimino etichette!
            Else
              line = ""
            End If
          Loop
          ProgramId = Replace(Trim(UCase(nexttoken(line))), ".", "")
          ProgramId = Replace(ProgramId, """", "")
          Set rsLoad = m_Fun.Open_Recordset("select nome from bs_oggetti where idoggetto = " & GbIdOggetto)
          nomePgm = Trim(UCase(rsLoad!nome))
          rsLoad.Close
          If ProgramId <> nomePgm Then
            Set rsLoad = m_Fun.Open_Recordset("select * from psload where " & _
                                              "source = '" & nomePgm & "' and load = '" & ProgramId & "'")
            If rsLoad.RecordCount = 0 Then
              rsLoad.AddNew
              rsLoad!source = nomePgm
              rsLoad!load = ProgramId
              rsLoad.Update
            End If
            rsLoad.Close
            ' Mauro 11/08/2010 : Se ho pi� Program-Id. nello stesso programma?
            Set rsUnk = m_Fun.Open_Recordset("select * from psrel_objUnk where " & _
                                             "nomeoggettoR = '" & ProgramId & "' and idoggettoc = " & GbIdOggetto)
            If rsUnk.RecordCount Then
              Set rsError = m_Fun.Open_Recordset("select * from bs_segnalazioni where " & _
                                                 "idoggetto = " & GbIdOggetto & " and var1 ='" & ProgramId & "'")
              If rsError.RecordCount Then
                rsError.Delete
                rsError.Update
              End If
              rsError.Close
              rsUnk.Delete
              rsUnk.Update
            End If
            rsUnk.Close
          End If
          line = ""
          copyEntry = False
        'T 18-08-2009 gestione "REMARKS"
        Case "REMARKS."
          Line Input #FD, line
          GbNumRec = GbNumRec + 1
          Do Until (Mid(line, 7, 1) = " " And Mid(line, 8, 1) <> " ") Or EOF(FD)
            Line Input #FD, line
            GbNumRec = GbNumRec + 1
          Loop
          line = ""
          copyEntry = False
        Case "MOVE"
          If manageMove Then
            getMove line, FD, CampiMove
          Else
            line = ""
          End If
          copyEntry = False
        ' Mauro 14/01/2009 : "++INCLUDE" trovato nel prj ACS
        'Case "COPY"
        Case "COPY", "++INCLUDE"
'          wCOPY_IDMS = False
          getCOPY line 'COPY
          'line = ""
          If copyEntry Then
            parseEntryInCopy
            isCopyUsing = False
          End If
          copyEntry = False
        Case "CALL"
          getCALL line, FD 'attenzione: modifica line
          copyEntry = False
        Case "EXEC"
          posDot = 0
          execStatement = getEXEC(line, FD)
          posDot = InStr(line, ".")
          token = nexttoken(execStatement)
          Select Case token
            Case "DLI"
              getEXEC_DLI execStatement
            Case "SQL"
              getEXEC_SQL execStatement
            Case "CICS"
              getEXEC_CICS execStatement
          End Select
          line = ""
          copyEntry = False
        Case ""
          'nop
          'copyentry = False
        Case "IF"
          line = ""
          copyEntry = False
        ' Mauro 30/07/2008: Se avevo un EXEC sulla stessa riga, brasava tutto
        ' Metto i trattini per evitare problemi con la RTRIM in testa alla SELECT
        Case "THEN"
          line = "-------" & line
          copyEntry = False
        Case "ELSE"
          line = "-------" & line
          copyEntry = False
        Case "ENTRY"
          parseEntry line, FD
          If Not isCopyUsing Then
            copyEntry = False
          End If
        Case "SELECT"
          parseSELECT line, FD
          line = ""
          copyEntry = False
        Case "OPEN"
          parseOPEN token & " " & line, FD 'ale modificata
          'line = ""
          copyEntry = False
        Case "CLOSE"
          line = token & " " & line
          parseCLOSE line, FD 'ale modificata
          'line = ""
          copyEntry = False
        Case "WRITE", "REWRITE"
          'FARE!
          line = token & " " & line
          parseWRITE line, FD
          'line = ""
          copyEntry = False
        Case "DELETE"
          parseDELETE token & " " & line, FD
          line = ""
          copyEntry = False
        Case "START"
          line = token & " " & line
          parseSTART line, FD
          'line = ""
          copyEntry = False
        Case "READ"
          line = token & " " & line
          parseREAD line, FD 'ale modificata
          'line = ""
          copyEntry = False
        Case "FD"
          getFD line, FD
          copyEntry = False
        Case Else
          'SQ 2-2-07
          'PROBLEMA: 2 statement su una riga;
          'Per ora gestiamo solo questi casi: 1648-REPL-INSHED-VIA-PART.      COPY INIO1648.
          '''''''''''''''''''''''''''''''''''''''''''''''''
          'Gestione LABEL
          '''''''''''''''''''''''''''''''''''''''''''''''''
          If isLabel Then
            'Label "potenziale"
            If Right(token, 1) <> "." Then
              'il punto puo' essere separato...
              token = nextToken_new(line)
              isLabel = token = "."
            End If
          End If
          If isLabel Then
            'Label "sicura"
            'Non devo pulire line! Riparto con l'analisi con il resto della riga pulita!
            'Formatto per far funzionare il giro "generico"
            line = Space(11) & line
          Else
            line = ""
          End If
          'SQ 17-04-07
          'copyentry = False
      End Select
    Else
      line = ""
    End If
  Wend
  Close FD
 
  ''''''''''''''''''''''''''''''''''''''
  'SQ 12-11-07
  ' Non sono pi� errori di "parsing"...
  ' 1) Altra gestione errori +
  ' 2) Scrittura DB subito, se lo salta � un casino!
  ' 3) Tolto ultimo ramo nel parseErr che NON resumava...
  ''''''''''''''''''''''''''''''''''''''
  On Error GoTo catch
  
  Set rsOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  rsOggetti!DtParsing = Now
  rsOggetti!ErrLevel = GbErrLevel
  rsOggetti!parsinglevel = 1
  rsOggetti!Cics = SwTpCX
  rsOggetti!Batch = SwBatch
  rsOggetti!DLI = SwDli 'sarebbe IMS!
  rsOggetti!WithSQL = SwSql
  rsOggetti!VSam = SwVsam
  rsOggetti!mapping = SwMapping
  rsOggetti.Update
  ' Mauro 28/09/2009 : Aggiornamento Flag DLI per i pgm che richiamano l'Include appena parserata
  If rsOggetti!Tipo = "CPY" And SwDli Then
    Set rsRel = m_Fun.Open_Recordset("select idoggettoC from psrel_obj where idoggettoR = " & rsOggetti!idOggetto)
    Do Until rsRel.EOF
      Parser.PsConnection.Execute "update bs_oggetti set dli = True where idoggetto = " & rsRel!IdOggettoC & " and dli = false"
      rsRel.MoveNext
    Loop
    rsRel.Close
  End If
  rsOggetti.Close
  
  ' parameter move
  If manageMove Then
    For i = 1 To UBound(CampiMove)
      Parser.PsConnection.Execute "Update PsData_Cmp Set Moved = true where Nome = '" & CampiMove(i).NomeCampo & "' and (IdOggetto = " & GbIdOggetto & " or IdOggetto in (Select IdOggettoR from PsRel_Obj where IdOggettoC = " & GbIdOggetto & " and Relazione = 'CPY') )"
      'stefano: si, provare, da rifare con cura
      'stefano: verifica se il campo � in copy
      'Set rsMoved = m_Fun.Open_Recordset("Select * from PsData_Cmp where Nome = '" & CampiMove(i).NomeCampo & "' and IdOggetto in (Select IdOggettoR from PsRel_Obj where IdOggettoC = " & GbIdOggetto & " and Relazione = 'CPY')")
      ' rivedere cambiata tabella
      'If Not rsMoved.EOF Then
         'Parser.PsConnection.Execute "Insert into PsData_CmpMoved (IdOggetto,IdArea,Ordinale,IdPgm) values(" & rsMoved!idOggetto & ", " & rsMoved!idArea & ", " & rsMoved!Ordinale & ", " & GbIdOggetto & ")"
         'rsMoved.Close
      'End If
    Next i
     
    'AC  scrittura tabelle  campimoved/valori
    For i = 1 To UBound(CmpIstruzione) ' alla fine la struttura mi serve solo
                                       ' per fissare le istruzioni non risolte
           
      ' verifichiamo se istruzione risolta
      If CmpIstruzione(i).Risolta = 0 Then
        response = risolviIstruzioneByMove(CmpIstruzione(i).CampoIstruzione, retvalue, CmpIstruzione(i).LastSerchIndex)
        If Len(response) Then
          ' update Psdli_Istruzioni, delete segnalazione
          Parser.PsConnection.Execute "Update PsDLI_Istruzioni Set Istruzione = '" & response _
                                    & "' where IdOggetto =" & CmpIstruzione(i).Oggetto & "' and Riga =" _
                                    & CmpIstruzione(i).Riga
          Parser.PsConnection.Execute "Delete from Bs_Segnalazioni where IdOggetto = " _
                                     & CmpIstruzione(i).Oggetto & " and Riga = " & CmpIstruzione(i).Riga _
                                     & " and ( Codice = 'H09' Or Codice = 'F09')"
        End If
      End If
    Next i
    For j = 1 To UBound(CampiMove)
      ' aggiornamento tabella campi  PsData_CmpMoved
      ' AreaFromCampo GbIdOggetto, CampiMove(j).NomeCampo, IdOggettoCmp, IdArea, Ordinale
      'If Not idArea = -1 Then
      For k = 1 To CampiMove(j).Valori.count
        'If CampiMove(j).NomeCampo = "II405-SSA-AREA" Then Stop
        Parser.PsConnection.Execute "Insert Into PsData_CmpMoved (IdPgm,Nome,valore,destSTART) Values (" _
                                    & GbIdOggetto & ",'" & CampiMove(j).NomeCampo & "','" & Replace(Left(CampiMove(j).Valori.item(k), 255), "'", "''") & "','" & CampiMove(j).Posizioni.item(k) & "')"
      Next k
      'End If
    Next j
  End If
  
  Exit Sub
parseErr:  'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        Do Until Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/"
          Line Input #FD, line
          GbNumRec = GbNumRec + 1
        Loop
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          GbOldNumRec = GbNumRec
          addSegnalazione line, "P00", line
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        GbOldNumRec = GbNumRec
        addSegnalazione line, "P00", line
        'm_Fun.WriteLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & Err.Description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
    m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
  Else
    GbOldNumRec = GbNumRec
    addSegnalazione "#!#" & line, "P00", "#!#" & line
    'SQ 12-11-07 - IMPORTANTISSIMO! VEDI SOPRA...
    'SQ - 20-11-06
    'On Error Resume Next
    'Close FD
    Resume Next
  End If
  Exit Sub
catch:
  'SQ Gestire!
  GbOldNumRec = 0
  addSegnalazione "#PARSING TERMINATED: MOVE MANAGING#", "P00", "#PARSING TERMINATED: MOVE MANAGING#"
End Sub

Private Sub getPositionalMove(pStatement As String, pStart As String, pEnd As String)
  Dim appoStatement As String
  Dim posDuePunti As Integer, posPrimaParentesi As Integer, posParentesi As Integer
  
  On Error GoTo errposmove
  appoStatement = pStatement
  If Right(appoStatement, 1) = "." Then
    appoStatement = Left(appoStatement, Len(appoStatement) - 1)
  End If
  If InStr(appoStatement, "(") And InStr(appoStatement, ")") And InStr(appoStatement, ":") Then
    posDuePunti = InStr(appoStatement, ":")
    posPrimaParentesi = InStrRev(appoStatement, "(", posDuePunti)
    pStart = Mid(appoStatement, posPrimaParentesi + 1, posDuePunti - posPrimaParentesi - 1)
    pEnd = Mid(appoStatement, posDuePunti + 1, InStr(posDuePunti, appoStatement, ")") - posDuePunti - 1)
  End If
  Exit Sub
errposmove:
  pStart = ""
  pEnd = ""
  Exit Sub
End Sub

Private Sub getMove(line As String, FD As Long, ByRef pcampiMove() As UtCampiMove)
  Dim statement As String, variable As String, token As String
  Dim endStatement As Boolean
  Dim linee As Long
  Dim campo As String, campoof As String, campoend As String
  Dim pStart As String, pEnd As String ' move posizionale (sulla var di destinazione)
  Dim i As Integer, j As Integer
  Dim variables() As String
  Dim bolCostant As Boolean, bolnumeric As Boolean
  Dim pCampiMoveMax As Long
  
  On Error GoTo callErr
  
  GbOldNumRec = GbNumRec  'linea iniziale
  ''''''''''''''''''''''''''''''''''
  ' Becco tutte le linee di call...
  ''''''''''''''''''''''''''''''''''
  'If InStr(line, "QSSA-01I-E") > 0 Then Stop
  statement = line
  If Len(line) = 0 Then line = " "
  'If Mid(line, Len(line), 1) <> "." Then
  If Mid(line, Len(line), 1) <> "." Or (CountWords(line, "'") Mod 2) = 1 Then
    While Not EOF(FD) And Not endStatement
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
        If Mid(line, 7, 1) <> "-" Then
          line = Trim(Mid(line, 8, 65))
          ' posso avere una linea vuota? (prob. no...)
          If Len(line) Then
            'Altro statement?
            If isCobolLabel(line) Then 'LABEL!
              endStatement = True
              linee = GbNumRec - GbOldNumRec
            Else
              'token = nextToken(line)
              token = nextToken_newnew(line)
              'Parola Cobol?
              'SQ 27-04-06: se mi arriva END-IF./END-CALL. con il punto sbaglia!
              If token <> "TO" And token <> "LENGTH" And token <> "OF" And _
                 isReservedWord(IIf(Right(token, 1) = ".", Left(token, IIf(Len(token) = 0, 1, Len(token)) - 1), token)) Then
                endStatement = True
                'linea buona per il giro successivo!
                line = Space(8) & token & " " & line
                'SQ 4-05-06
                'Mi serve linee perch� nei casi di "isReservedWord", gbnumrec uno in pi�
                linee = GbNumRec - GbOldNumRec
              Else
                'linea buona:
                statement = statement & " " & token & " " & line
                'SQ - Fine statement con Punto finale o ELSE!
                If Len(line) Then
                  If Mid(line, Len(line), 1) = "." Then
                    endStatement = True
                    linee = GbNumRec - GbOldNumRec + 1
                    line = "" 'e' il flag... per la nuova lettura
                  ElseIf Right(" " & line, 5) = " ELSE" Then
                    endStatement = True
                    linee = GbNumRec - GbOldNumRec
                    line = "" 'e' il flag... per la nuova lettura
                    'Mangio l'ELSE finale
                    statement = Trim(Left(statement, Len(statement) - 4))
                  Else
                    endStatement = False
                  End If
                Else
                  endStatement = Mid(token, Len(token), 1) = "."
                  linee = GbNumRec - GbOldNumRec + 1
                End If
              End If
            End If
          End If
        Else
          line = Trim(Mid(line, 8, 65))
          line = Mid(line, 2)
          statement = statement & " " & line
        End If
      End If
    Wend
  Else
    'SQ 14-02-06: se CALL su unica riga, rimane "line" sporca...
    line = ""
  End If
  
  token = ""
  If Left(statement, 1) = "'" Then
    bolCostant = True
  Else
    bolCostant = False
  End If
  variable = Replace(nexttoken(statement), ".", "")
  If variable = "ALL" Then
    If Left(statement, 1) = "'" Then
      bolCostant = True
    Else
      bolCostant = False
    End If
    variable = Replace(nexttoken(statement), ".", "")
  End If
  If IsNumeric(variable) Then
    bolnumeric = True
  Else
    bolnumeric = False
  End If
  If bolCostant Or bolnumeric Then
    variable = "'" & variable & "'"
  End If
  token = nexttoken(statement)
  Do While Left(statement, 1) = "'" Or UCase(token) <> "TO"
    If bolCostant Or bolnumeric Then
      variable = variable & "'" & token & "'"
    End If
    token = nexttoken(statement)
    If Len(statement) = 0 Then
      Exit Do
    End If
  Loop
  'tmp: indici! li eliminiamo... GESTIRE!
  
  pStart = ""
  pEnd = ""
  
  'If Left(statement, 1) = "(" Then token = nexttoken(statement)
  'token = nexttoken(statement) 'serve solo a mangiare l'eventuale "TO"
  While Len(statement)
    ' intercetto move posizionale (solo sul campo di destinazione)
    getPositionalMove statement, pStart, pEnd
    campo = nexttoken(statement)
    'tmp: indici! li eliminiamo... GESTIRE!
    If Left(statement, 1) = "(" Then token = nexttoken(statement)
    campoof = nexttoken(statement)
    ' se successivo OF compongo campo
    ' altrimenti ricostruisco statement
    If campoof = "OF" Then
      campoend = nexttoken(statement)
      campo = campo & " OF " & campoend
    ElseIf Not campoof = "" Then ' ero gi� alla fine
      statement = campoof & " " & statement
    End If
            
    If Right(campo, 1) = "." Then
      campo = Left(campo, Len(campo) - 1)
    End If
    For i = 1 To UBound(pcampiMove)
      If pcampiMove(i).NomeCampo = campo Then
        Exit For
      End If
    Next i
    
    If i = UBound(pcampiMove) + 1 Then ' campo nuovo
      ' La GetFieldValues potrebbe aggiungere nuovi campi alla struttura
      pCampiMoveMax = UBound(pcampiMove) + 1
      ReDim Preserve pcampiMove(pCampiMoveMax)
      pcampiMove(pCampiMoveMax).NomeCampo = Replace(campo, ",", "")
      Set pcampiMove(pCampiMoveMax).Valori = New Collection
      'pcampiMove(pCampiMoveMax).Valori.Add variable
      ReDim variables(0)
      If InStr(variable, "'") = 0 Then
        variables = MapsdM_Parser.getFieldValues(variable)
      End If
      If Len(variables(0)) Then
        pcampiMove(pCampiMoveMax).Valori.Add variables(0)
      Else
        pcampiMove(pCampiMoveMax).Valori.Add variable
      End If
      Set pcampiMove(pCampiMoveMax).Posizioni = New Collection
      pcampiMove(pCampiMoveMax).Posizioni.Add pStart
    Else
      For j = 1 To pcampiMove(i).Valori.count
        If pcampiMove(i).Valori.item(j) = variable Then
          Exit For
        End If
      Next j
      If j = pcampiMove(i).Valori.count + 1 Then
        ' Mauro 12/01/2009: Pezza Temporanea per ricavare i valori dalle variabili
        ReDim variables(0)
        'Stefano RP promo a rimuovere la pezza che fa casino sulle SSA, ma � un rischio per i singoli campi...
        'If variable = "II405000-SSA-QUAL-1" Then Stop
        'If InStr(variable, "'") = 0 Then
        '  variables = MapsdM_Parser.getFieldValues(variable)
        'End If
        If Len(variables(0)) Then
          pcampiMove(i).Valori.Add variables(0)
        Else
          pcampiMove(i).Valori.Add variable
        End If
        pcampiMove(i).Posizioni.Add pStart
      End If
    End If
    If Trim(statement) = "." Then statement = ""
  Wend
  
  Exit Sub
  
' Mauro 07/08/2007 : Modificata la gestione per stringhe a capo
''callErr:
''  If err.Number = 123 Then
''    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
''    If Left(line, 1) = "(" Then
''      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
''      'Butto anche via la successiva...
''      Line Input #FD, line
''      GbNumRec = GbNumRec + 1
''      '...
''    Else
''      err.Raise 124, "getMove", "syntax error on " & statement
''      'Resume
''    End If
''  End If
  'gestire...
'''  If Err.Number = -2147217887 Then  'chiave duplicata...
'''    '2 CALL su una linea...
'''  Else
'''    Err.Raise 124, "getMove", "syntax error on " & statement
'''    Resume
'''  End If
callErr:
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
      Resume Next
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
          Resume Next
        Else
          GbOldNumRec = GbNumRec
          addSegnalazione line, "P00", line
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        GbOldNumRec = GbNumRec
        addSegnalazione line, "P00", line
        'm_Fun.WriteLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & Err.Description
      End If
    End If
    'SQ 15-10-07 - casi di loop! (messo sopra, ma probabilmente meglio buttare!
    'Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    Resume Next
  Else
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    'SQ - 20-11-06
    On Error Resume Next
    Close FD
  End If
End Sub

'tmp public
Public Sub getEXEC_CICS(statement As String)
  Dim wResp As String
  Dim k As Integer
  Dim k1 As Double
  Dim rTipo As String, wStr1 As String, Wstr2 As String, wArea As String, wLink As String
  Dim WnomeTab As String
  Dim TbIstr As Recordset
  Dim wAmb As String, wComando As String

  SwTpCX = True

  'SQ '''''''''''''''''CreaIstrAmbExec GbIdOggetto, GbNumRec, statement, "CICS"
'  K = InStr(statement, " ")
'  If K > 0 Then
'    'wComando = Left(statement, K - 1)
'    'wStr1 = statement
'    'wStr1 = Trim(Replace(wStr1, wComando, ""))  'SQ: si mangia READ ovunque!!!!
'
'  Else
'    wComando = statement
'    wStr1 = ""
'  End If
  ' Mauro 14-02-2007
  statement = Replace(statement, """", "'")
  wStr1 = statement   'c'� gi� buffer?!
  wComando = nextToken_new(wStr1)

  Set TbIstr = m_Fun.Open_Recordset("select * from PsExec where idoggetto = " & GbIdOggetto)
  TbIstr.AddNew
  TbIstr!idOggetto = GbIdOggetto
  TbIstr!area = "CICS"
  TbIstr!Riga = GbOldNumRec
  TbIstr!comando = wComando
  TbIstr!istruzione = wStr1
  TbIstr!linee = GbNumRec - GbOldNumRec + 1
  '''silvia
  If posDot > 0 Then
    TbIstr!dot = True
  End If
  '''
  TbIstr.Update
  TbIstr.Close
  
  'SQ wbuffer = " CICS " & statement & " END-EXEC."
    
  '''''''''''''''''''''''''''''''''
  k = InStr(statement, " DATASET")
  '''''''''''''''''''''''''''''''''
  If k > 0 Then
      wResp = LTrim(Mid(statement, k + 8))
      k = InStr(wResp, ")")
      If k > 0 Then wResp = Mid$(wResp, 1, k - 1)
      wResp = Trim(wResp)
      k = InStr(wResp, "(")
      If k > 0 Then wResp = Mid(wResp, k + 1)
      wLink = Trim(wResp)
      k = InStr(wResp, "'")
      While k > 0
         Mid$(wResp, k, 1) = " "
         k = InStr(wResp, "'")
      Wend
      wStr1 = Trim(wResp)
      '
      AggiungiComponente wStr1, wStr1, "AGN", "DATASET"
      wStr1 = ""
      k = InStr(statement, " FROM")
      If k > 0 Then
          wStr1 = Mid(statement, k)
      End If
      k = InStr(statement, " INTO")
      If k > 0 Then
          wStr1 = Mid(statement, k)
      End If
      If Len(Trim(wStr1)) Then
        k = InStr(wStr1, ")")
        If k > 0 Then wStr1 = Left(wStr1, k - 1)
        k = InStr(wStr1, "(")
        If k > 0 Then wStr1 = Mid(wStr1, k + 1)
        wArea = wStr1
        '''''''''''''''''''''''''''''''''
        '
        '''''''''''''''''''''''''''''''''
        checkFileRelation wLink, Replace(Replace(wArea, "'", ""), """", "")
       End If
      SwVsam = True
      Exit Sub
  End If
  '''''''''''''''''''''''''''''''''
  k = InStr(statement, " TRANSID")
  '''''''''''''''''''''''''''''''''
  If k > 0 Then
    wResp = statement 'serve wResp?
    rTipo = "START"
    If InStr(wResp, "RETURN") > 0 Then rTipo = "RETURN"
    wResp = Mid$(wResp, k + 8)
    k = InStr(wResp, ")")
    If k > 0 Then wResp = Mid$(wResp, 1, k - 1)
    wResp = Trim(wResp)
    k = InStr(wResp, "(")
    If k > 0 Then wResp = Mid$(wResp, k + 1)
    k = InStr(wResp, "'")
    While k > 0
      Mid$(wResp, k, 1) = " "
      k = InStr(wResp, "'")
    Wend
    wStr1 = Trim(wResp)
    AggiungiComponente wStr1, wStr1, "TRN", rTipo
    Exit Sub
  End If
  '''''''''''''''''''''''''''''''
    k = InStr(statement, " DUMPCODE")
  '''''''''''''''''''''''''''''''''
  If k > 0 Then
    wResp = statement 'serve wResp?
    rTipo = "START"
    If InStr(wResp, "RETURN") > 0 Then rTipo = "RETURN"
    wResp = Mid$(wResp, k + 10)
    k = InStr(wResp, ")")
    If k > 0 Then wResp = Mid$(wResp, 1, k - 1)
    wResp = Trim(wResp)
    k = InStr(wResp, "(")
    If k > 0 Then wResp = Mid$(wResp, k + 1)
    k = InStr(wResp, "'")
    While k > 0
      Mid$(wResp, k, 1) = " "
      k = InStr(wResp, "'")
    Wend
    wStr1 = Trim(wResp)
    AggiungiComponente wStr1, wStr1, "TRN", rTipo
    Exit Sub
  End If
  '''''''''''''''''''''''''''''''
  '
  '''''''''''''''''''''''''''''''
  k = InStr(statement, " MAPSET")
  If k > 0 Then
      ''''rTipo = "MAPPING"
      wResp = Mid(statement, k + 8)
      k = InStr(wResp, ")")
      If k > 0 Then wResp = Mid$(wResp, 1, k - 1)
      wResp = Trim(wResp)
      k = InStr(wResp, "(")
      If k > 0 Then wResp = Trim(Mid(wResp, k + 1))
      ''''''''''''''''''''''''''''''''''''''''''''''
      ' SQ 16-02-06 - gestione segnalazioni
      ''''''''''''''''''''''''''''''''''''''''''''''
      k = InStr(wResp, "'")
      If k > 0 Then
        ''''''''''''''''''''''''''''''''''''''''''''''
        ' Nome BMS costante
        ''''''''''''''''''''''''''''''''''''''''''''''
            While k > 0
               Mid$(wResp, k, 1) = " "
               k = InStr(wResp, "'")
            Wend
            wStr1 = Trim(wResp)
            wResp = statement
            ''''''''''''''''''''''''
            ' Nome Mappa
            ''''''''''''''''''''''''
            k = InStr(wResp, " MAP ")
            If k = 0 Then
               k = InStr(wResp, " MAP(")
            End If
            wResp = Mid$(wResp, k + 4)
            k = InStr(wResp, ")")
            If k > 0 Then wResp = Mid$(wResp, 1, k - 1)
            wResp = Trim(wResp)
            k = InStr(wResp, "(")
            If k > 0 Then wResp = Mid$(wResp, k + 1)
            k = InStr(wResp, "'")
            While k > 0
               Mid$(wResp, k, 1) = " "
               k = InStr(wResp, "'")
            Wend
            Wstr2 = Trim(wResp)
            
            'SQ: (checkRelations sotto)
        SwNomeCall = True
      Else
        ''''''''''''''''''''''''''''
        ' Nome BMS dinamico
        ''''''''''''''''''''''''''''
        SwNomeCall = False
        wStr1 = wResp
      End If
      '''''''''''''''''''''''''''''''''
      ' Gestione relazioni/segnalazioni
      '''''''''''''''''''''''''''''''''
      MapsdM_Parser.checkRelations wStr1, "MAPSET"
'      Set tbComponenti = m_Fun.Open_Recordset("select * from PsCom_Obj where relazione = 'MPS' and nomeoggettor = '" & wStr1 & "'")
'      If tbComponenti.RecordCount > 0 Then
'        tbComponenti.MoveLast
'
'        If TbOggetti.RecordCount = 1 Then
'          AggiungiRelazione TbOggetti!idOggetto, rTipo, Wstr2, Wstr2
'        End If
'      Else
'        AggiungiSegnalazione wStr1, "NO" & rTipo, wStr1
'      End If
      Exit Sub
  End If
  '''''''''''''''''''''''''''''''''''''''''''
  ' PGM
  '''''''''''''''''''''''''''''''''''''''''''
  SwNomeCall = False
  k = InStr(statement, " PROGRAM")  'HO GIA' WCOMANDO!!!!!!!!!!!!!!!!
  If k > 0 Then
      rTipo = "LINK"
      If InStr(statement, " XCTL") > 0 Then rTipo = "XCTL"  'HO GIA' WCOMANDO!!!!!!!!!!!!!!!!
      If InStr(statement, " LOAD") > 0 Then rTipo = "LOAD"
      wResp = Mid(statement, k + 8)
      k = InStr(wResp, ")")
      If k > 0 Then wResp = Mid$(wResp, 1, k - 1)
      wResp = Trim(wResp)
      k = InStr(wResp, "(")
      If k > 0 Then wResp = Mid$(wResp, k + 1)
      k = InStr(wResp, "'")
      If k = 0 Then
        SwNomeCall = False
      Else
        While k > 0
          SwNomeCall = True
          Mid$(wResp, k, 1) = " "
          k = InStr(wResp, "'")
        Wend
      End If
      wStr1 = Trim(wResp)
      
      'SQ 16-02-06
'      Set TbOggetti = m_Fun.Open_Recordset("select idOggetto,tipo from bs_oggetti where nome = '" & wStr1 & "' and tipo in ('CBL','ASM','PLI')")
'      If TbOggetti.RecordCount = 0 Then
'        'Nome LOAD diverso da nome SOURCE: aggiorniamo il recordset
'        Set TbOggetti = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.load = '" & wStr1 & "' and b.source=a.nome and tipo in ('CBL','ASM','PLI')")
'      End If
'
'      If TbOggetti.RecordCount Then
'         TbOggetti.MoveLast
'         If TbOggetti.RecordCount = 1 Then
'            AggiungiRelazione TbOggetti!idOggetto, rTipo, wStr1, wStr1
'         End If
'      Else
'         If rTipo <> "LOAD" Then
'             If Not ExistObjSyst(wStr1, "PGM") Then
'                AggiungiSegnalazione wStr1, "NO" & rTipo, wStr1
'             End If
'         End If
'      End If
  
    ''''''''''''''''''''''''''''''''''''''
    ' Relazioni/Segnalazioni
    ''''''''''''''''''''''''''''''''''''''
    ' Mauro 14/01/2009 : Ciclo per tutti valori che pu� assumere la CALL
    'MapsdM_Parser.checkRelations wStr1, rTipo
    Dim itemValues() As String
    Dim i As Integer
    If Not SwNomeCall Then
      ReDim itemValues(0)
      itemValues() = getFieldValues(wStr1)
      If UBound(itemValues) Then
        ' Mauro 07/10/2009
        'SwNomeCall = True
        For i = 1 To UBound(itemValues)
          If Len(itemValues(i)) Then
            ' Mauro 07/10/2009
            If Left(itemValues(i), 1) = "'" Then
              SwNomeCall = True
            Else
              SwNomeCall = False
            End If
            MapsdM_Parser.checkRelations Replace(itemValues(i), "'", ""), rTipo
          End If
        Next i
      Else
        MapsdM_Parser.checkRelations wStr1, rTipo
      End If
    Else
      MapsdM_Parser.checkRelations wStr1, rTipo
    End If
  End If
    
End Sub

Public Sub getCOPY(line As String)
  Dim y As Integer, k As Integer
  Dim wStr1 As String, Wstr2 As String, copyname As String
  Dim rs As Recordset
  
  GbOldNumRec = GbNumRec  'linea iniziale
  
  If Right(line, 1) = "." Then
    line = Trim(Left(line, Len(line) - 1))
  End If
  'Modificato
  copyname = nexttoken(line)
'  If copyname = "IDMS" Then
'    wCOPY_IDMS = True
'    copyname = nexttoken(line)
'    If copyname = "RECORD" Then
'      copyname = nexttoken(line)
'    End If
'  End If
  
  If InStr(copyname, ".") Then
    copyname = Left(copyname, InStr(copyname, ".") - 1)
  End If
  copyname = Replace(copyname, "'", "")
  
  MapsdM_Parser.checkRelations copyname, "COPY"
  
  'Controlla se copy di "sistema" per riconoscimento TIPO oggetto:
  'NO: POSSO AVERE DFHAID o altro su programmi non CICS...
  'ParamCpy Wstr2

  '''''''''''''''''''''''''''''''''''
  ' Gestione REPLACING:
  '''''''''''''''''''''''''''''''''''
  Wstr2 = nexttoken(line)
  If Wstr2 = "SUPPRESS" Then
    'Non gestito
    'Wstr2 = nextToken(line)
  ElseIf Wstr2 = "REPLACING" Then
    wStr1 = nexttoken(line)
    If wStr1 = "==" Then wStr1 = nexttoken(line)
    wStr1 = Replace(wStr1, "==", "")
    Wstr2 = nexttoken(line)
    If Wstr2 = "==" Then Wstr2 = nexttoken(line)
    If Wstr2 = "BY" Then
      Wstr2 = nexttoken(line)
      If Wstr2 = "==" Then Wstr2 = nexttoken(line)
      Wstr2 = Replace(Wstr2, "==", "")
      
      Parser.PsConnection.Execute "INSERT INTO PsPgm_Copy(IdPgm,riga,copy,ReplacingVar,ReplacingValue) VALUES(" & GbIdOggetto & "," & GbOldNumRec & ",'" & copyname & "','" & Replace(wStr1, "'", "") & "','" & Replace(Wstr2, "'", "") & "')"
    Else
      'writelog...
    End If
  End If
  
  '???chiarire...
'  y = InStr(Wstr2, " REPLACING")
'  If y > 0 Then
'    If InStr(Wstr2, ".") > 0 Then
'      Wstr2 = Left(Wstr2, y - 1) & "."
'    Else
'      Wstr2 = Left(Wstr2, y - 1)
'    End If
'  End If
'  'attenzione... POSSO AVERE IL NOME NELLA RIGA SOTTO?
'  Wstr2 = Replace(Wstr2, ".", "")
'  y = InStr(Wstr2, "'")
'  While y > 0
'    Mid$(Wstr2, y, 1) = " "
'    y = InStr(Wstr2, "'")
'  Wend
'  Wstr2 = Trim(Wstr2)
'  If InStr(Wstr2, "*") Then Exit Sub
End Sub

''''''''''''''''''''''''''''''''''''''''
' SQ 10-02-06
' Restituisce l'intero statement COBOL (fino al punto)
' - Esplode le eventuali COPY
''''''''''''''''''''''''''''''''''''''''
Private Function getCobolStatement(line As String, FD As Long) As String
  Dim statement As String, token As String
  Dim endStatement As Boolean
  Dim rs As Recordset
  Dim i As Integer
  'On Error GoTo funcErr  'a livello superiore...
  
  While Not EOF(FD) And Not endStatement
    If Len(line) = 0 Then 'len(statement)
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
        line = Trim(Mid(line, 8, 65))
      Else
        line = ""
      End If
    End If
    If Len(line) Then
      '''''''''''''''''''''''''''''''''''''
      ' COPY?
      '''''''''''''''''''''''''''''''''''''
      token = nexttoken(line)
      If token = "COPY" Then
        getCOPY line  'setta idOggettoRel (0: copy not found)
        If IdOggettoRel Then
          statement = statement & " " & readCopyFile(IdOggettoRel)
          endStatement = True
        End If
      Else
        statement = statement & " " & token & " " & line
        If InStr(line, ".") Then
          endStatement = True
          statement = Replace(statement, ".", "")
        End If
        line = ""
      End If
    End If
  Wend
  'SQ 10-02-06
  'Mi arriva il file gi� chiuso (line=ultima riga file)
  If EOF(FD) And Not endStatement Then
    getCobolStatement = Trim(Replace(line, ".", ""))
  Else
    getCobolStatement = Trim(statement)
  End If
End Function

'tmp public
Public Sub getEXEC_DLI(statement As String)
  Dim rsIstruzioni  As Recordset, rsOggetti As Recordset
  Dim wComando As String, token As String, psb As String, itemValue As String
  Dim itemValues() As String
  Dim i As Integer, comboinstruction As String, isCCodeF As Boolean
  
  ' Mauro 09/01/2008
  ReDim itemValues(0)
  'SQ '''''''''''CreaIstrAmbExec GbIdOggetto, GbNumRec, statement, "DLI"
  
  SwDli = True
  
  'SQ brutto sto blocco!
  '(era anche incompleto... aggiunto XRST)
  wComando = nexttoken(statement)
  ' Mauro 01/07/2009
  ' Non � obbligatoria la USING dopo il comando
  ' Ma il comando � sempre formato da una parola sola, tranne le possibili GET
  ' ES: GET NEXT IN PARENT, GET UNIQUE
''  If Not (wComando = "CHECKPOINT" Or wComando = "CHKP" Or wComando = "SYMCHKP" Or wComando = "XRST") Then
''    token = nextToken(statement)  'USING ?
''    If Not token = "USING" And Not token = "PSB" And Not token = "" Then
''      wComando = wComando & " " & token
''      token = nextToken(statement)  'USING
''      While Not token = "USING" And Not token = ""
''        wComando = wComando & " " & token
''        token = nextToken(statement)  'USING
''      Wend
''    End If
''    If InStr(wComando, "FIRST") > 0 Then
''      addSegnalazione wComando, "I00", wComando
''      wComando = Replace(wComando, "FIRST", "")
''    End If
''  End If
  If wComando = "GET" Then
    token = nexttoken(statement)
    wComando = wComando & " " & token
    token = nexttoken(statement)
    If token = "IN" Then
      wComando = wComando & " IN"
      token = nexttoken(statement)
      wComando = wComando & " " & token
    Else
      statement = token & " " & statement
    End If
  End If
  token = nexttoken(statement)
  If Not token = "USING" And Not token = "PSB" And Not token = "" Then
    statement = Trim(token & " " & statement)
  End If
  
  ' uniformiamo i comandi
  Select Case wComando
    Case "INSERT"
      wComando = "ISRT"
    Case "REPLACE"
      wComando = "REPL"
    Case "DELETE"
      wComando = "DLET"
    Case "SCHEDULE", "SCHD"
      wComando = "SCHED"
    Case "TERMINATE"
      wComando = "TERM"
    Case "CHECKPOINT"
       wComando = "CHKP"
  End Select

  Set rsIstruzioni = m_Fun.Open_Recordset("select * from PsExec where idoggetto = " & GbIdOggetto)
  rsIstruzioni.AddNew
  rsIstruzioni!idOggetto = GbIdOggetto
  rsIstruzioni!area = "DLI"
  rsIstruzioni!Riga = GbOldNumRec
  rsIstruzioni!comando = wComando
  rsIstruzioni!istruzione = statement
  rsIstruzioni!linee = GbNumRec - GbOldNumRec + 1
  '''silvia
  If posDot > 0 Then
    rsIstruzioni!dot = True
  End If
  '''
  rsIstruzioni.Update
  rsIstruzioni.Close
  
  ' Inserimento PsDli_Istruzioni:
  Set rsIstruzioni = m_Fun.Open_Recordset("select idpgm, IdOggetto,Riga,istruzione,statement,isEXEC from PsDli_Istruzioni")
  rsIstruzioni.AddNew
  'SQ 31-10-07 gravissimo: INC
  rsIstruzioni!idpgm = IIf(GbTipoInPars = "CPY" Or GbTipoInPars = "INC", 0, GbIdOggetto)
  rsIstruzioni!idOggetto = GbIdOggetto
  rsIstruzioni!Riga = GbOldNumRec
  rsIstruzioni!statement = statement
  Select Case wComando
    Case "GET UNIQUE"
      comboinstruction = "GU"
    Case "GET NEXT IN PARENT"
      comboinstruction = "GNP"
    Case "GET NEXT"
      comboinstruction = "GN"
    Case Else
      comboinstruction = wComando
  End Select
  rsIstruzioni!istruzione = comboinstruction
  rsIstruzioni!isExec = True
  rsIstruzioni.Update
  rsIstruzioni.Close
  
  'TESTARE QUI SOTTO...
  If wComando = "SCHEDULE" Or wComando = "SCHD" Then
    '...
    ReDim itemValues(0)
    
    psb = statement
    psb = Replace(psb, ".", "")
    psb = Replace(psb, ",", "")
    psb = Replace(psb, "'", "")
    psb = Replace(psb, "(", "")
    psb = Replace(psb, ")", "")
    ' Mauro 10/10/2007 : se ci sono le doppie parentesi, � una variabile
    ' AC 15/01/08 Inserito controllo su tutto array itemValues
    ' nel caso di variabili con pipe segnalazione diventa NOPSBVALUE invece di NOPSB
    If Left(statement, 2) = "((" Then
      itemValues() = getFieldValues(psb)
      If Not UBound(itemValues) > 0 Then
        addSegnalazione psb, "NOPSBVALUE", psb
        Exit Sub
      End If
    Else
      '...
      ReDim itemValues(1)
      itemValues(1) = psb
    End If
    
    For i = 1 To UBound(itemValues)
      itemValue = Trim(Replace(itemValues(i), "'", ""))
      If Len(itemValue) Then
        If Not InStr(itemValue, "||") > 0 Then 'And Not IsPositionalMoveValue(itemValue, psb, GbIdOggetto)
          Set rsOggetti = m_Fun.Open_Recordset("select idOggetto from Bs_oggetti where " & _
                                               "tipo = 'PSB' and nome = '" & itemValue & "'")
          If rsOggetti.RecordCount = 0 Then
            If UBound(itemValues) Then
              addSegnalazione itemValues(1), "NOPSB", itemValue
            Else
              addSegnalazione psb, "NOPSB", psb
            End If
          Else
            AggiungiRelazione rsOggetti!idOggetto, "PSB", itemValue, itemValue
          End If
          rsOggetti.Close
        Else ' la variabile � costruito utilizzando la concatenazione di variabili (||)
          addSegnalazione psb, "NOPSBVALUE", itemValue
        End If
      End If
    Next
  End If
  Exit Sub
End Sub

Function IsPositionalMoveValue(value As String, psb As String, pIdPgm As Long) As Boolean
  Dim rs As Recordset
  Set rs = m_Fun.Open_Recordset("select deststart from PsData_CmpMoved where " _
                                & " nome = '" & psb & "' and valore = '" & value & "'")
  If Not rs.EOF Then
    If Not IsNull(rs!deststart) Then
      If Not rs!deststart = "" Then
        IsPositionalMoveValue = True
      End If
    End If
 End If
 rs.Close
End Function


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Analizza le CALL.
' - Allestisce la PsCall (NUOVA!)
' - Invoca checkRelation
' Effetto collaterale: puo' valorizzare "line" per il giro successivo
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getCALL(line As String, FD As Long)
  Dim statement As String, program As String, token As String
  Dim StatementOrig As String
  Dim endStatement As Boolean
  Dim linee As Integer
  Dim itemValues() As String
  Dim itemValue As String
  Dim i As Long
  'AC 14/07/09
  Dim NumCommentedLines As Integer
  On Error GoTo callErr
  
  GbOldNumRec = GbNumRec  'linea iniziale
  ''''''''''''''''''''''''''''''''''
  ' Becco tutte le linee di call...
  ''''''''''''''''''''''''''''''''''
  statement = line
  If Mid(line, IIf(Len(line) = 0, 1, Len(line)), 1) <> "." Then
    NumCommentedLines = 0
    While Not EOF(FD) And Not endStatement
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
        line = Trim(Mid(line, 8, 65))
        ' posso avere una linea vuota? (prob. no...)
        If Len(line) Then
          'Altro statement?
          If isCobolLabel(line) Then 'LABEL!
            endStatement = True
            linee = GbNumRec - GbOldNumRec - NumCommentedLines
          Else
            token = nexttoken(line)
            'Parola Cobol?
            'SQ 27-04-06: se mi arriva END-IF./END-CALL. con il punto sbaglia!
            'IRIS-DB: ADDRESS anche se � una parola riservata non chiude la CALL - Da fare in tutti gli altri moduli simili
            'IRIS-DB: anche OF non chiude la call
            'If token <> "USING" And isReservedWord(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Then
            If token <> "USING" And token <> "ADDRESS" And token <> "OF" And isReservedWord(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Then
              endStatement = True
              'linea buona per il giro successivo!
              line = Space(8) & token & " " & line
              'SQ 4-05-06
              'Mi serve linee perch� nei casi di "isReservedWord", gbnumrec uno in pi�
              linee = GbNumRec - GbOldNumRec - NumCommentedLines
            Else
              'linea buona:
              statement = statement & " " & token & " " & line
              'SQ - Fine statement con Punto finale o ELSE!
              If Len(line) Then
                If Mid(line, Len(line), 1) = "." Then
                  endStatement = True
                  linee = GbNumRec - GbOldNumRec + 1 - NumCommentedLines
                  line = "" 'e' il flag... per la nuova lettura
                ElseIf Right(" " & line, 5) = " ELSE" Then
                  endStatement = True
                  linee = GbNumRec - GbOldNumRec - NumCommentedLines
                  line = "" 'e' il flag... per la nuova lettura
                  'Mangio l'ELSE finale
                  statement = Trim(Left(statement, Len(statement) - 4))
                Else
                  NumCommentedLines = 0
                  endStatement = False
                End If
              Else
                endStatement = Mid(token, Len(token), 1) = "."
                linee = GbNumRec - GbOldNumRec + 1 - NumCommentedLines
              End If
            End If
          End If
        End If
      Else
        NumCommentedLines = NumCommentedLines + 1
      End If
    Wend
  Else
    'SQ 14-02-06: se CALL su unica riga, rimane "line" sporca...
    'stefano: vediamo cos�
    line = ""
    linee = 1
  End If
  
  ''''''''''''''''''''''''''''''
  ' Modulo Chiamato:
  ''''''''''''''''''''''''''''''
  'IRIS-DB: toglie gli spazi tra i parametri e le parentesi
  statement = Replace(statement, " (", "(")
  StatementOrig = statement
  SwNomeCall = Left(statement, 1) = "'"
  program = Replace(nexttoken(statement), ".", "")
  'SQ - gestire anche i DOPPI apici!
  'Pezza temporanea:
  If Left(program, 1) = """" Then
    SwNomeCall = True
    program = Replace(program, """", "")
  End If
  If SwNomeCall Then program = "'" & program & "'"   'nextToken mangia gli "'"!
  token = nexttoken(statement) 'serve solo a mangiare l'eventuale "USING"
  statement = Replace(statement, ",", "") 'se argomenti separati da virgole...
  
  '''''''''''''''''''''''''''''''''''''''''''''''''
  ' Inserimento istruzione in PsCall: (trasformare in execute INSERT)
  '''''''''''''''''''''''''''''''''''''''''''''''''
  Dim rsCall As Recordset
  Set rsCall = m_Fun.Open_Recordset("select * from PsCall")
  rsCall.AddNew
  rsCall!idOggetto = GbIdOggetto
  rsCall!Riga = GbOldNumRec
  rsCall!programma = program  'attenzione: puo' contenere apici...
  rsCall!parametri = statement
'IRIS-DB  rsCall!linee = linee
  rsCall!linee = linee + NumCommentedLines 'IRIS-DB : anche se prima lo sottrae, adesso lo risommo, secondo me serve perch� altrimenti poi considera l'istruzione pi� corta della realt�
  rsCall.Update
  rsCall.Close
  
  ''''''''''''''''''''''''''''''''''''''
  ' Relazioni/Segnalazioni
  ''''''''''''''''''''''''''''''''''''''
  ' Mauro 04/12/2007 : Ciclo per tutti valori che pu� assumere la CALL
  'MapsdM_Parser.checkRelations program, "CALL"
  If Not SwNomeCall Then
    ReDim itemValues(0)
    itemValues() = getFieldValues(program)
    If UBound(itemValues) Or Len(itemValues(0)) Then
      ' Mauro 07/10/2009
      'SwNomeCall = True
      For i = 0 To UBound(itemValues)
        If Len(itemValues(i)) Then
          ' Mauro 07/10/2009
          If Left(itemValues(i), 1) = "'" Then
            SwNomeCall = True
          Else
            SwNomeCall = False
          End If
          MapsdM_Parser.checkRelations itemValues(i), "CALL"
        End If
      Next i
    Else
      MapsdM_Parser.checkRelations program, "CALL"
    End If
  Else
    MapsdM_Parser.checkRelations program, "CALL"
  End If
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Relazione programma/PSB
  ' (riconosce le istruzioni e le inserisce nella PsDli_Istruzioni
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If isDliCall Then 'settato dalla checkRelation
    If program = "CSSTDLIC" Then
      token = nexttoken(statement)
      If token = "DFHEIBLK" Then
        token = nexttoken(statement)
      Else
        addSegnalazione "CALL " & StatementOrig, "F08", "CALL " & StatementOrig
      End If
    End If
    MapsdM_IMS.checkPSB statement
  End If
  
  Exit Sub
callErr:
  'gestire...
  If err.Number = -2147217887 Then  'chiave duplicata...
    '2 CALL su una linea...
  Else
    err.Raise 124, "getCall", "syntax error on " & statement
    'Resume
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Analizza i parametri della PROCEDURE DIVISION.
' - Allestisce la PsPgm (NUOVA!)
' Effetto collaterale: puo' valorizzare "line" per il giro successivo
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getUsingParameters(line As String, FD As Long)
  Dim statement As String, parameters As String, token As String
  Dim endStatement As Boolean

  On Error GoTo callErr

  GbOldNumRec = GbNumRec  'linea iniziale
  ''''''''''''''''''''''''''''''''''
  ' Becco tutte le linee di call...
  ''''''''''''''''''''''''''''''''''
  statement = line
  If Right(line, 1) <> "." Then
    While Not EOF(FD) And Not endStatement
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
        line = Trim(Mid(line, 8, 65))
        ' posso avere una linea vuota? (prob. no...)
        While Len(line) And Not endStatement
          token = nexttoken(line)
          If Right(token, 1) = "." Then 'If token <> "USING" And isReservedWord(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Then
            token = Left(token, Len(token) - 1)
            endStatement = True
          End If
          'linea buona:
          statement = statement & " " & token '& " " & line
        Wend
        'End If
      End If
    Wend
  Else
    'SQ 14-02-06: se CALL su unica riga, rimane "line" sporca...
    'stefano: vediamo cos�
    statement = Left(statement, Len(statement) - 1)
    line = ""
  End If
  
  'USING
  token = nexttoken(statement)
  If token = "USING" Then
    statement = Trim(Replace(statement, ",", ""))
    While Len(statement)
      parameters = parameters & IIf(parameters = "", "", ",") & nexttoken(statement)
    Wend
 
    '''''''''''''''''''''''''''''''''''''''''''''''''
    ' Inserimento istruzione in PsPgm:
    '''''''''''''''''''''''''''''''''''''''''''''''''
    Dim rs As Recordset
    If Len(parameters) Then
      Set rs = m_Fun.Open_Recordset("select * from PsPgm where idoggetto = " & GbIdOggetto) 'virgilio
      If rs.RecordCount = 0 Then 'virgilio
        rs.AddNew
        rs!idOggetto = GbIdOggetto
        ' Mauro 29/01/2008: Anche la riga � in chiave
        rs!Riga = GbOldNumRec
        rs!parameters = parameters
        rs!comando = "USING"
        rs.Update
      End If ' virgilio
      rs.Close
    End If
  End If
  Exit Sub
callErr:
  'gestire...
  MsgBox "tmp: " & err.description
  'Resume
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 13-09-07
' !!!
' SQ: il giro sulla PsCom_Obj/PsRel_File � stato
'     buttato (vedi vecchia sub): � giusto?
' !!!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parseOPEN(line As String, FD As Long)
  Dim k As Integer
  Dim statement As String, appo As String, token As String, wComando As String
  Dim endStatement As Boolean, continue As Boolean
  Dim linee As Long
  
  On Error GoTo catch
  
  continue = True
  statement = line
  
  GbOldNumRec = GbNumRec  'linea iniziale
  
  wComando = nextToken_new(statement)
  
  If Mid(line, Len(line), 1) = "." Then
    endStatement = True
  End If
  
  While Not EOF(FD) And Not endStatement
    Line Input #FD, line
    GbNumRec = GbNumRec + 1
    If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
      line = Trim(Mid(line, 8, 65))
      appo = line
      If Len(line) And Not isInstruction(nexttoken(Replace(appo, "-", " "))) Then
        statement = statement & "@" & line
        appo = line
        If Not isInstruction(nexttoken(appo)) Then
          'SQ SE LA LINEA E' VUOTA NON FUNZIA!
          'While Mid(line, Len(line), 1) <> "." and continue
          While Right(line, 1) <> "." And continue
            Line Input #FD, line
            GbNumRec = GbNumRec + 1
            line = Trim(Mid(line, 8, 65))
            appo = line
            If Not isInstruction(nexttoken(appo)) Then
              statement = statement & "@" & line
            Else
              continue = False
            End If
          Wend
        End If
        endStatement = True
        linee = GbNumRec - GbOldNumRec
      Else
        endStatement = True
      End If
    End If
  Wend
  
  Dim rsOpen As Recordset
  Set rsOpen = m_Fun.Open_Recordset("select * from PsIO_Batch")
  rsOpen.AddNew
  rsOpen!idOggetto = GbIdOggetto
  rsOpen!area = "BATCH"
  rsOpen!Riga = GbOldNumRec
  rsOpen!comando = wComando
  rsOpen!istruzione = statement
  rsOpen!linee = GbNumRec - GbOldNumRec + 1
  rsOpen.Update
  rsOpen.Close
  Exit Sub
catch:
  addSegnalazione line, "P00", line
End Sub
'''' MF VECCHIO parseOPEN
''''Private Sub parseOPEN(wRecIn As String, NumFile As Long)
''''  Dim wbuffer As String
''''  Dim k As Integer
''''  Dim wLink As String
''''  Dim wTipoOpen As String
''''  Dim wStr1 As String
''''  Dim tb As Recordset
''''  Dim wSql As String
''''
''''  wbuffer = wRecIn
''''
''''  If InStr(UCase(wbuffer), " OUTPUT ") > 0 Then wTipoOpen = "OUTPUT"
''''  If InStr(UCase(wbuffer), " I-O ") > 0 Then wTipoOpen = "I-O"
''''  If InStr(UCase(wbuffer), " INPUT-OUTPUT ") > 0 Then wTipoOpen = "INPUT-OUTPUT"
''''  If InStr(UCase(wbuffer), " INPUT ") > 0 Then wTipoOpen = "INPUT"
''''  If InStr(UCase(wbuffer), " EXTEND ") > 0 Then wTipoOpen = "EXTEND"
''''
''''  k = InStr(wbuffer, wTipoOpen)
''''  wbuffer = Mid$(wbuffer, k + Len(wTipoOpen))
''''  wbuffer = Trim(wbuffer)
''''
''''  While Len(wbuffer) > 0
''''   k = InStr(wbuffer, " ")
''''   If k = 0 Then k = Len(wbuffer) + 1
''''   wStr1 = Mid$(wbuffer, 1, k - 1)
''''   wStr1 = Replace(wStr1, ".", "")
''''   wbuffer = Mid$(wbuffer, k + 1) 'non capisco ALE
''''   wbuffer = Trim(wbuffer) 'non capisco ALE
''''   Set tb = m_Fun.Open_Recordset("select * from psCom_Obj where idoggettoc = " & GbIdOggetto & " and relazione = 'AGN' and nomeoggettor = '" & wStr1 & "' ")
''''   If tb.RecordCount > 0 Then
''''     wStr1 = tb!NomeComponente
''''     wSql = " Update PsRel_File set OpenCbl = '" & wTipoOpen & "' where " _
''''          & "program = '" & GbNomePgm & "' and " _
''''          & "link = '" & wStr1 & "' "
''''     Parser.PsConnection.Execute wSql
''''   End If
''''   tb.Close
''''   Set tb = Nothing
''''  Wend
''''End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 13-09-07
''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parseCLOSE(ByRef line As String, FD As Long)
  Dim k As Integer
  Dim statement As String, appo As String, token As String, wComando As String
  Dim endStatement As Boolean, continue As Boolean
  Dim linee As Long
  
  On Error GoTo catch
  
  continue = True
  statement = line
  
  GbOldNumRec = GbNumRec  'linea iniziale
  
  wComando = nextToken_new(statement)
  
  If Mid(line, Len(line), 1) = "." Then
    endStatement = True
  End If
  
  While Not EOF(FD) And Not endStatement
    Line Input #FD, line
    GbNumRec = GbNumRec + 1
    If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
      'line = Trim(Mid(line, 8, 65))
      appo = Trim(Mid(line, 8, 65))
      If Len(appo) Then
        token = nexttoken(appo)
        If Not isInstruction(token) Then
          statement = statement & "@" & token
          'SQ SE LA LINEA E' VUOTA NON FUNZIA!
          'While Mid(line, Len(line), 1) <> "." And continue
          While Right(statement, 1) <> "." And continue
            Line Input #FD, line
            GbNumRec = GbNumRec + 1
            'line = Trim(Mid(line, 8, 65))
            appo = Trim(Mid(line, 8, 65))
            token = nexttoken(appo)
            If Not isInstruction(token) Then
              statement = statement & "@" & token
            Else
              continue = False
            End If
          Wend
        End If
        endStatement = True
        linee = GbNumRec - GbOldNumRec
      End If
    End If
  Wend
  
  Dim rsOpen As Recordset
  Set rsOpen = m_Fun.Open_Recordset("select * from PsIO_Batch")
  rsOpen.AddNew
  rsOpen!idOggetto = GbIdOggetto
  rsOpen!area = "BATCH"
  rsOpen!Riga = GbOldNumRec
  rsOpen!comando = wComando
  rsOpen!istruzione = statement
  rsOpen!linee = GbNumRec - GbOldNumRec + 1
  rsOpen.Update
  rsOpen.Close
  
  Exit Sub
catch:
  addSegnalazione line, "P00", line
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' RIFARE:
' -Parserare bene: "nuovo stile"
' - Serve anche "FD"! (gestione multilinea)
' - Associazione AREA/File: VARIABILE ENV
' - Scrittura in PsVSM... solo se flag alzato
' - ...
' Fare parseWRITE allo stesso modo
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 28/08/07
'Parsing READ nei comandi BATCH
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parseREAD(ByRef line As String, FD As Long)
  Dim wbuffer As String
  Dim k As Integer, i As Long
  Dim wLink As String
  Dim wDsnName As String
  Dim wArea As String
  Dim VSAMName As String
  Dim word As Long
  Dim wIdObj As Long, wIdArea As Long
  Dim Tbf As Recordset, TbFile As Recordset
  Dim statement As String, token As String, wComando As String
  Dim endStatement As Boolean
  Dim linee As Long
  Dim rsCmp As Recordset, rs As Recordset
  
  On Error GoTo catch
  
  wbuffer = line
  statement = line
  
  GbOldNumRec = GbNumRec  'linea iniziale
  
  wComando = nextToken_new(statement)
  
  If Mid(line, Len(line), 1) <> "." Then
    If InStr(line, " END") Or InStr(line, " INVALID KEY") Then
      While Mid(line, IIf(Len(line) = 0, 1, Len(line)), 1) <> "." And Not endStatement
        ' Mauro 02/07/2009
        ' Ho copiato il blocco sotto perch� se ho un'istruzione qua dentro il parser la salta a piedi pari
''        Line Input #FD, line
''        GbNumRec = GbNumRec + 1
''        line = Trim(Mid(line, 8, 65))
''        statement = statement + "@" + line
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
          line = Trim(Mid(line, 8, 65))
          If Len(line) Then
            If line <> "END-READ" Then
              If InStr(line, " END") Or InStr(line, "INVALID KEY") Then
                statement = statement + "@" + line
                'SQ SE LA LINEA E' VUOTA NON FUNZIA!
                'While Mid(line, Len(line), 1) <> "."
                'IRIS-DB: per ora lo skippa altrimenti NON analizza tutto ci� che � nella AT END e simili, skippando CALL e altri statement importanti
'''''                While Right(Line, 1) <> "." And Line <> "END-READ"
'''''                  Line Input #FD, Line
'''''                  GbNumRec = GbNumRec + 1
'''''                  Line = Trim(Mid(Line, 8, 65))
'''''                  statement = statement + "@" + Line
'''''                Wend
                endStatement = True
                linee = GbNumRec - (GbOldNumRec + 1)
              Else
                token = nexttoken(line)
                'If isReservedWord(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Then
'                If (token & " " & Left(line, 3)) = "AT END" Then
'                  While Mid(line, Len(line), 1) <> "."
'                    statement = statement & "@" & token & "@" & line
'                    Line Input #FD, line
'                    GbNumRec = GbNumRec + 1
'                    line = Trim(Mid(line, 8, 65))
'                  Wend
'                  endStatement = True
'                  linee = GbNumRec - GbOldNumRec
'                  statement = statement + "@" + line
                If isInstruction(token) Then
                  endStatement = True
                  ' Mauro 01/08/2008
                  'linea buona per il giro successivo!
                  line = Space(8) & token & " " & line
                  'Mi serve linee perch� nei casi di "isReservedWord", gbnumrec uno in pi�
                  linee = GbNumRec - (GbOldNumRec + 1)
                Else
                  statement = statement & "@" & token & " " & line
                End If
              End If
            Else
              statement = statement + " " & line
              endStatement = True
            End If
          Else
            'SQ 24-10-07 VERIFICARE BENE!
            'questa non pu� funzionare... qui arrivo con linea vuota;
            'il token non l'ho letto...
            'endStatement = Mid(token, Len(token), 1) = "."
            'linee = GbNumRec - GbOldNumRec + 1
          End If
        End If
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Wend
      linee = GbNumRec - (GbOldNumRec + 1)
    Else
      While Not EOF(FD) And Not endStatement
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
          line = Trim(Mid(line, 8, 65))
          If Len(line) Then
            If line <> "END-READ" Then
              If InStr(line, " END") Or InStr(line, "INVALID KEY") Then
                statement = statement + "@" + line
                'SQ SE LA LINEA E' VUOTA NON FUNZIA!
                'While Mid(line, Len(line), 1) <> "."
                'IRIS-DB: per ora lo skippa altrimenti NON analizza tutto ci� che � nella AT END e simili, skippando CALL e altri statement importanti
''''                While Right(Line, 1) <> "." And Line <> "END-READ"
''''                  Line Input #FD, Line
''''                  GbNumRec = GbNumRec + 1
''''                  Line = Trim(Mid(Line, 8, 65))
''''                  statement = statement + "@" + Line
''''                Wend
                endStatement = True
                linee = GbNumRec - (GbOldNumRec + 1)
              Else
                token = nexttoken(line)
                'If isReservedWord(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Then
'                If (token & " " & Left(line, 3)) = "AT END" Then
'                  While Mid(line, Len(line), 1) <> "."
'                    statement = statement & "@" & token & "@" & line
'                    Line Input #FD, line
'                    GbNumRec = GbNumRec + 1
'                    line = Trim(Mid(line, 8, 65))
'                  Wend
'                  endStatement = True
'                  linee = GbNumRec - GbOldNumRec
'                  statement = statement + "@" + line
                If isInstruction(token) Then
                  endStatement = True
                  ' Mauro 01/08/2008
                  'linea buona per il giro successivo!
                  line = Space(8) & token & " " & line
                  'Mi serve linee perch� nei casi di "isReservedWord", gbnumrec uno in pi�
                  linee = GbNumRec - (GbOldNumRec + 1)
                Else
                  statement = statement & "@" & token & " " & line
                End If
              End If
            Else
              statement = statement + " " & line
              endStatement = True
            End If
          Else
            'SQ 24-10-07 VERIFICARE BENE!
            'questa non pu� funzionare... qui arrivo con linea vuota;
            'il token non l'ho letto...
            'endStatement = Mid(token, Len(token), 1) = "."
            'linee = GbNumRec - GbOldNumRec + 1
          End If
        End If
      Wend
    End If
  Else
    line = ""
  End If
    
  'MG 030907 Inserito controllo
  'Insert solo se si tratta di file VSAM
  'INIT
'  Dim rsObj As Recordset
'  Set rsObj = m_Fun.Open_Recordset("Select VSAM from Bs_Oggetti where IdOggetto=" & GbIdOggetto)
'  If rsObj!VSam Then
  Dim rsRead As Recordset
  Set rsRead = m_Fun.Open_Recordset("select * from PsIO_Batch")
  rsRead.AddNew
  rsRead!idOggetto = GbIdOggetto
  rsRead!area = "BATCH"
  rsRead!Riga = GbOldNumRec
  rsRead!comando = wComando
  rsRead!istruzione = statement
  rsRead!linee = GbNumRec - GbOldNumRec + 1
  rsRead.Update
  rsRead.Close
'  End If
'  rsObj.Close
  
  'wbuffer = Trim(Mid$(wbuffer, 5))
  k = InStr(wbuffer, " INTO ")
  If k = 0 Then
    line = Space(8) & line
    Exit Sub
  End If
  wArea = Trim(Mid$(wbuffer, k + 6))
  k = InStr(wArea, " ")
  If k > 0 Then wArea = Mid$(wArea, 1, k - 1)
  If Right(wArea, 1) = "." Then
    wArea = Left(wArea, Len(wArea) - 1)
  End If
  k = InStr(wbuffer, "READ ")
  If k > 0 Then wLink = Trim(Mid$(wbuffer, k + 5))
  k = InStr(wLink, " ")
  If k > 0 Then wLink = Trim(Mid$(wLink, 1, k - 1))

  If manageIO Then
    Dim idObjArea As String
    idObjArea = CercaIdArea(wArea)
    k = InStr(idObjArea, "/")
    If k > 0 Then
      wIdObj = Val(Mid(idObjArea, 1, k - 1))
      wIdArea = Val(Mid(idObjArea, k + 1))
    End If
    If wIdObj = 0 Then Exit Sub
  
    'MF 30/08/08
    'PROBLEMA: non trova mai nessun oggetto nella tabella psrel_file
    Set TbFile = m_Fun.Open_Recordset("select * from psrel_file where " & _
                                   "program = '" & GbNomePgm & "' and link = '" & wLink & "'")
    Do Until TbFile.EOF
      wDsnName = TbFile!dsnName
      
      Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Archivio where nome = '" & wDsnName & "' ")
      If Tbf.RecordCount = 0 Then Exit Sub
      ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
      'wIdArc = Tbf!idArchivio
      VSAMName = Tbf!VSam & ""
    
      'Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Tracciato where idarchivio = " & wIdArc & " and idoggetto = " & wIdObj & " and idarea = " & wIdArea & " ")
      Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Tracciato where " & _
                                     "VSAM = '" & VSAMName & "' and idoggetto = " & wIdObj & " and idarea = " & wIdArea & " ")
      If Tbf.RecordCount = 0 Then
        'Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Tracciato where idarchivio = " & wIdArc & " order by ordinale")
        Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Tracciato where VSAM = '" & VSAMName & "' order by ordinale")
        If Tbf.RecordCount = 0 Then
          word = 1
        Else
          Tbf.MoveLast
          word = Tbf!Ordinale + 1
        End If
        Tbf.AddNew
        'Tbf!idArchivio = wIdArc
        Tbf!VSam = VSAMName
        Tbf!idOggetto = wIdObj
        Tbf!Ordinale = word
        Tbf!idArea = wIdArea
        Tbf!Tag = "R"
        Tbf.Update
      End If
      TbFile.MoveNext
    Loop
    TbFile.Close
  End If
  
  If Len(line) Then line = Space(8) & line
  Exit Sub
catch:
  addSegnalazione line, "P00", line
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 28/08/07
'Parsing WRITE nei comandi BATCH
'''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parseWRITE(line As String, FD As Long)
  Dim wbuffer As String
  Dim k As Integer, i As Long
  Dim wLink As String
  Dim wDsnName As String
  Dim wArea As String
  Dim VSAMName As String
  Dim word As Long
  Dim wIdObj As Long, wIdArea As Long
  Dim Tbf As Recordset, rsTracciato As Recordset, TbFile As Recordset
  Dim statement As String, token As String, wComando As String
  Dim endStatement As Boolean
  Dim linee As Long
  Dim rsWrite As Recordset, rs As Recordset, rsCmp As Recordset
  Dim idObjArea As String
  
  On Error GoTo catch
  
  wbuffer = line
  statement = line
  
  GbOldNumRec = GbNumRec  'linea iniziale
  
  wComando = nextToken_new(statement)
  
  If Mid(line, Len(line), 1) <> "." Then
    If InStr(line, " INVALID KEY") Then
      While Mid(line, Len(line), 1) <> "." And Not endStatement
        ' Mauro 02/07/2009
        ' Ho copiato il blocco sotto perch� se ho un'istruzione qua dentro il parser la salta a piedi pari
        'Line Input #FD, line
        'GbNumRec = GbNumRec + 1
        'line = Trim(Mid(line, 8, 65))
        'statement = statement + "@" + line
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
          line = Trim(Mid(line, 8, 65))
          If Len(line) Then
            ' Mauro 11/12/2007 : Non si pu� controllare anche se c'� il punto alla fine?
            'If line <> "END-WRITE" Then
            If line <> "END-WRITE" And Right(line, 1) <> "." Then
              If InStr(line, "INVALID KEY") Then
                statement = statement + "@" + line
                'SQ SE LA LINEA E' VUOTA NON FUNZIA!
                'While Mid(line, Len(line), 1) <> "."
                While Right(line, 1) <> "."
                  Line Input #FD, line
                  GbNumRec = GbNumRec + 1
                  line = Trim(Mid(line, 8, 65))
                  statement = statement + "@" + line
                Wend
                endStatement = True
                linee = GbNumRec - GbOldNumRec
              Else
                token = nexttoken(line)
                If isInstruction(token) Then
                  endStatement = True
                  ' Mauro 01/08/2008
                  'linea buona per il giro successivo!
                  line = Space(8) & token & " " & line
                  'Mi serve linee perch� nei casi di "isReservedWord", gbnumrec uno in pi�
                  linee = GbNumRec - GbOldNumRec
                Else
                  statement = statement & "@" & token & " " & line
                End If
              End If
            Else
              statement = statement + " " & line
              line = Space(8) & line
              endStatement = True
            End If
          Else
            'SQ 24-10-07 VERIFICARE BENE!
            'questa non pu� funzionare... qui arrivo con linea vuota;
            'il token non l'ho letto...
            'endStatement = Mid(token, Len(token), 1) = "."
            'linee = GbNumRec - GbOldNumRec + 1
          End If
        End If
      Wend
      linee = GbNumRec - GbOldNumRec
    Else
      While Not EOF(FD) And Not endStatement
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
          line = Trim(Mid(line, 8, 65))
          If Len(line) Then
            ' Mauro 11/12/2007 : Non si pu� controllare anche se c'� il punto alla fine?
            'If line <> "END-WRITE" Then
            If line <> "END-WRITE" And Right(line, 1) <> "." Then
              If InStr(line, "INVALID KEY") Then
                statement = statement + "@" + line
                'SQ SE LA LINEA E' VUOTA NON FUNZIA!
                'While Mid(line, Len(line), 1) <> "."
                While Right(line, 1) <> "."
                  Line Input #FD, line
                  GbNumRec = GbNumRec + 1
                  line = Trim(Mid(line, 8, 65))
                  statement = statement + "@" + line
                Wend
                endStatement = True
                linee = GbNumRec - GbOldNumRec
              Else
                token = nexttoken(line)
                If isInstruction(token) Then
                  endStatement = True
                  ' Mauro 01/08/2008
                  'linea buona per il giro successivo!
                  line = Space(8) & token & " " & line
                  'Mi serve linee perch� nei casi di "isReservedWord", gbnumrec uno in pi�
                  linee = GbNumRec - GbOldNumRec
                Else
                  statement = statement & "@" & token & " " & line
                End If
              End If
            Else
              statement = statement + " " & line
              line = Space(8) & line
              endStatement = True
            End If
          Else
            'SQ 24-10-07 VERIFICARE BENE!
            'questa non pu� funzionare... qui arrivo con linea vuota;
            'il token non l'ho letto...
            'endStatement = Mid(token, Len(token), 1) = "."
            'linee = GbNumRec - GbOldNumRec + 1
          End If
        End If
      Wend
    End If
  Else
    line = ""
  End If

'''''  If Mid(line, Len(line), 1) <> "." Then
'''''    While Not EOF(FD) And Not endStatement
'''''      Line Input #FD, line
'''''      GbNumRec = GbNumRec + 1
'''''      If Mid(line, 7, 1) <> "*" Then
'''''        line = Trim(Mid(line, 8, 65))
'''''        If Len(line) Then
'''''          token = nextToken(line)
'''''          'Parola Cobol?
'''''          'SQ 27-04-06: se mi arriva END-IF./END-CALL. con il punto sbaglia!
'''''          If token <> "FROM" Or isReservedWord(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Then
'''''            endStatement = True
'''''            linee = GbNumRec - GbOldNumRec
'''''          Else
'''''            'linea buona:
'''''            statement = statement & " " & token & " " & line
'''''          If Len(line) Then
'''''              If Mid(line, Len(line), 1) = "." Then
'''''                endStatement = True
'''''                linee = GbNumRec - GbOldNumRec + 1
'''''                line = "" 'e' il flag... per la nuova lettura
'''''              Else
'''''                endStatement = False
'''''              End If
'''''            Else
'''''              endStatement = Mid(token, Len(token), 1) = "."
'''''              linee = GbNumRec - GbOldNumRec + 1
'''''            End If
'''''          End If
'''''        End If
'''''      End If
'''''    Wend
'''''  End If
    
  Set rsWrite = m_Fun.Open_Recordset("select * from PsIO_Batch")
  rsWrite.AddNew
  rsWrite!idOggetto = GbIdOggetto
  rsWrite!area = "BATCH"
  rsWrite!Riga = GbOldNumRec
  rsWrite!comando = wComando
  rsWrite!istruzione = statement
  rsWrite!linee = GbNumRec - GbOldNumRec + 1
  rsWrite.Update
  rsWrite.Close
  
  'wbuffer = Trim(Mid$(wbuffer, 6))
  k = InStr(wbuffer, " FROM ")
  If k = 0 Then
    'line = Space(8) & line
    Exit Sub
  End If
  wArea = Trim(Mid$(wbuffer, k + 6))
  k = InStr(wArea, " ")
  If k > 0 Then wArea = Mid$(wArea, 1, k - 1)
  If Right(wArea, 1) = "." Then
    wArea = Left(wArea, Len(wArea) - 1)
  End If
  k = InStr(wbuffer, "WRITE ")
  If k > 0 Then wLink = Trim(Mid$(wbuffer, k + 5))
  k = InStr(wLink, " ")
  If k > 0 Then wLink = Trim(Mid$(wLink, 1, k - 1))

  If manageIO Then
    idObjArea = CercaIdArea(wLink)
    'idObjArea = CercaIdArea(wArea)
    k = InStr(idObjArea, "/")
    If k > 0 Then
      wIdObj = Val(Mid(idObjArea, 1, k - 1))
      wIdArea = Val(Mid(idObjArea, k + 1))
    End If
    If wIdObj = 0 Then Exit Sub
    
    Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Tracciato where " & _
                                   "idoggetto = " & wIdObj & " and idarea = " & wIdArea)
    If Tbf.RecordCount Then
      ' L'area � gi� assegnata a un file sequenziale
      idObjArea = CercaIdArea(wArea)
      k = InStr(idObjArea, "/")
      If k > 0 Then
        wIdObj = Val(Mid(idObjArea, 1, k - 1))
        wIdArea = Val(Mid(idObjArea, k + 1))
      End If
      
      Do Until Tbf.EOF
        VSAMName = Tbf!VSam
        word = Tbf!Ordinale + 1
        
        Set rs = m_Fun.Open_Recordset("select * from DMVSM_Tracciato where " & _
                                      "VSAM = '" & VSAMName & "' and ordinale = " & word & " and " & _
                                      "idoggetto = " & wIdObj)
        If rs.RecordCount = 0 Then
          rs.AddNew
          rs!VSam = VSAMName
          rs!idOggetto = wIdObj
          rs!Ordinale = word
          rs!idArea = wIdArea
          rs!Tag = "W"
          rs.Update
        End If
        rs.Close
        Tbf.MoveNext
      Loop
    Else
      ' L'area non � associata a nessun file sequenziale e si trova "temporaneamente" nella PsRel_Tracciati
      Set rsTracciato = m_Fun.Open_Recordset("select ddname from psrel_tracciati where " & _
                                             "idoggetto = " & wIdObj & " and idarea = " & wIdArea)
      If rsTracciato.RecordCount = 0 Then Exit Sub
      wLink = rsTracciato!ddname
      rsTracciato.Close
      
      Set TbFile = m_Fun.Open_Recordset("select * from PsRel_File where " & _
                                        "program = '" & GbNomePgm & "' and link = '" & wLink & "'")
      Do Until TbFile.EOF
        wDsnName = TbFile!dsnName
        Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Archivio where nome = '" & wDsnName & "' ")
        If Tbf.RecordCount = 0 Then Exit Sub
        ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
        'wIdArc = Tbf!idArchivio
        VSAMName = Tbf!VSam & ""
      
        'Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Tracciato where idarchivio = " & wIdArc & " and idoggetto = " & wIdObj & " and idarea = " & wIdArea & " ")
        Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Tracciato where " & _
                                       "VSAM = '" & VSAMName & "' and idoggetto = " & wIdObj & " and idarea = " & wIdArea)
        If Tbf.RecordCount = 0 Then
          'Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Tracciato where idarchivio = " & wIdArc & " order by ordinale")
          Set Tbf = m_Fun.Open_Recordset("select * from DMVSM_Tracciato where VSAM = '" & VSAMName & "' order by ordinale")
          If Tbf.RecordCount = 0 Then
            word = 1
          Else
            Tbf.MoveLast
            word = Tbf!Ordinale + 1
          End If
          Tbf.AddNew
          'Tbf!idArchivio = wIdArc
          Tbf!VSam = VSAMName
          Tbf!idOggetto = wIdObj
          Tbf!Ordinale = word
          Tbf!idArea = wIdArea
          Tbf!Tag = "W"
          Tbf.Update
        End If
        Tbf.Close
        TbFile.MoveNext
      Loop
      TbFile.Close
    End If
    Tbf.Close
  End If
  Exit Sub
catch:
  addSegnalazione line, "P00", line
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 13-09-07
'''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parseDELETE(line As String, FD As Long)
  Dim k As Integer
  Dim statement As String, program As String, token As String, wComando As String, appo As String
  Dim endStatement As Boolean, continue As Boolean
  Dim linee As Long
  
  continue = True
  statement = line
  
  GbOldNumRec = GbNumRec  'linea iniziale
  
  wComando = nextToken_new(statement)
  
  If Mid(line, Len(line), 1) <> "." Then
    If InStr(line, " INVALID KEY") Then
      While Mid(line, Len(line), 1) <> "." And Not isInstruction(nexttoken(appo))
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        line = Trim(Mid(line, 8, 65))
        statement = statement + "@" + line
        appo = line
      Wend
      linee = GbNumRec - GbOldNumRec
    Else
      While Not EOF(FD) And Not endStatement
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
          line = Trim(Mid(line, 8, 65))
          If Len(line) Then
            If line <> "END-DELETE" Then
              If InStr(line, "INVALID KEY") Then
                statement = statement + "@" + line
                While Mid(line, Len(line), 1) <> "." And continue
                  Line Input #FD, line
                  GbNumRec = GbNumRec + 1
                  line = Trim(Mid(line, 8, 65))
                  appo = line
                  If Not isInstruction(nexttoken(appo)) Then
                    statement = statement + "@" + line
                  Else
                    continue = False
                  End If
                Wend
                endStatement = True
                linee = GbNumRec - GbOldNumRec
              Else
                token = nexttoken(line)
                If isInstruction(token) Then
                  endStatement = True
                Else
                  statement = statement & "@" & token & " " & line
                End If
              End If
            Else
              statement = statement + " " & line
              endStatement = True
            End If
          Else
            endStatement = Mid(token, Len(token), 1) = "."
            linee = GbNumRec - GbOldNumRec + 1
          End If
        End If
      Wend
    End If
  End If
  
  Dim rsDelete As Recordset
  Set rsDelete = m_Fun.Open_Recordset("select * from PsIO_Batch")
  rsDelete.AddNew
  rsDelete!idOggetto = GbIdOggetto
  rsDelete!area = "BATCH"
  rsDelete!Riga = GbOldNumRec
  rsDelete!comando = wComando
  rsDelete!istruzione = statement
  rsDelete!linee = GbNumRec - GbOldNumRec + 1
  rsDelete.Update
  rsDelete.Close
  
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''
' MF 14-09-07
''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parseSTART(ByRef line As String, FD As Long)
  Dim k As Integer
  Dim statement As String, program As String, token As String, wComando As String, appo As String
  Dim endStatement As Boolean
  Dim linee As Long
  
  statement = line
  
  GbOldNumRec = GbNumRec  'linea iniziale
  
  wComando = nextToken_new(statement)
  
  If Mid(line, Len(line), 1) <> "." Then
    If InStr(line, "KEY IS") Or InStr(line, " INVALID KEY") Then
      While Mid(line, IIf(Len(line) = 0, 1, Len(line)), 1) <> "." And Not endStatement
        ' Mauro 02/07/2009
        ' Ho copiato il blocco sotto perch� se ho un'istruzione qua dentro il parser la salta a piedi pari
''        Line Input #FD, line
''        GbNumRec = GbNumRec + 1
''        line = Trim(Mid(line, 8, 65))
''        statement = statement + "@" + line
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
          line = Trim(Mid(line, 8, 65))
          If Len(line) Then
            If line <> "END-START" Then
              If InStr(line, "KEY") Or InStr(line, "INVALID KEY") Then
                statement = statement + "@" + line
                While Mid(line, Len(line), 1) <> "." And line <> "END-START"
                  Line Input #FD, line
                  GbNumRec = GbNumRec + 1
                  line = Trim(Mid(line, 8, 65))
                  statement = statement + "@" + line
                Wend
                endStatement = True
                linee = GbNumRec - GbOldNumRec
              Else
                token = nexttoken(line)
                If isInstruction(token) Then
                  endStatement = True
                  line = Space(8) & token & " " & line
                Else
                  statement = statement & "@" & token & " " & line
                End If
              End If
            Else
              statement = statement + " " & line
              endStatement = True
              line = Space(8) & token & " " & line
            End If
          Else
            endStatement = Mid(token, IIf(Len(token) = 0, 1, Len(token)), 1) = "."
            linee = GbNumRec - GbOldNumRec
          End If
        End If
'''''''''''''''''''''''''''''''''''''''''''''''
      Wend
      linee = GbNumRec - GbOldNumRec
    Else
      While Not EOF(FD) And Not endStatement
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
          line = Trim(Mid(line, 8, 65))
          If Len(line) Then
            If line <> "END-START" Then
              If InStr(line, "KEY") Or InStr(line, "INVALID KEY") Then
                statement = statement + "@" + line
                While Mid(line, Len(line), 1) <> "." And line <> "END-START"
                  Line Input #FD, line
                  GbNumRec = GbNumRec + 1
                  line = Trim(Mid(line, 8, 65))
                  statement = statement + "@" + line
                Wend
                endStatement = True
                linee = GbNumRec - GbOldNumRec
              Else
                token = nexttoken(line)
                If isInstruction(token) Then
                  endStatement = True
                Else
                  statement = statement & "@" & token & " " & line
                End If
              End If
            Else
              statement = statement + " " & line
              endStatement = True
            End If
          Else
            endStatement = Mid(token, Len(token), 1) = "."
            linee = GbNumRec - GbOldNumRec
          End If
        End If
      Wend
    End If
  Else
    line = ""
  End If
  
  Dim rsStart As Recordset
  Set rsStart = m_Fun.Open_Recordset("select * from PsIO_Batch")
  rsStart.AddNew
  rsStart!idOggetto = GbIdOggetto
  rsStart!area = "BATCH"
  rsStart!Riga = GbOldNumRec
  rsStart!comando = wComando
  rsStart!istruzione = statement
  rsStart!linee = GbNumRec - GbOldNumRec + 1
  rsStart.Update
  rsStart.Close
End Sub

Private Sub parseSELECT(line As String, FD As Long)
  Dim token As String, statement As String
  Dim tmp() As String
  Dim i As Integer
  
  'Riconoscimento sicuro tipo programma:
  SwBatch = True
  
  'LETTURA intero statement:
  'SQ 10-02-06 - se c'e' una copy sbaglia! (non la esplode)
  'statement = Replace(LeggiBufferCbl(line, FD, "."), ".", "")
  statement = getCobolStatement(line, FD)
  
  'UTILIZZO STRUTTURA DEDICATA: SERVIRA' PER L'FD...
  i = UBound(assign_files)
  If Len(assign_files(i).ddname) Then
    i = i + 1
    ReDim Preserve assign_files(i)
  End If
  
  'NOME "INTERNO":
  assign_files(i).fileName = nexttoken(statement)
  'Attenzione: posso avere "OPTIONAL"/"NOT OPTIONAL"...
  'if token = "OPTIONAL" or token = "NOT"... 'posso avere "OPTIONAL"/"NOT OPTIONAL"...
  
  token = nexttoken(statement)  'ASSIGN
  token = nexttoken(statement)  'TO opzionale
  If token = "TO" Then
    tmp = Split(nexttoken(statement), "-")  'DEVO TENERE L'ULTIMO "PEZZO" DI NOME (SEPARATORE: "-")
  Else
    tmp = Split(token, "-")
  End If
  assign_files(i).ddname = tmp(UBound(tmp))
  
  'TIPO DI FILE:
  assign_files(i).filetype = "ESDS" 'INIT
  Do While Len(statement)
    token = nexttoken(statement)
    Select Case token
      Case "INDEXED"
        assign_files(i).filetype = "KSDS"
        SwVsam = True 'per la BS_Oggetti...
        Exit Do
      Case "RELATIVE"
        assign_files(i).filetype = "RRDS"
        SwVsam = True 'per la BS_Oggetti...
        Exit Do
    End Select
  Loop
  'AC cerco file status
  Do While Len(statement)
    token = nexttoken(statement)
    Select Case token
      Case "FILE"
      token = nexttoken(statement)
      If token = "STATUS" Then
        token = nexttoken(statement)
        If token = "IS" Then
          assign_files(i).filestatus = nexttoken(statement)
        Else
          'filestatus = token ' se IS fosse opzionale
        End If
        Exit Do
      End If
    End Select
  Loop
  'AC file status
  If Not assign_files(i).filestatus = "" Then
    AddFileStatus GbIdOggetto, assign_files(i).fileName, assign_files(i).filestatus
  End If
  'SQ: VERIFICARE SE SERVE...
  AggiungiComponente assign_files(i).fileName, assign_files(i).ddname, "AGN", "ASSIGN"

End Sub

Sub AddFileStatus(idOggetto As Long, fileName As String, filestatus As String)
  Dim tb As Recordset
  Set tb = m_Fun.Open_Recordset("select * from DMVSM_filestatus")
  tb.AddNew
  tb!idpgm = idOggetto
  tb!LINK = fileName
  tb!filestatus = filestatus
  tb.Update
  tb.Close
End Sub

'''''''''''''''''''''''''''''''''''''''''
' - Inserimento istruzione in PsExec (ex PsSys_Istr)
' - Inserimento in PsCom_Obj
'''''''''''''''''''''''''''''''''''''''''
'tmp Public
Public Sub getEXEC_SQL(statement As String)
  Dim k As Integer, k1 As Long
  Dim wStr1 As String, Wstr2 As String, wStr3 As String, WnomeTab As String
  Dim TbIstr As Recordset
  Dim wbuffer As String, wComando As String
  Dim tbTable As Recordset, wTypeSql As String

  If UCase(GbParmRel.DM_DefSqlType) = "ORACLE" Then
    wTypeSql = "DMORC_"
  Else
    wTypeSql = "DMDB2_"
  End If

  SwSql = True
  wbuffer = " " & statement
  ' Mauro 12/04/2010 : con la NextToken si portava dietro anche le stringhe tra apici
  'wComando = nextToken(statement)
  wComando = nextToken_newnew(statement)
  wStr1 = statement
  
  Set TbIstr = m_Fun.Open_Recordset("select * from PsExec where idoggetto = " & GbIdOggetto)
  TbIstr.AddNew
  TbIstr!idOggetto = GbIdOggetto
  TbIstr!area = "SQL"
  TbIstr!Riga = GbOldNumRec
  TbIstr!comando = wComando
  'SQ prova: si incartano i campi MEMO troppo lunghi! (fare compatta e ripristina DB...!)
  TbIstr!istruzione = statement
  'TbIstr!istruzione = Left(statement, 1024)
  TbIstr!linee = GbNumRec - GbOldNumRec + 1
  '''silvia
  If posDot > 0 Then
    TbIstr!dot = True
  End If
  '''
  TbIstr.Update
  TbIstr.Close
    
'  SQ 1-2-06 ''''''CreaIstrAmbExec GbIdOggetto, GbNumRec, statement, "SQL"
'  k = InStr(statement, " ")
'  If k > 0 Then
'    wComando = Mid$(statement, 1, k - 1)
'    wStr1 = statement
'    wStr1 = Trim(Replace(wStr1, wComando, ""))
'  Else
'    wComando = statement
'    wStr1 = ""
'  End If
  
  If wComando = "INCLUDE" Then
    parseINCLUDE statement
    Exit Sub
  End If
  
  ' Mauro 25/03/2010
  If wComando = "PREPARE" Then Exit Sub
  If wComando = "FETCH" Then Exit Sub
    
  k = InStr(wbuffer, " FROM ")
  If k > 0 Then
    wStr1 = Mid(wbuffer, k + 6)
    k1 = InStr(wStr1, " END-EXEC")
    If k1 > 0 Then wStr1 = Mid$(wStr1, 1, k1)
    k = InStr(wStr1, " WHERE ")
    If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
    k = InStr(wStr1, " ORDER ")
    If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
    k = InStr(wStr1, " VALUE(")
    If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
    k = InStr(wStr1, ",VALUE(")
    If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
    k = InStr(wStr1, " SET ")
    If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
    k = InStr(wStr1, " INTO ")
    If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
    k = InStr(wStr1, "(")
    If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
    k = InStr(wStr1, "*")
    If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
    k = InStr(wStr1, "--")
    If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
    k = InStr(wStr1, "FOR UPDATE OF")
    If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)

    While Len(wStr1) > 0
      k = InStr(wStr1, ",")
      If k > 0 Then
        Wstr2 = Mid$(wStr1, 1, k - 1)
        wStr1 = Mid$(wStr1, k + 1)
      Else
        Wstr2 = wStr1
        wStr1 = ""
      End If
      Wstr2 = Trim(Wstr2)
      k = InStr(Wstr2, " ")
      If k = 0 Then k = Len(Wstr2)
      WnomeTab = Trim(Mid$(Wstr2, 1, k))
      k = InStr(WnomeTab, ".")
      If k > 0 Then
        wStr3 = Mid$(WnomeTab, 1, k - 1)
        WnomeTab = Mid$(WnomeTab, k + 1)
      End If
      WnomeTab = Replace(WnomeTab, "'", "")
      WnomeTab = Replace(WnomeTab, "(", "")
      WnomeTab = Replace(WnomeTab, ")", "")
      If Trim(WnomeTab) <> "" Then
        AggiungiComponente WnomeTab, WnomeTab, "TBL", "SQL"
        ' Controllo se c'� la definizione nella *_Tabelle
        If UCase(WnomeTab) <> "SYSDUMMY1" Then
          Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "tabelle where nome = '" & WnomeTab & "'")
          If tbTable.RecordCount = 0 Then
            tbTable.Close
            Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "view where nome = '" & WnomeTab & "'")
            If tbTable.RecordCount = 0 Then
              addSegnalazione WnomeTab, "NOTBFND", ""
            End If
          End If
          tbTable.Close
        End If
      End If
    Wend
    If InStr(wbuffer, " IN ") > 0 Then
      k = InStr(wbuffer, " IN ")
      wStr1 = Mid$(wbuffer, k)
      If InStr(wStr1, " FROM ") > 0 Then
        k = InStr(wStr1, " FROM ")
        wStr1 = Mid$(wStr1, k + 6)
        k1 = InStr(wStr1, " END-EXEC")
        If k1 > 0 Then wStr1 = Mid$(wStr1, 1, k1)
        k = InStr(wStr1, " WHERE ")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
        k = InStr(wStr1, " ORDER ")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
        k = InStr(wStr1, " VALUE(")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
        k = InStr(wStr1, ",VALUE(")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
        k = InStr(wStr1, " SET ")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
        k = InStr(wStr1, "(")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
        k = InStr(wStr1, "*")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
        k = InStr(wStr1, "--")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
        
        While Len(wStr1) > 0
          k = InStr(wStr1, ",")
          If k > 0 Then
            Wstr2 = Mid$(wStr1, 1, k - 1)
            wStr1 = Mid$(wStr1, k + 1)
          Else
            Wstr2 = wStr1
            wStr1 = ""
          End If
          Wstr2 = Trim(Wstr2)
          k = InStr(Wstr2, " ")
          If k = 0 Then k = Len(Wstr2)
          WnomeTab = Trim(Mid$(Wstr2, 1, k))
          k = InStr(WnomeTab, ".")
          If k > 0 Then
            wStr3 = Mid$(WnomeTab, 1, k - 1)
            WnomeTab = Mid$(WnomeTab, k + 1)
          End If
          If Right(WnomeTab, 1) = ")" Then WnomeTab = Left(WnomeTab, Len(WnomeTab) - 1)
          If Trim(WnomeTab) <> "" Then
            AggiungiComponente WnomeTab, WnomeTab, "TBL", "SQL"
            ' Controllo se c'� la definizione nella *_Tabelle
            If UCase(WnomeTab) <> "SYSDUMMY1" Then
              Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "tabelle where nome = '" & WnomeTab & "'")
              If tbTable.RecordCount = 0 Then
                tbTable.Close
                Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "view where nome = '" & WnomeTab & "'")
                If tbTable.RecordCount = 0 Then
                  addSegnalazione WnomeTab, "NOTBFND", ""
                End If
              End If
              tbTable.Close
            End If
          End If
        Wend
      End If
    End If
    If InStr(wbuffer, " EXIST ") > 0 Then
      k = InStr(wbuffer, " EXIST ")
      wStr1 = Mid$(wbuffer, k)
      If InStr(wStr1, " FROM ") > 0 Then
        k = InStr(wStr1, " FROM ")
        wStr1 = Mid$(wStr1, k + 6)
        k1 = InStr(wStr1, " END-EXEC")
        If k1 > 0 Then wStr1 = Mid$(wStr1, 1, k1)
        k = InStr(wStr1, " WHERE ")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
        k = InStr(wStr1, " ORDER ")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
        k = InStr(wStr1, " VALUE(")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
        k = InStr(wStr1, ",VALUE(")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
        k = InStr(wStr1, " SET ")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k)
        k = InStr(wStr1, "(")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
        k = InStr(wStr1, "*")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
        k = InStr(wStr1, "--")
        If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
         
        While Len(wStr1) > 0
          k = InStr(wStr1, ",")
          If k > 0 Then
            Wstr2 = Mid$(wStr1, 1, k - 1)
            wStr1 = Mid$(wStr1, k + 1)
          Else
            Wstr2 = wStr1
            wStr1 = ""
          End If
          Wstr2 = Trim(Wstr2)
          k = InStr(Wstr2, " ")
          If k = 0 Then k = Len(Wstr2)
          WnomeTab = Trim(Mid$(Wstr2, 1, k))
          k = InStr(WnomeTab, ".")
          If k > 0 Then
            wStr3 = Mid$(WnomeTab, 1, k - 1)
            WnomeTab = Mid$(WnomeTab, k + 1)
          End If
          If Trim(WnomeTab) <> "" Then
            AggiungiComponente WnomeTab, WnomeTab, "TBL", "SQL"
            ' Controllo se c'� la definizione nella *_Tabelle
            If UCase(WnomeTab) <> "SYSDUMMY1" Then
              Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "tabelle where nome = '" & WnomeTab & "'")
              If tbTable.RecordCount = 0 Then
                tbTable.Close
                Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "view where nome = '" & WnomeTab & "'")
                If tbTable.RecordCount = 0 Then
                  addSegnalazione WnomeTab, "NOTBFND", ""
                End If
              End If
              tbTable.Close
            End If
          End If
        Wend
      End If
    End If
  Else
    If InStr(wbuffer, " DECLARE ") > 0 Then
      If InStr(wbuffer, " TABLE ") > 0 And InStr(wbuffer, " TEMPORARY ") > 0 Then
        WnomeTab = nextToken_newnew(statement)
        If WnomeTab = "GLOBAL" Then
          WnomeTab = TrovaParamDDL(wbuffer, " TABLE ")
        End If
        k = InStr(WnomeTab, ".")
        If k > 0 Then
          wStr3 = Mid$(WnomeTab, 1, k - 1)
          WnomeTab = Mid$(WnomeTab, k + 1)
        End If
        Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "tabelle where " & _
                                           "nome = '" & WnomeTab & "'" & " AND creator= '" & wStr3 & "'")
        If tbTable.RecordCount = 0 Then
          tbTable.AddNew
          tbTable!IdDB = 0
          tbTable!NomeDb = ""
          tbTable!IdTableSpc = 0
          tbTable!nome = WnomeTab
          tbTable!creator = wStr3
          tbTable!data_creazione = Now
          tbTable!data_modifica = Now
          tbTable!idtablejoin = -1
          tbTable!idorigine = -1
          tbTable!Tag = "Temporary Table"
          tbTable!idObjSource = GbIdOggetto
          tbTable.Update
        End If
        tbTable.Close
        Parser.PsConnection.Execute "Delete * from bs_segnalazioni where " & _
                                    "var1 = '" & WnomeTab & "' and codice = 'F24' and idoggetto = " & GbIdOggetto
      Else
        Exit Sub
      End If
    End If
    If InStr(wbuffer, " OPEN ") > 0 Then Exit Sub
    If InStr(wbuffer, " CLOSE ") > 0 Then Exit Sub
    If InStr(wbuffer, " FETCH ") > 0 Then Exit Sub
    If InStr(wbuffer, " ROLLBACK ") > 0 Then Exit Sub
    If InStr(wbuffer, " COMMIT ") > 0 Then Exit Sub

    k = InStr(wbuffer, " INSERT ")
    If k > 0 Then
      k = InStr(wbuffer, " INTO ")
      wStr1 = LTrim(Mid$(wbuffer, k + 6))
      k = InStr(wStr1, "(")
      If k > 0 Then
        wStr1 = Mid$(wStr1, 1, k - 1)
      End If
      k = InStr(wStr1, " ")
      If k > 0 Then
        wStr1 = Mid$(wStr1, 1, k - 1)
      End If
      WnomeTab = Trim(wStr1)
      k = InStr(WnomeTab, ".")
      If k > 0 Then
        wStr3 = Mid$(WnomeTab, 1, k - 1)
        WnomeTab = Mid$(WnomeTab, k + 1)
      End If
      If Trim(WnomeTab) <> "" Then
        AggiungiComponente WnomeTab, WnomeTab, "TBL", "SQL"
        ' Controllo se c'� la definizione nella *_Tabelle
        If UCase(WnomeTab) <> "SYSDUMMY1" Then
          Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "tabelle where nome = '" & WnomeTab & "'")
          If tbTable.RecordCount = 0 Then
            tbTable.Close
            Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "view where nome = '" & WnomeTab & "'")
            If tbTable.RecordCount = 0 Then
              addSegnalazione WnomeTab, "NOTBFND", ""
            End If
          End If
          tbTable.Close
        End If
      End If
      Exit Sub
    End If
    k = InStr(wbuffer, " UPDATE ")
    If k > 0 Then
      wStr1 = LTrim(Mid$(wbuffer, k + 8))
      k = InStr(wStr1, "(")
      If k > 0 Then
        wStr1 = Mid$(wStr1, 1, k - 1)
      End If
      k = InStr(wStr1, " ")
      If k > 0 Then
        wStr1 = Mid$(wStr1, 1, k - 1)
      End If
      WnomeTab = Trim(wStr1)
      k = InStr(WnomeTab, ".")
      If k > 0 Then
        wStr3 = Mid$(WnomeTab, 1, k - 1)
        WnomeTab = Mid$(WnomeTab, k + 1)
      End If
      If Trim(WnomeTab) <> "" Then
        AggiungiComponente WnomeTab, WnomeTab, "TBL", "SQL"
        ' Controllo se c'� la definizione nella *_Tabelle
        If UCase(WnomeTab) <> "SYSDUMMY1" Then
          Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "tabelle where nome = '" & WnomeTab & "'")
          If tbTable.RecordCount = 0 Then
            tbTable.Close
            Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "view where nome = '" & WnomeTab & "'")
            If tbTable.RecordCount = 0 Then
              addSegnalazione WnomeTab, "NOTBFND", ""
            End If
          End If
          tbTable.Close
        End If
      End If
      Exit Sub
    End If
    k = InStr(wbuffer, " LOCK ")
    If k > 0 Then
      k = InStr(wbuffer, " TABLE ")
      wStr1 = LTrim(Mid$(wbuffer, k + 7))
      k = InStr(wStr1, "(")
      If k > 0 Then
        wStr1 = Mid$(wStr1, 1, k - 1)
      End If
      k = InStr(wStr1, " ")
      If k > 0 Then
        wStr1 = Mid$(wStr1, 1, k - 1)
      End If
      WnomeTab = Trim(wStr1)
      k = InStr(WnomeTab, ".")
      If k > 0 Then
        wStr3 = Mid$(WnomeTab, 1, k - 1)
        WnomeTab = Mid$(WnomeTab, k + 1)
      End If
      If Trim(WnomeTab) <> "" Then
        AggiungiComponente WnomeTab, WnomeTab, "TBL", "SQL"
        ' Controllo se c'� la definizione nella *_Tabelle
        If UCase(WnomeTab) <> "SYSDUMMY1" Then
          Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "tabelle where nome = '" & WnomeTab & "'")
          If tbTable.RecordCount = 0 Then
            tbTable.Close
            Set tbTable = m_Fun.Open_Recordset("select nome from " & wTypeSql & "view where nome = '" & WnomeTab & "'")
            If tbTable.RecordCount = 0 Then
              addSegnalazione WnomeTab, "NOTBFND", ""
            End If
          End If
          tbTable.Close
        End If
      End If
      Exit Sub
    End If

    If InStr(UCase(wbuffer), " SET ") > 0 Then Exit Sub
    If InStr(UCase(wbuffer), " TIMESTAMP ") > 0 Then Exit Sub
    k = InStr(wbuffer, " DELETE ")
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Restituisce il primo statement della copy (fino al punto)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function readCopyFile(idCopy As Long) As String
  Dim fileName As String, line As String
  Dim FD As Long, lineNumber As Integer
  Dim rs As Recordset
  On Error GoTo funcErr  'a livello superiore...
  
  'ATTENZIONE: non devo incrementare il contatore di linea...
  lineNumber = GbNumRec
    
  Set rs = m_Fun.Open_Recordset("select nome,estensione,Directory_Input from BS_oggetti where idoggetto = " & idCopy & " and tipo = 'CPY'")
  If rs.RecordCount Then
    fileName = m_Fun.FnPathDef & Mid(rs!Directory_Input, 2) & "\" & rs!nome & IIf(Len(rs!estensione), "." & rs!estensione, "")
    FD = FreeFile
    Open fileName For Input As FD
    'Devo raggiungere la prima linea valida, per sfruttare getCobolStatement
    Do While True
      Line Input #FD, line
      If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
        'LINEA VALIDA:
        line = RTrim(Mid(line, 8, 65)) 'elimino etichette!
        Exit Do
      End If
    Loop
    readCopyFile = getCobolStatement(line, FD)
    'restore
    GbNumRec = lineNumber
    Close FD
  End If
  Exit Function
funcErr:
  readCopyFile = ""
  'restore
  GbNumRec = lineNumber
  'scrivere log...
End Function
''''''''''''''''''''''''''''''''''''''''''''''''
' Delete tabelle allestite dal parser (I)
''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub resetRepository()
  On Error GoTo dbErr
  
  Parser.PsConnection.Execute "Delete * from PsPgm where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsPgm_Copy where idPgm =" & GbIdOggetto
  
  Exit Sub
dbErr:
  m_Fun.Show_MsgBoxError "RC0002", , "resetRepository", err.source, err.description, False
End Sub

Function CountWords(ByVal Text As String, ByVal word As String) As Long
  Dim Position As Long, WordLength As Long
  
  Position = InStr(1, Text, word)
  WordLength = Len(word)
  Do While Position
    CountWords = CountWords + 1
    Position = InStr(Position + WordLength, Text, word)
  Loop
End Function
