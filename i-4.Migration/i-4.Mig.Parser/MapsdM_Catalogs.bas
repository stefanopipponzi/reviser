Attribute VB_Name = "MapsdM_Catalogs"
Option Explicit
''''''''''''''''''''''''''''''''''''''''''
' Import Catalogo CICS "completo"
' Scriviamo:
'  - FCT
'  - PPT
'  - PCT
''''''''''''''''''''''''''''''''''''''''''
Sub parseDFHCSDUP(fileInput As String)
  Dim FD As Long
  Dim line As String, token As String
  Dim rsOggetti As Recordset
  
  On Error GoTo parseErr
   
  GbNumRec = 0
  FD = FreeFile
  Open fileInput For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    Else
      'Ho gia' una linea da analizzare (letta dalla getCall, per es.)
    End If
    
    If Left(line, 1) <> "*" Then
      token = nextToken_new(line)
      Select Case token
        Case "DEFINE"
          token = nexttoken(line)
          Select Case token
            Case "FILE"
              parseDefineFile line, FD
            Case "PROGRAM"
              parseDefineProgram line, FD
            Case "TRANSACTION"
              parseDefineTransaction line, FD
            Case "MAPSET"
              parseDefineMapset line, FD
            Case "CONNECTION"
              parseDefineConnection line, FD
            Case "SESSIONS"
              parseDefineSessions line, FD
            Case "TERMINAL"
              parseDefineTerminal line, FD
            Case "PROFILE"
              parseDefineProfile line, FD
            Case "TRANCLASS"
              parseDefineTranclass line, FD
            Case "TYPETERM"
              parseDefineTypeterm line, FD
            Case "JOURNALMODEL"
              parseDefineJournalmodel line, FD
            Case Else
              'MsgBox "Else: " & token
              token = nextToken_new(line)
          End Select
        Case Else
          'MsgBox "Else: " & token
          token = nextToken_new(line)
      End Select
    Else
      line = ""
    End If
  Wend
  Close FD
   
  MsgBox "Import complete!", vbOKOnly + vbInformation, Parser.PsNomeProdotto
  
  Exit Sub
parseErr:  'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          GbOldNumRec = GbNumRec
          m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & err.description
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        GbOldNumRec = GbNumRec
        addSegnalazione line, "P00", line
        m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & err.description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & err.description
    Resume Next
  Else
    GbOldNumRec = GbNumRec
    m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & err.description
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''
' Import Catalogo PCT "completo"
' Scriviamo:
'  - PCT
''''''''''''''''''''''''''''''''''''''''''
Sub parsePCT(fileInput As String)
  Dim FD As Long
  Dim line As String, token As String, program As String, transaction As String
  Dim tbCics As Recordset, rs As Recordset
  Dim ArrToken() As String
  
  On Error GoTo parseErr
   
  GbNumRec = 0
  FD = FreeFile
  Open fileInput For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    Else
      'Ho gia' una linea da analizzare (letta dalla getCall, per es.)
    End If
    
    If Left(line, 1) <> "*" Then
      token = nextToken_tmp(line)
      If Len(token) = 0 Then token = " "
      ArrToken() = Split(token, "=")
      Select Case ArrToken(0)
        Case "PROGRAM"
          If UBound(ArrToken) Then
            program = ArrToken(1)
          Else
            program = Replace(Replace(nextToken_tmp(line), "(", ""), ")", "")
          End If
        Case "TRANSID", "TRANSACTION"
          If UBound(ArrToken) Then
            transaction = ArrToken(1)
          Else
            transaction = Replace(Replace(nextToken_tmp(line), "(", ""), ")", "")
          End If
'        Case Else
'          'MsgBox "Else: " & token
'          token = nextToken_tmp(line)
      End Select
    Else
      line = ""
    End If
    If Len(program) > 0 And Len(transaction) > 0 Then
      Set tbCics = m_Fun.Open_Recordset("select * from CICS_PCT where program = '" & program & "' and transaction = '" & transaction & "'")
      If tbCics.RecordCount = 0 Then
        tbCics.AddNew
        tbCics!transaction = transaction
        tbCics!program = program
        tbCics.Update
      End If
      tbCics.Close
      'T 10-07-2009
      Set rs = m_Fun.Open_Recordset("select main from Bs_Oggetti as a, PsPgm as b where a.Nome = '" & program & "' and a.idoggetto = b.idoggetto ")
      If rs.RecordCount Then
        rs!main = True
        rs.Update
      End If
      rs.Close
      '---TILVIA
      program = ""
      transaction = ""
    End If
  Wend
  Close FD
   
  MsgBox "Import complete!", vbOKOnly + vbInformation, Parser.PsNomeProdotto
  
  Exit Sub
parseErr:  'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          GbOldNumRec = GbNumRec
          addSegnalazione line, "P00", line
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        GbOldNumRec = GbNumRec
        addSegnalazione line, "P00", line
        'm_Fun.WriteLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & Err.Description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    Resume Next
  Else
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
  End If
End Sub

Public Sub parseDefineFile(line As String, FD As Long)
   Dim token As String
   Dim fileName, dsName As String
   Dim tbCics As Recordset
  
  On Error GoTo catch
  
   token = nextToken_new(line)
   fileName = Mid(token, 2, Len(token) - 2)
   Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
      If Len(line) = 0 Then
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Left(line, 2) <> "  " And Len(line) > 0 Then
          token = nextToken_tmp(line)
          If token <> "DEFINE" And token <> "DESCRIPTION" Then
            line = "DEFINE " & token & " " & line
          Else
            line = token & " " & line
          End If
          token = ""
        End If
      End If
      If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
         token = nextToken_new(line)
         Select Case token
            Case "DSNAME"
               token = nextToken_new(line)
               dsName = Mid(token, 2, Len(token) - 2)
            Case Else
               token = nextToken_new(line)
         End Select
      End If
  Loop
  
  Set tbCics = m_Fun.Open_Recordset("select * from CICS_FCT where fil = '" & fileName & "' and dsn = '" & dsName & "'")
  If tbCics.RecordCount = 0 Then
     tbCics.AddNew
     tbCics!fil = fileName
     tbCics!dsn = dsName
     tbCics.Update
  End If
  tbCics.Close
  Exit Sub
catch:
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      Resume Next
      '...
    Else
      err.Raise 124, "parseDefineFile", "syntax error on " & line
      'Resume
    End If
  Else
    m_Fun.writeLog "#DEFINE FILE: problems on line: " & GbNumRec & "#" & err.description
  End If
End Sub

Public Sub parseDefineProgram(line As String, FD As Long)
  Dim token As String
  Dim programName, language As String
  Dim tbCics As Recordset
   
  On Error GoTo catch
   
  token = nextToken_new(line)
  If Not Left(token, 1) = "(" Or Not Right(token, 1) = ")" Then
    Exit Sub
  End If
  programName = Mid(token, 2, Len(token) - 2)
   
  Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    End If
    If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
      token = nextToken_new(line)
      Select Case token
        Case "LANGUAGE"
          token = nextToken_new(line)
          language = Mid(token, 2, Len(token) - 2)
        Case Else
          token = nextToken_new(line)
      End Select
    End If
  Loop
   
  Set tbCics = m_Fun.Open_Recordset("select * from CICS_PPT where program = '" & programName & "'")
  If tbCics.RecordCount = 0 Then
    tbCics.AddNew
    tbCics!program = programName
    tbCics!language = language
    tbCics.Update
  End If
  tbCics.Close
   
  'Potremmo controllare il "language" nella Bs_oggetti
  Exit Sub
catch:
  m_Fun.writeLog "#DEFINE FILE: problems on line: " & GbNumRec & "#" & err.description
End Sub

Public Sub parseDefineTransaction(line As String, FD As Long)
  Dim token As String
  Dim transactionName, program As String
  Dim tbCics As Recordset, rsTemp As Recordset
   
  On Error GoTo catch
   
  token = nextToken_new(line)
  transactionName = Mid(token, 2, Len(token) - 2)
   
  Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" And token <> "DESCRIPTION" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    End If
    If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
      token = nextToken_new(line)
      Select Case token
        Case "PROGRAM"
          token = nextToken_new(line)
          program = Mid(token, 2, Len(token) - 2)
        Case Else
          token = nextToken_new(line)
      End Select
    End If
  Loop
   
  Set tbCics = m_Fun.Open_Recordset("select * from CICS_PCT where transaction = '" & transactionName & "' and program = '" & program & "'")
  If tbCics.RecordCount = 0 Then
    tbCics.AddNew
    tbCics!transaction = transactionName
    tbCics!program = program
    tbCics.Update
  End If
  tbCics.Close
  '''''''''''''''''''''''''''
  ' Aggiornamento BS_oggetti:
  ' - "program" � un CICS
  '''''''''''''''''''''''''''
  Set rsTemp = m_Fun.Open_Recordset("select * from bs_oggetti where nome = '" & program & "' and tipo in ('CBL','ASM','PLI')")
  If rsTemp.RecordCount = 0 Then
    'Nome LOAD diverso da nome SOURCE: aggiorniamo il recordset
    Set rsTemp = m_Fun.Open_Recordset("select a.* from bs_oggetti as a,PsLoad as b where b.load = '" & program & "' and b.source=a.nome and tipo in ('CBL','ASM','PLI')")
  End If
  If Not (rsTemp.EOF) Then
    rsTemp!Cics = True
    rsTemp.Update
  End If
  rsTemp.Close
   
  Exit Sub
catch:
  m_Fun.writeLog "#DEFINE TRANSACTION: problems on line: " & GbNumRec & "#" & err.description
End Sub

Public Sub parseDefineMapset(line As String, FD As Long)
  Dim token As String
  Dim mapsetName As String
  Dim tbCics As Recordset

  On Error GoTo catch:
   
  token = nextToken_new(line)
  mapsetName = Mid(token, 2, Len(token) - 2)
   
  Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    End If
    If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
      token = nextToken_new(line)
      Select Case token
        Case Else
          token = nextToken_new(line)
      End Select
    End If
  Loop
  'SILVIA 21-10-2008
  Set tbCics = m_Fun.Open_Recordset("select * from CICS_PPT where program = '" & mapsetName & "'")
  If tbCics.RecordCount = 0 Then
    tbCics.AddNew
    tbCics!program = mapsetName
    'SILVIA
    tbCics!language = "MAPSET"
    tbCics.Update
  End If
  tbCics.Close

  Exit Sub
catch:
  m_Fun.writeLog "#DEFINE MAPSET: problems on line: " & GbNumRec & "#" & err.description
End Sub

Public Sub parseDefineConnection(line As String, FD As Long)
  Dim token As String
  Dim connectionName As String
   
  token = nextToken_new(line)
  connectionName = Mid(token, 2, Len(token) - 2)
   
  Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    End If
    If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
      token = nextToken_new(line)
      Select Case token
        Case Else
          token = nextToken_new(line)
      End Select
    End If
  Loop
End Sub

Public Sub parseDefineSessions(line As String, FD As Long)
  Dim token As String
  Dim sessionName As String
   
  token = nextToken_new(line)
  sessionName = Mid(token, 2, Len(token) - 2)
   
  Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    End If
    If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
      token = nextToken_new(line)
      Select Case token
        Case Else
          token = nextToken_new(line)
      End Select
    End If
  Loop
End Sub

Public Sub parseDefineTerminal(line As String, FD As Long)
  Dim token As String
  Dim terminalName As String
   
  token = nextToken_new(line)
  terminalName = Mid(token, 2, Len(token) - 2)
   
  Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    End If
    If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
      token = nextToken_new(line)
      Select Case token
        Case Else
          token = nextToken_new(line)
      End Select
    End If
  Loop
End Sub

Public Sub parseDefineProfile(line As String, FD As Long)
  Dim token As String
  Dim profileName As String
   
  token = nextToken_new(line)
  profileName = Mid(token, 2, Len(token) - 2)
   
  Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    End If
    If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
      token = nextToken_new(line)
      Select Case token
        Case Else
          token = nextToken_new(line)
      End Select
    End If
  Loop
End Sub

Public Sub parseDefineTranclass(line As String, FD As Long)
  Dim token As String
  Dim tranclassName As String
  
  token = nextToken_new(line)
  tranclassName = Mid(token, 2, Len(token) - 2)
  
  Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    End If
    If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
      token = nextToken_new(line)
      Select Case token
        Case Else
          token = nextToken_new(line)
      End Select
    End If
  Loop
End Sub

Public Sub parseDefineTypeterm(line As String, FD As Long)
  Dim token As String
  Dim typetermName As String
  token = nextToken_new(line)
  typetermName = Mid(token, 2, Len(token) - 2)
  
  Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    End If
    If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
      token = nextToken_new(line)
      Select Case token
        Case Else
          token = nextToken_new(line)
      End Select
    End If
  Loop
End Sub

Public Sub parseDefineJournalmodel(line As String, FD As Long)
  Dim token As String
  Dim journalmodelName As String
  
  token = nextToken_new(line)
  journalmodelName = Mid(token, 2, Len(token) - 2)
  
  Do While InStr(line, "DEFINE") = 0 And Not (EOF(FD))
    If Len(line) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 2) <> "  " And Len(line) > 0 Then
        token = nextToken_tmp(line)
        If token <> "DEFINE" Then
          line = "DEFINE " & token & " " & line
        Else
          line = token & " " & line
        End If
        token = ""
      End If
    End If
    If Left(line, 1) <> "*" And InStr(line, "DEFINE") = 0 And Not (EOF(FD)) Then
      token = nextToken_new(line)
      Select Case token
        Case Else
          token = nextToken_new(line)
      End Select
    End If
  Loop
End Sub

Sub parseCONTENTS(fileInput As String)
    Dim FD As Long
  Dim line As String, token As String
  Dim rsDataSet As Recordset, rsColumn As Recordset
  
  Dim StDATASETNAME As String, stFileName As String, stName As String
  Dim RTB As RichTextBox

  Set tagValues = New Collection
  
  Set RTB = MapsdF_Parser.rText
  Const migrationPath = "Output-prj\Languages\SAS2Java"
  Const templatePath = "Input-prj\Languages\SAS2Java"

  On Error GoTo parseErr
   
  GbNumRec = 0
  FD = FreeFile
  Open fileInput For Input As FD
  While Not EOF(FD)
    Line Input #FD, line
    GbNumRec = GbNumRec + 1
    If InStr(line, "The SAS System") Then
      Line Input #FD, line
      Line Input #FD, line
      Line Input #FD, line
      Line Input #FD, line
      GbNumRec = GbNumRec + 4
    End If
    If InStr(line, "Data Set Name:") Then
      'DataSet name:
      StDATASETNAME = nextToken_new(Mid(Trim(line), 15))
      StDATASETNAME = IIf(InStr(StDATASETNAME, "."), Mid(StDATASETNAME, InStr(StDATASETNAME, ".") + 1), StDATASETNAME)
      Set rsDataSet = m_Fun.Open_Recordset("select * from SAS_Dataset where TableName = '" & StDATASETNAME & "'")
      If rsDataSet.RecordCount = 0 Then
          rsDataSet.AddNew
          rsDataSet!tableName = StDATASETNAME
          rsDataSet.Update
      End If
      rsDataSet.Close
    End If
    If InStr(line, "-----Alphabetic List of Variables") Then
      Line Input #FD, line
      Line Input #FD, line
    End If
    If InStr(line, "#    Variable") Then
       Line Input #FD, line
       Line Input #FD, line
       GbNumRec = GbNumRec + 2
      Set rsColumn = m_Fun.Open_Recordset("select * from SAS_Column_PERM where TableName = '" & StDATASETNAME & "'")

     Do While (InStr(line, "The SAS System") = 0 And InStr(line, "-----Alphabetic List of Indexes") = 0 And InStr(line, "-----Sort Information-----") = 0) And Not EOF(FD)
       If Len(line) Then
          rsColumn.AddNew
          rsColumn!tableName = StDATASETNAME
          token = nextToken_new(line)
          rsColumn!columnpos = token
          token = nextToken_new(line)
          stName = token
          rsColumn!Name = token
          token = nextToken_new(line)
          Select Case UCase(token)
            Case "NUM"
              If InStr(line, " DOLLAR") Then
                 token = "Double"
              ElseIf InStr(line, " DATE") Then
                token = "Date"
              Else
                If InStr(stName, "_DT") Or InStr(stName, "_TM") Or InStr(stName, "DATE") Or InStr(stName, "TIME") Then
                  token = "Date"
                Else
                  token = "Integer"
                End If
              End If
            Case "CHAR"
              token = "String"
          End Select
          rsColumn!Type = token
          token = nextToken_new(line)
          rsColumn!Length = token
          token = nextToken_new(line)
          rsColumn!Position = token
        End If
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
     Loop
     rsColumn.Update
     rsColumn.Close
    End If
    If InStr(line, "#    Index") Then
      Line Input #FD, line
      Line Input #FD, line
      GbNumRec = GbNumRec + 2
      Do While InStr(line, "The SAS System") = 0 And Not EOF(FD)
        token = nextToken_new(line)
        token = nextToken_new(line)
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
      Loop
    End If
  Wend
  Close FD
  
  MsgBox "Import complete!", vbOKOnly + vbInformation, Parser.PsNomeProdotto
  
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & err.description
End Sub

Sub parseCONTENTS_XML(fileInput As String)
    Dim FD As Long
  Dim line As String, token As String
  Dim rsDataSet As Recordset, rsColumn As Recordset
  
  Dim StDATASETNAME As String, stFileName As String, stName As String
  Dim RTB As RichTextBox

  Set tagValues = New Collection
  
  Set RTB = MapsdF_Parser.rText
  Const migrationPath = "Output-prj\Languages\SAS2Java"
  Const templatePath = "Input-prj\Languages\SAS2Java"

  RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\ContentsExp"
  
  tagValues.Add "", "NAMEDATASET(i)"
  tagValues.Add "", "COLUMNDESC(i)"
  tagValues.Add "", "INDEXLIST(i)"

  
  On Error GoTo parseErr
   
  GbNumRec = 0
  FD = FreeFile
  Open fileInput For Input As FD
  While Not EOF(FD)
    Line Input #FD, line
    GbNumRec = GbNumRec + 1
    If InStr(line, "The SAS System") Then
      Line Input #FD, line
      Line Input #FD, line
      Line Input #FD, line
      Line Input #FD, line
      GbNumRec = GbNumRec + 4
    End If
    If InStr(line, "Data Set Name:") Then
      'DataSet name:
      StDATASETNAME = nextToken_new(Mid(Trim(line), 15))
      StDATASETNAME = IIf(InStr(StDATASETNAME, "."), Mid(StDATASETNAME, InStr(StDATASETNAME, ".") + 1), StDATASETNAME)
      changeTag RTB, "<<COLUMNDESC(i)>>", ""
      changeTag RTB, "<<INDEXLIST(i)>>", ""
      changeTag RTB, "<<NAMEDATASET(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\NamedataSet"
      changeTag RTB, "<DATASETNAME>", StDATASETNAME
      Set rsDataSet = m_Fun.Open_Recordset("select * from SAS_Dataset where TableName = '" & StDATASETNAME & "'")
      If rsDataSet.RecordCount = 0 Then
          rsDataSet.AddNew
          rsDataSet!tableName = StDATASETNAME
          rsDataSet.Update
      End If
      rsDataSet.Close
    End If
    If InStr(line, "-----Alphabetic List of Variables") Then
      Line Input #FD, line
      Line Input #FD, line
    End If
    If InStr(line, "#    Variable") Then
       Line Input #FD, line
       Line Input #FD, line
       GbNumRec = GbNumRec + 2
       'changeTag RTB, "<<COLUMNDESC(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\ColumnDesc"
     'ColumnDesc
'      <column-position><COLPOS></column-position>
'      <name><NAME></name>
'      <type><TYPE></type>
'      <length><LENGTH></length>
'      <position><POS></position>
      Set rsColumn = m_Fun.Open_Recordset("select * from SAS_Column_PERM where TableName = '" & StDATASETNAME & "'")

     Do While (InStr(line, "The SAS System") = 0 And InStr(line, "-----Alphabetic List of Indexes") = 0) And Not EOF(FD)
       If Len(line) Then
          rsColumn.AddNew
          rsColumn!tableName = StDATASETNAME
          changeTag RTB, "<<COLUMNDESC(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\ColumnDesc"
          token = nextToken_new(line)
          changeTag RTB, "<COLPOS>", token
          rsColumn!columnpos = token
          token = nextToken_new(line)
          stName = token
          rsColumn!Name = token
          changeTag RTB, "<NOME>", token
          token = nextToken_new(line)
          Select Case UCase(token)
            Case "NUM"
              If InStr(line, " DOLLAR") Then
                 token = "Double"
              ElseIf InStr(line, " DATE") Then
                token = "Date"
              Else
                If InStr(stName, "_DT") Or InStr(stName, "_TM") Or InStr(stName, "DATE") Or InStr(stName, "TIME") Then
                  token = "Date"
                Else
                  token = "Integer"
                End If
              End If
            Case "CHAR"
              token = "String"
          End Select
          rsColumn!Type = token
          changeTag RTB, "<TIPO>", token
          token = nextToken_new(line)
          rsColumn!Length = token
          changeTag RTB, "<LUNGHEZZA>", token
          token = nextToken_new(line)
          rsColumn!Position = token
          changeTag RTB, "<POS>", token
        End If
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
     Loop
     rsColumn.Update
     rsColumn.Close
    End If
    If InStr(line, "#    Index") Then
      Line Input #FD, line
      Line Input #FD, line
      GbNumRec = GbNumRec + 2
      Do While InStr(line, "The SAS System") = 0 And Not EOF(FD)
        changeTag RTB, "<<INDEXLIST(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\Index"
        token = nextToken_new(line)
        token = nextToken_new(line)
        changeTag RTB, "<INDEXNAME>", token
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
      Loop
    End If
  Wend
  Close FD
  cleanTags RTB
  RTB.Text = Replace(RTB.Text, "      " & vbCrLf, "")
  stFileName = Split(fileInput, "\")(UBound(Split(fileInput, "\")))
  On Error GoTo parseErr
  RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\XML\" & stFileName & ".XML", 1
  
  MsgBox "Import complete!", vbOKOnly + vbInformation, Parser.PsNomeProdotto
  
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & err.description
End Sub

