Attribute VB_Name = "MapsdM_TELON"
Option Explicit
'silvia 01-10-2008
Const migrationPath = "Output-prj\Languages\TELON2Cbl"

Public Sub TELON_CBLization()
  Dim numFile As Integer
  Dim outFile As Integer
  Dim cpfile As Integer
  Dim outCopy As Integer, copyLev As Integer
  Dim inLine As String, cleanLine As String, copyname As String
  Dim bolIsCopy As Boolean
  Dim i As Integer
  On Error GoTo fileErr
  
  
 ' ReDim outCopy(0)
  copyLev = 0
  bolIsCopy = False
  numFile = 1
  'outCopy = FreeFile
  Open GbFileInput For Input As numFile
  outFile = 2
  cpfile = 3
  Open m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm For Output As outFile
  Do While Not EOF(numFile)
    Line Input #numFile, inLine
    
    ' *         T E L O N   R E L E A S E   D A T A          *
    If InStr(inLine, " *         T E L O N   R E L E A S E   D A T A          *") And bolIsCopy = False Then
      Line Input #numFile, inLine
      For i = 0 To 20
        Line Input #numFile, inLine
        If InStr(inLine, "********************************************************") Then
          Line Input #numFile, inLine
          Exit For
        End If
      Next i
    End If
    '
    'TELON-RELEASE-EYECATCH.
    If InStr(inLine, "TELON-RELEASE-EYECATCH.") Then
        Line Input #numFile, inLine
    End If
    '''
    
    If Mid(inLine, 7, 8) = "*TELON--" And bolIsCopy = False Then
      Line Input #numFile, inLine
    End If
    '*DS: H05 o H07
    If InStr(inLine, "*DS: H0") And bolIsCopy = False Then
      Line Input #numFile, inLine
    End If
    If (InStr(inLine, "--! END ") Or InStr(inLine, "--| END ")) And bolIsCopy = False Then
      Line Input #numFile, inLine
    End If

    If (InStr(inLine, "! COPY ") Or InStr(inLine, "| COPY ")) And bolIsCopy = False Then
      copyname = Trim(Mid(inLine, 62, 8))
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy\" & copyname For Output As cpfile
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
      Line Input #numFile, inLine
      Line Input #numFile, inLine
    End If
    If (InStr(inLine, "--! END " & copyname) Or InStr(inLine, "--| END " & copyname)) And bolIsCopy = True Then
      Line Input #numFile, inLine
      Print #cpfile, inLine
      bolIsCopy = False
      Line Input #numFile, inLine
      Close #cpfile
     End If
    If bolIsCopy = True Then
       Print #cpfile, inLine
    End If
' "! COPY " & copyname
    ' "--! END " & copyname
    'Else
    If bolIsCopy = False Then
      Print #outFile, inLine
    End If
  Loop
  Close #outFile
  Close #numFile
  
  Exit Sub
fileErr:
  If err.Number = 76 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath & "\copy\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\"
    Resume
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
  End If
  Exit Sub
err_catch:
  m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
End Sub


'---------

Public Sub TELON_CBLClean()
  Dim numFile As Integer
  Dim outFile As Integer
  Dim outCopy As Integer, copyLev As Integer
  Dim inLine As String, cleanLine As String, copyname As String
  Dim bolIsCopy As Boolean, IsBatch As Boolean, IsIM As Boolean, IsSM As Boolean
  Dim abt_pgmname As String, abt_pgm_tran_code As String, abt_pgm_map_name As String, abt_hdr As String

  On Error GoTo fileErr
  
 ' ReDim outCopy(0)
  copyLev = 0
  bolIsCopy = False
  IsBatch = False
  IsIM = False
  IsSM = False
  If Mid(GbNomePgm, 4, 2) = "BP" Then
    IsBatch = True
  End If
  If Mid(GbNomePgm, 4, 2) = "IM" Then
    IsIM = True
  End If
  If Mid(GbNomePgm, 4, 2) = "SM" Then
    IsSM = True
  End If
  
  numFile = FreeFile
  outCopy = FreeFile
  Open m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm For Input As numFile
  outFile = FreeFile
  Open m_Fun.FnPathPrj & "\" & migrationPath & "\clean\" & GbNomePgm For Output As outFile
  Do While Not EOF(numFile)
    Line Input #numFile, inLine
    
    
    '---------------------------------------
    ' Batch PROGRAMS - BP
    '---------------------------------------
    
    
    '*   A B N O R M A L   T E R M I N A T I O N   A R E A  *
    
    If InStr(inLine, "01  ABNORMAL-TERMINATION-AREA.") And bolIsCopy = False Then
      If IsBatch Then
        copyname = "ELSCABTA"
      End If
      If IsIM Then
        copyname = "KOLCABTW"
      End If
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
    End If
    ' IN LINKAGE
    '           02 ABNORMAL-TERMINATION-AREA.
    If InStr(inLine, "02 ABNORMAL-TERMINATION-AREA.") And bolIsCopy = False Then
      If IsSM Then
        copyname = "KOLCABTA"
      End If
      If IsIM Then
        copyname = "KOLCABTL"
      End If
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
    End If
    
    If InStr(inLine, "10 ABT-PGM-NAME") And bolIsCopy = True Then
'            10 ABT-PGM-NAME               PIC X(8)  VALUE 'ELSBPTI1'.
'            10 ABT-PGM-TRAN-CODE          PIC X(8)  VALUE 'ELSIMTI1'.
'            10 ABT-PGM-MAP-NAME           PIC X(8)  VALUE 'ELSOTI1'.
'            10 ABT-NEXT-PROGRAM-NAME.
'               15 ABT-NEXT-PROGRAM-NAME-HDR  PIC X(5)  VALUE 'ELSBP'.

      Print #3, "              10 ABT-PGM-NAME               PIC X(8)."
      If InStr(inLine, "'") Then
        abt_pgmname = Mid(inLine, InStr(inLine, "'"), 10)
        Line Input #numFile, inLine
        Print #3, "              10 ABT-PGM-TRAN-CODE          PIC X(8)."
        abt_pgm_tran_code = Mid(inLine, InStr(inLine, "'"), 10)
        Line Input #numFile, inLine
        Print #3, "              10 ABT-PGM-MAP-NAME           PIC X(8)."
        abt_pgm_map_name = Mid(inLine, InStr(inLine, "'"), 9)
        Line Input #numFile, inLine
        Print #3, inLine
        Line Input #numFile, inLine
        Print #3, "                 15 ABT-NEXT-PROGRAM-NAME-HDR  PIC X(5)."
        abt_hdr = Mid(inLine, InStr(inLine, "'"), 7)
        Line Input #numFile, inLine
      End If
      'boliscopy = False
      'Close #3
    End If

    ' 10 FILLER                     PIC X(16) VALUE LOW-VALUES.
    '10 FILLER                     PIC X(1)  VALUE LOW-VALUE.
    If (InStr(inLine, "10 FILLER                     PIC X(16)") Or _
        InStr(inLine, "10 FILLER                     PIC X(1)  VALUE LOW-VALUE")) And _
        bolIsCopy = True Then
      Print #3, inLine
      Line Input #numFile, inLine
      '*        F I E L D   L E N G T H   T A B L E           *
      If InStr(inLine, "*") Then
        Print #outFile, inLine
        Line Input #numFile, inLine
        bolIsCopy = False
        Close #3
      End If
    End If
    
'    '*        F I E L D   L E N G T H   T A B L E           *
'    If InStr(inLine, "*        F I E L D   L E N G T H   T A B L E           *") And boliscopy = True Then
'      'Print #3, inLine
'      '      ********************************************************
'      Print #outFile, "      ********************************************************"
'      Print #outFile, inLine
'      Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
'    If InStr(inLine, "*      T P   I N P U T   S C R E E N   T A B L E       *") And boliscopy = True Then
'      boliscopy = False
'      Close #3
'    End If
    

    '*              B A T C H   W O R K   A R E A
    If InStr(inLine, "01  BATCH-WORK-AREA.") And bolIsCopy = False Then
      copyname = "ELSCBTWA"
      IsBatch = True
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
    End If
    
    
    If InStr(inLine, "01  WORK-AREA-START") And bolIsCopy = True Then
      Print #3, inLine
      Line Input #numFile, inLine
      bolIsCopy = False
      Close #3
     End If
   ' If boliscopy = True Then
   '    Print #3, inLine
   ' End If
    
    '*              D L I   S S A   A R E A                 *
'    If InStr(inLine, "01  SSA-AREA.") And Not IsIM And boliscopy = False Then
'      'CURRENT-PROGRAM-NAME(1:3)CURRENT-PROGRAM-NAME(6:3)TP
'      copyname = Mid(GbNomePgm, 1, 3) & Mid(GbNomePgm, 6, 3) & "SA"
'
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'      'Print #3, inLine
'    End If
'    If InStr(inLine, "*         P R O G R A M   W O R K   A R E A ") And IsBatch And boliscopy = True Then
'      'Print #3, inLine
'      Print #outFile, "      ********************************************************"
'      Print #outFile, inLine
'      boliscopy = False
'      Close #3
'      Line Input #numFile, inLine
'     End If
'    If InStr(inLine, "*      A P P L I C A T I O N   W O R K   A R E A      ") And IsSM And boliscopy = True Then
'      'Print #3, inLine
'      Print #outFile, "      ********************************************************"
'      Print #outFile, inLine
'      boliscopy = False
'      Close #3
'      Line Input #numFile, inLine
'     End If
     
     
    '*-----
    ' *           S Y S T E M   W O R K   A R E A            *
    
     ' 05 CONTROL-VARIABLES.
    If InStr(inLine, "05 CONTROL-VARIABLES.") And IsBatch And bolIsCopy = False Then
      copyname = "ELSCSYCV"
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
    End If
    ' 15 TRACE-FIELD-NAME  ...
    If InStr(inLine, "15 TRACE-FIELD-NAME") And IsBatch And bolIsCopy = True Then
      Print #3, inLine
      Line Input #numFile, inLine
      bolIsCopy = False
      Close #3
    End If
    
    ' 05 ATTRIBUTE-VARIABLES.
'    If InStr(inLine, "05 ATTRIBUTE-VARIABLES.") And IsSM And boliscopy = False Then
'      copyname = "KOLCSYAV"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'    '88 PFK12-24  VALUE 12 24.
'    If InStr(inLine, "88 PFK12-24  VALUE 12 24.") And IsSM And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
    
     
    '05 DLI-FUNCTIONS.
    If InStr(inLine, "05 DLI-FUNCTIONS.") And bolIsCopy = False Then
      copyname = "EGECDLIF"
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
    End If
    '10 LOG-FUNC             PIC X(4) VALUE 'LOG '.
    If InStr(inLine, "10 LOG-FUNC             PIC X(4) VALUE 'LOG '.") And bolIsCopy = True Then
      Print #3, inLine
      Line Input #numFile, inLine
      bolIsCopy = False
      Close #3
    End If
    
    
    '05 DATASET-STATUS-CODES.
    If InStr(inLine, "05 DATASET-STATUS-CODES.") And IsBatch And bolIsCopy = False Then
      copyname = "ELSCDSSC"
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
    End If
    '10 DATASET-UPDTEDENIED
    If InStr(inLine, "10 DATASET-UPDTEDENIED") And IsBatch And bolIsCopy = True Then
      Print #3, inLine
      Line Input #numFile, inLine
      bolIsCopy = False
      Close #3
    End If
    
    
    '*          FILE STATUS CODES                           *
    'c'� DISTINZIONE TRA BATCH E TP
    '
    '05 FILE-STATUS-CODES.
    If InStr(inLine, "05 FILE-STATUS-CODES.") And IsBatch And bolIsCopy = False Then
      copyname = "ELSCFSCD"
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
    End If
    If InStr(inLine, "05 FILE-STATUS-CODES.") And IsBatch = False And bolIsCopy = False Then
      copyname = "KOLCFSCD"
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
    End If
    
    '                                               'DBM'.'
    If InStr(inLine, "                                               'DBM'.") And copyname <> "ELSCABTA" And copyname <> "KOLCABTW" And copyname <> "KOLCABTL" And copyname <> "KOLCABTA" And bolIsCopy = True Then
      Print #3, inLine
      Line Input #numFile, inLine
      bolIsCopy = False
      Close #3
    End If
     
 
 
    '---------------------------------------
    ' TP PROGRAMS - SM e IM
    '---------------------------------------
    
    ' 01  TP-OUTPUT-XFER-BUFFER.
    'identifica che � un IM
'    If InStr(inLine, " 01  TP-OUTPUT-XFER-BUFFER. ") Then
'      IsIM = True
'    End If
    
    
    '*      T P   I N P U T   S C R E E N   T A B L E       *
    '*      T P   I N P U T   S C R E E N   T A B L E       *
    If InStr(inLine, "*      T P   I N P U T   S C R E E N   T A B L E       *") And bolIsCopy = False Then
      Print #outFile, "      *      T P  S C R E E N   T A B L E                    *"
      Print #outFile, "      ********************************************************"
      'CURRENT-PROGRAM-NAME(1:3)  C CURRENT-PROGRAM-NAME(6:3) T
      copyname = Mid(GbNomePgm, 1, 3) & "C" & Mid(GbNomePgm, 6, 3) & "T"
      'IsSM = True
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
    End If
    
'    '*     T P   O U T P U T   S C R E E N   T A B L E      *
'    If InStr(inLine, "*     T P   O U T P U T   S C R E E N   T A B L E      *") And boliscopy = True Then
'      'Print #3, inLine
'      'Line Input #numFile, inLine
'      Print #outFile, "      ********************************************************"
'      boliscopy = False
'      Close #3
'    End If
'    If InStr(inLine, "01  TP-OUTPUT-TABLE.") And boliscopy = False Then
'      'M(PROGRAM-NAME)TPI
'      copyname = "M" & Mid(GbNomePgm, 6, 3) & "TPO"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
    
    '*              D L I   S S A   A R E A                 *
    If InStr(inLine, "*              D L I   S S A   A R E A                 *") And bolIsCopy = True Then
      'Print #3, inLine
      'Line Input #numFile, inLine
      Print #outFile, "      ********************************************************"
      bolIsCopy = False
      Close #3
    End If
    
'    '*        L I N K A G E   S E C T I O N
'    If InStr(inLine, "01  LINK-WORK-AREA.") And boliscopy = False Then
'      If IsSM Then
'        copyname = "SMLNKWA"
'      End If
'      If IsIM Then
'        copyname = "IMLNKWA"
'      End If
'
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
    
    '*        S E G M E N T   I/O   A R E A                 *
'    If InStr(inLine, "S E G M E N T   I/O   A R E A") And IsSM And boliscopy = True Then
'      'Print #3, inLine
'      'Line Input #numFile, inLine
'      '      ********************************************************
'      Print #outFile, "      ********************************************************"
'      boliscopy = False
'      Close #3
'    End If
'    '88 ABT-PGM-IS-STORED
'    If InStr(inLine, "88 ABT-PGM-IS-STORED") And IsIM And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
    
    '*          T P   O U T P U T   B U F F E R             *
    '01  TP-OUTPUT-BUFFER.
    If InStr(inLine, "01  TP-OUTPUT-BUFFER.") And IsSM And bolIsCopy = False Then
      'CURRENT-PROGRAM-NAME(1:3) C CURRENT-PROGRAM-NAME(6:3)O
      copyname = Mid(GbNomePgm, 1, 3) & "C" & Mid(GbNomePgm, 6, 3) & "O"
      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
      Print #outFile, "           COPY " & copyname & "."
      bolIsCopy = True
    End If
    
    '*                D L I   P C B S                       *
    If InStr(inLine, "*                D L I   P C B S                       *") And IsSM And bolIsCopy = True Then
      'Print #3, inLine
      'Line Input #numFile, inLine
      Print #outFile, "      ********************************************************"
      bolIsCopy = False
      Close #3
    End If
    
    '01  IO-PCB.
'    If InStr(inLine, "01  IO-PCB.") And IsSM And boliscopy = False Then
'      'CURRENT-PROGRAM-NAME(1:3)CURRENT-PROGRAM-NAME(6:3)IO
'      copyname = Mid(GbNomePgm, 1, 3) & Mid(GbNomePgm, 6, 3) & "IO"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'    If InStr(inLine, "*        P R O C E D U R E   D I V I S I O N           *") And boliscopy = True Then
'      'Print #3, inLine
'      'Line Input #numFile, inLine
'      Print #outFile, "      ********************************************************"
'      boliscopy = False
'      Close #3
'    End If

    
'    'MAIN-LINE SECTION.
    If InStr(inLine, "  MAIN SECTION.") Or InStr(inLine, "  MAIN SECTION.") And bolIsCopy = False Then
'            10 ABT-PGM-NAME               PIC X(8)  VALUE 'ELSBPTI1'.
'            10 ABT-PGM-TRAN-CODE          PIC X(8)  VALUE 'ELSIMTI1'.
'            10 ABT-PGM-MAP-NAME           PIC X(8)  VALUE 'ELSOTI1'.
'            10 ABT-NEXT-PROGRAM-NAME.
'               15 ABT-NEXT-PROGRAM-NAME-HDR  PIC X(5)  VALUE 'ELSBP'.
      Print #outFile, inLine
      Line Input #numFile, inLine
      If abt_pgmname <> "" Then
        Print #outFile, "DELL-      MOVE " & abt_pgmname & " TO ABT-PGM-NAME"
        Print #outFile, "DELL-      MOVE " & abt_pgm_tran_code & " TO ABT-PGM-TRAN-CODE"
        Print #outFile, "DELL-      MOVE " & abt_pgm_map_name & " TO ABT-PGM-MAP-NAME"
        Print #outFile, "DELL-      MOVE " & abt_hdr & " TO ABT-NEXT-PROGRAM-NAME-HDR"
      End If
    End If
    
'    'MAIN-LINE SECTION.
'    If InStr(inLine, "MAIN-LINE SECTION.") And IsSM And boliscopy = False Then
'      'PMAINL
'      copyname = "PMAINSM"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'    If InStr(inLine, "MAIN-LINE SECTION.") And IsIM And boliscopy = False Then
'      'PMAINL
'      copyname = "PMAINIM"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'
'    ' se � un SM
'    'MAIN-INPUT SECTION.
'    If InStr(inLine, "MAIN-INPUT SECTION.") And IsSM And boliscopy = True Then
'      'Print #3, inLine
'      'Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
'    ' se � un IM
'    'MAIN-INPUT-RETURN.
'    If InStr(inLine, "MAIN-INPUT-RETURN.") And IsIM And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
'
    '
'    'C-100-TERMIO-READ SECTION.
'    If InStr(inLine, "C-100-TERMIO-READ SECTION.") And boliscopy = False Then
'      'CTERMRW
'      copyname = "CTERMRW"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'    'C-100-TERMIO-READ-RETURN.
'    If InStr(inLine, "C-100-TERMIO-READ-RETURN.") And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      Print #3, inLine
'      Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
 
'    'C-400-TERMIO-XFER-MSG-SWITCH SECTION.
'    If InStr(inLine, "C-400-TERMIO-XFER-MSG-SWITCH SECTION.") And boliscopy = False Then
'      'C400TIOX
'      copyname = "C400TIOX"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'    ' C-400-TERMIO-MSG-SWITCH-RETURN.
'    If InStr(inLine, "C-400-TERMIO-MSG-SWITCH-RETURN.") And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      Print #3, inLine
'      Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
    
'    ' C-900-GET-SPA SECTION.
'    If InStr(inLine, "C-900-GET-SPA SECTION.") And boliscopy = False Then
'      'C900GSPA
'      copyname = "C900GSPA"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'    'C-900-GET-SPA-RETURN.
'    If InStr(inLine, "C-900-GET-SPA-RETURN.") And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      Print #3, inLine
'      Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
'
'    'C-910-GET-MESSAGE SECTION.
'    If InStr(inLine, "C-910-GET-MESSAGE SECTION.") And boliscopy = False Then
'      'C910GMSG
'      copyname = "C910GMSG"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'    'C-910-GET-MESSAGE-RETURN.
'    If InStr(inLine, "C-910-GET-MESSAGE-RETURN.") And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      Print #3, inLine
'      Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
    
'    'C-960-PUT-SPA SECTION.
'    If InStr(inLine, "C-960-PUT-SPA SECTION.") And boliscopy = False Then
'      'C960PSPA
'      copyname = "C960PSPA"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'
'    'C-960-PUT-SPA-RETURN.
'    If InStr(inLine, "C-960-PUT-SPA-RETURN.") And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      Print #3, inLine
'      Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
    
'    'C-970-PUT-MESSAGE SECTION.
'    If InStr(inLine, "C-970-PUT-MESSAGE SECTION.") And boliscopy = False Then
'      'C970PMSG
'      copyname = "C970PMSG"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'
'    'C-970-PUT-MESSAGE-RETURN.
'    If InStr(inLine, "C-970-PUT-MESSAGE-RETURN.") And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      Print #3, inLine
'      Line Input #numFile, inLine
'      boliscopy = False
'      Close #3
'    End If
     
    
'    'Z-980-ABNORMAL-TERM SECTION.
'    If InStr(inLine, "Z-980-ABNORMAL-TERM SECTION.") And IsIM And boliscopy = False Then
'      'Z980ABT
'      copyname = "Z980ABIM"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'
'    'Z-980-ABNORMAL-TERM-RETURN.
'    If InStr(inLine, "Z-980-ABNORMAL-TERM-RETURN.") And IsIM And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      Print #3, inLine
'      'Line Input #numFile, inLine
'      If EOF(numFile) Then
'        inLine = ""
'      End If
'      boliscopy = False
'      Close #3
'    End If
'
'    'Z-980-ABNORMAL-TERM SECTION.
'    If InStr(inLine, "Z-980-ABNORMAL-TERM SECTION.") And IsSM And boliscopy = False Then
'      'Z980ABT
'      copyname = "Z980ABSM"
'      Open m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\" & copyname For Output As 3
'      Print #outFile, "           COPY " & copyname & "."
'      boliscopy = True
'    End If
'
'    'Z-980-ABNORMAL-TERM-RETURN.
'    If InStr(inLine, "Z-980-ABNORMAL-TERM-RETURN.") And IsSM And boliscopy = True Then
'      Print #3, inLine
'      Line Input #numFile, inLine
'      Print #3, inLine
'      'Line Input #numFile, inLine
'      If EOF(numFile) Then
'        inLine = ""
'      End If
'      boliscopy = False
'      Close #3
'    End If
'
    '-----
    
    If bolIsCopy = True Then
       Print #3, inLine
    End If
    
    If bolIsCopy = False Then
      Print #outFile, inLine
    End If
    
  Loop
  Close #outFile
  Close #numFile
  
  Exit Sub
fileErr:
  If err.Number = 76 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath & "\copy\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath & "\copy-dell\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath & "\clean\"
    Resume
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
  End If
  Exit Sub
err_catch:
  m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
End Sub

