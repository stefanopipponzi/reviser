Attribute VB_Name = "MapsdM_SAS"
Option Explicit

Const migrationPath = "Output-prj\Languages\SAS2Java"
Const templatePath = "Input-prj\Languages\SAS2Java"
Dim CurrentTag As String
Dim RTB As RichTextBox, RTBDataStep As RichTextBox, RTBQuery As RichTextBox, RTBmacro As RichTextBox
Dim stBatchName As String
Dim StDataStep As String, SrProcName As String, StMacroName As String
Dim BolIsData As Boolean
Dim DataStep As Collection
Dim DataStep1 As Collection
Dim IntDataNum As Integer
Dim IntDataNum1 As Integer
Dim IntDatastep As Integer
Dim IntProcNum As Integer
Dim BolMultiData As Boolean
Dim booEliminaRiga As Boolean
Dim BolOutput As Boolean
Dim BolMerge As Boolean
Dim BolIsFile As Boolean
Dim BolIsMacro As Boolean
''Type DBDataSet
''  'DataSetname As String
''  fieldName() As String
''  fieldType() As String
''End Type
''Dim dataField() As DBDataSet

Dim ColDataSetField As Collection
Dim ColDataField As Collection
Dim ColOutputdataset As Collection
Dim ColWorkDataset As Collection
Dim ColFile As Collection
Dim ColLink As Collection
Dim ColLength As Collection
Dim ColRename As Collection
Dim ColRenameInv As Collection
Dim StinDataset As String
Dim SqlQuery As String
Dim StQuery As String
Dim StDATASETNAME As String
Dim StINPUTDATASET As String
Dim StMultidataStep As String
'''
Dim Countdst As Integer

'T
Public Sub CTLOracle(stdir As String)
  Dim k As Long
  Dim nFJava As Long
  Dim rtbFile As RichTextBox
  
  Dim line As String, stsplit() As String, Name As String, Tipo As String, formato As String, strcampi As String
  Set rtbFile = MapsdF_Parser.rText

  rtbFile.LoadFile m_Fun.FnPathPrj & "\Input-Prj\Template\imsdb\standard\rdbms\ORACLE\CTL_BASE_LOAD.ctl"
    
  changeTag rtbFile, "<TABLE-NAME>", GbNomePgm
  nFJava = FreeFile

  Open GbFileInput For Input As nFJava
  While Not EOF(nFJava)
     Line Input #nFJava, line
     If Trim(line) <> "" Then
       stsplit = Split(line, "|")
      'nel caso non ci siano campi 'VARCHAR' scrive il template
        strcampi = ""
        Name = stsplit(0)
        If stsplit(1) = "A" Then
          Tipo = "CHAR"
          'stsplit(2) � la lunghezza
        ElseIf stsplit(1) = "N" Then
          If Left(stsplit(2), 2) = "DD" Then
            Tipo = "DATE " & """DD/MM/YYYY HH24:MI:SS"""
          Else
            If Left(stsplit(2), 2) = "HH" Then
              Tipo = "DATE " & """DD/MM/YYYY HH24:MI"""
            Else
              ' se in Split(2) c'� un punto
              ' tipo � double e il punto separa parte intera da parte decimale
              Tipo = "INTEGER EXTERNAL"
               ' Split(2) c'� la lunghezza
            End If
          End If
        End If
        strcampi = Name & " " & Tipo & ","
  '      changeTag rtbFile, "<WHEN>", ""
        ''If EOF(nFJava) Then
        strcampi = Replace(strcampi, vbCrLf & "<FIELD-POS(i)>", "")
        changeTag rtbFile, "<FIELD-POS(i)>", strcampi
     End If
     strcampi = ""
  Wend
  changeTag rtbFile, "<FIELD-POS(i)>", ""
  'cleanTags rtbFile
  On Error GoTo errorHandler
  'Salva il file
  'MkDir m_Fun.FnPathPrj & "\Output-Prj\ORACLE\Sql\CTL\" & stdir & "\"
  rtbFile.SaveFile m_Fun.FnPathPrj & "\Output-Prj\ORACLE\Sql\CTL\" & stdir & "\" & GbNomePgm & ".ctl", 1
  
  Exit Sub
errorHandler:
  If err.Number = 75 Then
    MkDir m_Fun.FnPathPrj & "\Output-Prj\ORACLE\Sql\CTL\" & stdir & "\"
    rtbFile.SaveFile m_Fun.FnPathPrj & "\Output-Prj\ORACLE\Sql\CTL\" & stdir & "\" & GbNomePgm & ".ctl", 1
    Resume Next
  Else
   ' m_Fun.writeLog "Errore in [MadmdM_DB2.DeclareCtl_ORA]. Errore : " & err.Description
    MsgBox err.description
  End If

End Sub
Public Sub Param_PGM()
  Dim sttestname As String
  Dim rs As Recordset, rs1 As Recordset
  Dim i As Long
  
  
  Set tagValues = New Collection
  tagValues.Add "", "DATADEFINITIONIO(i)"
  tagValues.Add "", "DATADEFINITIONL(i)"
  Set RTB = MapsdF_Parser.rText
  
  RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\TestSAS"
  sttestname = UCase(GbNomePgm)
  m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath & "\"
  changeTag RTB, "<PGM>", sttestname
  
  
  'SELECT A.SasPgm as PGM, A.stepName AS STEPNAME, A.Param AS PARAM
  'FROM Proc_Sas_Param AS A, PsRel_File AS B
  'Where a.idoggetto = B.idoggetto And a.stepname = B.Step And B.program = "SASESA"
  Set rs = m_Fun.Open_Recordset("SELECT A.SasPgm as PGM, A.stepName AS STEPNAME, A.Param AS PARAM , B.LINK, B.DsnName  FROM Proc_Sas_Param AS A, PsRel_File AS B Where a.idoggetto = B.idoggetto And a.stepname = B.Step And (B.program = 'SASESA' or B.program = 'SAS') AND  A.SasPgm  = '" & sttestname & "'")
  If rs.RecordCount Then
    changeTag RTB, "<PARAM>", rs!param
    While Not rs.EOF
      Set rs1 = m_Fun.Open_Recordset("SELECT * FROM InputFiles where SasProgram = '" & sttestname & "'  and DDName = '" & rs!LINK & "'")
      If rs1.RecordCount Then
        changeTag RTB, "<<DATADEFINITIONIO(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\DATADEFINITIONIO"
        changeTag RTB, "<DDNAME>", rs!LINK
        changeTag RTB, "FILENAME", rs1!fileName
        If IsNull(rs1!LRECL) Then
          changeTag RTB, "LRECL", 0
        Else
          changeTag RTB, "LRECL", rs1!LRECL
        End If
        changeTag RTB, "RECFM", rs1!RECFM
      Else
        changeTag RTB, "<<DATADEFINITIONL(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\DATADEFINITIONL"
        changeTag RTB, "<DDNAME>", rs!LINK
        changeTag RTB, "FILENAME", rs!dsnName
      End If
      rs1.Close
      rs.MoveNext
    Wend
    changeTag RTB, "<<DATADEFINITIONIO(i)>>", ""
    changeTag RTB, "<<DATADEFINITIONL(i)>>", ""
  End If
  rs.Close
  RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\Test" & sttestname & ".java", 1
End Sub


Public Sub convertPGM()
  Dim previousTag As String
  Dim newriga As String
  Dim i As Long
  Set tagValues = New Collection
  
  tagValues.Add "", "addSasStep(i)"
  tagValues.Add "", "BatchNameMain(i)"
  tagValues.Add "", "DataSet(i)"
  tagValues.Add "", "inputDataset(i)"
  tagValues.Add "", "importClassDataset(i)"
  tagValues.Add "", "OutputData(i)"
  tagValues.Add "", "OutputDataset(i)"
  tagValues.Add "", "process(i)"
  tagValues.Add "", "linkprocess(i)"
  tagValues.Add "", "macroprocess(i)"
  tagValues.Add "", "ProcStep(i)"
  tagValues.Add "", "ProcStepName(i)"
  tagValues.Add "", "SasStep(i)"
  tagValues.Add "", "SASVARIABLE(i)"
  tagValues.Add "", "INPUTOPTION(i)"
  tagValues.Add "", "COMMENT(i)"
  tagValues.Add "", "WHERECOND(i)"
  tagValues.Add "", "procsort(i)"
  tagValues.Add "", "contents(i)"
  
  Set RTB = MapsdF_Parser.rText

  RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\BatchNameMain"
  CurrentTag = "COMMENT(i)"
  GbNomePgm = LCase(GbNomePgm)
  m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm
  
  stBatchName = UCase(Chr(Asc(GbNomePgm))) & Right(GbNomePgm, Len(GbNomePgm) - 1)
  changeTag RTB, "<MBatchName>", stBatchName
  changeTag RTB, "<BatchName>", UCase(stBatchName)
  
  Countdst = 0
  
  parsSAS

  writeTag "COMMENT(i)", RTB
  writeTag "importClassDataset(i)", RTB
  writeTag "SasStep(i)", RTB
  writeTag "ProcStepName(i)", RTB
  writeTag "addSasStep(i)", RTB
  writeTag "importClassProc(i)", RTB
  writeTag "addProcStep(i)", RTB
  
  cleanTags RTB
  cleanTags RTBQuery

  On Error GoTo saveErr
  RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & UCase(stBatchName) & ".java", 1
  RTBQuery.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm & "\" & stBatchName & "_Query.xml", 1
 Exit Sub
saveErr:
  If err.Number = 75 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath
    Resume
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
  End If
End Sub

Public Sub parserSAS()
  Dim nFCbl As Long, i As Long, j As Long, k As Long
  Dim wRec As String, statement As String
  Dim token As String, line As String
  Dim manageMove As String  'Fare globale Parser
  Dim multilinea As Integer, label As Boolean, IsSubstr As Boolean, pStart As String
  ' Mauro 17/04/2008
  Dim is_IF As Boolean
  Dim strLabel As String
  On Error GoTo parseErr
  
  'Non leggiamo tutte le volte!
  manageMove = m_Fun.LeggiParam("IMSDC-MOVEYN") = "YES"
  
  resetRepository
  
  'Init flag per riconoscimento tipo programma... (flag da sistemare - perlomeno rinominare...)
  SwTpCX = False
  SwTpIMS = False
  SwDli = False
  SwSql = False
  SwBatch = False
  SwVsam = False
  SwMapping = False
  
  'INIT:
  booEliminaRiga = False
  GbNumRec = 0
  ReDim entries(100)
  ReDim CmpIstruzione(0)
  ReDim CampiMove(0)
  ReDim CampiMoveCpy(0)
  'SQ 15-11-06
  ReDim DataValues(0)
  Idx = 0
  SwNomeCall = True
  ReDim arrFiles(0)
  'SQ [01]
  procName = ""
  
  ' Mauro 23/07/2008 : E se il file non c'�? Non usciva mai!!!
  If Len(Dir(GbFileInput)) = 0 Then
    MsgBox "This file is not avaliable...", vbExclamation, Parser.PsNomeProdotto
    Exit Sub
  End If

  nFCbl = FreeFile
  Open GbFileInput For Input As nFCbl
  While Not EOF(nFCbl)
    'SQ 10-02-06 (potenziale loop - es.: non e' un PLI!)
    While Len(line) = 0 And Not EOF(nFCbl)
      Line Input #nFCbl, line
      GbNumRec = GbNumRec + 1
      RipulisciRiga_SAS line
    Wend
    'SQ 21-11-06 - GESTIONE DIRETTIVE:
    'BBV "-INC PSTAMP"... chiarire se sono direttive,
    'comunque non terminano col ";"!
    'TMP: FARE routine dedicata
    'AC 10/12/2007 gestito EXEC dentro IF,ELSE,WHEN come per CALL
    token = ""
    statement = ""
''    Do While Not EOF(nFCbl)
''      statement = line
''      'SILVIA PROVIAMO CON IL nexttoten_newnew per tenere l'apice singolo
''      ''token = nextToken(statement)
''      token = nextToken_newnew(statement)
''      If token = "INC" Or Len(line) = 0 Or _
''         (token = "IF" And Not InStr(line, " CALL ") > 0 And Not InStr(line, " EXEC ") > 0) Or _
''         (token = "ELSE" And Not InStr(line, " CALL ") > 0 And Not InStr(line, " EXEC ") > 0 And Not InStr(line, " = ") > 0) Or _
''         (token = "THEN" And Not InStr(line, " CALL ") > 0 And Not InStr(line, " EXEC ") > 0) Or _
''         (token = "WHEN" And Not InStr(line, " CALL ") > 0 And Not InStr(line, " EXEC ") > 0 And Not InStr(line, " = ") > 0) Or _
''         (token = "OTHERWISE" And Not InStr(line, " CALL ") > 0 And Not InStr(line, " EXEC ") > 0 And Not InStr(line, " = ") > 0) Then
''        Line Input #nFCbl, line
''        GbNumRec = GbNumRec + 1
''        RipulisciRiga_SAS line
''      ElseIf token = "IF" Or token = "ELSE" Or token = "THEN" Or token = "WHEN" Or token = "OTHERWISE" Then
''        ' sotto considerando i casi in cui call sulla stessa linea di questi identificatori
''        RipulisciRiga_SAS line
''        If InStr(line, " CALL ") > 0 Then
''          line = Right(line, Len(line) - InStr(line, " CALL "))
''        ElseIf InStr(line, " EXEC ") > 0 Then
''          line = Right(line, Len(line) - InStr(line, " EXEC "))
''        ' Mauro 19/10/2009 : Prendo i valori della MOVE dopo la parola riservata
''        ElseIf InStr(line, " = ") > 0 And (token = "ELSE" Or token = "WHEN" Or token = "OTHERWISE") Then
''          Dim firstSpace As Integer
''          firstSpace = InStrRev(line, " ", InStr(line, " = ") - 2)
''          line = Trim(Right(line, Len(line) - firstSpace))
''        Else
''          Line Input #nFCbl, line
''          GbNumRec = GbNumRec + 1
''          RipulisciRiga_SAS line
''        End If
''      Else
''        Exit Do
''      End If
''    Loop
      
    statement = line
    GbOldNumRec = GbNumRec  'linea iniziale statement
    If label Then
      GbOldNumRec = GbOldNumRec - multilinea
    End If
        
    'stefano: caso di riga di commenti, quindi tutta pulita?????
    If line = "" Then
    'SQ 14-11-06: LABEL: "CICCIO:" ==> NON ho il ";"!
    ElseIf Right(line, 1) = ":" And InStr(Trim(Mid(line, 1, Len(line) - 1)), " ") = 0 Then
      'LABEL!
      'nop
    Else
      'spli
      'Do While Right(line, 1) <> ";" And Not EOF(nFCbl) And j = 1
      j = 0
      i = 1
      Do
        i = InStr(i, line, "'")
        If i = 0 Then Exit Do
        i = i + 1
        j = Switch(j = 0, 1, j = 1, 0)
      Loop
      multilinea = 0
      Do While (Right(line, 1) <> ";" Or j = 1) And Not EOF(nFCbl)
        Line Input #nFCbl, line
        GbNumRec = GbNumRec + 1
        multilinea = multilinea + 1
        RipulisciRiga_SAS line
        ' Mauro 22/04/2008
        If Right(line, 4) = "THEN" Or InStr(line, "THEN ") Then
          GbOldNumRec = GbNumRec
          'statement = LTrim(Mid(line, 5))
          statement = LTrim(Mid(line, InStr(line, "THEN") + 4))
        Else
          If statement = "" Then
            GbOldNumRec = GbNumRec
          End If
          statement = statement & " " & line
        End If
        
        i = 1
        Do
          i = InStr(i, line, "'")
          If i = 0 Then Exit Do
          i = i + 1
          j = Switch(j = 0, 1, j = 1, 0)
        Loop
      Loop
      'Pulizia ";"
      'split: non � colpa mia, doppio ";" in una decina di include...
      'If Right(statement, 1) = ";" Then statement = Left(statement, Len(statement) - 1)
      While Right(statement, 1) = ";"
        statement = RTrim(Left(statement, Len(statement) - 1))
      Wend
      'SILVIA PROVIAMO CON IL nexttoten_newnew per tenere l'apice singolo
      'token = nextToken(statement)
      token = nextToken_newnew(statement)
    End If
    'SQ - 14-11-06:
    'TMP: COLONNA 1: COS'E'?
    'If token = "0" Or token = "1" Or token = "-" Then
    '  token = nextToken(statement)
    'End If
    label = False
    
    Select Case token
      Case "%"
        token = nexttoken(statement)
        If token = "INCLUDE" Then
          getInclude statement
        Else
          '...
        End If
        line = ""
      Case "%INCLUDE"
        getInclude statement
        'ATTENZIONE A CASI CON type(...)... vedi sintassi IBM
        line = ""
      Case "FILE", "INFILE"
        'getFILE
        line = ""
      ' TEMP!!!!!!!!!!!!!!!!!! Tutti da gestire, prima o poi
      Case Else
        ' "OPTIONS", "IF", "THEN", "ELSE", "INPUT", "OUTPUT", "END", "DATA", "PROC", "%MACRO", "RETURN", _
        ' "MERGE", "RETAIN", "CALL", "LENGTH", "SET", "PUT", "LINK", "DO", "SUBSTR"
        line = ""
    End Select
  Wend
  Close #nFCbl
  
  'DATA:
  updateData
    
  Exit Sub
parseErr:    'Gestito a livello superiore...
  If err.Number = 123 Then
    'QUESTO E' PER IL COBOL. SISTEMARE...
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #nFCbl, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'gestire
      'GbErrLevel = "P-0"
      GbOldNumRec = GbNumRec
      addSegnalazione line, "P00", line
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "parserSAS: id#" & GbIdOggetto & ", ###" & err.description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
    m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
  Else
    addSegnalazione line, "P00", line
    Resume Next
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''
' Delete tabelle allestite dal parse (I)
''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub resetRepository()
  On Error GoTo dbErr
  
  MapsdM_Parser.deleteDataRelations
  
  Exit Sub
dbErr:
  m_Fun.Show_MsgBoxError "RC0002", , "resetRepository", err.source, err.description, False
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
' &INCLUDE name | dataset(membro)
''
' ATTENZIONE:
' se il membro era stato riconosciuto SAS, viene riclassificato...
' -Segnalazione NOCOPY... SPECIFICARNE UNA SAS!!!!!!!!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getInclude(statement As String) As String
  Dim includeName As String
  'silvia 26-05-2008
  Dim idIncludeID As Long
  Dim parAperta As Integer, parChiusa As Integer
  
  statement = Replace(statement, "SASMAC", "")
  statement = Replace(statement, "SOURCLIB", "")
  includeName = nexttoken(statement)
  If InStr(includeName, ")") Then
    If InStr(includeName, ".") Then
      parAperta = InStr(includeName, "(")
      parChiusa = InStr(parAperta, includeName, ")")
      includeName = Trim(Mid(includeName, parAperta + 1, parChiusa - parAperta - 1))
    Else
      includeName = Trim(Replace(Replace(includeName, "(", ""), ")", ""))
    End If
  End If
  
  If Right(includeName, 1) = ";" Then includeName = Left(includeName, Len(includeName) - 1)
  'SQ 2-11-06
  MapsdM_Parser.checkRelations includeName, "SAS-INC"
  'SQ [01]
  'TMP:
  Parser.PsConnection.Execute "INSERT INTO PsPgm_Copy (IdPgm,Riga,COPY,ReplacingVar) VALUES (" & GbIdOggetto & "," & GbOldNumRec & ",'" & includeName & "','" & procName & "')"

  '''''''
  'silvia 26-05-2008
  idIncludeID = getIncludeID(includeName)
  If idIncludeID <> 0 Then
    DataItem(Idx).idCopy = idIncludeID
    DataItem(Idx).DatDigit = getCopyDigit(idIncludeID)
    DataItem(Idx).DatString = "A"
  Else
    DataItem(Idx).idCopy = 0
  End If
  '''''
End Function

'silvia 26-05-2008
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ritorna l'ID della INCLUDE se "interna" (non livello 01);
' Ritorna 0 altrimenti
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getIncludeID(copyname As String) As Long
  Dim r As Recordset
  Dim idOggetto As Long
  
  Set r = Open_Recordset("Select idOggetto From BS_Oggetti Where " & _
                         "Nome = '" & Replace(copyname, "'", "") & "' And Tipo = 'SAS'")
  If r.RecordCount Then
    idOggetto = r!idOggetto
    Set r = Open_Recordset("Select idArea,ordinale,livello From PsData_Cmp Where " & _
                           "idOggetto = " & idOggetto & " ORDER BY idArea,ordinale")
    If r.RecordCount Then
      'Livello 01? (copy "esterna"?)
      If r!livello = 1 Then idOggetto = 0
    Else
      'Parser.PsFinestra.ListItems.Add , , "There is no parser of INCLUDE : " & copyname & " - ID: " & idOggetto
      idOggetto = 0
    End If
  End If
  r.Close
  getIncludeID = idOggetto
End Function

Sub parsSAS()
  Dim nFJava As Long, i As Long, j As Long, k As Long
  Dim wRec As String, statement As String
  Dim token As String, line As String
  Dim previousTag As String
  Dim nextTkn As String, strLabel As String, stkeepSql As String, numFileTXT As Long
  Dim label As String, multilinea As Integer
  Dim stappo As String, stoption As String, strename As String

  On Error GoTo errorHandler
  BolIsData = False
  BolMultiData = False
  BolOutput = False
  BolMerge = False
  BolIsFile = False
  BolIsMacro = False
  IntDataNum = 0
  IntDataNum1 = 0
  IntDatastep = 0
  IntProcNum = 0
  Set ColRename = New Collection
  Set ColRenameInv = New Collection
  Set DataStep = New Collection
  Set DataStep1 = New Collection
  Set ColOutputdataset = New Collection
  Set ColLink = New Collection
  Set ColLength = New Collection
  Set ColFile = New Collection
  
  'file xml query
  Set RTBQuery = MapsdF_Parser.rText1
  RTBQuery.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\namedQueries"
  If Len(Dir(GbFileInput)) = 0 Then
    MsgBox "This file is not avaliable...", vbExclamation, Parser.PsNomeProdotto
    Exit Sub
  End If

  nFJava = FreeFile
  Open GbFileInput For Input As nFJava
  While Not EOF(nFJava)
  
    While Len(Trim(line)) = 0 And Not EOF(nFJava)
      Line Input #nFJava, line
      GbNumRec = GbNumRec + 1
      RipulisciRiga_SAS line
    Wend
    statement = line

    GbOldNumRec = GbNumRec  'linea iniziale statement
    If line = "" Then
      token = ""
    Else
      multilinea = 0
        Do While (Right(Trim(line), 1) <> ";") And Not EOF(nFJava)
          Line Input #nFJava, line
          GbNumRec = GbNumRec + 1
          multilinea = multilinea + 1
          RipulisciRiga_SAS line
          If statement = "" Then
             GbOldNumRec = GbNumRec
          End If
          statement = statement & " " & line
        Loop
      While Right(statement, 1) = ";"
        statement = RTrim(Left(statement, Len(statement) - 1))
      Wend
      If InStr(statement, ";") Then
        statement = Left(statement, InStr(statement, ";") - 1)
        line = Mid(statement, InStr(statement, ";") + 1) & ";"
      End If
      statement = Replace(statement, "^=", " ^= ")
      statement = Replace(statement, "=", " = ")
      statement = Replace(statement, "-", " - ")
      statement = Replace(statement, "+", " + ")
      statement = Replace(statement, "> =", " >=")
      statement = Replace(statement, "< =", " <=")
      'statement = Replace(statement, "*", " * ")
      token = nextToken_newnew(statement)
    End If
    'token = Replace(token, "%", "")
    token = UCase(token)
    If token <> "PUT" Then
      statement = UCase(statement)
    End If
    Select Case token
      Case "OPTIONS"
        tagWrite "//# " & token & " " & statement
        line = ""
      Case "DATA"
        Set ColWorkDataset = New Collection
        BolOutput = False
        If BolIsData Then
          If BolOutput = False And InStr(StDATASETNAME, "DNULL") = 0 Then
            For i = 1 To DataStep1.count
             parseOutput DataStep1.item(i)
            Next
          End If
          Run
        End If
        Set ColOutputdataset = New Collection
        '''If BolIsData Then Run
        BolIsData = True
        previousTag = CurrentTag
        Set ColDataField = New Collection
        Set ColDataSetField = New Collection
        StinDataset = ""
        If CurrentTag = "macroprocess(i)" Then
          tagWrite "<<addSasStep(i)>>"
          previousTag = CurrentTag
          CurrentTag = "process(i)"
        End If
        parseDATAstep statement
        
        If statement <> "" Then
          'line = "DATA " & statement & ";"
          Do While statement <> ""
            CurrentTag = previousTag
            parseDATAstep statement
          Loop
          CurrentTag = "process(i)"
          line = ""
        Else
          CurrentTag = "process(i)"
          line = ""
        End If
        'CurrentTag = previoustag
      Case "SET", "INFILE"
        If token = "INFILE" Then
          BolIsFile = True
        Else
          BolIsFile = False
        End If
        parseSET statement
        If statement <> "" Then
          token = nexttoken(statement)
          If token = "KEY" Or token = "END" Then
            tagWrite "// " & token & " " & statement
            token = nexttoken(statement)
            token = nexttoken(statement)
            token = nexttoken(statement)
            line = statement
          Else
            line = "SET " & token & " " & statement & ";"
          End If
        Else
''          Print #numFileTXT, StQuery
''          Close numFileTXT
          line = ""
        End If
      
      Case "MODIFY"
        Line Input #nFJava, line
        GbNumRec = GbNumRec + 1
        RipulisciRiga_SAS line
        If InStr(line, "BY ") Then
          statement = statement & " " & Replace(line, ";", "")
          statement = UCase(statement)
          line = ""
        Else
          statement = UCase(statement)
          tagWrite "// " & token & " " & statement
          parseSET statement
        End If
        'tagWrite "// " & token & " " & statement
        'parseSET statement

      Case "KEEP"
        stkeepSql = ""
       '' If StinDataset <> "" Then
          ColOutputdataset.Remove (StDATASETNAME)
          ColOutputdataset.Add "KEEP: " & statement, StDATASETNAME
          stappo = statement
          Do While stappo <> "" And Left(stappo, 1) <> "("
            token = nextToken_new(stappo)
            If token = "-" Then
              token = nextToken_new(stappo)
              j = 2
              k = 1
              If IsNumeric(Right(token, 2)) Then
                k = 2
              End If
              If IsNumeric(Right(token, 3)) Then
                k = 3
              End If
              Do While token <> Left(token, Len(token) - k) & j
                stoption = Left(token, Len(token) - k) & j & " " & stoption
                j = j + 1
              Loop
            End If
          Loop
          statement = Replace(statement, "-", stoption)
          stoption = ""
          statement = Replace(statement, " ", vbCrLf)
          For i = 0 To UBound(Split(statement, vbCrLf))
            If Trim(Split(statement, vbCrLf)(i)) <> "" Then
                stkeepSql = stkeepSql & Trim(Split(statement, vbCrLf)(i)) & " , "
            End If
          Next
          stkeepSql = Trim(stkeepSql)
          stkeepSql = Left(stkeepSql, Len(stkeepSql) - 1)
          StQuery = Replace(StQuery, "*", stkeepSql)
          line = ""
      ''  End If
      Case "MERGE"
         BolMerge = True
         parseMerge (statement)
         Line Input #nFJava, line
         StQuery = "JOIN : " & statement & " " & Replace(line, ";", "")
         line = ""
      Case "DROP"
         If BolMerge Then
           StQuery = StQuery & vbCrLf & token & ": " & statement
         Else
           Dim stdrop As String
           ''If BolIsFile Then
             tagWrite "// DROP " & statement
             If StDATASETNAME <> "" Or BolIsFile Then
                ColOutputdataset.Remove (StDATASETNAME)
                ColOutputdataset.Add "DROP: " & statement, StDATASETNAME
                BolOutput = False
               'line = ""
            End If
           ''End If
         End If
         line = ""
      Case "RETAIN", "LENGTH"
        previousTag = CurrentTag
        CurrentTag = "SASVARIABLE(i)"
        parseSASvariable token & " " & statement
''        tagWrite "// RETAIN: "
''        tagWrite "// TODO: check variable type"
''        statement = Replace(statement, " ", vbCrLf)
''        For i = 0 To UBound(Split(statement, vbCrLf))
''          If Trim(Split(statement, vbCrLf)(i)) <> "" Then
''            tagWrite "private String " & (Split(statement, vbCrLf)(i)) & ";"
''          End If
''        Next
        CurrentTag = previousTag
        line = ""
      Case "INPUT"
        parseInput statement
        line = ""
        'statement = Replace(statement, "@", vbCrLf & "// ")
      Case "PUT"
        'CurrentTag = "process(i)"
        If InStr(statement, "/") Then
          'statement = Replace(statement, "/ ", ";" & vbCrLf & " PUT ")
          'line = "PUT " & statement
          For k = 0 To UBound(Split(statement, "/"))
            parsePUT (Split(statement, "/")(k))
          Next
        Else
          If Left(statement, "}") Then
            statement = Replace(statement, "}", "")
            parsePUT (statement)
            tagWrite "}"
          Else
            parsePUT (statement)
          End If
          ''statement = Replace(statement, "@", vbCrLf & "// ")
          '''tagWrite "//# " & token & " " & statement & ";"
        End If
        line = ""
      Case "LABEL"
        tagWrite "//# " & token & " " & statement & ";"
        line = ""
      Case "ELSE"
        'CurrentTag = "process(i)"
        If UCase(statement) = "DO" Then
           line = ""
        Else
           line = statement & " } ;"
        End If
        tagWrite "else " & vbCrLf & " {"
      Case "IF"
        'CurrentTag = "process(i)"
        Dim stIf As String
        stIf = Left(statement, InStr(UCase(statement), "THEN") + 4)
        parseIF stIf
        line = Mid(statement, InStr(UCase(statement), "THEN") + 5)
        If Trim(UCase(line)) = "DO" Then
          line = ""
        Else
          line = line & " } ;"
        End If
      Case "END"
        'CurrentTag = "process(i)"
        tagWrite "  }"
        line = ""
      Case "SELECT"
        'CurrentTag = "process(i)"
        Dim appo As String
        On Error GoTo catch
        appo = ColDataField.item((Replace(Replace(statement, "(", ""), ")", "")))
        If appo = "" Then
          appo = ColDataSetField.item((Replace(Replace(statement, "(", ""), ")", "")))
          If appo <> "" Then
            tagWrite "private " & Split(appo, " - ")(1) & " " & Split(appo, " - ")(0) & ";", "SASVARIABLE(i)"
            ' STATUS= Integer.parseInt(inputDataRow.get("STATUS").toString());
             'MKR_IN7 = Double.valueOf(inputDataRow.get("MKR_IN7").toString()) ;
             tagWrite Split(appo, " - ")(0) & "= (" & Split(appo, " - ")(1) & ") inputDataRow.get(""" & Split(appo, " - ")(0) & """);"
            'tagWrite Replace(Replace(statement, "(", ""), ")", "") & " = Integer.parseInt(inputDataRow.get(""" & Replace(Replace(statement, "(", ""), ")", "") & """).toString());"
          End If
        End If
        tagWrite "switch " & statement & "{" & vbCrLf
        line = ""
      Case "WHEN"
        'CurrentTag = "process(i)"
        token = nexttoken(statement)
        tagWrite "case " & Replace(Replace(token, "(", ""), ")", "") & ":"
        line = statement & " " & vbCrLf & "break;"
      Case "OTHERWISE"
        tagWrite "default: "
        line = statement & ";"
      Case "ARRAY"
        previousTag = CurrentTag
        CurrentTag = "SASVARIABLE(i)"
        parseSASvariable token & " " & statement
        ''tagWrite "//# " & token & " " & statement & ";"
        line = ""
        CurrentTag = previousTag
      Case "DO"
        previousTag = CurrentTag
        'CurrentTag = "process(i)"
        If InStr(statement, "}") Then
          tagWrite statement
        Else
          parseDO statement
        End If
        line = ""
      Case "RETURN"
        If CurrentTag = "process(i)" Then
           tagWrite "return 1;"
        Else
           tagWrite "return;"
           writeTag CurrentTag, RTBDataStep
           'changeTag RTBDataStep, "linkprocess(i)", ""
           CurrentTag = "process(i)"
        End If
        line = ""
      Case "DELETE"
        tagWrite "// DELETE "
        If CurrentTag = "process(i)" Then tagWrite "return 1;"
        line = ""
      Case "RENAME"
        On Error GoTo catch
        If CurrentTag = "process(i)" Then
          Do While Len(statement)
            stappo = nexttoken(statement)
            token = nexttoken(statement)
            strename = nexttoken(statement)
            ColRename.Add strename, stappo
            ColRenameInv.Add stappo, strename
          Loop
        Else
          tagWrite "// " & token & statement
        End If
        line = ""
      Case "RUN"
         If BolIsData Then
           If BolOutput = False And InStr(StDATASETNAME, "DNULL") = 0 Then
             For i = 1 To DataStep1.count
              parseOutput DataStep1.item(i)
             Next
           End If
           Run
           Set ColDataField = New Collection
           Set ColDataSetField = New Collection
           StDataStep = ""
           StMultidataStep = ""
         Else
           If CurrentTag = "procsort(i)" Then
             writeTag "procsort(i)", RTB
           Else
             changeTag RTBDataStep, "<ProcStep>", SrProcName
             If CurrentTag = "contents(i)" Then
               writeTag "contents(i)", RTBDataStep
               cleanTags RTBDataStep
               RTBDataStep.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm & "\" & SrProcName & ".java", 1
             Else
               writeTag "ProcStep(i)", RTBDataStep
               cleanTags RTBDataStep
               RTBDataStep.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm & "\Proc" & SrProcName & ".java", 1
             End If
           End If
           CurrentTag = previousTag
         End If
         line = ""
         BolIsFile = False
         Set ColOutputdataset = New Collection
         Set ColWorkDataset = New Collection
         Set DataStep1 = New Collection
      '''
      Case "PROC"
        BolIsData = False
        previousTag = CurrentTag
        '''Set ColWorkDataset = New Collection
        parsePROC statement
        line = ""
      Case "CLASS", "VAR"
        If BolIsData = False Then
          Do While statement <> ""
            token = nexttoken(statement)
            On Error GoTo catch
            ColWorkDataset.Add ColDataSetField.item(token), token
          Loop
        End If
        line = ""
      Case "TABLES"
        tagWrite "// " & token & statement
        parseOutputProc statement
        line = ""
      '''
      Case "CALL"
        nextTkn = nextToken_newnew(statement)
        If UCase(nextTkn) = "SYMPUT" Then
          tagWrite "// SYMPUT "
          statement = Mid(statement, 2, Len(statement) - 2)
          nextTkn = nextToken_newnew(statement)
          nextTkn = Replace(nextTkn, "'", """")
          '' SasCtxManager.setParam("CUR_DT",CUR_DT)
          '' SasCtxManager.setParameter("M_TODAY",LEFT(PUT(TODAYIS , 6.)))
          If InStr(statement, "(") Then
            tagWrite "SasCtxManager.setParam(" & nextTkn & "," & statement & ");"
          Else
            tagWrite "SasCtxManager.setParam(" & nextTkn & "," & statement & ");"
          End If
          line = ""
        End If
      Case "OUTPUT"
        On Error GoTo catch
        If BolIsData Then
           BolOutput = True
           appo = DataStep1.item("WORK." & Trim(Replace(statement, "WORK.", "")))
           If appo <> "" Then
             parseOutput appo
           Else
             For i = 1 To DataStep1.count
              parseOutput DataStep1.item(i)
             Next
           End If
           ''BolOutput = False
        Else
          parseOutputProc statement
          tagWrite "//# " & token & " " & statement
        End If
        line = ""
      Case "LINK", "GOTO"
        token = nexttoken(statement)
         tagWrite LCase(token & "(); " & statement)
         On Error GoTo catch
         ColLink.Add token, token
         line = ""
      Case "%MACRO"
         BolIsMacro = True
         parseMACRO statement
         line = ""
      Case "%DO", "%IF"
         CurrentTag = "macroprocess(i)"
         parseDO Replace(statement, "%", "")
         line = ""
      Case "%END"
         CurrentTag = "macroprocess(i)"
         tagWrite "  }" & vbCrLf
         line = ""
      Case "%MEND"
         BolIsMacro = False
         writeTag "macroprocess(i)", RTBmacro
         changeTag RTBmacro, "<macroprocess(i)>", ""
         'cleanTags RTBmacro
         RTBmacro.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm & "\" & "Macro" & StMacroName & ".java", 1

         CurrentTag = "process(i)"
         line = ""
      Case "FILE"
         ''String ayint=(String)SasCtxManager.getParam("ayint");
         tagWrite "//# " & token & " " & statement
         token = nexttoken(statement)
         appo = token
         tagWrite "vp=vp" & token & ";"
         ColFile.Add appo, appo
         If appo <> "" Then
           tagWrite "String " & token & "=(String)SasCtxManager.getParam(""" & token & """);", "OutputData(i)"
           tagWrite "VirtualDash vp" & token & "=new VirtualDash();", "OutputData(i)"
         ' vp.write(new PrintStream(new FileOutputStream(ayint)));
           changeTag RTBDataStep, "<<FILE(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\File"
           changeTag RTBDataStep, "<filename>", token
         End If
         line = ""
      
      Case Else
        If InStr(token, ":") Then
          writeTag "linkprocess(i)", RTBDataStep
          parseLINK (Replace(token, ":", ""))
          token = nextToken_newnew(statement)
        End If
        nextTkn = nextToken_newnew(statement)
          If nextTkn = "=" Then
          nextTkn = nextToken_newnew(statement)
          getnextTkn token, nextTkn, statement
        ElseIf InStr(statement, "=") Then
          getnextTkn token, nextTkn, statement
        ElseIf Right(token, 1) = ":" And statement = "" Then
            'previousTag = CurrentTag
            writeTag "linkprocess(i)", RTBDataStep
            parseLINK (Replace(token, ":", ""))
          ElseIf Left(token, 1) = "%" Then
             tagWrite Replace(token, "%", "") & IIf(Len(statement), statement, "()") & ";", CurrentTag
             CurrentTag = "process(i)"
          Else
             tagWrite "//# " & token & " " & nextTkn & " " & statement & ";"
          End If
        line = ""
    End Select
  Wend
  Close #nFJava
  
  Exit Sub
errorHandler:
  ''writeLog "E", "Unexpected Error: " & err.Description
    m_Fun.writeLog "Conversion Sas to java: id#" & GbIdOggetto & ", ###" & err.description
catch:
  appo = ""
  Resume Next
End Sub
Private Sub getnextTkn(token As String, nextTkn As String, statement As String)
  Dim stitemcol As String, sttoken As String, stwrite As String, sttipo As String, stparentesi As String, stgraffa As String
  On Error GoTo catch
  stwrite = ""
  stitemcol = ColDataField.item(token)
  If stitemcol = "" Then
    stitemcol = ColDataSetField.item(token)
    If stitemcol <> "" Then
      ColDataField.Add stitemcol, token
    End If
  End If
  If stitemcol = "" Then
    addOutputData token, nextTkn & statement
  End If
  If InStr(stitemcol, "[]") Then
    nextTkn = Replace(Replace(nextTkn, "(", "["), ")", "]")
  End If
  If statement = "" Then
    statement = nextTkn
    statement = Replace(statement, "*", " * ")
    statement = Replace(statement, "+", " + ")
    statement = Replace(statement, "-", " - ")
    statement = Replace(statement, "/", " / ")
    nextTkn = nextToken_newnew(statement)
    If statement = "" Then
      stitemcol = ColDataSetField.item(nextTkn)
      If stitemcol <> "" Then
        'MKR_IN7 = Double.valueOf(inputDataRow.get("MKR_IN7").toString()) ;
        tagWrite nextTkn & "= (" & Split(stitemcol, " - ")(1) & ") inputDataRow.get(""" & nextTkn & """);"
        'token = token & " = " & "inputDataRow.get(""" & nextTkn & """)"
''         ColDataField.Add stitemcol, nextTkn
''         tagWrite nextTkn & " = " & "inputDataRow.get(""" & nextTkn & """)"
      End If
      'Else
      token = token & " = " & nextTkn
      'End If
    Else
      token = token & " = "
    End If
     'stwrite = token & " = " & nextTkn & ";"
  Else
    token = IIf(InStr(statement, "="), token, token & " = ")
  End If
  
  Do While statement <> "" And nextTkn <> ""
    Select Case UCase(nextTkn)
    Case "DATE"
      '' Date CURRDTE= SasDateHelper.getSasToday() ;
      token = "Date " & token & " SasDateHelper.getSasToday() "
      statement = ""
    Case "YEAR"
       'YY=YEAR(CURRDTE);
      '' Date YY= SasDateHelper.getYear(Date CURRDTE) ;
''      token = "Date " & token & " SasDateHelper.getYear(Date "
      'stwrite = "Date " & token & "= SasDateHelper.getYear(Date "
      sttoken = nextToken_newnew(statement)
      sttoken = Replace(Replace(sttoken, "(", ""), ")", "")
      stitemcol = ColDataSetField.item(sttoken)
      If stitemcol <> "" Then
        ''tipo = Split(stitemcol, " - ")(1)
        'MKR_IN7 = Double.valueOf(inputDataRow.get("MKR_IN7").toString()) ;
        tagWrite Split(stitemcol, " - ")(0) & "= (" & Split(stitemcol, " - ")(1) & ") inputDataRow.get(""" & Split(stitemcol, " - ")(0) & """;"
        'token = token & "inputDataRow.get(""" & sttoken & """))"
         'stwrite = stwrite & "inputDataRow.get(""" & sttoken & """));"
      End If
      'Else
        'stwrite = stwrite & sttoken & ");"
      token = token & sttoken & ");"
      'End If
     ' tagWrite "Date " & token & "= SasDateHelper.getYear(Date " & Replace(Replace(statement, "(", ""), ")", "") & " ) ;"
    Case "SYMGET"
      token = token & " SasCtxManager.getParam(" & Replace(statement, "'", """") & ")"
      nextTkn = nextToken_newnew(statement)
'''    Case "SUBSTR"
'''        nextTkn = nextToken_newnew(statement)
'''        nextTkn = getSUBSTR(nextTkn)
'''        addOutputData token, Left(nextTkn, InStr(nextTkn, ".") - 1)
'''        replaceOperator statement
'''        tagWrite token & " = " & IIf(Right(statement, 1) = "}", Replace(statement, "}", "" & vbCrLf & "  }"), nextTkn & " " & statement & "")
    Case "INT" 'PARTE INTERA
    'double num5 = 7.87;
    'int num6 = (int)num5;  // num6 vale 7, cioe' la parte intera di 7.87
        nextTkn = nextToken_newnew(statement)
        If nextTkn = "(DATETIME())" Then
          tagWrite "//FIXME TO MARCO: function to return TIMESTAMP"
          token = token & "(int)" & nextTkn
       Else
          token = token & stwrite & "(int)"
        End If
        stwrite = ""
    Case Else
        If Left(nextTkn, 1) = "(" Then
          nextTkn = Mid(nextTkn, 2)
          statement = nextTkn & " " & statement
          nextTkn = nextToken_newnew(statement)
          token = token & " ("
          'stwrite = stwrite & " ("
        Else
          sttoken = nextToken_newnew(nextTkn)
          stparentesi = IIf(InStr(sttoken, ")"), ")", "")
          stgraffa = IIf(InStr(sttoken, "}"), stgraffa & "}", stgraffa)
          sttoken = Replace(Replace(sttoken, "(", ""), ")", "")
          stitemcol = ColDataSetField.item(sttoken)
          If stitemcol <> "" Then
             ColDataField.Add stitemcol, sttoken
             'MKR_IN7 = Double.valueOf(inputDataRow.get("MKR_IN7").toString()) ;
             tagWrite sttoken & "= (" & Split(stitemcol, " - ")(1) & ") inputDataRow.get(""" & sttoken & """);"
            ' tagWrite sttoken & " = " & "inputDataRow.get(""" & sttoken & """);"
          End If
          If stwrite = "" Then
             token = token & " " & sttoken & stparentesi
          Else
            stwrite = stwrite & " " & sttoken & stparentesi
          End If
          nextTkn = nextToken_newnew(statement)
          If statement = "" Then
            token = token & " " & nextTkn
          End If
        End If
        '''tagWrite token & " = " & IIf(Right(statement, 1) = "}", Replace(statement, "}", ";" & vbCrLf & "  }"), nextTkn & " " & statement & ";")
    End Select
  Loop
  If stwrite = "" Then
    token = Replace(token, "||", "+")
    token = Replace(token, "'", """")
    token = Replace(token, "}", "")
    token = Replace(token, vbCrLf & "BREAK", ";" & vbCrLf & "break")
    stwrite = token & ";" & stgraffa
    stwrite = Replace(stwrite, ".;", "null;")
  End If
  tagWrite stwrite
  
  Exit Sub
catch:
  stitemcol = ""
  Resume Next
End Sub
Private Sub addOutputData(item As String, Optional statement As String)
  Dim Tipo As String, stitemcol As String
  ''On Error Resume Next ' In caso inserisse un elemento gi� esistente
  On Error GoTo catch
  Select Case True
  Case Left(statement, 1) = "'" Or InStr(statement, "SUBSTR") Or InStr(statement, "TRIM") Or InStr(statement, "LEFT") Or InStr(statement, "TRANSLATE") Or InStr(statement, "SCAN") Or InStr(statement, "||")
     Tipo = "String"
     stitemcol = ColDataField.item(item)
     If stitemcol = "" Then
       ColDataField.Add item & " - " & Tipo, Trim(item)
     Else
       ColDataField.Remove (item)
       ColDataField.Add item & " - " & Tipo, Trim(item)
       '''tagWrite "private String " & item & ";", "OutputData(i)"
     End If
  Case IsNumeric(statement) Or InStr(statement, item) Or InStr(statement, "*") Or InStr(statement, "+") Or InStr(statement, "-") Or InStr(statement, "/") Or InStr(statement, "SUM(") Or InStr(statement, "INT(")
     Tipo = "Integer"
     ''ColDataField.Add item & " - " & tipo, Trim(item)
     stitemcol = ColDataField.item(item)
     If stitemcol = "" Then
       ColDataField.Add item & " - " & Tipo, Trim(item)
     Else
       ColDataField.Remove (item)
       ColDataField.Add item & " - " & Tipo, Trim(item)
       ''tagWrite "private Double " & item & ";", "OutputData(i)"
     End If
  Case InStr(statement, "DATE") > 0 Or InStr(statement, "MDY") > 0
     Tipo = "Date"
     stitemcol = ColDataField.item(item)
     If stitemcol = "" Then
       ColDataField.Add item & " - " & Tipo, Trim(item)
     Else
       ColDataField.Remove (item)
       ColDataField.Add item & " - " & Tipo, Trim(item)
     End If
  Case Else
    On Error GoTo catch
    stitemcol = ColDataSetField.item(statement)
''    If stitemcol <> "" Then
''       tipo = Split(stitemcol, " - ")(1)
''       ColDataField.Add item & " - " & tipo, Trim(item)
''    Else
    If stitemcol = "" Then
       If InStr(statement, "INT(") Then
         ColDataField.Remove (item)
         ColDataField.Add item & " - " & "Integer", Trim(item)
         ''tagWrite "private Integer " & item & ";", "OutputData(i)"
       Else
         stitemcol = ColDataField.item(statement)
         If stitemcol <> "" Then
           Tipo = Split(stitemcol, " - ")(1)
           ColDataField.Add item & " - " & Tipo, Trim(item)
         Else
        ' If stitemcol = "" Then
           tagWrite "// TODO : change type of variable!!!"
           Tipo = "String"
           ColDataField.Add item & " - " & Tipo, Trim(item)
           tagWrite "private String " & item & ";", "OutputData(i)"
         End If
       End If
    End If
  End Select
  Exit Sub
catch:
  stitemcol = ""
  Resume Next
End Sub
Private Sub parseOutputProc(statement As String)
  Dim token As String
  Dim rs As Recordset
  Dim i As Integer
  Dim NOMEBATCH  As String
  NOMEBATCH = GbNomePgm
  Dim sttable As String
  If InStr(statement, "OUT") Then
    statement = Mid(statement, InStr(statement, "=") + 1)
    token = nexttoken(statement)
    'insert o update nella SAS_Column delle colonne del dataset temporaneo
    sttable = UCase(token)
    SrProcName = sttable
    SrProcName = Replace(SrProcName, "_", "")
    SrProcName = UCase(Chr(Asc(LCase(SrProcName)))) & Right(LCase(SrProcName), Len(LCase(SrProcName)) - 1)
    
    changeTag RTB, "<ProcStep>", SrProcName

    'tagWrite "// SasStep " & SrProcName & "=null;", "ProcStepName(i)"

    sttable = IIf(InStr(sttable, "."), sttable, "WORK." & sttable)
      
    If InStr(sttable, "WORK.") Then
      Set rs = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & sttable & "' and PGM = '" & GbNomePgm & "'")
    Else
      Set rs = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & sttable & "'")
    End If
    If InStr(token, "DNULL") = 0 Then
      If rs.RecordCount = 0 Then
        For i = 1 To ColWorkDataset.count
          rs.AddNew
          rs!tableName = UCase(sttable)
          rs!PGM = NOMEBATCH
          rs!Name = Split(ColWorkDataset.item(i), " - ")(0)
          rs!Type = Split(ColWorkDataset.item(i), " - ")(1)
          rs.Update
        Next
        rs.Close
      Else
        Parser.PsConnection.Execute "DELETE * from  SAS_Column where TableName = '" & sttable & "' and PGM = '" & GbNomePgm & "'"
        For i = 1 To ColWorkDataset.count
          rs.AddNew
          rs!tableName = UCase(sttable)
          rs!PGM = NOMEBATCH
          rs!Name = Split(ColWorkDataset.item(i), " - ")(0)
          rs!Type = Split(ColWorkDataset.item(i), " - ")(1)
          rs.Update
        Next
        rs.Close
      End If
    End If
  End If
End Sub
Private Sub parsePUT(statement As String)
  Dim stsplit() As String, token As String, stType As String, tkname As String, tktype As String, stputstatement As String, stappo As String
  Dim i As Integer, j As Integer, intColNo  As Integer
'  statement = Left(statement, Len(statement) - 1)
  
  ''''
  ''    VirtualDash vp=new VirtualDash();
  ''    vp.put("ciccio");
  ''    vp.put("bello",ColNum);
  ''    vp.putCurrentLine("Mamma",ColNum);
  ''
  ''    vp.write(System.out);
  '''
  stsplit = Split(statement, "@")
  If UBound(stsplit) = 0 Then
   Do While Len(statement)
    tkname = nexttoken(statement)
    tktype = nexttoken(statement)
    If InStr(tktype, "$") Then
      stType = "String"
    Else
      If InStr(tktype, "YY") Or InStr(tkname, "_DT") Or InStr(tkname, "DATE") Or InStr(tkname, "TIME") Then
        stType = "Date"
      Else
        stType = "Double"
      End If
    End If
''    stlen = 0
''    For j = 1 To Len(tktype)
''     If IsNumeric(Mid(tktype, j, 1)) Then
''       stlen = stlen & Mid(tktype, j, 1)
''     Else
''       If Mid(tktype, j, 1) = "." Then
''         Exit For
''       End If
''     End If
''    Next
''    intlength = CInt(stlen)
    tagWrite tkname & "= (" & stType & ") inputDataRow.get(""" & tkname & """);"
    On Error GoTo catch
    ColDataField.Add tkname & " - " & stType, tkname
    stputstatement = stputstatement & tkname & " +" & vbCrLf
   Loop
   tagWrite "stput=" & stputstatement
   tagWrite "vp.put(stput);"
  End If
  '   put @2 '*DJDE* FORMS=NONE,FONTS=(AGL2YP,T00220),PMODE=POR,'
  '   PUT @1 '1' @2 '1' POLNO;
    If UBound(stsplit) = 1 Then
      stappo = stsplit(1)
      token = nextToken_newnew(stappo)
       intColNo = CInt(token)
      'stputstatement = IIf(CInt(token) > 20, """" & Space(CInt(token) - 1) & """" & vbCrLf & "+", """" & Space(CInt(token) - 1) & """" & "+")
      stappo = Replace(stappo, "'", """")
      'stputstatement = stputstatement & stappo
      'tagWrite "stput=" & stputstatement & ";"
      tagWrite "stput=" & stappo & ";"
      tagWrite "vp.put(stput," & intColNo & " );"
    Else
      For i = 1 To UBound(stsplit)
        stappo = stsplit(i)
        token = nextToken_newnew(stappo)
        If i = 1 Then
          If IsNumeric(token) Then
            intColNo = CInt(token)
 
            'stputstatement = IIf(CInt(token) > 20, """" & Space(CInt(token) - 1) & """" & vbCrLf & "+", """" & Space(CInt(token) - 1) & """" & "+")
          Else
            stputstatement = stputstatement & token & " +" & vbCrLf
          End If
          stappo = Replace(stappo, "'", """")
'          stputstatement = stputstatement & stappo
'          tagWrite "stput=" & stputstatement & ";"
'          tagWrite "vp.put(stput);"
          tagWrite "stput=" & stappo & ";"
          tagWrite "vp.put(stput," & intColNo & " );"
        Else
          If IsNumeric(token) Then
            intColNo = CInt(token)
            ''stputstatement = stputstatement & vbCrLf & """" & Space(CInt(token))
          Else
            stputstatement = stputstatement & token & " +" & vbCrLf
          End If
          stappo = Replace(stappo, "'", """")
          tagWrite "stput=" & stappo & ";"
          tagWrite "vp.putCurrentLine(stput," & intColNo & " );"
        End If
        'stputstatement = stappo
      Next
  End If
  'stputstatement = Left(stputstatement, Len(stputstatement) - 1)
  Exit Sub
catch:
  Resume Next
End Sub

Private Sub parseInput(statement As String)
  Dim stsplit() As String, token As String, stType As String, tkname As String, tktype As String
  Dim i As Integer
  statement = Left(statement, Len(statement) - 1)
  statement = UCase(statement)
  stsplit = Split(statement, "@")
  If UBound(stsplit) = 0 Then
   Do While Len(statement)
    tkname = nexttoken(statement)
    tktype = nexttoken(statement)
    If InStr(tktype, "$") Then
      stType = "String"
    Else
      If InStr(tktype, "YY") Or InStr(tkname, "_DT") Or InStr(tkname, "DATE") Or InStr(tkname, "TIME") Then
        stType = "Date"
      Else
        stType = "Double"
      End If
    End If
    On Error GoTo catch
    ColDataSetField.Add tkname & " - " & stType, tkname
   Loop
  End If
  For i = 1 To UBound(stsplit)
    token = nexttoken(stsplit(i))
    token = nexttoken(stsplit(i))
    If InStr(stsplit(i), "$") Then
      stType = "String"
    Else
      If InStr(stsplit(i), "YY") Then  ''Or InStr(token, "_DT")   Or InStr(token, "DATE") > 0 Or InStr(token, "TIME") > 0 Then
        stType = "Date"
      Else
        stType = "Double"
      End If
    End If
    On Error GoTo catch
    ColDataSetField.Add token & " - " & stType, token
    If Len(stsplit(i)) > 6 Then
      i = i - 1
    End If
  Next
  Exit Sub
catch:
  Resume Next

End Sub
Private Sub parseOutput(statement As String)
  Dim stoutput As String, token As String, stOutputRow As String, stdrop As String, stdataSetfield As String, nexttoken As String, sttable As String, stttipo As String, stappo As String
  Dim i As Integer, j As Integer, k As Integer
  Dim rs As Recordset
  Dim bolNOdelete As Boolean
  bolNOdelete = False
''  // OUTPUT DATASET POPULATION
'' Map<String,Object> outputRow=new HashMap<String,Object>();
'' outputRow.put("NAME", NAME);
  On Error GoTo catch
  'If BolMerge Then CurrentTag = "writeDown(i)"
  Set ColWorkDataset = New Collection
  statement = UCase(statement)
  statement = Replace(statement, "WORK.", "")
  statement = IIf(InStr(statement, "."), statement, "WORK." & statement)
  sttable = nextToken_new(statement)
  stoutput = ColOutputdataset.item(sttable)
  
  tagWrite "Map<String,Object> outputRow=new HashMap<String,Object>();"
  token = nextToken_new(stoutput)
  If token = "DROP:" Then
    For i = 1 To ColDataSetField.count
      stdataSetfield = nextToken_new(ColDataSetField.item(i))
      stOutputRow = ColDataField.item(stdataSetfield)
      ' campo del dataset gi� valorizzato
      If stOutputRow <> "" Then
        token = nextToken_new(stOutputRow)
        tagWrite "outputRow.put(""" & token & """, " & token & ");"
        If InStr(token, "[]") = 0 And InStr(stoutput, token) = 0 Then
          ColWorkDataset.Add ColDataField.item(token), token
          ColDataField.Remove (stdataSetfield)
        End If
      ElseIf stOutputRow = "" Then  ' campo del dataset non valorizzato
        'verifico che sia tra i campi della DROP
        If InStr(stoutput, stdataSetfield) = 0 Then
           tagWrite "outputRow.put(""" & stdataSetfield & """, inputDataRow.get(""" & stdataSetfield & """));"
           ColWorkDataset.Add ColDataSetField.item(stdataSetfield), stdataSetfield
        End If
      End If
    Next
   ' If BolIsFile Then
      For i = 1 To ColDataField.count
        stdataSetfield = nextToken_new(ColDataField.item(i))
        'stOutputRow = ColDataField.item(stdataSetfield)
        If InStr(stoutput, stdataSetfield) = 0 Then
          If InStr(stdataSetfield, "[]") = 0 And InStr(stdataSetfield, "%") = 0 Then
           ColWorkDataset.Add ColDataField.item(stdataSetfield), stdataSetfield
          End If
        End If
      Next
   ' End If
  ElseIf token = "KEEP:" Then
    Do While stoutput <> ""
      token = nextToken_new(stoutput)
      If token = "-" Then
        token = nextToken_new(stoutput)
        j = 2
        k = 1
        If IsNumeric(Right(token, 2)) Then
          k = 2
        End If
        If IsNumeric(Right(token, 3)) Then
          k = 3
        End If
        Do While token <> Left(token, Len(token) - k) & j
          stoutput = Left(token, Len(token) - k) & j & " " & stoutput
          j = j + 1
        Loop
      End If
      stOutputRow = ColDataField.item(token)
      ' campo gi� valorizzato
      If stOutputRow <> "" Then
        nexttoken = nextToken_new(stOutputRow)
        tagWrite "outputRow.put(""" & nexttoken & """,  " & nexttoken & ");"
        ColWorkDataset.Add ColDataField.item(token), token
      End If
      If stOutputRow = "" Then
        If ColRename.count Then
            stOutputRow = ColRename.item(token)
            If stOutputRow = "" Then
              stOutputRow = ColRenameInv.item(token)
            End If
            If stOutputRow <> "" Then
              tagWrite "outputRow.put(""" & token & """, inputDataRow.get(""" & stOutputRow & """));"
              ColWorkDataset.Add token & " - " & Split(ColDataSetField.item(stOutputRow), " - ")(1), token
              ColRename.Remove (stOutputRow)
              ColRenameInv.Remove (token)
            Else
              ColWorkDataset.Add ColDataSetField.item(token), token
              tagWrite "outputRow.put(""" & token & """, inputDataRow.get(""" & token & """));"
            End If
        Else
          stdataSetfield = nextToken_new(ColDataSetField.item(token))
          If stdataSetfield <> "" Then
            ColWorkDataset.Add ColDataSetField.item(token), token
            tagWrite "outputRow.put(""" & token & """, inputDataRow.get(""" & token & """));"
          Else
            ColWorkDataset.Add token & " - " & "String", token
            tagWrite "outputRow.put(""" & token & """,  " & token & ");"
          End If
        End If
         
         
      End If
    Loop
  Else
    ' tutti i campi del dataset
    If Not BolIsFile Or ColDataField.count = 0 Then
      tagWrite "outputRow.putAll(inputDataRow);"
      For i = 1 To ColDataField.count
        stOutputRow = ColDataField.item(i)
        token = nextToken_new(stOutputRow)
        tagWrite "outputRow.put(""" & token & """, " & token & ");"
      Next
      For i = 1 To ColDataSetField.count
        stdataSetfield = nextToken_new(ColDataSetField.item(i))
        ColWorkDataset.Add ColDataSetField.item(stdataSetfield), stdataSetfield
      Next
      'Set ColDataSetField = New Collection
    Else
      For i = 1 To ColDataSetField.count
        stdataSetfield = nextToken_new(ColDataSetField.item(i))
        stOutputRow = ColDataField.item(stdataSetfield)
        ' campo del dataset gi� valorizzato
        If stOutputRow <> "" Then
          token = nextToken_new(stOutputRow)
          'tagWrite "outputRow.put(""" & token & """, " & token & ");"
          tagWrite "outputRow.put(""" & stdataSetfield & """, " & stdataSetfield & ");"
          ColWorkDataset.Add ColDataSetField.item(stdataSetfield), stdataSetfield
          '''ColDataField.Remove (stdataSetfield)
        ElseIf stOutputRow = "" Then  ' campo del dataset non valorizzato
          'verifico che sia tra i campi della DROP
          'If InStr(stoutput, stdataSetfield) = 0 Then
             tagWrite "outputRow.put(""" & stdataSetfield & """, inputDataRow.get(""" & stdataSetfield & """));"
             ColWorkDataset.Add ColDataSetField.item(stdataSetfield), stdataSetfield
          'End If
        End If
      Next
     ' If BolIsFile Then
        For i = 1 To ColDataField.count
          stOutputRow = ColDataField.item(i)
          If InStr(stOutputRow, "[]") = 0 Then
            token = nextToken_new(stOutputRow)
            ColWorkDataset.Add ColDataField.item(token), token
          End If
        Next
     ' End If
    End If
  End If
  
  Dim NOMEBATCH As String
  NOMEBATCH = GbNomePgm
  
  If ColRename.count Then
    For i = 1 To ColWorkDataset.count
      stOutputRow = ColRename.item(Split(ColWorkDataset.item(i), " - ")(0))
      If stOutputRow <> "" Then
        ColRename.Remove (Split(ColWorkDataset.item(i), " - ")(0))
        ColRenameInv.Remove (stOutputRow)
        ColWorkDataset.Add stOutputRow & " - " & Split(ColWorkDataset.item(i), " - ")(1), stOutputRow
        ColWorkDataset.Remove (i)
        
      Else
        stOutputRow = ColRenameInv.item(Split(ColWorkDataset.item(i), " - ")(0))
        If stOutputRow <> "" Then
          ColRenameInv.Remove (Split(ColWorkDataset.item(i), " - ")(0))
           ColRename.Remove (stOutputRow)
          ' ColWorkDataset.Add stOutputRow & " - " & Split(ColWorkDataset.item(i), " - ")(1), stOutputRow
          ColWorkDataset.Remove (stOutputRow)
        End If
      End If
      If ColRename.count = 0 Then Exit For
    Next
  End If
  
 ' stappo = Right(sttable, 2)
  
  If IsNumeric(Right(sttable, 1)) And InStr(Right(sttable, 2), "_") > 0 Then
    sttable = Left(sttable, Len(sttable) - 2)
    bolNOdelete = True
  End If
  
    'insert o update nella SAS_Column delle colonne del dataset temporaneo
  Set rs = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & sttable & "' and PGM = '" & NOMEBATCH & "'")
  If InStr(sttable, "DNULL") = 0 Then
    If rs.RecordCount = 0 Then
     '' Parser.PsConnection.Execute "DELETE * from  SAS_Column where TableName = '" & Replace(StDATASETNAME, "WORK.", "") & "'"
      For i = 1 To ColWorkDataset.count
        rs.AddNew
        rs!tableName = sttable
        rs!PGM = NOMEBATCH
        rs!Name = Split(ColWorkDataset.item(i), " - ")(0)
        rs!Type = Split(ColWorkDataset.item(i), " - ")(1)
        rs.Update
      Next
      rs.Close
    Else
      If bolNOdelete Then
        For i = 1 To ColWorkDataset.count
          Set rs = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & sttable & "' and PGM = '" & NOMEBATCH & "' AND Name = '" & Trim(Split(ColWorkDataset.item(i), " - ")(0)) & "'")
          If rs.RecordCount = 0 Then
            rs.AddNew
            rs!tableName = sttable
            rs!PGM = NOMEBATCH
            rs!Name = Split(ColWorkDataset.item(i), " - ")(0)
            rs!Type = Split(ColWorkDataset.item(i), " - ")(1)
            rs.Update
          End If
          rs.Close
        Next
      Else
        Parser.PsConnection.Execute "DELETE * from  SAS_Column where TableName = '" & sttable & "' and PGM = '" & NOMEBATCH & "'"
        For i = 1 To ColWorkDataset.count
          rs.AddNew
          rs!tableName = sttable
          rs!PGM = NOMEBATCH
          rs!Name = Split(ColWorkDataset.item(i), " - ")(0)
          rs!Type = Split(ColWorkDataset.item(i), " - ")(1)
          rs.Update
        Next
      End If
      rs.Close
    End If
  End If
  'statement = Replace(statement, "WORK.", "")
  
  If InStr(sttable, ".") Then
    sttable = UCase(Chr(Asc(LCase(Split(sttable, ".")(0))))) & Right(LCase(Split(sttable, ".")(0)), Len(LCase(Split(sttable, ".")(0))) - 1) & _
             UCase(Chr(Asc(LCase(Split(sttable, ".")(1))))) & Right(LCase(Split(sttable, ".")(1)), Len(LCase(Split(sttable, ".")(1))) - 1)
  Else
    sttable = UCase(Chr(Asc(LCase(sttable)))) & Right(LCase(sttable), Len(LCase(sttable)) - 1)
  End If
  sttable = Replace(sttable, "_", "")
  sttable = LCase(Chr(Asc(LCase(sttable)))) & Right(sttable, Len(LCase(sttable)) - 1)
  tagWrite sttable & ".addRow(outputRow);"
  
  ' tipo query
''  If InStr(StQuery, "*") And Not BolIsFile Then
''     changeTag RTBDataStep, "<tipoquery>", "Query"
''     changeTag RTBDataStep, "<SQLQUERY>", StQuery
''     changeTag RTBQuery, "<QUERY>", StQuery
''  Else
     changeTag RTBDataStep, "<tipoquery>", IIf(BolIsFile, "File", "NamedQuery")
     If BolIsFile Then
       changeTag RTBDataStep, "<SQLQUERY>", "..PathFile/" & UCase(StDataStep)
       RTBDataStep.Text = Replace(RTBDataStep.Text, ",null", "")
       '''changeTag RTBQuery, "<NAMEQUERY>", "PathFile"
     Else
       changeTag RTBQuery, "<<SQLQUERY(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\SqlQuery"
       changeTag RTBDataStep, "<SQLQUERY>", "SQ" & UCase(StDataStep)
       changeTag RTBQuery, "<NAMEQUERY>", "SQ" & UCase(StDataStep)
       StQuery = Replace(StQuery, "FROM", vbCrLf & "FROM")
       StQuery = Replace(StQuery, "WHERE", vbCrLf & "WHERE")
       StQuery = Replace(StQuery, "AND", vbCrLf & "AND")
       StQuery = Replace(StQuery, "OR", vbCrLf & "OR")
       changeTag RTBQuery, "<QUERY>", StQuery
     End If
     StQuery = ""
''  End If

  Exit Sub
catch:
  stOutputRow = ""
  Resume Next
End Sub


Private Sub parseSASvariable(statement As String)
  Dim token As String, starray As String, stvariable As String, sttable As String, sttipo As String, stitemcol As String, stappo As String
  Dim rs As Recordset
  Dim i As Integer, j As Integer, k As Integer
  statement = UCase(statement)
  CurrentTag = "SASVARIABLE(i)"
  token = nexttoken(statement)
  On Error GoTo catch
  Select Case token
  Case "ARRAY"
    ' ARRAY R_NAME(20) R_CNAM0-R_CNAM19;
    'private String[] R_NAME = new String[20]
    statement = Replace(statement, "(", " (")
    statement = Replace(statement, "{", " {")
    starray = nexttoken(statement)
    token = nexttoken(statement)
    token = Replace(Replace(token, "(", "["), ")", "]")
    token = Replace(Replace(token, "{", "["), "}", "]")
    'If Not IsNumeric(token) And token <> "*" Then
    '   statement = token & " " & statement
    'End If
    If InStr(statement, "$") Then
      sttipo = "String[]"
      stvariable = nexttoken(statement)
      tagWrite "// ARRAY di elemento = non usato. Parto da 1 come il SAS"
      tagWrite "private String[] " & starray & " = new String" & token & ";"
    Else
      If statement <> "" Then
''        stvariable = statement
''        If InStr(stvariable, "-") Then
''          stvariable = Replace(stvariable, " - ", "' AND '")
''          stvariable = "'" & stvariable & "'"
''        Else
''
''        End If
''        If InStr(StINPUTDATASET, ".") And InStr(StINPUTDATASET, "WORK.") = 0 Then
''          sttable = Split(StINPUTDATASET, ".")(1)
''        Else
''          sttable = Replace(StINPUTDATASET, "WORK.", "")
''          sttable = "WORK." & sttable
''        End If

'''        Set rs = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & sttable & "' AND name BETWEEN " & stvariable & "")
'''        If rs.RecordCount Then
'''          sttipo = rs!Type & "[]"
'''          tagWrite "private " & rs!Type & "[] " & starray & " = new " & rs!Type & token & ";"
'''        Else
            sttipo = "String[]"
            tagWrite "private String[] " & starray & " = new String" & IIf(IsNumeric(Replace(Replace(token, "[", ""), "]", "")), token, "") & ";"
'''        End If
'''        rs.Close
      Else
          sttipo = "Double[]"
          tagWrite "private Double[] " & starray & " = new Double" & IIf(IsNumeric(Replace(Replace(token, "[", ""), "]", "")), token, "") & ";"
      End If
    End If
    ColDataField.Add starray & " - " & sttipo, starray
    If Len(statement) Then
      CurrentTag = "process(i)"
      For i = 1 To Len(statement)
        If IsNumeric(Mid(statement, i, 1)) Then
          statement = Left(statement, i - 1)
          Exit For
        End If
      Next
      'for (int i = 0; i < 20; i++) {
      '  R_NAME[i]= inputDataRow.get("R_CNAM"+(i)).toString();
      '}
      tagWrite "for (int i = 1; i <= " & Replace(Replace(IIf(InStr(token, "["), token, "???"), "[", ""), "]", "") & "; i++) {"
      tagWrite starray & "[i]= inputDataRow.get(""" & statement & "+(i)).toString();" & vbCrLf & "}"
    End If
  Case "RETAIN"
    tagWrite "// RETAIN: "
    tagWrite "// TODO: check variable type"
    'statement = Replace(statement, " ", vbCrLf)
     Do While statement <> ""
      token = nextToken_new(statement)
      If token = "-" Then
        token = nextToken_new(statement)
        j = 2
        k = 1
        If IsNumeric(Right(token, 2)) Then
          k = 2
        End If
        If IsNumeric(Right(token, 3)) Then
          k = 3
        End If
        Do While token <> Left(token, Len(token) - k) & j
          statement = Left(token, Len(token) - k) & j & " " & statement
          j = j + 1
        Loop
      End If
  
    
   '' For i = 0 To UBound(Split(statement, vbCrLf))
      stappo = token
      'stappo = Trim(Split(statement, vbCrLf)(i))
      'stvariable = Trim(Split(statement, vbCrLf)(i))
      If stappo <> "" And stappo <> "." And stappo <> "" And stappo <> "''" And InStr(stappo, "'") = 0 Then
        stvariable = stappo
        stitemcol = ColDataField.item(stvariable)
        If stitemcol = "" Then
          '''tagWrite "// TODO: check variable type .."
          If InStr(stvariable, "_DT") Or InStr(stvariable, "DATE") Then
             ColDataField.Add stvariable & " - " & "Date", stvariable
          Else
            stitemcol = ColLength.item(stvariable)
            If stitemcol <> "" Then
              ColDataField.Add ColLength.item(stvariable), stvariable
            Else
              ColDataField.Add stvariable & " - " & "String", stvariable
            End If
          End If
          ''''tagWrite "private String " & stvariable & ";"
        End If
      Else
        If InStr(stappo, "'") And stappo <> "''" Then
          'ColDataField.Remove (stvariable)
          ColDataField.Add stvariable & " - " & "String", stvariable
          stappo = Trim(Split(statement, vbCrLf)(i))
          stappo = Replace(stappo, "'", """")
          tagWrite "private String " & stvariable & " = " & stappo & ";"
        End If
''        If Trim(Split(statement, vbCrLf)(i)) = "." Then
''          ColDataField.Remove (stvariable)
''          ColDataField.Add stvariable & " - " & "Integer", stvariable
''          '''tagWrite "private Integer " & stvariable & ";"
''        End If
''        If Trim(Split(statement, vbCrLf)(i)) = "''" Then
''          ColDataField.Add stvariable & " - " & "String", stvariable
''          If stitemcol <> "" Then
''            tagWrite "private String " & stvariable & ";"
''          End If
''        End If
      
      End If
    Loop
  Case "LENGTH"
    Do While statement <> "" And token <> ""
      stvariable = nexttoken(statement)
      stitemcol = ColDataField.item(stvariable)
      If stitemcol = "" Then
        token = nexttoken(statement)
        If IsNumeric(token) Or Right(token, 1) = "." Or InStr(token, "$") Then
          sttipo = token
          If InStr(sttipo, "$") Then
             ColLength.Add stvariable & " - " & "String", stvariable
          Else
            If InStr(stvariable, "_DT") Or InStr(stvariable, "DATE") Then
              ColLength.Add stvariable & " - " & "Date", stvariable
            Else
              ColLength.Add stvariable & " - " & "Double", stvariable
            End If
          End If
        Else
          ColLength.Add stvariable & " - " & "String", stvariable
          statement = token & " " & statement
        End If
      End If
    Loop
  
  Case Else
    tagWrite "// TODO: check variable type"
    tagWrite "private String " & statement & ";"
  End Select
  
  Exit Sub
catch:
  stitemcol = ""
  Resume Next
End Sub
Private Sub parseDO(statement As String)
  Dim token As String, stINT As String
  On Error GoTo catch
  'SAS: Do i = 1 To 20;
  'JAVA: for(int i = 1; i <= 20; i++)
  
  'SAS: DO WHILE ...;
  'JAVA:  WHILE ...;
  If InStr(UCase(statement), "WHILE ") Then
    statement = Replace(statement, "(", vbCrLf & "(")
    replaceOperator statement
    tagWrite statement & vbCrLf & "{"
  Else
    statement = Replace(statement, "=", " = ")
    stINT = nexttoken(statement)
    ColDataField.Add stINT & " - " & "Integer", stINT
    token = nexttoken(statement)
    token = nexttoken(statement)
    statement = Trim(Replace(statement, "TO", ""))
    tagWrite "for(int " & stINT & " = " & token & "; " & stINT & " <= " & statement & "; " & stINT & "++);" & vbCrLf & "{"
  End If
  Exit Sub
catch:
  Resume Next
End Sub

Private Sub parseIF(parameters As String)
  Dim token As String, newtoken As String, statement As String, stitemcol As String, stappocol As String
  On Error GoTo catch
  token = nextToken_newnew(parameters)
  
''  Select Case token
''    Case "SUBSTR"
''      token = nextToken_newnew(parameters)
''      token = getSUBSTR(token)
''    'parameters = parameters
''    Case "UPCASE"
''      token = nexttoken(parameters)
''      token = token & ".toUpperCase()"
''    ' parameters = parameters & ".toUpperCase"
''    Case Else
''    End Select
  token = Replace(token, "_N_", "n")
  stitemcol = ColDataSetField.item(token)
  If stitemcol <> "" Then
    'inputDataRow.get("MVT_TYPE")
    stappocol = stitemcol
    'tagWrite token & "=" & Split(stitemcol, " - ")(1) & ".valueOf(inputDataRow.get(""" & token & """).toString()) ;"
    stitemcol = ColDataField.item(token)
    If stitemcol = "" Then
      ''(Integer)inputDataRow.get("MAT_DY")
      tagWrite token & "= (" & Split(stappocol, " - ")(1) & ") inputDataRow.get(""" & token & """);"
      ''tagWrite token & "=" & Split(stappocol, " - ")(1) & ".valueOf(inputDataRow.get(""" & token & """).toString()) ;"
      ColDataField.Add stappocol, token
    End If
    'token = "inputDataRow.get(""" & token & """)"
  End If
  Do Until UCase(token) = "THEN" Or parameters = ""
    If Left(token, 1) = "(" Then   ''''And Right(token, 1) = ")" Then
      token = Mid(token, 2)
      newtoken = nextToken_newnew(token)
      statement = statement & " ( "
      Do Until newtoken = ")" Or token = ""
''        Select Case newtoken
''        Case "SUBSTR"
''          newtoken = nextToken_newnew(token)
''          newtoken = getSUBSTR(newtoken)
''       'parameters = parameters
''        Case "UPCASE"
''          newtoken = nextToken_newnew(token)
''          newtoken = newtoken & ".toUpperCase"
''       ' parameters = parameters & ".toUpperCase"
''        Case Else
''        End Select
        statement = statement & " " & newtoken
        newtoken = nextToken_newnew(token)
       Loop
       statement = statement & " " & newtoken
       token = nextToken_newnew(parameters)
     End If
     If UCase(token) = "THEN" Then Exit Do
     statement = statement & " " & token
     If InStr(statement, ".get") Then statement = Replace(statement, "=", ".equals(")
     If InStr(parameters, "'") Then
       ''statement = statement & " " & "'"
       parameters = Replace(parameters, "'", """")
     End If
     token = nextToken_newnew(parameters)
     'statement = statement & " " & token
''     If parameters <> "THEN" And parameters <> "" Then
''       If Len(Trim(Left(parameters, InStr(parameters, "THEN") - 2))) < 3 Then
''         statement = statement & " " & token & " " & Left(parameters, InStr(parameters, "THEN") - 2)
''         token = "THEN"
''       End If
''     End If
  Loop
  statement = statement & " " & token
  replaceOperator statement
   
  '''statement = statement & " )"
  tagWrite "if(" & statement   ''' & vbCrLf & " {"
  
  Exit Sub
catch:
  stitemcol = ""
  Resume Next
End Sub

Private Function getSUBSTR(FIELD As String) As String
  Dim token As String
  ''FIELD = Replace(FIELD, "(", "")
  FIELD = Mid(FIELD, 2)
  token = Left(FIELD, InStr(FIELD, ",") - 1)
  FIELD = Mid(FIELD, InStr(FIELD, ",") + 1)
  getSUBSTR = token & ".substring(" & FIELD
  
End Function

Private Sub replaceOperator(statement As String)
  
  statement = Replace(UCase(statement), " AND ", vbCrLf & " && ")
  statement = Replace(UCase(statement), " OR ", vbCrLf & " || ")
  statement = Replace(UCase(statement), " GE ", ">=")
  statement = Replace(UCase(statement), " LT ", "<=")
  statement = Replace(UCase(statement), " THEN", ")" & vbCrLf & "  {")
  'statement = Replace(statement, "else", "")
  'statement = Replace(statement, "End", "  }") & vbCrLf
  statement = Replace(statement, "^=", "!=")
  statement = Replace(statement, " ^ = ", " != ")
  statement = Replace(statement, "=.", "=null")
  statement = Replace(statement, ". ", " null ")
  statement = Replace(statement, "= .", "= null ")
  statement = IIf(IsNumeric(Right(Trim(statement), 1)), Replace(statement, " = ", " == "), statement)
  statement = Replace(statement, "_N_", " n ")
  statement = Replace(statement, "()", "")
  statement = Replace(statement, "'", """")
  
  If Trim(statement) = "." Then statement = "NULL"
End Sub

Private Sub parseDATAstep(statement As String)
  Dim token As String
  Dim stoption As String, stKeep() As String, stdrop As String, starg As String, stOutDataset As String
  Dim i As Integer, j As Integer
  On Error GoTo catch
  statement = UCase(statement)
  StQuery = ""
  SqlQuery = ""
  If Not BolMultiData Then
    Set RTBDataStep = MapsdF_Parser.rText2
    RTBDataStep.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\DataStep"
    changeTag RTB, "<<importClassDataset(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\importClassDataset"
    changeTag RTB, "<<addSasStep(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\addSasStep"
  End If
  StDATASETNAME = nexttoken(statement)

'  If Len(statement) And InStr(statement, "=") = 0 Then
'    stDataSet = stDATASETNAME & Replace(statement, " ", "")
'  End If
  
'''  If DataStep.count And Not BolMultiData Then
'''    For i = 1 To DataStep.count
'''      If DataStep.item(i) = StDATASETNAME Or DataStep.item(i) = Replace(StDATASETNAME, "_", "") Then
'''        IntDataNum = IntDataNum + 1
'''        StDATASETNAME = StDATASETNAME & "_" & IntDataNum
'''      End If
'''    Next
'''  End If
  ''stDATASETNAME = Replace(UCase(stDATASETNAME), "WORK.", "")
  If InStr(StDATASETNAME, "_NULL_") Then
      StDATASETNAME = Replace(StDATASETNAME, "_", "")
    If DataStep.count And Not BolMultiData Then
      For i = 1 To DataStep.count
        If DataStep.item(i) = "D" & StDATASETNAME Then
          IntDataNum1 = IntDataNum1 + 1
          StDataStep = StDataStep & "_" & IntDataNum1
          StDATASETNAME = StDATASETNAME & "_" & IntDataNum1
        End If
      Next
    End If

    changeTag RTBDataStep, "<<OutputDataSet(i)>>", ""
    changeTag RTBDataStep, "<<AddDataSet(i)>>", ""
    StDATASETNAME = "D" & Replace(StDATASETNAME, "_", "")
    StDataStep = UCase(Chr(Asc(LCase(StDATASETNAME)))) & Right(LCase(StDATASETNAME), Len(LCase(StDATASETNAME)) - 1)
    DataStep.Add StDATASETNAME, StDATASETNAME
    changeTag RTBDataStep, "<outDataset>", "//<outDataset>"
    changeTag RTBDataStep, "<retcode>", 0
  Else
    changeTag RTBDataStep, "<retcode>", 1
''    If InStr(StDATASETNAME, "WORK.") Then
''      stDataStep = UCase(Chr(Asc(LCase(StDATASETNAME)))) & Right(LCase(StDATASETNAME), Len(LCase(StDATASETNAME)) - 1)
''      stDataStep = Replace(stDataStep, ".", "")
''    Else
      If InStr(StDATASETNAME, ".") Then
        StDataStep = UCase(Chr(Asc(LCase(Split(StDATASETNAME, ".")(0))))) & Right(LCase(Split(StDATASETNAME, ".")(0)), Len(LCase(Split(StDATASETNAME, ".")(0))) - 1) & _
                    UCase(Chr(Asc(LCase(Split(StDATASETNAME, ".")(1))))) & Right(LCase(Split(StDATASETNAME, ".")(1)), Len(LCase(Split(StDATASETNAME, ".")(1))) - 1)
      Else
        StDataStep = "Work" & UCase(Chr(Asc(LCase(StDATASETNAME)))) & Right(LCase(StDATASETNAME), Len(LCase(StDATASETNAME)) - 1)
        'stDataStep = "work" & stDataStep
        StDATASETNAME = "WORK." & StDATASETNAME
      End If
''    End If
    ''StDATASETNAME = IIf(InStr(StDATASETNAME, "WORK."), StDATASETNAME, "WORK." & StDATASETNAME)
    changeTag RTBDataStep, "<<OutputDataSet(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\OutputDataSet"
    changeTag RTBDataStep, "<<AddDataSet(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\AddDataSet"
    changeTag RTBDataStep, "<<TEMPORARYTABLE(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\TemporaryTable"

    changeTag RTBDataStep, "<DATASETNAME>", StDATASETNAME
'''    If InStr(StDATASETNAME, ".") Then
'''      stDataStep = UCase(Chr(Asc(LCase(Split(StDATASETNAME, ".")(0))))) & Right(LCase(Split(StDATASETNAME, ".")(0)), Len(LCase(Split(StDATASETNAME, ".")(0))) - 1) & _
'''               UCase(Chr(Asc(LCase(Split(StDATASETNAME, ".")(1))))) & Right(LCase(Split(StDATASETNAME, ".")(1)), Len(LCase(Split(StDATASETNAME, ".")(1))) - 1)
'''    Else
'''      stDataStep = UCase(Chr(Asc(LCase(StDATASETNAME)))) & Right(LCase(StDATASETNAME), Len(LCase(StDATASETNAME)) - 1)
'''    End If
    
    changeTag RTB, "<INPUTDATASET>", StDATASETNAME
    
    StDataStep = Replace(StDataStep, "_", "")
    
    If DataStep.count Then
      If IntDataNum = 9 Then IntDataNum = 0
      For i = 1 To DataStep.count
        If DataStep.item(i) = StDataStep Or DataStep.item(i) = Replace(StDataStep, "_", "") Then
          IntDataNum = IntDataNum + 1
          StDataStep = StDataStep & "_" & IntDataNum
          StDATASETNAME = StDATASETNAME & "_" & IntDataNum
          Exit For
        End If
      Next
    End If
    DataStep.Add StDataStep, StDataStep
    DataStep1.Add StDATASETNAME, StDATASETNAME
    'stOutDataset = IIf(InStr(stDataStep, "."), LCase(Chr(Asc(LCase(stDataStep)))) & Right(stDataStep, Len(LCase(stDataStep)) - 1), "work" & stDataStep)
    stOutDataset = LCase(Chr(Asc(LCase(StDataStep)))) & Right(StDataStep, Len(LCase(StDataStep)) - 1)
    changeTag RTBDataStep, "<outDataset>", stOutDataset
    changeTag RTBDataStep, "<dataset>", stOutDataset

  End If
  StDataStep = Replace(StDataStep, "_", "")
  
  changeTag RTB, "<BatchName>", UCase(stBatchName)
  changeTag RTB, "<lBatchName>", LCase(stBatchName)
  
 ' changeTag RTB, "<DataStep>", stDataStep
 ' CurrentTag = "process(i)"
  If statement <> "" And InStr(statement, "=") Then  ''options
'    If Left(statement, 1) = "(" Then
'    Else
    token = nexttoken(statement)
    'stOption = Replace(Replace(token, "(", ""), ")", "")
    If InStr(token, "=") Then
      stoption = Mid(token, 2, Len(token) - 2)
      Do While Len(stoption)
       token = nexttoken(stoption)
        Select Case UCase(token)
        Case "KEEP"
        '// OutputDataset
          token = nexttoken(stoption)
          token = IIf(token = "=", "", token)
          stoption = IIf(stoption = "-", "", stoption)
          ColOutputdataset.Add "KEEP: " & stoption, StDATASETNAME
'''          starg = Replace(stoption, " ", vbCrLf)
'''          stKeep = Split(starg, vbCrLf)
'''          For i = 0 To UBound(Split(starg, vbCrLf))
'''            If Trim(stKeep(i)) <> "" Then
'''               On Error Resume Next ' In caso inserisse un elemento gi� esistente
'''               ColDataField.Add Trim(stKeep(i)), Trim(stKeep(i))
'''            End If
'''          Next
          stoption = ""
        Case "DROP"
          token = nexttoken(stoption)
          stdrop = stoption
          ColOutputdataset.Add "DROP: " & stdrop, StDATASETNAME
          stoption = ""
        
        Case "="
            tagWrite "//# " & token & " = " & stoption
        Case "INDEX", "COMPRESS", "REUSE"   ''', "DROP"
           token = nexttoken(stoption)
           token = nexttoken(stoption)
        Case "RENAME"
        Case Else
            tagWrite "//# " & token & stoption
        End Select
      Loop
      token = ""
    End If
  Else
  ''no options
    If InStr(StDATASETNAME, "DNULL") = 0 Then
      ColOutputdataset.Add StDATASETNAME, StDATASETNAME
    End If
  End If
  If statement <> "" Then
      BolMultiData = True
      statement = token & " " & statement
      StMultidataStep = StMultidataStep & StDataStep
     '' stDataStep = "M_" & stDataSet
  Else
    If BolMultiData Then
      'IntDatastep = IntDatastep + 1
      StMultidataStep = StMultidataStep & StDataStep
    End If
    StMultidataStep = Replace(StMultidataStep, "Work", "")
    StMultidataStep = "Work" & StMultidataStep
    changeTag RTB, "<DataStep>", IIf(BolMultiData = True, StMultidataStep, StDataStep)
    changeTag RTBDataStep, "<TdataSet>", StDataStep
    ''changeTag RTB, "<DATASETNAME>", IIf(BolMultiData = True, "MDATASTEP" & IntDatastep, StDATASETNAME)
    changeTag RTBDataStep, "<BatchName>", LCase(stBatchName)
    changeTag RTBDataStep, "<DataStep>", IIf(BolMultiData = True, StMultidataStep, StDataStep)
    changeTag RTBDataStep, "<BatchName>", LCase(stBatchName)
    '''
    StDataStep = IIf(BolMultiData = True, StMultidataStep, StDataStep)
    BolMultiData = False
  End If
  
  Exit Sub
catch:
  Resume Next
  
End Sub
Private Sub parseLINK(stlink As String)
  changeTag RTBDataStep, "<<LINK(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\Link"
  changeTag RTBDataStep, "<link>", LCase(stlink)
  CurrentTag = "linkprocess(i)"
End Sub
Private Sub parseMACRO(stmacro As String)
  Set RTBmacro = MapsdF_Parser.RTErr
  StMacroName = nexttoken(stmacro)
  RTBmacro.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\Macro"
  changeTag RTBmacro, "<macro>", StMacroName
  If stmacro <> "" Then
   changeTag RTBmacro, "<parameters>", stmacro
  Else
   changeTag RTBmacro, "<parameters>", "()"
  End If
  CurrentTag = "macroprocess(i)"
  
End Sub

Private Sub parseMerge(statement As String)
  Dim token As String, stoption As String, stappo As String, strename As String
  Dim rs As Recordset
  Dim i As Integer
  On Error GoTo catch
  statement = UCase(statement)

  token = nexttoken(statement)
  If InStr(token, ".") And InStr(token, "WORK.") = 0 Then
    Set rs = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & Split(token, ".")(1) & "'")
  Else
    token = Replace(token, "WORK.", "")
    Set rs = m_Fun.Open_Recordset("select * from SAS_Column where TableName = 'WORK." & token & "'")
  End If
  If InStr(token, ".") Then
    StinDataset = UCase(Chr(Asc(LCase(Split(token, ".")(0))))) & Right(LCase(Split(token, ".")(0)), Len(LCase(Split(token, ".")(0))) - 1) & _
    UCase(Chr(Asc(LCase(Split(token, ".")(1))))) & Right(LCase(Split(token, ".")(1)), Len(LCase(Split(token, ".")(1))) - 1)
  Else
    StinDataset = UCase(Chr(Asc(LCase(token)))) & Right(LCase(token), Len(LCase(token)) - 1)
  End If
  
    While Not rs.EOF
    ColDataSetField.Add rs!Name & " - " & rs!Type, rs!Name
    rs.MoveNext
  Wend
  rs.Close
  Do While statement <> ""
    token = nexttoken(statement)
    If InStr(token, "(") = 0 Then
     ' statement = Mid(statement, InStr(token, ")"))
    'Else
      If InStr(token, ".") And InStr(token, "WORK.") = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & Split(token, ".")(1) & "'")
      Else
        token = Replace(token, "WORK.", "")
        Set rs = m_Fun.Open_Recordset("select * from SAS_Column where TableName = 'WORK." & token & "'")
      End If
      
      If InStr(token, ".") Then
        token = UCase(Chr(Asc(LCase(Split(token, ".")(0))))) & Right(LCase(Split(token, ".")(0)), Len(LCase(Split(token, ".")(0))) - 1) & _
        UCase(Chr(Asc(LCase(Split(token, ".")(1))))) & Right(LCase(Split(token, ".")(1)), Len(LCase(Split(token, ".")(1))) - 1)
      Else
        token = UCase(Chr(Asc(LCase(token)))) & Right(LCase(token), Len(LCase(token)) - 1)
      End If
      StinDataset = StinDataset & token
      
      'StinDataset = LCase(Chr(Asc(LCase(StinDataset)))) & Right(StinDataset, Len(LCase(StinDataset)) - 1)
      While Not rs.EOF
        On Error GoTo catch
        ColDataSetField.Add rs!Name & " - " & rs!Type, rs!Name
        rs.MoveNext
      Wend
      rs.Close
    End If
    If InStr(token, "RENAME") Then
      stoption = Mid(token, 2, Len(token) - 2)
      token = nexttoken(stoption)
      token = nexttoken(stoption)
      token = nexttoken(stoption)
      stoption = Mid(token, 2, Len(token) - 2)
      Do While Len(stoption)
        stappo = nexttoken(stoption)
        token = nexttoken(stoption)
        strename = nexttoken(stoption)
        ColRename.Add strename, stappo
        ColRenameInv.Add stappo, strename
      Loop
    End If
  Loop
  StinDataset = LCase(Chr(Asc(LCase(StinDataset)))) & Right(StinDataset, Len(LCase(StinDataset)) - 1)
  StinDataset = Replace(Replace(StinDataset, "_", ""), ".", "")
  
  changeTag RTBDataStep, "<<getFullName(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\getFullName"
  changeTag RTBDataStep, "<<InputDataSet(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\InputDataSet"
  changeTag RTBDataStep, "<INPUTDATASET>", UCase(StinDataset)
  changeTag RTBDataStep, "<inDataset>", StinDataset
  RTBDataStep.Text = Replace(RTBDataStep.Text, "this.inputDatasetName=" & StinDataset & ";", "")
  
  Exit Sub
catch:
  Resume Next
  
End Sub

Private Sub parseSET(statement As String)
  Dim token As String, stoption As String, stKeep() As String, starg As String, stWhere As String, sttable As String
  Dim previousTag As String, sttipo As String, stkeepSql As String, stappo As String, strename As String
  Dim i As Integer, j As Integer, k As Integer
  Dim rs As Recordset, rscol As Recordset
  previousTag = CurrentTag
  statement = UCase(statement)
  StinDataset = ""
  StINPUTDATASET = ""
  StINPUTDATASET = nexttoken(statement)
  If InStr(StINPUTDATASET, ".") And InStr(StINPUTDATASET, "WORK.") = 0 Then
    StinDataset = UCase(Chr(Asc(LCase(Split(StINPUTDATASET, ".")(0))))) & Right(LCase(Split(StINPUTDATASET, ".")(0)), Len(LCase(Split(StINPUTDATASET, ".")(0))) - 1) & _
    UCase(Chr(Asc(LCase(Split(StINPUTDATASET, ".")(1))))) & Right(LCase(Split(StINPUTDATASET, ".")(1)), Len(LCase(Split(StINPUTDATASET, ".")(1))) - 1)
    sttable = Split(StINPUTDATASET, ".")(1)
  Else
    If Not BolIsFile Then
      StINPUTDATASET = IIf(InStr(StINPUTDATASET, "WORK."), StINPUTDATASET, "WORK." & StINPUTDATASET)
    End If
    sttable = Replace(StINPUTDATASET, "WORK.", "")
    sttable = "WORK." & sttable
    StinDataset = UCase(Chr(Asc(LCase(StINPUTDATASET)))) & Right(LCase(StINPUTDATASET), Len(LCase(StINPUTDATASET)) - 1)
  End If
  ''''
  ''''
  StinDataset = LCase(Chr(Asc(LCase(StinDataset)))) & Right(StinDataset, Len(LCase(StinDataset)) - 1)
  StinDataset = Replace(Replace(StinDataset, "_", ""), ".", "")
  If InStr(sttable, "WORK.") Then
    Set rscol = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & sttable & "' and PGM = '" & GbNomePgm & "'")
  Else
    Set rscol = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & sttable & "'")
  End If
    On Error GoTo catch
    If InStr(statement, "KEEP") Then
      stappo = Replace(Mid(statement, InStr(statement, "=") + 1), ")", "")
      Do While stappo <> "" And Left(stappo, 1) <> "("
        token = nextToken_new(stappo)
        If token = "-" Then
          token = nextToken_new(stappo)
          j = 2
          k = 1
          If IsNumeric(Right(token, 2)) Then
            k = 2
          End If
          If IsNumeric(Right(token, 3)) Then
            k = 3
          End If
          Do While token <> Left(token, Len(token) - k) & j
            stoption = Left(token, Len(token) - k) & j & " " & stoption
            j = j + 1
          Loop
        End If
      Loop
      statement = Replace(statement, "-", stoption)
      stoption = ""
    
      While Not rscol.EOF
        If InStr(statement, rscol!Name) Then
          ColDataSetField.Add rscol!Name & " - " & rscol!Type, rscol!Name
        End If
        rscol.MoveNext

      Wend
    Else
      While Not rscol.EOF
        ColDataSetField.Add rscol!Name & " - " & rscol!Type, rscol!Name
        rscol.MoveNext
      Wend
    End If
  rscol.Close
  ''''
  If BolIsFile Then
    SqlQuery = ""
  Else
    StQuery = IIf(Len(StQuery), StQuery & " union SELECT * FROM " & StINPUTDATASET, "SELECT * FROM " & StINPUTDATASET)
  End If
  token = nexttoken(statement)
    If InStr(token, "=") Then
      stoption = Mid(token, 2, Len(token) - 2)
      stoption = Replace(stoption, "=", " = ")
      Do While Len(stoption)
       token = nexttoken(stoption)
        Select Case token
        Case "KEEP"
          ''tagWrite "// TODO: check variable type"
          token = nexttoken(stoption)
          starg = nexttoken(stoption)
          'starg = Replace(stoption, " ", vbCrLf)
          'stKeep = Split(starg, vbCrLf)
          Do While Len(stoption) And (starg <> "RENAME")
''          For i = 0 To UBound(Split(starg, vbCrLf))
'            If Trim(stKeep(i)) <> "" Then
                sttipo = ColDataSetField.item(starg)
                stkeepSql = stkeepSql & starg & ", "
                tagWrite "String " & starg & ";"
'            End If
''          Next
            starg = nexttoken(stoption)
          Loop
          stoption = IIf(starg = "RENAME", starg & " " & stoption, stoption)
          ''stoption = ""
          stkeepSql = Trim(stkeepSql)
          stkeepSql = Left(stkeepSql, Len(stkeepSql) - 1)
          StQuery = Replace(StQuery, "*", stkeepSql)
          
        Case "DROP"
          tagWrite "//# " & token & stoption
          token = nexttoken(stoption)
          starg = Replace(stoption, " ", vbCrLf)
          stKeep = Split(starg, vbCrLf)
          For i = 0 To UBound(Split(starg, vbCrLf))
            If Trim(stKeep(i)) <> "" Then
              ColDataSetField.Remove (Trim(stKeep(i)))
            End If
          Next
          For i = 1 To ColDataSetField.count
            stkeepSql = stkeepSql & ", " & Split(ColDataSetField.item(i), " - ")(0)
          Next
          stkeepSql = Mid(stkeepSql, 2)
          StQuery = Replace(StQuery, "*", stkeepSql)
          stoption = ""
        Case "OBS"
          token = nexttoken(stoption)
          token = nexttoken(stoption)
          tagWrite "// !!!!! ROWNUM = : " & token & ";"
        Case "WHERE"
          token = nexttoken(stoption)
          stWhere = nexttoken(stoption)
          stWhere = Replace(stWhere, " AND ", " AND " & vbCrLf & "// ")
          stWhere = Replace(stWhere, " OR ", " OR " & vbCrLf & "// ")
          StQuery = StQuery & StINPUTDATASET & vbCrLf & stWhere
        
        Case "RENAME"
          On Error GoTo catch
          token = nexttoken(stoption)
          token = nexttoken(stoption)
          stoption = Mid(token, 2, Len(token) - 2)
          Do While Len(stoption)
            stappo = nexttoken(stoption)
            token = nexttoken(stoption)
            strename = nexttoken(stoption)
            ColRename.Add strename, stappo
            ColRenameInv.Add stappo, strename
          Loop
        Case Else
            tagWrite "//# " & token & stoption
        End Select
      Loop
    Else
     statement = Trim(token & " " & statement)
    End If
    statement = Replace(statement, "MISSOVER", "")
    ''  If InStr(statement, "=") Then
    ''    tagWrite "// " & statement, "INPUTOPTION(i)"
    ''    statement = ""
    ''  End If
    changeTag RTBDataStep, "<<InputDataSet(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\InputDataSet"
    changeTag RTBDataStep, "<<getFullName(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\getFullName"
    changeTag RTBDataStep, "<INPUTDATASET>", UCase(StINPUTDATASET)
    changeTag RTBDataStep, "<inDataset>", StinDataset
   
  Exit Sub
catch:
  Resume Next

End Sub

Private Sub Run()
  Dim i As Integer
  Dim stkeycol As String, stitemcol As String
  Dim rs As Recordset
''  i = 0
  For i = 1 To ColDataField.count
    'OutputData
    stitemcol = ColDataField.item(i)
    If InStr(stitemcol, "[]") = 0 Then
      tagWrite "private " & Split(stitemcol, " - ")(1) & " " & Split(stitemcol, " - ")(0) & ";", "OutputData(i)"
    End If
''    On Error GoTo catch
'    stitemcol = ColDataSetField.item(stkeycol)
''    If stitemcol <> "" Then
''       tagWrite Split(stitemcol, " - ")(1) & " " & Split(stitemcol, " - ")(0) & ";", "OutputData(i)"
''    Else
''       tagWrite "// TODO : change type of variable!!!", "OutputData(i)"
''       tagWrite "String " & stkeycol & ";", "OutputData(i)"
''    End If
   ' ColDataField.Remove stkeycol
  Next
  writeTag "SASVARIABLE(i)", RTBDataStep
  writeTag "OutputData(i)", RTBDataStep
  writeTag "process(i)", RTBDataStep
  writeTag "linkprocess(i)", RTBDataStep
  BolMerge = False
  BolIsData = False
  ''BolIsFile = False
  cleanTags RTBDataStep
  RTBDataStep.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm & "\" & StDataStep & ".java", 1
  If BolIsMacro Then tagWrite StDataStep & "();", "macroprocess(i)"
  StDATASETNAME = ""
  Set ColRename = New Collection
  Set ColRenameInv = New Collection
  Exit Sub
catch:
  stitemcol = ""
  
  Resume Next
End Sub
Private Sub parsePROC(statement As String)
  Dim token As String, stdata As String
  Dim rscol As Recordset
  token = nextToken_newnew(statement)
  Set RTBDataStep = MapsdF_Parser.rText2
  Set ColWorkDataset = New Collection
  'changeTag RTB, "ProcStepName(i)", SrProcName
  statement = UCase(statement)
  Select Case UCase(token)
  Case "SUMMARY", "MEANS", "FREQ"
    RTBDataStep.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\ProcStep"
    changeTag RTB, "<<importClassProc(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\importClassProc"
    changeTag RTB, "<<addProcStep(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\addProcStep"

    If BolIsMacro Then
      CurrentTag = "macroprocess(i)"
    Else
      CurrentTag = "ProcStep(i)"
    End If
    tagWrite "//# PROC " & token & " " & statement

    If InStr(statement, "DATA") Then
      Set ColDataSetField = New Collection
      Set ColDataField = New Collection
      Set ColWorkDataset = New Collection
      statement = Mid(statement, InStr(statement, "=") + 1)
      stdata = nextToken_newnew(statement)
      If InStr(stdata, ".") And InStr(stdata, "WORK.") = 0 Then
        stdata = Split(stdata, ".")(1)
      Else
        stdata = Replace(stdata, "WORK.", "")
        stdata = "WORK." & stdata
      End If
      If InStr(stdata, "WORK.") Then
        Set rscol = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & stdata & "' and PGM = '" & GbNomePgm & "'")
      Else
        Set rscol = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & stdata & "'")
      End If
      
      While Not rscol.EOF
        On Error GoTo catch
        ColDataSetField.Add rscol!Name & " - " & rscol!Type, rscol!Name
        ColWorkDataset.Add rscol!Name & " - " & rscol!Type, rscol!Name
        rscol.MoveNext
      Wend
      rscol.Close
      
      
    End If
  Case "SORT", "COPY", "FORMAT"
    'IntProcNum = IntProcNum + 1
    'SrProcName = token & IntProcNum
    
    If BolIsMacro Then
      CurrentTag = "macroprocess(i)"
    Else
      CurrentTag = "procsort(i)"
    End If
    tagWrite "//# PROC " & token & " " & statement
    If InStr(statement, " OUT") Then
      Set ColDataSetField = New Collection
      statement = Mid(statement, InStr(statement, "=") + 1)
      stdata = nextToken_newnew(statement)
      stdata = IIf(InStr(stdata, "."), Split(stdata, ".")(1), "WORK." & stdata)
      
      If InStr(stdata, "WORK.") Then
        Set rscol = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & stdata & "' and PGM = '" & GbNomePgm & "'")
      Else
        Set rscol = m_Fun.Open_Recordset("select * from SAS_Column where TableName = '" & stdata & "'")
      End If
      
      While Not rscol.EOF
        On Error GoTo catch
        ColDataSetField.Add rscol!Name & " - " & rscol!Type, rscol!Name
        ColWorkDataset.Add rscol!Name & " - " & rscol!Type, rscol!Name
        rscol.MoveNext
      Wend
      rscol.Close
      parseOutputProc statement
    End If
    statement = ""
  Case "DATASETS", "CONTENTS"
    IntProcNum = IntProcNum + 1
    SrProcName = token & IntProcNum
    SrProcName = Replace(SrProcName, "_", "")
    SrProcName = UCase(Chr(Asc(LCase(SrProcName)))) & Right(LCase(SrProcName), Len(LCase(SrProcName)) - 1)
    changeTag RTB, "<<importClassProc(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\importClassProc"
    changeTag RTB, "<<addProcStep(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\addProcStep"

    RTBDataStep.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\DataStepDesc"
    changeTag RTBDataStep, "<BatchName>", LCase(stBatchName)

    CurrentTag = "contents(i)"
    tagWrite "//# PROC " & token & " " & statement
    statement = ""
  Case Else
    IntProcNum = IntProcNum + 1
    SrProcName = token & IntProcNum
    If BolIsMacro Then
      CurrentTag = "macroprocess(i)"
    Else
      CurrentTag = "procsort(i)"
    End If
 
  End Select
  If statement <> "" Then
    'token = nextToken(statement)
    tagWrite "//# PROC " & token & " " & statement
  End If
  Exit Sub
catch:
  Resume Next
  
End Sub

Private Sub RipulisciRiga_SAS(ByRef line As String)
  Dim InizioCommento As Double
  Dim FineCommento As Double
  
  line = Trim(Left(line, 71))
  ' Su MainFrame "dovrebbe" essere corretto troncare a colonna 72, ma su Unix non ci sono limiti
  If Not booEliminaRiga Then
    'Do While InStr(line, "/*")
      InizioCommento = InStr(1, line, "/*")
      If InizioCommento > 0 Then
        FineCommento = InStr(InizioCommento + 2, line, "*/")
        If FineCommento = 0 Then
          FineCommento = InStr(InizioCommento + 2, line, "*")
        End If
        If FineCommento = 0 Then
          booEliminaRiga = True
          FineCommento = Len(line) + 1
        End If
        getComment Mid(line, InizioCommento + 1), CurrentTag
        line = Trim(Mid(line, 1, InizioCommento - 1)) & Trim(Mid(line, FineCommento + 3))
      End If
      
      If InizioCommento = 0 Then
        If InStr(line, "*/") Then
          InizioCommento = 1
          FineCommento = InStr(line, "*/")
          getComment Mid(line, InizioCommento + 1), CurrentTag
          line = Trim(Mid(line, 1, InizioCommento - 1)) & Trim(Mid(line, FineCommento + 3))
        Else
          If Left(line, 1) = "*" Then
            InizioCommento = 1
          Else
            InizioCommento = InStr(line, " *")
          End If
          If InizioCommento > 0 Then
            getComment Mid(line, InizioCommento + 1), CurrentTag
            line = Trim(Mid(line, 1, InizioCommento - 1))    '''' & Trim(Mid(line, FineCommento + 3))
          End If
        End If
''        If InizioCommento > 0 Then
''          getComment Mid(line, InizioCommento + 1), CurrentTag
''          line = Trim(Mid(line, 1, InizioCommento - 1))    '''' & Trim(Mid(line, FineCommento + 3))
''        End If
      End If
    'Loop
  Else
    If InStr(line, "*/") Or InStr(line, "*") Then
      getComment Mid(line, InizioCommento + 1), CurrentTag
      line = Trim(Mid(line, InStr(line, "*/") + 2))
      booEliminaRiga = False
    Else
      line = "*" & line
    End If
  End If
  If Left(line, 1) = "*" Or Left(line, 2) = "/*" Then
    getComment Mid(line, InizioCommento + 1), CurrentTag
    line = ""
  End If
   
End Sub

Private Sub tagWrite(line As String, Optional tagName As String)
  Dim tagValue As String
  
  On Error GoTo catch
  
  If Len(tagName) = 0 Then
    tagName = CurrentTag
  End If
  
  tagValue = tagValues.item(tagName)
  tagValues.Remove tagName
  tagValues.Add tagValue & line & vbCrLf, tagName
  
  Exit Sub
catch:
  If err.Number = 5 Then '424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    tagValues.Add "", CurrentTag
    Resume
  End If
End Sub

Private Sub writeTag(Tag As String, RTB As RichTextBox)
  Dim tagValue As String
  
  On Error GoTo catch
  
  tagValue = tagValues.item(Tag)
  changeTag RTB, "<" & Tag & ">", tagValue
  tagValues.Remove Tag
  tagValues.Add "", Tag
  
  Exit Sub
catch:
  If err.Number = 424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    'tagValues.Add "", Tag
    'Resume
  End If
End Sub


'SILVIA 02-10-2008
''''''''''''''''''''''''''''''''''''''''''''''
' Scrittura Commenti
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getComment(statement As String, Optional tagName As String)
  On Error GoTo catch
  tagWrite "// " & statement, tagName
  Exit Sub
catch:
  writeLog "E", "parsePgm_I: error on getComment!"
End Sub

'T X CAPITA
Sub CreateTmpHibernateMap()
  
  Dim StDATASETNAME As String, stFileName As String, stcolumns As String, stSchema As String
  Dim RTB As RichTextBox, RTB1 As RichTextBox
  
  Set RTB = MapsdF_Parser.rText
  Set RTB1 = MapsdF_Parser.rText1

  Const migrationPath = "Output-prj\Languages\SAS2Java"
  Const templatePath = "Input-prj\Languages\SAS2Java"

  RTB1.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\CreateTmpDs"
  
  Dim rs As Recordset
  stSchema = ""
  StDATASETNAME = ""
  stcolumns = ""
  
  Set rs = m_Fun.Open_Recordset("Select * from SAS_TempDataSet_HBM ORDER BY  TABLENAME, NAME")
  stSchema = LCase(rs!SCHEMA)
  While Not rs.EOF
     If Replace(rs!tableName, "TMP_", "") <> StDATASETNAME Then
     ''If Replace(rs!tableName, "WORK.", "") <> StDATASETNAME Then
    ''If Trim(rs!tableName) <> StDATASETNAME Then
''       If Len(stcolumns) Then
''         stcolumns = Left(stcolumns, Len(stcolumns) - 1)
''         changeTag RTB1, "<COLUMNS>", stcolumns
''         stcolumns = ""
''       End If
       If StDATASETNAME <> "" Then
         changeTag RTB, "<<COLNAME(i)>>", ""
         RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\HBM\" & stSchema & "\" & LCase(StDATASETNAME) & ".hbm.xml", 1
       End If
 '''    RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\HibernateMap"
     RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\HibernateMapTmp"
      StDATASETNAME = Replace(rs!tableName, "WORK.", "")
      StDATASETNAME = Replace(rs!tableName, "TMP_", "")
      changeTag RTB, "<<COLNAME(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\ColName"
      changeTag RTB, "<TEMPDATASET>", StDATASETNAME
      changeTag RTB, "<DSCHEMA>", UCase(stSchema)
      changeTag RTB, "<NAME>", rs!Name
      changeTag RTB, "<TYPE>", IIf(Trim(rs!sqltype) = "DATE", "java.util.Date", "java.lang." & rs!Type)
''      changeTag RTB1, "<<CREATETMPDS(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\CreateTempDs"
''      changeTag RTB1, "<TEMPDATASET>", StDATASETNAME
      stcolumns = rs!Name & " " & rs!sqltype & ","
   Else
      changeTag RTB, "<<COLNAME(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\ColName"
      changeTag RTB, "<NAME>", rs!Name
      changeTag RTB, "<TYPE>", IIf(Trim(rs!sqltype) = "DATE", "java.util.Date", "java.lang." & rs!Type)
      stcolumns = stcolumns & vbCrLf & rs!Name & " " & rs!sqltype & ","
    End If
    stSchema = LCase(rs!SCHEMA)
    rs.MoveNext
  Wend
 ' stSchema = LCase(rs!schema)
  rs.Close
''  If Len(stcolumns) Then
''    stcolumns = Left(stcolumns, Len(stcolumns) - 1)
''    changeTag RTB1, "<COLUMNS>", stcolumns
''    stcolumns = ""
''  End If
  changeTag RTB, "<<COLNAME(i)>>", ""
  RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\HBM\" & stSchema & "\" & LCase(StDATASETNAME) & ".hbm.xml", 1

  'changeTag RTB1, "<<CREATETMPDS(i)>>", ""
  'RTB1.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\TEMPORARY_DOC.sql", 1

End Sub

'''' tilvia DELETE script
Sub SqlDeleteScript()
  Dim StDelete As String
  Dim rs As Recordset

  Dim RTB As RichTextBox
  Set RTB = MapsdF_Parser.rText

  Const migrationPath = "Output-prj\Languages\SAS2Java"
  Const templatePath = "Input-prj\Languages\SAS2Java"
  
  On Error GoTo errorHandler
  RTB.LoadFile m_Fun.FnPathPrj & "\Input-Prj\Template\imsdb\standard\rdbms\ORACLE\SQL_DELETE.sql"
 
  Set rs = m_Fun.Open_Recordset("Select DISTINCT SCHEMA + '.' + TABLENAME AS TB from SAS_DataSet_HBM")
  While Not rs.EOF
  'DELETE FROM  <DSCHEMA>.<TABLE-NAME> ;
   StDelete = StDelete & "DELETE FROM " & rs!tb & " ;" & vbCrLf
   rs.MoveNext
  Wend
  rs.Close
  StDelete = StDelete & "COMMIT;"
  changeTag RTB, "<DELETE>", StDelete
  
  RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\SCRIPT\Delete_all.sql", 1
  
  Exit Sub
  
errorHandler:
  If err.Number = 75 Then
    MkDir m_Fun.FnPathPrj & "\" & migrationPath & "\SCRIPT\" & StDelete
    Resume
  Else
   ' m_Fun.writeLog "Errore in [MadmdM_DB2.DeclareCtl_ORA]. Errore : " & err.Description
    MsgBox err.description
  End If

End Sub

'''' tilvia INSERT script
Sub SqlInsertScript()
  
  Dim StTableName As String, stFileName As String, stSchema As String
  Dim RTB As RichTextBox, RTB1 As RichTextBox
  Dim stcampi As String, stValues As String, stValue As String, BaseString As String, BaseString2 As String
  Dim rand As Integer, Temp As Integer, RandomDate As Date
  Dim i As Integer, j As Integer
  '' prova x generare numeri, stringhe e date casuali
'  Debug.Print "numeri     stringhe     date"
'  For j = 1 To 10
'    Randomize
'    BaseString = ""
'    BaseString2 = ""
'    For i = 1 To 10
'      rand = Int(6 * Rnd())
'      BaseString = BaseString & rand
'      Do
'        Temp = Int(Rnd * 100)
'      Loop While Not (Temp >= 33 And Temp <= 122)
'      BaseString2 = BaseString2 & Chr(Temp)
'    Next i
'    BaseString2 = "'" & BaseString2 & "'"
'    RandomDate = Int(Rnd() * CDbl(Date + 1))
'   Debug.Print BaseString & " " & BaseString2 & " " & RandomDate
'  Next j
  '''
  Set RTB = MapsdF_Parser.rText

  Const migrationPath = "Output-prj\Languages\SAS2Java"
  Const templatePath = "Input-prj\Languages\SAS2Java"
  
  Dim rs As Recordset
  stSchema = ""
  StTableName = ""
  stcampi = ""
  stValues = ""
  stValue = ""
  ' strcampi = Name & " " & Tipo & ","

  ' strcampi = Replace(strcampi, vbCrLf & "<FIELD-POS(i)>", "")
  ' changeTag rtbFile, "<FIELD-POS(i)>", strcampi
  
  On Error GoTo errorHandler
  
  Set rs = m_Fun.Open_Recordset("Select * from SAS_TempDataSet_HBM  WHERE SCHEMA = 'CLAIM_EX' ORDER BY SCHEMA, TABLENAME, NAME ")
  stSchema = rs!SCHEMA
  While Not rs.EOF
    If Trim(rs!tableName) <> StTableName Then
      If StTableName <> "" Then
        stcampi = Left(Trim(stcampi), Len(Trim(stcampi)) - 1)
        changeTag RTB, "<FIELD-NAME(i)>", stcampi
        stValues = Replace(stValues, vbCrLf & "<FIELD-TYPE(i)>", "")
        stValues = Left(Trim(stValues), Len(Trim(stValues)) - 1)
        changeTag RTB, "<FIELD-TYPE(i)>", stValues
        changeTag RTB, "<FIELD-NAME(i)>", ""
        changeTag RTB, "<FIELD-TYPE(i)>", ""
        changeTag RTB, "<DSCHEMA>", stSchema
        RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\SCRIPT\Temporary\Insert_" & StTableName & ".sql", 1
      End If
      RTB.LoadFile m_Fun.FnPathPrj & "\Input-Prj\Template\imsdb\standard\rdbms\ORACLE\SQL_INSERT.sql"
      StTableName = rs!tableName
      changeTag RTB, "<TABLE-NAME>", StTableName
      stcampi = rs!Name & ", "
      If InStr(rs!sqltype, "CHAR") Then
        stValue = Space(rs!Length)
        stValue = "'" & Replace(stValue, " ", "A") & "'" & ", "
      End If
      If InStr(rs!sqltype, "FLOAT") Or InStr(rs!sqltype, "NUMBER") Then
        stValue = Space(rs!Length)
        stValue = Replace(stValue, " ", 1) & ", "
      End If
      If InStr(rs!sqltype, "DATE") Then
        stValue = "to_date(SYSDATE)" & ", "
      End If
      stValues = stValue
      stcampi = Replace(stcampi, vbCrLf & "<FIELD-NAME(i)>", "")
   Else
      stcampi = stcampi & vbCrLf & rs!Name & ", "
      If InStr(rs!sqltype, "CHAR") Then
        stValue = Space(rs!Length)
        stValue = "'" & Replace(stValue, " ", "A") & "'" & ", "
      End If
      If InStr(rs!sqltype, "FLOAT") Or InStr(rs!sqltype, "NUMBER") Then
        stValue = Space(rs!Length)
        stValue = Replace(stValue, " ", 1) & ", "
      End If
      If InStr(rs!sqltype, "DATE") Then
        stValue = "to_date(SYSDATE)" & ", "
      End If
      stValues = stValues & vbCrLf & stValue
    End If
    stSchema = rs!SCHEMA
    rs.MoveNext
  Wend
 ' stSchema = LCase(rs!schema)
  rs.Close
  

  ''On Error GoTo errorHandler
  stcampi = Left(Trim(stcampi), Len(Trim(stcampi)) - 1)
  changeTag RTB, "<FIELD-NAME(i)>", stcampi
  stValues = Replace(stValues, vbCrLf & "<FIELD-TYPE(i)>", "")
  stValues = Left(Trim(stValues), Len(Trim(stValues)) - 1)
  changeTag RTB, "<FIELD-TYPE(i)>", stValues
  changeTag RTB, "<FIELD-NAME(i)>", ""
  changeTag RTB, "<FIELD-TYPE(i)>", ""
  changeTag RTB, "<DSCHEMA>", stSchema
  
  RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\SCRIPT\" & stSchema & "\Insert_" & StTableName & ".sql", 1
  
  Exit Sub
  
errorHandler:
  If err.Number = 75 Then
    MkDir m_Fun.FnPathPrj & "\" & migrationPath & "\SCRIPT\" & stSchema
    Resume
  Else
   ' m_Fun.writeLog "Errore in [MadmdM_DB2.DeclareCtl_ORA]. Errore : " & err.Description
    MsgBox err.description
  End If

End Sub


''''
Sub CreateHibernateCfgXml()
  Dim StPGM As String, stDataset As String, stDatasetTmp As String, stNQ As String
  Dim RTB As RichTextBox, RTB1 As RichTextBox
  Set RTB = MapsdF_Parser.rText
  Const migrationPath = "Output-prj\Languages\SAS2Java"
  Const templatePath = "Input-prj\Languages\SAS2Java"
  Dim rs As Recordset, rs1 As Recordset
  
  StPGM = ""
  stDataset = ""
  stDatasetTmp = ""
  stNQ = ""
  Set rs = m_Fun.Open_Recordset("Select DISTINCT * from SAS_PGM_DS ORDER BY PGM, DATASET")
  
  While Not rs.EOF
    If rs!PGM <> StPGM Then
      If Len(stDataset) Then
        '<!--  PERMANENT DATASET  -->
        stDataset = "<!--  PERMANENT DATASET  -->" & vbCrLf & stDataset
        changeTag RTB, "<PERMANENTDS>", stDataset
        stDataset = ""
      Else
        changeTag RTB, "<PERMANENTDS>", ""
      End If
      If Len(stDatasetTmp) Then
        '<!--  TEMPORARY DATASET  -->
        stDatasetTmp = "<!--  TEMPORARY DATASET  -->" & vbCrLf & stDatasetTmp
        changeTag RTB, "<TEMPDS>", stDatasetTmp
        stDatasetTmp = ""
      Else
        changeTag RTB, "<TEMPDS>", ""
      End If
      If Len(stNQ) Then
        '    <!--  NAMED QUERIES -->
        stNQ = "<!--  NAMED QUERIES -->" & vbCrLf & stNQ
        changeTag RTB, "<NQ>", stNQ
        stNQ = ""
      Else
        changeTag RTB, "<NQ>", ""
      End If
      If StPGM <> "" Then
        Set rs1 = m_Fun.Open_Recordset("Select * from SAS_NQ WHERE NQ = '" & UCase(StPGM) & "'")
        If rs1.RecordCount Then
          stNQ = "<mapping resource=""hibernate/nq/" & LCase(StPGM) & ".nq.xml"" />"
        Else
          stNQ = ""
        End If
        rs1.Close
        RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\HBMCFG\" & LCase(StPGM) & ".cfg.xml", 1
      End If
      RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\HibernateCfg"
      StPGM = rs!PGM
      If Left(rs!DATASET, 5) = "WORK." Then
        stDatasetTmp = " <mapping resource=""hibernate/temporary/" & LCase(Replace(rs!DATASET, "WORK.", "")) & ".hbm.xml"" />"
      Else
        If rs!SCHEMA <> "" Then
          stDataset = stDataset & vbCrLf & " <mapping resource=""hibernate/" & LCase(rs!SCHEMA) & "/" & LCase(rs!DATASET) & ".hbm.xml"" />"
        Else
          stDataset = stDataset & vbCrLf & " <mapping resource=""hibernate/" & LCase(rs!DATASET) & ".hbm.xml"" />"
        End If
      End If
    Else
      If Left(rs!DATASET, 5) = "WORK." Then
        stDatasetTmp = stDatasetTmp & vbCrLf & " <mapping resource=""hibernate/temporary/" & LCase(Replace(rs!DATASET, "WORK.", "")) & ".hbm.xml"" />"
      Else
        If rs!SCHEMA <> "" Then
          stDataset = stDataset & vbCrLf & " <mapping resource=""hibernate/" & LCase(rs!SCHEMA) & "/" & LCase(rs!DATASET) & ".hbm.xml"" />"
        Else
          stDataset = stDataset & vbCrLf & " <mapping resource=""hibernate/" & LCase(rs!DATASET) & ".hbm.xml"" />"
        End If
      End If
    End If
    rs.MoveNext
  Wend
  rs.Close
  If Len(stDataset) Then
    '<!--  PERMANENT DATASET  -->
    stDataset = "<!--  PERMANENT DATASET  -->" & vbCrLf & stDataset
    changeTag RTB, "<PERMANENTDS>", stDataset
    stDataset = ""
  Else
    changeTag RTB, "<PERMANENTDS>", ""
  End If
  If Len(stDatasetTmp) Then
    '<!--  TEMPORARY DATASET  -->
    stDatasetTmp = "<!--  TEMPORARY DATASET  -->" & vbCrLf & stDatasetTmp
    changeTag RTB, "<TEMPDS>", stDatasetTmp
    stDatasetTmp = ""
  Else
    changeTag RTB, "<TEMPDS>", ""
  End If
  If Len(stNQ) Then
    '    <!--  NAMED QUERIES -->
    stNQ = "<!--  NAMED QUERIES -->" & vbCrLf & stNQ
    changeTag RTB, "<NQ>", stNQ
    stNQ = ""
  Else
    changeTag RTB, "<NQ>", ""
  End If
  RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\HBMCFG\" & LCase(StPGM) & ".cfg.xml", 1

End Sub

Sub CreateTmpDatasetXml()

'''  Dim StDATASETNAME As String, stFileName As String, stName As String
     Dim RTB As RichTextBox
     Dim stChar As String, stChar2 As String, stChar3 As String, lLrecl As Long
'''  Dim tagvalues As Collection
'''  Set tagvalues = New Collection
'''
     Set RTB = MapsdF_Parser.rText
     Const migrationPath = "Output-prj\Languages\SAS2Java"
     Const templatePath = "Input-prj\Languages\SAS2Java"

  RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\ContentsExp"


     Dim rs As Recordset
  StDATASETNAME = ""
  Set rs = m_Fun.Open_Recordset("Select * from SAS_TempDataSet_XML")
     While Not rs.EOF
    If rs!tableName <> StDATASETNAME Then
      StDATASETNAME = rs!tableName
      changeTag RTB, "<<COLUMNDESC(i)>>", ""
      changeTag RTB, "<<NAMEDATASET(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\NamedataSet"
      changeTag RTB, "<<COLUMNDESC(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\Columndesc"
      changeTag RTB, "<DATASETNAME>", StDATASETNAME
      changeTag RTB, "<NOME>", rs!Name
      changeTag RTB, "<TIPO>", rs!Type
    Else
      changeTag RTB, "<<COLUMNDESC(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\Columndesc"
      changeTag RTB, "<NOME>", rs!Name
      changeTag RTB, "<TIPO>", rs!Type
    End If
    rs.MoveNext
  Wend
  rs.Close

  RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\XML\TemporaryDataSet.XML", 1

End Sub


Sub CreateParamFiles()

'''  Dim StDATASETNAME As String, stFileName As String, stName As String
     Dim RTB As RichTextBox
     Dim stChar As String, stChar2 As String, stChar3 As String, lLrecl As Long
'''  Dim tagvalues As Collection
'''  Set tagvalues = New Collection
'''
     Set RTB = MapsdF_Parser.rText
     Const migrationPath = "Output-prj\Languages\SAS2Java"
     Const templatePath = "Input-prj\Languages\SAS2Java"
'''
'''  RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\ContentsExp"
'''
'''  tagvalues.Add "", "NAMEDATASET(i)"
'''  tagvalues.Add "", "COLUMNDESC(i)"
'''
     Dim rs As Recordset
'''  StDATASETNAME = ""
'''  Set rs = m_Fun.Open_Recordset("Select * from SAS_TempDataSet_XML")
     Set rs = m_Fun.Open_Recordset("Select * from SAS_Pgm_File_L order by PGM")
     While Not rs.EOF
       RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\Vuoto"
       lLrecl = rs!LRECL
       stChar = Space(lLrecl)
       stChar2 = Space(lLrecl)
       stChar3 = Space(lLrecl)
       stChar = Replace(stChar, " ", "1")
       stChar2 = Replace(stChar2, " ", "2")
       stChar3 = Replace(stChar3, " ", "3")
       RTB.Text = stChar + stChar2 + stChar3
       m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath & "\Test\" & rs!PGM

       RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\Test\" & rs!PGM & "\" & rs!fileName & ".file", 1
'''    If rs!tableName <> StDATASETNAME Then
'''      StDATASETNAME = rs!tableName
'''      changeTag RTB, "<<COLUMNDESC(i)>>", ""
'''      changeTag RTB, "<<NAMEDATASET(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\NamedataSet"
'''      changeTag RTB, "<<COLUMNDESC(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\Columndesc"
'''      changeTag RTB, "<DATASETNAME>", StDATASETNAME
'''      changeTag RTB, "<NOME>", rs!Name
'''      changeTag RTB, "<TIPO>", rs!Type
'''    Else
'''      changeTag RTB, "<<COLUMNDESC(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\Columndesc"
'''      changeTag RTB, "<NOME>", rs!Name
'''      changeTag RTB, "<TIPO>", rs!Type
'''    End If
       rs.MoveNext
     Wend
     rs.Close
'''
'''  RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\XML\TemporaryDataSet.XML", 1

End Sub

