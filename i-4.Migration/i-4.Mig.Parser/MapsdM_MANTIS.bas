Attribute VB_Name = "MapsdM_MANTIS"

'Private Sub ParseSupraSchema(filename As String)
'spostata in modulo di classe
'richiamata da File-Import del base

Public Function TableFromLuv(strLuv As String) As String
  Dim rs As New Recordset
  Set rs = m_Fun.Open_Recordset("select Tabella from DMSUPRA_Luv  where  LUV = '" & strLuv & "'")
  If Not rs.EOF Then
   TableFromLuv = rs!Tabella
  Else
    TableFromLuv = ""
  End If
End Function
Public Function IsThereTable(strTable As String) As Boolean
  Dim rs As New Recordset
  IsThereTable = False
  Set rs = m_Fun.Open_Recordset("select File from DMSUPRA_Tabelle  where File = '" & strTable & "'")
  If Not rs.EOF Then
    IsThereTable = True
  End If
End Function
Function IsThereRecord_PSCom(idOggetto As Long, Tipo As String, OggettoR As String, Component As String) As Boolean
Dim rs As New Recordset
  IsThereRecord_PSCom = False
  Set rs = m_Fun.Open_Recordset("select IDOggettoC from PSCom_Obj where IdOggettoC = " & idOggetto & " and RElazione = '" & Tipo & "' and NomeOggettoR = '" _
                                 & OggettoR & "' and NomeCOmponente = '" & Component & "'")
  If Not rs.EOF Then
    IsThereRecord_PSCom = True
  End If
End Function
Sub parsePgm_I()
  Dim i As Long, j As Long
  Dim wRec As String, statement As String
  Dim token As String, Line As String
  Dim manageMove As String  'Fare globale Parser
  Dim SavLev As Integer, k As Integer
  Dim comando As String, table As String, parameters As String, tokencontinue As String, numLinea As Long, tokentext As String
  Dim OggettoR As String, Component As String, utilizzo As String ' PSCOm_Obj
  On Error GoTo parseErr
  
  'Non leggiamo tutte le volte!
  manageMove = m_Fun.LeggiParam("IMSDC-MOVEYN") = "YES"
    
  GbNumRec = 0            'init
  ReDim entries(100)      'init
  entryIndex = 0          'init
  ReDim builtins(100)     'init
  builtinIndex = 0
  
  Parser.PsConnection.Execute "Delete from PsMnt_Command where IdOggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete from PsCom_Obj where IdOggettoC = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete from Bs_Segnalazioni where IdOggetto = " & GbIdOggetto
  
  
  nFCbl = FreeFile
  Open GbFileInput For Input As nFCbl
  While Not EOF(nFCbl)
  
    While Len(Line) = 0 And Not EOF(nFCbl)
      Line Input #nFCbl, Line
      numLinea = numLinea + 1
      GbOldNumRec = numLinea
      If Len(Line) Then
        token = nexttoken(Line)
        If Not IsNumeric(token) Then
          Line = token & " " & Line
        End If
        Line = Replace(Line, ".", "")
      End If
      If Left(Line, 1) = "|" Then
        Line = ""
      End If
    Wend
    statement = Line
    
    token = nexttoken(statement)
    If token <> "" Then
      comando = ""
      table = ""
      parameters = ""
      Select Case token
       
        Case "VIEW"
         'AC: la LUV � il parametro, la tabella la ricaviamo dalla DMSUPRA_LUV
         ' questa � la logica su cui si basa l'evetuale segnalazione
          comando = "VIEW"
          token = nexttoken(statement)
          If Not token = "ON" Then
            table = token
            token = nexttoken(statement)
            If Not token = "" Then
              If Left(token, 2) = "(""" And (Right(token, 2) = """)" Or Right(token, 2) = "))") Then
                parameters = Mid(token, 3, Len(token) - 4)
                If InStr(parameters, ",") > 0 Then
                  parameters = Replace(Left(parameters, InStr(parameters, ",") - 1), """", "")
                End If
              End If
            End If
            utilizzo = "VIEW"
            If Not parameters = "" Then
              OggettoR = parameters
            Else
              OggettoR = table
            End If
            If Not parameters = "" Then
              Component = TableFromLuv(Replace(parameters, "_", "-"))
            Else
              Component = TableFromLuv("T1AV-" & Replace(table, "_", "-"))
            End If
            If Component = "" Then
              MapsdM_Parser.addSegnalazione "T1AV-" & Replace(table, "_", "-"), "NOSUPLUV", ""
            Else
              'controlla esistenza tabella
              If Not IsThereTable(Component) Then
                MapsdM_Parser.addSegnalazione Component, "NOSUPTABLE", ""
              End If
            End If
            IsToUpdate = True
          End If
        Case "GET"
          comando = "GET"
          token = nexttoken(statement)
          table = token
          token = nexttoken(statement)
          If Not token = "" Then
            If Left(token, 1) = "(" And Right(token, 1) = ")" Then
              parameters = Mid(token, 2, Len(token) - 2)
            ElseIf Left(token, 1) = "(" Then
              Line Input #nFCbl, Line
              numLinea = numLinea + 1
              GbOldNumRec = numLinea
              If Len(Line) Then
                tokentext = nexttoken(Line)
                If Not IsNumeric(tokentext) Then
                  Line = tokentext & " " & Line
                End If
                Line = Replace(Line, ".", "")
              End If
              If Left(Line, 1) = "'" Then
                statement = token & Mid(Line, 2)
                
                token = nexttoken(statement)
              
                If Left(token, 1) = "(" And Right(token, 1) = ")" Then
                  parameters = Mid(token, 2, Len(token) - 2)
                End If
              End If
            End If
            utilizzo = "GET"
            OggettoR = table
            Component = TableFromLuv("T1AV-" & Replace(table, "_", "-"))
            If Component = "" Then
              MapsdM_Parser.addSegnalazione "T1AV-" & Replace(table, "_", "-"), "NOSUPLUV", ""
            Else
              'controlla esistenza tabella
              If Not IsThereTable(Component) Then
                MapsdM_Parser.addSegnalazione Component, "NOSUPTABLE", ""
              End If
            End If
          End If
          IsToUpdate = True
        Case "UPDATE"
          comando = "UPDATE"
          token = nexttoken(statement)
          table = token
          utilizzo = "UPDATE"
          OggettoR = table
          'OggettoR = ""
          Component = table
          If Not IsThereTable(Component) Then
              MapsdM_Parser.addSegnalazione Component, "NOSUPTABLE", ""
          End If
          IsToUpdate = True
        Case "INSERT"
          comando = "INSERT"
          token = nexttoken(statement)
          table = token
          utilizzo = "INSERT"
          OggettoR = table
          'OggettoR = ""
          Component = table
          If Not IsThereTable(Component) Then
              MapsdM_Parser.addSegnalazione Component, "NOSUPTABLE", ""
          End If
          IsToUpdate = True
        Case "DELETE"
          comando = "DELETE"
          token = nexttoken(statement)
          table = token
          utilizzo = "DELETE"
          OggettoR = table
          'OggettoR = ""
          Component = table
          If Not IsThereTable(Component) Then
              MapsdM_Parser.addSegnalazione Component, "NOSUPTABLE", ""
          End If
          IsToUpdate = True
        Case Else
          'LABEL:
          'If Right(token, 1) = ":" Then
          '  token = nextToken(statement)
          '  If token = "PROC" Then
          '    parsePROC statement
          '  End If
          'Else
            If manageMove Then
              
            End If
          'End If
          
          Line = ""
      End Select
    End If
    Line = ""
    If IsToUpdate Then
      IsToUpdate = False
      Parser.PsConnection.Execute "INSERT INTO PsMnt_Command (IdOggetto,Riga,[Command],[Table],[Parameters]) VALUES (" & GbIdOggetto & "," & numLinea & ",'" & comando & "','" & table & "','" & parameters & "')"
      If Not IsThereRecord_PSCom(GbIdOggetto, "SUP", OggettoR, Component) Then
        Parser.PsConnection.Execute "INSERT INTO PsCom_Obj (IdOggettoC,Relazione,NomeOggettoR,NomeComponente,Utilizzo) VALUES (" & GbIdOggetto & ",'SUP','" & OggettoR & "','" & Component & "','" & utilizzo & "')"
      End If
    End If
  Wend
  Close #nFCbl
  
  Exit Sub
parseErr:
  If err.Number = 123 Then ' lettura token su righe che non ci interessano
    token = ""
    Resume Next
  Else
    Close #nFCbl
  End If
  End Sub
  
