Attribute VB_Name = "MapsdM_EASYTRIEVE"
Option Explicit
Option Compare Text

Const migrationPath = "Output-prj\Languages\Ezt2Cobol"
Const templatePath = "Input-prj\Languages\Ezt2Cobol"

Type eztData
  fieldName As String
  fieldType As String
  fieldValue As String
  fieldPosition As Integer
  location As String
  attributes As String
  groupName As String
  Index As String
  Offset As Integer
  Occurs As Integer
  ' Perch� erano a string?!?!
  bytes As Long
  dataLen As Long
  dataDec As Long
  Segno As String
  'Mauro 12/07/2007 : i seguenti campi gestiscono i campi di gruppo per il "livellamento" COBOL
  livello As String ' Livello Cobol
  byteBusy As Long ' Gestione dei bytes occupati dai sottocampi
  Redefines As String ' Contiene il nome del campo ridefinito
  isGroup As Boolean ' Identifica se � un campo di gruppo
End Type

'SQ - Attenzione alla RAM... valutare...
Dim dataList(10000) As eztData
Dim listIndex As Integer

Type cblData
  Name As String
  isGroup As Boolean
  dataType(2) As String
  dataLen As String
  dataDec As String
  bytes As String
  modifierValue As String
  value As String
  level As String
  Segno As String
  'SILVIA
  Redefines As String
  Occurs As Integer
End Type

Dim CurrentFILE As String, CurrentPROC As String
Dim CurrentLevel As Integer, CurrentBytes As Integer
'tmp?
Dim idArea As Long
Dim lOrdinale As Long
Dim reserverdWords() As String
Dim equOrdinale As Integer
Dim indentlevel As Integer
Const indentBase = 4
Const indentOffset = 2
Dim TmpIDMS As Integer
Dim RTB As RichTextBox
Dim tagValues As Collection
Dim CurrentJOB As Integer
Dim CurrentTag As String
Dim openJOB As Boolean
Dim DataLevel As Integer
Dim ReportName As String
Dim openREPORT As Boolean
Type openFiles
  fileName As String
  openInput As Boolean
  filetype As String
End Type
Dim fileEZT() As openFiles
Dim fileName() As String
  'silvia 22-04-2008
Dim oldfieldpos As Integer
Dim BolRetCode As Boolean
Dim ColMask As Collection
Dim BolSql As Boolean
'serve per costruire la fetch del cursore
Dim StINTO As String
'silvia
Dim StHEADING As Collection
Dim arrHeadKey() As String
Dim ColIndex As Collection
Dim arrColIndex() As String
Dim arrFilePrinter() As String
Dim arrNameFieldLikeFile() As String
Public operator As String
Dim isSection As Boolean
Dim NumReport As Integer

Private Function CheckReservedCBL(source As String) As String
Dim rs As Recordset, i As Integer
CheckReservedCBL = source
If Not InStr(source, ",") > 0 And Not InStr(source, "'") > 0 Then
  Set rs = m_Fun.Open_Recordset("select CBL_Translation from psPLI_WordsTranslation where PLI_Word = '" & UCase(Trim(source)) & "'")
  If Not rs.EOF Then
    CheckReservedCBL = rs!CBL_Translation
  Else
    For i = 1 To UBound(arrNameFieldLikeFile)
      If arrNameFieldLikeFile(i) = source Then
        CheckReservedCBL = "B-" & source
      End If
    Next i
  End If
End If
rs.Close

End Function
'''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''
Public Sub parsePgm_I()
  Dim fdIN As Long, fdOUT As Long
  Dim line As String, token As String, execStatement As String, statement As String
  Dim rsOggetti As Recordset
  Dim manageMove As String
  Dim previousTag As String
  'Non leggiamo tutte le volte!
  manageMove = m_Fun.LeggiParam("IMSDC-MOVEYN") = "YES"
  
  ReDim arrHeadKey(0)
  ReDim arrColIndex(0)
  ReDim CampiMove(0)
  ReDim CampiMoveCpy(0)
  ReDim DataValues(0)
  ReDim CmpIstruzione(0)
  ReDim arrFilePrinter(0)
  ReDim arrNameFieldLikeFile(0)
  NumReport = 0
  GbNumRec = 0    'init
  idArea = 0
  lOrdinale = 0
  CurrentFILE = ""
  indentlevel = 0
  'tmp
  TmpIDMS = 0
  CurrentJOB = 0
  'CurrentTag = ""
  listIndex = 1
  openREPORT = False
  ReDim fileEZT(0)
  previousTag = CurrentTag
  'silvia
  BolRetCode = False
  BolSql = False
  Set ColMask = New Collection
  Set StHEADING = New Collection
  Set ColIndex = New Collection

  On Error GoTo fileErr
  
  fdIN = FreeFile
  Open GbFileInput For Input As fdIN
  
  On Error GoTo parseErr
  ''''''''''''''''''''''''''''''
  ' Init
  ''''''''''''''''''''''''''''''
  Parser.PsConnection.Execute "DELETE * FROM PsData_Area WHERE idoggetto=" & GbIdOggetto
  Parser.PsConnection.Execute "DELETE * FROM PsData_Cmp WHERE idoggetto=" & GbIdOggetto
  Parser.PsConnection.Execute "DELETE * FROM PsDli_Istruzioni WHERE idoggetto=" & GbIdOggetto
  
  ' Mauro 19/07/2007 : Gestione Macro su EasyTrieve
  Parser.PsConnection.Execute "DELETE * FROM PsMacro WHERE idoggetto=" & GbIdOggetto
  Parser.PsConnection.Execute "DELETE * FROM PsPgm_MACRO WHERE idpgm=" & GbIdOggetto
  
  'reserverdWords = ...

  While Not EOF(fdIN)
    DoEvents  'valutare se tenere

    'Costruzione statement: gestione commenti+multilinea
    statement = IIf(Len(line), line, getStatement(fdIN, fdOUT, line))
    'statement = Replace(statement, "_", "-")
    If statement = "STOP" Then
      statement = "STOP"
    End If
    'LINEA VALIDA:
    If Left(statement, 1) = "%" Then
      'VERIFICARE: Evitare di concatenare in una struttura campi che non centrano niente
      If listIndex > 1 Then
        getLivelliCobol
      End If
      listIndex = 1
      getMacro statement
    Else
      Dim statementS As String
      Dim tokenS As String
      token = nextToken_new(statement) 'MANTIENE GLI APICI!!
      Select Case token
        Case "JOB"
          'Mauro:
          If listIndex > 1 Then
            getLivelliCobol
          End If
          listIndex = 1
          tokenS = ""
          'SQ
          If Ezt2cobol Then
            writeTag CurrentTag
            previousTag = CurrentTag
            CurrentTag = "JOB-ACTIVITIES(i)"
            parseJOB statement
          Else
            CurrentTag = ""
          End If
          CurrentFILE = ""
          line = ""
        Case "SORT"
          'Mauro:
          If listIndex > 1 Then
            getLivelliCobol
          End If
          listIndex = 1
          tokenS = ""
          'SQ
          If Ezt2cobol Then
          '  writeTag CurrentTag
            previousTag = CurrentTag
            CurrentTag = "JOB-ACTIVITIES(i)"
            statement = "*" & "## STATEMENT: " & token & statement
            tagWrite statement
          End If
          CurrentFILE = ""
          ' Penso che qui dovrebbe esserci un parseSORT o simile: da gestire
          line = ""
        Case "DEFINE"
          'SILVIA 02-02-2009
          If Len(CurrentFILE) Then
            If listIndex > 1 Then
              getLivelliCobol
            End If
            listIndex = 1
            getDEFINE statement
          Else
            getDEFINE statement
            'If listIndex > 1 Then
            '  getLivelliCobol
            'End If
            'listIndex = 1
            ' Mauro 17/07/2007 : L'ho messo io!!!!
            tokenS = ""
          End If
        Case "DLI"
          getDLI statement, fdOUT
        Case "SQL"
          ' SQL INCLUDE FROM CREATOR.TABLE ..
          If Ezt2cobol Then getSQL statement
        Case "FILE"
          line = ""
          If listIndex > 1 Then
            getLivelliCobol
          End If
          listIndex = 1
          getFILE fdIN, statement, line
        Case "RECORD"
          getRECORD statement
        Case "RETRIEVE"
          getRETRIEVE statement, fdOUT
        Case "CALL"
          getCALL fdOUT, statement
        Case "MACRO"
          getMacroParameters statement
'        Case "MOVE"
'          If manageMove Then
'            'TMP: fare... � come il COBOL...
'            getMove statement, fdIN, CampiMove
'          Else
'            line = ""
'          End If
           
        Case "SKIP"
        Case "END"
          'SILVIA
          line = ""
        Case "REPORT"
          Dim appoStatement As String
          tokenS = ""
          line = ""
          NumReport = NumReport + 1
          appoStatement = statement
          parserREPORT fdIN, fdOUT, statement
          statement = appoStatement
          'If Ezt2cobol Then
            'getREPORT fdIN, fdOUT, statement
            'Line = statement
          'End If
        Case "PRINT"
          If Ezt2cobol Then
            getPRINT statement
          End If
        'Case "NEWPAGE"
        '  tokenS = ""
        '  line = ""
        Case "IF"
        '  tokenS = ""
        '  line = ""
        '*****************************
          'SILVIA
          If Ezt2cobol Then
            parseStatement fdOUT, statement, token
          End If
        'SILVIA
        Case "ELSE-IF"
          If Ezt2cobol Then
            indentlevel = indentlevel - 1
            tagWrite indent & "ELSE" & vbCrLf
            line = "IF " & " " & statement
          End If
        'SILVIA
        Case "SEARCH"
          If Ezt2cobol Then
            GetSearch statement
          End If
        Case ""
          'nop
          If Ezt2cobol Then
            tagWrite ""
          End If
        Case Else
          If Len(CurrentFILE) Or CurrentTag = "WORKING-STORAGE(i)" Then
            'If Ezt2cobol Then tagWrite token & " " & statement
            If Left(statement, 2) = "W " Or Left(statement, 2) = "S " Then
              line = ""
              'SILVIA
              If listIndex > 1 Then
                getLivelliCobol
              End If
              listIndex = 1
            End If
            CurrentTag = "WORKING-STORAGE(i)"
            If statement <> "" Then
              getDEFINE statement, token, True
            End If
          Else
            If Left(statement, 2) = "W " Or Left(statement, 2) = "S " Then
              statementS = statement
              tokenS = token
              CurrentTag = "WORKING-STORAGE(i)"
              getDEFINE statement, token
            ElseIf Len(tokenS) And InStr(statement, tokenS) > 0 Then
              CurrentTag = "WORKING-STORAGE(i)"
              getDEFINE statement, token, True
            Else
              If Ezt2cobol Then
                parseStatement fdOUT, statement, token
                line = "" 'ci vuole?! (solo qui)?
              Else
                'Va bene qui?!
                If manageMove Then
                  Dim lValue As String, rValue As String
                  If Left(statement, 1) = "=" Then
                    lValue = token
                    rValue = Trim(Mid(statement, 2))
                    getEztMove lValue, rValue, CampiMove
                  End If
                End If
              End If
            End If
          End If
      End Select
    End If
  Wend
  If listIndex > 1 Then
    getLivelliCobol
  End If
  Close fdIN
  'TODO spostare qui la parte di gestione report che prende dati dalle tabelle
  'facendo attenzione alla variabile Current Tag
  
  If Ezt2cobol Then
    'ciclo su tabelle per elenco report
    Dim rs As Recordset
    Set rs = m_Fun.Open_Recordset("Select IDreport,REportName,Printer from PSEZ_Report where IdOggetto = " & GbIdOggetto)
    While Not rs.EOF
      ReportName = rs!ReportName
      getREPORT_ParametersTable rs!ReportName, IIf(IsNull(rs!Printer), "", rs!Printer)
      getREPORTTable (rs!IdReport)
      rs.MoveNext
    Wend
    'TODO controllare che tutti i file definiti come PRINTER siano stati associati ad un report
    ' arrFilePrinter
  End If
  
  
  ' Mauro 04/12/2007 : Sono a fine file! Adesso posso mettere le OPEN dei FILE
  Dim a As Long
  For a = 1 To UBound(fileEZT)
    If fileEZT(a).filetype = "SQL" Then
      changeTag RTB, "<OPEN-FILES(i)>", "EXEC SQL OPEN " & fileEZT(a).fileName & " END-EXEC." & vbCrLf & _
                     "IF SQLCODE <> 0 " & vbCrLf & _
                     "   STRING '" & fileEZT(a).fileName & " OPEN ERROR, SQLCODE='   SQLCODE" & vbCrLf & _
                     "   INTO ABEND-MESSAGE" & vbCrLf & _
                     "   MOVE SQLCODE TO ABEND-REQUEST" & vbCrLf & _
                     "   PERFORM ERROR-SESSION " & IIf(isSection = False, "THRU ERROR-SESSION-EX", "") & vbCrLf & _
                     "END-IF"

      
    ElseIf fileEZT(a).openInput Then
      changeTag RTB, "<OPEN-FILES(i)>", "OPEN INPUT " & fileEZT(a).fileName & vbCrLf & _
                     "IF NOT OK-" & fileEZT(a).fileName & vbCrLf & _
                     "   STRING '" & fileEZT(a).fileName & " OPEN ERROR, STATUS=' " & fileEZT(a).fileName & "-STATUS" & vbCrLf & _
                     "   INTO ABEND-MESSAGE" & vbCrLf & _
                     "   MOVE " & fileEZT(a).fileName & "-STATUS TO ABEND-REQUEST" & vbCrLf & _
                     "   PERFORM ERROR-SESSION " & IIf(isSection = False, "THRU ERROR-SESSION-EX", "") & vbCrLf & _
                     "END-IF"
    Else
      changeTag RTB, "<OPEN-FILES(i)>", "OPEN OUTPUT " & fileEZT(a).fileName & vbCrLf & _
                     "IF NOT OK-" & fileEZT(a).fileName & vbCrLf & _
                     "   STRING '" & fileEZT(a).fileName & " OPEN ERROR, STATUS=' " & fileEZT(a).fileName & "-STATUS" & vbCrLf & _
                     "   INTO ABEND-MESSAGE" & vbCrLf & _
                     "   MOVE " & fileEZT(a).fileName & "-STATUS TO ABEND-REQUEST" & vbCrLf & _
                     "   PERFORM ERROR-SESSION " & IIf(isSection = False, "THRU ERROR-SESSION-EX", "") & vbCrLf & _
                     "END-IF"
    End If
  Next a
    
  If Not Ezt2cobol Then
    Set rsOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
    rsOggetti!DtParsing = Now
    rsOggetti!ErrLevel = GbErrLevel
    rsOggetti!parsinglevel = 1
    rsOggetti!Cics = SwTpCX
    rsOggetti!Batch = SwBatch
    rsOggetti!DLI = SwDli 'sarebbe IMS!
    rsOggetti!WithSQL = SwSql
    rsOggetti!VSam = SwVsam
    rsOggetti!mapping = SwMapping
    rsOggetti.Update
    rsOggetti.Close
  End If
  
  ' parameter move
  If manageMove Then
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    
    For i = 1 To UBound(CampiMove)
    'stefano: pezza da rimettere a posto da parte di Matteo...
      CampiMove(i).NomeCampo = Replace(CampiMove(i).NomeCampo, "'", "")
      Parser.PsConnection.Execute "Update PsData_Cmp Set Moved = true where Nome = '" & CampiMove(i).NomeCampo & "' and (IdOggetto = " & GbIdOggetto & " or IdOggetto in (Select IdOggettoR from PsRel_Obj where IdOggettoC = " & GbIdOggetto & " and Relazione = 'CPY') )"
    Next
     
    'AC  scrittura tabelle  campimoved/valori
    Dim response As String
    Dim retvalue As Integer
    Dim IdOggettoCmp As Long
    Dim Ordinale As Integer
    For i = 1 To UBound(CmpIstruzione) ' alla fine la struttura mi serve solo
                                       ' per fissare le istruzioni non risolte
      ' verifichiamo se istruzione risolta
     If CmpIstruzione(i).Risolta = 0 Then
      response = risolviIstruzioneByMove(CmpIstruzione(i).CampoIstruzione, retvalue, CmpIstruzione(i).LastSerchIndex)
      If Len(response) Then
      ' update Psdli_Istruzioni, delete segnalazione
        'spli: C'ERA UN APICE DI TROPPO NELLA QUERY E GLI APICI IN response, COME FACEVA A FUNZIONARE?
        response = Replace(response, "'", "")
        Parser.PsConnection.Execute "Update PsDLI_Istruzioni Set Istruzione = '" & response _
                                  & "' where IdOggetto =" & CmpIstruzione(i).Oggetto & " and Riga =" _
                                  & CmpIstruzione(i).Riga
        Parser.PsConnection.Execute "Delete from Bs_Segnalazioni where IdOggetto = " _
                                   & CmpIstruzione(i).Oggetto & " and Riga = " & CmpIstruzione(i).Riga _
                                   & " and ( Codice = 'H09' Or Codice = 'F09')"
       End If
     End If
    Next
    For j = 1 To UBound(CampiMove)
     ' aggiornamento tabella campi  PsData_CmpMoved
        ' AreaFromCampo GbIdOggetto, CampiMove(j).NomeCampo, IdOggettoCmp, IdArea, Ordinale
      For k = 1 To CampiMove(j).Valori.count
        Parser.PsConnection.Execute "Insert Into PsData_CmpMoved (IdPgm,Nome,valore) Values (" _
                                  & GbIdOggetto & ",'" & CampiMove(j).NomeCampo & "','" & Replace(Left(CampiMove(j).Valori.item(k), 255), "'", "''") & "')"
      Next
    Next
  End If
    
  
  'Mauro 16/07/2007: Temporaneo - Creo delle copy fittizie dalla psdata_cmp per vedere se il parser degli Easytrieve � corretto
  'CreaCopyEZT
  
  Exit Sub
parseErr:  'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #fdIN, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #fdIN, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          GbOldNumRec = GbNumRec
          addSegnalazione line, "P00", line
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        GbOldNumRec = GbNumRec
        addSegnalazione line, "P00", line
        'm_Fun.WriteLog "parsePgm_I: id#" & GbIdOggetto & ", riga:  " & GbNumRec & "###" & Err.Description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    Resume Next
  ElseIf err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
    m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
  Else
    GbOldNumRec = GbNumRec
    'addSegnalazione line, "P00", line
    Close fdIN
  End If
  Exit Sub
fileErr:
  If err.Number = 52 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath
    Resume
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
  End If
End Sub

Private Sub CreaCopyEZT()
Dim rsCmp As Recordset
Dim NumFile As Integer
Dim RecCmp As String
Dim nomeCmp As String
  
  Set rsCmp = m_Fun.Open_Recordset("select * from PsData_Cmp where idoggetto = " & GbIdOggetto & " order by codice")
  If rsCmp.RecordCount Then
    NumFile = FreeFile
    Open "C:\customer\COPY EZT\CopyEZT_" & GbIdOggetto & ".cpy" For Output As NumFile
    Do Until rsCmp.EOF
      RecCmp = ""
      nomeCmp = Replace(rsCmp!nome, "/", "-")
      nomeCmp = Replace(nomeCmp, ".", "-")
      nomeCmp = Replace(nomeCmp, "#", "C")
      Select Case nomeCmp
        Case "DESTINATION", "STATUS", "CURRENCY"
          nomeCmp = nomeCmp & "-1"
      End Select
      RecCmp = Space(6 + CInt(rsCmp!livello)) & format(rsCmp!livello, "00") & " " & Mid(nomeCmp, 1, 31)
      If Not rsCmp!Tipo = "A" Then
        Select Case True
          Case Len(RecCmp) < 50
            RecCmp = RecCmp & Space(50 - Len(RecCmp)) & "PIC "
          Case Len(RecCmp) >= 50 And Len(RecCmp) <= 60
            RecCmp = RecCmp & Space(60 - Len(RecCmp)) & "PIC "
          Case Len(RecCmp) > 60
            Print #NumFile, RecCmp
            RecCmp = Space(10) & "PIC"
          End Select
        ' Lunghezza
        Select Case rsCmp!Lunghezza
          Case 1
            RecCmp = RecCmp & rsCmp!Tipo
          Case Is > 1
            RecCmp = RecCmp & rsCmp!Tipo & "(" & rsCmp!Lunghezza & ")"
        End Select
        ' Decimali
        Select Case rsCmp!Decimali
          Case 1
            RecCmp = RecCmp & "V" & rsCmp!Tipo
          Case Is > 1
            RecCmp = RecCmp & "V" & rsCmp!Tipo & "(" & rsCmp!Decimali & ")"
        End Select
        Select Case rsCmp!Usage
          Case "BNR"
            RecCmp = RecCmp & " COMP"
          Case "PCK"
            RecCmp = RecCmp & " COMP-3"
        End Select
      End If
      If rsCmp!Occurs Then
        RecCmp = RecCmp & " OCCURS " & rsCmp!Occurs
      End If
      If Len(rsCmp!nomeRed) Then
        RecCmp = RecCmp & " REDEFINES " & rsCmp!nomeRed
      End If
      RecCmp = RecCmp & "."
      
      Print #NumFile, RecCmp
      rsCmp.MoveNext
    Loop
    Close NumFile
  End If
  rsCmp.Close
End Sub

Sub getEztMove(campo As String, variable As String, ByRef pcampiMove() As UtCampiMove)
  Dim i As Integer, j As Integer

  For i = 1 To UBound(pcampiMove)
    If pcampiMove(i).NomeCampo = campo Then
        Exit For
      End If
  Next
  
  If i = UBound(pcampiMove) + 1 Then ' campo nuovo
      ReDim Preserve pcampiMove(UBound(pcampiMove) + 1)
      pcampiMove(UBound(pcampiMove)).NomeCampo = Replace(campo, ",", "")
      
      Set pcampiMove(UBound(pcampiMove)).Valori = New Collection
      pcampiMove(UBound(pcampiMove)).Valori.Add variable
  Else
    For j = 1 To pcampiMove(i).Valori.count
      If pcampiMove(i).Valori.item(j) = variable Then
        Exit For
      End If
    Next
    If j = pcampiMove(i).Valori.count + 1 Then
      pcampiMove(i).Valori.Add variable
    End If
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Analizza le CALL.
' - Allestisce la PsCall (NUOVA!)
' - Invoca checkRelation
' Effetto collaterale: puo' valorizzare "line" per il giro successivo
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getCALL(fdOUT As Long, line As String)
  Dim statement As String, program As String, token As String
  Dim endStatement As Boolean
  
  On Error GoTo callErr
  'stefano: probabilmente prima funzionava in un altro modo, ma ora devo fare cos�
  'GbOldNumRec = GbNumRec  'linea iniziale
  GbOldNumRec = GbNumRec - fdOUT + 1 'linea iniziale
  ''''''''''''''''''''''''''''''''''
  ' Becco tutte le linee di call...
  ''''''''''''''''''''''''''''''''''
  statement = line
  
  ''''''''''''''''''''''''''''''
  ' Modulo Chiamato:
  ''''''''''''''''''''''''''''''
  'program = Replace(nextToken(statement), ".", "")
  program = nexttoken(statement)
  
  If Not Ezt2cobol Then
    '''''''''''''''''''''''''''''''''''''''''''''''''
    ' Inserimento istruzione in PsCall: (trasformare in execute INSERT)
    '''''''''''''''''''''''''''''''''''''''''''''''''
    Dim rsCall As Recordset
    Set rsCall = m_Fun.Open_Recordset("select * from PsCall")
    rsCall.AddNew
    rsCall!idOggetto = GbIdOggetto
    rsCall!Riga = GbOldNumRec
    rsCall!programma = program  'attenzione: puo' contenere apici...
    rsCall!parametri = statement
    rsCall!linee = fdOUT
    rsCall.Update
    rsCall.Close
    
    ''''''''''''''''''''''''''''''''''''''
    ' Relazioni/Segnalazioni
    ''''''''''''''''''''''''''''''''''''''
    'SQ 7-06-07: CHI HA TOLTO QUESTO!!!!!!!!!!!!!!??????????????
    checkRelations program, "CALL"
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Relazione programma/PSB
    ' (riconosce le istruzioni e le inserisce nella PsDli_Istruzioni
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If isDliCall Then 'settato dalla checkRelation
      ''''SQ mettere a posto!!!!!!!! checkPSB statement
    End If
  
  Else
    'SILVIA
    'tagWrite indent & "CALL '" & program & "' USING " & statement
    If program = "EZTPX01" Then
      statement = Replace(statement, "USING", "")
      statement = Replace(statement, "(", "")
      statement = Replace(statement, ")", "")
      token = nexttoken(statement)
      changeTag RTB, "<LINKAGE(i)>", "01 JCL-PARM." & vbCrLf & _
                                     "  03 LEN-PARM    PIC 9(4) COMP." & vbCrLf & _
                                     "  03 DATA-PARM   PIC X(80)."
      changeTag RTB, "<USING(i)>", "USING JCL-PARM"
      tagWrite indent & "MOVE JCL-PARM(1:LEN-PARM + 2) TO " & statement
    Else
      tagWrite indent & "CALL '" & program & "' " & statement
    End If
  End If
  Exit Sub
callErr:
  'gestire...
  If err.Number = -2147217887 Then  'chiave duplicata...
    '2 CALL su una linea...
  Else
    err.Raise 124, "getCall", "syntax error on " & statement
    'Resume
  End If
End Sub
''''''''''''''''''''''''''''''''''
' Mangia i commenti "multi-linea"
''''''''''''''''''''''''''''''''''
Private Sub getComment(FD As Long, line As String)
  'TMP: non gestisce commenti innestati o "*/" dentro "literal"...
  'While InStr(line, "*/") = 0 And Not EOF(FD)
  '  Line Input #FD, line
  '  GbNumRec = GbNumRec + 1
  'Wend
  'If InStr(line, "*/") Then line = Mid(line, InStr(line, "*/") + 1)
End Sub
Private Function isReservedWord(word As String) As Boolean
  Dim i As Integer
  
  For i = 0 To UBound(reserverdWords)
    If word = reserverdWords(i) Then Exit For
  Next
  
  isReservedWord = i <= UBound(reserverdWords)
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' E' una COPIA! SE RIMANE UGUALE UTILIZZARE QUELLA DEL Parser!
'
' Rendere definitiva (X TUTTI I TIPI DI RELAZIONE)
' Gestisce: CALL/COPY
' - AggiungiRelazione (PSB!)
' - addSegnalazione
' Effetti collaterali:
' - setta idOggettoRel
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub checkRelations(item As String, relType As String)
  Dim rsOggetti As Recordset
  
  isDliCall = False
  
  'Da verificare
  itemValue = item
  
  Select Case relType
    Case "CALL"
      'SQ 21-11-07
      If item = "QJBTDLI" Or item = "DLI" Then
        SwDli = True        'programma DLI
        isDliCall = True    'istruzione DLI: serve al chiamante (e basta)
      Else
        ''''''''''''''''''''''''''''''''''
        ' SQ 15-02-06
        ' Gestione PsLoad
        ' Sostituire OVUNQUE la query commentata con il blocco sotto
        ''''''''''''''''''''''''''''''''''
        'OLD:
        'Set rsOggetti = m_Fun.Open_Recordset("select * from Bs_oggetti where (tipo = 'CBL' or tipo = 'ASM' or tipo = 'PLI') and nome = '" & itemValue & "'")
        'NEW:
        Set rsOggetti = m_Fun.Open_Recordset("select idOggetto,tipo from bs_oggetti where " & _
                        "nome = '" & itemValue & "' and tipo in ('CBL','ASM','PLI','EZT')")
        If rsOggetti.RecordCount = 0 Then
          'Nome LOAD diverso da nome SOURCE: aggiorniamo il recordset
          Set rsOggetti = m_Fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where " & _
                          "b.load = '" & itemValue & "' and b.source = a.nome and a.tipo in ('CBL','ASM','PLI','EZT')")
        End If
        
        If rsOggetti.RecordCount Then
          If rsOggetti.RecordCount = 1 Then
            AggiungiRelazione rsOggetti!idOggetto, relType, itemValue, itemValue
          Else
            'verificare 'sta storia...
            While Not rsOggetti.EOF
              'Inserisce le relazioni come duplicate nella tabella
              AggiungiRelazione rsOggetti!idOggetto, relType, itemValue, itemValue, True
              rsOggetti.MoveNext
            Wend
          End If
        Else
           If Not ExistObjSyst(itemValue, "PGM") Then
             addSegnalazione itemValue, "NO" & relType, itemValue
           Else
             AggiungiComponente itemValue, rsOggetti!idOggetto, "UTL", "SYSTEM"
           End If
        End If
        rsOggetti.Close
      End If
    Case "QUIK-INCLUDE"
      itemValue = item
      IdOggettoRel = 0  'init
      'SERVE UN TIPO SPECIFICO????? CHE OGGETTI SONO????
      Set rsOggetti = m_Fun.Open_Recordset("select idOggetto from Bs_oggetti where (tipo = 'QIM') and nome = '" & itemValue & "'")
      If rsOggetti.RecordCount Then
        IdOggettoRel = rsOggetti!idOggetto  'serve al getFD
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ: 09-02-06 CONTROLLARE...
        'If rsOggetti.RecordCount = 1 Then
        '  AggiungiRelazione idOggettoRel, "COPY", item, item
        'Else
          While Not rsOggetti.EOF
            'Inserisce le relazioni come duplicate nella tabella
            AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue ''''''', True
            rsOggetti.MoveNext
          Wend
        'End If
      Else
        If Not ExistObjSyst(item, "CPY") Then
          addSegnalazione item, "NO" & relType, itemValue
        End If
      End If
      rsOggetti.Close
    Case Else
      '...
  End Select
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Analizza i parametri delle CALL IMS
' - Gestione PSB
' - Riconoscimento istruzioni: inserimento in PsDli_Istruzioni
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'COPIA... unificare!!!!!!!!!!!!!!!
Sub checkPSB(parametri As String)
  Dim istrParam As String, istruzione As String, psbParam As String, psb As String
  Dim isSchedule As Boolean
  Dim rsIstruzioni As Recordset, rsOggetti As Recordset
  Dim IdPsb As Long
  Dim moveResponse As String
  
  On Error GoTo tmp
   
  istruzione = Replace(nextToken_tmp(parametri), ".", "")
  
  istrParam = istruzione 'mi serve una copia se non riesco a risolvere la variabile...
    
   '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Prepariamo la struttura con il campo istruzione che successivamente
  ' sar� associato ai possibili valori determinati dalle move
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ReDim Preserve CmpIstruzione(UBound(CmpIstruzione) + 1)
  CmpIstruzione(UBound(CmpIstruzione)).CampoIstruzione = istruzione
  CmpIstruzione(UBound(CmpIstruzione)).Oggetto = GbIdOggetto
  CmpIstruzione(UBound(CmpIstruzione)).Riga = GbOldNumRec
  CmpIstruzione(UBound(CmpIstruzione)).Risolta = 0
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'Se il parametro "istruzione" ha il nome esatto dell'istruzione, OK
  'altrimenti cerca il "VALUE":
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Select Case istruzione
    Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP"
    Case "DLET", "REPL", "ISRT"
    Case "PCB", "TERM"
'IRIS-DB    Case "AUTH", "GCMD", "ICMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG"
    Case "AUTH", "GCMD", "CMD", "POS", "RCMD", "GSCD", "GMSG", "CHNG", "PURG" 'IRIS-DB
    Case "SETB", "SETO", "SETS", "SETU", "XRST"
    Case "SYNC", "CHKP", "CKPT", "APSB", "DPSB", "INIT", "INQY", "LOG", "ROLB", "ROLL", "ROLS"
    Case "OPEN", "CLSE"
    Case "FOR"
    Case Else
      'VALUE:
      istruzione = getFieldValue(istruzione)
  End Select
  'Doppio controllo, ma almeno evitiamo il "PrendiValue" per i casi "sicuri"...
  Select Case istruzione
    Case "PCB"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
      isSchedule = True
    Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "DLET", "REPL", "ISRT"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "TERM"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "AUTH", "GCMD", "ICMD", "RCMD", "GSCD", "GMSG", "CHNG", "PURG"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "SETB", "SETO", "SETS", "SETU", "XRST"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "SYNC", "CHKP", "CKPT", "APSB", "DPSB", "INIT", "INQY", "LOG", "ROLB", "ROLL", "ROLS"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case "OPEN", "CLSE"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
'uso di subroutines
    Case "FOR"
      CmpIstruzione(UBound(CmpIstruzione)).Risolta = 1
    Case ""
      'getFieldValue non ha trovato niente
      ' provo a cercare fra le move fin qui trovate
      moveResponse = risolviIstruzioneByMove(CmpIstruzione(UBound(CmpIstruzione)).CampoIstruzione, CmpIstruzione(UBound(CmpIstruzione)).LastSerchIndex, 0)
      If Len(moveResponse) = 0 Then
        addSegnalazione istrParam, "NOISTRDLIVALUE", istrParam
      Else
        istruzione = moveResponse
        CmpIstruzione(UBound(CmpIstruzione)).Risolta = 2
      End If
    Case Else
      moveResponse = risolviIstruzioneByMove(CmpIstruzione(UBound(CmpIstruzione)).CampoIstruzione, CmpIstruzione(UBound(CmpIstruzione)).LastSerchIndex, 0)
      If Not Len(moveResponse) Then
        addSegnalazione istruzione, "NOISTRDLI", istruzione
        istruzione = ""
      Else
        istruzione = moveResponse
        CmpIstruzione(UBound(CmpIstruzione)).Risolta = 2
      End If
      
  End Select
   
  ''''''''''''''''''''''''''''''''''''''''''''
  ' Inserimento PsDli_Istruzioni:
  ''''''''''''''''''''''''''''''''''''''''''''
  ' AC :Case "" e ricerca move senza successo scrive campo vuoto
  ' a fine parser cercher� fra le move successive
  
  Set rsIstruzioni = m_Fun.Open_Recordset("select IdOggetto,Riga,istruzione,statement,isCBL, IdPgm from PsDli_Istruzioni")
  rsIstruzioni.AddNew
  rsIstruzioni!idOggetto = GbIdOggetto
  rsIstruzioni!idpgm = IIf(GbTipoInPars = "CPY", 0, GbIdOggetto)
  rsIstruzioni!idpgm = GbIdOggetto
  rsIstruzioni!Riga = GbOldNumRec
  rsIstruzioni!istruzione = istruzione
  'Parametri "utili" (gia' puliti da tutto...compreso eventuale punto finale)
  If Len(parametri) Then
    rsIstruzioni!statement = Trim(IIf(Right(parametri, 1) = ".", Left(parametri, Len(parametri) - 1), parametri))
  Else
    rsIstruzioni!statement = ""
  End If
  rsIstruzioni!isCBL = itemValue = "QJBTDLI"
  
  'Chiudo in fondo... dopo il controllo del psb
  
  ''''''''''''''''''''''''''''''''''''''''''''
  ' Schedulazione (gestione PSB):
  ' Es:
  ' - "PCB G006-PSBNAME ADDRESS OF DLIUIB"
  ' - "PCB G006-PSBNAME DLIUIB"
  ''''''''''''''''''''''''''''''''''''''''''''
  If isSchedule Then
    psbParam = nexttoken(parametri)
    'gestire psbParam=""...
    If Left(psbParam, 1) = "'" Then 'Puo' essere anche il valore costante?
      psb = Mid(psbParam, 2, Len(psbParam) - 2)
    Else
      '?????
      If ActParsNoPsb Then
        '??????
        psb = GbParmRel.DLIStdPsbName
      Else
        '''''''''''''''''''''''''''''''''''''''''''''''
        ' - VALUE iniziale; se non c'�:
        ' - MOVE (la prima, ma le prenderei tutte!)
        '''''''''''''''''''''''''''''''''''''''''''''''
        psb = PrendiValue(psbParam) 'Buttare via: usare getFieldValue
        If psb = "SPACE" Or psb = "SPACES" Or psb = "None" Then
          psb = TrovaPsb(psbParam)
        End If
      End If
    End If
        
    If Len(psb) Then
      Set rsOggetti = m_Fun.Open_Recordset("select idOggetto from Bs_oggetti where tipo = 'PSB' and nome = '" & psb & "'")
      If rsOggetti.RecordCount = 0 Then
        addSegnalazione psb, "NOPSB", psb
      Else
        AggiungiRelazione rsOggetti!idOggetto, "PSB", psb, psb
      End If
      rsOggetti.Close
    Else
      addSegnalazione psbParam, "NOPSBVALUE", psbParam
    End If
    
  End If
  rsIstruzioni.Update
  rsIstruzioni.Close
  
  Exit Sub
tmp:
  'gestire...
  MsgBox err.description
  'Resume
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' COPIA...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub getMove(line As String, FD As Long, ByRef pcampiMove() As UtCampiMove)
  Dim statement As String, variable As String, token As String
  Dim endStatement As Boolean
  Dim linee As Integer
  Dim campo As String, campoof As String, campoend As String
  
  Dim i As Integer, j As Integer
  On Error GoTo callErr
  
  GbOldNumRec = GbNumRec  'linea iniziale
  ''''''''''''''''''''''''''''''''''
  ' Becco tutte le linee di call...
  ''''''''''''''''''''''''''''''''''
  statement = line
  If Len(line) = 0 Then line = " "
  If Mid(line, Len(line), 1) <> "." Then
    While Not EOF(FD) And Not endStatement
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Left(line, 1) <> "*" Then
        'If Mid(line, 7, 1) <> "-" Then
          'Eliminazione Commenti a met� linea:
          line = Trim(Left(line, InStr(line & "/*", "/*") - 1))
          ' posso avere una linea vuota? (prob. no...)
          If Len(line) Then
            'Altro statement?
            'If isCobolLabel(line) Then 'LABEL!
            '  endStatement = True
            '  linee = GbNumRec - GbOldNumRec
            'Else
              token = nexttoken(line)
              'Parola Cobol?
              'SQ 27-04-06: se mi arriva END-IF./END-CALL. con il punto sbaglia!
              If isReservedWord(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Or IsNumeric(token) Then  'LABLE NUMERICHE!
                endStatement = True
                'linea buona per il giro successivo!
                line = Space(4) & token & " " & line
                'SQ 4-05-06
                'Mi serve linee perch� nei casi di "SeWordCobol", gbnumrec uno in pi�
                linee = GbNumRec - GbOldNumRec
              Else
                'linea buona:
                statement = statement & " " & token & " " & line
                'SQ - Fine statement con Punto finale o ELSE!
                If Len(line) Then
                  If Mid(line, Len(line), 1) = "." Then
                    endStatement = True
                    linee = GbNumRec - GbOldNumRec + 1
                    line = "" 'e' il flag... per la nuova lettura
                  ElseIf Right(" " & line, 5) = " ELSE" Then
                    endStatement = True
                    linee = GbNumRec - GbOldNumRec
                    line = "" 'e' il flag... per la nuova lettura
                    'Mangio l'ELSE finale
                    statement = Trim(Left(statement, Len(statement) - 4))
                  Else
                    endStatement = False
                  End If
                Else
                  endStatement = Mid(token, Len(token), 1) = "."
                  linee = GbNumRec - GbOldNumRec + 1
                End If
              End If
            'End If
          End If
        'Else
        '  line = Trim(Mid(line, 8, 65))
        '  line = Mid(line, 2)
        '  statement = statement & " " & line
        'End If
      End If
    Wend
  Else
    'SQ 14-02-06: se CALL su unica riga, rimane "line" sporca...
    line = ""
  End If
  
  '
  Dim bolCostant As Boolean, bolnumeric As Boolean
  If Left(statement, 2) = "C'" Then
    bolCostant = True
    statement = Mid(statement, 2)
  Else
    bolCostant = False
  End If
  variable = Replace(nexttoken(statement), ".", "")
  If variable = "ALL" Then
    If Left(statement, 1) = "'" Then
      bolCostant = True
    Else
      bolCostant = False
    End If
    variable = Replace(nexttoken(statement), ".", "")
  End If
  If IsNumeric(variable) Then
    bolnumeric = True
  Else
    bolnumeric = False
  End If
  If bolCostant Or bolnumeric Then
    variable = "'" & variable & "'"
  End If
  'tmp: indici! li eliminiamo... GESTIRE!
  If Left(statement, 1) = "(" Then token = nexttoken(statement)
  token = nexttoken(statement) 'serve solo a mangiare l'eventuale "TO"
  While Len(statement)
    campo = nexttoken(statement)
    'tmp: indici! li eliminiamo... GESTIRE!
    If Left(statement, 1) = "(" Then token = nexttoken(statement)
    campoof = nexttoken(statement)
    ' se successivo OF compongo campo
    ' altrimenti ricostruisco statement
    If campoof = "OF" Then
      campoend = nexttoken(statement)
      campo = campo & " OF " & campoend
    ElseIf Not campoof = "" Then ' ero gi� alla fine
      statement = campoof & " " & statement
    End If
      
      
    If Right(campo, 1) = "." Then
      campo = Left(campo, Len(campo) - 1)
    End If
   
       
    For i = 1 To UBound(pcampiMove)
      If pcampiMove(i).NomeCampo = campo Then
        Exit For
      End If
    Next
    If i = UBound(pcampiMove) + 1 Then ' campo nuovo
      ReDim Preserve pcampiMove(UBound(pcampiMove) + 1)
      pcampiMove(UBound(pcampiMove)).NomeCampo = Replace(campo, ",", "")
      Set pcampiMove(UBound(pcampiMove)).Valori = New Collection
      pcampiMove(UBound(pcampiMove)).Valori.Add variable
    Else
      For j = 1 To pcampiMove(i).Valori.count
        If pcampiMove(i).Valori.item(j) = variable Then
          Exit For
        End If
      Next
      If j = pcampiMove(i).Valori.count + 1 Then
        pcampiMove(i).Valori.Add variable
      End If
    End If
    If Trim(statement) = "." Then statement = ""
  Wend
  
  Exit Sub
callErr:
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      '...
    Else
      err.Raise 124, "getMove", "syntax error on " & statement
      'Resume
    End If
  End If
  'gestire...
'''  If Err.Number = -2147217887 Then  'chiave duplicata...
'''    '2 CALL su una linea...
'''  Else
'''    Err.Raise 124, "getMove", "syntax error on " & statement
'''    Resume
'''  End If
End Sub
''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''
Private Sub getInclude(parameters As String)
  Dim rs As Integer
  Dim token As String
  
  GbOldNumRec = GbNumRec  'linea iniziale
  
  token = nexttoken(parameters)
  
  'tmp
  If Len(parameters) Then MsgBox "TMP: getINCLUDE - " & parameters & "?"
  
  checkRelations token, "QUIK-INCLUDE"
  
'  Set TbOggetti = m_Fun.Open_Recordset("select * from bs_oggetti where tipo = 'CPY' and nome = '" & Wstr2 & "'")
'  If TbOggetti.RecordCount > 0 Then
'     If TbOggetti.RecordCount = 1 Then
'         AggiungiRelazione TbOggetti!idOggetto, "INCLUDE", Wstr2, Wstr2
'      Else
'        While Not TbOggetti.EOF
'            'Inswerisce le relazioni come duplicate nella tabella
'            AggiungiRelazione TbOggetti!idOggetto, "INCLUDE", Wstr2, Wstr2, True
'
'            TbOggetti.MoveNext
'        Wend
'      End If
'  Else
'     If Not ExistObjSyst(Wstr2, "CPY") Then
'         addSegnalazione Wstr2, "NOINCLUDE", Wstr2
'     End If
'  End If

End Sub
''''''''''''''''''''''''''''''''''
' Definizione Dati:
' Allestisce la PsQUIK_Dati
''''''''''''''''''''''''''''''''''
Private Sub getEQU(parameters As String)
  Dim rs As Recordset
  Dim token As String
  
  On Error GoTo dataErr

  equOrdinale = equOrdinale + 1
  
  Set rs = m_Fun.Open_Recordset("Select * from PsQUIK_Dati")
  rs.AddNew
  
  rs!idOggetto = GbIdOggetto
  rs!Ordinale = equOrdinale
  
  'Nome
  token = nextToken_tmp(parameters)
  rs!nome = token
  'Dichiarazione
  token = nextToken_tmp(parameters)
  rs!Tipo = Left(token, 3)
  rs!Posizione = Mid(token, 4)
  'Valore
  If Len(parameters) Then
    token = nextToken_tmp(parameters)
    If token = "C" Then
      token = nextToken_tmp(parameters)
     DoEvents
    End If
    rs!Valore = token
  End If
  
  rs.Update
  rs.Close
  Exit Sub
dataErr:
  'tmp
  MsgBox err.description
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Parsing di II livello
' Analizza le istruzioni IMS che trova in PsDli_Istruzioni
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parsePgm_II(Optional pRiga As Long = -1, Optional pIdOggetto As Long = -1)
  Dim istruzione As String, parametri As String
  Dim rs As Recordset
  Dim rsCall As ADODB.Recordset
  Dim OptWhereCond As String
  Dim strQRY As String
  Dim programma As String
  
  On Error GoTo parseErr
    
  ReDim idAree(0)
  ReDim ssaCache(0)
  ReDim wPcb(0)
  ReDim xPcb(0)
  ReDim fSegm(0)
   
  If pRiga = -1 Then
    OptWhereCond = ""
  Else
    OptWhereCond = " and Riga = " & pRiga
    GbIdOggetto = pIdOggetto
  End If
  
  'Naming Convention: nome segmento/nome area
  segmentNameParam = LeggiParam("SEGM-NAME-CRITERIA")
 
  ' PSB Schedulati
  getPsb_program
  
  strQRY = " Select I.isExec,I.istruzione,I.statement,I.Riga,I.valida, C.Programma " & _
           " from PsDLI_Istruzioni I, PsCall C " & _
           " where I.IdOggetto = C.IdOggetto and I.Riga = C.Riga " & _
           " and I.idOggetto = " & GbIdOggetto & OptWhereCond
           
  Set rs = m_Fun.Open_Recordset(strQRY)
  If rs.RecordCount Then
    If SwPsbAssociato Then
      IdPsb = psbPgm(0)
    Else
      addSegnalazione " ", "NORELPSB", " "
      IdPsb = 0
    End If
  End If
  
  While Not rs.EOF
    istruzione = IIf(IsNull(rs!istruzione), "", rs!istruzione)
    parametri = rs!statement
    GbOldNumRec = rs!Riga
    programma = rs!programma
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Select Case istruzione
      Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP"
          parseGU istruzione, parametri, programma
      Case "DLET", "REPL"
          parseREPL istruzione, parametri
      Case "ISRT"
          parseISRT istruzione, parametri
        '''getSegment istruzione, parametri
      Case "CHKP"
        ' Mauro 12-04-2007 : Attenzione, c'� una routine omonima sul modulo IMS
        MapsdM_EASYTRIEVE.parseCHKP parametri
      Case "AUTH"
        parseAUTH parametri
      Case "XRST"
        ' Mauro 12-04-2007 : Attenzione, c'� una routine omonima sul modulo IMS
        MapsdM_EASYTRIEVE.parseXRST parametri
      Case "ICMD"
        'parseICMD parametri
      Case "CHNG"
        'parseCHNG parametri
      Case "INQY"
        parseINQY parametri
      Case "LOG", "ROLB", "ROLL", "ROLS"
        'TMP: riuso il vecchio giro... sostituendo man mano...
        'parseROLL istruzione, parametri
      Case "PCB", "TERM"  ',"SCHEDULE","SCHD"
         'parsePCB parametri
      Case "SYNC"
        rs!valida = True
        rs.Update
      Case "CKPT", "APSB", "DPSB", "INIT"
        rs!valida = True
        rs.Update
      Case "OPEN", "CLSE"
        'parseOPEN_CLOSE istruzione, parametri
      Case "GCMD", "RCMD", "GSCD", "GMSG", "PURG"
        'Fare bene...
        'parsePURG istruzione, parametri
      Case "SETB", "SETO", "SETS", "SETU"
        'Fare bene...
        'parseSETx istruzione, parametri
      Case "FOR"
      'un po' assurda....
        parseFOR parametri
      Case Else
        'gestire...
    End Select
    
   rs.MoveNext
  Wend
  rs.Close
  
  ' Update Bs_oggetti: data,livello,errorLevel
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  TbOggetti!DtParsing = Now
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti!parsinglevel = 2
  TbOggetti.Update
  TbOggetti.Close
  
  ' Aggiornamento DBD in PsRel_Obj e SEGM in PsComObj (!?)
  AggiornaRelIstrDli GbIdOggetto
  
  Exit Sub
parseErr:
  If err.Number = -2147467259 Then  'tmp - problema Alby... prj Perot
    addSegnalazione GbNomePgm & ": nested PSB", "I00", GbNomePgm & ": nested PSB"
  Else
    addSegnalazione rs!statement, "I00", rs!statement
  End If
End Sub
Sub parseCHKP(parametri As String)
  Dim i As Integer
  Dim wDliRec As String
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = 0 'sembra che non ci sia in questa istruzione
  TabIstr.istr = "CHKP"
  TabIstr.PsbId = 0
  TabIstr.psbName = " "
  TabIstr.DBD_TYPE = DBD_UNKNOWN_TYPE
  TabIstr.DBD = "TO_MIGRATE" 'scherzetto per far migrare l'istruzione anche in assenza del DBD
  TabIstr.numpcb = 0

    'isDC = True
  Dim tb As Recordset
  'stefano: forzato, altrimenti � casuale, quello dell'istruzione precedente
  IsValid = True

  UpdatePsdliIstruzioni IsDC
  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")

  While Len(parametri)
    wDliRec = nexttoken(parametri)
    If Len(wDliRec) Then
      i = i + 1
      tb.AddNew
      'SQ COPY-ISTR
      'tb!idOggetto = GbIdOggetto
      tb!idOggetto = IdOggettoRel
      tb!idpgm = GbIdOggetto

      tb!Riga = GbOldNumRec
      tb!livello = i
      tb!NomeSSA = ""
      tb!dataarea = wDliRec
      tb.Update
    End If
  Wend
  tb.Close
End Sub

Sub parseXRST(parametri As String)
  Dim wDliRec As String
  Dim i As Integer
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = 0 'sembra che non ci sia in questa istruzione
  TabIstr.istr = "XRST"
  TabIstr.PsbId = 0
  TabIstr.psbName = " "
  'stefano: pezza per colpa di qualcun altro
  TabIstr.DBD_TYPE = DBD_UNKNOWN_TYPE
  TabIstr.DBD = "TO_MIGRATE" 'scherzetto per far migrare l'istruzione anche in assenza del DBD
  TabIstr.numpcb = 0
'If TabIstr.NumPcb = 1 Then
    '''''''''''''''''''
    'PCB-IO: IMS-DC
    '''''''''''''''''''
    'isDC = True
    'MsgBox "tmp: PCB-IO: IMS-DC!"
    'tmp:
    'parseRECEIVE
    'Exit Sub
  'Else
    '
  'End If
  Dim tb As Recordset
  'stefano: forzato, altrimenti � casuale, quello dell'istruzione precedente
  IsValid = True
  UpdatePsdliIstruzioni (IsDC)

  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  While Len(parametri)
    wDliRec = nexttoken(parametri)
    If Len(wDliRec) Then
      i = i + 1
      tb.AddNew
      'SQ COPY-ISTR
      'tb!idOggetto = GbIdOggetto
      tb!idOggetto = IdOggettoRel
      tb!idpgm = GbIdOggetto
      tb!Riga = GbOldNumRec
      tb!livello = i
      tb!NomeSSA = ""
      tb!dataarea = wDliRec
      tb.Update
    End If
  Wend
  tb.Close
End Sub

Sub parseFOR(parametri As String)
  Dim wDliRec As String
  Dim i As Integer
  Dim rsdbd As Recordset
  ReDim TabIstr.BlkIstr(0)

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = 0 'sembra che non ci sia in questa istruzione
  TabIstr.istr = "FOR"
  TabIstr.PsbId = 0
  TabIstr.psbName = " "
  'stefano: pezza per colpa di qualcun altro
  TabIstr.DBD_TYPE = DBD_UNKNOWN_TYPE
  TabIstr.DBD = "TO_MIGRATE" 'scherzetto per far migrare l'istruzione anche in assenza del DBD
  TabIstr.numpcb = 0
'If TabIstr.NumPcb = 1 Then
    '''''''''''''''''''
    'PCB-IO: IMS-DC
    '''''''''''''''''''
    'isDC = True
    'MsgBox "tmp: PCB-IO: IMS-DC!"
    'tmp:
    'parseRECEIVE
    'Exit Sub
  'Else
    '
  'End If
  Dim tb As Recordset
  'stefano: forzato, altrimenti � casuale, quello dell'istruzione precedente
  IsValid = True
  UpdatePsdliIstruzioni (IsDC)

  Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
  While Len(parametri)
    wDliRec = nexttoken(parametri)
    If Len(wDliRec) Then
      i = i + 1
      tb.AddNew
      'SQ COPY-ISTR
      'tb!idOggetto = GbIdOggetto
      tb!idOggetto = IdOggettoRel
      tb!idpgm = GbIdOggetto
      tb!Riga = GbOldNumRec
      tb!livello = i
      tb!NomeSSA = ""
      tb!dataarea = wDliRec
      tb.Update
    End If
  Wend
  tb.Close
End Sub

Public Sub parseGU(istruzione As String, parametri As String, programma As String)
  Dim rs As Recordset, tb As Recordset, tb1 As Recordset, tb2 As Recordset, rsdbd As Recordset
  Dim i As Integer, k As Integer, y As Integer, T1 As Integer, T2 As Integer, T3 As Integer, T4 As Integer
  Dim wNomeCmpLiv As String, wIdStr As String, wStrIdOggetto As String, wStrIdArea As Long
  Dim rsArea As Recordset, rsLink As Recordset, rsCmp As Recordset
  Dim sArea As String, sLink As String, sCmp As String
  Dim dbdName As String, wNomePsb As String, ssaValue As String
  Dim isGSAM As Boolean, unqualified As Boolean
  Dim idField As Integer
  Dim wDliRec As String, tempTokenSQ As String
  Dim ssa
  
  On Error GoTo parseErr
  
  ReDim gSSA(0)
  ReDim dbdPgm(0)
  ReDim TabIstr.BlkIstr(0)
  'SQ 17-11-06
  SwSegm = "" 'init
  
  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  wDliRec = nexttoken(parametri)
  If Len(parametri) = 0 Then tempTokenSQ = wDliRec
  
  If istruzione = "REPL" Then Stop
  
  TabIstr.istr = istruzione
  
  '''''''''''''''''''''
  ' PCB (number)
  '''''''''''''''''''''
  TabIstr.numpcb = getPCBnumber(TabIstr.pcb, programma)
  If IsDC Then
    parseRECEIVE istruzione, wDliRec
    Exit Sub
  End If
  
  'SQ 27-04-06 -
  'COMPLETAMENTE SQUALIFICATA?
  unqualified = Len(parametri) = 0
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Gestione SSA
  ' Valorizzo la TabIstrSSa
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ReDim wDliSsa(0)
  ReDim TabIstrSSa(0)
  ReDim TabIstrSSa(0).NomeSegmento.value(0)
  Dim ssaOk As Boolean
  ssaOk = True
  While Len(parametri) > 0
    i = i + 1
    ReDim Preserve wDliSsa(UBound(wDliSsa) + 1)
    tempTokenSQ = parametri
    wDliSsa(UBound(wDliSsa)) = nextToken_new(parametri)
    
    If InStr(wDliSsa(UBound(wDliSsa)), "'") = 0 Then
    'vedere 'sto blocco se serve...
      sArea = "Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & GbIdOggetto
      Set rsArea = m_Fun.Open_Recordset(sArea)
      If rsArea.RecordCount = 0 Then
        'Ricerca nelle COPY:
        sLink = "Select IdOggettoR From PsRel_Obj Where IdOggettoC = " & GbIdOggetto & " And utilizzo = 'EZM'"
        Set rsLink = m_Fun.Open_Recordset(sLink)
        Do While Not rsLink.EOF
          sCmp = "Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & rsLink!idOggettoR
          Set rsCmp = m_Fun.Open_Recordset(sCmp)
          If rsCmp.RecordCount Then
             Set rsArea = rsCmp
             Exit Do
          End If
          rsLink.MoveNext
        Loop
        rsLink.Close
      End If
    
      'Valorizza TabIstrSSa:
      If rsArea.RecordCount Then
        'SQ - tmp: mettere in linea...
        'Non funge per indirizzo? (in testa?)
        ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
        TabIstrSSa(UBound(TabIstrSSa)).SSAName = wDliSsa(UBound(wDliSsa))
        TabIstrSSa(UBound(TabIstrSSa)).idOggetto = rsArea!idOggetto
        TabIstrSSa(UBound(TabIstrSSa)).idArea = rsArea!idArea
        parseSSA_ETZ TabIstrSSa(UBound(TabIstrSSa))
        ssaOk = True
      Else
        ssaOk = False
      End If
      rsArea.Close
    Else
      ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
      TabIstrSSa(UBound(TabIstrSSa)).SSAName = wDliSsa(UBound(wDliSsa))
      TabIstrSSa(UBound(TabIstrSSa)).idOggetto = 0 'fittizio
      TabIstrSSa(UBound(TabIstrSSa)).idArea = 0 'fittizio
      parseSSA_ETZ TabIstrSSa(UBound(TabIstrSSa))
    End If
  Wend
  
  ''''''''''''''''''''''''''''''''''''''''
  ' SQ 20-04-06
  ' Area Segmento - la associava a tutti!
  '''''''''''''''''''''''''''''''''''''''''
  If ssaOk And Not unqualified Then
    TabIstrSSa(UBound(TabIstrSSa)).Struttura = wDliRec
    If Len(TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0)) = 0 And Len(wDliRec) Then  'Solo sul Segmento finale...
      Dim env As Collection 'area,idDbd
      If Len(segmentNameParam) Then
        Set env = New Collection  'resetto;
        env.Add wDliRec
        env.Add 0 'wIdDBD(0)
        TabIstrSSa(UBound(TabIstrSSa)).NomeSegmento.value(0) = getEnvironmentParam(segmentNameParam, "IMS", env)
      End If
    End If
    getTabIstr
  End If
  ''''''''''''''''''''''''''''''''
  ' Gestione PSB/DBD:
  ' Input: N PSB, PCB, M SEGMENTI
  ''''''''''''''''''''''''''''''''
  getInstructionPsbDbd (TabIstr.numpcb)
  
  'SQ - 27-04-06
  isGSAM = TabIstr.DBD_TYPE = DBD_GSAM
  ' non mi piace molto che sia separata, comunque...
  If isGSAM Then
    getGSAMinfo (TabIstr.numpcb)  'wIDdbd...
  Else
    ''''''''''''''''''''''''''''''''''''''''''
    ' TabIstr.BlkIstr:
    ''''''''''''''''''''''''''''''''''''''''''
    If ssaOk And IsValid Then 'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
      getTabIstrIds
    Else
      If Not unqualified Then
        IsValid = False
      End If
    End If
  End If
  
  If (IsValid And Not unqualified) Or isGSAM Then
    'AGGIORNAMENTO "PsDli_IstrDett_Liv"
    Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
    For k = 1 To UBound(TabIstr.BlkIstr)
      'SEGMENTO VARIABILE?
      If Left(TabIstr.BlkIstr(k).Segment, 1) = "(" And Right(TabIstr.BlkIstr(k).Segment, 1) = ")" Then
        ReDim IdSegmenti(0)
      End If
      wIdStr = getIdArea(TabIstr.BlkIstr(k).Record)
      wNomeCmpLiv = TabIstr.BlkIstr(k).Record
      
      'ADESSO HO N SEGMENTI QUANTI SONO I DBD... (o ZERO!)
      If SwParsExec Then
       '...
      Else
       'SQ 28-11-05:
       If TabIstr.BlkIstr(k).IdSegm Then
         wStrIdOggetto = Val(Left(wIdStr, 6))
         wStrIdArea = Val(Mid(wIdStr, 8, 4))
         'SQ - 28-11-05
         'Pezza provvisoria: capire perche' arriva cosi'...
         If wStrIdOggetto <> 0 And wStrIdArea <> 0 Then
           Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & TabIstr.BlkIstr(k).IdSegm _
                & " and stridoggetto = " & wStrIdOggetto _
                & " and stridarea = " & wStrIdArea _
                & " and nomecmp = '" & wNomeCmpLiv & "'")
                     
           If tb2.RecordCount = 0 Then
              tb2.AddNew
           End If
           
           tb2!idSegmento = TabIstr.BlkIstr(k).IdSegm
           tb2!stridoggetto = wStrIdOggetto
           tb2!strIdArea = wStrIdArea
           tb2!nomeCmp = wNomeCmpLiv
           'AC
           tb2.Update
           tb2.Close
         End If
       End If
     End If
    
     If TabIstr.DBD_TYPE <> DBD_GSAM Then
       If Len(IdSegmenti(0)) = 0 Then SwSegm = TabIstr.BlkIstr(k).Segment
     End If
          
     Dim wNomeCmp As String
     i = InStr(wIdStr, "-")
     If i > 0 Then
       wNomeCmp = Mid(wIdStr, i + 2)
       wIdStr = Val(Mid(wIdStr, 1, i - 1))
     Else
       wNomeCmp = " "
     End If
    
     ''''''''''''''''''''''''''''''''''''''
     'INSERIMENTO IN "PsDli_IstrDett_Liv":
     ''''''''''''''''''''''''''''''''''''''
     tb.AddNew
     tb!idOggetto = GbIdOggetto
     tb!idpgm = GbIdOggetto
     tb!Riga = GbOldNumRec
     tb!livello = k
    
     For i = 0 To UBound(TabIstrSSa)
      If Trim(TabIstrSSa(i).SSAName) = Trim(TabIstr.BlkIstr(k).SSAName) Then
        tb!stridoggetto = TabIstrSSa(i).idOggetto
        tb!strIdArea = TabIstrSSa(i).idArea
        If isGSAM Then
          tb!stridoggetto = wStrIdOggetto
          tb!strIdArea = wStrIdArea
        End If
        Set rsArea = m_Fun.Open_Recordset("Select Byte From PsData_Cmp Where IdOggetto = " & tb!stridoggetto & " and idarea = " & tb!strIdArea & " and nome = '" & wNomeCmpLiv & "'")
        If rsArea.EOF Then
           tb!AreaLen = Null 'forse sarebbe meglio zero, cos� saprei che l'analisi � stata fatta
        Else
           tb!AreaLen = rsArea!byte
        End If
        rsArea.Close
        Exit For
      End If
    Next
         
    If Not SwParsExec Then 'CALL
      tb!IdSegmento_moved = TabIstr.BlkIstr(k).segment_moved
      'SQ 30-01-07 - Modifica al EX getIdSegmento_By_MgTable...
      If TabIstr.BlkIstr(k).IdSegm = 0 Then
        'Scamuffo I/O AREA -> SEGMENTO (Ha senso solo sull'ultimo livello - volendo si pu� rendere opzionale)
        If k = UBound(TabIstr.BlkIstr) Then
          getSegment_byArea TabIstr.BlkIstr(k)
        End If
      End If
      If TabIstr.BlkIstr(k).IdSegm Then
        tb!idSegmento = TabIstr.BlkIstr(k).IdSegm
      Else
        'Segmento non trovato: istruzione INVALIDA!
        IsValid = False
        addSegnalazione TabIstr.BlkIstr(k).Segment, "NOSEGM", TabIstr.istr
      End If
    End If
     
    'E' sempre vuoto! servirebbe una "getLenSegmentoByIdSegmento(tb!IdSegmento)",
    'ma metterlo a posto a monte...
    If IsNumeric(TabIstr.BlkIstr(k).SegLen) Then 'blank/null (numerico <- stringa) !
     tb!SegLen = CInt(TabIstr.BlkIstr(k).SegLen)
    Else
     tb!SegLen = 0
    End If
    tb!NomeSSA = TabIstr.BlkIstr(k).SSAName   'togliere sti bianchi in fondo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    tb!dataarea = TabIstr.BlkIstr(k).Record
     
    'COMMAND CODES
    'Occhio: c'� la search gi� decodificata!
    tb!condizione = Left(TabIstr.BlkIstr(k).CodCom, 100)
    tb!comcodes_moved = TabIstr.BlkIstr(k).CodCom_moved
    
    'QUALIFICAZIONE (MULTIPLA)
    tb!Qualificazione = TabIstr.BlkIstr(k).qualified
    tb!Qualificazione_moved = TabIstr.BlkIstr(k).qualified_moved
    tb!Qualandunqual = TabIstr.BlkIstr(k).qual_unqual
    
    tb!Correzione = False
    tb!moved = TabIstr.BlkIstr(k).moved
    '''''''''''''''''''''''''''''''''''''''''''''''''
    'GESTIONE CHIAVI:
    'Cerca ID campo chiave
    'Allestisce tabella record in PsDli_IstrDett_Key
    'Setta la validita' dell'istruzione;
    '''''''''''''''''''''''''''''''''''''''''''''''''
    For y = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
      If TabIstr.BlkIstr(k).IdSegm Then
        'piu' di un elemento: indeterminatezza
        idField = CercaIdField(TabIstr.BlkIstr(k).IdSegm, TabIstr.BlkIstr(k).KeyDef(y).Key)
      Else
        idField = 0
      End If
        
      'SQ - 27-05-05
      If idField = 0 Then
        TabIstr.BlkIstr(k).valida = False 'istruzione sbagliata in ogni caso!
        IsValid = False
        '''''''''''''''''''''''''''''''''''''''
        ' SQ 5-05-06 - Nuova segnalazione (I04)
        '''''''''''''''''''''''''''''''''''''''
        addSegnalazione TabIstr.BlkIstr(k).KeyDef(y).Key, "SSAKEYNOTFOUND", TabIstr.BlkIstr(k).KeyDef(y).Key
      End If
    
      'SQ: spostata qui... era fuori ciclo e settava solo la chiave 1!?
      TabIstr.BlkIstr(k).KeyDef(y).xName = GbXName
        
      Set tb1 = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Key")
      tb1.AddNew
      'SQ COPY-ISTR
      tb1!idOggetto = GbIdOggetto
      IdOggettoRel = GbIdOggetto
      'tb1!idOggetto = IdOggettoRel
      tb1!idpgm = GbIdOggetto
      tb1!Riga = GbOldNumRec
      tb1!livello = k
      tb1!progressivo = y
        
      tb1!idField = idField
      tb1!idField_moved = TabIstr.BlkIstr(k).KeyDef(y).Key_moved
      'SQ: aspettare il via di Ste per mettere true se idField negativo.
      tb1!xName = TabIstr.BlkIstr(k).KeyDef(y).xName
      
      tb1!operatore = TabIstr.BlkIstr(k).KeyDef(y).Op 'multiplo
      tb1!operatore_moved = TabIstr.BlkIstr(k).KeyDef(y).Op_moved
      
      'SQ: perche' se e' = ""??????????
      If TabIstr.BlkIstr(k).KeyDef(y).Key <> "Nothing" Then
        tb1!Valore = TabIstr.BlkIstr(k).KeyDef(y).KeyVal & ""
        tb1!KeyValore = Left(TabIstr.BlkIstr(k).KeyDef(y).KeyVarField, 255)
        'SQ: aggiungere colonna in DB!
        tb1!KeyStart = TabIstr.BlkIstr(k).KeyDef(y).KeyStart
      End If
  
      tb1!KeyLen = TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ""
        
      'SG : Se la lunghezza non � stata calcolata o calcolata male recupera l'effettiva lunghezza dal field dli
      If Val(tb1!KeyLen) <= 0 Or Trim(tb1!KeyLen) = "" Then
         tb1!KeyLen = Restituisci_DatiKey(tb1!idField, tb1!xName, "LUNGHEZZA")
         TabIstr.BlkIstr(k).KeyDef(y).KeyLen = tb1!KeyLen
         
         If Len(TabIstr.BlkIstr(k).SSAName) Then
            TabIstr.BlkIstr(k).KeyDef(y).KeyVarField = TabIstr.BlkIstr(k).SSAName & "(" & TabIstr.BlkIstr(k).KeyDef(y).KeyStart & ":" & TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ")"
            tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
         End If
      End If
      
      If Len(TabIstr.BlkIstr(k).KeyDef(y).KeyBool) = 0 Then
        tb1!Booleano = " "
      Else
        tb1!Booleano = TabIstr.BlkIstr(k).KeyDef(y).KeyBool
      End If
      tb1!booleano_moved = TabIstr.BlkIstr(k).KeyDef(y).KeyBool_moved
      'SQ:
      tb1!valida = TabIstr.BlkIstr(k).valida
      tb1!Correzione = False
      tb1.Update
      tb1.Close
    Next y
    
    Dim wSsaPres As Boolean
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' PsDli_istrDett_Dup: COS'E' 'STA ROBA?????
    ' A COSA SERVE LA gSSA?????????????????????
    ' Per la molteplicit�?????????????????????? (forse)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim tbr As Recordset
    Set tbr = m_Fun.Open_Recordset("Select * from PsDli_istrDett_Dup")
    T4 = 0
    wIdStr = ""
    
    For T1 = 1 To UBound(gSSA)
   '   For t2 = 1 To UBound(wDliSsa)
       If k <= UBound(wDliSsa) Then
         If gSSA(T1).Name = wDliSsa(k) Then
            If UBound(gSSA(T1).Segm) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).Segm)
                 T4 = T4 + 1
                 tbr.AddNew
                    'SQ COPY-ISTR
                    tbr!idOggetto = IdOggettoRel
                    tbr!idpgm = GbIdOggetto
                    tbr!Riga = GbOldNumRec
                    tbr!livello = k
                    tbr!numeropar = T4
                    tbr!Tipo = "SG"
                    tbr!Valore = gSSA(T1).Segm(T3)
                 tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).FIELD) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).FIELD)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "FL"
                     tbr!Valore = gSSA(T1).FIELD(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).Op) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).Op)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "OP"
                     tbr!Valore = gSSA(T1).Op(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).value) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).value)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "VL"
                     tbr!Valore = gSSA(T1).value(T3)
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).CodCom) > 1 Then
               T4 = 0
               For T3 = 1 To UBound(gSSA(T1).CodCom)
                  T4 = T4 + 1
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "CC"
                     tbr!Valore = DecodificaCodCom(gSSA(T1).CodCom(T3))
                  tbr.Update
               Next T3
            End If
            If UBound(gSSA(T1).parentesi) > 1 Then
               T4 = 0
               wIdStr = getIdArea(gSSA(1).Name)
               For T3 = 1 To UBound(gSSA(T1).parentesi)
                  T4 = T4 + 1
                  If T4 > 2 Then
                     T4 = T4
                  End If
                  tbr.AddNew
                     tbr!idOggetto = GbIdOggetto
                     tbr!Riga = GbOldNumRec
                     tbr!livello = k
                     tbr!numeropar = T4
                     tbr!Tipo = "PA"
                     tbr!Valore = gSSA(T1).parentesi(T3)
                  tbr.Update
               Next T3
            End If
         End If
       End If
  '    Next t2
    Next T1
    If Not TabIstr.BlkIstr(k).valida Then IsValid = False
    tb.Update
  Next k 'ITERAZIONE LIVELLI...
  tb.Close
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' GESTIONE SEGMENTI/AREE: ANCORA?!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'Attenzione: wIdStr NON e' quello usato sopra per i singoli livelli... e' un'altra cosa...
  If InStr(wIdStr, "/") Then
      wStrIdOggetto = Val(Left(wIdStr, 6))
      wStrIdArea = Val(Mid$(wIdStr, 8, 4))
      For y = 0 To UBound(IdSegmenti)
        If Len(Trim(IdSegmenti(y))) Then
          Set tb2 = m_Fun.Open_Recordset("select * from PsRel_SegAree where idsegmento = " & IdSegmenti(y) _
                & " and stridoggetto = " & wStrIdOggetto _
                & " and stridarea = " & wStrIdArea _
                & " and nomecmp = '" & TabIstr.BlkIstr(1).Record & "'")
          If tb2.RecordCount = 0 Then
            tb2.AddNew
            tb2!idSegmento = IdSegmenti(y)
            tb2!stridoggetto = wStrIdOggetto
            tb2!strIdArea = wStrIdArea
            tb2!nomeCmp = TabIstr.BlkIstr(1).Record
            tb2.Update
            tb2.Close
          End If
        End If
     Next y
    End If
  Else
    'AGGIORNAMENTO "PsDli_IstrDett_Liv"
    Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
    
    
    '''nome segmento
    Dim rsSegment As ADODB.Recordset
    tempTokenSQ = Replace(tempTokenSQ, "(", "")
    tempTokenSQ = Replace(tempTokenSQ, ")", "")
    Set rsSegment = m_Fun.Open_Recordset("select * from PSDLI_Segmenti where nome='" & tempTokenSQ & "'")
    
    tb.AddNew
    tb!idOggetto = GbIdOggetto
    tb!idpgm = GbIdOggetto
    tb!Riga = GbOldNumRec
    tb!livello = k
    tb!stridoggetto = GbIdOggetto
    tb!livello = 1
    If Not rsSegment.EOF Then
      tb!idSegmento = rsSegment!idSegmento
      tb!SegLen = rsSegment!MaxLen
    End If
   'forzatura che serve solo per la retrieve
    tb!dataarea = wDliRec
    tb.Update
  End If

  ''''''''''''''''''''''''''''''''''''''''''''
  ' UPDATE "PsDli_Istruzioni":
  ''''''''''''''''''''''''''''''''''''''''''''
  IdOggettoRel = GbIdOggetto
  UpdatePsdliIstruzioni (False)
  
  'SQ ??????????????????
  Dim ssaPresent As Boolean
  'ssaPresent = True
  'SG: Controlla se c'� un livello di ssa
  '...
  
  
  ''''''''''''''''''''''''''''''''''''''''
  ' Segnalazioni
  ''''''''''''''''''''''''''''''''''''''''
  'Portare nelle rispettive posizioni...!
  If Not IsValid And ssaOk Then
    If Len(TabIstr.DBD) And wIdDBD(0) = 0 Then  'NO DBD
      addSegnalazione TabIstr.DBD, "NODBD", TabIstr.istr
    ElseIf Trim(SwSegm) <> "" Then  'SQ ?
      addSegnalazione SwSegm, "NOSEGM", TabIstr.istr
    Else
'     addSegnalazione "", "INVALIDDLIISTR", ""
    End If
  End If
  Exit Sub
parseErr:
  Resume Next
End Sub

'stefano: l'ho inserita copiandola, sarebbe da rivedere di brutto, ci sono un sacco di pezzi inutili
Public Sub parseREPL(istruzione As String, parametri As String)
  Dim rs As Recordset, tb As Recordset, tb1 As Recordset, tb2 As Recordset, rsdbd As Recordset
  Dim i As Integer, k As Integer, y As Integer
  Dim dbdName As String, ssaValue As String ', ssaField As String
  Dim isGSAM As Boolean, unqualified As Boolean, segmentNotFound As Boolean
  Dim idField As Integer
  
  On Error GoTo parseErr
  
  ReDim gSSA(0) 'A cosa serve?
  ReDim dbdPgm(0)
  ReDim TabIstr.BlkIstr(0)

  Dim wDliRec As String
  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  TabIstr.pcb = nexttoken(parametri)
  TabIstr.istr = istruzione
    
  '''''''''''''''''''''
  ' PCB (number)
  '''''''''''''''''''''
  TabIstr.numpcb = getPCBnumber(TabIstr.pcb, "DLI")  'setta isDC
  
  wDliRec = nexttoken(parametri)
  
  ReDim TabIstr.BlkIstr(1)
  
  TabIstr.BlkIstr(1).Segment = wDliRec
  TabIstr.BlkIstr(1).Record = wDliRec
  
  TabIstr.numpcb = getPCBnumber(TabIstr.pcb, "DLI")  'setta isDC
  'COMPLETAMENTE SQUALIFICATA?
  unqualified = Len(parametri) = 0
  'tmp
  unqualified = False
  
  ''''''''''''''''''''''''''''''''
  ' Gestione PSB/DBD:
  ' Input: N PSB, PCB, M SEGMENTI
  ''''''''''''''''''''''''''''''''
  getInstructionPsbDbd (TabIstr.numpcb)
  
  ''''''''''''''''''''''''''''''''''''''''''
  ' TabIstr.BlkIstr:
  ''''''''''''''''''''''''''''''''''''''''''
  'isGSAM = False
  'If TabIstr.DBD_TYPE = DBD_GSAM Then
  '   isGSAM = True
  'End If
  ''If ssaOk Then 'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
  'If ssaOk Or isGSAM Then 'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
  '  getTabIstrIds
  'Else
  ' IsValid = False
  'End If
  
  isGSAM = TabIstr.DBD_TYPE = DBD_GSAM
  If isGSAM Then
    getGSAMinfo (TabIstr.numpcb)  'wIDdbd...
  Else
    ''''''''''''''''''''''''''''''''''''''''''
    ' TabIstr.BlkIstr:
    ''''''''''''''''''''''''''''''''''''''''''
    If IsValid Then 'nuovo booleano, perch� getInstructionPsbDbd setta IsValid...
      getTabIstrIds
    Else
      If Not unqualified Then
        IsValid = False
      End If
    End If
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'SQ 10-04-06
  ' Se usciamo qui perdiamo tutti i settaggi per l'istruzione!
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'stefano: da rivedere, � una prova
  'If IsValid Then
  If IsValid Or isGSAM Then
    ''''''''''''''''''''''''''''''''''''
    'ITERAZIONE LIVELLI ISTRUZIONE:
    ''
    'AGGIORNAMENTO "PsDli_IstrDett_Liv"
    ''''''''''''''''''''''''''''''''''''
    Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Liv")
    For k = 1 To UBound(TabIstr.BlkIstr)
'      '''''''''''''''''''''
'      'SEGMENTO VARIABILE?
'      '''''''''''''''''''''
'      If Left(TabIstr.BlkIstr(k).segment, 1) = "(" And Right(TabIstr.BlkIstr(k).segment, 1) = ")" Then
'        ReDim IdSegmenti(0)
'        '''''''''''''''''''''''''''''''''''''
'        ' Segmento variabile: gestione temp.
'        '''''''''''''''''''''''''''''''''''''
'      Else
'        If Len(dbdPgm(0)) Then
'        '   getSegmentsId (Replace(TabIstr.BlkIstr(K).segment, "'", ""))
'        Else
'           ReDim IdSegmenti(0)
'        End If
'      End If
      
      ''''''''''''''''''''''''''''''''''''''
      'INSERIMENTO IN "PsDli_IstrDett_Liv":
      ''''''''''''''''''''''''''''''''''''''
      tb.AddNew
      'SQ COPY-ISTR
      'tb!idOggetto = GbIdOggetto
      tb!idOggetto = IdOggettoRel
      tb!idpgm = GbIdOggetto
      tb!Riga = GbOldNumRec
      tb!livello = k
      
      'SEGMENTO:
      tb!IdSegmento_moved = TabIstr.BlkIstr(k).segment_moved
      'SQ 30-01-07 - Modifica al EX getIdSegmento_By_MgTable...
      'Mauro 19-03-2007 Multi-Segm
      'If TabIstr.BlkIstr(k).idsegm = 0 Then
'      If Len(TabIstr.BlkIstr(k).idSegmenti) = 0 Then
'        'Scamuffo I/O AREA -> SEGMENTO (Ha senso solo sull'ultimo livello - volendo si pu� rendere opzionale)
'        If k = UBound(TabIstr.BlkIstr) Then
'          getSegment_byArea TabIstr.BlkIstr(k)
'        End If
'      End If
      
      'Mauro 19-03-2007 Multi-Segm
      'If TabIstr.BlkIstr(k).idsegm Then
      ' per ora fisso
      If Len(TabIstr.BlkIstr(k).IdSegm) Then
        tb!idSegmento = TabIstr.BlkIstr(k).IdSegm
        tb!IdSegmentiD = TabIstr.BlkIstr(k).IdSegm
        TabIstr.BlkIstr(k).valida = True
      Else
        'tb!IdSegmento = 0  'assicurarsi che sia il default
        'Segmento non trovato: istruzione INVALIDA!
        IsValid = False
        segmentNotFound = True
        addSegnalazione TabIstr.BlkIstr(k).Segment, "NOSEGM", TabIstr.istr
      End If
    
     'Mauro 19-03-2007 Multi-Segm
     'If IsNumeric(TabIstr.BlkIstr(k).SegLen) Then 'blank/null (numerico <- stringa) !
'     If Len(TabIstr.BlkIstr(k).SegmentiLen) Then
       If TabIstr.BlkIstr(k).SegLen Then
         tb!SegLen = CInt(TabIstr.BlkIstr(k).SegLen)
'       Else
'         'Imposto il primo valore tra quelli multipli
'         Dim SegmentLen() As String
'         SegmentLen() = Split(TabIstr.BlkIstr(k).SegmentiLen, ";")
'         tb!SegLen = SegmentLen(0)
'       End If
       tb!SegmentiLend = TabIstr.BlkIstr(k).SegLen
     Else
       tb!SegLen = 0
       tb!SegmentiLend = 0
     End If
     
      tb!NomeSSA = TabIstr.BlkIstr(k).SSAName
      
      tb!dataarea = TabIstr.BlkIstr(k).Record
    
      'COMMAND CODES
      'Occhio: c'� la search gi� decodificata...
      tb!condizione = Left(TabIstr.BlkIstr(k).CodCom, 100)
      
      tb!comcodes_moved = TabIstr.BlkIstr(k).CodCom_moved
       
      'QUALIFICAZIONE (MULTIPLA)
      tb!Qualificazione = TabIstr.BlkIstr(k).qualified
      tb!Qualificazione_moved = TabIstr.BlkIstr(k).qualified_moved
      tb!Qualandunqual = TabIstr.BlkIstr(k).qual_unqual
  
      tb!Correzione = False
      'stefano: mancava...  ??COS'E'??
      If istruzione = "ISRT" Then
'        For i = 0 To UBound(TabIstrSSa)
'          If Trim(TabIstrSSa(i).SSAName) = Trim(TabIstr.BlkIstr(k).SSAName) Then
'            tb!stridoggetto = TabIstrSSa(i).idOggetto
'            tb!strIdArea = TabIstrSSa(i).idArea
'        'stefano: per i GSAM la precedenti if � comunque vera, anche se entrambi i valori sono vuoti!!
'        ' Per ora ho messo una pezza, ma perch� non caricare direttamente dalla ricerca per la psdlirel_segaree?
'        ' ed inserire la lunghezza dalla funzione apposita? Perch� non riporta anche l'ordinale?
'            If isGSAM Then
'            'purtroppo devo fare cos�, altrimenti lo rifaccio da capo...
'               Set rsArea = m_Fun.Open_Recordset("Select IdOggetto,IdArea,Byte From PsData_Cmp Where Nome = '" & TabIstr.BlkIstr(k).Record & "' And IdOggetto = " & GbIdOggetto)
'               If rsArea.RecordCount = 0 Then
'                  'Ricerca nelle COPY:
'                  Set rsLink = m_Fun.Open_Recordset("Select IdOggettoR From PsRel_Obj Where IdOggettoC = " & GbIdOggetto & " And Relazione = 'CPY'")
'                  Do While Not rsLink.EOF
'                    Set rsCmp = m_Fun.Open_Recordset("Select IdOggetto,IdArea,Byte From PsData_Cmp Where Nome = '" & TabIstr.BlkIstr(k).Record & "' And IdOggetto = " & rsLink!idOggettoR)
'                    If rsCmp.RecordCount Then
'                       Set rsArea = rsCmp
'                       Exit Do
'                    End If
'                    rsCmp.Close
'                    rsLink.MoveNext
'                  Loop
'                  rsLink.Close
'               Else
'                  tb!stridoggetto = rsArea!idOggetto
'                  tb!strIdArea = rsArea!idArea
'                  tb!AreaLen = rsArea!byte
'               End If
'               rsArea.Close
'            End If
'            Exit For
'          End If
'        Next
      End If
    
      'SQ - 27-05-05: aspettare la validita' delle chiavi per chiudere...
      If TabIstr.BlkIstr(k).qualified Then
        '''''''''''''''''''''''''''''''''''''''''''''''''
        'GESTIONE CHIAVI:
        'Cerca ID campo chiave
        'Allestisce tabella record in PsDli_IstrDett_Key
        'Setta la validita' dell'istruzione;
        '''''''''''''''''''''''''''''''''''''''''''''''''
        For y = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
          'SQ-2: temporaneo - valutare caso di segmenti multipli...
          'idField = CercaIdField(wIdSegm, TabIstr.BlkIstr(K).KeyDef(y).Key)
          If TabIstr.BlkIstr(k).IdSegm Then
             'piu' di un elemento: indeterminatezza
             'Mauro 19-03-2007 Multi-Segm: Sarebbe da gestire per Multi-Segmenti
             idField = CercaIdField(TabIstr.BlkIstr(k).IdSegm, TabIstr.BlkIstr(k).KeyDef(y).Key)
          Else
             idField = 0
          End If
          
          'SQ - 27-05-05
          If idField = 0 Then
            TabIstr.BlkIstr(k).valida = False 'istruzione sbagliata in ogni caso!
            IsValid = False
            '''''''''''''''''''''''''''''''''''''''
            ' SQ 5-05-06 - Nuova segnalazione (I04)
            '''''''''''''''''''''''''''''''''''''''
            addSegnalazione TabIstr.BlkIstr(k).KeyDef(y).Key, "SSAKEYNOTFOUND", TabIstr.BlkIstr(k).KeyDef(y).Key
          End If
    
          'SQ: spostata qui... era fuori ciclo e settava solo la chiave 1!?
          TabIstr.BlkIstr(k).KeyDef(y).xName = GbXName
          
          Set tb1 = m_Fun.Open_Recordset("select * from PsDli_IstrDett_Key")
          tb1.AddNew
          'SQ COPY-ISTR
          'tb1!idOggetto = GbIdOggetto
          tb1!idOggetto = IdOggettoRel
          tb1!idpgm = GbIdOggetto
          tb1!Riga = GbOldNumRec
          tb1!livello = k
          tb1!progressivo = y
          
          tb1!idField = idField
          tb1!idField_moved = TabIstr.BlkIstr(k).KeyDef(y).Key_moved
          'SQ: aspettare il via di Ste per mettere true se idField negativo.
          tb1!xName = TabIstr.BlkIstr(k).KeyDef(y).xName
    
          'OPERATORE
          tb1!operatore = TabIstr.BlkIstr(k).KeyDef(y).Op 'multiplo
          tb1!operatore_moved = TabIstr.BlkIstr(k).KeyDef(y).Op_moved
          
          'SQ: perche' se e' = ""??????????
          If TabIstr.BlkIstr(k).KeyDef(y).Key <> "Nothing" Then
            tb1!Valore = TabIstr.BlkIstr(k).KeyDef(y).KeyVal & ""
            tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
            'SQ: aggiungere colonna in DB!
            tb1!KeyStart = TabIstr.BlkIstr(k).KeyDef(y).KeyStart
          End If
          tb1!KeyLen = TabIstr.BlkIstr(k).KeyDef(y).KeyLen
          
          'SG : Se la lunghezza non � stata calcolata o calcolata male recupera l'effettiva lunghezza dal field dli
          If Val(tb1!KeyLen) <= 0 Or Trim(tb1!KeyLen) = "" Then
             tb1!KeyLen = Restituisci_DatiKey(tb1!idField, tb1!xName, "LUNGHEZZA")
             TabIstr.BlkIstr(k).KeyDef(y).KeyLen = tb1!KeyLen
             
             If Trim(UCase(TabIstr.BlkIstr(k).SSAName)) <> "" Then
                TabIstr.BlkIstr(k).KeyDef(y).KeyVarField = TabIstr.BlkIstr(k).SSAName & "(" & TabIstr.BlkIstr(k).KeyDef(y).KeyStart & ":" & TabIstr.BlkIstr(k).KeyDef(y).KeyLen & ")"
                tb1!KeyValore = TabIstr.BlkIstr(k).KeyDef(y).KeyVarField
             End If
          End If
          
          'OPERATORE LOGICO ("BOOLEANO")
          If Len(TabIstr.BlkIstr(k).KeyDef(y).KeyBool) = 0 Then
            tb1!Booleano = " "
          Else
            tb1!Booleano = TabIstr.BlkIstr(k).KeyDef(y).KeyBool
          End If
          tb1!booleano_moved = TabIstr.BlkIstr(k).KeyDef(y).KeyBool_moved
                  
          tb1!valida = TabIstr.BlkIstr(k).valida
          
          tb1!Correzione = False
          tb1.Update
          tb1.Close
        Next y
      End If
      
      tb!moved = TabIstr.BlkIstr(k).moved
      If Not TabIstr.BlkIstr(k).valida Then IsValid = False
      
      tb.Update
    Next k 'ITERAZIONE LIVELLI...
    tb.Close
  Else
    'Manca PSB/DBD
  End If
  
  ''''''''''''''''''''''''''''''''''''''''
  ' UPDATE "PsDli_Istruzioni":
  ''''''''''''''''''''''''''''''''''''''''
  UpdatePsdliIstruzioni (IsDC)
  
  ''''''''''''''''''''''''''''''''''''''''
  ' Segnalazioni
  ''''''''''''''''''''''''''''''''''''''''
  'Da eliminare : portare nelle rispettive posizioni...
  If Not IsValid Then
    If Len(TabIstr.DBD) And wIdDBD(0) = 0 Then  'NO DBD
      addSegnalazione dbdName, "NODBD", TabIstr.istr
    ElseIf Not segmentNotFound Then
      'SQ: qui si entra per esclusione!!!!!!!!!
      addSegnalazione "", "INVALIDDLIISTR", ""
    End If
     '''''AggiungiSegnalazione TabIstr.Istr, "NODECOD", TabIstr.Istr
  End If
  Exit Sub
parseErr:
  addSegnalazione parametri, "I00", parametri
  'Resume
End Sub

Public Sub parseISRT(istruzione As String, parametri As String)
  parseREPL istruzione, parametri
End Sub

Public Sub parseSSA_ETZ(ssaStruct As t_ssa)
  Dim Ordinale As Integer, codesIndex As Integer, valueIndex As Integer, keyLevel As Integer, redefineLevel As Integer
  Dim ssa As String
  Dim dataCmp() As dataCmp
  Dim dataCmpValues() As String
  Dim manageMove As Boolean
  Dim i As Integer
  On Error GoTo errSSA
    
  ReDim ssaStruct.Chiavi(0)
  ReDim ssaStruct.NomeSegmento.value(0)
  ReDim ssaStruct.Qualificazione.value(0)
  ReDim ssaStruct.CommandCode.value(0)
  
  'stefano: servono un po' di pezze, forse era meglio fare una funzione ad hoc...
  Dim SSAConstant As Boolean
  SSAConstant = False
  If InStr(ssaStruct.SSAName, "'") > 0 Then
     SSAConstant = True
  End If
  
  If Not SSAConstant Then
    manageMove = m_Fun.LeggiParam("IMSDC-MOVEYN") = "YES"
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 18-05-06
    ' Restituisce la struttura di campi con TUTTE le COPY esplose
    ' Es:  01 BLUTO.
    '         COPY GIORGIO.
    '         03 FIFFO ...
    '         COPY ALPREDO.
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    dataCmp = getDataCmp(ssaStruct.idOggetto, ssaStruct.idArea, ssaStruct.SSAName)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 28-06-06
    ' Restituisce l'array relativo alla dataCmp con i valori (di move)
    ' Esterno alla struttura della dataCmp che mappa la PsData_Cmp
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If manageMove Then
      dataCmpValues = getDataCmpValues(dataCmp)
    End If
  
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Costruisce l'SSA: da struttura COBOL a stringa (posizionale/IMS)
    ' (setta ssaStruct.moved)
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 28-02-07
    If UBound(dataCmp) = 0 And dataCmp(0).Lunghezza > 0 And dataCmp(0).moved Then
      'CASO "SPEZZAGAMBE"
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' SSA PIC X... le move sono al livello 01 di SSA complete...
      ' la struttura delle SSA non corrisponde (posizione elementi diversa)
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Dim ssaValues() As String
      ssaValues = Split(dataCmpValues(0), ",")
      If Len(dataCmpValues(0)) Then
        ssa = ssaValues(0)
        'Altre potenziali SSA:
        dataCmpValues(0) = Mid(dataCmpValues(0), Len(ssaValues(0)) + 2)
        'SQ TMP!!!!!!!!! STO PRENDENDO SOLO IL PRIMO!!!! (INVENTARSI UN CICLO...)
        m_Fun.writeLog "!!SSA IGNORATA - Istruzione: " & GbIdOggetto & "-" & GbOldNumRec & " -> " & dataCmpValues(0)
        dataCmpValues(0) = ""
      End If
    Else
      'CASO "NORMALE"
      ssa = getSSAstream(dataCmp, ssaStruct)
    End If
  Else
  'stefano: imposto l'ssa costante
    ssa = Mid(ssaStruct.SSAName, 2, Len(ssaStruct.SSAName) - 2) 'matteo: se vuoi usare un sistema migliore, visto che sei esperto...
  End If
  If Len(ssa) Then
    'la ssaStruct sarebbe la TabIstrSSa(UBound(TabIstrSSa))
    
    'SQ - portati fuori if...
'    ReDim ssaStruct.Chiavi(0)
'    ReDim ssaStruct.NomeSegmento.value(0)
'    ReDim ssaStruct.Qualificazione.value(0)
'    ReDim ssaStruct.CommandCode.value(0)
    
    ssaStruct.ssaStream = ssa
    
    ''''''''''''''''''''''''''''''''''''''''''''
    ' ANALISI SSA (pulita)
    ''''''''''''''''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''
    ' Segmento: 1-8
    '''''''''''''''''''''''''
    ssaStruct.NomeSegmento.value(0) = Left(ssa, 8)  'att!! FARE IL TRIM DOPO...
    ssaStruct.NomeSegmento.pos = 1
    ssaStruct.NomeSegmento.len = 8
    If manageMove And Not SSAConstant Then
      checkMove_SSA dataCmp, dataCmpValues, ssaStruct.NomeSegmento
    End If
    
    ''''''''''''''''''''''''''
    ' Tipo SSA - Char (9:1):
    ' - bianco: squalificata
    ' - "(":    qualificata
    ' - "*":    command code
    ' - "": squalificata
    ''''''''''''''''''''''''''
    Select Case Mid(ssa, 9, 1)
      Case "@"  'NO VALUE! VALORIZZATO EVIDENTEMENTE A RUN-TIME
        ssa = ""
        ssaStruct.valida = False
        'manageUnknownQualification(
      Case " "
      'stefano: verifica se c'� il buco per i command code, forzatura da rivedere!!!
        If InStr(ssa, "(") > 0 Then
          codesIndex = 13
          ssaStruct.Qualificatore = True
        Else
          'SQUALIFICATA
          ssaStruct.Qualificatore = False
          ssaStruct.Qualificazione.pos = 9
          ssaStruct.Qualificazione.len = 1
          ssaStruct.Qualificazione.value(0) = " "
          'SQ 5-07-06 continuo l'analisi per qualificazioni a run-time...
          If Len(ssa) > 9 And Right(ssa, 1) = ")" Then
            'ATTENZIONE: forzatura!
            'La value potrebbe avere space o niente! (CONSIDERIAMO COMUNQUE LO SPACE!)
            ssaStruct.Qualificatore = True
            ssaStruct.Qualificazione.moved = True
            codesIndex = 10 'lo uso come "base"
          Else
            ssa = ""
            ssaStruct.valida = True
            ssaStruct.Qualificatore = False
          End If
        End If
      Case "("
        'QUALIFICATA
        codesIndex = 10 'lo uso come "base"
        ssaStruct.Qualificatore = True
      Case "*"
        'CommandCodes: fino a " " o "("
        ssaStruct.CommandCode.value(0) = "*"
        ssaStruct.CommandCode.pos = 9
        For codesIndex = 10 To Len(ssa)
          Select Case Mid(ssa, codesIndex, 1)
            Case "@"  'NO VALUE! VALORIZZATO EVIDENTEMENTE A RUN-TIME
              ssa = ""
              ssaStruct.valida = False
            Case " "
              'SQUALIFICATA: continuo l'analisi per qualificazioni a run-time...
              ssa = ""
              ''codesIndex = codesIndex + 1 'salto la parentesi
              
              ssaStruct.valida = True
              ssaStruct.Qualificatore = False
              'SQ - move/value
              ssaStruct.Qualificazione.pos = codesIndex
              ssaStruct.Qualificazione.len = 1
              ssaStruct.Qualificazione.value(0) = " "
            Case "("
              ssaStruct.Qualificatore = True
              'SQ - move/value
              ssaStruct.Qualificazione.pos = codesIndex
              ssaStruct.Qualificazione.len = 1
              ssaStruct.Qualificazione.value(0) = "("
              codesIndex = codesIndex + 1 'salto la parentesi
              Exit For
            Case Else
              ssaStruct.CommandCode.value(0) = ssaStruct.CommandCode.value(0) & Mid(ssa, codesIndex, 1)
          End Select
        Next
      Case ""
        'SQUALIFICATA
        ssaStruct.Qualificatore = False
        ssaStruct.Qualificazione.pos = 9
        ssaStruct.Qualificazione.len = 1
        ssaStruct.Qualificazione.value(0) = " "
        ssaStruct.valida = True
        ssaStruct.Qualificatore = False
        ssa = ""
      Case Else
        ''''''''''''''''''''''''''''''''''''''''
        ' SQ - 5-05-06
        ' Nuova segnalazione (I03)
        ''''''''''''''''''''''''''''''''''''''''
        addSegnalazione ssa, "SSAWRONG", ssa
        ReDim ssaStruct.Chiavi(0)
        Exit Sub
    End Select
    
    'MOVE/VALUE "QUALIFICAZIONE"
    If manageMove And Not SSAConstant Then
      checkMove_SSA dataCmp, dataCmpValues, ssaStruct.Qualificazione
    End If
    
    'MOVE/VALUE "COMMAND CODES"
    If ssaStruct.CommandCode.pos Then
      ssaStruct.CommandCode.len = Len(ssaStruct.CommandCode.value(0))
      'TMP
      If manageMove And Not SSAConstant Then
        checkMove_SSA dataCmp, dataCmpValues, ssaStruct.CommandCode, True
      End If
      'Potrei avere SPACE sul c.c => Effetto collaterale sulla squalificazione:
      For i = 1 To UBound(ssaStruct.CommandCode.value)
        If InStr(ssaStruct.CommandCode.value(i), " ") Then
          ssaStruct.Qualificazione.moved = True
          ReDim ssaStruct.Qualificazione.value(UBound(ssaStruct.Qualificazione.value) + 1)
          ssaStruct.Qualificazione.value(UBound(ssaStruct.Qualificazione.value)) = " "
          Exit For
        End If
      Next
    End If
    
    ''''''''''''''''''''''''''
    ' Analisi "QUALIFICA"
    ''''''''''''''''''''''''''
    ReDim ssaStruct.OpLogico(0) 'init
    ReDim ssaStruct.OpLogico(0).value(0) 'init
    keyLevel = 1  'init
    While Len(ssa)
      ReDim Preserve ssaStruct.Chiavi(keyLevel)
      
      ''''''''''''''''''''''''''''''''''''''''''''
      ' CHIAVE. Posizione relativa=(codesIndex:8)
      ''''''''''''''''''''''''''''''''''''''''''''
      ssaStruct.Chiavi(keyLevel).Key.pos = codesIndex
      ssaStruct.Chiavi(keyLevel).Key.len = 8
      ReDim Preserve ssaStruct.Chiavi(keyLevel).Key.value(0)
      ssaStruct.Chiavi(keyLevel).Key.value(0) = Mid(ssa, codesIndex, 8)   'TRIMMARE DOPO!
      'TMP
      If manageMove And Not SSAConstant Then
        checkMove_SSA dataCmp, dataCmpValues, ssaStruct.Chiavi(keyLevel).Key
      End If
      
      ''''''''''''''''''''''''''''''''''''''
      ' OPERATORE. Posizione relativa=(9:2)
      '''''''''''''''''''''''''''''''''''''
      'SQ - gestione MOVE/VALUES
      'ssaStruct.Chiavi(keyLevel).operatore = Mid(ssa, codesIndex + 8, 2)
      ssaStruct.Chiavi(keyLevel).operatore.pos = codesIndex + 8
      ssaStruct.Chiavi(keyLevel).operatore.len = 2
      ReDim ssaStruct.Chiavi(keyLevel).operatore.value(0)
      ssaStruct.Chiavi(keyLevel).operatore.value(0) = Mid(ssa, codesIndex + 8, 2)
      'TMP
      If manageMove And Not SSAConstant Then
        checkMove_SSA dataCmp, dataCmpValues, ssaStruct.Chiavi(keyLevel).operatore
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''
      ' VALORE. Posizione relativa=(11:x). Termina con:
      ' + ")"
      ' + OPERATORE RELAZIONALE:
      '   - *,& (AND)
      '   - +,! (OR)
      '''''''''''''''''''''''''''''''''''''''''''''''''
      codesIndex = codesIndex + 10  'verificare
      For valueIndex = codesIndex To Len(ssa)
        Select Case Mid(ssa, valueIndex, 1)
          Case "+", "!", "|" 'OR 'IRIS-DB
            ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico) + 1)
            ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0)
            'SQ - gestione MOVE/VALUES
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0) = "+"
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).pos = valueIndex
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).len = 1
            If manageMove And Not SSAConstant Then
              checkMove_SSA dataCmp, dataCmpValues, ssaStruct.OpLogico(UBound(ssaStruct.OpLogico))
            End If

            ssaStruct.Chiavi(keyLevel).Valore.KeyStart = codesIndex
            ssaStruct.Chiavi(keyLevel).Valore.Lunghezza = valueIndex - codesIndex
            ssaStruct.Chiavi(keyLevel).Valore.Valore = "'" & Mid(ssa, codesIndex, valueIndex - codesIndex) & "'"
            
            'aggiorno la "base" (uso codesIndex come "startValueIndex")
            codesIndex = valueIndex + 1
            keyLevel = keyLevel + 1
            Exit For
          Case "*", "&" 'AND
            ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico) + 1)
            ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0)
            'SQ - gestione MOVE/VALUES
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0) = "*"
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).pos = valueIndex
            ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).len = 1
            If manageMove And Not SSAConstant Then
              checkMove_SSA dataCmp, dataCmpValues, ssaStruct.OpLogico(UBound(ssaStruct.OpLogico))
            End If
            
            ssaStruct.Chiavi(keyLevel).Valore.KeyStart = codesIndex
            ssaStruct.Chiavi(keyLevel).Valore.Lunghezza = valueIndex - codesIndex
            ssaStruct.Chiavi(keyLevel).Valore.Valore = "'" & Mid(ssa, codesIndex, valueIndex - codesIndex) & "'"
            'aggiorno la "base" (uso codesIndex come "startValueIndex")
            codesIndex = valueIndex + 1
            keyLevel = keyLevel + 1
            Exit For
          Case ")"
            'FINE!
            ssaStruct.Chiavi(keyLevel).Valore.KeyStart = codesIndex
            ssaStruct.Chiavi(keyLevel).Valore.Lunghezza = valueIndex - codesIndex '+ 1
            ssaStruct.Chiavi(keyLevel).Valore.Valore = "'" & Mid(ssa, codesIndex, valueIndex - codesIndex) & "'"
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Caso particolare!
            'Es.: "SPJROOT *--(PPJKEY  >=87                  &PPJKEY  <=87                  )"
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Len(ssa) > valueIndex And Right(ssa, 1) = ")" Then
              ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico) + 1)
              ReDim Preserve ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0)
              
              'SQ - gestione MOVE/VALUES
              ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).value(0) = ")" '!!!
              ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).pos = valueIndex
              ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).len = 1
              If manageMove And Not SSAConstant Then
                checkMove_SSA dataCmp, dataCmpValues, ssaStruct.OpLogico(UBound(ssaStruct.OpLogico))
              End If
              ssaStruct.OpLogico(UBound(ssaStruct.OpLogico)).moved = True 'FORZATO!
              
              ssaStruct.Chiavi(keyLevel).Valore.KeyStart = codesIndex
              ssaStruct.Chiavi(keyLevel).Valore.Lunghezza = valueIndex - codesIndex
              ssaStruct.Chiavi(keyLevel).Valore.Valore = "'" & Mid(ssa, codesIndex, valueIndex - codesIndex) & "'"
              'aggiorno la "base" (uso codesIndex come "startValueIndex")
              codesIndex = valueIndex + 1
              keyLevel = keyLevel + 1
              Exit For
            Else
              ssaStruct.valida = True
              ssa = ""
            End If
          Case Else
            'NOP
            'Non mi serve il valore effettivo
        End Select
      Next
    Wend
    
    'TMP
    If manageMove And Not SSAConstant Then
'''      checkMove dataCmp, ssaStruct
    End If
  Else
    ssaStruct.valida = False
    ''''''''''''''''''''''''''''''''''''''''
    ' SQ - 19-04-06
    ' Nuova segnalazione (I02)
    ''''''''''''''''''''''''''''''''''''''''
    addSegnalazione ssaStruct.SSAName, "NOSSAVALUE", ssaStruct.SSAName
    
  End If
  
  '''''''''''''''''''''''
  ' Update ssaCache
  '''''''''''''''''''''''
  ssaCache(UBound(ssaCache)) = ssaStruct
  ReDim Preserve ssaCache(UBound(ssaCache) + 1)
  Exit Sub
errSSA:
  ssaStruct.valida = False
  'Resume
  'CAMBIARE LA SEGNALAZIONE!
  addSegnalazione ssa, "SSAWRONG", ssa
End Sub

Function getDataCmpValues(dataCmp() As dataCmp) As String()
  Dim rs As Recordset
  Dim i As Integer
  Dim dataCmpValues() As String
  Dim Valore As String
  Dim idOggettoBak As Long

  'volevo utilizzare una matrice ma non so la sintassi!
  On Error GoTo funErr

  ReDim dataCmpValues(UBound(dataCmp))

  For i = 0 To UBound(dataCmp)
    Set rs = m_Fun.Open_Recordset( _
      "SELECT valore From PsData_CmpMoved WHERE" & _
      " idPgm=" & GbIdOggetto & " And Nome='" & Replace(dataCmp(i).nome, "'", "") & "'" _
    )
    'VIRGILIO 1/3/2007 Ho inserito la Replace per le REPLACING tipo : =='WS'== BY ==WS==

    dataCmp(i).moved = Not rs.EOF

    While Not rs.EOF
      'VARIABILE/COSTANTI (perfezionare...)
      If Left(rs!Valore, 1) = "'" Then
       Valore = Replace(rs!Valore, "'", "")
      Else
        Valore = getFormatFieldValue(rs!Valore, dataCmp(i).byte)
      End If
      If Len(Valore) Then dataCmpValues(i) = dataCmpValues(i) & Valore & ","
      rs.MoveNext
    Wend
    rs.Close
    'ultima virgola
    If Len(dataCmpValues(i)) Then dataCmpValues(i) = Left(dataCmpValues(i), Len(dataCmpValues(i)) - 1)
  Next
  getDataCmpValues = dataCmpValues
  Exit Function
funErr:
  'MsgBox "tmp: getDataCmpValues - gestire..."
  'Resume
End Function

Public Function getDataCmp(idOggetto As Long, idArea As Long, nome As String) As dataCmp()
  Dim rs As Recordset
  Dim Ordinale As Integer, livello As Integer
  Dim dataCmp() As dataCmp
  Dim Posizione As Long, posizioneBase As Long
  
  On Error GoTo funErr
  
  ReDim dataCmp(0)
  
  posizioneBase = 1 'init
  
  'SQ - 29-09-06: errore mortifero... non consideravo il livello...
  Set rs = m_Fun.Open_Recordset("Select Ordinale,livello From PsData_Cmp Where idOggetto = " & idOggetto & " And IdArea = " & idArea & " And Nome = '" & nome & "'")
  If rs.RecordCount Then
    Ordinale = rs!Ordinale
    livello = rs!livello
    rs.Close
    Set rs = m_Fun.Open_Recordset("Select * From PsData_Cmp Where idOggetto = " & idOggetto & " And IdArea = " & idArea & " And ordinale >= " & Ordinale & " order by ordinale")
    Do While Not rs.EOF
      If rs!livello > livello Or rs!Ordinale = Ordinale Then
        ReDim Preserve dataCmp(Posizione)
        dataCmp(Posizione).idOggetto = idOggetto
        dataCmp(Posizione).idArea = idArea
        dataCmp(Posizione).Ordinale = rs!Ordinale
        dataCmp(Posizione).livello = rs!livello
        dataCmp(Posizione).Segno = rs!Segno & ""
        dataCmp(Posizione).byte = rs!byte
        dataCmp(Posizione).Lunghezza = rs!Lunghezza
        dataCmp(Posizione).Valore = rs!Valore
        dataCmp(Posizione).nomeRed = rs!nomeRed & ""
        dataCmp(Posizione).nome = rs!nome
        dataCmp(Posizione).Posizione = rs!Posizione + posizioneBase - 1
        If rs!idCopy > 0 Then
          dataCmp(Posizione).idCopy = rs!idCopy
          addToDataCmp dataCmp, Posizione, livello
          'devo proseguire con il posizionamento: ho aggiunto campi di copy!
          posizioneBase = dataCmp(Posizione).Posizione + dataCmp(Posizione).byte
        End If
        Posizione = Posizione + 1
        rs.MoveNext
      Else
        'Altra AREA
        Exit Do
      End If
    Loop
  End If
  rs.Close
  getDataCmp = dataCmp
  Exit Function
funErr:
  'MsgBox "tmp: getDataCmp - gestire..."
  'Resume
End Function

Private Sub addToDataCmp(dataCmp() As dataCmp, Posizione As Long, livello As Integer)
  Dim rs As Recordset
  Dim codice As String
  Dim idOggetto_Copy As Long, posizioneBase As Long
  
  idOggetto_Copy = dataCmp(Posizione).idCopy
  posizioneBase = dataCmp(Posizione).Posizione + IIf(dataCmp(Posizione).Lunghezza, dataCmp(Posizione).byte, 0) 'le copy hanno posizionamenti relativi!
  
  'SQ 5-07-06
  'Posso avere copy con pi� tracciati, anche se ho lo 01 fuori...
  'Set rs = m_Fun.Open_Recordset("Select * From PsData_Cmp Where idOggetto = " & idOggetto_Copy & " order by ordinale")
  Set rs = m_Fun.Open_Recordset("Select * From PsData_Cmp Where idOggetto = " & idOggetto_Copy & " and idArea = 1 order by ordinale")
  Do While Not rs.EOF
    If rs!livello > livello Then
      Posizione = Posizione + 1
      ReDim Preserve dataCmp(Posizione)
      dataCmp(Posizione).idOggetto = idOggetto_Copy
      dataCmp(Posizione).idArea = rs!idArea
      dataCmp(Posizione).Ordinale = rs!Ordinale
      dataCmp(Posizione).livello = rs!livello
      dataCmp(Posizione).Segno = rs!Segno
      dataCmp(Posizione).byte = rs!byte
      dataCmp(Posizione).Lunghezza = rs!Lunghezza
      dataCmp(Posizione).Valore = rs!Valore
      dataCmp(Posizione).nomeRed = rs!nomeRed
      dataCmp(Posizione).nome = rs!nome
      dataCmp(Posizione).Posizione = rs!Posizione + posizioneBase - 1
      rs.MoveNext
    Else
      'Altra AREA
      Exit Do
    End If
  Loop
  rs.Close
End Sub

Private Sub checkMove(dataCmp() As dataCmp, ssaStruct As t_ssa)
  Dim i As Integer, j As Integer
  
  ssaStruct.moved = False 'init
  
  For i = 0 To UBound(dataCmp)
    'MOVE sulla SSA
    If dataCmp(i).moved Then
      'Scarto le MOVE sulle "VALUE"
      For j = 1 To UBound(ssaStruct.Chiavi)
        If dataCmp(i).Posizione >= ssaStruct.Chiavi(j).Valore.KeyStart Then
          If dataCmp(i).byte + dataCmp(i).Posizione <= ssaStruct.Chiavi(j).Valore.KeyStart + ssaStruct.Chiavi(j).Valore.Lunghezza Then
            Exit For
          End If
        End If
      Next
      If j > UBound(ssaStruct.Chiavi) Then
        ssaStruct.moved = True
        Exit For
      End If
    End If
  Next
End Sub

Private Sub checkMove_SSA(dataCmp() As dataCmp, dataCmpValues() As String, ssaItem As SSA_ITEM, Optional combine As Boolean = False)
  Dim i As Integer, j As Integer, subFieldPos As Integer, subFieldLen As Integer, k As Integer, Z As Integer
  Dim values() As String
  Dim itemValue_init As String, itemValue_end As String
  Dim ValuesForCombination() As Collection
  Dim Combinazioni() As Integer
  
  ssaItem.moved = False 'init

  If combine Then
    ReDim ValuesForCombination(ssaItem.len - 1)
    ReDim Combinazioni(ssaItem.len - 1)
    For j = 0 To UBound(ValuesForCombination)
      Set ValuesForCombination(j) = New Collection
      ' prendo il primo valore preso dall'inizializzazione
      ' considero solo le posizioni interessate dalla move
      ValuesForCombination(j).Add Mid(ssaItem.value(0), j + 1, 1)
    Next
    For j = 0 To UBound(Combinazioni)
      Combinazioni(j) = 1
    Next
  End If

  For i = 0 To UBound(dataCmp)
    'MOVE sulla SSA
    If dataCmp(i).moved Then
      subFieldPos = 0 'init (usato come flag sotto...)
      'SQ
      'Campo interno a ssaItem (o coincidente)
      If (dataCmp(i).Posizione >= ssaItem.pos) And (dataCmp(i).byte + dataCmp(i).Posizione <= ssaItem.pos + ssaItem.len) Then
        ssaItem.moved = True
        subFieldPos = 1
        subFieldLen = dataCmp(i).byte
        itemValue_init = Left(ssaItem.value(0), dataCmp(i).Posizione - ssaItem.pos)
        itemValue_end = Mid(ssaItem.value(0), subFieldPos + subFieldLen + 1)
      'parte finale campo dentro (intersezione a sinistra o compreso)
      ElseIf (dataCmp(i).Posizione < ssaItem.pos) And (dataCmp(i).byte + dataCmp(i).Posizione > ssaItem.pos) And (dataCmp(i).byte + dataCmp(i).Posizione <= ssaItem.pos + ssaItem.len) Then
        ssaItem.moved = True
        subFieldPos = ssaItem.pos - dataCmp(i).Posizione + 1
        subFieldLen = dataCmp(i).Posizione + dataCmp(i).byte - ssaItem.pos
        itemValue_init = "" 'init
        itemValue_end = Right(ssaItem.value(0), ssaItem.len - subFieldLen)  'verificare...
      ElseIf (dataCmp(i).Posizione > ssaItem.pos) And (dataCmp(i).Posizione < ssaItem.pos + ssaItem.len) And (dataCmp(i).byte + dataCmp(i).Posizione >= ssaItem.pos + ssaItem.len) Then
        'parte iniziale campo dentro (intersezione a destra o compreso)
        ssaItem.moved = True
        subFieldPos = 1
        'subFieldLen = ssaItem.pos + ssaItem.len - (1 + dataCmp(i).byte)
        subFieldLen = ssaItem.pos + ssaItem.len - dataCmp(i).Posizione + 1
        itemValue_init = "" 'init
        itemValue_end = Left(ssaItem.value(0), ssaItem.len - subFieldLen)  'verificare...
      'campo comprende ssa
      ElseIf (dataCmp(i).Posizione <= ssaItem.pos) And (dataCmp(i).byte + dataCmp(i).Posizione >= ssaItem.pos + ssaItem.len) Then
        ssaItem.moved = True
        subFieldPos = ssaItem.pos - dataCmp(i).Posizione + 1
        subFieldLen = ssaItem.len
        itemValue_init = "" 'init
        itemValue_end = "" 'init
      End If

     If subFieldPos Then  'NON POSSO USARE QUESTO FLAG (ENTREREI PER TUTTI I CAMPI MOVED SUCCESSIVI A QUELLO VERO!!!)
     'Indice 0: valore della "PIC VALUE"
      values = Split(dataCmpValues(i), ",")
      
      Dim ssaMovedValue As String
      If Not combine Then
        For j = 0 To UBound(values)
          ssaMovedValue = itemValue_init & Mid(values(j), subFieldPos, subFieldLen) & itemValue_end
          'Lo tengo solo se diverso da quello iniziale
          If ssaMovedValue <> ssaItem.value(0) Then
            ReDim Preserve ssaItem.value(UBound(ssaItem.value) + 1)
            ssaItem.value(UBound(ssaItem.value)) = ssaMovedValue 'indice 0: value della PIC
          End If
        Next
      Else  ' calcolo le combinazioni (commandcode)
        For j = 0 To UBound(values) ' per tutti i valori
         For k = Len(itemValue_init) To Len(itemValue_init) + subFieldLen - 1  ' per tutte le posizioni interessate
            ' scorro collection per vedere se elemento present
            For Z = 1 To ValuesForCombination(k).count
              If ValuesForCombination(k).item(Z) = Mid(values(j), k - Len(itemValue_init) + 1, 1) Then
                Exit For
              End If
            Next
            If Z = ValuesForCombination(k).count + 1 Then
               ValuesForCombination(k).Add Mid(values(j), k - Len(itemValue_init) + 1, 1)
            End If
          Next
        Next
       
      End If
  
      'Se sono in un campo di gruppo devo controllare anche i sottocampi:
      'If dataCmp(i).Lunghezza Then
        'Exit For
      'End If
     End If
    End If
  Next
  
  If combine Then
  ' ciclo su elementi e costruisco combinazioni sul sottoinsieme
  ' concatenendo le eventuali parti fisse iniziale e finale
    ssaMovedValue = ""
    While AggiornaCombinazioneSSAItem(Combinazioni, ValuesForCombination)
      For j = 0 To UBound(ValuesForCombination)
        ssaMovedValue = ssaMovedValue & ValuesForCombination(j).item(Combinazioni(j))
      Next
      ReDim Preserve ssaItem.value(UBound(ssaItem.value) + 1)
      ssaItem.value(UBound(ssaItem.value)) = ssaMovedValue 'indice 0: value della PIC
      ssaMovedValue = ""
    Wend
  End If
End Sub

Public Function getSSAstream(dataCmp() As dataCmp, ssaStruct As t_ssa) As String
  Dim i As Integer, redefineLevel As Integer
  Dim isRedefines As Boolean
  Dim ssa As String, ssaTemp As String
  
  ' Mauro 21/12/207 : Sar� una cagata, ma io, al momento, la metto lo stesso!
  ' Come posso avere una Value su un livello 01 che poi viene ridefinito con altre Value?!?!
  If Len(dataCmp(0).Valore) Then
    getSSAstream = dataCmp(0).Valore
    Exit Function
  End If
  
  While i <= UBound(dataCmp)
    'MOVE sulla SSA
    If dataCmp(i).moved Then
      ssaStruct.moved = True  'Sbagliato?!... prob. da togliere (fa tutto la checkMove)
    End If
    
    If isRedefines Then
      If dataCmp(i).livello <= redefineLevel Then
        isRedefines = False
      Else
        'redefines: salto
        i = i + 1
      End If
    Else
      If Len(Trim(dataCmp(i).nomeRed)) Then
        isRedefines = True
        redefineLevel = dataCmp(i).livello
        'redefines: salto
        i = i + 1
      Else
        If dataCmp(i).Lunghezza Then
          ssaTemp = formatSsaValue(IIf(IsNull(dataCmp(i).Valore), "@", Trim(dataCmp(i).Valore)), dataCmp(i).byte & "", dataCmp(i).Segno, dataCmp(i).nome)
'''MG          ssaTemp = formatSsaValue(IIf(IsNull(dataCmp(i).Valore), "@", dataCmp(i).Valore), dataCmp(i).byte & "", dataCmp(i).Segno, dataCmp(i).nome)
          ssa = ssa & ssaTemp
        Else
          'campo di gruppo: ignoro
        End If
        i = i + 1
      End If
    End If
  Wend
 getSSAstream = Trim(ssa)
'''MG   getSSAstream = ssa
  
End Function

Private Function formatSsaValue(value As String, bytes As String, Segno As String, nome As String) As String
  Select Case value
    Case "@"
      formatSsaValue = " "
      'NO VALUE!
      'tmp...
      If Not nome = "" Then
        addSegnalazione nome, "NOSSAVALUE", nome
      End If
    Case "LOW-VALUE", "LOW-VALUES", "HIGH-VALUE", "HIGH-VALUES"
      formatSsaValue = "."
    Case "SPACE", "SPACES"
      formatSsaValue = " "
    Case "ZERO", "ZEROES", "ZEROS"
      formatSsaValue = "0"
    Case Else
      If Left(value, 1) = "'" Then
        formatSsaValue = Replace(value, "'", "")
      Else
        If Segno = "S" Then
          formatSsaValue = Replace(value, "+", "") 'il "+" � operatore relazionale... darebbe fastidio!
        Else
          formatSsaValue = value
        End If
      End If
  End Select
  'Lunghezza esatta:
  If Len(formatSsaValue) < bytes Then
    formatSsaValue = formatSsaValue & Space(bytes - Len(formatSsaValue))
  End If
End Function
Private Sub getSegment(istruzione As String, parametri As String)
  Dim rs As Recordset
  Dim token As String, dbdName As String, wNomePsb As String, ssaValue As String, fileName As String
  Dim pcbNumber As Integer
  
  On Error GoTo parseErr

  '''''''''''''''''''''''''''''''''
  ' ANALISI PARAMETRI:
  '''''''''''''''''''''''''''''''''
  'Pcb
  '''token = nextToken_tmp(parametri)
  '''If token = "C" Then
  '''  TabIstr.pcb = nextToken_tmp(parametri)
  '''Else
  '''  MsgBox "PCB???" & token
  '''End If
  
  fileName = nexttoken(parametri)
  
  Dim ioArea As String
  ioArea = nexttoken(parametri)
  '''''''''''''''''''''
  ' PCB (number)
  '''''''''''''''''''''
  TabIstr.numpcb = getPCBnumber(fileName)
  
  IsDC = False
  
  If IsDC Then
    parseRECEIVE istruzione, ioArea
    Exit Sub
  End If

  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Gestione SSA
  ' Valorizzo la TabIstrSSa
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim ssa
  ReDim wDliSsa(0)
  ReDim gSSA(0)
  ReDim TabIstrSSa(0)
  ReDim TabIstrSSa(0).NomeSegmento.value(0)
  Dim ssaOk As Boolean
  ssaOk = True
  Dim i As Integer
  While Len(parametri)
    i = i + 1
    ReDim Preserve wDliSsa(UBound(wDliSsa) + 1)
    ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
    ReDim Preserve TabIstrSSa(UBound(gSSA) + 1)
    wDliSsa(UBound(wDliSsa)) = nexttoken(parametri)

    ''''''''AGGIUSTARE... PER ORA NON SERVE!
    Dim rsArea As Recordset, rsLink As Recordset, rsCmp As Recordset
    Set rsArea = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & GbIdOggetto)
    If rsArea.RecordCount = 0 Then
      'Ricerca nelle COPY:
      Set rsLink = m_Fun.Open_Recordset("Select IdOggettoR From PsRel_Obj Where IdOggettoC = " & GbIdOggetto & " And Relazione = 'CPY'")
      Do While Not rsLink.EOF
        Set rsCmp = m_Fun.Open_Recordset("Select IdOggetto,IdArea From PsData_Cmp Where Nome = '" & wDliSsa(UBound(wDliSsa)) & "' And IdOggetto = " & rsLink!idOggettoR)
        If rsCmp.RecordCount Then
           Set rsArea = rsCmp
           Exit Do
        End If
        rsLink.MoveNext
      Loop
      rsLink.Close
    End If

    'Valorizza TabIstrSSa:
    If rsArea.RecordCount Then
      'SQ - tmp: mettere in linea...
      'Non funge per indirizzo? (in testa?)
      ReDim Preserve TabIstrSSa(UBound(TabIstrSSa) + 1)
      TabIstrSSa(UBound(TabIstrSSa)).SSAName = wDliSsa(UBound(wDliSsa))
      TabIstrSSa(UBound(TabIstrSSa)).idOggetto = rsArea!idOggetto
      TabIstrSSa(UBound(TabIstrSSa)).idArea = rsArea!idArea
      parseSSA_ETZ TabIstrSSa(UBound(TabIstrSSa))
    Else
      ''''''''''''''''''''''''''''''''''''''''
      ' SQ - 19-04-06
      ' Nuova segnalazione (I02)
      ''''''''''''''''''''''''''''''''''''''''
      addSegnalazione wDliSsa(UBound(wDliSsa)), "NOSSAVALUE", wDliSsa(UBound(wDliSsa))
      ssaOk = False
    End If
    rsArea.Close
  Wend
  
  If UBound(TabIstrSSa) Then
    getTabIstr
  End If
  ''''''''''''''''''''''''''''''''
  ' Gestione PSB/DBD:
  ' Input: N PSB, PCB, M SEGMENTI
  ''''''''''''''''''''''''''''''''
  getInstructionPsbDbd (TabIstr.numpcb)
  
  'getTabIstr
  For i = 1 To UBound(TabIstr.BlkIstr)
    'SQ 9-05-06
    'If Len(TabIstr.BlkIstr(i).segment) Then
    '  getSegmentsId TabIstr.BlkIstr(i).segment
    'Else
    '  ReDim IdSegmenti(0)
    'End If
    'Valorizza IdSegmenti (tutti N segmenti: per ora non usato) e
    'le info relative al segmento del TabIstr.BlkIstr(i)
    getSegmentsId TabIstr.BlkIstr(i)
    
    ''''''''''''''''''''''''''''
    ' Ne trovo N e ne uso 1...
    ''''''''''''''''''''''''''''
    If Len(IdSegmenti(0)) Then ' � stringa!
      TabIstr.BlkIstr(i).IdSegm = IdSegmenti(0)
    Else
      TabIstr.BlkIstr(i).SegLen = 0
      '''''''''''''''''''''''''''''''''''''
      ' Segnalazione
      '''''''''''''''''''''''''''''''''''''
    End If
    ''''''''''''''''''''''''''''''''''
    ' Nome Routine?!
    ''''''''''''''''''''''''''''''''''
    '''TabIstr.BlkIstr(i).NomeRout = CreaNomeRout(TabIstr.DBD)
  Next
  
  Exit Sub
parseErr:
  'tmp
  MsgBox err.description
End Sub
'Effetto Collaterale: setta isDC
'Specifico EASY: setta TabIstr.DBD!
'Private Function getPCBnumber(fileName As String) As Integer
Private Function getPCBnumber(fileName As String, Optional programma) As Integer
  Dim pcb As Integer
  Dim rsPSB As Recordset
  Dim rs As Recordset
  
  IsDC = False
  '''''''''''''''''''''''''''''''''''''''''
  ' Numero PCB
  '''''''''''''''''''''''''''''''''''''''''
  ' simulava una retrieve come una GN, per poi gestirla in modo diverso nel parser di II; equiparato
  'If programma = "DLI" Then
    Set rs = m_Fun.Open_Recordset("Select * from PsEZ_FILE where filetype = 'DLI' and IdOggetto = " & GbIdOggetto & " AND fileName='" & fileName & "'")
  'Else
  '  Set rs = m_Fun.Open_Recordset("Select * from PsEZ_FILE where IdOggetto = " & GbIdOggetto & " AND dbd='" & fileName & "'")
  'End If
  TabIstr.DBD = "" 'stefano: direi che � il caso, se no c'� quello vecchio, ma azzerare tutta la tabistr no???
  If rs.RecordCount Then
    pcb = rs!pcb
    TabIstr.DBD = rs!DBD
    
    ''''''''''''''''''''''''''''''''''
    ' isDC
    ''''''''''''''''''''''''''''''''''
    If SwTpIMS Then
      If pcb = 0 Then
        'PCB-IO ==> IMS/DC
        IsDC = True
      Else
        'PCB-ALTERNATE ==> IMS/DC
        If pcb < (UBound(gbCurEntryPCB) + 1) Then
          Set rsPSB = m_Fun.Open_Recordset("Select TypePCB from PsDLI_Psb where IdOggetto = " & IdPsb & " and numPCB = " & getPCBnumber)
          If Not rsPSB.EOF Then
            If Trim(rsPSB!TypePcb) = "TP" Then
              IsDC = True
            End If
          End If
          rsPSB.Close
        End If
      End If
    End If
  Else
    m_Fun.writeLog "###idOggetto: " & GbIdOggetto & " - EasyTrieve FILE not found."
  End If
  
  getPCBnumber = pcb
  
End Function

Private Function getStatement(fdIN As Long, fdOUT As Long, alreadyReadLine As String) As String
  Dim line As String
  Dim bIsComment As Boolean
  
  'On Error GoTo errToken
  
  If Len(alreadyReadLine) > 0 Then
    'gia' letta dall'istruzione precedente
    line = Trim(Left(alreadyReadLine, 72))
    alreadyReadLine = "" 'pulisco per i giri dopo
  Else
    Line Input #fdIN, line
    GbNumRec = GbNumRec + 1
    line = Trim(Left(line, 72))
    GbOldNumRec = GbNumRec
  End If
'stefano: se ho capito, serve a dire quante righe sono nello statement
 '' fdOUT = 1 'SQ ragazzi: state facendo dei danni!? FD � il file descriptor per l'i/o
  
  Do While Not EOF(fdIN)
    If Left(line, 1) <> "*" Then
''      getStatement = Trim(getStatement) & line
      'SQ 12-06-07 baco! Se getStatement � vuoto aggiungiamo un bianco in testa! (poi non funzionano pi� i controlli sul primo char)
      getStatement = Trim(getStatement) & IIf(Len(getStatement), " ", "") & line  'fare qualche test
      'CONTINUATIONS:
      ' Mauro 11/07/2007: Non basta il "+", anche il "-" funziona da concatenatore
''      If InStr(line, "+  ") > 0 Or Right(line, 1) = "+" Then
      '??????? Perch� il + con due spazi ?????
      If Right(line, 1) = "+" Or Right(line, 1) = "-" Then
        getStatement = Left(getStatement, Len(getStatement) - 1)
        Line Input #fdIN, line
        GbNumRec = GbNumRec + 1
        line = " " & Trim(Left(line, 72))
        ' Mauro 11/07/2007: Non basta il "+", anche il "-" funziona da concatenatore
''        If Right(line, 1) = "+" Then
        If Right(line, 1) = "+" Or Right(line, 1) = "-" Then
          line = Trim(line)
        End If
       '' fdOUT = fdOUT + 1
        'getStatement = getStatement & line
      Else
        Dim endStatement As Integer
        endStatement = InStr(getStatement, ".")
        If endStatement Then
          splitStatement getStatement, alreadyReadLine
        End If
        Exit Do
      End If
    Else
      'COMMENTS:
      'SQ 7-11-06 - Manteniamo i commenti:
      If Ezt2cobol Then
        'If Trim(line) <> "*" Then
          tagWrite line
        'End If
      End If
      Line Input #fdIN, line
      GbNumRec = GbNumRec + 1
      'SQ 12-06-07 - Sbagliava il numero di riga! (se le prime righe sono commento)
      If GbNumRec = GbOldNumRec + 1 Then
        GbOldNumRec = GbNumRec  'SQ fare qualche test...
      End If
      line = Trim(Left(line, 72))
    End If
  Loop
    
  'SQ 7-11-06 - Manteniamo i commenti:
  If EOF(fdIN) Then
    'Ultima linea:
    If Left(line, 1) <> "*" Then
      getStatement = getStatement & line
    Else
      getStatement = ""
      'COMMENTS:
      If Ezt2cobol Then
        tagWrite line
      End If
    End If
  End If
End Function

Private Function getStatement_Loop(fdIN As Long, fdOUT As Long, alreadyReadLine As String) As String
  Dim line As String
  Dim bIsComment As Boolean
  Dim oldNumRec As Long
  Dim NumRec As Long
  'On Error GoTo errToken
  
  If Len(alreadyReadLine) > 0 Then
    'gia' letta dall'istruzione precedente
    line = alreadyReadLine
    alreadyReadLine = "" 'pulisco per i giri dopo
  Else
    Line Input #fdIN, line
    NumRec = NumRec + 1
    line = Trim(Left(line, 72))
    oldNumRec = NumRec
  End If
  
  Do While Not EOF(fdIN)
    If Left(line, 1) <> "*" Then
      getStatement_Loop = Trim(getStatement_Loop) & line
      'CONTINUATIONS:
      If InStr(1, line, "+  ") > 0 Or Right(line, 1) = "+" Then
        getStatement_Loop = Left(getStatement_Loop, Len(getStatement_Loop) - 1)
        Line Input #fdIN, line
        NumRec = NumRec + 1
        line = " " & Trim(Left(line, 72))
        If Right(line, 1) = "+" Then
          line = Trim(line)
        End If
        'getStatement = getStatement & line
      Else
        Dim endStatement As Integer
        endStatement = InStr(getStatement_Loop, ".")
        If endStatement Then
          splitStatement getStatement_Loop
        End If
        Exit Do
      End If
    Else
      'COMMENTS:
      'SQ 7-11-06 - Manteniamo i commenti:
      If Ezt2cobol Then
        tagWrite line
      End If
      Line Input #fdIN, line
      NumRec = NumRec + 1
      line = Trim(Left(line, 72))
    End If
  Loop
    
  'SQ 7-11-06 - Manteniamo i commenti:
  If EOF(fdIN) Then
    'Ultima linea:
    If Left(line, 1) <> "*" Then
      getStatement_Loop = line
    Else
      getStatement_Loop = ""
      'COMMENTS:
      If Ezt2cobol Then
        tagWrite line
      End If
    End If
  End If
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''
' Assessment: relazioni/segnalazioni
'''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getMacro(statement As String)
  Dim macro As String, token As String
  
  statement = Mid(statement, 2)
  macro = nexttoken(statement)
  'SQ 2-07-07
  If Right(macro, 1) = "." Then
    macro = Left(macro, Len(macro) - 1)
  End If
  If Not Ezt2cobol Then
    '''''''''''''''''''''''''''''''''''''''''''''''''
    ' Inserimento istruzione in PsCall: (trasformare in execute INSERT)
    '''''''''''''''''''''''''''''''''''''''''''''''''
    Dim rsMacro As Recordset
    Set rsMacro = m_Fun.Open_Recordset("select * from PsPgm_MACRO")
    rsMacro.AddNew
    rsMacro!idpgm = GbIdOggetto
    rsMacro!Riga = GbOldNumRec
    rsMacro!macro = macro  'attenzione: puo' contenere apici...
    'parametri: char(70)
    rsMacro!parametri = Left(statement, 70)
    If Len(statement) > 70 Then
      m_Fun.writeLog "!!idOggetto: " & GbIdOggetto & " - macro: " & macro & ": parameters truncated (greater than 70 chars)..."
    End If
    rsMacro.Update
    rsMacro.Close
  End If
  
  MapsdM_Parser.checkRelations macro, "EZM"
  
  statement = ""

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''
' MF - 13/07/07
' Gestione tabella PsMacro
'''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getMacroParameters(statement As String)
  Dim macro As String, token As String
  
  'statement = Mid(statement, 2)
  'macro = nextToken(statement)

  If Right(statement, 1) = "." Then
    macro = Left(macro, Len(macro) - 1)
  End If
  If Not Ezt2cobol Then
    '''''''''''''''''''''''''''''''''''''''''''''''''
    ' Inserimento istruzione in PsCall: (trasformare in execute INSERT)
    '''''''''''''''''''''''''''''''''''''''''''''''''
    Dim rsMacroParam As Recordset
    Set rsMacroParam = m_Fun.Open_Recordset("select * from PsMacro")
    If statement <> "" Then
      rsMacroParam.AddNew
      rsMacroParam!idOggetto = GbIdOggetto
      'parametri: char(70)
      rsMacroParam!parametri = Left(Trim(statement), 70)
      If Len(statement) > 70 Then
        m_Fun.writeLog "!!idOggetto: " & GbIdOggetto & " - macro: " & macro & ": parameters truncated (greater than 70 chars)..."
      End If
      rsMacroParam.Update
      rsMacroParam.Close
    End If
  End If
  
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getDLI(statement As String, linee As Long)
  Dim token As String, fileName As String, instruction As String
  Dim ioArea As String
  Dim strSSA() As String
  Dim i As Long, k As Long, j As Integer
  Dim line As String, newline As String
  Dim x As Boolean
  
  On Error GoTo errDLI
  
  If Not Ezt2cobol Then
    SwDli = True
    
    token = nexttoken(statement)
    If token = "CHKP" Or token = "XRST" Then
    ' stefano: un po' forzato...
       statement = token & " " & statement
       getCALL linee, "'DLI' " & statement
       checkPSB statement
    Else
      fileName = token
      token = nexttoken(statement)
      If token <> "FOR" Then
        ioArea = token
        instruction = nexttoken(statement)
        token = nexttoken(statement)
        If Len(token) Then
          If token = "SSANO" Then
            token = nexttoken(statement)
          End If
          If token = "SSA" Then
            statement = Replace(Replace(statement, "(", ""), ")", "")
      'stefano: SSA costanti, gi� corretto nel parser del PLI, ma perch� qui � tutto duplicato?
'            statement = Trim(Replace(statement, "'", ""))
          End If
        End If
        
        'verifica se si tratta di una sola SSA o +
        If InStr(statement, " ") > 0 Then
        'stefano: non funge, ci sono le ssa costanti!!!!
          'strSSA = Split(statement, " ")
          ReDim strSSA(0)
          i = 0
          Dim statement_save As String
          statement_save = statement
          While token <> ""
            token = nextToken_new(statement_save) 'MANTIENE GLI APICI!!
            If token <> "" Then
            ReDim Preserve strSSA(i)
              strSSA(i) = token
              i = i + 1
            End If
          Wend
        Else
          ReDim strSSA(0)
          strSSA(0) = statement
        End If
        
        statement = instruction & " " & fileName & " " & ioArea & " " & statement
        
        getCALL linee, "'DLI' " & statement
        
        checkPSB statement
        
        ''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ !!!
        ' A parte rileggere il file 100 volte, ma
        ' FA DEI DANNI MOSTRUOSI... RICHIAMA la getDEFINE
        ' sporcando tutte le variabili d'ambiente!
        ' ==> NON SERVE NEANCHE PIU: VENGONO GIA' SCRITTI TUTTI I CAMPI!
        ''''''''''''''''''''''''''''''''''''''''''''''''''
        'MG ricavare tutta la struttura dell'SSA.... x il momento
        '.... solo se si tratta di SSA
        'If InStr(statement, "SSA") > 0 Then
        '  x = False
        '  i = FreeFile
        '  Open GbFileInput For Input As i
        '  While Not EOF(i)
        '    DoEvents
        '    line = getStatement_Loop(i, k, newLine)
''''    '        line = getStatement(i, k, newLine)
        '    For j = 0 To UBound(strSSA)
        '    'stefano: SSA costanti!!!!!!
        '      If Not InStr(strSSA(j), "'") > 0 Then
        '       If InStr(line, strSSA(j) & " ") > 0 Then
        '       If (Left(Trim(line), Len(strSSA(j) & " ")) <> strSSA(j) And Left(Trim(line), 3) <> "DLI" And Left(Trim(line), 7) <> "DISPLAY") Or x <> True Then
        '          getDEFINE line, "", x
        '          x = True
        '        End If
        '       End If
        '      End If
        '    Next
        '  Wend
        '  Close i
        'End If
      Else
    ' stefano: un po' forzato...
       statement = token & " " & fileName & " " & statement
       getCALL linee, "'DLI' " & statement
       checkPSB statement
      End If
    End If
  End If
  
  statement = ""
  Exit Sub
errDLI:
  MsgBox err.description
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Per DLI. Syntax:
' FILE-name DLI (literal-1|dbd-name[literal-2]|space [RESET])
' STATUS-CODE  011 02 A
' - Imposta "currentFILE"
' - Analizza i file DLI
' - Aggiunge anche lo status code
' Per TABLE
' - mangia e scarta l'eventuale tabella INSTREAM
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getFILE(FD As Long, statement As String, line As String)
Dim fileName As String, token As String
Dim dbdName As String
Dim pcbNumber As Integer
Dim testStatus1 As String      '011
Dim testStatus2 As String      '02
Dim statusName As String
Dim disposition As String
Dim blrecl As Long
Dim recl As Long
Dim str1 As String
Dim str2 As String
Dim filetype As String
Dim additional As String
Dim rsCmp As Recordset
Dim rsArea As Recordset
Dim RecMode As String
Dim Appo_Statement As String
  On Error GoTo catch
  
  'STEFANO: ho aggiunto la gestione per tutti i file: andrebbe migliorata, al fine di fare un unico giro
  '  ed una sola insert
 
  Appo_Statement = "FILE " & statement
  
  fileName = nexttoken(statement)
  fileName = Replace(fileName, ".", "")
  CurrentFILE = fileName
  'SQ 26-11-07 portato dentro le IF
  'If Not Ezt2cobol Then
  token = nexttoken(statement)
  If token = "DLI" Then
    If Not Ezt2cobol Then
      If Left(statement, 1) = "(" And Right(statement, 1) = ")" Then
        statement = Mid(Left(statement, Len(statement) - 1), 2)
        dbdName = nexttoken(statement)
        Dim pcbPos As Integer
        If Len(statement) Then
          'literal-2:
          token = nexttoken(statement)
          If IsNumeric(token) Then
            'stefano: no il valore calcolato era sempre 1, la cosa � un p' pi� complicata:
            ' quella � la posizione del PCB con lo stesso DBD all'interno del PSB, chiaro?!
            ' se assente, significa la prima ricorrenza
            pcbPos = token
          Else
'            MsgBox "PCB???: " & token
            pcbPos = 1
          End If
        Else
          pcbPos = 1
        End If
        'stefano: ora cerca il pcb number all'interno del PSB; qualcuno faccia una funzione, lettura di troppo
        Dim rsPSB As Recordset
        Set rsPSB = m_Fun.Open_Recordset("select * from psdli_Psb as a, psrel_obj as b  " & _
                                         "where a.idoggetto = b.idoggettoR and b.idoggettoC = " & GbIdOggetto)
        Dim pcbPos2 As Integer
        pcbNumber = 0
        pcbPos2 = 0
        If rsPSB.EOF Then
          pcbNumber = 1
          m_Fun.writeLog "###idOggetto: " & GbIdOggetto & " - PSB not found; default PCB assigned"
        Else
          While Not rsPSB.EOF And pcbPos2 < pcbPos
            If rsPSB!dbdName = dbdName Then
              pcbPos2 = pcbPos2 + 1
              pcbNumber = rsPSB!numpcb
            End If
            rsPSB.MoveNext
          Wend
          If Not pcbPos2 = pcbPos Then pcbNumber = 1
        End If
        rsPSB.Close
        'ricava lo status-code
        Do While Not EOF(FD)
          Line Input #FD, statement
          GbNumRec = GbNumRec + 1
          'prima pezza, almeno si ferma al successivo file
          If InStr(statement, "FILE ") > 0 Then
            line = statement
            statusName = ""
            Exit Do
          End If
          'seconda pezza, almeno si ferma alla working-storage
          If InStr(statement, " W ") > 0 Or _
            InStr(statement, " S ") > 0 Then
            line = statement
            statusName = ""
            Exit Do
          End If
          'stefano: da modificare con l'uso del pcb; ci ho provato, ma � complicato al momento
          statusName = nexttoken(statement)
          testStatus1 = nexttoken(statement)
          testStatus2 = nexttoken(statement)
          If (testStatus1 = "011" Or testStatus1 = "11") And (testStatus2 = "02" Or testStatus2 = "2") Then Exit Do
        Loop

        filetype = "DLI"
        Parser.PsConnection.Execute "INSERT INTO PsEZ_FILE " & _
                                    "(idOggetto,fileName,dbd,pcb, status_Code, filetype) " & _
                                    "VALUES " & _
                                    "(" & GbIdOggetto & ",'" & fileName & "','" & dbdName & "'," & pcbNumber & ",'" & statusName & "', '" & filetype & "')"
      Else
        m_Fun.writeLog "###idOggetto: " & GbIdOggetto & " - FILE format???: " & fileName & " " & statement
      End If
    Else
      'Ezt2cobol
      parseStatement FD, Appo_Statement, "DLI"
    End If
  ElseIf token = "TABLE" Then
   '' If Not Ezt2cobol Then
      ' Mauro 11/07/2007 : non mi serve CurrentFILE, tanto non ho nessuna struttura da associargli
      ' Per un file di tipo TABLE ho sempre 2 campi associati: ARG e DESC
      CurrentFILE = ""
      getTABLE token, statement, FD, fileName
      
   '' End If
  ElseIf token = "PRINTER" Or token = "VIRTUAL" Then
    If Not Ezt2cobol Then
      ' Mauro 11/07/2007 : non mi serve CurrentFILE, tanto non ho nessuna struttura da associargli
      CurrentFILE = ""
      'non so come metterci null, sarebbe meglio che su access fosse vuota la cella
      blrecl = 0
      recl = 0
      filetype = "PRINTER"
      
      If token = "VIRTUAL" Then
        additional = token
        filetype = "SEQUENTIAL"
      End If
      token = nexttoken(statement)
      disposition = token
      token = nexttoken(statement)
      str1 = ""
      str2 = ""
      If Left(token, 1) = "(" Then
        token = Replace(Replace(token, ")", ""), "(", "")
        str1 = nexttoken(token)
        If IsNumeric(str1) Then recl = CLng(str1)
        str2 = nexttoken(token)
        If IsNumeric(str2) Then blrecl = CLng(str2)
      End If
      If Not IsNumeric(str1) Then additional = additional & str1 & " " & str2
      Parser.PsConnection.Execute "INSERT INTO PsEZ_FILE " & _
                                  "(idOggetto,fileName, filetype, disposition, blrecl, recl, additional) " & _
                                  "VALUES " & _
                                  "(" & GbIdOggetto & ",'" & fileName & "','" & filetype & "', '" & disposition & "', " & blrecl & "," & recl & ",'" & additional & "')"
    Else
      If token <> "PRINTER" Then
        changeTag RTB, "<ASSIGN(i)>", "SELECT " & fileName & " ASSIGN TO S-" & fileName & vbCrLf _
                                    & IIf(Len(filetype), "       ORGANIZATION IS " & filetype & vbCrLf, "") _
                                    & IIf(Len(filetype), "       ACCESS MODE IS " & filetype & ".", ".")
        
        changeTag RTB, "<FD(i)>", "FD  " & fileName & vbCrLf & _
                                  IIf(Len(RecMode), "   RECORDING MODE IS " & RecMode & vbCrLf, "") & _
                                  "   BLOCK CONTAINS 0" & vbCrLf & _
                                  "   LABEL RECORDS ARE STANDARD." & vbCrLf & _
                                  "01 " & fileName & "-REC     PIC X(132)."
        'SILVIA
        'changeTag RTB, "<FILE-STATUS(i)>", "01 " & fileName & "-STATUS" & Space(30 - Len(fileName & "-STATUS")) & " PIC X(02) VALUE SPACES."
        changeTag RTB, "<CLOSE-FILES(i)>", "CLOSE " & fileName
        ReDim Preserve fileEZT(UBound(fileEZT) + 1)
        fileEZT(UBound(fileEZT)).fileName = fileName
        fileEZT(UBound(fileEZT)).openInput = False
      End If
    End If
  ElseIf token = "FB" Or token = "F" Or token = "VS" Or token = "VB" Or token = "V" Or token = "BUFNO" Or token = "" Then
    disposition = token
    RecMode = Switch(token = "F", "F", _
          token = "FB", "", _
          token = "VS", "", _
          token = "VB", "V", _
          token = "V", "V", _
          token = "BUFNO", "", _
          True, token)
    token = nexttoken(statement)
    
    'non so come metterci null, sarebbe meglio che su access fosse vuota la cella
    blrecl = 0
    recl = 0
    If disposition = "VS" Then
      filetype = "INDEXED"
    Else
      filetype = "SEQUENTIAL"
    End If
          
    str1 = ""
    str2 = ""
    additional = ""
    If Left(token, 1) = "(" Then
      token = Replace(Replace(token, ")", ""), "(", "")
      str1 = nexttoken(token)
      If IsNumeric(str1) Then recl = CLng(str1)
      str2 = nexttoken(token)
      If IsNumeric(str2) Then blrecl = CLng(str2)
      ' Mauro 19/07/2007 : Non � detto che questo per forza la lunghezza del record
    Else
      additional = token & Space(1)
    End If
    If Len(statement) Then additional = additional & Trim(statement)
    If Not IsNumeric(str1) Then additional = additional & str1 & " " & str2
    token = nexttoken(statement)
    If token = "TABLE" Then
      If Not Ezt2cobol Then
        ' Mauro 17/07/2007 : non mi serve CurrentFILE, tanto non ho nessuna struttura da associargli
        CurrentFILE = ""
        getTABLE token, statement, FD, fileName
      End If
    Else
      ' Mauro 13/07/2007 : Aggiungo il FILLER anche nella datalist
      dataList(listIndex).attributes = ""
      If RecMode = "V" Then
        dataList(listIndex).attributes = "V"
      End If
      dataList(listIndex).byteBusy = 0
      dataList(listIndex).bytes = 100000000
      dataList(listIndex).dataDec = 0
      dataList(listIndex).dataLen = recl
      dataList(listIndex).fieldName = fileName & "-REC"
      dataList(listIndex).fieldPosition = 1
      dataList(listIndex).fieldType = "A"
      dataList(listIndex).fieldValue = ""
      dataList(listIndex).groupName = ""
      dataList(listIndex).Index = ""
      dataList(listIndex).livello = "01"
      dataList(listIndex).location = ""
      dataList(listIndex).Occurs = 0
      dataList(listIndex).Offset = 1
      dataList(listIndex).Redefines = ""
      dataList(listIndex).Segno = ""
      listIndex = listIndex + 1
      
      If Not Ezt2cobol Then
        Parser.PsConnection.Execute "INSERT INTO PsEZ_FILE " & _
          "(idOggetto,fileName, filetype, disposition, blrecl, recl, additional) " & _
          "VALUES " & _
          "(" & GbIdOggetto & ",'" & fileName & "','" & filetype & "', '" & disposition & "', " & blrecl & "," & recl & ",'" & additional & "')"
      Else
        'AC NOTE entry SELECT FILE PRINTER
        'due rami, se token PRINTER sostituisco ASSIGN-REPORT e FD-REPORT
        If token = "PRINTER" Then
          ' le dichiarazioni di SELECT e FD spostata nella gestione dei report
          'implementare controllo che verifica che tutti i file di tipo printer siano stati associati
          ' ad un report
          ReDim Preserve arrFilePrinter(UBound(arrFilePrinter) + 1)
          arrFilePrinter(UBound(arrFilePrinter)) = fileName
        Else
          changeTag RTB, "<ASSIGN(i)>", "SELECT " & fileName & " ASSIGN TO S-" & fileName & vbCrLf _
                                      & IIf(Len(filetype), "       ORGANIZATION IS " & filetype & vbCrLf, "") _
                                      & "       FILE STATUS IS " & fileName & "-STATUS" & vbCrLf _
                                      & IIf(Len(filetype), "       ACCESS MODE IS " & filetype & ".", ".")
          
          changeTag RTB, "<FD(i)>", "FD  " & fileName & vbCrLf & _
                                    IIf(Len(RecMode), "   RECORDING MODE IS " & RecMode & vbCrLf, "") & _
                                    "    BLOCK CONTAINS 0" & vbCrLf & _
                                    "    LABEL RECORDS ARE STANDARD" & vbCrLf & _
                                    "    <FD-" & fileName & "(i)>"
        End If
       'silvia
       'changeTag RTB, "<FILE-STATUS(i)>", "01 " & fileName & "-STATUS" & Space(30 - Len(fileName & "-STATUS")) & " PIC X(02) VALUE SPACES."
        changeTag RTB, "<FILE-STATUS(i)>", "77 " & fileName & "-STATUS" & Space(20 - Len(fileName & "-STATUS")) & " PIC X(02)." & vbCrLf & _
                                           "  88 OK-" & fileName & Space(20 - Len("OK-" & fileName)) & " VALUE '00' '04'." & vbCrLf & _
                                           "  88 EOF-" & fileName & Space(20 - Len("EOF-" & fileName)) & " VALUE '10'." & vbCrLf & _
                                           "  88 NF-" & fileName & Space(20 - Len("NF-" & fileName)) & " VALUE '23'."
        
        changeTag RTB, "<CLOSE-FILES(i)>", "CLOSE " & fileName
        ReDim Preserve fileEZT(UBound(fileEZT) + 1)
        fileEZT(UBound(fileEZT)).fileName = fileName
        fileEZT(UBound(fileEZT)).openInput = False
      End If
    End If
  ElseIf token = "SYSNAME" Then 'gi� UNIX
    'scarta le aggiunte per UNIX
    token = nexttoken(statement)
    token = nexttoken(statement)
    'non so come metterci null, sarebbe meglio che su access fosse vuota la cella
    blrecl = 0
    recl = 0
    If token = "FB" Or token = "F" Or token = "" Then
      filetype = "SEQUENTIAL"
    ElseIf token = "PRINTER" Then
      filetype = "PRINTER"
    ElseIf token = "VIRTUAL" Then
      additional = token
      filetype = "SEQUENTIAL"
    Else
      filetype = "VSAM"
    End If
    RecMode = token
    RecMode = Switch(token = "F", "F", _
          token = "FB", "", _
          token = "VS", "", _
          token = "VB", "V", _
          token = "V", "V", _
          token = "BUFNO", "", _
          True, token)
   
    token = nexttoken(statement)
    str1 = ""
    str2 = ""
    If Left(token, 1) = "(" Then
      token = Replace(Replace(token, ")", ""), "(", "")
      str1 = nexttoken(token)
      If IsNumeric(str1) Then recl = CLng(str1)
      str2 = nexttoken(token)
      If IsNumeric(str2) Then blrecl = CLng(str2)
    End If
    If Len(statement) Then additional = Trim(statement)
    If Not IsNumeric(str1) Then additional = additional & str1 & " " & str2
    ' Mauro 13/07/2007 : Aggiungo il FILLER anche nella datalist
    dataList(listIndex).attributes = ""
    If RecMode = "V" Then
      dataList(listIndex).attributes = "V"
    End If
    dataList(listIndex).byteBusy = 0
    dataList(listIndex).bytes = 100000000
    dataList(listIndex).dataDec = 0
    dataList(listIndex).dataLen = recl
    dataList(listIndex).fieldName = fileName & "-REC"
    dataList(listIndex).fieldPosition = 1
    dataList(listIndex).fieldType = "A"
    dataList(listIndex).fieldValue = ""
    dataList(listIndex).groupName = ""
    dataList(listIndex).Index = ""
    dataList(listIndex).livello = "01"
    dataList(listIndex).location = ""
    dataList(listIndex).Occurs = 0
    dataList(listIndex).Offset = 1
    dataList(listIndex).Redefines = ""
    dataList(listIndex).Segno = ""
    listIndex = listIndex + 1
    If Not Ezt2cobol Then
      Parser.PsConnection.Execute "INSERT INTO PsEZ_FILE " & _
          "(idOggetto,fileName, filetype, disposition, blrecl, recl, additional) " & _
          "VALUES " & _
          "(" & GbIdOggetto & ",'" & fileName & "','" & filetype & "', '" & disposition & "', " & blrecl & "," & recl & ",'" & additional & "')"
    Else
      changeTag RTB, "<ASSIGN(i)>", "SELECT " & fileName & " ASSIGN TO S-" & fileName & vbCrLf _
                                  & IIf(Len(filetype), "       ORGANIZATION IS " & filetype & vbCrLf, "") _
                                  & "       FILE STATUS IS " & fileName & "-STATUS" & vbCrLf _
                                  & IIf(Len(filetype), "       ACCESS MODE IS " & filetype & ".", ".")
      
      changeTag RTB, "<FD(i)>", "FD  " & fileName & vbCrLf & _
                                IIf(Len(RecMode), "   RECORDING MODE IS " & RecMode & vbCrLf, "") & _
                                "   BLOCK CONTAINS 0" & vbCrLf & _
                                "   LABEL RECORDS ARE STANDARD" & vbCrLf & _
                                "   <FD-" & fileName & "(i)>"
      changeTag RTB, "<FILE-STATUS(i)>", "01 " & fileName & "-STATUS" & Space(20 - Len(fileName & "-STATUS")) & " PIC X(02) VALUE SPACES."
      changeTag RTB, "<CLOSE-FILES(i)>", "CLOSE " & fileName
      ReDim Preserve fileEZT(UBound(fileEZT) + 1)
      fileEZT(UBound(fileEZT)).fileName = fileName
      fileEZT(UBound(fileEZT)).openInput = False
    End If
  ' Mauro 30/11/2007 : Mi sembra molto simile al SYSNAME
  ElseIf token = "DISK" Then 'gi� UNIX
    token = nexttoken(statement)
    'non so come metterci null, sarebbe meglio che su access fosse vuota la cella
    blrecl = 0
    recl = 0
    If token = "FB" Or token = "F" Or token = "" Then
      filetype = "SEQUENTIAL"
    ElseIf token = "PRINTER" Then
      filetype = "PRINTER"
    ElseIf token = "VIRTUAL" Then
      additional = token
      filetype = "SEQUENTIAL"
    Else
      filetype = "VSAM"
    End If
    RecMode = token
    RecMode = Switch(token = "F", "F", _
          token = "FB", "", _
          token = "VS", "", _
          token = "VB", "V", _
          token = "V", "V", _
          token = "BUFNO", "", _
          True, token)
    token = nexttoken(statement)
    str1 = ""
    str2 = ""
    If Left(token, 1) = "(" Then
      token = Replace(Replace(token, ")", ""), "(", "")
      str1 = nexttoken(token)
      If IsNumeric(str1) Then recl = CLng(str1)
      str2 = nexttoken(token)
      If IsNumeric(str2) Then blrecl = CLng(str2)
    End If
    If Len(statement) Then additional = Trim(statement)
    If Not IsNumeric(str1) Then additional = additional & str1 & " " & str2
    ' Mauro 13/07/2007 : Aggiungo il FILLER anche nella datalist
    dataList(listIndex).attributes = ""
    If RecMode = "V" Then
     dataList(listIndex).attributes = "V"
    End If
    dataList(listIndex).byteBusy = 0
    dataList(listIndex).bytes = 100000000
    dataList(listIndex).dataDec = 0
    dataList(listIndex).dataLen = recl
    dataList(listIndex).fieldName = fileName & "-REC"
    dataList(listIndex).fieldPosition = 1
    dataList(listIndex).fieldType = "A"
    dataList(listIndex).fieldValue = ""
    dataList(listIndex).groupName = ""
    dataList(listIndex).Index = ""
    dataList(listIndex).livello = "01"
    dataList(listIndex).location = ""
    dataList(listIndex).Occurs = 0
    dataList(listIndex).Offset = 1
    dataList(listIndex).Redefines = ""
    dataList(listIndex).Segno = ""
    listIndex = listIndex + 1
    If Not Ezt2cobol Then
      Parser.PsConnection.Execute "INSERT INTO PsEZ_FILE " & _
          "(idOggetto,fileName, filetype, disposition, blrecl, recl, additional) " & _
          "VALUES " & _
          "(" & GbIdOggetto & ",'" & fileName & "','" & filetype & "', '" & disposition & "', " & blrecl & "," & recl & ",'" & additional & "')"
    Else
      changeTag RTB, "<ASSIGN(i)>", "SELECT " & fileName & " ASSIGN TO S-" & fileName & vbCrLf _
                                  & IIf(Len(filetype), "       ORGANIZATION IS " & filetype & vbCrLf, "") _
                                  & "       FILE STATUS IS " & fileName & "-STATUS" & vbCrLf _
                                  & IIf(Len(filetype), "       ACCESS MODE IS " & filetype & ".", ".")
      
      changeTag RTB, "<FD(i)>", "FD  " & fileName & vbCrLf & _
                                IIf(Len(RecMode), "   RECORDING MODE IS " & RecMode & vbCrLf, "") & _
                                "   BLOCK CONTAINS 0" & vbCrLf & _
                                "   LABEL RECORDS ARE STANDARD" & vbCrLf & _
                                "   <FD-" & fileName & "(i)>"
      changeTag RTB, "<FILE-STATUS(i)>", "01 " & fileName & "-STATUS" & Space(20 - Len(fileName & "-STATUS")) & " PIC X(02) VALUE SPACES."
      changeTag RTB, "<CLOSE-FILES(i)>", "CLOSE " & fileName
      ReDim Preserve fileEZT(UBound(fileEZT) + 1)
      fileEZT(UBound(fileEZT)).fileName = fileName
      fileEZT(UBound(fileEZT)).openInput = False
    End If
  ElseIf token = "SQL" Then
    'SILVIA 02-02-2009
    If Ezt2cobol Then
      statement = Replace(statement, "(", "")
      statement = Replace(statement, ")", "")
      statement = Replace(statement, "FROM", vbCrLf & "FROM")
      statement = Replace(statement, "WHERE", vbCrLf & "WHERE")
      statement = Replace(statement, "AND", vbCrLf & "AND")
      statement = Replace(statement, ",", "," & vbCrLf)
      StINTO = Mid(statement, InStr(statement, "INTO"))
      statement = Mid(statement, 1, InStr(statement, "INTO") - 1)
      changeTag RTB, "<OPEN-FILES(i)>", "EXEC SQL DECLARE " & fileName & " CURSOR FOR " & vbCrLf & _
                    statement & vbCrLf & "END-EXEC. " & vbCrLf
      
      changeTag RTB, "<CLOSE-FILES(i)>", "EXEC SQL CLOSE " & fileName & " END-EXEC."
      ReDim Preserve fileEZT(UBound(fileEZT) + 1)
      fileEZT(UBound(fileEZT)).fileName = fileName
      fileEZT(UBound(fileEZT)).openInput = False
      fileEZT(UBound(fileEZT)).filetype = "SQL"
    End If
  ElseIf token = "IDMS" Then
  ElseIf token = "HOSTDISK" Then
  Else
    'non ci interessa (per ora)
    ' Mauro 10/07/2007 : Adesso ci interessa!!!! Senn� non so a cosa associare la struttura
    If Not Mid(token, 1, 1) = "&" Then
      filetype = token
      additional = ""
      token = nexttoken(statement)
    End If
    additional = token
    Do While Len(statement)
      token = nexttoken(statement)
      additional = additional & " " & token
    Loop
    ' Mauro 13/07/2007 : Aggiungo il FILLER anche nella datalist
    dataList(listIndex).attributes = ""
    If RecMode = "V" Then
      dataList(listIndex).attributes = "V"
    End If
    dataList(listIndex).byteBusy = 0
    dataList(listIndex).bytes = 100000000
    dataList(listIndex).dataDec = 0
    dataList(listIndex).dataLen = recl
    dataList(listIndex).fieldName = fileName & "-REC"
    dataList(listIndex).fieldPosition = 1
    dataList(listIndex).fieldType = "A"
    dataList(listIndex).fieldValue = ""
    dataList(listIndex).groupName = ""
    dataList(listIndex).Index = ""
    dataList(listIndex).livello = "01"
    dataList(listIndex).location = ""
    dataList(listIndex).Occurs = 0
    dataList(listIndex).Offset = 1
    dataList(listIndex).Redefines = ""
    dataList(listIndex).Segno = ""
    listIndex = listIndex + 1
    If Not Ezt2cobol Then
        Parser.PsConnection.Execute "INSERT INTO PsEZ_FILE " & _
          "(idOggetto,fileName, filetype, disposition, blrecl, recl, additional) " & _
          "VALUES " & _
          "(" & GbIdOggetto & ",'" & fileName & "','" & filetype & "', '" & disposition & "', " & blrecl & "," & recl & ",'" & additional & "')"
    Else
      changeTag RTB, "<ASSIGN(i)>", "SELECT " & fileName & " ASSIGN TO S-" & fileName & vbCrLf _
                                  & IIf(Len(filetype), "       ORGANIZATION IS " & filetype & vbCrLf, "") _
                                  & "       FILE STATUS IS " & fileName & "-STATUS" & vbCrLf _
                                  & IIf(Len(filetype), "       ACCESS MODE IS " & filetype & ".", ".")
      
      changeTag RTB, "<FD(i)>", "FD  " & fileName & vbCrLf & _
                                IIf(Len(RecMode), "   RECORDING MODE IS " & RecMode & vbCrLf, "") & _
                                "   BLOCK CONTAINS 0" & vbCrLf & _
                                "   LABEL RECORDS ARE STANDARD" & vbCrLf & _
                                "   <FD-" & fileName & "(i)>" & "."
      changeTag RTB, "<FILE-STATUS(i)>", "01 " & fileName & "-STATUS" & Space(20 - Len(fileName & "-STATUS")) & " PIC X(02) VALUE SPACES."
      changeTag RTB, "<CLOSE-FILES(i)>", "CLOSE " & fileName
      ReDim Preserve fileEZT(UBound(fileEZT) + 1)
      fileEZT(UBound(fileEZT)).fileName = fileName
      fileEZT(UBound(fileEZT)).openInput = False
    End If
  End If
  ' Mauro 30/11/2007: ho tutti i dati per inserire l'ASSIGN
  statement = ""
  Exit Sub
catch:
  m_Fun.Show_MsgBoxError "RC0002", , "getFILE", err.source, err.description, False
End Sub

'SILVIA 02-02-2009
Private Sub getSQL(statement As String)
  ' SQL INCLUDE LOCATION W FROM CREATOR.TABLE ..
  ' SQL INCLUDE FROM CREATOR.TABLE ..
  ' SQL INCLUDE FROM CREATOR.ALIAS_TABLE ..
  Dim token As String, tableName As String, TipoDB As String
  Dim rs As Recordset, rstable As Recordset
  Dim nomeTabella As String
   
  If BolSql = False Then
    tagWrite "EXEC SQL INCLUDE SQLCA END-EXEC.", "WORKING-STORAGE(i)"
    BolSql = True
  End If
  token = nexttoken(statement)
  Do While Len(statement)
    token = nexttoken(statement)
    Select Case token
      Case "LOCATION"
      Case "FROM"
        If InStr(statement, ".") Then
          statement = Mid(statement, InStr(statement, ".") + 1)
        End If
        TipoDB = m_Fun.GetFirstParam("DM_DEFSQLTYPE")
        TipoDB = Switch(TipoDB = "ORACLE", "ORC", TipoDB = "DB2", "DB2")
        nomeTabella = "MISSING TABLE"
        Set rstable = m_Fun.Open_Recordset("select * from DM" & TipoDB & "_Tabelle where Nome = '" & statement & "'")
        If rstable.RecordCount = 0 Then
          rstable.Close
          Set rs = m_Fun.Open_Recordset("select * from DM" & TipoDB & "_Alias where Nome = '" & statement & "'")
          If Not rs.EOF Then
            Set rstable = m_Fun.Open_Recordset("select * from DM" & TipoDB & "_Tabelle where idTable = " & rs!IdTable)
            If rstable.RecordCount Then
              If Len(rstable!nome) Then
                nomeTabella = rstable!nome
              End If
            End If
            rstable.Close
          End If
          rs.Close
        Else
          nomeTabella = rstable!nome
          rstable.Close
        End If
        'SILVIA: bisogna impostare la NAMING CONVENTION DELLA DCL = NOMETABELLA
        statement = indent & "EXEC SQL INCLUDE " & nomeTabella & " END-EXEC."
        tagWrite statement, "WORKING-STORAGE(i)"
        statement = ""
      Case Else
    End Select
  Loop
End Sub
'MF - 19/07/07
'Gestione inserimento dati in tabella PsEZ_TABLE
Private Sub getTABLE(token As String, statement As String, FD As Long, fileName As String) 'optional
Dim blrecl As Long
Dim recl As Long
Dim tableType As String
Dim additional As String
Dim str1 As String, str2 As String, str3 As String
Dim entryNum As String
Dim argBase As String, argLen As String, argDec As String, argType As String
Dim descBase As String, descLen As String, descDec As String, descType As String
Dim arrayArg() As String, arrayDesc() As String, arrayInstream() As String
Dim i As Long
Dim line As String, stline(1) As String
Dim eztf As eztData
Dim cblField As cblData

  entryNum = "0"
  ReDim Preserve arrayArg(3)
  ReDim Preserve arrayDesc(3)
  ReDim arrayInstream(0)
  token = nexttoken(statement)
  tableType = token
  'stefano: l'ho corretto per le macro, ma a che serve parsare sto TABLE che poi non ci facciamo niente?
  ' comunque arrivava a fine programma, non parsando pi� niente
  If token = "INSTREAM" Then
    Do While Not EOF(FD)
      'token = nextToken(statement)
      'Line Input #FD, statement
      'GbNumRec = GbNumRec + 1
      statement = getStatement(FD, FD, statement)
      'tableType = token
      token = nexttoken(statement)
      If token = "ARG" Then
        i = 0
        'If Len(statement) Then additional = additional & Trim(statement)
        Do Until Len(statement) = 0
          token = nexttoken(statement)
          arrayArg(i) = token
          i = i + 1
        Loop
      End If
      If token = "DESC" Then
        i = 0
        'If Len(statement) Then additional = additional & Trim(statement)
        Do Until Len(statement) = 0
          token = nexttoken(statement)
          arrayDesc(i) = token
          i = i + 1
        Loop
        i = 0
        statement = getStatement(FD, FD, statement)
        Do Until statement = "ENDTABLE"
          arrayInstream(i) = statement
          statement = getStatement(FD, FD, "")
          i = i + 1
          ReDim Preserve arrayInstream(i)
        Loop
      End If
      If token = "ENDTABLE" Or Left(Trim(token), 1) = "%" Then
        'FINE!
        Exit Do
      End If
    Loop
  Else
    If Left(token, 1) = "(" Then
      token = Replace(Replace(token, ")", ""), "(", "")
      entryNum = nexttoken(token)
      token = nexttoken(statement)
    End If
    tableType = token
    Do Until Len(statement) = 0
      token = nexttoken(statement)
      If Left(token, 1) = "(" Then
        token = Replace(Replace(token, ")", ""), "(", "")
        str1 = nexttoken(token)
        If IsNumeric(str1) Then recl = CLng(str1)
        str2 = nexttoken(token)
        If IsNumeric(str2) Then blrecl = CLng(str2)
      Else
        additional = additional & " " & token
      End If
    Loop
   
    Do While Not EOF(FD)
      statement = getStatement(FD, FD, line)
      token = nexttoken(statement)
      If token = "ARG" Then
        i = 0
        Do Until Len(statement) = 0
          token = nexttoken(statement)
          arrayArg(i) = token
          i = i + 1
        Loop
      End If
      If token = "DESC" Then
        i = 0
        Do Until Len(statement) = 0
          token = nexttoken(statement)
          arrayDesc(i) = token
          i = i + 1
        Loop
        Exit Do
      End If
    Loop
  End If
  
  ' Mauro 21/12/2007
  ' Come faceva a funzionare mettendo dei campi a spazio in colonne di tipo numerico?!?!?
  If Len(arrayArg(0)) Then
    argBase = arrayArg(0)
  Else
    argBase = 0
  End If
  If Len(arrayArg(1)) Then
    argLen = arrayArg(1)
  Else
    argLen = 0
  End If
  argType = arrayArg(2)
  If Len(arrayArg(3)) Then
    argDec = arrayArg(3)
  Else
    argDec = 0
  End If
  If Len(arrayDesc(0)) Then
    descBase = arrayDesc(0)
  Else
    descBase = 0
  End If
  If Len(arrayDesc(1)) Then
    descLen = arrayDesc(1)
  Else
    descLen = 0
  End If
  descType = arrayDesc(2)
  If Len(arrayDesc(3)) Then
    descDec = arrayDesc(3)
  Else
    descDec = 0
  End If
  If Ezt2cobol Then
    CurrentTag = "WORKING-STORAGE(i)"
    statement = ""
    eztf.fieldName = "ARG"
    eztf.fieldType = argType
    eztf.dataLen = argLen
    cblField = getCobolData(eztf)
    stline(0) = "   05 ARG     PIC " & CStr(cblField.dataType(0)) & "(" & CStr(cblField.dataLen) & ")."
    eztf.fieldName = "DESC"
    eztf.fieldType = descType
    eztf.dataLen = descLen
    cblField = getCobolData(eztf)
    stline(1) = "   05 DESC    PIC " & CStr(cblField.dataType(0)) & "(" & CStr(cblField.dataLen) & ")."
  
  
    If tableType = "INSTREAM" Then
      str3 = ""
      For i = 0 To UBound(arrayInstream) - 1
      str3 = str3 & Replace(Replace(stline(0), ".", ""), " ARG ", " FILLER") & " VALUE '" & Left(arrayInstream(i), argLen) & "'." & vbCrLf & _
                    Replace(Replace(stline(1), ".", ""), " DESC ", " FILLER ") & " VALUE '" & Trim(Mid(arrayInstream(i), InStr(arrayInstream(i), " ") + 1, descLen)) & "'." & vbCrLf
      Next
      statement = "01 " & fileName & "-INSTREAM." & vbCrLf & _
                  " 03 FILLER." & vbCrLf & _
                  str3 & vbCrLf & _
                  "01 " & fileName & "-TAB REDEFINES " & fileName & "-INSTREAM." & vbCrLf & _
                  " 03 " & fileName & "-TABLE OCCURS " & UBound(arrayInstream) & vbCrLf & _
                  "                           ASCENDING KEY ARG " & vbCrLf & _
                  "                           INDEXED BY " & fileName & "-I." & vbCrLf & _
                  stline(0) & vbCrLf & _
                  stline(1)
      tagWrite statement
    
    Else
      changeTag RTB, "<ASSIGN(i)>", "SELECT " & fileName & " ASSIGN TO S-" & fileName & vbCrLf & _
                                 "       ORGANIZATION IS SEQUENTIAL" & vbCrLf & _
                                 "       ACCESS MODE IS SEQUENTIAL ."
     
      changeTag RTB, "<FD(i)>", "FD  " & fileName & vbCrLf & _
                                "   LABEL RECORDS ARE STANDARD." & vbCrLf & _
                                  "   <FD-" & fileName & "(i)>"
                              
     
      changeTag RTB, "<CLOSE-FILES(i)>", "CLOSE " & fileName
      ReDim Preserve fileEZT(UBound(fileEZT) + 1)
      fileEZT(UBound(fileEZT)).fileName = fileName
      fileEZT(UBound(fileEZT)).openInput = False
    
      
      statement = "01 " & fileName & "-TAB." & vbCrLf & _
                  stline(0) & vbCrLf & _
                  stline(1)
                  
      changeTag RTB, "<FD-" & fileName & "(i)>", statement
      statement = ""
    End If
    changeTag RTB, "<FILE-STATUS(i)>", "01 " & fileName & "-STATUS" & Space(20 - Len(fileName & "-STATUS")) & " PIC 9(01) VALUE 0." & vbCrLf & _
                                   "  88 OK-" & fileName & Space(20 - Len("OK-" & fileName)) & " VALUE 0." & vbCrLf & _
                                   "  88 NF-" & fileName & Space(20 - Len("NF-" & fileName)) & " VALUE 1."
  Else
    Parser.PsConnection.Execute "INSERT INTO PSEZ_TABLE " & _
                                "(idOggetto,Name, Type, Len, Lenbl, EntryNumber, ARG_base, ARG_len, ARG_dec, ARG_type, DESC_base, DESC_len, DESC_dec, DESC_type) " & _
                                "VALUES " & _
                                "(" & GbIdOggetto & ",'" & fileName & "','" & tableType & "' , " & recl & ", " & blrecl & "," & entryNum & "," & argBase & "," & argLen & "," & argDec & ",'" & argType & "'," & descBase & "," & descLen & "," & descDec & ",'" & descType & "')"
  End If
  'Parser.PsConnection.Execute "INSERT INTO PsEZ_TABLE " &
   '                           "(idOggetto,fileName, filetype, disposition, blrecl, recl, additional) " & _
    '                          "VALUES " & _
     '"(" & GbIdOggetto & ",'" & fileName & "','TABLE', '" & disposition & "', " & blrecl & "," & recl & ",'" & additional & "')"
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getRECORD(statement As String)
  'MsgBox "getRECORD: " & statement
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getRETRIEVE(statement As String, linee As Long)
  Dim token As String, fileName As String, instruction As String, strINQ As String
  Dim segName As String
  Dim ioArea As String
  Dim strSSA As String
  Dim i As Integer
  Dim line As String
  Dim x As Boolean
  Dim rs As ADODB.Recordset
  Dim dbdName As String
  
  On Error GoTo errDLI
  
  If Not Ezt2cobol Then
    SwDli = True
    
    If InStr(1, statement, "WHILE") > 0 Then
      ''''dopo
      token = nexttoken(statement)

      fileName = token
      
      'con il nome del file ricava il nome del db
      strINQ = "Select * from PSEZ_FILE where IdOggetto = " & GbIdOggetto & " and fileName='" & fileName & "' and filetype = 'DLI'"
      Set rs = m_Fun.Open_Recordset(strINQ)
      If Not rs.EOF Then
        dbdName = rs!DBD
      Else
        dbdName = ""  ''gestire eccezione...quale?Che metto?
      End If
      rs.Close
      
      'nome segmento
      token = nexttoken(statement)
      Dim kkk As Integer
      kkk = InStr(1, statement, "(")
      If kkk > 0 Then
        statement = Mid(statement, kkk + 1, Len(statement))
      End If
      segName = nexttoken(statement)
      'dal nome del segmento ricava l'area
      'RECORD ....
      'MA visto che non so come andare a ricavare univocamente l'area
      'ma so che il nome dell'area � uguale al nome del segnebto prendo
      'direttamente il nome del segmento e lo passo al nome dell'area
      'sar� corretto??? mah....
      ioArea = segName
      
      'nel caso di retrieve l'istruzione � sempre 'GN'
      'sar� vero???
      instruction = "GN"

      statement = instruction & " " & fileName & " " & ioArea
      
      'SCRIVE LA PSCALL
      getCALL linee, "'RETRIEVE' " & statement
      
      checkPSB statement
    Else
      token = nexttoken(statement)
      
      If token = "CHKP" Or token = "XRST" Then
        'Format 2,3 (manuale)
        instruction = token
        '...
      Else
        'nome del file
        fileName = token
        
        'con il nome del file ricava il nome del db
        strINQ = "Select * from PSEZ_FILE where IdOggetto = " & GbIdOggetto & " and fileName='" & fileName & "' and filetype = 'DLI'"
        Set rs = m_Fun.Open_Recordset(strINQ)
        If Not rs.EOF Then
          dbdName = rs!DBD
        Else
          dbdName = ""  ''gestire eccezione...quale?Che metto?
        End If
        rs.Close
        
        'nome segmento
        token = nexttoken(statement)
        segName = nexttoken(statement)
        'dal nome del segmento ricava l'area
        'RECORD ....
        'MA visto che non so come andare a ricavare univocamente l'area
        'ma so che il nome dell'area � uguale al nome del segnebto prendo
        'direttamente il nome del segmento e lo passo al nome dell'area
        'sar� corretto??? mah....
        ioArea = segName
        
        'nel caso di retrieve l'istruzione � sempre 'GN'
        'sar� vero???
        instruction = "GN"

        statement = instruction & " " & fileName & " " & ioArea
        
        'SCRIVE LA PSCALL
        getCALL linee, "'RETRIEVE' " & statement
        
        checkPSB statement
      End If
    End If
  End If
  
  statement = ""
  Exit Sub
errDLI:
  'tmp
  MsgBox err.description
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''
' Delete tabelle allestite dal parser (I)
''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub resetRepository()
  On Error GoTo dbErr
  
  Parser.PsConnection.Execute "Delete * from PsEZ_File where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PSEZ_TABLE where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PSEZ_REPORT where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PSEZ_Sequence where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PSEZ_Control where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PSEZ_Title where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PSEZ_Heading where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PSEZ_Line where idOggetto =" & GbIdOggetto

  Exit Sub
dbErr:
  m_Fun.Show_MsgBoxError "RC0002", , "resetRepository", err.source, err.description, False
End Sub
Sub UpdateEZHeading(header As String, fieldName As String, count As Integer)
Dim rs As Recordset
  If Not Left(header, 1) = "'" Or Not Right(header, 1) = "'" Then
    header = "'" & header & "'"
  End If
  header = Replace(header, "'", "''")
  Set rs = m_Fun.Open_Recordset("select FieldName from PsEZ_Heading where IdOggetto = " & GbIdOggetto & " and FieldName ='" & fieldName & "'")
  If rs.EOF Then
    Parser.PsConnection.Execute "Insert into PsEZ_Heading (IdOggetto,FieldName,IdReport,Stringa,Ordinale)" _
                          & " values (" & GbIdOggetto & ",'" & fieldName & "',1,'" & header & "'," & count & ")"
  End If
End Sub



'''''''''''''''''''''''''''''''''''''''''''''''''
' Analisi DATI!!!
'''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub getDEFINE(statement As String, Optional token As String = "", Optional make As Boolean = False)
  Dim rs As Recordset
  Dim eztfield As eztData
  Dim cblField As cblData
  Dim fileName As String
  Dim pos As Integer
  Dim a As Integer
  Dim livello As Integer
  On Error GoTo catch
  
  'debug:
  Dim st As String
  st = token & " " & statement
  
  'FieldName:
  If Len(token) Then
    eztfield.fieldName = token
  Else
    eztfield.fieldName = nexttoken(statement)
  End If
  
  If eztfield.fieldName = CurrentFILE Then
    ReDim Preserve arrNameFieldLikeFile(UBound(arrNameFieldLikeFile) + 1)
    arrNameFieldLikeFile(UBound(arrNameFieldLikeFile)) = eztfield.fieldName
    m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", ###" & "Prefix B- for field " & eztfield.fieldName & " (field name equal file name)"
    eztfield.fieldName = "B-" & eztfield.fieldName
  End If
    
  'Definizione FILE?
  pos = InStr(eztfield.fieldName, ":")
  If pos Then
    eztfield.fieldName = Left(eztfield.fieldName, pos - 1)
    fileName = Mid(eztfield.fieldName, pos + 1)
    If listIndex > 1 Then
      getLivelliCobol
    End If
    listIndex = 1
    'MsgBox "tmp..."
    'controllare?!
  End If
    
  'Location:
  eztfield.location = nexttoken(statement)
  If IsNumeric(eztfield.location) Then
    If Len(CurrentFILE) Then
      eztfield.fieldPosition = 2
      eztfield.Offset = eztfield.location
      eztfield.location = ""
    Else
      eztfield.fieldPosition = 1
    End If
  ElseIf eztfield.location = "*" Then
    '...
    'AC
     If Len(CurrentFILE) Then
      eztfield.fieldPosition = 2
      eztfield.Offset = oldfieldpos '+ 1
      eztfield.location = ""
    Else
      eztfield.fieldPosition = 1
    End If
    'silvia
    'eztfield.fieldPosition = oldfieldpos + 1
  ElseIf eztfield.location = "W" Then
    'OK: working-spooled
    'CurrentTag = "WORKING-STORAGE"
    eztfield.fieldPosition = 1
    eztfield.Offset = 1
    CurrentFILE = ""  '!!!!
    If listIndex > 1 Then
      getLivelliCobol
    End If
    listIndex = 1
  ElseIf eztfield.location = "S" Then
    'OK: working-no spooled
    'CurrentTag = "WORKING-STORAGE"
    eztfield.fieldPosition = 1
    eztfield.Offset = 1
    CurrentFILE = ""  '!!!!
    If listIndex > 1 Then
      getLivelliCobol
    End If
    listIndex = 1
  Else
    eztfield.groupName = eztfield.location
    eztfield.location = ""
    eztfield.Offset = 0
    ' Mauro 09/07/2007 : Ricerco l'offset nel campo di location
    For a = 1 To UBound(dataList)
      If dataList(a).fieldName = eztfield.groupName Then
        eztfield.Offset = dataList(a).Offset
        Exit For
      End If
    Next a
    If eztfield.Offset > 0 Then
      'Offset:
      token = nexttoken(statement)
      If Left(token, 1) = "+" Then
        eztfield.Offset = eztfield.Offset + Mid(token, 2)
      Else
        'eztField.offset = 1
        'opzionale: ripristino statement per semplicit� codice...
        statement = token & " " & statement
      End If
    Else
      ' Questo campo non ha l'area di riferimento associata
      m_Fun.writeLog "Il campo " & eztfield.fieldName & " non ha l'area di riferimento associata!"
    End If
  End If
  
 If eztfield.Offset > 0 Then
    'Attributes:
    eztfield.bytes = nexttoken(statement)
    If IsNumeric(eztfield.bytes) Then
      eztfield.fieldType = nexttoken(statement)
      Select Case eztfield.fieldType
        Case "A", "K", "M"
          eztfield.dataLen = eztfield.bytes
          eztfield.Segno = ""
        Case "N"
          eztfield.dataLen = eztfield.bytes
          'T 15-04-2009
          eztfield.Segno = IIf(IsNumeric(statement), "S", "")
        Case "P"
          eztfield.dataLen = (eztfield.bytes * 2) - 1
          'T 15-04-2009
          eztfield.Segno = IIf(IsNumeric(statement), "S", "")
          'eztfield.Segno = "S"
        Case "U"
          eztfield.dataLen = (eztfield.bytes * 2) - 1
          eztfield.Segno = ""
        Case "B"
          Select Case eztfield.bytes
            Case 1
              eztfield.dataLen = 3
            Case 2
              'silvia
              ''eztfield.dataLen = 5
              eztfield.dataLen = 4
            Case 3 ' al momento gestisco come se fosse senza segno, il controllo lo faccio dopo
                   ' se c'� il decimale
              eztfield.dataLen = 8
            Case 4
              eztfield.dataLen = 10
          End Select
          'T 15-04-2009
          'eztfield.Segno = "N"
          eztfield.Segno = IIf(IsNumeric(statement), "S", "")
        Case Else
          eztfield.dataLen = eztfield.bytes
          eztfield.Segno = ""
      End Select
    Else
''      MsgBox "eztField.attributes???: " & eztField.attributes
    End If
 
    If Len(statement) Then
      token = nexttoken(statement)
      If IsNumeric(token) Then
        'Gestione Decimali
        Select Case eztfield.fieldType
          Case "P", "U", "N"
            eztfield.dataLen = eztfield.dataLen - token
            eztfield.dataDec = token
          Case "B"
            If eztfield.dataLen = 8 Then
              eztfield.dataLen = 7
            End If
        End Select
        'T 15-04-2009
        eztfield.Segno = "S"
        token = nexttoken(statement)
      End If
      Select Case token
        Case "VALUE"
          eztfield.fieldValue = nexttoken(statement)
          If Left(eztfield.fieldValue, 1) = "(" Then
             eztfield.fieldValue = Mid(eztfield.fieldValue, 2, Len(eztfield.fieldValue) - 2)
          End If
        Case "EVEN"
        Case "VARYING"
        Case "HEADING"
          statement = Replace(statement, "(", "")
          statement = Replace(statement, ")", "")
          StHEADING.Add statement, eztfield.fieldName
          ReDim Preserve arrHeadKey(UBound(arrHeadKey) + 1)
          arrHeadKey(UBound(arrHeadKey)) = eztfield.fieldName
          'inseriamo nella PsEZ_Heading
          UpdateEZHeading statement, eztfield.fieldName, StHEADING.count
        Case "INDEX"
          eztfield.Index = nexttoken(statement)
          ReDim Preserve arrColIndex(UBound(arrColIndex) + 1)
          arrColIndex(UBound(arrColIndex)) = eztfield.Index
          'ColIndex.Add eztfield.Index, eztfield.Index
        Case "MASK"
          'Mi leggo la display: al momento non me ne faccio niente, ma potrebbe poi servirmi
          If Ezt2cobol Then getMask eztfield, statement
          ''token = nextToken(statement)
        Case "OCCURS"
          token = nexttoken(statement)
          If Left(token, 1) = "(" Then
            token = Mid(token, 2, Len(token) - 2)
          End If
          eztfield.Occurs = token
          If Len(statement) Then
            token = nexttoken(statement)
            If token = "INDEX" Then
              eztfield.Index = nexttoken(statement)
              ReDim Preserve arrColIndex(UBound(arrColIndex) + 1)
              arrColIndex(UBound(arrColIndex)) = eztfield.Index
              'ColIndex.Add eztfield.Index, eztfield.Index
            End If
          End If
        Case "RESET"
        Case Else
          'MsgBox "statement: " & statement
          '''m_Fun.WriteLog "statement: " & statement
      End Select
    End If
    statement = ""
    
    'silvia 22-04-2008: salvo la posizione nel caso il campo dopo abbia la location = *
    'oldfieldpos = eztfield.fieldPosition + eztfield.dataLen
  
   'Mauro 09/07/2007 : Portato tutto fuori dalla if senn� salvava solo il livello 01
  ''  'CONVERSIONE:
  ''  If Ezt2cobol Then
  ''    'Conversione tipo dati:
  ''    dataList(listIndex) = eztField
  ''    listIndex = listIndex + 1
  ''    cblField = getCobolData(eztField)
  ''    'Scrittura:
  ''    writeCobolData cblField
  ''  Else
  ''    ' Mauro 21/06/2007 : Converte i campi da EZ* a CBL
  ''    cblField = getCobolData(eztField)
  ''  End If
  
    'CONVERSIONE:
    'Conversione tipo dati:
 
 
  oldfieldpos = eztfield.Offset + eztfield.bytes  '+ eztfield.dataLen
  statement = ""
  dataList(listIndex) = eztfield
  listIndex = listIndex + 1
  cblField = getCobolData(eztfield)
 End If
''  'CONVERSIONE:
''  If Ezt2cobol Then
''    'Conversione tipo dati:
''    dataList(listIndex) = eztfield
''    listIndex = listIndex + 1
''    cblField = getCobolData(eztfield)
''    'Scrittura:
''    writeCobolData cblField
''  End If
  Exit Sub
catch:
  m_Fun.Show_MsgBoxError "RC0002", , "getDEFINE", err.source, err.description, False
  Resume Next
End Sub

Private Sub getMask(eztfield As eztData, statement As String)
  Dim token As String, wfield As String
  
  statement = Replace(statement, "(", "")
  statement = Replace(statement, ")", "")
  If InStr(statement, "HEADING") Then
    StHEADING.Add Mid(statement, InStr(statement, "HEADING") + 8), eztfield.fieldName
    statement = Left(statement, InStr(statement, "HEADING") - 1)
  End If
  statement = Replace(statement, "'", "")
  If eztfield.fieldType <> "N" And eztfield.fieldType <> "P" Then
    token = nexttoken(statement)
  End If
  If Len(statement) Then
    statement = Replace(statement, ",", "#")
    statement = Replace(statement, ".", ",")
    statement = Replace(statement, "#", ".")
    If token <> "" Then
      ColMask.Add statement, token
    End If
    wfield = "01 " & eztfield.fieldName & "-Z" & Space(35 - Len(eztfield.fieldName) + 2) & "PIC " & statement & "."
  Else
    wfield = "01 " & eztfield.fieldName & "-Z" & Space(35 - Len(eztfield.fieldName) + 2) & "PIC " & ColMask.item(token) & "."
  End If

  tagWrite wfield, "WORKING-STORAGE(i)"
End Sub

Private Sub getLivelliCobol()
  Dim i As Long
  Dim indexOut As Long
  Dim x As Long
  Dim listOut() As eztData
  Dim Index As Long
  Dim sottoLiv As String
  
  On Error GoTo errorHandler
  
  dataList(1).byteBusy = 0
  ReDim listOut(1)
  
  'T 15-04-2009
  If Len(CurrentFILE) Then
    listOut(0) = dataList(1)
    listOut(0).fieldName = dataList(1).fieldName & "ORD"
    listOut(0).dataLen = 0
    listOut(0).livello = "01"
    dataList(1).livello = "03"
  Else
   dataList(1).livello = "01"
  End If
  
  listOut(1) = dataList(1)
  i = 2
  indexOut = 2
  For i = 2 To listIndex - 1
    If dataList(i - 1).Offset = dataList(i).Offset And _
       (CDbl(dataList(i - 1).bytes) > CDbl(dataList(i).bytes) Or _
       CDbl(dataList(i - 1).bytes) = 100000000) Then
      If dataList(i - 1).fieldType = "N" Or _
         dataList(i - 1).fieldType = "P" Or _
         dataList(i - 1).fieldType = "U" Or _
         dataList(i - 1).fieldType = "B" Then
        ' Se il campo di gruppo � un numerico, lo ridefinisco come "A"
        ReDim Preserve listOut(indexOut)
        listOut(indexOut) = dataList(i - 1)
        listOut(indexOut).fieldName = "FILLER"
        listOut(indexOut).fieldType = "A"
        listOut(indexOut).Redefines = dataList(i - 1).fieldName
        listOut(indexOut).livello = listOut(indexOut - 1).livello
        listOut(indexOut).isGroup = True
        indexOut = indexOut + 1
      End If
      ReDim Preserve listOut(indexOut)
      listOut(indexOut) = dataList(i)
      listOut(indexOut).livello = format(listOut(indexOut - 1).livello + 2, "00")
      listOut(indexOut - 1).byteBusy = listOut(indexOut - 1).byteBusy + (CInt(dataList(i).bytes) * IIf(dataList(i).Occurs > 0, dataList(i).Occurs, 1))
      listOut(indexOut - 1).isGroup = True
      indexOut = indexOut + 1
    Else
      x = indexOut - 1
      sottoLiv = format(CInt(listOut(x).livello) + 2, "00")
      Do Until x = 0
        If CInt(listOut(x).livello) < CInt(sottoLiv) Then
          sottoLiv = listOut(x).livello
          ' Controllo in quale gruppo deve andare il campo interessato
          If listOut(x).byteBusy < CDbl(listOut(x).bytes) And _
             CDbl(listOut(x).bytes) + listOut(x).Offset > dataList(i).Offset And _
             CDbl(listOut(x).bytes) >= listOut(x).byteBusy + (CDbl(dataList(i).bytes) * IIf(dataList(i).Occurs > 0, dataList(i).Occurs, 1)) And _
             CDbl(listOut(x).bytes) + listOut(x).Offset >= CDbl(dataList(i).bytes) + dataList(i).Offset And _
             listOut(x).Offset <= dataList(i).Offset Then
            If listOut(x).Offset + listOut(x).byteBusy > dataList(i).Offset Then
              ' Se torno al livello precedente e lascio dei byte vuoti, li riempio con questo FILLER
              If listOut(x).byteBusy > 0 And _
                 CDbl(listOut(x).bytes) > listOut(x).byteBusy And _
                 CDbl(listOut(x).bytes) < 100000000 Then
                ' Se � un file, � inutile ke ci metto un FILLER per chiudere il livello 01
                ReDim Preserve listOut(indexOut)
                listOut(indexOut).fieldName = "FILLER"
                listOut(indexOut).fieldType = "A"
                listOut(indexOut).Offset = listOut(x).Offset + listOut(x).byteBusy + 1
                listOut(indexOut).bytes = CDbl(listOut(x).bytes) - listOut(x).byteBusy
                listOut(indexOut).dataLen = CDbl(listOut(x).bytes) - listOut(x).byteBusy
                listOut(indexOut).livello = format(listOut(x).livello + 2, "00")
                listOut(x).byteBusy = listOut(x).byteBusy + CDbl(listOut(indexOut).bytes)
                indexOut = indexOut + 1
              End If
              ReDim Preserve listOut(indexOut)
              listOut(indexOut) = listOut(x)
              listOut(indexOut).fieldName = "FILLER"
              listOut(indexOut).fieldType = "A"
              listOut(indexOut).Redefines = IIf(Len(listOut(x).Redefines), listOut(x).Redefines, listOut(x).fieldName)
              listOut(indexOut).byteBusy = 0
              listOut(indexOut).isGroup = True
              'silvia
              listOut(indexOut).fieldValue = ""
              x = indexOut
              indexOut = indexOut + 1
            End If
            listOut(x).isGroup = True
            Exit Do
          End If
        End If

        ' Se torno al livello precedente e lascio dei byte vuoti, li riempio con questo FILLER
        If listOut(x).byteBusy > 0 And _
           CDbl(listOut(x).bytes) > listOut(x).byteBusy And _
           CDbl(listOut(x).bytes) < 100000000 Then
            ReDim Preserve listOut(indexOut)
          listOut(indexOut).fieldName = "FILLER"
          listOut(indexOut).fieldType = "A"
          listOut(indexOut).Offset = listOut(x).Offset + listOut(x).byteBusy
          listOut(indexOut).bytes = CDbl(listOut(x).bytes) - listOut(x).byteBusy
          listOut(indexOut).dataLen = CDbl(listOut(x).bytes) - listOut(x).byteBusy
          listOut(indexOut).livello = format(listOut(x).livello + 2, "00")
          listOut(indexOut).isGroup = False
          listOut(x).byteBusy = listOut(x).byteBusy + CDbl(listOut(indexOut).bytes)
          indexOut = indexOut + 1
        End If
        x = x - 1
      Loop

      ' Se l'indice torna a 0, significa che devo creare un nuovo livello 01
      If x = 0 Then
        ' Ridefinisco un nuovo livello 01 se i campi si sovrascrivono
        ReDim Preserve listOut(indexOut)
        listOut(indexOut) = dataList(1)
        listOut(indexOut).fieldName = "FILLER"
        listOut(indexOut).fieldType = "A"
        listOut(indexOut).Redefines = listOut(1).fieldName
        listOut(indexOut).livello = listOut(1).livello
        listOut(indexOut).isGroup = True
        x = indexOut
        indexOut = indexOut + 1
        ' Inserisco un FILLER se il nuovo campo non parte dal byte 1
        If dataList(i).Offset > 1 Then
          ReDim Preserve listOut(indexOut)
          listOut(indexOut).fieldName = "FILLER"
          listOut(indexOut).Offset = 1
          listOut(indexOut).bytes = dataList(i).Offset - 1
          listOut(indexOut).dataLen = dataList(i).Offset - 1
          listOut(indexOut).livello = "03"
          listOut(indexOut).isGroup = False
          listOut(x).byteBusy = listOut(x).byteBusy + CDbl(listOut(indexOut).bytes)
          indexOut = indexOut + 1
        End If
      Else
        If listOut(x).Offset + listOut(x).byteBusy < dataList(i).Offset Then
          ' Inserisco un FILLER per riempire dei byte lasciati vuoti
          ReDim Preserve listOut(indexOut)
          listOut(indexOut).fieldName = "FILLER"
          listOut(indexOut).Offset = listOut(x).byteBusy + listOut(x).Offset
          listOut(indexOut).bytes = dataList(i).Offset - (listOut(x).byteBusy + listOut(x).Offset)
          listOut(indexOut).dataLen = dataList(i).Offset - (listOut(x).byteBusy + listOut(x).Offset)
          listOut(indexOut).livello = format(listOut(x).livello + 2, "00")
          listOut(indexOut).isGroup = False
          listOut(x).byteBusy = listOut(x).byteBusy + CDbl(listOut(indexOut).bytes)
          indexOut = indexOut + 1
        End If
      End If
      ReDim Preserve listOut(indexOut)
      listOut(indexOut) = dataList(i)
      listOut(indexOut).livello = format(listOut(x).livello + 2, "00")
      listOut(x).byteBusy = listOut(x).byteBusy + (CInt(dataList(i).bytes) * IIf(dataList(i).Occurs > 0, dataList(i).Occurs, 1))
      indexOut = indexOut + 1
    End If
  Next i
  
  ' Bytes = 100000000 --> La struttura � un file
  If CDbl(listOut(1).bytes) = 100000000 Then
    listOut(1).bytes = 0
    For i = 2 To listIndex - 1
      If listOut(i).livello = "03" And listOut(i).Redefines = "" Then
        listOut(1).bytes = CDbl(listOut(1).bytes) + CDbl(listOut(i).bytes)
      End If
    Next i
    If CDbl(listOut(1).bytes) > 0 Then
      If CDbl(listOut(1).bytes) <> CDbl(listOut(1).dataLen) Then
        ' La lunghezza dichiarata del file non corrisponde con i campi effettivi
        m_Fun.writeLog "LenStruct COBOL:" & listOut(1).bytes & "-LenStruct EZT:" & listOut(1).dataLen
      End If
      For i = 2 To listIndex - 1
        If listOut(i).livello = "01" Then
          listOut(i).bytes = listOut(1).bytes
        End If
      Next i
    Else
      If CDbl(listOut(0).bytes) = 0 Then
        ReDim listOut(1)
        indexOut = 1
      End If
    End If
  End If
  ' Ciclo per controllare eventuali byte rimasti vuoti in fondo alla struttura
  i = indexOut - 1
  Index = indexOut - 1
  Do Until i = 0
    If CInt(listOut(i).livello) < CInt(listOut(Index).livello) Then
      If CDbl(IIf(listOut(i).bytes = 0 Or listOut(i).bytes = 100000000, 0, listOut(i).bytes)) > listOut(i).byteBusy Then
        ReDim Preserve listOut(indexOut)
        listOut(indexOut).fieldName = "FILLER"
        listOut(indexOut).fieldType = "A"
        listOut(indexOut).Offset = listOut(i).byteBusy + 1
        
        listOut(indexOut).bytes = CDbl(listOut(i).bytes) - listOut(i).byteBusy
        listOut(indexOut).dataLen = CDbl(listOut(i).bytes) - listOut(i).byteBusy
        listOut(indexOut).livello = format(listOut(i).livello + 2, "00")
        listOut(indexOut).isGroup = False
        listOut(i).byteBusy = listOut(i).byteBusy + CDbl(listOut(indexOut).bytes)
        indexOut = indexOut + 1
      End If
      Index = Index - 1
      i = Index
    End If
    i = i - 1
  Loop
  
  writeDataCmp listOut, indexOut
  Exit Sub
errorHandler:
  MsgBox "IdOggetto: " & GbIdOggetto & " - " & err.Number & " " & err.description
End Sub

Private Sub writeDataCmp(listCmp() As eztData, Index As Long)
  Dim i As Long
  Dim rsArea As Recordset
  Dim rsCmp As Recordset
  Dim cblField As cblData

  'T 15-04-2009
  If Ezt2cobol Then
    If Len(CurrentFILE) <> 0 Then
      cblField = getCobolData(listCmp(0))
      changeTag RTB, "<FD-" & CurrentFILE & "(i)>", "DATA RECORD IS " & cblField.Name & "." & vbCrLf
      writeFD cblField
      If CInt(listCmp(i).livello) = "01" Then
        idArea = idArea + 1
        lOrdinale = 0
        Set rsArea = m_Fun.Open_Recordset("select * from PsData_Area where idoggetto = " & GbIdOggetto)
        rsArea.AddNew
       
        rsArea!idOggetto = GbIdOggetto
        rsArea!idArea = idArea
        rsArea!nome = listCmp(i + 1).fieldName
        rsArea!livello = listCmp(i).livello
      
        rsArea.Update
        rsArea.Close
      End If
    End If
  End If
  For i = 1 To Index - 1
    ' Se � un livelo 01, scrivo il record nella PsData_Area
    If CInt(listCmp(i).livello) = "01" Then
      idArea = idArea + 1
      lOrdinale = 0
      Set rsArea = m_Fun.Open_Recordset("select * from PsData_Area where idoggetto = " & GbIdOggetto)
      rsArea.AddNew
     
      rsArea!idOggetto = GbIdOggetto
      rsArea!idArea = idArea
      rsArea!nome = listCmp(i).fieldName
      rsArea!livello = listCmp(i).livello
    
      rsArea.Update
      rsArea.Close
    End If
    ' Scrivo sempre il record nella PsData_Cmp
    Set rsCmp = m_Fun.Open_Recordset("select * from PsData_Cmp where idoggetto = " & GbIdOggetto)
    cblField = getCobolData(listCmp(i))
    
    If Ezt2cobol Then
      If Len(CurrentFILE) = 0 Then
        'Scrittura:
        writeCobolData cblField
      Else
        writeFD cblField
        'SILVIA 20-02-2009: aggiunta 4 byte conteneNti lunghezza record
        If CInt(listCmp(i).livello) = "01" Or listCmp(i).attributes = "V" Then
          cblField.Name = listCmp(i).fieldName & "ORD-LENGTH"
          cblField.level = "03"
          cblField.dataLen = 9
          cblField.bytes = 4
          cblField.dataType(0) = 9
          cblField.dataType(1) = "COMP"
          cblField.isGroup = False
          writeFD cblField
        End If
      End If
    End If
    
    rsCmp.AddNew
    
    rsCmp!idOggetto = GbIdOggetto
    rsCmp!idArea = idArea
    rsCmp!nome = cblField.Name
    lOrdinale = lOrdinale + 1
    rsCmp!Ordinale = lOrdinale
    rsCmp!codice = format(GbIdOggetto, "00000000") & "/" & format(idArea, "0000") & "/" & format(lOrdinale, "0000")
    rsCmp!livello = cblField.level
    rsCmp!Tipo = IIf(cblField.isGroup, "A", cblField.dataType(0))
    rsCmp!Posizione = listCmp(i).Offset
    If IsNumeric(cblField.bytes) Then
      rsCmp!byte = cblField.bytes
    End If
    rsCmp!Occurs = listCmp(i).Occurs
    If IsNumeric(cblField.dataLen) Then
      rsCmp!Lunghezza = cblField.dataLen  '0 !!!!!!!!!!!!!!!
    End If
    If IsNumeric(cblField.dataDec) Then
      rsCmp!Decimali = cblField.dataDec  '0 !!!!!!!!!!!!!!!
    End If
    rsCmp!Segno = cblField.Segno
    rsCmp!Usage = Switch(cblField.dataType(1) = "COMP", "BNR", cblField.dataType(1) = "COMP-3", "PCK", True, "ZND")
    rsCmp!Valore = listCmp(i).fieldValue
    rsCmp!nomeRed = listCmp(i).Redefines
    
    rsCmp.Update
    rsCmp.Close
  Next i
End Sub
''''''''''''''''''''''''''''''''''''''''''
' GESTIONE PSB/DBD:
' - N PSB, PCB, M segmenti
' Copia di quello di IMS!!!! provarlo qui e metterlo in linea!!
' Pu� arrivare TabIstr.DBD gi� valorizzato!!!!
' => INIZIALIZZARLO IN TUTTE LE CHIAMATE del CBL!!!!
''''''''''''''''''''''''''''''''''''''''''
Private Sub getInstructionPsbDbd(pcbNumber As Integer)
  Dim rs As Recordset, tb As Recordset, rsdbd As Recordset
  Dim segmentsQuery As String, psbString As String, dbdWhereCond As String
  Dim i As Integer
  
  If Not IsDC Then
    ''''''''''''''''''''''''''''
    ' Lista DBD (filtro PCB)
    ''''''''''''''''''''''''''''
    For i = 0 To UBound(psbPgm)
      psbString = psbString & psbPgm(i) & ","
    Next
    psbString = Left(psbString, Len(psbString) - 1) 'ultima virgola superflua
    '''''''''''''''''''''
    ' Differenza:
    '''''''''''''''''''''
    If Len(TabIstr.DBD) Then
      Set rs = m_Fun.Open_Recordset("select distinct idOggetto as IdDbd from bs_oggetti WHERE nome='" & TabIstr.DBD & "' AND tipo='DBD'")
    Else
      'Old:
      Set rs = m_Fun.Open_Recordset("select distinct b.idOggetto as IdDbd from PsDLI_Psb as a,bs_oggetti as b where a.idOggetto IN (" & psbString & ") and NumPcb = " & pcbNumber & " and typePcb='DB' and a.dbdName=b.nome and b.tipo='DBD'")
    End If
    If rs.RecordCount Then
      dbdWhereCond = "("
      ReDim wIdDBD(rs.RecordCount - 1)
      For i = 0 To UBound(wIdDBD)
        wIdDBD(i) = rs!idDBD
        dbdWhereCond = dbdWhereCond & "idOggetto = " & rs!idDBD
        If i = UBound(wIdDBD) Then  'ultimo giro
          dbdWhereCond = dbdWhereCond & ")"
        Else
          dbdWhereCond = dbdWhereCond & " OR "
        End If
        rs.MoveNext
      Next
    Else
      'NESSUN DBD! (a meno di GSAM...)
      ReDim wIdDBD(0)
    End If
    
    If UBound(wIdDBD) Then  ' N DBD!
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' FILTRO SEGMENTI
      ' (VERIFICARE CHE NOME SEGMENTO NON SIA VARIABILE!!!)...
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'Preparazione query differenziata:
      If UBound(TabIstr.BlkIstr) Then
        If UBound(TabIstr.BlkIstr) = 1 And Len(TabIstr.BlkIstr(1).Segment) And Len(dbdWhereCond) = 0 Then
          segmentsQuery = "select IdOggetto from PsDli_Segmenti where nome = '" & Replace(TabIstr.BlkIstr(1).Segment, "'", "") & "'"
          'Se non ho PSB o PCB non filtro i DBD: su tutti!
        Else
         'filtro su tutti i nomi di segmento dell'istruzione...
         'es:
         'select idOggetto from
         '  (SELECT IdOggetto, Count(*) AS occurs
         '  From psDLI_Segmenti
         '  WHERE (Nome='XXX' Or Nome='YYY')
         '  GROUP BY IdOggetto)
         'Where Occurs = 2
         segmentsQuery = "select IdOggetto from " & _
                           "(select idOggetto,count(*) as occurs from psDLI_Segmenti " & _
                           "where ("
          For i = 1 To UBound(TabIstr.BlkIstr)
            segmentsQuery = segmentsQuery & "nome = '" & Replace(TabIstr.BlkIstr(i).Segment, "'", "") & "'"
            If i = UBound(TabIstr.BlkIstr) Then
               segmentsQuery = segmentsQuery & ")"
            Else
               segmentsQuery = segmentsQuery & " OR "
            End If
          Next i
          segmentsQuery = segmentsQuery & _
                            " group by idOggetto)" & _
                            " where occurs =" & UBound(TabIstr.BlkIstr)
          
          segmentsQuery = segmentsQuery & " AND " & dbdWhereCond
        End If
      Else
        'NO FILTRO SUI SEGMENTI:
        segmentsQuery = "select idOggetto from PsDLI_Psb where idOggetto IN (" & psbString & ") and NumPcb = " & pcbNumber
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' DBD:
      ' - Set wIdDBD()
      ' - Insert in PsDli_IstrDBD
      ' Ho la query finale, differenziata a seconda dei casi:
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Set tb = m_Fun.Open_Recordset(segmentsQuery)
      If tb.RecordCount Then
        ReDim wIdDBD(tb.RecordCount - 1)
        For i = 0 To tb.RecordCount - 1
          wIdDBD(i) = tb!idOggetto
          tb.MoveNext
        Next i
      ElseIf Len(dbdWhereCond) Then
        'tolgo il filtro sui PSB!!!!!!!! (se ne ho di variabili mi frega...)
        'integrare meglio sopra...
        segmentsQuery = Left(segmentsQuery, InStr(segmentsQuery, " AND "))
        Set tb = m_Fun.Open_Recordset(segmentsQuery)
        If tb.RecordCount Then
          ReDim wIdDBD(tb.RecordCount - 1)
          For i = 0 To tb.RecordCount - 1
            wIdDBD(i) = tb!idOggetto
            tb.MoveNext
          Next i
        End If
      End If
      tb.Close
    End If
    
'stefano: ancora, ma quante getinstructionpsbdbd ci sono? 3?????
    If UBound(wIdDBD) = 0 And UBound(TabIstr.BlkIstr) > 0 Then
       Dim flgDBDOk As Boolean
       i = 0
       If wIdDBD(0) = 0 Then
         Set tb = m_Fun.Open_Recordset("select distinct a.idOggetto, idsegmento from PsDLI_segmenti as a, Bs_Oggetti as b where a.nome = '" & TabIstr.BlkIstr(1).Segment & "' and a.idoggetto = b.idoggetto and b.tipo_dbd <> 'IND'")
         While Not tb.EOF
           flgDBDOk = True
           If UBound(TabIstr.BlkIstr(1).KeyDef) > 0 Then
             'stefano: ancora modifiche del cavolo: ma perch� la keydef viene definita anche sulle squalificate?
             If TabIstr.BlkIstr(1).KeyDef(1).Key <> "" Then
             Dim tb1, tb2 As Recordset
             Set tb2 = m_Fun.Open_Recordset("select * from PsDLI_field where nome = '" & TabIstr.BlkIstr(1).KeyDef(1).Key & "' and idsegmento = " & tb!idSegmento)
             If tb2.EOF Then
                Set tb2 = m_Fun.Open_Recordset("select * from PsDLI_xdfield where nome = '" & TabIstr.BlkIstr(1).KeyDef(1).Key & "' and idsegmento = " & tb!idSegmento)
             End If
             If tb2.EOF Then
                flgDBDOk = False
             End If
             tb2.Close
             End If
           End If
           If UBound(TabIstr.BlkIstr) > 1 And flgDBDOk Then
              Set tb1 = m_Fun.Open_Recordset("select distinct a.idOggetto, idsegmento from PsDLI_segmenti as a, Bs_Oggetti as b where a.nome = '" & TabIstr.BlkIstr(2).Segment & "' and a.idoggetto = b.idoggetto and b.tipo_dbd <> 'IND'")
              If Not tb1.EOF Then
                 flgDBDOk = True
              Else
                 flgDBDOk = False
              End If
              tb1.Close
           End If
           If flgDBDOk Then
             ReDim Preserve wIdDBD(i)
             wIdDBD(i) = tb!idOggetto
             i = i + 1
           End If
           tb.MoveNext
         Wend
         tb.Close
       End If
    End If
    
    Dim wPsb As ADODB.Recordset
    'Ho trovato almeno un DBD?
    If wIdDBD(0) Then
      'ANCHE SE NE HO N, PRENDO IL PRIMO... POI SEGNALO
      'TMP: trovare psbInstr (filtrato con i DBD OK) e usare quello...
      
      TabIstr.PsbId = psbPgm(0)
      TabIstr.psbName = m_Fun.Restituisci_NomeOgg_Da_IdOggetto(psbPgm(0))
      Set rs = m_Fun.Open_Recordset("select Nome,Tipo_DBD from Bs_oggetti where IdOggetto = " & wIdDBD(0))
      TabIstr.DBD_TYPE = getDBDType(rs!nome, rs)  'rs opzionale...
      TabIstr.DBD = rs!nome
      rs.Close
      ' serve ?
'      If UBound(wIdDBD) Then
'        AggiungiSegnalazione " ", "MANYRELPSB", " "
'        'ATTENZIONE: l'istruzione pero' e' valida?!
'      End If
      'Inserimento in Tabella PsDli_IstrDBD
      'SQ CPY-ISTR
      Set rsdbd = m_Fun.Open_Recordset("select * from PsDli_IstrDBD")
      For i = 0 To UBound(wIdDBD)
         rsdbd.AddNew
         rsdbd!idpgm = GbIdOggetto
         rsdbd!idOggetto = IdOggettoRel
         rsdbd!Riga = GbOldNumRec
         rsdbd!idDBD = wIdDBD(i)
         rsdbd.Update
      Next i
      rsdbd.Close
      IsValid = True
    Else
      ''''''''''''''''''''''''''''
      ' SQ 10-04-06
      ' GESTIONE GSAM
      ''''''''''''''''''''''''''''
      Set rs = m_Fun.Open_Recordset("select b.idOggetto as IdDbd,A.dbdName as dbdName from PsDLI_Psb as a,bs_oggetti as b where a.idOggetto IN (" & psbString & ") and NumPcb = " & pcbNumber & " and typePcb='GSAM' and a.dbdName=b.nome and b.tipo='DBD'")
      If rs.RecordCount Then
        TabIstr.DBD = rs!dbdName
        TabIstr.DBD_TYPE = DBD_GSAM
        ReDim wIdDBD(0)
        wIdDBD(0) = rs!idDBD
        
        'SQ CPY-ISTR
        'Inserimento in Tabella PsDli_IstrDBD
        Set rsdbd = m_Fun.Open_Recordset("select * from PsDli_IstrDBD")
        rsdbd.AddNew
        rsdbd!idpgm = GbIdOggetto
        rsdbd!idOggetto = IdOggettoRel
        rsdbd!Riga = GbOldNumRec
        rsdbd!idDBD = wIdDBD(0)
        rsdbd.Update
        rsdbd.Close
      Else
        IsValid = False
        'SQ - 23-04-06
        Set rs = m_Fun.Open_Recordset("select DbdName from PsDLI_Psb where idOggetto IN (" & psbString & ") and NumPcb = " & pcbNumber & " and typePcb='DB'")
        If rs.RecordCount Then
          TabIstr.DBD = rs!dbdName
        Else
          TabIstr.DBD = ""
        End If
        'SQ 28-04-06
        TabIstr.DBD_TYPE = DBD_UNKNOWN_TYPE
        rs.Close
        '''AggiungiSegnalazione TabIstr.DBD, "NODBD", TabIstr.DBD
      End If
    End If
  Else
    '''''''''''''''''''''''''
    ' IMS-DC:
    ' - PSB: quelli del PGM
    ' - NO DBD/SEGM
    '''''''''''''''''''''''''
    ReDim wIdDBD(0)
    ' Ma serve ?
'    If UBound(psbPgm) Then
'      AggiungiSegnalazione " ", "MANYRELPSB", " "
'      'ATTENZIONE: l'istruzione pero' e' valida?!
'    End If
  End If
  
  '''''''''''''''''''''''''''''''''''''''''''''''
  'Inserimento in Tabella
  '''''''''''''''''''''''''''''''''''''''''''''''
  If SwPsbAssociato Then
    If (UBound(psbPgm) > UBound(wIdDBD)) And wIdDBD(0) Then
      'Filtro la psbPgm con i DBD rimasti...
      For i = 0 To UBound(psbPgm)
      'stefano: da rivedere
       If Len(dbdWhereCond) Then
        Set tb = m_Fun.Open_Recordset("select psb.idOggetto from PsDli_Psb as psb,bs_oggetti as dbd where psb.IdOggetto=" & psbPgm(i) & " AND psb.numPcb=" & pcbNumber & " AND (psb.dbdName=dbd.nome and dbd.tipo='DBD') and " & Replace(dbdWhereCond, "idOggetto", "dbd.idOggetto"))
        If tb.RecordCount = 0 Then
          'da scartare!
          psbPgm(i) = 0
        End If
        tb.Close
       Else
        m_Fun.writeLog "SP-dbbwherecond vuoto " & GbIdOggetto & "-" & GbOldNumRec
       End If
      Next
    End If
    
    For i = 0 To UBound(psbPgm)
      If psbPgm(i) Then
        Set tb = m_Fun.Open_Recordset("select * from PsDli_IstrPSB where Idpgm=" & GbIdOggetto & " and Idoggetto=" & IdOggettoRel & " AND Riga=" & GbOldNumRec & " AND IdPSB=" & psbPgm(i))
        'lo faccio da dentro per evitare chiavi duplicate:
        If tb.RecordCount = 0 Then
          tb.AddNew
          tb!idpgm = GbIdOggetto
          tb!idOggetto = IdOggettoRel
          tb!Riga = GbOldNumRec
          tb!IdPsb = psbPgm(i)
          tb.Update
        End If
        tb.Close
      End If
    Next
  End If
End Sub

'Se ho un ". " NON LITERAL, tengo solo la prima parte;
'In teoria dovrei tenere anche il resto!!!
' Mauro 20/07/2007 : Adesso il resto(tail) lo butta dentro alreadyReadLine
Private Sub splitStatement(statement As String, Optional alreadyReadLine As String)
  Dim i As Integer, level As Integer
  Dim currentChar As String
  Dim tail As String
  
  For i = 1 To Len(statement)
    currentChar = Mid(statement, i, 1)
    Select Case currentChar
      Case "."
        'Considero solo ". "
        If Mid(statement, i + 1, 1) = " " Then
          If level = 0 Then
            Exit For
          Else
            'LITERAL! prosegue l'analisi...
          End If
        End If
      Case "'"
        'Non considero i "''": sono literal!
        If Mid(statement, i + 1, 1) <> "'" Then
          If level = 0 Then
            level = level + 1
          Else
            level = level - 1
          End If
        Else
        'stefano: prova
          level = 0
        End If
      Case Else
        'avanza pure
    End Select
  Next
  
  'Controllo "bilanciamento"
  If level Then
    
    m_Fun.writeLog "tmp: not balanced statement! #" & statement
  End If
  
  'Statement successivi:
  tail = Trim(Mid(statement, i + 1))
  
  'Statement Corrente
  ' Mauro 18-07-2007 : Se prende fino a i, si tira dentro anke il punto
  'statement = Left(statement, i)
  statement = Left(statement, i - 1)
  
  If Len(tail) Then
    'TMP:
    If Right(tail, 1) = "." Then tail = Left(tail, Len(tail) - 1)
    If Left(tail, 1) <> "*" Then
      'tmp:
      If Trim(tail) = "PROC" Then
        'SQ 22-11-07 si mangiava il punto... poi sbagliata
        statement = Trim(statement) & ". " & "PROC"
      Else
        alreadyReadLine = tail
        'Trattare! altro statement!!
        'MsgBox "tmp: multi-statement! #" & statement
        m_Fun.writeLog "tmp: multi-statement! #" & statement
      End If
    End If
  End If
End Sub
Function getWhileBody(stat As String) As String
'dovremmo gestire il corpo del while
getWhileBody = stat
If Left(stat, 3) = "EOF" Then
  getWhileBody = Replace(stat, "EOF ", "EOF-")
ElseIf InStr(stat, " EOF ") > 0 Then
  getWhileBody = Replace(stat, " EOF ", " EOF-")
End If
End Function

''''''''''''''''''''''''''''''''''''''''''''''
' Solo per conversione:
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parseStatement(fdOUT As Long, statement As String, token As String)
  Dim originalStatement As String
  Dim Tag As String
  
  originalStatement = statement
  
  Select Case token
    Case "DO"
      token = nexttoken(statement)
      If token = "WHILE" Then
        statement = Replace(statement, " EQ ", " EQUAL ")
        statement = Replace(statement, " NE ", " NOT EQUAL ")
        statement = Replace(statement, " NQ ", " NOT EQUAL ")
        statement = Replace(statement, " LT ", " < ")
        statement = Replace(statement, " GT ", " > ")
        statement = Replace(statement, " GE ", " >= ")
        statement = Replace(statement, " GQ ", " >= ")
        statement = Replace(statement, " LE ", " <= ")
        statement = Replace(statement, " LQ ", " <= ")
        statement = indent & "PERFORM UNTIL NOT ( " & getWhileBody(statement) & " )"
        statement = Replace(statement, " AND ", vbCrLf & indent & " AND ")
        statement = Replace(statement, " OR ", vbCrLf & indent & " OR ")
        indentlevel = indentlevel + 1
      Else
        m_Fun.writeLog "TMP: DO?! " & token & "-" & statement
      End If
    Case "END-DO"
      indentlevel = indentlevel - 1
      statement = indent & "END-PERFORM"
    Case "GOTO"
      If statement = "JOB" Then
          statement = indent & "GO TO JOB" & format(CurrentJOB, "00") & IIf(isSection, "-ACTIVITY", "-ACTIVITY-EX")
      Else
         statement = indent & "GO TO " & statement
      End If
    Case "GO"
      'SILVIA 27-01-2009
      '''statement = indent & "GO TO " & statement
      token = nexttoken(statement)
      If statement = "JOB" Then
          statement = indent & "GO TO JOB" & format(CurrentJOB, "00") & IIf(isSection, "-ACTIVITY", "-ACTIVITY-EX")
      Else
         statement = indent & "GO TO " & statement
      End If
    Case "IDMS"
      'TMP:
      TmpIDMS = TmpIDMS + 1
'      statement = "* I-4 BEGIN CMD" & format(TmpIDMS, "00000") & vbCrLf & _
'                  "* IDMS " & originalStatement & vbCrLf & _
'                  "* I-4 END CMD" & format(TmpIDMS, "00000")
      statement = "*IDMS#" & originalStatement
    Case "IF"
      If Left(statement, 4) = "EOF " Then
        token = nextToken_newnew(statement)
        originalStatement = "EOF-" & statement
        statement = indent & "IF " & parseIF(originalStatement)
        'getEOF token, statement
      ElseIf Left(statement, 4) = "NOT " Then
        token = nextToken_newnew(statement)
        If Left(statement, 4) = "EOF " Then
          token = nextToken_newnew(statement)
          originalStatement = "EOF-" & statement
          statement = indent & "IF NOT " & parseIF(originalStatement)
        Else
          statement = "NOT " & statement
        End If
        'getEOF token, statement
      ElseIf UBound(fileName) > 1 Then
        If Trim(statement) = "MATCHED" Then
          statement = indent & "IF W-KEY-" & fileName(1) & " = W-KEY-" & fileName(2)
        ElseIf Trim(statement) = fileName(1) Then
          statement = indent & "IF W-KEY-" & fileName(1) & " > W-KEY-" & fileName(2)
        ElseIf Trim(statement) = fileName(2) Then
          statement = indent & "IF W-KEY-" & fileName(2) & " > W-KEY-" & fileName(1)
        Else
          statement = indent & "IF " & parseIF(originalStatement)
        End If
      Else
        statement = indent & "IF " & parseIF(originalStatement)
      End If
      indentlevel = indentlevel + 1
    Case "MOVE"
      'Gestione FILL!
      token = nexttoken(statement)
      If InStr(statement, " FILL ") Then
        statement = indent & "MOVE ALL " & Left(originalStatement, InStr(originalStatement, " FILL ") - 1)
      Else
        statement = indent & "MOVE " & originalStatement
      End If
      
    Case "PARM"
      'Butto!?
      Exit Sub
    Case "COPY"
      'getCOPY line 'COPY
      'MsgBox token & "-" & line
    Case "DISPLAY"
      'Gestire i "FILL"
      token = nexttoken(statement)
      If token = "SKIP" Then
         token = nexttoken(statement)
         statement = indent & "DISPLAY " & statement & " AT LINE " & token
      ElseIf token = "NEWPAGE" Then
         tagWrite "*" & indent & "DISPALY NEWPAGE" & vbCrLf, Tag
         statement = indent & "DISPLAY '---'" & vbCrLf & indent & "DISPLAY '---'" & vbCrLf _
                     & indent & "DISPLAY " & statement
      Else
         If InStr(statement, " FILL ") Then
            statement = indent & "DISPLAY ALL " & Left(originalStatement, InStr(originalStatement, " FILL ") - 1)
         Else
            statement = indent & "DISPLAY " & " " & originalStatement
         End If
      End If
      If Len(statement) > 60 Then
        statement = Replace(statement, "DISPLAY", "DISPLAY" & vbCrLf & indent)
        Dim s() As String, extraline As String
        s = Split(statement, vbCrLf)
        If UBound(s) = 1 Then
          If Len(s(1)) > 65 Then
            If Right(Trim(s(1)), 1) = "'" Then
              extraline = Right(s(1), Len(s(1)) - 65)
              s(1) = Left(s(1), 65)
              ReDim Preserve s(2)
              s(2) = Space(6) & "-'" & extraline
            Else
              ReDim Preserve s(2)
              s(2) = "    " & Trim(Right(s(1), Len(s(1)) - InStrRev(s(1), "'")))
              s(1) = Left(s(1), InStrRev(s(1), "'"))
            End If
             statement = s(0) & vbCrLf & s(1) & vbCrLf & s(2)
          End If
        End If
        
      End If
    Case "GET"
      token = nexttoken(statement)
      statement = indent & "READ " & token
      'SILVIA 27-01-2009
      Dim a As Long
      For a = 1 To UBound(fileEZT)
        If fileEZT(a).fileName = token Then
          fileEZT(a).openInput = True
          Exit For
        End If
      Next a
    Case "PUT"
      token = nexttoken(statement)
      statement = indent & "WRITE " & token & "-RECORD " & statement
      statement = IIf(InStr(statement, " FROM "), statement & "-RECORD", statement)
    Case "POINT", "READ", "WRITE"
      statement = "#" & token & Space(6 - (Len(token) + 1)) & "*" & "STATEMENT: " & statement
    Case "ELSE"
      indentlevel = indentlevel - 1
      'uguale?!
      statement = indent & token & " " & statement
      indentlevel = indentlevel + 1
    Case "END-IF", "END-IF."
      indentlevel = indentlevel - 1
      'uguale?!
      statement = indent & token & " " & statement
    Case "END-PROC", "END-PROC."
      statement = IIf(isSection = False, indent & "." & vbCrLf & CurrentPROC & "-EX." & vbCrLf & _
                  indent & "    EXIT." & vbCrLf, ".")
    Case "IDD"
       '?!
       Exit Sub
    Case "STOP"
      'STOP:
      'Terminazione JOB corrente: NON ESECUZIONE! (ES. INPUT=NULL)
      token = nexttoken(statement)
      If token = "EXECUTE" Then
        'SILVIA 27-01-2009
        If BolRetCode Then
          statement = indent & "GOBACK"
        Else
        'SILVIA 19-02-2009
        'statement = indent & "GO TO STOP-JOB" & format(CurrentJOB, "00")
          'statement = indent & "GO TO JOB" & format(CurrentJOB, "00") & IIf(isSection, "-ACTIVITY", "-ACTIVITY-EX")
          'AC
          statement = indent & "GO TO JOB" & format(CurrentJOB, "00") & IIf(isSection, "", "-EX")
        End If
      Else
        'SILVIA 19-02-2009
        'statement = indent & "GO TO STOP-JOB" & format(CurrentJOB, "00")
         'statement = indent & "GO TO JOB" & format(CurrentJOB, "00") & IIf(isSection, "-ACTIVITY", "-ACTIVITY-EX")
         'AC
          statement = indent & "GO TO JOB" & format(CurrentJOB, "00") & IIf(isSection, "", "-EX")
      End If
    Case "IDMS"
    Case "PERFORM"
      statement = indent & token & " " & statement & IIf(isSection = False, " THRU " & statement & "-EX", "")
    ' Mauro 21/11/2007 : Temporaneo!
    Case "DLI"
      Tag = "WORKING-STORAGE(i)"
      statement = "*DLI#" & originalStatement
      Case ""
        'nop
    Case Else
      If Right(token, 1) = "." Then
        If Len(statement) Then
          'PROC
          CurrentPROC = Replace(token, ".", "")
          token = nexttoken(statement)
          If token = "PROC" Then
            statement = ""
            If openJOB Then
              'Chiusura JOB?
              'AC tolto -ACTIVITY
              statement = indent & "." & vbCrLf & IIf(isSection = False, "JOB" & format(CurrentJOB, "00") & "-ACTIVITY-EX." & vbCrLf & _
                          indent & "    EXIT." & vbCrLf, "")
              openJOB = False
            End If
            If openREPORT Then
              writeTag CurrentTag
              CurrentPROC = CurrentPROC & "-" & ReportName
              CurrentTag = "PROC-" & ReportName & "(i)"
            End If
            'PROC:
            'Da gestire: nomi duplicati proc (per EZT la visibilit� � limitata al JOB)
            ' => rinominare col progressivo del JOB!
            statement = statement & CurrentPROC & IIf(isSection, " SECTION.", ".")
          Else
            m_Fun.writeLog "Aho?! " & originalStatement
          End If
        Else
          'LABEL
          statement = indent & "." & vbCrLf & token
        End If
      Else
        Dim lField As String, rField As String
        lField = token
        '!!!gestire RETURN-CODE!!
        'SILVIA
        If lField = "RETURN-CODE" Then
          BolRetCode = True
        End If
        token = nexttoken(statement)
        If Left(token, 1) = "(" And Right(token, 1) = ")" Then
           lField = lField & " " & token
           token = nexttoken(statement)
        End If
        Select Case token
          Case "=", "EQ"
            Dim i As Integer
            On Error GoTo catch
            'If ColIndex(lField) = "" Then
            For i = 1 To UBound(arrColIndex)
              If arrColIndex(i) = lField Then
                Exit For
              End If
            Next
            If i > UBound(arrColIndex) Then
          'SILVIA 25 NOVEMBRE 2008 L'UGUALE VA GESTITO SEMPRE CON MOVE
              If (InStr(statement, " + ") Or InStr(statement, "*") Or InStr(statement, " - ") Or InStr(statement, "/") And InStr(statement, "'") = 0) Then
                 statement = indent & "COMPUTE " & lField & " = " & vbCrLf & indent & statement
              Else
              '''Assegnamento: chiarire con move...
              '''If Left(statement, 1) = "'" Then
              '''Alfanumerico: MOVE
                If statement = "' '" Then
                  statement = indent & "INITIALIZE " & lField
                Else
                  statement = indent & "MOVE " & Replace(CheckReservedCBL(statement), ":RECORD-LENGTH", "-REC") & " TO " & Replace(lField, ":RECORD-LENGTH", "-REC")
                End If
               '''c'� altro?!
              '''Else
                'Numerico: COMPUTE
              ''   statement = indent & "COMPUTE " & lField & " = " & statement
              End If
            Else
               token = nexttoken(statement)
               statement = indent & "SET " & lField & " " & IIf(token = lField, Replace(Replace(statement, "-", "DOWN BY"), "+", "UP BY"), " TO " & token)
            End If
          Case "ROUNDED"
               statement = indent & "COMPUTE " & lField & indent & Replace(originalStatement, "=", "=" & vbCrLf & indent)
          Case "CONTINUE"
               statement = indent & token & "."
          Case Else
            If lField = "CONTINUE" Then
              statement = indent & "CONTINUE"
            Else
              token = lField & " " & token
              If Len(originalStatement) Then
              'T
                ''statement = "#" & token & originalStatement
               statement = "*" & "## STATEMENT: " & token
              Else
                'label
                statement = indent & "." & vbCrLf & token & "."
              End If
           End If
        End Select
      End If
  End Select
  tagWrite statement, Tag
  Exit Sub
catch:
  If err.Number = 5 Then
    Resume Next
  End If
End Sub
Private Function indent(Optional Offset As Integer = 0) As String
  'SQ tmp!!!!!!!!!!
  If indentlevel < 0 Then indentlevel = 0
  indent = Space(indentBase + indentOffset * indentlevel + Offset)
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub convertPGM(language As String, bolSection As Boolean)
  Dim previousTag As String
  Dim newriga As String
  Dim i As Long
  Set tagValues = New Collection
  isSection = bolSection
  Select Case language
    Case "COBOL"
      Ezt2cobol = True
      'INIT tags
      tagValues.Add "", "JOB-ACTIVITIES(i)"
      tagValues.Add "", "JOB-FINISH-ROUTINES(i)"
      tagValues.Add "", "WORKING-STORAGE(i)"
      tagValues.Add "", "ASSIGN(i)"
      'SILVIA
      tagValues.Add "", "REPORT-SECTION(i)"
      tagValues.Add "", "LINKAGE(i)"
      tagValues.Add "", "USING(i)"
      tagValues.Add "", "COMMENT(i)"
      '...
      DataLevel = 0
      listIndex = 0
      
      'INIT template
      Set RTB = MapsdF_Parser.rText
      On Error GoTo fileErr
      If GbTipoInPars = "EZM" Then
        RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & "\Macro"
        CurrentTag = "WORKING-STORAGE(i)"
      Else
        RTB.LoadFile m_Fun.FnPathPrj & "\" & templatePath & IIf(isSection, "\MainS", "\Main")
        CurrentTag = "COMMENT(i)"
      End If
     
      On Error GoTo catch

      changeTag RTB, "<PGM-NAME>", GbNomePgm
      
      deleteSegnalazioni GbIdOggetto

      parsePgm_I
      
      'JOB "fuori ciclo"
      If CurrentJOB Then
        'SILVIA
        If openJOB And isSection Then
          tagWrite indent & "." & IIf(isSection = False, vbCrLf & "JOB" & format(CurrentJOB, "00") & "-ACTIVITY-EX." & vbCrLf & _
                   indent & "EXIT." & vbCrLf, "")
        End If
        writeTag CurrentTag
      End If
      writeTag "COMMENT(i)"
      writeTag "WORKING-STORAGE(i)"
            
      cleanTags RTB
      newriga = ""
      For i = 0 To UBound(Split(RTB.Text, vbCrLf))
        If Trim(Split(RTB.Text, vbCrLf)(i)) <> "" Then
          newriga = newriga + Split(RTB.Text, vbCrLf)(i) + vbCrLf
        End If
      Next
      RTB.Text = newriga
      On Error GoTo saveErr
      RTB.SaveFile m_Fun.FnPathPrj & "\" & migrationPath & "\" & GbNomePgm, 1
    Case Else
      MsgBox "TMP: not supported conversion", vbInformation, Parser.PsNomeProdotto  '"i-4.Migration"
  End Select
  
  Exit Sub
fileErr:
  If err.Number = 52 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath
    Resume
  ElseIf err.Number = 75 Then
    MsgBox "Template file '" & m_Fun.FnPathPrj & "\" & templatePath & "\Main' not found.", vbExclamation, Parser.PsNomeProdotto
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
  End If
  Exit Sub
catch:
  m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
  Exit Sub
saveErr:
  If err.Number = 75 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\Languages\"
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\" & migrationPath
    Resume
  Else
    m_Fun.Show_MsgBoxError "RC0002", , "parsePgm_I", err.source, err.description, False
  End If
End Sub

Private Function AdaptLine(line As String, tagName As String) As String
  Select Case tagName
    Case "JOB-ACTIVITIES(i)"
      AdaptLine = " " & Replace(line, vbCrLf, vbCrLf & " ")
      AdaptLine = Replace(AdaptLine, "      -", "-     ")
    Case Else
      AdaptLine = line
  End Select
End Function
'''''''''''''''''''''''''''''''''''''''
' SQ 26-11-07
' Problema "CurrentTAG"... parametro opzionale
'''''''''''''''''''''''''''''''''''''''
Private Sub tagWrite(line As String, Optional tagName As String)
  Dim tagValue As String
  
  On Error GoTo catch
  
  If Len(tagName) = 0 Then
    tagName = CurrentTag
  End If
  'AC
  'Line = AdaptLine(Line, tagName)
  
  tagValue = tagValues.item(tagName)
  tagValues.Remove tagName
  tagValues.Add tagValue & line & vbCrLf, tagName
  
  Exit Sub
catch:
  If err.Number = 5 Then '424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    tagValues.Add "", CurrentTag
    Resume
  End If
End Sub

Private Sub writeTag(Tag As String)
  Dim tagValue As String
  
  On Error GoTo catch
  
  tagValue = tagValues.item(Tag)
  changeTag RTB, "<" & Tag & ">", tagValue
  tagValues.Remove Tag
  tagValues.Add "", Tag
  
  Exit Sub
catch:
  If err.Number = 424 Then
    'Elemento non in collection: (per ora lo aggiungo!)
    'tagValues.Add "", Tag
    'Resume
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parseJOB(statement As String)
  Dim token As String
  Dim jobStart As String, jobRestart As String, jobFinish As String, keyname As String, keystatement As String
  Dim stMove As String
  Dim a As Long, j As Integer, lKey As Integer
  Dim rs As Recordset
  stMove = ""
  CurrentJOB = CurrentJOB + 1
  ReDim fileName(0)
  lKey = 0
  ''''''''''''''''''''''''''''''''
  ' Scrittura JOB precedente
  ''''''''''''''''''''''''''''''''
  If CurrentJOB > 1 Then
    writeTag CurrentTag
    If openJOB And isSection Then
      tagWrite indent & "." & IIf(isSection = False, vbCrLf & "JOB" & format(CurrentJOB - 1, "00") & "-ACTIVITY-EX." & vbCrLf & _
               indent & "    EXIT." & vbCrLf, "")
    End If
  End If
  openJOB = True
  
  ''''''''''''''''''''''''''''''''
  ' Nuovo JOB:
  ''''''''''''''''''''''''''''''''
  changeTag RTB, "<<JOB(i)>>", m_Fun.FnPathPrj & "\" & templatePath & IIf(isSection, "\JobS", "\Job")
  changeTag RTB, "<JOB-NAME>", "JOB" & format(CurrentJOB, "00")
  changeTag RTB, "<PERFORM-JOB(i)>", "PERFORM JOB" & format(CurrentJOB, "00") & IIf(isSection = False, " THRU JOB" & format(CurrentJOB, "00") & "-EX.", ".")
  tagWrite "JOB" & format(CurrentJOB, "00") & "-ACTIVITY" & IIf(isSection, " SECTION.", ".")

  'Gestione PARAMETRI:
  'SILVIA 27-01-2009
  '''''jobFinish = "*FINISH: NULL" 'INIT
  
  While Len(statement)
    token = nexttoken(statement)
    Select Case token
      Case "CHECKPOINT"
      Case "ENVIRONMENT"
      Case "FINISH"
        token = nextToken_new(statement)
        If Left(token, 1) = "(" Then
          token = Trim(Mid(Replace(token, ")", ""), 2))
        End If
        'SILVIA 27-01-2009
        'jobFinish = "*FINISH:" & vbCrLf &
         jobFinish = "PERFORM " & token & IIf(isSection = False, " THRU " & token & "-EX.", ".")
      Case "INPUT"
        ReDim Preserve fileName(UBound(fileName) + 1)
        If InStr(statement, " KEY ") Or InStr(statement, " KEY(") Or InStr(statement, " KEY (") Then
          If Left(statement, 1) = "(" Then
            If Right(statement, 1) = ")" Then
              statement = Mid(statement, 2, Len(statement) - 2)
            Else
              Dim afterP As String
              afterP = Right(statement, Len(statement) - InStrRev(statement, ")"))
              statement = Mid(statement, 2, InStrRev(statement, ")") - 2) & afterP
            End If
          End If
          token = nextToken_new(statement)
          fileName(UBound(fileName)) = token
          ' Mauro 04/12/2007 : Da gestire eventuale ciclo per N file in input
          For a = 1 To UBound(fileEZT)
            If fileEZT(a).fileName = fileName(UBound(fileName)) Then
              Exit For
            End If
          Next a
          If Not a > UBound(fileEZT) Then
            fileEZT(a).openInput = True
          End If
       
          token = nextToken_new(statement)
          
          keyname = nextToken_new(statement)
          keyname = Replace(keyname, "(", "")
          keyname = Replace(keyname, ")", "")
          keyname = Trim(keyname)
          keystatement = "01 W-KEY-" & fileName(UBound(fileName)) & "."
          tagWrite keystatement, "WORKING-STORAGE(i)"
          If UBound(Split(keyname, " ")) > 0 Then
            keyname = Replace(keyname, ",", "")
            For j = 0 To UBound(Split(keyname, " "))
              Set rs = m_Fun.Open_Recordset("select * from PsData_Cmp where IdOggetto = " & GbIdOggetto & " AND Nome = " & "'" & Trim(Split(keyname, " ")(j)) & "'")
              If rs.RecordCount Then
                keystatement = "  02 KEY-" & Trim(Split(keyname, " ")(j)) & "        PIC " & rs!Lunghezza & Switch(rs!Usage = "PCK", " COMP-3", rs!Usage = "BNR", " COMP", True, "") & "."
                tagWrite keystatement, "WORKING-STORAGE(i)"
                stMove = stMove & indent & "MOVE " & Trim(Split(keyname, " ")(j)) & " TO KEY-" & Trim(Split(keyname, " ")(j)) & vbCrLf
              End If
              lKey = lKey + rs!Lunghezza
              rs.Close
            Next j
          Else
              Set rs = m_Fun.Open_Recordset("select * from PsData_Cmp where IdOggetto = " & GbIdOggetto & " AND Nome = " & "'" & Trim(keyname) & "'")
              If rs.RecordCount Then
                keystatement = "  02 KEY-" & keyname & "        PIC " & rs!Segno & rs!Tipo & "(" & rs!Lunghezza & ") " & Switch(rs!Usage = "PCK", " COMP-3", rs!Usage = "BNR", " COMP", True, "") & "."
                tagWrite keystatement, "WORKING-STORAGE(i)"
                stMove = indent & "MOVE " & keyname & " TO KEY-" & keyname & vbCrLf
              End If
              lKey = rs!Lunghezza
              rs.Close
          End If
          If Len(statement) And Not UBound(fileName) = 2 Then
            'changeTag RTB, "<RETRIEVE-INPUTS>", indent & "PERFORM READ-" & FileName(1) & " THRU READ-" & FileName(1) & "-EX"
            changeTag RTB, "<RETRIEVE-INPUTS-ROUTINES(i)>", _
                           "READ-" & fileName(1) & IIf(isSection, " SECTION.", ".") & vbCrLf & _
                           indent & "READ " & fileName(1) & vbCrLf & _
                           indent & "  AT END MOVE 10 TO " & fileName(1) & "-STATUS" & vbCrLf & _
                           indent & "  NOT AT END  " & vbCrLf & stMove & _
                           indent & "." & vbCrLf & IIf(isSection = False, _
                           "READ-" & fileName(1) & "-EX." & vbCrLf & _
                           indent & "EXIT.", "")
            statement = "INPUT " & statement
          Else
            changeTag RTB, "<RETRIEVE-INPUTS>", indent & "PERFORM READ-" & fileName(1) & IIf(isSection = False, " THRU READ-" & fileName(1) & "-EX", "") & vbCrLf & _
                           indent & "PERFORM READ-" & fileName(2) & IIf(isSection = False, " THRU READ-" & fileName(2) & "-EX", "") & vbCrLf & _
                           indent & "MOVE W-KEY-" & fileName(2) & " TO W-KEY-" & fileName(2) & "-SAVE" & vbCrLf & _
                           indent & "PERFORM UNTIL EOF-" & fileName(1) & " AND EOF-" & fileName(2)
            changeTag RTB, "<RETRIEVE-INPUTS-ROUTINES(i)>", _
                           "READ-" & fileName(2) & IIf(isSection, " SECTION.", ".") & vbCrLf & _
                           indent & "READ " & fileName(2) & vbCrLf & _
                           indent & "  AT END MOVE 10 TO " & fileName(2) & "-STATUS" & vbCrLf & _
                           indent & "  NOT AT END  " & vbCrLf & stMove & _
                           indent & "." & vbCrLf & IIf(isSection = False, _
                           "READ-" & fileName(2) & "-EX." & vbCrLf & _
                           indent & "EXIT.", "")
            changeTag RTB, "<END-RETRIEVE-INPUTS>", indent & "  PERFORM READ-" & fileName(2) & IIf(isSection = False, " THRU READ-" & fileName(2) & "-EX", "") & vbCrLf & _
                           indent & "  IF W-KEY-" & fileName(2) & " = W-KEY-" & fileName(2) & "-SAVE" & vbCrLf & _
                           indent & "     PERFORM READ-" & fileName(1) & IIf(isSection = False, " THRU READ-" & fileName(1) & "-EX", "") & vbCrLf & _
                           indent & "  END-IF" & vbCrLf & _
                           indent & "  MOVE W-KEY-" & fileName(2) & " TO W-KEY-" & fileName(2) & "-SAVE" & vbCrLf & _
                           indent & "END-PERFORM."
            tagWrite "01  W-KEY-" & fileName(2) & "-SAVE      PIC X(" & lKey & ").", "WORKING-STORAGE(i)"
          End If

        Else
          token = nextToken_new(statement)
          ' Mauro 03/12/2007 : Restituisce il token con le parentesi
          'SILVIA 29-01-2009
          token = Replace(token, "(", "")
          token = Replace(token, ")", "")
          Select Case token
            Case "NULL"
              'SILVIA 26-01-2009
              '''changeTag RTB, "<RETRIEVE-INPUTS>", "*INPUT: NULL"
              changeTag RTB, "<RETRIEVE-INPUTS>", ""
              changeTag RTB, "<END-RETRIEVE-INPUTS>", indent & "."
            Case "SQL"
            Case Else
              fileName(1) = token
              ' Mauro 04/12/2007 : Da gestire eventuale ciclo per N file in input
              For a = 1 To UBound(fileEZT)
                If fileEZT(a).fileName = fileName(1) Then
                  Exit For
                End If
              Next a
              If Not a > UBound(fileEZT) Then
                fileEZT(a).openInput = True
              End If
              '...
              token = nextToken_new(statement)
              If a < UBound(fileEZT) Then
                If fileEZT(a).filetype = "SQL" Then
                  changeTag RTB, "<RETRIEVE-INPUTS>", indent & "PERFORM READ-" & fileName(1) & IIf(isSection = False, " THRU READ-" & fileName(1) & "-EX", ".") & vbCrLf & _
                                 indent & "PERFORM UNTIL SQLCODE <> 0"
                  changeTag RTB, "<END-RETRIEVE-INPUTS>", indent & "   PERFORM READ-" & fileName(1) & IIf(isSection = False, " THRU READ-" & fileName(1) & "-EX", "") & vbCrLf & _
                                 indent & "END-PERFORM."
                  changeTag RTB, "<RETRIEVE-INPUTS-ROUTINES(i)>", _
                                 "READ-" & fileName(1) & IIf(isSection, " SECTION.", ".") & vbCrLf & _
                                 indent & "EXEC SQL FETCH " & fileName(1) & vbCrLf & _
                                 indent & StINTO & vbCrLf & _
                                 indent & "END-EXEC." & vbCrLf & _
                                 IIf(isSection = False, "READ-" & fileName(1) & "-EX." & vbCrLf & _
                                 indent & "EXIT.", "")
                  statement = token & " " & statement
               
                Else
                  '''
                  ' caso JOB INPUT FILE1
                  '''
                  changeTag RTB, "<RETRIEVE-INPUTS>", indent & "PERFORM READ-" & fileName(1) & IIf(isSection = False, " THRU READ-" & fileName(1) & "-EX", ".") & vbCrLf & _
                                 indent & "PERFORM UNTIL EOF-" & fileName(1)
                  changeTag RTB, "<END-RETRIEVE-INPUTS>", indent & "   PERFORM READ-" & fileName(1) & IIf(isSection = False, " THRU READ-" & fileName(1) & "-EX", "") & vbCrLf & _
                                 indent & "END-PERFORM."
                  changeTag RTB, "<RETRIEVE-INPUTS-ROUTINES(i)>", _
                                 "READ-" & fileName(1) & IIf(isSection, " SECTION.", ".") & vbCrLf & _
                                 indent & "READ " & fileName(1) & "." & vbCrLf & _
                                 IIf(isSection = False, "READ-" & fileName(1) & "-EX." & vbCrLf & _
                                 indent & "EXIT.", "")
                  statement = token & " " & statement
                End If
              Else
                 '''
                 ' caso JOB INPUT FILE1
                 '''
                 changeTag RTB, "<RETRIEVE-INPUTS>", indent & "PERFORM READ-" & fileName(1) & IIf(isSection = False, " THRU READ-" & fileName(1) & "-EX", ".") & vbCrLf & _
                                indent & "PERFORM UNTIL EOF-" & fileName(1)
                 changeTag RTB, "<END-RETRIEVE-INPUTS>", indent & "   PERFORM READ-" & fileName(1) & IIf(isSection = False, " THRU READ-" & fileName(1) & "-EX", "") & vbCrLf & _
                                indent & "END-PERFORM."
                 changeTag RTB, "<RETRIEVE-INPUTS-ROUTINES(i)>", _
                                "READ-" & fileName(1) & IIf(isSection, " SECTION.", ".") & vbCrLf & _
                                indent & "READ " & fileName(1) & "." & vbCrLf & _
                                IIf(isSection = False, "READ-" & fileName(1) & "-EX." & vbCrLf & _
                                indent & "EXIT.", "")
                 statement = token & " " & statement
              End If
          End Select
        End If
      Case "NAME"
      Case "RESTART"
      Case "START"
        token = nextToken_new(statement)
        If Left(token, 1) = "(" Then
          token = Trim(Mid(Replace(token, ")", ""), 2))
        End If
       ' jobStart = "*START:" & vbCrLf &
        jobStart = "PERFORM " & token & IIf(isSection = False, " THRU " & token & "-EX", "")
      Case Else
        
    End Select
  Wend
  
  changeTag RTB, "<JOB-START>", jobStart
  changeTag RTB, "<JOB-RESTART>", jobRestart
  changeTag RTB, "<JOB-FINISH>", jobFinish
End Sub

'COPIA D.M.
Public Sub cleanTags(rtbFile As RichTextBox)
  Dim regExpr As Object
  Dim matches As Variant, Match As Variant
  
  Set regExpr = CreateObject("VBScript.regExp")
  regExpr.Global = True
  
  regExpr.Pattern = "<<[^>\n]*\(i\)>>"
  Set matches = regExpr.Execute(rtbFile.Text)
  For Each Match In matches
    changeTag rtbFile, Match & "", ""
  Next
  
  'regExpr.Pattern = "<[^>]*(i)>" 'altrimenti mi accorpa tutte quelle di una riga...
  'regExpr.Pattern = "<[^>^\p]*\(i\)>"
  regExpr.Pattern = "<[^>\n]*\(i\)>"
  
  Set matches = regExpr.Execute(rtbFile.Text)
  For Each Match In matches
    'SQ PEZZA!
    'Mi tira su anche i "<="!!!!
    'Es:
    ''            "<= 50
    ''             DISPLAY 'STORE LOANTRN-1882 ' CPFAC-EEACN-C
    ''             DISPLAY HEX LOANTRN-1882
    ''           END-IF
    ''
    ''           .
    ''       3100-STORE-LOANTRN-EX.
    ''
    ''       3200-STORE-CPFTRN.
    ''           COMPUTE CPFTRN - TRNDTE - C = ""
    If InStr(Match, vbCrLf) = 0 Then
      changeTag rtbFile, Match & "", ""
    End If
  Next
  'T
   changeTag rtbFile, "<JOB-ACTIVITIES(i)>", ""
   changeTag rtbFile, "<>", ""
  Exit Sub
fileErr:
  MsgBox err.description
End Sub
Private Function getCobolData(eztfield As eztData) As cblData
  Dim cblField As cblData
  '
  cblField.Name = eztfield.fieldName
  '
  cblField.dataLen = eztfield.dataLen
  cblField.dataDec = eztfield.dataDec
  cblField.Segno = eztfield.Segno
  'silvia 23-04-2008
  cblField.Redefines = eztfield.Redefines
  '
  cblField.dataType(0) = Switch(eztfield.fieldType = "A", "X", eztfield.fieldType = "N" Or eztfield.fieldType = "B" Or eztfield.fieldType = "P", "9", True, "X")
  cblField.dataType(1) = Switch(eztfield.fieldType = "B", "COMP", eztfield.fieldType = "P", "COMP-3", True, "")
  cblField.bytes = eztfield.bytes
  cblField.Occurs = eztfield.Occurs
  'SILVIA GESTIONE OCCURS INDEXED
  cblField.dataType(2) = eztfield.Index
   
  ' Livello
'''  If Len(CurrentFILE) = 0 Then
'''    If eztField.location = "W" Or eztField.location = "S" Then
''''''      cblField.isGroup = False
''''''
''''''      CurrentBytes = 0
''''''      CurrentLevel = 1
''''''    Else
''''''      'TMP
''''''      CurrentLevel = 3
''''''    End If
''''''    cblField.level = format(CurrentLevel, "00")
''''''  Else
'''    If eztField.fieldPosition = 1 Then
'''      cblField.level = "01"
'''      cblField.isGroup = True
'''      CurrentBytes = eztField.bytes
'''      'CurrentLevel = 1
'''    End If
'''    Else
'''      'CurrentLevel
'''      '''CurrentTag = "WORKING-STORAGE"
'''      cblField.isGroup = eztField.isGroup
'''
'''      CurrentBytes = 0
'''      'CurrentLevel = 3
'''    End If
    cblField.isGroup = eztfield.isGroup
    If eztfield.isGroup Then
      cblField.dataLen = 0
    End If
    cblField.level = eztfield.livello

    ''End If
'  isDSECT = True
'  If dsLevBytes = 0 Then
'    cblLevel = format(dsLevel, "00")
'    If FIELD.dupFactor = "0" Then
'      If FIELD.Type = "C" And InStr(FIELD.modifier, "L") Then
'        'Campo di gruppo!
'        isGroupField = True
'        dsLevBytes = CInt(FIELD.modifierValue)
'        dsLevel = dsLevel + 2
'      Else
'        isDSECT = False
'      End If
'    Else
'      isGroupField = False
'    End If
'  Else
'    'SQ - Gestione sbagliata... se ritorno ad un livello superiore mi perdo il conto!
'    cblLevel = format(dsLevel, "00")
'    If FIELD.dupFactor = "0" Then
'      If FIELD.Type = "C" And InStr(FIELD.modifier, "L") Then
'        'Campo di gruppo!
'        isGroupField = True
'        'Attenzione: ASM � fetente: pu� "sforare" con i byte => livello 01!
'        If CInt(FIELD.modifierValue) <= dsLevBytes Then
'          dsLevBytes = CInt(FIELD.modifierValue)
'          dsLevel = dsLevel + 2
'        Else
'          'AGGIUNTA FILLER PER PAREGGIARE!
'          Print #fdOUT, Space(7) + Space(cblLevel - 1) & cblLevel & " FILLER     PIC X(" & dsLevBytes & ")." & Space(40) & "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
'          dsLevBytes = 0
'          dsLevel = 1
'          cblLevel = "01"
'        End If
'      Else
'        isDSECT = False
'      End If
'    Else
'      isGroupField = False
'      dsLevBytes = dsLevBytes - cblBytes  'Attenzione... ci vorrebbe "bytes!"
'      If dsLevBytes <= 0 Then
'        'ultimo elemento... reset
'        dsLevel = 1
'        If dsLevBytes < 0 Then
'          MsgBox "Warning! non � preciso!"
'          dsLevBytes = 0
'        End If
'      End If
'    End If
'''  End If
  cblField.value = eztfield.fieldValue
  
'  Name
'  isGroup
'  dataType (2)
'  dataLen
'  bytes
'  modifierValue
'  value
'  level
  
  getCobolData = cblField
End Function
'
Private Sub writeCobolData(cblData As cblData)
  Dim statement As String
  
  On Error GoTo catch
  If cblData.level = "" Then cblData.level = 1
  cblData.Name = Replace(cblData.Name, "_", "-")
  'SQ Da ripristinare quando isGroup sar� gestito bene:
  ' Mauro 28/08/2007 : per sicurezza controllo anche la lunghezza del campo
  'If False And cblData.isGroup Then
  If cblData.isGroup Or cblData.dataLen = 0 Then
   'silvia 21-04-2008 SE C'� METTO LA REDEFINES
    statement = Space(cblData.level - 1) & format(cblData.level, "00") & " " & cblData.Name & _
    IIf(cblData.Redefines <> "", " REDEFINES " & cblData.Redefines, "") & _
    IIf(Len(cblData.value), vbCrLf & "   VALUE " & IIf(cblData.dataType(0) = "X", "'" & cblData.value & "'", cblData.value), IIf(cblData.dataType(0) = "9" And cblData.Redefines = "", "   VALUE 0", "")) & "."    '& Space(40)
    'statement = Space(cblData.level - 1) & format(cblData.level, "00") & " " & cblData.Name & "."
  Else
    statement = Space(cblData.level - 1) & format(cblData.level, "00") & " " & cblData.Name & _
      Space(15 - Len(cblData.Name)) & "PIC " & cblData.Segno & cblData.dataType(0) & "(" & cblData.dataLen & ")" & IIf(cblData.dataDec > 0, "V" & cblData.dataType(0) & "(" & cblData.dataDec & ") ", " ") & cblData.dataType(1) & _
      IIf(Len(cblData.value) > 0, vbCrLf & "   VALUE " & IIf(cblData.dataType(0) = "X", "'" & cblData.value & "'", cblData.value), IIf(cblData.dataType(0) = "9" And cblData.level = "01", "   VALUE 0", "")) & "."  '& Space(40)
  End If
 'SILVIA 28-01-2009
  If cblData.Occurs > 0 Then
      statement = Space(cblData.level - 1) & format(cblData.level, "00") & " " & cblData.Name & _
      Space(15 - Len(cblData.Name)) & IIf(cblData.dataLen, "PIC " & cblData.dataType(0) & "(" & cblData.dataLen & ") " & cblData.dataType(1), "") & _
      " OCCURS " & cblData.Occurs & IIf(cblData.dataType(2) <> "", vbCrLf & Space(CLng(Len(cblData.Name)) + 8) & vbCrLf & "    INDEXED BY " & cblData.dataType(2), "") & "."
  End If
'silvia 21-04-2008
'ripristinato AC
  If Len(Trim(statement)) > 64 Then
    Dim indentlevel As String
    If cblData.level < 10 Then
      indentlevel = Space(3 + cblData.level)
    ElseIf cblData.level = 77 Then
      indentlevel = Space(4)
    ElseIf cblData.level = 88 Then
      indentlevel = Space(6)
    End If
    statement = Replace(statement, "VALUE", vbCrLf & Space(CLng(20 - Len(cblData.Name)) + 8) & indentlevel & "VALUE")
  End If
  
  'SQ
  tagWrite statement, "WORKING-STORAGE(i)"
  Exit Sub
catch:
  m_Fun.writeLog "#Ezt2Cobol error: " & err.description
  Resume Next
End Sub

Private Sub writeFD(cblData As cblData)
  Dim statement As String, originalDataName As String
  
  On Error GoTo catch
  'controllo parole riservate Cobol
  originalDataName = cblData.Name
  cblData.Name = CheckReservedCBL(cblData.Name)
  
  'SQ Da ripristinare quando isGroup sar� gestito bene:
  ' Mauro 28/08/2007 : per sicurezza controllo anche la lunghezza del campo
  'If False And cblData.isGroup Then
  cblData.Name = Replace(cblData.Name, "_", "-")
  If cblData.isGroup Or cblData.dataLen = 0 Then
   'silvia 21-04-2008 SE C'� METTO LA REDEFINES
    'statement = statement & Space(cblData.level - 1) & format(cblData.level, "00") & " " & cblData.Name & "."
    statement = statement & Space(cblData.level - 1) & format(cblData.level, "00") & " " & cblData.Name & _
    IIf(cblData.Redefines <> "", " REDEFINES " & cblData.Redefines, "") & "."
  Else
  'SILVIA 28-01-2009
    If cblData.Occurs > 0 Then
      statement = statement & Space(cblData.level - 1) & format(cblData.level, "00") & " " & cblData.Name & _
      Space(20 - Len(cblData.Name)) & IIf(cblData.dataLen, "PIC " & cblData.dataType(0) & "(" & cblData.dataLen & ") " & cblData.dataType(1), "") & _
      " OCCURS " & cblData.Occurs & IIf(cblData.dataType(2) <> "", vbCrLf & Space(CLng(Len(cblData.Name)) + 8) & vbCrLf & "    INDEXED BY " & cblData.dataType(2), "") & "."
    Else
      statement = Space(cblData.level - 1) & format(cblData.level, "00") & " " & cblData.Name & Space(20 - Len(cblData.Name)) & _
              " PIC " & cblData.dataType(0) & "(" & cblData.dataLen & ")"

    'SILVIA 22-04-2008
      statement = statement & IIf(CLng(cblData.dataDec) > 0, "V9(" & cblData.dataDec & ")", "") & IIf(Len(cblData.dataType(1)), " ", "") & cblData.dataType(1)
      statement = statement & IIf(Len(cblData.value), " VALUE " & IIf(cblData.dataType(0) = "X", "'" & cblData.value & "'", cblData.value), "") & "." & Space(40)
    End If
  End If
  
  cblData.Name = originalDataName
  
  'silvia 21-04-2008
  If Len(Trim(statement)) > 60 Then
    statement = Replace(statement, "VALUE", vbCrLf & Space(CLng(Len(cblData.Name)) + 8) & "VALUE")
  End If
'SQ
  changeTag RTB, "<FD-" & CurrentFILE & "(i)>", statement
  Exit Sub
catch:
  m_Fun.writeLog "#Ezt2Cobol error: " & err.description
  Resume Next
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Modifica gli operatori (delle IF)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function parseIF(statement As String) As String
  Dim token As String, originalStatement As String
  Dim operator As String
  Dim j As Integer
  On Error GoTo catch
  
  originalStatement = statement
  token = nexttoken(statement)
  parseIF = token
  'operatore
  token = nexttoken(statement)
  operator = Switch(token = "EQ", "EQUAL", _
                    token = "NE", "NOT EQUAL", _
                    token = "NQ", "NOT EQUAL", _
                    token = "LT", "<", _
                    token = "GT", ">", _
                    token = "GE", ">=", _
                    token = "GQ", ">=", _
                    token = "LE", "<=", _
                    token = "LQ", "<=", _
                    True, token)
                 
  'Da gestire:
  'A = '1' '2' '3' => A = '1' OR '2' OR '3'
  'silvia 28-01-2009
''  originalStatement = statement
''  token = nextToken(statement)
''  If Len(statement) And UBound(Split(statement, " ")) > 1 Then
''     If (InStr(statement, " + ") Or InStr(statement, "*") Or InStr(statement, " - ") Or InStr(statement, "/")) Then
''     Else
''       statement = ""
''       For j = 0 To UBound(Split(originalStatement, " "))
''         statement = IIf(Trim(Split(originalStatement, " ")(j)) <> "", Split(originalStatement, " ")(j) & vbCrLf & " OR " & statement, Split(originalStatement, " ")(j) & statement)
''       Next
''       statement = Left(Trim(statement), Len(statement) - 3)
''     End If
    statement = Replace(statement, " EQ ", " EQUAL ")
    statement = Replace(statement, " NE ", " NOT EQUAL ")
    statement = Replace(statement, " NQ ", " NOT EQUAL ")
    statement = Replace(statement, " LT ", " < ")
    statement = Replace(statement, " GT ", " > ")
    statement = Replace(statement, " GE ", " >= ")
    statement = Replace(statement, " GQ ", " >= ")
    statement = Replace(statement, " LE ", " >= ")
    statement = Replace(statement, " LQ ", " >= ")
    statement = IIf(operator = "NOT EQUAL", Replace(statement, " , ", " AND "), Replace(statement, " , ", " OR "))
    statement = IIf(operator = "NOT EQUAL", Replace(statement, ", ", " AND "), Replace(statement, ", ", " OR "))

    If Len(originalStatement) > 55 Then
      statement = Replace(statement, " AND ", vbCrLf & indent & " AND ")
      statement = Replace(statement, " OR ", vbCrLf & indent & " OR ")
    End If
    
    statement = Replace(statement, " EQ ", " EQUAL ")
    If InStr(statement, " THRU ") Then
      token = nexttoken(statement)
      parseIF = parseIF & " >= " & token & " AND " & parseIF & " <= " & Mid(statement, InStr(statement, " THRU ") + 5)
    Else
      parseIF = parseIF & " " & operator & " " & statement
    End If
  Exit Function
catch:
  m_Fun.writeLog "#parseIF error: " & statement
  'Non modifichiamo!
  parseIF = originalStatement
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' PARSER REPORT EASYTRIEVE
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub parserREPORT(fdIN As Long, fdOUT As Long, line As String)
  Dim token As String, statement As String, Temp As String
  Dim rs As Recordset, rsFile As Recordset

  Parser.PsConnection.Execute "Delete * from PsEZ_Report where IdOggetto = " & GbIdOggetto

  Set rs = m_Fun.Open_Recordset("select * from PsEZ_Report")
  rs.AddNew
  rs!idOggetto = GbIdOggetto
  
  ReportName = nextToken_new(line)
  rs!ReportName = ReportName
  rs!IdReport = NumReport
  While Len(line)
    
    token = nextToken_new(line) 'MANTIENE GLI APICI!!
    Select Case token
      '''''''''''''''''
      ' FORMAT PARMS
      '''''''''''''''''
      Case "SUMMARY"
         rs!Summary = True
      Case "SUMFILE"
      Case "SUMSPACE"
        Temp = nextToken_new(line)
      Case "TALLYSIZE"
        Temp = nextToken_new(line)
      Case "DTLCTL"
        'nexttoken
        'case "EVERY"
        'case "FIRST"
        'case "NONE "
      Case "SUMCTL"
      '''
      ' label report and parm
      Case "LABELS"
        rs!Labels = True
      Case "ACROSS"
        Temp = nextToken_new(line)
        rs!Across = Temp
      Case "DOWN"
        Temp = nextToken_new(line)
        rs!Down = Temp
      Case "SIZE"
        Temp = nextToken_new(line)
        rs!Size = Temp
      '
      '''
      Case "FILE"
      Case "PRINTER"
        Temp = nextToken_new(line)
        rs!Printer = Temp
        Set rsFile = m_Fun.Open_Recordset("Select idreport from psez_file where " & _
                                          "filename = '" & Temp & "' and idoggetto = " & GbIdOggetto)
        If rsFile.RecordCount Then
          rsFile!IdReport = NumReport
          rsFile.Update
        End If
        rsFile.Close
      '''''''''''''''''
      ' SPACING PARMS
      '''''''''''''''''
      Case "PAGESIZE"
        Temp = nextToken_new(line)
        rs!PageSize = Temp
      Case "LINESIZE"
        Temp = nextToken_new(line)
        rs!LineSize = Temp
      Case "SKIP"
      Case "SPACE"
        If Len(line) Then
          rs!Space = nextToken_new(line)
        End If
      Case "TITLESKIP"
      Case "CONTROLSKIP"
      Case "SPREAD"
        rs!Spread = True
      Case "NOSPREAD"
        rs!Spread = False
      Case "NOADJUST"
        rs!NoAdjust = True
      Case "NODATE", "NOKDATE", "LONGDATE", "SHORTDATE"
        rs!NoDate = True
      Case "SUM"
        If Len(line) Then
          rs!Sum = nextToken_new(line)
        End If
      Case "NOPAGE", "NOKPAGE"
        rs!NoPage = True
      Case "NOHEADING"
      Case "NONE"
      '''''''''''''''''
      ' TESTING
      '''''''''''''''''
      Case "LIMIT", "EVERY"
      '''''''''''''''''
      ' IMS-only parameters
      '''''''''''''''''
      Case "CHECKPOINT"
      Case Else
        If Len(line) Then
          'ripristino
          line = token & " " & line
          'attenzione!!! poi tronca a 72
          If Len(line) > 72 Then MsgBox "TMP: line > 72 - " & line
          Exit Sub
        End If
    End Select
  Wend
  rs.Update
  rs.Close
  
  Parser.PsConnection.Execute "Delete * from PsEZ_Title where IdOggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsEZ_Sequence where IdOggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsEZ_Control where IdOggetto = " & GbIdOggetto
  'opzione HEADING a livello della definizione di campo
  'Parser.PsConnection.Execute "Delete * from PsEZ_Heading where IdOggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsEZ_Line where IdOggetto = " & GbIdOggetto
  While Not EOF(fdIN)
    'Costruzione statement: gestione commenti+multilinea
    statement = getStatement(fdIN, fdOUT, line)
    token = nextToken_new(statement) 'MANTIENE GLI APICI!!
    Select Case token
      Case "SEQUENCE"
        parserSequence fdIN, fdOUT, statement
      Case "CONTROL"
        parserControl fdIN, fdOUT, statement
      Case "TITLE"
        parserTitle fdIN, fdOUT, statement
      Case "HEADING"
        'opzione HEADING a livello della definizione di campo
        'nel caso si ritrovasse occorrerebbe warning
        ' per ora lasciamo stare il parser
      Case "SUM"
        Parser.PsConnection.Execute "Update PsEZ_Report set [Sum] = '" & statement & "' where IdOggetto = " & GbIdOggetto
        'parserHeading fdIN, fdOUT, statement
      Case "LINE"
        parserLine fdIN, fdOUT, statement
    End Select
  Wend
  'Tabella Heading riempita a partire dalla collection StHEADING  (statement, eztfield.fieldName)
  'Dim count As Integer
  'For count = 1 To StHEADING.count
    'Parser.PsConnection.Execute "Insert into PsEZ_Heading (IdOggetto,FieldName,Stringa,IdReport,Ordinale) values (" _
                                '& GbIdOggetto & "," & StHEADING.
  'Next
End Sub

Private Sub parserSequence(fdIN As Long, fdOUT As Long, line As String)
  Dim token As String, statement As String, Temp As String
  Dim rs As Recordset

  Set rs = m_Fun.Open_Recordset("select * from PsEZ_Sequence")
  rs.AddNew
  rs!idOggetto = GbIdOggetto
  rs!IdReport = NumReport
  While Len(line)
   token = nextToken_new(line) 'MANTIENE GLI APICI!!
   Select Case token
     Case "D"
       rs!Descending = True
     Case Else
       rs!fieldName = token
   End Select
  Wend
  rs.Update
  rs.Close
End Sub
  
Private Sub parserControl(fdIN As Long, fdOUT As Long, line As String)
  Dim token As String, statement As String, Temp As String
  Dim rs As Recordset
  Dim IsRenum, IsNewpage, IsFinal As Boolean
  Dim i As Integer
  'utilizziamo strutture di appoggio e salviamo alla fine
  'final e noprint riferiti al singolo campo
  'campio fittizio CONTROLHEADER per newpage, renum e final che sono riferite al blocco di controllo
  Dim arrFields() As String
  Dim arrIsNoPrint() As Boolean
  
  IsNewpage = False
  IsRenum = False
  IsFinal = False
  Set rs = m_Fun.Open_Recordset("select * from PsEZ_Control")
  
  
  ReDim arrFields(0)
  ReDim arrIsFinal(0)
  ReDim arrIsNoPrint(0)
  
  While Len(line)
   token = nextToken_new(line) 'MANTIENE GLI APICI!!
   Select Case token
   Case "NEWPAGE"
     IsNewpage = True
   Case "NOPRINT"
     arrIsNoPrint(i) = True
   Case "RENUM"
     IsRenum = True
   Case "FINAL"
     IsFinal = True
   Case Else ' ho trovato  campo
     arrFields(i) = token
     i = i + 1
     ReDim Preserve arrFields(UBound(arrFields) + 1)
     ReDim Preserve arrIsNoPrint(UBound(arrIsNoPrint) + 1)
   End Select
  Wend
  ' info generali
  rs.AddNew
  rs!idOggetto = GbIdOggetto
  rs!IdReport = NumReport
  rs!fieldName = "*CONTROLHEADER*"
  rs!NewPage = IsNewpage
  rs!renum = IsRenum
  rs!final = IsFinal
  
  For i = 0 To UBound(arrFields) - 1
    rs.AddNew
    rs!idOggetto = GbIdOggetto
    rs!IdReport = NumReport
    rs!fieldName = arrFields(i)
    rs!noprint = arrIsNoPrint(i)
  Next i
  rs.Update
  rs.Close

End Sub
Function getFieldLen(fieldName As String) As Integer
Dim rs As Recordset
   Set rs = m_Fun.Open_Recordset("select Lunghezza from PsData_Cmp where IdOggetto = " & GbIdOggetto _
                                 & " and Nome = '" & fieldName & "'")
   If Not rs.EOF Then
      getFieldLen = rs!Lunghezza
   End If
End Function


  
Private Sub parserTitle(fdIN As Long, fdOUT As Long, line As String)
  Dim token As String, statement As String, arrTitle() As String, indent As Integer
  Dim afterindent As String, arrFieldName() As String, arrCol() As Integer
  Dim rs As Recordset, i As Integer, numline As Integer, prog As Integer
  
  ' Scriviamo pi� record nel caso di stringhe seguite da campi
  
  'linea ---> unica per i record
  
  'Array
  'colonna ---> la prima esplicita, le altre calcolate a partire da stringa pi� campi precedenti
  'title   ---> se solo stringhe scriviamo un unico record concatenandole
  'fieldname ---> se presenti avremo un CONTROL HEADING invece di un PAGE HEADING
  
  'NOTA afterindent e aftercol ??? nel db mai presenti
  
  
  ReDim arrCol(0)
  ReDim arrFieldName(0)
  ReDim arrTitle(0)
  
  token = nextToken_new(line)
  
  If IsNumeric(token) Then
    numline = token
  Else
    numline = 1
    arrTitle(0) = token
    ReDim Preserve arrTitle(1)
  End If
  
  
  While Len(line)
    token = nextToken_new(line)
    If Left(token, 1) = "(" And Right(token, 1) = ")" Then
       token = Mid(token, 2, Len(token) - 2)
    End If
    If IsNumeric(token) Then
      rs!indent = token
      indent = token
    Else
      If token = "COL" Then
        token = nextToken_new(line)
        arrCol(UBound(arrCol)) = token
        'token = nextToken_new(line)
        'rs!aftercol = token
      Else
        If indent <> 0 Then
          afterindent = afterindent & " " & token
        Else
          If Left(token, 1) = "'" Then
            'title = title & " " & token
            arrTitle(UBound(arrTitle)) = token
            If UBound(arrTitle) > UBound(arrCol) Then
              ' significa che ho stringa per cui non ho determinato posizione di partenza
              ReDim Preserve arrCol(UBound(arrCol) + 1)
              arrCol(UBound(arrCol)) = arrCol(UBound(arrCol) - 1) + Len(arrTitle(UBound(arrTitle) - 1)) - 2 ' -2 + 1
              'se al livello precedente c'era anche un campo somma anche questa lunghezza
              If UBound(arrFieldName) = UBound(arrTitle) Then
                If Not arrFieldName(UBound(arrFieldName) - 1) = "" Then
                  arrCol(UBound(arrCol)) = arrCol(UBound(arrCol)) + getFieldLen(arrFieldName(UBound(arrFieldName) - 1))
                End If
              End If
            End If
            ReDim Preserve arrTitle(UBound(arrTitle) + 1)
          Else
            arrFieldName(UBound(arrFieldName)) = token
            ReDim Preserve arrFieldName(UBound(arrFieldName) + 1)
            ' assumiamo che field segua sempre la stringa del titolo e che quindi sia stata gi� calcolata
            ' la posizione della colonna
          End If
        End If
      End If
    End If
  Wend
  prog = 1
  Set rs = m_Fun.Open_Recordset("select * from PsEZ_Title")
  
    For i = 0 To UBound(arrTitle) - 1
      rs.AddNew
      rs!idOggetto = GbIdOggetto
      rs!IdReport = NumReport
      
      rs!Title = arrTitle(i)
      rs!prog = prog
      rs!afterindent = afterindent
      rs!linenum = numline
      rs!Col = arrCol(i)
      prog = prog + 1
      If i <= UBound(arrFieldName) Then
        If Not arrFieldName(i) = "" Then
          rs.AddNew
          rs!idOggetto = GbIdOggetto
          rs!IdReport = NumReport
          rs!prog = prog
          rs!afterindent = afterindent
          rs!linenum = numline
          rs!Col = arrCol(i) + (Len(arrTitle(i)) - 2)
          rs!fieldName = arrFieldName(i)
          prog = prog + 1
        End If
      End If
    Next i
  rs.Update
  rs.Close
End Sub
  
Private Sub parserHeading(fdIN As Long, fdOUT As Long, line As String)
  Dim token As String, statement As String, heading As String
  Dim rs As Recordset
  
  Set rs = m_Fun.Open_Recordset("select * from PsEZ_Heading")
  rs.AddNew
  rs!idOggetto = GbIdOggetto
  rs!IdReport = NumReport
  token = nextToken_new(line) 'MANTIENE GLI APICI!!
  heading = token
  While Len(line)
    heading = heading & " , " & token
  Wend
  rs!heading = heading
  rs.Update
  rs.Close
End Sub

Private Sub parserLine(fdIN As Long, fdOUT As Long, line As String)
  Dim token As String, statement As String, linenum As Integer
  Dim rs As Recordset, Ordinale As Integer

  Ordinale = 0
  Set rs = m_Fun.Open_Recordset("select * from PsEZ_Line")
  rs.AddNew
  rs!idOggetto = GbIdOggetto
  rs!IdReport = NumReport
  token = nextToken_new(line) 'MANTIENE GLI APICI!!
  If IsNumeric(token) Then
    linenum = token
    rs!lineNumber = token
    token = nextToken_new(line)
    Ordinale = Ordinale + 1
    rs!Ordinale = Ordinale
    rs!fieldName = token
  Else
    linenum = 1
    Ordinale = Ordinale + 1
    rs!Ordinale = Ordinale
    rs!fieldName = token
  End If
  While Len(line)
    rs.AddNew
    rs!idOggetto = GbIdOggetto
    rs!IdReport = NumReport
    rs!lineNumber = linenum
    token = nextToken_new(line)
    If IsNumeric(token) Then
      rs!indent = token
      token = nextToken_new(line)
      Ordinale = Ordinale + 1
      rs!Ordinale = Ordinale
      rs!fieldName = token
    Else
      If token = "POS" Then
        token = nextToken_new(line)
        rs!pos = token
        token = nextToken_new(line)
        Ordinale = Ordinale + 1
        rs!Ordinale = Ordinale
        rs!fieldName = token
      Else
        Ordinale = Ordinale + 1
        rs!Ordinale = Ordinale
        rs!fieldName = token
      End If
    End If
  Wend
  rs.Update
  rs.Close
End Sub
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 22-11-07
'''''''''''''''''''''''''''''''''''''''''''''''''''
'Private Sub getREPORT(fdIN As Long, fdOUT As Long, Line As String)
'  Dim token As String, statement As String
'  Dim Temp As String
'  'SILVIA: campi per GESTIONE REPORT WRITER
'  Dim stTitle As String, StlineTitle As String, StlineTitle2 As String, stdetails As String, stFields() As String, stline As String, stPH As String
'  Dim lineNumber As String, colNumber As Integer, i As Integer
'  Dim rs As Recordset
'  ReportName = ""
'  On Error GoTo catch
'  '''''''''''''''''''''''''''
'  'Analisi parametri REPORT:
'  '''''''''''''''''''''''''''
'  'fare
'
'  getREPORT_Parameters Line
'  changeTag RTB, "<<REPORT-SECTION(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\REPORT-SECTION"
'  changeTag RTB, "<RD(i)>", "RD " & ReportName & "-RECORD" & vbCrLf & "   PAGE LIMIT 60." & vbCrLf
'  stdetails = indent & "01 DETAIL-" & ReportName & "  TYPE IS DETAIL."
'
'  'While Not EOF(fdIN)
'    'Costruzione statement: gestione commenti+multilinea
'    'statement = getStatement(fdIN, fdOUT, Line)
'
'    'LINEA VALIDA:
''    If Left(statement, 1) = "%" Then
''      getMacro statement
''    Else
''      Dim statementS As String
''      Dim tokenS As String
''      token = nextToken_new(statement) 'MANTIENE GLI APICI!!
''      Select Case token
''        '''''''''''''''''
''        ' DEFINITION:
''        '''''''''''''''''
''
''        'da gestire
''        Case "SEQUENCE"
''        Case "CONTROL"
''        Case "SUM"
'
'        'gestione TITLE
''        Case "TITLE"
''          stPH = indent & "01  PAGE-HEAD TYPE IS PAGE HEADING."
''          token = nextToken_new(statement)
''          If IsNumeric(token) Then
''            lineNumber = token
''          Else
''            lineNumber = "PLUS 1"
''          End If
''         '' stTitle = indent & "01 REP-HEADING  TYPE    REPORT HEADING." & vbCrLf &
''
''          stTitle = stTitle & vbCrLf & indent & "  05 LINE " & lineNumber & "." & vbCrLf & _
''                    indent & "    10 COLUMN 10  PIC X(" & Len(statement) & ")" & vbCrLf & _
''                    indent & "       VALUE " & statement & "."
'
'        Set rs = m_Fun.Open_Recordset("select * from PsEZ_Title where IdOggetto = " & GbIdOggetto & " order by IDReport,lineNum")
'        If Not rs.EOF Then
'          stPH = indent & "01  PAGE-HEAD TYPE IS PAGE HEADING."
'          While Not rs.EOF
'            lineNumber = rs!linenum
'             stTitle = stTitle & vbCrLf & indent & "  05 LINE " & lineNumber & "." & vbCrLf & _
'                      indent & "    10 COLUMN 10  PIC X(" & Len(Trim(rs!title)) - 2 & ")" & vbCrLf & _
'                      indent & "       VALUE " & rs!title & "."
'            rs.MoveNext
'          Wend
'        End If
'
'          '''changeTag RTB, "<REPORT-HEADING(i)>", stTitle
'
'        Case "HEADING"
'        'SILVIA
'        ' 01  PAGE-HEAD
'        '     TYPE IS PAGE HEADING.
'          stPH = indent & "01  PAGE-HEAD TYPE IS PAGE HEADING."
'        Case "LINE"
'          token = nextToken_new(statement)
'          If IsNumeric(token) Then
'             lineNumber = token
'          Else
'             lineNumber = "PLUS 1"
'             statement = token & " " & statement
'          End If
'
'          stFields = Split(statement, " ")
'          StlineTitle = indent & "  05 LINE " & lineNumber & "."
'          StlineTitle2 = ""
'          colNumber = 2
'          '01   DETAIL-LINE-1 TYPE IS DETAIL.
'          '   03 LINE NUMBER IS PLUS 2.
'          '     05 COLUMN IS 82 PIC IS 9(2)
'          '        SOURCE IS LPRZ-OFEN-TASHLUM (1).
'
'
'          For i = 0 To UBound(stFields)
'            If stFields(i) <> "" Then
'              If StHEADING.count Then
'                StlineTitle2 = StlineTitle2 & IIf(StlineTitle2 <> "", vbCrLf, "") & _
'                               indent & "    10 COLUMN " & colNumber & " PIC X(" & Len(StHEADING(stFields(i))) - 2 & ")" & vbCrLf & _
'                               indent & "       VALUE " & StHEADING(stFields(i)) & "."
'''''              Else
'''''                StlineTitle2 = StlineTitle2 & IIf(StlineTitle2 <> "", vbCrLf, "") & _
'''''                               indent & "    10 COLUMN " & colNumber & " PIC X(" & Len(stFields(i)) & ")" & vbCrLf & _
'''''                               indent & "       VALUE " & stFields(i) & "."
'              End If
'             Set rs = m_Fun.Open_Recordset("select * from PsData_Cmp where IdOggetto = " & GbIdOggetto & " AND Nome = " & "'" & stFields(i) & "'")
'             If rs.RecordCount Then
'               stline = stline & IIf(stline <> "", vbCrLf, "") & _
'                           indent & "    10 COLUMN " & colNumber & " PIC " & rs!Tipo & "(" & rs!Lunghezza & ") " & Switch(rs!Usage = "PCK", " COMP-3", rs!Usage = "BNR", " COMP", True, "") & vbCrLf & _
'                           indent & "       SOURCE IS " & stFields(i) & "."
'             End If
'             rs.Close
'             colNumber = colNumber + Len(stFields(i)) + 2
'           End If
'         Next
'
'        changeTag RTB, "<PAGE-HEADING(i)>", stPH & vbCrLf & stTitle & vbCrLf & StlineTitle & vbCrLf & StlineTitle2
'        changeTag RTB, "<DETAILS(i)>", stdetails & vbCrLf & StlineTitle & vbCrLf & stline
'
'
'        '''''''''''''''''
'        ' FINE REPORT (ALTRO REPORT)
'        '''''''''''''''''
'        Case "REPORT"
'          'convertREPORT
'
'
'          getREPORT fdIN, fdOUT, statement
'
'
'          Line = ""
'          Exit Sub
'        '''''''''''''''''
'        ' FINE REPORT (ERRORE?!)
'        '''''''''''''''''
'        Case Else
'          If Len(statement) Then
'            'ripristino
'            Line = token & " " & statement
'            'attenzione!!! poi tronca a 72
'            If Len(Line) > 72 Then MsgBox "TMP: line > 72 - " & Line
'            Exit Sub
'          End If
'      End Select
'      '''tagWrite "#" & token & " " & statement
'    End If
'  Wend
'  '''''''''''''''''
'  ' FINE REPORT - (EOF)
'  '''''''''''''''''
'  'convertREPORT
'
'  Exit Sub
'catch:
'  m_Fun.writeLog "#Ezt2Cobol error: " & err.description
'End Sub
'Private Sub SplitHeadings(HeadingSplitted() As String)
'Dim i As Integer, stringHeader
'  ReDim HeadingSplitted(StHEADING.count, 5)
'  For i = 1 To UBound(arrHeadKeyStHEADING)
'   stringHeader = StHEADING.item(arrHeadKeyStHEADING(i))
'   j = 1
'   While Len(stringHeader)
'    HeadingSplitted(i, j) = nexttoken(stringHeader)
'    j = j + 1
'   Wend
'  Next
'End Sub
Function getControlList(idOggetto As Long, IdReport As Integer) As String
Dim rs As Recordset, control As String, count As Integer
  Set rs = m_Fun.Open_Recordset("select FieldName from PsEZ_COntrol where IdOggetto = " & idOggetto & " and IdReport = " & IdReport)
  While Not rs.EOF
    count = count + 1
    control = control & IIf(rs!fieldName = "*CONTROLHEADER*", "REPORT", rs!fieldName) & ", "
    rs.MoveNext
  Wend
  If count = 1 Then
    getControlList = "CONTROL IS " & Left(control, Len(control) - 2) & "." & vbCrLf
  Else
    getControlList = "CONTROLS ARE " & Left(control, Len(control) - 2) & "." & vbCrLf
  End If
End Function
Private Sub getREPORTTable(IdReport As Long)
  Dim token As String, statement As String
  Dim Temp As String
  'SILVIA: campi per GESTIONE REPORT WRITER
  Dim stTitle As String, StlineTitle As String, StlineTitle2 As String, stdetails As String, stFields As String, stline As String, stPH As String
  Dim lineNumber As String, colNumber As Integer, i As Integer
  Dim rs As Recordset, rs2 As Recordset, k As Integer
  Dim firstlineHeading As Integer
  Dim HeadingSplitted() As String
  Dim HeadingLunghezze() As String
  Dim stringHeader As String, j As Integer, maxHeadLines As Integer
  Dim spaceField As Integer
  Dim fieldForTitle As String, lenForTitle As Integer
  'ReportName = ""
  'On Error GoTo catch
  '''''''''''''''''''''''''''
  'Analisi parametri REPORT:
  '''''''''''''''''''''''''''
  'fare
  Dim linenumappo As Integer
  Dim psize, lsize As Integer
  Dim reportlsize As String, value As String, strreportRD As String
  changeTag RTB, "<<REPORT-SECTION(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\REPORT-SECTION"
  Set rs = m_Fun.Open_Recordset("select PageSize,LineSize,Space from PsEZ_Report where IdOggetto = " & GbIdOggetto & " and IDreport = " & IdReport)
  If Not rs.EOF Then
    If Not IsNull(rs!PageSize) Then
      psize = rs!PageSize
    End If
    If Not IsNull(rs!LineSize) Then
      lsize = rs!LineSize
    End If
  End If
  If Not lsize = 0 Then
    reportlsize = "   LINE LIMIT " & lsize & "." & vbCrLf
  End If
  strreportRD = "RD " & ReportName & "-RECORD" & vbCrLf & "   PAGE LIMIT " & IIf(psize = 0, 60, psize) & "." & vbCrLf & reportlsize
  strreportRD = strreportRD & "   " & getControlList(GbIdOggetto, 1)
  changeTag RTB, "<RD(i)>", strreportRD
  stdetails = indent & "01 DETAIL-" & ReportName & "  TYPE IS DETAIL."
  
  ReDim HeadingLunghezze(StHEADING.count)
  ' Gestione (split) delle stringhe di Heading
  ReDim HeadingSplitted(StHEADING.count, 5)
  maxHeadLines = 0
  
  

  'sezione PAGE HEADING se ho titoli oppure heading
  
  'Tabella Title (in StTitle la parte di title da attaccare a 01 PAGE-HEAD
  
  Set rs = m_Fun.Open_Recordset("select * from PsEZ_Title where IdOggetto = " & GbIdOggetto & " and IDreport = " & IdReport & " order by IDReport,lineNum,prog")
  If Not rs.EOF Then
   
    stPH = indent & "01  PAGE-HEAD TYPE IS PAGE HEADING."
    linenumappo = -1
    While Not rs.EOF
       lineNumber = rs!linenum
       If Not lineNumber = linenumappo Then
         stTitle = stTitle & IIf(linenumappo = -1, "", vbCrLf) & indent & "  05 LINE " & lineNumber & "." & vbCrLf
        linenumappo = rs!linenum
       End If
       If Not IsNull(rs!Title) Then
             fieldForTitle = indent & "       VALUE " & rs!Title & "."
             lenForTitle = Len(Trim(rs!Title)) - 2
       Else
             fieldForTitle = indent & "       SOURCE IS  " & rs!fieldName & "."
             lenForTitle = getFieldLen(rs!fieldName)
       End If
       stTitle = stTitle & indent & "    10 COLUMN " & IIf(IsNull(rs!Col), 1, rs!Col) & "  PIC X(" & lenForTitle & ")" & vbCrLf _
                & fieldForTitle & vbCrLf
      rs.MoveNext
    Wend
  ElseIf maxHeadLines > 0 Then
    stPH = indent & "01  PAGE-HEAD TYPE IS PAGE HEADING."
  End If
  rs.Close
  
  ' da questo numero di linea attacchiamo la parte di heading
  firstlineHeading = lineNumber + 1
  
  
  'Tabella Heading
'  Set rs = m_Fun.Open_Recordset("select * from PsEZ_Heading where IdOggetto = " & GbIdOggetto & " and IDreport = " & IdReport)
'  If Not rs.EOF Then
'    stPH = indent & "01  PAGE-HEAD TYPE IS PAGE HEADING."
'  End If
  
   'SPlit heading - inizializzazioni
  
   ReDim HeadingSplitted(StHEADING.count, 5)
   maxHeadLines = 0
   Dim indexH As Integer
   indexH = 0
  
  'Tabella Line - mi appoggio a questa tabella per trovare i campi linea, le relative lunghezze sulla psData_cmp e le informazioni
  '               sull'heading

  Dim rifLineNumber As Integer
  rifLineNumber = 0
  Set rs = m_Fun.Open_Recordset("select * from PsEZ_Line where IdOggetto = " & GbIdOggetto & " and IDreport = " & IdReport & " order by LineNumber,Ordinale")
  While Not rs.EOF
    
       If IsNumeric(rs!lineNumber) Then
          lineNumber = rs!lineNumber
       Else
          lineNumber = "PLUS 1"
       End If
       
       stFields = rs!fieldName
       'controllo parole riservate Cobol
       Dim originField As String
       originField = stFields
       stFields = CheckReservedCBL(stFields)
       If Not lineNumber = "PLUS 1" Then
        If Not lineNumber = rifLineNumber Then
          If Not StlineTitle = "" Then
           StlineTitle = StlineTitle & vbCrLf & StlineTitle2 '& vbCrLf & indent & "  05 LINE " & lineNumber & "."
          Else
           StlineTitle = indent & "  05 LINE " & lineNumber & "."
          End If
          StlineTitle2 = ""
          colNumber = 2
          rifLineNumber = lineNumber
        End If
       End If
           
         
       If stFields <> "" Then
        Set rs2 = m_Fun.Open_Recordset("select * from PsData_Cmp where IdOggetto = " & GbIdOggetto & " AND Nome = " & "'" & originField & "'")
        If rs2.RecordCount Then
          If StHEADING.count Then
           For k = 1 To UBound(arrHeadKey)
            If arrHeadKey(k) = originField Then
              Exit For
            End If
           Next
           If Not k > UBound(arrHeadKey) Then
           ' Gestione (split) delle stringhe di Heading
             indexH = indexH + 1
             stringHeader = StHEADING.item(arrHeadKey(k))
             j = 1
             While Len(stringHeader)
              HeadingSplitted(indexH, j) = nexttoken(stringHeader)
              j = j + 1
             Wend
             If j - 1 > maxHeadLines Then
              maxHeadLines = j - 1
             End If
             HeadingLunghezze(indexH) = rs2!Lunghezza
           'gestione heading
'           If Not k > UBound(arrHeadKey) Then
'            StlineTitle2 = StlineTitle2 & IIf(StlineTitle2 <> "", vbCrLf, "") & _
'                          indent & "    10 COLUMN " & colNumber & " PIC X(" & rs2!Lunghezza & ")" & vbCrLf & _
'                          indent & "       VALUE " & StHEADING(originField) & "."
'            End If
          End If
        End If
        stline = stline & IIf(stline <> "", vbCrLf, "") & _
                    indent & "    10 COLUMN " & colNumber & " PIC " & rs2!Tipo & "(" & rs2!Lunghezza & ") " & Switch(rs2!Usage = "PCK", " COMP-3", rs2!Usage = "BNR", " COMP", True, "") & vbCrLf & _
                    indent & "       SOURCE IS " & stFields & "."
      
        
        'colNumber = colNumber + Len(stFields) + 2
        
        'AC aggiungo oltre alla lunghezza del campo anche la spaziatura (parametro Space)
        colNumber = colNumber + rs2!Lunghezza + spaceField
        End If
        rs2.Close
      End If
   
    rs.MoveNext
  Wend
  rs.Close
  ReDim Preserve HeadingLunghezze(indexH)
  'scaliamo gli heading che hanno meno righe di quella massima
  'devono infatti rimanere vuote quelle in cima
  For i = 1 To UBound(HeadingLunghezze)
    j = maxHeadLines
    While HeadingSplitted(i, j) = "" And j > 1
      For k = j To 2 Step -1
        HeadingSplitted(i, k) = HeadingSplitted(i, k - 1)
      Next
      j = j - 1
   Wend
  Next
  StlineTitle2 = ""
  colNumber = 2
  For i = 1 To maxHeadLines
    colNumber = 2
    StlineTitle2 = StlineTitle2 & vbCrLf & indent & "  05 LINE " & firstlineHeading & "."
    For j = 1 To UBound(HeadingLunghezze)
      StlineTitle2 = StlineTitle2 & IIf(StlineTitle2 <> "", vbCrLf, "") & _
                              indent & "    10 COLUMN " & colNumber & " PIC X(" & HeadingLunghezze(j) & ")" & vbCrLf & _
                              indent & "       VALUE '" & HeadingSplitted(j, i) & "'."
      'AC aggiungo oltre alla lunghezza del campo anche la spaziatura (parametro Space)
      colNumber = colNumber + HeadingLunghezze(j) + spaceField
    Next j
    firstlineHeading = firstlineHeading + 1
  Next i
  
  'TO DO
  'PER I CAMPI CONTROL
  ' inserire il control footer per ogni campo CONTROl
   Set rs = m_Fun.Open_Recordset("select * from PsEZ_Control where IdOggetto = " & GbIdOggetto & " and IDreport = " & IdReport) ' & " order by ...")
   While Not rs.EOF
    rs.MoveNext
   Wend
  
  changeTag RTB, "<PAGE-HEADING(i)>", stPH & vbCrLf & stTitle & vbCrLf & StlineTitle2
  changeTag RTB, "<DETAILS(i)>", stdetails & vbCrLf & StlineTitle & vbCrLf & stline
End Sub


Private Sub getREPORT_ParametersTable(ReportName As String, filePrinter As String)
  writeTag CurrentTag
    
  If Not filePrinter = "" Then
    changeTag RTB, "<ASSIGN-REPORT(i)>", "SELECT " & filePrinter & " ASSIGN TO S-" & filePrinter & "." 'vbCrLf _

                                         
    changeTag RTB, "<FD-REPORT(i)>", "FD " & filePrinter & vbCrLf _
                                       & "    REPORT IS " & ReportName & "-RECORD."
  End If
  changeTag RTB, "<<REPORT-ROUTINES(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\REPORT-ROUTINES"
  'AC ho messo qui chiusura ACTIVITY-EX
  Dim performReport As String
  If openJOB Then
    'Chiusura JOB?
    performReport = indent & "." & vbCrLf & IIf(isSection = False, "JOB" & format(CurrentJOB, "00") & "-ACTIVITY-EX." & vbCrLf & _
                indent & "    EXIT." & vbCrLf, "")
    openJOB = False
  End If
  performReport = performReport & IIf(performReport = "", "", vbCrLf) & indent & "." & vbCrLf & "ROUT-" & ReportName & IIf(isSection, " SECTION.", ".") & vbCrLf _
                                      & "   <" & ReportName & "-ACTIVITIES(i)>" & vbCrLf _
                                      & IIf(isSection = False, "ROUT-" & ReportName & "-EX." & vbCrLf _
                                      & "    EXIT. " & vbCrLf, "") _
                                      & vbCrLf _
                                      & "<PROC-" & ReportName & "(i)>"
  changeTag RTB, "<PERFORM-REPORT>", performReport
  changeTag RTB, "<FILE-STATUS(i)>", "01 " & ReportName & "-STATUS" & Space(25 - Len(ReportName & "-STATUS")) & " PIC X(02) VALUE SPACES."
  changeTag RTB, "<OPEN-REPORTS(i)>", "OPEN OUTPUT " & ReportName & vbCrLf & "INITIATE " & ReportName & "-RECORD"
  changeTag RTB, "<CLOSE-REPORTS(i)>", "TERMINATE " & ReportName & "-RECORD" & vbCrLf & "CLOSE " & ReportName
  CurrentTag = ReportName & "-ACTIVITIES(i)"
  tagWrite "GENERATE  DETAIL-" & ReportName & ".", CurrentTag
  openREPORT = True
End Sub

Private Sub getREPORT_Parameters(line As String)
  Dim token As String
  Dim recLen As Long
  Dim Temp As String
  
  On Error GoTo catch
  
  ReportName = nextToken_new(line)
  While Len(line)
    
    token = nextToken_new(line) 'MANTIENE GLI APICI!!
    Select Case token
      '''''''''''''''''
      ' FORMAT PARMS
      '''''''''''''''''
      Case "SUMMARY"
      Case "SUMFILE"
      Case "SUMSPACE"
        Temp = nextToken_new(line)
      Case "TALLYSIZE"
        Temp = nextToken_new(line)
      Case "DTLCTL"
        'nexttoken
          'case "EVERY"
          'case "FIRST"
          'case "NONE "
      Case "SUMCTL"
      '''''''''''''''''
      ' FILE PARMS
      '''''''''''''''''
      Case "LABELS"
      Case "DOWN"
      Case "FILE"
      Case "PRINTER"
        Temp = nextToken_new(line)
      '''''''''''''''''
      ' SPACING PARMS
      '''''''''''''''''
      Case "PAGESIZE"
      Case "LINESIZE"
        recLen = nextToken_new(line)
      Case "SKIP"
      Case "SPACE"
        Temp = nextToken_new(line)
      Case "TITLESKIP"
      Case "CONTROLSKIP"
      Case "SPREAD", "NOSPREAD"
      Case "NOADJUST"
      Case "NODATE", "NOKDATE", "LONGDATE", "SHORTDATE"
      Case "NOPAGE", "NOKPAGE"
      Case "NOHEADING"
      '''''''''''''''''
      ' TESTING
      '''''''''''''''''
      Case "LIMIT", "EVERY"
      '''''''''''''''''
      ' IMS-only parameters
      '''''''''''''''''
      Case "CHECKPOINT"
      Case Else
        If Len(line) Then
          'ripristino
          line = token & " " & line
          'attenzione!!! poi tronca a 72
          If Len(line) > 72 Then MsgBox "TMP: line > 72 - " & line
          Exit Sub
        End If
    End Select
  Wend
  writeTag CurrentTag
    
  changeTag RTB, "<ASSIGN-REPORT(i)>", "SELECT " & ReportName & " ASSIGN TO S-" & ReportName & "." 'vbCrLf _
'                                       & "       ORGANIZATION IS SEQUENTIAL" & vbCrLf _
'                                       & "       FILE STATUS IS " & ReportName & "-STATUS" & vbCrLf _
'                                       & "       ACCESS MODE IS SEQUENTIAL."
                                       
  changeTag RTB, "<FD-REPORT(i)>", "FD " & ReportName & vbCrLf _
                                     & "    REPORT IS " & ReportName & "-RECORD."
'''                                       & "    RECORDING MODE IS F" & vbCrLf _
'''                                       & "    LABEL RECORDS ARE OMITTED" & vbCrLf _
'''                                       & "    BLOCK CONTAINS 0 RECORDS" & vbCrLf _
'''                                       & "    RECORD CONTAINS " & recLen & vbCrLf _
'''                                       & "    DATA RECORD IS " & ReportName & "-RECORD." & vbCrLf _
'''                                       & "01  " & ReportName & "-RECORD PIC X(" & recLen & ")."
  changeTag RTB, "<<REPORT-ROUTINES(i)>>", m_Fun.FnPathPrj & "\" & templatePath & "\REPORT-ROUTINES"
  changeTag RTB, "<PERFORM-REPORT>", indent & "." & vbCrLf & "ROUT-" & ReportName & IIf(isSection, " SECTION.", ".") & vbCrLf _
                                      & "   <" & ReportName & "-ACTIVITIES(i)>" & vbCrLf _
                                      & IIf(isSection = False, "ROUT-" & ReportName & "-EX." & vbCrLf _
                                      & "    EXIT. " & vbCrLf, "") _
                                      & vbCrLf _
                                      & "<PROC-" & ReportName & "(i)>"
  changeTag RTB, "<FILE-STATUS(i)>", "01 " & ReportName & "-STATUS" & Space(25 - Len(ReportName & "-STATUS")) & " PIC X(02) VALUE SPACES."
  changeTag RTB, "<OPEN-REPORTS(i)>", "OPEN OUTPUT " & ReportName & vbCrLf & "INITIATE " & ReportName & "-RECORD"
  changeTag RTB, "<CLOSE-REPORTS(i)>", "TERMINATE " & ReportName & "-RECORD" & vbCrLf & "CLOSE " & ReportName
  CurrentTag = ReportName & "-ACTIVITIES(i)"
  tagWrite "GENERATE  DETAIL-" & ReportName & ".", CurrentTag
  openREPORT = True
  
  Exit Sub
catch:
  m_Fun.writeLog "#Ezt2Cobol error: " & err.description
End Sub

Private Sub getPRINT(statement As String)
  ReportName = nextToken_new(statement)
  
  tagWrite indent & "PERFORM ROUT-" & ReportName & IIf(isSection, "", vbCrLf _
         & indent & "   THRU ROUT-" & ReportName & "-EX")
End Sub

'silvia
Private Sub getEOF(token As String, statement As String)
  Dim strtoken As String
  strtoken = nextToken_newnew(statement)
  
  tagWrite indent & token & " EOF-" & statement
End Sub

Private Sub writeLog(typeMsg As String, message As String)
  Dim rs As Recordset
  
  Set rs = m_Fun.Open_Recordset("SELECT * FROM MgEZT_segnalazioni WHERE IdOggetto= " & GbIdOggetto)
  rs.AddNew
  rs!idOggetto = GbIdOggetto
  rs!Riga = GbNumRec
  rs!tipoMsg = typeMsg
  rs!operatore = operator
  rs!descrizione = message
  rs.Update
  rs.Close
End Sub

Sub deleteSegnalazioni(idOggetto As Long)
  Parser.PsConnection.Execute "Delete * from MgEZT_segnalazioni where IdOggetto = " & idOggetto
End Sub

'silvia
Private Sub GetSearch(statement As String)
  Dim token As String, fileName As String, stsearch As String, stkey As String
    
'ESEMPIO:
'  SEARCH SAPCKS WITH WS-ACTP GIVING WS-CHECK-RANGE
'
'  MOVE 1 TO SAPCKS-I
'  SEARCH ALL SAPCKS-TAB
'    AT END SET SAPCKS-NF TO TRUE
'   WHEN ARG(SAPCKS-I) = WS-ACTP
'     SET SAPCKS-OK TO TRUE
'      MOVE DESC(SAPCKS-I) TO WS-CHECK-RANGE
'   END SEARCH
   
  fileName = nextToken_newnew(statement)
  token = nextToken_newnew(statement)
  stkey = nextToken_newnew(statement)
  
  stsearch = indent & "SET " & fileName & "-I TO 1" & vbCrLf & _
             indent & "SEARCH ALL " & fileName & "-TABLE" & vbCrLf & _
             indent & "AT END SET NF-" & fileName & " TO TRUE" & vbCrLf & _
             indent & "WHEN ARG(" & fileName & "-I )= " & stkey & vbCrLf & _
             indent & "  SET OK-" & fileName & " TO TRUE" & vbCrLf & _
             indent & "  MOVE DESC(" & fileName & "-I) TO " & Mid(statement, InStr(statement, "GIVING") + 6) & vbCrLf & _
             indent & "END-SEARCH"
  
  
''  stsearch = indent & "CLOSE " & fileName & vbCrLf & _
''             indent & "OPEN INPUT " & fileName & vbCrLf & _
''             indent & "READ " & fileName & vbCrLf & _
''             indent & "PERFORM UNTIL ARG NOT > " & stkey & vbCrLf & _
''             indent & "   OR EOF-" & fileName & vbCrLf & _
''             indent & "   READ " & fileName & vbCrLf & _
''             indent & "END-PERFORM " & IIf(InStr(statement, "GIVING"), vbCrLf & indent & "MOVE DESC TO " & Mid(statement, InStr(statement, "GIVING") + 6), "")
             
  tagWrite stsearch
End Sub
