Attribute VB_Name = "MapsdM_Parser"
Option Explicit
Option Compare Text

Global Const SYSTEM_DIR = "i-4.Mig.cfg"

Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long

'E' una copia di quello della funzioni...
'COME SI FA????? (A USARE QUELLO?)
Public Type t_ObjType
  oLabel As String
  oType As String
  oArea As String
  oLevel As String
  oDescrizione As String
  oEstensione As String
End Type
Dim ObjTypes() As t_ObjType

Type DefCampi
  Ordinale As Integer
  livello As String
  nome As String
  Tipo As String
  Usage As String
  Segno As String
  Lunghezza As Long
  Decimali As Integer
  value As String
  Picture As String
  Redefine As String
  StartByte As Double
  LenByte As Double
End Type

Type DefStruttura
  nome As String
  Campi() As DefCampi
End Type

Type t_trsEqu
  Alias As String
  cmd As String
End Type
Global mEqu() As t_trsEqu
 
Global Parser As New MapsdC_Menu
Global m_Fun As MaFndC_Funzioni
Global PsStrutture() As DefStruttura
Global PsMenu() As MaM1
Global SwMenu As Boolean

Type StrField
  nome As String
  Tipo As String
  Posizione As Integer
  Lunghezza As Integer
  Seq As Boolean
  Unique As Boolean
  Primario As Boolean
  Ptr_Punseq As String
  Ptr_DbdNome As String
  Ptr_Pointer As String
  Ptr_xNome As String
  Ptr_Segmento As String
  Ptr_Search As String
  Ptr_DData As String
  Ptr_SubSeq As String
  Ptr_NullVal As String
  Ptr_Index As Boolean
  'SQ: in teoria non serve qui... ma il parser va sistemato...
  XD_Extrtn As String
  'SP: 8/9 lchild
  Lchild As String
  ' Mauro 07/04/2008
  Segment As String
End Type

'SQ 15-12-06 - Per i segmenti dei DBD Logici
Type SEGM_SOURCE
  Segment As String
  Type As String
  DBD As String
End Type

Type StrSegmenti
  idSegmento As Variant
  nome As String
  Parent As String
  LParent As SEGM_SOURCE
  source(1) As SEGM_SOURCE
  MaxLen As Integer
  MinLen As Integer
  Pointer As String
  Rules As String
  CompRtn As String
  FIELD() As StrField
End Type

Global GbIndField As Integer
Global GbIndSeg As Integer
Global GbSegmenti() As StrSegmenti

Type PsStrParmRel
  AS_Macro As Boolean
  BM_Mapset As Boolean
  BM_Copy As Boolean
  MF_Mapset As Boolean
  MF_Copy As Boolean
  CB_Copy As Boolean
  CB_CallStat As Boolean
  CB_CallDyn As Boolean
  CB_Linkage As Boolean
  CX_Link As Boolean
  CX_XCTL As Boolean
  CX_Start_TR As Boolean
  CX_Return_TR As Boolean
  CX_Send As Boolean
  CX_Commarea As Boolean
  IMS_Call_Pgm As Boolean
  IMS_Call_TR As Boolean
  IMS_Transid As Boolean
  IMS_Send As Boolean
  IMS_SPAarea As Boolean
  DL_Pgm_Psb As Boolean
  DL_DecDli As Boolean
  DL_UseDli As Boolean
  VS_DecFile As Boolean
  VS_UseFile As Boolean
  RD_DecTable As Boolean
  RD_UseTable As Boolean
  JC_Proc As Boolean
  JC_Prog As Boolean
  JC_Utility As Boolean
  JC_Dlbl As Boolean
  DliStdPsb As Boolean
  DLIStdPsbName As String
  ImsStdPcb As Boolean
  ImsStdAltPcbName As String
  ImsStdIoPcbName As String
  ImsStdMstPcbName As String
  ImsStdSpaArea As String
  DM_DefSqlType As String
End Type

Public GbParmRel As PsStrParmRel

Type SenSegStr
  Segmento As String
  Parent As String
End Type

Type PcbStr
  ProcSeq As String
  ProcOpt As String
  Type As String
  NomePCB As String
  dbdname As String
  KeyLen As Integer
  SenSeg() As SenSegStr
End Type

Type KeyWord
  word As String
  lang As String
  Type As String
  column As Integer
  weight As Integer
  count As Long
End Type
Type language
  lang As String
  count As Long
  default As String
End Type

Dim keyWords() As KeyWord
' Mauro 08/08/2007 : Deve essere visto anche dal modulo di classe
Public lstWords As String

'gloria: gestione dinamica identificazione oggetto da dizionario
Dim Languages() As language

''''''''''''''''''''
''''''''''''''''''''
'Parametro per esplodere le copy durante il parsing
Global pmExpldeCopy As Boolean

'contiene la struttura guida dei pcb
Global wPcbStructure As t_PcbTemplate

Type t_IstrAlias
  istruzione As String
  Mnemonics As String
  Alias() As String
End Type

Type assign_file
  fileName As String
  ddname As String
  filetype As String
  filestatus As String
End Type
Global assign_files() As assign_file

Global gbIstrAlias() As t_IstrAlias

'Type pe ri campi BMS
Type t_Cmp_Bms
  nomeCmp As String
  PosX As Long
  PosY As Long
  Length As Long
  AttRB As String
  INITIAL As String
  Justify As String
  Color As String
  Hilight As String
  PicIN As String
  PicOUT As String
  attrib As String
  Function As String
  Exit As String
  Fill As String
End Type

'Ac struttura oggetto,riga,campo istruzione
Type Cmp_Istruzione
  Oggetto As Long
  Riga As Long
  CampoIstruzione As String
  Risolta As Integer  ' 1: by call  2: by move 0: not solved
  LastSerchIndex As Integer ' corrispondenza con ultima move confrontata
End Type
'Ac struttura campi move
Type UtCampiMove
  NomeCampo As String
  Valori As Collection
  Posizioni As Collection
End Type

Global CmpIstruzione() As Cmp_Istruzione
Global CampiMove() As UtCampiMove
Global CampiMoveCpy() As UtCampiMove
Global GbIdOggetto As Long

Global TbOggetti As Recordset
Global tbComponenti As Recordset
Global TbVSAM As Recordset
Global TbPSB As Recordset
Global TbPSBSEG As Recordset
'Global TbSegnalazioni As Recordset
'Global TbRelaObjUnk As Recordset
Global TbRela As Recordset

Global GbFileInput As String
Global GbTipoInPars As String
Global GbNomePgm As String
Global SwTpCX As Boolean
Global SwTpIMS As Boolean
Global SwDli As Boolean
Global SwSql As Boolean
Global SwMapping As Boolean
Global SwVsam As Boolean
Global SwBatch As Boolean
Global SwFileSection As Boolean
Global SwWorking As Boolean
Global SwProcedure As Boolean
Global SwSection As String  '  sezione attuale 'F' file section, 'W' Working, 'P' Procedure

Global ActParsDati As Boolean
Global ActParsRela As Boolean
Global ActParsCics As Boolean
Global ActParsIms As Boolean
Global ActParsSQL As Boolean
Global ActParsDli As Boolean
Global ActParsVSAM As Boolean

Global ActParsNoPsb As Boolean

Global SwPsbAssociato As Boolean
Global SwNomeCall As Boolean

Global SwParsExec As Boolean
Global SwSchedul As Boolean

Global idAree() As String
Global fSegm() As String

Global GbErrLevel As String

Global GbNumRec As Long
Global GbOldNumRec As Long

Type t_SourceType
  word As String
  sType As String
  daPos As Long
  wLinked() As String
End Type

Global gbWordType() As t_SourceType

'SQ 3-1-06
Global isDliCall As Boolean
Global IdOggettoRel As Long
Global RigaRel As Long
Global IsParseNeeded As Boolean  'Valorizzato in "checkRelations": per gli "oggetti" innestati...
'SQ 10-3-06
Global missingItemsSet As Collection
'3-05-06
'Environment Parameter: Naming Convention segmento/area
Global segmentNameParam As String
Global alreadyReadLine As String
'SQ 1-10-06: Migrazione ASM -> COBOL
Global Asm2cobol As Boolean
'MAuro 08/02/2011: Migrazione ASM -> SEQUENTIAL FILES
Global Asm2seqfiles As Boolean
'SQ 3-11-06: Migrazione EZT -> COBOL
Global Ezt2cobol As Boolean
'SILVIA 01-10-2008: Migrazione PLI -> COBOL
Global Pli2cobol As Boolean

'SQ
'AC 24/09/07
Global GbSegnObjRel As Long
'AC 11/05/10
Type StrCheckIncaps
    PGM As String
    line As Long
    routine As String
    Ordinale As Long
    OrdinaleMolt As Integer
End Type

'silvia 21/11/2007 : costanti per le intestazioni della lista Oggetti(PsObjList)
Global Const NAME_OBJ = 1
Global Const TYPE_OBJ = 2
Global Const DLI_OBJ = 3
Global Const CICS_OBJ = 4
Global Const RC_OBJ = 5
Global Const LOCS_OBJ = 6
Global Const NOTES_OBJ = 7
Global Const PARS_DATE_OBJ = 8
Global Const DIR_OBJ = 9
Global Const IMP_DATE_OBJ = 10

'silvia 7-1-2008
Global PsHideIgnore As Boolean
' Mauro 22/06/2010
Global isError As Boolean
' Mauro 10/12/2010
Type TpCampo
  Ordinale As Integer
  nome As String
  Alias_Colonna As String
  Funzione As String
  Tipo As String
  Lunghezza As Integer
  Decimali As Integer
  Tabella As String
  Alias_Tabella As String
End Type

Type TpTabella
  nome As String
  Alias As String
  Join_With As String
End Type

Public Sub load_wordSource_type()
  Dim rs As Recordset
  Dim s() As String
  Dim i As Long
  
  ReDim gbWordType(0)
  
  Set rs = m_Fun.Open_Recordset_Sys("select * From SysLangWords Order By SourceType")
  While Not rs.EOF
    ReDim Preserve gbWordType(UBound(gbWordType) + 1)
    gbWordType(UBound(gbWordType)).word = rs!word
    gbWordType(UBound(gbWordType)).sType = rs!sourcetype
    gbWordType(UBound(gbWordType)).daPos = rs!daPos
    
    ReDim Preserve gbWordType(UBound(gbWordType)).wLinked(0)
    
    If Not IsNull(rs!Linked) Then
    s = Split(rs!Linked, "#")
    
    For i = 0 To UBound(s)
      ReDim Preserve gbWordType(UBound(gbWordType)).wLinked(i)
      gbWordType(UBound(gbWordType)).wLinked(i) = s(i)
    Next i
    End If
    rs.MoveNext
  Wend
  rs.Close
End Sub

Function checkDupl(wIdOggetto As Long) As Long
  Dim rsOgg As Recordset, rsChkOgg As Recordset
  
  checkDupl = 0
  Set rsOgg = m_Fun.Open_Recordset("select Nome, tipo from bs_oggetti where idoggetto = " & wIdOggetto)
  If Not rsOgg.EOF Then
    Set rsChkOgg = m_Fun.Open_Recordset("select idoggetto from bs_oggetti where " & _
                                        "Nome = '" & rsOgg!nome & "' And tipo = '" & rsOgg!Tipo & "' And " & _
                                        "idoggetto <> " & wIdOggetto)
    If Not rsChkOgg.EOF Then
      checkDupl = rsChkOgg!idOggetto
    End If
    rsChkOgg.Close
  End If
  rsOgg.Close
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''
' FA SCHIFO 'STO ROBO!!!
' usare delete...
'''''''''''''''''''''''''''''''''''''''''''''''''
Function CancellaOggetto(wIdOggetto As Long) As Boolean
  Dim wSqlExec As String, wNomeOggetto As String
  Dim tb As Recordset, tb1 As Recordset
  Dim k As Integer
  Dim wGbIdOggetto As Variant
  
  GbIdOggetto = wIdOggetto
  wGbIdOggetto = GbIdOggetto
  
  ' Mauro 30/10/2007 : Cancello l'oggetto anche dalla BS_Oggetti
  Set tb = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  If tb.RecordCount Then
    wNomeOggetto = tb!nome
    tb.Delete
  End If
  tb.Close
  
  If Len(wNomeOggetto) Then
    k = InStr(wNomeOggetto, ".")
    If k > 0 Then wNomeOggetto = Mid$(wNomeOggetto, 1, k - 1)
    
    Parser.PsConnection.Execute "Delete * from PsJCL where idOggetto =" & GbIdOggetto
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Tools
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Parser.PsConnection.Execute "Delete * from Tl_AllignIstr where idOggetto=" & GbIdOggetto
    'Quell'altra?
    
    ' ELIMINA istruzioni
    'SQ COPY-ISTR
    If False Then
      Parser.PsConnection.Execute "Delete * from PsDli_Istruzioni where idPgm = " & GbIdOggetto
      'Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Dup where  idPgm = " & GbIdOggetto
      Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Liv where  idPgm = " & GbIdOggetto
      Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Key where  idPgm = " & GbIdOggetto
    End If
    'SQ - SERVE SOLO PER LE "COPY"...
    Parser.PsConnection.Execute "Delete * from PsDli_Istruzioni where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Dup where  idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Liv where  idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Key where  idoggetto = " & GbIdOggetto
    
    '  ELIMINA DATI
    Parser.PsConnection.Execute "Delete * from PsData_Area where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsData_Cmp where idoggetto = " & GbIdOggetto
    '  ELIMINA Componenti
    Parser.PsConnection.Execute "Delete * from PsCom_Obj where idoggettoc = " & GbIdOggetto
    '  ELIMINA DLI
    'Fare le if sul tipo!!!!
    MapsdM_Parser.resetRepository ("DBD")
    Parser.PsConnection.Execute "Delete * from PsDli_PSB where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsDli_PSBSEG where idoggetto = " & GbIdOggetto
    
    '*******************************************
    '  ELIMINA Relazioni
    '*******************************************
    Parser.PsConnection.Execute "Delete * from PsRel_DliSeg where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsRel_SegAree where stridoggetto = " & GbIdOggetto
    'SQ 27-04-06
    Parser.PsConnection.Execute "Delete * from MgRel_SegAree where stridoggetto = " & GbIdOggetto
      
    Select Case GbTipoInPars
      Case "JCL", "JOB", "PRC", "MBR"
        Parser.PsConnection.Execute "Delete * from PsRel_Obj where idoggettoc = " & GbIdOggetto
        'Parser.PsConnection.Execute "Delete * from PsRel_Obj where " _
        '                              & "NomeComponente = '" & nomeOggetto & "' and Utilizzo = 'PSB-JCL'"
        Parser.PsConnection.Execute "Delete * from PsRel_ObjUnk where idoggettoc = " & GbIdOggetto
        'Parser.PsConnection.Execute "Delete * from PsRel_ObjUnk where " _
        '                              & "NomeComponente = '" & nomeOggetto & "' and Utilizzo = 'PSB-JCL'"
      Case Else
        'PGM: Non braso la relazione da JCL
        Parser.PsConnection.Execute "Delete * from PsRel_Obj where " & _
                                    "idoggettoc = " & GbIdOggetto & " and Utilizzo <> 'PSB-JCL'"
        Parser.PsConnection.Execute "Delete * from PsRel_ObjUnk where " & _
                                    "idoggettoc = " & GbIdOggetto & " and Utilizzo <> 'PSB-JCL'"
    End Select
    
    wSqlExec = "Delete * from PsRel_ObjUnk where idoggettoc = " & GbIdOggetto
    Parser.PsConnection.Execute wSqlExec
    
    wSqlExec = "update PsRel_OUnkUnk " & _
               "Set IdoggettoR = 0 where " & _
               "idoggettor = " & GbIdOggetto
    Parser.PsConnection.Execute wSqlExec
    
    wSqlExec = "Delete * from Bs_Segnalazioni where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute wSqlExec
    
    Set tb = m_Fun.Open_Recordset("select * from PsRel_obj where idoggettor = " & wGbIdOggetto)
    If tb.RecordCount Then
      Set tb1 = m_Fun.Open_Recordset("Select * from PsRel_ObjUnk")
      While Not tb.EOF
        tb1.AddNew
        tb1!IdOggettoC = tb!IdOggettoC
        tb1!nomeOggettoR = wNomeOggetto
        tb1!relazione = tb!relazione
        tb1!utilizzo = tb!utilizzo
        tb1!nomecomponente = tb!nomecomponente
        tb1.Update
        GbIdOggetto = tb!IdOggettoC
        
        'SQ 2-03-06
        GbOldNumRec = 0 'init: non ho la riga!
        addSegnalazione wNomeOggetto, "NO" & tb!utilizzo, wNomeOggetto
         
        tb.MoveNext
      Wend
       
      wSqlExec = "Delete * from PsRel_Obj where " & _
                 "idoggettor = " & wGbIdOggetto
      Parser.PsConnection.Execute wSqlExec
    End If
    tb.Close
    
    GbIdOggetto = wGbIdOggetto
    CancellaOggetto = True
  End If
  
  Exit Function
errPsJCL:
  'OK (tmp) - tabella nuova...
  Resume Next
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''
' Mauro 25-05-2007
'''''''''''''''''''''''''''''''''''''''''''''''''
Function AggiornaRelOggetto(wIdOggetto As Long, wIdOggettoNew As Long) As Boolean
  Dim wSqlExec As String, wNomeOggetto As String
  Dim tb As Recordset, tb1 As Recordset
  Dim k As Integer
  Dim wGbIdOggetto As Variant
  Dim rsRelObj As Recordset
  
  GbIdOggetto = wIdOggetto
  wGbIdOggetto = GbIdOggetto
  
  ' Mauro 30/10/2007 : Cancello l'oggetto anche dalla BS_Oggetti
  Set tb = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  If tb.RecordCount Then
    wNomeOggetto = tb!nome
    GbTipoInPars = tb!Tipo
    tb.Delete
  End If
  tb.Close
  
  If Len(wNomeOggetto) Then
''    k = InStr(wNomeOggetto, ".")
''    If k > 0 Then wNomeOggetto = Mid$(wNomeOggetto, 1, k - 1)
    
    Parser.PsConnection.Execute "Delete * from PsJCL where idOggetto = " & GbIdOggetto
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Tools
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Parser.PsConnection.Execute "Delete * from Tl_AllignIstr where idOggetto = " & GbIdOggetto
    'Quell'altra?
    
    ' ELIMINA istruzioni
    'SQ COPY-ISTR
    If False Then
      Parser.PsConnection.Execute "Delete * from PsDli_Istruzioni where idPgm = " & GbIdOggetto
      'Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Dup where  idPgm = " & GbIdOggetto
      Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Liv where  idPgm = " & GbIdOggetto
      Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Key where  idPgm = " & GbIdOggetto
    End If
    'SQ - SERVE SOLO PER LE "COPY"...
    Parser.PsConnection.Execute "Delete * from PsDli_Istruzioni where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Dup where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Liv where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsDli_IstrDett_Key where idoggetto = " & GbIdOggetto
    
    '  ELIMINA DATI
    Parser.PsConnection.Execute "Delete * from PsData_Area where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsData_Cmp where idoggetto = " & GbIdOggetto
    '  ELIMINA Componenti
    Parser.PsConnection.Execute "Delete * from PsCom_Obj where idoggettoc = " & GbIdOggetto
    '  ELIMINA DLI
    'Fare le if sul tipo!!!!
    MapsdM_Parser.resetRepository ("DBD")
    Parser.PsConnection.Execute "Delete * from PsDli_PSB where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsDli_PSBSEG where idoggetto = " & GbIdOggetto
    
    '*******************************************
    '  ELIMINA Relazioni
    '*******************************************
    ' Mauro 25-05-2007 : Valutare se lasciare la cancellazione o mettere l'aggiornamento dell'IdOggetto come sotto
    Parser.PsConnection.Execute "Delete * from PsRel_DliSeg where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from PsRel_SegAree where stridoggetto = " & GbIdOggetto
    'SQ 27-04-06
    Parser.PsConnection.Execute "Delete * from MgRel_SegAree where stridoggetto = " & GbIdOggetto
      
    ' Mauro 31/10/2007: L'update va in errore se la relazione sul duplicato c'� gi�
''    Select Case GbTipoInPars
''      Case "JCL", "JOB", "PRC", "MBR"
''        'Parser.PsConnection.Execute "Delete * from PsRel_Obj where idoggettoc = " & GbIdOggetto
''        Parser.PsConnection.Execute "update psrel_obj " & _
''                                    "Set idoggettoc = " & wIdOggettoNew & _
''                                    "where idoggettoc = " & GbIdOggetto
''        'Parser.PsConnection.Execute "Delete * from PsRel_ObjUnk where idoggettoc = " & GbIdOggetto
''        Parser.PsConnection.Execute "update psrel_objunk " & _
''                                    "Set idoggettoc = " & wIdOggettoNew & _
''                                    "where idoggettoc = " & GbIdOggetto
''      Case Else
''        'PGM: Non braso la relazione da JCL
''        'Parser.PsConnection.Execute "Delete * from PsRel_Obj where " _
''        '                              & "idoggettoc = " & GbIdOggetto & " and Utilizzo <> 'PSB-JCL'"
''        Parser.PsConnection.Execute "update psrel_obj " & _
''                                    "Set idoggettoc = " & wIdOggettoNew & _
''                                    " where idoggettoc = " & GbIdOggetto & " and Utilizzo <> 'PSB-JCL'"
''        'Parser.PsConnection.Execute "Delete * from PsRel_ObjUnk where " _
''        '                              & "idoggettoc = " & GbIdOggetto & " and Utilizzo <> 'PSB-JCL'"
''        Parser.PsConnection.Execute "update psrel_objunk " & _
''                                    "Set idoggettoc = " & wIdOggettoNew & _
''                                    " where idoggettoc = " & GbIdOggetto & " and Utilizzo <> 'PSB-JCL'"
''    End Select
    Dim rs As Recordset, rsNew As Recordset
    
    Select Case GbTipoInPars
      Case "JCL", "JOB", "PRC", "MBR"
        Set rs = m_Fun.Open_Recordset("select * from PSRel_Obj where idoggettoc = " & GbIdOggetto)
        Do Until rs.EOF
          Set rsNew = m_Fun.Open_Recordset("select * from PSRel_Obj where " & _
                                           "idoggettoc = " & wIdOggettoNew & " and idoggettoR = " & rs!idOggettoR & " and " & _
                                           "Relazione = '" & rs!relazione & "' and nomecomponente = '" & rs!nomecomponente & "'")
          If rsNew.RecordCount Then
            rs.Delete
          Else
            rs!IdOggettoC = wIdOggettoNew
            rs.Update
          End If
          rsNew.Close
          rs.MoveNext
        Loop
        rs.Close
        
        Set rs = m_Fun.Open_Recordset("select * from PSRel_ObjUnk where idoggettoc = " & GbIdOggetto)
        Do Until rs.EOF
          Set rsNew = m_Fun.Open_Recordset("select * from PSRel_ObjUnk where " & _
                                           "idoggettoc = " & wIdOggettoNew & " and nomeoggettoR = '" & rs!nomeOggettoR & "' and " & _
                                           "Relazione = '" & rs!relazione & "' and nomecomponente = '" & rs!nomecomponente & "'")
          If rsNew.RecordCount Then
            rs.Delete
          Else
            rs!IdOggettoC = wIdOggettoNew
            rs.Update
          End If
          rsNew.Close
          rs.MoveNext
        Loop
        rs.Close
                                    
      Case Else
        'PGM: Non braso la relazione da JCL
        Set rs = m_Fun.Open_Recordset("select * from PSRel_Obj where idoggettoc = " & GbIdOggetto & " and Utilizzo <> 'PSB-JCL'")
        Do Until rs.EOF
          Set rsNew = m_Fun.Open_Recordset("select * from PSRel_Obj where " & _
                                           "idoggettoc = " & wIdOggettoNew & " and idoggettoR = " & rs!idOggettoR & " and " & _
                                           "Relazione = '" & rs!relazione & "' and nomecomponente = '" & rs!nomecomponente & "' and " & _
                                           "Utilizzo <> 'PSB-JCL'")
          If rsNew.RecordCount Then
            rs.Delete
          Else
            rs!IdOggettoC = wIdOggettoNew
            rs.Update
          End If
          rsNew.Close
          rs.MoveNext
        Loop
        rs.Close
                                    
        Set rs = m_Fun.Open_Recordset("select * from PSRel_ObjUnk where idoggettoc = " & GbIdOggetto & " and Utilizzo <> 'PSB-JCL'")
        Do Until rs.EOF
          Set rsNew = m_Fun.Open_Recordset("select * from PSRel_ObjUnk where " & _
                         "idoggettoc = " & wIdOggettoNew & " and nomeoggettoR = '" & rs!nomeOggettoR & "' and " & _
                         "Relazione = '" & rs!relazione & "' and nomecomponente = '" & rs!nomecomponente & "' and Utilizzo <> 'PSB-JCL'")
          If rsNew.RecordCount Then
            rs.Delete
          Else
            rs!IdOggettoC = wIdOggettoNew
            rs.Update
          End If
          rsNew.Close
          rs.MoveNext
        Loop
        rs.Close
    End Select
    
    wSqlExec = "update PsRel_OUnkUnk " & _
               " Set IdoggettoR = " & wIdOggettoNew & _
               " Where idoggettor = " & GbIdOggetto
    Parser.PsConnection.Execute wSqlExec
    
    wSqlExec = "Delete * from Bs_Segnalazioni where idoggetto = " & GbIdOggetto
    Parser.PsConnection.Execute wSqlExec
    
    wSqlExec = "update psrel_obj " & _
               " set idoggettor = " & wIdOggettoNew & _
               " where idoggettor = " & GbIdOggetto
    Parser.PsConnection.Execute wSqlExec
    
  '''  Set tb = m_Fun.Open_Recordset("select * from PsRel_obj where idoggettor = " & wGbIdOggetto)
  '''  If tb.RecordCount Then
  '''    Set tb1 = m_Fun.Open_Recordset("Select * from PsRel_ObjUnk")
  '''    While Not tb.EOF
  '''      tb1.AddNew
  '''      tb1!idoggettoc = tb!idoggettoc
  '''      tb1!nomeOggettoR = wNomeOggetto
  '''      tb1!relazione = tb!relazione
  '''      tb1!utilizzo = tb!utilizzo
  '''      tb1!NomeComponente = tb!NomeComponente
  '''      tb1.Update
  '''      GbIdOggetto = tb!idoggettoc
  '''
  '''      'SQ 2-03-06
  '''      GbOldNumRec = 0 'init: non ho la riga!
  '''      addSegnalazione wNomeOggetto, "NO" & tb!utilizzo, wNomeOggetto
  '''
  '''      tb.MoveNext
  '''    Wend
  '''
  '''    wSqlExec = "Delete * from PsRel_Obj where " _
  '''                        & "idoggettor = " & wGbIdOggetto
  '''    Parser.PsConnection.Execute wSqlExec
  '''  End If
  '''  tb.Close
    
    GbIdOggetto = wGbIdOggetto
    AggiornaRelOggetto = True
  End If
  
  Exit Function
errPsJCL:
  'OK (tmp) - tabella nuova...
  Resume Next
End Function

Function AnalizzaOggetto(wIdOggetto As Long) As Boolean
  Dim k As Long
  Dim dtNow As Date
  Dim DBDType As String
  Dim Tipo As String, nomePgm As String
  Dim rsCics As Recordset, rsLoad As Recordset
  
  DoEvents
 
  GbIdOggetto = wIdOggetto
  
  '23-01-06: portato in INIT_PARSING
  'ApriProgetto
  'wPcbStructure = load_PcbStructure_Template()
  'ActParsNoPsb = GbParmRel.DliStdPsb
  
  MapsdF_Parser.rText.Text = "" 'CHIARIRE 'STO ROBO!!!!!!!!!!!!!!!!!!
    
  Set TbOggetti = m_Fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & GbIdOggetto)
  GbNomePgm = TbOggetti!nome  'CAMBIARE NOME ALLA VARIABILE!
  GbTipoInPars = TbOggetti!Tipo
   
  If ActParsRela Then RelInitParser TbOggetti!nome
  If ActParsDati Then RelInitParserDati
  If ActParsDli Then DliInitParser
  
  Parser.AggErrLevel GbIdOggetto 'CHIARIRE...
  
  GbSegnObjRel = 0
  
  If Not IsNull(TbOggetti!Cics) Then SwTpCX = TbOggetti!Cics
  If Not IsNull(TbOggetti!Ims) Then SwTpIMS = TbOggetti!Ims
  If Not IsNull(TbOggetti!DLI) Then SwDli = TbOggetti!DLI
  If Not IsNull(TbOggetti!WithSQL) Then SwSql = TbOggetti!WithSQL
  If Not IsNull(TbOggetti!Batch) Then SwBatch = TbOggetti!Batch
  If Not IsNull(TbOggetti!VSam) Then SwVsam = TbOggetti!VSam
  If Not IsNull(TbOggetti!mapping) Then SwMapping = TbOggetti!mapping
  If Trim(TbOggetti!estensione) = "" Then
    GbFileInput = m_Fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!nome
  Else
    GbFileInput = m_Fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!nome & "." & TbOggetti!estensione
  End If
  
  'SQ: cos'e' questo? ci mette una vita!
  'SetMsgFinp "Analysis of " & GbFileInput & " Started"
      
  GbNumRec = 0
  Select Case GbTipoInPars
    Case "ASM", "MAC"
      If ActParsRela Then
        Asm2cobol = False
        Asm2seqfiles = False
        parseASM
      ElseIf ActParsDli Then
        'TMP: Migrazione: utilizzo il menu del parseII!
        'parseASM
      End If
    Case "BMS"
      If ActParsRela Then ParserBMS
    Case "MFS"
      If ActParsRela Then parseMFS
    'SQ 21-06-07
    'COBOL/XE
    Case "CBL", "CPY", "XEP", "XEC"
      If ActParsDati Then
        AggiornaDati  'I livello
      End If
      '''''''''''''''''''''''''''''''
      'SQ - Per NUOVO PARSER:
      ' 1) creare tabella PsCall:
      '    IdOggetto|Riga|Programma (testo30)|Parametri (Memo)|linee (numerico)
      ' 2) mettere false sotto
      '''''''''''''''''''''''''''''''
      If ActParsRela Then
        DeleteBatch
        checkPsbIMS
        MapsdM_COBOL.parsePgm_I                      'I livello
      End If
      '''''''''''''''''''''''''''''''
      'SQ - Per NUOVO PARSER (II liv.):
      ' 2) mettere false sotto
      '''''''''''''''''''''''''''''''
      'SQ CPY-ISTR
      If ActParsDli And GbTipoInPars <> "CPY" Then
        Parser.PsConnection.Execute "delete * from PsDLI_IstrPSB where idpgm = " & GbIdOggetto
        Parser.PsConnection.Execute "delete * from PsDLI_IstrDBD where idpgm = " & GbIdOggetto
        'Pulizia PsDli_Istruzioni?
        parseCbl_II 'II livello
        ' parser II VSM
        If ActParsVSAM Then
          MapsdM_VSM.clearVSMtable (GbIdOggetto)
          MapsdM_VSM.parseVSM_II GbIdOggetto, SwBatch
        End If
      End If
    Case "PLI", "INC"
      If ActParsRela Then
        MapsdM_PLI.parsePgm_I             'I livello
      End If
      'SQ CPY-ISTR
      If ActParsDli And GbTipoInPars <> "INC" Then
        Parser.PsConnection.Execute "delete * from PsDLI_IstrPSB where idpgm = " & GbIdOggetto
        Parser.PsConnection.Execute "delete * from PsDLI_IstrDBD where idpgm = " & GbIdOggetto
        parseCbl_II                       'II livello
      End If
    Case "NAT", "NIN"
      If ActParsRela Then
        MapsdM_NATURAL.parsePgm_I         'I livello
      End If
    Case "DBD"
      If ActParsRela Then
        parseDBD
      End If
    Case "PSB"
      If ActParsRela Then parsePSB
    Case "JCL", "PRC", "MBR"
      'SQ 19-10-06: Gestione "nested parsing" (MEMBER)
      IdOggettoRel = GbIdOggetto  'fa da "flag"!!!
      If ActParsRela Then parseJCL
    Case "JMB"
      If ActParsRela Then ParserJMB
    Case "JOB"
      If ActParsRela Then parseJOB
    Case "DDL"
      If ActParsRela Then ParserDDL
    'T 11102010
    Case "STP"
      If ActParsRela Then ParserStoreProcedure
    Case "LOA"
      If ActParsRela Then parseLoad
    Case "QIM"  'QuickIMS
      If ActParsRela Then
        'CheckPsbIMS
        MapsdM_QUICKIMS.parsePgm_I
      End If
      If ActParsDli Then
        'SQ CPY-ISTR
        Parser.PsConnection.Execute "delete * from PsDLI_IstrPSB where idpgm = " & GbIdOggetto
        Parser.PsConnection.Execute "delete * from PsDLI_IstrDBD where idpgm = " & GbIdOggetto
        MapsdM_QUICKIMS.parsePgm_II
      End If
    Case "EZT", "EZM" 'EasyTrieve
      Ezt2cobol = False
      If ActParsRela Then
        Ezt2cobol = False
        MapsdM_EASYTRIEVE.parsePgm_I
      End If
      If ActParsDli Then
        'SQ CPY-ISTR
        Parser.PsConnection.Execute "delete * from PsDLI_IstrPSB where idpgm = " & GbIdOggetto
        Parser.PsConnection.Execute "delete * from PsDLI_IstrDBD where idpgm = " & GbIdOggetto
        MapsdM_EASYTRIEVE.parsePgm_II
      End If
    Case "CLI"    'CLIST
      If ActParsRela Then
        'CheckPsbIMS
        MapsdM_TSO.parseCLIST
      End If
    Case "MNT"
      MapsdM_MANTIS.parsePgm_I
      Exit Function
  End Select
  
  dtNow = Now
  
  'SQ - provvisorio:
  If GbErrLevel <> "P-0" Then
    Parser.AggErrLevel GbIdOggetto
  End If
  
  'ATTENZIONE: LO FA ANCHE DENTRO I SINGOLI PARSING? (VEDI JCL...)
  Set TbOggetti = m_Fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & GbIdOggetto & " ")
  ' Mauro 20/01/2009 : Controllo sulle tabelle CICS_PPT e CICS_PCT
  If ActParsRela And Not TbOggetti!Cics Then
    ' Da implementare
    If TbOggetti!Tipo = "CBL" Then
      Tipo = "COBOL"
    ElseIf TbOggetti!Tipo = "ASM" Then
      Tipo = "ASSEMBLER"
    ElseIf TbOggetti!Tipo = "PLI" Then
      Tipo = "PLI"
    End If
    Set rsLoad = m_Fun.Open_Recordset("select load from psload where source = '" & TbOggetti!nome & "'")
    If rsLoad.RecordCount Then
      nomePgm = rsLoad!load
    Else
      nomePgm = TbOggetti!nome
    End If
    rsLoad.Close
    Set rsCics = m_Fun.Open_Recordset("select * from CICS_PPT where " & _
                                      "program = '" & nomePgm & "' and [language] = '" & Tipo & "'")
    If rsCics.RecordCount Then
      TbOggetti!Cics = True
      TbOggetti!Batch = False
    Else
      rsCics.Close
      Set rsCics = m_Fun.Open_Recordset("select * from CICS_PCT where " & _
                                        "program = '" & nomePgm & "'")
      If rsCics.RecordCount Then
        TbOggetti!Cics = True
        TbOggetti!Batch = False
      End If
    End If
    rsCics.Close
  End If
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti!DtParsing = dtNow
  TbOggetti!parsinglevel = IIf(ActParsDli, 2, 1)
  TbOggetti.Update
  TbOggetti.Close
  
  'SQ... un casino di tempo!
  'SetMsgFinp "Analysis of " & GbFileInput & " ended"
  
  For k = 1 To Parser.PsObjList.ListItems.count
    If Val(Parser.PsObjList.ListItems(k).Text) = wIdOggetto Then
      Parser.PsObjList.ListItems(k).SubItems(PARS_DATE_OBJ) = FormattaData(Str(dtNow))
      Parser.PsObjList.ListItems(k).SubItems(RC_OBJ) = GbErrLevel
      If Trim(GbErrLevel) <> "" Then
        Parser.PsObjList.ListItems(k).SmallIcon = 5
      Else
        Parser.PsObjList.ListItems(k).SmallIcon = 6
      End If
       
      If Parser.PsObjList.ListItems(k).Selected Then
        Parser.PsObjList.ListItems(k).Selected = True
        'Parser.PsObjList.Parent.gridlist_ItemClick Parser.PsObjList.SelectedItem
        Parser.PsObjList.Parent.gridlist_Click
      End If
      Exit For
    End If
  Next k
End Function
'''''''''''''''''''''''''''''
' Copia il file!!!
'''''''''''''''''''''''''''''
Sub Prj_VerObj()
  Dim NewDir As String
  Dim inFile As String
  Dim outFile As String
  Dim k As Integer
  Dim wStr1 As String
  
  Set TbOggetti = m_Fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & GbIdOggetto)
  If TbOggetti.RecordCount = 0 Then
     MsgBox "Adding Object not found...", vbInformation, Parser.PsNomeProdotto
  End If
  NewDir = TbOggetti!Directory_Input
  
'  wStr1 = Parser.PsNomeDB
'  K = InStr(wStr1, ".")
'  If K > 0 Then wStr1 = Mid$(wStr1, 1, K - 1)
'  NewDir = Replace(NewDir, "$", "$\" & wStr1)
  
  If NewDir = "$\" Then NewDir = "$"  'lo slash serve per i casi di sottodirectory...
  NewDir = Replace(NewDir, "$", m_Fun.FnPathPrj)
  
  wStr1 = ""
  For k = 1 To Len(NewDir)
    If Mid$(NewDir, k, 1) = "\" Then
      If Mid$(wStr1, 1, 2) <> "\\" Then
         If Dir(wStr1, vbDirectory) = "" Then
           MkDir (wStr1)
         End If
      End If
    End If
    wStr1 = wStr1 & Mid$(NewDir, k, 1)
  Next k
  If Dir(NewDir, vbDirectory) = "" Then
    MkDir NewDir
  End If
  
  inFile = TbOggetti!Directory_Input
  inFile = Replace(inFile, "$", m_Fun.FnPathDef)
  inFile = inFile & "\" & TbOggetti!nome
  If TbOggetti!estensione <> "" Then
    inFile = inFile & "." & TbOggetti!estensione
  End If
  
  outFile = NewDir & "\" & TbOggetti!nome
  If TbOggetti!estensione <> "" Then
    outFile = outFile & "." & TbOggetti!estensione
  End If
  
  'SQ NO se sbaglia la copia ce ne freghiamo???
  'On Error Resume Next
  'Gestito dal chiamante...
  If Len(Dir(outFile)) Then
    Kill outFile
  End If
  FileCopy inFile, outFile
  
  NewDir = Replace(NewDir, Parser.PsPathDef, "$")
  TbOggetti!Directory_Input = NewDir
  TbOggetti!Livello2 = NewDir
  TbOggetti.Update
  Set TbOggetti = m_Fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & GbIdOggetto)
End Sub

Sub restoreObject()
  Dim NewDir As String, inFile As String, outFile As String, wStr1 As String
  Dim k As Integer
  
  Set TbOggetti = m_Fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & GbIdOggetto)
  If TbOggetti.RecordCount = 0 Then
    MsgBox "Adding Object not found...", vbInformation, Parser.PsNomeProdotto
  End If
  NewDir = TbOggetti!Directory_Input
  
  wStr1 = Parser.PsNomeDB
  k = InStr(wStr1, ".")
  If k > 0 Then wStr1 = Left(wStr1, k - 1)
  
  'SQ 6-05-06
  'NewDir = Replace(NewDir, wStr1 & "\", "")
  'NewDir = Replace(Replace(NewDir, wStr1, ""), "\\", "\")
  NewDir = Replace(Replace(NewDir, "$\" & wStr1, "$\"), "\\", "\")
  TbOggetti!Directory_Input = NewDir
  TbOggetti!Livello2 = NewDir
  TbOggetti.Update
End Sub

Function changeTypeObj(wIdOggetto As Long, itemType As String, area As String, livello1 As String) As Boolean
  GbIdOggetto = wIdOggetto
  GbNumRec = 0
  GbErrLevel = ""
  GbTipoInPars = itemType

  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  'SQ 26-11-06
  TbOggetti!Area_Appartenenza = area
  TbOggetti!livello1 = livello1
  TbOggetti!Tipo = itemType
  TbOggetti!parsinglevel = 0
  TbOggetti!DtParsing = Now
  TbOggetti.Update
  
  RelInitParser TbOggetti!nome
  TbOggetti.Close
  Set TbOggetti = Nothing
End Function

Sub getLevel_Area(typeInPars As String, ByRef parea As String, ByRef plevel As String)
  Dim i As Integer
  
  ObjTypes = m_Fun.objtype
  For i = 0 To UBound(ObjTypes)
    If ObjTypes(i).oType = typeInPars Then Exit For
  Next i
  If i <= UBound(ObjTypes) Then
    parea = ObjTypes(i).oArea
    plevel = ObjTypes(i).oLevel
  Else
    parea = "UNKNOWN"
    plevel = "UNKNOWN"
  End If
End Sub

Function AggiungiOggetto(wIdOggetto As Long, Optional withTrace As Boolean = False) As Boolean
  Dim DBDType As String
  Dim rs As Recordset
  Dim i As Integer
  Dim area As String, level As String
  On Error GoTo errFile
  
  GbIdOggetto = wIdOggetto  'init
  GbNumRec = 0              'init
  GbErrLevel = ""           'init
  
  Prj_VerObj
  
  If Trim(TbOggetti!estensione) <> "" Then
    GbFileInput = m_Fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!nome & "." & TbOggetti!estensione
  Else
    GbFileInput = m_Fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!nome
  End If
  
  Parser.PsFinestra.ListItems.Clear
  
  SetMsgFin "Importing " & GbFileInput & "..."
  
  ''''''''''''''''''''''''''''''''''''''''''
  ' RICONOSCIMENTO
  ''''''''''''''''''''''''''''''''''''''''''
  'SQ 30-11-2005
  ' Mauro 11/02/2008: imposto tipo in base all'estensione
  If Parser.PsTipoEstensione = "Yes" Then
    GbTipoInPars = getObjTypeByExt(GbFileInput)
  Else
    GbTipoInPars = getObjectType(GbFileInput, withTrace)
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''
  ' RICLASSIFICAZIONE
  ' In base a "Relazioni con segnalazione"...
  ''''''''''''''''''''''''''''''''''''''''''
  'ROBERTO...
  If GbTipoInPars = "UNK" Then
    Dim rsSegnalazioni As Recordset
    Set rsSegnalazioni = m_Fun.Open_Recordset("select * from Bs_segnalazioni where Codice = 'P07' and Var1 = '" & TbOggetti!nome & "'")
    If rsSegnalazioni.RecordCount Then
      GbTipoInPars = "PRM"
      Parser.PsConnection.Execute "Delete * from Bs_segnalazioni where " & _
                                  "Codice = 'P07' and Var1 = '" & TbOggetti!nome & "'"
    End If
    rsSegnalazioni.Close
  End If
  'SQ: 31-01-06
  'SE serve, farlo prima... non qui
  'RelInitParser TbOggetti!Nome
  Parser.updateRelations TbOggetti!nome, GbTipoInPars
    
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  'SQ - Che spettacolo... non vedevo l'ora!
  getLevel_Area GbTipoInPars, area, level
  TbOggetti!Area_Appartenenza = area
  TbOggetti!livello1 = level
  '...
  '        TbOggetti!livello1 = "UNK"
  '  End Select
  
  TbOggetti!Tipo = GbTipoInPars
  TbOggetti!NumRighe = GbNumRec
  TbOggetti!parsinglevel = 0
  'Mauro 3-1-06
  TbOggetti!ErrLevel = ""
  
  '''''''''''''''''''''''''
  ' IMS: lettura catalogo
  'SQ: 28-09-06
  '''''''''''''''''''''''''
  If GbTipoInPars = "CBL" Or GbTipoInPars = "ASM" Or GbTipoInPars = "PLI" Or GbTipoInPars = "NAT" Then
    Set rs = m_Fun.Open_Recordset("select count(*) as card from IMS_Catalog where psb = '" & TbOggetti!nome & "' and pgmType='TP'")
    If rs!card = 0 Then
      'Nome LOAD diverso da Nome SOURCE: aggiorniamo il recordset
      Set rs = m_Fun.Open_Recordset("select count(*) as card from IMS_Catalog as a,PsLoad as b where " & _
                                    "b.source = '" & TbOggetti!nome & "' and b.load=a.psb")
    End If
    TbOggetti!Ims = rs!card > 0
    rs.Close
  End If
  
  TbOggetti.Update
  
  SetMsgFin "Import of " & GbFileInput & " Ended"
  Exit Function
errFile:
  If err.Number = 76 Then
    MsgBox err.description, vbCritical
  Else
    'gestire
    MsgBox err.description, vbExclamation
  End If
  'Resume
End Function

Sub RelInitParserDati()
  Parser.PsConnection.Execute "DELETE * FROM Bs_Segnalazioni WHERE " & _
                              "tipologia = 'DATI' and idoggetto = " & GbIdOggetto
  Parser.AggErrLevel GbIdOggetto
End Sub

Sub RelInitParser(nomeOggetto As String)
  Dim rs As Recordset, rsRel As Recordset
  Dim wSqlExec As String, wNomeOgg As String, wRel As String, wtipo As String
  Dim k As Integer
  Dim codeerror As String, relation As String
  
  wNomeOgg = nomeOggetto
  k = InStr(wNomeOgg, ".")
  If k > 0 Then
    wNomeOgg = Left(wNomeOgg, k - 1)
  End If
  
  GbErrLevel = ""
    
  deleteRelations nomeOggetto
    
  wtipo = GbTipoInPars
  
  Set rs = m_Fun.Open_Recordset("select * from PsRel_ObjUnk where nomeoggettor = '" & wNomeOgg & "'")
  While Not rs.EOF
    Set rsRel = m_Fun.Open_Recordset_Sys("SELECT * FROM itemTypes WHERE " & _
                                         "TipoOggetto = '" & wtipo & "' and Utilizzo = '" & rs!utilizzo & "'")
    If Not rsRel.EOF Then
      Set TbRela = m_Fun.Open_Recordset("select * from PsRel_Obj where " & _
                                        "idoggettoc = " & rs!IdOggettoC & " and idoggettor = " & GbIdOggetto & " and " & _
                                        "relazione = '" & rs!relazione & "' and nomecomponente = '" & rs!nomecomponente & "' and " & _
                                        "utilizzo = '" & rs!utilizzo & "' ")
      If TbRela.RecordCount = 0 Then
        TbRela.AddNew
        TbRela!IdOggettoC = rs!IdOggettoC
        TbRela!idOggettoR = GbIdOggetto
        TbRela!relazione = rs!relazione
        TbRela!nomecomponente = rs!nomecomponente
        TbRela!utilizzo = rs!utilizzo
        TbRela.Update
      End If
      TbRela.Close
      
      Set TbRela = m_Fun.Open_Recordset_Sys("Select * from usesRelations where " & _
                                            "Utilizzo = '" & rs!utilizzo & "'")
      If Not TbRela.EOF Then
        codeerror = IIf(IsNull(TbRela!codiceErrore), "", "'" & TbRela!codiceErrore & "'")
        If Not IsNull(TbRela!CodiceErroreAlt) And Not TbRela!CodiceErroreAlt = "" Then
          codeerror = codeerror & ",'" & TbRela!CodiceErroreAlt & "'"
        End If
        If Len(codeerror) Then
          codeerror = "(" & codeerror & ")"
          Parser.PsConnection.Execute "DELETE * from Bs_Segnalazioni where " & _
                                      "codice in " & codeerror & " and Var1 = '" & rs!nomeOggettoR & "'"
          relation = TbRela!relazione
        End If
      End If
      TbRela.Close
    End If
    rsRel.Close
    rs.MoveNext
  Wend
  rs.Close
                        
  Parser.PsConnection.Execute "Delete * from Psrel_Objunk where nomeoggettor = '" & wNomeOgg & "' and relazione = '" & relation & "'"
  ' AC
  Parser.PsConnection.Execute "delete * from PsPgm where IdOggetto = " & GbIdOggetto
  
  Select Case wtipo
    Case "PSB"
      Set rs = m_Fun.Open_Recordset("select * from PsRel_OUnkUnk where NomeOggettoR = '" & wNomeOgg & "' and relazione = 'PSB'")
      While Not rs.EOF
        rs!idOggettoR = GbIdOggetto
        rs.Update
        rs.MoveNext
      Wend
      rs.Close
    Case "CBL"
      Set rs = m_Fun.Open_Recordset("select * from PsRel_OUnkUnk where NomeOggettoC = '" & wNomeOgg & "' and relazione = 'PSB'")
      While Not rs.EOF
        If rs!idOggettoR > 0 Then
          Set TbRela = m_Fun.Open_Recordset("select * from PsRel_Obj where idoggettoc = " & GbIdOggetto)
          TbRela.AddNew
          TbRela!IdOggettoC = GbIdOggetto
          TbRela!idOggettoR = rs!idOggettoR
          TbRela!relazione = rs!relazione
          TbRela!nomecomponente = rs!nomecomponente
          TbRela!utilizzo = rs!utilizzo
          TbRela.Update
        Else
          Set TbRela = m_Fun.Open_Recordset("select * from PsRel_ObjUnk where idoggettoc = " & GbIdOggetto)
          TbRela.AddNew
          TbRela!IdOggettoC = GbIdOggetto
          TbRela!nomeOggettoR = rs!nomeOggettoR
          TbRela!relazione = rs!relazione
          TbRela!nomecomponente = rs!nomecomponente
          TbRela!utilizzo = rs!utilizzo
          TbRela.Update
        End If
        rs.Delete
        rs.MoveNext
      Wend
      rs.Close
  End Select
End Sub

' Mauro 06/08/2007 : La chiamata a questa routine � stata asteriscata
''Sub Aggiorna_Archivi(wIdObj, wType, wnStep, wnFile, wRec)
''  Dim Tbf As Recordset, Tba As Recordset
''  Dim wDsnname As String, wNomeLink As String
''  Dim k As Integer
''
''  Dim wKeyStart As Long, wKeyLen As Long, wMinLen As Long, wMaxLen As Long
''  Dim wRRDS As Boolean, wKSDS As Boolean, wESDS As Boolean, wSEQ As Boolean, wGDG As Boolean
''  Dim wRecF As String
''  Dim wRecS As Long
''
''  'SQ - l'ho appena inserito... lo riprendo dalla tabella e controllo anche????????????????????
''  Set Tbf = m_Fun.Open_Recordset("Select * from PsRel_File where idoggetto =" & wIdObj & " and type = '" & wType & "' and nStep = " & wnStep & " and nfile = " & wnFile & " ")
''  'If Tbf.RecordCount = 0 Then
''  '   Tbf.Close
''  '   Exit Sub
''  'End If
''
''  wDsnname = Tbf!dsnName
''  wNomeLink = Tbf!link
''
''  If wDsnname = "*" Then Exit Sub
''  If wDsnname = "DUMMY" Then Exit Sub
''  If InStr(wDsnname, ".") = 0 Then Exit Sub
''  If Left(wDsnname, 1) = "&" Then Exit Sub  'perche' se sono in mezzo????????????????????
''
''  If InStr(wNomeLink, "SORTWK") > 0 Then Exit Sub
''  If UCase(wNomeLink) = "STEPLIB" Then Exit Sub
''  If UCase(wNomeLink) = "JOBLIB" Then Exit Sub
''  If UCase(wNomeLink) = "TASKLIB" Then Exit Sub
''  If UCase(wNomeLink) = "SYSDUMP" Then Exit Sub
''  If UCase(wNomeLink) = "SYSUDUMP" Then Exit Sub
''
''  k = InStr(wDsnname, "(")
''  If k > 0 Then
''    wDsnname = Mid$(wDsnname, 1, k - 1)
''    wGDG = True
''    wSEQ = True
''  End If
''
''  Set Tba = m_Fun.Open_Recordset("select * from DMVSM_Archivio where Nome = '" & wDsnname & "' ")
''  If Tba.RecordCount = 0 Then
''    Tba.AddNew
''    Tba!data_creazione = Now
''    Tba!idorigine = wIdObj
''    Tba!Tag = "Generated by " & wType
''    wRRDS = False
''    wESDS = False
''    wKSDS = False
''    wRecF = "U"
''    wRecS = 0
''    wKeyStart = 0
''    wKeyLen = 0
''    wMinLen = 0
''    wMaxLen = 0
''  Else
''    wRRDS = Tba!RRDS
''    wESDS = Tba!ESDS
''    wKSDS = Tba!KSDS
''    wSEQ = Tba!Seq
''    wGDG = Tba!GDG
''    wRecF = Tba!recform
''    wRecS = Tba!recsize
''    wKeyStart = Tba!KeyStart
''    wKeyLen = Tba!KeyLen
''    wMinLen = Tba!MinLen
''    wMaxLen = Tba!MaxLen
''  End If
''
''  Tba!Nome = wDsnname
''  Tba!data_modifica = Now
''  Tba!ESDS = wESDS
''  Tba!KSDS = wKSDS
''  Tba!RRDS = wRRDS
''  Tba!Seq = wSEQ
''  Tba!GDG = wGDG
''  Tba!recform = wRecF
''  Tba!recsize = wRecS
''  Tba!MinLen = wMinLen
''  Tba!MaxLen = wMaxLen
''  Tba!KeyStart = wKeyStart
''  Tba!KeyLen = wKeyLen
''  Tba.Update
''
''  Tbf.Close
''  Tba.Close
''End Sub

Sub Aggiorna_RelFile(w_IdOggetto, w_Type, w_nStep, w_Step, w_Program, w_RunProgram)
  Dim wSql As String
  '''''MsgBox "tmp... fare!"
  wSql = "Update psrel_file set program = '" & w_RunProgram & "' where " & _
         "idoggetto = " & w_IdOggetto & " and type = '" & w_Type & "' and nstep = " & w_nStep & " and " & _
         "step = '" & w_Step & "' and program = '" & w_Program & "'"
  Parser.PsConnection.Execute wSql
End Sub
'Restituisce il "modello" che descrive la linea di input...
'sostiuisce le virgole con Space
'sostiuisce Gli apici con Space
'sostiuisce l'uguale con Space
Public Function getTemplate_Of_JCLline(wRecord As String) As String
  Dim wApp As String
  Dim s() As String, sApp() As String
  Dim i As Long
  Dim wFlagExec As Boolean
  
  wApp = Replace(wRecord, ",", " ")
  wApp = Replace(wApp, "'", " ")
  wApp = Replace(wApp, "=", " ")
  
  s = Split(wApp, " ")
  ReDim sApp(0)
  For i = 0 To UBound(s)
    If Trim(s(i)) <> "" Then
      If Mid(Trim(s(i)), 1, 2) <> "//" Then
        sApp(UBound(sApp)) = s(i)
        ReDim Preserve sApp(UBound(sApp) + 1)
      End If
    End If
  Next i
  
  If UBound(sApp) = 0 Then Exit Function
  
  ReDim Preserve sApp(UBound(sApp) - 1)
  wApp = ""
  For i = 0 To UBound(sApp)
    Select Case UCase(Trim(sApp(i)))
      Case "EXEC"
        wFlagExec = True
        wApp = wApp & "|" & UCase(Trim(sApp(i)))
      Case "PGM"
        If wFlagExec Then
          wApp = wApp & "|PRC"
        Else
          wApp = wApp & "|PGM"
        End If
        wFlagExec = False
      Case "PARM", "PSB", "MBR", "PAM", "CORE"
        wApp = wApp & "|" & UCase(Trim(sApp(i)))
        wFlagExec = False
      'Case else... (Nome PRG, per es...)
    End Select
  Next i
  
  getTemplate_Of_JCLline = Mid(wApp, 2) & "|"
End Function

Sub AggiungiObjUnkUnk(wNomeOc, WnomeOr, wRel, wNomeComp)
  Dim rs As Recordset
'SQ - 29-11-05
On Error GoTo errTmp

  Set rs = m_Fun.Open_Recordset("select * from PsRel_oUnkUnk where NomeOggettoC = '" & wNomeOc & "' and NomeOggettoR = '" & WnomeOr & "' and relazione = '" & wRel & "'")
  If rs.RecordCount = 0 Then
    Select Case wRel
     Case "PSB"
      rs.AddNew
      rs!NomeOggettoC = wNomeOc
      rs!nomeOggettoR = WnomeOr
      'SQ !!!!!!!!!!!   allungato il campo a 50...
      rs!nomecomponente = wNomeComp
      rs!idOggettoR = 0
      rs!relazione = wRel
      rs!utilizzo = "SCHEDULE"
      rs.Update
     Case "MFS"
      rs.AddNew
      rs!NomeOggettoC = wNomeOc
      rs!nomeOggettoR = WnomeOr
      rs!nomecomponente = wNomeComp
      rs!idOggettoR = 0
      rs!relazione = wRel
      rs!utilizzo = "MFS"
      rs.Update
   End Select
  End If
  rs.Close
Exit Sub
errTmp:
'SQ - gestire...
  'MsgBox Err.Number
End Sub

Sub ParserBMS()
   Dim nFBms As Variant
   Dim wRecIn As String, wRec As String, wNomeMapSet As String, wNomeMap As String
   Dim wStr As String, firstWord As String, MacroCmd As String, wLabel As String
   Dim k1 As Integer
       
   
   nFBms = FreeFile
   Open GbFileInput For Input As nFBms
   
   While Not EOF(nFBms)
      wRec = ""
      Line Input #nFBms, wRecIn
      GbNumRec = GbNumRec + 1
      If Not EOF(nFBms) Then
        While Mid$(wRecIn, 72, 1) <> " " And Not EOF(nFBms)
           wRec = wRec & Trim(Mid$(wRecIn, 1, 71))
           Line Input #nFBms, wRecIn
           GbNumRec = GbNumRec + 1
        Wend
        wRec = wRec & LTrim(Mid$(wRecIn, 1, 71))
      
        If Mid(wRec, 1, 1) <> "*" Then
           firstWord = wRec
           k1 = InStr(firstWord, " ")
           firstWord = Trim(Mid$(firstWord, 1, k1))
           
           If firstWord = "DFHMDF" Or firstWord = "DFHMSD" Or firstWord = "DFHMDI" Then
              wLabel = ""
              MacroCmd = firstWord
           Else
              wLabel = firstWord
              firstWord = wRec
              k1 = InStr(firstWord, " ")
              If k1 > 0 Then firstWord = Trim(Mid$(firstWord, k1))
              k1 = InStr(firstWord, " ")
              If k1 > 0 Then MacroCmd = Trim(Mid$(firstWord, 1, k1))
 '             MacroCmd = FirstWord
           End If
           Select Case MacroCmd
              Case "DFHMSD"
                 firstWord = TrovaParametro(wRec, "TYPE")
                 If firstWord = "MAP" Then
                   wNomeMapSet = wLabel
                   AggiungiComponente wNomeMapSet, wNomeMapSet, "MPS", "MAPSET"
                 End If
              Case "DFHMDI"
                 wNomeMap = wLabel
                 AggiungiComponente wNomeMapSet, wNomeMap, "MAP", "MAPPA"
              Case Else
           End Select
        End If
     End If
   Wend
   
   Close nFBms
   
   Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
   TbOggetti!DtParsing = Now
   TbOggetti!ErrLevel = GbErrLevel
   TbOggetti!parsinglevel = 1
   TbOggetti.Update
   
   'Scrive i campi della mappa nel database
   'SQ - CHE SIGNIFICA 'STA ROBA?! SCRIVE NELLA PsDLI_MFSCmp!!!!!
   'writeCampiBMS GbIdOggetto, GbFileInput
End Sub

Sub ParserDDL()
  Dim SwElab As Boolean
  Dim nFJcl As Variant
  Dim wRecIn As String, wRec As String
  Dim wStr As String, wStr1 As String, Wstr2 As String
  Dim k As Integer, k1 As Integer, k2 As Integer
  Dim wNomeTable As String, wNomeDb As String, wNomeTblSpc As String, wNomeCreator As String
  Dim wNomeCreatorIdx As String, wNomeIndex As String, wNomeAlias As String
  Dim wNomeAliasCreator As String, wNomeBP As String
  Dim wClose As Boolean, wCompress As Boolean
  Dim wIdDb As Long, wIdTblS As Long, wIdTable As Long, wIdIndex As Long, wIdCol As Long
  Dim wNomeStg As String, wNomeView As String

  Dim wNomeCol As String, wTipoCol As String, wLenCol As String, wDecCol As String
  Dim wNull As Boolean
  Dim wDefaultValue As String, isDefault As Boolean
  Dim wOrdinale As Long, wDescr As String

  Dim wTipoIndex As String, wUniqueIndex As Boolean
  Dim SwParentesi As Integer

  Dim TbDB As Recordset, TbTblS As Recordset, tbTable As Recordset, TbAlias As Recordset, TbView As Recordset
  Dim TbCol As Recordset, TbOggetti As Recordset, TbIndex As Recordset, TbIndexName As Recordset
  Dim TbIdxCol As Recordset
  Dim wTypeSql As String
  
  Dim wnoTipoCampo As Boolean
  Dim wIdTableSpc As Long, wIdTableSpcFor As Long
  
  Dim campoValore() As String, CampiLabel As String
  Dim i As Long
  Dim Campi() As String, check() As String
    
  Dim TbIndexNameCount As Long
  
  Dim keyCampi() As String
  Dim cont As Integer
  
  ' Mauro 18/03/2010 : Gestione PROCEDURE
  Dim wNomeProc As String, wNomeProcCreator As String, wNomePgm As String, wTipoPgm As String
  Dim isTemporary As Boolean
  Dim tmpWstr As String
  Dim wNomeLike As String, wNomeCreatorLike As String
  
  On Error GoTo catch
  
  nFJcl = FreeFile
  Open GbFileInput For Input As nFJcl
  If UCase(GbParmRel.DM_DefSqlType) = "ORACLE" Then
    wTypeSql = "DMORC_"
  Else
    wTypeSql = "DMDB2_"
  End If
  While Not EOF(nFJcl)
    'SQ - funzioncina...
    wRecIn = ""
    Line Input #nFJcl, wRecIn
    GbNumRec = GbNumRec + 1
    wRecIn = Trim(wRecIn)
    While Mid$(LTrim(wRecIn), 1, 2) = "--" And Not EOF(nFJcl)
      Line Input #nFJcl, wRecIn
      GbNumRec = GbNumRec + 1
    Wend
    wRec = Trim(wRecIn)
    GbOldNumRec = GbNumRec
    While InStr(wRec, ";") = 0 And Not EOF(nFJcl)
      Line Input #nFJcl, wRecIn
      GbNumRec = GbNumRec + 1
      If Left(wRecIn, 2) <> "--" Then
        wRec = wRec & " " & Trim(Left(wRecIn, 72))
      End If
    Wend
    wRec = Replace(wRec, ";", " ;")
    wRec = Replace(wRec, """", "")
    wRec = Replace(wRec, "(", " (")
    
    SwElab = False
    ' ***************************************************
    ' *  CREATE   (TABLE TABLESPACE DATABASE INDEX)
    ' ***************************************************
    isTemporary = False
    k = InStr(wRec, "CREATE")
    If k > 0 Then
      wNomeTable = ""
      wNomeDb = ""
      wNomeTblSpc = ""
      wNomeCreator = ""
      wStr = Trim(Mid(wRec, k + 6))
      
      ''''''''''''''
      'CREATE TABLE
      ''''''''''''''
      If Left(wStr, 7) = "GLOBAL " Then
        wStr = Trim(Mid(wStr, 7))
      End If
      If Left(wStr, 6) = "LARGE " Then
        wStr = Trim(Mid(wStr, 6))
      End If
      If Left(wStr, 10) = "TEMPORARY " Then
        isTemporary = True
        wStr = Trim(Mid(wStr, 10))
      End If
      
      If Left(wStr, 6) = "TABLE " Then
        SwElab = True
        'NOME TABELLA:
        wNomeTable = TrovaParamDDL(wStr, "TABLE ")
        k = InStr(wNomeTable, ".")
        If k > 0 Then
          'NOME CREATOR:
          wNomeCreator = Left(wNomeTable, k - 1)
          wNomeTable = Mid$(wNomeTable, k + 1)
        End If
        
        ' Mauro 09/02/2011 : Gestione LIKE per Cloni Tabella
        wNomeLike = ""
        wNomeCreatorLike = ""
        wNomeLike = TrovaParamDDL(wStr, " LIKE ")
        k = InStr(wNomeLike, ".")
        If k > 0 Then
          'NOME CREATOR:
          wNomeCreatorLike = Left(wNomeLike, k - 1)
          wNomeLike = Mid$(wNomeLike, k + 1)
        End If
        
        'NOME DB:
        wNomeDb = TrovaParamDDL(wStr, " IN ")
        'SQ 2-11-06 - Pezza, tanto fa schifo tutto!
        'Se ho:
        'PRIMARY KEY (NBR_COMP,
        '             ID_DLR),
        '  Constraint CD_ALIAS
        '             CHECK (CD_ALIAS IN (' ','D','N','P','T') ),...
        'becco la IN sbagliata!!!!
        If Left(wNomeDb, 1) = "(" Then
          k = InStrRev(wStr, " IN ")
          wNomeDb = Mid(wStr, k)
          wNomeDb = TrovaParamDDL(wNomeDb, " IN ")
        End If
        k = InStr(wNomeDb, ".")
        If k > 0 Then
          wNomeTblSpc = Mid$(wNomeDb, k + 1)
          wNomeDb = Mid$(wNomeDb, 1, k - 1)
        End If
           
        wNomeDb = Mid(Replace(wNomeDb, "'", ""), 1, 18) 'Tronca a 18
        If InStr(wNomeDb, "(") > 0 Or InStr(wNomeDb, ")") > 0 Or InStr(wNomeDb, ",") > 0 Then
          wNomeDb = "None"
        End If
        
        Set TbDB = m_Fun.Open_Recordset("select * from " & wTypeSql & "db where Nome = '" & wNomeDb & "'")
        If TbDB.RecordCount > 0 Then
          wIdDb = TbDB!IdDB
        Else
          'If UCase(GbParmRel.DM_DefSqlType) = "ORACLE" Or UCase(GbParmRel.DM_DefSqlType) = "ORACLE" Then
          If wNomeDb = "None" Then
            If wTypeSql = "DMORC_" Then
              wNomeDb = "DEFDBORA"
              wNomeTblSpc = "DEFTSORA"
            Else
              wNomeDb = "DEFDBDB2"
              wNomeTblSpc = "DEFTSORA"
            End If
          End If
          Set TbDB = m_Fun.Open_Recordset("select * from " & wTypeSql & "db where Nome = '" & wNomeDb & "'")
          If TbDB.RecordCount > 0 Then
            wIdDb = TbDB!IdDB
          Else
            TbDB.AddNew
            TbDB!nome = wNomeDb
            TbDB!data_creazione = Now
            TbDB!data_modifica = Now
            TbDB!descrizione = " "
            TbDB!iddbor = -1
            TbDB!Tag = "Default "
            TbDB!idObjSource = -1
            TbDB.Update
            TbDB.Close
            Set TbDB = m_Fun.Open_Recordset("select * from " & wTypeSql & "db where Nome = '" & wNomeDb & "'")
            wIdDb = TbDB!IdDB
          End If
          Set TbTblS = m_Fun.Open_Recordset("select * from " & wTypeSql & "tablespaces where Nome = '" & wNomeTblSpc & "'")
          If TbTblS.RecordCount = 0 Then
            Set TbTblS = m_Fun.Open_Recordset("select * from " & wTypeSql & "tablespaces order by idtablespc")
            If TbTblS.RecordCount = 0 Then
              wIdTblS = 1
            Else
              TbTblS.MoveLast
              wIdTblS = TbTblS!IdTableSpc + 1
            End If
            TbTblS.AddNew
            TbTblS!IdTableSpc = wIdTblS
            TbTblS!IdDB = wIdDb
            TbTblS!nome = wNomeTblSpc
            TbTblS!NomeDb = wNomeDb
            TbTblS!descrizione = " "
            TbTblS!data_creazione = Now
            TbTblS!data_modifica = Now
            TbTblS!Tag = "Default "
            TbTblS!idObjSource = -1
            TbTblS.Update
          End If
          'End If
        End If
        Set TbTblS = m_Fun.Open_Recordset("select * from " & wTypeSql & "tablespaces where Nome = '" & wNomeTblSpc & "'")
        If TbTblS.RecordCount > 0 Then
          wIdTblS = TbTblS!IdTableSpc
        Else
          wIdTblS = -1
          'Stop
        End If
            
        Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "tabelle where " & _
                                           "Nome = '" & wNomeTable & "' AND creator = '" & wNomeCreator & "'")
        If tbTable.RecordCount = 0 Then
          tbTable.AddNew
        End If
        tbTable!IdDB = wIdDb
        tbTable!NomeDb = wNomeDb
        tbTable!IdTableSpc = wIdTblS
        tbTable!nome = wNomeTable
        tbTable!creator = wNomeCreator
        tbTable!data_creazione = Now
        tbTable!data_modifica = Now
        tbTable!idtablejoin = -1
        tbTable!idorigine = -1
        tbTable!Tag = IIf(isTemporary, "Temporary Table DDL", "Da DDL")
        tbTable!idObjSource = GbIdOggetto
        tbTable.Update
        tbTable.Close
        
        'VC: prova per inserire pi� tabelle con creator diversi
        'Set TbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "tabelle where Nome = '" & wNomeTable & "'")
        Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "tabelle where " & _
                                           "Nome = '" & wNomeTable & "' AND creator = '" & wNomeCreator & "'")
        If tbTable.RecordCount Then
          wIdTable = tbTable!IdTable
        End If
        tbTable.Close
  
        'fine inserimento tabella
        Parser.PsConnection.Execute "Delete * FROM " & wTypeSql & "Colonne where idtable = " & wIdTable & ";"
        
        If wNomeLike <> "None" Then
          Set tbTable = m_Fun.Open_Recordset("select idtable from " & wTypeSql & "tabelle where nome = '" & wNomeLike & "' and creator = '" & wNomeCreatorLike & "'")
          ' Teoricamente, dovrebbe essercene una sola.... Prendo comunque solo la prima che ho trovato
          If tbTable.RecordCount Then
            ' Per ora, in caso di LIKE, copio solo le colonne e lascio inalterati tutti gli altri dettagli: index, alias, tablespace...
            copiaColonne wIdTable, tbTable!IdTable, wTypeSql
          Else
            addSegnalazione wNomeCreatorLike & "." & wNomeLike, "NOTBFND", Str(GbIdOggetto)
          End If
          tbTable.Close
          wRec = ""
        End If
        
        Set TbCol = m_Fun.Open_Recordset("select * from " & wTypeSql & "colonne where idtable = " & wIdTable)
        wStr = wRec
        k1 = InStr(wStr, "(")
        wStr = Mid$(wStr, k1 + 1)
        k1 = InStr(wStr, " IN ")
        If k1 > 0 Then wStr = Mid$(wStr, 1, k1 - 1)
        
        'CONSTRAINT:
        k1 = InStr(wStr, "CONSTRAINT ")
        If k1 > 0 Then
          wStr = Mid$(wStr, 1, k1 - 1)
          
          'Verificare CHECK
          k1 = InStr(wStr, " CHECK ")
          If k1 > 0 Then
            wStr = Mid$(wStr, 1, k1 - 1)
          End If
        Else
          'Potrebbe esserci solo PRIMARY KEY
          k1 = InStr(wStr, "PRIMARY KEY")
          If k1 > 0 Then
            wStr = Mid$(wStr, 1, k1 - 1)
          End If
        End If
        
        k1 = InStr(wStr, "PRIMARY KEY")
        If k1 > 0 Then
          wStr = Mid$(wStr, 1, k1 - 1)
        End If
        
        wLenCol = 0
        wOrdinale = 0
        wStr = Trim(wStr)
        While Len(wStr) > 0
          wOrdinale = wOrdinale + 1
          SwParentesi = 0
          k1 = 0
          For k = 1 To Len(wStr)
            If Mid$(wStr, k, 1) = ")" Then SwParentesi = SwParentesi + 1
            If Mid$(wStr, k, 1) = "(" Then SwParentesi = SwParentesi + 1
            If Mid$(wStr, k, 1) = "," Then
              If SwParentesi <> 1 Then
                k1 = k
                Exit For
              End If
            End If
          Next k
             
          If k1 = 0 Then
            Wstr2 = Mid$(wStr, 1, Len(wStr) - 1)
            wStr = ""
          Else
            Wstr2 = Mid$(wStr, 1, k1 - 1)
            wStr = LTrim(Mid$(wStr, k1 + 1))
          End If
          
          k1 = InStr(Wstr2, " ")
          If k1 = 0 Then
            wNomeCol = Wstr2
          Else
            wNomeCol = Mid$(Wstr2, 1, k1 - 1)
            Wstr2 = Trim(Mid$(Wstr2, k1 + 1))
          End If
               
          k1 = InStr(Wstr2, " ")
          k2 = InStr(Wstr2, "(")
          If k2 > 0 And k2 < k1 Then k1 = k2
          If k1 = 0 Then k1 = Len(Wstr2) + 1
          wTipoCol = Mid$(Wstr2, 1, k1 - 1)
          k1 = InStr(wTipoCol, "(")
          If k1 > 0 Then
            k1 = k2
            wTipoCol = Mid$(Wstr2, 1, k1 - 1)
          End If
          
          wDefaultValue = " "
          If InStr(Wstr2, "NOT ") > 0 Then
            wNull = False
            If InStr(Wstr2, "DEFAULT") > 0 Then
              wDefaultValue = Mid(Wstr2, InStr(Wstr2, "DEFAULT") + 7)
              If Not Trim(wDefaultValue) = ")" Then
                wDefaultValue = Trim(Replace(wDefaultValue, "'", ""))
              Else
                wDefaultValue = ""
              End If
              If InStr(wDefaultValue, "NOT ") > 0 Then
                wDefaultValue = Trim(Replace(Left(wDefaultValue, InStr(wDefaultValue, "NOT ") - 1), "'", ""))
              End If
              isDefault = True
            Else
              isDefault = False
            End If
          Else
            wNull = True
            If InStr(Wstr2, "DEFAULT") > 0 Then
              wDefaultValue = Trim(Replace(Mid(Wstr2, InStr(Wstr2, "DEFAULT") + 7), "'", ""))
            Else
              wDefaultValue = ""
              isDefault = False
            End If
          End If
          wDecCol = 0
          k1 = InStr(wTipoCol, ")")
          If k1 > 0 Then wTipoCol = Mid$(wTipoCol, 1, k1 - 1)
          If wTipoCol = "DEC" Then wTipoCol = "DECIMAL"
          If wTipoCol = "INT" Then wTipoCol = "INTEGER"
          If wTipoCol = "NUM" Then wTipoCol = "NUMBER"
          If wTipoCol = "CHARACTER" Then wTipoCol = "CHAR"
          If wTipoCol = "VARCHARACTER" Then wTipoCol = "VARCHAR"
          If wTipoCol = "VARCHARACTER2" Then wTipoCol = "VARCHAR2"
           
          wnoTipoCampo = False
          Select Case UCase(wTipoCol)
            Case "INTEGER"
              wLenCol = 4
            Case "SMALLINT"
              wLenCol = 2
            Case "DATE"
              wLenCol = 10
            Case "TIME"
              wLenCol = 8
            Case "TIMESTAMP"
              wLenCol = 26
            Case "REAL"
              wLenCol = 8
            Case "DOUBLE", "LONG"
              wLenCol = 8
            Case "FLOAT"
              wLenCol = 8
            Case "BLOB", "CLOB"
              wLenCol = -1
            Case "CHAR", "VARCHAR", "VARCHAR2"
              k1 = InStr(Wstr2, "(")
              Wstr2 = Mid$(Wstr2, k1 + 1)
              k1 = InStr(Wstr2, ")")
              If k1 > 0 Then wLenCol = Val(Mid$(Wstr2, 1, k1 - 1))
            Case "DECIMAL", "NUMBER", "NUMERIC"
              k1 = InStr(Wstr2, "(")
              Wstr2 = Mid$(Wstr2, k1 + 1)
              k1 = InStr(Wstr2, ")")
              Wstr2 = Mid$(Wstr2, 1, k1 - 1)
              k1 = InStr(Wstr2, ",")
              If k1 = 0 Then
                wLenCol = Val(Trim(Wstr2))
              Else
                wLenCol = Val(Mid$(Wstr2, 1, k1 - 1))
                wDecCol = Val(Mid$(Wstr2, k1 + 1))
              End If
            Case Else
              wnoTipoCampo = True
              addSegnalazione wTipoCol, "NOTYPECOL", wNomeCol
              m_Fun.writeLog "Type of Column(" & wTipoCol & ") not recognized:  <" & wRec & ">  IdObj: " & GbIdOggetto, "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
          End Select
          If Not wnoTipoCampo Then
            'scrive il Nome della colonna solo se il tipo di campo
            '� censito.
            TbCol.AddNew
            TbCol!idcolonna = (wIdTable * 1000) + wOrdinale
            TbCol!Ordinale = wOrdinale
            TbCol!IdTable = wIdTable
            TbCol!nome = wNomeCol
            TbCol!Tipo = wTipoCol
            TbCol!Lunghezza = wLenCol
            TbCol!Decimali = wDecCol
            TbCol!Null = wNull
            'TbCol!Default = wDefault
            'SQ 22-02-08
            'pezza: lo gestisco quando braso tutto...
            If InStr(wDefaultValue & "", "AS IDENTITY") = 0 Then
              TbCol!DefaultValue = wDefaultValue
            End If
            TbCol!isDefault = isDefault
            TbCol!idcmp = -1
            TbCol!idtablejoin = -1
            TbCol.Update
          End If
        Wend
        TbCol.Close
      End If
    End If
    If Mid$(wStr, 1, 9) = "DATABASE " Then
      SwElab = True
      wStr = Trim(Mid$(wStr, 9))
      k = InStr(wStr, " ")
      wNomeDb = Trim(Mid$(wStr, 1, k))
      k = InStr(wNomeDb, ".")
      If k > 0 Then
        wNomeDb = Mid$(wNomeDb, 1, k - 1)
      End If
      Set TbDB = m_Fun.Open_Recordset("select * from " & wTypeSql & "DB where Nome = '" & wNomeDb & "'")
      If TbDB.RecordCount = 0 Then
        TbDB.AddNew
      End If
      TbDB!nome = wNomeDb
      TbDB!data_creazione = Now
      TbDB!data_modifica = Now
      TbDB!descrizione = " "
      TbDB!iddbor = -1
      TbDB!Tag = "Da DDL "
      TbDB!idObjSource = GbIdOggetto
      TbDB.Update
      TbDB.Close
    End If
    If Mid$(wStr, 1, 11) = "TABLESPACE " Then
      SwElab = True
      wNomeTblSpc = TrovaParamDDL(wStr, "TABLESPACE ")
      k = InStr(wNomeTblSpc, ".")
      If k > 0 Then
        wNomeTblSpc = Mid$(wNomeTblSpc, 1, k - 1)
      End If
      wNomeDb = TrovaParamDDL(wStr, " IN ")
      k = InStr(wNomeDb, ".")
      If k > 0 Then
        wNomeDb = Mid$(wNomeDb, 1, k - 1)
      End If
      Set TbDB = m_Fun.Open_Recordset("select * from " & wTypeSql & "db where Nome = '" & wNomeDb & "'")
      If TbDB.RecordCount > 0 Then
        wIdDb = TbDB!IdDB
      Else
        'Stop
        ' Mauro 22/02/2008 : Se non c'� il DB, lo salvo al momento con il nuovo IdDB
        'm_Fun.WriteLog "ex stop... controllare"
        TbDB.AddNew
        TbDB!nome = wNomeDb
        TbDB!data_creazione = Now
        TbDB!data_modifica = Now
        TbDB!descrizione = " "
        TbDB!iddbor = -1
        TbDB!Tag = "Default "
        TbDB!idObjSource = -1
        TbDB.Update
        TbDB.Close
        Set TbDB = m_Fun.Open_Recordset("select * from " & wTypeSql & "db where Nome = '" & wNomeDb & "'")
        wIdDb = TbDB!IdDB
      End If
      TbDB.Close
      wNomeBP = TrovaParamDDL(wStr, " BUFFERPOOL ")
      wNomeStg = TrovaParamDDL(wStr, " STOGROUP ")
      If TrovaParamDDL(wStr, " CLOSE ") = "NO" Then
        wClose = False
      Else
        wClose = True
      End If
      If TrovaParamDDL(wStr, " COMPRESS ") = "NO" Then
        wCompress = False
      Else
        wCompress = True
      End If
      Set TbTblS = m_Fun.Open_Recordset("select * from " & wTypeSql & "tablespaces where Nome = '" & wNomeTblSpc & "'")
      If TbTblS.RecordCount = 0 Then
        Set TbTblS = m_Fun.Open_Recordset("select * from " & wTypeSql & "tablespaces order by idtablespc")
        If TbTblS.RecordCount = 0 Then
          wIdTblS = 1
        Else
          TbTblS.MoveLast
          wIdTblS = TbTblS!IdTableSpc + 1
        End If
        TbTblS.AddNew
      Else
        wIdTblS = TbTblS!IdTableSpc
      End If
       
      TbTblS!IdTableSpc = wIdTblS
      TbTblS!IdDB = wIdDb
      TbTblS!nome = wNomeTblSpc
      TbTblS!NomeDb = wNomeDb
      TbTblS!Close = wClose
      TbTblS!compress = wCompress
      TbTblS!storagegroup = wNomeStg
      TbTblS!BUFFERPOOL = wNomeBP
      TbTblS!data_creazione = Now
      TbTblS!data_modifica = Now
      TbTblS!Tag = "DA DDL"
      TbTblS!idObjSource = GbIdOggetto
      TbTblS.Update
      TbTblS.Close
    End If
    ' Mauro 13/02/2008: CREATE VIEW
    If Left(wStr, 5) = "VIEW " Then
      '' Tilvia 19-08-2009
      SwElab = True
      wNomeView = TrovaParamDDL(wStr, "VIEW ")
      k = InStr(wNomeView, ".")
      If k > 0 Then
        wNomeCreator = Left(wNomeView, k - 1)
        wNomeView = Mid$(wNomeView, k + 1)
      End If

      wNomeTable = TrovaParamDDL(wStr, "FROM ")
      k = InStr(wNomeTable, ".")
      If k > 0 Then
        'wNomeCreator = Left(wNomeTable, k - 1)
        wNomeTable = Mid$(wNomeTable, k + 1)
      End If

      Set TbView = m_Fun.Open_Recordset("select * from " & wTypeSql & "View where Nome = '" & wNomeView & "'")
      If TbView.RecordCount = 0 Then
        TbView.AddNew
      End If
      TbView!nome = wNomeView
      wNomeTable = Replace(Replace(wNomeTable, ")", ""), "(", "")
      If InStr(wNomeTable, ",") = 0 Then
        Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "tabelle where Nome = '" & wNomeTable & "'")
        If tbTable.RecordCount Then
          TbView!IdTable = tbTable!IdTable
          TbView!IdDB = tbTable!IdDB
        Else
          TbView!IdTable = 0
        End If
        tbTable.Close
      Else
        wNomeTable = Left(wNomeTable, InStr(wNomeTable, ",") - 1)
        TbView!IdTable = 0
      End If
      TbView.Update
      TbView.Close
     '' DoEvents
    End If
    ' Mauro 13/02/2008: CREATE ALIAS
    If Left(wStr, 6) = "ALIAS " Then
      SwElab = True
      wNomeAlias = TrovaParamDDL(wStr, "ALIAS ")
      k = InStr(wNomeAlias, ".")
      If k > 0 Then
        wNomeCreator = Left(wNomeAlias, k - 1)
        wNomeAlias = Mid$(wNomeAlias, k + 1)
      End If
      
      wNomeTable = TrovaParamDDL(wStr, "FOR ")
      k = InStr(wNomeTable, ".")
      If k > 0 Then
        'wNomeCreator = Left(wNomeTable, k - 1)
        wNomeTable = Mid$(wNomeTable, k + 1)
      End If

      Set TbAlias = m_Fun.Open_Recordset("select * from " & wTypeSql & "Alias where Nome = '" & wNomeAlias & "'")
      If TbAlias.RecordCount = 0 Then
        TbAlias.AddNew
      End If
      TbAlias!nome = wNomeAlias
      Set tbTable = m_Fun.Open_Recordset("select idtable from " & wTypeSql & "tabelle where Nome = '" & wNomeTable & "'")
      If tbTable.RecordCount Then
        TbAlias!IdTable = tbTable!IdTable
      Else
        TbAlias!IdTable = 0
      End If
      tbTable.Close
      TbAlias!creator = wNomeCreator
      TbAlias!data_creazione = Now
      TbAlias!data_modifica = Now
      TbAlias.Update
      TbAlias.Close
    End If
    
    '''''''''''''''''''''''''''''''
    '''''create trigger''''''''''''
    '''''''''''''''''''''''''''''''
    If Left(wStr, 8) = "TRIGGER " Then
      SwElab = True
      If insertTrigger(wStr, wTypeSql) = False Then
        'scrittura su file di log
      End If
    End If
    '''''''''''''''''''''''''''''''
    '''''fine trigger''''''''''''''
    '''''''''''''''''''''''''''''''
    k = InStr(" " & wStr, " SYNONYM ")
    If k > 0 Then
      SwElab = True
      wNomeAlias = TrovaParamDDL(wStr, "SYNONYM ")
      k = InStr(wNomeAlias, ".")
      If k > 0 Then
        wNomeAliasCreator = Mid$(wNomeTable, 1, k - 1)
        wNomeAlias = Mid$(wNomeTable, k + 1)
      Else
        wNomeAliasCreator = " "
      End If
      wNomeTable = TrovaParamDDL(wStr, " FOR ")
      k = InStr(wNomeTable, ".")
      If k > 0 Then
        wNomeCreator = Mid$(wNomeTable, 1, k - 1)
        wNomeTable = Mid$(wNomeTable, k + 1)
      End If
      Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "Tabelle where " & _
                                         "Nome = '" & wNomeTable & "' and creator = '" & wNomeCreator & "' ")
      If tbTable.RecordCount Then
        Set TbAlias = m_Fun.Open_Recordset("select * from " & wTypeSql & "Alias")
        TbAlias.AddNew
        TbAlias!IdTable = tbTable!IdTable
        TbAlias!Tipo = "S"
        TbAlias!nome = wNomeAlias
        TbAlias!creator = wNomeAliasCreator
        TbAlias!descrizione = " "
        TbAlias!data_creazione = Now
        TbAlias!data_modifica = Now
        TbAlias!location = " "
        TbAlias.Update
        TbAlias.Close
        tbTable.Close
      Else
        'MsgBox "attenzione: tabella " & wNomeTable & " non esistente... da gestire..."
        m_Fun.writeLog "attenzione: tabella " & wNomeTable & " non esistente... da gestire..."
      End If
    End If
    k = InStr(" " & wStr, " INDEX ")
    If k > 0 Then
      wStr1 = Mid$(wStr, k)
      If InStr(wStr1, " ON ") = 0 Then k = 0
    End If
    If k > 0 Then
      SwElab = True
      wNomeTable = TrovaParamDDL(wStr, " ON ")
      k = InStr(wNomeTable, ".")
      If k > 0 Then
        wNomeCreator = Mid$(wNomeTable, 1, k - 1)
        wNomeTable = Mid$(wNomeTable, k + 1)
      End If
      wNomeCreator = Replace(wNomeCreator, """", "")
      wNomeTable = Replace(wNomeTable, """", "")
      wNomeIndex = TrovaParamDDL(" " & wStr, " INDEX ")
      k = InStr(wNomeIndex, ".")
      If k > 0 Then
        wNomeCreatorIdx = Mid$(wNomeIndex, 1, k - 1)
        wNomeIndex = Mid$(wNomeIndex, k + 1)
      End If
      'wTipoIndex = TrovaParamDDL(wStr, "TYPE ")
      wTipoIndex = "I"
      wUniqueIndex = False
      wNomeCreatorIdx = Replace(wNomeCreatorIdx, """", "")
      wNomeIndex = Replace(wNomeIndex, """", "")
            
      If InStr(wStr, "UNIQUE") > 0 Then wUniqueIndex = True
      Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "Tabelle where " & _
                                         "Nome = '" & wNomeTable & "' and creator = '" & wNomeCreator & "' ")
      If tbTable.RecordCount = 0 Then
        addSegnalazione wNomeTable, "NOTBIDX", wNomeIndex
        m_Fun.writeLog "Table for index not recognized: <" & wRec & ">  IdObj: " & GbIdOggetto, "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
        'Stop
        wIdTable = -1
        wIdTableSpc = -1
      Else
        wIdTable = tbTable!IdTable
        wIdTableSpc = tbTable!IdTableSpc
      End If
      'Madrid: problema refresh!!!!
      '-> DA GESTIRE!
      Set TbIndexName = m_Fun.Open_Recordset("select Nome from " & wTypeSql & "index where Nome = '" & wNomeIndex & "'")
      If TbIndexName.RecordCount = 0 Then
        Set TbIndex = m_Fun.Open_Recordset("Select * from " & wTypeSql & "Index order by idindex")
        If TbIndex.RecordCount > 0 Then
          TbIndex.MoveLast
          wIdIndex = TbIndex!IdIndex + 1
        Else
          wIdIndex = 1
        End If
        TbIndex.AddNew
        TbIndex!IdIndex = wIdIndex
        TbIndex!IdTable = wIdTable
        TbIndex!IdTableSpc = wIdTableSpc
        TbIndex!nome = wNomeIndex
        TbIndex!Tipo = wTipoIndex
        TbIndex!Unique = wUniqueIndex
        TbIndex!used = True
        TbIndex!data_creazione = Now
        TbIndex!data_modifica = Now
        TbIndex!idorigine = -1
        TbIndex!descrizione = " "
        TbIndex.Update
        
        k1 = InStr(wStr, "(")
        wStr = Mid$(wStr, k1 + 1)
        k1 = InStr(wStr, ")")
        wStr = Mid$(wStr, 1, k1)
        wStr = Replace(wStr, ")", ", ")
        wStr = Trim(wStr)
        k = 0
        i = 0
        ReDim Campi(i)
        While Len(wStr) > 0
          k1 = InStr(wStr, ",")
          wStr1 = Mid$(wStr, 1, k1 - 1)
          wStr = Trim(Mid$(wStr, k1 + 1))
          k1 = InStr(wStr1, " ")
          If k1 > 0 Then
            Wstr2 = Trim(Mid$(wStr1, k1 + 1))
            wStr1 = Trim(Mid$(wStr1, 1, k1 - 1))
          Else
            Wstr2 = ""
          End If
          Set TbCol = m_Fun.Open_Recordset("select * from " & wTypeSql & "Colonne where " & _
                                           "idtable = " & wIdTable & " and Nome = '" & wStr1 & "' ")
          If TbCol.RecordCount = 0 Then
            'Stop
            addSegnalazione wNomeTable, "NOTBFND", Str(wIdTable)
            m_Fun.writeLog "TABLE not found <" & wRec & ">  IdObj: " & GbIdOggetto, "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
            wIdCol = -1
          Else
            wIdCol = TbCol!idcolonna
          End If
          TbCol.Close
          Set TbIdxCol = m_Fun.Open_Recordset("Select * from " & wTypeSql & "IdxCol where idindex = " & TbIndex!IdIndex)
          If TbIdxCol.RecordCount = 0 Then
            k = k + 1
            TbIdxCol.AddNew
            TbIdxCol!IdIndex = wIdIndex
            TbIdxCol!IdTable = wIdTable
            TbIdxCol!idcolonna = wIdCol
            TbIdxCol!Ordinale = k
            TbIdxCol!Order = Wstr2
            TbIdxCol.Update
          End If
          TbIdxCol.Close
          i = i + 1
          ReDim Preserve Campi(i)
          Campi(i) = wStr1 & "," & Wstr2
        Wend
        'If wUniqueIndex Then
        '  TbIndex!Tipo = ControllaPrimaryKey(wIdTable, Campi(), wTypeSql)
        '  TbIndex.Update
        'End If
        TbIndex.Close
        tbTable.Close
      Else
        'Segnala Index gi� presente
        'addSegnalazione wStr1, "NOCOLIDX", wNomeIndex
        'm_Fun.WriteLog "Column for index not recognized: <" & wRec & ">  IdObj: " & GbIdOggetto, "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
      End If
      TbIndexName.Close
    End If
    ''''''''''''''''''''''''''''''''''''''''
    ' STORED PROCEDURE
    ''''''''''''''''''''''''''''''''''''''''
    k = InStr(" " & wStr, " PROCEDURE ")
    If k > 0 Then
      SwElab = True
      wNomeProc = TrovaParamDDL(wStr, "PROCEDURE ")
      k = InStr(wNomeProc, ".")
      If k > 0 Then
        wNomeProcCreator = Mid$(wNomeProc, 1, k - 1)
        wNomeProc = Mid$(wNomeProc, k + 1)
      Else
        wNomeProcCreator = " "
      End If
      wNomePgm = TrovaParamDDL(wStr, " EXTERNAL NAME ")
      tmpWstr = Mid(wStr, InStr(wStr, " EXTERNAL NAME "))
      wTipoPgm = TrovaParamDDL(tmpWstr, " LANGUAGE ")
      If wTipoPgm = "COBOL" Then
        wTipoPgm = "CBL"
      End If
      aggiornaStoredProc wTypeSql, wNomeProc, wNomeProcCreator, wNomePgm, wTipoPgm, wStr
    End If
    
    If Trim(wNomeTable) <> "" Then
      AggiungiComponente wNomeTable, wNomeTable, "TBL", "DDLTABLE"
      If Trim(wNomeDb) <> "" Then
        AggiungiComponente wNomeDb, wNomeTable, "TBD", "DDLTABLE"
      End If
      If Trim(wNomeCreator) <> "" Then
        AggiungiComponente wNomeCreator, wNomeTable, "TBC", "DDLTABLE"
      End If
    Else
      If Trim(wNomeDb) <> "" Then
        AggiungiComponente wNomeDb, wNomeDb, "DBS", "DDLDATABAS"
      End If
      If Trim(wNomeTblSpc) <> "" Then
        AggiungiComponente wNomeTblSpc, wNomeTblSpc, "TBS", "DDLTABLESP"
      End If
    End If

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''A L T E R  T A B L E '''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    k = InStr(" " & wRec, " ALTER ")
    If k > 0 Then
      If InStr(wRec, " SESSION ") > 0 Then k = 0
      If InStr(wRec, " LOGGING ") > 0 Then k = 0
      'If InStr(wrec, " NOLOGGING ") > 0 Then K = 0
      If InStr(wRec, " TABLE ") > 0 Then
        If InStr(wRec, " ENABLE ") > 0 Then
          k = 0
        End If
        'SQ - Phoenix (PEZZA che non serve! sotto ricalcola K!)
        'Errore: ALTER TABLE ... DROP PRIMARY KEY
        'P.S. STA ROUTINI E' INGUARDABILE/IMMODIFICABILE!
        '-> RIFARE COMPLETAMENTE AL PIU PRESTO!
        If InStr(wRec, " DROP ") > 0 Then
          k = 0
        End If
      End If
    End If
  
    If k = 0 Then
      k = InStr(wRec, "CONSTRAINT ")
      If k > 0 Then
        k = InStr(wRec, " CHECK ")
      End If
    End If
  
    'SQ a cosa servono i settaggi =0 sopra se qui lo ricalcolo?!
    If k = 0 Then k = InStr(wRec, "PRIMARY KEY")
    'VC 050107 INSERIMENTO FK inizio'''''
    If k = 0 Then k = InStr(wRec, " FOREIGN")
  
    'SQ - Phoenix (PEZZA che non serve! sotto ricalcola K!)
    'Errore: ALTER TABLE ... DROP PRIMARY KEY
    'P.S. STA ROUTINI E' INGUARDABILE/IMMODIFICABILE!
    '-> RIFARE COMPLETAMENTE AL PIU PRESTO!
    If InStr(wRec, " DROP ") > 0 Then
      k = 0  '� una DROP... non possiamo considerarla! (soprattutto perch� va in runtime error!)
    End If
    If k > 0 Then
      If InStr(wRec, " TABLE ") > 0 Then
        SwElab = True
        wStr = Trim(wRec)
        wNomeTable = TrovaParamDDL(wStr, " TABLE ")
        k = InStr(wNomeTable, ".")
        If k > 0 Then
          wNomeCreator = Mid$(wNomeTable, 1, k - 1)
          wNomeTable = Mid$(wNomeTable, k + 1)
        End If
        Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "Tabelle where Nome = '" & wNomeTable & "' and creator = '" & wNomeCreator & "'")
        If tbTable.RecordCount = 0 Then
          wIdTable = 0
        Else
          wIdTable = tbTable!IdTable
          wIdTableSpc = tbTable!IdTableSpc
        End If
        tbTable.Close
        If InStr(wRec, "PRIMARY") > 0 Then
          wNomeIndex = "PRIMARY_KEY"
          wTipoIndex = "P"
          wUniqueIndex = True
          TbIndexNameCount = 0
          If wIdTable <> 0 Then
            Set TbIndexName = m_Fun.Open_Recordset("select * from " & wTypeSql & "index where tipo = 'P' and idtable = " & wIdTable)
            TbIndexNameCount = TbIndexName.RecordCount
            TbIndexName.Close
          End If
          If TbIndexNameCount = 0 Then
            Set TbIndex = m_Fun.Open_Recordset("Select * from " & wTypeSql & "Index order by idindex")
            If TbIndex.RecordCount Then
              TbIndex.MoveLast
              wIdIndex = TbIndex!IdIndex + 1
            Else
              wIdIndex = 1
            End If
            If wIdTable > 0 Then
              TbIndex.AddNew
              TbIndex!IdIndex = wIdIndex
              TbIndex!IdTable = wIdTable
              TbIndex!IdTableSpc = wIdTableSpc
              TbIndex!nome = wNomeIndex
              TbIndex!Tipo = wTipoIndex
              TbIndex!Unique = wUniqueIndex
              TbIndex!data_creazione = Now
              TbIndex!used = True
              TbIndex!data_modifica = Now
              TbIndex!idorigine = -1
              TbIndex!descrizione = " "
              TbIndex.Update
            End If
            wNomeIndex = "None"
            k1 = InStr(wStr, "CONSTRAINT ")
            If k1 > 0 Then
              wStr = Trim(Mid$(wStr, k1 + 10))
              k1 = InStr(wStr, "PRIMARY")
              If k1 > 0 Then
                wNomeIndex = Trim(Mid$(wStr, 1, k1 - 1))
              End If
            Else
              'solo primary senza constraint
              k1 = InStr(wStr, "PRIMARY")
              If k1 > 0 Then
                wNomeIndex = Trim(Mid$(wStr, 1, k1 - 1))
              End If
            End If
                 
''            K1 = InStr(wStr, "PRIMARY ")
''            If K1 > 0 Then wStr = Mid$(wStr, K1)
''            K1 = InStr(wStr, " IN ")
''            If K1 > 0 Then wStr = Mid$(wStr, 1, K1 - 1)
''            K1 = InStr(wStr, "(")
''            wStr = Mid$(wStr, K1 + 1)
''            K1 = InStr(wStr, ")")
''            wStr = Mid$(wStr, 1, K1)
''            wStr = Replace(wStr, ")", ", ")
''            wStr = Trim(wStr)
            wStr = Mid(wRec, InStr(wRec, "PRIMARY"))
            k = 0
''            While Len(wStr) > 0
            'K1 = InStr(wStr, ",")
''            wStr1 = Mid$(wStr, 1, K1 - 1)
            wStr = Trim(Mid(wStr, InStr(wStr, "(") + 1))
''            Wstr2 = Trim(Mid$(wStr, InStr(wStr, ")")))
''            K1 = InStr(wStr1, " ")
''            If K1 > 0 Then
''              Wstr2 = Trim(Mid$(wStr1, K1 + 1))
''              wStr1 = Trim(Mid$(wStr1, 1, K1 - 1))
''            Else
''              Wstr2 = "ASC"
''              wStr1 = Trim(wStr1)
''            End If
            wStr = Left(wStr, InStr(wStr, ")") - 1)
''''            If Left(Trim(wStr1), 1) = "(" Then
''''              wStr1 = Mid$(wStr1, 2)
''''            End If
                    
            keyCampi = Split(wStr, ",")
            For cont = 0 To UBound(keyCampi)
              Set TbCol = m_Fun.Open_Recordset("select * from " & wTypeSql & "Colonne where idtable = " & wIdTable & " and Nome = '" & Trim(keyCampi(cont)) & "'")
              Set TbIdxCol = m_Fun.Open_Recordset("Select * from " & wTypeSql & "IdxCol where idindex = " & TbIndex!IdIndex)
              If TbIdxCol.RecordCount = 0 Then
                k = k + 1
                TbIdxCol.AddNew
                TbIdxCol!IdIndex = wIdIndex
                TbIdxCol!IdTable = wIdTable
                TbIdxCol!idcolonna = TbCol!idcolonna
                TbIdxCol!Ordinale = k
                'TbIdxCol!Order = Left(Wstr2, 25)
                TbIdxCol.Update
              End If
              TbIdxCol.Close
            Next cont
            'Wend
            TbIndex.Close
          Else
            'Segnala Index gi� presente
          End If
        End If
            
        '*********************************
        '************ CHECK **************
        '*********************************
        'elimina i precedenti
'        Parser.PsConnection.Execute "DELETE * FROM " & wTypeSql & "Check_Constraint WHERE IdTable=" & wIdTable
        
        k1 = InStr(wRec, "CONSTRAINT ")
        If k1 > 0 Then
          wStr = Trim(Mid$(wRec, k1 - 1, Len(wRec)))
          check = Split(wStr, "CONSTRAINT ")
          For k2 = 1 To UBound(check)
            k = InStr(check(k2), " PRIMARY KEY ")
            If k = 0 Then
              'Debug.Print check(K2)
              If k2 = UBound(check) Then
                'Debug.Print "    " & Mid(check(K2), 1, InStrRev(check(K2), " IN ") - 2)
                If InStrRev(check(k2), " IN ") Then
                  check(k2) = Mid(check(k2), 1, InStrRev(check(k2), " IN ") - 1)
                Else
                  check(k2) = ""
                End If
              End If
              If insertCheckConstraint(check(k2), wTypeSql, wIdTable, k2) = False Then
                'scrivere log
              End If
            End If
          Next k2
        End If
          
        '*********************************
        '********* FOREIGN KEY ***********
        '*********************************
        If InStr(wRec, "FOREIGN") > 0 Then
          wStr = Trim(wRec)
          SwElab = True
          
          If insertForeignKey(wStr, wTypeSql) = False Then
            'errore su log
          End If
        End If
        End If
      End If
      
      k = InStr(Trim(wRec), "LABEL ")
      If k > 0 Then
        k = InStr(wRec, " ON ")
        wNomeTable = ""
        wNomeDb = ""
        wNomeCreator = ""
        wStr = Trim(Mid$(wRec, k + 4))
        If Left(wStr, 5) = "TABLE" Then
          ' label su tabella
          SwElab = True
          wNomeTable = TrovaParamDDL(wStr, "TABLE ")
          k = InStr(wNomeTable, ".")
          If k > 0 Then
            wNomeCreator = Mid$(wNomeTable, 1, k - 1)
            wNomeTable = Mid$(wNomeTable, k + 1)
          End If
          wDescr = TrovaParamDDL(wStr, " IS ")
                      
          Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "Tabelle where Nome = '" & wNomeTable & "' and creator = '" & wNomeCreator & "' ")
          If tbTable.RecordCount > 0 Then
            tbTable.MoveLast
            If tbTable.RecordCount = 1 Then
              tbTable!LabelOn = wDescr
              tbTable.Update
            Else
              m_Fun.writeLog "TABLE LABEL: More records with the same Table(" & wNomeTable & ") :  <" & wRec & "> ", "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
            End If
          Else
            m_Fun.writeLog "TABLE LABEL: No record for the table(" & wNomeTable & ") :  <" & wRec & "> ", "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
          End If
          tbTable.Close
        Else
          ' label su campo/i
          SwElab = True
          
          wNomeTable = Left(wStr, InStr(wStr, " "))
          k = InStr(wNomeTable, ".")
          If k > 0 Then
            wNomeCreator = Mid$(wNomeTable, 1, k - 1)
            wNomeTable = Mid$(wNomeTable, k + 1)
          End If
          Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "Tabelle where Nome = '" & wNomeTable & "' and creator = '" & wNomeCreator & "' ")
          If tbTable.RecordCount > 0 Then
            wIdTable = tbTable!IdTable

            k1 = InStr(wStr, " (") + 2
            k2 = InStr(wStr, ") ")
            CampiLabel = Mid(wStr, k1, k2 - k1)
            campoValore() = Split(CampiLabel, "',")
            For k = 0 To UBound(campoValore)
              campoValore(k) = campoValore(k) & "'"
              wNomeCol = Trim(Left(campoValore(k), InStr(Trim(campoValore(k)), " ")))
              wDescr = TrovaParamDDL(campoValore(k), " IS ")
              Set TbCol = m_Fun.Open_Recordset("select * from " & wTypeSql & "Colonne where Nome = '" & wNomeCol & "' and IdTable = " & wIdTable)
              If TbCol.RecordCount > 0 Then
                If TbCol.RecordCount = 1 Then
                  TbCol!LabelOn = wDescr
                  TbCol.Update
                Else
                  m_Fun.writeLog "FIELD LABEL: More records for field(" & wNomeCol & ") in the table(" & wNomeTable & ") :  <" & wRec & "> ", "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
                End If
              Else
                m_Fun.writeLog "FIELD LABEL: No record for field(" & wNomeCol & ") in the table(" & wNomeTable & ") :  <" & wRec & "> ", "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
              End If
            Next k
          Else
            m_Fun.writeLog "FIELD LABEL: No record for the table(" & wNomeTable & ") :  <" & wRec & "> ", "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
          End If
        End If
      End If
      ''''''''''''''''''''''''''''''''''''''''''''''''''''
      '''VC 040107 END LABEL '''''''''''''''''''''''''''''
      ''''''''''''''''''''''''''''''''''''''''''''''''''''
      
      k = InStr(" " & wRec, " COMMENT ")
      If k > 0 Then
        k = InStr(wRec, " ON ")
        wStr = wRec
      End If
      If k > 0 Then
        k = InStr(wStr, " ON ")
        wNomeTable = ""
        wNomeDb = ""
        wNomeTblSpc = ""
        wNomeCreator = ""
        'wStr = Trim(Mid$(wRec, k + 4))
        wRec = ""
        wStr = ""
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''        If Mid$(wStr, 1, 6) = "TABLE " Then
          SwElab = True
''          wNomeTable = TrovaParamDDL(wStr, "TABLE ")
''          k = InStr(wNomeTable, ".")
''          If k > 0 Then
''            wNomeCreator = Mid$(wNomeTable, 1, k - 1)
''            wNomeTable = Mid$(wNomeTable, k + 1)
''          End If
''          wDescr = TrovaParamDDL(wStr, "IS")
''          Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "Tabelle where Nome = '" & wNomeTable & "' and creator = '" & wNomeCreator & "' ")
''          If tbTable.RecordCount = 1 Then
''            tbTable!descrizione = Left(wDescr, 255)
''            tbTable.Update
''          End If
''          tbTable.Close
''        Else
''          SwElab = True
''          If Mid$(wStr, 1, 6) = "COLUMN" Then
''            wStr = Mid$(wStr, 8)
''          End If
''          k1 = InStr(wStr, " ")
''          wNomeTable = Mid$(wStr, 1, k1 - 1)
''          k = InStr(wNomeTable, ".")
''          If k > 0 Then
''            wNomeCreator = Mid$(wNomeTable, 1, k - 1)
''            wNomeTable = Mid$(wNomeTable, k + 1)
''          End If
''          k = InStr(wNomeTable, ".")
''          If k > 0 Then
''            wStr1 = Mid$(wNomeTable, k + 1)
''            wNomeTable = Mid$(wNomeTable, 1, k - 1)
''          End If
''
''          k1 = InStr(wStr, "(")
''          wStr = Mid$(wStr, k1 + 1)
''          k1 = InStrRev(wStr, ")")
'''         wStr = Mid$(wStr, 1, K1)
''          If k1 > 0 Then
''            wStr = Left(wStr, k1 - 1) & Mid(wStr, k1 + 1)
''          End If
''          Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "Tabelle where Nome = '" & wNomeTable & "' and creator = '" & wNomeCreator & "' ")
''          If tbTable.RecordCount = 0 Then
''            k1 = 0
''            addSegnalazione wNomeTable, "NOTBFND", Str(GbIdOggetto)
''          End If
''          If k1 > 0 Then
''            wStr = Trim(wStr)
''            While Len(wStr) > 0
''              k1 = InStr(wStr, ",")
''              If k1 = 0 Then
''                wStr1 = wStr
''                wStr = ""
''              Else
''                wStr1 = Mid$(wStr, 1, k1 - 1)
''                wStr = Trim(Mid$(wStr, k1 + 1))
''              End If
''              k1 = InStr(wStr1, " IS ")
''              If k1 = 0 Then
''                Wstr2 = Trim(wStr1)
''                wStr1 = ""
''              Else
''                Wstr2 = Trim(Mid$(wStr1, k1 + 4))
''                wStr1 = Trim(Mid$(wStr1, 1, k1 - 1))
''              End If
''              If Mid$(Wstr2, 1, 1) = "'" Then
''                Wstr2 = Mid$(Wstr2, 2, Len(Wstr2) - 2)
''              End If
''              Set TbCol = m_Fun.Open_Recordset("select * from " & wTypeSql & "Colonne where idtable = " & tbTable!IdTable & " and Nome = '" & wStr1 & "' ")
''              If TbCol.RecordCount = 1 Then
''                TbCol!descrizione = Left(Wstr2, 255)
''                TbCol.Update
''              End If
''              TbCol.Close
''            Wend
''          Else
''            k1 = InStr(wStr, " IS ")
''            If k1 = 0 Then
''              Wstr2 = Trim(wStr)
''            Else
''              Wstr2 = Trim(Mid$(wStr, k1 + 4))
''            End If
''            Wstr2 = Trim(Wstr2)
''            If Mid$(Wstr2, 1, 1) = "'" Then Wstr2 = Mid$(Wstr2, 2)
''            k1 = InStr(Wstr2, "'")
''            If k1 > 0 Then Wstr2 = Mid$(Wstr2, 1, k1 - 1)
''            If Trim(wStr1) <> "" Then
''              Set TbCol = m_Fun.Open_Recordset("select * from " & wTypeSql & "Colonne where idtable = " & tbTable!IdTable & " and Nome = '" & wStr1 & "' ")
''              If TbCol.RecordCount = 1 Then
''                TbCol!descrizione = Left(Wstr2, 255)
''                TbCol.Update
''              End If
''              TbCol.Close
''            End If
''          End If
''          tbTable.Close
''        End If
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End If
    If InStr(" " & wRec, " SET ") Then SwElab = True
    If InStr(" " & wRec, " COMMIT ") Then SwElab = True
    If InStr(" " & wRec, " GRANT ") Then SwElab = True
    If InStr(" " & wRec, " DROP ") Then SwElab = True
    If InStr(" " & wRec, " QUIT ") Then SwElab = True
    If InStr(" " & wRec, " CONNECT ") Then SwElab = True
    If Trim(wRec) = "" Then SwElab = True
    If Trim(wRec) = ";" Then SwElab = True
    If InStr(" " & wRec, " ALTER ") > 0 Then
      If InStr(wRec, " SESSION ") > 0 Then SwElab = True
      If InStr(wRec, " LOGGING ") > 0 Then SwElab = True
'      If InStr(wrec, " NOLOGGING ") > 0 Then SwElab = True
      If InStr(wRec, " TABLE ") > 0 Then
        If InStr(wRec, " ENABLE ") > 0 Then SwElab = True
      End If
    End If
    If Not SwElab Then
      'SQ: pezza, tanto fa schifo tutto!
      If Left(wRec, 2) <> "--" Then
        addSegnalazione Trim(Left(wRec, 15)), "NOISTRDDL", Str(GbIdOggetto)
        'm_Fun.WriteLog "Istruction not recognized: <" & wRec & ">  IdObj: " & GbIdOggetto, "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
      End If
    End If
  Wend
  Close nFJcl
  
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  TbOggetti!DtParsing = Now
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti!parsinglevel = 1
  TbOggetti.Update
  TbOggetti.Close
  
  Exit Sub
catch:
  GbOldNumRec = GbNumRec
  addSegnalazione wRec, "P00", wRec
  Resume Next
End Sub

'T 11102010
Sub ParserStoreProcedure()
  Dim nFJcl As Variant
  Dim nFErr As Variant
  Dim wStr As String
  Dim Wstr2 As String
  Dim wStr1 As String
  Dim k1 As Integer
  Dim k2 As Integer
  Dim k As Integer
  Dim wNomeTable As String, wRecIn As String, wRec As String
  Dim wIdCol As Long
  Dim wNomeStg As String
  
  Dim wNomeCol As String
  Dim wTipoCol As String
  Dim wLenCol As String
  Dim wDecCol As String
  Dim wOrdinale As Long
  Dim wArea As Long
  Dim wDescr As String
  Dim wIdTable As Long
  Dim stparam As String, stinit As String, wtipo As String
  Dim SwParentesi As Integer, wlivello As Integer
  
  Dim TbSPTable As Recordset, TbSPColonne As Recordset, TbParam As Recordset, TbPsData_Cmp As Recordset, Tbinit As Recordset, rs As Recordset
  Dim wTypeSql As String
  
  Dim campoValore() As String
  Dim CampiLabel As String
  Dim i As Long
  Dim Campi() As String, check() As String
    
  Dim wSql As String
  
  Dim keyCampi() As String
  Dim contatore As Integer
  
  ' Mauro 18/03/2010 : Gestione PROCEDURE
  Dim wNomeProc As String, wNomeProcCreator As String, wNomePgm As String, wTipoPgm As String
  Dim wSTInOut As String
  Dim tmpWstr As String
  Dim soloNome As String, stCampo As String, IntidOggetto As Long
  Dim RTB As RichTextBox
  Dim fileErr As String
  
  Set RTB = MapsdF_Parser.rText

  
  Dim isTemporary As Boolean
  
  On Error GoTo catch
  
  nFJcl = FreeFile
  Open GbFileInput For Input As nFJcl
  If UCase(GbParmRel.DM_DefSqlType) = "ORACLE" Then
    wTypeSql = "DMORC_"
  Else
    wTypeSql = "DMDB2_"
  End If
  While Not EOF(nFJcl)
    'SQ - funzioncina...
    wRecIn = ""
    Line Input #nFJcl, wRecIn
    GbNumRec = GbNumRec + 1
    wRecIn = Trim(wRecIn)
    While Mid$(LTrim(wRecIn), 1, 2) = "--" And Not EOF(nFJcl)
      Line Input #nFJcl, wRecIn
      GbNumRec = GbNumRec + 1
    Wend
    wRec = Trim(wRecIn)
    GbOldNumRec = GbNumRec
    While InStr(wRec, ";") = 0 And Not EOF(nFJcl)
      Line Input #nFJcl, wRecIn
      GbNumRec = GbNumRec + 1
      If Left(wRecIn, 2) <> "--" Then
        wRec = wRec & " " & Trim(Left(wRecIn, 72))
      End If
    Wend
    wRec = Replace(wRec, ";", " ;")
    wRec = Replace(wRec, """", "")
    wRec = Replace(wRec, "(", " (")
    
    
    ''''''''''''''''''''''''''''''''''''''''
    ' STORED PROCEDURE
    ''''''''''''''''''''''''''''''''''''''''
    k = InStr(wRec, " PROCEDURE ")
    If k > 0 Then
      'SwElab = True
      wNomeProc = TrovaParamDDL(wRec, "PROCEDURE ")
      k = InStr(wNomeProc, ".")
      If k > 0 Then
        wNomeProcCreator = Mid$(wNomeProc, 1, k - 1)
        wNomeProc = Mid$(wNomeProc, k + 1)
      Else
        wNomeProcCreator = " "
      End If
      '''
      wStr = wRec
      wNomePgm = Trim(TrovaParamDDL(wStr, " EXTERNAL NAME "))
      If InStr(wNomePgm, "/") Then
        soloNome = TrovaParamDDL(Mid(wNomePgm, Len(wNomePgm) - 9), "/")
        'wNomePgm = "'" & wNomePgm & "'"
      Else
        soloNome = wNomePgm
      End If
      tmpWstr = Mid(wStr, InStr(wStr, " EXTERNAL NAME "))
      wTipoPgm = TrovaParamDDL(tmpWstr, " LANGUAGE ")
      If wTipoPgm = "COBOL" Then
        wTipoPgm = "CBL"
      End If
      aggiornaStoredProc wTypeSql, wNomeProc, wNomeProcCreator, soloNome, wTipoPgm, wStr
      '''
      Set TbSPTable = m_Fun.Open_Recordset("select * from DMSP_tabelle where " & _
                                         "Nome = '" & wNomeProc & "'" & " AND creator= '" & wNomeProcCreator & "'")
      If TbSPTable.RecordCount = 0 Then
        TbSPTable.AddNew
      End If
      TbSPTable!nome = wNomeProc
      TbSPTable!creator = wNomeProcCreator
      TbSPTable!data_creazione = Now
      TbSPTable!data_modifica = Now
      TbSPTable!idObjSource = GbIdOggetto
      TbSPTable!EXT = wNomePgm
      TbSPTable.Update
      TbSPTable.Close
      
      'VC: prova per inserire pi� tabelle con creator diversi
      'Set TbSPTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "tabelle where Nome = '" & wNomeTable & "'")
      Set TbSPTable = m_Fun.Open_Recordset("select * from DMSP_tabelle where " & _
                                         "Nome = '" & wNomeProc & "'" & " AND creator= '" & wNomeProcCreator & "'")
      If TbSPTable.RecordCount Then
        wIdTable = TbSPTable!IdTable
      End If
      TbSPTable.Close

      'fine inserimento tabella
     
      Parser.PsConnection.Execute "Delete * FROM DMSP_Colonne where idtable = " & wIdTable & " ; "
        
      Set TbSPColonne = m_Fun.Open_Recordset("select * from DMSP_colonne where idtable = " & wIdTable)
      wStr = wRec
      k1 = InStr(wStr, "(")
      wStr = Mid$(wStr, k1 + 1)
        
        While Len(wStr) > 0
          wOrdinale = wOrdinale + 1
          SwParentesi = 0
          k1 = 0
          For k = 1 To Len(wStr)
            If Mid$(wStr, k, 1) = ")" Then SwParentesi = SwParentesi + 1
            If Mid$(wStr, k, 1) = "(" Then SwParentesi = SwParentesi + 1
            If Mid$(wStr, k, 1) = "," Then
              If SwParentesi <> 1 Then
                k1 = k
                Exit For
              End If
            End If
          Next k
             
          If k1 = 0 Then
            Wstr2 = Mid$(wStr, 1, Len(wStr) - 1)
            wStr = ""
          Else
            Wstr2 = Trim(Mid$(wStr, 1, k1 - 1))
            wStr = LTrim(Mid$(wStr, k1 + 1))
          End If
          
          k1 = InStr(Wstr2, " ")
'          If k1 = 0 Then
'            wNomeCol = Wstr2
'          Else
            wSTInOut = Mid$(Wstr2, 1, k1 - 1)
            Wstr2 = Trim(Mid$(Wstr2, k1 + 1))
'          End If
               
          k1 = InStr(Wstr2, " ")
          'K2 = InStr(Wstr2, "(")
          'If K2 > 0 And K2 < k1 Then k1 = K2
          'If k1 = 0 Then k1 = Len(Wstr2) + 1
          wNomeCol = Mid$(Wstr2, 1, k1 - 1)
          'k1 = InStr(wTipoCol, "(")
          Wstr2 = Trim(Mid$(Wstr2, k1 + 1))
          k1 = InStr(Wstr2, " ")
          
          If k1 > 0 Then
            wTipoCol = Mid$(Wstr2, 1, k1 - 1)
         Else
            wTipoCol = Wstr2
          End If
          
          wDecCol = 0
          k1 = InStr(wTipoCol, ")")
          If k1 > 0 Then wTipoCol = Mid$(wTipoCol, 1, k1 - 1)
          If wTipoCol = "DEC" Then wTipoCol = "DECIMAL"
          If wTipoCol = "INT" Then wTipoCol = "INTEGER"
          If wTipoCol = "NUM" Then wTipoCol = "NUMBER"
          If wTipoCol = "CHARACTER" Then wTipoCol = "CHAR"
          If wTipoCol = "VARCHARACTER" Then wTipoCol = "VARCHAR"
          If wTipoCol = "VARCHARACTER2" Then wTipoCol = "VARCHAR2"
          
          
          Select Case UCase(wTipoCol)
            Case "INTEGER"
              wLenCol = 4
            Case "SMALLINT"
              wLenCol = 2
            Case "DATE"
              wLenCol = 10
            Case "TIME"
              wLenCol = 8
            Case "TIMESTAMP"
              wLenCol = 26
            Case "REAL"
              wLenCol = 8
            Case "DOUBLE", "LONG"
              wLenCol = 8
            Case "FLOAT"
              wLenCol = 8
            Case "BLOB", "CLOB"
              wLenCol = -1
            Case "CHAR", "VARCHAR", "VARCHAR2"
              k1 = InStr(Wstr2, "(")
              Wstr2 = Mid$(Wstr2, k1 + 1)
              k1 = InStr(Wstr2, ")")
              If k1 > 0 Then wLenCol = Val(Mid$(Wstr2, 1, k1 - 1))
            Case "DECIMAL", "NUMBER", "NUMERIC"
              k1 = InStr(Wstr2, "(")
              Wstr2 = Mid$(Wstr2, k1 + 1)
              k1 = InStr(Wstr2, ")")
              Wstr2 = Mid$(Wstr2, 1, k1 - 1)
              k1 = InStr(Wstr2, ",")
              If k1 = 0 Then
                wLenCol = Val(Trim(Wstr2))
              Else
                wLenCol = Val(Mid$(Wstr2, 1, k1 - 1))
                wDecCol = Val(Mid$(Wstr2, k1 + 1))
              End If
            Case Else
          End Select
         
          TbSPColonne.AddNew
          TbSPColonne!idcolonna = (wIdTable * 1000) + wOrdinale
          TbSPColonne!Ordinale = wOrdinale
          TbSPColonne!IdTable = wIdTable
          TbSPColonne!nome = wNomeCol
          TbSPColonne!TipoINOUT = wSTInOut
          TbSPColonne!Tipo = wTipoCol
          TbSPColonne!Lunghezza = IIf(wLenCol = "", "0", wLenCol)
          TbSPColonne!Decimali = wDecCol
          TbSPColonne.Update
        Wend
        TbSPColonne.Close
    End If
    
  Wend
  Close nFJcl
  '
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  TbOggetti!DtParsing = Now
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti!parsinglevel = 1
  TbOggetti.Update
  TbOggetti.Close
  '
  stparam = ""
  stinit = ""
  Dim conta As Integer
  Set TbSPColonne = m_Fun.Open_Recordset("select Ordinale from DMSP_colonne where idtable = " & wIdTable & " order by Ordinale")
  If TbSPColonne.RecordCount Then
     Set TbParam = m_Fun.Open_Recordset("select * from PsPgm where idOggetto IN (select idOggetto from Bs_Oggetti where Nome = '" & soloNome & "' and tipo = '" & wTipoPgm & "')" & " AND comando = 'USING' ")
     If TbParam.RecordCount Then
       If TbSPColonne.RecordCount <> UBound(Split(TbParam!parameters, ",")) + 1 Then GoTo errparm
     End If
  End If
  TbSPColonne.Close
  TbParam.Close
  Set TbSPColonne = m_Fun.Open_Recordset("select Ordinale from DMSP_colonne where idtable = " & wIdTable & " AND TipoINOUT = 'OUT'  AND Tipo = 'VARCHAR' order by Ordinale")
  If TbSPColonne.RecordCount Then
    RTB.LoadFile m_Fun.FnPathPrj & "\Initialize"
    changeTag RTB, "<PGM-NAME>", soloNome
     
     Set TbParam = m_Fun.Open_Recordset("select * from PsPgm where idOggetto IN (select idOggetto from Bs_Oggetti where Nome = '" & soloNome & "' and tipo = '" & wTipoPgm & "')" & " AND comando = 'USING' ")
     If TbParam.RecordCount Then
        For i = 0 To TbSPColonne.RecordCount - 1
          IntidOggetto = TbParam!idOggetto
          'wOrdinale = TbSPColonne!Ordinale
          On Error GoTo errparm
          stCampo = Split(TbParam!parameters, ",")(TbSPColonne!Ordinale - 1)
          'stparam = stparam & stCampo & vbCrLf
          Set TbPsData_Cmp = m_Fun.Open_Recordset("Select * from PsData_Cmp where idoggetto = " & IntidOggetto & " AND Nome = '" & stCampo & "' order by idarea, ordinale ")
          If TbPsData_Cmp.RecordCount Then
             wArea = TbPsData_Cmp!idArea
             wOrdinale = TbPsData_Cmp!Ordinale
             wlivello = TbPsData_Cmp!livello
             wtipo = TbPsData_Cmp!Tipo
          Else
            Set rs = findDataCmp(IntidOggetto, stCampo)
            If rs.RecordCount Then
              IntidOggetto = rs!idOggetto
              wArea = rs!idArea
              wOrdinale = rs!Ordinale
              wlivello = rs!livello
              wtipo = rs!Tipo
            End If
            rs.Close
          End If
          TbPsData_Cmp.Close
          
          If wlivello = 1 And wtipo = "A" Then
             Set Tbinit = m_Fun.Open_Recordset("Select * from PsData_Cmp where idoggetto = " & IntidOggetto & " and idarea = " & wArea & "  and Ordinale = " & wOrdinale + 1)
             If Tbinit.RecordCount Then
             '& " AND BYTE = 2 AND USAGE = 'BNR' "
                If Tbinit!byte = 2 And Tbinit!Usage = "BNR" Then
                  stinit = stinit & "BPHX            " & Tbinit!nome & vbCrLf
                End If
             Else
                stparam = stparam & stCampo & vbCrLf
             End If
          Else
             stparam = stparam & stCampo & vbCrLf
          End If
          TbSPColonne.MoveNext
        Next i
     End If
    TbParam.Close
    
   If stparam <> "" Then
      nFErr = FreeFile
      fileErr = m_Fun.FnPathPrj & "\Output-Prj\Initialize-Param\" & soloNome & ".log"
      Open fileErr For Output As nFErr
      Print #nFErr, "PGM          STORE PROCEDURE  " & vbCrLf & soloNome & "     " & GbNomePgm & vbCrLf & vbCrLf & stparam
      Close nFErr
   End If
   
   If stinit <> "" Then
     stinit = Left(stinit, Len(stinit) - 2) & "."
     changeTag RTB, "<PARAM-INIT>", stinit
     'changeTag RTB, "<PARAM-FIELDS>", stparam
     On Error GoTo fileErr
     RTB.SaveFile m_Fun.FnPathPrj & "\Output-Prj\Initialize-Param\" & soloNome, 1
   End If
  End If
  TbSPColonne.Close
  Exit Sub

   
fileErr:
  If err.Number = 75 Then
    'Directory non esistente!?
    m_Fun.MkDir_API m_Fun.FnPathPrj & "\Output-Prj\"
  End If

catch:
      GbOldNumRec = GbNumRec
  addSegnalazione wRec, "P00", wRec
  Resume Next
errparm:
   'changeTag RTB, "<PARAM-INIT>", ""
   'changeTag RTB, "<PARAM-FIELDS>", "No match argoment in PGM: " & soloNome & " --- " & GbNomePgm
   'RTB.SaveFile m_Fun.FnPathPrj & "\Output-Prj\Initialize-Param\" & soloNome & ".err", 1
     
   nFErr = FreeFile
   fileErr = m_Fun.FnPathPrj & "\Output-Prj\Initialize-Param\File.err"
   Open fileErr For Append As nFErr
   Print #nFErr, "No match argoment in" & vbCrLf & "PGM      --- STORE PROCEDURE  " & vbCrLf & vbCrLf & soloNome & " --- " & GbNomePgm
   Close nFErr
End Sub

Function TrovaParametro(Stringa, Parola) As String
  Dim wStringa As String
  Dim wParm As String
  Dim Valore As String
  Dim k1 As Integer
  
  TrovaParametro = "None"
  wStringa = Stringa
  wParm = Parola & "="
  
  k1 = InStr(wStringa, wParm)
  If k1 = 0 Then Exit Function
  Valore = Mid$(wStringa, k1 + Len(wParm))
  Select Case Mid(Valore, 1, 1)
    Case "("
      k1 = InStr(Valore, ")")
      Valore = Mid$(Valore, 1, k1)
    Case Else
      k1 = InStr(Valore, " ")
      If k1 > 0 Then
        Valore = Mid$(Valore, 1, k1 - 1)
      End If
      k1 = InStr(Valore, ",")
      If k1 > 0 Then
        Valore = Mid$(Valore, 1, k1 - 1)
      End If
  End Select
  
  k1 = InStr(Valore, "$")
  While k1 > 0
    Mid$(Valore, k1, 1) = Space$(1)
    k1 = InStr(Valore, "$")
  Wend
  'IRIS-DB pezza a colori, non so perch� vanno avanti ad esperimenti...
  'IRIS-DB If Right(Valore, 1) = "'" Then
  If Right(Valore, 1) = "'" And Left(Valore, 2) <> "X'" And Left(Valore, 2) <> "C'" Then
    Valore = Left(Valore, Len(Valore) - 1)
  End If
  TrovaParametro = Trim(Valore)
End Function

Function findParameter(statement As String, parameter As String) As String
  Dim wStringa As String, wParm As String, Valore As String
  Dim k1 As Integer, k As Integer, parentesi As Integer
  
  findParameter = ""
  wStringa = statement
  wParm = parameter & "="
  
  k1 = InStr(wStringa, wParm)
  If k1 = 0 Then Exit Function
  Valore = Mid$(wStringa, k1 + Len(wParm))
  Select Case Left(Valore, 1)
    Case "("
      parentesi = 1
      For k = 2 To Len(Valore)
        Select Case Mid(Valore, k, 1)
          Case "("
            parentesi = parentesi + 1
          Case ")"
            parentesi = parentesi - 1
            If parentesi = 0 Then Exit For
        End Select
      Next k
      Valore = Left(Valore, k)
    Case Else
      k1 = InStr(Valore, " ")
      If k1 > 0 Then
        Valore = Left(Valore, k1 - 1)
      End If
      k1 = InStr(Valore, ",")
      If k1 > 0 Then
        Valore = Left(Valore, k1 - 1)
      End If
  End Select
  
  k1 = InStr(Valore, "$")
  While k1 > 0
    Mid$(Valore, k1, 1) = Space$(1)
    k1 = InStr(Valore, "$")
  Wend
  findParameter = Trim(Valore)
End Function

Function TrovaParamDDL(Stringa As String, Parola As String) As String
  Dim wStringa As String
  Dim wParm As String
  Dim Valore As String
  Dim k1 As Integer
  
  TrovaParamDDL = "None"
  wStringa = Stringa
  
  k1 = InStr(wStringa, Parola)
  If k1 = 0 Then Exit Function
  Valore = LTrim(Mid(wStringa, k1 + Len(Parola)))
  Select Case Mid$(Valore, 1, 1)
    Case "("
      k1 = InStr(Valore, ")")
      Valore = Mid$(Valore, 1, k1)
    Case "'"
      Valore = Mid$(Valore, 2)
      k1 = InStr(Valore, "'")
      Valore = Mid$(Valore, 1, k1 - 1)
    Case "/"
      Valore = Mid$(Valore, 2)
      k1 = InStr(Valore, "/")
      Valore = Mid$(Valore, 1, k1 - 1)
    Case Else
      k1 = InStr(Valore, " ")
      If k1 > 0 Then
        Valore = Mid$(Valore, 1, k1 - 1)
      End If
      k1 = InStr(Valore, ",")
      If k1 > 0 Then
        Valore = Mid$(Valore, 1, k1 - 1)
      End If
      If Mid$(Valore, Len(Valore), 1) = "(" Then
        Valore = Mid$(Valore, 1, Len(Valore) - 1)
      End If
  End Select
  
  k1 = InStr(Valore, "$")
  While k1 > 0
    Mid$(Valore, k1, 1) = Space$(1)
    k1 = InStr(Valore, "$")
  Wend
  TrovaParamDDL = Trim(Valore)
End Function
'''''''''''''''''''''''''
' Parsing Strutture
'''''''''''''''''''''''''
Sub DeleteBatch()
  Parser.PsConnection.Execute "Delete from PsIO_batch where idoggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from DMVSM_filestatus where idPgm = " & GbIdOggetto
End Sub

Sub AggiornaDati()
  Dim wIdStr As Long, wIdStr1 As Long, i As Long, y As Long
  Dim TbStrutture As Recordset, TbCampi As Recordset
  Dim wSqlExec As String, wStr As String
   
  On Error GoTo MngErr
  
  'SQ 3-11-06
  'Parser.PsConnection.BeginTrans
  
  ''''''''''''''''''''''''''''''
  ' Init
  ''''''''''''''''''''''''''''''
  Parser.PsConnection.Execute "Delete * from PsData_Area where idoggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsData_Cmp where idoggetto = " & GbIdOggetto
  'AC 06/07/07
  Parser.PsConnection.Execute "Delete * from PS_OBJROWOFFSET where idoggetto = " & GbIdOggetto
  ''''''''''''''''''''''''''''''''''''''''''
  ' Analisi Strutture
  ''''''''''''''''''''''''''''''''''''''''''
  CaricaLstCampi GbFileInput, GbIdOggetto
  
  ''''''''''''''''''''''''''''''''''''''''''
  ' Allestimento Tabelle
  ''''''''''''''''''''''''''''''''''''''''''
  Set TbCampi = m_Fun.Open_Recordset("select * from psdata_cmp where idoggetto = " & GbIdOggetto)
  Set TbStrutture = m_Fun.Open_Recordset("Select * from PsData_Area where idoggetto = " & GbIdOggetto)
  
  For y = 1 To Idx
    DoEvents
    If DataItem(y).DatLev = 1 Or DataItem(y).DatLev = 77 Or wIdStr = 0 Then
      wStr = DataItem(y).DatNomeFld
      wIdStr = wIdStr + 1
      
      TbStrutture.AddNew
      TbStrutture!idOggetto = GbIdOggetto
      TbStrutture!idArea = wIdStr
      TbStrutture!nome = wStr
      'SQ 11-03-06
      TbStrutture!livello = DataItem(y).DatLev
      TbStrutture!isLinkage = DataItem(y).isLinkage
      TbStrutture.Update

      wIdStr1 = wIdStr
      
      'SQ - 17-10-06 BUTTO TUTTO VIA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      'For i = 1 To UBound(mRelCopyLivello)
      '  ''''NOOOOOOOOOO!!!!!!!!!!!!!! VEDI CaricaLstCampi
      '  'If mRelCopyLivello(i).IdArea = wIdStr And mRelCopyLivello(i).idOggetto = GbIdOggetto Then
      '  If mRelCopyLivello(i).idArea = y And mRelCopyLivello(i).idOggetto = GbIdOggetto Then
      '    write_RelCpyLivelli mRelCopyLivello(i).idOggetto, wIdStr, mRelCopyLivello(i).idCopy
      '    Exit For
      '  End If
      'Next i
    End If
    'stefano: e perch� sta' roba?
    If DataItem(y).DatNomeRed = "" Then DataItem(y).DatNomeRed = " "
    If DataItem(y).DatString = "" Then DataItem(y).DatString = " "
    If DataItem(y).DatSign = "" Then DataItem(y).DatSign = " "
    If DataItem(y).DatVal = "" Then DataItem(y).DatVal = " "
    
    TbCampi.AddNew
    TbCampi!codice = format(GbIdOggetto, "00000000") & "/" & format(wIdStr, "0000") & "/" & format(DataItem(y).DatOrdinale, "0000")
    TbCampi!idArea = wIdStr1
    TbCampi!idOggetto = GbIdOggetto
    TbCampi!nome = DataItem(y).DatNomeFld
    TbCampi!nomeRed = DataItem(y).DatNomeRed
    TbCampi!Ordinale = DataItem(y).DatOrdinale
    TbCampi!livello = DataItem(y).DatLev
    TbCampi!Tipo = DataItem(y).DatString
    TbCampi!Posizione = DataItem(y).DatPos
    TbCampi!byte = DataItem(y).DatDigit
    TbCampi!Occurs = DataItem(y).DatOccurs
    TbCampi!Lunghezza = DataItem(y).DatInt
    TbCampi!Decimali = DataItem(y).DatDec
    TbCampi!Picture = DataItem(y).DatPicture
    TbCampi!Segno = DataItem(y).DatSign
    TbCampi!Usage = DataItem(y).DatUsage
    TbCampi!idCopy = DataItem(y).idCopy
    TbCampi!Valore = Left(DataItem(y).DatVal, 250)
    TbCampi.Update
    
    '''''''''''''''''''''''''''''''''''''''''''''''''
    ' Refresh segnalazioni campi programmi chiamanti
    '''''''''''''''''''''''''''''''''''''''''''''''''
    If GbTipoInPars = "CPY" And Len(Trim(TbCampi!Valore)) _
       And TbCampi!Valore <> "SPACES" And TbCampi!Valore <> "SPACES" Then
      updateSegnalazioni TbCampi!nome, GbTipoInPars
    End If
  Next y
  TbCampi.Close
  TbStrutture.Close
  'SQ 3-11-06
  'Parser.PsConnection.CommitTrans
  Exit Sub
MngErr:
  ' Oltrepassata l'area di 9999
  ' Superato
  'SQ Che hai fatto Maurizio?!
'  If Err.Number = 0 Then
'    GbOldNumRec = GbNumRec
'    AggiungiSegnalazione 0, "P00", 0
'  Else
'    Resume Next
'  End If
  GbOldNumRec = GbNumRec
  addSegnalazione 0, "P00", 0
End Sub

Sub parseINCLUDE(wRecord As String)
  Dim token As String
   
  token = nexttoken(wRecord)
  'SQ 10-02-09 (vedi PLI)
  checkRelations Trim(Replace(token, ";", "")), "INCLUDE"
    
  'Setta il tipo dell'oggetto se copy di sistema
  ParamCpy token
End Sub

Sub checkFileRelation(nomeLink As String, nomeArea As String)
  Dim idOggetto As Long, idArea As Long ', idArchivio As Long
  Dim Ordinale As Integer, k As Integer
  Dim fieldName As String
  Dim rs As Recordset
  Dim VSAMName As String
  
  'On Error GoTo subErr
  
  fieldName = nomeLink  'per una eventuale segnalazione...
  If InStr(nomeLink, "'") Or InStr(nomeLink, """") Then
    nomeLink = Replace(Replace(nomeLink, "'", ""), """", "")
  Else
    nomeLink = getFieldValue(nomeLink)
  End If
  
  If Len(nomeLink) = 0 Then
    addSegnalazione fieldName, "NO" & "DATASET" & "VALUE", fieldName
  Else
    '''''''''''''''''''''''''''''''
    ' Nome DATASET in FCT:
    ' nomeLink � il Nome logico!
    '''''''''''''''''''''''''''''''
    Dim dsn As String
    Set rs = m_Fun.Open_Recordset("select dsn from CICS_Fct where Fil = '" & nomeLink & "'")
    If rs.RecordCount Then
      dsn = rs!dsn & ""
      If Len(dsn) Then
        '''''''''''''''''''''''''''''''''''''''''''''
        ' Aggiornamento DMVSM_Archivio se nuovo file
        '''''''''''''''''''''''''''''''''''''''''''''
        Set rs = m_Fun.Open_Recordset("select * from DMVSM_Archivio where Nome = '" & dsn & "' ")
        If rs.RecordCount = 0 Then
          rs.AddNew
          rs!nome = dsn
          rs!Tag = "Generated by CICS"
          rs!data_creazione = Now
          rs.Update
          ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
          'idArchivio = rs!idArchivio  'mi serve dopo per i tracciati...
          VSAMName = "VSM_" & rs!idArchivio
          rs!VSam = VSAMName
          rs.Update
          Set rs = m_Fun.Open_Recordset("select * from DMVSM_NomiVSAM where NomeVSAM = '" & VSAMName & "'")
          If rs.RecordCount = 0 Then
            rs!NomeVSAM = VSAMName
            rs!Acronimo = VSAMName
            rs!to_migrate = True
            rs!obsolete = False
            rs.Update
          End If
        Else
          'idArchivio = rs!idArchivio  'mi serve dopo per i tracciati...
          VSAMName = rs!VSam & ""
        End If
      Else
        addSegnalazione nomeLink, "NO" & "DATASET" & "FOUND", nomeLink 'discriminare dal successivo?!
      End If
    Else
      addSegnalazione nomeLink, "NO" & "DATASET" & "FOUND", nomeLink
    End If
    rs.Close
    
    'SQ
    'Set rs = m_Fun.Open_Recordset("select * from PsRel_file where type = 'CX' and Link = '" & nomeLink & "' and program = '" & GbNomePgm & "'  and Step = '" & nomeArea & "' ")
    '...
    ''''?rs!idOggetto = 0
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' ATTENZIONE: ADESSO LI AGGIUNGO TUTTI, DUPLICATI... CAMBIARE!
    ' Chiarire chi usa questi record...
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set rs = m_Fun.Open_Recordset("select * from PsRel_file where idoggetto = " & GbIdOggetto)
    If rs.RecordCount Then
      Dim nFile As Integer
      rs.MoveLast 'mi serve l'ultimo progressivo... (nFile)
      nFile = rs!nFile + 1
    Else
      nFile = 1
    End If
    rs.AddNew
    rs!idOggetto = GbIdOggetto
    rs!nFile = nFile
    rs!Type = "CX"
    '''''''rs!use = " "
    rs!nstep = 0
    rs!program = GbNomePgm
    rs!LINK = nomeLink
    rs!dsnName = dsn
    '''''''''''?? rs!Step = nomeArea
    rs.Update
    
''''    Set rs = m_Fun.Open_Recordset("select * from DMVSM_Archivio where Nome = 'CICS." & nomeLink & "' ")
''''    If rs.RecordCount = 0 Then
''''      rs.AddNew
''''      rs!Nome = "CICS." & nomeLink
''''      rs!data_creazione = Now
''''      rs!data_modifica = Now
''''      rs!ESDS = False
''''      rs!KSDS = False
''''      rs!RRDS = False
''''      rs!Seq = False
''''      rs!gdg = False
''''      rs!recform = 0
''''      rs!recsize = 0
''''      rs!MinLen = 0
''''      rs!MaxLen = 0
''''      rs!KeyStart = 0
''''      rs!KeyLen = 0
''''      rs.Update
''''    End If
    
    ''''''''''''''''''''''''''''''''''''''''''
    ' Ricerca Area:
    ' - nomeArea: input ; idOggetto,IdArea: output
    ''''''''''''''''''''''''''''''''''''''''''
    Dim idObjArea As String
    idObjArea = CercaIdArea(nomeArea)
    k = InStr(idObjArea, "/")
    If k > 0 Then
      idOggetto = Val(Mid(idObjArea, 1, k - 1))
      idArea = Val(Mid(idObjArea, k + 1))
    End If
    If idOggetto Then
      ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
      'Set rs = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " and idoggetto = " & idOggetto & " and idarea = " & idArea)
      Set rs = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                    "VSAM = '" & VSAMName & "' and idoggetto = " & idOggetto & " and idarea = " & idArea)
      If rs.RecordCount = 0 Then
        'Set rs = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " order by ordinale ")
        Set rs = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                      "VSAM = '" & VSAMName & "' order by ordinale ")
        If rs.RecordCount = 0 Then
          Ordinale = 1
        Else
          rs.MoveLast
          Ordinale = rs.RecordCount + 1
        End If
        rs.AddNew
        'rs!idArchivio = idArchivio
        rs!VSam = VSAMName
        rs!Ordinale = Ordinale
        rs!idOggetto = idOggetto
        rs!idArea = idArea
        rs!Tag = " "
        rs.Update
      End If
    End If
  End If
  Exit Sub
subErr:
  'nop (per il momento...)
  'If Err.Number = xxx Then
  '  MsgBox Err.Description
  'Else
  '  'gestire
  'End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''
' Ritorna "None" se non trova inizializzazioni...
' NON aggancia i casi:
' 01 GINO.
'    COPY BLUTO.
'
' SQ !!!!!!!!!!!!!!! buttare via: usare getFieldValue
''''''''''''''''''''''''''''''''''''''''''''''''''
Function PrendiValue(Stringa As String) As String
  Dim tb As Recordset
  
  PrendiValue = "None"
  
  Set tb = m_Fun.Open_Recordset("Select * from psdata_cmp where Nome = '" & Stringa & "'  and idoggetto = " & GbIdOggetto)
  If tb.RecordCount Then
    If Len(tb!Valore) Then
      PrendiValue = Replace(tb!Valore, "'", "")
      PrendiValue = Trim(Replace(PrendiValue, """", ""))
    End If
  Else
    Set tb = m_Fun.Open_Recordset("Select * from psdata_cmp where Nome = '" & Stringa & "' ")
    Do While Not tb.EOF
      If SeConcatenato(tb!idOggetto, GbIdOggetto) Then
        If Len(tb!Valore) Then
          PrendiValue = Replace(tb!Valore, "'", "")
          PrendiValue = Trim(Replace(PrendiValue, """", ""))
          Exit Do
        End If
      End If
    tb.MoveNext
    Loop
  End If
  tb.Close
End Function

Function SeBilanciaParentesi(Stringa As String) As Integer
  Dim Ka As Integer, Kc As Integer, k As Integer
  Dim sKa() As String, sKc() As String, SkI() As String
  
  ReDim SkI(0)
  For k = 1 To Len(Stringa)
    ReDim Preserve SkI(k)
    SkI(k) = Mid$(Stringa, k, 1)
  Next k
  
  sKa = Filter(SkI, "(", True)
  sKc = Filter(SkI, ")", True)
  SeBilanciaParentesi = UBound(sKa) - UBound(sKc)
End Function

Function SeParamIstr(wParam, wAmb As String, wCom As String) As Boolean
  Dim SwParam As Boolean
  Dim IdEnv As Long
  Dim TbEnvIstr As Recordset
  Dim TbEnvIstrPar As Recordset
  If InStr(wParam, "'") > 0 Then
     SeParamIstr = False
     Exit Function
  End If
  If InStr(wParam, "(") > 0 Then
     SeParamIstr = False
     Exit Function
  End If
  If wParam = wCom Then
     SeParamIstr = True
     Exit Function
  End If
  
'  SeParamIstr = True
'  Exit Function
  
  Select Case wAmb
    Case "CICS"
      IdEnv = 1
    Case "IMS/DB C"
      IdEnv = 2
    Case "IMS/DB E"
      IdEnv = 3
    Case "IMS/DC"
      IdEnv = 4
    Case "SQL"
      IdEnv = 5
    Case "SQL ORA"
      IdEnv = 7
    Case Else
      SeParamIstr = False
      Exit Function
  End Select
  
  SwParam = False
  Set TbEnvIstr = m_Fun.Open_Recordset_Sys("select * from IstrParamAll where " & _
                                           "IdEnvironment = " & IdEnv & " and parametro = '" & wParam & "'")
  If TbEnvIstr.RecordCount Then
    SwParam = True
  End If
  TbEnvIstr.Close
  SeParamIstr = SwParam
  
End Function

Function NormNomeObj(Stringa As String) As String
  Dim wStr As String
  Dim k As Integer
  
  wStr = Stringa
  
  k = InStr(wStr, "/")
  While k > 0
    wStr = Mid$(wStr, k + 1)
    k = InStr(wStr, "/")
  Wend
  
  k = InStr(wStr, ".")
  While k > 0
    wStr = Mid$(wStr, k + 1)
    k = InStr(wStr, ".")
  Wend
  
  NormNomeObj = wStr
End Function

Function LeggiBufferCbl(wRec, nFcb, wLimite) As String
  Dim wRecord As String
  Dim flag As Boolean
  Dim wRecIn As String
  Dim SwSql As Boolean
  Dim PosPunto As Long
  
  wRecord = wRec
  
  SwSql = False
  If InStr(wRec & " ", " SQL ") > 0 Then SwSql = True
  
  flag = InStr(wRec, wLimite) > 0
  
  If Len(wRec) < 81 And Not flag Then
    wRecord = RTrim(Mid$(wRec, 1, 72))
  End If
  If wLimite = "END-EXEC" Then
    If InStr(wRecord, " PIC ") > 0 Or InStr(wRecord, " PICTURE ") > 0 Then
      LeggiBufferCbl = wRecord
      Exit Function
    End If
  End If
  While Not flag And Not EOF(nFcb)
    Line Input #nFcb, wRecIn
    GbNumRec = GbNumRec + 1
    If GbTipoInPars = "CBL" Or GbTipoInPars = "CPY" Then
      If Mid$(wRecIn, 7, 1) <> "*" Then
        wRecord = wRecord & " " & Trim(Mid$(wRecIn, 8, 65))
        If InStr(wRecIn, wLimite) > 0 Then flag = True
      End If
    End If
    'SQ 10-02-06
'    If InStr(wRecIn, wLimite) > 0 Then
'      flag = 1
'    End If
    If Not SwSql Then
'      If GbTipoInPars = "CBL" Or GbTipoInPars = "CPY" Then
'        PosPunto = InStr(wRecIn, ".")
'        If PosPunto > 7 Then
'          Flag = 1
'        End If
'      End If
    End If
  Wend
  
  LeggiBufferCbl = wRecord
End Function

Function FormattaWhere(Stringa As String) As String
   Dim wStr1 As String
   Dim Testo As String
   Dim k As Integer
   Dim wByte As String
   Dim wByte1 As String
   
   wStr1 = Stringa
   For k = 1 To Len(wStr1)
      wByte = Mid$(wStr1, k, 1)
      Select Case wByte
         Case "=", ">", "<"
            wByte1 = Mid$(wStr1, k - 1, 1)
            Select Case wByte1
               Case " ", "=", ">", "<"
               Case Else
                  Testo = Testo & " "
            End Select
            Testo = Testo & wByte
            wByte1 = Mid$(wStr1, k + 1, 1)
            Select Case wByte1
               Case " ", "=", ">", "<"
               Case Else
                  Testo = Testo & " "
            End Select
         Case "("
            wByte1 = Mid$(wStr1, k - 1, 1)
            Select Case wByte1
               Case " "
                  Testo = Mid$(Testo, 1, Len(Testo) - 1)
            End Select
            Testo = Testo & wByte
                 
         Case Else
            Testo = Testo & wByte
      End Select
   Next k
   
   FormattaWhere = Testo
End Function
''''''''''''''''''''''''''''''''''''''''''
' A cosa serve?!
''''''''''''''''''''''''''''''''''''''''''
Function CreaNomeRout(nomeDBD As String) As String
  'Dim wIdDBD As Long
  'Dim tb As Recordset
 
  'Set tb = m_Fun.Open_Recordset("select idOggetto from Bs_oggetti where tipo = 'DBD' and Nome = '" & Trim(nomeDBD) & "'")
  'If tb.RecordCount > 0 Then
  '  wIdDBD = tb!idOggetto
  'End If
  'tb.Close
  
  'CreaNomeRout = "%%%%" & Format(wIdDBD, "0000")
  CreaNomeRout = "%%%%" & format(wIdDBD(0), "0000")

End Function

Function SelezionaParam(Stringa, Ident) As String
  Dim wStringa As String
  Dim k As Integer
  Dim NumP As Integer
  Dim wStr As String
  Dim wByte As String
  
  wStr = ""
  wStringa = Stringa
  If Ident = "SEARCH" Then
    If InStr(Stringa, " LAST") > 0 Then
      SelezionaParam = "LAST"
      Exit Function
    End If
    If InStr(Stringa, " FIRST") > 0 Then
      SelezionaParam = "FIRST"
      Exit Function
    End If
    SelezionaParam = ""
    Exit Function
  End If
  
  k = InStr(wStringa, Ident)
  If k = 0 Then
    SelezionaParam = ""
    Exit Function
  End If
  If k > 1 Then
  Select Case Mid$(wStringa, k - 1, 1)
    Case " ", "(", ",", "."
    Case Else
      SelezionaParam = ""
      Exit Function
  End Select
  End If
  wStringa = Trim(Mid$(wStringa, k + Len(Ident)))
  If Mid$(wStringa, 1, 1) = "=" Then
    wStringa = Mid$(wStringa, 2)
  End If
  Select Case Mid$(wStringa, 1, 1)
    Case "("
      k = InStr(wStringa, ")")
      If k = 0 Then
        wStringa = RTrim(wStringa) & ")"
        k = InStr(wStringa, ")")
      End If
      wStr = Mid$(wStringa, 2, k - 2)
    Case "'"
      k = InStr(Mid$(wStringa, 2), "'")
      wStr = Mid$(wStringa, 2, k - 1)
    Case Else
      k = InStr(wStringa, " ")
      If k > 0 Then
        wStringa = Mid$(wStringa, 1, k)
      End If
      k = InStr(wStringa, ",")
      If k > 0 Then
        wStringa = Mid$(wStringa, 1, k - 1)
      End If
      wStr = wStringa
  End Select
  If Ident = "DSN=" Then
    SelezionaParam = wStr
    Exit Function
  End If
  k = InStr(wStr, ")")
  If k > 0 Then
    If k > Len(wStr) Then
      wStr = wStr
    End If
    wStr = Mid$(wStr, 1, k)
  End If
  
  If InStr(wStr, "(") > 0 Then
    wStr = ""
    NumP = 0
    For k = 1 To Len(wStringa)
      wByte = Mid$(wStringa, k, 1)
      If wByte = "(" Then NumP = NumP + 1
      If wByte = ")" Then NumP = NumP - 1
      wStr = wStr & wByte
      If NumP = 0 Then Exit For
    Next k
    If Left(wStr, 1) = "(" Then
      wStr = Mid$(wStr, 2, Len(wStr) - 2)
    End If
  End If
  
  SelezionaParam = wStr
End Function

Function CercaIdArea(NomeStr) As Variant
  Dim wId As Variant, k As Integer
  Dim tb As Recordset, tb1 As Recordset
  
  If Mid$(NomeStr, 1, 1) = "(" Then
    NomeStr = Mid$(NomeStr, 2, Len(NomeStr) - 2)
  End If
  Set tb = m_Fun.Open_Recordset("select * from PsData_Area where Nome = '" & NomeStr & "' and idoggetto = " & GbIdOggetto)
  If tb.RecordCount > 0 Then
    CercaIdArea = format(tb!idOggetto, "000000") & "/" & format(tb!idArea, "0000")
    tb.Close
    Exit Function
  End If
  tb.Close
  
  Set tb = m_Fun.Open_Recordset("select * from PsData_Area where Nome = '" & NomeStr & "'")
  If tb.RecordCount > 0 Then
    If tb.RecordCount = 1 Then
      CercaIdArea = format(tb!idOggetto, "000000") & "/" & format(tb!idArea, "0000")
      tb.Close
      Exit Function
    End If
    For k = 1 To tb.RecordCount
      Set tb1 = m_Fun.Open_Recordset("select * from PsRel_Obj where " & _
                                     "relazione = 'CPY' and idoggettor = " & tb!idOggetto & " and idoggettoc = " & GbIdOggetto)
      If tb1.RecordCount = 1 Then
        CercaIdArea = format(tb!idOggetto, "000000") & "/" & format(tb!idArea, "0000")
        tb1.Close
        tb.Close
        Exit Function
      End If
      tb1.Close
      tb.MoveNext
    Next k
  End If
  tb.Close
  
  If wId = 0 Then
    Set tb = m_Fun.Open_Recordset("select * from PsData_cmp where Nome = '" & NomeStr & "' ")
    While Not tb.EOF
      Set tb1 = m_Fun.Open_Recordset("select * from PsData_Area where IdArea = " & tb!idArea & " and idoggetto = " & tb!idOggetto)
      While Not tb1.EOF
        If SeConcatenato(tb1!idOggetto, GbIdOggetto) Then
          CercaIdArea = format(tb!idOggetto, "000000") & "/" & format(tb!idArea, "0000") & " - " & NomeStr
          tb1.Close
          tb.Close
          Exit Function
        End If
        tb1.MoveNext
      Wend
      tb1.Close
      tb.MoveNext
    Wend
    tb.Close
  End If
  CercaIdArea = 0
End Function

Function getIdArea(NomeStr As String) As String
  Dim wId As Variant
  Dim k As Integer, k1 As Integer
  Dim tb As Recordset, tb1 As Recordset
  
  'SQ Rileggeva il PSB!!!!
'''''''''    If UBound(wPcb) = 0 Then
'''''''''       Set tb = m_Fun.Open_Recordset("select * from PsRel_Obj where idoggettoc = " & GbIdOggetto & " and relazione = 'PSB'")
'''''''''       If tb.RecordCount > 0 Then
'''''''''          wId = tb!IdOggettoR
'''''''''          CaricaPsb wId
'''''''''       End If
'''''''''    End If
  
  If Left(NomeStr, 1) = "(" Then
    NomeStr = Mid$(NomeStr, 2, Len(NomeStr) - 2)
  End If
  
  '''''''''''''''''''''''''''''
  ' Gi� trovato in precedenza?
  '''''''''''''''''''''''''''''
  For k = 1 To UBound(idAree)
    k1 = InStr(idAree(k), ":")
    If Mid(idAree(k), k1 + 1) = NomeStr Then
      wId = Left(idAree(k), k1 - 1)
      If wId = "N" Then wId = 0
      getIdArea = wId
      Exit Function
    End If
  Next k
  
  Set tb = m_Fun.Open_Recordset("select * from PsData_Area where Nome = '" & NomeStr & "' and idoggetto = " & GbIdOggetto)
  If tb.RecordCount Then
    addIdArea format(tb!idOggetto, "000000") & "/" & format(tb!idArea, "0000"), NomeStr
    getIdArea = format(tb!idOggetto, "000000") & "/" & format(tb!idArea, "0000")
    tb.Close
    Exit Function
  End If
  tb.Close
  
  Set tb = m_Fun.Open_Recordset("select * from PsData_Area where Nome = '" & NomeStr & "'")
  If tb.RecordCount > 0 Then
    If tb.RecordCount = 1 Then
      addIdArea format(tb!idOggetto, "000000") & "/" & format(tb!idArea, "0000"), NomeStr
      getIdArea = format(tb!idOggetto, "000000") & "/" & format(tb!idArea, "0000")
      tb.Close
      Exit Function
    End If
    For k = 1 To tb.RecordCount
      Set tb1 = m_Fun.Open_Recordset("select * from PsRel_Obj where " & _
                                     "relazione = 'CPY' and idoggettor = " & tb!idOggetto & " and idoggettoc = " & GbIdOggetto)
      If tb1.RecordCount = 1 Then
        getIdArea = format(tb!idOggetto, "000000") & "/" & format(tb!idArea, "0000")
        addIdArea getIdArea, NomeStr
        tb1.Close
        tb.Close
        Exit Function
      End If
      tb1.Close
      tb.MoveNext
    Next k
  End If
  tb.Close
  
  If wId = 0 Then
    Set tb = m_Fun.Open_Recordset("select * from PsData_cmp where Nome = '" & NomeStr & "'")
    While Not tb.EOF
      Set tb1 = m_Fun.Open_Recordset("select * from PsData_Area where IdArea = " & tb!idArea & " and idoggetto = " & tb!idOggetto)
      While Not tb1.EOF
        If SeConcatenato(tb1!idOggetto, GbIdOggetto) Then
          getIdArea = format(tb!idOggetto, "000000") & "/" & format(tb!idArea, "0000") & " - " & NomeStr
          addIdArea getIdArea, NomeStr
          tb1.Close
          tb.Close
          Exit Function
        End If
        tb1.MoveNext
      Wend
      tb1.Close
      tb.MoveNext
    Wend
    tb.Close
  End If
  getIdArea = 0

  addIdArea "N", NomeStr
End Function

Sub addIdArea(risp, Stringa)
  ReDim Preserve idAree(UBound(idAree) + 1)
  idAree(UBound(idAree)) = risp & ":" & Stringa
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Chiarire se serve tutto 'sto giro,
' piuttosto che una sola select in join sulla relazioni...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function SeConcatenato(ID1 As Variant, ID2 As Variant) As Boolean
  Dim tbc As Recordset, tbr As Recordset
  
  SeConcatenato = False
  If ID1 = ID2 Then
    SeConcatenato = True
    Exit Function
  End If

  Set tbc = m_Fun.Open_Recordset("select * from psrel_obj where idoggettoc = " & ID2 & " and idoggettor = " & ID1)
  If tbc.RecordCount Then
    SeConcatenato = True
    tbc.Close
    Exit Function
  End If
  tbc.Close
  
  Set tbc = m_Fun.Open_Recordset("select * from psrel_obj where idoggettor = " & ID2 & " and idoggettoc = " & ID1)
  If tbc.RecordCount Then
    SeConcatenato = True
  Else
    tbc.Close
    'Prova a vedere se � richiamato da un programma che utilizza questi id (solo un livello)
    Set tbc = m_Fun.Open_Recordset("select * from psrel_obj where idoggettor = " & ID2)
    While Not tbc.EOF
      Set tbr = m_Fun.Open_Recordset("select * from psrel_obj where " & _
                                     "idoggettor = " & ID1 & " and idoggettoc = " & tbc!IdOggettoC)
      If tbr.RecordCount Then
        SeConcatenato = True
      End If
      tbr.Close
      
      Set tbr = m_Fun.Open_Recordset("select * from psrel_obj where " & _
                                     "idoggettor = " & tbc!IdOggettoC & " and idoggettoc = " & ID1)
      If tbr.RecordCount Then
        SeConcatenato = True
      End If
      tbr.Close
      tbc.MoveNext
    Wend
  End If
  tbc.Close
End Function

Public Function DecodificaCodCom(Valore As String) As String
  Dim Stringa As String
  Dim wApp As String
  Dim i As Long
  
  If Valore = "Nothing" Then
    DecodificaCodCom = "None"
    Exit Function
  End If
   
  If Len(Valore) > 5 Then
    DecodificaCodCom = "Undefined"
    Exit Function
  End If
  
  For i = 1 To Len(Valore)
    Select Case Mid(Valore, i, 1)
      Case "*"
      Case "F", "L", "C", "P", "D", "N", "Q", "U", "V", "-"
        Stringa = Mid(Valore, i, 1)
      Case "Nothing"
        Stringa = ""
      Case Else
        Stringa = "Undefined"
    End Select
    wApp = wApp & " " & Stringa
  Next i
  
  DecodificaCodCom = Trim(wApp)
End Function

Public Function getId(column As String, table As String)
  Dim r As Recordset
  
  On Error GoTo errdb
  Set r = m_Fun.Open_Recordset("Select max(" & column & ") From " & table)
  getId = IIf(IsNull(r(0)), 1, r(0) + 1)
  r.Close
  Exit Function
errdb:
  getId = -1
End Function
'ritorna la parola successiva (space unico separatore)
'Considera come delimitatore di "token" le parentesi e gli apici singoli...
'Effetto Collaterale: mangia...
Function nextToken_tmp(inputString As String)
  Dim level As Integer, i As Integer, j As Integer
  Dim currentChar As String, inputStringCopy As String
  
  If (Len(inputString)) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "("
        i = 2
        level = 1
        nextToken_tmp = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "("
              level = level + 1
              nextToken_tmp = nextToken_tmp & currentChar
            Case ")"
              level = level - 1
              nextToken_tmp = nextToken_tmp & currentChar
            Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nextToken_tmp = nextToken_tmp & currentChar
              Else
                err.Raise 123, "nextToken_tmp", "syntax error on " & inputString
                Exit Function
              End If

          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
        'SQ 7-03-6: ATTENZIONE ALLA VIRGOLA DOPO la parentesi!!!
        'Es: COND=(0,NE),PARM=
        If Left(inputString, 1) = "," Then
          inputString = Trim(Mid(inputString, 2))
        End If
      Case "'"
        inputString = Mid(inputString, 2)
        i = InStr(inputString, "'")
        If i Then
          nextToken_tmp = Left(inputString, InStr(inputString, "'") - 1)
          inputString = Trim(Mid(inputString, InStr(inputString, "'") + 1))
        Else
          err.Raise 123, "nextToken_tmp", "syntax error on " & inputString
          Exit Function
        End If
      Case Else
        'Ricerca primo separatore: " " o ","
        For i = 1 To Len(inputString)
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case " ", "'"
              nextToken_tmp = Left(inputString, i - 1)
              inputString = Trim(Mid(inputString, i))
              Exit Function
            Case "("
              nextToken_tmp = Left(inputString, i - 1)
              inputString = Trim(Mid(inputString, i))
              'pezza: se ho pippo(...),... rimane una virgola iniziale:
              'If Left(inputString, 1) = "," Then inputString = Trim(Mid(inputString, 2))
              Exit Function
            Case ","
              nextToken_tmp = Left(inputString, i - 1)
              inputString = Trim(Mid(inputString, i + 1))
              Exit Function
            Case Else
              'avanza pure
          End Select
        Next i
        'nessun separatore:
        nextToken_tmp = inputString
        inputString = ""
    End Select
  End If

End Function
'SQ: provvisorio per "," come separatore... farne uno con argomento opzionale...
'ritorna la parola successiva (space unico separatore)
'Considera come delimitatore di "token" le parentesi e gli apici singoli...
'Effetto Collaterale: mangia...
Function nexttoken(inputString As String)
  Dim level As Integer, i As Integer, j As Integer
  Dim currentChar As String
  Dim areaBackup As String
  
  'Mangio i bianchi davanti (primo giro...)
  inputString = LTrim(inputString)
  If (Len(inputString) > 0) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "("
        i = 2
        level = 1
        nexttoken = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "("
              level = level + 1
              nexttoken = nexttoken & currentChar
            Case ")"
              level = level - 1
              nexttoken = nexttoken & currentChar
            Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nexttoken = nexttoken & currentChar
              Else
                ''err.Raise 123, "nextToken", "syntax error on " & inputString
                
                addSegnalazione inputString, "NOISTRDLI", inputString
                inputString = ""
                Exit Function
              End If
          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
      Case "'"
      ' IRIS-DB: ma perch� toglie gli apici????? Cos� non si distinguono pi� variabili e costanti
      '          mi sembra veramente una cosa idiota - per ora lo cambio solo per il PLI, in COBOL
      '          in qualche modo miracoloso funziona
        If GbTipoInPars = "PLI" Or GbTipoInPars = "INC" Then
          areaBackup = Mid(inputString, 2)
          i = InStr(areaBackup, "'")
          If i Then
            nexttoken = Left(inputString, InStr(2, inputString, "'"))
            inputString = Mid(inputString, InStr(inputString, "'") + 1)
          Else
            'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
            inputString = "''" & inputString
            err.Raise 123, "nextToken", "syntax error on " & inputString
            Exit Function
          End If
        Else
          inputString = Mid(inputString, 2)
          i = InStr(inputString, "'")
          If i Then
            nexttoken = Left(inputString, InStr(inputString, "'") - 1)
            inputString = Mid(inputString, InStr(inputString, "'") + 1)
          Else
            'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
            inputString = "''" & inputString
            err.Raise 123, "nextToken", "syntax error on " & inputString
            Exit Function
          End If
        End If
      Case Else
        i = InStr(inputString, " ")
        j = InStr(inputString, "(")
        If ((i > 0 And j > 0 And j < i) Or (i = 0 And j > 0)) Then i = j
        If (i > 0) Then
          nexttoken = Left(inputString, i - 1)
          inputString = Trim(Mid(inputString, i))
        Else
          nexttoken = inputString
          inputString = ""
        End If
    End Select
  End If
End Function

Public Function nextLevel_is_greater(rs As Recordset) As Boolean
   Dim cLev As Long
   
   cLev = rs!livello
   
   If rs.AbsolutePosition <= rs.RecordCount - 1 Then
      rs.MoveNext
   
      If cLev >= rs!livello Then
         nextLevel_is_greater = False
      Else
         nextLevel_is_greater = True
      End If
      
      rs.MovePrevious
   Else
      nextLevel_is_greater = False
   End If
End Function
Public Function param_explode_Copy() As Boolean
   Dim wApp() As String
   
   wApp = m_Fun.RetrieveParameterValues("PS-EXPLODE-COPY")
 
   If wApp(0) = "Yes" Then
      param_explode_Copy = True
   Else
      param_explode_Copy = False
   End If
End Function

'SQ - 28-11-05
'Arrivano istruzioni senza segmento ed escono VALIDE!?
Public Function checkIstruction() As Boolean
  Dim wCheck As Boolean
  Dim i As Long, j As Long
   
  wCheck = True
   
  Select Case TabIstr.IstrType
    Case IMSDC_ISTRUCTION
      If TabIstr.pcb = "" Then wCheck = False
      For i = 1 To UBound(TabIstr.BlkIstr)
        If TabIstr.BlkIstr(i).Record = "" Then
          wCheck = False
          Exit For
        End If
      Next i
      
    Case IMSDB_ISTRUCTION
      If TabIstr.pcb = "" Or TabIstr.DBD = "" Or TabIstr.psbId <= 0 Or TabIstr.psbName = "" Then
        checkIstruction = False
        Exit Function
      End If
      
      For i = 1 To UBound(TabIstr.BlkIstr)
        If TabIstr.DBD_TYPE <> DBD_GSAM Then
          Select Case Trim(TabIstr.istr)
               Case "GU", "GN", "ISRT", "REPL", "GHN", "GNP", "GHU"
                  If TabIstr.BlkIstr(i).IdSegm <= 0 Or TabIstr.BlkIstr(i).Segment = "" Or TabIstr.BlkIstr(i).SegLen = 0 Then
                    checkIstruction = False
                    Exit Function
                  End If
                  If Trim(TabIstr.BlkIstr(i).SSAName) <> "" Then 'And Trim(UCase(TabIstr.BlkIstr(i).SSAName)) <> "<NONE>" Then
                    'SQ 28-11-05: portato fuori!
                    'If TabIstr.BlkIstr(i).IdSegm <= 0 Or TabIstr.BlkIstr(i).segment = "" Or TabIstr.BlkIstr(i).SegLen = "" Then
                    '  checkIstruction = False
                    '  Exit Function
                    'End If
                    For j = 1 To UBound(TabIstr.BlkIstr(i).KeyDef)
                      'If TabIstr.BlkIstr(i).KeyDef(j).KeyVal = "" Then wCheck = False
                      If TabIstr.BlkIstr(i).KeyDef(j).Key = "" Or TabIstr.BlkIstr(i).KeyDef(j).KeyLen = "" Or _
                          TabIstr.BlkIstr(i).KeyDef(j).Op = "" Then
                        checkIstruction = False
                        Exit Function
                      End If
                    Next j
                  End If
               Case "OPEN", "CLSE"
                  If TabIstr.pcb = "" Then wCheck = False
                  'SP corso : per ora.....
                  wCheck = False
               Case "CHKP"
                  wCheck = True 'SG : Da implementare
               Case "ROLB", "ROLL"
                  wCheck = True
               Case "REST"
                  wCheck = True 'SG : Da implementare
               Case Else
                  wCheck = False
        End Select
      End If
         
      If TabIstr.DBD_TYPE = DBD_GSAM Then
            '� un GSAM
            If TabIstr.pcb = "" Then wCheck = False
            If TabIstr.DBD = "" Then wCheck = False
      End If
    Next i
        
  End Select
  checkIstruction = wCheck
End Function

Public Function load_PcbStructure_Template() As t_PcbTemplate
   Dim wApp As t_PcbTemplate
   Dim wFile As String
   Dim nFile As Long
   Dim fLen As Long
   Dim sLine As String
   Dim wPcbIo As Boolean
   Dim wPcbAlt As Boolean
   Dim wPcbStd As Boolean
   Dim s() As String
   Dim Fixed As String
   
   'Apre il file contenente i template
   wFile = m_Fun.FnPathPrd & "\" & SYSTEM_DIR & "\Masks\pcb.msk"
   
   nFile = FreeFile
   Open wFile For Binary As nFile
   
   fLen = FileLen(wFile)
   
   ReDim wApp.IO_Pcb.Details(0)
   ReDim wApp.ALT_Pcb.Details(0)
   ReDim wApp.STD_Pcb.Details(0)
   
   While Loc(nFile) < fLen
      Line Input #nFile, sLine
      
      If Mid(sLine, 1, 1) <> "*" And Trim(sLine) <> "" Then
         If InStr(1, sLine, "<PCB-IO>") > 0 Then wPcbIo = True
         If InStr(1, sLine, "</PCB-IO>") > 0 Then wPcbIo = False
         If InStr(1, sLine, "<PCB-ALT>") > 0 Then wPcbAlt = True
         If InStr(1, sLine, "</PCB-ALT>") > 0 Then wPcbAlt = False
         If InStr(1, sLine, "<PCB-STD>") > 0 Then wPcbStd = True
         If InStr(1, sLine, "</PCB-STD>") > 0 Then wPcbStd = False
         
         s = Split(Trim(sLine), " ")
         If s(0) = "L1" Then
            If InStr(1, sLine, " FIX:") > 0 Then
               Fixed = Replace(s(2), "FIX:", "")
            End If
         End If
         
         'Carica i vari template
         If wPcbIo Then
            If s(0) <> "L1" Then
               ReDim Preserve wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details) + 1)
               wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details)).wSegno = s(1)
               wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details)).wtipo = s(2)
               wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details)).wLunghezza = s(3)
               wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details)).wUsage = s(4)
               wApp.IO_Pcb.FixedLength = Fixed
               
               wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details)).wPicture = Replace(s(1), "N", "") & s(2) & "(" & s(3) & ") " & s(4)
            End If
         End If
         
         If wPcbAlt Then
            If s(0) <> "L1" Then
               ReDim Preserve wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details) + 1)
               wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details)).wSegno = s(1)
               wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details)).wtipo = s(2)
               wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details)).wLunghezza = s(3)
               wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details)).wUsage = s(4)
               wApp.ALT_Pcb.FixedLength = Fixed
               
               wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details)).wPicture = Replace(s(1), "N", "") & s(2) & "(" & s(3) & ") " & s(4)
            End If
         End If
         
         If wPcbStd Then
            If s(0) <> "L1" Then
               ReDim Preserve wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details) + 1)
               wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details)).wSegno = s(1)
               wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details)).wtipo = s(2)
               wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details)).wLunghezza = s(3)
               wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details)).wUsage = s(4)
               wApp.STD_Pcb.FixedLength = Fixed
               
               wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details)).wPicture = Replace(s(1), "N", "") & s(2) & "(" & s(3) & ") " & s(4)
            End If
         End If
      End If
   Wend
   
   Close nFile
 
   load_PcbStructure_Template = wApp
End Function

Public Sub writeCampiBMS(wIdOggetto As Long, wFile As String)
   Dim nFBms As Variant
   Dim wRecIn As String
   Dim wRec As String
   
   Dim wCmp() As t_Cmp_Bms
   Dim aCmp As t_Cmp_Bms
   
   Dim wNomeMapSet As String
   Dim wNomeMap As String
   Dim wStr As String
   Dim firstWord As String
   Dim MacroCmd As String
   Dim wLabel As String
   Dim k1 As Integer
   Dim rs As Recordset
   Dim PosYX() As String
   
   'Da raffinare per ora mi interessano solo i nomi campi
   nFBms = FreeFile
   Open wFile For Input As nFBms
   
   While Not EOF(nFBms)
      wRec = ""
      Line Input #nFBms, wRecIn
      
      If Mid$(wRecIn, 1, 1) <> "*" Then
         firstWord = Trim(wRecIn)
         k1 = InStr(firstWord, " ")
         firstWord = Trim(Mid$(firstWord, 1, k1))
         
         If k1 > 0 Then
            If InStr(1, wRecIn, " DFHMDI ") > 0 Then
               MacroCmd = "DFHMDI"
            ElseIf InStr(1, wRecIn, " DFHMSD ") > 0 Then
               MacroCmd = "DFHMSD"
            ElseIf InStr(1, wRecIn, " DFHMDF ") > 0 Then
               MacroCmd = "DFHMDF"
            Else
               MacroCmd = ""
            End If
         Else
            MacroCmd = ""
         End If
         
         If MacroCmd = "DFHMDF" Then
            'E' un campo, recupera le informazioni
            aCmp.nomeCmp = firstWord
            PosYX() = Split(TrovaParametro(wRecIn, "POS"), ",")
            If Not (UCase(PosYX(0)) = "NONE") Then
              'SQ: 12-12-05
              'POSIZIONAMENTO MONODIMENSIONALE... ES: pos=0, pos=1580...
              'non so se possibile solo con DFHMSD TYPE=DSECT,LANG=ASM...
              If UBound(PosYX) = 0 Then
                'POSIZIONAMENTO MONODIMENSIONALE:
                '0 -> (01,01) ; N == (R-1)*80 + (C-1)
                aCmp.PosY = (PosYX(0) \ 80) + 1
                aCmp.PosX = (PosYX(0) Mod 80) + 1
              Else
                'POSIZIONAMENTO CLASSICO: BIDIMENSIONALE...
                aCmp.PosY = Mid(PosYX(0), 2)
                aCmp.PosX = Mid(PosYX(1), 1, Len(PosYX(1)) - 1)
              End If
            Else
               aCmp.PosY = 0
               aCmp.PosX = 0
            End If
            If Not (UCase(TrovaParametro(wRecIn, "LENGTH")) = "NONE") Then
               aCmp.Length = TrovaParametro(wRecIn, "LENGTH")
            Else
               aCmp.Length = 0
            End If
            
            Set rs = m_Fun.Open_Recordset("Select * From PsDLI_MFSCmp Where Nome = '" & firstWord & "' And idOggetto = " & GbIdOggetto)
            If rs.RecordCount = 0 Then
              rs.AddNew
            End If
            
            rs!idOggetto = GbIdOggetto
            rs!nome = aCmp.nomeCmp
            'TMP
            rs!DevType = "-"
            rs!Riga = aCmp.PosY
            rs!Colonna = aCmp.PosX
            rs!Lunghezza = aCmp.Length
            
            rs.Update
            
            rs.Close
         End If
     End If
   Wend
   
   Close nFBms
  
End Sub
Public Function CoppiaTastoValore(pwCoppia As String, ByRef tasto As String, ByRef Valore As String) As Boolean
    CoppiaTastoValore = False
    If InStr(2, pwCoppia, "=") > 0 Then
           tasto = Mid(pwCoppia, 1, InStr(2, pwCoppia, "=") - 1)
           Valore = Mid(pwCoppia, InStr(2, pwCoppia, "=") + 1, Len(pwCoppia) - InStr(2, pwCoppia, "="))
           If tasto = "'" Then Exit Function
           
           If Left(Valore, 1) = "'" Then
              Valore = Right(Valore, Len(Valore) - 1)
           End If
           If Right(Valore, 1) = "'" Then
              Valore = Left(Valore, Len(Valore) - 1)
           End If
           CoppiaTastoValore = True
    End If

End Function
Public Function Analizza_PFK(Linea As String) As String
   Dim App As String
   
   'TYPE
   App = get_Parametro_MFS(Linea, "PFK", ")")
   
   If Trim(App) <> "" Then
      App = Replace(App, "(", "")
      App = Replace(App, ")", "")
      
      If Mid(Linea, Len(Linea), 1) = "," Then
         App = Mid(App, 1, Len(Linea) - 1)
      End If
      
      
      Analizza_PFK = Trim(App)
   End If
End Function
Public Function ControllaAttributo(pAttributi As String) As String
    ' converte tutti gli attributi che non hanno un corrispondente in cics
    Dim pwattributo As String
    pwattributo = pAttributi
    If Left(pwattributo, 1) = "(" Then
        pwattributo = Mid(pwattributo, 2, Len(pwattributo) - 2)
    End If
    'HI,NODISP
    pwattributo = Replace(pwattributo, "HI", "BRT")
    pwattributo = Replace(pwattributo, "NODISP", "DRK")
    ControllaAttributo = pwattributo
     
End Function
Public Function get_Parametro_MFS(Linea As String, VoceParam As String, ChStop As String)
   Dim Idx As Long
   Dim idxStop As Long
   Dim App As String
   
   Idx = InStr(1, Linea, VoceParam & "=")
   
   If Idx = 0 Then
      Idx = InStr(1, Linea, VoceParam)
   End If
   
   If Idx > 0 Then
      idxStop = InStr(Idx, Linea, ChStop)
      
      If idxStop > 0 Then
         App = Mid(Linea, Idx + Len(VoceParam) + 1, idxStop - Idx - Len(VoceParam))
      Else
         idxStop = InStr(Idx, Linea, " ")
         
         If idxStop > 0 Then
            App = Mid(Linea, Idx + Len(VoceParam) + 1, idxStop - Idx - Len(VoceParam))
         End If
      End If
   End If
   
   'App = Replace(App, ",", "")
   
   get_Parametro_MFS = App
End Function

Public Function Restituisci_DatiKey(idField As Long, xName As Boolean, TipoRestituito As String) As Variant
  Dim r As Recordset
  
  If idField < 0 Then
    Set r = m_Fun.Open_Recordset("Select * From PSDli_XDField Where IdXDField = " & Abs(idField))
  Else
    Set r = m_Fun.Open_Recordset("Select * From PSDli_Field Where IdField = " & idField)
  End If
  
  If r.RecordCount Then
    Select Case Trim(UCase(TipoRestituito))
      Case "NOME"
        If xName = False Then
          Restituisci_DatiKey = r!nome
        Else
          Restituisci_DatiKey = r!Ptr_xNome
        End If
      Case "LUNGHEZZA"
        Restituisci_DatiKey = r!Lunghezza
    End Select
    r.Close
  Else
    If TipoRestituito = "LUNGHEZZA" Then Restituisci_DatiKey = 0
    If TipoRestituito = "NOME" Then Restituisci_DatiKey = ""
  End If
End Function
'SQ: 25-11-05
'True se:
' - stringa senza bianchi a colonna 8, terminante con un punto
Public Function isCobolLabel(line As String) As Boolean
  Dim i As Integer
  i = InStr(8, line, " ")
  If i Then isCobolLabel = Mid(line, InStr(8, line, " ") - 1, 1) = "." 'linea di 80 caratteri
  
  'SQ: rapina temporanea:
  If Not isCobolLabel Then
    'END-IF a colonna 12!
    isCobolLabel = Mid(line, 12, 6) = "END-IF"
  End If
End Function

Public Function EliminaApici(source As String) As String
Dim ret As String
  ret = source
  If Left(ret, 1) = "'" Then
    ret = Right(ret, Len(ret) - 1)
  End If
  If Right(ret, 1) = "'" Then
    ret = Left(ret, Len(ret) - 1)
  End If
  EliminaApici = ret
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Rendere definitiva... unificando le attivit� per tutte le tipologie
' - Ricerca oggetto
' - Eventuale riclassificazione
' - Eventuale parsing
' - Inserimento Relazioni/Segnalazioni
' Effetti collaterali:
' - setta IdOggettoRel
' - setta IsParseNeeded (se parseNeedes=true e oggetto mai parserato)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub checkRelations(item As String, relType As String)
  Dim rs As Recordset
  Dim itemType As String, itemTypes As String, wrongTypes As String
  Dim PrevName As String
  Dim IsPGM As Boolean
  
  item = Replace(item, "'", "")
  isDliCall = False
  Select Case relType
    Case "CALL", "CALLNAT", "NATFETCH"
      itemTypes = "'CBL','ASM','PLI','NAT','QIM'"
      wrongTypes = "'CPY','INC','UNK'"
      itemType = GbTipoInPars '� sempre corretto? Allora usarlo in tutti i casi!!
      '(item: programma chiamato)
      'item puo' essere Nome variabile...
      If Not SwNomeCall Then
        ' Mauro 13/01/2009 : Se � una variabile, cerco il valore corrispondente
        itemValue = PrendiValue(item)
        If itemValue = "None" Then
          ' Mauro 08/10/2009: Aggiunta SX000M0 per GRZ
          ' Mauro 19/10/2009: Aggiunte CSSTDLI e CSSTDLIC per UBS
          ' AC    01/03/2010: Aggiunto AERTDLI per i grandi magazzini WM
          If item <> "CBLTDLI" Or item <> "AIBTDLI" Or item <> "PLITDLI" Or item <> "PLIHSSR" Or item <> "SX000M0" Or _
             item <> "CSSTDLI" Or item <> "CSSTDLIC" Or item <> "SIXTDLI" Or item <> "AERTDLI" Or item <> "DBINTDLI" Then
            itemValue = Replace(item, "'", "")
          End If
        End If
      Else
        ' Mauro 08/10/2009: Aggiunta SX000M0 per GRZ
        ' Mauro 19/10/2009: Aggiunte CSSTDLI e CSSTDLIC per UBS
        ' AC    01/03/2010: Aggiunto AERTDLI per i grandi magazzini WM
        'Ottimizzazione: diamo per buone le seguenti variabili:
        If item = "CBLTDLI" Or item = "AIBTDLI" Or item = "PLITDLI" Or item = "PLIHSSR" Or item = "SX000M0" Or _
           item <> "CSSTDLI" Or item <> "CSSTDLIC" Or item <> "SIXTDLI" Or item <> "AERTDLI" Or item <> "DBINTDLI" Then
          itemValue = item
        Else
          ''''''''''''''''''''
          'SQ 19-12-06
          ' Prova solo per NATURAL... decidere se mettere in linea...
          ''''''''''''''''''''
          If GbTipoInPars = "NAT" Or GbTipoInPars = "NIN" Then
            Dim itemValues() As String
            'Non segue le "dependencies"...
            itemValues = getFieldValues(item)
            If UBound(itemValues) Then
              itemValue = Trim(Replace(itemValues(1), "'", "")) 'TMP: Teniamo solo il primo
            Else
              itemValue = ""
            End If
          Else
            itemValue = getFieldValue(item)
          End If
          If Len(itemValue) = 0 Then
            addSegnalazione item, "NO" & relType & "VALUE", item '"NOCALL": dinamico, per eventulmente accorpare questa routine...
            Exit Sub
          End If
        End If
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''
      ' Gestione "ALIAS" per prg d'interfaccia ad IMS:
      ' Es. Sungard: "INMCMIO"... prg centralizzato per
      ' invio mappe... i parametri sono gli stessi...
      ' SQ - ... implementare! Per ora scamuffo!
      '''''''''''''''''''''''''''''''''''''''''''''''''
      If itemValue = "INMCMIO" Then
        SwDli = True        'programma DLI
        isDliCall = True    'istruzione DLI: serve al chiamante (e basta)
      End If
      
      Select Case itemValue
        ' Mauro 08/10/2009: Aggiunta SX000M0 per GRZ
        ' Mauro 19/10/2009: Aggiunte CSSTDLI e CSSTDLIC per UBS
        ' AC 01/03/10  AERTDLI per WM
        Case "CBLTDLI", "AIBTDLI", "PLITDLI", "PLIHSSR", "SX000M0", "CSSTDLI", "CSSTDLIC", "SIXTDLI", "AERTDLI", "DBINTDLI"
          SwDli = True        'programma DLI
          isDliCall = True    'istruzione DLI: serve al chiamante (e basta)
        Case "ISPLINK"
          'Flag ISPF!
          '...
        Case Else
          ''''''''''''''''''''''''''''''''''
          ' Gestione PsLoad
          ''''''''''''''''''''''''''''''''''
          itemValue = Replace(itemValue, "'", "")
          Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                        "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                        "order by Nome, idoggetto")
          If rs.RecordCount = 0 Then
            'Nome LOAD diverso da Nome SOURCE: aggiorniamo il recordset
            Set rs = m_Fun.Open_Recordset("select a.Nome, a.idOggetto from bs_oggetti as a,PsLoad as b where " & _
                                          "b.load = '" & itemValue & "' and b.source=a.Nome and " & _
                                          "a.tipo in (" & itemTypes & ") order by a.Nome, a.idoggetto")
          End If
'          'RICLASSIFICAZIONE
'          If rs.RecordCount = 0 Then
'            Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ")")
'            If rs.RecordCount Then
'              rs!Tipo = itemType
'              rs!Area_Appartenenza = "SOURCE"
'              rs!livello1 = "COBOL-CBL"
'              rs.Update
'            End If
'          End If
          If rs.RecordCount Then
            ' Mauro 20/01/2009
'            If rs.RecordCount = 1 Then
'              AggiungiRelazione rs!idOggetto, relType, itemValue, itemValue
'            Else
'              'verificare 'sta storia...
'              While Not rs.EOF
'                'Inserisce le relazioni come duplicate nella tabella
'                AggiungiRelazione rs!idOggetto, relType, itemValue, itemValue, True
'                rs.MoveNext
'              Wend
'            End If
            ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
            While Not rs.EOF
              AggiungiRelazione rs!idOggetto, relType, itemValue, itemValue, PrevName = rs!nome
              PrevName = rs!nome
              rs.MoveNext
            Wend
          Else
            If Not ExistObjSyst(itemValue, "PGM") Then
              If SwNomeCall Then
                addSegnalazione itemValue, "NO" & relType, itemValue
              Else
                addSegnalazione itemValue, "NO" & relType & "VALUE", itemValue
              End If
            'SQ Ci vuole!!!! Mi perdo, per esempio, le DSNTIAUL da IKJEFT01!...
            Else
              AggiungiComponente itemValue, GbNomePgm, "CAL", "SYSTEM"
            End If
          End If
          rs.Close
      End Select
    Case "XCTL", "LOAD", "LINK"  'LOAD?
      itemTypes = "'CBL','ASM','PLI','NAT','QIM'"
      '(item: pgm utilizzato)
      'item puo' essere Nome variabile...
      If SwNomeCall Then
        itemValue = item
      Else
        itemValue = PrendiValue(item)
        If itemValue = "None" Or Len(itemValue) = 0 Or itemValue = "SPACE" Or itemValue = "SPACES" Then
          addSegnalazione item, "NO" & relType & "VALUE", item 'Aggiungere Bs_messaggi per: "NO" & relType
          Exit Sub
        End If
      End If

      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      If rs.RecordCount = 0 Then
        'Nome LOAD diverso da Nome SOURCE: aggiorniamo il recordset
        Set rs = m_Fun.Open_Recordset("select a.Nome, a.idOggetto from bs_oggetti as a,PsLoad as b where " & _
                                      "b.load = '" & itemValue & "' and b.source=a.Nome and " & _
                                      "tipo in (" & itemTypes & ") order by a.Nome, a.idoggetto")
      End If
      If rs.RecordCount Then
        ' Mauro 20/01/2009
'        rs.MoveLast
'        If rs.RecordCount = 1 Then
'          AggiungiRelazione rs!idOggetto, relType, itemValue, itemValue
'        End If
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione rs!idOggetto, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
      Else
        If relType <> "LOAD" Then
          If Not ExistObjSyst(itemValue, "PGM") Then
            If SwNomeCall Then
              addSegnalazione itemValue, "NO" & relType, itemValue
            Else
              addSegnalazione itemValue, "NO" & relType & "VALUE", itemValue
            End If
          'SQ Ci vuole!!!! Mi perdo, per esempio, le DSNTIAUL da IKJEFT01!...
          Else
            AggiungiComponente itemValue, GbNomePgm, "CAL", "SYSTEM"
          End If
        End If
      End If
    Case "MAC", "COPY-ASM"
      itemValue = item
      itemType = "MAC"
      itemTypes = "'MAC'"
      wrongTypes = "'ASM','UNK'"
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where " & _
                                      "Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ") " & _
                                      "order by Nome, idoggetto")
        If rs.RecordCount Then
          rs!Tipo = itemType
          'SQ - Farlo per tutti... ma non smartellato!
          rs!Area_Appartenenza = "SOURCE"
          rs!livello1 = "ASS-COPY"
          rs.Update
        End If
      End If
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto  'serve al getFD
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ: 09-02-06 CONTROLLARE...
        'If rs.RecordCount = 1 Then
        '  AggiungiRelazione idOggettoRel, "COPY", item, item
        'Else
        ' Mauro 20/01/2009
'        While Not rs.EOF
'          'Inserisce le relazioni come duplicate nella tabella
'          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue ''''''', True
'          rs.MoveNext
'        Wend
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
        'End If
      Else
        If Not ExistObjSyst(itemValue, relType) Then
          If SwNomeCall Then
            addSegnalazione itemValue, "NO" & relType, itemValue
          Else
            addSegnalazione itemValue, "NO" & relType & "VALUE", itemValue
          End If
        'SQ Ci vuole!!!! Mi perdo, per esempio, le DSNTIAUL da IKJEFT01!...
        Else
          AggiungiComponente itemValue, GbNomePgm, "CAL", "SYSTEM"
        End If

      End If
      rs.Close
    Case "COPY"
      itemValue = item
      itemTypes = "'CPY'"
      wrongTypes = "'CBL','UNK','LOA'"  'X ALBY: chi altro chiama questa routine con "COPY"?
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      '''''''''''''''''''''
      'X ALBY DA VERIFICARE...
      '''''''''''''''''''''
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where " & _
                                      "Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ") " & _
                                      "order by Nome, idoggetto")
        If rs.RecordCount Then
          rs!Tipo = "CPY"
          'SQ - Farlo per tutti... ma non smartellato!
          rs!Area_Appartenenza = "SOURCE"
          rs!livello1 = "COBOL-CPY"
          rs.Update
        End If
      End If
      '''''''''''''''''''''
      '''''''''''''''''''''
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto  'serve al getFD
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ: 09-02-06 CONTROLLARE...
        'If rs.RecordCount = 1 Then
        '  AggiungiRelazione idOggettoRel, "COPY", item, item
        'Else
        ' Mauro 20/01/2009
'        While Not rs.EOF
'          'Inserisce le relazioni come duplicate nella tabella
'          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue ''''''', True
'          rs.MoveNext
'        Wend
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
        'End If
      Else
        If Not ExistObjSyst(itemValue, relType) Then
          'If SwNomeCall Then
            addSegnalazione itemValue, "NO" & relType, itemValue
          'Else
          '  addSegnalazione itemValue, "NO" & relType & "VALUE", itemValue
          'End If
        'SQ Ci vuole!!!! Mi perdo, per esempio, le DSNTIAUL da IKJEFT01!...
        Else
          AggiungiComponente itemValue, GbNomePgm, "CAL", "SYSTEM"
        End If
      End If
      rs.Close
    Case "INCLUDE"  'SQL
      itemValue = item
      itemTypes = "'INC','CPY'"
      
      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        '''''''''''''''''''''''''''''''''''''''
        wrongTypes = "'CBL','UNK','LOA'"  'X ALBY: chi altro chiama questa routine con "COPY"?
        IdOggettoRel = 0  'init
        '''''''''''''''''''''''''''''''''''''''
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where " & _
                                      "Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ") " & _
                                      "order by Nome, idoggetto")
        If rs.RecordCount Then
          rs!Tipo = "CPY"
          'SQ - Farlo per tutti... ma non smartellato!
          rs!Area_Appartenenza = "SOURCE"
          rs!livello1 = "COBOL-CPY"
          rs.Update
        End If
      End If
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto
        ' Mauro 20/01/2009
'        If rs.RecordCount = 1 Then
'          AggiungiRelazione IdOggettoRel, "INCLUDE", itemValue, itemValue
'        Else
'          While Not rs.EOF
'            'Inswerisce le relazioni come duplicate nella tabella
'            AggiungiRelazione IdOggettoRel, "INCLUDE", itemValue, itemValue, True
'            rs.MoveNext
'          Wend
'        End If
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
      Else
        If Not ExistObjSyst(itemValue, "CPY") Then
'Stefano in i-4: nelle include SQL non ha senso!!!!
      '    If SwNomeCall Then
            addSegnalazione itemValue, "NOINCLUDE", itemValue
      '    Else
      '      addSegnalazione itemValue, "NOINCLUDEVALUE", itemValue
      '    End If
        'SQ Ci vuole!!!! Mi perdo, per esempio, le DSNTIAUL da IKJEFT01!...
        Else
          AggiungiComponente itemValue, GbNomePgm, "CAL", "SYSTEM"
        End If
      End If
    Case "MFS"
      itemTypes = "'MFS'"
      Set rs = m_Fun.Open_Recordset("select idOggetto from bs_oggetti where Nome = '" & item & "' and tipo in (" & itemTypes & ")")
      If Not rs.EOF Then
'        IdOggettoRel = rs!idOggetto  'Siamo nel parsing II: IdOggettoRel NON deve essere modificato
'        AggiungiRelazione IdOggettoRel, "MFS", item, item 'virgilio
        AggiungiRelazione rs!idOggetto, "MFS", item, item
      Else ' cerca msg di output con quel Nome
        rs.Close
        Set rs = m_Fun.Open_Recordset("select idOggetto from PsDLI_mfsMSG where MSGName = '" & item & "' and DevType = 'OUTPUT'")
        If Not rs.EOF Then
'          AggiungiRelazione IdOggettoRel, "MFS", item, item 'virgilio
          AggiungiRelazione rs!idOggetto, "MFS", item, item
        Else
          ' RELAZIONE SCONOSCIUTA
          AggiungiObjUnk GbIdOggetto, item, "MFS", item
          addSegnalazione item, "NOMAPMODNAME", item
        End If
      End If
      rs.Close
    Case "MAPSET"
      itemTypes = "'BMS'"
      '(item: mapset utilizzato)
      'item puo' essere Nome variabile...
      If SwNomeCall Then
        itemValue = item
      Else
        itemValue = PrendiValue(item)
        If itemValue = "None" Or Len(itemValue) = 0 Or itemValue = "SPACE" Or itemValue = "SPACES" Then
          addSegnalazione item, "NO" & relType & "VALUE", item
          Exit Sub
        End If
      End If
      
      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto  'potrebbe servire a qualcuno...
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ: 09-02-06 CONTROLLARE...
        'If rs.RecordCount = 1 Then
        '  AggiungiRelazione idOggettoRel, "COPY", item, item
        'Else
        ' Mauro 20/01/2009
'          While Not rs.EOF
'            'Inserisce le relazioni come duplicate nella tabella
'            AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue ''''''', True
'            rs.MoveNext
'          Wend
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
        'End If
      Else
        addSegnalazione itemValue, "NO" & relType, itemValue
      End If
      rs.Close
      
      'SQ: originale...?
'      Set tbComponenti = m_Fun.Open_Recordset("select * from PsCom_Obj where relazione = 'MPS' and nomeoggettor = '" & itemValue & "'")
'      If tbComponenti.RecordCount > 0 Then
'        tbComponenti.MoveLast
'        If rs.RecordCount = 1 Then
'          ''''''AggiungiRelazione rs!idOggetto, relType, Wstr2, Wstr2 'ATTENZIONE: era la mappa, non il mapname!
'        End If
'      Else
'        AggiungiSegnalazione itemValue, "NO" & relType, itemValue
'      End If
    Case "PANEL"
      itemType = "ISP"
      itemTypes = "'ISP'"
      itemValue = item
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select idOggetto,parsingLevel from bs_oggetti where Nome = '" & itemValue & "' and tipo in (" & itemTypes & ")")
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto
        IsParseNeeded = rs!parsinglevel = 0
        While Not rs.EOF
          addRelazione IdOggettoRel, relType, itemValue, itemValue
          rs.MoveNext
        Wend
      Else
        IsParseNeeded = False
        'If Not ExistObjSyst(name, relType) Then 'TABELLA ELEMENTI AGGIUNTI... DIREI CHE NON SERVE PER I MEBRI...
         addSegnalazione itemValue, "NO" & relType, itemValue  'IL TERZO PARAMETRO COS'E'?????????
        'Else
        ' AggiungiComponente ...
        'End If
      End If
      rs.Close
    Case "PLI-INCLUDE"  '2-11-06
      itemType = "INC"
      itemTypes = "'INC'"
      wrongTypes = "'PLI','UNK'"
      itemValue = item
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select idOggetto,parsingLevel from bs_oggetti where Nome = '" & Replace(itemValue, "'", "") & "' and tipo in (" & itemTypes & ")")
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where Nome = '" & Replace(itemValue, "'", "") & "' AND tipo in (" & wrongTypes & ")")
        If rs.RecordCount Then
          rs!Tipo = itemType
          rs!Area_Appartenenza = "SOURCE"
          rs!livello1 = "PLI-INCLUDE"
          rs.Update
        End If
      End If
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto
        IsParseNeeded = rs!parsinglevel = 0
        While Not rs.EOF
          addRelazione IdOggettoRel, relType, itemValue, itemValue
          rs.MoveNext
        Wend
      Else
        IsParseNeeded = False
        'IRIS-DB: test su oggetti di sistema reinserito in quanto esistono include di sistema anche in PLI
        If Not ExistObjSyst(itemValue, relType) Then 'TABELLA ELEMENTI AGGIUNTI... DIREI CHE NON SERVE PER I MEBRI...
          addSegnalazione itemValue, "NO" & relType, itemValue  'IL TERZO PARAMETRO COS'E'?????????
        'Else
        ' AggiungiComponente ...
        End If
      End If
      rs.Close
    Case "NAT-INCLUDE"  '6-12-06
      itemType = "NIN"
      itemTypes = "'NIN'"
      wrongTypes = "'NAT','UNK'"
      itemValue = item
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select idOggetto,parsingLevel from bs_oggetti where Nome = '" & itemValue & "' and tipo in (" & itemTypes & ")")
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ")")
        If rs.RecordCount Then
          rs!Tipo = itemType
          'SQ - Farlo per tutti... ma non smartellato!
          rs!Area_Appartenenza = "SOURCE"
          rs!livello1 = "NAT-INCLUDE"
          rs.Update
        End If
      End If
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto
        IsParseNeeded = rs!parsinglevel = 0
        While Not rs.EOF
          addRelazione IdOggettoRel, relType, itemValue, itemValue
          rs.MoveNext
        Wend
      Else
        IsParseNeeded = False
        'If Not ExistObjSyst(name, relType) Then 'TABELLA ELEMENTI AGGIUNTI... DIREI CHE NON SERVE PER I MEBRI...
         addSegnalazione itemValue, "NO" & relType, itemValue  'IL TERZO PARAMETRO COS'E'?????????
        'Else
        ' AggiungiComponente ...
        'End If
      End If
      rs.Close
    Case "MEMBER"  '6-12-06
      itemType = "MBR"
      itemTypes = "'MBR'"
      wrongTypes = "'PRC','UNK'"
      itemValue = item
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select idOggetto,parsingLevel from bs_oggetti where Nome = '" & itemValue & "' and tipo in (" & itemTypes & ")")
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ")")
        If rs.RecordCount Then
          rs!Tipo = itemType
          'SQ - Farlo per tutti... ma non smartellato!
          rs!Area_Appartenenza = "COMMAND"
          rs!livello1 = "MEMBER"
          rs.Update
        End If
      End If
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto
        IsParseNeeded = rs!parsinglevel = 0
        While Not rs.EOF
          addRelazione IdOggettoRel, relType, itemValue, itemValue
          rs.MoveNext
        Wend
      Else
        IsParseNeeded = False
        'If Not ExistObjSyst(name, relType) Then 'TABELLA ELEMENTI AGGIUNTI... DIREI CHE NON SERVE PER I MEBRI...
         addSegnalazione itemValue, "NO" & relType, itemValue  'IL TERZO PARAMETRO COS'E'?????????
        'Else
        ' AggiungiComponente ...
        'End If
      End If
      rs.Close
    Case "EZM"
      itemValue = item
      itemType = "EZM"
      itemTypes = "'EZM'"
      wrongTypes = "'EZT'"
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where " & _
                                      "Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ") " & _
                                      "order by Nome, idoggetto")
        If rs.RecordCount Then
          rs!Tipo = itemType
          'SQ - Farlo per tutti... ma non smartellato!
          rs!Area_Appartenenza = "COMMAND"
          rs!livello1 = "EZ MACRO"
          rs.Update
        End If
      End If
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto  'serve al getFD
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ: 09-02-06 CONTROLLARE...
        'If rs.RecordCount = 1 Then
        '  AggiungiRelazione idOggettoRel, "COPY", item, item
        'Else
        ' Mauro 20/01/2009
'        While Not rs.EOF
'          'Inserisce le relazioni come duplicate nella tabella
'          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue ''''''', True
'          rs.MoveNext
'        Wend
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
        'End If
      Else
        If Not ExistObjSyst(itemValue, relType) Then
          If SwNomeCall Then
            addSegnalazione itemValue, "NO" & relType, itemValue
          Else
            addSegnalazione itemValue, "NO" & relType & "VALUE", itemValue
          End If
        'SQ Ci vuole!!!! Mi perdo, per esempio, le DSNTIAUL da IKJEFT01!...
        Else
          AggiungiComponente itemValue, GbNomePgm, "CAL", "SYSTEM"
        End If
      End If
      rs.Close
    Case "EZT-PGM", "EZT" 'SQ 25-05-07 (serve anche EZT?)
      itemValue = item
      itemType = "EZT"
      itemTypes = "'EZT'"
      wrongTypes = "'EZM','UNK'"
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where " & _
                                      "Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ") " & _
                                      "order by Nome, idoggetto")
        If rs.RecordCount Then
          rs!Tipo = itemType
          'SQ - Farlo per tutti... ma non smartellato!
          rs!Area_Appartenenza = "COMMAND"
          rs!livello1 = "EZ PGM"
          rs.Update
        End If
      End If
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto  'serve al getFD
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ: 09-02-06 CONTROLLARE...
        'If rs.RecordCount = 1 Then
        '  AggiungiRelazione idOggettoRel, "COPY", item, item
        'Else
        ' Mauro 20/01/2009
'        While Not rs.EOF
'          'Inserisce le relazioni come duplicate nella tabella
'          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue ''''''', True
'          rs.MoveNext
'        Wend
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
        'End If
      Else
        If Not ExistObjSyst(itemValue, relType) Then
          If SwNomeCall Then
            addSegnalazione itemValue, "NO" & relType, itemValue
          Else
            addSegnalazione itemValue, "NO" & relType & "VALUE", itemValue
          End If
        'SQ Ci vuole!!!! Mi perdo, per esempio, le DSNTIAUL da IKJEFT01!...
        Else
          AggiungiComponente itemValue, GbNomePgm, "CAL", "SYSTEM"
        End If
      End If
      rs.Close
    'T X SAS
    Case "SAS-PGM", "SAS"
      itemValue = item
      itemType = "SAS"
      itemTypes = "'SAS'"
      wrongTypes = "'SAS','UNK'"
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where " & _
                                      "Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ") " & _
                                      "order by Nome, idoggetto")
        If rs.RecordCount Then
          rs!Tipo = itemType
          'SQ - Farlo per tutti... ma non smartellato!
          rs!Area_Appartenenza = "COMMAND"
          rs!livello1 = "SAS PGM"
          rs.Update
        End If
      End If
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto  'serve al getFD
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
        'End If
      Else
        If Not ExistObjSyst(itemValue, relType) Then
          If SwNomeCall Then
            addSegnalazione itemValue, "NO" & relType, itemValue
          Else
            addSegnalazione itemValue, "NO" & relType & "VALUE", itemValue
          End If
        'SQ Ci vuole!!!! Mi perdo, per esempio, le DSNTIAUL da IKJEFT01!...
        Else
          AggiungiComponente itemValue, GbNomePgm, "CAL", "SYSTEM"
        End If
      End If
      rs.Close
       
    
    Case "NAT-COPY"
      itemValue = item
      itemType = "NCP"
      itemTypes = "'NCP'"
      wrongTypes = "'NAT','NIN','UNK'"
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where " & _
                                      "Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ") " & _
                                      "order by Nome, idoggetto")
        If rs.RecordCount Then
          rs!Tipo = itemType
          rs!Area_Appartenenza = "SOURCE"
          rs!livello1 = "NAT-COPY"
          rs.Update
        End If
      End If
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto  'serve al getFD
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ: 09-02-06 CONTROLLARE...
        'If rs.RecordCount = 1 Then
        '  AggiungiRelazione idOggettoRel, "COPY", item, item
        'Else
        ' Mauro 20/01/2009
'        While Not rs.EOF
'          'Inserisce le relazioni come duplicate nella tabella
'          addRelazione IdOggettoRel, relType, itemValue, itemValue ''''''', True
'          rs.MoveNext
'        Wend
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
        'End If
      Else
        'If Not ExistObjSyst(item, relType) Then
          addSegnalazione item, "NO" & relType, itemValue
        'End If
      End If
      rs.Close
    Case "DBD-REL"
      'SQ - Andare sulle tabelle!
      itemValue = item
      itemType = "DBD"
      itemTypes = "'DBD'"
      wrongTypes = ""
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      If Len(wrongTypes) Then
        'RICLASSIFICAZIONE
        If rs.RecordCount = 0 Then
          Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where " & _
                                        "Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ") " & _
                                        "order by Nome, idoggetto")
          If rs.RecordCount Then
            rs!Tipo = itemType
            rs!Area_Appartenenza = "SOURCE"
            rs!livello1 = "NAT-COPY"
            rs.Update
          End If
        End If
      End If
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto  'serve al getFD
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ: 09-02-06 CONTROLLARE...
        'If rs.RecordCount = 1 Then
        '  AggiungiRelazione idOggettoRel, "COPY", item, item
        'Else
        ' Mauro 20/01/2009
'        While Not rs.EOF
'          'Inserisce le relazioni come duplicate nella tabella
'          addRelazione IdOggettoRel, relType, itemValue, itemValue ''''''', True
'          rs.MoveNext
'        Wend
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
        'End If
      Else
        'If Not ExistObjSyst(item, relType) Then
          addSegnalazione item, "NO" & relType, itemValue
        'End If
      End If
      rs.Close
    Case "SAS-EXEC"
      itemValue = item
      itemType = "SAS"
      itemTypes = "'SAS'"
      wrongTypes = "'UNK','PRM','EZT'"  'EZT!!!
      IdOggettoRel = 0  'init
      Set rs = m_Fun.Open_Recordset("select Nome, idOggetto from bs_oggetti where " & _
                                    "Nome = '" & itemValue & "' and tipo in (" & itemTypes & ") " & _
                                    "order by Nome, idoggetto")
      'RICLASSIFICAZIONE
      If rs.RecordCount = 0 Then
        Set rs = m_Fun.Open_Recordset("select * from Bs_oggetti where " & _
                                      "Nome = '" & itemValue & "' AND tipo in (" & wrongTypes & ") " & _
                                      "order by Nome, idoggetto")
        If rs.RecordCount Then
          rs!Tipo = itemType
          rs!Area_Appartenenza = "SOURCE"
          rs!livello1 = "SAS"
          rs.Update
        End If
      End If
      If rs.RecordCount Then
        IdOggettoRel = rs!idOggetto  'serve al getFD
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ: 09-02-06 CONTROLLARE...
        'If rs.RecordCount = 1 Then
        '  AggiungiRelazione idOggettoRel, "COPY", item, item
        'Else
        ' Mauro 20/01/2009
'        While Not rs.EOF
'          'Inserisce le relazioni come duplicate nella tabella
'          addRelazione IdOggettoRel, relType, itemValue, itemValue ''''''', True
'          rs.MoveNext
'        Wend
        ' Se il Nome � uguale al precedente, lo mette nella PsRel_Objdup
        While Not rs.EOF
          AggiungiRelazione IdOggettoRel, relType, itemValue, itemValue, PrevName = rs!nome
          PrevName = rs!nome
          rs.MoveNext
        Wend
        'End If
      Else
        'If Not ExistObjSyst(item, relType) Then
          addSegnalazione item, "NO" & relType, itemValue
        'End If
      End If
      rs.Close
    Case Else
      '...
  End Select
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Parsing di II livello
' Analizza le istruzioni IMS che trova in PsDli_Istruzioni
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseCbl_II(Optional pRiga As Long = -1, Optional pIdOggetto As Long = -1, Optional pIdPgm As Long = -1)
  Dim istruzione As String, parametri As String
  Dim rs As Recordset
  Dim rsCall As Recordset
  Dim rsOggetti As Recordset 'IRIS-DB
  Dim OptWhereCond As String
  Dim DBDOutOfScope As Boolean 'IRIS-DB
  Dim dbdname As String 'IRIS-DB
  
  On Error GoTo parseErr
    
  ReDim idAree(0)
  ReDim ssaCache(0)
  ReDim wPcb(0)
  ReDim xPcb(0)
  ReDim fSegm(0)
  'SQ 28-02-07
  ReDim DataValues(0)
  'SQ 9-11-06
  ReDim CampiMove(0)
  'SQ 16-10-07 (errore sui PSB: riga "sporca")
  GbOldNumRec = 0
  
  If pRiga = -1 Then
    OptWhereCond = ""
  Else
    OptWhereCond = " and Riga = " & pRiga
    'SQ CPY-ISTR 09/05/2008
    'GbIdOggetto = pIdOggetto
    IdOggettoRel = pIdOggetto
    GbIdOggetto = pIdPgm
    GbappoIdPgm = pIdPgm
    GbOldNumRec = pRiga
  End If
  
  '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ' SQ - chiarire/sistemare
  '
  'Perch� le facciamo senza sapere se esistono
  'effettivamente istruzioni? (attenzione per� a quelle in COPY!)
  'Naming Convention: Nome segmento/Nome area
  segmentNameParam = LeggiParam("SEGM-NAME-CRITERIA")
  
  'Portare fuori!
  Dim vPam As Variant
  vPam = m_Fun.RetrieveParameterValues("PS_FORCE_DLET_REPL")
  
  ''''''''''''''''''''''''''''''''
  ' PSB Schedulati
  ' tmp: valorizza IdPsb (uno solo) come nel vecchio giro...)
  ''''''''''''''''''''''''''''''''
  getPsb_program
  ' Mauro 07-05-2007 : Spostato per gestione istruzioni in COPY
  gbCurEntryPCB = getENTRY(GbIdOggetto)
  '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  If pIdPgm = -1 Then
    GbappoIdPgm = -1
    'IRIS-DB: gets the idpgm too, otherwise it would be using the former one
    Set rs = m_Fun.Open_Recordset( _
            "Select idPgm,idOggetto,NumPcb,OrdPcb,isExec,istruzione,statement,Riga,valida,ImsDB,To_Migrate from PsDLI_Istruzioni where " & _
            "idOggetto=" & GbIdOggetto & OptWhereCond)
    If rs.RecordCount Then IdOggettoRel = rs!idpgm 'IRIS-DB
  Else
    GbappoIdPgm = pIdPgm
    Set rs = m_Fun.Open_Recordset( _
            "Select idOggetto,NumPcb,ordPcb,isExec,istruzione,statement,Riga,valida,ImsDb,To_Migrate from PsDLI_Istruzioni where " & _
            "idPgm = " & pIdPgm & " and idOggetto=" & pIdOggetto & OptWhereCond)
  End If
  'Set rs = m_Fun.Open_Recordset("Select a.istruzione,a.statement,a.Riga from PsDLI_Istruzioni as a,PsCall as b where a.idOggetto=b.idOggetto and a.riga=b.riga and b.idOggetto=" & GbIdOggetto & " and (b.Programma='AIBTDLI' or b.Programma='CBLTDLI')")
  
  If rs.RecordCount Then
    If SwPsbAssociato Then
      IdPsb = psbPgm(0)
      'warning se piu' di uno:
      'AggiungiSegnalazione " ", "MANYRELPSB", " "
    Else
      addSegnalazione " ", "NORELPSB", " "
    End If
    '''''''SOLO CALL!!!!!!!!!!!!!!!!!!!!!!!
    ' ENTRY:
    ' gbCurEntryPCB: array globale con i PCB...
    ' (se globale perche' usiamo una function?)
    ''''''''''''''''''''''''''''''''
    'gbCurEntryPCB = CaricaEntry(GbIdOggetto)  'ci mette una vita!!!
    'Modificato... toglieva l'I-O e altre cose...
    ' Mauro 07-05-2007 : Spostato per gestione istruzioni in COPY
    'gbCurEntryPCB = getENTRY(GbIdOggetto)
  End If
  
  While Not rs.EOF
    ' Mauro/SQ 13/11/2009
    ''''''''''''''''''''''''''''''''''''''''''''
    ' GESTIONE CLONI
    ''''''''''''''''''''''''''''''''''''''''''''
    Set rsCall = m_Fun.Open_Recordset("Select Parametri from PsCall where IdOggetto = " & rs!idOggetto & " and Riga = " & rs!Riga)
    If Not rsCall.EOF Then
      parametri = rsCall!parametri
      istruzione = nextToken_new(parametri)
      If Left(istruzione, 1) <> "'" Then
      'IRIS-DB searches for MOVE of function just before the CALL - ONLY VALID FOR COBOL!!!!
        If GbTipoInPars = "CBL" Or GbTipoInPars = "CPY" Then 'IRIS-DB
          Dim actualInstr As String 'IRIS-DB
          actualInstr = parseFunctionMove(istruzione, rs!Riga) 'IRIS-DB
          If Len(actualInstr) Then 'IRIS-DB
            istruzione = getIstrValue(actualInstr) 'IRIS-DB
            rs!istruzione = istruzione 'IRIS-DB
            rs.Update 'IRIS-DB
          Else 'IRIS-DB
            AggiornaCloniIstruzione rs, istruzione, parametri
          End If 'IRIS-DB
        End If 'IRIS-DB
      End If
    End If
    rsCall.Close
    
        
    'SQ 11-01-08 (portato sopra l'AggiornaCloniIstruzione; i cloni venivano con l'idOggetto sbagliato!
    'SQ CPY-ISTR
    IdOggettoRel = rs!idOggetto
    GbSegnObjRel = IdOggettoRel
    GbOldNumRec = rs!Riga
    'AC '09/04/08
    TabIstr.keyFeed = ""
    TabIstr.keyFeedLen = "" 'IRIS-DB
    ' Mauro 21/04/2008
    TabIstr.KeyArg = ""
    
    istruzione = Trim(IIf(IsNull(rs!istruzione), "", rs!istruzione))
    parametri = rs!statement
    
    'IRIS-DB: it tries to avoid all the analysis if the DBD is out of scope, i.e. not touching it
    '         solo sel l'istruzione non � una PCB
    DBDOutOfScope = False
    If Not (rs!istruzione = "PCB") And Not (rs!istruzione = "SCHED") And Not (rs!istruzione = "TERM") And Not (rs!istruzione = "CHKP") Then
      If rs!isExec Then
        TabIstr.pcb = nexttoken(parametri) ' per ora lo faccio semplice, ma non sempre � cos�
        TabIstr.pcb = nexttoken(parametri) ' per ora lo faccio semplice, ma non sempre � cos�
        TabIstr.pcb = Replace(TabIstr.pcb, ")", "")
        TabIstr.pcb = Replace(TabIstr.pcb, "(", "")
      Else
        TabIstr.pcb = nexttoken(parametri)
      End If
      parametri = rs!statement 'resetta al valore iniziale
      TabIstr.numpcb = MapsdM_IMS.getPCBnumber(TabIstr.pcb, rs!isExec)
      DBDOutOfScope = checkDBDScope(IdPsb, TabIstr.numpcb, dbdname)
      If DBDOutOfScope Then
        ' setta comunque a valida le istruzioni da non migrare
        Parser.PsConnection.Execute "UPDATE PsDli_Istruzioni SET imsDB = true, to_migrate = false, valida = true where idPgm=" & rs!idpgm & " AND idOggetto=" & rs!idOggetto & " and riga=" & rs!Riga
        Parser.PsConnection.Execute "INSERT into PsDli_IstrPSB VALUES(" & rs!idpgm & "," & rs!idOggetto & "," & rs!Riga & "," & IdPsb & ")"
  ''''' blocco sotto non fattibile al momento in quanto il DBD non esiste sulla bs_oggetti, ma cos� ci perdiamo l'informazione, da vedere pi� avanti se da problemi
  '''''      Set rsOggetti = m_Fun.Open_Recordset("select idoggetto from bs_oggetti where Nome = '" & DBDName & "' and tipo = 'DBD'")
  '''''      If rsOggetti.RecordCount > 0 Then
  '''''        Parser.PsConnection.Execute "INSERT into PsDli_IstrDBD VALUES(" & rs!idpgm & "," & rs!idOggetto & "," & rs!Riga & "," & rsOggetti!idOggetto & ")"
  '''''      Else
  '''''        m_Fun.writeLog "parseCbl_II: DBD not found in bs_Oggetti!!! Name: " & DBDName
  '''''      End If
  '''''      rsOggetti.Close
     End If
    End If
    'IRIS-DB end qwerty
     If Not DBDOutOfScope Then
        Select Case istruzione
          Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP", "POS" 'IRIS-DB
            If rs!isExec Then
              parseGU_exec istruzione, parametri
            Else
              parseGU_call istruzione, parametri
            End If
          Case "DLET", "REPL", "DELETE", "REPLACE"
            If rs!isExec Then
              parseREPL_exec istruzione, parametri
            Else
              parseREPL_call istruzione, parametri
            End If
          Case "ISRT", "INSERT"
            If rs!isExec Then
              parseISRT_exec istruzione, parametri
            Else
              parseISRT_call istruzione, parametri
            End If
          Case "CHKP", "SYMCHKP"
            ' Mauro 12-04-2007 : Attenzione, c'� una routine omonima sul modulo EASYTRIEVE
            If rs!isExec Then
              MapsdM_IMS.parseCHKP_exec istruzione, parametri, pIdPgm, rs!idOggetto, rs!Riga
              rs!valida = True
              rs!Imsdb = True
              rs.Update
            Else
              MapsdM_IMS.parseCHKP parametri
            End If
          Case "AUTH"
            parseAUTH parametri
          Case "XRST"
            If rs!isExec Then
              'AC 22/05/08 GESTIRE
              MapsdM_IMS.parseXRST_exec istruzione, parametri, pIdPgm, rs!idOggetto, rs!Riga
              rs!valida = True
              rs!Imsdb = True
              rs.Update
            Else
              ' Mauro 12-04-2007 : attenzione, c'� una routine omonima sul modulo EASYTRIEVE
              MapsdM_IMS.parseXRST parametri
            End If
    'IRIS-DB      Case "ICMD"
    'IRIS-DB        parseICMD parametri
          Case "CMD" 'IRIS-DB
            parseCMD istruzione, parametri 'IRIS-DB
          Case "CHNG"
            parseCHNG parametri
          Case "INQY"
            parseINQY parametri
          Case "LOG", "ROLB", "ROLL"
            'TMP: riuso il vecchio giro... sostituendo man mano...
            parseROLL istruzione, parametri
            rs!valida = True 'IRIS-DB
            rs!to_migrate = True 'IRIS-DB
            rs!Imsdb = True 'IRIS-DB
            rs.Update 'IRIS-DB
          Case "PCB"  ',"SCHEDULE","SCHD"
            If Not rs!isExec Then
             MapsdM_IMS.parsePCB parametri
            Else
              'AC 22/05/08 GESTIRE
              rs!valida = True
              rs!Imsdb = True 'IRIS-DB
              rs!Imsdc = False 'IRIS-DB
              rs.Update
            End If
          Case "TERM"
             MapsdM_IMS.parseTERM parametri
          Case "SYNC"
            rs!Imsdb = True 'IRIS-DB
            rs!valida = True
            rs.Update
          'AC 22/05/08 --> aggiunte query,symchkp (poi accomunata a chkp)
          Case "CKPT", "APSB", "DPSB", "INIT", "QUERY"
            rs!valida = True
            rs.Update
          Case "OPEN", "CLSE"
            parseOPEN_CLOSE istruzione, parametri
          Case "GCMD", "RCMD", "GSCD", "GMSG", "PURG"
            'Fare bene...
            parsePURG istruzione, parametri
          Case "SETB", "SETO", "SETS", "SETU", "ROLS" 'IRIS-DB aggiunta ROLS per coerenza con SETS
            'Fare bene...
            parseSETx istruzione, parametri
          Case "SCHED", "SCHEDULE", "SCHD"
            rs!valida = True
            rs!to_migrate = True
            rs!Imsdb = True
            rs.Update
          Case Else
            'gestire...
            addSegnalazione istruzione, "F08", istruzione
        End Select
    End If
    'AC 01/09/10
    'AggiornaCloniPcb rs
    rs.MoveNext
  Wend
  rs.Close
  
  '''''''''''''''''
  '  'EXEC?
  '''''''''''''''''
  'Set rs = m_Fun.Open_Recordset("Select * from PsExec_SQ where idOggetto=" & GbIdOggetto & " AND type='DLI'")
  'DliCercaEXEC wRec, FD
  
  ''''''''''''''''''''''''''''''''''''''''''''
  ' SQ CPY-INSTR
  ''''''''''''''''''''''''''''''''''''''''''''
  'IRIS-DB: removing it for now, too heavy; TODO: remove when IRIS improved for managing copybooks
''  If pRiga = -1 Then
''    manageCpyInstructions
''  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Soluzione REPL/DLET non risolte con controllo GH
  ' Sistemare... cmq � giusto che stia fuori ciclo
  ''''''''''''''''''''''''''''''''''''''''''''''''''''
  If vPam(0) = "Yes" Then
    '''Resolve_Single_GH_REPL_DLET
    resolve_Istr_DLET_REPL
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''
  ' Update Bs_oggetti: data,livello,errorLevel
  ''''''''''''''''''''''''''''''''''''''''''''
  Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where idoggetto = " & GbIdOggetto)
  TbOggetti!DtParsing = Now
  TbOggetti!ErrLevel = GbErrLevel
  TbOggetti!parsinglevel = 2
  TbOggetti.Update
  TbOggetti.Close

  'SQ - CPY-INSTR chiarire questa qui sotto:
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Aggiornamento DBD in PsRel_Obj e SEGM in PsComObj (!?)
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  AggiornaRelIstrDli GbIdOggetto
  
  Exit Sub
parseErr:
  'gestire meglio...
  If err.Number = -2147467259 Or err.Number = 3704 Then  'tmp - problema Alby... prj Perot
    'm_Fun.WriteLog "parseCbl_II: id#" & GbIdOggetto & "#" & Error & "#" & Err.Description
    addSegnalazione GbNomePgm & ": nested PSB", "I00", GbNomePgm & ": nested PSB"
  Else
    'MsgBox "II level parsing: " & Err.Description
    addSegnalazione rs!statement, "I00", rs!statement
    'Resume
  End If
End Sub
Public Function checkDBDScope(IdPsb As Long, numpcb As Long, dbdname As String) As Boolean 'IRIS-DB
  Dim tb As Recordset
  Dim TypePcb As String
  On Error GoTo err
  checkDBDScope = False
  Set tb = m_Fun.Open_Recordset("select dbdName, TypePcb from PsDli_PSB where idOggetto = " & IdPsb & " and NumPCB = " & numpcb)
  If tb.RecordCount > 0 Then
    dbdname = tb!dbdname
    TypePcb = tb!TypePcb
  Else
    m_Fun.writeLog "checkDBDScope: PSB-PCB not found, idoggetto: " & GbIdOggetto & " - idPSB: " & IdPsb & " - numpcb: " & numpcb
    tb.Close
    Exit Function
  End If
  tb.Close
  If Not (TypePcb = "TP") Then
    Set tb = m_Fun.Open_Recordset("select to_migrate from PsDli_DBD where dbd_name = '" & dbdname & "'")
    If tb.RecordCount > 0 Then
      checkDBDScope = Not tb!to_migrate
    Else
      checkDBDScope = True
    End If
    tb.Close
  Else
    checkDBDScope = False
  End If
  Exit Function
err:
  m_Fun.writeLog "checkDBDScope: " & err.description
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Analizza:
' - Descrizione FILE
' - Strutture associate
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub getFD(line As String, FD As Long)
  Dim statement As String, token As String, fileName As String, ddname As String, filetype As String
  Dim endStatement As Boolean
  Dim rs As New Recordset, rs1 As Recordset
  Dim i As Integer
  Dim count As Long
  
  On Error GoTo fdErr
  
  '''''''''''''''''''''''''''''''''''''''''
  ' DESCRIZIONE FILE
  '''''''''''''''''''''''''''''''''''''''''
  While Not EOF(FD) And Not endStatement
    If Len(line) = 0 Then 'len(statement)
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
        line = Trim(Mid(line, 8, 65))
      Else
        line = ""
      End If
    End If
    If Len(line) Then
      statement = statement & " " & line
      If InStr(line, ".") Then
         endStatement = True
         statement = Replace(statement, ".", "")
      End If
      line = ""
    End If
  Wend
  
  fileName = nexttoken(statement)
  
  'DDname e fileType (JOIN con dichiarazioni in SELECT)
  For i = 0 To UBound(assign_files)
    If fileName = assign_files(i).fileName Then
      ddname = assign_files(i).ddname
      filetype = assign_files(i).filetype
      Exit For
    End If
  Next i
    
  '''''''''''''''''''''''''''''''''''''''''
  ' STRUTTURA ASSOCIATA
  '''''''''''''''''''''''''''''''''''''''''
  'TERMINATORE: FD/SECTION/DIVISION:
  endStatement = False 'init
  statement = "" 'init
  While Not EOF(FD) And Not endStatement
    If Len(statement) = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      If Mid(line, 7, 1) <> "*" And Mid(line, 7, 1) <> "/" Then
        statement = Trim(Mid(line, 8, 65))
      Else
        statement = ""
      End If
    End If
    If Len(statement) Then
      token = nexttoken(statement)
      Select Case token
        Case "01"
          'Struttura:
          token = Replace(nexttoken(statement), ".", "") '9-02-06
          '''''''''''''''''''''''''''
          ' IdArea:
          '''''''''''''''''''''''''''
          Dim idArea As Long ', idArchivio As Long
          Dim dsnName As String
          Dim VSAMName As String
          Set rs = m_Fun.Open_Recordset("select IdArea From PsData_Area where idOggetto = " & GbIdOggetto & " and nome = '" & token & "'")
          
          'MF 07/08/07 Prendeva solo il primo record anche se RecordCount era >1
          'count = rs1.RecordCount
          'While count
          If rs.RecordCount Then  'POSSO AVERE N FILLER!!!!!!!!!!!!!!!!!!!! (NELLO STESSO FD)
            idArea = rs!idArea
            'Ho gi� il "link"?
            Set rs = m_Fun.Open_Recordset("select DSNname From PsRel_File where" & _
                                          " Program = '" & GbNomePgm & "' and link = '" & ddname & "'")
            If rs.RecordCount Then
              'DATASET "GIA' NOTO": RELAZIONE FINALE
              ' update DMMVS_Archivio
              ' inserimento DMVSM_Tracciato
              dsnName = rs!dsnName
              Set rs = m_Fun.Open_Recordset("select * From DMVSM_Archivio where Nome = '" & dsnName & "'")
              If rs.RecordCount Then
                ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
                'idArchivio = rs!idArchivio
                VSAMName = rs!VSam & ""
                'CONTROLLO TIPO: (usare l'espressiono tipo iif...)
                ' Mauro 06/08/2007 : cambio i flag con un campo Type
                'rs!ESDS = fileType = "ESDS"
                'rs!KSDS = fileType = "KSDS"
                'rs!RRDS = fileType = "RRDS"
                rs!Type = filetype
                'VSAM/GDG/SEQ...
                rs!data_modifica = Now
                rs.Update
                '''''''''''''''''''''''
                ' 23-02-06: c'era il problema di chiavi duplicate...
                ' A cosa serve l'Ordinale? vedere se sarebbe OK la chiave idArchivio|IdOggetto|idArea
                '''''''''''''''''''''''
                Dim Ordinale As Integer
                
                ' Mauro 09/10/2007 : IdArchivio � stato sostituito con NomeVSAM
                'Set rs = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                '                              "idarchivio = " & idArchivio & " and idoggetto = " & GbIdOggetto & " and idarea = " & idArea)
                Set rs = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                              "VSAM = '" & VSAMName & "' and idoggetto = " & GbIdOggetto & " and " & _
                                              "idarea = " & idArea)
                If rs.RecordCount = 0 Then
                  'Set rs = m_Fun.Open_Recordset("select * from DMVSM_tracciato where idarchivio = " & idArchivio & " order by ordinale ")
                  Set rs = m_Fun.Open_Recordset("select * from DMVSM_tracciato where " & _
                                                "VSAM = '" & VSAMName & "' order by ordinale ")
                  If rs.RecordCount = 0 Then
                    Ordinale = 1
                  Else
                    rs.MoveLast
                    Ordinale = rs.RecordCount + 1
                  End If
                  rs.AddNew
                  'rs!idArchivio = idArchivio
                  rs!VSam = VSAMName
                  rs!Ordinale = Ordinale
                  rs!idOggetto = GbIdOggetto
                  rs!idArea = idArea
                  rs!Tag = "FD"
                  rs.Update
                End If
              Else
                'SEGNALAZIONE/AGGIUNGIAMO
              End If
            Else
              'DATASET "ANCORA DA SCOPRIRE": RELAZIONE TEMPORANEA (PsRel_Tracciati)
              'MF
              'Problemi chiavi duplicate...esempio 01 FILLER, 01 FILLER
              On Error GoTo catch
              Parser.PsConnection.Execute "INSERT INTO PsRel_Tracciati " & _
                                          "VALUES('" & GbNomePgm & "','" & ddname & "'," & GbIdOggetto & "," & idArea & ")"
              On Error GoTo 0
            End If
'            count = count - 1
'            If count > 0 Then rs1.MoveNext
          End If 'Wend
            'SEGNALAZIONE!!!!!!!!!!!!!!!!!!!!!
          'End If
          'rs1.Close
          rs.Close
          statement = ""  'non mi serve analizzare il resto...
          ''''''''''''''''''''''''''''''
          ' DSN:
          ' a) gia' noto
          ' b) appoggio in PsRel_Tracciati
          ''''''''''''''''''''''''''''''
         ' Set rs = m_Fun.Open_Recordset("select IdArea From PsData_Area where idOggetto=" & GbIdOggetto & " and nome='" & token & "'")
        Case "COPY"
          getCOPY statement
          ''Nome Copy:
          'token = nextToken(statement) 'semplificazione: solo su stessa riga di COPY
          ''Gestione RELAZIONE/SEGNALAZIONI:
          'GbOldNumRec = GbNumRec
          'checkRelations Replace(Replace(Replace(token, ".", ""), "'", ""), """", ""), "COPY" '9-2-06
          ''Struttura o sottocampo?
          ''... tmp: per ora ignoriamo le strutture dentro COPY... sicuramente da implementare
          ''...      => Problema: se ho un terminatore dentro COPY sono fregato!
        Case "FD"
          'EXIT: rientreremo subito...
          endStatement = True
        Case "WORKING-STORAGE", "THREAD-LOCAL-STORAGE", "OBJECT-STORAGE", "LINKAGE", "REPORT", "SCREEN"
          'ALTRA SECTION
          endStatement = True
        Case "PROCEDURE", "THREAD-LOCAL-STORAGE", "OBJECT-STORAGE", "LINKAGE", "REPORT", "SCREEN"
          'ALTRA DIVISION
          endStatement = True
        Case "."
          'endStatement = True
          'line = ""
        Case Else
          '
      End Select
    End If
  Wend
  Exit Sub
fdErr:
  'gestire...
  err.Raise err.Number, , err.description
  Exit Sub
catch:
  If err.Number = -2147467259 Then
    m_Fun.writeLog "Parser FD - idOggetto: " & GbIdOggetto & " Chiave duplicata su: " & token & ""
  Else
    'da gestire
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''
' Analisi "LOA": filettini di associazione src/load
' - Allestisce la PsLoad
''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub parseLoad()
  Dim FD As Long, TmpIdOggetto As Long
  Dim line As String, token As String, load As String, object As String
  Dim rsLoad As Recordset, rsObj As Recordset
  
  On Error GoTo parseErr
  
  GbNumRec = 0
  FD = FreeFile
  Open GbFileInput For Input As FD
  Do While Not EOF(FD)
    Line Input #FD, line
    GbNumRec = GbNumRec + 1
    
    'COMMENTI???
    'If Mid(line, 7, 1) <> "*" Then
      'LINEA VALIDA:
      line = Trim(Left(line, 72)) 'elimino etichette!
      token = nexttoken(line)
      Select Case token
        Case "INCLUDE"
          'Mauro 13/03/2008: INCLUDE OBJECT(...)
          token = nexttoken(line)
          Select Case token
            Case "OBJECT"
              If object = "" Then
                object = nexttoken(line)
                object = Replace(object, "(", "")
                object = Replace(object, ")", "")
              End If
          End Select
        Case "ENTRY"
          'nop
        Case "ALIAS"
          load = nexttoken(line)
          Set rsLoad = m_Fun.Open_Recordset("select * from PsLoad WHERE source='" & GbNomePgm & "' and load='" & load & "'")
          If rsLoad.EOF Then
          'If rsLoad.RecordCount = 0 Then
            rsLoad.AddNew
            rsLoad!source = GbNomePgm
            rsLoad!load = load
            rsLoad.Update
          End If
          rsLoad.Close
        Case "NAME"
          load = nexttoken(line)
          Set rsLoad = m_Fun.Open_Recordset("select * from PsLoad WHERE source='" & GbNomePgm & "' and load='" & load & "'")
          If rsLoad.EOF Then
            rsLoad.AddNew
            rsLoad!source = GbNomePgm
            rsLoad!load = load
            rsLoad.Update
          End If
          rsLoad.Close
          Exit Do
        Case Else
          'nop
      End Select
    '''Else
    '''  line = ""
    ''''End If
  Loop
  Close FD
  If load = "" Then
    Set rsLoad = m_Fun.Open_Recordset("select * from PsLoad WHERE source='" & GbNomePgm & "' and load='" & object & "'")
    If rsLoad.EOF Then
      rsLoad.AddNew
      rsLoad!source = GbNomePgm
      rsLoad!load = object
      rsLoad.Update
    End If
    rsLoad.Close
  End If
  
  ' Mauro 03/09/2009 : Aggiorno le relazioni al Nome del compilato
  Set rsObj = m_Fun.Open_Recordset("select idoggetto, tipo from bs_oggetti where " & _
                                   "Nome = '" & GbNomePgm & "' and tipo <> 'LOA'")
  If rsObj.RecordCount Then
    TmpIdOggetto = GbIdOggetto
    GbIdOggetto = rsObj!idOggetto
    Parser.updateRelations load, rsObj!Tipo
    GbIdOggetto = TmpIdOggetto
  End If
  rsObj.Close
  Exit Sub
parseErr:  'Gestito a livello superiore...
  If err.Number = 53 Then
    Parser.PsFinestra.ListItems.Add , , "parseLoad: id#" & GbIdOggetto & ", ###" & err.description
    Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
    m_Fun.writeLog "parseLoad: id#" & GbIdOggetto & ", ###" & err.description
  Else
    GbOldNumRec = GbNumRec
    addSegnalazione line, "P00", line
    'Resume
  End If
End Sub
'SQ: provvisorio per "," come separatore... farne uno con argomento opzionale...
'ritorna la parola successiva (space unico separatore)
'Considera come delimitatore di "token" le parentesi e gli apici singoli...
'Effetto Collaterale: mangia...
'
'Dovra' diventare quello definitivo: mantiene gli apici...
Function nextToken_new(inputString As String)
  Dim level As Integer, i As Integer, j As Integer
  Dim currentChar As String
  
  'Mangio i bianchi davanti (primo giro...)
  inputString = LTrim(inputString)
  If (Len(inputString) > 0) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "("
        i = 2
        level = 1
        nextToken_new = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "("
              level = level + 1
              nextToken_new = nextToken_new & currentChar
            Case ")"
              level = level - 1
              nextToken_new = nextToken_new & currentChar
            Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nextToken_new = nextToken_new & currentChar
              Else
                err.Raise 123, "nextToken_new", "syntax error on " & inputString
                Exit Function
              End If
          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
      Case "'"
        'Ritorno gli apici
        ''''inputString = Mid(inputString, 2)
        i = InStr(2, inputString, "'")
        If i Then
          'nextToken_new = Left(inputString, InStr(inputString, "'") - 1)
          nextToken_new = Left(inputString, i)
          inputString = Mid(inputString, i + 1)
        Else
          'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
          inputString = "''" & inputString
          
          err.Raise 123, "nextToken_new", "syntax error on " & inputString
          Exit Function
        End If
      Case Else
        i = InStr(inputString, " ")
        j = InStr(inputString, "(")
        If ((i > 0 And j > 0 And j < i) Or (i = 0 And j > 0)) Then i = j
        If (i > 0) Then
          nextToken_new = Left(inputString, i - 1)
          inputString = Trim(Mid(inputString, i))
        Else
          nextToken_new = inputString
          inputString = ""
        End If
    End Select
  End If
End Function
Function parseFunctionMove(istruzione As String, numRiga As Long) As String 'IRIS-DB
  Dim FD As Long
  Dim statement As String
  Dim word As String
  Dim IMSFunc As String
  ReDim Loc(numRiga) As String
  Dim LOCNum As Long
      
  If Len(Dir(GbFileInput)) = 0 Then
    MsgBox "This file is not avaliable...", vbExclamation, Parser.PsNomeProdotto
    Exit Function
  End If
  
  LOCNum = 0
  
  FD = FreeFile
  If err.Number = 67 Then 'IRIS-DB too many files
   err.Clear              'IRIS-DB
   FD = FreeFile(1)    'IRIS-DB
   If err.Number = 67 Then 'IRIS-DB
      addSegnalazione "#!# IRIS too many open files", " P00", "#!#" & istruzione
   End If
  End If
  On Error GoTo parseErr
  
  Open GbFileInput For Input As FD
  While Not EOF(FD) And LOCNum < numRiga 'IRIS-DB per ora carico solo le righe fino all'istruzione in esame, in seguito bisogner� caricarlo tutto se si vuole cercare richiami di paragrafo all'indietro
    'IRIS-DB DoEvents
    Line Input #FD, statement
    Loc(LOCNum) = statement
    LOCNum = LOCNum + 1
  Wend
  Close #FD
  LOCNum = numRiga - 1 'ritorna al numero di riga precedente
  While LOCNum > 0
    LOCNum = LOCNum - 1 ' si sposta sopra
    If Mid(Loc(LOCNum), 7, 1) <> "*" And Mid(Loc(LOCNum), 7, 1) <> "/" Then ' salta righe commentate
      statement = RTrim(Mid(Loc(LOCNum), 8, 65)) 'elimino etichette!
      If Len(statement) Then 'salta righe vuote
        word = nextToken_new(statement) 'MANTIENE GLI APICI!!
        If word = "MOVE" Then
          IMSFunc = nextToken_new(statement)
          word = nextToken_new(statement) ' scarta il "TO"
          word = Replace(nextToken_new(statement), ".", "") ' variabile di arrivo; non gestisce casi con "OF" per ora
          If word = istruzione Then
            parseFunctionMove = IMSFunc
            Exit Function
          End If
        Else
          parseFunctionMove = "" ' per ora si ferma quando finiscono le MOVE o i commenti, da vedere come gestire "IF" e paragrafi (molti casi in Fedex)
          Exit Function
        End If
      End If
    End If
  Wend
  Exit Function
parseErr:
  addSegnalazione "#!# IRIS" & istruzione, " P00", "#!#" & istruzione
End Function
'SQ: sempre peggio!!! da unire!!!!
'Dovra' diventare quello definitivo: mantiene gli apici...
Function nextToken_newnew(inputString As String)
  Dim level As Integer, i As Integer, j As Integer
  Dim currentChar As String
  
  'Mangio i bianchi e virgole davanti (primo giro...)
  inputString = LTrim(inputString)
  If (Len(inputString) > 0) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "("
        i = 2
        level = 1
        nextToken_newnew = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "("
              level = level + 1
              nextToken_newnew = nextToken_newnew & currentChar
            Case ")"
              level = level - 1
              nextToken_newnew = nextToken_newnew & currentChar
            Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nextToken_newnew = nextToken_newnew & currentChar
              Else
                err.Raise 123, "nextToken_newnew", "syntax error on " & inputString
                Exit Function
              End If
          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
      Case "'"
        'Ritorno gli apici
        ''''inputString = Mid(inputString, 2)
        i = InStr(2, inputString, "'")
        If i Then
          'nextToken_newnew = Left(inputString, InStr(inputString, "'") - 1)
          nextToken_newnew = Left(inputString, i)
          inputString = Mid(inputString, i + 1)
        Else
          'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
          inputString = "''" & inputString
          
          err.Raise 123, "nextToken_newnew", "syntax error on " & inputString
          Exit Function
        End If
      Case Else
'''        i = InStr(inputString, " ")
'''        j = InStr(inputString, "(")
'''        If ((i > 0 And j > 0 And j < i) Or (i = 0 And j > 0)) Then i = j
'''        If (i > 0) Then
'''            nextToken_newnew = Left(inputString, i - 1)
'''            inputString = Trim(Mid(inputString, i))
'''        Else
'''            nextToken_newnew = inputString
'''            inputString = ""
'''        End If
'Ricerca primo separatore: " " o ","
        For i = 1 To Len(inputString)
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case " ", "'"
              nextToken_newnew = Left(inputString, i - 1)
              inputString = Trim(Mid(inputString, i))
              Exit Function
            Case "("
              nextToken_newnew = Left(inputString, i - 1)
              inputString = Trim(Mid(inputString, i))
              'pezza: se ho pippo(...),... rimane una virgola iniziale:
              'If Left(inputString, 1) = "," Then inputString = Trim(Mid(inputString, 2))
              Exit Function
            Case ","
              nextToken_newnew = Left(inputString, i - 1)
              inputString = Trim(Mid(inputString, i + 1))
              Exit Function
            'IRIS-DB: usa anche l'uguale come separatore in quanto pu� essere attaccato alla variabile
            Case "="
              nextToken_newnew = Left(inputString, i - 1)
              inputString = Trim(Mid(inputString, i))
              Exit Function
            Case Else
              'avanza pure
          End Select
        Next i
        'nessun separatore:
        nextToken_newnew = inputString
        inputString = ""
    End Select
  End If
  'inizia con un terminatore?!
  If Left(inputString, 1) = "," Then
    inputString = Mid(inputString, 2)
  End If
End Function

''''''''''''''''''''''''''''''''''''''
' SQ 27-02-06
' E' un AggiungiOggetto "batch"...
' setta GbIdOggetto
''''''''''''''''''''''''''''''''''''''
'Sub addObject(fileName As String, fileType As String, Occurs As Integer)
Sub addObject(fileName As String)
  Dim rsOggetti As Recordset
  Dim estensione As String, dirName As String, outputDir As String, nomeOggetto As String
  Dim pos As Integer
  
  GbIdOggetto = 0 'init
  
  ''''''''''''''''''''''''''''''''''''
  'fileName: assoluto, con estensione
  ''''''''''''''''''''''''''''''''''''
  pos = InStrRev(fileName, "\")
  If pos Then
    dirName = Left(fileName, pos - 1)
    outputDir = Replace(dirName, m_Fun.FnPathDef, m_Fun.FnPathPrj)
    nomeOggetto = Mid(fileName, pos + 1)
  Else
    MsgBox "Incorrect file name: " & fileName, vbError, Parser.PsNomeProdotto
    Exit Sub
  End If
  pos = InStrRev(nomeOggetto, ".")
  If pos Then
    estensione = Mid(nomeOggetto, pos + 1)
    nomeOggetto = Left(nomeOggetto, pos - 1)
  End If
  
  '''''''''''''''''''''''''''''''''''
  ' Creazione subdir/Copia File
  '''''''''''''''''''''''''''''''''''
  Dim subDir As String, inFile As String, outFile As String
  Dim k As Integer
  For k = 1 To Len(outputDir)
    If Mid(outputDir, k, 1) = "\" Then
      If Left(subDir, 2) <> "\\" Then
         If Dir(subDir, vbDirectory) = "" Then
           MkDir (subDir)
         End If
      End If
    End If
    subDir = subDir & Mid(outputDir, k, 1)
  Next k
  If Dir(outputDir, vbDirectory) = "" Then
     MkDir outputDir
  End If
  
  ' Mauro 14-05-2007 : copia anche l'estensione
  'FileCopy fileName, outputDir & "\" & nomeOggetto
  FileCopy fileName, outputDir & "\" & nomeOggetto & IIf(Len(estensione), "." & estensione, "")
  
  ''''''''''''''''''''''''''''''''''''
  ' Aggiunta oggetto al db di progetto
  ''''''''''''''''''''''''''''''''''''
  outputDir = Replace(outputDir, m_Fun.FnPathDef, "$")
  Dim idOggetto As Long
  Set rsOggetti = m_Fun.Open_Recordset("select * from Bs_Oggetti where nome='" & nomeOggetto & "' and directory_input='" & outputDir & "' and estensione='" & estensione & "'")
  If rsOggetti.RecordCount = 0 Then
    rsOggetti.AddNew
    rsOggetti!nome = nomeOggetto
    rsOggetti!estensione = estensione
    rsOggetti!Directory_Input = outputDir
    rsOggetti!Livello2 = outputDir
    rsOggetti!parsinglevel = 0
    If GbNumRec Then
      rsOggetti!NumRighe = GbNumRec 'N.B.: CALCOLATO DAL RICONOSCIMENTO (getObjectType) - DA VERIFICARE
    End If
    rsOggetti!ErrLevel = ""
  Else
    'c'e' gia': strano!?
  End If
  
  'SQ 19-06-07 - c'era il vecchio mega "Select Case"
  Dim area As String, level As String
  getLevel_Area GbTipoInPars, area, level
  rsOggetti!Area_Appartenenza = area
  rsOggetti!livello1 = level
  'Select Case GbTipoInPars
  '  Case "CPY"
  '      rsOggetti!Area_Appartenenza = "SOURCE"
  '      rsOggetti!livello1 = "COBOL-CPY"
  '...  Case "CBL"
  'End Select
  
  rsOggetti!Tipo = GbTipoInPars
  rsOggetti.Update
  
  GbIdOggetto = rsOggetti!idOggetto
   
  rsOggetti.Close
  
  ''''''''''''''''''''''''''''''''''''''''''
  ' Aggiorna la tabella per update TreeView
  ''''''''''''''''''''''''''''''''''''''''''
  updateTreeViewTable GbIdOggetto 'copia di quello in Mainrev
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ: 23-02-06
' Import degli oggetti mancanti correlati ad objectList (lista di "idOggetto")
' -> Bs_segnalazioni (copy,mappe,moduli chiamati,...)
' Gestione riconoscimento:
' - se trovo solo un oggetto (nel filesystem di progetto)
'   NON faccio il riconoscimento (rischio)
''
' Attenzione: oggetti gi� presenti non vengono parserati!!!!!!!!!!
' Fare il parsing dopo la getItemList!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
'
' OTTIMIZZAZIONE: lista di oggetti gi� analizzati...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub addMissingObjects(objectList As String, objtype As String, level As Integer)
  Dim objects() As String
  'Dim Level As Integer
  Dim i As Integer
  
  objects = Split(objectList, ",")
  
  For i = 0 To UBound(objects)
    If newObject(objects(i)) Then 'gi� analizzato?
      'SQ - tmp...
      Dim rs As Recordset
      Set rs = m_Fun.Open_Recordset("SELECT tipo FROM Bs_oggetti WHERE idOggetto=" & CLng(objects(i)))
      If rs.RecordCount Then
        objtype = rs!Tipo
      Else
        'impossibile!
        MsgBox "TMP"
      End If
      Select Case objtype
        Case "CBL", "CPY", "ASM" 'SQ 19-02-07 aggiunto ASM... buttare via tutto!!!!  usare tabelline...
          '''''''''''''''''''''''''''''''''''''''''''
          ' "Esplosione" COPY
          ' Cerca tutte le copy utilizzate,
          ' aggiunge quelle mancanti al progetto e
          ' ritorna la lista degli idOggetto
          '''''''''''''''''''''''''''''''''''''''''''
          ''''objectList = getItemList(CLng(objects(i)), "COPY")
          objectList = getItemList(CLng(objects(i)), "CPY", level)
          If Len(objectList) Then
            addMissingObjects objectList, "CPY", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' Programmi chiamati:
          ' aggiunge quelli mancanti al progetto e
          ' ritorna la lista degli idOggetto
          '''''''''''''''''''''''''''''''''''''''''''
          'objectList = getItemList(CLng(objects(i)), "CALL",level)
          objectList = getItemList(CLng(objects(i)), "CBL", level)
          If Len(objectList) Then
            addMissingObjects objectList, "CBL", level + 1
          End If
          'objectList = getItemList(CLng(objects(i)), "ASM", level)
          'If Len(objectList) Then
          '  addMissingObjects objectList, "ASM", level + 1
          'End If
          objectList = getItemList(CLng(objects(i)), "XCTL", level)
          If Len(objectList) Then
            addMissingObjects objectList, "CBL", level + 1
          End If
          objectList = getItemList(CLng(objects(i)), "LINK", level)
          If Len(objectList) Then
            addMissingObjects objectList, "CBL", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' SQL INCLUDE
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getItemList(CLng(objects(i)), "INCLUDE", level)
          ''If Len(objectList) Then
          ''  addMissingObjects objectList, "INCLUDE", level + 1
          ''End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' MAPPE
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getItemList(CLng(objects(i)), "MAP", level)
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          updateReturnCode CLng(objects(i))
        Case "PGM"  'ocio ASM,PLI...
          '''''''''''''''''''''''''''''''''''''''''''
          ' PROGRAMMI BATCH
          '''''''''''''''''''''''''''''''''''''''''''
          'objectList = getItemList(CLng(objects(i)), "COPY",level)
          objectList = getItemList(CLng(objects(i)), "CPY", level)
          If Len(objectList) Then
            addMissingObjects objectList, "CPY", level + 1
          End If
          'objectList = getItemList(CLng(objects(i)), "CALL",level)
          objectList = getItemList(CLng(objects(i)), "PGM", level)
          If Len(objectList) Then
            addMissingObjects objectList, "CBL", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' SQL INCLUDE
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getItemList(CLng(objects(i)), "INCLUDE", level)
          ''If Len(objectList) Then
          ''  addMissingObjects objectList, "INCLUDE", level + 1
          ''End If
          
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          updateReturnCode CLng(objects(i))
        Case "PLI"  'stefano: aggiunto PLI, a cosa serve "PGM" sopra??
          objectList = getItemList(CLng(objects(i)), "INC", level)
          If Len(objectList) Then
            addMissingObjects objectList, "INC", level + 1
          End If
          objectList = getItemList(CLng(objects(i)), "PLI", level)
          If Len(objectList) Then
            addMissingObjects objectList, "PLI", level + 1
          End If
          objectList = getItemList(CLng(objects(i)), "INCLUDE", level)
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          updateReturnCode CLng(objects(i))
        Case "EZT", "EZM" 'SQ 12-09-07
          objectList = getItemList(CLng(objects(i)), "EZM", level)
          If Len(objectList) Then
            addMissingObjects objectList, "EZM", level + 1
          End If
          'objectList = getItemList(CLng(objects(i)), "PLI", level)
          'If Len(objectList) Then
          '  addMissingObjects objectList, "PLI", level + 1
          'End If
          'objectList = getItemList(CLng(objects(i)), "INCLUDE", level)
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          updateReturnCode CLng(objects(i))
          
        Case "JCL", "JOB", "PRC", "MBR"
          '''''''''''''''''''''''''''''''''''''''''''
          ' MEMBER (INCLUDE)
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getItemList(CLng(objects(i)), "MBR", level)
          If Len(objectList) Then
            addMissingObjects objectList, "MBR", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' PROC
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getItemList(CLng(objects(i)), "PRC", level)
          If Len(objectList) Then
            addMissingObjects objectList, "PRC", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' PGM
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getItemList(CLng(objects(i)), "PGM", level) 'ocio... non so se e' COBOL!
          If Len(objectList) Then
            addMissingObjects objectList, "CBL", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' PGM CHIAMATI CON UTILITY (CALL)
          '''''''''''''''''''''''''''''''''''''''''''
          'SQ - CAMBIARE TUTTO!!!!!!!
          objectList = getItemList(CLng(objects(i)), "CBL", level)
          If Len(objectList) Then
            addMissingObjects objectList, "CBL", level + 1
          End If
          ' Mauro 23/02/2009: EZT-PGM
          objectList = getItemList(CLng(objects(i)), "EZT", level)
          If Len(objectList) Then
            addMissingObjects objectList, "EZT", level + 1
          End If
          'SQ - CAMBIARE TUTTO!!!!!!!
          'objectList = getItemList(CLng(objects(i)), "ASM", level)
          'If Len(objectList) Then
          '  addMissingObjects objectList, "ASM", level + 1
          'End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' PSB
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getItemList(CLng(objects(i)), "PSB-JCL", level)
          If Len(objectList) Then
            addMissingObjects objectList, "PSB", level + 1
          End If
          ' Mauro 27/01/2009: Gestione Control Card/Schede Parametro
          objectList = getItemList(CLng(objects(i)), "PRM", level)
          If Len(objectList) Then
            addMissingObjects objectList, "PRM", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          updateReturnCode CLng(objects(i))
        Case "PSB"
          '''''''''''''''''''''''''''''''''''''''''''
          ' DBD
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getItemList(CLng(objects(i)), "DBD", level)
          ''If Len(objectList) Then
          ''  addMissingObjects objectList, "DBD"
          ''End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          updateReturnCode CLng(objects(i))
        Case "NAT", "NIN", "NCP"
          '''''''''''''''''''''''''''''''''''''''''''
          ' "Esplosione" INCLUDE
          ' Cerca tutte le copy utilizzate,
          ' aggiunge quelle mancanti al progetto e
          ' ritorna la lista degli idOggetto
          '''''''''''''''''''''''''''''''''''''''''''
          ''''objectList = getItemList(CLng(objects(i)), "COPY")
          objectList = getItemList(CLng(objects(i)), "NIN", level)
          If Len(objectList) Then
            addMissingObjects objectList, "NIN", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' Programmi chiamati:
          ' aggiunge quelli mancanti al progetto e
          ' ritorna la lista degli idOggetto
          '''''''''''''''''''''''''''''''''''''''''''
          'objectList = getItemList(CLng(objects(i)), "CALL",level)
          objectList = getItemList(CLng(objects(i)), "NAT", level)
          If Len(objectList) Then
            addMissingObjects objectList, "NAT", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' NAT-COPY
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getItemList(CLng(objects(i)), "NCP", level)
          '''''''''''''''''''''''''''''''''''''''''''
          ' MAPPE
          '''''''''''''''''''''''''''''''''''''''''''
          'objectList = getItemList(CLng(objects(i)), "MAP", level)
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          'tmp
          ''''''''''updateReturnCode CLng(objects(i))
        Case Else
          displayAddingInfo level, CLng(objects(i)) & "", objtype, "-", "already managed."
      End Select
    Else
      'gi� analizzato
    End If
  Next i
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Cerca il file nella directory di progetto,
' lo importa, ne fa il parsingI e ne ritorna l'idOggetto
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function searchFile(ByVal fileName As String, filetype As String) As Long
  Dim CurrentFILE As String
  Dim idOggetto As Long
  Dim fileList As Collection
  Dim i As Integer, k As Integer
  Dim rs As Recordset
  Dim cParam As Collection
   
  Set fileList = New Collection
  
  GbIdOggetto = 0 'init
  
  Select Case filetype
    Case "CBL", "PGM", "LINK", "XCLT", "PLI" 'stefano : stesso gioco sui load per PLI
      ''''''''''''''''''''''''''''''''''''''''
      ' Gestione LOAD (diverso da Nome SOURCE)
      ''''''''''''''''''''''''''''''''''''''''
      Set rs = m_Fun.Open_Recordset("select source from PsLoad where load = '" & fileName & "'")
      If rs.RecordCount Then
        'Nome src diverso!
        fileName = rs!source
      End If
      rs.Close
      'ATTENZIONE AGLI ASM,PLI!!!!!!!!!!
      'SQ 19-02-07
      filetype = "PGM"
    Case "PSB-JCL"
      filetype = "PSB"
    Case "INCLUDE"
      filetype = "CPY"
    Case "MAP"
      filetype = "BMS"
  End Select
    
  'Riempie fileList
  searchFiles fileName, m_Fun.FnPathDef, fileList
  
  ''''''''''''''''''''''''''''''''''''''''''''''''
  ' Fra la lista di oggetti (omonimi) cerco quello
  ' del tipo che mi serve
  ' (N.B. se ho un solo elemento, non riconosciuto,
  ' gli forzo il tipo!)
  ' - Aggiungo al progetto l'elemento
  ' - Eseguo il parsing I (deve creare le relazioni...)
  ''''''''''''''''''''''''''''''''''''''''''''''''
  For i = 1 To fileList.count
    ''''''''''''''''''''''''''''''''''''''''''
    ' RICONOSCIMENTO
    ' Se UNK ma una sola occorrenza, forzo il tipo!
    ''''''''''''''''''''''''''''''''''''''''''
    ' Mauro 24/02/2009
    'GbTipoInPars = getObjectType(fileList(i))
    If Parser.PsTipoEstensione = "Yes" Then
      GbTipoInPars = getObjTypeByExt(fileList(i))
    Else
      GbTipoInPars = getObjectType(fileList(i))
    End If
    'SQ 19-02-07
    'If GbTipoInPars = "UNK" And fileList.count = 1 Then
    '  GbTipoInPars = fileType
    'End If
    Select Case GbTipoInPars
      Case "CBL", "NAT", "PLI", "ASM"
        If filetype = "PGM" Then
          filetype = GbTipoInPars
        End If
      ' Mauro 24/02/2009 : E se non � l'oggetto corretto?!
      Case "UNK"
        If fileList.count = 1 Then
          GbTipoInPars = filetype
        Else
          If filetype = "CBL" Then
            GbTipoInPars = "CPY"
          End If
        End If
    End Select
    
    If filetype = GbTipoInPars Then 'lista di tipi (es. ASM,PLI,CBL)
      'ok: e' il mio uomo
      addObject fileList(i) ', fileType, fileList.Count
      
      searchFile = GbIdOggetto
      GbNomePgm = fileName
      GbFileInput = fileList(i) 'non e' quello copiato... meglio usare l'altro?
      
      ''''''''''''''''''''
      ' Parsing I
      ''''''''''''''''''''
      Dim rsOggetti As Recordset
      Set rsOggetti = m_Fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & GbIdOggetto)
      If rsOggetti!parsinglevel < 1 Then
        Select Case filetype
          Case "CBL", "CPY", "PGM"  'ocio...
            AggiornaDati  '(non serve alla routine, ma almeno abbiamo un P-I completo)
            MapsdM_COBOL.parsePgm_I
          Case "ASM", "MAC"
            Asm2cobol = False
            Asm2seqfiles = False
            parseASM
          Case "JCL", "PRC", "MBR"
            IdOggettoRel = GbIdOggetto
            parseJCL
          Case "JOB"
            IdOggettoRel = GbIdOggetto
            parseJOB
          Case "PSB"
            parsePSB 'se non Segnala i DBD...
          Case "BMS"
            ParserBMS
          Case "DBD"
            'nop
          Case "NAT"
            MapsdM_NATURAL.parsePgm_I
          Case "INC", "PLI" 'stefano: PLI!!!
            MapsdM_PLI.parsePgm_I
          Case "EZT", "EZM"
            Ezt2cobol = False
            MapsdM_EASYTRIEVE.parsePgm_I
          Case Else
''            MsgBox "fare parsing per " & filetype
            m_Fun.writeLog "fare parsing per " & filetype
        End Select
        
        'SQ: ripetuto N volte... ma pazienza!
        If GbErrLevel <> "P-0" Then
          Parser.AggErrLevel GbIdOggetto
        End If
        rsOggetti!ErrLevel = GbErrLevel
        rsOggetti!DtParsing = Now
        rsOggetti!parsinglevel = 1
        rsOggetti.Update
      Else
        'elemento gia' presente e parserato
      End If
      rsOggetti.Close
      
      Exit For
    End If
  Next i
  
  searchFile = GbIdOggetto
  
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Cerca il file nella directory di progetto, lo importa e ne ritorna l'idOggetto
' ATTENZIONE: FARE UNA SELECT SULLA BS_OGGETTI TIRANDO SU TUTTI I fileName omonimi
' ed ottenere una lista di directory: in queste NON devo cercare, perche' non
' posso trovarvi il mio file (sarebbero 2 file uguali nella stessa dir!)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub searchFiles(fileName As String, dirName As String, fileList As Variant)
  Dim CurrentFILE As String, currentDir As String, currentFileName As String
  Dim idOggetto As Long
  Dim dirlist As New Collection

  dirlist.Add dirName & "\"
  While dirlist.count
    currentDir = dirlist.item(1)
    dirlist.Remove 1
    CurrentFILE = Dir(currentDir, vbDirectory)
    While Len(CurrentFILE)
      If CurrentFILE & ".mty" <> m_Fun.FnNomeDB And CurrentFILE <> "." And CurrentFILE <> ".." Then
        If GetAttr(currentDir & CurrentFILE) = vbDirectory Then  'add the subdirectory
          dirlist.Add currentDir & CurrentFILE & "\"
        Else
          'ESTENSIONI
          Dim k As Integer
          k = InStrRev(CurrentFILE, ".")
          If k Then
            currentFileName = Left(CurrentFILE, k - 1)
          Else
            currentFileName = CurrentFILE
          End If
          If currentFileName = fileName Then
            fileList.Add currentDir & CurrentFILE
      'stefano: non capisco, provo, ma potrei creare casini....
          'End If
          'If CurrentFILE = fileName Then
          ElseIf CurrentFILE = fileName Then
            fileList.Add currentDir & CurrentFILE
          End If
        End If
      End If
      CurrentFILE = Dir
    Wend
  Wend

End Sub
''''''''''''''''''''''''''''''''''''''''''''
'MF 13/08/07
''''''''''''''''''''''''''''''''''''''''''''
Public Sub searchFiles_Matteo(dirName As String, fileList As Variant)
  Dim CurrentFILE As String, currentDir As String, currentFileName As String
  Dim idOggetto As Long
  Dim dirlist As New Collection

  dirlist.Add dirName & "\"
  While dirlist.count
    currentDir = dirlist.item(1)
    dirlist.Remove 1
    CurrentFILE = Dir(currentDir, vbDirectory)
    While Len(CurrentFILE)
      If CurrentFILE <> "." And CurrentFILE <> ".." Then
        If GetAttr(currentDir & CurrentFILE) = vbDirectory Then  'add the subdirectory
          dirlist.Add currentDir & CurrentFILE & "\"
        Else
          fileList.Add currentDir & CurrentFILE
          
'''''        Else
'''''          'ESTENSIONI
'''''          Dim k As Integer
'''''          k = InStrRev(CurrentFILE, ".")
'''''          If k Then
'''''            currentFileName = Left(CurrentFILE, k - 1)
'''''          Else
'''''            currentFileName = CurrentFILE
'''''          End If
'''''          If currentFileName = fileName Then
'''''            fileList.Add currentDir & CurrentFILE
'''''      'stefano: non capisco, provo, ma potrei creare casini....
'''''          'End If
'''''          'If CurrentFILE = fileName Then
'''''          ElseIf CurrentFILE = fileName Then
'''''            fileList.Add currentDir & CurrentFILE
'''''          End If
        End If
      End If
      Debug.Print CurrentFILE
      CurrentFILE = Dir
    Wend
  Wend
End Sub

''''''''''''''''''''''''''''''''''''''''''''
' Aggiorna il db e la listView
''''''''''''''''''''''''''''''''''''''''''''
Private Sub updateReturnCode(idOggetto As Long)
  Dim rsOggetti As Recordset
  Dim k As Integer
  'Calcolo GbErrLevel:
  Parser.AggErrLevel idOggetto
  
  'Update db
  Set rsOggetti = m_Fun.Open_Recordset("select ErrLevel from BS_oggetti where idoggetto = " & idOggetto)
  If GbErrLevel <> rsOggetti!ErrLevel Then
    rsOggetti!ErrLevel = GbErrLevel
    rsOggetti.Update
  End If
  rsOggetti.Close
  
  'Refresh listView
  For k = 1 To Parser.PsObjList.ListItems.count
    If Val(Parser.PsObjList.ListItems(k).Text) = idOggetto Then
      Parser.PsObjList.ListItems(k).SubItems(PARS_DATE_OBJ) = FormattaData(Str(Now))
      Parser.PsObjList.ListItems(k).SubItems(RC_OBJ) = GbErrLevel
      If Trim(GbErrLevel) <> "" Then
        Parser.PsObjList.ListItems(k).SmallIcon = 5
      Else
        Parser.PsObjList.ListItems(k).SmallIcon = 6
      End If

      If Parser.PsObjList.ListItems(k).Selected Then
         Parser.PsObjList.ListItems(k).Selected = True
         'Parser.PsObjList.Parent.gridlist_ItemClick Parser.PsObjList.SelectedItem
         Parser.PsObjList.Parent.gridlist_Click
      End If
      Exit For
    End If
  Next k
  
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ritorna la lista (idOggetto, separati da virgole) degli item utilizzati da idOggetto;
' - Cerca gli oggetti, gli aggiunge al progetto, aggiorna relazioni/segnalazioni
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getItemList(idOggetto As Long, itemType As String, level As Integer) As String
  Dim rs As Recordset, rsOgg As Recordset
  Dim idItem As Long
  Dim relazione As String, oldCode As String, newCode As String, msgType As String, utilizzo As String
  Dim nomeOggettoR As String, OggettiR() As String, OggR() As String
  Dim i As Integer, a As Integer, isTrovato As Boolean, totOggR As Integer
  
  Select Case itemType
    Case "CBL"
      relazione = "CAL"   'per PsRel_Obj
      utilizzo = "CALL"   'per PsRel_Obj
      oldCode = "F03"     'per Bs_Segnalazioni
      newCode = "G03"     'per Bs_Segnalazioni
      msgType = "Program" 'per Bs_Segnalazioni (Bs_Messaggi)
    Case "NAT"
      relazione = "NAT"   'per PsRel_Obj
      utilizzo = "CALL"   'per PsRel_Obj
      oldCode = "F03"     'per Bs_Segnalazioni
      newCode = "G03"     'per Bs_Segnalazioni
      msgType = "Program" 'per Bs_Segnalazioni (Bs_Messaggi)
    Case "NIN"
      relazione = "CPY"   'per PsRel_Obj
      utilizzo = "NAT-INCLUDE"   'per PsRel_Obj
      oldCode = "F40"     'per Bs_Segnalazioni
      newCode = "G40"     'per Bs_Segnalazioni
      msgType = "Include" 'per Bs_Segnalazioni (Bs_Messaggi)
    Case "NCP"
      relazione = "CPY"   'per PsRel_Obj
      utilizzo = "NAT-COPY"   'per PsRel_Obj
      oldCode = "F41"     'per Bs_Segnalazioni
      newCode = "G41"     'per Bs_Segnalazioni
      msgType = "Copy" 'per Bs_Segnalazioni (Bs_Messaggi)
    Case "ASM"
      relazione = "CAL"   'per PsRel_Obj
      utilizzo = "CALL"   'per PsRel_Obj
      oldCode = "F03"     'per Bs_Segnalazioni
      newCode = "G03"     'per Bs_Segnalazioni
      msgType = "Program" 'per Bs_Segnalazioni (Bs_Messaggi)
    Case "CPY"
      relazione = "CPY"
      utilizzo = "COPY"
      oldCode = "F01"
      newCode = "G01"
      msgType = "Copy"
    Case "PRC"
      relazione = "PRC"
      'utilizzo = "JCL"
      utilizzo = "PROCJCL"
      oldCode = "F05"
      newCode = "G05"
      msgType = "Proc"
    Case "PGM"
      relazione = "PGM"
      utilizzo = "PGM"
      oldCode = "F03"
      newCode = "G03"
      msgType = "Program"
    Case "PSB"
      relazione = "PSB"
      utilizzo = "SCHEDULE"
      oldCode = "F04"
      newCode = "G04"
      msgType = "PSB"
    Case "PSB-JCL"
      relazione = "PSB"
      utilizzo = "PSB-JCL"
      oldCode = "F04"
      newCode = "G04"
      msgType = "PSB"
    Case "MBR"
      relazione = "MBR"
      utilizzo = "MEMBER"
      oldCode = "F28"
      newCode = "G28"
      msgType = "Member"
    Case "DBD"
      relazione = "DBD"
      utilizzo = "DBD"
      oldCode = "F06"
      newCode = "G06"
      msgType = "DBD"
    Case "INCLUDE"
      itemType = "CPY"  '!!!!!!!!!!!!!!!!!!!
      relazione = "CPY"
      utilizzo = "INCLUDE"
      oldCode = "F02"
      newCode = "G02"
      msgType = "SQL Member"
    Case "MAP"
      relazione = "MAP"
      utilizzo = "MAPSET"
      oldCode = "P08" 'verificare
      newCode = "G08"
      msgType = "Map"
    Case "LINK"
      relazione = "CAL"
      utilizzo = "LINK"
      oldCode = "F03"
      newCode = "G03"
      msgType = "Program"
    Case "XCTL"
      relazione = "CAL"
      utilizzo = "XCTL"
      oldCode = "F03"
      newCode = "G03"
      msgType = "Program"
    Case "INC" 'stefano: PLI
      relazione = "INC"   'per PsRel_Obj
      utilizzo = "PLI-INCLUDE"   'per PsRel_Obj
      oldCode = "F39"     'per Bs_Segnalazioni
      newCode = "G39"     'per Bs_Segnalazioni
      msgType = "Include" 'per Bs_Segnalazioni (Bs_Messaggi)
    Case "PLI"
      relazione = "CAL"   'per PsRel_Obj
      utilizzo = "CALL"   'per PsRel_Obj
      oldCode = "F03"     'per Bs_Segnalazioni
      newCode = "G03"     'per Bs_Segnalazioni
      msgType = "Program" 'per Bs_Segnalazioni (Bs_Messaggi)
    Case "EZT"  'da sistemare
      relazione = "CAL"   'per PsRel_Obj
      'utilizzo = "EZM"    'per PsRel_Obj
      utilizzo = "EZT-PGM"    'per PsRel_Obj
      oldCode = "F03"     'per Bs_Segnalazioni
      newCode = "G03"     'per Bs_Segnalazioni
      msgType = "Macro"   'per Bs_Segnalazioni (Bs_Messaggi)
   'T X SAS
    Case "SAS"  'da sistemare
      relazione = "CAL"   'per PsRel_Obj
      'utilizzo = "EZM"    'per PsRel_Obj
      utilizzo = "SAS-PGM"    'per PsRel_Obj
      oldCode = "F03"     'per Bs_Segnalazioni
      newCode = "G03"     'per Bs_Segnalazioni
      msgType = "Program"   'per Bs_Segnalazioni (Bs_Messaggi)
    Case "EZM"
      relazione = "CAL"   'per PsRel_Obj
      utilizzo = "EZM"    'per PsRel_Obj
      oldCode = "F37"     'per Bs_Segnalazioni
      newCode = "G37"     'per Bs_Segnalazioni
      msgType = "Macro"   'per Bs_Segnalazioni (Bs_Messaggi)
    ' Mauro 27/01/2009
    Case "PRM"
      relazione = "PRM"   'per PsRel_Obj
      utilizzo = "PRM"    'per PsRel_Obj
      oldCode = "P07"     'per Bs_Segnalazioni
      newCode = "P07"     'per Bs_Segnalazioni
      msgType = "PARSER"  'per Bs_Segnalazioni (Bs_Messaggi)
  End Select
  '''''''''''''''''''''''''
  ' item presenti:
  '''''''''''''''''''''''''
  'Set rs = m_Fun.Open_Recordset("select idOggettoR,NomeComponente from PsRel_Obj " & _
  '                                                  "Where idOggettoC = " & idOggetto & " and Relazione='" & relazione & "'")
  Set rs = m_Fun.Open_Recordset("select idOggettoR,NomeComponente from PsRel_Obj " & _
                                "Where idOggettoC = " & idOggetto & " and Utilizzo='" & utilizzo & "'")
  While Not rs.EOF
    getItemList = getItemList & rs!idOggettoR & ","
    displayAddingInfo level, idOggetto & "", msgType, rs!nomecomponente, "already present.", , itemType
    rs.MoveNext
  Wend
  rs.Close
  '''''''''''''''''''''''''
  ' item mancanti:
  '''''''''''''''''''''''''
  'Set rs = m_Fun.Open_Recordset("select NomeOggettoR from PsRel_ObjUnk " & _
  '                                                  "Where idOggettoC = " & idOggetto & " and Relazione='" & relazione & "'")
  Set rs = m_Fun.Open_Recordset("SELECT NomeOggettoR FROM PsRel_ObjUnk " & _
                                "WHERE idOggettoC = " & idOggetto & " AND Utilizzo='" & utilizzo & "'")
  While Not rs.EOF
    nomeOggettoR = rs!nomeOggettoR
    displayAddingInfo level, idOggetto & "", msgType, nomeOggettoR, "searching..."
    idItem = searchFile(nomeOggettoR, itemType)  'esegue il parsing I
    If idItem Then
      getItemList = getItemList & idItem & ","
      displayAddingInfo level, idOggetto & "", msgType, nomeOggettoR, "found.", idItem
      
      ''''''''''''''''''''''''''''''''''''''''
      ' Aggiornamento Relazioni/Segnalazioni!
      ''''''''''''''''''''''''''''''''''''''''
      ' PsRel_ObjUnk -> PsRel_Obj
      Dim rsRelazioni As Recordset
      'Set rsRelazioni = m_Fun.Open_Recordset("select * from PsRel_Obj")
      Set rsRelazioni = m_Fun.Open_Recordset("select * from PsRel_Obj where " & _
                              "IdOggettoC = " & idOggetto & " and " & _
                              "idOggettoR = " & idItem & " and " & _
                              "relazione = '" & relazione & "' and " & _
                              "NomeComponente = '" & nomeOggettoR & "'")
      If rsRelazioni.RecordCount Then
        displayAddingInfo level, idOggetto & "", msgType, rsRelazioni!nomecomponente, "already present.", , itemType
      Else
        rsRelazioni.AddNew
        rsRelazioni!IdOggettoC = idOggetto
        rsRelazioni!idOggettoR = idItem
        rsRelazioni!relazione = relazione
        rsRelazioni!nomecomponente = nomeOggettoR
        rsRelazioni!utilizzo = utilizzo
        rsRelazioni.Update
      End If
      rsRelazioni.Close
      ' Pulizia Bs_Segnalazioni
      Parser.PsConnection.Execute "DELETE * from Bs_segnalazioni Where idOggetto=" & idOggetto & " and Codice='" & oldCode & "'"
      
      'Eliminazione record PsRel_ObjUnk
      ' Mauro 14-05-2007 : Il record � gi� stato eliminato
      'rs.Delete
    Else
      displayAddingInfo level, idOggetto & "", msgType, nomeOggettoR, "not found."
      'UPDATE SEGNALAZIONE (Fxx->Pxx)! (file non esistente)
      Parser.PsConnection.Execute "UPDATE Bs_segnalazioni SET Codice='" & newCode & "',Var2='" & msgType & "' " & _
                                  "WHERE idOggetto=" & idOggetto & " and Codice='" & oldCode & "' and Var1='" & nomeOggettoR & "'"
    End If
    rs.MoveNext
    DoEvents
  Wend
  rs.Close
  ' Mauro 27/01/2009 : Gestione Control Card/Schede Parametro
  If utilizzo = "PRM" Then
    Set rs = m_Fun.Open_Recordset("SELECT distinct Member FROM PsJCL_Sked " & _
                                  "WHERE idOggetto = " & idOggetto)
    While Not rs.EOF
      ReDim OggettiR(0)
      nomeOggettoR = rs!member
      ' Mauro 24/02/2009 : Cerca i file utilizzando le variabili non risolte
      If InStr(nomeOggettoR, "&") Then
        OggettiR = MapsdM_JCL.getParameters(rs!member)
      Else
        OggettiR(0) = nomeOggettoR
      End If
      ' Faccio un giro preliminare per eliminare eventuali doppioni
      If UBound(OggettiR) Then
        ReDim OggR(UBound(OggettiR))
        OggR = OggettiR
        ReDim OggettiR(0)
        totOggR = 0
        For i = 0 To UBound(OggR)
          isTrovato = False
          For a = 0 To UBound(OggettiR)
            If OggR(i) = OggettiR(a) Then
              isTrovato = True
            End If
          Next a
          If Not isTrovato Then
            ReDim Preserve OggettiR(totOggR)
            OggettiR(totOggR) = OggR(i)
            totOggR = totOggR + 1
          End If
        Next i
      End If
      For i = 0 To UBound(OggettiR)
        displayAddingInfo level, idOggetto & "", msgType, OggettiR(i), "searching..."
        idItem = searchFile(OggettiR(i), itemType)  'esegue il parsing I
        If idItem = 0 Then
          Set rsOgg = m_Fun.Open_Recordset("select idoggetto from bs_oggetti where " & _
                                           "Nome = '" & OggettiR(i) & "' and tipo = 'PRM'")
          If rsOgg.RecordCount = 1 Then
            idItem = rsOgg!idOggetto
          End If
          rsOgg.Close
        End If
        If idItem = 0 Then
          idItem = searchFile(OggettiR(i), "UNK")  'esegue il parsing I
        End If
        If idItem Then
          getItemList = getItemList & idItem & ","
          displayAddingInfo level, idOggetto & "", msgType, OggettiR(i), "found.", idItem
          ' Pulizia Bs_Segnalazioni
          Parser.PsConnection.Execute "DELETE * from Bs_segnalazioni Where " & _
                                      "idOggetto=" & idOggetto & " and Codice='" & oldCode & "'"
        Else
          displayAddingInfo level, idOggetto & "", msgType, OggettiR(i), "not found."
          'UPDATE SEGNALAZIONE (Fxx->Pxx)! (file non esistente)
          Parser.PsConnection.Execute "UPDATE Bs_segnalazioni SET Codice='" & newCode & "',Var2='" & msgType & "' " & _
                                      "WHERE idOggetto=" & idOggetto & " and Codice='" & oldCode & "' and Var1='" & OggettiR(i) & "'"
        End If
      Next i
      rs.MoveNext
      DoEvents
    Wend
    rs.Close
  End If
  If Right(getItemList, 1) = "," Then getItemList = Left(getItemList, Len(getItemList) - 1) 'ultima virgola di troppo'''''''''''''''''''''''''
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Per addMissing...
' Gestione lista di oggetti gi� selezionati...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function newObject(idOggetto As String) As Boolean
  On Error GoTo notFound
  missingItemsSet.item idOggetto
  Exit Function
notFound:
  newObject = True
  missingItemsSet.Add CLng(idOggetto), idOggetto
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Fine Parsing livello II
' Perch� non vengono aggiunte direttamente al loro posto?
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub AggiornaRelIstrDli(wIdOggetto As Long)
  Dim tb As Recordset, tbr As Recordset
  'SQ CPY-ISTR
  Set tb = m_Fun.Open_Recordset("Select b.IdDBD,Nome from " & _
                                "PsDli_Istruzioni as a, PsDli_IstrDBD as b, bs_oggetti as c where " & _
                                "a.idoggetto = " & wIdOggetto & " and " & _
                                "a.idOggetto = b.idpgm and " & _
                                "a.idoggetto = c.idoggetto and " & _
                                "a.riga = b.riga")
  While Not tb.EOF
    AggiungiRelazione tb!idDBD, "IMS-DBD", tb!nome, tb!nome
    tb.MoveNext
  Wend
  tb.Close
  
  Set tb = m_Fun.Open_Recordset("Select b.Nome from PsDli_IstrDett_Liv as a,PsDli_segmenti as b where a.idoggetto = " & wIdOggetto & " AND a.idSegmento > 0 and a.idSegmento=b.idSegmento")
  While Not tb.EOF
    AggiungiComponente tb!nome, GbNomePgm, "SGM", "SEGMENT"
    tb.MoveNext
  Wend
  tb.Close
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Mauro: da finire... giro "al dritto" per PSB/PCB
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Modulo IMS
Sub SearchPSB(objectList As String, objtype As String, level As Integer)
  Dim objects() As String
  Dim i As Integer
  
  objects = Split(objectList, ",")
  
  For i = 0 To UBound(objects)
    If newObject(objects(i)) Then
      Select Case objtype
        Case "CBL", "CPY"
          '''''''''''''''''''''''''''''''''''''''''''
          ' "Esplosione" COPY
          ' Cerca tutte le copy utilizzate,
          ' aggiunge quelle mancanti al progetto e
          ' ritorna la lista degli idOggetto
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "CPY", level)
          If Len(objectList) Then
            SearchPSB objectList, "CPY", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' Programmi chiamati:
          ' aggiunge quelli mancanti al progetto e
          ' ritorna la lista degli idOggetto
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "CBL", level)
          If Len(objectList) Then
            SearchPSB objectList, "CBL", level + 1
          End If
          objectList = getPSBList(CLng(objects(i)), "XCTL", level)
          If Len(objectList) Then
            SearchPSB objectList, "CBL", level + 1
          End If
          objectList = getPSBList(CLng(objects(i)), "LINK", level)
          If Len(objectList) Then
            SearchPSB objectList, "CBL", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' SQL INCLUDE
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "INCLUDE", level)
          '''''''''''''''''''''''''''''''''''''''''''
          ' MAPPE
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "MAP", level)
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          updateReturnCode CLng(objects(i))
        
        Case "PGM"  'ocio ASM,PLI...
          '''''''''''''''''''''''''''''''''''''''''''
          ' PROGRAMMI BATCH
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "CPY", level)
          If Len(objectList) Then
            SearchPSB objectList, "CPY", level + 1
          End If
          objectList = getPSBList(CLng(objects(i)), "PGM", level)
          If Len(objectList) Then
            SearchPSB objectList, "CBL", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' SQL INCLUDE
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "INCLUDE", level)
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          updateReturnCode CLng(objects(i))
        
        Case "JCL", "JOB", "PRC", "MBR"
          '''''''''''''''''''''''''''''''''''''''''''
          ' MEMBER (INCLUDE)
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "MBR", level)
          If Len(objectList) Then
            SearchPSB objectList, "MBR", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' PROC
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "PRC", level)
          If Len(objectList) Then
            SearchPSB objectList, "PRC", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' PGM
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "PGM", level) 'ocio... non so se e' COBOL!
          If Len(objectList) Then
            SearchPSB objectList, "CBL", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' PGM CHIAMATI CON UTILITY (CALL)
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "CBL", level)
          If Len(objectList) Then
            SearchPSB objectList, "CBL", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' PSB
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "PSB-JCL", level)
          If Len(objectList) Then
            SearchPSB objectList, "PSB", level + 1
          End If
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          updateReturnCode CLng(objects(i))
        Case "PSB"
          '''''''''''''''''''''''''''''''''''''''''''
          ' DBD
          '''''''''''''''''''''''''''''''''''''''''''
          objectList = getPSBList(CLng(objects(i)), "DBD", level)
          '''''''''''''''''''''''''''''''''''''''''''
          ' Aggiornamento RC oggetto (db + lista)
          '''''''''''''''''''''''''''''''''''''''''''
          updateReturnCode CLng(objects(i))
        Case Else
          displayAddingInfo level, CLng(objects(i)) & "", objtype, "-", "already managed."
      End Select
    Else
      'gi� analizzato
    End If
  Next i
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ritorna la lista (idOggetto, separati da virgole) degli item utilizzati da idOggetto;
' - Cerca gli oggetti, gli aggiunge al progetto, aggiorna relazioni/segnalazioni
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getPSBList(idOggetto As Long, itemType As String, level As Integer) As String
  Dim rs As Recordset
  Dim idItem As Long
  Dim relazione As String, oldCode As String, newCode As String, msgType As String, utilizzo As String
  
  Select Case itemType
    Case "CBL"
      relazione = "CAL"   'per PsRel_Obj
      utilizzo = "CALL"   'per PsRel_Obj
      oldCode = "F03"     'per Bs_Segnalazioni
      newCode = "G03"     'per Bs_Segnalazioni
      msgType = "Program" 'per Bs_Segnalazioni (Bs_Messaggi)
    Case "CPY"
      relazione = "CPY"
      utilizzo = "COPY"
      oldCode = "F01"
      newCode = "G01"
      msgType = "Copy"
    Case "PRC"
      relazione = "PRC"
      utilizzo = "JCL"
      oldCode = "F05"
      newCode = "G05"
      msgType = "Proc"
    Case "PGM"
      relazione = "PGM"
      utilizzo = "PGM"
      oldCode = "F03"
      newCode = "G03"
      msgType = "Program"
    Case "PSB"
      relazione = "PSB"
      utilizzo = "SCHEDULE"
      oldCode = "F04"
      newCode = "G04"
      msgType = "PSB"
    Case "PSB-JCL"
      relazione = "PSB"
      utilizzo = "PSB-JCL"
      oldCode = "F04"
      newCode = "G04"
      msgType = "PSB"
    Case "MBR"
      relazione = "MBR"
      utilizzo = "MEMBER_JCL"
      oldCode = "F28"
      newCode = "G28"
      msgType = "Member"
    Case "DBD"
      relazione = "DBD"
      utilizzo = "DBD"
      oldCode = "F06"
      newCode = "G06"
      msgType = "DBD"
    Case "INCLUDE"
      itemType = "CPY"  '!!!!!!!!!!!!!!!!!!!
      relazione = "CPY"
      utilizzo = "INCLUDE"
      oldCode = "F02"
      newCode = "G02"
      msgType = "SQL Member"
    Case "MAP"
      relazione = "MAP"
      utilizzo = "MAPSET"
      oldCode = "P08" 'verificare
      newCode = "G08"
      msgType = "Map"
    Case "LINK"
      relazione = "CAL"
      utilizzo = "LINK"
      oldCode = "F03"
      newCode = "G03"
      msgType = "Program"
    Case "XCTL"
      relazione = "CAL"
      utilizzo = "XCTL"
      oldCode = "F03"
      newCode = "G03"
      msgType = "Program"
  End Select
  '''''''''''''''''''''''''
  ' item presenti:
  '''''''''''''''''''''''''
  Set rs = m_Fun.Open_Recordset("select idOggettoR,NomeComponente from PsRel_Obj " & _
                                                    "Where idOggettoC = " & idOggetto & " and Utilizzo='" & utilizzo & "'")
  While Not rs.EOF
    getPSBList = getPSBList & rs!idOggettoR & ","
    rs.MoveNext
  Wend
  rs.Close
  
  If Right(getPSBList, 1) = "," Then getPSBList = Left(getPSBList, Len(getPSBList) - 1) 'ultima virgola di troppo'''''''''''''''''''''''''
End Function

Function FormatX(Numero) As String
  Dim HH As Variant, MM As Variant, SS As Variant
  Dim wNumero As Variant
  
  wNumero = Numero
  
  HH = Int(wNumero / 3600)
  wNumero = wNumero - (HH * 3600)
  MM = Int(wNumero / 60)
  wNumero = wNumero - (MM * 60)
  SS = wNumero
  
  FormatX = format(HH, "00") & "." & format(MM, "00") & "." & format(SS, "00")
End Function
'''''''''''''''''''''''''''''''''''''''''
'SQ - 18-04-06
'Sbagliava con CPY e PGM omonimi...
'''''''''''''''''''''''''''''''''''''''''
Function ExistObjSyst(wNome As String, wtipo As String) As Boolean
  Dim rs As Recordset
  Dim types As String
  
  Select Case wtipo
    Case "PGM"
      types = "'PSY','ASM','CBL','PLI','EZT','NAT', 'SAS'"
    Case "EZT-PGM"  'SQ 28-05-07
      types = "'PSY','EZT'"
      'TMP:
      m_Fun.writeLog "!!!TMP: EZT MISSING: " & wNome
    Case "SAS-PGM"
      types = "'SAS'"
      'TMP:
      m_Fun.writeLog "!!!TMP: SAS MISSING: " & wNome
    Case "CPY", "COPY", "COPY-ASM"
      types = "'CPY'"
    Case "MAC"
      types = "'MAC'" 'TMP: MAC deve essere un tipo specifico!
    Case "EZM"
      types = "'CPY'" 'TMP: MAC deve essere un tipo specifico!
    Case "PRC", "JCL", "JOB"
      types = "'PRC','JCL','JOB','PSY'"
    Case "PLI-INCLUDE"
      types = "'INC'"
    Case Else
      MsgBox "tmp: ExistObjSyst, type unknown: " & wtipo, vbExclamation, "i-4.Migration"
      ExistObjSyst = False
      Exit Function
  End Select
  
  Set rs = m_Fun.Open_Recordset("Select * from psUte_ObjSys where " & _
                                "Nome = '" & wNome & "' and tipo in (" & types & ")")
  ExistObjSyst = rs.RecordCount > 0
  rs.Close
End Function

Sub AddObjUte(ObjUteName As String, ObjUteType As String, ObjUteDescr As String)
  Dim TbObjUte As Recordset
  
  Set TbObjUte = m_Fun.Open_Recordset("Select * from PsUte_objSys")
  If TbObjUte.RecordCount = 0 Then
    TbObjUte.AddNew
    TbObjUte!nome = ObjUteName
    TbObjUte!Tipo = ObjUteType
    TbObjUte!descrizione = ObjUteDescr
    TbObjUte.Update
  End If
  TbObjUte.Close
End Sub

Sub CaricaParametri()
  Dim TbParametri As Recordset
  Dim wParm As String, wtipo As String
   
  GbParmRel.AS_Macro = True
  GbParmRel.BM_Mapset = True
  GbParmRel.BM_Copy = True
  GbParmRel.MF_Mapset = True
  GbParmRel.MF_Copy = True
  GbParmRel.CB_Copy = True
  GbParmRel.CB_CallStat = True
  GbParmRel.CB_CallDyn = False
  GbParmRel.CB_Linkage = True
  GbParmRel.CX_Link = True
  GbParmRel.CX_XCTL = True
  GbParmRel.CX_Start_TR = True
  GbParmRel.CX_Return_TR = True
  GbParmRel.CX_Send = True
  GbParmRel.CX_Commarea = True
  GbParmRel.IMS_Call_Pgm = True
  GbParmRel.IMS_Transid = True
  GbParmRel.IMS_Call_TR = True
  GbParmRel.IMS_Send = True
  GbParmRel.IMS_SPAarea = True
  GbParmRel.DL_Pgm_Psb = True
  GbParmRel.DL_DecDli = False
  GbParmRel.DL_UseDli = True
  GbParmRel.VS_DecFile = False
  GbParmRel.VS_UseFile = True
  GbParmRel.RD_DecTable = False
  GbParmRel.RD_UseTable = True
  GbParmRel.JC_Proc = True
  GbParmRel.JC_Prog = True
  GbParmRel.JC_Utility = True
  GbParmRel.JC_Dlbl = True
  
  GbParmRel.DliStdPsb = False

  GbParmRel.DLIStdPsbName = LeggiParam("IMSDB_DEFPSB")
  If Trim(GbParmRel.DLIStdPsbName) <> "" Then
    GbParmRel.DliStdPsb = True
  End If
  GbParmRel.ImsStdPcb = False
  GbParmRel.ImsStdAltPcbName = LeggiParam("IMSDC_ALTPCB")
  If Trim(GbParmRel.ImsStdAltPcbName) <> "" Then
    GbParmRel.ImsStdPcb = True
  End If
  GbParmRel.ImsStdIoPcbName = LeggiParam("IMSDC_IOPCB")
  GbParmRel.ImsStdMstPcbName = LeggiParam("IMSDC_MSTPCB")
  GbParmRel.ImsStdSpaArea = LeggiParam("IMSDC_SPAAREA")
  GbParmRel.DM_DefSqlType = LeggiParam("DM_DEFSQLTYPE")
     
  pmExpldeCopy = param_explode_Copy
  
  'silvia 7-1-2008
  '''PsHideIgnore = LeggiParam("PS_HIDE_IGNORE") = "YES"
   ' IRIS-DB: modifica fatta male in precedenza, reinserita, da rivedere
  PsHideIgnore = LeggiParam("PS_HIDE_IGNORE") = "YES"
End Sub

Function LeggiParam(wKey As String) As String
  Dim wResp() As String
  On Error GoTo ErrResp
  
  wResp = m_Fun.RetrieveParameterValues(wKey)
  If InStr(wResp(0), wKey) > 0 Then
    LeggiParam = ""
  Else
    LeggiParam = wResp(0)
  End If
  Exit Function
ErrResp:
  LeggiParam = ""
End Function

Public Function Carica_Menu() As Boolean
  SwMenu = True
  
  'inizializza la dll rendendo visibili le variabili della classe anche alle form
  Carica_Menu = True
  
  'crea il menu in memoria
  ReDim PsMenu(0)

  '1) Sottobottone : Project
  ReDim Preserve PsMenu(UBound(PsMenu) + 1)
  PsMenu(UBound(PsMenu)).Id = 306
  PsMenu(UBound(PsMenu)).label = "Parser"
  PsMenu(UBound(PsMenu)).ToolTipText = ""
  PsMenu(UBound(PsMenu)).PictureEn = Parser.PsImgDir & "\Maps_Parser_Enabled.ico"
  PsMenu(UBound(PsMenu)).Picture = Parser.PsImgDir & "\Maps_Parser_Disabled.ico"
  PsMenu(UBound(PsMenu)).M1Name = ""
  PsMenu(UBound(PsMenu)).M1SubName = ""
  PsMenu(UBound(PsMenu)).M1Level = 0
  PsMenu(UBound(PsMenu)).M2Name = "Project"
  PsMenu(UBound(PsMenu)).M3Name = ""
  PsMenu(UBound(PsMenu)).M4Name = ""
  PsMenu(UBound(PsMenu)).Funzione = "Show_PrjParser"
  PsMenu(UBound(PsMenu)).DllName = "MapsdP_00"
  PsMenu(UBound(PsMenu)).TipoFinIm = "Immediata"
    
'  ReDim Preserve PsMenu(UBound(PsMenu) + 1)
'  PsMenu(UBound(PsMenu)).Id = 308
'  PsMenu(UBound(PsMenu)).Label = "Restore Object"
'  PsMenu(UBound(PsMenu)).ToolTipText = "Restore Object"
'  PsMenu(UBound(PsMenu)).Picture = "1"
'  PsMenu(UBound(PsMenu)).M1Name = ""
'  PsMenu(UBound(PsMenu)).M1SubName = ""
'  PsMenu(UBound(PsMenu)).M1Level = 0
'  PsMenu(UBound(PsMenu)).M2Name = ""
'  PsMenu(UBound(PsMenu)).M3Name = "Restore Project"
'  PsMenu(UBound(PsMenu)).M3SubName = ""
'  PsMenu(UBound(PsMenu)).M3ButtonType = "Normal"
'  PsMenu(UBound(PsMenu)).M4Name = ""
'  PsMenu(UBound(PsMenu)).M4ButtonType = ""
'  PsMenu(UBound(PsMenu)).M4SubName = ""
'  PsMenu(UBound(PsMenu)).Funzione = "Restore_Object"
'  PsMenu(UBound(PsMenu)).DllName = "MapsdP_00"
'  PsMenu(UBound(PsMenu)).TipoFinIm = ""
    
'  '3) Sottobottone : Project
'  ReDim Preserve PsMenu(UBound(PsMenu) + 1)
'  PsMenu(UBound(PsMenu)).Id = 308
'  PsMenu(UBound(PsMenu)).Label = "Query Repository"
'  PsMenu(UBound(PsMenu)).ToolTipText = ""
'  PsMenu(UBound(PsMenu)).PictureEn = Parser.PsImgDir & "\jcls.ico"
'  PsMenu(UBound(PsMenu)).Picture = Parser.PsImgDir & "\jcls.ico"
'  PsMenu(UBound(PsMenu)).M1Name = ""
'  PsMenu(UBound(PsMenu)).M1SubName = ""
'  PsMenu(UBound(PsMenu)).M1Level = 0
'  PsMenu(UBound(PsMenu)).M2Name = "Project"
'  PsMenu(UBound(PsMenu)).M3Name = ""
'  PsMenu(UBound(PsMenu)).M4Name = ""
'  PsMenu(UBound(PsMenu)).Funzione = "Query_Repository"
'  PsMenu(UBound(PsMenu)).DllName = "MapsdP_00"
'  PsMenu(UBound(PsMenu)).TipoFinIm = "Immediata"
End Function

Sub SetMsgFin(Stringa)
  Parser.PsFinestra.ListItems.Add , , format(Now, "HH:MM:SS") & " - " & Stringa
  Parser.PsFinestra.Refresh
End Sub

''Sub SetMsgFinp(Stringa)
''  Dim wTesto As String
''
''  wTesto = format(Now, "HH:MM:SS") & " - " & Stringa & vbCrLf
''
''  MapsdF_Parser.RTErr.SelStart = Len(MapsdF_Parser.RTErr.text)
''  MapsdF_Parser.RTErr.SelLength = 0
''  MapsdF_Parser.RTErr.SelText = wTesto
''  MapsdF_Parser.RTErr.Refresh
''End Sub
''
''Function ConvertiBool(Stringa As String) As Boolean
''   ConvertiBool = False
''   If Trim(UCase(Stringa)) = "T" Then ConvertiBool = True
''End Function

Function ApriProgetto() As Boolean
  Dim TbParametri As Recordset
  Dim wtipo As String
  Dim wParm As String
   
  CaricaParametri
   
  ApriProgetto = True
End Function

Sub AggiungiRelazione(idOggettoR, utilizzo As String, wNomeOgg, wNomeComp, Optional ByVal isDuplicated As Boolean = False)
  'On Error GoTo errDb
  Dim rs As Recordset
  
  'SQ: FARLO FUORI 'STO CONTROLLO!
  If Trim(idOggettoR & "") = "" Then Exit Sub
  
  If isDuplicated Then
    Set TbRela = m_Fun.Open_Recordset("select * from PsRel_ObjDup where idoggettoc = " & GbIdOggetto & " and idoggettor = " & idOggettoR)
  Else
    Set TbRela = m_Fun.Open_Recordset("select * from PsRel_Obj where idoggettoc = " & GbIdOggetto & " and idoggettor = " & idOggettoR)
  End If
  
  If TbRela.RecordCount = 0 Then
    TbRela.AddNew
    TbRela!IdOggettoC = GbIdOggetto
    TbRela!idOggettoR = idOggettoR
    
    Select Case utilizzo
      Case "MFS"
        TbRela!relazione = "MFS"
      Case "COPY", "COPY-ASM", "INCLUDE"
        TbRela!relazione = "CPY"
      Case "CALL", "XCTL", "LINK", "MAC", "EZM" '1-10-06: Macro ASM
        TbRela!relazione = "CAL"
        'T 10-07-2009
        Set rs = m_Fun.Open_Recordset("select * from PsPgm where idoggetto = " & idOggettoR) 'virgilio
        If rs.RecordCount Then
          rs!main = True
          rs.Update
        End If
        rs.Close
      Case "QUIK-PGM", "EZT-PGM", "SAS-PGM" '<--TILVIA
        TbRela!relazione = "PGM"
      Case "BUILTIN"
         TbRela!relazione = "FUN"
      Case "IMS-DBD"
         TbRela!relazione = "DBD"
      Case "SEGMENT"
         TbRela!relazione = "SGM"
      'SQ 5-11-05
      Case "INCLUDE_JCL"
        TbRela!relazione = "INC"
      'spli
      Case "PSB-JCL", "PSB-IMS" 'SQ - VERIFICARE... E PORTARE in quella NUOVA!
        TbRela!relazione = "PSB"
        wNomeComp = wNomeOgg
        '''''''''''''''''''''''''''''''''''''
        'SQ 19 - 4 - 6
        'Aggiornamento eventuale segnalazione
        '''''''''''''''''''''''''''''''''''''
        Parser.PsConnection.Execute "Delete * from Bs_Segnalazioni where idOggetto =" & GbIdOggetto & " and codice='F07'"
      Case Else
 '       Stop
        TbRela!relazione = Left(utilizzo & Space(3), 3)
    End Select
    
    TbRela!nomecomponente = wNomeComp
    TbRela!utilizzo = utilizzo
    
    TbRela.Update
  End If
  TbRela.Close
  Exit Sub
'errDb:
  'gestire...
 ' MsgBox Err.Description
End Sub

Sub AggiungiComponente(NomeR As String, NomeC As String, rTipo, wUtilizzo)
  Dim wNomeR As String
  Dim wNomeC As String
  
  wNomeR = NormNomeObj(NomeR)
  'SQ: PEZZA: SE ARRIVA CON L'APICE E' GIA' SBAGLIATO...
  wNomeC = Replace(NormNomeObj(NomeC), "'", "''")
  
  Set TbRela = m_Fun.Open_Recordset("select * from PsCom_Obj where " & _
                                    "idoggettoc = " & GbIdOggetto & " and nomecomponente = '" & wNomeC & "' and " & _
                                    "nomeoggettor= '" & wNomeR & "'and relazione = '" & rTipo & "' ")
  If TbRela.RecordCount = 0 Then
    TbRela.AddNew
    TbRela!IdOggettoC = GbIdOggetto
    TbRela!relazione = rTipo
    TbRela!nomeOggettoR = wNomeR
    TbRela!nomecomponente = wNomeC
    TbRela!utilizzo = wUtilizzo
    TbRela.Update
  End If
  TbRela.Close
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Mettere in linea... buttare via "AggiungiSegnalazione"
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub addSegnalazione(parameter As String, errorAlias As String, item As String)
  Dim rs As Recordset
  Dim errorCode As String, errorWeight As String, relation As String, utilizzo As String
  Dim segnOk As Boolean
  
  Set rs = m_Fun.Open_Recordset_Sys("select * from Bs_Messaggi where Alias ='" & errorAlias & "'")
  If Not rs.EOF Then
    errorCode = rs!codice
    errorWeight = rs!gravita
    relation = IIf(IsNull(rs!relazione), "", rs!relazione)
    utilizzo = IIf(IsNull(rs!utilizzo), "", rs!utilizzo)
  Else
    'SQ - "quasi fuori linea..." farlo per tutti!!!!
    utilizzo = Mid(errorAlias, 3)
    Set rs = m_Fun.Open_Recordset_Sys("select relazione,codiceErrore from usesRelations where utilizzo='" & utilizzo & "'")
    If rs.RecordCount Then
      relation = rs!relazione
      errorCode = rs!codiceErrore & ""
      'Ci vuole anche la gravit�!!!!!! Che poi andrebbe tolta dalla Segnalazioni!!!
      Set rs = m_Fun.Open_Recordset_Sys("select gravita from Bs_Messaggi where codice='" & errorCode & "'")
      If rs.RecordCount Then
        errorWeight = rs!gravita & ""
      Else
        errorWeight = "W"
      End If
    Else
      errorCode = errorAlias
      errorWeight = "W"
    End If
  End If
  rs.Close
  
  segnOk = True
  If PsHideIgnore Then
    Set rs = m_Fun.Open_Recordset( _
      "select * from Bs_Segnalazioni_Ignore where code='" & errorCode & "' and var1 = 'ALL'")
    If rs.EOF Then
      rs.Close
      Set rs = m_Fun.Open_Recordset( _
        "select * from Bs_Segnalazioni_Ignore where code='" & errorCode & "' and var1='" & Replace(parameter, "'", "''") & "'")
    End If
    If Not rs.EOF Then
      segnOk = False
      errorWeight = ""
      GbErrLevel = ""
    Else
      Parser.SettaErrLevel errorWeight, "PARSER"
    End If
    rs.Close
  Else
    Parser.SettaErrLevel errorWeight, "PARSER"
  End If
  
  If errorCode <> "F11" And segnOk Then
    On Error Resume Next
    'AC 12/07/07 esordisce IdOggettoRel (chiave) nella tabella delle segnalazioni
    Set rs = m_Fun.Open_Recordset( _
      "select * from Bs_Segnalazioni where idoggetto=" & GbIdOggetto & " and IdOggettoRel = " & GbSegnObjRel & " and riga=" _
      & GbOldNumRec & " and codice='" & errorCode & "' and var1='" & Replace(parameter, "'", "''") & "'")
    If rs.EOF Then
      rs.AddNew
      rs!idOggetto = GbIdOggetto
      rs!IdOggettoRel = GbSegnObjRel
      rs!Riga = GbOldNumRec
      rs!codice = errorCode
      rs!Origine = "PS" 'Chi te l'ha detto?!
      rs!data_segnalazione = Now
      rs!Var1 = Left(parameter, 255) 'SQ: perche' non uso direttamente parameter???????????????
      rs!var2 = Left(item, 255)
      rs!gravita = errorWeight
      rs!Tipologia = "PARSER" 'Chi te l'ha detto?!
      rs.Update
    End If
    rs.Close
  End If

  If errorAlias <> "P00" Then
    parameter = Replace(parameter, "'", "''")
    Set rs = m_Fun.Open_Recordset("select * from PsRel_objunk where idoggettoc = " & GbIdOggetto & " and nomeoggettor = '" & parameter & "' and relazione = '" & relation & "'")
    If rs.EOF And Not utilizzo = "" Then 'in origine so,lo alcuni casi, quelli che in tabella hanno utilizzo not Null
      rs.AddNew
      rs!IdOggettoC = GbIdOggetto
      rs!nomeOggettoR = parameter
      rs!nomecomponente = item
      rs!relazione = relation
      rs!utilizzo = utilizzo
      rs.Update
    End If
    rs.Close
  End If
End Sub
'''SQ - UTILIZZARE addSegnalazione
''Sub AggiungiSegnalazione(eVar1 As String, eTipoErr As String, eNomeComp As String)
''  Dim wVar1 As String, wVar2 As String, wCodice As String, wPeso As String, wRel As String
''  Dim Riga As Long
''  Dim tb As Recordset
''
''  Riga = GbOldNumRec
''
''  'SQ:
''  'Da quel che capisco, 'sto case serve per utilizzare ALIAS (tipo NOCOPY) anzich� il codice...
''  Select Case eTipoErr
''    'SQ 21-12-05: generic Parser error (nextToken,...)
''    Case "P00"
''      wVar1 = Replace(eVar1, "'", "''")
''      wCodice = eTipoErr
''      wPeso = "0" 'SQ...
''      wRel = "..."
''    Case "NOCOPY"
''      wVar1 = eVar1
''      wCodice = "F01"
''      wPeso = "E"
''      wRel = "CPY"
''    Case "NOPGM" '1-03-06: EXECPGM
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "F03"  'OCIO: E' QUELLO DELLE CALL...
''      wPeso = "E"
''      wRel = "PGM"
''    Case "NOPGMVALUE" '2-03-06: EXECPGM
''      wVar1 = eVar1
''      wCodice = "H03"  'OCIO: E' QUELLO DELLE CALL...
''      wPeso = "W"
''      wRel = "PGM"
''    Case "NOQUIK-PGM" '27/09/: EXEC QUIKJOB
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "F31"
''      wPeso = "E"
''      wRel = "PGM"
''    Case "NOQUIK-INCLUDE" '1/10/06/: QUIKIMS
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "F32"
''      wPeso = "E"
''      wRel = "PGM"
''    Case "NOEZT-PGM" '27/09/: EXEC QUIKJOB
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "F35"
''      wPeso = "E"
''      wRel = "PGM"
''    Case "NOINCLUDE"
''      wVar1 = eVar1
''      wCodice = "F02"
''      wPeso = "E"
''      wRel = "CPY"
''    Case "NOCALL", "NOLINK", "NOXCTL", "NOLOAD"
''      wVar1 = eVar1
''      wCodice = "F03"
''      wPeso = "W"
''      wRel = "CAL"
''    'SQ 17-02-06
''    Case "NOCALLVALUE", "NOLINKVALUE", "NOXCTLVALUE", "NOLOADVALUE"
''      'SQ 23-02-06 - Messaggio comune
''      wCodice = "H03" '   ex F27
''      wVar1 = eVar1
''      wPeso = "W"
''      wRel = "CAL"
''    'SQ 1-10-06
''    Case "NOMAC"
''      wCodice = "F33"
''      wVar1 = eVar1
''      wPeso = "E"
''      wRel = "CAL"
''    'SQ 1-10-06
''    Case "NOEZM"
''      wCodice = "F37"
''      wVar1 = eVar1
''      wPeso = "E"
''      wRel = "CAL"
''    Case "NOCOPY-ASM"
''      wCodice = "F34"
''      wVar1 = eVar1
''      wPeso = "E"
''      wRel = "CPY"
''    Case "NOPSB"
''      wVar1 = eVar1
''      wCodice = "F04"
''      wPeso = "W"
''      wRel = "PSB"
''    Case "NOPSBVALUE"
''      wVar1 = eVar1
''      wCodice = "H04"
''      wPeso = "W" ' ma se e' in tabella????????????????????????????????????
''      wRel = "PSB"
''    Case "NOPSB-JCL"  'SQ 8-03-06
''      wVar1 = eVar1
''      wCodice = "F04"
''      wPeso = "W"
''      wRel = "PSB"
''    Case "NODATASETVALUE"
''      wVar1 = eVar1
''      wCodice = "H20" 'ex P10
''      wPeso = "W" ' ma se e' in tabella????????????????????????????????????
''      wRel = "AGN"  'quella della PsCom_Obj... (?)
''    Case "NODATASETFOUND"
''      wVar1 = eVar1
''      wVar2 = "DATASET"
''      wCodice = "P11"
''      wPeso = "W" ' ma se e' in tabella????????????????????????????????????
''      wRel = "AGN"  'quella della PsCom_Obj... (?)
''    Case "NOPRC"
''      wVar1 = eVar1
''      wCodice = "F05"
''      wPeso = "W"
''      wRel = "PRC"
''    Case "NOPRCVALUE"
''      wVar1 = eVar1
''      wCodice = "H05"
''      wPeso = "W"
''      wRel = "PRC"
''    Case "NODBD"
''      wVar1 = eVar1
''      wCodice = "F06"
''      wPeso = "W"
''      wRel = "DBD"
''    Case "NORELPSB"
''      Riga = 0  'legato al programma
''      wCodice = "F07"
''      wPeso = "E"
''      wRel = "PSB"
''    Case "MANYRELPSB"
''      Riga = 0  'legato al programma
''      wCodice = "F36"
''      wPeso = "W"
''      wRel = "PSB"
''      'SQ: a chi serve?
''      'Eliminare le chiamate!
''      Exit Sub
''    Case "NODECOD"
''      wCodice = "F08"
''      wPeso = "E"
''      wRel = "DCD"
''      'wNumRec = GbOldNumRec
''    Case "NOISTRDLI"
''      wVar1 = eVar1
''      wCodice = "F09"
''      wPeso = "E"
''      wRel = "IST"
''      'wNumRec = GbOldNumRec
''    Case "NOISTRDLIVALUE"
''      wVar1 = eVar1
''      wCodice = "H09"
''      wPeso = "W" ' ma se e' in tabella????????????????????????????????????
''      wRel = "IST"
''    Case "NOSEGM"
''      wVar1 = eVar1
''      wCodice = "F10"
''      wPeso = "E"
''      wRel = "SEG"
''      'wNumRec = GbOldNumRec
'''    Case "NOMAPPING"  'SQ '????
'''      wVar1 = eVar1
'''      wCodice = "F11"
'''      wPeso = "E"
'''      wRel = "MAP"
''    'SQ 16-02-06
''    Case "NOMAPSET"
''      wVar1 = eVar1
''      wCodice = "P08"
''      wPeso = "E"
''      wRel = "MAP"
''    Case "NOMAPSETVALUE"
''      wVar1 = eVar1
''      wCodice = "H08" 'ex P09
''      wPeso = "E"
''      wRel = "MAP"
''    Case "NOMEMBER", "NOMEMBER_JCL"
''      wVar1 = eVar1
''      wCodice = "F28"
''      wPeso = "E"
''      wRel = "MBR"
''    Case "NODD"
''      wVar1 = eVar1
''      wCodice = "F21"
''      wPeso = "E"
''      wRel = "JCL"
''    Case "NOTYPECOL"
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "F22"
''      wPeso = "E"
''      wRel = "DDL"
''   Case "NOTBIDX"
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "F23"
''      wPeso = "E"
''      wRel = "DDL"
''   Case "NOTBFND"
''      wVar1 = eVar1
''      wCodice = "F24"
''      wPeso = "E"
''      wRel = "DDL"
''   Case "NOCOLIDX"
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "F25"
''      wPeso = "E"
''      wRel = "DDL"
''   Case "NOISTRDDL"
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "F26"
''      wPeso = "E"
''      wRel = "DDL"
''    Case "NOSK" 'Scheda parametro
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "P07"
''      wPeso = "W"
''      wRel = "JCL"
''    Case "NOSKVALUE" 'Scheda parametro
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "H07"
''      wPeso = "W"
''      wRel = "JCL"
''    Case "DUPLICATEDNAMES"
''      wVar1 = eNomeComp
''      wVar2 = eVar1
''      wCodice = "P12"
''      wPeso = "W"
''      wRel = "MAP"
''    Case "NOCOPYMAPFIELD"
''      wVar1 = eNomeComp
''      wVar2 = eVar1
''      wCodice = "F29"
''      wPeso = "E"
''      wRel = "MAP"
''    Case "NOMAPMODNAME"
''      wVar1 = eNomeComp
''      wVar2 = eVar1
''      wCodice = "F30"
''      wPeso = "E"
''      wRel = "MAP"
''    Case "NOMAPMODNAMEVALUE"
''      wVar1 = eNomeComp
''      wVar2 = eVar1
''      wCodice = "H30"
''      wPeso = "W"
''      wRel = "MAP"
''    Case "INVALIDDLIISTR"
''      wVar1 = eNomeComp
''      wVar2 = eVar1
''      wCodice = "I01"
''      wPeso = "E"
''      wRel = "IST"
''   Case "REXX"  '9-03-06: tmp - solo segnalazione (no relazioni)
''      wVar1 = eVar1
''      wVar2 = eNomeComp
''      wCodice = "P13"
''      wPeso = "I"
''      wRel = "JCL"
''    Case "NOSSAVALUE"
''      wVar1 = eNomeComp
''      wVar2 = eVar1
''      wCodice = "I02"
''      wPeso = "E"
''      wRel = "IST"
''    Case "SSAWRONG"
''      wVar1 = eNomeComp
''      wVar2 = eVar1
''      wCodice = "I03"
''      wPeso = "E"
''      wRel = "IST"
''    Case "SSAKEYNOTFOUND"
''      wVar1 = eNomeComp
''      wVar2 = eVar1
''      wCodice = "I05"
''      wPeso = "E"
''      wRel = "IST"
''    Case "UNKNOWNOPERATOR"
''      wVar1 = eNomeComp
''      wVar2 = eVar1
''      wCodice = "I06"
''      wPeso = "E"
''      wRel = "IST"
''    Case "UNKPCB"
''      wVar1 = eNomeComp
''      wVar2 = eVar1
''      wCodice = "I07"
''      wPeso = "E"
''      wRel = "IST"
''    Case Else
''      wPeso = "W"
''      wVar1 = eVar1
''      wCodice = eTipoErr
''  End Select
''
''  If wCodice <> "F11" Then
''    On Error Resume Next
''    Dim TbSegnalazioni As Recordset
''    Set TbSegnalazioni = m_Fun.Open_Recordset( _
''      "select * from Bs_Segnalazioni where idoggetto=" & GbIdOggetto & " and riga=" & Riga & _
''      " and codice='" & wCodice & "' and var1='" & wVar1 & "'")
''    If TbSegnalazioni.RecordCount = 0 Then
''      TbSegnalazioni.AddNew
''      TbSegnalazioni!idOggetto = GbIdOggetto
''      'SQ TbSegnalazioni!Riga = wNumRec
''      TbSegnalazioni!Riga = Riga
''      TbSegnalazioni!codice = wCodice
''      TbSegnalazioni!Origine = "PS" 'Chi te l'ha detto?!
''      TbSegnalazioni!data_segnalazione = Now
''      TbSegnalazioni!Var1 = Left(wVar1, 255) 'SQ: perche' non uso direttamente eVar1???????????????
''      TbSegnalazioni!var2 = Left(wVar2, 255)
''      TbSegnalazioni!gravita = wPeso
''      TbSegnalazioni!Tipologia = "PARSER" 'Chi te l'ha detto?!
''      TbSegnalazioni.Update
''    End If
''    TbSegnalazioni.Close
''  End If
''
''  Parser.SettaErrLevel wPeso, "PARSER"
''
''  If eTipoErr <> "P00" Then
''    'SQ: provvisorio - problema: arriva eVar1 con apici...
''    eVar1 = Replace(eVar1, "'", "''")
''    Dim TbRelaObjUnk As Recordset
''    Set TbRelaObjUnk = m_Fun.Open_Recordset("select * from PsRel_objunk where idoggettoc = " & GbIdOggetto & " and nomeoggettor = '" & eVar1 & "' and relazione = '" & wRel & "'")
''    If TbRelaObjUnk.RecordCount = 0 Then
''      Select Case eTipoErr
''       Case "NOMEMBER", "NOMEMBER_JCL"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "MBR"
''          TbRelaObjUnk!utilizzo = "MEMBER_JCL"
''          TbRelaObjUnk.Update
''       Case "NOCOPY"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "CPY"
''          TbRelaObjUnk!utilizzo = "COPY"
''          TbRelaObjUnk.Update
''       Case "NOINCLUDE"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "CPY"
''          TbRelaObjUnk!utilizzo = "INCLUDE"
''          TbRelaObjUnk.Update
''       Case "NOCALL"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "CAL"
''          TbRelaObjUnk!utilizzo = "CALL"
''          TbRelaObjUnk.Update
''       Case "NOMAC", "NOEZM"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "CAL"
''          TbRelaObjUnk!utilizzo = "CALL"
''          TbRelaObjUnk.Update
''       Case "NOCOPY-ASM"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "CPY"
''          TbRelaObjUnk!utilizzo = "COPY-ASM"
''          TbRelaObjUnk.Update
''       Case "NOPGM" '1-03-06: EXECPGM
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "PGM"
''          TbRelaObjUnk!utilizzo = "PGM"
''          TbRelaObjUnk.Update
''       Case "NOQUIK-PGM"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "PGM"
''          TbRelaObjUnk!utilizzo = "QUIK-PGM"
''          TbRelaObjUnk.Update
''       Case "NOEZT-PGM"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "PGM"
''          TbRelaObjUnk!utilizzo = "EZT-PGM"
''          TbRelaObjUnk.Update
''       Case "NOLINK"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "CAL"
''          TbRelaObjUnk!utilizzo = "LINK"
''          TbRelaObjUnk.Update
''       Case "NOXCTL"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "CAL"
''          TbRelaObjUnk!utilizzo = "XCTL"
''          TbRelaObjUnk.Update
''       Case "NOPRC"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = Trim(eVar1)
''          TbRelaObjUnk!nomecomponente = Left(Trim(eNomeComp), 30) 'SQ: max len colonna
''          TbRelaObjUnk!relazione = "PRC"
''          TbRelaObjUnk!utilizzo = "JCL"
''          TbRelaObjUnk.Update
''       Case "NOPSB"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "PSB"
''          TbRelaObjUnk!utilizzo = "SCHEDULE"
''          TbRelaObjUnk.Update
''      Case "NOPSB-JCL"  'SQ 8-03-06
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "PSB"
''          TbRelaObjUnk!utilizzo = "PSB-JCL"
''          TbRelaObjUnk.Update
''       Case "NODBD"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "DBD"
''          TbRelaObjUnk!utilizzo = "DBD"
''          TbRelaObjUnk.Update
''       Case "NOMAPSET"
''          TbRelaObjUnk.AddNew
''          TbRelaObjUnk!IdOggettoC = GbIdOggetto
''          TbRelaObjUnk!nomeOggettoR = eVar1
''          TbRelaObjUnk!nomecomponente = eNomeComp
''          TbRelaObjUnk!relazione = "MAP"
''          TbRelaObjUnk!utilizzo = "MAPSET"
''          TbRelaObjUnk.Update
''       'Case "NOMAPMODNAME"
''          '
''       Case Else
''          'NOP
''      End Select
''    End If
''    TbRelaObjUnk.Close
''  End If
''End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Dovr� sostituire "AggiungiRelazione"
' Utilizzo tabella di sistema "usesRelations"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
'deve essere Private
Public Sub addRelazione(idOggettoR As Long, utilizzo As String, itemName As String, componentName As String, Optional ByVal isDuplicated As Boolean = False)
  Dim rs As Recordset
  Dim relation As String
  
  Set rs = m_Fun.Open_Recordset_Sys("select relazione from usesRelations where utilizzo = '" & utilizzo & "'")
  If rs.RecordCount Then
    relation = rs!relazione
  Else
    relation = Left(utilizzo, 3)
  End If
  rs.Close
  If isDuplicated Then
    Set rs = m_Fun.Open_Recordset("select * from PsRel_ObjDup where idoggettoc = " & GbIdOggetto & " and idoggettor = " & idOggettoR)
  Else
    Set rs = m_Fun.Open_Recordset("select * from PsRel_Obj where idoggettoc = " & GbIdOggetto & " and idoggettor = " & idOggettoR)
  End If
  If rs.RecordCount = 0 Then
    rs.AddNew
    rs!IdOggettoC = GbIdOggetto
    rs!idOggettoR = idOggettoR
    rs!relazione = relation
    'SQ - Chiarire questa eccezione!
    If utilizzo = "PSB-JCL" Then
      componentName = itemName
      '''''''''''''''''''''''''''''''''''''
      'SQ Aggiornamento eventuale segnalazione
      '''''''''''''''''''''''''''''''''''''
      Parser.PsConnection.Execute "Delete * from Bs_Segnalazioni where idOggetto =" & GbIdOggetto & " and codice = 'F07'"
    End If
    rs!nomecomponente = componentName
    rs!utilizzo = utilizzo
    rs.Update
  End If
  rs.Close
End Sub

'Function CreaRelExec(wbuffer As String) As String
'...
'End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' A COSA SERVE 'STO CASINO?
' PER ORA BRASATO TUTTE LE CHIAMATE!
' ...LA TABELLA PsExec_Param (ex PsSys_IstrParm) non era usata in nessun progetto!!!!!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function CreaIstrAmbExec(wId As Long, wNumRiga As Long, wbuffer As String, wAmb As String) As Boolean
  'Dim wAmb As String
  Dim wComando As String
  Dim k As Integer
  Dim wParam() As String
  Dim Ind As Long
  Dim wP As String, wV As String
  Dim wStr1 As String, Wstr2 As String, wStr3 As String
  Dim TbIstr As Recordset

  k = InStr(wbuffer, " ")
  If k Then
    wComando = Mid$(wbuffer, 1, k - 1)
  Else
    wComando = wbuffer
  End If
  wStr1 = wbuffer
  
  ReDim wParam(0)
  wStr1 = Replace(wStr1, ".", " ")
  wStr1 = Replace(wStr1, ",", " ")
  wStr1 = Replace(wStr1, "(", " (")
  wStr1 = Replace(wStr1, ")", ") ")
  
  While Len(wStr1) > 0
    k = InStr(wStr1, " ")
    If k = 0 Then k = Len(wStr1)
    Wstr2 = Mid$(wStr1, 1, k)
    wStr1 = Mid$(wStr1, k + 1)
    Wstr2 = LTrim(Wstr2)
    If Mid$(Wstr2, 1, 1) = "(" Then
      While SeBilanciaParentesi(Wstr2) <> 0
        k = InStr(wStr1, ")")
        If k > 0 Then
          Wstr2 = Wstr2 & Mid$(wStr1, 1, k)
          wStr1 = Trim(Mid$(wStr1, k + 1))
        Else
          Wstr2 = Wstr2 & wStr1 & ")"
          wStr1 = ""
        End If
      Wend
    End If
    Select Case UCase(Trim(Wstr2))
      Case "CICS", "DLI", "SQL", "EXEC", "END-EXEC", "CALL", "CBLTDLI", "DLITCBL", "SIXTDLI", "AERTDLI", "DBINTDLI"
      Case Else
        Ind = UBound(wParam) + 1
        ReDim Preserve wParam(Ind)
        wParam(Ind) = UCase(Trim(Wstr2))
    End Select
  Wend
  
  k = 0
  'SQ: 1-2-06 - ex PsSys_IstrParm
  Set TbIstr = m_Fun.Open_Recordset("select * from PsExec_Param where idoggetto = " & wId)
  For Ind = 1 To UBound(wParam)
    If SeParamIstr(wParam(Ind), wAmb, wComando) Then
      If wP <> "" Then
        k = k + 1
        TbIstr.AddNew
        TbIstr!idOggetto = wId
        TbIstr!Riga = wNumRiga
        TbIstr!parmcount = k
        TbIstr!parametro = wP
        TbIstr!Valore = wV
        TbIstr.Update
      End If
      wP = wParam(Ind)
      wV = ""
    Else
      wV = wV & " " & wParam(Ind)
    End If
  Next Ind
  
  If wP <> "" Then
    k = k + 1
    TbIstr.AddNew
    TbIstr!idOggetto = wId
    TbIstr!Riga = wNumRiga
    TbIstr!parmcount = k
    TbIstr!parametro = wP
    TbIstr!Valore = Trim(wV)
    TbIstr.Update
  End If
  TbIstr.Close
End Function

Sub ParamCpy(NomeCpy)
  Select Case NomeCpy
    Case "DFHAID", "DFHBMSCA", "DFHEIBLK"
      SwTpCX = True
    Case "DLIUIB"        ' , "DLIFUNZ", "DLIERR"
      SwDli = True
    Case "SQLCA"
      SwSql = True
  End Select
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Lettura parole chiave per il riconoscimento dal file: System/keyWords.diz
' SQ 12-10-2006
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Private Sub initkeyWords()
'  Dim FD As Long
'  Dim entry() As String
'  Dim keyType As String, keySubtype As String, KeyWord As String, line As String
'  Dim keyWeight As Integer
'  Dim entryOK As Boolean
'  Dim keyColumn As Integer
'
'  On Error GoTo errHand
'  If UBound(keyWords) = 0 Then 'SQ: vedi commento sotto in errVBstupido...
'    ReDim keyWords(0)
'
'    GbNumRec = 0  'init: serve solo per il log...
'    FD = FreeFile
'    Open m_Fun.FnPathPrd & "\" & SYSTEM_DIR & "\keyWords.diz" For Input As FD
'    While Not EOF(FD)
'      Line Input #FD, line
'      line = Trim(line)
'      GbNumRec = GbNumRec + 1
'      entryOK = True
'      Select Case Left(line, 1)
'        Case "'"
'          'commento
'        Case "["
'          'Nuova categoria:
'          line = Mid(line, 2) 'mangio [
'          If Right(line, 1) = "]" Then
'            entry = Split(Replace(line, "]", ""), ",")
'            keyType = entry(0)
'            If UBound(entry) Then
'              keySubtype = entry(1)
'            End If
'          Else
'            entryOK = False
'          End If
'        Case "@"
'          'Entry:
'          entry = Split(line, "@")
'          '@WORD@COLUMN@WEIGHT@
'          If UBound(entry) = 4 Then
'            '
'            KeyWord = entry(1)
'            '
'            If Len(entry(2)) Then
'              If IsNumeric(entry(2)) Then
'                keyColumn = entry(2)
'              ElseIf Len(entry(2)) Then
'                entryOK = False
'              End If
'            Else
'              keyColumn = 0  'default
'            End If
'            '
'            If Len(entry(3)) Then
'              If IsNumeric(entry(3)) Then
'                keyWeight = entry(3)
'              Else
'                entryOK = False
'              End If
'            Else
'              keyWeight = 1 'default
'            End If
'          Else
'            entryOK = False
'          End If
'          If entryOK Then
'            ReDim Preserve keyWords(UBound(keyWords) + 1)
'            keyWords(UBound(keyWords)).word = KeyWord
'            keyWords(UBound(keyWords)).lang = keyType
'            keyWords(UBound(keyWords)).Type = keySubtype
'            keyWords(UBound(keyWords)).column = keyColumn
'            keyWords(UBound(keyWords)).weight = keyWeight
'          End If
'        Case Else
'          If Len(Trim(line)) Then
'            entryOK = False
'          Else
'            'linea vuota
'          End If
'      End Select
'      If Not entryOK Then
'        m_Fun.writeLog "#keyWords.diz# Wrong entry found at line: " & GbNumRec
'      End If
'    Wend
'    Close #FD
'  Else
'    'Resetto il conteggio...
'    Dim i As Integer
'    For i = 0 To UBound(keyWords)
'      keyWords(i).count = 0
'    Next i
'  End If
'  Exit Sub
'errHand:
'  If err.Number = 9 Then
'    'la prima volta che istanzio ce l'ho sempre... non so come gestirlo "proattivamente"
'    Resume Next
'  ElseIf err.Number = 55 Or err.Number = 53 Then
'    MsgBox "System file '" & m_Fun.FnPathPrd & "\" & SYSTEM_DIR & "\keyWords.diz' not found.", vbCritical, Parser.PsNomeProdotto
'  Else
'    'SQ: adattare "MP00E"
'    m_Fun.Show_MsgBoxError "MP00E", "MapsdM_00", "initkeyWords", err.Number, err.Description
'  End If
'End Sub
'19/05/2009 gloria: nuova funzione con struttura per gestire tutti i tipi da dizionario
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Lettura parole chiave per il riconoscimento dal file: System/keyWords.diz
' SQ 12-10-2006
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function initkeyWords() As Boolean
  Dim FD As Long
  Dim entry() As String
  Dim keyType As String, keySubtype As String, KeyWord As String, line As String
  Dim keyWeight As Integer
  Dim entryOK As Boolean
  Dim keyColumn As Integer
  Dim n As Integer
  Dim booCorrectIni As Boolean
  
  On Error GoTo errHand
  
  If UBound(keyWords) = 0 Then 'SQ: vedi commento sotto in errVBstupido...
    ReDim keyWords(0)

    GbNumRec = 0  'init: serve solo per il log...
    FD = FreeFile
'    Open m_Fun.FnPathPrd & "\" & SYSTEM_DIR & "\keyWords.diz" For Input As FD
    'spostato keywords.diz a livello di progetto
    If Dir(m_Fun.FnPathPrj & "\Input-Prj\keyWords.diz") <> "" Then
       Open m_Fun.FnPathPrj & "\Input-Prj\keyWords.diz" For Input As FD
    Else
       Open m_Fun.FnPathPrd & "\" & SYSTEM_DIR & "\keyWords.diz" For Input As FD
    End If
    
    While Not EOF(FD)
      Line Input #FD, line
      line = Trim(line)
      GbNumRec = GbNumRec + 1
      entryOK = True
      Select Case Left(line, 1)
        Case "'"
          'commento
        Case "["
          'Nuova categoria:
          line = Mid(line, 2) 'mangio [
          If Right(line, 1) = "]" Then
            entry = Split(Replace(line, "]", ""), ",")
            'glo: sposto parametro a livello di progetto
'            If entry(0) = "NUMRIGHE" Then
'              RowsNum = entry(1)
'            Else
              keyType = entry(0)
              If UBound(entry) Then
                keySubtype = entry(1)
              End If
'            End If
            If UBound(entry) = 2 Then ' allora ho il default
              'se non ancora presente inserisco il linguaggio per poter memorizzare il conteggio delle parole chiave
              If UBound(keyWords) = 0 Then
                ReDim Preserve Languages(0)
              Else
                ReDim Preserve Languages(UBound(Languages) + 1)
              End If
              Languages(UBound(Languages)).lang = keyType
              Languages(UBound(Languages)).count = 0
              Languages(UBound(Languages)).default = entry(2)
              booCorrectIni = True
            End If
          Else
            entryOK = False
          End If
        Case "@"
          'Entry:
          entry = Split(line, "@")
          '@WORD@COLUMN@WEIGHT@
          If UBound(entry) = 4 Then
            '
            KeyWord = entry(1)
            '
            If Len(entry(2)) Then
              If IsNumeric(entry(2)) Then
                keyColumn = entry(2)
              ElseIf Len(entry(2)) Then
                entryOK = False
              End If
            Else
              keyColumn = 0  'default
            End If
            '
            If Len(entry(3)) Then
              If IsNumeric(entry(3)) Then
                keyWeight = entry(3)
              Else
                entryOK = False
              End If
            Else
              keyWeight = 1 'default
            End If
          Else
            entryOK = False
          End If
          If entryOK Then
            ReDim Preserve keyWords(UBound(keyWords) + 1)
            keyWords(UBound(keyWords)).word = KeyWord
            keyWords(UBound(keyWords)).lang = keyType
            keyWords(UBound(keyWords)).Type = keySubtype
            keyWords(UBound(keyWords)).column = keyColumn
            keyWords(UBound(keyWords)).weight = keyWeight
          End If
        Case Else
          If Len(Trim(line)) Then
            entryOK = False
          Else
            'linea vuota
          End If
      End Select
      If Not entryOK Then
        m_Fun.writeLog "#keyWords.diz# Wrong entry found at line: " & GbNumRec
      End If
    Wend
    Close #FD

    If Not booCorrectIni Then
      MsgBox "Languages default values not found. Incorrect format for common section languages." & _
            "Check KeywordDiz.ini, correct format is [LANGUAGES,ALL,DEFAULT].", vbCritical, "i-4.Migration"
      ReDim Languages(0)
    End If
  Else
    'Resetto il conteggio...
    Dim i As Integer
    For i = 0 To UBound(keyWords)
      keyWords(i).count = 0
    Next i

    For i = 0 To UBound(Languages)
      Languages(i).count = 0
    Next i
  End If

  initkeyWords = True

  Exit Function
errHand:
  If err.Number = 9 Then
    'la prima volta che istanzio ce l'ho sempre... non so come gestirlo "proattivamente"
    Resume Next
  ElseIf err.Number = 55 Or err.Number = 53 Then
    MsgBox "System file '" & m_Fun.FnPathPrd & "\" & SYSTEM_DIR & "\keyWords.diz' not found.", vbCritical, Parser.PsNomeProdotto
  Else
    'SQ: adattare "MP00E"
    m_Fun.Show_MsgBoxError "MP00E", "MapsdM_00", "initkeyWords", err.Number, err.description
  End If
End Function

Private Sub resetKeyWord(wWord As String, wLang As String, wType As String)
  Dim i As Integer
  For i = 0 To UBound(keyWords)
    keyWords(i).count = 0
  Next i
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''
' Riconoscimento tipo di oggetto (+ numero di linee)
' Effetto collaterale: setta la variabile GbNumRec
'''''''''''''''''''''''''''''''''''''''''''''''''''
'Function getObjectType(nomefile) As String
'  Dim wTipo As String, line As String, statement As String
'  Dim nFCbl As Long
'  Dim CountAsm As Long, CountCbl As Long, CountPli As Long, CountJcl As Long
'  Dim CountSql As Long, CountDbd As Long, CountPsb As Long, CountTot As Long
'  Dim CountEZ As Long, CountLOAD As Long, CountNatural As Long, CountCobolXE As Long
'  Dim k As Integer
'  Dim SwAsm As Boolean, SwDCL As Boolean, SwCbl As Boolean, SwJcl As Boolean, SwMBM As Boolean
'  Dim isPROC As Boolean, isJCL As Boolean, init As Boolean, statementOK As Boolean
'  Dim wordColumn As Integer
'  Dim countISPF As Long, countCLIST As Long
'
'  '''''''''''''''''''''''''''''''
'  'Riconoscimento Formato COBOL:
'  ' - LABEL:
'  ' - Incolonnamento (col 12...)
'  '''''''''''''''''''''''''''''''
'  Dim cblLabelCount As Integer
'  Dim cblFormatOK As Boolean
'
'  On Error GoTo routeErr
'
'  GbNumRec = 0  'init
'  cblFormatOK = True  'ne basta uno per sbagliare...
'  wTipo = "UNK"
'
'  initkeyWords  'inizializza/resetta i contatori delle parole chiave
'
'  nFCbl = FreeFile
'  Open nomefile For Input As nFCbl
'  ''''''''''''''''''''''''''''''''''''''
'  'JCL/JOB:
'  'Cerco la prima linea non commentata
'  ''''''''''''''''''''''''''''''''''''''
'  Do While Not EOF(nFCbl)
'    Line Input #nFCbl, line
'    GbNumRec = GbNumRec + 1
'    If Left(line, 3) <> "//*" Then
'      If Left(line, 2) = "//" Then
'        'Potenziale JCL
'        line = Trim(Mid(line, InStr(line & " ", " ")))  'davanti al tipo
'        If Left(line, 3) = "JOB" Then
'          isJCL = True
'        Else
'          isPROC = Left(line, 4) = "PROC"
'        End If
'        Exit Do
'      Else
'        'VSE: * a colonna 1
'        'SQ 6-04-06
'        If Left(line, 9) = "* $$ JOB " Then
'          isJCL = True
'        End If
'        Exit Do
'      End If
'    End If
'  Loop
'
'  'Posso essere gi� a fine file: non entrerebbe!
'  init = True
'
'  'While Not EOF(nFCbl)
'  While Not EOF(nFCbl) Or init Or Len(line) 'ultima riga!
'    'Formatta a 80 chars... vedere a chi serve (e mangia a 80!)
'    line = Left(line & Space(80), 80)
'
'    'CONTROLLO COMMENTO
'    'Cavolata: il commento lo devo commentare dentro il for: per ogni parola (linguaggio)!
'    If Left(line, 1) <> "*" _
'      And Mid(line, 7, 1) <> "*" _
'      And Left(line, 2) <> "--" _
'      And Left(LTrim(line), 1) <> "#" _
'      And Left(line, 3) <> "//*" Then
'      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'      ' N iterazioni (su N parole chiave) sulla linea corrente
'      ' - un exit for?
'      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'      For k = 1 To UBound(keyWords)
'        statement = " " & line & " " 'trattiamo inizio e fine linea come space: unico separatore!
'        statementOK = True
'        '''''''''''''''''''
'        'Eccezioni
'        '''''''''''''''''''
'        Select Case keyWords(k).lang
'          Case "PLI"
'            Mid$(statement, 1, 1) = " "
'            statement = " " & statement
'          Case "EASYTRIEVE"
'            'statement = " " & trim(line)  'colonna 1 sarebbe il primo carattere non space...
'          Case "LOAD", "CLIST"
'            statement = " " & Trim(line)  'colonna 1 sarebbe il primo carattere non space...
'          Case "JCL"
'            statementOK = Left(statement, 3) = " //"
'        End Select
'
'        If statementOK Then
'          wordColumn = InStr(statement, keyWords(k).word) - 1 'sottraggo lo space aggiunto in testa
'          If wordColumn > 0 Then
'            'column=0: no vincolo
'            If (keyWords(k).column = 0) Or (keyWords(k).column = wordColumn) Then
'              keyWords(k).count = keyWords(k).count + keyWords(k).weight
'            End If
'          End If
'        End If
'      Next k
'      ''''''''''''''''''''''''''''''
'      ' Eccezione:
'      ' CONTROLLO FORMATO COBOL:
'      '    - LABEL
'      '    - COLONNA 12
'      ''''''''''''''''''''''''''''''
'      If isCobolLabel(statement) Then
'        cblLabelCount = cblLabelCount + 1
'      Else
'        If cblFormatOK Then cblFormatOK = Mid(statement, 8, 4) = "    " Or InStr(statement, " SKIP") Or InStr(statement, " EJECT")
'      End If
'    Else
'      'statement = " " & line & " " 'trattiamo inizio e fine linea come space: unico separatore!
'      If Mid(line, 7, 1) = "*" Then SwCbl = True
'      If Mid(line, 1, 1) = "*" Then SwAsm = True
'      If Mid(line, 1, 3) = "//*" Then SwJcl = True
'      If Mid(line, 1, 2) = "--" Then SwDCL = True
'      If Mid(LTrim(line), 1, 1) = "#" Then SwMBM = True
'      '''''''''''''''''''''''''
'      ' 6-04-06
'      ' JOB: * a colonna 1...
'      '''''''''''''''''''''''''
'      If Left(line, 5) = "* $$ " Then
'        For k = 1 To UBound(keyWords)
'          If keyWords(k).Type = "JOB" Then
'            If InStr(line, keyWords(k).word) Then
'              keyWords(k).count = keyWords(k).count + keyWords(k).weight
'            End If
'          End If
'        Next k
'      End If
'    End If
'    'tmp: sistemare...
'    If Not EOF(nFCbl) Then
'      Line Input #nFCbl, line
'      GbNumRec = GbNumRec + 1
'    Else
'      line = ""
'    End If
'    init = False
'  Wend
'  Close nFCbl
'
'  'BUTTARE VIA IL CASE!
'  For k = 1 To UBound(keyWords)
'    CountTot = CountTot + keyWords(k).count
'    Select Case keyWords(k).lang
'       Case "ASSEMBLER"
'          CountAsm = CountAsm + keyWords(k).count
'       Case "COBOL"
'          CountCbl = CountCbl + keyWords(k).count
'       Case "PLI"
'          CountPli = CountPli + keyWords(k).count
'       Case "JCL"
'          CountJcl = CountJcl + keyWords(k).count
'       Case "SQL"
'          CountSql = CountSql + keyWords(k).count
'       Case "DBD"
'          CountDbd = CountDbd + keyWords(k).count
'       Case "PSB"
'          CountPsb = CountPsb + keyWords(k).count
'      Case "PSB"
'          CountPsb = CountPsb + keyWords(k).count
'      Case "EASYTRIEVE"
'          CountEZ = CountEZ + keyWords(k).count
'      Case "LOAD"
'          CountLOAD = CountLOAD + keyWords(k).count
'      Case "ISPF"
'          countISPF = countISPF + keyWords(k).count
'      Case "CLIST"
'          countCLIST = countCLIST + keyWords(k).count
'      Case "NATURAL"
'          CountNatural = CountNatural + keyWords(k).count
'      Case "COBOLXE"
'          CountCobolXE = CountCobolXE + keyWords(k).count
'      Case Else
'        'Tipo del .diz non gestito!
'        MsgBox "tmp: Type " & keyWords(k).lang & " not managed!", vbCritical, "i-4.Migration"
'    End Select
'  Next k
'
'  ''''''''''''''''''''''''''''''''''''''''''''''''''
'  ' Controllo "intestazione"
'  ' Gestione "peso" parole individuate
'  ''''''''''''''''''''''''''''''''''''''''''''''''''
'  If isJCL Or isPROC Then
'    CountJcl = CountJcl * 10 'ATTENZIONE: valore "sperimentale"
'  End If
'
'  If CountTot = 0 Then
'    ' Nessuna parola: probabilmente sono JCL da 2/3 righe...
'    If isJCL Then
'      getObjectType = "JCL"
'      Exit Function
'    ElseIf isPROC Then
'      getObjectType = "PRC"
'      Exit Function
'    Else
'      wTipo = "UNK"
'    End If
'  Else
'    If 2 * CountPli >= CountTot Then
'      wTipo = "INC"
'      For k = 1 To UBound(keyWords)
'        If keyWords(k).lang = "PLI" And keyWords(k).count > 0 Then
'           If Trim(keyWords(k).Type) <> "" Then
'              wTipo = keyWords(k).Type
'              Exit For
'           End If
'        End If
'      Next k
'    ElseIf 2 * CountDbd >= CountTot Then
'      wTipo = "DBD"
'    ElseIf 2 * CountPsb >= CountTot Then
'      wTipo = "PSB"
'    ElseIf 2 * CountSql >= CountTot Then
'      wTipo = "DDL"
'    ElseIf 2 * CountAsm >= CountTot Then
'      wTipo = "MAC"
'      For k = 1 To UBound(keyWords)
'        If keyWords(k).lang = "ASSEMBLER" And keyWords(k).count Then
'           If Trim(keyWords(k).Type) <> "" Then
'              wTipo = keyWords(k).Type
'              Exit For
'           End If
'        End If
'      Next k
'    ElseIf 2 * CountCbl >= CountTot Then
'      wTipo = "CPY"
'      For k = 1 To UBound(keyWords)
'        If keyWords(k).lang = "COBOL" And keyWords(k).count > 0 Then
'           If Trim(keyWords(k).Type) <> "" Then
'              wTipo = keyWords(k).Type
'              Exit For
'           End If
'        End If
'      Next k
'    ElseIf 2 * CountJcl >= CountTot Then
'      wTipo = "JCL"
'      For k = 1 To UBound(keyWords)
'        If keyWords(k).lang = "JCL" And keyWords(k).count > 0 Then
'           If Trim(keyWords(k).Type) <> "" Then
'              wTipo = keyWords(k).Type
'              Exit For
'           End If
'        End If
'      Next k
'    ElseIf 2 * CountEZ >= CountTot Then
'      wTipo = "EZT"
'      'Tipo macro...
'      For k = 1 To UBound(keyWords)
'        If keyWords(k).lang = "EASYTRIEVE" And keyWords(k).count > 0 Then
'          If Len(keyWords(k).Type) Then
'            wTipo = keyWords(k).Type
'            Exit For
'          End If
'        End If
'      Next k
'    ElseIf 2 * CountLOAD >= CountTot Then
'      wTipo = "LOA"
'    ElseIf 2 * countISPF >= CountTot Then
'      wTipo = "ISP"
'    ElseIf 2 * countCLIST >= CountTot Then
'      wTipo = "CLI"
'    ElseIf 2 * CountNatural >= CountTot Then
'      wTipo = "NAT"
'      'Tipo macro...
'      For k = 1 To UBound(keyWords)
'        If keyWords(k).lang = "NATURAL" And keyWords(k).count > 0 Then
'          If Len(keyWords(k).Type) Then
'            wTipo = keyWords(k).Type
'            Exit For
'          End If
'        End If
'      Next k
'    ElseIf 2 * CountCobolXE >= CountTot Then
'      wTipo = "XEP"
'      'Tipo copy panel...
'      For k = 1 To UBound(keyWords)
'        If keyWords(k).lang = "COBOLXE" And keyWords(k).count > 0 Then
'          If Len(keyWords(k).Type) Then
'            wTipo = keyWords(k).Type
'            Exit For
'          End If
'        End If
'      Next k
'    End If
'  End If
'
'  If wTipo = "" Then
'  '   Stop
'     wTipo = "UNK"
'     If SwCbl And Not SwAsm And Not SwMBM And Not SwJcl And Not SwDCL Then
'        wTipo = "CPY"
'     End If
'     If SwAsm And Not SwCbl And Not SwMBM And Not SwJcl And Not SwDCL Then
'        wTipo = "MAC"
'     End If
'     If SwJcl And Not SwAsm And Not SwMBM And Not SwCbl And Not SwDCL Then
'        If isJCL Then
'          wTipo = "JCL"
'        ElseIf isPROC Then
'          wTipo = "PROC"
'        Else
'          'SQ: NUOVA CATEGORIA: MEMBER (COMMAND)
'          'wTipo = "UNK_JCL"
'          wTipo = "MBR"
'        End If
'     End If
'     If SwMBM And Not SwAsm And Not SwCbl And Not SwJcl And Not SwDCL Then
'        wTipo = "JMB"
'     End If
'     If SwDCL And Not SwAsm And Not SwCbl And Not SwJcl And Not SwMBM Then
'        wTipo = "DCL"
'     End If
'  End If
'
'  'SQ: 25-11-05
'  'CONTROLLO FORMATO COBOL:
'  If wTipo = "UNK" Then
'    'Almeno una label e colonna 12 OK
'    If cblLabelCount And cblFormatOK Then wTipo = "CPY"
'  ElseIf wTipo = "JCL" Then
'    If isJCL Then
'      wTipo = "JCL"
'    ElseIf isPROC Then
'      wTipo = "PRC"
'    Else
'      'SQ: NUOVA CATEGORIA: MEMBER (COMMAND)
'      wTipo = "MBR"
'    End If
'  End If
'
'  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'  ' SQ 19-06-07 DSP#154
'  ' Log parole chiave per modifica "ResWord.diz"
'  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'  'If wTipo = "UNK" Then 'Togliere "UNK" ma NON produrlo sempre: voce specifica di menu...
'    lstWords = reportClassificationWords(keyWords)
'  'End If
'
'  getObjectType = wTipo
'
'  Exit Function
'routeErr:
'  'SQ 13-02-07
'  getObjectType = "UNK"
'  'gestire...
'  If err.Number = 53 Then
'    'gestire
'    MsgBox "Open file failed. Check if it's locked by another process."
'    'Resume
'  Else
'    'gestire!
'    MsgBox err.Description
'    'Resume
'  End If
'End Function
'gloria: nuova funzone per riconoscere dinamicamente nuovi tipi inseriti nel dizionario
'''''''''''''''''''''''''''''''''''''''''''''''''''
' Riconoscimento tipo di oggetto (+ numero di linee)
' Effetto collaterale: setta la variabile GbNumRec
'''''''''''''''''''''''''''''''''''''''''''''''''''
Function getObjectType(nomefile As String, Optional withTrace As Boolean = False) As String
  Dim wtipo As String, line As String, statement As String
  Dim nFCbl As Long
  Dim CountAsm As Long, CountCbl As Long, CountPli As Long, CountJcl As Long
  Dim CountSql As Long, CountDbd As Long, CountPsb As Long, CountTot As Long
  Dim CountEZ As Long, CountLOAD As Long, CountNatural As Long, CountCobolXE As Long
  Dim k As Integer, n As Integer
  Dim SwAsm As Boolean, SwDCL As Boolean, SwCbl As Boolean, SwJcl As Boolean, SwMBM As Boolean
  Dim isPROC As Boolean, isJCL As Boolean, init As Boolean, statementOK As Boolean
  Dim wordColumn As Integer
  Dim countISPF As Long, countCLIST As Long
  Dim RowsCount As Long ' righe lette
  Dim RowsNum As Long ' righe da parserare
  '''''''''''''''''''''''''''''''
  'Riconoscimento Formato COBOL:
  ' - LABEL:
  ' - Incolonnamento (col 12...)
  '''''''''''''''''''''''''''''''
  Dim cblLabelCount As Integer
  Dim cblFormatOK As Boolean

  On Error GoTo routeErr

  GbNumRec = 0  'init
  cblFormatOK = True  'ne basta uno per sbagliare...
  wtipo = "UNK"

  If Not initkeyWords Then Exit Function 'inizializza/resetta i contatori delle parole chiave
    
  'lo rinizializzo perch� viene usato nella initkeyWords per scrivere il log e rimane sporco al primo giro
  GbNumRec = 0
  
  nFCbl = FreeFile
  Open nomefile For Input As nFCbl
  ''''''''''''''''''''''''''''''''''''''
  'JCL/JOB:
  'Cerco la prima linea non commentata
  ''''''''''''''''''''''''''''''''''''''
  Do While Not EOF(nFCbl)
    Line Input #nFCbl, line

    GbNumRec = GbNumRec + 1
    If Left(line, 3) <> "//*" Then
      If Left(line, 2) = "//" Then
        'Potenziale JCL
        line = Trim(Mid(line, InStr(line & " ", " ")))  'davanti al tipo
        If Left(line, 3) = "JOB" Then
          isJCL = True
        Else
          isPROC = Left(line, 4) = "PROC"
        End If
        Exit Do
      Else
        'VSE: * a colonna 1
        'SQ 6-04-06
        If Left(line, 9) = "* $$ JOB " Then
          isJCL = True
        Else
          'SQ tmp: JCL x schedulatori: possono iniziare con variabili...
          If Left(line, 1) = "$" And GbNumRec < 15 Then 'non esagerimo... se sono schede parametro o altro...
            isJCL = True
          End If
        End If
        Exit Do
      End If
    End If
  Loop

  'Posso essere gi� a fine file: non entrerebbe!
  init = True
  
  'SQ 21-08-09 ritorna "" => non numerico!
  RowsNum = "0" & m_Fun.LeggiParam("PARSER_ROWSNUM")
  
  'While Not EOF(nFCbl)
  'While (Not EOF(nFCbl) Or init Or Len(line)) And RowsNum > RowsCount  'ultima riga!
  While (Not EOF(nFCbl) Or init Or Len(line))
    RowsCount = RowsCount + 1
    If (RowsNum > RowsCount) Or RowsNum = 0 Then
      'Formatta a 80 chars... vedere a chi serve (e mangia a 80!)
      line = Left(line & Space(80), 80)
      'CONTROLLO COMMENTO
      'Cavolata: il commento lo devo commentare dentro il for: per ogni parola (linguaggio)!
      If Left(line, 1) <> "*" _
        And Mid(line, 7, 1) <> "*" _
        And Left(line, 2) <> "--" _
        And Left(LTrim(line), 1) <> "#" _
        And Left(line, 3) <> "//*" Then
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' N iterazioni (su N parole chiave) sulla linea corrente
        ' - un exit for?
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        For k = 1 To UBound(keyWords)
          statement = " " & line & " " 'trattiamo inizio e fine linea come space: unico separatore!
          statementOK = True
          '''''''''''''''''''
          'Eccezioni
          '''''''''''''''''''
          Select Case keyWords(k).lang
            Case "PLI"
              Mid$(statement, 1, 1) = " "
              statement = " " & statement
            Case "EASYTRIEVE"
              'statement = " " & trim(line)  'colonna 1 sarebbe il primo carattere non space...
            Case "LOAD", "CLIST"
              statement = " " & Trim(line)  'colonna 1 sarebbe il primo carattere non space...
            Case "JCL"
              statementOK = Left(statement, 3) = " //"
          End Select
   
          If statementOK Then
            wordColumn = InStr(statement, keyWords(k).word) - 1 'sottraggo lo space aggiunto in testa
            If wordColumn > 0 Then
              'column=0: no vincolo
              If (keyWords(k).column = 0) Or (keyWords(k).column = wordColumn) Then
                'gloria: salvo conteggio sull'oggetto Languages
                keyWords(k).count = keyWords(k).count + keyWords(k).weight
                For n = 0 To UBound(Languages)
                  If Languages(n).lang = keyWords(k).lang Then
                    Languages(n).count = Languages(n).count + keyWords(k).weight
                    Exit For
                  End If
                Next n
                CountTot = CountTot + keyWords(k).weight
              End If
            End If
          End If
        Next k
        ''''''''''''''''''''''''''''''
        ' Eccezione:
        ' CONTROLLO FORMATO COBOL:
        '    - LABEL
        '    - COLONNA 12
        ''''''''''''''''''''''''''''''
        If isCobolLabel(statement) Then
          cblLabelCount = cblLabelCount + 1
        Else
          If cblFormatOK Then cblFormatOK = Mid(statement, 8, 4) = "    " Or InStr(statement, " SKIP") Or InStr(statement, " EJECT")
        End If
      Else
        'statement = " " & line & " " 'trattiamo inizio e fine linea come space: unico separatore!
        If Mid(line, 7, 1) = "*" Then SwCbl = True
        If Mid(line, 1, 1) = "*" Then SwAsm = True
        If Mid(line, 1, 3) = "//*" Then SwJcl = True
        If Mid(line, 1, 2) = "--" Then SwDCL = True
        If Mid(LTrim(line), 1, 1) = "#" Then SwMBM = True
        '''''''''''''''''''''''''
        ' 6-04-06
        ' JOB: * a colonna 1...
        '''''''''''''''''''''''''
        If Left(line, 5) = "* $$ " Then
          For k = 1 To UBound(keyWords)
            If keyWords(k).Type = "JOB" Then
              If InStr(line, keyWords(k).word) Then
                keyWords(k).count = keyWords(k).count + keyWords(k).weight
                For n = 0 To UBound(Languages)
                 If Languages(n).lang = keyWords(k).lang Then
                    Languages(n).count = Languages(n).count + keyWords(k).weight
                    Exit For
                 End If
                Next n
                CountTot = CountTot + keyWords(k).weight
              End If
            End If
          Next k
        End If
      End If
    End If
    'tmp: sistemare...
    If Not EOF(nFCbl) Then
      Line Input #nFCbl, line
      GbNumRec = GbNumRec + 1
    Else
      line = ""
    End If
    init = False
  Wend
  Close nFCbl

  ' Controllo "intestazione"
  ' Gestione "peso" parole individuate
  ''''''''''''''''''''''''''''''''''''''''''''''''''

  If CountTot = 0 Then
    ' Nessuna parola: probabilmente sono JCL da 2/3 righe...
    If isJCL Then
      getObjectType = "JCL"
      Exit Function
    ElseIf isPROC Then
      getObjectType = "PRC"
      Exit Function
    Else
      wtipo = "UNK"
    End If
  Else
    If isJCL Or isPROC Then
      For n = 0 To UBound(Languages)
        If Languages(n).lang = "JCL" Then
          Languages(n).count = Languages(n).count * 10
          Exit For
        End If
      Next n
    End If

    For n = 0 To UBound(Languages)
      If Languages(n).count * 2 >= CountTot Then
        For k = 1 To UBound(keyWords)
          If keyWords(k).lang = Languages(n).lang And keyWords(k).count > 0 Then
            If Trim(keyWords(k).Type) <> "ALL" Then
              wtipo = keyWords(k).Type
            Else
              If wtipo = "" Then
                wtipo = "UNK"
                If SwCbl And Not SwAsm And Not SwMBM And Not SwJcl And Not SwDCL Then
                  wtipo = "CPY"
                End If
                If SwAsm And Not SwCbl And Not SwMBM And Not SwJcl And Not SwDCL Then
                  wtipo = "MAC"
                End If
                If SwJcl And Not SwAsm And Not SwMBM And Not SwCbl And Not SwDCL Then
                  If isJCL Then
                    wtipo = "JCL"
                  ElseIf isPROC Then
                    wtipo = "PROC"
                  Else
                    'SQ: NUOVA CATEGORIA: MEMBER (COMMAND)
                    'wTipo = "UNK_JCL"
                    wtipo = "MBR"
                  End If
                End If
                If SwMBM And Not SwAsm And Not SwCbl And Not SwJcl And Not SwDCL Then
                  wtipo = "JMB"
                End If
                If SwDCL And Not SwAsm And Not SwCbl And Not SwJcl And Not SwMBM Then
                  wtipo = "DCL"
                End If
              End If
              If wtipo = "UNK" Then wtipo = Languages(n).default
            End If
            Exit For
          End If
        Next k
        Exit For
      End If
    Next n

    'SQ: 25-11-05
    'CONTROLLO FORMATO COBOL:
    If wtipo = "UNK" Then
      'Almeno una label e colonna 12 OK
      If cblLabelCount And cblFormatOK Then wtipo = "CPY"
    ElseIf wtipo = "JCL" Then
      If isJCL Then
        wtipo = "JCL"
      ElseIf isPROC Then
        wtipo = "PRC"
      Else
        'SQ: NUOVA CATEGORIA: MEMBER (COMMAND)
        wtipo = "MBR"
      End If
    End If
  End If
  
  '''''''''''''''''''''
  ' Tracing
  '''''''''''''''''''''
  If withTrace Then
    lstWords = reportClassificationWords(keyWords)
  End If

  getObjectType = wtipo

  Exit Function
routeErr:
  'SQ 13-02-07
  getObjectType = "UNK"
  'gestire...
  If err.Number = 53 Then
    'gestire
    MsgBox "Open file failed. Check if it's locked by another process."
    'Resume
  ElseIf err.Number = 13 Then
    Resume Next
    'gestire!
    MsgBox err.description
    'Resume
  End If
End Function

Function getObjTypeByExt(nomefile As String) As String
  Dim nFCbl As Integer
  Dim extFile As String
  Dim line As String

  extFile = ""
  If InStr(nomefile, ".") Then
    extFile = Right(nomefile, 3)
  End If
  getObjTypeByExt = getObjType(extFile)

  'SQ 10-02-09
  If getObjTypeByExt <> "UNK" Or Len(extFile) Then
    GbNumRec = 0
    nFCbl = FreeFile
    ' Devo fare sto giro assurdo per impostarmi il GbNumRec
    Open nomefile For Input As nFCbl
    Do Until EOF(nFCbl)
      Line Input #nFCbl, line
      GbNumRec = GbNumRec + 1
    Loop
    Close nFCbl
  Else
    'NO EXTENSION: giro normale
    getObjTypeByExt = getObjectType(GbFileInput)
  End If
End Function

Function getObjType(extFile As String) As String
  Dim i As Long
  
  getObjType = "UNK"
  ObjTypes = m_Fun.objtype
  For i = 0 To UBound(ObjTypes)
    If UCase(ObjTypes(i).oEstensione) = UCase(extFile) And _
       Len(ObjTypes(i).oEstensione) Then
      getObjType = ObjTypes(i).oType
      Exit For
    End If
  Next i
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 19-06-07 DSP#154
' Log parole chiave per modifica "ResWord.diz"
' TMP
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function reportClassificationWords(keyWords() As KeyWord) As String
  Dim i As Long
  Dim classificationLog As String

  classificationLog = "KEYWORD" & Space(15) & "COLUMN" & Space(4) & "WEIGHT" & Space(4) & "COUNT" & Space(4) & "LANGUAGE" & Space(4) & "TYPE" & vbCrLf
  classificationLog = classificationLog & "---------------------------------------------------------------------------------" & vbCrLf
  For i = 1 To UBound(keyWords)
    If keyWords(i).count Then
      classificationLog = classificationLog & "#" & keyWords(i).word & "#" & Space(20 - Len(keyWords(i).word))
      classificationLog = classificationLog & keyWords(i).column & Space(10 - Len(keyWords(i).column & ""))
      classificationLog = classificationLog & keyWords(i).weight & Space(10 - Len(keyWords(i).weight & ""))
      classificationLog = classificationLog & keyWords(i).count & Space(10 - Len(keyWords(i).count & ""))
      classificationLog = classificationLog & keyWords(i).lang & Space(11 - Len(keyWords(i).lang & ""))
      classificationLog = classificationLog & keyWords(i).Type & vbCrLf
    End If
  Next i
  
  'm_Fun.WriteLog "@@reportClassificationWords@@" & vbCrLf & classificationLog
  reportClassificationWords = classificationLog
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' copia del MabseM_Prj.Update_tbTreeView_After_AddObjects
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub updateTreeViewTable(wIdOggetto As Long)
   Dim rs As Recordset, rTrw As Recordset, rMax As Recordset
   Dim wArea As String
   Dim wLivello1 As String
   Dim wLivello2 As String
   Dim cMax As Long
   
   Set rs = m_Fun.Open_Recordset("select * from BS_Oggetti Where IdOggetto = " & wIdOggetto)
   If rs.RecordCount > 0 Then
      'Prende le variabili per accedere alla tabella BS_treeView
      wArea = rs!Area_Appartenenza
      wLivello1 = rs!livello1 & ""
      wLivello2 = rs!Livello2
      
      'Recupera il maxid
      Set rMax = m_Fun.FnConnection.Execute("Select MAX(Id) From BS_TreeView")
      cMax = CLng(rMax.Fields(0).value)
      rMax.Close
      
      'Ric per Area
      Set rTrw = m_Fun.Open_Recordset("Select * From BS_TreeView Where Key = '" & wArea & "'")
      If rTrw.RecordCount = 0 Then
         rTrw.AddNew
            rTrw!Id = cMax + 1
            rTrw!Testo = wArea
            rTrw!NRelative = "MB"
            rTrw!Key = wArea
            rTrw!Tipo = 4
            rTrw!livello = "L0"
            rTrw!Image = 5
         rTrw.Update
      End If
      
      rTrw.Close
      
      'Ric Per Livello1
      Set rTrw = m_Fun.Open_Recordset("Select * From BS_TreeView Where Key = '" & wArea & "#" & wLivello1 & "'")
      
      If rTrw.RecordCount = 0 Then
         rTrw.AddNew
            rTrw!Id = cMax + 2
            rTrw!Testo = wLivello1
            rTrw!NRelative = wArea
            rTrw!Key = wArea & "#" & wLivello1
            rTrw!Tipo = 4
            rTrw!livello = "L1"
            rTrw!Image = 4
         rTrw.Update
      End If
      
      rTrw.Close
      
      'Ric Per Livello2
      Set rTrw = m_Fun.Open_Recordset("Select * From BS_TreeView Where Key = '" & wArea & "#" & wLivello1 & "@" & wLivello2 & "'")
      
      If rTrw.RecordCount = 0 Then
         rTrw.AddNew
            rTrw!Id = cMax + 3
            rTrw!Testo = wLivello2
            rTrw!NRelative = wArea & "#" & wLivello1
            rTrw!Key = wArea & "#" & wLivello1 & "@" & wLivello2
            rTrw!Tipo = 4
            rTrw!livello = "L2"
            rTrw!Image = 2
         rTrw.Update
      End If
      rTrw.Close
   End If
   rs.Close
End Sub
'''''''''''''''''''''''''''''''''''''
' Logging progressione arborescente
' Rendere grafico
'''''''''''''''''''''''''''''''''''''
Public Sub displayAddingInfo_ORIG(level As Integer, idOggetto As String, msgType As String, itemName As String, msg As String, Optional idItem As Long = 0)
  Dim message As String
  message = Space(level * 2) & "#" & idOggetto & "#  " & msgType & " " & itemName & ": " & msg
  'message = "#" & idOggetto & "#  " & msgType & " " & itemName & ": " & msg
  If idItem Then
    message = message & " (New object: " & idItem & ")"
  End If
  Parser.PsFinestra.ListItems.Add , , message
  'mi sposto sull'ultimo
  Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).Selected = True
  Parser.PsFinestra.Refresh
  'TMP
  m_Fun.writeLog message
End Sub
Public Sub displayAddingInfo(level As Integer, idOggetto As String, msgType As String, itemName As String, msg As String, Optional idItem As Long = 0, Optional itemType As String = "")
  Dim message As String
  message = Space(level * 2) & "#" & idOggetto & "#  " & msgType & " " & itemName & " (" & itemType & ") : " & msg
  'message = "#" & idOggetto & "#  " & msgType & " " & itemName & ": " & msg
  If idItem Then
    message = message & " (New object: " & idItem & ")"
  End If
  Parser.PsFinestra.ListItems.Add , , message
  'mi sposto sull'ultimo
  Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).Selected = True
  Parser.PsFinestra.Refresh
  If msg = "already present." Or msg = "not found." Then
    'TMP
    m_Fun.writeLog message
  End If
End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''
' Reset Repository per parsing I;
' FARE:
' Utilizzare un "resetRepository" per ogni specifico modulo
' e lasciare la gestione delle tabelle comuni qui
'''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub deleteRelations(nomeOggetto As String)
  'EasyTrieve
  MapsdM_EASYTRIEVE.resetRepository
  MapsdM_COBOL.resetRepository
  
  ''''''''''''''''''''''''''''''''''''''
  ' Tools:
  ' Solo per restore! (cambiare tutto)
  ' idOggetto uguale a prima...
  '''''''''''''''''''''''''''''''''''''''
  Parser.PsConnection.Execute "Delete * from TL_AllignIstr where idOggetto =" & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsIMS_entry where idoggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from BS_Segnalazioni where idoggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsCall where idoggetto = " & GbIdOggetto
  'AC 14/02/08
  Parser.PsConnection.Execute "Delete * from PsExec where idoggetto = " & GbIdOggetto
  ' Mauro 14/01/2009 : TEMPORANEO - PROVO A AGGIRARE IL PROBLEMA
''  Dim rs As Recordset
''  Set rs = m_Fun.Open_Recordset("Select * from PsExec where idoggetto = " & GbIdOggetto)
''  Do While rs.RecordCount
''    rs!istruzione = ""
''    rs.Update
''    rs.Delete
''    If rs.RecordCount Then
''      rs.MoveFirst
''    End If
''  Loop
''  rs.Close
  'SQ 27-11-06
  Parser.PsConnection.Execute "Delete * from PsISPF_Call where idoggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsRel_Tracciati where Program='" & nomeOggetto & "'"
  
  MapsdM_IMS.resetRepository ("parseuno")
  'Per ora solo CBL/CPY cobol...
  Parser.PsConnection.Execute "Delete * from PsData_CmpMoved where idpgm = " & GbIdOggetto
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Relazioni
  ' Inghippo nuova relazione PSB-JCL (relazione PGM/PSB da JCL)
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Select Case GbTipoInPars
    Case "JCL", "JOB", "PRC", "MBR"
      MapsdM_JCL.resetRepository
    Case Else
      'PGM: Non braso la relazione da JCL
      If Left(m_Fun.FnNomeDB, 14) = "prj-RealeMutua" Then
        Parser.PsConnection.Execute "Delete * from PsRel_Obj where " _
                                    & "idoggettoc = " & GbIdOggetto & " and Utilizzo in ('PSB','PSB-INHE')"
      Else
        Parser.PsConnection.Execute "Delete * from PsRel_Obj where " _
                                    & "idoggettoc = " & GbIdOggetto & " and Utilizzo not in ('PSB-JCL','QUIK-PGM')"
      End If
      
      Parser.PsConnection.Execute "Delete * from PsRel_ObjUnk where " _
                                    & "idoggettoc = " & GbIdOggetto & " and Utilizzo not in ('PSB-JCL','QUIK-PGM')"
  End Select
  
  ' Mauro 29/01/2008: A volte si impalla se non trova nessun record da eliminare!!!
  ' Che cavolo si � fumato?!?!?!?!
  On Error Resume Next
  'ELIMINA Componenti dell'Oggetto
  Parser.PsConnection.Execute "Delete * from PsCom_Obj where idoggettoc = " & GbIdOggetto
  'ELIMINA Relazioni File
  Parser.PsConnection.Execute "Delete * from PsRel_File where idoggetto = " & GbIdOggetto
  'ELIMINA Comandi dell'Oggetto
  Parser.PsConnection.Execute "Delete * from PsExec where idoggetto = " & GbIdOggetto
  'SQ: 1-2-06 - ex PsSys_IstrParm
  Parser.PsConnection.Execute "Delete * from PsExec_Param where idoggetto = " & GbIdOggetto
  On Error GoTo 0
  
  'SQ 10-09-07
  Parser.PsConnection.Execute "Delete * from PsVSM_Istruzioni where idoggetto = " & GbIdOggetto
  '  ELIMINA Tabelle RDBMS se DDL
  If GbTipoInPars = "DDL" Or GbTipoInPars = "CBL" Or GbTipoInPars = "CPY" Then
    Parser.PsConnection.Execute "Delete * from DMDB2_Tablespaces where " _
                                   & "IdObjSource = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from DMDB2_DB where IdObjSource = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from DMDB2_Alias where " _
                                   & "IdTable in (Select IdTable from DMDB2_Tabelle where " _
                                   & "IdObjSource = " & GbIdOggetto & ") "
    Parser.PsConnection.Execute "Delete * from DMDB2_Colonne where " _
                                   & "IdTable in (Select IdTable from DMDB2_Tabelle where " _
                                   & "IdObjSource = " & GbIdOggetto & ") "
    Parser.PsConnection.Execute "Delete * from DMDB2_Index where " _
                                   & "IdTable in (Select IdTable from DMDB2_Tabelle where " _
                                   & "IdObjSource = " & GbIdOggetto & ") "
    Parser.PsConnection.Execute "Delete * from DMDB2_IdxCol where " _
                                   & "IdTable in (Select IdTable from DMDB2_Tabelle where " _
                                   & "IdObjSource = " & GbIdOggetto & ") "
    Parser.PsConnection.Execute "Delete * from DMDB2_Tabelle where IdObjSource = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from DMORC_Tablespaces where IdObjSource = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from DMORC_DB where IdObjSource = " & GbIdOggetto
    Parser.PsConnection.Execute "Delete * from DMORC_Alias where " _
                                   & "IdTable in (Select IdTable from DMORC_Tabelle where " _
                                   & "IdObjSource = " & GbIdOggetto & ") "
    Parser.PsConnection.Execute "Delete * from DMORC_Colonne where " _
                                   & "IdTable in (Select IdTable from DMORC_Tabelle where " _
                                   & "IdObjSource = " & GbIdOggetto & ") "
    Parser.PsConnection.Execute "Delete * from DMORC_Index where " _
                                   & "IdTable in (Select IdTable from DMORC_Tabelle where " _
                                   & "IdObjSource = " & GbIdOggetto & ") "
    Parser.PsConnection.Execute "Delete * from DMORC_IdxCol where " _
                                   & "IdTable in (Select IdTable from DMORC_Tabelle where " _
                                   & "IdObjSource = " & GbIdOggetto & ") "
    Parser.PsConnection.Execute "Delete * from DMORC_Tabelle where IdObjSource = " & GbIdOggetto
  End If
  Exit Sub
errPsJCL:
  'OK (tmp) - tabella nuova...
  Resume Next
End Sub
'Mauro - Madrid
Sub Carica_TreeView_Progetto(trw As TreeView)
   Dim rs As Recordset
   Dim wKey As String
   Dim wRel As String
   Dim wText As String
   Dim wImg As Long
   Dim wtipo As Long
   
   Set rs = m_Fun.Open_Recordset("Select * From BS_TreeView Order by ID ")
   If rs.RecordCount > 0 Then
      
      trw.Nodes.Clear
      
      While Not rs.EOF
         wKey = Trim(rs!Key)
         wRel = m_Fun.TN(rs!NRelative, vbString, True)
         
         If InStr(1, Trim(rs!Testo), "$") > 0 Then
            wText = m_Fun.Crea_Directory_Progetto(rs!Testo, Parser.PsPathDef)
         Else
            wText = rs!Testo
         End If
         
         wImg = rs!Image
         wtipo = rs!Tipo
         
         If wRel = "" Then
            trw.Nodes.Add , , wKey, wText, wImg
         Else
            trw.Nodes.Add wRel, wtipo, wKey, wText, wImg
         End If
         trw.Nodes.item(trw.Nodes.count).Tag = rs!livello
         
         Select Case rs!livello
            Case "L0"
               trw.Nodes.item(trw.Nodes.count).Bold = True
               If wKey = "MB" Then
                  trw.Nodes.item(trw.Nodes.count).ForeColor = vbBlack
               Else
                  trw.Nodes.item(trw.Nodes.count).ForeColor = vbRed
               End If
            Case "L1"
               trw.Nodes.item(trw.Nodes.count).Bold = True
               trw.Nodes.item(trw.Nodes.count).ForeColor = vbBlue
            Case "L2"
               trw.Nodes.item(trw.Nodes.count).Bold = False
               trw.Nodes.item(trw.Nodes.count).ForeColor = vbBlack
         End Select
         rs.MoveNext
      Wend
   End If
   rs.Close
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''
' "Modulo" del Cancellaoggetto (specifico per i dati)
'''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub deleteDataRelations()
  Parser.PsConnection.Execute "Delete * from PsData_Area where idoggetto = " & GbIdOggetto
  Parser.PsConnection.Execute "Delete * from PsData_Cmp where idoggetto = " & GbIdOggetto
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Aggiornamento segnalazioni "VALUE NOT FOUND" (Codice Hxx)
' in "avvisi"... (Codice Kxx)
' Iniziamo con le CPY... aggiungeremo il resto...
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub updateSegnalazioni(value As String, relazione As String)
  Dim rsRelObj As Recordset, rsSegnalazioni As Recordset
  Dim codici As String, newCode As String
  Dim i As Long
  
  Select Case relazione
    Case "CPY"
      codici = "'H03','H08','H09','H20','H30'"  'Program,Mapset,Instruction,Dataset
    Case Else
      'nop
      Exit Sub
  End Select
  
  Set rsRelObj = m_Fun.Open_Recordset("select IdOggettoC from PsRel_Obj where IdOggettoR = " & GbIdOggetto & " and Relazione = '" & relazione & "'")
  While Not rsRelObj.EOF
    'Update Segnalazione: da Hxx a Kxx
    Set rsSegnalazioni = m_Fun.Open_Recordset("select Codice,var2 from BS_Segnalazioni where IdOggetto = " & rsRelObj!IdOggettoC & " and Var1 = '" & Replace(value, "'", "''") & "' and codice in (" & codici & ")")
    If rsSegnalazioni.RecordCount Then
      'SQ
       'While rsSegnalazioni.RecordCount
       While Not rsSegnalazioni.EOF
         rsSegnalazioni!codice = "K" & Mid(rsSegnalazioni!codice, 2)
         rsSegnalazioni!var2 = value
         rsSegnalazioni.MoveNext
       Wend
       rsSegnalazioni.Close
       ''''''''''''''''''''''''
       'Aggiornamento R.C. - Al momento non serve: non eliminiamo le segnalazioni...
       ' dovremmo analizzare la situazione in seguito alla "scoperta"!
       ''''''''''''''''''''''''
'      For i = 1 To Parser.PsObjList.ListItems.Count
'         If Parser.PsObjList.ListItems(i).Text = Format(rsRelObj!IdOggettoC, "000000") Then
'            Exit For
'         End If
'      Next i
'      Parser.UpdateErrLevel rsRelObj!IdOggettoC, IIf(i > Parser.PsObjList.ListItems.Count, 0, i)
    End If
    rsRelObj.MoveNext
  Wend
  rsRelObj.Close
End Sub
Public Function getEXEC(line As String, FD As Long) As String
  Dim statement As String
  Dim pos As Long
  
  GbOldNumRec = GbNumRec  'linea iniziale
  statement = line
  'SQ 16-02-06
  pos = InStr(statement, "END-EXEC")
  'SQ 16-02-06
  Do Until EOF(FD) 'Or pos
    'SQ 16-02-06
    If pos = 0 Then
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
    Else
      'Sono gi� sulla riga finale
    End If
    If Mid(line, 7, 1) <> "*" Then  'per le righe successive alla prima... (ocio, pericoloso...)
      line = Trim(Mid(line, 8, 65))
      If Len(line) Then
        statement = statement & " " & line
        pos = InStr(statement, "END-EXEC")
        If pos Then
           Exit Do
        End If '
      End If
    End If
  Loop
  'SQ 16-02-06... da verificare
  'Mi arriva il file gi� chiuso (line=ultima riga file)
  If EOF(FD) And pos = 0 Then
    pos = InStr(statement, "END-EXEC")
  End If
  'SQ 10-10-07
  'Se manca l'END-EXEC! (� capitato... Atos)
  'inseriva nel campo memo una stringa lunghissima e Access si incartava!
  If pos Then
    'SQ pezza: fare bene nel giro sopra: pu� trovare quello del successivo!
    getEXEC = Trim(Left(statement, pos - 1))
    If InStr(getEXEC, " EXEC ") Then
      getEXEC = ""
      addSegnalazione Left(statement, 20) & "...", "P00", Left(statement, 20) & "..."
    End If
  Else
    addSegnalazione Left(statement, 20) & "...", "P00", Left(statement, 20) & "..."
  End If
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 9-11-06
' Array con valori campo:
' - indice 0: INIT VALUE
' - indice i: MOVE
' Utilizza "CampiMove" come "cache"...
' TMP: risolve MOVE in "CPY" di "primo livello"
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getFieldValues(fieldName As String) As String()
  Dim values() As String
  Dim i As Integer, j As Integer
  Dim rs As Recordset
  Dim value As String
  Dim pos As String
  
  ReDim values(0)
  'INIT VALUE:
  values(0) = getFieldValue(fieldName)
  
  'Campo gi� analizzato?:
  For i = 0 To UBound(CampiMove)
    If fieldName = CampiMove(i).NomeCampo Then
      For j = 1 To CampiMove(i).Valori.count
        'MsgBox CampiMove(i).Valori.item(j)
        If Len(CampiMove(i).Valori.item(j)) Then
          'SQ - PEZZA... SISTEMARE!
          If (CampiMove(i).Posizioni.count) > j Then
            pos = CampiMove(i).Posizioni.item(j)
          Else
            pos = ""
          End If
          ' Mauro 09/05/2008 : Non ci sono variabili in CampiMove. E' inutile controllare l'apice
          'If Left(CampiMove(i).Valori.item(j), 1) = "'" Then
            value = CampiMove(i).Valori.item(j)
          'Else
          '  value = getFieldValue(CampiMove(i).Valori.item(j))
          'End If
          If Len(value) And pos = "" Then
            ReDim Preserve values(UBound(values) + 1)
            values(UBound(values)) = value
          End If
        End If
      Next j
      Exit For
    End If
  Next i
  
  If i > UBound(CampiMove) Then
    ReDim Preserve CampiMove(UBound(CampiMove) + 1)
    CampiMove(UBound(CampiMove)).NomeCampo = fieldName
    Set CampiMove(UBound(CampiMove)).Valori = New Collection
    Set CampiMove(UBound(CampiMove)).Posizioni = New Collection
    'Vado sul DB:
    'IRIS-DB dovresti andare da un'altra parte!
    If Not InStr(fieldName, "'") > 0 Then
      Set rs = m_Fun.Open_Recordset("SELECT valore, destSTART FROM PsData_CmpMoved WHERE idPgm=" & GbIdOggetto & " AND Nome = '" & fieldName & "'")
      While Not rs.EOF
        If Len(rs!deststart & "") = 0 Then 'AC
          If Left(rs!Valore, 1) = "'" Then
            value = Trim(Replace(rs!Valore, "'", ""))
          Else
            value = getFieldValue(rs!Valore)
          End If
        Else
          value = ""
        End If
        ReDim Preserve values(UBound(values) + 1)
        values(UBound(values)) = value
        CampiMove(UBound(CampiMove)).Valori.Add value
        CampiMove(UBound(CampiMove)).Posizioni.Add rs!deststart & ""
        rs.MoveNext
      Wend
      rs.Close
      'SQ 19-12-06: "CPY" di "primo" livello
      'SQ 9-05-08: "INC"!!!!!!!!!!!!!!!!!!!! (portare a CPY anche INC!!!!!!!!!!!!!!)
      Set rs = m_Fun.Open_Recordset("SELECT a.valore, a.destSTART From PsData_CmpMoved as a, PsRel_Obj as b " & _
                                    "WHERE b.idOggettoC = " & GbIdOggetto & " and b.relazione IN ('CPY','INC') And " & _
                                    "a.IdPgm = b.IdOggettoR and a.Nome = '" & fieldName & "'")
      While Not rs.EOF
        If Len(rs!deststart & "") = 0 Then 'AC
          If Left(rs!Valore, 1) = "'" Then
            value = Trim(Replace(rs!Valore, "'", ""))
          Else
            value = getFieldValue(rs!Valore)
          End If
        Else
          value = ""
        End If
        ReDim Preserve values(UBound(values) + 1)
        values(UBound(values)) = value
        CampiMove(UBound(CampiMove)).Valori.Add value
        CampiMove(UBound(CampiMove)).Posizioni.Add rs!deststart & ""
        rs.MoveNext
      Wend
      rs.Close
    '''    ' Mauro 14/01/2009: "CPY" e "INC" di "secondo" livello
    '''    Set rs = m_Fun.Open_Recordset( _
    '''      "SELECT a.valore,a.destSTART From PsData_CmpMoved as a,PsRel_Obj as b " & _
    '''      " WHERE b.idOggettoR=" & GbIdOggetto & " And a.IdPgm=b.IdOggettoC and a.nome='" & fieldName & "'")
    '''    While Not rs.EOF
    '''      If Len(rs!deststart & "") = 0 Then 'AC
    '''        If Left(rs!Valore, 1) = "'" Then
    '''          value = Trim(Replace(rs!Valore, "'", ""))
    '''        Else
    '''          value = getFieldValue(rs!Valore)
    '''        End If
    '''      Else
    '''        value = ""
    '''      End If
    '''      ReDim Preserve values(UBound(values) + 1)
    '''      values(UBound(values)) = value
    '''      CampiMove(UBound(CampiMove)).Valori.Add value
    '''      CampiMove(UBound(CampiMove)).Posizioni.Add rs!deststart & ""
    '''      rs.MoveNext
    '''    Wend
    '''    rs.Close
    Else
      value = Trim(Replace(fieldName, "'", ""))
      ReDim Preserve values(UBound(values) + 1)
      values(UBound(values)) = value
      CampiMove(UBound(CampiMove)).Valori.Add value
      CampiMove(UBound(CampiMove)).Posizioni.Add "1" & ""
    End If
  End If
  
  getFieldValues = values
End Function
Private Function CheckCobolLen(statement As String) As String
 Dim j, k As Integer
 Dim splitLine As String
 Dim splitLine2 As String, inputText As String, starttag As String, lenUtil As Integer
 
If Len(RTrim(statement)) >= 73 Then
    'a capo automatico...
    'stefano: VALE SOLO PER IL COBOL, per ora...
    
     j = Len(RTrim(statement))
     splitLine = (RTrim(statement))
     k = 72
     splitLine2 = Space(72)
     Do While Mid(splitLine, j, 1) <> " " Or j > 72
      Mid(splitLine2, k, 1) = Mid(splitLine, j, 1)
      Mid(splitLine, j, 1) = " "
      k = k - 1
      j = j - 1
     Loop
     If j > 0 Then
         
        If Left(Trim(splitLine2), 1) = "'" And InStr(splitLine, "('") > 0 Then
            Dim strParam As String, numSpaces As Integer, OldLen As Integer
            strParam = Trim(Right(splitLine, Len(splitLine) - InStr(splitLine, "('") + 1))
            If Len(strParam) < 10 Then
                strParam = strParam & Space(10 - Len(strParam))
            End If
            splitLine = Left(splitLine, InStr(splitLine, "('") - 1)
            OldLen = Len(splitLine2)
            splitLine2 = Trim(splitLine2)
            If InStr(splitLine, "CALL") > 0 Then
                numSpaces = InStr(splitLine, "CALL") - 1
            Else
                numSpaces = OldLen - Len(splitLine2)
            End If
            splitLine2 = Space(numSpaces) & strParam & splitLine2
            statement = RTrim(splitLine) & Space(72 - Len(RTrim(splitLine)))
            
            inputText = inputText & statement & vbCrLf
            If Len(RTrim(splitLine2)) < 72 Then
                statement = RTrim(splitLine2) & Space(72 - Len(RTrim(splitLine2)))
            Else
                statement = Right(RTrim(splitLine2), 72)
            End If
            inputText = inputText & statement & vbCrLf
        Else
            statement = RTrim(splitLine) & Space(72 - Len(RTrim(splitLine)))
            inputText = inputText & statement & vbCrLf
            'space(20) arbitratrio
            statement = starttag & Space(20) & LTrim(splitLine2) & Space(72 - Len(LTrim(splitLine2)))
            'AC 17/06/08 ritocco space(20) se necessario
            lenUtil = Len(starttag & Space(20) & LTrim(splitLine2))
            If lenUtil > 72 Then
              statement = starttag & Space(20 - (lenUtil - 72)) & LTrim(splitLine2) & Space(72 - Len(LTrim(splitLine2)))
            End If
            inputText = inputText & statement & vbCrLf
        End If
     End If
  Else
    inputText = statement
  End If
  CheckCobolLen = inputText
End Function
'SQ - GESTIONE TAG DI TEMPLATE
Public Sub changeTag(RTB As RichTextBox, Tag As String, Text As String)
  Dim Posizione As Long
  Dim crLfPosition As Long
  Dim indentedText As String
  Dim i As Integer, arrSplit() As String
  
  '''''''''''''''''''''''''
  ' SQ 23-11-06
  ' Tag di template
  ' text � il Nome del template!
  '''''''''''''''''''''''''
  If isTemplateTag(Tag) And Len(Text) Then
    Dim rtbBackup As String
    On Error GoTo catch
    'N.B. Ricordarsi della "loadTemplate" del DM!
    rtbBackup = RTB.Text
    RTB.LoadFile Text
    Text = RTB.Text
    RTB.Text = rtbBackup
    On Error GoTo 0
  End If
  
  If isCiclicTag(Tag) Then
    If Len(Text) Then Text = Text & vbCrLf & Tag 'se text="" e' un clean!...
  End If
  While True
    Posizione = RTB.Find(Tag, Posizione + Len(indentedText))
    Select Case Posizione
      Case -1
        Exit Sub
      Case 0
        indentedText = Text
      Case Else
        crLfPosition = InStrRev(RTB.Text, vbCrLf, Posizione)
        If crLfPosition = 0 Then
          'Linea 1: non ho il fine linea...
          indentedText = Replace(Text, vbCrLf, vbCrLf & Space(Posizione))
        Else
          'SQ 10-11-06 - TMP: GESTIONE COMMENTI (TROPPO lungo... ottimizzare!)
          indentedText = Replace(Text, vbCrLf, vbCrLf & Space(Posizione - crLfPosition - 1))
          'AC gestione stringa su pi� righe con '-' a colonna 7
          indentedText = Replace(indentedText, vbCrLf & "             -'", vbCrLf & Space(6) & "-    '")
          'Porto indietro eventuali "*" di colonna "ex 1"...
          
          arrSplit() = Split(indentedText, vbCrLf)
          For i = 0 To UBound(arrSplit)
            arrSplit(i) = CheckCobolLen(arrSplit(i))
          Next i
          indentedText = ""
          For i = 0 To UBound(arrSplit)
            indentedText = indentedText & arrSplit(i) & IIf(i = UBound(arrSplit), "", vbCrLf)
          Next i
          
         If (Posizione - crLfPosition - 1) > 6 Then
            'NON FUNGE?!
            If Left(indentedText, 1) = "*" Then
              indentedText = vbCrLf & Space(6) & indentedText
            End If
            indentedText = Replace(indentedText, vbCrLf & Space(Posizione - crLfPosition - 1) & "*", vbCrLf & Space(6) & "*")
          End If
        End If
    End Select
    RTB.SelStart = Posizione
    RTB.SelLength = Len(Tag)
    RTB.SelText = indentedText
  Wend
  Exit Sub
catch:
  If err.Number = 9 Then
    m_Fun.writeLog "#Template: " & err.description & " not found."
  Else
    m_Fun.writeLog "#changeTag: " & err.description
  End If
End Sub
'SQ - Gestione "templateTag"
Public Function isCiclicTag(Tag As String) As Boolean
  isCiclicTag = Right(Tag, 4) = "(i)>" Or Right(Tag, 5) = "(i)>>"
End Function
Public Function isTemplateTag(Tag As String) As Boolean
  isTemplateTag = Left(Tag, 2) = "<<"
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''
' Reset Tabelle "specifiche" di inizio parsing
''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub resetRepository(itemType As String)
  Dim rs As Recordset
  
  On Error GoTo catch
  
  Select Case itemType
    Case "DBD"
      'SEGMENTI, XDFLD RELATIVI A QUESTO DBD
      Set rs = m_Fun.Open_Recordset("select * from PsDli_segmenti where idoggetto = " & GbIdOggetto)
      While Not rs.EOF
        Parser.PsConnection.Execute "DELETE * FROM PsDli_XDfield WHERE idsegmento = " & rs!idSegmento
        Parser.PsConnection.Execute "DELETE * FROM PsDli_Field WHERE idsegmento = " & rs!idSegmento
        Parser.PsConnection.Execute "DELETE * FROM PsDli_segmenti_SOURCE WHERE idsegmento = " & rs!idSegmento
        rs.Delete
        rs.MoveNext
      Wend
      rs.Close
      Parser.PsConnection.Execute "DELETE * FROM PsRel_DliSeg where idoggetto = " & GbIdOggetto
    Case Else
      m_Fun.writeLog "TMP: resetRepository, tipo: " & itemType
  End Select
  Exit Sub
catch:
  m_Fun.writeLog "###resetRepositori, " & GbIdOggetto & " - " & err.description
  Resume Next
End Sub

'Inserisce le foreign key
' Input:
'       la parte di riga interessata (wStr)
'       il tipo di db (wTypeSql)
' ritorna un booleano
' True  --> OK
' False --> KO
Public Function insertForeignKey(wStr As String, wTypeSql As String) As Boolean
  'dichiazione variabili locali
  Dim wNomeTable As String
  Dim wNomeCreator As String
  Dim wNomeIndex As String
  Dim wTipoIndex As String
  Dim Wstr2 As String
  Dim wStr1  As String
  Dim wRec As String
  
  Dim k As Integer
  Dim k1 As Integer
  
  Dim wOnCascade As Boolean
  Dim wOnDelete As Boolean
  Dim wOnNoAction As Boolean
  Dim wOnRestrict As Boolean
  Dim wOnSetNull As Boolean
  
  Dim wIdIndex As Long
  Dim wIdxCol As Long
  Dim wIdCol As Long
  
  Dim wIdTable As Long
  Dim wIdTableSpc As Long
  
  Dim wIdTableFor As Long
  Dim wIdTableSpcFor As Long
  
  Dim TbIndex As Recordset
  Dim tbTable As Recordset
  Dim TbCol As Recordset
  Dim TbIdxCol As Recordset
  
  On Error GoTo errorHandler
  insertForeignKey = True
  
  wNomeTable = TrovaParamDDL(wStr, " TABLE ")
  k = InStr(wNomeTable, ".")
  If k Then
    wNomeCreator = Mid$(wNomeTable, 1, k - 1)
    wNomeTable = Mid$(wNomeTable, k + 1)
  End If
  'aggiunge 1 all'ultimo
  Set TbIndex = m_Fun.Open_Recordset("Select * from " & wTypeSql & "Index order by idindex")
  If TbIndex.RecordCount Then
    TbIndex.MoveLast
    wIdIndex = TbIndex!IdIndex + 1
  Else
    wIdIndex = 1
  End If

  'tabella primaria
  wNomeIndex = TrovaParamDDL(wStr, " KEY ")
  If Mid$(wNomeIndex, 1, 1) = "(" Then
    wNomeIndex = "FORKEY_" & format(wIdIndex, "00000")
  End If
  
  wTipoIndex = "F"
  Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "Tabelle where " & _
                                     "Nome = '" & wNomeTable & "' and creator = '" & wNomeCreator & "'")
  If tbTable.RecordCount Then
    wIdTable = tbTable!IdTable
    wIdTableSpc = tbTable!IdTableSpc
  Else
    wIdTable = -1
    wIdTableSpc = -1
  End If
  
  'elimina la fk (se presente)
  Set TbIndex = m_Fun.Open_Recordset("Select * from " & wTypeSql & "Index WHERE " & _
                                     "Nome = '" & wNomeIndex & "' and IdTable = " & wIdTable & " and IdTableSpc = " & wIdTableSpc)
  If TbIndex.RecordCount Then
    wIdxCol = TbIndex!IdIndex
    Parser.PsConnection.Execute "DELETE * FROM " & wTypeSql & "Index WHERE Nome = '" & wNomeIndex & "' and IdTable = " & wIdTable & " and IdTableSpc = " & wIdTableSpc
    Parser.PsConnection.Execute "DELETE * FROM " & wTypeSql & "IdxCol WHERE IdIndex=" & wIdxCol
  End If
      
  'tabella collegata
  wNomeTable = TrovaParamDDL(wStr, " REFERENCES ")
  k = InStr(wNomeTable, ".")
  If k Then
    wNomeCreator = Mid$(wNomeTable, 1, k - 1)
    wNomeTable = Mid$(wNomeTable, k + 1)
  End If
  Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "Tabelle where Nome = '" & wNomeTable & "' and creator = '" & wNomeCreator & "' ")
  If tbTable.RecordCount = 0 Then
    wIdTableFor = -1
    wIdTableSpcFor = -1
  Else
    wIdTableFor = tbTable!IdTable
    wIdTableSpcFor = tbTable!IdTableSpc
  End If
  
  'prepara l'inserimento dei campi e inserisce nella tabella index
  wOnCascade = False
  wOnDelete = False
  wOnNoAction = False
  wOnRestrict = False
  wOnSetNull = False
  If InStr(wStr, " DELETE ") > 0 Then wOnDelete = True
  If InStr(wStr, " CASCADE ") > 0 Then wOnCascade = True
  If InStr(wStr, " NULL ") > 0 Then wOnSetNull = True
  If InStr(wStr, " ACTION ") > 0 Then wOnNoAction = True
  If InStr(wStr, " RESTRICT ") > 0 Then wOnRestrict = True
  TbIndex.AddNew
  TbIndex!IdIndex = wIdIndex
  TbIndex!IdTable = wIdTable
  TbIndex!IdTableSpc = wIdTableSpc
  TbIndex!nome = wNomeIndex
  TbIndex!Tipo = wTipoIndex
  TbIndex!Unique = False
  TbIndex!data_creazione = Now
  TbIndex!data_modifica = Now
  TbIndex!idorigine = -1
  TbIndex!used = True
  TbIndex!descrizione = " "
  TbIndex!foreignidtable = wIdTableFor
  TbIndex!IdTableSpcFor = wIdTableSpcFor
  TbIndex!foroncascade = wOnCascade
  TbIndex!foronsetnull = wOnSetNull
  TbIndex!forondelete = wOnDelete
  TbIndex!foronnoaction = wOnNoAction
  TbIndex!foronrestrict = wOnRestrict
  TbIndex.Update
  
  'controlla i campi collegati e li inserisce
  k1 = InStr(wStr, " FOREIGN")
  wStr = Mid$(wStr, k1 + 1)
  k1 = InStr(wStr, "(")
  wStr = Mid$(wStr, k1 + 1)
  k1 = InStr(wStr, ")")
  Wstr2 = Mid$(wStr, k1 + 1)
  wStr = Mid$(wStr, 1, k1)
  wStr = Replace(wStr, ")", ", ")
  wStr = Trim(wStr)
  k = 0
  
  'prima quelli della tabella primaria
  While Len(wStr) > 0
    k1 = InStr(wStr, ",")
    wStr1 = Mid$(wStr, 1, k1 - 1)
    If InStr(wStr1, " IS ") Then
      wStr1 = Left(wStr1, InStr(wStr1, " IS "))
    End If
    wStr = Trim(Mid$(wStr, k1 + 1))
    Set TbCol = m_Fun.Open_Recordset("select * from " & wTypeSql & "Colonne where idtable = " & wIdTable & " and Nome = '" & wStr1 & "' ")
    Set TbIdxCol = m_Fun.Open_Recordset("Select * from " & wTypeSql & "IdxCol where idindex = " & TbIndex!IdIndex)
    
    wIdCol = TbCol!idcolonna
    k = k + 1
    TbIdxCol.AddNew
       TbIdxCol!IdIndex = wIdIndex
       TbIdxCol!IdTable = wIdTable
       TbIdxCol!idcolonna = wIdCol
       TbIdxCol!Ordinale = k
       TbIdxCol!Order = " "
       TbIdxCol!foridtable = -1
       TbIdxCol!foridcolonna = -1
    TbIdxCol.Update
    TbIdxCol.Close
    TbCol.Close
  Wend
  
  
  'poi quelli della tabella collegata
  wStr = Wstr2
  k1 = InStr(wStr, "(")
  If k1 > 0 Then
    wStr = Mid$(wStr, k1 + 1)
    k1 = InStr(wStr, ")")
    wStr = Mid$(wStr, 1, k1)
    wStr = Replace(wStr, ")", ", ")
    wStr = Trim(wStr)
    k = 0
    While Len(wStr) > 0
      k1 = InStr(wStr, ",")
      wStr1 = Mid$(wStr, 1, k1 - 1)
      wStr = Trim(Mid$(wStr, k1 + 1))
      k = k + 1
      Set TbCol = m_Fun.Open_Recordset("select * from " & wTypeSql & "Colonne where idtable = " & wIdTableFor & " and Nome = '" & wStr1 & "' ")
      Set TbIdxCol = m_Fun.Open_Recordset("Select * from " & wTypeSql & "IdxCol where idindex = " & TbIndex!IdIndex & " and ordinale = " & k)
      wIdCol = TbCol!idcolonna
      If TbIdxCol.RecordCount > 0 Then
        'wIdCol = TbCol!IdColonna
        TbIdxCol!foridtable = wIdTableFor
        TbIdxCol!foridcolonna = wIdCol
        TbIdxCol.Update
      Else
        addSegnalazione wStr1, "NOCOLIDX", wNomeIndex
        m_Fun.writeLog "Column for index not recognized: <" & wRec & ">  IdObj: " & GbIdOggetto, "PARSER DLL", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\PARSER.LOG"
      End If
      TbIdxCol.Close
    Wend
  End If
  TbIndex.Close
  tbTable.Close

  Exit Function
errorHandler:
  m_Fun.writeLog "Errore nella insertForeignKey [MapsdM_Parser]: " & wStr & vbCrLf & "Errore: " & err.description
  insertForeignKey = False
End Function

'Inserisce i trigger
' Input:
'       la parte di riga interessata (wStr)
'       il tipo di db (wTypeSql)
' ritorna un booleano
' True  --> OK
' False --> KO
Function insertTrigger(wStr As String, wTypeSql As String) As Boolean
  Dim wEventoTrigger As String, wCampixTrigger As String
  Dim wReferencingTrigger As String, wCondizioneTrigger As String, wTransazioneTrigger As String
  Dim wAzioneTrigger As String, wCorpoTrigger As String, wTipoTrigger As String

  Dim TbTrigger As Recordset, tbTable As Recordset
  Dim wIdTrigger As Long, wIdTable As Long
  
  Dim wNomeTrigger As String, wCreatorTrigger As String
  Dim wNomeTable As String, wNomeCreator As String
  Dim k As Integer
  
  insertTrigger = True
  
  On Error GoTo errorHandler
  
  wCreatorTrigger = ""
  wNomeTrigger = TrovaParamDDL(wStr, "TRIGGER ")
  k = InStr(wNomeTrigger, ".")
  If k Then
    wCreatorTrigger = Left$(wNomeTrigger, k - 1)
    wNomeTrigger = Trim(Mid$(wNomeTrigger, k + 1))
  End If
              
  If InStrRev(wStr, " OF ") Then
    'evento
    wStr = Trim(Mid$(wStr, 8 + Len(wCreatorTrigger) + Len(wNomeTrigger) + 2, Len(wStr)))
    wEventoTrigger = Mid(wStr, 1, InStrRev(wStr, "OF") + 1)
    
    'campi
    wStr = Trim(Mid$(wStr, InStrRev(wStr, " OF ") + 3))
    wCampixTrigger = Trim(Mid$(wStr, 1, InStr(wStr, " ON ")))
  Else
    wStr = Trim(Mid$(wStr, 8 + Len(wCreatorTrigger) + Len(wNomeTrigger) + 2, Len(wStr)))
    wEventoTrigger = Trim(Mid$(wStr, 1, InStr(wStr, " ON ")))
    wCampixTrigger = ""
  End If
  
  'Nome tabella e creator
  wStr = Trim(Mid$(wStr, InStr(wStr, " ON ")))
  wNomeTable = TrovaParamDDL(wStr, "ON ")
  k = InStr(wNomeTable, ".")
  If k Then
    wNomeCreator = Left$(wNomeTable, k - 1)
    If wNomeCreator <> wCreatorTrigger Then
      wNomeCreator = wCreatorTrigger
    End If
    wNomeTable = Trim(Mid$(wNomeTable, k + 1))
  End If
  
  'referencing
  If InStr(wStr, " REFERENCING ") Then
    wStr = Trim(Mid$(wStr, InStr(wStr, " REFERENCING ")))
    wReferencingTrigger = Trim(Mid$(wStr, 12, InStr(wStr, " FOR ") - 12))
  End If
  
  'condizione
  wStr = Trim(Mid$(wStr, InStr(wStr, " FOR ")))
  wCondizioneTrigger = Trim(Mid$(wStr, 1, InStr(wStr, " BEGIN ")))
  
  'transazione
  wStr = Trim(Mid(wStr, InStr(wStr, " BEGIN ")))
  If InStr(wStr, " UPDATE ") Then
    wTransazioneTrigger = Trim(Mid$(wStr, 1, InStr(wStr, " UPDATE ")))
    wStr = Trim(Mid$(wStr, InStr(wStr, " UPDATE ")))
    wTipoTrigger = "UPDATE"
  ElseIf InStr(wStr, " INSERT ") Then
    wTransazioneTrigger = Trim(Mid$(wStr, 1, InStr(wStr, " INSERT ")))
    wStr = Trim(Mid$(wStr, InStr(wStr, " INSERT ")))
    wTipoTrigger = "INSERT"
  ElseIf InStr(wStr, " DELETE ") Then
    wTransazioneTrigger = Trim(Mid$(wStr, 1, InStr(wStr, " DELETE ")))
    wStr = Trim(Mid$(wStr, InStr(wStr, " DELETE ")))
    wTipoTrigger = "DELETE"
  End If
  
  'update  --> set
  'insert  --> ()values ()
  'delete  --> ???????????
  
  'azione e corpo
  If wTipoTrigger = "INSERT" Then
    wAzioneTrigger = Trim(Mid$(wStr, 1, InStr(wStr, " (")))
    wStr = Mid$(wStr, InStr(wStr, " (") + 2)
  ElseIf wTipoTrigger = "UPDATE" Or wTipoTrigger = "" Then 'UPDATE...
    wAzioneTrigger = Trim(Mid$(wStr, 1, InStr(wStr, " SET ")))
    wStr = Mid$(wStr, InStr(wStr, " SET ") + 5)
  Else
    ' DELETE?????
  End If
  wCorpoTrigger = wStr

  'controllo esistenza tabella ed acquisizione idTable
  Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "Tabelle where " & _
                                     "Nome = '" & wNomeTable & "' and Creator = '" & wNomeCreator & "'")
  If tbTable.RecordCount = 0 Then
    'ERRORE: Non  esiste la tabella!!!!!!!!!
  Else
    wIdTable = tbTable!IdTable
    Set TbTrigger = m_Fun.Open_Recordset("select * from " & wTypeSql & "Trigger where " & _
                                         "IdTable = " & wIdTable & " ORDER BY IdTrigger")
    If TbTrigger.RecordCount = 0 Then
      wIdTrigger = 1
    Else
      TbTrigger.MoveLast
      wIdTrigger = TbTrigger!IdTrigger + 1
    End If
    TbTrigger.AddNew
    TbTrigger!IdTrigger = wIdTrigger
    TbTrigger!nome = wNomeTrigger
    TbTrigger!creator = wNomeCreator
    TbTrigger!IdTable = wIdTable
    TbTrigger!Campi = wCampixTrigger
    TbTrigger!Evento = wEventoTrigger
    TbTrigger!Transazione = wTransazioneTrigger
    TbTrigger!Azione = wAzioneTrigger
    TbTrigger!Referencing = wReferencingTrigger
    TbTrigger!condizione = wCondizioneTrigger
    TbTrigger!Corpo = wCorpoTrigger
    TbTrigger!Stato = 1     '?????????
    TbTrigger!data_creazione = Now
    TbTrigger!data_modifica = Now
    TbTrigger!Tag = " Da DDL "
    TbTrigger.Update
    TbTrigger.Close
  End If

  Exit Function
errorHandler:
  m_Fun.writeLog "Errore nella insertTrigger [MapsdM_Parser]: " & wStr & vbCrLf & "Errore: " & err.description
  insertTrigger = False
End Function

'Inserisce le check constraint
' Input:
'       la parte di riga interessata (wStr)
'       il tipo di db (wTypeSql)
'       l'id della tabella (wIdTable)
'       il progressivo (k)
' ritorna un booleano
' True  --> OK
' False --> KO
Function insertCheckConstraint(wStr As String, wTypeSql As String, wIdTable As Long, k As Integer) As Boolean
  'verificare se l'idTable � quello corretto.
  Dim wNomeCheck As String
  Dim parToconvert As String
  Dim sfid() As String
  Dim wIdColonna As String
  Dim w As Integer, kk As Integer
  Dim TbCampi As Recordset, TbCheck As Recordset, rs As Recordset
  
  On Error GoTo errorHandler

  insertCheckConstraint = True

  wNomeCheck = Trim(Mid$(wStr, 1, InStr(wStr, " CHECK ")))
  
  If InStr(wStr, "(") Then
    wStr = Trim(Mid(wStr, InStr(wStr, "(") + 1))
  End If
  sfid = Split(wStr, " ")
  
  'parsa la stringa e sostituisce il Nome con l'id della colonna
  For w = 0 To UBound(sfid)
    kk = InStr(wStr, sfid(w))
    If Left(sfid(w), 1) = "(" Or kk > 0 Then
      parToconvert = getParameter(sfid(w))
      If parToconvert <> "" Then
        'ricava l'id della colonna
        Set TbCampi = m_Fun.Open_Recordset("SELECT * from " & wTypeSql & "Colonne WHERE " & _
                                           "IdTable = " & wIdTable & " AND Nome = '" & parToconvert & "'")
        wIdColonna = -1
        If TbCampi.RecordCount > 0 Then
          wIdColonna = TbCampi!idcolonna
          'sostituisce il Nome con l'id
          wStr = Trim(Replace(wStr, parToconvert, "#" & wIdColonna))
        End If
        TbCampi.Close
      End If
    End If
  Next w
  
  'elimina eventuali precedenti inserimenti
  Set TbCheck = m_Fun.Open_Recordset("SELECT * FROM " & wTypeSql & "Check_Constraint WHERE " & _
                                     "IdTable=" & wIdTable & " AND Nome='" & wNomeCheck & "' and idprogressivo = " & k)
  If TbCheck.RecordCount > 0 Then
    Parser.PsConnection.Execute "DELETE * FROM " & wTypeSql & "Check_Constraint WHERE " & _
                                "IdTable=" & wIdTable & " AND Nome='" & wNomeCheck & "' and idprogressivo = " & k
  Else
     'T 13102010
     Set rs = m_Fun.Open_Recordset("SELECT max(IdProgressivo) as Idmax FROM " & wTypeSql & "Check_Constraint WHERE " & _
                                     "IdTable=" & wIdTable)
     If rs!idmax > 0 Then
       k = rs!idmax + 1
     End If
     rs.Close
     ''
  End If

  TbCheck.AddNew
  TbCheck!IdProgressivo = k
  TbCheck!IdTable = wIdTable
  TbCheck!nome = wNomeCheck
  TbCheck!condizione = wStr
  TbCheck!data_creazione = Now
  TbCheck!data_modifica = Now
  TbCheck!Tag = "Da DDL"
  TbCheck.Update
  TbCheck.Close

  Exit Function
errorHandler:
  If err.Number = -2147217900 Then
    Resume Next
  Else
    m_Fun.writeLog "Errore nella insertCheckConstraint [MapsdM_Parser]: " & wStr & vbCrLf & "Errore: " & err.description
    insertCheckConstraint = False
  End If
End Function

'formatta la stringa escludendo parametri in eccesso
' Input:
'       la parte di riga interessata (x)
' ritorna una stringa 'finita'
Private Function getParameter(x As String) As String

  Dim car As String
  Dim xx As String
  Dim i As Integer
        
  On Error GoTo errorHandler

  For i = 1 To Len(x)
    car = Mid(x, i, 1)
        
    If car <> "(" Then
      xx = Mid(x, i)
      Exit For
    End If
  Next i

  xx = Replace(xx, "'", "")
  getParameter = Trim(xx)

  Exit Function
errorHandler:
  m_Fun.writeLog "Errore nella getParameter [MapsdM_Parser]: " & err.description
  getParameter = ""
End Function

' P = Primary Key
' I = Index
Function ControllaPrimaryKey(IdTable As Long, Campi() As String, wTypeSql As String) As String
  Dim k, i As Integer
  Dim wIdIndex As Long
  Dim rs As Recordset
  Dim NomeCampo As String
  Dim Order As String
  
  On Error GoTo errorHandler
  ControllaPrimaryKey = "P"
  Set rs = m_Fun.Open_Recordset("select a.* from " & wTypeSql & "IdxCol as a, " & wTypeSql & "index as b" & _
                                " where b.Idtable = " & IdTable & _
                                " and b.tipo = 'P'" & _
                                " and a.idindex = b.idindex")
  If Not (rs.RecordCount = UBound(Campi)) Then
    ControllaPrimaryKey = "I"
  Else
    wIdIndex = rs!IdIndex
  End If
  rs.Close
  If ControllaPrimaryKey = "P" Then
    For i = 1 To UBound(Campi)
      k = InStr(Campi(i), ",")
      NomeCampo = Trim(Mid(Campi(i), 1, k - 1))
      Order = Trim(Mid(Campi(i), k + 1))
      Set rs = m_Fun.Open_Recordset("select a.order, b.Nome from " & wTypeSql & "IdxCol as a, " & wTypeSql & "colonne as b" & _
                                    " where a.idindex = " & wIdIndex & _
                                    " and a.idcolonna = b.idcolonna" & _
                                    " and b.Nome = '" & NomeCampo & "'")
      If rs.RecordCount = 0 Or Order = "DESC" Then
        ControllaPrimaryKey = "I"
      End If
      rs.Close
    Next i
  End If
  ' Elimino la PRIMARY KEY esistente e imposto la nuova
  If ControllaPrimaryKey = "P" Then
    Parser.PsConnection.Execute "Delete * from " & wTypeSql & "Index where IdIndex  = " & wIdIndex
    Parser.PsConnection.Execute "Delete * from " & wTypeSql & "IdxCol where IdIndex = " & wIdIndex
  End If
  Exit Function
errorHandler:
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''
'SQ - 8-02-07
' Parameters: Nome campo, idOggetto corrente
' Result: recordset (idOggetto "reale" - cpy/pgm)
''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function findDataCmp(idOggetto As Long, fieldName As String) As Recordset
  Dim rsArea As Recordset, rsLink As Recordset, rsCmp As Recordset

  Set rsArea = m_Fun.Open_Recordset("SELECT * FROM PsData_Cmp WHERE Nome='" & fieldName & "' And IdOggetto=" & idOggetto)
  If rsArea.RecordCount = 0 Then
    'Ricerca nelle COPY:
    Set rsLink = m_Fun.Open_Recordset("SELECT IdOggettoR FROM PsRel_Obj WHERE IdOggettoC=" & idOggetto & " And Relazione='CPY'")
    Do While Not rsLink.EOF
      Set rsCmp = m_Fun.Open_Recordset("Select * From PsData_Cmp Where Nome = '" & fieldName & "' And IdOggetto = " & rsLink!idOggettoR)
      If rsCmp.RecordCount Then
         Set rsArea = rsCmp
         Exit Do
      End If
      rsLink.MoveNext
    Loop
    rsLink.Close
  End If
  
  Set findDataCmp = rsArea
End Function

Public Sub checkIncaps(fileName As String)
  Dim rs As Recordset, rsCall As Recordset
  Dim FD As Long
  Dim line As String, token As String, execStatement As String
  Dim copyEntry As Boolean, isLabel As Boolean
  Dim lineNumber As Long
  'SQ tmp
  If True Then
    checkIncapsLEVEL fileName
    Exit Sub
  End If
  
  On Error GoTo parseErr
  
  Set rs = m_Fun.Open_Recordset("SELECT * FROM TMP_checkIncaps")
  Set rsCall = m_Fun.Open_Recordset("SELECT * FROM TMP_checkNONIncaps")
  lineNumber = 0
  FD = FreeFile
  Open fileName For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(line) = 0 Then
      Line Input #FD, line
      lineNumber = lineNumber + 1
      ''''''''''''''''''''''''''
      'Gestione LABEL
      ''''''''''''''''''''''''''
      isLabel = Mid(line, 8, 1) <> " "
    Else
      'Ho gia' una linea da analizzare (letta dalla getCall, per es.)
      isLabel = False
    End If
    
    If Mid(line, 7, 1) <> "*" Then
      'LINEA VALIDA
      line = RTrim(Mid(line, 8, 65)) 'elimino etichette!
      'token = nextToken(line)
      token = nextToken_new(line) 'MANTIENE GLI APICI!!
      If token = "MOVE" Then
          ''''''''''''''''''''''''''''''''
          Dim codifica As String
          codifica = nexttoken(line)
          While token <> "TO"
            token = nexttoken(line)
            If Len(token) = 0 Then  'fine linea!
              'forzo l'entrata:
              line = "************"
              While Mid(line, 7, 1) = "*"
                Line Input #FD, line
                lineNumber = lineNumber + 1
                line = RTrim(Mid(line, 8, 65))
              Wend
            End If
            
          Wend
          'Campo destinazione:
          token = nexttoken(line)
          If token = "LNKDB-OPERATION" Or token = "LNKDB-OPERATION." Then
            ''''m_Fun.WriteLog "@@@@@@@@@@@@@@@@" & fileName & " " & lineNumber & " " & codifica
            ''''''''''''
            'TROVATO!
            ''''''''''''
            rs.AddNew
            rs!PGM = fileName
            rs!line = lineNumber
            rs!routine = codifica
          ElseIf Len(token) = 0 Then
            'forzo l'entrata:
            line = "************"
            While Mid(line, 7, 1) = "*"
              Line Input #FD, line
              lineNumber = lineNumber + 1
              line = RTrim(Mid(line, 8, 65))
            Wend
            token = nexttoken(line)
            If token = "LNKDB-OPERATION" Or token = "LNKDB-OPERATION." Then
              'm_Fun.WriteLog "@@@@@@@@@@@@@@@@" & fileName & " " & lineNumber & " " & codifica
              ''''''''''''
              'TROVATO!
              ''''''''''''
              rs.AddNew
              rs!PGM = fileName
              rs!line = lineNumber
              rs!routine = codifica
            ElseIf Len(token) = 0 Then
              MsgBox "ATTENZIONE! NON VA BENE: Ancora nuova linea - PGM: " & fileName & " - ROW: " & lineNumber
            End If
          End If
          ''''''''''''''''''''''''''''''''
          line = ""
          copyEntry = False
      ElseIf token = "CALL" Then
        ''''''''''''''''''''''''''''''''''
        ' CHECK CBLTDLI NON INCAPSULATE
        ''''''''''''''''''''''''''''''''''
        token = nexttoken(line)
        If Len(token) Then
          If token = "'CBLTDLI'" Or token = "CBLTDLI" Or _
             token = "'PLITDLI'" Or token = "PLITDLI" Or _
             token = "'DBINTDLI'" Or token = "DBINTDLI" Then
            rsCall.AddNew
            rsCall!PGM = fileName
            rsCall!line = lineNumber
          End If
        Else
          'caso tossico: CALL e routine su righe diverse
          MsgBox "TMP: CALL su righe diverse! - PGM: " & fileName & " - ROW: " & lineNumber
        End If
      Else
        line = ""
        copyEntry = False
      End If
    Else
      line = ""
    End If
  Wend
  Close FD
  
  On Error Resume Next
  rs.Update
  rsCall.Update
  
  rs.Close
  rsCall.Close
  Exit Sub
  
parseErr:   'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      lineNumber = lineNumber + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, line
        lineNumber = lineNumber + 1
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          MsgBox err.description
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        MsgBox err.description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    MsgBox err.description
    Resume Next
  Else
    MsgBox err.description
    Close FD
  End If
End Sub
Public Sub checkIncapsLEVEL(fileName As String)
  Dim rs As Recordset, rsCall As Recordset
  Dim FD As Long
  Dim line As String, token As String, execStatement As String
  Dim copyEntry As Boolean, isLabel As Boolean
  Dim lineNumber As Long
  Dim level As String, Ordinale As String
  
  On Error GoTo parseErr
  
  Set rs = m_Fun.Open_Recordset("SELECT * FROM TMP_checkIncaps")
  Set rsCall = m_Fun.Open_Recordset("SELECT * FROM TMP_checkNONIncaps")
  lineNumber = 0
  FD = FreeFile
  Open fileName For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(line) = 0 Then
      Line Input #FD, line
      lineNumber = lineNumber + 1
      ''''''''''''''''''''''''''
      'Gestione LABEL
      ''''''''''''''''''''''''''
      isLabel = Mid(line, 8, 1) <> " "
    Else
      'Ho gia' una linea da analizzare (letta dalla getCall, per es.)
      isLabel = False
    End If
    
    If Mid(line, 7, 1) <> "*" Then
      'LINEA VALIDA
      line = RTrim(Mid(line, 8, 65)) 'elimino etichette!
      'token = nextToken(line)
      token = nextToken_new(line) 'MANTIENE GLI APICI!!
      If token = "MOVE" Then
          ''''''''''''''''''''''''''''''''
          Dim codifica As String
          codifica = nexttoken(line)
          While token <> "TO"
            token = nexttoken(line)
            If Len(token) = 0 Then  'fine linea!
              'forzo l'entrata:
              line = "************"
              While Mid(line, 7, 1) = "*"
                Line Input #FD, line
                lineNumber = lineNumber + 1
                line = RTrim(Mid(line, 8, 65))
              Wend
            End If
            
          Wend
          'Campo destinazione:
          token = nexttoken(line)
          If token = "LNKDB-OPERATION" Or token = "LNKDB-OPERATION." Then
            level = ""
            'SQ LIVELLI
            Do While Not EOF(FD)
              Line Input #FD, line
              lineNumber = lineNumber + 1
              If Mid(line, 7, 1) <> "*" Then
                line = RTrim(Mid(line, 8, 65))
                token = nexttoken(line)
                If token = "CALL" Then
                  'Fine istruzione:
                  Exit Do
                ElseIf InStr(line, "LNKDB-SEGLEVEL") Then
                  'il mio uomo:
                  level = nexttoken(line)
                  token = nexttoken(line) 'TO
                  token = nexttoken(line)  'LNKDB-SEGLEVEL
                  Ordinale = nexttoken(line)
                  'INSERT
                  rs.AddNew
                  rs!PGM = fileName
                  rs!line = lineNumber
                  rs!routine = codifica
                  rs!Ordinale = Replace(Replace(Ordinale, ")", ""), "(", "")
                  rs!level = level
                End If
              End If
            Loop
          ElseIf Len(token) = 0 Then
            'forzo l'entrata:
            line = "************"
            While Mid(line, 7, 1) = "*"
              Line Input #FD, line
              lineNumber = lineNumber + 1
              line = RTrim(Mid(line, 8, 65))
            Wend
            token = nexttoken(line)
            If token = "LNKDB-OPERATION" Or token = "LNKDB-OPERATION." Then
              level = ""
              'SQ LIVELLI
              Do While Not EOF(FD)
                Line Input #FD, line
                lineNumber = lineNumber + 1
              If Mid(line, 7, 1) <> "*" Then
                line = RTrim(Mid(line, 8, 65))
                token = nexttoken(line)
                If token = "CALL" Then
                  'Fine istruzione:
                  Exit Do
                ElseIf InStr(line, "LNKDB-SEGLEVEL") Then
                  'il mio uomo:
                  level = nexttoken(line)
                  token = nexttoken(line) 'TO
                  token = nexttoken(line)  'LNKDB-SEGLEVEL
                  Ordinale = nexttoken(line)
                  'INSERT
                  rs.AddNew
                  rs!PGM = fileName
                  rs!line = lineNumber
                  rs!routine = codifica
                  rs!Ordinale = Replace(Replace(Ordinale, ")", ""), "(", "")
                  rs!level = level
                End If
              End If
              Loop
            ElseIf Len(token) = 0 Then
              'MsgBox "ATTENZIONE! NON VA BENE: Ancora nuova linea - PGM: " & fileName & " - ROW: " & lineNumber
            End If
          End If
          ''''''''''''''''''''''''''''''''
          line = ""
          copyEntry = False
      ElseIf token = "CALL" Then
        ''''''''''''''''''''''''''''''''''
        ' CHECK CBLTDLI NON INCAPSULATE
        ''''''''''''''''''''''''''''''''''
        token = nexttoken(line)
        If Len(token) Then
          If token = "'CBLTDLI'" Or token = "CBLTDLI" Or _
             token = "'PLITDLI'" Or token = "PLITDLI" Or _
             token = "'DBINTDLI'" Or token = "DBINTDLI" Then
            rsCall.AddNew
            rsCall!PGM = fileName
            rsCall!line = lineNumber
          End If
        Else
          'caso tossico: CALL e routine su righe diverse
          MsgBox "TMP: CALL su righe diverse! - PGM: " & fileName & " - ROW: " & lineNumber
        End If
      Else
        line = ""
        copyEntry = False
      End If
    Else
      line = ""
    End If
  Wend
  Close FD
  
  On Error Resume Next
  rs.Update
  rsCall.Update
  
  rs.Close
  rsCall.Close
  Exit Sub
  
parseErr:   'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      lineNumber = lineNumber + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, line
        lineNumber = lineNumber + 1
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          '''MsgBox err.Description
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        MsgBox err.description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    MsgBox err.description
    Resume Next
  Else
    MsgBox err.description
    Close FD
  End If
End Sub

Public Sub checkRoutine(fileName As String)
  Dim rs As Recordset
  Dim FD As Long
  Dim line As String, token As String, execStatement As String
  Dim copyEntry As Boolean, isLabel As Boolean
  Dim lineNumber As Long
  
  On Error GoTo parseErr
  
  Set rs = m_Fun.Open_Recordset("SELECT * FROM TMP_checkRoutine")
  lineNumber = 0
  FD = FreeFile
  Open fileName For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(line) = 0 Then
      Line Input #FD, line
      lineNumber = lineNumber + 1
      ''''''''''''''''''''''''''
      'Gestione LABEL
      ''''''''''''''''''''''''''
      isLabel = Mid(line, 8, 1) <> " "
    Else
      'Ho gia' una linea da analizzare (letta dalla getCall, per es.)
      isLabel = False
    End If
    
    If Mid(line, 7, 1) <> "*" Then
      'LINEA VALIDA
      line = RTrim(Mid(line, 8, 65)) 'elimino etichette!
      'token = nextToken(line)
      token = nextToken_new(line) 'MANTIENE GLI APICI!!
      If token = "IF" Then
          Dim codifica As String
        token = nexttoken(line)
        If token = "LNKDB-OPERATION" Then
          '� il nostro uomo!
          While token <> "="
            token = nexttoken(line)
            If Len(token) = 0 Then  'fine linea!
              'forzo l'entrata:
              line = "************"
              While Mid(line, 7, 1) = "*"
                Line Input #FD, line
                lineNumber = lineNumber + 1
                line = RTrim(Mid(line, 8, 65))
              Wend
            End If
          Wend
          codifica = nexttoken(line)
          ''''''''''''
          'TROVATO!
          ''''''''''''
          rs.AddNew
          rs!PGM = fileName
          rs!line = lineNumber
          rs!routine = codifica
        End If
        line = ""
        copyEntry = False
      Else
        line = ""
        copyEntry = False
      End If
    Else
      line = ""
    End If
  Wend
  Close FD
  
  On Error Resume Next
  rs.Update
  rs.Close
  Exit Sub
  
parseErr:   'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      lineNumber = lineNumber + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, line
        lineNumber = lineNumber + 1
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          MsgBox err.description
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        MsgBox err.description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    MsgBox err.description
    Resume Next
  Else
MsgBox err.description
    Close FD
  End If

End Sub

Public Sub checkIncaps_PLI(fileName As String, tableName As String)
  Dim rs As Recordset
  Dim FD As Long
  Dim line As String, token As String, execStatement As String
  Dim copyEntry As Boolean, isLabel As Boolean
  Dim lineNumber As Long
  Dim codifica As String
  Dim ArrCheckIncaps() As StrCheckIncaps, k As Integer
  Dim NomeRout As String
  Dim Ordinale As Long, OrdinaleMolt As Integer
  
  On Error GoTo parseErr
  
  Set rs = m_Fun.Open_Recordset("SELECT * FROM " & tableName)
  'Set rsCall = m_Fun.Open_Recordset("SELECT * FROM TMP_checkNONIncaps")
  lineNumber = 0
  FD = FreeFile
  Ordinale = 1
  OrdinaleMolt = 1
  ReDim ArrCheckIncaps(0)
  Open fileName For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(line) = 0 Then
      Line Input #FD, line
      lineNumber = lineNumber + 1
      If lineNumber = 11661 Then
        lineNumber = 11661
      End If
      RipulisciRiga line
''      ''''''''''''''''''''''''''
''      'Gestione LABEL
''      ''''''''''''''''''''''''''
''      isLabel = Mid(line, 8, 1) <> " "
''    Else
''      'Ho gia' una linea da analizzare (letta dalla getCall, per es.)
''      isLabel = False
    End If
    
''    If Mid(Trim(line, 1, 2)) <> "/*" Then
''      'LINEA VALIDA
''      line = RTrim(Mid(line, 8, 65)) 'elimino etichette!
''      'token = nextToken(line)
      token = nextToken_new(line) 'MANTIENE GLI APICI!!
      If token = "LNKDB_OPERATION" Then
        token = nexttoken(line)
        While token <> "="
          token = nexttoken(line)
          If Len(token) = 0 Then  'fine linea!
            'forzo l'entrata:
            line = "************"
            While Mid(line, 7, 1) = "*"
              Line Input #FD, line
              lineNumber = lineNumber + 1
              RipulisciRiga line
            Wend
          End If
        Wend
        'AC 11/05/10 accumulo su array poi salvo (e azzero array) quando trovo il Nome routine
        ReDim Preserve ArrCheckIncaps(UBound(ArrCheckIncaps) + 1)
        
        'Campo destinazione:
        codifica = nexttoken(line)
        ArrCheckIncaps(UBound(ArrCheckIncaps)).line = lineNumber
        ArrCheckIncaps(UBound(ArrCheckIncaps)).PGM = fileName
        ArrCheckIncaps(UBound(ArrCheckIncaps)).routine = codifica
        ArrCheckIncaps(UBound(ArrCheckIncaps)).Ordinale = Ordinale
        ArrCheckIncaps(UBound(ArrCheckIncaps)).OrdinaleMolt = OrdinaleMolt
        OrdinaleMolt = OrdinaleMolt + 1
'        rs.AddNew
'        rs!pgm = fileName
'        rs!line = lineNumber
'        rs!routine = codifica
       ElseIf token = "FETCH" Then 'cerco nomeroutine che sar� associata alla lista delle codifiche
        token = nexttoken(line) 'XRIORTN
        token = nexttoken(line) 'TITLE
        token = nexttoken(line)
        token = Replace(Replace(token, "('", ""), "')", "")
        For k = 1 To UBound(ArrCheckIncaps)
            rs.AddNew
            rs!PGM = ArrCheckIncaps(k).PGM
            rs!line = ArrCheckIncaps(k).line
            rs!routine = ArrCheckIncaps(k).routine
            rs!Ordinale = ArrCheckIncaps(k).Ordinale
            rs!OrdinaleMolt = ArrCheckIncaps(k).OrdinaleMolt
            rs!NomeRout = token
        Next k
        ReDim ArrCheckIncaps(0)
''      ElseIf token = "CALL" Then
''        ''''''''''''''''''''''''''''''''''
''        ' CHECK CBLTDLI NON INCAPSULATE
''        ''''''''''''''''''''''''''''''''''
''        token = nextToken(line)
''        If Len(token) Then
''          If token = "'CBLTDLI'" Or token = "CBLTDLI" Or _
''             token = "PLITDLI" Or token = "'PLITDLI'" Then
''            rsCall.AddNew
''            rsCall!PGM = fileName
''            rsCall!line = lineNumber
''          End If
''        Else
''          'caso tossico: CALL e routine su righe diverse
''          MsgBox "TMP: CALL su righe diverse! - PGM: " & fileName & " - ROW: " & lineNumber
''        End If
      ElseIf token = "CALL" Then
       token = nexttoken(line) 'XRIORTN
       If token = "XRIORTN" Then
        Ordinale = Ordinale + 1
        OrdinaleMolt = 1
       End If
      Else
        line = ""
        copyEntry = False
      End If
''    Else
''      line = ""
''    End If
  Wend
  Close FD
  
  On Error Resume Next
  rs.Update
  
  
  rs.Close
  
  Exit Sub
  
parseErr:   'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      lineNumber = lineNumber + 1
      RipulisciRiga line
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        If Right(line, 1) = ";" Then
        Else
          'Butto anche via la successiva...
          Line Input #FD, line
          lineNumber = lineNumber + 1
          RipulisciRiga line
          If Mid(line, 7, 1) = "-" Then
            'OK
            token = "VALUE" 'va nell'else: OK...
          Else
            MsgBox err.description
          End If
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        MsgBox err.description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    MsgBox err.description
    Resume Next
  Else
MsgBox err.description
    Close FD
  End If
End Sub
''''''''''''''''''''''''''''''''''''''
' SQ TMP: estrazione move SEGLEVEL
''''''''''''''''''''''''''''''''''''''
Public Sub checkIncapsLEVEL_PLI(fileName As String)
  Dim rs As Recordset, rsCall As Recordset
  Dim FD As Long
  Dim line As String, token As String, execStatement As String
  Dim copyEntry As Boolean, isLabel As Boolean
  Dim lineNumber As Long
  Dim codifica As String
  
  On Error GoTo parseErr
  
  Set rs = m_Fun.Open_Recordset("SELECT * FROM TMP_checkIncaps")
  Set rsCall = m_Fun.Open_Recordset("SELECT * FROM TMP_checkNONIncaps")
  lineNumber = 0
  FD = FreeFile
  Open fileName For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(line) = 0 Then
      Line Input #FD, line
      lineNumber = lineNumber + 1
      RipulisciRiga line
    End If
    
      token = nextToken_new(line) 'MANTIENE GLI APICI!!
      If token = "LNKDB_OPERATION" Then
        token = nexttoken(line)
        While token <> "="
          token = nexttoken(line)
          If Len(token) = 0 Then  'fine linea!
            'forzo l'entrata:
            line = "************"
            While Mid(line, 7, 1) = "*"
              Line Input #FD, line
              lineNumber = lineNumber + 1
              RipulisciRiga line
            Wend
          End If
        Wend
        'Campo destinazione:
        codifica = nexttoken(line)
        Dim level As String
        level = ""
        'SQ LIVELLI
        Do While Not EOF(FD)
          Line Input #FD, line
          lineNumber = lineNumber + 1
          RipulisciRiga line
          token = nexttoken(line)
          If token = "CALL" Then
            'Fine istruzione:
            Exit Do
          ElseIf token = "LNKDB_SEGLEVEL" Then
            'il mio uomo:
            Debug.Print codifica & " -->" & line
            Dim Ordinale As String
            Ordinale = nexttoken(line)
            token = nexttoken(line) '=
            level = nexttoken(line)
            'INSERT
            rs.AddNew
            rs!PGM = fileName
            rs!line = lineNumber
            rs!routine = codifica
            rs!Ordinale = Replace(Replace(Ordinale, ")", ""), "(", "")
            rs!level = level
          End If
        Loop

      Else
        line = ""
        copyEntry = False
      End If
  Wend
  Close FD
  
  On Error Resume Next
  rs.Update
  
  rs.Close
  rsCall.Close
  Exit Sub
  
parseErr:   'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      lineNumber = lineNumber + 1
      RipulisciRiga line
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        If Right(line, 1) = ";" Then
        Else
          'Butto anche via la successiva...
          Line Input #FD, line
          lineNumber = lineNumber + 1
          RipulisciRiga line
          If Mid(line, 7, 1) = "-" Then
            'OK
            token = "VALUE" 'va nell'else: OK...
          Else
            ''''MsgBox err.Description
          End If
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        MsgBox err.description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    MsgBox err.description
    Resume Next
  Else
MsgBox err.description
    Close FD
  End If
End Sub

Public Sub checkRoutine_PLI(fileName As String, tableName As String)
  Dim rs As Recordset
  Dim FD As Long
  Dim line As String, token As String, execStatement As String
  Dim copyEntry As Boolean, isLabel As Boolean
  Dim lineNumber As Long
  
  On Error GoTo parseErr
  
  Set rs = m_Fun.Open_Recordset("SELECT * FROM " & tableName)
  lineNumber = 0
  FD = FreeFile
  Open fileName For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(line) = 0 Then
      Line Input #FD, line
      lineNumber = lineNumber + 1
      RipulisciRiga line
''      ''''''''''''''''''''''''''
''      'Gestione LABEL
''      ''''''''''''''''''''''''''
''      isLabel = Mid(line, 8, 1) <> " "
''    Else
''      'Ho gia' una linea da analizzare (letta dalla getCall, per es.)
''      isLabel = False
    End If
    
''    If Mid(line, 7, 1) <> "*" Then
''      'LINEA VALIDA
''      line = RTrim(Mid(line, 8, 65)) 'elimino etichette!
''      'token = nextToken(line)
      token = nextToken_new(line) 'MANTIENE GLI APICI!!
      If token = "IF" Then
        Dim codifica As String
        token = nexttoken(line)
        If token = "LNKDB_OPERATION" Then
          '� il nostro uomo!
          While token <> "="
            token = nexttoken(line)
            If Len(token) = 0 Then  'fine linea!
              'forzo l'entrata:
              line = "************"
              While Mid(line, 7, 1) = "*"
                Line Input #FD, line
                lineNumber = lineNumber + 1
                line = RTrim(Mid(line, 8, 65))
              Wend
            End If
          Wend
          codifica = nexttoken(line)
          ''''''''''''
          'TROVATO!
          ''''''''''''
          rs.AddNew
          rs!PGM = fileName
          rs!line = lineNumber
          rs!routine = codifica
        End If
        line = ""
        copyEntry = False
      Else
        line = ""
        copyEntry = False
      End If
''    Else
''      line = ""
''    End If
  Wend
  Close FD
  
  On Error Resume Next
  rs.Update
  rs.Close
  Exit Sub
  
parseErr:   'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, line
      GbNumRec = GbNumRec + 1
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, line
        GbNumRec = GbNumRec + 1
        If Mid(line, 7, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          MsgBox err.description
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        MsgBox err.description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    MsgBox err.description
    Resume Next
  Else
    MsgBox err.description
    Close FD
  End If
End Sub

Public Function FormattaData(txtData As String) As String
  Dim txtGiorno As Integer, txtMese As Integer, txtAnno As Integer
  Dim txtOra As Integer, txtMinuti As Integer, txtSecondi As Integer
  
  If txtData <> "" Then
    txtGiorno = Day(txtData)
    txtMese = Month(txtData)
    txtAnno = Year(txtData)
    
    txtOra = Hour(txtData)
    txtMinuti = Minute(txtData)
    txtSecondi = Second(txtData)
    
    If txtAnno Then
      FormattaData = format(txtAnno, "0000") & "/" & format(txtMese, "00") & "/" & format(txtGiorno, "00")
      'If txtOra Then
        FormattaData = FormattaData & Space(1) & format(txtOra, "00") & "." & format(txtMinuti, "00") & "." & format(txtSecondi, "00")
      'End If
    End If
  Else
    FormattaData = ""
  End If
End Function

''Public Sub Ricerca_Hostvar()
''  Dim rsExec As Recordset, rsHostVar As Recordset
''  Dim txtInstr As String, txtHostVar As String
''  Dim StartPos As Long, i As Long, char As String
''
''  Parser.PsConnection.Execute "Delete * from PS_SQL_HostVar"
''
''  Set rsExec = m_Fun.Open_Recordset("select * from PsExec where " & _
''                                    "area = 'SQL' and comando not in ('INCLUDE','ROLLBACK','COMMIT','BEGIN','END')" & _
''                                    "order by idoggetto, riga, comando")
''  Do Until rsExec.EOF
''    If InStr(rsExec!istruzione, ":") Then
''      txtInstr = rsExec!istruzione
''      Do Until InStr(txtInstr, ":") = 0
''        StartPos = InStr(txtInstr, ":")
''        txtHostVar = ""
''        For i = StartPos To Len(txtInstr)
''          char = Mid(txtInstr, i, 1)
''          If txtHostVar = ":" And char = " " Then
''          Else
''            If char = " " Or char = "=" Or _
''               char = "(" Or char = ")" Or _
''               char = "," Or char = "|" Or _
''               char = "+" Or char = "*" Or _
''               char = "<" Or char = ">" Or _
''               char = "/" Or Mid(txtInstr, i, 2) = vbCrLf Or _
''               (char = ":" And txtHostVar <> "") Then
''              Exit For
''            End If
''            txtHostVar = txtHostVar & char
''          End If
''        Next i
''        ' Controlli di qualit� sulle Host Variable
''        If InStr(txtHostVar, "_") = 0 And InStr(txtHostVar, "'") = 0 Then
''          Set rsHostVar = m_Fun.Open_Recordset("select * from PS_SQL_HostVar where " & _
''                                               "idoggetto = " & rsExec!idOggetto & " and riga = " & rsExec!Riga & _
''                                               " and hostvar = '" & txtHostVar & "'")
''          If rsHostVar.EOF Then
''            rsHostVar.AddNew
''            rsHostVar!idOggetto = rsExec!idOggetto
''            rsHostVar!Riga = rsExec!Riga
''            rsHostVar!HostVar = txtHostVar
''            rsHostVar.Update
''          End If
''          rsHostVar.Close
''        End If
''        txtInstr = Mid(txtInstr, StartPos + 1)
''      Loop
''    End If
''    rsExec.MoveNext
''  Loop
''  rsExec.Close
''End Sub
''
''Public Function Update_Cross_Campi_HostVar(idObj As Long, ByRef txtEliminate As String) As Long
''  Dim rsExec As Recordset
''  Dim HIdOggetto As Long, HIdArea As Integer, HOrdinale As Integer
''  Dim HTipo As String, HUsage As String, HByte As Integer
''  Dim CTipo As String, CLunghezza As Integer
''  Dim Eliminate As Integer, intWHERE As Integer
''  Dim HostVar As String
''
''  Parser.PsConnection.Execute "Delete * from CROSS_Colonne_HostVar where idoggetto = " & idObj
''  Eliminate = 0
''
''  Set rsExec = m_Fun.Open_Recordset("select * from PsExec where " & _
''                                    "area = 'SQL' and comando in('DECLARE','DELETE','UPDATE','SELECT') " & _
''                                    "and idoggetto = " & idObj & " order by comando, riga")
''  Do Until rsExec.EOF
''    intWHERE = InStr(rsExec!istruzione & " WHERE ", " WHERE ")
''    If InStr(Mid(rsExec!istruzione, intWHERE), "SELECT ") = 0 Then
''      ' Le OUTER JOIN, le INNER JOIN e le UNION non devo gestirle
''      If rsExec!comando = "DECLARE" Or rsExec!comando = "SELECT" Then
''        If InStr(rsExec!istruzione, " JOIN ") = 0 And _
''           InStr(rsExec!istruzione, " UNION ") = 0 Then
''          AnalizzaIstruzione rsExec
''        Else
''          txtEliminate = txtEliminate & _
''                         Space(7 - Len(format(idObj, ""))) & format(idObj, "") & "   | " & _
''                         Space(5 - Len(format(rsExec!Riga, ""))) & format(rsExec!Riga, "") & "   | " & _
''                         Space(8 - Len(format(rsExec!comando, ""))) & format(rsExec!comando, "") & "   | " & _
''                         " JOIN" & vbCrLf
''          Eliminate = Eliminate + 1
''        End If
''      ElseIf InStr(rsExec!istruzione, " CURRENT OF ") = 0 Then
''        AnalizzaIstruzione rsExec
''      Else
''        txtEliminate = txtEliminate & _
''                       Space(7 - Len(format(idObj, ""))) & format(idObj, "") & "   | " & _
''                       Space(5 - Len(format(rsExec!Riga, ""))) & format(rsExec!Riga, "") & "   | " & _
''                       Space(8 - Len(format(rsExec!comando, ""))) & format(rsExec!comando, "") & "   | " & _
''                       " CURRENT OF" & vbCrLf
''        Eliminate = Eliminate + 1
''      End If
''    Else
''      txtEliminate = txtEliminate & _
''                     Space(7 - Len(format(idObj, ""))) & format(idObj, "") & "   | " & _
''                     Space(5 - Len(format(rsExec!Riga, ""))) & format(rsExec!Riga, "") & "   | " & _
''                     Space(8 - Len(format(rsExec!comando, ""))) & format(rsExec!comando, "") & "   | " & _
''                     " SELECT dopo WHERE" & vbCrLf
''      Eliminate = Eliminate + 1
''    End If
''    rsExec.MoveNext
''  Loop
''  rsExec.Close
''
''  ' FETCH
''  Set rsExec = m_Fun.Open_Recordset("select * from PsExec where " & _
''                                    "area = 'SQL' and comando = 'FETCH' and idoggetto = " & idObj & " order by idoggetto, riga")
''  Do Until rsExec.EOF
''    AnalizzaIstruzione rsExec
''    rsExec.MoveNext
''  Loop
''  rsExec.Close
''
''  ' Dettagli HostVar su PsData_Cmp
''  Set rsExec = m_Fun.Open_Recordset("select idoggetto, hostvar, h_idoggetto, h_idarea, h_ordinale, h_tipo, h_usage, h_byte from CROSS_Colonne_Hostvar " & _
''                                    "where hostvar <> '' and idoggetto = " & idObj & " order by idoggetto, riga")
''  Do Until rsExec.EOF
''    If Left(rsExec!HostVar, 1) = "(" Then
''      HostVar = Trim(Replace(Replace(rsExec!HostVar, ")", ""), "(", ""))
''    Else
''      HostVar = rsExec!HostVar
''    End If
''    If Left(HostVar, 1) = ":" Then
''      HIdOggetto = 0
''      HIdArea = 0
''      HOrdinale = 0
''      HTipo = ""
''      HUsage = ""
''      HByte = 0
''      TrovaCmp idObj, Trim(Mid(HostVar, 2)), HTipo, HUsage, HByte, HIdOggetto, HIdArea, HOrdinale
''      rsExec!H_idoggetto = HIdOggetto
''      rsExec!H_idarea = HIdArea
''      rsExec!H_ordinale = HOrdinale
''      rsExec!H_Tipo = HTipo
''      rsExec!H_Usage = HUsage
''      rsExec!H_Byte = HByte
''      rsExec.Update
''    End If
''    rsExec.MoveNext
''  Loop
''  rsExec.Close
''
''  ' Dettagli Colonne su DMDB2_Tabelle/DMDB2_Colonne
''  Set rsExec = m_Fun.Open_Recordset("select tabellanome, Colonna, c_tipo, c_lunghezza from CROSS_Colonne_Hostvar " & _
''                                    "where colonna <> '' and idoggetto = " & idObj & " order by idoggetto, riga")
''  Do Until rsExec.EOF
''    CTipo = ""
''    CLunghezza = 0
''    If InStr(rsExec!Colonna, "'") = 0 Then
''      TrovaColonna rsExec!Tabellanome, rsExec!Colonna, CTipo, CLunghezza
''    End If
''    rsExec!c_Tipo = CTipo
''    rsExec!c_lunghezza = CLunghezza
''    rsExec.Update
''    rsExec.MoveNext
''  Loop
''  rsExec.Close
''
''  Update_Cross_Campi_HostVar = Eliminate
''End Function
''
''Public Sub AnalizzaIstruzione(rsExec As Recordset)
''  Dim rsDeclare As Recordset
''  Dim NomeCursore As String, NomeTabella As String, istruzione As String
''  Dim CampoSelect As String, CampiSelect As String, CampiFetch() As String, CampiSET() As String
''  Dim intFOR_SELECT As Integer, intFROM As Integer, intWHERE As Integer, intINTO As Integer, intSET As Integer
''  Dim intUguale  As Integer, CampoSET1 As String, CampoSET2 As String
''  Dim i As Integer
''
''  On Error GoTo errorHandler
''  istruzione = rsExec!istruzione
''  Select Case rsExec!comando
''    Case "DECLARE"
''      intFOR_SELECT = 0
''      intFROM = 0
''      intWHERE = 0
''      NomeCursore = nexttoken(istruzione)
''      intFOR_SELECT = InStr(istruzione & " FOR SELECT ", " FOR SELECT ")
''      istruzione = Trim(Mid(istruzione, intFOR_SELECT + 12))
''      If Len(istruzione) = 0 Then Exit Sub ' � la DECLARE di una tabella e non di un cursore
''      intFROM = InStr(istruzione, " FROM ")
''      intWHERE = InStr(istruzione & " WHERE ", " WHERE ")
''      If intWHERE > Len(istruzione) Then
''        NomeTabella = nexttoken(Mid(istruzione, intFROM + 6, intWHERE - intFROM - 6))
''      Else
''        NomeTabella = Trim(Mid(istruzione, intFROM + 6, intWHERE - intFROM - 6))
''      End If
''      CampiSelect = Left(istruzione, intFROM)
''      i = 0
''      Do Until CampiSelect = ""
''        Do Until CampiSelect = ""
''          If Right(CampoSelect, 1) = "," Then
''            CampoSelect = Left(CampoSelect, Len(CampoSelect) - 1)
''            Exit Do
''          ElseIf Left(CampiSelect, 1) = "," Then
''            CampiSelect = Mid(CampiSelect, 2)
''            Exit Do
''          End If
''          CampoSelect = CampoSelect & " " & nextToken_new(CampiSelect)
''          CampiSelect = Trim(CampiSelect)
''          If Left(CampiSelect, 1) = "(" Then
''            CampoSelect = CampoSelect & nextToken_new(CampiSelect)
''            CampiSelect = Trim(CampiSelect)
''          End If
''        Loop
''        CampoSelect = Trim(CampoSelect)
''        If Len(CampoSelect) Then
''          CampoSelect = Replace(CampoSelect, "'", "''")
''          i = i + 1
''          Parser.PsConnection.Execute "INSERT into CROSS_Colonne_HostVar " & _
''                                      "VALUES(" & rsExec!idOggetto & "," & rsExec!Riga & ",'" & rsExec!comando & "','SELECT'," & i & _
''                                      ",'" & NomeCursore & "','" & NomeTabella & _
''                                      "','" & CampoSelect & "','',0,'','','',0,0,0,'','',0,False)"
''          CampoSelect = ""
''        End If
''      Loop
''      getWHERE Trim(Mid(istruzione, intWHERE + 7)), rsExec!idOggetto, rsExec!Riga, rsExec!comando, NomeCursore, NomeTabella
''    Case "FETCH"
''      NomeCursore = nexttoken(istruzione)
''      Set rsDeclare = m_Fun.Open_Recordset("select * from CROSS_Colonne_HostVar where " & _
''                                           "idoggetto = " & rsExec!idOggetto & " and cursorenome = '" & NomeCursore & "' and " & _
''                                           "area = 'SELECT' order by ordinale")
''      If rsDeclare.RecordCount Then
''        intINTO = 0
''        intINTO = InStr(istruzione, " INTO ")
''        If intINTO = 0 Then
''          intINTO = InStr(istruzione, " INTO")
''        End If
''        CampiFetch = Split(Trim(Mid(istruzione, intINTO + 6)), ",")
''        If UBound(CampiFetch) = 0 Then
''          If rsDeclare.RecordCount = 1 Then
''            If InStrRev(Trim(CampiFetch(0)), ":") > 1 Then
''              rsDeclare!HostVar = Trim(Left(CampiFetch(0), InStrRev(CampiFetch(0), ":") - 1))
''              rsDeclare!H_IndNull = Trim(Mid(CampiFetch(0), InStrRev(CampiFetch(0), ":")))
''            Else
''              rsDeclare!HostVar = Trim(CampiFetch(0))
''              rsDeclare!H_IndNull = ""
''            End If
''            rsDeclare.Update
''          Else
''            Do Until rsDeclare.EOF
''              rsDeclare!HostVar = "!!!LIVELLO 01 '" & Trim(CampiFetch(0)) & "' DA ESPANDERE!!!"
''              rsDeclare.Update
''              rsDeclare.MoveNext
''            Loop
''          End If
''        Else
''          Do Until rsDeclare.EOF
''            If rsDeclare!Ordinale - 1 > UBound(CampiFetch) Then
''              rsDeclare!HostVar = "!!!DA VERIFICARE!!!"
''              rsDeclare!H_IndNull = ""
''            Else
''              If InStrRev(Trim(CampiFetch(rsDeclare!Ordinale - 1)), ":") > 1 Then
''              rsDeclare!HostVar = Trim(Left(CampiFetch(rsDeclare!Ordinale - 1), InStrRev(CampiFetch(rsDeclare!Ordinale - 1), ":") - 1))
''              rsDeclare!H_IndNull = Trim(Mid(CampiFetch(rsDeclare!Ordinale - 1), InStrRev(CampiFetch(rsDeclare!Ordinale - 1), ":")))
''              Else
''                rsDeclare!HostVar = Trim(CampiFetch(rsDeclare!Ordinale - 1))
''                rsDeclare!H_IndNull = ""
''              End If
''            End If
''            rsDeclare.Update
''            rsDeclare.MoveNext
''          Loop
''        End If
''      End If
''      rsDeclare.Close
''    Case "DELETE"
''      intWHERE = 0
''      intWHERE = InStr(istruzione & " WHERE ", " WHERE ")
''      If intWHERE > Len(istruzione) Then
''        NomeTabella = nexttoken(Mid(istruzione, 6, intWHERE - 6))
''      Else
''        NomeTabella = Trim(Mid(istruzione, 6, intWHERE - 6))
''      End If
''      getWHERE Trim(Mid(istruzione, intWHERE + 7)), rsExec!idOggetto, rsExec!Riga, rsExec!comando, NomeCursore, NomeTabella
''    Case "UPDATE"
''      intWHERE = 0
''      intSET = 0
''      If InStr(istruzione, "SELECT ") Then Exit Sub
''      intWHERE = InStr(istruzione & " WHERE ", " WHERE ")
''      intSET = InStr(istruzione & " SET ", " SET ")
''      NomeTabella = Trim(Left(istruzione, intSET))
''      CampiSET() = Split(Trim(Mid(istruzione, intSET + 5, intWHERE - intSET - 5)), ",")
''      For i = 0 To UBound(CampiSET)
''        intUguale = InStr(CampiSET(i), "=")
''        If intUguale > 1 Then
''          CampoSET1 = Trim(Left(CampiSET(i), intUguale - 1))
''          CampoSET2 = Trim(Mid(CampiSET(i), intUguale + 1))
''          If InStr(CampoSET2, "'") Then
''            CampoSET2 = Replace(CampoSET2, "'", "''")
''          End If
''          Parser.PsConnection.Execute "INSERT into CROSS_Colonne_HostVar " & _
''                                      "VALUES(" & rsExec!idOggetto & "," & rsExec!Riga & ",'" & rsExec!comando & "','SET'," & i + 1 & _
''                                      ",'" & NomeCursore & "','" & NomeTabella & _
''                                      "','" & CampoSET1 & "','',0,'=','" & CampoSET2 & "','',0,0,0,'','',0,False)"
''        End If
''      Next i
''      getWHERE Trim(Mid(istruzione, intWHERE + 7)), rsExec!idOggetto, rsExec!Riga, rsExec!comando, NomeCursore, NomeTabella
''    Case "SELECT"
''      intINTO = InStr(istruzione, " INTO ")
''      If intINTO = 0 Then
''        intINTO = InStr(istruzione, " INTO")
''      End If
''      intFROM = InStr(istruzione & " FROM ", " FROM ")
''      intWHERE = InStr(istruzione & " WHERE ", " WHERE ")
''      If intWHERE > Len(istruzione) Then
''        NomeTabella = nexttoken(Mid(istruzione, intFROM + 6, intWHERE - intFROM - 6))
''      Else
''        NomeTabella = Trim(Mid(istruzione, intFROM + 6, intWHERE - intFROM - 6))
''      End If
''      If intINTO = 0 Then
''        CampiSelect = Trim(Left(istruzione, intFROM - 1))
''      Else
''        CampiSelect = Trim(Left(istruzione, intINTO - 1))
''      End If
''      i = 0
''      Do Until CampiSelect = ""
''        Do Until CampiSelect = ""
''          If Right(CampoSelect, 1) = "," Then
''            CampoSelect = Left(CampoSelect, Len(CampoSelect) - 1)
''            Exit Do
''          ElseIf Left(CampiSelect, 1) = "," Then
''            CampiSelect = Mid(CampiSelect, 2)
''            Exit Do
''          End If
''          CampoSelect = CampoSelect & " " & nextToken_new(CampiSelect)
''          CampiSelect = Trim(CampiSelect)
''          If Left(CampiSelect, 1) = "(" Then
''            CampoSelect = CampoSelect & nextToken_new(CampiSelect)
''            CampiSelect = Trim(CampiSelect)
''          End If
''        Loop
''        CampoSelect = Trim(CampoSelect)
''        If Len(CampoSelect) Then
''          CampoSelect = Replace(CampoSelect, "'", "''")
''          i = i + 1
''          Parser.PsConnection.Execute "INSERT into CROSS_Colonne_HostVar " & _
''                                      "VALUES(" & rsExec!idOggetto & "," & rsExec!Riga & ",'" & rsExec!comando & "','SELECT'," & i & _
''                                      ",'" & NomeCursore & "','" & NomeTabella & _
''                                      "','" & CampoSelect & "','',0,'','','',0,0,0,'','',0,False)"
''          CampoSelect = ""
''        End If
''      Loop
''      Set rsDeclare = m_Fun.Open_Recordset("select * from CROSS_Colonne_HostVar where " & _
''                                           "idoggetto = " & rsExec!idOggetto & " and riga = " & rsExec!Riga & " and " & _
''                                           "comando = 'SELECT' and area = 'SELECT' order by ordinale")
''      If rsDeclare.RecordCount Then
''        CampiFetch = Split(Trim(Mid(istruzione, intINTO + 6, intFROM - intINTO - 6)), ",")
''        If UBound(CampiFetch) = 0 Then
''          If rsDeclare.RecordCount = 1 Then
''            If InStrRev(Trim(CampiFetch(0)), ":") > 1 Then
''              rsDeclare!HostVar = Trim(Left(CampiFetch(0), InStrRev(CampiFetch(0), ":") - 1))
''              rsDeclare!H_IndNull = Trim(Mid(CampiFetch(0), InStrRev(CampiFetch(0), ":")))
''            Else
''              rsDeclare!HostVar = Trim(CampiFetch(0))
''              rsDeclare!H_IndNull = ""
''            End If
''            rsDeclare.Update
''          Else
''            Do Until rsDeclare.EOF
''              rsDeclare!HostVar = "!!!LIVELLO 01 '" & Trim(CampiFetch(0)) & "' DA ESPANDERE!!!"
''              rsDeclare!H_IndNull = ""
''              rsDeclare.Update
''              rsDeclare.MoveNext
''            Loop
''          End If
''        Else
''          Do Until rsDeclare.EOF
''            If rsDeclare!Ordinale - 1 > UBound(CampiFetch) Then
''              rsDeclare!HostVar = "!!!DA VERIFICARE!!!"
''              rsDeclare!H_IndNull = ""
''            Else
''              If InStrRev(Trim(CampiFetch(rsDeclare!Ordinale - 1)), ":") > 1 Then
''                rsDeclare!HostVar = Trim(Left(CampiFetch(rsDeclare!Ordinale - 1), InStrRev(CampiFetch(rsDeclare!Ordinale - 1), ":") - 1))
''                rsDeclare!H_IndNull = Trim(Mid(CampiFetch(rsDeclare!Ordinale - 1), InStrRev(CampiFetch(rsDeclare!Ordinale - 1), ":")))
''              Else
''                rsDeclare!HostVar = Trim(CampiFetch(rsDeclare!Ordinale - 1))
''                rsDeclare!H_IndNull = ""
''              End If
''            End If
''            rsDeclare.Update
''            rsDeclare.MoveNext
''          Loop
''        End If
''      End If
''      rsDeclare.Close
''      getWHERE Trim(Mid(istruzione, intWHERE + 7)), rsExec!idOggetto, rsExec!Riga, rsExec!comando, NomeCursore, NomeTabella
''    Case Else
''  End Select
''  Exit Sub
''errorHandler:
''  isError = True
''  If err.Number = -2147217833 Then
''    m_Fun.writeLog "idObject: " & Space(5 - Len(format(rsExec!idOggetto, ""))) & format(rsExec!idOggetto, "") & " - " & err.Number & " - " & err.Description, "DECLARE ANALISYS", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\ANALISYS_DECLARE.LOG"
''  Else
''    m_Fun.writeLog "idObject: " & Space(5 - Len(format(rsExec!idOggetto, ""))) & format(rsExec!idOggetto, "") & " - " & err.Number & " - " & err.Description, "DECLARE ANALISYS", m_Fun.FnPathPrj & "\DOCUMENTS\LOG\ANALISYS_DECLARE.LOG"
''  End If
''End Sub
''
''Public Sub TrovaCmp(idOggetto As Long, HostVar As String, ByRef HTipo As String, ByRef HUsage As String, ByRef HByte As Integer, ByRef HIdOggetto As Long, ByRef HIdArea As Integer, ByRef HOrdinale As Integer)
''  Dim rsCmp As Recordset, rsObj As Recordset, rsArea As Recordset
''  Dim rsRel As Recordset, rsRel2 As Recordset
''  Dim Oggetti() As Long, i As Integer
''  Dim areaHostVar As String
''
''  ReDim Oggetti(1)
''  Oggetti(1) = idOggetto
''  Set rsObj = m_Fun.Open_Recordset("select tipo from bs_oggetti where idoggetto = " & idOggetto)
''  If Not rsObj.EOF Then
''    If rsObj!Tipo = "CPY" Or rsObj!Tipo = "INC" Then
''      ReDim Oggetti(0)
''      Set rsRel = m_Fun.Open_Recordset("select idoggettoC from PsRel_Obj where IdoggettoR = " & idOggetto)
''      Do Until rsRel.EOF
''        ReDim Preserve Oggetti(UBound(Oggetti) + 1)
''        Oggetti(UBound(Oggetti)) = rsRel!IdOggettoC
''        rsRel.MoveNext
''      Loop
''      rsRel.Close
''    End If
''  End If
''  rsObj.Close
''
''  If InStr(HostVar, ".") Then
''    ' Se c'� il punto la HostVar � Area.Variabile
''    areaHostVar = Left(HostVar, InStr(HostVar, ".") - 1)
''    HostVar = Mid(HostVar, InStr(HostVar, ".") + 1)
''    For i = 1 To UBound(Oggetti)
''      Set rsArea = m_Fun.Open_Recordset("select idoggetto, idarea, ordinale, tipo, [usage], [byte] from PsData_Cmp where " & _
''                                       "idoggetto = " & Oggetti(i) & " and Nome = '" & areaHostVar & "'")
''      If rsArea.RecordCount Then
''        HIdOggetto = rsArea!idOggetto
''        HIdArea = rsArea!idArea
''        Set rsCmp = m_Fun.Open_Recordset("select idoggetto, idarea, ordinale, tipo, [usage], [byte] from PsData_Cmp where " & _
''                                         "idoggetto = " & HIdOggetto & " and idarea = " & HIdArea & " and Nome = '" & HostVar & "'")
''        If rsCmp.RecordCount Then
''          HOrdinale = rsCmp!Ordinale
''          HTipo = rsCmp!Tipo
''          HUsage = rsCmp!Usage
''          HByte = rsCmp!byte
''          rsCmp.Close
''        Else
''          rsCmp.Close
''          Set rsRel = m_Fun.Open_Recordset("select idoggettoR from PsRel_Obj where IdoggettoC = " & Oggetti(i))
''          Do Until rsRel.EOF
''            Set rsCmp = m_Fun.Open_Recordset("select idoggetto, idarea, ordinale, tipo, [usage], [byte] from PsData_Cmp where " & _
''                                             "idoggetto = " & rsRel!idOggettoR & " and Nome = '" & HostVar & "'")
''            If rsCmp.RecordCount Then
''              HIdOggetto = rsCmp!idOggetto
''              HIdArea = rsCmp!idArea
''              HOrdinale = rsCmp!Ordinale
''              HTipo = rsCmp!Tipo
''              HUsage = rsCmp!Usage
''              HByte = rsCmp!byte
''            End If
''            rsCmp.Close
''            If HIdOggetto <> 0 And HIdArea <> 0 And HOrdinale <> 0 Then Exit Do ' Se trovo il campo esco
''            rsRel.MoveNext
''          Loop
''          rsRel.Close
''        End If
''        rsArea.Close
''      Else
''        rsArea.Close
''        Set rsRel = m_Fun.Open_Recordset("select idoggettoR from PsRel_Obj where IdoggettoC = " & Oggetti(i))
''        Do Until rsRel.EOF
''          Set rsArea = m_Fun.Open_Recordset("select idoggetto, idarea, ordinale, tipo, [usage], [byte] from PsData_Cmp where " & _
''                                            "idoggetto = " & rsRel!idOggettoR & " and Nome = '" & areaHostVar & "'")
''          If rsArea.RecordCount Then
''            HIdOggetto = rsArea!idOggetto
''            HIdArea = rsArea!idArea
''            Set rsCmp = m_Fun.Open_Recordset("select idoggetto, idarea, ordinale, tipo, [usage], [byte] from PsData_Cmp where " & _
''                                             "idoggetto = " & HIdOggetto & " and idarea = " & HIdArea & " and Nome = '" & HostVar & "'")
''            If rsCmp.RecordCount Then
''              HOrdinale = rsCmp!Ordinale
''              HTipo = rsCmp!Tipo
''              HUsage = rsCmp!Usage
''              HByte = rsCmp!byte
''              rsCmp.Close
''            Else
''              rsCmp.Close
''              Set rsRel2 = m_Fun.Open_Recordset("select idoggettoR from PsRel_Obj where IdoggettoC = " & Oggetti(i))
''              Do Until rsRel2.EOF
''                Set rsCmp = m_Fun.Open_Recordset("select idoggetto, idarea, ordinale, tipo, [usage], [byte] from PsData_Cmp where " & _
''                                                 "idoggetto = " & rsRel2!idOggettoR & " and Nome = '" & HostVar & "'")
''                If rsCmp.RecordCount Then
''                  HIdOggetto = rsCmp!idOggetto
''                  HIdArea = rsCmp!idArea
''                  HOrdinale = rsCmp!Ordinale
''                  HTipo = rsCmp!Tipo
''                  HUsage = rsCmp!Usage
''                  HByte = rsCmp!byte
''                End If
''                rsCmp.Close
''                If HIdOggetto <> 0 And HIdArea <> 0 And HOrdinale <> 0 Then Exit Do ' Se trovo il campo esco
''                rsRel2.MoveNext
''              Loop
''              rsRel2.Close
''            End If
''          End If
''          rsArea.Close
''          If HIdOggetto <> 0 And HIdArea <> 0 And HOrdinale <> 0 Then Exit Do ' Se trovo il campo esco
''          rsRel.MoveNext
''        Loop
''        rsRel.Close
''      End If
''    Next i
''  Else
''    ' Altrimenti � solo Variabile
''    For i = 1 To UBound(Oggetti)
''      Set rsCmp = m_Fun.Open_Recordset("select idoggetto, idarea, ordinale, tipo, [usage], [byte] from PsData_Cmp where " & _
''                                       "idoggetto = " & Oggetti(i) & " and Nome = '" & HostVar & "'")
''      If rsCmp.RecordCount Then
''        HIdOggetto = rsCmp!idOggetto
''        HIdArea = rsCmp!idArea
''        HOrdinale = rsCmp!Ordinale
''        HTipo = rsCmp!Tipo
''        HUsage = rsCmp!Usage
''        HByte = rsCmp!byte
''        rsCmp.Close
''      Else
''        rsCmp.Close
''        Set rsRel = m_Fun.Open_Recordset("select idoggettoR from PsRel_Obj where IdoggettoC = " & Oggetti(i))
''        Do Until rsRel.EOF
''          Set rsCmp = m_Fun.Open_Recordset("select idoggetto, idarea, ordinale, tipo, [usage], [byte] from PsData_Cmp where " & _
''                                           "idoggetto = " & rsRel!idOggettoR & " and Nome = '" & HostVar & "'")
''          If rsCmp.RecordCount Then
''            HIdOggetto = rsCmp!idOggetto
''            HIdArea = rsCmp!idArea
''            HOrdinale = rsCmp!Ordinale
''            HTipo = rsCmp!Tipo
''            HUsage = rsCmp!Usage
''            HByte = rsCmp!byte
''          End If
''          rsCmp.Close
''          If HIdOggetto <> 0 And HIdArea <> 0 And HOrdinale <> 0 Then Exit Do ' Se trovo il campo esco
''          rsRel.MoveNext
''        Loop
''        rsRel.Close
''      End If
''    Next i
''  End If
''End Sub

Public Sub TrovaColonna(Tabella As String, Colonna As String, ByRef CTipo As String, ByRef CLunghezza As Integer)
  Dim rscol As Recordset
  Dim Tabelle() As String, AliasColonna As String
  Dim i As Integer

  If InStr(Tabella, " ") Then
    If InStr(Colonna, ".") Then
      AliasColonna = Left(Colonna, InStr(Colonna, ".") - 1)
      Colonna = Mid(Colonna, InStr(Colonna, ".") + 1)
    End If

    Tabelle = Split(Tabella, ",")
    For i = 0 To UBound(Tabelle)
      If AliasColonna = Mid(Trim(Tabelle(i)), InStrRev(Trim(Tabelle(i)), " ") + 1) Then
        Tabella = Left(Trim(Tabelle(i)), InStr(Trim(Tabelle(i)), " ") - 1)
        Exit For
      End If
    Next i
  End If
  Set rscol = m_Fun.Open_Recordset("select a.tipo, a.lunghezza from DMDB2_Colonne as a, DMDB2_Tabelle as b where " & _
                                   "a.idtable = b.idtable and b.Nome = '" & Tabella & "' and a.Nome = '" & Colonna & "'")
  If rscol.RecordCount Then
    CTipo = rscol!Tipo
    CLunghezza = rscol!Lunghezza
  Else
    rscol.Close
    Set rscol = m_Fun.Open_Recordset("select a.tipo, a.lunghezza from DMDB2_Colonne as a, DMDB2_Alias as b where " & _
                                     "a.idtable = b.idtable and b.Nome = '" & Tabella & "' and a.Nome = '" & Colonna & "'")
    If rscol.RecordCount Then
      CTipo = rscol!Tipo
      CLunghezza = rscol!Lunghezza
    End If
  End If
  rscol.Close
End Sub
''
''Public Sub getWHERE(istruzione As String, idOggetto As Long, Riga As Integer, comando As String, NomeCursore As String, NomeTabella As String)
''  Dim CampoWhere1 As String, CampoWhere2 As String, CampoWhereOld As String, Oper As String
''  Dim CampiIN() As String
''  Dim isBetween As Boolean
''  Dim a As Integer, j As Integer
''
''  '''''''''''''' WHERE CONDITIONS
''  a = 1
''  Do While Left(istruzione, 1) = ")"
''    istruzione = Trim(Mid(istruzione, 2))
''  Loop
''  Do While Left(istruzione, 1) = "("
''    istruzione = Trim(Mid(istruzione, 2))
''  Loop
''  CampoWhere1 = nexttoken(istruzione)
''  Do Until CampoWhere1 = "FOR" Or CampoWhere1 = "GROUPBY" Or _
''     CampoWhere1 = "ORDER" Or CampoWhere1 = "HAVING" Or _
''     CampoWhere1 = "FETCH" Or CampoWhere1 = "ROWS" Or _
''     CampoWhere1 = "WITH" Or CampoWhere1 = "OPTIMIZE" Or _
''     CampoWhere1 = "QUERYNO" Or CampoWhere1 = "GROUP" Or _
''     istruzione = ""
''    If CampoWhere1 = "OR" Or _
''       CampoWhere1 = "AND" Or _
''       CampoWhere1 = "NOT" Or _
''       CampoWhere1 = ")" Or _
''       CampoWhere1 = "(" Then
''      If isBetween Then
''        isBetween = False
''        CampoWhere2 = nextToken_new(istruzione)
''        If CampoWhere2 = ":" Then
''          CampoWhere2 = CampoWhere2 & nexttoken(istruzione)
''        ElseIf CampoWhere2 = "CURRENT" Then ' CURRENT DATE
''          CampoWhere2 = CampoWhere2 & " " & nexttoken(istruzione)
''        ElseIf Left(CampoWhere2, 1) = "'" Then
''          CampoWhere2 = Replace(CampoWhere2, "'", "''")
''        ElseIf Right(CampoWhere2, 1) = ")" And InStr(CampoWhere2, "(") = 0 Then
''          CampoWhere2 = Left(CampoWhere2, Len(CampoWhere2) - 1)
''        End If
''        Parser.PsConnection.Execute "INSERT into CROSS_Colonne_HostVar " & _
''                                    "VALUES(" & idOggetto & "," & Riga & ",'" & comando & "','WHERE'," & a & _
''                                    ",'" & NomeCursore & "','" & NomeTabella & _
''                                    "','" & CampoWhereOld & "','',0,'" & Oper & "','" & CampoWhere2 & "','',0,0,0,'','',0,False)"
''        a = a + 1
''        CampoWhereOld = ""
''      End If
''    Else
''      If CampoWhere1 = "CURRENT" Then ' CURRENT DATE
''        CampoWhere1 = CampoWhere1 & " " & nexttoken(istruzione)
''      End If
''      Oper = nextToken_new(istruzione)
''      If Left(Oper, 1) = "(" Then
''        CampoWhere1 = CampoWhere1 & Oper
''        Oper = nexttoken(istruzione)
''      End If
''      If Oper = "-" Or Oper = "+" Or Oper = "*" Or Oper = "/" Then
''        CampoWhere1 = CampoWhere1 & " " & Oper
''        Oper = nexttoken(istruzione)
''        Do Until Left(istruzione, 1) <> "("
''          CampoWhere1 = CampoWhere1 & " " & Oper
''          Oper = nexttoken(istruzione)
''        Loop
''        CampoWhere1 = CampoWhere1 & Oper
''        istruzione = Trim(istruzione)
''        Do While Left(istruzione, 1) = ")"
''          istruzione = Trim(Mid(istruzione, 2))
''        Loop
''        Do While Left(istruzione, 1) = "("
''          istruzione = Trim(Mid(istruzione, 2))
''        Loop
''        Oper = nexttoken(istruzione)
''      End If
''      If Oper = "IN" Then
''        CampoWhere2 = nextToken_new(istruzione)
''        CampiIN = Split(Replace(Replace(CampoWhere2, "(", ""), ")", ""), ",")
''        For j = 0 To UBound(CampiIN)
''          If Left(Trim(CampiIN(j)), 1) = "'" Then
''            CampiIN(j) = Replace(CampiIN(j), "'", "''")
''          End If
''          Parser.PsConnection.Execute "INSERT into CROSS_Colonne_HostVar " & _
''                                      "VALUES(" & idOggetto & "," & Riga & ",'" & comando & "','WHERE'," & a & _
''                                      ",'" & NomeCursore & "','" & NomeTabella & _
''                                      "','" & CampoWhere1 & "','',0,'" & Oper & "','" & Trim(CampiIN(j)) & "','',0,0,0,'','',0,False)"
''          a = a + 1
''        Next j
''      ElseIf Oper = "NOT" Then ' NOT IN, NOT LIKE
''        Oper = Oper & " " & nexttoken(istruzione)
''        CampoWhere2 = nextToken_new(istruzione)
''        CampiIN = Split(Replace(Replace(CampoWhere2, "(", ""), ")", ""), ",")
''        For j = 0 To UBound(CampiIN)
''          If Left(Trim(CampiIN(j)), 1) = "'" Then
''            CampiIN(j) = Replace(CampiIN(j), "'", "''")
''          End If
''          Parser.PsConnection.Execute "INSERT into CROSS_Colonne_HostVar " & _
''                                      "VALUES(" & idOggetto & "," & Riga & ",'" & comando & "','WHERE'," & a & _
''                                      ",'" & NomeCursore & "','" & NomeTabella & _
''                                      "','" & CampoWhere1 & "','',0,'" & Oper & "','" & Trim(CampiIN(j)) & "','',0,0,0,'','',0,False)"
''          a = a + 1
''        Next j
''      Else
''        If Oper = "BETWEEN" Then
''          CampoWhereOld = CampoWhere1
''          isBetween = True
''        End If
''        If InStr(Oper, ":") > 0 Then
''          CampoWhere2 = Mid(Oper, InStr(Oper, ":"))
''          Oper = Left(Oper, InStr(Oper, ":") - 1)
''        ElseIf InStr(Oper, "'") > 0 Then
''          CampoWhere2 = Mid(Oper, InStr(Oper, "'"))
''          Oper = Left(Oper, InStr(Oper, "'") - 1)
''        Else
''          CampoWhere2 = nextToken_new(istruzione)
''        End If
''        If CampoWhere2 = ":" Then
''          CampoWhere2 = CampoWhere2 & nexttoken(istruzione)
''        ElseIf CampoWhere2 = "CURRENT" Then ' CURRENT DATE
''          CampoWhere2 = CampoWhere2 & " " & nexttoken(istruzione)
''        ElseIf Right(CampoWhere2, 1) = ")" And InStr(CampoWhere2, "(") = 0 Then
''          Do While Right(CampoWhere2, 1) = ")"
''            CampoWhere2 = Left(CampoWhere2, Len(CampoWhere2) - 1)
''          Loop
''        ElseIf Left(istruzione, 1) = "(" Then ' SUBSTR, CHAR
''          CampoWhere2 = CampoWhere2 & nextToken_new(istruzione)
''        End If
''        If Left(istruzione, 1) = "+" Or Left(istruzione, 1) = "-" Or _
''           Left(istruzione, 1) = "*" Or Left(istruzione, 1) = "/" Then
''          CampoWhere2 = CampoWhere2 & " " & nextToken_new(istruzione)
''          Do Until Left(istruzione, 1) <> "("
''            CampoWhere2 = CampoWhere2 & " " & nextToken_new(istruzione)
''          Loop
''          istruzione = Trim(istruzione)
''          Do While Left(istruzione, 1) = ")"
''            istruzione = Trim(Mid(istruzione, 2))
''          Loop
''          Do While Left(istruzione, 1) = "("
''            istruzione = Trim(Mid(istruzione, 2))
''          Loop
''        End If
''        If InStr(CampoWhere1, "'") Then
''          CampoWhere1 = Replace(CampoWhere1, "'", "''")
''        End If
''        If InStr(CampoWhere2, "'") Then
''          CampoWhere2 = Replace(CampoWhere2, "'", "''")
''        End If
''        Parser.PsConnection.Execute "INSERT into CROSS_Colonne_HostVar " & _
''                                    "VALUES(" & idOggetto & "," & Riga & ",'" & comando & "','WHERE'," & a & _
''                                    ",'" & NomeCursore & "','" & NomeTabella & _
''                                    "','" & CampoWhere1 & "','',0,'" & Oper & "','" & CampoWhere2 & "','',0,0,0,'','',0,False)"
''        a = a + 1
''      End If
''    End If
''    istruzione = Trim(istruzione)
''    Do While Left(istruzione, 1) = ")"
''      istruzione = Trim(Mid(istruzione, 2))
''    Loop
''    Do While Left(istruzione, 1) = "("
''      istruzione = Trim(Mid(istruzione, 2))
''    Loop
''    CampoWhere1 = nexttoken(istruzione)
''  Loop
''End Sub

Public Sub InserisciBEGIN_END_DECLARE(idObj As Long)
  'X2OS        EXEC SQL BEGIN DECLARE SECTION END-EXEC.
  'X2OS        EXEC SQL END DECLARE SECTION END-EXEC.
  Dim rsVar As Recordset
  
  Set rsVar = m_Fun.Open_Recordset("select * from CROSS_Colonne_HostVar where HIdOggetto = " & idObj)
  Do Until rsVar.EOF
    
      
    rsVar.MoveNext
  Loop
  rsVar.Close
End Sub

Public Sub aggiornaStoredProc(wTypeSql As String, wNomeProc As String, wNomeProcCreator As String, wNomePgm As String, wTipoPgm As String, wStr As String)
  Dim TbOggetti As Recordset, tbTable As Recordset
  Dim rMax As Recordset, maxId As Long
  Dim idpgm As Long
  
  On Error GoTo errorHandler
  idpgm = 0
  Set TbOggetti = m_Fun.Open_Recordset("select * from bs_oggetti where " & _
                                       "Nome = '" & wNomePgm & "' and tipo = '" & wTipoPgm & "'")
  If Not TbOggetti.EOF Then
    TbOggetti!StoredProc = True
    idpgm = TbOggetti!idOggetto
    TbOggetti.Update
  Else
    addSegnalazione wNomePgm, "NOSTOREPROC", Str(GbIdOggetto)
  End If
  TbOggetti.Close
  ' Salvo la STORED PROCEDURE In tabella
  Set tbTable = m_Fun.Open_Recordset("select * from " & wTypeSql & "StoredProc where " & _
                                     "Nome = '" & wNomeProc & "' and Creator = '" & wNomeProcCreator & "' and " & _
                                     "idoggetto = " & GbIdOggetto & " and idproc = " & idpgm)
  If tbTable.RecordCount = 0 Then
    tbTable.AddNew
    
    tbTable!idOggetto = GbIdOggetto
    tbTable!idproc = idpgm
    tbTable!nome = wNomeProc
    tbTable!creator = wNomeProcCreator
    tbTable!program = wNomePgm
    tbTable!tipoPgm = wTipoPgm
    tbTable!parameters = wStr
    
    tbTable.Update
  End If
  tbTable.Close
Exit Sub
errorHandler:
  
End Sub

Public Sub Get_SQLEXEC()
  Dim rsExec As Recordset
  
  On Error GoTo errorHandler
  Init_PsSQL
  ' Dettagli SQL EXEC
'  Set rsExec = m_Fun.Open_Recordset("select * from PSexec where " & _
'                                    "area = 'SQL' and comando not in ('COMMIT','INCLUDE','ROLLBACK','CLOSE','OPEN') " & _
'                                    "order by idoggetto, riga, comando")
  Set rsExec = m_Fun.Open_Recordset("select * from PSexec where " & _
                                    "area = 'SQL' and comando = 'DECLARE' " & _
                                    "order by idoggetto, riga, comando")
  Do Until rsExec.EOF
    Select Case rsExec!comando
      Case "ALLOCATE"
      Case "ASSOCIATE"
      Case "BEGIN"
      Case "CALL"
      Case "CREATE"
      Case "DECLARE"
        getSQLDeclare rsExec!idOggetto, rsExec!Riga, rsExec!istruzione
      Case "DELETE"
      Case "DROP"
      Case "END"
      Case "FETCH"
      Case "FREE"
      Case "INSERT"
      Case "LOCK"
      Case "PREPARE"
      Case "SELECT"
      Case "SELECT'Y'"
      Case "SET"
      Case "UPDATE"
      Case Else
    End Select
    rsExec.MoveNext
  Loop
  rsExec.Close
Exit Sub
errorHandler:
  MsgBox err.Number & " - " & err.description, vbOKOnly + vbCritical, Parser.PsNomeProdotto
End Sub

Sub getSQLDeclare(idObj As Long, Riga As Long, istruzione As String)
  Dim CursorName As String
  Dim withHold As Boolean, withReturn As Boolean
  Dim arrCampi() As TpCampo, arrTabelle() As TpTabella
  Dim rsCursor As Recordset, rsDeclare As Recordset
  Dim i As Integer
  Dim arrCampi_Tmp() As String
  
  ' Definizione di una tabella temporanea
  If InStr(istruzione, " TEMPORARY ") Then
  Else
    ' Dichiarazione di un cursore
    CursorName = Trim(Left(istruzione, InStr(istruzione, " CURSOR ")))
    If CursorName = "" Then
      'Dichiarazione TABLE
      CursorName = nextToken_Mauro(istruzione)
      If Left(Trim(istruzione), 6) = "TABLE " Then
        Exit Sub
      End If
    End If
    istruzione = Trim(Mid(istruzione, InStr(istruzione, " CURSOR ") + 8))
    withHold = False
    If Left(istruzione, 10) = "WITH HOLD " Then
      withHold = True
      istruzione = Trim(Mid(istruzione, 10))
    End If
    withReturn = False
    If Left(istruzione, 12) = "WITH RETURN " Then
      withReturn = True
      istruzione = Trim(Mid(istruzione, 12))
    End If
    Set rsCursor = m_Fun.Open_Recordset("Select * from PsSql_Cursori where " & _
                                        "idoggetto = " & idObj & " and riga = " & Riga)
    rsCursor.AddNew
    rsCursor!idOggetto = idObj
    rsCursor!Riga = Riga
    rsCursor!comando = "DECLARE"
    rsCursor!NomeCursore = CursorName
    rsCursor!withHold = withHold
    rsCursor!withReturn = withReturn
    rsCursor.Update
    rsCursor.Close
    ' FOR SELECT
    istruzione = Trim(Mid(istruzione, 4))
    istruzione = Trim(Mid(istruzione, 7))
    ReDim arrCampi_Tmp(0)
    arrCampi_Tmp() = getListaCampi("FROM", istruzione)
    ReDim arrTabelle(0)
    arrTabelle() = getDetailTabella(getListaCampi("WHERE", istruzione))
    Set rsDeclare = m_Fun.Open_Recordset("Select * from PsSql_Tabelle")
    For i = 0 To UBound(arrTabelle)
      rsDeclare.AddNew
      rsDeclare!idOggetto = idObj
      rsDeclare!Riga = Riga
      rsDeclare!comando = "DECLARE"
      rsDeclare!Tabella = arrTabelle(i).nome
      rsDeclare!Alias_Tabella = arrTabelle(i).Alias
      rsDeclare.Update
    Next i
    rsDeclare.Close
    
    ReDim arrCampi(0)
    arrCampi() = getDetailCampo(arrCampi_Tmp, idObj, Riga, "DECLARE")
    Set rsDeclare = m_Fun.Open_Recordset("Select * from PsSql_Colonne")
    For i = 0 To UBound(arrCampi)
      rsDeclare.AddNew
      rsDeclare!idOggetto = idObj
      rsDeclare!Riga = Riga
      rsDeclare!comando = "DECLARE"
      rsDeclare!Ordinale = arrCampi(i).Ordinale
      rsDeclare!Colonna = arrCampi(i).nome
      rsDeclare!Funzione = arrCampi(i).Funzione
      rsDeclare!Tipo = arrCampi(i).Tipo
      rsDeclare!Lunghezza = arrCampi(i).Lunghezza
      rsDeclare!Decimali = arrCampi(i).Decimali
      rsDeclare!Alias_Colonna = arrCampi(i).Alias_Colonna
      rsDeclare!Tabella = arrCampi(i).Tabella
      rsDeclare!Alias_Tabella = arrCampi(i).Alias_Tabella
      rsDeclare.Update
    Next i
    rsDeclare.Close
  End If
End Sub

Function getListaCampi(KeyWord As String, istruzione As String) As String()
  Dim appoTxt As String
  Dim arrCampi() As String
  Dim i As Long
  
  On Error GoTo parseErr
  ReDim arrCampi(0)
  appoTxt = nextToken_Mauro(istruzione)
  Do Until appoTxt = KeyWord Or Len(istruzione) = 0
    If appoTxt = "," Then
      ' Nuovo Campo
      ReDim Preserve arrCampi(UBound(arrCampi) + 1)
    Else
      arrCampi(UBound(arrCampi)) = Trim(arrCampi(UBound(arrCampi)) & " " & appoTxt)
    End If
    appoTxt = nextToken_Mauro(istruzione)
  Loop
  getListaCampi = arrCampi
  Exit Function
parseErr:  'Gestito a livello superiore...
  Parser.PsFinestra.ListItems.Add , , "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
  Parser.PsFinestra.ListItems(Parser.PsFinestra.ListItems.count).EnsureVisible
  m_Fun.writeLog "parsePgm_I: id#" & GbIdOggetto & ", ###" & err.description
End Function

Function getDetailCampo(Campi() As String, idObj As Long, Riga As Long, KeyWord As String) As TpCampo()
  Dim appocampo() As TpCampo
  Dim i As Integer, a As Integer, token As String
  Dim isTrovato As Boolean
  Dim rstable As Recordset
  
  ReDim appocampo(0)
  For i = 0 To UBound(Campi)
    If InStr(Campi(i), " AS ") Then
      appocampo(UBound(appocampo)).Alias_Colonna = Mid(Campi(i), InStr(Campi(i), " AS ") + 4)
      Campi(i) = Trim(Left(Campi(i), InStr(Campi(i), " AS ")))
    End If
    Do Until Campi(i) = ""
      token = SeparaColonne(Campi(i))
      If token = "CASE" Then
        Campi(i) = Trim(Replace(Campi(i) & " ", " END ", ""))
      End If
      appocampo(UBound(appocampo)).Ordinale = i + 1
      If InStr(token, "(") Then
        appocampo(UBound(appocampo)).Funzione = Left(token, InStrRev(token, "(") - 1)
        token = Mid(token, InStrRev(token, "(") + 1, InStr(token, ")") - InStrRev(token, "(") - 1)
      End If
      If InStr(token, ".") Then
        appocampo(UBound(appocampo)).Alias_Tabella = Trim(Left(token, InStr(token, ".") - 1))
        appocampo(UBound(appocampo)).nome = Trim(Mid(token, InStr(token, ".") + 1))
      Else
        If Len(token) > 0 And Len(Campi(i)) = 0 Then
          appocampo(UBound(appocampo)).nome = Trim(token)
        End If
      End If
      If Len(appocampo(UBound(appocampo)).nome) Then
        If Len(appocampo(UBound(appocampo)).Alias_Tabella) > 0 Then
          Set rstable = m_Fun.Open_Recordset("select tabella from PsSql_Tabelle where " & _
                                             "Alias_Tabella = '" & appocampo(UBound(appocampo)).Alias_Tabella & "' and " & _
                                             "idoggetto = " & idObj & " and riga = " & Riga & " and comando = '" & KeyWord & "'")
          If rstable.RecordCount = 1 Then
            appocampo(UBound(appocampo)).Tabella = rstable!Tabella
          Else
            ' ERRORE
            MsgBox "ERRORE"
          End If
          rstable.Close
          TrovaColonna appocampo(UBound(appocampo)).Tabella, appocampo(UBound(appocampo)).nome, _
                       appocampo(UBound(appocampo)).Tipo, appocampo(UBound(appocampo)).Lunghezza
        Else
          If appocampo(UBound(appocampo)).Tabella = "" Then
            ' Facio un giro su tutte le tabelle della query per trovare quella giusta
            Set rstable = m_Fun.Open_Recordset("select tabella from pssql_tabelle where " & _
                                               "idoggetto = " & idObj & " and riga = " & Riga & " and comando = '" & KeyWord & "'")
            Do Until rstable.EOF
              TrovaColonna rstable!Tabella, appocampo(UBound(appocampo)).nome, _
                           appocampo(UBound(appocampo)).Tipo, appocampo(UBound(appocampo)).Lunghezza
              If Len(appocampo(UBound(appocampo)).Tipo) > 0 Then
                appocampo(UBound(appocampo)).Tabella = rstable!Tabella
                Exit Do
              End If
              rstable.MoveNext
            Loop
            rstable.Close
          Else
            TrovaColonna appocampo(UBound(appocampo)).Tabella, appocampo(UBound(appocampo)).nome, _
                         appocampo(UBound(appocampo)).Tipo, appocampo(UBound(appocampo)).Lunghezza
          End If
        End If
        
        isTrovato = False
        For a = 0 To UBound(appocampo) - 1
          If appocampo(a).Ordinale = appocampo(UBound(appocampo)).Ordinale Then
            appocampo(UBound(appocampo)).Alias_Colonna = appocampo(a).Alias_Colonna
            If appocampo(a).nome = appocampo(UBound(appocampo)).nome Then
              isTrovato = True
              Exit For
            End If
          End If
        Next a
        If Not isTrovato Then
          ReDim Preserve appocampo(UBound(appocampo) + 1)
        End If
      End If
    Loop
  Next i
  
  ' L'ultimo array � vuoto
  ReDim Preserve appocampo(UBound(appocampo) - 1)
  getDetailCampo = appocampo
End Function

Function getDetailTabella(Tabelle() As String) As TpTabella()
  Dim appoTabella() As TpTabella
  Dim i As Integer, instrJOIN As Integer
  
  ReDim appoTabella(0)
  For i = 0 To UBound(Tabelle)
    If InStr(Tabelle(i), " ") Then
      appoTabella(UBound(appoTabella)).Alias = Trim(Mid(Tabelle(i), InStrRev(Tabelle(i), " ")))
      Tabelle(i) = Trim(Left(Tabelle(i), InStrRev(Tabelle(i), " ")))
    End If
    If InStr(Tabelle(i), " JOIN ") Then
      instrJOIN = InStr(Tabelle(i), " ON ")
      
      ' Tipo JOIN
      ' ...
      ' Tabella in JOIN
      ' ...
      ' Campi in JOIN
      ' ...
    End If
    appoTabella(UBound(appoTabella)).nome = Tabelle(i)
    ReDim Preserve appoTabella(UBound(appoTabella) + 1)
  Next i
  
  ' L'ultimo array � vuoto
  ReDim Preserve appoTabella(UBound(appoTabella) - 1)
  getDetailTabella = appoTabella
End Function

'SQ: provvisorio per "," come separatore... farne uno con argomento opzionale...
'ritorna la parola successiva (space unico separatore)
'Considera come delimitatore di "token" le parentesi e gli apici singoli...
'Effetto Collaterale: mangia...
'
'Dovra' diventare quello definitivo: mantiene gli apici...
Function nextToken_Mauro(inputString As String)
  Dim level As Integer, i As Integer, j As Integer, y As Integer, Z As Integer
  Dim currentChar As String
  Dim isComment As Boolean
  
  'Mangio i bianchi davanti (primo giro...)
  inputString = LTrim(inputString)
  isComment = False
  If Len(inputString) > 0 Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "("
        i = 2
        level = 1
        nextToken_Mauro = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "("
              If Not isComment Then
                level = level + 1
              End If
              nextToken_Mauro = nextToken_Mauro & currentChar
            Case ")"
              If Not isComment Then
                level = level - 1
              End If
              nextToken_Mauro = nextToken_Mauro & currentChar
            Case "'"
              If Not isComment Then
                isComment = True
              Else
                isComment = False
              End If
              nextToken_Mauro = nextToken_Mauro & currentChar
            Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nextToken_Mauro = nextToken_Mauro & currentChar
              Else
                err.Raise 123, "nextToken_Mauro", "syntax error on " & inputString
                Exit Function
              End If
          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
      Case "'"
        'Ritorno gli apici
        ''''inputString = Mid(inputString, 2)
        i = InStr(2, inputString, "'")
        If i Then
          'nextToken_Mauro = Left(inputString, InStr(inputString, "'") - 1)
          nextToken_Mauro = Left(inputString, i)
          inputString = Mid(inputString, i + 1)
        Else
          'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
          inputString = "''" & inputString
          
          err.Raise 123, "nextToken_Mauro", "syntax error on " & inputString
          Exit Function
        End If
      Case ","
        nextToken_Mauro = Left(inputString, 1)
        inputString = Trim(Mid(inputString, 2))
      Case Else
        i = InStr(inputString, " ")
        j = InStr(inputString, "(")
        y = InStr(inputString, "'")
        Z = InStr(inputString, ",")
        If (i > 0 And j > 0 And j < i) Or (i = 0 And j > 0) Then i = j
        If (i > 0 And y > 0 And y < i) Or (i = 0 And y > 0) Then i = y
        If (i > 0 And Z > 0 And Z < i) Or (i = 0 And Z > 0) Then i = Z
        If i > 0 Then
          nextToken_Mauro = Left(inputString, i - 1)
          inputString = Trim(Mid(inputString, i))
        Else
          nextToken_Mauro = inputString
          inputString = ""
        End If
    End Select
  End If
End Function

Sub Init_PsSQL()
  Parser.PsConnection.Execute "Delete * from PsSQL_Cursori"
  Parser.PsConnection.Execute "Delete * from PsSQL_Colonne"
  Parser.PsConnection.Execute "Delete * from PsSQL_Tabelle"
  Parser.PsConnection.Execute "Delete * from PsSQL_WhereConditions"
End Sub

Public Function SeparaColonne(Campi As String) As String
  Dim appo As String
  
  appo = Trim(Left(Campi, InStr(Campi & " ", " ") - 1))
  Campi = Trim(Mid(Campi, InStr(Campi & " ", " ") + 1))
  If Left(Campi, 1) = "(" Then
    appo = appo & Left(Campi, InStrRev(Campi, ")"))
    Campi = Trim(Mid(Campi, InStrRev(Campi, ")") + 1))
  End If
  If InStr(appo, " ") Then
    
  End If
  SeparaColonne = appo
End Function

Public Sub copiaColonne(idTb1 As Long, idTb2 As Long, wTypeSql As String)
  Dim rsTb1 As Recordset, rsTb2 As Recordset
  Dim i As Integer
    
  Set rsTb1 = m_Fun.Open_Recordset("select * from " & wTypeSql & "colonne where idtable = " & idTb1)
  Set rsTb2 = m_Fun.Open_Recordset("select * from " & wTypeSql & "colonne where idtable = " & idTb2 & " and idtable <> idtablejoin")
  Do Until rsTb2.EOF
    rsTb1.AddNew
    
    rsTb1!idcolonna = getId("idcolonna", wTypeSql & "colonne")
    rsTb1!IdTable = idTb1
    For i = 2 To rsTb2.Fields.count - 1
      rsTb1.Fields(i).value = rsTb2.Fields(i).value
    Next i
    rsTb1.Update
    
    rsTb2.MoveNext
  Loop
  rsTb2.Close
  
End Sub
