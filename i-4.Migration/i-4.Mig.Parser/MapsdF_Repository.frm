VERSION 5.00
Begin VB.Form MapsdF_Repository 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Repository"
   ClientHeight    =   5100
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8865
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   8865
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "MapsdF_Repository"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub Resize()
  On Error Resume Next
  Me.Move Parser.PsLeft, Parser.PsTop, Parser.PsWidth, Parser.PsHeight
End Sub

Private Sub Form_Activate()
    If Not VerifyPs Then
       Unload Me
    End If
End Sub

Private Sub Form_Load()
  Resize
  If Parser.PsTipoFinIm <> "" Then
    Parser.PsFinestra.ListItems.Add , , "Parser: Repository Window Loaded at " & Time
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  If Parser.PsTipoFinIm <> "" Then
    Parser.PsFinestra.ListItems.Add , , "Parser: Repository Window Unloaded at " & Time
  End If
End Sub


