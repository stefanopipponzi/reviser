VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MapsdF_Parser 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Object Parser"
   ClientHeight    =   5580
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5580
   ScaleWidth      =   9000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin RichTextLib.RichTextBox rText2 
      Height          =   30
      Left            =   2880
      TabIndex        =   11
      Top             =   5520
      Visible         =   0   'False
      Width           =   135
      _ExtentX        =   238
      _ExtentY        =   53
      _Version        =   393217
      TextRTF         =   $"MapsdF_Parser.frx":0000
   End
   Begin RichTextLib.RichTextBox rText 
      Height          =   645
      Left            =   810
      TabIndex        =   10
      Top             =   6210
      Visible         =   0   'False
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   1138
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MapsdF_Parser.frx":008B
   End
   Begin VB.Frame Frame4 
      Caption         =   "Activity Log"
      ForeColor       =   &H00000080&
      Height          =   3135
      Left            =   3570
      TabIndex        =   5
      Top             =   450
      Width           =   5385
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   255
         Left            =   150
         TabIndex        =   7
         Top             =   2730
         Width           =   5085
         _ExtentX        =   8969
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Min             =   1e-4
         Scrolling       =   1
      End
      Begin MSComctlLib.ListView LvObject 
         Height          =   2415
         Left            =   120
         TabIndex        =   6
         Top             =   270
         Width           =   5115
         _ExtentX        =   9022
         _ExtentY        =   4260
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "ID"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Object"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Type"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Start"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Elapsed"
            Object.Width           =   1764
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Log"
      ForeColor       =   &H00000080&
      Height          =   1905
      Left            =   3570
      TabIndex        =   4
      Top             =   3600
      Width           =   5385
      Begin RichTextLib.RichTextBox RTErr 
         Height          =   1485
         Left            =   120
         TabIndex        =   8
         Top             =   270
         Width           =   5115
         _ExtentX        =   9022
         _ExtentY        =   2619
         _Version        =   393217
         BackColor       =   16777152
         Enabled         =   -1  'True
         TextRTF         =   $"MapsdF_Parser.frx":0116
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Analysis Functions"
      ForeColor       =   &H00000080&
      Height          =   1905
      Left            =   0
      TabIndex        =   3
      Top             =   3600
      Width           =   3495
      Begin MSComctlLib.TreeView TWFun 
         Height          =   1455
         Left            =   150
         TabIndex        =   9
         Top             =   300
         Width           =   3195
         _ExtentX        =   5636
         _ExtentY        =   2566
         _Version        =   393217
         Indentation     =   529
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Select Object for Analysis"
      ForeColor       =   &H00000080&
      Height          =   3135
      Left            =   0
      TabIndex        =   1
      Top             =   450
      Width           =   3495
      Begin MSComctlLib.TreeView TVSelObj 
         Height          =   2715
         Left            =   150
         TabIndex        =   2
         Top             =   270
         Width           =   3195
         _ExtentX        =   5636
         _ExtentY        =   4789
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   529
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9000
      _ExtentX        =   15875
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Attiva"
            Object.ToolTipText     =   "Start Parsing"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "PrintLog"
            Object.ToolTipText     =   "Print Error Log"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Help"
            Object.ToolTipText     =   "Help"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Analisys_DECLARE"
            Object.ToolTipText     =   "Analisys EXEC SQL"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   "Update_DECLARE"
            Object.ToolTipText     =   "Modify BEGIN/END DECLARE"
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   2760
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MapsdF_Parser.frx":0198
            Key             =   "Attiva"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MapsdF_Parser.frx":04B2
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MapsdF_Parser.frx":05C4
            Key             =   "Printer"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MapsdF_Parser.frx":08E8
            Key             =   "Analisys"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MapsdF_Parser.frx":0A42
            Key             =   "Update"
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox rText1 
      Height          =   30
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Visible         =   0   'False
      Width           =   135
      _ExtentX        =   238
      _ExtentY        =   53
      _Version        =   393217
      TextRTF         =   $"MapsdF_Parser.frx":0B9C
   End
End
Attribute VB_Name = "MapsdF_Parser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim NumObjInParser As Double
Dim NumFeatures As Integer
Dim FlagStop As Boolean

Private Sub Form_KeyPress(KeyAscii As Integer)
  If KeyAscii = 27 Then
    FlagStop = True
  End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
  
  If m_Fun.FnProcessRunning Then
    wScelta = m_Fun.Show_MsgBoxError("FB01I")
    
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_Fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_Fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim i As Long, idOggetto As Long, instrDel As Long, a As Long
  Dim instrTxt As String, txtHostVar As String
  Dim rsExec As Recordset, rsArea As Recordset
  Dim HIdOggetto As Long, HIdArea As Integer, HOrdinale As Integer, HByte As Integer
  Dim HTipo As String, HUsage As String
  Dim parameters() As String
  Dim areaHostVar As String, HostVar As String
  
  On Error Resume Next
  
  FlagStop = False
  Screen.MousePointer = vbHourglass
  
  Select Case Button.Key
    Case "Attiva"
      m_Fun.FnProcessRunning = True
      
      If NumObjInParser = 0 Then
        MsgBox "No Object selected for parser analysis " & vbCrLf, vbInformation, "i-4.Migration - Parser"
      Else
        If NumFeatures = 0 Then
          MsgBox "No Function selected for parser analysis." & vbCrLf, vbInformation, "i-4.Migration - Parser"
        Else
          AttivaParser
          'SG : Elimina i record su istrdett_liv , dup e key che sono IMS/DC ; si dovr� fare direttamente nelle funzioni.
          'SQ Cos'� sta roba??????????????????  erase_Istr_IMSDC_suLivelli_e_Key
        End If
      End If
      
      m_Fun.FnProcessRunning = False
    Case "PrintLog"
      '?
    Case "Help"
      m_Fun.Show_MsgBoxError "PB00P"
    ' Mauro 18/03/2009 : Cross reference tra Campi e Host Variable
    Case "Analisys_DECLARE"
''''      m_Fun.FnProcessRunning = True
''''      instrDel = 0
''''      instrTxt = " IDOBJECT |   ROW   |   COMAND   | ERROR " & vbCrLf & _
''''                 "-----------------------------------------" & vbCrLf
''''      isError = False
''''      For i = 1 To Parser.PsObjList.ListItems.count
''''        If Parser.PsObjList.ListItems(i).Selected Then
''''          If Parser.PsObjList.ListItems(i).ListSubItems(2) = "CBL" Then
''''            idOggetto = Val(Parser.PsObjList.ListItems(i).text)
''''            instrDel = instrDel + Update_Cross_Campi_HostVar(idOggetto, instrTxt)
''''          End If
''''        End If
''''      Next i
''''      m_Fun.FnProcessRunning = False
''''      Screen.MousePointer = vbNormal
''''      If isError Then
''''        MsgBox "Process terminated with errors! " & vbCrLf & _
''''               "Check " & m_Fun.FnPathPrj & "\DOCUMENTS\LOG\ANALISYS_DECLARE.LOG" & vbCrLf & _
''''               "Instructions not managed: " & instrDel, vbInformation + vbOKOnly, Parser.PsNomeProdotto
''''      Else
''''        MsgBox "Process terminated successfully!" & vbCrLf & _
''''               "Instructions not managed: " & instrDel, vbInformation + vbOKOnly, Parser.PsNomeProdotto
''''      End If
''''      If instrDel > 0 Then
''''        m_Fun.Show_ShowText instrTxt, "Instructions not Managed"
''''      End If
''      m_Fun.FnProcessRunning = True
''      Ricerca_Hostvar
''
''      ' Dettagli HostVar su PsData_Cmp
''      Set rsExec = m_Fun.Open_Recordset("select * from PS_SQL_Hostvar " & _
''                                        "where hostvar <> '' and H_IdOggetto Is Null or H_IdOggetto = 0 " & _
''                                        "order by idoggetto, riga")
''      Do Until rsExec.EOF
''        If Left(rsExec!HostVar, 1) = "(" Then
''          txtHostVar = Trim(Replace(Replace(rsExec!HostVar, ")", ""), "(", ""))
''        Else
''          txtHostVar = rsExec!HostVar
''        End If
''        If Left(txtHostVar, 1) = ":" Then
''          HIdOggetto = 0
''          HIdArea = 0
''          HOrdinale = 0
''          HTipo = ""
''          HUsage = ""
''          HByte = 0
''          TrovaCmp rsExec!idOggetto, Trim(Mid(txtHostVar, 2)), HTipo, HUsage, HByte, HIdOggetto, HIdArea, HOrdinale
''          rsExec!H_idoggetto = HIdOggetto
''          rsExec!H_idarea = HIdArea
''          rsExec!H_ordinale = HOrdinale
''          rsExec!H_Tipo = HTipo
''          rsExec!H_Usage = HUsage
''          rsExec!H_Byte = HByte
''          rsExec!is_linkage = False
''          If HIdOggetto > 0 And HIdArea > 0 Then
''            Set rsArea = m_Fun.Open_Recordset("select islinkage from psdata_area where " & _
''                                              "idoggetto = " & HIdOggetto & " and idarea = " & HIdArea)
''            If Not rsArea.EOF Then
''              rsExec!is_linkage = rsArea!isLinkage
''            End If
''            rsArea.Close
''          End If
''          If Not rsExec!is_linkage Then
''            Set rsArea = m_Fun.Open_Recordset("select parameters from pspgm where idoggetto = " & rsExec!idOggetto)
''            If Not rsArea.EOF Then
''              If Len(rsArea!parameters) Then
''                parameters = Split(rsArea!parameters, ",")
''                If InStr(txtHostVar, ".") Then
''                  ' Se c'� il punto la HostVar � Area.Variabile
''                  HostVar = Mid(txtHostVar, InStr(txtHostVar, ".") + 1)
''                Else
''                  HostVar = txtHostVar
''                End If
''                HostVar = Trim(Replace(HostVar, ":", ""))
''                For a = 0 To UBound(parameters)
''                  If HostVar = parameters(a) Then
''                    rsExec!is_linkage = True
''                    Exit For
''                  End If
''                Next a
''              End If
''            End If
''            rsArea.Close
''          End If
''          rsExec.Update
''        End If
''        rsExec.MoveNext
''      Loop
''      rsExec.Close
''      m_Fun.FnProcessRunning = False
''      Screen.MousePointer = vbNormal
''      MsgBox "Process terminated successfully!", vbInformation + vbOKOnly, Parser.PsNomeProdotto
      m_Fun.FnProcessRunning = True
      Get_SQLEXEC
      m_Fun.FnProcessRunning = False
      Screen.MousePointer = vbNormal
      MsgBox "Process terminated successfully!", vbInformation + vbOKOnly, Parser.PsNomeProdotto
    Case "Update_DECLARE"
      m_Fun.FnProcessRunning = True
      For i = 1 To Parser.PsObjList.ListItems.count
        If Parser.PsObjList.ListItems(i).Selected Then
          If Parser.PsObjList.ListItems(i).ListSubItems(2) = "CBL" Then
            idOggetto = Val(Parser.PsObjList.ListItems(i).text)
            InserisciBEGIN_END_DECLARE idOggetto
          End If
        End If
      Next i
      m_Fun.FnProcessRunning = False
      Screen.MousePointer = vbNormal
      MsgBox "Process terminated!", vbInformation + vbOKOnly, Parser.PsNomeProdotto
  End Select
  
  Screen.MousePointer = vbNormal
End Sub

Sub AttivaParser()
  Dim i As Integer
  Dim idOggetto As Variant
  Dim startTime As Variant
  Dim endTime As Variant
  Dim rs As Recordset
  Dim wKey As String
  Dim wResp As Variant
  
  LvObject.ListItems.Clear
  RTErr.text = ""
  ProgressBar1.value = 1
  ProgressBar1.Max = NumObjInParser + 1
  Parser.PsFinestra.ListItems.Add , , "Press ESC key to interrupt the job"
  
  Me.SetFocus
  
  ActParsDati = False
  ActParsRela = False
  ActParsCics = False
  ActParsIms = False
  ActParsSQL = False
  ActParsDli = False
  ActParsVSAM = False
  
  For i = 1 To TWFun.Nodes.count
    If TWFun.Nodes(i).Checked Then
      Select Case TWFun.Nodes(i).Key
        Case "DT"
          ActParsDati = True
        Case "OR"
          ActParsRela = True
        Case "CX"
          ActParsCics = True
        Case "IC"
          ActParsIms = True
        Case "SQ"
          ActParsSQL = True
        Case "IB"
          ActParsDli = True
        Case "VS"
          ActParsVSAM = True
      End Select
    End If
  Next i

  If TVSelObj.Nodes(1).Checked Then
    For i = 1 To Parser.PsObjList.ListItems.count
      If Parser.PsObjList.ListItems(i).Selected Then
        idOggetto = Val(Parser.PsObjList.ListItems(i).text)
        Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_oggetti where IdOggetto = " & idOggetto)
        With LvObject.ListItems.Add(, , format(TbOggetti!idOggetto, "000000"))
          .SubItems(1) = TbOggetti!nome
          .SubItems(2) = TbOggetti!Tipo
          startTime = Timer
          .SubItems(3) = format(Now, "HH:MM:SS")
          ProgressBar1.value = ProgressBar1.value + 1
          ProgressBar1.Refresh
          LvObject.Refresh
          LvObject.ListItems(LvObject.ListItems.count).EnsureVisible
          
          AnalizzaOggetto TbOggetti!idOggetto
          
          endTime = Timer
          endTime = endTime - startTime
          .SubItems(4) = FormatX(endTime)
        End With
        LvObject.Refresh
        LvObject.ListItems(LvObject.ListItems.count).EnsureVisible
        'Me.SetFocus
        Me.KeyPreview = True
        If FlagStop = True Then
          wResp = m_Fun.Show_MsgBoxError("PB00Q")
          If wResp = vbYes Then
            Exit For
          End If
        End If
      End If
    Next i
    Exit Sub
  End If
  
  For i = 2 To TVSelObj.Nodes.count
    If TVSelObj.Nodes(i).Checked Then
      wKey = TVSelObj.Nodes(i).Key
      If wKey = "MPS" Then
        wKey = "BMS' or tipo = 'MFS"
      End If
      Set rs = m_Fun.Open_Recordset("Select * from bs_Oggetti where tipo = '" & wKey & "'")
      While Not rs.EOF
        idOggetto = rs!idOggetto
        Set TbOggetti = m_Fun.Open_Recordset("select * from Bs_oggetti where IdOggetto = " & idOggetto)
        With LvObject.ListItems.Add(, , format(TbOggetti!idOggetto, "000000"))
          .SubItems(1) = TbOggetti!nome
          .SubItems(2) = TbOggetti!Tipo
          startTime = Timer
          .SubItems(3) = format(Now, "HH:MM:SS")
          ProgressBar1.value = ProgressBar1.value + 1
          ProgressBar1.Refresh
          LvObject.Refresh
          LvObject.ListItems(LvObject.ListItems.count).EnsureVisible
          
          AnalizzaOggetto TbOggetti!idOggetto
          
          endTime = Timer
          endTime = endTime - startTime
          .SubItems(4) = FormatX(endTime)
        End With
        LvObject.Refresh
        LvObject.ListItems(LvObject.ListItems.count).EnsureVisible
        
        rs.MoveNext
        'Me.SetFocus
        DoEvents
        Me.KeyPreview = True
        If FlagStop Then
          wResp = m_Fun.Show_MsgBoxError("PB00Q")
          If wResp = vbYes Then
            Exit For
          End If
        End If
      Wend
      rs.Close
    End If
  Next i
End Sub

Public Sub Resize()
  On Error Resume Next
  
  Me.Move m_Fun.FnLeft, m_Fun.FnTop, m_Fun.FnWidth, m_Fun.FnHeight
  Frame2.Move 30, 400, Frame2.Width, (Parser.PsHeight - 400 - Toolbar1.Height) / 3 * 2
  TVSelObj.Move 120, 240, Frame2.Width - 240, Frame2.Height - 360
  Frame1.Move 30, 430 + Frame2.Height, Frame1.Width, ((Parser.PsHeight - 340) / 3) - 60
  TWFun.Move 120, 240, Frame1.Width - 240, Frame1.Height - 360
  Frame4.Move Frame4.Left, Frame2.Top, Parser.PsWidth - Frame4.Left - 120, Frame2.Height
  LvObject.Move 120, 240, Frame4.Width - 240, Frame4.Height - 420 - ProgressBar1.Height
  ProgressBar1.Move LvObject.Left, LvObject.Top + LvObject.Height + 30, LvObject.Width, ProgressBar1.Height
  Frame3.Move Frame3.Left, Frame1.Top, Parser.PsWidth - Frame3.Left - 120, Frame1.Height
  RTErr.Move 120, 240, Frame3.Width - 240, Frame3.Height - 360
End Sub

Private Sub Form_Load()
  'Livello I
  TWFun.Nodes.Add , , "LV1", "Parsing Level 1"
  TWFun.Nodes.Add "LV1", tvwChild, "DT", "Data Definitions"
  TWFun.Nodes.Add "LV1", tvwChild, "OR", "Code Analysis"
  'Livello II
  TWFun.Nodes.Add , , "LV2", "Parsing Level 2"
  'TWFun.Nodes.Add "LV2", tvwChild, "CX", "CICS Instructions"
  TWFun.Nodes.Add "LV2", tvwChild, "IC", "IMS/DC Instructions"
  'TWFun.Nodes.Add "LV2", tvwChild, "SQ", "SQL Instructions"
  TWFun.Nodes.Add "LV2", tvwChild, "IB", "IMS/DB Instructions"
  TWFun.Nodes.Add "LV2", tvwChild, "VS", "VSAM Instructions"
  
  NumFeatures = 0
  NumObjInParser = 0

'  LstFunction.AddItem "VSAM Features"
'  LstFunction.AddItem "RDBMS Features"

  TVSelObj.Nodes.Add , , "SEL", "Only Selected Object"
'  TVSelObj.Nodes.Add , , "ALL", "All"
  
'  TVSelObj.Nodes.Add "ALL", tvwChild, "JOB", "JOB"
  TVSelObj.Nodes.Add , , "CMD", "COMMAND"
  TVSelObj.Nodes.Add "CMD", tvwChild, "JCL", "JCL"
  TVSelObj.Nodes.Add "CMD", tvwChild, "PRC", "Procedure"
  TVSelObj.Nodes.Add "CMD", tvwChild, "MBR", "JCL Member"
  TVSelObj.Nodes.Add "CMD", tvwChild, "PRM", "Parameter"
  TVSelObj.Nodes.Add "CMD", tvwChild, "JOB", "JOB"
  TVSelObj.Nodes.Add "CMD", tvwChild, "JMB", "JMB"
  TVSelObj.Nodes.Add "CMD", tvwChild, "EZM", "EasyTrieve Macro"
  TVSelObj.Nodes.Add "CMD", tvwChild, "EZT", "EasyTrieve Program"
  
'  TVSelObj.Nodes.Add "ALL", tvwChild, "DLI", "Dli"
  TVSelObj.Nodes.Add , , "DLI", "DataBase"
  TVSelObj.Nodes.Add "DLI", tvwChild, "PSB", "Psb"
  TVSelObj.Nodes.Add "DLI", tvwChild, "DBD", "Dbd"
  TVSelObj.Nodes.Add "DLI", tvwChild, "DDL", "Ddl"
  
'  TVSelObj.Nodes.Add "ALL", tvwChild, "SRC", "Sources"
  TVSelObj.Nodes.Add , , "SRC", "Sources"
  TVSelObj.Nodes.Add "SRC", tvwChild, "BMS", "Mapset"
  TVSelObj.Nodes.Add "SRC", tvwChild, "MFS", "IMS Map"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CMF", "Copy MFS"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CBL", "Cobol Program"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CPY", "Cobol Copy"
  TVSelObj.Nodes.Add "SRC", tvwChild, "ASM", "Assembler"
  TVSelObj.Nodes.Add "SRC", tvwChild, "PLI", "PLI"
'migdal
  TVSelObj.Nodes.Add "SRC", tvwChild, "INC", "PLI Includes"
  TVSelObj.Nodes.Add "SRC", tvwChild, "NAT", "Natural"
  TVSelObj.Nodes.Add "SRC", tvwChild, "NIN", "Natural includes"
  TVSelObj.Nodes.Add "SRC", tvwChild, "LOA", "Load Definition"
  
  'Resize
  If Parser.PsTipoFinIm <> "" Then
    Parser.PsFinestra.ListItems.Add , , "Parser: Parser Window Loaded at " & Time
  End If
  
  m_Fun.AddActiveWindows Me
  m_Fun.FnActiveWindowsBool = True
  
'''  Dim rsExec As Recordset
'''  Set rsExec = m_Fun.Open_Recordset("Select to_Modify From CROSS_Colonne_HostVar Where to_Modify = True")
'''  If Not rsExec.EOF Then
'''    Toolbar1.Buttons(7).Enabled = True
'''  End If
'''  rsExec.Close
End Sub

Private Sub Form_Unload(Cancel As Integer)
  If Parser.PsTipoFinIm <> "" Then
    Parser.PsFinestra.ListItems.Add , , "Parser: Parser Window Unloaded at " & Time
  End If
  
  m_Fun.AddActiveWindows Me
  m_Fun.FnActiveWindowsBool = False
  ActParsVSAM = TWFun.Nodes(7).Checked
End Sub

Private Sub TWFun_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  TWFun.Nodes(2).Checked = TWFun.Nodes(1).Checked
  TWFun.Nodes(3).Checked = TWFun.Nodes(1).Checked
  TWFun.Nodes(5).Checked = TWFun.Nodes(4).Checked
  TWFun.Nodes(6).Checked = TWFun.Nodes(4).Checked
  ' VSM non segue parser 2 quando � flaggato, deflaggato se parser2 deflaggato
  If Not TWFun.Nodes(4).Checked Then
    TWFun.Nodes(7).Checked = False
  End If
  
  'TWFun.Nodes(7).Checked = TWFun.Nodes(4).Checked
  'TWFun.Nodes(8).Checked = TWFun.Nodes(4).Checked
  'TWFun.Nodes(9).Checked = TWFun.Nodes(4).Checked
End Sub

Private Sub TWFun_NodeCheck(ByVal Node As MSComctlLib.Node)
  Dim k1 As Integer

   If Node.Index = 1 Then
     TWFun.Nodes(2).Checked = TWFun.Nodes(1).Checked
     TWFun.Nodes(3).Checked = TWFun.Nodes(1).Checked
   ElseIf Node.Index = 4 Then
     TWFun.Nodes(5).Checked = TWFun.Nodes(4).Checked
     TWFun.Nodes(6).Checked = TWFun.Nodes(4).Checked
     TWFun.Nodes(7).Checked = TWFun.Nodes(4).Checked
     ' nodi eliminati: SQL e CICS
     'TWFun.Nodes(8).Checked = TWFun.Nodes(4).Checked
     'TWFun.Nodes(9).Checked = TWFun.Nodes(4).Checked
   Else
'     If Node.Index = 7 Then ' svincolo VSM
'      TWFun.Nodes(7).Checked = Not TWFun.Nodes(7).Checked
'     End If
     Exit Sub
   End If

  NumFeatures = 0
  For k1 = 1 To TWFun.Nodes.count
    If TWFun.Nodes(k1).Checked Then
      NumFeatures = NumFeatures + 1
    End If
  Next k1
  TWFun.Refresh
End Sub
'''''''''''''''''''''''''''''''
'30-03-06
'Aggiunti controlli check-box
'''''''''''''''''''''''''''''''
Private Sub TVSelObj_NodeCheck(ByVal Node As MSComctlLib.Node)
 Dim k1 As Integer
 Dim wKey As String
 Dim wCheck As Variant
 Dim tb As Recordset

 wKey = Node.Key
 wCheck = Node.Checked

 'NumObjInParser = 0

  Select Case Node.Key
    Case "SEL"
      If Node.Checked Then
        NumObjInParser = 0
        For k1 = 1 To Parser.PsObjList.ListItems.count
          If Parser.PsObjList.ListItems(k1).Selected Then
            NumObjInParser = NumObjInParser + 1
          End If
        Next k1
        For k1 = 2 To TVSelObj.Nodes.count
          TVSelObj.Nodes(k1).Checked = False
        Next k1
      End If
    Case Else
      TVSelObj.Nodes(1).Checked = False
      If Node.Checked Then
        For k1 = Node.Index + 1 To Node.Index + Node.Children
          TVSelObj.Nodes(k1).Checked = True
        Next k1
      Else
        For k1 = Node.Index + 1 To Node.Index + Node.Children
          TVSelObj.Nodes(k1).Checked = False
        Next k1
      End If
  End Select

  If Not TVSelObj.Nodes(1).Checked Then
    NumObjInParser = 0
    For k1 = 2 To TVSelObj.Nodes.count
    If TVSelObj.Nodes(k1).Checked Then
      wKey = TVSelObj.Nodes(k1).Key
      If wKey = "MPS" Then
        wKey = "BMS' or tipo = 'MFS"
      End If
      Set tb = m_Fun.Open_Recordset("Select * from bs_Oggetti where tipo = '" & wKey & "'")
      If tb.RecordCount Then
        tb.MoveLast
        NumObjInParser = NumObjInParser + tb.RecordCount
      End If
      tb.Close
    End If
    Next k1
  End If

  Frame4.Caption = "Activite log for " & NumObjInParser & " Objects"
End Sub
