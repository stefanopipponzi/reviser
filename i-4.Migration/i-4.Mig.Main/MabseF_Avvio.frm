VERSION 5.00
Begin VB.Form MabseF_Avvio 
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   0  'None
   ClientHeight    =   3945
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6405
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "MabseF_Avvio.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   Picture         =   "MabseF_Avvio.frx":030A
   ScaleHeight     =   3945
   ScaleWidth      =   6405
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Left            =   5040
      Top             =   1920
   End
   Begin VB.Line Line4 
      BorderStyle     =   6  'Inside Solid
      X1              =   5
      X2              =   5
      Y1              =   0
      Y2              =   3950
   End
   Begin VB.Line Line2 
      BorderStyle     =   6  'Inside Solid
      X1              =   6390
      X2              =   6390
      Y1              =   5
      Y2              =   3950
   End
   Begin VB.Line Line3 
      X1              =   0
      X2              =   6390
      Y1              =   3935
      Y2              =   3935
   End
   Begin VB.Line Line1 
      BorderStyle     =   6  'Inside Solid
      X1              =   0
      X2              =   6395
      Y1              =   5
      Y2              =   5
   End
   Begin VB.Label lblVersion 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "version"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00E0E0E0&
      Height          =   270
      Left            =   4170
      TabIndex        =   2
      Top             =   2805
      Width           =   840
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Copyright i-4 s.r.l. 2011 - i-4.Migration Version 4.00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   15
      TabIndex        =   1
      Top             =   3720
      Width           =   6090
   End
   Begin VB.Shape Forma 
      BackColor       =   &H0080FF80&
      BackStyle       =   1  'Opaque
      Height          =   65
      Index           =   1
      Left            =   -30
      Top             =   3665
      Width           =   6645
   End
   Begin VB.Shape Forma 
      BackColor       =   &H00FF0000&
      BackStyle       =   1  'Opaque
      Height          =   60
      Index           =   0
      Left            =   -30
      Top             =   3600
      Width           =   6645
   End
   Begin VB.Label LblMsg 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   240
      TabIndex        =   0
      Top             =   5340
      Width           =   75
   End
End
Attribute VB_Name = "MabseF_Avvio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim i As Integer, ByIndex As Integer, ToIndex As Integer, StepIndex As Integer
Const LWA_ALPHA = &H2
Const GWL_EXSTYLE = (-20)
Const WS_EX_LAYERED = &H80000
Private Declare Function SetLayeredWindowAttributes Lib "user32" (ByVal hWnd As Long, ByVal crKey As Long, ByVal bAlpha As Byte, ByVal dwFlags As Long) As Long

Private Sub Connetti_SystemMDB()
  'valorizza le variabili
  Set ADOCntSys = New ADODB.Connection
  ADOCntSys.ConnectionString = "Provider=" & GbConnectionColl.Item(3) & ";Data Source=" & GbConnectionColl.Item(1) & _
      "\System\" & GbConnectionColl.Item(7) & ";" & "Persist Security Info=" & GbConnectionColl.Item(4)
  ADOCntSys.Open ADOCntSys.ConnectionString
  'setta DLLFunzioni
  Set DLLFunzioni.FnConnDBSys = ADOCntSys
End Sub

'Legge FILE_INI (tutto?!) in "GbConnectionColl"
Private Sub Carica_Parametri_Prodotto()
  Dim wRet As Long
  Dim wBuff As String
  Dim i As Integer
  Dim sApp As String
  Dim s() As String
  
  GbConnectionColl.Add GbPathPrd
  
  'PATH IMAGES (PARZIALE)
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("PATHS", "IMGPATH", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbPathImg = Left(wBuff, wRet)
  'EDITOR ESTERNO
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("TEXTEDT", "EXTERNALEDT", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbExternalEditor = Left(wBuff, wRet)
  
  ' silvia 10-1-2008
  'EDITOR COMPARE ESTERNO
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("TEXTEDT", "COMPARE-PARAM", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbCompareEditor = Left(wBuff, wRet)
  
  ReDim m_ObjType(0)
  
  For i = 1 To 100
    wBuff = String(100, Chr(10))
    wRet = GetPrivateProfileString("TYPESOURCE", CStr(i), "", wBuff, 100, GbPathPrd & FILE_INI)
    sApp = Left(wBuff, wRet)
    
    If Trim(sApp) <> "" Then
      s = Split(sApp, "#")
      ReDim Preserve m_ObjType(UBound(m_ObjType) + 1)
      m_ObjType(UBound(m_ObjType)).oLabel = s(0)
      m_ObjType(UBound(m_ObjType)).oType = s(1)
      m_ObjType(UBound(m_ObjType)).oArea = s(2)
      m_ObjType(UBound(m_ObjType)).oLevel = s(3)
      m_ObjType(UBound(m_ObjType)).oDescrizione = s(4)
      ' Mauro 11/02/2008 : Carica le estensioni dei file
      m_ObjType(UBound(m_ObjType)).oEstensione = s(5)
    End If
  Next i
  
  '******************************************************************************
  '**** Carica le variabili per le connessioni ADO ai Db sistema e progetto *****
  '******************************************************************************
  'PROVIDER SYS DB
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBCONNECTIONS", "SYS_PROVIDER", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbProvider_Sys = DLLFunzioni.GetProviderByType(Left(wBuff, wRet))
   
  GbConnectionColl.Add Left(wBuff, wRet)
  GbConnectionColl.Add GbProvider_Sys
   
  'SYS SECURITY INFO
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBCONNECTIONS", "SYS_SECURITYINFO", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbSecurityInfo_Sys = Left(wBuff, wRet)
  
  GbConnectionColl.Add GbSecurityInfo_Sys
  
  'SYS ID
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBCONNECTIONS", "SYS_ID", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbId_Sys = Left(wBuff, wRet)
  
  GbConnectionColl.Add GbId_Sys
  
  'SYS DATA SOURCE
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBCONNECTIONS", "SYS_DATASOURCE", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbDataSource_Sys = Left(wBuff, wRet)
  
  GbConnectionColl.Add GbDataSource_Sys
  
  'SYS DATABASE
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBCONNECTIONS", "SYS_DATABASE", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbDatabase_Sys = Left(wBuff, wRet)
  
  GbConnectionColl.Add GbDatabase_Sys
  
  'ACCESS PROVIDER
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROVIDER", "ACCESS", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbAccessProvider = Left(wBuff, wRet)
  'SQLSERVER PROVIDER
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROVIDER", "SQLSERVER", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbSqlServerProvider = Left(wBuff, wRet)

  'PATH SYS MDB
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("PATHS", "SYSMDBPATH", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbSysMdbPath = Left(wBuff, wRet)
End Sub

Private Sub Form_Load()
  Dim NomeFilic As String, NomePC As String
  Dim NumOK As Boolean, DataOK As Boolean
  Dim Ret As Long

  GbNomeProdotto = app.ProductName & " v" & app.Major & "." & app.Minor & "." & Format(app.Revision, "00")
  lblVersion.Caption = "v" & app.Major & "." & app.Minor & "." & Format(app.Revision, "00")
  
  Label4 = "Copyright " & "i-4 s.r.l. 2011-2012" & " - " & GbNomeProdotto
  'GbPathPrd: cos'� sto schifo?
  For i = 1 To Len(app.path)
   If Mid(app.path, Len(app.path) - i, 1) = "\" Then
     GbPathPrd = Left(app.path, Len(app.path) - i - 1)
     Exit For
   End If
  Next i
  'FILE "INI"
  If Len(Dir(GbPathPrd & FILE_INI)) = 0 Then
    MsgBox "Configuration file not found: " & GbPathPrd & FILE_INI & "." & vbCrLf & "Program exit.", vbExclamation, GbNomeProdotto
    End
  End If
  
  'Lettura FILE_INI
  Carica_Parametri_Prodotto
  
  Connetti_SystemMDB
    
  '''''''''''''''''''''''''''''
  ' File di controllo
  '''''''''''''''''''''''''''''
  If DllCTRLExist Then
    GbDllACTExist = DllACTExist(LblMsg, True)
     
    'Inizializza la DLL di attivazione
    If GbDllACTExist Then DllACTInit
     
    NomeFilic = DllACTExist_Step2(LblMsg, True)
     
    LblMsg.Caption = "User found [OK]..."
     
    GbNomeCliente = DLLACT.Restituisci_Nome_Cliente(GbPathPrd & "\" & SYSTEM_DIR & "\" & DLLACT.AcNomeFileLic)
  
    Ret = GetWindowLong(Me.hWnd, GWL_EXSTYLE)
    Ret = Ret Or WS_EX_LAYERED
    SetWindowLong Me.hWnd, GWL_EXSTYLE, Ret
    SetLayeredWindowAttributes Me.hWnd, 0, 0, LWA_ALPHA
    Me.Refresh
    Me.Show
       
    ByIndex = 0
    ToIndex = 255
    StepIndex = 1
      
    Timer1.Interval = 20000
    Timer1.Enabled = True
    For i = ByIndex To ToIndex Step StepIndex
      Timer1_Timer
    Next i
    
    i = 255
    SetLayeredWindowAttributes Me.hWnd, 0, i, LWA_ALPHA
     
    WaitTime 1
    
    'Controlla Id Number Prodotto
    DllCryInit
     
    If Controlla_PrdID(NomeFilic, LblMsg, True) Then
      GbRagSocCli = DLLACT.Restituisci_Nome_Cliente(GbPathPrd & "\" & SYSTEM_DIR & "\" & DLLACT.AcNomeFileLic)
      NomePC = GetComputerNameU
      GbComputerName = NomePC
      'In realt� viene comparato il seriale del disco
      NomePC = NomePC & getInformation
        
      Dim ctrlOff As Boolean
                   
      If UCase(Trim(NomePC)) = UCase(Trim(DLLLic.Lic_NomePC)) Then
        'PC OK:
        ctrlOff = False
        'Per PC: mai "per progetto"
      Else
        'PC non OK:
        If Left(DLLLic.Lic_NomePC, 9) = "SUNCOURSE" Then
          If Len(DLLLic.Lic_NomePC) = Len("SUNCOURSE") Then
            'OK
            ctrlOff = False
          Else
            'Per Progetto:
            namePrjLic = Mid(DLLLic.Lic_NomePC, 9 + 1)
            DLLLic.Lic_NomePC = Left(DLLLic.Lic_NomePC, 9)  'Ripristino NomePC
            If namePrjLic & ".lic" = NomeFilic Then
              'progetto OK
              ctrlOff = False
            Else
              'progetto non OK
              ctrlOff = True
              DLLFunzioni.Show_MsgBoxError "ML00E"
              'CTRL
              fixCtrl
              End 'lasciamo il controllo anche sotto... facciamo faticare un po' di pi�...
            End If
          End If
        Else
          'non OK
          ctrlOff = True
        End If
      End If
       
      If ctrlOff Then
        DLLFunzioni.Show_MsgBoxError "ML00E"
        'CTRL
        fixCtrl
        End
      End If
       
      'Controlla La data
      DataOK = Controlla_Data(DLLLic.Lic_DataInizio, DLLLic.Lic_DataFine)
   
      If Not DataOK Then
        DLLFunzioni.Show_MsgBoxError "ML01E"
        'CTRL
        fixCtrl
        End
      End If
      Unload Me
       
      MabseF_Prj.Show
    Else
      DLLFunzioni.Show_MsgBoxError "ML00E"
      'CTRL
      fixCtrl
      End
    End If
  Else
    'Non esiste il file
    DLLFunzioni.Show_MsgBoxError "ML01E"
    End
  End If
End Sub

Private Sub Timer1_Timer()
  Dim n As Long
  
  For n = 0 To 200000
  Next n
  SetLayeredWindowAttributes Me.hWnd, 0, i, LWA_ALPHA
  Me.Refresh
End Sub


