VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form MafndF_ListErr 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Relationships Error"
   ClientHeight    =   7320
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   9960
   Icon            =   "MafndF_ListErr.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7320
   ScaleWidth      =   9960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9630
      Top             =   -135
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_ListErr.frx":014A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_ListErr.frx":06E4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_ListErr.frx":0C7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_ListErr.frx":1558
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_ListErr.frx":1E32
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7320
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   12912
      _Version        =   393216
      TabOrientation  =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Relationship errors"
      TabPicture(0)   =   "MafndF_ListErr.frx":3B3C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "LwErr"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Toolbar1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Relationship corrector"
      TabPicture(1)   =   "MafndF_ListErr.frx":3B58
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraCorrector"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         Caption         =   "Selection criteria"
         ForeColor       =   &H00000080&
         Height          =   1455
         Left            =   75
         TabIndex        =   11
         Top             =   5460
         Width           =   9825
         Begin VB.OptionButton OptApp 
            Caption         =   "Application's Area"
            ForeColor       =   &H00C00000&
            Height          =   240
            Left            =   7860
            TabIndex        =   15
            Tag             =   "fixed"
            Top             =   675
            Width           =   1815
         End
         Begin VB.OptionButton OptPrj 
            Caption         =   "Project's Area"
            ForeColor       =   &H00C00000&
            Height          =   240
            Left            =   7860
            TabIndex        =   14
            Tag             =   "fixed"
            Top             =   315
            Width           =   1815
         End
         Begin VB.ComboBox CmbWeight 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   315
            ItemData        =   "MafndF_ListErr.frx":3B74
            Left            =   1605
            List            =   "MafndF_ListErr.frx":3B87
            TabIndex        =   13
            Tag             =   "fixed"
            Top             =   615
            Width           =   2055
         End
         Begin VB.ComboBox CmbError 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   315
            ItemData        =   "MafndF_ListErr.frx":3BB9
            Left            =   1605
            List            =   "MafndF_ListErr.frx":3BCC
            TabIndex        =   12
            Tag             =   "fixed"
            Top             =   300
            Width           =   4125
         End
         Begin VB.Label Label1 
            Caption         =   "Errors Found"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   6435
            TabIndex        =   20
            Tag             =   "fixed"
            Top             =   1133
            Width           =   1140
         End
         Begin VB.Label NumSelErr 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label4"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   300
            Left            =   7875
            TabIndex        =   19
            Tag             =   "fixed"
            Top             =   1080
            Width           =   1155
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Error Weight"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   180
            TabIndex        =   18
            Tag             =   "fixed"
            Top             =   675
            Width           =   1140
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Error Type"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   180
            TabIndex        =   17
            Tag             =   "fixed"
            Top             =   315
            Width           =   1140
         End
         Begin VB.Label lbParser 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   -1  'True
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   300
            Left            =   135
            TabIndex        =   16
            Top             =   1080
            Width           =   5625
         End
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   390
         Left            =   90
         TabIndex        =   10
         Tag             =   "fixed"
         Top             =   90
         Width           =   1185
         _ExtentX        =   2090
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         Appearance      =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   4
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "EXECUTE"
               Object.ToolTipText     =   "Execute Query"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PARSER1"
               Object.ToolTipText     =   "First Level Parser"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PARSER2"
               Object.ToolTipText     =   "Second Level Parser"
               ImageIndex      =   4
            EndProperty
         EndProperty
      End
      Begin VB.Frame fraCorrector 
         BorderStyle     =   0  'None
         Height          =   5025
         Left            =   -74880
         TabIndex        =   2
         Top             =   60
         Width           =   7875
         Begin VB.CommandButton cmdSave2 
            Caption         =   "Save"
            Height          =   405
            Left            =   6840
            TabIndex        =   9
            Top             =   4530
            Width           =   1005
         End
         Begin VB.CommandButton cmdSave 
            Caption         =   "Save"
            Height          =   405
            Left            =   6840
            TabIndex        =   6
            Top             =   2580
            Width           =   1005
         End
         Begin VB.Frame Frame4 
            Caption         =   "Duplicate relationship"
            ForeColor       =   &H00000080&
            Height          =   4965
            Left            =   2250
            TabIndex        =   4
            Top             =   0
            Width           =   4515
            Begin MSComctlLib.ListView lswRelDup 
               Height          =   2715
               Left            =   120
               TabIndex        =   7
               Top             =   270
               Width           =   4245
               _ExtentX        =   7488
               _ExtentY        =   4789
               SortKey         =   2
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   -1  'True
               HideSelection   =   0   'False
               Checkboxes      =   -1  'True
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   12582912
               BackColor       =   16777152
               BorderStyle     =   1
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               NumItems        =   4
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "Id"
                  Object.Width           =   1411
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Text            =   "Name"
                  Object.Width           =   2540
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Text            =   "Type"
                  Object.Width           =   1411
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   3
                  Text            =   "Path"
                  Object.Width           =   4410
               EndProperty
            End
            Begin MSComctlLib.ListView lswPaths 
               Height          =   1755
               Left            =   120
               TabIndex        =   8
               Top             =   3060
               Width           =   4245
               _ExtentX        =   7488
               _ExtentY        =   3096
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   -1  'True
               HideSelection   =   0   'False
               Checkboxes      =   -1  'True
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   12582912
               BackColor       =   16777152
               BorderStyle     =   1
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               NumItems        =   1
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "Path"
                  Object.Width           =   4410
               EndProperty
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "Objects List"
            ForeColor       =   &H00000080&
            Height          =   4965
            Left            =   30
            TabIndex        =   3
            Top             =   0
            Width           =   2145
            Begin MSComctlLib.ListView lswObjects 
               Height          =   4545
               Left            =   120
               TabIndex        =   5
               Top             =   270
               Width           =   1905
               _ExtentX        =   3360
               _ExtentY        =   8017
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   -1  'True
               HideSelection   =   0   'False
               HideColumnHeaders=   -1  'True
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               Icons           =   "ImageList1"
               SmallIcons      =   "ImageList1"
               ForeColor       =   12582912
               BackColor       =   -2147483624
               BorderStyle     =   1
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               NumItems        =   2
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "Id"
                  Object.Width           =   1764
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Text            =   "Name"
                  Object.Width           =   2540
               EndProperty
            End
         End
      End
      Begin MSComctlLib.ListView LwErr 
         Height          =   4890
         Left            =   30
         TabIndex        =   1
         Top             =   525
         Width           =   9885
         _ExtentX        =   17436
         _ExtentY        =   8625
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "ID Object"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Name Object"
            Object.Width           =   4304
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Error"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Area"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Weight"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Description"
            Object.Width           =   8819
         EndProperty
      End
   End
   Begin VB.Menu MnuEdit 
      Caption         =   "EditMenu"
      Visible         =   0   'False
      Begin VB.Menu Mnu_EditObj 
         Caption         =   "View Object"
      End
   End
End
Attribute VB_Name = "MafndF_ListErr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim OptType As String
Dim SwOpen As Boolean
Public formParent As Object

Public Sub Resize()
'  On Error Resume Next
'
'  m_DllFunctions.ResizeControls Me

End Sub

Private Sub cmdSave_Click()
    Dim rs As ADODB.Recordset
    Dim rsDet As ADODB.Recordset
    Dim wScelta As Long
    
    Screen.MousePointer = vbHourglass
    
    'salva
    m_DllFunctions.FnConnection.CommitTrans
    
    If relIsvalid(Val(lswObjects.SelectedItem.Text)) Then
        lswObjects.SelectedItem.SmallIcon = 2
    Else
        lswObjects.SelectedItem.SmallIcon = 1
        
        wScelta = MsgBox("This Source does not have a completly relationships resolved... Do you want to continue with saving this relationships (Y/N)?", vbYesNo, m_DllFunctions.FnNomeProdotto)
    
        If wScelta = vbNo Then
            m_DllFunctions.FnConnection.BeginTrans
        
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
    End If
    
    write_relazioni Val(lswObjects.SelectedItem.Text)
    
    m_DllFunctions.FnConnection.BeginTrans
    
    Screen.MousePointer = vbDefault
End Sub

Public Sub write_relazioni(cid As Long)
    Dim rs As ADODB.Recordset
    Dim rsDet As ADODB.Recordset
    
    'Trasporta i record nella tabella delle relazioni
    Set rs = m_DllFunctions.Open_Recordset("select * from PsRel_ObjDup Where IdOggettoC = " & cid)
    
    If rs.RecordCount > 0 Then
        While Not rs.EOF
            
            If rs!Valida Then
                Set rsDet = m_DllFunctions.Open_Recordset("Select * From PsRel_Obj Where IdOggettoc = " & rs!IdOggettoC & " And IdOggettoR = " & rs!IdOggettoR)
                
                If rsDet.RecordCount = 0 Then
                    rsDet.AddNew
                    
                    rsDet!IdOggettoC = rs!IdOggettoC
                    rsDet!IdOggettoR = rs!IdOggettoR
                    rsDet!Relazione = rs!Relazione
                    rsDet!nomecomponente = rs!nomecomponente
                    rsDet!Utilizzo = rs!Utilizzo
                    
                    rsDet.Update
                End If
                
                rsDet.Close
            Else
                m_DllFunctions.FnConnection.Execute "Delete * From PsRel_Obj where IdOggettoC = " & cid & " And IdOggettoR = " & rs!IdOggettoR
            End If
            
            rs.MoveNext
        Wend
    End If
    
    rs.Close
End Sub

Private Sub cmdSave2_Click()
    'Deve :
    '
    '1) Prendere le directory
    '2) Cominciare a scrivere le relazioni solo per quelle che matchano con le directory
    Dim rs As ADODB.Recordset
    Dim rsObj As ADODB.Recordset
    Dim wPath As String
    Dim i As Long
    Dim wExist As Boolean
    
    Set rs = m_DllFunctions.Open_Recordset("select IdOggetto,nome from Bs_Oggetti Where idOggetto IN(Select distinct(IdOggettoC) From PsRel_ObjDup order by IdOggettoC)")
    
    If rs.RecordCount > 0 Then
        rs.MoveFirst
        
        While Not rs.EOF
            Screen.MousePointer = vbHourglass
            
            Set rsObj = m_DllFunctions.Open_Recordset("Select * From psRel_ObjDup Where IdOggettoC = " & rs!IdOggetto & " Order by NomeComponente")
    
            If rsObj.RecordCount > 0 Then
                 rsObj.MoveFirst
                
                While Not rsObj.EOF
                    wPath = Replace(m_DllFunctions.Restituisci_Directory_Da_IdOggetto(rsObj!IdOggettoR), m_DllFunctions.FnPathPrj, "...")
                    
                    'scrive la relazione se fa parte delle directory segnate
                    wExist = False
                    
                    For i = 1 To lswPaths.ListItems.Count
                        If Trim(UCase(lswPaths.ListItems(i).Text)) = Trim(UCase(wPath)) Then
                            If lswPaths.ListItems(i).Checked = True Then
                                wExist = True
                                Exit For
                            End If
                        End If
                    Next i
                    
                    If wExist Then
                        'Scrive sta benedetta relazione
                        check_dbrel rsObj!IdOggettoC, rsObj!IdOggettoR, wExist
                        
                        write_relazioni rs!IdOggetto
                    End If
                    
                    rsObj.MoveNext
                Wend
            End If
            
             rsObj.Close
            
            rs.MoveNext
            
            Screen.MousePointer = vbDefault
        Wend
    End If
    
    rs.Close
    
    For i = 1 To lswObjects.ListItems.Count
        If relIsvalid(Val(lswObjects.ListItems(i))) Then
            lswObjects.ListItems(i).SmallIcon = 2
        Else
            lswObjects.ListItems(i).SmallIcon = 1
        End If
    Next i
    

End Sub



Private Sub Form_Activate()
  
  Me.Refresh
  OptApp.Value = True

End Sub

Public Sub listErr_Load()
  
  NumSelErr = 0
  OptType = ""
  
  load_ObjectsList
  
  lswRelDup.ColumnHeaders(4).Width = lswRelDup.Width - lswRelDup.ColumnHeaders(1).Width - lswRelDup.ColumnHeaders(2).Width - lswRelDup.ColumnHeaders(1).Width - 270
  
  m_DllFunctions.AddActiveWindows Me
  m_DllFunctions.FnActiveWindowsBool = True
  
  'Comincia la transazione
  m_DllFunctions.FnConnection.BeginTrans

End Sub


'Private Sub Form_Load()
'  'SetParent Me.hwnd, m_DllFunctions.FnParent
'  'Resize
'  Set formParent = formParent
'
'  NumSelErr = 0
'  OptType = ""
'
'  load_ObjectsList
'
'  lswRelDup.ColumnHeaders(4).Width = lswRelDup.Width - lswRelDup.ColumnHeaders(1).Width - lswRelDup.ColumnHeaders(2).Width - lswRelDup.ColumnHeaders(1).Width - 270
'
'  m_DllFunctions.AddActiveWindows Me
'  m_DllFunctions.FnActiveWindowsBool = True
'
'  'Comincia la transazione
'  m_DllFunctions.FnConnection.BeginTrans
'End Sub

Sub CaricaCombo()

  Dim tb As ADODB.Recordset
  Dim Tb1 As ADODB.Recordset
  Dim wStr As String
  
  
  CmbError.Clear
  CmbError.AddItem "*All"
  CmbWeight.Clear
  CmbWeight.AddItem "*All"
  
  If OptType = "A" Then
    Set tb = m_DllFunctions.Open_Recordset("select distinct codice from bs_segnalazioni order by codice")
  Else
    Set tb = m_DllFunctions.Open_Recordset("select distinct codice from bs_segnalazioniprj order by codice")
  End If
  If tb.RecordCount = 0 Then Exit Sub
  tb.MoveFirst
  While Not tb.EOF
     Set Tb1 = m_DllFunctions.Open_Recordset_Sys("select * from bs_messaggi where codice = '" & tb!Codice & "' ")
     wStr = Tb1!Testo
     CmbError.AddItem tb!Codice & " - " & wStr
     tb.MoveNext
  Wend
  
  
  If OptType = "A" Then
     Set tb = m_DllFunctions.Open_Recordset("select distinct gravita from bs_segnalazioni order by gravita")
  Else
     Set tb = m_DllFunctions.Open_Recordset("select distinct gravita from bs_segnalazioniprj order by gravita")
  End If
  If tb.RecordCount = 0 Then Exit Sub
  tb.MoveFirst
  While Not tb.EOF
     Select Case tb!gravita
       Case "I"
          wStr = "I - Informational"
       Case "W"
          wStr = "W - Warning"
       Case "E"
          wStr = "E - Error"
       Case "S"
          wStr = "S - Sever"
       Case Else
          wStr = tb!gravita & " - "
     End Select
     CmbWeight.AddItem wStr
     tb.MoveNext
  Wend
    
  CmbError.ListIndex = 0
  CmbWeight.ListIndex = 0
   
  Set tb = Nothing
  Set Tb1 = Nothing

End Sub
Sub CaricaErr()
  Dim tb As ADODB.Recordset
  Dim Tb1 As ADODB.Recordset
  Dim wStr As String
  Dim wSQL As String
  Dim wWhere As String
  Dim k As Integer
  Dim SwShow As Boolean
  Dim Y As Integer
  
  LwErr.ListItems.Clear
  
  NumSelErr = 0
  If CmbError.ListCount = 1 Then Exit Sub
  'If Not SwOpen Then Exit Sub
  
  wWhere = ""
  wSQL = "select distinct a.idoggetto, b.nome, a.codice, a.tipologia, a.gravita, a.var1, a.var2 from bs_segnalazioni as a, bs_oggetti as b where (a.idoggetto = b.idoggetto) "
  If OptType = "A" Then
    wSQL = wSQL & " "
  Else
    wSQL = wSQL & "Prj "
  End If
  
  If CmbError.Text <> "*All" Then
    k = InStr(CmbError.Text, "-")
    wStr = Trim(Mid$(CmbError.Text, 1, k - 1))
    wWhere = wWhere & " Codice = '" & wStr & "' "
  End If
  
  If CmbWeight.Text <> "*All" Then
    k = InStr(CmbWeight.Text, "-")
    wStr = Trim(Mid$(CmbWeight.Text, 1, k - 1))
    If Trim(wWhere) <> "" Then wWhere = wWhere & " and "
    wWhere = wWhere & " gravita = '" & wStr & "' "
  End If
  
  If Trim(wWhere) <> "" Then
    wSQL = wSQL & "AND " & wWhere
  End If
  
  'wSQL = wSQL & "order by idoggetto, var1"
  
  Set tb = m_DllFunctions.Open_Recordset(wSQL)
  While Not tb.EOF
     For Y = 0 To CmbError.ListCount - 1
       If tb!Codice = Left(CmbError.List(Y), 3) Then
          wStr = Mid(CmbError.List(Y), 5)
          Exit For
       End If
     Next Y
        With LwErr.ListItems.Add(, , tb!IdOggetto)
           .SubItems(1) = tb!Nome
           .SubItems(2) = tb!Codice
           .SubItems(3) = tb!Tipologia & ""
           .SubItems(4) = tb!gravita
           If Trim(tb!Var1) <> "" Then
              wStr = Replace(wStr, "%VAR1%", tb!Var1)
           End If
           If Trim(tb!Var2) <> "" Then
              wStr = Replace(wStr, "%VAR2%", tb!Var2)
           End If
           .SubItems(5) = wStr
        End With
     tb.MoveNext
  Wend
  Set tb = Nothing
    
  NumSelErr = LwErr.ListItems.Count
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
   Dim wScelta As Long
   
   If m_DllFunctions.FnProcessRunning Then
      wScelta = m_DllFunctions.Show_MsgBoxError("FB01I")
      If wScelta = vbNo Then
         Cancel = 0 'No unload
         m_DllFunctions.FnStopProcess = False
      Else
         Cancel = 1 'Unload
         m_DllFunctions.FnStopProcess = True
      End If
   End If
   
   m_DllFunctions.FnConnection.CommitTrans
End Sub

Private Sub Form_Resize()
    ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  SwOpen = False
  m_DllFunctions.RemoveActiveWindows Me
  m_DllFunctions.FnActiveWindowsBool = False
  
  SetFocusWnd m_DllFunctions.FnParent
End Sub

Private Sub lswObjects_Click()
    Dim rs As ADODB.Recordset
    Dim wPath As String
    If lswObjects.ListItems.Count > 0 Then
    
    lswRelDup.Sorted = False
    lswRelDup.ListItems.Clear
    
    Set rs = m_DllFunctions.Open_Recordset("Select * From psRel_ObjDup Where IdOggettoC = " & Val(lswObjects.SelectedItem.Text & " Order by NomeComponente"))
    
    If rs.RecordCount > 0 Then
        rs.MoveFirst
        
        While Not rs.EOF
            wPath = Replace(m_DllFunctions.Restituisci_Directory_Da_IdOggetto(rs!IdOggettoR), m_DllFunctions.FnPathPrj, "...")
            lswRelDup.ListItems.Add , , rs!IdOggettoR
            lswRelDup.ListItems(lswRelDup.ListItems.Count).ListSubItems.Add , , rs!nomecomponente
            lswRelDup.ListItems(lswRelDup.ListItems.Count).ListSubItems.Add , , rs!Utilizzo
            lswRelDup.ListItems(lswRelDup.ListItems.Count).ListSubItems.Add , , wPath
            
            If rs!Valida Then
                lswRelDup.ListItems(lswRelDup.ListItems.Count).Checked = True
            End If
            
            rs.MoveNext
        Wend
    End If
    
    rs.Close
    End If
End Sub



Private Sub lswRelDup_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
 'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswRelDup.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswRelDup.SortOrder = Order
  lswRelDup.SortKey = Key - 1
  lswRelDup.Sorted = True
End Sub

Private Sub lswRelDup_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    'Sflagga altri item con lo stesso nome
    Dim i As Long
   
    For i = 1 To lswRelDup.ListItems.Count
        If Trim(UCase(lswRelDup.ListItems(i).ListSubItems(1).Text)) = Trim(UCase(Item.ListSubItems(1).Text)) Then
            If i <> Item.Index Then
                If lswRelDup.ListItems(i).Checked = True Then
                    lswRelDup.ListItems(i).Checked = False
                End If
            End If
        End If
    Next i
    
    check_dbrel Val(lswObjects.SelectedItem.Text), Val(Item.Text), Item.Checked
    
End Sub

Public Sub check_dbrel(idObjC As Long, idObjR As Long, cValue As Boolean)
    Dim rs As ADODB.Recordset
    
    Set rs = m_DllFunctions.Open_Recordset("select * From Psrel_Objdup  Where IdOggettoC = " & idObjC & " And IdoggettoR = " & idObjR)
    
    If rs.RecordCount > 0 Then
        rs!Valida = cValue
        rs.Update
    End If
    
    rs.Close
End Sub

Private Sub LwErr_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   Static sOrder
   sOrder = Not sOrder
   
   'Usa l'ordinamento standard per ordinare gli items
   LwErr.SortKey = ColumnHeader.Index - 1
   LwErr.SortOrder = Abs(sOrder)
   LwErr.Sorted = True
End Sub

Private Sub LwErr_DblClick()
  If Not (LwErr.SelectedItem Is Nothing) Then
    m_DllFunctions.Show_TextEditor LwErr.SelectedItem.Text, , , MabseF_List
  End If
End Sub


'MAURO - 02/12/2005
'Private Sub LwErr_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'  If Button = 2 Then
'      PopupMenu MnuEdit
'  End If
'End Sub

'Private Sub Mnu_EditObj_Click()
'Dim wIdObj As Long
'wIdObj = CLng(LwErr.SelectedItem.Text)
'm_DllFunctions.Show_TextEditor wIdObj
'End Sub

Private Sub OptApp_Click()
m_DllFunctions.FnProcessRunning = True
Screen.MousePointer = vbHourglass

If OptApp.Value = True Then
  If OptType <> "A" Then
     OptType = "A"
     CaricaCombo
  End If
End If

Screen.MousePointer = vbNormal
m_DllFunctions.FnProcessRunning = False

End Sub
Private Sub OptPrj_Click()
  m_DllFunctions.FnProcessRunning = True
  Screen.MousePointer = vbHourglass
  
  If OptPrj.Value Then
    If OptType <> "P" Then
       OptType = "P"
       CaricaCombo
    End If
  End If
  
  Screen.MousePointer = vbNormal
  m_DllFunctions.FnProcessRunning = False

End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    If SSTab1.Tab = 1 Then
        fraCorrector.Move 50, 50
    End If
End Sub

Public Sub load_ObjectsList()
    Dim rs As ADODB.Recordset
    Dim wPath As String
    
    lswObjects.ListItems.Clear
    
    Set rs = m_DllFunctions.Open_Recordset("select IdOggetto,nome from Bs_Oggetti Where idOggetto IN(Select distinct(IdOggettoC) From PsRel_ObjDup order by IdOggettoC)")
    
    If rs.RecordCount > 0 Then
        rs.MoveFirst
        
        While Not rs.EOF
            Screen.MousePointer = vbHourglass
            
            If relIsvalid(rs!IdOggetto) Then
                lswObjects.ListItems.Add , , rs!IdOggetto, , 2
            Else
                lswObjects.ListItems.Add , , rs!IdOggetto, , 1
            End If
            
            lswObjects.ListItems(lswObjects.ListItems.Count).ListSubItems.Add , , rs!Nome
            
            rs.MoveNext
            
            Screen.MousePointer = vbDefault
        Wend
    End If
    
    rs.Close
    
    'Carica i path
    Set rs = m_DllFunctions.Open_Recordset("select distinct Directory_Input from Bs_Oggetti where IdOggetto IN  (Select IdOggettoR From psRel_ObjDup)")
    
    If rs.RecordCount > 0 Then
        rs.MoveFirst
        
        lswPaths.ListItems.Clear
        
        While Not rs.EOF
            Screen.MousePointer = vbHourglass
            
            wPath = m_DllFunctions.Crea_Directory_Parametrica(m_DllFunctions.FnPathPrj)
            
            wPath = Replace(rs!Directory_Input, wPath, "...")
            
            lswPaths.ListItems.Add , , wPath
            
            rs.MoveNext
            
            Screen.MousePointer = vbDefault
        Wend
    End If
    
    rs.Close
    
End Sub

Public Function relIsvalid(idObj As Long) As Boolean
    Dim rs As ADODB.Recordset
    Dim rsDet As ADODB.Recordset
    
    Dim bValid As Boolean
    Dim oldNome As String
    
    Set rs = m_DllFunctions.Open_Recordset("Select distinct nomecomponente from PsRel_ObjDup Where IdOggettoc = " & idObj & " Order by NomeComponente")
    
    If rs.RecordCount > 0 Then
        rs.MoveFirst
       
        Do While Not rs.EOF
            Set rsDet = m_DllFunctions.Open_Recordset("Select * From PsRel_ObjDup Where IdOggettoc = " & idObj & " and nomeComponente = '" & rs!nomecomponente & "'")
            
            If rsDet.RecordCount > 0 Then
                bValid = False
                
                Do While Not rsDet.EOF
                    If rsDet!Valida = True Then
                        bValid = True
                        Exit Do
                    End If
                    
                    rsDet.MoveNext
                Loop
            End If
            
            rsDet.Close
            
            If bValid = False Then
                Exit Do
            End If
            
            rs.MoveNext
        Loop
    End If
    
    rs.Close
    
    relIsvalid = bValid
End Function

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
  
  Case "EXECUTE"
      m_DllFunctions.FnProcessRunning = True
      SwOpen = True
      Screen.MousePointer = vbHourglass
      CaricaErr
      Screen.MousePointer = vbNormal
      m_DllFunctions.FnProcessRunning = False
  Case "PARSER1"
      parser1
  Case "PARSER2"
      parser2
  End Select
  
End Sub

Public Sub parser1()
   'Parser degli oggetti selezionati
   Dim i As Long
   Dim cParam As Collection
   
   On Error GoTo errorHandler
   lbParser.Caption = ""
   If DLLFunzioni.FnProcessRunning Then
      DLLFunzioni.Show_MsgBoxError "FB01I"
      Exit Sub
   End If
   
   Screen.MousePointer = vbHourglass
   
   DLLFunzioni.FnActiveWindowsBool = True
   DLLFunzioni.FnProcessRunning = True
   
   '23-01-06: faceva dentro il parsing 1000 volte le stesse cose...
   DllParser.AttivaFunzione "INIT_PARSING", cParam
   For i = 1 To LwErr.ListItems.Count
      If LwErr.ListItems(i).Selected Then
         Set cParam = New Collection
         cParam.Add CLng(LwErr.ListItems(i).Text)
         DLLFunzioni.FnActiveWindowsBool = False
         DllParser.AttivaFunzione "PARSE_OBJECT_FIRST_LEVEL", cParam
         DLLFunzioni.FnActiveWindowsBool = True
      End If
   Next i
   
   DLLFunzioni.FnActiveWindowsBool = False
   DLLFunzioni.FnProcessRunning = False
   
   Screen.MousePointer = vbDefault
   lbParser.Caption = "First Level Parsing concluded"
   Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "ML04E"
  lbParser.Caption = "First Level Parsing NOT concluded"
End Sub

Public Sub parser2()
  'Parser degli oggetti selezionati
  Dim i As Long
  Dim cParam As Collection
  
  On Error GoTo errorHandler
  
  lbParser.Caption = ""
  If DLLFunzioni.FnProcessRunning Then
     DLLFunzioni.Show_MsgBoxError "FB01I"
     Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  
   '23-01-06: faceva dentro il parsing 1000 volte le stesse cose...
  DllParser.AttivaFunzione "INIT_PARSING", cParam
  For i = 1 To LwErr.ListItems.Count
    If LwErr.ListItems(i).Selected Then
      Set cParam = New Collection
      cParam.Add CLng(LwErr.ListItems(i).Text)
      DLLFunzioni.FnActiveWindowsBool = False
      DllParser.AttivaFunzione "PARSE_OBJECT_SECOND_LEVEL", cParam
      DLLFunzioni.FnActiveWindowsBool = True
    End If
  Next i
  
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  lbParser.Caption = "Second Level Parsing concluded"
  
     Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "ML04E"
  lbParser.Caption = "Second Level Parsing NOT concluded"
End Sub
