VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form MabsdF_ListErr 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Project Exceptions"
   ClientHeight    =   7305
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   9975
   Icon            =   "MabsdF_ListErr.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7305
   ScaleWidth      =   9975
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   9360
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabsdF_ListErr.frx":014A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabsdF_ListErr.frx":06E4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabsdF_ListErr.frx":0C7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabsdF_ListErr.frx":1558
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabsdF_ListErr.frx":1E32
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabsdF_ListErr.frx":3B3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabsdF_ListErr.frx":3C96
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7320
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   12912
      _Version        =   393216
      TabOrientation  =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Relationship errors"
      TabPicture(0)   =   "MabsdF_ListErr.frx":4230
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "LwErr"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Toolbar1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Relationship corrector"
      TabPicture(1)   =   "MabsdF_ListErr.frx":424C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraCorrector"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         Caption         =   "Selection criteria"
         ForeColor       =   &H00000080&
         Height          =   1455
         Left            =   120
         TabIndex        =   6
         Top             =   5460
         Width           =   9765
         Begin VB.CheckBox ChkFlagIgnore 
            Caption         =   "Show Ignore"
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   8160
            TabIndex        =   19
            Top             =   1025
            Width           =   1260
         End
         Begin VB.OptionButton OptApp 
            Caption         =   "Application's Area"
            ForeColor       =   &H00C00000&
            Height          =   240
            Left            =   8150
            TabIndex        =   18
            Tag             =   "fixed"
            Top             =   675
            Width           =   1560
         End
         Begin VB.OptionButton OptPrj 
            Caption         =   "Project's Area"
            ForeColor       =   &H00C00000&
            Height          =   240
            Left            =   8150
            TabIndex        =   17
            Tag             =   "fixed"
            Top             =   315
            Width           =   1320
         End
         Begin VB.ComboBox CmbWeight 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   315
            ItemData        =   "MabsdF_ListErr.frx":4268
            Left            =   1605
            List            =   "MabsdF_ListErr.frx":427B
            TabIndex        =   10
            Tag             =   "fixed"
            Top             =   600
            Width           =   2055
         End
         Begin VB.ComboBox CmbError 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   315
            ItemData        =   "MabsdF_ListErr.frx":42AD
            Left            =   1605
            List            =   "MabsdF_ListErr.frx":42C0
            TabIndex        =   8
            Tag             =   "fixed"
            Top             =   300
            Width           =   4125
         End
         Begin VB.Label NumSelIgn 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label5"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   300
            Left            =   6900
            TabIndex        =   16
            Tag             =   "fixed"
            Top             =   860
            Width           =   1005
         End
         Begin VB.Label Label5 
            Caption         =   "Ignored"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   5895
            TabIndex        =   15
            Tag             =   "fixed"
            Top             =   900
            Width           =   675
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Messages"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   240
            TabIndex        =   11
            Top             =   1050
            Width           =   720
         End
         Begin VB.Label Label1 
            Caption         =   "Errors Found"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   5895
            TabIndex        =   13
            Tag             =   "fixed"
            Top             =   345
            Width           =   960
         End
         Begin VB.Label NumSelErr 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label4"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   300
            Left            =   6900
            TabIndex        =   14
            Tag             =   "fixed"
            Top             =   300
            Width           =   1005
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Error Weight"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   180
            TabIndex        =   9
            Tag             =   "fixed"
            Top             =   675
            Width           =   1140
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Error Type"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   240
            TabIndex        =   7
            Tag             =   "fixed"
            Top             =   315
            Width           =   1140
         End
         Begin VB.Label lbParser 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFC0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   -1  'True
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   1605
            TabIndex        =   12
            Top             =   1050
            Width           =   15
         End
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   390
         Left            =   120
         TabIndex        =   5
         Tag             =   "fixed"
         Top             =   120
         Width           =   2865
         _ExtentX        =   5054
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         Appearance      =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   8
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "EXECUTE"
               Object.ToolTipText     =   "Execute Query"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PARSER1"
               Object.ToolTipText     =   "First Level Parser"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PARSER2"
               Object.ToolTipText     =   "Second Level Parser"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DELETE"
               Description     =   "Delete Objects"
               Object.ToolTipText     =   "Delete List of Objects..."
               ImageIndex      =   6
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   3
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "DELETE"
                     Object.Tag             =   "Name Object"
                     Text            =   "Name Object"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "DELETE_OBJLST"
                     Object.Tag             =   "Name Object List"
                     Text            =   "Name Object List..."
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "DELETE_CALLST"
                     Object.Tag             =   "Called Module List"
                     Text            =   "Called Module List..."
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "IGNORE"
               Object.ToolTipText     =   "Ignore this kind of Error"
               ImageIndex      =   7
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "IGNORE"
                     Object.Tag             =   "Ignore"
                     Text            =   "Ignore"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "UNIGNORE"
                     Object.Tag             =   "Unignore"
                     Text            =   "Unignore"
                  EndProperty
               EndProperty
            EndProperty
         EndProperty
      End
      Begin VB.Frame fraCorrector 
         BorderStyle     =   0  'None
         Height          =   6825
         Left            =   -74880
         TabIndex        =   2
         Top             =   60
         Width           =   9735
         Begin TabDlg.SSTab SSTab2 
            Height          =   6615
            Left            =   2880
            TabIndex        =   20
            Top             =   120
            Width           =   6855
            _ExtentX        =   12091
            _ExtentY        =   11668
            _Version        =   393216
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Object Relationship"
            TabPicture(0)   =   "MabsdF_ListErr.frx":42F2
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "Frame2"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "Frame4"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).ControlCount=   2
            TabCaption(1)   =   "Segnalation Detail"
            TabPicture(1)   =   "MabsdF_ListErr.frx":430E
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "Label9"
            Tab(1).Control(1)=   "Label10"
            Tab(1).Control(2)=   "Label11"
            Tab(1).Control(3)=   "Label12"
            Tab(1).Control(4)=   "Label13"
            Tab(1).Control(5)=   "Label14"
            Tab(1).Control(6)=   "Label15"
            Tab(1).Control(7)=   "Label16"
            Tab(1).Control(8)=   "Label17"
            Tab(1).Control(9)=   "Label18"
            Tab(1).Control(10)=   "txtIdOggetto"
            Tab(1).Control(11)=   "txtIdOggettoR"
            Tab(1).Control(12)=   "txtRiga"
            Tab(1).Control(13)=   "txtCodice"
            Tab(1).Control(14)=   "txtOrigine"
            Tab(1).Control(15)=   "txtDataSegnalaz"
            Tab(1).Control(16)=   "txtTipologia"
            Tab(1).Control(17)=   "txtGravita"
            Tab(1).Control(18)=   "txtVar1"
            Tab(1).Control(19)=   "txtVar2"
            Tab(1).ControlCount=   20
            Begin VB.TextBox txtVar2 
               ForeColor       =   &H00C00000&
               Height          =   285
               Left            =   -73560
               TabIndex        =   55
               Top             =   5400
               Width           =   4815
            End
            Begin VB.TextBox txtVar1 
               ForeColor       =   &H00C00000&
               Height          =   285
               Left            =   -73560
               TabIndex        =   53
               Top             =   4903
               Width           =   4815
            End
            Begin VB.TextBox txtGravita 
               ForeColor       =   &H00C00000&
               Height          =   285
               Left            =   -73560
               TabIndex        =   51
               Top             =   4412
               Width           =   4815
            End
            Begin VB.TextBox txtTipologia 
               ForeColor       =   &H00C00000&
               Height          =   285
               Left            =   -73560
               TabIndex        =   49
               Top             =   3921
               Width           =   4815
            End
            Begin VB.TextBox txtDataSegnalaz 
               ForeColor       =   &H00C00000&
               Height          =   285
               Left            =   -73560
               TabIndex        =   47
               Top             =   3430
               Width           =   4815
            End
            Begin VB.TextBox txtOrigine 
               ForeColor       =   &H00C00000&
               Height          =   285
               Left            =   -73560
               TabIndex        =   45
               Top             =   2939
               Width           =   4815
            End
            Begin VB.TextBox txtCodice 
               ForeColor       =   &H00C00000&
               Height          =   285
               Left            =   -73560
               TabIndex        =   43
               Top             =   2448
               Width           =   4815
            End
            Begin VB.TextBox txtRiga 
               ForeColor       =   &H00C00000&
               Height          =   285
               Left            =   -73560
               TabIndex        =   41
               Top             =   1957
               Width           =   4815
            End
            Begin VB.TextBox txtIdOggettoR 
               ForeColor       =   &H00C00000&
               Height          =   285
               Left            =   -73560
               TabIndex        =   39
               Top             =   1466
               Width           =   4815
            End
            Begin VB.TextBox txtIdOggetto 
               ForeColor       =   &H00C00000&
               Height          =   285
               Left            =   -73560
               TabIndex        =   37
               Top             =   975
               Width           =   4815
            End
            Begin VB.Frame Frame4 
               Caption         =   "Duplicate relationship"
               ForeColor       =   &H00000080&
               Height          =   2565
               Left            =   120
               TabIndex        =   32
               Top             =   360
               Width           =   6675
               Begin VB.CommandButton cmdSave2 
                  Caption         =   "Save"
                  Height          =   435
                  Left            =   5640
                  TabIndex        =   34
                  Top             =   1380
                  Width           =   705
               End
               Begin VB.CommandButton cmdSave 
                  Caption         =   "Save"
                  Height          =   405
                  Left            =   5640
                  TabIndex        =   33
                  Top             =   270
                  Width           =   705
               End
               Begin MSComctlLib.ListView lswRelDup 
                  Height          =   1035
                  Left            =   120
                  TabIndex        =   35
                  Top             =   240
                  Width           =   5325
                  _ExtentX        =   9393
                  _ExtentY        =   1826
                  View            =   3
                  LabelEdit       =   1
                  LabelWrap       =   0   'False
                  HideSelection   =   0   'False
                  AllowReorder    =   -1  'True
                  FullRowSelect   =   -1  'True
                  GridLines       =   -1  'True
                  _Version        =   393217
                  ForeColor       =   12582912
                  BackColor       =   16777152
                  BorderStyle     =   1
                  Appearance      =   1
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  NumItems        =   4
                  BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     Text            =   "Id"
                     Object.Width           =   1411
                  EndProperty
                  BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   1
                     Text            =   "Name"
                     Object.Width           =   2540
                  EndProperty
                  BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   2
                     Text            =   "Use"
                     Object.Width           =   2540
                  EndProperty
                  BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   3
                     Text            =   "Path"
                     Object.Width           =   4410
                  EndProperty
               End
               Begin MSComctlLib.ListView lswPaths 
                  Height          =   1035
                  Left            =   120
                  TabIndex        =   36
                  Top             =   1320
                  Width           =   5325
                  _ExtentX        =   9393
                  _ExtentY        =   1826
                  View            =   3
                  LabelEdit       =   1
                  LabelWrap       =   0   'False
                  HideSelection   =   0   'False
                  AllowReorder    =   -1  'True
                  Checkboxes      =   -1  'True
                  FullRowSelect   =   -1  'True
                  GridLines       =   -1  'True
                  _Version        =   393217
                  ForeColor       =   12582912
                  BackColor       =   16777152
                  BorderStyle     =   1
                  Appearance      =   1
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  NumItems        =   1
                  BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     Text            =   "Path"
                     Object.Width           =   4410
                  EndProperty
               End
            End
            Begin VB.Frame Frame2 
               Caption         =   "Object relationship"
               ForeColor       =   &H00000080&
               Height          =   3525
               Left            =   120
               TabIndex        =   21
               Top             =   3000
               Width           =   6675
               Begin VB.CommandButton cmdSave3 
                  Caption         =   "Save"
                  Height          =   405
                  Left            =   5520
                  TabIndex        =   26
                  Top             =   2880
                  Width           =   825
               End
               Begin VB.CommandButton cmdRemove 
                  Caption         =   "Remove"
                  Height          =   405
                  Left            =   5520
                  TabIndex        =   25
                  Top             =   1080
                  Width           =   825
               End
               Begin VB.ComboBox cmbType 
                  Height          =   315
                  Left            =   180
                  TabIndex        =   24
                  Top             =   1800
                  Width           =   2055
               End
               Begin VB.TextBox txtNome 
                  Height          =   285
                  Left            =   2460
                  TabIndex        =   23
                  Top             =   1830
                  Width           =   1815
               End
               Begin VB.CommandButton cmdQuery 
                  Height          =   375
                  Left            =   4380
                  Picture         =   "MabsdF_ListErr.frx":432A
                  Style           =   1  'Graphical
                  TabIndex        =   22
                  Top             =   1740
                  Width           =   435
               End
               Begin MSComctlLib.ListView lswRelation 
                  Height          =   1155
                  Left            =   120
                  TabIndex        =   27
                  Top             =   360
                  Width           =   5325
                  _ExtentX        =   9393
                  _ExtentY        =   2037
                  SortKey         =   2
                  View            =   3
                  LabelEdit       =   1
                  MultiSelect     =   -1  'True
                  LabelWrap       =   0   'False
                  HideSelection   =   0   'False
                  AllowReorder    =   -1  'True
                  FullRowSelect   =   -1  'True
                  GridLines       =   -1  'True
                  _Version        =   393217
                  ForeColor       =   12582912
                  BackColor       =   16777152
                  BorderStyle     =   1
                  Appearance      =   1
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  NumItems        =   5
                  BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     Text            =   "Id"
                     Object.Width           =   1411
                  EndProperty
                  BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   1
                     Text            =   "Name"
                     Object.Width           =   2540
                  EndProperty
                  BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   2
                     Text            =   "Type"
                     Object.Width           =   1411
                  EndProperty
                  BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   3
                     Text            =   "Use"
                     Object.Width           =   2540
                  EndProperty
                  BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   4
                     Text            =   "Path"
                     Object.Width           =   4410
                  EndProperty
               End
               Begin MSComctlLib.ListView lswRelObjects 
                  Height          =   1155
                  Left            =   120
                  TabIndex        =   28
                  Top             =   2160
                  Width           =   5325
                  _ExtentX        =   9393
                  _ExtentY        =   2037
                  SortKey         =   2
                  View            =   3
                  LabelEdit       =   1
                  MultiSelect     =   -1  'True
                  LabelWrap       =   0   'False
                  HideSelection   =   0   'False
                  AllowReorder    =   -1  'True
                  FullRowSelect   =   -1  'True
                  GridLines       =   -1  'True
                  _Version        =   393217
                  ForeColor       =   12582912
                  BackColor       =   16777152
                  BorderStyle     =   1
                  Appearance      =   1
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  NumItems        =   4
                  BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     Text            =   "Id"
                     Object.Width           =   1411
                  EndProperty
                  BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   1
                     Text            =   "Name"
                     Object.Width           =   2540
                  EndProperty
                  BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   2
                     Text            =   "Type"
                     Object.Width           =   1411
                  EndProperty
                  BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   3
                     Text            =   "Path"
                     Object.Width           =   4410
                  EndProperty
               End
               Begin VB.Label Label8 
                  Caption         =   "(like = %)"
                  Height          =   375
                  Left            =   4920
                  TabIndex        =   31
                  Top             =   1740
                  Width           =   735
               End
               Begin VB.Label Label7 
                  Caption         =   "Name:"
                  ForeColor       =   &H00C00000&
                  Height          =   195
                  Left            =   2460
                  TabIndex        =   30
                  Top             =   1560
                  Width           =   1215
               End
               Begin VB.Label Label6 
                  Caption         =   "Type:"
                  ForeColor       =   &H00C00000&
                  Height          =   195
                  Left            =   180
                  TabIndex        =   29
                  Top             =   1560
                  Width           =   1215
               End
            End
            Begin VB.Label Label18 
               Caption         =   "Var2"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   -74760
               TabIndex        =   56
               Top             =   5400
               Width           =   1095
            End
            Begin VB.Label Label17 
               Caption         =   "Var1"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   -74760
               TabIndex        =   54
               Top             =   4904
               Width           =   1095
            End
            Begin VB.Label Label16 
               Caption         =   "Gravit�"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   -74760
               TabIndex        =   52
               Top             =   4411
               Width           =   1095
            End
            Begin VB.Label Label15 
               Caption         =   "Tipologia"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   -74760
               TabIndex        =   50
               Top             =   3918
               Width           =   1095
            End
            Begin VB.Label Label14 
               Caption         =   "Data Segnalaz."
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   -74760
               TabIndex        =   48
               Top             =   3425
               Width           =   1095
            End
            Begin VB.Label Label13 
               Caption         =   "Origine"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   -74760
               TabIndex        =   46
               Top             =   2932
               Width           =   1095
            End
            Begin VB.Label Label12 
               Caption         =   "Codice"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   -74760
               TabIndex        =   44
               Top             =   2439
               Width           =   1095
            End
            Begin VB.Label Label11 
               Caption         =   "Riga"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   -74760
               TabIndex        =   42
               Top             =   1946
               Width           =   1095
            End
            Begin VB.Label Label10 
               Caption         =   "IdOggettoRel"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   -74760
               TabIndex        =   40
               Top             =   1440
               Width           =   1095
            End
            Begin VB.Label Label9 
               Caption         =   "IdOggetto"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   -74760
               TabIndex        =   38
               Top             =   975
               Width           =   1095
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "Objects List"
            ForeColor       =   &H00000080&
            Height          =   6765
            Left            =   30
            TabIndex        =   3
            Top             =   0
            Width           =   2805
            Begin MSComctlLib.ListView lswObjects 
               Height          =   6345
               Left            =   120
               TabIndex        =   4
               Top             =   270
               Width           =   2565
               _ExtentX        =   4524
               _ExtentY        =   11192
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   0   'False
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   12582912
               BackColor       =   -2147483624
               BorderStyle     =   1
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               NumItems        =   4
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "Id"
                  Object.Width           =   1764
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Text            =   "Name"
                  Object.Width           =   2540
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Text            =   "Codice"
                  Object.Width           =   2540
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   3
                  Text            =   "Riga"
                  Object.Width           =   2540
               EndProperty
            End
         End
      End
      Begin MSComctlLib.ListView LwErr 
         Height          =   4890
         Left            =   0
         TabIndex        =   1
         Top             =   525
         Width           =   9885
         _ExtentX        =   17436
         _ExtentY        =   8625
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   16777215
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   9
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Object ID"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Object Name"
            Object.Width           =   4304
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Object Type"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Error"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Area"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Weight"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Description"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Called Module"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Ignore"
            Object.Width           =   1412
         EndProperty
      End
   End
   Begin VB.Menu MnuEdit 
      Caption         =   "EditMenu"
      Visible         =   0   'False
      Begin VB.Menu mnuTextEd 
         Caption         =   "Text Editor"
      End
      Begin VB.Menu mnuExternalEd 
         Caption         =   "EXTERNAL Text Editor"
      End
      Begin VB.Menu linea 
         Caption         =   "-"
      End
      Begin VB.Menu mnuObjProp 
         Caption         =   "Object Properties"
      End
      Begin VB.Menu linea2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuParseObj 
         Caption         =   "Parse Selected Object - First Level"
      End
      Begin VB.Menu mnuParseObj2 
         Caption         =   "Parse Selected Object - Second Level"
      End
   End
End
Attribute VB_Name = "MabsdF_ListErr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim OptType As String
Public objectList As Collection
'''''''''''''''''''''''''''''''''''''
'MF - 10-10-07
'Gestione flag "show ignore"
'''''''''''''''''''''''''''''''''''''
Private Sub ChkFlagIgnore_Click()
  Dim count As Long
  Dim j As Long
  
  lbParser.Visible = False
  lbParser.Caption = ""
    
  Screen.MousePointer = vbHourglass
  
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True

  If ChkFlagIgnore.Value = 0 Then
    count = LwErr.ListItems.count
    For j = 1 To LwErr.ListItems.count
      If j <= LwErr.ListItems.count Then
        If LwErr.ListItems(j).SubItems(8) = "X" Then
          LwErr.ListItems.Remove (j)
          'Eliminato un elemento: aggiornamento indice
          j = j - 1
        End If
      End If
    Next j
    NumSelErr = LwErr.ListItems.count
    NumSelIgn = count - LwErr.ListItems.count
    lbParser.Visible = True
    lbParser.Caption = "Selected Objects Unignored"
  Else
    CaricaErr
  End If
        
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "ML04E"
  lbParser.Visible = True
  lbParser.Caption = "Problems during Ignoring Objects"
  Exit Sub
tabellaInesistente:
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  lbParser.Visible = True
  lbParser.Caption = "Exception: Check-Repository needed."
End Sub

Private Sub cmdRemove_Click()
  Dim i As Integer, Query As String
  
  For i = 1 To lswRelation.ListItems.count
    If lswRelation.ListItems(i).Selected Then
      Query = "delete from PsRel_obj where " & _
              "idOggettoC = " & lswObjects.SelectedItem.Text & " and  " & _
              "idOggettoR = " & lswRelation.ListItems(i).Text
      DLLFunzioni.FnConnection.Execute Query
    End If
  Next i
  
  lswRelation.ListItems.Clear
  lswRelObjects.ListItems.Clear
  
  lswObjects_Click
  cmdQuery_Click
End Sub

Private Sub cmdSave_Click()
  Dim wScelta As Long
  
  Screen.MousePointer = vbHourglass
  
  If relIsvalid(Val(lswObjects.SelectedItem.Text)) Then
    lswObjects.SelectedItem.SmallIcon = 2
  Else
    lswObjects.SelectedItem.SmallIcon = 1
    
    wScelta = MsgBox("This Source does not have a completly relationships resolved... Do you want to continue with saving this relationships (Y/N)?", vbYesNo, m_fun.FnNomeProdotto)
    If wScelta = vbNo Then
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
  End If
  
  write_Relazioni Val(lswObjects.SelectedItem.Text)
  Screen.MousePointer = vbDefault
End Sub

Public Sub write_Relazioni(cId As Long)
  Dim rs As Recordset, rsDet As Recordset
  
  'Trasporta i record nella tabella delle relazioni
  Set rs = m_fun.Open_Recordset("select * from PsRel_ObjDup Where IdOggettoC = " & cId)
  While Not rs.EOF
    If rs!valida Then
      Set rsDet = m_fun.Open_Recordset("Select * From PsRel_Obj Where " & _
                                       "IdOggettoc = " & rs!idOggettoC & " And IdOggettoR = " & rs!IdOggettoR)
      If rsDet.RecordCount = 0 Then
        rsDet.AddNew
        
        rsDet!idOggettoC = rs!idOggettoC
        rsDet!IdOggettoR = rs!IdOggettoR
        rsDet!Relazione = rs!Relazione
        rsDet!nomecomponente = rs!nomecomponente
        rsDet!Utilizzo = rs!Utilizzo
        
        rsDet.Update
      End If
      rsDet.Close
    Else
      m_fun.FnConnection.Execute "Delete * From PsRel_Obj where " & _
                                 "IdOggettoC = " & cId & " And IdOggettoR = " & rs!IdOggettoR
    End If
    rs.MoveNext
  Wend
  rs.Close
End Sub

Private Sub cmdSave2_Click()
  'Deve :
  '1) Prendere le directory
  '2) Cominciare a scrivere le relazioni solo per quelle che matchano con le directory
  Dim rs As Recordset, rsObj As Recordset
  Dim wPath As String
  Dim i As Long
  Dim wExist As Boolean
  
  Screen.MousePointer = vbHourglass
  
  Set rs = m_fun.Open_Recordset("select IdOggetto,nome from Bs_Oggetti Where " & _
                                "idOggetto IN(Select distinct(IdOggettoC) From PsRel_ObjDup order by IdOggettoC)")
  While Not rs.EOF
    Set rsObj = m_fun.Open_Recordset("Select * From psRel_ObjDup Where " & _
                                     "IdOggettoC = " & rs!IdOggetto & " Order by NomeComponente")
    While Not rsObj.EOF
      wPath = Replace(m_fun.Restituisci_Directory_Da_IdOggetto(rsObj!IdOggettoR), m_fun.FnPathPrj, "...")
      
      'scrive la relazione se fa parte delle directory segnate
      wExist = False
      For i = 1 To lswPaths.ListItems.count
        If Trim(UCase(lswPaths.ListItems(i).Text)) = Trim(UCase(wPath)) Then
          If lswPaths.ListItems(i).Checked Then
            wExist = True
            Exit For
          End If
        End If
      Next i
      
      If wExist Then
        'Scrive sta benedetta relazione
        check_DbRel rsObj!idOggettoC, rsObj!IdOggettoR, wExist
        write_Relazioni rs!IdOggetto
      End If
      
      rsObj.MoveNext
    Wend
    rsObj.Close
    
    rs.MoveNext
  Wend
  rs.Close
  
'  For i = 1 To lswObjects.ListItems.count
'    If relIsvalid(Val(lswObjects.ListItems(i))) Then
'      lswObjects.ListItems(i).SmallIcon = 2
'    Else
'      lswObjects.ListItems(i).SmallIcon = 1
'    End If
'  Next i
   
  Screen.MousePointer = vbDefault
End Sub

Private Sub cmdQuery_Click()
  Dim Query As String, wPath As String
  Dim rs As Recordset
  Dim myItem As ListItem
  
  Screen.MousePointer = vbHourglass
  
  If Not cmbType.Text = "" Then
    lswRelObjects.ListItems.Clear
    Query = "Select Idoggetto, nome, tipo, directory_input from bs_oggetti where tipo = '" & cmbType.Text & "'" & vbCrLf
    Query = Query & "and idOggetto not in(select idOggettoR from PsRel_Obj where IdOggettoC = " & lswObjects.SelectedItem.Text & ")"
    
    If Len(Trim(txtNome)) Then
      Query = Query & " AND nome like '" & txtNome & "'"
    End If
    Query = Query & " order by nome"
    Set rs = m_fun.Open_Recordset(Query)
    Do Until rs.EOF
      wPath = Replace(m_fun.Restituisci_Directory_Da_IdOggetto(rs!IdOggetto), m_fun.FnPathPrj, "...")
      Set myItem = lswRelObjects.ListItems.Add(, , Format(rs!IdOggetto, "000000"))
      myItem.ListSubItems.Add , , rs!Nome
      myItem.ListSubItems.Add , , rs!Tipo
      myItem.ListSubItems.Add , , wPath
      
      rs.MoveNext
    Loop
    rs.Close
  End If
  Screen.MousePointer = vbDefault
End Sub

Private Sub cmdSave3_Click()
  Dim i As Integer, Query As String
  
  For i = 1 To lswRelObjects.ListItems.count
    If lswRelObjects.ListItems(i).Selected Then
      Query = "Insert into PsRel_obj values(" & CLng(lswObjects.SelectedItem.Text) & "," & lswRelObjects.ListItems(i).Text & "," & vbCrLf
      Query = Query & "'" & lswRelObjects.ListItems(i).SubItems(2) & "','" & lswRelObjects.ListItems(i).SubItems(1) & "','" & lswRelObjects.ListItems(i).SubItems(2) & "-IMPORT')"
      DLLFunzioni.FnConnection.Execute Query
    End If
  Next i
  
  lswRelation.ListItems.Clear
  lswRelObjects.ListItems.Clear
  
  lswObjects_Click
  cmdQuery_Click
End Sub

Private Sub Form_Activate()
  Me.Refresh
  OptApp.Value = True
End Sub

Public Sub Form_Load()
  NumSelErr = 0
  NumSelIgn = 0
  OptType = ""
  
  lswRelDup.ColumnHeaders(4).Width = lswRelDup.Width - lswRelDup.ColumnHeaders(1).Width - lswRelDup.ColumnHeaders(2).Width - lswRelDup.ColumnHeaders(1).Width - 270
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Sub CaricaCombo()
  Dim tb As Recordset, Tb1 As Recordset
  Dim wStr As String
  
  CmbError.Clear
  CmbError.AddItem "*All"
  CmbWeight.Clear
  CmbWeight.AddItem "*All"
  
  If OptType = "A" Then
    Set tb = m_fun.Open_Recordset("select distinct codice from bs_segnalazioni order by codice")
  Else
    Set tb = m_fun.Open_Recordset("select distinct codice from bs_segnalazioniprj order by codice")
  End If
  If tb.RecordCount = 0 Then Exit Sub
  While Not tb.EOF
    Set Tb1 = m_fun.Open_Recordset_Sys("select * from bs_messaggi where codice = '" & tb!Codice & "' ")
    wStr = Tb1!Testo
    Tb1.Close
    CmbError.AddItem tb!Codice & " - " & wStr
    tb.MoveNext
  Wend
  tb.Close
    
  If OptType = "A" Then
    Set tb = m_fun.Open_Recordset("select distinct gravita from bs_segnalazioni order by gravita")
  Else
    Set tb = m_fun.Open_Recordset("select distinct gravita from bs_segnalazioniprj order by gravita")
  End If
  If tb.RecordCount = 0 Then Exit Sub
  While Not tb.EOF
    Select Case tb!Gravita
      Case "I"
        wStr = "I - Informational"
      Case "W"
        wStr = "W - Warning"
      Case "E"
        wStr = "E - Error"
      Case "S"
        wStr = "S - Sever"
      Case Else
        wStr = tb!Gravita & " - "
    End Select
    CmbWeight.AddItem wStr
    tb.MoveNext
  Wend
  tb.Close
  
  CmbError.ListIndex = 0
  CmbWeight.ListIndex = 0
End Sub

'''''''''''''''''''''''''''''''''''''''''''''
' MF - 10-07-07
' Gestione flag "show ignore"
'''''''''''''''''''''''''''''''''''''''''''''
Sub CaricaErr()
  Dim tb As Recordset, Tb1 As Recordset
  Dim wStr As String, wSQL As String, wWhere As String
  Dim Y As Integer, k As Integer
  
  LwErr.ListItems.Clear
  
  NumSelErr = 0
  NumSelIgn = 0
  
  If CmbError.ListCount = 1 Then Exit Sub
  
  wSQL = "SELECT distinct a.idoggetto, b.nome, b.tipo, a.codice, a.tipologia, a.gravita, a.var1, a.var2 from bs_segnalazioni as a, bs_oggetti as b where " & _
         "(a.idoggetto = b.idoggetto) "
  If OptType = "A" Then
    wSQL = wSQL & " "
  ElseIf OptType = "P" Then
    wSQL = wSQL & "Prj "
  End If
  
  If CmbError.Text <> "*All" Then
    k = InStr(CmbError.Text, "-")
    wStr = Trim(Mid$(CmbError.Text, 1, k - 1))
    wWhere = wWhere & " Codice = '" & wStr & "' "
  End If
  
  If CmbWeight.Text <> "*All" Then
    k = InStr(CmbWeight.Text, "-")
    wStr = Trim(Mid$(CmbWeight.Text, 1, k - 1))
    If Trim(wWhere) <> "" Then wWhere = wWhere & " and "
    wWhere = wWhere & " gravita = '" & wStr & "' "
  End If
  
  If Trim(wWhere) <> "" Then
    wSQL = wSQL & "AND " & wWhere
  End If
  
  Set tb = m_fun.Open_Recordset(wSQL)
  While Not tb.EOF
    For Y = 0 To CmbError.ListCount - 1
      If tb!Codice = Left(CmbError.List(Y), 3) Then
        wStr = Mid(CmbError.List(Y), 5)
        Exit For
      End If
    Next Y
    
    '''''''''''''''''''''''''''''''''''''''''''
    'MF - 10-07-07
    'Gestione Flag "Show Ignore"
    '''''''''''''''''''''''''''''''''''''''''''
    Set Tb1 = m_fun.Open_Recordset("SELECT count(*) FROM bs_Segnalazioni_Ignore WHERE " & _
                                   "code = '" & tb!Codice & "' AND var1 = '" & Replace(tb!var1, "'", "''") & "'")
    If ChkFlagIgnore.Value = 1 Then
      'Visualizza tutti
      With LwErr.ListItems.Add(, , tb!IdOggetto)
        .SubItems(1) = tb!Nome
        .SubItems(2) = tb!Tipo
        .SubItems(3) = tb!Codice
        .SubItems(4) = tb!Tipologia & ""
        .SubItems(5) = tb!Gravita
        If Trim(tb!var1) <> "" Then
          wStr = Replace(wStr, "%VAR1%", tb!var1)
        End If
        If Trim(tb!Var2) <> "" Then
          wStr = Replace(wStr, "%VAR2%", tb!Var2)
        End If
        .SubItems(6) = wStr
        '''''''''''''''''''''''''''''''''''''''''
        'SQ Phoenix: mi serve il CHIAMATO!
        '(attenzione: uso Var1, quindi devo essere sicuro che sia effettivamente li' il chiamato!
        '''''''''''''''''''''''''''''''''''''''''
        .SubItems(7) = tb!var1 & ""
        '''''''''''''''''''''''''''''''''''''''''
        'SQ Phoenix: gestione "ignore error"
        'colonna flag: X == ignore
        '''''''''''''''''''''''''''''''''''''''''
        'TMP: progetti gi� esistenti
        On Error GoTo tabellaInesistente
        .SubItems(8) = IIf(Tb1(0) = 0, "", "X")
        NumSelErr = LwErr.ListItems.count
        lbParser.Visible = True
        lbParser.Caption = "Selected Objects Ignored"
        On Error GoTo 0
      End With
    Else
      'Non visualizza gli ignore (default)
      Set Tb1 = m_fun.Open_Recordset("SELECT code,var1 FROM bs_Segnalazioni_Ignore WHERE code='" & tb!Codice & "' AND var1='" & Replace(tb!var1, "'", "''") & "'")
      If Tb1.RecordCount = 0 Then
        With LwErr.ListItems.Add(, , tb!IdOggetto)
          .SubItems(1) = tb!Nome
          .SubItems(2) = tb!Tipo
          .SubItems(3) = tb!Codice
          .SubItems(4) = tb!Tipologia & ""
          .SubItems(5) = tb!Gravita
          If Trim(tb!var1) <> "" Then
            wStr = Replace(wStr, "%VAR1%", tb!var1)
          End If
          If Trim(tb!Var2) <> "" Then
            wStr = Replace(wStr, "%VAR2%", tb!Var2)
          End If
          .SubItems(6) = wStr
          .SubItems(7) = tb!var1 & ""
          On Error GoTo tabellaInesistente
          .SubItems(8) = ""
          On Error GoTo 0
        End With
        NumSelErr = LwErr.ListItems.count
        lbParser.Visible = True
        lbParser.Caption = "Selected Objects Unignored"
      End If
      Tb1.Close
    End If
    tb.MoveNext
  Wend
  NumSelIgn = tb.RecordCount - LwErr.ListItems.count
  tb.Close
    
  Exit Sub
tabellaInesistente:
  Resume Next
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
   
  If m_fun.FnProcessRunning Then
    wScelta = m_fun.Show_MsgBoxError("FB01I")
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub lswObjects_Click()
  Dim rs As Recordset
  Dim wPath As String
  Dim myItem As ListItem
  
  lswRelObjects.ListItems.Clear
  txtNome.Text = ""
  cmbType.ListIndex = -1
  
  If lswObjects.ListItems.count Then
    ' Gestione duplicati
    lswRelDup.Sorted = False
    lswRelDup.ListItems.Clear
    
    Set rs = m_fun.Open_Recordset("Select * From psRel_ObjDup Where " & _
                                  "IdOggettoC = " & Val(lswObjects.SelectedItem.Text) & " Order by NomeComponente")
    While Not rs.EOF
      wPath = Replace(m_fun.Restituisci_Directory_Da_IdOggetto(rs!IdOggettoR), m_fun.FnPathPrj, "...")
      Set myItem = lswRelDup.ListItems.Add(, , Format(rs!IdOggettoR, "000000"))
      myItem.ListSubItems.Add , , rs!nomecomponente
      myItem.ListSubItems.Add , , rs!Utilizzo
      myItem.ListSubItems.Add , , wPath
      
      If rs!valida Then
        myItem.Checked = True
      End If
      
      rs.MoveNext
    Wend
    rs.Close
    
    ' Gestione relazioni
    lswRelation.ListItems.Clear
    
    Set rs = m_fun.Open_Recordset("Select * From psRel_Obj Where " & _
                                  "IdOggettoC = " & Val(lswObjects.SelectedItem.Text) & " Order by NomeComponente")
    While Not rs.EOF
      wPath = Replace(m_fun.Restituisci_Directory_Da_IdOggetto(rs!IdOggettoR), m_fun.FnPathPrj, "...")
      Set myItem = lswRelation.ListItems.Add(, , Format(rs!IdOggettoR, "000000"))
      myItem.ListSubItems.Add , , rs!nomecomponente
      myItem.ListSubItems.Add , , rs!Relazione
      myItem.ListSubItems.Add , , rs!Utilizzo
      myItem.ListSubItems.Add , , wPath
      
      rs.MoveNext
    Wend
    rs.Close
    
    ' Carica Dettaglio segnalazione
    txtIdOggetto = ""
    txtIdOggettoR = ""
    txtRiga = ""
    txtCodice = ""
    txtOrigine = ""
    txtDataSegnalaz = ""
    txtGravita = ""
    txtVar1 = ""
    txtVar2 = ""
    Set rs = m_fun.Open_Recordset("Select * From Bs_segnalazioni Where " & _
                                  "IdOggetto = " & Val(lswObjects.SelectedItem.Text))
    If Not rs.EOF Then
      txtIdOggetto = rs!IdOggetto
      txtIdOggettoR = rs!IdOggettoRel
      txtRiga = rs!riga
      txtCodice = rs!Codice
      txtOrigine = rs!Origine
      txtDataSegnalaz = rs!Data_segnalazione
      txtGravita = rs!Gravita
      txtVar1 = rs!var1
      txtVar2 = rs!Var2
    End If
    rs.Close
  End If
End Sub

Private Sub lswRelObjects_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswRelObjects.SortOrder
  Key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswRelObjects.SortOrder = Order
  lswRelObjects.SortKey = Key - 1
  lswRelObjects.Sorted = True
End Sub

Private Sub lswRelation_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswRelation.SortOrder
  Key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswRelation.SortOrder = Order
  lswRelation.SortKey = Key - 1
  lswRelation.Sorted = True
End Sub

Private Sub lswObjects_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswObjects.SortOrder
  Key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswObjects.SortOrder = Order
  lswObjects.SortKey = Key - 1
  lswObjects.Sorted = True
End Sub

Private Sub lswPaths_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswPaths.SortOrder
  Key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswPaths.SortOrder = Order
  lswPaths.SortKey = Key - 1
  lswPaths.Sorted = True
End Sub

Private Sub lswRelDup_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswRelDup.SortOrder
  Key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswRelDup.SortOrder = Order
  lswRelDup.SortKey = Key - 1
  lswRelDup.Sorted = True
End Sub

Private Sub lswRelDup_ItemCheck(ByVal Item As MSComctlLib.ListItem)
  'Sflagga altri item con lo stesso nome
  Dim i As Long

  For i = 1 To lswRelDup.ListItems.count
    If Trim(UCase(lswRelDup.ListItems(i).ListSubItems(1).Text)) = Trim(UCase(Item.ListSubItems(1).Text)) Then
      If i <> Item.Index Then
        If lswRelDup.ListItems(i).Checked Then
          lswRelDup.ListItems(i).Checked = False
        End If
      End If
    End If
  Next i
 
  check_DbRel Val(lswObjects.SelectedItem.Text), Val(Item.Text), Item.Checked
End Sub

Public Sub check_DbRel(idObjC As Long, idObjR As Long, cValue As Boolean)
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("select * From Psrel_Objdup Where " & _
                                "IdOggettoC = " & idObjC & " And IdoggettoR = " & idObjR)
  If rs.RecordCount Then
    rs!valida = cValue
    rs.Update
  End If
  rs.Close
End Sub

Private Sub LwErr_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  Static sOrder
  sOrder = Not sOrder
  
  'Usa l'ordinamento standard per ordinare gli items
  LwErr.SortKey = ColumnHeader.Index - 1
  LwErr.SortOrder = Abs(sOrder)
  LwErr.Sorted = True
End Sub

Private Sub LwErr_DblClick()
  If Not (LwErr.SelectedItem Is Nothing) Then
    m_fun.Show_TextEditor LwErr.SelectedItem.Text, , , MabseF_List
  End If
End Sub

'MAURO - 02/12/2005
Private Sub LwErr_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Not (LwErr.SelectedItem Is Nothing) Then
    'Mostra popup menu
    If Button = vbRightButton Then
      'SQ 10-11-06 - Default
      mnuParseObj2.Enabled = False
      Select Case LwErr.SelectedItem.SubItems(2)
        Case "CBL", "EZT", "QIM", "PLI"
          mnuParseObj2.Enabled = True
        Case "CMF"
          mnuParseObj.Enabled = False
      End Select
      
      PopupMenu MnuEdit
    End If
  End If
End Sub

Private Sub mnuExternalEd_Click()
  'Avvia un editor esterno
  ExtEdit GbExternalEditor
End Sub

Private Sub mnuObjProp_Click()
  Dim OggErr As Ogg

  Carica_Proprieta_Oggetto_Err LwErr.SelectedItem, OggErr
  'visualizza le propriet� dell'oggetto Selezionato
  If Trim(OggErr.Nome) = "" Or Trim(OggErr.directory_input) = "" Then
    DLLFunzioni.Show_MsgBoxError "MP02I"
  Else
    DLLFunzioni.Show_ObjectProperties OggErr, MabsdF_ListErr
  End If
End Sub

Public Sub Carica_Proprieta_Oggetto_Err(id As Long, OggProp As Ogg)
  Dim r As Recordset
  
  Set r = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & id)
  If r.RecordCount Then
    '� un solo record
    OggProp.Area_App = r!Area_Appartenenza
    OggProp.Batch = TN(r!Batch, vbBoolean)
    OggProp.Cics = TN(r!Cics, vbBoolean)
    OggProp.directory_input = Crea_Directory_Progetto(r!directory_input & "", ProjectPath)
    OggProp.Directory_Output = Crea_Directory_Progetto(r!Directory_Output & "", ProjectPath)
    OggProp.DLI = TN(r!DLI, vbBoolean)
    OggProp.DtImport = r!DtImport & ""
    OggProp.DtIncaps = r!DtIncapsulamento & ""
    OggProp.DtParsing = r!DtParsing & ""
    OggProp.IdOggetto = r!IdOggetto
    OggProp.Livello1 = r!Livello1 & ""
    OggProp.Livello2 = r!Livello2 & ""
    OggProp.Mapping = TN(r!Mapping, vbBoolean)
    OggProp.Nome = Trim(r!Nome) & ""
    OggProp.Estensione = Trim(r!Estensione) & ""
    
    If Not IsNull(r!NumRighe) Then
      OggProp.NumRighe = r!NumRighe
    Else
      OggProp.NumRighe = 0
    End If
    
    If Not IsNull(r!parsingLevel) Then
      OggProp.ParsingLev = r!parsingLevel
    Else
      OggProp.ParsingLev = 0
    End If
    
    OggProp.sql = TN(r!WithSQL, vbBoolean)
    OggProp.Ims = TN(r!Ims, vbBoolean)
    OggProp.Tipo = r!Tipo & ""
    OggProp.TipoDBD = r!Tipo_DBD & ""
    OggProp.VSam = TN(r!VSam, vbBoolean)
  End If
  r.Close
End Sub

Private Sub mnuParseObj_Click()
  parser1
End Sub

Private Sub mnuParseObj2_Click()
  parser2
End Sub

Private Sub mnuTextEd_Click()
  If Not (LwErr.SelectedItem Is Nothing) Then
    m_fun.Show_TextEditor LwErr.SelectedItem.Text, , , MabseF_List
  End If
End Sub

Private Sub OptApp_Click()
  m_fun.FnProcessRunning = True
  Screen.MousePointer = vbHourglass
  
  If OptApp.Value = True Then
    If OptType <> "A" Then
      OptType = "A"
      CaricaCombo
    End If
  End If
  
  Screen.MousePointer = vbNormal
  m_fun.FnProcessRunning = False
End Sub

Private Sub OptPrj_Click()
  m_fun.FnProcessRunning = True
  Screen.MousePointer = vbHourglass
  
  If OptPrj.Value Then
    If OptType <> "P" Then
      OptType = "P"
      CaricaCombo
    End If
  End If
  
  Screen.MousePointer = vbNormal
  m_fun.FnProcessRunning = False
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  If SSTab1.Tab = 1 Then
    lswObjects.ListItems.Clear
    lswRelation.ListItems.Clear
    lswRelDup.ListItems.Clear
    lswRelObjects.ListItems.Clear
    lswPaths.ListItems.Clear
    load_ObjectsList
    fraCorrector.Move 50, 50
    ' Carica lista Tipi
    Load_Combo_Type
    ' Aggiusto le dimensioni dei campi nelle listView
    lswObjects.ColumnHeaders(1).Width = lswObjects.Width / 4 - 100
    lswObjects.ColumnHeaders(2).Width = lswObjects.Width / 4
    lswObjects.ColumnHeaders(3).Width = lswObjects.Width / 4
    lswObjects.ColumnHeaders(4).Width = lswObjects.Width / 4
    
    lswRelDup.ColumnHeaders(1).Width = lswRelDup.Width / 6 - 100
    lswRelDup.ColumnHeaders(2).Width = lswRelDup.Width / 6
    lswRelDup.ColumnHeaders(3).Width = lswRelDup.Width / 6
    lswRelDup.ColumnHeaders(4).Width = lswRelDup.Width / 6 * 3
    
    lswPaths.ColumnHeaders(1).Width = lswPaths.Width - 100
    
    lswRelation.ColumnHeaders(1).Width = lswRelation.Width / 6 - 100
    lswRelation.ColumnHeaders(2).Width = lswRelation.Width / 6
    lswRelation.ColumnHeaders(3).Width = lswRelation.Width / 6
    lswRelation.ColumnHeaders(4).Width = lswRelation.Width / 6
    lswRelation.ColumnHeaders(5).Width = lswRelation.Width / 6 * 2
    
    lswRelObjects.ColumnHeaders(1).Width = lswRelObjects.Width / 6 - 100
    lswRelObjects.ColumnHeaders(2).Width = lswRelObjects.Width / 6
    lswRelObjects.ColumnHeaders(3).Width = lswRelObjects.Width / 6
    lswRelObjects.ColumnHeaders(4).Width = lswRelObjects.Width / 6 * 3
  End If
End Sub

Public Sub load_ObjectsList()
  Dim rs As Recordset
  Dim wPath As String
  Dim k As Integer, wWhere As String, wStr As String
  Dim myItem As ListItem
  
  Screen.MousePointer = vbHourglass
  lswObjects.ListItems.Clear
  
  'SQ
  If Len(CmbError.Text) Then
    If CmbError.Text <> "*All" Then
      k = InStr(CmbError.Text, "-")
      wStr = Trim(Left(CmbError.Text, k - 1))
      wWhere = wWhere & " Codice = '" & wStr & "' "
    End If
    
    If CmbWeight.Text <> "*All" Then
      k = InStr(CmbWeight.Text, "-")
      wStr = Trim(Mid$(CmbWeight.Text, 1, k - 1))
      If Trim(wWhere) <> "" Then wWhere = wWhere & " and "
      wWhere = wWhere & " gravita = '" & wStr & "' "
    End If
    
    If wWhere <> "" Then wWhere = "AND " & wWhere
    
    Set rs = m_fun.Open_Recordset("SELECT b.idoggetto,b.nome,a.codice,a.riga from bs_segnalazioni as a, bs_oggetti as b where " & _
                                  "a.idoggetto = b.idoggetto " & wWhere)
    While Not rs.EOF
      Set myItem = lswObjects.ListItems.Add(, , Format((rs!IdOggetto), "000000")) ', , 1
      myItem.ListSubItems.Add , , rs!Nome
      myItem.ListSubItems.Add , , rs!Codice
      myItem.ListSubItems.Add , , rs!riga
    
      rs.MoveNext
    Wend
    rs.Close
  End If
  'Carica i path
  Set rs = m_fun.Open_Recordset("select distinct Directory_Input from Bs_Oggetti where " & _
                                "IdOggetto IN (Select IdOggettoR From psRel_ObjDup)")
  If rs.RecordCount Then
    lswPaths.ListItems.Clear
    
    While Not rs.EOF
      wPath = m_fun.Crea_Directory_Parametrica(m_fun.FnPathPrj)
      wPath = Replace(rs!directory_input, wPath, "...")
      lswPaths.ListItems.Add , , wPath
      rs.MoveNext
    Wend
  End If
  rs.Close
  Screen.MousePointer = vbDefault
End Sub

Public Sub Load_Combo_Type()
  Dim rs As Recordset
  
  Screen.MousePointer = vbHourglass
  
  cmbType.Clear
  Set rs = m_fun.Open_Recordset("select distinct tipo from Bs_Oggetti order by tipo")
  While Not rs.EOF
    cmbType.AddItem rs!Tipo & ""
    rs.MoveNext
  Wend
  rs.Close
  
  Screen.MousePointer = vbDefault
End Sub

Public Function relIsvalid(idObj As Long) As Boolean
  Dim rs As Recordset, rsDet As Recordset
  Dim bValid As Boolean
  
  Set rs = m_fun.Open_Recordset("Select distinct nomecomponente from PsRel_ObjDup Where " & _
                                "IdOggettoc = " & idObj & " Order by NomeComponente")
  Do While Not rs.EOF
    Set rsDet = m_fun.Open_Recordset("Select * From PsRel_ObjDup Where " & _
                                     "IdOggettoc = " & idObj & " and nomeComponente = '" & rs!nomecomponente & "'")
    If rsDet.RecordCount Then
      bValid = False
      Do While Not rsDet.EOF
        If rsDet!valida Then
          bValid = True
          Exit Do
        End If
        rsDet.MoveNext
      Loop
    End If
    rsDet.Close
    
    If bValid = False Then
      Exit Do
    End If
    rs.MoveNext
  Loop
  rs.Close
  
  relIsvalid = bValid
End Function

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "EXECUTE"
      m_fun.FnProcessRunning = True
      Screen.MousePointer = vbHourglass
      
      CaricaErr
      
      Screen.MousePointer = vbNormal
      m_fun.FnProcessRunning = False
      
    Case "PARSER1"
      parser1
      
    Case "PARSER2"
      parser2
      
    Case "DELETE"
      deleteObject
      
    Case "IGNORE"
      If DLLFunzioni.FnProcessRunning Then
        DLLFunzioni.Show_MsgBoxError "FB01I"
      Else
        ignoreError
      End If
  End Select
End Sub

Function isNewObject(IdOggetto As Long) As Boolean
  On Error GoTo errorHandler
  
  objectList.Add IdOggetto, CStr(IdOggetto)
  isNewObject = True
  
  Exit Function
errorHandler:
  isNewObject = False
End Function

Public Sub parser1()
  'Parser degli oggetti selezionati
  Dim i As Long
  Dim cParam As Collection
  
  Set objectList = New Collection
  On Error GoTo errorHandler
  
  lbParser.Visible = False
  lbParser.Caption = ""
  If DLLFunzioni.FnProcessRunning Then
    DLLFunzioni.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  
  '23-01-06: faceva dentro il parsing 1000 volte le stesse cose...
  DllParser.AttivaFunzione "INIT_PARSING", cParam
  For i = 1 To LwErr.ListItems.count
    If LwErr.ListItems(i).Selected Then
      If isNewObject(CLng(LwErr.ListItems(i).Text)) Then
        Set cParam = New Collection
        cParam.Add CLng(LwErr.ListItems(i).Text)
        DLLFunzioni.FnActiveWindowsBool = False
        DllParser.AttivaFunzione "PARSE_OBJECT_FIRST_LEVEL", cParam
        DLLFunzioni.FnActiveWindowsBool = True
      End If
    End If
  Next i
  
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  lbParser.Visible = True
  lbParser.Caption = "First Level Parsing concluded"
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "ML04E"
  lbParser.Visible = True
  lbParser.Caption = "First Level Parsing NOT concluded"
End Sub

Public Sub parser2()
  'Parser degli oggetti selezionati
  Dim i As Long
  Dim cParam As Collection
  
  Set objectList = New Collection
  On Error GoTo errorHandler
  
  lbParser.Visible = False
  lbParser.Caption = ""
  If DLLFunzioni.FnProcessRunning Then
    DLLFunzioni.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  
   '23-01-06: faceva dentro il parsing 1000 volte le stesse cose...
  DllParser.AttivaFunzione "INIT_PARSING", cParam
  For i = 1 To LwErr.ListItems.count
    If LwErr.ListItems(i).Selected Then
      If isNewObject(CLng(LwErr.ListItems(i).Text)) Then
        Set cParam = New Collection
        cParam.Add CLng(LwErr.ListItems(i).Text)
        DLLFunzioni.FnActiveWindowsBool = False
        DllParser.AttivaFunzione "PARSE_OBJECT_SECOND_LEVEL", cParam
        
        DLLFunzioni.FnActiveWindowsBool = True
      End If
    End If
  Next i
  
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  lbParser.Visible = True
  lbParser.Caption = "Second Level Parsing concluded"
  
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "ML04E"
  lbParser.Visible = True
  lbParser.Caption = "Second Level Parsing NOT concluded"
End Sub

Private Sub deleteObject()
  Dim i As Long
  Dim cParam As Collection
  Dim rs As Recordset
  Dim idOggettoC As Long
  
  Set objectList = New Collection
  On Error GoTo errorHandler
  
  lbParser.Visible = False
  lbParser.Caption = ""
  If DLLFunzioni.FnProcessRunning Then
    DLLFunzioni.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  
  For i = 1 To LwErr.ListItems.count
    If LwErr.ListItems(i).Selected Then
      Set cParam = New Collection
      cParam.Add CLng(LwErr.ListItems(i).Text)
      DLLFunzioni.FnActiveWindowsBool = False
      If MsgBox("Do you want to delete the selected object ", vbQuestion + vbYesNo) = vbYes Then
        DllParser.AttivaFunzione "DEL_TO_PROJECT", cParam
        DLLFunzioni.WriteLog "## " & LwErr.ListItems(i).Text & " " & LwErr.ListItems(i).SubItems(1)
        
        'ATTENZIONE ad EVENTUALE CHIAMANTE DI CHIAMANTE!
        Set rs = DLLFunzioni.Open_Recordset("SELECT * FROM PsRel_Obj WHERE idOggettoR = " & LwErr.ListItems(i).Text)
        If rs.RecordCount Then
          idOggettoC = rs!idOggettoC
          rs.Close
          Set rs = DLLFunzioni.Open_Recordset("SELECT * FROM Bs_Oggetti WHERE idOggetto = " & idOggettoC)
          If rs.RecordCount Then
            rs!Notes = "OUT_OF_SCOPE - " & LwErr.ListItems(i).Text
            rs.Update
          End If
          rs.Close
        End If
        rs.Close
        
        DLLFunzioni.FnActiveWindowsBool = True
      End If
    End If
  Next i
  
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  lbParser.Visible = True
  lbParser.Caption = "Selected Objects Deleted."
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "ML04E"
  lbParser.Visible = True
  lbParser.Caption = "Delete NOT concluded"
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ Phoenix TMP
' Importare una lista di oggetti da cancellare (txt o xls)...
' -> serve finestra di dialogo...
' IMP: la lista � di oggetti mancanti, quindi CHIAMATI;
'      => cancello i CHIAMANTI!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub deleteObject_List()
  Dim i As Long
  Dim dialogFileName As String
  Dim cParam As Collection
  Dim j As Long, count As Long
  Dim objList() As String 'solo il nome � pericoloso: dovra avere anche la directory
  Dim rs As Recordset
  Dim idOggettoC As Long

  Set objectList = New Collection
  On Error GoTo errorHandler
  
  lbParser.Visible = False
  lbParser.Caption = ""
  If DLLFunzioni.FnProcessRunning Then
    DLLFunzioni.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''
  'MF - 11-07-07
  'Gestione eliminazione lista oggetti da file
  ''''''''''''''''''''''''''''''''''''''''''''''
  dialogFileName = LoadCommDialog(True, "Select a file", "All Files", "*")
  If Len(dialogFileName) > 0 Then
    objList = readFile(dialogFileName)
  
    Screen.MousePointer = vbHourglass
    
    DLLFunzioni.FnActiveWindowsBool = True
    DLLFunzioni.FnProcessRunning = True
    
    count = UBound(objList)
    If MsgBox("Do you want to delete the list of " & count & " objects ", vbQuestion + vbYesNo) = vbYes Then
      For i = 1 To UBound(objList)
        For j = 1 To LwErr.ListItems.count
          If LwErr.ListItems(j).SubItems(1) = objList(i) Then
            count = count + 1
            If isNewObject(CLng(LwErr.ListItems(j))) Then
              Set cParam = New Collection
              cParam.Add CLng(LwErr.ListItems(j))
              DLLFunzioni.FnActiveWindowsBool = False
              'SQ ATTENZIONE: problema duplicati
              '-> il DELETE fa solo un update delle relazioni in caso di duplicati... controllare!
              DllParser.AttivaFunzione "DEL_TO_PROJECT", cParam
              DLLFunzioni.WriteLog "## " & LwErr.ListItems(j).Text & " " & LwErr.ListItems(j).SubItems(1)
              
              'ATTENZIONE ad EVENTUALE CHIAMANTE DI CHIAMANTE!
              Set rs = DLLFunzioni.Open_Recordset("SELECT * FROM PsRel_Obj WHERE idOggettoR=" & LwErr.ListItems(j).Text)
              If rs.RecordCount Then
                idOggettoC = rs!idOggettoC
                rs.Close
                Set rs = DLLFunzioni.Open_Recordset("SELECT * FROM Bs_Oggetti WHERE idOggetto=" & idOggettoC)
                If rs.RecordCount Then
                  rs!Notes = "OUT_OF_SCOPE - " & LwErr.ListItems(j).Text
                  rs.Update
                End If
                rs.Close
              End If
              rs.Close
              
              '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
              ' SQ Phoenix TMP
              ' Bisogna riportare SEMPRE tutte queste attivit�
              ' ==> ci vuole il file di LOG e di ERR! non tutto insieme incasinato
              '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
              DLLFunzioni.WriteLog "!!!!!!!!!!RELATIONSHIP ERROR: DELETE LIST - Caller Name: " & objList(i) & " ==> DELETE on: " & LwErr.ListItems(j).SubItems(1)
              DLLFunzioni.FnActiveWindowsBool = True
            End If
          End If
        Next
      Next
    End If
    
    DLLFunzioni.FnActiveWindowsBool = False
    DLLFunzioni.FnProcessRunning = False
    
    Screen.MousePointer = vbDefault
    lbParser.Visible = True
    lbParser.Caption = "Objects list Deleted"
  End If
  
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "RC0002", "MapsdF_ListErr", "deleteObject_List", ERR.Number, ERR.Description, True
  lbParser.Visible = True
  lbParser.Caption = "Delete NOT concluded"
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF - 12-07-07
'Importare da file una lista di oggetti da cancellare
'Eliminazione sulla base del "Called Module"
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub deleteObjCall_List()
  Dim i As Long
  Dim dialogFileName As String
  Dim cParam As Collection
  Dim j As Long, count As Long
  Dim objList() As String 'solo il nome � pericoloso: dovra avere anche la directory
  Dim rs As Recordset
  Dim idOggettoC As Long
  
  Set objectList = New Collection
  On Error GoTo errorHandler
  
  lbParser.Visible = False
  lbParser.Caption = ""
  If DLLFunzioni.FnProcessRunning Then
    DLLFunzioni.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''
  'MF - 12-07-07
  'Gestione eliminazione lista oggetti da file
  ''''''''''''''''''''''''''''''''''''''''''''''
  dialogFileName = LoadCommDialog(True, "Select a file", "All Files", "*")
  If Len(dialogFileName) > 0 Then
    objList = readFile(dialogFileName)
  
    Screen.MousePointer = vbHourglass
    
    DLLFunzioni.FnActiveWindowsBool = True
    DLLFunzioni.FnProcessRunning = True
    
    count = UBound(objList)
    If MsgBox("Do you want to delete the list of " & count & " called module ", vbQuestion + vbYesNo) = vbYes Then
      For i = 0 To UBound(objList)
        For j = 1 To LwErr.ListItems.count
          If LwErr.ListItems(j).SubItems(7) = objList(i) Then
            If isNewObject(CLng(LwErr.ListItems(j))) Then
              Set cParam = New Collection
              cParam.Add CLng(LwErr.ListItems(j))
              DLLFunzioni.FnActiveWindowsBool = False
              'SQ ATTENZIONE: problema duplicati
              '-> il DELETE fa solo un update delle relazioni in caso di duplicati... controllare!
              DllParser.AttivaFunzione "DEL_TO_PROJECT", cParam
              DLLFunzioni.WriteLog "## " & LwErr.ListItems(j).Text & " " & LwErr.ListItems(j).SubItems(1)
              
              'ATTENZIONE ad EVENTUALE CHIAMANTE DI CHIAMANTE!
              Set rs = DLLFunzioni.Open_Recordset("SELECT * FROM PsRel_Obj WHERE idOggettoR=" & LwErr.ListItems(j).Text)
              If rs.RecordCount Then
                idOggettoC = rs!idOggettoC
                rs.Close
                Set rs = DLLFunzioni.Open_Recordset("SELECT * FROM Bs_Oggetti WHERE idOggetto=" & idOggettoC)
                If rs.RecordCount Then
                  rs!Notes = "OUT_OF_SCOPE - " & LwErr.ListItems(j).Text
                  rs.Update
                End If
                rs.Close
              End If
              rs.Close
            
              '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
              ' SQ Phoenix TMP
              ' Bisogna riportare SEMPRE tutte queste attivit�
              ' ==> ci vuole il file di LOG e di ERR! non tutto insieme incasinato
              '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
              DLLFunzioni.WriteLog "!!!!!!!!!!RELATIONSHIP ERROR: DELETE LIST - Caller Name: " & objList(i) & " ==> DELETE on: " & LwErr.ListItems(j).SubItems(1)
              DLLFunzioni.FnActiveWindowsBool = True
            End If
          End If
        Next j
      Next i
    End If
    
    DLLFunzioni.FnActiveWindowsBool = False
    DLLFunzioni.FnProcessRunning = False
    
    Screen.MousePointer = vbDefault
    lbParser.Visible = True
    lbParser.Caption = "Objects list Deleted"
  End If
  
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "RC0002", "MapsdF_ListErr", "deleteObject_List", ERR.Number, ERR.Description, True
  lbParser.Visible = True
  lbParser.Caption = "Delete NOT concluded"
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ Phoenix (veramente sono a Francoforte!)
' Gestione "Errori NON Importanti"
' Nuova tabella bs_Segnalazionii_Ignore: codice + var1
' -> classificando una tipologia d'errore come "da ignorare",
'    vengono (opzionale) nascosti tutti i messaggi appartenenti
'    alla tipologia stessa; deve essere possibile abilitare/disabilitare
'    il trattamento dei messaggi da "ignorare"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub ignoreError()
  Dim i As Long
  Dim rs As Recordset
  Dim Code As String, var1 As String
  Dim ignoreCount As Long
  Dim j As Long
  
  On Error GoTo errorHandler
  
  Set objectList = New Collection
  lbParser.Visible = False
  lbParser.Caption = ""
    
  Screen.MousePointer = vbHourglass
  
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  
  For i = 1 To LwErr.ListItems.count
    If LwErr.ListItems(i).Selected Then
      'cParam.Add CLng(LwErr.ListItems(i).Text)
      DLLFunzioni.FnActiveWindowsBool = False
      
      Code = LwErr.ListItems(i).SubItems(3)
      var1 = LwErr.ListItems(i).SubItems(7)
      
      'SQ tmp - x progetti esistenti
      On Error GoTo tabellaInesistente
      Set rs = DLLFunzioni.Open_Recordset("SELECT * FROM bs_Segnalazioni_Ignore WHERE " & _
                                          "code = '" & Code & "' AND var1 = '" & Replace(var1, "'", "''") & "'")
      If rs.EOF Then
        rs.AddNew
        rs!Code = Code
        rs!var1 = var1
        rs.Update
      End If
      rs.Close
      On Error GoTo 0
      
      'REFRESH LIST:
      For j = 1 To LwErr.ListItems.count
        If LwErr.ListItems(j).SubItems(3) = Code And LwErr.ListItems(j).SubItems(7) = var1 Then
          ignoreCount = ignoreCount + 1
          LwErr.ListItems(j).SubItems(8) = "X"
        End If
      Next j
      
      DLLFunzioni.FnActiveWindowsBool = True
    End If
  Next i
    
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  lbParser.Visible = True
  lbParser.Caption = "Selected Objects Ignored"

  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "ML04E"
  lbParser.Visible = True
  lbParser.Caption = "Problems during Ignoring Objects"
  Exit Sub
tabellaInesistente:
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  lbParser.Visible = True
  lbParser.Caption = "Exception: Check-Repository needed."
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'SQ Phoenix (veramente sono a Francoforte!)
'Gestione "Errori NON Importanti"
'Nuova tabella bs_Segnalazionii_Ignore: codice + var1
'-> classificando una tipologia d'errore come "da ignorare",
'   vengono (opzionale) nascosti tutti i messaggi appartenenti
'   alla tipologia stessa; deve essere possibile abilitare/disabilitare
'   il trattamento dei messaggi da "ignorare"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF - 06-07-07
'Gestione opzione unignore
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub unignoreError()
  Dim i As Long
  Dim rs As Recordset
  Dim Code As String, var1 As String, ignore As String
  Dim j As Long
      
  On Error GoTo errorHandler
  
  Set objectList = New Collection
  lbParser.Visible = False
  lbParser.Caption = ""
    
  Screen.MousePointer = vbHourglass
  
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  
  For i = 1 To LwErr.ListItems.count
    If LwErr.ListItems(i).Selected Then
      DLLFunzioni.FnActiveWindowsBool = False
      
      Code = LwErr.ListItems(i).SubItems(3)
      var1 = LwErr.ListItems(i).SubItems(7)
      
      'REFRESH LIST:
      For j = 1 To LwErr.ListItems.count
        If LwErr.ListItems(j).SubItems(3) = Code And LwErr.ListItems(j).SubItems(7) = var1 Then
          LwErr.ListItems(j).SubItems(8) = ""
          
          On Error GoTo tabellaInesistente
          DLLFunzioni.FnConnection.Execute ("Delete * From bs_Segnalazioni_Ignore WHERE code = '" & Code & "' AND var1 = '" & var1 & "'")
          On Error GoTo 0
          
        End If
      Next j
      
      DLLFunzioni.FnActiveWindowsBool = True
    End If
  Next i
    
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  lbParser.Visible = True
  lbParser.Caption = "Selected Objects Unignored"

  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "ML04E"
  lbParser.Visible = True
  lbParser.Caption = "Problems during Ignoring Objects"
  Exit Sub
tabellaInesistente:
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  lbParser.Visible = True
  lbParser.Caption = "Exception: Check-Repository needed."
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''
'MF - 13/07/07
'Gestione pulsanti dropDown del men�
'''''''''''''''''''''''''''''''''''''''''''''''
Private Sub fraToolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Key
    Case "IGNORE"
      If DLLFunzioni.FnProcessRunning Then  'SQ portarli qui tutti sti controlli!
        DLLFunzioni.Show_MsgBoxError "FB01I"
      Else
        ignoreError
      End If
    Case "UNIGNORE"
      If DLLFunzioni.FnProcessRunning Then  'SQ portarli qui tutti sti controlli!
        DLLFunzioni.Show_MsgBoxError "FB01I"
      Else
        unignoreError
      End If
    Case "DELETE"
      deleteObject
    Case "DELETE_OBJLST"
      deleteObject_List
    Case "DELETE_CALLST"
      deleteObjCall_List
  End Select
End Sub

''''''''''''''''''''''''''''''''''''''''''''''
'MF - 11-07-07
'Gestione lettura da file
''''''''''''''''''''''''''''''''''''''''''''''
Public Function readFile(dialogFileName As String) As String()
  Dim values() As String
  Dim FD As Long
  Dim line As String
  
  ReDim values(0)
  
  FD = FreeFile
  Open dialogFileName For Input As FD
  While Not EOF(FD)
    Line Input #FD, line
    If Len(Trim(line)) Then
      ReDim Preserve values(UBound(values) + 1)
      values(UBound(values)) = Trim(line)
    Else
      'riga vuota: buttiamo via
    End If
  Wend
  Close FD
    
  readFile = values
End Function

Sub ExtEdit(strTxtEditor As String)
  'S 9-1-2008 apertura editor esterno con pi� elementi selezionati
  Dim strDir As String, strName As String
  Dim strFileSel As String
  Dim j As Long, k As Long
  Dim ArrFileTxtEd() As String
  Dim rs As Recordset
  
  k = 0
  ReDim ArrFileTxtEd(k)
  For j = 1 To LwErr.ListItems.count
    If LwErr.ListItems(j).Selected Then
      ' Ricerca Directory da Bs_oggetti
      Set rs = m_fun.Open_Recordset("Select Directory_Input From BS_Oggetti Where " & _
                                    "IdOggetto = " & LwErr.ListItems(j) & " and " & _
                                    "tipo = '" & LwErr.ListItems(j).ListSubItems(2) & "'")
      If rs.RecordCount Then
        strDir = rs!directory_input & ""
      End If
      rs.Close
      strDir = Crea_Directory_Progetto(Trim(strDir), ProjectPath)
      strName = Trim(m_fun.Restituisci_NomeOgg_Da_IdOggetto(LwErr.ListItems(j)))
      ArrFileTxtEd(k) = strDir & "\" & strName
      ArrFileTxtEd(k) = """" & ArrFileTxtEd(k) & """"
      k = k + 1
      ReDim Preserve ArrFileTxtEd(k)
    End If
  Next j
  For j = 0 To UBound(ArrFileTxtEd) - 1
    strFileSel = strFileSel & " " & ArrFileTxtEd(j)
  Next j
  If Len(Dir(strTxtEditor)) Then
    Shell strTxtEditor & " " & strFileSel, vbMaximizedFocus
  Else
    MsgBox strTxtEditor & vbCrLf & _
           "External Editor not found!", vbOKOnly + vbExclamation, GbNomeProdotto
  End If
End Sub

Private Sub txtNome_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
