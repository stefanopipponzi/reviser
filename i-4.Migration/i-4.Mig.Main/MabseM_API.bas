Attribute VB_Name = "MabseM_API"
Option Explicit

Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, _
                                                                              ByVal lpOperation As String, _
                                                                              ByVal lpFile As String, _
                                                                              ByVal lpParameters As String, _
                                                                              ByVal lpDirectory As String, _
                                                                              ByVal nShowCmd As Long) As Long

'A.P.I. per prendere il nome del computer e il serial del disco
Declare Function GetVolumeInformation Lib "kernel32" Alias "GetVolumeInformationA" (ByVal RootPathName As String, _
                                                                                    ByVal VolumeName As String, _
                                                                                    ByVal VolumeNameSize As Long, _
                                                                                    ByRef SerialNumber As Long, _
                                                                                    ByRef MaxFileLen As Long, _
                                                                                    ByRef FileSystemFlags As Long, _
                                                                                    ByVal FileSystemName As String, _
                                                                                    ByVal FileSystemNameSize As Long) As Long
   
Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpbuffer As String, _
                                                                          nSize As Long) As Long

'A.P.I. per gestire un file INI
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
                                                                                                 ByVal lpKeyName As Any, _
                                                                                                 ByVal lpDefault As String, _
                                                                                                 ByVal lpReturnedString As String, _
                                                                                                 ByVal nSize As Long, _
                                                                                                 ByVal lpFileName As String) As Long
'ferma l'effetto flickering delle form
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hWndLock As Long) As Long

'API per rimuovere i bottoni di max e min sulla mdi
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwnewlong As Long) As Long
Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Public Const WS_MINIMIZEBOX = &H20000
Public Const WS_MAXIMIZEBOX = &H10000
Public Const GWL_STYLE = (-16)

Public Declare Function GetDesktopWindow Lib "user32" () As Long
Public Declare Function GetDC Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function ReleaseDC Lib "user32" (ByVal hwnd As Long, ByVal hdc As Long) As Long
Public Const LOGPIXELSX = 88

' Remove MDIForm Max/Min Buttons
Public Declare Function SetWindowPos Lib "user32" ()

Public Const HWND_TOPMOST = -1

Public Const SWP_NOMOVE = &H2
Public Const SWP_NOSIZE = &H1
Public Const SWP_NOREPOSITION = &H200

Public Declare Function SetFocusWnd Lib "user32.dll" Alias "SetFocus" (ByVal hwnd As Long) As Long

Public Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpbuffer As String, nSize As Long) As Long
Public Declare Function SetFileAttributes Lib "kernel32" Alias "SetFileAttributesA" (ByVal lpFileName As String, ByVal dwFileAttributes As Long) As Long
''''''''''''''''''''''''''''''
' Toolbar Background
''''''''''''''''''''''''''''''
Public Declare Function CreateSolidBrush Lib "gdi32" (ByVal crColor As Long) As Long
Private Declare Function SetClassLong Lib "user32" Alias "SetClassLongA" ( _
                ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwnewlong As Long) As Long
Private Declare Function deleteObject Lib "gdi32" Alias "DeleteObject" (ByVal hObject As Long) As Long
Private Declare Function FindWindowEx Lib "user32" Alias "FindWindowExA" ( _
    ByVal hWnd1 As Long, ByVal hWnd2 As Long, _
    ByVal lpsz1 As String, ByVal lpsz2 As String) As Long
Public Declare Function InvalidateRect Lib "user32" _
                (ByVal hwnd As Long, lpRect As Long, ByVal bErase As Long) As Long

Public Enum enuTBType
    enuTB_FLAT = 1
    enuTB_STANDARD = 2
End Enum

Private Const GCL_HBRBACKGROUND = (-10)
Public Sub Elimina_Flickering(ByVal lHWnd As Long)
  Dim lRet As Long
  
  lRet = LockWindowUpdate(lHWnd)
End Sub

Public Sub Terminate_Flickering(Optional ByVal lHWnd As Long = 0)
  Dim lRet As Long
  
  lRet = LockWindowUpdate(lHWnd)
End Sub

Public Function getInformation() As String
  Dim root As String
  Dim volume_name As String
  Dim serial_number As Long
  Dim max_component_length As Long
  Dim file_system_flags As Long
  Dim file_system_name As String
  Dim pos As Integer
  Dim wApp As String
  Dim wStr As String
  Dim s() As String
  
  getInformation = ""
  
  'IRIS-DB nuovo problema sul disco mappato, per ora metto un work-around fisso
  If InStr(GbPathPrd, ":") Then
    s = Split(GbPathPrd, ":")
    root = s(0) & ":\"
  Else
    root = "Z:\"
  End If
  
  volume_name = Space$(1024)
  file_system_name = Space$(1024)
  
  If GetVolumeInformation(root, volume_name, _
                          Len(volume_name), serial_number, _
                          max_component_length, file_system_flags, _
                          file_system_name, Len(file_system_name)) = 0 Then
    MsgBox "Error Access/Write Cluster Disk Volume Information", vbInformation, "Contact TRechnical Support"
    End
  End If
  
  pos = InStr(volume_name, Chr$(0))
  volume_name = Left$(volume_name, pos - 1)
  
  pos = InStr(file_system_name, Chr$(0))
  file_system_name = Left$(file_system_name, pos - 1)
  
  wStr = Hex$(serial_number)

  getInformation = String(8 - Len(wStr), "0") & wStr
End Function

''''''''''''''''''''''''''''''
' Toolbar Background
''''''''''''''''''''''''''''''
Public Sub ChangeTBBack(tb As Object, PNewBack As Long, pType As enuTBType)
Dim lTBWnd      As Long
    Select Case pType
        Case enuTB_FLAT     'FLAT Button Style Toolbar
            'Apply directly to TB Hwnd
            deleteObject SetClassLong(tb.hwnd, GCL_HBRBACKGROUND, PNewBack)
        Case enuTB_STANDARD 'STANDARD Button Style Toolbar
            lTBWnd = FindWindowEx(tb.hwnd, 0, "msvb_lib_toolbar", vbNullString) 'Find Hwnd first
            deleteObject SetClassLong(lTBWnd, GCL_HBRBACKGROUND, PNewBack)      'Set new Back
    End Select
End Sub

