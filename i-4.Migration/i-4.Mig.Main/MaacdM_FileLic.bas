Attribute VB_Name = "MaacdM_FileLic"
Option Explicit

Global ACT As New MaACTXXXX

Global FileLIC() As String

Global Const OPEN_LICENSE = "REVISER"
Global Const OPEN_LICENSE_PC = "SUNCOURSE" 'SQ tmp test

Public Function Legge_Dati_Da_FileLIC(Dato As String, Percorso As String) As String
  Dim Parametro As String
  Dim Stringa As String
  Dim linea As String
  Dim i As Long
  
  Open Percorso For Input As #1
  
  Do While Not EOF(1)
    Line Input #1, linea
    
    If InStr(1, linea, Dato) <> 0 Then
     Parametro = Isola_Parametro(linea, "#")
     Exit Do
    End If
  Loop
  
  Legge_Dati_Da_FileLIC = Parametro
  
  Close #1
End Function
'*******************************************
'**** Apre un Set di Recordset Ado  ********
'*******************************************
Function Open_Recordset(sql As String, VarRecord As ADODB.Recordset, Optional OpenMode As Long, Optional LockMode As Long) As ADODB.Recordset
  Set VarRecord = New ADODB.Recordset
  
    If OpenMode <> 0 Then
      If LockMode <> 0 Then
        VarRecord.Open sql, ACT.AcConnection, OpenMode, LockMode, adCmdText
      Else
        VarRecord.Open sql, ACT.AcConnection, OpenMode, adLockOptimistic, adCmdText
      End If
    Else
      If LockMode <> 0 Then
        VarRecord.Open sql, ACT.AcConnection, adOpenStatic, LockMode, adCmdText
      Else
        VarRecord.Open sql, ACT.AcConnection, adOpenStatic, adLockOptimistic, adCmdText
      End If
    End If
  
  Set Open_Recordset = VarRecord
End Function

Public Function Restituisci_NomeOgg_Da_IdOggetto(id As Long) As String
  'restituisce la directory dall'id oggetto secondo il tipo INPUT,OUTPUT
  Dim r As ADODB.Recordset
  
  Set r = Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & id, r)
  
  If r.RecordCount > 0 Then
    r.MoveFirst
        
     Restituisci_NomeOgg_Da_IdOggetto = r!Nome
        
  End If
End Function

