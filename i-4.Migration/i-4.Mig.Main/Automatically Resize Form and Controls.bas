Attribute VB_Name = "Resize"
Option Explicit

Public Type ctrObj
  Name As String
  Index As Long
  Parrent As String
  Top As Long
  Left As Long
  Height As Long
  Width As Long
  ScaleHeight As Long
  ScaleWidth As Long
End Type

Private FormRecord() As ctrObj
Private ControlRecord() As ctrObj
Private bRunning As Boolean
Private MaxForm As Long
Private MaxControl As Long
Public formParentResize As Form

'**********************
Public Type SECURITY_ATTRIBUTES
    nLength As Long
    lpSecurityDescriptor As Long
    bInheritHandle As Long
    End Type
    '***Constants
   Public Const FILE_READ_DATA = (&H1)  ' file pipe
   Public Const FILE_READ_EA = (&H8)  ' file directory
   Public Const FILE_READ_PROPERTIES = FILE_READ_EA
   Public Const FILE_SHARE_READ = &H1
   Public Const FILE_SHARE_WRITE = &H2
   Public Const OPEN_ALWAYS = 4
    Public Const OPEN_EXISTING = 3
   Public Const FILE_ATTRIBUTE_NORMAL = &H80
    '***Declaration

Public Declare Function CreateFile _
    Lib "kernel32" Alias "CreateFileA" _
    (ByVal lpFileName As String, _
    ByVal dwDesiredAccess As Long, _
    ByVal dwShareMode As Long, _
    lpSecurityAttributes As SECURITY_ATTRIBUTES, _
    ByVal dwCreationDisposition As Long, _
    ByVal dwFlagsAndAttributes As Long, _
    ByVal hTemplateFile As Long) As Long
    '******Used to close the handle******'


Public Declare Function CloseHandle _
    Lib "kernel32" _
    (ByVal hObject As Long) As Long
    '******Used to get and convert the file
    '     Dates and Times******'
    '***Typesp


Public Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
    End Type


Public Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
    End Type


Public Type TIME_ZONE_INFORMATION
    Bias As Long
    StandardName(32) As Integer
    StandardDate As SYSTEMTIME
    StandardBias As Long
    DaylightName(32) As Integer
    DaylightDate As SYSTEMTIME
    DaylightBias As Long
    End Type
    '***Declarations


Public Declare Function GetFileTime _
    Lib "kernel32" _
    (ByVal hFile As Long, _
    lpCreationTime As FILETIME, _
    lpLastAccessTime As FILETIME, _
    lpLastWriteTime As FILETIME) As Long


Public Declare Function FileTimeToLocalFileTime _
    Lib "kernel32" _
    (lpFileTime As FILETIME, _
    lpLocalFileTime As FILETIME) As Long


Public Declare Function FileTimeToSystemTime _
    Lib "kernel32" _
    (lpFileTime As FILETIME, _
    lpSystemTime As SYSTEMTIME) As Long


Public Declare Function SystemTimeToTzSpecificLocalTime _
    Lib "kernel32" _
    (lpTimeZoneInformation As TIME_ZONE_INFORMATION, _
    lpUniversalTime As SYSTEMTIME, _
    lpLocalTime As SYSTEMTIME) As Long
    '******Used to get the file size******'


Public Declare Function GetFileSize _
    Lib "kernel32" _
    (ByVal hFile As Long, _
    lpFileSizeHigh As Long) As Long

Public Sub GetFileInformation(ByVal fileName As String, crDate As String, lmDate As String, laDate As String, fileSize As Long)
    Dim hFile As Long
    Dim crFileDate As FILETIME
    Dim lmFileDate As FILETIME
    Dim laFileDate As FILETIME
    Dim crSysDate As SYSTEMTIME
    Dim lmSysDate As SYSTEMTIME
    Dim laSysDate As SYSTEMTIME
    Dim fileHigh As Long
    Dim sec As SECURITY_ATTRIBUTES
    On Error GoTo errControl
    'get the handler
    hFile = CreateFile(fileName, FILE_READ_DATA, FILE_SHARE_READ, sec, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0)
    'get the file size using the handler
    fileSize = GetFileSize(hFile, fileHigh)
    'Get the file dates
    GetFileTime hFile, crFileDate, laFileDate, lmFileDate
    FileTimeToLocalFileTime crFileDate, crFileDate
    FileTimeToLocalFileTime lmFileDate, lmFileDate
    FileTimeToLocalFileTime laFileDate, laFileDate
    'convert the dates to system dates
    FileTimeToSystemTime crFileDate, crSysDate
    FileTimeToSystemTime laFileDate, laSysDate
    FileTimeToSystemTime lmFileDate, lmSysDate
    crDate = Format$(crSysDate.wMonth & "/" & crSysDate.wDay & "/" & crSysDate.wYear & " " & crSysDate.wHour & ":" & crSysDate.wMinute & ":" & crSysDate.wSecond, "mm/d/yyyy hh:mm:ss AMPM")
    lmDate = Format$(lmSysDate.wMonth & "/" & lmSysDate.wDay & "/" & lmSysDate.wYear & " " & lmSysDate.wHour & ":" & lmSysDate.wMinute & ":" & lmSysDate.wSecond, "mm/d/yyyy hh:mm:ss AMPM")
    laDate = Format$(laSysDate.wMonth & "/" & laSysDate.wDay & "/" & laSysDate.wYear, "mm/d/yyyy")
    CloseHandle hFile
errControl:
    Exit Sub
    Resume Next
End Sub
        


'**********************

Private Function ActualPos(plLeft As Long) As Long
  If plLeft < 0 Then
    ActualPos = plLeft + 75000
  Else
    ActualPos = plLeft
  End If
End Function

Private Function FindForm(pfrmIn As Form) As Long
  Dim i As Long
  
  FindForm = -1
  If MaxForm > 0 Then
    For i = 0 To (MaxForm - 1)
      If FormRecord(i).Name = pfrmIn.Name Then
        FindForm = i
        Exit Function
      End If
    Next i
  End If
End Function


Private Function AddForm(pfrmIn As Form) As Long
  Dim FormControl As Control
  Dim i As Long
  ReDim Preserve FormRecord(MaxForm + 1)

  FormRecord(MaxForm).Name = pfrmIn.Name
  FormRecord(MaxForm).Top = pfrmIn.Top
  FormRecord(MaxForm).Left = pfrmIn.Left
  FormRecord(MaxForm).Height = pfrmIn.Height
  FormRecord(MaxForm).Width = pfrmIn.Width
  FormRecord(MaxForm).ScaleHeight = pfrmIn.ScaleHeight

  FormRecord(MaxForm).ScaleWidth = pfrmIn.ScaleWidth
  AddForm = MaxForm
  MaxForm = MaxForm + 1

  For Each FormControl In pfrmIn
    i = FindControl(FormControl, pfrmIn.Name)
    If i < 0 Then i = AddControl(FormControl, pfrmIn.Name)
  Next FormControl
End Function

Private Function FindControl(inControl As Control, inName As String) As Long
  Dim i As Long
  
  FindControl = -1
  For i = 0 To (MaxControl - 1)
    If ControlRecord(i).Parrent = inName Then
      If ControlRecord(i).Name = inControl.Name Then
        On Error Resume Next
        
        If ControlRecord(i).Index = inControl.Index Then
          FindControl = i
          Exit Function
        End If
        On Error GoTo 0
      
      End If
    End If
  Next i
End Function

Private Function AddControl(inControl As Control, inName As String) As Long
  ReDim Preserve ControlRecord(MaxControl + 1)
  On Error Resume Next
  
  ControlRecord(MaxControl).Name = inControl.Name
  ControlRecord(MaxControl).Index = inControl.Index
  ControlRecord(MaxControl).Parrent = inName

  If TypeOf inControl Is line Then
    ControlRecord(MaxControl).Top = inControl.Y1
    ControlRecord(MaxControl).Left = ActualPos(inControl.X1)
    ControlRecord(MaxControl).Height = inControl.Y2
    ControlRecord(MaxControl).Width = ActualPos(inControl.X2)
  Else
    ControlRecord(MaxControl).Top = inControl.Top
    ControlRecord(MaxControl).Left = ActualPos(inControl.Left)
    ControlRecord(MaxControl).Height = inControl.Height
    ControlRecord(MaxControl).Width = inControl.Width
  End If

  inControl.IntegralHeight = False
  
  On Error GoTo 0
  AddControl = MaxControl
  MaxControl = MaxControl + 1
End Function

Private Function PerWidth(pfrmIn As Form) As Long
  Dim i As Long
  
  i = FindForm(pfrmIn)
  If i < 0 Then i = AddForm(pfrmIn)
  
  PerWidth = (pfrmIn.ScaleWidth * 100) \ FormRecord(i).ScaleWidth
End Function

Private Function PerHeight(pfrmIn As Form) As Single
  Dim i As Long
  
  i = FindForm(pfrmIn)
  If i < 0 Then i = AddForm(pfrmIn)
  
  PerHeight = (pfrmIn.ScaleHeight * 100) \ FormRecord(i).ScaleHeight
End Function

Private Sub ResizeControl(inControl As Control, pfrmIn As Form)
  On Error Resume Next
  Dim i As Long
  Dim widthfactor As Single, heightfactor As Single
  Dim minFactor As Single
  Dim yRatio, xRatio, lTop, lLeft, lWidth, lHeight As Long
  
  
  yRatio = PerHeight(pfrmIn)
  xRatio = PerWidth(pfrmIn)
  
  i = FindControl(inControl, pfrmIn.Name)

  If inControl.Left < 0 Then
    lLeft = CLng(((ControlRecord(i).Left * xRatio) \ 100) - 75000)
  Else
    lLeft = CLng((ControlRecord(i).Left * xRatio) \ 100)
  End If

  lTop = CLng((ControlRecord(i).Top * yRatio) \ 100)
  'ALE fa il resize solo se il tag � diverso da fixed
  If inControl.Tag <> "fixed" Then
    lWidth = CLng((ControlRecord(i).Width * xRatio) \ 100)
    lHeight = CLng((ControlRecord(i).Height * yRatio) \ 100)
  Else
      lWidth = CLng((ControlRecord(i).Width))
      lHeight = CLng((ControlRecord(i).Height))
      'per permettere il rimpicciolimento dei controlli
      '    If yRatio <= 100 Or xRatio <= 100 Then
      '        lWidth = CLng((ControlRecord(i).Width * xRatio) \ 100)
      '        lHeight = CLng((ControlRecord(i).Height * yRatio) \ 100)
      '    Else
      '      lWidth = CLng((ControlRecord(i).Width))
      '      lHeight = CLng((ControlRecord(i).Height))
      '    End If
  End If
  
  If TypeOf inControl Is line Then
    If inControl.X1 < 0 Then
      inControl.X1 = CLng(((ControlRecord(i).Left * xRatio) \ 100) - 75000)
    Else
      inControl.X1 = CLng((ControlRecord(i).Left * xRatio) \ 100)
    End If
    
    inControl.Y1 = CLng((ControlRecord(i).Top * yRatio) \ 100)
    If inControl.X2 < 0 Then
      inControl.X2 = CLng(((ControlRecord(i).Width * xRatio) \ 100) - 75000)
    Else
      inControl.X2 = CLng((ControlRecord(i).Width * xRatio) \ 100)
    End If

    inControl.Y2 = CLng((ControlRecord(i).Height * yRatio) \ 100)
  Else
    inControl.Move lLeft, lTop, lWidth, lHeight
    inControl.Move lLeft, lTop, lWidth
    inControl.Move lLeft, lTop
  End If
End Sub

Public Sub ResizeForm(pfrmIn As Form)
  Dim FormControl As Control
  Dim isVisible As Boolean
  Dim StartX, StartY, MaxX, MaxY As Long
  Dim bNew As Boolean
  
  If Not bRunning Then
    bRunning = True
    
    If FindForm(pfrmIn) < 0 Then
      bNew = True
    Else
      bNew = False
    End If

    If pfrmIn.Top < 30000 Then
      isVisible = pfrmIn.Visible
      On Error Resume Next
      
      If Not pfrmIn.MDIChild Then
        On Error GoTo 0
        'pfrmIn.Visible = False
      Else
        If bNew Then
          StartY = pfrmIn.Height
          StartX = pfrmIn.Width
          On Error Resume Next

          For Each FormControl In pfrmIn
            If FormControl.Left + FormControl.Width + 200 > MaxX Then _
              MaxX = FormControl.Left + FormControl.Width + 200
            If FormControl.Top + FormControl.Height + 500 > MaxY Then _
              MaxY = FormControl.Top + FormControl.Height + 500
            If FormControl.X1 + 200 > MaxX Then _
              MaxX = FormControl.X1 + 200
            If FormControl.Y1 + 500 > MaxY Then _
              MaxY = FormControl.Y1 + 500
            If FormControl.X2 + 200 > MaxX Then _
              MaxX = FormControl.X2 + 200
            If FormControl.Y2 + 500 > MaxY Then _
              MaxY = FormControl.Y2 + 500
          Next FormControl
          On Error GoTo 0
          
          pfrmIn.Height = MaxY
          pfrmIn.Width = MaxX
        End If
        On Error GoTo 0

      End If
      
      For Each FormControl In pfrmIn
          ResizeControl FormControl, pfrmIn
      Next FormControl
      On Error Resume Next

      If Not pfrmIn.MDIChild Then
        On Error GoTo 0
        pfrmIn.Visible = isVisible
      Else
        If bNew Then
          pfrmIn.Height = StartY
          pfrmIn.Width = StartX
          
          For Each FormControl In pfrmIn
            ResizeControl FormControl, pfrmIn
          Next FormControl
        End If
      End If
      On Error GoTo 0
      
    End If
    bRunning = False
  End If
End Sub

Public Sub SaveFormPosition(pfrmIn As Form)
  Dim i As Long

  If MaxForm > 0 Then
    For i = 0 To (MaxForm - 1)
      If FormRecord(i).Name = pfrmIn.Name Then
        FormRecord(i).Top = pfrmIn.Top
        FormRecord(i).Left = pfrmIn.Left
        FormRecord(i).Height = pfrmIn.Height
        FormRecord(i).Width = pfrmIn.Width
        Exit Sub
      End If
    Next i
    AddForm (pfrmIn)
  End If
End Sub

Public Sub RestoreFormPosition(pfrmIn As Form)
  Dim i As Long

  If MaxForm > 0 Then
    For i = 0 To (MaxForm - 1)
      If FormRecord(i).Name = pfrmIn.Name Then
        If FormRecord(i).Top < 0 Then
          pfrmIn.WindowState = 2
        ElseIf FormRecord(i).Top < 30000 Then
          pfrmIn.WindowState = 0
          pfrmIn.Move FormRecord(i).Left, FormRecord(i).Top, FormRecord(i).Width, FormRecord(i).Height
        Else
          pfrmIn.WindowState = 1
        End If
        Exit Sub
      End If
    Next i
  End If
End Sub

