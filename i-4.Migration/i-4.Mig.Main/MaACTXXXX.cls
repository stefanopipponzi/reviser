VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MaACTXXXX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
'******************************************************************
'***** Variabili per caricare informazioni dal progetto base ******
'***** (Variabili di Passaggio...)                           ******
'******************************************************************
'                                                           '******
Public AcIdOggetto As Integer                               '******
'                                                           '******
'******************************************************************

Public AcDatabase As ADOX.Catalog 'oggetto
Public AcNomeDB As String
Public AcConnection As ADODB.Connection
Public AcConnState As Long
Public AcPathDB As String

'serve per sapere il path parametrico delle directory dei sorgenti
Public AcPathDef As String
Public AcUnitDef As String

Public AcTipoDB As String

'Se il Main vede lo status True allora l'utente pu� lavorare altrimenti
'Blocca tutto
Public AcStatusWork As Boolean

'serve per contenere il nome del prodotto e la versione che si deve visualizzare
'ad esempio nelle msgbox
Public AcNomeProdotto As String

Public AcMaxPGM As Long
Public AcMaxDBD As Long
Public AcCurDBD As Long
Public AcCurPGM As Long

Public AcNomeFileLic As String

Public AcStatusLIC As Boolean

Public Function AttivaFunzione(Funzione As String, Optional IdButton As String) As Boolean
  Select Case Trim(UCase(Funzione))
    Case ""
   
  End Select
End Function

Public Sub InitDll()
   Set ACT = Me
End Sub

Public Function Restituisci_ProcuctID(Percorso As String) As String
  
  Restituisci_ProcuctID = Legge_Dati_Da_FileLIC("H0002", Percorso)
  
End Function

Public Function Restituisci_NomePC(Percorso As String) As String
  
  Restituisci_NomePC = Legge_Dati_Da_FileLIC("H0001", Percorso)
  
End Function


Public Function Restituisci_Nome_Cliente(Percorso As String) As String
  Dim Nome1 As String
  Dim Nome2 As String
  Dim Nome3 As String
  
  Nome1 = Legge_Dati_Da_FileLIC("I0001", Percorso)
  Nome2 = Legge_Dati_Da_FileLIC("I0002", Percorso)
  Nome3 = Legge_Dati_Da_FileLIC("I0003", Percorso)
  
  Restituisci_Nome_Cliente = Nome1 & " " & Nome2 & " -- " & Nome3
End Function

Public Function Controlla_Molteplicita_FileLIC(Percorso As String) As String
  Dim Nome As String
  
  ReDim FileLIC(0)
  
  Nome = Dir(Percorso & "\*.lic", vbNormal)
  
  While Nome <> "" And Right(Nome, 3) = "lic" 'SQ: prendeva anche ".lic_"!?
    ReDim Preserve FileLIC(UBound(FileLIC) + 1)
    FileLIC(UBound(FileLIC)) = Nome
    
    Nome = Dir
  Wend
  
  If UBound(FileLIC) > 1 Then
    MaacdF_FileLic.Show vbModal
    AcStatusLIC = True 'SQ: forzo quella di default
  Else
    If UBound(FileLIC) = 0 Then
      AcStatusLIC = False
    Else
      ACT.AcNomeFileLic = FileLIC(1)
      AcStatusLIC = True
    End If
  End If
  If AcStatusLIC Then
    Controlla_Molteplicita_FileLIC = ACT.AcNomeFileLic
  End If
End Function



