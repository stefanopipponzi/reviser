VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MabseF_SelectDatabase 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   2235
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   7560
   Icon            =   "MabseF_SelectDatabase.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   2235
   ScaleWidth      =   7560
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "..."
      Height          =   345
      Left            =   2220
      TabIndex        =   7
      ToolTipText     =   "Select source files directory..."
      Top             =   2520
      Visible         =   0   'False
      Width           =   345
   End
   Begin VB.TextBox txtDefaultDatabase 
      BackColor       =   &H00FFFFC0&
      Enabled         =   0   'False
      ForeColor       =   &H00C00000&
      Height          =   330
      Left            =   5760
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   900
      Width           =   1665
   End
   Begin VB.TextBox txtPassword 
      BackColor       =   &H00FFFFC0&
      Enabled         =   0   'False
      ForeColor       =   &H00C00000&
      Height          =   330
      Left            =   5760
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   480
      Width           =   1665
   End
   Begin VB.TextBox txtUserName 
      BackColor       =   &H00FFFFC0&
      Enabled         =   0   'False
      ForeColor       =   &H00C00000&
      Height          =   330
      Left            =   5760
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   60
      Width           =   1665
   End
   Begin VB.TextBox txtIP 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   330
      Left            =   1350
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   900
      Width           =   2895
   End
   Begin VB.TextBox txtProvider 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   330
      Left            =   1350
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   480
      Width           =   2895
   End
   Begin VB.ComboBox cboDatabaseTypes 
      BackColor       =   &H00FFFFC0&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   360
      Left            =   1350
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   60
      Width           =   2895
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   240
      Picture         =   "MabseF_SelectDatabase.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   1410
      Width           =   945
   End
   Begin VB.CommandButton cmdNewProject 
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   6360
      Picture         =   "MabseF_SelectDatabase.frx":1E74
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   1410
      Width           =   945
   End
   Begin MSComctlLib.ImageList DatabaseImageList 
      Left            =   4560
      Top             =   2820
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_SelectDatabase.frx":3BB6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView DatabaseListView 
      Height          =   525
      Left            =   2850
      TabIndex        =   6
      Top             =   2490
      Visible         =   0   'False
      Width           =   1605
      _ExtentX        =   2831
      _ExtentY        =   926
      View            =   2
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      Icons           =   "DatabaseImageList"
      SmallIcons      =   "DatabaseImageList"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label lblDefaultDatabase 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000B&
      BackStyle       =   0  'Transparent
      Caption         =   "Default Database:"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   4410
      TabIndex        =   16
      Top             =   960
      Width           =   1305
   End
   Begin VB.Label lblPassword 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000B&
      BackStyle       =   0  'Transparent
      Caption         =   "Password:"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   4950
      TabIndex        =   15
      Top             =   540
      Width           =   750
   End
   Begin VB.Label lblUserName 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000B&
      BackStyle       =   0  'Transparent
      Caption         =   "User Name:"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   4815
      TabIndex        =   14
      Top             =   120
      Width           =   885
   End
   Begin VB.Label lblIP 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000B&
      BackStyle       =   0  'Transparent
      Caption         =   "IP:"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   1065
      TabIndex        =   13
      Top             =   960
      Width           =   225
   End
   Begin VB.Label lblProvider 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000B&
      BackStyle       =   0  'Transparent
      Caption         =   "Provider:"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   585
      TabIndex        =   12
      Top             =   540
      Width           =   705
   End
   Begin VB.Label lblBrowseIn 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000B&
      BackStyle       =   0  'Transparent
      Caption         =   "Browse In:"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   465
      TabIndex        =   11
      Top             =   120
      Width           =   825
   End
   Begin VB.Label lblDatabases 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Databases:"
      Height          =   195
      Left            =   2850
      TabIndex        =   8
      Top             =   2250
      Visible         =   0   'False
      Width           =   810
   End
End
Attribute VB_Name = "MabseF_SelectDatabase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private moConn As ADODB.Connection
Private bCommonDialog As Boolean
Private mbDatabaseCreationProcess As Boolean

Private Sub cboDatabaseTypes_Click()
  On Error GoTo EH
  If bCommonDialog Then Exit Sub
  
  If Me.cmdNewProject.Caption = "New" Then
    miDatabaseType = cboDatabaseTypes.ListIndex
    LoadTextFields cboDatabaseTypes.ListIndex
  End If
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub cboDatabaseTypes_KeyPress(KeyAscii As Integer)
  On Error GoTo EH
  
  KeyAscii = 0

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

'MF 09/08/07 dialogFileTitle pu� anche non essere pi� passato visto che prj.mti � salvato in ProjectName globale
Public Sub loadProject(dialogFileName As String, dialogFileTitle As String)
  Dim i As Long
  Dim wTipoDB As String
  
  If Trim(dialogFileName) = "" Then
    prjAlreadyOpen = False
    Exit Sub
  Else
    prjAlreadyOpen = True
  End If
  
  'Carica il file .mti con le informazioni
  Load_File_Mti dialogFileName
  
  txtDefaultDatabase.Text = GbCurDefaultDatabase
  txtIP.Text = GbCurServerIp
  txtPassword.Text = GbCurPassword
  txtProvider.Text = GbCurProvider
  txtUserName.Text = GbCurUserName
  
  If GbCurTipoDB = Access Then wTipoDB = "ACCESS"
  If GbCurTipoDB = DB2 Then wTipoDB = "DB2"
  If GbCurTipoDB = MYSQL Then wTipoDB = "MYSQL"
  If GbCurTipoDB = ORACLE Then wTipoDB = "ORACLE"
  If GbCurTipoDB = SQLSERVER Then wTipoDB = "SQLSERVER"
  
  bCommonDialog = True
  For i = 1 To UBound(arrDatabaseTypes)
    If Trim(UCase(Replace(arrDatabaseTypes(i), " ", ""))) = wTipoDB And arrDatabaseIPs(i) = GbCurServerIp Then
      cboDatabaseTypes.ListIndex = i
      miDatabaseType = i
      Exit For
    End If
  Next i
  bCommonDialog = False
End Sub

Private Sub cmdCancel_Click()
  On Error GoTo EH
  m_CreateYesNo = False
  
  Unload Me

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Connect_Db_ACCESS()
  Dim sConnectionString  As String
  
  Set moConn = New ADODB.Connection
  
  'Crea il database
  'MF 09/08/07 Sostituisco GbCurDBPath con ProjectPath e GbCurDBName con DatabaseName
  sConnectionString = "Provider=" & GbCurProvider & " ;" & _
                      "Data Source=" & ProjectPath & "\" & DatabaseName & " ;" & _
                      "Persist Security Info=False"
  moConn.Open sConnectionString
  
  Set ADOCnt = moConn
End Sub

Private Function Create_New_Db_ACCESS() As Boolean
  Dim sConnectionString  As String
  Dim wScelta As Long
   
  Set moConn = New ADODB.Connection
  Set DbPrj = New ADOX.Catalog
   
  If Dir(DatabaseName, vbNormal) <> "" Then
    wScelta = MsgBox("This Database already exists, do you want to overwrite this (Y/N)?", vbYesNo + vbQuestion, GbNomeProdotto)
    If wScelta = vbYes Then
      Kill DatabaseName
    End If
    
    If wScelta = vbNo Then
      Exit Function
    End If
  End If
  Create_New_Db_ACCESS = True
  DbPrj.Create "Provider=" & arrDatabaseProviders(miDatabaseType) & ";Data Source=" & ProjectPath & "\" & DatabaseName & ";"
  
  sConnectionString = "Provider=" & arrDatabaseProviders(miDatabaseType) & " ;" & _
                      "Data Source=" & ProjectPath & "\" & DatabaseName & " ;" & _
                      "Persist Security Info=False"
                  
  moConn.Open sConnectionString
  
  Set ADOCnt = moConn
End Function

Private Sub Connect_Db_SQLSERVER()
  Dim sConnectionString As String
   
  Set moConn = New ADODB.Connection
  sConnectionString = "driver={" & arrDatabaseTypes(miDatabaseType) & "};" & _
                      "server=" & arrDatabaseIPs(miDatabaseType) & ";" & _
                      "uid=" & arrDatabaseUserNames(miDatabaseType) & ";" & _
                      "pwd=" & arrDatabasePasswords(miDatabaseType) & ";" & _
                      "database=" & DatabaseName
  moConn.Open sConnectionString

  Set ADOCnt = moConn
End Sub

Private Function Create_New_Db_SQLSERVER() As Boolean
  Dim i As Integer
  Dim bFound As Boolean
  Dim sConnectionString As String
  Dim wDatabase As String
  Dim wScelta As Long
   
  If Trim(DatabaseName) <> vbNullString Then
    wDatabase = Replace(UCase(ProjectName), ".MTI", "")
    ProjectName = wDatabase
    If Dir(DatabaseName, vbNormal) <> "" Then
      wScelta = MsgBox("This Database already exists, do you want to overwrite this (Y/N)?", vbYesNo + vbQuestion, GbNomeProdotto)
      If wScelta = vbYes Then
        Kill DatabaseName
      Else
        Exit Function
      End If
    End If
    
    Create_New_Db_SQLSERVER = True
    
    bFound = False

    For i = 0 To UBound(arrDatabaseNames)
      If arrDatabaseNames(i) = Trim(Replace(wDatabase, " ", "")) Then
        bFound = True
      End If
    Next i
    
    If Not bFound Then
      Set moConn = New ADODB.Connection
      sConnectionString = "driver={" & arrDatabaseTypes(miDatabaseType) & "}" & _
                          ";server=" & arrDatabaseIPs(miDatabaseType) & _
                          ";uid=" & arrDatabaseUserNames(miDatabaseType) & _
                          ";pwd=" & arrDatabasePasswords(miDatabaseType) & _
                          ";database="
      moConn.Open sConnectionString
      mbDatabaseCreationProcess = True
      Me.MousePointer = vbHourglass
      moConn.Execute "CREATE DATABASE " & Trim(Replace(wDatabase, " ", ""))
      Me.MousePointer = vbDefault
      mbDatabaseCreationProcess = False

      'At this point, Connect to New Database in i-4.Migration instead of loading List View with new name.
      sConnectionString = "driver={" & arrDatabaseTypes(miDatabaseType) & "}" & _
                          ";server=" & arrDatabaseIPs(miDatabaseType) & _
                          ";uid=" & arrDatabaseUserNames(miDatabaseType) & _
                          ";pwd=" & arrDatabasePasswords(miDatabaseType) & _
                          ";database=" & Trim(wDatabase)
      ADOCnt.Open sConnectionString
    Else
      wScelta = MsgBox("The database name of """ & Trim(Replace(wDatabase, " ", "")) & """ already exists." & vbCrLf & "Do you want overwrite it?", vbInformation + vbYesNo, "Database Creation Error")
      If wScelta = vbYes Then
        moConn.Execute "DROP DATABASE " & Trim(Replace(wDatabase, " ", ""))
        Resume
      End If
    End If
  Else
    MsgBox "Please type a database name.", vbInformation + vbOKOnly, "Database Creation Error"
  End If
End Function

Private Sub cmdNewProject_Click()
  CreateNewProject
End Sub

Private Sub DatabaseListView_ItemClick(ByVal Item As MSComctlLib.ListItem)
  On Error GoTo EH

  DatabaseName = arrDatabaseNames(DatabaseListView.SelectedItem.Index - 1)

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Form_Load()
  On Error GoTo EH
  
  m_CreateYesNo = True
  LoadDatabaseTypeCombo

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub LoadDatabaseListView(bShowLastRecord As Boolean, bSetDatabaseName As Boolean)
  Dim rsDatabases As Recordset
  Dim sConnectionString As String
  Dim i As Integer

  On Error GoTo EH
    
  DatabaseListView.ListItems.Clear
  DatabaseName = vbNullString

  For i = 0 To UBound(arrDatabaseNames)
    arrDatabaseNames(i) = vbNullString
  Next i

  If Not mbDatabaseCreationProcess Then
    If arrDatabaseTypes(miDatabaseType) = "SQL Server" Then
      Set moConn = New ADODB.Connection
          
      sConnectionString = "driver={" & arrDatabaseTypes(miDatabaseType) & "}" & _
                          ";server=" & arrDatabaseIPs(miDatabaseType) & _
                          ";uid=" & arrDatabaseUserNames(miDatabaseType) & _
                          ";pwd=" & arrDatabasePasswords(miDatabaseType) & _
                          ";database="
      moConn.Open sConnectionString
      rsDatabases.Open "SELECT CATALOG_NAME FROM Information_Schema.Schemata ORDER BY CATALOG_NAME", moConn
      i = 0
      Do While Not rsDatabases.EOF
        arrDatabaseNames(i) = rsDatabases.fields("CATALOG_NAME")
        DatabaseListView.ListItems.Add i + 1, , arrDatabaseNames(i), 1, 1
        rsDatabases.MoveNext
        i = i + 1
      Loop
          
      If DatabaseListView.ListItems.count > 0 Then
        If bShowLastRecord Then
          DatabaseListView.ListItems.Item(DatabaseListView.ListItems.count).Selected = True
          If bSetDatabaseName = True Then
            DatabaseName = arrDatabaseNames(DatabaseListView.ListItems.count - 1)
          End If
        Else
          DatabaseListView.ListItems.Item(1).Selected = True
          If bSetDatabaseName = True Then
            DatabaseName = arrDatabaseNames(0)
          End If
        End If
      End If
      
      If Not moConn Is Nothing Then
        moConn.Close
        Set moConn = Nothing
      End If
    End If
  End If
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Public Sub SetFormAndButtonText(sFormTitle As String, sButtonText As String)
  On Error GoTo EH
  
  Me.Caption = sFormTitle
  Me.cmdNewProject.Caption = sButtonText
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub LoadDatabaseTypeCombo()
  Dim wRet As Long
  Dim wBuff As String
  Dim i As Integer
  Dim bEOFFound As Boolean
    
  On Error GoTo EH
    
  i = 0
  bEOFFound = False

  Do Until bEOFFound
    bEOFFound = True

    wBuff = String(100, Chr(10))
    wRet = GetPrivateProfileString("Database" & CStr(i), "Type", "", wBuff, 100, GbPathPrd & "\" & SYSTEM_DIR & "\DatabaseConfig.ini")
    If wRet <> 0 Then
      arrDatabaseTypes(i) = Mid(wBuff, 1, wRet)
      arrDatabaseProviders(i) = DLLFunzioni.GetProviderByType(UCase(Trim(Replace(arrDatabaseTypes(i), " ", ""))))
      bEOFFound = False
    End If

    wBuff = String(100, Chr(10))
    wRet = GetPrivateProfileString("Database" & CStr(i), "IP", "", wBuff, 100, GbPathPrd & "\" & SYSTEM_DIR & "\DatabaseConfig.ini")
    If wRet <> 0 Then
      arrDatabaseIPs(i) = Mid(wBuff, 1, wRet)
    End If

    wBuff = String(100, Chr(10))
    wRet = GetPrivateProfileString("Database" & CStr(i), "UserName", "", wBuff, 100, GbPathPrd & "\" & SYSTEM_DIR & "\DatabaseConfig.ini")
    If wRet <> 0 Then
      arrDatabaseUserNames(i) = Mid(wBuff, 1, wRet)
    End If
    
    wBuff = String(100, Chr(10))
    wRet = GetPrivateProfileString("Database" & CStr(i), "Password", "", wBuff, 100, GbPathPrd & "\" & SYSTEM_DIR & "\DatabaseConfig.ini")
    If wRet <> 0 Then
      arrDatabasePasswords(i) = Mid(wBuff, 1, wRet)
    End If
    
    wBuff = String(100, Chr(10))
    wRet = GetPrivateProfileString("Database" & CStr(i), "DefaultDatabase", "", wBuff, 100, GbPathPrd & "\" & SYSTEM_DIR & "\DatabaseConfig.ini")
    If wRet <> 0 Then
      arrDefaultDatabases(i) = Mid(wBuff, 1, wRet)
    End If

    If bEOFFound = False Then
      cboDatabaseTypes.AddItem arrDatabaseTypes(i) & " - " & arrDatabaseIPs(i), i
    End If
    i = i + 1
  Loop

  If cboDatabaseTypes.ListCount > 0 Then
    cboDatabaseTypes.ListIndex = 0
    miDatabaseType = 0
    txtProvider.Text = arrDatabaseProviders(0)
    txtIP.Text = arrDatabaseIPs(0)
    txtUserName.Text = arrDatabaseUserNames(0)
    txtPassword.Text = arrDatabasePasswords(0)
    txtDefaultDatabase.Text = arrDefaultDatabases(0)
  End If
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub LoadTextFields(iIndex As Integer)
  On Error GoTo EH
  
  txtProvider.Text = arrDatabaseProviders(iIndex)
  txtIP.Text = arrDatabaseIPs(iIndex)
  
  txtUserName.Text = arrDatabaseUserNames(iIndex)
  txtPassword.Text = arrDatabasePasswords(iIndex)
  txtDefaultDatabase.Text = arrDefaultDatabases(iIndex)
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

' Ma 15/04/2008 : Intercetto la chiusura del form
'    0 - � stato scelto il comando Chiudi dal menu di controllo del form
'    1 - L'istruzione Unload viene richiamata dal codice
'    2 - La sessione corrente dell'ambiente operativo Microsoft Windows � conclusa
'    3 - L'applicazione � stata chiusa tramite Task Manager di Microsoft Windows
'    4 - Il form secondario MDI viene chiuso in quanto viene chiuso il form MDI principale
'    5 - Il form viene chiuso in quanto viene chiuso il proprietario
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  ' Ho scelto di mettere solo il caso <> 1 perch� in tutti gli altri casi,
  'potrei rischiare di brasare il DB di progetto
  If UnloadMode <> 1 Then
    m_CreateYesNo = False
  End If
End Sub

Private Sub txtDefaultDatabase_KeyPress(KeyAscii As Integer)
  On Error GoTo EH
  
  KeyAscii = 0

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub txtIP_KeyPress(KeyAscii As Integer)
  On Error GoTo EH
  
  KeyAscii = 0

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)
  On Error GoTo EH
  
  KeyAscii = 0

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub txtProvider_KeyPress(KeyAscii As Integer)
  On Error GoTo EH
  
  KeyAscii = 0

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub txtUserName_KeyPress(KeyAscii As Integer)
  On Error GoTo EH
  
  KeyAscii = 0

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Public Sub CreateNewProject()
  Dim wRet As Boolean
  Dim pos As Long
       
  On Error GoTo EH
       
  Me.Hide
  dialogFileName = LoadCommDialog(False, "Insert a new i-4.Migration project file", "i-4.Migration Project File", "mti")
  
  DatabaseName = Replace(ProjectName, ".mti", ".mty")
   
  MabseF_Prj.Caption = app.ProductName & IIf(Len(ProjectName), "  -  '" & ProjectName & "' Project", "")
   
  'Mauro: 13/03/2006
  If Len(ProjectName) = 0 And Len(DatabaseName) = 0 Then
    m_CreateYesNo = False
  End If
   
  'se comando � diverso da "" allora faccio load project
  If Trim(ProjectName) = "" And Trim(DatabaseName) = "" Then
    Exit Sub
  End If
   
  pos = InStrRev(dialogFileName, "\") 'ALE 18/01/2006 correzione de path letto dal file .mti
  'MF 09/08/07 Sostituisco GbCurDBPath con ProjectPath
  ProjectPath = Mid(dialogFileName, 1, pos - 1) 'ALE 18/01/2006
   
  If Not mbDatabaseCreationProcess Then
    Select Case Replace(Trim(UCase(arrDatabaseTypes(miDatabaseType))), " ", "")
      Case "SQLSERVER"
        GbCurTipoDB = SQLSERVER
        wRet = Create_New_Db_SQLSERVER
           
      Case "ORACLE"
        GbCurTipoDB = ORACLE
         
      Case "MYSQL"
        GbCurTipoDB = MYSQL
         
      Case "DB2"
        GbCurTipoDB = DB2
         
      Case "ACCESS"
        GbCurTipoDB = Access
        wRet = Create_New_Db_ACCESS
    
    End Select
  End If
  
  If Not wRet Then Exit Sub
  
  GbCurDefaultDatabase = txtDefaultDatabase.Text
  GbCurPassword = txtPassword.Text
  GbCurProvider = txtProvider.Text
  GbCurServerIp = txtIP.Text
  GbCurUserName = txtUserName.Text
   
  'Seleziona i caratteri Jolly specifici del provider
  Select Case GbCurTipoDB
    Case Access
      GbAsterixForDelete = "*"
    
    Case SQLSERVER
      GbAsterixForDelete = ""
         
    Case ORACLE
      
    Case DB2
      
    Case MYSQL
   
  End Select
  
  'Crea il file Ini
  Crea_File_Database DatabaseName, GbCurTipoDB, GbCurUserName, GbCurPassword, GbCurProvider, GbCurServerIp
  m_CreateYesNo = True
  Unload Me
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Public Function OpenProject(Optional oneDbLicense As String = "") As Boolean
  Dim wRet As Boolean
  Dim pos As Long
   
  On Error GoTo EH
   
  OpenProject = True
  If Len(initProject) = 0 Then
    Me.Hide
    dialogFileName = LoadCommDialog(True, "Select a i-4.Migration project file", "i-4.Migration Project File", "mti")
    
    'AC - "per progetto"
    If Len(oneDbLicense) > 0 And (oneDbLicense & ".mti") <> ProjectName Then
      'Non OK!
      OpenProject = False
      Exit Function
    End If
    
    MabseF_Prj.Caption = app.ProductName & IIf(Len(ProjectName), "  -  '" & ProjectName & "' Project", "") '& app.Comments
    loadProject dialogFileName, ProjectName
  Else
    pos = InStrRev(initProject, "\")
    ProjectName = Mid(initProject, pos + 1, Len(initProject) - pos)
    
    MabseF_Prj.Caption = app.ProductName & IIf(Len(initProject), "  -  '" & Mid(initProject, pos + 1, Len(initProject) - pos) & "' Project", "") '& app.Comments
    
    'AC - "per progetto"
    If Len(oneDbLicense) > 0 And (oneDbLicense & ".mti") <> ProjectName Then
      OpenProject = False
      Exit Function
    End If
    
    loadProject initProject, Mid(initProject, pos + 1, Len(initProject) - pos)
    initProject = ""
  End If
  
  If Trim(ProjectName) = "" Or Trim(ProjectPath & "\" & ProjectName) = "" Then
    Exit Function
  End If
   
  If Not mbDatabaseCreationProcess Then
    Select Case Replace(Trim(UCase(arrDatabaseTypes(miDatabaseType))), " ", "")
      Case "SQLSERVER"
        GbCurTipoDB = SQLSERVER
        Connect_Db_SQLSERVER
        wRet = True
           
      Case "ORACLE"
        GbCurTipoDB = ORACLE
           
      Case "MYSQL"
        GbCurTipoDB = MYSQL
           
      Case "DB2"
        GbCurTipoDB = DB2
           
      Case "ACCESS"
        GbCurTipoDB = Access
        Connect_Db_ACCESS
        wRet = True
    
    End Select
  End If
  
  If Not wRet Then Exit Function
  
  GbCurDefaultDatabase = txtDefaultDatabase.Text
  GbCurPassword = txtPassword.Text
  GbCurProvider = txtProvider.Text
  GbCurServerIp = txtIP.Text
  GbCurUserName = txtUserName.Text
     
  'Seleziona i caratteri Jolly specifici del provider
  Select Case GbCurTipoDB
    Case Access
      GbAsterixForDelete = "*"
    
    Case SQLSERVER
      GbAsterixForDelete = ""
       
    Case ORACLE
      
    Case DB2
      
    Case MYSQL
   
  End Select
  
   m_CreateYesNo = True
   Unload Me
Exit Function
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Function
