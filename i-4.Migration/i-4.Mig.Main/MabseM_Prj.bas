Attribute VB_Name = "MabseM_Prj"
Option Explicit

'''''''''''''''''''''''''''''''''''''
' Look & Feel
'''''''''''''''''''''''''''''''''''''
Global Const GUI_BACKCOLOR_MAIN = &HFFFF80
'''
Global Const SYSTEM_DIR = "i-4.Mig.cfg"
'''
'EX modulo DB
Global DbPrj As ADOX.Catalog
Global ADOCnt As New Connection
Global ADOCntSys As New Connection
Global NomeDB As String
'EX modulo SelectDatabase
Public arrDatabaseTypes(0 To 1000) As String
Public arrDatabaseProviders(0 To 1000) As String
Public arrDatabaseIPs(0 To 1000) As String
Public arrDatabaseUserNames(0 To 1000) As String
Public arrDatabasePasswords(0 To 1000) As String
Public arrDefaultDatabases(0 To 1000) As String
Public miDatabaseType As Integer
Public arrDatabaseNames(0 To 1000) As String
Public m_CreateYesNo As Boolean

'SQ - FILE DI CONFIGURAZIONE
Global Const FILE_INI = "\" & SYSTEM_DIR & "\i-4.Migration.ini"
'SQ
'Per il doppio click iniziale
Global initProject As String
'ALE 11-01-06
'Ultima directory aperta con "Add Object..."
Global lastOpen_dir As String
'ALE 19-06-06
Public CustomerConnection As ADODB.Connection
Public PrjConnection As ADODB.Connection
Public customerTabs As Collection
Public prjTabs As Collection

Global Ncmd As Long
 
Public m_fun As New MaFndC_Funzioni

'variabile per tenere conto dell'elemento cliccato sulla TreeView di progetto
Global TreePrj_Index As Long
Global TreePrj_Tag As String

'''Type t_ObjType
'''  oLabel As String
'''  oType As String
'''  oLevel As String
'''  oDescrizione As String
'''End Type

Global m_ObjType() As t_ObjType

'type per definire il menu
Type mnu1
  id As Long
  Image As String
  ImageEn As String
  Label As String
End Type

Type mnu
  id As Long
  ButtonMenu As String
  Bottoni() As mnu1
End Type

'Vartiabile per definire l'intervento della mappa per classificare gli oggetti
Global m_ClassifyObj_For_All As Boolean

'array contenente la definizione dei menu
Global DefMenu() As mnu
Global DefMenuAPP() As mnu 'Serve per riordinare il menu

'type di appoggio per i menu a tendina
Type MenuT
  id As Long
  Label As String
  Funzione As String
  Figlio As String
End Type

Global TotMenu() As MaM1
Global CollMenu() As MenuT

'menu di appoggio per la creazione del menu a tendina, Dopo la creazione dell'array Totale
'viene creato questo array dal quale poi verr� creato il menu a tendina
Type MnuTendina
  id As Long
  Funzione As String
  Label As String
  root As String
  Livello As Long
  Padre As String
  TipoFinIm As String
  DllName As String
End Type

Global TMenu() As MnuTendina

Type TBar
  id As Long
  Label As String
  Funzione As String
  ButtonType As String
  SubName As String
  TipoFinIm As String
  DllName As String
  Picture As Long
  ToolTipText As String
End Type

Global MDITBar() As TBar
Global LISTTBar() As TBar

'variabile per tenere traccia della colonna selezionata sulla lista di filtro selezione
Global Filter_List_Column  As Long

'variabile come la precedente, ma riguarda la lista totale degli oggetti
Global ListObjCol As Long

'variabili per tenere in memoria gli oggetti Durante le Operazioni di Copia incolla ecc...
Global Oggetti() As Ogg

'variabile che usa la precedente type per momorizzare le propriet� di un elemento
'per visualizzarle nel form appropriato
Global OggSel As Ogg
Global GbOggSelNotes As String

'serve per tenere traccia della sezione selezionata nella treeview (area appartenenza)
Global TrwArea As String

'variabile per memorizzare la nuova voce della treeview creata come directory
Global VoceTrwDir As String
Global TrwLiv1 As String
Global TrwLiv2 As String

'type per contenere la struttura corrente della TreeView (Max 5 livelli)
Type Liv
  Relative As String
  RelationShip As Long
  Key As String
  Caption As String
  Image As Long
End Type

Global TreeViewPrj() As Liv

'serve per memorizzare la text del nodo selezionato sulla treeview
Global TreeText As String

'serve per tenere traccia dell'ultima SQL Attivata
Global SQL_Selection_List As String

'Id oggetto da mandare sull'editor tramite popup del main
Global m_IdOggetto_To_Edit As Long

'serve a tenere traccia dell'ultima scelta per quanto riguarda l'editor
'es. Text Editor, Map Editor Ecc...
Global TypeEdit As String

'serve a memorizzare le info PRJ
Type inf
  nomePrj As String
  NumObj As Long
  DateCreation As String
  Directory As String
End Type

Global InfoPRJ() As inf

'Servono per sapere il numero di PGM e DBD che posso Caricare nel Progetto
Global MaxPGM As Long
Global MaxDBD As Long
Global CurPgm As Long
Global CurDBD As Long
Global Percent As Long
Global FreeSlotPGM As Long
Global FreeSlotDBD As Long
Global FreeSlotPGMReset As Long
Global FreeSlotDBDReset As Long

Global UserObj() As String

'Type per contenere le relazioni di un oggetto
Type Rel
  idOggettoC As Long
  IdOggettoR As Long
  Relazione As String
  NomeOggRel As String
  Utilizzo As String
  ConosciutoLiv As Long
  TipoOgg As String
  Tipo  As String
End Type

Global Relazioni() As Rel
Global wListError As String
'MAURO 28-11-05
'Problema "Annulla" "Apri Progetto" con progetto gia' aperto
Global prjAlreadyOpen As Boolean
Global IsTrace As Boolean
Global namePrjLic As String

'Mauro 18/10/2007 : costanti per le intestazioni della lista Oggetti(GridList)
' quando si aggiungono le costanti qui, devono essere aggiunte anche nei moduli MapsdM_Parser (per la lista PsObjList), MavsdM_GenRout (per la lista drObjList), MadrdM_GenRout (per la lista drObjList)
Global Const NAME_OBJ = 1
Global Const TYPE_OBJ = 2
Global Const DLI_OBJ = 3
Global Const CICS_OBJ = 4
Global Const RC_OBJ = 5
Global Const LOCS_OBJ = 6
Global Const NOTES_OBJ = 7
Global Const PARS_DATE_OBJ = 8
Global Const DIR_OBJ = 9
Global Const IMP_DATE_OBJ = 10
'S 7-1-2008
Global PsHideIgnore As Boolean
' S 9-1-2008
Global SelFileTxted() As String
' Ma 19/11/2010: Log per le segnalazioni di caratteri non printabili
Public NoPrintLog As String
''''''''''''''''''''''''''''
' i-4.Migration START
''''''''''''''''''''''''''''
Public Sub Main()
  GbNomeProdotto = app.ProductName & " v" & app.Major & "." & app.Minor & "." & Format(app.Revision, "00")
  'SQtmp lblVersion.Caption = "v" & app.Major & "." & app.Minor & "." & Format(app.Revision, "00")
  
  '''lblExit = "Copyright " & "i-4 s.r.l. 2011-2012" & " - " & GbNomeProdotto
  'SQ - sviluppo vs exe
  If Len(Dir(app.path & FILE_INI)) Then
    GbPathPrd = app.path
  Else
  'GbPathPrd: cos'� sto schifo?
  Dim i As Integer
    For i = 1 To Len(app.path)
     If Mid(app.path, Len(app.path) - i, 1) = "\" Then
       GbPathPrd = Left(app.path, Len(app.path) - i - 1)
       Exit For
     End If
    Next i
  End If
  'FILE "INI"
  If Len(Dir(GbPathPrd & FILE_INI)) Then
    'Lettura FILE_INI
    Carica_Parametri_Prodotto
    
    Connetti_SystemMDB
    
    MabseF_Start.checkLicense
    
    MabseF_Prj.Show
    
    MabseF_Start.startLoading
    
    Screen.MousePointer = vbDefault
  Else
    MsgBox "Configuration file not found: " & GbPathPrd & FILE_INI & "." & vbCrLf & "Program exit.", vbCritical, GbNomeProdotto
  End If
End Sub

Private Sub Connetti_SystemMDB()
  'valorizza le variabili
  Set ADOCntSys = New ADODB.Connection
  ADOCntSys.ConnectionString = "Provider=" & GbConnectionColl.Item(3) & ";Data Source=" & GbConnectionColl.Item(1) & _
      "\" & SYSTEM_DIR & "\" & GbConnectionColl.Item(7) & ";" & "Persist Security Info=" & GbConnectionColl.Item(4)
  ADOCntSys.Open ADOCntSys.ConnectionString
  'setta DLLFunzioni
  Set DLLFunzioni.FnConnDBSys = ADOCntSys
End Sub

Public Function FileExists(path$) As Boolean
  'verifica l'esistenza di un file
  Dim X As Integer
  
  On Error Resume Next
  X = FreeFile
  Open path$ For Input As X
  'con Append � come se aprissi il file quindi gli altri processi devono aspettare.
  If ERR = 0 Then
    FileExists = True
  Else
    FileExists = False
  End If
  Close X
  ERR = 0
End Function

Sub Carica_Type_Menu_MODULO_BASE()
  'copia nell'array di appoggio la parte riguardante il menu a sinistra
  Copia_ArrayTotMenu_In_Appoggio
  Riordina_Type_Menu
End Sub

Function Open_CommonDialogue(Com As CommonDialog, TipoRestituito As String, Optional wOpen) As String
  If IsMissing(wOpen) Then
    Com.Filter = "All Files *.*|*.*"
    Com.DialogTitle = "Open/Save/New File"
  Else
    Select Case Trim(UCase(wOpen))
      Case "DBOPEN"
        Com.Filter = "i-4.Migration Project File *.mti|*.mti"
        Com.DialogTitle = "Open i-4.Migration Project file"
      Case "DBNEW"
        Com.Filter = "i-4.Migration Project File *.mti|*.mti"
        Com.DialogTitle = "Create a New i-4.Migration Project file"
      Case "ADDOBJECTS"
        Com.Filter = "All Files *.*|*.*|CBL Files *.cbl|*.cbl|CPY Files *.cpy|*.cpy|" & _
                     "DBD Files *.dbd|*.dbd|PSB Files *.psb|*.psb|JCL Files *.jcl|*.jcl"
        Com.DialogTitle = "Add objects to project..."
    End Select
  End If
   
  Com.fileName = ""
  Com.MaxFileSize = 32000
  Com.Flags = cdlOFNAllowMultiselect + cdlOFNExplorer
  Com.ShowOpen
  
  Select Case UCase(Trim(TipoRestituito))
    Case "NOME"
      Open_CommonDialogue = Com.FileTitle
      Exit Function
    Case "PERCORSO"
      Open_CommonDialogue = Com.fileName
      Exit Function
  End Select
End Function

Public Function CheckMapToEdit(IdOggetto As Long) As Boolean
  Dim rMFS As Recordset
  
  Set rMFS = m_fun.Open_Recordset("Select * From PsDLI_MFS Where IdOggetto = " & IdOggetto)
  If Not rMFS.EOF Then
    CheckMapToEdit = True
  Else
    CheckMapToEdit = False
  End If
  rMFS.Close
End Function

Public Sub Clessidra(cOn As Boolean)
  DLLFunzioni.Clessidra cOn
End Sub

Public Sub Calcola_Dimensioni_Max()
  Dim i As Long
  
  'GbMaxWidth = MabseF_List.Width + 500
  GbMaxheight = 7500
End Sub

Public Sub Carica_ARRAY_Menu_A_Tendina()
  Dim i As Long, j As Long, k As Long
  Dim InsertInd As Long
  Dim Punto As String
  Dim CurRoot As String
  
  'array per memorizzare i livelli
  Dim L1() As String
  Dim L2() As String
  Dim L3() As String
  Dim L4() As String
  
  ReDim TMenu(0)
  
  ReDim L1(0)
  ReDim L2(0)
  ReDim L3(0)
  ReDim L4(0)
  
  ReDim CollMenu(0)
  
  Punto = ""
  
  For i = 1 To UBound(TotMenu)
    If TotMenu(i).M1Name <> "" Then
      'legge la prima <Root>
      Select Case UCase(TotMenu(i).M1Name)
        Case "<ROOT>"
          ReDim Preserve TMenu(UBound(TMenu) + 1)
          TMenu(UBound(TMenu)).root = ""
          TMenu(UBound(TMenu)).Padre = ""
          TMenu(UBound(TMenu)).Label = TotMenu(i).Label
          TMenu(UBound(TMenu)).Livello = TotMenu(i).M1Level
          TMenu(UBound(TMenu)).id = TotMenu(i).id
          TMenu(UBound(TMenu)).Funzione = TotMenu(i).Funzione
          TMenu(UBound(TMenu)).DllName = TotMenu(i).DllName
          TMenu(UBound(TMenu)).TipoFinIm = TotMenu(i).TipoFinIm
          
          CurRoot = TotMenu(i).Label
          
          'si punta sui vari Padri della Root e cerca tutti i figli accodandoli
          'nel nuovo array appena creato
          '1) Scende al primo livello
          For j = 1 To UBound(TotMenu)
            If TotMenu(j).M1Name = CurRoot Then
              
              If Verifica_Esistenza_VoceMenu(TotMenu(j).Label, CLng(TotMenu(j).M1Level)) = False Then
              
                ReDim Preserve TMenu(UBound(TMenu) + 1)
                TMenu(UBound(TMenu)).root = CurRoot
                TMenu(UBound(TMenu)).Padre = CurRoot
                TMenu(UBound(TMenu)).Label = TotMenu(j).Label
                TMenu(UBound(TMenu)).Livello = TotMenu(j).M1Level
                TMenu(UBound(TMenu)).id = TotMenu(j).id
                TMenu(UBound(TMenu)).Funzione = TotMenu(j).Funzione
                TMenu(UBound(TMenu)).DllName = TotMenu(j).DllName
                TMenu(UBound(TMenu)).TipoFinIm = TotMenu(j).TipoFinIm
                
                'memorizza il nome dei livelli
                If TotMenu(j).Label <> "-" Then
                  ReDim Preserve L1(UBound(L1) + 1)
                  L1(UBound(L1)) = TotMenu(j).Label
                End If
              End If
            End If
          Next j
          
          '2) Scende al Secondo Livello
          For j = 1 To UBound(L1)
            For k = 1 To UBound(TotMenu)
              If TotMenu(k).M1Name = L1(j) Then
                If Verifica_Esistenza_VoceMenu(TotMenu(k).Label, CLng(TotMenu(k).M1Level)) = False Then
                  'crea uno spazio vuoto all'interno dell'array
                  InsertInd = Inserisci_Voce_Menu_In_Array_Menu_A_Tendina(L1(j), 2)
                  
                  'inserisce in questo spazio il nuovo elemento
                  TMenu(InsertInd).root = CurRoot
                  TMenu(InsertInd).Padre = L1(j)
                  TMenu(InsertInd).Label = TotMenu(k).Label
                  TMenu(InsertInd).Livello = TotMenu(k).M1Level
                  TMenu(InsertInd).id = TotMenu(k).id
                  TMenu(InsertInd).Funzione = TotMenu(k).Funzione
                  TMenu(InsertInd).DllName = TotMenu(k).DllName
                  TMenu(InsertInd).TipoFinIm = TotMenu(k).TipoFinIm
                  
                  'memorizza il nome dei livelli
                  ReDim Preserve L2(UBound(L2) + 1)
                  L2(UBound(L2)) = TotMenu(k).Label
                End If
              End If
            Next k
          Next j
          
          '3) Scende al terzo Livello
          For j = 1 To UBound(L2)
            For k = 1 To UBound(TotMenu)
              If TotMenu(k).M1Name = L2(j) Then
                If Verifica_Esistenza_VoceMenu(TotMenu(k).Label, CLng(TotMenu(k).M1Level)) = False Then
  
                  'crea uno spazio vuoto all'interno dell'array
                  InsertInd = Inserisci_Voce_Menu_In_Array_Menu_A_Tendina(L2(j), 3)
                  
                  'inserisce in questo spazio il nuovo elemento
                  TMenu(InsertInd).root = CurRoot
                  TMenu(InsertInd).Padre = L2(j)
                  TMenu(InsertInd).Label = TotMenu(k).Label
                  TMenu(InsertInd).Livello = TotMenu(k).M1Level
                  TMenu(InsertInd).id = TotMenu(k).id
                  TMenu(InsertInd).Funzione = TotMenu(k).Funzione
                  TMenu(InsertInd).DllName = TotMenu(k).DllName
                  TMenu(InsertInd).TipoFinIm = TotMenu(k).TipoFinIm
                  
                  'memorizza il nome dei livelli
                  ReDim Preserve L3(UBound(L3) + 1)
                  L3(UBound(L3)) = TotMenu(k).Label
                End If
              End If
            Next k
          Next j
          
          '4) Scende al Quarto Livello
          For j = 1 To UBound(L3)
            For k = 1 To UBound(TotMenu)
              If TotMenu(k).M1Name = L3(j) Then
                If Verifica_Esistenza_VoceMenu(TotMenu(k).Label, CLng(TotMenu(k).M1Level)) = False Then
  
                  'crea uno spazio vuoto all'interno dell'array
                  InsertInd = Inserisci_Voce_Menu_In_Array_Menu_A_Tendina(L3(j), 4)
                  
                  'inserisce in questo spazio il nuovo elemento
                  TMenu(InsertInd).root = CurRoot
                  TMenu(InsertInd).Padre = L3(j)
                  TMenu(InsertInd).Label = TotMenu(k).Label
                  TMenu(InsertInd).Livello = TotMenu(k).M1Level
                  TMenu(InsertInd).id = TotMenu(k).id
                  TMenu(InsertInd).Funzione = TotMenu(k).Funzione
                  TMenu(InsertInd).DllName = TotMenu(k).DllName
                  TMenu(InsertInd).TipoFinIm = TotMenu(k).TipoFinIm
                  
                  'memorizza il nome dei livelli
                  ReDim Preserve L4(UBound(L4) + 1)
                  L4(UBound(L4)) = TotMenu(k).Label
                End If
              End If
            Next k
          Next j
      End Select
    End If
  Next i
  
  For k = 1 To UBound(TMenu)
    Select Case TMenu(k).Livello
      Case 0
        Punto = ""
      Case 1
        Punto = "."
      Case 2
        Punto = ".."
      Case 3
        Punto = "..."
      Case 4
        Punto = "...."
    End Select
    
    ReDim Preserve CollMenu(UBound(CollMenu) + 1)
    CollMenu(UBound(CollMenu)).id = TMenu(k).id
    CollMenu(UBound(CollMenu)).Label = Punto & TMenu(k).Label
    CollMenu(UBound(CollMenu)).Funzione = TMenu(k).Funzione
    
    If k < UBound(TMenu) Then
      If TMenu(k + 1).Livello <> TMenu(k).Livello Then
        Select Case TMenu(k + 1).Livello
          Case 0
            Punto = ""
          Case 1
            Punto = "."
          Case 2
            Punto = ".."
          Case 3
            Punto = "..."
          Case 4
            Punto = "...."
        End Select
        
        CollMenu(UBound(CollMenu)).Figlio = Punto & TMenu(k + 1).Label
      End If
    End If
  Next k
  
  'crea fisicamente il menu
  Crea_Menu_A_Tendina
End Sub

Public Sub Crea_Menu_A_Tendina()
  'Stop
End Sub

Function Verifica_Esistenza_VoceMenu(Voce As String, Livello As Long) As Boolean
  Dim k As Long
  Dim Ex As Boolean
  
  Ex = False
  For k = 1 To UBound(TMenu)
    If Voce = TMenu(k).Label And TMenu(k).Livello = Livello And Voce <> "-" Then
      Ex = True
      Exit For
    End If
  Next k
  
  Verifica_Esistenza_VoceMenu = Ex
End Function

Function Inserisci_Voce_Menu_In_Array_Menu_A_Tendina(Voce As String, Level As Long) As Long
  Dim k As Long
  Dim Ind As Long
  Dim Cont As Long
  
  'trova l'elemento nell'array di appoggio
  For k = 1 To UBound(TMenu)
    If Voce = TMenu(k).Label Then
      Ind = k + 1
      Exit For
    End If
  Next k
  
  ReDim Preserve TMenu(UBound(TMenu) + 1)
  
  'sposta tutti i dati dalla posizione index di una posizione
  'trova l'ultimo elemento
  For k = Ind To UBound(TMenu)
    If TMenu(k).Livello < Level Then
      Ind = Ind
      Exit For
    Else
      Ind = Ind + 1
    End If
  Next k
  
  For k = UBound(TMenu) To Ind Step -1
    TMenu(k) = TMenu(k - 1)
  Next k
  
  Inserisci_Voce_Menu_In_Array_Menu_A_Tendina = Ind
End Function

'*************************************************************************************
'***** 'INIZIO' FUNZIONI PER CREAZIONE DELLA TOOLBAR DEL MDI FORM E LIST FORM ********
'*************************************************************************************
Sub Carica_ARRAY_TBar_MDI()
  Dim i As Long, j As Long, k As Long
  Dim Cont As Long
  
  ReDim MDITBar(0)
  
  For i = 1 To UBound(TotMenu)
    If TotMenu(i).M3ButtonType <> "" Then
      'ricerca tutti i primi livelli <Root>
      If TotMenu(i).M3SubName = "" And TotMenu(i).M3Name <> "" Then
        ReDim Preserve MDITBar(UBound(MDITBar) + 1)
        MDITBar(UBound(MDITBar)).id = TotMenu(i).id
        MDITBar(UBound(MDITBar)).ButtonType = TotMenu(i).M3ButtonType
        MDITBar(UBound(MDITBar)).DllName = TotMenu(i).DllName
        MDITBar(UBound(MDITBar)).Funzione = TotMenu(i).Funzione
        MDITBar(UBound(MDITBar)).Label = TotMenu(i).Label
        MDITBar(UBound(MDITBar)).Picture = TotMenu(i).Picture
        MDITBar(UBound(MDITBar)).SubName = TotMenu(i).M3SubName
        MDITBar(UBound(MDITBar)).TipoFinIm = TotMenu(i).TipoFinIm
        MDITBar(UBound(MDITBar)).ToolTipText = TotMenu(i).ToolTipText
      End If
    End If
  Next i
  
  For i = 1 To UBound(TotMenu)
    If TotMenu(i).M3ButtonType <> "" Then
      'ricerca tutti i primi livelli <Root>
      If TotMenu(i).M3SubName <> "" And TotMenu(i).M3Name <> "" Then
        'cerca i sottobottoni
        For j = 1 To UBound(MDITBar)
          If MDITBar(j).Label = TotMenu(i).M3Name Then
            'cambia la propriet� del bottone in DropDown
            MDITBar(j).ButtonType = "DropDown"
            
            'sposta tutte le voci di una posizione da qui in poi
            ReDim Preserve MDITBar(UBound(MDITBar) + 1)
            
            For k = UBound(MDITBar) To j + 1 Step -1
              MDITBar(k) = MDITBar(k - 1)
            Next k
            
            'trova il punto di inserimento all'interno dei sottobottoni
            For k = j + 1 To UBound(MDITBar)
              If MDITBar(k).SubName = "" Then
                If Cont <> 0 Then
                  k = k - 1
                Else
                  k = k
                End If
                Exit For
              Else
                Cont = Cont + 1
              End If
            Next k
            
            'inserisce l'elemento
            MDITBar(k).id = TotMenu(i).id
            MDITBar(k).ButtonType = TotMenu(i).M3ButtonType
            MDITBar(k).DllName = TotMenu(i).DllName
            MDITBar(k).Funzione = TotMenu(i).Funzione
            MDITBar(k).Label = TotMenu(i).Label
            MDITBar(k).Picture = TotMenu(i).Picture
            MDITBar(k).SubName = TotMenu(i).M3SubName
            MDITBar(k).TipoFinIm = TotMenu(i).TipoFinIm
            MDITBar(k).ToolTipText = TotMenu(i).ToolTipText
            
          End If
        Next j
      End If
    End If
  Next i
  
  '''Crea_Tool_Bar_MDI MabseF_Prj.fraToolbar
End Sub

Sub Crea_Tool_Bar_MDI(Tool As Toolbar)
  Dim i As Long
  Dim id As Long
  Dim Image As Long
  Dim Key As String
  Dim ToolTip As String
  Dim FinIm As String
  Dim Caption As String
  Dim ButtonType As String
  Dim NDLL As String
  
  Dim Check As Boolean
  
  Dim btnButton As Button
  Dim btnButtonM As ButtonMenu
  
  For i = 1 To UBound(MDITBar)
    id = MDITBar(i).id
    Image = MDITBar(i).Picture
    Key = MDITBar(i).Funzione
    ToolTip = MDITBar(i).ToolTipText
    FinIm = MDITBar(i).TipoFinIm
    Caption = MDITBar(i).SubName
    ButtonType = MDITBar(i).ButtonType
    NDLL = MDITBar(i).DllName
    
    Select Case UCase(Trim(ButtonType))
      Case "NORMAL"
        ButtonType = 0
      Case "CHECKON"
        ButtonType = 1
        Check = True
      Case "CHECKOFF"
        ButtonType = 1
        Check = False
      Case "GROUP"
        ButtonType = 2
      Case "DROPDOWN"
        ButtonType = 5
      Case "SEPARATOR"
        ButtonType = 3
    End Select
    If MDITBar(i).SubName = "" Then
      Set btnButton = Tool.Buttons.Add(, Key, Caption, Val(ButtonType), Image)
      If Val(ButtonType) = 1 Then
        If Check = True Then
          btnButton.Value = tbrPressed
        Else
          btnButton.Value = tbrUnpressed
        End If
      End If
      
      btnButton.Tag = id
      'btnButton.Tag = NDLL
      btnButton.ToolTipText = ToolTip
      btnButton.Description = FinIm
    Else
      Set btnButtonM = Tool.Buttons(Tool.Buttons.count).ButtonMenus.Add(, Key, Caption)
      btnButton.Tag = id
      btnButtonM.Tag = id
      'btnButtonM.Tag = NDLL
    End If
  Next i
End Sub

Sub Carica_ARRAY_TBar_LIST()
  Dim i As Long, j As Long, k As Long
  Dim Cont As Long
  
  ReDim LISTTBar(0)
  
  For i = 1 To UBound(TotMenu)
    If TotMenu(i).M4ButtonType <> "" Then
      'ricerca tutti i primi livelli <Root>
      If TotMenu(i).M4SubName = "" And TotMenu(i).M4Name <> "" Then
        ReDim Preserve LISTTBar(UBound(LISTTBar) + 1)
        LISTTBar(UBound(LISTTBar)).id = TotMenu(i).id
        LISTTBar(UBound(LISTTBar)).ButtonType = TotMenu(i).M4ButtonType
        LISTTBar(UBound(LISTTBar)).DllName = TotMenu(i).DllName
        LISTTBar(UBound(LISTTBar)).Funzione = TotMenu(i).Funzione
        LISTTBar(UBound(LISTTBar)).Label = TotMenu(i).Label
        LISTTBar(UBound(LISTTBar)).Picture = TotMenu(i).Picture
        LISTTBar(UBound(LISTTBar)).SubName = TotMenu(i).M4SubName
        LISTTBar(UBound(LISTTBar)).TipoFinIm = TotMenu(i).TipoFinIm
        LISTTBar(UBound(LISTTBar)).ToolTipText = TotMenu(i).ToolTipText
      End If
    End If
  Next i
  
  For i = 1 To UBound(TotMenu)
    If TotMenu(i).M4ButtonType <> "" Then
      'ricerca tutti i primi livelli <Root>
      If TotMenu(i).M4SubName <> "" And TotMenu(i).M4Name <> "" Then
        'cerca i sottobottoni
        For j = 1 To UBound(LISTTBar)
          If LISTTBar(j).Label = TotMenu(i).M4Name Then
            'cambia la propriet� del bottone in DropDown
            LISTTBar(j).ButtonType = "DropDown"
            
            'sposta tutte le voci di una posizione da qui in poi
            ReDim Preserve LISTTBar(UBound(LISTTBar) + 1)
            
            For k = UBound(LISTTBar) To j + 1 Step -1
              LISTTBar(k) = LISTTBar(k - 1)
            Next k
            
            'trova il punto di inserimento all'interno dei sottobottoni
            For k = j + 1 To UBound(LISTTBar)
              If LISTTBar(k).SubName = "" Then
                If Cont <> 0 Then
                  k = k - 1
                Else
                  k = k
                End If
                Exit For
              Else
                Cont = Cont + 1
              End If
            Next k
            
            'inserisce l'elemento
            LISTTBar(k).id = TotMenu(i).id
            LISTTBar(k).ButtonType = TotMenu(i).M4ButtonType
            LISTTBar(k).DllName = TotMenu(i).DllName
            LISTTBar(k).Funzione = TotMenu(i).Funzione
            LISTTBar(k).Label = TotMenu(i).Label
            LISTTBar(k).Picture = TotMenu(i).Picture
            LISTTBar(k).SubName = TotMenu(i).M4SubName
            LISTTBar(k).TipoFinIm = TotMenu(i).TipoFinIm
            LISTTBar(k).ToolTipText = TotMenu(i).ToolTipText
          End If
        Next j
      End If
    End If
  Next i
End Sub

' Non viene mai utilizzata
'Sub Crea_Tool_Bar_LIST(Tool As Toolbar)
'  Dim i As Long
'  Dim id As Long
'  Dim Image As Long
'  Dim Key As String
'  Dim ToolTip As String
'  Dim FinIm As String
'  Dim Caption As String
'  Dim ButtonType As String
'  Dim NDLL As String
'
'  Dim Check As Boolean
'
'  Dim btnButton As Button
'  Dim btnButtonM As ButtonMenu
'
'  For i = 1 To UBound(LISTTBar)
'    id = LISTTBar(i).id
'    Image = LISTTBar(i).Picture
'    Key = LISTTBar(i).Funzione
'    ToolTip = LISTTBar(i).ToolTipText
'    FinIm = LISTTBar(i).TipoFinIm
'    Caption = LISTTBar(i).SubName
'    ButtonType = LISTTBar(i).ButtonType
'    NDLL = LISTTBar(i).DllName
'
'    Select Case UCase(Trim(ButtonType))
'      Case "NORMAL"
'        ButtonType = 0
'      Case "CHECKON"
'        ButtonType = 1
'        Check = True
'      Case "CHECKOFF"
'        ButtonType = 1
'        Check = False
'      Case "GROUP"
'        ButtonType = 2
'      Case "DROPDOWN"
'        ButtonType = 5
'      Case "SEPARATOR"
'        ButtonType = 3
'    End Select
'    If LISTTBar(i).SubName = "" Then
'      Set btnButton = Tool.Buttons.Add(, Key, Caption, Val(ButtonType), Image)
'      If Val(ButtonType) = 1 Then
'        If Check = True Then
'          btnButton.Value = tbrPressed
'        Else
'          btnButton.Value = tbrUnpressed
'        End If
'      End If
'
'      btnButton.Tag = NDLL
'      btnButton.ToolTipText = ToolTip
'      btnButton.Description = FinIm
'    Else
'      Set btnButtonM = Tool.Buttons(Tool.Buttons.count).ButtonMenus.Add(, Key, Caption)
'      btnButtonM.Tag = NDLL
'    End If
'  Next i
'End Sub

Public Function Crea_Directory_Parametrica(Directory As String) As String
  Crea_Directory_Parametrica = Replace(Directory, ProjectPath, "$\")
  'per sicurezza... verificare se serve... (se Directory finisce con "\"...)
  Crea_Directory_Parametrica = Replace(Crea_Directory_Parametrica, "\\", "\")
End Function

Function Crea_Directory_Progetto(Directory As String, PathD As String) As String
  Dim WDollaro As Long
   
  WDollaro = InStr(Directory, "$")
  If WDollaro Then
    Crea_Directory_Progetto = PathD & Trim(Mid(Directory, WDollaro + 1))
  End If
End Function

Function GetComputerNameU() As String
  Dim Str As String
  
  'prende il nome del computer
  Str = String(255, Chr$(0))
  GetComputerName Str, 255
  Str = Left$(Str, InStr(1, Str, Chr$(0)) - 1)
  
  GetComputerNameU = Replace(Str, "-", "")
End Function


Function FileUserExist() As Boolean '***********************************
  Dim exFile As String
                                             
  Dim Bool As Variant
  Dim Key As String
  Dim Codice As String
  Dim CodCrypt As String
  Dim MaxPGMCry As String
  Dim MaxDBDCry As String
  Dim PercentCry As String
  Dim Percent As Long
  Dim Pass As String
  
  Dim DllOK As Boolean
    
  On Error Resume Next
    
  exFile = Dir(GbPathPrd & "\SYSTEM_DIR\*.aut", vbNormal)
                                               
  Pass = GbPswUser
                                         
  Bool = False
                                         
  Do While exFile <> ""
    'Legge il file criptato
    Clessidra True
    
    DllOK = DllBase.Decodifica_File_Criptato(GbPathPrd & "\" & SYSTEM_DIR & "\" & exFile, Pass)
    
    Clessidra False
    
    'Confronta Il codice
    If Trim(DllBase.UsCodice) = Trim(GbCodePrj) And DllOK = True Then
      Bool = True
      Exit Do
    End If
    exFile = Dir
  Loop
  
  If Bool = True Then
    'Prende i valori dei numeri Max e percent dalla DLL
    MaxDBD = DllBase.UsMaxDBD
    MaxPGM = DllBase.UsMaxPGM
    Percent = DllBase.UsPercent
    FreeSlotPGM = DllBase.UsFreeSlotPGM
    FreeSlotDBD = DllBase.UsFreeSlotDBD
    
    'Variabili per memorizzare i valori iniziali (per un futuro reset dei valori)
    FreeSlotPGMReset = DllBase.UsFreeSlotPGM
    FreeSlotDBDReset = DllBase.UsFreeSlotDBD
    
    'Carica La list adegli oggetti
    DllBase.Carica_Array_Main UserObj
    
    FileUserExist = True
  Else
    FileUserExist = False
    MsgBox "User file is corrupted...", vbCritical, GbNomeProdotto
    End
  End If
End Function

Function DllACTExist_Step2(Optional Lab As Label, Optional shape As Boolean) As String
  Dim Percorso As String
  
  On Error GoTo errorHandler
  
  Clessidra True
  
  'Controlla Se ci sono pi� file lic nella Directory SYSTEM_DIR
  'space in caso di problemi di licenza
  DllACTExist_Step2 = DLLACT.Controlla_Molteplicita_FileLIC(GbPathPrd & "\" & SYSTEM_DIR)
  
  Clessidra False
  
  Exit Function '
errorHandler:
  MsgBox "Licence Error  " & vbCrLf & vbCrLf & "Check the licence files in <InstallDir>\" & SYSTEM_DIR, vbCritical, "i-4.Migration V3.0"
  End
End Function

Public Function Controlla_PrdID(NomeF As String, Optional Lab As Label, Optional shape As Boolean) As Boolean
  Dim Numero As String
  Dim NomePC As String
  Dim Nomecliente As String
  Dim License As Boolean
   
  If Lab <> "" Then
    'WaitTime 1
    Lab.Refresh
    Lab.Caption = "Verify ProductID Number Authorization..."
    Lab.Refresh
  End If
  
  Numero = DLLACT.Restituisci_ProcuctID(GbPathPrd & "\" & SYSTEM_DIR & "\" & NomeF)
  NomePC = DLLACT.Restituisci_NomePC(GbPathPrd & "\" & SYSTEM_DIR & "\" & NomeF)
  Nomecliente = DLLACT.Restituisci_Nome_Cliente(GbPathPrd & "\" & SYSTEM_DIR & "\" & NomeF)
  GbLicType = Mid(Nomecliente, 1, InStr(Nomecliente, " ") - 1)
  GbPrdID = Numero
  
  If Trim(Numero) <> "" And Trim(NomePC) <> "" Then
    ' DLLLic.
    NomePC = Replace(NomePC, "-", "")
    On Error GoTo catch
    License = DecodificaLicenza(Numero, NomePC)
    On Error GoTo 0
    
    Controlla_PrdID = License
    
    If shape Then
     ''MabseF_Avvio.Forma(1).BackColor = &H80FF&
    End If
  Else
    Controlla_PrdID = False
    DLLFunzioni.Show_MsgBoxError "ML00E"
    End
  End If
  
  GbNUMID = DLLLic.Lic_NumID
  
  Clessidra False
  Clessidra True
  
  If Controlla_PrdID Then
    Lab.Refresh
    Lab.Caption = "Authorization Test Passed..."
    
    If shape Then
      WaitTime 1
    End If
  End If
  
  Lab.Refresh
  Exit Function
catch:
  DLLFunzioni.Show_MsgBoxError "ML00E"
  End
End Function

Function DllCTRLExist() As Boolean
  Dim i As Long
  Dim exFile As String
  'buttare ExFile
   
  'Recupera il path di Windows System32
  GbPathSystem32 = DLLFunzioni.GetSystem32Dir
  
  If Len(Dir(GbPathSystem32 & "\" & NomeFileCTRL, vbNormal)) Then
    DllCTRLExist = True
    
    If getCtrl() = "31/05/1975 23.54.00" Then
      'Significa che � il primo avvio
      fixCtrl
    End If
  Else
    DllCTRLExist = False
  End If
End Function

Function DllACTExist(Optional Lab As Label, Optional shape As Boolean) As Boolean     '***********************************
  Dim exFile As String
  Dim DataC As Date, DataL As Variant
  Dim OraC As String, OraL As String
  Dim i As Long
  Dim Status As Boolean
  
  Dim Percorso As String
  Dim NomeF As String
  
  '1) Controlla esistenza fisica DLL
  If Lab <> "" Then
    Lab.Caption = "Start Verify's Authorization..."
    'WaitTime 1
    Lab.Caption = "Verify DLL's Structure Step-1..."
    Lab.Refresh
  End If
  
  Clessidra True
                                                    
  DllACTExist = True
  If shape Then
    ''MabseF_Avvio.Forma(0).BackColor = &HFF0000
  End If
  
  Clessidra False
  'WaitTime 1
End Function

Function DLLCryExist() As Boolean
  Dim exFile As String
                                                    
  exFile = Dir(GbPathPrd & "\i-4.Mig.Lib\" & NomeDLLLic, vbNormal)
                                                       
  If exFile <> "" Then
    DLLCryExist = True
  Else
    DLLCryExist = False
  End If
End Function

Function DLLDli2RdbMsExist() As Boolean '***********************************
  Dim exFile As String
                                                    
  exFile = Dir(GbPathPrd & "\i-4.Mig.Lib\" & NomeDLLDli2RDBMs, vbNormal)
  If exFile <> "" Then
    'If Verify_Dll(DLLLic.Lic_NumID, "DLI") Then
      DLLDli2RdbMsExist = True
    'End If
  Else
    DLLDli2RdbMsExist = False
  End If
End Function

'ALE xyz
Function DLLVSAM2RdbMsExist() As Boolean '***********************************
  Dim exFile As String
                                                    
  exFile = Dir(GbPathPrd & "\i-4.Mig.Lib\" & NomeDLLVSAM2RDBMs, vbNormal)
  If exFile <> "" Then
    'If Verify_Dll(DLLLic.Lic_NumID, "DLI") Then
      DLLVSAM2RdbMsExist = True
    'End If
  Else
    DLLVSAM2RdbMsExist = False
  End If
End Function

Function DLLToolsExist() As Boolean '***********************************
  Dim exFile As String
                                                    
  exFile = Dir(GbPathPrd & "\i-4.Mig.Lib\" & NomeDLLTools, vbNormal)
  If exFile <> "" Then
    'If Verify_Dll(DLLLic.Lic_NumID, "UTY") Then
      DLLToolsExist = True
    'End If
  Else
    DLLToolsExist = False
  End If
End Function

Function DLLAnalisiExist() As Boolean '***********************************
  Dim exFile As String
                                                    
  exFile = Dir(GbPathPrd & "\i-4.Mig.Lib\" & NomeDLLAnalisi, vbNormal)
  If exFile <> "" Then
    'If Verify_Dll(DLLLic.Lic_NumID, "AN") Then
      DLLAnalisiExist = True
    'End If
  Else
    DLLAnalisiExist = False
  End If
End Function

Function DLLImsExist() As Boolean
  Dim exFile As String
                                                    
  exFile = Dir(GbPathPrd & "\i-4.Mig.Lib\" & NomeDLLIms, vbNormal)
  If exFile <> "" Then
    'If Verify_Dll(DLLLic.Lic_NumID, "IM") Then
      DLLImsExist = True
    'End If
  Else
    DLLImsExist = False
  End If
End Function

Function DllParserExist() As Boolean
  Dim exFile As String
  
  exFile = Dir(GbPathPrd & "\i-4.Mig.Lib\" & NomeDLLParser, vbNormal)
  If exFile <> "" Then
    DllParserExist = True
  Else
    DllParserExist = False
  End If
End Function

Function DllDataManExist() As Boolean
  Dim exFile As String
  
  exFile = Dir(GbPathPrd & "\i-4.Mig.Lib\" & NomeDLLDataM, vbNormal)
  If exFile <> "" Then
    'If Verify_Dll(DLLLic.Lic_NumID, "DM") Then
      DllDataManExist = True
    'End If
  Else
    DllDataManExist = False
  End If
End Function
'**********************************************************************

Function DllFunzioniExist() As Boolean '
  Dim exFile As String
  
  exFile = Dir(GbPathPrd & "\i-4.Mig.Lib\" & NomeDLLFunzioni, vbNormal)
  If exFile <> "" Then
    DllFunzioniExist = True
  Else
    DllFunzioniExist = False
  End If
End Function

Sub Copia_ArrayTotMenu_In_Appoggio()
  Dim i As Long, k As Long, Y As Long
  Dim IndMenu As Long
  
  ReDim DefMenu(0)
  ReDim DefMenu(0).Bottoni(0)
  
  For i = 1 To UBound(TotMenu)
    If TotMenu(i).M2Name <> "" Then
      Select Case TotMenu(i).M2Name
        Case "<Root>"
          IndMenu = UBound(DefMenu) + 1
          For Y = 1 To UBound(DefMenu)
            If UCase(DefMenu(Y).ButtonMenu) = UCase(TotMenu(i).Label) Then
              IndMenu = 0
              Exit For
            End If
          Next Y
          If IndMenu > 0 Then
            ReDim Preserve DefMenu(IndMenu)
            DefMenu(IndMenu).ButtonMenu = TotMenu(i).Label
            DefMenu(IndMenu).id = TotMenu(i).id
            ReDim Preserve DefMenu(IndMenu).Bottoni(0)
          End If
          
        Case Else
          IndMenu = 0
          For Y = 1 To UBound(DefMenu)
            If UCase(DefMenu(Y).ButtonMenu) = UCase(TotMenu(i).M2Name) Then
              IndMenu = Y
              Exit For
            End If
          Next Y
          If IndMenu = 0 Then
            IndMenu = UBound(DefMenu) + 1
            ReDim Preserve DefMenu(IndMenu)
            ReDim Preserve DefMenu(IndMenu).Bottoni(0)
            DefMenu(IndMenu).ButtonMenu = TotMenu(i).Label
            DefMenu(IndMenu).id = TotMenu(i).id
          End If
          k = UBound(DefMenu(IndMenu).Bottoni) + 1
          ReDim Preserve DefMenu(IndMenu).Bottoni(k)
          DefMenu(IndMenu).Bottoni(k).Image = TotMenu(i).Picture
          DefMenu(IndMenu).Bottoni(k).ImageEn = TotMenu(i).PictureEn
          DefMenu(IndMenu).Bottoni(k).Label = TotMenu(i).Label
          DefMenu(IndMenu).Bottoni(k).id = TotMenu(i).id
      End Select
    End If
  Next i
End Sub

Public Function Attiva_Funzione(byTagModule As String, cParam As Collection, byTagFunction As String, Optional ByVal FinImmediata, Optional ByVal NoResize As Boolean = False, Optional ByVal noFreezeFlickering As Boolean = False) As Boolean
  Dim i As Long
  Dim FinImCheck As Boolean
  Dim wBool As Boolean
  Dim OldFinImmediata As Boolean
  Dim ActWinIsFree As Boolean
  Dim inode As Node
  
  On Error GoTo errorHandler
  'If DLLFunzioni.FnActiveWindowsBool = True Then Exit Function 'ALE commentato
  
  Attiva_Funzione = False
  
  FinImCheck = GbFinImCheck
  
  OldFinImmediata = GbFinImCheck
  
  If IsMissing(FinImmediata) Then
    FinImCheck = GbFinImCheck
  Else
    If Trim(FinImmediata) <> "" Then
      FinImCheck = True
      GbFinImCheck = True
    Else
      FinImCheck = False
      GbFinImCheck = False
    End If
  End If
          
  If noFreezeFlickering Then
    Terminate_Flickering
  Else
    Elimina_Flickering MabseF_Prj.hWnd
  End If
   
  'Lancia la funzione in base alla DLL
  Select Case Trim(UCase(byTagModule))
    Case "MAIN"
      MabseF_Prj.AttivaFunzioneMain byTagFunction, cParam
      For Each inode In MabseF_List.PrjTree.Nodes
        inode.Expanded = True
        Exit For
      Next
    Case "BASE", "MABSDP_00"
      If UCase(byTagFunction) = "SHOW_LISTERR" Then
        SetParent MabsdF_ListErr.hWnd, MabseF_List.hWnd
        Load MabsdF_ListErr
        MabsdF_ListErr.Show
        MabsdF_ListErr.Move 0, 0, MabseF_List.Width, MabseF_List.Height
      Else
        ''SQ move DllBase.AttivaFunzione byTagFunction, cParam, MabseF_List  'A
        DllBase.AttivaFunzione byTagFunction, cParam, MabseF_Prj  'A
        If UCase(byTagFunction) = "PRJ_SYSOBJ_DECLARE" Then
          DllParser.AttivaFunzione "ALIGN_SYSOBJECT", cParam, MabseF_List  'A
        End If
      End If
    Case "PARSER", "MAPSDP_00"
      DLLFunzioni.FnActiveWindowsBool = True
      'PRIMO LIVELLO SU PI� OGGETTI
      If Trim(UCase(byTagFunction)) = "PARSE_OBJECT_FIRST_LEVEL" Then
        MabseF_Prj.mnuParseObjsMenu_Click
        'SECONDO LIVELLO SU PI� OGGETTI
      ElseIf Trim(UCase(byTagFunction)) = "PARSE_OBJECT_SECOND_LEVEL" Then
        MabseF_Prj.mnuParseObjsMenu2_Click
      Else 'FUNZIONE PARSER GENERICA
        DLLFunzioni.FnProcessRunning = True
        DllParser.AttivaFunzione byTagFunction, cParam, MabseF_List   'A
      End If
    Case "DATAAN", "MAANDP_00"
      DLLDataAnalysis.AttivaFunzione byTagFunction, cParam, MabseF_List   'A
    Case "DATAMAN", "MADMDP_00"
      DllDataM.AttivaFunzione byTagFunction, cParam, MabseF_List   'A
    Case "TOOLS", "MATSDP_00"
      DLLTools.AttivaFunzione byTagFunction, cParam, MabseF_List   'A
    Case "IMSTOMTP", "MAIMDP_00"
      DLLIms.AttivaFunzione byTagFunction, cParam, MabseF_List   'A
    Case "DLITORDBMS", "MADRDP_00"
      Terminate_Flickering
      DLLDli2RdbMs.AttivaFunzione byTagFunction, cParam, MabseF_List   'A
    'ale xyz
    Case "VSAMTORDBMS", "MAVSDP_00"
      Terminate_Flickering
      DLLVSAM2RdbMs.AttivaFunzione byTagFunction, cParam, MabseF_List   'A
    Case "TREEVIEWREFRESH"
      Terminate_Flickering
      MabseF_Prj.AttivaFunzioneMain "PRJ_OPEN_SLOW", , True
      For Each inode In MabseF_List.PrjTree.Nodes
        inode.Expanded = True
        Exit For
      Next
    Case Else
  End Select
   
  If Not noFreezeFlickering Then
    Terminate_Flickering 'A
  End If
   
  GbFinImCheck = OldFinImmediata
   
  DLLFunzioni.FnProcessRunning = False
  Exit Function
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "RC0002"
End Function

Public Function Empty_Array(sFile() As String) As Boolean
  On Error GoTo errorHandler
  
  If UBound(sFile) = 0 Then
    Empty_Array = True
  Else
    Empty_Array = False
  End If
  Exit Function
errorHandler:
  Empty_Array = True
End Function

Public Function Aggiungi_Oggetti_In_DB(Pb As ProgressBar) As Boolean
  Dim TbOggetti As Recordset
  Dim Scelta As Long
  Dim FirstScelta As Long
  Dim i As Long
  Dim wDir As String, wNom As String, wTip As String
  Dim wExt As String, wDirMadre As String
  Dim wIdObjMax As Long
  Dim YesALL As Boolean, noALL As Boolean, wAddNew As Boolean
  Dim cParam As Collection
  Dim Existe As Boolean
  Dim sFile() As String
  Dim oMessage As Form
  Dim wPdir As String
  Dim arrNoPrint() As String
  Dim RTB As RichTextBox
  
  On Error GoTo EH
  
  'resetta l'array in memoria
  ReDim GbOggetti(0)
  
  MabseF_List.Refresh
  MabseF_Log.Refresh
  MabseF_Menu.Refresh
  MabseF_Prj.ResizeAll
  
  FirstScelta = Scelta
  
  If Scelta = vbCancel Then
    Aggiungi_Oggetti_In_DB = False 'Annulla
    Exit Function
  Else
    Aggiungi_Oggetti_In_DB = True 'OK
  End If
  
  Pb.Value = 0
  
  'Chiama la nuova common dialog
  Dim oDialog As New MabseF_CommDialog
  
  oDialog.FilterIndex = 12
  oDialog.Filter = "Cobol Sources(*.cbl) |*.cbl|" & _
                   "Assembler Sources(*.asm) |*.asm|" & _
                   "Cobol Copy(*.cpy) |*.cpy|" & _
                   "CICS Maps(*.bms) |*.bms|" & _
                   "IMS Maps(*.mfs) |*.mfs|" & _
                   "JCL(*.jcl) |*.jcl|" & _
                   "PROCEDURE(*.prc) |*.prc|" & _
                   "PARAMETERS(*.prm) |*.prm|" & _
                   "DBD(*.dbd) |*.dbd|" & _
                   "PSB(*.psb) |*.psb|" & _
                   "DDL DB2(*.ddl) |*.ddl|" & _
                   "All Files(*.*) |*.*|"
                       
  oDialog.start_Path = ProjectPath 'lastOpen_dir
  
  oDialog.Show vbModal
  
  Clessidra True
  'Recupera l,'array dei file
  sFile = oDialog.SelectedFileName
  
  Unload oDialog
  Set oDialog = Nothing
  
  Clessidra False
  
  MabseF_List.Refresh
  MabseF_Menu.Refresh
   
  'Controlla se sto facendo una ADD dalla directory del progetto
  If Empty_Array(sFile) Then
    Exit Function
  ElseIf InStr(1, sFile(0), ProjectPath, vbTextCompare) = 0 Then
    DLLFunzioni.Show_MsgBoxError "MP04I"
    Exit Function
  End If

  If UBound(sFile) Then
    Pb.Max = UBound(sFile)
    Pb.Value = 0
    Pb.Visible = True
    MabseF_List.SetFocus
    
    'Recupera la directory Madre
    wDirMadre = sFile(0)
    MabseF_List.FlagStop = False
    For i = 1 To UBound(sFile)
      wDirMadre = Mid(sFile(i), 1, InStrRev(sFile(i), "\") - 1) 'A
      Clessidra True

      'Nome:
      wNom = Mid(sFile(i), InStrRev(sFile(i), "\") + 1, Len(sFile(i)) - InStrRev(sFile(i), "\"))  'A sFile(i)
      'Tipo
      wTip = DLLFunzioni.getTipoOggettoByExtension(DLLFunzioni.getExtensionFile(wNom))
      wNom = UCase(wNom)
      wExt = Replace(UCase(Trim(DLLFunzioni.getExtensionFile(wNom))), "UNK", "")
      wDir = Crea_Directory_Parametrica(wDirMadre)
      
      'SQ: modificato il controllo sul percorso (sbagliava... con lo "\"...)
      wPdir = Replace(wDir, "$\", "$\" & Replace(DatabaseName, ".mty", "") & "\")
      Set TbOggetti = DLLFunzioni.Open_Recordset("select * From BS_Oggetti where " & _
                                                 "nome = '" & Replace(wNom, "." & wExt, "") & "' and " & _
                                                 "Directory_Input = '" & wPdir & "' AND Estensione = '" & wExt & "'")
      If TbOggetti.RecordCount Then
        'If Not YesALL Then
        If Not YesALL And Not noALL Then
          GbCurrentSourceImport = wNom
          Clessidra False
          
          Set oMessage = New MabseF_MsgBox
          
          oMessage.Show vbModal
          Clessidra True
          Scelta = GbSceltaMsgBox
          
          Unload oMessage  'c'e' gia'
          Set oMessage = Nothing
          'SQ - 12-12-06: erano sotto, fuori IF
          YesALL = Scelta = 1243
          noALL = Scelta = 1244
        End If
        Existe = True
        wAddNew = False
      Else
        'Aggiunge l'oggetto
        TbOggetti.AddNew
        wAddNew = True
        Scelta = vbYes
      End If

      'Controlla la len del nome
      If Len(wNom) > c_MaxSource_NameLen Then
        DLLFunzioni.WriteLog "Source: " & wNom & " Is not imported because its name is too long (>20)", "Objects Import"
        Scelta = vbNo
      End If

      If Scelta = vbYes Or YesALL Then
        TbOggetti!Nome = Trim(Replace(wNom, "." & wExt, ""))
        TbOggetti!Estensione = Trim(Replace(wExt, "UNK", ""))
        TbOggetti!directory_input = Trim(wDir)
        TbOggetti!Tipo = "UNK" 'wTip
        TbOggetti!DtImport = Trim(Now)
        TbOggetti!Livello1 = "UNK" 'wTip
        TbOggetti!Avaliable = True
        TbOggetti!Livello2 = Trim(wDir)
        TbOggetti!Area_Appartenenza = "UNKNOWN"
        TbOggetti!parsingLevel = 0
        TbOggetti.Update

        'Recupera il nuovo idoggetto
        wIdObjMax = GetMaxIdOggettoAfterAddNew(TbOggetti)

        'lancia il parsing per gli oggetti appena inseriti
        DllParser.PsIdOggetto_Add = wIdObjMax
          
        'Crea la collection parametri
        Set cParam = New Collection
        cParam.Add wIdObjMax
          
        DLLFunzioni.FnActiveWindowsBool = False
        Attiva_Funzione "PARSER", cParam, "ADD_TO_PROJECT_PARSER", , True, True
        DLLFunzioni.FnActiveWindowsBool = True
          
        ' Ma 19/11/2010: Controllo se ci sono caratteri non printabili confrontando il numero di righe
        Set RTB = MabseF_List.rText1
        If Trim(TbOggetti!Estensione) <> "" Then
          RTB.LoadFile DLLFunzioni.FnPathDef & Mid(TbOggetti!directory_input, 2) & "\" & TbOggetti!Nome & "." & TbOggetti!Estensione
        Else
          RTB.LoadFile DLLFunzioni.FnPathDef & Mid(TbOggetti!directory_input, 2) & "\" & TbOggetti!Nome
        End If
        arrNoPrint = Split(RTB.Text, vbCrLf)
        If UBound(arrNoPrint) > TbOggetti!NumRighe Then
          NoPrintLog = NoPrintLog & IIf(Len(NoPrintLog), vbCrLf, "") & _
                                    Space(6 - Len(TbOggetti!IdOggetto)) & TbOggetti!IdOggetto & "|" & _
                                    Space(50 - Len(TbOggetti!Nome)) & TbOggetti!Nome & "|" & _
                                    Space(6 - Len(TbOggetti!NumRighe)) & TbOggetti!NumRighe
          TbOggetti!NumRighe = UBound(arrNoPrint)
          TbOggetti.Update
        End If
        
        'Aggiorna la tabella della treeview
        Update_tbTreeView_After_AddObjects wIdObjMax
          
        'aggiorna l'array in memoria
        ReDim Preserve GbOggetti(UBound(GbOggetti) + 1)
        GbOggetti(UBound(GbOggetti)).IdOggetto = wIdObjMax
        GbOggetti(UBound(GbOggetti)).Nome = wNom
          
        If TbOggetti.State = adStateOpen Then
          TbOggetti.Close
        End If

        DoEvents

        Pb.Value = Pb.Value + 1
        Pb.Refresh
      End If
      
      'If Not YesALL Then Scelta = FirstScelta  '?
      If Not YesALL And Not noALL Then Scelta = FirstScelta  '?

      Clessidra False
      ' Ma - 15/05/09 Gestione uscita dall'import con ESC
      If MabseF_List.FlagStop Then
        'wResp = DLLFunzioni.Show_MsgBoxError("PB00Q")
        'wResp = MsgBox("Do you want interrupt the job?", vbYesNo, "i-4.Migration: Parser")
        If DLLFunzioni.Show_MsgBoxError("MP03Q") = vbYes Then
          Exit For
        Else
          MabseF_List.FlagStop = False
          Screen.MousePointer = vbHourglass
        End If
      End If
    Next i
    MabseF_List.FlagStop = False
  End If

  Pb.Value = 0
  Pb.Visible = False
   
  'aggiorna le variabili delle DLL
  Aggiorna_Variabili_DLL  'chiarire...
   
  Exit Function
EH:
  DLLFunzioni.Show_MsgBoxError "MP00E", "MabseM_Prj", "Aggiungi_Oggetti_In_DB", ERR.Number, ERR.Description
  ERR.Clear
  Pb.Visible = False
'  Resume Next
End Function

Public Sub Update_tbTreeView_After_AddObjects(wIdOggetto As Long)
  Dim rs As Recordset, rTrw As Recordset
  Dim rMax As Recordset
   
  Dim wArea As String
  Dim wLivello1 As String, wLivello2 As String
  Dim cMax As Long
   
  Set rs = DLLFunzioni.Open_Recordset("select * from BS_Oggetti Where IdOggetto = " & wIdOggetto)
  If rs.RecordCount Then
    'Prende le variabili per accedere alla tabella BS_treeView
    wArea = rs!Area_Appartenenza
    wLivello1 = rs!Livello1
    wLivello2 = rs!Livello2
    
    'Recupera il maxid
    Set rMax = DLLFunzioni.FnConnection.Execute("Select MAX(Id) From BS_TreeView")
    cMax = CLng(rMax.fields(0).Value)
    rMax.Close
    
    'Ric per Area
    Set rTrw = DLLFunzioni.Open_Recordset("Select * From BS_TreeView Where Key = '" & wArea & "'")
    If rTrw.RecordCount = 0 Then
      rTrw.AddNew
      rTrw!id = cMax + 1
      rTrw!Testo = wArea
      rTrw!NRelative = "MB"
      rTrw!Key = wArea
      rTrw!Tipo = 4
      rTrw!Livello = "L0"
      rTrw!Image = 5
      rTrw.Update
    End If
    rTrw.Close
      
    'Ric Per Livello1
    Set rTrw = DLLFunzioni.Open_Recordset("Select * From BS_TreeView Where Key = '" & wArea & "#" & wLivello1 & "'")
    If rTrw.RecordCount = 0 Then
      rTrw.AddNew
      rTrw!id = cMax + 2
      rTrw!Testo = wLivello1
      rTrw!NRelative = wArea
      rTrw!Key = wArea & "#" & wLivello1
      rTrw!Tipo = 4
      rTrw!Livello = "L1"
      rTrw!Image = 4
      rTrw.Update
    End If
    rTrw.Close
      
    'Ric Per Livello2
    Set rTrw = DLLFunzioni.Open_Recordset("Select * From BS_TreeView Where Key = '" & wArea & "#" & wLivello1 & "@" & wLivello2 & "'")
    If rTrw.RecordCount = 0 Then
      rTrw.AddNew
      rTrw!id = cMax + 3
      rTrw!Testo = wLivello2
      rTrw!NRelative = wArea & "#" & wLivello1
      rTrw!Key = wArea & "#" & wLivello1 & "@" & wLivello2
      rTrw!Tipo = 4
      rTrw!Livello = "L2"
      rTrw!Image = 2
      rTrw.Update
    End If
    rTrw.Close
  End If
  rs.Close
End Sub

'''''''Function Carica_Lista_Oggetti(Lista As ListView, SQL As String, Pb As ProgressBar, Optional OpenMode As Long) As Long
'''''''
'''''''    Dim r As Recordset
'''''''    Dim Cont As Long
'''''''
'''''''    On Error GoTo EH
'''''''
'''''''    Call Elimina_Flickering_Object(MabseF_List.GridList.hwnd)
'''''''
'''''''    Pb.Value = 0
'''''''
'''''''    Lista.ListItems.Clear
'''''''
'''''''    Lista.Sorted = False
'''''''
'''''''    If SQL <> "" Then
'''''''
'''''''        If OpenMode <> 0 Then
'''''''            Set r = DLLFunzioni.Open_Recordset(SQL, OpenMode)
'''''''        Else
'''''''            Set r = DLLFunzioni.Open_Recordset(SQL)
'''''''        End If
'''''''
'''''''        If r.RecordCount > 0 Then
''''''''            r.MoveLast
'''''''            r.MoveFirst
'''''''
'''''''            MabseF_List.PrjTree.Enabled = False
'''''''
'''''''            Pb.Max = r.RecordCount
'''''''
'''''''            Clessidra True
'''''''
'''''''            While Not r.EOF
'''''''        '        Clessidra True
'''''''
'''''''                'Controlla se l'oggetto ha un errore
'''''''                If Count_Error_Object(r!IdOggetto) > 0 Then
'''''''                    Lista.ListItems.Add , , Format(r!IdOggetto, "000000"), , 5
'''''''                Else
'''''''                    Lista.ListItems.Add , , Format(r!IdOggetto, "000000"), , 6
'''''''                End If
'''''''
'''''''                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , r!Nome
'''''''                Lista.ListItems(Lista.ListItems.Count).ListSubItems(1).Bold = True
'''''''
'''''''                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , r!Tipo
'''''''                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , wListError
'''''''                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Trim(r!DtParsing & " ")
'''''''                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Crea_Directory_Progetto(r!Directory_Input, GbPathDef)
'''''''                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Trim(r!DtImport & " ")
'''''''
'''''''                r.MoveNext
'''''''
'''''''                Cont = Cont + 1
'''''''
'''''''                If Pb.Value < Pb.Max Then
'''''''                    Pb.Value = Pb.Value + 1
'''''''                End If
'''''''
'''''''                Pb.Refresh
'''''''
'''''''            Wend
'''''''
'''''''            Clessidra False
'''''''
'''''''            MabseF_List.PrjTree.Enabled = True
'''''''
'''''''            r.Close
'''''''
'''''''            Lista.ListItems(1).Selected = True
'''''''            Call MabseF_List.GridList_ItemClick(Lista.ListItems(1))
'''''''
'''''''        End If
'''''''
'''''''    End If
'''''''
'''''''    Carica_Lista_Oggetti = Cont
'''''''    Pb.Value = 0
'''''''
'''''''    Call Terminate_Flickering_Object
'''''''
'''''''    'resize della lista
'''''''    Call Resize_ListView(Lista)
'''''''
'''''''    Exit Function
'''''''EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
'''''''    ERR.Clear
'''''''
'''''''End Function

Function Carica_Lista_Oggetti(Lista As ListView, sql As String) As Long
  Dim r As Recordset
  Dim Cont As Long
  Dim kPos As Integer
  Dim wstr1 As String
  Dim wStr2 As String
  Dim i As Long, Ind As Long, idObj As Long
  
  On Error GoTo EH
     
  'S 7-1-2008
  PsHideIgnore = LeggiParam("PS_HIDE_IGNORE") = "Yes"
  
  'Terminate_Flickering_Object MabseF_List.PBar.hWnd
  Terminate_Flickering MabseF_List.PBar1.hWnd

  Lista.ListItems.Clear
  Lista.Sorted = False

  If sql <> "" Then
    kPos = InStr(UCase(sql), " ORDER ")

    If kPos > 0 Then
      wstr1 = Mid$(sql, 1, kPos)
      wStr2 = Mid$(sql, kPos)
    Else
      wstr1 = sql
      wStr2 = ""
    End If

    sql = wstr1 & wStr2
    
    Set r = DLLFunzioni.Open_Recordset(sql)
    If r.RecordCount Then
      While Not r.EOF
        If TN(r!errLevel) = "" Then
          Lista.ListItems.Add , , Format(r!IdOggetto, "000000"), , 6
        Else
          Lista.ListItems.Add , , Format(r!IdOggetto, "000000"), , 5
        End If
       
        Lista.ListItems(Cont + 1).ListSubItems.Add , Trim(r!Estensione & ""), Trim(r!Nome)
        Lista.ListItems(Cont + 1).ListSubItems(NAME_OBJ).Bold = True
        Lista.ListItems(Cont + 1).ListSubItems.Add , , Trim(r!Tipo)
        ' S 10-1-2008
        Lista.ListItems(Cont + 1).ListSubItems.Add , , IIf(r!DLI, "X", "")
        Lista.ListItems(Cont + 1).ListSubItems.Add , , IIf(r!Cics, "X", "")
        
        Lista.ListItems(Cont + 1).ListSubItems.Add , , TN(r!errLevel)
        Lista.ListItems(Cont + 1).ListSubItems.Add , , " " & Trim(TN(r!NumRighe))
       
        If TN(r!Notes) <> "" Then
          'stefanopippo: serve cos�, fatelo meglio, ma mi serve troppo
          ' Lista.ListItems(Cont + 1).ListSubItems.Add , , "X"
          Lista.ListItems(Cont + 1).ListSubItems.Add , , TN(r!Notes)
          Lista.ListItems(Cont + 1).ListSubItems(Lista.ListItems(Cont + 1).ListSubItems.count).Bold = True
        Else
          Lista.ListItems(Cont + 1).ListSubItems.Add , , ""
        End If
       
        If r!parsingLevel > 0 Then
          Lista.ListItems(Cont + 1).ListSubItems.Add , , FormattaData(Trim(r!DtParsing & " "))
        Else
          Lista.ListItems(Cont + 1).ListSubItems.Add , , ""
        End If
       
        Lista.ListItems(Cont + 1).ListSubItems.Add , , Crea_Directory_Progetto(r!directory_input, ProjectPath)
        Lista.ListItems(Cont + 1).ListSubItems.Add , , FormattaData(Trim(r!DtImport & " "))
      
        r.MoveNext

        Cont = Cont + 1
      Wend
      r.Close
      
''''      'S 8-1-2008
''''      If PsHideIgnore Then
''''        For Ind = 1 To MabseF_List.GridList.ListItems.count
''''          'If MabseF_List.GridList.ListItems(Ind).Selected Then
''''             idObj = MabseF_List.GridList.ListItems(Ind).Text
''''             DllParser.UpdateErrLevel idObj, Ind
''''          'End If
''''        Next Ind
''''      End If
      
      Clessidra False
          
      MabseF_List.PrjTree.Enabled = True

      Lista.SortKey = 1
      Lista.Sorted = True

      Lista.ListItems(1).Selected = True
        
      MabseF_List.GridList_Click
'      MabseF_List.GridList_ItemClick Lista.ListItems(1)
    End If
  End If
  
  Carica_Lista_Oggetti = Cont

  'resize della lista
  Resize_ListView Lista

  MabseF_List.PrjTree.Enabled = True
  
  Screen.MousePointer = vbDefault
  
  'Seleziona il primo della lista
  Dim t As Long
  
  For t = 1 To MabseF_List.GridList.ListItems.count
    MabseF_List.GridList.ListItems(t).Selected = False
  Next t
  
  If MabseF_List.GridList.ListItems.count Then
    MabseF_List.GridList.ListItems(1).Selected = True
  End If
  
  Exit Function
EH:
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Function

Sub Carica_TreeView_Progetto(trw As TreeView, Optional Button As Long, Optional mTypeLoad)
  Dim r As Recordset
  Dim i As Long
  Dim Lev As String
  Dim Parametro As String
  Dim AreaApp As String
  Dim Ret As String
  Dim Percorso As String
  
  Dim Key As String
  Dim Texto As String
  Dim Image As Long
  Dim Relative As String
  Dim Tipo As Long
  Dim wMode As String
  
  Dim rs As Recordset
  
  'carica la TreeView in base al bottone cliccato
  trw.Nodes.Clear
  
  If Verifica_Connessione_ADO Then
    'carica i SottoLivelli della TreeView dalla tabella Oggetti
    Set rs = DLLFunzioni.Open_Recordset("Select * From BS_TreeView Order By Id")
    If rs.RecordCount Then
      wMode = "FAST"
    Else
      wMode = "SLOW"
    End If
    
    If IsMissing(mTypeLoad) And wMode = "FAST" Then
      wMode = "FAST"
    ElseIf IsMissing(mTypeLoad) And wMode = "SLOW" Then
      wMode = "SLOW"
    ElseIf Not IsMissing(mTypeLoad) And wMode = "SLOW" Then
      wMode = "SLOW"
    ElseIf Not IsMissing(mTypeLoad) And wMode = "FAST" Then
      wMode = mTypeLoad
    End If
    
    If wMode = "FAST" Then
      Carica_TreeView_Progetto_Fast trw
    ElseIf wMode = "SLOW" Then
      Carica_Dettaglio_TreeView_Progetto trw
    End If
    
    'salva la struttura nel database
    Salva_Struttura_TreeView_In_DB trw
  End If
  
  'carica l'array in memoria
  Carica_Array_TreeView trw
End Sub

Public Sub Carica_Dettaglio_TreeView_SecondoLivello(trw As TreeView)
  Dim r As Recordset
  Dim NodeExist As Boolean
  Dim Key As String
  Dim Image As Long
  Dim Texto As String
  Dim Tipo As Long
  Dim Relative As String
  
  Set r = DLLFunzioni.Open_Recordset("Select * From BS_TreeView Order By Id")
  While Not r.EOF
    Key = r!Key
    Image = r!Image
    Texto = r!Testo
    
    If InStr(1, Texto, "\") <> 0 Then
      Texto = Crea_Directory_Progetto(Texto, ProjectPath)
    End If
    
    NodeExist = Controlla_Nodo(trw, Texto, Key, Trim(r!Livello))
      
    If Not NodeExist Then
      If r!Tipo Then
        Relative = r!NRelative
        Tipo = r!Tipo
        
        trw.Nodes.Add Relative, Tipo, Trim(Key), Texto, Image
        
        Select Case Trim(r!Livello)
          Case "L0"
            trw.Nodes(trw.Nodes.count).Bold = True
            trw.Nodes(trw.Nodes.count).ForeColor = vbRed
            trw.Nodes(trw.Nodes.count).Text = UCase(trw.Nodes(trw.Nodes.count).Text)
          Case "L1"
            trw.Nodes(trw.Nodes.count).Bold = True
            trw.Nodes(trw.Nodes.count).ForeColor = vbBlue
        End Select
      Else
        trw.Nodes.Add , , Key, Texto, Image
        trw.Nodes(trw.Nodes.count).Bold = True
        trw.Nodes(trw.Nodes.count).ForeColor = vbBlack
      End If
      trw.Nodes(trw.Nodes.count).Tag = Trim(r!Livello)
    End If
    r.MoveNext
  Wend
  r.Close
End Sub

Public Function Controlla_Nodo(trw As TreeView, Nome As String, Key As String, Livello As String) As Boolean
  Dim i As Long
  
  For i = 1 To trw.Nodes.count
    If trw.Nodes(i).Tag = Livello And Trim(UCase(trw.Nodes(i).Text)) = Trim(UCase(Nome)) And _
       Trim(UCase(trw.Nodes(i).Key)) = Trim(UCase(Key)) Then
      Controlla_Nodo = True
      Exit Function
    End If
  Next i
End Function

Sub Carica_TreeView_Progetto_Fast(trw As TreeView)
   Dim rs As Recordset
   Dim wKey As String
   Dim wRel As String
   Dim wText As String
   Dim wImg As Long
   Dim wTipo As Long
   
   Set rs = DLLFunzioni.Open_Recordset("Select * From BS_TreeView Order by ID ")
   If rs.RecordCount Then
     trw.Nodes.Clear
     While Not rs.EOF
       wKey = Trim(rs!Key)
       wRel = DLLFunzioni.TN(rs!NRelative, vbString, True)
       
       If InStr(1, Trim(rs!Testo), "$") Then
         wText = DLLFunzioni.Crea_Directory_Progetto(rs!Testo, ProjectPath)
       Else
         wText = rs!Testo
       End If
       
       wImg = rs!Image
       wTipo = rs!Tipo
       
       If wRel = "" Then
         trw.Nodes.Add , , wKey, wText, wImg
       Else
         trw.Nodes.Add wRel, wTipo, wKey, wText, wImg
       End If
       
       trw.Nodes.Item(trw.Nodes.count).Tag = rs!Livello
       
       Select Case rs!Livello
         Case "L0"
           trw.Nodes.Item(trw.Nodes.count).Bold = True
           If wKey = "MB" Then
             trw.Nodes.Item(trw.Nodes.count).ForeColor = vbBlack
           Else
             trw.Nodes.Item(trw.Nodes.count).ForeColor = vbRed
           End If
         Case "L1"
           trw.Nodes.Item(trw.Nodes.count).Bold = True
           trw.Nodes.Item(trw.Nodes.count).ForeColor = vbBlue
         Case "L2"
           trw.Nodes.Item(trw.Nodes.count).Bold = False
           trw.Nodes.Item(trw.Nodes.count).ForeColor = vbBlack
       End Select
       rs.MoveNext
     Wend
   End If
   rs.Close
End Sub

Sub Carica_Dettaglio_TreeView_Progetto(trw As TreeView)
  Dim r As Recordset, R1 As Recordset, R2 As Recordset
  Dim j As Long, k As Long, w As Long, i As Long
  Dim Directory As String
  Dim KeyC As String
  Dim TipoSez As String
  Dim wLiv0 As String, wLiv1 As String, wLiv2 As String
   
  j = 1
  k = 1
  w = 1
   
  'cancella la TreeView
  trw.Nodes.Clear
     
  'crea gli elementi fissi
  trw.Nodes.Add , , "MB", "Project", 1 'PROJECT
  trw.Nodes.Item(trw.Nodes.count).Tag = "L0"
  trw.Nodes.Item(trw.Nodes.count).Bold = True
  trw.Nodes.Item(trw.Nodes.count).ForeColor = vbBlack
   
  Set r = DLLFunzioni.Open_Recordset("Select distinct area_appartenenza,Livello1,Livello2  From BS_Oggetti ") ' Order By Area_Appartenenza DESC,Livello1,livello2")
  While Not r.EOF
    DoEvents
    If wLiv0 <> Trim(r!Area_Appartenenza) Then
      wLiv0 = Trim(r!Area_Appartenenza)
      
      trw.Nodes.Add "MB", tvwChild, Trim(UCase(wLiv0)), r!Area_Appartenenza, 5
      trw.Nodes.Item(trw.Nodes.count).Tag = "L0"
      trw.Nodes.Item(trw.Nodes.count).Bold = True
      trw.Nodes.Item(trw.Nodes.count).ForeColor = vbRed
      
      If wLiv1 <> Trim(r!Livello1) Then
        wLiv1 = Trim(r!Livello1)
        trw.Nodes.Add Trim(UCase(wLiv0)), tvwChild, Trim(UCase(wLiv0)) & "#" & Trim(r!Livello1), Trim(r!Livello1), 4
        trw.Nodes.Item(trw.Nodes.count).Tag = "L1"
        trw.Nodes.Item(trw.Nodes.count).Bold = True
        trw.Nodes.Item(trw.Nodes.count).ForeColor = vbBlue
        
        wLiv2 = Trim(r!Livello2)
        trw.Nodes.Add Trim(UCase(wLiv0)) & "#" & Trim(r!Livello1), tvwChild, Trim(UCase(wLiv0)) & "#" & Trim(r!Livello1) & "@" & Trim(r!Livello2), Crea_Directory_Progetto(Trim(r!Livello2), ProjectPath), 2
        trw.Nodes.Item(trw.Nodes.count).Tag = "L2"
        trw.Nodes.Item(trw.Nodes.count).ForeColor = vbBlack
            
      ElseIf wLiv1 = Trim(r!Livello1) Then
        wLiv2 = Trim(r!Livello2)
        trw.Nodes.Add Trim(UCase(wLiv0)) & "#" & Trim(r!Livello1), tvwChild, Trim(UCase(wLiv0)) & "#" & Trim(r!Livello1) & "@" & Trim(r!Livello2), Crea_Directory_Progetto(Trim(r!Livello2), ProjectPath), 2
        trw.Nodes.Item(trw.Nodes.count).Tag = "L2"
        trw.Nodes.Item(trw.Nodes.count).ForeColor = vbBlack
        
      End If
    ElseIf wLiv0 = Trim(r!Area_Appartenenza) Then
      If wLiv1 <> Trim(r!Livello1) Then
        wLiv1 = Trim(r!Livello1)
        trw.Nodes.Add Trim(UCase(wLiv0)), tvwChild, Trim(UCase(wLiv0)) & "#" & Trim(r!Livello1), Trim(r!Livello1), 4
        trw.Nodes.Item(trw.Nodes.count).Tag = "L1"
        trw.Nodes.Item(trw.Nodes.count).Bold = True
        trw.Nodes.Item(trw.Nodes.count).ForeColor = vbBlue
        
        wLiv2 = Trim(r!Livello2)
        trw.Nodes.Add Trim(UCase(wLiv0)) & "#" & Trim(r!Livello1), tvwChild, Trim(UCase(wLiv0)) & "#" & Trim(r!Livello1) & "@" & Trim(r!Livello2), Crea_Directory_Progetto(Trim(r!Livello2), ProjectPath), 2
        trw.Nodes.Item(trw.Nodes.count).Tag = "L2"
        trw.Nodes.Item(trw.Nodes.count).ForeColor = vbBlack
        
      ElseIf wLiv1 = Trim(r!Livello1) Then
        wLiv2 = Trim(r!Livello2)
        trw.Nodes.Add Trim(UCase(wLiv0)) & "#" & Trim(r!Livello1), tvwChild, Trim(UCase(wLiv0)) & "#" & Trim(r!Livello1) & "@" & Trim(r!Livello2), Crea_Directory_Progetto(Trim(r!Livello2), ProjectPath), 2
        trw.Nodes.Item(trw.Nodes.count).Tag = "L2"
        trw.Nodes.Item(trw.Nodes.count).ForeColor = vbBlack
      End If
    End If
    r.MoveNext
    DoEvents
  Wend
  r.Close
  
  'carica l'array in memoria
  Carica_Array_TreeView trw
  
  'colora le varie sezioni
  For i = 1 To trw.Nodes.count
    Select Case Trim(UCase(trw.Nodes(i).Text))
      Case "SOURCE", "COMMAND", "DATABASES"
        trw.Nodes(i).ForeColor = vbRed
      Case "BACKUP"
        trw.Nodes(i).ForeColor = &H8000&     ' verde scuro
    End Select
  Next i
End Sub

Public Sub Seleziona_Filtro_Selezione(Lista As ListView, Filter As MSFlexGrid, Stato As Long, Index As Integer)
  Select Case Stato
    Case tbrPressed
      Lista.Visible = False
      Filter.Visible = True
      Filter.Height = 915
      '''MabseF_Prj.fraToolbar.Buttons(Index + 1).Enabled = True
    Case tbrUnpressed
      Lista.Visible = True
      Filter.Visible = False
      '''MabseF_Prj.fraToolbar.Buttons(Index + 1).Enabled = False
      MabseF_List.txtFilter.Visible = False
      
      'ricarica la lista oggetti precedente
      'MabseF_List.TotalObj.Text = Carica_Lista_Oggetti(Lista, SQL_Selection_List, MabseF_List.PBar)
  End Select

  'imposta la riga di intestazione
  Filter.Row = 0
  
  'imposta le colonne
  Filter.Col = 0
  Filter.Text = "ID"
  'Filter.ColWidth(0) = MabseF_List.GridList.ColumnHeaders(1).Width
  
  Filter.Col = NAME_OBJ
  Filter.Text = "Name"
  'Filter.ColWidth(1) = MabseF_List.GridList.ColumnHeaders(2).Width
  
  Filter.Col = TYPE_OBJ
  Filter.Text = "Type"
  'Filter.ColWidth(2) = MabseF_List.GridList.ColumnHeaders(3).Width
  
  Filter.Col = DLI_OBJ
  Filter.Text = "DLI"
  'Filter.ColWidth(3) = MabseF_List.GridList.ColumnHeaders(4).Width
  
  Filter.Col = CICS_OBJ
  Filter.Text = "CICS"
  'Filter.ColWidth(4) = MabseF_List.GridList.ColumnHeaders(5).Width
  
  Filter.Col = RC_OBJ
  Filter.Text = "RC"
  'Filter.ColWidth(5) = MabseF_List.GridList.ColumnHeaders(6).Width
  
  Filter.Col = LOCS_OBJ
  Filter.Text = "LOCS"
  'Filter.ColWidth(6) = MabseF_List.GridList.ColumnHeaders(7).Width
  
  Filter.Col = NOTES_OBJ
  Filter.Text = "NOTES"
  'Filter.ColWidth(7) = MabseF_List.GridList.ColumnHeaders(8).Width
  
  Filter.Col = PARS_DATE_OBJ
  Filter.Text = "Parsing Date"
  'Filter.ColWidth(8) = MabseF_List.GridList.ColumnHeaders(9).Width
  
  Filter.Col = DIR_OBJ
  Filter.Text = "Directory"
  'Filter.ColWidth(9) = MabseF_List.GridList.ColumnHeaders(10).Width
  
  Filter.Col = IMP_DATE_OBJ
  Filter.Text = "Import Date"
  'Filter.ColWidth(10) = MabseF_List.GridList.ColumnHeaders(11).Width
End Sub

Public Sub ON_OFF_Button_ToolBar(Tool As Object, Val As Boolean, Optional IndexButton)
  Dim i As Long, idx As Long
  
'''  If IsMissing(IndexButton) Then
'''    For i = 1 To Tool
'''      'Tool.Buttons(i).Enabled = Val 'A 23/05/2006
'''      If UCase(Tool.Buttons(i).Key) = "DELETE_FOLDER" Then
'''        Tool.Buttons(i).Enabled = False
'''      End If
'''    Next i
'''    Exit Sub
'''  End If
'''
'''  For i = 1 To Tool.Buttons.count
'''    If Tool.Buttons(i).Index = IndexButton Then
'''      idx = i
'''      Exit For
'''    End If
'''  Next i
'''  Tool.Buttons(idx).Enabled = Val
End Sub

Public Sub Execute_Filter(List As MSFlexGrid, LObj As ListView, totObj As TextBox, Stato As Long)
  Dim Prop() As String, sql As String, AppStr As String
  Dim Cont As Long, i As Long, FinalS As String
  
  Screen.MousePointer = vbHourglass
  If Stato = 0 Then
    '''MabseF_Prj.fraToolbar.Buttons(24).Value = tbrUnpressed
    MabseF_List.TotalObj.Text = Carica_Lista_Oggetti(LObj, SQL_Selection_List)
  Else
    sql = ""
    Cont = 0
    AppStr = ""
    FinalS = ""
    
    ReDim Prop(0)
    'memorizza le variabili di query
    List.Row = 1
    For i = 0 To List.Cols - 1
      List.Col = i
      ReDim Preserve Prop(UBound(Prop) + 1)
      Prop(UBound(Prop)) = List.Text
    Next i
    
    'crea la query
    For i = 1 To UBound(Prop)
      If Prop(i) <> "" Then
        Prop(i) = Sostituisci_ASTER_PERC(Prop(i))
        Select Case i
          Case 1
            ' Ma 10/04/2008 : TEMPORANEO!!!!!!!!!!!!!!!
            ' Bisogna rivedere bene il giro per il filtro: fa anche una Open_Recordset!!!!!!
            If Left(Prop(i), 1) = ">" Then
              sql = "IdOggetto > " & Mid(Prop(i), 2)
            ElseIf Left(Prop(i), 1) = "<" Then
              sql = "IdOggetto < " & Mid(Prop(i), 2)
            Else
              sql = "IdOggetto = " & Prop(i)
            End If
            Cont = Cont + 1
            
          Case NAME_OBJ + 1
            If InStr(1, Prop(i), "%") <> 0 Then
              sql = "Nome Like '" & Prop(i) & "'"
            Else
              sql = "Nome = '" & Prop(i) & "'"
            End If
            Cont = Cont + 1
            
          Case TYPE_OBJ + 1
            If InStr(1, Prop(i), "%") <> 0 Then
              sql = "Tipo Like '" & Prop(i) & "'"
            Else
              sql = "Tipo = '" & Prop(i) & "'"
            End If
            Cont = Cont + 1
                      
          Case DLI_OBJ + 1
            If Prop(i) = " " Then
              sql = "DLI = False "
              Cont = Cont + 1
            Else
              If Len(Trim(Prop(i))) Then
                sql = "DLI = True "
                Cont = Cont + 1
              End If
            End If
          Case CICS_OBJ + 1
            If Prop(i) = " " Then
              sql = "CICS = False "
              Cont = Cont + 1
            Else
              If Len(Trim(Prop(i))) Then
                sql = "CICS = True "
                Cont = Cont + 1
              End If
            End If
          Case RC_OBJ + 1
            If InStr(1, Prop(i), "%") <> 0 Then
              sql = "ErrLevel Like '" & Prop(i) & "'"
            Else
              sql = "ErrLevel = '" & Prop(i) & "'"
            End If
            Cont = Cont + 1
          
          Case LOCS_OBJ + 1
            If Left(Prop(i), 1) = ">" Then
              sql = "NUMRIGHE > " & Mid(Prop(i), 2)
            ElseIf Left(Prop(i), 1) = "<" Then
              sql = "NUMRIGHE < " & Mid(Prop(i), 2)
            Else
              sql = "NUMRIGHE = " & Prop(i)
            End If
            Cont = Cont + 1
          
          Case NOTES_OBJ + 1
            If InStr(1, Prop(i), "%") <> 0 Then
              sql = "NOTES Like '" & Prop(i) & "'"
            Else
              sql = "NOTES = '" & Prop(i) & "'"
            End If
            Cont = Cont + 1
          
          Case PARS_DATE_OBJ + 1
            If InStr(1, Prop(i), "%") <> 0 Then
              sql = "DtParsing Like '" & Prop(i) & "'"
            Else
              sql = "DtParsing = '" & Prop(i) & "'"
            End If
            Cont = Cont + 1
            
          Case DIR_OBJ + 1
            If InStr(1, Prop(i), "%") <> 0 Then
              sql = "Directory_Input Like '" & Prop(i) & "'"
            Else
              sql = "Directory_Input = '" & Prop(i) & "'"
            End If
            Cont = Cont + 1
            
          Case IMP_DATE_OBJ + 1
            If InStr(1, Prop(i), "%") <> 0 Then
              sql = "DtImport Like '" & Prop(i) & "'"
            Else
              sql = "DtImport = '" & Prop(i) & "'"
            End If
            Cont = Cont + 1
        End Select
        
        If sql <> "" Then
          If Cont > 1 Then
            AppStr = " And " & sql
          Else
            AppStr = sql
          End If
        End If
        
        FinalS = FinalS & AppStr
        AppStr = ""
        sql = ""
      End If
    Next i
    If FinalS <> "" Then
      FinalS = "Select * From BS_Oggetti Where " & FinalS & " Order By Nome"
    End If
    
    'visualizza la lista oggetti
    List.Visible = True
    
    'disabilita il bottone di filtro
    'Call ON_OFF_Button_ToolBar(MabseF_Prj.fraToolbar, False, 23)
    '''MabseF_Prj.fraToolbar.Buttons(23).Value = tbrUnpressed
    '''MabseF_Prj.fraToolbar.Buttons(24).Value = tbrPressed
    MabseF_List.txtFilter.Visible = False
    
    LObj.Visible = True
    
    'carica la lista oggetti
    totObj.Text = Carica_Lista_Oggetti(LObj, FinalS)
    FinalS = ""
    Screen.MousePointer = vbDefault
  End If
End Sub

Public Sub Ordina_Lista(Lista As ListView, Direction As Long)
  If ListObjCol = 0 Then
    ListObjCol = 1
  End If
  
  Lista.SortKey = ListObjCol - 1
  Lista.SortOrder = Direction
  Lista.Sorted = True
End Sub

Public Function Restituisci_Colonna_Da_X_Mouse(List As ListView, ByVal X As Single) As Long
  Dim i As Long
  Dim Somma As Long
  Dim Diff As Long
  
  For i = 1 To List.ColumnHeaders.count
    Somma = Somma + List.ColumnHeaders(i).Width
    Diff = List.ColumnHeaders(i).Width
    
    If Somma - Diff < X And Somma > X Then
      Restituisci_Colonna_Da_X_Mouse = i
      Exit Function
    End If
  Next i
End Function

Public Sub Ricerca_In_Lista(List As ListView, Chiave As String, IndexCol As Long)
  Dim i As Long
  
  If IndexCol = 0 Then IndexCol = 1
  
  For i = 1 To List.ListItems.count - 1
    List.ListItems(i).Selected = False
  Next i
  
  For i = 1 To List.ListItems.count - 1
    If IndexCol = NAME_OBJ Then
      Chiave = Format(Chiave, "000000")
      If Mid(UCase(List.ListItems(i).Text), 1, Len(UCase(Chiave))) = UCase(Chiave) Then
        List.ListItems(i).Selected = True
        List.ListItems(i).EnsureVisible
        Exit For
      End If
    Else
      If Mid(UCase(List.ListItems(i).ListSubItems(IndexCol - 1).Text), 1, Len(UCase(Chiave))) = UCase(Chiave) Then
        List.ListItems(i).Selected = True
        List.ListItems(i).EnsureVisible
        Exit For
      End If
    End If
  Next i
End Sub

Public Sub Memorizza_Oggetti_Selezionati(List As ListView, Status As Long)
  Dim i As Long
  Dim r As Recordset
  Dim IdOggetto As Long
  
  ReDim Oggetti(0)
  
  For i = 1 To List.ListItems.count
    Clessidra True
    If List.ListItems(i).Selected = True Then
      'evidenzia gli oggetti
      If Status = GbCopia Then
        List.ListItems(i).SmallIcon = 3
      End If
      
      If Status = GbTaglia Then
        List.ListItems(i).SmallIcon = 2
      End If
      
      'memorizza gli oggetti
      ReDim Preserve Oggetti(UBound(Oggetti) + 1)
      
      'apre il recordset
      Set r = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Val(List.ListItems(i)))
      If r.RecordCount Then
        r.MoveFirst
        Oggetti(UBound(Oggetti)).IdOggetto = r!IdOggetto
        Oggetti(UBound(Oggetti)).Nome = r!Nome
        Oggetti(UBound(Oggetti)).Tipo = r!Tipo
        Oggetti(UBound(Oggetti)).directory_input = r!directory_input
        Oggetti(UBound(Oggetti)).Directory_Output = Trim(r!Directory_Output & " ")
        Oggetti(UBound(Oggetti)).Area_App = r!Area_Appartenenza
        Oggetti(UBound(Oggetti)).Livello1 = r!Livello1
        Oggetti(UBound(Oggetti)).Livello2 = r!Livello2
      End If
      r.Close
      
    End If
    Clessidra False
  Next i
End Sub

Function Restituisci_Livello_Su_TreeView(tree As TreeView, Index As Long) As Long
  Dim root As String
  Dim Livello As Long
  
  'NB. Scende fino al 5� Livello
  
  'trova la root
  root = UCase(tree.Nodes(1).root)
  
  If UCase(tree.Nodes(Index)) = root Then
    Restituisci_Livello_Su_TreeView = 0
  ElseIf UCase(tree.Nodes(Index).Parent) = root Then
    Livello = 0
    Restituisci_Livello_Su_TreeView = Livello
  ElseIf UCase(tree.Nodes(Index).Parent.Parent) = root Then
    'sale di un livello
    Livello = 1
    Restituisci_Livello_Su_TreeView = Livello
  ElseIf UCase(tree.Nodes(Index).Parent.Parent.Parent) = root Then
    'sale di un livello
    Livello = 2
    Restituisci_Livello_Su_TreeView = Livello
  ElseIf UCase(tree.Nodes(Index).Parent.Parent.Parent.Parent) = root Then
    'sale di un livello
    Livello = 3
    Restituisci_Livello_Su_TreeView = Livello
  ElseIf UCase(tree.Nodes(Index).Parent.Parent.Parent.Parent.Parent) = root Then
    'sale di un livello
    Livello = 4
    Restituisci_Livello_Su_TreeView = Livello
  End If
End Function

Public Sub Aggiorna_File_TreeView(tree As TreeView)
  Dim i As Long, j As Long
  Dim Voce As String
  Dim Param As String
  Dim Percorso As String
  Dim LivKey As String
  
  Dim Tag As String
  
  Percorso = GbPathPrj & "\Config.ini"
  
  If Dir(Percorso, vbNormal) <> "" Then
    'elimina il file
    Kill Percorso
  End If
  
  'salva il file di config
  '1) Apre il file
  Open Percorso For Output As #1
  
  For j = 1 To UBound(TreeViewPrj)
    'scrive nel file
    If Trim(TreeViewPrj(j).Relative) = "" Then TreeViewPrj(j).Relative = " "
    Print #1, TreeViewPrj(j).Relative & "," & IsChild(tree, j) & "," & TreeViewPrj(j).Key & "," & TreeViewPrj(j).Caption & "," & TreeViewPrj(j).Image & Chr(10)
  Next j
  
  'chiude il file
  Close #1
End Sub

Public Function Verifica_Esistenza_File_Config_Progetto()
  Dim exFile As String
  
  exFile = Dir(GbPathPrj & "\Config.ini", vbNormal)
  If Trim(exFile) = "" Then
    Verifica_Esistenza_File_Config_Progetto = False
    Exit Function
  Else
    Verifica_Esistenza_File_Config_Progetto = True
    Exit Function
  End If
End Function

Public Function IsChild(tree As TreeView, Index As Long) As Long
  Dim root As String
  
  'controlla se l'elemento della tree � un figlio
  root = UCase(tree.Nodes(1).root)
  If root = UCase(tree.Nodes(Index)) Then
    IsChild = 0
    Exit Function
  Else
    IsChild = tvwChild
    Exit Function
  End If
End Function

Public Sub Carica_Array_TreeView(tree As TreeView)
  Dim i As Long
  
  'inizializza l'array
  ReDim TreeViewPrj(0)
  For i = 1 To tree.Nodes.count
    ReDim Preserve TreeViewPrj(UBound(TreeViewPrj) + 1)
    TreeViewPrj(UBound(TreeViewPrj)).Relative = Restituisci_Relative_Da_Key(tree.Nodes(i).Key)
    TreeViewPrj(UBound(TreeViewPrj)).RelationShip = IsChild(tree, tree.Nodes(i).Index)
    TreeViewPrj(UBound(TreeViewPrj)).Caption = tree.Nodes(i).Text
    TreeViewPrj(UBound(TreeViewPrj)).Key = tree.Nodes(i).Key
    TreeViewPrj(UBound(TreeViewPrj)).Image = tree.Nodes(i).Image
  Next i
End Sub

Public Function MyFather(tree As TreeView, Ind As Long) As Long
  'Dal nodo cliccato restituisce l'index del padre
  Dim i As Long
  Dim Relative As String
  
  Relative = TreeViewPrj(Ind).Relative
  For i = 1 To UBound(TreeViewPrj)
    If UCase(TreeViewPrj(i).Key) = UCase(Relative) Then
      MyFather = i
      Exit For
    End If
  Next i
End Function

Public Function MyChild(tree As TreeView, Ind As Long) As Long
  'Dal nodo cliccato restituisce l'index del figlio
  Dim i As Long
  Dim Key As String
  
  Key = TreeViewPrj(Ind).Key
  For i = 1 To UBound(TreeViewPrj)
    If UCase(TreeViewPrj(i).Relative) = UCase(Key) Then
      MyChild = i
      Exit For
    End If
  Next i
End Function

Public Function Restituisci_Area_Appartenenza_Da_Nodo(tree As TreeView, Index As Long) As String
  'NB. Suppone che l'array in memoria contenente i dati sulla tree sia caricato
  Dim j As Long, i As Long
  Dim Area As String
  Dim Ind As Long
    
  Ind = Index
  If Mid(tree.Nodes(Ind).Key, 1, 2) = "MB" Then
    Area = Mid(tree.Nodes(Ind).Key, 4)
  Else
    If InStr(1, tree.Nodes(Ind).Key, "#") <> 0 Then
      Area = Mid(tree.Nodes(Ind).Key, 1, InStr(1, tree.Nodes(Ind).Key, "#") - 1)
    Else
      Area = tree.Nodes(Ind).Key
    End If
  End If
  Restituisci_Area_Appartenenza_Da_Nodo = Area
End Function

Public Function Restituisci_Livello_Nodo() As Long
  If (TrwLiv1) <> "" And (TrwLiv2) <> "" Then
    Restituisci_Livello_Nodo = 2
    Exit Function
  End If
  
  If (TrwLiv1) <> "" And (TrwLiv2) = "" Then
    Restituisci_Livello_Nodo = 1
    Exit Function
  End If
  
  If (TrwLiv1) = "" And (TrwLiv2) = "" Then
    Restituisci_Livello_Nodo = 0
    Exit Function
  End If
End Function

Public Function Restituisci_Max_Numero_DIRxx_In_TreeView(tree As TreeView)
  Dim Max As Long, i As Long
  
  'trova il max numero usato per le chiavi _DIRxx
  For i = 1 To tree.Nodes.count
    If InStr(1, tree.Nodes(i).Key, "_DIR") <> 0 Then
      If Max < Val(Mid(tree.Nodes(i).Key, InStr(1, tree.Nodes(i).Key, "_DIR") + 4)) Then
        Max = Val(Mid(tree.Nodes(i).Key, InStr(1, tree.Nodes(i).Key, "_DIR") + 4))
      End If
    End If
  Next i
  Restituisci_Max_Numero_DIRxx_In_TreeView = Max
End Function

Public Function Resize_ListView(List As ListView)
  Dim i As Long, j As Long
  Dim Max As Long, MaxList As Long
  Dim Zero As Boolean
  
  For i = 2 To List.ColumnHeaders.count - 1
    If (i - 1 <> NOTES_OBJ) And (i - 1 <> DIR_OBJ) Then
      Max = Len(Trim(List.ColumnHeaders(i).Text)) * 190
      Zero = True
      For j = 1 To List.ListItems.count
        MaxList = Len(Trim(List.ListItems(j).ListSubItems(i - 1).Text)) * 190
        
        If MaxList <> 0 Then
          Zero = False
        End If
        
        If Max < MaxList Then
          Max = MaxList
        End If
      Next j
      List.ColumnHeaders(i).Width = Max
    End If
  Next i
End Function

Public Function Conta_Righe_File(Percorso As String) As Long
  Dim Righe As Long
  Dim NumFile As Long
  Dim line As String
  
  NumFile = FreeFile
  
  'conta le righe
  Open OggSel.directory_input & "\" & OggSel.Nome For Input As #NumFile
  Do While Not EOF(NumFile)
    Line Input #NumFile, line
    Righe = Righe + 1
  Loop
  Close #NumFile
  
  Conta_Righe_File = Righe
End Function

' Fare una REPLACE faceva schifo?
Public Function Sostituisci_ASTER_PERC(Stringa) As String
  Dim i As Long
  Dim ch As String
  Dim app As String
  
  For i = 1 To Len(Stringa)
    ch = Mid(Stringa, i, 1)
    If ch = "*" Then
      ch = "%"
    End If
    app = app & ch
  Next i
  Sostituisci_ASTER_PERC = app
End Function

Public Function Conta_Righe_Sorgente(Percorso As String) As Long
  Dim i As Long
  Dim SLine As String
  
  Open Percorso For Input As #1
  While Not EOF(1)
    Line Input #1, SLine
    i = i + 1
  Wend
  Close #1
  
  Conta_Righe_Sorgente = i
End Function

'''''''Public Function Obj_Is_Error(IdObj) As Boolean
'''''''  Dim r As Recordset
'''''''  Dim wErrDati As String
'''''''  Dim wErrPars As String
'''''''  Dim wErrLiv As String
'''''''  Dim wErrComp As String
'''''''  Dim wErrNum As String
'''''''
'''''''  Set r = DLLFunzioni.Open_Recordset("Select * From BS_Segnalazioni Where IdOggetto = " & IdObj)
'''''''
'''''''  If r.RecordCount > 0 Then
'''''''    r.MoveFirst
'''''''    While Not r.EOF
'''''''        If r!tipologia = "PARSER" Then wErrPars = "P"
'''''''        If r!tipologia = "DATI" Then wErrDati = "D"
'''''''        If r!tipologia = "COBOL" Then wErrComp = "C"
'''''''        If r!gravita = "I" Then wErrNum = "02"
'''''''        If r!gravita = "W" Then wErrNum = "04"
'''''''        If r!gravita = "E" Then wErrNum = "08"
'''''''        If r!gravita = "S" Then wErrNum = "16"
'''''''        If wErrLiv < wErrNum Then wErrLiv = wErrNum
'''''''        r.MoveNext
'''''''    Wend
'''''''    wListError = wErrDati & wErrPars & wErrComp & "-" & wErrLiv
'''''''    Obj_Is_Error = True
'''''''    r.Close
'''''''    Exit Function
'''''''  Else
'''''''    wListError = " "
'''''''  End If
'''''''
'''''''  Obj_Is_Error = False
'''''''End Function

Public Function Count_Error_Object(idObj) As Long
  Dim r As Recordset
  
  'Set r = DLLFunzioni.Open_Recordset("Select DISTINCT IdOggetto From BS_Segnalazioni Where IdOggetto = " & IdObj)
  'Count_Error_Object = r.RecordCount
  'r.Close
End Function

Public Function Restituisci_Relative_Da_Key(Key As String) As String
  Dim Start As Long, Fine As Long
  
  If InStr(1, Key, "#") <> 0 And InStr(1, Key, "@") <> 0 Then
    Start = InStr(1, Key, "#")
    Fine = InStr(Start + 1, Key, "@")
    
    Restituisci_Relative_Da_Key = Mid(Key, Start + 1, Fine - Start - 1)
    Exit Function
  End If
  
  If InStr(1, Key, "#") <> 0 And InStr(1, Key, "@") = 0 Then
    Start = InStr(1, Key, "#")
    
    Restituisci_Relative_Da_Key = Mid(Key, 1, Start - 1)
    Exit Function
  End If
  
  If InStr(1, Key, "#") = 0 And InStr(1, Key, "@") = 0 Then
    
    Restituisci_Relative_Da_Key = "MB"
    Exit Function
  End If
End Function

Public Sub Reset_TreeView_Image(trw As TreeView)
  Dim i As Long
  
  'resetta tutte le immagini della treeview
  For i = 1 To trw.Nodes.count
    If trw.Nodes(i).Tag = "L0" Then
      If Trim(trw.Nodes(i).Key) = "MB" Then
        trw.Nodes(i).Image = 1
      Else
        trw.Nodes(i).Image = 5
      End If
    End If

    If trw.Nodes(i).Tag = "L1" Then
      trw.Nodes(i).Image = 4
    End If

    If trw.Nodes(i).Tag = "L2" And trw.Nodes(i).Image <> 7 Then
      trw.Nodes(i).Image = 2
    End If
  Next i
End Sub

Public Function Crea_SQL_Selezione_Oggetti(Area As String, Liv1 As String, Liv2 As String) As String
  Dim sql As String
  
  If Trim(UCase(Area)) = "" Then 'L'utente ha cliccato Sulla <ROOT>
    'SQL = "Select * From BS_Oggetti Order By Nome"
  Else
    If Trim(Liv1) <> "" And Trim(Liv2) = "" Then
      sql = "Select * From BS_Oggetti Where Area_Appartenenza = '" & Trim(Area) & "' And Livello1 = '" & Trim(Liv1) & "' Order By Nome"
    End If
    
    If Trim(Liv1) <> "" And Trim(Liv2) <> "" Then
      sql = "Select * From BS_Oggetti Where Area_Appartenenza = '" & Trim(Area) & "' And Livello1 = '" & Trim(Liv1) & "' And Livello2 = '" & Trim(Crea_Directory_Parametrica(Liv2)) & "' Order By Nome"
    End If
    
    If Trim(Liv1) = "" And Trim(Liv2) = "" Then
      sql = "Select * From BS_Oggetti Where Area_Appartenenza = '" & Trim(Area) & "' Order By Nome"
    End If
  End If
    
  Crea_SQL_Selezione_Oggetti = Trim(sql)
End Function

Public Function Switch_TreeView_Image(trw As TreeView, Index As Long) As Long
  If trw.Nodes(TreePrj_Index).Image = 7 Then
    Switch_TreeView_Image = 7
    MsgBox "This folder is not avaliable...", vbExclamation, GbNomeProdotto
    Exit Function
  End If
  
  If trw.Nodes(TreePrj_Index).Image = 2 Or trw.Nodes(TreePrj_Index).Image = 4 Then
    Switch_TreeView_Image = 3
  Else
    If trw.Nodes(TreePrj_Index).Image = 5 Then
      Switch_TreeView_Image = 6
    Else
      Switch_TreeView_Image = 1
    End If
  End If
End Function

Public Sub Controlla_Esistenza_Directory_Su_TreeView(trw As TreeView)
  'Controlla se le directory al livello L2 esistono, se non esistono
  'incolla l'immagine di errore altrimenti altrimenti no
  Dim i As Long
  Dim ExDir As String
  
  For i = 1 To trw.Nodes.count
    If trw.Nodes(i).Tag = "L2" Then
      ExDir = Dir(trw.Nodes(i).Text, vbDirectory)
      
      If Trim(ExDir) = "" Then
        trw.Nodes(i).Image = 7
      Else
        trw.Nodes(i).Image = 2
      End If
    End If
  Next i
End Sub

Public Function Restituisci_PathPrj(PathPrj As String) As String
  Dim Sx As String
  Dim i As Long
  
  'Tronca al primo slash e isola tutto quello che c'� dalla posizione 1 a li
  For i = 1 To Len(PathPrj)
    If Mid(PathPrj, Len(PathPrj) - i, 1) = "\" Then
      Exit For
    End If
  Next i
  
  Sx = Mid(PathPrj, 1, Len(PathPrj) - i - 1)
  Restituisci_PathPrj = Trim(Sx)
End Function

Public Sub Abilita_Disabilita_CoolMenu(Valore As Boolean)
  Dim i As Long
  
End Sub

Public Sub Carica_Array_InfoPRJ()
  Dim r As Recordset
  
  ReDim InfoPRJ(0)
  
  Set r = DLLFunzioni.Open_Recordset("Select * From BS_InfoPRJ")
  If r.RecordCount Then
    r.MoveFirst
    'deve essere un solo record
    InfoPRJ(0).DateCreation = r!Date_Creation
    InfoPRJ(0).nomePrj = r!Project_Name
    InfoPRJ(0).Directory = r!localize_Directory & ""
    InfoPRJ(0).NumObj = r!Object_Number
  End If
  r.Close
End Sub

Public Sub Aggiorna_InfoPRJ()
  Dim r As Recordset
  
  Dim Cont As Long
  
  'conta gli oggetti
  Set r = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti")
  Cont = r.RecordCount
  r.Close
  
  Set r = DLLFunzioni.Open_Recordset("Select * From BS_InfoPRJ")
  If r.RecordCount Then
    r!Object_Number = Cont
    r.Update
    InfoPRJ(0).NumObj = Cont
  End If
  r.Close
End Sub

Public Sub Riordina_Type_Menu()
  Dim i As Long, j As Long
  
  ReDim DefMenuAPP(0)
  ReDim DefMenuAPP(0).Bottoni(0)
  
  'Cerca la voce Project
  For i = 1 To UBound(DefMenu)
    If Trim(UCase(DefMenu(i).ButtonMenu)) = "HOME" Then
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).ButtonMenu = DefMenu(i).ButtonMenu
      
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(0)
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(UBound(DefMenuAPP(UBound(DefMenuAPP)).Bottoni) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).Bottoni = DefMenu(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce DataManager
  For i = 1 To UBound(DefMenu)
    If Trim(UCase(DefMenu(i).ButtonMenu)) = "DATA MANAGER" Then
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).ButtonMenu = DefMenu(i).ButtonMenu
      
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(0)
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(UBound(DefMenuAPP(UBound(DefMenuAPP)).Bottoni) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).Bottoni = DefMenu(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce Dli2RDBMS
  For i = 1 To UBound(DefMenu)
    If DefMenu(i).ButtonMenu = "IMS/DB->RDBMS" Then
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).ButtonMenu = DefMenu(i).ButtonMenu
      
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(0)
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(UBound(DefMenuAPP(UBound(DefMenuAPP)).Bottoni) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).Bottoni = DefMenu(i).Bottoni
      Exit For
    End If
  Next i
  
  
  'Cerca la voce "IMS/DB->VSAM"
  For i = 1 To UBound(DefMenu)
    If DefMenu(i).ButtonMenu = "IMS/DB->VSAM" Then
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).ButtonMenu = DefMenu(i).ButtonMenu
      
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(0)
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(UBound(DefMenuAPP(UBound(DefMenuAPP)).Bottoni) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).Bottoni = DefMenu(i).Bottoni
      Exit For
    End If
  Next i
  
  ' xyz
  For i = 1 To UBound(DefMenu)
    If DefMenu(i).ButtonMenu = "VSAM->RDBMS" Then
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).ButtonMenu = DefMenu(i).ButtonMenu
      
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(0)
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(UBound(DefMenuAPP(UBound(DefMenuAPP)).Bottoni) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).Bottoni = DefMenu(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce Migratore
  For i = 1 To UBound(DefMenu)
    If Trim(UCase(DefMenu(i).ButtonMenu)) = "MIGRATOR" Then
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).ButtonMenu = DefMenu(i).ButtonMenu
      
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(0)
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(UBound(DefMenuAPP(UBound(DefMenuAPP)).Bottoni) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).Bottoni = DefMenu(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce Tools
  For i = 1 To UBound(DefMenu)
    If Trim(UCase(DefMenu(i).ButtonMenu)) = "TOOLS" Then
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).ButtonMenu = DefMenu(i).ButtonMenu
      
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(0)
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(UBound(DefMenuAPP(UBound(DefMenuAPP)).Bottoni) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).Bottoni = DefMenu(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce IMS2MTP
  For i = 1 To UBound(DefMenu)
    If DefMenu(i).ButtonMenu = "IMS/DC->CICS" Then
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).ButtonMenu = DefMenu(i).ButtonMenu
      
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(0)
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(UBound(DefMenuAPP(UBound(DefMenuAPP)).Bottoni) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).Bottoni = DefMenu(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce DataAnalysis
  For i = 1 To UBound(DefMenu)
    If Trim(UCase(DefMenu(i).ButtonMenu)) = "DATA ANALYSIS" Then
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).ButtonMenu = DefMenu(i).ButtonMenu
      
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(0)
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(UBound(DefMenuAPP(UBound(DefMenuAPP)).Bottoni) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).Bottoni = DefMenu(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce Config
  For i = 1 To UBound(DefMenu)
    If Trim(UCase(DefMenu(i).ButtonMenu)) = "CONFIG" Then
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).ButtonMenu = DefMenu(i).ButtonMenu
      
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(0)
      ReDim Preserve DefMenuAPP(UBound(DefMenuAPP)).Bottoni(UBound(DefMenuAPP(UBound(DefMenuAPP)).Bottoni) + 1)
      DefMenuAPP(UBound(DefMenuAPP)).Bottoni = DefMenu(i).Bottoni
      Exit For
    End If
  Next i
  
  'Copia nuovamente L'array Riordinato in DefMenu
  ReDim DefMenu(0)
  ReDim DefMenu(0).Bottoni(0)
  
  'Cerca la voce Project
  For i = 1 To UBound(DefMenuAPP)
    If Trim(UCase(DefMenuAPP(i).ButtonMenu)) = "PROJECT" Then
      ReDim Preserve DefMenu(UBound(DefMenu) + 1)
      DefMenu(UBound(DefMenu)).ButtonMenu = DefMenuAPP(i).ButtonMenu
      
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(0)
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(UBound(DefMenu(UBound(DefMenu)).Bottoni) + 1)
      DefMenu(UBound(DefMenu)).Bottoni = DefMenuAPP(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce DataManager
  For i = 1 To UBound(DefMenuAPP)
     If Trim(UCase(DefMenuAPP(i).ButtonMenu)) = "DATA MANAGER" Then
       ReDim Preserve DefMenu(UBound(DefMenu) + 1)
       DefMenu(UBound(DefMenu)).ButtonMenu = DefMenuAPP(i).ButtonMenu
       
       ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(0)
       ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(UBound(DefMenu(UBound(DefMenu)).Bottoni) + 1)
       DefMenu(UBound(DefMenu)).Bottoni = DefMenuAPP(i).Bottoni
       Exit For
     End If
   Next i
  
  'Cerca la voce Dli2RDBMS
  For i = 1 To UBound(DefMenuAPP)
    If DefMenuAPP(i).ButtonMenu = "IMS/DB->RDBMS" Then
      ReDim Preserve DefMenu(UBound(DefMenu) + 1)
      DefMenu(UBound(DefMenu)).ButtonMenu = DefMenuAPP(i).ButtonMenu
      
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(0)
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(UBound(DefMenu(UBound(DefMenu)).Bottoni) + 1)
      DefMenu(UBound(DefMenu)).Bottoni = DefMenuAPP(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce "IMS/DB->VSAM"
  For i = 1 To UBound(DefMenuAPP)
    If DefMenuAPP(i).ButtonMenu = "IMS/DB->VSAM" Then
      ReDim Preserve DefMenu(UBound(DefMenu) + 1)
      DefMenu(UBound(DefMenu)).ButtonMenu = DefMenuAPP(i).ButtonMenu
      
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(0)
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(UBound(DefMenu(UBound(DefMenu)).Bottoni) + 1)
      DefMenu(UBound(DefMenu)).Bottoni = DefMenuAPP(i).Bottoni
      Exit For
    End If
  Next i
  
  'ALe xyz
  'Cerca la voce VSAM2RDBMS
  For i = 1 To UBound(DefMenuAPP)
    If DefMenuAPP(i).ButtonMenu = "VSAM->RDBMS" Then
      ReDim Preserve DefMenu(UBound(DefMenu) + 1)
      DefMenu(UBound(DefMenu)).ButtonMenu = DefMenuAPP(i).ButtonMenu
      
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(0)
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(UBound(DefMenu(UBound(DefMenu)).Bottoni) + 1)
      DefMenu(UBound(DefMenu)).Bottoni = DefMenuAPP(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce IMS2MTP
  For i = 1 To UBound(DefMenuAPP)
    If DefMenuAPP(i).ButtonMenu = "IMS/DC->CICS" Then
      ReDim Preserve DefMenu(UBound(DefMenu) + 1)
      DefMenu(UBound(DefMenu)).ButtonMenu = DefMenuAPP(i).ButtonMenu
      
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(0)
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(UBound(DefMenu(UBound(DefMenu)).Bottoni) + 1)
      DefMenu(UBound(DefMenu)).Bottoni = DefMenuAPP(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce Migratore
  For i = 1 To UBound(DefMenuAPP)
    If Trim(UCase(DefMenuAPP(i).ButtonMenu)) = "MIGRATOR" Then
      ReDim Preserve DefMenu(UBound(DefMenu) + 1)
      DefMenu(UBound(DefMenu)).ButtonMenu = DefMenuAPP(i).ButtonMenu
      
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(0)
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(UBound(DefMenu(UBound(DefMenu)).Bottoni) + 1)
      DefMenu(UBound(DefMenu)).Bottoni = DefMenuAPP(i).Bottoni
      Exit For
    End If
  Next i
   
   'Cerca la voce DataAnalysis
  For i = 1 To UBound(DefMenuAPP)
    If Trim(UCase(DefMenuAPP(i).ButtonMenu)) = "DATA ANALYSIS" Then
      ReDim Preserve DefMenu(UBound(DefMenu) + 1)
      DefMenu(UBound(DefMenu)).ButtonMenu = DefMenuAPP(i).ButtonMenu
      
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(0)
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(UBound(DefMenu(UBound(DefMenu)).Bottoni) + 1)
      DefMenu(UBound(DefMenu)).Bottoni = DefMenuAPP(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce Utility
  For i = 1 To UBound(DefMenuAPP)
    If Trim(UCase(DefMenuAPP(i).ButtonMenu)) = "TOOLS" Then
      ReDim Preserve DefMenu(UBound(DefMenu) + 1)
      DefMenu(UBound(DefMenu)).ButtonMenu = DefMenuAPP(i).ButtonMenu
      
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(0)
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(UBound(DefMenu(UBound(DefMenu)).Bottoni) + 1)
      DefMenu(UBound(DefMenu)).Bottoni = DefMenuAPP(i).Bottoni
      Exit For
    End If
  Next i
  
  'Cerca la voce Config
  For i = 1 To UBound(DefMenuAPP)
    If Trim(UCase(DefMenuAPP(i).ButtonMenu)) = "CONFIG" Then
      ReDim Preserve DefMenu(UBound(DefMenu) + 1)
      DefMenu(UBound(DefMenu)).ButtonMenu = DefMenuAPP(i).ButtonMenu
      
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(0)
      ReDim Preserve DefMenu(UBound(DefMenu)).Bottoni(UBound(DefMenu(UBound(DefMenu)).Bottoni) + 1)
      DefMenu(UBound(DefMenu)).Bottoni = DefMenuAPP(i).Bottoni
      Exit For
    End If
  Next i
End Sub

Public Function Controlla_Autorizzazioni(wTipo As String, Nome As String) As Boolean
  Select Case Trim(UCase(wTipo))
    Case "PGM", "CBL", "SRC", "CTL", "COB" 'da implementare con nuovi tipi
      Controlla_Autorizzazioni = DllBase.ControlSource_StepByStep_PGM(Trim(UCase(wTipo)), Nome)
    Case "DB", "DBD", "DAT"
      Controlla_Autorizzazioni = DllBase.ControlSource_StepByStep_DBD(Trim(UCase(wTipo)), Nome)
    Case Else
      Controlla_Autorizzazioni = True
  End Select
End Function

Public Function Isola_Parametro(Stringa As String, Sep As String) As String
  Dim Start As Long, Stopp As Long
  
  Start = InStr(1, Stringa, Sep)
  Stopp = InStr(Start + 1, Stringa, Sep)
  Isola_Parametro = Mid(Stringa, Start + 1, Stopp - Start - 1)
End Function

Public Function Controlla_Data(DtInizio As Date, DtFine As Date) As Boolean
  Dim DataNow As Date
  Dim wDate As Date
  Dim rDate As Date
  Dim adesso As Date
  
  DataNow = Date
  wDate = Now
  
  'La data deve essere contenuta nell'intervallo
  If DtFine < DataNow Then
    Controlla_Data = False
    Exit Function
  Else
    Controlla_Data = True
  End If
  
  If DtInizio > DataNow Then
    Controlla_Data = False
    Exit Function
  End If
  
  WaitTime 1
  
  'Parte il controllo tramite CTRL
  rDate = getCtrl
  adesso = Now
  
  If rDate < adesso Then
    Controlla_Data = True
  Else
    Controlla_Data = False
    Exit Function
  End If
  
  If DtInizio < rDate Then
    Controlla_Data = True
  Else
    Controlla_Data = False
    Exit Function
  End If
  
  If DtFine < rDate Then
    Controlla_Data = False
    Exit Function
  Else
    Controlla_Data = True
  End If
End Function

Public Sub Carica_Proprieta_Oggetto(id As Long)
  Dim r As Recordset
  
  Set r = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & id)
  If r.RecordCount Then
    '� un solo record
    OggSel.Area_App = r!Area_Appartenenza
    OggSel.Batch = TN(r!Batch, vbBoolean)
    OggSel.Cics = TN(r!Cics, vbBoolean)
    OggSel.directory_input = Crea_Directory_Progetto(r!directory_input & "", ProjectPath)
    OggSel.Directory_Output = Crea_Directory_Progetto(r!Directory_Output & "", ProjectPath)
    OggSel.DLI = TN(r!DLI, vbBoolean)
    OggSel.DtImport = r!DtImport & ""
    OggSel.DtIncaps = r!DtIncapsulamento & ""
    OggSel.DtParsing = r!DtParsing & ""
    OggSel.IdOggetto = r!IdOggetto
    OggSel.Livello1 = r!Livello1 & ""
    OggSel.Livello2 = r!Livello2 & ""
    OggSel.Mapping = TN(r!Mapping, vbBoolean)
    OggSel.Nome = Trim(r!Nome) & ""
    OggSel.Estensione = Trim(r!Estensione) & ""
    
    If Not IsNull(r!NumRighe) Then
      OggSel.NumRighe = r!NumRighe
    Else
      OggSel.NumRighe = 0
    End If
    
    If Not IsNull(r!parsingLevel) Then
      OggSel.ParsingLev = r!parsingLevel
    Else
      OggSel.ParsingLev = 0
    End If
    
    OggSel.sql = TN(r!WithSQL, vbBoolean)
    OggSel.Ims = TN(r!Ims, vbBoolean)
    OggSel.Tipo = r!Tipo & ""
    OggSel.TipoDBD = r!Tipo_DBD & ""
    OggSel.VSam = TN(r!VSam, vbBoolean)
  End If
  r.Close
End Sub

Public Sub Carica_Relazioni_Oggetto(id As Long, NomeOgg As String, Arr() As Rel)
  Dim r As Recordset, rApp As Recordset
  Dim Dbd As String
  
  ReDim Arr(0)
  Set r = DLLFunzioni.Open_Recordset("Select * From PSRel_Obj Where IdOggettoC = " & id)
  While Not r.EOF
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)).idOggettoC = r!idOggettoC
    Arr(UBound(Arr)).IdOggettoR = r!IdOggettoR
    Arr(UBound(Arr)).Relazione = r!Relazione
    Arr(UBound(Arr)).NomeOggRel = r!nomecomponente
    Arr(UBound(Arr)).Utilizzo = r!Utilizzo
    Arr(UBound(Arr)).ConosciutoLiv = 1
    Arr(UBound(Arr)).TipoOgg = r!Relazione
    Arr(UBound(Arr)).Tipo = Restituisci_TipoOgg_Da_IdOggetto(r!IdOggettoR)
    r.MoveNext
  Wend
  r.Close
  
  'Carica gli oggetti Sconosciuti
  Set r = DLLFunzioni.Open_Recordset("Select * From PSRel_ObjUnk Where IdOggettoC = " & id)
  While Not r.EOF
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)).idOggettoC = r!idOggettoC
    Arr(UBound(Arr)).IdOggettoR = 0
    Arr(UBound(Arr)).Relazione = r!Relazione
    Arr(UBound(Arr)).NomeOggRel = r!nomecomponente
    Arr(UBound(Arr)).Utilizzo = r!Utilizzo
    Arr(UBound(Arr)).ConosciutoLiv = 0
    Arr(UBound(Arr)).TipoOgg = r!Relazione
    Arr(UBound(Arr)).Tipo = ""
    r.MoveNext
  Wend
  r.Close
  
  'Carica gli oggetti Sconosciuti
  If id = 0 Then
    Set r = DLLFunzioni.Open_Recordset("Select * From PSRel_OUnkUnk Where NomeOggettoC = '" & NomeOgg & "'")
    While Not r.EOF
      ReDim Preserve Arr(UBound(Arr) + 1)
      Arr(UBound(Arr)).idOggettoC = 0
      Arr(UBound(Arr)).IdOggettoR = 0
      Arr(UBound(Arr)).Relazione = r!Relazione
      Arr(UBound(Arr)).NomeOggRel = r!nomecomponente
      Arr(UBound(Arr)).Utilizzo = r!Utilizzo
      Arr(UBound(Arr)).ConosciutoLiv = 0
      Arr(UBound(Arr)).TipoOgg = r!Relazione
      Arr(UBound(Arr)).Tipo = ""
      r.MoveNext
    Wend
    r.Close
  End If
  
  'Carica gli DATASET
  Set r = DLLFunzioni.Open_Recordset("Select * From PSCom_Obj Where IdOggettoC = " & id & " And " & _
                                     "(Utilizzo = 'DATASET' or utilizzo = 'DDLDATABAS' or utilizzo = 'DDLTABLE')")
  While Not r.EOF
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)).idOggettoC = id
    Arr(UBound(Arr)).IdOggettoR = 0
    Arr(UBound(Arr)).Relazione = r!Relazione
    Arr(UBound(Arr)).NomeOggRel = r!nomecomponente
    Arr(UBound(Arr)).Utilizzo = r!Utilizzo
    Arr(UBound(Arr)).ConosciutoLiv = 1
    Arr(UBound(Arr)).TipoOgg = r!Relazione
    Arr(UBound(Arr)).Tipo = Restituisci_TipoOgg_Da_IdOggetto(id)
    r.MoveNext
  Wend
  r.Close
  
  'Carica gli TRANSID
  Set r = DLLFunzioni.Open_Recordset("Select * From PSCom_Obj Where IdOggettoC = " & id & " And (Utilizzo = 'RETURN' or Utilizzo = 'START')")
  While Not r.EOF
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)).idOggettoC = id
    Arr(UBound(Arr)).IdOggettoR = 0
    Arr(UBound(Arr)).Relazione = r!Relazione
    Arr(UBound(Arr)).NomeOggRel = r!nomecomponente
    Arr(UBound(Arr)).Utilizzo = r!Utilizzo
    Arr(UBound(Arr)).ConosciutoLiv = 1
    Arr(UBound(Arr)).TipoOgg = r!Relazione
    Arr(UBound(Arr)).Tipo = Restituisci_TipoOgg_Da_IdOggetto(id)
    r.MoveNext
  Wend
  r.Close
    
  'Carica Le Strutture
  Set r = DLLFunzioni.Open_Recordset("Select * From PsData_Area Where IdOggetto = " & id & " And Livello = 1")
  While Not r.EOF
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)).idOggettoC = id
    Arr(UBound(Arr)).IdOggettoR = 0
    Arr(UBound(Arr)).Relazione = "STR" 'Strutture
    Arr(UBound(Arr)).NomeOggRel = r!Nome
    Arr(UBound(Arr)).Utilizzo = "STR"
    Arr(UBound(Arr)).ConosciutoLiv = 1
    Arr(UBound(Arr)).TipoOgg = "STR"
    Arr(UBound(Arr)).Tipo = ""
    r.MoveNext
  Wend
  r.Close
  
  'Carica Le istruzioni
  Set r = DLLFunzioni.Open_Recordset("Select DISTINCT Istruzione,IdDBD,numpcb From PsDli_Istruzioni Where IdOggetto = " & id)
  While Not r.EOF
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)).idOggettoC = id
    Arr(UBound(Arr)).IdOggettoR = Val(r!NumPCB)
    Arr(UBound(Arr)).Relazione = "IST"
    Arr(UBound(Arr)).NomeOggRel = Trim(r!Istruzione)
    Arr(UBound(Arr)).Utilizzo = Trim(r!IdDbd)
    Arr(UBound(Arr)).ConosciutoLiv = 1
    Arr(UBound(Arr)).TipoOgg = "IST"
    Arr(UBound(Arr)).Tipo = ""
    r.MoveNext
  Wend
  r.Close
  
  'Carica il MapSet
  Set r = DLLFunzioni.Open_Recordset("Select * From PSRel_Obj Where IdOggettoC = " & id & " And Relazione = 'MAP'")
  If r.RecordCount Then
    r.MoveFirst
    
    Set rApp = DLLFunzioni.Open_Recordset("Select * From PSCom_Obj Where IdOggettoC = " & r!IdOggettoR & " And Utilizzo = 'MAPSET'")
    If rApp.RecordCount Then
      rApp.MoveFirst
      ReDim Preserve Arr(UBound(Arr) + 1)
      Arr(UBound(Arr)).idOggettoC = id
      Arr(UBound(Arr)).IdOggettoR = id
      Arr(UBound(Arr)).Relazione = "BMS"
      Arr(UBound(Arr)).NomeOggRel = rApp!nomecomponente
      Arr(UBound(Arr)).Utilizzo = "BMS"
      Arr(UBound(Arr)).ConosciutoLiv = 1
      Arr(UBound(Arr)).TipoOgg = "BMS"
      Arr(UBound(Arr)).Tipo = Restituisci_TipoOgg_Da_IdOggetto(id)
    End If
    rApp.Close
  End If
  r.Close
  
  'Carica Le Mappe
  Set r = DLLFunzioni.Open_Recordset("Select * From PSCom_Obj Where IdOggettoC = " & id & " And Relazione = 'MAP' And Utilizzo <> 'MAPSET'")
  While Not r.EOF
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)).idOggettoC = id
    Arr(UBound(Arr)).IdOggettoR = 0
    Arr(UBound(Arr)).Relazione = "MAP"
    Arr(UBound(Arr)).NomeOggRel = r!nomecomponente
    Arr(UBound(Arr)).Utilizzo = r!Utilizzo
    Arr(UBound(Arr)).ConosciutoLiv = 1
    Arr(UBound(Arr)).TipoOgg = "MAP"
    Arr(UBound(Arr)).Tipo = Restituisci_TipoOgg_Da_IdOggetto(id)
    r.MoveNext
  Wend
  r.Close
  
  'Carica I Database
  Set r = DLLFunzioni.Open_Recordset("Select Distinct IdDBD From PSDli_Istruzioni Where IdOggetto = " & id)
  While Not r.EOF
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)).idOggettoC = id
    Arr(UBound(Arr)).IdOggettoR = 0
    Arr(UBound(Arr)).Relazione = "DBD"
    
    If r!IdDbd <> 0 Then
      Dbd = Restituisci_NomeOgg_Da_IdOggetto(r!IdDbd)
      If Trim(Dbd) <> "" Then
        Arr(UBound(Arr)).NomeOggRel = Mid(Dbd, 1, InStr(1, Dbd, ".") - 1)
        Arr(UBound(Arr)).Utilizzo = "DBD"
        Arr(UBound(Arr)).ConosciutoLiv = 1
        Arr(UBound(Arr)).TipoOgg = "DBD"
        Arr(UBound(Arr)).Tipo = Restituisci_TipoOgg_Da_IdOggetto(id)
      End If
    End If
    r.MoveNext
  Wend
  r.Close
End Sub
'''''''''''''''''''''''''''''''''''''''''''''
' SQ 10-02-06
' + Reinizializzazione segnalazioni
'''''''''''''''''''''''''''''''''''''''''''''
'Public Sub Cambia_Type_Oggetti_Selezionati(trw As TreeView, lsw As ListView, MabseF_ClassObj.Tipo As String, MabseF_ClassObj.Level As String)
Public Sub Cambia_Type_Oggetti_Selezionati(trw As TreeView, lsw As ListView)
  Dim i As Long, w As Long, IdOggetto As Long
  Dim r As Recordset, rSel As Recordset
  Dim OldTipo As String, OldArea As String, OldLevel As String
  Dim nCol As Collection
  Dim wExists As Boolean, wExistsArea As Boolean, wExistsDir As Boolean
   
  For i = 1 To lsw.ListItems.count
    If lsw.ListItems(i).Selected Then
      IdOggetto = lsw.ListItems(i).Text
      'SQ 10-02-06
      DLLFunzioni.FnConnection.Execute ("Delete * From Bs_Segnalazioni where idOggetto=" & IdOggetto)
         
      Set nCol = New Collection
      'Va in update sul record
      Set r = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & IdOggetto)
      If r.RecordCount Then
        'Prende il vecchio tipo
        'SQ 26-11-06
        'wArea = Restituisci_Area_Appartenenza_Dal_Tipo(MabseF_ClassObj.Tipo)
        'Utilizzato direttamente la nuova "MabseF_ClassObj.Area"
        OldTipo = Trim(r!Tipo)
        OldArea = Trim(r!Area_Appartenenza)
        OldLevel = Trim(r!Livello1 & "")
        r!Area_Appartenenza = MabseF_ClassObj.Area
        r!Tipo = Trim(MabseF_ClassObj.Tipo)
        r!Livello1 = Trim(MabseF_ClassObj.Level)
        '''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 10-02-06
        ' DEVE REINIZIALIZZARE! (segnalazioni, ecc...)
        '''''''''''''''''''''''''''''''''''''''''''''''
        r!errLevel = Null
        r!DtParsing = Null
        r!parsingLevel = 0
        r.Update
         
        'Crea la collection con i parametri della funzione
        Set nCol = New Collection
        nCol.Add IdOggetto
        nCol.Add OldTipo
        nCol.Add MabseF_ClassObj.Tipo
        nCol.Add MabseF_ClassObj.Area
        nCol.Add MabseF_ClassObj.Level
          
        'Richiama Routine Parser
        Set DllParser.PsCollectionParam = nCol

        DLLFunzioni.FnActiveWindowsBool = False
        Attiva_Funzione "PARSER", nCol, "CHANGE_TYPE_PARSER", , True, True
        DLLFunzioni.FnActiveWindowsBool = True
          
        wExists = False
        wExistsArea = False
        wExistsDir = False
        
        'Va in update sulla treeview
        For w = 1 To trw.Nodes.count
          If trw.Nodes(w).Tag = "L0" Then
            If Trim(trw.Nodes(w).Text) = Trim(MabseF_ClassObj.Area) Then
              wExistsArea = True
            End If
          End If
           
          If trw.Nodes(w).Tag = "L1" Then
            If Trim(trw.Nodes(w).Text) = Trim(MabseF_ClassObj.Level) Then
              wExists = True
            End If
          End If
             
          If trw.Nodes(w).Tag = "L2" Then
            If Trim(trw.Nodes(w).Text) = Trim(DLLFunzioni.Crea_Directory_Progetto(r!directory_input, DLLFunzioni.FnPathDef)) Then
              If Trim(UCase(trw.Nodes(w).Parent.Text)) = Trim(UCase(MabseF_ClassObj.Level)) Then
                wExistsDir = True
              End If
            End If
          End If
        Next w
        If Not wExistsArea Then
          trw.Nodes.Add "MB", tvwChild, Trim(MabseF_ClassObj.Area), Trim(MabseF_ClassObj.Area), 5
          trw.Nodes(trw.Nodes.count).ForeColor = vbRed
          trw.Nodes(trw.Nodes.count).Bold = True
          trw.Nodes(trw.Nodes.count).Tag = "L0"
        End If
        If Not wExists Then
          'Aggiunge il nuovo nodo
          trw.Nodes.Add Trim(MabseF_ClassObj.Area), tvwChild, Trim(MabseF_ClassObj.Area) & "#" & Trim(MabseF_ClassObj.Level), Trim(MabseF_ClassObj.Level), 4
          trw.Nodes(trw.Nodes.count).ForeColor = vbBlue
          trw.Nodes(trw.Nodes.count).Bold = True
          trw.Nodes(trw.Nodes.count).Tag = "L1"
        End If
        If Not wExistsDir Then
          trw.Nodes.Add Trim(MabseF_ClassObj.Area) & "#" & Trim(MabseF_ClassObj.Level), tvwChild, Trim(MabseF_ClassObj.Area) & "#" & Trim(MabseF_ClassObj.Level) & "@" & r!directory_input, Crea_Directory_Progetto(r!directory_input, ProjectPath), 2
          trw.Nodes(trw.Nodes.count).Tag = "L2"
        End If
        If wExistsDir And Not wExists Then
          trw.Nodes.Add Trim(MabseF_ClassObj.Area) & "#" & Trim(MabseF_ClassObj.Level), tvwChild, Trim(MabseF_ClassObj.Area) & "#" & Trim(MabseF_ClassObj.Level) & "@" & r!directory_input, Crea_Directory_Progetto(r!directory_input, ProjectPath), 2
          trw.Nodes(trw.Nodes.count).Tag = "L2"
        End If
        'Controlla se ci sono oggetti sotto il ramo (se no --> lo cancella)
        Set rSel = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where " & _
                                              "Tipo = '" & OldTipo & "' And Area_Appartenenza = '" & OldArea & "'" & _
                                              " And Livello1 = '" & OldLevel & "' And Livello2 = '" & r!directory_input & "'")
        If rSel.RecordCount = 0 Then
          For w = 1 To trw.Nodes.count
            If trw.Nodes(w).Key = Trim(OldArea) & "#" & Trim(OldLevel) & "@" & Trim(r!directory_input) Then
              trw.Nodes.Remove (w)
              Exit For
            End If
          Next w
        End If
        rSel.Close
    
        'Controlla se anche sotto al padre non ci sono pi� oggetti
        Set rSel = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where " & _
                                              "Tipo = '" & OldTipo & "' And Area_Appartenenza = '" & OldArea & "'" & _
                                              " And Livello1 = '" & OldLevel & "'")
        If rSel.RecordCount = 0 Then
          For w = 1 To trw.Nodes.count
            If trw.Nodes(w).Key = Trim(OldArea) & "#" & Trim(OldLevel) Then
              trw.Nodes.Remove (w)
              Exit For
            End If
          Next w
        End If
        rSel.Close
        
        Set rSel = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where Area_Appartenenza = '" & OldArea & "'")
        If rSel.RecordCount = 0 Then
          For w = 1 To trw.Nodes.count
            If trw.Nodes(w).Key = Trim(OldArea) Then
              trw.Nodes.Remove (w)
              Exit For
            End If
          Next w
        End If
        rSel.Close
      End If
      r.Close
    End If
  Next i
End Sub

Public Sub Crea_File_Database(NomeDB As String, wTipoProvider As e_Provider, Optional wUser, Optional wPsw, Optional wProvider, _
                              Optional wIP)
  Dim wStr As String
   
  'Crea il file Ini sotto la directory scelta dei sorgenti
  If wTipoProvider <> Access Then
    wStr = "[DBPROJECT]" & vbCrLf
    wStr = wStr & "DB_NAME=" & Replace(NomeDB, ".mti", "") & vbCrLf
    wStr = wStr & "DB_USER=" & wUser & vbCrLf
    wStr = wStr & "DB_PASSWORD=" & wPsw & vbCrLf
    wStr = wStr & "DB_PROVIDER=" & wProvider & vbCrLf
    wStr = wStr & "DB_TYPE=" & wTipoProvider & vbCrLf
    wStr = wStr & "SERVER_IP=" & wIP & vbCrLf
    wStr = wStr & "DB_PATH=" & DLLFunzioni.Crea_Directory_Parametrica(ProjectPath) & vbCrLf
  Else
    wStr = "[DBPROJECT]" & vbCrLf
    wStr = wStr & "DB_NAME=" & NomeDB & vbCrLf
    wStr = wStr & "DB_USER=" & vbCrLf
    wStr = wStr & "DB_PASSWORD=" & vbCrLf
    wStr = wStr & "DB_PROVIDER=" & wProvider & vbCrLf
    wStr = wStr & "DB_TYPE=" & wTipoProvider & vbCrLf
    wStr = wStr & "SERVER_IP=" & wIP & vbCrLf
    wStr = wStr & "DB_PATH=" & DLLFunzioni.Crea_Directory_Parametrica(ProjectPath) & vbCrLf
  End If
   
  'Scrive il file ini
  'MF 09/08/07 Sostituisco Replace(NomeDB, ".mty", "") & ".mti" con ProjectName che gi� contiene il nome dle progetto
  Open ProjectPath & "\" & ProjectName For Output As #1
  Print #1, wStr
  Close #1
End Sub

Public Sub Load_File_Mti(Percorso As String)
  Dim wBuff As String, wApp As String
  Dim wRet As Long, wStr As String
  Dim pos As Long, pos2 As Long
  
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROJECT", "DB_NAME", "", wBuff, 100, Percorso)
  DatabaseName = Mid(wBuff, 1, wRet)
  
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROJECT", "DB_USER", "", wBuff, 100, Percorso)
  GbCurUserName = Mid(wBuff, 1, wRet)
  
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROJECT", "DB_PASSWORD", "", wBuff, 100, Percorso)
  GbCurPassword = Mid(wBuff, 1, wRet)
  
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROJECT", "DB_PROVIDER", "", wBuff, 100, Percorso)
  GbCurProvider = Mid(wBuff, 1, wRet)
  
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROJECT", "DB_TYPE", "", wBuff, 100, Percorso)
  GbCurTipoDB = CLng(Mid(wBuff, 1, wRet))
  
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROJECT", "SERVER_IP", "", wBuff, 100, Percorso)
  GbCurServerIp = Mid(wBuff, 1, wRet)
  
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROJECT", "DB_PATH", "", wBuff, 100, Percorso)
  GbCurDBPath = Mid(wBuff, 1, wRet)
  
  ' patch aggiunta il 18/01/2006 in caso di errore cartella nel mti
  pos = InStrRev(Percorso, "\")
  pos2 = InStrRev(Percorso, "\", pos - 1)
  GbCurDBPath = Mid(Percorso, pos2 + 1, pos - pos2 - 1)
  GbCurDBPath = "$\" & GbCurDBPath
  '***********************
  
  'Ricrea il path dinamico
  If Mid$(GbCurDBPath, 1, 1) = "$" Then
    wApp = Replace(GbCurDBPath, "$", "")
    ' Ma 15/04/2008
    wApp = IIf(Right(wApp, 1) = "\", wApp, wApp & "\")
    wStr = Mid(Percorso, 1, InStr(1, Percorso, wApp) - 1)

    ProjectPath = DLLFunzioni.Crea_Directory_Progetto(GbCurDBPath, wStr)
  End If
End Sub

Public Sub Create_DirectoryOut_Progetto(wPercorso As String)
  Dim i As Long
  Dim s() As String
  Dim wApp As String
 
  s = Split(wPercorso, "\")

  For i = 0 To UBound(s)
    If i = 0 Then
      wApp = wApp & s(i)
    Else
      wApp = wApp & "\" & s(i)
    End If
    If Dir(wApp, vbDirectory) = "" Then
      DLLFunzioni.MkDir_API wApp
    End If
  Next i
 
  'DIRECTORY OUTPUT-PRJ
  If Dir(wPercorso & "\Output-Prj", vbDirectory) = "" Then
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Compiled"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\dli"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\DB2"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\DB2\BtRout"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\DB2\CxRout"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\DB2\Cbl"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\DB2\Cpy"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\DB2\PLI"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\DB2\Include"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\DB2\Jcl"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\DB2\Sql"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\BtRout"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\CxRout"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\Cbl"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\Cpy"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\PLI"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\Include"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\Jcl"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\Sql"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\Sql\FK"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\Sql\IX"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\ORACLE\Sql\PK"
    'SQ 3-11-06 - Migrazioni linguaggi
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Languages\"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Languages\Asm2Cobol"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Languages\Ezt2Cobol"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Languages\Pli2Cobol"
    'Mauro 10-11-06 - Migrazioni EBCDIC to ASCII
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Data\"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Data\EBCDICtoASCII"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Data\ASCIItoEBCDIC"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Data\EBCDICtoASCII\Structure"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Data\EBCDICtoASCII\RedFile"
    'Giugy 05/05/08 - Aggiunta cartella /bin per script
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Data\EBCDICtoASCII\bin"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Data\ASCIItoEBCDIC\Structure"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Data\ASCIItoEBCDIC\RedFile"
    DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Data\ASCIItoEBCDIC\bin"
  Else
    If Dir(wPercorso & "\Output-Prj\dli", vbDirectory) = "" Then DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\dli"
    If Dir(wPercorso & "\Output-Prj\Compiled", vbDirectory) = "" Then DLLFunzioni.MkDir_API wPercorso & "\Output-Prj\Compiled"
  End If
 
  'DIRECTORY DOCUMENTS
  If Dir(wPercorso & "\Documents", vbDirectory) = "" Then
    'Crea le directory
    DLLFunzioni.MkDir_API wPercorso & "\Documents"
    DLLFunzioni.MkDir_API wPercorso & "\Documents\Excel"
    DLLFunzioni.MkDir_API wPercorso & "\Documents\HTML"
    DLLFunzioni.MkDir_API wPercorso & "\Documents\Log"
    DLLFunzioni.MkDir_API wPercorso & "\Documents\Txt"
  Else
    'Crea le directory
    If Dir(wPercorso & "\Documents\Excel", vbDirectory) = "" Then DLLFunzioni.MkDir_API wPercorso & "\Documents\Excel"
    If Dir(wPercorso & "\Documents\HTML", vbDirectory) = "" Then DLLFunzioni.MkDir_API wPercorso & "\Documents\HTML"
    If Dir(wPercorso & "\Documents\Log", vbDirectory) = "" Then DLLFunzioni.MkDir_API wPercorso & "\Documents\Log"
    If Dir(wPercorso & "\Documents\Txt", vbDirectory) = "" Then DLLFunzioni.MkDir_API wPercorso & "\Documents\Txt"
  End If
End Sub

Public Sub Write_Info_Nuovo_Progetto(cDbName As String)
  Dim r As Recordset
   
  'inserisce la data di creazione del DB Nel Database
  Set r = DLLFunzioni.Open_Recordset("Select * From BS_InfoPrj")
  If r.RecordCount = 0 Then
    r.AddNew
    r!Date_Creation = Format(Date, "dddd, d mmm yyyy")
    r!Project_Name = ProjectName
    r!localize_Directory = Crea_Directory_Parametrica(cDbName)
    r!Object_Number = 0
    r.Update
  Else
    r!Date_Creation = Format(Date, "dddd, d mmm yyyy")
    r!Project_Name = ProjectName
    r!localize_Directory = Crea_Directory_Parametrica(cDbName)
    r!Object_Number = 0
    r.Update
  End If
End Sub
   
Public Function TN(cField As Field, Optional nType, Optional nTrim) As Variant
  If IsNull(cField) Then
    If Not IsMissing(nType) Then
      Select Case nType
        Case vbBoolean
          TN = False
        Case vbLong
          TN = -1
        Case vbString
          TN = ""
        Case vbDate
          TN = Now
        Case Else
          TN = ""
      End Select
    Else
      TN = ""
    End If
  Else
    If IsMissing(nTrim) Then
      TN = cField.Value
    Else
      TN = Trim(cField.Value)
    End If
  End If
End Function

Public Function GetMaxIdOggettoAfterAddNew(wRec As Recordset) As Long
  Dim wIdObjMax As Long
  Dim rs As Recordset
   
  Select Case GbCurTipoDB
    Case SQLSERVER
      wRec.Close
      'Recupera l'id oggetto
      Set rs = DLLFunzioni.Open_Recordset("Select MAX(IdOggetto) FROM BS_Oggetti")
      wIdObjMax = rs.fields(0).Value
    Case Access
      wIdObjMax = wRec!IdOggetto
    Case MYSQL
    
    Case ORACLE
    
    Case DB2
    
    Case Else
      wRec.Close
      'Recupera l'id oggetto
      Set rs = DLLFunzioni.Open_Recordset("Select MAX(IdOggetto) FROM BS_Oggetti")
      wIdObjMax = wRec.fields(0).Value
  End Select
  
  GetMaxIdOggettoAfterAddNew = wIdObjMax
End Function

Public Function GetUser()
  Dim sBuffer As String, lSize As Long
    
  sBuffer = Space$(255)
  lSize = Len(sBuffer)
  GetUserName sBuffer, lSize
  If lSize Then
    GetUser = Left$(sBuffer, lSize)
  Else
    GetUser = vbNullString
  End If
End Function

'''''Public Sub template_copy_into_prj()
'''''    'Copia i tempate e le routine sotto il path del progetto
'''''    Dim wFile As String
'''''
'''''    Screen.MousePointer = vbHourglass
'''''
'''''    'Crea le directory se non esistono
'''''    If Dir(DLLFunzioni.FnPathPrj & "\Input-Prj", vbDirectory) = "" Then MkDir DLLFunzioni.FnPathPrj & "\Input-Prj"
'''''    If Dir(DLLFunzioni.FnPathPrj & "\Input-Prj\routbs", vbDirectory) = "" Then MkDir DLLFunzioni.FnPathPrj & "\Input-Prj\routbs"
'''''    If Dir(DLLFunzioni.FnPathPrj & "\Input-Prj\cpy_base", vbDirectory) = "" Then MkDir DLLFunzioni.FnPathPrj & "\Input-Prj\cpy_base"
'''''    If Dir(DLLFunzioni.FnPathPrj & "\Input-Prj\redefines", vbDirectory) = "" Then MkDir DLLFunzioni.FnPathPrj & "\Input-Prj\redefines"
'''''
'''''    'copia il contenuto di routbs
'''''    wFile = Dir(DLLFunzioni.FnPathPrd & "\" & SYSTEM_DIR & "\routbs\*.*", vbNormal)
'''''
'''''    While wFile <> ""
'''''        FileCopy DLLFunzioni.FnPathPrd & "\" & SYSTEM_DIR & "\routbs\" & wFile, DLLFunzioni.FnPathPrj & "\Input-Prj\routbs\" & wFile
'''''
'''''        wFile = Dir
'''''    Wend
'''''
'''''    'copia il contenuto di cpy_base
'''''    wFile = Dir(DLLFunzioni.FnPathPrd & "\" & SYSTEM_DIR & "\cpy_base\*.*", vbNormal)
'''''
'''''    While wFile <> ""
'''''        FileCopy DLLFunzioni.FnPathPrd & "\" & SYSTEM_DIR & "\cpy_base\" & wFile, DLLFunzioni.FnPathPrj & "\Input-Prj\cpy_base\" & wFile
'''''
'''''        wFile = Dir
'''''    Wend
'''''
'''''    Screen.MousePointer = vbDefault
'''''End Sub

Public Function Verifica_Connessione_ADO() As Boolean
  Verifica_Connessione_ADO = ADOCnt.State = adStateOpen
End Function

Public Function Elimina_Oggetti_Selezionati(Lista As ListView, Tot As TextBox) As Long
  Dim i As Long
  Dim r As Recordset
  
  Dim AreaApp As String
  Dim exFile As String
  Dim cParam As Collection
  
  Dim Scelta As Long
  
  Scelta = DLLFunzioni.Show_MsgBoxError("MP02Q")
  
  Elimina_Oggetti_Selezionati = Scelta
  
  If Scelta = vbYes Then
    For i = Lista.ListItems.count To 1 Step -1
      Clessidra True
      If Lista.ListItems(i).Selected Then
        DllParser.PsIdOggetto_Add = Val(Lista.ListItems(i))
        
        Set cParam = New Collection
        cParam.Add Val(Lista.ListItems(i))
        
        DLLFunzioni.FnActiveWindowsBool = False
        
        DllParser.AttivaFunzione "DEL_TO_PROJECT", cParam
        DLLFunzioni.FnActiveWindowsBool = True
        
        'cancella dalla tabella oggetti
        Set r = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Val(Lista.ListItems(i)))
        If r.RecordCount Then
          'Aggiorna i contatori User
          DllBase.Aggiorna_Contatori_Dopo_Cancel r!Nome, r!Tipo, CurPgm, CurDBD
          'Elimina l'oggetto anche fisicamente 'Nov2004
          DLLFunzioni.KillFile (DLLFunzioni.Restituisci_Directory_Da_IdOggetto(r!IdOggetto) & "\" & DLLFunzioni.Restituisci_NomeOgg_Da_IdOggetto(r!IdOggetto))
          r.Delete
        End If
        r.Close
        
        'cancella dalla tabella Segnalazioni
        Set r = DLLFunzioni.Open_Recordset("Select * From BS_Segnalazioni Where IdOggetto = " & Val(Lista.ListItems(i)))
        If r.RecordCount Then
          r.Delete
        End If
        r.Close
        
        'cancella dal video
        Lista.ListItems.Remove (i)
      End If
      
      Clessidra False
    Next i
  End If
    
  'conta gli oggetti
  Tot = Lista.ListItems.count
End Function

Public Function Restituisci_Directory_Da_IdOggetto(id As Long, Optional TipoDir As String) As String
  'restituisce la directory dall'id oggetto secondo il tipo INPUT,OUTPUT
  Dim r As Recordset
  
  Set r = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & id)
  If r.RecordCount Then
    If Trim(TipoDir) <> "" Then
      Select Case UCase(TipoDir)
        Case "INPUT"
          Restituisci_Directory_Da_IdOggetto = Crea_Directory_Progetto(r!directory_input, ProjectPath)
          r.Close
          Exit Function
        Case "OUTPUT"
          Restituisci_Directory_Da_IdOggetto = Crea_Directory_Progetto(Trim(r!Directory_Output & " "), ProjectPath)
          r.Close
          Exit Function
      End Select
    Else
      'restituisce la dir input di default
      Restituisci_Directory_Da_IdOggetto = Crea_Directory_Progetto(r!directory_input, ProjectPath)
      r.Close
      Exit Function
    End If
  End If
  r.Close
End Function

Public Sub Sostituisci_Area_Appartenenza_In_DB(Area_Sorgente As String, Area_Destinazione)
  Dim r As Recordset
    
  Clessidra True
  Set r = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where Area_Appartenenza = '" & Area_Sorgente & "'")
  While Not r.EOF
    r!Area_Appartenenza = UCase(Area_Destinazione)
    r.Update
    
    r.MoveNext
  Wend
  r.Close
  Clessidra False
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 10-10-06:
' Aggiunta Transazione (sql): per qualsiasi errorino perdeva TUTTA la TREEVIEW!!!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub Salva_Struttura_TreeView_In_DB(ByVal trw As TreeView, Optional wControl)
  Dim r As Recordset, rs As Recordset, rc As Recordset
  Dim i As Long, k As Long
  Dim Relative As String, Liv As String, wDir As String
  
  On Error GoTo sqlError
  
  DLLFunzioni.FnConnection.BeginTrans
  DLLFunzioni.FnConnection.Execute ("Delete * From BS_TreeView")
  Set r = DLLFunzioni.Open_Recordset("Select * From BS_TreeView")
  If r.RecordCount = 0 Then
    For i = 1 To trw.Nodes.count
      r.AddNew
      
      Liv = "L" & Restituisci_Livello_Su_TreeView(trw, trw.Nodes(i).Index)
      r!Livello = Liv
      
      r!id = trw.Nodes(i).Index
      
      If Liv = "L2" Then
        r!Testo = Crea_Directory_Parametrica(trw.Nodes(i).Text)
      Else
        r!Testo = trw.Nodes(i).Text
      End If
      
      If trw.Nodes(i).Index > 1 Then
        r!NRelative = trw.Nodes(i).Parent.Key
        r!Tipo = tvwChild
      Else
        r!Tipo = 0
      End If
      r!Key = Trim(trw.Nodes(i).Key)
      r!Image = trw.Nodes(i).Image
      r.Update
    Next i
  End If
  
  r.Close
  DLLFunzioni.FnConnection.CommitTrans
  Exit Sub
sqlError:
  'Ripristino!!!
  DLLFunzioni.FnConnection.RollbackTrans
  If ERR.Number = -2147467259 Then
    MsgBox "No writing permissions on Repository." & vbCrLf & "Please check '" & DatabaseName & "' properties.", vbCritical, "i-4.Migration TMP"
  Else
    MsgBox "TMP: Treeview problems!", vbCritical, "i-4.Migration TMP"
  End If
End Sub

Public Function FormattaData(txtData As String) As String
  Dim txtGiorno As Integer, txtMese As Integer, txtAnno As Integer
  Dim txtOra As Integer, txtMinuti As Integer, txtSecondi As Integer
  
  If txtData <> "" Then
    txtGiorno = Day(txtData)
    txtMese = Month(txtData)
    txtAnno = Year(txtData)
    
    txtOra = Hour(txtData)
    txtMinuti = Minute(txtData)
    txtSecondi = Second(txtData)
    
    If txtAnno Then
      FormattaData = Format(txtAnno, "0000") & "/" & Format(txtMese, "00") & "/" & Format(txtGiorno, "00")
      'If txtOra Then
        FormattaData = FormattaData & Space(1) & Format(txtOra, "00") & "." & Format(txtMinuti, "00") & "." & Format(txtSecondi, "00")
      'End If
    End If
  Else
    FormattaData = ""
  End If
End Function

Function LeggiParam(wKey As String) As String
  Dim wResp() As String
  
  On Error GoTo ErrResp
  
  wResp = m_fun.RetrieveParameterValues(wKey)
  If InStr(wResp(0), wKey) Then
    LeggiParam = ""
  Else
    LeggiParam = wResp(0)
  End If
  Exit Function
ErrResp:
  LeggiParam = ""
End Function

'Legge FILE_INI (tutto?!) in "GbConnectionColl"
Private Sub Carica_Parametri_Prodotto()
  Dim wRet As Long
  Dim wBuff As String
  Dim i As Integer
  Dim sApp As String
  Dim s() As String
  
  GbConnectionColl.Add GbPathPrd
  
  'PATH IMAGES (PARZIALE)
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("PATHS", "IMGPATH", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbPathImg = Left(wBuff, wRet)
  'EDITOR ESTERNO
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("TEXTEDT", "EXTERNALEDT", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbExternalEditor = Left(wBuff, wRet)
  
  ' S 10-1-2008
  'EDITOR COMPARE ESTERNO
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("TEXTEDT", "COMPARE-PARAM", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbCompareEditor = Left(wBuff, wRet)
  
  ReDim m_ObjType(0)
  
  For i = 1 To 100
    wBuff = String(100, Chr(10))
    wRet = GetPrivateProfileString("TYPESOURCE", CStr(i), "", wBuff, 100, GbPathPrd & FILE_INI)
    sApp = Left(wBuff, wRet)
    
    If Trim(sApp) <> "" Then
      s = Split(sApp, "#")
      ReDim Preserve m_ObjType(UBound(m_ObjType) + 1)
      m_ObjType(UBound(m_ObjType)).oLabel = s(0)
      m_ObjType(UBound(m_ObjType)).oType = s(1)
      m_ObjType(UBound(m_ObjType)).oArea = s(2)
      m_ObjType(UBound(m_ObjType)).oLevel = s(3)
      m_ObjType(UBound(m_ObjType)).oDescrizione = s(4)
      ' Ma 11/02/2008 : Carica le estensioni dei file
      m_ObjType(UBound(m_ObjType)).oEstensione = s(5)
    End If
  Next i
  
  '******************************************************************************
  '**** Carica le variabili per le connessioni ADO ai Db sistema e progetto *****
  '******************************************************************************
  'PROVIDER SYS DB
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBCONNECTIONS", "SYS_PROVIDER", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbProvider_Sys = DLLFunzioni.GetProviderByType(Left(wBuff, wRet))
   
  GbConnectionColl.Add Left(wBuff, wRet)
  GbConnectionColl.Add GbProvider_Sys
   
  'SYS SECURITY INFO
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBCONNECTIONS", "SYS_SECURITYINFO", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbSecurityInfo_Sys = Left(wBuff, wRet)
  
  GbConnectionColl.Add GbSecurityInfo_Sys
  
  'SYS ID
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBCONNECTIONS", "SYS_ID", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbId_Sys = Left(wBuff, wRet)
  
  GbConnectionColl.Add GbId_Sys
  
  'SYS DATA SOURCE
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBCONNECTIONS", "SYS_DATASOURCE", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbDataSource_Sys = Left(wBuff, wRet)
  
  GbConnectionColl.Add GbDataSource_Sys
  
  'SYS DATABASE
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBCONNECTIONS", "SYS_DATABASE", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbDatabase_Sys = Left(wBuff, wRet)
  
  GbConnectionColl.Add GbDatabase_Sys
  
  'ACCESS PROVIDER
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROVIDER", "ACCESS", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbAccessProvider = Left(wBuff, wRet)
  'SQLSERVER PROVIDER
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("DBPROVIDER", "SQLSERVER", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbSqlServerProvider = Left(wBuff, wRet)

  'PATH SYS MDB
  wBuff = String(100, Chr(10))
  wRet = GetPrivateProfileString("PATHS", "SYSMDBPATH", "", wBuff, 100, GbPathPrd & FILE_INI)
  GbSysMdbPath = Left(wBuff, wRet)
End Sub

