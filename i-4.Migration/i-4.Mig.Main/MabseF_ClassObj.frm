VERSION 5.00
Begin VB.Form MabseF_ClassObj 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Module Classification"
   ClientHeight    =   1560
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   3195
   ControlBox      =   0   'False
   Icon            =   "MabseF_ClassObj.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   1560
   ScaleWidth      =   3195
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOk 
      Caption         =   "Apply"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Change type of selected objects..."
      Top             =   990
      Width           =   945
   End
   Begin VB.ComboBox cboChangeType 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   60
      TabIndex        =   0
      Top             =   450
      Width           =   3045
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   7530
      Picture         =   "MabseF_ClassObj.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   3150
      Width           =   945
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   90
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   990
      Width           =   945
   End
   Begin VB.Label Label1 
      Caption         =   "Type Options:"
      ForeColor       =   &H00000080&
      Height          =   225
      Left            =   90
      TabIndex        =   4
      Top             =   150
      Width           =   1725
   End
End
Attribute VB_Name = "MabseF_ClassObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_wTipo As String
Private m_wLevel As String
Public Area As String

Public Property Get Tipo() As String
  Tipo = m_wTipo
End Property

Public Property Get Level() As String
  Level = m_wLevel
End Property

Private Sub cboChangeType_Click()
  m_wTipo = m_ObjType(cboChangeType.ListIndex + 1).oType
  m_wLevel = m_ObjType(cboChangeType.ListIndex + 1).oLevel
  Area = m_ObjType(cboChangeType.ListIndex + 1).oArea
End Sub

Private Sub cmdExit_Click()
  m_wTipo = ""
  m_wLevel = ""
  Area = ""
  Unload Me
End Sub

'Stefano Dell
Private Sub cmdOK_Click()
  Unload Me
End Sub
Private Sub Form_Load()
  Carica_Combo_Type_Object
End Sub

Public Sub Carica_Combo_Type_Object()
  'Carica dal file di config la combo dei type oggetti
  Dim i As Long
  
  For i = 1 To UBound(m_ObjType)
    cboChangeType.AddItem m_ObjType(i).oType & " (" & m_ObjType(i).oDescrizione & ")"
  Next i
End Sub

