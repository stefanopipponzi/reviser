VERSION 5.00
Begin VB.Form MabseF_About 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   2385
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   6495
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   18
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2385
   ScaleWidth      =   6495
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      BackColor       =   &H00008000&
      BorderStyle     =   0  'None
      ForeColor       =   &H00008000&
      Height          =   435
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   6495
      TabIndex        =   0
      Top             =   0
      Width           =   6495
      Begin VB.Label Label1 
         BackColor       =   &H80000016&
         BackStyle       =   0  'Transparent
         Caption         =   "About i-4.Migration"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   250
         Left            =   120
         TabIndex        =   1
         Top             =   50
         Width           =   4905
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Height          =   2085
      Index           =   1
      Left            =   20
      TabIndex        =   3
      Top             =   270
      Width           =   2085
      Begin VB.PictureBox picI4 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   1650
         Left            =   240
         Picture         =   "MabseF_About.frx":0000
         ScaleHeight     =   110
         ScaleMode       =   0  'User
         ScaleWidth      =   113
         TabIndex        =   4
         Top             =   360
         Width           =   1700
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Height          =   2085
      Index           =   0
      Left            =   2110
      TabIndex        =   2
      Top             =   270
      Width           =   4400
      Begin VB.Label lblLicenceInfo 
         BackColor       =   &H00FFFFFF&
         Caption         =   "License Information:"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   480
         TabIndex        =   8
         Top             =   1080
         Width           =   3135
      End
      Begin VB.Label lblCopyright 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Copyright i-4 s.r.l. (2011)"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   7
         Top             =   720
         Width           =   3135
      End
      Begin VB.Label lblProductLicence 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "<product-license>"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   480
         TabIndex        =   6
         Top             =   1320
         Width           =   3495
      End
      Begin VB.Label lblProductName 
         BackColor       =   &H00FFFFFF&
         Caption         =   "<product-name - version>"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   5
         Top             =   360
         Width           =   3135
      End
   End
End
Attribute VB_Name = "MabseF_About"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Dim wLicense As String
   
  lblProductName.Caption = app.ProductName & " " & "v" & app.Major & "." & app.Minor & "." & Format(app.Revision, "00") 'versione
  wLicense = "Customer: " & GbNomeCliente & vbCrLf & _
             "Workstation: " & DLLFunzioni.FnLicenseProperties_PCName & vbCrLf & _
             "Serial Number: " & GbPrdID
             
  lblProductLicence.Caption = wLicense
End Sub

Private Sub Form_Unload(Cancel As Integer)
  SetFocusWnd MabseF_Prj.hWnd
End Sub

Private Sub picI4_Click()
  Unload Me
End Sub

Private Sub Picture1_Click()
  Unload Me
End Sub
