VERSION 5.00
Begin VB.Form MabseF_Notes 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Modules notes"
   ClientHeight    =   2205
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   5040
   ControlBox      =   0   'False
   Icon            =   "MabseF_Notes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2205
   ScaleWidth      =   5040
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtNotes 
      BackColor       =   &H80000018&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1335
      Left            =   60
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   60
      Width           =   4905
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Save"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   3960
      Picture         =   "MabseF_Notes.frx":2AFA
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1530
      Width           =   945
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   2880
      Picture         =   "MabseF_Notes.frx":483C
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1530
      Width           =   945
   End
End
Attribute VB_Name = "MabseF_Notes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private pf_Note As String
Private pf_Unload As Long
Private pf_Change As Boolean

Private Sub cmdAnnulla_Click()
  'Sei sicuro...Perderai tutti i dati
  Dim wScelta As Long
  
  pf_Note = txtNotes.Text
  GbOggSelNotes = pf_Note
  
  Me.Hide
  
  If pf_Change And Trim(pf_Note) <> "" Then
    wScelta = DLLFunzioni.Show_MsgBoxError("MP05I")
  Else
    wScelta = vbYes
  End If
  
  pf_Unload = wScelta
  
  Me.Show
  
  If wScelta = vbYes Then
    Unload Me
  End If
End Sub

Private Sub cmdOK_Click()
  Dim rs As Recordset
  Dim i As Long
  
  Screen.MousePointer = vbHourglass
  
  For i = 1 To MabseF_List.GridList.ListItems.count
    If MabseF_List.GridList.ListItems(i).Selected = True Then
      Set rs = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where " & _
                                          "IdOggetto = " & CLng(MabseF_List.GridList.ListItems(i).Text))
      
      If rs.RecordCount Then
        'Scrive le note
        rs!Notes = txtNotes.Text
        rs.Update
        
        pf_Note = Trim(txtNotes.Text)
        GbOggSelNotes = pf_Note
      End If
         
      rs.Close
    End If
  Next i
  
  Screen.MousePointer = vbDefault
  
  Me.Hide
  Unload Me
End Sub

Property Get newNote() As String
  newNote = pf_Note
End Property

Public Sub setForm(mVar As String)
  txtNotes.Text = mVar
End Sub

Property Get goUnload() As Long
  goUnload = pf_Unload
End Property

Private Sub Form_Unload(Cancel As Integer)
  SetFocusWnd MabseF_Prj.hWnd
End Sub

Private Sub txtNotes_KeyPress(KeyAscii As Integer)
  pf_Change = True
End Sub
