VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MabseF_Log 
   BackColor       =   &H00FFFF80&
   BorderStyle     =   0  'None
   ClientHeight    =   1500
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10665
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   1500
   ScaleWidth      =   10665
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraLog 
      BackColor       =   &H00FFFF80&
      Caption         =   "Log"
      Height          =   1455
      Left            =   40
      TabIndex        =   1
      Top             =   0
      Width           =   10550
      Begin MSComctlLib.ListView LstLog 
         Height          =   1125
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   10335
         _ExtentX        =   18230
         _ExtentY        =   1984
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   16777215
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Col1"
            Object.Width           =   5292
         EndProperty
      End
   End
   Begin MSComctlLib.ListView LstCmdSrv 
      Height          =   405
      Left            =   2040
      TabIndex        =   0
      Top             =   600
      Visible         =   0   'False
      Width           =   2685
      _ExtentX        =   4736
      _ExtentY        =   714
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Oggetto"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Comando"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Stato "
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Text            =   "RetCode"
         Object.Width           =   1764
      EndProperty
   End
End
Attribute VB_Name = "MabseF_Log"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Resize
End Sub
Public Sub Resize()
  On Error Resume Next
  
  Me.Move GbLeftForm, GbHeightForm + 240, GbWidthForm, Me.Height
  
End Sub
Sub Form_Resize()
  On Error Resume Next
  
  fraLog.Move 40, 30, Me.Width - 160, Me.Height - 40
  
  LstLog.Width = fraLog.Width - 200
  LstLog.ColumnHeaders(1).Width = LstLog.Width - 100
  
End Sub

Private Sub LstLog_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

'Private Sub SSTab1_Click(PreviousTab As Integer)
'  If SSTab1.Tab = 1 Then
'    If Not GbConnMFE Then
'      If LstCmdSrv.ListItems.count = 0 Then
'        LstCmdSrv.ListItems.Add , , "No connection"
'      End If
'    Else
'      If LstCmdSrv.ListItems.count Then
'        LstCmdSrv.ListItems.Remove 0
'      End If
'    End If
'  End If
'End Sub
