VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.UserControl RevComDialog 
   ClientHeight    =   4185
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   8550
   ScaleHeight     =   4185
   ScaleWidth      =   8550
   ToolboxBitmap   =   "NewCommonDialog.ctx":0000
   Begin VB.ComboBox cboPattern 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00C00000&
      Height          =   315
      ItemData        =   "NewCommonDialog.ctx":0312
      Left            =   990
      List            =   "NewCommonDialog.ctx":0314
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   3570
      Width           =   5775
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "Add"
      Height          =   435
      Left            =   7200
      Picture         =   "NewCommonDialog.ctx":0316
      TabIndex        =   6
      Tag             =   "fixed"
      Top             =   3510
      Width           =   945
   End
   Begin VB.FileListBox FileListBox 
      Height          =   870
      Left            =   1260
      TabIndex        =   5
      Top             =   5220
      Visible         =   0   'False
      Width           =   435
   End
   Begin MSComctlLib.ListView lswFile 
      Height          =   3230
      Left            =   4200
      TabIndex        =   2
      Top             =   60
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   5689
      View            =   2
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   12582912
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "files"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.DirListBox DirListBox 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00C00000&
      Height          =   2790
      Left            =   90
      TabIndex        =   1
      Top             =   480
      Width           =   4035
   End
   Begin VB.DriveListBox DriveListBox 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   1080
      TabIndex        =   0
      Top             =   90
      Width           =   3045
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7800
      Top             =   2640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "NewCommonDialog.ctx":0758
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "NewCommonDialog.ctx":3262
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "File Type :"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   90
      TabIndex        =   4
      Top             =   3630
      Width           =   885
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Browse in :"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   150
      Width           =   930
   End
End
Attribute VB_Name = "RevComDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Event FileOpen(arFiles() As String, SelPath As String)
Event UnloadDialog()

Private arrFileName() As String
Private mFilter As String
Private mFilterIndex As Long

Private arrSelectedFileNames() As String
Dim arrDirectory() As String

Private Sub cboPattern_Click()
  Dim ptrn As String
  Dim pat1 As Integer, pat2 As Integer

  On Error GoTo EH

  ptrn = cboPattern.List(cboPattern.ListIndex)
  pat1 = InStr(ptrn, "(")
  pat2 = InStr(ptrn, ")")

  FileListBox.Pattern = Mid$(ptrn, pat1 + 1, pat2 - pat1 - 1)
  
  LoadFileNameArray

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub cmdCancel_Click()
  ReDim arrSelectedFileNames(0)
  RaiseEvent UnloadDialog
End Sub

Private Sub cmdOpen_Click()
  Dim i As Long
  Dim j As Long

  On Error GoTo EH
  
  If lswFile.ListItems.count > 0 Then
    i = 0
    j = 0
    ReDim arrSelectedFileNames(0) 'ALE
    arrSelectedFileNames(0) = DirListBox.path
    For i = 1 To lswFile.ListItems.count
      If lswFile.ListItems(i).Selected Then
        If lswFile.ListItems(i).SmallIcon = 1 Then 'si tratta di file
          j = j + 1
          ReDim Preserve arrSelectedFileNames(j)
          arrSelectedFileNames(j) = DirListBox.path & "\" & lswFile.ListItems(i)
        Else 'directory
'          Dim MyName As String
'          MyName = Dir(DirListBox.path & "\" & lswFile.ListItems(i) & "\", vbNormal)
'          Do While MyName <> ""   ' Avvia il ciclo.
'            j = j + 1
'            ReDim Preserve arrSelectedFileNames(j)
'            arrSelectedFileNames(j) = DirListBox.path & "\" & lswFile.ListItems(i) & "\" & MyName
'            MyName = Dir   ' Legge la voce successiva.
'          Loop
          arrDirectory = caricaDirectory(DirListBox.path & "\" & lswFile.ListItems(i), 0, arrDirectory)
          j = caricaFiles(arrDirectory, j)
        End If
      End If
    Next
    RaiseEvent FileOpen(arrSelectedFileNames, DirListBox.path)
  End If
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Function caricaDirectory(origDir As String, d As Long, arrDirectory() As String) As String()
  Dim MyName As String
  Dim contD As Long
  Dim i As Long
  Dim arrAppo() As String
    
  contD = d
  arrAppo = arrDirectory
  ReDim Preserve arrAppo(contD)
  arrAppo(contD) = origDir
  MyName = Dir(origDir & "\", vbDirectory)
  Do While MyName <> ""
    If MyName <> "." And MyName <> ".." Then
      If (GetAttr(origDir & "\" & MyName) And vbDirectory) = vbDirectory Then
        contD = contD + 1
        ReDim Preserve arrAppo(contD)
        arrAppo(contD) = origDir & "\" & MyName
        'd = caricaDirectory(arrDirectory(contD), contD, arrDirectory)
      End If
    End If
    MyName = Dir
  Loop
  For i = d + 1 To contD
    'SQ - aggiunto controllo... NON FUNZIONA SU SOTTODIRECTORY!
    If contD <= UBound(arrAppo) Then
      'arrAppo = caricaDirectory(arrAppo(contD), i, arrAppo)
      arrAppo = caricaDirectory(arrAppo(contD), contD, arrAppo)
    End If
  Next
  caricaDirectory = arrAppo
End Function

Private Function caricaFiles(pathDirectory() As String, j As Long) As Long
  Dim MyName As String
  Dim i As Long
  
  For i = 0 To UBound(pathDirectory)
    MyName = Dir(pathDirectory(i) & "\", vbDirectory)
    Do While MyName <> ""   ' Avvia il ciclo.
      If MyName <> "." And MyName <> ".." Then
        If Not (GetAttr(pathDirectory(i) & "\" & MyName) And vbDirectory) = vbDirectory Then
          j = j + 1
          ReDim Preserve arrSelectedFileNames(j)
          arrSelectedFileNames(j) = pathDirectory(i) & "\" & MyName
        End If
      End If
      MyName = Dir   ' Legge la voce successiva.
    Loop
  Next
  caricaFiles = j
End Function

Private Sub DirListBox_Change()
  On Error GoTo EH

  LoadFileNameArray

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub DirListBox_Click()
  Dim i As Long
  Dim MyName As String
  
  On Error GoTo EH

  DirListBox.path = DirListBox.List(DirListBox.ListIndex)

  ' Ma 19/06/2007 : Eliminata per incasinamento del path di progetto
  'GbCurDBPath = DirListBox.path 'ALE
  
  lswFile.ListItems.Clear

  For i = 0 To UBound(arrFileName)
    If Trim(arrFileName(i)) <> "" Then
      lswFile.ListItems.Add , , arrFileName(i), , 1
    End If
  Next i
  
  'da completare ALE 16/05/2006
  '*******************
  'MyName = Dir(GbCurDBPath & "\", vbDirectory)
  MyName = Dir(DirListBox.path & "\", vbDirectory)
  Do While MyName <> ""
    If MyName <> "." And MyName <> ".." Then
      'If (GetAttr(GbCurDBPath & "\" & MyName) And vbDirectory) = vbDirectory Then
      If (GetAttr(DirListBox.path & "\" & MyName) And vbDirectory) = vbDirectory Then
        lswFile.ListItems.Add , , MyName, , 2
      End If
    End If
    MyName = Dir
  Loop
  '********************
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub DriveListBox_Change()
  Dim sDrive As String

  On Error GoTo EH

  sDrive = DriveListBox.Drive
  If Right(sDrive, 1) <> "\" And Right(sDrive, 1) <> "]" Then
    sDrive = sDrive & "\"
  End If
  DirListBox.path = sDrive

  Exit Sub
EH: MsgBox sDrive & " Drive Read Error" & vbCr & _
  "Error #" & ERR.Number & ": " & ERR.Description, vbCritical, app.Title
End Sub

Private Sub LoadFileNameArray()
  Dim sMyPath As String
  Dim sMyName As String
  Dim i As Integer
  Dim iFileAttribute As Integer
  
  On Error GoTo EH
  
  MousePointer = vbHourglass
  
  FileListBox.path = DirListBox.path

  If FileListBox.ListCount > 0 Then
    ReDim arrFileName(FileListBox.ListCount - 1)
  End If

  For i = 0 To UBound(arrFileName)
    arrFileName(i) = vbNullString
  Next i

  sMyPath = DirListBox.path

  If Right(sMyPath, 1) <> "\" Then
    sMyPath = sMyPath & "\"
  End If
  
  For i = 0 To FileListBox.ListCount - 1
    arrFileName(i) = FileListBox.List(i)
  Next i
  
  'Testare se si pu� togliere
'  Do While sMyName <> ""
'    If sMyName <> "." And sMyName <> ".." Then
'      iFileAttribute = GetAttr(sMyPath & sMyName)
'      If (iFileAttribute And vbDirectory) <> vbDirectory Then
'        arrFileName(i) = sMyName
'        i = i + 1
'      End If
'    End If
'    sMyName = Dir
'  Loop
  MousePointer = vbDefault
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub LoadPatterns()
  Dim sFilter() As String
  Dim i As Long
  
  On Error GoTo EH
  
  cboPattern.Clear
  If Trim(mFilter) = "" Then
    'se il filter non � valorizzato mette i default filter
    cboPattern.AddItem "All Files(*.*) |*.*|"
    cboPattern.AddItem "Cobol Sources(*.cbl) |*.cbl|"
    cboPattern.AddItem "Assembler Sources(*.asm) |*.asm|"
    cboPattern.AddItem "Cobol Copy(*.cpy) |*.cpy|"
    cboPattern.AddItem "CICS Maps(*.bms) |*.bms|"
    cboPattern.AddItem "IMS Maps(*.mfs) |*.mfs|"
    cboPattern.AddItem "JCL(*.jcl) |*.jcl|"
    cboPattern.AddItem "PROCEDURE(*.prc) |*.prc|"
    cboPattern.AddItem "PARAMETERS(*.prm) |*.prm|"
    cboPattern.AddItem "DBD(*.dbd) |*.dbd|"
    cboPattern.AddItem "PSB(*.psb) |*.psb|"
    cboPattern.AddItem "DDL DB2(*.ddl) |*.ddl|"
    cboPattern.AddItem "Text Files (*.txt) |*.txt|"
    cboPattern.ListIndex = 0
  Else
    sFilter = Split(mFilter, "|")
    
    If UBound(sFilter) > 0 Then
      For i = 0 To UBound(sFilter) - 1 Step 2
        cboPattern.AddItem sFilter(i)
      Next i
      If mFilterIndex <> 0 Then
        cboPattern.ListIndex = mFilterIndex - 1
      Else
        cboPattern.ListIndex = 0
      End If
    End If
  End If
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub txtFiles_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub Label3_Click()

End Sub

Private Sub UserControl_Initialize()
  On Error GoTo EH

  LoadPatterns

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Public Property Get FileArray() As Variant
  On Error GoTo EH
  
  FileArray = arrSelectedFileNames
  
  Exit Property
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Property

Private Sub UserControl_Terminate()
  RaiseEvent UnloadDialog
End Sub

Public Property Let Filter(vFilter As String)
  mFilter = vFilter
   
  LoadPatterns
End Property

Public Property Let FilterIndex(vFilterIndex As String)
  mFilterIndex = vFilterIndex
End Property

Public Property Let DirListBox_Path(objPath As String)
  DirListBox.path = objPath
End Property
