VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.MDIForm MabseF_Prj 
   BackColor       =   &H00FFFF80&
   Caption         =   " i-4.Migration"
   ClientHeight    =   6945
   ClientLeft      =   60
   ClientTop       =   -2160
   ClientWidth     =   12525
   Icon            =   "MabseF_Prj.frx":0000
   NegotiateToolbars=   0   'False
   ScrollBars      =   0   'False
   Visible         =   0   'False
   Begin MSWinsockLib.Winsock wSokComp 
      Left            =   12000
      Top             =   5880
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog CmdDlgUty 
      Left            =   11160
      Top             =   5880
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   0
      Top             =   6600
      Width           =   12525
      _ExtentX        =   22093
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Key             =   "PRJ"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10874
            Key             =   "MSG"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
            Key             =   "STATUS"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1693
            MinWidth        =   1058
            TextSave        =   "14/06/2012"
            Key             =   "DATE"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1058
            MinWidth        =   1058
            TextSave        =   "17.56"
            Key             =   "TIME"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIconsOLD 
      Left            =   9240
      Top             =   5880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   27
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":4C4A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":4DA4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":4EFE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":5058
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":5372
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":568C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":59A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":5DF8
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":5F52
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":63A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":64FE
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":8CB0
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":B462
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":B5BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":B716
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":B870
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":B9CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":BB24
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":D846
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":D9A0
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":DAFA
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":DC54
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":DDAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":DF08
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":FC42
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":FD9C
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":FEF6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   10080
      Top             =   5880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   61
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":10210
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1036A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":104C4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1061E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":10938
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":10C52
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":10F6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":113BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":11518
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1196A
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":11AC4
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":14276
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":16A28
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":16B82
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":16CDC
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":16E36
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":16F90
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":170EA
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":18E0C
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":18F66
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":190C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1921A
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":19374
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":194CE
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1B208
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1B362
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1B4BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1B7D6
            Key             =   ""
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1C628
            Key             =   ""
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1D8AA
            Key             =   ""
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1EB2C
            Key             =   ""
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1F97E
            Key             =   ""
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":207D0
            Key             =   ""
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":21A52
            Key             =   ""
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2232C
            Key             =   ""
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":22C06
            Key             =   ""
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":28EA0
            Key             =   ""
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2EACA
            Key             =   ""
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2F3A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2FC7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":30558
            Key             =   ""
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":313AA
            Key             =   ""
         EndProperty
         BeginProperty ListImage43 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":316C4
            Key             =   ""
         EndProperty
         BeginProperty ListImage44 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":319DE
            Key             =   ""
         EndProperty
         BeginProperty ListImage45 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":322B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage46 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":32B92
            Key             =   ""
         EndProperty
         BeginProperty ListImage47 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":3346C
            Key             =   ""
         EndProperty
         BeginProperty ListImage48 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":33D46
            Key             =   ""
         EndProperty
         BeginProperty ListImage49 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":34620
            Key             =   ""
         EndProperty
         BeginProperty ListImage50 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":34EFA
            Key             =   ""
         EndProperty
         BeginProperty ListImage51 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":357D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage52 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":360AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage53 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":36988
            Key             =   ""
         EndProperty
         BeginProperty ListImage54 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":37262
            Key             =   ""
         EndProperty
         BeginProperty ListImage55 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":37B3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage56 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":38416
            Key             =   ""
         EndProperty
         BeginProperty ListImage57 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":38CF0
            Key             =   ""
         EndProperty
         BeginProperty ListImage58 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":3900A
            Key             =   ""
         EndProperty
         BeginProperty ListImage59 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":3AD5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage60 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":3CA66
            Key             =   ""
         EndProperty
         BeginProperty ListImage61 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":3D000
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuPopUp 
      Caption         =   "PopUp"
      Visible         =   0   'False
      Begin VB.Menu mnuAddMissing 
         Caption         =   "Add &Referenced Modules "
      End
      Begin VB.Menu sep01 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTextEd 
         Caption         =   "Tool Editor"
      End
      Begin VB.Menu mnuExternalEditor 
         Caption         =   "External Editor"
      End
      Begin VB.Menu mnuMapEd 
         Caption         =   "Map Editor"
      End
      Begin VB.Menu mnuExternalCompare 
         Caption         =   "Compare selection"
      End
      Begin VB.Menu sep02 
         Caption         =   "-"
      End
      Begin VB.Menu mnuProperties 
         Caption         =   "Properties"
      End
      Begin VB.Menu sep03 
         Caption         =   "-"
      End
      Begin VB.Menu mnuChangeType 
         Caption         =   "Change Classification..."
      End
      Begin VB.Menu sep04 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelObjs 
         Caption         =   "Delete selection"
      End
      Begin VB.Menu mnuRestoreObjs 
         Caption         =   "Restore selection"
      End
      Begin VB.Menu mnuRestoreTrace 
         Caption         =   "Restore selection (with trace)"
      End
      Begin VB.Menu sep05 
         Caption         =   "-"
      End
      Begin VB.Menu mnuParseObjs 
         Caption         =   "Parsing - first level"
      End
      Begin VB.Menu mnuParseObjs2 
         Caption         =   "Parsing - second level"
      End
      Begin VB.Menu sep06 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditNotes 
         Caption         =   "Edit Note..."
      End
      Begin VB.Menu sep07 
         Caption         =   "-"
      End
      Begin VB.Menu mnuUpdateObj 
         Caption         =   "Refresh List (R.C.)"
      End
      Begin VB.Menu sep08 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCompileObjs 
         Caption         =   "Compile selection"
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuNewProject 
         Caption         =   "New Project"
      End
      Begin VB.Menu mnuOpenPrj 
         Caption         =   "Open Project..."
      End
      Begin VB.Menu mnuOpenPrjRebuild 
         Caption         =   "Open Project [Rebuild]"
         Visible         =   0   'False
      End
      Begin VB.Menu sep12 
         Caption         =   "-"
      End
      Begin VB.Menu mnuImport 
         Caption         =   "Import"
         Begin VB.Menu mnuImportIms 
            Caption         =   "IMS"
            Begin VB.Menu mnuImportImsCat 
               Caption         =   "Catalog..."
            End
         End
         Begin VB.Menu mnuImportCics 
            Caption         =   "CICS"
            Begin VB.Menu mnuImportCicsPCT 
               Caption         =   "PCT..."
            End
            Begin VB.Menu mnuImportCicsFCT 
               Caption         =   "FCT..."
            End
            Begin VB.Menu mnuImportCicsDFHCSDUP 
               Caption         =   "DFHCSDUP..."
            End
         End
         Begin VB.Menu mnuImportContents 
            Caption         =   "SAS"
            Visible         =   0   'False
            Begin VB.Menu mnuImportContentsProc 
               Caption         =   "CONTENTS"
            End
         End
      End
      Begin VB.Menu sep14 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRepository 
         Caption         =   "Repository"
         Begin VB.Menu mnucheck 
            Caption         =   "Status Check"
         End
         Begin VB.Menu mnu_prjdbStructure 
            Caption         =   "Database details"
         End
      End
      Begin VB.Menu sep15 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuProject 
      Caption         =   "Project"
      Begin VB.Menu mnuAddObjects 
         Caption         =   "&Add Modules..."
      End
      Begin VB.Menu addMissing 
         Caption         =   "Add &Referenced Modules"
      End
      Begin VB.Menu mnuAddObjects_Tmp 
         Caption         =   "Add Object temp"
         Visible         =   0   'False
      End
      Begin VB.Menu sep20 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelObjects 
         Caption         =   "&Delete Modules"
      End
      Begin VB.Menu mnuRestoreObjects 
         Caption         =   "Restore Modules"
      End
      Begin VB.Menu mnuRestoreTracemnu 
         Caption         =   "Restore Modules with trace"
      End
      Begin VB.Menu sep11 
         Caption         =   "-"
      End
      Begin VB.Menu mnuParamPrj 
         Caption         =   "Configuration"
         Begin VB.Menu mnuSysObjDecl 
            Caption         =   """System Modules"" Declaration"
         End
         Begin VB.Menu mnuConfigEnv 
            Caption         =   "Environment Parameters"
         End
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "View"
      Begin VB.Menu mnuTextEditor 
         Caption         =   "Tool Editor"
      End
      Begin VB.Menu mnuEXTTextEditor 
         Caption         =   "External Editor"
      End
      Begin VB.Menu sep17 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMapEditor 
         Caption         =   "Map Editor"
      End
      Begin VB.Menu sep21 
         Caption         =   "-"
      End
      Begin VB.Menu filterSelection 
         Caption         =   "Filter Selection"
      End
      Begin VB.Menu filterExecution 
         Caption         =   "Filter Execution"
      End
      Begin VB.Menu sep99 
         Caption         =   "-"
      End
      Begin VB.Menu refreshTreeview 
         Caption         =   "Refresh Treeview"
      End
      Begin VB.Menu mnuRefreshRC 
         Caption         =   "Refresh Selection (R.C.)"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "Tools"
      Begin VB.Menu mnuParser 
         Caption         =   "Parser"
         Begin VB.Menu mnuParseObjsMenu 
            Caption         =   "First Level (selected modules)"
         End
         Begin VB.Menu mnuParseObjsMenu2 
            Caption         =   "Second Level (selected modules)"
         End
      End
      Begin VB.Menu sep18 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCompilers 
         Caption         =   "Compiler"
         Begin VB.Menu mnuOpenConnection 
            Caption         =   "Open Connection..."
         End
         Begin VB.Menu mnuConfigCompilers 
            Caption         =   "Configure..."
         End
         Begin VB.Menu sep16 
            Caption         =   "-"
         End
         Begin VB.Menu mnuCompile 
            Caption         =   "Compile selected Objects..."
         End
         Begin VB.Menu mnuCompileErr 
            Caption         =   "Compiling Errors"
         End
      End
   End
   Begin VB.Menu mnuObject 
      Caption         =   "Modules"
      Begin VB.Menu mnuObjectProperties 
         Caption         =   "Module Properties"
      End
      Begin VB.Menu sep19 
         Caption         =   "-"
      End
      Begin VB.Menu mnuChangeObjectsType 
         Caption         =   "Change Modules type..."
      End
      Begin VB.Menu sep22 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditNotesMenu 
         Caption         =   "Edit Modules note..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "?"
      Begin VB.Menu mnuHelpTopics 
         Caption         =   "User Manual..."
      End
      Begin VB.Menu sep24 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAbout 
         Caption         =   "About..."
      End
   End
End
Attribute VB_Name = "MabseF_Prj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function apiShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, _
                                                                                  ByVal lpOperation As String, _
                                                                                  ByVal lpFile As String, _
                                                                                  ByVal lpParameters As String, _
                                                                                  ByVal lpDirectory As String, _
                                                                                  ByVal nShowCmd As Long) As Long

'***App Window Constants***
Const WIN_NORMAL = 1         'Open Normal
Const WIN_MAX = 3            'Open Maximized
Const WIN_MIN = 2            'Open Minimized

'***Error Codes***
Private Const ERROR_SUCCESS = 32&
Private Const ERROR_NO_ASSOC = 31&
Private Const ERROR_OUT_OF_MEM = 0&
Private Const ERROR_FILE_NOT_FOUND = 2&
Private Const ERROR_PATH_NOT_FOUND = 3&
Private Const ERROR_BAD_FORMAT = 11&
'SQ tmp - per comodit�
Public fraToolbar As Object

Public Function Genera_Connection_String(nColl As Collection, TypeConn As String)
  'Collection structure
  '1) Percorso
  '2) Provider Type        (Sys)
  '3) Provider DB Progetto (Sys)
  '4) Security Info        (Sys)
  '5) Id                   (Sys)
  '6) Data Source          (Sys)
  '7) Database             (Sys)
  Dim cnStr As String
  
  If Trim(UCase(TypeConn)) = "SYSTEM" Then
    Select Case Trim(UCase(nColl.Item(2)))
      Case "SQLSERVER"
        cnStr = "Provider=" & nColl.Item(3) & "Persist Security Info=" & nColl.Item(7) & _
                "User ID=" & nColl.Item(5) & ";Data Source=" & nColl.Item(6)
      Case "ORACLE"
        DLLFunzioni.Show_MsgBoxError "MB01P"
      Case "ACCESS"
        'Prova a vedere se il file � presente
        If Dir(GbSysMdbPath & "\" & nColl.Item(7), vbNormal) <> "" Then
          cnStr = "Provider=" & nColl.Item(3) & ";Data Source=" & GbSysMdbPath & "\" & nColl.Item(7) & ";" & _
                  "Persist Security Info=" & nColl.Item(4)
        Else
          'Va in locale
          'lo scrive nella finestra di log
          cnStr = "Provider=" & nColl.Item(3) & ";Data Source=" & nColl.Item(1) & "\" & SYSTEM_DIR & "\" & nColl.Item(7) & ";" & _
                  "Persist Security Info=" & nColl.Item(4)
        End If
      Case "MYSQL"
        DLLFunzioni.Show_MsgBoxError "MB02P"
    End Select
  Else
    ' Stop
  End If
  
  Genera_Connection_String = cnStr
End Function
'''''''''''''''''''''''''''''''''''
' "Progressione Arborescente" (?!)
'''''''''''''''''''''''''''''''''''
Private Sub addMissing_Click()
  Dim i As Long
  Dim cParam As Collection
  
  On Error GoTo errorHandler
  
  Screen.MousePointer = vbHourglass
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  
  For i = 1 To MabseF_List.GridList.ListItems.count
    If MabseF_List.GridList.ListItems(i).Selected Then
      Set cParam = New Collection
      cParam.Add MabseF_List.GridList.ListItems(i).Text
      cParam.Add MabseF_List.GridList.ListItems(i).SubItems(TYPE_OBJ)
      DLLFunzioni.FnActiveWindowsBool = False
      DllParser.AttivaFunzione "ADD_MISSING_OBJECTS", cParam
      DLLFunzioni.FnActiveWindowsBool = True
    End If
  Next i
  
  ''''''''''''''''''''''''''''''''''''''''
  ' Update treeview
  ''''''''''''''''''''''''''''''''''''''''
  Carica_TreeView_Progetto_Fast MabseF_List.PrjTree
  
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  Screen.MousePointer = vbDefault
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "RC0002"
End Sub



Private Sub MDIForm_Load()
'SQ tmp - per comodit�
Set fraToolbar = MabseF_Menu.fraToolbar
'''  fraToolbar.ImageList = imlToolbarIcons
'''  fraToolbar.Buttons(1).Image = 41
'''  fraToolbar.Buttons(2).Image = 58
'''  fraToolbar.Buttons(3).Image = 31
'''  fraToolbar.Buttons(4).Image = 0
'''  fraToolbar.Buttons(5).Image = 45
'''  fraToolbar.Buttons(6).Image = 38
'''  fraToolbar.Buttons(7).Image = 0
'''  fraToolbar.Buttons(8).Image = 46
'''  fraToolbar.Buttons(9).Image = 0
'''  fraToolbar.Buttons(10).Image = 50
'''  fraToolbar.Buttons(11).Image = 35
'''  fraToolbar.Buttons(12).Image = 0
'''  fraToolbar.Buttons(13).Image = 61
'''  fraToolbar.Buttons(14).Image = 25
'''  fraToolbar.Buttons(15).Image = 60
'''  fraToolbar.Buttons(16).Image = 16
'''  fraToolbar.Buttons(17).Image = 17
'''  fraToolbar.Buttons(18).Image = 0
'''  fraToolbar.Buttons(19).Image = 30
'''  fraToolbar.Buttons(20).Image = 0
'''  fraToolbar.Buttons(21).Image = 19
'''  fraToolbar.Buttons(22).Image = 20
'''  fraToolbar.Buttons(23).Image = 22
'''  fraToolbar.Buttons(24).Image = 21
'''  fraToolbar.Buttons(24).Visible = True
'''  fraToolbar.Buttons(25).Image = 0
'''  fraToolbar.Buttons(26).Image = 33
'''  fraToolbar.Buttons(27).Image = 0
'''  fraToolbar.Buttons(28).Image = 24
'''  fraToolbar.Buttons(28).Visible = False
'''  fraToolbar.Buttons(29).Image = 42
'''  fraToolbar.Buttons(29).Visible = True

 'Riattivare prima di compilare
  StatusBar1.Panels(2).Text = ""
  GbFirstLoad = True
  
  'Scrive ctrl
  fixCtrl
  
  Elimina_Flickering Me.hwnd
    
  Clessidra False
  
  'Setta le variabili che devono essere viste dalle DLL
  Setta_Variabili_Export_DLL
  
  'Carica il menu in memoria
  ReDim TotMenu(0)
  
  'Prende il nome del computer (ANCORA?)
  GbComputerName = GetComputerNameU
  GbImgDir = GbPathPrd & "\" & GbPathImg
  
  'Forza a mano le modalit� : In futuro se necessario verr� reimplementato il blocco sui nomi ecc...
  GbModePrj = DLLLic.Lic_NomePC
  GbCodePrj = "0000-0000-0000"
  GbPswUser = GbModePrj
  GbOpenMode = GbModePrj

  Clessidra False
  
  GbDllFunzioniExist = DllFunzioniExist
  If GbDllFunzioniExist Then DllFunzioniInit
  GbDllBaseExist = True 'fare coerentemente con le altre
  If GbDllFunzioniExist Then DllBaseInit
  DllBase.BsPathPrd = GbPathPrd
  
  'verifica DLLParser
  GbDllParserExist = DllParserExist
  If GbDllParserExist Then DllParserInit
  'verifica DLL Dli2RDBMS
  GbDLLDli2RdbMsExist = DLLDli2RdbMsExist
  If GbDLLDli2RdbMsExist Then DLLDli2RdbMsInit
  'verifica DLL Data Manager
  GbDllDataManExist = DllDataManExist
  If GbDllDataManExist Then DllDataManInit (GbDLLDli2RdbMsExist)
  'verifica la DLL Tools
  GbDllToolsExist = DLLToolsExist
  If GbDllToolsExist Then DllToolsInit
  'verifica la DLL Analisi
  GbDllAnalisiExist = DLLAnalisiExist
  If GbDllAnalisiExist Then DllAnalisiInit
  'Verifica DLL Ims
  GbDllImsExist = DLLImsExist
  If GbDllImsExist Then DllImsInit
  
  GbDllVSAMExist = DLLVSAM2RdbMsExist
  If GbDllVSAMExist Then DLLVSAM2RdbMsInit
  
  '''''''''''''''''''''''''''''''''''''''''''''''
  'SQ tmp - Languages to be managed as above
  ' - Assembler
  ReDim Preserve TotMenu(UBound(TotMenu) + 1)
  TotMenu(UBound(TotMenu)).id = DLLVSAM2RdbMs.drId
  TotMenu(UBound(TotMenu)).Funzione = "ASSEMBLER_CONV"
  TotMenu(UBound(TotMenu)).DllName = "MatsdP_00"
  ' - EZT
  ReDim Preserve TotMenu(UBound(TotMenu) + 1)
  TotMenu(UBound(TotMenu)).id = DLLVSAM2RdbMs.drId
  TotMenu(UBound(TotMenu)).Funzione = "EZT_CONV"
  TotMenu(UBound(TotMenu)).DllName = "MatsdP_00"
  ' - PLI
  ReDim Preserve TotMenu(UBound(TotMenu) + 1)
  TotMenu(UBound(TotMenu)).id = DLLVSAM2RdbMs.drId
  TotMenu(UBound(TotMenu)).Funzione = "PLI_CONV"
  TotMenu(UBound(TotMenu)).DllName = "MatsdP_00"
  
  GbFinImCheck = True
  
  'calcola le dimesioni massime che la MDI pu� avere
  Calcola_Dimensioni_Max
  
  'Carica_Parametri_Prodotto
  '''GbMaxheight = 7500
  
  Modify_form
    
  'SQ Terminate_Flickering

  'Resize
  ResizeAll GbFirstLoad
  
  mnuTextEd.Enabled = False
  mnuMapEd.Enabled = False
  mnuEXTTextEditor.Enabled = False
  mnuProperties.Enabled = False
  
  initProject = Command$

  If GbFirstLoad And initProject = "" Then
    ''SQ tmp MabseF_Start.Show vbModal
    Enabled = False
  ElseIf initProject <> "" Then
    MabseF_Prj.runShortcuts "MAIN", "PRJ_NEW"
  End If
  GbFirstLoad = False 'ALE
  
  'SQ messo qui
  Terminate_Flickering
  
  '''''''''''''''''''''''
  ' Toolbar color
  '''''''''''''''''''''''
  ''ChangeTBBack Me.fraToolbar, CreateSolidBrush(RGB(102, 255, 255)), enuTB_STANDARD
  ''ChangeTBBack Me.Toolbar2, CreateSolidBrush(RGB(102, 255, 255)), enuTB_STANDARD
  'Refresh Screen to see changes
  InvalidateRect 0&, 0&, False
  
End Sub

Private Sub Modify_form()
  'Disattiva menu
  'FILE
  mnuAddObjects.Enabled = False
  mnuParamPrj.Enabled = False
  'Compiler
  mnuCompilers.Enabled = False
  'Objects
  mnuObject.Enabled = False
  'Help
  mnuHelp.Enabled = False

  'disattiva bottoni toolbar
  ON_OFF_Button_ToolBar fraToolbar, False
  
  'Open e New project
  ON_OFF_Button_ToolBar fraToolbar, True, 1
  ON_OFF_Button_ToolBar fraToolbar, True, 2
  
  'Help ed Esci
  ON_OFF_Button_ToolBar fraToolbar, True, 25
  ON_OFF_Button_ToolBar fraToolbar, True, 26
   
  'Help topics
  If Dir(GbPathPrd & "\Doc\" & c_FileHelp, vbNormal) <> "" Then
    fraToolbar.Buttons(26).ButtonMenus(1).Enabled = True
    mnuHelpTopics.Enabled = True
  Else
    '''fraToolbar.Buttons(26).ButtonMenus(1).Enabled = False
    mnuHelpTopics.Enabled = False
  End If
  
  'Editor Esterno
  mnuExternalEditor.Enabled = Len(DLLFunzioni.FnExternalEditor) > 0
  mnuExternalEditor.Visible = Len(DLLFunzioni.FnExternalEditor) > 0
  'Menu editor esterno
  mnuEXTTextEditor.Enabled = mnuExternalEditor.Enabled
  mnuEXTTextEditor.Visible = mnuExternalEditor.Visible
  '''fraToolbar.Buttons(13).ButtonMenus(3).Enabled = mnuExternalEditor.Enabled
  '''fraToolbar.Buttons(13).ButtonMenus(3).Visible = mnuExternalEditor.Visible
End Sub

Private Sub MDIForm_Resize()
  Dim i As Long
  Dim wObj As Object
  Dim f As Form 'ALE

  On Error GoTo ERR

  If GbFirstLoad Then Exit Sub

  If Me.WindowState <> vbMinimized Then
    ResizeAll GbFirstLoad

    For i = 1 To DLLFunzioni.CountActiveWindows
      Set wObj = DLLFunzioni.ItemActiveWindows(i)
      Set f = wObj
      f.WindowState = vbNormal
      '''f.Move 0, MabseF_List.Top, MabseF_List.Width, MabseF_List.Height
      'SQ move - TMP!! - until every form is managed...
      If f.Name <> "MabsdF_InfoPrj" Then
        f.Move 0, 0, MabseF_List.Width, MabseF_List.Height
      End If
    Next i
  End If
  Exit Sub
ERR:
  If ERR.Number = 384 Then
    Terminate_Flickering
    Exit Sub
  End If
  
  Terminate_Flickering
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
  Dim i As Long
  Dim wObj As Object
   
  fixCtrl
  
  'Scarica tutte le risorse allocate
  For i = DLLFunzioni.CountActiveWindows To 1 Step -1
    Set wObj = DLLFunzioni.ItemActiveWindows(i)
    Unload wObj
    Set wObj = Nothing
  Next i
  ''''''''''''''''''''''''''''''''''''''''
  ' Rimaneva appeso il processo!!!!!!!!!!
  ''''''''''''''''''''''''''''''''''''''''
  End
End Sub

Private Sub mnu_prjdbStructure_Click()
  Dim f As MabseF_DbStruct
  
  Set f = New MabseF_DbStruct
  SetParent f.hwnd, MabseF_List.hwnd
  Load f
  f.Show
  f.Move 0, 0, MabseF_List.Width, MabseF_List.Height
End Sub

Private Sub mnuAbout_Click()
  'runMenuClick fraToolbar.Buttons(24).ButtonMenus(3)
  Attiva_Funzione "MAIN", New Collection, "PRD_ABOUT"
End Sub

Private Sub mnuAddMissing_Click()
  addMissing_Click
End Sub

Private Sub mnuAddObjects_Click()
  runShortcuts "MAIN", "PRJ_ADD_OBJECTS"
End Sub

'S
Private Sub mnuAddObjects_Tmp_Click()
  'runShortcuts fraToolbar.Buttons(3)
  MabseF_Prj.AttivaFunzioneMain "PRJ_ADD_OBJECTS_TMP"
End Sub

Private Sub mnuAddObjs_Click()
  runShortcuts "MAIN", "PRJ_ADD_OBJECTS"
End Sub

Private Sub mnuAddObjsObj_Click()
  runShortcuts "MAIN", "PRJ_ADD_OBJECTS"
End Sub

Private Sub mnuChangeObjectsType_Click()
  runShortcuts "MAIN", "OBJ_CHANGE_TYPE"
End Sub

Private Sub mnuChangeType_Click()
  runShortcuts "MAIN", "OBJ_CHANGE_TYPE"
End Sub

Public Sub mnuCheck_Click()
  If checkRepository Then
    MsgBox "The Project Repository matches the " & MabseF_Prj.Caption & " requirements", vbInformation, "i-4.Migration"
    'MabseF_Check.Show vbModal
  Else
    MabseF_Check.Show vbModal
  End If
End Sub

Public Function checkRepository() As Boolean
  Dim strconnection As String
  Dim rs As Recordset
  Dim sCurrentTable As String, sNewTable As String, sTableComment As String
  Dim arrayTabsC() As String, arrayTabsP() As String
  Dim customerColumns() As String, prjColumns() As String
  Dim i As Long, c As Long, z As Long
  Dim boolTrovato As Boolean, boolTab As Boolean
  Dim prjField() As String, prjCustomer() As String
  Dim appArrayP() As String, appArrayC() As String
  Dim arrayCommentP() As String, arrayCommentC() As String
 
  checkRepository = False
  strconnection = DLLFunzioni.FnConnDBSys
  strconnection = Replace(strconnection, "System.mdb", "prj.mty")

  Set PrjConnection = New ADODB.Connection
  PrjConnection.ConnectionString = strconnection
  PrjConnection.Open PrjConnection.ConnectionString
  
  Set CustomerConnection = New ADODB.Connection
  strconnection = DLLFunzioni.FnConnection
  CustomerConnection.ConnectionString = strconnection
  CustomerConnection.Open CustomerConnection.ConnectionString
'  Set CustomerConnection = DLLFunzioni.FnConnection
  
  
  'gloria :***********************************************************
  ' prima verifico tabelle
  Dim Criteri As Variant

  Criteri = Array(Empty, Empty, Empty, "Table") ' esclude le tabelle di sistema
  
  Set rs = New Recordset
  Set rs = PrjConnection.OpenSchema(adSchemaTables, Criteri) 'adSchemaTables, Array(Empty, Empty, Empty, "TABLE"))

  i = 0
  ReDim arrayTabsP(0)
  
  Do Until rs.EOF
    sCurrentTable = rs!TABLE_NAME
    ReDim Preserve arrayTabsP(i)
    ReDim Preserve arrayCommentP(i)
    'gloria. aggiungo commento tabella
    sTableComment = IIf(IsNull(rs!Description), "", rs!Description)
    arrayCommentP(i) = sTableComment
    arrayTabsP(i) = UCase(sCurrentTable)
    rs.MoveNext
    i = i + 1
  Loop
  
  'MF - 19/07/07
  'Raggiungeva fine file e non scriveva l'ultima tabella (in questo caso Ts_Macro)
  If rs.EOF Then
    ReDim Preserve arrayTabsP(UBound(arrayTabsP) + 1)
    ReDim Preserve arrayCommentP(UBound(arrayTabsP))
       'gloria. aggiungo commento tabella
    arrayCommentP(UBound(arrayTabsP)) = sTableComment
    arrayTabsP(UBound(arrayTabsP)) = UCase(sCurrentTable)
  End If
  
    ' *************************prima verifico tabelle
  Set rs = New Recordset
  Set rs = CustomerConnection.OpenSchema(adSchemaTables, Criteri) 'adSchemaTables, Array(Empty, Empty, Empty, "TABLE"))

  i = 0
  ReDim arrayTabsC(0)
  sTableComment = ""
  sCurrentTable = ""
  
  Do Until rs.EOF
    sCurrentTable = rs!TABLE_NAME
    ReDim Preserve arrayTabsC(i)
    ReDim Preserve arrayCommentC(i)
    'gloria. aggiungo commento tabella
    sTableComment = IIf(IsNull(rs!Description), "", rs!Description)
    arrayCommentC(i) = sTableComment
    arrayTabsC(i) = UCase(sCurrentTable)
    rs.MoveNext
    i = i + 1
  Loop
  
  'MF - 19/07/07
  'Raggiungeva fine file e non scriveva l'ultima tabella (in questo caso Ts_Macro)
  If rs.EOF Then
    ReDim Preserve arrayTabsC(UBound(arrayTabsC) + 1)
    ReDim Preserve arrayCommentC(UBound(arrayTabsC))
    'gloria. aggiungo commento tabella
    arrayCommentC(UBound(arrayTabsC)) = sTableComment
    arrayTabsC(UBound(arrayTabsC)) = UCase(sCurrentTable)
  End If
  
  MabseF_Check.lswDifferences.ListItems.Clear
  MabseF_Check.lswDifferences.SmallIcons = MabseF_Check.ImageList1

  ' Ma 30/07/2009 : La gloria aveva cercato di fre una cosa un p� pi� carina,
  '                    ma � da rivedere meglio la gestione delle tabelle
  'I casi da gestire sono:
  '                        1) Tabelle presenti in entrambi i Db: Continuo a scorrere l'array
  '                        2) Tabella presente sul Project Db: Segnalo la mancanza sulla lista
  '                        3) Tabella presnete sul Customer Db: Me ne frego e vado avanti a controllare
'''  c = 0
'''  For i = 0 To UBound(arrayTabsP)
'''
'''    If Not (UCase(arrayTabsP(i)) = UCase(arrayTabsC(c))) Then ' manca una tabella
'''       MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i), , 1
'''    Else
''''       If Not (UCase(arrayCommentP(i)) = UCase(arrayCommentC(c))) Then
''''          MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i) & "." & " TableComment(" & arrayCommentP(i) & ")", , 5
''''       End If
'''       c = c + 1
'''    End If
'''  Next i
       
  '********************************
  
  Set rs = New Recordset
  Set rs = PrjConnection.OpenSchema(adSchemaColumns) 'adSchemaTables, Array(Empty, Empty, Empty, "TABLE"))
  
  sCurrentTable = ""
  sNewTable = ""
  i = 0
  Set prjTabs = New Collection
  ReDim arrayTabsP(0)
  
  Do Until rs.EOF
    sCurrentTable = rs!TABLE_NAME
    If (sCurrentTable <> sNewTable) Then
      If sNewTable <> "" Then
        ReDim Preserve arrayTabsP(prjTabs.count)
        'gloria. aggiungo commento tabella
        arrayTabsP(prjTabs.count) = UCase(sNewTable)
        prjTabs.Add prjColumns, UCase(sNewTable)
        i = 0
      End If
      sNewTable = rs!TABLE_NAME
      ReDim prjColumns(0)
    End If
    ReDim Preserve prjColumns(i)
    'gloria: aggiungo gestione commento
    prjColumns(i) = rs!COLUMN_NAME & ";" & ConvType(rs!DATA_TYPE, IIf(IsNull(rs!CHARACTER_MAXIMUM_LENGTH), "", rs!CHARACTER_MAXIMUM_LENGTH)) & ";" & rs!CHARACTER_MAXIMUM_LENGTH & ";" & rs!ordinal_position & ";" & rs!Description
    rs.MoveNext
    i = i + 1
  Loop
  
  'Raggiungeva fine file e non scriveva l'ultima tabella (in questo caso Ts_Macro)
  If rs.EOF Then
    ReDim Preserve arrayTabsP(prjTabs.count)
    'gloria. aggiungo commento tabella
    arrayTabsP(prjTabs.count) = UCase(sNewTable)
    prjTabs.Add prjColumns, UCase(sNewTable)
    i = 0
  End If
  rs.Close
  
  Set rs = CustomerConnection.OpenSchema(adSchemaColumns)

  sCurrentTable = ""
  sNewTable = ""
  i = 0
  Set customerTabs = New Collection
  
  Do Until rs.EOF
    sCurrentTable = rs!TABLE_NAME
    If (sCurrentTable <> sNewTable) Then
      If sNewTable <> "" Then
        ReDim Preserve arrayTabsC(customerTabs.count)
        customerTabs.Add customerColumns, UCase(sNewTable)
        i = 0
      End If
      sNewTable = rs!TABLE_NAME
      ReDim customerColumns(0)
    End If
    ReDim Preserve customerColumns(i)
    'gloria: aggiungo gestione commento
    customerColumns(i) = rs!COLUMN_NAME & ";" & ConvType(rs!DATA_TYPE, IIf(IsNull(rs!CHARACTER_MAXIMUM_LENGTH), "", rs!CHARACTER_MAXIMUM_LENGTH)) & ";" & rs!CHARACTER_MAXIMUM_LENGTH & ";" & rs!ordinal_position & ";" & rs!Description
    rs.MoveNext
    i = i + 1
  Loop
  
  'Raggiungeva fine file e non scriveva l'ultima tabella (in questo caso XE_CopyMap)
  If rs.EOF Then
    customerTabs.Add customerColumns, UCase(sNewTable)
    i = 0
  End If
  
  On Error GoTo errorH
  
  For i = 0 To UBound(arrayTabsP)
    appArrayP = prjTabs(arrayTabsP(i))
    boolTab = True
    appArrayC = customerTabs(arrayTabsP(i))
    If boolTab Then
      For c = 0 To UBound(appArrayP)
        boolTrovato = False
        For z = 0 To UBound(appArrayC)
          prjField() = Split(appArrayP(c), ";") 'nome;type;len
          prjCustomer() = Split(appArrayC(z), ";")
          If UCase(prjField(0)) = UCase(prjCustomer(0)) Then
            boolTrovato = True
            Exit For
          End If
        Next z
        If Not boolTrovato Then
          MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i) & "." & prjField(0), , 4 'appArrayP(c)
        Else
          'controllo il tipo e la lunghezza del campo
          If Not prjField(1) = prjCustomer(1) Then
            MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i) & "." & prjField(0) & " Type(" & prjField(1) & ")", , 3
          End If
          If Not prjField(2) = prjCustomer(2) Then
            MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i) & "." & prjField(0) & " Len(" & prjField(2) & ")", , 2
          End If
          'gloria. confronto la descrizione
          If Not prjField(4) = prjCustomer(4) Then
            MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i) & "." & prjField(0) & " Comment(" & prjField(4) & ")", , 5
          End If

        End If
      Next c
    End If
  Next i
  
  If MabseF_Check.lswDifferences.ListItems.count = 0 Then
    checkRepository = True
  End If
  Exit Function
errorH:
  ' Manca tabella sul Customer Db
  MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i), , 1
  boolTab = False
  Resume Next
End Function

Private Function ConvType(ByVal TypeVal As Long, MaxLen As String) As String
  Select Case TypeVal
    Case adBigInt                    ' 20
      ConvType = "Big Integer"
    Case adBinary                    ' 128
      ConvType = "Binary"
    Case adBoolean                   ' 11
      ConvType = "Boolean"
    Case adBSTR                      ' 8 i.e. null terminated string
      ConvType = "Text"
    Case adChar                      ' 129
      ConvType = "Text"
    Case adCurrency                  ' 6
      ConvType = "Currency"
    Case adDate                      ' 7
      ConvType = "Date/Time"
    Case adDBDate                    ' 133
      ConvType = "Date/Time"
    Case adDBTime                    ' 134
      ConvType = "Date/Time"
    Case adDBTimeStamp               ' 135
      ConvType = "Date/Time"
    Case adDecimal                   ' 14
      ConvType = "Float"
    Case adDouble                    ' 5
      ConvType = "Float"
    Case adEmpty                     ' 0
      ConvType = "Empty"
    Case adError                     ' 10
      ConvType = "Error"
    Case adGUID                      ' 72
      ConvType = "GUID"
    Case adIDispatch                 ' 9
      ConvType = "IDispatch"
    Case adInteger                   ' 3
      ConvType = "Integer"
    Case adIUnknown                  ' 13
      ConvType = "Unknown"
    Case adLongVarBinary             ' 205
      ConvType = "Binary"
    Case adLongVarChar               ' 201
      ConvType = "Text"
    Case adLongVarWChar              ' 203
      ConvType = "Text"
    Case adNumeric                  ' 131
      ConvType = "Long"
    Case adSingle                    ' 4
      ConvType = "Single"
    Case adSmallInt                  ' 2
      ConvType = "Small Integer"
    Case adTinyInt                   ' 16
      ConvType = "Tiny Integer"
    Case adUnsignedBigInt            ' 21
      ConvType = "Big Integer"
    Case adUnsignedInt               ' 19
      ConvType = "Integer"
    Case adUnsignedSmallInt          ' 18
      ConvType = "Small Integer"
    Case adUnsignedTinyInt           ' 17
      ConvType = "Timy Integer"
    Case adUserDefined               ' 132
      ConvType = "UserDefined"
    Case adVarNumeric                 ' 139
      ConvType = "Long"
    Case adVarBinary                 ' 204
      ConvType = "Binary"
    Case adVarChar                   ' 200
      ConvType = "Text"
    Case adVariant                   ' 12
      ConvType = "Variant"
    Case adVarWChar                  ' 202
      ConvType = "Text"
    Case adWChar                     ' 130
      If MaxLen = "0" Then
        ConvType = "Memo" 'Text
      Else
        ConvType = "Text"
      End If
    Case Else
      ConvType = "Unknown"
  End Select
End Function

Private Sub mnuCompile_Click()
  runShortcuts "MAIN", "COM_COMPILE_OBJS"
End Sub

Private Sub mnuCompileErr_Click()
  SetParent MabseF_CompErrors.hwnd, MabseF_List.hwnd
  MabseF_CompErrors.Show
  MabseF_CompErrors.Move 0, 0, MabseF_List.Width, MabseF_List.Height
End Sub

Private Sub mnuCompileObjs_Click()
  runShortcuts "MAIN", "COM_COMPILE_OBJS"
End Sub

Private Sub mnuConfigCompilers_Click()
  runMenuClick "BASE", "PRJ_ENVIRONMENT"
End Sub
'
'Private Sub mnuConfigEnv_Click()
'  runMenuClick fraToolbar.Buttons(8).ButtonMenus(1)
'End Sub

Private Sub mnuDelObjects_Click()
  runShortcuts "MAIN", "OBJ_DELETE"
End Sub

Private Sub mnuDelObjs_Click()
  runShortcuts "MAIN", "OBJ_DELETE"
End Sub

Private Sub mnuEditNotes_Click()
  Dim oForm As Object
  Dim i As Long
  
  If DLLFunzioni.FnProcessRunning Then
    DLLFunzioni.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  Set oForm = New MabseF_Notes
  
  oForm.setForm GbOggSelNotes
  
  oForm.Show vbModal
  
  'setta la listview a video
  For i = 1 To MabseF_List.GridList.ListItems.count
    If MabseF_List.GridList.ListItems(i).Selected Then
      If Trim(oForm.newNote) <> "" Then
        'S 20/10/2008
        'If MabseF_List.GridList.ListItems(i).ListSubItems(NOTES_OBJ).Text = "" Then
          MabseF_List.GridList.ListItems(i).ListSubItems(NOTES_OBJ).Text = GbOggSelNotes
          MabseF_List.GridList.ListItems(i).ListSubItems(NOTES_OBJ).Bold = True
        'Else
        '  MabseF_List.GridList.ListItems(i).ListSubItems(NOTES_OBJ).Text = "X"
        '  MabseF_List.GridList.ListItems(i).ListSubItems(NOTES_OBJ).Bold = True
        'End If
      Else
        MabseF_List.GridList.ListItems(i).ListSubItems(NOTES_OBJ).Text = ""
      End If
    End If
  Next i
  
  If oForm.goUnload Then
    Unload oForm
    Set oForm = Nothing
  End If
End Sub

Private Sub mnuEditNotesMenu_Click()
  Dim oForm As Object
  Dim i As Long
  
  Set oForm = New MabseF_Notes
  oForm.setForm GbOggSelNotes
  oForm.Show vbModal
  
  'setta la listview a video
  For i = 1 To MabseF_List.GridList.ListItems.count
    If MabseF_List.GridList.ListItems(i).Selected = True Then
      If Trim(oForm.newNote) <> "" Then
        MabseF_List.GridList.ListItems(i).ListSubItems(NOTES_OBJ).Text = "X"
        MabseF_List.GridList.ListItems(i).ListSubItems(NOTES_OBJ).Bold = True
      Else
        MabseF_List.GridList.ListItems(i).ListSubItems(NOTES_OBJ).Text = ""
      End If
    End If
  Next i
  
  If oForm.Unload = vbYes Then
    Unload oForm
    Set oForm = Nothing
  End If
End Sub

Private Sub mnuExit_Click()
  fixCtrl
  End
End Sub

Private Sub mnuExternalCompare_Click()
  'Avvia un editor compare esterno
  ExternalEdit GbCompareEditor
End Sub

Private Sub mnuExternalEditor_Click()
  'Avvia un editor esterno
  ExternalEdit GbExternalEditor
  'DLLFunzioni.Show_TextEditor GbIdOggetto, True, , MabseF_List
End Sub

Private Sub mnuEXTTextEditor_Click()
  runMenuClick "MAIN", "OBJ_EDIT_EXTEDT"
End Sub

Private Sub mnuHelpTopics_Click()
  fHandleFile GbPathPrd & "\Doc\" & c_FileHelp, vbMaximizedFocus
End Sub

Private Sub mnuImportCicsDFHCSDUP_Click()
  'Me.Move Screen.Width / 4, Screen.Height / 4, 1, 1 'ALE.
  On Error GoTo parseErr
  dialogFileName = LoadCommDialog(True, "Select a DFHCSDUP file", "All Files", "*")
      
  If Len(dialogFileName) Then
    Screen.MousePointer = vbHourglass
    DllParser.parseDFHCSDUP (dialogFileName)
    Screen.MousePointer = vbNormal
  End If
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & ERR.Description
End Sub

Private Sub mnuImportCicsPCT_Click()
  'Me.Move Screen.Width / 4, Screen.Height / 4, 1, 1 'ALE.
  On Error GoTo parseErr
  dialogFileName = LoadCommDialog(True, "Select a PCT file", "All Files", "*")
      
  If Len(dialogFileName) Then
    Screen.MousePointer = vbHourglass
    DllParser.parsePCT (dialogFileName)
    Screen.MousePointer = vbNormal
  End If
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & ERR.Description
End Sub

Private Sub mnuImportContentsProc_Click()
  'Me.Move Screen.Width / 4, Screen.Height / 4, 1, 1 'ALE.
  On Error GoTo parseErr
  dialogFileName = LoadCommDialog(True, "Select a Catalog file", "All Files", "*")
      
  If Len(dialogFileName) Then
    Screen.MousePointer = vbHourglass
    DllParser.parseCONTENTS (dialogFileName)
    Screen.MousePointer = vbNormal
  End If
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & ERR.Description
End Sub

Private Sub mnuImportImsCat_Click()
  'Me.Move Screen.Width / 4, Screen.Height / 4, 1, 1 'ALE.
  On Error GoTo parseErr
  dialogFileName = LoadCommDialog(True, "Select a Catalog file", "All Files", "*")
      
  If Len(dialogFileName) Then
    Screen.MousePointer = vbHourglass
    DllParser.parseAPPLCTN (dialogFileName)
    Screen.MousePointer = vbNormal
  End If
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & ERR.Description
End Sub

Private Sub mnuImportCicsFCT_Click()
  'Me.Move Screen.Width / 4, Screen.Height / 4, 1, 1 'ALE.
  On Error GoTo parseErr
  dialogFileName = LoadCommDialog(True, "Select a FCT file", "All Files", "*")
      
  If Len(dialogFileName) Then
    Screen.MousePointer = vbHourglass
    DllParser.parseFCT (dialogFileName)
    Screen.MousePointer = vbNormal
  End If
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & ERR.Description
End Sub

Private Sub mnuMapEd_Click()
  runMenuClick "MAIN", "OBJ_EDIT_MAP"
End Sub

Private Sub mnuMapEditor_Click()
  runMenuClick "MAIN", "OBJ_EDIT_MAP"
End Sub

Private Sub mnuNewProject_Click()
  runShortcuts "MAIN", "PRJ_NEW"
End Sub

Private Sub mnuObjectProperties_Click()
  runShortcuts "MAIN", "OBJ_PROPERTIES"
End Sub

Private Sub mnuOpenConnection_Click()
  runShortcuts "MAIN", "COM_OPENCLOSE_CONNECTION"
End Sub


Private Sub mnuOpenPrj_Click()
  'SQ capire la diff. di tempo...
  'SQ runMenuClick "MAIN", "PRJ_OPEN_FAST"
  runMenuClick "MAIN", "PRJ_OPEN_SLOW"
End Sub

Private Sub mnuOpenPrjRebuild_Click()
  runMenuClick "MAIN", "PRJ_OPEN_SLOW"
End Sub

Private Sub mnuParseObjs_Click()
  mnuParseObjsMenu_Click
End Sub

Private Sub mnuParseObjs2_Click()
  mnuParseObjsMenu2_Click
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''
'MF - 17/07/07
'Gestione digitazione ESC dal parser
''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub mnuParseObjsMenu_Click()
  'Parser degli oggetti selezionati
  Dim i As Long, countsel As Long
  Dim cParam As Collection
  Dim wResp As Variant
  DllParser.PsFinestra.ListItems.Add , , "Press ESC key to interrupt the job"
   
  On Error GoTo errorHandler

  If DLLFunzioni.FnProcessRunning Then
    DLLFunzioni.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
   
  Screen.MousePointer = vbHourglass
   
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  countsel = 0
  '23-01-06: faceva dentro il parsing 1000 volte le stesse cose...
  DllParser.AttivaFunzione "INIT_PARSING", cParam
  MabseF_List.FlagStop = False
  For i = 1 To MabseF_List.GridList.ListItems.count
    If MabseF_List.GridList.ListItems(i).Selected Then
      countsel = countsel + 1
      MabseF_List.SelPars = countsel
      Set cParam = New Collection
      cParam.Add CLng(MabseF_List.GridList.ListItems(i).Text)
      DLLFunzioni.FnActiveWindowsBool = False
      DllParser.AttivaFunzione "PARSE_OBJECT_FIRST_LEVEL", cParam
      DLLFunzioni.FnActiveWindowsBool = True
      'MF - 17/07/07 Gestione uscita dal parser con ESC
      If MabseF_List.FlagStop Then
        wResp = DLLFunzioni.Show_MsgBoxError("PB00Q")
        If wResp = vbYes Then
          Exit For
        Else
          MabseF_List.FlagStop = False
          Screen.MousePointer = vbHourglass
        End If
      End If
    End If
  Next i
  MabseF_List.FlagStop = False
   
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
   
  Screen.MousePointer = vbDefault
   
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "ML04E"
End Sub

Public Sub mnuParseObjsMenu2_Click()
  'Parser degli oggetti selezionati
  Dim i As Long
  Dim cParam As Collection
  Dim countsel As Long
  If DLLFunzioni.FnProcessRunning Then
     DLLFunzioni.Show_MsgBoxError "FB01I"
     Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  
   '23-01-06: faceva dentro il parsing 1000 volte le stesse cose...
  DllParser.AttivaFunzione "INIT_PARSING", cParam
  For i = 1 To MabseF_List.GridList.ListItems.count
    If MabseF_List.GridList.ListItems(i).Selected Then
      countsel = countsel + 1
      MabseF_List.SelPars = countsel
      Set cParam = New Collection
      cParam.Add CLng(MabseF_List.GridList.ListItems(i).Text)
      DLLFunzioni.FnActiveWindowsBool = False
      DllParser.AttivaFunzione "PARSE_OBJECT_SECOND_LEVEL", cParam
      DLLFunzioni.FnActiveWindowsBool = True
    End If
  Next i
  
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
End Sub

Private Sub mnuPrints_Click()
   runShortcuts "BASE", "PRJ_PRINTS"
End Sub

Private Sub mnuProperties_Click()
  Dim oldFinCheck As Boolean
'  Dim wPrc As String
   
  Elimina_Flickering Me.hwnd
   
'  If Trim(OggSel.Estensione) = "" Then
'    wPrc = OggSel.Directory_Input & "\" & OggSel.Nome
'  Else
'    wPrc = OggSel.Directory_Input & "\" & OggSel.Nome & "." & OggSel.Estensione
'  End If
   
'  If Dir(wPrc, vbNormal) = "" Then
'    DLLFunzioni.Show_MsgBoxError "MP01I"
'    Exit Sub
'  End If
   
  'visualizza le propriet� dell'oggetto Selezionato
  If Trim(OggSel.Nome) = "" Or Trim(OggSel.directory_input) = "" Then
    DLLFunzioni.Show_MsgBoxError "MP02I"
  Else
    Unload MabseF_Log
    oldFinCheck = GbFinImCheck
    GbFinImCheck = False
    'MF MabseF_Prj.ResizeAll
    
    DLLFunzioni.Show_ObjectProperties OggSel, MabseF_List
      
    GbFinImCheck = oldFinCheck
    'MF:
    MabseF_Prj.ResizeAll
  End If
   
  Terminate_Flickering
End Sub

Private Sub mnuPropertiesPrj_Click()
  runShortcuts "BASE", "PRJ_INFO"
End Sub

Private Sub mnuRefreshRC_Click()
  mnuUpdateObj_Click
End Sub

Private Sub mnuRestoreObjects_Click()
  ' Ma 08/08/2007 : Restore senza Traccia del riconoscimento
  IsTrace = False
  runShortcuts "MAIN", "OBJ_RESTORE"
End Sub

Private Sub mnuRestoreObjs_Click()
  ' Ma 08/08/2007 : Restore senza Traccia del riconoscimento
  IsTrace = False
  runShortcuts "MAIN", "OBJ_RESTORE"
End Sub

' Ma 08/08/2007 : Restore con Traccia del riconoscimento
Private Sub mnuRestoreTrace_Click()
  IsTrace = True
  runShortcuts "MAIN", "OBJ_RESTORE"
End Sub

' Ma 08/08/2007 : Restore con Traccia del riconoscimento
Private Sub mnuRestoreTracemnu_Click()
  IsTrace = True
  runShortcuts "MAIN", "OBJ_RESTORE"
End Sub

Private Sub mnuRevWeb_Click()
  'runMenuClick fraToolbar.Buttons(24).ButtonMenus(2)
  Attiva_Funzione "MAIN", New Collection, "PRD_WEBSITE"
End Sub

Private Sub mnuSysObjDecl_Click()
  runMenuClick "BASE", "PRJ_SYSOBJ_DECLARE"
End Sub

Private Sub mnuConfigEnv_Click()
  runMenuClick "BASE", "PRJ_ENVIRONMENT"
End Sub

Private Sub mnuTextEd_Click()
  If InStr(1, OggSel.Nome, "$") > 0 Then
    DLLFunzioni.Show_MsgBoxError "MP03I"
    Exit Sub
  End If
  
  runShortcuts "OBJ_EDIT", "MAIN"
End Sub

Private Sub mnuTextEditor_Click()
  If Not (MabseF_List.GridList.SelectedItem Is Nothing) Then
    MabseF_Prj.runShortcuts "MAIN", "OBJ_EDIT_TEXT"
  End If
End Sub

Public Sub AttivaFunzioneMain(TagFunction As String, Optional cParam As Collection, Optional boolRefresh As Boolean)
  Dim i As Long, wRetWeb As Long
  Dim bCompile As Boolean
  Dim nomePrj As String
  Dim FuncActive As Boolean
  Dim Connection As Boolean
  Dim toolTipStatus As String
  Dim annulla As Boolean, oldFinCheck As Boolean
  Dim cancella As Long
  Dim oForm As Form
  Dim NumFile As Integer
   
  On Error GoTo errorHandler
   
  If Not FuncActive Then
    Select Case UCase(Trim(TagFunction))
      Case "PRJ_OPEN", "PRJ_OPEN_FAST", "PRJ_OPEN_SLOW"
        DLLFunzioni.FnActiveWindowsBool = True
            
        MabseF_List.GridList.ListItems.Clear
      
        'Controlla il tipo di DB
        Set oForm = New MabseF_SelectDatabase
        Load oForm
        
        oForm.SetFormAndButtonText "Open i-4.Migration Project", "Open"
        If Not boolRefresh Then
          'AC
          If Not MabseF_SelectDatabase.OpenProject(namePrjLic) Then
            'prj NON OK
            DLLFunzioni.Show_MsgBoxError "ML00E"
            'CTRL
            fixCtrl
            End
          End If
        End If
        
        Connection = ADOCnt <> ""
            
        Clessidra True
        If prjAlreadyOpen And Connection And m_CreateYesNo Then
           
          DLLFunzioni.RemoveAllActiveWindows
          
          'MF 09/08/07 Commento tutto perch� il path C:\Customer\FTT � gi� valorizzato in ProjectPath
          nomePrj = Replace(ProjectName, ".mti", "")
         
          'INSERIMENTO PER NUOVO PERCORSO
          'MF 09/08/07 Sostituisco GbPathPrj con ProjectPath
          GbPathPrj = ProjectPath & "\" & nomePrj
         
          'Rende disponibile a tutti il file di log e le connessioni
          'MF 09/08/07 sostituisco nelle replace il nome del database DatabaseName con il nome del progetto projectname
          DLLFunzioni.FnFileLog = GbPathPrj & "\Documents\Log\" & Replace(Replace(ProjectName, ".mty", ""), ".mti", "") & ".log"
          Set DLLFunzioni.FnConnection = ADOCnt
         
          'Legge i Parametri tramite DLL Funzioni
          DLLFunzioni.Load_Parametri_Progetto
          
         
          'setta le variabili delle DLL contenetnti lo stato della connessione
          'MF 09/08/07 Posso anche non passargli DatabaseName visto che � globale
          Set_Connection_To_DLL Connection, DatabaseName
         
          ''''''''''''''
          'SQ 12-10-07
          ''''''''''''''
          If Len(namePrjLic) Then
            If Not checkProjectLicense(namePrjLic) Then
              'prj non OK
              DLLFunzioni.Show_MsgBoxError "ML00E"
              Unload Me
              'CTRL
              fixCtrl
              End
            End If
          End If
          
          'inizializza la toolbar
          MabseF_List.Filter_List.Visible = False
          MabseF_List.GridList.Visible = True
   
'''          If Not boolRefresh And InStr(1, StatusBar1.Panels(1).Text, Trim("[" & NomeDB & "]")) <> 0 Then
'''            DLLFunzioni.Show_MsgBoxError "MP06I"
'''            Terminate_Flickering
'''            Clessidra False
'''            DLLFunzioni.FnActiveWindowsBool = False
'''            Exit Sub
'''          End If
               
          'Carica la lista dalla tabella BS_Oggetti
          If UCase(Trim(TagFunction)) = "PRJ_OPEN" Or UCase(Trim(TagFunction)) = "PRJ_OPEN_FAST" Then
            'MF 09/08/07 Posso anche non passargli DatabaseName visto che � globale
            Carica_E_controlla_Lista_Progetto DatabaseName, Connection, "FAST"
          ElseIf UCase(Trim(TagFunction)) = "PRJ_OPEN_SLOW" Then
            Carica_E_controlla_Lista_Progetto DatabaseName, Connection, "SLOW"
          End If
          
          'carica le info prj
          Carica_Array_InfoPRJ
  
          'Setta Toolbar, Statusbar ecc... Nel mdi
          'MF 09/08/07 Posso anche non passargli DatabaseName visto che � globale
          Setting_FormMdi DatabaseName, toolTipStatus
  
          If DLLDli2RdbMsExist Then
            'Carica i parametri
            DLLDli2RdbMs.Carica_Parametri GbArrayDBD, GbArrayORWord, GbArrayNewWord
          End If
                 
          If DllDataManExist Then
            DllDataM.Carica_Parametri_DataManager
          End If
  
          'aggiorna le variabili delle DLL
          Aggiorna_Variabili_DLL
  
          'controlla le autorizzazioni
          Controlla_Autorizzazioni
  
          GbCoolMenu_Enabled = True
          
          MabseF_Prj.StatusBar1.Panels(3).Text = "Object number: " & InfoPRJ(0).NumObj
        End If 'Connection = True

        Clessidra False
        DLLFunzioni.FnActiveWindowsBool = False
            
      Case "PRJ_NEW"
         
        ' AC 05/10/2007
        If Not GbCheckObj = "" Then
          MsgBox "This i-4.Migration version is only for this project. Impossible to create a new project!"
          Exit Sub
        End If
          
        Dim Scelta As Long
        Dim r As Recordset
          
        'MF 01/08/07
        DLLFunzioni.RemoveAllActiveWindows
        DLLFunzioni.FnActiveWindowsBool = True
        
        'Scelta = DLLFunzioni.Show_MsgBoxError("MP00Q") 'ALE
        Scelta = 6 'ALE
        If Scelta = 6 Then
          MabseF_List.GridList.ListItems.Clear
           
          Set oForm = New MabseF_SelectDatabase
          Load oForm
          oForm.SetFormAndButtonText "Create New Database", "New"
           
          ' Ma 16/04/2008: Bypasso la form MabseF_SelectDatabase
          'oForm.Show vbModal
          oForm.CreateNewProject
          Connection = False
          If ADOCnt <> "" Then Connection = True
           
           'ALE chiudere tutte le finestre delle ddl attive 25/07/2006
        End If
          
        If Connection And m_CreateYesNo Then
          nomePrj = Replace(DatabaseName, ".mty", "")
          
          'INSERIMENTO PER NUOVO PERCORSO
          GbPathPrj = ProjectPath & "\" & nomePrj
          
          'Crea le directory di output dli e documents ecc...
          Create_DirectoryOut_Progetto GbPathPrj
           
          'Rende disponibile a tutti il file di log e le connessioni
          DLLFunzioni.FnFileLog = GbPathPrj & "\Documents\Log\" & Replace(Replace(DatabaseName, ".mty", ""), ".mti", "") & ".log"
          Set DLLFunzioni.FnConnection = ADOCnt
           
          'setta le variabili delle DLL contenetnti lo stato della connessione
          Set_Connection_To_DLL Connection, DatabaseName
           
          copia_new_prjDB
          copia_new_InputPrj
           
          If ADOCnt.State = adStateClosed Then
          Else
            ADOCnt.Close
          End If
          ADOCnt.Open
             
          'carica la treeview
          Carica_TreeView_Progetto MabseF_List.PrjTree
           
          'inserisce la data di creazione del DB Nel Database
          Write_Info_Nuovo_Progetto DatabaseName
                          
          'Legge i parametri del progetto
          DLLFunzioni.Load_Parametri_Progetto
            
          'Carica i parametri del DLI2RDBMS se installato
          If DLLDli2RdbMsExist Then
            DLLDli2RdbMs.Carica_Parametri GbArrayDBD, GbArrayORWord, GbArrayNewWord
          End If
           
          'carica le info prj
          Carica_Array_InfoPRJ
           
          'setta la status bar
          toolTipStatus = "Creation Date: " & InfoPRJ(0).DateCreation
           
          StatusBar1.Panels(1).ToolTipText = toolTipStatus
           
          StatusBar1.Panels(1).Text = "[" & DatabaseName & "]"
           
          Aggiorna_Variabili_DLL
          Set DLLFunzioni.FnDatabase = DbPrj
           
          'Abilita tutti i bottoni
          ''SQ tmp Abilita_Disabilita_CoolMenu True

          GbCoolMenu_Enabled = True
            
          DllBase.AttivaFunzione "Show_Parameters", New Collection, MabseF_List
            
          MabseF_Prj.StatusBar1.Panels(3).Text = "Object number: 0"
          MabseF_Prj.StatusBar1.Panels(2).Text = "Creation Date: " & InfoPRJ(0).DateCreation
            
          'Abilita i bottoni ecc
          Setting_FormMdi DatabaseName, toolTipStatus
        End If 'Connection = True
          
        DLLFunzioni.FnActiveWindowsBool = False
            
      'S
      Case "PRJ_ADD_OBJECTS_TMP"
        'aggiunge i file nel database
        annulla = Aggiungi_Oggetti_In_TMP(MabseF_List.PBar1)
           
      Case "PRJ_ADD_OBJECTS"
        DLLFunzioni.FnActiveWindowsBool = True
        Terminate_Flickering
        
        MabseF_Log.LstLog.ListItems.Clear
        
        m_ClassifyObj_For_All = False
        
        'aggiunge i file nel database
        NoPrintLog = ""
        annulla = Aggiungi_Oggetti_In_DB(MabseF_List.PBar1)
        If UCase(DLLFunzioni.LeggiParam("NOPRINTCHAR")) = "YES" And Len(NoPrintLog) Then
          NoPrintLog = "  ID  |                            PROGRAM NAME          |  ROW " & vbCrLf & _
                       "----------------------------------------------------------------" & vbCrLf & _
                       NoPrintLog & vbCrLf & _
                       "----------------------------------------------------------------" & vbCrLf & _
                       "(Check file '" & GbPathPrj & "\Documents\Log\NoPrintChar.log')"
          DLLFunzioni.Show_ShowText NoPrintLog, DLLFunzioni.FnNomeProdotto
          NumFile = FreeFile
          Open GbPathPrj & "\Documents\Log\NoPrintChar.log" For Output As NumFile
          Print #NumFile, NoPrintLog
          Close NumFile
        End If
        CurPgm = DllBase.Conta_record_Obj_In_Base_Al_Tipo("ALL")
        MabseF_Prj.StatusBar1.Panels(3).Text = "Object Number: " & CurPgm
        
        If Not annulla Then
          Clessidra False
          DLLFunzioni.FnActiveWindowsBool = False
          Exit Sub
        End If
  
        'aggiorna InfoPRJ
        Aggiorna_InfoPRJ
  
        'assgna la stringa alla status bar
        MabseF_Prj.StatusBar1.Panels(2).Text = "Creation Date: " & InfoPRJ(0).DateCreation
  
        'aggiorna la TreeView
        Carica_TreeView_Progetto MabseF_List.PrjTree, , "FAST"
  
        'Cancella la lista
        MabseF_List.GridList.ListItems.Clear
  
        'espande la treeview
        'espande la struttura
        If TreePrj_Index <> 0 Then
          If MabseF_List.PrjTree.Nodes(TreePrj_Index).Key = "MB" Then
            For i = 1 To MabseF_List.PrjTree.Nodes.count
              MabseF_List.PrjTree.Nodes(i).Expanded = True
            Next i
          End If
        End If
  
        'salva la struttura in memoria
        Salva_Struttura_TreeView_In_DB MabseF_List.PrjTree
        
        MabseF_Menu.CmdUty_Click MabseF_Menu.ButtonSelIndex
        
        DLLFunzioni.FnActiveWindowsBool = False
            
      Case "LST_FILTER_CONF"
        'Seleziona Filtro
        Seleziona_Filtro_Selezione MabseF_List.GridList, MabseF_List.Filter_List, IIf(filterSelection.Checked, tbrPressed, tbrUnpressed), 23
         
      Case "LST_FILTER_EXECUTE"
        'esegue la Query di filtro
        'Seleziona_Filtro_Selezione MabseF_List.GridList, MabseF_List.Filter_List, fraToolbar.Buttons(21).Value, 21
        Execute_Filter MabseF_List.Filter_List, MabseF_List.GridList, MabseF_List.TotalObj, IIf(filterSelection.Checked, tbrPressed, tbrUnpressed)
        MabseF_List.txtFilter.Visible = False
        MabseF_List.Filter_List.Visible = False
   
      Case "LST_ORDERDOWN"
        'ordina in base alla colonna selezionata
        Ordina_Lista MabseF_List.GridList, lvwDescending
   
      Case "LST_ORDERUP"
        'ordiana in base alla colonna selezionata
        Ordina_Lista MabseF_List.GridList, lvwAscending

      Case "OBJ_DELETE"
        DLLFunzioni.FnActiveWindowsBool = True
        
        If GbIdOggetto <= 0 Then
          DLLFunzioni.Show_MsgBoxError "MP02I"
          Screen.MousePointer = vbDefault
        Else
          Elimina_Flickering Me.hwnd
     
          'Elimina gli oggetti selezionati
          cancella = Elimina_Oggetti_Selezionati(MabseF_List.GridList, MabseF_List.TotalObj)
    
          'Scrive nel Pannello Lo status degli oggetti
          CurPgm = DllBase.Conta_record_Obj_In_Base_Al_Tipo("ALL")
          MabseF_Prj.StatusBar1.Panels(3).Text = "Object numbers: " & CurPgm
    
          If cancella = vbYes Then
            'se � un DBD chiamo il DataManager
            For i = 1 To MabseF_List.GridList.ListItems.count
              If MabseF_List.GridList.ListItems(i).Selected = True And Trim(UCase(MabseF_List.GridList.ListItems(i).ListSubItems(TYPE_OBJ))) = "DBD" Then
                DllDataM.DmIdOggetto = Val(MabseF_List.GridList.ListItems(i).Text)
                DllDataM.AttivaFunzione "DEL_TO_PROJECT"
              End If
            Next i
          End If
            
          'aggiorna InfoPRJ
          Aggiorna_InfoPRJ
    
          'assgna la stringa alla status bar
          MabseF_Prj.StatusBar1.Panels(2).Text = "Creation Date: " & InfoPRJ(0).DateCreation
    
          Terminate_Flickering
        End If
        DLLFunzioni.FnActiveWindowsBool = False
         
      Case "OBJ_RESTORE"
        
        Dim wRet As Long
        DLLFunzioni.FnActiveWindowsBool = True
         
        If Trim(OggSel.Nome) = "" Or Trim(OggSel.directory_input) = "" Then
          DLLFunzioni.Show_MsgBoxError "MP02I"
        Else
          wRet = DLLFunzioni.Show_MsgBoxError("MP01Q")
          If wRet = vbYes Then
            Restore_Objects
          End If
        End If
         
        DLLFunzioni.FnActiveWindowsBool = False
         
      Case "OBJ_CHANGE_TYPE"
        DLLFunzioni.FnActiveWindowsBool = True
         
        If GbIdOggetto <= 0 Then
          DLLFunzioni.Show_MsgBoxError "MP02I"
          Screen.MousePointer = vbDefault
        Else
          MabseF_ClassObj.Show vbModal
          Clessidra True
          'Cambia il tipo agli oggetti selezionati
          If Len(MabseF_ClassObj.Tipo) Then
            Cambia_Type_Oggetti_Selezionati MabseF_List.PrjTree, MabseF_List.GridList ', MabseF_ClassObj.Tipo, MabseF_ClassObj.Level
            'Riaggiorna La treeview e la lista e salva la sua struttura
            Salva_Struttura_TreeView_In_DB MabseF_List.PrjTree, False
            'Clear della lista
            MabseF_List.GridList.ListItems.Clear
          End If
          Clessidra False
        End If
         
        DLLFunzioni.FnActiveWindowsBool = False
         
      Case "OBJ_PROPERTIES"
        Elimina_Flickering Me.hwnd
      
        'visualizza le propriet� dell'oggetto Selezionato
        If Trim(OggSel.Nome) = "" Or Trim(OggSel.directory_input) = "" Then
          DLLFunzioni.Show_MsgBoxError "MP02I"
          'Exit Sub
        Else
          Unload MabseF_Log
          oldFinCheck = GbFinImCheck
          GbFinImCheck = False
          MabseF_Prj.ResizeAll
  
          DLLFunzioni.Show_ObjectProperties OggSel, MabseF_List
          
          GbFinImCheck = oldFinCheck
          'MabseF_Prj.ResizeAll
        End If
       
      Case "COM_OPENCLOSE_CONNECTION"
        Screen.MousePointer = vbHourglass
        
        Select Case wSokComp.State
          Case sckClosed
            wSokComp.RemoteHost = DLLFunzioni.Fn_IpServer_Comp
            wSokComp.RemotePort = DLLFunzioni.Fn_PortServer_Comp
            wSokComp.Connect
        
          Case sckClosing
            MabseF_Log.LstLog.ListItems.Add , , Now & " --> Connection Closing...WAIT PLEASE!!!"
              
            Do While wSokComp.State = sckClosing
              DoEvents
              wSokComp.Close
              If wSokComp.State = sckClosed Then
                GbConnMFE = False
                DLLFunzioni.FnCompilerConnection = GbConnMFE
                fraToolbar.Buttons(4).Value = tbrUnpressed
                
                MabseF_Log.LstLog.ListItems.Add , , Now & " --> Connection Closed."
                Exit Do
              End If
            Loop
              
          Case sckConnected
            'Chiude
            wSokComp.Close
            GbConnMFE = False
            DLLFunzioni.FnCompilerConnection = GbConnMFE
            fraToolbar.Buttons(4).Value = tbrUnpressed
            MabseF_Log.LstLog.ListItems.Add , , Now & " --> Connection Closed."
          
          Case sckConnecting
           
          Case sckConnectionRefused
            'Chiude
            wSokComp.Close
            GbConnMFE = False
            DLLFunzioni.FnCompilerConnection = GbConnMFE
            fraToolbar.Buttons(4).Value = tbrUnpressed
              
          Case sckError
            'Chiude
            wSokComp.Close
            GbConnMFE = False
            DLLFunzioni.FnCompilerConnection = GbConnMFE
            fraToolbar.Buttons(4).Value = tbrUnpressed
            
        End Select
        Screen.MousePointer = vbDefault
            
      Case "COM_COMPILE_OBJS"
        If GbConnMFE = False Then
          DLLFunzioni.Show_MsgBoxError "MC02I"
          bCompile = False
        Else
          bCompile = True
        End If
        
        If DLLFunzioni.Fn_Win2Unix_ProjectPath = "" Then
          DLLFunzioni.Show_MsgBoxError "MC01I"
          bCompile = False
        Else
          bCompile = True
        End If
        
        If bCompile = True Then
          Start_Compilazione_Objects
        End If
       
      Case "OBJ_EDIT", "OBJ_EDIT_TEXT"
        If GbIdOggetto = 0 Then
          DLLFunzioni.Show_MsgBoxError "MP02I"
        Else
          Unload MabseF_Log
          oldFinCheck = GbFinImCheck
          GbFinImCheck = False
          'MabseF_Prj.ResizeAll 'ale
          'aggiungere parametro formparent
          DLLFunzioni.Show_TextEditor GbIdOggetto, , , MabseF_List
              
          GbFinImCheck = oldFinCheck
          MabseF_Prj.ResizeAll
        End If
      
      Case "OBJ_EDIT_MAP"
        If GbIdOggetto = 0 Then
          DLLFunzioni.Show_MsgBoxError "MP02I"
        Else
          'SQ - Pezza: controllo solo per MFS!!!
          If MabseM_Prj.CheckMapToEdit(GbIdOggetto) Or MabseF_List.GridList.SelectedItem.ListSubItems(TYPE_OBJ) = "BMS" Then
            Unload MabseF_Log
            oldFinCheck = GbFinImCheck
            GbFinImCheck = False
            MabseF_Prj.ResizeAll
    
            DLLFunzioni.Show_MapEditor GbIdOggetto
            
            GbFinImCheck = oldFinCheck
            MabseF_Prj.ResizeAll
          Else
            MsgBox "Please, parse the map before using the Map Editor", vbInformation, "Map not parsed"
          End If
        End If
       
      Case "OBJ_EDIT_EXTEDT"
        'Avvia un editor esterno
        If GbIdOggetto = 0 Then
          DLLFunzioni.Show_MsgBoxError "MP02I"
        Else
          DLLFunzioni.Show_TextEditor GbIdOggetto, True, , MabseF_List
        End If
              
      Case "PRD_ABOUT"
        MabseF_About.Show vbModal
         
      Case "PRD_WEBSITE"
        wRetWeb = ShellExecute(Me.hwnd, "Open", GbReviserWebSite, "", app.path, 1)
      
        If wRetWeb < 32 Then
          ERR.Raise 2001
        End If
         
      Case "PRD_HELP"
        'Controlla se c'� l'help
        'DA FARE
         
      Case "PRJ_EXIT"
        fixCtrl
        End
       
      Case Else
        fixCtrl
         
    End Select
  End If
  
  If Len(nomePrj) Then
    DllParser.PsTipoEstensione = LeggiParam("PS_FILE_EXTENS")
  End If
  DLLFunzioni.FnActiveWindowsBool = False
  
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "RC0002"
End Sub

Public Function Aggiungi_Oggetti_In_TMP(Pb As ProgressBar) As Boolean
  Dim TbOggetti As Recordset
  Dim tbtmp As Recordset
  Dim Scelta As Long
  Dim FirstScelta As Long
  Dim AreaApp As String
  Dim i As Long, k As Long
  Dim wDir As String, wNom As String, wTip As String
  Dim wRighe As Long
  Dim wExt As String, wDirMadre As String
  Dim wIdObjMax As Long
  Dim AutOK As Boolean, YesALL As Boolean, noALL As Boolean, wAddNew As Boolean
  Dim rAut As Recordset
  Dim cParam As Collection
  Dim Existe As Boolean
  Dim sFile() As String
  Dim oMessage As Object
  
  On Error GoTo EH
  
  MabseF_List.Refresh
  MabseF_Log.Refresh
  MabseF_Menu.Refresh
  MabseF_Prj.ResizeAll
  
  FirstScelta = Scelta
  
  
  Pb.Value = 0
  Pb.Visible = True
  
  'Chiama la nuova common dialog
  Dim oDialog As New MabseF_CommDialog
  
  oDialog.FilterIndex = 12
  oDialog.Filter = "Cobol Sources(*.cbl) |*.cbl|" _
                       & "Assembler Sources(*.asm) |*.asm|" _
                       & "Cobol Copy(*.cpy) |*.cpy|" _
                       & "CICS Maps(*.bms) |*.bms|" _
                       & "IMS Maps(*.mfs) |*.mfs|" _
                       & "JCL(*.jcl) |*.jcl|" _
                       & "PROCEDURE(*.prc) |*.prc|" _
                       & "PARAMETERS(*.prm) |*.prm|" _
                       & "DBD(*.dbd) |*.dbd|" _
                       & "PSB(*.psb) |*.psb|" _
                       & "DDL DB2(*.ddl) |*.ddl|" _
                       & "All Files(*.*) |*.*|"
                       
  oDialog.start_Path = ProjectPath 'lastOpen_dir
  
  
  oDialog.Show vbModal
  
  Clessidra True
  'Recupera l,'array dei file
  sFile = oDialog.SelectedFileName
  
  
  Unload oDialog
  Set oDialog = Nothing
  
  Clessidra False
   
  MabseF_List.Refresh
  MabseF_Menu.Refresh
   
  'Controlla se sto facendo una ADD dalla directory del progetto
  If Empty_Array(sFile) Then
      Exit Function
  ElseIf InStr(1, sFile(0), ProjectPath, vbTextCompare) = 0 Then
    DLLFunzioni.Show_MsgBoxError "MP04I"
    Exit Function
  End If

  If UBound(sFile) Then
      Pb.Max = UBound(sFile)
      Pb.Value = 0
      
      'Recupera la directory Madre
      wDirMadre = sFile(0)
      
      Set tbtmp = DLLFunzioni.Open_Recordset("select * From tmp_oggetti")
      For i = 1 To UBound(sFile)
         wDirMadre = Mid(sFile(i), 1, InStrRev(sFile(i), "\") - 1) 'ALE
         Clessidra True

         'Nome:
         wNom = Mid(sFile(i), InStrRev(sFile(i), "\") + 1, Len(sFile(i)) - InStrRev(sFile(i), "\"))  'ALE sFile(i)
         'Tipo
         'wTip = DLLFunzioni.getTipoOggettoByExtension(DLLFunzioni.getExtensionFile(wNom))
         wNom = UCase(wNom)
         wExt = Replace(UCase(Trim(DLLFunzioni.getExtensionFile(wNom))), "UNK", "")
         wDir = Crea_Directory_Parametrica(wDirMadre)
         '''''wRighe = Conta_Righe_Sorgente(Trim(wDirMadre & "\" & wNom))
         
         'SQ: modificato il controllo sul percorso (sbagliava... con lo "\"...)
         Dim wPdir As String
         wPdir = Replace(wDir, "$\", "$\" & Replace(DatabaseName, ".mty", "") & "\")
         Set TbOggetti = DLLFunzioni.Open_Recordset("select * From BS_Oggetti where nome = '" & Replace(wNom, "." & wExt, "") & "'")

         If TbOggetti.RecordCount Then
            tbtmp!IdOggetto = TbOggetti!IdOggetto
            tbtmp!Notes = TbOggetti!Notes
         Else
            tbtmp!IdOggetto = 0
            tbtmp!Notes = ""
         End If
         tbtmp!Nome = Trim(Replace(wNom, "." & wExt, ""))
         tbtmp!directory_input = Trim(wDir)
 
         tbtmp.Update
        
         TbOggetti.Close
         DoEvents

         Pb.Value = Pb.Value + 1
         Pb.Refresh


         Clessidra False
      Next i
      tbtmp.Close
   End If

   Pb.Value = 0
   
   
   Exit Function
EH:
   DLLFunzioni.Show_MsgBoxError "MP00E", "MabseM_Prj", "Aggiungi_Oggetti_In_TMP", ERR.Number, ERR.Description
   ERR.Clear
   Resume Next
End Function

Public Sub copia_new_prjDB()
  Dim fso As New FileSystemObject
  
  'MF 09/08/07
  'fso.CopyFile GbPathPrd & "\" & SYSTEM_DIR & "\prj.mty", GbPathPrj & ".mty", True
  fso.CopyFile GbPathPrd & "\" & SYSTEM_DIR & "\prj.mty", ProjectPath & "\" & DatabaseName, True
  'Do Until FileExists(GbPathPrj & ".mty")
  Do Until FileExists(ProjectPath & "\" & DatabaseName)
    DoEvents
  Loop
  'Mauro: 14/03/2006 - Rendo modificabile il db di progetto
  SetFileAttributes ProjectPath & "\" & DatabaseName, 128
End Sub

Public Sub copia_new_InputPrj()
  Dim fso As New FileSystemObject
  
  fso.CopyFolder GbPathPrd & "\" & SYSTEM_DIR & "\Input-Prj", GbPathPrj & "\", True
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''
' Aggiorna Campi in lista degli ogggetti selezionati
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub mnuUpdateObj_Click()
  Dim Ind As Long
  Dim idObj As Long
  
  For Ind = 1 To MabseF_List.GridList.ListItems.count
    If MabseF_List.GridList.ListItems(Ind).Selected Then
       idObj = MabseF_List.GridList.ListItems(Ind).Text
       DllParser.UpdateErrLevel idObj, Ind ', PsHideIgnore
    End If
  Next Ind
End Sub

Public Sub runShortcuts(Tag As String, Key As String)
  On Error GoTo ERR
  
  If DLLFunzioni.FnProcessRunning Then
    DLLFunzioni.Show_MsgBoxError "FB01I"
    Exit Sub
  End If

  'scrive ctrl
  fixCtrl

  'Lancia le funzioni
  Attiva_Funzione Tag, New Collection, Key

  Exit Sub
ERR:
  Screen.MousePointer = vbDefault

  Select Case ERR.Number
    Case 75
      DLLFunzioni.Show_MsgBoxError "MP03I", , , ERR.Number, ERR.Description

    Case Else
      DLLFunzioni.Show_MsgBoxError "MB99E", "MabseF_Prj", "fraToolbar.ButtonClick", ERR.Number, ERR.Description

  End Select

  Terminate_Flickering

  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
End Sub

Private Sub Setting_FormMdi(NomeDB As String, toolTipStatus As String)
  'Setta gli oggetti sul form mdi (status-bar,Toolbar ecc...)

  'setta la status bar
  toolTipStatus = "Path Project..."

  StatusBar1.Panels(1).ToolTipText = toolTipStatus

  StatusBar1.Panels(1).Text = "[" & NomeDB & "]"
  StatusBar1.Panels(2).Text = "Creation Date: " & InfoPRJ(0).DateCreation

  'Abilita tutti i bottoni
  Abilita_Disabilita_CoolMenu True

  'Abilita la toolbar
  ON_OFF_Button_ToolBar fraToolbar, True
  
  'Menu
  mnuAddObjects.Enabled = True
  mnuParamPrj.Enabled = True

  'Compiler
  'mnuCompilers.Enabled = True 'ALE 23/05/2006

  'Objects
  mnuObject.Enabled = True

  'Help
  mnuHelp.Enabled = True
End Sub

Private Sub Controlla_Autorizzazioni()
   'controlla le autorizzazioni
   'Ripristina i valori dei contatori
'   FreeSlotDBD = FreeSlotDBDReset
'   FreeSlotPGM = FreeSlotPGMReset
'
'   If DllBase.ControlSource_Total("CBL", "DBD") = False Then
'     MsgBox "You don't have the authorization to open this project...", vbExclamation, GbNomeProdotto
'     End
'   End If
'
'   MaxPGM = DllBase.UsMaxPGM
'   MaxDBD = DllBase.UsMaxDBD
'   Percent = DllBase.UsPercent
'
   'Controlla se la tabella parametri � cambiata
   'Modifica necessaria solo fino a che tutti i progetti saranno allineati
'   Dim wDbOk As Boolean
   
'   If GbFinImCheck = True Then
'      MabseF_Log.LstLog.ListItems.Add , , Now & " : Control Database Structure..."
'   End If
End Sub

Public Sub ResizeAll(Optional ByVal isFirstLoad As Boolean = False)
  Dim wApp01 As Long, wApp02 As Long
  
  'On Error Resume Next
  
  'Posiziona La finestra
  If isFirstLoad Then
    Me.Height = Screen.Height '(Screen.Height / 10) * 9
    Me.Width = Screen.Width '(Screen.Width / 10) * 9
    Me.Top = ((Screen.Height - Me.Height) / 2)
    Me.Left = ((Screen.Width - Me.Width) / 2)
  End If
  
  'Setta le variabili globali dello spazio per le finestre interne
  GbTop = 0
  GbLeft = 0
  GbWidth = Me.Width
  GbHeight = Me.Height

  'inizializza le variabili per centrare le form delle DLL
  GbTopForm = MabseF_Menu.Top + MabseF_Menu.Height 'SQ
  GbLeftForm = MabseF_Menu.Left 'SQ + MabseF_Menu.Width
  GbWidthForm = Me.ScaleWidth
  GbHeightForm = Me.ScaleHeight - MabseF_Menu.Height 'SQ
 
  GbHeightNoFinImm = GbHeightForm

  MabseF_Menu.Resize

  'Rivalorizza lo spazio a disposizione
  GbWidthForm = Me.ScaleWidth
  GbHeightForm = Me.ScaleHeight - MabseF_Menu.Height 'SQ

  If GbFinImCheck Then
     MabseF_Log.Resize

     GbHeightForm = GbHeightForm - MabseF_Log.Height
  Else
     Unload MabseF_Log

     GbHeightForm = Me.ScaleHeight
  End If
  
  ''''''''''''''''
  'LIST FRAME
  ''''''''''''''''
  MabseF_List.Top = GbTopForm
  MabseF_List.Left = GbLeftForm
  MabseF_List.Resize
  MabseF_List.Height = GbHeightForm
  MabseF_List.Width = GbWidthForm
  
  If MabseF_CompErrors.loaded Then
    MabseF_CompErrors.Move 0, 0, MabseF_List.Width, MabseF_List.Height
  End If

  'se le varie Dll sono attive gli passo i parametri di posizionamento form
  If GbDllBaseExist Then
    DllBase.BsHeight = GbHeightForm
    DllBase.BsLeft = GbLeftForm + 25
    DllBase.BsTop = GbTopForm + 25
    DllBase.BsWidth = GbWidthForm
  End If

  If GbDllParserExist Then
    DllParser.PsHeight = GbHeightForm
    DllParser.PsLeft = GbLeftForm + 25
    DllParser.PsTop = GbTopForm + 5
    DllParser.PsWidth = GbWidthForm
  End If

  If GbDllDataManExist Then
    DllDataM.DmHeight = GbHeightForm
    DllDataM.DmLeft = GbLeftForm + 25
    DllDataM.DmTop = GbTopForm + 25
    DllDataM.DmWidth = GbWidthForm
  End If

  If GbDLLDli2RdbMsExist Then
    DLLDli2RdbMs.drHeight = GbHeightForm
    DLLDli2RdbMs.drLeft = GbLeftForm + 25
    DLLDli2RdbMs.drTop = GbTopForm + 25
    DLLDli2RdbMs.drWidth = GbWidthForm
  End If

  If GbDllImsExist Then
    DLLIms.ImHeight = GbHeightForm
    DLLIms.ImLeft = GbLeftForm + 25
    DLLIms.ImTop = GbTopForm + 25
    DLLIms.ImWidth = GbWidthForm
  End If

  If GbDllToolsExist Then
    DLLTools.TsHeight = GbHeightForm
    DLLTools.TsLeft = GbLeftForm + 25
    DLLTools.TsTop = GbTopForm + 25
    DLLTools.TsWidth = GbWidthForm
  End If

  If GbDllAnalisiExist Then
    DLLDataAnalysis.AnHeight = GbHeightForm
    DLLDataAnalysis.AnLeft = GbLeftForm + 25
    DLLDataAnalysis.AnTop = GbTopForm + 25
    DLLDataAnalysis.AnWidth = GbWidthForm
  End If

  If GbDllFunzioniExist Then
    DLLFunzioni.FnHeight = GbHeightForm
    DLLFunzioni.FnLeft = GbLeftForm + 25
    DLLFunzioni.FnTop = GbTopForm + 25
    DLLFunzioni.FnWidth = GbWidthForm
    DLLFunzioni.FnHeightNoFinImm = GbHeightNoFinImm
  End If
  
  If GbDllVSAMExist Then
    DLLVSAM2RdbMs.drHeight = GbHeightForm
    DLLVSAM2RdbMs.drLeft = GbLeftForm + 25
    DLLVSAM2RdbMs.drTop = GbTopForm + 25
    DLLVSAM2RdbMs.drWidth = GbWidthForm
  End If
    
End Sub

Private Sub runMenuClick(Tag As String, Key As String)
  Elimina_Flickering Me.hwnd
  
  On Error GoTo ERR
   
  Attiva_Funzione Tag, New Collection, Key
      
  
  Terminate_Flickering
  Exit Sub

ERR:
  Select Case ERR.Number
    Case 75
      DLLFunzioni.Show_MsgBoxError "MP03I"
    
    Case 2001
      DLLFunzioni.Show_MsgBoxError "MB00E"
       
    Case Else
      DLLFunzioni.Show_MsgBoxError "MB99E", "MabseF_Prj", "fraToolbar.ButtonMenuClick", ERR.Number, ERR.Description
         
  End Select
  
  Terminate_Flickering
End Sub

Private Sub Carica_E_controlla_Lista_Progetto(NomeDB As String, Connection As Boolean, mTypeLoad As String)
  'setta le variabili delle DLL contenetnti lo stato della connessione
  Set_Connection_To_DLL Connection, NomeDB
  
  If Connection Then
    'controlla l'integrit� del DB

    'cancella la listview
    MabseF_List.GridList.ListItems.Clear
    
    'carica la treeview
    Carica_TreeView_Progetto MabseF_List.PrjTree, , mTypeLoad
  Else
    If Trim(NomeDB) <> "" Then
      MsgBox "I'm sorry!" & vbCrLf & "Database Name or Database Type is incorrect...", vbExclamation, GbNomeProdotto
    End If
  End If
End Sub

Public Sub NoMaxBox(f As MDIForm)
  Dim l As Long
  l = GetWindowLong(f.hwnd, GWL_STYLE)
  l = l And Not (WS_MAXIMIZEBOX)
  l = SetWindowLong(f.hwnd, GWL_STYLE, l)
End Sub

Public Sub NoMinBox(f As MDIForm)
  Dim l As Long
  l = GetWindowLong(f.hwnd, GWL_STYLE)
  l = l And Not (WS_MINIMIZEBOX)
  l = SetWindowLong(f.hwnd, GWL_STYLE, l)
End Sub

Private Sub MDIFormOnTop(f As MDIForm)
'    HWND_TOPMOST = -1
'    HWND_NOTOPMOST = -2
    
'    SWP_NOMOVE = &H2
'    SWP_NOSIZE = &H1
'    SWP_NOOWNERZORDER = &H200
'    SWP_NOREPOSITION = SWP_NOOWNERZORDER
'    SWP_NOZORDER = &H4
'    SWP_NOREDRAW = &H8
    
  Dim l As Long
'    l = SetWindowPos(f.hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE + SWP_NOSIZE + SWP_NOOWNERZORDER + SWP_NOREPOSITION + SWP_NOZORDER)
    
  l = SetWindowPos(f.hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE + SWP_NOSIZE + SWP_NOREPOSITION)
End Sub

Public Sub DrawForm()
  On Error GoTo EH

  ResizeAll
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

'Private Sub DestroyMDIFormMenu()
'  Dim lMenu As Long
'
'  lMenu = GetSystemMenu(Me.hWnd, False)
'  lMenu = DestroyMenu(lMenu)
'End Sub

Public Sub Disabilita_Abilita_ToolBar_Editor(TipoObj As String)
  Dim i As Long
  
  For i = 1 To fraToolbar.Buttons(2).ButtonMenus.count
    If Trim(UCase(fraToolbar.Buttons(2).ButtonMenus(i).Key)) <> "SHOW_TEXT_EDITOR" Then
      fraToolbar.Buttons(2).ButtonMenus(i).Enabled = False
    Else
      fraToolbar.Buttons(2).ButtonMenus(i).Enabled = True
    End If
  Next i
  
  Select Case Trim(UCase(TipoObj))
    Case "BMS", "MFS"
      For i = 1 To fraToolbar.Buttons(2).ButtonMenus.count
        If Trim(UCase(fraToolbar.Buttons(2).ButtonMenus(i).Key)) = "SHOW_MAP_EDITOR" Then
          fraToolbar.Buttons(2).ButtonMenus(i).Enabled = True
        End If
      Next i
      
    Case "DBD"
      For i = 1 To fraToolbar.Buttons(2).ButtonMenus.count
        If Trim(UCase(fraToolbar.Buttons(2).ButtonMenus(i).Key)) = "SHOW_STRUCTURE" Then
          fraToolbar.Buttons(2).ButtonMenus(i).Enabled = True
        End If
      Next i
      
    Case Else
      For i = 1 To fraToolbar.Buttons(2).ButtonMenus.count
        If Trim(UCase(fraToolbar.Buttons(2).ButtonMenus(i).Key)) <> "SHOW_TEXT_EDITOR" Then
          fraToolbar.Buttons(2).ButtonMenus(i).Enabled = False
        Else
          fraToolbar.Buttons(2).ButtonMenus(i).Enabled = True
        End If
        
      Next i
  End Select
End Sub

Public Sub Restore_Objects()
  Dim nCol As New Collection
  Dim wIdOgg As Long, i As Long, k As Integer
  Dim rs As Recordset

  For i = 1 To MabseF_List.GridList.ListItems.count
    If MabseF_List.GridList.ListItems(i).Selected Then
      wIdOgg = MabseF_List.GridList.ListItems(i).Text
            
      For k = 1 To nCol.count
        nCol.Remove 1
      Next
      
      nCol.Add wIdOgg
      nCol.Add MabseF_List.GridList.ListItems(i).ListSubItems(TYPE_OBJ)
      ' Ma 08/08/2007 : Aggiunto per la Traccia
      nCol.Add IsTrace
       
      'Lancia la funzione del parser
      Set DllParser.PsCollectionParam = nCol
      DLLFunzioni.FnActiveWindowsBool = False
      Attiva_Funzione "PARSER", nCol, "RESTORE_OBJECT", , True, True
      ' Ma: 03/01/2006
      MabseF_List.GridList.ListItems(i).SmallIcon = 6
      MabseF_List.GridList.ListItems(i).ListSubItems(RC_OBJ) = ""
      MabseF_List.GridList.ListItems(i).ListSubItems(PARS_DATE_OBJ) = ""
      DLLFunzioni.FnActiveWindowsBool = True
     
      'Mauro 30-03-06
      Set rs = DLLFunzioni.Open_Recordset("Select numrighe From BS_Oggetti Where IdOggetto = " & wIdOgg)
      If rs.RecordCount > 0 Then
        MabseF_List.GridList.ListItems(i).ListSubItems(LOCS_OBJ) = rs!NumRighe
      End If
      rs.Close
    End If
  Next i
   
End Sub


Private Sub wSokComp_Close()
   GbConnMFE = False
   DLLFunzioni.FnCompilerConnection = GbConnMFE
   fraToolbar.Buttons(4).Value = tbrUnpressed
   
   MabseF_Log.LstLog.ListItems.Add , , Now & " --> Connection Closed."
End Sub

Private Sub wSokComp_Connect()
   GbConnMFE = True
   DLLFunzioni.FnCompilerConnection = GbConnMFE
   
   MabseF_Log.LstLog.ListItems.Add , , Now & " --> Connection Successfull."
End Sub

Private Sub wSokComp_DataArrival(ByVal bytesTotal As Long)
  Dim sBuf As String
  Dim pos1 As Integer
  Dim compMess1 As String, progpath1 As String
  Dim fso1 As New FileSystemObject
  Dim compMess As String
  'Dim mParse As New clsParserCob32
   
  wSokComp.GetData sBuf

  'GbSokBufferReader = GbSokBufferReader & vbCrLf & sBuf
  GbSokBufferReader = sBuf
   
  If InStr(1, GbSokBufferReader, "GENERIC_ERROR") > 0 Then
    pos1 = InStr(1, GbSokBufferReader, "GENERIC_ERROR")
    MabseF_Log.LstLog.ListItems.Add , , Now & " --> " & Mid(GbSokBufferReader, pos1, Len(GbSokBufferReader) - pos1)
    Exit Sub
  End If

  If InStr(1, GbSokBufferReader, "LAUNCH_COMPILE_SCRIPT") > 0 Then
    MabseF_Log.LstLog.ListItems.Add , , Now & " --> Compilation in Progress..."
  End If
   
  If InStr(1, GbSokBufferReader, "FINISH_COMPILE") > 0 Then
    MabseF_Log.LstLog.ListItems.Add , , Now & " --> Compile Complete..."
  End If
   
  If InStr(1, GbSokBufferReader, "COMPILATION ENDED WITHOUT ERRORS") > 0 Then
    compMess1 = Mid(GbSokBufferReader, InStr(1, GbSokBufferReader, "PROGRAM") + 8, InStr(1, GbSokBufferReader, "COMPILATION") - InStr(1, GbSokBufferReader, "PROGRAM") - 9)
    progpath1 = DLLFunzioni.FnPathPrj & "\Output-Prj\Compiled\" & compMess1
    If FileExists(progpath1) Then
      fso1.DeleteFile (progpath1)
    End If
    MabseF_Log.LstLog.ListItems.Add , , Now & " --> " & compMess1 & ": COMPILATION ENDED WITHOUT ERRORS"
    Exit Sub
   End If

'   If InStr(1, GbSokBufferReader, "PARSE_ERROR_LISTING_STARTED") > 0 Then
'     MabseF_Log.LstLog.ListItems.Add , , Now & " --> Start Parse Return-Code..."
'   End If
'   If InStr(1, GbSokBufferReader, "PARSE_ERROR_LISTING_FINISHED") > 0 Then
'     MabseF_Log.LstLog.ListItems.Add , , Now & " --> Parse Return-Code Complete..."
'   End If
   
   If InStr(1, GbSokBufferReader, "RETURN_CODELIST") > 0 Then
     MabseF_Log.LstLog.ListItems.Add , , Now & " --> Return-Code Data Receive Start..."
   End If
   
   If InStr(1, GbSokBufferReader, "COMPILING_ERRORS") > 0 Then 'END-RETURN_CODELIST
     compMess = Mid(GbSokBufferReader, InStr(1, GbSokBufferReader, "PROGRAM") + 8, InStr(1, GbSokBufferReader, "COMPILING_ERRORS") - InStr(1, GbSokBufferReader, "PROGRAM") - 10)
     MabseF_Log.LstLog.ListItems.Add , , Now & " --> " & compMess & " Compiling errors"
      
      'In mBuffer (prima di Blakarlo ho tutto il RetCode)
      
      'controlla il formalismo dei dati
      
      'Parse dei blocchi e strutturazione
      
      'Processa i blocchi e intaragisce con il repository (Messaggi, segnalazioni ecc...)
      'Conta gli errori, visualizza in lista gli errori
      
      If Not GbSokBufferReader = "" Then
      GbSokBufferReader = Replace(GbSokBufferReader, vbLf, Chr(13) & Chr(10))
      Dim progpath As String
      progpath = DLLFunzioni.FnPathPrj & "\Output-Prj\Compiled\" & compMess
      If FileExists(progpath) Then
        Dim fso As New FileSystemObject
        fso.DeleteFile (progpath)
      End If
        Open progpath For Binary As #1
        Dim c As Long
        c = InStr(1, GbSokBufferReader, "COMPILING_ERRORS: #")
        GbSokBufferReader = Mid(GbSokBufferReader, 1, c + 20) & Chr(13) & Chr(10) & Mid(GbSokBufferReader, c + 20, Len(GbSokBufferReader))
'        c = c + 80
'        Do Until c > Len(GbSokBufferReader)
'            GbSokBufferReader = Mid(GbSokBufferReader, 1, c) & vbCrLf & Mid(GbSokBufferReader, c, Len(GbSokBufferReader))
'            c = c + 80
'        Loop
        Put #1, , GbSokBufferReader
        Close #1
      End If
      GbSokBufferReader = ""
   End If
   
End Sub

Private Sub wSokComp_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
  'Chiude
  wSokComp.Close
  
  GbConnMFE = False
  DLLFunzioni.FnCompilerConnection = GbConnMFE
  fraToolbar.Buttons(4).Value = tbrUnpressed
  
  Select Case Number
    Case 10061
      DLLFunzioni.Show_MsgBoxError "MC00I"
    Case Else
      DLLFunzioni.Show_MsgBoxError "RNND", "MabseF_Prj", "wSokComp_Error", Number, vbCrLf & "sCode : " & Scode & vbCrLf & Description
  End Select
  MabseF_Log.LstLog.ListItems.Add , , Now & " --> Generic connection Error..."
End Sub

Private Sub wSokComp_SendComplete()
  DoEvents
End Sub

Private Sub wSokComp_SendProgress(ByVal bytesSent As Long, ByVal bytesRemaining As Long)
  DoEvents
End Sub

Public Sub Start_Compilazione_Objects()
  Dim i As Long, j As Long
  Dim m_WhoIs As t_WhoIs
  Dim m_ClientMsg As t_ClientMsg, m_ClientReq As t_ClientReq
  Dim wParseCob32 As New MaFndC_Compilers
  Dim wStr As String
  Dim wCopyPath As Collection
  Dim wCopyLib As String
   
   For i = 1 To MabseF_List.GridList.ListItems.count
      If MabseF_List.GridList.ListItems(i).Selected Then
         'Genera la stringa per la compilazione
         ReDim m_WhoIs.values(4)
   
         m_WhoIs.values(0).cTag = "USER"
         m_WhoIs.values(0).cValue = GetUser
         m_WhoIs.values(1).cTag = "PCNAME"
         m_WhoIs.values(1).cValue = GetComputerNameU
         m_WhoIs.values(2).cTag = "RAGSOCLIC"
         m_WhoIs.values(2).cValue = GbNomeCliente
         m_WhoIs.values(3).cTag = "NUMLIC"
         m_WhoIs.values(3).cValue = DLLLic.Lic_NumID
         m_WhoIs.values(4).cTag = "NUMPRJ"
         m_WhoIs.values(4).cValue = DLLLic.Lic_NumProdotto
      
         'CLIENT MESSAGE
         ReDim m_ClientMsg.Functions.Parameters(0)
         
         m_ClientMsg.Functions.Name = "EXECUTE_COMMAND"
         m_ClientMsg.Functions.Parameters(0).cTag = "COMMAND"
         
         If MabseF_List.GridList.ListItems(i).SubItems(TYPE_OBJ) = "CBL" Then
            Dim rs As Recordset
            Set rs = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & CLng(MabseF_List.GridList.ListItems(i).Text))
               If rs!Cics Then
                    m_ClientMsg.Functions.Parameters(0).cValue = "ONLINE_COMPILE"
               Else
                    m_ClientMsg.Functions.Parameters(0).cValue = "BATCH_COMPILE"
               End If
         ElseIf MabseF_List.GridList.ListItems(i).SubItems(TYPE_OBJ) = "BMS" Then
            m_ClientMsg.Functions.Parameters(0).cValue = "MAP_COMPILE"
         End If
         
         
        ' m_ClientMsg.Functions.Parameters(0).cValue = "ONLINE_COMPILE"
         
         '***********valori che non servono, verranno presi dal file ini del server*****************
         ReDim Preserve m_ClientMsg.Functions.Parameters(1)
         m_ClientMsg.Functions.Parameters(1).cTag = "SCRIPT_PATH"
         m_ClientMsg.Functions.Parameters(1).cValue = "WWWWWWWWWWWWWWWWW" '"$SERVER_CONF"
         ReDim Preserve m_ClientMsg.Functions.Parameters(2)
         m_ClientMsg.Functions.Parameters(2).cTag = "SCRIPT_NAME"
         m_ClientMsg.Functions.Parameters(2).cValue = "EEEEEEEEEEEEEEEEEE" '"$SERVER_CONF"
         '**********************************************************************
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(3)
         m_ClientMsg.Functions.Parameters(3).cTag = "PGM_NAME"
         m_ClientMsg.Functions.Parameters(3).cValue = MabseF_List.GridList.ListItems(i).ListSubItems(NAME_OBJ).Text & "." & rs!Estensione
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(4)
         m_ClientMsg.Functions.Parameters(4).cTag = "PGM_EXT"
         m_ClientMsg.Functions.Parameters(4).cValue = "clt" 'MabseF_List.GridList.ListItems(i).ListSubItems(2).Text
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(5)
         m_ClientMsg.Functions.Parameters(5).cTag = "PGM_PATH" 'PGM_PATH
         m_ClientMsg.Functions.Parameters(5).cValue = DLLFunzioni.Transcodifica_PathPrj_Win2Unix(MabseF_List.GridList.ListItems(i).ListSubItems(DIR_OBJ).Text)
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(6)
         m_ClientMsg.Functions.Parameters(6).cTag = "PATH_COPY"
         
         'Controllare se il path copy esiste!!!!
         Set wCopyPath = DLLFunzioni.getCopyPathByIdOggetto(MabseF_List.GridList.ListItems(i).Text)
         
         If wCopyPath.count = 0 Then
            'Non ci sono copy associate continuare
            wCopyLib = "NO_COPYLIB"
            
         ElseIf wCopyPath.count = 1 Then
            wCopyLib = DLLFunzioni.Transcodifica_PathPrj_Win2Unix(wCopyPath.Item(1))
         
         ElseIf wCopyPath.count > 1 Then
            For j = 1 To wCopyPath.count
               wCopyLib = wCopyLib & ":" & DLLFunzioni.Transcodifica_PathPrj_Win2Unix(wCopyPath.Item(j))
            Next j
         End If
         
         m_ClientMsg.Functions.Parameters(6).cValue = wCopyLib 'percorso delle copy
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(7)
         m_ClientMsg.Functions.Parameters(7).cTag = "DEST_COMP"
         m_ClientMsg.Functions.Parameters(7).cValue = DLLFunzioni.Transcodifica_PathPrj_Win2Unix(DLLFunzioni.FnPathPrj & "\Output-Prj\Compiled")
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(8)
         m_ClientMsg.Functions.Parameters(8).cTag = "PATH_COMP"
         m_ClientMsg.Functions.Parameters(8).cValue = DLLFunzioni.Transcodifica_PathPrj_Win2Unix(MabseF_List.GridList.ListItems(i).ListSubItems(DIR_OBJ).Text)
         
         '*********non serve*********************
         ReDim Preserve m_ClientMsg.Functions.Parameters(9)
         m_ClientMsg.Functions.Parameters(9).cTag = "PATH_COMPCFG"
         m_ClientMsg.Functions.Parameters(9).cValue = "AAAAAAAAAAAAAAAAAAAA" '"$SERVER_CONF"
         '**************************************
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(10)
         m_ClientMsg.Functions.Parameters(10).cTag = "ID_OGGETTO"
         m_ClientMsg.Functions.Parameters(10).cValue = Format(MabseF_List.GridList.ListItems(i).Text, "##########")
         
         
         'CLIENT EXTRA REQUEST
         ReDim Preserve m_ClientReq.Functions.Parameters(0)
         
         m_ClientReq.Functions.Name = "SET_LOG_FILE"
         m_ClientReq.Functions.Parameters(0).cTag = "PATH_LOG"
         m_ClientReq.Functions.Parameters(0).cValue = "$PATH_SERVER"
         
         ReDim Preserve m_ClientReq.Functions.Parameters(1)
         m_ClientReq.Functions.Parameters(1).cTag = "FILE_LOG"
         m_ClientReq.Functions.Parameters(1).cValue = "CPLOG" & Format(m_ClientMsg.Functions.Parameters(10).cValue, "000000000") & ".LOG"
   
         wStr = wParseCob32.FormatString(m_WhoIs, m_ClientMsg, m_ClientReq)
         
         'Send dei dati
         If wSokComp.State = 7 Then 'ale 15/12/2005
            wSokComp.SendData wStr
         Else
            Exit Sub
         End If
      End If
   Next i
End Sub
'''''''''''''''''''''
'AC 5-10-07
'''''''''''''''''''''
Private Function checkProjectLicense(dbToCheck As String)
  Dim objToCheck() As String
  Dim i As Integer
  Dim rs As Recordset
  
  Select Case dbToCheck
    Case "ampert"
      ReDim objToCheck(3)
      objToCheck(0) = "A99AA700"
      objToCheck(1) = "D9DTB002"
      objToCheck(2) = "B99BMM21"
      objToCheck(3) = "IP2BHRGG"
    Case Else
      '?
      'non so chi sia, ma non passa!
      Exit Function
  End Select
  
  For i = 0 To UBound(objToCheck)
    If Len(objToCheck(i)) Then
      Set rs = DLLFunzioni.Open_Recordset("Select nome From BS_Oggetti Where nome = '" & objToCheck(i) & "'")
      If rs.EOF Then
         checkProjectLicense = False
         Exit Function
      End If
      rs.Close
    End If
  Next i
  checkProjectLicense = True
End Function

Sub ExternalEdit(strTxtEditor As String)
  'S 9-1-2008 apertura editor esterno con pi� elementi selezionati
  Dim strDir As String, strName As String
  Dim strFileSel As String
  Dim i As Long, j As Long, k As Long
  'strtxteditor = txteditor
  k = 0
  ReDim SelFileTxted(k)
  For j = 1 To MabseF_List.GridList.ListItems.count
    If MabseF_List.GridList.ListItems(j).Selected Then
      strDir = Trim(MabseF_List.GridList.ListItems(j).ListSubItems(DIR_OBJ))
      strName = Trim(m_fun.Restituisci_NomeOgg_Da_IdOggetto(MabseF_List.GridList.ListItems(j)))
      SelFileTxted(k) = strDir & "\" & strName
      SelFileTxted(k) = """" & SelFileTxted(k) & """"
      k = k + 1
      ReDim Preserve SelFileTxted(k)
    End If
  Next j
  For i = 0 To UBound(SelFileTxted) - 1
    strFileSel = strFileSel & " " & SelFileTxted(i)
  Next i
  If Len(Dir(strTxtEditor)) Then
    Shell strTxtEditor & " " & strFileSel, vbMaximizedFocus
  Else
    MsgBox strTxtEditor & vbCrLf & "External Editor not found!", vbOKOnly + vbExclamation, GbNomeProdotto
  End If
End Sub

Function fHandleFile(stFile As String, lShowHow As Long)
'In base all'estensione del file identifica l'applicazione associata, se nn trova nulla apre la finestra pre la scelte dell'applicazione

Dim lRet As Long, varTaskID As Variant
Dim stRet As String
    'First try ShellExecute
    lRet = apiShellExecute(Me.hwnd, vbNullString, _
            stFile, vbNullString, vbNullString, lShowHow)
            
    If lRet > ERROR_SUCCESS Then
        stRet = vbNullString
        lRet = -1
    Else
        Select Case lRet
            Case ERROR_NO_ASSOC:
                'Try the OpenWith dialog
                varTaskID = Shell("rundll32.exe shell32.dll,OpenAs_RunDLL " _
                        & stFile, WIN_NORMAL)
                lRet = (varTaskID <> 0)
            Case ERROR_OUT_OF_MEM:
                stRet = "Error: Out of Memory/Resources. Couldn't Execute!"
            Case ERROR_FILE_NOT_FOUND:
                stRet = "Error: File not found.  Couldn't Execute!"
            Case ERROR_PATH_NOT_FOUND:
                stRet = "Error: Path not found. Couldn't Execute!"
            Case ERROR_BAD_FORMAT:
                stRet = "Error:  Bad File Format. Couldn't Execute!"
            Case Else:
        End Select
    End If
    fHandleFile = lRet & _
                IIf(stRet = "", vbNullString, ", " & stRet)
End Function

Private Sub filterExecution_Click()
  MabseF_Prj.AttivaFunzioneMain "LST_FILTER_EXECUTE"
End Sub

Private Sub filterSelection_Click()
  filterSelection.Checked = Not filterSelection.Checked
  MabseF_Prj.AttivaFunzioneMain "LST_FILTER_CONF"
End Sub
Private Sub refreshTreeview_Click()
  Attiva_Funzione "TREEVIEWREFRESH", New Collection, "TREEVIEWREFRESH"
End Sub
