VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MaacdF_FileLic 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Choose your License's File..."
   ClientHeight    =   3510
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   2865
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3510
   ScaleWidth      =   2865
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   1860
      Picture         =   "MaacdF_FileLic.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2520
      Width           =   945
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Select"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   90
      Picture         =   "MaacdF_FileLic.frx":1D2A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2520
      Width           =   945
   End
   Begin MSComctlLib.ListView LswLIC 
      Height          =   2355
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   2745
      _ExtentX        =   4842
      _ExtentY        =   4154
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   12648384
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "File"
         Object.Width           =   4842
      EndProperty
   End
End
Attribute VB_Name = "MaacdF_FileLic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAnnulla_Click()
  ACT.AcStatusLIC = False
  Screen.MousePointer = vbHourglass
  Unload Me
End Sub

Private Sub cmdOK_Click()
  ACT.AcStatusLIC = True
  Screen.MousePointer = vbHourglass
  Unload Me
End Sub

Private Sub Form_Load()
  Dim i As Long
  
  Screen.MousePointer = vbDefault
  
  For i = 1 To UBound(FileLIC)
    LswLIC.ListItems.Add , , FileLIC(i)
  Next i
  
  ACT.AcNomeFileLic = LswLIC.ListItems(1).Text 'Default il primo
End Sub

Private Sub LswLIC_DblClick()
  ACT.AcStatusLIC = True
  Screen.MousePointer = vbHourglass
  Unload Me
End Sub

Private Sub LswLIC_ItemClick(ByVal Item As MSComctlLib.ListItem)
  ACT.AcNomeFileLic = Item.Text
End Sub

Private Sub LswLIC_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then
    ACT.AcStatusLIC = True
    Screen.MousePointer = vbHourglass
    Unload Me
  End If
End Sub
