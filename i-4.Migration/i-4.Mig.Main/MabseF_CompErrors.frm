VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MabseF_CompErrors 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Compiling Errors"
   ClientHeight    =   4680
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   9360
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4680
   ScaleWidth      =   9360
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete Errors"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   1350
      TabIndex        =   5
      Top             =   120
      Width           =   1455
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   60
      TabIndex        =   4
      Top             =   120
      Width           =   1245
   End
   Begin VB.TextBox Err_txt 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4215
      HideSelection   =   0   'False
      Left            =   3060
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Text            =   "MabseF_CompErrors.frx":0000
      Top             =   390
      Width           =   6255
   End
   Begin MSComctlLib.ListView Err_lsw 
      Height          =   4215
      Left            =   30
      TabIndex        =   0
      Top             =   390
      Width           =   2955
      _ExtentX        =   5212
      _ExtentY        =   7435
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImgListView"
      ForeColor       =   12582912
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Errors"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Date"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Time"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Err_label 
      Caption         =   "Nessun File selezionato"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   5910
      TabIndex        =   3
      Top             =   120
      Width           =   2805
   End
   Begin VB.Label Label2 
      Caption         =   "Error Message of :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   4140
      TabIndex        =   2
      Top             =   120
      Width           =   1605
   End
End
Attribute VB_Name = "MabseF_CompErrors"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public loaded As Boolean
Public prjDir As String

Private Sub cmdDelete_Click()
  Dim MyName As String
  
  MyName = Dir(prjDir, vbDirectory)   ' Recupera la prima voce.
  Do While MyName <> ""   ' Avvia il ciclo.
     'Ignora la directory corrente e quella di livello superiore.
     If MyName <> "." And MyName <> ".." Then
        'Usa il confronto bit per bit per verificare se MyName � una directory.
        If Not (GetAttr(prjDir & MyName) And vbDirectory) = vbDirectory Then
             Dim fso As New FileSystemObject
             fso.DeleteFile (prjDir & MyName)
        End If ' se rappresenta una directory.
     End If
     MyName = Dir ' Legge la voce successiva.
  Loop
  Err_label = "Nessun File selezionato"
  Err_txt.Text = ""
End Sub

Private Sub cmdRefresh_Click()
  Err_lsw.ListItems.Clear
  caricaLsw
End Sub

Private Sub Err_lsw_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  Static sOrder
  sOrder = Not sOrder
  'Usa l'ordinamento standard per ordinare gli items
  Err_lsw.SortKey = ColumnHeader.Index - 1
  Err_lsw.SortOrder = Abs(sOrder)
  Err_lsw.Sorted = True
End Sub

Private Sub Err_lsw_ItemClick(ByVal Item As MSComctlLib.ListItem)
  'leggere il file e scrivere il contenuto su Err_txt
  If Item <> "" Then
    Err_txt.Text = ""
    Err_label = Item
    If Item.SubItems(1) <> "No Suitable File" Then
      Err_txt.Text = LeggiFile(DLLFunzioni.FnPathPrj & "\Output-Prj\Compiled\" & Item)
    End If
  End If
End Sub

Private Sub Form_Load()
  loaded = True
  Err_txt.Text = ""
  Err_lsw.ColumnHeaders.Item(1).Width = Err_lsw.Width / 3
  Err_lsw.ColumnHeaders.Item(2).Width = Err_lsw.Width / 3
  Err_lsw.ColumnHeaders.Item(3).Width = Err_lsw.Width / 3
  prjDir = DLLFunzioni.FnPathPrj & "\Output-Prj\Compiled\"
  caricaLsw
End Sub

Public Sub caricaLsw()
  Dim listx As ListItem
  Dim MyName As String
  
  MyName = Dir(prjDir, vbDirectory)   ' Recupera la prima voce.
  Do While MyName <> ""   ' Avvia il ciclo.
    'Ignora la directory corrente e quella di livello superiore.
    If MyName <> "." And MyName <> ".." Then
      'Usa il confronto bit per bit per verificare se MyName � una directory.
      If Not (GetAttr(prjDir & MyName) And vbDirectory) = vbDirectory Then
        Set listx = Err_lsw.ListItems.Add(, , MyName)
        listx.SubItems(1) = Mid(Comp_Data(prjDir & MyName), 1, 10)
        listx.SubItems(2) = Mid(Comp_Data(prjDir & MyName), 10, 10)
      End If ' se rappresenta una directory.
    End If
    MyName = Dir ' Legge la voce successiva.
  Loop
End Sub

Public Function Comp_Data(filePath As String) As String
  Dim crDate As String, lmDate As String, laDate As String
  Dim fileSize As Long
  
  GetFileInformation filePath, crDate, lmDate, laDate, fileSize
  Comp_Data = lmDate
End Function

Public Function LeggiFile(pathFile As String) As String
  On Error GoTo errorHandler
  Dim nFile As Integer, ST As String
  Dim AppStr As String
  
  nFile = FreeFile
  AppStr = ""
  Open pathFile For Input As #nFile
  Do While Not EOF(nFile)
    Line Input #nFile, ST
    AppStr = AppStr & ST & Chr(13) & Chr(10)
  Loop
  Close #nFile
  LeggiFile = AppStr
     
  Exit Function
errorHandler:
  MsgBox "Err. N: " & ERR.Number & ", " & ERR.Source & ", " & ERR.Description
End Function

Private Sub Form_Resize()
  ResizeForm Me
  Err_lsw.ColumnHeaders.Item(1).Width = Err_lsw.Width / 3
  Err_lsw.ColumnHeaders.Item(2).Width = Err_lsw.Width / 3
  Err_lsw.ColumnHeaders.Item(3).Width = Err_lsw.Width / 3
End Sub

Private Sub Form_Unload(Cancel As Integer)
  loaded = False
End Sub
