VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MabseF_Check 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Check Repository "
   ClientHeight    =   4905
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   7035
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4905
   ScaleWidth      =   7035
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdExit 
      Height          =   435
      Left            =   6510
      Picture         =   "MabseF_Check.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   3
      Tag             =   "fixed"
      ToolTipText     =   "Exit"
      Top             =   1140
      Width           =   465
   End
   Begin VB.CommandButton CmdRestore 
      Height          =   435
      Left            =   6510
      Picture         =   "MabseF_Check.frx":1D42
      Style           =   1  'Graphical
      TabIndex        =   2
      Tag             =   "fixed"
      ToolTipText     =   "Restore OLD Repository"
      Top             =   615
      Width           =   465
   End
   Begin VB.CommandButton cmdUpdateRepository 
      Height          =   435
      Left            =   6510
      Picture         =   "MabseF_Check.frx":22CC
      Style           =   1  'Graphical
      TabIndex        =   1
      Tag             =   "fixed"
      ToolTipText     =   "Update Repository Version"
      Top             =   90
      Width           =   465
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6510
      Top             =   2280
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Check.frx":330E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Check.frx":3628
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Check.frx":938A
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Check.frx":A064
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Check.frx":A4B6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lswDifferences 
      Height          =   4725
      Left            =   30
      TabIndex        =   0
      Top             =   90
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   8334
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImgListView"
      ForeColor       =   12582912
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "TABLE.FIELD MISSING"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "MabseF_Check"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim fso As New FileSystemObject

Private Sub CmdRestore_Click()
  If MsgBox("The Old Repository will be restored, confirm the operation? ", vbQuestion + vbYesNo, "i-4.Migration") = vbYes Then
    If FileExists(ProjectPath & "\" & DatabaseName & "_BCK.mty") Then
      'MF 09/08/07
      fso.CopyFile ProjectPath & "\" & DatabaseName & "_BCK.mty", ProjectPath & "\" & DatabaseName, True
      Do Until FileExists(ProjectPath & "\" & DatabaseName)
        DoEvents
      Loop
      'Rendo modificabile i db '
      'MF 09/08/07
      SetFileAttributes ProjectPath & "\" & DatabaseName, 128
      SetFileAttributes ProjectPath & "\" & DatabaseName & "_BCK.mty", 128
      fso.DeleteFile ProjectPath & "\" & DatabaseName & "_BCK.mty", True
    End If
    MsgBox "OLD Repository Restored", vbInformation, "i-4.Migration"
    MabseF_Prj.checkRepository
  End If
End Sub

Public Sub copia_CustomerDB()
  Dim DbName As String
  
  If InStr(DatabaseName, ".") Then
    DbName = Left(DatabaseName, InStr(DatabaseName, ".") - 1)
  Else
    DbName = DatabaseName
  End If
  If FileExists(ProjectPath & "\" & DbName & "_BCK.mty") Then
    If MsgBox("Back-Up of Database already exists." & vbCrLf & "Do you want to overwrite it?", vbInformation + vbYesNo, "i-4.Migration") = vbYes Then
      ' Cancello il vecchio Back-Up
      fso.DeleteFile ProjectPath & "\" & DbName & "_BCK.mty"
      Do Until Not FileExists(ProjectPath & "\" & DbName & "_BCK.mty")
        DoEvents
      Loop
      ' Creo il nuovo Back-Up
      fso.CopyFile ProjectPath & "\" & DatabaseName, ProjectPath & "\" & DbName & "_BCK.mty", True
      Do Until FileExists(ProjectPath & "\" & DbName & "_BCK.mty")
        DoEvents
      Loop
    End If
  Else
    ' Creo il nuovo Back-Up
    fso.CopyFile ProjectPath & "\" & DatabaseName, ProjectPath & "\" & DbName & "_BCK.mty", True
    Do Until FileExists(ProjectPath & "\" & DbName & "_BCK.mty")
      DoEvents
    Loop
  End If
End Sub

Private Sub cmdUpdateRepository_Click()
  ' Ma 11/02/2008 : Chiedo conferma per creazione Back Up
  If MsgBox("Do you want to create Database's Back-Up?", vbInformation + vbYesNo, "i-4.Migration") = vbYes Then
    copia_CustomerDB
  End If
  updateRepository
End Sub

Private Sub cmdExit_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  lswDifferences.ColumnHeaders(1).Width = lswDifferences.Width
End Sub

Public Sub updateRepository()
  Dim i As Long, c As Long
  Dim differences() As String
  Dim sqlStatement As String
  Dim DBCustomerPath As String, DBProjectPath As String
  Dim arrayTabs() As String
  Dim fields() As String
  Dim typeString As String, lenString As String
  Dim collFields As New Collection
  Dim arrayApp() As String
  
  Dim ADOXcatalog As ADOX.Catalog
  Dim ADOXtable As New Table
  Dim ADOXColumn As New ADOX.Column
  
  If lswDifferences.ListItems.count Then
    On Error GoTo errorH
    For i = 1 To lswDifferences.ListItems.count
      If lswDifferences.ListItems(i).Selected Then
        DBProjectPath = GbPathPrd & "\" & SYSTEM_DIR & "\prj.mty"
        DBCustomerPath = ProjectPath & "\" & DatabaseName
        If lswDifferences.ListItems(i).SmallIcon = 1 Then 'manca la tabella
          differences = ResolveDifferences(1, lswDifferences.ListItems(i).Text)
          sqlStatement = "CREATE TABLE " & differences(0)
          CustomerConnection.Execute (sqlStatement)
          Set collFields = ordinalFields(differences(0))
          For c = 1 To collFields.count
            arrayApp = Split(collFields.Item("ORDINAL" & c), ";")
            If c = collFields.count Then
              addField differences(0), arrayApp(0), False
            Else
              addField differences(0), arrayApp(0), True
            End If
          Next c
        ElseIf lswDifferences.ListItems(i).SmallIcon = 4 Then 'manca il campo
          differences = ResolveDifferences(4, lswDifferences.ListItems(i).Text)
          addField differences(0), differences(1), False
        ElseIf lswDifferences.ListItems(i).SmallIcon = 3 Or lswDifferences.ListItems(i).SmallIcon = 2 Then '3 type differente, 2 len differente
          If lswDifferences.ListItems(i).SmallIcon = 3 Then
            differences = ResolveDifferences(3, lswDifferences.ListItems(i).Text)
          ElseIf lswDifferences.ListItems(i).SmallIcon = 2 Then
            differences = ResolveDifferences(2, lswDifferences.ListItems(i).Text)
          End If
          arrayTabs = prjTabs(differences(0))
          For c = 0 To UBound(arrayTabs)
            fields = Split(arrayTabs(c), ";")
            If UCase(fields(0)) = UCase(differences(1)) Then
              Exit For
            End If
          Next c
          Select Case fields(1)
            Case "Text"
              typeString = "TEXT"
              lenString = "(" & fields(2) & ")"
            Case "Float"
              typeString = "FLOAT"
              lenString = ""
            Case "Integer", "Big Integer"
              typeString = "INTEGER"
              lenString = ""
            Case "Date/Time"
              typeString = "DATETIME"
              lenString = ""
            Case "Boolean"
              typeString = "BIT"
              lenString = ""
            Case "Small Integer"
              typeString = "SMALLINT"
              lenString = ""
            Case "Binary"
              typeString = "BINARY"
              lenString = ""
            Case "Memo"
              typeString = "MEMO"
              lenString = ""
            Case Else
              typeString = "TEXT"
              lenString = ""
          End Select
          sqlStatement = "ALTER TABLE " & differences(0) & " ALTER COLUMN [" & differences(1) & "] " & typeString & lenString
          CustomerConnection.Execute (sqlStatement)
        ElseIf lswDifferences.ListItems(i).SmallIcon = 5 Then '5 commento differente
          differences = ResolveDifferences(5, lswDifferences.ListItems(i).Text)
          Set ADOXcatalog = New ADOX.Catalog
          ADOXcatalog.ActiveConnection = CustomerConnection
          For Each ADOXtable In ADOXcatalog.Tables
            If UCase(ADOXtable.Name) = differences(0) Then
              For Each ADOXColumn In ADOXtable.Columns
                If UCase(ADOXColumn.Name) = UCase(differences(1)) Then
                  ADOXColumn.Properties("Description").Value = differences(5)
                  Exit For
                End If
              Next
            End If
          Next
          Set ADOXcatalog = Nothing
        End If
      End If
    Next i
  End If
  CustomerConnection.Close
  PrjConnection.Close
  
  If MabseF_Prj.checkRepository Then
    MsgBox "The Project Repository matches the " & MabseF_Prj.Caption & " requirements ", vbInformation, "i-4.Migration"
  End If
  
  Exit Sub
errorH:
  Resume Next
End Sub

Public Function addField(NomeTabella As String, NomeCampo As String, boolTable As Boolean)
  Dim ADOXprjcatalog As ADOX.Catalog
  Dim ADOXcatalog As ADOX.Catalog
  Dim ADOXprjtable As New Table
  Dim ADOXprjindex As New ADOX.Index
  Dim ADOXprjColumn As New ADOX.Column
  Dim j As Integer, i As Integer
  Dim ADOXtable As New Table
  Dim ADOXColumn As New ADOX.Column
  Dim ADOXindex() As New ADOX.Index
  Dim boolAutoincrement As Boolean
  
  On Error GoTo errorF
  
  Set ADOXprjcatalog = New ADOX.Catalog
  Set ADOXcatalog = New ADOX.Catalog
    
  ADOXcatalog.ActiveConnection = CustomerConnection
  ADOXprjcatalog.ActiveConnection = PrjConnection
        
  For Each ADOXprjtable In ADOXprjcatalog.Tables
    If UCase(ADOXprjtable.Name) = NomeTabella Then
      For Each ADOXprjColumn In ADOXprjtable.Columns
        If UCase(ADOXprjColumn.Name) = UCase(NomeCampo) Then
          Set ADOXColumn = New ADOX.Column
          ADOXColumn.Name = ADOXprjColumn.Name
          ADOXColumn.Type = ADOXprjColumn.Type
          For j = 0 To ADOXprjColumn.Properties.count - 1
            If ADOXprjColumn.Properties.Item(j).Name = "Autoincrement" And ADOXprjColumn.Properties.Item(j).Value = True Then
              Set ADOXColumn.ParentCatalog = ADOXcatalog
              ADOXColumn.Properties("AutoIncrement") = True
              boolAutoincrement = True
              Exit For
            End If
          Next j
          If Not boolAutoincrement Then
            ADOXColumn.Attributes = ADOXprjColumn.Attributes
            ADOXColumn.DefinedSize = ADOXprjColumn.DefinedSize
          End If
          Exit For
        End If
      Next
      Exit For
    End If
  Next
    
  For Each ADOXtable In ADOXcatalog.Tables
    If UCase(ADOXtable.Name) = UCase(NomeTabella) Then
      Do Until ADOXtable.Indexes.count = 0
        ADOXtable.Indexes.Delete (ADOXtable.Indexes.count - 1)
      Loop
      ADOXtable.Columns.Append ADOXColumn
      Exit For
    End If
  Next
  
  If Not boolTable Then
    i = 0
    If ADOXprjtable.Indexes.count Then
      ReDim ADOXindex(ADOXprjtable.Indexes.count - 1)
    Else
      ReDim ADOXindex(0)
    End If
    For Each ADOXprjindex In ADOXprjtable.Indexes
      ADOXindex(i).Clustered = ADOXprjindex.Clustered
      ADOXindex(i).Name = ADOXprjindex.Name
      For j = 0 To ADOXprjindex.Columns.count - 1
        ADOXindex(i).Columns.Append ADOXprjindex.Columns.Item(j).Name
      Next j
      ADOXindex(i).IndexNulls = ADOXprjindex.IndexNulls
      ADOXindex(i).PrimaryKey = ADOXprjindex.PrimaryKey
      ADOXindex(i).Unique = ADOXprjindex.Unique
      ADOXtable.Indexes.Append ADOXindex(i)
      i = i + 1
    Next
  End If

  Set ADOXtable = Nothing
  Set ADOXcatalog = Nothing
  
  Exit Function
errorF:
  Set ADOXtable = Nothing
  Set ADOXcatalog = Nothing
  Resume Next
End Function

Public Function ordinalFields(NomeTabella As String) As Collection
  Dim i As Integer
  Dim appFields() As String, app() As String
  Dim collAppoggio As New Collection

  appFields = prjTabs.Item(NomeTabella)
  For i = 0 To UBound(appFields)
    app() = Split(appFields(i), ";")
    collAppoggio.Add appFields(i), "ORDINAL" & app(3)
  Next i
  Set ordinalFields = New Collection
  Set ordinalFields = collAppoggio
End Function

Public Function ResolveDifferences(Tipo As Integer, strItem As String) As String()
  Dim posPunto As Integer, posType As Integer, posLen As Integer
  Dim differences() As String
  
  ReDim differences(5)
  Select Case Tipo
    Case 1
      differences(0) = strItem 'tab
      differences(1) = "#" 'campo
      differences(2) = "#" 'type
      differences(3) = "#" 'len
      differences(4) = "#" 'table comment
      differences(5) = "#" 'column comment
    Case 2
      posPunto = InStr(strItem, ".")
      posLen = InStr(strItem, " Len(")
      differences(0) = Mid(strItem, 1, posPunto - 1)
      differences(1) = Mid(strItem, posPunto + 1, posLen - posPunto - 1)
      differences(2) = "#"
      differences(3) = Mid(strItem, posLen + 5, Len(strItem) - posLen - 5)
      differences(4) = "#"
      differences(5) = "#"
    Case 3
      posPunto = InStr(strItem, ".")
      posType = InStr(strItem, " Type(")
      differences(0) = Mid(strItem, 1, posPunto - 1)
      differences(1) = Mid(strItem, posPunto + 1, posType - posPunto - 1)
      differences(2) = Mid(strItem, posType + 6, Len(strItem) - posType - 6)
      differences(3) = "#"
      differences(4) = "#"
      differences(5) = "#"
    Case 4
      posPunto = InStr(strItem, ".")
      differences(0) = Left(strItem, posPunto - 1)
      differences(1) = Mid(strItem, posPunto + 1)
      differences(2) = "#"
      differences(3) = "#"
      differences(4) = "#"
      differences(5) = "#"
    Case 5
      posPunto = InStr(strItem, ".")
      If InStr(strItem, "TableComment") > 0 Then
        posType = InStr(strItem, " TableComment(")
        differences(0) = Mid(strItem, 1, posPunto - 1)
        differences(1) = "#"
        differences(2) = "#"
        differences(3) = "#"
        differences(4) = Mid(strItem, posPunto + 1, posType - posPunto - 1)
        differences(5) = "#"
      Else
        posType = InStr(strItem, " Comment(")
        differences(0) = Mid(strItem, 1, posPunto - 1)
        differences(1) = Mid(strItem, posPunto + 1, posType - posPunto - 1)
        differences(2) = "#"
        differences(3) = "#"
        differences(4) = "#"
        differences(5) = Mid(strItem, posType + 9, Len(strItem) - posType - 9)
      End If
  End Select
  
  ResolveDifferences = differences
End Function

Private Sub lswDifferences_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswDifferences.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswDifferences.SortOrder = Order
  lswDifferences.SortKey = Key - 1
  lswDifferences.Sorted = True
End Sub
