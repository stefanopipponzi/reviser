VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form MabseF_List 
   BackColor       =   &H00FFFF80&
   BorderStyle     =   0  'None
   Caption         =   "Project Manager"
   ClientHeight    =   6900
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11505
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   6900
   ScaleWidth      =   11505
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox SelObjN 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   10800
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   6440
      Width           =   465
   End
   Begin VB.TextBox SelPars 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   10320
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   6440
      Width           =   465
   End
   Begin VB.Frame fraDetails 
      BackColor       =   &H00FFFF80&
      Caption         =   "Module details"
      Height          =   6855
      Left            =   4275
      TabIndex        =   7
      Top             =   0
      Width           =   7160
      Begin VB.TextBox PathSel 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   620
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   6440
         Width           =   5355
      End
      Begin VB.TextBox TotalObj 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   120
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   6440
         Width           =   465
      End
      Begin VB.TextBox txtFilter 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H00C00000&
         Height          =   405
         Left            =   1320
         TabIndex        =   8
         Top             =   360
         Visible         =   0   'False
         Width           =   1575
      End
      Begin MSFlexGridLib.MSFlexGrid Filter_List 
         Height          =   915
         Left            =   105
         TabIndex        =   11
         Top             =   240
         Visible         =   0   'False
         Width           =   6900
         _ExtentX        =   12171
         _ExtentY        =   1614
         _Version        =   393216
         Cols            =   11
         FixedCols       =   0
         BackColor       =   16777215
         ForeColor       =   12582912
         BackColorSel    =   16777088
         ForeColorSel    =   16711680
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ListView GridList 
         Height          =   6165
         Left            =   105
         TabIndex        =   12
         Top             =   240
         Width           =   6900
         _ExtentX        =   12171
         _ExtentY        =   10874
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImgListView"
         ForeColor       =   12582912
         BackColor       =   16777215
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   12
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "ID"
            Text            =   "ID"
            Object.Width           =   2294
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "NAME"
            Text            =   "Name"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "TYPE"
            Text            =   "Type"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   3
            Key             =   "DLI"
            Text            =   "DLI"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   4
            Key             =   "CICS"
            Text            =   "CICS"
            Object.Width           =   529
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Key             =   "RC"
            Text            =   "R.C."
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   6
            Text            =   "Locs"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Notes"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Key             =   "PD"
            Text            =   "Parsing Date"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Key             =   "DIR"
            Text            =   "Directory"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Key             =   "IM"
            Text            =   "Import Date"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Object.Width           =   35
         EndProperty
      End
   End
   Begin VB.Frame fraTree 
      BackColor       =   &H00FFFF80&
      Caption         =   "Project Structure"
      Height          =   6855
      Left            =   40
      TabIndex        =   5
      Top             =   0
      Width           =   4220
      Begin MSComctlLib.ProgressBar PBar1 
         Height          =   375
         Left            =   105
         TabIndex        =   6
         Top             =   6450
         Visible         =   0   'False
         Width           =   4005
         _ExtentX        =   7064
         _ExtentY        =   661
         _Version        =   393216
         Appearance      =   1
      End
      Begin MSComctlLib.TreeView PrjTree 
         Height          =   6555
         Left            =   105
         TabIndex        =   0
         Top             =   240
         Width           =   4005
         _ExtentX        =   7064
         _ExtentY        =   11562
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   353
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         ImageList       =   "ImageTree"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ImageList ImageTree 
         Left            =   3240
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   22
         ImageHeight     =   22
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   7
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":0000
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":0423
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":0805
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":0BE7
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":0FE8
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":13CA
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":17AC
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin RichTextLib.RichTextBox rText1 
      Height          =   30
      Left            =   10035
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   360
      Visible         =   0   'False
      Width           =   30
      _ExtentX        =   53
      _ExtentY        =   53
      _Version        =   393217
      Enabled         =   0   'False
      Appearance      =   0
      TextRTF         =   $"MabseF_List.frx":1BB4
   End
   Begin VB.Frame frmDummy 
      Height          =   1695
      Left            =   8640
      TabIndex        =   4
      Top             =   3600
      Width           =   1935
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   1
         Left            =   840
         Top             =   240
      End
      Begin MSComctlLib.ImageList ImgListView 
         Left            =   120
         Top             =   1080
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   6
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":1C3F
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":1D9B
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":1EF7
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":2053
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":25EF
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":2B8F
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComDlg.CommonDialog CommD 
         Left            =   1440
         Top             =   240
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList ImagePrj 
         Left            =   960
         Top             =   840
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   18
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":312F
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":3289
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":33E3
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":36FD
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":3B51
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":3FA5
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":4105
               Key             =   ""
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":4265
               Key             =   ""
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":43C5
               Key             =   ""
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":4529
               Key             =   ""
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":4685
               Key             =   ""
            EndProperty
            BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":47E1
               Key             =   ""
            EndProperty
            BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":493D
               Key             =   ""
            EndProperty
            BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":4A99
               Key             =   ""
            EndProperty
            BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":4BF5
               Key             =   ""
            EndProperty
            BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":73A9
               Key             =   ""
            EndProperty
            BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":7505
               Key             =   ""
            EndProperty
            BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MabseF_List.frx":7AED
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "MabseF_List"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'variabili per memorizzare il buffer di tastiera ed effettuare le ricerche sulla
'lista, inoltre TimerStatus per azzerare dopo 1,5 secondi la key di ricerca in lista
Private TimerStatus As Boolean
Private Key As String
Private pButtonMouse As Boolean

Public FlagStop As Boolean

Public Sub Resize()
  ResizeControls Me
  MabseF_Menu.resizeShortcuts
End Sub

Private Sub Filter_List_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim XN As Long
  Dim i As Long
  
  'assegna alla globale la colonna selezionata
  Filter_List_Column = Filter_List.ColSel
  txtFilter.Text = ""
  txtFilter.Visible = True
  txtFilter.SetFocus
  
  'prende le coordinate
  For i = 0 To Filter_List.Cols - 1
    If i < Filter_List_Column Then
      Filter_List.Col = i
      Filter_List.Row = 1
      XN = XN + Filter_List.CellWidth
    Else
      Exit For
    End If
  Next i
  Filter_List.Row = 0
  Filter_List.Col = 0
  
  'posiziona la text box
  Filter_List.Row = 1
  Filter_List.Col = Filter_List_Column
  
  txtFilter.Top = Filter_List.Top + Filter_List.CellTop
  txtFilter.Left = Filter_List.Left + Filter_List.CellLeft
  txtFilter.Width = Filter_List.CellWidth
  txtFilter.Height = Filter_List.CellHeight
  
  Filter_List.Refresh
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''
'MF - 17/07/07
'Gestione digitazione ESC
''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub Form_KeyPress(KeyAscii As Integer)
  FlagStop = KeyAscii = 27
End Sub

Private Sub Form_Load()
  'carica la toolbar dalla memoria
  Carica_ARRAY_TBar_LIST
  Carica_TreeView_Progetto PrjTree
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 18/07/07
'Gestione richiesta conferma per uscire da i-4.Migration durante esecuzione
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wResp As Variant
     
  If DLLFunzioni.FnProcessRunning Then
    wResp = DLLFunzioni.Show_MsgBoxError("PB00Q")
    If wResp = vbYes Then
      Cancel = 0 'No unload
      DLLFunzioni.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      DLLFunzioni.FnStopProcess = True
    End If
  End If
End Sub

Public Sub GridList_Click()
  Dim j As Long, k As Long
  Dim Item As ListItem
  
  k = 0
  For j = 1 To GridList.ListItems.count
    If GridList.ListItems(j).Selected Then
      k = k + 1
      Set Item = GridList.ListItems(j)
    End If
  Next j
  SelObjN = k

  If SelObjN Then
    'assegna l'id oggetto alla globale vista dalle DLL
    GbIdOggetto = Val(Item)
    GbItemList = Item.ListSubItems(NAME_OBJ)
    GbOggSelNotes = Item.ListSubItems(NOTES_OBJ)
    
    'setta l'idoggetto nelle variabili viste dalle DLL
    Aggiorna_Variabili_DLL
    
    'memorizza le propriet� dell'oggetto selezionato
    Carica_Proprieta_Oggetto GbIdOggetto
    
    Item.ToolTipText = GbOggSelNotes
    Item.ListSubItems(NAME_OBJ).ToolTipText = GbOggSelNotes
    Item.ListSubItems(TYPE_OBJ).ToolTipText = GbOggSelNotes
    Item.ListSubItems(RC_OBJ).ToolTipText = GbOggSelNotes
    Item.ListSubItems(LOCS_OBJ).ToolTipText = GbOggSelNotes
    Item.ListSubItems(NOTES_OBJ).ToolTipText = GbOggSelNotes
    
    'popola le textbox di riepilogo
    PathSel.Text = Str(Val(Item)) & " --- [" & Item.ListSubItems(NAME_OBJ) & "] --- " & OggSel.directory_input
    
    'Abilita Toolbar
    ON_OFF_Button_ToolBar MabseF_Prj.fraToolbar, True
    
    'Voci di menu:
    MabseF_Prj.mnuTextEd.Enabled = True
    MabseF_Prj.mnuProperties.Enabled = True
    'MabseF_Prj.fraToolbar.Buttons(13).ButtonMenus(3).Enabled = True 'texteditor
    'MabseF_Prj.fraToolbar.Buttons(13).ButtonMenus(1).Enabled = Trim(UCase(OggSel.Tipo)) = "BMS" Or Trim(UCase(OggSel.Tipo)) = "MFS"  'Mapeditor
    MabseF_Prj.mnuMapEd.Enabled = Trim(UCase(OggSel.Tipo)) = "BMS" Or Trim(UCase(OggSel.Tipo)) = "MFS"
    MabseF_Prj.mnuMapEditor.Enabled = MabseF_Prj.mnuMapEd
    MabseF_Prj.mnuEXTTextEditor.Enabled = MabseF_Prj.mnuExternalEditor.Enabled
  End If
End Sub

Private Sub GridList_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
 
  Order = GridList.SortOrder
  Key = ColumnHeader.Index
  
  'assegna alla variabile globale l'index colonna selezionato
  ListObjCol = Key
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  GridList.SortOrder = Order
  GridList.SortKey = Key - 1
  GridList.Sorted = True
End Sub

'DOPPIO CLICK == Edit
Private Sub GridList_DblClick()
  If Not (GridList.SelectedItem Is Nothing) Then
    MabseF_Prj.runShortcuts "MAIN", "OBJ_EDIT"
  End If
End Sub

Private Sub GridList_KeyPress(KeyAscii As Integer)
  'serve per permettere all'utente di posizionarsi sulla lista
  'con la digitazione diretta dei tasti
  If Not DLLFunzioni.FnProcessRunning Then
    'ACCENDE IL TIMER
    Timer1.Interval = 1
    Timer1.Enabled = True
  
    If Not TimerStatus Then
      Key = ""
    End If
    
    Key = Key & Chr(KeyAscii)
  End If
End Sub

Private Sub GridList_KeyUp(KeyCode As Integer, Shift As Integer)
  GridList_Click
End Sub

Private Sub GridList_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Not (GridList.SelectedItem Is Nothing) Then
    'Mostra popup menu
    If Button = vbRightButton Then
      'SQ 10-11-06 - Default
      MabseF_Prj.mnuParseObjs2.Enabled = False
      MabseF_Prj.mnuCompileObjs.Enabled = True
      Select Case GridList.SelectedItem.SubItems(TYPE_OBJ)
        Case "ASM", "MAC"
        Case "MBR"
          MabseF_Prj.mnuCompileObjs.Enabled = False
        Case "CPY"
          MabseF_Prj.mnuCompileObjs.Enabled = False
        Case "CBL", "EZT", "QIM", "PLI"
          MabseF_Prj.mnuParseObjs2.Enabled = True
        Case "CMF"
          MabseF_Prj.mnuParseObjs.Enabled = False
      End Select
      
      If Not UCase(GbLicType) = OPEN_LICENSE Then
        MabseF_Prj.mnuCompileObjs.Enabled = False
      End If
      
      PopupMenu MabseF_Prj.mnuPopUp
    End If
  End If
End Sub

Private Sub GridList_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  'assegna alla variabile globale la colonna selezionata
  ListObjCol = Restituisci_Colonna_Da_X_Mouse(GridList, X)
End Sub

Private Sub lblSep_Click()

End Sub

Private Sub PathSel_KeyPress(KeyAscii As Integer)
  On Error GoTo EH
  KeyAscii = 0
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub PrjTree_AfterLabelEdit(Cancel As Integer, NewString As String)
  'Controlla se la rinomina del nodo � corretta
  If UCase(NewString) <> UCase(TreeText) Then
    If Restituisci_Livello_Su_TreeView(PrjTree, TreePrj_Index) = 2 And _
       UCase(PrjTree.Nodes(TreePrj_Index).Text) <> "SOURCE" And _
       UCase(PrjTree.Nodes(TreePrj_Index).Text) <> "DATABASES" And _
       UCase(PrjTree.Nodes(TreePrj_Index).Text) <> "COMMAND" And _
       PrjTree.Nodes(TreePrj_Index).Tag <> "L2" Then
       
      'Cambia il nome nel database
      Sostituisci_Area_Appartenenza_In_DB TreeText, NewString
      
      PrjTree.Nodes(TreePrj_Index).Text = NewString
    Else
      MsgBox "Folder name cannot be changed...", vbExclamation, GbNomeProdotto
      PrjTree.Nodes(TreePrj_Index).Text = TreeText
      
      Cancel = True
    End If
  End If
End Sub

Private Sub PrjTree_GotFocus()
  Controlla_Esistenza_Directory_Su_TreeView PrjTree
End Sub

Private Sub PrjTree_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    pButtonMouse = False
  Else
    pButtonMouse = True
  End If
End Sub

Private Sub PrjTree_NodeClick(ByVal Node As MSComctlLib.Node)
  Dim i As Long
  Dim sql As String
  Dim Counter As Long
  
  If DLLFunzioni.FnProcessRunning Then
    DLLFunzioni.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  If Not pButtonMouse Then Exit Sub
  
  'Abilita Toolbar
  ON_OFF_Button_ToolBar MabseF_Prj.fraToolbar, True
  
  TreePrj_Index = Node.Index
  TreePrj_Tag = Node.Tag
  
  TrwLiv1 = ""
  TrwLiv2 = ""
  
  'memorizza il livello 1 e il livello 2 se ci sono
  Select Case Trim(TreePrj_Tag)
    Case "L1"
      'sono su un livello 1
      TrwLiv1 = PrjTree.Nodes(TreePrj_Index).Text
    Case "L2"
      'sono su un livello 2
      TrwLiv1 = PrjTree.Nodes(TreePrj_Index).Parent
      TrwLiv2 = PrjTree.Nodes(TreePrj_Index).Text
  End Select
  
  'memorizza nella globale la text selezionata della treeview
  TreeText = PrjTree.Nodes(TreePrj_Index).Text
  
  'espande la struttura
  If PrjTree.Nodes(TreePrj_Index).Key = "MB" Then
    For i = 1 To PrjTree.Nodes.count
      PrjTree.Nodes(i).Expanded = True
    Next i
  End If
  
  'prende l'area di appartenenza
  TrwArea = Restituisci_Area_Appartenenza_Da_Nodo(PrjTree, TreePrj_Index)
  
  'Lascia traccia nella TextBox
  PathSel.Text = Node.Text
  
  '****************************************************************
  '****** Parte per visualizzare i contenuti delle cartelle *******
  '****************************************************************
  
  'resetta tutte le immagini della treeview
  Reset_TreeView_Image PrjTree
  
  If TreePrj_Index <> 0 Then
    'seleziona l'immagine
    PrjTree.Nodes(TreePrj_Index).Image = Switch_TreeView_Image(PrjTree, TreePrj_Index)
    
    'Crea la stringa SQL
    sql = Crea_SQL_Selezione_Oggetti(TrwArea, TrwLiv1, TrwLiv2)
  End If
    
  'carica la lista
  If sql <> "" And Verifica_Connessione_ADO Then
    'memorizza L'sql nella globale per averla sempre a disposizione per un eventuale ricaricamento
    'della lista oggetti dopo un cambiamento
    SQL_Selection_List = sql
    
    'Attiva L'antiFlick
    Elimina_Flickering MabseF_List.hwnd
    
    TotalObj = vbNullString
    SelObjN = vbNullString
    SelPars = vbNullString

    Screen.MousePointer = vbHourglass
    Counter = Carica_Lista_Oggetti(GridList, sql)
    TotalObj = Counter
    Screen.MousePointer = vbDefault
    
    Terminate_Flickering
  Else
    GridList.ListItems.Clear
    TotalObj = vbNullString
    SelObjN = vbNullString
    SelPars = vbNullString
  End If
End Sub

Private Sub SelObjN_KeyPress(KeyAscii As Integer)
  On Error GoTo EH
  KeyAscii = 0
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Timer1_Timer()
  Static Counter As Long
  
  If Counter > 100 Then
    Timer1.Enabled = False
    TimerStatus = False
    
    'funzione di ricerca sulla lista tramite Key
    Ricerca_In_Lista GridList, Key, ListObjCol
    
    Counter = 0
  Else
    TimerStatus = True
  End If
  
  Counter = Counter + 1
End Sub

Private Sub TotalObj_KeyPress(KeyAscii As Integer)
  On Error GoTo EH
  KeyAscii = 0
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub txtFilter_Change()
  Filter_List.Col = Filter_List_Column
  Filter_List.Row = 1
  
  Filter_List.Text = txtFilter.Text
End Sub

Private Sub txtFilter_KeyPress(KeyAscii As Integer)
  Filter_List.Text = ""
  If KeyAscii = 13 Then
    If Filter_List_Column = 0 Then
      txtFilter.Text = Format(txtFilter.Text, "000000")
    End If
    
    txtFilter.Visible = False
  End If
End Sub
