Attribute VB_Name = "MabseM_DLL"
Option Explicit

Sub Carica_Variabili_DLLBase(Indice As Long)
  DllBase.BsDllName = TotMenu(Indice).DllName
  DllBase.BsFunzione = TotMenu(Indice).Funzione
  DllBase.BsId = TotMenu(Indice).id
  DllBase.BsTipoFinIm = TotMenu(Indice).TipoFinIm
  DllBase.BsToolTiptext = TotMenu(Indice).ToolTipText
  
  'setta l'idoggetto selezionato nella lista
  DllBase.BsIdOggetto = GbIdOggetto
  
  DllBase.BsTipoMigrazione = GbTipoMigrazione
End Sub

Sub Carica_Variabili_DLLTools(Indice As Long)
  DLLTools.TsDllName = TotMenu(Indice).DllName
  DLLTools.TsFunzione = TotMenu(Indice).Funzione
  DLLTools.TsId = TotMenu(Indice).id
  DLLTools.TsTipoFinIm = TotMenu(Indice).TipoFinIm
  DLLTools.TsToolTiptext = TotMenu(Indice).ToolTipText
  
  'setta l'idoggetto selezionato nella lista
  DLLTools.TsIdOggetto = GbIdOggetto
End Sub

Sub Carica_Variabili_DLLAnalisi(Indice As Long)
  
  DLLDataAnalysis.AnDllName = TotMenu(Indice).DllName
  DLLDataAnalysis.AnFunzione = TotMenu(Indice).Funzione
  DLLDataAnalysis.AnId = TotMenu(Indice).id
  DLLDataAnalysis.AnTipoFinIm = TotMenu(Indice).TipoFinIm
  DLLDataAnalysis.AnToolTiptext = TotMenu(Indice).ToolTipText
  
  'setta l'idoggetto selezionato nella lista
  'DLLDataAnalysis.AnIdOggetto = GbIdOggetto
End Sub

Sub Carica_Variabili_DLLIms(Indice As Long)
  If DLLImsExist Then
    DLLIms.ImDllName = TotMenu(Indice).DllName
    DLLIms.ImFunzione = TotMenu(Indice).Funzione
    DLLIms.ImId = TotMenu(Indice).id
    DLLIms.ImTipoFinIm = TotMenu(Indice).TipoFinIm
    DLLIms.ImToolTiptext = TotMenu(Indice).ToolTipText
    
    'setta l'idoggetto selezionato nella lista
    DLLIms.ImIdOggetto_Add = GbIdOggetto
  End If
End Sub

Sub Carica_Variabili_DLLParser(Indice As Long)
  DllParser.PsDllName = TotMenu(Indice).DllName
  DllParser.PsFunzione = TotMenu(Indice).Funzione
  DllParser.PsId = TotMenu(Indice).id
  DllParser.PsTipoFinIm = TotMenu(Indice).TipoFinIm
  DllParser.PsToolTiptext = TotMenu(Indice).ToolTipText
  
  DllParser.PsTipoMigrazione = GbTipoMigrazione
End Sub

Sub Carica_Variabili_DLLDataManager(Indice As Long)
  If DllDataManExist Then
    DllDataM.DmDllName = TotMenu(Indice).DllName
    DllDataM.DmFunzione = TotMenu(Indice).Funzione
    DllDataM.DmId = TotMenu(Indice).id
    DllDataM.DmTipoFinIm = TotMenu(Indice).TipoFinIm
    DllDataM.DmToolTiptext = TotMenu(Indice).ToolTipText
    
    'setta l'idoggetto selezionato nella Lista
    DllDataM.DmIdOggetto = GbIdOggetto
    
    DllDataM.DmTipoMigrazione = GbTipoMigrazione
  End If
End Sub

Sub Carica_Variabili_DLLDli2RdbMs(Indice As Long)
  If DLLDli2RdbMsExist Then
    DLLDli2RdbMs.drDllName = TotMenu(Indice).DllName
    DLLDli2RdbMs.drFunzione = TotMenu(Indice).Funzione
    DLLDli2RdbMs.drId = TotMenu(Indice).id
    DLLDli2RdbMs.drTipoFinIm = TotMenu(Indice).TipoFinIm
    DLLDli2RdbMs.drToolTiptext = TotMenu(Indice).ToolTipText
    
    'setta l'idoggetto selezionato nella Lista
    DLLDli2RdbMs.drIdOggetto = GbIdOggetto
    
    DLLDli2RdbMs.drTipoMigrazione = GbTipoMigrazione
  End If
End Sub

'ALE XYZ
Sub Carica_Variabili_VSAM2RdbMs(Indice As Long)
  If DLLVSAM2RdbMsExist Then
    DLLVSAM2RdbMs.drDllName = TotMenu(Indice).DllName
    DLLVSAM2RdbMs.drFunzione = TotMenu(Indice).Funzione
    DLLVSAM2RdbMs.drId = TotMenu(Indice).id
    DLLVSAM2RdbMs.drTipoFinIm = TotMenu(Indice).TipoFinIm
    DLLVSAM2RdbMs.drToolTiptext = TotMenu(Indice).ToolTipText

    'setta l'idoggetto selezionato nella Lista
    DLLVSAM2RdbMs.drIdOggetto = GbIdOggetto

    DLLVSAM2RdbMs.drTipoMigrazione = GbTipoMigrazione
  End If
End Sub

Function DllACTInit() As Boolean
  DLLACT.InitDll
  
  Set DLLACT.AcDatabase = DbPrj
  Set DLLACT.AcConnection = ADOCnt
  
  DLLACT.AcIdOggetto = GbIdOggetto
  DLLACT.AcNomeProdotto = GbNomeProdotto
  DLLACT.AcPathDef = ProjectPath
  DLLACT.AcUnitDef = GbUnitDef
  
  DllACTInit = True
End Function

Function DllCryInit() As Boolean
  'inizializza la DLL
  DLLLic.Init_DLL
End Function

Function DllImsInit() As Boolean
  Dim Button As Boolean
  
  Set DLLIms = CreateObject("i4MigIms.MaimdC_Menu")
  
  DLLIms.ImImgDir = GbImgDir
  
  'inizializza la DLL
  DLLIms.InitDll DLLFunzioni
    
  Button = DLLIms.Move_To_Button("FIRST")
  
  While Button
    ReDim Preserve TotMenu(UBound(TotMenu) + 1)
    TotMenu(UBound(TotMenu)).id = DLLIms.ImId
    TotMenu(UBound(TotMenu)).Label = DLLIms.ImLabel
    TotMenu(UBound(TotMenu)).ToolTipText = DLLIms.ImToolTiptext
    TotMenu(UBound(TotMenu)).Picture = DLLIms.ImPicture
    TotMenu(UBound(TotMenu)).PictureEn = DLLIms.ImPictureEn
    TotMenu(UBound(TotMenu)).M1Name = DLLIms.ImM1Name
    TotMenu(UBound(TotMenu)).M1SubName = DLLIms.ImM1SubName
    TotMenu(UBound(TotMenu)).M1Level = DLLIms.ImM1Level
    TotMenu(UBound(TotMenu)).M2Name = DLLIms.ImM2Name
    TotMenu(UBound(TotMenu)).M3Name = DLLIms.ImM3Name
    TotMenu(UBound(TotMenu)).M3ButtonType = DLLIms.ImM3ButtonType
    TotMenu(UBound(TotMenu)).M3SubName = DLLIms.ImM3SubName
    TotMenu(UBound(TotMenu)).M4Name = DLLIms.ImM4Name
    TotMenu(UBound(TotMenu)).M4ButtonType = DLLIms.ImM4ButtonType
    TotMenu(UBound(TotMenu)).M4SubName = DLLIms.ImM4SubName
    TotMenu(UBound(TotMenu)).Funzione = DLLIms.ImFunzione
    TotMenu(UBound(TotMenu)).DllName = DLLIms.ImDllName
    TotMenu(UBound(TotMenu)).TipoFinIm = DLLIms.ImTipoFinIm
        
    Button = DLLIms.Move_To_Button("NEXT")
  Wend
  
  Set DLLIms.ImFinestra = MabseF_Log.LstLog
  Set DLLIms.ImDatabase = DbPrj
  Set DLLIms.ImConnection = ADOCnt
  
  Set DLLIms.ImObjList = MabseF_List.GridList
  
  'DLLIms.ImIdOggetto = GbIdOggetto
  DLLIms.ImNomeProdotto = GbNomeProdotto
  DLLIms.ImPathDef = ProjectPath
  DLLIms.ImUnitDef = GbUnitDef
  'DLLIms.ImPathPrd = GbPathPrd
  
  Set DLLIms.ImConnDBSys = ADOCntSys
  
  DLLIms.ImParent = MabseF_Prj.hwnd
End Function

'ALE xyz
Function DLLVSAM2RdbMsInit() As Boolean
  Dim Button As Boolean
  
  Set DLLVSAM2RdbMs = CreateObject("i4MigVsam.MaVSAMC_Menu")
  
  DLLVSAM2RdbMs.drImgDir = GbImgDir
  
  'inizializza la DLL
  'DLLDli2RdbMs.InitDll DLLFunzioni
  DLLVSAM2RdbMs.InitDll DLLFunzioni

  'Carica il menu in memoria

  Button = DLLVSAM2RdbMs.Move_To_Button("FIRST")

  While Button
    ReDim Preserve TotMenu(UBound(TotMenu) + 1)
    TotMenu(UBound(TotMenu)).id = DLLVSAM2RdbMs.drId
    TotMenu(UBound(TotMenu)).Label = DLLVSAM2RdbMs.drLabel
    TotMenu(UBound(TotMenu)).ToolTipText = DLLVSAM2RdbMs.drToolTiptext
    TotMenu(UBound(TotMenu)).Picture = DLLVSAM2RdbMs.drPicture
    TotMenu(UBound(TotMenu)).PictureEn = DLLVSAM2RdbMs.drPictureEn
    TotMenu(UBound(TotMenu)).M1Name = DLLVSAM2RdbMs.drM1Name
    TotMenu(UBound(TotMenu)).M1SubName = DLLVSAM2RdbMs.drM1SubName
    TotMenu(UBound(TotMenu)).M1Level = DLLVSAM2RdbMs.drM1Level
    TotMenu(UBound(TotMenu)).M2Name = DLLVSAM2RdbMs.drM2Name
    TotMenu(UBound(TotMenu)).M3ButtonType = DLLVSAM2RdbMs.drM3ButtonType
    TotMenu(UBound(TotMenu)).M3SubName = DLLVSAM2RdbMs.drM3SubName
    TotMenu(UBound(TotMenu)).M3Name = DLLVSAM2RdbMs.drM3Name
    TotMenu(UBound(TotMenu)).M4Name = DLLVSAM2RdbMs.drM4Name
    TotMenu(UBound(TotMenu)).M4ButtonType = DLLVSAM2RdbMs.drM4ButtonType
    TotMenu(UBound(TotMenu)).M4SubName = DLLVSAM2RdbMs.drM4SubName
    TotMenu(UBound(TotMenu)).Funzione = DLLVSAM2RdbMs.drFunzione
    TotMenu(UBound(TotMenu)).DllName = DLLVSAM2RdbMs.drDllName
    TotMenu(UBound(TotMenu)).TipoFinIm = DLLVSAM2RdbMs.drTipoFinIm
    
    Button = DLLVSAM2RdbMs.Move_To_Button("NEXT")
  Wend
  
  DLLVSAM2RdbMs.drIdOggetto = GbIdOggetto
  
  Set DLLVSAM2RdbMs.drObjList = MabseF_List.GridList
  Set DLLVSAM2RdbMs.drFinestra = MabseF_Log.LstLog
  Set DLLVSAM2RdbMs.drDatabase = DbPrj
  Set DLLVSAM2RdbMs.drConnection = ADOCnt
  Set DLLVSAM2RdbMs.DrTreePrj = MabseF_List.PrjTree
  DLLVSAM2RdbMs.drNomeProdotto = GbNomeProdotto
  
  DLLVSAM2RdbMs.drPathDef = ProjectPath
  DLLVSAM2RdbMs.drUnitDef = GbUnitDef
  DLLVSAM2RdbMs.drPathPrd = GbPathPrd
  
  DLLVSAM2RdbMs.drTipoMigrazione = GbTipoMigrazione
  'SQ prova:
  'DLLVSAM2RdbMs.drParent = MabseF_List.hWnd 'MabseF_Prj.hWnd 'ALE
  DLLVSAM2RdbMs.drParent = MabseF_Prj.hwnd
  
End Function

Function DllBaseInit() As Boolean
  Dim Button As Boolean
  
  DllBase.BsImgDir = GbImgDir
  
  'inizializza la DLL
  DllBase.InitDll DLLFunzioni
  DllBase.InitDllParser DllParser
   
  Button = DllBase.Move_To_Button("FIRST")
  
  While Button
    ReDim Preserve TotMenu(UBound(TotMenu) + 1)
    TotMenu(UBound(TotMenu)).id = DllBase.BsId
    TotMenu(UBound(TotMenu)).Label = DllBase.BsLabel
    TotMenu(UBound(TotMenu)).ToolTipText = DllBase.BsToolTiptext
    TotMenu(UBound(TotMenu)).Picture = DllBase.BsPicture
    
    TotMenu(UBound(TotMenu)).PictureEn = DllBase.BsPictureEn
    TotMenu(UBound(TotMenu)).M1Name = DllBase.BsM1Name
    TotMenu(UBound(TotMenu)).M1SubName = DllBase.BsM1SubName
    TotMenu(UBound(TotMenu)).M1Level = DllBase.BsM1Level
    TotMenu(UBound(TotMenu)).M2Name = DllBase.BsM2Name
    TotMenu(UBound(TotMenu)).M3Name = DllBase.BsM3Name
    TotMenu(UBound(TotMenu)).M3ButtonType = DllBase.BsM3ButtonType
    TotMenu(UBound(TotMenu)).M3SubName = DllBase.BsM3SubName
    TotMenu(UBound(TotMenu)).M4Name = DllBase.BsM4Name
    TotMenu(UBound(TotMenu)).M4ButtonType = DllBase.BsM4ButtonType
    TotMenu(UBound(TotMenu)).M4SubName = DllBase.BsM4SubName
    TotMenu(UBound(TotMenu)).Funzione = DllBase.BsFunzione
    TotMenu(UBound(TotMenu)).DllName = DllBase.BsDllName
    TotMenu(UBound(TotMenu)).TipoFinIm = DllBase.BsTipoFinIm
        
    Button = DllBase.Move_To_Button("NEXT")
  Wend
  
  Set DllBase.BsFinestra = MabseF_Log.LstLog
  Set DllBase.BsDatabase = DbPrj
  Set DllBase.BsConnection = ADOCnt
  
  Set DllBase.BsObjList = MabseF_List.GridList
  
  DllBase.BsIdOggetto = GbIdOggetto
  DllBase.BsNomeProdotto = GbNomeProdotto
  DllBase.BsPathDef = ProjectPath
  DllBase.BsUnitDef = GbUnitDef
  DllBase.BsPathPrd = app.path
  
  DllBase.BsTipoMigrazione = GbTipoMigrazione
  
  Set DllBase.BsSysConnection = ADOCntSys
  
  DllBase.BsParent = MabseF_Prj.hwnd
End Function

Function DllParserInit() As Boolean  ' Aggiunta
  Dim Button As Boolean
  
  DllParser.PsImgDir = GbImgDir
  
  'inizializza la DLL
  DllParser.InitDll DLLFunzioni

  'Carica il menu in memoria
  Button = DllParser.Move_To_Button("FIRST")

  While Button
    ReDim Preserve TotMenu(UBound(TotMenu) + 1)
    TotMenu(UBound(TotMenu)).id = DllParser.PsId
    TotMenu(UBound(TotMenu)).Label = DllParser.PsLabel
    TotMenu(UBound(TotMenu)).ToolTipText = DllParser.PsToolTiptext
    TotMenu(UBound(TotMenu)).Picture = DllParser.PsPicture
    TotMenu(UBound(TotMenu)).PictureEn = DllParser.PsPictureEn
    TotMenu(UBound(TotMenu)).M1Name = DllParser.PsM1Name
    TotMenu(UBound(TotMenu)).M1SubName = DllParser.PsM1SubName
    TotMenu(UBound(TotMenu)).M1Level = DllParser.PsM1Level
    TotMenu(UBound(TotMenu)).M2Name = DllParser.PsM2Name
    TotMenu(UBound(TotMenu)).M3Name = DllParser.PsM3Name
    TotMenu(UBound(TotMenu)).M3ButtonType = DllParser.PsM3ButtonType
    TotMenu(UBound(TotMenu)).M3SubName = DllParser.PsM3SubName
    TotMenu(UBound(TotMenu)).M4Name = DllParser.PsM4Name
    TotMenu(UBound(TotMenu)).M4ButtonType = DllParser.PsM4ButtonType
    TotMenu(UBound(TotMenu)).M4SubName = DllParser.PsM4SubName
    TotMenu(UBound(TotMenu)).Funzione = DllParser.PsFunzione
    TotMenu(UBound(TotMenu)).DllName = DllParser.PsDllName
    TotMenu(UBound(TotMenu)).TipoFinIm = DllParser.PsTipoFinIm

    Button = DllParser.Move_To_Button("NEXT")
  Wend
  
  Set DllParser.PsFinestra = MabseF_Log.LstLog
  Set DllParser.PsDatabase = DbPrj
  Set DllParser.PsConnection = ADOCnt
  Set DllParser.PsConnDBSys = ADOCntSys
  DllParser.PsNomeProdotto = GbNomeProdotto

  
  Set DllParser.PsObjList = MabseF_List.GridList
  'SQ 1-03-06
  Set DllParser.PsObjTree = MabseF_List.PrjTree
  
  DllParser.PsPathDef = ProjectPath
  DllParser.PsUnitDef = GbUnitDef
  
  DllParser.PsTipoMigrazione = GbTipoMigrazione
  
  DllParser.PsParent = MabseF_Prj.hwnd
End Function

Function DllFunzioniInit() As Boolean
  DLLFunzioni.FnImgDir = GbImgDir
  
  'inizializza la DLL
  DLLFunzioni.InitDll i4LicenceManager.Licenza
  
  Set DLLFunzioni.FnFinestra = MabseF_Log.LstLog
  Set DLLFunzioni.FnDatabase = DbPrj
  Set DLLFunzioni.FnConnection = ADOCnt
  Set DLLFunzioni.FnConnDBSys = ADOCntSys
  DLLFunzioni.FnNomeProdotto = GbNomeProdotto
  DLLFunzioni.FnExternalEditor = GbExternalEditor
  DLLFunzioni.FnCompareEditor = GbCompareEditor
  
  Set DLLFunzioni.FnObjList = MabseF_List.GridList
  DLLFunzioni.FnPathDef = ProjectPath
  DLLFunzioni.FnUnitDef = GbUnitDef
  DLLFunzioni.objType = m_ObjType
  
  DLLFunzioni.FnParent = MabseF_Prj.hwnd
End Function

Function DllToolsInit() As Boolean
  Dim Button As Boolean
  
  DLLTools.TsImgDir = GbImgDir
  
  'inizializza la DLL
  DLLTools.InitDll DLLFunzioni
  DLLTools.InitDllParser DllParser
    
  Button = DLLTools.Move_To_Button("FIRST")
  
  While Button
    ReDim Preserve TotMenu(UBound(TotMenu) + 1)
    TotMenu(UBound(TotMenu)).id = DLLTools.TsId
    TotMenu(UBound(TotMenu)).Label = DLLTools.TsLabel
    TotMenu(UBound(TotMenu)).ToolTipText = DLLTools.TsToolTiptext
    TotMenu(UBound(TotMenu)).Picture = DLLTools.TsPicture
    TotMenu(UBound(TotMenu)).PictureEn = DLLTools.TsPictureEn
    TotMenu(UBound(TotMenu)).M1Name = DLLTools.TsM1Name
    TotMenu(UBound(TotMenu)).M1SubName = DLLTools.TsM1SubName
    TotMenu(UBound(TotMenu)).M1Level = DLLTools.TsM1Level
    TotMenu(UBound(TotMenu)).M2Name = DLLTools.TsM2Name
    TotMenu(UBound(TotMenu)).M3Name = DLLTools.TsM3Name
    TotMenu(UBound(TotMenu)).M3ButtonType = DLLTools.TsM3ButtonType
    TotMenu(UBound(TotMenu)).M3SubName = DLLTools.TsM3SubName
    TotMenu(UBound(TotMenu)).M4Name = DLLTools.TsM4Name
    TotMenu(UBound(TotMenu)).M4ButtonType = DLLTools.TsM4ButtonType
    TotMenu(UBound(TotMenu)).M4SubName = DLLTools.TsM4SubName
    TotMenu(UBound(TotMenu)).Funzione = DLLTools.TsFunzione
    TotMenu(UBound(TotMenu)).DllName = DLLTools.TsDllName
    TotMenu(UBound(TotMenu)).TipoFinIm = DLLTools.TsTipoFinIm
        
    Button = DLLTools.Move_To_Button("NEXT")
  Wend
  
  Set DLLTools.TsFinestra = MabseF_Log.LstLog
  Set DLLTools.TsDatabase = DbPrj
  Set DLLTools.TsConnection = ADOCnt
  
  Set DLLTools.TsObjList = MabseF_List.GridList
  
  DLLTools.TsIdOggetto = GbIdOggetto
  DLLTools.TsNameProdotto = GbNomeProdotto
  DLLTools.TsPathDef = ProjectPath
  DLLTools.TsUnitDef = GbUnitDef
  DLLTools.TsPathPrd = GbPathPrd
  
  Set DLLTools.TsConnDBSys = ADOCntSys
  
  DLLTools.TsParent = MabseF_Prj.hwnd
End Function

Function DllDataManInit(Dli2RdbMs As Boolean) As Boolean  ' Aggiunta
  Dim Button As Boolean
  
  Set DllDataM = CreateObject("i4MigDataConversion.MadmdC_Menu")
  
  'Attiva il flag del DataManager
  DllDataM.DmDli2RdbmsACT = True
  
  DllDataM.DmImgDir = GbImgDir
  DllDataM.DmPathPrd = GbPathPrd
  
  'inizializza la DLL
  DllDataM.InitDll DLLFunzioni
  
  'Carica il menu in memoria
  Button = DllDataM.Move_To_Button("FIRST")

  While Button
    If DllDataM.DmId = 212 Then 'Bottone per la migrazione dei DB DLI verso RELAZIONALE
      If Dli2RdbMs Then
        ReDim Preserve TotMenu(UBound(TotMenu) + 1)
        TotMenu(UBound(TotMenu)).id = DllDataM.DmId
        TotMenu(UBound(TotMenu)).Label = DllDataM.DmLabel
        TotMenu(UBound(TotMenu)).ToolTipText = DllDataM.DmToolTiptext
        TotMenu(UBound(TotMenu)).Picture = DllDataM.DmPicture
        TotMenu(UBound(TotMenu)).PictureEn = DllDataM.DmPictureEn
        TotMenu(UBound(TotMenu)).M1Name = DllDataM.DmM1Name
        TotMenu(UBound(TotMenu)).M1SubName = DllDataM.DmM1SubName
        TotMenu(UBound(TotMenu)).M1Level = DllDataM.DmM1Level
        TotMenu(UBound(TotMenu)).M2Name = DllDataM.DmM2Name
        TotMenu(UBound(TotMenu)).M3ButtonType = DllDataM.DmM3ButtonType
        TotMenu(UBound(TotMenu)).M3SubName = DllDataM.DmM3SubName
        TotMenu(UBound(TotMenu)).M3Name = DllDataM.DmM3Name
        TotMenu(UBound(TotMenu)).M4Name = DllDataM.DmM4Name
        TotMenu(UBound(TotMenu)).M4ButtonType = DllDataM.DmM4ButtonType
        TotMenu(UBound(TotMenu)).M4SubName = DllDataM.DmM4SubName
        TotMenu(UBound(TotMenu)).Funzione = DllDataM.DmFunzione
        TotMenu(UBound(TotMenu)).DllName = DllDataM.DmDllName
        TotMenu(UBound(TotMenu)).TipoFinIm = DllDataM.DmTipoFinIm
      End If
    Else
      ReDim Preserve TotMenu(UBound(TotMenu) + 1)
      TotMenu(UBound(TotMenu)).id = DllDataM.DmId
      TotMenu(UBound(TotMenu)).Label = DllDataM.DmLabel
      TotMenu(UBound(TotMenu)).ToolTipText = DllDataM.DmToolTiptext
      TotMenu(UBound(TotMenu)).Picture = DllDataM.DmPicture
      TotMenu(UBound(TotMenu)).PictureEn = DllDataM.DmPictureEn
      TotMenu(UBound(TotMenu)).M1Name = DllDataM.DmM1Name
      TotMenu(UBound(TotMenu)).M1SubName = DllDataM.DmM1SubName
      TotMenu(UBound(TotMenu)).M1Level = DllDataM.DmM1Level
      TotMenu(UBound(TotMenu)).M2Name = DllDataM.DmM2Name
      TotMenu(UBound(TotMenu)).M3ButtonType = DllDataM.DmM3ButtonType
      TotMenu(UBound(TotMenu)).M3SubName = DllDataM.DmM3SubName
      TotMenu(UBound(TotMenu)).M3Name = DllDataM.DmM3Name
      TotMenu(UBound(TotMenu)).M4Name = DllDataM.DmM4Name
      TotMenu(UBound(TotMenu)).M4ButtonType = DllDataM.DmM4ButtonType
      TotMenu(UBound(TotMenu)).M4SubName = DllDataM.DmM4SubName
      TotMenu(UBound(TotMenu)).Funzione = DllDataM.DmFunzione
      TotMenu(UBound(TotMenu)).DllName = DllDataM.DmDllName
      TotMenu(UBound(TotMenu)).TipoFinIm = DllDataM.DmTipoFinIm
    End If
    
    Button = DllDataM.Move_To_Button("NEXT")
  Wend
  
  DllDataM.DmIdOggetto = GbIdOggetto
  
  Set DllDataM.DmFinestra = MabseF_Log.LstLog
  Set DllDataM.DmDatabase = DbPrj
  Set DllDataM.DmConnection = ADOCnt
  DllDataM.DmNomeProdotto = GbNomeProdotto
  
  Set DllDataM.DmObjList = MabseF_List.GridList
  
  DllDataM.DmPathDef = ProjectPath
  DllDataM.DmUnitDef = GbUnitDef
  
  DllDataM.DmPathPrd = GbPathPrd
  
  DllDataM.DmTipoMigrazione = GbTipoMigrazione
  
  DllDataM.DmParent = MabseF_Prj.hwnd
End Function

Function DLLDli2RdbMsInit() As Boolean
  Dim Button As Boolean
  
  Set DLLDli2RdbMs = CreateObject("i4MigIms.MadrdC_Menu")
  
  DLLDli2RdbMs.drImgDir = GbImgDir
  
  'inizializza la DLL
  DLLDli2RdbMs.InitDll DLLFunzioni

  'Carica il menu in memoria

  Button = DLLDli2RdbMs.Move_To_Button("FIRST")

  While Button
    ReDim Preserve TotMenu(UBound(TotMenu) + 1)
    TotMenu(UBound(TotMenu)).id = DLLDli2RdbMs.drId
    TotMenu(UBound(TotMenu)).Label = DLLDli2RdbMs.drLabel
    TotMenu(UBound(TotMenu)).ToolTipText = DLLDli2RdbMs.drToolTiptext
    TotMenu(UBound(TotMenu)).Picture = DLLDli2RdbMs.drPicture
    TotMenu(UBound(TotMenu)).PictureEn = DLLDli2RdbMs.drPictureEn
    TotMenu(UBound(TotMenu)).M1Name = DLLDli2RdbMs.drM1Name
    TotMenu(UBound(TotMenu)).M1SubName = DLLDli2RdbMs.drM1SubName
    TotMenu(UBound(TotMenu)).M1Level = DLLDli2RdbMs.drM1Level
    TotMenu(UBound(TotMenu)).M2Name = DLLDli2RdbMs.drM2Name
    TotMenu(UBound(TotMenu)).M3ButtonType = DLLDli2RdbMs.drM3ButtonType
    TotMenu(UBound(TotMenu)).M3SubName = DLLDli2RdbMs.drM3SubName
    TotMenu(UBound(TotMenu)).M3Name = DLLDli2RdbMs.drM3Name
    TotMenu(UBound(TotMenu)).M4Name = DLLDli2RdbMs.drM4Name
    TotMenu(UBound(TotMenu)).M4ButtonType = DLLDli2RdbMs.drM4ButtonType
    TotMenu(UBound(TotMenu)).M4SubName = DLLDli2RdbMs.drM4SubName
    TotMenu(UBound(TotMenu)).Funzione = DLLDli2RdbMs.drFunzione
    TotMenu(UBound(TotMenu)).DllName = DLLDli2RdbMs.drDllName
    TotMenu(UBound(TotMenu)).TipoFinIm = DLLDli2RdbMs.drTipoFinIm
    
    Button = DLLDli2RdbMs.Move_To_Button("NEXT")
  Wend
  
  DLLDli2RdbMs.drIdOggetto = GbIdOggetto
  
  Set DLLDli2RdbMs.drObjList = MabseF_List.GridList
  Set DLLDli2RdbMs.drFinestra = MabseF_Log.LstLog
  Set DLLDli2RdbMs.drDatabase = DbPrj
  Set DLLDli2RdbMs.drConnection = ADOCnt
  Set DLLDli2RdbMs.DrTreePrj = MabseF_List.PrjTree
  DLLDli2RdbMs.drNomeProdotto = GbNomeProdotto
  
  DLLDli2RdbMs.drPathDef = ProjectPath
  DLLDli2RdbMs.drUnitDef = GbUnitDef
  DLLDli2RdbMs.drPathPrd = GbPathPrd
  
  DLLDli2RdbMs.drTipoMigrazione = GbTipoMigrazione
 'SQ tmp:
  'DLLDli2RdbMs.drParent = MabseF_List.hWnd 'MabseF_Prj.hWnd 'ALE
  'SQ perch� cambiato in List?! Gli altri sono tutti Prj...
  DLLDli2RdbMs.drParent = MabseF_Prj.hwnd

End Function

Sub Set_Connection_To_DLL(Connection As Boolean, DatabaseName As String)
  'setta la variabile connessione Attiva o non attiva
  If Connection = True Then
    'INIT VARIABILI DI FUNZIONI
    'RIMARRA' SOLO QUESTA INIT
    'PERCHE' TUTTI VEDRANNO FUNZIONI E NON LE LORO VARIABILI
    DLLFunzioni.FnAsterixForDelete = GbAsterixForDelete
    DLLFunzioni.FnAsterixForWhere = GbAsterixForWhere
    Set DLLFunzioni.FnConnDBSys = ADOCntSys
    Set DLLFunzioni.FnConnection = ADOCnt
    Set DLLFunzioni.FnDatabase = DbPrj
    DLLFunzioni.FnNomeDB = DatabaseName
    DLLFunzioni.FnConnState = adStateOpen
    DLLFunzioni.FnProvider = GbCurTipoDB
    DLLFunzioni.FnPathDef = ProjectPath
    DLLFunzioni.FnPathPrd = GbPathPrd
    DLLFunzioni.FnPathDB = GbPathPrj
    DLLFunzioni.FnPathPrj = GbPathPrj
    
    DllBase.BsConnState = adStateOpen
    Set DllBase.BsConnection = ADOCnt
    DllBase.BsProvider = GbCurTipoDB
    DllBase.BsAsterixForDelete = GbAsterixForDelete
    DllBase.BsAsterixForWhere = GbAsterixForWhere
    
    DllParser.PsConnState = adStateOpen
    Set DllParser.PsConnection = ADOCnt
    DllParser.PsProvider = GbCurTipoDB
    DllParser.PsAsterixForDelete = GbAsterixForDelete
    DllParser.PsAsterixForWhere = GbAsterixForWhere

    DLLTools.TsConnState = adStateOpen
    Set DLLTools.TsConnection = ADOCnt
    DLLTools.TsProvider = GbCurTipoDB
    DLLTools.TsAsterixForDelete = GbAsterixForDelete
    DLLTools.TsAsterixForWhere = GbAsterixForWhere
    
    DLLACT.AcConnState = adStateOpen
    Set DLLACT.AcConnection = ADOCnt
    DllBase.BsNomeDB = DatabaseName
    DllParser.PsNomeDB = DatabaseName
    DLLTools.TsNameDB = DatabaseName
    DLLACT.AcNomeDB = DatabaseName
    DllBase.BsPathDB = ProjectPath
    DllBase.BsPathPrj = GbPathPrj
    DllParser.PsPathDB = ProjectPath
    DllParser.PsPathPrj = GbPathPrj
    DLLTools.TsPathDB = ProjectPath
    DLLTools.TsPathPrj = GbPathPrj
    DLLACT.AcPathDB = ProjectPath
    Set DllBase.BsDatabase = DbPrj
    Set DllParser.PsDatabase = DbPrj
    Set DLLTools.TsDatabase = DbPrj
    Set DLLACT.AcDatabase = DbPrj
    
    If GbDllDataManExist Then
      DllDataM.DmConnState = adStateOpen
      DllDataM.DmNomeDB = DatabaseName
      DllDataM.DmPathDb = ProjectPath
      Set DllDataM.DmDatabase = DbPrj
      DllDataM.DmProvider = GbCurTipoDB
      Set DllDataM.DmConnection = ADOCnt
      DllDataM.DmAsterixForDelete = GbAsterixForDelete
      DllDataM.DmAsterixForWhere = GbAsterixForWhere
    End If
    
    If GbDLLDli2RdbMsExist Then
      DLLDli2RdbMs.drConnState = adStateOpen
      DLLDli2RdbMs.drNomeDB = DatabaseName
      DLLDli2RdbMs.drPathDb = GbPathPrj
      Set DLLDli2RdbMs.drDatabase = DbPrj
      DLLDli2RdbMs.drProvider = GbCurTipoDB
      Set DLLDli2RdbMs.drConnection = ADOCnt
      DLLDli2RdbMs.drAsterixForDelete = GbAsterixForDelete
      DLLDli2RdbMs.drAsterixForWhere = GbAsterixForWhere
    End If
    
    ' xyz
    If GbDllVSAMExist Then
      DLLVSAM2RdbMs.drConnState = adStateOpen
      DLLVSAM2RdbMs.drNomeDB = DatabaseName
      DLLVSAM2RdbMs.drPathDb = ProjectPath
      Set DLLVSAM2RdbMs.drDatabase = DbPrj
      DLLVSAM2RdbMs.drProvider = GbCurTipoDB
      Set DLLVSAM2RdbMs.drConnection = ADOCnt
      DLLVSAM2RdbMs.drAsterixForDelete = GbAsterixForDelete
      DLLVSAM2RdbMs.drAsterixForWhere = GbAsterixForWhere
    End If
      
    If GbDllImsExist Then
      DLLIms.ImConnState = adStateOpen
      DLLIms.ImNomeDB = DatabaseName
      DLLIms.ImPathDB = ProjectPath
      Set DLLIms.ImDatabase = DbPrj
      DLLIms.ImProvider = GbCurTipoDB
      Set DLLIms.ImConnection = ADOCnt
      DLLIms.ImAsterixForDelete = GbAsterixForDelete
      DLLIms.ImAsterixForWhere = GbAsterixForWhere
    End If
    
    If GbDllAnalisiExist Then
      DLLDataAnalysis.AnConnState = adStateOpen
      Set DLLDataAnalysis.AnConnection = ADOCnt
      DLLDataAnalysis.AnNomeDB = DatabaseName
      DLLDataAnalysis.AnPathDB = ProjectPath
      Set DLLDataAnalysis.AnDatabase = DbPrj
      DLLDataAnalysis.AnProvider = GbCurTipoDB
      DLLDataAnalysis.AnAsterixForDelete = GbAsterixForDelete
      DLLDataAnalysis.AnAsterixForWhere = GbAsterixForWhere
    End If
  Else
    DllBase.BsConnState = adStateClosed
    DllParser.PsConnState = adStateClosed
    DLLTools.TsConnState = adStateClosed
    DLLACT.AcConnState = adStateClosed
    
    If GbDllDataManExist Then
      DllDataM.DmConnState = adStateClosed
    End If
    
    If GbDLLDli2RdbMsExist Then
      DLLDli2RdbMs.drConnState = adStateClosed
    End If
      
    If GbDllImsExist Then
      DLLIms.ImConnState = adStateClosed
    End If
    
    If GbDllAnalisiExist Then
      DLLDataAnalysis.AnConnState = adStateClosed
    End If
  End If
End Sub

Function DllAnalisiInit() As Boolean
  Dim Button As Boolean
  
  Set DLLDataAnalysis = CreateObject("MaandP_00.MaandC_Menu")
  
  DLLDataAnalysis.AnImgDir = GbImgDir
  
  'inizializza la DLL
  DLLDataAnalysis.InitDll DLLFunzioni
    
  Button = DLLDataAnalysis.Move_To_Button("FIRST")
  
  While Button
    ReDim Preserve TotMenu(UBound(TotMenu) + 1)
    TotMenu(UBound(TotMenu)).id = DLLDataAnalysis.AnId
    TotMenu(UBound(TotMenu)).Label = DLLDataAnalysis.AnLabel
    TotMenu(UBound(TotMenu)).ToolTipText = DLLDataAnalysis.AnToolTiptext
    TotMenu(UBound(TotMenu)).Picture = DLLDataAnalysis.AnPicture
    TotMenu(UBound(TotMenu)).PictureEn = DLLDataAnalysis.AnPictureEn
    TotMenu(UBound(TotMenu)).M1Name = DLLDataAnalysis.AnM1Name
    TotMenu(UBound(TotMenu)).M1SubName = DLLDataAnalysis.AnM1SubName
    TotMenu(UBound(TotMenu)).M1Level = CLng(DLLDataAnalysis.AnM1Level)
    TotMenu(UBound(TotMenu)).M2Name = DLLDataAnalysis.AnM2Name
    TotMenu(UBound(TotMenu)).M3Name = DLLDataAnalysis.AnM3Name
    TotMenu(UBound(TotMenu)).M3ButtonType = DLLDataAnalysis.AnM3ButtonType
    TotMenu(UBound(TotMenu)).M3SubName = DLLDataAnalysis.AnM3SubName
    TotMenu(UBound(TotMenu)).M4Name = DLLDataAnalysis.AnM4Name
    TotMenu(UBound(TotMenu)).M4ButtonType = DLLDataAnalysis.AnM4ButtonType
    TotMenu(UBound(TotMenu)).M4SubName = DLLDataAnalysis.AnM4SubName
    TotMenu(UBound(TotMenu)).Funzione = DLLDataAnalysis.AnFunzione
    TotMenu(UBound(TotMenu)).DllName = DLLDataAnalysis.AnDllName
    TotMenu(UBound(TotMenu)).TipoFinIm = DLLDataAnalysis.AnTipoFinIm
        
    Button = DLLDataAnalysis.Move_To_Button("NEXT")
  Wend
  
  Set DLLDataAnalysis.AnFinestra = MabseF_Log.LstLog
  Set DLLDataAnalysis.AnDatabase = DbPrj
  Set DLLDataAnalysis.AnConnection = ADOCnt
  
  Set DLLDataAnalysis.AnObjList = MabseF_List.GridList
  
  DLLDataAnalysis.AnNomeProdotto = GbNomeProdotto
  DLLDataAnalysis.AnPathDef = ProjectPath
  DLLDataAnalysis.AnUnitDef = GbUnitDef
  
  Set DLLDataAnalysis.AnConnDBSys = ADOCntSys
  
  DLLDataAnalysis.AnParent = MabseF_Prj.hwnd
End Function

Public Sub Setta_Variabili_Export_DLL()
  'setta tutte le variabili che devono essere viste dalle DLL
  Set DbPrj = New ADOX.Catalog
  Set ADOCnt = New ADODB.Connection
End Sub

Public Sub Aggiorna_Variabili_DLL()
  'DLL FUNZIONI
  DLLFunzioni.FnIdOggetto = GbIdOggetto
  DLLFunzioni.FnNomeProdotto = GbNomeProdotto
  '...IMPLEMENTARE
  
  'DLL Base
  DllBase.BsIdOggetto = GbIdOggetto
  DllBase.BsNomeProdotto = GbNomeProdotto
  DllBase.BsPathDef = ProjectPath
  DllBase.BsUnitDef = GbUnitDef
  DllBase.BsTipoMigrazione = GbTipoMigrazione
  
  'DLL Data Manager
  If GbDllDataManExist Then
    DllDataM.DmNomeProdotto = GbNomeProdotto
    DllDataM.DmIdOggetto = GbIdOggetto
    DllDataM.DmPathDef = ProjectPath
    DllDataM.DmUnitDef = GbUnitDef
    DllDataM.DmTipoMigrazione = GbTipoMigrazione
    DllDataM.DmPathDb = GbPathPrj
    DllDataM.DmParam_PercOut = DllDataM.DmPathDb & "\Output-Prj"
    DllDataM.DmParam_OutField_Db = DllDataM.DmParam_PercOut & "\DB2"
    DllDataM.DmParam_OutField_Orc = DllDataM.DmParam_PercOut & "\Oracle"
    DllDataM.DmParam_OutField_Vsam = DllDataM.DmParam_PercOut & "\Vsam"
    DllDataM.DmParam_PercOutCpy = DllDataM.DmParam_PercOut & "\Dli\"
    DllDataM.DmParam_PercOutField = DllDataM.DmParam_PercOut & "\" & SYSTEM_DIR & "\CPY_BASE\OutPut\"
  End If
  
  'DLL Parser
  DllParser.PsNomeProdotto = GbNomeProdotto
  DllParser.PsPathDef = ProjectPath
  DllParser.PsUnitDef = GbUnitDef
  DllParser.PsTipoMigrazione = GbTipoMigrazione
  
  'DLL Dli2RDBMS
  If GbDLLDli2RdbMsExist Then
    DLLDli2RdbMs.drNomeProdotto = GbNomeProdotto
    DLLDli2RdbMs.drPathDef = ProjectPath
    DLLDli2RdbMs.drUnitDef = GbUnitDef
    DLLDli2RdbMs.drTipoMigrazione = GbTipoMigrazione
    DLLDli2RdbMs.drNomeOggetto = GbItemList
  End If
  
  'DLL Tools
  DLLTools.TsNameProdotto = GbNomeProdotto
  DLLTools.TsPathDef = ProjectPath
  DLLTools.TsUnitDef = GbUnitDef
  DLLTools.TsPathPrj = GbPathPrj
  
  'DLL ACT
  DLLACT.AcNomeProdotto = GbNomeProdotto
  DLLACT.AcPathDef = ProjectPath
  DLLACT.AcUnitDef = GbUnitDef
  
  'DLL IMS
  If GbDllImsExist Then
    DLLIms.ImNomeProdotto = GbNomeProdotto
    DLLIms.ImPathDef = ProjectPath
    DLLIms.ImUnitDef = GbUnitDef
  End If
  
  'DLL DataAnalysis
  If GbDllAnalisiExist Then
    '??????????
  End If
  
  'DLL VSAM2RDBMS
  If GbDllVSAMExist Then
    DLLVSAM2RdbMs.drNomeProdotto = GbNomeProdotto
    DLLVSAM2RdbMs.drPathDef = ProjectPath
    DLLVSAM2RdbMs.drUnitDef = GbUnitDef
    DLLVSAM2RdbMs.drTipoMigrazione = GbTipoMigrazione
    DLLVSAM2RdbMs.drNomeOggetto = GbItemList
  End If
End Sub
