VERSION 5.00
Begin VB.Form MabseF_prgAvvio 
   BackColor       =   &H00FFFFFF&
   ClientHeight    =   2355
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   6495
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   18
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   2355
   ScaleWidth      =   6495
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      BackColor       =   &H00008000&
      BorderStyle     =   0  'None
      ForeColor       =   &H00008000&
      Height          =   435
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   6495
      TabIndex        =   0
      Top             =   0
      Width           =   6495
      Begin VB.Label lblVersion 
         BackStyle       =   0  'Transparent
         Caption         =   "V. x.yy"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   2250
         TabIndex        =   2
         Top             =   150
         Width           =   3015
      End
      Begin VB.Label Label1 
         BackColor       =   &H80000016&
         BackStyle       =   0  'Transparent
         Caption         =   "i-4.Migration"
         BeginProperty Font 
            Name            =   "Century Gothic"
            Size            =   15.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   555
         Left            =   120
         TabIndex        =   1
         Top             =   10
         Width           =   2385
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Height          =   2085
      Index           =   1
      Left            =   20
      TabIndex        =   10
      Top             =   270
      Width           =   2085
      Begin VB.PictureBox picI4 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   1650
         Left            =   240
         Picture         =   "MabseF_prgAvvio.frx":0000
         ScaleHeight     =   110
         ScaleMode       =   0  'User
         ScaleWidth      =   113
         TabIndex        =   11
         Top             =   360
         Width           =   1700
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Height          =   2085
      Index           =   0
      Left            =   2110
      TabIndex        =   3
      Top             =   270
      Width           =   4400
      Begin VB.PictureBox Picture4 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   795
         Picture         =   "MabseF_prgAvvio.frx":0998
         ScaleHeight     =   25
         ScaleMode       =   0  'User
         ScaleWidth      =   25
         TabIndex        =   6
         Top             =   510
         Width           =   375
      End
      Begin VB.PictureBox Picture5 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   375
         Index           =   0
         Left            =   765
         Picture         =   "MabseF_prgAvvio.frx":26DA
         ScaleHeight     =   375
         ScaleWidth      =   255
         TabIndex        =   5
         Top             =   1050
         Width           =   255
      End
      Begin VB.PictureBox Picture6 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   375
         Index           =   0
         Left            =   780
         Picture         =   "MabseF_prgAvvio.frx":43D4
         ScaleHeight     =   375
         ScaleWidth      =   345
         TabIndex        =   4
         Top             =   1620
         Width           =   345
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Open an Existing Project..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1230
         TabIndex        =   9
         Top             =   540
         Width           =   3255
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Create a New Project..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1230
         TabIndex        =   8
         Top             =   1080
         Width           =   3015
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1230
         TabIndex        =   7
         Top             =   1650
         Width           =   1815
      End
   End
End
Attribute VB_Name = "MabseF_prgAvvio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'*************************************
' this example simply makes the hand cursor the form's primary cursor

' used to convert icons/bitmaps to stdPicture objects
Private Declare Function OleCreatePictureIndirect Lib "olepro32.dll" (lpPictDesc As PICTDESC, _
                                                                      riid As Any, _
                                                                      ByVal fOwn As Long, _
                                                                      ipic As IPicture) As Long
Private Type PICTDESC
  cbSize As Long
  pictType As Long
  hIcon As Long
  hPal As Long
End Type
' used to load the current hand cursor theme
Private Declare Function LoadCursor Lib "user32.dll" Alias "LoadCursorA" (ByVal hInstance As Long, ByVal lpCursorName As Long) As Long
Private Declare Function SetCursor Lib "user32.dll" (ByVal hCursor As Long) As Long
Private Const IDC_HAND As Long = 32649

Private myHandCursor As StdPicture
Private myHand_handle As Long

Private Sub Form_Load()
  lblVersion.Caption = "v" & app.Major & "." & app.Minor & "." & Format(app.Revision, "00")
  ' load the system's hand cursor if it exists
  myHand_handle = LoadCursor(0, IDC_HAND)
  If myHand_handle <> 0 Then
    ' use function to convert memory handle to stdPicture
    ' so we can apply it to the MouseIcon
    Set myHandCursor = HandleToPicture(myHand_handle, False)
  End If

  ' this is option #1
  ' Once we set the cursor, we'll never know if the user changed their
  ' mouse theme, but that happens rarely, option #2 you can find in the
  ' Label1 MouseMove function, which will always display the current themed
  ' hand icon regardless if it is changed while the app is running.
  If Not myHandCursor Is Nothing Then
    Label4.MouseIcon = myHandCursor
    Label4.MousePointer = vbCustom
    Label6.MouseIcon = myHandCursor
    Label6.MousePointer = vbCustom
    Label7.MouseIcon = myHandCursor
    Label7.MousePointer = vbCustom
  End If
  ' note that if you used replaced Label1 with Me, not only the form itself,
  ' but all controls on the form would show the hand cursor as long as the
  ' controls have their MousePointer=vbDefault.
End Sub

Private Function HandleToPicture(ByVal hHandle As Long, isBitmap As Boolean) As IPicture
  ' Convert an icon/bitmap handle to a Picture object
  Dim pic As PICTDESC
  Dim guid(0 To 3) As Long
  
  On Error GoTo ExitRoutine
  
  ' initialize the PictDesc structure
  pic.cbSize = Len(pic)
  If isBitmap Then pic.pictType = vbPicTypeBitmap Else pic.pictType = vbPicTypeIcon
  pic.hIcon = hHandle
  ' this is the IPicture GUID {7BF80980-BF32-101A-8BBB-00AA00300CAB}
  ' we use an array of Long to initialize it faster
  guid(0) = &H7BF80980
  guid(1) = &H101ABF32
  guid(2) = &HAA00BB8B
  guid(3) = &HAB0C3000
  ' create the picture,
  ' return an object reference right into the function result
  OleCreatePictureIndirect pic, guid(0), True, HandleToPicture
  
ExitRoutine:
End Function
Private Sub Label4_Click()
  End
End Sub

Private Sub Label4_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  SetCursor myHand_handle
End Sub

Private Sub Label6_Click()
  Dim Button1 As MSComctlLib.Button
  
  'progetto esistente
  Me.Visible = False
  Set Button1 = MabseF_Prj.Toolbar1.Buttons(2)
  Button1.Key = "PRJ_OPEN"
  Button1.Tag = "MAIN"
  MabseF_Prj.Toolbar1_ButtonClick Button1
  Unload Me
End Sub

Private Sub Label6_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  SetCursor myHand_handle
End Sub

Private Sub Label7_Click()
  Dim Button2 As MSComctlLib.Button
  
  'nuovo progetto
  Me.Visible = False
  Set Button2 = MabseF_Prj.Toolbar1.Buttons(1)
  Button2.Key = "PRJ_NEW"
  Button2.Tag = "MAIN"
  MabseF_Prj.Toolbar1_ButtonClick Button2
  Unload Me
End Sub

Private Sub Label7_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  SetCursor myHand_handle
End Sub

