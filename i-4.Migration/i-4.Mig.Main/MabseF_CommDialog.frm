VERSION 5.00
Begin VB.Form MabseF_CommDialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Add Modules to Project"
   ClientHeight    =   4245
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8520
   Icon            =   "MabseF_CommDialog.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   4245
   ScaleWidth      =   8520
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin i4MigMain.RevComDialog RevComDialog 
      Height          =   4215
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   8505
      _ExtentX        =   15002
      _ExtentY        =   7435
   End
End
Attribute VB_Name = "MabseF_CommDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mArray() As String
Private mPath As String

Public Property Get SelectedFileName() As String()
  SelectedFileName = mArray
End Property

Public Property Get SelectedPathName() As String
  SelectedPathName = mPath
End Property

Public Property Let Filter(vFilter As String)
  RevComDialog.Filter = vFilter
End Property

Public Property Let FilterIndex(vIndex As Long)
  RevComDialog.FilterIndex = vIndex
End Property

Private Sub Form_Unload(Cancel As Integer)
  SetFocusWnd MabseF_Prj.hwnd
End Sub

Public Property Let start_Path(objPath As String)
  RevComDialog.DirListBox_Path = objPath
End Property

Private Sub RevComDialog_FileOpen(arFiles() As String, SelPath As String)
  mArray = arFiles
  mPath = SelPath
  'Gestiamo applicativamente il mantenimento dell'ultima directory aperta...
  lastOpen_dir = mPath
  Unload Me
End Sub

Private Sub RevComDialog_UnloadDialog()
  Dim s(0) As String
  Dim sPath As String
  
  mArray = s
  mPath = sPath
  
  Unload Me
End Sub
