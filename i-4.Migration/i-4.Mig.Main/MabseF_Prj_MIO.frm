VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.MDIForm MabseF_Prj 
   BackColor       =   &H8000000C&
   ClientHeight    =   5115
   ClientLeft      =   60
   ClientTop       =   -2160
   ClientWidth     =   12015
   Icon            =   "MabseF_Prj.frx":0000
   NegotiateToolbars=   0   'False
   ScrollBars      =   0   'False
   Begin VB.PictureBox Toolbar2 
      Align           =   1  'Align Top
      Height          =   405
      Left            =   0
      ScaleHeight     =   345
      ScaleWidth      =   11955
      TabIndex        =   1
      Top             =   0
      Width           =   12015
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   990
         Left            =   0
         TabIndex        =   2
         Top             =   0
         Width           =   9000
         _ExtentX        =   15875
         _ExtentY        =   1746
         ButtonWidth     =   609
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         ImageList       =   "imlToolbarIcons"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   29
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PRJ_NEW"
               Object.ToolTipText     =   "Create a new project repository..."
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PRJ_OPEN"
               Object.ToolTipText     =   "Open existing project repository..."
               Object.Tag             =   "MAIN"
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PRJ_OPEN_FAST"
                     Object.Tag             =   "MAIN"
                     Text            =   "Open Project [Fast loading]"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PRJ_OPEN_SLOW"
                     Object.Tag             =   "MAIN"
                     Text            =   "Open project [Slow loading (Rebuild structure)]"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PRJ_ADD_OBJECTS"
               Object.ToolTipText     =   "Add object(s) to this project..."
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "COM_OPENCLOSE_CONNECTION"
               Object.ToolTipText     =   "Open/Close Compiler connection..."
               Object.Tag             =   "MAIN"
               Style           =   1
               Object.Width           =   1e-4
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "COM_COMPILE_OBJS"
               Object.ToolTipText     =   "Compile selected object(s)..."
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PRJ_PARAMETERS"
               Object.ToolTipText     =   "Configure enviroments settings..."
               Object.Tag             =   "BASE"
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PRJ_SYSOBJ_DECLARE"
                     Object.Tag             =   "BASE"
                     Text            =   "System objects declaration..."
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PRJ_ENVIRONMENT"
                     Object.Tag             =   "BASE"
                     Text            =   "Environment Configuration..."
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PRJ_INFO"
               Object.ToolTipText     =   "View info of this project..."
               Object.Tag             =   "BASE"
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PRJ_PRINTS"
               Object.ToolTipText     =   "Print Tools..."
               Object.Tag             =   "BASE"
            EndProperty
            BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "OBJ_EDIT"
               Object.ToolTipText     =   "View with text editor..."
               Object.Tag             =   "MAIN"
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   3
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "OBJ_EDIT_MAP"
                     Object.Tag             =   "MAIN"
                     Text            =   "View with map editor..."
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "OBJ_EDIT_TEXT"
                     Object.Tag             =   "MAIN"
                     Text            =   "View with text editor..."
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "OBJ_EDIT_EXTEDT"
                     Object.Tag             =   "MAIN"
                     Text            =   "View with an external text editor..."
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "OBJ_CHANGE_TYPE"
               Object.ToolTipText     =   "Chage type of selected objec(s)..."
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "OBJ_RESTORE"
               Object.ToolTipText     =   "Restore selected object(s)..."
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "OBJ_DELETE"
               Object.ToolTipText     =   "Delete selected object(s)..."
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button17 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "OBJ_PROPERTIES"
               Object.ToolTipText     =   "View selected object properties..."
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button18 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button19 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SHOW_PRJPARSER"
               Object.Tag             =   "PARSER"
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PARSE_OBJECT_FIRST_LEVEL"
                     Object.Tag             =   "PARSER"
                     Text            =   "Parse selected objects [first lev.]..."
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PARSE_OBJECT_SECOND_LEVEL"
                     Object.Tag             =   "PARSER"
                     Text            =   "Parse selected objects [second lev.]..."
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button20 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button21 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "LST_ORDERDOWN"
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button22 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "LST_ORDERUP"
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button23 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "LST_FILTER_CONF"
               Object.ToolTipText     =   "Refresh R.C. Object"
               Object.Tag             =   "MAIN"
               Style           =   1
            EndProperty
            BeginProperty Button24 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "LST_FILTER_EXECUTE"
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button25 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button26 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PRD_ABOUT"
               Object.Tag             =   "MAIN"
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   3
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PRD_HELP"
                     Object.Tag             =   "MAIN"
                     Text            =   "Help topics..."
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PRD_WEBSITE"
                     Object.Tag             =   "MAIN"
                     Text            =   "Reviser Web site"
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "PRD_ABOUT"
                     Object.Tag             =   "MAIN"
                     Text            =   "About..."
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button27 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button28 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PRJ_EXIT"
               Object.ToolTipText     =   "Close and exit..."
               Object.Tag             =   "MAIN"
            EndProperty
            BeginProperty Button29 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "TreeviewRefresh"
               Object.ToolTipText     =   "Refresh Treeview"
               Object.Tag             =   "TreeviewRefresh"
            EndProperty
         EndProperty
      End
   End
   Begin MSWinsockLib.Winsock wSokComp 
      Left            =   5370
      Top             =   2220
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog CmdDlgUty 
      Left            =   4200
      Top             =   960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   348
      Left            =   0
      TabIndex        =   0
      Top             =   4764
      Width           =   12012
      _ExtentX        =   21193
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
            Key             =   "PRJ"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9975
            Key             =   "MSG"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
            Key             =   "STATUS"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1693
            MinWidth        =   1058
            TextSave        =   "17/10/2006"
            Key             =   "DATE"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1058
            MinWidth        =   1058
            TextSave        =   "17.25"
            Key             =   "TIME"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIconsOLD 
      Left            =   1470
      Top             =   720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   27
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":06E4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":083E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":0998
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":0CB2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":0FCC
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":12E6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1738
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1892
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1CE4
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1E3E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":45F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":6DA2
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":6EFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":7056
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":71B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":730A
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":7464
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":9186
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":92E0
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":943A
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":9594
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":96EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":9848
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":B582
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":B6DC
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":B836
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   3300
      Top             =   2400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   61
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":BB50
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":BCAA
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":BE04
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":BF5E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":C278
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":C592
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":C8AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":CCFE
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":CE58
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":D2AA
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":D404
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":FBB6
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":12368
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":124C2
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1261C
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":12776
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":128D0
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":12A2A
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1474C
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":148A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":14A00
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":14B5A
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":14CB4
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":14E0E
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":16B48
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":16CA2
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":16DFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":17116
            Key             =   ""
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":17F68
            Key             =   ""
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":191EA
            Key             =   ""
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1A46C
            Key             =   ""
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1B2BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1C110
            Key             =   ""
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1D392
            Key             =   ""
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1DC6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":1E546
            Key             =   ""
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":247E0
            Key             =   ""
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2A40A
            Key             =   ""
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2ACE4
            Key             =   ""
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2B5BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2BE98
            Key             =   ""
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2CCEA
            Key             =   ""
         EndProperty
         BeginProperty ListImage43 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2D004
            Key             =   ""
         EndProperty
         BeginProperty ListImage44 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2D31E
            Key             =   ""
         EndProperty
         BeginProperty ListImage45 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2DBF8
            Key             =   ""
         EndProperty
         BeginProperty ListImage46 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2E4D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage47 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2EDAC
            Key             =   ""
         EndProperty
         BeginProperty ListImage48 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2F686
            Key             =   ""
         EndProperty
         BeginProperty ListImage49 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":2FF60
            Key             =   ""
         EndProperty
         BeginProperty ListImage50 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":3083A
            Key             =   ""
         EndProperty
         BeginProperty ListImage51 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":31114
            Key             =   ""
         EndProperty
         BeginProperty ListImage52 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":319EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage53 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":322C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage54 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":32BA2
            Key             =   ""
         EndProperty
         BeginProperty ListImage55 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":3347C
            Key             =   ""
         EndProperty
         BeginProperty ListImage56 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":33D56
            Key             =   ""
         EndProperty
         BeginProperty ListImage57 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":34630
            Key             =   ""
         EndProperty
         BeginProperty ListImage58 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":3494A
            Key             =   ""
         EndProperty
         BeginProperty ListImage59 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":3669C
            Key             =   ""
         EndProperty
         BeginProperty ListImage60 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":383A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage61 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabseF_Prj.frx":38940
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuPopUp 
      Caption         =   "PopUp"
      Visible         =   0   'False
      Begin VB.Menu mnuAddObjs 
         Caption         =   "Add new Objects..."
      End
      Begin VB.Menu mnuAddMissing 
         Caption         =   "&Add Related Items"
      End
      Begin VB.Menu Sep07 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTextEd 
         Caption         =   "Text Editor"
      End
      Begin VB.Menu mnuExternalEditor 
         Caption         =   "EXTERNAL Text Editor"
      End
      Begin VB.Menu mnuMapEd 
         Caption         =   "Map Editor"
      End
      Begin VB.Menu s0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuProperties 
         Caption         =   "Object Properties"
      End
      Begin VB.Menu Sep05 
         Caption         =   "-"
      End
      Begin VB.Menu mnuChangeType 
         Caption         =   "Change Objects type..."
      End
      Begin VB.Menu Sep06 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelObjs 
         Caption         =   "Delete selected Objects"
      End
      Begin VB.Menu mnuRestoreObjs 
         Caption         =   "Restore selected Objects"
      End
      Begin VB.Menu sep99 
         Caption         =   "-"
      End
      Begin VB.Menu mnuParseObjs 
         Caption         =   "Parse selected objects - first level"
      End
      Begin VB.Menu mnuParseObjs2 
         Caption         =   "Parse selected objects - second level"
      End
      Begin VB.Menu s32 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditNotes 
         Caption         =   "Edit object's note..."
      End
      Begin VB.Menu s31 
         Caption         =   "-"
      End
      Begin VB.Menu mnuUpdateObj 
         Caption         =   "Refresh R.C. selected Objects"
      End
      Begin VB.Menu Sep20 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCompileObjs 
         Caption         =   "Compile selected Objects"
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuNewProject 
         Caption         =   "New Project"
      End
      Begin VB.Menu mnuOpenPrj 
         Caption         =   "Open Project..."
      End
      Begin VB.Menu Sep08 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOpenPrjRebuild 
         Caption         =   "Open Project [Rebuild]"
      End
      Begin VB.Menu Sep09 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAddObjects 
         Caption         =   "Add Objects..."
      End
      Begin VB.Menu Sep10 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPropertiesPrj 
         Caption         =   "Project's Properties"
      End
      Begin VB.Menu mnuParamPrj 
         Caption         =   "Parameters"
         Begin VB.Menu mnuSysObjDecl 
            Caption         =   "System Objects Declaration"
         End
         Begin VB.Menu mnuConfigEnv 
            Caption         =   "Environment Configuration"
         End
      End
      Begin VB.Menu Sep11 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPrints 
         Caption         =   "Prints..."
      End
      Begin VB.Menu sep12 
         Caption         =   "-"
      End
      Begin VB.Menu mnuImport 
         Caption         =   "Import"
         Begin VB.Menu mnuImportIms 
            Caption         =   "IMS"
            Begin VB.Menu mnuImportImsCat 
               Caption         =   "Catalog..."
            End
         End
         Begin VB.Menu mnuImportCics 
            Caption         =   "CICS"
            Begin VB.Menu mnuImportCicsPCT 
               Caption         =   "PCT..."
            End
            Begin VB.Menu mnuImportCicsFCT 
               Caption         =   "FCT..."
            End
            Begin VB.Menu mnuImportCicsDFHCSCUP 
               Caption         =   "DFHCSCUP..."
            End
         End
      End
      Begin VB.Menu cc 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCheck 
         Caption         =   "Check Repository"
      End
      Begin VB.Menu sep14 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuCompilers 
      Caption         =   "Compilers"
      Begin VB.Menu mnuOpenConnection 
         Caption         =   "Open Connection..."
      End
      Begin VB.Menu mnuConfigCompilers 
         Caption         =   "Configure..."
      End
      Begin VB.Menu Sep15 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCompile 
         Caption         =   "Compile selected Objects..."
      End
      Begin VB.Menu mnuCompileErr 
         Caption         =   "Compiling Errors"
      End
   End
   Begin VB.Menu mnuObject 
      Caption         =   "Object"
      Begin VB.Menu mnuAddObjsObj 
         Caption         =   "Add new Objects..."
      End
      Begin VB.Menu addMissing 
         Caption         =   "&Add Related Items"
      End
      Begin VB.Menu Sep19 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTextEditor 
         Caption         =   "View with Text Editor"
      End
      Begin VB.Menu mnuEXTTextEditor 
         Caption         =   "View with an EXTERNAL Text Editor"
      End
      Begin VB.Menu mnuMapEditor 
         Caption         =   "View with Map Editor"
      End
      Begin VB.Menu Sep17 
         Caption         =   "-"
      End
      Begin VB.Menu mnuObjectProperties 
         Caption         =   "View Object Properties..."
      End
      Begin VB.Menu sep13 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelObjects 
         Caption         =   "Delete Objects..."
      End
      Begin VB.Menu mnuRestoreObjects 
         Caption         =   "Restore Objects..."
      End
      Begin VB.Menu s30 
         Caption         =   "-"
      End
      Begin VB.Menu mnuParseObjsMenu 
         Caption         =   "Parse Objects [first Lev.]..."
      End
      Begin VB.Menu mnuParseObjsMenu2 
         Caption         =   "Parse Objects [second Lev.]..."
      End
      Begin VB.Menu s33 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditNotesMenu 
         Caption         =   "Edit Object's note..."
      End
      Begin VB.Menu Sep18 
         Caption         =   "-"
      End
      Begin VB.Menu mnuChangeObjectsType 
         Caption         =   "Change Objects' Type..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "?"
      Begin VB.Menu mnuHelpTopics 
         Caption         =   "Help Topics..."
      End
      Begin VB.Menu mnuRevWeb 
         Caption         =   "Reviser Web Site"
      End
      Begin VB.Menu Sep16 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAbout 
         Caption         =   "About..."
      End
   End
End
Attribute VB_Name = "MabseF_Prj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Function Genera_Connection_String(nColl As Collection, TypeConn As String)
   'Collection structure
   '1) Percorso
   '2) Provider Type        (Sys)
   '3) Provider DB Progetto (Sys)
   '4) Security Info        (Sys)
   '5) Id                   (Sys)
   '6) Data Source          (Sys)
   '7) Database             (Sys)
   
   Dim cnStr As String
   
   If Trim(UCase(TypeConn)) = "SYSTEM" Then
   
      Select Case Trim(UCase(nColl.Item(2)))
         Case "SQLSERVER"
            cnStr = "Provider=" & nColl.Item(3) & "Persist Security Info=" & nColl.Item(7) & _
                    "User ID=" & nColl.Item(5) & ";Data Source=" & nColl.Item(6)
                         
         Case "ORACLE"
            DLLFunzioni.Show_MsgBoxError "MB01P"
            
         Case "ACCESS"

            'Prova a vedere se il file � presente
            If Dir(GbSysMdbPath & "\" & nColl.Item(7), vbNormal) <> "" Then
               cnStr = "Provider=" & nColl.Item(3) & ";Data Source=" & GbSysMdbPath & "\" & nColl.Item(7) & ";" _
                         & "Persist Security Info=" & nColl.Item(4)
            Else
               'Va in locale
               'lo scrive nella finestra di log
               
               cnStr = "Provider=" & nColl.Item(3) & ";Data Source=" & nColl.Item(1) & "\System\" & nColl.Item(7) & ";" _
                         & "Persist Security Info=" & nColl.Item(4)
            
            End If
                         
         Case "MYSQL"
            DLLFunzioni.Show_MsgBoxError "MB02P"
            
      End Select
   Else
     ' Stop
   End If
   
   Genera_Connection_String = cnStr
End Function
'''''''''''''''''''''''''''''''''''
' "Progressione Arborescente" (?!)
'''''''''''''''''''''''''''''''''''
Private Sub addMissing_Click()
  Dim i As Long
  Dim cParam As Collection
  
  On Error GoTo errorHandler
  
  Screen.MousePointer = vbHourglass
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  
  For i = 1 To MabseF_List.GridList.ListItems.Count
    If MabseF_List.GridList.ListItems(i).Selected Then
      Set cParam = New Collection
      cParam.Add MabseF_List.GridList.ListItems(i).Text
      cParam.Add MabseF_List.GridList.ListItems(i).SubItems(2)
      DLLFunzioni.FnActiveWindowsBool = False
      DllParser.AttivaFunzione "ADD_MISSING_OBJECTS", cParam
      DLLFunzioni.FnActiveWindowsBool = True
    End If
  Next
  
  ''''''''''''''''''''''''''''''''''''''''
  ' Update treeview
  ''''''''''''''''''''''''''''''''''''''''
  Carica_TreeView_Progetto_Fast MabseF_List.PrjTree
  
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  Screen.MousePointer = vbDefault
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "RC0002"
End Sub

Private Sub MDIForm_Load()

  Toolbar1.ImageList = imlToolbarIcons
  Toolbar1.Buttons(1).Image = 41
  Toolbar1.Buttons(2).Image = 58
  Toolbar1.Buttons(3).Image = 31
  Toolbar1.Buttons(4).Image = 0
  Toolbar1.Buttons(5).Image = 45
  Toolbar1.Buttons(6).Image = 38
  Toolbar1.Buttons(7).Image = 0
  Toolbar1.Buttons(8).Image = 46
  Toolbar1.Buttons(9).Image = 0
  Toolbar1.Buttons(10).Image = 50
  Toolbar1.Buttons(11).Image = 35
  Toolbar1.Buttons(12).Image = 0
  Toolbar1.Buttons(13).Image = 61
  Toolbar1.Buttons(14).Image = 25
  Toolbar1.Buttons(15).Image = 60
  Toolbar1.Buttons(16).Image = 16
  Toolbar1.Buttons(17).Image = 17
  Toolbar1.Buttons(18).Image = 0
  Toolbar1.Buttons(19).Image = 30
  Toolbar1.Buttons(20).Image = 0
  Toolbar1.Buttons(21).Image = 19
  Toolbar1.Buttons(22).Image = 20
  Toolbar1.Buttons(23).Image = 26
  Toolbar1.Buttons(24).Image = 21
  Toolbar1.Buttons(24).Visible = False
  Toolbar1.Buttons(25).Image = 0
  Toolbar1.Buttons(26).Image = 33
  Toolbar1.Buttons(27).Image = 0
  Toolbar1.Buttons(28).Image = 24
  Toolbar1.Buttons(28).Visible = False
  Toolbar1.Buttons(29).Image = 42
  Toolbar1.Buttons(29).Visible = True
  
  

'    SetWindowMDIToolWindow frmToolWindow, MabseF_Prj 'ALE

 'Riattivare prima di compilare
  StatusBar1.Panels(2).Text = "START OPENING a NEW PROJECT or an EXISTING PROJECT" 'ALE
  GbFirstLoad = True
  
  'Scrive ctrl
  fixCtrl
  
  Elimina_Flickering Me.hWnd
    
  Clessidra False
  
  'Setta le variabili che devono essere viste dalle DLL
  Setta_Variabili_Export_DLL
  
  'Carica il menu in memoria
  ReDim TotMenu(0)
  
  'SQ - 16-02-06
  'Me.Caption = GbNomeProdotto & " -- " & GbRagSocCli
  Caption = app.ProductName & " " & app.Comments
  
  'Prende il nome del computer (ANCORA?)
  GbComputerName = GetComputerNameU
  
  'Lancia La Form per la scelta della Modalit� del Progetto
  'MabseF_Scelta.Show vbModal
  'Forza a mano le modalit� : In futuro se necessario verr� reimplementato il blocco sui nomi ecc...
  GbModePrj = DLLLic.Lic_NomePC
  GbCodePrj = "0000-0000-0000"
  GbPswUser = GbModePrj
  GbOpenMode = GbModePrj
  
  'Verifica DLL Funzioni
  GbDllFunzioniExist = DllFunzioniExist
  If GbDllFunzioniExist Then DllFunzioniInit
  
  'verifica DLLBase
  GbDllBaseExist = DllBaseExist
  If GbDllBaseExist Then
    
    GbImgDir = GbPathPrd & "\" & GbPathImg
    
    DllBaseInit
    
    DllBase.BsPathPrd = GbPathPrd
    
    'Carica i parametri del prodotto
    'SQ  - ancora? DllBase.Carica_Parametri_Prodotto
  End If
  
  'Carica La DLL Del Cliente
'  Clessidra True
'  GbFileUserExist = FileUserExist
  Clessidra False
  
  'verifica DLLParser
  GbDllParserExist = DllParserExist
  If GbDllParserExist Then DllParserInit
  'verifica DLL Dli2RDBMS
  GbDLLDli2RdbMsExist = DLLDli2RdbMsExist
  If GbDLLDli2RdbMsExist Then DLLDli2RdbMsInit
  'verifica DLL Data Manager
  GbDllDataManExist = DllDataManExist
  If GbDllDataManExist Then DllDataManInit (GbDLLDli2RdbMsExist)
  'verifica la DLL Tools
  GbDllToolsExist = DLLToolsExist
  If GbDllToolsExist Then DllToolsInit
  'verifica la DLL Analisi
  GbDllAnalisiExist = DLLAnalisiExist
  If GbDllAnalisiExist Then DllAnalisiInit
  'Verifica DLL Ims
  GbDllImsExist = DLLImsExist
  If GbDllImsExist Then DllImsInit
 
  'carica il menu a tendina
  'Call Carica_ARRAY_Menu_A_Tendina
  'carica la toolbar del mdi form
  'Call Carica_ARRAY_TBar_MDI
  
  GbFinImCheck = True
  
  'calcola le dimesioni massime che la MDI pu� avere
  Calcola_Dimensioni_Max
  
  'Carica_Parametri_Prodotto
  GbMaxheight = 7500
  
  Modify_form
    
  Terminate_Flickering

  'Resize
  ResizeAll GbFirstLoad
  
  mnuTextEd.Enabled = False
  mnuMapEd.Enabled = False
  'mnuExternalEditor.Enabled = False
  mnuEXTTextEditor.Enabled = False
  mnuProperties.Enabled = False
  
  initProject = Command$

  If GbFirstLoad And initProject = "" Then
    MabseF_prgAvvio.Show vbModal
  ElseIf initProject <> "" Then
    Dim Button1 As MSComctlLib.Button
    Set Button1 = MabseF_Prj.Toolbar1.Buttons(2)
    Button1.Key = "PRJ_OPEN"
    Button1.Tag = "MAIN"
    MabseF_Prj.Toolbar1_ButtonClick Button1
  End If
  GbFirstLoad = False 'ALE
  
'  If GbLicType = "" Then
'    Toolbar1.Buttons(1).Enabled = False
'    MabseF_Menu.cmdStr(1).Enabled = False
'  End If
  
End Sub

Private Sub Modify_form()
  'Rimuove i bottoni minimize e maximize
  'Call Elimina_Button_MDI(Me, False, True)
  
  'Disattiva menu
  'FILE
  mnuPrints.Enabled = False
  mnuAddObjects.Enabled = False
  mnuPropertiesPrj.Enabled = False
  mnuParamPrj.Enabled = False
  'Compiler
  mnuCompilers.Enabled = False
  'Objects
  mnuObject.Enabled = False
  'Help
  mnuHelp.Enabled = False

  'disattiva bottoni toolbar
  ON_OFF_Button_ToolBar Toolbar1, False
  
  'Open e New project
  ON_OFF_Button_ToolBar Toolbar1, True, 1
  ON_OFF_Button_ToolBar Toolbar1, True, 2
  
  'Help ed Esci
  ON_OFF_Button_ToolBar Toolbar1, True, 25
  ON_OFF_Button_ToolBar Toolbar1, True, 26
   
  'Help topics
  If Dir(GbPathPrd & "\Help\" & c_FileHelp, vbNormal) <> "" Then
    Toolbar1.Buttons(26).ButtonMenus(1).Enabled = True
     mnuHelpTopics.Enabled = True
  Else
    Toolbar1.Buttons(26).ButtonMenus(1).Enabled = False
    mnuHelpTopics.Enabled = False
  End If
  
  'Editor Esterno
  If DLLFunzioni.FnExternalEditor = "" Then
      mnuExternalEditor.Enabled = False
      mnuExternalEditor.Visible = False
  Else
      mnuExternalEditor.Enabled = True
      mnuExternalEditor.Visible = True
  End If
  
  'Menu editor esterno
  mnuEXTTextEditor.Visible = mnuExternalEditor.Visible
  
  Toolbar1.Buttons(13).ButtonMenus(3).Enabled = mnuExternalEditor.Enabled
  Toolbar1.Buttons(13).ButtonMenus(3).Visible = mnuExternalEditor.Visible
End Sub

Private Sub MDIForm_Resize()
   Dim i As Long
   Dim wObj As Object

   On Error GoTo ERR

   If GbFirstLoad = True Then Exit Sub

   If Me.WindowState <> vbMinimized Then
      ResizeAll GbFirstLoad

      For i = 1 To DLLFunzioni.CountActiveWindows
         Set wObj = DLLFunzioni.ItemActiveWindows(i)
            Dim f As Form 'ALE
            Set f = wObj
            f.WindowState = vbNormal
            f.Move 0, MabseF_List.Top, MabseF_List.Width, MabseF_List.Height
         'DLLFunzioni.ResizeControls wObj
      Next i
   End If

Exit Sub

ERR:
  If ERR.Number = 384 Then
    Terminate_Flickering
    Exit Sub
  End If
  
  Terminate_Flickering
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
  Dim i As Long
  Dim wObj As Object
   
  fixCtrl
  
  'Scarica tutte le risorse allocate
  For i = DLLFunzioni.CountActiveWindows To 1 Step -1
    Set wObj = DLLFunzioni.ItemActiveWindows(i)
    Unload wObj
    Set wObj = Nothing
  Next i
  ''''''''''''''''''''''''''''''''''''''''
  ' Rimaneva appeso il processo!!!!!!!!!!
  ''''''''''''''''''''''''''''''''''''''''
  End
End Sub

Private Sub mnuAbout_Click()
'   Toolbar1_ButtonMenuClick Toolbar1.Buttons(24).ButtonMenus(3)
Attiva_Funzione "MAIN", New Collection, "PRD_ABOUT"
End Sub

Private Sub mnuAddMissing_Click()
  addMissing_Click
End Sub
Private Sub mnuAddObjects_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(3)
End Sub
Private Sub mnuAddObjs_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(3)
End Sub
Private Sub mnuAddObjsObj_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(3)
End Sub
Private Sub mnuChangeObjectsType_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(14)
End Sub
Private Sub mnuChangeType_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(14)
End Sub

Public Sub mnuCheck_Click()

  If checkRepository Then
    MsgBox "The Project Repository matches the " & MabseF_Prj.Caption & " requirements ", vbInformation, "Reviser"
    MabseF_Check.Show vbModal
  Else
   MabseF_Check.Show vbModal
  End If

End Sub

Public Function checkRepository() As Boolean
 Dim strconnection As String
 Dim rsCampo As New Recordset
 Dim rs As Recordset
 Dim sCurrentTable As String
 Dim sNewTable As String

 Dim arrayTabsC() As String
 Dim arrayTabsP() As String
 Dim customerColumns() As String
 Dim prjColumns() As String
 Dim i As Long
 
  checkRepository = False
  strconnection = DLLFunzioni.FnConnDBSys
  strconnection = Replace(strconnection, "System.mdb", "prj.mty")

  Set PrjConnection = New ADODB.Connection
  PrjConnection.ConnectionString = strconnection
  PrjConnection.Open PrjConnection.ConnectionString
  
  Set CustomerConnection = New ADODB.Connection
  strconnection = DLLFunzioni.FnConnection
  CustomerConnection.ConnectionString = strconnection
  CustomerConnection.Open CustomerConnection.ConnectionString
'  Set CustomerConnection = DLLFunzioni.FnConnection
  
  Set rs = New Recordset
  Set rs = PrjConnection.OpenSchema(adSchemaColumns) 'adSchemaTables, Array(Empty, Empty, Empty, "TABLE"))
  
  sCurrentTable = ""
  sNewTable = ""
  i = 0
  Set prjTabs = New Collection
  ReDim arrayTabsP(0)
  
  Do Until rs.EOF
  sCurrentTable = rs!TABLE_NAME
  If (sCurrentTable <> sNewTable) Then
    If sNewTable <> "" Then
      ReDim Preserve arrayTabsP(prjTabs.Count)
      arrayTabsP(prjTabs.Count) = UCase(sNewTable)
      prjTabs.Add prjColumns, UCase(sNewTable)
      i = 0
    End If
    sNewTable = rs!TABLE_NAME
    ReDim prjColumns(0)
    'Debug.Print "Current Table: " & rs!TABLE_NAME
  End If
    ReDim Preserve prjColumns(i)
    prjColumns(i) = rs!COLUMN_NAME & ";" & ConvType(rs!DATA_TYPE, IIf(IsNull(rs!CHARACTER_MAXIMUM_LENGTH), "", rs!CHARACTER_MAXIMUM_LENGTH)) & ";" & rs!CHARACTER_MAXIMUM_LENGTH & ";" & rs!ORDINAL_POSITION 'comincia a contare da 1
    'Debug.Print " Field: " & rs!COLUMN_NAME
    rs.MoveNext
    i = i + 1
  Loop
  
  Set rs = New Recordset
  Set rs = CustomerConnection.OpenSchema(adSchemaColumns) 'adSchemaTables, Array(Empty, Empty, Empty, "TABLE"))

  sCurrentTable = ""
  sNewTable = ""
  i = 0
  Set customerTabs = New Collection
  ReDim arrayTabsC(0)
  
  Do Until rs.EOF
  sCurrentTable = rs!TABLE_NAME
  If (sCurrentTable <> sNewTable) Then
    If sNewTable <> "" Then
      ReDim Preserve arrayTabsC(customerTabs.Count)
      arrayTabsC(customerTabs.Count) = UCase(sNewTable)
      customerTabs.Add customerColumns, UCase(sNewTable)
      i = 0
    End If
    sNewTable = rs!TABLE_NAME
    ReDim customerColumns(0)
    'Debug.Print "Current Table: " & rs!TABLE_NAME
  End If
    ReDim Preserve customerColumns(i)
    customerColumns(i) = rs!COLUMN_NAME & ";" & ConvType(rs!DATA_TYPE, IIf(IsNull(rs!CHARACTER_MAXIMUM_LENGTH), "", rs!CHARACTER_MAXIMUM_LENGTH)) & ";" & rs!CHARACTER_MAXIMUM_LENGTH & ";" & rs!ORDINAL_POSITION   'comincia a contare da 1
    'Debug.Print " Field: " & rs!COLUMN_NAME
    rs.MoveNext
    i = i + 1
  Loop
  
  On Error GoTo errorH
  Dim c As Long
  Dim z As Long
  Dim boolTrovato As Boolean
  Dim boolTab As Boolean
  Dim prjField() As String
  Dim prjCustomer() As String
  
  MabseF_Check.lswDifferences.ListItems.Clear
  MabseF_Check.lswDifferences.SmallIcons = MabseF_Check.ImageList1
  
  For i = 0 To UBound(arrayTabsP)
    Dim appArrayP() As String
    Dim appArrayC() As String
    appArrayP = prjTabs(arrayTabsP(i))
    boolTab = True
    appArrayC = customerTabs(arrayTabsP(i))
    If boolTab Then
      For c = 0 To UBound(appArrayP)
        boolTrovato = False
        For z = 0 To UBound(appArrayC)
            prjField() = Split(appArrayP(c), ";") 'nome;type;len
            prjCustomer() = Split(appArrayC(z), ";")
         If UCase(prjField(0)) = UCase(prjCustomer(0)) Then 'If appArrayP(c) = appArrayC(z) Then
           boolTrovato = True
           Exit For
         End If
        Next
        If Not boolTrovato Then
          MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i) & "." & prjField(0), , 4 'appArrayP(c)
        Else
          'controllo il tipo e la lunghezza del campo
          If Not prjField(1) = prjCustomer(1) Then
                MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i) & "." & prjField(0) & " Type(" & prjField(1) & ")", , 3
          End If
          If Not prjField(2) = prjCustomer(2) Then
                MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i) & "." & prjField(0) & " Len(" & prjField(2) & ")", , 2
          End If
        End If
      Next
    End If
  Next
  
  If MabseF_Check.lswDifferences.ListItems.Count = 0 Then
    checkRepository = True
  End If
'  If MabseF_Check.lswDifferences.ListItems.Count = 0 Then
'    MsgBox "The Project Repository matches the " & MabseF_Prj.Caption & " requirements ", vbInformation, "Reviser"
'  Else
'    MabseF_Check.Show vbModal
'  End If
  Exit Function
errorH:
  MabseF_Check.lswDifferences.ListItems.Add , , arrayTabsP(i), , 1
  boolTab = False
  Resume Next
End Function
Private Function ConvType(ByVal TypeVal As Long, MaxLen As String) As String
  Select Case TypeVal
        Case adBigInt                    ' 20
            ConvType = "Big Integer"
        Case adBinary                    ' 128
            ConvType = "Binary"
        Case adBoolean                   ' 11
            ConvType = "Boolean"
        Case adBSTR                      ' 8 i.e. null terminated string
            ConvType = "Text"
        Case adChar                      ' 129
            ConvType = "Text"
        Case adCurrency                  ' 6
            ConvType = "Currency"
        Case adDate                      ' 7
            ConvType = "Date/Time"
        Case adDBDate                    ' 133
            ConvType = "Date/Time"
        Case adDBTime                    ' 134
            ConvType = "Date/Time"
        Case adDBTimeStamp               ' 135
            ConvType = "Date/Time"
        Case adDecimal                   ' 14
            ConvType = "Float"
        Case adDouble                    ' 5
            ConvType = "Float"
        Case adEmpty                     ' 0
            ConvType = "Empty"
        Case adError                     ' 10
            ConvType = "Error"
        Case adGUID                      ' 72
            ConvType = "GUID"
        Case adIDispatch                 ' 9
            ConvType = "IDispatch"
        Case adInteger                   ' 3
            ConvType = "Integer"
        Case adIUnknown                  ' 13
            ConvType = "Unknown"
        Case adLongVarBinary             ' 205
            ConvType = "Binary"
        Case adLongVarChar               ' 201
            ConvType = "Text"
        Case adLongVarWChar              ' 203
            ConvType = "Text"
        Case adNumeric                  ' 131
            ConvType = "Long"
        Case adSingle                    ' 4
            ConvType = "Single"
        Case adSmallInt                  ' 2
            ConvType = "Small Integer"
        Case adTinyInt                   ' 16
            ConvType = "Tiny Integer"
        Case adUnsignedBigInt            ' 21
            ConvType = "Big Integer"
        Case adUnsignedInt               ' 19
            ConvType = "Integer"
        Case adUnsignedSmallInt          ' 18
            ConvType = "Small Integer"
        Case adUnsignedTinyInt           ' 17
            ConvType = "Timy Integer"
        Case adUserDefined               ' 132
            ConvType = "UserDefined"
        Case adVarNumeric                 ' 139
            ConvType = "Long"
        Case adVarBinary                 ' 204
            ConvType = "Binary"
        Case adVarChar                   ' 200
            ConvType = "Text"
        Case adVariant                   ' 12
            ConvType = "Variant"
        Case adVarWChar                  ' 202
            ConvType = "Text"
        Case adWChar                     ' 130
            If MaxLen = "0" Then
              ConvType = "Memo" 'Text
            Else
              ConvType = "Text"
            End If
        Case Else
            ConvType = "Unknown"
   End Select
End Function

Private Sub mnuCompile_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(6)
End Sub
Private Sub mnuCompileErr_Click()
    SetParent MabseF_CompErrors.hWnd, MabseF_List.hWnd
    MabseF_CompErrors.Show
    MabseF_CompErrors.Move 0, 0, MabseF_List.Width, MabseF_List.Height
End Sub
Private Sub mnuCompileObjs_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(6)
End Sub
Private Sub mnuConfigCompilers_Click()
   Toolbar1_ButtonMenuClick Toolbar1.Buttons(8).ButtonMenus(2)
End Sub
'
'Private Sub mnuConfigEnv_Click()
'   Toolbar1_ButtonMenuClick Toolbar1.Buttons(8).ButtonMenus(1)
'End Sub
Private Sub mnuDelObjects_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(16)
End Sub
Private Sub mnuDelObjs_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(16)
End Sub
Private Sub mnuEditNotes_Click()
   Dim oForm As Object
   Dim i As Long
   
   If DLLFunzioni.FnProcessRunning Then
      DLLFunzioni.Show_MsgBoxError "FB01I"
      Exit Sub
   End If
   
   Set oForm = New MabseF_Notes
   
   oForm.setForm GbOggSelNotes
   
   oForm.Show vbModal
   
   'setta la listview a video
   For i = 1 To MabseF_List.GridList.ListItems.Count
      If MabseF_List.GridList.ListItems(i).Selected Then
         If Trim(oForm.newNote) <> "" Then
            MabseF_List.GridList.ListItems(i).ListSubItems(5).Text = "X"
            MabseF_List.GridList.ListItems(i).ListSubItems(5).Bold = True
         Else
            MabseF_List.GridList.ListItems(i).ListSubItems(5).Text = ""
         End If
      End If
   Next i
   
   If oForm.goUnload Then
      Unload oForm
      Set oForm = Nothing
   End If
End Sub

Private Sub mnuEditNotesMenu_Click()
   Dim oForm As Object
   Dim i As Long
   
   Set oForm = New MabseF_Notes
   
   oForm.setForm GbOggSelNotes
   
   oForm.Show vbModal
   
   'setta la listview a video
   For i = 1 To MabseF_List.GridList.ListItems.Count
      If MabseF_List.GridList.ListItems(i).Selected = True Then
         If Trim(oForm.newNote) <> "" Then
            MabseF_List.GridList.ListItems(i).ListSubItems(5).Text = "X"
            MabseF_List.GridList.ListItems(i).ListSubItems(5).Bold = True
         Else
            MabseF_List.GridList.ListItems(i).ListSubItems(5).Text = ""
         End If
      End If
   Next i
   
   If oForm.Unload = vbYes Then
      Unload oForm
      Set oForm = Nothing
   End If
End Sub

Private Sub mnuExit_Click()
   fixCtrl
   End
End Sub

Private Sub mnuExternalEditor_Click()
   'Avvia un editor esterno
   DLLFunzioni.Show_TextEditor GbIdOggetto, True, , MabseF_List
End Sub

Private Sub mnuEXTTextEditor_Click()
   Toolbar1_ButtonMenuClick Toolbar1.Buttons(13).ButtonMenus(3)
End Sub

Private Sub mnuHelpTopics_Click()
   Toolbar1_ButtonMenuClick Toolbar1.Buttons(24).ButtonMenus(1)
End Sub

Private Sub mnuImportCicsDFHCSCUP_Click()
  'Me.Move Screen.Width / 4, Screen.Height / 4, 1, 1 'ALE.
  On Error GoTo parseErr
  dialogFileName = LoadCommDialog(True, "Select a DFHCSDUP file", "All Files", "*")
      
  If Len(dialogFileName) Then
    Screen.MousePointer = vbHourglass
    DllParser.parseDFHCSDUP (dialogFileName)
    Screen.MousePointer = vbNormal
  End If
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & ERR.Description
End Sub

Private Sub mnuImportCicsPCT_Click()
  'Me.Move Screen.Width / 4, Screen.Height / 4, 1, 1 'ALE.
  On Error GoTo parseErr
  dialogFileName = LoadCommDialog(True, "Select a PCT file", "All Files", "*")
      
  If Len(dialogFileName) Then
    Screen.MousePointer = vbHourglass
    DllParser.parsePCT (dialogFileName)
    Screen.MousePointer = vbNormal
  End If
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & ERR.Description
End Sub

Private Sub mnuImportImsCat_Click()
  'Me.Move Screen.Width / 4, Screen.Height / 4, 1, 1 'ALE.
  On Error GoTo parseErr
  dialogFileName = LoadCommDialog(True, "Select a Catalog file", "All Files", "*")
      
  If Len(dialogFileName) Then
    Screen.MousePointer = vbHourglass
    DllParser.parseAPPLCTN (dialogFileName)
    Screen.MousePointer = vbNormal
  End If
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & ERR.Description
End Sub

Private Sub mnuImportCicsFCT_Click()
  'Me.Move Screen.Width / 4, Screen.Height / 4, 1, 1 'ALE.
  On Error GoTo parseErr
  dialogFileName = LoadCommDialog(True, "Select a FCT file", "All Files", "*")
      
  If Len(dialogFileName) Then
    Screen.MousePointer = vbHourglass
    DllParser.parseFCT (dialogFileName)
    Screen.MousePointer = vbNormal
  End If
  Exit Sub
parseErr:
  'tmp - gestire
  MsgBox "tmp: " & ERR.Description
End Sub

Private Sub mnuMapEd_Click()
   Toolbar1_ButtonMenuClick Toolbar1.Buttons(13).ButtonMenus(1)
End Sub

Private Sub mnuMapEditor_Click()
   Toolbar1_ButtonMenuClick Toolbar1.Buttons(13).ButtonMenus(1)
End Sub

Private Sub mnuNewProject_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(1)
End Sub

Private Sub mnuObjectProperties_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(17)
End Sub

Private Sub mnuOpenConnection_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(5)
End Sub

Private Sub mnuOpenPrj_Click()
   Toolbar1_ButtonMenuClick Toolbar1.Buttons(2).ButtonMenus(1)
End Sub

Private Sub mnuOpenPrjRebuild_Click()
   Toolbar1_ButtonMenuClick Toolbar1.Buttons(2).ButtonMenus(2)
End Sub

Private Sub mnuParseObjs_Click()
   mnuParseObjsMenu_Click
End Sub

Private Sub mnuParseObjs2_Click()
   mnuParseObjsMenu2_Click
End Sub

Public Sub mnuParseObjsMenu_Click()
   'Parser degli oggetti selezionati
   Dim i As Long
   Dim cParam As Collection
   
   On Error GoTo errorHandler

   If DLLFunzioni.FnProcessRunning Then
      DLLFunzioni.Show_MsgBoxError "FB01I"
      Exit Sub
   End If
   
   Screen.MousePointer = vbHourglass
   
   DLLFunzioni.FnActiveWindowsBool = True
   DLLFunzioni.FnProcessRunning = True
   
   '23-01-06: faceva dentro il parsing 1000 volte le stesse cose...
   DllParser.AttivaFunzione "INIT_PARSING", cParam
   For i = 1 To MabseF_List.GridList.ListItems.Count
      If MabseF_List.GridList.ListItems(i).Selected Then
         Set cParam = New Collection
         cParam.Add CLng(MabseF_List.GridList.ListItems(i).Text)
         DLLFunzioni.FnActiveWindowsBool = False
         DllParser.AttivaFunzione "PARSE_OBJECT_FIRST_LEVEL", cParam
         DLLFunzioni.FnActiveWindowsBool = True
      End If
   Next i
   
   DLLFunzioni.FnActiveWindowsBool = False
   DLLFunzioni.FnProcessRunning = False
   
   Screen.MousePointer = vbDefault
   
   Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "ML04E"
End Sub

Public Sub mnuParseObjsMenu2_Click()
  'Parser degli oggetti selezionati
  Dim i As Long
  Dim cParam As Collection
  
  If DLLFunzioni.FnProcessRunning Then
     DLLFunzioni.Show_MsgBoxError "FB01I"
     Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  
  DLLFunzioni.FnActiveWindowsBool = True
  DLLFunzioni.FnProcessRunning = True
  
   '23-01-06: faceva dentro il parsing 1000 volte le stesse cose...
  DllParser.AttivaFunzione "INIT_PARSING", cParam
  For i = 1 To MabseF_List.GridList.ListItems.Count
    If MabseF_List.GridList.ListItems(i).Selected Then
      Set cParam = New Collection
      cParam.Add CLng(MabseF_List.GridList.ListItems(i).Text)
      DLLFunzioni.FnActiveWindowsBool = False
      DllParser.AttivaFunzione "PARSE_OBJECT_SECOND_LEVEL", cParam
      DLLFunzioni.FnActiveWindowsBool = True
    End If
  Next i
  
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
End Sub

Private Sub mnuPrints_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(11)
End Sub

Private Sub mnuProperties_Click()
   Elimina_Flickering Me.hWnd
   Dim OldFinCheck As Boolean
   Dim wPrc As String
   
   If Trim(OggSel.Estensione) = "" Then
      wPrc = OggSel.Directory_Input & "\" & OggSel.Nome
   Else
      wPrc = OggSel.Directory_Input & "\" & OggSel.Nome & "." & OggSel.Estensione
   End If
   
   If Dir(wPrc, vbNormal) = "" Then
      DLLFunzioni.Show_MsgBoxError "MP01I"
      Exit Sub
   End If
   
   'visualizza le propriet� dell'oggetto Selezionato
   If Trim(OggSel.Nome) = "" Or Trim(OggSel.Directory_Input) = "" Then
      DLLFunzioni.Show_MsgBoxError "MP02I"
   Else
      Unload MabseF_Log
      OldFinCheck = GbFinImCheck
      GbFinImCheck = False
      MabseF_Prj.ResizeAll

      DLLFunzioni.Show_ObjectProperties OggSel, MabseF_List
      
      GbFinImCheck = OldFinCheck
     ' MabseF_Prj.ResizeAll
   End If
   
   Terminate_Flickering
End Sub

Private Sub mnuPropertiesPrj_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(10)
End Sub

Private Sub mnuRestoreObjects_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(15)
End Sub

Private Sub mnuRestoreObjs_Click()
   Toolbar1_ButtonClick Toolbar1.Buttons(15)
End Sub

Private Sub mnuRevWeb_Click()
   'Toolbar1_ButtonMenuClick Toolbar1.Buttons(24).ButtonMenus(2)
   Attiva_Funzione "MAIN", New Collection, "PRD_WEBSITE"
End Sub

Private Sub mnuSysObjDecl_Click()
   Toolbar1_ButtonMenuClick Toolbar1.Buttons(8).ButtonMenus(1)

End Sub

Private Sub mnuConfigEnv_Click()
   Toolbar1_ButtonMenuClick Toolbar1.Buttons(8).ButtonMenus(2)
End Sub

Private Sub mnuTextEd_Click()
   If InStr(1, OggSel.Nome, "$") > 0 Then
      DLLFunzioni.Show_MsgBoxError "MP03I"
      Exit Sub
   End If
   
   Toolbar1_ButtonClick Toolbar1.Buttons(13)
End Sub

Private Sub mnuTextEditor_Click()
   Toolbar1_ButtonMenuClick Toolbar1.Buttons(13).ButtonMenus(2)
End Sub

Public Sub AttivaFunzioneMain(TagFunction As String, Optional cParam As Collection, Optional boolRefresh As Boolean)
   Dim OldMouse As Variant
   Dim wp As Long
   Dim s As String
   Dim wposiz As Long
   Dim lPrj As Long
   Dim i As Long
   Dim wPos As Long
   Dim wRetWeb As Long
   
   Dim bCompile As Boolean
   
   Dim Directory As String
   Dim NomePrj As String
   Dim wDbOk As Boolean
  
   Static AppNomeDB As String
   
   Dim DllOpen As String
   Dim FuncActive As Boolean
   
   Dim Connection As Boolean
   
   Dim ToolTipStatus As String
   
   Dim TotObj As Long
   
   Dim ExFile As String
   Dim Annulla As Boolean
   Dim Cancella As Long
   Dim oForm As MabseF_SelectDatabase
   
   Dim OldFinCheck As Boolean
  
  
  On Error GoTo errorHandler
  
   If Not FuncActive Then
      Select Case UCase(Trim(TagFunction))
         Case "PRJ_OPEN", "PRJ_OPEN_FAST", "PRJ_OPEN_SLOW"
            DLLFunzioni.FnActiveWindowsBool = True
            
            MabseF_List.GridList.ListItems.Clear
          
            'Controlla il tipo di DB
            Set oForm = New MabseF_SelectDatabase
            Load oForm
            
            oForm.SetFormAndButtonText "Open Reviser Project", "Open"
            
            'oForm.Show vbModal
            
            'oForm.cmdNewOpenDB_Click
            If Not boolRefresh Then
              MabseF_SelectDatabase.OpenProject
            End If
            
            Connection = ADOCnt <> ""
            'ALE chiudere tutte le finestre delle ddl attive 25/0//2006
'            Unload MabseF_List
'            MabseF_List.Show
            
            Clessidra True
            'MAURO - 28-11-05
            'If Connection And m_CreateYesNo Then
            If prjAlreadyOpen And Connection And m_CreateYesNo Then
               'Recupera il path del progetto e le altre info
               GbPathPrj = GbCurDBPath
               GbPathDef = GbPathPrj
               NomePrj = Replace(GbCurDBName, ".mty", "")
               NomeDB = GbCurDBName
               
               'INSERIMENTO PER NUOVO PERCORSO
               GbPathPrj = GbPathPrj & "\" & NomePrj
               
               'Rende disponibile a tutti il file di log e le connessioni
               DLLFunzioni.FnFileLog = GbPathPrj & "\Documents\Log\" & Replace(Replace(NomeDB, ".mty", ""), ".mti", "") & ".log"
               Set DLLFunzioni.FnConnection = ADOCnt
               
               'Legge i Parametri tramite DLL Funzioni
               DLLFunzioni.Load_Parametri_Progetto
               
               'setta le variabili delle DLL contenetnti lo stato della connessione
               Call Set_Connection_To_DLL(Connection, NomeDB)
               
               'inizializza la toolbar
               MabseF_List.Filter_List.Visible = False
               MabseF_List.GridList.Visible = True
   
               If Not boolRefresh And InStr(1, StatusBar1.Panels(1).Text, Trim("[" & NomeDB & "]")) <> 0 Then
                  DLLFunzioni.Show_MsgBoxError "MP06I"
                  Call Terminate_Flickering
                  Clessidra False
                  DLLFunzioni.FnActiveWindowsBool = False
                  Exit Sub
               End If
               
               'Carica la lista dalla tabella BS_Oggetti
               If UCase(Trim(TagFunction)) = "PRJ_OPEN" Or UCase(Trim(TagFunction)) = "PRJ_OPEN_FAST" Then
                  Call Carica_E_controlla_Lista_Progetto(NomeDB, Connection, "FAST")
               ElseIf UCase(Trim(TagFunction)) = "PRJ_OPEN_SLOW" Then
                  Call Carica_E_controlla_Lista_Progetto(NomeDB, Connection, "SLOW")
               End If
               
               'carica le info prj
               Call Carica_Array_InfoPRJ
   
               'Setta Toolbar, Statusbar ecc... Nel mdi
               Call Setting_FormMdi(NomeDB, ToolTipStatus)
   
               If DLLDli2RdbMsExist Then
                 'Carica i parametri
                 Call DLLDli2RdbMs.Carica_Parametri(GbArrayDBD, GbArrayORWord, GbArrayNewWord)
               End If
               
               If DllDataManExist Then
                  Call DllDataM.Carica_Parametri_DataManager
               End If
   
              'aggiorna le variabili delle DLL
              Call Aggiorna_Variabili_DLL
   
              'controlla le autorizzazioni
              Call Controlla_Autorizzazioni
   
              'Controlla Integrit� del database
              'wDbOk = Controlla_INTEGRITA_Database
              
              GbCoolMenu_Enabled = True
              
              MabseF_Prj.StatusBar1.Panels(3).Text = "Object number: " & InfoPRJ(0).NumObj
              
              'ALTER TABLE PROVVISORIA
              Aggiorna_tabelle_Versioni_Precedenti
            End If 'Connection = True
            
            Clessidra False
            DLLFunzioni.FnActiveWindowsBool = False
            
         Case "PRJ_NEW"
            Dim Scelta As Long
            Dim r As ADODB.Recordset
            
            
            DLLFunzioni.FnActiveWindowsBool = True
            
            'Scelta = DLLFunzioni.Show_MsgBoxError("MP00Q") 'ALE
            Scelta = 6 'ALE
            If Scelta = 6 Then
               MabseF_List.GridList.ListItems.Clear
               
               Set oForm = New MabseF_SelectDatabase
               Load oForm
               oForm.SetFormAndButtonText "Create New Database", "New"
               
               oForm.Show vbModal
               
               
               Connection = False
               If ADOCnt <> "" Then Connection = True
               
               'ALE chiudere tutte le finestre delle ddl attive 25/0//2006
            End If
            
            If Connection And m_CreateYesNo Then
               'Recupera il path del progetto e le altre info
               GbPathPrj = GbCurDBPath
               GbPathDef = GbPathPrj
               NomePrj = Replace(GbCurDBName, ".mty", "")
               NomeDB = GbCurDBName
               
               'INSERIMENTO PER NUOVO PERCORSO
               GbPathPrj = GbPathPrj & "\" & NomePrj
               
               'Crea le directory di output dli e documents ecc...
               Create_DirectoryOut_Progetto GbPathPrj
               
               'Rende disponibile a tutti il file di log e le connessioni
               DLLFunzioni.FnFileLog = GbPathPrj & "\Documents\Log\" & Replace(Replace(NomeDB, ".mty", ""), ".mti", "") & ".log"
               Set DLLFunzioni.FnConnection = ADOCnt
               
               'setta le variabili delle DLL contenetnti lo stato della connessione
                Set_Connection_To_DLL Connection, NomeDB
               
               'crea il nuovo database contenente le tabelle Base
               'da sostituire con la copia del prj.mty 'ALE
               'Call Crea_Tabelle_Nuovo_Progetto(NomeDB)
               
                copia_new_prjDB
               
               If ADOCnt.State = adStateClosed Then
                    ADOCnt.Open
               Else
                    ADOCnt.Close
                    ADOCnt.Open
               End If

               
               'carica la treeview
                Carica_TreeView_Progetto MabseF_List.PrjTree
               
               'inserisce la data di creazione del DB Nel Database
               Write_Info_Nuovo_Progetto NomeDB
                              
               'Legge i parametri del progetto
               DLLFunzioni.Load_Parametri_Progetto
               
                'Carica i parametri del DLI2RDBMS se installato
               If DLLDli2RdbMsExist Then
                  Call DLLDli2RdbMs.Carica_Parametri(GbArrayDBD, GbArrayORWord, GbArrayNewWord)
               End If
               
               'Copia i template in input-prj
               template_copy_into_prj
               
              'carica le info prj
              Call Carica_Array_InfoPRJ
               
              'setta la status bar
              ToolTipStatus = "Creation Date: " & InfoPRJ(0).DateCreation
               
              StatusBar1.Panels(1).ToolTipText = ToolTipStatus
               
              StatusBar1.Panels(1).Text = "[" & NomeDB & "]"
               
              Call Aggiorna_Variabili_DLL
              Set DLLFunzioni.FnDatabase = DbPrj
               
              'Abilita tutti i bottoni
              Call Abilita_Disabilita_CoolMenu(True)
  
              GbCoolMenu_Enabled = True
              
             ' Call DllBase.AttivaFunzione("Show_Parameters", New Collection)'ALE    ' Fabio
              DllBase.AttivaFunzione "Show_Parameters", New Collection, MabseF_List
              
              MabseF_Prj.StatusBar1.Panels(3).Text = "Object number: 0"
              MabseF_Prj.StatusBar1.Panels(2).Text = "Creation Date: " & InfoPRJ(0).DateCreation
              
              'Abilta i bottoni ecc
              Call Setting_FormMdi(NomeDB, ToolTipStatus)
            End If 'Connection = True
            
            DLLFunzioni.FnActiveWindowsBool = False
            
         Case "PRJ_ADD_OBJECTS"
            DLLFunzioni.FnActiveWindowsBool = True
            Terminate_Flickering
            
            MabseF_Log.LstLog.ListItems.Clear
            
            m_ClassifyObj_For_All = False
            
            'aggiunge i file nel database
            Annulla = Aggiungi_Oggetti_In_DB(MabseF_List.PBar)
            
            CurPgm = DllBase.Conta_record_Obj_In_Base_Al_Tipo("ALL")
            MabseF_Prj.StatusBar1.Panels(3).Text = "Object Number: " & CurPgm
            
            If Not Annulla Then
              Clessidra False
              DLLFunzioni.FnActiveWindowsBool = False
              Exit Sub
            End If
      
            'aggiorna InfoPRJ
            Call Aggiorna_InfoPRJ
      
            'assgna la stringa alla status bar
            MabseF_Prj.StatusBar1.Panels(2).Text = "Creation Date: " & InfoPRJ(0).DateCreation
      
            'aggiorna la TreeView
            Call Carica_TreeView_Progetto(MabseF_List.PrjTree, , "FAST")
      
            'Cancella la lista
            MabseF_List.GridList.ListItems.Clear
      
            'espande la treeview
            'espande la struttura
            If TreePrj_Index <> 0 Then
              If MabseF_List.PrjTree.Nodes(TreePrj_Index).Key = "MB" Then
                For i = 1 To MabseF_List.PrjTree.Nodes.Count
                  MabseF_List.PrjTree.Nodes(i).Expanded = True
                Next i
              End If
            End If
      
            'salva la struttura in memoria
            Salva_Struttura_TreeView_In_DB MabseF_List.PrjTree
            
            MabseF_Menu.CmdUty_Click MabseF_Menu.ButtonSelIndex
            
            DLLFunzioni.FnActiveWindowsBool = False
            
       Case "LST_FILTER_CONF"
         'Seleziona Filtro
         Call Seleziona_Filtro_Selezione(MabseF_List.GridList, MabseF_List.Filter_List, Toolbar1.Buttons(21).Value, 21)
         
       Case "LST_FILTER_EXECUTE"
         'esegue la Query di filtro
         Call Execute_Filter(MabseF_List.Filter_List, MabseF_List.GridList, MabseF_List.TotalObj)
         MabseF_List.txtFilter.Visible = False
   
       Case "LST_ORDERDOWN"
         'ordiana in base alla colonna selezionata
         Call Ordina_Lista(MabseF_List.GridList, lvwDescending)
   
       Case "LST_ORDERUP"
         'ordiana in base alla colonna selezionata
         Call Ordina_Lista(MabseF_List.GridList, lvwAscending)

       Case "OBJ_DELETE"
         DLLFunzioni.FnActiveWindowsBool = True
         
         Call Elimina_Flickering(Me.hWnd)
   
         'Elimina gli oggetti selezionati
         Cancella = Elimina_Oggetti_Selezionati(MabseF_List.GridList, MabseF_List.TotalObj)
   
         'Scrive nel Pannello Lo status degli oggetti
         CurPgm = DllBase.Conta_record_Obj_In_Base_Al_Tipo("ALL")
         MabseF_Prj.StatusBar1.Panels(3).Text = "Object numbers: " & CurPgm
   
         If Cancella = vbYes Then
           'se � un DBD chiamo il DataManager
           For i = 1 To MabseF_List.GridList.ListItems.Count
             If MabseF_List.GridList.ListItems(i).Selected = True And Trim(UCase(MabseF_List.GridList.ListItems(i).ListSubItems(2))) = "DBD" Then
               DllDataM.DmIdOggetto = Val(MabseF_List.GridList.ListItems(i).Text)
               Call DllDataM.AttivaFunzione("DEL_TO_PROJECT")
             End If
           Next i
         End If
   
         'aggiorna InfoPRJ
         Call Aggiorna_InfoPRJ
   
         'assgna la stringa alla status bar
         MabseF_Prj.StatusBar1.Panels(2).Text = "Creation Date: " & InfoPRJ(0).DateCreation
   
         Terminate_Flickering
         
         DLLFunzioni.FnActiveWindowsBool = False
         
       Case "OBJ_RESTORE"
         Dim wRet As Long
         DLLFunzioni.FnActiveWindowsBool = True
         
         If Trim(OggSel.Nome) = "" Or Trim(OggSel.Directory_Input) = "" Then
            DLLFunzioni.Show_MsgBoxError "MP02I"
   
         Else
            wRet = DLLFunzioni.Show_MsgBoxError("MP01Q")
            
            If wRet = vbYes Then
               Restore_Objects
            End If
         End If
         
         DLLFunzioni.FnActiveWindowsBool = False
         
       Case "OBJ_CHANGE_TYPE"
         DLLFunzioni.FnActiveWindowsBool = True
         
         If GbIdOggetto <= 0 Then
            DLLFunzioni.Show_MsgBoxError "MP02I"
            Screen.MousePointer = vbDefault
         Else
            MabseF_ClassObj.Show vbModal
            Clessidra True
            'Cambia il tipo agli oggetti selezionati
            If Trim(MabseF_ClassObj.Tipo) <> "" Then
              Cambia_Type_Oggetti_Selezionati MabseF_List.PrjTree, MabseF_List.GridList, MabseF_ClassObj.Tipo, MabseF_ClassObj.Level
              'Riaggiorna La treeview e la lista e salva la sua struttura
              Salva_Struttura_TreeView_In_DB MabseF_List.PrjTree, False
              'Clear della lista
              MabseF_List.GridList.ListItems.Clear
            End If
            Clessidra False
         End If
         
         DLLFunzioni.FnActiveWindowsBool = False
         
       Case "OBJ_PROPERTIES"
            Elimina_Flickering Me.hWnd
      
            'visualizza le propriet� dell'oggetto Selezionato
            If Trim(OggSel.Nome) = "" Or Trim(OggSel.Directory_Input) = "" Then
               DLLFunzioni.Show_MsgBoxError "MP02I"
              'Exit Sub
            Else
              Unload MabseF_Log
              OldFinCheck = GbFinImCheck
              GbFinImCheck = False
              MabseF_Prj.ResizeAll
      
              DLLFunzioni.Show_ObjectProperties OggSel, MabseF_List
              
              GbFinImCheck = OldFinCheck
              'MabseF_Prj.ResizeAll
            End If
       
       Case "COM_OPENCLOSE_CONNECTION"
            Screen.MousePointer = vbHourglass
            
            Select Case wSokComp.State
               Case sckClosed
                  wSokComp.RemoteHost = DLLFunzioni.Fn_IpServer_Comp
                  wSokComp.RemotePort = DLLFunzioni.Fn_PortServer_Comp
                  wSokComp.Connect
            
               Case sckClosing
                  MabseF_Log.LstLog.ListItems.Add , , Now & " --> Connection Closing...WAIT PLEASE!!!"
                  
                  Do While wSokComp.State = sckClosing
                     DoEvents
                     wSokComp.Close
                     If wSokComp.State = sckClosed Then
                        GbConnMFE = False
                        DLLFunzioni.FnCompilerConnection = GbConnMFE
                        Toolbar1.Buttons(4).Value = tbrUnpressed
                        
                        MabseF_Log.LstLog.ListItems.Add , , Now & " --> Connection Closed."
                        Exit Do
                     End If
                  Loop
                  
               Case sckConnected
                  'Chiude
                  wSokComp.Close
                  GbConnMFE = False
                  DLLFunzioni.FnCompilerConnection = GbConnMFE
                  Toolbar1.Buttons(4).Value = tbrUnpressed
                  MabseF_Log.LstLog.ListItems.Add , , Now & " --> Connection Closed."
               Case sckConnecting
               
               Case sckConnectionRefused
                  'Chiude
                  wSokComp.Close
                  GbConnMFE = False
                  DLLFunzioni.FnCompilerConnection = GbConnMFE
                  Toolbar1.Buttons(4).Value = tbrUnpressed
                  
               Case sckError
                  'Chiude
                  wSokComp.Close
                  GbConnMFE = False
                  DLLFunzioni.FnCompilerConnection = GbConnMFE
                  Toolbar1.Buttons(4).Value = tbrUnpressed
                  
            End Select
            
            Screen.MousePointer = vbDefault
            
       Case "COM_COMPILE_OBJS"
         If GbConnMFE = False Then
           DLLFunzioni.Show_MsgBoxError "MC02I"
           bCompile = False
         Else
           bCompile = True
         End If
         
         If DLLFunzioni.Fn_Win2Unix_ProjectPath = "" Then
            DLLFunzioni.Show_MsgBoxError "MC01I"
            bCompile = False
         Else
            bCompile = True
         End If
         
         If bCompile = True Then
            Start_Compilazione_Objects
         End If
       
       Case "OBJ_EDIT", "OBJ_EDIT_TEXT"
         If GbIdOggetto = 0 Then
            DLLFunzioni.Show_MsgBoxError "MP02I"
         Else
              Unload MabseF_Log
              OldFinCheck = GbFinImCheck
              GbFinImCheck = False
              'MabseF_Prj.ResizeAll 'ale
      'aggiungere parametro formparent
              DLLFunzioni.Show_TextEditor GbIdOggetto, , , MabseF_List
              
              GbFinImCheck = OldFinCheck
              MabseF_Prj.ResizeAll
         End If
      
       Case "OBJ_EDIT_MAP"
         If GbIdOggetto = 0 Then
            DLLFunzioni.Show_MsgBoxError "MP02I"
         Else
              If MabseM_Prj.CheckMapToEdit(GbIdOggetto) Then
                Unload MabseF_Log
                OldFinCheck = GbFinImCheck
                GbFinImCheck = False
                MabseF_Prj.ResizeAll
        
                DLLFunzioni.Show_MapEditor GbIdOggetto
                
                GbFinImCheck = OldFinCheck
                MabseF_Prj.ResizeAll
              Else
                MsgBox "Please, parse the map before using the Map Editor", vbInformation, "Map not parsed"
              End If
         End If
       
       Case "OBJ_EDIT_EXTEDT"
         'Avvia un editor esterno
         If GbIdOggetto = 0 Then
            DLLFunzioni.Show_MsgBoxError "MP02I"
         Else
            DLLFunzioni.Show_TextEditor GbIdOggetto, True, , MabseF_List
         End If
               
       Case "PRD_ABOUT"
         MabseF_About.Show vbModal
         
       Case "PRD_WEBSITE"
         wRetWeb = ShellExecute(Me.hWnd, "Open", GbReviserWebSite, "", app.path, 1)
      
         If wRetWeb < 32 Then
            ERR.Raise 2001
         End If
         
       Case "PRD_HELP"
         'Controlla se c'� l'help
         'DA FARE
         
       Case "PRJ_EXIT"
          fixCtrl
          End
       
       Case Else
         fixCtrl
         
   End Select
  End If
  
  DLLFunzioni.FnActiveWindowsBool = False
  
  
     Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "RC0002"
End Sub

Public Sub copia_new_prjDB()
    Dim apppo As String
    Dim fso As New FileSystemObject
    fso.CopyFile GbPathPrd & "\system\prj.mty", GbPathPrj & ".mty", True
    Do Until FileExists(GbPathPrj & ".mty")
      DoEvents
    Loop
    'Mauro: 14/03/2006 - Rendo modificabile il db di progetto
    SetFileAttributes GbPathPrj & ".mty", 128
End Sub

Private Sub Aggiorna_tabelle_Versioni_Precedenti()
  On Error GoTo errh
  
   'DIFFERENZIARE PER I VARI TIPI DI DB (SQL SERVER,ORACLE ECC...)
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE Bs_Oggetti ADD [Notes] MEMO "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE Bs_Oggetti ADD [RDBMS_DBName] TEXT(32) "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PSDLI_Segmenti ADD [RDBMS_TBName] TEXT(32) "
''''''   '29-11-05
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PSDLI_Segmenti ADD [Condizione] TEXT(50) "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE TS_Macro ADD [PerLinea] BIT "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_Istruzioni ADD [ImsDB] BIT "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_Istruzioni ADD [ImsDC] BIT "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_Istruzioni ADD [MapArea] TEXT(32)  "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_Istruzioni ADD [MapName] TEXT(32)  "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_Istruzioni ADD [MapSet] TEXT(32)  "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_Istruzioni ADD [OrdPCB] LONG  "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_Istruzioni ADD [KeyFeedBack] TEXT(255)  "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_IstrDett_Liv ADD [DataArea] TEXT(50)  "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_IstrDett_Liv ADD [VariabileSegmento] TEXT(50)  "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_IstrDett_Key ADD [KeyValore] TEXT(255)  "
''''''   'SQ 28-11-05
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_IstrDett_Key ADD [KeyStart] LONG  "
''''''
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_IstrDett_Key ALTER COLUMN [Operatore] TEXT(30) "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDli_Istruzioni ALTER COLUMN [Istruzione] TEXT(200) "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE MgDLI_DecodificaIstr ADD [ImsDC] BIT "
''''''   DLLFunzioni.FnConnection.Execute "ALTER TABLE MgDLI_DecodificaIstr ADD [ImsDB] BIT "
   ''''''DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDLI_XDField ADD [SubSeq] TEXT(20)"
   ''''''DLLFunzioni.FnConnection.Execute "ALTER TABLE PsDLI_XDField ADD [Lchild] TEXT(20)"
   'SQ - 1-12-05: questo cosa c'entra?
   'DllParser.CreaTableParser
  Exit Sub

errh:
   If ERR.Number = -2147217887 Then
      'Compo gi� esistente, no problema continua
      Resume Next
   End If
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' Aggiorna Campi in lista degli ogggetti selezionati
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub mnuUpdateObj_Click()
  Dim Ind As Long
  Dim IdObj As Long
  For Ind = 1 To MabseF_List.GridList.ListItems.Count
    If MabseF_List.GridList.ListItems(Ind).Selected Then
       IdObj = MabseF_List.GridList.ListItems(Ind).Text
       DllParser.UpdateErrLevel IdObj, Ind
    End If
  Next Ind
End Sub

Public Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  On Error GoTo ERR
  
  If DLLFunzioni.FnProcessRunning Then
      DLLFunzioni.Show_MsgBoxError "FB01I"
      Exit Sub
  End If
  
  'scrive ctrl
  fixCtrl
  
  'Lancia le funzioni
  Attiva_Funzione Button.Tag, New Collection, Button.Key

Exit Sub

ERR:
  Screen.MousePointer = vbDefault
   
  Select Case ERR.Number
      Case 75
         DLLFunzioni.Show_MsgBoxError "MP03I", , , ERR.Number, ERR.Description
      
      Case Else
         DLLFunzioni.Show_MsgBoxError "MB99E", "MabseF_Prj", "Toolbar1.ButtonClick", ERR.Number, ERR.Description
  
  End Select
  
  Terminate_Flickering
  
  DLLFunzioni.FnActiveWindowsBool = False
  DLLFunzioni.FnProcessRunning = False
End Sub

Private Sub Setting_FormMdi(NomeDB As String, ToolTipStatus As String)
   'Setta gli oggetti sul form mdi (status-bar,Toolbar ecc...)

   'setta la status bar
    ToolTipStatus = "Path Project..."

    StatusBar1.Panels(1).ToolTipText = ToolTipStatus

    StatusBar1.Panels(1).Text = "[" & NomeDB & "]"
    StatusBar1.Panels(2).Text = "Creation Date: " & InfoPRJ(0).DateCreation

    'Abilita tutti i bottoni
    Call Abilita_Disabilita_CoolMenu(True)

    'Abilita la toolbar
    Call ON_OFF_Button_ToolBar(Toolbar1, True)
    
    'Menu
    mnuPrints.Enabled = True
    mnuAddObjects.Enabled = True
    mnuPropertiesPrj.Enabled = True
    mnuParamPrj.Enabled = True

    'Compiler
    'mnuCompilers.Enabled = True 'ALE 23/05/2006

    'Objects
    mnuObject.Enabled = True

    'Help
    mnuHelp.Enabled = True
End Sub

Private Sub Controlla_Autorizzazioni()
   'controlla le autorizzazioni
   'Ripristina i valori dei contatori
'   FreeSlotDBD = FreeSlotDBDReset
'   FreeSlotPGM = FreeSlotPGMReset
'
'   If DllBase.ControlSource_Total("CBL", "DBD") = False Then
'     MsgBox "You don't have the authorization to open this project...", vbExclamation, GbNomeProdotto
'     End
'   End If
'
'   MaxPGM = DllBase.UsMaxPGM
'   MaxDBD = DllBase.UsMaxDBD
'   Percent = DllBase.UsPercent
'
   'Controlla se la tabella parametri � cambiata
   'Modifica necessaria solo fino a che tutti i progetti saranno allineati
'   Dim wDbOk As Boolean
   
'   If GbFinImCheck = True Then
'      MabseF_Log.LstLog.ListItems.Add , , Now & " : Control Database Structure..."
'   End If
End Sub

Public Sub ResizeAll(Optional ByVal isFirstLoad As Boolean = False)
    Dim wApp01 As Long
    Dim wApp02 As Long
    
    'On Error Resume Next
    
    'Posiziona La finestra
    If isFirstLoad Then
      Me.Height = Screen.Height '(Screen.Height / 10) * 9
      Me.Width = Screen.Width '(Screen.Width / 10) * 9
      Me.Top = ((Screen.Height - Me.Height) / 2)
      Me.Left = ((Screen.Width - Me.Width) / 2)
    End If
    
    'Setta le variabili globali dello spazio per le finestre interne
    GbTop = 0
    GbLeft = 0
    GbWidth = Me.Width
    GbHeight = Me.Height

    'inizializza le variabili per centrare le form delle DLL
    GbTopForm = MabseF_Menu.Top
    GbLeftForm = MabseF_Menu.Left + MabseF_Menu.Width
    GbWidthForm = Me.ScaleWidth
    GbHeightForm = Me.ScaleHeight

    GbHeightNoFinImm = GbHeightForm

    MabseF_Menu.Resize

    'Rivalorizza lo spazio a disposizione
    GbWidthForm = Me.ScaleWidth - MabseF_Menu.Width
    GbHeightForm = Me.ScaleHeight

    If GbFinImCheck = True Then
       MabseF_Log.Resize

       wApp01 = GbHeightForm
       wApp02 = wApp01 - MabseF_Log.Height
       GbHeightForm = wApp02
    Else
       Unload MabseF_Log

       GbHeightForm = Me.ScaleHeight
    End If

    MabseF_List.Top = GbTopForm
    MabseF_List.Left = GbLeftForm

    MabseF_List.Resize

    MabseF_List.Height = GbHeightForm
    MabseF_List.Width = GbWidthForm
    
    If MabseF_CompErrors.loaded Then
            MabseF_CompErrors.Move 0, 0, MabseF_List.Width, MabseF_List.Height
    End If

    'se le varie Dll sono attive gli passo i parametri di posizionamento form

    If GbDllBaseExist Then
      DllBase.BsHeight = GbHeightForm
      DllBase.BsLeft = GbLeftForm + 25
      DllBase.BsTop = GbTopForm + Toolbar2.Height + 25
      DllBase.BsWidth = GbWidthForm
    End If

    If GbDllParserExist Then
      DllParser.PsHeight = GbHeightForm
      DllParser.PsLeft = GbLeftForm + 25
      DllParser.PsTop = GbTopForm + Toolbar2.Height + 5
      DllParser.PsWidth = GbWidthForm
    End If

    If GbDllDataManExist Then
      DllDataM.DmHeight = GbHeightForm
      DllDataM.DmLeft = GbLeftForm + 25
      DllDataM.DmTop = GbTopForm + Toolbar2.Height + 25
      DllDataM.DmWidth = GbWidthForm
    End If

    If GbDLLDli2RdbMsExist Then
      DLLDli2RdbMs.drHeight = GbHeightForm
      DLLDli2RdbMs.drLeft = GbLeftForm + 25
      DLLDli2RdbMs.drTop = GbTopForm + Toolbar2.Height + 25
      DLLDli2RdbMs.drWidth = GbWidthForm
    End If

    If GbDllImsExist Then
      DLLIms.ImHeight = GbHeightForm
      DLLIms.ImLeft = GbLeftForm + 25
      DLLIms.ImTop = GbTopForm + Toolbar2.Height + 25
      DLLIms.ImWidth = GbWidthForm
    End If

    If GbDllToolsExist Then
      DLLTools.TsHeight = GbHeightForm
      DLLTools.TsLeft = GbLeftForm + 25
      DLLTools.TsTop = GbTopForm + Toolbar2.Height + 25
      DLLTools.TsWidth = GbWidthForm
    End If

    If GbDllAnalisiExist Then
      DLLDataAnalysis.AnHeight = GbHeightForm
      DLLDataAnalysis.AnLeft = GbLeftForm + 25
      DLLDataAnalysis.AnTop = GbTopForm + Toolbar2.Height + 25
      DLLDataAnalysis.AnWidth = GbWidthForm
    End If

    If GbDllFunzioniExist Then
      DLLFunzioni.FnHeight = GbHeightForm
      DLLFunzioni.FnLeft = GbLeftForm + 25
      DLLFunzioni.FnTop = GbTopForm + Toolbar2.Height + 25
      DLLFunzioni.FnWidth = GbWidthForm
      DLLFunzioni.FnHeightNoFinImm = GbHeightNoFinImm
    End If
'da qua in poi ALE
'    If GbDllFunzioniExist Then
'      DLLFunzioni.FnHeight = MabseF_List.Height
'      DLLFunzioni.FnLeft = MabseF_List.Left
'      DLLFunzioni.FnTop = MabseF_List.Top + 440
'      DLLFunzioni.FnWidth = MabseF_List.Width
'      DLLFunzioni.FnHeightNoFinImm = GbHeightNoFinImm
'    End If
    

End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Elimina_Flickering Me.hWnd
  
  On Error GoTo ERR
   
  Attiva_Funzione ButtonMenu.Tag, New Collection, ButtonMenu.Key
      
  Terminate_Flickering
Exit Sub

ERR:
  Select Case ERR.Number
      Case 75
         DLLFunzioni.Show_MsgBoxError "MP03I"
      
      Case 2001
         DLLFunzioni.Show_MsgBoxError "MB00E"
         
      Case Else
         DLLFunzioni.Show_MsgBoxError "MB99E", "MabseF_Prj", "Toolbar1.ButtonMenuClick", ERR.Number, ERR.Description
         
  End Select
  
  Terminate_Flickering
End Sub

Private Sub Carica_E_controlla_Lista_Progetto(NomeDB As String, Connection As Boolean, mTypeLoad As String)
    'setta le variabili delle DLL contenetnti lo stato della connessione
    Call Set_Connection_To_DLL(Connection, NomeDB)
    
    If Connection Then
       'controlla l'integrit� del DB

       'cancella la listview
       MabseF_List.GridList.ListItems.Clear
       
       'carica la treeview
       Call Carica_TreeView_Progetto(MabseF_List.PrjTree, , mTypeLoad)
       
    Else
      If Trim(NomeDB) <> "" Then
         Call MsgBox("I'm sorry!" & vbCrLf & "Database Name or Database Type is incorrect...", vbExclamation, GbNomeProdotto)
       End If
       Exit Sub
    End If
End Sub

Public Sub NoMaxBox(f As MDIForm)
      
    Dim l As Long
    l = GetWindowLong(f.hWnd, GWL_STYLE)
    l = l And Not (WS_MAXIMIZEBOX)
    l = SetWindowLong(f.hWnd, GWL_STYLE, l)
    
End Sub

Public Sub NoMinBox(f As MDIForm)
      
    Dim l As Long
    l = GetWindowLong(f.hWnd, GWL_STYLE)
    l = l And Not (WS_MINIMIZEBOX)
    l = SetWindowLong(f.hWnd, GWL_STYLE, l)
    
End Sub

Private Sub MDIFormOnTop(f As MDIForm)
    
'    HWND_TOPMOST = -1
'    HWND_NOTOPMOST = -2
    
'    SWP_NOMOVE = &H2
'    SWP_NOSIZE = &H1
'    SWP_NOOWNERZORDER = &H200
'    SWP_NOREPOSITION = SWP_NOOWNERZORDER
'    SWP_NOZORDER = &H4
'    SWP_NOREDRAW = &H8
    
    Dim l As Long
'    l = SetWindowPos(f.hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE + SWP_NOSIZE + SWP_NOOWNERZORDER + SWP_NOREPOSITION + SWP_NOZORDER)
    
    l = SetWindowPos(f.hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE + SWP_NOSIZE + SWP_NOREPOSITION)

End Sub

Public Sub DrawForm()

    On Error GoTo EH

    ResizeAll
    
    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear
    
End Sub

Private Sub DestroyMDIFormMenu()

    Dim lMenu As Long
    lMenu = GetSystemMenu(Me.hWnd, False)
    lMenu = DestroyMenu(lMenu)

End Sub

Public Sub Disabilita_Abilita_ToolBar_Editor(TipoObj As String)
  Dim i As Long
  
  For i = 1 To Toolbar1.Buttons(2).ButtonMenus.Count
    If Trim(UCase(Toolbar1.Buttons(2).ButtonMenus(i).Key)) <> "SHOW_TEXT_EDITOR" Then
      Toolbar1.Buttons(2).ButtonMenus(i).Enabled = False
    Else
      Toolbar1.Buttons(2).ButtonMenus(i).Enabled = True
    End If
  Next i
  
  Select Case Trim(UCase(TipoObj))
    Case "BMS", "MFS"
      For i = 1 To Toolbar1.Buttons(2).ButtonMenus.Count
        If Trim(UCase(Toolbar1.Buttons(2).ButtonMenus(i).Key)) = "SHOW_MAP_EDITOR" Then
          Toolbar1.Buttons(2).ButtonMenus(i).Enabled = True
        End If
      Next i
      
    Case "DBD"
      For i = 1 To Toolbar1.Buttons(2).ButtonMenus.Count
        If Trim(UCase(Toolbar1.Buttons(2).ButtonMenus(i).Key)) = "SHOW_STRUCTURE" Then
          Toolbar1.Buttons(2).ButtonMenus(i).Enabled = True
        End If
      Next i
      
    Case Else
      For i = 1 To Toolbar1.Buttons(2).ButtonMenus.Count
        If Trim(UCase(Toolbar1.Buttons(2).ButtonMenus(i).Key)) <> "SHOW_TEXT_EDITOR" Then
          Toolbar1.Buttons(2).ButtonMenus(i).Enabled = False
        Else
          Toolbar1.Buttons(2).ButtonMenus(i).Enabled = True
        End If
        
      Next i
  End Select
End Sub

Public Sub Restore_Objects()
  Dim nCol As New Collection
  Dim wIdOgg As Long, i As Long, k As Integer
  Dim rs As ADODB.Recordset

  For i = 1 To MabseF_List.GridList.ListItems.Count
    If MabseF_List.GridList.ListItems(i).Selected Then
      wIdOgg = MabseF_List.GridList.ListItems(i).Text
            
      For k = 1 To nCol.Count
        nCol.Remove 1
      Next
      
      nCol.Add wIdOgg
      nCol.Add MabseF_List.GridList.ListItems(i).ListSubItems(2)
       
      'Lancia la funzione del parser
      Set DllParser.PsCollectionParam = nCol
      DLLFunzioni.FnActiveWindowsBool = False
      Attiva_Funzione "PARSER", nCol, "RESTORE_OBJECT", , True, True
      ' MAURO: 03/01/2006
      MabseF_List.GridList.ListItems(i).SmallIcon = 6
      MabseF_List.GridList.ListItems(i).ListSubItems(3) = ""
      MabseF_List.GridList.ListItems(i).ListSubItems(6) = ""
      DLLFunzioni.FnActiveWindowsBool = True
     
      'Mauro 30-03-06
      Set rs = DLLFunzioni.Open_Recordset("Select numrighe From BS_Oggetti Where IdOggetto = " & wIdOgg)
      If rs.RecordCount > 0 Then
        MabseF_List.GridList.ListItems(i).ListSubItems(4) = rs!NumRighe
      End If
      rs.Close
    End If
  Next i
   
End Sub

Private Sub wSokComp_Close()
   
   GbConnMFE = False
   DLLFunzioni.FnCompilerConnection = GbConnMFE
   Toolbar1.Buttons(4).Value = tbrUnpressed
   
   MabseF_Log.LstLog.ListItems.Add , , Now & " --> Connection Closed."
End Sub

Private Sub wSokComp_Connect()
   
   GbConnMFE = True
   DLLFunzioni.FnCompilerConnection = GbConnMFE
   
   MabseF_Log.LstLog.ListItems.Add , , Now & " --> Connection Successfull."
End Sub

Private Sub wSokComp_DataArrival(ByVal bytesTotal As Long)
   Dim sBuf As String
   
   'Dim mParse As New clsParserCob32
   
   wSokComp.GetData sBuf

   'GbSokBufferReader = GbSokBufferReader & vbCrLf & sBuf
   GbSokBufferReader = sBuf
   
  If InStr(1, GbSokBufferReader, "GENERIC_ERROR") > 0 Then
        Dim pos1 As Integer
        pos1 = InStr(1, GbSokBufferReader, "GENERIC_ERROR")
        MabseF_Log.LstLog.ListItems.Add , , Now & " --> " & Mid(GbSokBufferReader, pos1, Len(GbSokBufferReader) - pos1)
        Exit Sub
   End If

   If InStr(1, GbSokBufferReader, "LAUNCH_COMPILE_SCRIPT") > 0 Then
      MabseF_Log.LstLog.ListItems.Add , , Now & " --> Compilation in Progress..."
   End If
   
   If InStr(1, GbSokBufferReader, "FINISH_COMPILE") > 0 Then
      MabseF_Log.LstLog.ListItems.Add , , Now & " --> Compile Complete..."
   End If
   
   If InStr(1, GbSokBufferReader, "COMPILATION ENDED WITHOUT ERRORS") > 0 Then
      Dim compMess1 As String
        compMess1 = Mid(GbSokBufferReader, InStr(1, GbSokBufferReader, "PROGRAM") + 8, InStr(1, GbSokBufferReader, "COMPILATION") - InStr(1, GbSokBufferReader, "PROGRAM") - 9)
      Dim progpath1 As String
      progpath1 = DLLFunzioni.FnPathPrj & "\Output-Prj\Compiled\" & compMess1
      If FileExists(progpath1) Then
         Dim fso1 As New FileSystemObject
         fso1.DeleteFile (progpath1)
      End If
      MabseF_Log.LstLog.ListItems.Add , , Now & " --> " & compMess1 & ": COMPILATION ENDED WITHOUT ERRORS"
      Exit Sub
   End If
'
'   If InStr(1, GbSokBufferReader, "PARSE_ERROR_LISTING_STARTED") > 0 Then
'      MabseF_Log.LstLog.ListItems.Add , , Now & " --> Start Parse Return-Code..."
'   End If
'
'   If InStr(1, GbSokBufferReader, "PARSE_ERROR_LISTING_FINISHED") > 0 Then
'      MabseF_Log.LstLog.ListItems.Add , , Now & " --> Parse Return-Code Complete..."
'   End If
   
   If InStr(1, GbSokBufferReader, "RETURN_CODELIST") > 0 Then
      MabseF_Log.LstLog.ListItems.Add , , Now & " --> Return-Code Data Receive Start..."
   End If
   

   
   If InStr(1, GbSokBufferReader, "COMPILING_ERRORS") > 0 Then 'END-RETURN_CODELIST
   
        Dim compMess As String
        compMess = Mid(GbSokBufferReader, InStr(1, GbSokBufferReader, "PROGRAM") + 8, InStr(1, GbSokBufferReader, "COMPILING_ERRORS") - InStr(1, GbSokBufferReader, "PROGRAM") - 10)
      
      MabseF_Log.LstLog.ListItems.Add , , Now & " --> " & compMess & " Compiling errors"
      
      'In mBuffer (prima di Blakarlo ho tutto il RetCode)
      
      'controlla il formalismo dei dati
      
      'Parse dei blocchi e strutturazione
      
      'Processa i blocchi e intaragisce con il repository (Messaggi, segnalazioni ecc...)
      'Conta gli errori, visualizza in lista gli errori
      
      If Not GbSokBufferReader = "" Then
      GbSokBufferReader = Replace(GbSokBufferReader, vbLf, Chr(13) & Chr(10))
      Dim progpath As String
      progpath = DLLFunzioni.FnPathPrj & "\Output-Prj\Compiled\" & compMess
      If FileExists(progpath) Then
        Dim fso As New FileSystemObject
        fso.DeleteFile (progpath)
      End If
        Open progpath For Binary As #1
        Dim c As Long
        c = InStr(1, GbSokBufferReader, "COMPILING_ERRORS: #")
        GbSokBufferReader = Mid(GbSokBufferReader, 1, c + 20) & Chr(13) & Chr(10) & Mid(GbSokBufferReader, c + 20, Len(GbSokBufferReader))
'        c = c + 80
'        Do Until c > Len(GbSokBufferReader)
'            GbSokBufferReader = Mid(GbSokBufferReader, 1, c) & vbCrLf & Mid(GbSokBufferReader, c, Len(GbSokBufferReader))
'            c = c + 80
'        Loop
        Put #1, , GbSokBufferReader
        Close #1
      End If
      GbSokBufferReader = ""
   End If
   
End Sub

'Public Function FileExists(path$) As Boolean
''verifica l'esistenza di un file
'Dim x As Integer
'On Error Resume Next
'1000    x = FreeFile
'1010    Open path$ For Input As x
'   'con Append � come se aprissi il file quindi gli altri processi devono aspettare.
'1020    If ERR = 0 Then
'1030       FileExists = True
'1040    Else
'1050       FileExists = False
'1060    End If
'1070    Close x
'1080    ERR = 0
'End Function


Private Sub wSokComp_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
   
   'Chiude
   wSokComp.Close
   
   GbConnMFE = False
   DLLFunzioni.FnCompilerConnection = GbConnMFE
   Toolbar1.Buttons(4).Value = tbrUnpressed
   
   Select Case Number
      Case 10061
         DLLFunzioni.Show_MsgBoxError "MC00I"
      
      Case Else
         DLLFunzioni.Show_MsgBoxError "RNND", "MabseF_Prj", "wSokComp_Error", Number, vbCrLf & "sCode : " & Scode & vbCrLf & Description
         
   End Select
   
   MabseF_Log.LstLog.ListItems.Add , , Now & " --> Generic connection Error..."
   
End Sub

Private Sub wSokComp_SendComplete()
   DoEvents
End Sub

Private Sub wSokComp_SendProgress(ByVal bytesSent As Long, ByVal bytesRemaining As Long)
   DoEvents
End Sub

Public Sub Start_Compilazione_Objects()
   Dim i As Long
   Dim j As Long
   Dim m_WhoIs As t_WhoIs
   Dim m_ClientMsg As t_ClientMsg
   Dim m_ClientReq As t_ClientReq
   Dim wParseCob32 As New MaFndC_Compilers
   Dim wStr As String
   Dim wCopyPath As Collection
   Dim wCopyLib As String
   
   For i = 1 To MabseF_List.GridList.ListItems.Count
      If MabseF_List.GridList.ListItems(i).Selected Then
         'Genera la stringa per la compilazione
         ReDim m_WhoIs.Values(4)
   
         m_WhoIs.Values(0).cTag = "USER"
         m_WhoIs.Values(0).cValue = GetUser
         m_WhoIs.Values(1).cTag = "PCNAME"
         m_WhoIs.Values(1).cValue = GetComputerNameU
         m_WhoIs.Values(2).cTag = "RAGSOCLIC"
         m_WhoIs.Values(2).cValue = GbNomeCliente
         m_WhoIs.Values(3).cTag = "NUMLIC"
         m_WhoIs.Values(3).cValue = DLLLic.Lic_NumID
         m_WhoIs.Values(4).cTag = "NUMPRJ"
         m_WhoIs.Values(4).cValue = DLLLic.Lic_NumProdotto
      
         'CLIENT MESSAGE
         ReDim m_ClientMsg.Functions.Parameters(0)
         
         m_ClientMsg.Functions.Name = "EXECUTE_COMMAND"
         m_ClientMsg.Functions.Parameters(0).cTag = "COMMAND"
         
         If MabseF_List.GridList.ListItems(i).SubItems(2) = "CBL" Then
            Dim rs As ADODB.Recordset
            Set rs = DLLFunzioni.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & CLng(MabseF_List.GridList.ListItems(i).Text))
               If rs!Cics Then
                    m_ClientMsg.Functions.Parameters(0).cValue = "ONLINE_COMPILE"
               Else
                    m_ClientMsg.Functions.Parameters(0).cValue = "BATCH_COMPILE"
               End If
         ElseIf MabseF_List.GridList.ListItems(i).SubItems(2) = "BMS" Then
            m_ClientMsg.Functions.Parameters(0).cValue = "MAP_COMPILE"
         End If
         
         
        ' m_ClientMsg.Functions.Parameters(0).cValue = "ONLINE_COMPILE"
         
         '***********valori che non servono, verranno presi dal file ini del server*****************
         ReDim Preserve m_ClientMsg.Functions.Parameters(1)
         m_ClientMsg.Functions.Parameters(1).cTag = "SCRIPT_PATH"
         m_ClientMsg.Functions.Parameters(1).cValue = "WWWWWWWWWWWWWWWWW" '"$SERVER_CONF"
         ReDim Preserve m_ClientMsg.Functions.Parameters(2)
         m_ClientMsg.Functions.Parameters(2).cTag = "SCRIPT_NAME"
         m_ClientMsg.Functions.Parameters(2).cValue = "EEEEEEEEEEEEEEEEEE" '"$SERVER_CONF"
         '**********************************************************************
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(3)
         m_ClientMsg.Functions.Parameters(3).cTag = "PGM_NAME"
         m_ClientMsg.Functions.Parameters(3).cValue = MabseF_List.GridList.ListItems(i).ListSubItems(1).Text & "." & rs!Estensione
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(4)
         m_ClientMsg.Functions.Parameters(4).cTag = "PGM_EXT"
         m_ClientMsg.Functions.Parameters(4).cValue = "clt" 'MabseF_List.GridList.ListItems(i).ListSubItems(2).Text
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(5)
         m_ClientMsg.Functions.Parameters(5).cTag = "PGM_PATH" 'PGM_PATH
         m_ClientMsg.Functions.Parameters(5).cValue = DLLFunzioni.Transcodifica_PathPrj_Win2Unix(MabseF_List.GridList.ListItems(i).ListSubItems(7).Text)
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(6)
         m_ClientMsg.Functions.Parameters(6).cTag = "PATH_COPY"
         
         'Controllare se il path copy esiste!!!!
         Set wCopyPath = DLLFunzioni.getCopyPathByIdOggetto(MabseF_List.GridList.ListItems(i).Text)
         
         If wCopyPath.Count = 0 Then
            'Non ci sono copy associate continuare
            wCopyLib = "NO_COPYLIB"
            
         ElseIf wCopyPath.Count = 1 Then
            wCopyLib = DLLFunzioni.Transcodifica_PathPrj_Win2Unix(wCopyPath.Item(1))
         
         ElseIf wCopyPath.Count > 1 Then
            For j = 1 To wCopyPath.Count
               wCopyLib = wCopyLib & ":" & DLLFunzioni.Transcodifica_PathPrj_Win2Unix(wCopyPath.Item(j))
            Next j
         End If
         
         m_ClientMsg.Functions.Parameters(6).cValue = wCopyLib 'percorso delle copy
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(7)
         m_ClientMsg.Functions.Parameters(7).cTag = "DEST_COMP"
         m_ClientMsg.Functions.Parameters(7).cValue = DLLFunzioni.Transcodifica_PathPrj_Win2Unix(DLLFunzioni.FnPathPrj & "\Output-Prj\Compiled")
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(8)
         m_ClientMsg.Functions.Parameters(8).cTag = "PATH_COMP"
         m_ClientMsg.Functions.Parameters(8).cValue = DLLFunzioni.Transcodifica_PathPrj_Win2Unix(MabseF_List.GridList.ListItems(i).ListSubItems(7).Text)
         
         '*********non serve*********************
         ReDim Preserve m_ClientMsg.Functions.Parameters(9)
         m_ClientMsg.Functions.Parameters(9).cTag = "PATH_COMPCFG"
         m_ClientMsg.Functions.Parameters(9).cValue = "AAAAAAAAAAAAAAAAAAAA" '"$SERVER_CONF"
         '**************************************
         
         ReDim Preserve m_ClientMsg.Functions.Parameters(10)
         m_ClientMsg.Functions.Parameters(10).cTag = "ID_OGGETTO"
         m_ClientMsg.Functions.Parameters(10).cValue = Format(MabseF_List.GridList.ListItems(i).Text, "##########")
         
         
         'CLIENT EXTRA REQUEST
         ReDim Preserve m_ClientReq.Functions.Parameters(0)
         
         m_ClientReq.Functions.Name = "SET_LOG_FILE"
         m_ClientReq.Functions.Parameters(0).cTag = "PATH_LOG"
         m_ClientReq.Functions.Parameters(0).cValue = "$PATH_SERVER"
         
         ReDim Preserve m_ClientReq.Functions.Parameters(1)
         m_ClientReq.Functions.Parameters(1).cTag = "FILE_LOG"
         m_ClientReq.Functions.Parameters(1).cValue = "CPLOG" & Format(m_ClientMsg.Functions.Parameters(10).cValue, "000000000") & ".LOG"
   
         wStr = wParseCob32.FormatString(m_WhoIs, m_ClientMsg, m_ClientReq)
         
         'Send dei dati
         If wSokComp.State = 7 Then 'ale 15/12/2005
            wSokComp.SendData wStr
         Else
            Exit Sub
         End If
      End If
   Next i
End Sub
