VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MabseF_DbStructA 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Prj Db Structure"
   ClientHeight    =   7515
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   10845
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7515
   ScaleWidth      =   10845
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ListView lswColumns 
      Height          =   7335
      Left            =   4080
      TabIndex        =   0
      Top             =   0
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   12938
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Ord. Position"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Column Name"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Type"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Comment"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView lswTables 
      Height          =   7335
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   12938
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   12648447
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Table Name "
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Description"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "MabseF_DbStruct"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    
  Dim strconnection As String, rs As Recordset, sCurrentTable As String, sNewTable As String
  Dim arrayTabsC() As String, arrayTabsP() As String
  Dim customerColumns() As String, prjColumns() As String
  Dim i As Long, c As Long, z As Long
  Dim boolTrovato As Boolean, boolTab As Boolean
  Dim prjField() As String, prjCustomer() As String
  Dim appArrayP() As String, appArrayC() As String
  Dim myItem As ListItem
  Dim sDescription As String
   

  Set CustomerConnection = New ADODB.Connection
'  strConnection = DLLFunzioni.FnConnection
  strconnection = "Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=" & ProjectPath & "\" & DatabaseName & ". ;Mode=Read;"

  CustomerConnection.ConnectionString = strconnection
  CustomerConnection.Open CustomerConnection.ConnectionString

  Set rs = New Recordset
  Set rs = CustomerConnection.OpenSchema(adSchemaTables) 'adSchemaColumns, Array(Empty, Empty, Empty, "TABLE"))

  sCurrentTable = ""
  sNewTable = ""
  i = 0
  Set customerTabs = New Collection
  ReDim arrayTabsC(0)
  lswTables.ListItems.Clear
  
  Do Until rs.EOF
    sCurrentTable = rs!TABLE_NAME
    If (sCurrentTable <> sNewTable) Then
      If sNewTable <> "" Then
        Set myItem = lswTables.ListItems.Add(, , sNewTable)
        myItem.ListSubItems.Add , , sDescription
        i = 0
      End If
      sNewTable = rs!TABLE_NAME
      sDescription = IIf(IsNull(rs!Description), "", rs!Description)
      ReDim customerColumns(0)
    End If
    rs.MoveNext
    i = i + 1
  Loop

  'MF - 19/07/07
  'Raggiungeva fine file e non scriveva l'ultima tabella (in questo caso XE_CopyMap)
  If rs.EOF Then
    Set myItem = lswTables.ListItems.Add(, , sNewTable)
    i = 0
  End If

  rs.Close
  Set rs = Nothing
  CustomerConnection.Close
  Set CustomerConnection = Nothing
End Sub

Private Sub lswColumns_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
     'ordina la lista
  Dim Key As Long
  Static Order As String
 
  Order = lswColumns.SortOrder
  
  Key = ColumnHeader.Index
  
  'assegna alla variabile globale l'index colonna selezionato
  ListObjCol = Key
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswColumns.SortOrder = Order
  lswColumns.SortKey = Key - 1
  lswColumns.Sorted = True
End Sub

Private Sub lswTables_ItemClick(ByVal Item As MSComctlLib.ListItem)
    
  Dim strconnection As String, rs As Recordset, sCurrentTable As String
  Dim Criteri As Variant, myItem As ListItem
    
  lswColumns.ListItems.Clear
    
  sCurrentTable = Item.Text
  Criteri = Array(Empty, Empty, sCurrentTable)

  Set CustomerConnection = New ADODB.Connection
  strconnection = "Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=" & ProjectPath & "\" & DatabaseName & ". ;Mode=Read;"

  CustomerConnection.ConnectionString = strconnection
  CustomerConnection.Open CustomerConnection.ConnectionString

  Set rs = New Recordset
  
  Set rs = CustomerConnection.OpenSchema(adSchemaColumns, Criteri) 'adSchemaColumns, Array(Empty, Empty, Empty, "TABLE"))
  
  While Not rs.EOF
    Set myItem = lswColumns.ListItems.Add(, , Format(rs!ordinal_position, "00"))
    myItem.ListSubItems.Add , , rs!COLUMN_NAME
    myItem.ListSubItems.Add , , ConvType(rs!DATA_TYPE, IIf(IsNull(rs!CHARACTER_MAXIMUM_LENGTH), "", rs!CHARACTER_MAXIMUM_LENGTH))
    myItem.ListSubItems.Add , , IIf(IsNull(rs!Description), "", rs!Description)
    rs.MoveNext
  Wend
  
  rs.Close
  Set rs = Nothing
  CustomerConnection.Close
  Set CustomerConnection = Nothing
  
  lswColumns_ColumnClick lswColumns.ColumnHeaders(1)
  lswColumns_ColumnClick lswColumns.ColumnHeaders(1) ' lo faccio 2 volte perch� la prima ordina desc.
End Sub
Private Function ConvType(ByVal TypeVal As Long, MaxLen As String) As String
  Select Case TypeVal
    Case adBigInt                    ' 20
      ConvType = "Big Integer"
    Case adBinary                    ' 128
      ConvType = "Binary"
    Case adBoolean                   ' 11
      ConvType = "Boolean"
    Case adBSTR                      ' 8 i.e. null terminated string
      ConvType = "Text"
    Case adChar                      ' 129
      ConvType = "Text"
    Case adCurrency                  ' 6
      ConvType = "Currency"
    Case adDate                      ' 7
      ConvType = "Date/Time"
    Case adDBDate                    ' 133
      ConvType = "Date/Time"
    Case adDBTime                    ' 134
      ConvType = "Date/Time"
    Case adDBTimeStamp               ' 135
      ConvType = "Date/Time"
    Case adDecimal                   ' 14
      ConvType = "Float"
    Case adDouble                    ' 5
      ConvType = "Float"
    Case adEmpty                     ' 0
      ConvType = "Empty"
    Case adError                     ' 10
      ConvType = "Error"
    Case adGUID                      ' 72
      ConvType = "GUID"
    Case adIDispatch                 ' 9
      ConvType = "IDispatch"
    Case adInteger                   ' 3
      ConvType = "Integer"
    Case adIUnknown                  ' 13
      ConvType = "Unknown"
    Case adLongVarBinary             ' 205
      ConvType = "Binary"
    Case adLongVarChar               ' 201
      ConvType = "Text"
    Case adLongVarWChar              ' 203
      ConvType = "Text"
    Case adNumeric                  ' 131
      ConvType = "Long"
    Case adSingle                    ' 4
      ConvType = "Single"
    Case adSmallInt                  ' 2
      ConvType = "Small Integer"
    Case adTinyInt                   ' 16
      ConvType = "Tiny Integer"
    Case adUnsignedBigInt            ' 21
      ConvType = "Big Integer"
    Case adUnsignedInt               ' 19
      ConvType = "Integer"
    Case adUnsignedSmallInt          ' 18
      ConvType = "Small Integer"
    Case adUnsignedTinyInt           ' 17
      ConvType = "Timy Integer"
    Case adUserDefined               ' 132
      ConvType = "UserDefined"
    Case adVarNumeric                 ' 139
      ConvType = "Long"
    Case adVarBinary                 ' 204
      ConvType = "Binary"
    Case adVarChar                   ' 200
      ConvType = "Text"
    Case adVariant                   ' 12
      ConvType = "Variant"
    Case adVarWChar                  ' 202
      ConvType = "Text"
    Case adWChar                     ' 130
      If MaxLen = "0" Then
        ConvType = "Memo" 'Text
      Else
        ConvType = "Text"
      End If
    Case Else
      ConvType = "Unknown"
  End Select
End Function

Private Sub Form_Resize()
  ResizeForm Me
  lswTables.ColumnHeaders(1).Width = lswTables.Width / 3
  lswTables.ColumnHeaders(2).Width = (lswTables.Width / 3) * 8

  lswColumns.ColumnHeaders(1).Width = lswColumns.Width / 8
  lswColumns.ColumnHeaders(2).Width = (lswColumns.Width / 8) * 2
  lswColumns.ColumnHeaders(3).Width = lswColumns.Width / 8
  lswColumns.ColumnHeaders(4).Width = (lswColumns.Width / 8) * 10
End Sub
