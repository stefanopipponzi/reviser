Attribute VB_Name = "MabseM_GlobalSys"
'serve a non mettere mano nelle MsgBox
Global GbNomeProdotto As String '= "i-4.Migration - v3.20"
'Global Const GbNomeProdotto As String = App.ProductName & " " & App.Comments
Global Const GbReviserWebSite As String = "http://www.i-4.it/pages/i-4.Migration.asp"
Global GbImgDir As String

'Misure iniziali della form MDI
Global Const GbMDIHeight As Long = 7410 + 1705 + 300
Global Const GbMDIWidth As Long = 11190 + 1905 + 100
Global Const c_MaxSource_NameLen = 50 'Lunghezza max nome degli oggetti da importare
Global Const c_FileHelp As String = "i-4.Migration_User_Manual.doc"

Global GbTop As Long
Global GbLeft As Long
Global GbWidth As Long
Global GbHeight As Long

Global GbDllBaseExist As Boolean
Global GbDLLDli2RdbMsExist As Boolean
Global GbDllParserExist As Boolean
Global GbDllDataManExist As Boolean
Global GbDllToolsExist As Boolean
Global GbDllACTExist As Boolean
Global GbFileUserExist As Boolean
Global GbDllAnalisiExist As Boolean
Global GbDllLicenzaExist As Boolean
Global GbDllImsExist As Boolean
Global GbDllFunzioniExist As Boolean
Global GbDllVSAMExist As Boolean

'Variabile per contenere la path iniziale del percorso definito dall'utente come parametro
Public GbPathDef As String
Public GbUnitDef As String
Public GbTipoMigrazione As String
Global GbComputerName As String

Global GbPathSystem32 As String

Global GbPathPrj As String
Global GbPathPrd As String
Global GbPathImg As String
Global GbItemList As String
Global GbExternalEditor As String
'S compare editor
Global GbCompareEditor As String

'Parametri per stringa connessione ADO
Global GbProvider_Sys As String
Global GbSecurityInfo_Sys As String
Global GbId_Sys As String
Global GbDataSource_Sys As String
Global GbDatabase_Sys As String
Global GbProvider_Usr As String
Global GbSecurityInfo_Usr As String
Global GbId_Usr As String
Global GbDataSource_Usr As String
Global GbConnectionColl As New Collection 'Contiene tutte le variabili sopra da passare alla
                                          'funzione che genera la stringa di connessione
Global GbSysMdbPath As String
Global GbAccessProvider As String
Global GbSqlServerProvider As String
Global GbAsterixForDelete As String
Global GbAsterixForWhere As String

'Variabili per definire la connesisone corrente
Global GbCurProvider As String
Global GbCurServerIp As String
Global GbCurUserName As String
Global GbCurPassword As String
Global GbCurDefaultDatabase As String
Global GbCurTipoDB As e_Provider
Global GbCurDBPath As String
Global GbCurDBName As String  'MF 09/08/07


'Variabili per definire quele finestra � attiva
Global GbFinParam As Boolean
Global GbFinExport As Boolean
Global GbFinMapEd As Boolean
Global GbFinTxtEd As Boolean
Global GbActWindows As Collection 'Contiene gli handle delle form caricate dal MDI

'Globale per definire l'oggetto selezionato in lista
Global GbIdOggetto As Long

'Variabili per definire il centraggio delle form
Global GbTopForm As Long
Global GbLeftForm As Long
Global GbHeightForm As Long
Global GbWidthForm As Long
Global GbHeightNoFinImm As Long

'Globale per la finestra immediata
Global GbFinImCheck As Boolean

'indica che la finestra Main � al primo avvio (per i resize)
Global GbFirstLoad As Boolean

'Serve per sapere Quale Modalit� di progetto il cliente ha caricato
Global GbModePrj As String
Global GbCodePrj As String
Global GbOpenMode As String
Global GbNewOpen As e_NewOpen

'Stabilisce se la connessione MFE � attiva
Global GbConnMFE As Boolean
Global GbSokBufferReader As String

'Serve per sapere la ragione sociale del Cliente
Global GbRagSocCli As String

'dichiarazioni variabili per le DLL (menu)
Global DllBase As New MabsdC_Functions
Global DllParser As New MapsdC_Menu
Global DllDataM As Object ' MadmdC_Menu
Global DLLDli2RdbMs As Object 'MadrdC_Menu
Global DLLTools As New MatsdC_Functions
Global DLLIms As Object 'MaimdC_Menu
Global DLLACT As New MaACTXXXX
Global DLLLic As New Licenza
Global DLLFunzioni As New MaFndC_Funzioni
Global DLLDataAnalysis As Object 'New MaandC_Menu

'A xyz
Global DLLVSAM2RdbMs As Object 'MavsdC_Menu

'Variabile per indicare lo stato di Taglia,Copia,Incolla
Public Enum GbActionTCI
  GbTaglia = 1
  GbCopia = 2
  GbIncolla = 3
End Enum

Global GbStatus_TCI As Long

'serve per sapere quanto pu� essere piccola la form MDI
Global GbMaxWidth As Long
Global GbMaxheight  As Long

'Indica lo stato attivo/disattivo del coolmenu
Global GbCoolMenu_Enabled As Boolean

'servono per memorizzare gli oggetti appena inseriti con la ADD (per fare poi il parsing)
Public Type Obj
  IdOggetto As Long
  Nome As String
End Type

Global GbOggetti() As Obj

'Serve per contenere gli oggetti validi dello User
Global ObjUser() As String
Global GbPswUser As String

Global GbPrdID As String
Global GbLicType As String
Global GbNomeCliente As String
'AC
Global GbCheckObj As String
Global GbNUMID As String


'Array in memoria per avere i vari dizionari
Global GbDizCOBOL() As String
Global GbDizASM() As String

'Variabile per sapere auando aggiungo gli oggetti la scelta di ritorno dalla
'Form MSGBox Personalizzata
Global GbSceltaMsgBox As Long
Global GbCurrentSourceImport As String

'Variabili contenenti i parametri del prodotto
Global GbArrayDBD() As String
Global GbArrayORWord() As String
Global GbArrayNewWord() As String

'Array per contenere il codice del pacchetto Associato al codice di install
Type ComposizioneBase
  Code As String 'Lettera di riconoscimento
  DM As Boolean
  DLI As Boolean
  IM As Boolean
End Type

Type ComposizioneExt
  Code As String 'Lettera di riconoscimento
  UTY As Boolean
  MC As Boolean
  AN As Boolean
End Type

Global GbCompBase() As ComposizioneBase
Global GbCompExt() As ComposizioneExt

'Costanti per contenere il nome delle DLL
Public Const NomeDLLBase As String = "i-4.Mig.Base.dll"
Public Const NomeDLLParser As String = "i-4.Mig.Parser.dll"
Public Const NomeDLLDataM As String = "i-4.Mig.DataConversion.dll"
Public Const NomeDLLFunzioni As String = "i-4.Mig.Functions.dll"
Public Const NomeDLLAnalisi As String = "i-4.Mig.dummy.dll"
Public Const NomeDLLTools As String = "i-4.Mig.CodeConversion.dll"
Public Const NomeDLLLic As String = "i-4.Mig.Licence.dll"
Public Const NomeDLLACT As String = "i-4.Mig.dummy.dll"
Public Const NomeDLLDli2RDBMs As String = "i-4.Mig.Ims.dll"
Public Const NomeDLLIms As String = "i-4.Mig.Ims.dll"
Public Const NomeDLLVSAM2RDBMs As String = "i-4.Mig.Vsam.dll"
Public Const NomeFileCTRL As String = "rvctrl.sys"

Global DatabaseName As String, ProjectName As String
Global ProjectPath As String

Public Function IsScreenFontSmall() As Boolean
    Dim hWndDesk As Long
    Dim hDCDesk As Long
    Dim logPix As Long
    Dim r As Long
    hWndDesk = GetDesktopWindow()
    hDCDesk = GetDC(hWndDesk)
    logPix = GetDeviceCaps(hDCDesk, LOGPIXELSX)
    r = ReleaseDC(hWndDesk, hDCDesk)
    If logPix = 96 Then IsScreenFontSmall = True
    Exit Function
End Function

Sub ResizeControls(frmName As Form)
    On Error Resume Next
    Dim designwidth As Long
    Dim designheight As Long
    Dim designfontsize As Integer
    Dim currentfontsize As Integer
    Dim numofcontrols As Integer
    Dim a As Integer
    Dim movetype As String, moveamount As Integer
    Dim GetResolutionX As Variant
    Dim GetResolutionY As Variant
    Dim RatioX As Variant
    Dim RatioY As Variant
    Dim FontRatio As Variant
    
    Dim ModFont As Boolean
    
    Dim Bool As Boolean
    
    designwidth = frmName.Width / Screen.TwipsPerPixelX
    designheight = frmName.Height / Screen.TwipsPerPixelY
    designfontsize = 96

    GetResolutionX = GbWidthForm / Screen.TwipsPerPixelX
    GetResolutionY = GbHeightForm / Screen.TwipsPerPixelY
    
    RatioX = GetResolutionX / designwidth
    RatioY = GetResolutionY / designheight
    
    If IsScreenFontSmall Then
      currentfontsize = 96
    Else
      currentfontsize = 120
    End If
    
    FontRatio = designfontsize / currentfontsize
    
    If RatioX = 1 And RatioY = 1 And FontRatio = 1 Then
      frmName.Move GbLeftForm, GbTopForm, GbWidthForm, GbHeightForm
      Exit Sub
    End If
    
    numofcontrols = frmName.Controls.count - 1

    For a = 0 To numofcontrols
      If TypeOf frmName.Controls(a) Is ImageList Or _
         TypeOf frmName.Controls(a) Is Menu Or _
         TypeOf frmName.Controls(a) Is Toolbar Then
            
         Bool = True
      End If
      
      If Bool = False Then
        '*****************
        '*** L A B E L ***
        '*****************
        If TypeOf frmName.Controls(a) Is Label Then
          'il font non cambia
        
          frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        '***********************
        '*** F L E X G R I D ***
        '***********************
        If TypeOf frmName.Controls(a) Is MSFlexGrid Then
          'il font non cambia
        
          frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          'frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        '*********************************
        '*** C O M M A N D B U T T O N ***
        '*********************************
        If TypeOf frmName.Controls(a) Is CommandButton Then
          'il font non cambia
        
          'frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        '*********************
        '*** T E X T B O X ***
        '*********************
        If TypeOf frmName.Controls(a) Is TextBox Then
          'il font non cambia
          frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        '*****************
        '*** F R A M E ***
        '*****************
        If TypeOf frmName.Controls(a) Is Frame Then
          'il font non cambia
        
          frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        '***********************
        '*** C H E C K B O X ***
        '***********************
        If TypeOf frmName.Controls(a) Is CheckBox Then
          'il font non cambia
        
          frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        '***********************
        '*** L I S T V I E W ***
        '***********************
        If TypeOf frmName.Controls(a) Is ListView Then
          'il font non cambia
        
          frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        '*****************************
        '*** P R O G R E S S B A R ***
        '*****************************
        If TypeOf frmName.Controls(a) Is ProgressBar Then
          'il font non cambia
        
          frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        '***********************
        '*** T R E E V I E W ***
        '***********************
        If TypeOf frmName.Controls(a) Is TreeView Then
          'il font non cambia
        
          frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        '*****************
        '*** I M A G E ***
        '*****************
        If TypeOf frmName.Controls(a) Is Image Then
          'il font non cambia
        
          'frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        
        '***********************
        '*** C O M B O B O X ***
        '***********************
        If TypeOf frmName.Controls(a) Is ComboBox Then
          'il font non cambia
        
          frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
        
        '*****************
        '*** S S T A B ***
        '*****************
        If TypeOf frmName.Controls(a) Is SSTab Then
          'il font non cambia
        
          frmName.Controls(a).Width = frmName.Controls(a).Width * RatioX
          frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          frmName.Controls(a).Top = frmName.Controls(a).Top * RatioY
          frmName.Controls(a).Left = frmName.Controls(a).Left * RatioX
        End If
      Else
        Bool = False
      End If
    Next a
    
    frmName.Move GbLeftForm, GbTopForm, GbWidthForm, GbHeightForm
End Sub

Public Sub WaitTime(sec As Long)
  Dim PauseTime, Start, Finish, TotalTime
  
  PauseTime = sec
  Start = Timer
  Do While Timer < Start + PauseTime
  
  Loop
  Finish = Timer
  TotalTime = Finish - Start
    
End Sub
    
Public Function getCtrl() As Date
   Dim fLen As Long
   Dim wRecord As String
   Dim wKey As String
   
   'Ritorna la data 31/05/1975 23.54.00 se va in errore
   On Error GoTo hError
   
   Open GbPathSystem32 & "\" & NomeFileCTRL For Binary As #1
    
   fLen = 6000
   
   wRecord = String(24, Chr(0))
    
   Get #1, fLen, wRecord
   
   For i = 1 To Len(wRecord)
      Select Case Mid(wRecord, i, 1)
         Case "E"
            wKey = wKey & "0"
         
         Case "R"
            wKey = wKey & "1"
         
         Case "T"
            wKey = wKey & "2"
   
         Case "Q"
            wKey = wKey & "3"
            
         Case "P"
            wKey = wKey & "4"
            
         Case "N"
            wKey = wKey & "5"
            
         Case "1"
            wKey = wKey & "6"
            
         Case "L"
            wKey = wKey & "7"
            
         Case "H"
            wKey = wKey & "8"
            
         Case "W"
            wKey = wKey & "9"
            
         Case "S"
            wKey = wKey & "/"
            
         Case "G"
            wKey = wKey & "."
            
         Case "V"
            wKey = wKey & ":"
           
         Case "D"
            wKey = wKey & " "
            
      End Select
   Next i
   
   Close #1
   
   getCtrl = CDate(Trim(wKey))
Exit Function
hError:
   getCtrl = CDate("31/05/1975 23.54.00")
End Function

Public Sub fixCtrl()
   Dim wData As String
   Dim i As Long
   Dim wKey As String
   
   wData = Now
   wData = wData & String(24 - Len(wData), " ")
   
   For i = 1 To Len(wData)
      Select Case Mid(wData, i, 1)
         Case "0"
            wKey = wKey & "E"
         
         Case "1"
            wKey = wKey & "R"
         
         Case "2"
            wKey = wKey & "T"
   
         Case "3"
            wKey = wKey & "Q"
            
         Case "4"
            wKey = wKey & "P"
            
         Case "5"
            wKey = wKey & "N"
            
         Case "6"
            wKey = wKey & "1"
            
         Case "7"
            wKey = wKey & "L"
            
         Case "8"
            wKey = wKey & "H"
            
         Case "9"
            wKey = wKey & "W"
            
         Case "/"
            wKey = wKey & "S"
            
         Case "."
            wKey = wKey & "G"
            
         Case ":"
            wKey = wKey & "V"
           
         Case " "
            wKey = wKey & "D"
            
      End Select
   Next i
   
   Open GbPathSystem32 & "\" & NomeFileCTRL For Binary As #1

'   Put #1, 6000, wKey

   Close #1
End Sub

