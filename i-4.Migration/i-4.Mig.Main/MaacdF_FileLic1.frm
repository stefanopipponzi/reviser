VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MaacdF_FileLic 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " <product License_ file>"
   ClientHeight    =   3015
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3465
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3015
   ScaleWidth      =   3465
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   40
      Left            =   40
      TabIndex        =   2
      Top             =   2280
      Width           =   3345
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Select"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   2280
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2400
      Width           =   1065
   End
   Begin MSComctlLib.ListView LswLIC 
      Height          =   2115
      Left            =   40
      TabIndex        =   0
      Top             =   60
      Width           =   3350
      _ExtentX        =   5900
      _ExtentY        =   3731
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "File"
         Object.Width           =   5821
      EndProperty
   End
End
Attribute VB_Name = "MaacdF_FileLic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOK_Click()
  ACT.AcStatusLIC = True
  Screen.MousePointer = vbHourglass
  Unload Me
End Sub

Private Sub Form_Load()
  Dim i As Long
  
  Me.Caption = "i-4.Migration" & " - License Files:"
  Screen.MousePointer = vbDefault
  
  For i = 1 To UBound(FileLIC)
    LswLIC.ListItems.Add , , FileLIC(i)
  Next i
  
  ACT.AcNomeFileLic = LswLIC.ListItems(1).Text 'Default il primo
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  ACT.AcStatusLIC = False
  Screen.MousePointer = vbHourglass
  Unload Me
End Sub

Private Sub LswLIC_DblClick()
  ACT.AcStatusLIC = True
  Screen.MousePointer = vbHourglass
  Unload Me
End Sub

Private Sub LswLIC_ItemClick(ByVal Item As MSComctlLib.ListItem)
  ACT.AcNomeFileLic = Item.Text
End Sub

Private Sub LswLIC_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then
    ACT.AcStatusLIC = True
    Screen.MousePointer = vbHourglass
    Unload Me
  End If
End Sub
