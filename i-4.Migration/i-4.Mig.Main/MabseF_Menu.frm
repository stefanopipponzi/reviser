VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MabseF_Menu 
   BackColor       =   &H00FFFF80&
   BorderStyle     =   0  'None
   ClientHeight    =   1740
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15555
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   1740
   ScaleWidth      =   15555
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   Begin VB.Frame fraHome 
      BackColor       =   &H00FFFF80&
      Height          =   1375
      Left            =   -5
      TabIndex        =   20
      Top             =   360
      Width           =   15600
      Begin VB.CommandButton cmdProjectInventory 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   720
         Picture         =   "MabseF_Menu.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdReportGenerator 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   2760
         Picture         =   "MabseF_Menu.frx":040E
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdParser 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   4802
         Picture         =   "MabseF_Menu.frx":0819
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdProjectException 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   6840
         Picture         =   "MabseF_Menu.frx":0BDE
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   480
         Width           =   700
      End
      Begin VB.Label lblHome 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Project Inventory"
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   0
         Left            =   105
         TabIndex        =   28
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblHomea 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Report Generator"
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   1
         Left            =   2040
         TabIndex        =   27
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblHomea 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Project Exceptions"
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   2
         Left            =   6120
         TabIndex        =   26
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblHomea 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Parser"
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   3
         Left            =   4080
         TabIndex        =   25
         Top             =   195
         Width           =   2145
      End
   End
   Begin VB.Frame fraDataConversion 
      BackColor       =   &H00FFFF80&
      Height          =   1375
      Left            =   -5
      TabIndex        =   0
      Top             =   360
      Width           =   15600
      Begin VB.CommandButton cmdDatabase 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   720
         Picture         =   "MabseF_Menu.frx":0F92
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdDataIMS 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   2760
         Picture         =   "MabseF_Menu.frx":3D85
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdDataVSAM 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   4802
         Picture         =   "MabseF_Menu.frx":4165
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdDataDB2 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   6840
         Picture         =   "MabseF_Menu.frx":4545
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   480
         Width           =   700
      End
      Begin VB.Label lblDatabase 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Database"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   720
         TabIndex        =   8
         Top             =   195
         Width           =   705
      End
      Begin VB.Label lblIMS 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "IMS-DB to RDBMS"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   2415
         TabIndex        =   7
         Top             =   195
         Width           =   1395
      End
      Begin VB.Label lblDb2 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "DB2 to Oracle"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   6675
         TabIndex        =   6
         Top             =   195
         Width           =   1035
      End
      Begin VB.Label lblVsam 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Vsam to RDBMS"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   4545
         TabIndex        =   5
         Top             =   195
         Width           =   1215
      End
   End
   Begin MSComctlLib.TabStrip menuTabs 
      Height          =   375
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Width           =   15495
      _ExtentX        =   27331
      _ExtentY        =   661
      TabWidthStyle   =   2
      TabFixedWidth   =   4410
      TabFixedHeight  =   582
      TabMinWidth     =   1764
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   6
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Home"
            Key             =   "home"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Data Migration"
            Key             =   "dataConversion"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Code Migration"
            Key             =   "codeConversion"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Rehosting"
            Key             =   "rehosting"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "IMS/DB"
            Key             =   "imsDB"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab6 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "IMS/DC"
            Key             =   "imsDC"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Calibri"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraToolbar 
      BackColor       =   &H00FFFF80&
      Caption         =   "Shortcuts"
      Height          =   1220
      Left            =   8040
      TabIndex        =   19
      Top             =   460
      Width           =   7395
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   23
         Left            =   2040
         TabIndex        =   53
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   22
         Left            =   2280
         TabIndex        =   52
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   21
         Left            =   2520
         TabIndex        =   51
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   20
         Left            =   2760
         TabIndex        =   50
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   19
         Left            =   3000
         TabIndex        =   49
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   18
         Left            =   3360
         TabIndex        =   48
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   17
         Left            =   3600
         TabIndex        =   47
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   16
         Left            =   3840
         TabIndex        =   46
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   15
         Left            =   4080
         TabIndex        =   45
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   14
         Left            =   4320
         TabIndex        =   44
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   13
         Left            =   4560
         TabIndex        =   43
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   12
         Left            =   4800
         TabIndex        =   42
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   11
         Left            =   5040
         TabIndex        =   41
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   10
         Left            =   5280
         TabIndex        =   40
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   9
         Left            =   840
         TabIndex        =   39
         Top             =   600
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   8
         Left            =   960
         TabIndex        =   38
         Top             =   600
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   7
         Left            =   1320
         TabIndex        =   37
         Top             =   600
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   6
         Left            =   1920
         TabIndex        =   36
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   5
         Left            =   1680
         TabIndex        =   35
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   4
         Left            =   1440
         TabIndex        =   34
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   3
         Left            =   1200
         TabIndex        =   33
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   2
         Left            =   1080
         TabIndex        =   32
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "1"
         Height          =   400
         Index           =   1
         Left            =   840
         TabIndex        =   31
         Top             =   360
         Width           =   400
      End
      Begin VB.CommandButton Buttons 
         Caption         =   "0"
         Height          =   400
         Index           =   0
         Left            =   240
         TabIndex        =   30
         Top             =   360
         Width           =   400
      End
   End
   Begin VB.Frame fraRehosting 
      BackColor       =   &H00FFFF80&
      Height          =   1400
      Left            =   0
      TabIndex        =   54
      Top             =   360
      Width           =   15600
      Begin VB.CommandButton cmdImportExport 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   6840
         Picture         =   "MabseF_Menu.frx":4925
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdFileConversion 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   4802
         Picture         =   "MabseF_Menu.frx":4D3E
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdMacros 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   2760
         Picture         =   "MabseF_Menu.frx":5193
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdRehosting 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   720
         Picture         =   "MabseF_Menu.frx":55FD
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   480
         Width           =   700
      End
      Begin VB.Label lblRehosting 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "File Conversion"
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   5
         Left            =   4080
         TabIndex        =   62
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblRehosting 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Import/Export"
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   4
         Left            =   6120
         TabIndex        =   61
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblRehosting 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Macros Lab"
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   0
         Left            =   2040
         TabIndex        =   60
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblRehosting 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Code Conversion"
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   1
         Left            =   178
         TabIndex        =   59
         Top             =   195
         Width           =   1785
      End
   End
   Begin VB.Frame fraDummy 
      BackColor       =   &H00FFFF80&
      BorderStyle     =   0  'None
      Height          =   1455
      Left            =   -5
      TabIndex        =   18
      Top             =   360
      Width           =   12615
   End
   Begin VB.Frame fraImsDc 
      BackColor       =   &H00FFFF80&
      Height          =   1375
      Left            =   0
      TabIndex        =   63
      Top             =   360
      Width           =   15600
      Begin VB.CommandButton cmdIoRoutineDC 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   4802
         Picture         =   "MabseF_Menu.frx":59C2
         Style           =   1  'Graphical
         TabIndex        =   66
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdProgramEncapsulationDC 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   2760
         Picture         =   "MabseF_Menu.frx":5DFD
         Style           =   1  'Graphical
         TabIndex        =   65
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdMfsMigration 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   720
         Left            =   780
         Picture         =   "MabseF_Menu.frx":62A1
         Style           =   1  'Graphical
         TabIndex        =   64
         Top             =   480
         Width           =   700
      End
      Begin VB.Label lblIoRoutineDC 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "i-o Routine Generation"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   4080
         TabIndex        =   69
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblProgramEncapsulationDC 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Program Encapsulation"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   2040
         TabIndex        =   68
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblMfsMigration 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "MFS Migration"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   120
         TabIndex        =   67
         Top             =   195
         Width           =   2025
      End
   End
   Begin VB.Frame fraCodeConversion 
      BackColor       =   &H00FFFF80&
      Height          =   1375
      Left            =   -5
      TabIndex        =   9
      Top             =   360
      Width           =   15600
      Begin VB.CommandButton cmdSas 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   6840
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   480
         Visible         =   0   'False
         Width           =   700
      End
      Begin VB.CommandButton cmdPli 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   4802
         Picture         =   "MabseF_Menu.frx":6659
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdEasytrieve 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   2760
         Picture         =   "MabseF_Menu.frx":6B38
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdAssembler 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   720
         Left            =   780
         Picture         =   "MabseF_Menu.frx":6FFF
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   480
         Width           =   700
      End
      Begin VB.Label lblPLI 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "PLI"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   4080
         TabIndex        =   17
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblSAS 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "SAS"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   6120
         TabIndex        =   16
         Top             =   195
         Visible         =   0   'False
         Width           =   2145
      End
      Begin VB.Label lblEasytrieve 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Easytrieve"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   2040
         TabIndex        =   15
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblAssembler 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Assembler"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   120
         TabIndex        =   14
         Top             =   195
         Width           =   2025
      End
   End
   Begin VB.Frame fraIms 
      BackColor       =   &H00FFFF80&
      Height          =   1380
      Left            =   0
      TabIndex        =   70
      Top             =   360
      Width           =   15600
      Begin VB.CommandButton cmdIoRoutine 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   4802
         Picture         =   "MabseF_Menu.frx":74CD
         Style           =   1  'Graphical
         TabIndex        =   73
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdProgramEncapsulation 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   2760
         Picture         =   "MabseF_Menu.frx":7908
         Style           =   1  'Graphical
         TabIndex        =   72
         Top             =   480
         Width           =   700
      End
      Begin VB.CommandButton cmdDliLab 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   720
         Left            =   780
         Picture         =   "MabseF_Menu.frx":7DA4
         Style           =   1  'Graphical
         TabIndex        =   71
         Top             =   480
         Width           =   700
      End
      Begin VB.Label lblIoRoutine 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "i-o Routine Generation"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   4080
         TabIndex        =   76
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblProgramConversion 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "Program Encapsulation"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   2040
         TabIndex        =   75
         Top             =   195
         Width           =   2145
      End
      Begin VB.Label lblDliLab 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF80&
         Caption         =   "DL/I Lab"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   120
         TabIndex        =   74
         Top             =   195
         Width           =   2025
      End
   End
End
Attribute VB_Name = "MabseF_Menu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
    Option Explicit
' HOME menu
Private Const MNU_HOME_PROJECT_INVENTORY = 2
Private Const MNU_HOME_REPORT_GENERATOR = 3
Private Const MNU_HOME_PROJECT_EXCEPTIONS = 4
Private Const MNU_HOME_PARSER = 5
' DATA CONVERSION menu
Private Const MNU_DATA_DATABASES = 15
Private Const MNU_DATA_IMS = 16
Private Const MNU_DATA_VSAM = 17
Private Const MNU_DATA_DB2 = 18
' CODE CONVERSION menu
Private Const MNU_CODE_ASM = 34
Private Const MNU_CODE_EZT = 35
Private Const MNU_CODE_PLI = 36
Private Const MNU_CODE_SAS = 0
' REHOSTING menu
Private Const MNU_REHOSTING = 20
Private Const MNU_REHOSTING_MACROS = 21
Private Const MNU_REHOSTING_IMPEXP = 22
Private Const MNU_REHOSTING_FILECONVERSION = 23
' IMS/DB menu
Private Const MNU_IMSDB_DLI = 7
Private Const MNU_IMSDB_ENCAPS = 8
Private Const MNU_IMSDB_IOROUTINE = 9
' IMS/DC menu
Private Const MNU_IMSDC_MFS = 27
Private Const MNU_IMSDC_ENCAPS = 28
Private Const MNU_IMSDC_IOROUTINE = 29
''

Dim MaxNcmd As Long
Dim MaxNBut As Long
Dim NcmdMin As Long
Dim NcmdMax As Long
Dim nButMin(10) As Long
Dim nButMax(10) As Long

'serve a tenere traccia del'ultimo bottone premuto
Public IndexButton As Long
'SQ Public TagButton As String

Public ButtonSelIndex As Long

Sub Resize()
  On Error Resume Next

  Me.Move GbLeft, GbTop, GbWidth, Me.Height
  
  resizeShortcuts
  
End Sub

Sub ResizeMnu()
  Dim k As Long
  
  On Error GoTo ERR

  For k = 1 To MaxNBut
    '''cmdHome(k).Visible = False
    lblHome(k).Visible = False
    lblHome(k).Enabled = False
  Next k

  For k = 1 To NcmdMax
    '''cmdHome(k).Visible = True
    lblHome(k).Visible = True
    lblHome(k).Enabled = True

    '''cmdHome(k).Move (k + 1) * cmdHome(0).Left + k * cmdHome(0).Width, cmdHome(0).Top, cmdHome(k).Width, cmdHome(k).Height
    lblHome(k).Move (k + 1) * lblHome(0).Left + k * lblHome(0).Width, lblHome(k).Width, lblHome(k).Height
  Next

  Exit Sub
ERR:
  If ERR.Number = 5 Then 'not can minimize
    Resume Next
  End If
End Sub
Public Sub resizeShortcuts()
  'Toolbar panel right-justified to details panel
  fraToolbar.Left = (MabseF_List.fraDetails.Left + MabseF_List.fraDetails.Width) - fraToolbar.Width
End Sub

Private Sub runButtonMenu(Index As Integer)
  Dim i As Long
  Dim wId As String
  Dim bFind As Boolean
  
  'scrive ctrl
  fixCtrl
  
'  wId = cmdHome(Index).tag
'  If Not IsNumeric(wId) Then
'    Select Case wId
'      Case "Assembler"
'        Attiva_Funzione "MatsdP_00", New Collection, "ASSEMBLER_CONV", ""
'        Exit Sub
'      Case "EZT"
'        Attiva_Funzione "MatsdP_00", New Collection, "EZT_CONV", ""
'        Exit Sub
'      Case "PLI"
'        Attiva_Funzione "MatsdP_00", New Collection, "PLI_CONV", ""
'        Exit Sub
'      Case "SAS"
'        Attiva_Funzione "MatsdP_00", New Collection, "SAS_CONV", ""
'        Exit Sub
'    End Select
'  End If
'
'  'SQ tmp
'  For i = 1 To UBound(TotMenu)
'    Debug.Print i & ": " & TotMenu(i).tag & " - " & TotMenu(i).Label & " - " & TotMenu(i).Funzione
'  Next i
  
'SQ
  'Cerca nel menu (Array) l'id e recupera le informazioni
'  For i = 1 To UBound(TotMenu)
'    If TotMenu(i).id = Val(wId) Then
'      bFind = True
'      Exit For
'    End If
'  Next i
  i = Index
  If (bFind Or True) And GbCoolMenu_Enabled Then
    Attiva_Funzione TotMenu(i).DllName, New Collection, TotMenu(i).Funzione, TotMenu(i).TipoFinIm
     
    'SQ TagButton = cmdHome(Index).tag
    IndexButton = Index
  Else
    MabseF_Start.Show vbModal
  End If
End Sub

Private Sub CmdStr_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  'prova per l'attivazione a livello visuale dei bottoni
  'per ora � stato fatto solo con il bottone project info
  'SQ TagButton = cmdHome(Index).tag
  IndexButton = Index
End Sub

Public Sub CmdUty_Click(Index As Integer)
  Dim i As Long
  
''  Ncmd = Index
''  'Globale
''  ButtonSelIndex = Index
''
''  NcmdMin = nButMin(Index)
''  NcmdMax = nButMax(Index)
''
''  ResizeMnu
''
''  On Error Resume Next
''
''  If nButMin(Index) = 0 Then
''    '''cmdHome(0).SetFocus
''  Else
''    MabseF_Menu.Refresh
''    For i = 0 To MabseF_Menu.cmdHome.count - 1
''      MabseF_Menu.cmdHome(i).Refresh
''    Next i
''  End If
''
''  'Scrive ctrl
''  fixCtrl
End Sub


Private Sub Form_Load()
  '''Carica_Type_Menu_MODULO_BASE
  Me.fraDummy.ZOrder 1
  
  Resize
  
  '''Init_Cool_Menu
 
  '''Abilita_Disabilita_CoolMenu True
  
  '''NcmdMin = nButMin(0)
  '''NcmdMax = nButMax(0)
  
  '''''''''''''''''''''''
  ' Toolbar color
  '''''''''''''''''''''''
  ChangeTBBack Me.menuTabs, CreateSolidBrush(RGB(102, 255, 255)), enuTB_STANDARD
  'Refresh Screen to see changes
  InvalidateRect 0&, 0&, False
End Sub

Private Sub Init_Cool_Menu()
  Dim i As Long, j As Long
  
''  If UBound(DefMenu) <> 0 Then
''    'CREA I VARI BOTTONI E IMMAGINI NEL MENU leggendo l'array in memoria'
''    Ncmd = 0
''
''    '''CmdUty(Ncmd).Caption = DefMenu(1).ButtonMenu
''    '''CmdUty(Ncmd).Tag = DefMenu(1).id
''
''    MaxNBut = 0
''    nButMin(Ncmd) = MaxNBut
''
''    If UBound(DefMenu(1).Bottoni) <> 0 Then
''      cmdHome(MaxNBut).Picture = LoadPicture(DefMenu(1).Bottoni(1).ImageEn)
''      lblHome(MaxNBut).Caption = DefMenu(1).Bottoni(1).Label
''
''      cmdHome(MaxNBut).Tag = DefMenu(1).Bottoni(1).id
''    End If
''
''    For i = 2 To UBound(DefMenu(1).Bottoni)
''      MaxNBut = MaxNBut + 1
''      Load cmdHome(MaxNBut)
''      cmdHome(MaxNBut).Picture = LoadPicture(DefMenu(1).Bottoni(i).ImageEn)
''      Load lblHome(MaxNBut)
''      lblHome(MaxNBut).Caption = DefMenu(1).Bottoni(i).Label
''      cmdHome(MaxNBut).Tag = DefMenu(1).Bottoni(i).id
''    Next i
''
''    nButMax(Ncmd) = MaxNBut
''
''    For i = 2 To UBound(DefMenu)
''      Ncmd = Ncmd + 1
''      '''Load CmdUty(Ncmd)
''      '''CmdUty(Ncmd).Caption = DefMenu(i).ButtonMenu
''      '''CmdUty(Ncmd).Visible = True
''      '''CmdUty(Ncmd).Tag = DefMenu(i).id
''
''      nButMin(Ncmd) = MaxNBut + 1
''
''      For j = 1 To UBound(DefMenu(i).Bottoni)
''        MaxNBut = MaxNBut + 1
''        Load cmdHome(MaxNBut)
''        cmdHome(MaxNBut).Picture = LoadPicture(DefMenu(i).Bottoni(j).ImageEn)
''        Load lblHome(MaxNBut)
''        lblHome(MaxNBut).Caption = DefMenu(i).Bottoni(j).Label
''        cmdHome(MaxNBut).Tag = DefMenu(i).Bottoni(j).id
''      Next j
''
''      nButMax(Ncmd) = MaxNBut
''    Next i
''
''    'forzatura basata sul tag nella sub runButtonMenu
''    Ncmd = Ncmd + 1
'''    Load CmdUty(Ncmd)
'''    CmdUty(Ncmd).Caption = "Languages"
'''    CmdUty(Ncmd).Visible = True
'''    CmdUty(Ncmd).Tag = "Languages"
''
''    MaxNBut = MaxNBut + 1
''    Load cmdHome(MaxNBut)
''    cmdHome(MaxNBut).Picture = LoadPicture(DefMenu(3).Bottoni(1).ImageEn)
''    Load lblHome(MaxNBut)
''    lblHome(MaxNBut).Caption = "Assembler"
''    cmdHome(MaxNBut).Tag = "Assembler"
''    nButMax(Ncmd) = MaxNBut
''
''    MaxNBut = MaxNBut + 1
''    Load cmdHome(MaxNBut)
''    cmdHome(MaxNBut).Picture = LoadPicture(DefMenu(3).Bottoni(1).ImageEn)
''    Load lblHome(MaxNBut)
''    lblHome(MaxNBut).Caption = "Easytrieve"
''    cmdHome(MaxNBut).Tag = "EZT"
''    nButMax(Ncmd) = MaxNBut
''    ' Ma 24-04-2007 : conto i pulsanti nel men� Tools, altrimeti si tira su tutto
''    nButMin(Ncmd) = nButMin(Ncmd - 1) + 6
''
''    'S 02-10-2008: PLI
''    MaxNBut = MaxNBut + 1
''    Load cmdHome(MaxNBut)
''    cmdHome(MaxNBut).Picture = LoadPicture(DefMenu(3).Bottoni(1).ImageEn)
''    Load lblHome(MaxNBut)
''    lblHome(MaxNBut).Caption = "PLI"
''    cmdHome(MaxNBut).Tag = "PLI"
''    nButMax(Ncmd) = MaxNBut
''    nButMin(Ncmd) = nButMin(Ncmd - 1) + 6
''
''    'S 21-10-2009: SAS
''    MaxNBut = MaxNBut + 1
''    Load cmdHome(MaxNBut)
''    cmdHome(MaxNBut).Picture = LoadPicture(DefMenu(3).Bottoni(1).ImageEn)
''    Load lblHome(MaxNBut)
''    lblHome(MaxNBut).Caption = "SAS"
''    cmdHome(MaxNBut).Tag = "SAS"
''    nButMax(Ncmd) = MaxNBut
''    nButMin(Ncmd) = nButMin(Ncmd - 1) + 6
''
''
''    '************************************************
''    MaxNcmd = Ncmd
''    Ncmd = 0
''    NcmdMin = nButMin(Ncmd)
''    NcmdMax = nButMax(Ncmd)
''
''    ResizeMnu
''  Else
''    DLLFunzioni.Show_MsgBoxError "ML04E", "MabseF_Menu", "Init_Cool_Menu"
''    fixCtrl
''    End
''  End If
''
''  Me.Refresh
End Sub

Private Sub Form_Resize()
  Me.menuTabs.Width = Me.Width
  Me.fraDummy.Width = Me.Width
  Me.fraHome.Width = Me.Width
  Me.fraDataConversion.Width = Me.Width
  Me.fraCodeConversion.Width = Me.Width
  Me.fraRehosting.Width = Me.Width
  Me.fraIms.Width = Me.Width
  Me.fraImsDc.Width = Me.Width
  '
End Sub


Private Sub menuTabs_Click()
  Me.fraHome.Visible = menuTabs.SelectedItem.Key = "home"
  Me.fraDataConversion.Visible = menuTabs.SelectedItem.Key = "dataConversion"
  Me.fraCodeConversion.Visible = menuTabs.SelectedItem.Key = "codeConversion"
  Me.fraRehosting.Visible = menuTabs.SelectedItem.Key = "rehosting"
  Me.fraIms.Visible = menuTabs.SelectedItem.Key = "imsDB"
  Me.fraImsDc.Visible = menuTabs.SelectedItem.Key = "imsDC"
  '
  Me.fraToolbar.Visible = Me.fraHome.Visible And False 'SQ tmp
End Sub


Private Sub cmdParser_Click()
  runButtonMenu MNU_HOME_PARSER
End Sub

Private Sub cmdProjectException_Click()
  runButtonMenu MNU_HOME_PROJECT_EXCEPTIONS
End Sub

Private Sub cmdProjectInventory_Click()
  runButtonMenu MNU_HOME_PROJECT_INVENTORY
End Sub

Private Sub cmdReportGenerator_Click()
  runButtonMenu MNU_HOME_REPORT_GENERATOR
End Sub


Private Sub cmdDatabase_Click()
  runButtonMenu MNU_DATA_DATABASES
End Sub

Private Sub cmdDataDB2_Click()
  runButtonMenu MNU_DATA_DB2
End Sub

Private Sub cmdDataIMS_Click()
  runButtonMenu MNU_DATA_IMS
End Sub

Private Sub cmdDataVSAM_Click()
  runButtonMenu MNU_DATA_VSAM
End Sub


Private Sub cmdFileConversion_Click()
  runButtonMenu MNU_REHOSTING_FILECONVERSION
End Sub

Private Sub cmdImportExport_Click()
  runButtonMenu MNU_REHOSTING_IMPEXP
End Sub

Private Sub cmdMacros_Click()
  runButtonMenu MNU_REHOSTING_MACROS
End Sub

Private Sub cmdRehosting_Click()
  runButtonMenu MNU_REHOSTING
End Sub

Private Sub cmdAssembler_Click()
  runButtonMenu MNU_CODE_ASM
End Sub

Private Sub cmdEasytrieve_Click()
  runButtonMenu MNU_CODE_EZT
End Sub

Private Sub cmdPli_Click()
  runButtonMenu MNU_CODE_PLI
End Sub

Private Sub cmdDliLab_Click()
  runButtonMenu MNU_IMSDB_DLI
End Sub

Private Sub cmdIoRoutine_Click()
  runButtonMenu MNU_IMSDB_IOROUTINE
End Sub

Private Sub cmdProgramEncapsulation_Click()
  runButtonMenu MNU_IMSDB_ENCAPS
End Sub
Private Sub cmdMfsMigration_Click()
  runButtonMenu MNU_IMSDC_MFS
End Sub

Private Sub cmdIoRoutineDc_Click()
  runButtonMenu MNU_IMSDC_IOROUTINE
End Sub

Private Sub cmdProgramEncapsulationDC_Click()
  runButtonMenu MNU_IMSDC_ENCAPS
End Sub

