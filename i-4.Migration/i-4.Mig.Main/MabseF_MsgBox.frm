VERSION 5.00
Begin VB.Form MabseF_MsgBox 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Module Import"
   ClientHeight    =   2025
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5715
   Icon            =   "MabseF_MsgBox.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   2025
   ScaleWidth      =   5715
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   40
      Left            =   20
      TabIndex        =   5
      Top             =   1520
      Width           =   5700
   End
   Begin VB.CheckBox chkAll 
      Caption         =   "Show this dialog for each item in list"
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   330
      TabIndex        =   4
      Top             =   1650
      Value           =   1  'Checked
      Width           =   2925
   End
   Begin VB.CommandButton cmdNo 
      Caption         =   "No"
      Default         =   -1  'True
      Height          =   435
      Left            =   3660
      Picture         =   "MabseF_MsgBox.frx":4C4A
      TabIndex        =   1
      Top             =   990
      Width           =   1095
   End
   Begin VB.CommandButton cmdYes 
      Caption         =   "Yes"
      Height          =   435
      Left            =   930
      Picture         =   "MabseF_MsgBox.frx":508C
      TabIndex        =   0
      Top             =   990
      Width           =   1095
   End
   Begin VB.Image Image1 
      Height          =   435
      Left            =   120
      Picture         =   "MabseF_MsgBox.frx":54CE
      Stretch         =   -1  'True
      Top             =   240
      Width           =   450
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Object ""[WNOME]"" already present in project."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   960
      TabIndex        =   3
      Top             =   150
      Width           =   4050
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Do you want to overwrite it?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   960
      TabIndex        =   2
      Top             =   480
      Width           =   2850
   End
End
Attribute VB_Name = "MabseF_MsgBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAll_Click()
  GbSceltaMsgBox = 1243 'E' un numero inventato che serve a far tornare il valore
                        'Yes ALL : come per vbyes = 6  e vbNo = 7 ---> VbYesAll = 1243
  Me.Hide
  Unload Me
End Sub

Private Sub cmdAnnulla_Click()
  GbSceltaMsgBox = vbCancel
  Me.Hide
  Unload Me
End Sub

Private Sub cmdNo_Click()
  GbSceltaMsgBox = IIf(chkAll, vbNo, 1244)
  Me.Hide
  Unload Me
End Sub

Private Sub cmdYes_Click()
  GbSceltaMsgBox = IIf(chkAll, vbYes, 1243)
  Me.Hide
  Unload Me
End Sub

Private Sub Form_Load()
  Label2.Caption = Replace(Label2.Caption, "[WNOME]", GbCurrentSourceImport)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  SetFocusWnd MabseF_Prj.hwnd
End Sub

