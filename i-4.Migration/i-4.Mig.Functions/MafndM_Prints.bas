Attribute VB_Name = "MafndM_Prints"
Option Explicit

Public Function getIntestazioneDaParametri() As String()
   Dim rSp As ADODB.Recordset
   Dim wIntestazione() As String
   
   'Prende l'intestazione dal database Parametri
   Set rSp = m_DllFunctions.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'BSHEADER'")

   If rSp.RecordCount > 0 Then
      rSp.MoveFirst
      
      If Trim(rSp.Fields("ParameterValue").Value) <> "" Then
         wIntestazione = Split(rSp.Fields("ParameterValue").Value, "~")
      Else
         ReDim wIntestazione(5)
         wIntestazione(0) = "i-4 s.r.l."
         wIntestazione(1) = "i-4.Migration Prints Export"
         wIntestazione(2) = "Via Marano, 5"
         wIntestazione(3) = "47853 - Coriano (RN)"
         wIntestazione(4) = "ITALY"
         wIntestazione(5) = ""
      End If
   Else
      ReDim wIntestazione(5)
      wIntestazione(0) = "i-4 s.r.l."
      wIntestazione(1) = "i-4.Migration Prints Export"
      wIntestazione(2) = "Via Marano, 5"
      wIntestazione(3) = "47853 - Coriano (RN)"
      wIntestazione(4) = "ITALY"
      wIntestazione(5) = ""
   End If

   rSp.Close
    
   Set rSp = Nothing
   
   getIntestazioneDaParametri = wIntestazione
End Function

Public Sub Crea_Foglio_Excel(wNomeFoglio As String, wSQL As String, wBook As Workbook)
   Dim rs As ADODB.Recordset

   Dim r_Start As Long
   Dim i As Long
   Dim eRange As Range
   Dim r As Long
   Dim o_Ex As Object
   Dim wIntestazione() As String
   
   wIntestazione = getIntestazioneDaParametri()
   
   'Parte dalla quarta riga
   r_Start = 6
   r = r_Start + 1
   
   'Setta il foglio
   wBook.Worksheets.Add
   Set o_Ex = wBook.Worksheets.Item(1)
   o_Ex.Name = wNomeFoglio
   
   Set rs = m_DllFunctions.Open_Recordset(wSQL)
   
   If rs.RecordCount > 0 Then
      rs.MoveFirst
      
      Screen.MousePointer = vbHourglass
      
      'Incolla l'intestazione a destra
      o_Ex.Cells(1, 6) = Trim(wIntestazione(0))
      o_Ex.Cells(1, 6).Font.Bold = True
      o_Ex.Cells(2, 6) = Trim(wIntestazione(1))
      o_Ex.Cells(2, 6).Font.Bold = True
      o_Ex.Cells(3, 6) = Trim(wIntestazione(2))
      o_Ex.Cells(3, 6).Font.Bold = True
      o_Ex.Cells(4, 6) = Trim(wIntestazione(3))
      o_Ex.Cells(4, 6).Font.Bold = True
      o_Ex.Cells(5, 6) = Trim(wIntestazione(4))
      o_Ex.Cells(5, 6).Font.Bold = True
      
       Set eRange = o_Ex.Cells(1, 6)
       eRange.HorizontalAlignment = xlLeft
       eRange.EntireColumn.AutoFit
      
      'Incolla l'intestazione dei campi
      For i = 0 To rs.Fields.Count - 1
         Set eRange = o_Ex.Cells(r_Start, i + 1)
         
         o_Ex.Cells(r_Start, i + 1) = Trim(rs.Fields(i).Name)
         o_Ex.Cells(r_Start, i + 1).Interior.ColorIndex = 4
         o_Ex.Cells(r_Start, i + 1).Borders.ColorIndex = 1
         o_Ex.Cells(r_Start, i + 1).Font.Bold = True
         
         eRange.HorizontalAlignment = xlLeft
         eRange.EntireColumn.AutoFit
      Next i
      
      Screen.MousePointer = vbDefault
      
      While Not rs.EOF
         Screen.MousePointer = vbHourglass
         
         'Incolla i dati nel foglio excel
         For i = 0 To rs.Fields.Count - 1
            o_Ex.Cells(r, i + 1) = Trim(rs.Fields(i).Value)
            o_Ex.Cells(r, i + 1).Borders.ColorIndex = 1
            
            Set eRange = o_Ex.Cells(r, i + 1)
            
            eRange.HorizontalAlignment = xlLeft
            eRange.EntireColumn.AutoFit
         Next i
         
         rs.MoveNext
         r = r + 1
         Screen.MousePointer = vbDefault

      Wend
      rs.Close
   End If
   
   Screen.MousePointer = vbDefault
End Sub

Public Function Crea_Intestazione_HTML() As String
   Dim sHTML As String
   
   sHTML = vbNullString
   sHTML = sHTML & "<html><head><title>HTML i-4.Migration Prints Export</title>" & vbCrLf & vbCrLf
   sHTML = sHTML & "</head><body><script Language=Javascript>" & vbCrLf & vbCrLf
   sHTML = sHTML & "//<!--" & vbCrLf & vbCrLf
   sHTML = sHTML & "alert('HTML Export Success...'); " & vbCrLf & vbCrLf
   sHTML = sHTML & "//-->" & vbCrLf & vbCrLf
   sHTML = sHTML & "</script>" & vbCrLf & vbCrLf
   
   Crea_Intestazione_HTML = sHTML
End Function

Public Function Crea_Foglio_HTML(wNomeFoglio As String, wSQL As String, wDescrizioneQuery As String, wPageName As String) As String
   Dim rs As ADODB.Recordset
   Dim r_Start As Long
   Static cc As Long
   Dim i As Long
   Dim eRange As Range
   Dim r As Long
   Dim SQL As String
    
   Dim sPageKey As String
   Dim sPageName As String
   
   Dim sHTMLOutput As String
   
   Dim sLeftImage As String
   Dim sRightImage As String
   
   Dim iPageCounter As Integer
   Dim j As Long
   Dim wIntestazione() As String
   
   wIntestazione = getIntestazioneDaParametri()
   
   'Init del wRet della funzione a NULLSTRING
   Crea_Foglio_HTML = vbNullString
 
   'Parte dalla 6� riga
   r_Start = 6
   r = r_Start + 1
   
   'Imposta il titolo della pagina
   sPageKey = wNomeFoglio
   sPageName = wPageName
           
   'Carica i loghi
   sLeftImage = Replace(Carica_Logo_Da_DB("L"), " ", "%20")
   sRightImage = Replace(Carica_Logo_Da_DB("R"), " ", "%20")
        
   sHTMLOutput = vbNullString
   
   'Apre il recordset
   Set rs = m_DllFunctions.Open_Recordset(wSQL)
        
   If rs.RecordCount > 0 Then
      iPageCounter = 1
           
      rs.MoveFirst
           
      Screen.MousePointer = vbHourglass
        
      sHTMLOutput = sHTMLOutput & "<b>" & wDescrizioneQuery & "</b><br><br>" & vbCrLf & vbCrLf
            
      sHTMLOutput = sHTMLOutput & "<center><table><tr><td align=center><img src=" & sLeftImage & _
                                  "></td><td align=center>" & vbCrLf & vbCrLf

      sHTMLOutput = sHTMLOutput & "<b>" & Trim(wIntestazione(0)) & "<br>" & Trim(wIntestazione(1)) & "<br>" & Trim(wIntestazione(2)) & _
                                  "<br>" & Trim(wIntestazione(4)) & "<br>"
                
      If InStr(1, Trim(wIntestazione(5)), "http://") > 0 Then
         sHTMLOutput = sHTMLOutput & "<a href=" & Trim(wIntestazione(5)) & " target=""_blank"">" & Trim(wIntestazione(5)) & "</a>"
      ElseIf InStr(1, Trim(wIntestazione(5)), "www") > 0 Then
         sHTMLOutput = sHTMLOutput & "<a href=http://" & Trim(wIntestazione(5)) & " target=""_blank"">" & Trim(wIntestazione(5)) & "</a>"
      Else
         sHTMLOutput = sHTMLOutput & Trim(wIntestazione(5))
      End If
            
      sHTMLOutput = sHTMLOutput & "</b>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "</td><td align=center><img src=" & sRightImage & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "></td></tr></table>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "<br>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "<table border=1 bordercolor=black bordercolorlight=black bordercolordark=black><tr>" & vbCrLf & vbCrLf
        
      For i = 0 To rs.Fields.Count - 1
         DoEvents
         sHTMLOutput = sHTMLOutput & "<td bgcolor=yellow>" & vbCrLf & vbCrLf
         
         sHTMLOutput = sHTMLOutput & "<font face=arial color=black size=3><b>" & Trim(rs.Fields(i).Name) & _
                                     "</b></font>" & vbCrLf & vbCrLf
         
         sHTMLOutput = sHTMLOutput & "</td>" & vbCrLf & vbCrLf
      Next i
            
      sHTMLOutput = sHTMLOutput & "</tr>" & vbCrLf & vbCrLf
            
      Screen.MousePointer = vbDefault
    
      Do While Not rs.EOF
         DoEvents
         Screen.MousePointer = vbHourglass
   
         sHTMLOutput = sHTMLOutput & "<tr>" & vbCrLf & vbCrLf
                     
         For i = 0 To rs.Fields.Count - 1
             sHTMLOutput = sHTMLOutput & "<td>" & vbCrLf & vbCrLf
             sHTMLOutput = sHTMLOutput & Trim(rs.Fields(i).Value)
             sHTMLOutput = sHTMLOutput & "</td>" & vbCrLf & vbCrLf
         Next i
   
         rs.MoveNext
         r = r + 1
   
         Screen.MousePointer = vbDefault
   
         sHTMLOutput = sHTMLOutput & "</tr>" & vbCrLf & vbCrLf
      Loop
            
      rs.Close
   
      sHTMLOutput = sHTMLOutput & "</table>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "<br><hr>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "</center>" & vbCrLf & vbCrLf
   End If
        
   Screen.MousePointer = vbDefault
   
   Crea_Foglio_HTML = sHTMLOutput
End Function

Public Function Carica_Logo_Da_DB(Tipo As String) As String
  Dim r As ADODB.Recordset
  
  Select Case Tipo
    Case "R"
      Set r = m_DllFunctions.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'BSLOGOR'")
  
      If r.RecordCount > 0 Then
        r.MoveFirst
        
        Carica_Logo_Da_DB = r!ParameterValue
        
        r.Close
      Else
        Carica_Logo_Da_DB = ""
      End If
  
    Case "L"
      Set r = m_DllFunctions.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'BSLOGOL'")
  
      If r.RecordCount > 0 Then
        r.MoveFirst
        
        Carica_Logo_Da_DB = r!ParameterValue
        
        r.Close
      Else
        Carica_Logo_Da_DB = ""
      End If
  End Select
End Function

