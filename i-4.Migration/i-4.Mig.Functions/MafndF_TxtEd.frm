VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MafndF_TxtEd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Text Editor"
   ClientHeight    =   6960
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9060
   ClipControls    =   0   'False
   Icon            =   "MafndF_TxtEd.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6960
   ScaleWidth      =   9060
   Begin VB.CommandButton cmdCollapseCopy 
      Height          =   375
      Left            =   2835
      Picture         =   "MafndF_TxtEd.frx":2AFA
      Style           =   1  'Graphical
      TabIndex        =   19
      Tag             =   "fixed"
      ToolTipText     =   "Collapse All Expanded Copy"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   375
      Left            =   2040
      Picture         =   "MafndF_TxtEd.frx":3084
      Style           =   1  'Graphical
      TabIndex        =   17
      Tag             =   "fixed"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdUndo 
      Enabled         =   0   'False
      Height          =   375
      Left            =   3285
      Picture         =   "MafndF_TxtEd.frx":3A0E
      Style           =   1  'Graphical
      TabIndex        =   16
      Tag             =   "fixed"
      ToolTipText     =   "Undo"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdExpandCopy 
      Height          =   375
      Left            =   2430
      Picture         =   "MafndF_TxtEd.frx":3B58
      Style           =   1  'Graphical
      TabIndex        =   15
      Tag             =   "fixed"
      ToolTipText     =   "Expand Copy Level by Level"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdPrint 
      Height          =   375
      Left            =   4245
      Picture         =   "MafndF_TxtEd.frx":40E2
      Style           =   1  'Graphical
      TabIndex        =   14
      Tag             =   "fixed"
      ToolTipText     =   "Print"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdFind 
      Height          =   375
      Left            =   1665
      Picture         =   "MafndF_TxtEd.frx":422C
      Style           =   1  'Graphical
      TabIndex        =   13
      Tag             =   "fixed"
      ToolTipText     =   "Find"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdOpenCpy 
      Enabled         =   0   'False
      Height          =   375
      Left            =   1320
      Picture         =   "MafndF_TxtEd.frx":4376
      Style           =   1  'Graphical
      TabIndex        =   12
      Tag             =   "fixed"
      ToolTipText     =   "Open Cpy"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdFIndCpy 
      Height          =   375
      Left            =   45
      Picture         =   "MafndF_TxtEd.frx":6070
      Style           =   1  'Graphical
      TabIndex        =   11
      Tag             =   "fixed"
      ToolTipText     =   "Find CPY/MACRO"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton CmdSave 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   375
      Left            =   3765
      Picture         =   "MafndF_TxtEd.frx":65FA
      Style           =   1  'Graphical
      TabIndex        =   10
      Tag             =   "fixed"
      ToolTipText     =   "Save"
      Top             =   0
      Width           =   375
   End
   Begin VB.ComboBox cmbPgmCpy 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      ItemData        =   "MafndF_TxtEd.frx":6B84
      Left            =   360
      List            =   "MafndF_TxtEd.frx":6B86
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Tag             =   "fixed"
      ToolTipText     =   "Open current selected copy..."
      Top             =   0
      Width           =   975
   End
   Begin MSComctlLib.ListView lswAppo 
      Height          =   735
      Left            =   3360
      TabIndex        =   2
      Top             =   7920
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   1296
      View            =   2
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComDlg.CommonDialog CommonD 
      Left            =   870
      Top             =   8040
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      FontStrikeThru  =   -1  'True
      FontUnderLine   =   -1  'True
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   150
      Top             =   7920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   13
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":6B88
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":6C9A
            Key             =   "Cut"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":6DAC
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":6EBE
            Key             =   "Paste"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":6FD0
            Key             =   "Find"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":70E2
            Key             =   "Properties"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":71F4
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":7306
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":7462
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":75BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":8842
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":8DE6
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_TxtEd.frx":938A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTCopy 
      Height          =   705
      Left            =   1560
      TabIndex        =   1
      Top             =   7800
      Visible         =   0   'False
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1244
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   60000
      TextRTF         =   $"MafndF_TxtEd.frx":94E6
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ListView lswErrori 
      Height          =   1905
      Left            =   0
      TabIndex        =   3
      Top             =   5040
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   3360
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   16777152
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Code"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Line"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Description"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Relationed Object"
         Object.Width           =   2540
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTSource 
      Height          =   4395
      Left            =   0
      TabIndex        =   4
      Top             =   360
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   7752
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   60000
      TextRTF         =   $"MafndF_TxtEd.frx":9566
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   9060
      _ExtentX        =   15981
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      ToolTips        =   0   'False
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ExpandCPY"
            Object.ToolTipText     =   "Open current selected copy..."
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   1
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FINDCPY"
            Object.ToolTipText     =   "Find CPY/MACRO"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "OPENCPY"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Find"
            Object.ToolTipText     =   "Find Text"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Print"
            Object.ToolTipText     =   "Print Source"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Copybooks"
            Object.ToolTipText     =   "Expand Copy"
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.Label lblLivExp 
      Caption         =   "Expansion level"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Left            =   1260
      TabIndex        =   18
      Top             =   4770
      Width           =   2805
   End
   Begin VB.Label lblID 
      Appearance      =   0  'Flat
      Caption         =   "ID: "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   30
      TabIndex        =   8
      Tag             =   "FIXED"
      Top             =   4770
      Width           =   1215
   End
   Begin VB.Label lblCol 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   8280
      TabIndex        =   7
      Tag             =   "FIXED"
      Top             =   4770
      Width           =   855
   End
   Begin VB.Label lblRow 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   7560
      TabIndex        =   6
      Tag             =   "FIXED"
      Top             =   4770
      Width           =   735
   End
   Begin VB.Label lblLines 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   6720
      TabIndex        =   5
      Tag             =   "FIXED"
      Top             =   4770
      Width           =   855
   End
   Begin VB.Menu PopUp 
      Caption         =   "pop"
      Visible         =   0   'False
      Begin VB.Menu cerca_def 
         Caption         =   "Find field definition"
      End
      Begin VB.Menu ret 
         Caption         =   "Return"
      End
      Begin VB.Menu exp_cpy 
         Caption         =   "Expand copy"
         Enabled         =   0   'False
      End
      Begin VB.Menu col_cpy 
         Caption         =   "Collapse Copy"
         Enabled         =   0   'False
      End
   End
End
Attribute VB_Name = "MafndF_TxtEd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Compare Text
'variabile contenente le stringhe per le operazioni di copia incolla ecc...
Dim Buffer As String

'variabili per tenere traccia della posizione precedente di modifica
Dim PrecPos As Long
Dim NewPos As Long
Dim Paste As Boolean

Dim ItemErrClick As Long

Private FindMatch As Boolean
Private FindWhole As Boolean
Private idxPos As Long
Private parola As String
Private Linea As Integer

Public RtBefore As String
Public RtBeforeA As String

Private fOldHeight As Long

'silvia
Dim PercorsoFile As String

'silvia 14-1-2008
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd _
                         As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
' requested action. Same routine could
' be called from a toolbar event.
'
Const WM_CUT = &H300
Const WM_COPY = &H301
Const WM_PASTE = &H302
Const WM_CLEAR = &H303
Const WM_UNDO = &H304
'
Dim posDef As Long
Dim posIni As Long
Dim selTextd As String
Dim selText As String

Private Sub cerca_def_Click()
  Dim posProc_Job As Long
  
  posIni = RTSource.SelStart
  selTextd = RTSource.selText
  If tipoPgm = "CBL" Then
    posProc_Job = FindProcedure_Job(tipoPgm, RTSource)
  Else
    posProc_Job = posIni
  End If
  posDef = RTSource.Find(" " & Trim(Replace(selTextd, vbCrLf, "")), 1, posProc_Job)
  If posDef = -1 Then
    MsgBox "No more items found...", vbExclamation, m_fun.FnNomeProdotto
    RTSource.SelStart = posIni
    RTSource.SelLength = Len(selTextd)
  Else
    RTSource.SelStart = posDef + 1
    RTSource.SelLength = Len(selTextd)
    ret.Enabled = True
    cerca_def.Enabled = False
  End If
  RTSource.SetFocus
End Sub

Private Sub cmdCollapseCopy_Click()
  If Not copyExpanded Then Exit Sub
     
  RTSource.Text = ""
  RTSource.Text = RtBeforeA
  RTSource.SelStart = 1
  RTSource.SelLength = Len(RTSource.Text)
  RTSource.Find " ", 0, 100
  Carica_Lista_Errori RTSource, lswErrori, m_fun.FnIdOggetto
  
  idLevel = 0
  
  lblLivExp.Caption = "Expansion Level nr. " & idLevel
  
  Set collExpandedCopy = Nothing
  
  nrExpandedCopy = 0
  
  copyExpanded = False
  
  cmdExpandCopy.Enabled = True
  cmdCollapseCopy.Enabled = False
End Sub

Private Sub cmdExpandCopy_Click()
'  gloria il pulsante espande tutti i livelli di copy e non collassa
  copyExpanded = True
  If idLevel = 0 Then
    RtBeforeA = RTSource.Text
  End If
  
  'gloria 28/04/2009: la funzione deve essere richiamata passando il txtFind corretto a seconda del tipo oggetto chiamato
  Select Case tipoPgm
    Case "CBL"
      Expand_Cpy RTCopy, RTSource, " COPY "
      Expand_Cpy RTCopy, RTSource, "INCLUDE "
    Case "PLI"
      Expand_Cpy RTCopy, RTSource, "INCLUDE "
    Case "EZT"
      Expand_Cpy RTCopy, RTSource, "MACRO"
    Case "ASM"
'      DA IMPLEMENTARE X ASSEMBLER
  End Select

  lblLivExp.Caption = "Expansion Level nr. " & idLevel
  
  If nrExpandedCopy Then
    cmdCollapseCopy.Enabled = True
  Else
    cmdCollapseCopy.Enabled = False
  End If
End Sub

Private Sub cmdFind_Click()
  Find
End Sub

Private Sub Find()
  Dim idxPos As Long
  Dim oFindForm As MafndF_TxtFind
  
  Set oFindForm = New MafndF_TxtFind
  msSearchString = RTSource.selText
  oFindForm.Show vbModal
  
  idxPos = 1
  parola = msSearchString
  
  FindMatch = oFindForm.mbFindMatch
  FindWhole = oFindForm.mbFindWhole
      
  Set oFindForm = Nothing
  If Trim(parola) <> vbNullString Then
    If FindMatch Then
      If FindWhole Then
        'True/True
        idxPos = RTSource.Find(parola, idxPos, , rtfWholeWord And rtfMatchCase)
      Else
        'True/False
        idxPos = RTSource.Find(parola, idxPos, , rtfMatchCase)
      End If
    Else
      If FindWhole Then
        'False/True
        idxPos = RTSource.Find(parola, idxPos, , rtfWholeWord)
      Else
        'False/False
        idxPos = RTSource.Find(parola, idxPos)
      End If
    End If
    
    If idxPos = -1 Then
      MsgBox "Word not found...", vbExclamation, m_fun.FnNomeProdotto
    End If
     
    RTSource.SetFocus
  End If

End Sub

Private Sub cmdFIndCpy_Click()
  Dim idx As Long
  Static CpyPos As Long, MacroPos As Long
  Static Expand As Boolean

  'silvia 8-4-2008
  If Trim(cmbPgmCpy.Text) <> "" Then
    Trova_Copy RTSource, Trim(UCase(cmbPgmCpy.Text))
  Else
    If tipoPgm = "EZT" Then
      idx = RTSource.Find("%", MacroPos)
  
      If idx = -1 Then
        MsgBox "No More Items Found...", vbExclamation, m_fun.FnNomeProdotto
        MacroPos = 1
      Else
        MacroPos = idx + 1
        RTSource.SetFocus
      End If
    ElseIf tipoPgm = "PLI" Then
      'idx = RTSource.Find("%INCLUDE ", MacroPos)
      idx = RTSource.Find("INCLUDE ", MacroPos)
  
      If idx = -1 Then
        MsgBox "No More Items Found...", vbExclamation, m_fun.FnNomeProdotto
        MacroPos = 1
      Else
        MacroPos = idx + 1
        RTSource.SetFocus
      End If
    Else
      'Trova Le copy all'interno della RichtextBox
      idx = RTSource.Find(" COPY ", CpyPos)
      If idx = -1 Then
        idx = RTSource.Find("INCLUDE ", CpyPos)
        If idx = -1 Then
          MsgBox "No More Items Found...", vbExclamation, m_fun.FnNomeProdotto
          CpyPos = 1
        Else
          CpyPos = idx + 1
          RTSource.SetFocus
        End If
      Else
        CpyPos = idx + 1
        RTSource.SetFocus
      End If
    End If
  End If
End Sub

Private Sub cmdGoTo_Click()
  Dim i As Long
  Dim Ch As String
  Dim idx As Long
  Dim parola() As String
  Dim oGotoForm As MafndF_Goto

  Set oGotoForm = New MafndF_Goto
  oGotoForm.Show vbModal
  Linea = oGotoForm.msGotoLine
  Set oGotoForm = Nothing
  parola = Split(RTSource.Text, vbCrLf)
  If Linea > 0 And Linea < UBound(parola) + 1 Then
    idx = 0
    idx = RTSource.Find(parola(Linea - 1), idx)
    RTSource.SelStart = idx
    RTSource.SelLength = Len(parola(Linea - 1))
    RTSource.SelColor = vbBlue
    RTSource.SetFocus
  End If
End Sub

Private Sub cmdOpenCpy_Click()
  Dim oFormCopy As MafndF_ObjViewer
  Set oFormCopy = New MafndF_ObjViewer
  oFormCopy.loadForm gIdOggetto, ""
  collHandler.Add oFormCopy.hwnd
End Sub

Private Sub cmdPrint_Click()
  m_fun.Stampa_Contenuto_RichTextBox RTSource, CommonD
End Sub

Private Sub CmdSave_Click()
  Dim risp As Integer
  
  risp = MsgBox("Do you want to save file?", vbYesNo + vbQuestion + vbDefaultButton2, "Text Editor")
  If risp = vbYes Then
    RTSource.SaveFile (PercorsoFile), 1
    CmdSave.Enabled = False
  End If
End Sub

'Private Sub cmdSearch_Click()
'  idxPos = 1
'  parola = Trim(txtWord.Text)
'
'  If ChkMatch.Value = 1 Then
'    FindMatch = True
'  Else
'    FindMatch = False
'  End If
'
'  If ChkWhole.Value = 1 Then
'    FindWhole = True
'  Else
'    FindWhole = False
'  End If
'
'  If FindMatch Then
'    If FindWhole = True Then
'      'True/True
'      idxPos = RTSource.Find(parola, idxPos, , rtfWholeWord And rtfMatchCase)
'    Else
'      'True/False
'      idxPos = RTSource.Find(parola, idxPos, , rtfMatchCase)
'    End If
'  Else
'    If FindWhole Then
'      'False/True
'      idxPos = RTSource.Find(parola, idxPos, , rtfWholeWord)
'    Else
'      'False/False
'      idxPos = RTSource.Find(parola, idxPos)
'    End If
'  End If
'
'  If idxPos = -1 Then
'    MsgBox "Word not found...", vbExclamation, m_fun.FnNomeProdotto
'  End If
'
'  RTSource.SetFocus
'
'  FraFind.Visible = False
'End Sub

Private Sub cmbpgmcpy_Change()
  If cmbPgmCpy.Text <> "" Then
    posIni = Trova_Copy(RTSource, Trim(UCase(cmbPgmCpy.Text))) + 8
  End If
End Sub

Private Sub cmbpgmcpy_Click()
  If cmbPgmCpy.Text <> "" Then
    posIni = Trova_Copy(RTSource, Trim(UCase(cmbPgmCpy.Text))) + 8
  End If
End Sub

Private Sub cmdUndo_Click()
  EditPerform WM_UNDO
End Sub

' silvia 14-1-2008
Private Sub EditPerform(EditFunction As Integer)
  SendMessage RTSource.hwnd, EditFunction, 0, 0&
End Sub

Private Sub col_cpy_Click()
  RTSource.Text = ""
  RTSource.Text = RtBefore
  'RTSource.SelStart = 1
  RTSource.SelStart = posIni
  RTSource.SelLength = Len(selText)
  'RTSource.SelColor = vbgreen
  'RTSource.Find " ", 0, 100
  Carica_Lista_Errori RTSource, lswErrori, m_fun.FnIdOggetto
  col_cpy.Enabled = False
  copyExpanded = False
End Sub

Private Sub exp_cpy_Click()
  Dim MarcatoreCPY As String, CopyPath As String
  Dim Riga As Long, riganew  As Long, RigaIni As Long, i As Integer, j As Integer
  Dim RTApp As RichTextBox
  Dim RTFine As Long, LenCpy As Long
  Dim NomeCOPY As String
  Dim CopyFind As Boolean
  Dim cpy As Long

  RtBefore = RTSource.Text

  cpy = RTSource.Find(RTSource.selText, 1)
  'silvia 09-10-2008
  If cpy < posIni Then
    cpy = RTSource.Find(RTSource.selText, posIni)
  End If
  If cpy > -1 Then
    NomeCOPY = RTSource.selText
    'silvia: tolgo le parentesi
    NomeCOPY = Replace(NomeCOPY, "(", "")
    NomeCOPY = Replace(NomeCOPY, ")", "")
     
    'Toglie eventuali punti
    If mId(NomeCOPY, Len(NomeCOPY), 1) = "." Then
      NomeCOPY = mId(NomeCOPY, 1, Len(NomeCOPY) - 1)
    End If
    
    'Toglie eventuali apici
    If mId(NomeCOPY, Len(NomeCOPY), 1) = "'" Then
      NomeCOPY = mId(NomeCOPY, 1, Len(NomeCOPY) - 1)
    End If
    
    If mId(NomeCOPY, 1, 1) = "'" Then
      NomeCOPY = mId(NomeCOPY, 2)
    End If
    
    'Toglie eventuali punti e virgola: solo per PLI
    If mId(NomeCOPY, Len(NomeCOPY), 1) = ";" Then
      NomeCOPY = mId(NomeCOPY, 1, Len(NomeCOPY) - 1)
    End If
        
    Riga = RTSource.GetLineFromChar(cpy)
    MarcatoreCPY = "C" & String(5 - Len(Hex(Riga)), "0") & Hex(Riga)
    
    ' Mauro 31/10/2007: creo tutto il percorso nella routine, cos� mi evito 2 query diverse
    '''Prende il path della copy
    ''CopyPath = m_fun.Restituisci_Percorso_Da_NomeOggetto(NomeCOPY)
   
    'Prende l'estensione della copy
    CopyPath = Restituisci_File_Da_NomeOggetto(NomeCOPY)
   
    If Trim(CopyPath) <> "" Then
      'Espande la copy :
      'CARICA LA COPY NELLA TEXT DI APPOGGIO
      'CopyPath = CopyPath & "\" & NomeCOPY & IIf(Len(CopyEstensione), "." & CopyEstensione, "")
      RTCopy.LoadFile CopyPath
      
      LenCpy = Len(RTCopy.Text)
      
      'trova RTPosition
      For i = 1 To 100
        RTSource.SelStart = cpy + i
        RTSource.SelLength = 2
        
        If RTSource.selText = vbCrLf Then
          RTPosition = cpy + i + 2
          Exit For
        End If
      Next i
      

      'Copia la copy nella RichTextBox principale
      RTSource.SelStart = RTPosition
      RTSource.SelLength = 0
      
      RTSource.SelColor = vbRed
      'If Not isINCLUDE Then
        RTSource.selText = MarcatoreCPY & vbCrLf & RTCopy.Text & vbCrLf & "CPYFN" & vbCrLf
      'Else
      '  RTSource.SelText = MarcatoreCPY & vbCrLf & RTApp.Text & vbCrLf & "INCFN" & vbCrLf
      'End If
      
      RTFine = RTPosition + Len(RTCopy.Text) + 11
      'silvia: nuove posizioni con copy espanse
      For j = 1 To MafndF_TxtEd.lswErrori.ListItems.Count
        riganew = CLng(MafndF_TxtEd.lswErrori.ListItems(j).ListSubItems(1).Text)
        If riganew > CLng(Riga + 1) Then
          MafndF_TxtEd.lswErrori.ListItems(j).ListSubItems(1).key = "K" & (CLng(mId(MafndF_TxtEd.lswErrori.ListItems(j).ListSubItems(1).key, 2)) + Len(MarcatoreCPY & vbCrLf & RTCopy.Text & vbCrLf & "CPYFN" & vbCrLf)) ' + Len(RTCopy.Text)   + 11)
          MafndF_TxtEd.lswErrori.ListItems(j).ListSubItems(1).Text = RTSource.GetLineFromChar(CLng(mId(MafndF_TxtEd.lswErrori.ListItems(j).ListSubItems(1).key, 2))) + 1
        Else
          'MsgBox "end"
        End If
      Next j
      RTSource.SelStart = RTPosition
      RTSource.SelLength = 0
      RTSource.SelColor = vbRed
      CopyFind = True
      col_cpy.Enabled = True
      exp_cpy.Enabled = False
      'silvia
      copyExpanded = True

    End If
  End If
End Sub

Private Sub Form_Activate()
  If Not m_fun.FnMDIAppend Then
    Me.ClipControls = True
    Me.BorderStyle = 2
  End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'  If (vbKeyControl + vbKeyF) Then
'    Find
'  End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  'Rilascia dalla memoria e fa l'unload delle form correlate
  Dim i As Long
  Dim Frm As Form

  For Each Frm In Forms
    For i = 1 To collHandler.Count
      If Frm.hwnd = collHandler(i) Then
        Unload Frm
        Set Frm = Nothing
        Exit For
      End If
    Next i
  Next
  
  idLevel = 0
  lblLivExp.Caption = "Expansion Level nr. 0"
  Set collExpandedCopy = Nothing
  nrExpandedCopy = 0
  
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
   
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub lswErrori_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
 
  Order = lswErrori.SortOrder
  
  key = ColumnHeader.Index
    
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswErrori.SortOrder = Order
  lswErrori.SortKey = key - 1
  lswErrori.Sorted = True
End Sub

Private Sub lswErrori_DblClick()
  Dim Riga As Long
  Dim idx As Long
  Dim idxFine As Long
  Dim idxStart As Long
  Dim i As Long
  Dim Ch As String
  Dim ind As Long
  Dim NewPos As Long
  Dim NewRiga As Long
  
  On Error GoTo ErrorHandler
  If ItemErrClick > 0 Then
    If lswErrori.ListItems.Count > 0 Then
      If Trim(lswErrori.ListItems(ItemErrClick).ListSubItems(1).Text) <> "" Then
        Riga = lswErrori.ListItems(ItemErrClick).ListSubItems(1).Text
        idxFine = mId(lswErrori.ListItems(ItemErrClick).ListSubItems(1).key, 2)
        'If copyExpanded = True Then
        '  idxFine = idxFine + 79
        'End If
        If idxFine < 85 Then idxFine = 85
        'Torno indietro
      End If
        
      For i = idxFine - 2 To idxFine - 85 Step -1
        RTSource.SelStart = i
        RTSource.SelLength = 2
        Ch = RTSource.selText
        If Ch = vbCrLf Then
          idxStart = i
          Exit For
        End If
      Next i
      If idxStart = 0 Then idxStart = idxFine
      RTSource.SelStart = idxStart + 1
      RTSource.SelLength = idxFine - idxStart + 1
      
      If idx <> -1 Then
        RTSource.SetFocus
      End If
      'End If
    End If
  End If
  Exit Sub
ErrorHandler:
  'gestire
  MsgBox ERR.Description, vbExclamation, m_fun.FnNomeProdotto
End Sub

Private Sub lswErrori_ItemClick(ByVal Item As MSComctlLib.ListItem)
  If lswErrori.ListItems.Count > 0 Then
    ItemErrClick = Item.Index
    ''''''''''''lswErrori_DblClick  'NON FUNGE????
    ''''''''''''RTSource.Refresh
  End If
End Sub

Private Sub ret_Click()
  RTSource.SelStart = posIni
  RTSource.SelLength = Len(selTextd)
  ret.Enabled = False
  cerca_def.Enabled = True
End Sub

Private Sub RTSource_KeyDown(KeyCode As Integer, Shift As Integer)
  Dim wFound As Boolean
  idxPos = idxPos + Len(parola)
  wFound = False
  
  If idxPos > -1 Then
    wFound = True
  End If
  
  If KeyCode = vbKeyF3 Then
    If FindMatch Then
      If FindWhole Then
        'True/True
        idxPos = RTSource.Find(parola, idxPos, , rtfWholeWord And rtfMatchCase)
      Else
        'True/False
        idxPos = RTSource.Find(parola, idxPos, , rtfMatchCase)
      End If
    Else
      If FindWhole Then
        'False/True
        idxPos = RTSource.Find(parola, idxPos, , rtfWholeWord)
      Else
        'False/False
        idxPos = RTSource.Find(parola, idxPos)
      End If
    End If
  End If
  
  If idxPos = -1 Then
    If wFound Then
      MsgBox "Search terminated...", vbInformation, m_fun.FnNomeProdotto
    End If
    
    idxPos = 1
    RTSource.SelStart = 0
    RTSource.SelLength = 0
  End If
  
  'silvia 10-1-2008
  If KeyCode <> vbKeyF3 Then
    CmdSave.Enabled = True
    cmdUndo.Enabled = True
  End If
  If KeyCode = 27 Then
    Unload Me
  End If
End Sub

Private Sub RTSource_KeyPress(KeyAscii As Integer)
  'gloria
  If KeyAscii = 6 Then
   Find
  End If
   
 'silvia
  Me.lblLines.Caption = "Lines: " & CStr(GetLineCount(Me.RTSource))
  Me.lblRow.Caption = "Row: " & CStr(GetLineNum(Me.RTSource) + 1)
  Me.lblCol.Caption = "Col: " & CStr(GetColPos(Me.RTSource) + 1)
  'silvia
End Sub

Private Sub RTSource_KeyUp(KeyCode As Integer, Shift As Integer)
 'silvia
  Me.lblLines.Caption = "Lines: " & CStr(GetLineCount(Me.RTSource))
  Me.lblRow.Caption = "Row: " & CStr(GetLineNum(Me.RTSource) + 1)
  Me.lblCol.Caption = "Col: " & CStr(GetColPos(Me.RTSource) + 1)
  'silvia
End Sub

Private Sub RTSource_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  Dim strcopy As String
  If tipoPgm = "CBL" Then
    strcopy = " COPY "
  End If
  If tipoPgm = "PLI" Then
    strcopy = "INCLUDE"
  End If
  
  If Button = vbRightButton Then
    If RTSource.Find(strcopy, posIni - 9, posIni + 1) <> -1 Then
      RTSource.SelStart = posIni
      RTSource.SelLength = Len(selText)
      If Len(selText) Then
        exp_cpy.Enabled = True
        col_cpy.Enabled = False
      'End If
    '''If col_cpy.Enabled = False Then
      Else
        exp_cpy.Enabled = False
      End If
    Else
      exp_cpy.Enabled = False
    End If
    PopupMenu Me.PopUp
  Else
    cmdOpenCpy.Enabled = False
  End If
  If copyExpanded = True Then
    col_cpy.Enabled = True
  End If
End Sub

Private Sub RTSource_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  'silvia
  Me.lblLines.Caption = "Lines: " & CStr(GetLineCount(Me.RTSource))
  Me.lblRow.Caption = "Row: " & CStr(GetLineNum(Me.RTSource) + 1)
  Me.lblCol.Caption = "Col: " & CStr(GetColPos(Me.RTSource) + 1)
  'silvia
End Sub

Private Sub RTSource_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
  Dim i As Long
  Dim Start As Long
  Dim rs As Recordset
  Dim wFind As Boolean
  
  'Prende il testo selezionato e vede se � una copy
  'Se si la carica e la incolla nella RichTextBox
  
  selText = Trim(RTSource.selText)
  posIni = RTSource.SelStart
  
  If selText = "" Then Exit Sub
  
  If InStr(1, selText, "/") <> 0 Then Exit Sub
  If InStr(1, selText, "\") <> 0 Then Exit Sub
  If InStr(1, selText, "!") <> 0 Then Exit Sub
  If InStr(1, selText, "'") <> 0 Then Exit Sub
  If InStr(1, selText, "�") <> 0 Then Exit Sub
  If InStr(1, selText, "$") <> 0 Then Exit Sub
  If InStr(1, selText, "%") <> 0 Then Exit Sub
  If InStr(1, selText, "&") <> 0 Then Exit Sub
  If InStr(1, selText, "(") <> 0 Then Exit Sub
  If InStr(1, selText, ")") <> 0 Then Exit Sub
  If InStr(1, selText, "=") <> 0 Then Exit Sub
  If InStr(1, selText, "^") <> 0 Then Exit Sub
  If InStr(1, selText, "*") <> 0 Then Exit Sub

  If InStr(1, selText, "+") <> 0 Then Exit Sub
  If InStr(1, selText, "|") <> 0 Then Exit Sub
  If InStr(1, selText, ";") <> 0 Then Exit Sub
  If InStr(1, selText, ",") <> 0 Then Exit Sub
  If InStr(1, selText, ":") <> 0 Then Exit Sub

  If InStr(1, selText, "_") <> 0 Then Exit Sub
  If InStr(1, selText, "�") <> 0 Then Exit Sub
  If InStr(1, selText, "�") <> 0 Then Exit Sub
  If InStr(1, selText, "@") <> 0 Then Exit Sub
  If InStr(1, selText, "#") <> 0 Then Exit Sub
  If InStr(1, selText, "�") <> 0 Then Exit Sub
  If InStr(1, selText, "[") <> 0 Then Exit Sub
  If InStr(1, selText, "]") <> 0 Then Exit Sub
  If InStr(1, selText, "<") <> 0 Then Exit Sub
  If InStr(1, selText, ">") <> 0 Then Exit Sub
  
  wFind = False
  
  Set rs = m_fun.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & Trim(selText) & "' And Tipo in ('CPY', 'INC')")
  
  If rs.RecordCount Then
    wFind = True
  Else
    rs.Close
    
    Set rs = m_fun.Open_Recordset("Select * From PsData_Cmp Where Nome = '" & Trim(selText) & "'")
    If rs.RecordCount Then
      wFind = True
    End If
  End If
  
  If wFind Then
    Toolbar1.Buttons(3).Enabled = True
    cmdOpenCpy.Enabled = True
    cmdExpandCopy.Enabled = True
    cmdCollapseCopy.Enabled = True
    'Recupera l'id oggetto
    gIdOggetto = rs!IdOggetto
  Else
    Toolbar1.Buttons(3).Enabled = False
    cmdOpenCpy.Enabled = False
    'cmdExpandCopy.Enabled = False
  End If
  
  rs.Close
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim Scelta As Long
  Dim Percorso As String
  Dim IdOgg As Long
  Dim vScelta As Long
  Dim oFormCopy As MafndF_ObjViewer
  Dim wcopy As String
  Dim i As Long
  Dim idx As Long
  Dim oFindForm As MafndF_TxtFind
  Static CpyPos As Long, MacroPos As Long
  Static Expand As Boolean
  
  On Error Resume Next
  
  If CpyPos = 0 Then CpyPos = 1
  If MacroPos = 0 Then MacroPos = 1
  
  Select Case Button.key
    Case "Copybooks"
      If Not copyExpanded Then
        copyExpanded = True
        RtBefore = RTSource.Text
        Expand_Cpy RTCopy, RTSource, " COPY "
        Expand_Cpy RTCopy, RTSource, "INCLUDE"
      Else
        copyExpanded = False
        RTSource.Text = ""
        RTSource.Text = RtBefore
        RTSource.SelStart = 1
        RTSource.SelLength = Len(RTSource.Text)
        'RTSource.SelColor = vbgreen
        RTSource.Find " ", 0, 100
        Carica_Lista_Errori RTSource, lswErrori, m_fun.FnIdOggetto
      End If
      
    Case "Find"
'      FraFind.Visible = True
'      txtWord.SetFocus
    
      Set oFindForm = New MafndF_TxtFind
      oFindForm.Show vbModal
      
      idxPos = 1
      parola = msSearchString
      
      FindMatch = oFindForm.mbFindMatch
      FindWhole = oFindForm.mbFindWhole
          
      Set oFindForm = Nothing
      If Trim(parola) <> vbNullString Then
        If FindMatch Then
          If FindWhole Then
            'True/True
            idxPos = RTSource.Find(parola, idxPos, , rtfWholeWord And rtfMatchCase)
          Else
            'True/False
            idxPos = RTSource.Find(parola, idxPos, , rtfMatchCase)
          End If
        Else
          If FindWhole Then
            'False/True
            idxPos = RTSource.Find(parola, idxPos, , rtfWholeWord)
          Else
            'False/False
            idxPos = RTSource.Find(parola, idxPos)
          End If
        End If
        
        If idxPos = -1 Then
          MsgBox "Word not found...", vbExclamation, m_fun.FnNomeProdotto
        End If
         
        RTSource.SetFocus
      End If

    Case "FINDCPY"
      ' Mauro 31/10/2007 : Gestione tipoPGM
      'If (MafndM_TxtEd.eztMacro) Then
      If tipoPgm = "EZT" Then
        idx = RTSource.Find("%", MacroPos)
           
        If idx = -1 Then
          MsgBox "No More Items Found...", vbExclamation, m_fun.FnNomeProdotto
          MacroPos = 1
        Else
          MacroPos = idx + 1
          RTSource.SetFocus
        End If
      ElseIf tipoPgm = "PLI" Then
        'idx = RTSource.Find("%INCLUDE ", MacroPos)
        idx = RTSource.Find(" INCLUDE ", MacroPos)
           
        If idx = -1 Then
          MsgBox "No More Items Found...", vbExclamation, m_fun.FnNomeProdotto
          MacroPos = 1
        Else
          MacroPos = idx + 1
          RTSource.SetFocus
        End If
      Else
        'Trova Le copy all'interno della RichtextBox
        idx = RTSource.Find(" COPY ", CpyPos)
        If idx = -1 Then
          idx = RTSource.Find(" INCLUDE ", CpyPos)
          If idx = -1 Then
            MsgBox "No More Items Found...", vbExclamation, m_fun.FnNomeProdotto
            CpyPos = 1
          Else
            CpyPos = idx + 1
            RTSource.SetFocus
          End If
        Else
          CpyPos = idx + 1
          RTSource.SetFocus
        End If
      End If
   
    Case "OPENCPY"
      Set oFormCopy = New MafndF_ObjViewer
      
      'MF 31/07/07
      oFormCopy.loadForm gIdOggetto, ""
      collHandler.Add oFormCopy.hwnd
       
    Case "Print"
      m_fun.Stampa_Contenuto_RichTextBox RTSource, CommonD
      
  End Select
End Sub

Public Sub Resize()
  On Error Resume Next
  
  If m_fun.FnMDIAppend Then
    m_fun.ResizeControls Me
  'Else
  End If
End Sub

Private Sub Form_Load()
  Dim Directory As String
  Dim Nome As String
  Dim Riga As String
  
  Dim OldHeight As Long
  
  On Error GoTo errFile
  
  'silvia 7-1-2008
  PsHideIgnore = LeggiParam("PS_HIDE_IGNORE") = "Yes"

  copyExpanded = False
  
  'Setta il parent
  If m_fun.FnMDIAppend Then
    SetParent Me.hwnd, m_fun.FnParent
  End If
  
  'If maf.FnIdOggetto <> 0 Then
  If m_fun.FnIdOggetto <> 0 Then
    'carica i parametri
    Carica_Parametri_TextEditor RTSource
   
    'carica il sorgente
    Directory = m_fun.Restituisci_Directory_Da_IdOggetto(m_fun.FnIdOggetto, "INPUT")
    Nome = m_fun.Restituisci_NomeOgg_Da_IdOggetto(m_fun.FnIdOggetto)
    
    PercorsoFile = Directory & "\" & Nome
    
    RTSource.LoadFile Directory & "\" & Nome
  End If
  
  'resize della listview
  lswErrori.ColumnHeaders(3).Width = lswErrori.Width - lswErrori.ColumnHeaders(2).Width - lswErrori.ColumnHeaders(1).Width - 270
  
  Carica_Lista_Errori RTSource, lswErrori, m_fun.FnIdOggetto
  
  Me.Caption = "Object: " & Directory & "\" & Nome
  lblID.Caption = "ID: " & m_fun.FnIdOggetto
  cmbPgmCpy.Clear
  'Carica la lista delle Copy
  Carica_Lista_Copy m_fun.FnIdOggetto, Me
  Dim i
  If cmbPgmCpy.ListCount > 0 Then
   'SILVIA 15-04-2008
   cmbPgmCpy.AddItem ""
   cmdExpandCopy.Enabled = True
  Else
  ' cmdExpandCopy.Enabled = False
  End If
  Toolbar1.Buttons(7).Enabled = True
  
  RTSource.SelStart = 0
  RTSource.SelLength = 0
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
  
' gloria
  cmdCollapseCopy.Enabled = False
  lblLivExp.Caption = "Expansion Level nr. 0"

  Exit Sub
errFile:
  If ERR.Number = 76 Then
    MsgBox ERR.Description, vbCritical, , m_fun.FnNomeProdotto
    Unload Me
  ElseIf ERR.Number = 75 Then
    MsgBox "File " & PercorsoFile & " not found.", vbCritical, m_fun.FnNomeProdotto
    Resume Next
  Else
    'gestire
    MsgBox ERR.Description, vbExclamation, , m_fun.FnNomeProdotto
  End If
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  'Si posiziona sulla COPY espansa
  Trova_Copy RTSource, Trim(UCase(ButtonMenu.Text))
End Sub
