VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form MafndF_CopyLoad 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selected Copy source code"
   ClientHeight    =   6570
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9420
   Icon            =   "MafndF_CopyLoad.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6570
   ScaleWidth      =   9420
   StartUpPosition =   2  'CenterScreen
   Begin RichTextLib.RichTextBox RTCopyLoad 
      Height          =   6555
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9405
      _ExtentX        =   16589
      _ExtentY        =   11562
      _Version        =   393217
      BackColor       =   16777152
      Enabled         =   -1  'True
      ScrollBars      =   3
      TextRTF         =   $"MafndF_CopyLoad.frx":0442
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "MafndF_CopyLoad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
   Dim wCopy As String
   
   wCopy = m_DllFunctions.Restituisci_NomeOgg_Da_IdOggetto(gIdOggetto)
   
   Me.Caption = "Selected Copy source code - " & wCopy
   
   RTCopyLoad.SelStart = 0
   RTCopyLoad.SelLength = 1
   RTCopyLoad.SelColor = vbBlue
   RTCopyLoad.SelLength = 0
   
   RTCopyLoad.LoadFile m_DllFunctions.Restituisci_Directory_Da_IdOggetto(gIdOggetto) & "\" & wCopy
End Sub

Private Sub Form_Resize()
   RTCopyLoad.Width = Me.ScaleWidth
   RTCopyLoad.Height = Me.ScaleHeight
   RTCopyLoad.Top = Me.ScaleTop
   RTCopyLoad.Left = Me.ScaleLeft
End Sub
