VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MacroRelCallerObject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Option Explicit

Public key As String

Private Type ObjectCallerType
    PsRel_ObjCaller_ID As Long
    PsRel_ObjCaller_ParentID As Long
    PsRel_ObjCaller_Name As String
    PsRel_ObjCaller_RelType As String
    PsRel_ObjCaller_RelDetail As String
End Type

Private vObject As ObjectCallerType

' PsRel_Obj Table
Public Property Let PsRel_ObjCaller_ID(ByVal vData As Variant)
    vObject.PsRel_ObjCaller_ID = vData
End Property

Public Property Get PsRel_ObjCaller_ID() As Variant
   PsRel_ObjCaller_ID = vObject.PsRel_ObjCaller_ID
End Property

Public Property Let PsRel_ObjCaller_ParentID(ByVal vData As Variant)
    vObject.PsRel_ObjCaller_ParentID = vData
End Property

Public Property Get PsRel_ObjCaller_ParentID() As Variant
   PsRel_ObjCaller_ParentID = vObject.PsRel_ObjCaller_ParentID
End Property

Public Property Let PsRel_ObjCaller_Name(ByVal vData As Variant)
    vObject.PsRel_ObjCaller_Name = vData
End Property

Public Property Get PsRel_ObjCaller_Name() As Variant
   PsRel_ObjCaller_Name = vObject.PsRel_ObjCaller_Name
End Property

Public Property Let PsRel_ObjCaller_RelType(ByVal vData As Variant)
    vObject.PsRel_ObjCaller_RelType = vData
End Property

Public Property Get PsRel_ObjCaller_RelType() As Variant
   PsRel_ObjCaller_RelType = vObject.PsRel_ObjCaller_RelType
End Property

Public Property Let PsRel_ObjCaller_RelDetail(ByVal vData As Variant)
    vObject.PsRel_ObjCaller_RelDetail = vData
End Property

Public Property Get PsRel_ObjCaller_RelDetail() As Variant
   PsRel_ObjCaller_RelDetail = vObject.PsRel_ObjCaller_RelDetail
End Property
