VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPgm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_Id As Long
Private m_Nome As String
Private m_Attributo As e_TipoObj
Private m_NumRighe As Long
Private m_Tipo As String
Private m_Relazione As String
Private m_Caller As New Collection
Private m_Called As New Collection
Private m_Copy As New Collection
Private m_Psb As New Collection
Private m_Maps As New Collection

'Da implementare
Private m_DB As New Collection
Private m_VSam As New Collection

Public Enum e_TipoObj
   Cics = 0 ^ 2
   Batch = 1 ^ 2
   SQL = 2 ^ 2
   VSam = 3 ^ 2
   DLI = 4 ^ 2
   Mapping = 5 ^ 2
   Filesection = 6 ^ 2
   Working = 7 ^ 2
   Procedure = 8 ^ 2
   Undefined = 9 ^ 2
End Enum

'ProprietÓ base
'Configurazione property Set/Let
Public Property Let Id(ByVal Id As Long)
   m_Id = Id
End Property

Public Property Get Id() As Long
   Id = m_Id
End Property

Public Property Let Nome(ByVal Nome As String)
   m_Nome = Nome
End Property

Public Property Get Nome() As String
   Nome = m_Nome
End Property

Public Property Let nRighe(ByVal NumRighe As Long)
   m_NumRighe = NumRighe
End Property

Public Property Get nRighe() As Long
   nRighe = m_NumRighe
End Property

Public Property Let Tipo(ByVal Tipo As String)
   m_Tipo = Tipo
End Property

Public Property Get Tipo() As String
   Tipo = m_Tipo
End Property

Public Property Let Relazione(ByVal Relazione As String)
   m_Relazione = Relazione
End Property

Public Property Get Relazione() As String
    Relazione = m_Relazione
End Property


Public Property Let Attributo(ByVal Attrib As e_TipoObj)
   m_Attributo = Attrib
End Property

Public Property Get Attributo() As e_TipoObj
   Attributo = m_Attributo
End Property

'Pgm Caller
Public Sub AddCaller(PgmCaller As clsPgm)
   m_Caller.Add PgmCaller
End Sub

Public Sub RemoveCaller(Index As Long)
   m_Caller.Remove (Index)
End Sub

Public Function GetCaller(Index As Long) As clsPgm
   Set GetCaller = m_Caller.Item(Index)
End Function

Public Function CountCaller() As Long
   CountCaller = m_Caller.Count
End Function

'Copy
Public Sub AddCopy(cCopy As clsCopy)
   m_Copy.Add cCopy
End Sub

Public Sub RemoveCopy(Index As Long)
   m_Copy.Remove (Index)
End Sub

Public Function GetCopy(Index As Long) As clsCopy
   Set GetCopy = m_Copy.Item(Index)
End Function

Public Function CountCopy() As Long
   CountCopy = m_Copy.Count
End Function

'Psb
Public Sub AddPsb(cPsb As clsPsb)
   m_Psb.Add cPsb
End Sub

Public Sub RemovePsb(Index As Long)
   m_Psb.Remove (Index)
End Sub

Public Function GetPsb(Index As Long) As clsPsb
   Set GetPsb = m_Psb.Item(Index)
End Function

Public Function CountPsb() As Long
   CountPsb = m_Psb.Count
End Function

'Maps
Public Sub AddMaps(cMaps As clsMappe)
   m_Maps.Add cMaps
End Sub

Public Sub RemoveMaps(Index As Long)
   m_Maps.Remove (Index)
End Sub

Public Function GetMaps(Index As Long) As clsMappe
   Set GetMaps = m_Maps.Item(Index)
End Function

Public Function CountMaps() As Long
   CountMaps = m_Maps.Count
End Function

'Pgm Called
Public Sub AddCalled(PgmCalled As clsPgm)
   m_Called.Add PgmCalled
End Sub

Public Sub RemoveCalled(Index As Long)
   m_Called.Remove (Index)
End Sub

Public Function GetCalled(Index As Long) As clsPgm
   Set GetCalled = m_Called.Item(Index)
End Function

Public Function CountCalled() As Long
   CountCalled = m_Called.Count
End Function
