VERSION 5.00
Begin VB.Form MafndF_Goto 
   Caption         =   "Goto..."
   ClientHeight    =   705
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   2775
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   705
   ScaleWidth      =   2775
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtGoto 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   600
      TabIndex        =   0
      Text            =   "0"
      Top             =   120
      Width           =   915
   End
   Begin VB.CommandButton CmdOk 
      Caption         =   "Ok"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1800
      Picture         =   "MafndF_Goto.frx":0000
      TabIndex        =   1
      Top             =   120
      Width           =   825
   End
   Begin VB.Label Label4 
      Caption         =   "Line:"
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   240
      Width           =   735
   End
End
Attribute VB_Name = "MafndF_Goto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public msGotoLine As Integer

Private Sub cmdCancel_Click()
  txtGoto.Text = ""
  Unload Me
End Sub

Private Sub CmdOk_Click()
  If Not IsNumeric(Trim(txtGoto.Text)) Then
    MsgBox "Insert numeric value", , "Goto line.."
    txtGoto.SetFocus
  Else
    msGotoLine = Int(Trim(txtGoto.Text))
  End If
  Unload Me
End Sub
