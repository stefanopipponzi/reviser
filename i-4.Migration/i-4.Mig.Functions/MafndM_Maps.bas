Attribute VB_Name = "MafndM_Maps"
Option Explicit

Global XCellMap As Long
Global YCellMap As Long

'Parametri della Mappa
Type Cmp
  NomeCmp As String
  PosX As Long
  PosY As Long
  Length As Long
  AttRB As String
  INITIAL As String
  Justify As String
  Color As String
  Hilight As String
  PicIN As String
  PicOUT As String
End Type

Type Map
  Nome As String
  TioApFx As String
  SizeX As Long
  SizeY As Long
  Line As String
  Column As String
  Data As String
  Justify As String
  OBFMT As String
  MapAtts As String
  DSatts As String
  Trailer As String
  Header As String
  Campi() As Cmp
End Type

Type MapSet
  MapSet As String
  Tipo As String
  Term As String
  Lang As String
  CtRl As String
  Data As String
  TioApFx As String
  Mode As String
  Storage As String
  Suffix As String
  Maps() As Map
End Type

'MFS
Type t_Dev
   Tipo As String
   Feat As String
   SysMSG As String
   PFK As New Collection
End Type

Type t_Cursor
   x As Long
   y As Long
End Type

Type t_Page
   Fill As String
   Cursor As t_Cursor
End Type

Type t_Div
   Tipo As String
End Type

Type t_Campi
   Nome As String
   INITIAL As String
   POS As t_Cursor
   Attr As New Collection
   len As Long
   Occurs As Integer
   Funzione As String
End Type

Type t_Mfs
   NomeMappa As String
   Dev As t_Dev
   Div As t_Div
   Campi() As t_Campi
End Type

Public BMS As MapSet

Public MFS As t_Mfs

'Indica il limite massimo per scrivere il codice es 1 72 per i CBL
Public Const MaxEdit As Long = 71

Public HVisible As Single
Public VVisible As Single

Public NewC As Boolean

Public Const ColDefault As Variant = &HFF00& 'Verde

Public Sub Resize()
  DoEvents
End Sub

Public Function Vai_Inizio_Riga(RT As RichTextBox, POS As Long) As Long
  Dim i As Long
  
  For i = 1 To 100
    If POS - i = 0 Then
      Vai_Inizio_Riga = 0
      Exit Function
    End If
    
    RT.SelStart = POS - i
    RT.SelLength = 2
    
    If RT.selText = vbCrLf Then
      Vai_Inizio_Riga = POS - i + 1
      Exit For
    End If
  Next i
End Function

Public Function Vai_Fine_Riga(RT As RichTextBox, POS As Long) As Long
  Dim i As Long
  
  For i = 1 To 100
    RT.SelStart = POS + i
    RT.SelLength = 2
    
    If RT.selText = vbCrLf Then
      Vai_Fine_Riga = POS + i - 1
      Exit For
    End If
  Next i
End Function

Public Sub Migrate_Structure_MFS2BMS()
   Dim i As Long
   
   ReDim BMS.Maps(0)
   ReDim BMS.Maps(1)
   
   BMS.Maps(1).Nome = MFS.NomeMappa
   
   Select Case MFS.Dev.Tipo
      Case "3270"
         BMS.Maps(1).SizeX = 80
         BMS.Maps(1).SizeY = 24
         
      Case Else
         '80X24 di default
         BMS.Maps(1).SizeX = 80
         BMS.Maps(1).SizeY = 24
         
   End Select
   
   ReDim Preserve BMS.Maps(1).Campi(0)
   
   For i = 1 To UBound(MFS.Campi)
      ReDim Preserve BMS.Maps(1).Campi(UBound(BMS.Maps(1).Campi) + 1)
         
      BMS.Maps(1).Campi(UBound(BMS.Maps(1).Campi)).PosX = MFS.Campi(i).POS.x
      BMS.Maps(1).Campi(UBound(BMS.Maps(1).Campi)).PosY = MFS.Campi(i).POS.y
      
      BMS.Maps(1).Campi(UBound(BMS.Maps(1).Campi)).INITIAL = MFS.Campi(i).INITIAL
      BMS.Maps(1).Campi(UBound(BMS.Maps(1).Campi)).Length = MFS.Campi(i).len
      BMS.Maps(1).Campi(UBound(BMS.Maps(1).Campi)).NomeCmp = MFS.Campi(i).Nome
      
      'Attributi : Da fare
   Next i
   
End Sub

Public Function Normalizza_Riga(Riga As String) As String
  'Normalizza la stringa mettendo un solo spazio fra le parole (tranne che per l'initial)
  'e toglie l'asterisco
  Dim i As Long
  Dim Count As Long
  
  Dim parola As String
    
  Dim App As String
  Dim Ch As String
  Dim Spazio As Boolean
  Dim AppRiga As String
  Dim Stringa As String
  
  Dim Appoggio As String
  
  Stringa = ""
  Count = 1
  
  'Legge le righe
  For i = 1 To Len(Riga)
    App = mId(Riga, i, 2)
    Ch = mId(Riga, i, 1)
    
    If App = vbCrLf Then
      If InStr(1, Stringa, " DFHMSD ") <> 0 Or _
         InStr(1, Stringa, " DFHMDI ") <> 0 Or _
         InStr(1, Stringa, " DFHMDF ") <> 0 Then
            
         Appoggio = Appoggio & mId(Stringa, 1, 72)
      Else
        Appoggio = Appoggio & mId(Stringa, 16, 56)
      End If
      
      Stringa = ""
      Count = 0
    Else
      If Ch <> Chr(10) And Ch <> Chr(13) Then
        Stringa = Stringa & Ch
      End If
    End If
    
    Count = Count + 1
  Next i
  
  'Toglie gli accapo (vbcrlf)
  For i = 1 To Len(Appoggio)
    App = mId(Appoggio, i, 2)
    Ch = mId(Appoggio, i, 1)
    
    If App <> vbCrLf And (Trim(Ch) <> Chr(10) And Trim(Ch) <> Chr(13)) Then
      Stringa = Stringa & Ch
      
    End If
    DoEvents
  Next i
  
  AppRiga = Stringa
  
  Stringa = ""
  
  Dim Asterix As Boolean
  
  'Toglie gli asterischi
  For i = 1 To Len(AppRiga)
    If mId(AppRiga, i, 1) <> "*" Then
      Stringa = Stringa & mId(AppRiga, i, 1)
    End If
    DoEvents
  Next i
  
  Stringa = Trim(Stringa) & ","
  
  Normalizza_Riga = Stringa
  
End Function

Public Function Get_Parametro(Riga As String, Parametro As String, ChStop As String) As String
  Dim idx As Long
  
  Dim Divs As Long
  
  Dim Stops As Long
  
  Dim Valore As String
  
  idx = InStr(1, Riga, Parametro)
  
  If idx <> 0 Then
    Divs = InStr(idx, Riga, "=")
    
    If Divs <> 0 Then
      If Trim(UCase(Parametro)) = "INITIAL" Then
        Stops = InStr(Divs, Riga, ChStop)
        Stops = InStr(Stops + 1, Riga, ChStop)
      Else
        Stops = InStr(Divs, Riga, ChStop)
      End If
 
      If Stops <> 0 Then
        Valore = mId(Riga, Divs + 1, Stops - Divs - 1)
      Else
        Select Case Trim(UCase(Parametro))
          Case "INITIAL"
            'Cerca l'apice
            Stops = InStr(Divs, Riga, "'") + 1
          Case Else
            'Si ferma al primo spazio (tronca)
            Stops = InStr(Divs, Riga, " ")
        
        End Select
      
        If Stops <> 0 Then
          Valore = mId(Riga, Divs + 1, Stops - Divs - 1)
        Else
          'Mette il dato in una lista di errori : DA FARE
        End If
      End If
    End If
  End If

  Get_Parametro = Valore
End Function


Public Sub Analizza_DFHMSD(RT As RichTextBox)
  Dim idx As Long
  Dim IdxMdi As Long
  
  Dim StartPos As Long
  Dim StopPos As Long
  
  Dim i As Long
  
  'Parametri DFHMSD
  Dim MapSet As String
  Dim Tipo As String
  Dim Term As String
  Dim Lang As String
  Dim CtRl As String
  Dim Data As String
  Dim TioApFx As String
  Dim Mode As String
  
  Dim Storage As String
  Dim Suffix As String
  
  Dim Riga As String
  
  idx = RT.Find(" DFHMSD ", 1)
  IdxMdi = RT.Find(" DFHMDI ", idx)
  
  If idx <> -1 Then
    StartPos = Vai_Inizio_Riga(RT, idx)
    StopPos = Vai_Inizio_Riga(RT, IdxMdi)
    
    RT.SelStart = StartPos
    RT.SelLength = StopPos - StartPos
    
    Riga = RT.selText
    
    Riga = Normalizza_Riga(Riga)
    
    'Prende tutti i parametri
    MapSet = mId(Riga, 1, InStr(1, Riga, " ") - 1)
    Tipo = Get_Parametro(Riga, "TYPE", ",")
    Term = Get_Parametro(Riga, "TERM", ",")
    Lang = Get_Parametro(Riga, "LANG", ",")
    CtRl = Get_Parametro(Riga, "CTRL", ",")
    Data = Get_Parametro(Riga, "DATA", ",")
    TioApFx = Get_Parametro(Riga, "TIOAPFX", ",")
    Mode = Get_Parametro(Riga, "MODE", ",")
    Storage = Get_Parametro(Riga, "STORAGE", ",")
    Suffix = Get_Parametro(Riga, "SUFFIX", ",")
    
    'Memorizza nell'array
    BMS.MapSet = MapSet
    BMS.Tipo = Tipo
    BMS.Term = Term
    BMS.Lang = Lang
    BMS.CtRl = CtRl
    BMS.Data = Data
    BMS.TioApFx = TioApFx
    BMS.Mode = Mode
    BMS.Storage = Storage
    BMS.Suffix = Suffix
  End If
End Sub

Public Sub Analizza_DFHMDF(RT As RichTextBox, IdxArray As Long, StPos As Long, StStop As Long)
  Dim Idx1 As Long
  Dim Idx2 As Long
  Dim StartPos As Long
  Dim StopPos As Long
  
  Dim lng As String
  
  Dim old As Long
  
  Dim Riga As String
  
  'Variabili
  Dim NomeCmp As String
  Dim POS As String
  Dim PosX As Long
  Dim PosY As Long
  Dim Length As Long
  Dim Attr As String
  Dim INITIAL As String
  Dim Justify As String
  Dim Color As String
  Dim Hilight As String
  Dim PicIN As String
  Dim PicOUT As String
  
  Dim Fine As Boolean
  
  Dim j As Long
  
  j = 1
  
  Idx1 = RT.Find(" DFHMDF ", StPos, StStop)
  While Idx1 <> -1
    Screen.MousePointer = vbHourglass
    
    Idx2 = RT.Find(" DFHMDF ", Idx1 + 8, StStop)
    If Idx2 <> -1 Then
      StartPos = Vai_Inizio_Riga(RT, Idx1)
      StopPos = Vai_Inizio_Riga(RT, Idx2)
      
      RT.SelStart = StartPos
      RT.SelLength = StopPos - StartPos
      
      Riga = RT.selText
      
      Riga = Normalizza_Riga(Riga)
    Else
      'Cerca il DFHMSD di chiusura
      Idx2 = RT.Find(" DFHMSD ", Idx1 + 8)
      
      If Idx2 <> -1 Then
        StartPos = Vai_Inizio_Riga(RT, Idx1)
        StopPos = Vai_Inizio_Riga(RT, Idx2)
        
        RT.SelStart = StartPos
        RT.SelLength = StopPos - StartPos
        
        Riga = RT.selText
        
        Riga = Normalizza_Riga(Riga)
        
        Fine = True
      End If
    End If
    
    'Debug.Print Riga
    'WaitTime 1
    
    'Carica i parametri
    If Trim(mId(Riga, 1, InStr(1, Riga, " ") - 1)) <> "DFHMDF" Then
      NomeCmp = Trim(mId(Riga, 1, InStr(1, Riga, " ") - 1))
    Else
      NomeCmp = ""
    End If
    'SQ - perche' diverso dal parsing???????????????????????
    'POS = Get_Parametro(Riga, "POS", ")")
    'If mId(POS, 1, 1) = "(" Then POS = mId(POS, 2)
    'PosY = Val(mId(POS, 1, InStr(1, POS, ",") - 1))
    'PosX = Val(mId(POS, InStr(1, POS, ",") + 1))
    ''''''''''''''''''''''''''''''''''''''''''''
    Dim posYX() As String
    posYX() = Split(TrovaParametro(Riga, "POS"), ",")
    If Not (UCase(posYX(0)) = "NONE") Then
      'SQ: 12-12-05
      'POSIZIONAMENTO MONODIMENSIONALE... ES: pos=0, pos=1580...
      'non so se possibile solo con DFHMSD TYPE=DSECT,LANG=ASM...
      If UBound(posYX) = 0 Then
        'POSIZIONAMENTO MONODIMENSIONALE:
        '0 -> (01,01) ; N == (R-1)*80 + (C-1)
        PosY = (posYX(0) \ 80) + 1
        PosX = (posYX(0) Mod 80) + 1
      Else
        'POSIZIONAMENTO CLASSICO: BIDIMENSIONALE...
        PosY = mId(posYX(0), 2)
        PosX = mId(posYX(1), 1, Len(posYX(1)) - 1)
      End If
    Else
       PosY = 0
       PosX = 0
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Attr = Get_Parametro(Riga, "ATTRB", ",")
    If mId(Attr, 1, 1) = "(" Then
      Attr = Get_Parametro(Riga, "ATTRB", ")")
      
      If mId(Attr, 1, 1) = "(" Then Attr = mId(Attr, 2)
    End If
    
    Justify = Get_Parametro(Riga, "JUSTIFY", ")")
    
    If mId(Justify, 1, 1) = "(" Then Justify = mId(Justify, 2)
    
    INITIAL = Get_Parametro(Riga, "INITIAL", "'")
    If INITIAL <> "" Then
      If mId(INITIAL, 1, 1) = "'" Then INITIAL = mId(INITIAL, 2)
      
      If Trim(INITIAL) <> "" Then
        If mId(INITIAL, Len(INITIAL)) = "'" Then INITIAL = mId(INITIAL, 1, Len(INITIAL) - 1)
      End If
   End If
   
   'LENGHT
   lng = Get_Parametro(Riga, "LENGTH", ",")
    
    If lng <> "" Then
      lng = lng & "  "
      Length = mId(lng, 1, InStr(1, lng, " ") - 1) 'Tronca al primo spazio (Controllare se � giusto)
    Else
      Length = Len(INITIAL)
    End If
   
    Color = Get_Parametro(Riga, "COLOR", ",")
    
    Hilight = Get_Parametro(Riga, "HILIGHT", ",")
    PicIN = Get_Parametro(Riga, "PICIN", ",")
    
    If mId(PicIN, 1, 1) = "'" Then
      PicIN = mId(PicIN, 2)
      
      If mId(PicIN, Len(PicIN)) = "'" Then
        PicIN = mId(PicIN, 1, Len(PicIN) - 1)
      End If
    End If
    
    PicOUT = Get_Parametro(Riga, "PICOUT", ",")
    
    If mId(PicOUT, 1, 1) = "'" Then
      PicOUT = mId(PicOUT, 2)
      
      If mId(PicOUT, Len(PicOUT)) = "'" Then
        PicOUT = mId(PicOUT, 1, Len(PicOUT) - 1)
      End If
    End If
    
    'Memorizza nell'array
    ReDim Preserve BMS.Maps(IdxArray).Campi(j)
    BMS.Maps(IdxArray).Campi(j).NomeCmp = NomeCmp
    BMS.Maps(IdxArray).Campi(j).AttRB = Attr
    BMS.Maps(IdxArray).Campi(j).Color = Color
    BMS.Maps(IdxArray).Campi(j).INITIAL = INITIAL
    BMS.Maps(IdxArray).Campi(j).Justify = Justify
    BMS.Maps(IdxArray).Campi(j).Length = Length
    BMS.Maps(IdxArray).Campi(j).PosX = PosX
    BMS.Maps(IdxArray).Campi(j).PosY = PosY
    BMS.Maps(IdxArray).Campi(j).PicIN = PicIN
    BMS.Maps(IdxArray).Campi(j).PicOUT = PicOUT
    BMS.Maps(IdxArray).Campi(j).Hilight = Hilight
    
    old = Idx1
    
    Idx1 = RT.Find(" DFHMDF ", Idx1 + 8, StStop)
    j = j + 1
    
    If Fine = True Then
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
    
    DoEvents
    
    Screen.MousePointer = vbDefault
  Wend
End Sub

Public Sub Analizza_DFHMDI(RT As RichTextBox)
  'Variabili dell'MDI
  Dim NomeMappa As String
  Dim SizeX As Long
  Dim SizeY As Long
  Dim Size As String
  Dim Line As String
  Dim Column As String
  Dim TioApFx As String
  
  Dim Data As String
  Dim Justify As String
  Dim OBFMT As String
  Dim MapAtts As String
  Dim DSatts As String
  Dim Trailer As String
  Dim Header As String
  
  Dim Riga As String
  Dim IdxMdf As Long
  
  Dim StartPos As Long
  Dim StopPos As Long
  
  Dim idx As Long
  Dim idxStop As Long
  
  idx = RT.Find(" DFHMDI ", 1)
  
  While idx <> -1
    idxStop = RT.Find(" DFHMDI ", idx + 8)
    
    If idxStop = -1 Then idxStop = Len(RT.Text)
    IdxMdf = RT.Find(" DFHMDF ", idx, idxStop)
    
    If idx <> -1 Then
      StartPos = Vai_Inizio_Riga(RT, idx)
      If IdxMdf <> -1 Then
        StopPos = Vai_Inizio_Riga(RT, IdxMdf)
      Else
        StopPos = StartPos + 80
      End If
      
      RT.SelStart = StartPos
      RT.SelLength = StopPos - StartPos
      
      Riga = RT.selText
      
      Riga = Normalizza_Riga(Riga)
      
      'Carica i parametri
      NomeMappa = mId(Riga, 1, InStr(1, Riga, " ") - 1)
      
      Size = Get_Parametro(Riga, "SIZE", ")")
      
      If mId(Size, 1, 1) = "(" Then Size = mId(Size, 2)
      
      SizeY = Val(mId(Size, 1, InStr(1, Size, ",") - 1))
      SizeX = Val(mId(Size, InStr(1, Size, ",") + 1))
      
      Line = Get_Parametro(Riga, "LINE", ",")
      Column = Get_Parametro(Riga, "COLUMN", ",")
      
      TioApFx = Get_Parametro(Riga, "TIOAPFX", ",")
      
      Data = Get_Parametro(Riga, "DATA", ",")
      Justify = Get_Parametro(Riga, "JUSTIFY", ",")
      OBFMT = Get_Parametro(Riga, "OBFMT", ",")
      MapAtts = Get_Parametro(Riga, "MAPATTS", ")")
      
      If mId(MapAtts, 1, 1) = "(" Then
        MapAtts = mId(MapAtts, 2)
        
        If mId(MapAtts, Len(MapAtts)) = ")" Then
          MapAtts = mId(MapAtts, 1, Len(MapAtts) - 1)
        End If
      End If
      
      DSatts = Get_Parametro(Riga, "DSATTS", ")")
      
      If mId(DSatts, 1, 1) = "(" Then
        DSatts = mId(DSatts, 2)
        
        If mId(DSatts, Len(DSatts)) = ")" Then
          DSatts = mId(DSatts, 1, Len(DSatts) - 1)
        End If
      End If
      
      Trailer = Get_Parametro(Riga, "TRAILER", ",")
      Header = Get_Parametro(Riga, "HEADER", ",")
    End If
    
    'Memorizza nell'array in memoria
    ReDim Preserve BMS.Maps(UBound(BMS.Maps) + 1)
    ReDim Preserve BMS.Maps(UBound(BMS.Maps)).Campi(0)
    BMS.Maps(UBound(BMS.Maps)).Nome = NomeMappa
    BMS.Maps(UBound(BMS.Maps)).SizeX = SizeX
    BMS.Maps(UBound(BMS.Maps)).SizeY = SizeY
    BMS.Maps(UBound(BMS.Maps)).Line = Val(Line)
    BMS.Maps(UBound(BMS.Maps)).Column = Val(Column)
    BMS.Maps(UBound(BMS.Maps)).TioApFx = TioApFx
    BMS.Maps(UBound(BMS.Maps)).Data = Data
    BMS.Maps(UBound(BMS.Maps)).Justify = Justify
    BMS.Maps(UBound(BMS.Maps)).OBFMT = OBFMT
    BMS.Maps(UBound(BMS.Maps)).MapAtts = MapAtts
    BMS.Maps(UBound(BMS.Maps)).DSatts = DSatts
    BMS.Maps(UBound(BMS.Maps)).Trailer = Trailer
    BMS.Maps(UBound(BMS.Maps)).Header = Header
    
    'Carica i campi di questa Mappa
    Analizza_DFHMDF RT, UBound(BMS.Maps), idx, idxStop
    
    idx = RT.Find(" DFHMDI ", idx + 8)
  Wend
End Sub

Public Function get_Parametro_MFS(Linea As String, VoceParam As String, ChStop As String)
   Dim idx As Long
   Dim idxStop As Long
   Dim App As String
   
   idx = InStr(1, Linea, VoceParam & "=")
   
   If idx = 0 Then
      idx = InStr(1, Linea, VoceParam)
   End If
   
   If idx > 0 Then
      idxStop = InStr(idx, Linea, ChStop)
      
      If idxStop > 0 Then
         App = mId(Linea, idx + Len(VoceParam) + 1, idxStop - idx - Len(VoceParam))
      Else
         idxStop = InStr(idx, Linea, " ")
         
         If idxStop > 0 Then
            App = mId(Linea, idx + Len(VoceParam) + 1, idxStop - idx - Len(VoceParam))
         End If
      End If
   End If
   
   'App = Replace(App, ",", "")
   
   get_Parametro_MFS = App
End Function

Sub RiempiDevDiv(prMFS As Recordset)
  ' AC
  MFS.Dev.Tipo = prMFS!devtype
  MFS.Dev.Feat = prMFS!Feat
  MFS.Dev.SysMSG = prMFS!SysMSG
  MFS.Div.Tipo = prMFS!DivType
  MFS.NomeMappa = prMFS!FMTname
End Sub

Sub RiempiCampi(prMFS As Recordset)
  'AC
  Dim Attr() As String
  Dim i As Integer
    
  ReDim Preserve MFS.Campi(UBound(MFS.Campi) + 1)
  MFS.Campi(UBound(MFS.Campi)).len = CLng(prMFS!Lunghezza)
  If Not Left(prMFS!Nome, 1) = "#" Then
    MFS.Campi(UBound(MFS.Campi)).Nome = prMFS!Nome
  Else
    'Campo statico
    MFS.Campi(UBound(MFS.Campi)).Nome = ""
  End If
  MFS.Campi(UBound(MFS.Campi)).INITIAL = IIf(IsNull(prMFS!InitValue), "", prMFS!InitValue)
  If MFS.Campi(UBound(MFS.Campi)).len = 0 Then
    MFS.Campi(UBound(MFS.Campi)).len = Len(prMFS!InitValue)
  End If
  If MFS.Campi(UBound(MFS.Campi)).INITIAL = "" And MFS.Campi(UBound(MFS.Campi)).len > 0 And Trim(MFS.Campi(UBound(MFS.Campi)).Nome) <> "" Then
    MFS.Campi(UBound(MFS.Campi)).INITIAL = String(MFS.Campi(UBound(MFS.Campi)).len, ".")
  End If
  MFS.Campi(UBound(MFS.Campi)).POS.y = prMFS!Riga
  MFS.Campi(UBound(MFS.Campi)).POS.x = prMFS!Colonna
  Attr = Split(IIf(IsNull(prMFS!Attributo), "", prMFS!Attributo), ",")
  For i = 0 To UBound(Attr)
    MFS.Campi(UBound(MFS.Campi)).Attr.Add Attr(i)
  Next i
  MFS.Campi(UBound(MFS.Campi)).Occurs = IIf(IsNull(prMFS!Occurs), 1, prMFS!Occurs)
  MFS.Campi(UBound(MFS.Campi)).Funzione = IIf(IsNull(prMFS!Funzione), "", prMFS!Funzione)
End Sub

Public Function RiempiStrutturaInfoMappa(IdOggetto As Long) As Boolean
  ' AC
  Dim rMFS As Recordset
  Dim bol3270ver As Boolean
  Dim devtype As String
  
  ' lettura info generali
  bol3270ver = False
  Set rMFS = m_fun.Open_Recordset("Select * From PsDLI_MFS Where IdOggetto = " & IdOggetto)
  If Not rMFS.EOF Then
  ' riempio struttura con il primo
    devtype = rMFS!devtype
    RiempiDevDiv rMFS
    If Not InStr(rMFS!devtype, "3270") > 0 Then
    ' se non � versione 3270 la cerco nei successivi
      rMFS.MoveNext
      While Not rMFS.EOF And Not bol3270ver
        If InStr(rMFS!devtype, "3270") > 0 Then
        ' riempio struttura con il record per 3270
        devtype = rMFS!devtype
        RiempiDevDiv rMFS
          bol3270ver = True
        End If
      rMFS.MoveNext
      Wend
    End If
  Else
    MsgBox "Please, Parse the map before using the Map Editor", vbInformation, "Map not parsed"
    RiempiStrutturaInfoMappa = False
    Exit Function
  End If
  rMFS.Close
  
  ' lettura info campi
  ReDim MFS.Campi(0)
  Set rMFS = m_fun.Open_Recordset("Select * From PsDLI_MFSCmp Where IdOggetto = " & IdOggetto & " and DevType = '" & devtype & "'")
  While Not rMFS.EOF
    
    RiempiCampi rMFS
    rMFS.MoveNext
  Wend
  rMFS.Close
  RiempiStrutturaInfoMappa = True
End Function


Public Sub Analizza_Sorgente_MFS(RT As RichTextBox, Percorso As String)
   Dim i As Long
   Dim Linea As String
   Dim KeyLine As String
   Dim ChComm As String
   
   RT.Text = ""
   
   ReDim MFS.Campi(0)
   
   On Error Resume Next
   
   Open Percorso For Binary Access Read As #1
   
   If Dir(Percorso) = "" Then
      MsgBox "File not found...", vbInformation, m_fun.FnNomeProdotto
      Exit Sub
   End If
   
   Line Input #1, Linea
   Do While Not EOF(1)
      Linea = mId(Linea, 1, 72)
      'Controlla se � un commento
      ChComm = mId(Linea, 1, 1)
      If ChComm = "*" Then
         'Commento
      Else
        'SQ 3-05-06
        'Sbagliava!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        'Es.: DESB     DFLD LTH=9,POS=(3,27),ATTR=(PROT,HI,MOD)
        ' -> Tirava su "DFLD L"
         'KeyLine = Trim(UCase(mId(Linea, 10, 6)))
         KeyLine = mId(Linea, 10) & " " 'col bianco in fondo non devo fare if qui sotto...
         KeyLine = Left(KeyLine, InStr(KeyLine, " ") - 1)
         Select Case KeyLine
            Case "TITLE"
 '              Debug.Print "Found : " & Linea
            Case "PRINT"
 '              Debug.Print "Found : " & Linea
            Case "FMT"
               'Prende il nome della Mappa
               MFS.NomeMappa = Trim(mId(Linea, 1, 9))
            Case "DEV"
               Analizza_DEV Linea
            Case "DIV"
               Analizza_DIV Linea
            Case "DPAGE"
               'Da fare
            Case "DFLD"
               Analizza_DFLD Linea
            Case "FMTEND"
               'La descrizione della mappa � conclusa
               Exit Do
            Case Else
  '             Debug.Print "Found : " & Linea
         End Select
      End If
      Line Input #1, Linea
   Loop
   Close #1
End Sub

Public Sub Analizza_DIV(Linea As String)


   Dim App As String
   
   'TYPE
   App = get_Parametro_MFS(Linea, "TYPE", ")")
   
   If Trim(App) <> "" Then
      App = Replace(App, "(", "")
      App = Replace(App, ")", "")
      
      If mId(Linea, Len(Linea), 1) = "," Then
         App = mId(App, 1, Len(Linea) - 1)
      End If
      
      'Assegna il Tipo
      MFS.Div.Tipo = Trim(App)
   End If
End Sub

Public Sub Analizza_DFLD(Linea As String)
   Dim App As String
   Dim Attr() As String
   Dim i As Long
   Dim idx As Long
   
   ReDim Preserve MFS.Campi(UBound(MFS.Campi) + 1)
   
   'LEN
   App = get_Parametro_MFS(Linea, "LTH", ",")
   
   If Trim(App) = "" Then
      App = get_Parametro_MFS(Linea, "L.", ",")
   End If
   
   If Trim(App) <> "" Then
      App = Replace(App, "(", "")
      App = Replace(App, ")", "")
      App = Replace(App, ",", "")
      
      If mId(Linea, Len(Linea), 1) = "," Then
         App = mId(App, 1, Len(Linea) - 1)
      End If
      
      MFS.Campi(UBound(MFS.Campi)).len = CLng(App)
   End If
   
   'Nome Campo
   App = Trim(mId(Linea, 1, 9))
   
   If App <> "" Then
      'Campo Variabile
      MFS.Campi(UBound(MFS.Campi)).Nome = App
   Else
      'Campo statico
      MFS.Campi(UBound(MFS.Campi)).Nome = ""
      
      'Initial
      idx = InStr(16, Linea, ",")
      
      If idx > 0 Then
         App = mId(Linea, 16, idx - 16)
         
         App = Replace(App, "'", "")
         
         MFS.Campi(UBound(MFS.Campi)).INITIAL = App
         
         If MFS.Campi(UBound(MFS.Campi)).len = 0 Then
            MFS.Campi(UBound(MFS.Campi)).len = Len(App)
         End If
      End If
   End If
   
   If MFS.Campi(UBound(MFS.Campi)).INITIAL = "" And MFS.Campi(UBound(MFS.Campi)).len > 0 And Trim(MFS.Campi(UBound(MFS.Campi)).Nome) <> "" Then
      MFS.Campi(UBound(MFS.Campi)).INITIAL = String(MFS.Campi(UBound(MFS.Campi)).len, ".")
   End If
   
   'Pos
   App = get_Parametro_MFS(Linea, "POS", ")")
   
   If Trim(App) = "" Then
      App = get_Parametro_MFS(Linea, "P.", ")")
   End If
   
   If Trim(App) <> "" Then
      App = Replace(App, "(", "")
      App = Replace(App, ")", "")
      
      If mId(Linea, Len(Linea), 1) = "," Then
         App = mId(App, 1, Len(Linea) - 1)
      End If
      
      'Assegna la POS
      MFS.Campi(UBound(MFS.Campi)).POS.y = Left(App, InStr(App, ",") - 1)
      MFS.Campi(UBound(MFS.Campi)).POS.x = mId(App, InStr(App, ",") + 1)
                
   End If
   
   'ATTR
   App = get_Parametro_MFS(Linea, "ATTR", ")")
   
   If Trim(App) <> "" Then
      App = Replace(App, "(", "")
      App = Replace(App, ")", "")
      
      If mId(Linea, Len(Linea), 1) = "," Then
         App = mId(App, 1, Len(Linea) - 1)
      End If
      
      Attr = Split(App, ",")
      
      For i = 0 To UBound(Attr)
         MFS.Campi(UBound(MFS.Campi)).Attr.Add Attr(i)
      Next i
   End If
End Sub

Public Sub Analizza_DEV(Linea As String)
   Dim App As String
   
   'TYPE
   App = get_Parametro_MFS(Linea, "TYPE", ")")
   
   If Trim(App) <> "" Then
      App = Replace(App, "(", "")
      App = Replace(App, ")", "")
      App = mId(App, 1, InStr(1, App, ",") - 1)
      
      'Assegna il Tipo
      MFS.Dev.Tipo = App
   End If
   
   'FEAT
   App = get_Parametro_MFS(Linea, "FEAT", ",")
   
   If Trim(App) <> "" Then
      App = Replace(App, ",", "")
      
      'Assegna il Feat
      MFS.Dev.Feat = App
   End If
   
   App = get_Parametro_MFS(Linea, "SYSMSG", ",")
   
   If Trim(App) <> "" Then
      App = Replace(App, ",", "")
      
      'Assegna il Feat
      MFS.Dev.SysMSG = App
   End If
   
   'PFK : Da fare
End Sub

Public Sub Analizza_Testata_MFS(RT As RichTextBox)
   Dim idxStart  As Long
   Dim idxStop As Long
   Dim Linea As String
   Dim App As String
   
   idxStart = RT.Find(" FMT ", 1)
   
   If idxStart > 0 Then
      'Isola il parametro
      idxStart = Get_StartLine(RT, idxStart)
      idxStop = Get_EndLine(RT, idxStart)
      
      RT.SelStart = idxStart
      RT.SelLength = idxStop - idxStart
      Linea = RT.selText
      
      App = Trim(Replace(Linea, "FMT", ""))
      
      'Prende il nome
      MFS.NomeMappa = App
   Else
      MsgBox "Map Format Error... No Name Found...", vbInformation, m_fun.FnNomeProdotto
   End If
   
End Sub

Public Function Get_StartLine(RT As RichTextBox, idx As Long) As Long
   Dim i As Long
   Dim Ch As String
   Dim Index As Long
   
   Index = -1
   
   For i = 1 To 500
      RT.SelStart = idx - i
      RT.SelLength = 2
      
      Ch = RT.selText
      
      If Ch = vbCrLf Then
         Index = idx - i + 1
         Exit For
      End If
   Next i
   
   Get_StartLine = Index
End Function

Public Function Get_EndLine(RT As RichTextBox, idx As Long) As Long
  Dim i As Long
  Dim Ch As String
  Dim Index As Long
  
  Index = -1
  
  For i = 1 To 500
    RT.SelStart = idx + i
    RT.SelLength = 2
    
    Ch = RT.selText
    
    If Ch = vbCrLf Then
      Index = idx + i - 1
      Exit For
    End If
  Next i
  
  Get_EndLine = Index
End Function

Public Sub Analizza_Sorgente_BMS(RT As RichTextBox, Percorso As String)
  '1) Inizializza la struttura in memoria
  ReDim BMS.Maps(0)
  ReDim BMS.Maps(0).Campi(0)
  
  RT.Text = ""
  
  On Error GoTo ERR
  
  '2) Carica il file nella RICHTEXTBOxt
  RT.LoadFile Percorso
  
  '3) Analizza i DFHMSD
  Analizza_DFHMSD RT
  
  '4) Analizza i DFHMDI
  Analizza_DFHMDI RT
  Exit Sub
ERR:
  Close #1
  
  '3) Analizza i DFHMSD
  Analizza_DFHMSD RT
  
  '4) Analizza i DFHMDI
  Analizza_DFHMDI RT
End Sub

Public Sub Carica_TreeView_Mappa(TR As TreeView)
  Dim i As Long, j As Long
  Dim KeyC As String, KeyN As String
  
  TR.Nodes.Clear
  
  TR.Nodes.Clear
  
  TR.Nodes.Add , , "MAPSET", "MapSet : " & BMS.MapSet
  TR.Nodes(TR.Nodes.Count).Tag = "000" & BMS.MapSet
    TR.Nodes.Add "MAPSET", tvwChild, "M1", "Type : " & BMS.Tipo
    TR.Nodes(TR.Nodes.Count).Tag = "000" & BMS.MapSet
    TR.Nodes.Add "MAPSET", tvwChild, "M2", "Term : " & BMS.Term
    TR.Nodes(TR.Nodes.Count).Tag = "000" & BMS.MapSet
    TR.Nodes.Add "MAPSET", tvwChild, "M3", "Language : " & BMS.Lang
    TR.Nodes(TR.Nodes.Count).Tag = "000" & BMS.MapSet
    TR.Nodes.Add "MAPSET", tvwChild, "M4", "TioApFx : " & BMS.TioApFx
    TR.Nodes(TR.Nodes.Count).Tag = "000" & BMS.MapSet
    TR.Nodes.Add "MAPSET", tvwChild, "M5", "CtRl : " & BMS.CtRl
    TR.Nodes(TR.Nodes.Count).Tag = "000" & BMS.MapSet
    TR.Nodes.Add "MAPSET", tvwChild, "M6", "Data : " & BMS.Data
    TR.Nodes(TR.Nodes.Count).Tag = "000" & BMS.MapSet
    TR.Nodes.Add "MAPSET", tvwChild, "M7", "Mode : " & BMS.Mode
    TR.Nodes(TR.Nodes.Count).Tag = "000" & BMS.MapSet
  
      TR.Nodes.Add "MAPSET", tvwChild, "MAPS", "Maps"
      TR.Nodes(TR.Nodes.Count).Tag = "000" & BMS.MapSet
      
      For i = 1 To UBound(BMS.Maps)
        KeyN = "N" & Format(i, "00")
        TR.Nodes.Add "MAPS", tvwChild, KeyN, "Name : " & BMS.Maps(i).Nome
        TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
        
          TR.Nodes.Add KeyN, tvwChild, KeyN & i, "Size X : " & BMS.Maps(i).SizeX
          TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
          TR.Nodes.Add KeyN, tvwChild, KeyN & i + 1, "Size Y : " & BMS.Maps(i).SizeY
          TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
          TR.Nodes.Add KeyN, tvwChild, KeyN & i + 2, "Column : " & BMS.Maps(i).Column
          TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
          TR.Nodes.Add KeyN, tvwChild, KeyN & i + 3, "Line : " & BMS.Maps(i).Line
          TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
          TR.Nodes.Add KeyN, tvwChild, KeyN & i + 4, "TioApFx : " & BMS.Maps(i).TioApFx
          TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
          
          TR.Nodes.Add KeyN, tvwChild, "CMP" & i, "Fields"
          TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
          
            'Carica i suoi campi
            For j = 1 To UBound(BMS.Maps(i).Campi)
              If Trim(BMS.Maps(i).Campi(j).NomeCmp) <> "" Then
                KeyC = "C" & Format(j, "00") & Format(i, "00")
                TR.Nodes.Add "CMP" & i, tvwChild, KeyC, BMS.Maps(i).Campi(j).NomeCmp
                TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
                
                  TR.Nodes.Add KeyC, tvwChild, KeyC & i & j, "Pos X : " & BMS.Maps(i).Campi(j).PosX
                  TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
                  TR.Nodes.Add KeyC, tvwChild, KeyC & i & j + 1, "Pos Y : " & BMS.Maps(i).Campi(j).PosY
                  TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
                  TR.Nodes.Add KeyC, tvwChild, KeyC & i & j + 2, "Length : " & BMS.Maps(i).Campi(j).Length
                  TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
                  TR.Nodes.Add KeyC, tvwChild, KeyC & i & j + 3, "Justify : " & BMS.Maps(i).Campi(j).Justify
                  TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
                  TR.Nodes.Add KeyC, tvwChild, KeyC & i & j + 4, "Initial : " & BMS.Maps(i).Campi(j).INITIAL
                  TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
                  TR.Nodes.Add KeyC, tvwChild, KeyC & i & j + 5, "Color : " & BMS.Maps(i).Campi(j).Color
                  TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
                  TR.Nodes.Add KeyC, tvwChild, KeyC & i & j + 6, "AttRB : " & BMS.Maps(i).Campi(j).AttRB
                  TR.Nodes(TR.Nodes.Count).Tag = Format(i, "000") & BMS.Maps(i).Nome
                
              End If
            Next j
      Next i
End Sub

Public Sub Carica_ListView_Initial(Lsw As ListView, IdxMap As Long)
  Dim i As Long
  
  Lsw.ListItems.Clear
  
  For i = 1 To UBound(BMS.Maps(IdxMap).Campi)
    If Trim(BMS.Maps(IdxMap).Campi(i).INITIAL) <> "" Then
      Lsw.ListItems.Add , , BMS.Maps(IdxMap).Campi(i).INITIAL
      Lsw.ListItems(Lsw.ListItems.Count).ListSubItems.Add , , BMS.Maps(IdxMap).Campi(i).PosY
      Lsw.ListItems(Lsw.ListItems.Count).ListSubItems.Add , , BMS.Maps(IdxMap).Campi(i).PosX
    End If
  Next i
End Sub

Public Sub WaitTime(Sec As Long)
   Dim PauseTime As Long
   Dim Start As Variant
   
   PauseTime = Sec
   Start = Timer
   Do While Timer < Start + PauseTime
      DoEvents
   Loop
End Sub

Public Function Converti_Colore(Colore As String) As Variant
  Dim Col As Variant
  
  Select Case Trim(UCase(Colore))
    Case "DEFAULT", "NEUTRAL", ""
      Col = ColDefault
    Case "RED"
      Col = vbRed
    Case "BLUE"
      Col = vbBlue
    Case "PINK"
      Col = &HFFC0FF 'Rosa
    Case "GREEN"
      Col = vbGreen
    Case "TURQUOISE"
      Col = vbCyan
    Case "YELLOW"
      Col = vbYellow
    Case Else
      '??????????
      Col = vbWhite
'      Menu.BsFinestra.ListItems.Add , , "Color Undecoded..."
  End Select
  
  Converti_Colore = Col
End Function

Sub ResizeControls_MAPS(FrmName As Form)
    On Error Resume Next
    Dim designwidth As Long
    Dim designheight As Long
    Dim designfontsize As Integer
    Dim currentfontsize As Integer
    Dim numofcontrols As Integer
    Dim a As Integer
    Dim movetype As String, moveamount As Integer
    Dim GetResolutionX As Variant
    Dim GetResolutionY As Variant
    Dim RatioX As Variant
    Dim RatioY As Variant
    Dim FontRatio As Variant
    
    Dim ModFont As Boolean
    
    Dim Bool As Boolean
    
    designwidth = FrmName.Width / Screen.TwipsPerPixelX
    designheight = FrmName.Height / Screen.TwipsPerPixelY
    designfontsize = 96

    GetResolutionX = m_fun.FnWidth / Screen.TwipsPerPixelX
    GetResolutionY = m_fun.FnHeight / Screen.TwipsPerPixelY
    
    RatioX = GetResolutionX / designwidth
    RatioY = GetResolutionY / designheight
    
'    If IsScreenFontSmall Then
      currentfontsize = 96
'    Else
'      currentfontsize = 120
'    End If
    
    FontRatio = designfontsize / currentfontsize
    
    If RatioX = 1 And RatioY = 1 And FontRatio = 1 Then
      FrmName.Move m_fun.FnLeft, m_fun.FnTop, m_fun.FnWidth, m_fun.FnHeight
     Exit Sub
    End If
    
    numofcontrols = FrmName.Controls.Count - 1

    For a = 0 To numofcontrols
      If TypeOf FrmName.Controls(a) Is ImageList Or _
         TypeOf FrmName.Controls(a) Is Menu Or _
         TypeOf FrmName.Controls(a) Is Toolbar Then
            
         Bool = True
      End If
      
      If Bool = False Then
        '*****************
        '*** L A B E L ***
        '*****************
        If TypeOf FrmName.Controls(a) Is label Then
          'il font  cambia
          'FrmName.Controls(a).Font.Size = (FrmName.Controls(a).Font.Size - 1) * RatioX
          
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '*******************************
        '*** O P T I O N B U T T O N ***
        '*******************************
        If TypeOf FrmName.Controls(a) Is OptionButton Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        
        '***********************
        '*** C H E C K B O X ***
        '***********************
        If TypeOf FrmName.Controls(a) Is CheckBox Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '*********************************
        '*** C O M M A N D B U T T O N ***
        '*********************************
        If TypeOf FrmName.Controls(a) Is CommandButton Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        If TypeOf FrmName.Controls(a) Is Image Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          FrmName.Controls(a).Height = FrmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '***************************
        '*** D R I V E C O M B O ***
        '***************************
        If TypeOf FrmName.Controls(a) Is DriveListBox Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '***************************
        '*** D I R L I S T B O X ***
        '***************************
        If TypeOf FrmName.Controls(a) Is DirListBox Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          FrmName.Controls(a).Height = FrmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '*****************************************
        '*** V S C R O O L   E   H S C R O L L ***
        '*****************************************
        If TypeOf FrmName.Controls(a) Is VScrollBar Then
          'il font non cambia
        
          'FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          FrmName.Controls(a).Height = FrmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        If TypeOf FrmName.Controls(a) Is HScrollBar Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          'FrmName.Controls(a).Height = FrmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '*********************
        '*** T E X T B O X ***
        '*********************
        If TypeOf FrmName.Controls(a) Is TextBox Then
          'il font non cambia
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          'frmName.Controls(a).Height = frmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '*****************
        '*** F R A M E ***
        '*****************
        If TypeOf FrmName.Controls(a) Is Frame Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          FrmName.Controls(a).Height = FrmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '***********************
        '*** L I S T V I E W ***
        '***********************
        If TypeOf FrmName.Controls(a) Is ListView Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          FrmName.Controls(a).Height = FrmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '***********************
        '*** S S T A B       ***
        '***********************
        If TypeOf FrmName.Controls(a) Is SSTab Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          FrmName.Controls(a).Height = FrmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '***********************
        '*** T R E E V I E W ***
        '***********************
        If TypeOf FrmName.Controls(a) Is TreeView Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          FrmName.Controls(a).Height = FrmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '*******************************
        '**** R I C H T E X T B O X ****
        '*******************************
        If TypeOf FrmName.Controls(a) Is RichTextBox Then
          'il font cambia
          FrmName.Controls(a).Font.Size = (FrmName.Controls(a).Font.Size - 1) * RatioX
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          FrmName.Controls(a).Height = FrmName.Controls(a).Height * RatioY
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
        '***********************
        '*** C O M B O B O X ***
        '***********************
        If TypeOf FrmName.Controls(a) Is ComboBox Then
          'il font non cambia
        
          FrmName.Controls(a).Width = FrmName.Controls(a).Width * RatioX
          FrmName.Controls(a).Top = FrmName.Controls(a).Top * RatioY
          FrmName.Controls(a).Left = FrmName.Controls(a).Left * RatioX
        End If
        
      Else
        Bool = False
      End If
    Next a
    
    FrmName.Move m_fun.FnLeft, m_fun.FnTop, m_fun.FnWidth, m_fun.FnHeight
End Sub

'SQ - copiato...
Function TrovaParametro(Stringa, parola) As String
   Dim wStringa As String
   Dim wParm As String
   Dim Valore As String
   Dim K1 As Integer
   
   TrovaParametro = "None"
   wStringa = Stringa
   wParm = parola & "="
   
   K1 = InStr(wStringa, wParm)
   If K1 = 0 Then Exit Function
   Valore = mId$(wStringa, K1 + Len(wParm))
   Select Case mId$(Valore, 1, 1)
       Case "("
           K1 = InStr(Valore, ")")
           Valore = mId$(Valore, 1, K1)
       Case Else
           K1 = InStr(Valore, " ")
           If K1 > 0 Then
              Valore = mId$(Valore, 1, K1 - 1)
           End If
           K1 = InStr(Valore, ",")
           If K1 > 0 Then
              Valore = mId$(Valore, 1, K1 - 1)
           End If
   End Select
   
   K1 = InStr(Valore, "$")
   While K1 > 0
      Mid$(Valore, K1, 1) = Space$(1)
      K1 = InStr(Valore, "$")
   Wend
   TrovaParametro = Trim(Valore)
End Function

