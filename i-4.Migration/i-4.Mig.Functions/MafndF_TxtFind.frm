VERSION 5.00
Begin VB.Form MafndF_TxtFind 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Find..."
   ClientHeight    =   1905
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4020
   Icon            =   "MafndF_TxtFind.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1905
   ScaleWidth      =   4020
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtWord 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   630
      TabIndex        =   0
      Top             =   120
      Width           =   3285
   End
   Begin VB.CheckBox ChkWhole 
      Caption         =   "Whole words"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   630
      TabIndex        =   1
      Top             =   540
      Width           =   1395
   End
   Begin VB.CheckBox ChkMatch 
      Caption         =   "Match case"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   2760
      TabIndex        =   2
      Top             =   540
      Width           =   1245
   End
   Begin VB.CommandButton cmdSearch 
      Caption         =   "Search"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   600
      Picture         =   "MafndF_TxtFind.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   960
      Width           =   945
   End
   Begin VB.CommandButton CmdExit 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   3000
      Picture         =   "MafndF_TxtFind.frx":63D4
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   960
      Width           =   945
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Word"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   180
      Width           =   450
   End
End
Attribute VB_Name = "MafndF_TxtFind"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public mbFindMatch As Boolean
Public mbFindWhole As Boolean
Public txtSearch As Boolean

Private Sub CmdExit_Click()
  On Error GoTo EH
  
  txtSearch = False
  Unload Me
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub cmdSearch_Click()
  On Error GoTo EH
  
  txtSearch = True
  msSearchString = Trim(txtWord.Text)
  
  If ChkMatch.Value = 1 Then
    mbFindMatch = True
  Else
    mbFindMatch = False
  End If
  
  If ChkWhole.Value = 1 Then
    mbFindWhole = True
  Else
    mbFindWhole = False
  End If
  Unload Me
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Form_Load()
  On Error GoTo EH
      
  'msSearchString = vbNullString
  txtWord.Text = msSearchString
  
  mbFindMatch = False
  mbFindWhole = False
    
'  txtWord.SetFocus
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode = 0 Then
    txtSearch = False
  End If
End Sub

Private Sub txtWord_KeyPress(KeyAscii As Integer)
  On Error GoTo EH
      
  If KeyAscii = 13 Then
    cmdSearch_Click
  End If
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Public Sub Text()
  On Error GoTo EH
        
  'msSearchString = vbNullString
  'txtWord.Text = msSearchString
  
  mbFindMatch = False
  mbFindWhole = False
    
'  txtWord.SetFocus
    
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub
