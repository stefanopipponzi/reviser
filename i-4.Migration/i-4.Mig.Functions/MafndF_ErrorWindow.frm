VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MafndF_ErrorWindow 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Valorizzato RunTime"
   ClientHeight    =   2715
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   6075
   ControlBox      =   0   'False
   Icon            =   "MafndF_ErrorWindow.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2715
   ScaleWidth      =   6075
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   465
      Left            =   4710
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2190
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.CommandButton cmdYes 
      Caption         =   "Yes"
      Height          =   465
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2190
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.CommandButton cmdNo 
      Caption         =   "No"
      Height          =   465
      Left            =   2475
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2190
      Visible         =   0   'False
      Width           =   1305
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Height          =   435
      Left            =   2475
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2190
      Visible         =   0   'False
      Width           =   1275
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3960
      Top             =   1920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_ErrorWindow.frx":000C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_ErrorWindow.frx":1CE6
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_ErrorWindow.frx":7A48
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_ErrorWindow.frx":7FE2
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_ErrorWindow.frx":8434
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2085
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   6000
      _ExtentX        =   10583
      _ExtentY        =   3678
      _Version        =   393216
      TabOrientation  =   2
      Style           =   1
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Error"
      TabPicture(0)   =   "MafndF_ErrorWindow.frx":8886
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "imgErr"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblMessage"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "cmdSendByMail"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Details"
      TabPicture(1)   =   "MafndF_ErrorWindow.frx":88A2
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtRoutMod"
      Tab(1).Control(1)=   "txtDetails"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "System"
      TabPicture(2)   =   "MafndF_ErrorWindow.frx":88BE
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "lblMsgNativo"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      Begin VB.TextBox txtRoutMod 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   705
         Left            =   -74550
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   9
         Top             =   120
         Width           =   5475
      End
      Begin VB.TextBox txtDetails 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   1275
         Left            =   -74550
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Top             =   900
         Width           =   5475
      End
      Begin VB.CommandButton cmdSendByMail 
         Height          =   525
         Left            =   5400
         Picture         =   "MafndF_ErrorWindow.frx":88DA
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Send This Error by mail..."
         Top             =   1320
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.Label lblMsgNativo 
         Caption         =   "messaggio valorizzato a run time"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   1875
         Left            =   -74490
         TabIndex        =   6
         Top             =   180
         Width           =   5235
      End
      Begin VB.Label lblMessage 
         Caption         =   "messaggio valorizzato a run time"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   1545
         Left            =   1380
         TabIndex        =   5
         Top             =   420
         Width           =   4515
      End
      Begin VB.Image imgErr 
         Height          =   405
         Left            =   690
         Stretch         =   -1  'True
         Top             =   330
         Width           =   405
      End
   End
End
Attribute VB_Name = "MafndF_ErrorWindow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_retCode As e_ButtonChoose
Private wMousePointer As Long

Private Sub cmdCancel_Click()
  m_retCode = FN_CANCEL
  Me.Hide
  Screen.MousePointer = wMousePointer
End Sub

Private Sub cmdNo_Click()
  m_retCode = FN_NO
  Me.Hide
  Screen.MousePointer = wMousePointer
End Sub

Private Sub CmdOk_Click()
  m_retCode = FN_OK
  Me.Hide
  Screen.MousePointer = wMousePointer
End Sub

Private Sub cmdYes_Click()
  m_retCode = FN_YES
  Me.Hide
  Screen.MousePointer = wMousePointer
End Sub

Public Property Get RetCode() As e_ButtonChoose
  RetCode = m_retCode
  Me.Hide
  Screen.MousePointer = wMousePointer
End Property

Private Sub Form_Activate()
  If CmdOk.Visible Then
    CmdOk.SetFocus
  End If
  
  If cmdYes.Visible Then
    cmdYes.SetFocus
  End If
End Sub

Private Sub Form_Load()
  wMousePointer = Screen.MousePointer
  
  Screen.MousePointer = vbDefault
End Sub
