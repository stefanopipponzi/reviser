VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MafndF_ObjProp 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Object Properties"
   ClientHeight    =   6990
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   9135
   Icon            =   "MafndF_ObjProp.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6990
   ScaleWidth      =   9135
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraGeneral 
      Caption         =   "General Options"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   6945
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   9015
      Begin VB.Frame Frame3 
         Caption         =   "Attributes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   1275
         Left            =   120
         TabIndex        =   19
         Top             =   1500
         Width           =   5325
         Begin VB.CheckBox ChkIms 
            Caption         =   "IMS/DC"
            ForeColor       =   &H00C00000&
            Height          =   225
            Left            =   1160
            TabIndex        =   27
            Top             =   360
            Width           =   1065
         End
         Begin VB.CommandButton cmdUpdate 
            Caption         =   "Update"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   855
            Left            =   4290
            Picture         =   "MafndF_ObjProp.frx":014A
            Style           =   1  'Graphical
            TabIndex        =   26
            Top             =   270
            Width           =   885
         End
         Begin VB.CheckBox chkDLI 
            Caption         =   "DLI"
            ForeColor       =   &H00C00000&
            Height          =   225
            Left            =   3420
            TabIndex        =   25
            Top             =   360
            Width           =   795
         End
         Begin VB.CheckBox chkVSAM 
            Caption         =   "VSAM"
            ForeColor       =   &H00C00000&
            Height          =   225
            Left            =   150
            TabIndex        =   24
            Top             =   780
            Width           =   885
         End
         Begin VB.CheckBox chkSQL 
            Caption         =   "SQL"
            ForeColor       =   &H00C00000&
            Height          =   225
            Left            =   1530
            TabIndex        =   23
            Top             =   780
            Width           =   915
         End
         Begin VB.CheckBox chkBat 
            Caption         =   "BATCH"
            ForeColor       =   &H00C00000&
            Height          =   225
            Left            =   2320
            TabIndex        =   22
            Top             =   360
            Width           =   1005
         End
         Begin VB.CheckBox chkMap 
            Caption         =   "MAPPING"
            ForeColor       =   &H00C00000&
            Height          =   225
            Left            =   2940
            TabIndex        =   21
            Top             =   780
            Width           =   1095
         End
         Begin VB.CheckBox chkCICS 
            Caption         =   "CICS"
            ForeColor       =   &H00C00000&
            Height          =   225
            Left            =   150
            TabIndex        =   20
            Top             =   360
            Width           =   915
         End
      End
      Begin VB.TextBox txtParLev 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   6780
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   1470
         Width           =   2085
      End
      Begin VB.TextBox txtDtIncaps 
         BackColor       =   &H00FFFFC0&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dddddd mmmm yyyy, hh:mm.ss"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   6780
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   1065
         Width           =   2085
      End
      Begin VB.TextBox txtDtParsing 
         BackColor       =   &H00FFFFC0&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dddddd mmmm yyyy, hh:mm.ss"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   6780
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   645
         Width           =   2085
      End
      Begin VB.TextBox txtDtImport 
         BackColor       =   &H00FFFFC0&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dddddd mmmm yyyy, hh:mm.ss"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   6780
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   240
         Width           =   2085
      End
      Begin VB.TextBox txtRighe 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   6780
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   2385
         Width           =   2115
      End
      Begin VB.TextBox txtByte 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   6780
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   1920
         Width           =   2085
      End
      Begin VB.TextBox txtObject 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1290
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   240
         Width           =   4125
      End
      Begin VB.TextBox txtTipo 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1290
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   645
         Width           =   4125
      End
      Begin VB.TextBox txtDir 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1290
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   1065
         Width           =   4125
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   5640
         Top             =   3060
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   128
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   31
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":1E5C
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":1FB8
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":2114
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":2270
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":2E4C
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":4F88
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":6CDC
               Key             =   ""
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":C4D0
               Key             =   ""
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":E1DC
               Key             =   ""
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":FF00
               Key             =   ""
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":1005C
               Key             =   ""
            EndProperty
            BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":15DC0
               Key             =   ""
            EndProperty
            BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":188CC
               Key             =   ""
            EndProperty
            BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":191A8
               Key             =   ""
            EndProperty
            BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":195FC
               Key             =   ""
            EndProperty
            BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":19A54
               Key             =   ""
            EndProperty
            BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":1F678
               Key             =   ""
            EndProperty
            BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":20984
               Key             =   ""
            EndProperty
            BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":20DD8
               Key             =   ""
            EndProperty
            BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":21230
               Key             =   ""
            EndProperty
            BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":21684
               Key             =   ""
            EndProperty
            BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":233D8
               Key             =   ""
            EndProperty
            BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":250FC
               Key             =   ""
            EndProperty
            BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":25558
               Key             =   ""
            EndProperty
            BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":2587C
               Key             =   ""
            EndProperty
            BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":259D6
               Key             =   ""
            EndProperty
            BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":25B30
               Key             =   ""
            EndProperty
            BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":25C8A
               Key             =   ""
            EndProperty
            BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":25DE4
               Key             =   ""
            EndProperty
            BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":260FE
               Key             =   ""
            EndProperty
            BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MafndF_ObjProp.frx":26258
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView TRel 
         Height          =   3945
         Left            =   120
         TabIndex        =   28
         Top             =   2880
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   6959
         _Version        =   393217
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Parsing Level"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   5730
         TabIndex        =   17
         Top             =   1530
         Width           =   1095
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Encaps. Date"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   5730
         TabIndex        =   15
         Top             =   1110
         Width           =   1080
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Parsing Date"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   5730
         TabIndex        =   13
         Top             =   705
         Width           =   1065
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Import Date"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   5730
         TabIndex        =   11
         Top             =   285
         Width           =   1050
      End
      Begin VB.Label lblRighe 
         AutoSize        =   -1  'True
         Caption         =   "Line number"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   5730
         TabIndex        =   10
         Top             =   2430
         Width           =   975
      End
      Begin VB.Label lblByte 
         AutoSize        =   -1  'True
         Caption         =   "Byte"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   5730
         TabIndex        =   8
         Top             =   1980
         Width           =   390
      End
      Begin VB.Label lblObject 
         AutoSize        =   -1  'True
         Caption         =   "Sel. Object"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   180
         TabIndex        =   6
         Top             =   270
         Width           =   960
      End
      Begin VB.Label lblTipo 
         AutoSize        =   -1  'True
         Caption         =   "Object Type"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   180
         TabIndex        =   5
         Top             =   675
         Width           =   1035
      End
      Begin VB.Label lblDir 
         AutoSize        =   -1  'True
         Caption         =   "Dir. Object"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   180
         TabIndex        =   4
         Top             =   1125
         Width           =   975
      End
   End
   Begin VB.Menu mnuPopUpEdit 
      Caption         =   "PopUpEdit"
      Visible         =   0   'False
      Begin VB.Menu mnuEdit 
         Caption         =   "Edit Object selected..."
      End
      Begin VB.Menu mnuGenTXt 
         Caption         =   "Generate Txt"
      End
      Begin VB.Menu mnuGenTxtAllCaller 
         Caption         =   "Generate Txt All Caller"
      End
      Begin VB.Menu mnuGenTxtAllCalled 
         Caption         =   "Generate Txt All Called"
      End
      Begin VB.Menu Spe00 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExpRel 
         Caption         =   "Expand relationships..."
         Begin VB.Menu mnuAllRel 
            Caption         =   "All kind of Relationships"
         End
         Begin VB.Menu Sep01 
            Caption         =   "-"
         End
         Begin VB.Menu mnuCalledObj 
            Caption         =   "Only Called Objects..."
            Visible         =   0   'False
         End
         Begin VB.Menu mnuCallerobj 
            Caption         =   "Only Caller Objects..."
            Visible         =   0   'False
         End
         Begin VB.Menu mnuAllCalled 
            Caption         =   "All Called Objects"
         End
         Begin VB.Menu mnuAllCaller 
            Caption         =   "All Caller Objects"
         End
         Begin VB.Menu Sep02 
            Caption         =   "-"
         End
         Begin VB.Menu mnuAutoExp 
            Caption         =   "Automatic Relactionship expansion"
            Checked         =   -1  'True
         End
      End
   End
End
Attribute VB_Name = "MafndF_ObjProp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private ExpandedRelactionShips As Collection
Private ExpandedNodes As Collection
Private PrintedNodes As Collection
Private fAutoExpand As Boolean

Private Sub cmdUpdate_Click()
  Dim r As Recordset
  Dim Scelta As Long
  
  Scelta = MsgBox("Update Program Attributes? Are You sure...", vbYesNo, m_fun.FnNomeProdotto)
  If Scelta = vbNo Then Exit Sub
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & OggSel.IdOggetto)
  If r.RecordCount Then
    If chkDLI.Value = 1 Then
      r!DLI = True
    Else
      r!DLI = False
    End If
    
    If chkVSAM.Value = 1 Then
      r!VSam = True
    Else
      r!VSam = False
    End If
    
    If chkSQL.Value = 1 Then
      r!withSql = True
    Else
      r!withSql = False
    End If
    
    If chkCICS.Value = 1 Then
      r!Cics = True
    Else
      r!Cics = False
    End If
    
    If ChkIms.Value = 1 Then
      r!Ims = True
    Else
      r!Ims = False
    End If
    
    If chkBat.Value = 1 Then
      r!Batch = True
    Else
      r!Batch = False
    End If
    
    If chkMap.Value = 1 Then
      r!Mapping = True
    Else
      r!Mapping = False
    End If
    
    r.Update
    r.Close
  End If
End Sub

Private Sub Form_Load()
  Dim Righe As Long, Bytes As Long, Capt As String
  Dim fileName As String
  
  fAutoExpand = False
  mnuAutoExp.Checked = False
  
  Set ExpandedRelactionShips = New Collection
  Set ExpandedNodes = New Collection
  
  SetParent Me.hwnd, m_fun.FnParent
  
  m_fun.ResizeControls Me
  
  'Incolla nelle text le propriet�
  txtObject = OggSel.Nome
  txtDir = OggSel.Directory_Input
  txtTipo = OggSel.Tipo
  
  'Conta i byte e le righe dell'oggetto selezionato
  Righe = OggSel.NumRighe
  
  'MF
  If Trim(OggSel.Estensione) = "" Then
    fileName = OggSel.Directory_Input & "\" & OggSel.Nome
  Else
    fileName = OggSel.Directory_Input & "\" & OggSel.Nome & "." & OggSel.Estensione
  End If
  If Len(Dir(fileName, vbNormal)) Then
    Bytes = FileLen(fileName)
  Else
    Bytes = 0
    MsgBox "File " & fileName & " not found.", vbCritical, m_fun.FnNomeProdotto
  End If
  
  txtByte.Text = Format(Bytes, "###,###,###,##0")
  txtRighe.Text = Format(Righe, "###,###,###,##0")
  txtDtImport.Text = Format(OggSel.DtImport, "dd mmmm yyyy, hh:mm.ss")
  txtDtIncaps.Text = Format(OggSel.DtIncaps, "dd mmmm yyyy, hh:mm.ss")
  txtDtParsing.Text = Format(OggSel.DtParsing, "dd mmmm yyyy, hh:mm.ss")
  txtParLev = OggSel.ParsingLev
  
  If OggSel.Batch Then
    chkBat.Value = 1
  Else
    chkBat.Value = 0
  End If
  If OggSel.Cics Then
    chkCICS.Value = 1
  Else
    chkCICS.Value = 0
  End If
  If OggSel.Ims Then
    ChkIms.Value = 1
  Else
    ChkIms.Value = 0
  End If
  If OggSel.DLI Then
    chkDLI.Value = 1
  Else
    chkDLI.Value = 0
  End If
  If OggSel.Sql Then
    chkSQL.Value = 1
  Else
    chkSQL.Value = 0
  End If
  If OggSel.VSam Then
    chkVSAM.Value = 1
  Else
    chkVSAM.Value = 0
  End If
  If OggSel.Mapping Then
    chkMap.Value = 1
  Else
    chkMap.Value = 0
  End If
  
  Righe = 0
  Bytes = 0
  
  Capt = Me.Caption
  
  Me.Caption = Capt & " -- Object Id : " & OggSel.IdOggetto
  
  TRel.Nodes.Clear
  TRel.Nodes.Add , , "ROOT", OggSel.Nome, 7
  TRel.Nodes(TRel.Nodes.Count).Bold = True
  TRel.Nodes(TRel.Nodes.Count).ForeColor = vbBlue
  TRel.Nodes(TRel.Nodes.Count).Tag = OggSel.IdOggetto
  
  'Carica Le relazioni di primo livello
  Add_Section_Block TRel, "ROOT", OggSel.IdOggetto
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Public Sub Add_Section_Block(TRW As TreeView, wRootKey As String, wIdOggetto As Long, Optional ByVal wProfondita As e_ProfonditaRelazioni = TUTTI_GLI_OGGETTI)
  Dim wCurrentKey As String
  Dim wNewKey As String
  Dim wTipo As String
  Dim wIcon As Long
  Dim wDescription As String
  Dim cPgm As clsPgm
  Dim wRel As String
  Dim i As Long, n As Long
            
  ' gloria: utilizzo l'oggetto nodo per gestire meglio la treeView
  Dim myNode As Node
            
  'Controlla se la relazione � gi� stata espansa
  ' aggiungo anche la profondit� altrimenti se espando solo il ramo dei caller nn mi apre pi� i called e viceversa
  Dim itemSPlit() As String
   
  For i = 1 To ExpandedRelactionShips.Count
    itemSPlit = Split(ExpandedRelactionShips(i), "-")
    If itemSPlit(0) = wIdOggetto Then
'    If ExpandedRelactionShips(i) = wIdOggetto Then
      Select Case wProfondita
        Case TUTTI_GLI_OGGETTI ' se l'ho aperto in parte posso aggiungere solo quello che manca
          Exit Sub
        Case Else
          If (ExpandedRelactionShips(i) = wIdOggetto & "-" & wProfondita) Or (ExpandedRelactionShips(i) = wIdOggetto & "-" & TUTTI_GLI_OGGETTI) Then
            Exit Sub
          End If
      End Select
    End If
  Next i

  ExpandedRelactionShips.Add wIdOggetto & "-" & wProfondita
   
  Set cProgrammi = m_fun.Load_Relazioni_Programmi(wIdOggetto)
   
  '********************************** C A L L E R **************************************************
  If cProgrammi.CountCaller > 0 And (wProfondita = TUTTI_GLI_OGGETTI Or wProfondita = SOLO_OGGETTI_CALLER) Then
    TRel.Nodes.Add wRootKey, tvwChild, wRootKey & "#PGMCALLER", "Caller", 2
    TRel.Nodes(TRel.Nodes.Count).Bold = True
    TRel.Nodes(TRel.Nodes.Count).ForeColor = &H80&
     
    'Inserisce le sezioni Copy, Db, ecc....
    wCurrentKey = wRootKey & "#PGMCALLER"
     
    'Primo giro per mettere i tipi
    For i = 1 To cProgrammi.CountCaller
      Set cPgm = cProgrammi.GetCaller(i)
      
      Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
      If wIcon <> -1 And wDescription <> "" Then
        If Element_Is_Present_InList(wCurrentKey & wTipo) = False Then
          TRel.Nodes.Add wCurrentKey, tvwChild, wCurrentKey & wTipo, wDescription, wIcon
          TRel.Nodes(TRel.Nodes.Count).ForeColor = &H8000&
          TRel.Nodes(TRel.Nodes.Count).Bold = True
        End If
      End If
    Next i

    'Secondo giro per mettere gli oggetti nelle sezioni giuste
    For i = 1 To cProgrammi.CountCaller
      
      Set cPgm = cProgrammi.GetCaller(i)
         
      Valorizza_Relazione_By_RelOggetto cPgm.Relazione, wRel
      Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
      If wIcon <> -1 And wDescription <> "" Then
        wNewKey = wCurrentKey & wTipo
        wDescription = cPgm.Nome & wRel
        wIcon = 12

        Set myNode = Nothing
        Set myNode = TRel.Nodes.Add(wNewKey, tvwChild, wCurrentKey & wTipo & Trim(UCase(cPgm.Nome)) & cPgm.Id & Format(TRel.Nodes.Count, "0000"), wDescription, wIcon)
        myNode.Tag = cPgm.Id
'        myNode.EnsureVisible ' espande il nodo
      End If
    Next i
  End If
   
   '********************************** C A L L E D **************************************************
   If cProgrammi.CountCalled > 0 And (wProfondita = TUTTI_GLI_OGGETTI Or wProfondita = SOLO_OGGETTI_CALLED) Then
      TRel.Nodes.Add wRootKey, tvwChild, wRootKey & "#PGMCALLED", "Called", 1
      TRel.Nodes(TRel.Nodes.Count).Bold = True
      TRel.Nodes(TRel.Nodes.Count).ForeColor = &H80&
      
      'Inserisce le sezioni Copy, Db, ecc....
      wCurrentKey = wRootKey & "#PGMCALLED"
      
      'Primo giro per mettere i tipi
      For i = 1 To cProgrammi.CountCalled
         Set cPgm = cProgrammi.GetCalled(i)
         
         Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
         If wIcon <> -1 And wDescription <> "" Then
            If Element_Is_Present_InList(wCurrentKey & Trim(UCase(wTipo))) = False Then
               TRel.Nodes.Add wCurrentKey, tvwChild, wCurrentKey & wTipo, wDescription, wIcon
               TRel.Nodes(TRel.Nodes.Count).ForeColor = &H8000&
               TRel.Nodes(TRel.Nodes.Count).Bold = True
            End If
         End If
      Next i

      'Secondo giro per mettere gli oggetti nelle sezioni giuste
      For i = 1 To cProgrammi.CountCalled
         Set cPgm = cProgrammi.GetCalled(i)
         
         Valorizza_Relazione_By_RelOggetto cPgm.Relazione, wRel
         
         Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
         
         If wIcon <> -1 And wDescription <> "" Then
            'forza la valorizzazione di icon e description
            wDescription = cPgm.Nome & wRel
            wIcon = 12
            wNewKey = wCurrentKey & wTipo
            
            Set myNode = Nothing
            Set myNode = TRel.Nodes.Add(wNewKey, tvwChild, wCurrentKey & wTipo & Trim(UCase(cPgm.Nome)) & cPgm.Id & Format(TRel.Nodes.Count, "0000"), wDescription, wIcon)
            myNode.Tag = cPgm.Id
'            myNode.EnsureVisible ' espande il nodo
      
         End If
         
      Next i
      
   End If
End Sub

Public Sub Add_Section_Block_All(TRW As TreeView, wRootKey As String, wIdOggetto As Long, Optional ByVal wProfondita As e_ProfonditaRelazioni = TUTTI_GLI_OGGETTI)
   Dim wCurrentKey As String
   Dim wNewKey As String
   Dim wTipo As String
   Dim wIcon As Long
   Dim wDescription As String
   Dim cPgm As clsPgm
   Dim wRel As String
   Dim i As Long, n As Long
            
  ' gloria: utilizzo l'oggetto nodo per gestire meglio la treeView
   Dim myNode As Node
   Dim booTrovato As Boolean
   'Controlla se la relazione � gi� stata espansa
   ' aggiungo anche la profondit� altrimenti se espando solo il ramo dei caller nn mi apre pi� i called e viceversa
   Dim itemSPlit() As String
   
   
'   If TRel.Nodes.Count > 2000 Then Exit Sub
   
   For i = 1 To ExpandedRelactionShips.Count
    itemSPlit = Split(ExpandedRelactionShips(i), "-")
     If itemSPlit(0) = wIdOggetto Then
       Select Case wProfondita
       Case TUTTI_GLI_OGGETTI ' se l'ho aperto in parte posso aggiungere solo quello che manca
         Exit Sub
       Case Else
         If (ExpandedRelactionShips(i) = wIdOggetto & "-" & wProfondita) Or (ExpandedRelactionShips(i) = wIdOggetto & "-" & TUTTI_GLI_OGGETTI) Then
            Exit Sub
         End If
       End Select
    End If
   Next i

   ExpandedRelactionShips.Add wIdOggetto & "-" & wProfondita
   
   Set cProgrammi = m_fun.Load_Relazioni_Programmi(wIdOggetto, , CStr(wProfondita))
      
      
   '*************************************************************************************************
   '********************************** C A L L E R **************************************************
   '*************************************************************************************************
   If cProgrammi.CountCaller > 0 And (wProfondita = TUTTI_GLI_OGGETTI Or wProfondita = SOLO_OGGETTI_CALLER) Then
           
      TRel.Nodes.Add wRootKey, tvwChild, wRootKey & "#PGMCALLER", "Caller", 2
      TRel.Nodes(TRel.Nodes.Count).Bold = True
      TRel.Nodes(TRel.Nodes.Count).ForeColor = &H80&
      
      'Inserisce le sezioni Copy, Db, ecc....
      wCurrentKey = wRootKey & "#PGMCALLER"
       
      booTrovato = False
      
      If ExpandedNodes.Count = 0 Then
         ExpandedNodes.Add wIdOggetto ' il primo nodo lo inserisco subito
      Else
         For i = 1 To ExpandedNodes.Count
            If ExpandedNodes(i) = wIdOggetto Then
               booTrovato = True
               Exit For
            End If
         Next i
      End If
               
      If Not booTrovato Then ' se � gi� stato espanso lo inserisco ma non carico i caller
      
         ExpandedNodes.Add wIdOggetto ' mi salvo i nodi gi� espansi cos� non espando pi�
         'Primo giro per mettere i tipi
         For i = 1 To cProgrammi.CountCaller
            Set cPgm = cProgrammi.GetCaller(i)
            
            Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
            
            If wIcon <> -1 And wDescription <> "" Then
               If Element_Is_Present_InList(wCurrentKey & wTipo) = False Then
                  TRel.Nodes.Add wCurrentKey, tvwChild, wCurrentKey & wTipo, wDescription, wIcon
                  TRel.Nodes(TRel.Nodes.Count).ForeColor = &H8000&
                  TRel.Nodes(TRel.Nodes.Count).Bold = True
               End If
            End If
         Next i
   
         'Secondo giro per mettere gli oggetti nelle sezioni giuste
         For i = 1 To cProgrammi.CountCaller
            
            Set cPgm = cProgrammi.GetCaller(i)
               
            Valorizza_Relazione_By_RelOggetto cPgm.Relazione, wRel
            
            Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
            
            If wIcon <> -1 And wDescription <> "" Then
               wNewKey = wCurrentKey & wTipo
               wDescription = cPgm.Nome & wRel
               wIcon = 12
      
               Set myNode = Nothing
               Set myNode = TRel.Nodes.Add(wNewKey, tvwChild, wCurrentKey & wTipo & Trim(UCase(cPgm.Nome)) & cPgm.Id & Format(TRel.Nodes.Count, "0000"), wDescription, wIcon)
               myNode.Tag = cPgm.Id
               myNode.EnsureVisible ' espande il nodo
               
            End If
         Next i
         
         'reitero per caricare tutti i caller
          If wProfondita = SOLO_OGGETTI_CALLER Then
             For n = TRel.Nodes.Count To 1 Step -1
               Set myNode = TRel.Nodes(n)
               
               If Trim(myNode.Tag) <> "" And myNode.Tag <> "-1" Then
               'controllo che il nodo non sia gi� stato espanso
                  For i = 1 To ExpandedNodes.Count
                     If ExpandedNodes(i) = myNode.Tag Then
                        booTrovato = True
                        Exit For
                     End If
                  Next i
                  If Not booTrovato Then
                     Add_Section_Block_All TRel, myNode.key, myNode.Tag, wProfondita
                  Else
                     booTrovato = False
                  End If
               End If
             Next n
          End If
      End If
   End If
   
   '*************************************************************************************************
   '********************************** C A L L E D **************************************************
   '*************************************************************************************************
   If cProgrammi.CountCalled > 0 And (wProfondita = TUTTI_GLI_OGGETTI Or wProfondita = SOLO_OGGETTI_CALLED) Then
      TRel.Nodes.Add wRootKey, tvwChild, wRootKey & "#PGMCALLED", "Called", 1
      TRel.Nodes(TRel.Nodes.Count).Bold = True
      TRel.Nodes(TRel.Nodes.Count).ForeColor = &H80&
      
      'Inserisce le sezioni Copy, Db, ecc....
      wCurrentKey = wRootKey & "#PGMCALLED"
      
      booTrovato = False
      
      If ExpandedNodes.Count = 0 Then
         ExpandedNodes.Add wIdOggetto ' il primo nodo lo inserisco subito
      Else
         For i = 1 To ExpandedNodes.Count
            If ExpandedNodes(i) = wIdOggetto Then
               booTrovato = True
               Exit For
            End If
         Next i
      End If
               
      If Not booTrovato Then
         
         ExpandedNodes.Add wIdOggetto ' il primo nodo lo inserisco subito
         
         'Primo giro per mettere i tipi
         For i = 1 To cProgrammi.CountCalled
            Set cPgm = cProgrammi.GetCalled(i)
            
            Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
            If wIcon <> -1 And wDescription <> "" Then
               If Element_Is_Present_InList(wCurrentKey & Trim(UCase(wTipo))) = False Then
                  TRel.Nodes.Add wCurrentKey, tvwChild, wCurrentKey & wTipo, wDescription, wIcon
                  TRel.Nodes(TRel.Nodes.Count).ForeColor = &H8000&
                  TRel.Nodes(TRel.Nodes.Count).Bold = True
               End If
            End If
         Next i
   
         'Secondo giro per mettere gli oggetti nelle sezioni giuste
         For i = 1 To cProgrammi.CountCalled
            Set cPgm = cProgrammi.GetCalled(i)
            
            Valorizza_Relazione_By_RelOggetto cPgm.Relazione, wRel
            
            Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
            
            If wIcon <> -1 And wDescription <> "" Then
               'forza la valorizzazione di icon e description
               wDescription = cPgm.Nome & wRel
               wIcon = 12
               wNewKey = wCurrentKey & wTipo
               
               Set myNode = Nothing
               Set myNode = TRel.Nodes.Add(wNewKey, tvwChild, wCurrentKey & wTipo & Trim(UCase(cPgm.Nome)) & cPgm.Id & Format(TRel.Nodes.Count, "0000"), wDescription, wIcon)
               myNode.Tag = cPgm.Id
               myNode.EnsureVisible ' espande il nodo
           
            End If
            
         Next i

         'reitero per caricare tutti i called
          If wProfondita = SOLO_OGGETTI_CALLED Then

             For n = TRel.Nodes.Count To 1 Step -1
               Set myNode = TRel.Nodes(n)
               
               If Trim(myNode.Tag) <> "" And myNode.Tag <> "-1" Then
               'controllo che il nodo non sia gi� stato espanso
                  For i = 1 To ExpandedNodes.Count
                     If ExpandedNodes(i) = myNode.Tag Then
                        booTrovato = True
                        Exit For
                     End If
                  Next i
                  If Not booTrovato Then
                     Add_Section_Block_All TRel, myNode.key, myNode.Tag, wProfondita
                  Else
                     booTrovato = False
                  End If
               End If
             Next n
          End If
       End If
   End If
End Sub

'genera File Txt del primo livello con elenco propriet� oggetto
Public Sub Generate_Txt(TRW As TreeView, wRootKey As String, wIdOggetto As Long, ByVal ObjName As String, Optional ByVal wProfondita As e_ProfonditaRelazioni = TUTTI_GLI_OGGETTI)
   Dim wCurrentKey As String
   Dim wNewKey As String
   Dim wTipo As String
   Dim wIcon As Long
   Dim wDescription As String
   Dim cPgm As clsPgm
   Dim wRel As String
   Dim i As Long

  ' gloria: utilizzo l'oggetto nodo per gestire meglio la treeView
   Dim myNode As Node
   Dim booExpanded As Boolean ' dice se l'oggetto � gi� espanso

   Dim intNumIndent As Integer
   Dim fileName As String
   Dim NumFile As Integer
   Dim strTab As String
   Dim strTipologia As String
   Dim splitKey() As String

   fileName = m_fun.FnPathPrj & "\Documents\TXT\objProp_" & ObjName & ".txt"

   If Dir(fileName) <> "" Then
     If MsgBox("File already exists " & fileName & vbCrLf & "Do you want to rewrite it?", vbQuestion + vbYesNo, "i-4.Migration - Object Properties") = vbNo Then Exit Sub
   End If

   NumFile = FreeFile
   Open fileName For Output As #NumFile
   Set cProgrammi = m_fun.Load_Relazioni_Programmi(wIdOggetto)
   Print #NumFile, ObjName

 '*************************************************************************************************
 '********************************** C A L L E R **************************************************
 '*************************************************************************************************
  If cProgrammi.CountCaller > 0 And (wProfondita = TUTTI_GLI_OGGETTI Or wProfondita = SOLO_OGGETTI_CALLER) Then
    
    Print #NumFile, "CALLER"
    'Inserisce le sezioni Copy, Db, ecc....
    wCurrentKey = wRootKey & "#PGMCALLER"

    'Primo giro per mettere i tipi
    For i = 1 To cProgrammi.CountCaller
       Set cPgm = cProgrammi.GetCaller(i)

       Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription

    Next i

    'Secondo giro per mettere gli oggetti nelle sezioni giuste
    For i = 1 To cProgrammi.CountCaller
       Set cPgm = cProgrammi.GetCaller(i)

       Valorizza_Relazione_By_RelOggetto cPgm.Relazione, wRel

       Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription

       If wIcon <> -1 And wDescription <> "" Then
          wNewKey = wCurrentKey & wTipo
          wIcon = 12
          wDescription = cPgm.Nome & " (" & cPgm.Tipo & " )"
         
          splitKey() = Split(wCurrentKey & wTipo & Trim(UCase(cPgm.Nome)) & cPgm.Id & Format(TRel.Nodes.Count, "0000"), "#")
                            
          strTab = Space(CLng(UBound(splitKey) * 3))
                            
          If Trim(wDescription) <> "( )" Then Print #NumFile, strTab & wDescription

       End If
    Next i
  End If

  strTipologia = ""
  strTab = ""

   
 '*************************************************************************************************
 '********************************** C A L L E D **************************************************
 '*************************************************************************************************
  If cProgrammi.CountCalled > 0 And (wProfondita = TUTTI_GLI_OGGETTI Or wProfondita = SOLO_OGGETTI_CALLED) Then
   
    Print #NumFile, "CALLED"

    'Inserisce le sezioni Copy, Db, ecc....
    wCurrentKey = wRootKey & "#PGMCALLED"

    'Primo giro per mettere i tipi
    For i = 1 To cProgrammi.CountCalled
       Set cPgm = cProgrammi.GetCalled(i)

       Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
    Next i

    'Secondo giro per mettere gli oggetti nelle sezioni giuste
    For i = 1 To cProgrammi.CountCalled
       Set cPgm = cProgrammi.GetCalled(i)

       Valorizza_Relazione_By_RelOggetto cPgm.Relazione, wRel

       Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription

       If wIcon <> -1 And wDescription <> "" Then
          'forza la valorizzazione di icon e description
          wDescription = cPgm.Nome & " (" & cPgm.Tipo & " )"
          wIcon = 12
          wNewKey = wCurrentKey & wTipo
         
          splitKey() = Split(wCurrentKey & wTipo & Trim(UCase(cPgm.Nome)) & cPgm.Id & Format(TRel.Nodes.Count, "0000"), "#")
                            
          strTab = Space(CLng(UBound(splitKey) * 3))
                            
          If Trim(wDescription) <> "( )" Then Print #NumFile, strTab & wDescription

       End If
    Next i
  End If
  
  Close #NumFile
   
   Dim testoFile As String, Line As String
   
   Open fileName For Input As #NumFile
   
   Line Input #NumFile, Line
   testoFile = Line
   Do Until EOF(NumFile)
     Line Input #NumFile, Line
     testoFile = testoFile & vbCrLf & Line
   Loop
   Close #NumFile
   
   m_fun.Show_ShowText testoFile, fileName
End Sub

'genera File Txt con elenco propriet� oggetto
Public Sub Generate_Txt_All(wRootKey As String, wIdOggetto As Long, ByRef NumFile As Integer, Optional ByVal ObjName As String, Optional ByVal wProfondita As e_ProfonditaRelazioni = TUTTI_GLI_OGGETTI)
   Dim wCurrentKey As String
   Dim wNewKey As String
   Dim wTipo As String
   Dim wIcon As Long
   Dim wDescription As String
   Dim cPgm As clsPgm
   Dim wRel As String
   Dim i As Long, n As Long

   Dim strTab As String
   Dim strTipologia As String
   Dim splitKey() As String
   Dim booTrovato As Boolean
   Dim nodeKey As String
   
   Dim MyProgrammi As clsPgm
   
'   If PrintedNodes.Count > 2000 Then Exit Sub
   
   Set MyProgrammi = m_fun.Load_Relazioni_Programmi(wIdOggetto, , CStr(wProfondita))
       
   '*************************************************************************************************
   '********************************** C A L L E R **************************************************
   '*************************************************************************************************
   If MyProgrammi.CountCaller > 0 And (wProfondita = TUTTI_GLI_OGGETTI Or wProfondita = SOLO_OGGETTI_CALLER) Then
            
            'Inserisce le sezioni Copy, Db, ecc....
     wCurrentKey = wRootKey & "#PGMCALLER"
      
      booTrovato = False
      
      If PrintedNodes.Count = 0 Then
         PrintedNodes.Add wIdOggetto ' il primo nodo lo inserisco subito
      Else
         For i = 1 To PrintedNodes.Count
            If PrintedNodes(i) = wIdOggetto Then
               booTrovato = True
               Exit For
            End If
         Next i
      End If
               
      If Not booTrovato Then ' se � gi� stato espanso lo inserisco ma non carico i caller
         
         PrintedNodes.Add wIdOggetto ' il primo nodo lo inserisco subito
         'Primo giro per mettere i tipi
         For i = 1 To MyProgrammi.CountCaller
            Set cPgm = MyProgrammi.GetCaller(i)
            
            Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
         Next i
   
         'Secondo giro per mettere gli oggetti nelle sezioni giuste
         For i = 1 To MyProgrammi.CountCaller
            
            Set cPgm = MyProgrammi.GetCaller(i)
               
            Valorizza_Relazione_By_RelOggetto cPgm.Relazione, wRel
            
            Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
                  
             If wIcon <> -1 And wDescription <> "" Then
                wNewKey = wCurrentKey & wTipo
                wDescription = cPgm.Nome & " " & cPgm.Tipo
                wIcon = 12
                wDescription = cPgm.Nome & " (" & cPgm.Tipo & " )"
                               
                nodeKey = wCurrentKey & wTipo & Trim(UCase(cPgm.Nome)) & cPgm.Id & Format(PrintedNodes.Count, "0000")
                splitKey() = Split(nodeKey, "#")
                                  
                strTab = Space(CLng(UBound(splitKey) * 3))
                                  
                If Trim(wDescription) <> "( )" Then Print #NumFile, strTab & wDescription
                
                Generate_Txt_All nodeKey, cPgm.Id, NumFile, cPgm.Nome, wProfondita
      
             End If
         Next i
      Else
         nodeKey = wCurrentKey
         splitKey() = Split(nodeKey, "#")
         strTab = Space(CLng(UBound(splitKey) * 3))
         
         Print #NumFile, strTab & ObjName & "(*)"
      End If
   End If

  strTipologia = ""
  strTab = ""

 '*************************************************************************************************
 '********************************** C A L L E D **************************************************
 '*************************************************************************************************
  If MyProgrammi.CountCalled > 0 And (wProfondita = TUTTI_GLI_OGGETTI Or wProfondita = SOLO_OGGETTI_CALLED) Then
     
     wCurrentKey = wRootKey & "#PGMCALLED"

      booTrovato = False
      
      If PrintedNodes.Count = 0 Then
         PrintedNodes.Add wIdOggetto ' il primo nodo lo inserisco subito
      Else
         For i = 1 To PrintedNodes.Count
            If PrintedNodes(i) = wIdOggetto Then
               booTrovato = True
               Exit For
            End If
         Next i
      End If
               
      If Not booTrovato Then ' se � gi� stato espanso lo inserisco ma non carico i caller
         
         PrintedNodes.Add wIdOggetto ' il primo nodo lo inserisco subito
         'Primo giro per mettere i tipi
         For i = 1 To MyProgrammi.CountCalled
            Set cPgm = MyProgrammi.GetCalled(i)
            
            Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
         Next i
   
         'Secondo giro per mettere gli oggetti nelle sezioni giuste
         For i = 1 To MyProgrammi.CountCalled
            
            Set cPgm = MyProgrammi.GetCalled(i)
               
            Valorizza_Relazione_By_RelOggetto cPgm.Relazione, wRel
            
            Valorizza_Tipo_Icon_Description_By_TipoOggetto cPgm.Tipo, wTipo, wIcon, wDescription
                  
             If wIcon <> -1 And Trim(wDescription) <> "" Then
                wNewKey = wCurrentKey & wTipo
                wDescription = cPgm.Nome & " (" & cPgm.Tipo & " )"
                wIcon = 12
                               
                nodeKey = wCurrentKey & wTipo & Trim(UCase(cPgm.Nome)) & cPgm.Id & Format(PrintedNodes.Count, "0000")
                splitKey() = Split(nodeKey, "#")
                                  
                strTab = Space(CLng(UBound(splitKey) * 3))
                                  
                If Trim(wDescription) <> "( )" Then Print #NumFile, strTab & wDescription
                
                Generate_Txt_All nodeKey, cPgm.Id, NumFile, cPgm.Nome, wProfondita
      
             End If
         Next i
      Else
         nodeKey = wCurrentKey
         splitKey() = Split(nodeKey, "#")
         strTab = Space(CLng(UBound(splitKey) * 3))
         
         Print #NumFile, strTab & ObjName & "(*)"
      End If

  End If
  
End Sub

Public Sub Valorizza_Relazione_By_RelOggetto(wRelObj As String, wRel As String)
  Select Case Trim(UCase(wRelObj))
    Case "XCTL"
      wRel = " - (" & wRelObj & ")"
    Case "CAL", "CALL"
      wRel = " - (" & wRelObj & ")"
    Case "RETURN", "START"
      wRel = " - (" & wRelObj & ")"
    Case "LINK"
      wRel = " - (" & wRelObj & ")"
    Case "DATASET"
      wRel = " - (" & wRelObj & ")"
    Case "ASSIGN"
      wRel = " - (" & wRelObj & ")"
    Case "USED"
      wRel = " - (" & wRelObj & ")"
    Case "SCHEDULE" 'psb
      wRel = ""
    Case Else
      wRel = ""
  End Select
End Sub

Public Sub Valorizza_Tipo_Icon_Description_By_TipoOggetto(PgmTipo As String, wTipo As String, wIcon As Long, wDescription As String)
  Select Case UCase(Trim(PgmTipo))
    Case "CBL", "COB", "CAL", "CALL"
      wDescription = "Cobol Programs"
      wIcon = 5
      wTipo = "CBL"
    Case "CPY", "INCLUDE", "COPY"
      wDescription = "Copy"
      wIcon = 3
      wTipo = "CPY"
    Case "VSM", "VSAM", "FILE", "DATASET", "ASSIGN"
      wDescription = "VSam/Files"
      wIcon = 16
      wTipo = "VSM"
    Case "PSB"
      wDescription = "Psb"
      wIcon = 21
      wTipo = "PSB"
    Case "JOB", "JCL"
      wDescription = "Jcl/Job Schedules"
      wIcon = 24
      wTipo = "JOB"
    Case "UNK"
      wDescription = "Unknown Objects"
      wIcon = 9
      wTipo = "UNK"
    Case "RDBMS"
      wDescription = "Database"
      wIcon = 4
      wTipo = "RDBMS"
    Case "MAP", "MAPS", "MAPSET", "BMS", "MFS", "MAPPA", "MAPPING"
      wDescription = "Maps"
      wIcon = 6
      wTipo = "MAP"
    Case "RETURN", "START"
      wDescription = "TransId"
      wIcon = 25
      wTipo = "TRANSID"
    Case "IST", "ISTR"
      wDescription = "Instructions"
      wIcon = 15
      wTipo = "IST"
    Case "DLI", "DL1"
      wDescription = "DLI Database"
      wIcon = 17
      wTipo = "DLI"
    Case "PLI"
      wDescription = "PLI Programs"
      wIcon = 28
      wTipo = "PLI"
    Case "INC"
      wDescription = "Include"
      wIcon = 3
      wTipo = "INC"
    'migdal
    Case "NAT"
      wDescription = "Natural Programs"
      wIcon = 28
      wTipo = "NAT"
    Case "NIN"
      wDescription = "Natural Includes"
      wIcon = 3
      wTipo = "NIN"
    Case "SQL", "SQL ORA", "SQL DB2", "DB2", "ORA", "ORC"
      wDescription = "SQL (Generic Standard)"
      wIcon = 26
      wTipo = "SQL"
    Case "PRC", "PROC"
      wDescription = "Procedure Programs"
      wIcon = 13
      wTipo = "PRC"
    Case "ASM"
      wDescription = "Assembler Programs"
      wIcon = 14
      wTipo = "ASM"
    Case "EZT"
      wDescription = "EasyTrieve Programs"
      wIcon = 14
      wTipo = "EZT"
    Case "EZM", "MAC"
      wDescription = "Macro"
      wIcon = 3
      wTipo = UCase(Trim(PgmTipo))
    Case "PLAN"
      wDescription = ""
      wIcon = -1
      wTipo = ""
    Case "SYSTEM"
      wDescription = "System Objects"
      wIcon = 29
      wTipo = "SYS"
    Case "DBD"
      wDescription = "DBD"
      wIcon = 31
      wTipo = "DBD"
    Case "SEGMENT"
      wDescription = "Segment"
      wIcon = 30
      wTipo = "SGM"
    'Mauro: 13/06/2006
    Case "PGM"
      wDescription = "Unknown Programs"
      wIcon = 5
      wTipo = "PGM"
    Case "MBR"
      wDescription = "Include Member"
      wIcon = 3
      wTipo = "MBR"
    Case "PRM"
      wDescription = "SK-Param"
      wIcon = 3
      wTipo = "PRM"
    Case Else
      m_fun.WriteLog "NEW TYPE : " & PgmTipo, " - Valorizza_Tipo_Icon_Description_By_TipoOggetto"
      wDescription = "Unknown New Objects"
      wIcon = 27
      wTipo = "NEW"
  End Select
End Sub

Public Function Element_Is_Present_InList(wKey As String) As Boolean
  Dim i As Long
   
  Element_Is_Present_InList = False
  For i = 1 To TRel.Nodes.Count
    If Trim(UCase(TRel.Nodes(i).key)) = Trim(UCase(wKey)) Then
      Element_Is_Present_InList = True
      Exit Function
    End If
  Next i
End Function

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False

  SetFocusWnd m_fun.FnParent
End Sub

Private Sub mnuAllCalled_Click()
  TRel.Nodes.Clear
  TRel.Nodes.Add , , "ROOT", OggSel.Nome, 7
  TRel.Nodes(TRel.Nodes.Count).Bold = True
  TRel.Nodes(TRel.Nodes.Count).ForeColor = vbBlue
  TRel.Nodes(TRel.Nodes.Count).Tag = OggSel.IdOggetto
  TRel.Nodes(1).Selected = True
  TRel_NodeClick TRel.Nodes(1)

  Set ExpandedRelactionShips = Nothing
  Set ExpandedRelactionShips = New Collection
  Set ExpandedNodes = Nothing
  Set ExpandedNodes = New Collection
    
  If m_IdOggetto_To_Expand <> -1 Then
    Screen.MousePointer = vbHourglass
    Add_Section_Block_All TRel, m_KeyTreeViewSel, m_IdOggetto_To_Expand, SOLO_OGGETTI_CALLED
    Screen.MousePointer = vbDefault
  End If
End Sub

Private Sub mnuAllCaller_Click()
  TRel.Nodes.Clear
  TRel.Nodes.Add , , "ROOT", OggSel.Nome, 7
  TRel.Nodes(TRel.Nodes.Count).Bold = True
  TRel.Nodes(TRel.Nodes.Count).ForeColor = vbBlue
  TRel.Nodes(TRel.Nodes.Count).Tag = OggSel.IdOggetto
  TRel.Nodes(1).Selected = True
  TRel_NodeClick TRel.Nodes(1)
     
  Set ExpandedRelactionShips = Nothing
  Set ExpandedRelactionShips = New Collection
  Set ExpandedNodes = Nothing
  Set ExpandedNodes = New Collection

  If m_IdOggetto_To_Expand <> -1 Then
    Screen.MousePointer = vbHourglass
    Add_Section_Block_All TRel, m_KeyTreeViewSel, m_IdOggetto_To_Expand, SOLO_OGGETTI_CALLER
    Screen.MousePointer = vbDefault
  End If
End Sub

Private Sub mnuAllRel_Click()
  If m_KeyTreeViewSel = "" Then
    MsgBox "No selected object"
    Exit Sub
  End If
   
  TRel.Nodes.Clear
  TRel.Nodes.Add , , "ROOT", OggSel.Nome, 7
  TRel.Nodes(TRel.Nodes.Count).Bold = True
  TRel.Nodes(TRel.Nodes.Count).ForeColor = vbBlue
  TRel.Nodes(TRel.Nodes.Count).Tag = OggSel.IdOggetto
  TRel.Nodes(1).Selected = True
  TRel_NodeClick TRel.Nodes(1)
   
  Set ExpandedRelactionShips = Nothing
  Set ExpandedRelactionShips = New Collection
  Set ExpandedNodes = Nothing
  Set ExpandedNodes = New Collection
   
  If m_IdOggetto_To_Expand <> -1 Then
    Screen.MousePointer = vbHourglass
    Add_Section_Block TRel, m_KeyTreeViewSel, m_IdOggetto_To_Expand, TUTTI_GLI_OGGETTI
    Screen.MousePointer = vbDefault
  End If
End Sub

Private Sub mnuAutoExp_Click()
  fAutoExpand = Not fAutoExpand
  mnuAutoExp.Checked = fAutoExpand
End Sub

Private Sub mnuCalledObj_Click()
  If m_IdOggetto_To_Expand <> -1 Then
    Screen.MousePointer = vbHourglass
    Add_Section_Block TRel, m_KeyTreeViewSel, m_IdOggetto_To_Expand, SOLO_OGGETTI_CALLED
    Screen.MousePointer = vbDefault
  End If
End Sub

Private Sub mnuCallerobj_Click()
  If m_IdOggetto_To_Expand <> -1 Then
    Screen.MousePointer = vbHourglass
    Add_Section_Block TRel, m_KeyTreeViewSel, m_IdOggetto_To_Expand, SOLO_OGGETTI_CALLER
    Screen.MousePointer = vbDefault
  End If
End Sub

Private Sub mnuEdit_Click()
  'Seleziona l'editor in base al tipo
  'es. CBL --> Text Editor
  '    BMS --> Text Editor / Map Editor
  '    ...
  'Da implementare
  m_fun.FnHeight = Me.Height
  m_fun.Show_TextEditor m_IdOggetto_To_Edit, , , Me 'm_fun.formParent
End Sub

'gloria: genera il txt con albero propriet�
Private Sub mnuGenTxt_Click()
  If m_KeyTreeViewSel = "" Then
    MsgBox "No selected object"
    Exit Sub
  End If
     
  TRel.Nodes.Clear
  TRel.Nodes.Add , , "ROOT", OggSel.Nome, 7
  TRel.Nodes(TRel.Nodes.Count).Bold = True
  TRel.Nodes(TRel.Nodes.Count).ForeColor = vbBlue
  TRel.Nodes(TRel.Nodes.Count).Tag = OggSel.IdOggetto
  TRel.Nodes(1).Selected = True
  TRel_NodeClick TRel.Nodes(1)
   
  Set ExpandedRelactionShips = Nothing
  Set ExpandedRelactionShips = New Collection
  Set ExpandedNodes = Nothing
  Set ExpandedNodes = New Collection
  
  If m_IdOggetto_To_Expand <> -1 Then
    Screen.MousePointer = vbHourglass
    Generate_Txt TRel, m_KeyTreeViewSel, m_IdOggetto_To_Expand, TRel.SelectedItem.Text, TUTTI_GLI_OGGETTI
    Screen.MousePointer = vbDefault
  End If
End Sub

Private Sub mnuGenTxtAllCaller_Click()
  Dim fileName As String
  Dim NumFile As Integer
  Dim testoFile As String, Line As String
   
  If m_KeyTreeViewSel = "" Then
    MsgBox "No selected object"
    Exit Sub
  End If
   
  TRel.Nodes.Clear
  TRel.Nodes.Add , , "ROOT", OggSel.Nome, 7
  TRel.Nodes(TRel.Nodes.Count).Bold = True
  TRel.Nodes(TRel.Nodes.Count).ForeColor = vbBlue
  TRel.Nodes(TRel.Nodes.Count).Tag = OggSel.IdOggetto
  TRel.Nodes(1).Selected = True
  TRel_NodeClick TRel.Nodes(1)
   
  Set ExpandedRelactionShips = Nothing
  Set ExpandedRelactionShips = New Collection
  Set ExpandedNodes = Nothing
  Set ExpandedNodes = New Collection
   
  If m_IdOggetto_To_Expand <> -1 Then
          
    Screen.MousePointer = vbHourglass
    fileName = m_fun.FnPathPrj & "\Documents\TXT\objProp_" & TRel.SelectedItem.Text & "AllCaller.txt"
    If Dir(fileName) <> "" Then
      If MsgBox("File already exists " & fileName & vbCrLf & "Do you want to rewrite it?", vbQuestion + vbYesNo, "i-4.Migration - Object Properties") = vbNo Then Exit Sub
    End If
    
    NumFile = FreeFile ' This is safer than assigning a number
    Open fileName For Output As #NumFile
    Print #NumFile, TRel.SelectedItem.Text
    Set PrintedNodes = New Collection
    Generate_Txt_All m_KeyTreeViewSel, m_IdOggetto_To_Expand, NumFile, TRel.SelectedItem.Text, SOLO_OGGETTI_CALLER
    Screen.MousePointer = vbDefault
    Close #NumFile
      
    Open fileName For Input As #NumFile
    If Not EOF(NumFile) Then
      Line Input #NumFile, Line
      testoFile = Line
      Do Until EOF(NumFile)
        Line Input #NumFile, Line
        testoFile = testoFile & vbCrLf & Line
      Loop
    End If
    Close #NumFile
    
    m_fun.Show_ShowText testoFile, fileName
  End If
End Sub

Private Sub mnuGenTxtAllCalled_Click()
  Dim fileName As String
  Dim NumFile As Integer
  Dim testoFile As String, Line As String
   
  If m_KeyTreeViewSel = "" Then
    MsgBox "No selected object"
    Exit Sub
  End If
   
  TRel.Nodes.Clear
  TRel.Nodes.Add , , "ROOT", OggSel.Nome, 7
  TRel.Nodes(TRel.Nodes.Count).Bold = True
  TRel.Nodes(TRel.Nodes.Count).ForeColor = vbBlue
  TRel.Nodes(TRel.Nodes.Count).Tag = OggSel.IdOggetto
  TRel.Nodes(1).Selected = True
  TRel_NodeClick TRel.Nodes(1)

  Set ExpandedRelactionShips = Nothing
  Set ExpandedRelactionShips = New Collection
  Set ExpandedNodes = Nothing
  Set ExpandedNodes = New Collection
  
  If m_IdOggetto_To_Expand <> -1 Then
    Screen.MousePointer = vbHourglass
    fileName = m_fun.FnPathPrj & "\Documents\TXT\objProp_" & TRel.SelectedItem.Text & "AllCalled.txt"
    If Dir(fileName) <> "" Then
      If MsgBox("File already exists " & fileName & vbCrLf & "Do you want to rewrite it?", vbQuestion + vbYesNo, "i-4.Migration - Object Properties") = vbNo Then Exit Sub
    End If
   
    NumFile = FreeFile ' This is safer than assigning a number
    Open fileName For Output As #NumFile
    Print #NumFile, TRel.SelectedItem.Text
    Set PrintedNodes = New Collection
    Generate_Txt_All m_KeyTreeViewSel, m_IdOggetto_To_Expand, NumFile, TRel.SelectedItem.Text, SOLO_OGGETTI_CALLED
    Screen.MousePointer = vbDefault
    Close #NumFile
       
    Open fileName For Input As #NumFile
    If Not EOF(NumFile) Then
      Line Input #NumFile, Line
      testoFile = Line
      Do Until EOF(NumFile)
        Line Input #NumFile, Line
        testoFile = testoFile & vbCrLf & Line
      Loop
    End If
    Close #NumFile
       
    m_fun.Show_ShowText testoFile, fileName
  End If
End Sub

Private Sub TRel_DblClick()
  If fAutoExpand = True And m_IdOggetto_To_Expand <> -1 Then
    'Espande le relazioni in automatico
    mnuAllRel_Click
  End If
End Sub

Private Sub TRel_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = vbRightButton Then
    If m_IdOggetto_To_Edit <> -1 Then
      If m_IdOggetto_To_Expand = -1 Then
        mnuExpRel.Visible = False
      Else
        mnuExpRel.Visible = True
      End If
      PopupMenu mnuPopUpEdit
    Else
      PopupMenu mnuExpRel
    End If
  End If
End Sub

Private Sub TRel_NodeClick(ByVal Node As MSComctlLib.Node)
  m_IdOggetto_To_Expand = -1
  If Trim(Node.Tag) <> "" And Node.Tag <> -1 Then
    m_fun.FnIdOggetto = Node.Tag
    m_IdOggetto_To_Edit = m_fun.FnIdOggetto
    m_IdOggetto_To_Expand = Node.Tag
  Else
    m_IdOggetto_To_Edit = -1
    m_IdOggetto_To_Expand = -1
  End If
  
  m_KeyTreeViewSel = Node.key
End Sub
