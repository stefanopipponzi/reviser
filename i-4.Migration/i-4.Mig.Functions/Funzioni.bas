Attribute VB_Name = "Funzioni"
Option Explicit
'SQ Product Name
Public Const PRODUCT_NAME = "i-4.Migration"

'API
Public Const MAX_PATH = 260
Public Const ERROR_NO_MORE_FILES = 18
'Public Const FILE_ATTRIBUTE_DIRECTORY = &H10
Public Const FILE_ATTRIBUTE_NORMAL = &H80


Public Declare Function GetWindowsDirectory Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Public Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long

Public Declare Function SetFocusWnd Lib "user32.dll" Alias "SetFocus" (ByVal hwnd As Long) As Long
  
Public Type FILETIME
  dwLowDateTime As Long
  dwHighDateTime As Long
End Type

Public Type WIN32_FIND_DATA
  dwFileAttributes As Long
  ftCreationTime As FILETIME
  ftLastAccessTime As FILETIME
  ftLastWriteTime As FILETIME
  nFileSizeHigh As Long
  nFileSizeLow As Long
  dwReserved0 As Long
  dwReserved1 As Long
  cFileName As String * MAX_PATH
  cAlternate As String * 14
End Type

Public Declare Function DeleteFile Lib "kernel32.dll" Alias "DeleteFileA" (ByVal lpFileName As String) As Long

Public Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" _
    (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
Public Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long

Public Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" _
    (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long
    
Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long

Public Type SECURITY_ATTRIBUTES
   nLength As Long
   lpSecurityDescriptor As Long
   bInheritHandle As Long
End Type

''''Public Const SW_SHOW = 1
''''Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
''''                        (ByVal hwnd As Long, ByVal lpOperation As String, _
''''                        ByVal lpFile As String, ByVal lpParameters As String, _
''''                        ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Public Declare Function CreateDirectory Lib "kernel32" Alias "CreateDirectoryA" (ByVal lpPathName As String, lpSecurityAttributes As SECURITY_ATTRIBUTES) As Long
'Elimina effetto refresh di un oggetto
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hWndLock As Long) As Long

'variabili per crc
Public blnTabellaCalcolata As Boolean
Public lngTabellaCRC(255) As Long

Public Const lngSemeCRC As Long = &HEDB88320
Public Const lngDefaultCRC As Long = &HFFFFFFFF

Dim NumOpen As Double
Public mCnt As New ADODB.Connection
Public mCntSys As New ADODB.Connection

Public m_Top As Single
Public m_Left As Single
Public m_Width As Single
Public m_Height As Single
Public m_FnHeightNoFinImm As Long

Public m_fun As MaFndC_Funzioni

Public Function TN(Campo As ADODB.Field) As String
  Dim v As String
   
  If IsNull(Campo.Value) Then
    v = ""
  Else
    v = Campo.Value
  End If
  TN = v
End Function

Public Function Restituisci_Oggetto_Da_ID(Id As Long) As String
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Id)
  If r.RecordCount Then
    Restituisci_Oggetto_Da_ID = TN(r!Nome)
  End If
  r.Close
End Function

Function StripTerminator(ByVal strString As String) As String
  Dim intZeroPos As Integer
  
  intZeroPos = InStr(strString, Chr$(0))
  If intZeroPos > 0 Then
    StripTerminator = Left$(strString, intZeroPos - 1)
  Else
    StripTerminator = strString
  End If
End Function

Public Function Carica_Proprieta_Copy(Id As Long) As clsCopy
  Dim rs As Recordset
  Dim cpy As New clsCopy
    
  'Implementare
  cpy.Id = Id
  Set rs = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Id)
  If rs.RecordCount Then
    cpy.Nome = TN(rs!Nome)
    'Deve prendere le altre proprietÓ
  End If
  rs.Close
  Set Carica_Proprieta_Copy = cpy
End Function

Public Function Carica_Proprieta_Mappa(Id As Long) As clsMappe
  Dim rs As Recordset
  Dim Map As New clsMappe
    
  'Implementare
  Map.Id = Id
  
  Set rs = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Id)
  If rs.RecordCount > 0 Then
    Map.Nome = TN(rs!Nome)
    'Deve prendere le altre proprietÓ
  End If
  rs.Close
  
  Set Carica_Proprieta_Mappa = Map
End Function

Public Function Carica_Proprieta_Psb(Id As Long) As clsPsb
  Dim rs As Recordset
  Dim psb As New clsPsb
    
  'Implementare
  psb.Id = Id
  
  Set rs = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Id)
  If rs.RecordCount > 0 Then
    psb.Nome = TN(rs!Nome)
    'Deve Prendere le altre proprietÓ
  End If
  rs.Close
  Set Carica_Proprieta_Psb = psb

End Function
   
Public Function Carica_Proprieta_Programma(Id As Long) As clsPgm
  Dim rs As Recordset
  Dim Pgm As New clsPgm
    
  'Passando l'id e il nome dell'oggetto la funzione riempie la classe con i caller e i called ecc....
  'Controlla Id e Nome
  
  Set rs = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Id)
  If rs.RecordCount > 0 Then
    Pgm.Id = Id
    Pgm.Nome = TN(rs!Nome)
    Pgm.nRighe = IIf(IsNull(rs!NumRighe), 0, rs!NumRighe)
    Pgm.Tipo = TN(rs!Tipo)
    
    'Assegna l'attributo
    Pgm.Attributo = Undefined
    If TN(rs!Cics) = True Then Pgm.Attributo = Cics
    If TN(rs!Batch) = True Then Pgm.Attributo = Batch
    If TN(rs!withSql) = True Then Pgm.Attributo = Sql
    If TN(rs!VSam) = True Then Pgm.Attributo = VSam
    If TN(rs!DLI) = True Then Pgm.Attributo = DLI
    If TN(rs!Mapping) = True Then Pgm.Attributo = Mapping
    If TN(rs!FileSection) = True Then Pgm.Attributo = FileSection
    If TN(rs!Working) = True Then Pgm.Attributo = Working
    If TN(rs!Procedure) = True Then Pgm.Attributo = Procedure
  End If
  
  rs.Close
  
  Set rs = Nothing
  
  Set Carica_Proprieta_Programma = Pgm
    
End Function

Public Sub InitTab()
Dim intBytes As Integer
Dim intConta As Integer
Dim lngTmpCRC As Long
Dim lngValoreCRC As Long

  For intBytes = 0 To 255
    lngValoreCRC = intBytes
    For intConta = 0 To 7
      lngTmpCRC = (lngValoreCRC And &HFFFFFFFE) \ 2 And &H7FFFFFFF
      If lngValoreCRC And &H1 Then
        lngValoreCRC = lngTmpCRC Xor lngSemeCRC
      Else
        lngValoreCRC = lngTmpCRC
      End If
    Next intConta
    lngTabellaCRC(intBytes) = lngValoreCRC
  Next intBytes
  blnTabellaCalcolata = True
End Sub

Public Function CalcolaCRC(ByVal Buffer As String, Optional ByVal CRCPrecedente As Long = lngDefaultCRC, Optional UltimoCRC As Boolean = False) As Long
Dim Carattere As Byte
Dim Conta As Integer
Dim Temp As Long
Dim IndiceTabellaCRC As Long
    
  If blnTabellaCalcolata = False Then InitTab
  CalcolaCRC = CRCPrecedente
  For Conta = 1 To Len(Buffer)
    Carattere = Asc(mId$(Buffer, Conta, 1))
    Temp = (CalcolaCRC And &HFFFFFF00) \ &H100 And &HFFFFFF
    IndiceTabellaCRC = Carattere Xor (CalcolaCRC And &HFF)
    CalcolaCRC = Temp Xor lngTabellaCRC(IndiceTabellaCRC)
  Next Conta
  If UltimoCRC = True Then CalcolaCRC = Not CalcolaCRC
End Function
