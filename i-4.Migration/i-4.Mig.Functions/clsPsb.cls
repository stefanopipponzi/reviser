VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPsb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_Id As Long
Private m_Nome As String
Private m_PCB As Variant
Private m_Dbd As String

Public Property Let Id(ByVal mId As Long)
   m_Id = mId
End Property

Public Property Get Id() As Long
   Id = m_Id
End Property

Public Property Let Nome(ByVal mNome As String)
   m_Nome = mNome
End Property

Public Property Get Nome() As String
   Nome = m_Nome
End Property

Public Property Let PCB(ByVal mPCB As Variant)
   m_PCB = mPCB
End Property

Public Property Get PCB() As Variant
   PCB = m_PCB
End Property

Public Property Let Dbd(ByVal mDbd As String)
   m_Dbd = mDbd
End Property

Public Property Get Dbd() As String
   Dbd = m_Dbd
End Property



