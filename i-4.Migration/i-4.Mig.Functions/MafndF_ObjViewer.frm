VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form MafndF_ObjViewer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selected Copy source code"
   ClientHeight    =   6570
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9420
   Icon            =   "MafndF_ObjViewer.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6570
   ScaleWidth      =   9420
   StartUpPosition =   2  'CenterScreen
   Begin RichTextLib.RichTextBox RTCopyLoad 
      Height          =   6555
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9405
      _ExtentX        =   16589
      _ExtentY        =   11562
      _Version        =   393217
      BackColor       =   16777152
      ScrollBars      =   3
      TextRTF         =   $"MafndF_ObjViewer.frx":0442
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "MafndF_ObjViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private idxPos As Long
Private parola As String
Private FindMatch As Boolean
Private FindWhole As Boolean

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  Dim wFound As Boolean
  Dim r As Recordset
  Dim Posizione As Long
  
  idxPos = idxPos + Len(parola)
  wFound = False
  
  If idxPos > -1 Then
    wFound = True
  End If
  
  If KeyCode = vbKeyF3 Then
    If FindMatch Then
      If FindWhole Then
        'True/True
        idxPos = RTCopyLoad.Find(parola, idxPos, , rtfWholeWord And rtfMatchCase)
      Else
        'True/False
        idxPos = RTCopyLoad.Find(parola, idxPos, , rtfMatchCase)
      End If
    Else
      If FindWhole = True Then
        'False/True
        idxPos = RTCopyLoad.Find(parola, idxPos, , rtfWholeWord)
      Else
        'False/False
        idxPos = RTCopyLoad.Find(parola, idxPos)
      End If
    End If
  ElseIf KeyCode = vbKeyF5 Then
    Set r = m_fun.Open_Recordset("SELECT * FROM AN_Semi WHERE Codice = '" & m_fun.codiceOggetto & "'")
    If r.RecordCount Then
      Posizione = FindDefinition(RTCopyLoad, r!Struttura, r!Campo)
    End If
    r.Close
  End If
  
  If idxPos = -1 Then
    If wFound = True Then
      MsgBox "Search terminated...", vbInformation, m_fun.FnNomeProdotto
    End If
   
    idxPos = 1
    RTCopyLoad.SelStart = 0
    RTCopyLoad.SelLength = 0
  End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
  Dim oFindForm As MafndF_TxtFind
  
  If KeyAscii = 6 Then  'CTRL+F
    Set oFindForm = New MafndF_TxtFind
    
    oFindForm.Show vbModal
    
    idxPos = 1
    parola = msSearchString
    
    FindMatch = oFindForm.mbFindMatch
    FindWhole = oFindForm.mbFindWhole
        
    Set oFindForm = Nothing
        
    If Trim(parola) <> vbNullString Then
      If FindMatch = True Then
        If FindWhole = True Then
          'True/True
          idxPos = RTCopyLoad.Find(parola, idxPos, , rtfWholeWord And rtfMatchCase)
        Else
          'True/False
          idxPos = RTCopyLoad.Find(parola, idxPos, , rtfMatchCase)
        End If
      Else
        If FindWhole = True Then
          'False/True
          idxPos = RTCopyLoad.Find(parola, idxPos, , rtfWholeWord)
        Else
          'False/False
          idxPos = RTCopyLoad.Find(parola, idxPos)
        End If
      End If
      
      If idxPos = -1 Then
        MsgBox "Word not found...", vbExclamation, m_fun.FnNomeProdotto
      End If
       
      RTCopyLoad.SetFocus
    End If
  End If
End Sub

Private Sub Form_Resize()
  RTCopyLoad.Width = Me.ScaleWidth
  RTCopyLoad.Height = Me.ScaleHeight
  RTCopyLoad.Top = Me.ScaleTop
  RTCopyLoad.Left = Me.ScaleLeft
End Sub

Public Sub loadForm(IdOggetto As Long, nomeOggetto As String)
  Dim rs As Recordset, Name As String
  
  On Error GoTo fileErr
  
  RTCopyLoad.SelStart = 0
  RTCopyLoad.SelLength = 1
  RTCopyLoad.SelColor = vbBlue
  RTCopyLoad.SelLength = 0
  
  Set rs = m_fun.Open_Recordset("Select nome,Directory_Input,estensione From BS_Oggetti Where " & _
                                "IdOggetto = " & IdOggetto)
  If rs.RecordCount Then
    Me.Caption = "Object - " & rs!Nome & "  IDobject - " & IdOggetto
    Name = m_fun.Crea_Directory_Progetto(rs!Directory_Input, m_fun.FnPathDef) & "\" & rs!Nome
    If Len(rs!Estensione) Then
      Name = Name & "." & rs!Estensione
    End If
  End If
  rs.Close
  
  RTCopyLoad.LoadFile Name
  
  parola = nomeOggetto
  If Len(parola) Then
    idxPos = RTCopyLoad.Find(parola, 1)
  End If
   
  Me.Show
  Exit Sub
fileErr:
  If ERR.Number = 75 Then
    MsgBox "File " & Name & " not found.", vbCritical, m_fun.FnNomeProdotto
  Else
    MsgBox ERR.Description, vbExclamation
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 19-09-07
'Cerco la riga in cui un campo viene definito
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function FindDefinition(RT As RichTextBox, Struttura As String, Campo As String) As Long
  Dim wStr As Long
  Dim wCampo As Long
  Dim wStr1 As Long

  wStr = RT.Find(" " & Struttura, 1)
  Do While wStr <> -1
    wStr1 = Controlla_Se_Parola_VALIDA(RT, wStr, Struttura)
    If wStr1 = -1 Then
      wStr = RT.Find(" " & Struttura, wStr + Len(" " & Struttura))
    Else
      Exit Do
    End If
  Loop

  wStr = wStr1
  If wStr <> -1 Then
    'cerca la posizione del campo
    wCampo = RT.Find(" " & Campo & " ", wStr)
    If wCampo = -1 Then
      wCampo = RT.Find(" " & Campo, wStr)
    End If
  End If

  If wCampo <> -1 And wCampo <> 0 Then
    FindDefinition = wCampo
  Else
    FindDefinition = 0
  End If
End Function

'controlla se dopo aver trovato una struttura il byte successivo � uno spazio o un punto
'in questo caso la struttura cercata � quella giusta. questo perch� se si cerca a priori
'una struttura fra due spazi sicuramente tutte quelle che hanno dei campi sottodefiniti
' e quindi hanno un punto attaccato non verranno trovate
'La funzione restituisce direttamente il valore -1 restituito dalla rich quando non trova
'nessuna occorrenza, oppure restituisce la posizione di input rendendola quindi valida
Function Controlla_Se_Parola_VALIDA(RT As RichTextBox, Posizione As Long, parola As String) As Long
  Dim Front As Boolean
  Dim Rear As Boolean
  Dim i As Long
  Dim Vbc As Long
  Dim PosLevel As Long
  Dim level As Boolean
  Dim Livello As String

  RT.SelStart = Posizione + Len(parola) + 1
  RT.SelLength = 1

  If RT.selText = "." Or RT.selText = " " Then
    Front = True
  Else
    Front = False
  End If

  If Front = True Then
    'continua con la verifica della parte iniziale della struttura
    For i = 1 To 1000
      If i > Posizione Then
        Exit For
      End If
      RT.SelStart = Posizione - i
      RT.SelLength = 2
      If RT.selText = vbCrLf Then
        Vbc = Posizione - i + 1
        Exit For
      End If
    Next i

    PosLevel = RT.Find(" 01 ", Vbc)

    If PosLevel = -1 Then
      PosLevel = RT.Find(" 77 ", Vbc)

      If PosLevel = -1 Then
        PosLevel = RT.Find(" 88 ", Vbc)

        If PosLevel = -1 Then
          'cerca il livello della struttura che non � nessuno dei tre cercati
          'in precedenza
          Livello = ""
          level = False

          For i = 1 To 1000
            RT.SelStart = Posizione - i
            RT.SelLength = 1

            If RT.selText <> " " Then
              level = True
              Livello = RT.selText & Livello
            Else
              If level = True And RT.selText = " " Then
                Exit For
              End If
            End If
          Next i

          If IsNumeric(Val(Livello)) Then
            PosLevel = Posizione - i
          Else
            'BOHHH????!?!?!?!?!?
            PosLevel = -1
          End If
        End If
      End If
    End If

    If PosLevel <> -1 Then
      Rear = True
    Else
      Rear = False
    End If
  Else
    Controlla_Se_Parola_VALIDA = -1
  End If

  If Rear And Front Then
    Controlla_Se_Parola_VALIDA = Posizione
  Else
    Controlla_Se_Parola_VALIDA = -1
  End If
End Function
