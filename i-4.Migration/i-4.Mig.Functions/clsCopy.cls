VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCopy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_Id As Long
Private m_Nome As String
Private m_Livello As Variant
Private m_Campi As New Collection

Public Property Let Id(ByVal Id As Long)
   m_Id = Id
End Property

Public Property Get Id() As Long
   Id = m_Id
End Property

Public Property Let Nome(ByVal Nome As String)
   m_Nome = Nome
End Property

Public Property Get Nome() As String
   Nome = m_Nome
End Property


