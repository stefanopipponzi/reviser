VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MacroObjects"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"MacroObject"
Attribute VB_Ext_KEY = "Member0" ,"MacroObject"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Dim RegEx As New RegExp
'Variabile locale per memorizzare l'insieme.
Private mCol As Collection

Public Function Add(key As String, Optional sKey As String) As MacroObject
  'crea un nuovo oggetto
  Dim objNewMember As MacroObject
  Set objNewMember = New MacroObject

  'imposta le propriet� passate al metodo
  objNewMember.key = key
  If Len(sKey) = 0 Then
    mCol.Add objNewMember
  Else
    mCol.Add objNewMember, sKey
  End If
  
  'restituisce l'oggetto creato
  Set Add = objNewMember
  Set objNewMember = Nothing
End Function

Public Property Get Item(vntIndexKey As Variant) As MacroObject
Attribute Item.VB_UserMemId = 0
  'Utilizzato per fare riferimento a un elemento nell'insieme.
  'vntIndexKey contiene la chiave o l'indice dell'insieme,
  'e per questo motivo � dichiarata come Variant.
  'Sintassi: Set foo = x.Item(xyz) oppure Set foo = x.Item(5)
  Set Item = mCol(vntIndexKey)
End Property

Public Property Get Count() As Long
  'Utilizzata per recuperare il numero di elementi dell'insieme.
  'Sintassi: Debug.Print x.Count
  Count = mCol.Count
End Property

Public Sub Remove(vntIndexKey As Variant)
  'Utilizzata per rimuovere un elemento dall'insieme.
  'vntIndexKey contiene l'indice o la chiave, e per questo
  'motivo viene dichiarata come Variant.
  'Sintassi: x.Remove(xyz)
  mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
  'Questa propriet� consente di enumerare l'insieme
  'corrente con la sintassi For...Each.
  Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
  'Crea l'insieme quando viene creata questa classe.
  Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
  'Rimuove l'insieme quando la classe viene eliminata.
  Set mCol = Nothing
End Sub

Public Sub CreateEnvironment(iObjectID As Long, sMacroName As String)
  Dim K As Variant
  
  '    Add CStr(iObjectID)
  K = MOs.Count

  ExecuteMacro iObjectID, sMacroName, 0

  K = MOs.Count
End Sub

Public Function WriteLog(Stringa, Optional EchoLog As Boolean)
  MafndF_RunMacro.RtBVb.selText = Stringa & vbCrLf
  MafndF_RunMacro.RtBVb.Refresh
  
  If IsMissing(EchoLog) Then Exit Function
  If EchoLog = False Then Exit Function
  
  m_fun.WriteLog CStr(Stringa), "Tool(Macro)"
End Function

Public Function FindRegExp(Source As Variant, Expression As Variant, IgnoreCase As Variant) As Variant
  RegEx.Pattern = Expression
  RegEx.IgnoreCase = IgnoreCase
  RegEx.Global = True
  
  Set FindRegExp = RegEx.Execute(Source)
End Function

Public Function Abort()
  MOs(Me.Count).Abort = True
End Function

Public Function FindPos(StringToFind As String, iStart As Integer) As Long
  FindPos = MafndF_Macro.RTBFind.Find(StringToFind, iStart)
End Function

Public Function GetSubStr(Buffer As String, Tag As String) As String
  Dim wBuffer As String, wTag As String
  Dim wSelBuf As String, wStr As String
  Dim wStrNext As String, K As Long
  Dim SwStart As Boolean, SwFine As Boolean
   
  wSelBuf = ""
  wBuffer = Buffer & vbCrLf
  wTag = Tag
  If InStr(wBuffer, wTag) = 0 Then
    GetSubStr = wSelBuf
    Exit Function
  End If
  K = InStr(wBuffer, vbCrLf)
  
  SwStart = False
  SwFine = False
  
  While K > 0
    wStr = Left(wBuffer, K - 1)
    wBuffer = mId(wBuffer, K + 2)
    K = InStr(wBuffer, vbCrLf)
    If K > 0 Then
      wStrNext = Left(wBuffer, K - 1)
    Else
      wStrNext = ""
    End If
    
    If Not SwStart Then
      If InStr(wStr, wTag) > 0 Then
        SwStart = True
      End If
    End If
      
    If SwStart Then
      wSelBuf = wSelBuf & wStr & vbCrLf
      If BS_Obj_Type = "JCL" Or BS_Obj_Type = "JOB" Or BS_Obj_Type = "PRC" Then
        wStr = Trim(Left(wStr & Space(72), 72))
        'SQ - madrid: da errore il mid se la stringa � vuota!
        If Len(wStr) Then
          If mId(wStr, Len(wStr), 1) <> "," Then
            If Left(wSelBuf, 8) = "//SYSIN " Or mId$(wSelBuf, 1, 10) = "//SYSTSIN " Then
              If Left(wStrNext & "  ", 2) = "//" Or Left(wStrNext & "  ", 2) = "/*" Then
                SwFine = True
              End If
            Else
              SwFine = True
            End If
          End If
        End If
      ElseIf BS_Obj_Type = "CBL" Or BS_Obj_Type = "CPY" Then
        If mId$(wStrNext & Space$(7), 7, 1) <> "-" Then
          SwFine = True
        End If
      ElseIf BS_Obj_Type = "MAC" Or BS_Obj_Type = "ASM" Or BS_Obj_Type = "BMS" Or BS_Obj_Type = "MFS" Then
        If mId$(wStr & Space$(72), 72, 1) = " " Then
          SwFine = True
        End If
      Else
        SwFine = True
      End If
      If SwFine Then
        K = 0
      End If
    End If
  Wend
  If mId$(wSelBuf, Len(wSelBuf) - 1, 2) = vbCrLf Then
    wSelBuf = mId$(wSelBuf, 1, Len(wSelBuf) - 2)
  End If
  GetSubStr = wSelBuf
End Function

Public Function GetSysin(Buffer As String) As String
  Dim Rows() As String
  Dim i As Integer
   
  GetSysin = ""
  Rows() = Split(Buffer, vbCrLf)
  For i = 0 To UBound(Rows())
    If InStr(Rows(i), " EXEC ") And Left(Rows(i), 2) = "//" Then
      i = i + 1
      Do Until InStr(Rows(i), "/*")
        GetSysin = GetSysin & Rows(i) & vbCrLf
        i = i + 1
      Loop
      Exit For
    End If
  Next i
End Function

Public Function GetLineFromPos(CharPosition As Long) As Integer
  If CharPosition > -1 Then
    MafndF_Macro.RTBFind.SelStart = 0
    GetLineFromPos = MafndF_Macro.RTBFind.GetLineFromChar(CharPosition) + 1
  End If
End Function

Public Function GetCurrentRTBFileName() As String
  GetCurrentRTBFileName = MafndF_Macro.RTBFind.fileName
End Function

Public Function ReplaceString(Expression As String, Find As String, Replacement As String) As String
  ReplaceString = Replace(Expression, Find, Replacement)
End Function

Public Function OpenFile(NameFile As String, Modality As String) As String
  Dim Riga As String
  Dim booOpen As Boolean
  Dim NumFile As Integer
  NumFile = FreeFile

  booOpen = True
  Select Case UCase(Modality)
    Case "APPEND"
      Open NameFile For Append As NumFile
    Case "BINARY"
      Open NameFile For Binary As NumFile
    Case "INPUT"
      Open NameFile For Input As NumFile
    Case "OUTPUT"
      Open NameFile For Output As NumFile
    Case "RANDOM"
      Open NameFile For Random As NumFile
    Case Else
      OpenFile = ""
      booOpen = False
  End Select
  If booOpen Then
    Do Until EOF(NumFile)
      Line Input #NumFile, Riga
      OpenFile = OpenFile & vbCrLf & Riga
    Loop
    Close NumFile
  End If
End Function

Public Function rsOpen(Sql As String) As Recordset
  Set rsOpen = m_fun.Open_Recordset(Sql)
End Function

' Non posso usare il tipo Recordset in ingresso della routine
' Mi tocca cambiarlo in Variant
Public Sub rsMoveFirst(ByVal Sql As Variant)
  Sql.MoveFirst
End Sub

' Non posso usare il tipo Recordset in ingresso della routine
' Mi tocca cambiarlo in Variant
Public Sub rsMovePrev(ByVal Sql As Variant)
  Sql.MovePrevious
End Sub

' Non posso usare il tipo Recordset in ingresso della routine
' Mi tocca cambiarlo in Variant
Public Sub rsMoveNext(ByVal Sql As Variant)
  Sql.MoveNext
End Sub

' Non posso usare il tipo Recordset in ingresso della routine
' Mi tocca cambiarlo in Variant
Public Sub rsMoveLast(ByVal Sql As Variant)
  Sql.MoveLast
End Sub

' Non posso usare il tipo Recordset in ingresso della routine
' Mi tocca cambiarlo in Variant
Public Sub rsClose(ByVal Sql As Variant)
  Sql.Close
End Sub

' Non posso usare il tipo Recordset in ingresso della routine
' Mi tocca cambiarlo in Variant
Public Function rsRecordCount(Sql As Variant) As Long
  rsRecordCount = Sql.RecordCount
End Function

' Non posso usare il tipo Recordset in ingresso della routine
' Mi tocca cambiarlo in Variant
Public Function rsFieldValue(Sql As Variant, Field As String) As String
  rsFieldValue = Sql.Fields(Field) & ""
End Function

Public Sub rsExecute(Sql As String)
  m_fun.FnConnection.Execute Sql
End Sub

' Public Macro Variables
Public Property Let BS_Obj_RecArea(ByVal vData As Variant)
  Me(Me.Count).BS_Obj_RecArea = vData
End Property

Public Property Get BS_Obj_RecArea() As Variant
  BS_Obj_RecArea = Me(Me.Count).BS_Obj_RecArea
End Property

Public Property Let Param_DefValueChar(ByVal vData As Variant)
  Me(Me.Count).Param_DefValueChar = vData
End Property

Public Property Get Param_DefValueChar() As Variant
  Param_DefValueChar = Me(Me.Count).Param_DefValueChar
End Property

Public Property Let Param_DefValueNum(ByVal vData As Variant)
  Me(Me.Count).Param_DefValueNum = vData
End Property

Public Property Get Param_DefValueNum() As Variant
  Param_DefValueNum = Me(Me.Count).Param_DefValueNum
End Property

Public Property Let Param_DefValueDate(ByVal vData As Variant)
    Me(Me.Count).Param_DefValueDate = vData
End Property

Public Property Get Param_DefValueDate() As Variant
   Param_DefValueDate = Me(Me.Count).Param_DefValueDate
End Property

Public Property Let Param_DefValueTime(ByVal vData As Variant)
    Me(Me.Count).Param_DefValueTime = vData
End Property

Public Property Get Param_DefValueTime() As Variant
   Param_DefValueTime = Me(Me.Count).Param_DefValueTime
End Property

Public Property Let Param_DefValueTimeStamp(ByVal vData As Variant)
    Me(Me.Count).Param_DefValueTimeStamp = vData
End Property

Public Property Get Param_DefValueTimeStamp() As Variant
   Param_DefValueTimeStamp = Me(Me.Count).Param_DefValueTimeStamp
End Property

Public Property Let Param_FormDateDB2(ByVal vData As Variant)
    Me(Me.Count).Param_FormDateDB2 = vData
End Property

Public Property Get Param_FormDateDB2() As Variant
   Param_FormDateDB2 = Me(Me.Count).Param_FormDateDB2
End Property

Public Property Let Param_FormDateOracle(ByVal vData As Variant)
    Me(Me.Count).Param_FormDateOracle = vData
End Property

Public Property Get Param_FormDateOracle() As Variant
   Param_FormDateOracle = Me(Me.Count).Param_FormDateOracle
End Property

' Public Macro Variables
Public Property Let BS_Obj_Selected(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Selected = vData
End Property

Public Property Get BS_Obj_Selected() As Variant
   BS_Obj_Selected = Me(Me.Count).BS_Obj_Selected
End Property

Public Property Let BS_Obj_CurrentLine(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_CurrentLine = vData
End Property

Public Property Get BS_Obj_CurrentLine() As Variant
   BS_Obj_CurrentLine = Me(Me.Count).BS_Obj_CurrentLine
End Property

' Current Index is READONLY
Public Property Get BS_Obj_CurrentIndex() As Variant
   BS_Obj_CurrentIndex = Me.Count
End Property

' BS_Oggetti
Public Property Let BS_Obj_ObjectID(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_ObjectID = vData
End Property

Public Property Get BS_Obj_ObjectID() As Variant
   BS_Obj_ObjectID = Me(Me.Count).BS_Obj_ObjectID
End Property

Public Property Let BS_Obj_Path(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Path = vData
End Property

Public Property Get BS_Obj_Path() As Variant
   BS_Obj_Path = Me(Me.Count).BS_Obj_Path
End Property

Public Property Let BS_Obj_Name(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Name = vData
End Property

Public Property Get BS_Obj_Name() As Variant
   BS_Obj_Name = Me(Me.Count).BS_Obj_Name
End Property

Public Property Let BS_Obj_Extension(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Extension = vData
End Property

Public Property Get BS_Obj_Extension() As Variant
   BS_Obj_Extension = Me(Me.Count).BS_Obj_Extension
End Property

Public Property Let BS_Obj_RowNum(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_RowNum = vData
End Property

Public Property Get BS_Obj_RowNum() As Variant
   BS_Obj_RowNum = Me(Me.Count).BS_Obj_RowNum
End Property

Public Property Let BS_Obj_Type(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Type = vData
End Property

Public Property Get BS_Obj_Type() As Variant
   BS_Obj_Type = Me(Me.Count).BS_Obj_Type
End Property

Public Property Let BS_Obj_Type_DBD(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Type_DBD = vData
End Property

Public Property Get BS_Obj_Type_DBD() As Variant
   BS_Obj_Type_DBD = Me(Me.Count).BS_Obj_Type_DBD
End Property

Public Property Let BS_Obj_Type_JCL(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Type_JCL = vData
End Property

Public Property Get BS_Obj_Type_JCL() As Variant
   BS_Obj_Type_JCL = Me(Me.Count).BS_Obj_Type_JCL
End Property

Public Property Let BS_Obj_BelongsToArea(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_BelongsToArea = vData
End Property

Public Property Get BS_Obj_BelongsToArea() As Variant
   BS_Obj_BelongsToArea = Me(Me.Count).BS_Obj_BelongsToArea
End Property

Public Property Let BS_Obj_Level1(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Level1 = vData
End Property

Public Property Get BS_Obj_Level1() As Variant
   BS_Obj_Level1 = Me(Me.Count).BS_Obj_Level1
End Property
    
Public Property Let BS_Obj_Level2(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Level2 = vData
End Property

Public Property Get BS_Obj_Level2() As Variant
   BS_Obj_Level2 = Me(Me.Count).BS_Obj_Level2
End Property
    
Public Property Let BS_Obj_Available(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Available = vData
End Property

Public Property Get BS_Obj_Available() As Variant
   BS_Obj_Available = Me(Me.Count).BS_Obj_Available
End Property

Public Property Let BS_Obj_Directory_Input(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Directory_Input = vData
End Property

Public Property Get BS_Obj_Directory_Input() As Variant
   BS_Obj_Directory_Input = Me(Me.Count).BS_Obj_Directory_Input
End Property

Public Property Let BS_Obj_Directory_Output(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Directory_Output = vData
End Property

Public Property Get BS_Obj_Directory_Output() As Variant
   BS_Obj_Directory_Output = Me(Me.Count).BS_Obj_Directory_Output
End Property

Public Property Let BS_Obj_Cics(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Cics = vData
End Property

Public Property Get BS_Obj_Cics() As Variant
   BS_Obj_Cics = Me(Me.Count).BS_Obj_Cics
End Property

Public Property Let BS_Obj_Ims(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Ims = vData
End Property

Public Property Get BS_Obj_Ims() As Variant
   BS_Obj_Ims = Me(Me.Count).BS_Obj_Ims
End Property
    
Public Property Let BS_Obj_Batch(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Batch = vData
End Property

Public Property Get BS_Obj_Batch() As Variant
   BS_Obj_Batch = Me(Me.Count).BS_Obj_Batch
End Property

Public Property Let BS_Obj_sql(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_sql = vData
End Property

Public Property Get BS_Obj_sql() As Variant
   BS_Obj_sql = Me(Me.Count).BS_Obj_sql
End Property

Public Property Let BS_Obj_Dli(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Dli = vData
End Property

Public Property Get BS_Obj_Dli() As Variant
   BS_Obj_Dli = Me(Me.Count).BS_Obj_Dli
End Property

Public Property Let BS_Obj_Mapping(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Mapping = vData
End Property

Public Property Get BS_Obj_Mapping() As Variant
   BS_Obj_Mapping = Me(Me.Count).BS_Obj_Mapping
End Property

Public Property Let BS_Obj_FileSection(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_FileSection = vData
End Property

Public Property Get BS_Obj_FileSection() As Variant
   BS_Obj_FileSection = Me(Me.Count).BS_Obj_FileSection
End Property
     
Public Property Let BS_Obj_Working(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Working = vData
End Property

Public Property Get BS_Obj_Working() As Variant
   BS_Obj_Working = Me(Me.Count).BS_Obj_Working
End Property
     
Public Property Let BS_Obj_Procedure(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_Procedure = vData
End Property

Public Property Get BS_Obj_Procedure() As Variant
   BS_Obj_Procedure = Me(Me.Count).BS_Obj_Procedure
End Property
     
Public Property Let BS_Obj_DtImport(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_DtImport = vData
End Property

Public Property Get BS_Obj_DtImport() As Variant
    BS_Obj_DtImport = Me(Me.Count).BS_Obj_DtImport
End Property
    
Public Property Let BS_Obj_DtParsing(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_DtParsing = vData
End Property

Public Property Get BS_Obj_DtParsing() As Variant
   BS_Obj_DtParsing = Me(Me.Count).BS_Obj_DtParsing
End Property
    
Public Property Let BS_Obj_ParsingLevel(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_ParsingLevel = vData
End Property

Public Property Get BS_Obj_ParsingLevel() As Variant
   BS_Obj_ParsingLevel = Me(Me.Count).BS_Obj_ParsingLevel
End Property
    
Public Property Let BS_Obj_DtEncapsulated(ByVal vData As Variant)
    Me(Me.Count).BS_Obj_DtEncapsulated = vData
End Property

Public Property Get BS_Obj_DtEncapsulated() As Variant
   BS_Obj_DtEncapsulated = Me(Me.Count).BS_Obj_DtEncapsulated
End Property

' Relationship Count is READONLY
Public Property Get PsRel_ObjCalled_Count() As Variant
   PsRel_ObjCalled_Count = Me(Me.Count).MacroRelCalledObjects.Count
End Property

' PsRel_Obj
Public Property Let PsRel_ObjCalled_ID(Index As Integer, ByVal vData As Variant)
    Me(Me.Count).MacroRelCalledObjects(Index).PsRel_ObjCalled_ID = vData
End Property

Public Property Get PsRel_ObjCalled_ID(Index As Integer) As Variant
    PsRel_ObjCalled_ID = Me(Me.Count).MacroRelCalledObjects(Index).PsRel_ObjCalled_ID
End Property

Public Property Let PsRel_ObjCalled_ParentID(Index As Integer, ByVal vData As Variant)
    Me(Me.Count).MacroRelCalledObjects(Index).PsRel_ObjCalled_ParentID = vData
End Property

Public Property Get PsRel_ObjCalled_ParentID(Index As Integer) As Variant
    PsRel_ObjCalled_ParentID = Me(Me.Count).MacroRelCalledObjects(Index).PsRel_ObjCalled_ParentID
End Property

Public Property Let PsRel_ObjCalled_Name(Index As Integer, ByVal vData As Variant)
    Me(Me.Count).MacroRelCalledObjects(Index).PsRel_ObjCalled_Name = vData
End Property

Public Property Get PsRel_ObjCalled_Name(Index As Integer) As Variant
    PsRel_ObjCalled_Name = Me(Me.Count).MacroRelCalledObjects(Index).PsRel_ObjCalled_Name
End Property

Public Property Let PsRel_ObjCalled_RelType(Index As Integer, ByVal vData As Variant)
    Me(Me.Count).MacroRelCalledObjects(Index).PsRel_ObjCalled_RelType = vData
End Property

Public Property Get PsRel_ObjCalled_RelType(Index As Integer) As Variant
    PsRel_ObjCalled_RelType = Me(Me.Count).MacroRelCalledObjects(Index).PsRel_ObjCalled_RelType
End Property

Public Property Let PsRel_ObjCalled_RelDetail(Index As Integer, ByVal vData As Variant)
    Me(Me.Count).MacroRelCalledObjects(Index).PsRel_ObjCalled_RelDetail = vData
End Property

Public Property Get PsRel_ObjCalled_RelDetail(Index As Integer) As Variant
    PsRel_ObjCalled_RelDetail = Me(Me.Count).MacroRelCalledObjects(Index).PsRel_ObjCalled_RelDetail
End Property

' Relationship Count is READONLY
Public Property Get PsRel_ObjCaller_Count() As Variant
   PsRel_ObjCaller_Count = Me(Me.Count).MacroRelCallerObjects.Count
End Property

' PsRel_Obj
Public Property Let PsRel_ObjCaller_ID(Index As Integer, ByVal vData As Variant)
    Me(Me.Count).MacroRelCallerObjects(Index).PsRel_ObjCaller_ID = vData
End Property

Public Property Get PsRel_ObjCaller_ID(Index As Integer) As Variant
    PsRel_ObjCaller_ID = Me(Me.Count).MacroRelCallerObjects(Index).PsRel_ObjCaller_ID
End Property

Public Property Let PsRel_ObjCaller_ParentID(Index As Integer, ByVal vData As Variant)
    Me(Me.Count).MacroRelCallerObjects(Index).PsRel_ObjCaller_ParentID = vData
End Property

Public Property Get PsRel_ObjCaller_ParentID(Index As Integer) As Variant
    PsRel_ObjCaller_ParentID = Me(Me.Count).MacroRelCallerObjects(Index).PsRel_ObjCaller_ParentID
End Property

Public Property Let PsRel_ObjCaller_Name(Index As Integer, ByVal vData As Variant)
    Me(Me.Count).MacroRelCallerObjects(Index).PsRel_ObjCaller_Name = vData
End Property

Public Property Get PsRel_ObjCaller_Name(Index As Integer) As Variant
    PsRel_ObjCaller_Name = Me(Me.Count).MacroRelCallerObjects(Index).PsRel_ObjCaller_Name
End Property

Public Property Let PsRel_ObjCaller_RelType(Index As Integer, ByVal vData As Variant)
    Me(Me.Count).MacroRelCallerObjects(Index).PsRel_ObjCaller_RelType = vData
End Property

Public Property Get PsRel_ObjCaller_RelType(Index As Integer) As Variant
    PsRel_ObjCaller_RelType = Me(Me.Count).MacroRelCallerObjects(Index).PsRel_ObjCaller_RelType
End Property

Public Property Let PsRel_ObjCaller_RelDetail(Index As Integer, ByVal vData As Variant)
    Me(Me.Count).MacroRelCallerObjects(Index).PsRel_ObjCaller_RelDetail = vData
End Property

Public Property Get PsRel_ObjCaller_RelDetail(Index As Integer) As Variant
    PsRel_ObjCaller_RelDetail = Me(Me.Count).MacroRelCallerObjects(Index).PsRel_ObjCaller_RelDetail
End Property

