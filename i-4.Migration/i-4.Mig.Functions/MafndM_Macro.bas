Attribute VB_Name = "MafndM_Macro"
Option Explicit

'Oggetti da Inserire Nello Script
Global RichLyb As RichTextBox
Global DirLyb As DirListBox
Global FileLyb As FileListBox

Public MOs As New MacroObjects

Global SubIstr() As String
Global titolo As String
Global title As String

Global CbNomeMacro As String
Global CbOwnerMacro As String

Global SCR As ScriptControl

'silvia 7-1-2008
Global PsHideIgnore As Boolean

Type McFunc
  Name As String
  Description As String
  ReadOnly As Boolean
End Type

Type Ob
  ObjectID As Long
  Name As String
  Extension As String
  Directory As String
  type As String
  Selected As Boolean
End Type

Global McObjsel() As Ob

Global NumObjInMacro As Long

'Variabile per sapere su che Tab mi trovo
Global GMcTabSel  As Long

Global McF() As McFunc
Global McV() As McFunc

' Check if Macro text changed
Global gbMacroTextChanged As Boolean

Public Sub Salva_Macro_User(Name As String, Codice As String, PerLinea As Integer)
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From TS_Macro Where Nome = '" & Name & "'")
  If r.RecordCount = 0 Then
    r.AddNew
  End If
  
  r!Nome = Name
  r!PerLinea = PerLinea
  r!Sintax = Codice
  r.Update
  
  r.Close
End Sub

Public Sub Aggiungi_Oggetti_Script(Oggetto As String, Variabile As String, SC As ScriptControl)
  On Error GoTo ERR
  Select Case Trim(UCase(Oggetto))
    Case "DAOENGINE"
'      SCR.AddObject Variabile, DaoEngine, True
'      frmBase.LswObj.ListItems.Add , , "DaoEngine"
    Case "RICHTEXTLIB"
'      Set RichLyb = frmBase.RTApp
'      SCR.AddObject Variabile, RichLyb, True
'      frmBase.LswObj.ListItems.Add , , "RichTextBox Lib."
    Case "DIRLISTLIB"
'      Set DirLyb = frmBase.DirListBox
'      SCR.AddObject Variabile, DirLyb, True
'      frmBase.LswObj.ListItems.Add , , "DirListBox Lib."
    Case "FILELISTLIB"
'      Set FileLyb = frmBase.FileListBox
'      SCR.AddObject Variabile, FileLyb, True
'      frmBase.LswObj.ListItems.Add , , "FileListBox Lib."
    Case Else
'      MsgBox "Unknown Object..."
  End Select
  
  Exit Sub
ERR:
  MsgBox ERR.Description
End Sub

Public Sub Carica_Lista_Macro(Lsw As ListView, LswSys As ListView, SC As ScriptControl, Optional OnlyCode As Boolean)
  Dim r As Recordset, rSys As Recordset
  Dim wSQL As String
  Dim SwOnlyCode As Boolean
  
  If IsMissing(OnlyCode) Then
    SwOnlyCode = False
  Else
    SwOnlyCode = OnlyCode
  End If
  If Not SwOnlyCode Then
    Lsw.ListItems.Clear
    LswSys.ListItems.Clear
  End If
  wSQL = "Select * From SysMacro "
  If InStr(MafndF_Macro.LstAmb.List(MafndF_Macro.LstAmb.ListIndex), "MTP") > 0 Then wSQL = wSQL & " where area = 'MTPConv' "
  If InStr(MafndF_Macro.LstAmb.List(MafndF_Macro.LstAmb.ListIndex), "SQL") > 0 Then wSQL = wSQL & " where area = 'SQLConv' "
  If InStr(MafndF_Macro.LstAmb.List(MafndF_Macro.LstAmb.ListIndex), "MVS") > 0 Then wSQL = wSQL & " where area = 'MVSConv' "
  If InStr(MafndF_Macro.LstAmb.List(MafndF_Macro.LstAmb.ListIndex), "VSE") > 0 Then wSQL = wSQL & " where area = 'VSEConv' "
  If InStr(MafndF_Macro.LstAmb.List(MafndF_Macro.LstAmb.ListIndex), "IMS/DB") > 0 Then wSQL = wSQL & " where area = 'DLIConv' "
  If InStr(MafndF_Macro.LstAmb.List(MafndF_Macro.LstAmb.ListIndex), "IMS/DC") > 0 Then wSQL = wSQL & " where area = 'IMSConv' "
  If MafndF_Macro.LstAmb.List(MafndF_Macro.LstAmb.ListIndex) = "Generic" Then wSQL = wSQL & " where area = 'generic' "
  If InStr(MafndF_Macro.LstAmb.List(MafndF_Macro.LstAmb.ListIndex), "Migration - Generic") > 0 Then wSQL = wSQL & " where area = 'OBJConv' "
  
  wSQL = wSQL & " order by nome "
  
  On Error Resume Next
  Set rSys = m_fun.Open_Recordset_Sys(wSQL)
  While Not rSys.EOF
    If Not SwOnlyCode Then
      LswSys.ListItems.Add , , rSys!Nome, , 2
      If rSys!changeable = True Then
        LswSys.ListItems(LswSys.ListItems.Count).Tag = "MOD"
      Else
        LswSys.ListItems(LswSys.ListItems.Count).Tag = "UNMOD"
      End If
    End If
    SCR.AddCode Sostituisci_Carattere(rSys!Sintax & " ", "@", vbCrLf)
    rSys.MoveNext
  Wend
  rSys.Close
  
  'Carica le Macro del progetto
  Set r = m_fun.Open_Recordset("Select * From TS_Macro Order By Nome")
  While Not r.EOF
    If Not SwOnlyCode Then
      Lsw.ListItems.Add , , r!Nome, , 1
    End If
    If MOs.Count = 1 Then
      SCR.AddCode Sostituisci_Carattere(r!Sintax, "@", vbCrLf)
    End If
    r.MoveNext
  Wend
  r.Close
End Sub

Public Sub Carica_Lista_Funzioni(Lsw As ListView, SC As ScriptControl)
  Dim K As Integer
  
  Lsw.ListItems.Clear
  For K = 1 To UBound(McF)
    Lsw.ListItems.Add , , McF(K).Name, , 3
  Next K
End Sub

Public Sub Carica_Lista_Oggetti(Lsw As ListView, SC As ScriptControl)
  Dim K As Integer

  Lsw.ListItems.Clear
  For K = 1 To UBound(McV)
    Lsw.ListItems.Add , , McV(K).Name, , 4
  Next K
End Sub

Public Function Trova_Name_Sub(Codice As String) As String
  Dim i As Long, j As Long
  Dim NameSub As String
    
  If Trim(Codice) <> vbNullString Then
    i = InStr(1, UCase(Codice), "FUNCTION ")
    j = InStr(1, UCase(Codice), "(")
    
    NameSub = mId(Codice, i + 9, j - i - 9)
    Trova_Name_Sub = Trim(NameSub)
  End If
End Function

Public Sub Carica_Macro_In_Script(SC As ScriptControl, Codice As String)
  SCR.AddCode Codice
End Sub

Public Sub Elimina_MacroUSER_Da_DB(Name As String)
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From TS_Macro Where Nome = '" & Name & "'")
  If r.RecordCount Then
    r.Delete
  End If
  r.Close
End Sub

Public Function Copia_MacroUSER_To_MacroSYS(Name As String) As Boolean
  Dim r As Recordset, rSys As Recordset
  
  Screen.MousePointer = vbHourglass
  Set r = m_fun.Open_Recordset("Select * From TS_Macro Where Name = '" & Name & "'")
  If r.RecordCount Then
    'Trovata la macro la copia nel DB di Sistema e poi la cancella
    Set rSys = m_fun.Open_Recordset_Sys("Select * From SysMacro Where Name = '" & Name & "'")
    If rSys.RecordCount = 0 Then
      rSys.AddNew
      rSys!Name = Name
      rSys!Sintax = r!Sintax
      rSys.Update
      
      MsgBox Name & " User Macro was moved into System Macro Group...", vbInformation, m_fun.FnNomeProdotto
      r.Delete
      
      Copia_MacroUSER_To_MacroSYS = True
    Else
      MsgBox "Warning!!! " & Name & " macro Already exists in System Macro Group..." & vbCrLf & _
             "Macro was not moved...", vbInformation, m_fun.FnNomeProdotto
      
      Copia_MacroUSER_To_MacroSYS = False
    End If
    rSys.Close
  End If
  r.Close
  
  Screen.MousePointer = vbDefault
End Function

Public Function Copia_MacroSYS_To_MacroUSER(Name As String) As Boolean
  Dim r As Recordset, rSys As Recordset
  
  Screen.MousePointer = vbHourglass
  Set rSys = m_fun.Open_Recordset_Sys("Select * From SysMacro Where Name = '" & Name & "'")
  If rSys.RecordCount Then
    'Trovata la macro la copia nel DB di Sistema e poi la cancella
    Set r = m_fun.Open_Recordset("Select * From TS_Macro Where Name = '" & Name & "'")
    If r.RecordCount = 0 Then
      r.AddNew
      r!Name = Name
      r!Sintax = rSys!Sintax
      r.Update
      
      MsgBox Name & " System Macro was moved into User Macro Group...", vbInformation, m_fun.FnNomeProdotto
      
      rSys.Delete
      
      Copia_MacroSYS_To_MacroUSER = True
    Else
      MsgBox "Warning!!! " & Name & " macro Already exists in System Macro Group..." & vbCrLf & _
             "Macro was not moved...", vbInformation, m_fun.FnNomeProdotto
      Copia_MacroSYS_To_MacroUSER = False
    End If
    rSys.Close
  End If
  r.Close
  
  Screen.MousePointer = vbDefault
End Function

Public Sub Elimina_MacroSYS_Da_DB(Name As String)
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset_Sys("Select * From SysMacro Where Nome = '" & Name & "' ")
  If r.RecordCount Then
    r.Delete
  End If
  r.Close
End Sub

Public Sub Carica_Codice_Macro(RT As RichTextBox, MacroName As String, MacroType As String)
  Dim r As Recordset, rSys As Recordset
  
  MafndF_Macro.SSTab1.Tab = 0
  
  Select Case Trim(UCase(MacroType))
    Case "USER", "TRY_BOTH"
      On Error Resume Next
      
      Set r = m_fun.Open_Recordset("Select * From TS_Macro Where Nome = '" & MacroName & "'")
      If r.RecordCount Then
        If r!PerLinea Then
          MafndF_Macro.ChkForRow.Value = 1
        Else
          MafndF_Macro.ChkForRow.Value = 0
        End If
        RT.Text = Sostituisci_Carattere(r!Sintax, "@", vbCrLf)
      ElseIf Trim(UCase(MacroType)) = "TRY_BOTH" Then
        'Carica dal sistema
        Set rSys = m_fun.Open_Recordset_Sys("Select * From SysMacro Where Nome = '" & MacroName & "'")
        If rSys.RecordCount Then
          If rSys!PerLinea Then
            MafndF_Macro.ChkForRow.Value = 1
          Else
            MafndF_Macro.ChkForRow.Value = 0
          End If
          RT.Text = Sostituisci_Carattere(rSys!Sintax, "@", vbCrLf)
        End If
        rSys.Close
      End If
      r.Close
      
    Case "SYS"
      On Error Resume Next
      'Carica dal sistema
      Set rSys = m_fun.Open_Recordset_Sys("Select * From SysMacro Where Nome = '" & MacroName & "'")
      If rSys.RecordCount Then
        If rSys!PerLinea Then
          MafndF_Macro.ChkForRow.Value = 1
        Else
          MafndF_Macro.ChkForRow.Value = 0
        End If
        RT.Text = Sostituisci_Carattere(rSys!Sintax, "@", vbCrLf)
      End If
      rSys.Close
    
    Case "FUNC"
      On Error Resume Next
      'Carica dal sistema
      Set rSys = m_fun.Open_Recordset_Sys("Select * From Sysfunc Where Nome = '" & MacroName & "'")
      If rSys.RecordCount Then
        If rSys!PerLinea Then
          MafndF_Macro.ChkForRow.Value = 1
        Else
          MafndF_Macro.ChkForRow.Value = 0
        End If
        RT.Text = Sostituisci_Carattere(rSys!Sintax, "@", vbCrLf)
      End If
      rSys.Close
  End Select
  RT.SelStart = 0
End Sub

Public Function Sostituisci_Carattere(Stringa As String, ChFind As String, ChChg As String) As String
  Dim idx As Long
  Dim AppR As String
  Dim sx As String, dx As String
  
  AppR = Stringa
  idx = InStr(1, AppR, ChFind)
  While idx <> 0
    sx = mId(AppR, 1, idx - 1)
    dx = mId(AppR, idx + Len(ChFind))
   
    AppR = sx & ChChg & dx
    
    idx = InStr(idx + 1, AppR, ChFind)
  Wend
  Sostituisci_Carattere = Trim(AppR)
End Function

Public Sub Esegui_Macro(Name As String, Codice As String)
    
  On Error GoTo EH

  titolo = "Compile Macro..."
  Set SCR = MafndF_RunMacro.SC
  SCR.AddCode Codice
  
  Exit Sub
EH:
  MafndF_RunMacro.RtBVb.selText = "Execute Macro: " & ERR.Description
  SCR.Reset
      
  ERR.Clear
  Resume Next
End Sub

Public Function Restituisci_Directory_Da_ObjectID(Id As Long, Optional TypeDir As String) As String
  'restituisce la directory dall'id oggetto secondo il Type INPUT,OUTPUT
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select Directory_Input, Directory_Output From BS_Oggetti Where IDOggetto = " & Id)
  If r.RecordCount Then
    If Trim(TypeDir) <> "" Then
      Select Case UCase(TypeDir)
        Case "INPUT"
          Restituisci_Directory_Da_ObjectID = Crea_Directory_Progetto(r!Directory_Input, m_fun.FnPathDef)
          
        Case "OUTPUT"
          Restituisci_Directory_Da_ObjectID = Crea_Directory_Progetto(Trim(r!Directory_Output & " "), m_fun.FnPathDef)
      End Select
    Else
      'restituisce la dir input di default
      Restituisci_Directory_Da_ObjectID = Crea_Directory_Progetto(r!Directory_Input, m_fun.FnPathDef)
    End If
  End If
  r.Close
End Function

Public Sub Carica_Oggetti_For_Process(Optional ObjectID As Integer = 0)
  Dim r As Recordset
  Dim ParDir As String
  Dim i As Long, K As Long
  Dim Kiave As String
  Dim wStr As String
  Dim Count As Long
  
  On Error GoTo LOadSCR
  wStr = SCR.Name
  
  On Error GoTo EH
  
  Count = 0
  ReDim McObjsel(0)
  
  Screen.MousePointer = vbHourglass
  If ObjectID > 0 Then
  Else
    For i = 1 To MafndF_Macro.TVSelObj.Nodes.Count
      If MafndF_Macro.TVSelObj.Nodes(i).Checked Then
        Select Case Trim(UCase(MafndF_Macro.TVSelObj.Nodes(i).Key))
          Case "SEL"
            'Carica gli oggetti selezionati
            For K = 1 To m_fun.FnObjList.ListItems.Count
              If m_fun.FnObjList.ListItems(K).Selected Then
                Count = Count + 1
      
                ReDim Preserve McObjsel(UBound(McObjsel) + 1)
                McObjsel(UBound(McObjsel)).ObjectID = Val(m_fun.FnObjList.ListItems(K).Text)
                McObjsel(UBound(McObjsel)).Name = m_fun.FnObjList.ListItems(K).ListSubItems(1).Text
                
                McObjsel(UBound(McObjsel)).Extension = m_fun.FnObjList.ListItems(K).ListSubItems(1).Key
                
                McObjsel(UBound(McObjsel)).type = m_fun.FnObjList.ListItems(K).ListSubItems(2).Text
      
                ParDir = Restituisci_Directory_Da_ObjectID(Val(m_fun.FnObjList.ListItems(K).Text))
                McObjsel(UBound(McObjsel)).Directory = Crea_Directory_Progetto(ParDir, m_fun.FnPathDef)
              End If
            Next K
            Screen.MousePointer = vbDefault
            Exit Sub
          
          Case Else
            'Carica gli oggetti
            Kiave = Trim(UCase(MafndF_Macro.TVSelObj.Nodes(i).Key))
            Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where Tipo = '" & Kiave & "' Order By Nome")
            While Not r.EOF
              Count = Count + 1

              ReDim Preserve McObjsel(UBound(McObjsel) + 1)
              McObjsel(UBound(McObjsel)).ObjectID = Val(r!IdOggetto)
              McObjsel(UBound(McObjsel)).Name = r!Nome
              McObjsel(UBound(McObjsel)).Extension = r!Estensione
              McObjsel(UBound(McObjsel)).type = r!Tipo
              ParDir = r!Directory_Input
              McObjsel(UBound(McObjsel)).Directory = Crea_Directory_Progetto(ParDir, m_fun.FnPathDef)
              r.MoveNext
            Wend
            r.Close
        End Select
      End If
    Next i
  End If
  Screen.MousePointer = vbDefault
  
  Exit Sub
LOadSCR:
  Set SCR = MafndF_RunMacro.SC
  Resume Next
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Function Crea_Directory_Progetto(Directory As String, PathD As String) As String
  Dim WDollaro As Long
  
  WDollaro = InStr(1, Directory, "$")
  If WDollaro <> 0 Then
    Crea_Directory_Progetto = PathD & Trim(mId(Directory, WDollaro + 1))
    Exit Function
  Else
    Crea_Directory_Progetto = Directory
  End If
End Function

Public Function ExecuteMacro(iObjectID As Long, sMacroName As String, ChkOutSrc) As Boolean
  Dim WBuf As String, wRec As String
  Dim K As Integer
  Dim sNameI As String, iNumI As Integer
  Dim sNameO As String, iNumO As Integer
  Dim iRelationshipObjectID As Long
  Dim r As Recordset, R1 As Recordset
  Dim sFileName As String, sFileNamePath As String, sFileNamePathOut As String
  Dim SwModFile As Boolean
    
  On Error GoTo LOadSCR
  WBuf = SCR.Name
  
  On Error GoTo EH
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IDOggetto = " & iObjectID)
  If r.RecordCount Then
    Do While Not r.EOF
      MOs.Add CStr(iObjectID), CStr(iObjectID)
      
      MOs(CStr(iObjectID)).BS_Obj_ObjectID = Val(r!IdOggetto)
      MOs(CStr(iObjectID)).BS_Obj_Path = Crea_Directory_Progetto(r!Directory_Input, m_fun.FnPathDef)
      MOs(CStr(iObjectID)).BS_Obj_ObjectID = r!IdOggetto
      MOs(CStr(iObjectID)).BS_Obj_Name = r!Nome
      MOs(CStr(iObjectID)).BS_Obj_Extension = r!Estensione
      MOs(CStr(iObjectID)).BS_Obj_RowNum = r!NumRighe
      MOs(CStr(iObjectID)).BS_Obj_Type = r!Tipo
      MOs(CStr(iObjectID)).BS_Obj_Type_DBD = r!Tipo_DBD
      MOs(CStr(iObjectID)).BS_Obj_Type_JCL = r!Tipo_JCL
      MOs(CStr(iObjectID)).BS_Obj_BelongsToArea = r!Area_Appartenenza
      MOs(CStr(iObjectID)).BS_Obj_Level1 = r!Livello1
      MOs(CStr(iObjectID)).BS_Obj_Level2 = r!Livello2
      MOs(CStr(iObjectID)).BS_Obj_Available = r!Avaliable
      MOs(CStr(iObjectID)).BS_Obj_Directory_Input = r!Directory_Input
      MOs(CStr(iObjectID)).BS_Obj_Directory_Output = r!Directory_Output
      MOs(CStr(iObjectID)).BS_Obj_Cics = r!Cics
      MOs(CStr(iObjectID)).BS_Obj_Ims = r!Ims
      MOs(CStr(iObjectID)).BS_Obj_Batch = r!Batch
      MOs(CStr(iObjectID)).BS_Obj_sql = r!withSql
      MOs(CStr(iObjectID)).BS_Obj_Dli = r!DLI
      MOs(CStr(iObjectID)).BS_Obj_Mapping = r!Mapping
      MOs(CStr(iObjectID)).BS_Obj_FileSection = r!FileSection
      MOs(CStr(iObjectID)).BS_Obj_Working = r!Working
      MOs(CStr(iObjectID)).BS_Obj_Procedure = r!Procedure
      MOs(CStr(iObjectID)).BS_Obj_DtImport = r!DtImport
      MOs(CStr(iObjectID)).BS_Obj_DtParsing = r!DtParsing
      MOs(CStr(iObjectID)).BS_Obj_ParsingLevel = r!ParsingLevel
      MOs(CStr(iObjectID)).BS_Obj_DtEncapsulated = r!DtIncapsulamento

      r.MoveNext
    Loop
    r.Close
  
    Set r = m_fun.Open_Recordset("Select idoggettor, nomecomponente, relazione, utilizzo From PsRel_Obj Where IDOggettoC = " & iObjectID)
    Do While Not r.EOF
      iRelationshipObjectID = Val(r!IdOggettoR)
      
      MOs(CStr(iObjectID)).MacroRelCalledObjects.Add CStr(iRelationshipObjectID), CStr(iRelationshipObjectID)
      MOs(CStr(iObjectID)).MacroRelCalledObjects(CStr(iRelationshipObjectID)).PsRel_ObjCalled_ID = iRelationshipObjectID
      MOs(CStr(iObjectID)).MacroRelCalledObjects(CStr(iRelationshipObjectID)).PsRel_ObjCalled_ParentID = iObjectID
      MOs(CStr(iObjectID)).MacroRelCalledObjects(CStr(iRelationshipObjectID)).PsRel_ObjCalled_Name = r!NomeComponente
      MOs(CStr(iObjectID)).MacroRelCalledObjects(CStr(iRelationshipObjectID)).PsRel_ObjCalled_RelType = r!Relazione
      MOs(CStr(iObjectID)).MacroRelCalledObjects(CStr(iRelationshipObjectID)).PsRel_ObjCalled_RelDetail = r!Utilizzo
       
      r.MoveNext
    Loop
    r.Close
    
    Set r = m_fun.Open_Recordset("Select idoggettoc, relazione, utilizzo From PsRel_Obj Where IDOggettoR = " & iObjectID)
    Do While Not r.EOF
      iRelationshipObjectID = Val(r!IDOggettoC)
           
      MOs(CStr(iObjectID)).MacroRelCallerObjects.Add CStr(iRelationshipObjectID), CStr(iRelationshipObjectID)
      MOs(CStr(iObjectID)).MacroRelCallerObjects(CStr(iRelationshipObjectID)).PsRel_ObjCaller_ID = iRelationshipObjectID
      MOs(CStr(iObjectID)).MacroRelCallerObjects(CStr(iRelationshipObjectID)).PsRel_ObjCaller_ParentID = iObjectID
      Set R1 = m_fun.Open_Recordset("Select nome From Bs_oggetti Where IDOggetto = " & iRelationshipObjectID)
      If R1.RecordCount Then
        MOs(CStr(iObjectID)).MacroRelCallerObjects(CStr(iRelationshipObjectID)).PsRel_ObjCaller_Name = R1!Nome
      End If
      R1.Close
      MOs(CStr(iObjectID)).MacroRelCallerObjects(CStr(iRelationshipObjectID)).PsRel_ObjCaller_RelType = r!Relazione
      MOs(CStr(iObjectID)).MacroRelCallerObjects(CStr(iRelationshipObjectID)).PsRel_ObjCaller_RelDetail = r!Utilizzo
      
      r.MoveNext
    Loop
    r.Close
     
    CaricaProjectParam
    Carica_Lista_Macro MafndF_Macro.LswMacro, MafndF_Macro.LswMacroSys, SCR, True
        
    sFileNamePath = MOs(CStr(iObjectID)).BS_Obj_Path
    If Trim(MOs(CStr(iObjectID)).BS_Obj_Extension) <> "" Then
      sFileName = MOs(CStr(iObjectID)).BS_Obj_Name & "." & MOs(CStr(iObjectID)).BS_Obj_Extension
    Else
      sFileName = MOs(CStr(iObjectID)).BS_Obj_Name
    End If
        
    If ChkOutSrc = 0 Then
      MafndF_Macro.RTBFind.fileName = sFileNamePath & "\" & sFileName
    Else
      If Dir(sFileNamePath & "\out\" & sFileName) = "" Then 'arimettemola
        MOs.Remove MOs.Count
        Exit Function
      End If
      MafndF_Macro.RTBFind.fileName = sFileNamePath & "\out\" & sFileName
    End If
    iNumI = ((MOs.Count - 1) * 2) + 1

    If ChkOutSrc = 0 Then
      sNameI = sFileNamePath & "\" & sFileName
    Else
      sNameI = sFileNamePath & "\out\" & sFileName
    End If
    Open sNameI For Input As iNumI
        
    iNumO = ((MOs.Count - 1) * 2) + 2
        
    MOs(CStr(iObjectID)).BS_Obj_Selected = False
        
    sFileNamePathOut = sFileNamePath & "\tmp"
    If Dir(sFileNamePathOut, vbDirectory) = "" Then
      MkDir sFileNamePathOut
    End If
    
    If m_fun.Check_If_License_Update Then
      sNameO = sFileNamePathOut & "\" & sFileName
      Open sNameO For Output As iNumO
    End If
     
    wRec = ""
    SwModFile = False
    
    MOs.BS_Obj_CurrentLine = 0
    MOs(CStr(iObjectID)).Abort = False
        
    While Not EOF(iNumI) And Not MOs(CStr(iObjectID)).Abort
    
      WBuf = wRec
      Line Input #iNumI, wRec
        
      MOs.BS_Obj_CurrentLine = MOs.BS_Obj_CurrentLine + 1
      WBuf = WBuf & wRec
      wRec = ""
             
      MOs.BS_Obj_RecArea = WBuf
      SCR.Run sMacroName
      
      If m_fun.Check_If_License_Update Then
        If WBuf <> MOs.BS_Obj_RecArea Then
          WBuf = MOs.BS_Obj_RecArea
          SwModFile = True
        End If
      End If
      
      wRec = ""
      If m_fun.Check_If_License_Update Then
        Print #iNumO, WBuf
      End If
    Wend
    Close #iNumI
    
    If m_fun.Check_If_License_Update Then
      Close #iNumO
      If SwModFile And Not MOs(CStr(iObjectID)).Abort Then
        FileCopy sNameO, sNameI
      End If
      Kill sNameO
    End If
    
    MafndF_Macro.RtLogPrj.SelStart = Len(MafndF_Macro.RtLogPrj.Text)
    MafndF_Macro.RtLogPrj.SelLength = 0
 
    MOs.Remove MOs.Count
 
    If MOs.Count Then
      sFileNamePath = MOs(MOs.Count).BS_Obj_Path
      sFileName = MOs(MOs.Count).BS_Obj_Name & "." & MOs(MOs.Count).BS_Obj_Extension
      MafndF_Macro.RTBFind.fileName = sFileNamePath & "\" & sFileName
    End If
  Else
    r.Close
  End If

  Exit Function
LOadSCR:
  Set SCR = MafndF_RunMacro.SC
  SCR.AddObject "MC", MOs, True
  Resume Next
EH:
  MafndF_RunMacro.RtBVb.selText = "VB Script Environment Error (" & ERR.Number & ") <" & sMacroName & ">" & vbCrLf & _
                                  ERR.Description & vbCrLf
  ERR.Clear
  For K = MOs.Count To 1 Step -1
    MOs.Remove K
  Next K
  Close #iNumI
  Close #iNumO
End Function

Public Sub CaricaProjectParam()
  MOs.Param_FormDateDB2 = LeggiParam("DO_FORDATEDB2")
  MOs.Param_FormDateOracle = LeggiParam("DO_FORDATEORA")
  MOs.Param_DefValueChar = LeggiParam("DO_DEFVALCHAR")
  MOs.Param_DefValueNum = LeggiParam("DO_DEFVALNUM")
  MOs.Param_DefValueDate = LeggiParam("DO_DEFVALDATE")
  MOs.Param_DefValueTime = LeggiParam("DO_DEFVALTIME")
  MOs.Param_DefValueTimeStamp = LeggiParam("DO_DEFVALTIMESTP")
End Sub

Function LeggiParam(wKey As String) As String
  Dim wResp() As String
  On Error GoTo ErrResp
  
  wResp = m_fun.RetrieveParameterValues(wKey)
  If InStr(wResp(0), wKey) Then
    LeggiParam = ""
  Else
    LeggiParam = wResp(0)
  End If
  Exit Function
ErrResp:
  LeggiParam = ""
End Function
