VERSION 5.00
Object = "{0E59F1D2-1FBE-11D0-8FF2-00A0D10038BC}#1.0#0"; "msscript.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form MafndF_RunMacro 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "VbScript Log Viewer"
   ClientHeight    =   4125
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7230
   Icon            =   "MafndF_RunMacro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4125
   ScaleWidth      =   7230
   StartUpPosition =   1  'CenterOwner
   Begin RichTextLib.RichTextBox RtBVb 
      Height          =   4125
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   7185
      _ExtentX        =   12674
      _ExtentY        =   7276
      _Version        =   393217
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"MafndF_RunMacro.frx":0442
   End
   Begin MSScriptControlCtl.ScriptControl SC 
      Left            =   270
      Top             =   4200
      _ExtentX        =   1005
      _ExtentY        =   1005
      Timeout         =   100000
      UseSafeSubset   =   -1  'True
   End
End
Attribute VB_Name = "MafndF_RunMacro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  RtBVb.Text = ""
  Cancel = 1
  Me.Hide
End Sub

Private Sub SC_Error()
  Dim Messaggio As String
      
  On Error GoTo ErrorHandler
      
  Messaggio = SCR.Error.Description & "(" & SC.Error.Line & "," & SC.Error.Column & ")"
'  If SSTab1.Tab = 0 Then
'    RtLogMacro.SelStart = Len(RtLogMacro.Text)
'    RtLogMacro.SelLength = 0
'    RtLogMacro.SelText = "     --> " & Title & " <--" & vbCrLf & Space$(10) & Messaggio & vbCrLf
'  Else
  RtBVb.SelStart = Len(RtBVb.Text)
  RtBVb.SelLength = 0
  RtBVb.selText = "     --> " & title & " <--" & vbCrLf & _
                  Space$(10) & Messaggio & vbCrLf
'  End If
  Me.Show
    
  SCR.Error.Clear
  
  Exit Sub
ErrorHandler:
  Screen.MousePointer = vbDefault
  'MsgBox ERR.Description, vbOKOnly, ERR.Source
  RtBVb.SelStart = Len(RtBVb.Text)
  RtBVb.SelLength = 0
  RtBVb.selText = "     --> " & title & " <--" & vbCrLf & _
                  "(" & ERR.Number & ") - <" & ERR.LastDllError & ">" & vbCrLf & _
                  ERR.Description & vbCrLf
  Me.Show
  ERR.Clear
    
  SCR.Error.Clear
End Sub
