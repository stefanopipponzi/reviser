Attribute VB_Name = "MafndM_TxtEd"
Option Explicit

'servono per sapere dove andare a caricare la copy selezionata sull'editor
Public CopySelect As String
Public CopyPath As String
Public RTPosition As Long  'posizione inserimento copy
Public RTFine As Long 'posizione di fine copy dopo inserimento
Public copyExpanded As Boolean
Public lineeIstr() As String

Public gIdOggetto As Long 'Serve per caricare l'oggetto selezionato nell'editor (COPY)

Public collHandler As New Collection 'Collection per contenere le form copyload
' Mauro 31/10/2007 : Metto una variabile perch� posso avere "PLI", "CBL" o "EZT"
'Public eztMacro As Boolean
Public tipoPgm As String

Public idLevel As Integer
Public collExpandedCopy As Collection
Public nrExpandedCopy As Integer


''silvia
'Get Row and Column Positions Fast in a Textbox or Rich Textbox
Private Declare Function SendMessageByNum Lib "user32" Alias "SendMessageA" _
       (ByVal hwnd As Long, _
        ByVal wMsg As Long, _
        ByVal wParam As Long, _
        ByVal lParam As Long) As Long

Private Const EM_LINEFROMCHAR As Long = &HC9
Private Const EM_LINEINDEX As Long = &HBB
Private Const EM_GETLINECOUNT As Long = &HBA
Private Const EM_LINESCROLL As Long = &HB6

Type EZTStatement
  EZT_Riga As String
  EZT_WIn As Long
  EZT_WFin As Long
End Type

Public msSearchString As String

'
'*******************
' GetLineCount
' Get the line count
'*******************
Public Function GetLineCount(tBox As Object) As Long
  GetLineCount = SendMessageByNum(tBox.hwnd, EM_GETLINECOUNT, 0&, 0&)
End Function

'*******************
' GetLineNum
' Get current line number
'*******************
Public Function GetLineNum(tBox As Object) As Long
  GetLineNum = SendMessageByNum(tBox.hwnd, EM_LINEFROMCHAR, tBox.SelStart, 0&)
End Function


'*******************
' GetColPos
' Get current Column
'*******************
Public Function GetColPos(tBox As Object) As Long
  GetColPos = tBox.SelStart - SendMessageByNum(tBox.hwnd, EM_LINEINDEX, -1&, 0&)
End Function
''silvia

''
Public Function Crea_Directory_Parametrica(Directory As String) As String
  Dim WSlch As Long
  Dim old As Long
  
  WSlch = InStr(1, Directory, "\")
  
  If WSlch <> 0 Then
    'controlla se c'� un'altra \ dopo la prima (percorso di rete)
    If mId(Directory, 2, 1) = "\" Then
      WSlch = 2
    Else
      '� una directory normale
      If Trim("$\" & mId(Directory, WSlch + 1)) = "$\" Then
        Crea_Directory_Parametrica = "$"
      Else
        Crea_Directory_Parametrica = Trim("$\" & mId(Directory, WSlch + 1))
      End If
      Exit Function
    End If
    
    If WSlch <> 0 Then
      WSlch = InStr(WSlch + 1, Directory, "\") - 1
      'elimina il nome del pc
      'tronca la stringa a wslch
      If WSlch <> -1 Then
        Crea_Directory_Parametrica = Trim("$" & mId(Directory, WSlch + 1))
        Exit Function
      Else
        Crea_Directory_Parametrica = Trim("$" & mId(Directory, 3))
        Exit Function
      End If
    End If
  End If
End Function

Function Crea_Directory_Progetto(Directory As String, PathD As String) As String
  Dim WDollaro As Long
    
  WDollaro = InStr(1, Directory, "$")
  
  If WDollaro <> 0 Then
    Crea_Directory_Progetto = PathD & Trim(mId(Directory, WDollaro + 1))
    Exit Function
  End If
End Function

Public Sub Colora_File_RichTextBox(RT As RichTextBox, Percorso As String)
  Dim i As Long
  Dim Cont As Long
  
  Screen.MousePointer = vbHourglass

  For i = 0 To Len(RT.Text)
    RT.SelStart = i
    RT.SelLength = 2

    Cont = Cont + 1
    If RT.selText = vbCrLf Then
      Cont = 0
    Else
      RT.SelLength = 1
      If Cont > 72 Then
        RT.SelColor = vbGreen
      End If
    End If
  Next i

  Screen.MousePointer = vbDefault
  RT.SelStart = 0
  RT.SelLength = 0
End Sub

Public Sub Carica_Parametri_TextEditor(RT As RichTextBox)
  'RT.Font.Name = Menu.BsFont
  'RT.Font.Size = Menu.BsFontSize
End Sub

Function nameFromId(pIdOggetto As Long) As String
  Dim r As Recordset

  Set r = m_fun.Open_Recordset("Select nome from Bs_Oggetti where IdOggetto = " & pIdOggetto)
  If Not r.EOF Then
    nameFromId = r!Nome
  Else
    nameFromId = ""
  End If
  r.Close
End Function

Public Sub Carica_Lista_Errori(RT As RichTextBox, Lsw As ListView, Id As Long)
  Dim r As Recordset, rs As Recordset
  Dim OldPos As Long, oldRiga As Long
  Dim Nome As String, Messaggio As String, Var1 As String
  
  Nome = m_fun.Restituisci_NomeOgg_Da_IdOggetto(Id)
  
  Lsw.ListItems.Clear
  
  'Set r = m_fun.Open_Recordset("Select codice,riga,var1,var2 From BS_Segnalazioni Where IdOggetto = " & Id & " Order By Riga")
  Set r = m_fun.Open_Recordset("Select IdOggetto,IdOggettoRel,codice,riga,var1,var2 From BS_Segnalazioni Where " & _
                                        "(IdOggetto = " & Id & " or IdOggettoRel = " & Id & ") Order By Riga")
  While Not r.EOF
    Lsw.ListItems.Add , , r!Codice
      
    'Mauro: 15/03/2006 - Che senso ha????????
    If oldRiga <> r!Riga Then
      'SQ????????
      If Len(Nome) > 2 Then
        Select Case Trim(UCase(mId(Nome, Len(Nome) - 2)))
          Case "JCL", "PRC", "ASM"
            OldPos = Marca_IndexRiga_In_RTBox(RT, OldPos, r!Riga, False)
          Case Else
            OldPos = Marca_IndexRiga_In_RTBox(RT, OldPos, r!Riga, True)
        End Select
      End If
      oldRiga = r!Riga
    End If
    
    'Posizione linea (in RTB):
    Lsw.ListItems(Lsw.ListItems.Count).ListSubItems.Add , "K" & OldPos, r!Riga
    'Messaggio:
    Messaggio = m_fun.getMessageError(r!Codice)
    'Lsw.ListItems(Lsw.ListItems.Count).ListSubItems.Add , , m_fun.Componi_Messaggio_Errore(Id, r!Codice, r!Riga)
    Lsw.ListItems(Lsw.ListItems.Count).ListSubItems.Add , , Replace(Replace(Messaggio, "%VAR1%", r!Var1), "%VAR2%", r!Var2 & "")
    If r!IdOggetto = Id And Not r!IdOggetto = r!IdOggettoRel Then 'chiamante mostra quello relazionato
      Lsw.ListItems(Lsw.ListItems.Count).ListSubItems.Add , , nameFromId(r!IdOggettoRel)
    ElseIf Not r!IdOggetto = Id Then  ' oggetto relazionato mostra il chiamante
      Lsw.ListItems(Lsw.ListItems.Count).ListSubItems.Add , , nameFromId(r!IdOggetto)
    End If
      
    'silvia 7-1-2008
    If PsHideIgnore Then
      Var1 = Replace(r!Var1, "'", "''")
      Set rs = m_fun.Open_Recordset("select * from Bs_Segnalazioni_Ignore where " & _
                                             "code = '" & r!Codice & "' and var1 = '" & Var1 & "'")
      If Not rs.EOF Then
        Lsw.ListItems.Remove (Lsw.ListItems.Count)
      End If
      rs.Close
    End If

    r.MoveNext
  Wend
  r.Close
End Sub

Public Sub Trova_Riga_In_RTBox(RT As RichTextBox, Riga As Long)
  Dim i As Long, j As Long
  
  i = RT.Find(vbCrLf, 1)
  While i <> -1
    If Riga = RT.GetLineFromChar(i) + 1 Then
      For j = 1 To 200
        'trova l'accapo precedente
        RT.SelStart = i - j
        RT.SelLength = 2
        
        If RT.selText = vbCrLf Then Exit For
      Next j
            
      RT.SelStart = i - j + 1
      RT.SelLength = j
            
      RT.SetFocus
      Exit Sub
    End If
    i = RT.Find(vbCrLf, i + 2)
  Wend
End Sub

Public Function Marca_IndexRiga_In_RTBox(RT As RichTextBox, OldPos As Long, Riga As Long, Right As Boolean) As Long
  Dim i As Long, j As Long
  
  If OldPos = 0 Then OldPos = 1
  
  'trova la riga e si posiziona all'inizio della riga
  i = RT.Find(vbCrLf, OldPos)
  While i <> -1
    If Riga = RT.GetLineFromChar(i) + 1 Then
      For j = 1 To 200
        If j > i Then Exit For
        If Right = True Then
          'trova l'accapo precedente
          RT.SelStart = i - j
          RT.SelLength = 2
        Else
          'Trova l'accapo successivo
          RT.SelStart = i + j
          RT.SelLength = 2
        End If
        
        If RT.selText = vbCrLf Then Exit For
      Next j
        
      If Right = True Then
        RT.SelStart = i - j + 1
        RT.SelLength = 6
      Else
        RT.SelStart = i + j - 1
        RT.SelLength = 0
      End If
      
      'scrive l'index di riga nel sorgente
      'If Right = True Then
      '  RT.SelText = "R" & Format(Riga, "00000")
      'Else
      '  RT.SelText = "         R" & Format(Riga, "00000")
      'End If
      
      Exit Function
    End If
    i = RT.Find(vbCrLf, i + 2)
    Marca_IndexRiga_In_RTBox = i
  Wend
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MF 18-09-07
'Modificato per trovare anche le macro EZT oltre che le copy per Cobol
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub Carica_Lista_Copy(IdOgg As Long, Tool As MafndF_TxtEd)
  Dim r As Recordset, rMacro As Recordset, rCopy As Recordset, rInclude As Recordset, rAssembler As Recordset
  Dim Exist As Boolean
  
  Set r = m_fun.Open_Recordset("Select * From Bs_Oggetti Where IdOggetto = " & IdOgg & "")
  If r.RecordCount Then
    ' Mauro 31/10/2007 : Gestione tipoPGM
    tipoPgm = r!Tipo
    'claudio 09/11/2010 :gestione tipo "ASM"
    If r!Tipo = "ASM" Then
      Set rAssembler = m_fun.Open_Recordset("Select * From PSRel_Obj Where IdOggettoC = " & IdOgg & " And Relazione = 'CAL'")
      While Not rAssembler.EOF
        Tool.Toolbar1.Buttons(1).ButtonMenus.Add , , rAssembler!NomeComponente
        Tool.cmbPgmCpy.AddItem rAssembler!NomeComponente

        'Controlla se la copy esiste
        If m_fun.Restituisci_NomeOgg_Da_IdOggetto(rAssembler!IdOggettoR) = "" Then
          Exist = False
        Else
          Exist = True
        End If
        
        Tool.Toolbar1.Buttons(1).ButtonMenus(Tool.Toolbar1.Buttons(1).ButtonMenus.Count).Enabled = Exist
        rAssembler.MoveNext
      Wend
      rAssembler.Close
    ElseIf r!Tipo = "EZT" Then
      'eztMacro = True
      Set rMacro = m_fun.Open_Recordset("Select * From PSRel_Obj Where IdOggettoC = " & IdOgg & " And Relazione = 'CAL'")
      While Not rMacro.EOF
        Tool.Toolbar1.Buttons(1).ButtonMenus.Add , , rMacro!NomeComponente
        Tool.cmbPgmCpy.AddItem rMacro!NomeComponente

        'Controlla se la copy esiste
        If m_fun.Restituisci_NomeOgg_Da_IdOggetto(rMacro!IdOggettoR) = "" Then
          Exist = False
        Else
          Exist = True
        End If
        
        Tool.Toolbar1.Buttons(1).ButtonMenus(Tool.Toolbar1.Buttons(1).ButtonMenus.Count).Enabled = Exist
        rMacro.MoveNext
      Wend
      rMacro.Close
    ElseIf r!Tipo = "PLI" Then
      Set rInclude = m_fun.Open_Recordset("Select * From PSRel_Obj Where IdOggettoC = " & IdOgg & " And Relazione = 'INC'")
      While Not rInclude.EOF
        Tool.Toolbar1.Buttons(1).ButtonMenus.Add , , rInclude!NomeComponente
        Tool.cmbPgmCpy.AddItem rInclude!NomeComponente
        'Controlla se la copy esiste
        If m_fun.Restituisci_NomeOgg_Da_IdOggetto(rInclude!IdOggettoR) = "" Then
          Exist = False
        Else
          Exist = True
        End If
        
        Tool.Toolbar1.Buttons(1).ButtonMenus(Tool.Toolbar1.Buttons(1).ButtonMenus.Count).Enabled = Exist
        rInclude.MoveNext
      Wend
      rInclude.Close
    Else
      'Carica Le copy presenti nel progetto
      Set rCopy = m_fun.Open_Recordset("Select * From PSRel_Obj Where IdOggettoC = " & IdOgg & " And Relazione = 'CPY'")
      While Not rCopy.EOF
        Tool.Toolbar1.Buttons(1).ButtonMenus.Add , , rCopy!NomeComponente
        Tool.cmbPgmCpy.AddItem rCopy!NomeComponente
        'Controlla se la copy esiste
        If m_fun.Restituisci_NomeOgg_Da_IdOggetto(rCopy!IdOggettoR) = "" Then
          Exist = False
        Else
          Exist = True
        End If
        
        Tool.Toolbar1.Buttons(1).ButtonMenus(Tool.Toolbar1.Buttons(1).ButtonMenus.Count).Enabled = Exist
        rCopy.MoveNext
      Wend
      rCopy.Close
    End If
  End If
  r.Close
End Sub

Public Function Trova_Copy(RT As RichTextBox, NomeCpy As String) As Long
  Dim idx As Long
  Dim i As Long
  'silvia 8-4-2008
  Dim strparam As String, strfind As String
  Dim LenInclude As Long
  
  strparam = "(, (, "
  If tipoPgm = "CBL" Then
    If Trim(UCase(mId(NomeCpy, Len(NomeCpy) - 4))) = ".CPY" Then
      NomeCpy = mId(NomeCpy, 1, Len(NomeCpy) - 4)
    End If
  End If
  
  ' Mauro 31/10/2007 : Gestione tipoPGM
  'If eztMacro Then
  If tipoPgm = "EZT" Then
    idx = RT.Find("%" & NomeCpy, 1)
  'silvia 8-4-2008
  ElseIf tipoPgm = "PLI" Then
    For i = 0 To UBound(Split(strparam, ","))
      strfind = "INCLUDE" & Split(strparam, ",")(i) & NomeCpy
      If RT.Find(strfind, 1) > 0 Then
        LenInclude = Len("INCLUDE" & Split(strparam, ",")(i))
        idx = RT.Find(strfind)
        Exit For
      'Else
      '  idx = -1
      End If
    Next i
    If i = UBound(Split(strparam, ",")) + 1 Then
      idx = RT.Find("INCLUDE " & NomeCpy, 1)
      LenInclude = Len("INCLUDE ")
    End If
    'idx = RT.Find("%INCLUDE " & NomeCpy, 1)
  Else
    idx = RT.Find(" " & NomeCpy, 1)
  End If
  
  Do While idx <> -1
    'Controlla se � asteriscata
    'controlla se c'� un asterisco a colonna 7
    For i = 1 To 100
      RT.SelStart = idx - i
      RT.SelLength = 2
  
      If RT.selText = vbCrLf Then
        ' Mauro 31/10/2007 : Gestione tipoPGM
        'If (eztMacro) Then
        If tipoPgm = "EZT" Then
          RT.SelStart = idx - i + 1
        ElseIf tipoPgm = "PLI" Then
          'silvia 8-4-2008
          'RT.SelStart = idx - i + 11
          RT.SelStart = idx + LenInclude
        Else
          'RT.SelStart = idx - i + 7
          RT.SelStart = idx + 1
        End If
''''        RT.SelStart = idx - i + 7
        'silvia 8-4-2008
        'RT.SelLength = 1
        RT.SelLength = Len(NomeCpy)
  
        If RT.selText = "*" Then
          Exit For
        Else
          Exit Do
        End If
      End If
    Next i
    
    ' Mauro 31/10/2007 : Gestione tipoPGM
    'If (eztMacro) Then
    If tipoPgm = "EZT" Then
      idx = RT.Find("%" & NomeCpy, idx + Len(" " & NomeCpy))
    ElseIf tipoPgm = "PLI" Then
      idx = RT.Find("%INCLUDE " & NomeCpy, idx + Len(" " & NomeCpy))
    Else
      idx = RT.Find(" " & NomeCpy, idx + Len(" " & NomeCpy))
    End If
''''    idx = RT.Find(" " & NomeCpy, idx + Len(" " & NomeCpy))
  Loop
  Trova_Copy = idx
  RT.SetFocus
End Function

Public Sub Expand_Cpy(RTApp As RichTextBox, RTSource As RichTextBox, TxtFind As String)
  Dim RTPosition As Long, RTFine As Long
  Dim i As Long, j As Long
  Dim cpy As Long
  Dim Accapo As Long
  Dim NomeCOPY As String
  Dim FlagStart As Boolean
  Dim CopyPath  As String
  Dim CopyFind As Boolean
  
  Dim MarcatoreCPY As String
  Dim Riga As Long, riganew  As Long
  Dim LenCpy As Long
  Dim Count As Long
  
  Dim isINCLUDE As Boolean
  Dim intI As Integer, booExpanded As Boolean
  Dim STipoOggetto As String
  
  'silvia
  Dim LenInclude As Long

  FlagStart = False
  isINCLUDE = False
  'flgparentesi = False
  'Cerca le copy nella RichTextBox
  'silvia 2-1-2008
  cpy = RTSource.Find(TxtFind, 1)

  If cpy = -1 Then
    Exit Sub
  Else
    If InStr(TxtFind, "INCLUDE ") Then
      isINCLUDE = True
    End If
  End If
  
  CopyFind = False
  
  While cpy <> -1
    NomeCOPY = ""
    
    'Controlla che non sia asteriscato
    For i = 1 To 100
      RTSource.SelStart = cpy - i
      RTSource.SelLength = 2
  
      If RTSource.selText = vbCrLf Then
        Accapo = cpy - i + 1
        Exit For
      End If
    Next i
    
    If Not isINCLUDE Then
      'cpy = cpy - i + 6
      RTSource.SelStart = Accapo + 6
    Else
      RTSource.SelStart = cpy - i
      'RTSource.SelStart = Accapo + lenInclude
    End If
    RTSource.SelLength = 1
    
    If RTSource.selText <> "*" Then
      'Trova il nome della Copy
      For i = 1 To 100
        If Not isINCLUDE Then
          RTSource.SelStart = cpy + 5 + i
        Else
          RTSource.SelStart = cpy + 7 + i
          'RTSource.SelStart = cpy + lenInclude + i
        End If
        RTSource.SelLength = 1
        If RTSource.selText <> " " And RTSource.selText <> "." And RTSource.selText <> "'" And RTSource.selText <> ";" Then
          NomeCOPY = NomeCOPY & RTSource.selText
          FlagStart = True
        Else
          If FlagStart Then
            Exit For
          End If
        End If
      Next i
      
      If Len(Trim(NomeCOPY)) Then
        'silvia: tolgo le parentesi
        NomeCOPY = Replace(NomeCOPY, "(", "")
        NomeCOPY = Replace(NomeCOPY, ")", "")
        
        'Toglie eventuali punti
        If mId(NomeCOPY, Len(NomeCOPY), 1) = "." Then
          NomeCOPY = mId(NomeCOPY, 1, Len(NomeCOPY) - 1)
        End If
       
        'Toglie eventuali apici
        If mId(NomeCOPY, Len(NomeCOPY), 1) = "'" Then
          NomeCOPY = mId(NomeCOPY, 1, Len(NomeCOPY) - 1)
        End If
       
        If mId(NomeCOPY, 1, 1) = "'" Then
          NomeCOPY = mId(NomeCOPY, 2)
        End If
       
        'Toglie eventuali punti e virgola: solo per PLI
        If isINCLUDE And mId(NomeCOPY, Len(NomeCOPY), 1) = ";" Then
          NomeCOPY = mId(NomeCOPY, 1, Len(NomeCOPY) - 1)
        End If
        
        Riga = RTSource.GetLineFromChar(cpy)
        MarcatoreCPY = "C" & String(5 - Len(Hex(Riga)), "0") & Hex(Riga)
        
        ' Mauro 31/10/2007: creo tutto il percorso nella routine, cos� mi evito 2 query diverse
        '''Prende il path della copy
        ''CopyPath = m_fun.Restituisci_Percorso_Da_NomeOggetto(NomeCOPY)
       
        'Creata nuova funzione
        'Gloria 28/04/2009: cerco l'oggetto con il nome e il tipo (in base all'oggetto su cui sto lavorando) perch� il nome non � univoco
        
  '      CopyPath = Restituisci_File_Da_NomeOggetto(NomeCOPY)
        
        If Not collExpandedCopy Is Nothing Then
          For intI = 1 To collExpandedCopy.Count
            If collExpandedCopy(intI) = NomeCOPY Then
              booExpanded = True
              Exit For
            End If
          Next intI
        End If
      Else
        booExpanded = False
      End If
      
      If Not booExpanded Then
        Select Case Trim(TxtFind)
          Case "COPY"
            STipoOggetto = "CPY"
          Case "INCLUDE"
            If tipoPgm = "CBL" Then
              STipoOggetto = "CPY"
            Else
              STipoOggetto = "INC"
            End If
          Case "MACRO"
            STipoOggetto = "EZM"
          ' MANCA GESTIONE ASSEMBLER
        End Select
        CopyPath = Restituisci_File_Da_Nome_Tipo_Oggetto(NomeCOPY, STipoOggetto)
         
        '*************************
        ' gloria 28/04/2009: lo azzero alla fine di ogni ciclo altrimenti rimane sporco!
        'LenCpy = 0
        If Trim(CopyPath) <> "" Then
          'Espande la copy :
          'CARICA LA COPY NELLA TEXT DI APPOGGIO
          'CopyPath = CopyPath & "\" & NomeCOPY & IIf(Len(CopyEstensione), "." & CopyEstensione, "")
          RTApp.LoadFile CopyPath
          LenCpy = Len(RTApp.Text)
           
          'trova RTPosition
          For i = 1 To 100
            RTSource.SelStart = cpy + i
            RTSource.SelLength = 2
             
            If RTSource.selText = vbCrLf Then
              RTPosition = cpy + i + 2
              Exit For
            End If
          Next i
     
          'Copia la copy nella RichTextBox principale
          RTSource.SelStart = RTPosition
          RTSource.SelLength = 0
           
          RTSource.SelColor = vbRed
          If Not isINCLUDE Then
            RTSource.selText = MarcatoreCPY & vbCrLf & RTApp.Text & vbCrLf & "CPYFN" & vbCrLf
          Else
            RTSource.selText = MarcatoreCPY & vbCrLf & RTApp.Text & vbCrLf & "INCFN" & vbCrLf
          End If
           
          RTFine = RTPosition + Len(RTApp.Text) + 11
           
          'silvia: nuove posizioni con copy espanse
          For j = 1 To MafndF_TxtEd.lswErrori.ListItems.Count
            riganew = CLng(MafndF_TxtEd.lswErrori.ListItems(j).ListSubItems(1).Text)
            If riganew > CLng(Riga + 1) Then
              MafndF_TxtEd.lswErrori.ListItems(j).ListSubItems(1).key = "K" & (CLng(mId(MafndF_TxtEd.lswErrori.ListItems(j).ListSubItems(1).key, 2)) + Len(MarcatoreCPY & vbCrLf & RTApp.Text & vbCrLf & "CPYFN" & vbCrLf)) ' + Len(RTApp.Text)   + 11)
              MafndF_TxtEd.lswErrori.ListItems(j).ListSubItems(1).Text = RTSource.GetLineFromChar(CLng(mId(MafndF_TxtEd.lswErrori.ListItems(j).ListSubItems(1).key, 2))) + 1
            Else
              'MsgBox "end"
            End If
          Next j
   
          'aggiorna linee istruzioni ALE
          'Dim rigaIn As Long
          'Dim rigaOut As Long
          'rigaIn = Riga 'RTSource.GetLineFromChar(RTPosition)
          'rigaOut = RTSource.GetLineFromChar(RTFine)
          'aggiornaLineeIstruz rigaIn, rigaOut
        
          RTSource.SelStart = RTPosition
          RTSource.SelLength = 0
          RTSource.SelColor = vbRed
          CopyFind = True
           
          If collExpandedCopy Is Nothing Then Set collExpandedCopy = New Collection
           
          collExpandedCopy.Add NomeCOPY
            
        End If
      End If
      booExpanded = False
    End If ' espando solo le copy non ancora espanse
          
    FlagStart = False
    If Not isINCLUDE Then
      'silvia 2-1-2008
      cpy = RTSource.Find(TxtFind, LenCpy + cpy + 6)
      ''cpy = RTSource.Find(" COPY ", LenCpy + 14 + cpy + 6)
    Else
      'cpy = RTSource.Find("%INCLUDE", LenCpy + 14 + cpy + 8)
      cpy = RTSource.Find("INCLUDE", LenCpy + cpy + 7)
''      If cpy > -1 Then
''        'Controlla che ci sia il %
''        For i = 1 To 100
''          RTSource.SelStart = cpy - i
''          RTSource.SelLength = 1
''
''          If RTSource.SelText = "%" Then
''            lenInclude = 7 + i
''            cpy = cpy - i
''            Exit For
''          End If
''        Next i
''      End If
    End If
    LenCpy = 0
    Count = Count + 1
  Wend
  
  If Not collExpandedCopy Is Nothing Then
    If nrExpandedCopy <> collExpandedCopy.Count Then
      nrExpandedCopy = collExpandedCopy.Count
      idLevel = idLevel + 1
    Else
      ' Per le Copy Cobol, deve ancora fare il secondo giro per le Include
      If TxtFind <> " COPY " Then
        MsgBox "All copy levels expandend", vbInformation, "Expand copy"
      End If
    End If
  End If
End Sub

'Gloria 28/04/2009 : dal nome dell'Oggetto e dal Tipo, mi ricreo il percorso e il nome del file
Public Function Restituisci_File_Da_Nome_Tipo_Oggetto(NomeOgg As String, TipoOggetto As String) As String
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & NomeOgg & "' and tipo = '" & TipoOggetto & "'")
  If r.RecordCount Then
    Restituisci_File_Da_Nome_Tipo_Oggetto = Crea_Directory_Progetto(r!Directory_Input, m_fun.FnPathDef) & "\" & _
                                            NomeOgg & IIf(Len(r!Estensione), "." & r!Estensione, "")
  End If
  r.Close
End Function

'Mauro 31/10/2007 : dal nome dell'Oggetto, mi ricreo il percorso e il nome del file
Public Function Restituisci_File_Da_NomeOggetto(NomeOgg As String) As String
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & NomeOgg & "'")
  If r.RecordCount Then
    Restituisci_File_Da_NomeOggetto = Crea_Directory_Progetto(r!Directory_Input, m_fun.FnPathDef) & "\" & _
                                      NomeOgg & IIf(Len(r!Estensione), "." & r!Estensione, "")
  End If
  r.Close
End Function

'SILVIA 9-4-2008 (copiato dal Data Analisi- Analisi_di_Impatto)
Function FindProcedure_Job(tipoOgg As String, rt1 As RichTextBox) As Long
  Dim minWpox As Long
  Dim EZT As EZTStatement
  Dim i As Integer
  Dim RigaInit As String
  Dim ctlloop As Boolean
  
  Select Case tipoOgg
    Case "CBL", "CPY"
      'SILVIA 21-09-2007 faccio la find come per gli ezt
      Do Until ctlloop
        'SQ lo spazio davanti?!
        minWpox = rt1.Find("PROCEDURE ", minWpox)
        If minWpox = -1 Then ctlloop = False:  Exit Do
        'SQ getStatement � per l'EZT...
''        EZT = MaandM_ImpactEZT.getStatement(rt1, minWpox)
''        'tra l'altro ritorna il fine linea davanti allo statement!
''        EZT.EZT_Riga = mId(EZT.EZT_Riga, 3)
''        If mId(EZT.EZT_Riga, 7, 1) = "*" Then
          'la riga � asteriscata....
        '
          'SILVIA: con questo � meglio?! If InStr(10, RigaInit, " DIVISION") Then
          If rt1.Find(" DIVISION", minWpox) Then
            'SQ questo DIVISION potrebbe essere ovunque!
            ctlloop = True
          End If
        'End If
        If minWpox = -1 Then
          ctlloop = True: Exit Do
        End If
        minWpox = minWpox + 1
      Loop
''    Case "EZT", "EZM"   'MC INIZIO NEW
''      'trovo l'inizio della procedure division
''      ' Mauro 23/07/2007 : Controllo se sto analizzando il tipo giusto senn� s'incasina con i CBL
''      Do Until ctlloop
''        minWpox = rt1.Find("JOB ", minWpox)
''        If minWpox = -1 Then ctlloop = False:  Exit Do
''        EZT = getStatement(rt1, minWpox)
''        RigaInit = mId(EZT.EZT_Riga, 3, Len(EZT.EZT_Riga)) & " "
''        If Left(RigaInit, 1) = "*" Then
''          'la riga � asteriscata....
''        Else
''          ctlloop = True
''        End If
''        If minWpox = -1 Then
''          ctlloop = True: Exit Do
''        End If
''        minWpox = minWpox + 1
''      Loop
  End Select
  FindProcedure_Job = minWpox
End Function

