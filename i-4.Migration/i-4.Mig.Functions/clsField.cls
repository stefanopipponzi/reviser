VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Enum e_Usage
   BNR = 0 ^ 2
   PKC = 1 ^ 2
   EDT = 2 ^ 2
   ZND = 3 ^ 2
End Enum

Private m_IdOggetto As Long
Private m_IdArea As Long
Private m_Ordinale As Long
Private m_Codice As Variant
Private m_Nome As String
Private m_Livello As Long
Private m_Tipo As String
Private m_Posizione As Long
Private m_Lunghezza As Long
Private m_Byte As Long
Private m_Decimali As Long
Private m_Segno As String
Private m_Usage As e_Usage
Private m_Valore As String
Private m_Picture As Variant
Private m_Occurs As Long
Private m_NomeRed As String
