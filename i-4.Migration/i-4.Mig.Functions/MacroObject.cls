VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MacroObject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Attribute VB_Ext_KEY = "Member0" ,"MacroRelationshipObjects"
Attribute VB_Ext_KEY = "Member1" ,"MatsdC_Fuctions"
Option Explicit

Public Key As String
Public BS_Obj_RecArea
Public BS_Obj_Selected
Public BS_Obj_CurrentLine As Long
Public Param_FormDateDB2
Public Param_FormDateOracle
Public Param_DefValueChar
Public Param_DefValueNum
Public Param_DefValueDate
Public Param_DefValueTime
Public Param_DefValueTimeStamp
Public Abort As Boolean

Private mvarMacroRelCalledObjects As MacroRelCalledObjects
Private mvarMacroRelCallerObjects As MacroRelCallerObjects

Private Type ObjectType
  BS_Obj_ObjectID As Variant
  BS_Obj_Path As Variant
  BS_Obj_Name As Variant
  BS_Obj_Extension As Variant
  BS_Obj_RowNum As Variant
  BS_Obj_Type As Variant
  BS_Obj_Type_DBD As Variant
  BS_Obj_Type_JCL As Variant
  BS_Obj_BelongsToArea As Variant
  BS_Obj_Level1 As Variant
  BS_Obj_Level2 As Variant
  BS_Obj_Available As Variant
  BS_Obj_Directory_Input As Variant
  BS_Obj_Directory_Output As Variant
  BS_Obj_Cics As Variant
  BS_Obj_Ims As Variant
  BS_Obj_Batch As Variant
  BS_Obj_sql As Variant
  BS_Obj_Dli As Variant
  BS_Obj_Mapping As Variant
  BS_Obj_FileSection As Variant
  BS_Obj_Working As Variant
  BS_Obj_Procedure As Variant
  BS_Obj_DtImport As Variant
  BS_Obj_DtParsing As Variant
  BS_Obj_ParsingLevel As Variant
  BS_Obj_DtEncapsulated As Variant
End Type
Private vObject As ObjectType

Public Property Get MacroRelCalledObjects() As MacroRelCalledObjects
  If mvarMacroRelCalledObjects Is Nothing Then
    Set mvarMacroRelCalledObjects = New MacroRelCalledObjects
  End If
  Set MacroRelCalledObjects = mvarMacroRelCalledObjects
End Property

Public Property Set MacroRelCalledObjects(vData As MacroRelCalledObjects)
  Set mvarMacroRelCalledObjects = vData
End Property

Public Property Get MacroRelCallerObjects() As MacroRelCallerObjects
  If mvarMacroRelCallerObjects Is Nothing Then
    Set mvarMacroRelCallerObjects = New MacroRelCallerObjects
  End If
  Set MacroRelCallerObjects = mvarMacroRelCallerObjects
End Property

Public Property Set MacroRelCallerObjects(vData As MacroRelCallerObjects)
  Set mvarMacroRelCallerObjects = vData
End Property

Private Sub Class_Terminate()
  Set mvarMacroRelCalledObjects = Nothing
  Set mvarMacroRelCallerObjects = Nothing
End Sub

' BS_Oggetti Table
Public Property Let BS_Obj_ObjectID(ByVal vData As Variant)
  vObject.BS_Obj_ObjectID = vData
End Property

Public Property Get BS_Obj_ObjectID() As Variant
  BS_Obj_ObjectID = vObject.BS_Obj_ObjectID
End Property

Public Property Let BS_Obj_Path(ByVal vData As Variant)
  vObject.BS_Obj_Path = vData
End Property

Public Property Get BS_Obj_Path() As Variant
  BS_Obj_Path = vObject.BS_Obj_Path
End Property

Public Property Let BS_Obj_Name(ByVal vData As Variant)
  vObject.BS_Obj_Name = vData
End Property

Public Property Get BS_Obj_Name() As Variant
  BS_Obj_Name = vObject.BS_Obj_Name
End Property

Public Property Let BS_Obj_Extension(ByVal vData As Variant)
  vObject.BS_Obj_Extension = vData
End Property

Public Property Get BS_Obj_Extension() As Variant
  BS_Obj_Extension = vObject.BS_Obj_Extension
End Property

Public Property Let BS_Obj_RowNum(ByVal vData As Variant)
  vObject.BS_Obj_RowNum = vData
End Property

Public Property Get BS_Obj_RowNum() As Variant
  BS_Obj_RowNum = vObject.BS_Obj_RowNum
End Property

Public Property Let BS_Obj_Type(ByVal vData As Variant)
  vObject.BS_Obj_Type = vData
End Property

Public Property Get BS_Obj_Type() As Variant
  BS_Obj_Type = vObject.BS_Obj_Type
End Property

Public Property Let BS_Obj_Type_DBD(ByVal vData As Variant)
  vObject.BS_Obj_Type_DBD = vData
End Property

Public Property Get BS_Obj_Type_DBD() As Variant
  BS_Obj_Type_DBD = vObject.BS_Obj_Type_DBD
End Property

Public Property Let BS_Obj_Type_JCL(ByVal vData As Variant)
  vObject.BS_Obj_Type_JCL = vData
End Property

Public Property Get BS_Obj_Type_JCL() As Variant
  BS_Obj_Type_JCL = vObject.BS_Obj_Type_JCL
End Property

Public Property Let BS_Obj_BelongsToArea(ByVal vData As Variant)
  vObject.BS_Obj_BelongsToArea = vData
End Property

Public Property Get BS_Obj_BelongsToArea() As Variant
  BS_Obj_BelongsToArea = vObject.BS_Obj_BelongsToArea
End Property

Public Property Let BS_Obj_Level1(ByVal vData As Variant)
  vObject.BS_Obj_Level1 = vData
End Property

Public Property Get BS_Obj_Level1() As Variant
  BS_Obj_Level1 = vObject.BS_Obj_Level1
End Property

Public Property Let BS_Obj_Level2(ByVal vData As Variant)
  vObject.BS_Obj_Level2 = vData
End Property

Public Property Get BS_Obj_Level2() As Variant
  BS_Obj_Level2 = vObject.BS_Obj_Level2
End Property

Public Property Let BS_Obj_Available(ByVal vData As Variant)
  vObject.BS_Obj_Available = vData
End Property

Public Property Get BS_Obj_Available() As Variant
  BS_Obj_Available = vObject.BS_Obj_Available
End Property

Public Property Let BS_Obj_Directory_Input(ByVal vData As Variant)
  vObject.BS_Obj_Directory_Input = vData
End Property

Public Property Get BS_Obj_Directory_Input() As Variant
  BS_Obj_Directory_Input = vObject.BS_Obj_Directory_Input
End Property

Public Property Let BS_Obj_Directory_Output(ByVal vData As Variant)
  vObject.BS_Obj_Directory_Output = vData
End Property

Public Property Get BS_Obj_Directory_Output() As Variant
  BS_Obj_Directory_Output = vObject.BS_Obj_Directory_Output
End Property

Public Property Let BS_Obj_Cics(ByVal vData As Variant)
  vObject.BS_Obj_Cics = vData
End Property

Public Property Get BS_Obj_Cics() As Variant
  BS_Obj_Cics = vObject.BS_Obj_Cics
End Property

Public Property Let BS_Obj_Ims(ByVal vData As Variant)
  vObject.BS_Obj_Ims = vData
End Property

Public Property Get BS_Obj_Ims() As Variant
  BS_Obj_Ims = vObject.BS_Obj_Ims
End Property

Public Property Let BS_Obj_Batch(ByVal vData As Variant)
  vObject.BS_Obj_Batch = vData
End Property

Public Property Get BS_Obj_Batch() As Variant
  BS_Obj_Batch = vObject.BS_Obj_Batch
End Property

Public Property Let BS_Obj_sql(ByVal vData As Variant)
  vObject.BS_Obj_sql = vData
End Property

Public Property Get BS_Obj_sql() As Variant
  BS_Obj_sql = vObject.BS_Obj_sql
End Property

Public Property Let BS_Obj_Dli(ByVal vData As Variant)
  vObject.BS_Obj_Dli = vData
End Property

Public Property Get BS_Obj_Dli() As Variant
  BS_Obj_Dli = vObject.BS_Obj_Dli
End Property

Public Property Let BS_Obj_Mapping(ByVal vData As Variant)
  vObject.BS_Obj_Mapping = vData
End Property

Public Property Get BS_Obj_Mapping() As Variant
  BS_Obj_Mapping = vObject.BS_Obj_Mapping
End Property

Public Property Let BS_Obj_FileSection(ByVal vData As Variant)
  vObject.BS_Obj_FileSection = vData
End Property

Public Property Get BS_Obj_FileSection() As Variant
  BS_Obj_FileSection = vObject.BS_Obj_FileSection
End Property

Public Property Let BS_Obj_Working(ByVal vData As Variant)
  vObject.BS_Obj_Working = vData
End Property

Public Property Get BS_Obj_Working() As Variant
  BS_Obj_Working = vObject.BS_Obj_Working
End Property

Public Property Let BS_Obj_Procedure(ByVal vData As Variant)
  vObject.BS_Obj_Procedure = vData
End Property

Public Property Get BS_Obj_Procedure() As Variant
  BS_Obj_Procedure = vObject.BS_Obj_Procedure
End Property

Public Property Let BS_Obj_DtImport(ByVal vData As Variant)
  vObject.BS_Obj_DtImport = vData
End Property

Public Property Get BS_Obj_DtImport() As Variant
  BS_Obj_DtImport = vObject.BS_Obj_DtImport
End Property

Public Property Let BS_Obj_DtParsing(ByVal vData As Variant)
  vObject.BS_Obj_DtParsing = vData
End Property

Public Property Get BS_Obj_DtParsing() As Variant
  BS_Obj_DtParsing = vObject.BS_Obj_DtParsing
End Property

Public Property Let BS_Obj_ParsingLevel(ByVal vData As Variant)
  vObject.BS_Obj_ParsingLevel = vData
End Property

Public Property Get BS_Obj_ParsingLevel() As Variant
  BS_Obj_ParsingLevel = vObject.BS_Obj_ParsingLevel
End Property

Public Property Let BS_Obj_DtEncapsulated(ByVal vData As Variant)
  vObject.BS_Obj_DtEncapsulated = vData
End Property

Public Property Get BS_Obj_DtEncapsulated() As Variant
  BS_Obj_DtEncapsulated = vObject.BS_Obj_DtEncapsulated
End Property
