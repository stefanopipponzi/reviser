VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPgmRepository"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'Private
Private m_Pgm As New clsPgm

Private Sub Load_PgmRel(Id As Long, Optional Nome As String, Optional ByVal wProfondita As String)
  Set m_Pgm = Carica_Proprieta_Programma(Id)
   
  Select Case wProfondita
  Case "1"
   'Aggiunge Programmi Caller
    Carica_Programmi_Caller Id
  Case "2"
   'Aggiunge Programmi Called
    Carica_Programmi_Called Id
  Case Else
    Carica_Programmi_Called Id
    Carica_Programmi_Caller Id
  End Select
  
End Sub

Public Property Get OpenRelazioni(Id As Long, Optional ByVal wProfondita As String) As clsPgm
   Load_PgmRel Id, , wProfondita
   
   Set OpenRelazioni = m_Pgm
End Property

Private Sub Carica_Programmi_Caller(Id As Long, Optional Simple As Boolean)
  Dim p As clsPgm
  Dim r As Recordset
  Dim AppPgm As clsPgm
  
  Screen.MousePointer = vbHourglass
  Set r = m_fun.Open_Recordset("Select IdOggettoC,Relazione From PsRel_Obj Where IdOggettoR = " & Id)
  While Not r.EOF
    Set p = Carica_Proprieta_Programma(r!IDOggettoC & "")
    p.Relazione = r!Relazione & ""
    
    'Aggiunge il caller
    m_Pgm.AddCaller p
    
    'Copia nel temporaneo il caller
    'Set AppPgm = Aggiunge_SubNodo_Caller(TN(r!IdOggettoC), p)
    r.MoveNext
  Wend
  r.Close
  Screen.MousePointer = vbDefault
End Sub

Private Function Aggiunge_SubNodo_Caller(Id As Long, Padre As clsPgm) As clsPgm
  Dim AppPgm As clsPgm
  Dim p As clsPgm
  Dim rs As Recordset
  
  Set AppPgm = Padre
        
  Set rs = m_fun.Open_Recordset("Select * From PsRel_Obj Where IdOggettoR = " & Id)
  While Not rs.EOF
    Set p = Carica_Proprieta_Programma(TN(rs!IDOggettoC))
    AppPgm.AddCaller p
    
    Set Aggiunge_SubNodo_Caller = Aggiunge_SubNodo_Caller(TN(rs!IDOggettoC), p)
    
    Screen.MousePointer = vbDefault
    rs.MoveNext
  Wend
  rs.Close
End Function

Private Sub Carica_Programmi_Called(Id As Long, Optional Simple As Boolean)
  Dim p As clsPgm
  Dim cp As clsCopy
  Dim psb As clsPsb
  Dim Map As clsMappe
  Dim Asm As clsPgm
  Dim r As Recordset
  Dim rs As Recordset
  Dim AppPgm As clsPgm
  
  Screen.MousePointer = vbHourglass
  Set r = m_fun.Open_Recordset("Select distinct IdOggettoR,Utilizzo From PsRel_Obj Where IdOggettoC = " & Id)
  While Not r.EOF
    Set p = Carica_Proprieta_Programma(r!IdOggettoR & "")
    p.Relazione = r!Utilizzo & ""
    m_Pgm.AddCalled p
    Set AppPgm = Aggiunge_SubNodo_Called(r!IdOggettoR & "", p)
    r.MoveNext
  Wend
  r.Close
    
  'Visiona la tabella degli oggetti sconosciuti
  Set r = m_fun.Open_Recordset("Select distinct NomeOggettoR,Relazione,Utilizzo From PsRel_ObjUnk Where IdOggettoC = " & Id)
  While Not r.EOF
    Set p = New clsPgm
    p.Id = -1
    p.Nome = r!NomeOggettoR & ""
    p.nRighe = -1
    p.Tipo = r!Relazione & ""
    p.Relazione = r!Utilizzo & ""
    
    m_Pgm.AddCalled p
    
    r.MoveNext
  Wend
  r.Close
    
  'Oggetti PsCom
  Set r = m_fun.Open_Recordset("Select distinct NomeOggettoR,Utilizzo From PsCom_Obj Where IdOggettoC = " & Id)
  While Not r.EOF
    Set p = New clsPgm
    p.Id = -1
    p.Nome = r!NomeOggettoR & ""
    p.nRighe = -1
    p.Tipo = r!Utilizzo & ""
    p.Relazione = r!Utilizzo & ""
    
    m_Pgm.AddCalled p
       
    r.MoveNext
  Wend
  r.Close
   
  'file
  Set r = m_fun.Open_Recordset("Select DsnName From PsRel_File Where IdOggetto = " & Id)
  While Not r.EOF
'    If TN(r!DsnName) <> "" Then
      If InStr(1, r!DsnName & "", ".") > 1 Then 'Deve partire dal secondo almeno
        Set p = New clsPgm
        p.Id = -1
        p.Nome = r!DsnName & ""
        p.nRighe = -1
        p.Tipo = "VSM"
        p.Relazione = "USED"
               
        m_Pgm.AddCalled p
        
      End If
'    End If
    r.MoveNext
  Wend
  r.Close
   
End Sub

Private Function IsAssembler(Id As Long) As Boolean
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Id)
  If rs.RecordCount Then
    If TN(rs!Tipo) = "ASM" Then
      IsAssembler = True
    Else
      IsAssembler = False
    End If
  End If
  rs.Close
End Function

Private Function Aggiunge_SubNodo_Called(Id As Long, Padre As clsPgm) As clsPgm
  Dim AppPgm As clsPgm
  Dim p As clsPgm
  Dim rs As Recordset
  Dim cp As clsCopy
  Dim psb As clsPsb
  Dim Map As clsMappe
  Dim Asm As clsPgm
    
  Set AppPgm = Padre
        
  Set rs = m_fun.Open_Recordset("Select * From PsRel_Obj Where IdOggettoC = " & Id)
  While Not rs.EOF
    Select Case TN(rs!Relazione)
      Case "CPY"
        Set cp = Carica_Proprieta_Copy(TN(rs!IdOggettoR))
        AppPgm.AddCopy cp
      
      Case "PSB"
        Set psb = Carica_Proprieta_Psb(TN(rs!IdOggettoR))
        AppPgm.AddPsb psb
      
      Case "MAP", "BMS", "SPF"
        Set Map = Carica_Proprieta_Mappa(TN(rs!IdOggettoR))
        AppPgm.AddMaps Map
      
      Case Else
        Set p = Carica_Proprieta_Programma(TN(rs!IdOggettoR))
        p.Relazione = TN(rs!Utilizzo)
        AppPgm.AddCalled p
    End Select
    rs.MoveNext
  Wend
  rs.Close
End Function
