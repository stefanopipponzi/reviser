VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MafndF_TableEditor 
   Caption         =   " DBD details"
   ClientHeight    =   8430
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11010
   LinkTopic       =   "Form1"
   ScaleHeight     =   8430
   ScaleWidth      =   11010
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Delete"
      Height          =   405
      Left            =   6593
      Style           =   1  'Graphical
      TabIndex        =   2
      Tag             =   "fixed"
      Top             =   7950
      Width           =   1845
   End
   Begin VB.CommandButton cmdInsert 
      Caption         =   "Insert "
      Height          =   405
      Left            =   4583
      Style           =   1  'Graphical
      TabIndex        =   1
      Tag             =   "fixed"
      Top             =   7950
      Width           =   1845
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "Update"
      Height          =   405
      Left            =   2573
      Style           =   1  'Graphical
      TabIndex        =   0
      Tag             =   "fixed"
      Top             =   7950
      Width           =   1845
   End
   Begin MSComctlLib.ListView lswTab 
      Height          =   7815
      Left            =   60
      TabIndex        =   3
      Top             =   60
      Width           =   10875
      _ExtentX        =   19182
      _ExtentY        =   13785
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
End
Attribute VB_Name = "MafndF_TableEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public txtNum As Long
Dim TabName As String
Dim NomiCampi() As String
Dim ValoriCampi() As String
Public update_insert As Integer

Private Sub cmdCancel_Click()
  'stop
  Dim varBeforUpdate As Variant
  Dim titolo As String
  Dim i As Long, j As Long, K As Long

  On Error GoTo ErrorHandler

  If MsgBox("Do you confirm the cancellation of the selected records?", vbOKCancel + vbQuestion, "i-4.Migration") = vbOK Then
    For i = lswTab.ListItems.Count To 1 Step -1 '''''''''
      If lswTab.ListItems(i).Selected Then  '''''''''''''
  
        ReDim FieldValues(lswTab.ColumnHeaders.Count - 1)
        If lswTab.ListItems.Count > 0 Then
          FieldValues(0) = lswTab.ListItems(i)
          For j = 1 To lswTab.ColumnHeaders.Count - 1
            FieldValues(j) = lswTab.ListItems(i).SubItems(j)
          Next j
        End If
  
'***************************************************
        For K = 0 To UBound(FieldNames) - 1
          If FieldNames(K).type = "3" Then
            varBeforUpdate = varBeforUpdate & FieldNames(K).Name & "=" & FieldValues(K) & " and "
          ElseIf FieldNames(K).type = "202" Then
            varBeforUpdate = varBeforUpdate & FieldNames(K).Name & checkNull(FieldValues(K)) & " and "
          ElseIf FieldNames(K).type = "11" Then
            varBeforUpdate = varBeforUpdate & FieldNames(K).Name & "= " & IIf(FieldValues(K), "True", "False") & " and "
          End If
        Next K
        If FieldNames(K).type = "3" Then
          varBeforUpdate = varBeforUpdate & FieldNames(UBound(FieldNames)).Name & "=" & FieldValues(UBound(FieldNames))
        ElseIf FieldNames(K).type = "202" Then
          varBeforUpdate = varBeforUpdate & FieldNames(UBound(FieldNames)).Name & checkNull(FieldValues(UBound(FieldNames)))
        ElseIf FieldNames(K).type = "11" Then
          varBeforUpdate = varBeforUpdate & FieldNames(UBound(FieldNames)).Name & "=" & IIf(FieldValues(UBound(FieldNames)), "True", "False")
        End If
            
        m_fun.FnConnection.Execute ("DELETE FROM " & nomeTab & " WHERE  " & varBeforUpdate & " ;") 'varBeforUpdate
      End If
      varBeforUpdate = ""
    Next i
    'SILVIA
    'displayTab False, nomeTab, title
    displayTab True, nomeTab, title
  End If
  Exit Sub
ErrorHandler:
  MsgBox ERR.Number & " " & ERR.Description
End Sub

Public Function checkNull(Campo As String) As String
  If UCase(Campo) = "NULL" Then
    checkNull = " is null"
  Else
    checkNull = "= '" & Campo & "'"
  End If
End Function

Private Sub cmdInsert_Click()
  'stop
  Dim i As Long
  
  ReDim FieldValues(lswTab.ColumnHeaders.Count - 1)
  If lswTab.ListItems.Count Then
    FieldValues(0) = ""
    For i = 1 To lswTab.ColumnHeaders.Count - 1
      FieldValues(i) = ""
    Next i
  End If
  
  MafndF_UpdateTab.Start 1
End Sub

Private Sub cmdUpdate_Click()
  Dim i As Long
  
  'FieldValues(i)  = inserire i valori del record selezionati
  ReDim FieldValues(lswTab.ColumnHeaders.Count - 1)
  If lswTab.ListItems.Count Then
    FieldValues(0) = lswTab.SelectedItem
    For i = 1 To lswTab.ColumnHeaders.Count - 1
      FieldValues(i) = lswTab.SelectedItem.SubItems(i)
    Next i
  End If
 
  MafndF_UpdateTab.Start 0
End Sub

Private Sub Form_Load()
  'displayTab False
End Sub

Public Sub displayTab(boolRefresh As Boolean, table As String, title As String)
  Dim rs As Recordset
  Dim listx As ListItem
  Dim i As Long
    
  Set rs = m_fun.Open_Recordset("Select * from " & table & "")
  If Not rs Is Nothing Then
    For i = 0 To rs.Fields.Count - 1
      If Not boolRefresh Then
        lswTab.ColumnHeaders.Add , , rs.Fields(i).Name
        lswTab.ColumnHeaders(i + 1).Width = lswTab.Width / rs.Fields.Count * 0.99
      End If
      lswTab.ListItems.Clear
      ReDim Preserve FieldNames(rs.Fields.Count - 1)
      FieldNames(i).Name = rs.Fields(i).Name
      FieldNames(i).type = rs.Fields(i).type
      FieldNames(i).len = rs.Fields(i).DefinedSize
      'SQ - run-time error se non ho il default impostato!
      'Proviamo almeno una volta le modifiche...
      If rs.RecordCount Then
        If Not IsNull(rs.Fields(i).OriginalValue) Then
          FieldNames(i).default = rs.Fields(i).OriginalValue
        End If
      End If
    Next i
    
    Do Until rs.EOF
      For i = 0 To rs.Fields.Count - 1
        If i = 0 Then
          Set listx = lswTab.ListItems.Add(, , TN(rs.Fields(i)))
        Else
          If IsNull(rs.Fields(i)) Then
            listx.SubItems(i) = "null"
          Else
            If rs.Fields(i).type = 11 Then
              If rs.Fields(i) Then
                listx.SubItems(i) = "True"
              Else
                listx.SubItems(i) = "False"
              End If
            Else
              'lswTab.ListItems.Add , , TN(rs.Fields(i))
              listx.SubItems(i) = rs.Fields(i)
            End If
          End If
        End If
      Next i
      rs.MoveNext
    Loop
  End If
  rs.Close
'  frmTab.Caption = " " & lswTables.SelectedItem
  
  Caption = title
   
  nomeTab = table
  
  'Me.Show vbModal
End Sub

Private Sub Form_Resize()
  Dim i As Integer
  
  m_fun.Elimina_Flickering_Object Me.hwnd
  ResizeForm Me
  For i = 1 To lswTab.ColumnHeaders.Count
    lswTab.ColumnHeaders(i).Width = lswTab.Width / lswTab.ColumnHeaders.Count * 0.99
  Next i
  m_fun.Terminate_Flickering_Object
End Sub

Private Sub lswTab_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswTab.SortOrder
  
  key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswTab.SortOrder = Order
  lswTab.SortKey = key - 1
  lswTab.Sorted = True
End Sub
