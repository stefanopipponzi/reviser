VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MaFndC_Compilers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_IdOggetto As Long
Private m_FlagError As String

Public Function FormatString(cWhoIs As t_WhoIs, cClientMsg As t_ClientMsg, cClientReq As t_ClientReq) As String
   Dim aStr As String
   Dim i As Long
   Dim c As Long
   
   'WhoIs
   aStr = "WHOIS("
   
   For c = 0 To UBound(cWhoIs.Values)
      aStr = aStr & cWhoIs.Values(c).cTag & "=" & cWhoIs.Values(c).cValue & "#"
   Next
   
   aStr = aStr & ")END-WHOIS "
   
   'ClientMsg
   aStr = aStr & "CLIENTMSG("
   
   If cClientMsg.Script <> "" Then
      'Incolla lo script se presente
      aStr = aStr = "SCRIPT{" & cClientMsg.Script & "}END-SCRIPT "
   End If
      
   aStr = aStr & "FUNCTION=" & cClientMsg.Functions.Name & "(" 'Parametri
   
   For i = 0 To UBound(cClientMsg.Functions.Parameters)
      aStr = aStr & cClientMsg.Functions.Parameters(i).cTag & "=" & cClientMsg.Functions.Parameters(i).cValue & "#"
   Next i
   
   aStr = aStr & ")END-FUNCTION "
   
   aStr = aStr & ")END-CLIENTMSG "
   
   'ClientReq
   aStr = aStr & "CLIENTREQ("
      
   aStr = aStr & "FUNCTION=" & cClientReq.Functions.Name & "(" 'Parametri
   
   For i = 0 To UBound(cClientReq.Functions.Parameters)
      aStr = aStr & cClientReq.Functions.Parameters(i).cTag & "=" & cClientReq.Functions.Parameters(i).cValue & "#"
   Next i
   
   aStr = aStr & ")END-FUNCTION "
   
   aStr = aStr & ")END-CLIENTREQ"
   
   FormatString = aStr & Chr(10)
End Function

Public Function parse_cob32RetCode(wBlock As String)
   'Parse del blocco di ritorno dal server sotto unix dopo la compilazione del sorgente
   m_IdOggetto = getIdOggetto_fromBlock(wBlock)
   
   If m_IdOggetto > -1 Then
   
   Else
      m_FlagError = "IDOBJECT_NOT_FOUND"
   End If
End Function

Private Function getIdOggetto_fromBlock(wBlock As String) As String
   Dim wIdx As Long
   Dim wIdx2 As Long
   
   getIdOggetto_fromBlock = -1
   
   wIdx = InStr(1, wBlock, "FORMATTING_COMMAND_REQUEST(ID_OGGETTO=")
   
   If wIdx > 0 Then
      wIdx2 = InStr(wIdx + 37, wBlock, "#)")
      
      If wIdx2 > 0 And wIdx2 > wIdx Then
         getIdOggetto_fromBlock = mId(wBlock, wIdx + 38, wIdx2 - wIdx - 38)
      Else
         m_FlagError = "ERROR_RETCODE_FORMAT"
      End If
   Else
      m_FlagError = "ERROR_RETCODE_FORMAT"
   End If
End Function

'**************************************************************************************
'******  P R O P E R T Y   G E T / L E T / S E T  *************************************
'**************************************************************************************
'IDOGGETTO
Public Property Let IdOggetto(mVar As Long)
    m_IdOggetto = mVar
End Property

Public Property Get IdOggetto() As Long
    IdOggetto = m_IdOggetto
End Property

'MFLAGERROR
Public Property Let FlagError(mVar As String)
    m_FlagError = mVar
End Property

Public Property Get FlagError() As String
    FlagError = m_FlagError
End Property





