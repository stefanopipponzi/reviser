VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MafndF_Macro 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Macro Editor"
   ClientHeight    =   5910
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   10770
   Icon            =   "MafndF_Macro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5910
   ScaleWidth      =   10770
   ShowInTaskbar   =   0   'False
   Begin RichTextLib.RichTextBox RTBFind 
      Height          =   675
      Left            =   2400
      TabIndex        =   11
      Top             =   6060
      Visible         =   0   'False
      Width           =   1050
      _ExtentX        =   1852
      _ExtentY        =   1191
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MafndF_Macro.frx":08CA
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   210
      Top             =   5850
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":094C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":0AA8
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":0EFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":1218
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab2 
      Height          =   5835
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   4155
      _ExtentX        =   7329
      _ExtentY        =   10292
      _Version        =   393216
      TabOrientation  =   2
      Style           =   1
      Tabs            =   4
      TabsPerRow      =   5
      TabHeight       =   529
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "User Macros"
      TabPicture(0)   =   "MafndF_Macro.frx":1538
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "LswMacro"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "CmdErase"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "cmdUser2Sys"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "CmdNewMacro"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "System Macros"
      TabPicture(1)   =   "MafndF_Macro.frx":1554
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "LstAmb"
      Tab(1).Control(1)=   "cmdSYS2USER"
      Tab(1).Control(2)=   "LswMacroSys"
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "Functions"
      TabPicture(2)   =   "MafndF_Macro.frx":1570
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "LswFunctions"
      Tab(2).Control(1)=   "DescFun"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Objects"
      TabPicture(3)   =   "MafndF_Macro.frx":158C
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "LswObj"
      Tab(3).Control(1)=   "DescObj"
      Tab(3).ControlCount=   2
      Begin VB.CommandButton CmdNewMacro 
         Height          =   345
         Left            =   3630
         Picture         =   "MafndF_Macro.frx":15A8
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Create a new Macro"
         Top             =   90
         Width           =   345
      End
      Begin VB.ListBox LstAmb 
         BackColor       =   &H80000018&
         ForeColor       =   &H00C00000&
         Height          =   1230
         Left            =   -74580
         TabIndex        =   12
         Top             =   60
         Width           =   3135
      End
      Begin VB.CommandButton cmdSYS2USER 
         Height          =   345
         Left            =   -71340
         Picture         =   "MafndF_Macro.frx":16F2
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Move Selected System Macros To User Macro Group..."
         Top             =   60
         Width           =   345
      End
      Begin VB.CommandButton cmdUser2Sys 
         Height          =   345
         Left            =   3630
         Picture         =   "MafndF_Macro.frx":183C
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Move Selected User Macros to System Macro Group"
         Top             =   870
         Width           =   345
      End
      Begin VB.CommandButton CmdErase 
         Height          =   345
         Left            =   3630
         Picture         =   "MafndF_Macro.frx":1986
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Delete Selected Macro"
         Top             =   480
         Width           =   345
      End
      Begin MSComctlLib.ListView LswObj 
         Height          =   4065
         Left            =   -74640
         TabIndex        =   3
         Top             =   60
         Width           =   3165
         _ExtentX        =   5583
         _ExtentY        =   7170
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Loaded Objects"
            Object.Width           =   3528
         EndProperty
      End
      Begin MSComctlLib.ListView LswMacroSys 
         Height          =   4365
         Left            =   -74640
         TabIndex        =   5
         Top             =   1380
         Width           =   3165
         _ExtentX        =   5583
         _ExtentY        =   7699
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Macros Avaliable"
            Object.Width           =   7056
         EndProperty
      End
      Begin MSComctlLib.ListView LswFunctions 
         Height          =   4065
         Left            =   -74640
         TabIndex        =   6
         Top             =   60
         Width           =   3165
         _ExtentX        =   5583
         _ExtentY        =   7170
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Functions Avaliable"
            Object.Width           =   7056
         EndProperty
      End
      Begin MSComctlLib.ListView LswMacro 
         Height          =   5685
         Left            =   360
         TabIndex        =   2
         Top             =   60
         Width           =   3165
         _ExtentX        =   5583
         _ExtentY        =   10028
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Macros Avaliable"
            Object.Width           =   7056
         EndProperty
      End
      Begin VB.Label DescFun 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00C00000&
         Height          =   1515
         Left            =   -74610
         TabIndex        =   10
         Top             =   4200
         Width           =   3135
      End
      Begin VB.Label DescObj 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00C00000&
         Height          =   1515
         Left            =   -74640
         TabIndex        =   9
         Top             =   4200
         Width           =   3135
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Index           =   0
      Left            =   9750
      Top             =   1860
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":1AD0
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":1BE2
            Key             =   "Forward"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":1CF4
            Key             =   "Comp"
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTPublic 
      Height          =   435
      Left            =   990
      TabIndex        =   0
      Top             =   6090
      Visible         =   0   'False
      Width           =   465
      _ExtentX        =   820
      _ExtentY        =   767
      _Version        =   393217
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MafndF_Macro.frx":2010
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Index           =   1
      Left            =   4980
      Top             =   3690
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":2092
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":21A4
            Key             =   "Forward"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":22B6
            Key             =   "Selall"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Macro.frx":25D2
            Key             =   "DeSelAll"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5835
      Left            =   4200
      TabIndex        =   14
      Top             =   0
      Width           =   6525
      _ExtentX        =   11509
      _ExtentY        =   10292
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Macro Editor"
      TabPicture(0)   =   "MafndF_Macro.frx":28EE
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblCol"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblRow"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblLines"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Toolbar1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Macro"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "ChkForRow"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "Execute Macro"
      TabPicture(1)   =   "MafndF_Macro.frx":290A
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(1)=   "Toolbar2"
      Tab(1).Control(2)=   "SSTab3"
      Tab(1).ControlCount=   3
      Begin VB.Frame Frame1 
         Caption         =   "Objects' Selection"
         ForeColor       =   &H00000080&
         Height          =   1965
         Left            =   -74910
         TabIndex        =   16
         Top             =   870
         Width           =   3315
         Begin VB.CheckBox ChkOutSrc 
            Caption         =   "Output Objects"
            Height          =   195
            Left            =   120
            TabIndex        =   17
            Top             =   1680
            Width           =   1425
         End
         Begin MSComctlLib.TreeView TVSelObj 
            Height          =   1335
            Left            =   120
            TabIndex        =   18
            Top             =   300
            Width           =   3015
            _ExtentX        =   5318
            _ExtentY        =   2355
            _Version        =   393217
            Indentation     =   529
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   7
            Checkboxes      =   -1  'True
            Appearance      =   1
         End
      End
      Begin VB.CheckBox ChkForRow 
         Caption         =   "Execution for Row"
         Height          =   255
         Left            =   1200
         TabIndex        =   15
         Top             =   480
         Width           =   1695
      End
      Begin MSComctlLib.Toolbar Toolbar2 
         Height          =   390
         Left            =   -74910
         TabIndex        =   19
         Top             =   480
         Width           =   6345
         _ExtentX        =   11192
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         Appearance      =   1
         ImageList       =   "imlToolbarIcons(0)"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Forward"
               Object.ToolTipText     =   "Run Macro"
               ImageIndex      =   2
            EndProperty
         EndProperty
      End
      Begin RichTextLib.RichTextBox Macro 
         Height          =   4695
         Left            =   30
         TabIndex        =   20
         Top             =   840
         Width           =   6405
         _ExtentX        =   11298
         _ExtentY        =   8281
         _Version        =   393217
         ScrollBars      =   3
         RightMargin     =   80000
         TextRTF         =   $"MafndF_Macro.frx":2926
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   390
         Left            =   60
         TabIndex        =   21
         Top             =   420
         Width           =   1050
         _ExtentX        =   1852
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         Appearance      =   1
         ImageList       =   "imlToolbarIcons(0)"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Save"
               Object.ToolTipText     =   "Save Macro"
               ImageKey        =   "Save"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Forward"
               Object.ToolTipText     =   "Compile Macro"
               ImageKey        =   "Comp"
            EndProperty
         EndProperty
      End
      Begin TabDlg.SSTab SSTab3 
         Height          =   2805
         Left            =   -74910
         TabIndex        =   22
         Top             =   2880
         Width           =   6315
         _ExtentX        =   11139
         _ExtentY        =   4948
         _Version        =   393216
         TabOrientation  =   2
         Style           =   1
         Tabs            =   1
         TabsPerRow      =   1
         TabHeight       =   520
         ForeColor       =   128
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Project's Log"
         TabPicture(0)   =   "MafndF_Macro.frx":29A6
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "RtLogPrj"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         Begin RichTextLib.RichTextBox RtLogPrj 
            Height          =   2625
            Left            =   360
            TabIndex        =   23
            Top             =   60
            Width           =   5895
            _ExtentX        =   10398
            _ExtentY        =   4630
            _Version        =   393217
            Enabled         =   -1  'True
            ReadOnly        =   -1  'True
            ScrollBars      =   3
            RightMargin     =   10000
            TextRTF         =   $"MafndF_Macro.frx":29C2
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Label lblLines 
         BorderStyle     =   1  'Fixed Single
         Height          =   240
         Left            =   3960
         TabIndex        =   26
         Top             =   5520
         Width           =   855
      End
      Begin VB.Label lblRow 
         BorderStyle     =   1  'Fixed Single
         Height          =   240
         Left            =   4800
         TabIndex        =   25
         Top             =   5520
         Width           =   735
      End
      Begin VB.Label lblCol 
         BorderStyle     =   1  'Fixed Single
         Height          =   240
         Left            =   5520
         TabIndex        =   24
         Top             =   5520
         Width           =   855
      End
   End
End
Attribute VB_Name = "MafndF_Macro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim IndMacro As Long
Dim IndMacroSys As Long

Private Sub CmdErase_Click()
  On Error GoTo EH
  
  If Not (LswMacro.SelectedItem Is Nothing) Then
    Screen.MousePointer = vbHourglass
    Elimina_MacroUSER_Da_DB LswMacro.ListItems(IndMacro)
    Macro.Text = ""
    'Elimina a video
    LswMacro.ListItems.Remove IndMacro
    Macro.Text = ""
    Screen.MousePointer = vbDefault
  End If
  
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub CmdNewMacro_Click()
  If Toolbar1.Buttons("Save").Enabled Then
    SaveYN
  End If
  Macro.Text = "Function NewMacro()" & vbCrLf & vbCrLf & "End Function"
  Toolbar1.Buttons("Save").Enabled = True
  Toolbar1.Buttons("Forward").Enabled = True
End Sub

Private Sub cmdSYS2USER_Click()
  Dim TbSys As Recordset, TbUte As Recordset
  Dim wNomeMac As String
  Dim Sintax As String, PerLinea As Integer
  
  On Error GoTo EH
  
  If IndMacroSys = 0 Then Exit Sub
  gbMacroTextChanged = False
  wNomeMac = LswMacroSys.ListItems(IndMacroSys)
  
  ' controllo se la Macro di Sistema esiste
  Set TbSys = m_fun.Open_Recordset_Sys("select * from SysMacro where nome = '" & wNomeMac & "'")
  If TbSys.RecordCount = 0 Then
    MsgBox "System Macro " & wNomeMac & " doesn't exist", vbCritical, "i-4.Migration - Macro"
    TbSys.Close
    Exit Sub
  Else
    Sintax = TbSys!Sintax
    PerLinea = TbSys!PerLinea
    
    TbSys.Delete
    TbSys.Close
  End If
  
  ' Controllo se esiste gi� sul progetto
  Set TbUte = m_fun.Open_Recordset("select * from Ts_Macro where nome = '" & wNomeMac & "'")
  If TbUte.RecordCount Then
    MsgBox "User Macro " & wNomeMac & " already exists", vbCritical, "i-4.Migration - Macro"
    TbUte.Close
    Exit Sub
  Else
    TbUte.AddNew
    TbUte!Nome = wNomeMac
    TbUte!Sintax = Sintax
    TbUte!PerLinea = PerLinea
    
    TbUte.Update
    TbUte.Close
  End If
  
  LswMacroSys.ListItems.Remove IndMacroSys
  IndMacroSys = 0
  cmdSYS2USER.Enabled = False
  LswMacro.ListItems.Add , , wNomeMac, , 1
  
  LswMacro.ListItems(LswMacro.ListItems.Count).Selected = True
  LswMacro_DblClick
  
  SSTab2.Tab = 0
  SSTab2_Click 1
    
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub cmdUser2Sys_Click()
  Dim TbSys As Recordset, TbUte As Recordset
  Dim wNomeMac As String
      
  On Error GoTo EH
    
  If IndMacro = 0 Then Exit Sub

  wNomeMac = LswMacro.ListItems(IndMacro)
  Set TbUte = m_fun.Open_Recordset("select * from Ts_Macro where nome = '" & wNomeMac & "'")
  If TbUte.RecordCount = 0 Then
    MsgBox "User Macro " & wNomeMac & " doesn't exist", vbCritical, "i-4.Migration - Macro"
    TbUte.Close
    Exit Sub
  End If
  
  Set TbSys = m_fun.Open_Recordset_Sys("select * from SysMacro where nome = '" & wNomeMac & "'")
  If TbSys.RecordCount > 0 Then
    MsgBox "System Macro " & wNomeMac & " already exist", vbCritical, "i-4.Migration - Macro"
    TbUte.Close
    TbSys.Close
    Exit Sub
  Else
    TbSys.AddNew
    TbSys!Nome = wNomeMac
    TbSys!Sintax = TbUte!Sintax
    TbSys!changeable = True
    TbSys!Area = "Generic"
    TbSys!PerLinea = TbUte!PerLinea
    TbSys.Update
  End If
  TbUte.Delete
  TbUte.Close
  
  TbSys.Close
  
  LswMacro.ListItems.Remove IndMacro
  IndMacro = 0
  LswMacroSys.ListItems.Add , , wNomeMac, , 2
  LswMacroSys.ListItems(LswMacroSys.ListItems.Count).Tag = "MOD"
  LswMacroSys.ListItems(LswMacroSys.ListItems.Count).Selected = True
  LswMacroSys_DblClick
  
  SSTab2.Tab = 1
  SSTab2_Click 0
    
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
   
  If m_fun.FnProcessRunning Then
    wScelta = m_fun.Show_MsgBoxError("FB01I")
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Set SCR = Nothing
  
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub LstAmb_DblClick()
  Carica_Lista_Macro LswMacro, LswMacroSys, SCR
End Sub

Private Sub LswFunctions_Click()
  Dim K As Integer
   
  On Error GoTo EH
   
  DescFun.Caption = ""
  For K = 1 To UBound(McF)
    If McF(K).Name = LswFunctions.SelectedItem.Text Then
      DescFun.Caption = McF(K).Name & vbCrLf & McF(K).Description
      Exit Sub
    End If
  Next K
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub LswMacro_Click()
  ' SelezionaMacro
  If Not (LswMacro.SelectedItem Is Nothing) Then
    IndMacro = LswMacro.SelectedItem.Index
  End If
End Sub

Private Sub LswMacroSys_DblClick()
  SelezionaMacro
End Sub

Sub SelezionaMacro()
  On Error GoTo EH
    
  If Toolbar1.Buttons("Save").Enabled Then
    SaveYN
  End If
  Screen.MousePointer = vbHourglass
  
  Toolbar1.Buttons("Save").Enabled = False
  Toolbar1.Buttons("Forward").Enabled = False
  IndMacroSys = LswMacroSys.SelectedItem.Index
  
  If LswMacroSys.SelectedItem.Tag = "MOD" Then
    cmdSYS2USER.Enabled = True
  Else
    cmdSYS2USER.Enabled = False
  End If
  Carica_Codice_Macro Macro, LswMacroSys.SelectedItem, "SYS"
  Screen.MousePointer = vbDefault
  
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub LswObj_Click()
  Dim K As Integer
   
  On Error GoTo EH
   
  DescObj.Caption = ""
  For K = 1 To UBound(McV)
    If McV(K).Name = LswObj.SelectedItem.Text Then
      DescObj.Caption = McV(K).Name & vbCrLf & _
                        McV(K).Description & IIf(McV(K).ReadOnly, vbCrLf & "READ ONLY", vbNullString)
      Exit Sub
    End If
  Next K
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Macro_KeyDown(KeyCode As Integer, Shift As Integer)
  Dim lCurrentPosition As Long
  Dim sLeftString As String
  Dim sRightString As String
    
  On Error GoTo EH
    
  ' Tab Key Pressed
  If KeyCode = 9 Then
    lCurrentPosition = Macro.SelStart
 
    sLeftString = Left(Macro.Text, lCurrentPosition)
    sRightString = Right(Macro.Text, Len(Macro.Text) - lCurrentPosition)
        
    ' Insert 4 Spaces
    Macro.Text = sLeftString & Space(4) & sRightString
    Macro.SelStart = lCurrentPosition + Len(Space(4))
    KeyCode = 0
  Else
    gbMacroTextChanged = True
  End If
  'silvia
  Me.lblLines.Caption = "Lines: " & CStr(GetLineCount(Me.Macro))
  Me.lblRow.Caption = "Row: " & CStr(GetLineNum(Me.Macro) + 1)
  Me.lblCol.Caption = "Col: " & CStr(GetColPos(Me.Macro) + 1)
  'silvia
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Macro_KeyPress(KeyAscii As Integer)
  On Error GoTo EH

  gbMacroTextChanged = True
  'silvia
  Me.lblLines.Caption = "Lines: " & CStr(GetLineCount(Me.Macro))
  Me.lblRow.Caption = "Row: " & CStr(GetLineNum(Me.Macro) + 1)
  Me.lblCol.Caption = "Col: " & CStr(GetColPos(Me.Macro) + 1)
  'silvia
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Macro_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  'silvia
  Me.lblLines.Caption = "Lines: " & CStr(GetLineCount(Me.Macro))
  Me.lblRow.Caption = "Row: " & CStr(GetLineNum(Me.Macro) + 1)
  Me.lblCol.Caption = "Col: " & CStr(GetColPos(Me.Macro) + 1)
  'silvia
End Sub

Private Sub SSTab2_Click(PreviousTab As Integer)
  On Error GoTo EH

  GMcTabSel = SSTab2.Tab
  LswObj.Visible = False
  LswFunctions.Visible = False
  LswMacro.Visible = False
  LswMacroSys.Visible = False
  CmdErase.Visible = False
  cmdUser2Sys.Visible = False
  cmdSYS2USER.Visible = False
  DescFun.Visible = False
  DescObj.Visible = False
  
  Select Case SSTab2.Tab
    Case 0
      LswMacro.Visible = True
      CmdErase.Visible = True
      cmdUser2Sys.Visible = True
    Case 1
      LswMacroSys.Visible = True
      cmdSYS2USER.Visible = True
    Case 2
      LswFunctions.Visible = True
      DescFun.Visible = True
    Case 3
      LswObj.Visible = True
      DescObj.Visible = True
  End Select
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim K As Integer, L As Integer
  Dim NomeMacro As String
  Dim SwJcl As Boolean
  Dim wNome As String
  Dim wEstensione As String
  Dim wTipo As String
  Dim wIdObj As Long
  Dim r As Recordset
  Dim NumFile As Integer
  Dim rObj As Recordset, TbObj As Recordset
  Dim PathFile As String, NomeFile As String, wRec As String
  Dim wStrInput As String, wStrOutput As String
  Dim Fs As Object
  
  On Error GoTo EH
  
  Screen.MousePointer = vbHourglass
  
  Select Case Button.key
    Case "Forward"
      m_fun.FnProcessRunning = True

      m_fun.initScriptControl
      RtLogPrj.Text = "Processing started at " & Now() & vbCrLf
      
      'Carica La lista degli oggetti
      Carica_Oggetti_For_Process
      
      NomeMacro = ""
      SwJcl = False
      MafndF_RunMacro.Show
      
      ''''''''''''''''''''''''''''''''''''''''''''''
      ' Macro UTENTE
      ''''''''''''''''''''''''''''''''''''''''''''''
      For K = 1 To LswMacro.ListItems.Count
        DoEvents
        If LswMacro.ListItems(K).Checked Then
          NomeMacro = LswMacro.ListItems(K).Text
          If NomeMacro <> vbNullString Then
            For L = 1 To UBound(McObjsel)
              If Len(McObjsel(L).Extension) Then
                wNome = McObjsel(L).Name & "." & McObjsel(L).Extension
              Else
                wNome = McObjsel(L).Name
              End If
              wTipo = Trim(UCase(McObjsel(L).type))
              wIdObj = Val(McObjsel(L).ObjectID)
              Set r = m_fun.Open_Recordset("Select * From TS_Macro where nome = '" & NomeMacro & "'")
              If r!PerLinea Then
                ExecuteMacro wIdObj, NomeMacro, ChkOutSrc
              Else
                PathFile = Replace(McObjsel(L).Directory, "$", m_fun.FnPathDef)
                If ChkOutSrc.Value = vbChecked Then
                  PathFile = PathFile & "\out\"
                End If
                NomeFile = PathFile & "\" & wNome
                
                wStrInput = ""
                NumFile = FreeFile
                Open NomeFile For Input As NumFile
                wStrInput = Input(FileLen(NomeFile), #NumFile)
                Close NumFile
                ''''''''''''''''''''''''''''''''''
                ' Esecuzione Macro
                ''''''''''''''''''''''''''''''''''
                wStrOutput = m_fun.ExecuteMacro(CLng(wIdObj), NomeMacro, wStrInput)
                
                If wStrOutput <> wStrInput Then
                  '''''''''''''''''''''''''''''''''
                  ' File da modificare
                  '''''''''''''''''''''''''''''''''
                  Set TbObj = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & wIdObj)
                  TbObj!ParsingLevel = 0
                  TbObj!DtParsing = Null
                  TbObj!errlevel = Null
                  TbObj.Update
                  TbObj.Close
                  
                  If Dir(PathFile & "\out", vbDirectory) = "" Then
                    MkDir PathFile & "\out"
                  End If
                  If Dir(PathFile & "\bck", vbDirectory) = "" Then
                    MkDir PathFile & "\bck"
                  End If
                  
                  '!!!!!!!backup!!!!!!!!!!
                  Set Fs = CreateObject("Scripting.FileSystemObject")
                  Fs.copyfile NomeFile, PathFile & "\bck\" & wNome
                  
                  '''''''''''''''''''''''''''''''''''''''
                  ' Sovrascrittura file!
                  '''''''''''''''''''''''''''''''''''''''
                  'SQ madrid
                  RTBFind.fileName = NomeFile
                  RTBFind.Text = wStrOutput
                  RTBFind.SaveFile NomeFile, 1
                End If
              End If
            Next L
          End If
        End If
      Next K
      
      ''''''''''''''''''''''''''''''''''''''''''''''
      ' Macro di SISTEMA
      ''''''''''''''''''''''''''''''''''''''''''''''
      For K = 1 To LswMacroSys.ListItems.Count
        If LswMacroSys.ListItems(K).Checked Then
          NomeMacro = LswMacroSys.ListItems(K).Text
          If NomeMacro <> vbNullString Then
            For L = 1 To UBound(McObjsel)
              If ChkOutSrc.Value = vbChecked Then
                wNome = McObjsel(L).Directory & "\out\" & McObjsel(L).Name
              Else
                wNome = McObjsel(L).Directory & "\" & McObjsel(L).Name
              End If
              wEstensione = McObjsel(L).Extension
              If Len(McObjsel(L).Extension) Then
                wNome = wNome & "." & wEstensione
              End If
              wTipo = Trim(UCase(McObjsel(L).type))
              wIdObj = Val(McObjsel(L).ObjectID)
              Set r = m_fun.Open_Recordset_Sys("Select * From SysMacro where nome = '" & NomeMacro & "'")
              If r!PerLinea Then
                ExecuteMacro wIdObj, NomeMacro, ChkOutSrc
              Else
                wStrInput = ""
                'Carica la stringa con tutto il sorgente del programma
                PathFile = m_fun.FnPathDef
                
                Set rObj = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & wIdObj)
                PathFile = Replace(rObj!Directory_Input, "$", PathFile)
                'silvia
                NomeFile = PathFile & "\" & rObj!Nome & IIf(Len(wEstensione), "." & wEstensione, "")
                
                NumFile = FreeFile
                Open NomeFile For Input As NumFile
                While Not EOF(NumFile)
                  Line Input #NumFile, wRec
                  wStrInput = wStrInput & wRec & vbCrLf
                  wRec = ""
                Wend
                Close NumFile
                       
                '''''''''''''''''''''''''''
                ' Esecuzione Macro:
                '''''''''''''''''''''''''''
                wStrOutput = m_fun.ExecuteMacro(CLng(wIdObj), NomeMacro, wStrInput)
                If wStrOutput <> wStrInput Then
                  'Reset segnalazioni!
                  Set TbObj = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & wIdObj)
                  TbObj!ParsingLevel = 0
                  TbObj!DtParsing = Null
                  TbObj!errlevel = Null
                  TbObj.Update
                   
                  If Dir(PathFile & "\out", vbDirectory) = "" Then
                    MkDir PathFile & "\out"
                  End If
                  If Dir(PathFile & "\bck", vbDirectory) = "" Then
                    MkDir PathFile & "\bck"
                  End If
                  Set Fs = CreateObject("Scripting.FileSystemObject")
                  'SQ !!!!!!!backup!!!!!!!!!!
                  Fs.copyfile PathFile & "\" & rObj!Nome & IIf(Len(wEstensione), "." & wEstensione, ""), PathFile & "\bck\" & rObj!Nome & IIf(Len(wEstensione), "." & wEstensione, "")
                   
                  '''''''''''''''''''''''''''''''''''''''
                  ' Sovrascrittura file!
                  '''''''''''''''''''''''''''''''''''''''
                  'SQ madrid
                  RTBFind.fileName = NomeFile
                  RTBFind.Text = wStrOutput
                  RTBFind.SaveFile NomeFile, 1
                End If
              End If
            Next L
          End If
        End If
      Next K
      RtLogPrj.Text = RtLogPrj.Text & "Processing ended at " & Now()
      
      'SQ prova
      m_fun.closeScriptControl
      m_fun.FnProcessRunning = False
      
  End Select
  Screen.MousePointer = vbDefault
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub LswMacro_DblClick()
  On Error GoTo EH
  
  ' SelezionaMacro
  If Not (LswMacro.SelectedItem Is Nothing) Then
    If Toolbar1.Buttons("Save").Enabled Then
      SaveYN
    End If
    IndMacro = LswMacro.SelectedItem.Index
    Screen.MousePointer = vbHourglass
    Toolbar1.Buttons("Save").Enabled = True
    Toolbar1.Buttons("Forward").Enabled = True
    Carica_Codice_Macro Macro, LswMacro.SelectedItem, "USER"
    Screen.MousePointer = vbDefault
  End If
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  On Error GoTo EH
 
  Select Case SSTab1.Tab
    Case 0
      Macro.Visible = True
    Case 1
      Macro.Visible = False
  End Select
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim Codice As String, NomeSub As String
  
  On Error GoTo EH
  
  'Carica il codice
  Codice = Macro.Text
  NomeSub = Trova_Name_Sub(Codice)
  
  If Trim(NomeSub) <> vbNullString Then
    Screen.MousePointer = vbHourglass
    Codice = Sostituisci_Carattere(Codice, vbCrLf, "@")
    Select Case Button.key
      Case "Save"
        SaveMacro
      
      Case "Forward"
        MafndF_RunMacro.Show
        MafndF_RunMacro.RtBVb = ""
        MafndF_RunMacro.RtBVb.selText = "Start compile: " & Now & vbCrLf
        
        SaveYN
          
        Codice = Sostituisci_Carattere(Codice, "@", vbCrLf)
        Esegui_Macro NomeSub, Codice
        Carica_Codice_Macro Macro, NomeSub, "TRY_BOTH"
        SCR.Reset
          
        MafndF_RunMacro.RtBVb.selText = "End compile: " & Now
    End Select
    Screen.MousePointer = vbDefault
  End If
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
  SCR.Reset
End Sub

Private Sub Form_Load()
  Dim K As Integer
  Dim NumMac As Long

  On Error GoTo EH
  
  Screen.MousePointer = vbHourglass
  'Tab di default
      
  GMcTabSel = 0
  cmdSYS2USER.Enabled = False
  
  'Carica la TreeView Progetto
  TVSelObj.Nodes.Add , , "SEL", "Only Selected Objects"
  
  TVSelObj.Nodes.Add , , "JOB", "JOB"
  TVSelObj.Nodes.Add "JOB", tvwChild, "JCL", "JCL"
  TVSelObj.Nodes.Add "JOB", tvwChild, "PRC", "Procedure"
  TVSelObj.Nodes.Add "JOB", tvwChild, "PRM", "Parameter"
  
  TVSelObj.Nodes.Add , , "DLI", "Dli"
  TVSelObj.Nodes.Add "DLI", tvwChild, "PSB", "Psb"
  TVSelObj.Nodes.Add "DLI", tvwChild, "DBD", "Dbd"
  
  TVSelObj.Nodes.Add , , "SRC", "Sources"
  TVSelObj.Nodes.Add "SRC", tvwChild, "BMS", "Mapset"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CBL", "Cobol Programs"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CPY", "Cobol Copies"
  
  LstAmb.AddItem "All"
  LstAmb.AddItem "Generic"
  LstAmb.AddItem "Migration - Generic"
  LstAmb.AddItem "Migration - MTP"
  LstAmb.AddItem "Migration - SQL"
  LstAmb.AddItem "Migration - IMS/DB"
  LstAmb.AddItem "Migration - IMS/DC"
  LstAmb.AddItem "Migration - MVS-JCL"
  LstAmb.AddItem "Migration - VSE-JCL"
  
  InitCls
    
  On Error Resume Next
  SCR.AddObject "MC", MOs, True
  On Error GoTo EH
    
  Carica_Lista_Oggetti LswObj, SCR
  Carica_Lista_Funzioni LswFunctions, SCR
    
  Carica_Lista_Macro LswMacro, LswMacroSys, SCR

  Screen.MousePointer = vbDefault

  SSTab2_Click 0
  
  gbMacroTextChanged = False
  
  If Trim(CbNomeMacro) = "" Then
    'MF 01/08/07 Se entrava qui dentro non aggiungeva la finestra aperta con AddActiveWindows
    m_fun.AddActiveWindows Me
    m_fun.FnActiveWindowsBool = True
  Else
    NumMac = 0
    SSTab1.TabEnabled(1) = False
    For K = 1 To LswMacro.ListItems.Count
      If UCase(LswMacro.ListItems(K).Text) = UCase(CbNomeMacro) Then
        NumMac = K
        LswMacro.ListItems(K).Selected = True
        LswMacro_DblClick
        Exit For
      End If
    Next K
    If NumMac = 0 Then
      For K = 1 To LswMacroSys.ListItems.Count
        If UCase(LswMacroSys.ListItems(K).Text) = UCase(CbNomeMacro) Then
          SSTab2.Tab = 1
          SSTab2_Click 0
          NumMac = K
          LswMacroSys.ListItems(K).Selected = True
          LswMacroSys_DblClick
          Exit For
        End If
      Next K
    End If
    If NumMac = 0 Then
      LswMacro.ListItems.Add , , CbNomeMacro
      Macro.Text = "Function " & CbNomeMacro & "()" & vbCrLf & vbCrLf & "End Function " & vbCrLf
    End If
    
    SSTab1.Tab = 0
    
    m_fun.AddActiveWindows Me
    m_fun.FnActiveWindowsBool = True
  End If
  
  Exit Sub
LOadSCR:
  Set SCR = MafndF_RunMacro.SC
  Resume Next
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
  Resume Next
End Sub

Public Sub Carica_Oggetti(RT As RichTextBox)
  Dim Ogg As String, Var As String
  Dim ix As Long, vx As Long
  Dim idx As Long, St As Long
  Dim Riga As String
  
  On Error GoTo EH
  
  'Legge Una riga alla volta
  idx = RT.Find(vbCrLf, 1)
  St = 0
  
  While idx <> -1
    RT.SelStart = St
    RT.SelLength = idx - St
    
    Riga = RT.selText
    
    ix = InStr(1, UCase(Riga), "DIM ")
    vx = InStr(1, UCase(Riga), " AS ")
    
    Var = Trim(mId(Riga, ix + 3, vx - (ix + 3)))
    Ogg = Trim(mId(Riga, vx + 4))
    
    'Aggiunge Gli oggetti allo Script
    Aggiungi_Oggetti_Script Ogg, Var, SCR
    
    idx = RT.Find(vbCrLf, idx + 3)
  Wend
  
  RTPublic.selText = ""
  RTPublic.SelStart = 0
  RTPublic.SelLength = 0
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub TVSelObj_NodeCheck(ByVal Node As MSComctlLib.Node)
  Dim K1 As Integer, wKey As String
  Dim tb As Recordset
  
  On Error GoTo EH
  
  wKey = Node.key
  NumObjInMacro = 0
  
  Select Case Node.key
    Case "SEL"
      If Node.Checked Then
        For K1 = 1 To m_fun.FnObjList.ListItems.Count
          If m_fun.FnObjList.ListItems(K1).Selected = True Then
            NumObjInMacro = NumObjInMacro + 1
          End If
        Next K1
      End If
    
    Case Else
      If Node.Checked Then
        For K1 = Node.Index + 1 To Node.Index + Node.Children
          TVSelObj.Nodes(K1).Checked = True
        Next K1
      Else
        For K1 = Node.Index + 1 To Node.Index + Node.Children
          TVSelObj.Nodes(K1).Checked = False
        Next K1
      End If
  End Select
        
  If Not TVSelObj.Nodes(1).Checked Then
    For K1 = 2 To TVSelObj.Nodes.Count
      If TVSelObj.Nodes(K1).Checked Then
        wKey = TVSelObj.Nodes(K1).key
        If wKey = "MPS" Then
          wKey = "BMS"
        End If
        Set tb = m_fun.Open_Recordset("Select * from bs_Oggetti where tipo = '" & wKey & "'")
        If tb.RecordCount Then
          tb.MoveLast
          NumObjInMacro = NumObjInMacro + tb.RecordCount
        End If
        tb.Close
      End If
    Next K1
  End If
  
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub SaveYN()
  On Error GoTo EH
  
  If gbMacroTextChanged Then
    If MsgBox("Save Macro Changes (Y/N)?", vbYesNo, "Save Changes") = vbYes Then
      SaveMacro
    End If
    gbMacroTextChanged = False
  End If
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub SaveMacro()
  Dim Codice As String
  Dim PerLinea As Integer
  Dim NomeSub As String
  Dim SwAdd As Boolean
  Dim K As Integer

  On Error GoTo EH

  'Carica il codice
  Codice = Macro.Text
  PerLinea = ChkForRow.Value
  NomeSub = Trova_Name_Sub(Codice)
  
  If Trim(NomeSub) <> vbNullString Then
    Codice = Sostituisci_Carattere(Codice, vbCrLf, "@")
    Salva_Macro_User NomeSub, Codice, PerLinea
    SwAdd = True
    For K = 1 To LswMacro.ListItems.Count
      If LswMacro.ListItems(K).Text = NomeSub Then
        SwAdd = False
        Exit For
      End If
    Next K
    If SwAdd Then 'Aggiunge alla lista (a Video)
      LswMacro.ListItems.Add , , NomeSub, , 1
    End If
  End If
  gbMacroTextChanged = False
  Exit Sub
EH:
  Screen.MousePointer = vbDefault
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Public Sub InitCls()
  ReDim McF(0)
  ReDim McV(0)
    
  ' BS_Oggetti
  AddObject "BS_Obj_Available", "Is Object Available", True
  AddObject "BS_Obj_Batch", "Is Object Batch", True
  AddObject "BS_Obj_BelongsToArea", "Area Object Belongs To", True
  AddObject "BS_Obj_Cics", "Is Object Cics", True
  AddObject "BS_Obj_CurrentLine", "Current Execution Line", False
  AddObject "BS_Obj_CurrentIndex", "Current Collection Index", False
  AddObject "BS_Obj_Directory_Input", "Object Directory Input", True
  AddObject "BS_Obj_Directory_Output", "Object Directory Input", True
  AddObject "BS_Obj_Dli", "Is Object Dli", True
  AddObject "BS_Obj_DtImport", "Object Import Date", True
  AddObject "BS_Obj_DtEncapsulated", "Object Encapsulation Date", True
  AddObject "BS_Obj_DtParsing", "Object Parsing Date", True
  AddObject "BS_Obj_Extension", "Object Extension", True
  AddObject "BS_Obj_FileSection", "Is Object File Section", True
  AddObject "BS_Obj_ObjectID", "Object ID", True
  AddObject "BS_Obj_Ims", "Is Object Ims", True
  AddObject "BS_Obj_Level1", "Object Level 1", True
  AddObject "BS_Obj_Level2", "Object Level 2", True
  AddObject "BS_Obj_Mapping", "Is Object Mapping", True
  AddObject "BS_Obj_Name", "Object Name", True
  AddObject "BS_Obj_RowNum", "Object Row Number", True
  AddObject "BS_Obj_Path", "Object Path", True
  AddObject "BS_Obj_ParsingLevel", "Object Parsing Level", True
  AddObject "BS_Obj_Procedure", "Is Object Procedure", True
  AddObject "BS_Obj_RecArea", "Object's record  area  (string)", False
  AddObject "BS_Obj_Selected", "If Object is selected in Project List (True / False)", False
  AddObject "BS_Obj_Sql", "Is Object Sql", True
  AddObject "BS_Obj_Type", "Object Type", True
  AddObject "BS_Obj_Type_DBD", "Object DBD Type", True
  AddObject "BS_Obj_Type_JCL", "Object JCL Type", True
  AddObject "BS_Obj_Working", "Is Object Working", True
    
  AddObject "Parm_FormDateDB2", "Format of Date Field in DB2", True
  AddObject "Parm_FormDateOracle", "Format of Date Field in Oracle", True
  AddObject "Parm_DefValueChar", "Default Value of Char (VarChar, VarChar2) Field in Oracle", True
  AddObject "Parm_DefValueNum", "Default Value of Numeric  Field in Oracle", True
  AddObject "Parm_DefValueDate", "Default Value of Date Field in Oracle", True
  AddObject "Parm_DefValueTime", "Default Value of Time  Field in Oracle", True
  AddObject "Parm_DefValueTimeStamp", "Default Value of TimeStamp Field in Oracle", True
    
  ' PsRel_Obj
  AddObject "PsRel_ObjCalled_Count", "Caller Relationship Count", False
  AddObject "PsRel_ObjCalled_ID()", "Relationship ID Called Array", True
  AddObject "PsRel_ObjCalled_ParentID()", "Caller ID Array", True
  AddObject "PsRel_ObjCalled_Name()", "Called Object Name Array", True
  AddObject "PsRel_ObjCalled_RelType()", "Called/Caller Relationship Type Array", True
  AddObject "PsRel_ObjCalled_RelDetail()", "Called/Caller Relationship Detail Array", True

  AddObject "PsRel_ObjCaller_Count", "Caller Relationship Count", False
  AddObject "PsRel_ObjCaller_ID()", "Relationship ID Caller Array", True
  AddObject "PsRel_ObjCaller_ParentID()", "Called ID Array", True
  AddObject "PsRel_ObjCaller_Name()", "Caller Object Name Array", True
  AddObject "PsRel_ObjCaller_RelType()", "Called/Caller Relationship Type Array", True
  AddObject "PsRel_ObjCaller_RelDetail()", "Called/Caller Relationship Detail Array", True
    
  AddFunction "ABORT()", "ABORT Macro Execution"
  AddFunction "CreateEnvironment(ObjectID As Integer, MacroName As String)", "Opens New Macro Environment"
  AddFunction "FindPos(StringToFind As String, iStart As Integer) As Long", "Finds Position of String"
  AddFunction "FindRegExp(Source As String, Criteria As String, IgnoreCase As boolean ) As Object", "Find Criteria in Source using VbScript Regular Expression Object"
  AddFunction "GetCurrentRTBFileName() As String", "Returns Current RTB File Name"
  AddFunction "GetLineFromPos(CharPosition As Long) As Integer", "Finds Line of a Character Position"
  AddFunction "GetSubStr(Buffer As String, Tag as String) As String", "Extract Logical SubString, defined by a Tag, from a Buffer"
  AddFunction "GetSysin(Buffer As String) As String", "Extract Sysin, defined by EXEC, from a Buffer"
  AddFunction "ReplaceString(Expression As String, Find As String, Replacement As String) As String", "Replaces String"
  AddFunction "OpenFile(NameFile As String,Modality As String) As String", "Opens external file"
  
  AddFunction "WriteLog(Text as String, EchoLog as Boolean)", "Write string on Macro Log " & vbCrLf & "If EchoLog is True write the string on the system file log also"
  
  ' Funzioni per navigare le tabelle del db di progetto
  AddFunction "rsOpen(Sql as String) as Recordset", "Open a recordset"
  AddFunction "rsRecordCount(rs as Recordset) as Long", "Returns how many records are in a recordset"
  AddFunction "rsFieldValue(Sql as Recordset, Field as String) as String", "Returns the value of a field of a recordset"
  AddFunction "rsExecute(Sql as Recordset)", "Execute an instruction SQL"
  AddFunction "rsMoveFirst(Sql as Recordset)", "Move a recordset on the first record"
  AddFunction "rsMovePrev(Sql as Recordset)", "Move a recordset on the previous record"
  AddFunction "rsMoveNext(Sql as Recordset)", "Move a recordset on the next record"
  AddFunction "rsMoveLast(Sql as Recordset)", "Move a recordset on the last record"
  AddFunction "rsClose(Sql as Recordset)", "Close a recordset"
End Sub

Private Function AddFunction(NomeFun As String, Descrizione As String)
  Dim K As Integer
  
  K = UBound(McF) + 1
  ReDim Preserve McF(K)
  McF(K).Name = NomeFun
  McF(K).Description = Descrizione
End Function

Private Function AddObject(NomeFun As String, Descrizione As String, bReadOnly As Boolean)
  Dim K As Integer
  
  K = UBound(McV) + 1
  ReDim Preserve McV(K)
  McV(K).Name = NomeFun
  McV(K).Description = Descrizione
  McV(K).ReadOnly = bReadOnly
End Function
