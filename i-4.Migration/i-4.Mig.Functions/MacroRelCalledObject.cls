VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MacroRelCalledObject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Option Explicit

Public Key As String

Private Type ObjectCalledType
    PsRel_ObjCalled_ID As Long
    PsRel_ObjCalled_ParentID As Long
    PsRel_ObjCalled_Name As String
    PsRel_ObjCalled_RelType As String
    PsRel_ObjCalled_RelDetail As String
End Type

Private vObject As ObjectCalledType

' PsRel_Obj Table
Public Property Let PsRel_ObjCalled_ID(ByVal vData As Variant)
    vObject.PsRel_ObjCalled_ID = vData
End Property

Public Property Get PsRel_ObjCalled_ID() As Variant
   PsRel_ObjCalled_ID = vObject.PsRel_ObjCalled_ID
End Property

Public Property Let PsRel_ObjCalled_ParentID(ByVal vData As Variant)
    vObject.PsRel_ObjCalled_ParentID = vData
End Property

Public Property Get PsRel_ObjCalled_ParentID() As Variant
   PsRel_ObjCalled_ParentID = vObject.PsRel_ObjCalled_ParentID
End Property

Public Property Let PsRel_ObjCalled_Name(ByVal vData As Variant)
    vObject.PsRel_ObjCalled_Name = vData
End Property

Public Property Get PsRel_ObjCalled_Name() As Variant
   PsRel_ObjCalled_Name = vObject.PsRel_ObjCalled_Name
End Property

Public Property Let PsRel_ObjCalled_RelType(ByVal vData As Variant)
    vObject.PsRel_ObjCalled_RelType = vData
End Property

Public Property Get PsRel_ObjCalled_RelType() As Variant
   PsRel_ObjCalled_RelType = vObject.PsRel_ObjCalled_RelType
End Property

Public Property Let PsRel_ObjCalled_RelDetail(ByVal vData As Variant)
    vObject.PsRel_ObjCalled_RelDetail = vData
End Property

Public Property Get PsRel_ObjCalled_RelDetail() As Variant
   PsRel_ObjCalled_RelDetail = vObject.PsRel_ObjCalled_RelDetail
End Property
