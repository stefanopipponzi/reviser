VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MafndF_Maps 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " "
   ClientHeight    =   5760
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   9795
   Icon            =   "MafndF_Maps.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   9795
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ListView LswInit 
      Height          =   1695
      Left            =   5070
      TabIndex        =   8
      Top             =   6090
      Visible         =   0   'False
      Width           =   3645
      _ExtentX        =   6429
      _ExtentY        =   2990
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   2540
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTApp 
      Height          =   525
      Left            =   960
      TabIndex        =   7
      Top             =   6180
      Visible         =   0   'False
      Width           =   2385
      _ExtentX        =   4207
      _ExtentY        =   926
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MafndF_Maps.frx":1D42
   End
   Begin VB.VScrollBar VS 
      Height          =   5025
      Left            =   9570
      TabIndex        =   6
      Top             =   450
      Width           =   225
   End
   Begin VB.HScrollBar HS 
      Height          =   225
      Left            =   3510
      TabIndex        =   5
      Top             =   5520
      Width           =   6045
   End
   Begin VB.Frame Base 
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Height          =   5055
      Left            =   3570
      TabIndex        =   2
      Top             =   450
      Width           =   5985
      Begin VB.Label CHAR 
         AutoSize        =   -1  'True
         BackColor       =   &H00000000&
         Caption         =   "A"
         BeginProperty Font 
            Name            =   "Fixedsys"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   225
         Index           =   0
         Left            =   60
         TabIndex        =   3
         Top             =   4740
         Visible         =   0   'False
         Width           =   120
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   6300
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Maps.frx":1DC4
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MafndF_Maps.frx":1F20
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   336
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9792
      _ExtentX        =   17277
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EXIT"
            Object.ToolTipText     =   "Expands Map..."
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Expand"
            ImageIndex      =   2
         EndProperty
      EndProperty
   End
   Begin VB.FileListBox File1 
      Height          =   285
      Left            =   3690
      TabIndex        =   0
      Top             =   6390
      Visible         =   0   'False
      Width           =   615
   End
   Begin MSComctlLib.TreeView TRW 
      Height          =   5325
      Left            =   0
      TabIndex        =   4
      Top             =   420
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   9393
      _Version        =   393217
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
End
Attribute VB_Name = "MafndF_Maps"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CHAR_Click(Index As Integer)
  If CHAR(Index).BackColor = vbGreen Then
    CHAR(Index).BackColor = vbBlack
    CHAR(Index).ForeColor = vbGreen
  Else
    CHAR(Index).BackColor = vbGreen
    CHAR(Index).ForeColor = vbBlack
  End If
End Sub

Private Sub Carica_MFS(IdOggetto As Long)
  Dim Percorso As String
  Dim i As Long
  Dim MapOk As Boolean
  
  Percorso = m_fun.Restituisci_Directory_Da_IdOggetto(IdOggetto)
  Percorso = Percorso & "\" & m_fun.Restituisci_NomeOgg_Da_IdOggetto(IdOggetto)
  
  'Analizza_Sorgente_MFS RTApp, Percorso -> prima riparserava file ora legge dal db
  ' eliminare quando la nuova procedura � stabile
  
  If RiempiStrutturaInfoMappa(IdOggetto) Then
    'Trasporta la struttura MFS in quella BMS
    Migrate_Structure_MFS2BMS
    
    MapOk = Carica_Mappa("", Base, 1, -1, True, False)
    
    If MapOk Then
      Carica_TreeView_Mappa TRW
      Carica_ListView_Initial LswInit, 1
    Else
      MsgBox "Map Error...", vbExclamation, m_fun.FnNomeProdotto
    End If
  
  End If
End Sub

Private Sub Carica_BMS(IdOggetto As Long)
  Dim Percorso As String
  Dim i As Long
  Dim MapOk As Boolean
  
  Percorso = m_fun.Restituisci_Directory_Da_IdOggetto(IdOggetto)
  
  Percorso = Percorso & "\" & m_fun.Restituisci_NomeOgg_Da_IdOggetto(IdOggetto)
  
  'La seguente funzione analizza un BMS e carica l'array in memoria
  Analizza_Sorgente_BMS RTApp, Percorso
 
  'Disegna la mappa sul video (La prima di default)
  Screen.MousePointer = vbHourglass
  
  MapOk = Carica_Mappa("", Base, 1, True, False, False)
  
  Screen.MousePointer = vbDefault
  
  If MapOk = True Then
    Carica_TreeView_Mappa TRW
    Carica_ListView_Initial LswInit, 1
  Else
    MsgBox "Map Error...", vbExclamation, m_fun.FnNomeProdotto
  End If
End Sub

Private Sub Form_Load()
  Dim i As Long
  Dim Tipo As String
  
  'Setta il parent
  SetParent Me.hwnd, m_fun.FnParent
  
  '2) Prende le sue dimensioni
  ResizeControls_MAPS Me
  
  XCellMap = 120 'CHAR(0).Width
  YCellMap = 200 'CHAR(0).Height
  
  HVisible = Base.Width / XCellMap
  VVisible = Base.Height / YCellMap
  
  Tipo = m_fun.Restituisci_TipoOgg_Da_IdOggetto(m_fun.FnIdOggetto)
  
  Select Case Tipo
    Case "BMS"
      Carica_BMS m_fun.FnIdOggetto
    Case "MFS"
      Carica_MFS m_fun.FnIdOggetto
    Case Else
      MsgBox "This Maps Type is not supported...", vbInformation, m_fun.FnNomeProdotto
  End Select
  NewC = True
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Public Function Carica_Mappa(NomeMappa As String, Base As Frame, IdxMap As Long, idxCmp As Long, Color As Boolean, Evidenzia As Boolean) As Boolean
  Dim i As Long
  On Error Resume Next
  Dim AppColor As Variant
  
  'Clear
  Screen.MousePointer = vbHourglass
  For i = CHAR.Count - 1 To 1 Step -1
   Unload CHAR(i)
  Next i
  
  If UBound(BMS.Maps) Then
    If HVisible < BMS.Maps(IdxMap).SizeX Then
      HS.Max = BMS.Maps(IdxMap).SizeX - HVisible + 3
    Else
      HS.Max = 0
    End If
    
    If VVisible < BMS.Maps(IdxMap).SizeY Then
      VS.Max = BMS.Maps(IdxMap).SizeY - VVisible + 3
    Else
      VS.Max = 0
    End If
    
    'Carica I campi
    For i = 1 To UBound(BMS.Maps(IdxMap).Campi)
      If BMS.Maps(IdxMap).Campi(i).Length <> 0 Then
        Load CHAR((CHAR.Count - 1) + 1)
        
        CHAR(CHAR.Count - 1).Visible = True
        
        If BMS.Maps(IdxMap).Campi(i).INITIAL = "" Then
            BMS.Maps(IdxMap).Campi(i).INITIAL = String(BMS.Maps(IdxMap).Campi(i).Length, ".")
        End If
        
        'Debug.Print i & "  " & BMS.Maps(IdxMap).Campi(i).INITIAL
        
        CHAR(CHAR.Count - 1).Caption = BMS.Maps(IdxMap).Campi(i).INITIAL
        
        CHAR(CHAR.Count - 1).Tag = BMS.Maps(IdxMap).Campi(i).NomeCmp 'NomeCampo
        
        CHAR(CHAR.Count - 1).Top = (YCellMap * BMS.Maps(IdxMap).Campi(i).PosY)
        CHAR(CHAR.Count - 1).Left = (XCellMap * BMS.Maps(IdxMap).Campi(i).PosX)
        
        If Evidenzia = True Then
           CHAR(CHAR.Count - 1).BackColor = &H808080 'Grigio scuro
        End If
        
        If Color = True Then
          CHAR(CHAR.Count - 1).ForeColor = Converti_Colore(BMS.Maps(IdxMap).Campi(i).Color)
        End If
        
        
        Select Case Trim(UCase(BMS.Maps(IdxMap).Campi(i).Hilight))
          Case "OFF", "BLINK"
          
          Case "REVERSE"
            AppColor = CHAR(CHAR.Count - 1).ForeColor
            CHAR(CHAR.Count - 1).ForeColor = CHAR(CHAR.Count - 1).BackColor
            CHAR(CHAR.Count - 1).BackColor = AppColor
            
          Case "UNDERLINE"
            CHAR(CHAR.Count - 1).FontUnderline = True
            
          Case Else
            '?????
            
        End Select
      End If
    Next i
    
    Carica_Mappa = True
    
    If idxCmp <> -1 Then
      For i = 1 To CHAR.Count - 1
         If UCase(Trim(CHAR(i).Tag)) = Trim(UCase(BMS.Maps(IdxMap).Campi(idxCmp).NomeCmp)) Then
            CHAR(i).BackColor = &H808080
            Exit For
         End If
      Next i
    End If
    
  Else
    Carica_Mappa = False
  End If
  
  Screen.MousePointer = vbDefault
End Function


Private Sub Form_Unload(Cancel As Integer)
   m_fun.RemoveActiveWindows Me
   m_fun.FnActiveWindowsBool = False
   
   SetFocusWnd m_fun.FnParent

End Sub

Private Sub HS_change()
  Dim i As Long
  
  Static OldH As Long
  
  If NewC Then
    OldH = 0
    NewC = False
  End If
  
  For i = 1 To CHAR.Count - 1
    If HS.Value < OldH Then
      CHAR(i).Left = CHAR(i).Left + XCellMap
    Else
      CHAR(i).Left = CHAR(i).Left - XCellMap
    End If
  Next i
  
  OldH = HS.Value
End Sub


Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim w As Single
  
  Select Case Trim(UCase(Button.key))
   Case "EXPAND"
      'Espande la mappa
      If Base.Tag = "" Then
         w = Base.Left + Base.Width
         Base.Left = TRW.Left
         Base.Top = TRW.Top
         Base.Width = w
         Base.Tag = "EXP"
         TRW.Visible = False
         HS.Width = Base.Width
         HS.Left = Base.Left
         VS.Left = Base.Left + Base.Width + 10
      Else
         Base.Left = TRW.Left + TRW.Width + 50
         Base.Top = TRW.Top
         Base.Width = Base.Width - 100 - TRW.Width
         Base.Tag = ""
         TRW.Visible = True
         HS.Width = Base.Width
         HS.Left = Base.Left
         VS.Left = Base.Left + Base.Width + 10
      End If
      
   Case "EXIT"
      Unload Me
  End Select
  
End Sub

Private Sub TRW_NodeClick(ByVal Node As MSComctlLib.Node)
  Dim NomeMappa As String
  Dim MapOk As Boolean
  Dim nMappa As Long
  Dim ncmp As Long
  
  m_fun.Elimina_Flickering_Object Base.hwnd
  NomeMappa = Replace(Node.Tag, "Name : ", "") & ".bms"
  
  If Trim(Node.Tag) <> "" Then
    nMappa = mId(Node.Tag, 1, 3)
  Else
    nMappa = -1
  End If
  
  If Trim(Node.key) <> "" And Len(Node.key) > 4 And _
     IsNumeric(Node.key) Then
    ncmp = mId(Node.key, 2, 2)
  Else
    ncmp = -1
  End If
  
  If nMappa > -1 Then
    MapOk = Carica_Mappa("", Base, nMappa, ncmp, True, False)
    If MapOk = True Then
      Carica_ListView_Initial LswInit, 1
      m_fun.Terminate_Flickering_Object
    Else
      m_fun.Terminate_Flickering_Object
      MsgBox "Map Error...", vbExclamation, m_fun.FnNomeProdotto
    End If
  End If
End Sub

Private Sub VS_Change()
  Dim i As Long
  
  Static OldV As Long
  
  If NewC Then
    OldV = 0
    NewC = False
  End If
  
  For i = 1 To CHAR.Count - 1
    If VS.Value > OldV Then
      CHAR(i).Top = CHAR(i).Top - YCellMap
    Else
      CHAR(i).Top = CHAR(i).Top + YCellMap
    End If
  Next i
  
  OldV = VS.Value
End Sub
