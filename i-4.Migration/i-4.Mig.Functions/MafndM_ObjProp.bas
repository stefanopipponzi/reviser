Attribute VB_Name = "MafndM_ObjProp"
Option Explicit

'Contiene la classe caricata con tutte le relazioni di un programma
Global cProgrammi As clsPgm
Global cFiltriPgm As New Collection

'Variabile contenente l'oggetto selezionato dal main sulla griglia
Public OggSel As Ogg

'Prende l'id oggetto dell'oggetto sellezionato sulle relazioni per poter andare in edit
Public m_IdOggetto_To_Edit As Long
Public m_IdOggetto_To_Expand As Long
Public m_KeyTreeViewSel As String
Public m_TipologiaChimate As e_ProfonditaRelazioni

Public Function HaveCalledChildren(Pgm As clsPgm) As Boolean
   Dim bChild As Boolean
   
   If Pgm.CountCalled > 0 Then
      bChild = True
   Else
      bChild = False
   End If
   HaveCalledChildren = bChild
End Function

Public Function HaveCallerChildren(Pgm As clsPgm) As Boolean
   Dim bChild As Boolean
   
   If Pgm.CountCaller > 0 Then
      bChild = True
   Else
      bChild = False
   End If
   HaveCallerChildren = bChild
End Function

Public Function Aggiungi_Nodo(TR As TreeView, Oggetto As clsPgm, cc As Long) As String
   Dim KeyPadre As String
   Dim KeyCur As String
   Dim cpy01 As clsCopy
   Dim psb01 As clsPsb
   Dim map01 As clsMappe
   Dim Key01 As String
   Dim Key_Padre As String
   Dim K As Long
   Dim Icon As Long
   
   On Error Resume Next
   
   Icon = Restituisci_Icona_Tipologia(Oggetto.Tipo)
   
   Select Case Trim(UCase(Oggetto.Tipo))
         Case "CBL", "COB"
            KeyCur = "COBOL" & Format(cc, "0000")
            KeyPadre = Format(cc, "0000") & Oggetto.Nome & Format(TR.Nodes.Count, "0000")
            TR.Nodes.Add KeyCur, tvwChild, KeyPadre, Oggetto.Nome, Icon
            TR.Nodes(TR.Nodes.Count).Tag = Oggetto.Id
            TR.Nodes(TR.Nodes.Count).Bold = True
            TR.Nodes(TR.Nodes.Count).ForeColor = vbBlue
            
         Case "ASM"
            KeyCur = "ASSEMBLER" & Format(cc, "0000")
            KeyPadre = Format(cc, "0000") & Oggetto.Nome & Format(TR.Nodes.Count, "0000")
            TR.Nodes.Add KeyCur, tvwChild, KeyPadre, Oggetto.Nome, Icon
            TR.Nodes(TR.Nodes.Count).Tag = Oggetto.Id
            TR.Nodes(TR.Nodes.Count).Bold = True
            TR.Nodes(TR.Nodes.Count).ForeColor = vbBlue
         
         Case "PRC"
            KeyCur = "PRC" & Format(cc, "0000")
            KeyPadre = Format(cc, "0000") & Oggetto.Nome & Format(TR.Nodes.Count, "0000")
            TR.Nodes.Add KeyCur, tvwChild, KeyPadre, Oggetto.Nome, Icon
            TR.Nodes(TR.Nodes.Count).Tag = Oggetto.Id
            TR.Nodes(TR.Nodes.Count).Bold = True
            TR.Nodes(TR.Nodes.Count).ForeColor = vbBlue
            
         Case "JCL", "JOB"
            KeyCur = "JCL" & Format(cc, "0000")
            KeyPadre = Format(cc, "0000") & Oggetto.Nome & Format(TR.Nodes.Count, "0000")
            TR.Nodes.Add KeyCur, tvwChild, KeyPadre, Oggetto.Nome, Icon
            TR.Nodes(TR.Nodes.Count).Tag = Oggetto.Id
            TR.Nodes(TR.Nodes.Count).Bold = True
            TR.Nodes(TR.Nodes.Count).ForeColor = vbBlue
         
         Case "VSM"
            KeyCur = "VSM" & Format(cc, "0000")
            KeyPadre = Format(cc, "0000") & Oggetto.Nome & Format(TR.Nodes.Count, "0000")
            TR.Nodes.Add KeyCur, tvwChild, KeyPadre, Oggetto.Nome, Icon
            TR.Nodes(TR.Nodes.Count).Tag = Oggetto.Id
            TR.Nodes(TR.Nodes.Count).Bold = True
            TR.Nodes(TR.Nodes.Count).ForeColor = vbBlue
         
         Case "RDBMS"
            KeyCur = "RDBMS" & Format(cc, "0000")
            KeyPadre = Format(cc, "0000") & Oggetto.Nome & Format(TR.Nodes.Count, "0000")
            TR.Nodes.Add KeyCur, tvwChild, KeyPadre, Oggetto.Nome, Icon
            TR.Nodes(TR.Nodes.Count).Tag = Oggetto.Id
            TR.Nodes(TR.Nodes.Count).Bold = True
            TR.Nodes(TR.Nodes.Count).ForeColor = vbBlue
            
         Case Else 'Unknow
            KeyCur = "UNKNOW" & Format(cc, "0000")
            KeyPadre = Format(cc, "0000") & Oggetto.Nome & Format(TR.Nodes.Count, "0000")
            TR.Nodes.Add KeyCur, tvwChild, KeyPadre, Oggetto.Nome & " - [" & Oggetto.Relazione & "]", Icon
            TR.Nodes(TR.Nodes.Count).Tag = Oggetto.Id
            TR.Nodes(TR.Nodes.Count).Bold = True
            TR.Nodes(TR.Nodes.Count).ForeColor = vbBlue
      End Select
      
      Aggiungi_Nodo = KeyPadre
End Function

Public Sub Aggiungi_Altri(TR As TreeView, Oggetto As clsPgm, cc As Long)
   Dim KeyPadre As String
   Dim KeyCur As String
   Dim cpy01 As clsCopy
   Dim psb01 As clsPsb
   Dim map01 As clsMappe
   Dim Key01 As String
   Dim Key_Padre As String
   Dim K As Long
   Dim Icon As Long
   
   On Error Resume Next
   
    'Aggiunge le Copy
      For K = 1 To Oggetto.CountCopy
         Set cpy01 = Oggetto.GetCopy(K)
         If (cpy01.Id = -1) Then
            Key01 = "UNKNOW" & Format(cc, "0000")
         Else
            Key01 = "COPY" & Format(cc, "0000")
         End If
         Icon = Restituisci_Icona_Tipologia("CPY")
         Key_Padre = Format(cc, "0000") & cpy01.Nome & Format(TR.Nodes.Count, "0000")
         TR.Nodes.Add Key01, tvwChild, Key_Padre, cpy01.Nome, Icon
         TR.Nodes(TR.Nodes.Count).Tag = cpy01.Id
         TR.Nodes(TR.Nodes.Count).Bold = True
         TR.Nodes(TR.Nodes.Count).ForeColor = vbBlue
      Next K

   
      'Aggiunge i Psb
      For K = 1 To Oggetto.CountPsb
         Set psb01 = Oggetto.GetPsb(K)
         If (psb01.Id = -1) Then
            Key01 = "UNKNOW" & Format(cc, "0000")
         Else
            Key01 = "PSB" & Format(cc, "0000")
         End If
         Icon = Restituisci_Icona_Tipologia("PSB")
         Key_Padre = Format(cc, "0000") & psb01.Nome & Format(TR.Nodes.Count, "0000")
         TR.Nodes.Add Key01, tvwChild, Key_Padre, psb01.Nome, Icon
         TR.Nodes(TR.Nodes.Count).Tag = psb01.Id
         TR.Nodes(TR.Nodes.Count).Bold = True
         TR.Nodes(TR.Nodes.Count).ForeColor = vbBlue
      Next K
   
      'Aggiunge le mappe
      For K = 1 To Oggetto.CountMaps
         Set map01 = Oggetto.GetMaps(K)
         If (map01.Id = -1) Then
            Key01 = "UNKNOW" & Format(cc, "0000")
         Else
            Key01 = "MAP" & Format(cc, "0000")
         End If
         Icon = Restituisci_Icona_Tipologia("MAP")
         Key_Padre = Format(cc, "0000") & map01.Nome & Format(TR.Nodes.Count, "0000")
         TR.Nodes.Add Key01, tvwChild, Key_Padre, map01.Nome, Icon
         TR.Nodes(TR.Nodes.Count).Tag = map01.Id
         TR.Nodes(TR.Nodes.Count).Bold = True
         TR.Nodes(TR.Nodes.Count).ForeColor = vbBlue
      Next K
End Sub

Public Function HaveAssembler(Pgm As clsPgm) As Boolean
   Dim i As Long
   Dim Trovato As Boolean
   
   For i = 1 To Pgm.CountCalled
      If Pgm.GetCalled(i).Tipo = "ASM" Then
         Trovato = True
         Exit For
      End If
   Next i
   
   HaveAssembler = Trovato
   
End Function

Public Function HaveProcedure(Pgm As clsPgm) As Boolean
   Dim i As Long
   Dim Trovato As Boolean
   
   For i = 1 To Pgm.CountCalled
      If Pgm.GetCalled(i).Tipo = "PRC" Then
         Trovato = True
         Exit For
      End If
   Next i
   
   HaveProcedure = Trovato
   
End Function

Public Function HaveJcl(Pgm As clsPgm) As Boolean
   Dim i As Long
   Dim Trovato As Boolean
   
   For i = 1 To Pgm.CountCalled
      If Pgm.GetCalled(i).Tipo = "JCL" Or Pgm.GetCalled(i).Tipo = "JOB" Then
         Trovato = True
         Exit For
      End If
   Next i
   
   HaveJcl = Trovato
   
End Function


Public Function HaveUnknown(Pgm As clsPgm) As Boolean
   Dim i As Long
   Dim Trovato As Boolean
   
   For i = 1 To Pgm.CountCalled
      If Pgm.GetCalled(i).Tipo = "UNK" Then
         Trovato = True
         Exit For
      End If
   Next i
   
   HaveUnknown = Trovato
   
End Function

Public Function HaveProgram(Pgm As clsPgm) As Boolean
   Dim i As Long
   Dim Trovato As Boolean
   
   For i = 1 To Pgm.CountCalled
      Select Case Pgm.GetCalled(i).Tipo
      
         Case "CBL", "COB", "PGM"
            Trovato = True
         
      End Select
   Next i
   
   HaveProgram = Trovato
   
End Function

Public Function HaveAssemblerCaller(Pgm As clsPgm) As Boolean
   Dim i As Long
   Dim Trovato As Boolean
   
   For i = 1 To Pgm.CountCaller
      If Pgm.GetCaller(i).Tipo = "ASM" Then
         Trovato = True
         Exit For
      End If
   Next i
   
   HaveAssemblerCaller = Trovato
   
End Function

Public Function HaveProcedureCaller(Pgm As clsPgm) As Boolean
   Dim i As Long
   Dim Trovato As Boolean
   
   For i = 1 To Pgm.CountCaller
      If Pgm.GetCaller(i).Tipo = "PRC" Then
         Trovato = True
         Exit For
      End If
   Next i
   
   HaveProcedureCaller = Trovato
   
End Function

Public Function HaveJclCaller(Pgm As clsPgm) As Boolean
   Dim i As Long
   Dim Trovato As Boolean
   
   For i = 1 To Pgm.CountCaller
      If Pgm.GetCaller(i).Tipo = "JCL" Or Pgm.GetCaller(i).Tipo = "JOB" Then
         Trovato = True
         Exit For
      End If
   Next i
   
   HaveJclCaller = Trovato
   
End Function


Public Function HaveUnknowCaller(Pgm As clsPgm) As Boolean
   Dim i As Long
   Dim Trovato As Boolean
   
   For i = 1 To Pgm.CountCaller
      If Pgm.GetCaller(i).Tipo = "UNK" Then
         Trovato = True
         Exit For
      End If
   Next i
   
   HaveUnknowCaller = Trovato
   
End Function

Public Function HaveProgramCaller(Pgm As clsPgm) As Boolean
   Dim i As Long
   Dim Trovato As Boolean
   
   For i = 1 To Pgm.CountCaller
      Select Case Pgm.GetCaller(i).Tipo
      
         Case "CBL", "COB", "PGM"
            Trovato = True
         
      End Select
   Next i
   
   HaveProgramCaller = Trovato
   
End Function

Public Function Restituisci_Icona_Tipologia(Tipo As String) As Long
  Select Case UCase(Trim(Tipo))
    Case "PGM", "CAL", "CBL"
      Restituisci_Icona_Tipologia = 12
    Case "DBD", "DBS", "DB"
      Restituisci_Icona_Tipologia = 4
    Case "RDBMS", "SQL"
      Restituisci_Icona_Tipologia = 10
    Case "CPY"
      Restituisci_Icona_Tipologia = 3
    Case "PSB"
      Restituisci_Icona_Tipologia = 7
    Case "MAP", "BMS"
      Restituisci_Icona_Tipologia = 6
    Case "JCL", "JOB"
      Restituisci_Icona_Tipologia = 5
    Case "DLI"
      Restituisci_Icona_Tipologia = 17
    Case "AGN", "TBL"
      Restituisci_Icona_Tipologia = 11
    Case "STR"
      Restituisci_Icona_Tipologia = 11
    Case "TRN"
      Restituisci_Icona_Tipologia = 11
    Case "IST"
      Restituisci_Icona_Tipologia = 15
    Case "PRC"
      Restituisci_Icona_Tipologia = 13
   Case "ASM"
      Restituisci_Icona_Tipologia = 14
   Case "VSM"
      Restituisci_Icona_Tipologia = 16
   Case "UNK"
      Restituisci_Icona_Tipologia = 9
   Case Else
      Restituisci_Icona_Tipologia = 11
  End Select
End Function
