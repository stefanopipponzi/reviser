Attribute VB_Name = "MabsdM_Parametri"
Option Explicit

'SQ farla unica!!
Global Const SYSTEM_DIR = "i-4.Mig.cfg"
Global Const FILE_INI = "\" & SYSTEM_DIR & "\i-4.Migration.ini"
Private Type Field
  Name As String
  type As String
  len As Long
  default As String
End Type
Public FieldNames() As Field

Public Sub Carica_TreeView_Parametri_Prodotto(Trw As TreeView, Percorso As String)
  Dim i As Long
  
  Trw.Nodes.Add , , "ENVIRONMENT", "Environment"
  Trw.Nodes(Trw.Nodes.Count).Tag = "L0"
  Trw.Nodes(Trw.Nodes.Count).Bold = True
  
  Trw.Nodes.Add "ENVIRONMENT", tvwChild, "GENERAL", "Various Parameters : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L1"
  Trw.Nodes(Trw.Nodes.Count).ForeColor = vbBlue
  Trw.Nodes(Trw.Nodes.Count).Bold = True
  Trw.Nodes.Add "GENERAL", tvwChild, "PATHDOC", "Documents Path : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L2"
  Trw.Nodes.Add "GENERAL", tvwChild, "HDERASE", "Erase Object Permanently (Yes/No) : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L2"
  Trw.Nodes.Add "ENVIRONMENT", tvwChild, "TEXTEDT", "Text Editor Parameters"
  Trw.Nodes(Trw.Nodes.Count).ForeColor = vbBlue
  Trw.Nodes(Trw.Nodes.Count).Bold = True
  Trw.Nodes(Trw.Nodes.Count).Tag = "L1"
  Trw.Nodes.Add "TEXTEDT", tvwChild, "COLTEXT", "Painted Source (Yes/No) : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L2"
  Trw.Nodes.Add "TEXTEDT", tvwChild, "FONTTXT", "Font Source : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L2"
  Trw.Nodes.Add "TEXTEDT", tvwChild, "FONTSIZ", "Font Size : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L2"
  Trw.Nodes.Add "ENVIRONMENT", tvwChild, "MFEPARA", "MFE Server's Parameters"
  Trw.Nodes(Trw.Nodes.Count).ForeColor = vbBlue
  Trw.Nodes(Trw.Nodes.Count).Bold = True
  Trw.Nodes(Trw.Nodes.Count).Tag = "L1"
  Trw.Nodes.Add "MFEPARA", tvwChild, "MFEPORT", "MFE Connection Port : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L2"
  Trw.Nodes.Add "MFEPARA", tvwChild, "MFENOME", "MFE Server's Name : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L2"
  Trw.Nodes.Add "MFEPARA", tvwChild, "MFE_IPNU", "MFE IP server's Number : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L2"

  'chekka tutti i nodi di default
  For i = 1 To Trw.Nodes.Count
    Trw.Nodes(i).Checked = True
  Next i
  
  'carica i parametri gi� memorizzati in precedenza
  Carica_Parametri_Prodotto_Da_INI_Su_TreeView Trw
End Sub

Public Sub Carica_TreeView_Parametri_Progetto(Trw As TreeView)
  Dim i As Long
  
  Trw.Nodes.Add , , "PROJECT", "Project"
  Trw.Nodes(Trw.Nodes.Count).Tag = "L0"
  Trw.Nodes(Trw.Nodes.Count).Bold = True
  Trw.Nodes.Add "PROJECT", tvwChild, "GENERAL", "Various Parameters : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L1"
  Trw.Nodes(Trw.Nodes.Count).ForeColor = vbBlue
  Trw.Nodes(Trw.Nodes.Count).Bold = True
  Trw.Nodes.Add "GENERAL", tvwChild, "PATHDOC", "Documents Path : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L2"
  Trw.Nodes.Add "PROJECT", tvwChild, "MFEPARA", "MFE Project's Parameters : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L1"
  Trw.Nodes(Trw.Nodes.Count).ForeColor = vbBlue
  Trw.Nodes(Trw.Nodes.Count).Bold = True
  Trw.Nodes.Add "MFEPARA", tvwChild, "MFEPATH", "MFE Project's Path : "
  Trw.Nodes(Trw.Nodes.Count).Tag = "L2"
End Sub

Public Sub Scrivi_File_INI(Trw As TreeView, Percorso As String)
  Dim i As Long
  Dim Root As String
  Dim Param As String
  Dim Voce As String
  Dim Bool As Boolean
  
  If Dir(Percorso, vbNormal) <> "" Then Kill Percorso
  
  For i = 1 To Trw.Nodes.Count
    'recupera le voci della treeview corrente
    Select Case Trw.Nodes(i).Tag
      Case "L0"
        Root = ""
        Voce = ""
        Param = ""
        Bool = True
      Case "L1"
        Root = Trw.Nodes(i).Key
        Bool = True
      Case "L2"
        Voce = Trw.Nodes(i).Key
        Param = Restituisci_Parametro_Da_Key_TreeView(Trw.Nodes(i).Key)
        Bool = False
    End Select
    
    If Not Bool Then
      If Trw.Nodes(i).Checked Then
        WritePrivateProfileString Root, Voce, Param, Percorso
      End If
    End If
  Next i
End Sub
  
Public Function Trova_Parametro_In_File_INI(Root As String, Key As String, ByVal Percorso As String) As String
  Dim Parametro As String, App As String
  
  App = String(255, 0)
  Key = Trim(Key)
  Parametro = GetPrivateProfileString(Root, Key, "", App, 255, Percorso)
  If Val(Parametro) <> 0 Then
    Parametro = Left$(App, Parametro)
  Else
    Parametro = ""
  End If
  Trova_Parametro_In_File_INI = Parametro
End Function

Public Function Restituisci_Parametro_Da_Key_TreeView(Key As String) As Variant
  Dim AppParam As Variant
  
  'Da aggiornare ma mano che si aggiungono le variabili di parametro
  Select Case Trim(UCase(Key))
    Case "PATHDOC"
      AppParam = Menu.BsPath_Document
      
      If Trim(AppParam) = "" Then
        AppParam = "C:\Documenti"
      End If
    
    Case "COLTEXT"
      AppParam = CLng(Menu.BsText_ColorRich)
      
    Case "FONTTXT"
      AppParam = Menu.BsFont
      
      If Trim(AppParam) = "" Then
        AppParam = "MS Sans Serif"
      End If
      
    Case "FONTSIZ"
      AppParam = Menu.BsFontSize
      
      If Trim(AppParam) = "0" Then
        AppParam = "8"
      End If
      
    Case "MFEPORT"
      AppParam = Menu.BsMFESvPort
      
    Case "MFENOME"
      AppParam = Menu.BsMFESvName
      
    Case "MFE_IPNU"
      AppParam = Menu.BsMFESvIP
  End Select
  
  Restituisci_Parametro_Da_Key_TreeView = AppParam
End Function

Public Sub Leggi_Parametri_Da_INI(Percorso)
  Dim ExFile As String
  
  ExFile = Dir(Percorso, vbNormal)
  
  If Trim(ExFile) = "" Then
    'il file non esiste : Carica i default
    Menu.BsText_ColorRich = False
    Menu.BsPath_Document = "C:\Documenti"
    Menu.BsFont = "MS Sans Serif"
    Menu.BsFontSize = "8"
    Menu.BsMFESvName = ""
    Menu.BsMFESvIP = ""
    Menu.BsMFESvPort = ""
    
    Menu.BsErase_File = False
  Else
    'carica i parametri dal file
    If Trova_Parametro_In_File_INI("TEXTEDT", "COLTEXT", Percorso) = "0" Then
      Menu.BsText_ColorRich = False
    Else
      Menu.BsText_ColorRich = True
    End If
    
    Menu.BsPath_Document = Trova_Parametro_In_File_INI("GENERAL", "PATHDOC", Percorso)
    If Trova_Parametro_In_File_INI("TEXTEDT", "COLTEXT", Percorso) = "" Then
      Menu.BsText_ColorRich = 0
    Else
      Menu.BsText_ColorRich = Trova_Parametro_In_File_INI("TEXTEDT", "COLTEXT", Percorso)
    End If
    
    If Trova_Parametro_In_File_INI("TEXTEDT", "FONTTXT", Percorso) = "" Then
      Menu.BsFont = "Curier New"
    Else
      Menu.BsFont = Trova_Parametro_In_File_INI("TEXTEDT", "FONTTXT", Percorso)
    End If
    
    If Trova_Parametro_In_File_INI("TEXTEDT", "FONTSIZ", Percorso) = "" Then
      Menu.BsFontSize = 8
    Else
      Menu.BsFontSize = Trova_Parametro_In_File_INI("TEXTEDT", "FONTSIZ", Percorso)
    End If
    
    Menu.BsMFESvName = Trova_Parametro_In_File_INI("MFEPARA", "MFENOME", Percorso)
    Menu.BsMFESvIP = Trova_Parametro_In_File_INI("MFEPARA", "MFE_IPNU", Percorso)
    Menu.BsMFESvPort = Trova_Parametro_In_File_INI("MFEPARA", "MFEPORT", Percorso)
    
    If Trim(UCase(Trova_Parametro_In_File_INI("GENERAL", "HDERASE", Percorso))) = "FALSE" Then
      Menu.BsErase_File = False
    Else
      Menu.BsErase_File = True
    End If
  End If
End Sub

Public Sub Carica_Parametri_Prodotto_Da_INI_Su_TreeView(Trw As TreeView)
  Dim idx As Long
  
  Leggi_Parametri_Da_INI Menu.BsPathPrd & FILE_INI
  
  idx = Restituisci_Index_TreeView_Per_Partametro(Trw, "PATHDOC")
  'assegna il parametro alla treeview
  If idx > 0 Then
    Trw.Nodes(idx).Text = Trw.Nodes(idx).Text & " " & Menu.BsPath_Document
  End If
  
  idx = Restituisci_Index_TreeView_Per_Partametro(Trw, "COLTEXT")
  'assegna il parametro alla treeview
  If idx > 0 Then
    If Menu.BsText_ColorRich Then
      Trw.Nodes(idx).Text = Trw.Nodes(idx).Text & " Yes"
    Else
      Trw.Nodes(idx).Text = Trw.Nodes(idx).Text & " No"
    End If
  End If
  
  idx = Restituisci_Index_TreeView_Per_Partametro(Trw, "FONTTXT")
  'assegna il parametro alla treeview
  If idx > 0 Then
    Trw.Nodes(idx).Text = Trw.Nodes(idx).Text & " " & Menu.BsFont
  End If
  
  idx = Restituisci_Index_TreeView_Per_Partametro(Trw, "FONTSIZ")
  'assegna il parametro alla treeview
  If idx > 0 Then
    Trw.Nodes(idx).Text = Trw.Nodes(idx).Text & " " & Menu.BsFontSize
  End If
  
  idx = Restituisci_Index_TreeView_Per_Partametro(Trw, "MFENOME")
  'assegna il parametro alla treeview
  If idx > 0 Then
    Trw.Nodes(idx).Text = Trw.Nodes(idx).Text & " " & Menu.BsMFESvName
  End If
  
  idx = Restituisci_Index_TreeView_Per_Partametro(Trw, "MFE_IPNU")
  'assegna il parametro alla treeview
  If idx > 0 Then
    Trw.Nodes(idx).Text = Trw.Nodes(idx).Text & " " & Menu.BsMFESvIP
  End If
  
  idx = Restituisci_Index_TreeView_Per_Partametro(Trw, "MFEPORT")
  'assegna il parametro alla treeview
  If idx > 0 Then
    Trw.Nodes(idx).Text = Trw.Nodes(idx).Text & " " & Menu.BsMFESvPort
  End If
End Sub

Public Function Restituisci_Index_TreeView_Per_Partametro(Trw As TreeView, Key As String) As Long
  Dim i As Long
  
  For i = 1 To Trw.Nodes.Count
    If Trim(UCase(Trw.Nodes(i).Key)) = Trim(UCase(Key)) Then
      Restituisci_Index_TreeView_Per_Partametro = i
      Exit For
    End If
  Next i
End Function

Public Sub Aggiorna_Voce_TreeView(Trw As TreeView, TagObj As String, Voce As String)
  Dim i As Long
  
  For i = 1 To Trw.Nodes.Count
    If Trim(UCase(Trw.Nodes(i).Key)) = Trim(UCase(TagObj)) Then
      Trw.Nodes(i).Text = Mid(Trw.Nodes(i).Text, 1, InStr(1, Trw.Nodes(i).Text, ":")) & " " & Voce
      Exit For
    End If
  Next i
End Sub

Public Function Isola_Parametro(Stringa As String, Sep As String) As String
  Dim Start As Long, Stopp As Long
  
  Start = InStr(1, Stringa, Sep)
  Stopp = InStr(Start + 1, Stringa, Sep)
  
  Isola_Parametro = Mid(Stringa, Start + 1, Stopp - Start - 1)
End Function
