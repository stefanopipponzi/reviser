VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MabsdF_Print 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Print Outputs"
   ClientHeight    =   6990
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   10125
   Icon            =   "MabsdF_Print.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6990
   ScaleWidth      =   10125
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Caption         =   "SQL Preview"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1980
      Left            =   60
      TabIndex        =   10
      Top             =   4950
      Width           =   10035
      Begin VB.TextBox txtSQL 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   1590
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   11
         Top             =   270
         Width           =   9855
      End
   End
   Begin VB.Frame FraDetails 
      Caption         =   "Report Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4425
      Left            =   5340
      TabIndex        =   3
      Top             =   450
      Width           =   4755
      Begin VB.CommandButton cmdTemplate 
         Caption         =   "..."
         Height          =   375
         Left            =   4200
         TabIndex        =   22
         Top             =   3405
         Width           =   375
      End
      Begin VB.TextBox txtTemplate 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1260
         TabIndex        =   7
         Top             =   3405
         Width           =   2895
      End
      Begin VB.TextBox txtF2 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1260
         TabIndex        =   5
         Top             =   690
         Width           =   3315
      End
      Begin VB.TextBox txtReportName 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1260
         TabIndex        =   8
         Top             =   3885
         Width           =   3315
      End
      Begin VB.TextBox txtF3 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1260
         TabIndex        =   6
         Top             =   1110
         Width           =   3315
      End
      Begin VB.TextBox txtF1 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1260
         TabIndex        =   4
         Top             =   270
         Width           =   3315
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Template Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   3480
         Width           =   1335
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   """"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Index           =   4
         Left            =   120
         TabIndex        =   19
         Top             =   810
         Width           =   1050
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   """"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Index           =   5
         Left            =   120
         TabIndex        =   18
         Top             =   1230
         Width           =   1050
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Header field"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   17
         Top             =   330
         Width           =   1050
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Report Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   3945
         Width           =   1260
      End
   End
   Begin VB.Frame FraSelOpt 
      Caption         =   "Selection Options"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4425
      Left            =   60
      TabIndex        =   1
      Top             =   450
      Width           =   5265
      Begin VB.CommandButton cmdModifica 
         Caption         =   "Edit"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   2265
         Picture         =   "MabsdF_Print.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Edit existing query..."
         Top             =   3525
         Width           =   795
      End
      Begin VB.CommandButton cmdCancella 
         Caption         =   "Erase"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   4380
         Picture         =   "MabsdF_Print.frx":0294
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Erase selected Query..."
         Top             =   3525
         Width           =   795
      End
      Begin VB.CommandButton cmdAggiungi 
         Caption         =   "Add"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   150
         Picture         =   "MabsdF_Print.frx":03DE
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Add new query for print report..."
         Top             =   3525
         Width           =   795
      End
      Begin MSComctlLib.ListView lswStampe 
         Height          =   3135
         Left            =   105
         TabIndex        =   2
         Top             =   240
         Width           =   5085
         _ExtentX        =   8969
         _ExtentY        =   5530
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Description"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Report"
            Object.Width           =   3528
         EndProperty
      End
   End
   Begin MSComDlg.CommonDialog Cmdlg 
      Left            =   9450
      Top             =   1290
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8400
      Top             =   60
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabsdF_Print.frx":0528
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabsdF_Print.frx":157A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MabsdF_Print.frx":16D6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10125
      _ExtentX        =   17859
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PRINT"
            Object.ToolTipText     =   "Generate Report"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ISTRDLI"
            Object.ToolTipText     =   "Print DLI Instruction"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Restore default queries"
            ImageIndex      =   3
         EndProperty
      EndProperty
      Begin VB.OptionButton optPrint 
         Caption         =   "TXT"
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   2
         Left            =   3720
         TabIndex        =   20
         Top             =   60
         Width           =   945
      End
      Begin VB.OptionButton optPrint 
         Caption         =   "Excel"
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   0
         Left            =   1800
         TabIndex        =   15
         Top             =   60
         Width           =   825
      End
      Begin VB.OptionButton optPrint 
         Caption         =   "HTML"
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   1
         Left            =   2700
         TabIndex        =   16
         Top             =   60
         Width           =   945
      End
   End
End
Attribute VB_Name = "MabsdF_Print"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Keyok As Boolean
Private Const SW_SHOW As Long = 1

Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
                        (ByVal hwnd As Long, ByVal lpOperation As String, _
                         ByVal lpFile As String, ByVal lpParameters As String, _
                         ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Private miPrintType As e_VisType
'Public SystemCount As Integer

Private Sub cmdAggiungi_Click()
  mTipoAdd = "ADD"
  MabsdF_PQuery.Show vbModal

  Carica_Lista_Stampe lswStampe
End Sub

Private Sub cmdCancella_Click()
  Dim i As Long
  
  If MsgBox("Do you want to erase checked queries?", vbQuestion + vbYesNo, Menu.BsNomeProdotto) = vbYes Then
    Screen.MousePointer = vbHourglass
    For i = 1 To lswStampe.ListItems.Count
      If lswStampe.ListItems(i).Checked Then
        Menu.BsConnection.Execute "DELETE * FROM BS_SysQuery Where Key = '" & lswStampe.ListItems(i).Key & "'"
      End If
    Next i
    'Carica la lista delle stampe disponibili
    Carica_Lista_Stampe lswStampe
    
    Screen.MousePointer = vbDefault
    
    txtSQL.Text = ""
    
    'SQ non c'� pi�
    'lswStampe_Click
    setCurrentQuery
  End If
End Sub

Private Sub cmdModifica_Click()
  mTipoAdd = "EDIT"
  MabsdF_PQuery.Show vbModal
   
  'Ricarica la lista delle stampe
  'Carica la lista delle stampe disponibili
  'Carica_Lista_Stampe lswStampe
  
  setCurrentQuery
End Sub

Private Sub cmdTemplate_Click()
  Dim dialogFileName As String
  Dim arrTemplate() As String
  Dim i As Integer, isTrovato As Boolean
  
  ReDim arrTemplate(3)
  arrTemplate(0) = "DAReport.xls"
  arrTemplate(1) = "FilesReport.xls"
  arrTemplate(2) = "InventoryReport.xls"
  arrTemplate(3) = "MissingsReport.xls"
  Cmdlg.InitDir = Menu.BsPathPrj & "\input-prj\Template\report\"
  Cmdlg.ShowOpen
  dialogFileName = Cmdlg.Filename
  dialogFileName = Mid(dialogFileName, InStrRev(dialogFileName, "\") + 1)
  txtTemplate.Text = dialogFileName
  isTrovato = False
  For i = 0 To UBound(arrTemplate)
    If UCase(arrTemplate(i)) = UCase(txtTemplate.Text) Then
      isTrovato = True
      Exit For
    End If
  Next i
  If isTrovato Then
    If MsgBox("Do you want to filter only queries for this Template?", vbQuestion + vbYesNo, Menu.BsNomeProdotto) = vbYes Then
      Carica_Lista_Stampe lswStampe, "Select * From bs_sysquery where " & _
                                     "report like '% REP" & Left(txtTemplate.Text, 1) & "%' Order By Report, Descrizione"
    End If
  End If
End Sub

Private Sub Form_Load()
  Dim Percorso As String
  Dim arrHeader() As String
  Dim i As Integer
  Dim r As Recordset
  
  On Error GoTo EH
  Keyok = True
  
  'SystemCount = 0
  miPrintType = 0
  optPrint(miPrintType).Value = True
  
  'Carica la lista delle stampe disponibili
  Carica_Lista_Stampe lswStampe

  m_SingleDoc = True

  'Seleziona il primo
  'If lswStampe.ListItems.Count Then
  '  lswStampe_Click
  'End If

  Set r = m_fun.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'BSHEADER'")
  If r.RecordCount Then
    arrHeader = Split(r!ParameterValue & "", "~")
         
    txtF1.Text = Trim(arrHeader(0))
    txtF2.Text = Trim(arrHeader(1))
    txtF3.Text = Trim(arrHeader(2))
  End If
  r.Close
  
  'Aggiunge la form alla collection
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
  
  Exit Sub
EH:
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
   
  If m_fun.FnProcessRunning = True Then
    wScelta = m_fun.Show_MsgBoxError("FB01I")
      
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  
  'Resize delle liste
'  lswStampe.ColumnHeaders(2).Width = 600
'  lswStampe.ColumnHeaders(1).Width = (lswStampe.Width - 380) - lswStampe.ColumnHeaders(2).Width
  lswStampe.ColumnHeaders(2).Width = 1600
  lswStampe.ColumnHeaders(1).Width = (lswStampe.Width - 380) - lswStampe.ColumnHeaders(2).Width
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Dim r As Recordset

  On Error GoTo EH
  
  Set r = m_fun.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'BSHEADER'")
  If r.RecordCount Then
    r!ParameterValue = Trim(txtF1.Text) & "~" & Trim(txtF2.Text) & "~" & Trim(txtF3.Text)
    r.Update
  End If
  r.Close
  
  'Rimuove la form dalla collection
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  
  SetFocusWnd m_fun.FnParent

  Exit Sub
EH:
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub lswStampe_ItemCheck(ByVal Item As MSComctlLib.ListItem)
  Item.Selected = True
  setCurrentQuery
End Sub

Private Sub lswStampe_ItemClick(ByVal Item As MSComctlLib.ListItem)
  'MsgBox "click"
  If Keyok Then
    If lswStampe.SelectedItem.Checked Then
      lswStampe.SelectedItem.Checked = False
    Else
      lswStampe.SelectedItem.Checked = True
    End If
  End If
  Keyok = True
  setCurrentQuery
End Sub

Private Sub setCurrentQuery()
  Dim Sql As String
  
  If lswStampe.ListItems.Count > 0 Then
    mCurKey = lswStampe.SelectedItem.Key
 
    mCurDescription = lswStampe.SelectedItem
    mCurReportName = lswStampe.SelectedItem.ListSubItems(1)
    'mCurReportName = lswStampe.SelectedItem.Tag
    'IsSystemQuery = lswStampe.SelectedItem.SubItems(1) = "X"
    Sql = Get_SQLQuery(mCurKey)
    txtSQL.Text = Sql
  End If
End Sub

Private Sub lswStampe_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswStampe.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswStampe.SortOrder = Order
  lswStampe.SortKey = Key - 1
  lswStampe.Sorted = True
End Sub

Private Sub lswStampe_DblClick()
  mTipoAdd = "EDIT"
  MabsdF_PQuery.Show vbModal
   
  'Ricarica la lista delle stampe
  'Carica la lista delle stampe disponibili
  'Carica_Lista_Stampe lswStampe
  
  setCurrentQuery
End Sub

Private Sub lswStampe_KeyDown(KeyCode As Integer, Shift As Integer)
  Keyok = False
End Sub

'''Private Sub lswStampe_ItemCheck(ByVal Item As MSComctlLib.ListItem)
'''  If Item.Checked Then
'''    If Item.SubItems(1) = "X" Then
'''      SystemCount = SystemCount - 1
'''    End If
'''  Else
'''    If Item.SubItems(1) = "X" Then
'''      SystemCount = SystemCount + 1
'''    End If
'''  End If
'''  If SystemCount = 0 Then
'''    cmdModifica.Enabled = True
'''  Else
'''    cmdModifica.Enabled = False
'''  End If
'''End Sub

Private Sub optPrint_Click(Index As Integer)
  On Error GoTo EH

  If Index = 0 Then
    miPrintType = VIS_EXCEL
  ElseIf Index = 1 Then
    miPrintType = VIS_HTML
  Else
    miPrintType = VIS_TXT
  End If
  optPrint(Index).Value = True
  Exit Sub
EH:
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim i As Integer, n As Integer, sRepToCompare As String, sNome As String
  Dim Filename As String
  Dim scelta As Long
 
  On Error GoTo EH
  
  If m_fun.FnProcessRunning Then Exit Sub
  
  Select Case Trim(UCase(Button.Key))
    Case "PRINT"
      'Attiva flag Processo
      m_fun.FnProcessRunning = True
       
      If txtReportName.Text = "" Then
        scelta = MsgBox("You didn't enter a Document name, Apply default Document file name?", vbQuestion + vbYesNo, "i-4.Migration")
        If scelta = vbYes Then
          If miPrintType = VIS_EXCEL Then
            'silvia: uso la cartella TXT di appoggio. Poi salvo il file nella Excel
            Filename = Menu.BsPathPrj & "\Documents\TXT\" & Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud.txt"
            ''Filename = Menu.BsPathPrj & "\Documents\Excel\" & Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud.xls"
          ElseIf miPrintType = VIS_HTML Then
            Filename = Menu.BsPathPrj & "\Documents\HTML\" & Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud.htm"
          ElseIf miPrintType = VIS_TXT Then
            Filename = Menu.BsPathPrj & "\Documents\TXT\" & Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud.txt"
          End If
        Else
          'Disattiva Flag Processo
          m_fun.FnProcessRunning = False
          Exit Sub
        End If
      Else
        If InStr(txtReportName.Text, ".") Then
          'Filename = Trim(Left(txtReportName.Text, InStr(txtReportName.Text, ".") - 1))
          If miPrintType = VIS_EXCEL Then
            MsgBox "Attention! The Report Type has been chosen: please eliminate the extension!" & vbCrLf & _
                   "The report will be created as EXCEL file!", vbOKOnly, Menu.BsNomeProdotto
          ElseIf miPrintType = VIS_HTML Then
            MsgBox "Attention! The Report Type has been chosen: please eliminate the extension!" & vbCrLf & _
                   "The report will be created as HTML file!", vbOKOnly, Menu.BsNomeProdotto
          ElseIf miPrintType = VIS_TXT Then
            MsgBox "Attention! The Report Type has been chosen: please eliminate the extension!" & vbCrLf & _
                   "The report will be created as TXT file!", vbOKOnly, Menu.BsNomeProdotto
          End If
          'Disattiva Flag Processo
          m_fun.FnProcessRunning = False
          Exit Sub
        End If
        If miPrintType = VIS_EXCEL Then
          'silvia: uso la cartella TXT di appoggio. Poi salvo il file nella Excel
          Filename = Menu.BsPathPrj & "\Documents\TXT\" & Trim(txtReportName.Text) & ".txt"
          ''Filename = Menu.BsPathPrj & "\Documents\Excel\" & Trim(txtReportName.Text) & ".xls"
        ElseIf miPrintType = VIS_HTML Then
          Filename = Menu.BsPathPrj & "\Documents\HTML\" & Trim(txtReportName.Text) & ".htm"
        ElseIf miPrintType = VIS_TXT Then
          Filename = Menu.BsPathPrj & "\Documents\TXT\" & Trim(txtReportName.Text) & ".txt"
        End If
      End If
       
      'gloria: se nn hanno inserito il template apro excel generico
      If Trim(txtTemplate.Text) = "" Then
        If MsgBox("You didn't enter a Document Template, do you want create a generic Report?", vbQuestion + vbYesNo, "i-4.Migration") = vbYes Then
          Visualizza_Stampa miPrintType, Filename
        Else
          m_fun.FnProcessRunning = False
          Exit Sub
        End If
      Else
        'verifico che non abbia selezionato query con lo stesso Report
        For i = 1 To lswStampe.ListItems.Count
          If lswStampe.ListItems(i).Checked Then
            sRepToCompare = lswStampe.ListItems(i).Tag
            sNome = lswStampe.ListItems(i).Text
            For n = 1 To lswStampe.ListItems.Count
              If lswStampe.ListItems(n).Checked Then
                If lswStampe.ListItems(n).Text <> sNome And lswStampe.ListItems(n).Tag = sRepToCompare Then
                  MsgBox "Report '" & sRepToCompare & "' is already associated to '" & sNome & _
                         "'. It's not possible to generate the report.", vbCritical, "i-4.Migration - Export Report"
                  m_fun.FnProcessRunning = False
                  Exit Sub
                End If
              End If
            Next n
          End If
        Next i
        Visualizza_Stampa miPrintType, Filename, Menu.BsPathPrj & "\INPUT-PRJ\Template\Report\" & Trim(txtTemplate.Text)
      End If
      
      'Disattiva Flag Processo
      m_fun.FnProcessRunning = False
    Case "REFRESH"
      'Attiva flag Processo
      m_fun.FnProcessRunning = True
      
      scelta = MsgBox("All customized queries will be deleted and default queries will be restored." & vbCrLf & "Are you sure?", vbQuestion + vbYesNo, "Query restoring")
      If (scelta = vbYes) Then
        'Carica la lista delle stampe disponibili
        Carica_Lista_Stampe lswStampe
      End If
      
      'Disattiva Flag Processo
      m_fun.FnProcessRunning = False
      
    'silvia
    Case "ISTRDLI"
      m_fun.FnProcessRunning = True
       
      If Trim(txtReportName.Text) = "" Then
        scelta = MsgBox("You didn't enter a Document name, Apply default Document file name?", vbQuestion + vbYesNo, "i-4.Migration")
        If scelta = vbYes Then
          Filename = Menu.BsPathPrj & "\Documents\TXT\" & Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud.txt"
        Else
          'Disattiva Flag Processo
          m_fun.FnProcessRunning = False
          Exit Sub
        End If
      Else
        If InStr(txtReportName.Text, ".") Then
          MsgBox "Attention! The Report Type has been chosen: please eliminate the extension!" & vbCrLf & _
                 "The report will be created as TXT file!", vbOKOnly, Menu.BsNomeProdotto
          'Disattiva Flag Processo
          m_fun.FnProcessRunning = False
          Exit Sub
        End If
        Filename = Menu.BsPathPrj & "\Documents\TXT\" & Trim(txtReportName.Text) & ".txt"
      End If
       
      Carica_Istr_DLI Filename
      If miPrintType = VIS_EXCEL Then
        Filename = Replace(Filename, "\TXT\", "\Excel\", , , vbTextCompare)
        Filename = Replace(Filename, ".txt", ".xls")
      End If
      ShellExecute Me.hwnd, "open", Filename, "", "", SW_SHOW
      m_fun.FnProcessRunning = False
  End Select
  Exit Sub
EH:
  If ERR.Number = 76 Then
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Excel"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\HTML"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Log"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Txt"
    Resume
  Else
    MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear
    Screen.MousePointer = vbDefault
    'Disattiva Flag Processo
    m_fun.FnProcessRunning = False
  End If
End Sub

Public Sub Visualizza_Stampa(wTipoVisualizzazione As e_VisType, Filename As String, Optional ByVal mTemplate As String)
  Dim i As Long
  Dim sHTML As String
  Dim hBrowse As Long
  Dim Sql As String
  Dim FieldCount As Integer
  Dim RowCount As Long
  Dim IntIndex As Integer
  
  IntIndex = 0
  On Error GoTo ErrorHandler
  'Avvia la creazione del file
  BsText1 = txtF1
  BsText2 = txtF2
  BsText3 = txtF3
  
  Screen.MousePointer = vbHourglass
  If miPrintType = VIS_EXCEL Then
    'gloria: se non ho il template apro excel generico
    If mTemplate = "" Then
      Stampe_in_Excel Filename
    Else
      ' controllo che il file ci sia realmente
      If FileExists(mTemplate) Then
        Stampe_Excel_daTemplate Filename, mTemplate
      Else
        MsgBox "Template '" & mTemplate & "' doesn't exist!" & vbCrLf & _
               "Report will generate as generic Excel file.", vbInformation + vbOKOnly, Menu.BsNomeProdotto
        Stampe_in_Excel Filename
      End If
    End If
    ShellExecute Me.hwnd, "open", Filename, "", "", SW_SHOW
  ElseIf miPrintType = VIS_HTML Then
    sHTML = vbNullString
    sHTML = sHTML & "<html><head><title>HTML Export</title>" & vbCrLf & vbCrLf
    sHTML = sHTML & "</head><body><script Language=Javascript>" & vbCrLf & vbCrLf
    sHTML = sHTML & "//<!--" & vbCrLf & vbCrLf
    sHTML = sHTML & "alert('HTML Export Success...'); " & vbCrLf & vbCrLf
    sHTML = sHTML & "//-->" & vbCrLf & vbCrLf
    sHTML = sHTML & "</script>" & vbCrLf & vbCrLf
   
    For i = 1 To lswStampe.ListItems.Count
      If lswStampe.ListItems(i).Checked Then
        sHTML = sHTML & Crea_Foglio_HTML(lswStampe.ListItems(i).Key, lswStampe.ListItems(i).Text)
      End If
      DoEvents
    Next i
          
    sHTML = sHTML & "</body></html>"
         
    Open Filename For Output As #1
    Print #1, sHTML
    Close #1
          
    On Error Resume Next
      
    hBrowse = ShellExecute(Me.hwnd, "open", Filename, "", "", SW_SHOW)
  ElseIf miPrintType = VIS_TXT Then
    'Crea un nuovo documento di testo
    numFileTXT = FreeFile
    Open Filename For Output As numFileTXT

    For i = 1 To lswStampe.ListItems.Count
      If lswStampe.ListItems(i).Checked Then
        Sql = Get_SQLQuery(lswStampe.ListItems(i).Key, False)
        If Sql <> "" Then
          Crea_Foglio_Txt lswStampe.ListItems(i).Key, Sql, FieldCount, RowCount
        Else
          MsgBox "No Records Found..."
        End If
      End If
    Next i
    Close numFileTXT
    ShellExecute Me.hwnd, "open", Filename, "", "", SW_SHOW
  End If
  Screen.MousePointer = vbDefault
 
Exit Sub
ErrorHandler:
  If ERR.Number = 76 Then
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Excel"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\HTML"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Log"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Txt"
    Resume
  Else
    MsgBox ERR.Number & " - " & ERR.Description
    Screen.MousePointer = vbDefault
  End If
End Sub

Private Function Crea_Foglio_HTML(Key As String, sText As String) As String
  Dim rs As Recordset
  Dim r_Start As Long
  Static cc As Long
  Dim i As Long, r As Long, j As Long
  Dim eRange As Range
  Dim Sql As String
  Dim sPageKey As String, sPageName As String
  Dim sHTMLOutput As String
  Dim sLeftImage As String, sRightImage As String
  Dim iPageCounter As Integer
  
  Crea_Foglio_HTML = vbNullString
  Sql = Get_SQLQuery(Key)
  
  If Sql <> "" Then
    'Parte dalla quarta riga
    r_Start = 6
    r = r_Start + 1
    sPageKey = Key
    sPageName = Menu.BsPathDef & "\Documents\HTML\" & Key & ".htm"
    
'    sLeftImage = Replace(Carica_Logo_Da_DB("L"), " ", "%20")
'    sRightImage = Replace(Carica_Logo_Da_DB("R"), " ", "%20")
          
    sHTMLOutput = vbNullString
    Sql = Get_SQLQuery(Key, False)
    
    Set rs = m_fun.Open_Recordset(Sql)
    If rs.RecordCount Then
      iPageCounter = 1
      rs.MoveFirst
     
      Screen.MousePointer = vbHourglass
  
      sHTMLOutput = sHTMLOutput & "<b>" & sText & "</b><br><br>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "<center><table><tr><td align=center><img src=" & sLeftImage & _
          "></td><td align=center>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "<b>" & Trim(BsText1) & "<br>" & Trim(BsText2) & "<br>" & Trim(BsText3) & _
          "<br>" & Trim(BsText4) & "<br>"
          
      If InStr(1, Trim(BsText5), "http://") > 0 Then
        sHTMLOutput = sHTMLOutput & "<a href=" & Trim(BsText5) & " target=""_blank"">" & Trim(BsText5) & "</a>"
      ElseIf InStr(1, Trim(BsText5), "www") > 0 Then
        sHTMLOutput = sHTMLOutput & "<a href=http://" & Trim(BsText5) & " target=""_blank"">" & Trim(BsText5) & "</a>"
      Else
        sHTMLOutput = sHTMLOutput & Trim(BsText5)
      End If
      
      sHTMLOutput = sHTMLOutput & "</b>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "</td><td align=center><img src=" & sRightImage & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "></td></tr></table>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "<br>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "<table border=1 bordercolor=black bordercolorlight=black bordercolordark=black><tr>" & vbCrLf & vbCrLf
  
      For i = 0 To rs.Fields.Count - 1
        DoEvents
        sHTMLOutput = sHTMLOutput & "<td bgcolor=yellow>" & vbCrLf & vbCrLf
        sHTMLOutput = sHTMLOutput & "<font face=arial color=black size=3><b>" & Trim(rs.Fields(i).Name) & _
                                    "</b></font>" & vbCrLf & vbCrLf
        sHTMLOutput = sHTMLOutput & "</td>" & vbCrLf & vbCrLf
      Next i
      sHTMLOutput = sHTMLOutput & "</tr>" & vbCrLf & vbCrLf
      
      'Screen.MousePointer = vbDefault
      Do While Not rs.EOF
        DoEvents
        Screen.MousePointer = vbHourglass

        sHTMLOutput = sHTMLOutput & "<tr>" & vbCrLf & vbCrLf
        For i = 0 To rs.Fields.Count - 1
          sHTMLOutput = sHTMLOutput & "<td>" & vbCrLf & vbCrLf
          sHTMLOutput = sHTMLOutput & Trim(rs.Fields(i).Value)
          sHTMLOutput = sHTMLOutput & "</td>" & vbCrLf & vbCrLf
        Next i
        rs.MoveNext
        r = r + 1

        'Screen.MousePointer = vbDefault
        sHTMLOutput = sHTMLOutput & "</tr>" & vbCrLf & vbCrLf
      Loop
      rs.Close

      sHTMLOutput = sHTMLOutput & "</table>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "<br><hr>" & vbCrLf & vbCrLf
      sHTMLOutput = sHTMLOutput & "</center>" & vbCrLf & vbCrLf
    End If
    
    Screen.MousePointer = vbHourglass
    'Screen.MousePointer = vbDefault
    
    Crea_Foglio_HTML = sHTMLOutput
  Else
    MsgBox "No Records Found..."
  End If
End Function

Public Sub Stampe_in_Excel(Filename As String)
  Dim ObjExc As Object
  Dim wBook As Workbook
  Dim wBooks As Workbooks
  Dim wsheet As Worksheet
  Dim Sql As String
  Dim FieldCount As Integer
  Dim RowCount As Long
  Dim IntIndex As Integer, int1 As Integer
  Dim wbookname As String, txtFileName As String, xlsfilename As String
  Dim filexls() As String
  Dim c As Integer, i As Integer
  
  IntIndex = 0
  int1 = 0
  On Error GoTo ErrorHandler

  'Crea un nuovo foglio excel
  Set ObjExc = CreateObject("Excel.Application")
  'ObjExc.Visible = True
  
  Set wBook = Nothing
  Set wBooks = Nothing
  Set wsheet = Nothing

  Set wBooks = ObjExc.Workbooks
  'SILVIA
  'wbook.Worksheets.Add 'Aggiunge il primo foglio vuoto che � riepilogativo
  'wbook.Worksheets(1).Name = "Final"
  ' Set wbook = ObjExc.Workbooks.Add
  txtFileName = Filename
  Filename = Replace(Filename, "\TXT\", "\Excel\", , , vbTextCompare)
  Filename = Replace(Filename, ".txt", ".xls")
   
  ''silvia
    
  'Crea un nuovo documento di testo
  For i = 1 To lswStampe.ListItems.Count
    If lswStampe.ListItems(i).Checked Then
      Sql = Get_SQLQuery(lswStampe.ListItems(i).Key, False)
      If Sql <> "" Then
        numFileTXT = FreeFile
        Open (Replace(txtFileName, ".txt", "_" & i & ".txt")) For Output As numFileTXT
        Crea_Foglio_Txt lswStampe.ListItems(i).Key, Sql, FieldCount, RowCount
        Close numFileTXT
        
        IntIndex = IntIndex + 1
        Copia_su_Excel IIf(lswStampe.ListItems(i).Tag <> "", lswStampe.ListItems(i).Tag, lswStampe.ListItems(i).Key), Replace(txtFileName, ".txt", "_" & i & ".txt"), wBooks, FieldCount, IntIndex, RowCount + 4
        ReDim Preserve filexls(IntIndex)
        filexls(IntIndex - 1) = Replace(Filename, ".xls", "_" & i & ".xls")
      Else
        MsgBox "No Records Found for Report "
      End If
    End If
  Next i
  
  If IntIndex > 0 Then
    If IntIndex = 1 Then
      m_SingleDoc = True
    Else
      m_SingleDoc = False
    End If
    If Not m_SingleDoc Then
      IntIndex = 0
      For i = 1 To lswStampe.ListItems.Count
        If lswStampe.ListItems(i).Checked Then
          IntIndex = IntIndex + 1
          wBooks.Application.Windows(wBooks.Item(IntIndex).Name).Activate
          wBooks.Application.Cells.Select
          wBooks.Application.Cells.Copy
          If wBook Is Nothing Then
            Set wBook = wBooks.Add
            Set wsheet = wBook.Worksheets.Item(1)
            wbookname = wBook.Name
          Else
            wBook.Worksheets.Add
          End If
          wBook.Worksheets.Item(1).Paste
          wBook.Worksheets.Item(1).Name = IIf(lswStampe.ListItems(i).Tag <> "", lswStampe.ListItems(i).Tag, lswStampe.ListItems(i).Key)
          'FILTRO
          wBook.Worksheets.Item(1).Activate
          wBook.Worksheets.Item(1).Rows("4:4").Select
          wBook.Worksheets.Item(1).Application.Rows(4).AutoFilter
          'IntIndex = IntIndex - 1
        End If
      Next i
      '
      If FileExists(Filename) Then
        If MsgBox("File already exist. Do you want overwrite it?", vbYesNo, Me.Caption) = vbYes Then
          Kill Filename
        Else
          Filename = Replace(Filename, ".xls", Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud.xls")
          txtReportName.Text = Trim(txtReportName.Text) & Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud"
        End If
      End If
      wBook.SaveAs Filename
      wbookname = wBook.Name
      i = 1
      Do While wBooks.Count > 1
        If wBooks.Item(i).Name <> wbookname Then
          wBooks.Item(i).Close
        Else
          i = i + 1
        End If
      Loop
      'SILVIA FOGLIO TOC
'      wbook.Worksheets.Add
'      Set wsheet = wbook.Worksheets.Item(1)
'      wsheet.Name = "TOC"
'      wbook.Application.ActiveWindow.DisplayGridlines = False
'      wbook.Save
'''''''------
      For i = 0 To UBound(filexls) - 1
        Kill filexls(i)
      Next i
      ReDim filexls(0)
      wBook.Close
    'Else
''''  'SILVIA FOGLIO TOC
'      Set wbook = wbooks.Item(1)
'      wbook.Worksheets.Add
'      Set wsheet = wbook.Worksheets.Item(1)
'      wsheet.Name = "TOC"
'      wbook.Application.ActiveWindow.DisplayGridlines = False
'      wbook.Save
''''' ------
    End If
  Else
    m_SingleDoc = False
  End If
    
  ObjExc.Quit
  Set ObjExc = Nothing
  Set wBook = Nothing
  Set o_Ex = Nothing
    
  If m_SingleDoc Then
    If FileExists(Filename) Then
      If MsgBox("File already exist. Do you want overwrite it?", vbYesNo, Me.Caption) = vbYes Then
        Kill Filename
      Else
        Filename = Replace(Filename, ".xls", Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud.xls")
        txtReportName.Text = Trim(txtReportName.Text) & Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud"
      End If
    End If
    Name filexls(0) As Filename
  End If
  Exit Sub
ErrorHandler:
  If ERR.Number = 76 Then
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Excel"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\HTML"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Log"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Txt"
    Resume
'  ElseIf ERR.Number = 75 Then
'    Resume
  Else
    MsgBox ERR.Number & " - " & ERR.Description
    Screen.MousePointer = vbDefault
  End If
End Sub

Public Sub Stampe_Excel_daTemplate(Filename As String, Template As String)
  Dim ObjExc As Excel.Application, wBooks As Workbooks
  Dim Sql As String
  Dim FieldCount As Integer
  Dim RowCount As Long, IntIndex As Integer
  Dim txtFileName As String, filexls() As String
  Dim c As Integer, i As Integer
  Dim mySheet As Worksheet
  Dim isTrovato As Boolean
  
  IntIndex = 0
  On Error GoTo ErrorHandler
   
  'Crea un nuovo foglio excel
  Set ObjExc = New Excel.Application
'  Set ObjExc = CreateObject("Excel.Application")
 
  Set wBooks = Nothing
     
  txtFileName = Filename
  Filename = Replace(Filename, "\TXT\", "\Excel\", , , vbTextCompare)
  Filename = Replace(Filename, ".txt", ".xls")
        
  'devo aprire il template
  ObjExc.Workbooks.Open Template
  Set wBooks = ObjExc.Workbooks
      
  If FileExists(Filename) Then
    If MsgBox("File already exist. Do you want overwrite it?", vbYesNo, Me.Caption) = vbYes Then
      Kill Filename
    Else
      Filename = Replace(Filename, ".xls", Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud.xls")
      txtReportName.Text = Trim(txtReportName.Text) & Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Aud"
    End If
  End If
      
  wBooks.Item(1).SaveAs Filename
  wBooks.Item(1).Close

  Set wBooks = Nothing
  ObjExc.Quit
  Set ObjExc = New Excel.Application
    
  'devo aprire il report
  ObjExc.Workbooks.Open Filename
  Set wBooks = ObjExc.Workbooks
    
  'Crea un nuovo documento di testo
  For i = 1 To lswStampe.ListItems.Count
    If lswStampe.ListItems(i).Checked Then
    ' lswStampe.ListItems(i).subitems(1)
      Sql = Get_SQLQuery(lswStampe.ListItems(i).Key, False)
      If Sql <> "" Then
        numFileTXT = FreeFile
        Open (Replace(txtFileName, ".txt", "_" & i & ".txt")) For Output As numFileTXT
        Crea_Foglio_Txt_Template lswStampe.ListItems(i).Key, Sql, FieldCount, RowCount
        Close numFileTXT
        IntIndex = IntIndex + 1
        If lswStampe.ListItems(i).Tag = "" Then
          MsgBox "Not assigned report for " & lswStampe.ListItems(i).Key, vbCritical, "i-4.Migration - Export Report"
        Else
          Copia_su_Template lswStampe.ListItems(i).Tag, Replace(txtFileName, ".txt", "_" & i & ".txt"), wBooks, FieldCount, IntIndex, RowCount
          ReDim Preserve filexls(IntIndex)
          filexls(IntIndex - 1) = Replace(Filename, ".xls", "_" & i & ".xls")
        End If
      Else
        MsgBox "No Records Found for Report "
      End If
    End If
  Next i
       
  Set mySheet = wBooks.Item(1).Sheets("TOC")
  mySheet.Activate
  isTrovato = False
  For i = 1 To 10
    For c = 1 To 50
      If mySheet.Cells(c, i).Value = "<SAVEDATE>" Then
        mySheet.Cells(c, i) = Date
        isTrovato = True
        Exit For
      End If
    Next c
    If isTrovato Then
      Exit For
    End If
  Next i
       
  wBooks.Item(1).Save
  wBooks.Item(1).Close
       
'  ActiveWorkbook.SaveAs Filename
'  ActiveWorkbook.Close

  Set wBooks = Nothing
  ObjExc.Quit
  Set ObjExc = Nothing
  
  Exit Sub
ErrorHandler:
  If ERR.Number = 76 Then
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Excel"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\HTML"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Log"
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Txt"
    Resume
'  ElseIf ERR.Number = 75 Then
'    Resume
  Else
    ActiveWorkbook.Close
    ObjExc.Quit
    Set ObjExc = Nothing
    MsgBox ERR.Number & " - " & ERR.Description
    Screen.MousePointer = vbDefault
  End If
End Sub

Private Sub txtTemplate_Change()
  If txtTemplate.Text = "" Then
    Carica_Lista_Stampe lswStampe
  End If
End Sub
