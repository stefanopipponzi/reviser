Attribute VB_Name = "MabsdM_Print"
Option Explicit

Global IdxCSel As Long
Global IdxSelect As Long
Global IdxWhere As Long
Global IdxOrder As Long
'SELECT,WHERE,ORDERBY
Global TipoSel As String
Global ActiveList As String
Global m_SingleDoc As Boolean
'serve per far vedere alla form preview la stringa sql creata nella form print
Global ExportSql As String
'propriet� del foglio
Global Const PFontName As String = "Courier New"
Global Const PFontSize As Long = 11
'Variabili per contenere le opzioni di stampa
Global CmpOrder() As String
Global CmpSelect() As String
Global Distinct As Boolean
Global PercorsoLogoL As String
Global PercorsoLogoR As String
Global FinalSect As String
Global IdxStampa As Long
'Parti variabili della Stampa
Global BsLogoL As String
Global BsLogoR As String
Global BsText1 As String
Global BsText2 As String
Global BsText3 As String
Global BsText4 As String
Global BsText5 As String
Global gbRoutineExec As String
Global o_Ex As Worksheet
Global mCurKey As String, mCurDescription As String, mCurReportName As String
Global mTipoAdd As String 'ADD o EDIT

Private Const SW_SHOW = 1

Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
                        (ByVal hwnd As Long, ByVal lpOperation As String, _
                        ByVal lpFile As String, ByVal lpParameters As String, _
                        ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
                        
'Tipo di visualizzazione delle Query
Public Enum e_VisType
  VIS_EXCEL = 2 ^ 0
  VIS_HTML = 2 ^ 1
  'VIS_HTML_BY_EXCEL = 2 ^ 2
  VIS_TXT = 2 ^ 2
End Enum
Public numFileTXT As Integer

Public Sub Carica_Lista_Stampe(Lsw As ListView, Optional Sql As String)
  Dim rs As Recordset
     
  On Error GoTo ErrH
   
  Lsw.ListItems.Clear
  If Lsw.Sorted Then
    Lsw.Sorted = False
  End If
   
  If Sql = "" Then
    Set rs = m_fun.Open_Recordset("SELECT * FROM bs_SysQuery Order By Report, Descrizione")
  Else
    Set rs = m_fun.Open_Recordset(Sql)
    If rs.RecordCount = 0 Then
      rs.Close
      MsgBox "No query found for this Template!", vbExclamation + vbOKOnly, Menu.BsNomeProdotto
      Set rs = m_fun.Open_Recordset("SELECT * FROM bs_SysQuery Order By Report, Descrizione")
    End If
  End If
  While Not rs.EOF
    Lsw.ListItems.Add , Trim(rs!Key), Trim(rs!Descrizione)
    'gloria: salvo il nome report nel tag
    Lsw.ListItems(Lsw.ListItems.Count).Tag = IIf(IsNull(rs!Report), "", rs!Report)
    Lsw.ListItems(Lsw.ListItems.Count).SubItems(1) = IIf(IsNull(rs!Report), "", rs!Report)
    rs.MoveNext
  Wend
  rs.Close
  
  Exit Sub
ErrH:
  m_fun.WriteLog "||Carica_Lista_Stampe: " & ERR.Description
End Sub

Public Function Get_SQLQuery(Key As String, Optional ByVal OnlySel As Boolean = True) As String
  Dim rs As Recordset, rs1 As Recordset
  Dim wSQL As String
  Dim wTitle As String
  Dim cc As Long
  Dim wIdx As Long
  Dim wIdxStop As Long
  Dim wParamRet As String
  Dim oForm As Object
  Dim wPrintName As String
  Dim wKeyWord As String
  Dim k1 As Long, k2 As Long
   
  Set rs = m_fun.Open_Recordset("SELECT * FROM bs_SysQuery Where Key = '" & UCase(Key) & "'")
  If rs.RecordCount Then
    k1 = InStr(Trim(rs!Testo), "<INTERNAL-ROUTINE>::")
    k2 = InStr(Trim(rs!Testo), ">::") + 3 'SQ: bello!
    
    If k1 Then
      wSQL = "<INTERNAL-ROUTINE>"
      gbRoutineExec = Mid(Trim(rs!Testo), k2)
    Else
      wSQL = Trim(rs!Testo)
      gbRoutineExec = ""
    End If
    
    wPrintName = Trim(rs!Descrizione)
  End If
  rs.Close
   
  If Not OnlySel Then
   'Chiede all'utente i parametri
   cc = 1
   wIdx = InStr(1, wSQL, "%VAR" & cc & "-")
   wIdxStop = InStr(wIdx + 5, wSQL, "%")
   
   If wIdx > 0 And wIdxStop > 0 Then
     While wIdx > 0 And wIdxStop > 0
       wTitle = Mid(wSQL, wIdx + 6, wIdxStop - wIdx - 6)
       wKeyWord = Mid(wSQL, wIdx, wIdxStop - wIdx + 1)
        
       'Costruzione e lancio form
       Set oForm = New MabsdF_QueryParam
       oForm.SettingLabels wPrintName, wTitle, wSQL
       oForm.Show vbModal
         
       wParamRet = oForm.QueryParam
       'Replace con il parametro
       wSQL = Replace(wSQL, wKeyWord, wParamRet)
       'Cerca il Parametro successivo
       cc = cc + 1
       wIdx = InStr(1, wSQL, "%VAR" & cc & "-")
         
       If wIdx Then
         wIdxStop = InStr(wIdx + 5, wSQL, "%")
       Else
         wIdxStop = 0
       End If
     Wend
   End If
  End If
  Get_SQLQuery = wSQL
End Function

Public Sub Imposta_Foglio_Excel(Key As String, wBook As Workbook)
  Dim Sql As String
   
  Sql = Get_SQLQuery(Key, False)
  If Sql <> "" Then
    Crea_Foglio_Excel Key, Sql, wBook
  Else
    MsgBox "No Records Found..."
  End If
End Sub

Public Sub Crea_Foglio_Excel(Key As String, Sql As String, wBook As Workbook)
  Dim rs As Recordset
  Dim r_Start As Long
  Static cc As Long
  Dim i As Long, r As Long
  Dim eRange As Range
  Dim NomeFoglio As String, NomeFile As String
  
  On Error GoTo ErrorHandler
  'Parte dalla quarta riga
  r_Start = 6
  r = r_Start + 1
  
  NomeFoglio = Key
  NomeFile = Menu.BsPathPrj & "\Documents\Excel\" & Key & ".xls"
  
  'Setta il foglio
  wBook.Worksheets.Add
  Set o_Ex = wBook.Worksheets.Item(1)
  ' Mauro 05/11/2007: Controllo chiave(QueryName)
  ' Caratteri riservati
  If controllaQuery(NomeFoglio) Then
    ' Lunghezza max: 31 char
    If Len(NomeFoglio) > 31 Then
      MsgBox "Query Name too long! (Max char: 31)", vbOKOnly + vbExclamation
    Else
      o_Ex.Name = NomeFoglio
     
      Set rs = m_fun.Open_Recordset(Sql)
      If rs.RecordCount Then
        Screen.MousePointer = vbHourglass
        
        'Incolla l'intestazione a destra
        o_Ex.Cells(1, 6) = Trim(BsText1)
        o_Ex.Cells(1, 6).Font.Bold = True
        o_Ex.Cells(2, 6) = Trim(BsText2)
        o_Ex.Cells(2, 6).Font.Bold = True
        o_Ex.Cells(3, 6) = Trim(BsText3)
        o_Ex.Cells(3, 6).Font.Bold = True
        o_Ex.Cells(4, 6) = Trim(BsText4)
        o_Ex.Cells(4, 6).Font.Bold = True
        o_Ex.Cells(5, 6) = Trim(BsText5)
        o_Ex.Cells(5, 6).Font.Bold = True
        
        Set eRange = o_Ex.Cells(1, 6)
        eRange.HorizontalAlignment = xlLeft
        eRange.EntireColumn.AutoFit
        
        'Incolla l'intestazione dei campi
        For i = 0 To rs.Fields.Count - 1
          Set eRange = o_Ex.Cells(r_Start, i + 1)
           
          o_Ex.Cells(r_Start, i + 1) = Trim(rs.Fields(i).Name)
          o_Ex.Cells(r_Start, i + 1).Interior.ColorIndex = 4
          o_Ex.Cells(r_Start, i + 1).Borders.ColorIndex = 1
          o_Ex.Cells(r_Start, i + 1).Font.Bold = True
          
          eRange.HorizontalAlignment = xlLeft
          eRange.EntireColumn.AutoFit
        Next i
        
        Screen.MousePointer = vbDefault
        
        While Not rs.EOF
          Screen.MousePointer = vbHourglass
          
          'Incolla i dati nel foglio excel
          For i = 0 To rs.Fields.Count - 1
            o_Ex.Cells(r, i + 1) = Trim(rs.Fields(i).Value)
            o_Ex.Cells(r, i + 1).Borders.ColorIndex = 1
            
            Set eRange = o_Ex.Cells(r, i + 1)
            
            eRange.HorizontalAlignment = xlLeft
            eRange.EntireColumn.AutoFit
          Next i
          rs.MoveNext
          r = r + 1
          Screen.MousePointer = vbDefault
  
        Wend
      End If
      rs.Close
     
      cc = cc + 1
      'Salva il file se siamo in modalit� multidocumento
      Screen.MousePointer = vbDefault
    End If
  End If
Exit Sub
ErrorHandler:
  MsgBox ERR.Number & " - " & ERR.Description
End Sub

Public Sub Copia_su_Excel(Key As String, strFileName As String, wBooks, FieldCount As Integer, IntIndex As Integer, RowCount As Long)
  Dim NomeFoglio As String, NomeFile As String
  Dim wBook As Workbook
  Dim wsheet As Worksheet
  Dim eRange As Range
  Dim i As Integer
  Dim j As String
  
  On Error GoTo ErrorHandler
  
  'Dim wbooks As Workbooks
  Set wsheet = Nothing
  Set wBook = Nothing
  'Set wbooks = Nothing
  'Parte dalla quarta riga
  NomeFoglio = Key
  'NomeFile = Menu.BsPathPrj & "\Documents\Excel\" & Key & ".xls"
  NomeFile = Replace(strFileName, "\TXT\", "\Excel\", , , vbTextCompare)
  NomeFile = Replace(NomeFile, ".txt", ".xls")
  'silvia 15-04-2008
  If FileExists(NomeFile) Then
    Kill NomeFile
  End If
  '
''  wbooks.OpenText Filename:=strfilename, _
''    Origin:=xlMSDOS, StartRow:=1, DataType:=xlDelimited, TextQualifier:= _
''    xlNone, ConsecutiveDelimiter:=False, Tab:=False, Semicolon:=False, Comma _
''    :=False, Space:=False, Other:=True, OtherChar:="�", FieldInfo:=Array( _
''    Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1), Array(5, 1), Array(6, 1), Array(7, 1), _
''    Array(8, 1), Array(9, 1), Array(10, 1), Array(11, 1)), TrailingMinusNumbers:=True
  
  wBooks.OpenText Filename:=strFileName, _
         Origin:=xlWindows, StartRow:=1, DataType:=xlDelimited, TextQualifier:=xlDoubleQuote, _
         ConsecutiveDelimiter:=False, Tab:=False, Semicolon:=False, _
         Comma:=False, Space:=False, Other:=True, OtherChar:="�", _
         FieldInfo:=Array(Array(1, 1), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True
  
  wBooks.Item(IntIndex).SaveAs Filename:=NomeFile, _
      FileFormat:=xlNormal, Password:="", WriteResPassword:="", _
      ReadOnlyRecommended:=False, CreateBackup:=False
  Set wBook = wBooks.Item(IntIndex)
  wBook.Sheets.Select
  wBook.Sheets(1).Name = NomeFoglio
  Set wsheet = wBook.Worksheets.Item(1)
  
  For i = 0 To FieldCount - 1
    'Range.HorizontalAlignment = xlLeft
    Set eRange = wsheet.Cells(4, i + 1)
    'wsheet.Cells(4, i + 1).Interior.ColorIndex = 4
    'wsheet.Cells(4, i + 1).Borders.ColorIndex = 1
    wsheet.Cells(4, i + 1).Font.Bold = True
    eRange.EntireColumn.AutoFit
  Next i
  
 'silvia formattazione foglio
  wsheet.Application.Rows.Interior.ColorIndex = 2
  wsheet.Cells(1, 1).Select
  
  '''''''''''''''''''''' HEADER FIELDS '''''''''''''''''''''''''''''''''''''
  Set eRange = wsheet.Cells(1, 1)
  eRange.Select
  eRange.FormulaR1C1 = MabsdF_Print.txtF1.Text
  
  Set eRange = wsheet.Cells(2, 1)
  eRange.Select
  eRange.FormulaR1C1 = MabsdF_Print.txtF2.Text
  
  Set eRange = wsheet.Cells(3, 1)
  eRange.Select
  eRange.FormulaR1C1 = MabsdF_Print.txtF3.Text
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
  wsheet.Rows("1:1").Select
  wsheet.Application.Rows(1).RowHeight = 40
  wsheet.Application.Rows(1).Font.Name = "Arial"
  wsheet.Application.Rows(1).Font.Size = 18
  wsheet.Application.Rows(1).Font.Bold = True
  
  wsheet.Rows("2:2").Select
  wsheet.Application.Rows(2).RowHeight = 30
  wsheet.Application.Rows(2).Font.Name = "Arial"
  wsheet.Application.Rows(2).Font.Size = 18
  wsheet.Application.Rows(2).Font.Bold = True

  If RowCount > 4 Then
    wsheet.Rows("4:4").Select
    wsheet.Application.Rows(4).AutoFilter
  
    wsheet.Rows(5).Select
    wsheet.Application.Rows(5).Interior.ColorIndex = 35
    wsheet.Rows(6).Select
    wsheet.Application.Rows(6).Interior.ColorIndex = 2
  End If
  If RowCount > 6 Then
    wsheet.Rows("5:6").Select
    wsheet.Application.Rows("5:6").Copy
    j = "7:" & RowCount & ""
    wsheet.Rows(j).Select
    wsheet.Application.Rows(j).PasteSpecial Paste:=xlPasteFormats, Operation:=xlNone, _
          SkipBlanks:=False, Transpose:=False
  End If
  wBook.Save
  
  Kill strFileName
  
  Exit Sub
ErrorHandler:
  Kill strFileName
  Kill NomeFile
  MsgBox ERR.Number & " - " & ERR.Description
  Screen.MousePointer = vbDefault
End Sub

Public Sub Copia_su_Template(Key As String, strFileName As String, wBooks As Workbooks, FieldCount As Integer, IntIndex As Integer, RowCount As Long)
  Dim NomeFoglio As String, NomeFile As String
  Dim i As Long
  Dim myReport As Worksheet
  Dim isTrovato As Boolean, isOk As Boolean
  Dim numFile As Integer
  Dim myRange As String
  Dim rowLeft As Double, StartRow As Long
  Dim fso As Object
  Dim InFile As Integer, OutFile As Integer
  Dim strFileNameO As String, line As String
  Dim j As Long

  On Error GoTo ErrorHandler

  'Parte dalla quarta riga
  NomeFoglio = Key
  
  NomeFile = Replace(strFileName, "\TXT\", "\Excel\", , , vbTextCompare)
  NomeFile = Replace(NomeFile, ".txt", ".xls")
  
  numFile = FreeFile
  Open strFileName For Input As #numFile
  
  isOk = False
  
  If Not EOF(numFile) Then isOk = True
  Close #numFile
  
  If isOk Then  'se il file txt � vuoto non importo nulla
    Set myReport = wBooks.Item(1).Worksheets(NomeFoglio)
    myReport.Activate
     
    If myReport Is Nothing Then
      MsgBox "Report not found. Enable to copy on template Report" & Key, vbCritical, "i-4.Migration - Export Report"
      Exit Sub
    End If
     
    isTrovato = False
    For i = 1 To 65536
      If UCase(myReport.Cells(i, 1)) = "<START>" Then
        ' Controllo se ci sono pi� righe di quante ne posso incollare sul foglio XLS
        rowLeft = RowCount - 65536 - (i - 1)
        ' rowLeft = numero di righe in eccedenza
        If rowLeft > 0 Then
          ' Serve una nuova pagina, la copio dalla pagina corrente
          myReport.Copy , Sheets(myReport.Index)
          myReport.Activate
        End If
        myRange = "A" & i
        myReport.Range(myRange).Select

        With myReport.QueryTables.Add(Connection:="TEXT;" & strFileName, _
                                      Destination:=myReport.Range(myRange))
          .Name = "P"
          .FieldNames = False
          .RowNumbers = False
          .FillAdjacentFormulas = False
          .PreserveFormatting = True
          .RefreshOnFileOpen = False
          .RefreshStyle = xlOverwriteCells
          .SavePassword = False
          .SaveData = False
          .AdjustColumnWidth = False
          .RefreshPeriod = 0
          .TextFilePromptOnRefresh = False
          .TextFilePlatform = 1252
          .TextFileStartRow = 1
          .TextFileParseType = xlDelimited
          .TextFileTextQualifier = xlTextQualifierDoubleQuote
          .TextFileConsecutiveDelimiter = False
          .TextFileTabDelimiter = False
          .TextFileSemicolonDelimiter = False
          .TextFileCommaDelimiter = False
          .TextFileSpaceDelimiter = False
          .TextFileOtherDelimiter = "|"
          .TextFileColumnDataTypes = Array(1)
          .TextFileTrailingMinusNumbers = True
          .Refresh BackgroundQuery:=False
        End With
        isTrovato = True
        If rowLeft > 0 Then
          ' Imposto la nuova pagina come MyReport, per ricominciare il giro
          Set myReport = wBooks.Item(1).Worksheets(myReport.Index + 1)
          myReport.Activate
          ' Imposto il RecordCount con le righe che rimangono da "copiare"
          RowCount = rowLeft
          StartRow = 65536 - (i - 2)
          i = 0
          ' Devo fare sto giro assurdo perch� su XLS non incolla pi� di 65356 righe
          strFileNameO = strFileName & "_"
          InFile = FreeFile
          Open strFileName For Input As InFile
          OutFile = FreeFile
          Open strFileNameO For Output As OutFile
          j = 0
          ' Creo un nuovo file riempiendolo con le righe non copiate sul foglio XLS
          Do Until EOF(InFile)
            j = j + 1
            Line Input #InFile, line
            If j >= StartRow Then
              Print #OutFile, line
            End If
          Loop
          Close InFile
          Close OutFile
          ' Sovrascrivo il file di testo con il nuovo contenente solo le righe ancora da copiare
          Set fso = CreateObject("Scripting.FileSystemObject")
          fso.copyfile strFileNameO, strFileName, True
          Kill strFileNameO
        Else
          Exit For
        End If
      End If
    Next i
     
    If Not isTrovato Then
      MsgBox "Tag <START> not found. Enable to import on template Report " & Key, vbCritical, "i-4.Migration - Export Report"
    End If
    Set myReport = Nothing
  Else
    Set myReport = wBooks.Item(1).Worksheets(NomeFoglio)
    myReport.Activate
     
    If myReport Is Nothing Then
      MsgBox "Report not found. Enable to copy on template Report" & Key, vbCritical, "i-4.Migration - Export Report"
      Exit Sub
    End If
  
    isTrovato = False
    For i = 1 To 65536
      If UCase(myReport.Cells(i, 1)) = "<START>" Then
        myRange = "A" & i
        myReport.Range(myRange).Select
        myReport.Range(myRange).ClearContents
        isTrovato = True
        Exit For
      End If
    Next i
     
    If Not isTrovato Then
      MsgBox "Tag <START> not found. Enable to import on template Report " & Key, vbCritical, "i-4.Migration - Export Report"
    End If
     
    Set myReport = Nothing
  End If
  Kill strFileName
  Exit Sub
ErrorHandler:
  If ERR.Number = 9 Then
    MsgBox "Report not found. Enable to copy on template Report " & Key, vbCritical, "i-4.Migration - Export Report"
    Exit Sub
  End If
  Kill strFileName
'  Kill NomeFile
  MsgBox ERR.Number & " - " & ERR.Description
  Screen.MousePointer = vbDefault
End Sub

' silvia: ho aggiunto il FieldCount x contare il numero dei campi in una select
Public Sub Crea_Foglio_Txt(Key As String, Sql As String, FieldCount As Integer, RowCount As Long)
  Dim rs As Recordset
  Dim i As Long
  Dim rigaFile As String
   
  On Error GoTo ErrorHandler
 
  'SQ - TMP... SEGRETO PER ANDARE SULLA SYSTEM!
  If MabsdF_Print.txtSQL = "SYSTEM-QUERY:" Then
    Set rs = m_fun.Open_Recordset_Sys(Sql)
  Else
    Set rs = m_fun.Open_Recordset(Sql)
  End If
  FieldCount = rs.Fields.Count
  RowCount = rs.RecordCount
  'Screen.MousePointer = vbHourglass
  Print #numFileTXT, rigaFile
  Print #numFileTXT, rigaFile
  Print #numFileTXT, rigaFile
  
  'Intestazione dei campi
  For i = 0 To rs.Fields.Count - 1
    rigaFile = rigaFile & Trim(UCase(rs.Fields(i).Name))
    If i < rs.Fields.Count - 1 Then
      rigaFile = rigaFile & "�"
    End If
  Next i
  Print #numFileTXT, rigaFile
  
  If rs.RecordCount > 0 Then
    While Not rs.EOF
      'Incolla i dati nel file txt
      rigaFile = ""
      For i = 0 To rs.Fields.Count - 1
        rigaFile = rigaFile & Trim(rs.Fields(i).Value)
        If i < rs.Fields.Count - 1 Then
          rigaFile = rigaFile & "�"
        End If
      Next i
      Print #numFileTXT, rigaFile
      rs.MoveNext
    Wend
    Print #numFileTXT, ""
  End If
  rs.Close
   
  Exit Sub
'SILVIA
ErrorHandler:
  'Kill strfilename
  'wbook.Close
  'Kill NomeFile
  MsgBox ERR.Number & " - " & ERR.Description & " - for Report: " & Key
 'Screen.MousePointer = vbDefault
End Sub

Public Sub Crea_Foglio_Txt_Template(Key As String, Sql As String, FieldCount As Integer, RowCount As Long)
  Dim rs As Recordset
  Dim i As Long
  Dim rigaFile As String
   
  On Error GoTo ErrorHandler

  Set rs = m_fun.Open_Recordset(Sql)
  
  FieldCount = rs.Fields.Count
  RowCount = rs.RecordCount
  
  If rs.RecordCount Then
    While Not rs.EOF
      'Incolla i dati nel file txt
      rigaFile = ""
      For i = 0 To rs.Fields.Count - 1
        rigaFile = rigaFile & Trim(rs.Fields(i).Value)
        If i < rs.Fields.Count - 1 Then
          rigaFile = rigaFile & "|"
        End If
      Next i
      Print #numFileTXT, rigaFile
      rs.MoveNext
    Wend
    Print #numFileTXT, ""
  End If
  rs.Close
   
  Exit Sub
ErrorHandler:
  MsgBox ERR.Number & " - " & ERR.Description & " - for Report: " & Key
End Sub

' Mauro 05/11/2007: Controllo caratteri riservati nella chiave
Public Function controllaQuery(QueryName As String) As Boolean
  Dim i As Integer
  Dim CHAR As String
  
  controllaQuery = True
  For i = 1 To Len(QueryName)
    CHAR = Mid(QueryName, i, 1)
    If CHAR = ":" Or CHAR = "/" Or CHAR = "\" Or _
      CHAR = "?" Or CHAR = "*" Or CHAR = "[" Or CHAR = "]" Then
      MsgBox "Character '" & CHAR & "' not admitted in Query Name!", vbOKOnly + vbExclamation
      controllaQuery = False
      Exit For
    End If
  Next i
End Function

'silvia
'claudio - 08/11/2010 cambio separatore("|" al posto di "|")
Public Sub Carica_Istr_DLI(Filename As String)
  Dim rs As Recordset, rs1 As Recordset, r1 As Recordset, r2 As Recordset, r3 As Recordset
  Dim i As Long
  Dim rigaFile As String, riga As String, riga1 As String, riga2 As String, riga3 As String
  Dim txtPGM As String, txtINCLUDE As String, txtRiga As String, txtIstruzione As String, txtStatement As String
  Dim txtPSB As String, txtDBD As String, txtVariabile As String, txtValore As String, txtSegmento As String, txtChiave As String, txtProcSeq As String
  
  On Error GoTo ErrorHandler
  
  'SQ tmp
  If Left(m_fun.FnNomeDB, 14) <> "prj-RealeMutua" Then
    Set rs = m_fun.Open_Recordset("SELECT a.*, b.nome FROM PSDLI_Istruzioni as a, Bs_Oggetti as b WHERE " & _
                                  "a.idPgm > 0 and a.idPgm = b.idOggetto order by a.idpgm, a.idoggetto")
  Else
    Set rs = m_fun.Open_Recordset("SELECT a.*, b.nome FROM PSDLI_Istruzioni as a, Bs_Oggetti as b WHERE " & _
                                  "a.idPgm > 0 and a.idPgm = b.idOggetto AND b.notes = 'CONV' order by a.idpgm, a.idoggetto")
  End If
  If rs.RecordCount Then
    Screen.MousePointer = vbHourglass
    numFileTXT = FreeFile
    Open Filename For Output As numFileTXT
    'SQ statement portato in fondo
    rigaFile = "PGM|COPY|Linea|Istr|PCB-variabile|PCB-valore|Valida|PSB|DBD|ProcSeq|Livello|Segmento|IO-Area|Qualif|Chiave|Operatore|Booleano|Statement"
    Print #numFileTXT, rigaFile
    
    While Not rs.EOF
      txtPGM = rs!Nome
      'NOME cpy (meglio in JOIN sopra?)
      Set rs1 = m_fun.Open_Recordset("SELECT nome FROM BS_Oggetti WHERE " & _
                                     "IdOggetto = " & rs!IdOggetto)
      If rs1.RecordCount Then
        txtINCLUDE = rs1!Nome
      Else
        txtINCLUDE = ""
        'SEGNALAZIONE!!!!!!
      End If
      rs1.Close
      
      txtRiga = rs!riga
      txtIstruzione = rs!Istruzione
      'clone
      Set rs1 = m_fun.Open_Recordset("SELECT Istruzione FROM PSDLI_Istruzioni_Clone WHERE " & _
                                     "IdPgm = " & rs!idPgm & " AND IdOggetto = " & rs!IdOggetto & " AND Riga = " & rs!riga)
      While Not rs1.EOF
        txtIstruzione = txtIstruzione & "," & rs1!Istruzione
        rs1.MoveNext
      Wend
      rs1.Close
      
      txtStatement = rs!Statement
      If rs!OrdPCB_mg <> 0 Then
        txtValore = "" & rs!OrdPCB_mg
      Else
        txtValore = "0" & rs!ordPCB
      End If
      'SQ dava errore sotto!? If txtValore = "00" Then txtValore = "-"
      txtVariabile = "" & rs!NumPCB
      rigaFile = ""
      'SQ statement portato in fondo
      riga = Trim(txtPGM) & "|" & Trim(txtINCLUDE) & "|" & Trim(txtRiga) & "|" & Trim(txtIstruzione) & "|" & _
             txtVariabile & "|" & IIf(txtValore <> "" & txtValore <> "0", txtValore, "-") & "|" & IIf(rs!valida, "X", "") ' & "|" & Trim(txtStatement)
      txtPSB = ""
      txtDBD = ""
      Set r1 = m_fun.Open_Recordset("SELECT Nome, IdPSB FROM PSDLI_IstrPsb as A, BS_Oggetti as B WHERE " & _
                                    "IdPSB = B.IdOggetto AND A.IdPgm = " & rs!idPgm & " AND A.IdOggetto = " & rs!IdOggetto & " AND " & _
                                    "A.Riga = " & rs!riga)
      While Not r1.EOF
        If IsNull(rs!NumPCB) = False Then
          Set r3 = m_fun.Open_Recordset("SELECT ProcSeq from PSDLI_Psb WHERE " & _
                                        "IdOggetto = " & r1!idPSB & " AND numPCB = " & Int(txtValore))
          If r3.RecordCount Then
            txtProcSeq = IIf(r3!procSeq <> vbNull And r3!procSeq <> "None", r3!procSeq, "")
          End If
          r3.Close
        End If
        If txtPSB = "" Then
          txtPSB = r1!Nome
        Else
          txtPSB = txtPSB & ", " & r1!Nome
        End If
        r1.MoveNext
      Wend
      r1.Close
            
      Set r1 = m_fun.Open_Recordset("SELECT Nome FROM PSDLI_IstrDBD as A, BS_Oggetti as B WHERE " & _
                                    "IdDBD = B.IdOggetto AND A.IdPgm = " & rs!idPgm & " AND A.IdOggetto = " & rs!IdOggetto & " AND " & _
                                    "A.Riga = " & rs!riga)
      While Not r1.EOF
        If txtDBD = "" Then
          txtDBD = r1!Nome
        Else
          txtDBD = txtDBD & ", " & r1!Nome
        End If
        r1.MoveNext
      Wend
      r1.Close

      riga1 = "|" & Trim(txtPSB) & "|" & Trim(txtDBD) & "|" & Trim(txtProcSeq)
      Set r2 = m_fun.Open_Recordset("SELECT *  FROM PSDLI_IstrDett_Liv as A, PsDLI_Segmenti as B WHERE " & _
                                    "A.IdPgm = " & rs!idPgm & " AND A.IdOggetto = " & rs!IdOggetto & " AND " & _
                                    "A.Riga = " & rs!riga & " AND A.IdSegmento = B.IdSegmento ORDER BY Livello")
      riga2 = ""
      If r2.RecordCount Then
        While Not r2.EOF
          riga2 = "|" & Trim(r2!Livello) & "|" & Trim(r2!Nome) & "|" & r2!DataArea & "|" & IIf(r2!Qualificazione, "X", "")
          Set r3 = m_fun.Open_Recordset("SELECT * FROM PSDLI_IstrDett_Key WHERE " & _
                                        "IdPgm = " & rs!idPgm & " AND IdOggetto = " & rs!IdOggetto & " AND " & _
                                        "Riga = " & rs!riga & " AND Livello = " & r2!Livello & " ORDER BY Livello, Progressivo")
          If r3.RecordCount Then
            While Not r3.EOF
              If r3!idField > 0 Then
                Set r1 = m_fun.Open_Recordset("SELECT * FROM PSDLI_Field WHERE IdField = " & r3!idField)
              Else
                Set r1 = m_fun.Open_Recordset("SELECT * FROM PSDLI_XDField WHERE IdXDField = " & (r3!idField * -1))
              End If
              txtChiave = r1!Nome
              riga3 = ""
              riga3 = "|" & Trim(txtChiave)
              rigaFile = ""
              rigaFile = riga & riga1 & riga2 & riga3 & "|" & r3!operatore & "|" & r3!Booleano & "|" & rs!Statement
              Print #numFileTXT, rigaFile
              r3.MoveNext
              riga = "|||||||"
              riga1 = "||"
              riga2 = "|"
            Wend
          Else
            rigaFile = ""
            rigaFile = riga & riga1 & riga2
            Print #numFileTXT, rigaFile
            riga = "|||||||"
            riga1 = "|||"
          End If
          r3.Close
          r2.MoveNext
        Wend
      Else
        rigaFile = ""
        rigaFile = riga & riga1
        Print #numFileTXT, rigaFile
        riga = "|||||||"
      End If
      r2.Close
      rs.MoveNext
      '
      DoEvents
    Wend
    Print #numFileTXT, ""
    Close numFileTXT
  End If
  rs.Close
  Caricatxt Filename
  Kill Filename
  Screen.MousePointer = vbDefault
  Exit Sub
ErrorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002", "MabsdM_Print" & ERR.Description, True
End Sub

Sub Caricatxt(strFileName As String)
  Dim strfilexls As String
 
  strfilexls = Replace(strFileName, "\TXT\", "\Excel\", , , vbTextCompare)
  strfilexls = Replace(strfilexls, ".txt", ".xls")
    
  Workbooks.OpenText Filename:=strFileName, _
       Origin:=xlMSDOS, StartRow:=1, DataType:=xlDelimited, TextQualifier:=xlNone, _
       ConsecutiveDelimiter:=False, Tab:=False, Semicolon:=False, Comma:=False, _
       Space:=False, Other:=True, OtherChar:="|", FieldInfo:=Array( _
       Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1), Array(5, 1), Array(6, 1), Array(7, 1), _
       Array(8, 1), Array(9, 1), Array(10, 1), Array(11, 1)), TrailingMinusNumbers:=True
  Columns("A:A").EntireColumn.AutoFit
  Columns("B:B").EntireColumn.AutoFit
  Columns("C:C").EntireColumn.AutoFit
  Columns("D:D").EntireColumn.AutoFit
  Columns("E:E").EntireColumn.AutoFit
  Columns("F:F").EntireColumn.AutoFit
  Columns("G:G").EntireColumn.AutoFit
  Columns("H:H").EntireColumn.AutoFit
  Columns("I:I").EntireColumn.AutoFit
  Columns("J:J").EntireColumn.AutoFit
  Columns("K:K").EntireColumn.AutoFit
  Columns("L:L").EntireColumn.AutoFit
  Columns("M:M").EntireColumn.AutoFit
  Columns("N:N").EntireColumn.AutoFit
  Columns("O:O").EntireColumn.AutoFit
  Columns("P:P").EntireColumn.AutoFit
  Columns("Q:Q").EntireColumn.AutoFit
  Columns("R:R").EntireColumn.AutoFit
  Range("A1:R1").Select
  Selection.Font.Bold = True
  With Selection.Interior
    .ColorIndex = 35
    .Pattern = xlSolid
  End With
  With Selection.Borders(xlEdgeLeft)
    .LineStyle = xlContinuous
    .Weight = xlThin
  End With
  With Selection.Borders(xlEdgeTop)
    .LineStyle = xlContinuous
    .Weight = xlThin
  End With
  With Selection.Borders(xlEdgeBottom)
    .LineStyle = xlContinuous
    .Weight = xlThin
  End With
  With Selection.Borders(xlEdgeRight)
    .LineStyle = xlContinuous
    .Weight = xlThin
  End With
  With Selection.Borders(xlInsideVertical)
    .LineStyle = xlContinuous
    .Weight = xlThin
  End With

  ActiveWorkbook.SaveAs Filename:=strfilexls, _
      FileFormat:=xlNormal, Password:="", WriteResPassword:="", _
      ReadOnlyRecommended:=False, CreateBackup:=False
  ActiveWorkbook.Sheets.Select
  ActiveWorkbook.Sheets(1).Name = "DLI Instruction"
  Selection.AutoFilter
  ActiveWorkbook.Save
  ActiveWorkbook.Close
End Sub

Public Function FileExists(path$) As Boolean
  'verifica l'esistenza di un file
  Dim x As Integer
  On Error Resume Next
    
  x = FreeFile
  Open path$ For Input As x
  'con Append � come se aprissi il file quindi gli altri processi devono aspettare.
  If ERR = 0 Then
    FileExists = True
  Else
    FileExists = False
  End If
  Close x
  ERR = 0
End Function
