VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MabseF_Parameters 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Projects' Parameters"
   ClientHeight    =   5985
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   10185
   Icon            =   "MabseF_Parameters.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5985
   ScaleWidth      =   10185
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame frmParameterValues 
      Caption         =   "Parameter Value(s)"
      ForeColor       =   &H00000080&
      Height          =   4005
      Left            =   3720
      TabIndex        =   2
      Top             =   360
      Width           =   3975
      Begin VB.TextBox txtParameterDescription 
         BackColor       =   &H80000018&
         ForeColor       =   &H00C00000&
         Height          =   885
         Left            =   240
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   9
         Top             =   300
         Width           =   3585
      End
      Begin VB.CommandButton cmdSystemPrint 
         Height          =   375
         Left            =   1320
         TabIndex        =   8
         Top             =   3450
         Width           =   1815
      End
      Begin VB.CommandButton cmdSystemColor 
         Height          =   375
         Left            =   660
         TabIndex        =   7
         Top             =   3450
         Width           =   1815
      End
      Begin VB.CommandButton cmdSystemFont 
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   3450
         Width           =   1815
      End
      Begin MSComDlg.CommonDialog cdlgSystem 
         Left            =   3330
         Top             =   390
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.OptionButton optBoolean 
         Caption         =   "No"
         ForeColor       =   &H00C00000&
         Height          =   255
         Index           =   1
         Left            =   1230
         TabIndex        =   5
         Top             =   1320
         Width           =   825
      End
      Begin VB.OptionButton optBoolean 
         Caption         =   "Yes"
         ForeColor       =   &H00C00000&
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   4
         Top             =   1320
         Width           =   795
      End
      Begin VB.TextBox txtString 
         BackColor       =   &H00FFFFC0&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   1260
         Width           =   3585
      End
      Begin MSComctlLib.ListView lswMultiSelect 
         Height          =   2565
         Left            =   270
         TabIndex        =   10
         Top             =   1260
         Width           =   3555
         _ExtentX        =   6271
         _ExtentY        =   4524
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         Checkboxes      =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Value"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.ListBox lstParameters 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   3765
      ItemData        =   "MabseF_Parameters.frx":000C
      Left            =   480
      List            =   "MabseF_Parameters.frx":000E
      TabIndex        =   1
      Top             =   460
      Width           =   3015
   End
   Begin MSComctlLib.TabStrip tabCategories 
      Height          =   5775
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   10186
      Placement       =   1
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MabseF_Parameters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mbLoadList As Boolean
Private miPreviousSelectedValue As Integer
Private msFontSelected As String, mlColorSelected As String

Private Sub Retrieveparam()
    Dim arrReturnArray() As String
    Dim arrKeyArray(10) As String
    Dim i As Integer, j As Integer
    
    On Error GoTo EH
    
    arrKeyArray(0) = "IMSDB_DefPsb"
    arrKeyArray(1) = "DocumentPath"
    arrKeyArray(2) = "DeleteFilePermanently"
    arrKeyArray(3) = "SelectFontType"
    arrKeyArray(4) = "SelectFontColor"
    arrKeyArray(5) = "SelectPrinter"
    arrKeyArray(6) = "EditorTextColor"
    arrKeyArray(7) = "Test"
    arrKeyArray(8) = "MultipleTest"
    arrKeyArray(9) = ""
    arrKeyArray(10) = "DoesNotExist" 'To Check For Error Handling
    
    For j = 0 To UBound(arrKeyArray)
        arrReturnArray = m_fun.RetrieveParameterValues(arrKeyArray(j))
    
        For i = 0 To UBound(arrReturnArray)
            MsgBox arrReturnArray(i)
        Next i
    Next
    
    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear
End Sub

Private Sub cmdSystemColor_Click()

    On Error GoTo EH

    cdlgSystem.Flags = cdlCCRGBInit
    cdlgSystem.Color = mlColorSelected
    cdlgSystem.ShowColor

    mlColorSelected = cdlgSystem.Color

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub cmdSystemFont_Click()

    On Error GoTo EH
    
    cdlgSystem.Flags = cdlCFScreenFonts
    cdlgSystem.FontName = msFontSelected
    cdlgSystem.ShowFont
    
    msFontSelected = cdlgSystem.FontName
    
    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear
    
End Sub

Private Sub cmdSystemPrint_Click()
    
    On Error GoTo EH
    
    cdlgSystem.ShowPrinter
    
    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub Form_Load()

    On Error GoTo EH
   
    'Setta il parent
    'SetParent Me.hwnd, Menu.BsParent
  
    mbLoadList = False
    miPreviousSelectedValue = -1

    'Call Resize

    SetTabCategories
    
    'Aggiunge la form alla collection
    m_fun.AddActiveWindows Me
    m_fun.FnActiveWindowsBool = True
    
    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear
    
End Sub

Private Sub SetTabCategories()

    Dim r As ADODB.Recordset

    On Error GoTo EH

    Set r = m_fun.Open_Recordset_Sys("Select DISTINCT Categories.ID, Categories.CategoryName " & _
        "From Categories, [Parameters] WHERE Categories.ID = [Parameters].CategoryID ORDER BY Categories.CategoryName")

'    r.MoveFirst

    Do While Not r.EOF

        If r.AbsolutePosition = 1 Then

'            tabCategories.Tabs(1).Index = r.Fields("ID").Value
            tabCategories.Tabs(1).Caption = r.Fields("CategoryName").Value
            tabCategories.Tabs(1).Key = "ID" & r.Fields("ID").Value

'            Call SetParameterList(r.Fields("ID").Value)

        Else

            tabCategories.Tabs.Add , , r.Fields("CategoryName").Value
            tabCategories.Tabs(tabCategories.Tabs.Count).Key = "ID" & r.Fields("ID").Value
        
        End If
        
        r.MoveNext

    Loop

    r.Close

    Set r = Nothing
    
    tabCategories.Tabs(1).Selected = True

    SetParameterList Mid(tabCategories.Tabs(1).Key, 3, Len(tabCategories.Tabs(1).Key))

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub SetParameterList(iCategoryID As Integer)

    Dim r As ADODB.Recordset

    On Error GoTo EH

    Set r = m_fun.Open_Recordset_Sys("Select [Parameters].ID, [Parameters].ParameterName, " & _
        "[Parameters].ParameterLabel, [Parameters].ParameterDescription, " & _
        "[Parameters].ParameterTypeID, [Parameters].ParameterSelectTypeID " & _
        "From [Parameters] WHERE [Parameters].CategoryID = " & iCategoryID & " ORDER BY [Parameters].ParameterLabel ")

    ClearAllFields True

'    r.MoveFirst

    Do While Not r.EOF

        lstParameters.AddItem r.Fields("ParameterLabel").Value
        
        lstParameters.ItemData(lstParameters.ListCount - 1) = r.Fields("ID").Value
        
        r.MoveNext

    Loop
    
    r.Close
    
    Set r = Nothing

    lstParameters.ListIndex = 0

    lstParameters.Selected(0) = True
    
    miPreviousSelectedValue = 0
    
    SetParameterSystemValue lstParameters.ItemData(lstParameters.ListIndex)

    mbLoadList = True

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub SetParameterSystemValue(iParameterID As Integer)

    Dim r As ADODB.Recordset

    On Error GoTo EH

    Set r = m_fun.Open_Recordset_Sys("Select [Parameters].ID, [Parameters].ParameterName, " & _
        "[Parameters].ParameterLabel, [Parameters].ParameterDescription, " & _
        "[Parameters].ParameterTypeID, [Parameters].ParameterSelectTypeID, " & _
        "ParameterTypes.ParameterTypeName, ParameterSelectTypes.ParameterSelectTypeName " & _
        "From [Parameters], ParameterTypes, ParameterSelectTypes  " & _
        "WHERE " & _
        "ParameterSelectTypes.ID = [Parameters].ParameterSelectTypeID AND " & _
        "ParameterTypes.ID = [Parameters].ParameterTypeID AND [Parameters].ID = " & iParameterID & " ORDER BY [Parameters].ParameterLabel ")
    
    ClearAllFields False

'    r.MoveFirst

    Do While Not r.EOF

        txtParameterDescription.Text = IIf(IsNull(r.Fields("ParameterDescription").Value), vbNullString, r.Fields("ParameterDescription").Value)

        If r.Fields("ParameterSelectTypeName").Value = "Single" Then

            If r.Fields("ParameterTypeName").Value = "Boolean" Then
    
                optBoolean(0).Left = txtParameterDescription.Left
                optBoolean(1).Left = optBoolean(0).Left + 780
    
                optBoolean(0).Top = txtParameterDescription.Top + txtParameterDescription.Height + 50
                optBoolean(1).Top = txtParameterDescription.Top + txtParameterDescription.Height + 50
    
                optBoolean(0).Visible = True
                optBoolean(1).Visible = True
            
            ElseIf r.Fields("ParameterTypeName").Value = "String" Then
            
                txtString.Top = txtParameterDescription.Top + txtParameterDescription.Height + 50
            
                txtString.Left = txtParameterDescription.Left
                
                txtString.DataFormat.type = 0
            
                txtString.Visible = True
            
            ElseIf r.Fields("ParameterTypeName").Value = "Numeric" Then
    
                txtString.Top = txtParameterDescription.Top + txtParameterDescription.Height + 50
    
                txtString.Left = txtParameterDescription.Left
    
                txtString.DataFormat.type = 1
            
                txtString.Visible = True
            
            ElseIf r.Fields("ParameterTypeName").Value = "System Font" Then
            
                cmdSystemFont.Top = txtParameterDescription.Top + txtParameterDescription.Height + 50
                
                cmdSystemFont.Left = txtParameterDescription.Left
                
                cmdSystemFont.Caption = r.Fields("ParameterTypeName").Value
            
                cmdSystemFont.Visible = True
            
            ElseIf r.Fields("ParameterTypeName").Value = "System Color" Then
            
                cmdSystemColor.Top = txtParameterDescription.Top + txtParameterDescription.Height + 50
                
                cmdSystemColor.Left = txtParameterDescription.Left
                
                cmdSystemColor.Caption = r.Fields("ParameterTypeName").Value
            
                cmdSystemColor.Visible = True
            
            ElseIf r.Fields("ParameterTypeName").Value = "System Printer" Then
            
                cmdSystemPrint.Top = txtParameterDescription.Top + txtParameterDescription.Height + 50
                
                cmdSystemPrint.Left = txtParameterDescription.Left
                
                cmdSystemPrint.Caption = r.Fields("ParameterTypeName").Value
            
                cmdSystemPrint.Visible = True
                
            End If
        
            
            GetParameterSavedValue
        
        
        ElseIf r.Fields("ParameterSelectTypeName").Value = "Multiple" Then
        
            SetParameterSystemMultipleValue iParameterID
        
        End If
        
        r.MoveNext

    Loop

    r.Close

    Set r = Nothing

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub Form_Resize()
    ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)

    On Error GoTo EH

    SaveParameterValues
    
    'Aggiorna funzioni
    m_fun.Load_Parametri_Progetto
    
    'Aggiunge la form alla collection
    m_fun.RemoveActiveWindows Me
    m_fun.FnActiveWindowsBool = False
    
    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub lstParameters_Click()

    On Error GoTo EH

    If mbLoadList = True Then

        SaveParameterValues
    
        miPreviousSelectedValue = lstParameters.ListIndex
    
        SetParameterSystemValue lstParameters.ItemData(lstParameters.ListIndex)

    End If

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub lswMultiSelect_ItemCheck(ByVal Item As MSComctlLib.ListItem)
   Dim i As Long
   
   For i = 1 To lswMultiSelect.ListItems.Count
      lswMultiSelect.ListItems(i).Checked = False
   Next i
   
   Item.Checked = True
End Sub

Private Sub tabCategories_Click()

    On Error GoTo EH

    If lstParameters.ListIndex <> -1 Then

        SaveParameterValues
        
        mbLoadList = False
        miPreviousSelectedValue = -1
        
        SetParameterList Mid(tabCategories.SelectedItem.Key, 3, Len(tabCategories.SelectedItem.Key))
    
    End If

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub txtString_KeyPress(KeyAscii As Integer)

    On Error GoTo EH

    If txtString.DataFormat.type = 1 Then
    
        If KeyAscii < 48 Or KeyAscii > 57 Then
        
            KeyAscii = 0
            
        End If
    
    End If

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub GetParameterSavedValue()

    Dim r As ADODB.Recordset
    Dim rSave As ADODB.Recordset
        
    On Error GoTo EH

    Set r = m_fun.Open_Recordset_Sys("Select [Parameters].ID, [Parameters].ParameterName, " & _
        "[Parameters].ParameterLabel, [Parameters].ParameterDescription, " & _
        "[Parameters].ParameterTypeID, [Parameters].ParameterSelectTypeID, " & _
        "ParameterTypes.ParameterTypeName, ParameterSelectTypes.ParameterSelectTypeName " & _
        "From [Parameters], ParameterTypes, ParameterSelectTypes  " & _
        "WHERE " & _
        "ParameterSelectTypes.ID = [Parameters].ParameterSelectTypeID AND " & _
        "ParameterTypes.ID = [Parameters].ParameterTypeID AND [Parameters].ID = " & lstParameters.ItemData(lstParameters.ListIndex) & " ORDER BY [Parameters].ParameterLabel ")

    Set rSave = m_fun.Open_Recordset("SELECT BS_Parametri.ID, BS_Parametri.ParameterSystemID, BS_Parametri.ParameterValue FROM BS_Parametri WHERE ParameterSystemID = " & lstParameters.ItemData(lstParameters.ListIndex) & "")

'    r.MoveFirst
'    rSave.MoveFirst

    Do While Not r.EOF

        If r.Fields("ParameterSelectTypeName").Value = "Single" Then
            
            If r.Fields("ParameterTypeName").Value = "Boolean" Then
                
                If rSave.RecordCount > 0 Then

                    If rSave.Fields("ParameterValue").Value = "Yes" Then

                        optBoolean(0).Value = True

                    ElseIf rSave.Fields("ParameterValue").Value = "No" Then

                        optBoolean(1).Value = True

                    End If
            
                End If
            
            ElseIf r.Fields("ParameterTypeName").Value = "String" Then
            
                If rSave.RecordCount > 0 Then
                    
                    txtString.Text = Trim(rSave.Fields("ParameterValue").Value)
                    
                End If
            
            ElseIf r.Fields("ParameterTypeName").Value = "Numeric" Then
    
                If rSave.RecordCount > 0 Then
                    
                    txtString.Text = Trim(rSave.Fields("ParameterValue").Value)
                    
                End If
                
            ElseIf r.Fields("ParameterTypeName").Value = "System Font" Then
            
                If rSave.RecordCount > 0 Then
            
                    msFontSelected = Trim(rSave.Fields("ParameterValue").Value)
            
                End If
                
            ElseIf r.Fields("ParameterTypeName").Value = "System Color" Then
            
                If rSave.RecordCount > 0 Then
                
                    mlColorSelected = CLng(Trim(rSave.Fields("ParameterValue").Value))
                    
                End If
            
            ElseIf r.Fields("ParameterTypeName").Value = "System Print" Then
            
'                If rSave.RecordCount > 0 Then
'
'                   msPrinterSelected = Trim(rSave.Fields("ParameterValue").Value)
'
'                End If
            End If
        
        ElseIf r.Fields("ParameterSelectTypeName").Value = "Multiple" Then
        
        
        End If
        
        r.MoveNext
        
    Loop

    r.Close
    rSave.Close
    
    Set r = Nothing
    Set rSave = Nothing

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub SaveParameterValues()

    Dim r As ADODB.Recordset
    Dim rSave As ADODB.Recordset
        
    On Error GoTo EH

    Set r = m_fun.Open_Recordset_Sys("Select [Parameters].ID, [Parameters].ParameterName, " & _
        "[Parameters].ParameterLabel, [Parameters].ParameterDescription, " & _
        "[Parameters].ParameterTypeID, [Parameters].ParameterSelectTypeID, " & _
        "ParameterTypes.ParameterTypeName, ParameterSelectTypes.ParameterSelectTypeName " & _
        "From [Parameters], ParameterTypes, ParameterSelectTypes  " & _
        "WHERE " & _
        "ParameterSelectTypes.ID = [Parameters].ParameterSelectTypeID AND " & _
        "ParameterTypes.ID = [Parameters].ParameterTypeID AND [Parameters].ID = " & lstParameters.ItemData(miPreviousSelectedValue) & " ORDER BY [Parameters].ParameterLabel ")
    
    Set rSave = m_fun.Open_Recordset("SELECT BS_Parametri.ID, BS_Parametri.ParameterSystemID, BS_Parametri.ParameterValue FROM BS_Parametri WHERE ParameterSystemID = " & lstParameters.ItemData(miPreviousSelectedValue) & "")
    
'    r.MoveFirst
'    rSave.MoveFirst
    
    Do While Not r.EOF

        If r.Fields("ParameterSelectTypeName").Value = "Single" Then

            If r.Fields("ParameterTypeName").Value = "Boolean" Then
    
                If optBoolean(0).Value = True Then
                                    
                    If rSave.RecordCount > 0 Then
                                    
                        rSave.Fields("ParameterValue").Value = "Yes"
                        rSave.Update
                    
                    Else
                    
                        rSave.AddNew
                        rSave.Fields("ParameterSystemID").Value = r.Fields("ID").Value
                        rSave.Fields("ParameterValue").Value = "Yes"
                        rSave.Update
                    
                    End If
                    
                ElseIf optBoolean(1).Value = True Then
                
                    If rSave.RecordCount > 0 Then

                        rSave.Fields("ParameterValue").Value = "No"
                        rSave.Update
                        
                    Else
                    
                        rSave.AddNew
                        rSave.Fields("ParameterSystemID").Value = r.Fields("ID").Value
                        rSave.Fields("ParameterValue").Value = "No"
                        rSave.Update
                    
                    End If
    
                End If
            
            ElseIf r.Fields("ParameterTypeName").Value = "String" Then
            
                If rSave.RecordCount > 0 Then
                                
                    If Trim(txtString.Text) <> vbNullString Then

                        rSave.Fields("ParameterValue").Value = Trim(txtString.Text)
                        rSave.Update
                
                    Else
                
                        Menu.BsConnection.Execute "DELETE FROM BS_Parametri WHERE ID = " & rSave.Fields("ID").Value
                
                    End If
                
                Else
                
                    If Trim(txtString.Text) <> vbNullString Then
                
                        rSave.AddNew
                        rSave.Fields("ParameterSystemID").Value = r.Fields("ID").Value
                        rSave.Fields("ParameterValue").Value = Trim(txtString.Text)
                        rSave.Update
                
                    End If
                
                End If
            
            ElseIf r.Fields("ParameterTypeName").Value = "Numeric" Then
    
                If rSave.RecordCount > 0 Then
                                
                    If Trim(txtString.Text) <> vbNullString Then
                                
                        rSave.Fields("ParameterValue").Value = Trim(txtString.Text)
                        rSave.Update
                
                    Else
                
                        Menu.BsConnection.Execute "DELETE FROM BS_Parametri WHERE ID = " & rSave.Fields("ID").Value

                    End If
                
                Else
                
                    If Trim(txtString.Text) <> vbNullString Then
                
                        rSave.AddNew
                        rSave.Fields("ParameterSystemID").Value = r.Fields("ID").Value
                        rSave.Fields("ParameterValue").Value = Trim(txtString.Text)
                        rSave.Update
                
                    End If
                    
                End If
                
            ElseIf r.Fields("ParameterTypeName").Value = "System Font" Then
            
                If rSave.RecordCount > 0 Then
                                
                    If Trim(msFontSelected) <> vbNullString Then
                                
                        rSave.Fields("ParameterValue").Value = Trim(msFontSelected)
                        rSave.Update
                
                    Else
                
                        Menu.BsConnection.Execute "DELETE FROM BS_Parametri WHERE ID = " & rSave.Fields("ID").Value

                    End If
                
                Else
                
                    If Trim(msFontSelected) <> vbNullString Then

                        rSave.AddNew
                        rSave.Fields("ParameterSystemID").Value = r.Fields("ID").Value
                        rSave.Fields("ParameterValue").Value = Trim(msFontSelected)
                        rSave.Update

                    End If
                    
                End If
            
            ElseIf r.Fields("ParameterTypeName").Value = "System Color" Then
            
                If rSave.RecordCount > 0 Then
                                
                    If Trim(mlColorSelected) <> vbNullString Then
                                
                        rSave.Fields("ParameterValue").Value = CLng(Trim(mlColorSelected))
                        rSave.Update
                
                    Else
                
                        Menu.BsConnection.Execute "DELETE FROM BS_Parametri WHERE ID = " & rSave.Fields("ID").Value

                    End If
                
                Else
                
                    If Trim(mlColorSelected) <> vbNullString Then
                
                        rSave.AddNew
                        rSave.Fields("ParameterSystemID").Value = r.Fields("ID").Value
                        rSave.Fields("ParameterValue").Value = CLng(Trim(mlColorSelected))
                        rSave.Update
                
                    End If
                    
                End If
                
            ElseIf r.Fields("ParameterTypeName").Value = "System Print" Then
            
'                If rSave.RecordCount > 0 Then
'
'                    If Trim(msPrinterSelected) <> vbNullString Then
'
'                        rSave.Fields("ParameterValue").Value = Trim(msPrinterSelected)
'                        rSave.Update
'
'                    Else
'
'                        Menu.BsConnection.Execute "DELETE FROM BS_Parametri WHERE ID = " & rSave.Fields("ID").Value
'
'                    End If
'
'                Else
'
'                    If Trim(msPrinterSelected) <> vbNullString Then
'
'                        rSave.AddNew
'                        rSave.Fields("ParameterSystemID").Value = r.Fields("ID").Value
'                        rSave.Fields("ParameterValue").Value = Trim(msPrinterSelected)
'                        rSave.Update
'
'                    End If
'
'                End If
            
            End If
        
        ElseIf r.Fields("ParameterSelectTypeName").Value = "Multiple" Then
        
            SaveParameterMultipleValues lstParameters.ItemData(miPreviousSelectedValue)
            
        End If
        
        r.MoveNext
        
    Loop

    r.Close
    rSave.Close
    
    Set r = Nothing
    Set rSave = Nothing

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub ClearAllFields(bClearParametersList As Boolean)

    On Error GoTo EH

    If bClearParametersList = True Then

        lstParameters.Clear
    
    End If
    
    lswMultiSelect.Visible = False
    lswMultiSelect.ListItems.Clear
    
    txtString.Visible = False
    txtString.DataFormat.type = 0
    txtString.Text = vbNullString
    
    txtParameterDescription.Text = vbNullString
    
    optBoolean(0).Visible = False
    optBoolean(1).Visible = False
    optBoolean(0).Value = False
    optBoolean(1).Value = False
    
    cmdSystemFont.Visible = False
    cmdSystemFont.Caption = vbNullString
     
    cmdSystemColor.Visible = False
    cmdSystemColor.Caption = vbNullString
     
    cmdSystemPrint.Visible = False
    cmdSystemPrint.Caption = vbNullString
    
    msFontSelected = vbNullString
    mlColorSelected = 0
'    msPrinterSelected = vbNullString

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub


Private Sub SetParameterSystemMultipleValue(iParameterID As Integer)

    Dim r As ADODB.Recordset
    Dim rSave As ADODB.Recordset

    On Error GoTo EH

    Set r = m_fun.Open_Recordset_Sys("SELECT [Parameters].ID, ParameterValues.ID AS ParameterValueID, " & _
        "ParameterValues.ParameterValueName " & _
        "From [Parameters], ParameterTypes, ParameterSelectTypes, " & _
        "ParameterParameterValues, " & _
        "ParameterValues WHERE " & _
        "ParameterSelectTypes.ID = [Parameters].ParameterSelectTypeID AND " & _
        "ParameterParameterValues.ParameterID = [Parameters].ID AND " & _
        "ParameterParameterValues.ParameterValueID = ParameterValues.ID AND " & _
        "ParameterTypes.ID = [Parameters].ParameterTypeID AND [Parameters].ID = " & iParameterID & " ORDER BY ParameterValues.ParameterValueName ")

    Set rSave = m_fun.Open_Recordset("SELECT BS_Parametri.ID, BS_Parametri.ParameterSystemID, BS_Parametri.ParameterSystemValueID, BS_Parametri.ParameterValue FROM BS_Parametri WHERE ParameterSystemID = " & iParameterID & " ORDER BY BS_Parametri.ParameterValue ")

    lswMultiSelect.Top = txtParameterDescription.Top + txtParameterDescription.Height + 50

    lswMultiSelect.Left = txtParameterDescription.Left

    lswMultiSelect.Visible = True
    
    Do While Not r.EOF

        lswMultiSelect.ListItems.Add , , r.Fields("ParameterValueName").Value
        
        lswMultiSelect.ListItems(lswMultiSelect.ListItems.Count).Tag = r.Fields("ParameterValueID").Value
        
        If rSave.RecordCount > 0 Then
        
            rSave.MoveFirst
            
            Do While Not rSave.EOF
            
                If r.Fields("ID").Value = rSave.Fields("ParameterSystemID").Value And _
                    r.Fields("ParameterValueID").Value = rSave.Fields("ParameterSystemValueID").Value Then
            
                    lswMultiSelect.ListItems(lswMultiSelect.ListItems.Count).Checked = True
            
                End If
            
                rSave.MoveNext
            
            Loop
        
        End If
            
        r.MoveNext

    Loop

    r.Close
    rSave.Close
    
    Set r = Nothing
    Set rSave = Nothing

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Private Sub SaveParameterMultipleValues(iParameterID As Integer)

    Dim r As ADODB.Recordset
    Dim rSave As ADODB.Recordset
        
    Dim i As Integer
        
    On Error GoTo EH

    Set r = m_fun.Open_Recordset_Sys("SELECT [Parameters].ID, ParameterValues.ID AS ParameterValueID, " & _
        "ParameterValues.ParameterValueName " & _
        "From [Parameters], ParameterTypes, ParameterSelectTypes, ParameterParameterValues, " & _
        "ParameterValues WHERE " & _
        "ParameterSelectTypes.ID = [Parameters].ParameterSelectTypeID AND " & _
        "ParameterParameterValues.ParameterID = [Parameters].ID AND " & _
        "ParameterParameterValues.ParameterValueID = ParameterValues.ID AND " & _
        "ParameterTypes.ID = [Parameters].ParameterTypeID AND [Parameters].ID = " & iParameterID & " ORDER BY ParameterValues.ParameterValueName ")

    Set rSave = m_fun.Open_Recordset("SELECT BS_Parametri.ID, BS_Parametri.ParameterSystemID, " & _
        "BS_Parametri.ParameterSystemValueID, BS_Parametri.ParameterValue " & _
        "FROM BS_Parametri WHERE ParameterSystemID = " & iParameterID & " ORDER BY BS_Parametri.ParameterValue ")

    If r.RecordCount > 0 Then
        
        If rSave.RecordCount > 0 Then

            Menu.BsConnection.Execute "DELETE FROM BS_Parametri WHERE ParameterSystemID = " & iParameterID

        End If

        For i = 1 To lswMultiSelect.ListItems.Count

            If lswMultiSelect.ListItems(i).Checked Then

                rSave.AddNew
                rSave.Fields("ParameterSystemID").Value = r.Fields("ID").Value
                rSave.Fields("ParameterValue").Value = lswMultiSelect.ListItems(i).Text
                rSave.Fields("ParameterSystemValueID").Value = r.Fields("ParameterValueID").Value
                rSave.Update

            End If

            r.MoveNext
        
        Next i

    End If

    r.Close
    rSave.Close
    
    Set r = Nothing
    Set rSave = Nothing

    Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Sub

Public Function RetrieveParameterValues(sParameterName As String) As Variant

    Dim r As ADODB.Recordset
    Dim rSave As ADODB.Recordset
        
    Dim arrParameterValues() As String
    Dim i As Integer
    
    On Error GoTo EH

    Set r = m_fun.Open_Recordset_Sys("Select [Parameters].ID, [Parameters].ParameterName, " & _
        "[Parameters].ParameterLabel, [Parameters].ParameterDescription, " & _
        "[Parameters].ParameterTypeID, [Parameters].ParameterSelectTypeID, " & _
        "ParameterTypes.ParameterTypeName, ParameterSelectTypes.ParameterSelectTypeName " & _
        "From [Parameters], ParameterTypes, ParameterSelectTypes  " & _
        "WHERE " & _
        "ParameterSelectTypes.ID = [Parameters].ParameterSelectTypeID AND " & _
        "ParameterTypes.ID = [Parameters].ParameterTypeID AND [Parameters].ParameterName = '" & sParameterName & "' ORDER BY [Parameters].ParameterLabel ")

    If r.RecordCount > 0 Then
       
        Set rSave = m_fun.Open_Recordset("SELECT BS_Parametri.ID, BS_Parametri.ParameterSystemID, BS_Parametri.ParameterValue FROM BS_Parametri WHERE ParameterSystemID = " & r.Fields("ID").Value & "")

    End If

    i = 0

    ReDim arrParameterValues(rSave.RecordCount)

    Do While Not rSave.EOF

        arrParameterValues(i) = rSave.Fields("ParameterValue").Value

        i = i + 1

        rSave.MoveNext
    
    Loop
    
    r.Close
    rSave.Close
    
    Set r = Nothing
    Set rSave = Nothing

    RetrieveParameterValues = arrParameterValues

    Exit Function
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
    ERR.Clear

End Function
