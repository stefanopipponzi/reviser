Attribute VB_Name = "MabsdM_User"
Option Explicit

Global CountPGM As Long
Global CountDBD As Long

'Serve Per caricare il nome degli oggetti
Public Oggetti() As String

Type Cdi
  word As String
  Id As Long
End Type

Global Alfabeto(36) As String
Global PswCode(36) As Cdi

Public Const FattConst As Long = 2

Public Function Carica_Contatori_Obj(TipoPGM As String, TipoDBD As String) As Boolean
  Dim r As Recordset
  Dim MaxPercPGM As Long, MaxPercDbd As Long
  Dim Ex As Boolean
  Dim SlotFree As Boolean
  
  Carica_Contatori_Obj = True
  
  'Carica l'array totale degli oggetti user
  Menu.Carica_Array_Main Oggetti
  
  'Ripristina i valori degli oggetti che si possono caricare nel progetto
  Menu.UsFreeSlotDBD = Menu.UsFreeSlotDBDReset
  Menu.UsFreeSlotPGM = Menu.UsFreeSlotPGMReset
  
  'Controlla L'eccedenza dei sorgenti
  MaxPercPGM = (Menu.UsMaxPGM * Menu.UsPercent) / 100
  MaxPercDbd = (Menu.UsMaxDBD * Menu.UsPercent) / 100
  
  'Carica i Programmi
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where Tipo = '" & TipoPGM & "' Or Tipo ='" & TipoDBD & "'")
  If r.RecordCount Then
    CountPGM = r.RecordCount
    Menu.UsCurPGM = CountPGM
    Do Until r.EOF
      Screen.MousePointer = vbHourglass
      
      If Trim(r!Tipo) = "DBD" Then
         'Controlla Se il nome esiste nell'array
         Ex = Exist_obj_In_Array(r!Nome)
         
         If Ex = False Then
           'Controlla se pu� mettere il nuovo oggetto negli slot liberi
           SlotFree = Is_SlotFree("DBD")
                   
           If SlotFree = False Then
             Carica_Contatori_Obj = False
             Exit Function
           Else
             SlotFree = False
             Menu.UsFreeSlotDBD = Menu.UsFreeSlotDBD - 1
           End If
         End If
      ElseIf Trim(r!Tipo) = "CBL" Then
         'Controlla Se il nome esiste nell'array
         Ex = Exist_obj_In_Array(r!Nome)
         
         If Ex = False Then
           'Controlla se pu� mettere il nuovo oggetto negli slot liberi
           SlotFree = Is_SlotFree("PGM")
                   
           If SlotFree = False Then
             Carica_Contatori_Obj = False
             Exit Function
           Else
             SlotFree = False
             Menu.UsFreeSlotPGM = Menu.UsFreeSlotPGM - 1
           End If
         End If
      
         CountPGM = CountPGM + 1
      End If
      
      r.MoveNext
      Screen.MousePointer = vbDefault
    Loop
  Else
    Menu.UsCurPGM = 0
    Menu.UsCurDBD = 0
  End If
  r.Close
  
  'Valorizza i contatori
  Menu.UsCurPGM = CountPGM
  Menu.UsCurDBD = CountDBD
End Function

Public Sub Carica_Array_Alfabeto()
  Alfabeto(1) = "A"
  Alfabeto(2) = "B"
  Alfabeto(3) = "C"
  Alfabeto(4) = "D"
  Alfabeto(5) = "E"
  Alfabeto(6) = "F"
  Alfabeto(7) = "G"
  Alfabeto(8) = "H"
  Alfabeto(9) = "I"
  Alfabeto(10) = "L"
  Alfabeto(11) = "M"
  Alfabeto(12) = "N"
  Alfabeto(13) = "O"
  Alfabeto(14) = "P"
  Alfabeto(15) = "Q"
  Alfabeto(16) = "R"
  Alfabeto(17) = "S"
  Alfabeto(18) = "T"
  Alfabeto(19) = "U"
  Alfabeto(20) = "V"
  Alfabeto(21) = "Z"
  Alfabeto(22) = "X"
  Alfabeto(23) = "Y"
  Alfabeto(24) = "W"
  Alfabeto(25) = "J"
  Alfabeto(26) = "K"
  
  Alfabeto(27) = "0"
  Alfabeto(28) = "1"
  Alfabeto(29) = "2"
  Alfabeto(30) = "3"
  Alfabeto(31) = "4"
  Alfabeto(32) = "5"
  Alfabeto(33) = "6"
  Alfabeto(34) = "7"
  Alfabeto(35) = "8"
  Alfabeto(36) = "9"
End Sub

Public Function Restituisci_Index_Alfabeto(Lettera As String) As Long
  Dim i As Long
  
  For i = 1 To 36
    If Alfabeto(i) = Lettera Then
      Restituisci_Index_Alfabeto = i
      Exit Function
    End If
  Next i
End Function

Function HEX_2_DEC(NumeroHEX As String) As Variant
  Dim Num() As String
  Dim i As Long
  Dim Fattore As Long
  Dim Potenza As Long
  Dim Valore As Long
  
  ReDim Num(0)
  
  'riempie ogni elemento dell'array con un numero
  For i = 1 To Len(NumeroHEX)
    ReDim Preserve Num(UBound(Num) + 1)
    Num(UBound(Num)) = Mid(NumeroHEX, i, 1)
  Next i

  For i = 1 To UBound(Num)
    Select Case Num(i)
      Case "0"
        Fattore = 0
      Case "1"
        Fattore = 1
      Case "2"
        Fattore = 2
      Case "3"
        Fattore = 3
      Case "4"
        Fattore = 4
      Case "5"
        Fattore = 5
      Case "6"
        Fattore = 6
      Case "7"
        Fattore = 7
      Case "8"
        Fattore = 8
      Case "9"
        Fattore = 9
      Case "A"
        Fattore = 10
      Case "B"
        Fattore = 11
      Case "C"
        Fattore = 12
      Case "D"
        Fattore = 13
      Case "E"
        Fattore = 14
      Case "F"
        Fattore = 15
      Case Else
        'formato non valido
        HEX_2_DEC = "X"
        Exit Function
    End Select
    
    Potenza = 16 ^ (UBound(Num) - i)
    
    'somma i valori
    Valore = Valore + (Fattore * Potenza)
  Next i
  
  HEX_2_DEC = Valore
  
End Function

Public Function Bin(Numero) As String
  Dim Valore As Long, Num As String
  
  Num = ""
  Valore = Numero
  While Valore <> 1
    Num = (Valore Mod 2) & Num
    Valore = (Valore \ 2)
  Wend
  Num = "1" & Num
  Bin = Num
End Function

Public Function Decodifica_Carattere_UNO(Psw As String, CHAR As String) As String
  Dim Ascii As Long
  Dim i As Long
  Dim Id As Long
  Dim NewVal As Long
 
  NewVal = Asc(CHAR)
  For i = 1 To UBound(Alfabeto)
    If Trim(Alfabeto(i)) = UCase(Psw) Then
      Id = i
      Exit For
    End If
  Next i
  
  Ascii = (NewVal + Id) - FattConst
  If Ascii < 0 Then Ascii = 0
  Decodifica_Carattere_UNO = Chr(Ascii)
End Function

Public Function Decodifica_Carattere_DUE(Psw As String, CHAR As String) As String
  Dim Ascii As Long
  Dim i As Long
  Dim Id As Long
  Dim NewVal As Long
  Dim Valr As String
  
  Valr = Mid(CHAR, 2, 1) & Mid(CHAR, 1, 1)
  NewVal = Val(Valr)
  
  For i = 1 To UBound(Alfabeto)
    If Trim(Alfabeto(i)) = UCase(Psw) Then
      Id = i
      Exit For
    End If
  Next i
  
  Ascii = (NewVal + Id) - FattConst
  Decodifica_Carattere_DUE = Chr(Ascii)
End Function

Public Function Decodifica_Carattere_TRE(Psw As String, CHAR As String) As String
  Dim Ascii As Long
  Dim ValAr As Long
  Dim i As Long
  Dim Id As Long
  Dim NewVal As Long
  Dim Val As String
  Dim ValorV As Variant
  
  ValorV = HEX_2_DEC(CHAR)
  
  If Not IsNumeric(ValorV) Then
    Decodifica_Carattere_TRE = ""
    Exit Function
  End If
  
  NewVal = HEX_2_DEC(CHAR)
  
  For i = 1 To UBound(Alfabeto)
    If Trim(Alfabeto(i)) = UCase(Psw) Then
      Id = i
      Exit For
    End If
  Next i
  
  Ascii = (NewVal + Id) - FattConst
  
  If Ascii > 255 Then
    Decodifica_Carattere_TRE = ""
    Exit Function
  End If
  
  Val = Chr(Ascii)
  
  Decodifica_Carattere_TRE = Val
End Function

Public Function Decodifica_Carattere_QUATTRO(Psw As String, CHAR As String) As String
  Dim Ascii As Long
  Dim ValAr As Long
  Dim i As Long
  Dim Id As Long
  Dim NewVal As Long
  Dim Cry As String
  Dim Val1 As Long
  Dim Val2 As Long
  Dim Val3 As Long
  Dim Val4 As Long
  Dim Chk As String
  
  For i = 1 To UBound(Alfabeto)
    If Trim(Alfabeto(i)) = UCase(Psw) Then
      Id = i
      Exit For
    End If
  Next i
  
  Val1 = Val(Mid(CHAR, 1, 1))
  Val2 = Val(Mid(CHAR, 2, 1))
  
  Cry = (9 - Val1) & (9 - Val2)
  
  Val3 = 9 - Asc(Mid(CHAR, 3, 1))
  Val4 = 9 - Asc(Mid(CHAR, 4, 1))
  
  Chk = Val3 & Val4
  
  If Trim(Chk) = Trim(Cry) Then
    NewVal = (Val(Cry) + Id) - FattConst
    Cry = Chr(NewVal)
  Else
    'MsgBox "User Authorization Error...", vbCritical, "Error"
  End If
    
    
  Decodifica_Carattere_QUATTRO = Cry
End Function

Public Function Decodifica_Carattere_CINQUE(Psw As String, CHAR As String) As String
  Dim Ascii As Long
  Dim ValAr As Long
  Dim i As Long
  Dim Id As Long
  Dim NewVal3 As Long
  Dim NewVal4 As Long
  Dim Cry As String
  Dim Val1 As Long
  Dim Val2 As Long
  Dim Val3 As Long
  Dim Val4 As Long
  Dim NewStr As String
  Dim Chd As String
  Dim NewVal As Long
  
  For i = 1 To UBound(Alfabeto)
    If Trim(Alfabeto(i)) = UCase(Psw) Then
      Id = i
      Exit For
    End If
  Next i
  
  Val1 = Val(Mid(CHAR, 1, 1))
  Val2 = Val(Mid(CHAR, 3, 1))
  
  Cry = Val1 & Val2
  
  'Prende i checkdigit
  Val3 = Val(Mid(CHAR, 2, 1))
  Val4 = Val(Mid(CHAR, 4, 1))
  
  NewVal3 = 9 - Val(Val3)
  NewVal4 = 9 - Val(Val4)
  
  Chd = Trim(Str(NewVal3)) & Trim(Str(NewVal4))
  
  If Trim(Chd) = Trim(Cry) Then
    NewVal = (Val(Cry) + Id) - FattConst
    Cry = Chr(NewVal)
  Else
    'MsgBox "User Authorization Error...", vbCritical, "Error"
  End If
  
  Decodifica_Carattere_CINQUE = Cry
End Function

Public Function DECODIFICA_Carattere_SEI(Psw As String, CHAR As String) As String
  Dim Ascii As Long
  Dim ValAr As Long
  Dim i As Long
  Dim Id As Long
  Dim NewVal As Long
  Dim Cry As String
  Dim Val1 As Long
  Dim Val2 As Long
  Dim Val3 As Long
  Dim Val4 As Long
  Dim Val5 As Long
  Dim Val6 As Long
  Dim NewStr As String
  Dim ValArStr As String
  Dim V1 As Long
  Dim V2 As Long
  Dim Chd As Variant
  
  For i = 1 To UBound(Alfabeto)
    If Trim(Alfabeto(i)) = UCase(Psw) Then
      Id = i
      Exit For
    End If
  Next i
  
  Val1 = Val(Mid(CHAR, 1, 1))
  Val6 = Val(Mid(CHAR, 6, 1))
  Val2 = Val(Mid(CHAR, 2, 1))
  Val5 = Val(Mid(CHAR, 5, 1))
  Val3 = Asc(Mid(CHAR, 3, 1))
  Val4 = Asc(Mid(CHAR, 4, 1))
  
  
  
  NewStr = Trim(Str(9 - Val1)) & Trim(Str(9 - Val6))
  
  NewVal = ((100 - Val(NewStr)) + Id) - FattConst
  
  Cry = Chr(NewVal)
  
  'Controlla i check digit
  NewStr = Trim(Str(9 - Val2)) & Trim(Str(9 - Val5))
  Chd = ((Val(NewStr)) + Id) - FattConst
  
  Chd = Chr(Chd)
  
  If Trim(Chd) = Trim(Cry) Then
    'Controlla il secondo check digit
    If Val3 = 100 - Val1 And Val4 = Val3 + 9 Then
      'OK
    Else
      'MsgBox "User Authorization Error...", vbCritical, "Error"
      
    End If
    
  Else
    'MsgBox "User Authorization Error...", vbCritical, "Error"
  End If
  
  DECODIFICA_Carattere_SEI = Cry
  
End Function

Public Function Decodifica_Carattere_SETTE(Psw As String, CHAR As String) As String
  Dim Ascii As Long
  Dim ValAr As Long
  Dim i As Long
  Dim Id As Long
  Dim NewVal As Long
  Dim Cry As String
  Dim Val1 As Long
  Dim Val2 As Long
  Dim Val3 As Long
  Dim Val4 As Long
  Dim Val5 As Long
  Dim Val6 As Long
  Dim NewStr As String
  Dim ValArStr As String
  
  NewStr = BIN_2_DEC(CHAR)
  
  For i = 1 To UBound(Alfabeto)
    If Trim(Alfabeto(i)) = UCase(Psw) Then
      Id = i
      Exit For
    End If
  Next i
  
  Ascii = Val(NewStr)
  
  Decodifica_Carattere_SETTE = Chr((Ascii + Id) - FattConst)
End Function

Public Sub Carica_Array_PSW_Code()
  PswCode(1).word = "A"
  PswCode(1).Id = 1
  PswCode(2).word = "B"
  PswCode(2).Id = 2
  PswCode(3).word = "C"
  PswCode(3).Id = 3
  PswCode(4).word = "D"
  PswCode(4).Id = 4
  PswCode(5).word = "E"
  PswCode(5).Id = 5
  PswCode(6).word = "F"
  PswCode(6).Id = 6
  PswCode(7).word = "G"
  PswCode(7).Id = 7
  PswCode(8).word = "H"
  PswCode(8).Id = 6
  PswCode(9).word = "I"
  PswCode(9).Id = 5
  PswCode(10).word = "L"
  PswCode(10).Id = 4
  PswCode(11).word = "M"
  PswCode(11).Id = 3
  PswCode(12).word = "N"
  PswCode(12).Id = 2
  PswCode(13).word = "O"
  PswCode(13).Id = 1
  PswCode(14).word = "P"
  PswCode(14).Id = 4
  PswCode(15).word = "Q"
  PswCode(15).Id = 3
  PswCode(16).word = "R"
  PswCode(16).Id = 2
  PswCode(17).word = "S"
  PswCode(17).Id = 1
  PswCode(18).word = "T"
  PswCode(18).Id = 7
  PswCode(19).word = "U"
  PswCode(19).Id = 1
  PswCode(20).word = "V"
  PswCode(20).Id = 5
  PswCode(21).word = "Z"
  PswCode(21).Id = 3
  PswCode(22).word = "X"
  PswCode(22).Id = 2
  PswCode(23).word = "Y"
  PswCode(23).Id = 5
  PswCode(24).word = "W"
  PswCode(24).Id = 4
  PswCode(25).word = "J"
  PswCode(25).Id = 7
  PswCode(26).word = "K"
  PswCode(26).Id = 5
  PswCode(27).word = "1"
  PswCode(27).Id = 5
  PswCode(28).word = "2"
  PswCode(28).Id = 1
  PswCode(29).word = "3"
  PswCode(29).Id = 3
  PswCode(30).word = "4"
  PswCode(30).Id = 5
  PswCode(31).word = "5"
  PswCode(31).Id = 7
  PswCode(32).word = "6"
  PswCode(32).Id = 1
  PswCode(33).word = "7"
  PswCode(33).Id = 2
  PswCode(34).word = "8"
  PswCode(34).Id = 4
  PswCode(35).word = "9"
  PswCode(35).Id = 6
  PswCode(36).word = "0"
  PswCode(36).Id = 3
End Sub


Public Function Codifica_PSW(Psw As String) As Long
  Dim HexD As Long
  Dim i As Long
  
  Psw = UCase(Psw)
  
  For i = 1 To UBound(PswCode)
    If Trim(UCase(Psw)) = PswCode(i).word Then
      HexD = PswCode(i).Id
      Exit For
    End If
  Next i
  
  Codifica_PSW = HexD
End Function


Function BIN_2_DEC(NumeroBIN As String) As Variant
  Dim Num() As String
  Dim i As Long
  Dim Fattore As Long
  Dim Potenza As Long
  Dim Valore As Long
  
  ReDim Num(0)
  Valore = 0
  
  'riempie ogni elemento dell'array con un numero
  For i = 1 To Len(NumeroBIN)
    ReDim Preserve Num(UBound(Num) + 1)
    Num(UBound(Num)) = Mid(NumeroBIN, i, 1)
  Next i

  For i = 1 To UBound(Num)
    Select Case Num(i)
      Case "0"
        Fattore = 0
      Case "1"
        Fattore = 1
      
    End Select
    
    Potenza = 2 ^ (UBound(Num) - i)
   
    'somma i valori
    Valore = Valore + (Fattore * Potenza)
  Next i
  
  BIN_2_DEC = Valore
  
End Function

Public Function Exist_obj_In_Array(Nome As String) As Boolean
  Dim i As Long
  
  For i = 1 To UBound(Oggetti)
    If Trim(UCase(Nome)) = Trim(UCase(Oggetti(i))) Then
      Exist_obj_In_Array = True
      Exit Function
    End If
  Next i
  
  Exist_obj_In_Array = False
End Function

Public Function Is_SlotFree(Tipo As String) As Boolean
  'Controlla quanti slot libero (dalla percentuale ho a disposizione)
  Select Case UCase(Tipo)
    Case "PGM"
      If Menu.UsFreeSlotPGM <> 0 Then
        Is_SlotFree = True
      Else
        Is_SlotFree = False
      End If
      
    Case "DBD"
      If Menu.UsFreeSlotDBD <> 0 Then
        Is_SlotFree = True
      Else
        Is_SlotFree = False
      End If
  End Select
  
End Function

