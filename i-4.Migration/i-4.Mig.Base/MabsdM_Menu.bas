Attribute VB_Name = "MabsdM_Menu"
Option Explicit

Global BsMenu() As MaM1
Global Menu As MabsdC_Functions
Global m_fun As New MaFndC_Funzioni
Global m_dllParser As New MapsdC_Menu
Global SwMenu As Boolean

'SQ - erano nel modulo API
Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Public Declare Function SetFocusWnd Lib "user32.dll" Alias "SetFocus" (ByVal hwnd As Long) As Long
'API per file INI
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

Public Function Carica_Menu() As Boolean
  SwMenu = True
  Carica_Menu = True
  
  'crea il menu in memoria
  ReDim BsMenu(0)
  
  'Primo Bottone del menu a sinistra : Progetto
  ReDim Preserve BsMenu(UBound(BsMenu) + 1)
  BsMenu(UBound(BsMenu)).Id = 1
  BsMenu(UBound(BsMenu)).Label = "Home"
  BsMenu(UBound(BsMenu)).ToolTipText = ""
  BsMenu(UBound(BsMenu)).Picture = ""
  BsMenu(UBound(BsMenu)).M1Name = ""
  BsMenu(UBound(BsMenu)).M1SubName = ""
  BsMenu(UBound(BsMenu)).M1Level = 0
  BsMenu(UBound(BsMenu)).M2Name = "<Root>"
  BsMenu(UBound(BsMenu)).M3Name = ""
  BsMenu(UBound(BsMenu)).M4Name = ""
  BsMenu(UBound(BsMenu)).Funzione = ""
  BsMenu(UBound(BsMenu)).DllName = "MabsdP_00"
  BsMenu(UBound(BsMenu)).TipoFinIm = ""
  '1) Sottobottone : Project --> Info Project
  ReDim Preserve BsMenu(UBound(BsMenu) + 1)
  BsMenu(UBound(BsMenu)).Id = 2
  BsMenu(UBound(BsMenu)).Label = "Project Inventory"
  BsMenu(UBound(BsMenu)).ToolTipText = ""
  BsMenu(UBound(BsMenu)).Picture = Menu.BsImgDir & "\Info_Disabled.ico"
  BsMenu(UBound(BsMenu)).PictureEn = Menu.BsImgDir & "\Info_Enabled.ico"
  BsMenu(UBound(BsMenu)).M1Name = ""
  BsMenu(UBound(BsMenu)).M1SubName = ""
  BsMenu(UBound(BsMenu)).M1Level = 0
  BsMenu(UBound(BsMenu)).M2Name = "Home"
  BsMenu(UBound(BsMenu)).M3Name = ""
  BsMenu(UBound(BsMenu)).M4Name = ""
  BsMenu(UBound(BsMenu)).Funzione = "Show_Project_Info"
  BsMenu(UBound(BsMenu)).DllName = "BASE" '"MabsdP_00"
  BsMenu(UBound(BsMenu)).TipoFinIm = "Immediata"
  '2) Sottobottone : Statistics
  ReDim Preserve BsMenu(UBound(BsMenu) + 1)
  BsMenu(UBound(BsMenu)).Id = 5
  BsMenu(UBound(BsMenu)).Label = "Report Generator"
  BsMenu(UBound(BsMenu)).ToolTipText = ""
  BsMenu(UBound(BsMenu)).Picture = Menu.BsImgDir & "\Statistics_Disabled.ico"
  BsMenu(UBound(BsMenu)).PictureEn = Menu.BsImgDir & "\Statistics_Enabled.ico"
  BsMenu(UBound(BsMenu)).M1Name = ""
  BsMenu(UBound(BsMenu)).M1SubName = ""
  BsMenu(UBound(BsMenu)).M1Level = 0
  BsMenu(UBound(BsMenu)).M2Name = "Home"
  BsMenu(UBound(BsMenu)).M3Name = ""
  BsMenu(UBound(BsMenu)).M4Name = ""
  BsMenu(UBound(BsMenu)).Funzione = "Show_Statistics"
  BsMenu(UBound(BsMenu)).DllName = "BASE" '"MabsdP_00"
  BsMenu(UBound(BsMenu)).TipoFinIm = ""
  '6) Sottobottone : ListErrors
  ReDim Preserve BsMenu(UBound(BsMenu) + 1)
  BsMenu(UBound(BsMenu)).Id = 6
  BsMenu(UBound(BsMenu)).Label = "Project Exceptions"
  BsMenu(UBound(BsMenu)).ToolTipText = ""
  BsMenu(UBound(BsMenu)).PictureEn = Menu.BsImgDir & "\VerifyTools_Enabled.ico"
  BsMenu(UBound(BsMenu)).Picture = Menu.BsImgDir & "\VerifyTools_Disabled.ico"
  BsMenu(UBound(BsMenu)).M1Name = ""
  BsMenu(UBound(BsMenu)).M1SubName = ""
  BsMenu(UBound(BsMenu)).M1Level = 0
  BsMenu(UBound(BsMenu)).M2Name = "Home"
  BsMenu(UBound(BsMenu)).M3Name = ""
  BsMenu(UBound(BsMenu)).M4Name = ""
  BsMenu(UBound(BsMenu)).Funzione = "Show_ListErr"
  BsMenu(UBound(BsMenu)).DllName = "BASE" '"MabsdP_00"
  BsMenu(UBound(BsMenu)).TipoFinIm = "Immediata"
End Function
