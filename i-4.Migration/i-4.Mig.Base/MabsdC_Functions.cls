VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MabsdC_Functions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Dim BsIndMnu As Integer
Dim BsIndImg As Integer

'Array per contenere il dizionario COBOL,ASM ECC...
Private BSDizCOBOL() As String
Private BSDizASM() As String
Private UsOgg() As String

'*****************************************************
Private m_BsErrCodice As String
Private m_BsErrMsg As String
Private m_BsFinestra As Object
Private m_BsObjList As Object
'SQ - 24-11-05: piu' di 32000 oggetti...
Private m_BsIdOggetto As Long
Private m_BsTop As Long
Private m_BsLeft As Long
Private m_BsWidth As Long
Private m_BsHeight As Long
Private m_BsParent As Long
Private m_BsDatabase As ADOX.Catalog
Private m_BsNomeDB As String
Private m_BsConnection As ADODB.Connection
Private m_BsSysConnection As ADODB.Connection
Private m_BsConnState As Long
Private m_BsPathDB As String
Private m_BsTipoMigrazione As String
Private m_BsPathDef As String
Private m_BsUnitDef As String
Private m_BsPathPrd As String
Private m_BsPathPrj As String
Private m_BsTipoDB As String
Private m_BsId As Integer
Private m_BsLabel As String
Private m_BsToolTiptext As String
Private m_BsPicture As String
Private m_BsPictureEn As String
Private m_BsM1Name As String
Private m_BsM1SubName As String
Private m_BsM1Level As Long
Private m_BsM2Name As String
Private m_BsM3Name As String
Private m_BsM3ButtonType As String
Private m_BsM3SubName As String
Private m_BsM4Name As String
Private m_BsM4ButtonType As String
Private m_BsM4SubName As String
Private m_BsFunzione As String
Private m_BsDllName As String
Private m_BsTipoFinIm As String
Private m_BsText_ColorRich As Boolean
Private m_BsPath_Document As String
Private m_BsFont As String
Private m_BsFontSize As Long
Private m_BsMFESvName As String
Private m_BsMFESvIP As String
Private m_BsMFESvPort As String
Private m_BsErase_File As Boolean
Private m_BsNomeProdotto As String
Private m_BsImgDir As String
Private m_UsIdOggetto As Integer
Private m_UsDatabase As ADOX.Catalog
Private m_UsNomeDB As String
Private m_UsConnection As ADODB.Connection
Private m_UsConnState As Long
Private m_UsPathDB As String
Private m_UsPathDef As String
Private m_UsUnitDef As String
Private m_UsTipoDB As String
Private m_UsStatusWork As Boolean
Private m_UsNomeProdotto As String
Private m_UsMaxPGM As Long
Private m_UsMaxDBD As Long
Private m_UsPercent As Long
Private m_UsFreeSlotPGM As Long
Private m_UsFreeSlotDBD As Long
Private m_UsFreeSlotPGMReset As Long
Private m_UsFreeSlotDBDReset As Long
Private m_UsCurDBD As Long
Private m_UsCurPGM As Long
Private m_UsCodice As String
Private m_BsProvider As e_Provider
Private m_BsAsterixForDelete As String
Private m_BsAsterixForWhere As String
Public DLLFunzioni As New MaFndC_Funzioni

Public Function AttivaFunzione(Funzione As String, cParam As Collection, formParent As Object) As Boolean
  Dim AppId As Long
  
  On Error GoTo ErrorHandler
  AttivaFunzione = True
  
  Select Case Trim(UCase(Funzione))
    Case "SHOW_STATISTICS", "SHOW_PRINT_OBJECT", "PRJ_PRINTS"
    
        SetParent MabsdF_Print.hwnd, formParent.hwnd
        MabsdF_Print.Show
        MabsdF_Print.Move 0, 0, formParent.Width, formParent.Height
      
      
    Case "PRJ_PARAMETERS", "PRJ_ENVIRONMENT"
    
        SetParent MabsdF_Parameters.hwnd, formParent.hwnd
        MabsdF_Parameters.Show
        MabsdF_Parameters.Move 0, 0, formParent.Width, formParent.Height
      
    Case "PRJ_INFO", "SHOW_PROJECT_INFO"
      'MabsdF_InfoPrj.ShowMe formParent
        ''SQ move SetParent MabsdF_InfoPrj.hwnd, formParent.hwnd
        SetParent MabsdF_InfoPrj.hwnd, formParent.hwnd
        
        MabsdF_InfoPrj.Show 'vbModal 'SQ modale
        ''SQ move MabsdF_InfoPrj.Move 0, 0, formParent.Width, formParent.Height
    
     Case "SHOW_LISTERR"
     
        m_fun.Show_ListErrors formParent
        Set m_fun.formParent = formParent
                 
    Case "PRJ_SYSOBJ_DECLARE"
      'MabsdF_SysObj.Show vbModal
        ' Mauro 12/06/2007 : Nuova gestione di SysObj
        'SetParent MabsdF_SysObj.hwnd, formParent.hwnd
        'MabsdF_SysObj.Show
        'MabsdF_SysObj.Move 0, 0
        MabsdF_ListSysObj.Show vbModal
    Case Else
      AttivaFunzione = False
      
  End Select
  
  Exit Function
ErrorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "RC0002"
End Function

Public Function Move_To_Button(Direction As String) As Boolean
  'Direction =
  '
  '1)First    : Si muove al primo bottone dell'array
  '2)Next     : Si muove al successivo
  '3)Previous : Si muove al precedente
  '4)Last     : Si muove all'ultimo
  
  If Not SwMenu Then
    Move_To_Button = Carica_Menu
  End If
  
  'seleziona l'indice
  Select Case UCase(Trim(Direction))
    Case "FIRST"
      BsIndMnu = 1
      
    Case "NEXT"
      BsIndMnu = BsIndMnu + 1
    
    Case "PREVIOUS"
      BsIndMnu = BsIndMnu - 2
    
    Case "LAST"
      BsIndMnu = UBound(BsMenu)
  
  End Select
    
  If BsIndMnu > UBound(BsMenu) Then
    Move_To_Button = False
    Exit Function
  End If
  
  'assegna alle variabili public il contenuto del 1� elemento dell'array
  BsId = BsMenu(BsIndMnu).Id
  BsLabel = BsMenu(BsIndMnu).Label
  BsToolTiptext = BsMenu(BsIndMnu).ToolTipText
  BsPicture = BsMenu(BsIndMnu).Picture
  BsPictureEn = BsMenu(BsIndMnu).PictureEn
  BsM1Name = BsMenu(BsIndMnu).M1Name
  BsM1SubName = BsMenu(BsIndMnu).M1SubName
  BsM1Level = BsMenu(BsIndMnu).M1Level
  BsM2Name = BsMenu(BsIndMnu).M2Name
  BsM3Name = BsMenu(BsIndMnu).M3Name
  BsM3ButtonType = BsMenu(BsIndMnu).M3ButtonType
  BsM3SubName = BsMenu(BsIndMnu).M3SubName
  BsM4Name = BsMenu(BsIndMnu).M4Name
  BsM4ButtonType = BsMenu(BsIndMnu).M4ButtonType
  BsM4SubName = BsMenu(BsIndMnu).M4SubName
  BsFunzione = BsMenu(BsIndMnu).Funzione
  BsDllName = BsMenu(BsIndMnu).DllName
  BsTipoFinIm = BsMenu(BsIndMnu).TipoFinIm
  
  Move_To_Button = True
End Function

Public Sub InitDll(curDllFunzioni As MaFndC_Funzioni)
   Set m_fun = curDllFunzioni
   Set Menu = Me
   
   Carica_Array_Alfabeto
End Sub

Public Sub InitDllParser(curDllParser As MapsdC_Menu)
   Set m_dllParser = curDllParser
   Set Menu = Me
   
   Carica_Array_Alfabeto
End Sub

'Parte dedicata ai parametri
Public Sub Carica_Parametri_Prodotto()
  Leggi_Parametri_Da_INI Menu.BsPathPrd & FILE_INI
End Sub

Public Function ControlSource_Total(TipoPGM As String, TipoDBD As String) As Boolean
  ControlSource_Total = Carica_Contatori_Obj(TipoPGM, TipoDBD)
End Function

Public Function ControlSource_StepByStep_PGM(TipoPGM As String, NomeOgg As String) As Boolean
  Dim r As Recordset
  Dim NPgm As Long
  Dim MaxPercPGM As Long
  Dim i As Long
  Dim Ex As Boolean
  Dim SlotFree As Boolean
  
  For i = 1 To UBound(UsOgg)
    If Trim(UCase(UsOgg(i))) = Trim(UCase(NomeOgg)) Then
      ControlSource_StepByStep_PGM = True
      Exit Function
    Else
      Ex = False
    End If
  Next i
  
  'Controlla se ci sono degli slot liberi
  If Ex = False Then
    'Controlla se pu� mettere il nuovo oggetto negli slot liberi
    SlotFree = Is_SlotFree("PGM")
            
    If SlotFree = False Then
      ControlSource_StepByStep_PGM = False
      Exit Function
    Else
      SlotFree = False
      Menu.UsFreeSlotPGM = Menu.UsFreeSlotPGM - 1
      ControlSource_StepByStep_PGM = True
      Exit Function
    End If
  End If
End Function

Public Function ControlSource_StepByStep_DBD(TipoDBD As String, NomeOgg As String) As Boolean
  Dim r As Recordset
  Dim NDbd As Long
  Dim MaxPercDbd As Long
  Dim i As Long
  Dim Ex As Boolean
  Dim SlotFree As Boolean
  
  For i = 1 To UBound(UsOgg)
    If UCase(UsOgg(i)) = UCase(NomeOgg) Then
      ControlSource_StepByStep_DBD = True
      Exit Function
    Else
      Ex = False
    End If
  Next i
  
  'Controlla se ci sono degli slot liberi
  If Ex = False Then
    'Controlla se pu� mettere il nuovo oggetto negli slot liberi
    SlotFree = Is_SlotFree("DBD")
            
    If SlotFree = False Then
      ControlSource_StepByStep_DBD = False
      Exit Function
    Else
      SlotFree = False
      Menu.UsFreeSlotDBD = Menu.UsFreeSlotDBD - 1
      ControlSource_StepByStep_DBD = True
      Exit Function
    End If
  End If
End Function

Public Function Conta_record_Obj_In_Base_Al_Tipo(Tipo As String) As Long
  Dim r As Recordset
  
  If Trim(UCase(Tipo)) = "ALL" Then
    Set r = m_fun.Open_Recordset("Select COUNT(IdOggetto) From BS_Oggetti")
  Else
    Set r = m_fun.Open_Recordset("Select COUNT(IdOggetto) From BS_Oggetti Where Tipo = '" & Tipo & "'")
  End If
  
  If r.RecordCount Then
    Conta_record_Obj_In_Base_Al_Tipo = r.Fields(0).Value
  End If
  r.Close
End Function

Public Function Decodifica_File_Criptato(Percorso As String, Psw As String) As Boolean
  Dim Cont As Long
  Dim ChPsw As String
  Dim LoadCh As Long
  Dim Area As String
  Dim ChDec As String
  Dim AggPos As Long
  Dim LenFile As Long
  
  Dim Codice As String
  Dim MaxPGM As String
  Dim MaxDBD As String
  Dim Percent As String
  
  Dim OggettiCBL() As String
  Dim OggettiDBD() As String
  
  Dim i As Long
  Dim Objecto As String
  Dim NumByte As Long
  
  ReDim OggettiCBL(0)
  ReDim OggettiDBD(0)
  
  Carica_Array_PSW_Code
  Carica_Array_Alfabeto
  
  On Error GoTo ERR
  
  ReDim UsOgg(0)
  
  LenFile = FileLen(Percorso)
  
  NumByte = 1
  Cont = 1
  
  Open Percorso For Binary As #1
  
  AggPos = 1
  
  Do While Loc(1) < LOF(1)
    
    Screen.MousePointer = vbHourglass
    
    If Cont > Len(Psw) Then Cont = 1
    
    ChPsw = Mid(Psw, Cont, 1)
    
    'Restituisce il numero di caratteri da leggere
    LoadCh = Codifica_PSW(UCase(ChPsw))
    
    Area = String(LoadCh, " ")
    
    Get #1, AggPos, Area
    
    Select Case LoadCh
      Case 1
        ChDec = Decodifica_Carattere_UNO(ChPsw, Area)
      Case 2
        ChDec = Decodifica_Carattere_DUE(ChPsw, Area)
      Case 3
        ChDec = Decodifica_Carattere_TRE(ChPsw, Area)
      Case 4
        ChDec = Decodifica_Carattere_QUATTRO(ChPsw, Area)
      Case 5
        ChDec = Decodifica_Carattere_CINQUE(ChPsw, Area)
      Case 6
        ChDec = DECODIFICA_Carattere_SEI(ChPsw, Area)
      Case 7
        ChDec = Decodifica_Carattere_SETTE(ChPsw, Area)
    End Select
    
    Cont = Cont + 1
    Screen.MousePointer = vbDefault
    
    AggPos = AggPos + LoadCh
    
    If Len(Codice) < 14 Then
      'Scrive il codice
      Codice = Codice & ChDec
    End If
    
    If Len(MaxPGM) < 4 And NumByte > 14 And NumByte < 19 Then
      'Scrive il codice
      MaxPGM = MaxPGM & ChDec
    End If
    
    If Len(MaxDBD) < 4 And NumByte >= 19 And NumByte < 23 Then
      'Scrive il codice
      MaxDBD = MaxDBD & ChDec
    End If
    
    If Len(Percent) < 2 And NumByte >= 23 And NumByte < 25 Then
      'Scrive il codice
      Percent = Percent & ChDec
    End If
    
    If NumByte >= 25 Then
      'Comincia a leggere gli oggetti di tipo CBL e DBD
      If ChDec = "," Then
        
        Select Case UCase(Mid(Objecto, InStr(1, Objecto, ".") + 1))
          Case "CBL", "COB", "ORC", "CTL", "PGM"
            ReDim Preserve OggettiCBL(UBound(OggettiCBL) + 1)
            OggettiCBL(UBound(OggettiCBL)) = Objecto
            
          Case "DBD", "DB", "DAT", "STG"
            ReDim Preserve OggettiDBD(UBound(OggettiDBD) + 1)
            OggettiDBD(UBound(OggettiDBD)) = Objecto
            
        End Select
        
        Objecto = ""
      Else
        Objecto = Objecto & ChDec
      End If
      
    End If
    
    NumByte = NumByte + 1
  Loop
  
  Close #1
  Close #2
  
  'Carica l'array totale degli oggetti
  For i = 1 To UBound(OggettiCBL)
    ReDim Preserve UsOgg(UBound(UsOgg) + 1)
    UsOgg(UBound(UsOgg)) = OggettiCBL(i)
  Next i

  For i = 1 To UBound(OggettiDBD)
    ReDim Preserve UsOgg(UBound(UsOgg) + 1)
    UsOgg(UBound(UsOgg)) = OggettiDBD(i)
  Next i
  
  'Assegna i valori alle variabili della DLL
  Menu.UsCodice = Codice
  Menu.UsMaxDBD = Val(MaxDBD) + ((Val(MaxDBD) * Val(Percent)) / 100)
  Menu.UsMaxPGM = Val(MaxPGM) + ((Val(MaxPGM) * Val(Percent)) / 100)
  Menu.UsPercent = Val(Percent)
  Menu.UsFreeSlotPGM = Menu.UsMaxPGM
  Menu.UsFreeSlotDBD = Menu.UsMaxDBD
  
  'Copia i valori iniziali per un futuro reset
  Menu.UsFreeSlotPGMReset = Menu.UsMaxPGM
  Menu.UsFreeSlotDBDReset = Menu.UsMaxDBD
  
  Decodifica_File_Criptato = True
  
Exit Function

ERR:
  Decodifica_File_Criptato = False
  Close #1
  Close #2
End Function

Public Sub Carica_Array_Main(Arr() As String)
  Dim i As Long
  
  ReDim Arr(0)
  
  For i = 1 To UBound(UsOgg)
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)) = UsOgg(i)
  Next i
End Sub

Public Sub Aggiorna_Contatori_Dopo_Cancel(Nome As String, Tipo As String, CurPGM As Long, CurDBD As Long)
  Dim i As Long
  Dim Ex As Boolean
  
  CurPGM = Menu.UsCurPGM
  CurDBD = Menu.UsCurDBD
End Sub

Public Sub Carica_Array_Dizionario_COBOL(Ar() As String, Percorso As String)
  Dim i As Long
  Dim Parametro As String
  Dim Colore As Variant
  Dim Linea As String
  
  ReDim BSDizCOBOL(0)
  Stop
  'Apre il file
  Open Percorso For Input As #1
  
  'Legge Linea
   Do While Not EOF(1)
     Line Input #1, Linea
     Parametro = Isola_Parametro(Linea, "#")
     
     ReDim Preserve BSDizCOBOL(UBound(BSDizCOBOL) + 1)
     BSDizCOBOL(UBound(BSDizCOBOL)) = Parametro
   Loop
  
  'Chiude il file
  Close #1
  
  ReDim Ar(0)
  
  For i = 1 To UBound(BSDizCOBOL)
    ReDim Preserve Ar(UBound(Ar) + 1)
    Ar(UBound(Ar)) = BSDizCOBOL(i)
  Next i
End Sub

'**************************************************************************
'******** DICHIARAZIONI PROPERTY GET/LET/SET DLL **************************
'**************************************************************************
'BSERRCODICE
Public Property Let BsErrCodice(mVar As String)
  m_BsErrCodice = mVar
End Property

Public Property Get BsErrCodice() As String
  BsErrCodice = m_BsErrCodice
End Property

'BSERRMSG
Public Property Let BsErrMsg(mVar As String)
  m_BsErrMsg = mVar
End Property

Public Property Get BsErrMsg() As String
  BsErrMsg = m_BsErrMsg
End Property

'BSFINESTRA
Public Property Set BsFinestra(mVar As Object)
  Set m_BsFinestra = mVar
End Property

Public Property Get BsFinestra() As Object
  Set BsFinestra = m_BsFinestra
End Property

'BSOBJLIST
Public Property Set BsObjList(mVar As Object)
  Set m_BsObjList = mVar
End Property

Public Property Get BsObjList() As Object
  Set BsObjList = m_BsObjList
End Property
'SQ - 24-11-05: problema con piu' di 32000 oggetti...
'BSIDOGGETTO
Public Property Let BsIdOggetto(mVar As Long)
  m_BsIdOggetto = mVar
End Property
'SQ - 24-11-05: problema con piu' di 32000 oggetti...
Public Property Get BsIdOggetto() As Long
  BsIdOggetto = m_BsIdOggetto
End Property

'BSTOP
Public Property Let BsTop(mVar As Long)
  m_BsTop = mVar
End Property

Public Property Get BsTop() As Long
  BsTop = m_BsTop
End Property

'BSLEFT
Public Property Let BsLeft(mVar As Long)
   m_BsLeft = mVar
End Property

Public Property Get BsLeft() As Long
  BsLeft = m_BsLeft
End Property

'BSWIDTH
Public Property Let BsWidth(mVar As Long)
  m_BsWidth = mVar
End Property

Public Property Get BsWidth() As Long
  BsWidth = m_BsWidth
End Property

'BSHEIGHT
Public Property Let BsHeight(mVar As Long)
  m_BsHeight = mVar
End Property

Public Property Get BsHeight() As Long
  BsHeight = m_BsHeight
End Property

'BSPARENT
Public Property Let BsParent(mVar As Long)
  m_BsParent = mVar
End Property

Public Property Get BsParent() As Long
  BsParent = m_BsParent
End Property

'BSDATABASE
Public Property Set BsDatabase(mVar As ADOX.Catalog)
  Set m_BsDatabase = mVar
End Property

Public Property Get BsDatabase() As ADOX.Catalog
  Set BsDatabase = m_BsDatabase
End Property

'BSNOMEDB
Public Property Let BsNomeDB(mVar As String)
  m_BsNomeDB = mVar
End Property

Public Property Get BsNomeDB() As String
  BsNomeDB = m_BsNomeDB
End Property

'BSCONNECTION
Public Property Set BsConnection(mVar As ADODB.Connection)
  Set m_BsConnection = mVar
End Property

Public Property Get BsConnection() As ADODB.Connection
  Set BsConnection = m_BsConnection
End Property

'BSSYSCONNECTION
Public Property Set BsSysConnection(mVar As ADODB.Connection)
  Set m_BsSysConnection = mVar
End Property

Public Property Get BsSysConnection() As ADODB.Connection
  Set BsSysConnection = m_BsSysConnection
End Property

'BSCONNSTATE
Public Property Let BsConnState(mVar As Long)
  m_BsConnState = mVar
End Property

Public Property Get BsConnState() As Long
  BsConnState = m_BsConnState
End Property

'BSPATHDB
Public Property Let BsPathDB(mVar As String)
  m_BsPathDB = mVar
End Property

Public Property Get BsPathDB() As String
  BsPathDB = m_BsPathDB
End Property

'BSPATHPRJ
Public Property Let BsPathPrj(mVar As String)
  m_BsPathPrj = mVar
End Property

Public Property Get BsPathPrj() As String
  BsPathPrj = m_BsPathPrj
End Property

'BSTIPOMIGRAZIONE
Public Property Let BsTipoMigrazione(mVar As String)
  m_BsTipoMigrazione = mVar
End Property

Public Property Get BsTipoMigrazione() As String
  BsTipoMigrazione = m_BsTipoMigrazione
End Property

'BSPATHDEF
Public Property Let BsPathDef(mVar As String)
  m_BsPathDef = mVar
End Property

Public Property Get BsPathDef() As String
  BsPathDef = m_BsPathDef
End Property

'BSUNITDEF
Public Property Let BsUnitDef(mVar As String)
  m_BsUnitDef = mVar
End Property

Public Property Get BsUnitDef() As String
  BsUnitDef = m_BsUnitDef
End Property

'BSPATHPRD
Public Property Let BsPathPrd(mVar As String)
  m_BsPathPrd = mVar
End Property

Public Property Get BsPathPrd() As String
  BsPathPrd = m_BsPathPrd
End Property

'BSTIPODB
Public Property Let BsTipoDB(mVar As String)
  m_BsTipoDB = mVar
End Property

Public Property Get BsTipoDB() As String
  BsTipoDB = m_BsTipoDB
End Property

'BSID
Public Property Let BsId(mVar As Integer)
  m_BsId = mVar
End Property

Public Property Get BsId() As Integer
  BsId = m_BsId
End Property

'BSLABEL
Public Property Let BsLabel(mVar As String)
  m_BsLabel = mVar
End Property

Public Property Get BsLabel() As String
  BsLabel = m_BsLabel
End Property

'BSTOOLTIPTEXT
Public Property Let BsToolTiptext(mVar As String)
  m_BsToolTiptext = mVar
End Property

Public Property Get BsToolTiptext() As String
  BsToolTiptext = m_BsToolTiptext
End Property

'BSPICTURE
Public Property Let BsPicture(mVar As String)
  m_BsPicture = mVar
End Property

Public Property Get BsPicture() As String
  BsPicture = m_BsPicture
End Property

'BSPICTUREEN
Public Property Let BsPictureEn(mVar As String)
  m_BsPictureEn = mVar
End Property

Public Property Get BsPictureEn() As String
  BsPictureEn = m_BsPictureEn
End Property

'BSM1NAME
Public Property Let BsM1Name(mVar As String)
  m_BsM1Name = mVar
End Property

Public Property Get BsM1Name() As String
  BsM1Name = m_BsM1Name
End Property

'BSM1SUBNAME
Public Property Let BsM1SubName(mVar As String)
  m_BsM1SubName = mVar
End Property

Public Property Get BsM1SubName() As String
  BsM1SubName = m_BsM1SubName
End Property

'BSM1LEVEL
Public Property Let BsM1Level(mVar As Long)
  m_BsM1Level = mVar
End Property

Public Property Get BsM1Level() As Long
  BsM1Level = m_BsM1Level
End Property

'BSM2NAME
Public Property Let BsM2Name(mVar As String)
  m_BsM2Name = mVar
End Property

Public Property Get BsM2Name() As String
  BsM2Name = m_BsM2Name
End Property

'BSM3NAME
Public Property Let BsM3Name(mVar As String)
  m_BsM3Name = mVar
End Property

Public Property Get BsM3Name() As String
  BsM3Name = m_BsM3Name
End Property

'BSM3BUTTONTYPE
Public Property Let BsM3ButtonType(mVar As String)
  m_BsM3ButtonType = mVar
End Property

Public Property Get BsM3ButtonType() As String
  BsM3ButtonType = m_BsM3ButtonType
End Property

'BSM3SUBNAME
Public Property Let BsM3SubName(mVar As String)
  m_BsM3SubName = mVar
End Property

Public Property Get BsM3SubName() As String
  BsM3SubName = m_BsM3SubName
End Property

'BSM4NAME
Public Property Let BsM4Name(mVar As String)
  m_BsM4Name = mVar
End Property

Public Property Get BsM4Name() As String
  BsM4Name = m_BsM4Name
End Property

'BSM4BUTTONTYPE
Public Property Let BsM4ButtonType(mVar As String)
  m_BsM4ButtonType = mVar
End Property

Public Property Get BsM4ButtonType() As String
  BsM4ButtonType = m_BsM4ButtonType
End Property

'BSM4SUBNAME
Public Property Let BsM4SubName(mVar As String)
  m_BsM4SubName = mVar
End Property

Public Property Get BsM4SubName() As String
  BsM4SubName = m_BsM4SubName
End Property

'BSFUNZIONE
Public Property Let BsFunzione(mVar As String)
  m_BsFunzione = mVar
End Property

Public Property Get BsFunzione() As String
  BsFunzione = m_BsFunzione
End Property

'BSDLLNAME
Public Property Let BsDllName(mVar As String)
  m_BsDllName = mVar
End Property

Public Property Get BsDllName() As String
  BsDllName = m_BsDllName
End Property

'BSTIPOFINIM
Public Property Let BsTipoFinIm(mVar As String)
  m_BsTipoFinIm = mVar
End Property

Public Property Get BsTipoFinIm() As String
  BsTipoFinIm = m_BsTipoFinIm
End Property

'BSTEXT_COLORRICH
Public Property Let BsText_ColorRich(mVar As Boolean)
  m_BsText_ColorRich = mVar
End Property

Public Property Get BsText_ColorRich() As Boolean
  BsText_ColorRich = m_BsText_ColorRich
End Property

'BSFONT
Public Property Let BsFont(mVar As String)
  m_BsFont = mVar
End Property

Public Property Get BsFont() As String
  BsFont = m_BsFont
End Property

'BSFONTSIZE
Public Property Let BsFontSize(mVar As Long)
  m_BsFontSize = mVar
End Property

Public Property Get BsFontSize() As Long
  BsFontSize = m_BsFontSize
End Property

'BSMFESVNAME
Public Property Let BsMFESvName(mVar As String)
  m_BsMFESvName = mVar
End Property

Public Property Get BsMFESvName() As String
  BsMFESvName = m_BsMFESvName
End Property

'BSMFESVIP
Public Property Let BsMFESvIP(mVar As String)
  m_BsMFESvIP = mVar
End Property

Public Property Get BsMFESvIP() As String
  BsMFESvIP = m_BsMFESvIP
End Property

'BSMFESVPORT
Public Property Let BsMFESvPort(mVar As String)
  m_BsMFESvPort = mVar
End Property

Public Property Get BsMFESvPort() As String
  BsMFESvPort = m_BsMFESvPort
End Property

'BSERASE_FILE
Public Property Let BsErase_File(mVar As Boolean)
  m_BsErase_File = mVar
End Property

Public Property Get BsErase_File() As Boolean
  BsErase_File = m_BsErase_File
End Property

'BSNOMEPRODOTTO
Public Property Let BsNomeProdotto(mVar As String)
  m_BsNomeProdotto = mVar
End Property

Public Property Get BsNomeProdotto() As String
  BsNomeProdotto = m_BsNomeProdotto
End Property

'BSIMGDIR
Public Property Let BsImgDir(mVar As String)
  m_BsImgDir = mVar
End Property

Public Property Get BsImgDir() As String
  BsImgDir = m_BsImgDir
End Property

'USIDOGGETTO
Public Property Let UsIdOggetto(mVar As Integer)
  m_UsIdOggetto = mVar
End Property

Public Property Get UsIdOggetto() As Integer
  UsIdOggetto = m_UsIdOggetto
End Property

'USDATABASE
Public Property Set UsDatabase(mVar As ADOX.Catalog)
  Set m_UsDatabase = mVar
End Property

Public Property Get UsDatabase() As ADOX.Catalog
  Set UsDatabase = m_UsDatabase
End Property

'USNOMEDB
Public Property Let UsNomeDB(mVar As String)
  m_UsNomeDB = mVar
End Property

Public Property Get UsNomeDB() As String
  UsNomeDB = m_UsNomeDB
End Property

'USCONNECTION
Public Property Set UsConnection(mVar As ADODB.Connection)
  Set m_UsConnection = mVar
End Property

Public Property Get UsConnection() As ADODB.Connection
  Set UsConnection = m_UsConnection
End Property

'USCONNSTATE
Public Property Let UsConnState(mVar As Long)
  m_UsConnState = mVar
End Property

Public Property Get UsConnState() As Long
  UsConnState = m_UsConnState
End Property

'USPATHDB
Public Property Let UsPathDB(mVar As String)
  m_UsPathDB = mVar
End Property

Public Property Get UsPathDB() As String
  UsPathDB = m_UsPathDB
End Property

'USPATHDEF
Public Property Let UsPathDef(mVar As String)
  m_UsPathDef = mVar
End Property

Public Property Get UsPathDef() As String
  UsPathDef = m_UsPathDef
End Property

'USUNITDEF
Public Property Let UsUnitDef(mVar As String)
  m_UsUnitDef = mVar
End Property

Public Property Get UsUnitDef() As String
  UsUnitDef = m_UsUnitDef
End Property

'USTIPODB
Public Property Let UsTipoDB(mVar As String)
  m_UsTipoDB = mVar
End Property

Public Property Get UsTipoDB() As String
  UsTipoDB = m_UsTipoDB
End Property

'USSTATUSWORK
Public Property Let UsStatusWork(mVar As Boolean)
  m_UsStatusWork = mVar
End Property

Public Property Get UsStatusWork() As Boolean
  UsStatusWork = m_UsStatusWork
End Property

'USNOMEPRODOTTO
Public Property Let UsNomeProdotto(mVar As String)
  m_UsNomeProdotto = mVar
End Property

Public Property Get UsNomeProdotto() As String
  UsNomeProdotto = m_UsNomeProdotto
End Property

'USMAXPGM
Public Property Let UsMaxPGM(mVar As Long)
  m_UsMaxPGM = mVar
End Property

Public Property Get UsMaxPGM() As Long
  UsMaxPGM = m_UsMaxPGM
End Property

'USMAXDBD
Public Property Let UsMaxDBD(mVar As Long)
  m_UsMaxDBD = mVar
End Property

Public Property Get UsMaxDBD() As Long
  UsMaxDBD = m_UsMaxDBD
End Property

'USPERCENT
Public Property Let UsPercent(mVar As Long)
  m_UsPercent = mVar
End Property

Public Property Get UsPercent() As Long
  UsPercent = m_UsPercent
End Property

'USFREESLOTPGM
Public Property Let UsFreeSlotPGM(mVar As Long)
  m_UsFreeSlotPGM = mVar
End Property

Public Property Get UsFreeSlotPGM() As Long
  UsFreeSlotPGM = m_UsFreeSlotPGM
End Property

'USFREESLOTDBD
Public Property Let UsFreeSlotDBD(mVar As Long)
  m_UsFreeSlotDBD = mVar
End Property

Public Property Get UsFreeSlotDBD() As Long
  UsFreeSlotDBD = m_UsFreeSlotDBD
End Property

'USFREESLOTPGMRESET
Public Property Let UsFreeSlotPGMReset(mVar As Long)
  m_UsFreeSlotPGMReset = mVar
End Property

Public Property Get UsFreeSlotPGMReset() As Long
  UsFreeSlotPGMReset = m_UsFreeSlotPGMReset
End Property

'USFREESLOTDBDRESET
Public Property Let UsFreeSlotDBDReset(mVar As Long)
  m_UsFreeSlotDBDReset = mVar
End Property

Public Property Get UsFreeSlotDBDReset() As Long
  UsFreeSlotDBDReset = m_UsFreeSlotDBDReset
End Property

'USCURDBD
Public Property Let UsCurDBD(mVar As Long)
  m_UsCurDBD = mVar
End Property

Public Property Get UsCurDBD() As Long
  UsCurDBD = m_UsCurDBD
End Property

'USCURPGM
Public Property Let UsCurPGM(mVar As Long)
  m_UsCurPGM = mVar
End Property

Public Property Get UsCurPGM() As Long
  UsCurPGM = m_UsCurPGM
End Property

'USCODICE
Public Property Let UsCodice(mVar As String)
  m_UsCodice = mVar
End Property

Public Property Get UsCodice() As String
  UsCodice = m_UsCodice
End Property

'BSPATH_DOCUMENT
Public Property Let BsPath_Document(mVar As String)
  m_BsPath_Document = mVar
End Property

Public Property Get BsPath_Document() As String
  BsPath_Document = m_BsPath_Document
End Property

'BSPROVIDER
Public Property Let BsProvider(mVar As e_Provider)
  m_BsProvider = mVar
End Property

Public Property Get BsProvider() As e_Provider
  BsProvider = m_BsProvider
End Property

'M_BSASTERIXFORDELETE
Public Property Let BsAsterixForDelete(mVar As String)
  m_BsAsterixForDelete = mVar
End Property

Public Property Get BsAsterixForDelete() As String
  BsAsterixForDelete = m_BsAsterixForDelete
End Property

'M_BSASTERIXFORWHERE
Public Property Let BsAsterixForWhere(mVar As String)
  m_BsAsterixForWhere = mVar
End Property

Public Property Get BsAsterixForWhere() As String
  BsAsterixForWhere = m_BsAsterixForWhere
End Property
