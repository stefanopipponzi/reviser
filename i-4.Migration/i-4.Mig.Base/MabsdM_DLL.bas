Attribute VB_Name = "MabsdM_DLL"
Option Explicit

'SQ - erano nel modulo API
Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Public Declare Function SetFocusWnd Lib "user32.dll" Alias "SetFocus" (ByVal hwnd As Long) As Long
'A.P.I. per file INI
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

'serve a tenere in memoria tutte le parole riservate
Global WordRes() As String

Public Declare Function GetDesktopWindow Lib "user32" () As Long
Public Declare Function GetDeviceCaps Lib "gdi32" (ByVal hdc As Long, ByVal nIndex As Long) As Long
Public Declare Function GetDC Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function ReleaseDC Lib "user32" (ByVal hwnd As Long, ByVal hdc As Long) As Long
Public Const LOGPIXELSX = 88
Public Const LOGPIXELSY = 90

Public Function IsScreenFontSmall() As Boolean
    Dim hWndDesk As Long
    Dim hDCDesk As Long
    Dim logPix As Long
    Dim r As Long
    hWndDesk = GetDesktopWindow()
    hDCDesk = GetDC(hWndDesk)
    logPix = GetDeviceCaps(hDCDesk, LOGPIXELSX)
    r = ReleaseDC(hWndDesk, hDCDesk)
    If logPix = 96 Then IsScreenFontSmall = True
    Exit Function
End Function

Sub ResizeControls(FrmName As Form)
   'Chiama la funzione di resize centralizzata nella DLL Funzioni
  m_fun.ResizeControls FrmName
End Sub

Public Sub Carica_WordRes()
  ReDim WordRes(0)
  
   ReDim Preserve WordRes(UBound(WordRes) + 1)
   WordRes(UBound(WordRes)) = "COPY"
  
  'Da ampliare man mano con i vari linguaggi
End Sub

Public Function IsWordRes(Parola As String) As Boolean
  'controlla se la parola � una parola riservata
  Dim i As Long
  
  For i = 1 To UBound(WordRes)
    If WordRes(i) = UCase(Trim(Parola)) Then
      IsWordRes = True
      Exit Function
    End If
    Exit For
  Next i
  
  IsWordRes = False
End Function

Public Sub Cambia_Dimensione_Font_RTBox(FrmName As Form, RT As RichTextBox)
  Dim RatioX As Single
  Dim GetResolutionX As Single
  
  GetResolutionX = Menu.BsWidth / Screen.TwipsPerPixelX
  
  RatioX = GetResolutionX / (FrmName.Width / Screen.TwipsPerPixelX)
  
  RT.Font.Size = (RT.Font.Size - 1) * RatioX
End Sub

