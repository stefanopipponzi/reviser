VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MabsdF_InfoPrj 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Project Inventory - Summary"
   ClientHeight    =   9390
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   12345
   Icon            =   "MabsdF_InfoPrj.frx":0000
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   9390
   ScaleWidth      =   12345
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ListView lswInfo 
      Height          =   7935
      Left            =   120
      TabIndex        =   7
      Top             =   1320
      Width           =   12105
      _ExtentX        =   21352
      _ExtentY        =   13996
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   16777152
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Description"
         Object.Width           =   7056
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   1
         Text            =   "Number"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Text            =   "Loc"
         Object.Width           =   1764
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Project Info"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1155
      Left            =   90
      TabIndex        =   0
      Top             =   60
      Width           =   12135
      Begin VB.CommandButton cmdExcelFile 
         Caption         =   "Excel Report"
         Height          =   735
         Left            =   10080
         Picture         =   "MabsdF_InfoPrj.frx":4C4A
         Style           =   1  'Graphical
         TabIndex        =   8
         Tag             =   "fixed"
         ToolTipText     =   "Export Inventory Summary to Excel"
         Top             =   240
         Width           =   1455
      End
      Begin VB.TextBox txtDTCr 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   7290
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   420
         Width           =   1785
      End
      Begin VB.TextBox txtPath 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   3810
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   420
         Width           =   1785
      End
      Begin VB.TextBox txtName 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   810
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   420
         Width           =   1785
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Creation Date"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   6030
         TabIndex        =   6
         Top             =   510
         Width           =   975
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Path"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   3210
         TabIndex        =   3
         Top             =   510
         Width           =   330
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Name"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   150
         TabIndex        =   1
         Top             =   510
         Width           =   420
      End
   End
End
Attribute VB_Name = "MabsdF_InfoPrj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdExcelFile_Click()
  Dim ObjExc As Excel.Application
  Dim wsheet As Worksheet, wBook As Workbook
  Dim i As Integer, myItem As ListItem
    
  On Error GoTo ErrorHandler
  
  'Crea un nuovo foglio excel
  Set ObjExc = CreateObject("Excel.Application")
  Set wBook = Excel.Workbooks.Add
  Set wsheet = wBook.Worksheets(1)
   
  wBook.Worksheets(3).Delete
  wBook.Worksheets(2).Delete
   
  'SILVIA
  wsheet.Name = "Project Info"
     
  wsheet.Cells(1, 1) = "Description"
  wsheet.Cells(1, 2) = "Number"
  wsheet.Cells(1, 3) = "Loc"
 
  For i = 1 To lswInfo.ListItems.Count
    Set myItem = lswInfo.ListItems(i)
    wsheet.Cells(i + 1, 1) = myItem.Text
    wsheet.Cells(i + 1, 2) = CLng(myItem.ListSubItems(1))
    wsheet.Cells(i + 1, 3) = CLng(myItem.ListSubItems(2))
  Next i
     
  wBook.SaveAs Menu.BsPathPrj & "\Documents\Excel\ProjectInfo.xls"
   
  Set wsheet = Nothing
  Set wBook = Nothing
   
  ObjExc.Quit
  Set ObjExc = Nothing
   
  MsgBox "Report Complete!" & vbCrLf & Menu.BsPathPrj & "\Documents\Excel\ProjectInfo.xls", vbOKOnly + vbInformation, Menu.BsNomeProdotto
Exit Sub
ErrorHandler:
  If ERR.Number = 76 Then
    m_fun.MkDir_API Menu.BsPathPrj & "\Documents\Excel"

    Resume
  Else
    Set wsheet = Nothing
    ObjExc.Quit
    Set ObjExc = Nothing
    
    MsgBox ERR.Number & " - " & ERR.Description
    Screen.MousePointer = vbDefault
  End If
End Sub

Private Sub Form_Load()
  Dim i As Long
  Dim r As Recordset
  Dim nFile As Long
  Dim cmp01 As Long, cmp02 As Long
  
  Screen.MousePointer = vbHourglass
  
  txtName.Text = Menu.BsNomeDB
  txtPath.Text = Menu.BsPathDB
  
  'Data di creazione
  Set r = m_fun.Open_Recordset("Select * From BS_InfoPrj")
  If r.RecordCount Then
    txtDTCr.Text = r!Date_Creation
  End If
  r.Close
      
  'Inserisce le info del progetto dentro la lista
  lswInfo.ListItems.Clear
  
  'Carica le query che popolano la lista dal Db system
  Set r = m_fun.Open_Recordset_Sys("Select * From SysStatistics Order By Ordinale")
  If r.RecordCount Then
    While Not r.EOF
      If TN(r!nCampoCol01) = "" Then
        cmp01 = -1
      Else
        cmp01 = r!nCampoCol01
      End If
      
      If TN(r!nCampoCol02) = "" Then
        cmp02 = -1
      Else
        cmp02 = r!nCampoCol02
      End If
      
      Insert_Informazione_Query r!QuerySQL, cmp01, cmp02, r!Descrizione, r!Ordinale, TN(r!formatting)
      r.MoveNext
    Wend
    ''lswInfo_ItemClick lswInfo.ListItems(1)
  End If
  r.Close
  
  'Aggiunge la form alla collection
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
  
  
  lswInfo.ColumnHeaders(1).Width = (lswInfo.Width / 2) - 135
  lswInfo.ColumnHeaders(2).Width = (lswInfo.ColumnHeaders(1).Width / 2) - 73
  lswInfo.ColumnHeaders(3).Width = (lswInfo.ColumnHeaders(1).Width / 2) - 73
  
  ''''''''''''''''''''''''''''''''''''''''''''''
  'Scrive nei log il file di info del progetto
  ''''''''''''''''''''''''''''''''''''''''''''''
  nFile = FreeFile
  
  'Elimina il file
  If Dir_API(m_fun.FnPathPrj & "\documents\Log\Project.info") <> "" Then
    KillFile m_fun.FnPathPrj & "\documents\Log\Project.info"
  End If
  
  Open m_fun.FnPathPrj & "\documents\Log\Project.info" For Output As nFile
  
  Print #nFile, "+-----------------------------------------------------------------------+"
  Print #nFile, "|                                                                       |"
  Print #nFile, "|                       P R O J E C T   I N F O                         |"
  Print #nFile, "|                                                                       |"
  Print #nFile, "|                                                " & Trim(CStr(Now)) & "    |"
  Print #nFile, "|                                                                       |"
  Print #nFile, "+-----------------------------------------------------------------------+"
  Print #nFile, "| Description                                          N�       Locs    |"
  Print #nFile, "+-----------------------------------------------------------------------+"
  
  For i = 1 To lswInfo.ListItems.Count
    Print #nFile, vbTab & lswInfo.ListItems(i).Text & Space(46 - Len(lswInfo.ListItems(i).Text)) & Space(10 - Len(lswInfo.ListItems(i).ListSubItems(1).Text)) & lswInfo.ListItems(i).ListSubItems(1).Text & Space(14 - Len(lswInfo.ListItems(i).ListSubItems(2).Text)) & lswInfo.ListItems(i).ListSubItems(2).Text
  Next i
  
  Print #nFile, vbCrLf
  Print #nFile, "+-----------------------------------------------------------------------+" & vbCrLf
  
  Close nFile
  
  Screen.MousePointer = vbDefault
End Sub

Public Sub Insert_Informazione_Query(cSql As String, nCmp01 As Long, nCmp02 As Long, cDescr As String, Ordinale As Long, Optional formatting)
  Dim rs As Recordset
  Dim wStr01 As String, wStr02 As String
  
  'SQ Madrid - tmp
  On Error GoTo errTmp
  
  Set rs = m_fun.Open_Recordset(cSql)
  If rs.RecordCount Then
    wStr01 = TN(rs.Fields(nCmp01 - 1))
    
    If nCmp02 <> -1 Then
      If rs.Fields.Count > nCmp02 - 1 Then
        wStr02 = TN(rs.Fields(nCmp02 - 1))
      End If
    End If
    
    lswInfo.ListItems.Add , , cDescr
    If IsMissing(formatting) = True Then
      lswInfo.ListItems(lswInfo.ListItems.Count).ListSubItems.Add , , wStr01
      If nCmp02 <> -1 Then
        If Trim(wStr02) <> "" Then
          lswInfo.ListItems(lswInfo.ListItems.Count).ListSubItems.Add , , wStr02
        Else
          lswInfo.ListItems(lswInfo.ListItems.Count).ListSubItems.Add , , "0"
        End If
      End If
    Else
      lswInfo.ListItems(lswInfo.ListItems.Count).ListSubItems.Add , , Format(wStr01, formatting)
      If nCmp02 <> -1 Then
        If Trim(wStr02) <> "" Then
          lswInfo.ListItems(lswInfo.ListItems.Count).ListSubItems.Add , , Format(wStr02, formatting)
        Else
          lswInfo.ListItems(lswInfo.ListItems.Count).ListSubItems.Add , , "0"
        End If
      End If
    End If
  Else
    lswInfo.ListItems.Add , , cDescr
    lswInfo.ListItems(lswInfo.ListItems.Count).ListSubItems.Add , , "0"
    lswInfo.ListItems(lswInfo.ListItems.Count).ListSubItems.Add , , "0"
  End If
  rs.Close
  
  lswInfo.ListItems(lswInfo.ListItems.Count).Tag = "" & Ordinale
  
  Exit Sub
errTmp:
  'nuova tabella...
  Exit Sub
End Sub

Private Sub Form_Resize()
  ''SQ move ResizeForm Me
  'SQ
  lswInfo.ColumnHeaders(1).Width = ((lswInfo.Width - 370) / 5) * 3
  lswInfo.ColumnHeaders(2).Width = ((lswInfo.Width - 370) / 5) * 1
  lswInfo.ColumnHeaders(3).Width = ((lswInfo.Width - 370) / 5) * 1
End Sub

Private Sub Form_Unload(Cancel As Integer)
  'rimuove la form alla collection
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  
  SetFocusWnd m_fun.FnParent
End Sub



Private Sub lswInfo_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswInfo.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswInfo.SortOrder = Order
  lswInfo.SortKey = Key - 1
  lswInfo.Sorted = True
End Sub


