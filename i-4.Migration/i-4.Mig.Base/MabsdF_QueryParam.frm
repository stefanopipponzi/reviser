VERSION 5.00
Begin VB.Form MabsdF_QueryParam 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Insert Query Parameters..."
   ClientHeight    =   4725
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   6390
   Icon            =   "MabsdF_QueryParam.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   4725
   ScaleWidth      =   6390
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtSQL 
      BackColor       =   &H80000018&
      ForeColor       =   &H00C00000&
      Height          =   2145
      Left            =   60
      MultiLine       =   -1  'True
      TabIndex        =   4
      Top             =   1470
      Width           =   6255
   End
   Begin VB.TextBox txtParam 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   405
      Left            =   60
      TabIndex        =   3
      Top             =   960
      Width           =   6255
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   5400
      Picture         =   "MabsdF_QueryParam.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   3750
      Width           =   945
   End
   Begin VB.TextBox txtPrintName 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFC0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   390
      Left            =   60
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   60
      Width           =   6255
   End
   Begin VB.Label lblRequest 
      Caption         =   "Request"
      ForeColor       =   &H00C00000&
      Height          =   285
      Left            =   90
      TabIndex        =   2
      Top             =   630
      Width           =   6225
   End
End
Attribute VB_Name = "MabsdF_QueryParam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub SettingLabels(wPrintName As String, wRequest As String, wSQL As String)
  txtPrintName.Text = wPrintName
  lblRequest.Caption = wRequest
  txtSQL.Text = wSQL
End Sub

Private Sub cmdOK_Click()
  Me.Hide
End Sub

Public Property Get QueryParam() As String
  QueryParam = txtParam.Text
End Property

Private Sub Form_Activate()
  txtParam.SetFocus
End Sub

Private Sub txtParam_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = 13 Then
    Me.Hide
  End If
End Sub
