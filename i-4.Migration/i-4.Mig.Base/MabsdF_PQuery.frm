VERSION 5.00
Begin VB.Form MabsdF_PQuery 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Create/Modify query"
   ClientHeight    =   4080
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   9585
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   4080
   ScaleWidth      =   9585
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtReport 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1800
      MaxLength       =   31
      TabIndex        =   8
      Top             =   3510
      Width           =   5505
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   7500
      Picture         =   "MabsdF_PQuery.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Save/Modify Query..."
      Top             =   120
      Width           =   945
   End
   Begin VB.CommandButton CmdExit 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   8550
      Picture         =   "MabsdF_PQuery.frx":1D42
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Exit..."
      Top             =   120
      Width           =   945
   End
   Begin VB.TextBox txtSQL 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   2385
      Left            =   1800
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   1020
      Width           =   7665
   End
   Begin VB.TextBox txtDescription 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1800
      TabIndex        =   2
      Top             =   570
      Width           =   5505
   End
   Begin VB.TextBox txtQueryKey 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1800
      MaxLength       =   31
      TabIndex        =   0
      Top             =   120
      Width           =   5505
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Report Name"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   195
      Left            =   615
      TabIndex        =   9
      Top             =   3600
      Width           =   1125
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "SQL String"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   240
      Left            =   750
      TabIndex        =   5
      Top             =   1080
      Width           =   990
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Description"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   240
      Left            =   780
      TabIndex        =   3
      Top             =   660
      Width           =   960
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Query Name (KEY)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   240
      Left            =   90
      TabIndex        =   1
      Top             =   180
      Width           =   1665
   End
End
Attribute VB_Name = "MabsdF_PQuery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CmdExit_Click()
  Unload Me
End Sub

'SQ cambiato tutto
Private Sub cmdOK_Click()
  Dim rs As Recordset
  
  On Error GoTo ErrH
     
  ' Mauro 05/11/2007: Controllo caratteri riservati nella chiave
  ' Il controllo di lunghezza � gi� impostato da Textbox (31 caratteri)
  If controllaQuery(txtQueryKey) Then
    'SQ SEGRETO...
    If txtDescription = "SYSTEM-EXECUTE:" Then
      m_fun.FnConnDBSys.Execute txtSQL
      Exit Sub
    ElseIf txtDescription = "PRJ-EXECUTE:" Then
      m_fun.FnConnection.Execute txtSQL
      Exit Sub
    End If
    
    Set rs = m_fun.Open_Recordset("SELECT * FROM BS_SysQuery WHERE key = '" & txtQueryKey & "'")
    Select Case mTipoAdd
      Case "ADD"
        If rs.RecordCount = 0 Then
          rs.AddNew
          rs!Key = txtQueryKey
          rs!Descrizione = txtDescription
          rs!Testo = txtSQL
          rs!Report = txtReport
          rs.Update
        Else
          MsgBox "No query added. This key already exists.", vbInformation, "i-4.Migration"
        End If
      Case "EDIT"
        If rs.RecordCount = 0 Then
          rs.AddNew
        End If
        rs!Key = txtQueryKey
        rs!Descrizione = txtDescription
        rs!Testo = txtSQL
        rs!Report = txtReport
        rs.Update
    End Select
    rs.Close
     
    MabsdF_Print.lswStampe.SelectedItem = txtDescription.Text
    MabsdF_Print.lswStampe.SelectedItem.ListSubItems(1) = txtReport.Text
    Unload Me
  End If
Exit Sub
ErrH:
  MsgBox "Error saving query." & vbCrLf & ERR.Description, vbCritical, "i-4.Migration"
End Sub

Private Sub Form_Load()
  txtSQL.Text = Get_SQLQuery(mCurKey)
  Select Case mTipoAdd
    Case "ADD"
      txtQueryKey.Enabled = True
      txtQueryKey = "COPYOF_" & mCurKey
      txtDescription = mCurDescription & "[C]"
      txtReport = mCurReportName & "[C]"
    Case "EDIT"
      txtQueryKey.Enabled = False
      txtQueryKey = mCurKey
      txtDescription = mCurDescription
      txtReport = mCurReportName
  End Select
End Sub

Private Sub txtReport_KeyPress(KeyAscii As Integer)
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
