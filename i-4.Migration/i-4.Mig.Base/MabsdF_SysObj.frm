VERSION 5.00
Begin VB.Form MabsdF_SysObj 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " System Objects"
   ClientHeight    =   3300
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   9285
   Icon            =   "MabsdF_SysObj.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3300
   ScaleWidth      =   9285
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdInsertList 
      Caption         =   "..."
      Height          =   360
      Left            =   3480
      TabIndex        =   19
      ToolTipText     =   "Import objects list..."
      Top             =   533
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.ComboBox cboParamStr 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   315
      ItemData        =   "MabsdF_SysObj.frx":0442
      Left            =   1110
      List            =   "MabsdF_SysObj.frx":0444
      TabIndex        =   16
      Top             =   1920
      Width           =   5925
   End
   Begin VB.Frame Frame1 
      Caption         =   "Parameters criteria"
      ForeColor       =   &H00000080&
      Height          =   2235
      Left            =   7140
      TabIndex        =   10
      Top             =   60
      Width           =   2055
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Ext Proc : <PRC>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   270
         Left            =   270
         TabIndex        =   17
         Top             =   1890
         Width           =   1455
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Cur Proc : <ME>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   270
         Left            =   270
         TabIndex        =   15
         Top             =   1560
         Width           =   1395
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Ignore : <IGN>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   270
         Left            =   420
         TabIndex        =   14
         Top             =   1230
         Width           =   1335
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "DB Type : <DB>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   270
         Left            =   270
         TabIndex        =   13
         Top             =   900
         Width           =   1350
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Psb : <PSB>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   270
         Left            =   720
         TabIndex        =   12
         Top             =   570
         Width           =   1005
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Program : <PGM>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   270
         Left            =   300
         TabIndex        =   11
         Top             =   240
         Width           =   1470
      End
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   8340
      Picture         =   "MabsdF_SysObj.frx":0446
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2400
      Width           =   825
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Confirm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   7380
      Picture         =   "MabsdF_SysObj.frx":2170
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2400
      Width           =   825
   End
   Begin VB.TextBox txtUserDefinedType 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   360
      Left            =   5610
      MaxLength       =   3
      TabIndex        =   2
      Top             =   540
      Visible         =   0   'False
      Width           =   1425
   End
   Begin VB.TextBox txtDescription 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   825
      Left            =   1110
      MaxLength       =   250
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   1020
      Width           =   5925
   End
   Begin VB.TextBox txtName 
      BackColor       =   &H00FFFFC0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   360
      Left            =   1110
      MaxLength       =   20
      TabIndex        =   0
      Top             =   540
      Width           =   2325
   End
   Begin VB.ComboBox cboType 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   315
      ItemData        =   "MabsdF_SysObj.frx":22BA
      Left            =   1080
      List            =   "MabsdF_SysObj.frx":22BC
      TabIndex        =   1
      Top             =   120
      Width           =   2655
   End
   Begin VB.Label lblUserDefinedObject 
      AutoSize        =   -1  'True
      Caption         =   "User Object Type:"
      ForeColor       =   &H00C00000&
      Height          =   195
      Left            =   4320
      TabIndex        =   18
      Top             =   630
      Width           =   1290
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Parameters:"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   90
      TabIndex        =   9
      Top             =   1980
      Width           =   900
   End
   Begin VB.Label lblDescription 
      AutoSize        =   -1  'True
      Caption         =   "Description:"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   120
      TabIndex        =   8
      Top             =   1050
      Width           =   885
   End
   Begin VB.Label lblName 
      AutoSize        =   -1  'True
      Caption         =   "Name:"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   570
      TabIndex        =   7
      Top             =   600
      Width           =   480
   End
   Begin VB.Label lblType 
      AutoSize        =   -1  'True
      Caption         =   "Type:"
      ForeColor       =   &H00C00000&
      Height          =   225
      Index           =   0
      Left            =   600
      TabIndex        =   6
      Top             =   180
      Width           =   420
   End
End
Attribute VB_Name = "MabsdF_SysObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private arrTypes(0 To 1000) As String

' Read ini File
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Insert As Boolean
Public idSysObj As Long
'SQ Phoenix
Private ObjList() As String 'Gli altri attributi devono essere scritti nel form
Private Const OBJECT_LIST_TAG = "<OBJECT_LIST>"

Public Sub Start(update_insert As Boolean, Optional idObj As Long)
  Dim i As Long
  
  SetTypes
  load_ParamStr
  
  Insert = update_insert 'True = Insert
  idSysObj = idObj
    
  cboType.Text = cboType.List(0)
  txtName.Text = ""
  txtUserDefinedType.Text = ""
  txtDescription.Text = ""
  cboParamStr.Text = cboParamStr.List(0)
  If Not Insert Then  ' False = Update
    SetSysObjects idSysObj
  End If
    
  'SQ Madrid
  lblUserDefinedObject.Visible = False
  txtUserDefinedType.Visible = False
  
  'SQ Phoenix - gestione Lista oggetti da importare
  ReDim ObjList(0)
  
  Load Me
  Me.Show vbModal
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
  
  Load Me
  Me.Show vbModal
End Sub

'non funge!!!
Private Sub cboType_Change()
  txtUserDefinedType.Visible = cboType.Text = "USR"
  lblUserDefinedObject.Visible = txtUserDefinedType.Visible
End Sub

Private Sub cboType_Click()
  txtUserDefinedType.Visible = cboType.Text = "USR"
  lblUserDefinedObject.Visible = txtUserDefinedType.Visible
End Sub

Private Sub cboType_KeyDown(KeyCode As Integer, Shift As Integer)
  txtUserDefinedType.Visible = cboType.Text = "USR"
  lblUserDefinedObject.Visible = txtUserDefinedType.Visible
End Sub

Private Sub cboType_KeyPress(KeyAscii As Integer)
  Dim i As Integer
  Dim bFound As Boolean
    
  On Error GoTo EH
  
  bFound = False
  For i = 0 To cboType.ListCount - 1
    If UCase(Left(arrTypes(i), 1)) = UCase(Chr(KeyAscii)) And bFound = False Then
      cboType.ListIndex = i
      bFound = True
    End If
  Next i
  KeyAscii = 0

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub cboType_KeyUp(KeyCode As Integer, Shift As Integer)
  txtUserDefinedType.Visible = cboType.Text = "USR"
  lblUserDefinedObject.Visible = txtUserDefinedType.Visible
End Sub

Private Sub CmdExit_Click()
  Unload Me
End Sub

'''''''''''''''''''''''''''''''''''''''
' SQ Phoenix TMP
' Importare una lista di oggetti (txt o xls)
'''''''''''''''''''''''''''''''''''''''
Private Sub cmdInsertList_Click()
  Dim i As Long
  
  'TMP: dialog!
  Dim rs As Recordset
  Set rs = m_fun.Open_Recordset("Select distinct calledObj From tmp_deleteList")
  ReDim ObjList(rs.RecordCount - 1)
  For i = 0 To UBound(ObjList)
    ObjList(i) = rs!calledObj & ""
    rs.MoveNext
  Next i
  rs.Close
  
  txtName.Text = OBJECT_LIST_TAG  'ATTENZIONE: fa da flag!
  
End Sub
'''''''''''''''''''''''''''''''''''''''''''
' SQ Phoenix
' Gestione lista
'''''''''''''''''''''''''''''''''''''''''''
Private Sub cmdOK_Click()
  Dim i As Long
  
  Screen.MousePointer = vbHourglass
  If Trim(txtName.Text) = OBJECT_LIST_TAG Then
    For i = 0 To UBound(ObjList)
      addSystemObjects ObjList(i)
    Next i
  Else
    addSystemObjects UCase(Trim(txtName.Text)) 'SQ non � bello tagliare senza avviso!
  End If
  Screen.MousePointer = vbDefault
  
  Unload Me
  
End Sub
'SQ Phoenix - Import Lista
Private Sub addSystemObjects(objectName As String)
  Dim sCol As New Collection
  Dim sType As String, sDescription As String, sParamStr As String

  On Error GoTo EH
  
  If cboType.Text = "USR" Then
    sType = Trim(txtUserDefinedType.Text)
    If Len(sType) = 0 Then
      MsgBox "Please, insert a correct value for the new object type.", vbInformation, "System Objects - Update"
      txtUserDefinedType.SetFocus
      Exit Sub
    End If
  Else
    sType = Trim(cboType.Text)
  End If
  
  sDescription = Trim(txtDescription.Text)
  sParamStr = Trim(cboParamStr.Text)

  If Insert Then ' True = Insert
    'SQ non controlla se ce l'abbiamo gi�?!
    m_fun.FnConnection.Execute "INSERT INTO PsUte_ObjSys (Nome, Tipo, Descrizione,ParamStr) VALUES " & _
                               "('" & objectName & "'," & _
                               " '" & sType & "'," & _
                               " '" & sDescription & "'," & _
                               " '" & sParamStr & "')"
    'SQ - se � una lista, solo alla fine!
    'MsgBox "Item '" & objectName & "' correctly inserted.", vbOKOnly + vbInformation, "System Objects - Insert"
    'SQ nel file di log (separare errori da log)
    m_fun.WriteLog "!!!!SYSTEM OBJECTS - INSERT " & "Object name: " & objectName & " - Type: " & sType
  Else ' False = Update
    m_fun.FnConnection.Execute "UPDATE PsUte_ObjSys SET" & _
                               " Nome = '" & objectName & "'," & _
                               " Tipo = '" & sType & "'," & _
                               " Descrizione = '" & sDescription & "'," & _
                               " paramStr = '" & sParamStr & "'" & _
                               " WHERE IdOggettoSys = " & idSysObj & ""
    
    MsgBox "Item '" & objectName & "' correctly updated.", vbOKOnly + vbInformation, "System Objects - Update"
    'SQ nel file di log (separare errori da log)
    m_fun.WriteLog "!!!!SYSTEM OBJECTS - UPDATE " & "Object name: " & objectName & " - Type: " & sType
  End If
  
  'SQ 18-05-07
  'E' un "COMPONENTE!"...
  'm_dllParser.updateRelations objectName, sType
  m_dllParser.updateRelations objectName, sType, True
  
Exit Sub
EH:
  MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear '?
  Screen.MousePointer = vbDefault
End Sub

Private Sub load_ParamStr()
  Dim rs As Recordset
   
  ' Mauro 07/08/2007 : la prima opzione dev'essere a spazio
  cboParamStr.AddItem ""
  
  Set rs = m_fun.Open_Recordset_Sys("Select * From SysObjectParamStr Order by Id")
  While Not rs.EOF
    cboParamStr.AddItem rs!paramstr
    rs.MoveNext
  Wend
  rs.Close
End Sub

Private Sub SetTypes()
  On Error GoTo EH

  LoadTypeCombo
    
  If cboType.ListCount > 0 Then
    cboType.ListIndex = 0
  End If
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

Private Sub SetSysObjects(idObj As Long)
  Dim rsSysObj As Recordset
    
  On Error GoTo EH

  Set rsSysObj = m_fun.Open_Recordset("SELECT * FROM PsUte_ObjSys where idOggettoSys = " & idObj)
  If rsSysObj.RecordCount Then
    cboType.Text = rsSysObj!Tipo & ""
    txtName.Text = rsSysObj!Nome & ""
    'txtUserDefinedType = rssysobj!???
    txtDescription.Text = rsSysObj!Descrizione & ""
    cboParamStr.Text = rsSysObj!paramstr & ""
  End If
  rsSysObj.Close

  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub

'''''''''''''''''''''''''''''''''''''
' SQ: Portare tutto nel form di AVVIO!
' E' l� che leggiamo tutto il resto!
'''''''''''''''''''''''''''''''''''''
Private Sub LoadTypeCombo()
  Dim wRet As Long
  Dim wBuff As String
  Dim i As Integer
  Dim bEOFFound As Boolean

  On Error GoTo EH

  i = 0
  bEOFFound = False
  
  Do Until bEOFFound
    bEOFFound = True

    wBuff = String(100, Chr(10))
    wRet = GetPrivateProfileString("USR_SYS_OBJ", CStr(i), "", wBuff, 100, Menu.BsPathPrd & FILE_INI)
    If wRet <> 0 Then
      arrTypes(i) = Mid(wBuff, 1, wRet)
      cboType.AddItem arrTypes(i), i
      bEOFFound = False
    End If
    i = i + 1
  Loop
  
  Exit Sub
EH: MsgBox ERR.Description, vbOKOnly, ERR.Source
  ERR.Clear
End Sub
