VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MabsdF_ListSysObj 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "System Object List"
   ClientHeight    =   9015
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   13170
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9015
   ScaleWidth      =   13170
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete"
      Height          =   405
      Left            =   7620
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   8460
      Width           =   1845
   End
   Begin VB.CommandButton cmdInsert 
      Caption         =   "Insert "
      Height          =   405
      Left            =   5663
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   8490
      Width           =   1845
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "Update"
      Height          =   405
      Left            =   3720
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   8490
      Width           =   1845
   End
   Begin MSComctlLib.ListView lswTab 
      Height          =   8355
      Left            =   30
      TabIndex        =   3
      Top             =   60
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   14737
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   16777152
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
End
Attribute VB_Name = "MabsdF_ListSysObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public txtNum As Long

Private Sub cmdDelete_Click()
  On Error GoTo ErrorHandler
  Dim i As Long

  If MsgBox("Do you confirm the cancellation of the selected record?", vbOKCancel + vbExclamation, "i-4.Migration") = vbOK Then
    For i = 1 To lswTab.ListItems.Count
      If lswTab.ListItems(i).Selected Then
        m_fun.FnConnection.Execute "DELETE FROM PsUte_ObjSys WHERE IdOggettoSys = " & lswTab.ListItems(i)
      End If
    Next i
    displayTab True
  End If
  
  Exit Sub
ErrorHandler:
  MsgBox ERR.Number & " " & ERR.Description
End Sub

Private Sub cmdInsert_Click()
  MabsdF_SysObj.Start True
  displayTab True
End Sub

Private Sub cmdUpdate_Click()
  If lswTab.ListItems.Count = 0 Then
    MsgBox "List is empty...", vbInformation, "System Object"
    Exit Sub
  Else
    If lswTab.SelectedItem Is Nothing Then
      MsgBox "You must select a object in a list, before...", vbInformation, "System Object"
      Exit Sub
    Else
      MabsdF_SysObj.Start False, lswTab.SelectedItem
      displayTab True
    End If
  End If
End Sub

Private Sub Form_Load()
  displayTab False
End Sub

Public Sub displayTab(boolRefresh As Boolean)
  Dim rs As Recordset
  Dim listx As ListItem
  Dim i As Long
    
  Set rs = m_fun.Open_Recordset("Select * from PsUTE_ObjSys")
  If rs.RecordCount Then
    ' Carica le intestazioni della listView
    For i = 0 To rs.Fields.Count - 1
      If Not boolRefresh Then
        lswTab.ColumnHeaders.Add , , rs.Fields(i).Name
        lswTab.ColumnHeaders(i + 1).Width = lswTab.Width / rs.Fields.Count * 0.99
      End If
      lswTab.ListItems.Clear
      ReDim Preserve FieldNames(rs.Fields.Count - 1)
      FieldNames(i).Name = rs.Fields(i).Name
      FieldNames(i).type = rs.Fields(i).type
      FieldNames(i).len = rs.Fields(i).DefinedSize
      'SQ - run-time error se non ho il default impostato!
      'Proviamo almeno una volta le modifiche...
      If rs.RecordCount > 0 Then
        If Not IsNull(rs.Fields(i).OriginalValue) Then
          FieldNames(i).default = rs.Fields(i).OriginalValue
        End If
      End If
    Next i
    ' Carica i dati
    Do Until rs.EOF
      Set listx = lswTab.ListItems.Add(, , rs!idoggettosys)
      listx.SubItems(1) = rs!Nome & ""
      listx.SubItems(2) = rs!Tipo & ""
      listx.SubItems(3) = rs!Descrizione & ""
      listx.SubItems(4) = rs!paramstr & ""
      listx.SubItems(5) = rs!supported & ""
      rs.MoveNext
    Loop
  End If
  rs.Close
End Sub

Private Sub lswTab_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswTab.SortOrder
  
  Key = ColumnHeader.Index
  
  'assegna alla variabile globale l'index colonna selezionato
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswTab.SortOrder = Order
  lswTab.SortKey = Key - 1
  lswTab.Sorted = True
End Sub

Private Sub lswTab_DblClick()
  cmdUpdate_Click
End Sub
