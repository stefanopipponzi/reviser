VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MatsdF_RegExp 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Regular Expression"
   ClientHeight    =   8985
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   13125
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8985
   ScaleWidth      =   13125
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton CmdNoMacro 
      Caption         =   "Disable All Macros for selected type/s"
      Height          =   495
      Left            =   4080
      TabIndex        =   9
      Top             =   960
      Width           =   1935
   End
   Begin VB.CommandButton CmdAllMacro 
      Caption         =   "Enable All Macros for selected type/s"
      Height          =   495
      Left            =   4080
      TabIndex        =   8
      Top             =   240
      Width           =   1935
   End
   Begin VB.Frame Frame2 
      Height          =   7335
      Left            =   120
      TabIndex        =   3
      Top             =   1560
      Width           =   12855
      Begin VB.CommandButton cmdUpdate 
         Caption         =   "Edit"
         Height          =   405
         Left            =   3315
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   6480
         Width           =   1515
      End
      Begin VB.CommandButton cmdInsert 
         Caption         =   "Add"
         Height          =   405
         Left            =   5835
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   6480
         Width           =   1365
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Delete"
         Enabled         =   0   'False
         Height          =   405
         Left            =   8295
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   6480
         Width           =   1485
      End
      Begin MSComctlLib.ListView lswSysMigr 
         Height          =   6075
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   12585
         _ExtentX        =   22199
         _ExtentY        =   10716
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "OPER"
            Text            =   "Function"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Active"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "DESCR"
            Text            =   "Description"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Key             =   "WORD1"
            Text            =   "WORD1"
            Object.Width           =   8820
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Key             =   "Bol"
            Text            =   "Boolean"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Key             =   "WORD2"
            Text            =   "WORD2"
            Object.Width           =   8820
         EndProperty
      End
   End
   Begin VB.CommandButton CmdReload 
      Appearance      =   0  'Flat
      Caption         =   "Reload"
      Height          =   615
      Left            =   120
      Picture         =   "MatsdF_RegExp.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   2
      Tag             =   "fixed"
      ToolTipText     =   "Convert"
      Top             =   240
      Width           =   1035
   End
   Begin VB.Frame Frame1 
      Height          =   1335
      Left            =   1320
      TabIndex        =   0
      Top             =   120
      Width           =   2295
      Begin VB.ListBox LstFunction 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   960
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   1
         Top             =   240
         Width           =   2085
      End
   End
End
Attribute VB_Name = "MatsdF_RegExp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public IsMTP As Boolean
Public IsSQL As Boolean
Public IsJCL As Boolean
Public IsJOB As Boolean

Private Sub CmdAllMacro_Click()
Dim strMTP As String, strSQL As String, strJCL As String, strJOB As String
Dim wTipo As String
  IsMTP = LstFunction.Selected(0)
  IsSQL = LstFunction.Selected(1)
  IsJCL = LstFunction.Selected(2)
  IsJOB = LstFunction.Selected(3)
  strMTP = IIf(IsMTP, """MTP"" ", "")
  strSQL = IIf(IsSQL, """SQL"" ", "")
  strJCL = IIf(IsJCL, """JCL"" ", "")
  strJOB = IIf(IsJOB, """JOB""", "")
  
  wTipo = Trim(strMTP & strSQL & strJCL & strJOB)
  wTipo = Replace(wTipo, " ", ",")
  wTipo = "(" & wTipo & ")"
  If Not wTipo = "()" Then
    Menu.TsConnection.Execute "Update TL_Migr set Apply = true where type in " & wTipo
    LoadSysMigr
  Else
    MsgBox ("Select one or more types of Operations")
  End If
  
End Sub

Private Sub cmdDelete_Click()
  Dim i As Long
  Dim risp As Integer
  Dim rs As Recordset
  On Error GoTo ErrorHandler

  risp = MsgBox("Do you want to delete these records?", vbYesNo + vbInformation + vbDefaultButton2, Me.Caption)
  If risp = vbYes Then
  
    For i = lswSysMigr.ListItems.Count To 1 Step -1
      If lswSysMigr.ListItems(i).Selected Then
        Set rs = m_fun.Open_Recordset_Sys("select * from SysMigr where " & _
                                      "Operazione = '" & lswSysMigr.ListItems(i).text & "'" & _
                                      "and type = '" & lswSysMigr.ListItems(i).ListSubItems(5) & "'" & _
                                      " and typesql = '" & lswSysMigr.ListItems(i).ListSubItems(7) & "'")
        If rs.RecordCount Then
          rs.Delete
        End If
        rs.Close
        lswSysMigr.ListItems.Remove (i)
      End If
    Next i
  End If
  Exit Sub
ErrorHandler:
  MsgBox Err.Number & " " & Err.Description
End Sub

Private Sub cmdInsert_Click()
'  SetParent MatsdF_EditSysMigr.hwnd, MatsdF_RegExp.hwnd
  Dim f As MatsdF_EditSysMigr
  
  Set f = New MatsdF_EditSysMigr
  
  MatsdF_EditSysMigr.addInstr
End Sub

Private Sub CmdNoMacro_Click()
Dim strMTP As String, strSQL As String, strJCL As String, strJOB As String
Dim wTipo As String
  IsMTP = LstFunction.Selected(0)
  IsSQL = LstFunction.Selected(1)
  IsJCL = LstFunction.Selected(2)
  IsJOB = LstFunction.Selected(3)
  strMTP = IIf(IsMTP, """MTP"" ", "")
  strSQL = IIf(IsSQL, """SQL"" ", "")
  strJCL = IIf(IsJCL, """JCL"" ", "")
  strJOB = IIf(IsJOB, """JOB""", "")
  
  wTipo = Trim(strMTP & strSQL & strJCL & strJOB)
  wTipo = Replace(wTipo, " ", ",")
  wTipo = "(" & wTipo & ")"
  If Not wTipo = "()" Then
    Menu.TsConnection.Execute "Update TL_Migr set Apply = false where type in " & wTipo
    LoadSysMigr
  Else
    MsgBox ("Select one or more types of Operations")
  End If
End Sub

Private Sub CmdReload_Click()
  LoadSysMigr
End Sub

Private Sub cmdUpdate_Click()
  UpdateSys
End Sub

Private Sub UpdateSys()
  Dim i As Long
  
'  SetParent MatsdF_EditSysMigr.hwnd, MatsdF_RegExp.hwnd

  Dim f As MatsdF_EditSysMigr
  
  Set f = New MatsdF_EditSysMigr
  
  If lswSysMigr.ListItems.Count Then
    If lswSysMigr.SelectedItem Is Nothing Then
      MsgBox "You must select a object in a list, before...", vbInformation, "System Object"
      Exit Sub
    Else
      MatsdF_EditSysMigr.editInstr lswSysMigr.SelectedItem, lswSysMigr.SelectedItem.ListSubItems(6).text, CInt(lswSysMigr.SelectedItem.ListSubItems(7).text), lswSysMigr.SelectedItem.ListSubItems(8).text, lswSysMigr.SelectedItem.ListSubItems(9).text
    End If
  End If
End Sub

Private Sub Form_Load()
  LstFunction.AddItem "CICS to MTP"
  LstFunction.AddItem "DB2 to ORACLE"
  LstFunction.AddItem "JCL to MBM"
  LstFunction.AddItem "PLI to PLI-Liant"

  LstFunction.Selected(0) = True
  LstFunction.Selected(1) = True
  LstFunction.Selected(2) = True
  LstFunction.Selected(3) = True

  LoadSysMigr
  
'  m_fun.AddActiveWindows Me
'  m_fun.FnActiveWindowsBool = True
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Sub LoadSysMigr()
  Dim strMTP As String, strSQL As String, strJCL As String, strJOB As String
  Dim wTipo As String
  Dim tb As Recordset
  Dim wSql As String
  Dim wNomeMacro As String
  Dim k As Integer
  Dim wStr As String
  
  lswSysMigr.ListItems.Clear
  IsMTP = LstFunction.Selected(0)
  IsSQL = LstFunction.Selected(1)
  IsJCL = LstFunction.Selected(2)
  IsJOB = LstFunction.Selected(3)
  strMTP = IIf(IsMTP, """MTP"" ", "")
  strSQL = IIf(IsSQL, """SQL"" ", "")
  strJCL = IIf(IsJCL, """JCL"" ", "")
  strJOB = IIf(IsJOB, """JOB""", "")
  
  wTipo = Trim(strMTP & strSQL & strJCL & strJOB)
  wTipo = Replace(wTipo, " ", ",")
  wTipo = "(" & wTipo & ")"
  wSql = "select * from TL_Migr "
  If Not wTipo = "()" Then
    wSql = wSql & " where type in " & wTipo & " order by operazione "
  Else
    MsgBox ("Select one or more types of Operations")
    Exit Sub
  End If
  Set tb = m_fun.Open_Recordset(wSql)
  Do Until tb.EOF
    lswSysMigr.ListItems.Add , , "" & tb!operazione
    lswSysMigr.ListItems(k + 1).ListSubItems.Add , , IIf(tb!Apply, "YES", "NO")
    lswSysMigr.ListItems(k + 1).ListSubItems.Add , , Trim("" & tb!Description)
    lswSysMigr.ListItems(k + 1).ListSubItems.Add , , Trim("" & tb!word1)
    lswSysMigr.ListItems(k + 1).ListSubItems.Add , , Trim("" & tb!Boolean)
    lswSysMigr.ListItems(k + 1).ListSubItems.Add , , Trim("" & tb!word2)
    
    lswSysMigr.ListItems(k + 1).ListSubItems.Add , , tb!Type
    lswSysMigr.ListItems(k + 1).ListSubItems.Add , , IIf(IsNull(tb!Weight), "", tb!Weight) & ""
    lswSysMigr.ListItems(k + 1).ListSubItems.Add , , tb!Typesql
    
    'wNomeMacro = tb!template
    'wNomeMacro = Replace(wNomeMacro, "@EXECUTEMACRO(", "")
    'wNomeMacro = Trim(wNomeMacro)
    'wNomeMacro = Mid$(wNomeMacro, 1, Len(wNomeMacro) - 1)
    
    wNomeMacro = "" & Replace(Replace(tb!template, "@EXECUTEMACRO(", ""), ")", "")
            
    lswSysMigr.ListItems(k + 1).ListSubItems.Add , , wNomeMacro
    

    tb.MoveNext
    k = k + 1
  Loop
  tb.Close
  'cmdDelete.Enabled = True
End Sub

'Private Sub Form_Unload(Cancel As Integer)
'  m_fun.RemoveActiveWindows Me
'  m_fun.FnActiveWindowsBool = False
'End Sub

Private Sub lswSysMigr_DblClick()
  UpdateSys
End Sub
