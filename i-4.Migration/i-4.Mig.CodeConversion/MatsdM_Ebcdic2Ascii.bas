Attribute VB_Name = "MatsdM_Ebcdic2Ascii"
Option Explicit
Const StructEB2ASPath = "Data\EBCDICtoASCII\Structure"
Const RedFileEB2ASPath = "Data\EBCDICtoASCII\RedFile"
Const StructAS2EBPath = "Data\ASCIItoEBCDIC\Structure"
Const RedFileAS2EBPath = "Data\ASCIItoEBCDIC\RedFile"

'SILVIA 27-03-2008: gestione separatore di fields
Dim Separatore As String

Public Type t_Copy
  IdOggetto As Long
  Nome As String
  Area As String
  Position As Long
  PhisicalPath As String
  Tipo As String
End Type
Global indexArea As String
Public Type Rec_Cmp
  IdOggetto As Long
  idArea As Integer
  IdCmp As String
  Nome As String
  Ordinale As Integer
  Livello As Integer
  Tipo As String
  Posizione As Long
  Byte As Long
  Lunghezza As Long
  Decimali As Integer
  Segno As String
  Usage As String
  Picture As String
  Occurs As Integer
  NomeRed As String
  Sel_Field As String
  Operatore As String
  Selettore_Valore As String
  ' Mauro 21/03/2008: Mi servono x la cancellazione delle redefines inutili
  NumRedefines As Integer ' Contiene l'ordinale della redefines
  DeleteRed As String ' o BLANK, o "X"
End Type
Global Fields() As Rec_Cmp
' Mauro 21/03/2008 : Contengono le strutture (Base e le redefines) per i confronti
Global rigaStructBase As String
Global rigaStructRed() As String
Public Type tOccurs
  NumOccurs As Integer
  levOccurs As Integer
End Type
'variabile come la precedente, ma riguarda la lista totale degli oggetti
Global ListObjCol As Long
' Mauro 04/02/2011
Public isFilterConf As Boolean
Public isFilterExe As Boolean
'variabile per tenere traccia della colonna selezionata sulla lista di filtro selezione
Global Filter_List_Column  As Long

Public Sub Add_Structure()
  Dim tbArea As Recordset
  
  CopiaAreaMG
  CopiaCmpMG
    
  MatsdF_EBCDIC2ASCII.lswStructSel.ListItems.Clear
  Set tbArea = m_fun.Open_Recordset("select IdOggetto, IdArea, Nome, FileName from MgConvData_Area")
  Do Until tbArea.EOF
    With MatsdF_EBCDIC2ASCII.lswStructSel.ListItems.Add(, , tbArea!fileName)
      .SubItems(1) = tbArea!Nome
      .tag = tbArea!IdOggetto & "\" & tbArea!idArea
    End With
    tbArea.MoveNext
  Loop
  tbArea.Close
End Sub

Sub CopiaAreaMG()
  Dim tbAreaOrig As Recordset, tbAreaDest As Recordset
  Dim wSql As String

  wSql = "Delete from mgconvdata_area where " & _
         "idoggetto = " & MatsdF_AddArea.lswAreas.SelectedItem & " AND " & _
         "IdArea = " & MatsdF_AddArea.lswAreas.SelectedItem.ListSubItems(3)
  Menu.TsConnection.Execute wSql
  
  Set tbAreaOrig = m_fun.Open_Recordset("select * from psdata_area where " & _
                                        "idoggetto = " & MatsdF_AddArea.lswAreas.SelectedItem & " AND " & _
                                        "idarea = " & MatsdF_AddArea.lswAreas.SelectedItem.ListSubItems(3))
  Set tbAreaDest = m_fun.Open_Recordset("select * from mgconvdata_area where " & _
                                        "idoggetto = " & MatsdF_AddArea.lswAreas.SelectedItem & " AND " & _
                                        "idarea = " & MatsdF_AddArea.lswAreas.SelectedItem.ListSubItems(3))
  If Not tbAreaOrig.EOF Then
    If tbAreaDest.EOF Then
      tbAreaDest.AddNew
    End If
    tbAreaDest!IdOggetto = tbAreaOrig!IdOggetto
    tbAreaDest!idArea = tbAreaOrig!idArea
    tbAreaDest!Nome = tbAreaOrig!Nome
    tbAreaDest!Livello = tbAreaOrig!Livello
    tbAreaDest!NomeRed = tbAreaOrig!NomeRed
    tbAreaDest!fileName = MatsdF_AddArea.txtFileName.text
    tbAreaDest.Update
  End If
  tbAreaDest.Close
  tbAreaOrig.Close
End Sub

'''''''''''''''''''
' SILVIA 20-03-2008
' associazione file - aree da lista
'''''''''''
Sub AddStructure_from_list()
  Dim i As Long
  Dim Line As String, dialogFileName As String
  Dim isLabel As Boolean
  Dim cParam As Collection
  
  Dim riga() As String
  Dim objList() As String 'solo il nome � pericoloso: dovra avere anche la directory
  Dim rs As Recordset
  Dim IdOggetto As Long, idArea As Long
  Dim strIns As String
  Dim strCpynf As String
  Dim strAreanf As String
  Dim strlog As String
  Dim strlog1 As String
  Dim Componente As String
  Dim Count As Integer
  
  On Error GoTo ErrorHandler
  
  If m_fun.FnProcessRunning Then
    m_fun.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''
  'Gestione inserimento lista oggetti da file
  ''''''''''''''''''''''''''''''''''''''''''''''
 MatsdF_EBCDIC2ASCII.ComD.ShowOpen

  dialogFileName = MatsdF_EBCDIC2ASCII.ComD.fileName
  If Len(dialogFileName) > 0 Then
    objList = readFile(dialogFileName)
    Screen.MousePointer = vbHourglass
    m_fun.FnActiveWindowsBool = True
    m_fun.FnProcessRunning = True
    
    If MsgBox("Do you want to insert the list of " & UBound(objList) & " objects ", vbQuestion + vbYesNo, "Add Structures") = vbYes Then
      For i = 1 To UBound(objList)
        riga() = Split(objList(i), " ")
        If UBound(riga()) = 1 Or UBound(riga()) = 2 Then
          'count = count + 1
          m_fun.FnActiveWindowsBool = False
          
          Set rs = m_fun.Open_Recordset("SELECT * FROM Bs_Oggetti WHERE " & _
                                        "tipo IN ('CPY','CBL') AND Nome = '" & Trim(riga(1)) & "'")
          If rs.RecordCount Then
            IdOggetto = rs!IdOggetto
          Else
            IdOggetto = 0
            strCpynf = strCpynf & riga(1) & vbCrLf
          End If
          rs.Close
          
          If UBound(riga()) = 2 Then
            Set rs = m_fun.Open_Recordset("SELECT * FROM PsData_Area WHERE " & _
                                          "idOggetto = " & IdOggetto & " AND Nome = '" & Trim(riga(2)) & "'")
          Else
            Set rs = m_fun.Open_Recordset("SELECT * FROM PsData_Area WHERE " & _
                                          "idOggetto = " & IdOggetto & " ORDER BY idarea")
          End If
          If rs.RecordCount Then
            idArea = rs!idArea
          Else
            idArea = 0
            strAreanf = strAreanf & riga(1) & vbCrLf
          End If
          rs.Close
          
          If IdOggetto > 0 And idArea > 0 Then
            strlog1 = strlog1 & (CopiaAreaMG_new(IdOggetto, idArea, Trim(riga(0))))
          End If
        Else
          strlog = strlog & "WRONG LINE : " & objList(i) & vbCrLf
        End If
      Next i
      If strCpynf <> "" Then
        strCpynf = "---COPYBOOK NOT FOUND---" & vbCrLf & strCpynf
      End If
      If strAreanf <> "" Then
        strAreanf = "---AREA NOT FOUND FOR COPY ---" & vbCrLf & strAreanf
      End If
      If strIns <> "" Then
        strIns = "INSERT RELATION AREA - COPYBOOK: " & vbCrLf & strIns
      End If
      If strlog1 <> "" Then
        strlog1 = "Relation already exist from File - Area " & vbCrLf & strlog1
      End If
      
      ' Aggiunge un contatore per evitare fileName duplicati
      Update_fileName
      
      Carica_Lista_Structure

      Screen.MousePointer = vbDefault
      m_fun.Show_ShowText strCpynf & vbCrLf & _
                          strAreanf & vbCrLf & _
                          strIns & vbCrLf & _
                          strlog1 & vbCrLf & _
                          strlog, "Add Structure Log"
      m_fun.FnActiveWindowsBool = True
      'Screen.MousePointer = vbDefault
      
      m_fun.FnActiveWindowsBool = False
      m_fun.FnProcessRunning = False
      
      Screen.MousePointer = vbDefault
    End If
  End If
  Screen.MousePointer = vbDefault
  
  Exit Sub
ErrorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002", "Add Structures", "AddStructure_from_List", Err.Number, Err.Description, True
End Sub

Function CopiaAreaMG_new(IdOggetto As Long, idArea As Long, strFileName As String) As String
  Dim tbAreaOrig As Recordset, tbAreaDest As Recordset

  Set tbAreaOrig = m_fun.Open_Recordset("select * from psdata_area where " & _
                                        "idoggetto = " & IdOggetto & " AND " & _
                                        "idarea = " & idArea & " order by idarea")
  Set tbAreaDest = m_fun.Open_Recordset("select * from mgconvdata_area where " & _
                                        "idoggetto = " & IdOggetto & " AND " & _
                                        "idarea = " & tbAreaOrig!idArea & " AND trim(filename) = '" & strFileName & "'")
  If Not tbAreaOrig.EOF Then
    If tbAreaDest.EOF Then
      tbAreaDest.AddNew
      tbAreaDest!IdOggetto = tbAreaOrig!IdOggetto
      tbAreaDest!idArea = tbAreaOrig!idArea
      tbAreaDest!Nome = tbAreaOrig!Nome
      tbAreaDest!Livello = tbAreaOrig!Livello
      tbAreaDest!NomeRed = tbAreaOrig!NomeRed
      tbAreaDest!fileName = strFileName
      tbAreaDest.Update
      CopiaAreaMG_new = ""
      CopiaCmpMG_new IdOggetto, idArea
    Else
      CopiaAreaMG_new = strFileName & " - " & tbAreaOrig!Nome & vbCrLf
    End If
  End If
  tbAreaDest.Close
  tbAreaOrig.Close
End Function

Sub CopiaCmpMG_new(IdOggetto As Long, idArea As Long)
  Dim tbCmpOrig As Recordset, tbCmpDest As Recordset
  Dim wSql As String
  
  wSql = "Delete from mgconvdata_cmp where " & _
         "idoggetto = " & IdOggetto & " AND IdArea = " & idArea
  Menu.TsConnection.Execute wSql
  
  Set tbCmpOrig = m_fun.Open_Recordset("select * from psdata_cmp where " & _
                                       "idoggetto = " & IdOggetto & " AND " & _
                                       "idarea = " & idArea & " AND " & _
                                       "livello <> 88")
  Do Until tbCmpOrig.EOF
    Set tbCmpDest = m_fun.Open_Recordset("select * from mgconvdata_cmp where " & _
                                         "idcmp = '" & tbCmpOrig!Codice & "' AND " & _
                                         "idarea = " & tbCmpOrig!idArea & " AND " & _
                                         "idoggetto = " & tbCmpOrig!IdOggetto)
    If tbCmpDest.EOF Then
      tbCmpDest.AddNew
    End If
    tbCmpDest!IdOggetto = tbCmpOrig!IdOggetto
    tbCmpDest!idArea = tbCmpOrig!idArea
    tbCmpDest!IdCmp = tbCmpOrig!Codice
    tbCmpDest!Nome = tbCmpOrig!Nome
    tbCmpDest!Ordinale = tbCmpOrig!Ordinale
    tbCmpDest!Livello = tbCmpOrig!Livello
    tbCmpDest!Tipo = tbCmpOrig!Tipo
    tbCmpDest!Posizione = tbCmpOrig!Posizione
    tbCmpDest!Byte = tbCmpOrig!Byte
    tbCmpDest!Lunghezza = tbCmpOrig!Lunghezza
    tbCmpDest!Decimali = tbCmpOrig!Decimali
    tbCmpDest!Segno = tbCmpOrig!Segno
    tbCmpDest!Usage = tbCmpOrig!Usage
    tbCmpDest!Picture = tbCmpOrig!Picture
    tbCmpDest!Occurs = tbCmpOrig!Occurs
    tbCmpDest!NomeRed = tbCmpOrig!NomeRed
    tbCmpDest!Selettore_Valore = ""
    tbCmpDest.Update
    tbCmpDest.Close
    tbCmpOrig.MoveNext
  Loop
  tbCmpOrig.Close
End Sub

Public Sub Update_fileName()
  Dim rs As Recordset, rsCont As Recordset
  Dim i As Integer
  
  Set rs = m_fun.Open_Recordset("select distinct filename, count(filename) as Cont from mgconvdata_area " & _
                                "Group by filename order by filename")
  Do Until rs.EOF
    If rs!cont > 1 Then
      Set rsCont = m_fun.Open_Recordset("select * from mgconvdata_area where " & _
                                        "filename = '" & rs!fileName & "' order by idoggetto, idarea")
      For i = 0 To rsCont.RecordCount - 1
        If i > 0 Then
          rsCont!fileName = rsCont!fileName & "_" & i
          rsCont.Update
        End If
        rsCont.MoveNext
      Next i
      rsCont.Close
    End If
    rs.MoveNext
  Loop
  rs.Close
End Sub

Public Function readFile(dialogFileName As String) As String()
  Dim values() As String
  Dim FD As Long
  Dim Line As String
  
  ReDim values(0)
  
  FD = FreeFile
  Open dialogFileName For Input As FD
  While Not EOF(FD)
    Line Input #FD, Line
    If Len(Trim(Line)) Then
      ReDim Preserve values(UBound(values) + 1)
      values(UBound(values)) = Trim(Line)
    Else
      'riga vuota: buttiamo via
    End If
  Wend
  Close FD
    
  readFile = values
End Function

Sub CopiaCmpMG()
  Dim tbCmpOrig As Recordset, tbCmpDest As Recordset
  Dim wSql As String
  
  wSql = "Delete from mgconvdata_cmp where " & _
         "idoggetto = " & MatsdF_AddArea.lswAreas.SelectedItem & " AND " & _
         "IdArea = " & MatsdF_AddArea.lswAreas.SelectedItem.ListSubItems(3) & ";"
  Menu.TsConnection.Execute wSql
  
  Set tbCmpOrig = m_fun.Open_Recordset("select * from psdata_cmp where " & _
                                       "idoggetto = " & MatsdF_AddArea.lswAreas.SelectedItem & " AND " & _
                                       "idarea = " & MatsdF_AddArea.lswAreas.SelectedItem.ListSubItems(3) & " AND " & _
                                       "livello <> 88")
  Do Until tbCmpOrig.EOF
    Set tbCmpDest = m_fun.Open_Recordset("select * from mgconvdata_cmp where " & _
                                         "idcmp = '" & tbCmpOrig!Codice & "' AND " & _
                                         "idarea = " & tbCmpOrig!idArea & " AND " & _
                                         "idoggetto = " & tbCmpOrig!IdOggetto)
    If tbCmpDest.EOF Then
      tbCmpDest.AddNew
    End If
    tbCmpDest!IdOggetto = tbCmpOrig!IdOggetto
    tbCmpDest!idArea = tbCmpOrig!idArea
    tbCmpDest!IdCmp = tbCmpOrig!Codice
    tbCmpDest!Nome = tbCmpOrig!Nome
    tbCmpDest!Ordinale = tbCmpOrig!Ordinale
    tbCmpDest!Livello = tbCmpOrig!Livello
    tbCmpDest!Tipo = tbCmpOrig!Tipo
    tbCmpDest!Posizione = tbCmpOrig!Posizione
    tbCmpDest!Byte = tbCmpOrig!Byte
    tbCmpDest!Lunghezza = tbCmpOrig!Lunghezza
    tbCmpDest!Decimali = tbCmpOrig!Decimali
    tbCmpDest!Segno = tbCmpOrig!Segno
    tbCmpDest!Usage = tbCmpOrig!Usage
    tbCmpDest!Picture = tbCmpOrig!Picture
    tbCmpDest!Occurs = tbCmpOrig!Occurs
    tbCmpDest!NomeRed = tbCmpOrig!NomeRed
    tbCmpDest!Selettore_Valore = ""
    tbCmpDest.Update
    tbCmpDest.Close
    tbCmpOrig.MoveNext
  Loop
  tbCmpOrig.Close
End Sub

Public Sub Carica_Lista_Structure(Optional sql As String = "")
  Dim tbArea As Recordset
   
  MatsdF_EBCDIC2ASCII.lswStructSel.ListItems.Clear
  If sql = "" Then
    Set tbArea = m_fun.Open_Recordset("select IdOggetto, IdArea, Nome, FileName from MgConvData_Area order by filename")
  Else
    Set tbArea = m_fun.Open_Recordset(sql)
  End If
  Do Until tbArea.EOF
    With MatsdF_EBCDIC2ASCII.lswStructSel.ListItems.Add(, , tbArea!fileName)
      .SubItems(1) = tbArea!Nome
      .tag = tbArea!IdOggetto & "\" & tbArea!idArea
    End With
    tbArea.MoveNext
  Loop
  tbArea.Close
End Sub

Public Sub Converti_Structure(IndStruct As String, fileName As String)
  Dim StructName As String
  Dim rsStruct As Recordset
  Dim numFile As Integer
  Dim RecordLenght As String
  Dim TipoConv As String
  Dim ConversioneBlank As String
  Dim ElementoStructure As String
  Dim Area As Integer
  Dim Oggetto As Long
  
  On Error GoTo ErrorHandler
    
  Oggetto = Left(IndStruct, InStr(IndStruct, "\") - 1)
  Area = Mid(IndStruct, InStr(IndStruct, "\") + 1)
  Set rsStruct = m_fun.Open_Recordset("select * from MgConvData_Area where " & _
                                      "IdArea = " & Area & " AND IdOggetto = " & Oggetto)
  If Not rsStruct.EOF Then
    If rsStruct.RecordCount = 1 Then
      fileName = rsStruct!fileName
    End If
    StructName = rsStruct!Nome
    If rsStruct!ReplaceSpacetoZero Then
      ConversioneBlank = " "
    Else
      ConversioneBlank = 0
    End If
  End If
  rsStruct.Close
  
  If MatsdF_EBCDIC2ASCII.optEbcdic2Ascii Then
    'EBCDIC to ASCII
    numFile = FreeFile
    Carica_Array Area, Oggetto
    TipoConv = "e2a"
    ElementoStructure = CreaRigaStructure(fileName)
    RecordLenght = Format(Fields(1).Byte, "000000")
    
    'stefano: boh???? cosi' funziona, altrimenti dava sempre errore, quando c'erano di mezzo redefines
    ' ed il file esisteva gi�
    Close numFile
    Open m_fun.FnPathPrj & "\Output-prj\" & StructEB2ASPath & "\" & fileName For Output As numFile
    Print #numFile, RecordLenght & TipoConv & ConversioneBlank & ElementoStructure;
    Close numFile
  Else
''''    'ASCII to EBCDIC
''''    Numfile = FreeFile
''''    Open m_fun.FnPathPrj & "\Output-prj\" & StructAS2EBPath & "\" & FileName For Output As Numfile
''''    'Carica_Array IndStruct
''''    RecordLenght = Format(Fields(1).Lunghezza, "000000")
''''    TipoConv = "a2e"
''''    'ElementoStructure = CreaRigaStructure
''''    Close Numfile
''''    'REDFILE
''''    Numfile = FreeFile
''''    Open m_fun.FnPathPrj & "\Output-prj\" & RedFileAS2EBPath & "\" & FileName For Output As Numfile
''''
''''    Close Numfile
  End If
   
  Exit Sub
ErrorHandler:
  If Err.Number = 76 Then
    'Crea directory mancanti
    m_fun.MkDir_API m_fun.FnPathPrj & "\Output-Prj\Data\"
    m_fun.MkDir_API m_fun.FnPathPrj & "\Output-Prj\Data\EBCDICtoASCII"
    m_fun.MkDir_API m_fun.FnPathPrj & "\Output-Prj\Data\ASCIItoEBCDIC"
    m_fun.MkDir_API m_fun.FnPathPrj & "\Output-Prj\Data\EBCDICtoASCII\Structure"
    m_fun.MkDir_API m_fun.FnPathPrj & "\Output-Prj\Data\EBCDICtoASCII\RedFile"
    m_fun.MkDir_API m_fun.FnPathPrj & "\Output-Prj\Data\ASCIItoEBCDIC\Structure"
    m_fun.MkDir_API m_fun.FnPathPrj & "\Output-Prj\Data\ASCIItoEBCDIC\RedFile"
    Resume
  ElseIf Err.Number = 55 Then
    Resume Next
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' E' un SostWord con indentazione (relativa alla posizione del TAG)
' Tratta i TAG "ciclici": devono terminare con (i)...
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub changeTag(rtb As RichTextBox, tag As String, text As String)
  Dim Posizione As Long
  Dim crLfPosition As Long
  Dim indentedText As String
    
  If isCiclicTag(tag) Then
    If Len(text) Then text = text & vbCrLf & tag 'se text="" e' un clean!...
  End If
  While True
    Posizione = rtb.Find(tag, Posizione + Len(indentedText))
    Select Case Posizione
      Case -1
        Exit Sub
      Case 0
        indentedText = text
      Case Else
        crLfPosition = InStrRev(rtb.text, vbCrLf, Posizione)
        If crLfPosition = 0 Then
          'Linea 1: non ho il fine linea...
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(Posizione))
        Else
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(Posizione - crLfPosition - 1))
        End If
    End Select
    rtb.SelStart = Posizione
    rtb.SelLength = Len(tag)
    rtb.SelText = indentedText
  Wend
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function isCiclicTag(tag As String) As String
  isCiclicTag = Right(tag, 4) = "(i)>"
End Function

Sub Carica_Array(Area As Integer, Oggetto As Long)
  Dim rsCmp As Recordset, i As Integer
   
  On Error GoTo ErrorHandler
  i = 1
  Set rsCmp = m_fun.Open_Recordset("select * from MgConvData_Cmp where " & _
                                   "IdArea = " & Area & " AND IdOggetto = " & Oggetto & " AND " & _
                                   "Livello <> 88 order by Ordinale")
  Do Until rsCmp.EOF
    ReDim Preserve Fields(i)
    Fields(i).IdOggetto = rsCmp!IdOggetto
    Fields(i).idArea = rsCmp!idArea
    Fields(i).IdCmp = rsCmp!IdCmp
    Fields(i).Nome = rsCmp!Nome
    Fields(i).Ordinale = rsCmp!Ordinale
    Fields(i).Livello = rsCmp!Livello
    Fields(i).Tipo = rsCmp!Tipo
    Fields(i).Posizione = rsCmp!Posizione
    Fields(i).Byte = rsCmp!Byte
    Fields(i).Lunghezza = rsCmp!Lunghezza
    Fields(i).Decimali = rsCmp!Decimali
    Fields(i).Segno = rsCmp!Segno
    Fields(i).Usage = rsCmp!Usage
    Fields(i).Picture = rsCmp!Picture
    Fields(i).Occurs = rsCmp!Occurs
    Fields(i).NomeRed = rsCmp!NomeRed
    Fields(i).Sel_Field = rsCmp!Sel_Field & ""
    Fields(i).Operatore = rsCmp!Operatore & ""
    Fields(i).Selettore_Valore = rsCmp!Selettore_Valore
    Fields(i).NumRedefines = 0
    Fields(i).DeleteRed = ""
    rsCmp.MoveNext
    i = i + 1
  Loop
  rsCmp.Close
  Exit Sub
ErrorHandler:
  Resume Next
End Sub

Function CreaRigaStructure(NomeFile As String) As String
  Dim i As Long, a As Long, b As Long, t As Long
  Dim ArrOcc(2) As tOccurs, indOcc As Integer ' In Cobol posso avere fino a 3 livelli di Occurs
  Dim NumByte As Long
  Dim TipoPicture As String
  Dim Indicatore As String
  Dim PictureCampo As String, PictureOccurs As String, PictureRedefines As String
  Dim numFile As Integer, RedFile As Integer
  Dim DirFile As String, Start As String
  Dim IndRedefines As String
  
  Dim NumRedefines As String
  Dim StringaRedefines As String
  Dim OperatoreLogico As String
  Dim Str_Red_Da As String, Str_Red_Per As String
  Dim PictureRedFile As String
  Dim rsCmpRedefines As Recordset
  Dim TipoPictureRed As String
  Dim SegnoRed As String
  Dim PosizioneRed As String, ByteRed As String
  Dim DecimaliRed As Integer
  Dim arrayBetween() As String
  Dim StringaFinale As String
  Dim addOn As Long
  Dim primoRec As Boolean
  Dim X As Long
  Dim levGroup As Integer
  Dim tmpPicture(2) As String
  Dim tmpPic As String
  
  On Error GoTo ErrorHandler
  
  Separatore = Trim(m_fun.LeggiParam("EBC2ASC_SEPARATOR"))
  
  CreaRigaStructure = ""
  IndRedefines = 1
  
  numFile = FreeFile
  Open m_fun.FnPathPrj & "\Output-prj\" & StructEB2ASPath & "\" & NomeFile & "_RED" For Output As numFile
  Close numFile
  RedFile = FreeFile
  Open m_fun.FnPathPrj & "\Output-prj\" & RedFileEB2ASPath & "\" & NomeFile For Output As RedFile
  Close RedFile
  For i = 1 To UBound(Fields)
    TipoPicture = ""
    Indicatore = ""
    If Len(Trim(Fields(i).NomeRed)) Then
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' REDEFINES
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Start = Fields(i).Posizione
      NumByte = Fields(i).Byte
      PictureRedefines = ""
      DecimaliRed = 0
      TipoPictureRed = ""
      SegnoRed = ""
      For indOcc = 0 To 2
        ArrOcc(indOcc).NumOccurs = 0
        ArrOcc(indOcc).levOccurs = 0
        tmpPicture(indOcc) = ""
      Next indOcc
      indOcc = 0
      primoRec = True
      tmpPic = ""
      
      'Creo un file esterno con nome "RED_" & NomeCampo per indicare la redefines
      numFile = FreeFile
      Open m_fun.FnPathPrj & "\Output-prj\" & StructEB2ASPath & "\" & NomeFile & "_RED" For Append As numFile
      
      'controllo se la lunghezza del campo ridefinito � uguale a lunghezza campo redefines
      addOn = 0
      Set rsCmpRedefines = m_fun.Open_Recordset("select byte,nome from MgConvData_Cmp where " & _
                                                "Nome = '" & Replace(Fields(i).NomeRed, "'", "''") & "'")
      If rsCmpRedefines.RecordCount Then
        If rsCmpRedefines!Byte <> Fields(i).Byte Then
          If rsCmpRedefines!Byte > Fields(i).Byte Then
            MatsdF_EBCDIC2ASCII.LswLog.ListItems.Add , , Time & " - Warning: Length " & Fields(i).Nome & " greater than " & rsCmpRedefines!Nome & "! Authomatically filled up"
            addOn = rsCmpRedefines!Byte - Fields(i).Byte
          Else
            MatsdF_EBCDIC2ASCII.LswLog.ListItems.Add , , Time & " - Warning: Length " & Fields(i).Nome & " smaller than " & rsCmpRedefines!Nome & "! Not valid result"
            addOn = 0
          End If
          MatsdF_EBCDIC2ASCII.LswLog.Refresh
          MatsdF_EBCDIC2ASCII.LswLog.ListItems(MatsdF_EBCDIC2ASCII.LswLog.ListItems.Count).EnsureVisible
        End If
      End If
      rsCmpRedefines.Close
            
      Set rsCmpRedefines = m_fun.Open_Recordset("select tipo,[usage],segno,decimali,posizione,byte from MgConvData_Cmp where " & _
                                                "nome = '" & Fields(i).Sel_Field & "'")
      If rsCmpRedefines.RecordCount Then
        If rsCmpRedefines!Tipo = "X" Or rsCmpRedefines!Tipo = "A" Then
          TipoPictureRed = "X"
          SegnoRed = "N"
        Else
          If rsCmpRedefines!Usage = "ZND" Then
            TipoPictureRed = "9"
          ElseIf rsCmpRedefines!Usage = "BNR" Then
            TipoPictureRed = "B"
          Else
            TipoPictureRed = "P"
          End If
          If rsCmpRedefines!Segno = "S" Then
            SegnoRed = "S"
          Else
            SegnoRed = "N"
          End If
        End If
        DecimaliRed = rsCmpRedefines!Decimali
        PosizioneRed = Format(rsCmpRedefines!Posizione, "000000")
        ByteRed = Format(rsCmpRedefines!Byte, "000000")
      End If
      rsCmpRedefines.Close
      
      If Fields(i).Tipo = "A" Then
        ' Gestione di una struttura occursata: StrutturaA ridefinisce CampoB
        levGroup = Fields(i).Livello
        For a = i To UBound(Fields)
          If Fields(a).Livello > levGroup Or a = i Then
            If Fields(a).Tipo = "X" Or Fields(a).Tipo = "A" Then
              TipoPicture = "X"
              Indicatore = "N"
            Else
              If Fields(a).Usage = "ZND" Then
                TipoPicture = "9"
              ElseIf Fields(a).Usage = "BNR" Then
                TipoPicture = "B"
              Else
                TipoPicture = "P"
              End If
              If Fields(a).Segno = "S" Then
                Indicatore = "S"
              Else
                Indicatore = "N"
              End If
            End If
            
            If Len(Fields(a).Selettore_Valore) Then
              'REDFILE
              RedFile = FreeFile
              Open m_fun.FnPathPrj & "\Output-prj\" & RedFileEB2ASPath & "\" & NomeFile For Append As RedFile
              NumRedefines = Format(IndRedefines, "00")
              'stefano: non funzionava per il caso pi� probabile: la between sui numerici per ovviare al
              ' problema del NUMERIC
              OperatoreLogico = Fields(a).Operatore & Space(2 - Len(Fields(a).Operatore))
              arrayBetween = Split(Trim(Fields(i).Selettore_Valore), " ")
              StringaFinale = ""
              For t = 0 To UBound(arrayBetween)
                If Len(arrayBetween(t)) Then 'elimina eventuali righe vuote
                  StringaRedefines = ImpostaValore(arrayBetween(t), TipoPictureRed, DecimaliRed)
                  StringaFinale = Trim(StringaFinale & " " & StringaRedefines)
                End If
              Next t
              StringaFinale = StringaFinale & Space(62 - Len(StringaFinale))
              Str_Red_Da = PosizioneRed
              Str_Red_Per = Format(ByteRed, "000000")
              PictureRedFile = TipoPictureRed & SegnoRed
              Print #RedFile, NumRedefines & StringaFinale & OperatoreLogico & Str_Red_Da & Str_Red_Per & PictureRedFile
              Close RedFile
            End If
            
            If Fields(a).Livello > ArrOcc(indOcc).levOccurs Then
              'PictureRedefines = ""
              If Fields(a).Occurs Then
                ' Gestione per Occurs annidate
                If ArrOcc(indOcc).levOccurs = 0 Then
                  ArrOcc(indOcc).NumOccurs = Fields(a).Occurs
                  ArrOcc(indOcc).levOccurs = Fields(a).Livello
                Else
                  If Fields(a).Livello = ArrOcc(indOcc).levOccurs Then
                    If ArrOcc(indOcc).NumOccurs = 0 Then
                      ArrOcc(indOcc).NumOccurs = Fields(a).Occurs
                      ArrOcc(indOcc).levOccurs = Fields(a).Livello
                    End If
                  ElseIf Fields(a).Livello > ArrOcc(indOcc).levOccurs Then
                    indOcc = indOcc + 1
                    ArrOcc(indOcc).NumOccurs = Fields(a).Occurs
                    ArrOcc(indOcc).levOccurs = Fields(a).Livello
                  Else
                    For X = 1 To ArrOcc(indOcc).NumOccurs
                      tmpPic = tmpPic & tmpPicture(indOcc)
                    Next X
                    tmpPicture(indOcc - 1) = tmpPic
                    tmpPicture(indOcc) = ""
                    ArrOcc(indOcc).NumOccurs = 0
                    ArrOcc(indOcc).levOccurs = 0
                    indOcc = indOcc - 1
                  End If
                End If
                PictureOccurs = ""
                'campi con lunghezza zero esistono, perch� lunghezza � solo la parte intera..
                If (Fields(a).Lunghezza + Fields(a).Decimali) Then
                  For b = 1 To IIf(Fields(a).Tipo = "A", (Fields(a).Byte / Fields(a).Occurs), Fields(a).Byte)
                    If Indicatore = "S" And b = Fields(a).Byte Then
                      PictureOccurs = PictureOccurs & TipoPicture & Indicatore
                    Else
                      PictureOccurs = PictureOccurs & TipoPicture & "N"
                    End If
                  Next b
                  If PictureOccurs <> "" Then
                    PictureOccurs = Separatore & Separatore & PictureOccurs
                  End If
                End If
                If Len(PictureOccurs) Then
                  For X = 1 To Fields(a).Occurs
                    tmpPicture(indOcc) = tmpPicture(indOcc) & PictureOccurs
                  Next X
                  PictureOccurs = ""
                  If Fields(a).Livello = ArrOcc(indOcc).levOccurs Then
                    ArrOcc(indOcc).NumOccurs = 0
                    ArrOcc(indOcc).levOccurs = 0
                    indOcc = IIf(indOcc > 0, indOcc - 1, indOcc)
                  End If
                End If
              Else
                'campi con lunghezza zero esistono, perch� lunghezza � solo la parte intera..
                PictureCampo = ""
                If (Fields(a).Lunghezza + Fields(a).Decimali) Then
                  For b = 1 To Fields(a).Byte
                    If Indicatore = "S" And b = Fields(a).Byte Then
                      PictureCampo = PictureCampo & TipoPicture & Indicatore
                    Else
                      PictureCampo = PictureCampo & TipoPicture & "N"
                    End If
                  Next b
                End If
                If PictureCampo <> "" Then
                  If ArrOcc(indOcc).NumOccurs = 0 Then
                    PictureRedefines = PictureRedefines & Separatore & Separatore & PictureCampo
                  Else
                    tmpPicture(indOcc) = tmpPicture(indOcc) & Separatore & Separatore & PictureCampo
                  End If
                End If
                PictureCampo = ""
                If Fields(a).Livello = ArrOcc(indOcc).levOccurs Then
                  ArrOcc(indOcc).NumOccurs = 0
                  ArrOcc(indOcc).levOccurs = 0
                  indOcc = IIf(indOcc > 0, indOcc - 1, indOcc)
                End If
              End If
            Else
              If tmpPicture(indOcc) <> "" Then
                For X = 1 To ArrOcc(indOcc).NumOccurs
                  tmpPic = tmpPic & tmpPicture(indOcc)
                Next X
                If indOcc = 0 Then
                  PictureRedefines = PictureRedefines & tmpPic
                Else
                  tmpPicture(indOcc - 1) = tmpPicture(indOcc - 1) & tmpPic
                End If
                tmpPicture(indOcc) = ""
                If Fields(a).Livello = ArrOcc(indOcc).levOccurs Then
                  ' Non serve reimpostare anche il livello, tanto � gi� uguale
                  ArrOcc(indOcc).NumOccurs = Fields(a).Occurs
                  If Fields(a).Occurs = 0 Then
                    ArrOcc(indOcc).levOccurs = 0
                  End If
                Else
                  ArrOcc(indOcc).NumOccurs = 0
                  ArrOcc(indOcc).levOccurs = 0
                  indOcc = indOcc - 1
                End If
                a = a - 1
              End If
            End If
          Else
            Exit For
          End If
        Next a
        
        Do Until indOcc = 0
          tmpPic = ""
          For X = 1 To ArrOcc(indOcc).NumOccurs
            tmpPic = tmpPic & tmpPicture(indOcc)
          Next X
          tmpPicture(indOcc - 1) = tmpPicture(indOcc - 1) & tmpPic
          tmpPicture(indOcc) = ""
          ArrOcc(indOcc).NumOccurs = 0
          ArrOcc(indOcc).levOccurs = 0
          indOcc = indOcc - 1
        Loop
        ' Sono sul campo successivo alla struttura, ergo, � tempo di chiudere e tirare le somme
        For X = 1 To ArrOcc(0).NumOccurs
          PictureRedefines = PictureRedefines & tmpPicture(0)
        Next X
        If Len(PictureRedefines) = 0 Then
          PictureRedefines = PictureRedefines & tmpPicture(0)
        End If
        tmpPicture(0) = ""
        PictureOccurs = ""
        i = a - 1
      Else
        ' Gestione semplice: CampoA(Occursato o no) ridefinisce CampoB
        If Fields(i).Tipo = "X" Then
          TipoPicture = "X"
          Indicatore = "N"
        Else
          If Fields(i).Usage = "ZND" Then
            TipoPicture = "9"
          ElseIf Fields(i).Usage = "BNR" Then
            TipoPicture = "B"
          Else
            TipoPicture = "P"
          End If
          If Fields(i).Segno = "S" Then
            Indicatore = "S"
          Else
            Indicatore = "N"
          End If
        End If
        
        If Len(Fields(i).Selettore_Valore) Then
          'REDFILE
          RedFile = FreeFile
          Open m_fun.FnPathPrj & "\Output-prj\" & RedFileEB2ASPath & "\" & NomeFile For Append As RedFile
          NumRedefines = Format(IndRedefines, "00")
          'stefano: non funzionava per il caso pi� probabile: la between sui numerici per ovviare al
          ' problema del NUMERIC
          OperatoreLogico = Fields(i).Operatore & Space(2 - Len(Fields(i).Operatore))
          arrayBetween = Split(Trim(Fields(i).Selettore_Valore), " ")
          StringaFinale = ""
          For t = 0 To UBound(arrayBetween)
            If Len(arrayBetween(t)) Then 'elimina eventuali righe vuote
              StringaRedefines = ImpostaValore(arrayBetween(t), TipoPictureRed, DecimaliRed)
              StringaFinale = Trim(StringaFinale & " " & StringaRedefines)
            End If
          Next t
          StringaFinale = StringaFinale & Space(62 - Len(StringaFinale))
          Str_Red_Da = PosizioneRed
          Str_Red_Per = Format(ByteRed, "000000")
          PictureRedFile = TipoPictureRed & SegnoRed
          Print #RedFile, NumRedefines & StringaFinale & OperatoreLogico & Str_Red_Da & Str_Red_Per & PictureRedFile
          Close RedFile
        End If

        PictureRedefines = ""
        If Fields(i).Occurs Then
          PictureOccurs = ""
          'campi con lunghezza zero esistono, perch� lunghezza � solo la parte intera..
          If (Fields(i).Lunghezza + Fields(i).Decimali) Then
            For a = 1 To (Fields(i).Byte / Fields(i).Occurs)
              If Indicatore = "S" And a = Fields(i).Byte Then
                PictureOccurs = PictureOccurs & TipoPicture & Indicatore
              Else
                PictureOccurs = PictureOccurs & TipoPicture & "N"
              End If
            Next a
            If PictureOccurs <> "" Then
              PictureOccurs = Separatore & Separatore & PictureOccurs
            End If
          End If
          For X = 1 To Fields(i).Occurs
            PictureRedefines = PictureRedefines & PictureOccurs
          Next X
          PictureOccurs = ""
        Else
          'campi con lunghezza zero esistono, perch� lunghezza � solo la parte intera..
          If (Fields(i).Lunghezza + Fields(i).Decimali) Then
            For a = 1 To Fields(i).Byte
              If Indicatore = "S" And a = Fields(i).Byte Then
                PictureRedefines = PictureRedefines & TipoPicture & Indicatore
              Else
                PictureRedefines = PictureRedefines & TipoPicture & "N"
              End If
            Next a
          End If
          If PictureRedefines <> "" Then
            PictureRedefines = Separatore & Separatore & PictureRedefines
          End If
        End If
      End If
      
      'riempie la struttura della redefines fino al valore di quella base
      If addOn > 0 Then
        PictureRedefines = PictureRedefines & Separatore & Separatore
        For t = 1 To addOn
          PictureRedefines = PictureRedefines & "XN"
        Next t
      End If
      Print #numFile, Format(Start, "000000") & Format(NumByte, "000000") & PictureRedefines
      IndRedefines = IndRedefines + 1
      PictureRedefines = ""
      Close numFile
      'se ha raggiunto la fine con l'ultima redefines, deve uscire anche dal ciclo generale
      If a > UBound(Fields) Then Exit For
    ElseIf Fields(i).Occurs Then
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' OCCURS
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      For indOcc = 0 To 2
        ArrOcc(indOcc).NumOccurs = 0
        ArrOcc(indOcc).levOccurs = 0
        tmpPicture(indOcc) = ""
      Next indOcc
      indOcc = 0
      ArrOcc(indOcc).NumOccurs = Fields(i).Occurs
      ArrOcc(indOcc).levOccurs = Fields(i).Livello
      PictureOccurs = ""
      For a = i To UBound(Fields)
        If Fields(a).Livello > Fields(i).Livello Or a = i Then
          If Len(Trim(Fields(a).NomeRed)) Then
            'Creo un file esterno con nome "RED_" & NomeCampo per indicare la redefines
            numFile = FreeFile
            Open m_fun.FnPathPrj & "\Output-prj\" & StructEB2ASPath & "\" & NomeFile & "_RED" For Append As numFile
            
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' OCCURS COMPLESSA CON REDEFINES INTERNA
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'controllo se la lunghezza del campo ridefinito � uguale a lunghezza campo redefines
            addOn = 0
            Set rsCmpRedefines = m_fun.Open_Recordset("select * from MgConvData_Cmp where " & _
                                                      "Nome = '" & Replace(Fields(a).NomeRed, "'", "''") & "'")
            If rsCmpRedefines.RecordCount Then
              If rsCmpRedefines!Byte <> Fields(a).Byte Then
                If rsCmpRedefines!Byte > Fields(a).Byte Then
                  MatsdF_EBCDIC2ASCII.LswLog.ListItems.Add , , Time & " - Warning: Length " & Fields(a).Nome & " greater than " & rsCmpRedefines!Nome & "! Authomatically filled up"
                  addOn = rsCmpRedefines!Byte - Fields(a).Byte
                Else
                  MatsdF_EBCDIC2ASCII.LswLog.ListItems.Add , , Time & " - Warning: Length " & Fields(a).Nome & " smaller than " & rsCmpRedefines!Nome & "! Not valid result"
                  addOn = 0
                End If
                MatsdF_EBCDIC2ASCII.LswLog.Refresh
                MatsdF_EBCDIC2ASCII.LswLog.ListItems(MatsdF_EBCDIC2ASCII.LswLog.ListItems.Count).EnsureVisible
              End If
            End If
            rsCmpRedefines.Close
            
            Start = Format(Fields(a).Posizione, "000000")
            ''Start = Format(Trim(Fields(a).Posizione), "000000")
            NumByte = 0
            PictureRedefines = ""
            DecimaliRed = 0
            TipoPictureRed = ""
            SegnoRed = ""
            Set rsCmpRedefines = m_fun.Open_Recordset("select * from MgConvData_Cmp where " & _
                                                      "nome = '" & Fields(a).Sel_Field & "'")
            If rsCmpRedefines.RecordCount Then
              If rsCmpRedefines!Tipo = "X" Or rsCmpRedefines!Tipo = "A" Then
                TipoPictureRed = "X"
                SegnoRed = "N"
              Else
                If rsCmpRedefines!Usage = "ZND" Then
                  TipoPictureRed = "9"
                ElseIf rsCmpRedefines!Usage = "BNR" Then
                  TipoPictureRed = "B"
                Else
                  TipoPictureRed = "P"
                End If
                If rsCmpRedefines!Segno = "S" Then
                  SegnoRed = "S"
                Else
                  SegnoRed = "N"
                End If
              End If
              DecimaliRed = rsCmpRedefines!Decimali
              PosizioneRed = Format(rsCmpRedefines!Posizione, "000000")
              ByteRed = Format(rsCmpRedefines!Byte, "000000")
            End If
            rsCmpRedefines.Close
            For b = a To UBound(Fields)
              If Fields(b).Livello > Fields(a).Livello Or b = a Then
                If Fields(b).Tipo = "X" Or Fields(b).Tipo = "A" Then
                  TipoPicture = "X"
                  Indicatore = "N"
                Else
                  If Fields(b).Usage = "ZND" Then
                    TipoPicture = "9"
                  ElseIf Fields(b).Usage = "BNR" Then
                    TipoPicture = "B"
                  Else
                    TipoPicture = "P"
                  End If
                  If Fields(b).Segno = "S" Then
                    Indicatore = "S"
                  Else
                    Indicatore = "N"
                  End If
                End If
                If Len(Fields(b).Selettore_Valore) Then
                  'REDFILE
                  RedFile = FreeFile
                  Open m_fun.FnPathPrj & "\Output-prj\" & RedFileEB2ASPath & "\" & NomeFile For Append As RedFile
                  NumRedefines = Format(IndRedefines, "00")
                  'stefano: non funzionava per il caso pi� probabile: la between sui numerici per ovviare al
                  ' problema del NUMERIC
                  OperatoreLogico = Fields(b).Operatore & Space(2 - Len(Fields(b).Operatore))
                  arrayBetween = Split(Trim(Fields(b).Selettore_Valore), " ")
                  StringaFinale = ""
                  For t = 0 To UBound(arrayBetween)
                    If Len(arrayBetween(t)) Then 'elimina eventuali righe vuote
                      StringaRedefines = ImpostaValore(arrayBetween(t), TipoPictureRed, DecimaliRed)
                      StringaFinale = Trim(StringaFinale & " " & StringaRedefines)
                    End If
                  Next
                  StringaFinale = StringaFinale & Space(62 - Len(StringaFinale))
                  Str_Red_Da = PosizioneRed
                  'se � binario o packed, la lunghezza � diversa
                  'If InStr(Trim(Fields(a).Selettore_Valore), " ") And OperatoreLogico = "<>" Then
                  'se � binario o packed, la lunghezza � diversa
                    'Str_Red_Per = Format(Len(Trim(Mid(Fields(a).Selettore_Valore, 1, InStr(Fields(a).Selettore_Valore, " ")))), "000000")
                  'Else
                    'Str_Red_Per = Format(Len(Trim(Fields(a).Selettore_Valore)), "000000")
                  'End If
                  'Str_Red_Per = Format(Fields(a).Byte, "000000")
                  Str_Red_Per = Format(ByteRed, "000000")
                  PictureRedFile = TipoPictureRed & SegnoRed
                  Print #RedFile, NumRedefines & StringaFinale & OperatoreLogico & Str_Red_Da & Str_Red_Per & PictureRedFile
                  Close RedFile
                End If
                'campi con lunghezza zero esistono, perch� lunghezza � solo la parte intera..
                If (Fields(b).Lunghezza + Fields(b).Decimali) Then
                  For t = 1 To Fields(b).Byte
                    If Indicatore = "S" And t = Fields(b).Byte Then
                      PictureRedefines = PictureRedefines & TipoPicture & Indicatore
                    Else
                      PictureRedefines = PictureRedefines & TipoPicture & "N"
                    End If
                    NumByte = NumByte + 1
                  Next t
                End If
              Else
                'Reimposto l'indice per leggere il campo successivo
                a = b - 1
                Exit For
              End If
            Next b
            'silvia 27-03-2008
            If PictureRedefines <> "" Then
              PictureRedefines = Separatore & Separatore & PictureRedefines
            End If
            'se ha raggiunto la fine con l'ultima redefines, deve uscire anche dal ciclo generale
            If b > UBound(Fields) Then Exit For
          Else
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' OCCURS SEMPLICE, SENZA REDEFINES
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Fields(a).Tipo = "X" Or Fields(a).Tipo = "A" Then
              TipoPicture = "X"
              Indicatore = "N"
            Else
              If Fields(a).Usage = "ZND" Then
                TipoPicture = "9"
              ElseIf Fields(a).Usage = "BNR" Then
                TipoPicture = "B"
              Else
                TipoPicture = "P"
              End If
            End If
            If Fields(a).Segno = "S" Then
              Indicatore = "S"
            Else
              Indicatore = "N"
            End If
            
            If Fields(a).Livello > ArrOcc(indOcc).levOccurs Then
              PictureRedefines = ""
              If Fields(a).Occurs Then
                ' Gestione per Occurs annidate
                If ArrOcc(indOcc).levOccurs = 0 Then
                  ArrOcc(indOcc).NumOccurs = Fields(a).Occurs
                  ArrOcc(indOcc).levOccurs = Fields(a).Livello
                Else
                  If Fields(a).Livello = ArrOcc(indOcc).levOccurs Then
                    If ArrOcc(indOcc).NumOccurs = 0 Then
                      ArrOcc(indOcc).NumOccurs = Fields(a).Occurs
                      ArrOcc(indOcc).levOccurs = Fields(a).Livello
                    End If
                  ElseIf Fields(a).Livello > ArrOcc(indOcc).levOccurs Then
                    indOcc = indOcc + 1
                    ArrOcc(indOcc).NumOccurs = Fields(a).Occurs
                    ArrOcc(indOcc).levOccurs = Fields(a).Livello
                  Else
                    For X = 1 To ArrOcc(indOcc).NumOccurs
                      tmpPic = tmpPic & tmpPicture(indOcc)
                    Next X
                    tmpPicture(indOcc - 1) = tmpPic
                    tmpPicture(indOcc) = ""
                    ArrOcc(indOcc).NumOccurs = 0
                    ArrOcc(indOcc).levOccurs = 0
                    indOcc = indOcc - 1
                  End If
                End If
                PictureOccurs = ""
                'campi con lunghezza zero esistono, perch� lunghezza � solo la parte intera..
                If (Fields(a).Lunghezza + Fields(a).Decimali) Then
                  For b = 1 To IIf(Fields(a).Tipo = "A", (Fields(a).Byte / Fields(a).Occurs), Fields(a).Byte)
                    If Indicatore = "S" And b = Fields(a).Byte Then
                      PictureOccurs = PictureOccurs & TipoPicture & Indicatore
                    Else
                      PictureOccurs = PictureOccurs & TipoPicture & "N"
                    End If
                  Next b
                  If PictureOccurs <> "" Then
                    PictureOccurs = Separatore & Separatore & PictureOccurs
                  End If
                End If
                If Len(PictureOccurs) Then
                  For X = 1 To Fields(a).Occurs
                    tmpPicture(indOcc) = tmpPicture(indOcc) & PictureOccurs
                  Next X
                  PictureOccurs = ""
                  If Fields(a).Livello = ArrOcc(indOcc).levOccurs Then
                    ArrOcc(indOcc).NumOccurs = 0
                    ArrOcc(indOcc).levOccurs = 0
                    indOcc = IIf(indOcc > 0, indOcc - 1, indOcc)
                  End If
                End If
              Else
                'campi con lunghezza zero esistono, perch� lunghezza � solo la parte intera..
                PictureCampo = ""
                If (Fields(a).Lunghezza + Fields(a).Decimali) Then
                  For b = 1 To Fields(a).Byte
                    If Indicatore = "S" And b = Fields(a).Byte Then
                      PictureCampo = PictureCampo & TipoPicture & Indicatore
                    Else
                      PictureCampo = PictureCampo & TipoPicture & "N"
                    End If
                  Next b
                End If
                If PictureCampo <> "" Then
                  tmpPicture(indOcc) = tmpPicture(indOcc) & Separatore & Separatore & PictureCampo
                End If
                PictureCampo = ""
                If Fields(a).Livello = ArrOcc(indOcc).levOccurs Then
                  ArrOcc(indOcc).NumOccurs = 0
                  ArrOcc(indOcc).levOccurs = 0
                  indOcc = IIf(indOcc > 0, indOcc - 1, indOcc)
                End If
              End If
            Else
              If tmpPicture(indOcc) <> "" Then
                For X = 1 To ArrOcc(indOcc).NumOccurs
                  tmpPic = tmpPic & tmpPicture(indOcc)
                Next X
                tmpPicture(indOcc - 1) = tmpPicture(indOcc - 1) & tmpPic
                tmpPicture(indOcc) = ""
                If Fields(a).Livello = ArrOcc(indOcc).levOccurs Then
                  ' Non serve reimpostare anche il livello, tanto � gi� uguale
                  ArrOcc(indOcc).NumOccurs = Fields(a).Occurs
                Else
                  ArrOcc(indOcc).NumOccurs = 0
                  ArrOcc(indOcc).levOccurs = 0
                  indOcc = indOcc - 1
                End If
                a = a - 1
              End If
            End If
          End If
        Else
          'Reimposto l'indice per leggere il campo successivo
          i = a - 1
          Exit For
        End If
      Next a
      Do Until indOcc = 0
        tmpPic = ""
        For X = 1 To ArrOcc(indOcc).NumOccurs
          tmpPic = tmpPic & tmpPicture(indOcc)
        Next X
        tmpPicture(indOcc - 1) = tmpPicture(indOcc - 1) & tmpPic
        tmpPicture(indOcc) = ""
        ArrOcc(indOcc).NumOccurs = 0
        ArrOcc(indOcc).levOccurs = 0
        indOcc = indOcc - 1
      Loop
      
      PictureCampo = ""
      For X = 1 To ArrOcc(indOcc).NumOccurs
        PictureCampo = PictureCampo & tmpPicture(indOcc)
        If PictureRedefines <> "" Then
          Print #numFile, Format(Start, "000000") & Format(NumByte, "000000") & PictureRedefines
          IndRedefines = IndRedefines + 1
        End If
      Next X
      PictureRedefines = ""
      ArrOcc(indOcc).NumOccurs = 0
      ArrOcc(indOcc).levOccurs = 0
      indOcc = 0
      Close numFile
      'se ha raggiunto la fine con l'ultima occurs, deve uscire anche dal ciclo generale
      If a > UBound(Fields) Then Exit For
    Else
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' CAMPI SEMPLICI
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If Fields(i).Tipo = "X" Or Fields(i).Tipo = "A" Then
        TipoPicture = "X"
        Indicatore = "N"
      Else
        If Fields(i).Segno = "S" Then
          Indicatore = "S"
        Else
          Indicatore = "N"
        End If
        If Fields(i).Usage = "ZND" Then
          TipoPicture = "9"
        ElseIf Fields(i).Usage = "BNR" Then
          TipoPicture = "B"
        Else
          TipoPicture = "P"
        End If
      End If
      PictureCampo = ""
      If (Fields(i).Lunghezza + Fields(i).Decimali) Then
        For a = 1 To Fields(i).Byte
          If Indicatore = "S" And a = Fields(i).Byte Then
            PictureCampo = PictureCampo & TipoPicture & Indicatore
          Else
            PictureCampo = PictureCampo & TipoPicture & "N"
          End If
        Next a
      End If
      If PictureCampo <> "" Then
        PictureCampo = Separatore & Separatore & PictureCampo
      End If
    End If
    CreaRigaStructure = CreaRigaStructure & PictureCampo
    PictureCampo = ""
  Next i
  CreaRigaStructure = CreaRigaStructure & PictureCampo 'serve quando l'occurs � in fondo al tracciato
  
  Exit Function
ErrorHandler:
  'Eliminazione di un file inesistente
  If Err.Number = 53 Then
    Resume Next
  Else
    Err.Raise Err.Number
  End If
End Function

'Routine che scompone il numero e mette gli zeri dopo la virgola
Function ImpostaValore(Valore As String, TipoPic As String, DecimaliPic As Integer) As String
  Dim Decimali As String, Virgola As Integer
   
  If TipoPic = "9" Or TipoPic = "P" Then
    Virgola = InStr(Valore, ",")
    If DecimaliPic > 0 And Virgola > 0 Then
      Decimali = Mid(Valore, Virgola + 1)
      Decimali = Replace(Decimali & Space(DecimaliPic - Len(Decimali)), " ", "0")
      Valore = Mid(Valore, 1, Virgola - 1) & Decimali
    End If
  End If
  'Formattazione a 66 spostata fuori per le between
  'ImpostaValore = Valore & Space(66 - Len(Valore))
  ImpostaValore = Valore
End Function

Function ConvertiOperatore(Operatore As String) As String
  Select Case Operatore
    Case "EQUAL"
      ConvertiOperatore = "="
    Case "NOT EQUAL"
      ConvertiOperatore = "^="
    Case "GREATER"
      ConvertiOperatore = ">"
    Case "GREATER EQUAL"
      ConvertiOperatore = ">="
    Case "LESS"
      ConvertiOperatore = "<"
    Case "LESS EQUAL"
      ConvertiOperatore = "<="
    Case "BETWEEN"
      ConvertiOperatore = "<>"
    Case "="
      ConvertiOperatore = "EQUAL"
    Case "^="
      ConvertiOperatore = "NOT EQUAL"
    Case ">"
      ConvertiOperatore = "GREATER"
    Case ">="
      ConvertiOperatore = "GREATER EQUAL"
    Case "<"
      ConvertiOperatore = "LESS"
    Case "<="
      ConvertiOperatore = "LESS EQUAL"
    Case "<>"
      ConvertiOperatore = "BETWEEN"
    Case Else
      ConvertiOperatore = ""
  End Select
End Function

' Mauro 21/03/2008 : Compare per eliminare le redefines inutili
Public Sub compareRedefines(IndStruct As String)
  Dim Area As Integer
  Dim Oggetto As Long, i As Long, a As Long
  Dim StartRed As Long, LenRed As Long
      
  On Error GoTo ErrorHandler
  
  Oggetto = Left(IndStruct, InStr(IndStruct, "\") - 1)
  Area = Mid(IndStruct, InStr(IndStruct, "\") + 1)
  Carica_Array Area, Oggetto
  
  generaRigheStruct
  
  ' Ciclo sulle redefines per confrontarle con la stringa della struttura base
  For i = 1 To UBound(rigaStructRed)
    StartRed = Left(rigaStructRed(i), 6)
    LenRed = Mid(rigaStructRed(i), 7, 6)
    If Mid(rigaStructBase, StartRed, LenRed) = Mid(Trim(rigaStructRed(i)), 13) Then
      For a = 1 To UBound(Fields)
        If Fields(a).NumRedefines = i Then
          Fields(a).DeleteRed = "X"
        End If
      Next a
    End If
  Next i
  Exit Sub
ErrorHandler:
  MsgBox ""
End Sub

' Mauro 21/03/2008 : Compare per eliminare le redefines inutili
Public Function generaRigheStruct()
  Dim i As Long, a As Long, t As Long
  Dim NumOccurs As Long, NumByte As Long
  Dim TipoPicture As String
  Dim Indicatore As String
  Dim PictureCampo As String, PictureOccurs As String, PictureRedefines As String
  Dim numFile, RedFile As Integer
  Dim DirFile, Start As String
  
  Dim NumRedefines As String
  Dim StringaRedefines As String
  Dim OperatoreLogico As String
  Dim Str_Red_Da As String, Str_Red_Per As String
  Dim PictureRedFile As String
  Dim rsCmpRedefines As Recordset
  Dim TipoPictureRed As String
  Dim SegnoRed As String
  Dim PosizioneRed As String, ByteRed As String
  Dim DecimaliRed As Integer
  Dim Intero As Integer
  Dim arrayBetween() As String
  Dim StringaFinale As String
  Dim addOn As Long
  Dim levOccurs As Long
  Dim primoRec As Boolean
  Dim X As Long
  Dim Separatore As String
   
  On Error GoTo ErrorHandler
  
  Separatore = Trim(m_fun.LeggiParam("EBC2ASC_SEPARATOR"))
  
  rigaStructBase = ""
  ReDim rigaStructRed(0)
  
  For i = 1 To UBound(Fields)
    TipoPicture = ""
    Indicatore = ""
    If Not (Trim(Fields(i).NomeRed) = "") Then
      'controllo se a lunghezza del campo ridefinito � uguale a lunghezza campo redefines
      addOn = 0
      Set rsCmpRedefines = m_fun.Open_Recordset("select * from MgConvData_Cmp where " & _
                                                "Nome = '" & Replace(Fields(i).NomeRed, "'", "''") & "'")
      If Not (rsCmpRedefines.EOF) Then
        If Not (rsCmpRedefines!Byte = Fields(i).Byte) Then
          If rsCmpRedefines!Byte > Fields(i).Byte Then
            MatsdF_EBCDIC2ASCII.LswLog.ListItems.Add , , Time & " - Warning: Length " & Fields(i).Nome & " greater than " & rsCmpRedefines!Nome & "! Authomatically filled up"
            addOn = rsCmpRedefines!Byte - Fields(i).Byte
          Else
            MatsdF_EBCDIC2ASCII.LswLog.ListItems.Add , , Time & " - Warning: Length " & Fields(i).Nome & " smaller than " & rsCmpRedefines!Nome & "! Not valid result"
            addOn = 0
          End If
          MatsdF_EBCDIC2ASCII.LswLog.Refresh
          MatsdF_EBCDIC2ASCII.LswLog.ListItems(MatsdF_EBCDIC2ASCII.LswLog.ListItems.Count).EnsureVisible
        End If
      End If
      rsCmpRedefines.Close
      Start = Format(Trim(Fields(i).Posizione), "000000")
      NumByte = 0
      PictureRedefines = ""
      DecimaliRed = 0
      TipoPictureRed = ""
      SegnoRed = ""
      Set rsCmpRedefines = m_fun.Open_Recordset("select * from MgConvData_Cmp where " & _
                                                "nome = '" & Fields(i).Sel_Field & "'")
      If Not (rsCmpRedefines.EOF) Then
        If rsCmpRedefines!Tipo = "X" Or rsCmpRedefines!Tipo = "A" Then
          TipoPictureRed = "X"
          SegnoRed = "N"
        Else
          If rsCmpRedefines!Usage = "ZND" Then
            TipoPictureRed = "9"
          ElseIf rsCmpRedefines!Usage = "BNR" Then
            TipoPictureRed = "B"
          Else
            TipoPictureRed = "P"
          End If
          If rsCmpRedefines!Segno = "S" Then
            SegnoRed = "S"
          Else
            SegnoRed = "N"
          End If
        End If
        DecimaliRed = rsCmpRedefines!Decimali
        PosizioneRed = Format(rsCmpRedefines!Posizione, "000000")
        ByteRed = Format(rsCmpRedefines!Byte, "000000")
      End If
      rsCmpRedefines.Close
      NumOccurs = 0
      levOccurs = 0
      primoRec = True
      For a = i To UBound(Fields)
        If Fields(a).Livello > Fields(i).Livello Or a = i Then
      ' per ora gestisce solo un livello di occurs (il cobol ne permette fino a 3)
          If Fields(a).Occurs Then
            If NumOccurs Then 'svuota l'occurs precedente
              For X = 2 To NumOccurs
                NumByte = NumByte + Len(PictureOccurs) / 2
                PictureRedefines = PictureRedefines & PictureOccurs
              Next
            End If
            primoRec = True
            NumOccurs = Fields(a).Occurs
            levOccurs = Fields(a).Livello
            PictureOccurs = ""
          Else
            primoRec = False
          End If
          If Fields(a).Tipo = "X" Or Fields(a).Tipo = "A" Then
            TipoPicture = "X"
            Indicatore = "N"
          Else
            If Fields(a).Usage = "ZND" Then
              TipoPicture = "9"
            ElseIf Fields(a).Usage = "BNR" Then
              TipoPicture = "B"
            Else
              TipoPicture = "P"
            End If
            If Fields(a).Segno = "S" Then
              Indicatore = "S"
            Else
              Indicatore = "N"
            End If
          End If
          If (Fields(a).Lunghezza + Fields(a).Decimali) Then
            For t = 1 To Fields(a).Byte
              Intero = Fields(a).Byte - Fields(a).Decimali
              'Indicatore = "S" solo per l'ultimo byte dei campi S9 ZND
              ' stefano: cambiato indicatore deve poter essere "S" anche per tutti gli altri
              ' poi decide ITERCONV se gli interessa o meno, non capisco perch� dare un'informazione sbagliata
'              If Indicatore = "S" And t = Intero Then
'                PictureRedefines = PictureRedefines & TipoPicture & Indicatore
'              Else
'                PictureRedefines = PictureRedefines & TipoPicture & "N"
'              End If
              'stefano: intanto salva per l'occurs nella redefine
              If (Fields(a).Livello > levOccurs Or primoRec) And NumOccurs Then
                 PictureOccurs = PictureOccurs & TipoPicture & Indicatore
              End If
              PictureRedefines = PictureRedefines & TipoPicture & Indicatore
              NumByte = NumByte + 1
            Next t
          End If
          If Fields(a).Livello <= levOccurs And Not primoRec Then
            If NumOccurs Then
              For X = 2 To NumOccurs
                NumByte = NumByte + Len(PictureOccurs) / 2
                PictureRedefines = PictureRedefines & PictureOccurs
              Next
              PictureOccurs = ""
              NumOccurs = 0
              levOccurs = 0
            End If
          End If
        Else
          If NumOccurs Then 'svuota l'occurs precedente
            For X = 2 To NumOccurs
              NumByte = NumByte + Len(PictureOccurs) / 2
              PictureRedefines = PictureRedefines & PictureOccurs
            Next
            NumOccurs = 0
            PictureOccurs = ""
          End If
          'Reimposto l'indice per leggere il campo successivo
          i = a - 1
          Exit For
        End If
      Next a
      If NumOccurs Then 'svuota l'occurs precedente
        For X = 2 To NumOccurs
          NumByte = NumByte + Len(PictureOccurs) / 2
          PictureRedefines = PictureRedefines & PictureOccurs
        Next
        NumOccurs = 0
        PictureOccurs = ""
      End If
      'riempie la struttura della redefines fino al valore di quella base
      For t = 1 To addOn
        PictureRedefines = PictureRedefines & "XN"
      Next
      NumByte = NumByte + addOn
      
      ReDim Preserve rigaStructRed(UBound(rigaStructRed) + 1)
      rigaStructRed(UBound(rigaStructRed)) = Start & Format(NumByte, "000000") & PictureRedefines
      
      Close numFile
      PictureCampo = ""
      'se ha raggiunto la fine con l'ultima redefines, deve uscire anche dal ciclo generale
      If a >= UBound(Fields) Then Exit For
    ElseIf Fields(i).Occurs Then
      NumOccurs = Fields(i).Occurs
      PictureOccurs = ""
      For a = i To UBound(Fields)
        If Fields(a).Livello > Fields(i).Livello Or a = i Then
          If Fields(a).Tipo = "X" Or Fields(a).Tipo = "A" Then
            TipoPicture = "X"
            Indicatore = "N"
          Else
            If Fields(a).Usage = "ZND" Then
              TipoPicture = "9"
            ElseIf Fields(a).Usage = "BNR" Then
              TipoPicture = "B"
            Else
              TipoPicture = "P"
            End If
          End If
          If Fields(a).Segno = "S" Then
            Indicatore = "S"
          Else
            Indicatore = "N"
          End If
          If (Fields(a).Lunghezza + Fields(a).Decimali) Then
            For t = 1 To Fields(a).Byte
              Intero = Fields(a).Byte - Fields(a).Decimali
              'Indicatore = "S" solo per l'ultimo byte dei campi S9 ZND
              If Indicatore = "S" And t = Intero Then
                PictureOccurs = PictureOccurs & TipoPicture & Indicatore
              Else
                PictureOccurs = PictureOccurs & TipoPicture & "N"
              End If
            Next t
          End If
        Else
          'Reimposto l'indice per leggere il campo successivo
          i = a - 1
          Exit For
        End If
      Next a
      PictureCampo = ""
      For X = 1 To NumOccurs
        PictureCampo = PictureCampo & PictureOccurs
      Next X
      'se ha raggiunto la fine con l'ultima occurs, deve uscire anche dal ciclo generale
      If a >= UBound(Fields) Then Exit For
    Else
      If Fields(i).Tipo = "X" Or Fields(i).Tipo = "A" Then
        TipoPicture = "X"
        Indicatore = "N"
      Else
        If Fields(i).Segno = "S" Then
          Indicatore = "S"
        Else
          Indicatore = "N"
        End If
        If Fields(i).Usage = "ZND" Then
          TipoPicture = "9"
        ElseIf Fields(i).Usage = "BNR" Then
          TipoPicture = "B"
        Else
          TipoPicture = "P"
        End If
      End If
      PictureCampo = ""
      If (Fields(i).Lunghezza + Fields(i).Decimali) Then
        For a = 1 To Fields(i).Byte
          'Indicatore = "S" solo per l'ultimo byte dei campi S9 ZND
          Intero = Fields(i).Byte - Fields(i).Decimali
          If Indicatore = "S" And a = Intero Then
            PictureCampo = PictureCampo & TipoPicture & Indicatore
          Else
            PictureCampo = PictureCampo & TipoPicture & "N"
          End If
        Next a
      End If
    End If
    rigaStructBase = rigaStructBase & PictureCampo
    PictureCampo = ""
  Next i
  rigaStructBase = rigaStructBase & PictureCampo 'serve quando l'occurs � in fondo al tracciato
  
  Exit Function
ErrorHandler:
  'Eliminazione di un file inesistente
  If Err.Number = 53 Then
    Resume Next
  Else
    Err.Raise Err.Number
  End If
End Function

Public Sub Seleziona_Filtro_Selezione(Lista As ListView, Filter As MSFlexGrid, Stato As Boolean)
  If Stato Then
    Lista.Visible = False
    Filter.Visible = True
    Filter.Height = 915
    MatsdF_EBCDIC2ASCII.cmdFilterExe.Enabled = True
  Else
    Lista.Visible = True
    Filter.Visible = False
    MatsdF_EBCDIC2ASCII.cmdFilterExe.Enabled = False
    MatsdF_EBCDIC2ASCII.txtFilter.Visible = False
    'ricarica la lista oggetti precedente
    'MabseF_List.TotalObj.Text = Carica_Lista_Oggetti(Lista, SQL_Selection_List, MabseF_List.PBar)
  End If

  'imposta la riga di intestazione
  Filter.Row = 0
  'imposta le colonne
  Filter.Col = 0
  Filter.text = "File"
  Filter.ColWidth(0) = MatsdF_EBCDIC2ASCII.lswStructSel.ColumnHeaders(1).Width
  
  Filter.Col = 1
  Filter.text = "Associated Area"
  Filter.ColWidth(1) = MatsdF_EBCDIC2ASCII.lswStructSel.ColumnHeaders(2).Width
End Sub

Public Sub Execute_Filter(list As MSFlexGrid, LObj As ListView, Stato As Boolean)
  Dim Prop() As String, sql As String, AppStr As String
  Dim cont As Long, i As Long, FinalS As String
  
  Screen.MousePointer = vbHourglass
  If Not Stato Then
    isFilterExe = False
    MatsdF_EBCDIC2ASCII.cmdFilterExe.Enabled = False
    Carica_Lista_Structure
  Else
    sql = ""
    cont = 0
    AppStr = ""
    FinalS = ""
    
    ReDim Prop(0)
    'memorizza le variabili di query
    list.Row = 1
    For i = 0 To list.Cols - 1
      list.Col = i
      ReDim Preserve Prop(UBound(Prop) + 1)
      Prop(UBound(Prop)) = list.text
    Next i
    
    'crea la query
    For i = 1 To UBound(Prop)
      If Prop(i) <> "" Then
        Prop(i) = Replace(Prop(i), "*", "%")
        Select Case i
          Case 1
            If InStr(1, Prop(i), "%") <> 0 Then
              sql = "Filename Like '" & Prop(i) & "'"
            Else
              sql = "Filename = '" & Prop(i) & "'"
            End If
            cont = cont + 1
            
          Case 2
            If InStr(1, Prop(i), "%") <> 0 Then
              sql = "Nome Like '" & Prop(i) & "'"
            Else
              sql = "Nome = '" & Prop(i) & "'"
            End If
            cont = cont + 1
        End Select
        
        If sql <> "" Then
          If cont > 1 Then
            AppStr = " And " & sql
          Else
            AppStr = sql
          End If
        End If
        
        FinalS = FinalS & AppStr
        AppStr = ""
        sql = ""
      End If
    Next i
    If FinalS <> "" Then
      FinalS = "select IdOggetto, IdArea, Nome, FileName from MgConvData_Area Where " & FinalS & " order by filename"
    End If
    
    'visualizza la lista oggetti
    list.Visible = True
    
    'disabilita il bottone di filtro
    isFilterConf = False
    isFilterExe = True
    MatsdF_EBCDIC2ASCII.txtFilter.Visible = False
    
    LObj.Visible = True
    
    'carica la lista oggetti
    Carica_Lista_Structure FinalS
    FinalS = ""
  End If
  Screen.MousePointer = vbDefault
End Sub
