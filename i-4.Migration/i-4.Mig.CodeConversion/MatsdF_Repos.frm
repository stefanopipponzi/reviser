VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form MatsdF_Repos 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Check Repository Structure"
   ClientHeight    =   6600
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   10200
   Icon            =   "MatsdF_Repos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6600
   ScaleWidth      =   10200
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame3 
      Caption         =   "Error Log"
      ForeColor       =   &H00000080&
      Height          =   1725
      Left            =   120
      TabIndex        =   0
      Top             =   4740
      Width           =   9945
      Begin RichTextLib.RichTextBox RTErr 
         Height          =   1305
         Left            =   150
         TabIndex        =   1
         Top             =   270
         Width           =   9645
         _ExtentX        =   17013
         _ExtentY        =   2302
         _Version        =   393217
         BackColor       =   -2147483624
         Enabled         =   -1  'True
         TextRTF         =   $"MatsdF_Repos.frx":0442
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   10200
      _ExtentX        =   17992
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Attiva"
            Object.ToolTipText     =   "Start Analysis"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "PrintLog"
            Object.ToolTipText     =   "Print Error Log"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   1
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Help"
            Object.ToolTipText     =   "Help"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Cancella"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Aggiorna"
            Object.ToolTipText     =   "Update Macro/System Print"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Repos.frx":04C4
            Key             =   "Attiva"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Repos.frx":07DE
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Repos.frx":08F0
            Key             =   "Printer"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Repos.frx":0C14
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Repos.frx":0D6E
            Key             =   "Copy"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4155
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   7329
      _Version        =   393216
      TabOrientation  =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Repository"
      TabPicture(0)   =   "MatsdF_Repos.frx":0EC8
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Macro"
      TabPicture(1)   =   "MatsdF_Repos.frx":0EE4
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame7"
      Tab(1).Control(1)=   "Frame6"
      Tab(1).Control(2)=   "Label1"
      Tab(1).ControlCount=   3
      Begin VB.Frame Frame7 
         Caption         =   "System Macro"
         ForeColor       =   &H00000080&
         Height          =   3255
         Left            =   -69720
         TabIndex        =   11
         Top             =   360
         Width           =   4455
         Begin MSComctlLib.ListView lswmacrosys 
            Height          =   2835
            Left            =   150
            TabIndex        =   12
            Top             =   240
            Width           =   4125
            _ExtentX        =   7276
            _ExtentY        =   5001
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            Checkboxes      =   -1  'True
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Descrizione"
               Object.Width           =   5292
            EndProperty
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Project Macro"
         ForeColor       =   &H00000080&
         Height          =   3255
         Left            =   -74880
         TabIndex        =   9
         Top             =   360
         Width           =   4935
         Begin MSComctlLib.ListView lswmacro 
            Height          =   2775
            Left            =   150
            TabIndex        =   10
            Top             =   300
            Width           =   4575
            _ExtentX        =   8070
            _ExtentY        =   4895
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            Checkboxes      =   -1  'True
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   -2147483624
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Descrizione"
               Object.Width           =   5292
            EndProperty
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Activity Log"
         ForeColor       =   &H00000080&
         Height          =   3435
         Left            =   3840
         TabIndex        =   5
         Top             =   240
         Width           =   5865
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   150
            TabIndex        =   6
            Top             =   3120
            Width           =   5535
            _ExtentX        =   9763
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Min             =   1e-4
            Scrolling       =   1
         End
         Begin MSComctlLib.ListView LvObject 
            Height          =   2835
            Left            =   150
            TabIndex        =   7
            Top             =   270
            Width           =   5535
            _ExtentX        =   9763
            _ExtentY        =   5001
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "ID"
               Object.Width           =   1411
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Object"
               Object.Width           =   3175
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Type"
               Object.Width           =   1236
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Cod. Errore"
               Object.Width           =   1766
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Descrizione Errore"
               Object.Width           =   4410
            EndProperty
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Select Object for Analysis"
         ForeColor       =   &H00000080&
         Height          =   3435
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   3435
         Begin MSComctlLib.TreeView TvSelObj 
            Height          =   2955
            Left            =   150
            TabIndex        =   8
            Top             =   300
            Width           =   3105
            _ExtentX        =   5477
            _ExtentY        =   5212
            _Version        =   393217
            Indentation     =   529
            LineStyle       =   1
            Style           =   7
            Checkboxes      =   -1  'True
            Appearance      =   1
         End
      End
      Begin VB.Label Label1 
         Caption         =   "Check Macro of Project to insert in System"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   -74820
         TabIndex        =   13
         Top             =   90
         Width           =   4815
      End
   End
End
Attribute VB_Name = "MatsdF_Repos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim IdOggetto As Long
Dim SelTipo As String

Private Sub Form_Load()
  NumFeatures = 1
  NumObjInMod = 0
  SSTab1.Tab = 0
      
  CaricaTrw
  
  SSTab1.TabEnabled(1) = m_fun.Check_If_License_Update
  Toolbar1.Buttons("Cancella").Enabled = m_fun.Check_If_License_Update
  Toolbar1.Buttons("Aggiorna").Enabled = m_fun.Check_If_License_Update
 
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Sub CaricaTrw()
  TVSelObj.Nodes.Clear
  TVSelObj.Nodes.Add , , "SRC", "Sources"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CPY", "Cobol Copy"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CBL", "Cobol Program"
  TVSelObj.Nodes.Add "SRC", tvwChild, "BMS", "BMS Screen"
  TVSelObj.Nodes.Add "SRC", tvwChild, "PSB", "Segment Psb"
  TVSelObj.Nodes.Add "SRC", tvwChild, "MAC", "Macro"
  TVSelObj.Nodes.Add "SRC", tvwChild, "PRC", "Procedure"
  
  TVSelObj.Nodes.Add , , "TBL", "Table RDBMS"
  TVSelObj.Nodes.Add "TBL", tvwChild, "DB2", "Table DB2"
  TVSelObj.Nodes.Add "TBL", tvwChild, "ORC", "Table Oracle"
  
  TVSelObj.Nodes.Add , , "DLI", "DLI Components"
  TVSelObj.Nodes.Add "DLI", tvwChild, "DBD", "DLI DBD"

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
   
  If m_fun.FnProcessRunning Then
    wScelta = m_fun.Show_MsgBoxError("FB01I")
     
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
End Sub

Private Sub LvObject_ItemClick(ByVal Item As MSComctlLib.ListItem)
  Toolbar1.Buttons.Item(6).Enabled = True
  IdOggetto = Format(LvObject.SelectedItem, 0)
  SelTipo = LvObject.SelectedItem.ListSubItems(2).text
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  Select Case SSTab1.Tab
    Case 0
      CaricaTrw
    Case 1
      Carica_Lista_Macro lswmacro
      Carica_Lista_MacroSys lswmacrosys
  End Select
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim K1 As Integer
  
  Screen.MousePointer = vbHourglass
  m_fun.FnProcessRunning = True
   
  Select Case SSTab1.Tab
    Case 0
      Select Case Button.Key
        Case "Attiva"
          MatsdF_Repos.RTErr.text = ""
          MatsdF_Repos.RTErr.Refresh
          LvObject.ListItems.Clear
          For K1 = 1 To TVSelObj.Nodes.Count
            If TVSelObj.Nodes(K1).Checked Then
              CheckRepository TVSelObj.Nodes(K1).Key
            End If
          Next K1
        Case "Cancella"
          CancellaRepository SelTipo
      End Select
    Case 1
      Select Case Button.Key
        Case "Aggiorna"
          For K1 = 1 To lswmacro.ListItems.Count
            If lswmacro.ListItems(K1).Checked Then
              AggiornaSystemMacro lswmacro.ListItems(K1).Key
            End If
          Next K1
          Carica_Lista_Macro lswmacro
          Carica_Lista_MacroSys lswmacrosys
      End Select
  End Select
  
  Screen.MousePointer = vbNormal
  m_fun.FnProcessRunning = False
End Sub

Sub CancellaRepository(Tipo As String)
  Dim i As Long
  Dim wSql As String, wSql1 As String
  Dim Scelta As Long
  
  If Tipo = "MPS" Then
    Tipo = "BMS"
  End If
  Scelta = MsgBox("Selected Object will be erased... Are you sure? ", vbYesNo, Me.Caption)
  If Scelta = vbYes Then
    For i = LvObject.ListItems.Count To 1 Step -1
      Clessidra True
      If LvObject.ListItems(i).Selected = True Then
        Select Case Tipo
          Case "CPY", "BMS", "PSB", "PRC", "MAC", "CBL"
            wSql = "Delete from bs_oggetti where idoggetto = " & IdOggetto & " ; "
          Case "DB2"
            wSql = "Delete from DMDB2_Tabelle Where idtable = " & IdOggetto & " ; "
          Case "ORC"
            wSql = "Delete from DMORC_Tabelle Where idtable = " & IdOggetto & " ; "
          Case "DBD"
            wSql = "Delete from PsDli_Segmenti Where IdSegmento = " & IdOggetto & " ; "
        End Select
        wSql1 = "Delete from bs_segnalazioniprj where idoggetto = " & IdOggetto & " ; "
                 
        Menu.TsConnection.Execute wSql
        Menu.TsConnection.Execute wSql1
        'cancella dal video
        LvObject.ListItems.Remove (i)
      End If
      Clessidra False
    Next i
  End If
  Toolbar1.Buttons.Item(6).Enabled = False
End Sub

Sub AggiornaSystemMacro(StrKey As String)
  Dim Id As String, Nome As String
  Dim rs As Recordset, rp As Recordset
  Dim i As Integer
  
  On Error GoTo errorHandler
  i = InStr(1, StrKey, "-")
  If i <> 0 Then
    Id = Mid(StrKey, 1, i - 1)
    Nome = Mid(StrKey, i + 1)
  End If
  'Copia i record dal progetto a System
  Set rs = m_fun.Open_Recordset("Select * From TS_Macro where IdMacro = " & Id & " and Nome = '" & Nome & "'")
  If rs.RecordCount Then
    Set rp = m_fun.Open_Recordset_Sys("Select * From SysMacro Where Id = " & Id & " and Nome = '" & Nome & "'")
    If rp.RecordCount = 0 Then
      rp.AddNew
                     
      rp!Id = rs!IdMacro
      rp!Nome = rs!Nome
      rp!Sintax = rs!Sintax
    Else
      rp!Nome = rs!Nome
      rp!Syntax = rs!Syntax
    End If
    rp.Update
    rp.Close
  End If
  rs.Close
  Exit Sub
errorHandler:
  
End Sub

Sub CheckRepository(Tipo As String)
  ProgressBar1.Value = 1
  If Tipo = "MPS" Then
    Tipo = "BMS"
  End If
  
  Select Case Tipo
    Case "CPY", "BMS", "PSB", "PRC", "MAC"
      CheckSource Tipo
    Case "DDL"
      CheckTable Tipo, "DB2"
      CheckTable Tipo, "ORC"
    Case "DLI"
      CheckSegmento "DBD"
  End Select
End Sub

Sub CheckSource(Tipo As String)
  Dim TbO As Recordset, TbR As Recordset, TbS As Recordset
  Dim Codice As String
  
  Select Case Tipo
    Case "CPY"
      Codice = "E00"
    Case "BMS"
      Codice = "E01"
    Case "PSB"
      Codice = "E04"
    Case "PRC"
      Codice = "E05"
    Case "CBL"
      Codice = "???"
    Case "MAC"
      Codice = "E06"
  End Select
  
  Set TbO = m_fun.Open_Recordset("select * from bs_oggetti where tipo = '" & Tipo & "'")
  If TbO.RecordCount Then
    ProgressBar1.Max = TbO.RecordCount + 1
    While Not TbO.EOF
      Set TbR = m_fun.Open_Recordset("select * from psrel_obj where idoggettor = " & TbO!IdOggetto)
      If TbR.RecordCount = 0 Then
        Set TbS = m_fun.Open_Recordset("Select * from bs_segnalazioniprj where " & _
                                       "idoggetto = " & TbO!IdOggetto & " and Codice = '" & Codice & "' ")
        If TbS.RecordCount = 0 Then
          m_fun.FnConnection.Execute "INSERT INTO BS_SegnalazioniPrj (IdOggetto, Codice, Origine, Gravita, Var1) VALUES " & _
                                     "(" & TbO!IdOggetto & ", '" & Codice & "', 'TS', 'I', '" & TbO!Nome & "')"
        End If
        TbS.Close
        With LvObject.ListItems.Add(, , Format(TbO!IdOggetto, "000000"))
          .SubItems(1) = TbO!Nome
          .SubItems(2) = TbO!Tipo
          .SubItems(3) = Codice
          .SubItems(4) = Componi_Messaggio_Errore(LvObject, TbO!IdOggetto, TbO!Nome, Codice)
          ProgressBar1.Value = ProgressBar1.Value + 1
          ProgressBar1.Refresh
          .EnsureVisible
          LvObject.Refresh
        End With
        MatsdF_Repos.RTErr.SelText = Tipo & ": " & TbO!directory_input & "\" & TbO!Nome! & " non referenziata" & vbCrLf
        MatsdF_Repos.RTErr.Refresh
      End If
      TbR.Close
      TbO.MoveNext
    Wend
  End If
  TbO.Close
End Sub

Sub CheckTable(Tipo As String, TipoDB As String)
  Dim strquery As String
  Dim TbO As Recordset, TbR As Recordset, TbS As Recordset
  Dim Codice As String
      
  Select Case TipoDB
    Case "DB2"
      Codice = "E02"
      strquery = "Select * From DMDB2_Tabelle Where IdOrigine = -1"
    Case "ORC"
      Codice = "E03"
      strquery = "Select * From DMORC_Tabelle Where IdOrigine = -1"
  End Select
      
  Set TbO = m_fun.Open_Recordset(strquery)
  If TbO.RecordCount Then
    ProgressBar1.Max = TbO.RecordCount + 1
    TbO.MoveFirst
    While Not TbO.EOF
      IdOggetto = TbO!IdTable
      Set TbR = m_fun.Open_Recordset("Select * From PSCom_Obj Where NomeOggettoR = '" & TbO!Nome & "' And Utilizzo = 'SQL' And Relazione = 'TBL'")
      If TbR.RecordCount = 0 Then
        strquery = "Select * from bs_segnalazioniprj where idoggetto = " & TbO!IdOggetto & " and Codice = '" & Codice & "' and Riga = 1"
        Set TbS = m_fun.Open_Recordset(strquery)
        If TbS.RecordCount = 0 Then
          strquery = "INSERT INTO BS_SegnalazioniPrj(IdOggetto, Riga, Codice, Origine, Gravita, Var1) VALUES (" & TbO!IdOggetto & " ,1, '" & Codice & "', 'TS', 'I', '" & TbO!Nome & "')"
          m_fun.FnConnection.Execute strquery  '"INSERT INTO BS_SegnalazioniPrj (IdOggetto , Codice, Origine, Gravita,  Var1) VALUES ( " & TbO!IdOggetto & " , 'E00', 'TS', 'I', '" & TbO!Nome & "')"
        End If
        TbS.Close
        With LvObject.ListItems.Add(, , Format(TbO!IdTable, "000000"))
          .SubItems(1) = TbO!Nome
          .SubItems(2) = Tipo
          .SubItems(3) = Codice
          .SubItems(4) = Componi_Messaggio_Errore(LvObject, TbO!IdTable, TbO!Nome, "E02")
          ProgressBar1.Value = ProgressBar1.Value + 1
          ProgressBar1.Refresh
          .EnsureVisible
          LvObject.Refresh
        End With
        MatsdF_Repos.RTErr.SelText = Tipo & ": " & TbO!Nome! & " non referenziata" & vbCrLf
        MatsdF_Repos.RTErr.Refresh
      End If
      TbR.Close
      TbO.MoveNext
    Wend
  End If
  TbO.Close
End Sub

Sub CheckSegmento(Tipo As String)
  Dim strquery As String
  Dim TbO As Recordset, TbR As Recordset, TbS As Recordset
  Dim Codice As String, Var1 As String
  
  On Error Resume Next
  Codice = "E07"
  strquery = "select * from PsDli_Segmenti"
  Set TbO = m_fun.Open_Recordset(strquery)
  If TbO.RecordCount > 0 Then
    ProgressBar1.Max = TbO.RecordCount + 1
    TbO.MoveFirst
    While Not TbO.EOF
      'IdOggetto = TbO!IdOggetto
      Set TbR = m_fun.Open_Recordset("select * from PsDli_IstrDett_Liv where IdSegmento = " & TbO!IdSegmento)
      If TbR.RecordCount = 0 Then
        strquery = "Select * from bs_segnalazioniprj where idoggetto = " & TbO!IdOggetto & " and Codice = '" & Codice & "' and var1 = '" & Var1 & "' "
        Set TbS = m_fun.Open_Recordset(strquery)
        If TbS.RecordCount = 0 Then
          strquery = "INSERT INTO BS_SegnalazioniPrj(IdOggetto,  Codice, Origine, Gravita, Var1) VALUES ( " & TbO!IdOggetto & " , '" & Codice & "', 'TS', 'I', '" & TbO!Nome & "')"
          m_fun.FnConnection.Execute strquery  '"INSERT INTO BS_SegnalazioniPrj (IdOggetto , Codice, Origine, Gravita,  Var1) VALUES ( " & TbO!IdOggetto & " , 'E00', 'TS', 'I', '" & TbO!Nome & "')"
        End If
        TbS.Close
        With LvObject.ListItems.Add(, , Format(TbO!IdSegmento, "000000"))
          .SubItems(1) = TbO!Nome
          .SubItems(2) = Tipo
          .SubItems(3) = Codice
          .SubItems(4) = Componi_Messaggio_Errore(LvObject, TbO!IdSegmento, TbO!Nome, Codice)
          ProgressBar1.Value = ProgressBar1.Value + 1
          ProgressBar1.Refresh
          .EnsureVisible
          LvObject.Refresh
        End With
        MatsdF_Repos.RTErr.SelText = Tipo & ": " & TbO!Nome! & " non referenziata" & vbCrLf
        MatsdF_Repos.RTErr.Refresh
      End If
      TbR.Close
      TbO.MoveNext
    Wend
  End If
  TbO.Close
End Sub
      
Private Sub TVSelObj_NodeCheck(ByVal Node As MSComctlLib.Node)
  Dim K1 As Integer
  Dim wKey As String
  Dim wCheck As Variant
  
  wKey = Node.Key
  wCheck = Node.Checked
  
  NumObjInMod = 0
  Select Case Node.Key
    Case "SRC", "DLI", "TBL"
      If Node.Checked Then
        For K1 = Node.index + 1 To Node.index + Node.Children
          TVSelObj.Nodes(K1).Checked = True
        Next K1
      End If
      If Not Node.Checked Then
        For K1 = Node.index + 1 To Node.index + Node.Children
          TVSelObj.Nodes(K1).Checked = False
        Next K1
      End If
  End Select
End Sub

Sub Clessidra(Status As Boolean)
  If Status Then
    Screen.MousePointer = vbHourglass
    Exit Sub
  End If
  
  If Not Status Then
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
End Sub

Function Componi_Messaggio_Errore(Lsw As ListView, IdOggetto As Long, Nome As String, Codice As String) As String
  Dim r As Recordset, R1 As Recordset
  Dim Var1 As Variant, Var2 As Variant, Var3 As Variant, Var4 As Variant, Var5 As Variant
  Dim Testo As String

  Dim Sx As String
  Dim Dx As String

  Dim i As Long
  Dim bool As Boolean
  Dim Icona As Long
  
  Set r = m_fun.Open_Recordset("Select * From BS_SegnalazioniPrj Where IdOggetto = " & IdOggetto & " and Codice = '" & Codice & "' ")  'and Riga = 1")
  If r.RecordCount Then
    Set R1 = m_fun.Open_Recordset_Sys("Select * From BS_Messaggi Where Codice = '" & r!Codice & "'")
    If R1.RecordCount Then
      'deve essere solo uno
      Testo = R1!Testo
    End If
    R1.Close
    'prende le variabili Var<x>
    Var1 = Trim(r!Var1)
    Var2 = Trim(r!Var2)
    Var3 = Trim(r!Var3)
    Var4 = Trim(r!Var4)
    Var5 = Trim(r!Var5)
    
    'Sostituisce Var1
    i = InStr(1, Testo, "%VAR1%")
    If i <> 0 Then
      Sx = Mid(Testo, 1, i - 2)
      Dx = Mid(Testo, i + 7)
      Testo = Sx & " " & Var1 & " " & Dx
    End If
    
    'Sostituisce Var2
    i = InStr(1, Testo, "%VAR2%")
    If i <> 0 Then
      Sx = Mid(Testo, 1, i - 2)
      Dx = Mid(Testo, i + 7)
      Testo = Sx & " " & Var2 & " " & Dx
    End If
    
    'Sostituisce Var3
    i = InStr(1, Testo, "%VAR3%")
    If i <> 0 Then
      Sx = Mid(Testo, 1, i - 2)
      Dx = Mid(Testo, i + 7)
      Testo = Sx & " " & Var3 & " " & Dx
    End If
    
    'Sostituisce Var4
    i = InStr(1, Testo, "%VAR4%")
    If i <> 0 Then
      Sx = Mid(Testo, 1, i - 2)
      Dx = Mid(Testo, i + 7)
      Testo = Sx & " " & Var4 & " " & Dx
    End If
    
    'Sostituisce Var5
    i = InStr(1, Testo, "%VAR5%")
    If i <> 0 Then
      Sx = Mid(Testo, 1, i - 2)
      Dx = Mid(Testo, i + 7)
      Testo = Sx & " " & Var5 & " " & Dx
    End If
  End If
  r.Close
  Componi_Messaggio_Errore = Trim(Testo)
End Function

Sub Carica_Lista_Macro(Lsw As ListView)
  Dim rs As Recordset
  
  On Error GoTo ErrH
  
  Lsw.ListItems.Clear
  Set rs = m_fun.Open_Recordset("SELECT * FROM TS_MACRO Order By Nome")
  While Not rs.EOF
    Lsw.ListItems.Add , , Trim(rs!Nome)
    Lsw.ListItems(Lsw.ListItems.Count).Key = rs!IdMacro & "-" & Trim(rs!Nome)
    Lsw.ListItems(Lsw.ListItems.Count).Checked = True
    rs.MoveNext
  Wend
  rs.Close
  Exit Sub
ErrH:
  Exit Sub
End Sub

Sub Carica_Lista_MacroSys(Lsw As ListView)
  Dim rs As Recordset
   
  On Error GoTo ErrH
  
  Lsw.ListItems.Clear
  Set rs = m_fun.Open_Recordset_Sys("SELECT * FROM SysMacro Order By Nome")
  While Not rs.EOF
    Lsw.ListItems.Add , , Trim(rs!Nome)
    Lsw.ListItems(Lsw.ListItems.Count).Key = rs!Id & "-" & Trim(rs!Nome)
    Lsw.ListItems(Lsw.ListItems.Count).Checked = True
    rs.MoveNext
  Wend
  rs.Close
  Exit Sub
ErrH:
  Exit Sub
End Sub
