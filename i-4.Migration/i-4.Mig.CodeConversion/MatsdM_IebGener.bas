Attribute VB_Name = "MAtsdM_IebGener"
Option Explicit
Global DefTypeObj As String
Global oldSelectType As Integer
Public ebcdic() As Variant
Public NumLog As Integer
Public fso As New FileSystemObject

Public Function CaricaArchivio(NomeArchivio) As String
  Dim k As Integer
  Dim wTesto As String, wRec As String
  Dim NomefileI As String, NumFileI As Integer
  
  NomefileI = NomeArchivio
  NumFileI = FreeFile
  
  Open NomefileI For Binary As NumFileI
  
  wTesto = ""
  If FileLen(NomefileI) Then
    For k = 1 To 300
      Line Input #NumFileI, wRec
      If Loc(NumFileI) = FileLen(NomefileI) Then Exit For
      wTesto = wTesto & wRec & vbCrLf
    Next k
    If k > 300 Then
      wTesto = wTesto & "..."
    Else
      wTesto = wTesto & wRec
    End If
  End If
  Close #NumFileI
  CaricaArchivio = wTesto
End Function

Sub Crea_Directory(NewDir As String)
  Dim wStr1 As String
  Dim k As Integer
  
  wStr1 = ""
  For k = 1 To Len(NewDir)
    If Mid$(NewDir, k, 1) = "\" Then
      If Mid$(wStr1, 1, 2) <> "\\" Then
         If Dir(wStr1, vbDirectory) = "" Then
           m_fun.MkDir_API wStr1
         End If
      End If
    End If
    wStr1 = wStr1 & Mid$(NewDir, k, 1)
  Next k
  If Dir(NewDir, vbDirectory) = "" Then
    m_fun.MkDir_API NewDir
  End If
End Sub

Sub CatalogaFile(NomeFile As String, DirectoryOut As String, NomeObj As String)
  Dim wTypeObj As String
  Dim wDir As String
  Dim wFileGen As String
  
  'SQ
  'wTypeObj = m_fun.SearchObjType(NomeFile)
  'Select Case wTypeObj
  'Alla faccia delle procedure automatiche
  
  Select Case DefTypeObj
    Case "CBL"
      wDir = DirectoryOut & "\CBL"
      wFileGen = wDir & "\" & NomeObj & ".CBL"
    Case "CPY"
      wDir = DirectoryOut & "\CPY"
      wFileGen = wDir & "\" & NomeObj & ".CPY"
    Case "BMS"
      wDir = DirectoryOut & "\BMS"
      wFileGen = wDir & "\" & NomeObj & ".BMS"
    Case "MFS"
      wDir = DirectoryOut & "\MFS"
      wFileGen = wDir & "\" & NomeObj & ".MFS"
    Case "PSB"
      wDir = DirectoryOut & "\PSB"
      wFileGen = wDir & "\" & NomeObj & ".PSB"
    Case "JCL"
      wDir = DirectoryOut & "\JCL"
      wFileGen = wDir & "\" & NomeObj & ".JCL"
    Case "JMB"
      wDir = DirectoryOut & "\JCL"
      wFileGen = wDir & "\" & NomeObj & ".JMB"
    Case "JMX"
      wDir = DirectoryOut & "\JCL"
      wFileGen = wDir & "\" & NomeObj & ".JMX"
    Case "JOB"
      wDir = DirectoryOut & "\JCL"
      wFileGen = wDir & "\" & NomeObj & ".JOB"
    Case "PRM"
      wDir = DirectoryOut & "\PRM"
      wFileGen = wDir & "\" & NomeObj & ".PRM"
    Case "PRC"
      wDir = DirectoryOut & "\PRC"
      wFileGen = wDir & "\" & NomeObj & ".PRC"
    Case "ASM"
      wDir = DirectoryOut & "\ASM"
      wFileGen = wDir & "\" & NomeObj & ".ASM"
    Case "MAC"
      wDir = DirectoryOut & "\MAC"
      wFileGen = wDir & "\" & NomeObj & ".MAC"
    Case "DBD"
      wDir = DirectoryOut & "\DBD"
      wFileGen = wDir & "\" & NomeObj & ".DBD"
    Case "DDL"
      wDir = DirectoryOut & "\DDL"
      wFileGen = wDir & "\" & NomeObj & ".DDL"
    Case "PPT"
      wDir = DirectoryOut & "\CX"
      wFileGen = wDir & "\" & NomeObj & ".PPT"
    Case "PCT"
      wDir = DirectoryOut & "\CX"
      wFileGen = wDir & "\" & NomeObj & ".PCT"
    Case "FCT"
      wDir = DirectoryOut & "\CX"
      wFileGen = wDir & "\" & NomeObj & ".FCT"
    Case "EZT"
      wDir = DirectoryOut & "\EZT"
      wFileGen = wDir & "\" & NomeObj & ".EZT"
    Case "EZM"
      wDir = DirectoryOut & "\EZM"
      wFileGen = wDir & "\" & NomeObj & ".EZM"
    Case Else
      wDir = DirectoryOut             'SQ & "\" & DefTypeObj
      wFileGen = wDir & "\" & NomeObj 'SQ & "." & DefTypeObj
  End Select
  
  Crea_Directory wDir
  FileCopy NomeFile, wFileGen
  Kill NomeFile
  MatsdF_IebGener.AddLogEib "Objects selected from Import: " & wFileGen
End Sub

Function ascii_to_ebcdic(ByVal buffer As String) As String
  Dim ascii As Variant
  Dim i As Long, bufferlen As Long
    
  ascii = Array( _
  &H0, &H1, &H2, &H3, &H37, &H2D, &H2E, &H2F, &H16, &H5, &H25, &HB, &HC, &HD, &HE, &HF, _
  &H10, &H11, &H12, &H13, &H3C, &H3D, &H32, &H26, &H18, &H19, &H3F, &H27, &H1C, &H1D, &H1E, &H1F, _
  &H40, &H4F, &H7F, &H7B, &H5B, &H6C, &H50, &H7D, &H4D, &H5D, &H5C, &H4E, &H6B, &H60, &H4B, &H61, _
  &HF0, &HF1, &HF2, &HF3, &HF4, &HF5, &HF6, &HF7, &HF8, &HF9, &H7A, &H5E, &H4C, &H7E, &H6E, &H6F, _
  &H7C, &HC1, &HC2, &HC3, &HC4, &HC5, &HC6, &HC7, &HC8, &HC9, &HD1, &HD2, &HD3, &HD4, &HD5, &HD6, _
  &HD7, &HD8, &HD9, &HE2, &HE3, &HE4, &HE5, &HE6, &HE7, &HE8, &HE9, &H4A, &HE0, &H5A, &H5F, &H6D, _
  &H79, &H81, &H82, &H83, &H84, &H85, &H86, &H87, &H88, &H89, &H91, &H92, &H93, &H94, &H95, &H96, _
  &H97, &H98, &H99, &HA2, &HA3, &HA4, &HA5, &HA6, &HA7, &HA8, &HA9, &HC0, &H6A, &HD0, &HA1, &H7, _
  &H20, &H21, &H22, &H23, &H24, &H15, &H6, &H17, &H28, &H29, &H2A, &H2B, &H2C, &H9, &HA, &H1B, _
  &H30, &H31, &H1A, &H33, &H34, &H35, &H36, &H8, &H38, &H39, &H3A, &H3B, &H4, &H14, &H3E, &HE1, _
  &H41, &H42, &H43, &H44, &H45, &H46, &H47, &H48, &H49, &H51, &H52, &H53, &H54, &H55, &H56, &H57, _
  &H58, &H59, &H62, &H63, &H64, &H65, &H66, &H67, &H68, &H69, &H70, &H71, &H72, &H73, &H74, &H75, _
  &H76, &H77, &H78, &H80, &H8A, &H8B, &H8C, &H8D, &H8E, &H8F, &H90, &H9A, &H9B, &H9C, &H9D, &H9E, _
  &H9F, &HA0, &HAA, &HAB, &HAC, &HAD, &HAE, &HAF, &HB0, &HB1, &HB2, &HB3, &HB4, &HB5, &HB6, &HB7, _
  &HB8, &HB9, &HBA, &HBB, &HBC, &HBD, &HBE, &HBF, &HCA, &HCB, &HCC, &HCD, &HCE, &HCF, &HDA, &HDB, _
  &HDC, &HDD, &HDE, &HDF, &HEA, &HEB, &HEC, &HED, &HEE, &HEF, &HFA, &HFB, &HFC, &HFD, &HFE, &HFF)
  bufferlen = Len(buffer)

  For i = 1 To bufferlen
    Mid$(buffer, i, 1) = Chr$(ascii(Asc(Mid$(buffer, i, 1))))
  Next i
  ascii_to_ebcdic = buffer
End Function

Function ebcdic_to_ascii(ByVal buffer As String) As String
  Dim ValHexEBD As String, ValHexASC As String, ValDecASC As String
  Dim bufferlen As Double, i As Double
  
  ' Tabella esternalizzata in "...\Input-Prj\EBCDIC2ASCII\ConvertionTable"
  If UBound(ebcdic) = 0 Then
    ebcdic = Array( _
        0, 1, 2, 3, 156, 9, 134, 127, 151, 141, 142, 11, 12, 13, 14, 15, _
        16, 17, 18, 19, 157, 133, 8, 135, 24, 25, 146, 143, 28, 29, 30, 31, _
        128, 129, 130, 131, 132, 10, 23, 27, 136, 137, 138, 139, 140, 5, 6, 7, _
        144, 145, 22, 147, 148, 149, 150, 4, 152, 153, 154, 155, 20, 21, 158, 26, _
        32, 160, 226, 228, 224, 225, 227, 229, 231, 241, 162, 46, 60, 40, 43, 124, _
        38, 233, 234, 235, 232, 237, 238, 239, 236, 223, 33, 36, 42, 41, 59, 172, _
        45, 47, 194, 196, 192, 193, 195, 197, 199, 209, 166, 44, 37, 95, 62, 63, _
        248, 201, 202, 203, 200, 205, 206, 207, 204, 96, 58, 35, 64, 39, 61, 34, _
        216, 97, 98, 99, 100, 101, 102, 103, 104, 105, 171, 187, 240, 253, 254, 177, _
        176, 106, 107, 108, 109, 110, 111, 112, 113, 114, 170, 186, 230, 184, 198, 164, _
        181, 126, 115, 116, 117, 118, 119, 120, 121, 122, 161, 191, 208, 221, 222, 174, _
        94, 163, 165, 183, 169, 167, 182, 188, 189, 190, 91, 93, 175, 168, 180, 215, _
        123, 65, 66, 67, 68, 69, 70, 71, 72, 73, 173, 244, 246, 242, 243, 245, _
        125, 74, 75, 76, 77, 78, 79, 80, 81, 82, 185, 251, 252, 249, 250, 255, _
        92, 247, 83, 84, 85, 86, 87, 88, 89, 90, 178, 212, 214, 210, 211, 213, _
        48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 179, 219, 220, 217, 218, 255)
  End If
  bufferlen = Len(buffer)

  For i = 1 To bufferlen
    ValHexEBD = Hex(Asc(Mid$(buffer, i, 1)))
    Mid$(buffer, i, 1) = Chr$(ebcdic(Asc(Mid$(buffer, i, 1))))
    ValDecASC = Asc(Mid$(buffer, i, 1))
    If ValDecASC < 32 Or ValDecASC > 128 Then
      ' Not Printable Char
      ValHexASC = Hex(Asc(Mid$(buffer, i, 1)))
      ValHexASC = Left("00", 2 - Len(ValHexASC)) & ValHexASC
      ValHexEBD = Left("00", 2 - Len(ValHexEBD)) & ValHexEBD
      Print #NumLog, i - 1 & " - EBCDIC:" & ValHexEBD & " - ASCII:" & ValHexASC & "('" & Mid$(buffer, i, 1) & "')"
    End If
  Next i
  ebcdic_to_ascii = buffer
End Function

Public Sub E2A(TxtFileLib As String, fileName As String, RtbImport As RichTextBox)
  Dim NomefileI As String, NumFileI As Integer
  Dim NomeFileO As String, NumFileO As Integer
  Dim strE As String, strA As String
  Dim lenFile As Double
  
  On Error GoTo ErrorHandler
  NomefileI = fileName
  fileName = Mid(NomefileI, InStrRev(NomefileI, "\") + 1)
  fileName = Left(fileName, InStr(fileName & ".", ".") - 1)
  NumFileI = FreeFile
  Open NomefileI For Binary As NumFileI
  NumLog = FreeFile
  Open m_fun.FnPathDB & "\Output-Prj\EBCDIC2ASCII\Log\" & fileName For Output As NumLog
  lenFile = FileLen(NomefileI)
  If lenFile > 0 Then
    strE = Input(lenFile, #NumFileI)
    strA = ebcdic_to_ascii(strE)
  Else
    strA = ""
  End If
  Close NumFileI
  Close NumLog
  
  NomeFileO = NomefileI & ".ascii"
  NumFileO = FreeFile
  Open NomeFileO For Output As NumFileO
  Print #NumFileO, strA
  Close NumFileO
  
  TxtFileLib = NomeFileO
  RtbImport.text = strA
  RtbImport.SaveFile NomeFileO, 1
  Exit Sub
ErrorHandler:
  If Err.Number = 76 Then
    If Not fso.FolderExists(m_fun.FnPathDB & "\Output-Prj\EBCDIC2ASCII") Then
      MkDir m_fun.FnPathDB & "\Output-Prj\EBCDIC2ASCII"
      MkDir m_fun.FnPathDB & "\Output-Prj\EBCDIC2ASCII\Log"
      Resume
    Else
      MsgBox Err.Number & " - " & Err.Description, vbExclamation, Menu.TsNameProdotto
      Resume Next
    End If
  Else
    MsgBox Err.Number & " - " & Err.Description, vbExclamation, Menu.TsNameProdotto
    Resume Next
  End If
End Sub

Public Sub E2A_Directory(TxtFileLib As String, RtbImport As RichTextBox)
  Dim fileName As String
  Dim NomefileI As String, NumFileI As Integer
  Dim NomeFileO As String, NumFileO As Integer
  
  Dim strE As String, strA As String
  Dim lenFile As Double
  
  On Error GoTo ErrorHandler
  
  MatsdF_IebGener.MousePointer = vbHourglass
  TxtFileLib = TxtFileLib & IIf(Right(TxtFileLib, 1) = "\", "", "\")
  fileName = Dir(TxtFileLib)
  Do Until fileName <> "." Or fileName <> ".."
    fileName = Dir(TxtFileLib)
  Loop
  Do While Len(fileName)
    NomefileI = TxtFileLib & fileName
    fileName = Left(fileName, InStr(fileName & ".", ".") - 1)
    NumFileI = FreeFile
    Open NomefileI For Binary As NumFileI
    NumLog = FreeFile
    Open m_fun.FnPathDB & "\Output-Prj\EBCDIC2ASCII\Log\" & fileName For Output As NumLog
    lenFile = FileLen(NomefileI)
    If lenFile > 0 Then
      strE = Input(lenFile, #NumFileI)
      strA = ebcdic_to_ascii(strE)
    Else
      strA = ""
    End If
    Close NumFileI
    Close NumLog
    
    NomeFileO = TxtFileLib & "Out\" & fileName
    NumFileO = FreeFile
    Open NomeFileO For Output As NumFileO
    Print #NumFileO, strA
    Close NumFileO
    
    fileName = Dir
    Do Until fileName <> "." Or fileName <> ".."
      fileName = Dir
    Loop
  Loop
  TxtFileLib = TxtFileLib & "Out\"
  MatsdF_IebGener.MousePointer = vbDefault
  MsgBox "Convertion Complete!", vbInformation, Menu.TsNameProdotto
  Exit Sub
ErrorHandler:
  If Err.Number = 76 Then
    MkDir TxtFileLib & "Out"
    If Not fso.FolderExists(m_fun.FnPathDB & "\Output-Prj\EBCDIC2ASCII") Then
      MkDir m_fun.FnPathDB & "\Output-Prj\EBCDIC2ASCII"
      MkDir m_fun.FnPathDB & "\Output-Prj\EBCDIC2ASCII\Log"
    End If
    Resume
  Else
    MsgBox Err.Number & " - " & Err.Description, vbExclamation, Menu.TsNameProdotto
    Resume Next
  End If
End Sub

Public Sub Unblock(fileName As String, recLen As Long, RtbImport As RichTextBox)
  Dim i As Double, lenFile As Double
  Dim wRec As String, wTesto As String
  Dim NomeFileO As String, NumFileO As Integer
  
  NumFileO = FreeFile
  NomeFileO = fileName
  Open NomeFileO For Binary As NumFileO
  lenFile = FileLen(NomeFileO)
  wRec = Input(lenFile, #NumFileO)
  For i = 1 To lenFile Step recLen
    wTesto = wTesto & RTrim(Mid(wRec, i, recLen)) & vbCrLf
  Next i
  Close NumFileO
  
  NumFileO = FreeFile
  Open NomeFileO For Output As NumFileO
  Print #NumFileO, wTesto
  Close NumFileO
  RtbImport.text = wTesto
  'RtbImport.SaveFile NomeFileO, 1
End Sub

Public Sub Unblock_Directory(wPath As String, recLen As Long, RtbAppo As RichTextBox)
  Dim i As Double, lenFile As Double
  Dim fileName As String
  Dim wTesto As String, wRec As String
  Dim NomeFileO As String, NumFileO As Integer
    
  MatsdF_IebGener.MousePointer = vbHourglass
  wPath = wPath & IIf(Right(wPath, 1) = "\", "", "\")
  fileName = Dir(wPath)
  Do Until fileName <> "." Or fileName <> ".."
    fileName = Dir(wPath)
  Loop
  Do While Len(fileName)
    NumFileO = FreeFile
    NomeFileO = wPath & fileName
    Open NomeFileO For Binary As NumFileO
    lenFile = FileLen(NomeFileO)
    wRec = Input(lenFile, #NumFileO)
    wTesto = ""
    For i = 1 To lenFile Step recLen
      wTesto = wTesto & RTrim(Mid(wRec, i, recLen)) & vbCrLf
    Next i
    Close NumFileO
    
    NumFileO = FreeFile
    Open NomeFileO For Output As NumFileO
    Print #NumFileO, wTesto
    Close NumFileO
    'RtbAppo.text = wTesto
    'RtbAppo.SaveFile NomeFileO, 1
    
    fileName = Dir
    Do Until fileName <> "." Or fileName <> ".."
      fileName = Dir
    Loop
  Loop
  MatsdF_IebGener.MousePointer = vbDefault
  MsgBox "Convertion Complete!", vbInformation, Menu.TsNameProdotto
End Sub

Sub LeggiArrayEBCDIC2ASCII()
  Dim numFile As Integer, Line As String
  Dim arrChar() As String, txtTable As String
  Dim i As Long
  
  On Error GoTo ErrorHandler
  txtTable = ""
  numFile = FreeFile
  Open m_fun.FnPathDB & "\Input-Prj\EBCDIC2ASCII\ConvertionTable" For Input As numFile
  Line Input #numFile, Line
  Do Until EOF(numFile)
    If Left(Line, 1) <> "'" Then
      txtTable = txtTable & Line
    End If
    Line Input #numFile, Line
  Loop
  Close #numFile
  
  ReDim ebcdic(0)
  arrChar() = Split(txtTable, ",")
  For i = 0 To UBound(arrChar)
    If Len(Trim(arrChar(i))) Then
      ebcdic(UBound(ebcdic)) = Int(arrChar(i))
      ReDim Preserve ebcdic(UBound(ebcdic) + 1)
    End If
  Next i
  Exit Sub
ErrorHandler:
  ReDim ebcdic(0)
End Sub
