Attribute VB_Name = "MatsdM_Menu"
Option Explicit

Global Const SYSTEM_DIR = "i-4.Mig.cfg"
'Global Const FILE_INI = "\" & SYSTEM_DIR & "\i-4.Migration.ini"

Global TsMenu() As MaM1

Global Menu As MatsdC_Functions
Global m_fun As MaFndC_Funzioni
Global m_pars As MapsdC_Menu

Global SwMenu As Boolean

Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long

''silvia 12/9/2008
'Get Row and Column Positions Fast in a Textbox or Rich Textbox
Private Declare Function SendMessageByNum Lib "user32" Alias "SendMessageA" _
       (ByVal hwnd As Long, _
        ByVal wMsg As Long, _
        ByVal wParam As Long, _
        ByVal lParam As Long) As Long

Private Const EM_LINEFROMCHAR As Long = &HC9
Private Const EM_LINEINDEX As Long = &HBB
Private Const EM_GETLINECOUNT As Long = &HBA
Private Const EM_LINESCROLL As Long = &HB6
'
'*******************
' GetLineCount
' Get the line count
'*******************
Public Function GetLineCount(tBox As Object) As Long
  GetLineCount = SendMessageByNum(tBox.hwnd, EM_GETLINECOUNT, 0&, 0&)
End Function

'*******************
' GetLineNum
' Get current line number
'*******************
Public Function GetLineNum(tBox As Object) As Long
  GetLineNum = SendMessageByNum(tBox.hwnd, EM_LINEFROMCHAR, tBox.SelStart, 0&)
End Function


'*******************
' GetColPos
' Get current Column
'*******************
Public Function GetColPos(tBox As Object) As Long
  GetColPos = tBox.SelStart - SendMessageByNum(tBox.hwnd, EM_LINEINDEX, -1&, 0&)
End Function
''silvia
'''''''''

'************************************************************************
'***** SEZIONE RIGUARDANTE LE FUNZIONI SUI MENU *************************
'************************************************************************
Public Function Carica_Menu() As Boolean
  SwMenu = True
  
  Carica_Menu = True
  
  'crea il menu in memoria
  ReDim TsMenu(0)
  
  'Primo Bottone del menu a sinistra : Tools
  ReDim Preserve TsMenu(UBound(TsMenu) + 1)
  TsMenu(UBound(TsMenu)).Id = 800
  TsMenu(UBound(TsMenu)).Label = "Tools"
  TsMenu(UBound(TsMenu)).ToolTipText = ""
  TsMenu(UBound(TsMenu)).Picture = ""
  TsMenu(UBound(TsMenu)).M1Name = ""
  TsMenu(UBound(TsMenu)).M1SubName = ""
  TsMenu(UBound(TsMenu)).M1Level = 0
  TsMenu(UBound(TsMenu)).M2Name = "<Root>"
  TsMenu(UBound(TsMenu)).M3Name = ""
  TsMenu(UBound(TsMenu)).M4Name = ""
  TsMenu(UBound(TsMenu)).Funzione = ""
  TsMenu(UBound(TsMenu)).DllName = "MatsdP_00"
  TsMenu(UBound(TsMenu)).TipoFinIm = ""

    '1) Sottobottone : Tools ---> Allineamento Cobol
    ReDim Preserve TsMenu(UBound(TsMenu) + 1)
    TsMenu(UBound(TsMenu)).Id = 805
    TsMenu(UBound(TsMenu)).Label = "Environment Align"
    TsMenu(UBound(TsMenu)).ToolTipText = ""
    TsMenu(UBound(TsMenu)).Picture = Menu.TsImgDir & "\EnvAlign_Enabled.ico"
    TsMenu(UBound(TsMenu)).PictureEn = Menu.TsImgDir & "\EnvAlign_Enabled.ico"
    TsMenu(UBound(TsMenu)).M1Name = ""
    TsMenu(UBound(TsMenu)).M1SubName = ""
    TsMenu(UBound(TsMenu)).M1Level = 0
    TsMenu(UBound(TsMenu)).M2Name = "Tools"
    TsMenu(UBound(TsMenu)).M3Name = ""
    TsMenu(UBound(TsMenu)).M4Name = ""
    TsMenu(UBound(TsMenu)).Funzione = "Show_Allineamento_COBOL"
    TsMenu(UBound(TsMenu)).DllName = "MatsdP_00"
    TsMenu(UBound(TsMenu)).TipoFinIm = ""

'    '2) Sottobottone : MTP
'    ReDim Preserve TsMenu(UBound(TsMenu) + 1)
'    TsMenu(UBound(TsMenu)).Id = 810
'    TsMenu(UBound(TsMenu)).Label = "CICS -> MTP Align"
'    TsMenu(UBound(TsMenu)).ToolTipText = ""
'    TsMenu(UBound(TsMenu)).Picture = Menu.TsImgDir & "\MTPAlign_Enabled.ico"
'    TsMenu(UBound(TsMenu)).PictureEn = Menu.TsImgDir & "\MTPAlign_Enabled.ico"
'    TsMenu(UBound(TsMenu)).M1Name = ""
'    TsMenu(UBound(TsMenu)).M1SubName = ""
'    TsMenu(UBound(TsMenu)).M1Level = 0
'    TsMenu(UBound(TsMenu)).M2Name = "Tools"
'    TsMenu(UBound(TsMenu)).M3Name = ""
'    TsMenu(UBound(TsMenu)).M4Name = ""
'    TsMenu(UBound(TsMenu)).Funzione = "Show_MTP_Align"
'    TsMenu(UBound(TsMenu)).DllName = "MatsdP_00"
'    TsMenu(UBound(TsMenu)).TipoFinIm = ""
    
    
'    '3) Sottobottone : Migrazione JCL
'    ReDim Preserve TsMenu(UBound(TsMenu) + 1)
'    TsMenu(UBound(TsMenu)).Id = 815
'    TsMenu(UBound(TsMenu)).Label = "Jcl Migration"
'    TsMenu(UBound(TsMenu)).ToolTipText = ""
'    TsMenu(UBound(TsMenu)).Picture = Menu.TsImgDir & "\JclMigr_Disabled.ico"
'    TsMenu(UBound(TsMenu)).PictureEn = Menu.TsImgDir & "\JclMigr_Enabled.ico"
'    TsMenu(UBound(TsMenu)).M1Name = ""
'    TsMenu(UBound(TsMenu)).M1SubName = ""
'    TsMenu(UBound(TsMenu)).M1Level = 0
'    TsMenu(UBound(TsMenu)).M2Name = "Tools"
'    TsMenu(UBound(TsMenu)).M3Name = ""
'    TsMenu(UBound(TsMenu)).M4Name = ""
'    TsMenu(UBound(TsMenu)).Funzione = "Show_JclMigr"
'    TsMenu(UBound(TsMenu)).DllName = "MatsdP_00"
'    TsMenu(UBound(TsMenu)).TipoFinIm = ""
    
    '3) Sottobottone : Macro
    ReDim Preserve TsMenu(UBound(TsMenu) + 1)
    TsMenu(UBound(TsMenu)).Id = 815
    TsMenu(UBound(TsMenu)).Label = "Macro"
    TsMenu(UBound(TsMenu)).ToolTipText = ""
    TsMenu(UBound(TsMenu)).Picture = Menu.TsImgDir & "\Macro_Disabled.ico"
    TsMenu(UBound(TsMenu)).PictureEn = Menu.TsImgDir & "\Macro_Enabled.ico"
    TsMenu(UBound(TsMenu)).M1Name = ""
    TsMenu(UBound(TsMenu)).M1SubName = ""
    TsMenu(UBound(TsMenu)).M1Level = 0
    TsMenu(UBound(TsMenu)).M2Name = "Tools"
    TsMenu(UBound(TsMenu)).M3Name = ""
    TsMenu(UBound(TsMenu)).M4Name = ""
    TsMenu(UBound(TsMenu)).Funzione = "Show_Macro"
    TsMenu(UBound(TsMenu)).DllName = "MatsdP_00"
    TsMenu(UBound(TsMenu)).TipoFinIm = ""
        
  ' 4) Sottobottone : IebGener
    ReDim Preserve TsMenu(UBound(TsMenu) + 1)
    TsMenu(UBound(TsMenu)).Id = 820
    TsMenu(UBound(TsMenu)).Label = "Import/Export Library"
    TsMenu(UBound(TsMenu)).ToolTipText = ""
    TsMenu(UBound(TsMenu)).Picture = Menu.TsImgDir & "\jcls.ico"
    TsMenu(UBound(TsMenu)).PictureEn = Menu.TsImgDir & "\jcls.ico"
    TsMenu(UBound(TsMenu)).M1Name = ""
    TsMenu(UBound(TsMenu)).M1SubName = ""
    TsMenu(UBound(TsMenu)).M1Level = 0
    TsMenu(UBound(TsMenu)).M2Name = "Tools"
    TsMenu(UBound(TsMenu)).M3Name = ""
    TsMenu(UBound(TsMenu)).M4Name = ""
    TsMenu(UBound(TsMenu)).Funzione = "Show_IEBGener"
    TsMenu(UBound(TsMenu)).DllName = "MatsdP_00"
    TsMenu(UBound(TsMenu)).TipoFinIm = ""
  
''''    '5) Sottobottone : Repository
''''    ReDim Preserve TsMenu(UBound(TsMenu) + 1)
''''    TsMenu(UBound(TsMenu)).Id = 825
''''    TsMenu(UBound(TsMenu)).Label = "Control Repository"
''''    TsMenu(UBound(TsMenu)).ToolTipText = ""
''''    TsMenu(UBound(TsMenu)).Picture = Menu.TsImgDir & "\Default_Disabled.ico"
''''    TsMenu(UBound(TsMenu)).PictureEn = Menu.TsImgDir & "\Default_Enabled.ico"
''''    TsMenu(UBound(TsMenu)).M1Name = ""
''''    TsMenu(UBound(TsMenu)).M1SubName = ""
''''    TsMenu(UBound(TsMenu)).M1Level = 0
''''    TsMenu(UBound(TsMenu)).M2Name = "Tools"
''''    TsMenu(UBound(TsMenu)).M3Name = ""
''''    TsMenu(UBound(TsMenu)).M4Name = ""
''''    TsMenu(UBound(TsMenu)).Funzione = "Show_CTRL_Repository"
''''    TsMenu(UBound(TsMenu)).DllName = "MatsdP_00"
''''    TsMenu(UBound(TsMenu)).TipoFinIm = ""
    
    '6) Sottobottone : EBCDIC TO ASCII
    ReDim Preserve TsMenu(UBound(TsMenu) + 1)
    TsMenu(UBound(TsMenu)).Id = 830
    TsMenu(UBound(TsMenu)).Label = "Files Conversion"
    TsMenu(UBound(TsMenu)).ToolTipText = ""
    TsMenu(UBound(TsMenu)).Picture = Menu.TsImgDir & "\Macro_Enabled3.ico"
    TsMenu(UBound(TsMenu)).PictureEn = Menu.TsImgDir & "\Macro_Enabled3.ico"
    TsMenu(UBound(TsMenu)).M1Name = ""
    TsMenu(UBound(TsMenu)).M1SubName = ""
    TsMenu(UBound(TsMenu)).M1Level = 0
    TsMenu(UBound(TsMenu)).M2Name = "Tools"
    TsMenu(UBound(TsMenu)).M3Name = ""
    TsMenu(UBound(TsMenu)).M4Name = ""
    TsMenu(UBound(TsMenu)).Funzione = "Show_EBCDIC2ASCII"
    TsMenu(UBound(TsMenu)).DllName = "MatsdP_00"
    TsMenu(UBound(TsMenu)).TipoFinIm = ""
    
    '7) Sottobottone : Cobol/XE
    ReDim Preserve TsMenu(UBound(TsMenu) + 1)
    TsMenu(UBound(TsMenu)).Id = 835
    TsMenu(UBound(TsMenu)).Label = "Cobol/XE"
    TsMenu(UBound(TsMenu)).ToolTipText = ""
    TsMenu(UBound(TsMenu)).Picture = Menu.TsImgDir & "\00010.ico"
    TsMenu(UBound(TsMenu)).PictureEn = Menu.TsImgDir & "\00010.ico"
    TsMenu(UBound(TsMenu)).M1Name = ""
    TsMenu(UBound(TsMenu)).M1SubName = ""
    TsMenu(UBound(TsMenu)).M1Level = 0
    TsMenu(UBound(TsMenu)).M2Name = "Tools"
    TsMenu(UBound(TsMenu)).M3Name = ""
    TsMenu(UBound(TsMenu)).M4Name = ""
    TsMenu(UBound(TsMenu)).Funzione = "Show_COBOL_XE"
    TsMenu(UBound(TsMenu)).DllName = "MatsdP_00"
    TsMenu(UBound(TsMenu)).TipoFinIm = ""
   
    '5) Sottobottone : Repository
    ReDim Preserve TsMenu(UBound(TsMenu) + 1)
    TsMenu(UBound(TsMenu)).Id = 825
    TsMenu(UBound(TsMenu)).Label = "Control Repository"
    TsMenu(UBound(TsMenu)).ToolTipText = ""
    TsMenu(UBound(TsMenu)).Picture = Menu.TsImgDir & "\Default_Disabled.ico"
    TsMenu(UBound(TsMenu)).PictureEn = Menu.TsImgDir & "\Default_Enabled.ico"
    TsMenu(UBound(TsMenu)).M1Name = ""
    TsMenu(UBound(TsMenu)).M1SubName = ""
    TsMenu(UBound(TsMenu)).M1Level = 0
    TsMenu(UBound(TsMenu)).M2Name = "Tools"
    TsMenu(UBound(TsMenu)).M3Name = ""
    TsMenu(UBound(TsMenu)).M4Name = ""
    TsMenu(UBound(TsMenu)).Funzione = "Show_CTRL_Repository"
    TsMenu(UBound(TsMenu)).DllName = "MatsdP_00"
    TsMenu(UBound(TsMenu)).TipoFinIm = ""
   
End Function
