Attribute VB_Name = "MatsdM_DB"
Option Explicit
Global GbErrLevel As String

Function VerificaRelazioni(Tipo As String) As Boolean
  Dim TbO As Recordset, TbR As Recordset
  Dim wSql As String, wSql1 As String
  
  MatsdF_Repos.RTErr.text = ""
  MatsdF_Repos.RTErr.Refresh
  
  Select Case Tipo
    Case "CPY"
       Set TbO = m_fun.Open_Recordset("select * from bs_oggetti where tipo = 'CPY'")
       If TbO.RecordCount > 0 Then
         TbO.MoveFirst
         While Not TbO.EOF
           Set TbR = m_fun.Open_Recordset("select * from psrel_obj where idoggettor = " & TbO!IdOggetto)
           If TbR.RecordCount = 0 Then
              MatsdF_Repos.RTErr.SelText = "Copy " & TbO!directory_input & "\" & TbO!Nome! & " non referenziata" & vbCrLf
              MatsdF_Repos.RTErr.Refresh
              wSql = "Delete from bs_oggetti where idoggetto = " & TbO!IdOggetto & " ; "
              wSql1 = "Delete from bs_segnalazioni where idoggetto = " & TbO!IdOggetto & " ; "
              GbErrLevel = ""
              Menu.TsConnection.Execute wSql
              Menu.TsConnection.Execute wSql1
           End If
           TbO.MoveNext
         Wend
       End If
  End Select
  
End Function

Sub AggErrLevel(wIdObj)
  Dim tb As Recordset
  
  GbErrLevel = ""
  Set tb = m_fun.Open_Recordset("Select * from bs_segnalazioni where idoggetto = " & wIdObj)
  While Not tb.EOF
    SettaErrLevel tb!gravita, tb!tipologia
    tb.MoveNext
  Wend
  tb.Close
End Sub

Sub SettaErrLevel(wPeso, wAmb)
  Dim xPeso As Integer
  Dim xAmb As String
  
  Dim wErrL1 As String
  Dim wErrL2 As String
  Dim k As Integer
  
  Select Case wPeso
    Case "I"
       xPeso = "02"
    Case "W"
       xPeso = "04"
    Case "E"
       xPeso = "08"
    Case "S"
       xPeso = "12"
  End Select
  
  Select Case wAmb
   Case "DATI"
       xAmb = "D"
   Case Else
       xAmb = "P"
  End Select


  k = InStr(GbErrLevel, "-")
  If k = 0 Then
    wErrL1 = ""
    wErrL2 = "00"
  Else
    wErrL1 = Mid$(GbErrLevel, 1, k - 1)
    wErrL2 = Mid$(GbErrLevel, k + 1)
  End If
  
  If InStr(wErrL1, xAmb) = 0 Then
    wErrL1 = wErrL1 & xAmb
  End If

  If xPeso > wErrL2 Then wErrL2 = xPeso

  GbErrLevel = wErrL1 & "-" & wErrL2
End Sub

Function VerificaObsolescenza(Tipo As String) As Boolean
  Dim TbO As Recordset, TbR As Recordset
  Dim wStr1 As String, wStr2 As String
  Dim wNom1 As String, wNom2 As String
  Dim k As Integer
  Dim wResp As Variant
  
  
  MatsdF_Repos.RTErr.text = ""
  MatsdF_Repos.RTErr.Refresh
  
  Select Case Tipo
    Case "DBD"
      Set TbO = m_fun.Open_Recordset("select distinct directory_input from bs_oggetti where tipo = 'DBD'")
      While Not TbO.EOF
        wStr1 = Menu.TsPathDef & Mid$(TbO!directory_input, 2) & "\*.dbd"
        wStr2 = Dir(wStr1)
        While Trim(wStr2) <> ""
          Set TbR = m_fun.Open_Recordset("select * from bs_oggetti where tipo = 'DBD' and nome = '" & wStr2 & "' ")
          If TbR.RecordCount = 0 Then
            wNom1 = Menu.TsPathDef & Mid$(TbO!directory_input, 2) & "\" & wStr2
            MatsdF_Repos.RTErr.SelText = wNom1 & vbCrLf
            MatsdF_Repos.RTErr.Refresh

            wNom2 = Menu.TsPathDef & Mid$(TbO!directory_input, 2) & "\" & wStr2
            k = InStr(wNom2, ".")
            Mid$(wNom2, k, 4) = ".DBO"
            FileCopy wNom1, wNom2
            Kill wNom1
          End If
          wStr2 = Dir()
        Wend
        TbO.MoveNext
      Wend
       
    Case "CBL"
      Set TbO = m_fun.Open_Recordset("select distinct directory_input from bs_oggetti where tipo = 'CBL'")
      While Not TbO.EOF
        wStr1 = Menu.TsPathDef & Mid$(TbO!directory_input, 2) & "\*.cbl"
        wStr2 = Dir(wStr1)
        While Trim(wStr2) <> ""
          Set TbR = m_fun.Open_Recordset("select * from bs_oggetti where tipo = 'CBL' and nome = '" & wStr2 & "' ")
          If TbR.RecordCount = 0 Then
            
            wNom1 = Menu.TsPathDef & Mid$(TbO!directory_input, 2) & "\" & wStr2
            MatsdF_Repos.RTErr.SelText = wNom1 & vbCrLf
            MatsdF_Repos.RTErr.Refresh

            wNom2 = Menu.TsPathDef & Mid$(TbO!directory_input, 2) & "\" & wStr2
            k = InStr(wNom2, ".")
            Mid$(wNom2, k, 4) = ".COB"
            FileCopy wNom1, wNom2
            Kill wNom1
          End If
          wStr2 = Dir()
        Wend
        TbO.MoveNext
      Wend
       
    Case "CPY"
      Set TbO = m_fun.Open_Recordset("select distinct directory_input from bs_oggetti where tipo = 'CPY'")
      While Not TbO.EOF
        wStr1 = Menu.TsPathDef & Mid$(TbO!directory_input, 2) & "\*.cpy"
        wStr2 = Dir(wStr1)
        While Trim(wStr2) <> ""
          Set TbR = m_fun.Open_Recordset("select * from bs_oggetti where tipo = 'CPY' and nome = '" & wStr2 & "' ")
          If TbR.RecordCount = 0 Then
            
            wNom1 = Menu.TsPathDef & Mid$(TbO!directory_input, 2) & "\" & wStr2
            MatsdF_Repos.RTErr.SelText = wNom1 & vbCrLf
            MatsdF_Repos.RTErr.Refresh

            wNom2 = Menu.TsPathDef & Mid$(TbO!directory_input, 2) & "\" & wStr2
            k = InStr(wNom2, ".")
            Mid$(wNom2, k, 4) = ".CPO"
            FileCopy wNom1, wNom2
            Kill wNom1
          End If
          wStr2 = Dir()
        Wend
        TbO.MoveNext
      Wend
  End Select

End Function

Public Function Restituisci_NomeOgg_Da_IdOggetto(Id As Long) As String
  'restituisce la directory dall'id oggetto secondo il tipo INPUT,OUTPUT
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Id)
  If r.RecordCount Then
    Restituisci_NomeOgg_Da_IdOggetto = r!Nome
  End If
  r.Close
End Function

Public Sub Crea_Tabelle_Macro()
  Dim sql As String
  
  
  If Menu.TsProvider = SQLSERVER Then ' Check for SQL Server Connection
    'TABELLE TS_MACRO
    'tabella Oggetti
    sql = "CREATE TABLE [TS_Macro] ([IdMacro] [int] IDENTITY (1,1) NOT NULL, " _
        & "[Nome]                             NVARCHAR(50), " _
        & "[PerLinea]                         BIT," _
        & "[Sintax]                           NTEXT, " _
        & "CONSTRAINT TS_MacroPK PRIMARY KEY ([IdMacro],[Nome]));"
  ElseIf Menu.TsProvider = Access Then
    'TABELLE TS_MACRO
    'tabella Oggetti
    sql = "CREATE TABLE [TS_Macro] ([IdMacro] COUNTER, " _
        & "[Nome]                             TEXT(50), " _
        & "[PerLinea]                         BIT," _
        & "[Sintax]                           MEMO, " _
        & "CONSTRAINT PK PRIMARY KEY ([IdMacro],[Nome]));"
  End If

  Menu.TsConnection.Execute sql
  
  'crea gli indici
  sql = "CREATE INDEX [UniqIdx01] ON TS_Macro ([IdMacro]);"
  Menu.TsConnection.Execute sql
  
  sql = "CREATE INDEX [UniqIdx02] ON TS_Macro ([Nome]);"
  Menu.TsConnection.Execute sql
End Sub
