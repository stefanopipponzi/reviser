VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form MatsdF_Cobol_XE 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "COBOL/XE"
   ClientHeight    =   5340
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8895
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5340
   ScaleWidth      =   8895
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.OptionButton OptPgm 
      Caption         =   "Conv PGM"
      Height          =   192
      Left            =   1980
      TabIndex        =   15
      Top             =   4200
      Width           =   1155
   End
   Begin VB.OptionButton OptExtr 
      Caption         =   "Extract Copy Panel"
      Height          =   192
      Left            =   180
      TabIndex        =   14
      Top             =   4200
      Width           =   1755
   End
   Begin VB.Frame Frame6 
      Caption         =   "Source Options"
      ForeColor       =   &H00000080&
      Height          =   1335
      Left            =   60
      TabIndex        =   11
      Top             =   3600
      Width           =   3375
      Begin VB.OptionButton optValue 
         Caption         =   "Add Value to CopyMap"
         Height          =   192
         Left            =   120
         TabIndex        =   16
         Top             =   960
         Width           =   1935
      End
      Begin VB.OptionButton OptCPY 
         Caption         =   "CopyMapOcc"
         Height          =   192
         Left            =   1920
         TabIndex        =   13
         Top             =   240
         Width           =   1335
      End
      Begin VB.OptionButton OptBMS 
         Caption         =   "To BMS"
         Height          =   192
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Value           =   -1  'True
         Width           =   1335
      End
   End
   Begin RichTextLib.RichTextBox RTBModifica 
      Height          =   525
      Left            =   1320
      TabIndex        =   10
      Top             =   5460
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MatsdF_Cobol_XE.frx":0000
   End
   Begin RichTextLib.RichTextBox RTRoutine 
      Height          =   525
      Left            =   690
      TabIndex        =   9
      Top             =   5460
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MatsdF_Cobol_XE.frx":0082
   End
   Begin RichTextLib.RichTextBox RTFile 
      Height          =   525
      Left            =   60
      TabIndex        =   8
      Top             =   5460
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MatsdF_Cobol_XE.frx":0104
   End
   Begin VB.Frame Frame4 
      Caption         =   "Activity Log"
      ForeColor       =   &H00000080&
      Height          =   3135
      Left            =   3480
      TabIndex        =   4
      Top             =   450
      Width           =   5385
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   255
         Left            =   90
         TabIndex        =   6
         Top             =   2835
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ListView LvObject 
         Height          =   2535
         Left            =   90
         TabIndex        =   5
         Top             =   240
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   4471
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "ID"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Object"
            Object.Width           =   12347
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Time"
            Object.Width           =   3528
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Error Log"
      ForeColor       =   &H00000080&
      Height          =   1725
      Left            =   3480
      TabIndex        =   3
      Top             =   3600
      Width           =   5385
      Begin RichTextLib.RichTextBox RTErr 
         Height          =   1365
         Left            =   60
         TabIndex        =   7
         Top             =   270
         Width           =   5235
         _ExtentX        =   9234
         _ExtentY        =   2408
         _Version        =   393217
         BackColor       =   -2147483624
         Enabled         =   -1  'True
         ScrollBars      =   2
         TextRTF         =   $"MatsdF_Cobol_XE.frx":0186
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Select Object for Analysis"
      ForeColor       =   &H00000080&
      Height          =   3135
      Left            =   0
      TabIndex        =   1
      Top             =   450
      Width           =   3435
      Begin MSComctlLib.TreeView TVSelObj 
         Height          =   2745
         Left            =   120
         TabIndex        =   2
         Top             =   270
         Width           =   3195
         _ExtentX        =   5636
         _ExtentY        =   4842
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   529
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Start"
            Object.ToolTipText     =   "Start Encapsulating"
            ImageKey        =   "Attiva"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Help"
            Object.ToolTipText     =   "Help"
            ImageKey        =   "Help"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   2760
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol_XE.frx":0208
            Key             =   "Attiva"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol_XE.frx":0522
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol_XE.frx":0634
            Key             =   "Printer"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MatsdF_Cobol_XE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const EMBEDDED_CHK = 4

Dim NumObjInParser As Double
Dim NumFeatures As Integer
'SQ: BUTTARE VIA --^

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub OptBMS_Click()
  OptExtr.Value = False
  
  OptCPY.Value = False
  OptPgm.Value = False
End Sub

Private Sub OptCPY_Click()
  OptExtr.Value = False
  
  OptBMS.Value = False
  OptPgm.Value = False
End Sub

Private Sub OptExtr_Click()
  OptCPY.Value = False
  
  OptBMS.Value = False
  OptPgm.Value = False
End Sub

Private Sub OptPgm_Click()
  OptCPY.Value = False
  
  OptBMS.Value = False
  OptExtr.Value = False
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim rIst As Recordset
  Dim Selezione As String
  Dim MaxVal As Long
  
  On Error GoTo errorHandler
  LvObject.ListItems.Clear
  RTErr.Text = ""
  
  Select Case Button.key
    Case "Start"
      DoEvents
            
      Selezione = Restituisci_Selezione(TVSelObj)
      MaxVal = Seleziona_Istruzioni(Selezione, rIst)
      
      If MaxVal = 0 Then
        MaxVal = 1
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
      
      PBar.Max = MaxVal
      PBar.Value = 0
      
      Screen.MousePointer = vbHourglass
      While Not rIst.EOF
        trattaPanel rIst!IdOggetto
        PBar.Value = PBar.Value + 1
        With LvObject.ListItems.Add(, , rIst!IdOggetto)
           .SubItems(1) = outputFile
           .SubItems(2) = Now
        End With
        rIst.MoveNext
      Wend
    
      PBar.Value = 0
'    Case "Close"
'      Unload Me
'    Case "Help"
    
  End Select
  
  Screen.MousePointer = vbDefault
Exit Sub
errorHandler:
  MsgBox Err.Number & " - " & Err.Description
End Sub

Private Sub Form_Load()
  NumFeatures = 1
  NumObjInParser = 0

  TVSelObj.Nodes.Add , , "SEL", "Only Selected Object"
  TVSelObj.Nodes(1).Checked = True
  
  If Menu.TsTipoFinIm <> "" Then
    Menu.TsFinestra.ListItems.Add , , "Tools: Cobol - XE Window Loaded at " & Time
  End If
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

'''Private Sub TVSelObj_NodeCheck(ByVal Node As MSComctlLib.Node)
'''  Dim K1 As Integer
'''  Dim K2 As Integer
'''  Dim wKey As String
'''  Dim wCheck As Variant
'''  Dim tb As Recordset
'''
'''  wKey = Node.key
'''  wCheck = Node.Checked
'''
'''  NumObjInParser = 0
'''
'''  Select Case Node.key
'''    Case "SEL"
'''      If Node.Checked Then
'''        For K1 = 1 To Menu.TsObjList.ListItems.count
'''          If Menu.TsObjList.ListItems(K1).Selected Then
'''             NumObjInParser = NumObjInParser + 1
'''          End If
'''        Next K1
'''      End If
'''
'''      If TVSelObj.Nodes(Node.Index + 1).Checked Then
'''        TVSelObj.Nodes(Node.Index + 1).Checked = False
'''        TVSelObj.Nodes(Node.Index + 2).Checked = False
'''      End If
'''
'''    Case "CBL"
'''      TVSelObj.Nodes(Node.Index - 1).Checked = Node.Checked
'''      TVSelObj.Nodes(1).Checked = False
'''
'''    Case Else
'''      For K1 = Node.Index + 1 To Node.Index + Node.Children
'''        TVSelObj.Nodes(K1).Checked = Node.Checked
'''      Next K1
'''      TVSelObj.Nodes(1).Checked = False
'''  End Select
'''
'''  If Not TVSelObj.Nodes(1).Checked Then
'''    For K1 = 2 To TVSelObj.Nodes.count
'''      If TVSelObj.Nodes(K1).Checked Then
'''        wKey = TVSelObj.Nodes(K1).key
'''        If wKey = "MPS" Then
'''           wKey = "BMS"
'''        End If
'''        Set tb = m_fun.Open_Recordset("Select * from bs_Oggetti where tipo = '" & wKey & "'")
'''        If tb.Recordcount Then
'''          NumObjInParser = NumObjInParser + tb.Recordcount
'''          tb.Close
'''        End If
'''      End If
'''    Next K1
'''  End If
'''
'''  Frame4.Caption = "Activite log for " & NumObjInParser & " Objects"
'''End Sub
