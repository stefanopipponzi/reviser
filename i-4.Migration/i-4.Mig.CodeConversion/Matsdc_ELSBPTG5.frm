VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form Matsdc_ELSBPTG5 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Change CALL for Dynamic PCB"
   ClientHeight    =   6750
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   8010
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6750
   ScaleWidth      =   8010
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame7 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   3795
      Left            =   3930
      TabIndex        =   13
      Top             =   3000
      Width           =   4125
      Begin MSComctlLib.ListView LswSegnalazioni 
         Height          =   3495
         Left            =   60
         TabIndex        =   14
         Top             =   180
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   6165
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Log"
            Object.Width           =   26458
         EndProperty
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "PLI Selected"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   5175
      Left            =   30
      TabIndex        =   11
      Top             =   1620
      Width           =   3885
      Begin MSComctlLib.ListView lswFileSel 
         Height          =   4755
         Left            =   120
         TabIndex        =   12
         Top             =   270
         Width           =   3645
         _ExtentX        =   6429
         _ExtentY        =   8387
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Program"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tipo"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Relazione"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Locs"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Directory"
            Object.Width           =   2470
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Notes"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1000
      Left            =   3990
      TabIndex        =   7
      Top             =   630
      Width           =   4095
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   195
         Left            =   90
         TabIndex        =   8
         Top             =   240
         Width           =   3765
         _ExtentX        =   6641
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBarK 
         Height          =   195
         Left            =   90
         TabIndex        =   9
         Top             =   450
         Width           =   3765
         _ExtentX        =   6641
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBar01 
         Height          =   195
         Left            =   90
         TabIndex        =   10
         Top             =   660
         Width           =   3765
         _ExtentX        =   6641
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selection"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1000
      Left            =   0
      TabIndex        =   4
      Top             =   630
      Width           =   3885
      Begin VB.OptionButton OptSel 
         Caption         =   "Only Selected CBL File"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   600
         Value           =   -1  'True
         Width           =   2655
      End
      Begin VB.OptionButton OptAll 
         Caption         =   "All CBL Files"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   270
         Width           =   2655
      End
   End
   Begin VB.CommandButton CmdConvert 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   30
      Picture         =   "Matsdc_ELSBPTG5.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   3
      Tag             =   "fixed"
      ToolTipText     =   "Convert"
      Top             =   0
      Width           =   550
   End
   Begin VB.CommandButton CmdErase 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   555
      Left            =   585
      Picture         =   "Matsdc_ELSBPTG5.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   2
      Tag             =   "fixed"
      ToolTipText     =   "Erase"
      Top             =   0
      Width           =   550
   End
   Begin VB.Frame Frame3 
      Caption         =   "Log"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1395
      Left            =   3930
      TabIndex        =   0
      Top             =   1620
      Width           =   4125
      Begin MSComctlLib.ListView LswLog 
         Height          =   1035
         Left            =   60
         TabIndex        =   1
         Top             =   240
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   1826
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Log"
            Object.Width           =   26458
         EndProperty
      End
   End
End
Attribute VB_Name = "Matsdc_ELSBPTG5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'silvia 12/9/2008
Dim IntLine As Integer
Dim ListObjCol As Long
Dim GbNomePgm As String

Const txtPgmCalled = "ELSBPTG5"
Const PCBName = "TECIP-PCB"

Private Sub Conversion()
  Dim i As Long
  Dim rs As Recordset, TbOggetti As Recordset
    
  LswLog.ListItems.Clear
  LswSegnalazioni.ListItems.Clear
  PBar.Max = 10
  PBar.Value = 0
  PBarK.Max = 10
  PBarK.Value = 0
  
  DoEvents

  LswLog.ListItems.Add , , Time & " - Starting Analysis"
  LswLog.Refresh
    
  If OptAll.Value Then
    If lswFileSel.ListItems.Count > 0 Then
      PBar01.Max = lswFileSel.ListItems.Count
      For i = 1 To lswFileSel.ListItems.Count
        Set rs = m_fun.Open_Recordset("select * from psrel_obj where " & _
                                      "idoggettoc = " & lswFileSel.ListItems(i).tag & " and nomecomponente = '" & txtPgmCalled & "'")
        If Not rs.EOF Then
          GbNomePgm = lswFileSel.ListItems(i)
          GbIdOggetto = lswFileSel.ListItems(i).tag
          LswLog.ListItems.Add , , Time & " - Source Migration: " & GbNomePgm
          LswLog.Refresh
          LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible

          Set TbOggetti = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & GbIdOggetto)
          If Not (TbOggetti.EOF) Then
            If Trim(TbOggetti!estensione) = "" Then
              GbFileInput = m_fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!Nome
            Else
              GbFileInput = m_fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!Nome & "." & TbOggetti!estensione
            End If
            ConvertCBL
          End If
          TbOggetti.Close
        Else
          LswLog.ListItems.Add , , Time & " - No CALL found for : " & lswFileSel.ListItems(i)
          LswLog.Refresh
          LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
        End If
        load_CBL_Segnalazioni lswFileSel.ListItems(i).tag
        PBar01.Value = PBar01.Value + 1
     Next i
    End If
  Else 'converte solo l'elemento selezionato
    PBar01.Max = lswFileSel.ListItems.Count
    For i = 1 To lswFileSel.ListItems.Count
      If lswFileSel.ListItems(i).Selected Then
        Set rs = m_fun.Open_Recordset("select * from psrel_obj where " & _
                                      "idoggettoc = " & lswFileSel.ListItems(i).tag & " and nomecomponente = '" & txtPgmCalled & "' and " & _
                                      "relazione = 'CAL'")
        If Not rs.EOF Then
          GbNomePgm = lswFileSel.ListItems(i)
          GbIdOggetto = lswFileSel.ListItems(i).tag
          LswLog.ListItems.Add , , Time & " - Source Migration: " & GbNomePgm
          LswLog.Refresh
          LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
        
          Set TbOggetti = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & GbIdOggetto)
          If Not (TbOggetti.EOF) Then
            If Trim(TbOggetti!estensione) = "" Then
              GbFileInput = m_fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!Nome
            Else
              GbFileInput = m_fun.FnPathDef & Mid(TbOggetti!Directory_Input, 2) & "\" & TbOggetti!Nome & "." & TbOggetti!estensione
            End If
            ConvertCBL
          End If
          TbOggetti.Close
        Else
          LswLog.ListItems.Add , , Time & " - No CALL found for : " & lswFileSel.ListItems(i)
          LswLog.Refresh
          LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
        End If
        load_CBL_Segnalazioni lswFileSel.ListItems(i).tag
      End If
      PBar01.Value = PBar01.Value + 1
    Next i
  End If
  
  PBar01.Value = PBar01.Max
  
  LswLog.ListItems.Add , , Time & " - Source Migration Terminated"
  LswLog.Refresh
  LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
    
  'silvia 11/9/2008
  LswSegnalazioni.Refresh
  If LswSegnalazioni.ListItems.Count Then
    LswSegnalazioni.ListItems(LswSegnalazioni.ListItems.Count).EnsureVisible
  End If
  PBar.Value = 0
  PBarK.Value = 0
  PBar01.Value = 0
End Sub

Private Sub CmdConvert_Click()
  MousePointer = vbHourglass
  If lswFileSel.ListItems.Count > 0 Then Conversion
  MousePointer = vbNormal
End Sub

Private Sub Form_Load()
  'lswFileSel.ColumnHeaders(1).Width = lswFileSel.Width / 4
  lswFileSel.ColumnHeaders(5).Width = (lswFileSel.Width / 4) * 3

  Carica_Lista_CBL
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Public Sub Carica_Lista_CBL()
  Dim rs As Recordset
  Dim listx As ListItem
  
  lswFileSel.ListItems.Clear
  Set rs = m_fun.Open_Recordset("Select DISTINCT IdOggetto,Nome,Tipo,NumRighe,directory_input,'xxx' as Relazione, Notes " & _
                                "From BS_Oggetti Where " & _
                                "Tipo = 'CBL' Order By Nome")
  'SILVIA 07-10-2008
''  Set rs = m_fun.Open_Recordset("Select DISTINCT IdOggetto,Nome,Tipo,NumRighe,directory_input,'MAIN' as Relazione,Notes" & _
''        " From BS_Oggetti Where Tipo = 'PLI'" & _
''        " UNION" & _
''        " Select DISTINCT IdOggetto,Nome,Tipo,NumRighe,directory_input,Relazione,Notes" & _
''        " From BS_Oggetti, PsRel_Obj Where Tipo = 'INC'" & _
''        " AND Nome = NomeComponente AND Relazione = 'INC'" & _
''        " UNION " & _
''        " Select DISTINCT IdOggetto,Nome,Tipo,NumRighe,directory_input,Relazione,Notes" & _
''        " From BS_Oggetti, PsRel_Obj Where tipo = 'INC'" & _
''        " AND Nome = NomeComponente AND Relazione = 'CAL'" & _
''        " Order By Nome")
  While Not rs.EOF
    Set listx = lswFileSel.ListItems.Add(, , rs!Nome)
    listx.ListSubItems.Add = rs!Tipo
    listx.ListSubItems.Add = rs!Relazione
    listx.ListSubItems.Add = rs!NumRighe
    listx.ListSubItems.Add = rs!Directory_Input
    listx.ListSubItems.Add = IIf(IsNull(rs!Notes) = True, "", rs!Notes)
    listx.tag = rs!idOggetto
    rs.MoveNext
  Wend
  rs.Close
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  'lswFileSel.ColumnHeaders(1).Width = lswFileSel.Width / 4
  'lswFileSel.ColumnHeaders(2).Width = (lswFileSel.Width / 4) * 3
End Sub

Private Sub lswFileSel_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
 
  Order = lswFileSel.SortOrder
  
  key = ColumnHeader.index
  
  ListObjCol = key
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswFileSel.SortOrder = Order
  lswFileSel.SortKey = key - 1
  lswFileSel.Sorted = True
End Sub

Private Sub lswFileSel_DblClick()
  If lswFileSel.ListItems.Count Then
    Matsdc_File_Load.load_File lswFileSel.SelectedItem, lswFileSel.SelectedItem.tag
  End If
End Sub

'''''''''''''''''''''''''
' Gestione Segnalazioni
' silvia 11/9/2008
'''''''''''
Sub load_CBL_Segnalazioni(idOggetto As Long)
  Dim tb As Recordset
  On Error GoTo ErrorHandler
  
  Set tb = m_fun.Open_Recordset("SELECT * FROM MgCBL_Segnalazioni where IdOggetto = " & idOggetto & " order by riga")
  Do Until tb.EOF
    LswSegnalazioni.ListItems.Add , , "[" & tb!tipoMsg & "] [" & Format(tb!Operatore, "_____") & "] [" & Format(tb!Riga, "00000") & "] - " & tb!Descrizione
    tb.MoveNext
  Loop
  tb.Close
  Exit Sub
ErrorHandler:
  MsgBox Err.Number & " " & Err.Description, vbExclamation + vbOKOnly, Menu.TsNameProdotto
End Sub

Private Sub lswFileSel_ItemClick(ByVal Item As MSComctlLib.ListItem)
  LswSegnalazioni.ListItems.Clear
  load_CBL_Segnalazioni Item.tag
End Sub

Private Sub LswSegnalazioni_DblClick()
  If LswSegnalazioni.ListItems.Count Then
    Matsdc_File_Load.load_File lswFileSel.SelectedItem, lswFileSel.SelectedItem.tag, IntLine
  End If
End Sub

Private Sub LswSegnalazioni_ItemClick(ByVal Item As MSComctlLib.ListItem)
  Dim strItem() As String
  
  strItem = Split(Item, "]")
  IntLine = Int(Replace(strItem(2), "[", ""))
End Sub

Private Sub ConvertCBL()
  Dim inFile As Integer, outFile As Integer
  Dim Riga As String
  Dim numRiga As Long
  Dim rs As Recordset
  Dim indent As Integer
  Dim numPCB As Integer
  
  On Error GoTo fileErr
  numRiga = 0
  inFile = 1
  Open GbFileInput For Input As inFile
  outFile = 2
  Open m_fun.FnPathPrj & "\output-prj\languages\Cbl\" & GbNomePgm For Output As outFile
  Do While Not EOF(inFile)
    Line Input #inFile, Riga
    numRiga = numRiga + 1
    If InStr(UCase(Riga), txtPgmCalled) Then
      ' Controllo di sicurezza se sono sulla CALL e la riga corrette
      Set rs = m_fun.Open_Recordset("select * from pscall where " & _
                                    "idoggetto = " & GbIdOggetto & " and riga = " & numRiga & " and " & _
                                    "programma = '" & txtPgmCalled & "'")
      If Not rs.EOF Then
        indent = InStr(7, Riga, "CALL ") - 1
        ' Aggiungo le MOVE prima della CALL
        Print #outFile, "DELLIN" & Space(indent - 6) & "MOVE ?? TO LNKDB-NUMPCB"
        Print #outFile, "DELLIN" & Space(indent - 6) & "MOVE 'LK' TO LNKDB-MODE"
        Print #outFile, Riga
        Do While Not EOF(inFile)
          Line Input #inFile, Riga
          numRiga = numRiga + 1
          If InStr(Riga, ".") Then
            ' Tolgo il punto dall'ultima variabile e accodo la LNKDB-AREA
            indent = InStr(7, Riga, PCBName) - 1
            Print #outFile, "DELLDE*" & Mid(Riga, 8)
            Print #outFile, "DELLIN " & Mid(Replace(Riga, ".", " "), 8)
            Print #outFile, "DELLIN" & Space(indent - 6) & "LNKDB-AREA."
            Exit Do
          Else
            Print #outFile, Riga
          End If
        Loop
      Else
        Print #outFile, Riga
      End If
    Else
      Print #outFile, Riga
    End If
  Loop
  Close inFile
  Close outFile
  Exit Sub
fileErr:
  If Err.Number = 76 Then
    'Directory non esistente!?
    m_fun.MkDir_API m_fun.FnPathPrj & "\Output-Prj\Languages\"
    m_fun.MkDir_API m_fun.FnPathPrj & "\Output-prj\Languages\Cbl"
    Resume
  Else
    m_fun.Show_MsgBoxError "RC0002", , "ConvertCBL", Err.Source, Err.Description, False
  End If
  Exit Sub
err_catch:
  m_fun.Show_MsgBoxError "RC0002", , "ConvertCBL", Err.Source, Err.Description, False
End Sub
