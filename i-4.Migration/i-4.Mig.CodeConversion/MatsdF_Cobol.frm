VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form MatsdF_Cobol 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " ENVIRONMENT ALIGN - Rehosting & Oracle migration Tool"
   ClientHeight    =   6270
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9270
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6270
   ScaleWidth      =   9270
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab SSTab1 
      Height          =   6225
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9195
      _ExtentX        =   16219
      _ExtentY        =   10980
      _Version        =   393216
      TabOrientation  =   1
      Style           =   1
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Analysis"
      TabPicture(0)   =   "MatsdF_Cobol.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Toolbar1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "TwAnaSeg"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame4"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame2"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Frame1"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Frame3"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "(1)"
      TabPicture(1)   =   "MatsdF_Cobol.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Activity"
      TabPicture(2)   =   "MatsdF_Cobol.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame6"
      Tab(2).Control(1)=   "Frame5"
      Tab(2).Control(2)=   "TabStrip1"
      Tab(2).ControlCount=   3
      Begin VB.Frame Frame6 
         Caption         =   "Macro"
         ForeColor       =   &H00000080&
         Height          =   3885
         Left            =   -74640
         TabIndex        =   16
         Top             =   1950
         Width           =   8775
         Begin VB.Frame Frame7 
            ForeColor       =   &H00000080&
            Height          =   3225
            Left            =   90
            TabIndex        =   20
            Top             =   600
            Width           =   4185
            Begin VB.TextBox TxtNomeMacro 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   375
               Left            =   990
               TabIndex        =   23
               Top             =   630
               Width           =   3135
            End
            Begin VB.TextBox TxtDescMtp 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   375
               Left            =   990
               TabIndex        =   21
               Top             =   210
               Width           =   3135
            End
            Begin MSComctlLib.ListView LwTemp 
               Height          =   2145
               Left            =   120
               TabIndex        =   25
               Top             =   1050
               Width           =   4005
               _ExtentX        =   7064
               _ExtentY        =   3784
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   -1  'True
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               _Version        =   393217
               ForeColor       =   12582912
               BackColor       =   -2147483624
               BorderStyle     =   1
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               NumItems        =   4
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "IdMod"
                  Object.Width           =   1411
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Text            =   "Description"
                  Object.Width           =   4410
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Text            =   "Command"
                  Object.Width           =   2540
               EndProperty
               BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   3
                  Text            =   "Parameter"
                  Object.Width           =   2540
               EndProperty
            End
            Begin VB.Label Label1 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "Name:"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   270
               TabIndex        =   24
               Top             =   690
               Width           =   510
            End
            Begin VB.Label LblDescMtp 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               Caption         =   "Description:"
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   60
               TabIndex        =   22
               Top             =   270
               Width           =   885
            End
         End
         Begin RichTextLib.RichTextBox RtbTemp 
            Height          =   1905
            Left            =   4290
            TabIndex        =   17
            Top             =   1890
            Width           =   4395
            _ExtentX        =   7752
            _ExtentY        =   3360
            _Version        =   393217
            BackColor       =   -2147483624
            Enabled         =   -1  'True
            ScrollBars      =   3
            RightMargin     =   10000
            TextRTF         =   $"MatsdF_Cobol.frx":0054
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSComctlLib.Toolbar Toolbar2 
            Height          =   330
            Left            =   90
            TabIndex        =   18
            Top             =   240
            Width           =   2415
            _ExtentX        =   4260
            _ExtentY        =   582
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            ImageList       =   "imlToolbarIcons"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   9
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Key             =   "Applica"
                  Object.ToolTipText     =   "Apply to Istruction"
                  ImageKey        =   "Attiva"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Key             =   "Add"
                  Object.ToolTipText     =   "Add new Template"
                  ImageIndex      =   8
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Key             =   "Save"
                  Object.ToolTipText     =   "Save Template"
                  ImageKey        =   "Save"
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Key             =   "Delete"
                  Object.ToolTipText     =   "Delete Template"
                  ImageIndex      =   6
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Macro"
                  Object.ToolTipText     =   "Edit Macro"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Run"
                  Object.ToolTipText     =   "Test Macro"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Key             =   "AppMod"
                  Object.ToolTipText     =   "Apply modify to Template"
               EndProperty
            EndProperty
         End
         Begin RichTextLib.RichTextBox RtbIstr 
            Height          =   1755
            Left            =   4290
            TabIndex        =   19
            Top             =   120
            Width           =   4395
            _ExtentX        =   7752
            _ExtentY        =   3096
            _Version        =   393217
            BackColor       =   16777152
            Enabled         =   -1  'True
            ScrollBars      =   3
            RightMargin     =   10000
            TextRTF         =   $"MatsdF_Cobol.frx":00D4
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Istructions"
         ForeColor       =   &H00000080&
         Height          =   1935
         Left            =   -74640
         TabIndex        =   13
         Top             =   30
         Width           =   8775
         Begin VB.CheckBox ChkAll 
            Caption         =   "Select All Instructions"
            Height          =   195
            Left            =   1215
            TabIndex        =   26
            Top             =   300
            Width           =   1905
         End
         Begin MSComctlLib.ListView LwIstr 
            Height          =   1275
            Left            =   90
            TabIndex        =   14
            Top             =   600
            Width           =   8595
            _ExtentX        =   15161
            _ExtentY        =   2249
            View            =   3
            LabelEdit       =   1
            MultiSelect     =   -1  'True
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   9
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Obj"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   2
               SubItemIndex    =   1
               Text            =   "Type"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   2
               Text            =   "Row"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Command"
               Object.Width           =   3704
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Param"
               Object.Width           =   2117
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   5
               Text            =   "IdMod"
               Object.Width           =   1411
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Text            =   "IdIstr"
               Object.Width           =   1411
            EndProperty
            BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   7
               Text            =   "Included"
               Object.Width           =   1411
            EndProperty
            BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   8
               Text            =   "Executed"
               Object.Width           =   1764
            EndProperty
         End
         Begin MSComctlLib.Toolbar Toolbar3 
            Height          =   330
            Left            =   90
            TabIndex        =   15
            Top             =   210
            Width           =   885
            _ExtentX        =   1561
            _ExtentY        =   582
            ButtonWidth     =   609
            ButtonHeight    =   582
            Style           =   1
            ImageList       =   "imlToolbarIcons"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Delete"
                  Object.ToolTipText     =   "Include/Exclude Instruction"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Start"
                  Object.ToolTipText     =   "Run Macro"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Object.Visible         =   0   'False
                  Key             =   "Edit"
                  Object.ToolTipText     =   "View Source"
               EndProperty
            EndProperty
         End
      End
      Begin MSComctlLib.TabStrip TabStrip1 
         Height          =   5895
         Left            =   -74970
         TabIndex        =   12
         Top             =   0
         Width           =   11935
         _ExtentX        =   21061
         _ExtentY        =   10398
         MultiRow        =   -1  'True
         Placement       =   2
         _Version        =   393216
         BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
            NumTabs         =   5
            BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "MTP"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "SQL"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "JCL"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "JOB"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "PLI"
               ImageVarType    =   2
            EndProperty
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame3 
         Caption         =   "Error Log"
         ForeColor       =   &H00000080&
         Height          =   1485
         Left            =   3510
         TabIndex        =   10
         Top             =   2130
         Width           =   5625
         Begin RichTextLib.RichTextBox RTErr 
            Height          =   1065
            Left            =   90
            TabIndex        =   11
            Top             =   270
            Width           =   5475
            _ExtentX        =   9657
            _ExtentY        =   1879
            _Version        =   393217
            BackColor       =   16777152
            ScrollBars      =   2
            TextRTF         =   $"MatsdF_Cobol.frx":0154
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Analysis Function"
         ForeColor       =   &H00000080&
         Height          =   1485
         Left            =   60
         TabIndex        =   8
         Top             =   2130
         Width           =   3405
         Begin VB.ListBox LstFunction 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   960
            Left            =   120
            Style           =   1  'Checkbox
            TabIndex        =   9
            Top             =   330
            Width           =   3165
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Select Object "
         ForeColor       =   &H00000080&
         Height          =   1665
         Left            =   60
         TabIndex        =   6
         Top             =   450
         Width           =   3405
         Begin MSComctlLib.TreeView TVSelObj 
            Height          =   1215
            Left            =   120
            TabIndex        =   7
            Top             =   300
            Width           =   3165
            _ExtentX        =   5583
            _ExtentY        =   2143
            _Version        =   393217
            HideSelection   =   0   'False
            Indentation     =   529
            LineStyle       =   1
            Style           =   7
            Checkboxes      =   -1  'True
            Appearance      =   1
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Activity Log"
         ForeColor       =   &H00000080&
         Height          =   1665
         Left            =   3510
         TabIndex        =   3
         Top             =   450
         Width           =   5625
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   120
            TabIndex        =   4
            Top             =   1320
            Width           =   5445
            _ExtentX        =   9604
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Min             =   1e-4
            Scrolling       =   1
         End
         Begin MSComctlLib.ListView LvObject 
            Height          =   975
            Left            =   90
            TabIndex        =   5
            Top             =   300
            Width           =   5475
            _ExtentX        =   9657
            _ExtentY        =   1720
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "ID"
               Object.Width           =   1411
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Object"
               Object.Width           =   3175
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Type"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Start"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Elapsed"
               Object.Width           =   1764
            EndProperty
         End
      End
      Begin MSComctlLib.TreeView TwAnaSeg 
         Height          =   2115
         Left            =   60
         TabIndex        =   2
         Top             =   3720
         Width           =   9075
         _ExtentX        =   16007
         _ExtentY        =   3731
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   529
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   330
         Left            =   0
         TabIndex        =   1
         Top             =   60
         Width           =   4935
         _ExtentX        =   8705
         _ExtentY        =   582
         ButtonWidth     =   2461
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Style           =   1
         TextAlignment   =   1
         ImageList       =   "imlToolbarIcons"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   5
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Start Analysis"
               Key             =   "Attiva"
               Object.ToolTipText     =   "Start Analysis"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   4
               Object.Width           =   1e-4
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Export case"
               Key             =   "Export"
               Object.ToolTipText     =   "Export case analyzed"
               ImageIndex      =   7
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Update"
                     Text            =   "Update"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "New"
                     Text            =   "New"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Configuration"
               Key             =   "Config"
               Object.ToolTipText     =   "Configuration"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Object.Visible         =   0   'False
               Key             =   "Help"
               Object.ToolTipText     =   "Help"
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   4920
      Top             =   6480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol.frx":01D6
            Key             =   "Attiva"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol.frx":04F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol.frx":11CA
            Key             =   "Config"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol.frx":1AA4
            Key             =   "Esporta"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol.frx":1EF6
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol.frx":2050
            Key             =   "Add"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol.frx":21AA
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol.frx":2304
            Key             =   "Macro1"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol.frx":299E
            Key             =   "Run"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_Cobol.frx":2AF8
            Key             =   "Macro"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MatsdF_Cobol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim IstrSelected As Long
Dim M_Template As String
Dim M_Area As String
Dim wStrStep() As String
'SQ 15-03-06
'Dim countOutLines As Integer
Dim offSet As Long
Public formParent As Form

Public Sub Resize()
  On Error Resume Next
  Me.Move Menu.TsLeft, Menu.TsTop, Menu.TsWidth, Menu.TsHeight
  
  SSTab1.TabVisible(1) = True
  SSTab1.Tab = 1
  SSTab1.Move 30, 30, Menu.TsWidth - 90, Menu.TsHeight - 350
    
  ' tab analysis
  Toolbar1.Move 60, 60, SSTab1.Width - 120, Toolbar1.Height
  Frame2.Move 60, Toolbar1.Top + Toolbar1.Height + 60, Frame2.Width, Frame2.Height
  Frame1.Move 60, Frame2.Top + Frame2.Height + 30, Frame1.Width, Frame1.Height
  Frame4.Move Frame2.Left + Frame2.Width + 60, Frame2.Top, SSTab1.Width - Frame2.Left - Frame2.Width - 120, Frame2.Height
  Frame3.Move Frame1.Left + Frame1.Width + 60, Frame1.Top, Frame4.Width, Frame1.Height
  
  RTErr.Move 120, 240, Frame3.Width - 240, Frame3.Height - 360
  
  TVSelObj.Move 120, 240, Frame2.Width - 240, Frame2.Height - 360
  TwAnaSeg.Move Frame1.Left, TwAnaSeg.Top, Toolbar1.Width, SSTab1.Height - 500 - Frame3.Height - Frame3.Top
  LstFunction.Move 120, 240, Frame1.Width - 240, Frame1.Height - 360
  LvObject.Move 120, 240, Frame4.Width - 240, Frame4.Height - 420 - ProgressBar1.Height
  ProgressBar1.Move LvObject.Left, ProgressBar1.Top, LvObject.Width, ProgressBar1.Height
  
  
   ' tab mtp
  TabStrip1.Move TabStrip1.Left, TabStrip1.Top, SSTab1.Width - 300, SSTab1.Height - 600
  Frame5.Move TabStrip1.Left + 400, TabStrip1.Top + 120, TabStrip1.Width - 500, (TabStrip1.Height - 600) / 2
  
  Toolbar3.Move 120, 230, Frame5.Width - 240, Toolbar3.Height
  LwIstr.Move 120, Toolbar3.Height + Toolbar3.Top + 60, Toolbar3.Width, Frame5.Height - 180 - Toolbar3.Height - Toolbar3.Top
  
  Frame6.Move Frame5.Left, Frame5.Top + Frame5.Height + 60, Frame5.Width, TabStrip1.Height - 300 - Frame5.Height
  Toolbar2.Move Toolbar2.Left, Toolbar2.Top, Frame7.Width, Toolbar2.Height
  
  Frame7.Move Frame7.Left, Frame7.Top, Frame7.Width, Frame6.Height - Frame7.Top - 200
  LwTemp.Move LwTemp.Left, LwTemp.Top, LwTemp.Width, Frame7.Height - LwTemp.Top - 200
  
  RtbIstr.Move RtbIstr.Left, Toolbar2.Top, Frame6.Width - Frame7.Left - Frame7.Width - 120, (Frame6.Height - 240) / 2
  RtbTemp.Move RtbIstr.Left, RtbIstr.Top + RtbIstr.Height + 60, RtbIstr.Width, RtbIstr.Height
  
  SSTab1.Tab = 0
 ' SSTab1.TabVisible(4) = False
  SSTab1.TabVisible(1) = False
End Sub

Private Sub ChkAll_Click()
   
   Dim i As Integer, MyItem As ListItem
   
   If ChkAll.Value = 1 Then
   
      For i = 1 To LwIstr.ListItems.Count
         
         Set MyItem = LwIstr.ListItems(i)
         MyItem.Selected = True
         
         
      Next i
      
   Else
      For i = 1 To LwIstr.ListItems.Count
         
         Set MyItem = LwIstr.ListItems(i)
         MyItem.Selected = False
         
      Next i
   End If
   
   LwIstr.Refresh
   LwIstr.SetFocus
 
End Sub

Private Sub Form_Load()
  'Setta il parent
  'SetParent Me.hWnd, Menu.TsParent
  
  SSTab1.TabVisible(1) = False
  'SQ M_Template = Me.Caption
  M_Template = "i-4.Migration - Environment Align"
  M_Area = "MTP"
'  LstFunction.AddItem "Cobol for MVS"
  LstFunction.AddItem "CICS to MTP"
'  LstFunction.AddItem "Verify and Align DLI Features"
  LstFunction.AddItem "DB2 to ORACLE"
  LstFunction.AddItem "JCL to MBM"
  LstFunction.AddItem "PLI to PLI-Liant"
  
  TxtNomeMacro.Enabled = True
  Toolbar2.Buttons("Macro").Enabled = True
  Toolbar2.Buttons("Run").Enabled = True
  Toolbar2.Buttons("AppMod").Enabled = True
  RtbTemp.Locked = True
  RtbTemp.Enabled = True
  
  LstFunction.Selected(0) = True
  NumFeatures = 1
  NumObjInMod = 0

  TVSelObj.Nodes.Add , , "SEL", "Only Selected Object"
  TVSelObj.Nodes.Add , , "SRC", "Sources"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CBL", "Cobol Program"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CPY", "Cobol Copy"
  TVSelObj.Nodes.Add "SRC", tvwChild, "JCL", "MVS-JCL"
  TVSelObj.Nodes.Add "SRC", tvwChild, "JOB", "VSE-JCL"
  TVSelObj.Nodes.Add "SRC", tvwChild, "PLI", "PLI Program"
  
  'Resize
  IstrSelected = -1
  ReDim AC_CblWord(0)
  
  ReDim Preserve AC_CblWord(1)
  AC_CblWord(1) = "TEST"
  ReDim Preserve AC_CblWord(2)
  AC_CblWord(2) = "TERMINAL"
  ReDim Preserve AC_CblWord(3)
  AC_CblWord(3) = "CD"
  ReDim Preserve AC_CblWord(4)
  AC_CblWord(4) = "TEXT"
  ReDim Preserve AC_CblWord(5)
  AC_CblWord(5) = "MESSAGE"
  ReDim Preserve AC_CblWord(6)
  AC_CblWord(6) = "END-CALL"
  ReDim Preserve AC_CblWord(7)
  AC_CblWord(7) = "CANCEL"

  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
  
  EditSysMigr = ""
End Sub

Sub AttivaCheck()
  Dim k As Integer
  Dim IdOggetto As Long
  Dim StartTime As Variant, EndTime As Variant
  Dim tb As Recordset
  Dim wKey As String
  
  LvObject.ListItems.Clear
  RTErr.text = ""
  ProgressBar1.Value = 1
  ProgressBar1.Max = NumObjInMod + 1
  
  ActCobol = False
  ActExecCics = LstFunction.Selected(0)
  ActExecDB2 = LstFunction.Selected(1)
  ActExecJCL = LstFunction.Selected(2)
  ActExecPLI = LstFunction.Selected(3)
  
  TwAnaSeg.Nodes.Clear

  If TVSelObj.Nodes(1).Checked Then
    For k = 1 To Menu.TsObjList.ListItems.Count
      If Menu.TsObjList.ListItems(k).Selected Then
        IdOggetto = Val(Menu.TsObjList.ListItems(k).text)
        Set TbOggetti = m_fun.Open_Recordset("select nome,tipo,ParsingLevel FROM Bs_oggetti where IdOggetto = " & IdOggetto)
'       If TbOggetti!tipo = "CBL" Or TbOggetti!tipo = "CPY" Then
        With LvObject.ListItems.Add(, , Format(IdOggetto, "000000"))
          .SubItems(1) = TbOggetti!Nome
          .SubItems(2) = TbOggetti!Tipo
          StartTime = Timer
          .SubItems(3) = Format(Now, "HH:MM:SS")
          .EnsureVisible
          ProgressBar1.Value = ProgressBar1.Value + 1
          ProgressBar1.Refresh
          '''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' SQ: se non � stato parserato bisogner� dirglelo...
          '''''''''''''''''''''''''''''''''''''''''''''''''''''
          If TbOggetti!ParsingLevel Then
            ' Mauro 19/02/2009: Analizza anche le Schede Parametro
            'If ActExecJCL And (TbOggetti!Tipo = "JCL" Or TbOggetti!Tipo = "PRC" Or TbOggetti!Tipo = "MBR") Then
            If ActExecJCL And (TbOggetti!Tipo = "JCL" Or TbOggetti!Tipo = "PRC" Or _
                               TbOggetti!Tipo = "MBR" Or TbOggetti!Tipo = "PRM") Then
              Analizza_JCL IdOggetto
            End If
            If ActExecJCL And TbOggetti!Tipo = "JOB" Then
              Analizza_JOB IdOggetto
            End If
            If ActExecCics And (TbOggetti!Tipo = "CBL" Or TbOggetti!Tipo = "CPY") Then
              Analizza_Mtp IdOggetto
            End If
            If ActExecPLI And (TbOggetti!Tipo = "PLI") Then
              Analizza_PLI IdOggetto
            End If
            'SQ 31-12-06
            'If ActExecDB2 And (TbOggetti!Tipo = "CBL" Or TbOggetti!Tipo = "CPY") Then
            If ActExecDB2 Then
              'SQ Sto metodo fa schifo... trovare una soluzione
              Select Case TbOggetti!Tipo
                Case "CBL", "CPY", "PLI", "INC"
                  Analizza_Db2 IdOggetto
                Case Else
                  'nop
              End Select
            End If
          Else
            RTErr.text = IIf(Len(RTErr.text), RTErr.text & vbCrLf, "") & "Object " & TbOggetti!Nome & " skipped: parsing needed."
          End If
          EndTime = Timer
          EndTime = EndTime - StartTime
          .SubItems(4) = FormatX(EndTime)
        End With
      '  End If
        TbOggetti.Close
      End If
    Next k
  Else
    '''''''''''''''''''''''''''''''''''''
    ' SELECT ALL!
    '''''''''''''''''''''''''''''''''''''
    For k = 2 To TVSelObj.Nodes.Count
      If TVSelObj.Nodes(k).Checked Then
        wKey = TVSelObj.Nodes(k).key
        If wKey = "MPS" Then
          wKey = "BMS"
        End If
        Set tb = m_fun.Open_Recordset("Select idOggetto from bs_Oggetti where tipo = '" & wKey & "'")
        While Not tb.EOF
          IdOggetto = tb!IdOggetto
          Set TbOggetti = m_fun.Open_Recordset("select nome,tipo,ParsingLevel from Bs_oggetti where IdOggetto = " & IdOggetto)
          With LvObject.ListItems.Add(, , Format(IdOggetto, "000000"))
            .SubItems(1) = TbOggetti!Nome
            .SubItems(2) = TbOggetti!Tipo
            StartTime = Timer
            .SubItems(3) = Format(Now, "HH:MM:SS")
            ProgressBar1.Value = ProgressBar1.Value + 1
            ProgressBar1.Refresh
            .EnsureVisible
            LvObject.Refresh
            If TbOggetti!ParsingLevel Then
              If ActExecJCL And (TbOggetti!Tipo = "JCL" Or TbOggetti!Tipo = "JOB" Or TbOggetti!Tipo = "PRC" Or TbOggetti!Tipo = "MBR") Then
                Analizza_JCL IdOggetto
              End If
              If ActExecCics And (TbOggetti!Tipo = "CBL" Or TbOggetti!Tipo = "CPY") Then
                Analizza_Mtp IdOggetto
              End If
              'SQ 31-12-06
              'If ActExecDB2 And (TbOggetti!Tipo = "CBL" Or TbOggetti!Tipo = "CPY") Then
              If ActExecDB2 Then
                'SQ Sto metodo fa schifo... trovare una soluzione
                Select Case TbOggetti!Tipo
                  Case "CBL", "CPY", "PLI", "INC"
                    Analizza_Db2 IdOggetto
                  Case Else
                    'nop
                End Select
              End If
              If ActExecPLI And (TbOggetti!Tipo = "PLI") Then
                Analizza_PLI IdOggetto
              End If
            Else
              RTErr.text = IIf(Len(RTErr.text), RTErr.text & vbCrLf, "") & "Object " & TbOggetti!Nome & " skipped: need parsing."
            End If
            EndTime = Timer
            EndTime = EndTime - StartTime
            .SubItems(4) = FormatX(EndTime)
          End With
          tb.MoveNext
        Wend
        tb.Close
      End If
    Next k
  End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
   
  If m_fun.FnProcessRunning = True Then
    wScelta = m_fun.Show_MsgBoxError("FB01I")
      
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  'm_fun.RemoveActiveWindows MatsdF_RegExp
  m_fun.FnActiveWindowsBool = False
End Sub

Private Sub LstFunction_Click()
'  SSTab1.TabEnabled(1) = LstFunction.Selected(0)
'  SSTab1.TabEnabled(2) = LstFunction.Selected(1)
'  SSTab1.TabEnabled(3) = LstFunction.Selected(2)

  SSTab1.TabEnabled(1) = False
'  tabstrip1.Tabs(1). = LstFunction.Selected(0)
'  SSTab1.TabEnabled(3) = LstFunction.Selected(1)
End Sub

Sub MtpApplTemplate()
  Dim wIdIstr As Long
  Dim wKeyMod As Long
  
  Dim tb As Recordset
  Dim k As Integer
  
  wIdIstr = LwIstr.SelectedItem.SubItems(6)
  wKeyMod = LwIstr.SelectedItem.SubItems(5)
  If wKeyMod = Val(LwTemp.SelectedItem.text) Then
    MsgBox "The selected instruction is already associated to this template", vbInformation, M_Template & ": Join Template"
    Exit Sub
  End If
  Set tb = m_fun.Open_Recordset("select * from TL_allignistr where numistr = " & wIdIstr)
  If tb.RecordCount = 0 Then
    '???????????????????????
    MsgBox "There is a severe problem with Repository.", vbCritical, M_Template & ": Join Template"
    tb.Close
    Exit Sub
  End If
  tb!keymodappl = LwTemp.SelectedItem.text
  tb.Update
  tb.Close
  
  LwIstr.SelectedItem.SubItems(5) = LwTemp.SelectedItem.text
End Sub

Sub MtpDeleteTemplate()
  Dim wIdKey As Long
  Dim tb As Recordset
  Dim wResp As Variant
  
  wResp = MsgBox("Do you want to delete the selected template?", vbYesNo + vbQuestion, M_Template & ": Delete Template")
  If wResp = vbYes Then
    wIdKey = Val(LwTemp.SelectedItem.text)
    Set tb = m_fun.Open_Recordset("select * from TL_AllignIstr where keymodappl = " & wIdKey)
    If tb.RecordCount Then
      MsgBox "This template can't be deleted because it has instructions associated", vbInformation, M_Template & ": Delete template"
    Else
      LwTemp.ListItems.Remove LwTemp.SelectedItem.index
      LwTemp.ListItems(1).Selected = True
      LwTemp_Click
    End If
  End If
End Sub

Sub MtpAddTemplate()
  Dim wCmd As String, wPar As String
  Dim tb As Recordset
 
  wCmd = LwTemp.SelectedItem.SubItems(2)
  wPar = LwTemp.SelectedItem.SubItems(3)
  If wPar = "" Then wPar = " "
   
  Set tb = m_fun.Open_Recordset("Select * from TL_keymod where comando = '" & wCmd & "' and parametro = '" & wPar & "' ")
  tb.AddNew
  tb!comando = wCmd
  tb!parametro = wPar
  tb!Descrizione = "New Template for " & wCmd & "(" & wPar & ")"
  tb!template = "*----------------------------------*" & vbCrLf & "* New Template " & vbCrLf & "*----------------------------------*"
  tb.Update
  '?
  Set tb = m_fun.Open_Recordset("Select * from TL_keymod where comando = '" & wCmd & "' and parametro = '" & wPar & "' order by keymod desc")
  With LwTemp.ListItems.Add(, , tb!Keymod)
    .SubItems(1) = tb!Descrizione
    .SubItems(2) = tb!comando
    .SubItems(3) = tb!parametro
  End With
  tb.Close
   
  LwTemp.ListItems(LwTemp.ListItems.Count).Selected = True
  LwTemp_Click
   
End Sub
Sub MtpSalvaTemplate()
  Dim tb As Recordset
   
  Set tb = m_fun.Open_Recordset("select * from TL_KeyMod where keymod = " & LwTemp.SelectedItem.text)
  If tb.RecordCount = 0 Then
    MsgBox "Template not found!", vbCritical, M_Template & ": Save Template"
    tb.Close
    Exit Sub
  End If
   
  'SQ: ???
  If Not RtbTemp.Locked Then
    tb!template = RtbTemp.TextRTF
    tb!Macro = " "
  Else
    tb!template = " "
    tb!Macro = TxtNomeMacro
  End If
  If TxtDescMtp = "" Then TxtDescMtp = " "
  tb!Descrizione = TxtDescMtp
  tb.Update
  tb.Close
  
  LwTemp.SelectedItem.SubItems(1) = TxtDescMtp
End Sub

Sub MtpViewIstrSelected()
  Dim wIdObj As Long
  Dim wRiga As Long
  Dim wParam As String
  Dim wCmd As String
  Dim wPos As Long
  Dim wKeyMod As Long
  Dim wLinee As Integer
  Dim tb As Recordset
  Dim rsLines As Recordset
  Dim k As Integer
  Dim wColorBase As Variant
  Dim wColorSelect As Variant
  
  If LwIstr.ListItems.Count = 0 Then Exit Sub
  
  'SQ
  IstrSelected = LwIstr.SelectedItem.index
  
  If LwIstr.SelectedItem.SubItems(7) = "No" Then
    LwIstr.ListItems(IstrSelected).Selected = True
    LwIstr.SelectedItem.EnsureVisible
    Exit Sub
  End If

  wColorBase = &H80000008
  wColorSelect = vbBlue
  If IstrSelected > -1 Then
    LwIstr.ListItems(IstrSelected).ForeColor = wColorBase
    LwIstr.ListItems(IstrSelected).Bold = False
    For k = 1 To LwIstr.ListItems(IstrSelected).ListSubItems.Count
      LwIstr.ListItems(IstrSelected).ListSubItems(k).ForeColor = wColorBase
      LwIstr.ListItems(IstrSelected).ListSubItems(k).Bold = False
    Next k
  End If
  IstrSelected = LwIstr.SelectedItem.index
  LwIstr.ListItems(IstrSelected).ForeColor = wColorSelect
  LwIstr.ListItems(IstrSelected).Bold = True
  For k = 1 To LwIstr.ListItems(IstrSelected).ListSubItems.Count
    LwIstr.ListItems(IstrSelected).ListSubItems(k).ForeColor = wColorSelect
    LwIstr.ListItems(IstrSelected).ListSubItems(k).Bold = True
  Next k
  LwIstr.Refresh
  
  RtbIstr.text = ""
  wIdObj = LwIstr.SelectedItem.tag
  wRiga = LwIstr.SelectedItem.SubItems(2)
  wCmd = LwIstr.SelectedItem.SubItems(3)
  wParam = LwIstr.SelectedItem.SubItems(4)
  wKeyMod = LwIstr.SelectedItem.SubItems(5)
  Set rsLines = m_fun.Open_Recordset("select linee from PsExec where " & _
                                     "idoggetto = " & wIdObj & " and riga = " & wRiga)
  If rsLines.RecordCount Then
    wLinee = rsLines!Linee
  End If
  
  Select Case M_Area
    Case "JOB"
      RtbIstr.text = GetOriginalStep(wIdObj, wRiga)
      'SQ
      'Non si vedeva niente: testo in basso a destra...
      'Posiziono il cursore per avere le barre di scorrimento all'inizio
      RtbIstr.SelStart = 0
    Case "JCL", "PRC", "MBR"
      RtbIstr.text = TrattaStepJcl(wIdObj, wRiga, "Get")
      'SQ
      'Non si vedeva niente: testo in basso a destra...
      'Posiziono il cursore per avere le barre di scorrimento all'inizio
      RtbIstr.SelStart = 0
    Case Else
      RtbIstr.text = getOriginalInstruction(M_Area, wIdObj, wRiga, wLinee)
      RtbIstr.SelStart = 0
  End Select
  
  TxtNomeMacro = ""
  LwTemp.ListItems.Clear
  k = 1
  Set tb = m_fun.Open_Recordset("select * from tl_keymod where keymod = " & wKeyMod)
  If tb.RecordCount Then
    While Not tb.EOF
      With LwTemp.ListItems.Add(, , tb!Keymod)
        .SubItems(1) = tb!Descrizione & ""
        .SubItems(2) = tb!comando
        .SubItems(3) = tb!parametro & ""
      End With
      If wKeyMod = tb!Keymod Then k = LwTemp.ListItems.Count
      tb.MoveNext
    Wend
    LwTemp.ListItems(k).Selected = True
    LwTemp_Click
  End If
  tb.Close
End Sub

Private Sub CopiaOut(wIdObj)
  Dim tb As Recordset
  Dim NomeInput As String, NomeOutPut As String
  
  On Error GoTo ErrorHandler
  
  'HO GIA' TUTTO!!!!!!!!!!!!!!!!!!!!!! RIFACCIAMO LE QUERY?
  Set tb = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & wIdObj)
  NomeInput = tb!directory_input & "\" & tb!Nome
  If Trim(tb!estensione) <> "" Then
    NomeInput = NomeInput & "." & tb!estensione
  End If
  NomeInput = Replace(NomeInput, "$", Menu.TsPathDef)
  
  NomeOutPut = tb!directory_input & "\out\" & tb!Nome
  If Trim(tb!estensione) <> "" Then
    NomeOutPut = NomeOutPut & "." & tb!estensione
  End If
  NomeOutPut = Replace(NomeOutPut, "$", Menu.TsPathDef)
  
  tb.Close
  If m_fun.Check_If_License_Update Then
    FileCopy NomeOutPut, NomeInput
  End If
  Kill NomeOutPut
  
  Exit Sub
ErrorHandler:
  m_fun.WriteLogMacro Now & " - " & NomeInput & " - " & Err.Description & vbCrLf, False
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Legge le linee di istruzione (Riga+linee della PsExec)
' dal file originale
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getOriginalInstruction(wArea As String, IdOggetto As Long, riga As Long, ByVal Linee As Integer) As String
  'Dim linee As Integer
  Dim rs As Recordset
  Dim NomeInput As String
  Dim nFileIn As Long, currentLine As Long
  Dim Line As String
  Dim lineOk As Boolean
  
  'Numero linee (salvarlo nella temporanea?!)
'''  Set rs = m_fun.Open_Recordset("select linee from PsExec where idoggetto = " & IdOggetto & " and Riga = " & riga)
'''  If rs.RecordCount Then
'''    linee = rs!linee
'''  Else
'''    MsgBox "tmp: Instruction not found.", vbExclamation
'''    rs.Close
'''    Exit Function
'''  End If
  
  Set rs = m_fun.Open_Recordset("select Nome,Directory_Input,Estensione from bs_oggetti where idoggetto = " & IdOggetto)
  NomeInput = rs!directory_input & "\" & rs!Nome
  If Trim(rs!estensione) <> "" Then
    NomeInput = NomeInput & "." & rs!estensione
  End If
  NomeInput = Replace(NomeInput, "$", Menu.TsPathDef)
  rs.Close
  
  nFileIn = FreeFile
  Open NomeInput For Input As nFileIn
     
  Do While Not EOF(nFileIn)
    Line Input #nFileIn, Line
    currentLine = currentLine + 1
    If lineOk Then
      If Linee = 0 Then Exit Do
      getOriginalInstruction = getOriginalInstruction & vbCrLf & Line
      Linee = Linee - 1
    Else
      If currentLine = riga Then
        'Prima riga valida
        getOriginalInstruction = Line
        Linee = Linee - 1
        lineOk = True
      End If
    End If
  Loop
  Close nFileIn
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Sostituisce le linee di istruzione (Riga+linee della PsExec)
' dal file originale con newInstruction;
' Si appoggia nella dir "\out" e lo ricopia successivamente
''
' Ritorna true se tutto OK
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function writeUpdatedInstruction(IdOggetto As Long, riga As Long, newInstruction As String, ByVal Linee As Integer) As String
  'Dim linee As Integer
  Dim rs As Recordset
  Dim inputFile As String, outputFile As String, inputDir As String
  Dim nFileIn As Long, nFileOut As Long, currentLine As Long
  Dim Line As String
  Dim lineOk As Boolean
  
  On Error GoTo writeErr
  
  'Numero linee (salvarlo nella temporanea?!)
'''  Set rs = m_fun.Open_Recordset("select linee from PsExec where idoggetto = " & IdOggetto & " and Riga = " & riga)
'''  If rs.RecordCount Then
'''    linee = rs!linee
'''  Else
'''    MsgBox "tmp: Instruction not found.", vbExclamation
'''    rs.Close
'''    Exit Function
'''  End If
  
  Set rs = m_fun.Open_Recordset("select Nome,Directory_Input,Estensione from bs_oggetti where idoggetto = " & IdOggetto)
  inputDir = Replace(rs!directory_input, "$", Menu.TsPathDef) & "\"
  If Dir(inputDir & "out\", vbDirectory) = "" Then
    MkDir inputDir & "out"
  End If
  inputFile = inputDir & rs!Nome
  outputFile = inputDir & "out\" & rs!Nome
  If Trim(rs!estensione) <> "" Then
    inputFile = inputFile & "." & rs!estensione
    outputFile = outputFile & "." & rs!estensione
  End If
  rs.Close
  
  nFileIn = FreeFile
  Open inputFile For Input As nFileIn
  nFileOut = FreeFile
  Open outputFile For Output As nFileOut
     
  Do While Not EOF(nFileIn)
    Line Input #nFileIn, Line
    currentLine = currentLine + 1
    If lineOk Then
      'Nuova Istruzione: bypasso le vecchie...
      'getOriginalInstruction = getOriginalInstruction & vbCrLf & line
        Linee = Linee - 1
      ' Mauro 22/02/2008: Se l'istruzione � su una sola riga, qui arriva con -1
      'If Linee = 0 Then
      If Linee < 1 Then
        'Scrittura nuova istruzione
        'Linea da mantenere
        Print #nFileOut, newInstruction
        lineOk = False
        ' Mauro 22/02/2008: Devo riscrivere la linea originale solo per le istruzioni su una sola riga
        'senn� me la mangia e si scasinano gli offset
        If Linee < 0 Then
          Print #nFileOut, Line
        End If
      End If
    Else
      If currentLine = riga Then
        'Prima riga di EXEC
        'getOriginalInstruction = line
        Linee = Linee - 1
        lineOk = True
      Else
        'Linea da mantenere
        Print #nFileOut, Line
      End If
    End If
  Loop
  Close nFileIn
  Close nFileOut
  
  '''''''''''''''''''''''''''''''''''''''''''''
  'Tutto OK: ho il file modificato nella "out"
  ' Sovrascrivo quello di input
  '''''''''''''''''''''''''''''''''''''''''''''
  writeUpdatedInstruction = True
  
  If m_fun.Check_If_License_Update Then
    FileCopy outputFile, inputFile
  End If
  Kill outputFile
  
  Exit Function
writeErr:
  m_fun.WriteLogMacro Now & " - Problems with " & IdOggetto & " object!" & vbCrLf, False
  m_fun.WriteLog "#writeUpdatedInstruction: id: " & IdOggetto & "- Err: " & Err.Description
  writeUpdatedInstruction = False
End Function

Private Function TrattaStepJcl(wIdObj As Long, wStep As Long, wOper As String, Optional wBufIns As String) As String
  Dim NomeInput As String, NomeOutPut As String
  Dim nFileIn As Variant, nFileOu As Variant
  Dim wRec As String, WBuf As String, wBuffer As String, wStr As String, wTipo As String, wDir As String
  Dim k As Integer, nStep As Integer
  Dim tb As Recordset
  Dim Kout As Long
  
  Set tb = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & wIdObj)
  
  NomeInput = tb!directory_input & "\" & tb!Nome
  If Len(Trim(tb!estensione)) Then
    NomeInput = NomeInput & "." & tb!estensione
  End If
  wTipo = tb!Tipo
  ''''''''''''''''''''''''
  ' SQ: tmp
  ''''''''''''''''''''''''
  If wTipo = "PRC" Or wTipo = "MBR" Then
    wTipo = "JCL"
  End If
  NomeInput = Replace(NomeInput, "$", Menu.TsPathDef)
  nFileIn = FreeFile
  Open NomeInput For Input As nFileIn
  
  If wOper = "Put" Then
    wDir = tb!directory_input & "\out"
    wDir = Replace(wDir, "$", Menu.TsPathDef)
    If Dir(wDir, vbDirectory) = "" Then
      MkDir wDir
    End If
    NomeOutPut = tb!directory_input & "\Out\" & tb!Nome
    If Len(Trim(tb!estensione)) Then
      NomeOutPut = NomeOutPut & "." & tb!estensione
    End If
    NomeOutPut = Replace(NomeOutPut, "$", Menu.TsPathDef)
    nFileOu = FreeFile
    Open NomeOutPut For Output As nFileOu
  End If
  tb.Close
  
  k = 0
  WBuf = ""
  If wTipo = "JCL" Then
    nStep = 0
    wStep = wStep + 1
  Else
    nStep = 1
  End If
   
  While Not EOF(nFileIn)
    DoEvents
    wRec = ""
    Line Input #nFileIn, wRec
    If Left(wRec, 3) <> "//*" Then
      If InStr(wRec, " EXEC ") > 0 Or InStr(wRec, "/EXEC ") > 0 Then
        nStep = nStep + 1
        If wTipo = "JCL" Then
          If nStep <> wStep Then
            WBuf = wRec
          End If
        Else
          If nStep = wStep Then
            WBuf = WBuf & vbCrLf & wRec
          Else
            If wOper = "Get" Then
              wRec = " "
              While Not EOF(nFileIn) And Left(wRec, 1) <> "/"
                Line Input #nFileIn, wRec
                If Left(wRec, 1) <> "/" Or Left(wRec, 2) = "/*" Then
                  WBuf = WBuf & vbCrLf & wRec
                End If
              Wend
              WBuf = ""
            Else
              Print #nFileOu, WBuf
              WBuf = ""
            End If
          End If
        End If
        wRec = ""
      End If
    End If
    
    If wRec <> "" Then
      WBuf = WBuf & vbCrLf & wRec
    End If
    
    If wStep = nStep Then
      If wOper = "Get" Then
        If wTipo = "JOB" Then
          wRec = " "
          While Not EOF(nFileIn) And Mid$(wRec, 1, 1) <> "/"
            Line Input #nFileIn, wRec
            If Mid$(wRec, 1, 1) <> "/" Then
              WBuf = WBuf & vbCrLf & wRec
            ElseIf Mid$(wRec, 1, 2) = "/*" Then
              WBuf = WBuf & vbCrLf & wRec
            End If
          Wend
        End If
    
        If Trim(WBuf) <> "" Then
          TrattaStepJcl = WBuf
        Else
          TrattaStepJcl = "Step not found"
        End If
        Close nFileIn
        Exit Function
      Else
        Print #nFileOu, wBufIns
        WBuf = ""
        While Not EOF(nFileIn)
          Line Input #nFileIn, wRec
          Print #nFileOu, wRec
        Wend
        Close nFileIn
        Close nFileOu
        CopiaOut wIdObj
        Exit Function
      End If
    End If
  Wend
   
  nStep = nStep + 1
  If wTipo = "JCL" Then
    If nStep <> wStep Then
      WBuf = wRec
    End If
  Else
    If nStep = wStep Then
      WBuf = WBuf & vbCrLf & wRec
      If wOper = "Put" Then
        Print #nFileOu, wBufIns
      End If
    Else
      If wOper = "Put" Then
        Print #nFileOu, WBuf
      Else
        WBuf = ""
      End If
    End If
  End If
    
  If wOper = "Get" Then
    If wStep = nStep Then
      If Trim(WBuf) <> "" Then
        TrattaStepJcl = WBuf
      Else
        TrattaStepJcl = "Step not found"
      End If
    End If
  Else
    Print #nFileOu, wBufIns
  End If

  Close nFileIn
  If wOper = "Put" Then
    Close nFileOu
    CopiaOut wIdObj
  End If
End Function

Private Function TrattaAllStepJcl(wIdObj As Long, wOper As String) As Boolean
  Dim NomeInput As String, NomeOutPut As String
  Dim nFileIn As Variant, nFileOu As Variant
  Dim wRec As String, WBuf As String, wBuffer As String
  Dim wStr As String
  Dim k As Integer
  Dim nStep As Integer
  Dim tb As Recordset
  Dim wTipo As String, Kout As Long, wDir As String
  
  Set tb = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & wIdObj)
  
  wTipo = tb!Tipo
  ''''''''''''''''''''''''
  ' SQ: tmp
  ''''''''''''''''''''''''
  If wTipo = "PRC" Or wTipo = "MBR" Then
    wTipo = "JCL"
  End If
    
  If wOper = "Put" Then
    wDir = tb!directory_input & "\out"
    wDir = Replace(wDir, "$", Menu.TsPathDef)
    If Dir(wDir, vbDirectory) = "" Then
      MkDir wDir
    End If
    NomeOutPut = tb!directory_input & "\Out\" & tb!Nome
    If Trim(tb!estensione) <> "" Then
      NomeOutPut = NomeOutPut & "." & tb!estensione
    End If
    NomeOutPut = Replace(NomeOutPut, "$", Menu.TsPathDef)
    nFileOu = FreeFile
    Open NomeOutPut For Output As nFileOu
    If wTipo = "JCL" Then
      For k = 0 To UBound(wStrStep)
        Print #nFileOu, wStrStep(k)
      Next k
    Else
      For k = 1 To UBound(wStrStep)
        Print #nFileOu, wStrStep(k)
      Next k
    End If
    Close nFileOu
    CopiaOut wIdObj
    TrattaAllStepJcl = True
    Exit Function
  End If
    
  NomeInput = tb!directory_input & "\" & tb!Nome
  If Trim(tb!estensione) <> "" Then
    NomeInput = NomeInput & "." & tb!estensione
  End If
'''    wTipo = Tb!Tipo
'''    ''''''''''''''''''''''''
'''    ' SQ: tmp
'''    ''''''''''''''''''''''''
'''    If wTipo = "PRC" Or wTipo = "MBR" Then
'''      wTipo = "JCL"
'''    End If
    
  NomeInput = Replace(NomeInput, "$", Menu.TsPathDef)
  nFileIn = FreeFile
  Open NomeInput For Input As nFileIn
  Set tb = Nothing
  k = 0
  WBuf = ""
  If wTipo = "JCL" Then
    nStep = -1
  Else
    nStep = 1
  End If
   
  ReDim wStrStep(0)
  wRec = ""
  While Not EOF(nFileIn)
    DoEvents
    Line Input #nFileIn, wRec
    If (wTipo = "JCL" And Mid$(wRec, 1, 3) <> "//*") Or _
       (wTipo = "JOB" And Mid$(wRec, 1, 1) <> "*") Then
      If InStr(wRec, " EXEC ") > 0 Or InStr(wRec, "/EXEC ") > 0 Then
        nStep = nStep + 1
        ReDim Preserve wStrStep(nStep)
        If wTipo = "JCL" Then
          wStrStep(nStep) = WBuf
          WBuf = wRec
        Else
          WBuf = WBuf & vbCrLf & wRec
          wRec = " "
          While Not EOF(nFileIn) And Mid$(wRec, 1, 1) <> "/"
            Line Input #nFileIn, wRec
            If Mid$(wRec, 1, 1) <> "/" Then
              WBuf = WBuf & vbCrLf & wRec
            ElseIf Mid$(wRec, 1, 2) = "/*" Then
              WBuf = WBuf & vbCrLf & wRec
            End If
          Wend
          wStrStep(nStep) = WBuf
          If Mid$(wRec, 1, 2) = "/*" Then
            WBuf = ""
          Else
            WBuf = wRec
          End If
        End If
      Else
        If Trim(WBuf) = "" Then
          WBuf = wRec
        Else
          WBuf = WBuf & vbCrLf & wRec
        End If
      End If
    Else
      If Trim(WBuf) = "" Then
        WBuf = wRec
      Else
        WBuf = WBuf & vbCrLf & wRec
      End If
    End If
  Wend
  Close nFileIn
  
  nStep = nStep + 1
  If Trim(WBuf) <> "" Then
    ReDim Preserve wStrStep(nStep)
    wStrStep(nStep) = WBuf
  End If
  TrattaAllStepJcl = True
End Function

'Private Sub LwIstr_Click()
'  If IstrSelected = -1 Then Exit Sub
'  LwIstr.ListItems(IstrSelected).Selected = True
'  LwIstr.SelectedItem.EnsureVisible
'End Sub

Private Sub LwIstr_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  Dim k As Integer
 
  LwIstr.SortKey = ColumnHeader.index - 1
  If LwIstr.SortOrder = lvwAscending Then
    LwIstr.SortOrder = lvwDescending
  Else
    LwIstr.SortOrder = lvwAscending
  End If
  LwIstr.Sorted = True
  
  If IstrSelected = -1 Then Exit Sub
  For k = 1 To LwIstr.ListItems.Count
    If LwIstr.ListItems(k).Bold = True Then
      IstrSelected = k
      Exit For
    End If
  Next k
  
  LwIstr.ListItems(IstrSelected).Selected = True
  LwIstr.SelectedItem.EnsureVisible
End Sub

Private Sub LwIstr_DblClick()
'  If IstrSelected = -1 Then Exit Sub
'  LwIstr.ListItems(IstrSelected).Selected = True
'  LwIstr.SelectedItem.EnsureVisible
'  IstrSelected = LwIstr.SelectedItem.Index
  MtpViewIstrSelected
End Sub

Private Sub LwTemp_Click()
  Dim tb As Recordset
  
  If LwTemp.ListItems.Count > 0 Then
    Set tb = m_fun.Open_Recordset("select * from tl_keymod where " & _
                                  "keymod = " & LwTemp.SelectedItem.text & " ")
    If tb.RecordCount Then
      RtbTemp.TextRTF = tb!template & ""
      RtbTemp.SelStart = 0
      RtbTemp.SelLength = Len(RtbTemp.text)
      RtbTemp.SelFontName = "Currier New"
      
      TxtDescMtp = tb!Descrizione & ""
      
      If Trim(tb!template & " ") <> "" Then
        TxtNomeMacro.Enabled = False
        Toolbar2.Buttons("Macro").Enabled = False
        Toolbar2.Buttons("Run").Enabled = False
        Toolbar2.Buttons("AppMod").Enabled = False
        RtbTemp.Locked = False
        TxtNomeMacro = ""
      Else
        TxtNomeMacro.Enabled = True
        Toolbar2.Buttons("Macro").Enabled = False
        Toolbar2.Buttons("Run").Enabled = False
        Toolbar2.Buttons("AppMod").Enabled = False
        RtbTemp.Locked = True
        RtbTemp.text = ""
        TxtNomeMacro = tb!Macro & ""
      End If
    End If
    tb.Close
  End If
End Sub

Private Sub RtbIstr_Change()
  RtbIstr.SelStart = 0
  RtbIstr.SelLength = Len(RtbIstr.text)
  RtbIstr.SelFontName = "Courier New"
  RtbIstr.SelFontSize = 8
End Sub

Private Sub RtbTemp_Change()
  RtbTemp.SelStart = 0
  RtbTemp.SelLength = Len(RtbIstr.text)
  RtbTemp.SelFontName = "Courier New"
  RtbTemp.SelFontSize = 8
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
  If SSTab1.Tab = 2 Then CaricaMtp
End Sub

Sub CaricaMtp()
  Dim tb As Recordset, TbIstr As Recordset
  
  TabStrip1.Visible = False
  Screen.MousePointer = vbHourglass
  LwIstr.ListItems.Clear
  
  Set TbIstr = m_fun.Open_Recordset("select * from TL_AllignIstr where area = '" & M_Area & "'and applicata = false order by idoggetto,riga,comando,parametro ")
  While Not TbIstr.EOF
    Set tb = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & TbIstr!IdOggetto & " ")
    With LwIstr.ListItems.Add(, , tb!Nome)
      .SubItems(1) = tb!Tipo
      .SubItems(2) = TbIstr!riga
      .SubItems(3) = TbIstr!comando
      .SubItems(4) = TbIstr!parametro & ""
      .SubItems(5) = TbIstr!keymodappl
      .SubItems(6) = TbIstr!NumIstr
      If TbIstr!attiva Then
        .SubItems(7) = "Yes"
      Else
        .SubItems(7) = "No"
      End If
      If TbIstr!applicata Then
        .SubItems(8) = "Yes"
      Else
        .SubItems(8) = "No"
      End If
      .tag = TbIstr!IdOggetto
    End With
    TbIstr.MoveNext
  Wend
  
  RtbIstr.text = ""
  RtbTemp.text = ""
  TxtDescMtp.text = ""
  LwTemp.ListItems.Clear
  
  TabStrip1.Visible = True
  Screen.MousePointer = vbNormal
End Sub

Private Sub TabStrip1_Click()
  M_Area = TabStrip1.SelectedItem.Caption
  IstrSelected = -1
  CaricaMtp
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  On Error Resume Next
  
  m_fun.FnProcessRunning = True
  
  Select Case Button.key
    Case "Attiva"
      If NumObjInMod = 0 Then
        MsgBox "No selected object for Analysis Check Source", vbInformation, M_Template
      Else
        If NumFeatures = 0 Then
          MsgBox "No selected function for Analysis Check Source", vbInformation, M_Template
        Else
          AttivaCheck
        End If
      End If
    Case "Export"
      ExportCase "UPDT"
    Case "Config"
      'SetParent MatsdF_RegExp.hwnd, MatsdF_Cobol.hwnd
      Load MatsdF_RegExp
      MatsdF_RegExp.Show
      'MatsdF_RegExp.Move 0, 0, MatsdF_Cobol.Width, MatsdF_Cobol.Height
    Case "Help"
  End Select
  
  m_fun.FnProcessRunning = False
End Sub

Sub ExportCase(wTipo As String)
  Dim TbIstrExp As Recordset, TbIstrKey As Recordset
  
  Dim wCmdP As String, wCmdS As String, wParm As String
  Dim wRiga As Long, wIdObj As Long, wKey As String
  Dim wCmdx As String, wStr1 As String, wSql As String
  Dim wTempl As Long
  
  Dim k As Integer, Y As Integer
  
  Screen.MousePointer = vbHourglass
  If wTipo = "NEW" Then
    Menu.TsConnection.Execute "Delete * from TL_Allignistr "
    Menu.TsConnection.Execute "Delete * from TL_keymod "
  End If
  
  RTErr.text = "Start Export Case - " & Now
  RTErr.Refresh
  For k = 1 To TwAnaSeg.Nodes.Count
    wKey = TwAnaSeg.Nodes(k).key
    If Left(wKey, 3) = "MTP" Then
      wCmdP = ""
      wCmdS = ""
      wParm = ""
      wIdObj = Val(Mid$(wKey, 4, 6))
      wRiga = Val(Mid$(wKey, 11, 6))
         
      Y = InStr(wKey, "#C#")
      If Y > 0 Then wCmdP = Mid$(wKey, Y + 3)
      Y = InStr(wCmdP, "#")
      If Y > 0 Then wCmdP = Mid$(wCmdP, 1, Y - 1)
      
      Y = InStr(wKey, "#T#")
      If Y > 0 Then wCmdS = Mid$(wKey, Y + 3)
      Y = InStr(wCmdS, "#")
      If Y > 0 Then wCmdS = Mid$(wCmdS, 1, Y - 1)
      
      Y = InStr(wKey, "#P#")
      If Y > 0 Then wParm = Mid$(wKey, Y + 3)
      Y = InStr(wParm, "#")
      If Y > 0 Then wParm = Mid$(wParm, 1, Y - 1)
      wCmdx = wCmdP
      If wCmdS <> "" Then wCmdx = wCmdx & "/" & wCmdS
      wParm = Replace(wParm, "'", "")
      Set TbIstrKey = m_fun.Open_Recordset("select * from tl_keymod where " & _
                                           "area = 'MTP' and comando = '" & wCmdx & "' and parametro = '" & wParm & "' ")
      If TbIstrKey.RecordCount = 0 Then
        TbIstrKey.AddNew
        TbIstrKey!Area = "MTP"
        TbIstrKey!comando = wCmdx
        TbIstrKey!parametro = wParm & ""
        TbIstrKey!Macro = PrendiMacroDaSys("MTP", wCmdx, wParm)
        TbIstrKey!Descrizione = PrendiDescMacroDaSys("MTP", wCmdx, wParm)
        TbIstrKey.Update
      End If
      Set TbIstrKey = m_fun.Open_Recordset("select * from tl_keymod where " & _
                                           "area = 'MTP' and comando = '" & wCmdx & "' and parametro = '" & wParm & "' ")
      wTempl = TbIstrKey!Keymod
      Set TbIstrExp = m_fun.Open_Recordset("select * from tl_allignistr where " & _
                                           "area = 'MTP' and comando = '" & wCmdx & "' and parametro = '" & wParm & "' and " & _
                                           "idoggetto = " & wIdObj & " and riga = " & wRiga & " ")
      If TbIstrExp.RecordCount = 0 Then
        TbIstrExp.AddNew
        TbIstrExp!IdOggetto = wIdObj
        TbIstrExp!riga = wRiga
        TbIstrExp!comando = wCmdx
        TbIstrExp!parametro = wParm
        TbIstrExp!keymodappl = wTempl
        TbIstrExp!Area = "MTP"
        TbIstrExp!attiva = True
        TbIstrExp!applicata = False
        TbIstrExp.Update
      End If
      TbIstrExp.Close
      TbIstrKey.Close
    End If
    If Left(wKey, 3) = "SQL" Then
      wCmdP = ""
      wCmdS = ""
      wParm = ""
      wIdObj = Val(Mid$(wKey, 4, 6))
      wRiga = Val(Mid$(wKey, 11, 6))
      
      Y = InStr(wKey, "#W#")
      If Y > 0 Then wCmdP = Mid$(wKey, Y + 3)
      Y = InStr(wCmdP, "#")
      If Y > 0 Then wCmdP = Mid$(wCmdP, 1, Y - 1)
      Set TbIstrKey = m_fun.Open_Recordset("select * from tl_keymod where area = 'SQL' and comando = '" & wCmdP & "'  ")
      If TbIstrKey.RecordCount = 0 Then
        TbIstrKey.AddNew
        TbIstrKey!Area = "SQL"
        TbIstrKey!comando = wCmdP
'        TbIstrKey!parametro = wParm
        TbIstrKey!Descrizione = PrendiDescMacroDaSys("SQL", wCmdP, wParm)
        TbIstrKey!Macro = PrendiMacroDaSys("SQL", wCmdP, wParm)
        TbIstrKey.Update
      End If
      Set TbIstrKey = m_fun.Open_Recordset("select * from tl_keymod where area = 'SQL' and comando = '" & wCmdP & "'  ")
      wTempl = TbIstrKey!Keymod
      Set TbIstrExp = m_fun.Open_Recordset("select * from tl_allignistr where area = 'SQL' and comando = '" & wCmdP & "'  and idoggetto = " & wIdObj & " and riga = " & wRiga & " ")
      If TbIstrExp.RecordCount = 0 Then
        TbIstrExp.AddNew
        TbIstrExp!IdOggetto = wIdObj
        TbIstrExp!riga = wRiga
        TbIstrExp!comando = wCmdP
'        TbIstrExp!parametro = wParm
        TbIstrExp!keymodappl = wTempl
        TbIstrExp!Area = "SQL"
        TbIstrExp!attiva = True
        TbIstrExp!applicata = False
        TbIstrExp.Update
      End If
      TbIstrExp.Close
      TbIstrKey.Close
    End If
    If Left(wKey, 3) = "JCL" Or Left(wKey, 3) = "JOB" Then
      wCmdP = ""
      wCmdS = ""
      wParm = ""
      'SQ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      'wIdObj = Val(Mid$(wKey, 6, 6))
      wIdObj = Val(Mid$(wKey, 4, 6))
      wRiga = Val(Mid$(wKey, 11, 6))
      
      Y = InStr(wKey, "#W1#")
      If Y > 0 Then wCmdP = Mid$(wKey, Y + 4)
      Set TbIstrKey = m_fun.Open_Recordset("select * from tl_keymod where area = '" & Mid$(wKey, 1, 3) & "' and comando = '" & wCmdP & "'  ")
      If TbIstrKey.RecordCount = 0 Then
        TbIstrKey.AddNew
        TbIstrKey!Area = Mid$(wKey, 1, 3)
        TbIstrKey!comando = wCmdP
'        TbIstrKey!parametro = wParm
        TbIstrKey!Descrizione = PrendiDescMacroDaSys(Mid$(wKey, 1, 3), wCmdP, wParm)
        TbIstrKey!Macro = PrendiMacroDaSys(Mid$(wKey, 1, 3), wCmdP, wParm)
        TbIstrKey.Update
      End If
      Set TbIstrKey = m_fun.Open_Recordset("select * from tl_keymod where area = '" & Mid$(wKey, 1, 3) & "' and comando = '" & wCmdP & "'  ")
      wTempl = TbIstrKey!Keymod
      Set TbIstrExp = m_fun.Open_Recordset("select * from tl_allignistr where area = '" & Mid$(wKey, 1, 3) & "' and comando = '" & wCmdP & "'  and idoggetto = " & wIdObj & " and riga = " & wRiga & " ")
      If TbIstrExp.RecordCount = 0 Then
        TbIstrExp.AddNew
        TbIstrExp!IdOggetto = wIdObj
        TbIstrExp!riga = wRiga
        TbIstrExp!comando = wCmdP
'        TbIstrExp!parametro = wParm
        TbIstrExp!keymodappl = wTempl
        TbIstrExp!attiva = True
        TbIstrExp!applicata = False
        TbIstrExp!Area = Mid$(wKey, 1, 3)
        TbIstrExp.Update
      End If
      TbIstrExp.Close
      TbIstrKey.Close
    End If
     
  Next k
  RTErr.text = RTErr.text & vbCrLf & "End Export Case - " & Now
  Screen.MousePointer = vbNormal
End Sub

Function PrendiMacroDaSys(wTipo, wCmd, wParm) As String
  Dim tb As Recordset
  Dim wSql As String
  Dim wNomeMacro As String
  Dim k As Integer
  Dim wStr As String
    
  On Error GoTo ErrorHandler
  If wTipo = "MTP" Then
    wStr = wCmd & "/" & wParm
  Else
    wStr = wCmd
  End If
  wSql = "select * from TL_Migr "
  
  wSql = wSql & " where type = '" & wTipo & "' and operazione = '" & wStr & "' "
  wNomeMacro = " "
  Set tb = m_fun.Open_Recordset(wSql)
  If tb.RecordCount Then
    If Trim(tb!template & " ") <> "" Then
      wNomeMacro = tb!template
      wNomeMacro = Replace(wNomeMacro, "@EXECUTEMACRO(", "")
      wNomeMacro = Trim(wNomeMacro)
      wNomeMacro = Mid$(wNomeMacro, 1, Len(wNomeMacro) - 1)
    End If
  Else
    If wTipo = "MTP" Then
      tb.AddNew
      If Trim(wParm) = "" Then
        wParm = " "
      End If
      tb!Type = "MTP"
      tb!Typesql = "New"
      tb!operazione = wStr
      tb!Description = "New Macro for MTP: " & wStr
      wStr = Replace(wStr, "/", "_")
      wStr = Replace(wStr, " ", "")
      tb!template = "@EXECUTEMACRO(MACRO_" & wStr & ")"
      tb!word1 = " "
      tb!word2 = " "
      tb.Update
      wNomeMacro = "MACRO_" & wStr
    End If
  End If
  tb.Close
  If Trim(wNomeMacro) = "" Then wNomeMacro = " "
  
  PrendiMacroDaSys = wNomeMacro
  Exit Function
ErrorHandler:
  MsgBox Err.Number & " " & Err.Description
End Function

Function PrendiDescMacroDaSys(wTipo, wCmd, wParm) As String
  Dim tb As Recordset
  Dim wSql As String
  Dim wDescMacro As String
  Dim k As Integer
  Dim wStr As String
  
  If wTipo = "MTP" Then
    wStr = wCmd & "/" & wParm
  Else
    wStr = wCmd
  End If
  wSql = "select * from TL_Migr "
  
  wSql = wSql & " where type = '" & wTipo & "' and operazione = '" & wStr & "' "
  wDescMacro = " "
  Set tb = m_fun.Open_Recordset(wSql)
  If tb.RecordCount Then
    If Trim(tb!Description & " ") <> "" Then
      wDescMacro = tb!Description
    End If
  End If
  tb.Close
  
  PrendiDescMacroDaSys = wDescMacro
End Function

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.key
    Case "Update"
      m_fun.FnProcessRunning = True
      ExportCase "UPDT"
      m_fun.FnProcessRunning = False
    Case "New"
      m_fun.FnProcessRunning = True
      ExportCase "NEW"
      m_fun.FnProcessRunning = False
  End Select
End Sub

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)
  If LwIstr.ListItems.Count Then
    m_fun.FnProcessRunning = True
    Select Case Button.key
      Case "Save"
        MtpSalvaTemplate
      Case "Applica"
        MtpApplTemplate
      Case "Delete"
        MtpDeleteTemplate
      Case "Add"
        MtpAddTemplate
      Case "Macro"
       'Ale 21/03/2006
        Set m_fun.formParent = Me.formParent
        m_fun.LoadMacro TxtNomeMacro, M_Area
      Case "Run"
        EseguiMacro "Show", M_Area
      Case "AppMod"
    End Select
    m_fun.FnProcessRunning = False
  Else
    'messaggino...
  End If
End Sub

Private Function FormattaStrInput(wArea, stringa) As String
  Dim wStrInput As String
  Dim k As Integer
  Dim wRiga() As String
  Dim Ir As Integer
  On Error Resume Next
   
  Ir = 0
  wStrInput = stringa
  ReDim wRiga(Ir)
   
  If wArea = "MTP" Or wArea = "SQL" Then
    k = InStr(wStrInput, vbCrLf)
    While k > 0
      Ir = Ir + 1
      ReDim Preserve wRiga(Ir)
      wRiga(Ir) = Mid$(Mid$(wStrInput, 1, k - 1) & Space$(72), 1, 72)
      wStrInput = Mid$(wStrInput, k + 2)
      k = InStr(wStrInput, vbCrLf)
    Wend
    If wStrInput <> "" Then
      Ir = Ir + 1
      ReDim Preserve wRiga(Ir)
      wRiga(Ir) = Mid$(wStrInput & Space$(72), 1, 72)
    End If
    wStrInput = ""
    For k = 1 To UBound(wRiga)
      wStrInput = wStrInput & wRiga(k) & vbCrLf
    Next k
  End If
  FormattaStrInput = wStrInput
End Function

Function LeggiParam(wKey As String) As String
  Dim wResp() As String
  On Error GoTo ErrResp
  
  wResp = m_fun.RetrieveParameterValues(wKey)
  If InStr(wResp(0), wKey) > 0 Then
    LeggiParam = ""
  Else
    LeggiParam = wResp(0)
  End If
  Exit Function
ErrResp:
  LeggiParam = ""
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Effetto collaterale:
' Setta la countOutLines...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function FormattaStrOutput(wArea, StringaOut, StringaIn) As String
  Dim wStrOutput As String, wStrInput As String
  Dim k As Integer
  Dim wRiga() As String
  Dim Ir As Integer
  Dim wTag As String
  Dim xxIn() As String, xxOut() As String
  Dim i As Integer, j As Integer
  Dim b As Boolean
  On Error Resume Next
  
  If StringaIn = StringaOut Then
    FormattaStrOutput = StringaOut
    Exit Function
  End If
   
  wStrOutput = StringaOut
  wStrInput = StringaIn
   
  Ir = 0
  ReDim wRiga(Ir)
'''  Ir = Ir + 1
'''  ReDim Preserve wRiga(Ir)
  Select Case wArea
    Case "MTP"
      wTag = Left(LeggiParam("PREFIX-MTP"), 6)
      wTag = wTag & Space(6 - Len(wTag))
      'wTag = "ITER-MTP"
'      wRiga(Ir) = "IT-MTP* - " & Now() & " - START MODIFIEDS BY I-4.MIGRATION"
    Case "SQL"
      wTag = Left(LeggiParam("PREFIX-SQL"), 6)
      wTag = wTag & Space(6 - Len(wTag))
      'wTag = "ITER-SQL"
'      wRiga(Ir) = "IT-SQL* - " & Now() & " - START MODIFIEDS BY I-4.MIGRATION"
    Case "JCL", "MBR", "PRC"
      wTag = LeggiParam("PREFIX-JCL")
      'wTag = "ITER-JCL"
      Ir = Ir + 1
      ReDim Preserve wRiga(Ir)
      wRiga(Ir) = "//*IT-JCL* - " & Now() & " - START MODIFIEDS BY I-4.MIGRATION"
    Case "JOB"
      wTag = LeggiParam("PREFIX-JOB")
      wTag = "ITER-JOB"
      Ir = Ir + 1
      ReDim Preserve wRiga(Ir)
      wRiga(Ir) = "//*IT-JOB* - " & Now() & " - START MODIFIEDS BY I-4.MIGRATION"
  End Select
   
  'iniziale
  'MG 210207
''''''   Dim ante As String
''''''   If wArea = "MTP" Or wArea = "SQL" Then
''''''     Dim xxIn() As String
''''''     Dim i As Integer
''''''     xxIn = Split(wStrInput, vbCrLf)
''''''     ante = "SQLOLD*"
''''''  End If
''''''
'''''''''''''''''MG 210207
''''''  Dim xxOut() As String
''''''  Dim j As Integer
''''''  xxOut = Split(wStrOutput, vbCrLf)

   'iniziale
'''''''''''MG 210207
  If wArea = "MTP" Or wArea = "SQL" Then
    If Right(wStrInput, 2) = vbCrLf Then wStrInput = Left(wStrInput, Len(wStrInput) - 2)
    xxIn = Split(wStrInput, vbCrLf)
  End If

'''''''''''MG 210207
  If Right(wStrOutput, 2) = vbCrLf Then wStrOutput = Left(wStrOutput, Len(wStrOutput) - 2)
  xxOut = Split(wStrOutput, vbCrLf)
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Mauro 18/11/2009: Da rivedere completamente per disallineamento tra righe!!!
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''  b = False
'''  Dim kk As Integer
'''  kk = 0
'''  For j = 0 To UBound(xxIn)
'''    ReDim Preserve wRiga(j + kk)
'''    If xxIn(j) = xxOut(j) Then
'''      If Not b Then wRiga(j) = xxIn(j)
'''      If b Then wRiga(j + kk) = xxIn(j)
'''    Else
'''      b = True
'''      'SILVIA 17-03-2009 non asteriscare la END-EXEC, qualche compilatore si arrabbia!!!
'''      'wRiga(j + kk) = wTag & "*" & IIf(InStr(Mid(xxIn(j), 8), "END-EXEC"), "", Mid(xxIn(j), 8))
'''      wRiga(j + kk) = wTag & "*" & Mid(xxIn(j), 8)
'''      kk = kk + 1
'''      'If Mid(xxOut(j), 8) <> "" Then
'''      ReDim Preserve wRiga(j + kk)
'''      wRiga(j + kk) = Left(wTag & Space(7), 7) & Mid(xxOut(j), 8) & " "
'''      'End If
'''    End If
'''  Next j

  Dim IIn As Integer, IOut As Integer, IRiga As Integer
  Dim Fine As Boolean
  IIn = 0
  IOut = 0
  IRiga = UBound(wRiga)
  Fine = False
  Do Until Fine
    If xxIn(IIn) = xxOut(IOut) Then
      ReDim Preserve wRiga(IRiga)
      IRiga = IRiga + 1
      wRiga(UBound(wRiga)) = xxIn(IIn)
      IIn = IIn + 1
      IOut = IOut + 1
    Else
      i = 0
      Do Until xxIn(IIn) = xxOut(IOut + i) Or IOut + i = UBound(xxOut)
        i = i + 1
      Loop
      If xxIn(IIn) = xxOut(IOut + i) Then
        For j = 0 To i - 1
          ReDim Preserve wRiga(IRiga)
          IRiga = IRiga + 1
          wRiga(UBound(wRiga)) = Left(wTag & Space(7), 7) & Mid(xxOut(IOut + j), 8) & " "
        Next j
        IOut = IOut + i
      ElseIf IOut + i = UBound(xxOut) Then
        ReDim Preserve wRiga(IRiga)
        IRiga = IRiga + 1
        wRiga(UBound(wRiga)) = wTag & "*" & Mid(xxIn(IIn), 8)
        IIn = IIn + 1
      End If
    End If
    If IIn > UBound(xxIn) Then
      If IOut <= UBound(xxOut) Then
        Do Until IOut > UBound(xxOut)
          ReDim Preserve wRiga(IRiga)
          IRiga = IRiga + 1
          wRiga(UBound(wRiga)) = Left(wTag & Space(7), 7) & Mid(xxOut(IOut), 8) & " "
          IOut = IOut + 1
        Loop
      End If
      Fine = True
    End If
  Loop
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'finale
  Select Case wArea
'     Case "MTP"
'       wRiga(Ir) = "IT-MTP* - " & Now() & " - END MODIFIEDS BY I-4.MIGRATION"
'     Case "SQL"
'       wRiga(Ir) = "IT-SQL* - " & Now() & " - END MODIFIEDS BY I-4.MIGRATION"
    Case "JCL", "PRC", "MBR"
      'SQ - NON FUNZIA!!! PROVA:
      k = InStr(wStrOutput, vbCrLf)
      While k > 0
        Ir = Ir + 1
        ReDim Preserve wRiga(Ir)
        wRiga(Ir) = Mid$(wStrOutput, 1, k - 1)
        wStrOutput = Mid$(wStrOutput, k + 2)
        k = InStr(wStrOutput, vbCrLf)
      Wend
      If wStrOutput <> "" Then
        Ir = Ir + 1
        ReDim Preserve wRiga(Ir)
        wRiga(Ir) = wStrOutput
        wStrOutput = ""
      End If
      '''''''''''''''''''''''''''''''''''''''''''''.
      
      Ir = Ir + 1
      ReDim Preserve wRiga(Ir)
      wRiga(Ir) = "//*IT-JCL* - " & Now() & " - END MODIFIEDS BY I-4.MIGRATION"
    Case "JOB"
      Ir = Ir + 1
      ReDim Preserve wRiga(Ir)
      wRiga(Ir) = "//*IT-JOB* - " & Now() & " - END MODIFIEDS BY I-4.MIGRATION"
  End Select

  For Ir = 0 To UBound(wRiga)
    'silvia 16-03-2009
    If Len(Trim(wRiga(Ir))) > 72 And wArea = "MTP" Then
      For k = 72 To 1 Step -1
        If Mid$(wRiga(Ir), k, 1) = " " Then
          wRiga(Ir) = Mid$(wRiga(Ir), 1, k) & Space$(72 - k) & wTag & vbCrLf & Space$(71 - (Len(wRiga(Ir)) - k)) & Mid$(wRiga(Ir), k + 1) & " " & wTag
          Exit For
        End If
      Next k
    ElseIf Len(Trim(wRiga(Ir))) > 72 And wArea = "SQL" Then
      For k = 72 To 1 Step -1
        If Mid$(wRiga(Ir), k, 1) = " " Then
          wRiga(Ir) = Mid$(wRiga(Ir), 1, k) & Space$(72 - k) & vbCrLf & Space$(IIf(71 - (Len(wRiga(Ir)) - k) > 15, 71 - (Len(wRiga(Ir)) - k), 15)) & Mid$(wRiga(Ir), k + 1)   '  & " " & wTag
          Exit For
        End If
      Next k
    Else
      If wArea <> "JCL" And wArea <> "JOB" Then
        If Len(wRiga(Ir)) > 0 Then
          wRiga(Ir) = Mid$(wRiga(Ir) & Space$(72), 1, 72) '& wTag
        End If
      End If
    End If
    'Debug.Print wRiga(Ir)
  Next Ir
   
  wStrOutput = ""
  For k = 0 To UBound(wRiga)
    ' Mauro 22/02/2008: Adesso viene brasata la riga vuota di troppo in fondo all'istruzione
''    If Len(wRiga(k)) > 0 Then
''      If k = UBound(wRiga) Then
''        wStrOutput = wStrOutput & wRiga(k)
''      Else
''        wStrOutput = wStrOutput & wRiga(k) & vbCrLf
''      End If
''    Else
''      wStrOutput = Left(wStrOutput, InStrRev(wStrOutput & vbCrLf, vbCrLf) - 1)
''    End If
    If k = UBound(wRiga) Then
      If Len(wRiga(k)) Then
        wStrOutput = wStrOutput & IIf(Len(wStrOutput), vbCrLf, "") & wRiga(k)
      End If
    Else
      wStrOutput = wStrOutput & IIf(Len(wStrOutput), vbCrLf, "") & wRiga(k)
    End If
  Next k
   
  '''''''''''''''''''''''''''''''
  ' SQ
  '''''''''''''''''''''''''''''''
  'countOutLines = UBound(wRiga)
   
  FormattaStrOutput = wStrOutput
End Function

Private Sub EseguiMacro(wMode, wArea)
  Dim wStrInput As String, wStrOutput As String
  Dim k As Integer
  Dim wRiga() As String
  Dim Ir As Integer
  Dim wTag As String
  On Error Resume Next
   
  wStrInput = RtbIstr.text
  wStrInput = FormattaStrInput(wArea, wStrInput)
'''''  wStrInput = ""
'''''  wStrInput = wStrInput & "Z05012         EXEC SQL                                                 "
'''''  wStrInput = wStrInput & vbCrLf & "Z05012             OPEN CRSR-DLAADDREXTE                              "
'''''  wStrInput = wStrInput & vbCrLf & "Z05012         END-EXEC                                                 "

  m_fun.initScriptControl
  wStrOutput = m_fun.ExecuteMacro(CLng(Val(LwIstr.SelectedItem.tag)), TxtNomeMacro, wStrInput)
  'SQ 20-3-06
  'Nessuna Macro esistente con quel nome!
  If wStrOutput <> "NOMACRO" Then
    wStrOutput = FormattaStrOutput(wArea, wStrOutput, wStrInput)
  Else
    wStrOutput = "Macro " & TxtNomeMacro & " not found!"
  End If
  m_fun.closeScriptControl
   
  RtbTemp.text = ""
   
  If wMode = "Show" Then
    RtbTemp.text = wStrOutput
  End If
  'SQ
  'Non si vedeva niente: testo in basso a destra...
  'Posiziono il cursore per avere le barre di scorrimento all'inizio
  RtbTemp.SelStart = 0
End Sub

Private Sub Toolbar3_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim wParam As New Collection
  Dim i As Integer
  
  m_fun.FnProcessRunning = True
  Select Case Button.key
    'Case "Select"
    '  MtpViewIstrSelected
    Case "Delete"
      If IstrSelected <> -1 Then
'        LwIstr.SelectedItem.SubItems(7) = "Yes"
'        MtpViewIstrSelected
'        MtpNoActiveIstr
        For i = 1 To LwIstr.ListItems.Count
          If LwIstr.ListItems(i).Selected = True Then
             LwIstr.SelectedItem.SubItems(7) = IIf(LwIstr.ListItems(i).SubItems(7) = "Yes", "No", "Yes")
             MtpViewIstrSelected
             MtpNoActiveIstr
          End If
        Next i
      End If
    Case "Start"
      ' Mauro 07/04/2008
'      If MsgBox("Do you want to convert all instructions?", vbInformation + vbYesNo, Menu.TsNameProdotto) = vbYes Then
      If MsgBox("Do you want to convert all selected instructions?", vbInformation + vbYesNo, Menu.TsNameProdotto) = vbYes Then
        ApplicaAllMacro
        IstrSelected = -1
        CaricaMtp
      End If
'    Case "Edit"  '???????????????????
'      If IstrSelected <> -1 Then
'        wParam.Add Val(LwIstr.SelectedItem.Tag)
'      End If
  End Select
  m_fun.FnProcessRunning = False
End Sub

Private Sub ApplicaAllMacro()
  Dim k As Integer
  Dim rsIstruzioni As Recordset, rsMacro As Recordset, rsOggetti As Recordset, rsOffset As Recordset
  Dim LstExecMacro() As ExecMacro
  Dim EleExecOrd As ExecMacro
  Dim Flag As Boolean
  Dim wSql As String, wStr As String
  Dim IdOggetto As Long, riga As Long
  Dim inputStatement As String, outputStatement As String, s() As String ', offSet As Long
  'Dim lastriga As Long, lastoffset As Long
  
  offSet = 0
  'lastoffset = 0
  'lastriga = 0
  ReDim LstExecMacro(k)
  Screen.MousePointer = vbHourglass
  m_fun.ClearLogMacro
  m_fun.WriteLogMacro Now & "- Start Execute Environment Allign" & vbCrLf, False
  
  '''''''''''''''''''''''''''''''''
  ' Lista Istruzioni da analizzare
  '''''''''''''''''''''''''''''''''
  If M_Area = "JCL" Or M_Area = "JOB" Then
    Set rsIstruzioni = m_fun.Open_Recordset("select a.*, b.Linee " & _
                       "from TL_AllignIstr as a, PsJCL as b " & _
                       "where a.area = '" & M_Area & "' and a.attiva " & _
                       "and not a.applicata " & _
                       "and (a.idoggetto = b.idoggetto and a.riga = b.stepnumber) " & _
                       "order by a.idoggetto, a.riga")
  Else
    Set rsIstruzioni = m_fun.Open_Recordset("select a.*, b.Linee " & _
                       "from TL_AllignIstr as a, PsExec as b " & _
                       "where a.area = '" & M_Area & "' and a.attiva " & _
                       "and not a.applicata " & _
                       "and (a.idoggetto = b.idoggetto and a.riga = b.riga) " & _
                       "order by a.idoggetto, a.riga")
  End If
  While Not rsIstruzioni.EOF
    k = k + 1
    ReDim Preserve LstExecMacro(k)
    LstExecMacro(k).IdOggetto = rsIstruzioni!IdOggetto
    LstExecMacro(k).KeyMacro = rsIstruzioni!keymodappl
    LstExecMacro(k).riga = rsIstruzioni!riga
    LstExecMacro(k).NumIstr = rsIstruzioni!NumIstr
    LstExecMacro(k).Linee = rsIstruzioni!Linee
        
    'Debug.Print LstExecMacro(k).IdOggetto & " . IdOggetto"
    'Debug.Print LstExecMacro(k).KeyMacro & " . KeyMacro"
    'Debug.Print LstExecMacro(k).riga & " . riga"
    'Debug.Print LstExecMacro(k).NumIstr & " . NumIstr"
    'Debug.Print LstExecMacro(k).Linee & " . Linee"
    'Debug.Print "--------"
    
    Set rsMacro = m_fun.Open_Recordset("select * from TL_keymod where keymod = " & rsIstruzioni!keymodappl)
    LstExecMacro(k).NameMacro = rsMacro!Macro & ""
    If M_Area = "MTP" Then
      wStr = rsIstruzioni!comando & "/" & rsIstruzioni!parametro
    Else
      wStr = rsIstruzioni!comando
    End If
    Set rsMacro = m_fun.Open_Recordset_Sys("select * from SysMigr where type = '" & M_Area & "' and operazione = '" & wStr & "' ")
    If rsMacro.RecordCount Then
      If Trim(rsMacro!Weight & " ") = "" Then
        LstExecMacro(k).OrderMacro = 0
      Else
        LstExecMacro(k).OrderMacro = rsMacro!Weight
      End If
    Else
      LstExecMacro(k).OrderMacro = 0
    End If
    rsMacro.Close
    rsIstruzioni.MoveNext
  Wend
  rsIstruzioni.Close
  
  ''''''''''''''''''''''''''''''''''''''
  ' Preparazione struttura LstExecMacro
  ''''''''''''''''''''''''''''''''''''''
  Flag = True
  While Flag
    Flag = False
    For k = 1 To UBound(LstExecMacro) - 1
      If LstExecMacro(k).IdOggetto > LstExecMacro(k + 1).IdOggetto Then
        Flag = True
        EleExecOrd = LstExecMacro(k)
        LstExecMacro(k) = LstExecMacro(k + 1)
        LstExecMacro(k + 1) = EleExecOrd
      ElseIf LstExecMacro(k).IdOggetto = LstExecMacro(k + 1).IdOggetto And _
        LstExecMacro(k).riga > LstExecMacro(k + 1).riga Then
        Flag = True
        EleExecOrd = LstExecMacro(k)
        LstExecMacro(k) = LstExecMacro(k + 1)
        LstExecMacro(k + 1) = EleExecOrd
      ElseIf LstExecMacro(k).IdOggetto = LstExecMacro(k + 1).IdOggetto And _
        LstExecMacro(k).riga = LstExecMacro(k + 1).riga And _
        LstExecMacro(k).OrderMacro > LstExecMacro(k + 1).OrderMacro Then
        Flag = True
        EleExecOrd = LstExecMacro(k)
        LstExecMacro(k) = LstExecMacro(k + 1)
        LstExecMacro(k + 1) = EleExecOrd
      End If
    Next k
  Wend
  
  m_fun.initScriptControl
  
  ''''''''''''''''''''''''''''''''''
  ' Esecuzione Macro
  ''''''''''''''''''''''''''''''''''
  For k = 1 To UBound(LstExecMacro)
    DoEvents
    '''''''''''''''''''''''''''''''''''''''''
    ' Gestione JCL/JOB
    '''''''''''''''''''''''''''''''''''''''''
    If M_Area = "JCL" Or M_Area = "JOB" Then
      If IdOggetto <> LstExecMacro(k).IdOggetto Then
        'NUOVO OGGETTO: SCRIVO IL VECCHIO
        If IdOggetto Then  '(sul primo oggetto non ho un precedente, ovviamente)
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' Scrittura nuovo statement
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          outputStatement = FormattaStrOutput(M_Area, outputStatement, inputStatement)
          wStrStep(riga) = outputStatement
          If TrattaAllStepJcl(IdOggetto, "Put") Then
            ''''''''''''''''''''''''''''''''''''
            ' Update status istruzione/oggetto
            ''''''''''''''''''''''''''''''''''''
            updateJCLStatus IdOggetto
          End If
        End If

        ''''''''''''''''''''''''''''''''''''''
        ' Nuova ISTRUZIONE:
        ''''''''''''''''''''''''''''''''''''''
        IdOggetto = LstExecMacro(k).IdOggetto
        riga = LstExecMacro(k).riga
        ''''''''''''''''''''''''''''''''''''''
        ' Valorizza wStrStep: array di "step"
        ''''''''''''''''''''''''''''''''''''''
        TrattaAllStepJcl IdOggetto, "Get"
              
        inputStatement = wStrStep(riga)
        inputStatement = FormattaStrInput(M_Area, inputStatement)
        outputStatement = inputStatement
      End If
      If IdOggetto <> LstExecMacro(k).IdOggetto Or riga <> LstExecMacro(k).riga Then
        'NUOVO OGGETTO: SCRIVO IL VECCHIO
        If IdOggetto Then '(sul primo oggetto non ho un precedente, ovviamente)
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' Scrittura nuovo statement (su array... non su file)
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          outputStatement = FormattaStrOutput(M_Area, outputStatement, inputStatement)
          wStrStep(riga) = outputStatement
        End If
        ''''''''''''''''''''''''''''''''''''''
        ' Nuova ISTRUZIONE:
        ''''''''''''''''''''''''''''''''''''''
        IdOggetto = LstExecMacro(k).IdOggetto
        riga = LstExecMacro(k).riga
        inputStatement = wStrStep(riga)
        inputStatement = FormattaStrInput(M_Area, inputStatement)
        outputStatement = ""  'init (importante... fa da flag)
      Else
        'ALTRA MACRO SULLA STESSA ISTRUZIONE
        'SQ cos� perdo l'originale!!!!!!!!!!!!!!!!!!!!!!
        'inputStatement = outputStatement
      End If
    Else
      '''''''''''''''''''''''''''''''''''''''''
      ' Gestione SQL/MTP/...
      '''''''''''''''''''''''''''''''''''''''''
      If IdOggetto <> LstExecMacro(k).IdOggetto Then
        Set rsOffset = m_fun.Open_Recordset("select * from Ps_ObjRowOffset where " & _
                                            "Idoggetto = " & LstExecMacro(k).IdOggetto)
        If rsOffset.EOF Then
          Set rsIstruzioni = m_fun.Open_Recordset("select riga from PsExec where " & _
                                                  "Idoggetto = " & LstExecMacro(k).IdOggetto)
          While Not rsIstruzioni.EOF
            rsOffset.AddNew
            rsOffset!IdOggetto = LstExecMacro(k).IdOggetto
            rsOffset!riga = rsIstruzioni!riga
            rsOffset.Update
            rsIstruzioni.MoveNext
          Wend
          rsIstruzioni.Close
        End If
        rsOffset.Close
      End If
      
      If IdOggetto <> LstExecMacro(k).IdOggetto Or riga <> LstExecMacro(k).riga Then
        'NUOVO OGGETTO: SCRIVO IL VECCHIO
        If IdOggetto Then  '(sul primo oggetto non ho un precedente, ovviamente)
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' Scrittura nuovo statement
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' Quando cambia oggetto azzero l'offset totale e preparo la tabella degli offset
          
          'SQ - TMP: GESTIRE MEGLIO...
          If outputStatement <> "NOMACRO" Then
            outputStatement = FormattaStrOutput(M_Area, outputStatement, inputStatement)
            'TrattaIstrFromSource M_Area, idOggetto, riga, "Put", outputStatement
            'SQ
            If writeUpdatedInstruction(IdOggetto, riga, outputStatement, LstExecMacro(k - 1).Linee) Then
              'tutto ok
              ''''''''''''''''''''''''''''''''''''
              ' Update status istruzione/oggetto
              ''''''''''''''''''''''''''''''''''''
              ' ATTENZIONE: aggiornamento riga/linee delle PsExec e TL_AllignIstr
              updateInstructionStatus IdOggetto, riga
              s = Split(outputStatement, vbCrLf)
              ' Mauro 22/02/2008: Le istruzioni su una sola riga vanno gestite a parte, senn� si incarta tutto
              If UBound(s) > 1 Then
                offSet = (UBound(s) + 1) - LstExecMacro(k - 1).Linee ' UBound(s) parte da 0, quindi devo aggiungere 1
              Else
                offSet = 1
              End If
              updateoffset IdOggetto, riga, offSet
              'lastriga = riga
              'lastoffset = totaloffset
              If IdOggetto <> LstExecMacro(k).IdOggetto Then
                updateObjectStatus IdOggetto
              Else
                ' Nuova Istruzione/Stesso Programma
                UpdateRigheIstr LstExecMacro, k - 1
              End If
            Else
              'qualche errore
            End If
          End If
        End If
        ''If Len(outputStatement) Then
        ''  UpdateRigheIstr outputStatement, LstExecMacro, k - 1
        ''End If
        ''''''''''''''''''''''''''''''''''''''
        ' Nuova ISTRUZIONE:
        ''''''''''''''''''''''''''''''''''''''
        IdOggetto = LstExecMacro(k).IdOggetto
        riga = LstExecMacro(k).riga
        'SQ
        'inputStatement = TrattaIstrFromSource(M_Area, idOggetto, riga, "Get")
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Input - Lettura file: EXEC completa da sostituire
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        inputStatement = getOriginalInstruction(M_Area, IdOggetto, riga, LstExecMacro(k).Linee)
        inputStatement = FormattaStrInput(M_Area, inputStatement)
        outputStatement = ""  'init (importante... fa da flag)
      'Else
        'ALTRA MACRO SULLA STESSA ISTRUZIONE
        'SQ cos� perdo l'originale!!!!!!!!!!!!!!!!!!!!!!
        'inputStatement = outputStatement
      End If
    End If
   
    '''''''''''''''''''''''''''''''''''''
    ' Esecuzione Macro
    '''''''''''''''''''''''''''''''''''''
    m_fun.WriteLogMacro Now & "- Start Execute for object: " & LstExecMacro(k).IdOggetto & "/" & LstExecMacro(k).riga & vbCrLf
    If Len(outputStatement) Then
      'input macro: outputStatement (ne ha gi� girato almeno una)
      outputStatement = m_fun.ExecuteMacro(CLng(Val(LwIstr.SelectedItem.tag)), LstExecMacro(k).NameMacro, outputStatement)
    Else
      'input macro: inputStatement (originale)
      outputStatement = m_fun.ExecuteMacro(CLng(Val(LwIstr.SelectedItem.tag)), LstExecMacro(k).NameMacro, inputStatement)
    End If
    'updateInstructionStatus LstExecMacro(k).idOggetto, LstExecMacro(k).numIstr
  Next k
  
  m_fun.closeScriptControl
  
  ''''''''''''''''''''''''''''''''''''''
  ' Ultima istruzione rimasta sola!
  ''''''''''''''''''''''''''''''''''''''
  If IdOggetto Then
    If outputStatement <> "NOMACRO" Then
      outputStatement = FormattaStrOutput(M_Area, outputStatement, inputStatement) 'SQ: setta la countOutLines
      '''''''''''''''''''''''''''''''''''''''''
      ' Update File
      '''''''''''''''''''''''''''''''''''''''''
      If M_Area = "JCL" Or M_Area = "JOB" Then
        wStrStep(riga) = outputStatement
        If TrattaAllStepJcl(IdOggetto, "Put") Then
          ''''''''''''''''''''''''''''''''''''
          ' Update status istruzione/oggetto
          ''''''''''''''''''''''''''''''''''''
          updateJCLStatus IdOggetto
        End If
      Else
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Scrittura nuovo statement
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'TrattaIstrFromSource M_Area, idOggetto, riga, "Put", outputStatement
        If writeUpdatedInstruction(IdOggetto, riga, outputStatement, LstExecMacro(k - 1).Linee) Then
          ''''''''''''''''''''''''''''''''''''
          ' Update status istruzione/oggetto
          ''''''''''''''''''''''''''''''''''''
          updateInstructionStatus IdOggetto, riga
          updateObjectStatus IdOggetto
        Else
          'qualche errore
        End If
      End If
    End If
  End If
  Screen.MousePointer = vbNormal
  m_fun.WriteLogMacro Now & "- End Execute Environment Allign" & vbCrLf

End Sub

Public Sub UpdateRigheIstr(LstExecMacro() As ExecMacro, Ind As Integer)
  Dim i As Long
  'Dim ContaRighe() As String
  'Dim RigheAggiunte As Integer
  'ContaRighe = Split(output, vbCrLf)
  'RigheAggiunte = UBound(ContaRighe)
  ' Aggiorno le righe sulla tabella PsExec
  'Menu.TsConnection.Execute "Update PsExec set riga = riga + " & countOutLines & " where idoggetto = " & LstExecMacro(ind).IdOggetto & " AND riga >= " & LstExecMacro(ind).riga
  ' Aggiorno le righe sulla tabella TS_AllignIstr
'  Menu.TsConnection.Execute "Update TL_Allignistr set riga = riga + " & countOutLines - LstExecMacro(Ind).Linee & " where idoggetto = " & LstExecMacro(Ind).IdOggetto & " AND riga >= " & LstExecMacro(Ind).riga
  Menu.TsConnection.Execute "Update TL_Allignistr set riga = riga + " & offSet & " where " & _
                            "idoggetto = " & LstExecMacro(Ind).IdOggetto & " AND riga >= " & LstExecMacro(Ind).riga
  For i = Ind + 1 To UBound(LstExecMacro)
    If LstExecMacro(Ind).IdOggetto = LstExecMacro(i).IdOggetto Then
      ' Aggiorno le righe sulla struttura di working
      'LstExecMacro(i).riga = LstExecMacro(i).riga + countOutLines - LstExecMacro(Ind).Linee
      LstExecMacro(i).riga = LstExecMacro(i).riga + offSet
    Else
      Exit For
    End If
  Next i
End Sub

Private Sub MtpNoActiveIstr()
  Dim tb As Recordset
  Dim wNumIstr As Long
  Dim SwAttiva As Boolean
  
  wNumIstr = Val(LwIstr.SelectedItem.SubItems(6))
  Set tb = m_fun.Open_Recordset("select * from tl_allignistr where numistr = " & wNumIstr & " ")
  If tb.RecordCount Then
    SwAttiva = Not tb!attiva
    tb!attiva = SwAttiva
    tb.Update
  End If
  
  tb.Close
 
  If SwAttiva Then
    LwIstr.SelectedItem.SubItems(7) = "Yes"
  Else
    LwIstr.SelectedItem.SubItems(7) = "No"
  End If
End Sub

Private Sub TVSelObj_NodeCheck(ByVal Node As MSComctlLib.Node)
  Dim K1 As Integer
  Dim K2 As Integer
  Dim wKey As String
  Dim wCheck As Variant
  Dim tb As Recordset
  
  wKey = Node.key
  wCheck = Node.Checked
  NumObjInMod = 0
  
  Select Case Node.key
    Case "SEL"
      If Node.Checked Then
        For K1 = 1 To Menu.TsObjList.ListItems.Count
          If Menu.TsObjList.ListItems(K1).Selected Then
            NumObjInMod = NumObjInMod + 1
          End If
        Next K1
      End If
    Case Else
      If Node.Checked Then
        For K1 = Node.index + 1 To Node.index + Node.Children
          TVSelObj.Nodes(K1).Checked = True
        Next K1
      End If
  End Select
      
  If TVSelObj.Nodes(1).Checked = False Then
    For K1 = 2 To TVSelObj.Nodes.Count
      If TVSelObj.Nodes(K1).Checked = True Then
        wKey = TVSelObj.Nodes(K1).key
        If wKey = "MPS" Then
          wKey = "BMS"
        End If
        Set tb = m_fun.Open_Recordset("Select * from bs_Oggetti where tipo = '" & wKey & "'")
        If tb.RecordCount Then
          tb.MoveLast
          NumObjInMod = NumObjInMod + tb.RecordCount
        End If
        tb.Close
      End If
    Next K1
  End If
  
  Frame4.Caption = "Activity log for " & NumObjInMod & " Objects"
End Sub

Private Sub TxtNomeMacro_Change()
  If Trim(TxtNomeMacro) = "" Then
    Toolbar2.Buttons("Macro").Enabled = False
    Toolbar2.Buttons("Run").Enabled = False
    Toolbar2.Buttons("AppMod").Enabled = False
  Else
    Toolbar2.Buttons("Macro").Enabled = True
    Toolbar2.Buttons("Run").Enabled = True
    Toolbar2.Buttons("AppMod").Enabled = True
  End If
End Sub

Private Function TrattaIstrFromSource(wArea As String, wIdObj As Long, wRiga As Long, wOper As String, Optional wBufIns As String) As String
  Dim wNumIstr As Integer
  Dim tb As Recordset
  Dim NomeInput As String, NomeOutPut As String
  Dim nFileIn As Variant, nFileOu As Variant
  Dim wRec As String, WBuf As String, wBuffer As String
  Dim wStr As String
  Dim k As Integer, Y As Integer
  Dim wDir As String
  
  If M_Area = "SQL" Then
    'SQ: 1-2-06 - ex PsSys_Istr
    Set tb = m_fun.Open_Recordset("select * from PsExec where idoggetto = " & wIdObj & " and area = 'SQL' order by riga")
  Else
    Set tb = m_fun.Open_Recordset("select * from PsExec where idoggetto = " & wIdObj & " and area = 'CICS' order by riga")
  End If
  '?????????????????????????????????????????????????
  '?????????????????????????????????????????????????
  '?????????????????????????????????????????????????
  '?????????????????????????????????????????????????
  If tb.RecordCount Then
    k = 0
    While Not tb.EOF
      k = k + 1
      If wRiga = tb!riga Then
        wNumIstr = k
        wBuffer = tb!istruzione
        tb.MoveLast
      End If
      tb.MoveNext
    Wend
  End If
   
  Set tb = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & wIdObj)
  NomeInput = tb!directory_input & "\" & tb!Nome
  If Trim(tb!estensione) <> "" Then
    NomeInput = NomeInput & "." & tb!estensione
  End If
  NomeInput = Replace(NomeInput, "$", Menu.TsPathDef)
   
  nFileIn = FreeFile
  Open NomeInput For Input As nFileIn
   
  '''''''''''''''''''''''''''
  ' Apertura file OUTPUT
  '''''''''''''''''''''''''''
  If wOper = "Put" Then
    On Error Resume Next
    wDir = tb!directory_input & "\out"
    wDir = Replace(wDir, "$", Menu.TsPathDef)
    If Dir(wDir, vbDirectory) = "" Then
      MkDir wDir
    End If
    NomeOutPut = tb!directory_input & "\out\" & tb!Nome
    If Trim(tb!estensione) <> "" Then
      NomeOutPut = NomeOutPut & "." & tb!estensione
    End If
    NomeOutPut = Replace(NomeOutPut, "$", Menu.TsPathDef)
    nFileOu = FreeFile
    Open NomeOutPut For Output As nFileOu
  End If
  tb.Close
   
  While Not EOF(nFileIn)
    Line Input #nFileIn, wRec
    If Mid$(wRec, 7, 1) <> "*" Then
      If InStr(wRec, " EXEC ") > 0 Then
        If wArea = "SQL" Then
          If InStr(wRec & " ", " SQL ") > 0 Then
            k = k + 1
            If k = wNumIstr Then
              WBuf = LeggiBufferCbl(wRec, nFileIn, "END-EXEC")
              If wOper = "Put" Then Print #nFileOu, wBufIns
            Else
              If wOper = "Put" Then Print #nFileOu, wRec
            End If
          Else
            If wOper = "Put" Then Print #nFileOu, wRec
          End If
        Else
          If InStr(wRec & " ", " CICS ") > 0 Then
            Y = Y + 1
            If Y = wNumIstr Then
              WBuf = LeggiBufferCbl(wRec, nFileIn, "END-EXEC")
              If wOper = "Put" Then Print #nFileOu, wBufIns
            Else
              If wOper = "Put" Then Print #nFileOu, wRec
            End If
          Else
            If wOper = "Put" Then Print #nFileOu, wRec
          End If
        End If
           
        If wOper = "Get" Then
          If Trim(WBuf) <> "" Then
            wStr = Mid$(WBuf, 8)
            wStr = Replace(wStr, vbCrLf, "")
            wStr = CompattaBlank(wStr)
            ''''''''''''SQ If wStr = wBuffer Then
            'SQ 7-02-05
            If True Then
              TrattaIstrFromSource = WBuf
            Else
              TrattaIstrFromSource = "Instruction not found"
            End If
            Close nFileIn
            Exit Function
          End If
        End If
      Else
        If wOper = "Put" Then Print #nFileOu, wRec
      End If
    Else
      If wOper = "Put" Then Print #nFileOu, wRec
    End If
    DoEvents
  Wend
  Close nFileIn
   
  TrattaIstrFromSource = "Instruction not found"
  If wOper = "Put" Then
    TrattaIstrFromSource = "Function PUT: OK"
    Close nFileOu
    CopiaOut (wIdObj)
  End If
End Function

Private Sub updateoffset(IdOggetto As Long, riga As Long, offSet As Long)
Dim rs As Recordset
  Set rs = m_fun.Open_Recordset("Select * from Ps_ObjRowOffset where idoggetto = " & IdOggetto & " AND riga >= " & riga)
  While Not rs.EOF
    Menu.TsConnection.Execute "Update Ps_ObjRowOffset set OffsetMacro = " & offSet & " where idoggetto = " & IdOggetto & " AND riga = " & rs!riga
    rs.MoveNext
  Wend
  rs.Close
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ho modificato un file (una sola istruzione) con le macro (1 o N)
' Resetto relazioni
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub updateInstructionStatus(IdOggetto As Long, riga As Long)
  Dim rs As Recordset
  'MsgBox "Linee Nuove: " & countOutLines
  'Menu.TsConnection.Execute "Delete * from PsExec where idoggetto = " & IdOggetto & " AND riga=" & riga
  Set rs = m_fun.Open_Recordset("select riga, istruzione from psexec where riga >= " & riga & " and idoggetto = " & IdOggetto & " ORDER BY riga DESC")
  Do Until rs.EOF
'''MG    Menu.TsConnection.Execute "Update PsExec set riga = " & rs!riga + offSet & " where idoggetto = " & IdOggetto & " AND riga=" & rs!riga
      Menu.TsConnection.Execute "Update PsExec set riga = " & rs!riga + offSet & " where idoggetto = " & IdOggetto & " AND riga=" & rs!riga & " AND Istruzione=""" & rs!istruzione & """"
    rs.MoveNext
  Loop
  rs.Close
  'Attenzione: o tutte o nessuna (sullo stesso oggetto)
  Menu.TsConnection.Execute "Update TL_AllignIstr set applicata = true where idoggetto = " & IdOggetto & " AND riga=" & riga

End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ho modificato un file con le macro
' Resetto parsing/segnalazioni/...
' SISTEMARE - REFRESHARE LISTA PRINCIPALE!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub updateObjectStatus(IdOggetto As Long)
  Dim rs As Recordset
  '''''''''''''''''''''''''''''''''''
  ' RESET SEGNALAZIONI!!!!!!!!!!! e tutto il resto? Relazioni,ecc...????
  ' Per lui � un oggetto nuovo!!!!
  '''''''''''''''''''''''''''''''''''
  GbErrLevel = ""
  Menu.TsConnection.Execute "Delete * from bs_segnalazioni where idoggetto = " & IdOggetto
  
  Set rs = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & IdOggetto)
  rs!ParsingLevel = 0
  rs!DtParsing = Null
  rs!errlevel = Null
  rs.Update
  rs.Close
End Sub
''''''''''''''''''''''''''''''''''''''''''''''
' Proviamo a non resettare parsing/segnalazioni...
''''''''''''''''''''''''''''''''''''''''''''''
Private Sub updateJCLStatus(IdOggetto As Long)
  'Attenzione: o tutte o nessuna (sullo stesso oggetto)
  Menu.TsConnection.Execute "Update TL_AllignIstr set applicata=true where idoggetto = " & IdOggetto & " AND Attiva"
End Sub

Private Function GetOriginalStep(IdOggetto As Long, nStep As Long) As String
  Dim tb As Recordset
  Dim wRigaStart As Integer
  Dim NomeInput As String, nFile As String, WBuf As String
  Dim wStep As Integer
  
  On Error Resume Next '????ALE
  
  Set tb = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & IdOggetto)
  
  NomeInput = tb!directory_input & "\" & tb!Nome
  If Trim(tb!estensione) <> "" Then
     NomeInput = NomeInput & "." & tb!estensione
  End If
  NomeInput = Replace(NomeInput, "$", Menu.TsPathDef)
  nFile = FreeFile
  
  Open NomeInput For Input As nFile
  wRigaStart = 0
  WBuf = ""
  Do While Not EOF(nFile)
    wRigaStart = wRigaStart + CountRiga(WBuf, "none")
    WBuf = LeggiBufferVSE(nFile, False)
    wStep = wStep + 1
    
    If nStep = wStep Then
       GetOriginalStep = WBuf
       Exit Do
    End If
  Loop
  Close nFile
End Function


