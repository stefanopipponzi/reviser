VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form MatsdF_EBCDIC2ASCII 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "EBCDIC to ASCII Conversion"
   ClientHeight    =   6555
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   8220
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6555
   ScaleWidth      =   8220
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdFilterExe 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   555
      Left            =   3780
      Picture         =   "MatsdF_EBCDIC2ASCII.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   22
      Tag             =   "fixed"
      ToolTipText     =   "Filter Execution"
      Top             =   60
      Width           =   550
   End
   Begin VB.CommandButton cmdFilterConf 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   3240
      Picture         =   "MatsdF_EBCDIC2ASCII.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   21
      Tag             =   "fixed"
      ToolTipText     =   "Filter Configuration"
      Top             =   60
      Width           =   550
   End
   Begin VB.CommandButton cmdCompareRed 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   2700
      Picture         =   "MatsdF_EBCDIC2ASCII.frx":0294
      Style           =   1  'Graphical
      TabIndex        =   19
      Tag             =   "fixed"
      ToolTipText     =   "Delete obsolete redefines"
      Top             =   60
      Width           =   550
   End
   Begin MSComDlg.CommonDialog ComD 
      Left            =   720
      Top             =   6720
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton CmdAddAreaAll 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   2160
      Picture         =   "MatsdF_EBCDIC2ASCII.frx":06D6
      Style           =   1  'Graphical
      TabIndex        =   18
      Tag             =   "fixed"
      ToolTipText     =   "Add Area from list "
      Top             =   60
      Width           =   550
   End
   Begin VB.CommandButton cmdOpenArea 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   540
      Picture         =   "MatsdF_EBCDIC2ASCII.frx":09E0
      Style           =   1  'Graphical
      TabIndex        =   17
      Tag             =   "fixed"
      ToolTipText     =   "Open"
      Top             =   60
      Width           =   550
   End
   Begin VB.CommandButton CmdConvert 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   1080
      Picture         =   "MatsdF_EBCDIC2ASCII.frx":12AA
      Style           =   1  'Graphical
      TabIndex        =   16
      Tag             =   "fixed"
      ToolTipText     =   "Create Convertion Elements"
      Top             =   60
      Width           =   550
   End
   Begin VB.CommandButton CmdErase 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   1620
      Picture         =   "MatsdF_EBCDIC2ASCII.frx":1F74
      Style           =   1  'Graphical
      TabIndex        =   15
      Tag             =   "fixed"
      ToolTipText     =   "Erase"
      Top             =   60
      Width           =   550
   End
   Begin VB.CommandButton CmdAddArea 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   0
      Picture         =   "MatsdF_EBCDIC2ASCII.frx":227E
      Style           =   1  'Graphical
      TabIndex        =   14
      Tag             =   "fixed"
      ToolTipText     =   "Add"
      Top             =   60
      Width           =   550
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selection"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   975
      Left            =   30
      TabIndex        =   11
      Top             =   630
      Width           =   3885
      Begin VB.OptionButton OptAll 
         Caption         =   "All Structures"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   270
         Width           =   2655
      End
      Begin VB.OptionButton OptSel 
         Caption         =   "Only Selected Structure"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Value           =   -1  'True
         Width           =   2655
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Options"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1005
      Left            =   3990
      TabIndex        =   8
      Top             =   630
      Width           =   1695
      Begin VB.OptionButton optEbcdic2Ascii 
         Caption         =   "EBCDIC to ASCII"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   300
         Value           =   -1  'True
         Width           =   1515
      End
      Begin VB.OptionButton optAscii2Ebcdic 
         Caption         =   "ASCII to EBCDIC"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   1515
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1005
      Left            =   5700
      TabIndex        =   4
      Top             =   630
      Width           =   2415
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   195
         Left            =   90
         TabIndex        =   5
         Top             =   240
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBarK 
         Height          =   195
         Left            =   90
         TabIndex        =   6
         Top             =   450
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBar01 
         Height          =   195
         Left            =   90
         TabIndex        =   7
         Top             =   660
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Area Selected"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4935
      Left            =   30
      TabIndex        =   2
      Top             =   1620
      Width           =   3885
      Begin VB.TextBox txtFilter 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H00C00000&
         Height          =   285
         Left            =   120
         TabIndex        =   23
         Top             =   270
         Visible         =   0   'False
         Width           =   1575
      End
      Begin MSComctlLib.ListView lswStructSel 
         Height          =   4515
         Left            =   120
         TabIndex        =   3
         Top             =   270
         Width           =   3645
         _ExtentX        =   6429
         _ExtentY        =   7964
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "File"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Associated Area"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid Filter_List 
         Height          =   915
         Left            =   120
         TabIndex        =   24
         Top             =   270
         Visible         =   0   'False
         Width           =   3645
         _ExtentX        =   6429
         _ExtentY        =   1614
         _Version        =   393216
         FixedCols       =   0
         BackColor       =   -2147483624
         ForeColor       =   12582912
         BackColorSel    =   16777088
         ForeColorSel    =   16711680
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Log"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4935
      Left            =   3990
      TabIndex        =   0
      Top             =   1620
      Width           =   4125
      Begin RichTextLib.RichTextBox RtbScript 
         Height          =   375
         Left            =   1920
         TabIndex        =   20
         Top             =   4200
         Visible         =   0   'False
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   661
         _Version        =   393217
         Enabled         =   -1  'True
         TextRTF         =   $"MatsdF_EBCDIC2ASCII.frx":30C0
      End
      Begin MSComctlLib.ListView LswLog 
         Height          =   4515
         Left            =   150
         TabIndex        =   1
         Top             =   270
         Width           =   3885
         _ExtentX        =   6853
         _ExtentY        =   7964
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Log"
            Object.Width           =   26458
         EndProperty
      End
   End
End
Attribute VB_Name = "MatsdF_EBCDIC2ASCII"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAddArea_Click()
  Load MatsdF_AddArea
  MatsdF_AddArea.Show vbModal
End Sub

Private Sub CmdAddAreaAll_Click()
  AddStructure_from_list
End Sub

' Mauro 21/03/2008 : Compare per eliminare le redefines inutili
Private Sub cmdCompareRed_Click()
  Dim i As Long

  LswLog.ListItems.Clear
  PBar.Max = 10
  PBar.Value = 0
  PBarK.Max = 10
  PBarK.Value = 0
  DoEvents

  If lswStructSel.ListItems.Count > 0 Then
    LswLog.ListItems.Add , , Time & " - Starting Analysis"
    LswLog.Refresh

    If OptAll.Value Then
      PBar01.Max = lswStructSel.ListItems.Count
      For i = 1 To lswStructSel.ListItems.Count
        LswLog.ListItems.Add , , Time & " - Source Comparison: " & lswStructSel.ListItems(i)
        LswLog.Refresh
        LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible

        compareRedefines lswStructSel.ListItems(i).tag
        PBar01.Value = PBar01.Value + 1
      Next i
    Else 'converte solo l'elemento selezionato
      PBar01.Max = lswStructSel.ListItems.Count
      For i = 1 To lswStructSel.ListItems.Count
        If lswStructSel.ListItems(i).Selected Then
          LswLog.ListItems.Add , , Time & " - Source Comparison: " & lswStructSel.ListItems(i)
          LswLog.Refresh
          LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible

          compareRedefines lswStructSel.ListItems(i).tag
        End If
        PBar01.Value = PBar01.Value + 1
      Next i
    End If

    PBar01.Value = PBar01.Max
    LswLog.ListItems.Add , , Time & " - Source Comparison Terminated"
    LswLog.Refresh
    LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible

    PBar.Value = 0
    PBarK.Value = 0
    PBar01.Value = 0
  End If
End Sub

Private Sub CmdConvert_Click()
  Dim i As Long
  ' Giugy 05/05/08 Creazione automatica script
  Dim rtbKey As RichTextBox

  LswLog.ListItems.Clear
  PBar.Max = 10
  PBar.Value = 0
  PBarK.Max = 10
  PBarK.Value = 0
  DoEvents

  If lswStructSel.ListItems.Count > 0 Then
    LswLog.ListItems.Add , , Time & " - Starting Analysis"
    LswLog.Refresh
      
    If OptAll.Value Then
      PBar01.Max = lswStructSel.ListItems.Count
         
      For i = 1 To lswStructSel.ListItems.Count
        LswLog.ListItems.Add , , Time & " - Source Conversion: " & lswStructSel.ListItems(i)
        LswLog.Refresh
        LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible

        If optEbcdic2Ascii.Value Then
          Converti_Structure lswStructSel.ListItems(i).tag, lswStructSel.ListItems(i)
        Else
          'Seconda opzione
        End If
        PBar01.Value = PBar01.Value + 1
      Next i
    Else 'converte solo l'elemento selezionato
      PBar01.Max = lswStructSel.ListItems.Count
      For i = 1 To lswStructSel.ListItems.Count
        If lswStructSel.ListItems(i).Selected Then
          LswLog.ListItems.Add , , Time & " - Source Conversion: " & lswStructSel.ListItems(i)
          LswLog.Refresh
          LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
            
          If optEbcdic2Ascii.Value Then
            Converti_Structure lswStructSel.ListItems(i).tag, lswStructSel.ListItems(i)
          Else
            'Seconda opzione
          End If
        End If
        PBar01.Value = PBar01.Value + 1
      Next i
    End If
      
    PBar01.Value = PBar01.Max
    LswLog.ListItems.Add , , Time & " - Source Conversion Terminated"
    LswLog.Refresh
    LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
      
    PBar.Value = 0
    PBarK.Value = 0
    PBar01.Value = 0
  End If
  
  ' Giugy 05/05/08 Creazione automatica script
  On Error GoTo ErrorHandler
  
  Set rtbKey = MatsdF_EBCDIC2ASCII.RtbScript
  If lswStructSel.ListItems.Count Then
    For i = 1 To lswStructSel.ListItems.Count
      If lswStructSel.ListItems(i).Selected Then
        rtbKey.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\FILE_CONVERT\e2a-filename"
        changeTag rtbKey, "<INPFILE>", lswStructSel.ListItems(i)
        rtbKey.SaveFile m_fun.FnPathPrj & "\Output-Prj\Data\EBCDICtoASCII\bin\e2a-" & lswStructSel.ListItems(i), 1
      End If
    Next i
    MsgBox "Script created correctly!", vbInformation, "Script"
  End If
  Exit Sub
ErrorHandler:
  If Err.Number = 75 Then
    MsgBox Err.Number & " - " & Err.Description, vbExclamation, "ERROR"
    Exit Sub
  End If
End Sub

Private Sub cmdErase_Click()
  Dim i As Integer
  Dim Area As Long, Oggetto As Long
  
  If lswStructSel.ListItems.Count Then
    If MsgBox("Do you want delete this element?", vbInformation + vbYesNo, "Attention!") = vbYes Then
      For i = 1 To lswStructSel.ListItems.Count
        If lswStructSel.ListItems(i).Selected Then
          Oggetto = Left(lswStructSel.ListItems(i).tag, InStr(lswStructSel.ListItems(i).tag, "\") - 1)
          Area = Mid(lswStructSel.ListItems(i).tag, InStr(lswStructSel.ListItems(i).tag, "\") + 1)
          m_fun.FnConnection.Execute "Delete * From mgconvdata_area where Idarea = " & Area & " AND IdOggetto = " & Oggetto
          m_fun.FnConnection.Execute "Delete * From mgconvdata_cmp where Idarea = " & Area & " AND IdOggetto = " & Oggetto
        End If
      Next i
      Carica_Lista_Structure
    End If
  End If
End Sub
' Mauro 04/02/2011
Private Sub cmdFilterConf_Click()
  'Seleziona Filtro
  isFilterConf = Not isFilterConf
  If isFilterConf Then
    isFilterExe = False
  End If
  Seleziona_Filtro_Selezione lswStructSel, Filter_List, isFilterConf
End Sub
' Mauro 04/02/2011
Private Sub cmdFilterExe_Click()
  'esegue la Query di filtro
  isFilterExe = Not isFilterExe
  Execute_Filter Filter_List, lswStructSel, isFilterExe
  txtFilter.Visible = False
  Filter_List.Visible = False
End Sub
' Mauro 04/02/2011
Private Sub Filter_List_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  Dim XN As Long, yN As Long
  Dim i As Long
  
  'assegna alla globale la colonna selezionata
  Filter_List_Column = Filter_List.ColSel
  'txtFilter = ""
  txtFilter.text = ""
  txtFilter.Visible = True
  txtFilter.SetFocus
  
  'prende le coordinate
  For i = 0 To Filter_List.Cols - 1
    If i < Filter_List_Column Then
      Filter_List.Col = i
      Filter_List.Row = 1
      XN = XN + Filter_List.CellWidth
    Else
      Exit For
    End If
  Next i
  Filter_List.Row = 0
  Filter_List.Col = 0
  yN = Filter_List.CellHeight
  
  'posiziona la text box
  Filter_List.Row = 1
  Filter_List.Col = Filter_List_Column
  
  txtFilter.Top = Filter_List.Top + Filter_List.CellTop
  txtFilter.Left = Filter_List.Left + Filter_List.CellLeft
  txtFilter.Width = Filter_List.CellWidth
  txtFilter.Height = Filter_List.CellHeight
  
  Filter_List.Refresh
End Sub

Private Sub cmdOpenArea_Click()
  lswStructSel_DblClick
End Sub

Private Sub Form_Load()
  isFilterConf = False
  isFilterExe = False
  
  lswStructSel.ColumnHeaders(1).Width = lswStructSel.Width / 2
  lswStructSel.ColumnHeaders(2).Width = lswStructSel.Width / 2
   
  Carica_Lista_Structure
      
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
   
  If m_fun.FnProcessRunning = True Then
    wScelta = m_fun.Show_MsgBoxError("FB01I")
   
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  lswStructSel.ColumnHeaders(1).Width = lswStructSel.Width / 2
  lswStructSel.ColumnHeaders(2).Width = lswStructSel.Width / 2
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
End Sub

Private Sub lswStructSel_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
 
  Order = lswStructSel.SortOrder
  
  key = ColumnHeader.Index
  
  'assegna alla variabile globale l'index colonna selezionato
  ListObjCol = key
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswStructSel.SortOrder = Order
  lswStructSel.SortKey = key - 1
  lswStructSel.Sorted = True
End Sub

Private Sub lswStructSel_DblClick()
  If lswStructSel.ListItems.Count Then
    indexArea = lswStructSel.SelectedItem.tag ' Campo composto da IdOggetto\IdArea
    Load MatsdF_StructDetails
    MatsdF_StructDetails.Show vbModal
  End If
End Sub

Private Sub txtFilter_Change()
  Filter_List.Col = Filter_List_Column
  Filter_List.Row = 1
  
  Filter_List.text = txtFilter.text
End Sub

Private Sub txtFilter_KeyPress(KeyAscii As Integer)
  Filter_List.text = ""
  If KeyAscii = 13 Then
    If Filter_List_Column = 0 Then
      txtFilter.text = Format(txtFilter.text, "000000")
    End If
    
    txtFilter.Visible = False
  End If
End Sub

