VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form MatsdF_IebGener 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Import / Export Source Library"
   ClientHeight    =   7545
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   11355
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7545
   ScaleWidth      =   11355
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdSplitMantis 
      Caption         =   "Mantis"
      Height          =   465
      Left            =   3720
      TabIndex        =   29
      Top             =   60
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.CommandButton CmdSplitModule 
      Caption         =   "Split Modules"
      Height          =   465
      Left            =   3000
      TabIndex        =   28
      Top             =   60
      Width           =   465
   End
   Begin VB.CommandButton CmdCrlf 
      Height          =   465
      Left            =   2400
      Picture         =   "MatsdF_IebGener.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   27
      Tag             =   "fixed"
      ToolTipText     =   "Replace Crlf"
      Top             =   60
      Width           =   465
   End
   Begin VB.CommandButton CmdUnblock 
      Height          =   465
      Left            =   1800
      Picture         =   "MatsdF_IebGener.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   25
      Tag             =   "fixed"
      ToolTipText     =   "Unblock"
      Top             =   60
      Width           =   465
   End
   Begin MSComDlg.CommonDialog CD1 
      Left            =   6600
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton CmdExport 
      Height          =   465
      Left            =   600
      Picture         =   "MatsdF_IebGener.frx":058C
      Style           =   1  'Graphical
      TabIndex        =   22
      Tag             =   "fixed"
      ToolTipText     =   "Export files of Project"
      Top             =   60
      Width           =   465
   End
   Begin VB.CommandButton CmdE2A 
      Height          =   465
      Left            =   1200
      Picture         =   "MatsdF_IebGener.frx":0E56
      Style           =   1  'Graphical
      TabIndex        =   21
      Tag             =   "fixed"
      ToolTipText     =   "E2A Conversion "
      Top             =   60
      Width           =   465
   End
   Begin VB.CommandButton CmdSplit 
      Height          =   465
      Left            =   0
      Picture         =   "MatsdF_IebGener.frx":13E0
      Style           =   1  'Graphical
      TabIndex        =   20
      Tag             =   "fixed"
      ToolTipText     =   "Split"
      Top             =   60
      Width           =   465
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7005
      Left            =   0
      TabIndex        =   0
      Top             =   540
      Width           =   11385
      _ExtentX        =   20082
      _ExtentY        =   12356
      _Version        =   393216
      TabOrientation  =   1
      Style           =   1
      Tab             =   1
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "(0)"
      TabPicture(0)   =   "MatsdF_IebGener.frx":3B82
      Tab(0).ControlEnabled=   0   'False
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "Import"
      TabPicture(1)   =   "MatsdF_IebGener.frx":3B9E
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Label1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "RtbCrlf"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "TxtFileLib"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Command1"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Frame1"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "FrmOut"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "ProgressBar1"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "FrmLog"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).ControlCount=   8
      TabCaption(2)   =   "Export"
      TabPicture(2)   =   "MatsdF_IebGener.frx":3BBA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      Begin VB.Frame FrmLog 
         Caption         =   "Log"
         ForeColor       =   &H00000080&
         Height          =   975
         Left            =   150
         TabIndex        =   18
         Top             =   5670
         Width           =   11085
         Begin RichTextLib.RichTextBox RtbLog 
            Height          =   645
            Left            =   120
            TabIndex        =   19
            Top             =   240
            Width           =   10815
            _ExtentX        =   19076
            _ExtentY        =   1138
            _Version        =   393217
            BackColor       =   -2147483624
            Enabled         =   -1  'True
            TextRTF         =   $"MatsdF_IebGener.frx":3BD6
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   255
         Left            =   150
         TabIndex        =   16
         Top             =   5340
         Width           =   11085
         _ExtentX        =   19553
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin VB.Frame FrmOut 
         Caption         =   "Output "
         ForeColor       =   &H00000080&
         Height          =   5235
         Left            =   6810
         TabIndex        =   9
         Top             =   60
         Width           =   4425
         Begin VB.Frame FrmObj 
            Caption         =   "Items type"
            ForeColor       =   &H00000080&
            Height          =   1725
            Left            =   150
            TabIndex        =   13
            Top             =   3420
            Width           =   4095
            Begin VB.ListBox LstObjTypeImport 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   1185
               Left            =   150
               Style           =   1  'Checkbox
               TabIndex        =   14
               Top             =   300
               Width           =   3825
            End
         End
         Begin VB.Frame FrmDir 
            Caption         =   "Directory"
            ForeColor       =   &H00000080&
            Height          =   3045
            Left            =   150
            TabIndex        =   10
            Top             =   270
            Width           =   4095
            Begin VB.TextBox TxtDirImport 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   375
               Left            =   120
               TabIndex        =   15
               Top             =   300
               Width           =   3855
            End
            Begin VB.DriveListBox Drive1 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   315
               Left            =   120
               TabIndex        =   12
               Top             =   2580
               Width           =   3855
            End
            Begin VB.DirListBox Dir1 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   1665
               Left            =   120
               TabIndex        =   11
               Top             =   780
               Width           =   3855
            End
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Library Preview"
         ForeColor       =   &H00000080&
         Height          =   4815
         Left            =   150
         TabIndex        =   4
         Top             =   480
         Width           =   6615
         Begin RichTextLib.RichTextBox RtbAppo 
            Height          =   1065
            Left            =   720
            TabIndex        =   26
            Top             =   1080
            Visible         =   0   'False
            Width           =   1815
            _ExtentX        =   3201
            _ExtentY        =   1879
            _Version        =   393217
            TextRTF         =   $"MatsdF_IebGener.frx":3C58
         End
         Begin VB.TextBox txtRecLen 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   285
            Left            =   1740
            TabIndex        =   23
            Top             =   4440
            Width           =   1665
         End
         Begin VB.CheckBox ChkCrLf 
            Alignment       =   1  'Right Justify
            Caption         =   "LF to CRLF conversion"
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   4440
            TabIndex        =   17
            Top             =   4410
            Width           =   2055
         End
         Begin VB.CheckBox ChkVarImport 
            Alignment       =   1  'Right Justify
            Caption         =   "Import library is Variable "
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   4470
            TabIndex        =   8
            Top             =   4080
            Width           =   2025
         End
         Begin VB.TextBox SkSeparImport 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   1740
            TabIndex        =   7
            Top             =   4020
            Width           =   2265
         End
         Begin RichTextLib.RichTextBox RtbImport 
            Height          =   3615
            Left            =   120
            TabIndex        =   5
            Top             =   300
            Width           =   6345
            _ExtentX        =   11192
            _ExtentY        =   6376
            _Version        =   393217
            BackColor       =   16777152
            Enabled         =   -1  'True
            ReadOnly        =   -1  'True
            ScrollBars      =   3
            RightMargin     =   10000
            TextRTF         =   $"MatsdF_IebGener.frx":3CDA
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label LblRecLen 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Record length for"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   420
            TabIndex        =   24
            Top             =   4440
            Width           =   1230
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "SPLIT Identifier Label"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   120
            TabIndex        =   6
            Top             =   4080
            Width           =   1530
         End
      End
      Begin VB.CommandButton Command1 
         Caption         =   "..."
         Height          =   315
         Left            =   6420
         TabIndex        =   3
         Top             =   120
         Width           =   315
      End
      Begin VB.TextBox TxtFileLib 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1530
         TabIndex        =   2
         Text            =   "*.*"
         Top             =   90
         Width           =   5235
      End
      Begin RichTextLib.RichTextBox RtbCrlf 
         Height          =   1065
         Left            =   3375
         TabIndex        =   30
         Top             =   2070
         Visible         =   0   'False
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   1879
         _Version        =   393217
         TextRTF         =   $"MatsdF_IebGener.frx":3D5A
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Import Library"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   165
         TabIndex        =   1
         Top             =   150
         Width           =   1140
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   5280
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_IebGener.frx":3DE0
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_IebGener.frx":3EF2
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_IebGener.frx":4004
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_IebGener.frx":4116
            Key             =   "Crea"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_IebGener.frx":456E
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MatsdF_IebGener"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public formParent As Form

Sub Resize()
  On Error Resume Next
  
  Me.Move Menu.TsLeft, Menu.TsTop, Menu.TsWidth, Menu.TsHeight
  
  SSTab1.TabVisible(0) = True
  SSTab1.Tab = 0
  
  SSTab1.Move 30, SSTab1.Top, Menu.TsWidth - 180, Menu.TsHeight - 350 - SSTab1.Top
  FrmOut.Move FrmOut.Left, FrmOut.Top, SSTab1.Width - FrmOut.Left - 90, FrmOut.Height
  FrmDir.Move FrmDir.Left, FrmDir.Top, FrmOut.Width - 240, FrmDir.Height
  FrmObj.Move FrmObj.Left, FrmObj.Top, FrmDir.Width, FrmObj.Height
  
  TxtDirImport.Width = FrmDir.Width - TxtDirImport.Left - 90
  Dir1.Width = TxtDirImport.Width
  Drive1.Width = TxtDirImport.Width
  LstObjTypeImport.Width = TxtDirImport.Width
  ProgressBar1.Width = SSTab1.Width - 240
  
  FrmLog.Move FrmLog.Left, FrmLog.Top, SSTab1.Width - FrmLog.Left - 90, SSTab1.Height - 450 - FrmLog.Top
  RtbLog.Move RtbLog.Left, RtbLog.Top, FrmLog.Width - 180, FrmLog.Height - 300
  
  SSTab1.TabVisible(0) = False
  SSTab1.TabEnabled(2) = False
  SSTab1.Tab = 1

End Sub

'GIUGY 20/06/08 Temporaneo.....
Private Sub CmdCrlf_Click()
  Dim DirTmp As String, NomeFileOutput As String, NomeFileInput As String, wRec As String
  Dim k As Integer, NumFileI As Integer
  Dim first As Boolean
  Dim NumFileTemp As Integer
  
  On Error GoTo ErrorHandler
  Screen.MousePointer = vbHourglass
  
  first = False
  
  AddLogEib "Start Replace Lf for " & TxtFileLib & " Library"

  Crea_Directory TxtDirImport
  DirTmp = TxtDirImport & "\ReplaceClrf"
  Crea_Directory DirTmp
     
  ProgressBar1.Max = FileLen(TxtFileLib)
  ProgressBar1.Value = 0
  
  k = Len(TxtFileLib) - InStrRev(TxtFileLib, "\")
  NomeFileInput = Right(TxtFileLib, k)
  k = InStrRev(NomeFileInput, ".")
  If k <> 0 Then
    NomeFileInput = Left(NomeFileInput, k - 1)
  End If
  
  NumFileI = FreeFile
  NomeFileOutput = DirTmp & "\" & NomeFileInput & ".rep"
  Open TxtFileLib For Binary As NumFileI
  While Not Loc(NumFileI) = FileLen(TxtFileLib)
    If first Then
      Line Input #NumFileI, wRec
      wRec = Replace(wRec, vbCrLf, vbLf)
      wRec = Replace(wRec, vbLf, vbCrLf)
      Print #NumFileTemp, wRec
    Else
      NumFileTemp = FreeFile
      Open NomeFileOutput For Output As NumFileTemp
      first = True
    End If
  Wend
  Close NumFileI
  Close NumFileTemp
  
  Screen.MousePointer = vbNormal
  
  AddLogEib "End Replace LineFeed for " & TxtFileLib & " Library"
  
  Exit Sub
ErrorHandler:
  If Err.Number = 52 Then
    MsgBox "Please specify the input file", vbOKOnly + vbExclamation, "Attention!"
    Close NumFileI
    Close NumFileTemp
    Screen.MousePointer = vbNormal
  End If
End Sub

Private Sub CmdE2A_Click()
  Dim fileName As String, txtFile As String
  
  txtFile = TxtFileLib.text
  Select Case UCase(SSTab1.TabCaption(SSTab1.Tab))
    Case "IMPORT"
      If Trim(txtFile) = "" Or InStr(txtFile, "*") > 0 Then
        MsgBox "Select a File to Convert", vbInformation, "i-4.Migration - Library Import"
        Exit Sub
      End If
      If Right(txtFile, 1) = "*" Or Right(txtFile, 1) = "\" Then
        E2A_Directory txtFile, RtbImport
      Else
        fileName = Dir(txtFile)
        Do Until fileName <> "." Or fileName <> ".."
          fileName = Dir(txtFile)
        Loop
        If Len(fileName) Then
          E2A txtFile, CD1.fileName, RtbImport
        Else
          E2A_Directory txtFile, RtbImport
        End If
      End If
    Case "EXPORT"
  End Select
  TxtFileLib = txtFile
End Sub

Private Sub CmdExport_Click()
  Dim rs As Recordset
  Dim exportDir As String, subDir As String, fileExt As String
  
  On Error GoTo ErrExp
  
  RtbLog.text = ""

  If MsgBox("All files of Project will be exported, confirm the operation? ", vbQuestion + vbYesNo, "Export File System") = vbYes Then
    Screen.MousePointer = vbHourglass
    exportDir = m_fun.FnPathPrj & "\Output-Prj\Export"
    If Dir(exportDir, vbDirectory) = "" Then m_fun.MkDir_API exportDir
    
    'SQ tmp - filtro objectType
    Set rs = m_fun.Open_Recordset("SELECT Tipo, Cics, Batch, Area_Appartenenza, Livello1, Livello2, Nome, Estensione, WithSQL " & _
                                  "From BS_Oggetti " & _
                                  "Where Tipo in ('CBL','CPY','JCL','MBR','PRC') " & _
                                  "ORDER BY Area_Appartenenza, Livello1")
    Do Until rs.EOF
      subDir = ""
      fileExt = ""
      DoEvents
      If rs!Tipo = "CBL" Then
        If rs!CICS Then
          subDir = "\CICS"
          If rs!WithSQL Then
            fileExt = "cl2"
          Else
            fileExt = "pco"
          End If
        Else
          'SQ batch == non-cics
          subDir = "\BATCH"
          If rs!WithSQL Then
            fileExt = "db2"
          Else
            fileExt = "cob"
          End If
        End If
      End If
      If fso.FileExists(m_fun.FnPathDef & Mid(rs!Livello2, 2) & "\" & rs!Nome & IIf(rs!estensione <> "", "." & rs!estensione, "")) Then
         fso.CopyFile m_fun.FnPathDef & Mid(rs!Livello2, 2) & "\" & rs!Nome & IIf(rs!estensione <> "", "." & rs!estensione, ""), _
                      exportDir & "\" & rs!Area_Appartenenza & "\" & rs!Livello1 & subDir & "\", True
      Else
        AddLogEib "File not found: " & m_fun.FnPathDef & Mid(rs!Livello2, 2) & "\" & rs!Nome & IIf(rs!estensione <> "", "." & rs!estensione, "")
      End If
      rs.MoveNext
    Loop
    rs.Close
    Screen.MousePointer = vbDefault
    MsgBox "All Files are exported", vbInformation, "i-4.Migration"
  End If
  Exit Sub
ErrExp:
  If Err.Number = 53 Then
    AddLogEib "File not found: " & m_fun.FnPathDef & Mid(rs!Livello2, 2) & "\" & rs!Nome & IIf(rs!estensione <> "", "." & rs!estensione, "")
    Resume Next
  End If
  If Err.Number = 76 Then
    m_fun.MkDir_API exportDir & "\" & rs!Area_Appartenenza & "\" & rs!Livello1 & subDir
    Resume
  End If
End Sub

Private Sub CmdSplit_Click()
  m_fun.FnProcessRunning = True
  SalvaEibGener
  m_fun.FnProcessRunning = False
End Sub

Private Sub CmdSplitMantis_Click()
  If Trim(CD1.fileName) = "" Then
    MsgBox "Please select a file to split", vbInformation
    Exit Sub
  End If
  If Trim(TxtDirImport.text) = "" Then
    MsgBox "Please select a directory for splitted file/s", vbInformation
    Exit Sub
  End If
  SplitMantis CD1.fileName, TxtDirImport.text
End Sub

Private Sub SplitMantis(FilenameI As String, DirImport As String)
  Dim Line As String, IsStoppingBuff As Boolean, strName As String, DirType As String, buffer As String, linetoex As String
  Dim FDin As Long, FDout As Long, FileNameO As String  'FILE
  Dim actualName As String, numLinea As Long
  
  On Error GoTo catch
  'input
  FDin = FreeFile
  Open FilenameI For Input As FDin
  'output:
  actualName = ""
  IsStoppingBuff = True
  While Not EOF(FDin)
    Line Input #FDin, Line
    numLinea = numLinea + 1
    'linea da cui si inizia a bufferizzare (buffer aggiornato con l'if successivo)
    'linea che contiene il tipo
    If findTarget_Module(Line, "NAME :") = "" And findTarget_Module(Line, "DESCRIPTION :") = "" Then
      linetoex = Right(Line, Len(Line) - InStr(Line, "NAME") + 1)
      strName = nextToken_OF(linetoex)
      strName = nextToken_OF(linetoex)
      strName = nextToken_OF(linetoex)
      Line Input #FDin, Line ' saltiamo riga  PROGR BOUND : No
      numLinea = numLinea + 1
      If Not findTarget_Module(Line, "PROGR BOUND") = "" Or strName = "" Then
        MsgBox ("Error in splitting, check your split routine")
      End If
      IsStoppingBuff = False
      If Not strName = actualName Then
        ' scrittura precedente
        If Not FileNameO = "" Then
          Print #FDout, buffer
          Close FDout
          buffer = ""
        End If
        ' preparazione nuovo file
        actualName = strName
        FileNameO = DirImport & "\" & strName
        FDout = FreeFile
        If Dir(DirImport & "\" & DirType, vbDirectory) = "" Then m_fun.MkDir_API DirImport & "\" & DirType
        Open FileNameO For Output As FDout
      End If
    ElseIf findTarget_Module(Line, "PAGE -") = "" Then '
      IsStoppingBuff = True
    ElseIf Not IsStoppingBuff Then
      If Not Trim(Mid(Line, 2)) = "" Then
        If Not buffer = "" Then
          buffer = buffer & vbCrLf & Mid(Line, 2)
        Else
          buffer = Mid(Line, 2)
        End If
      End If
    End If
  Wend
  If Not FileNameO = "" Then
    Print #FDout, buffer
    Close FDout
  End If
  Close FDin
  MsgBox "Split ended, processed lines: " & numLinea, vbInformation
  Exit Sub
catch:
  'tmp
  MsgBox Err.Description
End Sub

Private Sub CmdSplitModule_Click()
  If Trim(CD1.fileName) = "" Then
    MsgBox "Please select a file to split", vbInformation
    Exit Sub
  End If
  If Trim(TxtDirImport.text) = "" Then
    MsgBox "Please select a directory for splitted file/s", vbInformation
    Exit Sub
  End If
  SplitModules CD1.fileName, TxtDirImport.text
End Sub

Sub SplitModules(FilenameI As String, DirImport As String)
  Dim Line As String, IsStoppingBuff As Boolean, strName As String, DirType As String, buffer As String, linetoex As String
  Dim FDin As Long, FDout As Long, FileNameO As String  'FILE
  Dim n As Integer, lineAppo As String
  
  On Error GoTo catch
  'input
  FDin = FreeFile
  Open FilenameI For Input As FDin
  'output:
  
  While Not EOF(FDin)
    Line Input #FDin, Line
     
    'linea da cui si inizia a bufferizzare (buffer aggiornato con l'if successivo)
    'linea che contiene il tipo
    If findTarget_Module(Line, "LANGUAGE") = "" Then
      linetoex = Right(Line, Len(Line) - InStr(Line, "LANGUAGE") + 1)
      DirType = nextToken_OF(linetoex)
      DirType = nextToken_OF(linetoex)
      ' costruiamo percorso del file di output
      FileNameO = DirImport & "\" & DirType & "\" & strName
      FDout = FreeFile
      If Dir(DirImport & "\" & DirType, vbDirectory) = "" Then m_fun.MkDir_API DirImport & "\" & DirType
      Open FileNameO For Output As FDout
    End If
    ' linea identificatrice di pagina
    ' se 1 salviamo il blocco precedente,azzeriamo il buffer,estraiamo info sul nome esterno,
    ' se > 1 non scriviamo la riga sul buffer
    If findTarget_Module(Line, "LISTING OF MODULE") = "" Then
      If findTarget_Module(Line, "PAGE 1") = "" Then
        If Not FileNameO = "" Then
          Print #FDout, buffer
          Close FDout
        End If
        buffer = ""
        IsStoppingBuff = True
        ' estraiamo nome esterno
        linetoex = Right(Line, Len(Line) - InStr(Line, "MODULE") + 1)
        strName = nextToken_OF(linetoex)
        strName = nextToken_OF(linetoex)
      End If
    ElseIf Not IsStoppingBuff Then
      If Not Trim(Mid(Line, 2)) = "" Then
        If Not buffer = "" Then
          buffer = buffer & vbCrLf & Mid(Line, 4)
        Else
          buffer = Mid(Line, 4)
        End If
      End If
    End If
    If findTarget_Module(Line, "PROC PARAMETER") = "" Then
      n = 1
      Do While Not EOF(FDin)
        Line Input #FDin, Line
        n = n + 1
        If n = 3 Then
          If Not findTarget_Module(Line, "HISTORY RECORD") = "" Then
            If Not buffer = "" Then
              buffer = buffer & vbCrLf & Mid(Line, 4)
            Else
              buffer = Mid(Line, 4)
            End If
            Exit Do
          End If
        End If
        ' esco alla 3a se non trovo History oppure alla 5 dopo prendo tutto
        If n > 3 Then
          lineAppo = Trim(Line)
          If lineAppo = "" Then Exit Do
          If InStr(lineAppo, " ") > 0 Then
            lineAppo = Left(lineAppo, InStr(lineAppo, " ") - 1)
            If Not IsDate(lineAppo) Then
              If Not buffer = "" Then
                buffer = buffer & vbCrLf & Mid(Line, 4)
              Else
                buffer = Mid(Line, 4)
              End If
              Exit Do
            End If
          Else
            If Not buffer = "" Then
              buffer = buffer & vbCrLf & Mid(Line, 4)
            Else
              buffer = Mid(Line, 4)
            End If
            Exit Do
          End If
        End If
      Loop
      IsStoppingBuff = False
    End If
  Wend
  If Not FileNameO = "" Then
    Print #FDout, buffer
    Close FDout
  End If
  Close FDin
  MsgBox "Split ended", vbInformation
  Exit Sub
catch:
  'tmp
  MsgBox Err.Description
End Sub

Function nextToken_OF(inputString As String)
  Dim level As Integer, i As Integer, j As Integer
  Dim currentChar As String
  
  'Mangio i bianchi davanti (primo giro...)
  inputString = LTrim(inputString)
  If (Len(inputString) > 0) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "("
        i = 2
        level = 1
        nextToken_OF = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "("
              level = level + 1
              nextToken_OF = nextToken_OF & currentChar
            Case ")"
              level = level - 1
              nextToken_OF = nextToken_OF & currentChar
            Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nextToken_OF = nextToken_OF & currentChar
              Else
                Err.Raise 123, "nextToken_OF", "syntax error on " & inputString
                Exit Function
              End If
          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
      Case "'"
        inputString = Mid(inputString, 2)
        i = InStr(inputString, "'")
        If i Then
          nextToken_OF = Left(inputString, InStr(inputString, "'") - 1)
          inputString = Mid(inputString, InStr(inputString, "'") + 1)
        Else
          'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
          inputString = "''" & inputString
          Err.Raise 123, "nextToken_OF", "syntax error on " & inputString
          Exit Function
        End If
      Case Else
        i = InStr(inputString, " ")
        j = InStr(inputString, "(")
        If ((i > 0 And j > 0 And j < i) Or (i = 0 And j > 0)) Then i = j
        If (i > 0) Then
          nextToken_OF = Left(inputString, i - 1)
          inputString = Trim(Mid(inputString, i))
        Else
          nextToken_OF = inputString
          inputString = ""
        End If
    End Select
  End If
End Function

Sub AdjustToken(plinetoken As String, pline As String)
  If Len(plinetoken) > 1 Then
    '1) i due punti devono essere gestiti come un token distinto
    If Right(plinetoken, 1) = ":" Then
      plinetoken = Left(plinetoken, Len(plinetoken) - 1)
      pline = ": " & pline
    End If
    '2) la virgola come primo carattere la elimino
    If Left(plinetoken, 1) = "," Then
      plinetoken = Right(plinetoken, Len(plinetoken) - 1)
    End If
    '3) punto e virgola attaccato ad ultima parola
    If Right(plinetoken, 1) = ";" Then
      plinetoken = Left(plinetoken, Len(plinetoken) - 1)
      pline = "; " & pline
    End If
    '4) virgola separatore di due token
    If InStr(plinetoken, ",") > 0 Then
      pline = Right(plinetoken, Len(plinetoken) - InStr(plinetoken, ",")) & " " & pline
      plinetoken = Left(plinetoken, InStr(plinetoken, ",") - 1)
    End If
    '5) punto e virgola con attaccato qualcos'altro (commento o numero linea ad esempio)
    If Left(plinetoken, 1) = ";" Then
      pline = Right(plinetoken, Len(plinetoken) - 1)
      plinetoken = ";"
    End If
     '6) punto attaccato ad ultima parola trovata
    If Right(plinetoken, 1) = "." Then
       plinetoken = Left(plinetoken, Len(plinetoken) - 1)
      pline = ". " & pline
    End If
    '7) due punti con attaccato qualcos'altro (commento o numero linea ad esempio)
    If Left(plinetoken, 1) = ":" Then
      pline = Right(plinetoken, Len(plinetoken) - 1) & " " & pline
      plinetoken = ":"
    End If
    '8) due punti separatore di due token
    If InStr(plinetoken, ":") > 0 And Not plinetoken = ":" Then
      pline = ":" & Right(plinetoken, Len(plinetoken) - InStr(plinetoken, ":")) & " " & pline
      plinetoken = Left(plinetoken, InStr(plinetoken, ":") - 1)
    End If
  End If
End Sub

Function findTarget_Module(ByVal Line As String, ByVal target As String) As String
'a)
'ricerca la stringa all'interno della linea passata, utilizzando il metodo dei token
'restituisce la stringa vuota se il target � stato trovato
' restituisce una parte del target se solo una sua parte � stata trovata e la ricerca deve proseguire
' sulla riga successiva
' restituisce il target intero se la verifica � fallita
' Riassumendo, se strReturn � la stringa ritornata  strReturn = "": verifica OK  (true)
'                                                   strReturn <> target: proseguire ricerca
'                                                   strReturn = target: verifica KO  (false)

'b)
'la funzione si occupa anche di tener traccia dei commenti aperti/chiusi per il PLI

'c)
'la funzione tiene traccia anche dell'apertura degli THEN attraverso la variabile globale IsThenOpen
  Dim trgToken As String, lineToken As String, fulltarget As String
  Dim findfirst As Boolean
  Dim IsMacroOpen As Boolean
  Dim IsCSens As String
  
  IsCSens = False
  fulltarget = target

  'in attesa di idee migliori azzeriamo le parentesi e "'" per far funzionare il nexttoken
  ' valutare se anziche leggere linea per linea raggruppare le linee in modo da avere
  ' la parificazione delle parentesi
  Line = Replace(Replace(Line, "(", " "), ")", " ")
  Line = Replace(Line, "'", " ")
  Line = Mid(Line, 2)
  While Not (target = "" Or Line = "")
    trgToken = nextToken_OF(target)
    lineToken = nextToken_OF(Line)
    AdjustToken lineToken, Line ' in questa funzioncina aggiustiamo ci� che non gestisce il nextToken_OF
    'AC 06/10/2010 facciamo lo stesso anche per il target token
    AdjustToken trgToken, target
    While Not (IIf(IsCSens, trgToken, UCase(trgToken)) = IIf(IsCSens, lineToken, UCase(lineToken))) And Not Trim(Line) = ""
      If Not findfirst Then
        lineToken = nextToken_OF(Line)
        AdjustToken lineToken, Line ' in questa funzioncina aggiustiamo ci� che non gestisce il nextToken_OF
        '*****************************
      Else
        findTarget_Module = fulltarget  ' |----->  stringa non trovata
        While Not (Line = "")
          lineToken = nextToken_OF(Line)
          AdjustToken lineToken, Line ' in questa funzioncina aggiustiamo ci� che non gestisce il nextToken_OF
        Wend
        Exit Function
      End If
    Wend
    If Not Trim(Line) = "" Then
      findfirst = True
      'trgToken = nextToken_OF(target)
    Else  ' line = ""
      If (IIf(IsCSens, trgToken, UCase(trgToken)) = IIf(IsCSens, lineToken, UCase(lineToken))) Then
        findTarget_Module = target  ' |-----> stringa trovata in parte: ricerca deve proseguire su riga successiva
        Exit Function
      Else ' linea esaurita, ma ultimo token non trovato --> devo ricostroire target
        ' ricostruisco target
        If target = "" Then
          target = trgToken
        Else
          target = trgToken & " " & target
        End If
      End If
    End If
  Wend
  findTarget_Module = target ' |-----> esco qui se target = "" (target trovato) o linea vuota in partenza (restituisce target intero, quindi non trovato)
  
End Function

Private Sub CmdUnblock_Click()
  Dim fileName As String
  
  Select Case UCase(SSTab1.TabCaption(SSTab1.Tab))
    Case "IMPORT"
      If Trim(TxtFileLib) = "" Or InStr(TxtFileLib, "*") > 0 Then
        MsgBox "Select a File to Unblock", vbInformation, "i-4.Migration Tool - Library Import"
        Exit Sub
      End If
      If txtRecLen = "" Then
        MsgBox "Insert Record length for Unblock", vbInformation
        txtRecLen.SetFocus
        Exit Sub
      End If
      
      If Right(TxtFileLib, 1) = "*" Or Right(TxtFileLib, 1) = "\" Then
        Unblock_Directory TxtFileLib, Trim(txtRecLen), RtbAppo
      Else
        fileName = Dir(TxtFileLib)
        Do Until fileName <> "." Or fileName <> ".."
          fileName = Dir(TxtFileLib)
        Loop
        If Len(fileName) Then
          Unblock TxtFileLib, Trim(txtRecLen), RtbImport
        Else
          Unblock_Directory TxtFileLib, Trim(txtRecLen), RtbAppo
        End If
      End If
    Case "EXPORT"
  End Select
End Sub

Private Sub Command1_Click()
  Dim k As Integer
  
  Select Case UCase(SSTab1.TabCaption(SSTab1.Tab))
    Case "IMPORT"
      CD1.ShowOpen
      If Trim(CD1.fileName) <> "" Then
        If InStr(CD1.fileName, "*") = 0 Then
          TxtFileLib = CD1.fileName
          RtbImport.text = CaricaArchivio(TxtFileLib)
          RiconosciSchedaImport RtbImport.text
          For k = 0 To LstObjTypeImport.ListCount - 1
            LstObjTypeImport.Selected(k) = False
          Next k
        End If
      End If
    Case "EXPORT"
  End Select
End Sub

Private Sub Dir1_Change()
  TxtDirImport = Dir1.path
End Sub

Private Sub Drive1_Change()
  On Error GoTo ErrDrive

  Dir1.path = Drive1.Drive
  Exit Sub
ErrDrive:
  MsgBox "Drive specified cannot ready", vbCritical, "i-4.Migration - Library Import"
  Resume Next
End Sub

Private Sub Form_Load()
  ' SetParent Me.hWnd, Menu.TsParent
  'Resize
    
  'SQ 25-11-05
  SSTab1.TabVisible(0) = False
  SSTab1.TabEnabled(2) = False
  
  TxtDirImport = Dir1.path
   
  LstObjTypeImport.AddItem "Cobol Program (CBL SubDirectory)"
  LstObjTypeImport.AddItem "PLI Program (PLI SubDirectory)"
  LstObjTypeImport.AddItem "Assembler Program (ASM SubDirectory)"
  LstObjTypeImport.AddItem "EasyTrieve Program (EZT SubDirectory)"
   
  LstObjTypeImport.AddItem "Cobol Copy (CPY SubDirectory)"
  LstObjTypeImport.AddItem "PLI Include (INC SubDirectory)"
  LstObjTypeImport.AddItem "Assembler MACRO (MAC SubDirectory)"
  LstObjTypeImport.AddItem "EasyTrieve MACRO (EZM SubDirectory)"
   
  LstObjTypeImport.AddItem "BMS Maps (BMS SubDirectory)"
  LstObjTypeImport.AddItem "MFS Maps (MFS SubDirectory)"
   
  LstObjTypeImport.AddItem "MVS JCL (JCL SubDirectory)"
  LstObjTypeImport.AddItem "VSE JCL (JCL SubDirectory)"
  LstObjTypeImport.AddItem "MBM JCL (JCL SubDirectory)"
  LstObjTypeImport.AddItem "JCL Procedure (PRC SubDirectory)"
  LstObjTypeImport.AddItem "JCL Include  (PRC SubDirectory)"
  LstObjTypeImport.AddItem "JCL Parameters (PRM SubDirectory)"
   
  LstObjTypeImport.AddItem "DL/I DBD (DBD SubDirectory)"
  LstObjTypeImport.AddItem "DL/I PSB (PSB SubDirectory)"
   
  LstObjTypeImport.AddItem "DB2 DDL (DDL SubDirectory)"
  LstObjTypeImport.AddItem "Oracle DDL (DDL SubDirectory)"

  LstObjTypeImport.AddItem "FCT (CX SubDirectory)"
  LstObjTypeImport.AddItem "PCT (CX SubDirectory)"
  LstObjTypeImport.AddItem "PPT (CX SubDirectory)"

  LstObjTypeImport.AddItem "Unknown Objects (UNK SubDirectory)"
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True

  oldSelectType = -1
  
  LeggiArrayEBCDIC2ASCII
End Sub

Sub RiconosciSchedaImport(stringa)
  If Left(stringa, 1) = "V" Then
    ChkVarImport = vbChecked
  Else
    ChkVarImport = vbUnchecked
  End If
  
  SkSeparImport = ""
  If Left(stringa, 11) = "MEMBER NAME" Then
    SkSeparImport = "MEMBER NAME"
    Exit Sub
  End If
  If Left(stringa, 12) = "VMEMBER NAME" Then
    SkSeparImport = "MEMBER NAME"
    Exit Sub
  End If
  If Left(stringa, 10) = "ADD MEMBER" Then
    SkSeparImport = "ADD MEMBER"
    Exit Sub
  End If
  If Left(stringa, 11) = "VADD MEMBER" Then
    SkSeparImport = "ADD MEMBER"
    Exit Sub
  End If

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
  
  If m_fun.FnProcessRunning = True Then
    wScelta = m_fun.Show_MsgBoxError("FB01I")
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
End Sub

Private Sub LstObjTypeImport_Click()
  Dim i As Integer

  If oldSelectType = -1 Then
    oldSelectType = LstObjTypeImport.ListIndex
  ElseIf oldSelectType = LstObjTypeImport.ListIndex Then
    LstObjTypeImport.Selected(LstObjTypeImport.ListIndex) = False
    oldSelectType = -1
  Else
    LstObjTypeImport.Selected(oldSelectType) = False
    oldSelectType = LstObjTypeImport.ListIndex
  End If
End Sub

Sub SalvaEibGener()
  Dim k As Integer
  Dim SwSel As Boolean
   
  Select Case UCase(SSTab1.TabCaption(SSTab1.Tab))
    Case "IMPORT"
      If Trim(TxtFileLib) = "" Or InStr(TxtFileLib, "*") > 0 Then
        MsgBox "Select a Library to Import", vbInformation, "i-4.Migration - Library Import"
        Exit Sub
      End If
      If Trim(SkSeparImport) = "" Then
        MsgBox "Insert a correct ""Identifier Label"", please.", vbInformation, "i-4.Migration - Library Import"
        SkSeparImport.SetFocus
        Exit Sub
      End If
      SwSel = False
      If oldSelectType > -1 Then
        DefTypeObj = LstObjTypeImport.list(oldSelectType)
        k = InStr(DefTypeObj, "(")
        DefTypeObj = Mid$(DefTypeObj, k + 1, 3)
        SwSel = True
      End If
      
      If Not SwSel Then
        'SQ
        'MsgBox "Select a default for objects type", vbInformation, "i-4.Migration - Library Import"
        'Exit Sub
        DefTypeObj = ""
        SwSel = True
      End If
      ''''''''''''''''''''''''
      'SQ Gestione directory
      ''''''''''''''''''''''''
      Dim fileList() As String
      Dim path As String, dirName As String
      
      On Error Resume Next 'la dir da errore...
      dirName = Dir(TxtFileLib & "\", vbDirectory)
      On Error GoTo 0
      If Len(dirName) Then
        path = TxtFileLib & "\"
        fileList = getFiles(path)
          Dim i As Integer
          For i = 0 To UBound(fileList)
            If Len(fileList(0)) Then
              TxtFileLib = path & fileList(i)
              TxtDirImport = path & "..\" & fileList(i)
              StartImportFase
            Else
              Exit For
            End If
          Next
      Else
        StartImportFase
      End If
    Case "EXPORT"
  End Select
End Sub

Sub StartImportFase()
  Dim DirTmp As String
  Dim NomeFileTemp As String
  Dim NumFileTemp As Variant
  Dim NumFileI As Variant
  Dim NomeObj As String
  Dim wRec As String
  Dim wPos As Long
  
  On Error GoTo ErrorHandler
  Screen.MousePointer = vbHourglass

  RtbLog.text = ""
  AddLogEib "Start Import for " & TxtFileLib & " Library"
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Modifica fine linea
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If ChkCrLf = vbChecked Then
    AddLogEib "Start Transform LineFeed in CarriageReturn + LineFeed "
    DoEvents
    RtbCrlf.LoadFile TxtFileLib
    RtbCrlf.SaveFile TxtFileLib & ".bak", 1
    RtbCrlf.LoadFile TxtFileLib
    DoEvents
    RtbCrlf.text = Replace(RtbCrlf.text, vbCrLf, vbLf)
    DoEvents
    RtbCrlf.text = Replace(RtbCrlf.text, vbLf, vbCrLf)
    DoEvents
    RtbCrlf.SaveFile TxtFileLib, 1
    RtbCrlf.text = ""
    AddLogEib "End Transform LineFeed in CarriageReturn + LineFeed "
  End If
  
  Crea_Directory TxtDirImport
  DirTmp = TxtDirImport & "\Temp"
  Crea_Directory DirTmp

  ProgressBar1.Max = FileLen(TxtFileLib)
  ProgressBar1.Value = 0
  
  NumFileI = FreeFile
  NomeFileTemp = ""
  Open TxtFileLib For Binary As NumFileI
  While Not Loc(NumFileI) = FileLen(TxtFileLib)
    Line Input #NumFileI, wRec
    If ChkVarImport = vbChecked Then
      wRec = Mid(wRec, 2)
    End If
    If Left(wRec, Len(SkSeparImport)) = SkSeparImport Then
      If Trim(NomeFileTemp) <> "" Then
        Close NumFileTemp
        ' Mauro 12/02/2007
        'ProgressBar1.Value = ProgressBar1.Value + FileLen(NomeFileTemp)
        CatalogaFile NomeFileTemp, TxtDirImport, NomeObj
      End If
      NomeObj = Trim(Mid(wRec, Len(SkSeparImport) + 1))
      wPos = InStr(NomeObj, " ")
      If wPos > 0 Then NomeObj = Mid$(NomeObj, 1, wPos - 1)
      NomeFileTemp = DirTmp & "\" & NomeObj & ".tmp"
      NumFileTemp = FreeFile
      Open NomeFileTemp For Output As NumFileTemp
    Else
      ' Mauro 27/02/2008 : Perch� scrive la riga dal secondo byte?!?!? si mangia il primo "/" delle JCL
      'Print #NumFileTemp, Mid(wrec, 2)
      Print #NumFileTemp, wRec
    End If
    ' Mauro 12/02/2007
    ProgressBar1.Value = Loc(NumFileI)
    DoEvents
  Wend
  Close NumFileI
  Close NumFileTemp
   
  '  ProgressBar1.Value = ProgressBar1.Value + FileLen(NomeFileTemp)
  CatalogaFile NomeFileTemp, TxtDirImport, NomeObj
  AddLogEib "End Import for " & TxtFileLib & " Library"
   
  Screen.MousePointer = vbNormal
  Exit Sub
ErrorHandler:
  If Err.Number = 52 Then
    MsgBox "Indentifier Label must be in first line!", vbOKOnly + vbExclamation, "Attention!"
    Close NumFileI
  End If
  Screen.MousePointer = vbNormal
End Sub

Sub AddLogEib(stringa)
  DoEvents
  RtbLog.SelStart = Len(RtbLog.text)
  RtbLog.SelText = Now & " - " & stringa & vbCrLf
  RtbLog.Refresh
End Sub

'TILVIA
Sub FileReplaceString(fileName As String, SearchFor As String, ReplaceWith As String)
  Dim fNum As Long
  Dim lBuf As String
  
  fNum = FreeFile
  On Error Resume Next
  Err = 0
  Open fileName For Binary Access Read As fNum
  If Not Err = 0 Then Exit Sub
  lBuf = Space(LOF(fNum))
  Get #fNum, , lBuf
  Close fNum
  fNum = FreeFile
  lBuf = ReplaceInString(lBuf, SearchFor, ReplaceWith)
  Open fileName For Output Access Write As fNum
  Print #fNum, lBuf;
  Close fNum
End Sub

Function ReplaceInString(Source As String, SearchFor As String, ReplaceWith As String) As String
  Dim iVal As Integer
  Dim Workspace As String
  
  Workspace = Source
  iVal = 1
  Do
    iVal = InStr(iVal, Workspace, SearchFor, 1)
    If iVal = 0 Then Exit Do
    Workspace = Left$(Workspace, iVal - 1) + ReplaceWith + _
    Mid$(Workspace, iVal + Len(SearchFor))
    iVal = iVal + 1
  Loop
  ReplaceInString = Workspace
End Function

Function getFiles(dirName As String) As String()
  Dim fileName As String, files() As String
  
  On Error GoTo catch
  
  ReDim files(0)
  
  ' start the search
  fileName = Dir(dirName, vbDirectory)
  Do While Len(fileName)
    If fileName <> "." And fileName <> ".." Then
      files(UBound(files)) = fileName
      ReDim Preserve files(UBound(files) + 1)
    End If
    fileName = Dir
  Loop
  
  getFiles = files
  
  Exit Function
catch:
  'nop
End Function

