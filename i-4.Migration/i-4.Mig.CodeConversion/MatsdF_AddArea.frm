VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MatsdF_AddArea 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Project Areas"
   ClientHeight    =   7140
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9090
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7140
   ScaleWidth      =   9090
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtFileName 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   120
      TabIndex        =   10
      Text            =   "txtFileName"
      Top             =   5880
      Width           =   8775
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   495
      Left            =   7590
      TabIndex        =   4
      Top             =   6510
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   495
      Left            =   120
      TabIndex        =   3
      Top             =   6540
      Width           =   1455
   End
   Begin VB.Frame Frame3 
      Height          =   80
      Left            =   120
      TabIndex        =   2
      Top             =   6360
      Width           =   8865
   End
   Begin VB.Frame Frame2 
      Caption         =   "Source Type"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   8865
      Begin VB.OptionButton optType 
         Caption         =   "All"
         ForeColor       =   &H00C00000&
         Height          =   255
         Index           =   2
         Left            =   3150
         TabIndex        =   9
         Top             =   300
         Width           =   855
      End
      Begin VB.CommandButton cmdFind 
         Caption         =   "Find Areas"
         Height          =   375
         Left            =   6540
         TabIndex        =   8
         Top             =   240
         Width           =   2175
      End
      Begin VB.OptionButton optType 
         Caption         =   "Program"
         ForeColor       =   &H00C00000&
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   7
         Top             =   300
         Width           =   1095
      End
      Begin VB.OptionButton optType 
         Caption         =   "CopyBook"
         ForeColor       =   &H00C00000&
         Height          =   255
         Index           =   0
         Left            =   600
         TabIndex        =   6
         Top             =   300
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Areas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4695
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   8865
      Begin MSComctlLib.ListView lswAreas 
         Height          =   4335
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   8595
         _ExtentX        =   15161
         _ExtentY        =   7646
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "IdOggetto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Member"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Area"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Position"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Phisical Path"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Type"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Label Label1 
      Caption         =   "File Name:"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   5640
      Width           =   855
   End
End
Attribute VB_Name = "MatsdF_AddArea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim whereCondition As String

Dim wCopyBook() As t_Copy
Dim BarValue As Long
Public isVSAM As Boolean

Private Sub Form_Load()
  'setListAreas
  optType.Item(0) = True
  BarValue = 1
  txtFileName.Text = ""
End Sub
Private Sub cmdFind_Click()
  setListAreas
End Sub

Public Sub setListAreas()
  Dim tb As Recordset
  Dim query As String
  Dim wAppPath As String
  Dim i As Long
  'options; filtrare anche il nome...
 
  On Error GoTo errordB:
  
  'VC: ho tolto il livello per prendere anche tutte le strutture che non sono
  '    presenti dentro ad oggetti di tipo cpy
  'query = "select PsData_area.IdOggetto, bs_oggetti.nome, PsData_Area.nome, IdArea ,tipo,Directory_Input from PsData_Area,bs_oggetti where PsData_Area.idoggetto=bs_oggetti.idoggetto and livello = 1 " & whereCondition & " Order by bs_oggetti.Nome"
  query = "select PsData_area.IdOggetto, bs_oggetti.nome, PsData_Area.nome, IdArea ,tipo,Directory_Input from PsData_Area,bs_oggetti where PsData_Area.idoggetto=bs_oggetti.idoggetto  " & whereCondition & " Order by bs_oggetti.Nome"

  lswAreas.ListItems.Clear
  Set tb = m_fun.Open_Recordset(query)
  Screen.MousePointer = 11
  'rigaSelez = IIf(lswAreas.SelectedItem Is Nothing, "", lswAreas.SelectedItem)
    
  lswAreas.ListItems.Clear
   
  ReDim wCopyBook(0)
   
  'If rsjob.RecordCount > 0 Then rsjob.MoveFirst
  Do Until tb.EOF
    ReDim Preserve wCopyBook(UBound(wCopyBook) + 1)
    
    wAppPath = tb!directory_input
    
    wCopyBook(UBound(wCopyBook)).IdOggetto = tb(0).Value
    wCopyBook(UBound(wCopyBook)).Nome = tb(1).Value
    wCopyBook(UBound(wCopyBook)).Area = tb(2).Value
    wCopyBook(UBound(wCopyBook)).Position = tb(3).Value
    wCopyBook(UBound(wCopyBook)).PhisicalPath = Replace(wAppPath, "$", "...")
    wCopyBook(UBound(wCopyBook)).Tipo = tb(4).Value
    
    tb.MoveNext
    
  Loop
   
  'Carica i primi 20
  Load_Lista 1, UBound(wCopyBook)
  
  Screen.MousePointer = 0
  Exit Sub
errordB:
  Screen.MousePointer = 0
  'gestire
  MsgBox Err.Description
End Sub

Public Sub Load_Lista(wStart As Long, wStop As Long)
  Dim i As Long
  Dim listx As ListItem
   
  lswAreas.ListItems.Clear
   
  For i = wStart To wStop
    If wStop > UBound(wCopyBook) Then Exit For
    Set listx = lswAreas.ListItems.Add(, , wCopyBook(i).IdOggetto)
   
    listx.SubItems(1) = wCopyBook(i).Nome
    listx.SubItems(2) = wCopyBook(i).Area
    listx.SubItems(3) = wCopyBook(i).Position
    listx.SubItems(4) = wCopyBook(i).PhisicalPath
    listx.SubItems(5) = wCopyBook(i).Tipo
  Next i
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub lswAreas_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  Dim newSort As Integer, Ordinam As String
  Static oldsort As Integer, oldDirezione As String
  
  newSort = ColumnHeader.SubItemIndex
  lswAreas.Sorted = False
  oldsort = lswAreas.SortKey
  If oldsort = newSort Then
    If lswAreas.SortOrder = lvwAscending Then
      lswAreas.SortOrder = lvwDescending
    Else
      lswAreas.SortOrder = lvwAscending
    End If
  Else
    lswAreas.SortOrder = lvwAscending
  End If
  lswAreas.SortKey = newSort
  lswAreas.Sorted = True
End Sub

Private Sub optType_Click(Index As Integer)
  'whereCondition = Array("AND tipo='CPY'", "AND tipo='CBL'", "")
  Select Case Index
    Case 0 ' CPY
      whereCondition = "AND tipo='CPY'"
    Case 1 ' CBL
      whereCondition = "AND tipo='CBL'"
    Case 2 ' ALL
      whereCondition = ""
  End Select
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdOK_Click()
  If lswAreas.SelectedItem Is Nothing Then
    MsgBox "No item selected."
  ElseIf txtFileName = "" Then
    MsgBox "No file name."
  Else
    Add_Structure
''''    If Not isVSAM Then
''''      MadmdF_EditDli.AddArea lswAreas.SelectedItem, lswAreas.SelectedItem.SubItems(3), lswAreas.SelectedItem.SubItems(2)
''''      'MadmdF_EditDli.AddAreagSeg lswAreas.SelectedItem, lswAreas.SelectedItem.SubItems(3), lswAreas.SelectedItem.SubItems(2)
''''      'CaricaStr MadmdF_EditDli.ListStr, MadmdF_EditDli.ImageList1, Nomenodo 'commentato 11/07/2006
''''    Else
''''      MadmdF_VSAM.AddArea lswAreas.SelectedItem, lswAreas.SelectedItem.SubItems(3), lswAreas.SelectedItem.SubItems(2)
''''    End If
    Unload Me
  End If
End Sub
