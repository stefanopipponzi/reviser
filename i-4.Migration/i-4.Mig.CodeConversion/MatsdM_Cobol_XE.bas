Attribute VB_Name = "Matsdm_Cobol_XE"
Option Explicit
Global tipoFile As String
Public AddRighe As Long
Public j As Long
Public outputFile As String
Public Type matriceBMS
  idRiga As Long
  colonna(79) As String
  nomeField(79) As String
  Lunghezza(79) As String
  nomeFieldDaOccurs(79) As String
  nomeInOccurs(79) As String
  livelloInOccurs(79) As Integer
  MaxOccurs(79) As Integer
  ' Mauro 04/01/2008
  Valore(79) As String
End Type

Public Type VSAMDefinition
  TipoVsam As String
  Nome As String
  NomeDataset As String
  NomeInto As String
  fileStatus As String
  RecordKey As String
End Type

Public Function Restituisci_Selezione(TR As TreeView) As String
  Dim i As Long
  
  For i = 1 To TR.Nodes.Count
    If TR.Nodes(i).Checked Then
      Restituisci_Selezione = Trim(UCase(TR.Nodes(i).key))
      Exit For
    End If
  Next i
End Function

Public Function Seleziona_Istruzioni(Selezione As String, rIst As Recordset) As Long
  Dim Criterio As String
  Dim K1 As Long
  
  Criterio = ""
  
  Select Case Selezione
    Case "SRC" 'Tutti i programmi
      Set rIst = m_fun.Open_Recordset("Select distinct idoggetto From BS_Oggetti Where " & _
                                      "tipo = 'CPY' Order By IdOggetto")
    Case "SEL" 'Solo i programmi Selezionati
      For K1 = 1 To Menu.TsObjList.ListItems.Count
        If Menu.TsObjList.ListItems(K1).Selected Then
          If Trim(Criterio) <> "" Then
            Criterio = Criterio & ", " & Val(Menu.TsObjList.ListItems(K1).Text)
          Else
            Criterio = Val(Menu.TsObjList.ListItems(K1).Text)
          End If
        End If
      Next K1
      
      If Trim(Criterio) = "" Then Exit Function
      
      Criterio = " IdOggetto in (" & Criterio & ") "
      Set rIst = m_fun.Open_Recordset("Select distinct idoggetto From BS_Oggetti Where " & _
                                      Criterio & " Order By IdOggetto")
  End Select
  
  'Si posiziona al primo recordset
  'If rIst.RecordCount Then
    Seleziona_Istruzioni = rIst.RecordCount
  'Else
  '  Seleziona_Istruzioni = 0
  'End If
End Function

Public Sub trattaPanel(IdOggetto As Long)
  Dim rsObj As Recordset
  Dim numFile As Integer ', inputFile As String
  Dim wbuff() As String
  Dim BMS As String, NomeFile As String
  Dim fileInput As String
  
  Dim i As Long
'  Dim numCopy As Long
  
  On Error GoTo errorHandler
  ''  Set rsObj = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & IdOggetto)
  ''  inputFile = Replace(rsObj!directory_input, "$", Menu.TsPathDef) & "\" & _
  ''              rsObj!Nome & IIf(Len(Trim(rsObj!estensione)), "." & rsObj!estensione, "")
  ''  Numfile = FreeFile
  ''  ReDim wbuff(0)
  ''  i = 0
  ''  Open inputFile For Input As Numfile
  ''  Do Until EOF(Numfile)
  ''    i = i + 1
  ''    ReDim Preserve wbuff(i)
  ''    Line Input #Numfile, wbuff(i)
  ''  Loop
  ''  Close Numfile
  
  ' Le Opzioni sono esclusive!!
  ' OPTION: EXTRACT COPY PANEL
  If MatsdF_Cobol_XE.OptExtr.Value Then  ' Crea COPY pannel da LST
    EstraiCopyPanel (IdOggetto)
  ' OPTION: CONV PGM
  ElseIf MatsdF_Cobol_XE.OptPgm.Value Then ' Converte il pgm per vsa, e tctua
    Set rsObj = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & IdOggetto)
    If rsObj.RecordCount Then
      NomeFile = rsObj!Nome
      numFile = FreeFile
      ' Mauro 04/01/2008 : Perch� passargli solo l'idOggetto, se poi dentro deve rifare la query?!?!?!
      'wbuff = ConvertiPgm(IdOggetto)
      wbuff = ConvertiPgm(rsObj)
      outputFile = Menu.TsPathPrj & "\Output-prj\Panel\" & NomeFile & ".CBL"
      Open outputFile For Output As numFile
      For i = 1 To UBound(wbuff)
        Print #numFile, wbuff(i)
      Next i
      Close numFile
    End If
    rsObj.Close
  ' OPTION: COPYMAPOCC
  ElseIf MatsdF_Cobol_XE.OptCPY.Value Then ' Adegua le copy_map alla naming convenction e alle occurs di colonna
    Set rsObj = m_fun.Open_Recordset("select * from PsData_cmp where " & _
                                     "idoggetto = " & IdOggetto & " and " & _
                                     "idarea = 1 and nome like '%I' order by codice")
    If rsObj.RecordCount Then
      NomeFile = "R" & Mid(rsObj!Nome, 2, 6)
      
      ' Mauro 04/01/2008 : Perch� passargli solo l'idOggetto, se poi dentro deve rifare la query?!?!?!
      'wbuff = RedefineCopyMap(IdOggetto)
      wbuff = RedefineCopyMap(rsObj)
      numFile = FreeFile
      
      outputFile = Menu.TsPathPrj & "\Output-prj\Panel\" & NomeFile & ".copymap"
      Open outputFile For Output As numFile
      For i = 0 To UBound(wbuff)
        Print #numFile, wbuff(i)
      Next i
      Close numFile
    End If
    rsObj.Close
  ' OPTION: TO BMS
  ElseIf MatsdF_Cobol_XE.OptBMS.Value Then ' crea bms da panel
    Set rsObj = m_fun.Open_Recordset("select nome from PsData_cmp where " & _
                                     "idoggetto = " & IdOggetto & " order by codice")
    If rsObj.RecordCount Then
      NomeFile = Trim(Mid(rsObj!Nome, 1, 7))
      numFile = FreeFile
      
      BMS = convertiPanel(IdOggetto)
      wbuff = Split(BMS, vbCrLf)
      
      outputFile = Menu.TsPathPrj & "\Output-prj\Panel\" & NomeFile & ".bms"
      Open outputFile For Output As numFile
      For i = 1 To UBound(wbuff)
        Print #numFile, wbuff(i)
      Next i
      Close numFile
    End If
    rsObj.Close
  ' OPTION: ADD VALUE TO COPYMAP
  ElseIf MatsdF_Cobol_XE.optValue.Value Then ' Aggiunge Value alle copymap compilate in Unix
    Set rsObj = m_fun.Open_Recordset("select a.*, b.* from XE_CopyMap as a, BS_Oggetti as b where " & _
                                     "b.idoggetto = " & IdOggetto & " and " & _
                                     "b.nome = a.panel order by a.ordinale")
    If rsObj.RecordCount Then
      NomeFile = rsObj!Panel
      fileInput = Replace(rsObj!directory_input, "$", Menu.TsPathDef) & "\" & rsObj!Nome & rsObj!estensione
      
      wbuff = ValueCopyMap(rsObj, fileInput)
      numFile = FreeFile
      
      outputFile = Menu.TsPathPrj & "\Output-prj\Panel\" & NomeFile & ".copymap"
      Open outputFile For Output As numFile
      For i = 1 To UBound(wbuff)
        Print #numFile, wbuff(i)
      Next i
      Close numFile
    End If
    rsObj.Close
  End If

Exit Sub
errorHandler:
  If Err.Number = 76 Then
    m_fun.MkDir_API Menu.TsPathPrj & "\Output-Prj\Panel"
    Resume
  Else
    AggLog Err.Number & "-" & Err.Description, MatsdF_Cobol_XE.RTErr
  End If
End Sub

Private Sub EstraiCopyPanel(IdOggetto As Long)
'  Dim Copy As String
'  Dim inColonna As String
'  Dim Col7 As StringFormatEnum
  Dim numFile As Integer, numFileOut As Integer, inputFile As String
  Dim wbuff() As String, CopyBuff() As String
  
  Dim NomeFile As String
  Dim i As Long
'  Dim numCopy As Long
  Dim riga As Long
  
  Dim estrai As Boolean
  Dim rsObj As Recordset
  
  estrai = False
  
  Set rsObj = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & IdOggetto)
  If rsObj.RecordCount Then
    inputFile = Replace(rsObj!directory_input, "$", Menu.TsPathDef) & "\" & _
                rsObj!Nome & IIf(Len(Trim(rsObj!estensione)), "." & rsObj!estensione, "")
    numFile = FreeFile
    ReDim wbuff(0)
    riga = 0
    Open inputFile For Input As numFile
    Do Until EOF(numFile)
      riga = riga + 1
      ReDim Preserve wbuff(riga)
      If InStr(wbuff(riga), " REPRESENTS ") > 0 And i > 3 Then
        '???
      Else
        Line Input #numFile, wbuff(riga)
      End If
      If InStr(wbuff(riga), " REPRESENTS ") > 0 Or estrai Then
        If Not estrai Then ' prima volta
          estrai = True
          NomeFile = Trim(Mid(wbuff(riga), 25, 11))
          ReDim CopyBuff(0)
          CopyBuff(0) = "       01 " & NomeFile & " REPRESENTS PANEL" & vbCrLf & "           HEIGHT  24 WIDTH  80."
          i = 0
        End If
        If InStr(Mid(wbuff(riga), 22, 2), "01") = 0 Then
          If InStr(Mid(wbuff(riga), 21, 1), "*") = 0 And InStr(wbuff(riga), "COBOL/XE") = 0 And InStr(wbuff(riga), " REPRESENTS ") = 0 And InStr(wbuff(riga), " HEIGHT ") = 0 And InStr(wbuff(riga), " LINKAGE ") = 0 And InStr(wbuff(riga), " PROCEDURE ") = 0 Then
            i = i + 1
            ReDim Preserve CopyBuff(i)
            CopyBuff(i) = Mid(wbuff(riga), 15, 72)
          End If
        Else
          If InStr(wbuff(riga), " REPRESENTS ") = 0 Or i > 3 Then
            estrai = False
            numFileOut = FreeFile
            outputFile = Menu.TsPathPrj & "\Output-prj\Panel\" & NomeFile & ".cpypnl"
            Open outputFile For Output As numFileOut
            i = 0
            For i = 0 To UBound(CopyBuff)
              Print #numFileOut, CopyBuff(i)
            Next i
            riga = riga - 1
            Close numFileOut
          End If
        End If
      End If
    Loop
    Close numFile
  End If
  rsObj.Close
End Sub

Private Function convertiPanel(IdOggetto As Long) As String
  Dim BMS As String, bmsRedefine(24) As matriceBMS
  Dim Col16 As String, Col10 As String
  Dim NuovaRiga As String
  Dim Attrb As String, Initial As String
  Dim Valore As String, Nome As String
  Dim nomeField As String, bmsHead As String
    
  Dim Lunghezza As Long
  Dim deltaRiga As Long
  Dim riga As Long
  Dim rigaPrec As Long
  Dim colonna As Long
  Dim AppoColonna As Long
  Dim i As Integer ', Count As Long
  Dim countSucc As Long
  Dim RecordCount As Integer
'  Dim countOccurs As Integer
  Dim livelloOccurs As Integer
'  Dim livelloRedefine As Integer
  Dim livelloGruppo As Integer
  Dim Occurs As Integer
  Dim indInizio As Integer, indFine As Integer ', PosPrecedente As Integer
  
  Dim rsObj As Recordset
  Dim rsRedefine As Recordset
  Dim rsObjAppo As Recordset
  
  Dim SaltaCampo As Boolean
  Dim filler0 As Boolean
  Dim modificataBMS As Boolean
'  Dim primaVolta As Boolean
  
  Dim indice As Integer
  Dim strng As String
  Dim strngCont As String
'  Dim allinea As Integer
  Dim appoRow As String
  Dim sqlDelete As String
  Dim copyMove(500) As String
  Dim appoValore As String
  
  ' Inizializzazioni
  Col16 = Space(15) '"               "
  Col10 = Space(9) '"         "
  riga = 1
  colonna = 0
  SaltaCampo = False
  
  Set rsObj = m_fun.Open_Recordset("select * from PsData_cmp where " & _
                                   "idoggetto = " & IdOggetto & " order by codice")
  
  ' Header della BMS
  BMS = "***********************************************************************" & vbCrLf
  BMS = BMS & Continuation(Mid(rsObj!Nome, 1, 7) & "  DFHMSD TYPE=MAP,") & vbCrLf
  BMS = BMS & Continuation(Col16 + "MODE=INOUT, ") & vbCrLf
  BMS = BMS & Continuation(Col16 + "TERM=3270,  ") & vbCrLf
  BMS = BMS & Continuation(Col16 + "LANG=COBOL, ") & vbCrLf
  BMS = BMS & Continuation(Col16 + "DATA=FIELD, ") & vbCrLf
  BMS = BMS & Continuation(Col16 + "TIOAPFX=YES,") & vbCrLf
  BMS = BMS & Col16 & "CTRL=(FREEKB,FRSET)" & vbCrLf
  BMS = BMS & Mid(rsObj!Nome, 1, 7) & " DFHMDI SIZE=(24,80)" & vbCrLf
  
  bmsHead = BMS
  
  bmsRedefine(0).idRiga = 0
  bmsRedefine(0).colonna(0) = BMS
  bmsRedefine(0).nomeField(0) = Mid(rsObj!Nome, 1, 7)
  filler0 = False
  
  RecordCount = rsObj.RecordCount
  AppoColonna = 0
  Do While Not rsObj.EOF 'And rsObj!Ordinale < Recordcount
    SaltaCampo = False
    Attrb = ""
    Initial = ""
    NuovaRiga = ""
    Nome = rsObj!Nome
    nomeField = Nome
    Valore = rsObj!Valore
    Lunghezza = rsObj!Lunghezza

    If Len(Trim(rsObj!NomeRed)) Then ' sono in redefine e precisamente nella prima
      riga = 1
      colonna = 0
      modificataBMS = False
      Set rsRedefine = m_fun.Open_Recordset("select * from PsData_cmp where " & _
                                            "idoggetto = " & IdOggetto & " and NomeRed <> '' order by codice")
      Do While Not rsRedefine.EOF
        rigaPrec = 0
        Occurs = 1
        riga = 1
        colonna = 1
        'livelloRedefine = rsRedefine!Livello
        'countOccurs = 0
        'Occurs = 1
        livelloOccurs = 0
        filler0 = False
        'primaVolta = True
        rsObj.MoveNext
        
        If Not rsObj.EOF Then
          'Do While Len(Trim(rsObj!NomeRed)) = 0 And rsObj!Ordinale <= (RecordCount) ' devo gestire ultimo record
          Do Until rsObj.EOF
            If Len(Trim(rsObj!NomeRed)) > 0 Or rsObj!Ordinale > RecordCount Then
              Exit Do
            End If
            Nome = rsObj!Nome
            If rsObj!Tipo = "A" Then
              If rsObj!Lunghezza = 0 And rsObj!Occurs > 0 Then
                rigaPrec = 0
                Occurs = rsObj!Occurs
                livelloOccurs = rsObj!Livello
              End If
            Else
              If Nome <> "FILLER" Then
                Lunghezza = rsObj!Lunghezza
                Valore = rsObj!Valore
                Set rsObjAppo = m_fun.Open_Recordset("select * from PsData_cmp where " & _
                                                     "idoggetto = " & IdOggetto & "and " & _
                                                     "ordinale > " & rsObj!Ordinale & " order by codice")
                countSucc = 0
                
                deltaRiga = 1
                SaltaCampo = True
                
                Do While Not rsObjAppo.EOF And SaltaCampo ' da finire
                ' devo contare i byte che servono per chiudere l'occurs che contiene campo per calcolare la nuova riga
                  If rsObjAppo!Livello <= livelloOccurs Then
                    SaltaCampo = False
                  Else
                    If rsObjAppo!Byte = 79 And rsObjAppo!Occurs > 0 Then
                      countSucc = countSucc + 79 * rsObjAppo!Occurs
                    Else
                      countSucc = countSucc + rsObjAppo!Byte
                    End If
                    rsObjAppo.MoveNext
                  End If
                Loop
                rsObjAppo.Close
                rigaPrec = Int(rigaPrec / 79)
                If rigaPrec = 0 Then
                  rigaPrec = 1
                End If
                riga = Int(rsObj!Posizione / 79) + rigaPrec
                colonna = rsObj!Posizione - (79 * (riga - rigaPrec))
                If colonna > 1 Then
                  colonna = colonna + 1
                End If
                If filler0 Then
                  colonna = colonna + 1
                  filler0 = False
                End If
                If riga = 0 Then
                  riga = 1
                End If
                ''riga = Int(rsObj!Posizione / 79) + rigaprec ' per spiazzamento
                
                deltaRiga = Int(countSucc / 79)
                If deltaRiga = 0 Then
                  deltaRiga = 1
                Else
                  deltaRiga = deltaRiga + 1 + (riga - 1)
                End If
                For i = 1 To Occurs '- 1
                  If bmsRedefine(riga).nomeField(colonna) = "FILLER" Then
                     If bmsRedefine(riga).Lunghezza(colonna) = Lunghezza Then
                       bmsRedefine(riga).nomeFieldDaOccurs(colonna) = "ROW" & riga & "COL" & colonna
                       bmsRedefine(riga).nomeInOccurs(colonna) = Nome
                       bmsRedefine(riga).livelloInOccurs(colonna) = i
                       bmsRedefine(riga).MaxOccurs(colonna) = Occurs
                       modificataBMS = True
                     End If
                  Else
                    If InStr(bmsRedefine(riga).nomeField(colonna), "ROW") = 0 Then
                      ' problema segnala errore
                      'MsgBox "corrispond. errata riga colonna: " & Nome
                      AggLog "Corrispond. errata riga\colonna: " & Nome & " - IdOggetto: " & IdOggetto, MatsdF_Cobol_XE.RTErr
                      ' puo esere come nel mio caso che le redefine non rispecchiano le occurs
                      bmsRedefine(riga).nomeField(colonna) = "ROW" & riga & "COL" & colonna
                      bmsRedefine(riga).nomeInOccurs(colonna) = Nome
                      bmsRedefine(riga).livelloInOccurs(colonna) = i
                      bmsRedefine(riga).MaxOccurs(colonna) = Occurs
                      bmsRedefine(riga).Lunghezza(colonna) = Lunghezza
                    Else
                      bmsRedefine(riga).nomeInOccurs(colonna) = Nome
                      bmsRedefine(riga).livelloInOccurs(colonna) = i
                      bmsRedefine(riga).MaxOccurs(colonna) = Occurs
                    End If
                  End If
                  riga = riga + deltaRiga
                Next i
              Else
                If rsObj!Lunghezza > 0 Then
                  If rsObj!Lunghezza = 79 And rsObj!Occurs > 0 Then
                    rigaPrec = rigaPrec + 79 * rsObj!Occurs
                  Else
                    rigaPrec = rigaPrec + rsObj!Lunghezza
                  End If
                End If
                If rsObj!Lunghezza = 0 Then
                  filler0 = True
                End If
              End If
            End If
            rsObj.MoveNext
          Loop ' campi di una redefine
        End If
        rsRedefine.MoveNext
      Loop
      rsRedefine.Close
    End If
''''''''NON OCCURS '''''''''''''''''''''''''''
'''    If PosPrecedente > rsObj!Posizione Then
'''      i = rsObj.RecordCount
'''      rsObj!Tipo = "A"
'''    End If
    If Not rsObj.EOF Then 'And rsObj!Ordinale < Recordcount Then
      If rsObj!Tipo <> "A" Or (rsObj!Tipo = "A" And riga > 0 And rsObj!Livello > 5) Then
        If rsObj!Tipo = "A" Then
          livelloGruppo = rsObj!Livello
          Nome = rsObj!Nome
          nomeField = Nome
          Lunghezza = 0
          ' Valore = rsObj!Valore
          Valore = ""
          rsObj.MoveNext
          'Do While rsObj!Livello > livelloGruppo
          Do Until rsObj.EOF
            If rsObj!Livello <= livelloGruppo Then
              Exit Do
            End If
            Valore = Valore & rsObj!Valore
            Lunghezza = Lunghezza + rsObj!Lunghezza
            rsObj.MoveNext
            '''''i = i + 1
          Loop
          rsObj.MovePrevious
          Valore = Replace(Valore, "'", "")
          Valore = "'" & Valore & "'"
        End If
        AppoColonna = colonna + Lunghezza
        If AppoColonna > 79 Then
          riga = riga + 1
          If filler0 Then
            colonna = 2
          Else
            colonna = 1
          End If
          AppoColonna = Lunghezza
        Else
          colonna = colonna + 1
        End If
        filler0 = False
        
        If Nome = "FILLER" Then ' Campo in bms senza nome
          If Len(Trim(Valore)) > 0 And Lunghezza > 0 Then
            If InStr(Valore, "LOW-VALUE") > 0 Then ' Campo da saltare
              NuovaRiga = ""
              SaltaCampo = True
            Else ' campo con valore fisso di output o Input in Occurs
              indInizio = InStr(Valore, "DATA-ENTRY")
              If indInizio > 0 Then ' Campo Input in Occurs
                nomeField = "ROW" & riga & "COL" & colonna
                NuovaRiga = Continuation("ROW" & riga & "COL" & colonna & " DFHMDF POS=(" & riga & "," & colonna & "),LENGTH=" & Lunghezza & ",")
                Attrb = Attrb & "UNPROT,FSET"
              Else
                NuovaRiga = Continuation(Col10 & "DFHMDF POS=(" & riga & "," & colonna & "),LENGTH=" & Lunghezza & ",")
                Attrb = Attrb & "ASKIP" ' era prot
              End If
            End If
          Else ' Campo da saltare FILLER senza VALUE
            If Lunghezza = 0 Then 'dovrebbe indicare qualcosa
              filler0 = True
              ' AppoColonna = AppoColonna + 1
            End If
            NuovaRiga = ""
            SaltaCampo = True
          End If
        End If
        
        If Nome <> "FILLER" And Not SaltaCampo Then ' Campo I/O Con Nome
          NuovaRiga = Continuation(Nome & " DFHMDF POS=(" & riga & "," & colonna & "),LENGTH=" & Lunghezza & ",")
          If InStr(Valore, "DATA-ENTRY") > 0 And Not SaltaCampo Then ' Campo di INPUT
            Attrb = Attrb & "UNPROT,FSET"
          Else
            Attrb = Attrb & "ASKIP"
          End If
        End If
        appoValore = ""
        indInizio = InStr(Valore, "'")
        If indInizio > 0 And Not SaltaCampo Then ' presente valore di default, INITIAL
          indFine = InStrRev(Valore, "'")
          If InStr(Valore, "ALL ") > 0 Then
            ' Mauro
            appoValore = "ALL '" & Mid(Valore, indInizio + 1, 1) & "'"
            strng = ""
            strngCont = ""
            For indice = 1 To rsObj!Lunghezza
              If (Len(Col16 & strng) + 11) >= 72 And strngCont = "" Then ' Initial su piu righe
                strngCont = strng & Mid(Valore, indInizio + 1, 1) & "*" & vbCrLf
                strng = Col16 '& Mid(Valore, indinizio + 1, 1)
              Else
                strng = strng & Mid(Valore, indInizio + 1, 1)
              End If
              ''strng = strngcont & strng
            Next indice
            strng = strngCont & strng
            Initial = "INITIAL='" & strng & "'"
          Else
            Initial = "INITIAL=" & Mid(Valore, indInizio, (indFine - indInizio) + 2)
            ' Mauro
            appoValore = Mid(Valore, indInizio, (indFine - indInizio) + 2)
            If Len(Initial) > 56 Then
              Initial = Mid(Initial, 1, 56) & "*" & vbCrLf & Col16 & Mid(Initial, 57, Len(Initial)) & vbCrLf
            End If
          End If
        End If
        indInizio = InStr(Valore, "SPACES")
        If indInizio > 0 And Not SaltaCampo Then ' presente valore di default SPACES, INITIAL
          Initial = "INITIAL=' '"
          ' Mauro
          appoValore = "ALL SPACE"
        End If
        
        ' Mauro 07/01/2008: Teoricamente "NON-DISPLAY" andrebbe convertito in "DRK", ma non � sempre cos�
        'If InStr(Valore, "NON-DISPLAY") > 0 And Not SaltaCampo Then ' Attributo DRK
        '  Attrb = Attrb & ",DRK"
        '  Valore = Replace(Valore, "NON-DISPLAY", "")
        'Else
          If InStr(Valore, "HIGH-INTENSITY") > 0 And Not SaltaCampo Then ' Atributo BRT
            Attrb = Attrb & ",BRT"
            Valore = Replace(Valore, "HIGH-INTENSITY", "")
          End If
        'End If
        
        If Not SaltaCampo Then
          If Attrb <> "" Then
            NuovaRiga = NuovaRiga & vbCrLf & Continuation(Col16 & "ATTRB=(" & Attrb & "),")
          End If
          If Initial <> "" Then
            NuovaRiga = NuovaRiga & vbCrLf & Col16 & Initial & vbCrLf
          End If
          BMS = BMS & NuovaRiga
          If riga < 25 And colonna < 80 Then ' da eliminare
          bmsRedefine(riga).idRiga = riga
          bmsRedefine(riga).colonna(colonna) = NuovaRiga
          bmsRedefine(riga).nomeField(colonna) = nomeField
          bmsRedefine(riga).Lunghezza(colonna) = Lunghezza
          bmsRedefine(riga).Valore(colonna) = appoValore
          End If
        End If
        If filler0 Then
          'colonna = AppoColonna
          filler0 = False
        Else
          colonna = AppoColonna + 1 ' space tra campi
        End If
      End If
''      PosPrecedente = rsObj!Posizione
''      Next i
      rsObj.MoveNext
    End If
  Loop
  If modificataBMS Then
    'allinea = 0
''   devo aggiornare bms con bms redefine fo trovato campi filler da cambiare con il nome
    BMS = bmsHead
    
    For riga = 1 To 24
      For colonna = 1 To 79
        If Len(bmsRedefine(riga).nomeField(colonna)) > 0 Then
          If Len(bmsRedefine(riga).nomeFieldDaOccurs(colonna)) > 0 Then
            appoRow = bmsRedefine(riga).nomeFieldDaOccurs(colonna) & _
                      Mid(bmsRedefine(riga).colonna(colonna), 9, Len(bmsRedefine(riga).colonna(colonna)))
            appoRow = Replace(appoRow, vbCrLf, "")
            appoRow = Replace(appoRow, "*", "")
            ' Mauro 07/01/2008 : � un campo DATA-ENTRY, quindi vuole la FSET
            Dim mAttrb As Long
            mAttrb = InStr(appoRow, "ATTRB=")
            If InStr(mAttrb, appoRow, "FSET") = 0 Then
              appoRow = Left(appoRow, mAttrb - 1) & Replace(appoRow, "),     ", ",FSET),", mAttrb)
            End If
            appoRow = Mid(appoRow, 1, 71) & "*" & vbCrLf & _
                      Mid("col16" & Trim(appoRow), 72, 71) & "*" & vbCrLf & Mid("col16" & Trim(appoRow), 144, 72)
            
            BMS = BMS & appoRow & vbCrLf
          Else
            BMS = BMS & bmsRedefine(riga).colonna(colonna)
          End If
        End If
      Next colonna
    Next riga
  End If
  ' Tail della BMS
  BMS = BMS & "*" & vbCrLf
  BMS = BMS & Col10 & "DFHMSD TYPE=FINAL" & vbCrLf
  BMS = BMS & Col10 & "END" & vbCrLf
  '' COPY MOVE pgm to bms
  sqlDelete = "Delete from XE_CopyMap where " & _
              "ordinale > -1 and " & _
              "panel = '" & Trim(Mid(bmsRedefine(0).nomeField(0), 1, 7)) & "';"
  Menu.TsConnection.Execute sqlDelete

  Set rsObj = m_fun.Open_Recordset("select * from XE_CopyMap")
  i = 0
  'Count = 0
  For riga = 1 To 24
    For colonna = 1 To 79
      If Len(bmsRedefine(riga).nomeInOccurs(colonna)) > 0 Then
        rsObj.AddNew
        rsObj!Panel = Trim(bmsRedefine(0).nomeField(0))
        rsObj!Ordinale = i
        rsObj!nomeinpanel = bmsRedefine(riga).nomeInOccurs(colonna)
        rsObj!nomeinbms = "ROW" & riga & "COL" & colonna
        rsObj!livelloOccurs = bmsRedefine(riga).livelloInOccurs(colonna)
        rsObj!MaxOccurs = bmsRedefine(riga).MaxOccurs(colonna)
        ' Mauro 04/01/2008
        rsObj!Value = bmsRedefine(riga).Valore(colonna)
        rsObj!riga = riga
        rsObj!colonna = colonna
        rsObj!Descrizione = "OCCURS"
        rsObj.Update
        
        copyMove(i) = Col10 & "MOVE " & bmsRedefine(riga).nomeInOccurs(colonna) & "(" & bmsRedefine(riga).livelloInOccurs(colonna) & ") TO " & "ROW" & riga & "COL" & colonna & "I OF " & bmsRedefine(0).nomeField(0) & "I"
        i = i + 1
      Else
        If Len(bmsRedefine(riga).nomeField(colonna)) > 0 And bmsRedefine(riga).nomeField(colonna) <> "FILLER" Then
          rsObj.AddNew
          rsObj!Panel = bmsRedefine(0).nomeField(0)
          rsObj!Ordinale = i
          rsObj!nomeinpanel = bmsRedefine(riga).nomeField(colonna)
          rsObj!nomeinbms = Trim(bmsRedefine(riga).nomeField(colonna))
        ' Mauro 04/01/2008
          rsObj!Value = Trim(bmsRedefine(riga).Valore(colonna))
          
          rsObj!livelloOccurs = 0
          rsObj!MaxOccurs = 0
          rsObj!Descrizione = "NORMALE"
          rsObj.Update
          
          copyMove(i) = Col10 & "MOVE " & bmsRedefine(riga).nomeField(colonna) & " TO " & Trim(bmsRedefine(riga).nomeField(colonna)) & "I OF " & bmsRedefine(0).nomeField(0) & "I"
          i = i + 1
        End If
      End If
    Next colonna
  Next riga
  rsObj.Close
  
  convertiPanel = BMS
End Function

' Mauro 04/01/2008 : Perch� passargli solo l'idOggetto, se poi dentro deve rifare la query?!?!?!
'Private Function RedefineCopyMap(IdOggetto As Long) As String()
Private Function RedefineCopyMap(rsObj As Recordset) As String()
  Dim CopyMap() As String, NomePanel As String
  
  'Dim rsObj As Recordset
  Dim rsObjCopyMap As Recordset
  Dim i As Long, Posizione As Long
  Dim Col8 As String, Col11 As String
  
  Dim MaxOccurs As Long, fillerOccurs As Boolean
  
  Col8 = Space(7) '"       "
  Col11 = Space(10) '"          "
    
  'Set rsObj = m_fun.Open_Recordset("select * from PsData_cmp where " & _
  '                                 "idoggetto = " & IdOggetto & " and " & _
  '                                 "idarea = 1 and nome like '%I' order by codice")
  rsObj.MoveFirst
  NomePanel = Mid(rsObj!Nome, 1, 7)
  
  Set rsObjCopyMap = m_fun.Open_Recordset("select * from XE_copymap where " & _
                                          "Panel = '" & NomePanel & "' order by ordinale")

  rsObj.MoveNext
  
  Do While Not rsObj.EOF And Not rsObjCopyMap.EOF ' da or a and PAG1NAM
    If rsObjCopyMap!nomeinbms & "I" = rsObj!Nome Then
      rsObjCopyMap!PosCopyMap = rsObj!Posizione
      rsObjCopyMap!Picture = rsObj!Picture
      rsObjCopyMap!Byte = rsObj!Byte
      
      rsObjCopyMap.Update
      rsObjCopyMap.MoveNext
    End If
    rsObj.MoveNext
  Loop
  'rsObj.Close
  rsObjCopyMap.MoveFirst
  fillerOccurs = False
  
  ReDim CopyMap(0)
  MaxOccurs = 0
  i = 0
  ReDim Preserve CopyMap(i)
  CopyMap(i) = Col8 & "01 " & NomePanel & " REDEFINES " & NomePanel & "I."
  i = i + 1
'  rsObjCopyMap.MoveNext
  Posizione = 1
  Do While Not rsObjCopyMap.EOF
    If rsObjCopyMap!Descrizione = "NORMALE" Then
      fillerOccurs = False
      ReDim Preserve CopyMap(i)
      CopyMap(i) = Col11 & "20 FILLER PIC X(" & (rsObjCopyMap!PosCopyMap - Posizione - 1) & ")."
      i = i + 1
      ReDim Preserve CopyMap(i)
      CopyMap(i) = Col11 & "20 " & rsObjCopyMap!nomeinpanel & "AC PIC X(001)."
      i = i + 1
      ReDim Preserve CopyMap(i)
      CopyMap(i) = Col11 & "20 " & rsObjCopyMap!nomeinpanel & " PIC " & rsObjCopyMap!Picture & "."
      i = i + 1
      Posizione = rsObjCopyMap!PosCopyMap + rsObjCopyMap!Byte ' tolata + posizione
    Else
      If rsObjCopyMap!livelloOccurs = 1 And Not fillerOccurs Then
        If Posizione = 1 Then
          ReDim Preserve CopyMap(i)
          CopyMap(i) = Col11 & "20 FILLER  PIC X(12)."
          i = i + 1
          Posizione = 13
        End If
        ReDim Preserve CopyMap(i)
        CopyMap(i) = Col11 & "20 FILLER OCCURS " & rsObjCopyMap!MaxOccurs & "."
        MaxOccurs = rsObjCopyMap!MaxOccurs
        fillerOccurs = True
        i = i + 1
''''        CopyMap(i) = Col11 & "   25 FILLER            PIC X(002)."
        ReDim Preserve CopyMap(i)
        CopyMap(i) = Col11 & "   25 FILLER PIC X(" & (rsObjCopyMap!PosCopyMap - Posizione - 1) & ")."
        i = i + 1
        ReDim Preserve CopyMap(i)
        CopyMap(i) = Col11 & "   25 " & rsObjCopyMap!nomeinpanel & "AC PIC X(001)."
        i = i + 1
        ReDim Preserve CopyMap(i)
        CopyMap(i) = Col11 & "   25 " & rsObjCopyMap!nomeinpanel & " PIC " & rsObjCopyMap!Picture & "."
        i = i + 1
        Posizione = rsObjCopyMap!PosCopyMap + rsObjCopyMap!Byte
      Else
        If rsObjCopyMap!livelloOccurs = 1 Then
'          i = i + 1
 ''''         CopyMap(i) = Col11 & "   25  FILLER            PIC X(002)."
          ReDim Preserve CopyMap(i)
          CopyMap(i) = Col11 & "   25 FILLER PIC X(" & (rsObjCopyMap!PosCopyMap - Posizione - 1) & ")."
          i = i + 1
          ReDim Preserve CopyMap(i)
          CopyMap(i) = Col11 & "   25 " & rsObjCopyMap!nomeinpanel & "AC PIC X(001)."
          i = i + 1
          ReDim Preserve CopyMap(i)
          CopyMap(i) = Col11 & "   25 " & rsObjCopyMap!nomeinpanel & " PIC " & rsObjCopyMap!Picture & "."
          i = i + 1
          Posizione = rsObjCopyMap!PosCopyMap + rsObjCopyMap!Byte
        Else
          fillerOccurs = False
          Posizione = rsObjCopyMap!PosCopyMap + rsObjCopyMap!Byte
        End If
      End If
    End If
    rsObjCopyMap.MoveNext
  Loop
  
  rsObjCopyMap.Close
  RedefineCopyMap = CopyMap
End Function

Private Function Continuation(rigaConACapo As String) As String
  Dim Length As Integer
  Dim Count As Integer
  
  Length = 71 - Len(rigaConACapo)
  For Count = 1 To Length
    rigaConACapo = rigaConACapo & " "
  Next Count
  rigaConACapo = rigaConACapo & "*"
  Continuation = rigaConACapo
End Function

' Mauro 04/01/2008 : Perch� passargli solo l'idOggetto, se poi dentro deve rifare la query?!?!?!
'Private Function ConvertiPgm(IdOggetto As Long) As String()
Private Function ConvertiPgm(rsObj As Recordset) As String()
'  Dim Copy As String
'  Dim Incolonna As String
'  Dim Col7 As StringFormatEnum
  Dim numFile As Integer, inputFile As String
  Dim rigaInEsame As String
  Dim appoString As String
'  Dim Col12 As String
  Dim Spunto As String
  
  Dim indInizio As Integer, indFine As Integer, indTab As String
  Dim newPgm() As String ', CopyBuff() As String
  
  'Dim NomeFile As String
  Dim NomeInto As String, TipoVsam As String
  Dim NomeDataset As String, RecordKey As String
'  Dim NomeAreaDataset As String
  Dim fileStatus As String
  Dim NumeroFile As Long
  Dim i As Long, Count As Long
  Dim righePgm As Long
  Dim riga As Long
  Dim appo As Long
'  Dim rigaSplit() As String
  
  Dim FileControl As Boolean, FileSection As Boolean, Working As Boolean
  Dim Linkage As Boolean, Procedure As Boolean, Identification As Boolean
  Dim punto As Boolean
  Dim FD As Boolean
  Dim rigaStart(10) As String
  Dim finito As Boolean
  Dim Operatore As String
  Dim verso As String
  
  'Dim rsObj As Recordset
  Dim FileInPgm(20) As VSAMDefinition
  
  Dim tag As String, tagc As String
  
  Dim fieldda As String
  Dim fielda  As String
  Dim fieldstatus As String
  
  Dim perfDescending As String, isDesc As Boolean
  
  tag = "ITER   "
  tagc = "ITER  *"

'  Col12 = "           "
  FileControl = False
  FileSection = False
  Working = False
  Linkage = False
  Procedure = False
  FD = False
  Identification = False
  
  'Set rsObj = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & IdOggetto)
  inputFile = Replace(rsObj!directory_input, "$", Menu.TsPathDef) & "\" & _
              rsObj!Nome & IIf(Len(Trim(rsObj!estensione)), "." & rsObj!estensione, "")
  numFile = FreeFile
  ReDim newPgm(0)
  i = 0
  Open inputFile For Input As numFile
  Do Until EOF(numFile)
    i = i + 1
    ReDim Preserve newPgm(i)
    Line Input #numFile, newPgm(i)
  Loop
  Close numFile
  righePgm = i
  riga = 1
  If rsObj!Tipo = "CPY" Then
    FileControl = True
    FileSection = True
    Working = True
    Linkage = True
    Procedure = True
  Else
    Do While riga < righePgm And Not FileControl And Not FileSection And Not Working
      If InStr(newPgm(riga), " IDENTIFICATION ") > 0 Or Identification Then
        Identification = True
      Else
        newPgm(riga) = ""
      End If
      If InStr(newPgm(riga), " FILE-CONTROL") > 0 Then
        FileControl = True
      End If
      If InStr(newPgm(riga), " FILE ") > 0 And InStr(newPgm(riga), " SECTION.") > 0 Then
        FileSection = True
      End If
      If InStr(newPgm(riga), " WORKING ") > 0 Then
        Working = True
      End If
      riga = riga + 1
    Loop
  End If
  NumeroFile = 0 ' indice file vsam
  Do While riga < righePgm
    rigaInEsame = newPgm(riga)
    If Mid(rigaInEsame, 7, 1) <> "*" And Left(rigaInEsame, 4) <> Left(tag, 4) Then
      ' FILE-CONTROLL
      If FileControl Then
        If InStr(rigaInEsame, " SELECT ") > 0 Then
          NumeroFile = NumeroFile + 1
          appo = InStr(rigaInEsame, "SELECT ") + 6
          FileInPgm(NumeroFile).Nome = Trim(Mid(rigaInEsame, appo, 20))
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63)
        End If
        If InStr(rigaInEsame, " ASSIGN ") > 0 Then
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63)
          appo = InStr(rigaInEsame, " DB-") + 4
          FileInPgm(NumeroFile).NomeDataset = "'" & Trim(Mid(rigaInEsame, appo, 20)) & "'"
          FileInPgm(NumeroFile).NomeInto = Trim(Mid(rigaInEsame, appo, 20)) & "-REC"
        End If
        If InStr(rigaInEsame, " ORGANIZATION ") > 0 Then
          If InStr(rigaInEsame, " RELATIVE ") > 0 Then
            FileInPgm(NumeroFile).TipoVsam = "RRDS"
          Else
            FileInPgm(NumeroFile).TipoVsam = "KSDS"
          End If
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63)
        End If
        If InStr(rigaInEsame, " ACCESS ") > 0 Then
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63)
        End If
        If InStr(rigaInEsame, " RECORD ") > 0 Or InStr(rigaInEsame, " RELATIVE ") > 0 Then
          If InStr(rigaInEsame, " KEY ") > 0 Then
            appoString = Trim(Mid(rigaInEsame, InStr(rigaInEsame, " KEY ") + 5, 20))
            FileInPgm(NumeroFile).RecordKey = appoString
          Else
            FileInPgm(NumeroFile).RecordKey = ""
          End If
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63)
        End If
        If InStr(rigaInEsame, " STATUS ") > 0 Then
          appo = InStr(rigaInEsame, "STATUS ") + 6
          FileInPgm(NumeroFile).fileStatus = Trim(Mid(rigaInEsame, appo, InStr(rigaInEsame, ".") - Len(appo)))
          FileInPgm(NumeroFile).fileStatus = Replace(FileInPgm(NumeroFile).fileStatus, ".", "")
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63)
        End If
        If InStr(rigaInEsame, " FILE ") > 0 And InStr(rigaInEsame, " SECTION.") > 0 Then
          FileSection = True
          FileControl = False
        End If
      End If
      ' FILE SECTION
      If FileSection Then
        If InStr(rigaInEsame, " FILE ") > 0 Then
          rigaInEsame = "ITER  * Insert Manually Sequential Files in File Section" & vbCrLf & tagc & Mid(rigaInEsame, 8, 63) & vbCrLf
          rigaInEsame = rigaInEsame & tag & "WORKING-STORAGE SECTION." & vbCrLf
        Else
          If InStr(rigaInEsame, " FD ") > 0 Or FD Then
            If Mid(rigaInEsame, 8, 2) <> "01" Then
              FD = True
              rigaInEsame = tagc & Mid(rigaInEsame, 8, 63)
            Else
              FD = False
            End If
          End If
          If InStr(rigaInEsame, " WORKING-STORAGE") > 0 Then
            appoString = tag & "COPY WKITER." & vbCrLf
            appoString = appoString & tag & "COPY DFHAID." & vbCrLf
            appoString = appoString & tag & "COPY DFHBMSCA." & vbCrLf
            rigaInEsame = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf & appoString
            Working = True
            FileSection = False
            FileControl = False
          End If
        End If
      End If
      ' WORKING-STORAGE
      If Working Then
        If InStr(rigaInEsame, " WORKING-STORAGE") > 0 Then
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf
        End If
        If InStr(rigaInEsame, " LINKAGE ") > 0 Then
          Linkage = True
          Working = False
          FileSection = False
          FileControl = False
        End If
        If InStr(rigaInEsame, " PROCEDURE ") > 0 Then
          Procedure = True
          Working = False
          FileSection = False
          FileControl = False
        End If
      End If
      ' LINKAGE
      If Linkage Then
        If InStr(rigaInEsame, " PROCEDURE ") > 0 Then
          Procedure = True
          Linkage = False
          Working = False
          FileSection = False
          FileControl = False
        End If
      End If
      ' PROCEDURE
      If Procedure Then
        If InStr(rigaInEsame, " PROCEDURE ") > 0 Then
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63)
          If InStr(newPgm(riga + 1), "TCTUA") > 0 Then
            newPgm(riga + 1) = tagc & Mid(newPgm(riga + 1), 8, 63) '& vbCrlf
          End If
          rigaInEsame = tag & "PROCEDURE DIVISION." & vbCrLf & rigaInEsame & vbCrLf & tag & "COPY CICSAREE." & vbCrLf
        End If
        
        ' COMMENTO OPEN
        If InStr(rigaInEsame, " OPEN ") > 0 Then
          indTab = InsSpace(InStr(rigaInEsame, " OPEN ") - 7)
          fileStatus = ""
          For i = 1 To NumeroFile
            If InStr(rigaInEsame, FileInPgm(i).Nome) > 0 Or InStr(rigaInEsame, FileInPgm(i).NomeInto) > 0 Then
              fileStatus = Trim(FileInPgm(i).fileStatus)
              NomeDataset = Trim(FileInPgm(i).NomeDataset)
              RecordKey = Trim(FileInPgm(i).RecordKey)
              TipoVsam = Trim(FileInPgm(i).TipoVsam)
              NomeInto = Trim(FileInPgm(i).NomeInto)
            End If
          Next i
          If Len(fileStatus) = 0 Then
            fileStatus = "<NOT-FOUND>"
            NomeDataset = "<NOT-FOUND>"
            RecordKey = "<NOT-FOUND>"
          End If
          appoString = ""
          punto = False
          Do While InStr(rigaInEsame, " MOVE ") = 0 And InStr(rigaInEsame, " IF ") = 0 _
            And InStr(rigaInEsame, " EXEC ") = 0 And Not punto And InStr(rigaInEsame, " GO ") = 0 _
            And InStr(rigaInEsame, " ELSE") = 0
            For i = 1 To NumeroFile
              If InStr(rigaInEsame, FileInPgm(i).Nome) > 0 Or InStr(rigaInEsame, FileInPgm(i).NomeInto) > 0 Then
                fileStatus = Trim(FileInPgm(i).fileStatus)
                NomeDataset = Trim(FileInPgm(i).NomeDataset)
                RecordKey = Trim(FileInPgm(i).RecordKey)
                TipoVsam = Trim(FileInPgm(i).TipoVsam)
                NomeInto = Trim(FileInPgm(i).NomeInto)
              End If
            Next i
            If InStr(rigaInEsame, ".") > 0 Then
              punto = True
              rigaInEsame = tagc & indTab & Mid(rigaInEsame, 8, 63) & vbCrLf
              rigaInEsame = rigaInEsame & tag & indTab & "MOVE " & NomeDataset & " TO WK-DATASETNAME" & vbCrLf
              rigaInEsame = rigaInEsame & tag & indTab & "PERFORM MANAGE-OPEN THRU MANAGE-OPEN-EX." & vbCrLf
              rigaInEsame = rigaInEsame & tag & indTab & "MOVE WK-RESP TO " & fileStatus
            Else
              appoString = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf
              appoString = appoString & tag & indTab & "MOVE " & NomeDataset & " TO WK-DATASETNAME" & vbCrLf
              appoString = appoString & tag & indTab & "PERFORM MANAGE-OPEN THRU MANAGE-OPEN-EX" & vbCrLf
              appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus
              newPgm(riga) = appoString
              riga = riga + 1
              rigaInEsame = newPgm(riga)
            End If
          Loop
        End If
        ' COMMENTO CLOSE
        If InStr(rigaInEsame, " CLOSE ") > 0 Then
          appoString = ""
          punto = False
          indTab = InsSpace(InStr(rigaInEsame, " CLOSE ") - 7)
          rigaInEsame = Mid(rigaInEsame, 1, 72)
          Count = 0
          rigaStart(Count) = Mid(rigaInEsame, 1, 72)
          rigaInEsame = ""
          Do While InStr(rigaStart(Count), " MOVE ") = 0 And InStr(rigaStart(Count), " IF ") = 0 _
                 And InStr(rigaStart(Count), " EXEC ") = 0 And Not punto And InStr(rigaStart(Count), " GO ") = 0 _
                 And InStr(rigaStart(Count), " ELSE") = 0
            If InStr(rigaStart(Count), ".") > 0 Then
              punto = True
            End If
            fileStatus = ""
            For i = 1 To NumeroFile
              If InStr(rigaStart(Count), FileInPgm(i).Nome) > 0 Or InStr(rigaStart(Count), FileInPgm(i).NomeInto) > 0 Then
                fileStatus = Trim(FileInPgm(i).fileStatus)
                NomeDataset = Trim(FileInPgm(i).NomeDataset)
                RecordKey = Trim(FileInPgm(i).RecordKey)
                TipoVsam = Trim(FileInPgm(i).TipoVsam)
                NomeInto = Trim(FileInPgm(i).NomeInto)
              End If
            Next i
            If Len(fileStatus) = 0 Then
              fileStatus = "<NOT-FOUND>"
              NomeDataset = "<NOT-FOUND>"
              RecordKey = "<NOT-FOUND>"
            End If
            ' EXEC CICS ENDBR DATASET()
            ' END-EXEC
              appoString = ""
              rigaInEsame = rigaInEsame & tagc & Mid(rigaStart(Count), 8, 63) & vbCrLf
              appoString = appoString & tagc & indTab & "EXEC CICS ENDBR DATASET(" & NomeDataset & ")" & vbCrLf
              If punto Then
                appoString = appoString & tagc & indTab & "END-EXEC." & vbCrLf
              Else
                appoString = appoString & tagc & indTab & "END-EXEC" & vbCrLf
              End If
              
             Count = Count + 1
             rigaStart(Count) = Mid(newPgm(riga + Count), 1, 72)
             newPgm(riga + Count) = appoString
          Loop
   ' ROB 11
             newPgm(riga + Count) = rigaStart(Count)
  ' no      riga = riga + count - 1
          rigaInEsame = rigaInEsame ' & appostring
        End If
        
        ' CAMBIO START
        If InStr(rigaInEsame, " START ") > 0 And InStr(rigaInEsame, " MOVE ") = 0 _
           And Mid(rigaInEsame, 7, 1) <> "*" Then
          
          verso = ""
          Operatore = ""
          finito = False
                
          indTab = InsSpace(InStr(rigaInEsame, " START ") - 6)
          rigaInEsame = Mid(rigaInEsame, 1, 72)
          Count = 0
          fileStatus = ""
          For Count = 1 To NumeroFile
            If InStr(rigaInEsame, FileInPgm(Count).Nome) > 0 Or InStr(rigaInEsame, FileInPgm(Count).NomeInto) > 0 Then
              fileStatus = Trim(FileInPgm(Count).fileStatus)
              NomeDataset = Trim(FileInPgm(Count).NomeDataset)
              NomeInto = Trim(FileInPgm(Count).NomeInto)
              RecordKey = Trim(FileInPgm(Count).RecordKey)
              TipoVsam = Trim(FileInPgm(Count).TipoVsam)
            End If
          Next Count
          If Len(fileStatus) = 0 Then
            fileStatus = "<NOT-FOUND>"
            NomeDataset = "<NOT-FOUND>"
            RecordKey = "<NOT-FOUND>"
          End If
          Count = 0
          rigaInEsame = ""
          Do While Not finito
            rigaStart(Count) = Mid(newPgm(riga + Count), 1, 72)
            If InStr(rigaStart(Count), " LESS ") > 0 Or InStr(rigaStart(Count), " < ") > 0 Then
              If InStr(rigaStart(Count), " NOT ") > 0 Then
                Operatore = "GTEQ"
              Else ' per usi futuri non criticare
                Operatore = "GTEQ"
              End If
            End If
            If InStr(rigaStart(Count), " GREATER ") > 0 Or InStr(rigaStart(Count), " > ") > 0 Then
              Operatore = "GTEQ"
            End If
            
            If InStr(rigaStart(Count), " DESCENDING") > 0 Then
              verso = "DESCENDING <NOT-FOUND>"
              ' Mauro 10/01/2008
              isDesc = True
              perfDescending = perfDescending & vbCrLf
              perfDescending = perfDescending & tag & "9999-READPREV-" & Replace(NomeDataset, "'", "") & vbCrLf
              perfDescending = perfDescending & tag & "       EXEC CICS READPREV DATASET(" & NomeDataset & ")" & vbCrLf
              perfDescending = perfDescending & tag & "                          INTO   (" & NomeInto & ")" & vbCrLf
              perfDescending = perfDescending & tag & "                          RIDFLD (" & RecordKey & ")" & vbCrLf
              perfDescending = perfDescending & tag & "                          RESP   (WK-CICS-RESP)" & vbCrLf
              perfDescending = perfDescending & tag & "       END-EXEC." & vbCrLf
              perfDescending = perfDescending & tag & "9999-READPREV-" & Replace(NomeDataset, "'", "") & "-EXIT." & vbCrLf
              perfDescending = perfDescending & tag & "                    EXIT. " & vbCrLf
            End If
            
            If InStr(rigaStart(Count), ".") > 0 Or InStr(rigaStart(Count), " GO ") > 0 _
               Or InStr(rigaStart(Count), " MOVE ") > 0 Or InStr(rigaStart(Count), " IF ") > 0 _
               Or Len(Trim(rigaStart(Count))) = 0 Or InStr(rigaStart(Count), " ELSE") > 0 _
               Or InStr(rigaStart(Count), "EXIT") > 0 Or Mid(rigaStart(Count), 7, 1) = "*" Then
              finito = True
            Else
              rigaInEsame = rigaInEsame & tagc & Mid(rigaStart(Count), 8, 63) & vbCrLf
              Count = Count + 1
            End If
          Loop
          If Count = 0 Then
            rigaInEsame = tagc & Mid(rigaStart(Count), 8, 63) & vbCrLf
          Else
            newPgm(riga + 1) = rigaStart(Count)
          End If
          rigaInEsame = rigaInEsame & tagc & indTab & "        " & verso & vbCrLf
          ' Mauro 10/01/2008
          If isDesc Then
            rigaInEsame = rigaInEsame & tagc & indTab & "Programs Requiring User Review" & vbCrLf
            isDesc = False
          End If
          rigaInEsame = rigaInEsame & tag & indTab & "EXEC CICS STARTBR DATASET(" & NomeDataset & ")" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "     RIDFLD(" & RecordKey & ")" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & Operatore & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "    RESP        (WK-CICS-RESP)" & vbCrLf
          If TipoVsam = "RRDS" Then
            rigaInEsame = rigaInEsame & tag & indTab & "     RRN" & vbCrLf
          End If
          rigaInEsame = rigaInEsame & tag & indTab & "END-EXEC" & vbCrLf
          ' Mauro 03/01/2008
          rigaInEsame = rigaInEsame & tag & indTab & "EVALUATE WS-CICS-RESP" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "    WHEN DFHRESP(NORMAL)" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "        CONTINUE" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "    WHEN DFHRESP(INVREQ)" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "        EXEC CICS ENDBR FILE(" & NomeDataset & ")END-EXEC" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "        EXEC CICS STARTBR" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "                 FILE(" & NomeDataset & ")" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "                 RIDFLD(" & RecordKey & ")" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "                 RESP(WK-CICS-RESP)" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "        END-EXEC" & vbCrLf
          rigaInEsame = rigaInEsame & tag & indTab & "END-EVALUATE" & vbCrLf
          
          appoString = ""
          appoString = tag & indTab & "PERFORM MANAGE-STATUS THRU MANAGE-STATUS-EX" & vbCrLf
          If InStr(rigaInEsame, ".") > 0 Then
            appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & "." & vbCrLf
          Else
            appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & vbCrLf
          End If
          rigaInEsame = rigaInEsame & appoString
       '''''   riga = riga + count - 1
        End If
        
        'CAMBIO READ
        If InStr(rigaInEsame, " READ ") > 0 And InStr(rigaInEsame, " MOVE ") = 0 Then
          indTab = InsSpace(InStr(rigaInEsame, " READ ") - 7)
          rigaInEsame = Mid(rigaInEsame, 1, 72)
          Count = 0
          fileStatus = ""
          For Count = 1 To NumeroFile
            If InStr(rigaInEsame, FileInPgm(Count).Nome) > 0 Or InStr(rigaInEsame, FileInPgm(Count).NomeInto) > 0 Then
              fileStatus = Trim(FileInPgm(Count).fileStatus)
              NomeDataset = Trim(FileInPgm(Count).NomeDataset)
              NomeInto = Trim(FileInPgm(Count).NomeInto)
              RecordKey = Trim(FileInPgm(Count).RecordKey)
              TipoVsam = Trim(FileInPgm(Count).TipoVsam)
            End If
          Next Count
          If Len(fileStatus) = 0 Then
            fileStatus = "<NOT-FOUND>"
            NomeDataset = "<NOT-FOUND>"
            RecordKey = "<NOT-FOUND>"
          End If
          indInizio = InStr(rigaInEsame, "READ ")
          If InStr(rigaInEsame, " NEXT") > 0 Then
            indFine = InStr(rigaInEsame, " NEXT")
            'NomeAreaDataset = Trim(Mid(rigainesame, indinizio + 5, indfine - (indinizio + 5)))
            appoString = tag & indTab & "EXEC CICS READNEXT DATASET(" & NomeDataset & ")" & vbCrLf
            appoString = appoString & tag & indTab & "    INTO (" & NomeInto & ")" & vbCrLf
            appoString = appoString & tag & indTab & "    RIDFLD (" & RecordKey & ")" & vbCrLf
            appoString = appoString & tag & indTab & "    RESP        (WK-CICS-RESP)" & vbCrLf
            If TipoVsam = "RRDS" Then
              appoString = appoString & tag & indTab & "    RRN" & vbCrLf
            End If
            appoString = appoString & tag & indTab & "END-EXEC" & vbCrLf
            appoString = appoString & tag & indTab & "PERFORM MANAGE-STATUS THRU MANAGE-STATUS-EX" & vbCrLf
            If InStr(rigaInEsame, ".") > 0 Then
              appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & "." & vbCrLf
            Else
              appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & vbCrLf
            End If
            rigaInEsame = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf & appoString
          End If
          If InStr(rigaInEsame, " LOCK") > 0 Then
            indFine = InStr(rigaInEsame, " WITH ")
            'NomeAreaDataset = Trim(Mid(rigainesame, indinizio + 5, indfine - (indinizio + 5)))
'''            appostring = Replace(rigainesame, "READ", "EXEC CICS READ DATASET(")
            appoString = tag & indTab & "EXEC CICS READ DATASET(" & NomeDataset & ")" & vbCrLf
'''            appostring = Replace(appostring, ".", "")
'''            appostring = Replace(appostring, "LOCK", "")
'''            appostring = Replace(appostring, " WITH ", ")") & vbCrLf
            appoString = appoString & tag & indTab & "      INTO (" & NomeInto & ") " & "UPDATE" & vbCrLf
            appoString = appoString & tag & indTab & "    RIDFLD (" & RecordKey & ")" & vbCrLf
            If TipoVsam = "RRDS" Then
              appoString = appoString & tag & indTab & "    RRN" & vbCrLf
            End If
            appoString = appoString & tag & indTab & "    RESP        (WK-CICS-RESP)" & vbCrLf
            appoString = appoString & tag & indTab & "END-EXEC" & vbCrLf
            appoString = appoString & tag & indTab & "PERFORM MANAGE-STATUS THRU MANAGE-STATUS-EX" & vbCrLf
            If InStr(rigaInEsame, ".") > 0 Then
              appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & "." & vbCrLf
            Else
              appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & vbCrLf
            End If
            rigaInEsame = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf & appoString
          End If
          
          If InStr(rigaInEsame, tag) = 0 Then  ' READ NORMALE
            indInizio = InStr(rigaInEsame, " READ ")
          '  NomeAreaDataset = Trim(Mid(rigainesame, indinizio + 5, 72 - (indinizio + 5)))
           ' NomeAreaDataset = Replace(NomeAreaDataset, ".", "")
'''            appostring = tag & indtab & "EXEC CICS READ DATASET(" & NomeDataset & ")"
            appoString = tag & indTab & "EXEC CICS READ DATASET(" & NomeDataset & ")" & vbCrLf
'''            appostring = Replace(appostring, ".", "") & vbCrLf
            appoString = appoString & tag & indTab & "    INTO (" & NomeInto & ")" & vbCrLf
            appoString = appoString & tag & indTab & "    RIDFLD (" & RecordKey & ")" & vbCrLf
            appoString = appoString & tag & indTab & "    RESP        (WK-CICS-RESP)" & vbCrLf
            If TipoVsam = "RRDS" Then
              appoString = appoString & tag & indTab & "    RRN" & vbCrLf
            End If
            appoString = appoString & tag & indTab & "END-EXEC" & vbCrLf
            appoString = appoString & tag & indTab & "PERFORM MANAGE-STATUS THRU MANAGE-STATUS-EX" & vbCrLf
            If InStr(rigaInEsame, ".") > 0 Then
              appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & "." & vbCrLf
            Else
              appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & vbCrLf
            End If
            rigaInEsame = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf & appoString
          End If
        End If
        'CAMBIO WRITE
        If InStr(rigaInEsame, " WRITE ") > 0 And InStr(rigaInEsame, " MOVE ") = 0 Then
          indTab = InsSpace(InStr(rigaInEsame, " WRITE ") - 7)
          rigaInEsame = Mid(rigaInEsame, 1, 72)
          Count = 0
          fileStatus = ""
          For Count = 1 To NumeroFile
            If InStr(rigaInEsame, FileInPgm(Count).Nome) > 0 Or InStr(rigaInEsame, FileInPgm(Count).NomeInto) > 0 Then
              fileStatus = Trim(FileInPgm(Count).fileStatus)
              NomeDataset = Trim(FileInPgm(Count).NomeDataset)
              NomeInto = Trim(FileInPgm(Count).NomeInto)
              RecordKey = Trim(FileInPgm(Count).RecordKey)
              TipoVsam = Trim(FileInPgm(Count).TipoVsam)
            End If
          Next Count
          If Len(fileStatus) = 0 Then
            fileStatus = "<NOT-FOUND>"
            NomeDataset = "<NOT-FOUND>"
            RecordKey = "<NOT-FOUND>"
          End If
          indInizio = InStr(rigaInEsame, " WRITE ")
'          NomeAreaDataset = Trim(Mid(rigainesame, indinizio + 7, 72 - (indinizio + 7)))
 '         NomeAreaDataset = Replace(NomeAreaDataset, ".", "")
'''          appostring = tag & indtab & "EXEC CICS WRITE DATASET(" & NomeDataset & ")"
          appoString = tag & indTab & "EXEC CICS WRITE DATASET(" & NomeDataset & ")" & vbCrLf
'''          appostring = Replace(appostring, ".", "") & vbCrLf
          appoString = appoString & tag & indTab & "       FROM (" & NomeInto & ")" & vbCrLf
          appoString = appoString & tag & indTab & "     RIDFLD (" & RecordKey & ")" & vbCrLf
           appoString = appoString & tag & indTab & "    RESP        (WK-CICS-RESP)" & vbCrLf
          If TipoVsam = "RRDS" Then
            appoString = appoString & tag & indTab & "     RRN" & vbCrLf
          End If
          appoString = appoString & tag & indTab & "END-EXEC" & vbCrLf
          appoString = appoString & tag & indTab & "PERFORM MANAGE-STATUS THRU MANAGE-STATUS-EX" & vbCrLf
          If InStr(rigaInEsame, ".") > 0 Then
            appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & "." & vbCrLf
          Else
            appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & vbCrLf
          End If
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf & appoString
        End If
        'CAMBIO REWRITE
        If InStr(rigaInEsame, " REWRITE ") > 0 And InStr(rigaInEsame, " MOVE ") = 0 Then
          indTab = InsSpace(InStr(rigaInEsame, " REWRITE ") - 7)
          rigaInEsame = Mid(rigaInEsame, 1, 72)
          Count = 0
          fileStatus = ""
          For Count = 1 To NumeroFile
            If InStr(rigaInEsame, FileInPgm(Count).Nome) > 0 Or InStr(rigaInEsame, FileInPgm(Count).NomeInto) > 0 Then
              fileStatus = Trim(FileInPgm(Count).fileStatus)
              NomeDataset = Trim(FileInPgm(Count).NomeDataset)
              NomeInto = Trim(FileInPgm(Count).NomeInto)
              RecordKey = Trim(FileInPgm(Count).RecordKey)
            End If
          Next Count
          If Len(fileStatus) = 0 Then
            fileStatus = "<NOT-FOUND>"
            NomeDataset = "<NOT-FOUND>"
            RecordKey = "<NOT-FOUND>"
          End If
          indInizio = InStr(rigaInEsame, " REWRITE ")
'          NomeAreaDataset = Trim(Mid(rigainesame, indinizio + 9, 72 - (indinizio + 9)))
'          NomeAreaDataset = Replace(NomeAreaDataset, ".", "")
''          appostring = tag & indtab & "EXEC CICS REWRITE DATASET(" & NomeDataset & ")"
          appoString = tag & indTab & "EXEC CICS REWRITE DATASET(" & NomeDataset & ")" & vbCrLf
          appoString = Replace(appoString, ".", "")
          appoString = appoString & tag & indTab & "       FROM (" & NomeInto & ")" & vbCrLf
          appoString = appoString & tag & indTab & "    RESP        (WK-CICS-RESP)" & vbCrLf
          appoString = appoString & tag & indTab & "END-EXEC" & vbCrLf
          appoString = appoString & tag & indTab & "PERFORM MANAGE-STATUS THRU MANAGE-STATUS-EX" & vbCrLf
          If InStr(rigaInEsame, ".") > 0 Then
            appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & "."
          Else
            appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus
          End If
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf & appoString
        End If
        'CAMBIO UNLOCK
        If InStr(rigaInEsame, " UNLOCK ") > 0 Then
          indTab = InsSpace(InStr(rigaInEsame, " UNLOCK ") - 7)
          rigaInEsame = Mid(rigaInEsame, 1, 72)
          Count = 0
          fileStatus = ""
          For Count = 1 To NumeroFile
            If InStr(rigaInEsame, FileInPgm(Count).Nome) > 0 Or InStr(rigaInEsame, FileInPgm(Count).NomeInto) > 0 Then
              fileStatus = Trim(FileInPgm(Count).fileStatus)
              NomeDataset = Trim(FileInPgm(Count).NomeDataset)
              NomeInto = Trim(FileInPgm(Count).NomeInto)
            End If
          Next Count
          If Len(fileStatus) = 0 Then
            fileStatus = "<NOT-FOUND>"
            NomeDataset = "<NOT-FOUND>"
            RecordKey = "<NOT-FOUND>"
          End If
'''          appostring = tag & indtab & "EXEC CICS" & vbCrLf
'''          appostring = appostring & tag & Replace(Mid(rigainesame, 8, 63), "UNLOCK", "    UNLOCK DATASET (") & vbCrLf
'''          appostring = Replace(appostring, "RECORD", ")")
'''          appostring = Replace(appostring, ".", "")
          appoString = tag & indTab & "EXEC CICS UNLOCK DATASET(" & NomeDataset & ")" & vbCrLf
          appoString = appoString & tag & indTab & "    RESP        (WK-CICS-RESP)" & vbCrLf
          appoString = appoString & tag & indTab & "END-EXEC" & vbCrLf
          appoString = appoString & tag & indTab & "PERFORM MANAGE-STATUS THRU MANAGE-STATUS-EX" & vbCrLf
          If InStr(rigaInEsame, ".") > 0 Then
            appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & "."
          Else
            appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus
          End If
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf & appoString
        End If
        'CAMBIO DELETE
        If InStr(rigaInEsame, " DELETE ") > 0 And InStr(rigaInEsame, " MOVE ") = 0 Then
          indTab = InsSpace(InStr(rigaInEsame, " DELETE ") - 7)
          If InStr(rigaInEsame, " RECORD") > 0 Then
            rigaInEsame = Mid(rigaInEsame, 1, 72)
            Count = 0
            fileStatus = ""
            For Count = 1 To NumeroFile
              If InStr(rigaInEsame, FileInPgm(Count).Nome) > 0 Or InStr(rigaInEsame, FileInPgm(Count).NomeInto) > 0 Then
                fileStatus = Trim(FileInPgm(Count).fileStatus)
                NomeDataset = Trim(FileInPgm(Count).NomeDataset)
                NomeInto = Trim(FileInPgm(Count).NomeInto)
                RecordKey = Trim(FileInPgm(Count).RecordKey)
                TipoVsam = Trim(FileInPgm(Count).TipoVsam)
              End If
            Next Count
            If Len(fileStatus) = 0 Then
              fileStatus = "<NOT-FOUND>"
              NomeDataset = "<NOT-FOUND>"
              RecordKey = "<NOT-FOUND>"
            End If
'''            appostring = tag & indtab & "EXEC CICS" & vbCrLf
'''            appostring = appostring & tag & Replace(Mid(rigainesame, 8, 63), "DELETE", "    DELETE DATASET (")
'''            appostring = Replace(appostring, "RECORD", ")")
'''            appostring = Replace(appostring, ".", "") & vbCrLf
            appoString = tag & indTab & "EXEC CICS DELETE DATASET(" & NomeDataset & ")" & vbCrLf
            If TipoVsam = "RRDS" Then
              appoString = appoString & tag & indTab & "     RRN" & vbCrLf
            End If
            appoString = appoString & tag & indTab & "    RESP        (WK-CICS-RESP)" & vbCrLf
            appoString = appoString & tag & indTab & "END-EXEC" & vbCrLf
            appoString = appoString & tag & indTab & "PERFORM MANAGE-STATUS THRU MANAGE-STATUS-EX" & vbCrLf
            If InStr(rigaInEsame, ".") > 0 Then
              appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus & "."
            Else
              appoString = appoString & tag & indTab & "MOVE WK-RESP TO " & fileStatus
            End If
            rigaInEsame = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf & appoString
          End If
        End If
      ''  NORMALIZE
        If InStr(rigaInEsame, " NORMALIZE ") > 0 Then
          
          fieldda = ""
          fielda = ""
          fieldstatus = ""
          appoString = ""
          
          indTab = InStr(rigaInEsame, " NORMALIZE")
          rigaInEsame = Mid(rigaInEsame, 1, 72)
          indInizio = InStr(rigaInEsame, " INTO")
          If indInizio > 0 Then
            fieldda = Trim(Mid(rigaInEsame, indTab + 10, indInizio - (10 + indTab)))
            fielda = Trim(Mid(rigaInEsame, indInizio + 6, 72 - (6 + indInizio)))
          Else
            fieldda = Trim(Mid(rigaInEsame, indTab + 10, 72 - indTab + 10))
          End If
          indTab = InsSpace(InStr(rigaInEsame, " NORMALIZE") - 7)
          appoString = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf
          rigaInEsame = newPgm(riga + 1)
          newPgm(riga + 1) = ""
          indInizio = InStr(rigaInEsame, " INTO")
          indFine = InStr(rigaInEsame, " STATUS ")
          If indInizio > 0 Then
            indInizio = indInizio + 5
            fielda = Trim(Mid(rigaInEsame, indInizio, indFine - indInizio))
          Else
            indInizio = 1
            If Len(fielda) = 0 Then
              fielda = Trim(Mid(rigaInEsame, indInizio, indFine - indInizio))
            End If
          End If
          
          If InStr(rigaInEsame, ".") > 0 Then
            fieldstatus = Trim(Mid(rigaInEsame, indFine + 8, InStr(rigaInEsame, ".") - indFine - 7))
          Else
            fieldstatus = Trim(Mid(rigaInEsame, indFine + 8, 72 - indFine - 8)) ' + 8))
          End If
          appoString = appoString & tagc & Mid(rigaInEsame, 8, 63) & vbCrLf
          rigaInEsame = appoString
          
          appoString = tag & indTab & "MOVE " & fieldda & " TO WK-SCREEN-VALUE" & vbCrLf
          If Len(fielda) = 0 Then
            fielda = "NOT-FOUND"
          End If
          
          appoString = appoString & tag & indTab & "PERFORM MANAGE-NORMALIZE THRU MANAGE-NORMALIZE-EX" & vbCrLf
          appoString = appoString & tag & indTab & "MOVE WK-VALIDATE-NUMBER TO " & fielda & vbCrLf
          appoString = appoString & tag & indTab & "MOVE WK-NSTAT TO " & fieldstatus & vbCrLf
          
          
          rigaInEsame = rigaInEsame & appoString
        
        End If
      ''  CURRENT-TRANS-ID
        If InStr(rigaInEsame, " CURRENT-TRANS-ID") > 0 Then
          appoString = tag & Replace(Mid(rigaInEsame, 8, 63), "CURRENT-TRANS-ID", "EIBTRNID")
          rigaInEsame = tagc & Mid(rigaInEsame, 8, 63) & vbCrLf & appoString
        End If
      ''  DISPLAY CLEAR
        If InStr(rigaInEsame, " DISPLAY ") > 0 Then
          If InStr(rigaInEsame, " CLEAR") > 0 Then
            rigaInEsame = tagc & Mid(rigaInEsame, 8, 63)
          End If
        End If
      ''  STOP RUN
        If InStr(rigaInEsame, " STOP ") > 0 Then
          If InStr(rigaInEsame, " RUN") > 0 Then
            rigaInEsame = tagc & Mid(rigaInEsame, 8, 63)
          End If
        End If
      ''  AUTOSKIP  ricorda non-display
        If InStr(rigaInEsame, " AUTOSKIP ") > 0 _
         Or InStr(rigaInEsame, " NON-DISPLAY ") > 0 _
         Or InStr(rigaInEsame, " DATA-ENTRY ") > 0 Then
          rigaInEsame = Mid(rigaInEsame, 8, 63)
          indTab = InsSpace(InStr(rigaInEsame, " MOVE"))
          If InStr(rigaInEsame, ".") > 0 Then
             Spunto = "."
          Else
             Spunto = ""
          End If
          rigaInEsame = Replace(rigaInEsame, ".", "")
          indInizio = InStr(rigaInEsame, "(")
          indFine = InStr(rigaInEsame, ")")
          appoString = ""
          If indInizio > 0 Then
            appoString = tag & indTab & Mid(rigaInEsame, 1, indInizio - 1) & "AC" & Trim(Mid(rigaInEsame, indInizio, 14))
          Else
            appoString = tag & indTab & Trim(rigaInEsame) & "AC"
          End If
          indTab = InsSpace(InStr(rigaInEsame, " TO ") - 8)
          If InStr(rigaInEsame, " AUTOSKIP ") > 0 Then
            appoString = Replace(appoString, "AUTOSKIP", "DFHBMASK")
          End If
          If InStr(rigaInEsame, " NON-DISPLAY ") > 0 Then
            appoString = Replace(appoString, "NON-DISPLAY", "DFHBMDAR   ")
          End If
          If InStr(rigaInEsame, " DATA-ENTRY ") > 0 Then
            appoString = Replace(appoString, "DATA-ENTRY", "DFHBMUNP  ")
          End If
          rigaInEsame = tagc & rigaInEsame & vbCrLf & Mid(appoString, 8, 70) & Spunto
          appoString = ""
          Count = 1
          Do While InStr(newPgm(riga + Count), " GO ") = 0 And _
                   InStr(newPgm(riga + Count), " MOVE ") = 0 And InStr(newPgm(riga + Count), " IF ") = 0 And _
                   InStr(newPgm(riga + Count), " ELSE") = 0 And _
                   InStr(newPgm(riga + Count), "EXIT") = 0 And Len(Trim(Mid(newPgm(riga + Count), 8, 63))) > 0
            If Mid(newPgm(riga + Count), 7, 1) <> "*" Then
              If InStr(newPgm(riga + Count), ".") > 0 Then
                newPgm(riga + Count) = Replace(newPgm(riga + Count), ".", "")
                Spunto = "."
              Else
                Spunto = ""
              End If
              indInizio = InStr(newPgm(riga + Count), "(")
              indFine = InStr(newPgm(riga + Count), ")")
              If indInizio > 0 Then
                appoString = Mid(newPgm(riga + Count), 1, indInizio - 1) & "AC" & Trim(Mid(newPgm(riga + Count), indInizio, 14))
              Else
                appoString = RTrim(newPgm(riga + Count)) & "AC"
              End If
              newPgm(riga + Count) = tag & Mid(appoString, 8, 63) & Spunto
            End If
            Count = Count + 1
          Loop
        End If
        
      End If
      newPgm(riga) = rigaInEsame
    Else
      riga = riga
    End If ' riga commentata
    riga = riga + 1
  Loop
  'rsObj.Close
  ' INSERISCO COPY PROCEDURE PER GESTIONE FILE STATU, sembra non servire
  ReDim Preserve newPgm(riga)
  newPgm(riga) = newPgm(riga) & perfDescending
  ReDim Preserve newPgm(riga)
  newPgm(riga) = newPgm(riga) & vbCrLf & "ITER       COPY WKMANAGE." & vbCrLf
  ConvertiPgm = newPgm
  
End Function

Private Function InsSpace(numeroSpace As Integer) As String
  Dim a As Integer
  Dim spazi As String
  
  spazi = ""
  For a = 1 To numeroSpace
    spazi = spazi & " "
  Next a
  InsSpace = spazi
End Function

Public Sub AggLog(stringa, log As RichTextBox)
  Dim vPos As Variant
  '''vPos = Len(log.Text)
  log.SelLength = 0
  log.SelText = Format(Now, "HH:MM:SS") & " - " & stringa & vbCrLf
  log.Refresh
End Sub

Private Function ValueCopyMap(rsObj As Recordset, fileInput As String) As String()
  Dim numFile As Integer
  Dim lineIn As String
  Dim fileOut() As String
  Dim i As Long, punto As Long
  
  ReDim fileOut(0)
  i = 0
  numFile = FreeFile
  Open fileInput For Input As numFile
  Line Input #numFile, lineIn
  i = i + 1
  Do Until EOF(numFile)
    rsObj.MoveFirst
    Do Until rsObj.EOF
      If InStr(lineIn, rsObj!nomeinbms & "I") Then
        punto = InStr(lineIn & ".", ".")
        lineIn = Left(lineIn, punto - 1) & " VALUE " & rsObj!Value & "."
        Exit Do
      End If
      rsObj.MoveNext
    Loop
    Do While Len(lineIn) > 72
      ReDim Preserve fileOut(i)
      fileOut(i) = Left(lineIn, 72)
      i = i + 1
      lineIn = Space(6) & "-" & Space(8) & "'" & Right(lineIn, Len(lineIn) - 72)
    Loop
    ReDim Preserve fileOut(i)
    fileOut(i) = lineIn
    Line Input #numFile, lineIn
    i = i + 1
  Loop
  Close #numFile
  ValueCopyMap = fileOut
End Function
