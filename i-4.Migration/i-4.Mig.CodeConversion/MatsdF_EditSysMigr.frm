VERSION 5.00
Begin VB.Form MatsdF_EditSysMigr 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Edit Regular Expression"
   ClientHeight    =   5925
   ClientLeft      =   60
   ClientTop       =   330
   ClientWidth     =   9660
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5925
   ScaleWidth      =   9660
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   8640
      Picture         =   "MatsdF_EditSysMigr.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   5040
      Width           =   825
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Confirm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   7680
      Picture         =   "MatsdF_EditSysMigr.frx":1D2A
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   5040
      Width           =   825
   End
   Begin VB.Frame Frame1 
      Height          =   4695
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   9375
      Begin VB.CheckBox ChkActiveMacro 
         Caption         =   "Active"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   7200
         TabIndex        =   22
         Top             =   4200
         Value           =   1  'Checked
         Width           =   855
      End
      Begin VB.TextBox txtFunction 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   315
         Left            =   3720
         MaxLength       =   50
         TabIndex        =   2
         Top             =   240
         Width           =   5415
      End
      Begin VB.TextBox txtdescription 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   315
         Left            =   1080
         MaxLength       =   250
         TabIndex        =   3
         Top             =   840
         Width           =   8055
      End
      Begin VB.ComboBox cmbType 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1560
         Width           =   1455
      End
      Begin VB.TextBox txtWeight 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   315
         Left            =   3720
         MaxLength       =   5
         TabIndex        =   5
         Top             =   1560
         Width           =   855
      End
      Begin VB.TextBox txtWord1 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   315
         Left            =   1080
         MaxLength       =   250
         TabIndex        =   6
         Top             =   2280
         Width           =   7935
      End
      Begin VB.ComboBox cmbBol 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   315
         ItemData        =   "MatsdF_EditSysMigr.frx":1E74
         Left            =   1080
         List            =   "MatsdF_EditSysMigr.frx":1E7E
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   2880
         Width           =   975
      End
      Begin VB.TextBox txtWord2 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   315
         Left            =   1080
         MaxLength       =   250
         TabIndex        =   8
         Top             =   3480
         Width           =   7935
      End
      Begin VB.ComboBox cmbTemplate 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   4200
         Width           =   5175
      End
      Begin VB.ComboBox cmbtypeSql 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   315
         Left            =   1080
         TabIndex        =   1
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Lbl10 
         Height          =   255
         Left            =   6720
         TabIndex        =   21
         Top             =   4200
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Function:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2760
         TabIndex        =   20
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Description:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Type:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Weight:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2760
         TabIndex        =   17
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label Label5 
         Caption         =   "Word1:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   2400
         Width           =   855
      End
      Begin VB.Label Label6 
         Caption         =   "Boolean:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   3000
         Width           =   855
      End
      Begin VB.Label Label7 
         Caption         =   "Word2:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   3600
         Width           =   855
      End
      Begin VB.Label Label8 
         Caption         =   "Template:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   4320
         Width           =   855
      End
      Begin VB.Label Label9 
         Caption         =   "Type Sql:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   855
      End
   End
End
Attribute VB_Name = "MatsdF_EditSysMigr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private UpdateSys As Boolean
Public index As Long

Private Sub cmdExit_Click()
  Unload Me
End Sub

Private Sub cmdOk_Click()
  Dim i As Long
  Dim rs As Recordset
  Dim query As String
  Dim risp As String
  
  On Error GoTo ErrorHandler
  Screen.MousePointer = vbHourglass
  
  If Trim(cmbType.text) = "" Then
    MsgBox "Insert value!", vbOKOnly + vbInformation, Me.Caption
    cmbType.SetFocus
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
  If Trim(cmbtypeSql.text) = "" Then
    MsgBox "Insert value!", vbOKOnly + vbInformation, Me.Caption
    cmbtypeSql.SetFocus
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
  If Trim(txtFunction.text) = "" Then
    MsgBox "Insert value!", vbOKOnly + vbInformation, Me.Caption
    txtFunction.SetFocus
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
  
  If UpdateSys Then
  ' EDIT
    Set rs = m_fun.Open_Recordset("select * from TL_Migr where " & _
                                  "TypeSql = '" & cmbtypeSql.text & "' and " & _
                                  "Type = '" & cmbType.text & "' and " & _
                                  "Operazione = '" & txtFunction.text & "'")
    If rs.RecordCount Then
      risp = MsgBox("This Regular Expression already exists, do you want to overwrite?", vbYesNoCancel + vbQuestion)
      If risp = vbYes Then
        rs!Description = txtdescription.text
        rs!word1 = txtWord1.text
        rs!word2 = txtWord2.text
        rs!Weight = txtWeight.text
        rs!Boolean = cmbBol.text
        rs!template = "@EXECUTEMACRO(" & cmbTemplate.text & ")"
        rs!Apply = ChkActiveMacro.Value
       
        rs.Update
      ElseIf risp = vbCancel Then
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
    Else
      rs.AddNew
      rs!Type = cmbType.text
      rs!Typesql = cmbtypeSql.text
      rs!operazione = Trim(txtFunction.text)
      rs!Description = txtdescription.text
      rs!word1 = txtWord1.text
      rs!word2 = txtWord2.text
      rs!Weight = txtWeight.text
      rs!Boolean = cmbBol.text
      rs!template = "@EXECUTEMACRO(" & cmbTemplate.text & ")"
      rs!Apply = ChkActiveMacro.Value
      rs.Update
      rs.Close
      
      MsgBox "New Regular Expression inserted correctly!", vbOKOnly + vbInformation
      
      Unload Me
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
    rs.Close
    Unload Me
  Else
    ' ADD
    Set rs = m_fun.Open_Recordset("select * from TL_Migr where " & _
                                  "TypeSql = '" & cmbtypeSql.text & "' and " & _
                                  "Type = '" & cmbType.text & "' and " & _
                                  "Operazione = '" & txtFunction.text & "'")
    If rs.RecordCount Then
      risp = MsgBox("This Regular Expression already exists, do you want to overwrite?", vbYesNoCancel + vbQuestion)
      If risp = vbYes Then
        rs!Description = txtdescription.text
        rs!word1 = txtWord1.text
        rs!word2 = txtWord2.text
        rs!Weight = txtWeight.text
        rs!Boolean = cmbBol.text
        rs!template = "@EXECUTEMACRO(" & cmbTemplate.text & ")"
        rs!Apply = ChkActiveMacro.Value
        rs.Update
      ElseIf risp = vbCancel Then
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
    Else
      rs.AddNew
      rs!Type = cmbType.text
      rs!Typesql = cmbtypeSql.text
      rs!operazione = Trim(txtFunction.text)
      rs!Description = txtdescription.text
      rs!word1 = txtWord1.text
      rs!word2 = txtWord2.text
      rs!Weight = txtWeight.text
      rs!Boolean = cmbBol.text
      rs!template = "@EXECUTEMACRO(" & cmbTemplate.text & ")"
      rs!Apply = ChkActiveMacro.Value
      rs.Update
      rs.Close
      
      MsgBox "New Regular Expression inserted correctly!", vbOKOnly + vbInformation
      Unload Me
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
    rs.Close
  End If
  Screen.MousePointer = vbDefault
  Unload Me
  Exit Sub
ErrorHandler:
  MsgBox Err.Number & " - " & Err.Description
End Sub

Sub UpdateSysMigr()
  txtFunction = MatsdF_RegExp.lswSysMigr.ListItems(1).text
End Sub

Sub LoadCombo()
  Dim rs As Recordset
  cmbType.AddItem "MTP"
  cmbType.AddItem "SQL"
  cmbType.AddItem "JCL"
  cmbType.AddItem "JOB"
  
  Set rs = m_fun.Open_Recordset("select distinct TypeSql from TL_Migr ")
  If rs.RecordCount > 0 Then
    While Not rs.EOF
      cmbtypeSql.AddItem rs!Typesql
      rs.MoveNext
    Wend
  End If
  rs.Close
  
  Set rs = m_fun.Open_Recordset_Sys("select Nome from SysMacro order by Nome ")
  If rs.RecordCount > 0 Then
    While Not rs.EOF
      cmbTemplate.AddItem Trim(rs!Nome)
      rs.MoveNext
    Wend
  End If
  rs.Close
  
End Sub

Public Sub editInstr(Item As ListItem, wType As String, weightS As Integer, wtypeSql As String, wtemplate As String)
  Dim i As Integer
  LoadCombo
  UpdateSys = True
  index = Item.index
  txtFunction = Item.text
  txtdescription = Item.ListSubItems(2).text
  txtWord1 = Item.ListSubItems(3).text
  For i = 0 To cmbBol.ListCount - 1
    If cmbBol.List(i) = Item.ListSubItems(4).text Then
      cmbBol.text = cmbBol.List(i)
      Exit For
    End If
  Next i
  txtWord2 = Item.ListSubItems(5).text
  cmbtypeSql.text = wtypeSql
  txtWeight = weightS
  For i = 0 To cmbTemplate.ListCount - 1
    If cmbTemplate.List(i) = wtemplate Then
      cmbTemplate.text = cmbTemplate.List(i)
      Exit For
    End If
  Next i
  For i = 0 To cmbType.ListCount - 1
    If cmbType.List(i) = wType Then
      cmbType.text = cmbType.List(i)
      Exit For
    End If
  Next i
  For i = 0 To cmbtypeSql.ListCount - 1
    If cmbtypeSql.List(i) = wtypeSql Then
      cmbtypeSql.text = cmbtypeSql.List(i)
      Exit For
    End If
  Next i
  If Item.ListSubItems(1).text = "YES" Then
    ChkActiveMacro.Value = 1
  Else
    ChkActiveMacro.Value = 0
  End If
  Me.Caption = "Edit Regular Expression"
  Me.Show
End Sub

Public Sub addInstr()
  UpdateSys = False
  LoadCombo
  txtFunction = ""
  txtdescription = ""
  txtWord1 = ""
  'cmbBol.Text = ""
  txtWord2 = ""
  'CmbType.Text = ""
  txtWeight = ""
  Me.Caption = "Add Regular Expression"
  Me.Show '
End Sub

