VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form Matsdc_Sas2Java 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "SAS Conversion"
   ClientHeight    =   6960
   ClientLeft      =   150
   ClientTop       =   420
   ClientWidth     =   8160
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6960
   ScaleWidth      =   8160
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin RichTextLib.RichTextBox rtbox 
      Height          =   30
      Left            =   2640
      TabIndex        =   15
      Top             =   240
      Width           =   30
      _ExtentX        =   53
      _ExtentY        =   53
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"Matsdc_Sas2Java.frx":0000
   End
   Begin VB.Frame Frame3 
      Caption         =   "Log"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1395
      Left            =   3960
      TabIndex        =   9
      Top             =   1680
      Width           =   4125
      Begin MSComctlLib.ListView LswLog 
         Height          =   1035
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   1826
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Log"
            Object.Width           =   26458
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selection"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1000
      Left            =   30
      TabIndex        =   6
      Top             =   690
      Width           =   3885
      Begin VB.OptionButton OptAll 
         Caption         =   "All SAS Files"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   270
         Width           =   2655
      End
      Begin VB.OptionButton OptSel 
         Caption         =   "Only Selected SAS File"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   600
         Value           =   -1  'True
         Width           =   2655
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Convert to"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1000
      Left            =   3990
      TabIndex        =   3
      Top             =   690
      Width           =   1695
      Begin VB.OptionButton optCobol 
         Caption         =   "Cobol"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   300
         Width           =   915
      End
      Begin VB.OptionButton optJava 
         Caption         =   "Java"
         Enabled         =   0   'False
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Value           =   -1  'True
         Width           =   675
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1000
      Left            =   5700
      TabIndex        =   2
      Top             =   690
      Width           =   2415
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBarK 
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   480
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBar01 
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   720
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "SAS Selected"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   5175
      Left            =   60
      TabIndex        =   1
      Top             =   1680
      Width           =   3885
      Begin MSComctlLib.ListView lswFileSel 
         Height          =   4755
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   3645
         _ExtentX        =   6429
         _ExtentY        =   8387
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Program"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tipo"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Relazione"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Locs"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Directory"
            Object.Width           =   2470
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Notes"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame Frame7 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   3795
      Left            =   3960
      TabIndex        =   0
      Top             =   3060
      Width           =   4125
      Begin MSComctlLib.ListView LswSegnalazioni 
         Height          =   3495
         Left            =   120
         TabIndex        =   11
         Top             =   120
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   6165
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Log"
            Object.Width           =   26458
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2400
      Top             =   4560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Matsdc_Sas2Java.frx":008B
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Matsdc_Sas2Java.frx":03A5
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Matsdc_Sas2Java.frx":07F7
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Matsdc_Sas2Java.frx":0C49
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Matsdc_Sas2Java.frx":2D83
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Matsdc_Sas2Java.frx":2EDD
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   8160
      _ExtentX        =   14393
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Convert"
            Object.ToolTipText     =   "Conversion Sas to Java"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Param"
            Object.ToolTipText     =   "Params"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DataLoader"
            Object.ToolTipText     =   "Data Loader"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ScriptSql"
            Object.ToolTipText     =   "Insert Script "
            ImageIndex      =   1
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SqlInsert"
                  Object.Tag             =   "SQLINS"
                  Text            =   "Script Insert"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SqlDelete"
                  Object.Tag             =   "SQLDEL"
                  Text            =   "Script Delete"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "CreateXml"
            Object.ToolTipText     =   "Create XML"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "HIBERNATE"
            ImageIndex      =   2
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "HibernateMap"
                  Object.Tag             =   "HBMMAP"
                  Text            =   "Hibernate Map"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "HibernateCfg"
                  Object.Tag             =   "HBMCFG"
                  Text            =   "Hibernate Cfg"
               EndProperty
            EndProperty
         EndProperty
      EndProperty
   End
   Begin VB.Menu popup 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu Register 
         Caption         =   "Register Association"
      End
   End
End
Attribute VB_Name = "Matsdc_Sas2Java"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'silvia 12/9/2008
Dim IntLine As Integer
Dim ListObjCol As Long
Dim dialogFileName As String
Dim dialogFileTitle As String

Private Sub Conversion()
  Dim i As Long
  
  LswLog.ListItems.Clear
  LswSegnalazioni.ListItems.Clear
  PBar.Max = 10
  PBar.Value = 0
  PBarK.Max = 10
  PBarK.Value = 0
  
  DoEvents

  LswLog.ListItems.Add , , Time & " - Starting Analysis"
  LswLog.Refresh
    
  If OptAll.Value Then
    If lswFileSel.ListItems.Count > 0 Then
      PBar01.Max = lswFileSel.ListItems.Count
      For i = 1 To lswFileSel.ListItems.Count
        LswLog.ListItems.Add , , Time & " - Source Migration: " & lswFileSel.ListItems(i)
        LswLog.Refresh
        LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible

        If optJava.Value Then
          Set TsCollectionParam = New Collection
          TsCollectionParam.Add lswFileSel.ListItems(i).tag
          TsCollectionParam.Add "JAVA"
          'TsCollectionParam.Add LswSegnalazioni
          'silvia 07-10-2008: passo il TIPO per scegliere se � un PGM o una COPY
          TsCollectionParam.Add lswFileSel.ListItems(i).ListSubItems.Item(2)
          m_pars.ConvertSAS TsCollectionParam
        Else
          'Seconda opzione
        End If
        'load_PLI_Segnalazioni lswFileSel.ListItems(i).tag
        PBar01.Value = PBar01.Value + 1
     Next i
    End If
  Else 'converte solo l'elemento selezionato
    PBar01.Max = lswFileSel.ListItems.Count
    For i = 1 To lswFileSel.ListItems.Count
      If lswFileSel.ListItems(i).Selected Then
        LswLog.ListItems.Add , , Time & " - Source Migration: " & lswFileSel.ListItems(i)
        LswLog.Refresh
        LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
        
        If optJava.Value Then
          ' Mauro 25/08/2008
          Set TsCollectionParam = New Collection
          TsCollectionParam.Add lswFileSel.ListItems(i).tag
          TsCollectionParam.Add "JAVA"
          ''silvia 11/9/2008
          'TsCollectionParam.Add LswLog
          TsCollectionParam.Add LswSegnalazioni
          'silvia 07-10-2008: passo il TIPO per scegliere se � un PGM o una COPY
          TsCollectionParam.Add lswFileSel.ListItems(i).ListSubItems.Item(2)
          ''
          m_pars.ConvertSAS TsCollectionParam
        Else
           'Seconda opzione
        End If
        'load_PLI_Segnalazioni lswFileSel.ListItems(i).tag
      End If
      PBar01.Value = PBar01.Value + 1
    Next i
  End If
  
  PBar01.Value = PBar01.Max
  
  LswLog.ListItems.Add , , Time & " - Source Migration Terminated"
  LswLog.Refresh
  LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
    
  'silvia 11/9/2008
  LswSegnalazioni.Refresh
  If LswSegnalazioni.ListItems.Count Then
    LswSegnalazioni.ListItems(LswSegnalazioni.ListItems.Count).EnsureVisible
  End If
  PBar.Value = 0
  PBarK.Value = 0
  PBar01.Value = 0
End Sub

Private Sub CmdConvert_Click()
  MousePointer = vbHourglass
  If lswFileSel.ListItems.Count > 0 Then Conversion
  MousePointer = vbNormal
End Sub

Private Sub CmdCrea_XML_Click()
'TILVIA X CAPITA
   m_pars.Crea_xml
End Sub

'tilvia dataloader x sas
Private Sub DataLoader()
  Dim i As Long
  
  LswLog.ListItems.Clear
  LswSegnalazioni.ListItems.Clear
  PBar.Max = 10
  PBar.Value = 0
  PBarK.Max = 10
  PBarK.Value = 0
  
  DoEvents

    
  If OptAll.Value Then
    If lswFileSel.ListItems.Count > 0 Then
      PBar01.Max = lswFileSel.ListItems.Count
      For i = 1 To lswFileSel.ListItems.Count
         m_pars.SasDataLoader lswFileSel.ListItems(i).tag
        PBar01.Value = PBar01.Value + 1
     Next i
    End If
  Else 'converte solo l'elemento selezionato
    PBar01.Max = lswFileSel.ListItems.Count
    For i = 1 To lswFileSel.ListItems.Count
      If lswFileSel.ListItems(i).Selected Then
          m_pars.SasDataLoader lswFileSel.ListItems(i).tag
      End If
      PBar01.Value = PBar01.Value + 1
    Next i
  End If
  
  PBar01.Value = PBar01.Max
  
  PBar.Value = 0
  PBarK.Value = 0
  PBar01.Value = 0

End Sub







Private Sub Param()
  Dim i As Long
  
  LswLog.ListItems.Clear
  LswSegnalazioni.ListItems.Clear
  PBar.Max = 10
  PBar.Value = 0
  PBarK.Max = 10
  PBarK.Value = 0
  
  DoEvents

  LswLog.ListItems.Add , , Time & " - Starting Analysis"
  LswLog.Refresh
    
  If OptAll.Value Then
    If lswFileSel.ListItems.Count > 0 Then
      PBar01.Max = lswFileSel.ListItems.Count
      For i = 1 To lswFileSel.ListItems.Count
        LswLog.ListItems.Add , , Time & " - Source Migration: " & lswFileSel.ListItems(i)
        LswLog.Refresh
        LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible

        If optJava.Value Then
          Set TsCollectionParam = New Collection
          TsCollectionParam.Add lswFileSel.ListItems(i).tag
          'TsCollectionParam.Add "JAVA"
          'TsCollectionParam.Add lswFileSel.ListItems(i).ListSubItems.Item(2)
          m_pars.SasParam TsCollectionParam
        Else
          'Seconda opzione
        End If
        PBar01.Value = PBar01.Value + 1
     Next i
    End If
  Else 'converte solo l'elemento selezionato
    PBar01.Max = lswFileSel.ListItems.Count
    For i = 1 To lswFileSel.ListItems.Count
      If lswFileSel.ListItems(i).Selected Then
        LswLog.ListItems.Add , , Time & " - Source Migration: " & lswFileSel.ListItems(i)
        LswLog.Refresh
        LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
        
        If optJava.Value Then
          Set TsCollectionParam = New Collection
          TsCollectionParam.Add lswFileSel.ListItems(i).tag
          'TsCollectionParam.Add "JAVA"
          'TsCollectionParam.Add LswSegnalazioni
          'TsCollectionParam.Add lswFileSel.ListItems(i).ListSubItems.Item(2)
          ''
          m_pars.SasParam TsCollectionParam
        Else
        End If
      End If
      PBar01.Value = PBar01.Value + 1
    Next i
  End If
  
  PBar01.Value = PBar01.Max
  
  LswLog.ListItems.Add , , Time & " - Add Param to Source Terminated"
  LswLog.Refresh
  LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
    
  'silvia 11/9/2008
  LswSegnalazioni.Refresh
  If LswSegnalazioni.ListItems.Count Then
    LswSegnalazioni.ListItems(LswSegnalazioni.ListItems.Count).EnsureVisible
  End If
  PBar.Value = 0
  PBarK.Value = 0
  PBar01.Value = 0

End Sub



Private Sub Form_Load()
  'lswFileSel.ColumnHeaders(1).Width = lswFileSel.Width / 4
  'lswFileSel.ColumnHeaders(5).Width = (lswFileSel.Width / 4) * 3

  Carica_Lista_SAS
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Public Sub Carica_Lista_SAS()
  Dim rs As Recordset
  Dim listx As ListItem
  
  lswFileSel.ListItems.Clear
  Set rs = m_fun.Open_Recordset("Select DISTINCT IdOggetto,Nome,Tipo,NumRighe,directory_input,'xxx' as Relazione, Notes From BS_Oggetti Where Tipo IN ( 'SAS' , 'SAL') Order By Nome")
  While Not rs.EOF
    Set listx = lswFileSel.ListItems.Add(, , rs!Nome)
    listx.ListSubItems.Add = rs!Tipo
    listx.ListSubItems.Add = rs!Relazione
    listx.ListSubItems.Add = rs!NumRighe
    listx.ListSubItems.Add = rs!directory_input
    listx.ListSubItems.Add = IIf(IsNull(rs!Notes) = True, "", rs!Notes)
    listx.tag = rs!IdOggetto
    rs.MoveNext
  Wend
  rs.Close
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  'lswFileSel.ColumnHeaders(1).Width = lswFileSel.Width / 4
  'lswFileSel.ColumnHeaders(2).Width = (lswFileSel.Width / 4) * 3
End Sub

Private Sub lswFileSel_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String

  Order = lswFileSel.SortOrder
  key = ColumnHeader.index

  ListObjCol = key

  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If

  lswFileSel.SortOrder = Order
  lswFileSel.SortKey = key - 1
  lswFileSel.Sorted = True
End Sub

Private Sub lswFileSel_DblClick()
  If lswFileSel.ListItems.Count Then
    Matsdc_File_Load.load_File lswFileSel.SelectedItem, lswFileSel.SelectedItem.tag
  End If
End Sub



Private Sub LswSegnalazioni_DblClick()
  If LswSegnalazioni.ListItems.Count Then
    Matsdc_File_Load.load_File lswFileSel.SelectedItem, lswFileSel.SelectedItem.tag, IntLine
  End If
End Sub

Private Sub LswSegnalazioni_ItemClick(ByVal Item As MSComctlLib.ListItem)
  Dim strItem() As String
  
  strItem = Split(Item, "]")
  IntLine = Int(Replace(strItem(2), "[", ""))
End Sub

Private Sub LswSegnalazioni_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    If InStr(LswSegnalazioni.SelectedItem.text, "Insert the association:") Then
      Register.Enabled = True
    Else
      Register.Enabled = False
    End If
    PopupMenu Me.popup
  End If
End Sub


Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case UCase(Trim(Button.key))
     Case "CONVERT"
       MousePointer = vbHourglass
       If lswFileSel.ListItems.Count > 0 Then Conversion
       MousePointer = vbNormal
     Case "PARAM"
       Param
     Case "DATALOADER"
       DataLoader
     'Case "SCRIPTSQL"
     Case "CREATEXML"
       m_pars.Crea_xml
     Case "HIBERNATE"
   End Select
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  
  ''ButtonMenu.tag ,  ButtonMenu.key
  Select Case ButtonMenu.tag
    Case "HBMMAP"
      m_pars.Crea_Hibernatemap
    Case "HBMCFG"
      m_pars.Crea_HibernateCfg
    Case "SQLINS"
      m_pars.Crea_ScriptSql "SQLINS"
    Case "SQLDEL"
      m_pars.Crea_ScriptSql "SQLDEL"
  End Select
End Sub
