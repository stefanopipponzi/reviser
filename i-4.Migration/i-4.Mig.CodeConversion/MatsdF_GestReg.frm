VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MatsdF_GestReg 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Table Registers Management"
   ClientHeight    =   8535
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   7530
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8535
   ScaleWidth      =   7530
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbregister 
      BackColor       =   &H80000018&
      Height          =   315
      Index           =   0
      ItemData        =   "MatsdF_GestReg.frx":0000
      Left            =   1200
      List            =   "MatsdF_GestReg.frx":0034
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   6960
      Width           =   2085
   End
   Begin VB.TextBox txtnomepgm 
      BackColor       =   &H80000018&
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   1200
      MaxLength       =   250
      TabIndex        =   10
      Top             =   6000
      Width           =   2895
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "Exit"
      Height          =   405
      Left            =   6240
      Picture         =   "MatsdF_GestReg.frx":006E
      TabIndex        =   5
      Top             =   8040
      Width           =   825
   End
   Begin VB.TextBox txtfield 
      BackColor       =   &H80000018&
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   1200
      MaxLength       =   250
      TabIndex        =   1
      Top             =   7440
      Width           =   4575
   End
   Begin VB.TextBox txtparagraph 
      BackColor       =   &H80000018&
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   1200
      MaxLength       =   250
      TabIndex        =   0
      Top             =   6480
      Width           =   2895
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete"
      Enabled         =   0   'False
      Height          =   405
      Left            =   2520
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   8040
      Width           =   885
   End
   Begin VB.CommandButton cmdInsert 
      Caption         =   "Insert"
      Height          =   405
      Left            =   1200
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   8040
      Width           =   1005
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "Update"
      Enabled         =   0   'False
      Height          =   405
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   8040
      Width           =   915
   End
   Begin MSComctlLib.ListView lswregister 
      Height          =   5775
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   7305
      _ExtentX        =   12885
      _ExtentY        =   10186
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Source"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Paragraph"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "register"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "field"
         Object.Width           =   5292
      EndProperty
   End
   Begin VB.Label Label4 
      Caption         =   "Source:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   6120
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Field:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   7560
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Register:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   7080
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Paragraph:"
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   6600
      Width           =   855
   End
End
Attribute VB_Name = "MatsdF_GestReg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdDelete_Click()
  Dim i As Long
  Dim risp As Integer
  Dim rs As Recordset
  Dim stquery As String
  On Error GoTo ErrorHandler
  
  stquery = "SELECT * FROM MgASM_Registers order by nomepgm, paragraph, register"
  risp = MsgBox("Do you want to delete these records?", vbYesNo + vbInformation + vbDefaultButton2, Menu.TsNameProdotto)
  If risp = vbYes Then
    For i = lswregister.ListItems.Count To 1 Step -1
      If lswregister.ListItems(i).Selected Then
        Set rs = m_fun.Open_Recordset("select * from MgASM_Registers where " & _
                                      "paragraph = '" & lswregister.ListItems(i).text & "'" & _
                                      "and register = '" & lswregister.ListItems(i).ListSubItems(1) & "'")
        If rs.RecordCount Then
          rs.Delete
        End If
        rs.Close
        lswregister.ListItems.Remove (i)
      End If
    Next i
    txtnomepgm.text = ""
    txtparagraph.text = ""
    cmbregister.Item(0).text = " "
    txtfield.text = ""
  End If
  loadTabregister stquery
  Exit Sub
ErrorHandler:
  MsgBox Err.Number & " " & Err.Description, vbExclamation + vbOKOnly, Menu.TsNameProdotto
End Sub

Private Sub cmdExit_Click()
  Unload Me
End Sub

Private Sub cmdInsert_Click()
  Dim stquery As String
  On Error GoTo ErrorHandler
  
  stquery = "SELECT * FROM MgASM_Registers order by nomepgm, paragraph, register"
  If txtparagraph.text <> "" And cmbregister.Item(0).text <> "" And txtnomepgm.text <> "" Then
    InsReg txtnomepgm.text, txtparagraph.text, cmbregister.Item(0).text, True
    loadTabregister stquery
  End If
  Exit Sub
ErrorHandler:
  MsgBox Err.Number & " " & Err.Description, vbExclamation + vbOKOnly, Menu.TsNameProdotto
End Sub

Private Sub cmdUpdate_Click()
  Dim stquery As String
  On Error GoTo ErrorHandler
  
  stquery = "SELECT * FROM MgASM_Registers order by nomepgm, paragraph, register"
  If Not lswregister.SelectedItem Is Nothing And _
     txtparagraph.text <> "" And txtnomepgm.text <> "" And cmbregister.Item(0).text <> "" Then
    InsReg lswregister.ListItems(lswregister.SelectedItem.index), lswregister.ListItems(lswregister.SelectedItem.index).SubItems(1), lswregister.ListItems(lswregister.SelectedItem.index).SubItems(2), False
    loadTabregister stquery
  End If
  Exit Sub
ErrorHandler:
  MsgBox Err.Number & " " & Err.Description, vbExclamation + vbOKOnly, Menu.TsNameProdotto
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub lswregister_ItemClick(ByVal Item As MSComctlLib.ListItem)
  Dim i As Integer
  
  If Not lswregister.SelectedItem Is Nothing Then
    cmdDelete.Enabled = True
    cmdUpdate.Enabled = True
    txtnomepgm.text = Item.text
    txtparagraph.text = lswregister.ListItems(lswregister.SelectedItem.index).SubItems(1)
    cmbregister.Item(0).text = lswregister.ListItems(lswregister.SelectedItem.index).SubItems(2)
    txtfield.text = lswregister.ListItems(lswregister.SelectedItem.index).SubItems(3)
  Else
    cmdDelete.Enabled = False
    cmdUpdate.Enabled = False
  End If
End Sub

Public Sub loadTabregister(stquery As String, Optional stnomepgm As String, Optional stparagraph As String, Optional stregister As String)
  Dim tb As Recordset
  Dim k As Integer
  On Error GoTo ErrorHandler
  
  lswregister.ListItems.Clear
  Set tb = m_fun.Open_Recordset(stquery)
  If tb.RecordCount Then
    While Not tb.EOF
      lswregister.ListItems.Add , , "" & tb!nomepgm
      lswregister.ListItems(k + 1).ListSubItems.Add , , Trim("" & tb!paragraph)
      lswregister.ListItems(k + 1).ListSubItems.Add , , Trim("" & tb!Register)
      lswregister.ListItems(k + 1).ListSubItems.Add , , Trim("" & tb!Field)
      tb.MoveNext
      k = k + 1
    Wend
  End If
  tb.Close
  If stnomepgm <> "" And stparagraph <> "" Then
    txtnomepgm.text = stnomepgm
    txtparagraph.text = stparagraph
    cmbregister.Item(0).text = stregister
  End If
  Me.Show
  Me.txtparagraph.SetFocus
  Exit Sub
ErrorHandler:
  MsgBox Err.Number & " " & Err.Description, vbExclamation + vbOKOnly, Menu.TsNameProdotto
End Sub

Sub InsReg(stnomepgm As String, stparagraph As String, stregister As String, isNew As Boolean)
  Dim rs As Recordset
  Dim risp As String
  On Error GoTo ErrorHandler
  
  Set rs = m_fun.Open_Recordset("select * from MgASM_Registers where " & _
                                "nomepgm = '" & stnomepgm & "' and " & _
                                "paragraph = '" & stparagraph & "' and " & _
                                "register = '" & stregister & "'")
  If rs.RecordCount Then
    If isNew Then
      risp = MsgBox("This Record already exists, do you want to overwrite?", vbYesNoCancel + vbQuestion, Menu.TsNameProdotto)
    Else
      risp = MsgBox("Do you want update this record?", vbYesNoCancel + vbQuestion, Menu.TsNameProdotto)
    End If
    If risp = vbYes Then
      rs!nomepgm = txtnomepgm.text
      rs!paragraph = txtparagraph.text
      rs!Register = cmbregister.Item(0).text
      rs!Field = txtfield.text
      rs.Update
    ElseIf risp = vbCancel Then
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
  Else
    rs.AddNew
    rs!nomepgm = txtnomepgm.text
    rs!paragraph = txtparagraph.text
    rs!Register = cmbregister.Item(0).text
    rs!Field = txtfield.text
    rs.Update
    rs.Close
    
    MsgBox "New Record inserted correctly!", vbOKOnly + vbInformation, Menu.TsNameProdotto
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
  rs.Close
  Screen.MousePointer = vbDefault
  Exit Sub
ErrorHandler:
  MsgBox Err.Number & " " & Err.Description, vbExclamation + vbOKOnly, Menu.TsNameProdotto
End Sub
