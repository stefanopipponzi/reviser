VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MatsdF_StructDetails 
   Appearance      =   0  'Flat
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Files Tools"
   ClientHeight    =   6285
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9855
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   6285
   ScaleWidth      =   9855
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtByte 
      Height          =   375
      Left            =   540
      TabIndex        =   25
      Text            =   "Text1"
      Top             =   6600
      Width           =   795
   End
   Begin VB.CheckBox chkBlankZero 
      Caption         =   "Replace Blank with Zero"
      ForeColor       =   &H8000000D&
      Height          =   375
      Left            =   7680
      TabIndex        =   18
      Top             =   120
      Width           =   2055
   End
   Begin VB.Frame Frame7 
      Caption         =   "Field's Details"
      ForeColor       =   &H8000000D&
      Height          =   1680
      Left            =   120
      TabIndex        =   0
      Top             =   4560
      Width           =   9645
      Begin VB.ComboBox cmbField 
         BackColor       =   &H00FFFFC0&
         Height          =   315
         ItemData        =   "MatsdF_StructDetails.frx":0000
         Left            =   6240
         List            =   "MatsdF_StructDetails.frx":0002
         TabIndex        =   23
         Top             =   420
         Width           =   1905
      End
      Begin VB.ComboBox cmbOperator 
         BackColor       =   &H00FFFFC0&
         Height          =   315
         ItemData        =   "MatsdF_StructDetails.frx":0004
         Left            =   6240
         List            =   "MatsdF_StructDetails.frx":001D
         TabIndex        =   21
         Top             =   780
         Width           =   1905
      End
      Begin VB.TextBox TxtSelValue 
         BackColor       =   &H00FFFFC0&
         Height          =   345
         Left            =   6240
         TabIndex        =   19
         Top             =   1140
         Width           =   1905
      End
      Begin VB.TextBox TxtLiv 
         BackColor       =   &H00FFFFC0&
         Height          =   315
         Left            =   1200
         TabIndex        =   8
         Top             =   420
         Width           =   495
      End
      Begin VB.TextBox TxtNome 
         BackColor       =   &H00FFFFC0&
         Height          =   345
         Left            =   1200
         TabIndex        =   7
         Top             =   780
         Width           =   2355
      End
      Begin VB.TextBox TxtOccurs 
         BackColor       =   &H00FFFFC0&
         Height          =   345
         Left            =   4320
         TabIndex        =   6
         Top             =   1140
         Width           =   555
      End
      Begin VB.TextBox TxtNomeRed 
         BackColor       =   &H00FFFFC0&
         Height          =   345
         Left            =   1200
         TabIndex        =   5
         Top             =   1140
         Width           =   2325
      End
      Begin VB.ComboBox CmbType 
         BackColor       =   &H00FFFFC0&
         Height          =   315
         ItemData        =   "MatsdF_StructDetails.frx":0066
         Left            =   2160
         List            =   "MatsdF_StructDetails.frx":0079
         TabIndex        =   4
         Top             =   420
         Width           =   1395
      End
      Begin VB.TextBox TxtLen 
         BackColor       =   &H00FFFFC0&
         Height          =   315
         Left            =   4320
         TabIndex        =   3
         Top             =   420
         Width           =   795
      End
      Begin VB.TextBox TxTDec 
         BackColor       =   &H00FFFFC0&
         Height          =   345
         Left            =   4320
         TabIndex        =   2
         Top             =   780
         Width           =   825
      End
      Begin VB.CommandButton CmdFldOk 
         Height          =   345
         Left            =   8280
         Picture         =   "MatsdF_StructDetails.frx":00AA
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   1140
         Width           =   1155
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Field"
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   5400
         TabIndex        =   24
         Top             =   480
         Width           =   720
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Operator"
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   5400
         TabIndex        =   22
         Top             =   840
         Width           =   720
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Selector Value"
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   5055
         TabIndex        =   20
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Level"
         ForeColor       =   &H8000000D&
         Height          =   165
         Index           =   4
         Left            =   660
         TabIndex        =   15
         Top             =   480
         Width           =   465
      End
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         Caption         =   "Name"
         ForeColor       =   &H8000000D&
         Height          =   165
         Left            =   690
         TabIndex        =   14
         Top             =   870
         Width           =   465
      End
      Begin VB.Label Label14 
         Caption         =   "Type"
         ForeColor       =   &H8000000D&
         Height          =   225
         Left            =   1755
         TabIndex        =   13
         Top             =   480
         Width           =   405
      End
      Begin VB.Label Label15 
         Alignment       =   1  'Right Justify
         Caption         =   "Occurs"
         ForeColor       =   &H8000000D&
         Height          =   225
         Left            =   3600
         TabIndex        =   12
         Top             =   1200
         Width           =   675
      End
      Begin VB.Label Label19 
         Alignment       =   1  'Right Justify
         Caption         =   "Redef. Name"
         ForeColor       =   &H8000000D&
         Height          =   225
         Left            =   120
         TabIndex        =   11
         Top             =   1200
         Width           =   1035
      End
      Begin VB.Label Label21 
         Alignment       =   1  'Right Justify
         Caption         =   "Len"
         ForeColor       =   &H8000000D&
         Height          =   225
         Left            =   3780
         TabIndex        =   10
         Top             =   480
         Width           =   465
      End
      Begin VB.Label Label23 
         Alignment       =   1  'Right Justify
         Caption         =   "Dec"
         ForeColor       =   &H8000000D&
         Height          =   225
         Left            =   3780
         TabIndex        =   9
         Top             =   810
         Width           =   465
      End
   End
   Begin MSComctlLib.ListView LstFldEdit 
      Height          =   3945
      Left            =   120
      TabIndex        =   16
      Top             =   540
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   6959
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   14
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Key             =   "Livello"
         Text            =   "Lv."
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Key             =   "Ordinale"
         Text            =   "Ord."
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Key             =   "NCampo"
         Text            =   "Nome campo"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Key             =   "Bytes"
         Text            =   "Bytes"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Key             =   "Lunghezza"
         Text            =   "Len."
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Dec."
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Key             =   "Posizione"
         Text            =   "Start"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Sign"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Key             =   "Tipo"
         Text            =   "Type"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Key             =   "Occurs"
         Text            =   "Occurs"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "Redefines"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "Field"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "Oper."
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Text            =   "Selector"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   4680
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   11
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":0634
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":0746
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":0858
            Key             =   "Paste"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":096A
            Key             =   "Cut"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":0A7C
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":0B8E
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":0CA0
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":0DB2
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":0F0E
            Key             =   "Check"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":136E
            Key             =   "Up"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MatsdF_StructDetails.frx":168A
            Key             =   "Down"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Height          =   390
      Left            =   120
      TabIndex        =   17
      Tag             =   "fixed"
      Top             =   30
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   688
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   11
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "New"
            Object.ToolTipText     =   "New"
            ImageKey        =   "New"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Copy"
            Object.ToolTipText     =   "Copy"
            ImageKey        =   "Copy"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Paste"
            Object.ToolTipText     =   "Paste"
            ImageKey        =   "Paste"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Cut"
            Object.ToolTipText     =   "Cut"
            ImageKey        =   "Cut"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Up"
            Object.ToolTipText     =   "Up"
            ImageKey        =   "Up"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Down"
            Object.ToolTipText     =   "Down"
            ImageKey        =   "Down"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Save"
            Object.ToolTipText     =   "Save"
            ImageKey        =   "Save"
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Check"
            Object.ToolTipText     =   "Check Area"
            ImageKey        =   "Check"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MatsdF_StructDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim SwMod As Boolean
Dim IndSel As Integer
Dim IndSelC As Integer
Dim varIdarchivio As Long
Dim varIdArea As Long
Dim varIdOggetto As Long

Private Sub CmdFldOk_Click()
Dim wLen As Long

  CmdFldOk.Enabled = False
  If IndSel = 0 Or IndSel > LstFldEdit.ListItems.Count Then Exit Sub

  If VerificaSelettore Then
    TxtSelValue.SetFocus
    Exit Sub
  End If
   
  LstFldEdit.ListItems(IndSel).text = TxtLiv
  LstFldEdit.ListItems(IndSel).SubItems(2) = UCase(TxtNome)
  'Byte
  If TxtLen > 0 Then
    wLen = Val(TxtLen) + Val(TxTDec)
    If InStr(cmbType.text, "PCK") > 0 Then
      wLen = Int((wLen) / 2) + 1
    End If
    If InStr(cmbType.text, "BIN") > 0 Then
      wLen = 2
      If TxtLen > 4 Then wLen = 4
      If TxtLen > 8 Then wLen = 8
    End If
    LstFldEdit.ListItems(IndSel).SubItems(3) = wLen
  End If
  LstFldEdit.ListItems(IndSel).SubItems(4) = UCase(TxtLen)
  LstFldEdit.ListItems(IndSel).SubItems(5) = UCase(TxTDec)
  LstFldEdit.ListItems(IndSel).SubItems(8) = UCase(cmbType.text)
  LstFldEdit.ListItems(IndSel).SubItems(9) = UCase(TxtOccurs)
  LstFldEdit.ListItems(IndSel).SubItems(10) = UCase(TxtNomeRed)
  LstFldEdit.ListItems(IndSel).SubItems(11) = UCase(cmbField.text)
  LstFldEdit.ListItems(IndSel).SubItems(12) = ConvertiOperatore(cmbOperator.text)
  LstFldEdit.ListItems(IndSel).SubItems(13) = UCase(TxtSelValue)
  
  TxtLiv = ""
  TxtNome = ""
  TxtLen = ""
  TxTDec = ""
  cmbType = ""
  TxtOccurs = ""
  TxtNomeRed = ""
  cmbField.text = ""
  cmbOperator.text = ""
  TxtSelValue = ""
  
  VerificaArea
  SwMod = True
End Sub

Private Sub Form_Load()
  CaricaArea
  Carica_cmbField
   
  SwMod = False
  CmdFldOk.Enabled = False
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Sub CaricaArea()
  Dim rsArea As Recordset
  Dim Area As Long, Oggetto As Long
  
  LstFldEdit.ListItems.Clear
  
  Oggetto = Left(indexArea, InStr(indexArea, "\") - 1)
  Area = Mid(indexArea, InStr(indexArea, "\") + 1)
  Set rsArea = m_fun.Open_Recordset("select * from MgConvData_Cmp where " & _
                                    "IdOggetto = " & Oggetto & " AND " & _
                                    "IdArea = " & Area & _
                                    " Order by Ordinale")
      
  Do Until rsArea.EOF
    With LstFldEdit.ListItems.Add(, , rsArea!Livello)
      .SubItems(1) = IIf(IsNull(rsArea!Ordinale), "", rsArea!Ordinale)
      .SubItems(2) = IIf(IsNull(rsArea!Nome), "", rsArea!Nome)
      .SubItems(3) = IIf(IsNull(rsArea!Byte), "", rsArea!Byte)
      .SubItems(4) = IIf(IsNull(rsArea!Lunghezza), "", rsArea!Lunghezza)
      .SubItems(5) = IIf(IsNull(rsArea!Decimali), "", rsArea!Decimali)
      .SubItems(6) = IIf(IsNull(rsArea!Posizione), "", rsArea!Posizione)
      If rsArea!Segno = "N" Then
        .SubItems(7) = ""
      Else
        .SubItems(7) = IIf(IsNull(rsArea!Segno), "", rsArea!Segno)
      End If
      .SubItems(8) = IIf(IsNull(rsArea!Tipo & rsArea!Usage), "", rsArea!Tipo & " - " & rsArea!Usage)
      .SubItems(9) = IIf(IsNull(rsArea!Occurs), "", rsArea!Occurs)
      .SubItems(10) = IIf(IsNull(rsArea!NomeRed), "", rsArea!NomeRed)
      .SubItems(11) = IIf(IsNull(rsArea!Sel_Field), "", rsArea!Sel_Field)
      .SubItems(12) = IIf(IsNull(rsArea!Operatore), "", rsArea!Operatore)
      .SubItems(13) = IIf(IsNull(rsArea!Selettore_Valore), "", rsArea!Selettore_Valore)
      .tag = rsArea!IdCmp
    End With
    rsArea.MoveNext
  Loop
  rsArea.Close
   
  Set rsArea = m_fun.Open_Recordset("select * from MgConvData_Area where " & _
                                    "IdOggetto = " & Oggetto & " AND " & _
                                    "IdArea = " & Area)
  If Not (rsArea.EOF) Then
    If rsArea!ReplaceSpacetoZero Then
      chkBlankZero = 1
    Else
      chkBlankZero = 0
    End If
  End If
  rsArea.Close
   
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim wResp As Variant
  If SwMod Then
    wResp = MsgBox("Save the area?", vbYesNo)
  End If
  If wResp = vbYes Then AggiornaArea
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim wResp As Variant
  Dim k As Integer
  Dim IndSel As Integer

  On Error Resume Next
  Select Case Button.key
    Case "New"
      SwMod = True
      InserisciCampoEdit
    Case "Up"
      SpostaCampo Button.key
    Case "Down"
      SpostaCampo Button.key
    Case "Copy"
      IndSelC = LstFldEdit.SelectedItem.index
    Case "Paste"
      IndSel = LstFldEdit.SelectedItem.index
      With LstFldEdit.ListItems.Add(, , LstFldEdit.ListItems(IndSelC).text)
        For k = 1 To 11
          .SubItems(k) = LstFldEdit.ListItems(IndSelC).SubItems(k)
        Next k
      End With
      For k = LstFldEdit.ListItems.Count To IndSel Step -1
        LstFldEdit.ListItems(k).Selected = True
        SpostaCampo "Up"
      Next k
      SwMod = True
    Case "Cut"
      SwMod = True
      LstFldEdit.ListItems.Remove LstFldEdit.SelectedItem.index
      LstFldEdit.Refresh
    Case "Save"
      If SwMod Then
        AggiornaArea
        SwMod = False
      End If
    Case "Check"
      SwMod = True
      VerificaArea
    Case "Close"
      If SwMod Then
        wResp = MsgBox("Save the new area?", vbYesNo)
      End If
      If wResp = vbYes Then AggiornaArea
  End Select
End Sub

Sub InserisciCampoEdit()
  Dim K1 As Integer
  Dim I1 As Integer
  Dim I2 As Integer
  Dim wStr As String
   
  K1 = LstFldEdit.SelectedItem.index
   
  With LstFldEdit.ListItems.Add(, , "5")
     .SubItems(1) = "0"
     .SubItems(2) = "New Field"
     .SubItems(3) = "0"
     .SubItems(4) = "0"
     .SubItems(5) = "0"
     .SubItems(6) = "0"
     .SubItems(7) = "N"
     .SubItems(8) = "X - ZND"
     .SubItems(9) = "0"
     .SubItems(10) = ""
  End With
   
  For I1 = LstFldEdit.ListItems.Count To K1 + 2 Step -1
    wStr = LstFldEdit.ListItems(I1).text
    LstFldEdit.ListItems(I1).text = LstFldEdit.ListItems(I1 - 1).text
    LstFldEdit.ListItems(I1 - 1).text = wStr
      
    For I2 = 1 To 11
      wStr = LstFldEdit.ListItems(I1).SubItems(I2)
      LstFldEdit.ListItems(I1).SubItems(I2) = LstFldEdit.ListItems(I1 - 1).SubItems(I2)
      LstFldEdit.ListItems(I1 - 1).SubItems(I2) = wStr
    Next I2
   
  Next I1
End Sub

Sub SpostaCampo(Modo As String)
  Dim K1 As Integer
  Dim Ida As Integer
  Dim Ia As Integer
  Dim wStr As String
  Dim swBold As Boolean
  Dim swColor As Variant
   
  Ida = LstFldEdit.SelectedItem.index
   
  If Modo = "Up" Then
    Ia = Ida - 1
  Else
    Ia = Ida + 1
  End If

  wStr = LstFldEdit.ListItems(Ida).text
  LstFldEdit.ListItems(Ida).text = LstFldEdit.ListItems(Ia).text
  LstFldEdit.ListItems(Ia).text = wStr
  
  swBold = LstFldEdit.ListItems(Ida).ListSubItems(2).Bold
  swColor = LstFldEdit.ListItems(Ida).ListSubItems(2).ForeColor
  
  For K1 = 1 To 11
    wStr = LstFldEdit.ListItems(Ida).SubItems(K1)
    LstFldEdit.ListItems(Ida).SubItems(K1) = LstFldEdit.ListItems(Ia).SubItems(K1)
    LstFldEdit.ListItems(Ida).ListSubItems(K1).Bold = LstFldEdit.ListItems(Ia).ListSubItems(K1).Bold
    LstFldEdit.ListItems(Ida).ListSubItems(K1).ForeColor = LstFldEdit.ListItems(Ia).ListSubItems(K1).ForeColor
      
    LstFldEdit.ListItems(Ia).SubItems(K1) = wStr
  Next K1
   
  LstFldEdit.ListItems(Ia).ListSubItems(2).Bold = swBold
  LstFldEdit.ListItems(Ia).ListSubItems(2).ForeColor = swColor
   
End Sub

Sub AggiornaArea()
  Dim indCmp As Integer
  Dim rsArea As Recordset
  
  Dim k, i As Integer
  Dim wTipo As String
  Dim wUsage As String
  Dim wOperatore As String
  Dim wSelettoreValore As String
  Dim SenzaSelector As Boolean
  Dim wResp As String
  Dim Area, Oggetto As Long
  
  On Error GoTo ErrNoStr:
  
  SenzaSelector = False
  For i = 1 To LstFldEdit.ListItems.Count
    If LstFldEdit.ListItems(i).SubItems(12) = "" And Not (Trim(LstFldEdit.ListItems(i).SubItems(10)) = "") Then
      SenzaSelector = True
      Exit For
    End If
  Next i
  If SenzaSelector Then
    wResp = MsgBox("Redefines without selector!" & vbCrLf & "Do you want to continue?", vbInformation + vbYesNo, "Attention!")
    If wResp = vbNo Then
      Exit Sub
    End If
  End If

  Oggetto = Left(indexArea, InStr(indexArea, "\") - 1)
  Area = Mid(indexArea, InStr(indexArea, "\") + 1)
  m_fun.FnConnection.BeginTrans
  m_fun.FnConnection.Execute "Delete * From MgConvData_Cmp where IdArea = " & Area & " AND IdOggetto = " & Oggetto
  
  Set rsArea = m_fun.Open_Recordset("Select * From MgConvData_Cmp")
    
  For indCmp = 1 To LstFldEdit.ListItems.Count
    
    k = InStr(LstFldEdit.ListItems(indCmp).SubItems(8), "-")
    wTipo = Trim(Mid$(LstFldEdit.ListItems(indCmp).SubItems(8), 1, k - 1))
    wUsage = Trim(Mid$(LstFldEdit.ListItems(indCmp).SubItems(8), k + 1))
    
    rsArea.AddNew
    rsArea!IdOggetto = Oggetto
    rsArea!idArea = Area
    rsArea!IdCmp = LstFldEdit.ListItems(indCmp).tag
    rsArea!Nome = LstFldEdit.ListItems(indCmp).SubItems(2)
    rsArea!Ordinale = indCmp
    rsArea!Livello = LstFldEdit.ListItems(indCmp).text
    rsArea!Tipo = wTipo
    rsArea!Posizione = Val(LstFldEdit.ListItems(indCmp).SubItems(6))
    rsArea!Byte = Val(LstFldEdit.ListItems(indCmp).SubItems(3))
    rsArea!Lunghezza = Val(LstFldEdit.ListItems(indCmp).SubItems(4))
    rsArea!Decimali = Val(LstFldEdit.ListItems(indCmp).SubItems(5))
    rsArea!Segno = LstFldEdit.ListItems(indCmp).SubItems(7)
    rsArea!Usage = wUsage
    rsArea!Picture = " "
    rsArea!Occurs = LstFldEdit.ListItems(indCmp).SubItems(9)
    rsArea!NomeRed = LstFldEdit.ListItems(indCmp).SubItems(10)
    rsArea!Sel_Field = LstFldEdit.ListItems(indCmp).SubItems(11)
    rsArea!Operatore = LstFldEdit.ListItems(indCmp).SubItems(12)
    rsArea!Selettore_Valore = LstFldEdit.ListItems(indCmp).SubItems(13)
    rsArea.Update

  Next indCmp
  rsArea.Close
  
  Set rsArea = m_fun.Open_Recordset("select * from MgConvData_Area")
  If Not (rsArea.EOF) Then
    rsArea!ReplaceSpacetoZero = chkBlankZero
    rsArea.Update
  End If
  rsArea.Close
  
  m_fun.FnConnection.CommitTrans
  Exit Sub
ErrNoStr:
  m_fun.FnConnection.RollbackTrans
  MsgBox "Save Error"
End Sub

Sub VerificaArea()
   
  Dim K1 As Integer
  Dim Y1 As Integer
  Dim wTot As Integer
  Dim wLen As Integer
  Dim LivLen(50, 2)
  For K1 = 1 To LstFldEdit.ListItems.Count
    If K1 < LstFldEdit.ListItems.Count Then
      If Val(LstFldEdit.ListItems(K1).text) < Val(LstFldEdit.ListItems(K1 + 1).text) Then
        If LstFldEdit.ListItems(K1 + 1) <> "88" Then
          LstFldEdit.ListItems(K1).SubItems(4) = 0
        End If
        If LstFldEdit.ListItems(K1).SubItems(4) > 0 Then
          LstFldEdit.ListItems(K1).SubItems(5) = Val(LstFldEdit.ListItems(K1).SubItems(5))
          wLen = Val(LstFldEdit.ListItems(K1).SubItems(4)) + Val(LstFldEdit.ListItems(K1).SubItems(5))
          If InStr(LstFldEdit.ListItems(K1).SubItems(8), "PCK") > 0 Then
            wLen = Int((wLen) / 2) + 1
          End If
          If InStr(LstFldEdit.ListItems(K1).SubItems(8), "BIN") > 0 Then
            wLen = 2
            If LstFldEdit.ListItems(K1).SubItems(4) > 4 Then wLen = 4
            If LstFldEdit.ListItems(K1).SubItems(4) > 8 Then wLen = 8
          End If
          LstFldEdit.ListItems(K1).SubItems(3) = wLen
        Else
          LstFldEdit.ListItems(K1).SubItems(5) = 0
          LstFldEdit.ListItems(K1).SubItems(3) = 0
        End If
      End If
    End If
  Next K1
  Y1 = 1
  K1 = LstFldEdit.ListItems.Count
   
  LivLen(Y1, 1) = 1
  LivLen(Y1, 2) = 0
  Y1 = 2
  LivLen(Y1, 1) = LstFldEdit.ListItems(K1).text
  If LstFldEdit.ListItems(K1).SubItems(9) > 0 Then
    LivLen(Y1, 2) = Val(LstFldEdit.ListItems(K1).SubItems(3)) * LstFldEdit.ListItems(K1).SubItems(9)
  Else
    LivLen(Y1, 2) = Val(LstFldEdit.ListItems(K1).SubItems(3))
  End If
  
  For K1 = LstFldEdit.ListItems.Count - 1 To 1 Step -1
    If Val(LstFldEdit.ListItems(K1)) > Val(LstFldEdit.ListItems(K1 + 1)) Then
      wTot = Val(LstFldEdit.ListItems(K1).SubItems(3))
      For Y1 = 1 To 50
        If Val(LivLen(Y1, 1)) > Val(LstFldEdit.ListItems(K1).text) Then
          LivLen(Y1, 1) = ""
          LivLen(Y1, 2) = 0
        End If
        If Val(LivLen(Y1, 1)) = Val(LstFldEdit.ListItems(K1).text) Then
          If Trim(LstFldEdit.ListItems(K1).SubItems(10)) = "" Then
            LivLen(Y1, 2) = Val(LivLen(Y1, 2)) + wTot
          End If
        End If
        If Trim(LivLen(Y1, 1)) = "" Then
          LivLen(Y1, 1) = LstFldEdit.ListItems(K1).text
          If Trim(LstFldEdit.ListItems(K1).SubItems(10)) = "" Then
            If LstFldEdit.ListItems(K1).SubItems(9) > 0 Then
              LivLen(Y1, 2) = (Val(LstFldEdit.ListItems(K1).SubItems(3)) * Val(LstFldEdit.ListItems(K1).SubItems(9)))
            Else
              LivLen(Y1, 2) = Val(LstFldEdit.ListItems(K1).SubItems(3))
            End If
          Else
            LivLen(Y1, 2) = 0
          End If
          Exit For
        End If
      Next Y1
    End If
    If Val(LstFldEdit.ListItems(K1)) = Val(LstFldEdit.ListItems(K1 + 1)) Then
      If Trim(LstFldEdit.ListItems(K1).SubItems(10)) = "" Then
        For Y1 = 1 To 50
          If Val(LivLen(Y1, 1)) = Val(LstFldEdit.ListItems(K1).text) Then
            If LstFldEdit.ListItems(K1).SubItems(9) > 0 Then
              LivLen(Y1, 2) = Val(LivLen(Y1, 2)) + (Val(LstFldEdit.ListItems(K1).SubItems(3)) * Val(LstFldEdit.ListItems(K1).SubItems(9)))
            Else
              LivLen(Y1, 2) = Val(LivLen(Y1, 2)) + Val(LstFldEdit.ListItems(K1).SubItems(3))
            End If
            Exit For
          End If
          If Trim(LivLen(Y1, 1)) = "" Then
            LivLen(Y1, 1) = LstFldEdit.ListItems(K1).text
            If Trim(LstFldEdit.ListItems(K1).SubItems(10)) = "" Then
              LivLen(Y1, 2) = LstFldEdit.ListItems(K1).SubItems(3)
            Else
              LivLen(Y1, 2) = 0
            End If
            Exit For
          End If
        Next Y1
      End If
    End If
    If Val(LstFldEdit.ListItems(K1)) < Val(LstFldEdit.ListItems(K1 + 1)) Then
      wLen = 0
      For Y1 = 1 To 50
        If Val(LivLen(Y1, 1)) > Val(LstFldEdit.ListItems(K1).text) Then
          LivLen(Y1, 1) = ""
          wLen = wLen + Val(LivLen(Y1, 2))
          LivLen(Y1, 2) = 0
        End If
      Next Y1
      For Y1 = 1 To 50
        If Val(LivLen(Y1, 1)) = Val(LstFldEdit.ListItems(K1).text) Then
          If LstFldEdit.ListItems(K1).SubItems(9) > 0 Then
            wLen = wLen * LstFldEdit.ListItems(K1).SubItems(9)
          End If
          LstFldEdit.ListItems(K1).SubItems(3) = wLen  'non c'ho capito niente
          If Trim(LstFldEdit.ListItems(K1).SubItems(10)) = "" Then
            LivLen(Y1, 2) = Val(LivLen(Y1, 2)) + wLen
            wLen = 0
          End If
'            LstFldEdit.ListItems(K1).SubItems(3) = val(LivLen(Y1, 2))
          wTot = 0
        End If
      Next Y1
      If wLen > 0 Then
        For Y1 = 1 To 50
          If Trim(LivLen(Y1, 1)) = LstFldEdit.ListItems(K1).text Then
            Exit For
          End If
          If Trim(LivLen(Y1, 1)) = "" Then
            LivLen(Y1, 1) = LstFldEdit.ListItems(K1).text
            If Trim(LstFldEdit.ListItems(K1).SubItems(10)) = "" Then
              If Trim(LstFldEdit.ListItems(K1).SubItems(9)) > 0 Then
                wLen = wLen * LstFldEdit.ListItems(K1).SubItems(9)
              End If
              LivLen(Y1, 2) = Val(wLen)
            Else
              LivLen(Y1, 2) = 0
            End If
            Exit For
          End If
        Next Y1
        LstFldEdit.ListItems(K1).SubItems(3) = wLen
        wTot = 0
      End If
    End If
  Next K1
   
  K1 = 1
  LstFldEdit.ListItems(K1).SubItems(1) = K1
  LstFldEdit.ListItems(K1).SubItems(6) = 1
  wLen = 1
   
  For K1 = 2 To LstFldEdit.ListItems.Count
    LstFldEdit.ListItems(K1).SubItems(1) = K1
    If Trim(LstFldEdit.ListItems(K1).SubItems(10)) = "" Then
      If LstFldEdit.ListItems(K1 - 1).SubItems(4) <> "0" Or LstFldEdit.ListItems(K1 - 1).SubItems(5) <> "0" Then
        wLen = wLen + Val(LstFldEdit.ListItems(K1 - 1).SubItems(3))
      End If
      LstFldEdit.ListItems(K1).SubItems(6) = wLen
    Else
      For Y1 = 1 To K1
        If Trim(LstFldEdit.ListItems(Y1).SubItems(2)) = Trim(LstFldEdit.ListItems(K1).SubItems(10)) Then
          wLen = LstFldEdit.ListItems(Y1).SubItems(6)
          LstFldEdit.ListItems(K1).SubItems(6) = wLen
          Exit For
        End If
      Next Y1
    End If
  Next K1
End Sub

Private Sub LstFldEdit_DblClick()
  Dim Blank As Integer
  
  IndSel = LstFldEdit.SelectedItem.index
  TxtLiv = LstFldEdit.SelectedItem.text
  TxtNome = LstFldEdit.SelectedItem.SubItems(2)
  TxtLen = LstFldEdit.SelectedItem.SubItems(4)
  TxTDec = LstFldEdit.SelectedItem.SubItems(5)
  TxtOccurs = LstFldEdit.SelectedItem.SubItems(9)
  TxtNomeRed = LstFldEdit.SelectedItem.SubItems(10)
  cmbType.text = LstFldEdit.SelectedItem.SubItems(8)
  If Trim(TxtNomeRed) = "" Then
    cmbField.Enabled = False
    cmbOperator.Enabled = False
    TxtSelValue.Enabled = False
  Else
    cmbField.Enabled = True
    cmbOperator.Enabled = True
    TxtSelValue.Enabled = True
    cmbField.text = Trim(LstFldEdit.SelectedItem.SubItems(11))
    cmbOperator.text = ConvertiOperatore(LstFldEdit.SelectedItem.SubItems(12))
    TxtSelValue.text = Trim(LstFldEdit.SelectedItem.SubItems(13))
  End If
  CmdFldOk.Enabled = True
End Sub

Sub Carica_cmbField()
  Dim rsArea As Recordset
  Dim IdCampo As String
  Dim LivelloCampo As Integer
  Dim Area As Long, Oggetto As Long
   
  cmbField.Clear
  
  Oggetto = Left(indexArea, InStr(indexArea, "\") - 1)
  Area = Mid(indexArea, InStr(indexArea, "\") + 1)
  Set rsArea = m_fun.Open_Recordset("Select * from MgConvData_Cmp where IdArea = " & Area & " AND IdOggetto = " & Oggetto)
  Do Until rsArea.EOF
    'Carico elenco campi senza Occurs e Redefines
    If Not (Trim(rsArea!NomeRed) = "") Or Not (rsArea!Occurs = 0) Then
      LivelloCampo = rsArea!Livello
      IdCampo = rsArea!IdCmp
      Do Until rsArea.EOF
        If rsArea!Livello <= LivelloCampo And rsArea!IdCmp <> IdCampo Then
          Exit Do
        End If
        rsArea.MoveNext
      Loop
    Else
      cmbField.AddItem rsArea!Nome
      rsArea.MoveNext
    End If
  Loop
  rsArea.Close
End Sub

Function VerificaSelettore() As Boolean
  Dim rsCmp As Recordset
  Dim arrayBetween() As String
  Dim i As Long
  Dim textCompare As String
  
  VerificaSelettore = False
  
  'stefano: era veramente povero: non gestiva BETWEEN sui numerici, n� il segno, n� il NUMERIC
  ' (per ora simulato con il BETWEEN tra -9999 e +9999
  Set rsCmp = m_fun.Open_Recordset("select * from MgConvData_Cmp where Nome = '" & Replace(cmbField.text, "'", "''") & "'")
  If Not (rsCmp.EOF) Then
    If cmbOperator.text = "BETWEEN" Then
     arrayBetween = Split(Trim(TxtSelValue), " ")
     If UBound(arrayBetween) = 0 Then
        MsgBox "Insert data range!", vbExclamation + vbOKOnly, "Attention!"
        VerificaSelettore = True
     End If
    Else
     ReDim arrayBetween(0)
     arrayBetween(0) = TxtSelValue
    End If
    If rsCmp!Tipo = "9" Then
      For i = 0 To UBound(arrayBetween)
       arrayBetween(i) = Replace(arrayBetween(i), ".", ",")
       If Not (IsNumeric(arrayBetween(i))) Then
         MsgBox "Only numeric values!", vbExclamation + vbOKOnly, "Attention!"
         VerificaSelettore = True
       End If
       If InStr(arrayBetween(i), ",") > 0 And rsCmp!Decimali = 0 Then
         MsgBox "Only integer values!", vbExclamation + vbOKOnly, "Attention!"
         VerificaSelettore = True
       ElseIf InStr(arrayBetween(i), ",") > 0 And rsCmp!Decimali > 0 Then
         If Len(Trim(Mid(arrayBetween(i), InStr(arrayBetween(i), ",") + 1))) > rsCmp!Decimali Then
           MsgBox "Decimal wrong!", vbExclamation + vbOKOnly, "Attention!"
           VerificaSelettore = True
         End If
       End If
       textCompare = Replace(Replace(Replace(arrayBetween(i), ",", ""), "-", ""), "+", "")
       If Len(textCompare) > rsCmp!Lunghezza + rsCmp!Decimali Then
         MsgBox "Value too long!", vbExclamation + vbOKOnly, "Attention!"
         VerificaSelettore = True
       End If
      Next
    Else
      For i = 0 To UBound(arrayBetween)
       If Len(arrayBetween(i)) > rsCmp!Byte Then
         MsgBox "Value too long!", vbExclamation + vbOKOnly, "Attention!"
         VerificaSelettore = True
       End If
      Next
    End If
  End If
  rsCmp.Close
  
  'Confronto Redefines con campo origine per lunghezza corretta
  
  
End Function
