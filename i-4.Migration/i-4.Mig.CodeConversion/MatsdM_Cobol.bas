Attribute VB_Name = "MatsdM_Cobol"
Option Explicit

'''Type ExecMacro
'''   IdOggetto As Long
'''   riga As Long
'''   KeyMacro As Long
'''   OrderMacro As String
'''   NameMacro As String
'''   NumIstr As Long
'''End Type

Global ActCobol As Boolean
Global ActExecCics As Boolean
Global ActExecDB2 As Boolean
Global ActExecJCL As Boolean
Global ActExecPLI As Boolean

Global GbIdOggetto As Long

Global TbOggetti As Recordset
Global TbComponenti As Recordset
Global TbVSAM As Recordset
Global TbPSB As Recordset
Global TbPSBSEG As Recordset
Global TbSegnalazioni As Recordset
Global TbRelaObjUnk As Recordset
Global TbRela As Recordset

Global GbFileInput As String
Global GbNumRec As Variant


Global AC_FileInput As String
Global AC_NFileInput As Variant
Global AC_FileOutput As String
Global AC_NFileOutput As Variant
Global AC_Recin As String
Global AC_Recout As String
Global AC_CblWord() As String

Global NumFeatures As Integer
Global NumObjInMod As Long

Global GbOldRec As String

Public EditSysMigr As String
Public IndLsw As Long
'Collection Per i Parametri
Public TsCollectionParam As Collection

Function CompattaBlank(wStringa As String) As String
  Dim wStr As String
  Dim k As Long
  
  wStr = Trim(wStringa)
  While InStr(wStr, Space$(2)) > 0
     wStr = Replace(wStr, Space$(2), Space$(1))
  Wend
  
  CompattaBlank = wStr
End Function
Function LeggiBufferCbl(wRec, nFcb, wLimite) As String
  Dim wRecord As String
  Dim Flag As Boolean
  Dim wRecin As String
  Dim SwSql As Boolean
  
  Flag = 0
  wRecord = Mid$(wRec, 1, 72) & Space$(8)
  
  If InStr(wRec & " ", " SQL ") > 0 Then SwSql = True
  If InStr(wRec, wLimite) > 0 Then
     Flag = 1
  End If

'If Len(wRec) < 81 And Flag = 0 Then
'   wRecord = RTrim(Mid$(wRec, 1, 72))
'End If

  While Flag = 0 And Not EOF(nFcb)
      Line Input #nFcb, wRecin
      If Mid$(wRecin, 7, 1) <> "*" Then
         wRecord = wRecord & vbCrLf & Space$(7) & Mid$(wRecin, 8, 65) & Space$(8)
         If InStr(wRecin, wLimite) > 0 Then
             Flag = 1
         End If
      End If
      If Not SwSql Then
  '        If InStr(wRecin, ".") > 0 Then
  '          Flag = 1
  '        End If
      End If
  Wend
  
  LeggiBufferCbl = wRecord

End Function

Sub AggiungiSegnalazione(wTipo, wVariab1, wVariab2, wRiga, wIdOggetto)

Dim wVar1 As String
Dim wVar2 As String
Dim wCodice As String
Dim wPeso As String
Dim wRel As String
Dim wNumRec As Variant


Set TbSegnalazioni = m_fun.Open_Recordset("select * from Bs_Segnalazioni where idoggetto = " & GbIdOggetto)

wVar1 = Space$(1)
wVar2 = Space$(1)
wCodice = "F00"

wNumRec = wRiga

Select Case wTipo
   Case "NOCXCMD"
      wVar1 = wVariab1
      wCodice = "F13"
      wPeso = "W"
   Case "NOCXPARM"
      wVar1 = wVariab1
      wVar2 = wVariab2
      wCodice = "F14"
      wPeso = "W"
   Case "NOORACMD"
      wVar1 = wVariab1
      wCodice = "F15"
      wPeso = "W"
   Case "NOORAWORD"
      wVar1 = wVariab1
      wCodice = "F17"
      wPeso = "W"
   Case "NOORAPARM"
      wVar1 = wVariab1
      wVar2 = wVariab2
      wCodice = "F16"
      wPeso = "W"
   Case "NOJCLWORD"
      wVar1 = wVariab1
      wVar2 = wVariab2
      wCodice = "F19"
      wPeso = "W"
   Case "NOJCLPARM"
      wVar1 = wVariab1
      wVar2 = wVariab2
      wCodice = "F18"
      wPeso = "W"
End Select
    
  On Error Resume Next
  TbSegnalazioni.AddNew
  TbSegnalazioni!IdOggetto = wIdOggetto
  TbSegnalazioni!riga = wNumRec
  TbSegnalazioni!Codice = wCodice
  TbSegnalazioni!Origine = "AS"
  TbSegnalazioni!data_segnalazione = Now
  TbSegnalazioni!Var1 = wVar1
  TbSegnalazioni!Var2 = wVar2
  TbSegnalazioni!gravita = wPeso
  TbSegnalazioni!tipologia = "PARSER"
  
  TbSegnalazioni.Update

  SettaErrLevel wPeso, "PARSER"

  TbSegnalazioni.Close
End Sub

Function AnalizzaOggetto(wIdOggetto) As Boolean

  Dim DirectoryOut As String
  
  GbIdOggetto = wIdOggetto
  
 
  Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & GbIdOggetto & " ")
  If TbOggetti.RecordCount = 0 Then
     MsgBox "Adding Object not found...", vbInformation, Menu.TsNameProdotto
  End If
   
  GbNumRec = 0
 
  GbFileInput = Menu.TsPathDef & Mid$(TbOggetti!directory_input, 2) & "\" & TbOggetti!Nome
  If Trim(TbOggetti!estensione) = "" Then
    AC_FileInput = Menu.TsPathDef & Mid$(TbOggetti!directory_input, 2) & "\" & TbOggetti!Nome
  Else
    AC_FileInput = Menu.TsPathDef & Mid$(TbOggetti!directory_input, 2) & "\" & TbOggetti!Nome & "." & TbOggetti!estensione
  End If
  If Trim(TbOggetti!estensione) = "" Then
    AC_FileOutput = Menu.TsPathDef & Mid$(TbOggetti!directory_input, 2) & "\Check\" & TbOggetti!Nome
  Else
    AC_FileOutput = Menu.TsPathDef & Mid$(TbOggetti!directory_input, 2) & "\Check\" & TbOggetti!Nome & "." & TbOggetti!estensione
  End If
  
  DirectoryOut = Menu.TsPathDef & Mid$(TbOggetti!directory_input, 2) & "\Check"
  If Dir(DirectoryOut, vbDirectory) = "" Then
     MkDir DirectoryOut
  End If
  
'  SetMsgFinp "Check Source Cobol " & GbFileInput & " started"
       
   ALLINEA_COBOL
 
'  SetMsgFinp "Check Source Cobol " & GbFileInput & " ended"

End Function

Sub ALLINEA_COBOL()
    
    AC_NFileInput = FreeFile
    Open AC_FileInput For Input As AC_NFileInput
    AC_NFileOutput = FreeFile
    Close #AC_NFileOutput
    
    Open AC_FileOutput For Output As AC_NFileOutput
    
    While Not EOF(AC_NFileInput)
      Line Input #AC_NFileInput, AC_Recin
         
         If ActCobol Then
            AC_Remarks
            AC_FileDescr
            AC_Copy
            AC_Values
            AC_CurrentDate
            AC_TimeOfDay
            AC_Transform
            AC_Examine
            AC_ResWord
         End If
                  
'         If ActExecCics Then
'            AC_MTP
'            AC_EXECCICS
'         End If
'
'         If ActExecDB2 Then
'
'         End If
         
      Print #AC_NFileOutput, AC_Recin
    Wend
    
    Close #AC_NFileInput
    Close #AC_NFileOutput

End Sub

Sub Analizza_Mtp(wIdOggetto As Long)
  Dim TbIstr As Recordset, TbMtp As Recordset
  Dim wEnv As Integer
  Dim SwErr As Boolean, SwFlag As Boolean
  Dim wSql As String
  Dim wTabWord() As String
  Dim IndW As Integer
  Dim wWord As String, wStr1 As String, wType As String, wType1 As String, wType2 As String, istruzione As String
  Dim wIdIstr As Variant, wIstrNone As Variant
  Dim wRiga As Variant
  Dim k As Integer
  
  'SQ: chiarire/buttare via 'sta roba!!!!!!!
  wEnv = 8 ' MTP
  
  On Error Resume Next

  'SQ - Buttare via 'ste SELECT: provengo dalla stessa select!!!!!!!!
  Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & wIdOggetto)
  If TbOggetti.RecordCount = 0 Then
    MsgBox "Adding Object not found...", vbInformation, Menu.TsNameProdotto
  End If
  
  '''''''''''''''''''''''''''
  ' Init Segnalazioni Macro
  ''''''''''''''''''''''''''
  wSql = "Delete * from Bs_Segnalazioni where idoggetto = " & wIdOggetto & " and (codice = 'F13' or codice = 'F14')"
  Menu.TsConnection.Execute wSql
  
  AggErrLevel wIdOggetto
  
  Set TbIstr = m_fun.Open_Recordset("select * from PsExec where idoggetto = " & wIdOggetto & " and area = 'CICS' order by riga")
  While Not TbIstr.EOF
    'SQ - TMP: compatibilit� vecchio giro (EXEC CICS ...) (serve alle "instr" sotto)
    istruzione = " " & TbIstr!istruzione
    
    SwErr = False
    Set TbMtp = m_fun.Open_Recordset_Sys("select * from Instructions where " & _
                                                  "environmentid = " & wEnv & " and instruction = '" & TbIstr!comando & "' order by type desc")
     If TbMtp.RecordCount = 0 Then
        MatsdF_Cobol.TwAnaSeg.Nodes.Add , , "OBJ" & Format(wIdOggetto, "000000"), TbOggetti!Nome & "(" & TbOggetti!Tipo & ")"
        MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, "MTP" & Format(wIdOggetto, "000000") & "-" & Format(TbIstr!riga, "000000") & "#C#" & TbIstr!comando, TbIstr!comando & ": MTP command isn't supported"
        AggiungiSegnalazione "NOCXCMD", TbIstr!comando, " ", TbIstr!riga, wIdOggetto
        SwErr = True
     Else
        wIstrNone = 0
        If TbMtp.RecordCount > 1 Then 'SQ - perch� 'sta separazione?
          SwFlag = False
          'Ricerca "caso"
          While Not TbMtp.EOF And Not SwFlag
            wIdIstr = TbMtp!Id
            wType = Trim(TbMtp!Type)
            'wType: uno/enne parametri => wType1,wType2
            k = InStr(wType, " ")
            If k = 0 Then
               wType1 = wType
               wType2 = ""
            Else
               wType1 = Trim(Left(wType, k - 1))
               wType2 = Trim(Mid(wType, k + 1))
            End If
            
            If wType1 <> "none" Then
              'Check parametro 1
              If InStr(istruzione, " " & wType1 & " ") Or InStr(istruzione, " " & wType1 & "(") Then  'terminatore: spazio/tonda
                'Check parametro 2
                If Trim(wType2) <> "" Then
                  If InStr(istruzione, " " & wType2 & " ") Or InStr(istruzione, " " & wType2 & "(") Then
                    SwFlag = True 'trovato il caso
                  End If
                Else
                  SwFlag = True 'trovato il caso
                End If
              End If
            Else
              wIstrNone = TbMtp!Id
            End If
            TbMtp.MoveNext
          Wend
          
          If wIstrNone And Not SwFlag Then
            SwFlag = True
            wIdIstr = wIstrNone
          End If
          
          If Not SwFlag Then
            'caso non trovato! => ISTRUZIONE INVALIDA
            MatsdF_Cobol.TwAnaSeg.Nodes.Add , , "OBJ" & Format(wIdOggetto, "000000"), TbOggetti!Nome & "(" & TbOggetti!Tipo & ")"
            MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, "MTP" & Format(wIdOggetto, "000000") & "-" & Format(TbIstr!riga, "000000") & "#C#" & TbIstr!comando, TbIstr!comando & ": MTP command Type isn't supported"
            '''AggiungiSegnalazione "NOCXPARM", "None", TbIstr!comando, TbIstr!riga, wIdOggetto
            AggiungiSegnalazione "NOCXPARM", "", TbIstr!comando, TbIstr!riga, wIdOggetto
            SwErr = True
          End If
        Else
          wIdIstr = TbMtp!Id
          wType = Trim(TbMtp!Type)
        End If
     End If
     '''''''''''''''''''''''''''''''''''''''''''''''
     ' Analisi parametri
     '''''''''''''''''''''''''''''''''''''''''''''''
     'SwErr=true <=> Segnalazione gi� fatta sopra
     If Not SwErr Then
      'Costruzione TabWord: array di parametri
      wStr1 = Trim(istruzione)
      wStr1 = Replace(wStr1, "(", " (")
      wStr1 = Replace(wStr1, ")", ") ")
      wStr1 = Replace(wStr1, "  ", " ")
  '           wStr1 = Replace(wStr1, "ADDRESS OF ", "")
      IndW = 0
      ReDim TabWord(IndW)
      While Len(wStr1)
        If Left(wStr1, 1) = "(" Then
           k = InStr(wStr1 & " ", ")")
        Else
           k = InStr(wStr1 & " ", " ")
        End If
        If k = 0 Then k = Len(wStr1)
        wWord = Trim(Left(wStr1, k - 1))
        wStr1 = Trim(Mid(wStr1, k + 1))
        While Mid(wStr1, 1, 1) = ")"
          wStr1 = Trim(Mid$(wStr1, 2))
        Wend
        If Mid$(wWord, 1, 1) = "(" Then wWord = ""
        k = InStr(wWord, "(")
        If k > 0 Then wWord = Mid$(wWord, 1, k - 1)
        wWord = Replace(wWord, ".", "")
        If Trim(wWord) <> "" Then
          IndW = IndW + 1
          ReDim Preserve TabWord(IndW)
          TabWord(IndW) = UCase(Trim(wWord))
        End If
      Wend
      
      'SQ  ??????????????
      'If TabWord(1) <> "EXEC" Then
      '  TabWord(1) = "COMMENT"
      'End If
      For IndW = 1 To UBound(TabWord)
        Select Case TabWord(IndW)
           Case "COMMENT", "RESP", "RESP2", "NOHANDLE"
           Case Else
            'If TbIstr!comando <> TabWord(IndW) And wType1 <> TabWord(IndW) And wType2 <> TabWord(IndW) Then
            'Controllo parametri (salto i token appartenenti al comando)
            If wType1 <> TabWord(IndW) And wType2 <> TabWord(IndW) Then
              'Per ogni parola rifaccio la stessa query: ricerca "like": chiarire
               Set TbMtp = m_fun.Open_Recordset_Sys("select * from IstrParameters where instructionid = " & wIdIstr & " and parameter like '" & TabWord(IndW) & "%' ")
               If TbMtp.RecordCount = 0 Then
                  MatsdF_Cobol.TwAnaSeg.Nodes.Add , , "OBJ" & Format(wIdOggetto, "000000"), TbOggetti!Nome & "(" & TbOggetti!Tipo & ")"
                  If wType <> "none" Then
                    AggiungiSegnalazione "NOCXPARM", TabWord(IndW), TbIstr!comando & "[" & wType & "]", TbIstr!riga, wIdOggetto
                    MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, "MTP" & Format(wIdOggetto, "000000") & "-" & Format(TbIstr!riga, "000000") & "#C#" & TbIstr!comando & "#T#" & wType & "#P#" & TabWord(IndW), TbIstr!comando & "[" & wType & "]" & ": MTP Parameter (" & TabWord(IndW) & ") isn't supported"
                  Else
                    AggiungiSegnalazione "NOCXPARM", TabWord(IndW), TbIstr!comando, TbIstr!riga, wIdOggetto
                    MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, "MTP" & Format(wIdOggetto, "000000") & "-" & Format(TbIstr!riga, "000000") & "#C#" & TbIstr!comando & "#P#" & TabWord(IndW), TbIstr!comando & ": MTP Parameter (" & TabWord(IndW) & ") isn't supported"
                  End If
               Else
                'OK: parametro noto
               End If
            End If
        End Select
      Next IndW
     End If
     TbIstr.MoveNext
  Wend
  
  'Aggiornamento Error level
  'SQ ?????????????????????????
  If TbIstr.RecordCount Then
    'SQ � gi� aperto??!!
    ''''''''''''Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & wIdOggetto & " ")
    TbOggetti!errlevel = GbErrLevel
    TbOggetti.Update
    'SQ lo chiude per il chiamante!!!!!!!!!!!!!!!!!
    'TbOggetti.Close
  End If
  TbIstr.Close
End Sub

'DA FARE...
Sub Analizza_PLI(wIdOggetto As Long)
  Dim TbIstr As Recordset, TbMtp As Recordset
  Dim wEnv As Integer
  Dim SwErr As Boolean, SwFlag As Boolean
  Dim wSql As String
  Dim wTabWord() As String
  Dim IndW As Integer
  Dim wWord As String, wStr1 As String, wType As String, wType1 As String, wType2 As String, istruzione As String
  Dim wIdIstr As Variant, wIstrNone As Variant
  Dim wRiga As Variant
  Dim k As Integer
  
  'TMP
  Exit Sub
  
  'SQ: chiarire/buttare via 'sta roba!!!!!!!
  wEnv = 8 ' MTP
  
   
  On Error Resume Next

  'SQ - Buttare via 'ste SELECT: provengo dalla stessa select!!!!!!!!
  Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & wIdOggetto)
  If TbOggetti.RecordCount = 0 Then
    MsgBox "Adding Object not found...", vbInformation, Menu.TsNameProdotto
  End If
  
  '''''''''''''''''''''''''''
  ' Init Segnalazioni Macro
  ''''''''''''''''''''''''''
  wSql = "Delete * from Bs_Segnalazioni where idoggetto = " & wIdOggetto & " and (codice = 'F13' or codice = 'F14')"
  Menu.TsConnection.Execute wSql
  
  AggErrLevel wIdOggetto
  
  Set TbIstr = m_fun.Open_Recordset("select * from PsExec where idoggetto = " & wIdOggetto & " and area = 'CICS' order by riga")
  While Not TbIstr.EOF
    'SQ - TMP: compatibilit� vecchio giro (EXEC CICS ...) (serve alle "instr" sotto)
    istruzione = " " & TbIstr!istruzione
    
    SwErr = False
    Set TbMtp = m_fun.Open_Recordset_Sys("select * from Instructions where " & _
                                                  "environmentid = " & wEnv & " and instruction = '" & TbIstr!comando & "' order by type desc")
     If TbMtp.RecordCount = 0 Then
        MatsdF_Cobol.TwAnaSeg.Nodes.Add , , "OBJ" & Format(wIdOggetto, "000000"), TbOggetti!Nome & "(" & TbOggetti!Tipo & ")"
        MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, "MTP" & Format(wIdOggetto, "000000") & "-" & Format(TbIstr!riga, "000000") & "#C#" & TbIstr!comando, TbIstr!comando & ": MTP command isn't supported"
        AggiungiSegnalazione "NOCXCMD", TbIstr!comando, " ", TbIstr!riga, wIdOggetto
        SwErr = True
     Else
        wIstrNone = 0
        If TbMtp.RecordCount > 1 Then 'SQ - perch� 'sta separazione?
          SwFlag = False
          'Ricerca "caso"
          While Not TbMtp.EOF And Not SwFlag
            wIdIstr = TbMtp!Id
            wType = Trim(TbMtp!Type)
            'wType: uno/enne parametri => wType1,wType2
            k = InStr(wType, " ")
            If k = 0 Then
               wType1 = wType
               wType2 = ""
            Else
               wType1 = Trim(Left(wType, k - 1))
               wType2 = Trim(Mid(wType, k + 1))
            End If
            
            If wType1 <> "none" Then
              'Check parametro 1
              If InStr(istruzione, " " & wType1 & " ") Or InStr(istruzione, " " & wType1 & "(") Then  'terminatore: spazio/tonda
                'Check parametro 2
                If Trim(wType2) <> "" Then
                  If InStr(istruzione, " " & wType2 & " ") Or InStr(istruzione, " " & wType2 & "(") Then
                    SwFlag = True 'trovato il caso
                  End If
                Else
                  SwFlag = True 'trovato il caso
                End If
              End If
            Else
              wIstrNone = TbMtp!Id
            End If
            TbMtp.MoveNext
          Wend
          
          If wIstrNone And Not SwFlag Then
            SwFlag = True
            wIdIstr = wIstrNone
          End If
          
          If Not SwFlag Then
            'caso non trovato! => ISTRUZIONE INVALIDA
            MatsdF_Cobol.TwAnaSeg.Nodes.Add , , "OBJ" & Format(wIdOggetto, "000000"), TbOggetti!Nome & "(" & TbOggetti!Tipo & ")"
            MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, "MTP" & Format(wIdOggetto, "000000") & "-" & Format(TbIstr!riga, "000000") & "#C#" & TbIstr!comando, TbIstr!comando & ": MTP command Type isn't supported"
            '''AggiungiSegnalazione "NOCXPARM", "None", TbIstr!comando, TbIstr!riga, wIdOggetto
            AggiungiSegnalazione "NOCXPARM", "", TbIstr!comando, TbIstr!riga, wIdOggetto
            SwErr = True
          End If
        Else
          wIdIstr = TbMtp!Id
          wType = Trim(TbMtp!Type)
        End If
     End If
     '''''''''''''''''''''''''''''''''''''''''''''''
     ' Analisi parametri
     '''''''''''''''''''''''''''''''''''''''''''''''
     'SwErr=true <=> Segnalazione gi� fatta sopra
     If Not SwErr Then
      'Costruzione TabWord: array di parametri
      wStr1 = Trim(istruzione)
      wStr1 = Replace(wStr1, "(", " (")
      wStr1 = Replace(wStr1, ")", ") ")
      wStr1 = Replace(wStr1, "  ", " ")
  '           wStr1 = Replace(wStr1, "ADDRESS OF ", "")
      IndW = 0
      ReDim TabWord(IndW)
      While Len(wStr1)
        If Left(wStr1, 1) = "(" Then
           k = InStr(wStr1 & " ", ")")
        Else
           k = InStr(wStr1 & " ", " ")
        End If
        If k = 0 Then k = Len(wStr1)
        wWord = Trim(Left(wStr1, k - 1))
        wStr1 = Trim(Mid(wStr1, k + 1))
        While Mid(wStr1, 1, 1) = ")"
          wStr1 = Trim(Mid$(wStr1, 2))
        Wend
        If Mid$(wWord, 1, 1) = "(" Then wWord = ""
        k = InStr(wWord, "(")
        If k > 0 Then wWord = Mid$(wWord, 1, k - 1)
        wWord = Replace(wWord, ".", "")
        If Trim(wWord) <> "" Then
          IndW = IndW + 1
          ReDim Preserve TabWord(IndW)
          TabWord(IndW) = UCase(Trim(wWord))
        End If
      Wend
      
      'SQ  ??????????????
      'If TabWord(1) <> "EXEC" Then
      '  TabWord(1) = "COMMENT"
      'End If
      For IndW = 1 To UBound(TabWord)
        Select Case TabWord(IndW)
           Case "COMMENT", "RESP", "RESP2", "NOHANDLE"
           Case Else
            'If TbIstr!comando <> TabWord(IndW) And wType1 <> TabWord(IndW) And wType2 <> TabWord(IndW) Then
            'Controllo parametri (salto i token appartenenti al comando)
            If wType1 <> TabWord(IndW) And wType2 <> TabWord(IndW) Then
              'Per ogni parola rifaccio la stessa query: ricerca "like": chiarire
               Set TbMtp = m_fun.Open_Recordset_Sys("select * from IstrParameters where instructionid = " & wIdIstr & " and parameter like '" & TabWord(IndW) & "%' ")
               If TbMtp.RecordCount = 0 Then
                  MatsdF_Cobol.TwAnaSeg.Nodes.Add , , "OBJ" & Format(wIdOggetto, "000000"), TbOggetti!Nome & "(" & TbOggetti!Tipo & ")"
                  If wType <> "none" Then
                    AggiungiSegnalazione "NOCXPARM", TabWord(IndW), TbIstr!comando & "[" & wType & "]", TbIstr!riga, wIdOggetto
                    MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, "MTP" & Format(wIdOggetto, "000000") & "-" & Format(TbIstr!riga, "000000") & "#C#" & TbIstr!comando & "#T#" & wType & "#P#" & TabWord(IndW), TbIstr!comando & "[" & wType & "]" & ": MTP Parameter (" & TabWord(IndW) & ") isn't supported"
                  Else
                    AggiungiSegnalazione "NOCXPARM", TabWord(IndW), TbIstr!comando, TbIstr!riga, wIdOggetto
                    MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, "MTP" & Format(wIdOggetto, "000000") & "-" & Format(TbIstr!riga, "000000") & "#C#" & TbIstr!comando & "#P#" & TabWord(IndW), TbIstr!comando & ": MTP Parameter (" & TabWord(IndW) & ") isn't supported"
                  End If
               Else
                'OK: parametro noto
               End If
            End If
        End Select
      Next IndW
     End If
     TbIstr.MoveNext
  Wend
  
  'Aggiornamento Error level
  'SQ ?????????????????????????
  If TbIstr.RecordCount Then
    'SQ � gi� aperto??!!
    ''''''''''''Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & wIdOggetto & " ")
    TbOggetti!errlevel = GbErrLevel
    TbOggetti.Update
    'SQ lo chiude per il chiamante!!!!!!!!!!!!!!!!!
    'TbOggetti.Close
  End If
  TbIstr.Close
End Sub

Sub Analizza_JCL(wIdOggetto)
  Dim TbIstr As Recordset, TbJOB As Recordset
  Dim wEnv As Integer
  Dim SwErr As Boolean, SwFlag As Boolean
  Dim wSql As String
  Dim wTabWord() As String
  Dim IndW As Integer
  Dim wWord As String, wStr1 As String
  Dim wIdIstr As Variant
  Dim wRiga As Variant
  Dim wRigaStart As Integer
  Dim k As Integer
  Dim NomeInput As String, nFile As String, wRec As String, WBuf As String
  Dim nStep As Integer
  Dim wProg As String, wTypeJob As String, jobType As String
  
  On Error Resume Next '????ALE
  
  GbOldRec = ""
  
  '''''''''''''''''''''''''''''''''
  ' Serve per l'update alla fine
  '''''''''''''''''''''''''''''''''
  Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & wIdOggetto & " ")
  If TbOggetti.RecordCount = 0 Then
     MsgBox "Adding Object not found...", vbInformation, Menu.TsNameProdotto
     Exit Sub
  Else
    jobType = TbOggetti!Tipo
    ''''''''''''''''''''''''''
    'TMP - ATTENZIONE... (VSE)
    ''''''''''''''''''''''''''
    If jobType = "PRC" Or jobType = "MBR" Then jobType = "JCL"
  End If
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Reset segnalazioni tipo: "needs clean-up"...
  ' Vengono aggiornate alla fine
  '''''''''''''''''''''''''''''''''''''''''''''''''''
  Menu.TsConnection.Execute "Delete * from Bs_Segnalazioni " & _
                            "where idoggetto = " & wIdOggetto & " and codice in('F18','F19','F20')"
  AggErrLevel wIdOggetto
  
  NomeInput = TbOggetti!directory_input & "\" & TbOggetti!Nome
  If Trim(TbOggetti!estensione) <> "" Then
    NomeInput = NomeInput & "." & TbOggetti!estensione
  End If
  'SQ: nome file con "$" -> SBAGLIA!!!!!!!!!
  'NomeInPut = Replace(NomeInPut, "$", Menu.TsPathDef)
  NomeInput = Replace(NomeInput, "$\", Menu.TsPathDef & "\")
  nFile = FreeFile
  
  Open NomeInput For Input As nFile
  wRigaStart = 0
  WBuf = ""
  If jobType = "JCL" Then
    nStep = -1
  End If
  While Not EOF(nFile)
    wRigaStart = wRigaStart + CountRiga(WBuf, "none")
    
    Select Case jobType
      Case "JCL"
        WBuf = LeggiBufferMVS(nFile, False)
        wProg = LeggiProgMVS(WBuf)
        wTypeJob = "M%"
      Case "JOB"
        WBuf = LeggiBufferVSE(nFile, False)
        wProg = LeggiProgVSE(WBuf)
        wTypeJob = "V%"
    End Select
    nStep = nStep + 1
    SwErr = False
    Set TbJOB = m_fun.Open_Recordset("select * from TL_migr where type = '" & jobType & "' and Apply = true")
    While Not TbJOB.EOF
      If SearchRegEx(WBuf, TbJOB!word1, TbJOB!Boolean & "", TbJOB!word2 & "") Then
        wRiga = wRigaStart + CountRiga(WBuf, TbJOB!word1)
        MatsdF_Cobol.TwAnaSeg.Nodes.Add , , "OBJ" & Format(wIdOggetto, "000000"), TbOggetti!Nome & "(" & jobType & ")"
        MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, jobType & Format(wIdOggetto, "000000") & "-" & Trim(nStep) & "#W1#" & TbJOB!operazione, "Step n. " & nStep & ": JCL Command " & TbJOB!operazione & " needs clean-up for MBM"
        '''''''''''''''''''''''''''''''''''''''''
        '
        '''''''''''''''''''''''''''''''''''''''''
        AggiungiSegnalazione "NOJCLWORD", TbJOB!operazione, wProg, wRiga, wIdOggetto
        SwErr = True
      End If
      TbJOB.MoveNext
    Wend
    TbJOB.Close
  Wend
  Close nFile
  
  ''''''''''Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & wIdOggetto & " ")
  TbOggetti!errlevel = GbErrLevel
  TbOggetti.Update
  '''''''''''''TbOggetti.Close
   
End Sub

Function CountRiga(wBuffer, wWord) As Integer
  Dim nRighe As Integer
  Dim wStr1 As String
  Dim k As Integer
  If UCase(wWord) = "NONE" Then
     wStr1 = wBuffer
  Else
     k = InStr(wBuffer, wWord)
     wStr1 = Mid$(wBuffer, 1, k)
  End If
  If wBuffer = "" Then
     CountRiga = 0
     Exit Function
  End If
  If Mid$(wBuffer, 1, 2) = vbCrLf Then
    nRighe = 0
  Else
    nRighe = 1
  End If
  k = InStr(wStr1, vbCrLf)
  While k > 0
     nRighe = nRighe + 1
     wStr1 = Mid$(wStr1, k + 2)
     k = InStr(wStr1, vbCrLf)
  Wend
  CountRiga = nRighe
End Function
Function LeggiProgMVS(wBuffer) As String
  Dim wStr1 As String
  Dim k As Integer
  k = InStr(wBuffer, " EXEC ")
  If k = 0 Then
     LeggiProgMVS = "none"
     Exit Function
  End If
  wStr1 = LTrim(Mid$(wBuffer, k + 6))
  k = InStr(wStr1, " ")
  If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
  k = InStr(wStr1, ",")
  If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
  k = InStr(wStr1, vbCrLf)
  If k > 0 Then wStr1 = Mid$(wStr1, 1, k - 1)
  k = InStr(wStr1, "=")
  If k > 0 Then wStr1 = Mid$(wStr1, k + 1)
  
  LeggiProgMVS = wStr1

End Function
Function LeggiProgVSE(wBuffer) As String

End Function
Function LeggiBufferMVS(wFile, IncludeComment) As String
   Dim wRecin As String
   Dim wBuffer As String
   If GbOldRec <> "" Then
     wBuffer = GbOldRec
     GbOldRec = ""
   Else
     wBuffer = ""
   End If
   While Not EOF(wFile)
      Line Input #wFile, wRecin
      If Mid$(wRecin, 1, 3) <> "//*" And Not IncludeComment Then
        If InStr(wRecin, " EXEC ") > 0 Then
           GbOldRec = wRecin
           LeggiBufferMVS = wBuffer
           Exit Function
        End If
        wBuffer = wBuffer & vbCrLf & wRecin
      End If
   Wend
   LeggiBufferMVS = wBuffer
End Function
Function LeggiBufferVSE(nFile, IncludeComment) As String
   Dim wRecin As String
   Dim wBuffer As String
   'If GbOldRec <> "" Then
   '  wBuffer = GbOldRec
   '  GbOldRec = ""
   'Else
   '  wBuffer = ""
   'End If
   While Not EOF(nFile)
      If Len(GbOldRec) Then
         wBuffer = GbOldRec
         GbOldRec = ""
      Else
         Line Input #nFile, wRecin
         wBuffer = wBuffer & vbCrLf & wRecin
      End If
      If Left(wBuffer, 3) <> "//*" And Not IncludeComment Then
        If InStr(wBuffer, " EXEC ") Then
            'DOBBIAMO LEGGERE TUTTA LA SYSIN...
            Line Input #nFile, wRecin
            '''GbOldRec = wRecin
            While Not EOF(nFile) And Left(wRecin, 2) <> "/*" And Left(wRecin, 2) <> "//"
               wBuffer = wBuffer & vbCrLf & wRecin
               Line Input #nFile, wRecin
            Wend
            'If InStr(wRecin, " EXEC ") Then
            If Left(wRecin, 2) = "//" Then
               GbOldRec = wRecin
               LeggiBufferVSE = wBuffer
            Else
               GbOldRec = ""
               LeggiBufferVSE = wBuffer & vbCrLf & wRecin
            End If
            Exit Function
        End If
'''        wBuffer = wBuffer & vbCrLf & wRecin
      End If
   Wend
   LeggiBufferVSE = wBuffer
End Function

Sub Analizza_Db2(wIdOggetto)
  Dim TbIstr As Recordset, TbSQL As Recordset
  Dim wEnv As Integer
  Dim SwErr As Boolean, SwFlag As Boolean
  Dim wSql As String
  Dim wTabWord() As String
  Dim IndW As Integer
  Dim wWord As String, wStr1 As String, wType As String, wType1 As String, wType2 As String
  Dim wIdIstr As Variant, wRiga As Variant
  Dim k As Integer

  On Error Resume Next
  
  'SQ: e' stato appena aperto dal chiamante!?
  Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & wIdOggetto & " ")
  If TbOggetti.RecordCount = 0 Then
    MsgBox "Adding Object not found...", vbInformation, Menu.TsNameProdotto
  End If
  
  'SQ - SISTEMARE CON LE NUOVE TABELLE! CHI SA CHE MESSAGGI SONO QUELLI!?
  wSql = "DELETE * FROM Bs_Segnalazioni WHERE idoggetto = " & wIdOggetto & _
         " and (codice = 'F15' or codice = 'F16' or codice = 'F17')"
  Menu.TsConnection.Execute wSql
  
  AggErrLevel wIdOggetto
  
  Set TbIstr = m_fun.Open_Recordset("select * from PsExec where idoggetto = " & wIdOggetto & " and area = 'SQL' order by riga")
  If TbIstr.RecordCount Then
    While Not TbIstr.EOF
      SwErr = False
      Set TbSQL = m_fun.Open_Recordset("select * from TL_migr where type = 'SQL' and Apply = true order by Operazione")
      If TbSQL.RecordCount Then
        While Not TbSQL.EOF
          ' If SearchRegEx(TbIstr!Istruzione, TbSQL!word1, TbSQL!Boolean & "", TbSQL!word2 & "") Then
            If SearchRegEx(TbIstr!comando & " " & TbIstr!istruzione, TbSQL!word1, TbSQL!Boolean & "", TbSQL!word2 & "") Then
               MatsdF_Cobol.TwAnaSeg.Nodes.Add , , "OBJ" & Format(wIdOggetto, "000000"), TbOggetti!Nome & "(" & TbOggetti!Tipo & ")"
               MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, "SQL" & Format(wIdOggetto, "000000") & "-" & Format(TbIstr!riga, "000000") & "#C#" & TbIstr!comando & "#W#" & TbSQL!operazione, TbIstr!comando & ": Oracle doesn't support the format for : " & TbSQL!operazione
               AggiungiSegnalazione "NOORAWORD", TbSQL!operazione, TbIstr!comando, TbIstr!riga, wIdOggetto
               SwErr = True
            End If
            TbSQL.MoveNext
         Wend
      End If
      TbIstr.MoveNext
    Wend
    
    'SQ ?????????????????????
    'Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & wIdOggetto & " ")
    TbOggetti!errlevel = GbErrLevel
    TbOggetti.Update
  End If
End Sub

Function SearchRegEx(stringa As String, Criterio1 As String, AndOr As String, Criterio2 As String)
  Dim RegEx As Object
  Dim SwFound1 As Boolean
  Dim SwFound2 As Boolean
  Dim Matches As Variant
  Dim wStr As Variant
  Dim Match As Variant
  
  SwFound1 = False
  SwFound2 = False
  
  Set RegEx = CreateObject("VBScript.RegExp")
  RegEx.Pattern = Criterio1
  RegEx.IgnoreCase = True
  RegEx.Global = True
  
  Set Matches = RegEx.Execute(stringa)
    
  For Each Match In Matches
    SwFound1 = True
    Exit For
  Next
  
  Set RegEx = Nothing
  If Trim(AndOr) = "" Then
    SearchRegEx = SwFound1
    Exit Function
  End If
  
  Set RegEx = CreateObject("VBScript.RegExp")
  RegEx.Pattern = Criterio2
  RegEx.IgnoreCase = True
  RegEx.Global = True
  
  Set Matches = RegEx.Execute(stringa)
    
  For Each Match In Matches
    SwFound2 = True
    Exit For
  Next
  SearchRegEx = False
  If Trim(UCase(AndOr)) = "AND" Then
    If SwFound1 = True And SwFound2 = True Then
       SearchRegEx = True
    End If
 End If
    
  If Trim(UCase(AndOr)) = "OR" Then
    If SwFound1 = True Or SwFound2 = True Then
       SearchRegEx = True
    End If
  End If
  
  If Trim(UCase(AndOr)) = "NOT" Then
    If SwFound1 = True And SwFound2 = False Then
       SearchRegEx = True
    End If
  End If
    
  Set RegEx = Nothing
  
End Function

Sub AC_MTP()

Dim k As Integer

Dim wStr As String
Dim wWord As String
Dim wIstr As String
Dim wRec As String

Dim TabWord() As String
Dim IndW As Integer

If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub
If InStr(AC_Recin, " EXEC ") = 0 Then Exit Sub
If InStr(AC_Recin, " CICS ") = 0 Then Exit Sub

wIstr = Mid$(AC_Recin, 12, 61)
k = InStr(AC_Recin, "END-EXEC")
If k = 0 Then k = InStr(AC_Recin, ".")

While k = 0
   Line Input #AC_NFileInput, wRec
   AC_Recin = AC_Recin & vbCrLf & wRec
   wIstr = wIstr & " " & Mid$(wRec, 12, 61)
   k = InStr(AC_Recin, "END-EXEC")
   If k = 0 Then k = InStr(AC_Recin, ".")
Wend

wStr = Trim(Mid$(AC_Recin, 8))
IndW = 0

ReDim TabWord(IndW)
While Len(wStr) > 0
  k = InStr(wStr, " ")
  If k = 0 Then k = Len(wStr)
  wWord = Trim(Mid$(wStr, 1, k - 1))
  wStr = Trim(Mid$(wStr, k + 1))
  IndW = IndW + 1
  ReDim Preserve TabWord(IndW)
  TabWord(IndW) = wWord
Wend

For IndW = 1 To UBound(TabWord)

Next IndW

End Sub
Sub AC_EXECCICS()
Dim k As Integer
Dim Y As Integer
Dim Ind As Integer

Dim wTabIstr()
Dim wRec As String
Dim wIstr As String
Dim SwCorr As Boolean
Dim wIdent As Integer
Dim pWhere As Integer
Dim pLength As Integer
Dim wStr1 As String
Dim wStr2 As String
Dim wStr3 As String
Dim NewArea As String

Dim K0 As Integer
Dim K1 As Integer


If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub
If InStr(AC_Recin, " EXEC ") = 0 Then Exit Sub
If InStr(AC_Recin, " CICS ") = 0 Then Exit Sub

wIstr = Mid$(AC_Recin, 12, 61)
k = InStr(AC_Recin, "END-EXEC")
If k = 0 Then k = InStr(AC_Recin, ".")

While k = 0
   Line Input #AC_NFileInput, wRec
   AC_Recin = AC_Recin & vbCrLf & wRec
   wIstr = wIstr & " " & Mid$(wRec, 12, 61)
   k = InStr(AC_Recin, "END-EXEC")
   If k = 0 Then k = InStr(AC_Recin, ".")
Wend

k = InStr(AC_Recin, " SPOOLOPEN ")

If k > 0 Then
  k = InStr(AC_Recin, " USERDATA")
  If k > 0 Then
    For Y = k To Len(AC_Recin)
     If Mid$(AC_Recin, Y, 1) = ")" Then
       Mid$(AC_Recin, Y, 1) = " "
       Exit For
     End If
     Mid$(AC_Recin, Y, 1) = " "
    Next Y
    Mid$(AC_Recin, k + 1, 18) = "OUTPUT NODE(LOCAL)"
  End If
  k = InStr(AC_Recin, " LINES")
  If k > 0 Then
    For Y = k To Len(AC_Recin)
       If Mid$(AC_Recin, Y, 1) = ")" Then
         Mid$(AC_Recin, Y, 1) = " "
         Exit For
       End If
       Mid$(AC_Recin, Y, 1) = " "
    Next Y
  End If
  k = InStr(AC_Recin, " REPORT")
  If k > 0 Then
     Mid$(AC_Recin, k, 7) = " USERID"
  End If
End If

k = InStr(AC_Recin, " SPOOLWRITE ")

If k > 0 Then
  k = InStr(AC_Recin, " REPORT")
  If k > 0 Then
        For Y = k To Len(AC_Recin)
           If Mid$(AC_Recin, Y, 1) = ")" Then
             Mid$(AC_Recin, Y, 1) = " "
             Exit For
           End If
           Mid$(AC_Recin, Y, 1) = " "
        Next Y
    End If
End If

k = InStr(AC_Recin, " SPOOLCLOSE ")

If k > 0 Then
  k = InStr(AC_Recin, " REPORT")
  If k > 0 Then
        For Y = k To Len(AC_Recin)
           If Mid$(AC_Recin, Y, 1) = ")" Then
             Mid$(AC_Recin, Y, 1) = " "
             Exit For
           End If
           Mid$(AC_Recin, Y, 1) = " "
        Next Y
  End If
  k = InStr(AC_Recin, " HOLD ")
  If k > 0 Then
     Mid$(AC_Recin, k, 6) = Space$(6)
  End If
End If

k = InStr(AC_Recin, " ADDRESS ")
If k > 0 Then
  
  If InStr(AC_Recin, " TCTUA") > 0 Then
     NewArea = "IO-TCTUA"
     k = InStr(AC_Recin, " TCTUA")
  ElseIf InStr(AC_Recin, " TWA") > 0 Then
     NewArea = "IO-TWA"
     k = InStr(AC_Recin, " TWA")
  Else
  '   Stop
  End If
  wStr1 = Mid$(AC_Recin, 1, k)
  wStr2 = Mid$(AC_Recin, k)
  K1 = InStr(wStr2, ")")
  wStr3 = Mid$(wStr2, K1)
  K0 = InStr(wStr2, "(")
  wStr2 = Mid$(wStr2, 1, K0)
  wStr2 = Space$(Len(wStr1) - Len(NewArea) - 13 + (K1 - K0)) & wStr2 & "ADDRESS OF " & NewArea & wStr3
  AC_Recin = wStr1 & vbCrLf & wStr2
  
End If

k = InStr(AC_Recin, " LOAD ")
If k > 0 Then
  
  If InStr(AC_Recin, "PXASMESS") > 0 Then
     NewArea = "IO-ASMESS"
  ElseIf InStr(AC_Recin, "PXASUT") > 0 Then
     NewArea = "IO-UTMESS"
  ElseIf InStr(AC_Recin, "PXASSS") > 0 Then
     NewArea = "IO-SSMESS"
  Else
  '   Stop
  End If
  
  k = InStr(AC_Recin, " SET")
  wStr1 = Mid$(AC_Recin, 1, k)
  wStr2 = Mid$(AC_Recin, k)
  K1 = InStr(wStr2, ")")
  wStr3 = Mid$(wStr2, K1)
  K0 = InStr(wStr2, "(")
  wStr2 = Mid$(wStr2, 1, K0)
  wStr2 = Space$(Len(wStr1) - Len(NewArea) - 13 + (K1 - K0)) & wStr2 & "ADDRESS OF " & NewArea & wStr3
  AC_Recin = wStr1 & vbCrLf & wStr2
  
End If

k = InStr(AC_Recin, "END-EXEC")
If k = 0 Then
   k = InStr(AC_Recin, ".")
   Mid$(AC_Recin, k, 10) = " END-EXEC."
End If

End Sub

Sub AC_Values()
Dim k As Integer

If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub
k = InStr(AC_Recin, " VALUES ")
If k = 0 Then Exit Sub

Mid$(AC_Recin, k, 8) = " VALUE  "

   


End Sub
Sub AC_ResWord()

Dim k As Integer
Dim Y As Integer
Dim IndW As Integer
Dim IsWord As Boolean
Dim wStr1 As String
Dim wStr2 As String

If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub
If InStr(AC_Recin, " EXEC ") > 0 Then Exit Sub

For IndW = 1 To UBound(AC_CblWord)
  IsWord = False
  k = InStr(AC_Recin, AC_CblWord(IndW))
  If k > 11 Then
      Select Case Mid$(AC_Recin, k - 1, 1)
          Case " ", ",", "(", "."
             IsWord = True
          Case Else
             IsWord = False
     End Select
     If IsWord Then
        If Len(AC_Recin) > k + Len(AC_CblWord(IndW)) Then
           Select Case Mid$(AC_Recin, k + Len(AC_CblWord(IndW)), 1)
               Case " ", ",", ")", "."
                  IsWord = True
               Case Else
                  IsWord = False
           End Select
        End If
     End If
     If IsWord Then
        Y = InStr(Mid$(AC_Recin, k), "   ")
        If Y > 0 Then
            AC_Recin = Mid$(AC_Recin, 1, k + Y - 2) & Mid$(AC_Recin, k + Y + 2)
            AC_Recin = Mid$(AC_Recin, 1, k - 1) & "IT-" & Mid$(AC_Recin, k)
        Else
            wStr1 = Mid$(AC_Recin, 1, k - 1)
            wStr2 = Mid$(AC_Recin, k)
            AC_Recin = wStr1 & " " & vbCrLf & Space$(Len(wStr1) - 3) & "IT-" & wStr2
        End If
     End If
  End If
  
Next IndW


End Sub

Sub AC_Copy()

Dim wRec As String
Dim wWord As String
Dim wStr1 As String
Dim wStr2 As String
Dim wStr3 As String
Dim wCommento As String
Dim SwSuppress As Boolean
Dim k As Integer

If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub
If InStr(AC_Recin, " COPY ") = 0 Then Exit Sub

wWord = LTrim(Mid$(AC_Recin, 8))
If Mid$(wWord, 1, 3) <> "01 " Then Exit Sub

SwSuppress = False
If InStr(AC_Recin, " SUPPRESS.") > 0 Then SwSuppress = True
If InStr(AC_Recin, " SUPPRESS ") > 0 Then SwSuppress = True

k = InStr(AC_Recin, ".")
If k = 0 Then Exit Sub

k = InStr(AC_Recin, " COPY ")
wStr1 = Mid$(AC_Recin, 8, k - 8)
wStr1 = Mid$(wStr1, 3)

wStr2 = Trim(Mid$(AC_Recin, k + 6))

k = InStr(wStr2, ".")
If k > 0 Then wStr2 = Mid$(wStr2, 1, k - 1)
k = InStr(wStr2, " ")
If k > 0 Then wStr2 = Mid$(wStr2, 1, k - 1)

wRec = Mid$(AC_Recin, 1, 7)

wCommento = "ITERAC*" & vbCrLf
wCommento = wCommento & "ITERAC*" & Mid$(AC_Recin, 8) & vbCrLf
wCommento = wCommento & "ITERAC*" & vbCrLf

If Trim(wStr1) = "" Then
  wRec = wRec & " COPY " & wStr2
  If SwSuppress Then
     wRec = wRec & " SUPPRESS."
  Else
     wRec = wRec & "."
  End If
  AC_Recin = wCommento & wRec
  Exit Sub
End If

wStr1 = Trim(wStr1)

wStr3 = wStr2 & "AI"

If wStr1 = wStr2 Or wStr1 = wStr3 Then
    wRec = wRec & " COPY " & wStr2
Else
    wRec = wRec & " COPY " & wStr2 & " REPLACING ==" & TrovaPrRiga(wStr2) & "==" & vbCrLf
    wRec = wRec & Space$(22) & "BY ==01  " & wStr1 & ".=="
End If

If SwSuppress Then
   wRec = wRec & " SUPPRESS."
Else
   wRec = wRec & "."
End If

AC_Recin = wCommento & wRec

End Sub

Function TrovaPrRiga(Nome) As String

Dim tb As Recordset
Dim wIdOggetto As Variant
Dim wNomeFile As String
Dim wNumFile As Variant
Dim wRecCpy As String
Dim k As Integer
If Trim(Nome) = "" Or Trim(Nome) = "'" Then
   TrovaPrRiga = "WARNING CPY NOT FOUND"
   Exit Function
End If
Set tb = m_fun.Open_Recordset("Select * from Bs_Oggetti where tipo = 'CPY' and nome = '" & Nome & ".CPY'")
If tb.RecordCount = 0 Then
   TrovaPrRiga = "WARNING CPY NOT FOUND"
   Exit Function
End If
tb.MoveFirst
wIdOggetto = tb!IdOggetto
wNomeFile = Menu.TsPathDef & Mid$(tb!directory_input, 2) & "\" & tb!Nome
wNumFile = FreeFile

Open wNomeFile For Input As wNumFile

While Not EOF(wNumFile)
  Line Input #wNumFile, wRecCpy
  If Mid$(wRecCpy, 7, 1) <> "*" Then
      wRecCpy = Trim(Mid$(wRecCpy, 8))
      If Mid$(wRecCpy, 1, 2) = "01" Then
          k = InStr(wRecCpy, ".")
          Close wNumFile
          TrovaPrRiga = Mid$(wRecCpy, 1, k)
          Exit Function
      End If
  End If
Wend

Close wNumFile
TrovaPrRiga = "WARNING LV 01 NOT FOUND"

End Function

Sub AC_FileDescr()

Dim wRec As String
Dim vWord As String
Dim wStr1 As String
Dim wStr2 As String

Dim k As Integer
Dim K1 As Integer

If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub

vWord = LTrim(Mid$(AC_Recin, 8))
If Mid$(vWord, 1, 3) <> "FD " Then Exit Sub
k = InStr(AC_Recin, ".")

While k = 0
   Line Input #AC_NFileInput, wRec
   AC_Recin = AC_Recin & vbCrLf & wRec
   k = InStr(AC_Recin, ".")
Wend

k = InStr(AC_Recin, " LABEL ")
If k > 0 Then
    wStr1 = Mid$(AC_Recin, 1, k + 6)
    wStr2 = Mid$(AC_Recin, k + 7)
    vWord = LTrim(wStr2)
    If Mid$(vWord, 1, 7) <> "RECORD " Then
        k = InStr(wStr2, Space$(7))
       If k > 0 Then
           AC_Recin = wStr1 & "@@@1@@@" & Mid$(wStr2, 1, k - 1) & Mid$(wStr2, k + 7)
       Else
           AC_Recin = wStr1 & vbCrLf & Space$(Len(wStr1) - 7) & "@@@1@@@" & wStr2
       End If
    Else
       k = InStr(wStr2, "RECORD ")
       wStr2 = Mid$(wStr2, 1, k - 1) & "@@@1@@@" & Mid$(wStr2, k + 7)
       AC_Recin = wStr1 & wStr2
    End If
End If

k = InStr(AC_Recin, " DATA ")
If k > 0 Then
    wStr1 = Mid$(AC_Recin, 1, k + 5)
    wStr2 = Mid$(AC_Recin, k + 6)
    vWord = LTrim(wStr2)
    If Mid$(vWord, 1, 7) <> "RECORD " Then
        k = InStr(wStr2, Space$(7))
       AC_Recin = wStr1 & "@@@3@@@" & Mid$(wStr2, 1, k - 1) & Mid$(wStr2, k + 7)
    Else
       k = InStr(wStr2, "RECORD ")
       wStr2 = Mid$(wStr2, 1, k - 1) & "@@@3@@@" & Mid$(wStr2, k + 7)
       AC_Recin = wStr1 & wStr2
    End If
End If

k = InStr(AC_Recin, " RECORD ")
If k > 0 Then
    wStr1 = Mid$(AC_Recin, 1, k + 7)
    wStr2 = Mid$(AC_Recin, k + 8)
    vWord = LTrim(wStr2)
    If Mid$(vWord, 1, 9) <> "CONTAINS " Then
       If Mid$(vWord, 1, 3) <> "IS " Then
            k = InStr(wStr2, Space$(9))
            If k > 0 Then
                   AC_Recin = wStr1 & "@@@@2@@@@" & Mid$(wStr2, 1, k - 1) & Mid$(wStr2, k + 9)
            Else
                   AC_Recin = wStr1 & vbCrLf & Space$(Len(wStr1) - 9) & "@@@@2@@@@" & wStr2
            End If
       End If
    End If
End If


k = InStr(AC_Recin, "@@@1@@@")
If k > 0 Then
   AC_Recin = Mid$(AC_Recin, 1, k - 1) & "RECORD " & Mid$(AC_Recin, k + 7)
End If
k = InStr(AC_Recin, "@@@@2@@@@")
If k > 0 Then
   AC_Recin = Mid$(AC_Recin, 1, k - 1) & "CONTAINS " & Mid$(AC_Recin, k + 9)
End If
k = InStr(AC_Recin, "@@@3@@@")
If k > 0 Then
   AC_Recin = Mid$(AC_Recin, 1, k - 1) & " RECORD" & Mid$(AC_Recin, k + 7)
End If

k = InStr(AC_Recin, ".")
If k = 0 Then
   Mid$(AC_Recin, Len(AC_Recin) - 9, 1) = "."
End If
k = InStr(AC_Recin, " 01 ")
If k > 0 Then
   K1 = InStr(AC_Recin, ".")
   If K1 > k Then
      For K1 = k To 1 Step -1
        If Mid$(AC_Recin, K1, 2) = vbCrLf Then
           Mid$(AC_Recin, K1 - 9, 1) = "."
           Exit Sub
        End If
      Next K1
   End If
End If
End Sub
Sub AC_CurrentDate()
Dim vWord As String
Dim wStr1 As String
Dim wStr2 As String

Dim k As Integer

If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub
If InStr(AC_Recin, "CURRENT-DATE") = 0 Then Exit Sub
If InStr(AC_Recin, " MOVE ") = 0 Then Exit Sub
k = InStr(AC_Recin, "CURRENT-DATE")

Mid$(AC_Recin, k, 12) = "'99/99/99'  "

End Sub
Sub AC_TimeOfDay()
Dim vWord As String
Dim wStr1 As String
Dim wStr2 As String
Dim wPunto As String

Dim k As Integer

If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub
If InStr(AC_Recin, "TIME-OF-DAY") = 0 Then Exit Sub
If InStr(AC_Recin, " MOVE ") = 0 Then Exit Sub

wPunto = ""
If InStr(AC_Recin, ".") > 0 Then wPunto = "."

k = InStr(AC_Recin, " MOVE ")
wStr1 = Mid$(AC_Recin, 1, k)

k = InStr(AC_Recin, " TO ")
wStr2 = LTrim(Mid$(AC_Recin, k + 3))
k = InStr(wStr2, " ")
If k > 0 Then wStr2 = Mid$(wStr2, 1, k - 1)
k = InStr(wStr2, ".")
If k > 0 Then wStr2 = Mid$(wStr2, 1, k - 1)

AC_Recin = "I-TERC*" & Mid$(AC_Recin, 8) & vbCrLf
AC_Recin = AC_Recin & wStr1 & "ACCEPT " & wStr2 & " FROM TIME" & wPunto

End Sub
Sub AC_Remarks()
Dim vWord As String
Dim wStr1 As String
Dim wStr2 As String
Dim wRec As String
Dim Flag As Boolean

Dim k As Integer

If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub
If Mid$(AC_Recin, 8, 9) <> "REMARKS. " Then Exit Sub

Mid$(AC_Recin, 7, 1) = "*"
Flag = True
While Flag
  Line Input #AC_NFileInput, wRec
  If Mid$(wRec, 7, 13) = " ENVIRONMENT " Then Flag = False
  If Flag Then
    Mid$(wRec, 7, 1) = "*"
  End If
  AC_Recin = AC_Recin & vbCrLf & wRec
Wend

End Sub

Sub AC_Transform()
Dim vWord As String
Dim wStr1 As String
Dim wStr2 As String
Dim wRec As String
Dim wPref As String
Dim wSuff As String

Dim k As Integer

If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub
If InStr(AC_Recin, " TRANSFORM ") = 0 Then Exit Sub
k = InStr(AC_Recin, " TRANSFORM ")
wPref = Mid$(AC_Recin, 1, k)
wSuff = Mid$(AC_Recin, 73)
AC_Recin = Mid$(AC_Recin, 12, 61)

k = InStr(AC_Recin, " TO ")
While k = 0
   Line Input #AC_NFileInput, wRec
   AC_Recin = AC_Recin & Mid$(wRec, 12, 61)
   k = InStr(AC_Recin, " TO ")
Wend

k = InStr(AC_Recin, "TRANSFORM ")
wStr1 = Mid$(AC_Recin, 1, k - 1)
wStr2 = Mid$(AC_Recin, k + 10)
AC_Recin = wStr1 & " INSPECT " & wStr2

k = InStr(AC_Recin, " FROM ")
wStr1 = Mid$(AC_Recin, 1, k - 1)
wStr2 = Mid$(AC_Recin, k + 6)
If Mid$(LTrim(wStr2), 1, 4) = "ALL" Then
   AC_Recin = wStr1 & " REPLACING " & wStr2
Else
   AC_Recin = wStr1 & " REPLACING ALL " & wStr2
End If

k = InStr(AC_Recin, " TO ")
wStr1 = Mid$(AC_Recin, 1, k - 1)
wStr2 = Mid$(AC_Recin, k + 4)
AC_Recin = wStr1 & " BY " & wStr2
wStr1 = AC_Recin

wStr1 = Trim(AC_Recin)
AC_Recin = ""

While Len(wStr1) > (72 - Len(wPref))
   For k = 72 - Len(wPref) To 1 Step -1
      If Mid$(wStr1, k, 1) = " " Then Exit For
   Next k
   AC_Recin = AC_Recin & Mid$(wPref & Mid$(wStr1, 1, k) & Space$(72), 1, 72) & wSuff & vbCrLf
   wStr1 = Trim(Mid$(wStr1 & Space$(k), k))
Wend
AC_Recin = AC_Recin & Mid$(wPref & wStr1 & Space$(72), 1, 72) & wSuff



End Sub
Sub AC_Examine()
Dim vWord As String
Dim wStr1 As String
Dim wStr2 As String
Dim wRec As String
Dim wPref As String
Dim wSuff As String

Dim k As Integer

If Mid$(AC_Recin, 7, 1) = "*" Then Exit Sub
If InStr(AC_Recin, " EXAMINE ") = 0 Then Exit Sub
k = InStr(AC_Recin, " EXAMINE ")
Mid$(AC_Recin, k, 9) = " INSPECT "

k = InStr(AC_Recin, "TALLYING")
If k = 0 Then Exit Sub
k = InStr(AC_Recin, "REPLACING")
If k = 0 Then Exit Sub

wStr1 = Mid$(AC_Recin, 1, k - 1)
wStr2 = Mid$(AC_Recin, k + 10)
AC_Recin = wStr1 & Space$(9) & wStr2

k = InStr(AC_Recin, "TALLYING")
wStr1 = Mid$(AC_Recin, 1, k - 1)
wStr2 = Mid$(AC_Recin, k + 9)
AC_Recin = wStr1 & "REPLACING " & wStr2


End Sub

Function FormatX(Numero) As String
  Dim HH As Variant
  Dim MM As Variant
  Dim SS As Variant
  Dim wNumero As Variant
  
  wNumero = Numero
  
  HH = Int(wNumero / 3600)
  wNumero = wNumero - (HH * 3600)
  MM = Int(wNumero / 60)
  wNumero = wNumero - (MM * 60)
  SS = wNumero
  
  FormatX = Format(HH, "00") & "." & Format(MM, "00") & "." & Format(SS, "00")
End Function

Sub Analizza_JOB(wIdOggetto As Long)
  Dim TbIstr As Recordset, TbJOB As Recordset
  Dim wEnv As Integer
  Dim SwErr As Boolean, SwFlag As Boolean
  Dim wSql As String
  Dim wTabWord() As String
  Dim IndW As Integer
  Dim wWord As String, wStr1 As String
  Dim wIdIstr As Variant
  Dim wRiga As Variant
  Dim wRigaStart As Integer
  Dim k As Integer
  Dim NomeInput As String, nFile As String, wRec As String, WBuf As String
  Dim nStep As Integer
  Dim wProg As String, wTypeJob As String, jobType As String
  
  On Error Resume Next '????ALE
  
  GbOldRec = ""
  
  '''''''''''''''''''''''''''''''''
  ' Serve per l'update alla fine
  '''''''''''''''''''''''''''''''''
  Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & wIdOggetto & " ")
  If TbOggetti.RecordCount = 0 Then
     MsgBox "Adding Object not found...", vbInformation, Menu.TsNameProdotto
     Exit Sub
  Else
    jobType = TbOggetti!Tipo
    ''''''''''''''''''''''''''
    'TMP - ATTENZIONE... (VSE)
    ''''''''''''''''''''''''''
    If jobType = "PRC" Or jobType = "MBR" Then jobType = "JCL"
  End If
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Reset segnalazioni tipo: "needs clean-up"...
  ' Vengono aggiornate alla fine
  '''''''''''''''''''''''''''''''''''''''''''''''''''
  Menu.TsConnection.Execute "Delete * from Bs_Segnalazioni " & _
                            "where idoggetto = " & wIdOggetto & " and codice in('F18','F19','F20')"
  AggErrLevel wIdOggetto
  
  NomeInput = TbOggetti!directory_input & "\" & TbOggetti!Nome
  If Trim(TbOggetti!estensione) <> "" Then
    NomeInput = NomeInput & "." & TbOggetti!estensione
  End If
  'SQ: nome file con "$" -> SBAGLIA!!!!!!!!!
  'NomeInPut = Replace(NomeInPut, "$", Menu.TsPathDef)
  NomeInput = Replace(NomeInput, "$\", Menu.TsPathDef & "\")
  nFile = FreeFile
  
  Open NomeInput For Input As nFile
  wRigaStart = 0
  WBuf = ""
  While Not EOF(nFile)
    wRigaStart = wRigaStart + CountRiga(WBuf, "none")
    
    WBuf = LeggiBufferVSE(nFile, False)
    '''''''''''''wProg = LeggiProgVSE(WBuf)
    wTypeJob = "V%"
    
    nStep = nStep + 1
    SwErr = False
     
    Set TbJOB = m_fun.Open_Recordset("select * from TL_migr where type = '" & jobType & "' and Apply = true")
    While Not TbJOB.EOF
      If SearchRegEx(WBuf, TbJOB!word1, TbJOB!Boolean & "", TbJOB!word2 & "") Then
        wRiga = wRigaStart + CountRiga(WBuf, TbJOB!word1)
        MatsdF_Cobol.TwAnaSeg.Nodes.Add , , "OBJ" & Format(wIdOggetto, "000000"), TbOggetti!Nome & "(" & jobType & ")"
        MatsdF_Cobol.TwAnaSeg.Nodes.Add "OBJ" & Format(wIdOggetto, "000000"), tvwChild, jobType & Format(wIdOggetto, "000000") & "-" & Trim(nStep) & "#W1#" & TbJOB!operazione, "Step n. " & nStep & ": JCL Command " & TbJOB!operazione & " needs clean-up for MBM"
        '''''''''''''''''''''''''''''''''''''''''
        '
        '''''''''''''''''''''''''''''''''''''''''
        AggiungiSegnalazione "NOJCLWORD", TbJOB!operazione, wProg, wRiga, wIdOggetto
        SwErr = True
      End If
      TbJOB.MoveNext
    Wend
  Wend
  Close nFile
  
  ''''''''''Set TbOggetti = m_fun.Open_Recordset("select * from BS_oggetti where idoggetto = " & wIdOggetto & " ")
  TbOggetti!errlevel = GbErrLevel
  TbOggetti.Update
  '''''''''''''TbOggetti.Close
   
End Sub
