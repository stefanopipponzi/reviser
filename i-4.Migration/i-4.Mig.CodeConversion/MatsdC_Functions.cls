VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MatsdC_Functions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Option Explicit

Dim TsIndMnu As Integer
Dim TsIndImg As Integer

Private m_TsErrCodice As String
Private m_TsErrMsg As String
Private m_TsFinestra As Object
Private m_TsObjList As Object
Private m_TsIdOggetto As Integer
Private m_TsTop As Long
Private m_TsLeft As Long
Private m_TsWidth As Long
Private m_TsHeight As Long
Private m_TsDatabase As ADOX.Catalog
Private m_TsNameDB As String
Private m_TsConnection As ADODB.Connection
Private m_TsConnState As Long
Private m_TsPathDB As String
Private m_TsParent As Long
Private m_TsConnDBSys As ADODB.Connection
Private m_TsPathDef As String
Private m_TsPathPrj As String
Private m_TsUnitDef As String
Private m_TsPathPrd As String
Private m_TsTipoDB As String
Private m_TsId As Integer
Private m_TsLabel As String
Private m_TsToolTiptext As String
Private m_TsPicture As String
Private m_TsPictureEn As String
Private m_TsM1Name As String
Private m_TsM1SubName As String
Private m_TsM1Level As Long
Private m_TsM2Name As String
Private m_TsM3Name As String
Private m_TsM3ButtonType As String
Private m_TsM3SubName As String
Private m_TsM4Name As String
Private m_TsM4ButtonType As String
Private m_TsM4SubName As String
Private m_TsFunzione As String
Private m_TsDllName As String
Private m_TsTipoFinIm As String
Private m_TsText_ColorRich As Boolean
Private m_TsFont As String
Private m_TsFontSize As Long
Private m_TsMFESvName As String
Private m_TsMFESvIP As String
Private m_TsMFESvPort As String
Private m_TsNameProdotto As String
Private m_TsImgDir As String
Private m_TsProvider As e_Provider
Private m_TsAsterixForDelete As String
Private m_TsAsterixForWhere As String
Public DLLFunzioni As New MaFndC_Funzioni

Type ExecMacro
   IdOggetto As Long
   riga As Long
   KeyMacro As Long
   OrderMacro As String
   NameMacro As String
   NumIstr As Long
   Linee As Integer
End Type

Public Function AttivaFunzione(Funzione As String, cParam As Collection, formParent As Object, Optional IdButton As String) As Boolean
  'ALE qui
  
  On Error GoTo ErrorHandler
  
  Select Case Trim(UCase(Funzione))
    Case "SHOW_ALLINEAMENTO_COBOL"
    'ALe 21/03/2006
    Set MatsdF_Cobol.formParent = formParent
    SetParent MatsdF_Cobol.hwnd, formParent.hwnd
    MatsdF_Cobol.Show
    MatsdF_Cobol.Move 0, 0, formParent.Width, formParent.Height
        
'    Case "SHOW_MTP_ALIGN"
'      MatsdF_MTP.Show
    
    Case "SHOW_MACRO"
    
    m_fun.Show_EditMacro formParent
      
      
    Case "SHOW_IEBGENER"
    'Set MatsdF_Cobol.formParent = formParent 'non so se servir� ALE
    SetParent MatsdF_IebGener.hwnd, formParent.hwnd
    MatsdF_IebGener.Show
    MatsdF_IebGener.Move 0, 0, formParent.Width, formParent.Height
    
    Case "SHOW_CTRL_REPOSITORY"
    'Set MatsdF_Cobol.formParent = formParent 'non so se servir� ALE
    SetParent MatsdF_Repos.hwnd, formParent.hwnd
    MatsdF_Repos.Show
    MatsdF_Repos.Move 0, 0, formParent.Width, formParent.Height
    
    Case "SHOW_EBCDIC2ASCII"
    'Set MatsdF_Cobol.formParent = formParent 'non so se servir� ALE
    SetParent MatsdF_EBCDIC2ASCII.hwnd, formParent.hwnd
    MatsdF_EBCDIC2ASCII.Show
    MatsdF_EBCDIC2ASCII.Move 0, 0, formParent.Width, formParent.Height

    Case "SHOW_COBOL_XE"
    'Set MatsdF_Cobol.formParent = formParent 'non so se servir� ALE
    SetParent MatsdF_Cobol_XE.hwnd, formParent.hwnd
    MatsdF_Cobol_XE.Show
    MatsdF_Cobol_XE.Move 0, 0, formParent.Width, formParent.Height

    Case "ASSEMBLER_CONV"
          SetParent Matsdc_Ass2Cob.hwnd, formParent.hwnd
          Matsdc_Ass2Cob.Show
          Matsdc_Ass2Cob.Move 0, 0, formParent.Width, formParent.Height
    Case "EZT_CONV"
        SetParent Matsdc_Ezt2Cob.hwnd, formParent.hwnd
        Matsdc_Ezt2Cob.Show
        Matsdc_Ezt2Cob.Move 0, 0, formParent.Width, formParent.Height
    'SILVIA 02-10-2008 PLI_CONV
    Case "PLI_CONV"
        SetParent Matsdc_Pli2Cob.hwnd, formParent.hwnd
        Matsdc_Pli2Cob.Show
        Matsdc_Pli2Cob.Move 0, 0, formParent.Width, formParent.Height
    'SILVIA 21-10-2009 SAS_CONV
    Case "SAS_CONV"
        SetParent Matsdc_Sas2Java.hwnd, formParent.hwnd
        Matsdc_Sas2Java.Show
        Matsdc_Sas2Java.Move 0, 0, formParent.Width, formParent.Height
  End Select
  
    Exit Function
ErrorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "RC0002"
End Function
Public Function Move_To_Button(Direction As String) As Boolean
  'Direction =
  '
  '1)First    : Si muove al primo bottone dell'array
  '2)Next     : Si muove al successivo
  '3)Previous : Si muove al precedente
  '4)Last     : Si muove all'ultimo
  
  If Not SwMenu Then
    Move_To_Button = Carica_Menu
  End If
  
  'seleziona l'indice
  Select Case UCase(Trim(Direction))
    Case "FIRST"
      TsIndMnu = 1
    Case "NEXT"
      TsIndMnu = TsIndMnu + 1
    Case "PREVIOUS"
      TsIndMnu = TsIndMnu - 2
    Case "LAST"
      TsIndMnu = UBound(TsMenu)
  End Select
    
  If TsIndMnu > UBound(TsMenu) Then
    Move_To_Button = False
    Exit Function
  End If
  
  'assegna alle variabili public il contenuto del 1� elemento dell'array
  TsId = TsMenu(TsIndMnu).Id
  TsLabel = TsMenu(TsIndMnu).Label
  TsToolTiptext = TsMenu(TsIndMnu).ToolTipText
  TsPicture = TsMenu(TsIndMnu).Picture
  TsPictureEn = TsMenu(TsIndMnu).PictureEn
  TsM1Name = TsMenu(TsIndMnu).M1Name
  TsM1SubName = TsMenu(TsIndMnu).M1SubName
  TsM1Level = TsMenu(TsIndMnu).M1Level
  TsM2Name = TsMenu(TsIndMnu).M2Name
  TsM3Name = TsMenu(TsIndMnu).M3Name
  TsM3ButtonType = TsMenu(TsIndMnu).M3ButtonType
  TsM3SubName = TsMenu(TsIndMnu).M3SubName
  TsM4Name = TsMenu(TsIndMnu).M4Name
  TsM4ButtonType = TsMenu(TsIndMnu).M4ButtonType
  TsM4SubName = TsMenu(TsIndMnu).M4SubName
  TsFunzione = TsMenu(TsIndMnu).Funzione
  TsDllName = TsMenu(TsIndMnu).DllName
  TsTipoFinIm = TsMenu(TsIndMnu).TipoFinIm
  
  Move_To_Button = True
End Function

Public Sub InitDll(curDllFunzioni As MaFndC_Funzioni)
   
   Set m_fun = curDllFunzioni
   Set Menu = Me
   
End Sub

Public Sub InitDllParser(curDllParser As MapsdC_Menu)
   
   Set m_pars = curDllParser
   Set Menu = Me
   
End Sub

Public Sub Crea_Tabelle_Tools()
  Crea_Tabelle_Macro
  
  'Carica il DB di Sistema con le macro di Sistema
  
End Sub
'**********************************************************************************
'TSERRCODICE
Public Property Let TsErrCodice(mVar As String)
    m_TsErrCodice = mVar
End Property
Public Property Get TsErrCodice() As String
    TsErrCodice = m_TsErrCodice
End Property
'TSERRMSG
Public Property Let TsErrMsg(mVar As String)
    m_TsErrMsg = mVar
End Property
Public Property Get TsErrMsg() As String
    TsErrMsg = m_TsErrMsg
End Property
'TSFINESTRA
Public Property Set TsFinestra(mVar As Object)
   Set m_TsFinestra = mVar
End Property
Public Property Get TsFinestra() As Object
   Set TsFinestra = m_TsFinestra
End Property
'TSOBJLIST
Public Property Set TsObjList(mVar As Object)
   Set m_TsObjList = mVar
End Property
Public Property Get TsObjList() As Object
   Set TsObjList = m_TsObjList
End Property
'TSIDOGGETTO
Public Property Let TsIdOggetto(mVar As Integer)
    m_TsIdOggetto = mVar
End Property
Public Property Get TsIdOggetto() As Integer
    TsIdOggetto = m_TsIdOggetto
End Property
'TSTOP
Public Property Let TsTop(mVar As Long)
    m_TsTop = mVar
End Property
Public Property Get TsTop() As Long
    TsTop = m_TsTop
End Property
'TSLEFT
Public Property Let TsLeft(mVar As Long)
    m_TsLeft = mVar
End Property
Public Property Get TsLeft() As Long
    TsLeft = m_TsLeft
End Property
'TSWIDTH
Public Property Let TsWidth(mVar As Long)
    m_TsWidth = mVar
End Property
Public Property Get TsWidth() As Long
    TsWidth = m_TsWidth
End Property
'TSHEIGHT
Public Property Let TsHeight(mVar As Long)
    m_TsHeight = mVar
End Property
Public Property Get TsHeight() As Long
    TsHeight = m_TsHeight
End Property
'TSDATABASE
Public Property Set TsDatabase(mVar As ADOX.Catalog)
   Set m_TsDatabase = mVar
End Property
Public Property Get TsDatabase() As ADOX.Catalog
   Set TsDatabase = m_TsDatabase
End Property
'TSNameDB
Public Property Let TsNameDB(mVar As String)
    m_TsNameDB = mVar
End Property
Public Property Get TsNameDB() As String
    TsNameDB = m_TsNameDB
End Property
'TSCONNECTION
Public Property Set TsConnection(mVar As ADODB.Connection)
   Set m_TsConnection = mVar
End Property

Public Property Get TsConnection() As ADODB.Connection
   Set TsConnection = m_TsConnection
End Property

'TSCONNSTATE
Public Property Let TsConnState(mVar As Long)
    m_TsConnState = mVar
End Property

Public Property Get TsConnState() As Long
    TsConnState = m_TsConnState
End Property

'TSPATHDB
Public Property Let TsPathDB(mVar As String)
    m_TsPathDB = mVar
End Property

Public Property Get TsPathDB() As String
    TsPathDB = m_TsPathDB
End Property

'TSPARENT
Public Property Let TsParent(mVar As Long)
    m_TsParent = mVar
End Property

Public Property Get TsParent() As Long
    TsParent = m_TsParent
End Property

'TSCONNDBSYS
Public Property Set TsConnDBSys(mVar As ADODB.Connection)
   Set m_TsConnDBSys = mVar
End Property

Public Property Get TsConnDBSys() As ADODB.Connection
   Set TsConnDBSys = m_TsConnDBSys
End Property

'TSPATHDEF
Public Property Let TsPathDef(mVar As String)
    m_TsPathDef = mVar
End Property

Public Property Get TsPathDef() As String
    TsPathDef = m_TsPathDef
End Property

'TsPathPrj
Public Property Let TsPathPrj(mVar As String)
    m_TsPathPrj = mVar
End Property

Public Property Get TsPathPrj() As String
    TsPathPrj = m_TsPathPrj
End Property

'TSUNITDEF
Public Property Let TsUnitDef(mVar As String)
    m_TsUnitDef = mVar
End Property

Public Property Get TsUnitDef() As String
    TsUnitDef = m_TsUnitDef
End Property

'TSPATHPRD
Public Property Let TsPathPrd(mVar As String)
    m_TsPathPrd = mVar
End Property

Public Property Get TsPathPrd() As String
    TsPathPrd = m_TsPathPrd
End Property

'TSTIPODB
Public Property Let TsTipoDB(mVar As String)
    m_TsTipoDB = mVar
End Property

Public Property Get TsTipoDB() As String
    TsTipoDB = m_TsTipoDB
End Property

'TSID
Public Property Let TsId(mVar As Integer)
    m_TsId = mVar
End Property

Public Property Get TsId() As Integer
    TsId = m_TsId
End Property

'TSLABEL
Public Property Let TsLabel(mVar As String)
    m_TsLabel = mVar
End Property

Public Property Get TsLabel() As String
    TsLabel = m_TsLabel
End Property

'TSTOOLTIPTEXT
Public Property Let TsToolTiptext(mVar As String)
    m_TsToolTiptext = mVar
End Property

Public Property Get TsToolTiptext() As String
    TsToolTiptext = m_TsToolTiptext
End Property

'TSPICTURE
Public Property Let TsPicture(mVar As String)
    m_TsPicture = mVar
End Property

Public Property Get TsPicture() As String
    TsPicture = m_TsPicture
End Property

'TSPICTUREEN
Public Property Let TsPictureEn(mVar As String)
    m_TsPictureEn = mVar
End Property

Public Property Get TsPictureEn() As String
    TsPictureEn = m_TsPictureEn
End Property

'TSM1NAME
Public Property Let TsM1Name(mVar As String)
    m_TsM1Name = mVar
End Property

Public Property Get TsM1Name() As String
    TsM1Name = m_TsM1Name
End Property

'TSM1SUBNAME
Public Property Let TsM1SubName(mVar As String)
    m_TsM1SubName = mVar
End Property

Public Property Get TsM1SubName() As String
    TsM1SubName = m_TsM1SubName
End Property

'TSM1LEVEL
Public Property Let TsM1Level(mVar As Long)
    m_TsM1Level = mVar
End Property

Public Property Get TsM1Level() As Long
    TsM1Level = m_TsM1Level
End Property

'TSM2NAME
Public Property Let TsM2Name(mVar As String)
    m_TsM2Name = mVar
End Property

Public Property Get TsM2Name() As String
    TsM2Name = m_TsM2Name
End Property

'TSM3NAME
Public Property Let TsM3Name(mVar As String)
    m_TsM3Name = mVar
End Property

Public Property Get TsM3Name() As String
    TsM3Name = m_TsM3Name
End Property

'TSM3BUTTONTYPE
Public Property Let TsM3ButtonType(mVar As String)
    m_TsM3ButtonType = mVar
End Property

Public Property Get TsM3ButtonType() As String
    TsM3ButtonType = m_TsM3ButtonType
End Property

'TSM3SUBNAME
Public Property Let TsM3SubName(mVar As String)
    m_TsM3SubName = mVar
End Property

Public Property Get TsM3SubName() As String
    TsM3SubName = m_TsM3SubName
End Property

'TSM4NAME
Public Property Let TsM4Name(mVar As String)
    m_TsM4Name = mVar
End Property

Public Property Get TsM4Name() As String
    TsM4Name = m_TsM4Name
End Property

'TSM4BUTTONTYPE
Public Property Let TsM4ButtonType(mVar As String)
    m_TsM4ButtonType = mVar
End Property

Public Property Get TsM4ButtonType() As String
    TsM4ButtonType = m_TsM4ButtonType
End Property

'TSM4SUBNAME
Public Property Let TsM4SubName(mVar As String)
    m_TsM4SubName = mVar
End Property

Public Property Get TsM4SubName() As String
    TsM4SubName = m_TsM4SubName
End Property

'TSFUNZIONE
Public Property Let TsFunzione(mVar As String)
    m_TsFunzione = mVar
End Property

Public Property Get TsFunzione() As String
    TsFunzione = m_TsFunzione
End Property

'TSDLLNAME
Public Property Let TsDllName(mVar As String)
    m_TsDllName = mVar
End Property

Public Property Get TsDllName() As String
    TsDllName = m_TsDllName
End Property

'TSTIPOFINIM
Public Property Let TsTipoFinIm(mVar As String)
    m_TsTipoFinIm = mVar
End Property

Public Property Get TsTipoFinIm() As String
    TsTipoFinIm = m_TsTipoFinIm
End Property

'TSTEXT_COLORRICH
Public Property Let TsText_ColorRich(mVar As Boolean)
    m_TsText_ColorRich = mVar
End Property

Public Property Get TsText_ColorRich() As Boolean
    TsText_ColorRich = m_TsText_ColorRich
End Property

'TSFONT
Public Property Let TsFont(mVar As String)
    m_TsFont = mVar
End Property

Public Property Get TsFont() As String
    TsFont = m_TsFont
End Property

'TSFONTSIZE
Public Property Let TsFontSize(mVar As Long)
    m_TsFontSize = mVar
End Property

Public Property Get TsFontSize() As Long
    TsFontSize = m_TsFontSize
End Property

'TSMFESVNAME
Public Property Let TsMFESvName(mVar As String)
    m_TsMFESvName = mVar
End Property

Public Property Get TsMFESvName() As String
    TsMFESvName = m_TsMFESvName
End Property

'TSMFESVIP
Public Property Let TsMFESvIP(mVar As String)
    m_TsMFESvIP = mVar
End Property

Public Property Get TsMFESvIP() As String
    TsMFESvIP = m_TsMFESvIP
End Property

'TSMFESVPORT
Public Property Let TsMFESvPort(mVar As String)
    m_TsMFESvPort = mVar
End Property
Public Property Get TsMFESvPort() As String
    TsMFESvPort = m_TsMFESvPort
End Property
'TSNamePRODOTTO
Public Property Let TsNameProdotto(mVar As String)
    m_TsNameProdotto = mVar
End Property
Public Property Get TsNameProdotto() As String
    TsNameProdotto = m_TsNameProdotto
End Property
'TSIMGDIR
Public Property Let TsImgDir(mVar As String)
    m_TsImgDir = mVar
End Property
Public Property Get TsImgDir() As String
    TsImgDir = m_TsImgDir
End Property
'TSPROVIDER
Public Property Let TsProvider(mVar As e_Provider)
    m_TsProvider = mVar
End Property
Public Property Get TsProvider() As e_Provider
    TsProvider = m_TsProvider
End Property
'TSASTERIXFORDELETE
Public Property Let TsAsterixForDelete(mVar As String)
    m_TsAsterixForDelete = mVar
End Property

Public Property Get TsAsterixForDelete() As String
    TsAsterixForDelete = m_TsAsterixForDelete
End Property

'TSASTERIXFORWHERE
Public Property Let TsAsterixForWhere(mVar As String)
    m_TsAsterixForWhere = mVar
End Property

Public Property Get TsAsterixForWhere() As String
    TsAsterixForWhere = m_TsAsterixForWhere
End Property
