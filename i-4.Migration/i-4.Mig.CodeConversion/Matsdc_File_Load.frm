VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form Matsdc_File_Load 
   Caption         =   "Converted Items:"
   ClientHeight    =   11925
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13065
   LinkTopic       =   "Form1"
   ScaleHeight     =   11925
   ScaleWidth      =   13065
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton CmdSave 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   375
      Left            =   12585
      Picture         =   "Matsdc_File_Load.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   8
      Tag             =   "fixed"
      ToolTipText     =   "Save"
      Top             =   45
      Width           =   375
   End
   Begin VB.CommandButton cmdFind2 
      Height          =   375
      Left            =   6480
      Picture         =   "Matsdc_File_Load.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   7
      Tag             =   "fixed"
      ToolTipText     =   "Find"
      Top             =   45
      Width           =   375
   End
   Begin VB.CommandButton cmdFind 
      Height          =   375
      Left            =   0
      Picture         =   "Matsdc_File_Load.frx":06D4
      Style           =   1  'Graphical
      TabIndex        =   2
      Tag             =   "fixed"
      ToolTipText     =   "Find"
      Top             =   60
      Width           =   375
   End
   Begin RichTextLib.RichTextBox RTOriginalFile 
      Height          =   11055
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   6315
      _ExtentX        =   11139
      _ExtentY        =   19500
      _Version        =   393217
      BackColor       =   16777152
      HideSelection   =   0   'False
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"Matsdc_File_Load.frx":081E
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTConvertedFile 
      Height          =   11055
      Left            =   6480
      TabIndex        =   6
      Top             =   480
      Width           =   6480
      _ExtentX        =   11430
      _ExtentY        =   19500
      _Version        =   393217
      BackColor       =   16777152
      HideSelection   =   0   'False
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"Matsdc_File_Load.frx":089E
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblcol1 
      Height          =   240
      Left            =   8760
      TabIndex        =   4
      Tag             =   "FIXED"
      Top             =   11640
      Width           =   855
   End
   Begin VB.Label lblCol 
      Height          =   240
      Left            =   1200
      TabIndex        =   3
      Tag             =   "FIXED"
      Top             =   11640
      Width           =   855
   End
   Begin VB.Label lblrow1 
      Height          =   240
      Left            =   7560
      TabIndex        =   1
      Top             =   11640
      Width           =   1095
   End
   Begin VB.Label lblRow 
      Height          =   240
      Left            =   30
      TabIndex        =   0
      Top             =   11640
      Width           =   1095
   End
End
Attribute VB_Name = "Matsdc_File_Load"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const migrationPath = "Output-prj\Languages\"
'silvia 12/9/2008 gestione find
Dim Parola As String
Dim Idxpos As Long
Dim ConvType As String
Dim convertedName As String


Private Sub cmdFind_Click()
  Find
End Sub

Private Sub cmdFind2_Click()
   Find2
End Sub

Private Sub CmdSave_Click()
  Dim risp As Integer
  risp = MsgBox("Do you want to save file" & vbCrLf & convertedName & "?", vbYesNo + vbQuestion + vbDefaultButton2, "Converted Items")
  If risp = vbYes Then
    RTConvertedFile.SaveFile (convertedName), 1
    CmdSave.Enabled = False
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

'silvia 12/9/2008
Public Sub load_File(objNome As String, Optional objId As Long, Optional linea As Integer)
  Dim rs As Recordset
  Dim Name As String
  'La metto a livello di modulo perch� mi serve per il salva
'  Dim convertedName As String
  Dim PathMig As String
   
 'silvia 11/9/2008
  Dim idx As Long, i As Long
  Dim ch As String
  Dim Parola() As String
 
  On Error GoTo errHandler
  
  Me.Caption = "Converted Item:" & objNome
  
  RTOriginalFile.SelStart = 0
  RTOriginalFile.SelLength = 1
  RTOriginalFile.SelColor = vbBlue
  RTOriginalFile.SelLength = 0
  
  Set rs = Open_Recordset("Select Directory_Input,estensione,tipo From BS_Oggetti Where IdOggetto = " & objId)
  If rs.RecordCount Then
    Name = Crea_Directory_Progetto(rs!directory_input, m_fun.FnPathDef) & "\" & objNome
    If Len(rs!estensione) Then
      Name = Name & "." & rs!estensione
    End If
    If rs!Tipo = "ASM" Then
      ConvType = "Asm2Cobol"
    End If
    If rs!Tipo = "EZT" Then
      ConvType = "Ezt2Cobol"
    End If
    If rs!Tipo = "PLI" Or rs!Tipo = "INC" Then
      ConvType = "Pli2Cobol"
      objNome = IIf(rs!Tipo = "INC", objNome & "W", objNome) 'RSI
    End If
    PathMig = migrationPath & ConvType
  End If
  rs.Close
  
  RTOriginalFile.LoadFile Name
  
 'silvia 11/9/2008
  If linea <> 0 Then
    idx = 1
    Parola = Split(RTOriginalFile.text, vbCrLf)
    idx = RTOriginalFile.Find(Parola(linea - 1), idx)
    RTOriginalFile.SelStart = idx
    RTOriginalFile.SelLength = 60
    RTOriginalFile.SelColor = vbRed
    RTOriginalFile.SetFocus
  End If
  
  convertedName = m_fun.FnPathPrj & "\" & PathMig & "\" & objNome
  If fso.FileExists(convertedName & ".CBL") Then
    RTConvertedFile.LoadFile convertedName & ".CBL"
  Else
    RTConvertedFile.LoadFile convertedName
  End If
  
  Me.Show
  Exit Sub
errHandler:
  Resume Next
End Sub

Function Crea_Directory_Progetto(Directory As String, PathD As String) As String
  Dim WDollaro As Long
     
  WDollaro = InStr(1, Directory, "$")
  If WDollaro <> 0 Then
    Crea_Directory_Progetto = PathD & Trim(Mid(Directory, WDollaro + 1))
  End If
End Function

Sub GetSelRow()
  Me.lblRow.Caption = "Row: " & CStr(GetLineNum(Me.RTOriginalFile) + 1)
  Me.lblrow1.Caption = "Row: " & CStr(GetLineNum(Me.RTConvertedFile) + 1)
  Me.lblCol.Caption = "Col: " & CStr(GetColPos(Me.RTOriginalFile) + 1)
  Me.lblcol1.Caption = "Col: " & CStr(GetColPos(Me.RTConvertedFile) + 1)
End Sub

Private Sub RTConvertedFile_KeyDown(KeyCode As Integer, Shift As Integer)
  Idxpos = Idxpos + Len(Parola)
  
  If KeyCode = vbKeyF3 Then
    Idxpos = RTConvertedFile.Find(Parola, Idxpos)
  End If
  
  If Idxpos = -1 Then
    MsgBox "Search terminated...", vbInformation, "Find"
    Idxpos = 1
    RTConvertedFile.SelStart = 0
    RTConvertedFile.SelLength = 0
  End If
  
  If KeyCode <> vbKeyF3 Then
    CmdSave.Enabled = True
  End If
  
End Sub

Private Sub RTConvertedFile_KeyPress(KeyAscii As Integer)
  GetSelRow
End Sub

Private Sub RTConvertedFile_KeyUp(KeyCode As Integer, Shift As Integer)
  GetSelRow
End Sub

Private Sub RTConvertedFile_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  GetSelRow
End Sub

Private Sub RTOriginalFile_Click()
  GetSelRow
End Sub

Private Sub RTOriginalFile_DblClick()
  Dim numriga As Long, idx As Long, idxend As Long, i As Long
  Dim riga() As String
  
  'If ConvType = "Asm2Cobol" Then
    RTOriginalFile.SetFocus
    numriga = GetLineNum(Me.RTOriginalFile) + 1
    If numriga <> 0 Then
      idx = 1
      idxend = 1
      idx = RTConvertedFile.Find(" " & Format(numriga, "00000"), idx)
      riga = Split(Mid(RTConvertedFile.text, idx + 8), vbCrLf)
      For i = 0 To UBound(riga)
        If Len(riga(i)) > 128 And InStr(riga(i), " " & Format(numriga, "00000")) = 0 Then
          idxend = RTConvertedFile.Find(riga(i), idxend)
          Exit For
        End If
      Next
      
      If idx > 0 And idxend > 0 Then
        RTConvertedFile.SelStart = idx - 126
        If idxend > idx Then
          RTConvertedFile.SelLength = idxend - idx + 126
        Else
          RTConvertedFile.SelLength = 3
        End If
      Else
        RTConvertedFile.SelLength = 0
      End If
    End If
  'End If
End Sub

Private Sub RTOriginalFile_KeyDown(KeyCode As Integer, Shift As Integer)
  Idxpos = Idxpos + Len(Parola)
  'Gestione ricerca tramite apertura form TxtFind
 ' parola = Trim(RTOriginalFile.SelText)
 ' m_fun.FindTxt parola
 ' parola = m_fun.txtModif
  'gestione Ctrl + F
  'If KeyCode = vbKeyControl And Shift = 2 Then
  '  Find
  'End If
  
  If KeyCode = vbKeyF3 Then
    Idxpos = RTOriginalFile.Find(Parola, Idxpos)
  End If
  
  If Idxpos = -1 Then
    MsgBox "Search terminated...", vbInformation, "Find"
    Idxpos = 1
    RTOriginalFile.SelStart = 0
    RTOriginalFile.SelLength = 0
  End If
End Sub

Private Sub RTOriginalFile_KeyPress(KeyAscii As Integer)
  GetSelRow
End Sub

Private Sub RTOriginalFile_KeyUp(KeyCode As Integer, Shift As Integer)
  GetSelRow
End Sub

Sub Find()
  'Gestione ricerca tramite apertura form TxtFind
  Idxpos = Idxpos + Len(Parola)
  Parola = Trim(RTOriginalFile.SelText)
  m_fun.FindTxt Parola
  Parola = m_fun.txtModif
  Idxpos = RTOriginalFile.Find(Parola, Idxpos)
  RTOriginalFile.SetFocus
End Sub
Sub Find2()
  'Gestione ricerca tramite apertura form TxtFind
  Idxpos = Idxpos + Len(Parola)
  Parola = Trim(RTConvertedFile.SelText)
  m_fun.FindTxt Parola
  Parola = m_fun.txtModif
  Idxpos = RTConvertedFile.Find(Parola, Idxpos)
  RTConvertedFile.SetFocus
End Sub
Private Sub RTOriginalFile_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  GetSelRow
End Sub

Private Sub RTOriginalFile_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  GetSelRow
End Sub

