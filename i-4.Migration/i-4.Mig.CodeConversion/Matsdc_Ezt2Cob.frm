VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Matsdc_Ezt2Cob 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Easytrieve Conversion"
   ClientHeight    =   6720
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   8175
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   8175
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdConvert 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   60
      Picture         =   "Matsdc_Ezt2Cob.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   15
      Tag             =   "fixed"
      ToolTipText     =   "Convert"
      Top             =   120
      Width           =   550
   End
   Begin VB.CommandButton CmdErase 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   555
      Left            =   615
      Picture         =   "Matsdc_Ezt2Cob.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   14
      Tag             =   "fixed"
      ToolTipText     =   "Erase"
      Top             =   120
      Width           =   550
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selection"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   975
      Left            =   30
      TabIndex        =   11
      Top             =   750
      Width           =   3885
      Begin VB.OptionButton OptAll 
         Caption         =   "All Easytrieve Programs"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   270
         Width           =   2655
      End
      Begin VB.OptionButton OptSel 
         Caption         =   "Only Selected Easytrieve Program"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Value           =   -1  'True
         Width           =   2775
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Preferences"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1005
      Left            =   3990
      TabIndex        =   8
      Top             =   750
      Width           =   2055
      Begin VB.CheckBox ChSection 
         Caption         =   "Section"
         Height          =   255
         Left            =   1080
         TabIndex        =   17
         Top             =   300
         Value           =   1  'Checked
         Width           =   855
      End
      Begin VB.OptionButton optCobol 
         Caption         =   "Cobol"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   300
         Value           =   -1  'True
         Width           =   915
      End
      Begin VB.OptionButton optVB 
         Enabled         =   0   'False
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   675
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1005
      Left            =   6060
      TabIndex        =   4
      Top             =   750
      Width           =   2055
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   1845
         _ExtentX        =   3254
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBarK 
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   450
         Width           =   1845
         _ExtentX        =   3254
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBar01 
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   660
         Width           =   1845
         _ExtentX        =   3254
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Easytrieve Selected"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4935
      Left            =   30
      TabIndex        =   2
      Top             =   1740
      Width           =   3885
      Begin MSComctlLib.ListView lswFileSel 
         Height          =   4515
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   3645
         _ExtentX        =   6429
         _ExtentY        =   7964
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Program"
            Object.Width           =   1588
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Notes "
            Object.Width           =   1588
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Locs"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Directory"
            Object.Width           =   1588
         EndProperty
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Log"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4935
      Left            =   3990
      TabIndex        =   0
      Top             =   1740
      Width           =   4125
      Begin MSComctlLib.ListView LswLog 
         Height          =   915
         Left            =   120
         TabIndex        =   1
         Top             =   270
         Width           =   3885
         _ExtentX        =   6853
         _ExtentY        =   1614
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Log"
            Object.Width           =   26458
         EndProperty
      End
      Begin MSComctlLib.ListView LswSegnalazioni 
         Height          =   3495
         Left            =   120
         TabIndex        =   16
         Top             =   1320
         Width           =   3885
         _ExtentX        =   6853
         _ExtentY        =   6165
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Log"
            Object.Width           =   26458
         EndProperty
      End
   End
End
Attribute VB_Name = "Matsdc_Ezt2Cob"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Conversion()
  Dim sql As String
  Dim i As Long
  
  LswLog.ListItems.Clear
  LswSegnalazioni.ListItems.Clear
  PBar.Max = 10
  PBar.Value = 0
  PBarK.Max = 10
  PBarK.Value = 0
  DoEvents

  LswLog.ListItems.Add , , Time & " - Starting Analysis"
  LswLog.Refresh
  
  If OptAll.Value Then
    If lswFileSel.ListItems.Count > 0 Then
      PBar01.Max = lswFileSel.ListItems.Count
      
      For i = 1 To lswFileSel.ListItems.Count
        LswLog.ListItems.Add , , Time & " - Source Migration: " & lswFileSel.ListItems(i)
        LswLog.Refresh
        LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible

        If optCobol.Value Then
          ' Mauro 25/08/2008
          'm_pars.ConvertEasyTrieve lswFileSel.ListItems(i).tag, "COBOL"
          Set TsCollectionParam = New Collection
          TsCollectionParam.Add lswFileSel.ListItems(i).tag
          TsCollectionParam.Add "COBOL"
          'TsCollectionParam.Add LswLog
          TsCollectionParam.Add LswSegnalazioni
          'tilvia 17-04-2009 flag per la scelta di Section o Paragrafi
          TsCollectionParam.Add ChSection
          
          m_pars.ConvertEasyTrieve TsCollectionParam
        Else
          'Seconda opzione
        End If
        load_EZT_Segnalazioni lswFileSel.ListItems(i).tag
        PBar01.Value = PBar01.Value + 1
      Next i
    End If
  Else 'converte solo l'elemento selezionato
    PBar01.Max = lswFileSel.ListItems.Count
    For i = 1 To lswFileSel.ListItems.Count
      If lswFileSel.ListItems(i).Selected Then
        LswLog.ListItems.Add , , Time & " - Source Migration: " & lswFileSel.ListItems(i)
        LswLog.Refresh
        LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
        
        If optCobol.Value Then
          ' Mauro 25/08/2008
          'm_pars.ConvertEasyTrieve lswFileSel.ListItems(i).tag, "COBOL"
          Set TsCollectionParam = New Collection
          TsCollectionParam.Add lswFileSel.ListItems(i).tag
          TsCollectionParam.Add "COBOL"
          'TsCollectionParam.Add LswLog
          TsCollectionParam.Add LswSegnalazioni
         'tilvia 17-04-2009 flag per la scelta di Section o Paragrafi
          TsCollectionParam.Add ChSection
          m_pars.ConvertEasyTrieve TsCollectionParam
        Else
          'Seconda opzione
        End If
        load_EZT_Segnalazioni lswFileSel.ListItems(i).tag
        
      End If
      PBar01.Value = PBar01.Value + 1
    Next i
  End If
  
  PBar01.Value = PBar01.Max
  
  LswLog.ListItems.Add , , Time & " - Source Migration Terminated"
  LswLog.Refresh
  LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
   
  PBar.Value = 0
  PBarK.Value = 0
  PBar01.Value = 0
End Sub

Private Sub CmdConvert_Click()
  MousePointer = vbHourglass
  If lswFileSel.ListItems.Count > 0 Then Conversion
  MousePointer = vbNormal
End Sub

Private Sub Form_Load()
  ChSection = 1
  lswFileSel.ColumnHeaders(1).Width = lswFileSel.Width / 4
  lswFileSel.ColumnHeaders(2).Width = (lswFileSel.Width / 4) * 2
  lswFileSel.ColumnHeaders(3).Width = (lswFileSel.Width / 4) * 2

  Carica_Lista_EasyTrieve
  
  optCobol = True
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Public Sub Carica_Lista_EasyTrieve()
  Dim rs As Recordset
  Dim listx As ListItem
 
  lswFileSel.ListItems.Clear

  Set rs = m_fun.Open_Recordset("Select DISTINCT IdOggetto,Nome, directory_Input, Notes, NumRighe From BS_Oggetti Where " & _
                                "Tipo in ('EZT', 'EZM') Order By Nome")

  While Not rs.EOF
    Set listx = lswFileSel.ListItems.Add(, , rs!Nome)
    listx.ListSubItems.Add = IIf(IsNull(rs!Notes) = True, "", rs!Notes)
    listx.ListSubItems.Add = rs!NumRighe
    listx.ListSubItems.Add = rs!directory_input
    listx.tag = rs!IdOggetto
    rs.MoveNext
  Wend
  rs.Close
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  lswFileSel.ColumnHeaders(1).Width = lswFileSel.Width / 4
  lswFileSel.ColumnHeaders(2).Width = (lswFileSel.Width / 4) * 2
  lswFileSel.ColumnHeaders(3).Width = (lswFileSel.Width / 4) * 2
End Sub

Private Sub lswFileSel_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswFileSel.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswFileSel.SortOrder = Order
  lswFileSel.SortKey = key - 1
  lswFileSel.Sorted = True
End Sub

Private Sub lswFileSel_DblClick()
  If lswFileSel.ListItems.Count Then
    Matsdc_File_Load.load_File lswFileSel.SelectedItem, lswFileSel.SelectedItem.tag
    Matsdc_File_Load.Show
  End If
End Sub

'''''''''''''''''''''''''
' Gestione Segnalazioni
' silvia 19/02/2009
'''''''''''
Sub load_EZT_Segnalazioni(IdOggetto As Long)
  Dim tb As Recordset
  On Error GoTo ErrorHandler
  
  Set tb = m_fun.Open_Recordset("SELECT * FROM MgEZT_Segnalazioni where IdOggetto = " & IdOggetto & " order by riga")
  Do Until tb.EOF
    LswSegnalazioni.ListItems.Add , , "[" & tb!tipoMsg & "] [" & Format(tb!Operatore, "_____") & "] [" & Format(tb!riga, "00000") & "] - " & tb!Descrizione
    tb.MoveNext
  Loop
  tb.Close
  Exit Sub
ErrorHandler:
  MsgBox Err.Number & " " & Err.Description, vbExclamation + vbOKOnly, Menu.TsNameProdotto
End Sub

Private Sub lswFileSel_ItemClick(ByVal Item As MSComctlLib.ListItem)
  LswSegnalazioni.ListItems.Clear
  load_EZT_Segnalazioni Item.tag
End Sub
