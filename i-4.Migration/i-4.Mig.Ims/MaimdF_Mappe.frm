VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MaimdF_Mappe 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Porting from MFS to BMS"
   ClientHeight    =   5340
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8940
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5340
   ScaleWidth      =   8940
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   336
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   8940
      _ExtentX        =   15769
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "MFS2BMS"
            Object.ToolTipText     =   "MFS 2 BMS - Start"
            ImageKey        =   "Macro"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Select Object for Analysis"
      ForeColor       =   &H00000080&
      Height          =   3135
      Left            =   0
      TabIndex        =   6
      Top             =   450
      Width           =   3435
      Begin MSComctlLib.TreeView TVSelObj 
         Height          =   2655
         Left            =   120
         TabIndex        =   7
         Top             =   300
         Width           =   3165
         _ExtentX        =   5583
         _ExtentY        =   4683
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   529
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Analysis Function"
      ForeColor       =   &H00000080&
      Height          =   1725
      Left            =   0
      TabIndex        =   4
      Top             =   3600
      Width           =   3435
      Begin VB.ListBox LstFunction 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   735
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   5
         Top             =   330
         Width           =   3135
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Error Log"
      ForeColor       =   &H00000080&
      Height          =   1725
      Left            =   3480
      TabIndex        =   3
      Top             =   3570
      Width           =   5385
      Begin RichTextLib.RichTextBox RtErr 
         Height          =   1335
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   5115
         _ExtentX        =   9022
         _ExtentY        =   2355
         _Version        =   393217
         BackColor       =   -2147483624
         ReadOnly        =   -1  'True
         TextRTF         =   $"MaimdF_Mappe.frx":0000
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Activity Log"
      ForeColor       =   &H00000080&
      Height          =   3135
      Left            =   3480
      TabIndex        =   0
      Top             =   450
      Width           =   5385
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   255
         Left            =   90
         TabIndex        =   1
         Top             =   2790
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ListView LvObject 
         Height          =   2445
         Left            =   120
         TabIndex        =   2
         Top             =   300
         Width           =   5115
         _ExtentX        =   9022
         _ExtentY        =   4313
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "ID"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Object"
            Object.Width           =   6703
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "St.Time"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "End Time"
            Object.Width           =   2540
         EndProperty
      End
      Begin RichTextLib.RichTextBox RTBR 
         Height          =   972
         Left            =   2280
         TabIndex        =   10
         Top             =   1560
         Visible         =   0   'False
         Width           =   2268
         _ExtentX        =   3995
         _ExtentY        =   1720
         _Version        =   393217
         Enabled         =   -1  'True
         TextRTF         =   $"MaimdF_Mappe.frx":0082
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   3345
      Top             =   2430
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaimdF_Mappe.frx":0108
            Key             =   "Macro"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MaimdF_Mappe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
   'SetParent Me.hWnd, m_ImsDll_DC.ImParent
   
   m_fun.AddActiveWindows Me
   m_fun.FnActiveWindowsBool = True
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  On Error Resume Next
   
  Select Case Button.key
    Case "MFS2BMS"
      ReDim mEqu(0)
      Screen.MousePointer = vbHourglass
      AttivaMigrMFS
      Screen.MousePointer = vbNormal
   End Select
End Sub

Sub AttivaMigrMFS()
  Dim k As Integer, K1 As Integer
  Dim SwSel As Boolean
  'buttare SwSel
  Dim Rtb As Recordset
  Dim wSql As String
  Dim SwCopyContr As Boolean, SwMapsContr As Boolean, firstsource As Boolean, trovato As Boolean
  Dim startTime As Single, endTime As Single
  
  RTErr.text = ""
  LvObject.ListItems.Clear
  
  SwCopyContr = LstFunction.Selected(1)
  SwMapsContr = LstFunction.Selected(0)

  If Not (SwCopyContr Or SwMapsContr) Then
   AggLog "No command selected", RTErr
   Exit Sub
  End If
   
  LvObject.ListItems.Clear
   
  If TVSelObj.Nodes(1).Checked Then
      If m_ImsDll_DC.ImObjList.ListItems.Count = 0 Then
        AggLog "No source selected", RTErr
        Exit Sub
      End If
      PBar.Value = 0
      PBar.Max = 1
      For K1 = 1 To m_ImsDll_DC.ImObjList.ListItems.Count
         If m_ImsDll_DC.ImObjList.ListItems(K1).Selected Then
           PBar.Max = PBar.Max + 1
         End If
      Next K1
      PBar.Max = PBar.Max - 1
      
      For K1 = 1 To m_ImsDll_DC.ImObjList.ListItems.Count
         If m_ImsDll_DC.ImObjList.ListItems(K1).Selected Then
            GbIdOggetto = Val(m_ImsDll_DC.ImObjList.ListItems(K1).text)
            Set Rtb = Open_Recordset("select * from Bs_oggetti where IdOggetto = " & GbIdOggetto & " and tipo = 'MFS' ")
            If Rtb.RecordCount > 0 Then
              trovato = True
                With LvObject.ListItems.Add(, , Format(Rtb!IdOggetto, "000000"))
                   .SubItems(1) = Rtb!nome
     '              .SubItems(2) = TbOggetti!Tipo
                   startTime = Timer
                   .SubItems(2) = Format(Now, "HH:MM:SS")
                   PBar.Value = PBar.Value + 1
                   PBar.Refresh
                   LvObject.Refresh
                  LvObject.ListItems(LvObject.ListItems.Count).EnsureVisible
                  
                  MigraMFS SwCopyContr, SwMapsContr
                  
                  endTime = Timer
                  endTime = endTime - startTime
                 .SubItems(3) = FormatX(endTime)
             End With
          End If
          If Not trovato Then
            AggLog "No valid MFS source in your selection", RTErr
          End If
          LvObject.Refresh
          LvObject.ListItems(LvObject.ListItems.Count).EnsureVisible
       End If
       
     Next K1
     Exit Sub
    End If
    
    wSql = "Select * from bs_Oggetti where tipo = 'MFS' "
    Set Rtb = Open_Recordset(wSql)
    If Rtb.RecordCount = 0 Then
      Rtb.Close
      AggLog "No objects to process...", RTErr
      Exit Sub
    End If
    Rtb.MoveLast
    PBar.Max = Rtb.RecordCount
    PBar.Value = 0
    Rtb.MoveFirst
    While Not Rtb.EOF
      GbIdOggetto = Rtb!IdOggetto
         With LvObject.ListItems.Add(, , Format(Rtb!IdOggetto, "000000"))
             .SubItems(1) = Rtb!nome
'             .SubItems(2) = rTb!Tipo
             startTime = Timer
             .SubItems(2) = Format(Now, "HH:MM:SS")
             PBar.Value = PBar.Value + 1
             PBar.Refresh
             LvObject.Refresh
             LvObject.ListItems(LvObject.ListItems.Count).EnsureVisible
             '*************************
             MigraMFS SwCopyContr, SwMapsContr
             '*************************
             
             endTime = Timer
             endTime = endTime - startTime
            .SubItems(3) = FormatX(endTime)
          End With
          LvObject.Refresh
          LvObject.ListItems(LvObject.ListItems.Count).EnsureVisible
      Rtb.MoveNext
    Wend
  AggLog "Porting from MFS to BMS is terminated", RTErr
End Sub

Sub Resize()
  On Error Resume Next
  Me.Move m_ImsDll_DC.ImLeft, m_ImsDll_DC.ImTop, m_ImsDll_DC.ImWidth, m_ImsDll_DC.ImHeight
  Frame2.Move 30, 400, Frame2.Width, (m_ImsDll_DC.ImHeight - 400 - Toolbar1.Height) / 3 * 2
  TVSelObj.Move 120, 240, Frame2.Width - 240, Frame2.Height - 360
  Frame1.Move 30, 430 + Frame2.Height, Frame1.Width, ((m_ImsDll_DC.ImHeight - 340) / 3) - 150
  LstFunction.Move 120, 240, Frame1.Width - 240, Frame1.Height - 300
  Frame4.Move Frame4.Left, Frame2.Top, m_ImsDll_DC.ImWidth - Frame4.Left - 120, Frame2.Height
  LvObject.Move 120, 240, Frame4.Width - 240, Frame4.Height - 420 - PBar.Height
  PBar.Move LvObject.Left, LvObject.Top + LvObject.Height + 30, LvObject.Width, PBar.Height
  Frame3.Move Frame3.Left, Frame1.Top, m_ImsDll_DC.ImWidth - Frame3.Left - 120, Frame1.Height
  RTErr.Move 120, 240, Frame3.Width - 240, Frame3.Height - 360

End Sub

Private Sub Form_Activate()
  On Error Resume Next
   
  ' voci per le opzion di generazione
  LstFunction.AddItem "Generate Source BMS"
  LstFunction.Selected(LstFunction.ListCount - 1) = True
  LstFunction.AddItem "Generate Copy Books"
  
  TVSelObj.Nodes.Add , , "SEL", "Selected Object"
  TVSelObj.Nodes.Add , , "SRC", "All MFS Maps for IMS"
  
  TVSelObj.Nodes(1).Checked = True

  ' Resize

  load_equMaps
End Sub

Private Sub Form_Unload(Cancel As Integer)
  If m_ImsDll_DC.ImTipoFinIm <> "" Then
    m_ImsDll_DC.ImFinestra.ListItems.Add , , "ImsDll: Mfs 2 BMS Window Unloaded at " & Time
  End If
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
End Sub

Private Sub TVSelObj_NodeCheck(ByVal Node As MSComctlLib.Node)
  'per scongiurare la selezione multipla
  Select Case Node.key
    Case "SEL"
      If TVSelObj.Nodes(1).Checked Then
        TVSelObj.Nodes(2).Checked = False
      Else
        TVSelObj.Nodes(2).Checked = True
      End If
    Case "SRC"
      If TVSelObj.Nodes(2).Checked Then
        TVSelObj.Nodes(1).Checked = False
      Else
        TVSelObj.Nodes(1).Checked = True
      End If
  End Select
End Sub
