Attribute VB_Name = "MaimdM_00"
Option Explicit

Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Public Declare Function SetFocusWnd Lib "user32.dll" Alias "SetFocus" (ByVal hwnd As Long) As Long
  
Global m_ImsDll_DC As New MaimdC_Menu
'SQ merge Global m_fun As New MaFndC_Funzioni
Global TbOggetti As Recordset

Global GbIdOggetto As Long
Global GbFileInput As String
Global GbFileOutput As String
Global GbNumRec As Double

Global ImMenu() As MaM1

'SQ merge Global SwMenu As Boolean

Type t_trsMaps
  mfs As String
  bms As String
End Type

Type t_trsEqu
  Alias As String
  cmd As String
End Type

Public mTrsMaps() As t_trsMaps
Public mEqu() As t_trsEqu

Type TDfld
  name As String
  Initial As String
  Row As Integer
  Col As Integer
  Length As Integer
  Fill As String
  AttrHi As Boolean
  AttrProt As Boolean
  AttrMod As Boolean
  AttrOut As Boolean
  AttrNum As Boolean
  AttrNoDisp As Boolean
  AttrCur As Boolean
  Output As Boolean
  Input As Boolean
  RoutOut As String
  RoutIn As String
  Occurs As Integer
  isTranslated As Boolean
End Type

Type TDpageArea
  name As String
  DPageFill As String
  DPageCurs As String
  Dfld() As TDfld
End Type

Type TMapMfs
  mapset As String
  Rows As Integer
  Cols As Integer
  TypeDev As String
  TypeDiv As String
  Feat As String
  Page As String
  SysMsg As String
  MapIn As String
  MapOut As String
  PFK(24) As Boolean
  DpageArea() As TDpageArea
End Type

Global GbMapMfs As TMapMfs

Public TabIstr_DC As IstrDliInc
'SQ merge Public ImsDCIstr As IstrImsDC

Public Sub writelogmap(Riga As String)
  Open "c:\logmap" For Append As #1
  Print #1, Riga
  Close #1
End Sub

Public Function Carica_Menu_DC() As Boolean
  SwMenu = True
  
  'inizializza la dll rendendo visibili le variabili della classe anche alle form
  
  Carica_Menu_DC = True
  
  'crea il menu in memoria
  ReDim ImMenu(0)
  
  ReDim Preserve ImMenu(UBound(ImMenu) + 1)
  ImMenu(UBound(ImMenu)).Id = 500
  ImMenu(UBound(ImMenu)).Label = "IMS/DC->CICS"
  ImMenu(UBound(ImMenu)).ToolTipText = ""
  ImMenu(UBound(ImMenu)).Picture = ""
  ImMenu(UBound(ImMenu)).M1Name = ""
  ImMenu(UBound(ImMenu)).M1SubName = ""
  ImMenu(UBound(ImMenu)).M1Level = 0
  ImMenu(UBound(ImMenu)).M2Name = "<Root>"
  ImMenu(UBound(ImMenu)).M3Name = ""
  ImMenu(UBound(ImMenu)).M4Name = ""
  ImMenu(UBound(ImMenu)).Funzione = ""
  ImMenu(UBound(ImMenu)).DllName = "MaImdP_00"
  ImMenu(UBound(ImMenu)).TipoFinIm = ""
  
  If m_fun.Type_License <> LIC_REV_NODLIIMS And m_fun.Type_License <> LIC_REV_NOIMS And m_fun.Type_License <> LIC_REV_NOAGG Then
    '1) Sottobottone : Project --> Migrazione da Mfs a BMS
    ReDim Preserve ImMenu(UBound(ImMenu) + 1)
    ImMenu(UBound(ImMenu)).Id = 501
    ImMenu(UBound(ImMenu)).Label = "Mfs 2 BMS"
    ImMenu(UBound(ImMenu)).ToolTipText = ""
    ImMenu(UBound(ImMenu)).Picture = m_ImsDll_DC.ImImgDir & "\Maps_Mfs2bms_Disabled.ico"
    ImMenu(UBound(ImMenu)).PictureEn = m_ImsDll_DC.ImImgDir & "\Maps_Mfs2bms_Enabled.ico"
    ImMenu(UBound(ImMenu)).M1Name = ""
    ImMenu(UBound(ImMenu)).M1SubName = ""
    ImMenu(UBound(ImMenu)).M1Level = 0
    ImMenu(UBound(ImMenu)).M2Name = "IMS/DC->CICS"
    ImMenu(UBound(ImMenu)).M3Name = ""
    ImMenu(UBound(ImMenu)).M4Name = ""
    ImMenu(UBound(ImMenu)).Funzione = "Migr_Mfs2Bms"
    ImMenu(UBound(ImMenu)).DllName = "MaImdP_00"
    ImMenu(UBound(ImMenu)).TipoFinIm = "Immediata"
      
    '2) Sottobottone : Project --> Correttore comandi IMS
    ReDim Preserve ImMenu(UBound(ImMenu) + 1)
    ImMenu(UBound(ImMenu)).Id = 505
    ImMenu(UBound(ImMenu)).Label = "IMS Encapsulator"
    ImMenu(UBound(ImMenu)).ToolTipText = ""
    ImMenu(UBound(ImMenu)).Picture = m_ImsDll_DC.ImImgDir & "\Incapsulatore_Disabled.ico"
    ImMenu(UBound(ImMenu)).PictureEn = m_ImsDll_DC.ImImgDir & "\Incapsulatore_Enabled.ico"
    ImMenu(UBound(ImMenu)).M1Name = ""
    ImMenu(UBound(ImMenu)).M1SubName = ""
    ImMenu(UBound(ImMenu)).M1Level = 0
    ImMenu(UBound(ImMenu)).M2Name = "IMS/DC->CICS"
    ImMenu(UBound(ImMenu)).M3Name = ""
    ImMenu(UBound(ImMenu)).M4Name = ""
    ImMenu(UBound(ImMenu)).Funzione = "IMS_INCAPS"
    ImMenu(UBound(ImMenu)).DllName = "MaImdP_00"
    ImMenu(UBound(ImMenu)).TipoFinIm = ""
  End If
  
  If m_fun.Type_License <> LIC_REV_NODLIIMS And m_fun.Type_License <> LIC_REV_NOIMS And m_fun.Type_License <> LIC_REV_NOAGG Then
    '3) Sottobottone : Project --> Migratore comandi IMS verso CICS
    ReDim Preserve ImMenu(UBound(ImMenu) + 1)
    ImMenu(UBound(ImMenu)).Id = 510
    ImMenu(UBound(ImMenu)).Label = "IMS Routine Generator"
    ImMenu(UBound(ImMenu)).ToolTipText = ""
    ImMenu(UBound(ImMenu)).Picture = m_ImsDll_DC.ImImgDir & "\Ims2Mtp_Disabled.ico"
    ImMenu(UBound(ImMenu)).PictureEn = m_ImsDll_DC.ImImgDir & "\Ims2Mtp_Enabled.ico"
    ImMenu(UBound(ImMenu)).M1Name = ""
    ImMenu(UBound(ImMenu)).M1SubName = ""
    ImMenu(UBound(ImMenu)).M1Level = 0
    ImMenu(UBound(ImMenu)).M2Name = "IMS/DC->CICS"
    ImMenu(UBound(ImMenu)).M3Name = ""
    ImMenu(UBound(ImMenu)).M4Name = ""
    ImMenu(UBound(ImMenu)).Funzione = "IMS_GENROUT"
    ImMenu(UBound(ImMenu)).DllName = "MaImdP_00"
    ImMenu(UBound(ImMenu)).TipoFinIm = ""
  End If
    
End Function

Function Open_Recordset(SQL As String, Optional OpenMode As Long, Optional LockMode As Long) As Recordset
  Set Open_Recordset = New Recordset
  
  If OpenMode <> 0 Then
    If LockMode <> 0 Then
      Open_Recordset.Open SQL, m_ImsDll_DC.ImConnection, OpenMode, LockMode, adCmdText
    Else
      Open_Recordset.Open SQL, m_ImsDll_DC.ImConnection, OpenMode, adLockOptimistic, adCmdText
    End If
  Else
    If LockMode <> 0 Then
      Open_Recordset.Open SQL, m_ImsDll_DC.ImConnection, adOpenStatic, LockMode, adCmdText
    Else
      Open_Recordset.Open SQL, m_ImsDll_DC.ImConnection, adOpenStatic, adLockOptimistic, adCmdText
    End If
  End If
End Function

Private Function LeggiCommenti(pFileInput As String) As String
  Dim NFileIn As Integer, wRecIn As String
  
  NFileIn = FreeFile
  Open GbFileInput For Input As NFileIn
  Do Until EOF(NFileIn)
    Line Input #NFileIn, wRecIn
    If Left(wRecIn, 1) = "*" Or InStr(1, wRecIn, "PRINT NOGEN") Then
      If Not InStr(1, wRecIn, "PRINT NOGEN") > 0 Then
        LeggiCommenti = LeggiCommenti & wRecIn & vbCrLf
      End If
    Else
       Exit Do
    End If
  Loop
  Close NFileIn
End Function
'IRIS-DB: consistent change to manage multi-format MFS
Private Sub getCursorPos(cursor As String, cursorrow As String, cursorcol As String)
  Dim appocursor As String
  
  appocursor = cursor
  'elimino apertura parentesi
  Do While Left(appocursor, 1) = "("
    appocursor = Right(appocursor, Len(appocursor) - 1)
  Loop
  cursorrow = Left(appocursor, InStr(appocursor, ",") - 1)
  'elimino eventuali zeri
  Do While Left(cursorrow, 1) = "0"
    cursorrow = Right(cursorrow, Len(cursorrow) - 1)
  Loop
  ' elimino chiusura parentesi
  appocursor = Right(appocursor, Len(appocursor) - InStr(appocursor, ","))
  Do While Right(appocursor, 1) = ")"
    appocursor = Left(appocursor, Len(appocursor) - 1)
  Loop
  cursorcol = appocursor
  Do While Left(cursorcol, 1) = "0"
    cursorcol = Right(cursorcol, Len(cursorcol) - 1)
  Loop
End Sub

Function MigraMFS(GenCopyCont As Boolean, GenMapCont As Boolean) As Boolean
  Dim DirectoryOut As String, DirectoryCics As String
  Dim nFileOut As Integer
  Dim wRecOu As String
  Dim wstr As String
  Dim k3 As Integer
  Dim IndFld As Integer
  Dim wNome As String
  Dim pos4 As Integer
  ' AC
  Dim uboundDpage As Integer, Progressivo As String, LunRef As Integer
  Dim coltappo As Integer, rigatappo As Integer
  
  'AC 13/10/2006
  Dim FmtRecord  As Recordset, DpageRecordset As Recordset, MultiMaprec As Recordset
  Dim Cmprecord As Recordset
  Dim colInfo As New Collection, StrCommenti As String, i As Integer
  'Ac 24/10
  Dim Interline As Integer, RowOffset As Integer
  Dim colonna As Integer, Riga As Integer, IndFldBase As Integer, IndFldOccur As Integer, CountDO As Integer
  Dim rigabase As Integer, colonnabase As Integer, dfsize As String, cinitial As String
  Dim prevOrdinaleDo As Integer, boundcolonna As Integer, assigncursor As Boolean, firstfset As Boolean
  Dim cursor As String
  Dim poscolonna As Integer
  Dim isTranslated As Boolean
  Dim cursorrow As String, cursorcol As String
  
  Dim numdpage As Integer
  'IRIS-DB
  Dim wNameMapset As String
  Dim firstFmt As Boolean
  On Error GoTo EH
  
  'IRIS-DB print maps not supported for now
  Set FmtRecord = Open_Recordset("select * from PsDli_MFS where idOggetto = " & GbIdOggetto & " and instr(DevType,'3270P') > 1")
  If Not FmtRecord.EOF Then
    AggLog "MFS id " & GbIdOggetto & " containing printer maps, not supported yet", MaimdF_Mappe.RTErr
    FmtRecord.Close
    Exit Function
  End If
  FmtRecord.Close
  
  Set TbOggetti = Open_Recordset("select * from BS_oggetti where idoggetto = " & GbIdOggetto)
  If GenMapCont Then
    GbFileInput = m_ImsDll_DC.ImPathDef & Mid$(TbOggetti!Directory_Input, 2)

    colInfo.Add GbIdOggetto
    colInfo.Add TbOggetti!nome
    colInfo.Add " "
      
    If TbOggetti!estensione <> "" Then
      GbFileInput = GbFileInput & "\" & TbOggetti!nome & "." & TbOggetti!estensione
    Else
      GbFileInput = GbFileInput & "\" & TbOggetti!nome
    End If
    
    StrCommenti = LeggiCommenti(GbFileInput)
    
'IRIS-DB gestione mapset messa fuori ciclo
    wNome = MaimdM_Genrout.getEnvironmentParam(MaimdM_Genrout.RetrieveParameterValues("IMSDC-BMSFILENAME")(0), "mappe", colInfo)
    'Output file
    wstr = m_ImsDll_DC.ImNomeDB
    pos4 = InStr(wstr, ".")
    If pos4 > 0 Then wstr = Mid$(wstr, 1, pos4 - 1)
    GbFileOutput = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\cics\BMS\" & wNome & ".bms"
    DirectoryCics = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\cics"
    DirectoryOut = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\cics\BMS"
    If Dir(DirectoryCics, vbDirectory) = "" Then
      MkDir DirectoryCics
    End If
    If Dir(DirectoryOut, vbDirectory) = "" Then
      MkDir DirectoryOut
    End If
      
    nFileOut = FreeFile()
    Open GbFileOutput For Output As #nFileOut
    wRecOu = "*--------------------------------------------* "
    Print #nFileOut, wRecOu
    wRecOu = "* "
    Print #nFileOut, wRecOu
    wRecOu = "* IMS / DC   TO   CICS - MFS TO BMS(MAP)"
    Print #nFileOut, wRecOu
''''''      wRecOu = "* CONVERTED BY  i-4 s.r.l."
''''''      Print #nFileOut, wRecOu
''''''      wRecOu = "* DATE: " & Now
''''''      Print #nFileOut, wRecOu
    wRecOu = "* "
    Print #nFileOut, wRecOu
    wRecOu = "*--------------------------------------------* "
    Print #nFileOut, wRecOu
    wRecOu = "* "
    Print #nFileOut, wRecOu
    
    'AC commenti di testa
    wRecOu = StrCommenti
    Print #nFileOut, wRecOu
    '**************** eliminata lettura file
    firstFmt = True
    Set MultiMaprec = Open_Recordset("select * from PsDLI_MFS where idoggetto = " & GbIdOggetto)
    Do Until MultiMaprec.EOF
      colInfo.Remove (3)
      colInfo.Add MultiMaprec!fmtName
      
      'AC commenti div,dev,dpage originali
      'IRIS-DB: non � molto bello che riscorra la stessa tabella del ciclo principale, anche perch� allora il ciclo peincipale non � gestito correttamente se pu� avere pi� DPAGE per lo stesso FMT name
      '         visto che � solo per commenti, al momento elimino il ciclo interno come se ci fosse un solo DPAGE per FMT, ma il ciclo principale non funziona con multipli DPAGE a questo punto
''      Set FmtRecord = Open_Recordset("select * from PsDli_MFS where FMTName = '" & MultiMaprec!fmtName & "' order by DevType")
''      Do Until FmtRecord.EOF
''        wRecOu = vbCrLf & "*        DEV   TYPE=" & FmtRecord!DevType & vbCrLf _
''                        & "*              FEAT=" & FmtRecord!Feat & vbCrLf _
''                        & "*              SYSMSG=" & FmtRecord!SysMsg & vbCrLf _
''                        & "*        DIV   TYPE=" & FmtRecord!DivType & vbCrLf & vbCrLf
''        Set DpageRecordset = Open_Recordset("select distinct DPageName  from PsDli_MFSCmp where FMTName = '" & MultiMaprec!fmtName & "' and DevType='" & FmtRecord!DevType & "' and DPageName is not Null")
''        Do Until DpageRecordset.EOF
''          wRecOu = wRecOu & "*" & DpageRecordset!DPageName & Space(8 - Len(DpageRecordset!DPageName)) & "DPAGE CURSOR=" & FmtRecord!cursor & vbCrLf
''          cursor = FmtRecord!cursor
''          If Len(cursor) Then
''            getCursorPos cursor, cursorrow, cursorcol
''          End If
''          DpageRecordset.MoveNext
''        Loop
''        DpageRecordset.Close
''        FmtRecord.MoveNext
''      Loop
''      FmtRecord.Close
      wRecOu = vbCrLf & "*        DEV   TYPE=" & MultiMaprec!DevType & vbCrLf _
                      & "*              FEAT=" & MultiMaprec!Feat & vbCrLf _
                      & "*              SYSMSG=" & MultiMaprec!SysMsg & vbCrLf _
                      & "*        DIV   TYPE=" & MultiMaprec!DivType & vbCrLf & vbCrLf
      Set DpageRecordset = Open_Recordset("select distinct DPageName  from PsDli_MFSCmp where FMTName = '" & MultiMaprec!fmtName & "' and DevType='" & MultiMaprec!DevType & "' and DPageName is not Null")
      Do Until DpageRecordset.EOF
        wRecOu = wRecOu & "*" & DpageRecordset!DPageName & Space(8 - Len(DpageRecordset!DPageName)) & "DPAGE CURSOR=" & MultiMaprec!cursor & vbCrLf
        cursor = MultiMaprec!cursor
        If Len(cursor) Then
          getCursorPos cursor, cursorrow, cursorcol
        End If
        DpageRecordset.MoveNext
      Loop
      DpageRecordset.Close
         
      Print #nFileOut, wRecOu
      '****************
      
    'IRIS-DB: fuori ciclo
      If firstFmt Then
        wNameMapset = MaimdM_Genrout.getEnvironmentParam(RetrieveParameterValues("IMSDC-BMSMAPSET")(0), "mappe", colInfo)
        wRecOu = Mid$(wNome & Space$(7), 1, 7) & "  DFHMSD TYPE=MAP,MODE=INOUT,LANG=COBOL,"
        wRecOu = Mid$(wRecOu & Space$(71), 1, 71) & "X"
        Print #nFileOut, wRecOu
        wRecOu = Space$(15) & "STORAGE=AUTO,TIOAPFX=YES,CTRL=FREEKB "
        Print #nFileOut, wRecOu
      End If
      firstFmt = False
      ReDim GbMapMfs.DpageArea(0)
      ReDim GbMapMfs.DpageArea(0).Dfld(0)
      
      'Attenzione giro funzionava solo per versione 3270
      ' inserita pezza per prendere info nel caso non ci sia 3270 ma 3270P o altro
      'IRIS-DB: visto che la query non ha pi� la specifica sul DevType, diventa inutile, siamo gi� dentro al ciclo di PsDli_MFS
''      Set FmtRecord = Open_Recordset("select * from PsDli_MFS where FMTName = '" & MultiMaprec!fmtName & "'") ' and DevType = '(3270,2)'")
''      If Not FmtRecord.EOF Then
''        GbMapMfs.Feat = FmtRecord!Feat
''        'IRIS-DB GbMapMfs.mapset = FmtRecord!FMTName
''        GbMapMfs.mapset = MaimdM_Genrout.getEnvironmentParam(RetrieveParameterValues("IMSDC-BMSMAPSET")(0), "mappe", colInfo)
''        GbMapMfs.Page = IIf(IsNull(FmtRecord!Page), "", FmtRecord!Page)
''        If GbMapMfs.Page = "" Then
''          dfsize = "(24,80)"
''          boundcolonna = 80
''        ElseIf Not InStr(GbMapMfs.Page, "SPACE") > 0 Then
''          dfsize = GbMapMfs.Page
''          boundcolonna = 80
''        Else ' sostituisco space con la massima lunghezza
''          dfsize = Replace(GbMapMfs.Page, "SPACE", maxMapCol(MultiMaprec!fmtName, boundcolonna))
''        End If
''      Else
''        FmtRecord.Close
''        Set FmtRecord = Open_Recordset("select * from PsDli_MFS where FMTName = '" & MultiMaprec!fmtName & "'")
''        If Not FmtRecord.EOF Then
''          GbMapMfs.Feat = FmtRecord!Feat
''          GbMapMfs.mapset = FmtRecord!fmtName
''        End If
''      End If
''      FmtRecord.Close
     'IRIS-DB GbMapMfs.mapset = FmtRecord!FMTName
      GbMapMfs.mapset = wNameMapset
      GbMapMfs.Feat = MultiMaprec!Feat
      GbMapMfs.Page = IIf(IsNull(MultiMaprec!Page), "", MultiMaprec!Page)
      If GbMapMfs.Page = "" Then
        dfsize = "(24,80)"
        boundcolonna = 80
      ElseIf Not InStr(GbMapMfs.Page, "SPACE") > 0 Then
        dfsize = GbMapMfs.Page
        boundcolonna = 80
      Else ' sostituisco space con la massima lunghezza
        dfsize = Replace(GbMapMfs.Page, "SPACE", maxMapCol(MultiMaprec!fmtName, boundcolonna))
      End If
      Set FmtRecord = Open_Recordset("select MSGname from PsDli_mfsMSG where " & _
                                     "FMTName = '" & MultiMaprec!fmtName & "' and DevType = 'INPUT'")
      If Not FmtRecord.EOF Then
        GbMapMfs.MapIn = FmtRecord!MSGName
      End If
      FmtRecord.Close
      Set FmtRecord = Open_Recordset("select MSGname from PsDli_mfsMSG where " & _
                                     "FMTName = '" & MultiMaprec!fmtName & "' and DevType = 'OUTPUT'")
      If Not FmtRecord.EOF Then
        GbMapMfs.MapOut = FmtRecord!MSGName
      End If
      FmtRecord.Close
        
      Set FmtRecord = Open_Recordset("select * from PsDli_MFScmp where " & _
                                     "FMTName = '" & MultiMaprec!fmtName & "' and DevType = '(3270,2)' order by OrdinaleCmp")
      If Not FmtRecord.EOF Then
        ReDim Preserve GbMapMfs.DpageArea(UBound(GbMapMfs.DpageArea) + 1)
        uboundDpage = UBound(GbMapMfs.DpageArea)
        numdpage = 1
        GbMapMfs.DpageArea(uboundDpage).name = FmtRecord!DPageName
      Else
        FmtRecord.Close
        Set FmtRecord = Open_Recordset("select * from PsDli_MFScmp where " & _
                                       "FMTName = '" & MultiMaprec!fmtName & "' order by OrdinaleCmp")
        If Not FmtRecord.EOF Then
          ReDim Preserve GbMapMfs.DpageArea(UBound(GbMapMfs.DpageArea) + 1)
          uboundDpage = UBound(GbMapMfs.DpageArea)
          numdpage = 1
          GbMapMfs.DpageArea(uboundDpage).name = FmtRecord!DPageName
        End If
      End If
      IndFld = 0
      ReDim Preserve GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld)
      Do Until FmtRecord.EOF
        If Not numdpage = FmtRecord!DPage Then
          ReDim Preserve GbMapMfs.DpageArea(UBound(GbMapMfs.DpageArea) + 1)
          assigncursor = False
          uboundDpage = UBound(GbMapMfs.DpageArea)
          numdpage = FmtRecord!DPage
          GbMapMfs.DpageArea(uboundDpage).name = FmtRecord!DPageName
          IndFld = 0
          ReDim Preserve GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld)
        End If
          
        If CInt(FmtRecord!Occurs) > 1 Then
          Set Cmprecord = Open_Recordset("select count(*) as countDO from PsDli_MFScmp where " & _
                                         "FMTName = '" & MultiMaprec!fmtName & "' and " & _
                                         "OrdinaleDo = " & FmtRecord!ordinaleDo)
          CountDO = Cmprecord!CountDO
          Cmprecord.Close
          If Not (prevOrdinaleDo = FmtRecord!ordinaleDo) Then
            IndFldBase = 0
          Else
            prevOrdinaleDo = FmtRecord!ordinaleDo
          End If
          prevOrdinaleDo = FmtRecord!ordinaleDo
          If IndFldBase = 0 Then
            IndFldBase = IndFld + 1
          Else
            IndFldBase = IndFldBase + 1
          End If
          colonnabase = FmtRecord!colonna
          colonna = colonnabase
          rigabase = FmtRecord!Riga
          Riga = rigabase
          Interline = IIf(IsNull(FmtRecord!Interlinea) Or FmtRecord!Interlinea = "", 1, FmtRecord!Interlinea)
          RowOffset = IIf(IsNull(FmtRecord!OffsetRiga) Or FmtRecord!OffsetRiga = "", 0, FmtRecord!OffsetRiga)
          
          For i = 1 To CInt(FmtRecord!Occurs)
            If RowOffset = 0 Then
              Riga = rigabase + Interline * (i - 1)
            Else ' c'� un offset di riga
              If colonna + RowOffset + FmtRecord!Lunghezza < boundcolonna Then
                If i = 1 Then ' sommo l'offset riga dal secondo giro
                  colonna = colonnabase
                Else
                  colonna = colonna + RowOffset
                End If
              ElseIf i > 1 Then ' cambio riga solo a partire dal secondo giro
                Riga = Riga + Interline
                colonna = colonnabase
              End If
            End If
            
            IndFld = IndFld + 1
            IndFldOccur = IndFldBase + CountDO * (i - 1)
            
            If UBound(GbMapMfs.DpageArea(uboundDpage).Dfld) < IndFldOccur Then
              ReDim Preserve GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur)
            End If
            
            'TRASLAZIONE A SINISTRA
            poscolonna = colonna
            If Not poscolonna = 1 Then
              If IndFldOccur > 1 Then ' il controllo successivo solo se non � il primo campo
                ' traslo se precedente traslato o primo della riga
                If (GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur - 1).isTranslated Or Not Riga = GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur - 1).Row) Then
                  poscolonna = poscolonna - 1
                  isTranslated = True
                Else ' controllo se cozza con il precedente
                  If (GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur - 1).Col + GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur - 1).Length) < poscolonna - 1 Then
                    colonna = colonna - 1
                    isTranslated = True
                  Else
                    isTranslated = False
                  End If
                End If
              Else
                ' traslo se primo campo in assoluto
                poscolonna = poscolonna - 1
                isTranslated = True
              End If
            Else '  alzo flag per il controllo del successivo
              isTranslated = False
            End If
              
            GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).Col = poscolonna
            GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).Initial = Replace(FmtRecord!InitValue, "&", "+")
            GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).Length = FmtRecord!Lunghezza
            GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).name = IIf(Left(FmtRecord!nome, 1) = "#", "", FmtRecord!nome)
            GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).Occurs = i + CInt(FmtRecord!Do_Suff) - 1
            GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).Row = Riga
            GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).isTranslated = isTranslated
            'Attributi
          'IRIS-DB: setta l'attributo di default su UNPROT come per lo IMS; i campi costanti sono poi overridati subito dopo
            GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).AttrProt = False
            If Left(FmtRecord!nome, 1) = "#" Then
              GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).AttrProt = True
            End If
            If InStr(FmtRecord!Attributo, "HI") > 0 Or InStr(FmtRecord!Attributo, "BRT") > 0 Then GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).AttrHi = True
            If InStr(FmtRecord!Attributo, "PROT") > 0 And Not InStr(FmtRecord!Attributo, "NOPROT") > 0 Then GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).AttrProt = True
            If InStr(FmtRecord!Attributo, "MOD") > 0 Then GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).AttrMod = True
            If InStr(FmtRecord!Attributo, "DRK") > 0 Then GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).AttrNoDisp = True
            If InStr(FmtRecord!Attributo, "NUM") > 0 And (Not InStr(FmtRecord!Attributo, "PROT") > 0 Or InStr(FmtRecord!Attributo, "NOPROT") > 0) Then
              GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).AttrNum = True
            End If
            If Not assigncursor Then
              If cursorrow = FmtRecord!Riga And cursorcol = FmtRecord!colonna Then
                GbMapMfs.DpageArea(uboundDpage).Dfld(IndFldOccur).AttrCur = True
                assigncursor = True
              End If
            End If
          Next i
        Else
          IndFldBase = 0
          IndFld = IndFld + 1
          colonna = FmtRecord!colonna
          If Not colonna = 1 Then
            If IndFld > 1 Then ' il controllo successivo solo se non � il primo campo
              ' traslo se precedente traslato o primo della riga
              If (GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld - 1).isTranslated Or Not FmtRecord!Riga = GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld - 1).Row) Then
                colonna = colonna - 1
                isTranslated = True
              Else ' controllo se cozza con il precedente
                If (GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld - 1).Col + GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld - 1).Length) < colonna - 1 Then
                  colonna = colonna - 1
                  isTranslated = True
                Else
                  isTranslated = False
                End If
              End If
            Else
              ' traslo se primo campo in assoluto
              colonna = colonna - 1
              isTranslated = True
            End If
          Else ' alzo flag per il controllo del successivo
            isTranslated = False
          End If

          ReDim Preserve GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld)
          GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).Col = colonna
          GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).Initial = IIf(IsNull(FmtRecord!InitValue), "", Replace(FmtRecord!InitValue, "&", "+"))
          GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).Length = FmtRecord!Lunghezza
          GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).name = IIf(Left(FmtRecord!nome, 1) = "#", "", FmtRecord!nome)
          GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).Occurs = 0
          GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).Row = FmtRecord!Riga
          GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).isTranslated = isTranslated
          
          'IRIS-DB: setta l'attributo di default su UNPROT come per lo IMS; i campi costanti sono poi overridati subito dopo
          GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).AttrProt = False
          If Left(FmtRecord!nome, 1) = "#" Then
            GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).AttrProt = True
          End If
          
          'Attributi
          If InStr(FmtRecord!Attributo, "HI") > 0 Or InStr(FmtRecord!Attributo, "BRT") > 0 Then GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).AttrHi = True
          If InStr(FmtRecord!Attributo, "PROT") > 0 And Not InStr(FmtRecord!Attributo, "NOPROT") > 0 Then GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).AttrProt = True
          If InStr(FmtRecord!Attributo, "MOD") > 0 Then GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).AttrMod = True
          If InStr(FmtRecord!Attributo, "DRK") > 0 Then GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).AttrNoDisp = True
          If InStr(FmtRecord!Attributo, "NUM") > 0 And (Not InStr(FmtRecord!Attributo, "PROT") > 0 Or InStr(FmtRecord!Attributo, "NOPROT") > 0) Then
            GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).AttrNum = True
          End If
          If Not assigncursor Then
            If cursorrow = FmtRecord!Riga And cursorcol = FmtRecord!colonna Then
              GbMapMfs.DpageArea(uboundDpage).Dfld(IndFld).AttrCur = True
              assigncursor = True
            End If
          End If
        End If
        FmtRecord.MoveNext
      Loop
      FmtRecord.Close
        
      'Ac occorre ciclare per tutti i dpage
      For k3 = 1 To UBound(GbMapMfs.DpageArea)
        firstfset = False
        If UBound(GbMapMfs.DpageArea) = 1 Then
          Progressivo = ""
          LunRef = 8
        Else
          If Not k3 = 10 Then
            Progressivo = k3
          Else
            Progressivo = 0
          End If
          LunRef = 7
        End If
         
        If GbMapMfs.DpageArea(k3).name = "" Then
          'IRIS-DB GbMapMfs.DpageArea(k3).name = GbMapMfs.mapset
          GbMapMfs.DpageArea(k3).name = MaimdM_Genrout.getEnvironmentParam(RetrieveParameterValues("IMSDC-BMSMAP")(0), "mappe", colInfo)
          If m_fun.FnNomeDB = "LandG.mty" Then 'IRIS-DB: pezza per evitare di riconsegnare tutti i programmi a L&G; da rimuovere in seguito
            If MultiMaprec.RecordCount = 1 Then
              GbMapMfs.DpageArea(k3).name = GbMapMfs.mapset & "R"
            End If
          End If
        End If
        GbMapMfs.DpageArea(k3).name = Left(GbMapMfs.DpageArea(k3).name, 7) 'IRIS-DB mapnames max 7 bytes
        wRecOu = GbMapMfs.DpageArea(k3).name _
        & "  DFHMDI SIZE=" & dfsize & " "
        Print #nFileOut, wRecOu
         
        '**** creazione campi
        rigatappo = -1
        coltappo = -1
        For IndFld = 1 To UBound(GbMapMfs.DpageArea(k3).Dfld)
          'AC  in precedenza col -1, correzione Lunghezza max 79?!
          ' Va bene per formato 24x80
          If GbMapMfs.DpageArea(k3).Dfld(IndFld).Col + GbMapMfs.DpageArea(k3).Dfld(IndFld).Length = boundcolonna + 1 Then
            Select Case GbMapMfs.DpageArea(k3).Dfld(IndFld).Col
              Case 2, boundcolonna
                GbMapMfs.DpageArea(k3).Dfld(IndFld).Col = GbMapMfs.DpageArea(k3).Dfld(IndFld).Col - 1
              Case Else
                GbMapMfs.DpageArea(k3).Dfld(IndFld).Length = GbMapMfs.DpageArea(k3).Dfld(IndFld).Length - 1
            End Select
          End If
            
          ' Tappo per il campo precedente
          If Not (rigatappo = GbMapMfs.DpageArea(k3).Dfld(IndFld).Row And (coltappo = GbMapMfs.DpageArea(k3).Dfld(IndFld).Col)) And rigatappo > 0 And Not GbMapMfs.DpageArea(k3).Dfld(IndFld - 1).name = "" Then
            wRecOu = Space(9)
            wRecOu = wRecOu & "DFHMDF POS=(" & Format(rigatappo, "00") & "," & Format(coltappo, "00") & "),LENGTH=001,ATTRB=(ASKIP,NORM)"
            Print #nFileOut, wRecOu
          End If
          If GbMapMfs.DpageArea(k3).Dfld(IndFld).name <> "" Then
            wRecOu = "* " & GbMapMfs.DpageArea(k3).Dfld(IndFld).name
            If GbMapMfs.DpageArea(k3).Dfld(IndFld).Occurs > 0 Then
              'IRIS-DB wRecOu = wRecOu & " (" & GbMapMfs.DpageArea(k3).Dfld(IndFld).Occurs & ")"
              wRecOu = wRecOu & "(" & GbMapMfs.DpageArea(k3).Dfld(IndFld).Occurs & ")"
            End If
            Print #nFileOut, wRecOu
            wRecOu = GbMapMfs.DpageArea(k3).Dfld(IndFld).name
            If GbMapMfs.DpageArea(k3).Dfld(IndFld).Occurs > 0 Then
              wRecOu = wRecOu & Format(GbMapMfs.DpageArea(k3).Dfld(IndFld).Occurs, "00")
              'metto il progressivo in posizione 5
              If Not Progressivo = "" Then ' sempre nel terzultimo
                'Mid$(wRecOu, Len(wRecOu) - 2, 1) = progressivo
                wRecOu = Left(wRecOu, Len(wRecOu) - 2) & Progressivo & Right(wRecOu, 2)
              End If
            ElseIf Not Progressivo = "" Then
              'caso campo singolo attacco il progressivo
              wRecOu = wRecOu & Progressivo
            End If
          Else
            wRecOu = Space(7)
          End If
          wRecOu = wRecOu & "  DFHMDF POS=(" & Format(GbMapMfs.DpageArea(k3).Dfld(IndFld).Row, "00") & "," & Format(GbMapMfs.DpageArea(k3).Dfld(IndFld).Col, "00") & "),"
          wRecOu = wRecOu & "LENGTH=" & Format(GbMapMfs.DpageArea(k3).Dfld(IndFld).Length, "000") & ",ATTRB=("
          wstr = ""
          
          'SG : ATTRIBUTI
          'IRIS-DB setta il default ad UNPROT se non � specificato PROT
          'IRIS-DB If GbMapMfs.DpageArea(k3).Dfld(IndFld).AttrProt Then wstr = wstr & ",ASKIP"
          If GbMapMfs.DpageArea(k3).Dfld(IndFld).AttrProt Then
            wstr = wstr & ",ASKIP"
          Else
            wstr = wstr & ",UNPROT"
          End If
          
          If GbMapMfs.DpageArea(k3).Dfld(IndFld).AttrMod Then
            wstr = wstr & ",FSET"
            ' se non ho trovato la pos del cursor
            ' al primo campo co FSET associo asnche l'attributo IC
          End If
          If GbMapMfs.DpageArea(k3).Dfld(IndFld).AttrHi Then wstr = wstr & ",BRT"
          If GbMapMfs.DpageArea(k3).Dfld(IndFld).AttrNum Then wstr = wstr & ",NUM"
          If GbMapMfs.DpageArea(k3).Dfld(IndFld).AttrNoDisp Then wstr = wstr & ",DRK"
          If GbMapMfs.DpageArea(k3).Dfld(IndFld).AttrCur Then wstr = wstr & ",IC"

          wRecOu = wRecOu & wstr & ")"
          wRecOu = Replace(wRecOu, "(,", "(")
          wRecOu = Replace(wRecOu, ",ATTRB=()", "")
          If GbMapMfs.DpageArea(k3).Dfld(IndFld).Initial <> "" Then
            wRecOu = Mid$(wRecOu & "," & Space$(71), 1, 71) & "X"
            Print #nFileOut, wRecOu
            If Len(GbMapMfs.DpageArea(k3).Dfld(IndFld).Initial) < 47 Then
              wRecOu = Space$(15) & "INITIAL='" & GbMapMfs.DpageArea(k3).Dfld(IndFld).Initial & "' "
            ElseIf Len(GbMapMfs.DpageArea(k3).Dfld(IndFld).Initial) = 47 Then
              wRecOu = Space$(15) & "INITIAL='" & GbMapMfs.DpageArea(k3).Dfld(IndFld).Initial & "X"
              Print #nFileOut, wRecOu
              wRecOu = Space$(15) & "' "
            Else
              ' ciclo per initial dei formati di stampa
              cinitial = GbMapMfs.DpageArea(k3).Dfld(IndFld).Initial
              wRecOu = Space$(15) & "INITIAL='" & Left(cinitial, 47) & "X"
              Print #nFileOut, wRecOu
              cinitial = Right(cinitial, Len(cinitial) - 47)
              Do While Len(cinitial) > 56
                wRecOu = Space$(15) & Left(cinitial, 56) & "X"
                Print #nFileOut, wRecOu
                cinitial = Right(cinitial, Len(cinitial) - 56)
              Loop
              If Len(cinitial) = 56 Then
                wRecOu = Space$(15) & cinitial & "X"
                Print #nFileOut, wRecOu
                wRecOu = Space$(15) & "' "
              Else
                wRecOu = Space$(15) & cinitial & "' "
              End If
            End If
          End If
          Print #nFileOut, wRecOu
          'tappo
          coltappo = GbMapMfs.DpageArea(k3).Dfld(IndFld).Col + GbMapMfs.DpageArea(k3).Dfld(IndFld).Length + 1
          rigatappo = GbMapMfs.DpageArea(k3).Dfld(IndFld).Row
          If coltappo > boundcolonna - 1 Then '+ 1 Then
            coltappo = -1
            rigatappo = -1
          End If
        Next IndFld
      Next k3
      '********* parte finale
      
      '  ** ultimo tappo se non ho superato riga 24
      If Not (rigatappo > 24 Or rigatappo = -1) And Not GbMapMfs.DpageArea(k3 - 1).Dfld(IndFld - 1).name = "" Then
        wRecOu = Space(9)
        wRecOu = wRecOu & "DFHMDF POS=(" & Format(rigatappo, "00") & "," & Format(coltappo, "00") & "),LENGTH=001,ATTRB=(ASKIP,NORM)"
        Print #nFileOut, wRecOu
      End If
        
      'IRIS-DB: spostato fuori ciclo
''      wRecOu = "* "
''      Print #nFileOut, wRecOu
''      wRecOu = Space$(9) & "DFHMSD TYPE=FINAL "
''      Print #nFileOut, wRecOu
''      wRecOu = Space$(9) & "END " & vbCrLf & "* "
''      Print #nFileOut, wRecOu
''      Close nFileOut
      
      If GenCopyCont Then
        GeneraCopyMfsAndmove MultiMaprec!fmtName
      End If
     
      MultiMaprec.MoveNext
    Loop
    MultiMaprec.Close
    wRecOu = "* "
    Print #nFileOut, wRecOu
    wRecOu = Space$(9) & "DFHMSD TYPE=FINAL "
    Print #nFileOut, wRecOu
    wRecOu = Space$(9) & "END " & vbCrLf & "* "
    Print #nFileOut, wRecOu
    Close nFileOut
  End If
  TbOggetti.Close 'IRIS-DB
  'IRIS-DB: altro sistema per scrivere un log, non funzionante
''''''  If Not cursor = "" And Not assigncursor Then
''''''    writelogmap wNome & ": corrispondenza cursore non trovata"
''''''  ElseIf cursor = "" Then
''''''    writelogmap wNome & ": nessun cursore indicato"
''''''  End If
  Exit Function
'IRIS-DB: gestione errore per chiudere il file correttamente
EH:
  Close nFileOut
  AggLog "MFS id " & GbIdOggetto & " generic error on migration in MigraMFS", MaimdF_Mappe.RTErr
End Function

Sub GeneraCopyMfsAndmove(pFMTname As String)
  Dim wOggetto As String
  Dim wstr As String
  Dim pos4 As Integer
  Dim Percorso As String, PercorsoCics As String
  Dim rs As Recordset
  
  MaimdF_Mappe.RTErr.text = ""
  AggLog "Starting Copy Map Generation", MaimdF_Mappe.RTErr
        
  wOggetto = TbOggetti!nome
  wstr = m_ImsDll_DC.ImNomeDB
  pos4 = InStr(wstr, ".")
 
  Set rs = m_fun.Open_Recordset("select Mapping from BS_Oggetti where IdOggetto =" & GbIdOggetto)
  If Not rs.EOF Then
    mapping = rs!mapping
  End If
  rs.Close
 
  If pos4 > 0 Then wstr = Mid$(wstr, 1, pos4 - 1)
  PercorsoCics = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\cics"
  Percorso = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\cics\Cpy"
  
  If Dir(PercorsoCics, vbDirectory) = "" Then MkDir PercorsoCics
  If Dir(Percorso, vbDirectory) = "" Then MkDir Percorso
           
  GeneraCopyMappa_DLI_CICS wOggetto, MaimdF_Mappe.RTBR, pFMTname
  'Generiamo anche le copy move
  GeneraCopyMove_DLI_CICS wOggetto, MaimdF_Mappe.RTBR, pFMTname
  If mapping Then
    GeneraCopyCicsJ_DLI_CICS wOggetto, MaimdF_Mappe.RTBR, pFMTname
  End If
End Sub

Function maxMapCol(pFMT As String, pbcolonna As Integer)
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("SELECT Max(lunghezza + colonna - 1) As MaxCol FROM PsDli_MFScmp WHERE " & _
                                "FMTName = '" & pFMT & "'")
  If Not rs.EOF Then
    maxMapCol = rs!MaxCol
  End If
  rs.Close
  If maxMapCol < 81 Then
    maxMapCol = 80
  Else
    maxMapCol = 132
  End If
  pbcolonna = maxMapCol
End Function

Function FormatX(Numero As Single) As String
  Dim HH As Variant, MM As Variant, SS As Variant
  Dim wNumero As Variant
  
  wNumero = Numero
  
  HH = Int(wNumero / 3600)
  wNumero = wNumero - (HH * 3600)
  MM = Int(wNumero / 60)
  wNumero = wNumero - (MM * 60)
  SS = wNumero
  
  FormatX = Format(HH, "00") & "." & Format(MM, "00") & "." & Format(SS, "00")
End Function

Public Sub load_equMaps()
  ReDim mTrsMaps(0)
  
  ReDim Preserve mTrsMaps(UBound(mTrsMaps) + 1)
  mTrsMaps(UBound(mTrsMaps)).mfs = "PROT"
  mTrsMaps(UBound(mTrsMaps)).bms = "ASKIP"
  
  ReDim Preserve mTrsMaps(UBound(mTrsMaps) + 1)
  mTrsMaps(UBound(mTrsMaps)).mfs = "MOD"
  mTrsMaps(UBound(mTrsMaps)).bms = "ASKIP"
  
  ReDim Preserve mTrsMaps(UBound(mTrsMaps) + 1)
  mTrsMaps(UBound(mTrsMaps)).mfs = "NUM"
  mTrsMaps(UBound(mTrsMaps)).bms = "NUM"
  
  ReDim Preserve mTrsMaps(UBound(mTrsMaps) + 1)
  mTrsMaps(UBound(mTrsMaps)).mfs = "HI"
  mTrsMaps(UBound(mTrsMaps)).bms = "BRIGHT"
  
  ReDim Preserve mTrsMaps(UBound(mTrsMaps) + 1)
  mTrsMaps(UBound(mTrsMaps)).mfs = "NODISP"
  mTrsMaps(UBound(mTrsMaps)).bms = "DARK"
End Sub

Public Function getTrovaEQU(mfsAttr As String) As String
  Dim i As Long
  
  'SG : Da implementera diversamente
  For i = 1 To UBound(mEqu)
    If mEqu(i).Alias <> "L" And mEqu(i).Alias <> "P" Then
      If InStr(10, mfsAttr, mEqu(i).Alias & " ") Then
        getTrovaEQU = mEqu(i).cmd
        Exit Function
      End If
    End If
  Next i
End Function
