Attribute VB_Name = "MadrdM_00"
Option Explicit

Global Const SYSTEM_DIR = "i-4.Mig.cfg"

Public gbTypeToLoad As String

'Template per i PCB
Type t_PcbStructDett
   wPicture As String
   wTipo As String
   wLunghezza As String
   wSegno As String
   wUsage As String
End Type

Type t_PcbStruct
   FixedLength As Long
   Details() As t_PcbStructDett
End Type

Type t_PcbTemplate
   IO_Pcb As t_PcbStruct
   ALT_Pcb As t_PcbStruct
   STD_Pcb As t_PcbStruct
End Type

Enum e_TypePcb
   IO_Pcb = 2 ^ 0
   ALTERNATE_PCB = 2 ^ 1
   STANDARD_PCB = 2 ^ 2
   IS_NOT_A_PCB = 2 ^ 3
End Enum
'Type Totale per i menu
Type MaM1
  Id  As Long
  Label As String
  ToolTipText As String
  Picture As String
  PictureEn As String
  M1Name As String
  M1SubName As String
  M1Level As Long
  M2Name As String
  M3Name As String
  M3ButtonType As String
  M3SubName As String
  M4Name As String
  M4ButtonType As String
  M4SubName As String
  Funzione As String
  DllName As String
  TipoFinIm As String
End Type

'SG : Indica la modalit� di incapsulamento/generazione (CALL o EXEC)
Public swCall As Boolean

Public Const WM_SETFOCUS As Long = &H7
Public Const WM_ACTIVATE As Long = &H6

Global drMenu() As MaM1

Global Dli2Rdbms As New MadrdC_Menu
Global m_fun As New MaFndC_Funzioni

Global SwMenu As Boolean

Global ConnectionState As Boolean

Type WdCgh
  Origine  As String
  Nuova As String
End Type

Public DrNomiDBD() As String         'Nomi DBD
Public DrWordChange() As WdCgh       'Parole da cambiare

Public Declare Function SetFocusWnd Lib "user32.dll" Alias "SetFocus" _
  (ByVal hwnd As Long) As Long
Public Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Public Declare Function SendMessage Lib "user32" _
   Alias "SendMessageA" _
  (ByVal hwnd As Long, _
   ByVal wMsg As Long, _
   ByVal wParam As Long, _
   lParam As Any) As Long
   
' Mauro 14/11/2007 : Struttura d'appoggio per la Sartoria
Type Instructions
  nome As String
  Commento As String
  If_Perform As String
  Istruzione As String
  Routine As String
End Type

Type TypeProgr
  FileName As String
  NumProgressivo As Long
End Type
  
Public Sub Carica_TreeView_Parametri(TR As TreeView, LSW As ListView)
  Dim i As Long
  Dim MaxItem As Long
  
  TR.Nodes.Clear
  
  TR.Nodes.Add , , "DBD", "Database List"
    'Aggiunge la lista dei database
    Aggiungi_Lista_DBD_In_TreeView TR, "DBD"
    
  TR.Nodes.Add , , "DLI", "Dli Calling"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "DLI", tvwChild, "DLICALL", "CALL"
    TR.Nodes.Add "DLI", tvwChild, "DLIEXEC", "EXEC"
  
  TR.Nodes.Add , , "RTF", "Routine Name's Format"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "RTF", tvwChild, "ROUTTP", "CICS : " & Dli2Rdbms.DrTPRout
    TR.Nodes.Add "RTF", tvwChild, "ROUTBT", "BATCH : " & Dli2Rdbms.DrBTRout
    TR.Nodes.Add "RTF", tvwChild, "ROUTGN", "GENERAL : " & Dli2Rdbms.DrGNRout
  
  TR.Nodes.Add , , "RTN", "Routine Calling"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "RTN", tvwChild, "RTNCALL", "CALL"
    TR.Nodes.Add "RTN", tvwChild, "RTNEXEC", "EXEC CICS"
  
  TR.Nodes.Add , , "IMS", "IMS DIFFERENCES"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "IMS", tvwChild, "IMSUNI", "WHETHER IMS TP/BATCH VALID"
    TR.Nodes.Add "IMS", tvwChild, "IMSDIV", "IMS TP/BATCH DIFFERENT"
    
  TR.Nodes.Add , , "USG", "USING"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "USG", tvwChild, "USGCPY", "USING COPY"
    TR.Nodes.Add "USG", tvwChild, "USGPRE", "USING PRECOMPILE"
    
  TR.Nodes.Add , , "LEV", "Max Istruction Level Call"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "LEV", tvwChild, "LEVNUM", "Number : "
    
  TR.Nodes.Add , , "WRD", "Changing Word"
  TR.Nodes(TR.Nodes.Count).Checked = True
    
  Chekka_TreeView_Parametri TR, LSW
  
  'Carica la lista delle parole da cambiare in lista
  LSW.ListItems.Clear
  For i = 1 To UBound(DrWordChange)
    LSW.ListItems.Add , , DrWordChange(i).Origine
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , DrWordChange(i).Nuova
    MaxItem = MaxItem + 1
  Next i
  
  'Carica gli Item rimanenti fino a 50 di default nella lista
  For i = 1 To 50 - MaxItem
    LSW.ListItems.Add , , ""
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , ""
  Next i
End Sub

Public Sub Aggiungi_Lista_DBD_In_TreeView(TR As TreeView, Padre As String)
  Dim r As Recordset
  Dim i As Long
  Dim key As String
  Dim Cont As Long
  
  Dim Idx As Long
  Dim stringa As String
  Dim Parametro As String
  Dim Valore As String
  
  ReDim DrNomiDBD(0)
  
  'Controlla se ci sono dei parametri salvati
  Set r = m_fun.Open_Recordset("Select * From BS_Parametri Where Tipo = 'DRDBD'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    'Carica l'array in memoria
    stringa = r!Valore
    
    Idx = InStr(1, stringa, ";")
    
    While Idx <> 0
      
      Parametro = Mid(stringa, 1, Idx - 1)
      
      ReDim Preserve DrNomiDBD(UBound(DrNomiDBD) + 1)
      DrNomiDBD(UBound(DrNomiDBD)) = Parametro
      
      stringa = Mid(stringa, Idx + 1)
      
      Idx = InStr(1, stringa, ";")
    Wend
    
    r.Close
  End If
  
  If UBound(DrNomiDBD) = 0 Then 'Carica dal DB
    Cont = 1
    
    Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where Tipo = 'DBD' And Tipo_DBD = 'HID' Order By IdOggetto")
    
    If r.RecordCount > 0 Then
      r.MoveFirst
      
      While Not r.EOF
        key = "DB" & Format(Cont, "00") & r!nome
        
        TR.Nodes.Add Padre, tvwChild, key, r!nome
        
        r.MoveNext
      Wend
      r.Close
    End If
  Else  'Carica dai parametri
    For i = 1 To UBound(DrNomiDBD)
        key = "DB" & Format(i, "00") & DrNomiDBD(i)
        
        TR.Nodes.Add Padre, tvwChild, key, Mid(DrNomiDBD(i), 1, InStr(1, DrNomiDBD(i), "=") - 1)
        
        If Mid(DrNomiDBD(i), InStr(1, DrNomiDBD(i), "=") + 1) = "1" Then
          TR.Nodes(TR.Nodes.Count).Checked = True
        Else
          TR.Nodes(TR.Nodes.Count).Checked = False
        End If
    Next i
  End If
End Sub

Public Sub Carica_Parametri_Da_DB(ArrayDBD() As String, ArrOR() As String, ArrNew() As String)
  Dim r As Recordset
  Dim Idx As Long
  Dim stringa As String
  Dim Parametro As String
  Dim Valore As String
  
  'Parametri Globali
  Set r = m_fun.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'DRPARAM'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    'Deve essere un solo record
    stringa = r!ParameterValue
    
    Idx = InStr(1, stringa, ";")
    
    While Idx <> 0
      
      Parametro = Mid(stringa, 1, Idx - 1)
      
      Valore = Mid(Parametro, InStr(1, Parametro, "=") + 1)
      Parametro = Mid(Parametro, 1, InStr(1, Parametro, "=") - 1)
      
      Select Case Trim(UCase(Parametro))
        'Assegna i parametri
        Case "DLICALL"
          If Valore = 1 Then
            Dli2Rdbms.DrCallDli = "CALL"
          End If
          
        Case "DLIEXEC"
           If Valore = 1 Then
            Dli2Rdbms.DrCallDli = "EXEC"
          End If
          
        Case "RTNCALL"
          If Valore = 1 Then
            Dli2Rdbms.DrCallRtn = "CALL"
          End If
          
        Case "RTNEXEC"
          If Valore = 1 Then
            Dli2Rdbms.DrCallRtn = "EXEC"
          End If
          
        Case "IMSUNI"
          If Valore = 1 Then
            Dli2Rdbms.DrIMSUsing = "UNI"
          End If
          
        Case "IMSDIV"
          If Valore = 1 Then
            Dli2Rdbms.DrIMSUsing = "DIV"
          End If
          
        Case "USGCPY"
          If Valore = 1 Then
            Dli2Rdbms.DrUsing = "CPY"
          End If
          
        Case "USGPRE"
          If Valore = 1 Then
            Dli2Rdbms.DrUsing = "PRE"
          End If
          
        Case "LEVNUM"
          Dli2Rdbms.DrMaxLevIstr = Valore
        
        Case "ROUTTP"
          Dli2Rdbms.DrTPRout = Valore
        Case "ROUTBT"
          Dli2Rdbms.DrBTRout = Valore
        Case "ROUTGN"
          Dli2Rdbms.DrGNRout = Valore
          
      End Select
      
      stringa = Mid(stringa, Idx + 1)
      
      Idx = InStr(1, stringa, ";")
    Wend
    
    r.Close
  Else
    'Carica i default
    Dli2Rdbms.DrCallDli = "EXEC"
    Dli2Rdbms.DrCallRtn = "CALL"
    Dli2Rdbms.DrIMSUsing = "UNI"
    Dli2Rdbms.DrUsing = "CPY"
    Dli2Rdbms.DrMaxLevIstr = 0
    Dli2Rdbms.DrTPRout = "RTTP0000"
    Dli2Rdbms.DrBTRout = "RTBT0000"
    Dli2Rdbms.DrGNRout = "RTGN0000"
  End If
  
  'Nomi Database
  'Carica i nomi nell'array della classe e poi li copia in quello passato dal MAIN
  Set r = m_fun.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'DRDBD'")
    
  ReDim DrNomiDBD(0)
     
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    stringa = r!ParameterValue
    
    Idx = InStr(1, stringa, ";")
    
    While Idx <> 0
      
      Parametro = Mid(stringa, 1, Idx - 1)
      
      Valore = Mid(Parametro, InStr(1, Parametro, "=") + 1)
      Parametro = Mid(Parametro, 1, InStr(1, Parametro, "=") - 1)
      
      If Valore = 1 Then
        ReDim Preserve DrNomiDBD(UBound(DrNomiDBD) + 1)
        DrNomiDBD(UBound(DrNomiDBD)) = Parametro
      End If
      
      stringa = Mid(stringa, Idx + 1)
      
      Idx = InStr(1, stringa, ";")
    Wend
    
  End If
  
  Carica_Array_DBD_Da_Convertire ArrayDBD
  
  'Carica le parole da cambiare
  Set r = m_fun.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'DRWORD'")
    
  ReDim DrWordChange(0)
     
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    stringa = r!ParameterValue
    
    Idx = InStr(1, stringa, ";")
    
    While Idx <> 0
      
      Parametro = Mid(stringa, 1, Idx - 1)
      
      Valore = Mid(Parametro, InStr(1, Parametro, "=") + 1)
      Parametro = Mid(Parametro, 1, InStr(1, Parametro, "=") - 1)
     
      ReDim Preserve DrWordChange(UBound(DrWordChange) + 1)
      DrWordChange(UBound(DrWordChange)).Origine = Parametro
      DrWordChange(UBound(DrWordChange)).Nuova = Valore
      
      stringa = Mid(stringa, Idx + 1)
      
      Idx = InStr(1, stringa, ";")
    Wend
    
  End If
  
  Carica_Array_Word_Da_Cambiare ArrOR, ArrNew
  
End Sub

Public Sub Carica_Array_DBD_Da_Convertire(Arr() As String)
  Dim i As Long
  
  ReDim Arr(0)
  
  For i = 1 To UBound(DrNomiDBD)
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)) = DrNomiDBD(i)
  Next i
End Sub

Public Sub Carica_Array_Word_Da_Cambiare(ArrOR() As String, ArrNew() As String)
  Dim i As Long
  
  ReDim ArrOR(0)
  ReDim ArrNew(0)
  
  For i = 1 To UBound(DrWordChange)
    ReDim Preserve ArrOR(UBound(ArrOR) + 1)
    ReDim Preserve ArrNew(UBound(ArrNew) + 1)
    ArrOR(UBound(ArrOR)) = DrWordChange(i).Origine
    ArrNew(UBound(ArrNew)) = DrWordChange(i).Nuova
  Next i
End Sub

Public Sub Chekka_TreeView_Parametri(TR As TreeView, LSW As ListView)
  Dim i As Long
  
 
  For i = 1 To TR.Nodes.Count
    Select Case Trim(UCase(TR.Nodes(i).key))
      Case "DLICALL"
        If Trim(UCase(Dli2Rdbms.DrCallDli)) = "CALL" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "DLIEXEC"
        If Trim(UCase(Dli2Rdbms.DrCallDli)) = "EXEC" Then
          TR.Nodes(i).Checked = True
        End If
      
      Case "RTNCALL"
        If Trim(UCase(Dli2Rdbms.DrCallRtn)) = "CALL" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "RTNEXEC"
        If Trim(UCase(Dli2Rdbms.DrCallRtn)) = "EXEC" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "IMSUNI"
        If Trim(UCase(Dli2Rdbms.DrIMSUsing)) = "UNI" Then
          TR.Nodes(i).Checked = True
        End If
      
      Case "IMSDIV"
        If Trim(UCase(Dli2Rdbms.DrIMSUsing)) = "DIV" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "USGCPY"
        If Trim(UCase(Dli2Rdbms.DrUsing)) = "CPY" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "USGPRE"
        If Trim(UCase(Dli2Rdbms.DrUsing)) = "PRE" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "LEVNUM"
        TR.Nodes(i).text = "Number : " & Dli2Rdbms.DrMaxLevIstr
        TR.Nodes(i).Checked = True
    End Select
  Next i
  
  'Checcka i DBD
  Dim AllBool As Boolean
  
  AllBool = True
  
  For i = 2 To TR.Nodes.Count
    If Mid(TR.Nodes(i).key, 1, 2) = "DB" Then
      If TR.Nodes(i).Checked = False Then
        AllBool = False
        Exit For
      End If
    End If
  Next i
  
  If AllBool = True Then
    TR.Nodes(1).Checked = True
  End If
  
End Sub

Public Sub Salva_Parametri(TR As TreeView, LSW As ListView)
  Dim r As Recordset
  
  Dim i As Long
  Dim Valore As Long
  
  Dim ListaDBD() As String
  
  Dim stringa As String
  
  ReDim ListaDBD(0)
  
  For i = 2 To TR.Nodes.Count
    If Mid(TR.Nodes(i).key, 1, 2) = "DB" Then
      ReDim Preserve ListaDBD(UBound(ListaDBD) + 1)
      
      If TR.Nodes(i).Checked = True Then
        Valore = 1
      Else
        Valore = 0
      End If
      
      ListaDBD(UBound(ListaDBD)) = TR.Nodes(i).text & "=" & Valore
    End If
  Next i
  
  'Compone la stringa
  For i = 1 To UBound(ListaDBD)
    stringa = stringa & ListaDBD(i) & ";"
  Next i
  
  Set r = m_fun.Open_Recordset("Select * From BS_Parametri Where Tipo = 'DRDBD'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    r!Valore = stringa
    r.Update
    
    r.Close
  Else
    r.AddNew
    r!tipo = "DRDBD"
    r!Valore = stringa
    r.Update
  End If
  
  stringa = ""
  
  'Compone la stringa
  For i = 1 To LSW.ListItems.Count
    If Trim(LSW.ListItems(i).text) <> "" Then
      stringa = stringa & LSW.ListItems(i).text & "=" & LSW.ListItems(i).ListSubItems(1).text & ";"
    End If
  Next i
  
  Set r = m_fun.Open_Recordset("Select * From BS_Parametri Where Tipo = 'DRWORD'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    r!Valore = stringa
    r.Update
    
    r.Close
  Else
    r.AddNew
    r!tipo = "DRWORD"
    r!Valore = stringa
    r.Update
  End If
  
  
  '********************************************
  '*** PARTE CHE Salva I PARAMETRI GLOBALI ***
  '********************************************
  stringa = ""
  
  If Dli2Rdbms.DrCallDli = "EXEC" Then
    stringa = "DLICALL=0;DLIEXEC=1;"
  End If
  
  If Dli2Rdbms.DrCallDli = "CALL" Then
    stringa = "DLICALL=1;DLIEXEC=0;"
  End If
  
  If Dli2Rdbms.DrCallRtn = "CALL" Then
    stringa = stringa & "RTNCALL=1;RTNEXEC=0;"
  End If
  
  If Dli2Rdbms.DrCallRtn = "EXEC" Then
    stringa = stringa & "RTNCALL=0;RTNEXEC=1;"
  End If
  
  If Dli2Rdbms.DrIMSUsing = "UNI" Then
    stringa = stringa & "IMSUNI=1;IMSDIV=0;"
  End If
  
  If Dli2Rdbms.DrIMSUsing = "DIV" Then
    stringa = stringa & "IMSUNI=0;IMSDIV=1;"
  End If
  
  If Dli2Rdbms.DrUsing = "CPY" Then
    stringa = stringa & "USGCPY=1;USGPRE=0;"
  End If
  
  If Dli2Rdbms.DrUsing = "PRE" Then
    stringa = stringa & "USGCPY=0;USGPRE=1;"
  End If
  
  stringa = stringa & "ROUTTP=" & Dli2Rdbms.DrTPRout & ";"
  stringa = stringa & "ROUTBT=" & Dli2Rdbms.DrBTRout & ";"
  stringa = stringa & "ROUTGN=" & Dli2Rdbms.DrGNRout & ";"
  
  stringa = stringa & "LEVNUM=" & Dli2Rdbms.DrMaxLevIstr
  
  Set r = m_fun.Open_Recordset("Select * From BS_Parametri Where Tipo = 'DRPARAM'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    'Edita
    r!Valore = stringa
    r.Update
    
    r.Close
  Else
    'Aggiunge
    r.AddNew
    r!tipo = "DRPARAM"
    r!Valore = stringa
    r.Update
  End If
End Sub

Public Function Restituisci_Colonna_Lista_Selezionata(LSW As ListView, x As Single) As Long
  Dim IdxRiga As Long
  Dim Somma As Single
  Dim OldSomma As Single
  Dim i As Long
  
  Somma = 0
  
  IdxRiga = LSW.SelectedItem.index
  
  'Trova la colonna selezionata
  For i = 1 To LSW.ColumnHeaders.Count
    OldSomma = Somma
    
    If x > OldSomma And x < Somma + LSW.ColumnHeaders(i).Width Then
      Restituisci_Colonna_Lista_Selezionata = i
      Exit Function
    End If
    
    Somma = Somma + LSW.ColumnHeaders(i).Width
  Next i
End Function

Public Function Trova_Primo_Carattere_In_Avanti(RT As RichTextBox, Idx As Long) As Long
  Dim i As Long
  
  For i = 1 To 100
    RT.SelStart = Idx + i
    RT.SelLength = 1
    
    If RT.SelText <> " " Then
      Trova_Primo_Carattere_In_Avanti = Idx + i
      Exit For
    End If
  Next i
End Function

'SQ: 28-07-05
Public Function getEnvironmentParam_SQ(ByVal parameter As String, envType As String, environment As Collection) As String
  Dim token As String
  Dim subTokens() As String
  'intervallo:
  Dim intervallo() As String
  
  On Error GoTo errdb
  
  Select Case envType
    Case "colonne"
      'environment = rsColonne,rsTabelle
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "GROUP-NAME"
              'token = getGroupName_colonne(environment.Item(1)!IdOggetto)
            Case "COLUMN-NAME"
              token = environment.item(1)!nome
            Case "ALIAS"
              If m_fun.FnNomeDB = "banca.mty" Then
                token = Left(environment.item(2)!nome, 2) & "N" & Mid(environment.item(2)!nome, 10)
              Else
                'fare...
              End If
            Case Else
              MsgBox "Variable unknown: " & subTokens(0)
          End Select
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        End If
      Wend
    Case "tabelle"
      'environment = rsTabelle,rsSegmenti
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "ROUTE-NAME", "ROUT-NAME"
              token = getRouteName_tabelle(environment.item(2)!IdOggetto)
            Case "SEGM-PROGR"
              token = getSegProgr_tabelle(environment.item(2))
            Case "SEGM-NAME"
              token = environment.item(2)!nome
            Case "TABLE-NAME"
              token = getTableName_tabelle(environment.item(2)!IdOggetto)
            Case "ALIAS"
              If m_fun.FnNomeDB = "banca.mty" Then
                token = Left(environment.item(2)!nome, 2) & "N" & Mid(environment.item(1)!nome, 10)
              Else
                'fare...
              End If
            Case "NOME-AREA"  'Area Cobol associata al segmento:
              'token = getNomeArea_tabelle(environment.Item(2)!IdOggetto)
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        End If
      Wend
    Case "dbd"
      'environment = nome DBD
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "ROUTE-NAME", "ROUT-NAME"
              token = getRouteName_dbd(environment.item(1))
            Case "TABLE-NAME"
              token = getTableName_dbd(environment.item(1))
            Case "DBD-NAME"
              token = environment.item(1)
            'Case "SEGM-PROGR"
            '  token = getSegProgr_tabelle(environment.Item(2))
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        End If
      Wend
    Case "else"
      '...
  End Select
  Exit Function
errdb:
  If err.Number = 13 Then
    'sbagliato l'intervallo numerico
    MsgBox "Wrong definition for " & parameter, vbExclamation
  Else
    MsgBox err.Description
    'Resume
  End If
  getEnvironmentParam_SQ = ""
End Function
'SQ
Public Function getRouteName_dbd(NomeDbD As String) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("SELECT Route_name from psDli_DBD where DBD_name='" & NomeDbD & "'")
  If r.RecordCount Then
    getRouteName_dbd = r(0)
  Else
    getRouteName_dbd = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox err.Description
  getRouteName_dbd = ""
End Function
'SQ
Public Function getTableName_dbd(NomeDbD As String) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("SELECT Table_name from psDli_DBD where DBD_name='" & NomeDbD & "'")
  If r.RecordCount Then
    getTableName_dbd = r(0)
  Else
    getTableName_dbd = ""
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox err.Description
  getTableName_dbd = ""
End Function
'SQ
Public Function getSegProgr_tabelle(rs As Recordset) As String
  Dim r As Recordset
  On Error GoTo errordB
  
  If InStr(rs.Source, " MgDLI_Segmenti ") Then
    Set r = m_fun.Open_Recordset("SELECT count(*) from MgDLI_Segmenti where idOggetto = " & rs!IdOggetto & " AND idSegmento <= " & rs!idSegmento)
  Else
    Set r = m_fun.Open_Recordset("SELECT count(*) from PsDLI_Segmenti where idOggetto = " & rs!IdOggetto & " AND idSegmento <= " & rs!idSegmento)
  End If
  If r.RecordCount Then
    getSegProgr_tabelle = Format(r(0), "000")
  Else
    getSegProgr_tabelle = "000"
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox err.Description
  getSegProgr_tabelle = "000"
End Function
'SQ
Public Function getRouteName_tabelle(idDBD As Long) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("SELECT a.Route_name from psDli_DBD as a, bs_oggetti as b where " & _
                               "b.nome = a.DBD_name AND b.idOggetto = " & idDBD)
  If r.RecordCount Then
    getRouteName_tabelle = r(0)
  Else
    getRouteName_tabelle = ""
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox err.Description
  getRouteName_tabelle = ""
End Function
'SQ
Public Function getTableName_tabelle(idDBD As Long) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("SELECT a.Table_name from psDli_DBD as a, bs_oggetti as b where " & _
                               "b.nome = a.DBD_name AND b.idOggetto = " & idDBD)
  If r.RecordCount Then
    getTableName_tabelle = r(0)
  Else
    getTableName_tabelle = ""
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox err.Description
  getTableName_tabelle = ""
End Function
'SQ
Public Function getDliCopies_BPER(RsTabelle As Recordset) As String
  Dim inCondition As String
  Dim rsCopies As Recordset
  
  On Error GoTo errdb
  
  inCondition = RsTabelle!idOrigine
  RsTabelle.MoveNext
  While Not RsTabelle.EOF
    inCondition = inCondition & "," & RsTabelle!idOrigine
    RsTabelle.MoveNext
  Wend
  'ripristiono per il chiamante
  RsTabelle.MoveFirst
  Set rsCopies = m_fun.Open_Recordset("select distinct nome from bs_oggetti as a,MgRel_SegAree as b where " & _
                                      "StrIdOggetto = IdOggetto AND IdSegmento IN (" & inCondition & ")")
  getDliCopies_BPER = "COPY " & rsCopies(0) & "."
  rsCopies.MoveNext
  While Not rsCopies.EOF
    getDliCopies_BPER = getDliCopies_BPER & vbCrLf & Space(11) & "COPY " & rsCopies(0) & "."
    rsCopies.MoveNext
  Wend
  rsCopies.Close
  Exit Function
errdb:
  MsgBox err.Description
  getDliCopies_BPER = ""
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ riesumata dal DM... NON la usa nessuno, ma � spettacolare!
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub loadTemplate(rtbFile As RichTextBox, FileName As String, Optional templateDir As String = "\Input-Prj\Template\")
  Dim regExpr As Object
  Dim matches As Variant, Match As Variant
  Dim rtbTemplate As RichTextBox
  Dim changingTag As String, rtbBackup As String, stringTemplate As String
  
  On Error GoTo fileErr
  
  rtbFile.LoadFile FileName
  
  Set regExpr = CreateObject("VBScript.regExp")
  
  regExpr.Pattern = "<<[^>]*>>" 'altrimenti mi accorpa tutte quelle di una riga...
  regExpr.Global = True
  
  Set matches = regExpr.Execute(rtbFile.text)
    
  For Each Match In matches
    'carenza di rtbFile...
    rtbBackup = rtbFile.text
    rtbFile.LoadFile m_fun.FnPathPrj & templateDir & Mid(Match, 3, Len(Match) - 4)
    stringTemplate = rtbFile.text
    rtbFile.text = rtbBackup
    changeTag rtbFile, Match & "", stringTemplate
  Next
  
  ' DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & rsColonne!RoutLoad
  Exit Sub
fileErr:
  If err.Number = 75 Then
    MsgBox "Template " & m_fun.FnPathPrj & templateDir & Mid(Match, 3, Len(Match) - 4) & " not found.", vbExclamation
  Else
    MsgBox err.Description
  End If
End Sub
