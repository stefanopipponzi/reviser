Attribute VB_Name = "MadrdM_Corrector"
Option Explicit

Public arrayRighe() As String 'ale 02/05/2006
Public lineeIstr() As String
Public copyExpanded As Boolean
'Gloria: per gestione espansione su pi� livelli
Public idLevel As Integer
Public collExpandedCopy As Collection
Public nrExpandedCopy As Integer
'**************
Global AUSelText As String
Global AURigaSel As Long

'AC per verivicare se PCB � cambiato
Global GbPrevPCB As String
Global GbPrevIstr As String
Global GbNomePCB As String
Global GbDefaultLevel As Boolean
' per gestire con un unico form scelta operatori ed istruzioni
Global GbIntFormSel As Integer
'Serve per selezionare una voce sulla lista delle molteplicit�
Global AUMoltSel As String

'SG : Indica l'index in memoria dove � mappata la chiave
Global AUSelIndexKey As Long

'Serve per sapere se nella form devo caricare i segmenti, i qualif ecc...
Global AUKeyMolt As String
  'SEG : Segmenti
  'KEY : Key
  'QLF : Qualificatori
  'OPE : Operatori
  'VAL : Valori

'Serve per sapere su che livello mi trovo nella TreeView
Global AULivello As Long

'Serve per fotografare la situazione delle immagini della treeview prima
'di cominciare a fare tutte le modifiche
Public Mappa_Immagini_TreeView() As Long

Enum e_DBDType
  DBD_GSAM = 0 ^ 2
  DBD_HDAM = 1 ^ 2
  DBD_HIDAM = 2 ^ 2
  DBD_INDEX = 3 ^ 2
  DBD_UNKNOWED_TYPE = 4 ^ 2
End Enum

'Type per contenere i dati dell'istruzione
''''Enum t_type
''''    TYPE_CBL = 2 ^ 0
''''    TYPE_DBD = 2 ^ 1
''''    TYPE_PSB = 2 ^ 2
''''End Enum

Type tObj
  IdOggetto As Long
  nome As String
  tipo As t_type
End Type

Type SSAIstr
  livello As Long
  Segmento As String
  SegLen As String
  ssaName As String
  Struttura As String
  key As String
  KeyLen As String
  KeyStart As String
  KeyMask As String
  OPERATORI As String
  Valore As String
  ccode As String
End Type

Type tl_SSA
  IdOggetto As Long
  IdArea As Long
  ssaName As String
  Moved As Boolean
  ssaStream As String
  idSegmento As Long
  IdSegmentiD As String
  NomeSegmento As String
  NomeSegmentiD As String
  LenSegmento As Long
  LenSegmentiD As String
  Struttura As String
  Qualificatore As Boolean
  BothQualAndUnqual As Boolean
  livello As Long
  Chiavi() As t_Key
  CommandCode As String
  OpLogico() As String
  valida As Boolean
End Type

Type istr
  Istruzione As String
  Riga As Long
  Tipologia As String
  MapName As String
  ImsDB As Boolean
  ImsDC As Boolean
  DBD_Type As e_DBDType
  Database() As tObj
  psb() As tObj
  valida As Boolean
  SSA() As tl_SSA
  pcbName As String '2-5-06
  to_migrate As Boolean
  obsolete As Boolean
  calltype As String
End Type

Global Istruzione As istr

'***********************************************************************
'*** indica la molteplicit� dei segmenti di una stessa SSA  ************
'***********************************************************************
Public Type MOLTEPLICITA
  IdOggetto As Long
  Riga As Long
  livello As Long
  nome As String
End Type

Global SEGMENTI() As MOLTEPLICITA
Global Field() As MOLTEPLICITA
Global FIELDLEN() As MOLTEPLICITA
Global OPERATORI() As MOLTEPLICITA
Global VALORI() As MOLTEPLICITA
Global QUALIFICATORI() As MOLTEPLICITA

Global copyFileLoad As Boolean
Global IdCopyFileLoad As Long

'Id dell'istruzione corrente
Public gIdDbd As Long
Public gIdPsb As Long
Public gIdField As Long
Public RTStart As Long
'...
' Mauro 16/10/2007
Const BAFFETTO = 8
Const DIVIETO = 11
Const BAFFETTO_MOVED = 19
Const DIVIETO_MOVED = 20

' Mauro 28/01/2008
Public is_AddClone As Boolean

' Mauro 07/05/2008 : Costanti per colonne del Corrector
' *** Nel TAG c'� il numero di righe dell'Istruzione ***
Public Const L_IDPGM = 0
Public Const L_IDOGG = 1
Public Const L_LINE = 2
Public Const L_INSTR = 3
Public Const L_STATEMENT = 4
Public Const L_IDPSB = 5

Public Sub Carica_File_In_RichTextBox(LSW As ListView, RT As RichTextBox, IdOggetto As Long)
  Dim r As Recordset
  Dim Directory As String
  Dim wExt As String
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & IdOggetto)
  
  If r.RecordCount Then
    'E' un record solo
    If Trim(r!estensione) <> "" Then
      wExt = "." & r!estensione
    Else
      wExt = ""
    End If
    
    Directory = Crea_Directory_Progetto(r!Directory_Input, Dli2Rdbms.drPathDef) & "\" & r!nome & wExt
    LSW.ListItems.Add , , "Loading File " & r!nome & "..."
    LSW.ListItems(LSW.ListItems.Count).tag = "LOAD" & Directory
    
    'Carica il file nella rich
    RT.LoadFile Directory
    
    'ALE memorizza le righe con gli indici di capo linea e fine linea
    indiciRighe RT
  End If
  r.Close
End Sub

Public Sub indiciRighe(RT As RichTextBox)
  'Idx = RT.Find(vbCrLf, OldPos) 'ALE
  'array dalla riga 1 alla fine contenenti gli  indici del carattere capo linea e fine linea
  'es:rt.Find("GDEL",x1,x2)
  Dim Idx As Long
  Dim Riga As Long
  Dim x1 As Long, x2 As Long
  Dim i As Long
  On Error GoTo errorHandler
 
  i = 0
  x1 = 1
  ReDim arrayRighe(0)
  Do Until Idx = -1
    Idx = RT.find(vbCrLf, x1)
    x2 = Idx
    'riga = RT.GetLineFromChar(idx)
    If x2 - x1 < 2 Then
      x2 = x1 + 2
      ReDim Preserve arrayRighe(i) 'riga
      arrayRighe(i) = x1 & ";" & x2
      i = i + 1
      x1 = x2
    Else
      ReDim Preserve arrayRighe(i) 'riga
      arrayRighe(i) = x1 & ";" & x2
      i = i + 1
      x1 = x2 + 2
    End If
  Loop
  Exit Sub
errorHandler:
  Resume Next
End Sub

' Mauro 08-05-2007 : copiato da Funzioni
Public Function getDBDByIdOggettoRiga(wIdPgm As Long, WidOggetto As Long, wRiga As Long) As Long()
  Dim str() As Long
  Dim rs As Recordset
  
  ReDim str(0)
  'SQ: Fare il redim una sola volta con il recordCount
  Set rs = m_fun.Open_Recordset("Select * From PsDLI_IstrDBD Where " & _
                                "idpgm = " & wIdPgm & " and " & _
                                "IdOggetto = " & WidOggetto & " And Riga = " & wRiga)
  While Not rs.EOF
    ReDim Preserve str(UBound(str) + 1)
    str(UBound(str)) = rs!idDBD
     
    rs.MoveNext
  Wend
  rs.Close
  
  getDBDByIdOggettoRiga = str
End Function

' Mauro 08-05-2007 : Copiato da Funzioni
Public Function getPSBByIdOggettoRiga(wIdPgm As Long, WidOggetto As Long, wRiga As Long) As Long()
  Dim str() As Long
  Dim rs As Recordset, rsPcb As Recordset
  
  ReDim str(0)
  Set rs = m_fun.Open_Recordset("Select * From PsDLI_IstrPSB Where " & _
                                "idpgm = " & wIdPgm & " and " & _
                                "IdOggetto = " & WidOggetto & " And Riga = " & wRiga)
  While Not rs.EOF
    ReDim Preserve str(UBound(str) + 1)
    str(UBound(str)) = rs!IdPSB
    
    rs.MoveNext
  Wend
  rs.Close
  
  getPSBByIdOggettoRiga = str
End Function

Public Sub Carica_ListView_Istruzioni(LSW As ListView, IdOggetto As Long, Opzioni As Integer, RT As RichTextBox)
  Dim r As Recordset, rs As Recordset, rGen As Recordset
  Dim Reset As Boolean
  Dim ssaName As String
  Dim ssaIdArea As Long, ssaIdOggetto As Long, ordinale As Integer
  Dim BolExitWhilegen As Boolean, BolMoved As Boolean
  
  LSW.ListItems.Clear
  
  Reset = True
  
  'SQ CPY-ISTR 'AC 12/12/07 istruzioni anche con a.idpgm = IdOggetto
  'AC 25/06/08 union con la PSExec (prima alternativi)
  If Opzioni = 1 Then  'se � stato selezionato l'option button optAllInstr
    If Not MadrdF_Corrector.chkErrInstr.Value = 1 Then
      Set r = m_fun.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & "  and  " & _
                                   "a.idoggetto = b.idoggetto and a.riga = b.riga Order By a.Riga" & _
                                    " Union Select a.*, b.linee From PSDLI_Istruzioni as a,PSexec as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & "  and  " & _
                                   "a.idoggetto = b.idoggetto and a.riga = b.riga Order By a.Riga")
    Else
      Set r = m_fun.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & " AND " & _
                                   "NOT VALIDA  and  a.idoggetto = b.idoggetto and a.riga = b.riga Order By a.Riga" & _
                                   " union Select a.*, b.linee From PSDLI_Istruzioni as a,PSexec as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & " AND " & _
                                   "NOT VALIDA  and  a.idoggetto = b.idoggetto and a.riga = b.riga Order By a.Riga")
    End If
  ElseIf Opzioni = 2 Then 'se � stato selezionato l'option button IMS/DB
    If Not MadrdF_Corrector.chkErrInstr.Value = 1 Then
      Set r = m_fun.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & " and " & _
                                   "a.idoggetto = b.idoggetto and a.riga = b.riga and a.ImsDB Order By a.Riga" & _
                                   " union Select a.*, b.linee From PSDLI_Istruzioni as a,PSExec as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & " and " & _
                                   "a.idoggetto = b.idoggetto and a.riga = b.riga and a.ImsDB Order By a.Riga")
    Else
      Set r = m_fun.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & " AND " & _
                                   "NOT VALIDA  and  a.idoggetto = b.idoggetto and a.riga = b.riga and a.ImsDB Order By a.Riga" & _
                                   " union Select a.*, b.linee From PSDLI_Istruzioni as a,PSExec as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & " AND " & _
                                   "NOT VALIDA  and  a.idoggetto = b.idoggetto and a.riga = b.riga and a.ImsDB Order By a.Riga")
    End If
  Else 'Opzioni = 3 'se � stato selezionato l'option button IMS/DC
    If Not MadrdF_Corrector.chkErrInstr.Value = 1 Then
      Set r = m_fun.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & "  and  " & _
                                   "a.idoggetto = b.idoggetto and a.riga = b.riga  and not a.ImsDB Order By a.Riga" & _
                                   " union Select a.*, b.linee From PSDLI_Istruzioni as a,PSExec as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & "  and  " & _
                                   "a.idoggetto = b.idoggetto and a.riga = b.riga  and not a.ImsDB Order By a.Riga")
    Else
      Set r = m_fun.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & " AND " & _
                                   "NOT VALIDA  and  a.idoggetto = b.idoggetto and a.riga = b.riga  and not a.ImsDB Order By a.Riga" & _
                                   " union Select a.*, b.linee From PSDLI_Istruzioni as a,PSExec as b WHERE " & _
                                   "a.idPgm > 0 AND (a.IdOggetto = " & IdOggetto & " or a.IdPgm = " & IdOggetto & ")" & " AND " & _
                                   "NOT VALIDA  and  a.idoggetto = b.idoggetto and a.riga = b.riga  and not a.ImsDB Order By a.Riga")
    End If
  End If
  
  BolExitWhilegen = False
  BolMoved = False
  While Not r.EOF
    Screen.MousePointer = vbHourglass
      
    '***********************  AC  ***************
    BolExitWhilegen = False
    BolMoved = False
    'stefano: un dato in pi�....
    'Set rGen = m_fun.Open_Recordset("SELECT StrIdOggetto,StrIdArea,NomeSSA FROM PSDli_IstrDett_Liv WHERE IdOggetto = " & r!IdOggetto & " AND Riga = " & r!Riga & " ORDER BY Livello")
    Set rGen = m_fun.Open_Recordset("SELECT StrIdOggetto,StrIdArea,NomeSSA,moved FROM PSDli_IstrDett_Liv WHERE " & _
                                    "IdOggetto = " & r!IdOggetto & " AND Riga = " & r!Riga & " ORDER BY Livello")
    While Not rGen.EOF And Not BolExitWhilegen
      If Not IsNull(rGen!strIdOggetto) Then
        ssaIdOggetto = rGen!strIdOggetto
      End If
      ssaName = IIf(IsNull(rGen!NomeSSA), "", rGen!NomeSSA)
        
      If Not IsNull(rGen!Stridarea) Then
        ssaIdArea = rGen!Stridarea
      End If
      
      'stefano: semplificato, non so se va bene sempre (parser interno al corrector?)
      If rGen!Moved Then
        BolMoved = True
        'stefano: a che serve questo? Non viene usato praticamente mai...
        BolExitWhilegen = True
      End If
      
      rGen.MoveNext
    Wend
    rGen.Close
     
    '*******************************
    ' Mauro 07/05/2008 : Adesso la prima colonna � l'IdPgm
    'LSW.ListItems.Add , , r!idOggetto
    LSW.ListItems.Add , , r!idPgm
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , r!IdOggetto
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , r!Riga
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , r!Istruzione & ""
    'LSW.ListItems(LSW.ListItems.count).ListSubItems.Add , , r!IdPSB & ""
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , r!statement & ""
    
    'ALE
    LSW.ListItems(LSW.ListItems.Count).tag = r!Linee
    'SQ CPY-ISTR (tmp)
    ' Mauro 07/05/2008 : La toolTip non contiene pi� l'IdPgm
    'LSW.ListItems(LSW.ListItems.count).ToolTipText = "IdPgm: " & r!idPgm
    
    If r!valida Then
      If BolMoved Then
        LSW.ListItems(LSW.ListItems.Count).SmallIcon = BAFFETTO_MOVED
      Else
        LSW.ListItems(LSW.ListItems.Count).SmallIcon = BAFFETTO
      End If
    Else
      If BolMoved Then
        LSW.ListItems(LSW.ListItems.Count).SmallIcon = DIVIETO_MOVED
      Else
        LSW.ListItems(LSW.ListItems.Count).SmallIcon = DIVIETO
      End If
    End If
    r.MoveNext

    Screen.MousePointer = vbDefault
    Reset = False
  Wend
  r.Close
End Sub

Public Function Marca_Istruzione_In_RichTextBox(RT As RichTextBox, Riga As Long, Reset As Boolean) As String
  Dim Idx As Long
  Dim Start As Long
  Dim PosRiga As Long
  Dim Marcatore As String
  Dim i As Long
  Static Oldpos As Long
 
  Marcatore = "L" & String(5 - Len(Hex(Riga)), "0") & Hex(Riga)
  
  Marca_Istruzione_In_RichTextBox = Marcatore
  
  If Reset Then
    Oldpos = 1
  End If
  
  Idx = RT.find(vbCrLf, Oldpos)
  
  Do While Idx <> -1
    PosRiga = RT.GetLineFromChar(Idx)
    If PosRiga = Riga - 2 Then
      Start = Idx
      Exit Do
    End If
    Idx = RT.find(vbCrLf, Idx + 2)
  Loop
  
  'Va avanti di 2 posizione e seleziona 6 byte
  RT.SelStart = Start + 2
  RT.SelLength = 6
  
  RT.SelText = Marcatore
  
  RT.SelStart = 0
  RT.SelLength = 0
End Function

Public Sub Expand_cpyNew(LSW As ListView, RTApp As RichTextBox, RTSource As RichTextBox, TxtFind As String)
  Dim RTPosition As Long, RTFine As Long
  Dim i As Long, j As Long
  Dim cpy As Long, cpyor As Long
  Dim Accapo As Long
  Dim NomeCOPY As String
  Dim FlagStart As Boolean
  Dim CopyPath  As String
  Dim CopyFind As Boolean
  Dim MarcatoreCPY As String
  Dim Riga As Long, riganew  As Long, RigaIni As Long
  Dim strg() As String
  Dim LenCpy As Long
  Dim Count As Long
  Dim rigaIn As Long, rigaOut As Long
  Dim isINCLUDE As Boolean
  
  'silvia
  Dim lenInclude As Long

  'Gloria
  Dim intI As Integer, booExpanded As Boolean
  Dim STipoOggetto As String
  
  LSW.ListItems.Add , , "Expanding Copy..."

  FlagStart = False
  isINCLUDE = False
  
  cpy = RTSource.find(TxtFind, 1)

  If cpy = -1 Then
    Exit Sub
  Else
    If InStr(TxtFind, "INCLUDE") Then
      isINCLUDE = True
    End If
  End If
  
  CopyFind = False
  
  While cpy <> -1
    NomeCOPY = ""
    
    'Controlla che non sia asteriscato
    For i = 1 To 100
      RTSource.SelStart = cpy - i
      RTSource.SelLength = 2
      If RTSource.SelText = vbCrLf Then
        Accapo = cpy - i + 1
        Exit For
      End If
    Next i
     
    If Not isINCLUDE Then
      RTSource.SelStart = Accapo + 6
    Else
      RTSource.SelStart = cpy - i
    End If
    RTSource.SelLength = 1
    
    If RTSource.SelText <> "*" Then
      'Trova il nome della Copy
      For i = 1 To 100
        If Not isINCLUDE Then
          RTSource.SelStart = cpy + 5 + i
        Else
          RTSource.SelStart = cpy + 7 + i
        End If
        RTSource.SelLength = 1
        If RTSource.SelText <> " " And RTSource.SelText <> "." And _
           RTSource.SelText <> "'" And RTSource.SelText <> ";" Then
          NomeCOPY = NomeCOPY & RTSource.SelText
          FlagStart = True
        Else
          If FlagStart Then
            Exit For
          End If
        End If
      Next i
      
      'silvia: tolgo le parentesi
      If Len(Trim(NomeCOPY)) Then
        NomeCOPY = Replace(NomeCOPY, "(", "")
        NomeCOPY = Replace(NomeCOPY, ")", "")
        
        'Toglie eventuali punti
        If Mid(NomeCOPY, Len(NomeCOPY), 1) = "." Then
          NomeCOPY = Mid(NomeCOPY, 1, Len(NomeCOPY) - 1)
        End If
      
        'Toglie eventuali apici
        If Mid(NomeCOPY, Len(NomeCOPY), 1) = "'" Then
          NomeCOPY = Mid(NomeCOPY, 1, Len(NomeCOPY) - 1)
        End If
      
        If Mid(NomeCOPY, 1, 1) = "'" Then
          NomeCOPY = Mid(NomeCOPY, 2)
        End If
       
        'Toglie eventuali punti e virgola: solo per PLI
        If isINCLUDE And Mid(NomeCOPY, Len(NomeCOPY), 1) = ";" Then
          NomeCOPY = Mid(NomeCOPY, 1, Len(NomeCOPY) - 1)
        End If
        
        Riga = RTSource.GetLineFromChar(cpy)
        MarcatoreCPY = "C" & String(5 - Len(Hex(Riga)), "0") & Hex(Riga)
  
        'Creata nuova funzione
        'Gloria 28/04/2009: cerco l'oggetto con il nome e il tipo (in base all'oggetto su cui sto lavorando) perch� il nome non � univoco
        If Not collExpandedCopy Is Nothing Then
          For intI = 1 To collExpandedCopy.Count
            If collExpandedCopy(intI) = NomeCOPY Then
              booExpanded = True
              Exit For
            End If
          Next intI
        End If
      Else
        booExpanded = False
      End If
      
      If Not booExpanded Then
        Select Case Trim(TxtFind)
          Case "COPY"
            STipoOggetto = "CPY"
          Case "INCLUDE"
            STipoOggetto = "INC"
        End Select
        CopyPath = Restituisci_File_Da_Nome_Tipo_Oggetto(NomeCOPY, STipoOggetto)
           
        '*************************
        ' gloria 28/04/2009: lo azzero alla fine di ogni ciclo altrimenti rimane sporco!
        If Trim(CopyPath) <> "" Then
          'Espande la copy :
          'CARICA LA COPY NELLA TEXT DI APPOGGIO
          RTApp.LoadFile CopyPath
          LenCpy = Len(RTApp.text)
          'trova RTPosition
          For i = 1 To 100
            RTSource.SelStart = cpy + i
            RTSource.SelLength = 2
            If RTSource.SelText = vbCrLf Then
              RTPosition = cpy + i + 2
              Exit For
            End If
          Next i
       
          'Copia la copy nella RichTextBox principale
          RTSource.SelStart = RTPosition
          RTSource.SelLength = 0
          
          RTSource.SelColor = vbRed
          If Not isINCLUDE Then
            RTSource.SelText = MarcatoreCPY & vbCrLf & RTApp.text & vbCrLf & "CPYFN" & vbCrLf
          Else
            RTSource.SelText = MarcatoreCPY & vbCrLf & RTApp.text & vbCrLf & "INCFN" & vbCrLf
          End If
          
          RTFine = RTPosition + Len(RTApp.text) + 11
             
          'aggiorna linee istruzioni ALE
          rigaIn = Riga 'RTSource.GetLineFromChar(RTPosition)
          rigaOut = RTSource.GetLineFromChar(RTFine)
          aggiornaLineeIstruz rigaIn, rigaOut
              
          RTSource.SelStart = RTPosition
          RTSource.SelLength = 0
          RTSource.SelColor = vbRed
          
          LSW.ListItems.Add , , "Copy " & NomeCOPY & " Expanded..."
          LSW.ListItems(LSW.ListItems.Count).tag = "EXPD" & MarcatoreCPY & CopyPath
  
          CopyFind = True
          If collExpandedCopy Is Nothing Then Set collExpandedCopy = New Collection
          collExpandedCopy.Add NomeCOPY
        Else
          If NomeCOPY <> ":" And NomeCOPY <> "=" And NomeCOPY <> " " Then
            LSW.ListItems.Add , , "Copy " & NomeCOPY & " Not Found..."
            LSW.ListItems(LSW.ListItems.Count).tag = "EXPE" & MarcatoreCPY & NomeCOPY
          End If
        End If
      End If
      booExpanded = False
    End If ' espando solo le copy non ancora espanse
            
    FlagStart = False
    If Not isINCLUDE Then
      'silvia 2-1-2008
      cpy = RTSource.find(TxtFind, LenCpy + cpy + 6)
    Else
      cpy = RTSource.find("INCLUDE", LenCpy + cpy + 7)
    End If
    LenCpy = 0
    Count = Count + 1
  Wend
  
  If Not CopyFind Then
    LSW.ListItems.Add , , "No Copy Find to expand..."
  End If
  
  If Not (collExpandedCopy Is Nothing) Then
    If nrExpandedCopy <> collExpandedCopy.Count Then
      nrExpandedCopy = collExpandedCopy.Count
      idLevel = idLevel + 1
    Else
      MsgBox "All copy levels expandend", vbInformation, "Expand copy"
    End If
  End If
End Sub

Public Sub Expand_cpy(LSW As ListView, RTApp As RichTextBox, RTSource As RichTextBox)
  Dim RTPosition As Long, RTFine As Long
  Dim i As Long
  Dim cpy As Long
  Dim Accapo As Long
  Dim NomeCOPY As String
  Dim FlagStart As Boolean
  Dim CopyPath  As String
  Dim CopyFind As Boolean
  
  Dim MarcatoreCPY As String
  Dim Riga As Long
  Dim LenCpy As Long
  Dim Count As Long
  
  Dim rigaIn As Long, rigaOut As Long
  
  FlagStart = False
  LSW.ListItems.Add , , "Expanding Copy..."
  
  'Cerca le copy nella RichTextBox
  cpy = RTSource.find(" COPY ", 1)
  CopyFind = False
  
  While cpy <> -1
    NomeCOPY = ""
    
    'Controlla che non sia asteriscato
    For i = 1 To 100
      RTSource.SelStart = cpy - i
      RTSource.SelLength = 2
      
      If RTSource.SelText = vbCrLf Then
        Accapo = cpy - i + 1
        Exit For
      End If
    Next i
    
    RTSource.SelStart = Accapo + 6
    RTSource.SelLength = 1
    
    If RTSource.SelText <> "*" Then
      'Trova il nome della Copy
      For i = 1 To 100
        RTSource.SelStart = cpy + 4 + i
        RTSource.SelLength = 1
        
        If RTSource.SelText <> " " And RTSource.SelText <> "." And RTSource.SelText <> "'" Then
          NomeCOPY = NomeCOPY & RTSource.SelText
          FlagStart = True
        Else
          If FlagStart Then
            Exit For
          End If
        End If
      Next i
        
      'Toglie eventuali punti
      If Mid(NomeCOPY, Len(NomeCOPY), 1) = "." Then
        NomeCOPY = Mid(NomeCOPY, 1, Len(NomeCOPY) - 1)
      End If
       
      'Toglie eventuali apici
      If Mid(NomeCOPY, Len(NomeCOPY), 1) = "'" Then
        NomeCOPY = Mid(NomeCOPY, 1, Len(NomeCOPY) - 1)
      End If
       
      If Mid(NomeCOPY, 1, 1) = "'" Then
        NomeCOPY = Mid(NomeCOPY, 2)
      End If
       
      'Prende il path della copy
      CopyPath = Restituisci_Percorso_Da_NomeOggetto(NomeCOPY)
      Riga = RTSource.GetLineFromChar(cpy)
      MarcatoreCPY = "C" & String(5 - Len(Hex(Riga)), "0") & Hex(Riga)
       
      If Trim(CopyPath) <> "" Then
        'Espande la copy :
        'CARICA LA COPY NELLA TEXT DI APPOGGIO
        RTApp.LoadFile CopyPath
        LenCpy = Len(RTApp.text)
        'trova RTPosition
        For i = 1 To 100
          RTSource.SelStart = cpy + i
          RTSource.SelLength = 2
           
          If RTSource.SelText = vbCrLf Then
            RTPosition = cpy + i + 2
            Exit For
          End If
        Next i
          
        'Copia la copy nella RichTextBox principale
        RTSource.SelStart = RTPosition
        RTSource.SelLength = 0
        
        RTSource.SelColor = vbWhite
        RTSource.SelText = MarcatoreCPY & vbCrLf & RTApp.text & vbCrLf & "CPYFN" & vbCrLf
        RTFine = RTPosition + Len(RTApp.text) + 11
        
        'aggiorna linee istruzioni ALE
        rigaIn = Riga 'RTSource.GetLineFromChar(RTPosition)
        rigaOut = RTSource.GetLineFromChar(RTFine)
        aggiornaLineeIstruz rigaIn, rigaOut
        
        RTSource.SelStart = RTPosition
        RTSource.SelLength = 0
        RTSource.SelColor = vbWhite
        
        LSW.ListItems.Add , , "Copy " & NomeCOPY & " Expanded..."
        LSW.ListItems(LSW.ListItems.Count).tag = "EXPD" & MarcatoreCPY & CopyPath
        
        CopyFind = True
      Else
        If NomeCOPY <> ":" And NomeCOPY <> "=" And NomeCOPY <> " " Then
          LSW.ListItems.Add , , "Copy " & NomeCOPY & " Not Found..."
          LSW.ListItems(LSW.ListItems.Count).tag = "EXPE" & MarcatoreCPY & NomeCOPY
        End If
      End If
    Else
      LenCpy = 0
    End If
    
    FlagStart = False
    cpy = RTSource.find(" COPY ", LenCpy + 14 + cpy + 6)
    Count = Count + 1
  Wend
  
  If Not CopyFind Then
    LSW.ListItems.Add , , "No Copy Find to expand..."
  End If
End Sub

Public Sub Expand_INC(LSW As ListView, RTApp As RichTextBox, RTSource As RichTextBox)
  Dim RTPosition As Long, RTFine As Long
  Dim i As Long, Inc As Long
  Dim Accapo As Long
  Dim NomeINCLUDE As String
  Dim FlagStart As Boolean
  Dim IncludePath  As String
  Dim IncludeFind As Boolean
  
  Dim MarcatoreINC As String
  Dim Riga As Long
  Dim LenInc As Long
  Dim Count As Long
  
  Dim rigaIn As Long, rigaOut As Long
  
  FlagStart = False
  LSW.ListItems.Add , , "Expanding Include..."
  
  'Cerca le include nella RichTextBox
  Inc = RTSource.find(" INCLUDE ", 1)
  IncludeFind = False
  
  While Inc <> -1
    NomeINCLUDE = ""
    
    'Controlla che non sia asteriscato
    For i = 1 To 100
      RTSource.SelStart = Inc - i
      RTSource.SelLength = 2
      
      If RTSource.SelText = vbCrLf Then
        Accapo = Inc - i + 1
        Exit For
      End If
    Next i
    
    RTSource.SelStart = Accapo + 6
    RTSource.SelLength = 1
    
    If RTSource.SelText <> "*" Then
      'Trova il nome dell'Include
      For i = 1 To 100
        RTSource.SelStart = Inc + 7 + i
        RTSource.SelLength = 1
        
        If RTSource.SelText <> " " And RTSource.SelText <> "." And RTSource.SelText <> "'" Then
          NomeINCLUDE = NomeINCLUDE & RTSource.SelText
          FlagStart = True
        Else
          If FlagStart Then
            Exit For
          End If
        End If
      Next i
        
      'Toglie eventuali punti
      If Mid(NomeINCLUDE, Len(NomeINCLUDE), 1) = ";" Then
        NomeINCLUDE = Left(NomeINCLUDE, Len(NomeINCLUDE) - 1)
      End If
       
      'Toglie eventuali apici
      If Mid(NomeINCLUDE, Len(NomeINCLUDE), 1) = "'" Then
        NomeINCLUDE = Left(NomeINCLUDE, Len(NomeINCLUDE) - 1)
      End If
      
      If Mid(NomeINCLUDE, 1, 1) = "'" Then
        NomeINCLUDE = Mid(NomeINCLUDE, 2)
      End If
       
      'Prende il path dell'include
      IncludePath = Restituisci_Percorso_Da_NomeOggetto(NomeINCLUDE)
      Riga = RTSource.GetLineFromChar(Inc)
      MarcatoreINC = "C" & String(5 - Len(Hex(Riga)), "0") & Hex(Riga)
       
      If Trim(IncludePath) <> "" Then
        'Espande l'include :
        'CARICA L'INCLUDE NELLA TEXT DI APPOGGIO
        RTApp.LoadFile IncludePath
        LenInc = Len(RTApp.text)
        
        'trova RTPosition
        For i = 1 To 100
          RTSource.SelStart = Inc + i
          RTSource.SelLength = 2
          
          If RTSource.SelText = vbCrLf Then
            RTPosition = Inc + i + 2
            Exit For
          End If
        Next i
        
        'Copia l'include nella RichTextBox principale
        RTSource.SelStart = RTPosition
        RTSource.SelLength = 0
        
        RTSource.SelColor = vbWhite
        RTSource.SelText = MarcatoreINC & vbCrLf & RTApp.text & vbCrLf & "INCFN" & vbCrLf
        
        
        RTFine = RTPosition + Len(RTApp.text) + 11
        
        'aggiorna linee istruzioni ALE
        rigaIn = Riga 'RTSource.GetLineFromChar(RTPosition)
        rigaOut = RTSource.GetLineFromChar(RTFine)
        aggiornaLineeIstruz rigaIn, rigaOut
        
        RTSource.SelStart = RTPosition
        RTSource.SelLength = 0
        RTSource.SelColor = vbWhite
        
        LSW.ListItems.Add , , "Copy " & NomeINCLUDE & " Expanded..."
        LSW.ListItems(LSW.ListItems.Count).tag = "EXPD" & MarcatoreINC & IncludePath
        
        IncludeFind = True
      Else
        If NomeINCLUDE <> ":" And NomeINCLUDE <> "=" And NomeINCLUDE <> " " Then
          LSW.ListItems.Add , , "Include " & NomeINCLUDE & " Not Found..."
          LSW.ListItems(LSW.ListItems.Count).tag = "EXPE" & MarcatoreINC & NomeINCLUDE
        End If
      End If
    Else
      LenInc = 0
    End If
    FlagStart = False
    Inc = RTSource.find(" INCLUDE ", LenInc + 14 + Inc + 9)
    Count = Count + 1
  Wend
  
  If Not IncludeFind Then
    LSW.ListItems.Add , , "No Include Find to expand..."
  End If
End Sub

Public Sub aggiornaLineeIstruz(pIn As Long, pOut As Long)
  Dim i As Long
  
  On Error GoTo Err_Handler
  
  For i = 1 To UBound(lineeIstr)
    If lineeIstr(i) > pIn Then
      lineeIstr(i) = lineeIstr(i) + (pOut - pIn)
    End If
  Next i
     
  Exit Sub
Err_Handler:
  If err.Number = 9 Then
    Resume Next
  End If
End Sub

Public Function Carica_TreeView_Dettaglio_Istruzioni(TR As TreeView, Riga As Long) As Boolean
  'Oltre a caricare la treeview ritorna un valore booleano che indica se l'istruzione � valida o meno
  Dim i As Long
  Dim wApp As String
  Dim j As Long
  Dim sKey As String, Kiave As String, grpKey As String, ParKey As String, grpKey2 As String
  
  Dim ImgIcon As Long
    
  On Error GoTo errorHandler

  'Carica la prima parte dell'array DBD,PSB ecc...
  TR.Nodes.Clear
  TR.Nodes.Add , , "ISTR ", "Istruction Information :", 14
  TR.Nodes.Add "ISTR ", tvwChild, , "Object : " & Trim(Restituisci_NomeOgg_Da_IdOggetto(Dli2Rdbms.drIdOggetto)), 15
  TR.Nodes.Add "ISTR ", tvwChild, , "Istruction : " & Trim(Istruzione.Tipologia), 16
  
  If Istruzione.ImsDB Then
    TR.Nodes.Add "ISTR ", tvwChild, , "Istruction Type : IMS/DB", 18
  ElseIf Istruzione.ImsDC Then
    TR.Nodes.Add "ISTR ", tvwChild, , "Istruction Type : IMS/DC", 18
  Else
    TR.Nodes.Add "ISTR ", tvwChild, , "Istruction Type : UNKNOWN", DIVIETO
  End If
  
  TR.Nodes.Add "ISTR ", tvwChild, , "Line : " & Trim(str(Istruzione.Riga)), 13
 
  Select Case Trim(UCase(Istruzione.Tipologia))
    Case "TERM", "PCB", "ROLL", "ROLB", "SYNC"
      '
    Case Else
      If UBound(Istruzione.Database) > 0 Then
        For i = 1 To UBound(Istruzione.Database)
          wApp = wApp & ";" & Istruzione.Database(i).nome
        Next i
        TR.Nodes.Add "ISTR ", tvwChild, "DBD  ", "DBD : " & Mid(wApp, 2), 3
      Else
        If Istruzione.ImsDB Then
          TR.Nodes.Add "ISTR ", tvwChild, "DBD  ", "DBD : ", DIVIETO
        End If
      End If
  End Select
  
  If UBound(Istruzione.psb) > 0 Then
    wApp = ""
    For i = 1 To UBound(Istruzione.psb)
      wApp = wApp & ";" & Istruzione.psb(i).nome
    Next i
    TR.Nodes.Add "ISTR ", tvwChild, "PSB  ", "PSB : " & Mid(wApp, 2), 3
  Else
    TR.Nodes.Add "ISTR ", tvwChild, "PSB  ", "PSB : ", DIVIETO
  End If
  
  Select Case Trim(UCase(Istruzione.Tipologia))
    Case "TERM", "PCB", "ROLL", "ROLB", "OPEN", "CLSE", "REST", "SYNC"
      Exit Function
  End Select
  
  If UBound(Istruzione.SSA) Then
    For i = 1 To UBound(Istruzione.SSA)
      ParKey = "LEV" & Format(Istruzione.SSA(i).livello, "00")
  
      '***********************
      '**** L I V E L L I ****
      '***********************
      TR.Nodes.Add , , ParKey, "LEVEL : " & Format(Istruzione.SSA(i).livello, "00"), 2
      
      If Istruzione.ImsDB Then
        '*************************
        '**** S E G M E N T O ****
        '*************************
        Kiave = "SEG" & Format(i, "00")
        If Trim(Istruzione.SSA(i).NomeSegmento) <> "" Then
          ImgIcon = BAFFETTO
        Else
          ImgIcon = DIVIETO
        End If
    
        'TR.Nodes.Add ParKey, tvwChild, Kiave, "Segment : " & Istruzione.SSA(i).NomeSegmento, ImgIcon
        If Len(Istruzione.SSA(i).NomeSegmentiD) Then
          TR.Nodes.Add ParKey, tvwChild, Kiave, "Segment : " & Istruzione.SSA(i).NomeSegmentiD, ImgIcon
        Else
          TR.Nodes.Add ParKey, tvwChild, Kiave, "Segment : " & Istruzione.SSA(i).NomeSegmento, ImgIcon
        End If
        
        '*********************
        '**** S E G L E N ****
        '*********************
        Kiave = "SGL" & Format(i, "00")
        If Trim(Istruzione.SSA(i).LenSegmento) <> "" Then
          ImgIcon = BAFFETTO
        Else
          ImgIcon = DIVIETO
        End If
        
        If Len(Istruzione.SSA(i).LenSegmentiD) Then
          TR.Nodes.Add ParKey, tvwChild, Kiave, "Segment Len: " & Istruzione.SSA(i).LenSegmentiD, ImgIcon
        Else
          TR.Nodes.Add ParKey, tvwChild, Kiave, "Segment Len: " & Istruzione.SSA(i).LenSegmento, ImgIcon
        End If
      End If
      
      ' Mauro 16/10/2007 : La struttura non � obbligatoria
      ' Non devo mettere il divietino se non c'�
      '***************************
      '**** S T R U T T U R A ****
      '***************************
      'If Trim(Istruzione.SSA(i).Struttura) <> "" Then
        ImgIcon = BAFFETTO
      'Else
      '  ImgIcon = 11
      'End If
      
      '*******************************
      Dim rs As Recordset
      Dim ordinale As Integer
      Dim BolMoved As Boolean
    
      BolMoved = False
      'SQ - 13-12-05: gestione strutture tipo:
      '01 PIPPO.
      '   COPY BLUTO.
      'N.B.: l'ordinale di BLUTO parte da 1, come il livello 01... aggiungo AUX nella select per fare un ordinamento corretto... (order by UNICO sulla UNION)
      'Set rs = m_Fun.Open_Recordset("Select * From PsData_Cmp Where idOggetto = " & wIdOggetto & " And IdArea = " & wIdArea & " And ordinale >= " & ordinale & " order by Ordinale")
    
      'stefano: per ora lo prendo dal moved caricato: perch� no?
  '    Set rs = m_fun.Open_Recordset("Select Ordinale From PsData_Cmp Where idOggetto = " & Istruzione.SSA(i).IdOggetto & " And IdArea = " & Istruzione.SSA(i).IdArea & " And Nome = '" & Trim(Istruzione.SSA(i).SSAName) & "'")
  '    If rs.RecordCount > 0 Then
  '      ordinale = rs!ordinale
  '    End If
  '    rs.Close
  '
  '
  '    Dim sqlUnion As String
  '
  '        Set rs = m_fun.Open_Recordset("SELECT IdCopy from PsData_RelCpy where IdOggetto = " & Istruzione.SSA(i).IdOggetto & " And IdArea = " & Istruzione.SSA(i).IdArea)
  '        If Not rs.EOF Then
  '
  '        sqlUnion = " UNION SELECT 2 as AUX,ordinale,livello,segno,byte,lunghezza,valore,nomeRed,nome From PsData_Cmp " & _
  '                                "  where idOggetto = " & rs!idCopy & " And IdArea = " & Istruzione.SSA(i).IdArea & " and Moved order by AUX,ORDINALE"
  '        End If
  '        rs.Close
  '        Set rs = m_fun.Open_Recordset("SELECT 1 as AUX ,ordinale as ORDINALE,livello,segno,byte,lunghezza,valore,nomeRed,nome From PsData_Cmp Where idOggetto = " & Istruzione.SSA(i).IdOggetto & " And IdArea = " & Istruzione.SSA(i).IdArea & " And ordinale >= " & ordinale & _
  '                                " and Moved " & sqlUnion)
  '    If Not rs.EOF Then
  '         BolMoved = True
  '    End If
  '    rs.Close
  'stefano: lo carico da quello del db, serve a poco, ma comunque....
      BolMoved = Istruzione.SSA(i).Moved
      
      '*******************************
      
      Kiave = "STR" & Format(i, "00")
      TR.Nodes.Add ParKey, tvwChild, Kiave, "Data Area : " & Istruzione.SSA(i).Struttura, ImgIcon
      Kiave = "MAP" & Format(i, "00")
      If Istruzione.ImsDC And Istruzione.Tipologia = "ISRT" Then
          TR.Nodes.Add ParKey, tvwChild, Kiave, "MapName : " & Istruzione.MapName, ImgIcon
      End If
      '*************************
      '**** S S A   N A M E ****
      '*************************
      Kiave = "SSA" & Format(i, "00")
    
      If Trim(Istruzione.SSA(i).ssaName) <> "" Or Istruzione.calltype = "EXEC" Then
        If Not BolMoved Then
          ImgIcon = BAFFETTO
        Else
          ImgIcon = BAFFETTO_MOVED
        End If
      Else
        If Not BolMoved Then
          ImgIcon = DIVIETO
        Else
          ImgIcon = DIVIETO_MOVED
        End If
      End If
        
      TR.Nodes.Add ParKey, tvwChild, Kiave, "SSA Name : " & Istruzione.SSA(i).ssaName, ImgIcon
      'If Trim(UCase(Istruzione.SSA(i).ssaName)) <> "" Then
        If MadrdF_Corrector.ChkQual.Value Then
           If UBound(Istruzione.SSA(i).Chiavi) > 1 Then
             'Crea il nodo contenete le chiavi dell'SSA
             grpKey = Kiave
             
             For j = 1 To UBound(Istruzione.SSA(i).Chiavi)
                '*********************
                '**** C H I A V I ****
                '*********************
                Kiave = "KEY" & Format(j, "00")
                
                If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                  ImgIcon = BAFFETTO
                Else
                  ImgIcon = DIVIETO
                End If
                
                sKey = Istruzione.SSA(i).Chiavi(j).key
                
                
                TR.Nodes.Add grpKey, tvwChild, Kiave, "Key : " & sKey, ImgIcon
                TR.Nodes(TR.Nodes.Count).tag = j
                
                grpKey2 = Kiave
                
                '*************************
                '**** K E Y S T A R T ****
                '*************************
                 Kiave = "KYS" & Format(j, "00")
             
                 If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                   ImgIcon = BAFFETTO
                 Else
                   ImgIcon = DIVIETO
                 End If
                 
                 'SG : Concatena nella treeview le chiavi se sono + di una
                 sKey = Istruzione.SSA(i).Chiavi(j).Valore.KeyStart
                 
                 TR.Nodes.Add grpKey2, tvwChild, Kiave, "Key Start : " & sKey, ImgIcon
                 TR.Nodes(TR.Nodes.Count).tag = j
                 
                 '*********************
                 '**** K E Y L E N ****
                 '*********************
                 Kiave = "KYL" & Format(j, "00")
             
                 If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                   ImgIcon = BAFFETTO
                 Else
                   ImgIcon = DIVIETO
                 End If
                 
                 'SG : Concatena nella treeview le chiavi se sono + di una
                 sKey = Istruzione.SSA(i).Chiavi(j).Valore.Lunghezza
                 
                 TR.Nodes.Add grpKey2, tvwChild, Kiave, "Key Len : " & sKey, ImgIcon
                 TR.Nodes(TR.Nodes.Count).tag = j
                 
                 '***************************
                 '**** O P E R A T O R I ****
                 '***************************
                 Kiave = "OPE" & Format(j, "00")
             
                 If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                   ImgIcon = BAFFETTO
                 Else
                   ImgIcon = DIVIETO
                 End If
                 
                 'SG : Concatena nella treeview le chiavi se sono + di una
                 sKey = Istruzione.SSA(i).Chiavi(j).Operatore
                 
                 TR.Nodes.Add grpKey2, tvwChild, Kiave, "Operators : " & sKey, ImgIcon
                 TR.Nodes(TR.Nodes.Count).tag = j
                 
                 '*********************
                 '**** V A L O R I ****
                 '*********************
                 Kiave = "VAL" & Format(j, "00")
             
                 If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                   ImgIcon = BAFFETTO
                 Else
                   ImgIcon = DIVIETO
                 End If
                 
                  'SG : Concatena nella treeview le chiavi se sono + di una
                 sKey = Trim(Istruzione.SSA(i).Chiavi(j).Valore.Valore)
                 
                 TR.Nodes.Add grpKey2, tvwChild, Kiave, "Key Value: " & sKey, ImgIcon
                 TR.Nodes(TR.Nodes.Count).tag = j
                 
                 '*********************
                 '**** V A L K E Y ****
                 '*********************
                 Kiave = "KYV" & Format(j, "00")
             
                 If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                   ImgIcon = BAFFETTO
                 Else
                   ImgIcon = DIVIETO
                 End If
                 
                  'SG : Concatena nella treeview le chiavi se sono + di una
                 sKey = Trim(Istruzione.SSA(i).Chiavi(j).Valore.FieldValue)
                 
                 TR.Nodes.Add grpKey2, tvwChild, Kiave, "Value : " & sKey, ImgIcon
                 TR.Nodes(TR.Nodes.Count).tag = j
                 
                '*************************
                '**** O P L O G I C O ****
                '*************************
                If UBound(Istruzione.SSA(i).OpLogico) >= j Then
                   Kiave = "OPL" & Format(i, "00")
                   
                   If UBound(Istruzione.SSA(i).OpLogico) > 0 Then
                      ImgIcon = BAFFETTO
                   Else
                      ImgIcon = DIVIETO
                   End If
                    
                   'SG : Concatena gli operatori
                   sKey = Trim(Istruzione.SSA(i).OpLogico(j))
                   
                   TR.Nodes.Add grpKey, tvwChild, Kiave, "Logic Op : " & sKey, ImgIcon
                   TR.Nodes(TR.Nodes.Count).tag = j
                End If
             Next j
           Else
             If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                j = 1
                grpKey = Kiave
                
                'C'� una sola chiave (non booleana)
                '*********************
                '**** C H I A V I ****
                '*********************
                Kiave = "KEY" & Format(j, "00") & Format(i, "00")
                
                If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                  ImgIcon = BAFFETTO
                Else
                  ImgIcon = DIVIETO
                End If
                
                sKey = Istruzione.SSA(i).Chiavi(j).key
                
                TR.Nodes.Add grpKey, tvwChild, Kiave, "Key : " & sKey, ImgIcon
                TR.Nodes(TR.Nodes.Count).tag = j
                
                grpKey2 = Kiave
                
                '*************************
                '**** K E Y S T A R T ****
                '*************************
                 Kiave = "KYS" & Format(j, "00") & Format(i, "00")
             
                 If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                   ImgIcon = BAFFETTO
                 Else
                   ImgIcon = DIVIETO
                 End If
                 
                 'SG : Concatena nella treeview le chiavi se sono + di una
                 sKey = Istruzione.SSA(i).Chiavi(j).Valore.KeyStart
                 
                 TR.Nodes.Add grpKey, tvwChild, Kiave, "Key Start : " & sKey, ImgIcon
                 TR.Nodes(TR.Nodes.Count).tag = j
                 
                 '*********************
                 '**** K E Y L E N ****
                 '*********************
                 Kiave = "KYL" & Format(j, "00") & Format(i, "00")
             
                 If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                   ImgIcon = BAFFETTO
                 Else
                   ImgIcon = DIVIETO
                 End If
                 
                 'SG : Concatena nella treeview le chiavi se sono + di una
                 sKey = Istruzione.SSA(i).Chiavi(j).Valore.Lunghezza
                 
                 TR.Nodes.Add grpKey, tvwChild, Kiave, "Key Len : " & sKey, ImgIcon
                 TR.Nodes(TR.Nodes.Count).tag = j
                 
                 '***************************
                 '**** O P E R A T O R I ****
                 '***************************
                 Kiave = "OPE" & Format(j, "00") & Format(i, "00")
             
                 If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                   ImgIcon = BAFFETTO
                 Else
                   ImgIcon = DIVIETO
                 End If
                 
                 'SG : Concatena nella treeview le chiavi se sono + di una
                 sKey = Istruzione.SSA(i).Chiavi(j).Operatore
                 
                 TR.Nodes.Add grpKey, tvwChild, Kiave, "Operators : " & sKey, ImgIcon
                 TR.Nodes(TR.Nodes.Count).tag = j
                 
                 '*********************
                 '**** V A L O R I ****
                 '*********************
                 Kiave = "VAL" & Format(j, "00") & Format(i, "00")
             
                 If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                   ImgIcon = BAFFETTO
                 Else
                   ImgIcon = DIVIETO
                 End If
                 
                  'SG : Concatena nella treeview le chiavi se sono + di una
                 sKey = Trim(Istruzione.SSA(i).Chiavi(j).Valore.Valore)
                 
                 TR.Nodes.Add grpKey, tvwChild, Kiave, "Key Value: " & sKey, ImgIcon
                 TR.Nodes(TR.Nodes.Count).tag = j
                 
                 '*********************
                 '**** V A L K E Y ****
                 '*********************
                 Kiave = "KYV" & Format(j, "00") & Format(i, "00")
             
                 If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                   ImgIcon = BAFFETTO
                 Else
                   ImgIcon = DIVIETO
                 End If
                 
                  'SG : Concatena nella treeview le chiavi se sono + di una
                 ''''''sKey = Trim(Istruzione.SSA(i).Chiavi(j).Valore.FieldValue)
                 ''''''TR.Nodes.Add grpKey, tvwChild, Kiave, "Value: " & sKey, ImgIcon
                 ''''''TR.Nodes(TR.Nodes.Count).Tag = j
             End If
           End If  ' ChkQual
         End If
      'End If 'SSA = <NONE>
      
      '*****************************
      '**** C O N D I Z I O N E ****
      '*****************************
      Kiave = "CON" & Format(i, "00")
  
     ' If Trim(Istruzione.SSA(i).CommandCode) <> "" Then
        ImgIcon = BAFFETTO
      'Else
     '   ImgIcon = 11
     ' End If
  
      TR.Nodes.Add ParKey, tvwChild, Kiave, "Comm. Codes: " & Istruzione.SSA(i).CommandCode, ImgIcon
    Next i
  End If
  
  'Carica l'array delle immagini
  ReDim Mappa_Immagini_TreeView(0)

  For i = 1 To TR.Nodes.Count
    ReDim Preserve Mappa_Immagini_TreeView(UBound(Mappa_Immagini_TreeView) + 1)
    Mappa_Immagini_TreeView(UBound(Mappa_Immagini_TreeView)) = TR.Nodes(i).Image
  Next i

  'Restituisce il valore
  Carica_TreeView_Dettaglio_Istruzioni = Istruzione.valida
  
Exit Function
errorHandler:
  If err.Number = 35602 Then
    Kiave = Kiave & Format(j, "00")
    Resume
  End If
End Function

Public Sub Controlla_Moltiplicita_SSA_Di_Livello(Id As Variant, Riga As Long)
    'verr� inserita nella routine che si lancia quando si clicca sui vari livelli
    Dim r As Recordset

    '***********************
    '*** S E G M E N T I ***
    '***********************
    Set r = m_fun.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'SG' AND Riga = " & Riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst
      
      ReDim SEGMENTI(0)
      
      If r.RecordCount > 1 Then
        While Not r.EOF
          'Carica la type dei Segmenti
          ReDim Preserve SEGMENTI(UBound(SEGMENTI) + 1)
          SEGMENTI(UBound(SEGMENTI)).IdOggetto = r!IdOggetto
          SEGMENTI(UBound(SEGMENTI)).Riga = r!Riga
          SEGMENTI(UBound(SEGMENTI)).livello = r!livello
          SEGMENTI(UBound(SEGMENTI)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
 
    '***********************
    '*** F I E L D (KEY) ***
    '***********************
    Set r = m_fun.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'FL' AND Riga = " & Riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst
      
      ReDim Field(0)
      
      If r.RecordCount > 1 Then
        While Not r.EOF
          'carica la type
          ReDim Preserve Field(UBound(Field) + 1)
          Field(UBound(Field)).IdOggetto = r!IdOggetto
          Field(UBound(Field)).Riga = r!Riga
          Field(UBound(Field)).livello = r!livello
          Field(UBound(Field)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
    

    '********************************
    '*** F I E L D L E N (KEYLEN) ***
    '********************************
    Set r = m_fun.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'FN' AND Riga = " & Riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst
      
      ReDim FIELDLEN(0)
      
      If r.RecordCount > 1 Then
        While Not r.EOF
          'carica la type
          ReDim Preserve FIELDLEN(UBound(FIELDLEN) + 1)
          FIELDLEN(UBound(FIELDLEN)).IdOggetto = r!IdOggetto
          FIELDLEN(UBound(FIELDLEN)).Riga = r!Riga
          FIELDLEN(UBound(FIELDLEN)).livello = r!livello
          FIELDLEN(UBound(FIELDLEN)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
    
    '*************************
    '*** O P E R A T O R I ***
    '*************************
    Set r = m_fun.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'OP' AND Riga = " & Riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst

      If r.RecordCount > 1 Then
        ReDim OPERATORI(0)
        
        While Not r.EOF
          'carica la type
          ReDim Preserve OPERATORI(UBound(OPERATORI) + 1)
          OPERATORI(UBound(OPERATORI)).IdOggetto = r!IdOggetto
          OPERATORI(UBound(OPERATORI)).Riga = r!Riga
          OPERATORI(UBound(OPERATORI)).livello = r!livello
          OPERATORI(UBound(OPERATORI)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
    
    '*********************************
    '*** Q U A L I F I C A T O R I ***
    '*********************************
    Set r = m_fun.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'PA' AND Riga = " & Riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst

      If r.RecordCount > 1 Then
        ReDim QUALIFICATORI(0)
        While Not r.EOF
          'carica la type
          ReDim Preserve QUALIFICATORI(UBound(QUALIFICATORI) + 1)
          QUALIFICATORI(UBound(QUALIFICATORI)).IdOggetto = r!IdOggetto
          QUALIFICATORI(UBound(QUALIFICATORI)).Riga = r!Riga
          QUALIFICATORI(UBound(QUALIFICATORI)).livello = r!livello
          QUALIFICATORI(UBound(QUALIFICATORI)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
    
    '*******************
    '*** V A L O R I ***
    '*******************
    Set r = m_fun.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'VL' AND Riga = " & Riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst

      If r.RecordCount > 1 Then
        ReDim VALORI(0)
        While Not r.EOF
          'carica la type
          ReDim Preserve VALORI(UBound(VALORI) + 1)
          VALORI(UBound(VALORI)).IdOggetto = r!IdOggetto
          VALORI(UBound(VALORI)).Riga = r!Riga
          VALORI(UBound(VALORI)).livello = r!livello
          VALORI(UBound(VALORI)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
End Sub

Public Sub Seleziona_Word_E_MarcaWord(RT As RichTextBox, LSW As ListView)
  Dim Marcatore As String
  Dim i As Long
  Dim Riga As Long
  
  Dim LenWord As Long
  
  If Trim(RT.SelText) <> "" Then
    AUSelText = Trim(RT.SelText)
    
    LenWord = RT.SelLength
  
    Riga = RT.GetLineFromChar(RT.SelStart)
    AURigaSel = Riga
    Marcatore = "S" & String(5 - Len(Hex(Riga)), "0") & Hex(Riga)
    
    'LSW.ListItems.Add , , "Select Text : [" & AUSelText & "]"
    LSW.ListItems(LSW.ListItems.Count).tag = "SELT" & Marcatore
    
    Marca_Linea_Da_Posizione RT, RT.SelStart, Marcatore, LenWord
    
    LSW.ListItems(LSW.ListItems.Count).EnsureVisible
  End If
End Sub

Public Sub Avvia_Ricerca_Ricorsiva_Parola(RT As RichTextBox, LSW As ListView, Parola As String)
  Dim Idx As Long, IdxFine As Long
  Dim Marcatore As String
  Dim Riga As Long
  Dim LenWord As Long, LenRiga As Long
  
  Idx = RT.find(Parola, 1)
  LSW.ListItems.Add , , "Select Text : [" & AUSelText & "]"
  
  While Idx <> -1
    Riga = RT.GetLineFromChar(Idx)
    Marcatore = "S" & String(5 - Len(Hex(Riga)), "0") & Hex(Riga)
    
    'MF 02/08/07
    'Visualizza l'intera riga in cui viene trovata la Parola cercata
   
    LenWord = Len(Parola)
    
    If Riga <> AURigaSel Then
      IdxFine = RT.find(vbCrLf, Idx)
      Marca_Linea_Da_Posizione RT, Idx, Marcatore, LenWord
      
      RT.SelStart = RTStart
      RT.SelLength = IdxFine - RTStart
      LSW.ListItems.Add , , "Line " & Riga & " : " & Trim(Left(RT.SelText, 80))
      LSW.ListItems(LSW.ListItems.Count).tag = "FIND" & Marcatore
    End If
    
    Idx = RT.find(Parola, Idx + Len(Parola))
  Wend

  LSW.ListItems(LSW.ListItems.Count).EnsureVisible
End Sub
'' 16/02/2010:gloria
'Public Sub Marca_Linea_Da_Posizione(RT As RichTextBox, pos As Long, Marcatore As String, LenWord As Long)
'  Dim i As Long, j As Long
'
'  For i = 1 To 1000
'    If pos < i Then Exit Sub
'
'    RT.SelStart = pos - i
'    RT.SelLength = 2
'
'    If RT.SelText = vbCrLf Then
'      RT.SelStart = pos - i + 1
'      RT.SelLength = 6
'      RT.SelText = Marcatore
'      Exit For
'    End If
'  Next i
'
'  RTStart = RT.SelStart
'
'  RT.SelStart = pos
'  RT.SelLength = LenWord
'End Sub

'mette il tag a fine riga
Public Sub Marca_Linea_Da_Posizione(RT As RichTextBox, pos As Long, Marcatore As String, LenWord As Long)
  Dim i As Long, j As Long
  
  For i = 1 To 1000
    If pos < i Then Exit Sub

    RT.SelStart = pos - i
    RT.SelLength = 2

    If RT.SelText = vbCrLf Then
     
    ' cerca vbcrlf succ
      RT.SelStart = pos - i + 2
      While Not RT.SelText = vbCrLf
        RT.SelStart = RT.SelStart + 2
        RT.SelLength = 2
      Wend
      RT.SelText = Marcatore & vbCrLf
      RT.SelStart = RT.SelStart - 8
      RT.SelLength = 6
      RT.SelColor = RT.BackColor
      RTStart = pos - i + 1
      Exit For
    End If
  Next i

'  RTStart = RT.SelStart
  RT.SelStart = pos
  RT.SelLength = LenWord
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ex Carica_Array_Istruzione - NON ERA USATO COME ARRAY!!!!!!!!!!!!!!!!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub Carica_Istruzione(idPgm As Long, Riga As Long, IdOggetto As Long)
  Dim i As Long, k As Long, j As Long, w As Long, y As Long, Q As Long
  Dim r As Recordset, rGen As Recordset, rAll As Recordset, rSeg As Recordset
  Dim rQualif As Recordset, rStrutt As Recordset, rKey As Recordset, rOp As Recordset
  Dim rVal As Recordset, rCond As Recordset
  
  Dim TotLivelli As Long
  
  Dim nomeSegm As String
  Dim LungSegmento As String
  Dim NomeStr As String
  Dim sDbd() As Long
  Dim sPsb() As Long
  
  ReDim Istruzione.SSA(0)
  
  i = 1 'Contatore SSA
  y = 1 'Contatore Struttura
  
  'Prende le informazioni base dell'istruzione
  Set r = m_fun.Open_Recordset("Select * From PSDli_Istruzioni Where " & _
                                        "IdPgm = " & idPgm & " And IdOggetto = " & IdOggetto & " And Riga = " & Riga)
  If r.RecordCount Then
    ReDim Preserve Istruzione.SSA(0)
    Istruzione.Istruzione = r!statement
    Istruzione.Tipologia = IIf(IsNull(r!Istruzione), "", r!Istruzione)
    '2-5-06
    Istruzione.pcbName = IIf(IsNull(r!numpcb), "", r!numpcb)
    If Not IsNull(r!MapName) Then
      Istruzione.MapName = r!MapName
    Else
      Istruzione.MapName = ""
    End If
    Istruzione.Riga = Riga
    Istruzione.valida = r!valida
    Istruzione.to_migrate = r!to_migrate
    Istruzione.obsolete = r!obsolete
    If r!isExec Then
      Istruzione.calltype = "EXEC"
    End If
    sDbd = getDBDByIdOggettoRiga(r!idPgm, r!IdOggetto, r!Riga)
   
    For w = 0 To UBound(sDbd)
      ReDim Preserve Istruzione.Database(w)
      Istruzione.Database(w).IdOggetto = sDbd(w)
      Istruzione.Database(w).nome = Restituisci_NomeOgg_Da_IdOggetto(sDbd(w))
      Istruzione.Database(w).tipo = TYPE_DBD
    Next w
    
    'Setta l'id database univoco
    If UBound(sDbd) = 1 Then gIdDbd = sDbd(1)
    
    sPsb = getPSBByIdOggettoRiga(r!idPgm, r!IdOggetto, r!Riga)
     
    For w = 0 To UBound(sPsb)
      ReDim Preserve Istruzione.psb(w)
      Istruzione.psb(w).IdOggetto = sPsb(w)
      Istruzione.psb(w).nome = Restituisci_NomeOgg_Da_IdOggetto(sPsb(w))
      Istruzione.psb(w).tipo = TYPE_PSB
    Next w
    
    Istruzione.ImsDB = r!ImsDB
    Istruzione.ImsDC = r!ImsDC
    
    'AC
    ' Mauro 16/10/2007 : Aggiunto OrdPCB_MG
    On Error Resume Next
    MadrdF_Corrector.TxtPCB.text = IIf(("" & r!ordPcb_MG) <> "" And ("" & r!ordPcb_MG) <> "0", "" & r!ordPcb_MG, IIf(("" & r!ordPcb) = "", 0, "" & r!ordPcb))
    If err.Number > 0 Then MadrdF_Corrector.TxtPCB.text = IIf(("" & r!ordPcb) = "", 0, "" & r!ordPcb)
    On Error GoTo 0
    MadrdF_Corrector.TxtInstr.text = IIf(IsNull(r!Istruzione), "", r!Istruzione)
    
    GbPrevPCB = MadrdF_Corrector.TxtPCB.text
    GbPrevIstr = firstElement(MadrdF_Corrector.TxtInstr.text)
    r.Close
  End If
  
  ' AC carica anche Istruzioni da tabella clone
  Set r = m_fun.Open_Recordset("Select * From PSDli_Istruzioni_Clone Where " & _
                               "IdOggetto = " & IdOggetto & " and  IdPgm = " & idPgm & " And Riga = " & Riga & " and " & _
                               "Istruzione is not Null  ")
  While Not r.EOF
    MadrdF_Corrector.TxtInstr.text = MadrdF_Corrector.TxtInstr.text & ";" & r!Istruzione
    r.MoveNext
  Wend
  r.Close
  
  'Set r = m_fun.Open_Recordset("Select * From PSDli_Istruzioni_Clone Where IdOggetto = " & IdOggetto & " And Riga = " & Riga & " and OrdPCB is not Null ")
  
  'While Not r.EOF
  '  MadrdF_Corrector.TxtPCB.Text = MadrdF_Corrector.TxtPCB.Text & ";" & r!OrdPCB
  '  r.MoveNext
  'Wend
  'r.Close
     
  'Carica i vari livelli di SSA
  Set rGen = m_fun.Open_Recordset("SELECT Distinct IdOggetto,Riga,Livello FROM PSDli_IstrDett_Liv WHERE " & _
                                  "IdOggetto = " & IdOggetto & " and IdPgm = " & idPgm & " AND Riga = " & Riga & _
                                  " ORDER BY Livello")
  
  If rGen.RecordCount > 0 Then
    
    'Prende il numero di livelli
    TotLivelli = rGen.RecordCount
    
    While Not rGen.EOF
      ReDim Preserve Istruzione.SSA(i)
      Istruzione.SSA(i).livello = rGen!livello
      
      Set rAll = m_fun.Open_Recordset("Select * From PsDli_IstrDett_Liv Where IdOggetto = " & rGen!IdOggetto & " and IdPgm = " & idPgm & " And Riga = " & rGen!Riga & " And Livello = " & rGen!livello)
      If rAll.RecordCount Then
        If rAll.RecordCount > 1 Then
          m_fun.WriteLog "Carica_Array_Istruzione", "DR - Auto"
        End If
        
        While Not rAll.EOF
          j = 1 'Contatore Key
          k = 1 'Contatore Segmento
          w = 1 'Contatore Qualificatore
          Q = 1 'Contatore Valori
          
          If Not IsNull(rAll!strIdOggetto) Then
            Istruzione.SSA(i).IdOggetto = rAll!strIdOggetto
          End If
          
          Istruzione.SSA(i).ssaName = IIf(IsNull(rAll!NomeSSA), "", rAll!NomeSSA)
          
          If Not IsNull(rAll!Stridarea) Then
            Istruzione.SSA(i).IdArea = rAll!Stridarea
          End If
          
          '***********************
          '*** S E G M E N T O ***
          '***********************
          If Not IsNull(rAll!idSegmento) Then
            Istruzione.SSA(i).NomeSegmento = Restituisci_Dati_Segmento_Da_IdSegm(rAll!idSegmento, "NOME")
            Istruzione.SSA(i).idSegmento = rAll!idSegmento
            '*** S E G L E N ***
            Istruzione.SSA(i).LenSegmento = rAll!SegLen
          End If
          If Not IsNull(rAll!IdSegmentiD) Then
            Istruzione.SSA(i).NomeSegmentiD = Restituisci_Dati_Segmento_Da_IdSegmD(rAll!IdSegmentiD, "NOME")
            Istruzione.SSA(i).IdSegmentiD = rAll!IdSegmentiD
            '*** S E G L E N ***
            Istruzione.SSA(i).LenSegmentiD = rAll!SegmentiLenD
          End If
         
          '*************************
          '*** S T R U T T U R A ***
          '*************************
          Istruzione.SSA(i).Struttura = rAll!DataArea & ""
          '******************************
          '*** C O M M A N D   C O D E ***
          '******************************
          Istruzione.SSA(i).CommandCode = rAll!condizione & ""
          
          Istruzione.SSA(i).Qualificatore = rAll!Qualificazione
          
          Istruzione.SSA(i).BothQualAndUnqual = rAll!QualAndUnqual
          
          'stefano: visto che ce l'ho.....
          Istruzione.SSA(i).Moved = rAll!Moved
          
          '*************
          '*** K E Y ***
          '*************
          Carica_Istruzione_KEY idPgm, rAll!IdOggetto, rAll!Riga, rAll!livello, i
          '*************
          '*** ORD PCB ***
          '*************
          rAll.MoveNext
        Wend
        
        rAll.Close
      End If
      
      '**********************************
      '*****  F  I  N  E  ***************
      '**********************************
      i = i + 1
      rGen.MoveNext
    Wend
    
    rGen.Close
  End If
End Sub

Public Function Restituisci_DatiKey(idField As Long, xName As Boolean, TipoRestituito As String) As Variant
  Dim rs As Recordset
  
  If idField < 0 Then
    'SQ PEZZA:
    Set rs = m_fun.Open_Recordset("Select *,'?' as lunghezza From PSDli_XDField Where IdXDField = " & Abs(idField))
  Else
    Set rs = m_fun.Open_Recordset("Select * From PSDli_Field Where IdField = " & idField)
  End If
  
  If rs.RecordCount > 0 Then
    Select Case Trim(UCase(TipoRestituito))
      Case "NOME"
        If xName = False Then
          Restituisci_DatiKey = rs!nome
        Else
          Restituisci_DatiKey = rs!ptr_xNome
        End If
      Case "LUNGHEZZA"
        'SQ XDFIELD NON HA LUNGHEZZA!
        Restituisci_DatiKey = rs!Lunghezza
    End Select
  Else
    If TipoRestituito = "LUNGHEZZA" Then Restituisci_DatiKey = 0
    If TipoRestituito = "NOME" Then Restituisci_DatiKey = ""
  End If
  rs.Close
End Function

Public Sub Associa_Dato_In_TreeView(TR As TreeView, livello As Long, key As String, Dato As String)
  Dim i As Long
  Dim Start As Long
  Dim Fin As Long
  
  Select Case Trim(UCase(key))
    Case "DBD"
      For i = 1 To TR.Nodes.Count
        If Trim(UCase(TR.Nodes(i).key)) = Trim(UCase(key)) Then
          TR.Nodes(i).text = "DBD : " & Dato
          
          Exit For
        End If
      Next i
      
    Case "PSB"
      For i = 1 To TR.Nodes.Count
        If Trim(UCase(TR.Nodes(i).key)) = Trim(UCase(key)) Then
          TR.Nodes(i).text = "PSB : " & Dato
          
          Exit For
        End If
      Next i
      
    
    Case Else
      If TR.Nodes.Count = 0 Then Exit Sub
      
      Start = 0
      For i = 1 To TR.Nodes.Count
        If Val(Mid(TR.Nodes(i).key, 4)) = livello Then
          Start = i
          Exit For
        End If
      Next i
      
      Fin = 0
      For i = 1 To TR.Nodes.Count
        If Mid(TR.Nodes(i).key, 1, 3) = "LEV" Then
         If Val(Mid(TR.Nodes(i).key, 4)) > livello Then
           Fin = i
           Exit For
         End If
        End If
        
      Next i
      
      If Fin = 0 Then Fin = i
      
      If Fin > TR.Nodes.Count Then Fin = TR.Nodes.Count
      
      If Start = 0 Then Start = 1
      
      For i = Start To Fin
        If TR.Nodes(i).key = key Then
          TR.Nodes(i).text = Mid(TR.Nodes(i).text, 1, InStr(1, TR.Nodes(i).text, ":")) & " " & UCase(Dato)
          Exit Sub
        End If
      Next i
  End Select
End Sub
Public Sub Associa_Dato_In_Memoria(livello As Long, key As String, Dato As String)
  Dim i As Long, Idx As Long
  
  Select Case Trim(UCase(key))
    Case "PSB"
      For i = 1 To MadrdF_Corrector.lswPsb.ListItems.Count
        ReDim Preserve Istruzione.psb(i)
        Istruzione.psb(i).IdOggetto = MadrdF_Corrector.lswPsb.ListItems(i).tag
        Istruzione.psb(i).nome = MadrdF_Corrector.lswPsb.ListItems(i).text
        Istruzione.psb(i).tipo = TYPE_PSB
      Next i
      
      gIdPsb = Restituisci_IdOggetto_Da_Nome(Dato, "PSB")
    
    Case "DBD"
      For i = 1 To MadrdF_Corrector.lswDBD.ListItems.Count
        ReDim Preserve Istruzione.Database(i)
        Istruzione.Database(i).IdOggetto = MadrdF_Corrector.lswDBD.ListItems(i).tag
        Istruzione.Database(i).nome = MadrdF_Corrector.lswDBD.ListItems(i).text
        Istruzione.Database(i).tipo = TYPE_DBD
      Next i
      
      'Recupera l'id del database
      gIdDbd = Restituisci_IdOggetto_Da_Nome(Dato, "DBD")
    
    Case Else
      'SQ
      If UBound(Istruzione.SSA) Then
        For i = 1 To UBound(Istruzione.SSA)
          If Istruzione.SSA(i).livello = livello Then
            Idx = i 'SQ proprio utile!
            Exit For
          End If
        Next i
        
        If Idx = 0 Then
          Idx = livello
        End If
        
        Select Case Trim(UCase(key))
          Case "SEG"
            If InStr(Dato, ";") Then
              Istruzione.SSA(Idx).NomeSegmentiD = Dato
            Else
              Istruzione.SSA(Idx).NomeSegmento = Dato
              Istruzione.SSA(Idx).NomeSegmentiD = ""
            End If
          Case "SSA"
            Istruzione.SSA(Idx).ssaName = Dato
          Case "SGL"
            If InStr(Dato, ";") Then
              Istruzione.SSA(Idx).LenSegmentiD = Dato
            Else
              If Dato = "" Then
                Istruzione.SSA(Idx).LenSegmento = 0
              Else
                Istruzione.SSA(Idx).LenSegmento = Dato
              End If
              Istruzione.SSA(Idx).LenSegmentiD = ""
            End If
          Case "KEY"
            Istruzione.SSA(Idx).Chiavi(AUSelIndexKey).key = Dato
          Case "KYV"
            Istruzione.SSA(Idx).Chiavi(AUSelIndexKey).Valore.FieldValue = Dato
          Case "OPE"
            Istruzione.SSA(Idx).Chiavi(AUSelIndexKey).Operatore = Dato
          Case "KYL"
            Istruzione.SSA(Idx).Chiavi(AUSelIndexKey).Valore.Lunghezza = Dato
          Case "KYS"
            If Len(Dato) Then
              Istruzione.SSA(Idx).Chiavi(AUSelIndexKey).Valore.KeyStart = Dato
            Else
              Istruzione.SSA(Idx).Chiavi(AUSelIndexKey).Valore.KeyStart = 0
            End If
          Case "COD"
            Istruzione.SSA(Idx).CommandCode = Dato
          Case "VAL"
            Istruzione.SSA(Idx).Chiavi(AUSelIndexKey).Valore.Valore = Dato
          Case "STR"
            Istruzione.SSA(Idx).Struttura = Dato
          Case "OPL"
            'ReDim Preserve Istruzione.SSA(idx).OpLogico(AUSelIndexKey)
            Istruzione.SSA(Idx).OpLogico(AUSelIndexKey) = Dato
          Case "CKB"
            Istruzione.SSA(Idx).BothQualAndUnqual = Dato
          Case "CKQ"
            Istruzione.SSA(Idx).Qualificatore = Dato
        End Select
      End If
  End Select
End Sub

Public Function Restituisci_Dati_Segmento_Da_IdSegm(Id As Long, TipoRestituito As String) As Variant
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From PSDli_Segmenti Where IdSegmento = " & Id)
  If r.RecordCount Then
    Select Case UCase(Trim(TipoRestituito))
      Case "NOME"
        Restituisci_Dati_Segmento_Da_IdSegm = Trim(r!nome)
      Case "LUNGHEZZA"
        Restituisci_Dati_Segmento_Da_IdSegm = Trim(r!MaxLen)
    End Select
  End If
  r.Close
End Function
Public Function Restituisci_Dati_Segmento_Da_IdSegmD(IdD As String, TipoRestituito As String) As Variant
  Dim r As Recordset, i As Integer
  Dim idSegmenti() As String
  
  idSegmenti = Split(IdD, ";")
  
  For i = 0 To UBound(idSegmenti)
    Set r = m_fun.Open_Recordset("Select * From PSDli_Segmenti Where IdSegmento = " & CLng(idSegmenti(i)))
    If r.RecordCount Then
      Select Case UCase(Trim(TipoRestituito))
        Case "NOME"
          Restituisci_Dati_Segmento_Da_IdSegmD = Restituisci_Dati_Segmento_Da_IdSegmD & Trim(r!nome) & ";"
        Case "LUNGHEZZA"
          Restituisci_Dati_Segmento_Da_IdSegmD = Restituisci_Dati_Segmento_Da_IdSegmD & Trim(r!MaxLen) & ";"
      End Select
    End If
    r.Close
  Next i
  If Not Restituisci_Dati_Segmento_Da_IdSegmD = "" Then
    Restituisci_Dati_Segmento_Da_IdSegmD = Left(Restituisci_Dati_Segmento_Da_IdSegmD, Len(Restituisci_Dati_Segmento_Da_IdSegmD) - 1)
  End If
End Function

Public Function Restituisci_Dati_Struttura(IdObj As Long, Riga As Long, livello As Long) As Variant
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrDett_Liv WHERE " & _
                               "IdOggetto = " & IdObj & " And Riga = " & Riga & " And Livello = " & livello)
  If r.RecordCount Then
    Restituisci_Dati_Struttura = Trim(r!DataArea)
  End If
  r.Close
End Function

Public Sub Carica_Istruzione_CCODE(IdOggetto As Long, Riga As Long, livello As Long, ContSSA As Long)
  Dim rKey As Recordset
  Dim i As Long
  
  Set rKey = m_fun.Open_Recordset("Select * From PsDli_IstrDett_Liv Where " & _
                                  "IdOggetto = " & IdOggetto & " And Riga = " & Riga & " And Livello = " & livello)
  If rKey.RecordCount Then
    If rKey.RecordCount > 1 Then
      m_fun.WriteLog "Carica_Istruzione_CCODE", "DR - Auto"
    End If
    If Not IsNull(rKey!condizione) Then
      Istruzione.SSA(ContSSA).CommandCode = rKey!condizione
    End If
  End If
  rKey.Close
End Sub

Public Sub Carica_Istruzione_KEY(idPgm As Long, IdOggetto As Long, Riga As Long, livello As Long, ContSSA As Long)
  Dim rKey As Recordset
  
  Set rKey = m_fun.Open_Recordset("Select * From PsDli_IstrDett_Key Where " & _
                                  "IdOggetto = " & IdOggetto & " and IdPgm = " & idPgm & " And Riga = " & Riga & " And " & _
                                  "Livello = " & livello & " Order By Progressivo")
  
  ReDim Preserve Istruzione.SSA(ContSSA).Chiavi(0)
  ReDim Preserve Istruzione.SSA(ContSSA).OpLogico(0)
   
  While Not rKey.EOF
    ReDim Preserve Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi) + 1)
    ReDim Preserve Istruzione.SSA(ContSSA).OpLogico(UBound(Istruzione.SSA(ContSSA).OpLogico) + 1)
    
    'SQ 10-10-07
    If Not IsNull(rKey!idField) Then
      Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).key = Trim(Restituisci_DatiKey(rKey!idField, rKey!xName, "NOME"))
      'Lunghezza:
      If rKey!KeyLen = 0 Or Trim(rKey!KeyLen) = "" Then
        'Prende dal field dli la lunghezza
        Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.Lunghezza = Trim(Restituisci_DatiKey(rKey!idField, rKey!xName, "LUNGHEZZA"))
        
        'Aggiorna
        rKey!KeyLen = Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.Lunghezza
        rKey.Update
      Else
        Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.Lunghezza = rKey!KeyLen
      End If
    End If
    
    Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.Valore = rKey!Valore & ""
    Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Operatore = rKey!Operatore & ""
    Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.FieldValue = rKey!keyvalore & ""
    
    If Not IsNull(rKey!KeyStart) Then
      Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.KeyStart = rKey!KeyStart
    End If
    
    If Trim(rKey!booleano) <> "" Then
      'ReDim Preserve Istruzione.SSA(ContSSA).OpLogico(UBound(Istruzione.SSA(ContSSA).OpLogico) + 1)
      Istruzione.SSA(ContSSA).OpLogico(UBound(Istruzione.SSA(ContSSA).OpLogico)) = rKey!booleano
    End If
    
    rKey.MoveNext
  Wend
  rKey.Close
End Sub

Public Function firstElement(Source As String) As String
  Dim s() As String
  
  firstElement = ""
  s = Split(Source, ";")
  If UBound(s) >= 0 Then
    firstElement = s(0)
  End If
End Function

Public Sub Conferma_Convalida_Istruzione(IdOggetto As Long, Riga As Long, ValoreCheck As Boolean, CheckObsolete As Boolean, CheckToMigrate As Boolean, felement As String)
  Dim i As Long
  Dim j As Integer
  Dim r As Recordset
  Dim scelta As Long, idsegm As Long, idField As Long, idDBD As Long
  Dim SceltaSegm As Long
  Dim SceltaField As Long
  Dim sDbd() As Long
  Dim sPsb() As Long
  Dim rs As Recordset
  Dim BolSegValid As Boolean
  Dim BolkeyValid As Boolean
  Dim BolValidBefore
  Dim s() As String
  Dim primaIstr As String
  Dim primoPCB As String, idPgm As Long, defistr As String
  'AC 17/10/2007 -- gestione salvataggio segmenti multipli
  Dim IdSegmentiD() As String, k As Integer, idSegmenti As String
  
  SceltaSegm = vbNo
  SceltaField = vbNo
  ' Mauro 07/05/2008 : IdOggetto non era quello giusto in caso di Istruzione in Copy
  'idPgm = Replace(MadrdF_Corrector.lswRighe.SelectedItem.ToolTipText, "IdPgm: ", "") 'provvisorio!
  idPgm = MadrdF_Corrector.lswRighe.SelectedItem
  IdOggetto = MadrdF_Corrector.lswRighe.SelectedItem.ListSubItems(L_IDOGG)
  primaIstr = firstElement(Trim(MadrdF_Corrector.TxtInstr.text))
  'primoPCB = FirstElement(Trim(MadrdF_Corrector.TxtPCB.Text))
  
  'Salva i dati nella tabella Istruzioni (Livello 1)
  Set r = m_fun.Open_Recordset("Select * From PSDli_Istruzioni Where " & _
                               "IdPgm = " & idPgm & " And " & _
                               "IdOggetto = " & IdOggetto & " And Riga = " & Riga)
  If r.RecordCount Then
    BolValidBefore = r!valida
    r!valida = ValoreCheck
    
    r!obsolete = CheckObsolete
    r!to_migrate = CheckToMigrate
    r!Correzione = True
    ' Mauro 16/10/2007
    'r!ordPcb = IIf(Trim(MadrdF_Corrector.TxtPCB.text) = "", Null, Trim(MadrdF_Corrector.TxtPCB.text))
    On Error Resume Next
    If Not Trim(MadrdF_Corrector.TxtPCB.text) = "0" Then
      r!ordPcb_MG = IIf(Trim(MadrdF_Corrector.TxtPCB.text) = "", Null, Trim(MadrdF_Corrector.TxtPCB.text))
    End If
    If err.Number > 0 Then r!ordPcb = IIf(Trim(MadrdF_Corrector.TxtPCB.text) = "", Null, Trim(MadrdF_Corrector.TxtPCB.text))
    On Error GoTo 0
    r!Istruzione = IIf(primaIstr = "", Null, primaIstr)
    r.Update
  End If
  r.Close
  
  m_fun.FnConnection.Execute "Delete * From PSDli_Istruzioni_Clone Where " & _
                             "IdPgm = " & idPgm & " And " & _
                             "IdOggetto = " & IdOggetto & " And Riga = " & Riga
  ' Lettura e scrittura cloni
  j = 1
  
'  Set r = m_fun.Open_Recordset("Select Istruzione From PSDli_Istruzioni Where IdOggetto = " & IdOggetto & " And Riga = " & Riga)
'  If Not r.EOF Then
'    defistr = r!istruzione
'  End If
'  r.Close
  
  Set r = m_fun.Open_Recordset("Select * From PSDli_Istruzioni_Clone Where " & _
                               "IdPgm = " & idPgm & " And " & _
                               "IdOggetto = " & IdOggetto & " And Riga = " & Riga)
 
  ' istruzioni
  s = Split(Trim(MadrdF_Corrector.TxtInstr.text), ";")
'  For i = 0 To UBound(s)
'    If s(i) = defistr Then
'      Exit For
'    End If
'  Next
'  If i > 0 And i < (UBound(s) + 1) Then ' trovato in posizione diverso da zero
'  ' scambio la pos 0 con quella dell'istruzione principale
'    s(i) = s(0)
'    s(0) = defistr
'  ElseIf i = (UBound(s) + 1) Then ' � stata eliminata quella principale
'  ' l'istruzione principale diventa quella in pos 0
'  ' che copio sulla tabella istruzioni
'     m_fun.FnConnection.Execute "Update PSDli_Istruzioni set Istruzione = '" & s(0) & "' Where IdOggetto = " & IdOggetto & " And Riga = " & Riga
'  End If
  felement = s(0)
  For i = 1 To UBound(s)
    r.AddNew
    r!IdOggetto = IdOggetto
    r!idPgm = idPgm
    r!Riga = Riga
    r!IdClone = j
    r!Istruzione = s(i)
    j = j + 1
  Next
  ' num PCB
  's = Split(Trim(MadrdF_Corrector.TxtPCB.Text), ";")
  'For i = 1 To UBound(s)
    'r.AddNew
    'r!IdOggetto = IdOggetto
    'r!Riga = Riga
    'r!IdClone = j
    'r!OrdPcb = s(i)
    'j = j + 1
  'Next
  If j > 1 Then
    r.Update
  End If
  r.Close
  
  'Scrive i dbd e i psb
  'm_fun.FnConnection.Execute "Delete * From PSDli_IstrDBD Where IdOggetto = " & idOggetto & " And Riga = " & Riga
  m_fun.FnConnection.Execute "Delete * From PSDli_IstrDBD Where " & _
                             "Idpgm = " & idPgm & " And idoggetto = " & IdOggetto & " and Riga = " & Riga
  
  For i = 1 To UBound(Istruzione.Database)
    idDBD = Istruzione.Database(1).IdOggetto
    'SG : prende il dbd primo che � quello giusto
    
    Set r = m_fun.Open_Recordset("Select * From PSDli_IstrDBD Where " & _
                                 "Idpgm = " & idPgm & " And " & _
                                 "idoggetto = " & IdOggetto & " And " & _
                                 "Riga = " & Riga & " And " & _
                                 "IdDbd = " & Istruzione.Database(i).IdOggetto)
    If r.RecordCount = 0 Then
      r.AddNew
      '**********
    
      '*********
      r!idPgm = idPgm
      r!IdOggetto = IdOggetto
      r!Riga = Riga
      r!idDBD = Istruzione.Database(i).IdOggetto
      
      r.Update
    End If
    r.Close
    
    'Scrive in relazioni Obj
    Set rs = m_fun.Open_Recordset("Select * From PsRel_Obj Where IdOggettoC = " & IdOggetto & " And IdOggettoR = " & Istruzione.Database(i).IdOggetto)
    If rs.RecordCount = 0 Then
      rs.AddNew
      
      rs!idOggettoC = IdOggetto
      rs!idOggettoR = Istruzione.Database(i).IdOggetto
      rs!Relazione = "DBD"
      rs!Utilizzo = "IMS-DBD"
      rs!NomeComponente = m_fun.Restituisci_NomeOgg_Da_IdOggetto(Istruzione.Database(i).IdOggetto)
      
      rs.Update
    End If
    rs.Close
  Next i
  
  'm_fun.FnConnection.Execute "Delete * From PSDli_IstrPSB Where " & _
                                      "IdOggetto = " & IdOggetto & IdOggetto & " And Riga = " & Riga
  m_fun.FnConnection.Execute "Delete * From PSDli_IstrPSB Where " & _
                             "IdOggetto = " & IdOggetto & " and idpgm = " & idPgm & " And Riga = " & Riga
  
  For i = 1 To UBound(Istruzione.psb)
    IdPSB = Istruzione.psb(1).IdOggetto
    'SG : prende il psb primo che � quello giusto
    
    Set r = m_fun.Open_Recordset("Select * From PSDli_Istrpsb Where " & _
                                 "idpgm = " & idPgm & " and " & _
                                 "IdOggetto = " & IdOggetto & " And Riga = " & Riga & " And " & _
                                 "Idpsb = " & Istruzione.psb(i).IdOggetto)
  
    If r.RecordCount = 0 Then
      r.AddNew
      
      r!idPgm = idPgm
      r!IdOggetto = IdOggetto
      r!Riga = Riga
      r!IdPSB = Istruzione.psb(i).IdOggetto
      
      r.Update
    End If
   
    'SG :Scrive in relazioni Obj
    Set rs = m_fun.Open_Recordset("Select * From PsRel_Obj Where IdOggettoC = " & IdOggetto & " And IdOggettoR = " & Istruzione.psb(i).IdOggetto)
      
    If rs.RecordCount = 0 Then
      rs.AddNew
      
      rs!idOggettoC = IdOggetto
      rs!idOggettoR = Istruzione.psb(i).IdOggetto
      rs!Relazione = "PSB"
      rs!Utilizzo = "PSB"
      rs!NomeComponente = m_fun.Restituisci_NomeOgg_Da_IdOggetto(Istruzione.psb(i).IdOggetto)
      
      rs.Update
    End If
    rs.Close
  Next i
  
  ReDim IdSegmentiD(0)
  For i = 1 To UBound(Istruzione.SSA)
    Set r = m_fun.Open_Recordset("Select * From PSDli_IstrDett_Liv Where " & _
                                 "idpgm = " & idPgm & " And " & _
                                 "IdOggetto = " & IdOggetto & " And Riga = " & Riga & " And " & _
                                 "Livello = " & Istruzione.SSA(i).livello)
  
    If r.RecordCount Then
      If Len(Istruzione.SSA(i).NomeSegmentiD) Then
        IdSegmentiD = Split(Istruzione.SSA(i).NomeSegmentiD, ";")
            
        'AC 18/10/2007
        'idSegm = Restituisci_IdSegm_DaNome(idOggetto, Istruzione.SSA(i).NomeSegmento, IdDBD)
        For k = 0 To UBound(IdSegmentiD)
          IdSegmentiD(k) = CStr(Restituisci_IdSegm_DaNome(IdOggetto, IdSegmentiD(k), idDBD))
          If (GbPrevPCB = Trim(MadrdF_Corrector.TxtPCB.text)) Then ' non sto riparser perch� cambiato num PCB
            If Istruzione.Tipologia <> "REPL" And Istruzione.Tipologia <> "DLET" Then
               'If idSegm = 0 And SceltaSegm = vbNo Then
               If IdSegmentiD(k) = 0 And SceltaSegm = vbNo Then
                 scelta = MsgBox("Segment not Found..." & vbCrLf & "Continue saving instruction ? ", vbYesNo, Dli2Rdbms.drNomeProdotto)
                   
                 If scelta = vbNo Then
                   Exit Sub
                 Else
                   SceltaSegm = vbYes
                 End If
               End If
            End If
          End If
        Next k
        'r!idSegmento = idSegm
        r!idSegmento = IdSegmentiD(0)
        idSegmenti = IdSegmentiD(0)
        For k = 1 To UBound(IdSegmentiD)
         If Not IdSegmentiD(k) = "0" Then
          idSegmenti = idSegmenti & ";" & IdSegmentiD(k)
         End If
        Next k
        'r!IdSegmentiD = idSegm
        r!IdSegmentiD = idSegmenti
        ' Mauro 05/12/2007 : Se � multiplo, gli faccio prendere solo il primo. TEMPORANEO
        If InStr(Istruzione.SSA(i).LenSegmentiD, ";") Then
          r!SegLen = Mid(Istruzione.SSA(i).LenSegmentiD, 1, InStr(Istruzione.SSA(i).LenSegmentiD, ";") - 1)
        Else
          r!SegLen = Istruzione.SSA(i).LenSegmento
        End If
        r!SegmentiLenD = Istruzione.SSA(i).LenSegmentiD
      Else
        ' Ho solo un valore
        IdSegmentiD(0) = CStr(Restituisci_IdSegm_DaNome(IdOggetto, Istruzione.SSA(i).NomeSegmento, idDBD))
        r!idSegmento = IdSegmentiD(0)
        r!SegLen = Istruzione.SSA(i).LenSegmento
        r!IdSegmentiD = IdSegmentiD(0)
        r!SegmentiLenD = Istruzione.SSA(i).LenSegmento
      End If
      r!condizione = Istruzione.SSA(i).CommandCode & ""
      r!Qualificazione = Istruzione.SSA(i).Qualificatore
      r!QualAndUnqual = Istruzione.SSA(i).BothQualAndUnqual
      'r!qualificazione = MadrdF_Corrector.ChkQual.Value
      'r!QualAndUnqual = MadrdF_Corrector.ChkBoth.Value
      r!DataArea = Istruzione.SSA(i).Struttura
      r!NomeSSA = Istruzione.SSA(i).ssaName
      r!valida = ValoreCheck
      BolSegValid = r!valida
      r!Correzione = True
      
      r.Update
      r.Close
       
      Set r = m_fun.Open_Recordset("Select * From PSDli_IstrDett_Key Where " & _
                                  "idpgm = " & idPgm & " And " & _
                                  "IdOggetto = " & IdOggetto & " And Riga = " & Riga & " And " & _
                                  "Livello = " & Istruzione.SSA(i).livello)
      
      If r.EOF And Not Trim(MadrdF_Corrector.txtKeyName) = "" Then
        MsgBox "Please, use 'Add Key' button to add a key level before insert the key name", vbInformation
      End If
      While Not r.EOF
        r!Valore = Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Valore.Valore
        ' Mauro 11/02/2010: Gestione Molteplicit� sugli operatori
        Dim arrOp() As String, MoltOp As String, a As Integer
        MoltOp = ""
        If InStr(Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Operatore, ";") Then
          arrOp = Split(Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Operatore, ";")
          For a = 0 To UBound(arrOp)
            MoltOp = MoltOp & IIf(Len(MoltOp) > 0 And Len(getOperator(arrOp(a))) > 0, ";", "") & getOperator(arrOp(a))
          Next a
          r!Operatore = MoltOp
        Else
          r!Operatore = Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Operatore
        End If
        r!KeyStart = Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Valore.KeyStart
        r!keyvalore = Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Valore.FieldValue
        'ReDim Preserve Istruzione.SSA(i).OpLogico(r.AbsolutePosition)
        r!booleano = Istruzione.SSA(i).OpLogico(r.AbsolutePosition)
        If Not IdSegmentiD(0) = "" Then
          idField = Restituisci_IdField_DaNome(IdSegmentiD(0), Istruzione.SSA(i).Chiavi(r.AbsolutePosition).key)
          If idField = 0 Then
            idField = FindIdFieldFromSource(IdSegmentiD(0), Istruzione.SSA(i).Chiavi(r.AbsolutePosition).key)
          End If
        End If
        If (GbPrevPCB = Trim(MadrdF_Corrector.TxtPCB.text)) Then ' non sto riparser perch� cambiato num PCB
          If Istruzione.Tipologia <> "REPL" And Istruzione.Tipologia <> "DLET" Then
            If idField = 0 And SceltaField = vbNo Then
              scelta = MsgBox("Field (Key) not Found..." & vbCrLf & "Continue saving instruction ? ", vbYesNo, m_fun.FnNomeProdotto)
              If scelta = vbNo Then
                Exit Sub
              Else
                SceltaField = vbYes
              End If
            End If
            If Not idField = 0 And Not Istruzione.SSA(i).Qualificatore Then
                MsgBox "Field (Key) Found, but level " & i & " not flagged as qualified.", vbInformation, m_fun.FnNomeProdotto
            End If
          End If
        End If
        r!idField = idField
        r!KeyLen = Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Valore.Lunghezza
       
        r!valida = ValoreCheck
        BolkeyValid = r!valida
        r!Correzione = True
       
        r.Update
        r.MoveNext
      Wend
      r.Close
    End If
  Next i
  
  ' AC
  ' If BolkeyValid And BolSegValid Then
  '  Set r = m_fun.Open_Recordset("Select * From PsDli_Istruzioni Where IdOggetto = " & IdOggetto & " And Riga = " & Riga)
  '  If Not r.EOF Then
  '      If Not r!Valid Then
  '          r!Valid = True
            'Refresh
  '          Carica_ListView_Istruzioni MadrdF_Corrector.lswRighe, IdOggetto, True, MadrdF_Corrector.RTIncaps
 '           Carica_Array_Istruzione Riga, IdOggetto
   '     End If
  '  End If
  '  r.Close
'  End If
'AC
  'If Not (ValoreCheck = BolValidBefore) Then
    'Refresh
    If ValoreCheck Then
      If MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO Or _
         MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO Then
        MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO
      Else
        MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO_MOVED
      End If
    Else
      If MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO Or _
         MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO Then
        MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO
      Else
        MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO_MOVED
      End If
    End If
    
    'Carica_Array_Istruzione Riga, IdOggetto
  'End If

  '*B********* tolto 30/10/07
'  Set r = m_fun.Open_Recordset("Select * From PsDli_Istruzioni Where " & _
'                                        "IdOggetto = " & idOggetto & " And idpgm <> 0 And Valida = False")
'
'  If r.RecordCount > 0 Then
'      ValoreCheck = False
'  ElseIf r.RecordCount = 0 Then
'      ValoreCheck = True
'  End If
  
  '*E*************************************
  'AC 30/10/07
  'Set r = m_fun.Open_Recordset("Select valida From PsDli_Istruzioni Where " & _
  '                                      "IdOggetto = " & idOggetto & " And idpgm <> 0 And riga = " & Riga)
  ' Mauro 09/05/2008 :  Ho tutti i valori, perch� devo fare una ricerca cos� generica?!?!?
  Set r = m_fun.Open_Recordset("Select valida From PsDli_Istruzioni Where " & _
                               "IdOggetto = " & IdOggetto & " And idpgm = " & idPgm & " And riga = " & Riga)
  If Not r.EOF Then
    ValoreCheck = r!valida
  End If
  r.Close
  
  '*************************************************************************
  '******** Cancella le segnalazioni di istruzione non decodificata ********
  '*************************************************************************
  Dim colActive As New Collection
  Dim strErrLevel As String
  
  colActive.Add IdOggetto
 
  DLLExtParser.AttivaFunzione "AGGERRLEVEL", colActive
  
  'AC non tocco segnalazioni
  If ValoreCheck Then
'    Set r = m_fun.Open_Recordset("Select * From BS_Segnalazioni Where (IdOggetto = " & idOggetto & " or IdOggettoRel = " & idOggetto & ") And Codice = 'F08'")
'    While Not r.EOF
'      r.Delete
'      r.MoveNext
'    Wend
'    r.Close
'
'    'Elimina da Bs_Oggetti errLevel
'    Set r = m_fun.Open_Recordset("Select * From BS_Segnalazioni Where (IdOggetto = " & idOggetto & " or IdOggettoRel = " & idOggetto & ") And Codice = 'F08'")
'
'    If r.RecordCount = 0 Then
'
      Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & IdOggetto)

      If r.RecordCount Then
        r!ErrLevel = colActive.item(2)

        'Aggiorna il video
        If Not m_fun.FnObjList.SelectedItem Is Nothing Then
          m_fun.FnObjList.SelectedItem.SmallIcon = 6
          m_fun.FnObjList.SelectedItem.ListSubItems(3) = ""
        End If
        r.Update
      End If
'    End If
      r.Close
        
    m_fun.FnConnection.Execute "DELETE * from Bs_Segnalazioni where idoggetto = " & IdOggetto & " and riga = " _
                              & Riga & " AND (codice like 'I%%' or codice in ('F04','F06','F07','F08','F09','F10','F29','F30','H30','P12'))"
    
    If MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO Or _
       MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO Then
      MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO
    Else
      MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO_MOVED
    End If
   
  Else
    If MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO Or _
       MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO Then
      MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO
    Else
      MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO_MOVED
    End If
   
    'Scrive la segnalazione di istruzione non decodificata
    Set r = m_fun.Open_Recordset("Select * From BS_Segnalazioni Where " & _
                                 "IdOggetto = " & IdOggetto & " And Codice = 'I01' and Riga = " & Riga)
    If r.RecordCount = 0 Then
      r.AddNew
    End If
    
    r!IdOggetto = IdOggetto
    r!Riga = Riga
    r!Codice = "I01"
    r!Origine = "AT"
    r!Data_Segnalazione = Now
    r!Tipologia = "AUTO"
    r!Gravita = "E"
      
    r.Update
    r.Close
    
    'La scrive anche su BS_Oggetti : errLevel
    Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & IdOggetto)
      
    If r.RecordCount Then
      'If Trim(r!ErrLevel) = "" Then
      r!ErrLevel = colActive.item(2)
      If Not m_fun.FnObjList.SelectedItem Is Nothing Then
        m_fun.FnObjList.SelectedItem.SmallIcon = 5
        m_fun.FnObjList.SelectedItem.ListSubItems(3) = colActive.item(2)
      End If
      r.Update
      'End If
    End If
    r.Close
  End If
End Sub

Public Function Restituisci_IdOggetto_Da_Nome(nome As String, wTipo As String) As Long
  Dim r As Recordset
  
  If Trim(wTipo) = "" Then
    Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & nome & "'")
  Else
    Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & nome & "' And Tipo = '" & wTipo & "'")
  End If
  If r.RecordCount Then
    'E' un record solo. SQ: ma cosa stai dicendo?!
    Restituisci_IdOggetto_Da_Nome = r!IdOggetto
  End If
  r.Close
End Function

Public Sub Aggiorna_Icone_TreeView_Istruzione(ValoreChk As Long, TRW As TreeView)
  Dim i As Long
  
  If ValoreChk = 1 Or UBound(Mappa_Immagini_TreeView) = 0 Then
    For i = 1 To TRW.Nodes.Count
      Select Case Trim(UCase(TRW.Nodes(i).key))
        Case "DBD", "PSB"
          'If ValoreChk = 1 Then
            'TRW.Nodes(i).Image = 8
          'End If
        
        Case "", "ISTR"
          'Non mi importa niente !!!
          
        Case Else
          If InStr(1, Trim(UCase(TRW.Nodes(i).key)), "LEV") = 0 Then
              'stefano: provo....
              If InStr(1, TRW.Nodes(i).text, "SSA Name") > 0 Then
                If MadrdF_Corrector.lswRighe.SelectedItem.Icon = BAFFETTO Or _
                   MadrdF_Corrector.lswRighe.SelectedItem.Icon = DIVIETO Then
                  TRW.Nodes(i).Image = BAFFETTO
'                Else
'                 TRW.Nodes(i).Image = 19
                End If
              Else
                TRW.Nodes(i).Image = BAFFETTO
              End If
         End If
      End Select
      
    Next i
  Else
    For i = 1 To TRW.Nodes.Count
      TRW.Nodes(i).Image = Mappa_Immagini_TreeView(i)
    Next
  End If
  If ValoreChk = 1 Then
    If MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO Or _
       MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO Then
      MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO
    Else
      MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO_MOVED
    End If
  Else
    If MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = BAFFETTO Or _
       MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO Then
      MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO
    Else
      MadrdF_Corrector.lswRighe.SelectedItem.SmallIcon = DIVIETO_MOVED
    End If
  End If
End Sub

Public Function Restituisci_IdSegm_DaNome(IdOggetto As Long, nome As String, widDbd As Long) As Long
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("Select * From PSDli_Segmenti Where Nome = '" & nome & "' And IdOggetto = " & widDbd)
  If rs.RecordCount Then
    If rs.RecordCount = 1 Then
      Restituisci_IdSegm_DaNome = rs!idSegmento
    Else
      'Molteplicit� : Impossibile
    End If
  End If
  rs.Close
End Function

Public Function Restituisci_IdField_DaNome(idsegm As String, nome As String) As Long
  Dim rf As Recordset, rs As Recordset
  
  Set rf = m_fun.Open_Recordset("Select * From PSDli_Field Where " & _
                                "IdSegmento = " & idsegm & " And Nome = '" & nome & "'")
  If rf.RecordCount Then
    Restituisci_IdField_DaNome = rf!idField
  ElseIf rf.RecordCount = 0 Then
    Set rs = m_fun.Open_Recordset("Select * From PSDli_XDField Where " & _
                                  "IdSegmento = " & idsegm & " And Nome = '" & nome & "'")
    If rs.RecordCount Then
      Restituisci_IdField_DaNome = -1 * rs!idXDfield
    End If
    rs.Close
  End If
  rf.Close
End Function
    
'AC 05/03/10 giro sui db source alla ricerca del campo
Public Function FindIdFieldFromSource(idsegm As String, nome As String) As Long
  Dim rf As Recordset
  Dim rs As Recordset, idDBD As Long, idSeg As Long, idField As Long
    
  'primo giro: da db logico a fisico (for example)
  Set rf = m_fun.Open_Recordset("Select segmento,DBD From PSDli_Segmenti_SOURCE Where " & _
                                "IdSegmento = " & idsegm & " And ordinale = 1")
  If Not rf.EOF Then
    idDBD = Restituisci_IdOggetto_Da_Nome(rf!DBD, "")
    If Not idDBD = 0 Then
      idSeg = Restituisci_IdSegm_DaNome(0, rf!Segmento, idDBD)
      If Not idSeg = 0 Then
        idField = Restituisci_IdField_DaNome(CStr(idSeg), nome)
        If idField = 0 Then ' facciamo un altro giro: il segmento sul db fisico punta ad un altro segmento
          Set rs = m_fun.Open_Recordset("Select Segmento,DBD From PSDli_Segmenti_SOURCE Where " & _
                                        "IdSegmento = " & idSeg & " And ordinale = 1")
          If Not rs.EOF Then
            idDBD = Restituisci_IdOggetto_Da_Nome(rs!DBD, "")
            If Not idDBD = 0 Then
              idSeg = Restituisci_IdSegm_DaNome(0, rs!Segmento, idDBD)
              If Not idSeg = 0 Then
                idField = Restituisci_IdField_DaNome(CStr(idSeg), nome)
              End If
            End If
          End If
          rs.Close
        End If
      End If
    End If
  End If
  rf.Close
  FindIdFieldFromSource = idField
End Function

Sub Riparser_after_PCBChange(pNumPCB As Integer, pNamePCB As String)
  Dim colIdPSB As New Collection
  Dim i As Integer
  Dim Istruzione As String, parametri As String
  'SQ CPY-ISTR
  Dim idPgm As Long, IdOggetto As Long, Riga As Long
  Dim rs As Recordset, isExec As Boolean

  If Not MadrdF_Corrector.lswRighe.SelectedItem Is Nothing Then
    'SQ CPY-ISTR
    ' Mauro 07/05/2008
    'idPgm = Replace(MadrdF_Corrector.lswRighe.SelectedItem.ToolTipText, "IdPgm: ", "") 'provvisorio!
    'idOggetto = MadrdF_Corrector.lswRighe.SelectedItem
    'Riga = MadrdF_Corrector.lswRighe.SelectedItem.SubItems(1)
    idPgm = MadrdF_Corrector.lswRighe.SelectedItem
    IdOggetto = MadrdF_Corrector.lswRighe.SelectedItem.ListSubItems(L_IDOGG)
    Riga = MadrdF_Corrector.lswRighe.SelectedItem.SubItems(L_LINE)
    Istruzione = MadrdF_Corrector.lswRighe.SelectedItem.SubItems(L_INSTR)
    parametri = MadrdF_Corrector.lswRighe.SelectedItem.SubItems(L_STATEMENT)
  End If
  ' Sulle exec il  riparser � da implementare
  ' Mauro 07/05/2008 :  e l'IdPgm?!?!?!
  'Set rs = m_fun.Open_Recordset("Select IsExec From PsDli_Istruzioni where " & _
  '                                     "IdOggetto = " & idOggetto & " and riga = " & Riga)
  Set rs = m_fun.Open_Recordset("Select IsExec From PsDli_Istruzioni where " & _
                                         "idpgm = " & idPgm & " and IdOggetto = " & IdOggetto & " and riga = " & Riga)
  If rs!isExec Then
    isExec = True
    'rs.Close
    ' Mauro 16/10/2007
    On Error GoTo ok_tmp
    ' Mauro 07/05/2008 :  e l'IdPgm?!?!?!
    'm_fun.FnConnection.Execute ("Update PsDLi_Istruzioni set OrdPcb_MG = '" & pNumPCB & "' where " & _
    '                            "IdOggetto = " & idOggetto & " and riga = " & Riga)
    m_fun.FnConnection.Execute ("Update PsDLi_Istruzioni set OrdPcb_MG = '" & pNumPCB & "' where " & _
                                "idpgm = " & idPgm & " and IdOggetto = " & IdOggetto & " and riga = " & Riga)
  End If
  rs.Close
  If MadrdF_Corrector.lswPsb.ListItems.Count Then
    For i = 1 To MadrdF_Corrector.lswPsb.ListItems.Count
      'SQ - andava in overflow... buttata via, tanto non serviva a nessun altro
      'IdPSB = IdPSBfromName(MadrdF_Corrector.lswPsb.ListItems(i))
      'If Not IdPSB = -1 Then
      '  colIdPSB.Add (IdPSB)
      'End If
      Set rs = m_fun.Open_Recordset("Select IdOggetto From BS_Oggetti Where " & _
                                    "Nome= '" & MadrdF_Corrector.lswPsb.ListItems(i) & "' and Tipo='PSB'")
      If rs.RecordCount Then
        colIdPSB.Add CLng(rs!IdOggetto)
      Else
        '??
      End If
      rs.Close
    Next i
    'SQ portato nel Parser... se no non viene mai aggiornata...
    'DliInitParserRiga idPgm, idOggetto, riga 'Mettere nel Parser!
    
    'chiamata funz parser che valorizza array PSB per CBLII
    'chiamata funz parser che valorizza struttura con PCB
    'SQ CPY-ISTR
    'AC 02/01/08
    'introduco versione del riparser per istruzioni exec
    If isExec Then
      DLLExtParser.ParserAfterPCBNumber_Exec colIdPSB, pNumPCB, pNamePCB, Istruzione, parametri, idPgm, IdOggetto, Riga
    Else
      'DLLExtParser.ParserAfterPCBNumber colIdPSB, pNumPCB, pNamePCB, istruzione, parametri, idOggetto, riga
      DLLExtParser.ParserAfterPCBNumber colIdPSB, pNumPCB, pNamePCB, Istruzione, parametri, idPgm, IdOggetto, Riga
    End If
  Else
    'gestire...
    MsgBox "No PSB Associated", vbInformation + vbOKOnly, "PSB not found"
  End If
  Exit Sub
ok_tmp:
  m_fun.FnConnection.Execute ("Update PsDLi_Istruzioni set OrdPcb = '" & pNumPCB & "' where " & _
                                       "IdOggetto = " & IdOggetto & " and riga = " & Riga)
End Sub

Sub Carica_ListaCloni(IdOgg As Long, Riga As Long, idPgm As Long)
  Dim rs As Recordset
    
  MadrdF_Corrector.lswCloni.ListItems.Clear
  Set rs = m_fun.Open_Recordset("select b.codifica, b.decodifica from psdli_istrcodifica as a, mgdli_Decodificaistr as b where " & _
                                "a.codifica = b.codifica and a.idoggetto = " & IdOgg & " and a.idpgm = " & idPgm & " and " & _
                                "a.riga = " & Riga)
  Do Until rs.EOF
    MadrdF_Corrector.lswCloni.ListItems.Add , , rs!Codifica
    MadrdF_Corrector.lswCloni.ListItems(MadrdF_Corrector.lswCloni.ListItems.Count).SubItems(1) = rs!Decodifica
    MadrdF_Corrector.lswCloni.ListItems(MadrdF_Corrector.lswCloni.ListItems.Count).tag = idPgm & "/" & IdOgg & "/" & Riga
    
    rs.MoveNext
  Loop
  rs.Close
End Sub

Public Function Restituisci_File_Da_Nome_Tipo_Oggetto(NomeOgg As String, TipoOggetto As String) As String
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & NomeOgg & "' and tipo = '" & TipoOggetto & "'")
  If r.RecordCount Then
    Restituisci_File_Da_Nome_Tipo_Oggetto = Crea_Directory_Progetto(r!Directory_Input, m_fun.FnPathDef) & "\" & _
                                      NomeOgg & IIf(Len(r!estensione), "." & r!estensione, "")
  End If
  r.Close
End Function

Function getOperator(operator As String) As String
  Select Case operator
    Case "=", "EQ"
      getOperator = "EQ"
    Case ">=", "=>", "GE"
      getOperator = "GE"
    Case "<=", "=<", "LE"
      getOperator = "LE"
    Case "<", "LT"
      getOperator = "LT"
    Case ">", "GT"
      getOperator = "GT"
    Case "!=", "=!", "NE"
      getOperator = "NE"
    Case Else
      getOperator = ""
  End Select
End Function
