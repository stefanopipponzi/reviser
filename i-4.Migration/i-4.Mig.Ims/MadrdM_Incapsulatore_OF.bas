Attribute VB_Name = "MadrdM_Incapsulatore_OF"
Option Explicit
Global NCBLcpyOF As String
Global IsLineAnalized As Boolean
'Ac indice su cui il testo di InizializzaCopy e parte di testo ricavato da "insertPLIPtrVariables_OF"
Global InitCopyIndex As Integer
Global ArrProc() As String, ArrProcLines() As String
Global isThenOpen As Boolean
Global thenPos As Integer
Global WMId As Integer
Global IsWMIncaps As Boolean
Global isWALMARTpgm As Boolean
Global isMoltIstr As Boolean
Global isMoltPcb As Boolean

Public Sub InitializeXOPTS(directive As String)
  ReDim Preserve FItable(UBound(FItable) + 1)
  FItable(UBound(FItable)).CommentType = TipoFile
  FItable(UBound(FItable)).isCaseSensitive = True 'verificare
  FItable(UBound(FItable)).grouping_policy = "ONEROW"
  FItable(UBound(FItable)).infoModify = "XOPTSCBL"
  FItable(UBound(FItable)).isUnique = True
  FItable(UBound(FItable)).target = directive
  If TipoFile = "PLI" Then
    FItable(UBound(FItable)).grouping_policy = "TERMINATOR"
    FItable(UBound(FItable)).infoModify = "XOPTSPLI"
    FItable(UBound(FItable)).terminator = ";"
  End If
End Sub

Public Sub InitializeIncludeGRZ(directive As String)
  ReDim Preserve FItable(UBound(FItable) + 1)
  FItable(UBound(FItable)).CommentType = TipoFile
  FItable(UBound(FItable)).isCaseSensitive = False 'verificare
  FItable(UBound(FItable)).grouping_policy = "TERMINATOR"
  FItable(UBound(FItable)).infoModify = "PLI_COMMENT"
  FItable(UBound(FItable)).terminator = ";"
  FItable(UBound(FItable)).isUnique = True
  FItable(UBound(FItable)).target = Trim(directive)
End Sub

Public Sub WM_CBL_COMMON_COPYBOOK(rs As Recordset, typeIncaps As String)
  Dim Tip As String
  Dim k As Integer
  Dim w As Long
  Dim ww As Long, wstr As String, pos4 As Integer
  Dim r As Recordset
  Dim isVSAMbatch As Boolean
  Dim strcomment As String, templateName As String
  Dim rmain As Recordset
  Dim nfile As Integer
  Dim lFile As Integer, sLine As String
  
  Set r = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & rs!IdOggetto)
  If r.RecordCount Then
    If Trim(r!estensione) = "" Then
      NCBLcpyOF = r!nome
    Else
      NCBLcpyOF = r!nome & "." & r!estensione
    End If
    NomeProg = r!nome
    
    DirCBLcpy = Crea_Directory_Progetto(r!Directory_Input, Dli2Rdbms.drPathDef)
    w = InStr(1, DirCBLcpy, "\")
    Do While w
      ww = w
      w = InStr(w + 1, DirCBLcpy, "\")
    Loop
    
    'qui ottengo la directory di origine del file
    dirOr = Mid(DirCBLcpy, ww + 1)
    
    Tip = r!tipo
    
    '**************  nuovo percorso destinazione  Incapsulator/Nome_directory_origine_programma
    MadrdM_Incapsulatore.Percorso = m_fun.FnPathDB & "\output-prj\incapsulator"
    If Dir(MadrdM_Incapsulatore.Percorso, vbDirectory) = "" Then MkDir MadrdM_Incapsulatore.Percorso
    MadrdM_Incapsulatore.Percorso = MadrdM_Incapsulatore.Percorso & "\" & dirOr
    If Dir(MadrdM_Incapsulatore.Percorso, vbDirectory) = "" Then MkDir MadrdM_Incapsulatore.Percorso
    
    On Error GoTo err
    '
    ReDim Preserve FItable(UBound(FItable) + 1)
    FItable(UBound(FItable)).CommentType = Tip
    FItable(UBound(FItable)).isCaseSensitive = True 'verificare
    FItable(UBound(FItable)).grouping_policy = "ONEROW"
    FItable(UBound(FItable)).infoModify = ""
    FItable(UBound(FItable)).isUnique = True
    FItable(UBound(FItable)).target = "IDENTIFICATION DIVISION"
    FItable(UBound(FItable)).terminator = ""
    ' Inizio
    templateName = m_fun.FnPathPrj & "\input-prj\TEMPLATE\imsdb\esternalizzazione\INCAPS" & dirincaps & "\WM_COMMONBEGIN"
    If Len(Dir(templateName, vbNormal)) Then
      nfile = FreeFile
      lFile = FileLen(templateName)
      
      Open templateName For Binary As nfile
      While Loc(nfile) < lFile
        Line Input #nfile, sLine
        FItable(UBound(FItable)).lines_up = FItable(UBound(FItable)).lines_up & sLine & vbCrLf
      Wend
      Close (nfile)
      'CHANGE TAG
      FItable(UBound(FItable)).lines_up = Replace(FItable(UBound(FItable)).lines_up, "<version>", App.Major & "." & App.Minor & "." & Format(App.Revision, "00"))
      FItable(UBound(FItable)).lines_up = Left(FItable(UBound(FItable)).lines_up, Len(FItable(UBound(FItable)).lines_up) - 2)
    End If
     
    ReDim Preserve FItable(UBound(FItable) + 1)
    FItable(UBound(FItable)).CommentType = Tip
    FItable(UBound(FItable)).isCaseSensitive = True 'verificare
    FItable(UBound(FItable)).grouping_policy = "ONEROW"
    FItable(UBound(FItable)).infoModify = ""
    FItable(UBound(FItable)).isUnique = True
    FItable(UBound(FItable)).target = "WORKING-STORAGE"
    FItable(UBound(FItable)).terminator = ""
    
    ' WORKING STORAGE
    WMId = WMId + 1
    templateName = m_fun.FnPathPrj & "\input-prj\TEMPLATE\imsdb\esternalizzazione\INCAPS" & dirincaps & "\WM_COPYBOOK"
    If Len(Dir(templateName, vbNormal)) Then
      nfile = FreeFile
      lFile = FileLen(templateName)
      
      Open templateName For Binary As nfile
      While Loc(nfile) < lFile
        Line Input #nfile, sLine
        FItable(UBound(FItable)).lines_down = FItable(UBound(FItable)).lines_down & sLine & vbCrLf
      Wend
      Close (nfile)
      'CHANGE TAG
      FItable(UBound(FItable)).lines_down = Replace(FItable(UBound(FItable)).lines_down, "<id>", WMId)
      FItable(UBound(FItable)).lines_down = Replace(FItable(UBound(FItable)).lines_down, "<copybook>", "OMLECC01")
      FItable(UBound(FItable)).lines_down = Left(FItable(UBound(FItable)).lines_down, Len(FItable(UBound(FItable)).lines_down) - 2)
    End If
  End If
  OldName = MadrdM_Incapsulatore.Percorso & "\" & NCBLcpyOF
  Exit Sub
err:
  MsgBox "Incapsulated IMS-DC version of file not found", vbCritical, "File not found"
  Resume Next
End Sub

Private Sub buildStringForFItable(targetWord As String, rTextBox As RichTextBox)
  Dim begin As Long, fineriga As Long, Line As String
  Dim Start As Long, beginline As String
  Dim IsFoundEntry As Boolean
  Dim target As String, token1 As String, token2 As String, token3 As String
    
  begin = 1
  Do While rTextBox.find(targetWord, begin, Len(rTextBox.text)) > 0
    begin = rTextBox.find(targetWord, begin, Len(rTextBox.text))
    fineriga = rTextBox.find(vbCrLf, begin + 6, Len(rTextBox.text))
    If fineriga > 0 Then
      rTextBox.SelStart = begin
      rTextBox.SelLength = fineriga - begin + 1
'      If Len(rTextBox.SelText) > 72 Then
'        Line = Left(rTextBox.SelText, 72)
'      Else
      Line = rTextBox.SelText
'      End If
      If InStr(UCase(Line), " ENTRY ") > 0 Or InStr(UCase(Line), " ENTRY;") > 0 Then
        'ricerca inizio linea per capire se DCL o DECLARE
        Start = begin - 4
        While Not rTextBox.find(vbCrLf, Start, Start + 2) > 0 And Start > 0
          Start = Start - 1
        Wend
        rTextBox.SelStart = Start + 3
        rTextBox.SelLength = 12
        beginline = rTextBox.SelText
        '___________
        If Not (InStr(UCase(Line), "*/") > 0 And Not InStr(UCase(Line), "/*") > 0) Then
          token1 = Replace(nextToken(Line), ")", "")
          token2 = nextToken(Line)
          If Len(Line) Then
            token3 = Replace(nextToken(Line), ";", "")
            IsFoundEntry = True
            Exit Do
          End If
        End If
      End If
    End If
    begin = begin + 6
  Loop
  If IsNumeric(token3) Then
    token3 = ""
  End If
  If IsFoundEntry = True Then
    If InStr(UCase(beginline), "DECLARE") > 0 Then
      InitializeIncludeGRZ "DECLARE " & token1 & " " & token2 & " " & RTrim(token3)
    Else
      InitializeIncludeGRZ "DCL " & token1 & " " & token2 & " " & RTrim(token3)
    End If
  End If
End Sub

Public Sub Controlla_CBL_CPY_OF(RT As RichTextBox, RTBErr As RichTextBox, rs As Recordset, typeIncaps As String)
  '2) Controlla se l'istruzione Corrente fa parte Del CBL o la CPY gi� aperta, se si non carica il CBL o CPY ma li aggiorna:
  'Dim NCBLcpy As String
  Dim Tip As String
  Dim k As Integer
  Dim w As Long
  Dim ww As Long, wstr As String, pos4 As Integer
  Dim r As Recordset
  Dim isVSAMbatch As Boolean
  Dim strcomment As String
  Dim rmain As Recordset
  AddRighe = 0
  
  Set r = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & rs!IdOggetto)
  If r.RecordCount Then
    If Trim(r!estensione) = "" Then
      NCBLcpyOF = r!nome
    Else
      NCBLcpyOF = r!nome & "." & r!estensione
    End If
    NomeProg = r!nome
    
    DirCBLcpy = Crea_Directory_Progetto(r!Directory_Input, Dli2Rdbms.drPathDef)
    
    w = InStr(1, DirCBLcpy, "\")
    While w > 0
      ww = w
      w = InStr(w + 1, DirCBLcpy, "\")
    Wend
    
    'qui ottengo la directory di origine del file
    dirOr = Mid(DirCBLcpy, ww + 1)
    
    Tip = r!tipo
    RT.text = ""
    
    '**************  nuovo percorso destinazione  Incapsulator/Nome_directory_origine_programma
    MadrdM_Incapsulatore.Percorso = m_fun.FnPathDB & "\output-prj\incapsulator"
    If Dir(MadrdM_Incapsulatore.Percorso, vbDirectory) = "" Then MkDir MadrdM_Incapsulatore.Percorso
    MadrdM_Incapsulatore.Percorso = MadrdM_Incapsulatore.Percorso & "\" & dirOr
    If Dir(MadrdM_Incapsulatore.Percorso, vbDirectory) = "" Then MkDir MadrdM_Incapsulatore.Percorso
    '********************************
    On Error GoTo err
    
    isVSAMbatch = r!Batch And dirincaps = "\VSAM"
    
    If RT.text = "" Then
      If MadrdF_Incapsulatore.Optsource Then
        RT.LoadFile DirCBLcpy & "\" & NCBLcpyOF
      Else
        RT.LoadFile MadrdM_Incapsulatore.Percorso & "\" & NCBLcpyOF
      End If
      
      'GESTIONE XOPTS : elimina dal file le direttive di compilazione DLI dalla XOPTS
      'IRIS-DB start: non sono sicuro onestamente perch� cercava nei prime 3600 bytes, comunque l'ho cambiato in qualcosa di pi� sensato
''''      If RT.find("XOPTS", 1, 3600) > 0 Then
''''        InitializeXOPTS "XOPTS"
''''      End If
''''      If RT.find("FDUMP", 1, 3600) > 0 Then
''''        InitializeXOPTS "FDUMP"
''''      End If
''''      If RT.find("SYM", 1, 3600) > 0 Then
''''        InitializeXOPTS "SYM"
''''      End If
''''      If RT.find("OFFSET", 1, 3600) > 0 Then
''''        InitializeXOPTS "OFFSET"
''''      End If
''''      If RT.find("LANGUAGE(UE)", 1, 3600) > 0 Then
''''        InitializeXOPTS "LANGUAGE(UE)"
''''      End If
      
      If RT.find("XOPTS", 1, 164) > 0 Then
        InitializeXOPTS "XOPTS"
        If RT.find("FDUMP", 1, 164) > 0 Then
          InitializeXOPTS "FDUMP"
        End If
        If RT.find("SYM", 1, 164) > 0 Then
          InitializeXOPTS "SYM"
        End If
        If RT.find("OFFSET", 1, 164) > 0 Then
          InitializeXOPTS "OFFSET"
        End If
        If RT.find("LANGUAGE(UE)", 1, 164) > 0 Then
          InitializeXOPTS "LANGUAGE(UE)"
        End If
      End If
      
      ' non solo per GRZ
      buildStringForFItable "PLITDLI", RT
      buildStringForFItable "AIBTDLI", RT
      
      ' Mauro 25/01/2010 : Commentare Include\Ext. Entry per GRZ
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
      
        If RT.find("FLGIMSI#", 1, Len(RT.text)) > 0 Then
          InitializeIncludeGRZ "%INCLUDE FLGIMSI#"
        End If
        
        'If RT.find("PLITDLI", 1, Len(RT.text)) > 0 Then
          'InitializeIncludeGRZ "DCL PLITDLI EXT ENTRY"
        'End If
        'If RT.find("PLIHSSR", 1, Len(RT.text)) > 0 Then
'          InitializeIncludeGRZ "DCL PLIHSSR EXT ENTRY"
'        End If
        'If RT.find("AIBTDLI", 1, Len(RT.text)) > 0 Then
'          InitializeIncludeGRZ "DCL AIBTDLI EXT ENTRY"
'        End If
'        If RT.find("SX000M0", 1, Len(RT.text)) > 0 Then
'          InitializeIncludeGRZ "DCL SX000M0 EXT ENTRY"
'        End If
         
         buildStringForFItable "PLIHSSR", RT
         
         buildStringForFItable "SX000M0", RT

        '_____________________  vecchia versione per entry ___________________________
'        Dim begin As Long, fineriga As Long, line As String
'        Dim IsFoundEntry As Boolean
'        Dim target As String, token1 As String, token2 As String, token3 As String
'        begin = 1
'
'        Do While RT.find("PLITDLI", begin, Len(RT.text)) > 0
'            begin = RT.find("PLITDLI", begin, Len(RT.text))
'            fineriga = RT.find(vbCrLf, begin + 6, Len(RT.text))
'            If fineriga > 0 Then
'                RT.SelStart = begin
'                RT.SelLength = fineriga - begin + 1
'                line = RT.SelText
'                If InStr(UCase(line), " ENTRY ") > 0 Or InStr(UCase(line), " ENTRY;") > 0 Then
'                    If Not (InStr(UCase(line), "*/") > 0 And Not InStr(UCase(line), "/*") > 0) Then
'                        token1 = Replace(nextToken(line), ")", "")
'                        token2 = nextToken(line)
'                        If Len(line) Then
'                            token3 = Replace(nextToken(line), ";", "")
'                            IsFoundEntry = True
'                            Exit Do
'                        End If
'                    End If
'                End If
'            End If
'            begin = begin + 6
'        Loop
'        If IsFoundEntry = True Then
'            If InStr(UCase(Left(line, 10)), "DECLARE") > 0 Then
'                InitializeIncludeGRZ "DECLARE " & token1 & " " & token2 & " " & token3
'            Else
'                InitializeIncludeGRZ "DCL " & token1 & " " & token2 & " " & token3
'            End If
'        End If
'        IsFoundEntry = False
'        begin = 1
'
'
'        Do While RT.find("PLIHSSR", begin, Len(RT.text)) > 0
'            begin = RT.find("PLIHSSR", begin, Len(RT.text))
'            fineriga = RT.find(vbCrLf, begin + 6, Len(RT.text))
'            If fineriga > 0 Then
'                RT.SelStart = begin
'                RT.SelLength = fineriga - begin + 1
'                line = RT.SelText
'                If InStr(UCase(line), " ENTRY ") > 0 Or InStr(UCase(line), " ENTRY;") > 0 Then
'                    If Not (InStr(UCase(line), "*/") > 0 And Not InStr(UCase(line), "/*") > 0) Then
'                        token1 = Replace(nextToken(line), ")", "")
'                        token2 = nextToken(line)
'                        If Len(line) Then
'                            token3 = Replace(nextToken(line), ";", "")
'                            IsFoundEntry = True
'                            Exit Do
'                        End If
'                    End If
'                End If
'            End If
'            begin = begin + 6
'        Loop
'        If IsFoundEntry = True Then
'            If InStr(UCase(Left(line, 10)), "DECLARE") > 0 Then
'                InitializeIncludeGRZ "DECLARE " & token1 & " " & token2 & " " & token3
'            Else
'                InitializeIncludeGRZ "DCL " & token1 & " " & token2 & " " & token3
'            End If
'        End If
'        IsFoundEntry = False
'        begin = 1
'
'
'        Do While RT.find("AIBTDLI", begin, Len(RT.text)) > 0
'            begin = RT.find("AIBTDLI", begin, Len(RT.text))
'            fineriga = RT.find(vbCrLf, begin + 6, Len(RT.text))
'            If fineriga > 0 Then
'                RT.SelStart = begin
'                RT.SelLength = fineriga - begin + 1
'                line = RT.SelText
'                If InStr(UCase(line), " ENTRY ") > 0 Or InStr(UCase(line), " ENTRY;") > 0 Then
'                    If Not (InStr(UCase(line), "*/") > 0 And Not InStr(UCase(line), "/*") > 0) Then
'                        token1 = Replace(nextToken(line), ")", "")
'                        token2 = nextToken(line)
'                        If Len(line) Then
'                            token3 = Replace(nextToken(line), ";", "")
'                            IsFoundEntry = True
'                            Exit Do
'                        End If
'                    End If
'                End If
'            End If
'            begin = begin + 6
'        Loop
'        If IsFoundEntry = True Then
'            If InStr(UCase(Left(line, 10)), "DECLARE") > 0 Then
'                InitializeIncludeGRZ "DECLARE " & token1 & " " & token2 & " " & token3
'            Else
'                InitializeIncludeGRZ "DCL " & token1 & " " & token2 & " " & token3
'            End If
'        End If
'        IsFoundEntry = False
'        begin = 1
'        Do While RT.find("SX000M0", begin, Len(RT.text)) > 0
'            begin = RT.find("SX000M0", begin, Len(RT.text))
'            fineriga = RT.find(vbCrLf, begin + 6, Len(RT.text))
'            If fineriga > 0 Then
'                RT.SelStart = begin
'                RT.SelLength = fineriga - begin + 1
'                line = RT.SelText
'                If InStr(UCase(line), " ENTRY ") > 0 Or InStr(UCase(line), " ENTRY;") > 0 Then
'                    If Not (InStr(UCase(line), "*/") > 0 And Not InStr(UCase(line), "/*") > 0) Then
'                        token1 = Replace(nextToken(line), ")", "")
'                        token2 = nextToken(line)
'                        If Len(line) Then
'                            token3 = Replace(nextToken(line), ";", "")
'                            IsFoundEntry = True
'                            Exit Do
'                        End If
'                    End If
'                End If
'            End If
'            begin = begin + 6
'        Loop
'        If IsFoundEntry = True Then
'            If InStr(UCase(Left(line, 10)), "DECLARE") > 0 Then
'                InitializeIncludeGRZ "DECLARE " & token1 & " " & token2 & " " & token3
'            Else
'                InitializeIncludeGRZ "DCL " & token1 & " " & token2 & " " & token3
'            End If
'        End If
'        IsFoundEntry = False
'        begin = 1
        '____________________________________________

      End If
      
      ' PLI MAIN
     'IRIS-DB: MOVED IN ANother location
      'If TipoFile = "PLI" Then
        'IRIS-DB: the parser does not work so well at the moment, in PLI it inserts a row only when the program is a main
        '         but does not set the "main" column to true and does not update the line where the MAIN is set
        'Set rmain = m_fun.Open_Recordset("SELECT * FROM PsPgm WHERE Idoggetto = " & rs!IdOggetto & " and Main = True")
        'Set rmain = m_fun.Open_Recordset("SELECT * FROM PsPgm WHERE Idoggetto = " & rs!IdOggetto)
        'If Not rmain.EOF Then
              'IRIS-DB: cambiato per la nuova gestione con IRIS
'              Dim targetmain As String
'              targetmain = Replace(Replace(rmain!parameters & "", ",", " "), "  ", " ")
'              If Len(targetmain) Then
'            'If RT.find("MAIN", 1, 5400) > 0 Then
'                ReDim Preserve FItable(UBound(FItable) + 1)
'                FItable(UBound(FItable)).CommentType = TipoFile
'                FItable(UBound(FItable)).isCaseSensitive = True 'verificare
'                FItable(UBound(FItable)).grouping_policy = "ONEROW"
'                FItable(UBound(FItable)).infoModify = "PCBPARAMETERS:" & rmain!parameters & ""
'                FItable(UBound(FItable)).isUnique = True
'                FItable(UBound(FItable)).target = Replace(Replace(rmain!parameters & "", ",", " "), "  ", " ")
'                FItable(UBound(FItable)).grouping_policy = "TERMINATOR"
'                FItable(UBound(FItable)).terminator = ";"
'              End If
          'End If
        'End If
        'rmain.Close
      'End If
      
      If r!tipo <> "CPY" And r!tipo <> "INC" Then
        If r!tipo = "EZT" Then
          Inizializza_Copy_IMSDB_EZT_OF RT, RTBErr
        Else
          Inizializza_Copy_IMSDB_OF RT, RTBErr, r!tipo, rs!IdOggetto, isVSAMbatch
          If isVSAMbatch And EmbeddedRoutine Then
            Inizializza_Data_Division RT, RTBErr, r!tipo, rs!IdOggetto, "INCAPS"
            Inizializza_File_Control RT, RTBErr, r!tipo, rs!IdOggetto, "INCAPS"
          End If
        End If
      ElseIf r!tipo = "INC" Then
        'AC 18/07/08 per le INC inserisco varibili AREA e KEYVALUE
        Inizializza_PLIINC_IMSDB RT, RTBErr, rs!IdOggetto
      End If
    Else
      If RT.FileName <> DirCBLcpy & "\" & NCBLcpyOF Then
        'Salva il file
        Crea_Directory_Cbl_Cpy DirCBLcpy 'crea le directory CBLOut e CPYOut se non esistono
        
        RT.SaveFile OldName, 1
        
        If MadrdF_Incapsulatore.Optsource Then
          RT.LoadFile DirCBLcpy & "\" & NCBLcpyOF
        Else
          RT.LoadFile MadrdM_Incapsulatore.Percorso & "\" & NCBLcpyOF
        End If
        'silvia 16-1-2008 : elimina dal file copia le direttive di compilazione DLI dalla XOPTS
        'If RT.find("XOPTS", 1, 72) > 0 Then
        ' Mauro: 72 = 1 riga; 3600 = 50 righe
        If RT.find("XOPTS", 1, 3600) > 0 Then
           Elimina_DLI_in_Xopts RT, r!tipo
        End If
        If r!tipo <> "CPY" Then
          If r!tipo = "EZT" Then
            Inizializza_Copy_IMSDB_EZT RT, RTBErr
          Else
            Inizializza_Copy_IMSDB RT, RTBErr, r!tipo, rs!IdOggetto
            'If r!Batch Then
            If isVSAMbatch And EmbeddedRoutine Then
              Inizializza_Data_Division RT, RTBErr, r!tipo, rs!IdOggetto, "INCAPS"
              Inizializza_File_Control RT, RTBErr, r!tipo, rs!IdOggetto, "INCAPS"
            End If
          End If
        End If
      End If
    End If
   
    'oldName = DirCBLcpy & "\Out\" & NCBLcpyOF
    OldName = MadrdM_Incapsulatore.Percorso & "\" & NCBLcpyOF
    OldName1 = NCBLcpyOF
  End If
  Exit Sub
err:
  MsgBox "Encapsulated IMSDB version of file not found", vbCritical, "File not found" & vbCrLf & err.Description
End Sub
'IRIS-DB the whole function
Public Sub EncapsENTRYTDLIPLI(RT As RichTextBox, IdOgg As Long)
  Dim rsmain As Recordset
  Dim idx01 As Long
  Dim i As Long
  Dim wSpc As Long
  Dim rsEntry As Recordset
  Dim rsPSB As Recordset
  Dim psbName As String
  Dim idx02, idx03, idx04 As Long 'IRIS-DB
  
  Dim lastPCB As String 'IRIS-DB
  Dim CMPAT_Yes As Boolean
  Dim PsbId As Long
  Dim addIOPcb As Long
  Dim dynamicSSA As Boolean
  Dim param As String
  Dim pcb() As String
  
  Set rsmain = m_fun.Open_Recordset("SELECT * FROM PsPgm WHERE Idoggetto = " & IdOgg)
  If rsmain.EOF Then
    rsmain.Close
    Exit Sub
  End If
    
  i = 1
    
  'per ora cerca solo "(MAIN)", cio� con la doppia parentesti; in futuro da sostituire correggendo il parser sulla PsPGM
  'non verifica neanche se commentata per ora
  idx01 = RT.find("(MAIN)", 0)
  If idx01 > 0 Then
    'cerca il fine istruzione
    idx01 = RT.find(";", idx01)
    If idx01 > 0 Then
      'cerca la fine della riga
      idx01 = RT.find(vbCrLf, idx01)
      If idx01 > 0 Then
        'IRIS-DB: attenzione che prende solo il primo in tabella in caso di PSB multipli
        Set rsPSB = m_fun.Open_Recordset("Select a.nome as PSBName, b.IdPSB as PsbId from bs_oggetti as a, PsDLI_IstrPSB as b  where " & _
                                        "a.idoggetto = b.IdPSB and b.idpgm = " & IdOgg)
        If Not rsPSB.EOF Then
          psbName = rsPSB!psbName
          PsbId = rsPSB!PsbId
          rsPSB.Close
        Else
      'IRIS-DB: faccio una pezza per non cambiare tutto: ci sono programmi che hanno solo la entry per cui non sono presenti nelle istruzioni
          rsPSB.Close
          Set rsPSB = m_fun.Open_Recordset("Select a.nome as PSBName, b.IdOggettoR as PsbId from bs_oggetti as a, PsRel_Obj as b where " & _
                                        "a.idoggetto = b.IdOggettoR and b.idOggettoC = " & IdOgg & " and Relazione = 'PSB'")
          If Not rsPSB.EOF Then
            psbName = rsPSB!psbName
            PsbId = rsPSB!PsbId
          Else
            psbName = "MISSING"
            PsbId = 0
          End If
          rsPSB.Close
        End If
        RT.SelStart = idx01 + 2
        RT.SelLength = 0
        RT.SelText = " /* IRISDB MAIN ALLOCATE PCB */" & vbCrLf _
                   & " IRIS_CALL_ID = 1;" & vbCrLf _
                   & " IRIS_COMMAND_CODES_TAB = ' ';" & vbCrLf _
                   & " IRIS_IMS_FUNCTION = 'DLIT';" & vbCrLf _
                   & " IRIS_WS_RTN = 'IRISUTIL';" & vbCrLf _
                   & " IRIS_PSB_NAME = '" & psbName & "';" & vbCrLf
        AddRighe = 6
        CMPAT_Yes = True
        Set rsPSB = m_fun.Open_Recordset("Select NumPcb, TypePcb, DbdName from PsDLI_PSB  where idOggetto = " & PsbId)
        If rsPSB.EOF Then
         'IRIS-DB only IO PCB present
          RT.SelText = RT.SelText & " IRIS_PCB_TYPE(1) = 'T';" & vbCrLf
        Else
          While Not rsPSB.EOF
            If rsPSB!numpcb = 1 Then
              CMPAT_Yes = False
            End If
            If CMPAT_Yes And rsPSB!numpcb = 2 Then
              CMPAT_Yes = False
              RT.SelText = RT.SelText & " IRIS_PCB_TYPE(1) = 'T';" & vbCrLf
              AddRighe = AddRighe + 1
            End If
            If rsPSB!TypePcb = "TP" Then
              RT.SelText = RT.SelText & " IRIS_PCB_TYPE(" & rsPSB!numpcb & ") = 'T';" & vbCrLf
            Else
              RT.SelText = RT.SelText & " IRIS_PCB_TYPE(" & rsPSB!numpcb & ") = 'D';" & vbCrLf
              RT.SelText = RT.SelText & " IRIS_PCB_DBD(" & rsPSB!numpcb & ") = '" & rsPSB!dbdname & "';" & vbCrLf
              AddRighe = AddRighe + 1
            End If
            AddRighe = AddRighe + 1
            rsPSB.MoveNext
          Wend
        End If
        rsPSB.Close
        RT.SelText = RT.SelText & " IRIS_MAIN_LANG = 'PLI';" & vbCrLf
        AddRighe = AddRighe + 1
        
        If Not rsmain!parameters = "" Then
          pcb = Split(rsmain!parameters, ",")
          RT.SelText = RT.SelText & " IRIS_PARAM_NUM = " & UBound(pcb) + 1 & ";" & vbCrLf
        Else
          RT.SelText = RT.SelText & " IRIS_PARAM_NUM = 1;" & vbCrLf
        End If
        AddRighe = AddRighe + 1
        RT.SelText = RT.SelText & " IRISIORT = 'IRISUTIL';" & vbCrLf
        AddRighe = AddRighe + 1
        RT.SelText = RT.SelText & " CALL IRISIORT (IRIS_WORK_AREA," & vbCrLf
        AddRighe = AddRighe + 1
        i = 0
        While Not i > UBound(pcb)
          If Not i = UBound(pcb) Then
            RT.SelText = RT.SelText & "                ADDR(" & Trim(pcb(i)) & ")," & vbCrLf
          Else
            RT.SelText = RT.SelText & "                ADDR(" & Trim(pcb(i)) & "));" & vbCrLf
          End If
          AddRighe = AddRighe + 1
          i = i + 1
        Wend
        RT.SelText = RT.SelText & " /* IRISDB MAIN END */" & vbCrLf
        AddRighe = AddRighe + 1
        m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = OffsetEncaps + " & AddRighe & " where idoggetto = " & IdOgg
      Else
        m_fun.WriteLog "EncapsENTRYTDLIPLI: Obj(" & IdOgg & ") not found Main paragraph", "MadrdM_Incapsulatore"
      End If
    Else
      m_fun.WriteLog "EncapsENTRYTDLIPLI: Obj(" & IdOgg & ") not found Main paragraph", "MadrdM_Incapsulatore"
    End If
  Else
    m_fun.WriteLog "EncapsENTRYTDLIPLI: Obj(" & IdOgg & ") not found Main paragraph", "MadrdM_Incapsulatore"
  End If
  rsmain.Close
End Sub

Sub CostruisciCommonInitializeTemplateC_OF(rst As Recordset)
  Dim Rtb As RichTextBox, wPath As String, appoFinalString As String, posEndLine As String
  Dim segLevel() As String, levelKey() As String, appoLevelString As String, k As Integer
  Dim K1 As Integer, i As Integer, h As Integer
  Dim dataType As String, templateName As String
 
  Set Rtb = MadrdF_Incapsulatore.RTBR
  wPath = m_fun.FnPathPrj
  'stefanopul
  'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMMONINIT"
  Select Case (MadrdM_Incapsulatore.TabIstr.istr)
    Case "PCB"
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMMONINIT_PCB"
    Case Else
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMMONINIT"
  End Select
  Rtb.LoadFile templateName
  'Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMMONINIT"
  
  ' per le copy non muoviamo niente in LNKDB-CALLEr, rimane il nome del chiamante
  'MadrdM_GenRout.changeTag Rtb, "<NAMEPGM>", Trim(NomeProg)
  If isPliCallinsideSelect Then
    MadrdM_GenRout.changeTag Rtb, "<insideselect>", ""
  End If
  
  appoFinalString = Rtb.text
  Testo = Testo & vbCrLf
  insertLineOF Testo, "", rst!IdOggetto
  
  '************  embedded *********
  If EmbeddedRoutine Then
    MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
    If Not MadrdM_Incapsulatore.TabIstr.pcb = "" Then
      MadrdM_GenRout.changeTag Rtb, "<PCB>", MadrdM_Incapsulatore.TabIstr.pcb
    End If
    If Not MadrdM_Incapsulatore.TabIstr.numpcb = 0 Then
      MadrdM_GenRout.changeTag Rtb, "<NUMPCB>", MadrdM_Incapsulatore.TabIstr.numpcb
    End If
   
    'Salvataggio parziale
    appoFinalString = Rtb.text
    'appofinalString = appofinalString & Rtb.text
   
    ReDim segLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
      ' GSAM to Sequential: da adeguare per GSAM to RDBMS
      If MadrdM_Incapsulatore.TabIstr.isGsam Then
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\SEGMENTLV_GSAM"
      Else
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\SEGMENTLV"
      End If
      
      DoEvents
      If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment = "" Then
        'AggLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!Riga & " missing segment", MadrdF_Incapsulatore.RTErr
      End If
       
      If UBound(MadrdM_Incapsulatore.TabIstr.psb) Then
        ' Mauro 16/12/2008
        If MadrdM_Incapsulatore.TabIstr.numpcb = 0 And UBound(MadrdM_Incapsulatore.TabIstr.DBD) Then
          h = Restituisci_Livello_Segmento_dbd(MadrdM_Incapsulatore.TabIstr.DBD(1).Id, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment)
        Else
          h = Restituisci_Livello_Segmento(MadrdM_Incapsulatore.TabIstr.psb(1).Id, MadrdM_Incapsulatore.TabIstr.numpcb, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment, 0)
        End If
      Else
        'stefano: istruzioni senza psb, assegna comunque un default
        'h = -1
        If UBound(MadrdM_Incapsulatore.TabIstr.DBD) Then
          h = Restituisci_Livello_Segmento_dbd(MadrdM_Incapsulatore.TabIstr.DBD(1).Id, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment, 0)
        Else
          h = -1
        End If
      End If
       
      If h >= 0 Then
        MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
      Else
        m_fun.WriteLog "Incaps: Obj(" & rst!IdOggetto & ") Row( " & rst!Riga & " missing level", "MadrdM_Incapsulatore"
        MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
      End If
      MadrdM_GenRout.changeTag Rtb, "<LEVEL>", k
       
      appoLevelString = Rtb.text
       
      ReDim levelKey(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))
  
      For K1 = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef)
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\LEVKEY"
        MadrdM_GenRout.changeTag Rtb, "<copy>", ""
        If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart <> 0 Then
          MadrdM_GenRout.changeTag Rtb, "<SSANAME>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName
          MadrdM_GenRout.changeTag Rtb, "<KEYSTARTC>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart
          MadrdM_GenRout.changeTag Rtb, "<KEYLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen
          MadrdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(k, "#0")
          MadrdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(K1, "#0")
          'AC 20/03/09
          getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName), rst!IdOggetto, 0, dataType, "", -1
          If dataType = "X" Then
            MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
          Else
            MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
          End If
        End If
        levelKey(K1) = Rtb.text
      Next K1
       
      ' riprendo da SEGMENTLV
      Rtb.text = appoLevelString
       
      For i = 1 To K1 - 1
        MadrdM_GenRout.changeTag Rtb, "<<LEVKEY(i)>>", levelKey(i)
      Next i
       
      'AC 24/10/2007 tolto da seglev e messo in main call con controllo diverso
'      If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(K).Record) Then
'        MadrdM_GenRout.changeTag Rtb, "<RECORD>", MadrdM_Incapsulatore.TabIstr.BlkIstr(K).Record
'        MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(K, "#0")
'        If TabIstr.isGsam Then
'          MadrdM_GenRout.changeTag Rtb, "<RECLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(K).AreaLen
'        End If
'      End If
       
      If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName) Then
        MadrdM_GenRout.changeTag Rtb, "<SSANAME>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName
        MadrdM_GenRout.changeTag Rtb, "<FLEVELSA>", Format(k, "#0")
        'AC 20/03/09
        getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName), rst!IdOggetto, 0, dataType, "", -1
        If dataType = "X" Then
          MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
        Else
          MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
        End If
      End If
       
      segLevel(k) = Rtb.text
    Next k
       
    'recupero maincall
    Rtb.text = appoFinalString
    If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) > 0 Then
      MadrdM_GenRout.changeTag Rtb, "<RECORD>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record
      MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr), "#0")
      If MadrdM_Incapsulatore.TabIstr.isGsam Then
        MadrdM_GenRout.changeTag Rtb, "<RECLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).areaLen
      End If
    End If
       
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
      MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV(i)>>", segLevel(k)
    Next k
    MadrdM_GenRout.changeTag Rtb, "<LANGUAGE>", TipoFile 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", TabIstr.istr 'IRIS-DB
    If TabIstr.istr <> "PCB" And TabIstr.istr <> "SCHE" Then 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", CInt(Right(TabDec(1).Codifica, 6)) 'IRIS-DB
    End If 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<SEGMENT>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).segment 'IRIS-DB
    'SQ 6-10-06
    'MOVE PCB "VERO" nel nostro: (per l'ultimo segmento letto, per es...)
    If Len(MadrdM_Incapsulatore.TabIstr.pcb) Then
      MadrdM_GenRout.changeTag Rtb, "<PCBCALL>", MadrdM_Incapsulatore.TabIstr.pcb
    End If
    appoFinalString = Rtb.text
  Else
    ' nel caso non embedded rendo disponibili dei tag al programmatore
    ' in questo modo inserendo i tag nel template p�o accedere ai
    ' campi della tabistr
    If MadrdM_Incapsulatore.TabIstr.istr <> "TERM" Then
      MadrdM_GenRout.changeTag Rtb, "<XPCB>", MadrdM_Incapsulatore.TabIstr.pcb
    End If
    MadrdM_GenRout.changeTag Rtb, "<XNUMPCB>", MadrdM_Incapsulatore.TabIstr.numpcb
    appoFinalString = Rtb.text
  End If
  
  While InStr(1, appoFinalString, vbCrLf) > 0
    posEndLine = InStr(1, appoFinalString, vbCrLf)
    insertLineOF Testo, Left(appoFinalString, posEndLine - 1), rst!IdOggetto, indentkey
    appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
  Wend

  'insertLineOF testo, ""
  If Not Trim(appoFinalString) = "" Then
    insertLineOF Testo, appoFinalString, rst!IdOggetto, indentkey
  End If
End Sub

Sub SetSegmentGroup_WM(segment As String, isKeyBuf As Boolean, isOnlyBuff As Boolean)
  Select Case segment
    Case "P1A", "P1B", "P1E"
      isKeyBuf = True
      Exit Sub
    Case "P1A5", "P1C", "P1D", "P1F", "P1H", "P1J", "P1L", "P1O", "P1Q"
      isOnlyBuff = True
      Exit Sub
    Case Else
      isKeyBuf = False
      isOnlyBuff = False
  End Select
End Sub

Function IsToMoveBuffer_WM(isKB As Boolean, isOB As Boolean, area As String) As Boolean
  If (isKB Or isOB) And Len(area) Then
    IsToMoveBuffer_WM = True
  Else
    IsToMoveBuffer_WM = False
  End If
End Function

Function IsToMoveKey_WM(isKB As Boolean, area As String, KeyDef() As BlkIstrKeyLocal, segment As String, Index As Integer) As Boolean
  Dim i As Integer
  
  If isKB And area = "" Then
    For i = 1 To UBound(KeyDef)
      'AC segmenti con la move delle chiave sono tutti di 3 lettere)
      If Left(KeyDef(i).key, 3) = segment And (KeyDef(i).op = "EQ" Or KeyDef(i).op = "=") Then
        Index = i
        IsToMoveKey_WM = True
        Exit Function
      End If
    Next i
  End If
End Function

Sub CostruisciAll_WM_OF(rst As Recordset, ptype As Integer, IstypeExec As Boolean)
  Dim Rtb As RichTextBox, wPath As String, appoFinalString As String, posEndLine As String
  Dim segLevel() As String, levelKey() As String, appoLevelString As String, k As Integer
  Dim K1 As Integer, i As Integer, h As Integer
  Dim templateName As String, appoXRST As String, dataType As String
  Dim isGroupKeyBuf As Boolean, isGroupOnlyBuff As Boolean
  Dim keyindex As Integer
  
  On Error GoTo catch
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  Select Case ptype
    Case 1
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\WM_MAIN_INSTRUCTION"
    Case 2
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\WM_MAIN_ISRT"
    Case Else
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\WM_MAIN_INSTRUCTION"
  End Select
  
  Rtb.LoadFile templateName
  MadrdM_GenRout.changeTag Rtb, "<program_name>", Trim(NomeProg)
  WMId = WMId + 1
  MadrdM_GenRout.changeTag Rtb, "<id>", WMId
  MadrdM_GenRout.changeTag Rtb, "<instruction>", MadrdM_Incapsulatore.TabIstr.istr
  If Not MadrdM_Incapsulatore.TabIstr.pcb = "" Then
    If IstypeExec Then
      MadrdM_GenRout.changeTag Rtb, "<numpcb>", MadrdM_Incapsulatore.TabIstr.pcb
    Else
      MadrdM_GenRout.changeTag Rtb, "<numpcb>", MadrdM_Incapsulatore.TabIstr.pcb & "(9:2)"
    End If
  Else
    MadrdM_GenRout.changeTag Rtb, "<numpcb>", MadrdM_Incapsulatore.TabIstr.numpcb
  End If
  appoFinalString = Rtb.text

  ReDim segLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)   ' ciclo su livelli
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\WM_SEGMENTLEVEL"
    Rtb.LoadFile templateName
    
    SetSegmentGroup_WM MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment, isGroupKeyBuf, isGroupOnlyBuff
    
    If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) > 0 Then
      If IsToMoveKey_WM(isGroupKeyBuf, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment, keyindex) Then
        MadrdM_GenRout.changeTag Rtb, "<segmentk>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment
        MadrdM_GenRout.changeTag Rtb, "<k>", ""
        'MadrdM_GenRout.changeTag Rtb, "<key>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(keyindex).KeyVal
        MadrdM_GenRout.changeTag Rtb, "<key>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(keyindex).key
      End If
    End If
    
    If IsToMoveBuffer_WM(isGroupKeyBuf, isGroupOnlyBuff, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record) Then
      MadrdM_GenRout.changeTag Rtb, "<segmentb>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment
      MadrdM_GenRout.changeTag Rtb, "<b>", ""
      MadrdM_GenRout.changeTag Rtb, "<buffer>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record
    End If
    
    segLevel(k) = Rtb.text
  Next k
  ' riprendo da SEGMENTLEVEL
  Rtb.text = appoFinalString
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
    MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLEVEL(i)>>", segLevel(k), True
  Next k
  appoFinalString = Rtb.text
    
  While InStr(appoFinalString, vbCrLf) > 0
    posEndLine = InStr(1, appoFinalString, vbCrLf)
    insertLineOF_WM Testo, Left(appoFinalString, posEndLine - 1), rst!IdOggetto, indentkey
    appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
  Wend
  Select Case ptype
    Case 1
      VItable(UBound(VItable)).lines_down = Testo
    Case 2
      VItable(UBound(VItable)).lines_up = Testo
    Case Else
      VItable(UBound(VItable)).lines_down = Testo
  End Select
  
  Testo = ""
  
  '***********  ISRT,DLT  parte sotto (solo per CICS SCREEN, tipo 2)
  If ptype = 2 Then
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\WM_POST_ISRT"
    Rtb.LoadFile templateName
    'WMId = WMId + 1 rimane allo stesso valore della parte sopra istruzione
    MadrdM_GenRout.changeTag Rtb, "<id>", WMId
    appoFinalString = Rtb.text
    
    While InStr(appoFinalString, vbCrLf) > 0
      posEndLine = InStr(1, appoFinalString, vbCrLf)
      insertLineOF_WM Testo, Left(appoFinalString, posEndLine - 1), rst!IdOggetto, indentkey
      appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
    Wend
    VItable(UBound(VItable)).lines_down = Testo
    Testo = ""
  End If
  '*****************************************************
  Exit Sub
catch:
    
End Sub
Function CheckMolt(pIdPgm As Long, pIdOggetto As Long, pRiga As Long) As Boolean
Dim rsDecodificaIstr As Recordset
 Set rsDecodificaIstr = m_fun.Open_Recordset("SELECT count(*) as NumCod from PsDLi_IstrCodifica where IdPgm = " _
                                          & pIdPgm & " and IdOggetto = " & pIdOggetto & " and riga = " & pRiga)
 If Not rsDecodificaIstr.EOF Then
  If rsDecodificaIstr!NumCod > 1 Then
    CheckMolt = True
  End If
End If
                    
                    
End Function

Sub CostruisciCommonInitializeTemplate_OF(rst As Recordset)
  Dim Rtb As RichTextBox, wPath As String, appoFinalString As String, posEndLine As String
  Dim segLevel() As String, levelKey() As String, appoLevelString As String, k As Integer
  Dim K1 As Integer, i As Integer, h As Integer
  Dim templateName As String, appoXRST As String, dataType As String
  On Error GoTo catch
  
  'AC _OF
  'lascio cos� per ora, dovrebbe costruire il testo da inserire successivamente
  Set Rtb = MadrdF_Incapsulatore.RTBR
  Select Case (MadrdM_Incapsulatore.TabIstr.istr)
    Case "PCB"
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMMONINIT_PCB"
    Case Else
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMMONINIT"
  End Select
  Rtb.LoadFile templateName
  
  'Controllo molteplicit� per operation = unk
  If CheckMolt(rst!idpgm, rst!IdOggetto, rst!Riga) Then
    MadrdM_GenRout.changeTag Rtb, "<MOLT>", ""
  End If
  
  MadrdM_GenRout.changeTag Rtb, "<NAMEPGM>", Trim(NomeProg)
  If isPliCallinsideSelect Then
    MadrdM_GenRout.changeTag Rtb, "<insideselect>", ""
  End If
  appoFinalString = Rtb.text
  Testo = ""
  insertLineOF Testo, "", rst!IdOggetto
  
  '************  embedded *********
  If EmbeddedRoutine Then
    MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
    If Not MadrdM_Incapsulatore.TabIstr.pcb = "" Then
      MadrdM_GenRout.changeTag Rtb, "<PCB>", MadrdM_Incapsulatore.TabIstr.pcb
    End If
    If Not MadrdM_Incapsulatore.TabIstr.numpcb = 0 Then
      MadrdM_GenRout.changeTag Rtb, "<NUMPCB>", MadrdM_Incapsulatore.TabIstr.numpcb
    End If
   
    'Salvataggio parziale
    appoFinalString = Rtb.text
   
    ReDim segLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
      ' GSAM to Sequential: da adeguare per GSAM to RDBMS
      If MadrdM_Incapsulatore.TabIstr.isGsam Then
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLV_GSAM"
      Else
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\" & dirincaps & "SEGMENTLV"
      End If
      Rtb.LoadFile templateName
      
      If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment = "" Then
        'AggLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!Riga & " missing segment", MadrdF_Incapsulatore.RTErr
      End If
       
      If UBound(MadrdM_Incapsulatore.TabIstr.psb) Then
        ' Mauro 16/12/2008
        If MadrdM_Incapsulatore.TabIstr.numpcb = 0 And UBound(MadrdM_Incapsulatore.TabIstr.DBD) Then
          h = Restituisci_Livello_Segmento_dbd(MadrdM_Incapsulatore.TabIstr.DBD(1).Id, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment)
        Else
          h = Restituisci_Livello_Segmento(MadrdM_Incapsulatore.TabIstr.psb(1).Id, MadrdM_Incapsulatore.TabIstr.numpcb, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment, 0)
        End If
      Else
        'stefano: istruzioni senza psb, assegna comunque un default
        'h = -1
        If UBound(MadrdM_Incapsulatore.TabIstr.DBD) Then
          h = Restituisci_Livello_Segmento_dbd(MadrdM_Incapsulatore.TabIstr.DBD(1).Id, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment, 0)
        Else
          h = -1
        End If
      End If
       
      If h >= 0 Then
        MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
      Else
        m_fun.WriteLog "Incaps: Obj(" & rst!IdOggetto & ") Row( " & rst!Riga & " missing level", "MadrdM_Incapsulatore"
        MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
      End If
      MadrdM_GenRout.changeTag Rtb, "<LEVEL>", k
     
      appoLevelString = Rtb.text
     
      ReDim levelKey(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))

      For K1 = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef)
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\LEVKEY"
        Rtb.LoadFile templateName
         
        If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart <> 0 Then
          MadrdM_GenRout.changeTag Rtb, "<SSANAME>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName
          MadrdM_GenRout.changeTag Rtb, "<KEYSTART>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart
          MadrdM_GenRout.changeTag Rtb, "<KEYLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen
          MadrdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(k, "#0")
          MadrdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(K1, "#0")
          'AC 20/03/09
          getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName), rst!IdOggetto, 0, dataType, "", -1
          If dataType = "X" Then
            MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
          Else
            MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
          End If
        Else
          AggLog "[Object: " & rst!IdOggetto & "] Riga: " & rst!Riga & " KEYSTART(" & K1 & ") level " & k & " missing.", MadrdF_Incapsulatore.RTErr
        End If
        
         
        levelKey(K1) = Rtb.text
      Next K1
       
      ' riprendo da SEGMENTLV
      Rtb.text = appoLevelString
       
      For i = 1 To K1 - 1
        MadrdM_GenRout.changeTag Rtb, "<<LEVKEY(i)>>", levelKey(i)
      Next i
       
      If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName) Then
        MadrdM_GenRout.changeTag Rtb, "<SSANAME>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName
        MadrdM_GenRout.changeTag Rtb, "<SSALEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).SsaLen
        MadrdM_GenRout.changeTag Rtb, "<FLEVELSA>", Format(k, "#0")
        'AC 20/03/09
        getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName), rst!IdOggetto, 0, dataType, "", -1
        If dataType = "X" Then
          MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
        Else
          MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
        End If
      End If
     
      segLevel(k) = Rtb.text
    Next k
       
    'recuper maincall
    Rtb.text = appoFinalString
    'Rtb.SaveFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\logpar ", 1
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
      MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV(i)>>", segLevel(k)
    Next k
    If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) > 0 Then
      MadrdM_GenRout.changeTag Rtb, "<RECORD>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record
      MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr), "#0")
      If MadrdM_Incapsulatore.TabIstr.isGsam Then
        MadrdM_GenRout.changeTag Rtb, "<RECLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).areaLen
      End If
    End If
    MadrdM_GenRout.changeTag Rtb, "<LANGUAGE>", TipoFile 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", MadrdM_Incapsulatore.TabIstr.istr 'IRIS-DB
    If MadrdM_Incapsulatore.TabIstr.istr <> "PCB" And MadrdM_Incapsulatore.TabIstr.istr <> "SCHE" Then 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", CInt(Right(TabDec(1).Codifica, 6)) 'IRIS-DB
    End If 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<SEGMENT>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).segment 'IRIS-DB
    'MOVE PCB "VERO" nel nostro: (per l'ultimo segmento letto, per es...)
    If Len(MadrdM_Incapsulatore.TabIstr.pcb) Then
      MadrdM_GenRout.changeTag Rtb, "<PCBCALL>", MadrdM_Incapsulatore.TabIstr.pcb
    End If
    appoFinalString = Rtb.text
  Else
    ' nel caso non embedded rendo disponibili dei tag al programmatore
    ' in questo modo inserendo i tag nel template p�o accedere ai
    ' campi della tabistr
    If Not rst!isExec Then
      If MadrdM_Incapsulatore.TabIstr.istr <> "TERM" Then
        MadrdM_GenRout.changeTag Rtb, "<XPCB>", MadrdM_Incapsulatore.TabIstr.pcb
      End If
      MadrdM_GenRout.changeTag Rtb, "<XNUMPCB>", MadrdM_Incapsulatore.TabIstr.numpcb
    End If
    appoFinalString = Rtb.text
  End If
  
  While InStr(appoFinalString, vbCrLf) > 0
    posEndLine = InStr(1, appoFinalString, vbCrLf)
    insertLineOF Testo, Left(appoFinalString, posEndLine - 1), rst!IdOggetto, indentkey
    appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
  Wend

  insertLineOF Testo, "", rst!IdOggetto
  If Not Trim(appoFinalString) = "" Then
    insertLineOF Testo, appoFinalString, rst!IdOggetto, indentkey
  End If
  Exit Sub
catch:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & rst!IdOggetto & "] Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  ElseIf err.Number = 75 Then
    AggLog "[Object: " & rst!IdOggetto & "] Template: " & templateName & " - " & err.Description, MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & rst!IdOggetto & "] unexpected error: " & err.Description, MadrdF_Incapsulatore.RTErr
  End If
End Sub

Private Function insertLineOF(inputText As String, Line As String, pIdOggetto As Long, Optional indent As Integer = 7) As Boolean
  Dim statement As String, extraIndent As Integer, LenbefTrim As Integer, appoIndent As Integer
  Dim startTag As String, lenUtil As Integer
  Dim j As Integer, k As Integer
  Dim splitLine As String, splitLine2 As String
  Dim strParam As String, numSpaces As Integer, oldLen As Integer
  Dim irisComment As Boolean 'IRIS-DB
  Dim irisFixPCB As Boolean 'IRIS-DB
  irisComment = InStr(Line, "IRISXX") 'IRIS-DB
  If irisComment Then 'IRIS-DB
    Line = Replace(Line, "IRISXX ", "") 'IRIS-DB
  End If 'IRIS-DB
  irisFixPCB = InStr(Line, "FIXPCB") 'IRIS-DB
  If irisFixPCB Then 'IRIS-DB
     Line = Replace(Line, "FIXPCB ", "") 'IRIS-DB
  End If 'IRIS-DB
  
  insertLineOF = True
  
  On Error GoTo errinsert
  extraIndent = getTotalIndent(Line) ' ripulisce anche dai tag indent
  ' se rimangono tag non risolti non inserisco linea
  If (InStr(1, Line, "<") > 0 And InStr(1, Line, ">") > 0) Or Trim(Line) = "" Then
    insertLineOF = False
    Exit Function
  End If
  If isPLI Then
    If indent + extraIndent > 0 Then
      statement = Space(indent + extraIndent) & Line
    Else
      LenbefTrim = Len(Line)
      Line = LTrim(Line)
      statement = Space(indent + extraIndent + (LenbefTrim - Len(Line))) & Line
    End If
  Else
    startTag = START_TAG  'Copia: viene modificato
  '  If startTag = "" Then
  '    startTag = "IRIS-P"
  '  End If
    
    ' startTag formattato a sette caratteri
    If Len(startTag) > 7 Then
      startTag = Left(startTag, 7)
    ElseIf Len(startTag) < 7 Then
      startTag = startTag & Space(7 - Len(startTag))
    End If
    ' tolgo spazi davanti a linea e li sommo all'indentazione
    LenbefTrim = Len(Line)
    Line = LTrim(Line)
    appoIndent = indent + (LenbefTrim - Len(Line))
    If Len(Line) > Len(startTag) - extraIndent Then
      If Not (appoIndent - Len(startTag) + extraIndent) < 0 Then
        'statement = startTag & Space(indent - 7 + extraindent) & Right(Line, Len(Line) - Len(startTag))
        statement = startTag & Space(appoIndent - Len(startTag) + extraIndent) & Line
      Else
        'statement = startTag & Space(indent - 7) & Right(Line, Len(Line) - Len(startTag) + extraindent)
      End If
    Else
      statement = startTag & Space(appoIndent - 7) & Line
    End If
  End If
  'stefano: lo ho reinserito, altrimenti considera > 72 anche quelli con spazi in fondo
  statement = RTrim(statement)
  'stefano: sono entrato a gamba tesa, ma non mi � chiaro questo giro...
  If Len(statement) >= 73 Then
    'a capo automatico...
    'stefano: VALE SOLO PER IL COBOL, per ora...
    j = Len(RTrim(statement))
    splitLine = (RTrim(statement))
    k = 72
    splitLine2 = Space(72)
    Do While Mid(splitLine, j, 1) <> " " Or j > 72
      Mid(splitLine2, k, 1) = Mid(splitLine, j, 1)
      Mid(splitLine, j, 1) = " "
      k = k - 1
      j = j - 1
    Loop
    If j > 0 Then
      ' la prima parte di if � pezza per exec PLI
      If Left(Trim(splitLine2), 1) = "'" And InStr(splitLine, "('") > 0 Then
        strParam = Trim(Right(splitLine, Len(splitLine) - InStr(splitLine, "('") + 1))
        If Len(strParam) < 10 Then
          strParam = strParam & Space(10 - Len(strParam))
        End If
        splitLine = Left(splitLine, InStr(splitLine, "('") - 1)
        oldLen = Len(splitLine2)
        splitLine2 = Trim(splitLine2)
        If InStr(splitLine, "CALL") > 0 Then
          numSpaces = InStr(splitLine, "CALL") - 1
        Else
          numSpaces = oldLen - Len(splitLine2)
        End If
        splitLine2 = Space(numSpaces) & strParam & splitLine2
        statement = RTrim(splitLine) & Space(72 - Len(RTrim(splitLine))) & END_TAG
        If irisComment Then 'IRIS-DB
          statement = Replace(statement, "IRISDB ", "IRISXX*") 'IRIS-DB
        End If 'IRIS-DB
        If irisFixPCB Then 'IRIS-DB
          statement = Replace(statement, "IRISDB ", "FIXPCB ") 'IRIS-DB
        End If 'IRIS-DB
        inputText = inputText & statement & vbCrLf
        If Len(RTrim(splitLine2)) < 72 Then
          statement = RTrim(splitLine2) & Space(72 - Len(RTrim(splitLine2))) & END_TAG
        Else
          statement = Right(RTrim(splitLine2), 72)
        End If
        If irisComment Then 'IRIS-DB
          statement = Replace(statement, "IRISDB ", "IRISXX*") 'IRIS-DB
        End If 'IRIS-DB
        If irisFixPCB Then 'IRIS-DB
          statement = Replace(statement, "IRISDB ", "FIXPCB ") 'IRIS-DB
        End If 'IRIS-DB
        inputText = inputText & statement & vbCrLf
      Else
        statement = RTrim(splitLine) & Space(72 - Len(RTrim(splitLine))) & END_TAG
        If irisComment Then 'IRIS-DB
          statement = Replace(statement, "IRISDB ", "IRISXX*") 'IRIS-DB
        End If 'IRIS-DB
        If irisFixPCB Then 'IRIS-DB
          statement = Replace(statement, "IRISDB ", "FIXPCB ") 'IRIS-DB
        End If 'IRIS-DB
        If Not Trim(statement) = "" Then  'AC 12/04/10
            inputText = inputText & statement & vbCrLf
        End If
        'space(20) arbitratrio  Ac 12/04/10 : corretto prima era 72 - ....
        'IRIS-DB statement = startTag & Space(20) & LTrim(splitLine2) & Space(52 - Len(LTrim(splitLine2))) & END_TAG
        If Len(LTrim(splitLine2)) < 46 Then
          statement = startTag & Space(20) & LTrim(splitLine2) & Space(45 - Len(LTrim(splitLine2))) & END_TAG
        End If
        'AC 17/06/08 ritocco space(20) se necessario
        lenUtil = Len(startTag & Space(20) & LTrim(splitLine2))
        If lenUtil > 72 Then
          statement = startTag & Space(20 - (lenUtil - 72)) & LTrim(splitLine2) & Space(72 - Len(LTrim(splitLine2))) & END_TAG
        End If
        If irisComment Then 'IRIS-DB
          statement = Replace(statement, "IRISDB ", "IRISXX*") 'IRIS-DB
        End If 'IRIS-DB
        If irisFixPCB Then 'IRIS-DB
          statement = Replace(statement, "IRISDB ", "FIXPCB ") 'IRIS-DB
        End If 'IRIS-DB
        inputText = inputText & statement & vbCrLf
      End If
    End If
  Else
    If Not isPLI Then
      statement = statement & Space(72 - Len(statement)) & END_TAG
    End If
    If irisComment Then 'IRIS-DB
      statement = Replace(statement, "IRISDB ", "IRISXX*") 'IRIS-DB
    End If 'IRIS-DB
    If irisFixPCB Then 'IRIS-DB
      statement = Replace(statement, "IRISDB ", "FIXPCB ") 'IRIS-DB
    End If 'IRIS-DB
    inputText = inputText & statement & vbCrLf
  End If
  Exit Function
errinsert:
  AggLog "[Object: " & pIdOggetto & "] unexpected error on 'insertlineOF'.", MadrdF_Incapsulatore.RTErr
  Resume Next
End Function

Private Function insertLineOF_WM(inputText As String, Line As String, pIdOggetto As Long, Optional indent As Integer = 7) As Boolean
  Dim statement As String, extraIndent As Integer, LenbefTrim As Integer, appoIndent As Integer
  Dim startTag As String, lenUtil As Integer
  Dim j As Integer, k As Integer
  Dim splitLine As String, splitLine2 As String
  Dim strParam As String, numSpaces As Integer, oldLen As Integer
  Dim spaceblank As String
  insertLineOF_WM = True
  
  On Error GoTo errinsert
  extraIndent = getTotalIndent(Line) ' ripulisce anche dai tag indent
  ' se rimangono tag non risolti non inserisco linea
  If (InStr(1, Line, "<") > 0 And InStr(1, Line, ">") > 0) Or Trim(Line) = "" Then
    insertLineOF_WM = False
    Exit Function
  End If
  
  ' tolgo spazi davanti a linea e li sommo all'indentazione
  LenbefTrim = Len(Line)
  Line = LTrim(Line)
  appoIndent = indent + (LenbefTrim - Len(Line))
  If Len(Line) > Len(startTag) - extraIndent Then
    If Not (appoIndent - Len(startTag) + extraIndent) < 0 Then
      statement = startTag & Space(appoIndent - Len(startTag) + extraIndent) & Line
    End If
  Else
    statement = startTag & Space(appoIndent - 7) & Line
  End If
  
  'stefano: lo ho reinserito, altrimenti considera > 72 anche quelli con spazi in fondo
  'statement = Line
  statement = RTrim(statement)
  'stefano: sono entrato a gamba tesa, ma non mi � chiaro questo giro...
  If Len(statement) >= 73 Then
    'a capo automatico...
    'stefano: VALE SOLO PER IL COBOL, per ora...
    j = Len(RTrim(statement))
    splitLine = (RTrim(statement))
    k = 72
    splitLine2 = Space(72)
    Do While Mid(splitLine, j, 1) <> " " Or j > 72
      Mid(splitLine2, k, 1) = Mid(splitLine, j, 1)
      Mid(splitLine, j, 1) = " "
      k = k - 1
      j = j - 1
    Loop
    If j > 0 Then
      ' la prima parte di if � pezza per exec PLI
      If Left(Trim(splitLine2), 1) = "'" And InStr(splitLine, "('") > 0 Then
        strParam = Trim(Right(splitLine, Len(splitLine) - InStr(splitLine, "('") + 1))
        If Len(strParam) < 10 Then
          strParam = strParam & Space(10 - Len(strParam))
        End If
        splitLine = Left(splitLine, InStr(splitLine, "('") - 1)
        oldLen = Len(splitLine2)
        splitLine2 = Trim(splitLine2)
        If InStr(splitLine, "CALL") > 0 Then
          numSpaces = InStr(splitLine, "CALL") - 1
        Else
          numSpaces = oldLen - Len(splitLine2)
        End If
        splitLine2 = Space(numSpaces) & strParam & splitLine2
        statement = RTrim(splitLine) & Space(72 - Len(RTrim(splitLine))) & END_TAG
          
        inputText = inputText & statement & vbCrLf
        If Len(RTrim(splitLine2)) < 72 Then
          statement = RTrim(splitLine2) & Space(72 - Len(RTrim(splitLine2))) & END_TAG
        Else
          statement = Right(RTrim(splitLine2), 72)
        End If
        inputText = inputText & statement & vbCrLf
      Else
        statement = RTrim(splitLine) & Space(72 - Len(RTrim(splitLine))) & END_TAG
        inputText = inputText & statement & vbCrLf
        'space(20) arbitratrio
        statement = startTag & Space(20) & LTrim(splitLine2) & Space(72 - Len(LTrim(splitLine2))) & END_TAG
        'AC 17/06/08 ritocco space(20) se necessario
        lenUtil = Len(startTag & Space(20) & LTrim(splitLine2))
        If lenUtil > 72 Then
          statement = startTag & Space(20 - (lenUtil - 72)) & LTrim(splitLine2) & Space(72 - Len(LTrim(splitLine2))) & END_TAG
        End If
        inputText = inputText & statement & vbCrLf
      End If
    End If
  Else
    'statement = statement & Space(72 - Len(statement)) & END_TAG
    If Mid(Line, 7, 1) = "*" Then
      statement = Trim(statement)
    End If
    inputText = inputText & statement & vbCrLf
  End If
  Exit Function
errinsert:
  AggLog "[Object: " & pIdOggetto & "] unexpected error on 'insertlineOF'.", MadrdF_Incapsulatore.RTErr
End Function

Sub INSERT_DECOD_OF(wPos As Long, RTBModifica As RichTextBox, RTBErr As RichTextBox, rs As Recordset, typeIncaps As String, Optional seqInstr As Integer = 0)
  Dim wIstruzione As String, statement As String
  Dim blankspace As String
  On Error GoTo catch
  
  ' nella vecchia versione no template
  If Not rs!isExec Then
    Testo = CostruisciCall_CALLTemplate_OF(Testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr, False, seqInstr)
  Else
    Testo = CostruisciCall_CALLTemplateOF_EXEC(Testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr, False, seqInstr)
  End If
  
   RTBModifica.SelStart = wPos
'   RTBModifica.SelLength = 0
'   RTBModifica.SelText = Testo
'   wPos = wPos + Len(Testo)
         
 
  If (indentkey - Len(START_TAG) - 3) > 0 Then
    blankspace = Space(indentkey - Len(START_TAG))
  Else
    blankspace = Space(3)
  End If
  'IRIS-DB start
''  If wPunto Then
''    If rs!isExec Then
''      ' Mauro 29/07/2008
''      If TipoFile = "CBL" Or TipoFile = "CPY" Then
''        statement = START_TAG & blankspace & "."
''      End If
''    Else
''      statement = START_TAG & blankspace & "."
''    End If
''  End If
''
''  If Not rs!isExec Then
''    statement = statement & Space(72 - Len(statement)) & END_TAG
''  End If
  'IRIS-DB end
  'AC _OF
'IRIS-DB IRIS-DB  If Not statement = "" Then
'IRIS-DB  If Not Trim(statement) = "" Then 'IRIS-DB
'IRIS-DB    Testo = Testo & vbCrLf & statement
'IRIS-DB  End If
  
  If TipoFile = "PLI" Or TipoFile = "INC" Then 'IRIS-DB
    'Testo = Testo & IIf(wPunto, ".", "") 'IRIS-DB: il punto non va in PLI, casomai un punto e virgola, che comunque c'� gi� sempre
  Else 'IRIS-DB
    Testo = Trim(Testo) & IIf(wPunto, ".", "")
  End If 'IRIS-DB
  
  VItable(UBound(VItable)).lines_down = VItable(UBound(VItable)).lines_down & vbCrLf & Testo
  
  '**********************************�����������������������
  'TO DO
  ' creare il record su VItable
  ' chiamare la sub per l'elaborazione del file
  
  'ReDim Preserve VItable(UBound(VItable) + 1)
  'VItable(UBound(VItable)).actionFail = ""
  'VItable(UBound(VItable)).CommentType = "CBL"
  'VItable(UBound(VItable)).grouping_policy = "TERMINATOR"
  '************************************��������������������
  'AC 30/04708
'  If Not statement = "" Then
'    RTBModifica.SelStart = wPos
'    RTBModifica.SelLength = 0
'    RTBModifica.SelText = statement
'    wPos = wPos + Len(statement)
'  End If
  
  'SQ 18-04-08
  Exit Sub
catch:
  AggLog "[Object: " & rs!IdOggetto & " - Row: " & rs!Riga & "] INSERT DECOD ERROR", MadrdF_Incapsulatore.RTErr
End Sub

Sub INSERT_NODECOD_OF(rst As Recordset)
  Dim Testo As String
  
  'SP segmenti e DBD obsoleti
  'If rst!template = "OBSO" Then
  If rst!obsolete Then
    'IRIS-DB attenzione che la prossima modifica potrebbe essere pericolosa: le CBLTDLI su una sola riga non funzionano bene, se c'� una riga a blank dopo
    'IRIS-DB la include nell'istruzione
    'IRIS-DB Testo = vbcrlf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
    Testo = m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
    Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "*   OBSOLETE INSTRUCTION" 'IRIS-DB
    Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
    'IRIS-DB removing custom abend
'''''    If rst!Istruzione = "ISRT" Or rst!Istruzione = "DLET" Or rst!Istruzione = "REPL" Or Len(rst!Istruzione & "") = 0 Then
'''''      If SwTp Then
'''''        Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "     EXEC CICS ABEND ABCODE('OBSO') END-EXEC"
'''''      Else
'''''        Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "     CALL 'OBSO'"
'''''      End If
'''''    Else
      'IRIS-DB: usato il campo template della psdli_istruzioni per modificare il default del codice di ritorno; da mettere nella OBSO_FINAL
      If Len(rst!template) Then
        Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "     MOVE " & rst!template & " TO " & Trim(rst!numpcb) & "(11:2)" & IIf(wPunto, ".", "") 'IRIS-DB
      Else
        Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "     MOVE 'GE' TO " & Trim(rst!numpcb) & "(11:2)" & IIf(wPunto, ".", "") 'IRIS-DB
      End If
'''''    End If
    AddRighe = AddRighe + 4
  Else
    If TipoFile = "PLI" Or TipoFile = "INC" Then
      Testo = vbCrLf & " /* " & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------*/"
      Testo = Testo & vbCrLf & " /* " & m_fun.LABEL_NONDECODE & "    WARNING: NOT SOLVED INSTRUCTION                       */"
      Testo = Testo & vbCrLf & " /* " & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------*/"
    Else
      Testo = vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
      Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "*   WARNING: NOT SOLVED INSTRUCTION"
      Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
    End If
    Testo = Testo & vbCrLf
    
  End If
  VItable(UBound(VItable)).lines_down = VItable(UBound(VItable)).lines_down & vbCrLf & Testo
End Sub

Public Sub Inizializza_Copy_IMSDB_EZT_OF(RT As RichTextBox, RTBErr As RichTextBox)
  Dim t As Long, w As Long
  Dim Testo As String
  Dim s() As String
  Dim nfile As Long
  Dim lFile As Long
  Dim sLine As String
  Dim templateName As String
  
  'stefano: deve cambiare la definizione dei file dbd in aree di di working
  ' per ora faccio finta che l'offset macro non esista...
  
  ReDim CmpPcb(0)
  Dim wJI() As String
  Dim x As Long
    
  On Error GoTo ErrJob
   
  ReDim Preserve FItable(UBound(FItable) + 1)
  
  FItable(UBound(FItable)).grouping_policy = "ONEROW"
  FItable(UBound(FItable)).target = "JOB INPUT"
    
  'TARGET
'  Dim foundx As Boolean
'  foundx = False
'  w = 1
'  'stefano: un po meglio, ma ancora non gestisce le asteriscate, ecc.
'  While Not foundx And w < Len(RT.text)
'    w = RT.find("JOB ", w + 1)
'    x = RT.find(vbCrLf, w) 'VIRGILIOOO
'    x = RT.find("INPUT", w, x)
'    If x > 0 Then
'      foundx = True
'    End If
'  Wend
'
'  w = RT.find(vbCrLf, w - 2)
'  w = w - 1
'  RT.SelStart = w
'  RT.SelLength = 0
        
  'Template di Working:
  If EmbeddedRoutine Then
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\EZT\WorkingPgmEZTDB"
  Else
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps\EZT\InitCpyRoutIMSDBezt.tpl"
  End If
    
  If Len(Dir(templateName, vbNormal)) Then
    nfile = FreeFile
    lFile = FileLen(templateName)
    
    Open templateName For Binary As nfile
    Testo = ""
    While Loc(nfile) < lFile
      Line Input #nfile, sLine
      
      'Trascodifica le label
      If InStr(1, sLine, "#LABEL_AUTHOR#") > 0 Then
        sLine = Replace(sLine, "#LABEL_AUTHOR#", m_fun.LABEL_AUTHOR)
        Testo = Testo & vbCrLf & sLine
      ElseIf InStr(1, sLine, "#LABEL_INFO#") > 0 Then
        sLine = Replace(sLine, "#LABEL_INFO#", m_fun.LABEL_INFO)
        Testo = Testo & vbCrLf & sLine
      ElseIf InStr(1, sLine, "#LABEL_INCAPS#") > 0 Then
        sLine = Replace(sLine, "#LABEL_INCAPS#", m_fun.LABEL_INCAPS)
        Testo = Testo & vbCrLf & sLine
      Else
        Testo = Testo & LABEL_INFO & vbCrLf & sLine
      End If
      'testo = testo & vbCrLf & sLine
    Wend
    Close nfile
  Else
    'SQ - E' sempre bello scrivere dentro i programmi generati!
    AggLog "* TEMPLATE NOT FOUND: " & templateName, RTBErr
    Testo = ""
    Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
    Testo = Testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
    Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
  End If
  
  FItable(UBound(FItable)).lines_down = Testo
'  RT.SelText = Testo
'
'  s = Split(Testo, vbCrLf)
'  AddRighe = UBound(s)
  Exit Sub
ErrJob:
  MsgBox "JOB INPUT not found "
End Sub

Public Sub Inizializza_Copy_IMSDB_OF(RT As RichTextBox, RTBErr As RichTextBox, ptype As String, pIdOggetto As Long, Optional VsamBatch As Boolean = False)
  'controlla se il link con la copy LNKDBRT � presente nel file
  Dim t As Long, w As Long
  Dim Testo As String
  Dim s() As String
  Dim nfile As Long
  Dim lFile As Long
  Dim sLine As String
  Dim templateName As String
  Dim keyword As String
  Dim boladdtesto As Boolean
  Dim pstart As Long
  Dim firstTime As Boolean 'IRIS-DB
  Dim rsPtr As Recordset 'IRIS-DB
  
  boladdtesto = True
  ReDim CmpPcb(0)
  'OF - comincio a scrivere righe su FItable
  ' la parola chiave da ricercare sar� WORKING-STORAGE oppure DCL
  ReDim Preserve FItable(UBound(FItable) + 1)
  
  keyword = "WORKING-STORAGE"
  FItable(UBound(FItable)).CommentType = "CBL"
  FItable(UBound(FItable)).isCaseSensitive = True
  FItable(UBound(FItable)).infoModify = "CBL"
  FItable(UBound(FItable)).grouping_policy = "ONEROW" ' mi interessa solo attaccare testo senza commentare
  If ptype = "PLI" Then
    keyword = "DCL"
    FItable(UBound(FItable)).CommentType = "PLI"
    FItable(UBound(FItable)).isCaseSensitive = False
    FItable(UBound(FItable)).infoModify = "PLI"
    FItable(UBound(FItable)).grouping_policy = "TERMINATOR"
    FItable(UBound(FItable)).terminator = ";"
  End If
    
  FItable(UBound(FItable)).target = keyword
  FItable(UBound(FItable)).isUnique = True ' la sostituzione vale solo per la prima occorrenza
  FItable(UBound(FItable)).infoModify = ""  ' la riga ricercata non deve essere modificata
  InitCopyIndex = UBound(FItable) ' mi serve in "insertPLIPtrVariables_OF"
    
  t = -1
  If Not ptype = "PLI" Then
    t = RT.find("COPY LNKDBRT", 1)
  End If
    
  If t = -1 Then
    'Template di Working:
    If EmbeddedRoutine Then
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\WorkingPgmDB"
    Else
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\InitCpyRoutIMSDB.tpl"
    End If
    
    If Len(Dir(templateName, vbNormal)) Then
      If Not dirincaps = "\PLI" Then  'CBL
        nfile = FreeFile
        lFile = FileLen(templateName)
        
        Open templateName For Binary As nfile
        'Testo = vbCrLf
        firstTime = True 'IRIS-DB
        While Loc(nfile) < lFile
          Line Input #nfile, sLine
          'Trascodifica le label
          If InStr(1, sLine, "#LABEL_AUTHOR#") Then
            sLine = Replace(sLine, "#LABEL_AUTHOR#", m_fun.LABEL_AUTHOR)
            Testo = Testo & vbCrLf & sLine
          ElseIf InStr(1, sLine, "#LABEL_INFO#") Then
            sLine = Replace(sLine, "#LABEL_INFO#", m_fun.LABEL_INFO)
            Testo = Testo & vbCrLf & sLine
          ElseIf InStr(1, sLine, "#LABEL_INCAPS#") Then
            sLine = Replace(sLine, "#LABEL_INCAPS#", m_fun.LABEL_INCAPS)
            Testo = Testo & vbCrLf & sLine
          ElseIf InStr(1, sLine, "<NAMEPGM>") Then 'IRIS-DB
            Set rsPtr = m_fun.Open_Recordset("select Nome from bs_Oggetti where " & _
                                   "idoggetto = " & pIdOggetto) 'IRIS-DB
            If Not rsPtr.EOF Then 'IRIS-DB
              sLine = Replace(sLine, "<NAMEPGM>", rsPtr!nome) 'IRIS-DB
              Testo = Testo & vbCrLf & sLine 'IRIS-DB
            End If 'IRIS-DB
          Else
          'IRIS-DB  Testo = Testo & LABEL_INFO & vbCrLf & sLine
            If firstTime Then
              Testo = Testo & LABEL_INFO & sLine
              firstTime = False
            Else
              Testo = Testo & LABEL_INFO & vbCrLf & sLine
            End If
          End If
        Wend
        Close nfile
      End If
    Else
      'SQ - E' sempre bello scrivere dentro i programmi generati!
      AggLog "[Object: " & pIdOggetto & "] TEMPLATE NOT FOUND: " & templateName, RTBErr
      Testo = vbCrLf
      Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
      Testo = Testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
      Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
    End If
      
    If ptype = "PLI" Then
      TestoREV = ""
      boladdtesto = insertPLIPtrVariables_OF(TestoREV, pIdOggetto, RT)
      Testo = Testo & TestoREV
      FItable(InitCopyIndex).lines_up = Testo & FItable(InitCopyIndex).lines_up
    Else
      FItable(InitCopyIndex).lines_down = Testo
    End If
'    RT.SelStart = w
'    RT.SelLength = 0
  End If
  s = Split(Testo, vbCrLf) 'IRIS-DB PDMC
  AddRighe = AddRighe + UBound(s) + 1 'IRIS-DB PDMC
  ' Sono gi� dentro la WORKING-STORAGE, quindi ne approfitto per inserire i FILE-STATUS
  If VsamBatch And EmbeddedRoutine Then
    Inizializza_File_Status_OF RT, RTBErr, ptype, pIdOggetto, w, "INCAPS"
  End If
End Sub

Public Sub Inizializza_File_Status_OF(RT As RichTextBox, RTBErr As RichTextBox, ptype As String, pIdOggetto As Long, w As Long, Tool As String)
  Dim rsIstr As Recordset, rsVSAM As Recordset
  Dim i As Long
  Dim templateName As String, sLine As String, idSegmenti() As String, s() As String
  Dim nfile As Integer, lFile As Integer
  
  Set rsIstr = m_fun.Open_Recordset( _
    "SELECT DISTINCT b.idsegmentid FROM PSDLI_Istruzioni as a,PSDLI_IstrDett_Liv as b where " & _
    "a.idpgm = " & pIdOggetto & " and a.valida and a.to_migrate and " & _
    "a.idpgm = b.idpgm and a.idoggetto = b.idoggetto and a.riga = b.riga")
  Do Until rsIstr.EOF
    If InStr(rsIstr!IdSegmentiD, ";") Then
      idSegmenti = Split("0" & rsIstr!IdSegmentiD, ";")
    Else
      ReDim idSegmenti(0)
      'SQ pezza: ci sono istruzioni valide con idSegmentiD=null che non dovrebbero esserci (c'� anche la GN sq di Pippo...)
      '-> len(idSegmentiD)
      idSegmenti(0) = "0" & rsIstr!IdSegmentiD
    End If
    For i = 0 To UBound(idSegmenti)
      If CLng(idSegmenti(i)) Then
        Set rsVSAM = m_fun.Open_Recordset("select nome from DMDB2_Tabelle where idorigine = " & idSegmenti(i))
        If rsVSAM.RecordCount Then
          templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\COPY_FILE_STATUS.cpy"
          If Len(Dir(templateName, vbNormal)) Then
            nfile = FreeFile
            lFile = FileLen(templateName)

            Open templateName For Binary As nfile
            Testo = vbCrLf
            While Loc(nfile) < lFile
              Line Input #nfile, sLine

              'Trascodifica le label
              If InStr(1, sLine, "<NOMEVSAM>") Then
                sLine = Replace(sLine, "<NOMEVSAM>", rsVSAM!nome)
                Testo = Testo & vbCrLf & START_TAG & Mid(sLine, Len(START_TAG) + 1)
              Else
                Testo = Testo & vbCrLf & START_TAG & Mid(sLine, Len(START_TAG) + 1)
              End If
            Wend
            Close nfile
          Else
            'SQ - E' sempre bello scrivere dentro i programmi generati!
            AggLog "[Object: " & pIdOggetto & "] TEMPLATE NOT FOUND: " & templateName, RTBErr
            Testo = vbCrLf
            Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
            Testo = Testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
            Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
          End If


'          RT.SelStart = w
'          RT.SelLength = 0
'
'          RT.SelText = Testo
'          s = Split(Testo, vbCrLf)
'          AddRighe = AddRighe + UBound(s)
        End If
        rsVSAM.Close
      End If
    Next i
    rsIstr.MoveNext
  Loop
  rsIstr.Close
End Sub

Function insertPLI_INC_PtrVariables_OF(Testo As String, poggetto As Long, RT As RichTextBox) As Boolean
  Dim rsNumPcb As Recordset, t As Long, w As Long, s() As String
  Dim lenAreaRED As Long, Pcbred As String, testosave As String, bolpcb As Boolean, bolSSA As Boolean
  Dim bolArea As Boolean, areaRed As String, ColPcbRed() As String, i As Integer
  Dim areaFeedRed As String
  Dim statement1 As String, statement2 As String
  Dim margine As Integer, indent As Integer
  'SQ [01]
  Dim ProcName() As String
  Dim dataType As String
  Dim ProcNameS As String, lines As String, namePgr As String
  'AC 19/07/08
  ' Versione per le INC, solo AREE e KEYVALUE (tolte dal programma chiamante)
  On Error GoTo err
  
  namePgr = namePgmFromId(poggetto)
  ReDim ColPcbRed(0)
  ReDim ArrProc(0)
  ReDim ArrProcLines(0)

  testosave = vbCrLf '& " /* " & START_TAG & " START              */"
  
  ' Mauro 04/08/2008
  Dim rsPtr As Recordset
  Set rsPtr = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & poggetto & " and pointer")
  If rsPtr.RecordCount Then
    statement1 = " DCL IRIS_PTR POINTER;"
    ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
    ColPcbRed(UBound(ColPcbRed)) = "IRIS_PTR"
    margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
    testosave = testosave & vbCrLf & _
                statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
  End If
  
  ' DATA AREA
  Set rsNumPcb = m_fun.Open_Recordset("select distinct trim(DataArea) as DataArea,IdOggetto from PsDli_IstrDett_Liv where  IdOggetto = " & poggetto)
  If Not rsNumPcb.EOF Then
    bolArea = True
  End If
  While Not rsNumPcb.EOF
    statement1 = ""
    statement2 = ""
    If Not IsNull(rsNumPcb!DataArea) And Len(rsNumPcb!DataArea) Then
      areaRed = findReadd2(rsNumPcb!DataArea, poggetto, rsNumPcb!IdOggetto, lenAreaRED)
      If Not areaRed = "" Then
        For i = 1 To UBound(ColPcbRed)
         If ColPcbRed(i) = Trim(areaRed) Then
          Exit For
         End If
        Next
        If i = UBound(ColPcbRed) + 1 Then
          'testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(areaRed) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & areaRed & "));"
          'AC 18/02/09
          ProcName = GetProcName(poggetto, rsNumPcb!DataArea)
          For i = 1 To UBound(ProcName)
            CheckProc " DCL IRIS_" & Trim(areaRed) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & areaRed & "));", ProcName(i)
          Next
          ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
          ColPcbRed(UBound(ColPcbRed)) = Trim(areaRed)
        End If
      Else
        'SQ [01]
        'lenAreaRED = getAreaLen(rsNumPcb!DataArea, rsNumPcb!idOggetto)
        getAreaInfo rsNumPcb!DataArea, rsNumPcb!IdOggetto, lenAreaRED, dataType, ProcNameS
        ' Mauro 01/08/2008
        If InStr(rsNumPcb!DataArea, "(") Then
          Dim DataArea As String, DataAreaAdd As String
          Dim rsOccurs As Recordset
          DataAreaAdd = Trim(Left(rsNumPcb!DataArea, InStr(rsNumPcb!DataArea, "(") - 1))
          DataArea = Trim(Replace(Left(rsNumPcb!DataArea, InStr(rsNumPcb!DataArea, "(") - 1), ".", "_"))
          For i = 1 To UBound(ColPcbRed)
            If ColPcbRed(i) = DataArea Then
                Exit For
            End If
          Next
          If i = UBound(ColPcbRed) + 1 Then
            Set rsOccurs = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & rsNumPcb!IdOggetto & " and nomecmp = '" & DataArea & "'")
            If rsOccurs.RecordCount = 0 Then
              statement1 = " DCL IRIS_" & DataArea & "(" & getAreaoccurs(DataArea, rsNumPcb!IdOggetto) & ") CHAR(" & lenAreaRED & ")"
              'statement2 = " BASED(ADDR(" & DataArea & "));"
              statement2 = " BASED(ADDR(" & DataAreaAdd & "));"
            Else
              If rsOccurs!pointer Then
                statement1 = " DCL IRIS_" & DataArea & " CHAR(" & lenAreaRED & ")"
                statement2 = " BASED(IRIS_PTR);"
              End If
            End If
            rsOccurs.Close
          End If
        Else
          'SQ [02]
          If dataType <> "X" Then
            For i = 1 To UBound(ColPcbRed)
                If ColPcbRed(i) = Trim(rsNumPcb!DataArea) Then
                    Exit For
                End If
            Next
            If i = UBound(ColPcbRed) + 1 Then
                statement1 = " DCL IRIS_" & rsNumPcb!DataArea & " CHAR(" & lenAreaRED & ")"
                statement2 = " BASED(ADDR(" & rsNumPcb!DataArea & "));"
            End If
          End If
        End If
        If Len(statement1) > 0 And Len(statement2) > 0 Then
          margine = 72 - (Len(statement1 & statement2) + Len("/*" & Trim(START_TAG) & "*/"))
          If margine > 0 Then
            '1 riga:
            lines = statement1 & statement2 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
          Else
            '2 righe:
            indent = 3 'costante
            margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
            lines = statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
            margine = 72 - (Len(statement2) + Len("/*" & Trim(START_TAG) & "*/"))
            lines = lines & vbCrLf & _
                        Space(indent) & statement2 & Space(margine - indent) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
          End If
        End If
        CheckProc lines, ProcNameS
        ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
        'AC 25/02/10 in caso di array deve mettere nella struttura
        'ColPcbRed(UBound(ColPcbRed)) = rsNumPcb!DataArea
        If InStr(rsNumPcb!DataArea, "(") Then
            ColPcbRed(UBound(ColPcbRed)) = Trim(Replace(Left(rsNumPcb!DataArea, InStr(rsNumPcb!DataArea, "(") - 1), ".", "_"))
        Else
            ColPcbRed(UBound(ColPcbRed)) = rsNumPcb!DataArea
        End If
      End If
      
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
  'KEY
  Set rsNumPcb = m_fun.Open_Recordset("select distinct trim(Valore) as valore,IdOggetto from PsDli_IstrDett_Key where IdOggetto = " & poggetto)
  While Not rsNumPcb.EOF
    lines = ""
    statement1 = ""
    statement2 = ""
    If Not (rsNumPcb!Valore = "" Or Left(Trim(rsNumPcb!Valore), 1) = "'") Then
      'SQ [01]
      'lenAreaRED = getAreaLen(rsNumPcb!Valore, rsNumPcb!idOggetto)
      getAreaInfo rsNumPcb!Valore, rsNumPcb!IdOggetto, lenAreaRED, dataType, ProcNameS
      If InStr(rsNumPcb!Valore, "(") Then
        Dim Valore As String, ValoreAdd As String
        ValoreAdd = Trim(Left(rsNumPcb!Valore, InStr(rsNumPcb!Valore, "(") - 1))
        Valore = Trim(Replace(Left(rsNumPcb!Valore, InStr(rsNumPcb!Valore, "(") - 1), ".", "_"))
        Set rsOccurs = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & rsNumPcb!IdOggetto & " and nomecmp = '" & Valore & "'")
        If rsOccurs.RecordCount = 0 Then
          statement1 = " DCL IRIS_" & Valore & "(" & getAreaoccurs(Valore, rsNumPcb!IdOggetto) & ") CHAR(" & lenAreaRED & ")"
          'statement2 = " BASED(ADDR(" & Valore & "));"
          statement2 = " BASED(ADDR(" & ValoreAdd & "));"
        Else
          If rsOccurs!pointer Then
            statement1 = " DCL IRIS_" & Valore & " CHAR(" & lenAreaRED & ")"
            statement2 = " BASED(IRIS_PTR);"
          End If
        End If
        rsOccurs.Close
      Else
        'SQ [02]
        If dataType <> "X" Then
          statement1 = " DCL IRIS_" & Trim(Replace(rsNumPcb!Valore, ".", "_")) & " CHAR(" & lenAreaRED & ")"
          'statement2 = " BASED(ADDR(" & Trim(Replace(rsNumPcb!Valore, ".", "_")) & "));"
          'AC 06/04/09  tolto il replace . _ per il based address
          statement2 = " BASED(ADDR(" & rsNumPcb!Valore & "));"
        End If
      End If
      
      If Len(statement1) > 0 And Len(statement2) > 0 Then
        'FUNZ:
        margine = 72 - (Len(statement1 & statement2) + Len("/*" & Trim(START_TAG) & "*/"))
        If margine > 0 Then
          '1 riga:
          lines = statement1 & statement2 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
        Else
            '2 righe:
            indent = 3 'costante
            margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
            lines = statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
            margine = 72 - (Len(statement2) + Len("/*" & Trim(START_TAG) & "*/"))
            lines = lines & vbCrLf & _
                        Space(indent) & statement2 & Space(margine - indent) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
        End If
      End If
      'AC
      If Len(lines) Then
        CheckProc lines, ProcNameS
      End If
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
  'AC  query per IdOggetto = poggetto
  Set rsNumPcb = m_fun.Open_Recordset("select riga from PsDli_Istruzioni where IdOggetto = " & poggetto & " and  to_migrate = true and obsolete = false and Valida = true")
  If Not rsNumPcb.EOF Then
    insertPLI_INC_PtrVariables_OF = True
  Else
    insertPLI_INC_PtrVariables_OF = False
  End If
  rsNumPcb.Close
 
  
  'testosave = testosave & vbCrLf & "<externalentry>"
  'testosave = testosave & vbCrLf '& " /* " & START_TAG & " END                */"

  If Not bolpcb And Not bolSSA And Not bolArea Then
    testosave = ""
  End If
  
  'Ac 21/05/08
  testosave = Replace(testosave, "IRIS_$", "IRIS_")
  
  Testo = Testo & testosave
  Dim Arr() As String
  While (Left(Testo, 2) = vbCrLf)
    Testo = Right(Testo, Len(Testo) - 2)
  Wend
  'AC OF
  AddFIrecordTypeProc namePgr
  Exit Function
err:
  'SQ
  AggLog "[Object: " & poggetto & "] VARIABLES INSERT ERROR", MadrdF_Incapsulatore.RTErr
End Function

Function LenOrStg(lenArea As Long, Valore As String, pIdOggetto As Long) As String
  If lenArea > 0 Then
    LenOrStg = CStr(lenArea)
  Else 'TODO parametrizzare STG  -- potrebbe essere STORAGE
    LenOrStg = "STG(" & Valore & ")"
    If Not pIdOggetto = 0 Then
      AggLog "[Object: " & pIdOggetto & "]" & "STORAGE function used for " & Valore & " variable", MadrdF_Incapsulatore.RTErr
    End If
  End If
End Function

Function insertPLIPtrVariables_OF(Testo As String, poggetto As Long, RT As RichTextBox) As Boolean
  Dim rsNumPcb As Recordset
  Dim lenAreaRED As Long, Pcbred As String, testosave As String, bolpcb As Boolean, bolSSA As Boolean
  Dim bolArea As Boolean, areaRed As String, ColPcbRed() As String, i As Integer
  Dim statement1 As String, statement2 As String
  Dim margine As Integer, indent As Integer, templateName As String
  Dim nfile As Long, nfile2 As Long, nfile3 As Long
  Dim lFile As Long, sLine As String
  Dim ProcName() As String, ProcNameS As String, lines As String, namePgr As String
  Dim TemplateNameAppo As String, sLinePCBLIST As String, sLineAppo As String
  Dim ArrayPCB() As String, TestoPCBList As String, k As Integer
  Dim isPCBAREAto_manage As Boolean, pcbareas As String, sLinePCBAREA As String, testoAREAPCB As String
  Dim rsPtr As Recordset, rsTmp As Recordset
  Dim procToCheck As String, IsRevptrInserted As Boolean
  Dim dataType As String
  Dim DataArea As String, DataAreaAdd As String
  Dim rsOccurs As Recordset
  Dim Valore As String, ValoreAdd As String
  Dim isTrovato As Boolean, arrVar() As String, a As Integer
  
  On Error GoTo err

  ReDim ArrayPCB(0)
  ReDim arrVar(0)

  Set rsPtr = m_fun.Open_Recordset("select main, parameters from PsPgm where idoggetto = " & poggetto)
  If Not rsPtr.EOF Then
    If rsPtr!Main Then
      isPCBAREAto_manage = True
      pcbareas = rsPtr!parameters
      While Not pcbareas = ""
        ReDim Preserve ArrayPCB(UBound(ArrayPCB) + 1)
        If InStr(pcbareas, ",") > 0 Then
          ArrayPCB(UBound(ArrayPCB)) = Left(pcbareas, InStr(pcbareas, ",") - 1)
          pcbareas = Right(pcbareas, Len(pcbareas) - InStr(pcbareas, ","))
        Else
          ArrayPCB(UBound(ArrayPCB)) = pcbareas
          pcbareas = ""
        End If
      Wend
    End If
  End If
  rsPtr.Close
  
  Set rsPtr = m_fun.Open_Recordset("select idoggetto from psdli_istruzioni where " & _
                                   "idoggetto = " & poggetto & " and istruzione = 'PCB'")
  If Not rsPtr.EOF Then
    isPCBAREAto_manage = True
    For i = 1 To 9
      ReDim Preserve ArrayPCB(UBound(ArrayPCB) + 1)
      ArrayPCB(UBound(ArrayPCB)) = "IRIS_PCB_A(" & i & ")"
    Next i
  End If
  rsPtr.Close
    
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps\PLI\InitCpyRoutIMSDB.tpl"
  If Len(Dir(templateName, vbNormal)) Then
    nfile = FreeFile
    lFile = FileLen(templateName)
    Open templateName For Binary As nfile
    Testo = vbCrLf
    While Loc(nfile) < lFile
      Line Input #nfile, sLine
      '**** nuovo template PCB_AREA_ASS
      If InStr(sLine, "<<PCB_AREA_ASS>>") Then
        If isPCBAREAto_manage Then
          TemplateNameAppo = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\PCB_AREA_ASS.tpl"
          nfile2 = FreeFile
          Open TemplateNameAppo For Binary As nfile2
          If Not EOF(nfile2) Then
            While Not EOF(nfile2)
              Line Input #nfile2, sLinePCBAREA
              If InStr(sLinePCBAREA, "<NUMPCBENTRIES>") Then
                sLinePCBAREA = Replace(sLinePCBAREA, "<NUMPCBENTRIES>", Format(UBound(ArrayPCB), "00"))
                testoAREAPCB = testoAREAPCB & vbCrLf & sLinePCBAREA
              ElseIf InStr(sLinePCBAREA, "<<PCBLIST>>") Then
                TemplateNameAppo = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\PCBLIST.tpl"
                nfile3 = FreeFile
                Open TemplateNameAppo For Binary As nfile3
                If Not EOF(nfile3) Then
                  Line Input #nfile3, sLinePCBLIST
                  For k = 1 To UBound(ArrayPCB)
                    sLineAppo = Replace(sLinePCBLIST, "<PCBNAME>", ArrayPCB(k))
                    sLineAppo = Replace(sLineAppo, "<PCBINDEX>", k)
                    sLineAppo = Replace(sLineAppo, "<SPACES_A>", Space(27 - Len(ArrayPCB(k)) - IIf(k < 10, 1, 4)))
                    TestoPCBList = TestoPCBList & vbCrLf & sLineAppo
                  Next k
                  testoAREAPCB = testoAREAPCB & TestoPCBList
                End If
                Close nfile3
              ElseIf InStr(sLinePCBAREA, "<NEW_PCB>") Then
                Set rsTmp = m_fun.Open_Recordset("select idoggetto from psdli_istruzioni where " & _
                                                 "idoggetto = " & poggetto & " and istruzione = 'PCB'")
                If Not rsTmp.EOF Then
                  sLinePCBAREA = Replace(sLinePCBAREA, "<NEW_PCB>", "")
                  testoAREAPCB = testoAREAPCB & vbCrLf & _
                                 sLinePCBAREA
                End If
                rsTmp.Close
              Else
                testoAREAPCB = testoAREAPCB & vbCrLf & _
                               sLinePCBAREA
              End If
            Wend
          End If
          Close nfile2
          Testo = Testo & testoAREAPCB
        End If
        '****
      ElseIf InStr(sLine, "<<PCB_AREA_EXEC>>") Then
        Set rsPtr = m_fun.Open_Recordset("select idoggetto from PsDli_Istruzioni where " & _
                                         "idoggetto = " & poggetto & " And isExec = True")
        If Not rsPtr.EOF Then
          TemplateNameAppo = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\PCB_AREA_EXEC.tpl"
          nfile2 = FreeFile
          Open TemplateNameAppo For Binary As nfile2
          Do Until EOF(nfile2)
            Line Input #nfile2, sLinePCBAREA
            testoAREAPCB = testoAREAPCB & vbCrLf & sLinePCBAREA
          Loop
        End If
        rsPtr.Close
        Close nfile2
        Testo = Testo & testoAREAPCB
      ElseIf InStr(sLine, "<<FLGDB2I>>") Then
        Set rsTmp = m_fun.Open_Recordset("select idoggetto from Include_Incaps where idoggetto = " & poggetto)
        If Not rsTmp.EOF Then
          TemplateNameAppo = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\FLGDB2I.tpl"
          nfile2 = FreeFile
          Open TemplateNameAppo For Binary As nfile2
          Do Until EOF(nfile2)
            Line Input #nfile2, sLinePCBAREA
            Testo = Testo & vbCrLf & sLinePCBAREA
          Loop
          Close nfile2
        End If
        rsTmp.Close
      ElseIf InStr(sLine, "<STATIC>") Then
        Set rsTmp = m_fun.Open_Recordset("select static from pspgm where idoggetto = " & poggetto & " and static = true")
        If Not rsTmp.EOF Then
          sLine = Replace(sLine, "<STATIC>", " STATIC EXTERNAL,")
        Else
          sLine = Replace(sLine, "<STATIC>", "," & Space(16))
        End If
        rsTmp.Close
        Testo = Testo & vbCrLf & sLine
      ElseIf InStr(sLine, "<INIT_STATIC>") Then
        Set rsTmp = m_fun.Open_Recordset("select static from pspgm where idoggetto = " & poggetto & " and static = true")
        If rsTmp.EOF Then
          sLine = Replace(sLine, "<INIT_STATIC>", "")
          Testo = Testo & vbCrLf & sLine
        End If
        rsTmp.Close
      ElseIf InStr(sLine, "<NAMEPGM>") Then 'IRIS-DB
        Set rsTmp = m_fun.Open_Recordset("select Nome from bs_Oggetti where idoggetto = " & poggetto) 'IRIS-DB
        If Not rsTmp.EOF Then 'IRIS-DB
          sLine = Replace(sLine, "<NAMEPGM>", rsTmp!nome) 'IRIS-DB
          Testo = Testo & vbCrLf & sLine 'IRIS-DB
        End If 'IRIS-DB
        rsTmp.Close
      Else
        Testo = Testo & LABEL_INFO & vbCrLf & sLine
      End If
    Wend
    Close nfile
  Else
    'SQ - E' sempre bello scrivere dentro i programmi generati!
    'AggLog "[Object: " & poggetto & "] TEMPLATE NOT FOUND: " & templatename, RTBErr
    Testo = vbCrLf
    Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
    Testo = Testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
    Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
  End If

  namePgr = namePgmFromId(poggetto)
  
  ReDim ColPcbRed(0)
  ReDim ArrProc(0)
  ReDim ArrProcLines(0)

  testosave = vbCrLf '& " /* " & START_TAG & " START              */"
  
  Set rsPtr = m_fun.Open_Recordset("select idoggetto from MgData_Cmp_DCL where idoggetto = " & poggetto & " and pointer")
  If rsPtr.RecordCount Then
    statement1 = " DCL IRIS_PTR POINTER;"
    ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
    ColPcbRed(UBound(ColPcbRed)) = "IRIS_PTR"
    margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
    testosave = testosave & vbCrLf & _
                statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
    IsRevptrInserted = True
  End If
  'SQ
  rsPtr.Close
  'AC 12/10/10 gestione io-areee puntatori
  Set rsPtr = m_fun.Open_Recordset("select a.DataArea from PsDli_IstrDett_Liv as a, PsData_Cmp as b where " & _
                                   "a.IdPgm = " & poggetto & " and a.IdOggetto = " & poggetto & _
                                   " and b.IdOggetto = " & poggetto & " and b.Nome = a.DataArea and b.[Usage] = 'PTR'")
  If Not rsPtr.EOF Then
    statement1 = " DCL IRIS_IO_AREA  CHAR(1000) BASED(IRIS_PTR);"
    ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
    ColPcbRed(UBound(ColPcbRed)) = "IRIS_IO"
    margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
    testosave = testosave & vbCrLf & _
                statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
    If Not IsRevptrInserted Then
      statement1 = " DCL IRIS_PTR POINTER;"
      ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
      ColPcbRed(UBound(ColPcbRed)) = "IRIS_PTR"
      margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
      testosave = testosave & vbCrLf & _
                  statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
    End If
  End If
  rsPtr.Close
  
  'SQ tutti 'sti blocchi sotto, non sono tutti uguali? Non possono essere "unificati"?
  
  'NUMPCB  -------------->  FITable con section
  Set rsNumPcb = m_fun.Open_Recordset("select main from PsPgm where IdOggetto = " & poggetto)
  If Not rsNumPcb.EOF Then
    If Not rsNumPcb!Main Then
      rsNumPcb.Close
      Set rsNumPcb = m_fun.Open_Recordset("select distinct NUMPcb from PsDli_Istruzioni where IdOggetto = " & poggetto & " and  to_migrate = true and obsolete = false")
      bolpcb = Not rsNumPcb.EOF
      While Not rsNumPcb.EOF
        If Not IsNull(rsNumPcb!numpcb) Then
          If Not rsNumPcb!numpcb = "" Then
            'leanAreaRED variabile da valorizzare
            Pcbred = getPCDRedFromPCB_OF(rsNumPcb!numpcb, poggetto, ProcNameS, lenAreaRED)
            If Not Pcbred = "" Then
              For i = 1 To UBound(ColPcbRed)
                If ColPcbRed(i) = Trim(Pcbred) Then Exit For
              Next i
              If i = UBound(ColPcbRed) + 1 Then
                'testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(Pcbred) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & Pcbred & "));        /*" & Trim(START_TAG) & "*/"
                'AC nuovo record in FITable
                CheckProc " DCL IRIS_" & Trim(Pcbred) & " CHAR(" & LenOrStg(lenAreaRED, Pcbred, poggetto) & ") BASED(ADDR(" & Pcbred & "));        /*" & Trim(START_TAG) & "*/", ProcNameS
                ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
                ColPcbRed(UBound(ColPcbRed)) = Trim(Pcbred)
              End If
            End If
          End If
        End If
        rsNumPcb.MoveNext
      Wend
    End If
  End If
  rsNumPcb.Close
  
  ' DATA AREA
  'AC 09/01/08 query per IdPgm = poggetto (considero anche istruzioni in copy)
  'Set rsNumPcb = m_fun.Open_Recordset("select distinct DataArea,IdOggetto from PsDli_IstrDett_Liv where IdPgm = " & poggetto)
  'AC 18/07/08 considero solo quelle del programma: IdPgm = poggetto IdOggetto = poggetto
  Set rsNumPcb = m_fun.Open_Recordset("select distinct trim(DataArea) as DataArea,IdOggetto from PsDli_IstrDett_Liv where IdPgm = " & poggetto & " and IdOggetto = " & poggetto)
  bolArea = Not rsNumPcb.EOF
  While Not rsNumPcb.EOF
    lines = ""
    statement1 = ""
    statement2 = ""
    'SQ: condizioni da portare nella WHERE CONDITION!
    If Not IsNull(rsNumPcb!DataArea) And Len(rsNumPcb!DataArea) And Not InStr(rsNumPcb!DataArea, "'") > 0 Then
      areaRed = findReadd2(rsNumPcb!DataArea, poggetto, rsNumPcb!IdOggetto, lenAreaRED)
      If Not areaRed = "" Then
        For i = 1 To UBound(ColPcbRed)
          If ColPcbRed(i) = Trim(areaRed) Then
            Exit For
          End If
        Next i
        If i = UBound(ColPcbRed) + 1 Then
          'testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(areaRed) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & areaRed & "));"
          'AC 18/02/09
          ProcName = GetProcName(poggetto, rsNumPcb!DataArea)
          For i = 1 To UBound(ProcName)
            CheckProc " DCL IRIS_" & Trim(areaRed) & " CHAR(" & LenOrStg(lenAreaRED, areaRed, poggetto) & ") BASED(ADDR(" & areaRed & "));", ProcName(i)
          Next i
          ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
          ColPcbRed(UBound(ColPcbRed)) = Trim(areaRed)
        End If
      Else
        'SQ [01] (ottiene: byte, type, procedure)
        'lenAreaRED = getAreaLen(rsNumPcb!DataArea, rsNumPcb!idOggetto)
        getAreaInfo rsNumPcb!DataArea, rsNumPcb!IdOggetto, lenAreaRED, dataType, ProcNameS
        '''''''''''''''''''''''''''''''''
        ' Mauro 01/08/2008
        If InStr(rsNumPcb!DataArea, "(") Then
          DataAreaAdd = Trim(Left(rsNumPcb!DataArea, InStr(rsNumPcb!DataArea, "(") - 1))
          DataArea = Trim(Replace(Left(rsNumPcb!DataArea, InStr(rsNumPcb!DataArea, "(") - 1), ".", "_"))
          For i = 1 To UBound(ColPcbRed)
            If ColPcbRed(i) = DataArea Then
              Exit For
            End If
          Next i
          If i = UBound(ColPcbRed) + 1 Then
            Set rsOccurs = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & rsNumPcb!IdOggetto & " and nomecmp = '" & DataArea & "'")
            If rsOccurs.RecordCount = 0 Then
              statement1 = " DCL IRIS_" & DataArea & "(" & getAreaoccurs(DataArea, rsNumPcb!IdOggetto) & ") CHAR(" & LenOrStg(lenAreaRED, DataAreaAdd, poggetto) & ")"
              'statement2 = " BASED(ADDR(" & DataArea & "));"
              statement2 = " BASED(ADDR(" & DataAreaAdd & "));"
            Else
              If rsOccurs!pointer Then
                statement1 = " DCL IRIS_" & DataArea & " CHAR(" & LenOrStg(lenAreaRED, DataArea, poggetto) & ")"
                statement2 = " BASED(IRIS_PTR);"
              '*****************
              'AC 04/10/2010
              Else ' verificare (siamo nel caso di campo occursato senza pointer)
                statement1 = " DCL IRIS_" & DataArea & "(" & getAreaoccurs(DataArea, rsNumPcb!IdOggetto) & ") CHAR(" & LenOrStg(lenAreaRED, DataAreaAdd, poggetto) & ")"
                statement2 = " BASED(ADDR(" & DataAreaAdd & "));"
              '******************
              End If
            End If
            rsOccurs.Close
          End If
        Else
          'SQ [02] (I campi "X" non li aggiungiamo!)
          If dataType <> "X" Then
            For i = 1 To UBound(ColPcbRed)
              If ColPcbRed(i) = Trim(rsNumPcb!DataArea) Then
                Exit For
              End If
            Next i
            If i = UBound(ColPcbRed) + 1 Then
              DataArea = Trim(Replace(rsNumPcb!DataArea, ".", "_"))
              'IRIS-DB data area not needed
              'statement1 = " DCL IRIS_" & DataArea & " CHAR(" & LenOrStg(lenAreaRED, rsNumPcb!DataArea, poggetto) & ")"
              'statement2 = " BASED(ADDR(" & rsNumPcb!DataArea & "));"
            End If
          End If
        End If
        If Len(statement1) > 0 And Len(statement2) > 0 Then
          margine = 72 - (Len(statement1 & statement2) + Len("/*" & Trim(START_TAG) & "*/"))
          If margine > 0 Then
            '1 riga:
            lines = statement1 & statement2 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
          Else
            '2 righe:
            indent = 3 'costante
            margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
            lines = statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
            margine = 72 - (Len(statement2) + Len("/*" & Trim(START_TAG) & "*/"))
            lines = lines & vbCrLf & _
                    Space(indent) & statement2 & Space(margine - indent) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
          End If
        End If
        CheckProc lines, ProcNameS
        ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
        'AC 25/02/10 in caso di array deve mettere nella struttura
        'ColPcbRed(UBound(ColPcbRed)) = rsNumPcb!DataArea
        If InStr(rsNumPcb!DataArea, "(") Then
          ColPcbRed(UBound(ColPcbRed)) = Trim(Replace(Left(rsNumPcb!DataArea, InStr(rsNumPcb!DataArea, "(") - 1), ".", "_"))
        Else
          ColPcbRed(UBound(ColPcbRed)) = rsNumPcb!DataArea
        End If
      End If
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
  'KEYFEEDBACK
  Set rsNumPcb = m_fun.Open_Recordset("select distinct Keyfeedback,IdOggetto from PsDli_Istruzioni where IdPgm = " & poggetto)
  While Not rsNumPcb.EOF
    If Not IsNull(rsNumPcb!keyfeedback) Then
      If Not rsNumPcb!keyfeedback = "" Then
        lenAreaRED = getAreaLen(rsNumPcb!keyfeedback, rsNumPcb!IdOggetto)
        ProcName = GetProcName(rsNumPcb!IdOggetto, rsNumPcb!keyfeedback)
        For i = 1 To UBound(ColPcbRed)
          If ColPcbRed(i) = Trim(rsNumPcb!keyfeedback) Then
            Exit For
          End If
        Next i
        If i = UBound(ColPcbRed) + 1 Then
          'testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(rsNumPcb!keyfeedback) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & rsNumPcb!keyfeedback & "));" _
                                & Space(27 - 2 * Len(Trim(rsNumPcb!keyfeedback)) - Len(CStr(lenAreaRED))) & "/*" & Trim(START_TAG) & "*/"
          'AC 18/02/10
          For i = 1 To UBound(ProcName)
            getAreaInfo rsNumPcb!keyfeedback, poggetto, lenAreaRED, dataType, ProcName(i)
            If dataType <> "X" Then
              procToCheck = " DCL IRIS_" & Trim(rsNumPcb!keyfeedback) & " CHAR(" & LenOrStg(lenAreaRED, rsNumPcb!keyfeedback, poggetto) & ") BASED(ADDR(" & rsNumPcb!keyfeedback & "));"
              If 27 >= 2 * Len(Trim(rsNumPcb!keyfeedback)) + Len(LenOrStg(lenAreaRED, rsNumPcb!keyfeedback, 0)) Then
                procToCheck = procToCheck & Space(27 - 2 * Len(Trim(rsNumPcb!keyfeedback)) - Len(LenOrStg(lenAreaRED, rsNumPcb!keyfeedback, 0))) & "/*" & Trim(START_TAG) & "*/"
              Else
                procToCheck = procToCheck & "/*" & Trim(START_TAG) & "*/"
              End If
              CheckProc procToCheck, ProcName(i)
            End If
          Next i
          ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
          ColPcbRed(UBound(ColPcbRed)) = Trim(rsNumPcb!keyfeedback)
        End If
      End If
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
'  ' SSA
  Set rsNumPcb = m_fun.Open_Recordset("select distinct trim(NomeSSA) as NomeSSA from PsDli_IstrDett_Liv where IdOggetto = " & poggetto)
  bolSSA = Not rsNumPcb.EOF
  While Not rsNumPcb.EOF
    If Not (rsNumPcb!NomeSSA = "" Or Left(rsNumPcb!NomeSSA, 1) = "'") Then
      'IRIS-DB: provo, tanto le istruzioni in copy adesso le esplodiamo
      'lenAreaRED = getAreaLen(rsNumPcb!NomeSSA, poggetto)
      lenAreaRED = getAreaLen(rsNumPcb!NomeSSA, poggetto, poggetto)
      If Not InStr(rsNumPcb!NomeSSA, "!!") > 0 Then
        ProcName = GetProcName(poggetto, rsNumPcb!NomeSSA)
        'AC 18/02/10
        For i = 1 To UBound(ProcName)
          getAreaInfo rsNumPcb!NomeSSA, poggetto, lenAreaRED, dataType, ProcName(i)
          If dataType <> "X" Then
            'IRIS-DB it is necessary for PLI SUBSTR usage and arrays
            procToCheck = " DCL IRIS_" & Trim(rsNumPcb!NomeSSA) & " CHAR(" & LenOrStg(lenAreaRED, rsNumPcb!NomeSSA, poggetto) & ") BASED(ADDR(" & rsNumPcb!NomeSSA & "));"
            'If 33 >= Len(LenOrStg(lenAreaRED, rsNumPcb!NomeSSA, 0)) + 2 * Len(rsNumPcb!NomeSSA) Then
              'procToCheck = procToCheck & Space(33 - Len(LenOrStg(lenAreaRED, rsNumPcb!NomeSSA, 0)) - 2 * Len(rsNumPcb!NomeSSA)) & "/*" & Trim(START_TAG) & "*/"
            'Else
              'procToCheck = procToCheck & "/*" & Trim(START_TAG) & "*/"
            'End If
            CheckProc procToCheck, ProcName(i)
          End If
        Next i
      End If
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
 
  ' AC 10/12/2007  KEYVALUE
  'AC 09/01/08 query per IdPgm = poggetto (considero anche istruzioni in copy)
  'Set rsNumPcb = m_fun.Open_Recordset("select distinct Valore,IdOggetto from PsDli_IstrDett_Key where IdPgm = " & poggetto)
  'AC 18/07/08 considero solo quelle del programma: IdPgm = poggetto IdOggetto = poggetto
  Set rsNumPcb = m_fun.Open_Recordset("select distinct trim(Valore) as valore,IdOggetto from PsDli_IstrDett_Key where IdPgm = " & poggetto & " and IdOggetto = " & poggetto)
  While Not rsNumPcb.EOF
    lines = ""
    statement1 = ""
    statement2 = ""
    If Not (rsNumPcb!Valore = "" Or Left(Trim(rsNumPcb!Valore), 1) = "'") Then
      'lenAreaRED = getAreaLen(rsNumPcb!Valore, rsNumPcb!idOggetto)
      getAreaInfo rsNumPcb!Valore, rsNumPcb!IdOggetto, lenAreaRED, dataType, ProcNameS
      
      ''''''''''''''''''''''''''''''''''''''''
      'SQ TMP!!!!!!!!!!!!!!!!!!!! ERA 27...
      'fare funzioncina generica/comune (vedi writeLine o qualcosa di simile nel COBOL)!
      ''''''''''''''''''''''''''''''''''''''''
      'testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(rsNumPcb!Valore) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & Trim(rsNumPcb!Valore) & "));" _
      '            & Space(27 - 2 * Len(Trim(rsNumPcb!Valore)) - Len(CStr(lenAreaRED))) & "/*" & Trim(START_TAG) & "*/"
      If InStr(rsNumPcb!Valore, "(") Then
        ValoreAdd = Trim(Left(rsNumPcb!Valore, InStr(rsNumPcb!Valore, "(") - 1))
        Valore = Trim(Replace(Left(rsNumPcb!Valore, InStr(rsNumPcb!Valore, "(") - 1), ".", "_"))
        isTrovato = False
        For a = 1 To UBound(arrVar)
          If arrVar(a) = Valore Then
            isTrovato = True
            Exit For
          End If
        Next a
        If Not isTrovato Then
          ReDim Preserve arrVar(UBound(arrVar) + 1)
          arrVar(UBound(arrVar)) = Valore
          Set rsOccurs = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & rsNumPcb!IdOggetto & " and nomecmp = '" & Valore & "'")
          If rsOccurs.RecordCount = 0 Then
            statement1 = " DCL IRIS_" & Valore & "(" & getAreaoccurs(Valore, rsNumPcb!IdOggetto) & ") CHAR(" & LenOrStg(lenAreaRED, ValoreAdd, poggetto) & ")"
            'statement2 = " BASED(ADDR(" & Valore & "));"
            statement2 = " BASED(ADDR(" & ValoreAdd & "));"
          Else
            If rsOccurs!pointer Then
              statement1 = " DCL IRIS_" & Valore & " CHAR(" & lenAreaRED & ")"
              statement2 = " BASED(IRIS_PTR);"
            '*****************
            'AC 04/10/2010
            Else ' verificare (siamo nel caso di campo occursato senza pointer)
              statement1 = " DCL IRIS_" & Valore & "(" & getAreaoccurs(Valore, rsNumPcb!IdOggetto) & ") CHAR(" & LenOrStg(lenAreaRED, ValoreAdd, poggetto) & ")"
              statement2 = " BASED(ADDR(" & ValoreAdd & "));"
            '******************
            End If
          End If
          rsOccurs.Close
        End If
      Else
        'SQ [02] (I campi "X" non li aggiungiamo!)
        If dataType <> "X" Then
          statement1 = " DCL IRIS_" & Trim(Replace(rsNumPcb!Valore, ".", "_")) & " CHAR(" & LenOrStg(lenAreaRED, rsNumPcb!Valore, poggetto) & ")"
          statement2 = " BASED(ADDR(" & rsNumPcb!Valore & "));"
        End If
      End If
      If Len(statement1) > 0 And Len(statement2) > 0 Then
        'FUNZ:
        margine = 72 - (Len(statement1 & statement2) + Len("/*" & Trim(START_TAG) & "*/"))
        If margine > 0 Then
          '1 riga:
          lines = statement1 & statement2 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
        Else
          '2 righe:
          indent = 3 'costante
          margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
          lines = statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
          margine = 72 - (Len(statement2) + Len("/*" & Trim(START_TAG) & "*/"))
          lines = lines & vbCrLf & _
                  Space(indent) & statement2 & Space(margine - indent) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
        End If
      End If
      'AC
      If Len(lines) Then
        CheckProc lines, ProcNameS
      End If
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
  'AC 09/01/08 query per IdPgm = poggetto (considero anche istruzioni in copy)
  Set rsNumPcb = m_fun.Open_Recordset("select riga from PsDli_Istruzioni where IdPgm = " & poggetto & " and to_migrate = true and obsolete = false and Valida = true")
  insertPLIPtrVariables_OF = Not rsNumPcb.EOF
  rsNumPcb.Close
  
  'AC 08/10/08 il testosave per Reale Mutua non pu� essere svuotato,
  'ci serve il tag <externalentry> da riempire con " DCL SE0R001 ENTRY OPTIONS(COBOL);"
  If Not bolpcb And Not bolSSA And Not bolArea And Not Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
    testosave = ""  'SQ: chiarire
  End If
  
  'Ac 21/05/08
  testosave = Replace(testosave, "IRIS_$", "IRIS_")
  
  Testo = Testo & testosave
  While (Left(Testo, 2) = vbCrLf)
    Testo = Right(Testo, Len(Testo) - 2)
  Wend
  
  'AC OF
  AddFIrecordTypeProc namePgr
  Exit Function
err:
  'SQ
  AggLog "[Object: " & poggetto & "] VARIABLES INSERT ERROR", MadrdF_Incapsulatore.RTErr
  Resume Next
End Function

Public Function AsteriscaIstruzioneOF_PLI(rst As Recordset, lineInstr As String, lineprevinstr As String, typeIncaps As String, isExec As Boolean) As Long
  Dim NumRighe As Long, wPosizione As Long, pCount As Long
  Dim pstart As Long, pend As Variant, pPos As Long, wIdSegm As Variant
  Dim flag As Boolean, wResp As Boolean
  Dim tb As Recordset
  Dim wVbCrLf As String
  Dim oldNomeRout As String, wTipoCall As String, wstr As String
  Dim k As Integer, numline As Integer
  Dim startkey As Long, offsetendtag As Integer, rsCall As Recordset, windentfor As Integer
  Dim linea As String, lineacomm1 As String, lineacomm2 As String, noindent As Integer, i As Integer
  Dim selstart1 As Long, selstart2 As Long, sellen1 As Integer, sellen2 As Integer
  Dim lendiff As Integer, elsethen As String
  Dim appoPos As Long, appoPstart As Long, delcomment As Integer, elsethen1 As String

  If isExec Then
    Set rsCall = m_fun.Open_Recordset("select linee from PsExec where IdOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga)
    wTipoCall = "EXEC"
  Else
    Set rsCall = m_fun.Open_Recordset("select linee from PsCall where IdOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga)
    wTipoCall = "CALL"
  End If
  
  'VERIFICA istruzione trovata
  If Not InStr(UCase(lineInstr), wTipoCall) > 0 Then
    AggLog "[Object: " & rst!IdOggetto & " - row: " & rst!Riga & "(" & NumRighe & ")] Instruction not found!", MadrdF_Incapsulatore.RTErr
    AsteriscaIstruzioneOF_PLI = 0
    Exit Function
  End If
  
  If Not rsCall.EOF Then
    numline = rsCall!linee
  End If
  'alpitour 5/8/2005: se non valida � inutile fare tutti i giri
  If rst!valida Then
    wResp = CaricaIstruzione(rst)
  End If

  'SQ: tutto sto casino per nomeroutine?
  oldNomeRout = nomeRoutine
  nomeRoutine = ""
  If rst!valida Then
    If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) > 0 Then
      nomeRoutine = MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).nomeRout
    End If
  End If
  If nomeRoutine = "" Then
    nomeRoutine = oldNomeRout
  End If
  
  indentkey = 0
  While (Mid(lineInstr, indentkey + 1, 1) = " ")
   indentkey = indentkey + 1
  Wend
  
  'AC
  ReDim Preserve VItable(UBound(VItable) + 1)
  VItable(UBound(VItable)).actionFail = ""
  VItable(UBound(VItable)).CommentType = "PLI"
  VItable(UBound(VItable)).isCaseSensitive = False
  VItable(UBound(VItable)).infoModify = "PLI"
  VItable(UBound(VItable)).RowNumber = rst!Riga
  VItable(UBound(VItable)).grouping_policy = "NUMBER"
  VItable(UBound(VItable)).numlines = numline
  VItable(UBound(VItable)).target = wTipoCall
  
  isPliCallinsideSelect = False
  
  'non cambiamo la linea originale, la funzione sotto va utilizzata al momento della scrittura del file
  'delcomment = MadrdM_Incapsulatore.findComment(lineinstr)
  elsethen = MadrdM_Incapsulatore.findThen(lineInstr)
  If Not elsethen = "" Then
    isPliCallinsideSelect = True
  Else
    elsethen1 = findThenDo(lineprevinstr)
    If Not elsethen1 = "" Then
      isPliCallinsideSelect = True
    End If
    elsethen = findThen(lineInstr)
  End If
  
  If indentkey < 1 Then indentkey = 2
  
  VItable(UBound(VItable)).lines_up = elsethen & Space(indentkey) & "/* " & IIf(Len(Trim(START_TAG)), START_TAG, "IRISDB") & " START ------------------------*"
  'VItable(UBound(VItable)).lines_down = Space(indentkey) & "* " & IIf(Len(Trim(END_TAG)), END_TAG, START_TAG) & " END    */ "
  VItable(UBound(VItable)).lines_down = Space(indentkey) & "* ---------------------------------------*/ "
  'TO DO gestire commenti all'interno delle linee di istruzione
  AsteriscaIstruzioneOF_PLI = 1
End Function

Public Function AsteriscaIstruzioneOF(rst As Recordset, lineInstr As String, lineprevinstr As String, typeIncaps As String, isExec As Boolean) As Long
  Dim NumRighe As Long, wPosizione As Long, pCount As Long
  Dim pstart As Variant, pend As Variant, pPos As Variant, wIdSegm As Variant
  Dim flag As Boolean, wResp As Boolean
  Dim tb As Recordset, rsCall As Recordset
  Dim wVbCrLf As String
  Dim oldNomeRout As String, wTipoCall As String, wstr As String
  Dim k As Integer
  Dim startkey As Long, offsetendtag As Integer, numline As Integer
  ' Mauro 31/07/2008
  Dim posTHEN As Long, wstart As Long
  Dim elsethen As String
  
  
  If isExec Then
    Set rsCall = m_fun.Open_Recordset("select linee from PsExec where IdOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga)
    wTipoCall = "EXEC"
  Else
    Set rsCall = m_fun.Open_Recordset("select linee from PsCall where IdOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga)
    wTipoCall = "CALL"
  End If
  If Not rsCall.EOF Then
    numline = rsCall!linee
  End If
  'alpitour 5/8/2005: se non valida � inutile fare tutti i giri
  'IRIS-DB: usare le non valide come dinamiche
  ' IRIS-DB If rst!valida Then
    wResp = CaricaIstruzione(rst)
  ' IRIS-DB End If

  'SQ: tutto sto casino per nomeroutine?
  oldNomeRout = nomeRoutine
  nomeRoutine = ""
  'IRIS-DB: usare le non valide come dinamiche
'IRIS-DB  If rst!valida Then
    If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) > 0 Then
      nomeRoutine = MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).nomeRout
    End If
'IRIS-DB  End If
  If nomeRoutine = "" Then
    nomeRoutine = oldNomeRout
  End If

   'VERIFICA istruzione trovata
  If Not InStr(lineInstr, wTipoCall) > 0 Then
    AggLog "[Object: " & rst!IdOggetto & " - row: " & rst!Riga & "(" & NumRighe & ")] Instruction not found!", MadrdF_Incapsulatore.RTErr
    AsteriscaIstruzioneOF = 0
    Exit Function
  End If
  indentkey = 0
  While (Mid(lineInstr, indentkey + 1, Len(wTipoCall)) <> wTipoCall)
   indentkey = indentkey + 1
  Wend
  If indentkey < 11 Then indentkey = 11 'IRIS-DB: con EXEC DLI si pu� anche iniziare a colonna 8, ma le righe COBOL devono partire da 12 minimo
   'AC
  ReDim Preserve VItable(UBound(VItable) + 1)
  VItable(UBound(VItable)).actionFail = ""
  VItable(UBound(VItable)).CommentType = "CBL"
  VItable(UBound(VItable)).isCaseSensitive = False
  VItable(UBound(VItable)).infoModify = "CBL"
  VItable(UBound(VItable)).RowNumber = rst!Riga
  VItable(UBound(VItable)).grouping_policy = "NUMBER"
  VItable(UBound(VItable)).numlines = numline
  'WALMART
  If IsWMIncaps Then
  'If UCase(Left(m_fun.FnNomeDB, 7)) = "WALMART" Then
     VItable(UBound(VItable)).infoModify = ""
  Else
    VItable(UBound(VItable)).infoModify = "CBLTAG" ' il commento sopra e sotto, non sulla riga
  End If
  VItable(UBound(VItable)).target = wTipoCall
  
  elsethen = findThenCBL_OF(lineInstr)
  If Len(elsethen) Then
    VItable(UBound(VItable)).lines_up = elsethen
  End If
  
  AsteriscaIstruzioneOF = 1
End Function

Public Function findThenCBL_OF(plinea As String) As String
  Dim posIF As Integer, posTHEN As Integer, posELSE As Integer
  Dim Parentesi As Integer, criterio As String
  
  posTHEN = InStr(plinea, " THEN ")
  If posTHEN = 0 Then
    posTHEN = InStr(plinea, " THEN")
  End If
  posIF = InStr(plinea, " IF ")
  posELSE = InStr(plinea, " ELSE ")
  If posELSE = 0 Then
    posELSE = InStr(plinea, " ELSE")
  End If
  If posTHEN > 0 Then
    If Not posIF > 0 Then
      findThenCBL_OF = Space(posTHEN - 1) & " THEN " '& vbCrLf
    Else
      findThenCBL_OF = Left(plinea, posTHEN + 4) '& vbCrLf
    End If
    plinea = Space(posTHEN + 5) & Right(plinea, Len(plinea) - posTHEN - 5)
  ElseIf posELSE > 0 Then
    findThenCBL_OF = Space(posELSE - 1) & " ELSE " '& vbCrLf
    plinea = Space(posELSE + 5) & Right(plinea, Len(plinea) - posELSE - 5)
  Else
    findThenCBL_OF = ""
  End If
End Function

Function findTarget_OF(ByVal Line As String, ByVal target As String, comment_type As String, isCSens As Boolean) As String
'a)
'ricerca la stringa all'interno della linea passata, utilizzando il metodo dei token
'restituisce la stringa vuota se il target � stato trovato
' restituisce una parte del target se solo una sua parte � stata trovata e la ricerca deve proseguire
' sulla riga successiva
' restituisce il target intero se la verifica � fallita
' Riassumendo, se strReturn � la stringa ritornata  strReturn = "": verifica OK  (true)
'                                                   strReturn <> target: proseguire ricerca
'                                                   strReturn = target: verifica KO  (false)


'b)
'la funzione si occupa anche di tener traccia dei commenti aperti/chiusi per il PLI

'c)
'la funzione tiene traccia anche dell'apertura degli THEN attraverso la variabile globale IsThenOpen
  Dim trgToken As String, lineToken As String, fulltarget As String
  Dim findfirst As Boolean
  Dim IsMacroOpen As Boolean
  
  fulltarget = target
  IsLineAnalized = True
  IsMacroOpen = False
  If comment_type = "PLI" And Len(Line) > 72 Then
    Line = Left(Line, 72)
  End If
  If comment_type = "PLI" And Len(Line) Then
    Line = Right(Line, Len(Line) - 1)
  End If
  If comment_type = "CBL" And Mid(Line, 7, 1) = "*" Then
    findTarget_OF = fulltarget ' equivale a target non trovato
    '''''''''''''''''''''''''''''''''''''
    If comment_type = "PLI" Then '
      While Not (Line = "")
        lineToken = nextToken_OF(Line)
        AdjustToken lineToken, Line ' in questa funzioncina aggiustiamo ci� che non gestisce il nextToken_OF
        If Left(lineToken, 2) = "/*" Then
          isPLIcommentOpened = True
        End If
        If Left(lineToken, 2) = "*/" Or Right(lineToken, 2) = "*/" Then
          isPLIcommentOpened = False
        End If
      Wend
    End If
    ''''''''''''''''''''''''''''''''''''
    Exit Function
  End If
  If comment_type = "CBL" And Not Mid(Line, 7, 1) = "*" And InStr(Line, "THEN") > 0 Then
    isThenOpen = True
  End If
  
  'in attesa di idee migliori azzeriamo le parentesi e "'" per far funzionare il nexttoken
  ' valutare se anziche leggere linea per linea raggruppare le linee in modo da avere
  ' la parificazione delle parentesi
  Line = Replace(Replace(Line, "(", " "), ")", " ")
  Line = Replace(Line, "'", " ")
  
  'prendiamo la posizione di un eventuale THEN
  thenPos = InStr(Line, "THEN")
  
  While Not (target = "" Or Line = "")
    trgToken = nextToken_OF(target)
    lineToken = nextToken_OF(Line)
    AdjustToken lineToken, Line ' in questa funzioncina aggiustiamo ci� che non gestisce il nextToken_OF
    'AC 06/10/2010 facciamo lo stesso anche per il target token
    AdjustToken trgToken, target
    If Left(lineToken, 2) = "/*" And comment_type = "PLI" Then
      isPLIcommentOpened = True
    End If
    If (Left(lineToken, 2) = "*/" Or Right(lineToken, 2) = "*/") And comment_type = "PLI" Then
      isPLIcommentOpened = False
    End If
    '*********************************************
    ' controlliamo se siamo all'interno di macro
    'vogliamo evitare ad esempio di inserire  righe dentro costrutti tipo %IF  ... %END
    If Left(lineToken, 1) = "%" And comment_type = "PLI" Then
     If Mid(lineToken, 2, Len(lineToken) - 1) = "IF" Or Mid(lineToken, 2, Len(lineToken) - 1) = "DO" Or Mid(lineToken, 2, Len(lineToken) - 1) = "SELECT" _
                                                 Or Mid(lineToken, 2, Len(lineToken) - 1) = "WHEN" Then
        If Not IsMacroOpen Then ' un solo incremmento per riga
          cntPliMacro = cntPliMacro + 1
          IsMacroOpen = True
        End If
     ElseIf Mid(lineToken, 2, Len(lineToken) - 1) = "END" Then
        cntPliMacro = cntPliMacro - 1
     End If
    End If
    '*********************************************
    
    ' controllo "THEN"
    If Not isPLIcommentOpened Then
      If lineToken = "THEN" Then
        isThenOpen = True
      Else
        isThenOpen = False
      End If
    End If
    
    While Not (IIf(isCSens, trgToken, UCase(trgToken)) = IIf(isCSens, lineToken, UCase(lineToken))) And Not Trim(Line) = ""
      If Not findfirst Then
        lineToken = nextToken_OF(Line)
        AdjustToken lineToken, Line ' in questa funzioncina aggiustiamo ci� che non gestisce il nextToken_OF
        ' solito censimento dei commenti
        If Left(lineToken, 2) = "/*" And comment_type = "PLI" Then
          isPLIcommentOpened = True
        End If
        If (Left(lineToken, 2) = "*/" Or Right(lineToken, 2) = "*/") And comment_type = "PLI" Then
          isPLIcommentOpened = False
        End If
        ' controllo "THEN"
        If Not isPLIcommentOpened Then
          If lineToken = "THEN" Then
            isThenOpen = True
          Else
            isThenOpen = False
          End If
        End If
         '*********************************************
        ' controlliamo se siamo all'interno di macro
        'vogliamo evitare ad esempio di inserire  righe dentro costrutti tipo %IF  ... %END
        If Left(lineToken, 1) = "%" And comment_type = "PLI" Then
          If Mid(lineToken, 2, Len(lineToken) - 1) = "IF" Or Mid(lineToken, 2, Len(lineToken) - 1) = "DO" Or Mid(lineToken, 2, Len(lineToken) - 1) = "SELECT" _
                                                      Or Mid(lineToken, 2, Len(lineToken) - 1) = "WHEN" Then
            If Not IsMacroOpen Then ' un solo incremmento per riga
              cntPliMacro = cntPliMacro + 1
              IsMacroOpen = True
            End If
          ElseIf Mid(lineToken, 2, Len(lineToken) - 1) = "END" Then
            cntPliMacro = cntPliMacro - 1
          End If
        End If
         '*********************************************
        '*****************************
      Else
        findTarget_OF = fulltarget  ' |----->  stringa non trovata
        '''''''''''''''''''''''''''''''''''''
        If comment_type = "PLI" Then '
          While Not (Line = "")
            lineToken = nextToken_OF(Line)
            AdjustToken lineToken, Line ' in questa funzioncina aggiustiamo ci� che non gestisce il nextToken_OF
            If Left(lineToken, 2) = "/*" Then
              isPLIcommentOpened = True
            End If
            If Left(lineToken, 2) = "*/" Or Right(lineToken, 2) = "*/" Then
              isPLIcommentOpened = False
            End If
            '*********************************************
            ' controlliamo se siamo all'interno di macro
            'vogliamo evitare ad esempio di inserire  righe dentro costrutti tipo %IF  ... %END
            If Left(lineToken, 1) = "%" And comment_type = "PLI" Then
              If Mid(lineToken, 2, Len(lineToken) - 1) = "IF" Or Mid(lineToken, 2, Len(lineToken) - 1) = "DO" Or Mid(lineToken, 2, Len(lineToken) - 1) = "SELECT" _
                                                         Or Mid(lineToken, 2, Len(lineToken) - 1) = "WHEN" Then
                If Not IsMacroOpen Then ' un solo incremmento per riga
                  cntPliMacro = cntPliMacro + 1
                  IsMacroOpen = True
                End If
              ElseIf Mid(lineToken, 2, Len(lineToken) - 1) = "END" Then
                cntPliMacro = cntPliMacro - 1
              End If
            End If
          '*********************************************
          Wend
        End If
        ''''''''''''''''''''''''''''''''''''
        Exit Function
      End If
    Wend
    If Not Trim(Line) = "" Then
      findfirst = True
      'trgToken = nextToken_OF(target)
    Else  ' line = ""
      If (IIf(isCSens, trgToken, UCase(trgToken)) = IIf(isCSens, lineToken, UCase(lineToken))) Then
        If Not (comment_type = "PLI" And isPLIcommentOpened) Then
          findTarget_OF = target  ' |-----> stringa trovata in parte: ricerca deve proseguire su riga successiva
        Else
          findTarget_OF = fulltarget ' equivale a target non trovato
        End If
        '''''''''''''''''''''''''''''''''''''
        If comment_type = "PLI" Then '
          While Not (Trim(Line) = "")
            lineToken = nextToken_OF(Line)
            AdjustToken lineToken, Line ' in questa funzioncina aggiustiamo ci� che non gestisce il nextToken_OF
            If Left(lineToken, 2) = "/*" Then
              isPLIcommentOpened = True
            End If
            If Left(lineToken, 2) = "*/" Or Right(lineToken, 2) = "*/" Then
              isPLIcommentOpened = False
            End If
            '*********************************************
            ' controlliamo se siamo all'interno di macro
            'vogliamo evitare ad esempio di inserire  righe dentro costrutti tipo %IF  ... %END
            If Left(lineToken, 1) = "%" And comment_type = "PLI" Then
              If Mid(lineToken, 2, Len(lineToken) - 1) = "IF" Or Mid(lineToken, 2, Len(lineToken) - 1) = "DO" Or Mid(lineToken, 2, Len(lineToken) - 1) = "SELECT" _
                                                         Or Mid(lineToken, 2, Len(lineToken) - 1) = "WHEN" Then
                If Not IsMacroOpen Then ' un solo incremmento per riga
                  cntPliMacro = cntPliMacro + 1
                  IsMacroOpen = True
                End If
              ElseIf Mid(lineToken, 2, Len(lineToken) - 1) = "END" Then
                cntPliMacro = cntPliMacro - 1
              End If
            End If
          '*********************************************
          Wend
        End If
        ''''''''''''''''''''''''''''''''''''
        Exit Function
      Else ' linea esaurita, ma ultimo token non trovato --> devo ricostroire target
        ' ricostruisco target
        If target = "" Then
          target = trgToken
        Else
          target = trgToken & " " & target
        End If
      End If
    End If
  Wend
  If Not (comment_type = "PLI" And isPLIcommentOpened) Then
    findTarget_OF = target ' |-----> esco qui se target = "" (target trovato) o linea vuota in partenza (restituisce target intero, quindi non trovato)
  Else
    findTarget_OF = fulltarget ' equivale a target non trovato
  End If
  'Check commenti PLI
  If comment_type = "PLI" Then '
    While Not (Line = "")
      lineToken = nextToken_OF(Line)
      AdjustToken lineToken, Line ' in questa funzioncina aggiustiamo ci� che non gestisce il nextToken_OF
      If Left(lineToken, 2) = "/*" Then
        isPLIcommentOpened = True
      End If
      If Left(lineToken, 2) = "*/" Or Right(lineToken, 2) = "*/" Then
        isPLIcommentOpened = False
      End If
      '*********************************************
     ' controlliamo se siamo all'interno di macro
     'vogliamo evitare ad esempio di inserire  righe dentro costrutti tipo %IF  ... %END
     If Left(lineToken, 1) = "%" And comment_type = "PLI" Then
       If Mid(lineToken, 2, Len(lineToken) - 1) = "IF" Or Mid(lineToken, 2, Len(lineToken) - 1) = "DO" Or Mid(lineToken, 2, Len(lineToken) - 1) = "SELECT" _
                                                 Or Mid(lineToken, 2, Len(lineToken) - 1) = "WHEN" Then
         If Not IsMacroOpen Then ' un solo incremmento per riga
           cntPliMacro = cntPliMacro + 1
           IsMacroOpen = True
         End If
       ElseIf Mid(lineToken, 2, Len(lineToken) - 1) = "END" Then
         cntPliMacro = cntPliMacro - 1
       End If
     End If
     '*********************************************
    Wend
  End If
End Function

Function chkempty(Line As String, updwn As String) As String
  If Line = "" Then
    chkempty = ""
    Exit Function
  End If
  'IRIS-DB: mah, sbaglia sulle TERM o sulle istruzioni di una riga sola per qualche motivo, questa e' proprio una pezza a colori
  If Not Left(Line, 2) = vbCrLf Then
    If updwn = "U" Then
      chkempty = Line & vbCrLf
    Else
      chkempty = vbCrLf & Line
    End If
  Else
    chkempty = Line
  End If
End Function

'Inizia una nuova era, la lettura scrittura ON FLY
'elabora il file utilizzando le tabelle (matrici) delle regole VItable e FItable
Function TransformFile_OF(filenameI As String, filenameO As String) As String
  Dim FDin As Long, FDout As Long  'FILE
  Dim lastindexRow As Integer ' ultimo indice controllato della matrice VItable
  Dim isVIchecked As Boolean  ' indica se � stato fatto controllo su VItable (in questo caso salto FItable)
  Dim FIinprogress As InfoVerifyAndInsert ' tiene traccia di un controllo parziale VI da estendere
                                          ' sulla riga successiva
  Dim FIindex As Integer, isFItofinish As Boolean ' indica l'indice della ricerca FI in corso
  Dim i As Integer, buffer As String '  accumulo righe
  Dim isBUFFERcomplete As Boolean ' mi dice se il buffer � pronto per essere scritto sul file di output
                                  ' oppure sono ancora in una fase in cui sto accumulando righe
  Dim isSerchingTerminator As Boolean 'fleg (ricerca particolare)
  Dim terminatoroffset As Integer ' distanza fra l'ultimo elemeto del target e il terminatore
  Dim lineNumber As Long, Line As String, outputtarget As String, linesInProgress As Integer
  Dim isBuffering As Boolean
  
  'variabile da inserire prima o dopo lines_up e lines_down (ad esempio per la gestione degli if nel PLI)
  'utilizzate il valore non pu� essere determinato a priori ma solo durante la lettura delle linee
  '(in caso contrario lines_up e lines_down sono comprensive di tutto)
  Dim before_up As String, after_up As String, before_down As String, after_down As String
  Dim bolInsideFIcicle As Boolean
  Dim IsSectionInProgress As Boolean  ' verifica se sono in fase di validazione di una sottosessione
  Dim appoCommentOpened As Boolean, IsSectionLabelTruncated As Boolean, labelTrunc As String, somethingfound As Boolean
  Dim isPartialSectionFound As Boolean, indexPartialSection As Integer, wordPartialSection As String, outerror As String


  On Error GoTo HandleError
  isThenOpen = False
  IsSectionInProgress = False
  lineNumber = 0
  lastindexRow = 1
  cntPliMacro = 0
  'input
  FDin = FreeFile
  Open filenameI For Input As FDin
  'output:
  FDout = FreeFile
  Open filenameO For Output As FDout

  '********************* AC 23/12/2008  ******************************************
  'all'interno del ciclo While facciamo una sola lettura di riga per volta
  ' se troviamo informazioni parziali aggiorniamo le nostre strutture per riprendere
  ' la ricerca dalla linea successiva
  '
  ' TIPI DI RICERCA
  'a) VI  serve per verificare se su una data riga � presente una stringa target
  '       (es. incapsulamento istruzione)
  '       se si trova si inserisce la stringa lines
  '       Utilizza la matrice FItable
  '       Nel caso si trovi la stringa target solo parzialmente la struttura si autoaggiorna per
  '       proseguire la ricerca sulla riga successiva
  '
  'b) FI  serve per vedere se all'interno della linea letta c'� una delle stringhe target che
  '       stiamo cercando (es. inserire IRIS_ all'interno di proc)
  '       Utilizza la matrice VItable (in questo caso si scorrono tutte le righe della matrice
  '       ed usciamo dal ciclo solo se troviamo qualcosa di utile)
  '       Nel caso si trovi la stringa target solo parzialmente, si utilizza la struttura
  '       FIinprogress di appoggio (strutturata come la VItable perch� sappiamo che il pezzo di
  '       stringa target va cercato sulla riga successiva)

  '*****************************************************************************************
  FIindex = -1
  While Not EOF(FDin)
    Line Input #FDin, Line
    IsLineAnalized = False
    appoCommentOpened = isPLIcommentOpened
    'buffer = buffer & vbCrLf & Line
    lineNumber = lineNumber + 1
    isVIchecked = False
    'TO CHECK
    If isBuffering Then
      ' sto accumulando righe, verifico se sono arrivato alla fine dell'accumulo per abilitare scrittura
      ' riguarda solo la VItable
      'IRIS-DB vaffanculo alle cose complicate: non gestisce righe pi� corte di 7 byte...
      'buffer = buffer & vbCrLf & ModifyLine_OF(Line, VItable(lastindexRow).infoModify, filenameI, lineNumber)
      If Len(Line) > 6 Then
         buffer = buffer & vbCrLf & ModifyLine_OF(Line, VItable(lastindexRow).infoModify, filenameI, lineNumber)
      Else
         buffer = buffer & vbCrLf & Line
      End If
      linesInProgress = linesInProgress + 1
      If VItable(lastindexRow).numlines = linesInProgress Then
        isBuffering = False
        If Left(VItable(lastindexRow).lines_down, 2) = vbCrLf Then 'IRIS-DB
           VItable(lastindexRow).lines_down = Right(VItable(lastindexRow).lines_down, Len(VItable(lastindexRow).lines_down) - 2) 'IRIS-DB
        End If 'IRIS-DB
        buffer = before_up & chkempty(VItable(lastindexRow).lines_up, "U") & after_up & ModifyLine_OF(buffer, VItable(lastindexRow).infoModify, filenameI, lineNumber) & before_down & chkempty(VItable(lastindexRow).lines_down, "D") & after_down
        isBUFFERcomplete = True ' ---> scrittura su file output
        lastindexRow = lastindexRow + 1
      End If
    Else
      'Primo controllo: verifico se c'� una ricerca di tipo FI in progress, cio�
      'se ho trovato una parte del target sulle linee precedenti e sto cercando il resto sulla riga corrente
      If FIinprogress.RowNumber = lineNumber Then  ' ricerca di tipo FI in corso ----------------->  FItable iniziato
        linesInProgress = linesInProgress + 1
        isPLIcommentOpened = appoCommentOpened
        outputtarget = findTarget_OF(Line, FIinprogress.target, FIinprogress.CommentType, FIinprogress.isCaseSensitive)
        Select Case outputtarget
          Case "" 'il target � stato completamente trovato
            If Not isSerchingTerminator Then
              Select Case FItable(FIindex).grouping_policy
                Case "ONEROW"
                  ' le righe precedenti le ho gia scritte sull'output, nel buffer metto solo l'elaborazione dell'ultima riga
                  buffer = before_up & chkempty(FIinprogress.lines_up, "U") & after_up & ModifyLine_OF(Line, FIinprogress.infoModify, filenameI, lineNumber) & before_down & chkempty(FIinprogress.lines_down, "D") & after_down
                  isBUFFERcomplete = True ' ---> scrittura su file output
                  If FItable(FIindex).isUnique Then
                    ' elimino riga da FItable
                    removeItemFromFITable FItable, FIindex
                    If FIindex < indexPartialSection Then
                      indexPartialSection = indexPartialSection - 1
                    End If
                  End If
                  FIindex = -1
                Case "TARGET"
                  buffer = buffer & vbCrLf & Line
                  buffer = before_up & chkempty(FIinprogress.lines_up, "U") & after_up & ModifyLine_OF(buffer, FIinprogress.infoModify, filenameI, lineNumber) & before_down & chkempty(FIinprogress.lines_down, "D") & after_down
                  isBUFFERcomplete = True ' ---> scrittura su file output
                  If FItable(FIindex).isUnique Then
                    ' elimino riga da FItable
                    removeItemFromFITable FItable, FIindex
                    If FIindex < indexPartialSection Then
                      indexPartialSection = indexPartialSection - 1
                    End If
                  End If
                  FIindex = -1
                Case "TERMINATOR"
                  buffer = buffer & vbCrLf & Line ' accumulo finch� ricerca non � finita
                  isPLIcommentOpened = appoCommentOpened
                  outputtarget = findTarget_OF(Line, FIinprogress.terminator, FIinprogress.CommentType, FIinprogress.isCaseSensitive)
                  If outputtarget = "" Then
                    buffer = before_up & chkempty(FIinprogress.lines_up, "U") & after_up & ModifyLine_OF(buffer, FIinprogress.infoModify, filenameI, lineNumber, outerror) & before_down & chkempty(FIinprogress.lines_down, "D") & after_down
                    isBUFFERcomplete = True ' ---> scrittura su file output
                    If outerror = "LENERROR" Then
                      AggLog "[Object: " & filenameI & ", LineNumber: " & lineNumber & "] error: " & "Line/s with more than 72 characters" & vbCrLf, MadrdF_Incapsulatore.RTErr
                    End If
                    If FItable(FIindex).isUnique Then
                      ' elimino riga da FItable
                      removeItemFromFITable FItable, FIindex
                      If FIindex < indexPartialSection Then
                        indexPartialSection = indexPartialSection - 1
                      End If
                    End If
                    FIindex = -1
                  Else
                    isBUFFERcomplete = False
                    isSerchingTerminator = True
                    FIinprogress.RowNumber = FIinprogress.RowNumber + 1
                    FIinprogress.target = FIinprogress.terminator
                    terminatoroffset = 1
                  End If
              End Select
            Else
              '� in corso una ricerca sul terminatore andata a buon fine
              buffer = buffer & vbCrLf & Line
              buffer = before_up & chkempty(FIinprogress.lines_up, "U") & after_up & ModifyLine_OF(buffer, FIinprogress.infoModify, filenameI, lineNumber) & before_down & chkempty(FIinprogress.lines_down, "D") & after_down
              isBUFFERcomplete = True ' ---> scrittura su file output
              If FItable(FIindex).isUnique Then
                ' elimino riga da FItable
                removeItemFromFITable FItable, FIindex
                If FIindex < indexPartialSection Then
                  indexPartialSection = indexPartialSection - 1
                End If
              End If
              FIindex = -1
              isSerchingTerminator = False
            End If
          Case FIinprogress.target ' target non trovato
            If isSerchingTerminator Then
              buffer = buffer & vbCrLf & Line ' accumulo sul buffer
              FIinprogress.RowNumber = FIinprogress.RowNumber + 1
              terminatoroffset = terminatoroffset + 1
              'TO DO : mettere un limite per la ricerca del terminatore
            Else
              ' scrivo quello che ho accumulato sul buffer senza nessuna elaborazione
              If buffer = "" Then
                buffer = Line
              Else
                buffer = buffer & vbCrLf & Line
              End If
              isBUFFERcomplete = True ' ---> scrittura su file output
            End If
          Case Else
            ' il target � stato trovato solo in parte, la ricerca riparte dalla linea successiva
            ' aggiorno riga su cui cercare e cosa cercare
            Select Case FItable(FIindex).grouping_policy
              Case "ONEROW"
                buffer = Line
                isBUFFERcomplete = True ' ---> scrittura su file output
              Case "TARGET", "TERMINATOR"
                buffer = buffer & vbCrLf & Line
                isBUFFERcomplete = False ' accumulo e non scrivo
            End Select
            FIinprogress.RowNumber = FIinprogress.RowNumber + 1
            FIinprogress.target = outputtarget
        End Select
      Else
        'Nessuna ricerca di tipo FI in corso, vediamo se la riga attuale � segnata nella VItable come riga da controllare
        If UBound(VItable) >= lastindexRow Then 'sto tenendo anche traccia dell'ultimo indeci di VItable controllato
          If VItable(lastindexRow).RowNumber = lineNumber Then
            isVIchecked = True
            linesInProgress = 1
            isPLIcommentOpened = appoCommentOpened
            outputtarget = findTarget_OF(Line, VItable(lastindexRow).target, VItable(lastindexRow).CommentType, VItable(lastindexRow).isCaseSensitive)
            Select Case outputtarget
              Case "" 'inserisco linea
                If Not isSerchingTerminator Then
                  Select Case VItable(lastindexRow).grouping_policy
                    Case "ONEROW"
                      buffer = chkempty(VItable(lastindexRow).lines_up, "U") & after_up & ModifyLine_OF(Line, VItable(lastindexRow).infoModify, filenameI, lineNumber) & before_down & chkempty(VItable(lastindexRow).lines_down, "D") & after_down
                      isBUFFERcomplete = True ' ---> scrittura su file output
                      lastindexRow = lastindexRow + 1
                    Case "TARGET"
                      buffer = buffer & vbCrLf & Line
                      buffer = before_up & chkempty(VItable(lastindexRow).lines_up, "U") & after_up & ModifyLine_OF(buffer, VItable(lastindexRow).infoModify, filenameI, lineNumber) & before_down & chkempty(VItable(lastindexRow).lines_down, "D") & after_down
                      isBUFFERcomplete = True ' ---> scrittura su file output
                      lastindexRow = lastindexRow + 1
                    Case "TERMINATOR"
                      'cerco il terminatore sulla stessa riga su cui ho trovato target
                      'NB: IPOTIZZO DI NON POTER AVERE TERMINATORE DAVANTI A TARGET
                      isPLIcommentOpened = appoCommentOpened
                      outputtarget = findTarget_OF(Line, VItable(lastindexRow).target, VItable(lastindexRow).CommentType, VItable(lastindexRow).isCaseSensitive)
                      If outputtarget = "" Then
                        buffer = chkempty(VItable(lastindexRow).lines_up, "U") & after_up & ModifyLine_OF(Line, VItable(lastindexRow).infoModify, filenameI, lineNumber) & before_down & chkempty(VItable(lastindexRow).lines_down, "D") & after_down
                        isBUFFERcomplete = True ' ---> scrittura su file output
                        lastindexRow = lastindexRow + 1
                      Else
                        buffer = buffer & vbCrLf & Line ' accumulo finch� ricerca non � finita
                        isBUFFERcomplete = False
                        isSerchingTerminator = True
                        VItable(lastindexRow).RowNumber = VItable(lastindexRow).RowNumber + 1
                        VItable(lastindexRow).target = VItable(lastindexRow).terminator
                        terminatoroffset = 1
                      End If
                    Case "NUMBER"
                      If VItable(lastindexRow).numlines = 1 Then
                        If Not buffer = "" Then
                          buffer = buffer & vbCrLf & Line
                        Else
                          buffer = Line
                        End If
                        ' RIMOSSO!!! inserimento del DO; e dell END; NB: per ora gestito nella creazione della linea
                        'If isThenOpen And VItable(lastindexRow).CommentType = "PLI" Then
                        '    before_up = Space(thenPos) & "DO;" & vbCrLf
                        '    after_down = vbCrLf & Space(thenPos) & "END;"
                        'End If
                        buffer = before_up & chkempty(VItable(lastindexRow).lines_up, "U") & after_up & ModifyLine_OF(buffer, VItable(lastindexRow).infoModify, filenameI, lineNumber) & before_down & chkempty(VItable(lastindexRow).lines_down, "D") & after_down
                        isBUFFERcomplete = True ' ---> scrittura su file output
                        lastindexRow = lastindexRow + 1
                      Else
                        isBuffering = True
                        isBUFFERcomplete = False
                        If Not buffer = "" Then
                          buffer = buffer & vbCrLf & ModifyLine_OF(Line, VItable(lastindexRow).infoModify, filenameI, lineNumber)    'Line
                        Else
                          buffer = ModifyLine_OF(Line, VItable(lastindexRow).infoModify, filenameI, lineNumber) 'Line
                        End If
                      End If
                  End Select
                Else
                  '� in corso una ricerca sul terminatore andata a buon fine
                  buffer = buffer & vbCrLf & Line
                  buffer = before_up & chkempty(VItable(lastindexRow).lines_up, "U") & after_up & ModifyLine_OF(buffer, VItable(lastindexRow).infoModify, filenameI, lineNumber) & before_down & chkempty(VItable(lastindexRow).lines_down, "D") & after_down
                  isBUFFERcomplete = True ' ---> scrittura su file output
                  isSerchingTerminator = False
                  lastindexRow = lastindexRow + 1
                End If
              Case VItable(lastindexRow).target ' target non trovato
                If isSerchingTerminator Then
                  buffer = buffer & vbCrLf & Line ' accumulo sul buffer
                  terminatoroffset = terminatoroffset + 1
                  'TO DO : mettere un limite per la ricerca del terminatore
                Else
                  ' scrivo quello che ho accumulato sul buffer senza nessuna elaborazione
                  If buffer = "" Then
                    buffer = Line
                  Else
                    buffer = buffer & vbCrLf & Line
                  End If
                  isBUFFERcomplete = True ' ---> scrittura su file output
                  lastindexRow = lastindexRow + 1
                End If
              Case Else
                ' la ricerca riparte dalla linea successiva
                ' aggiorno riga su cui cercare e cosa cercare
                Select Case FItable(FIindex).grouping_policy
                  Case "ONEROW"
                    buffer = Line
                    isBUFFERcomplete = True ' ---> scrittura su file output
                  Case "TARGET", "TERMINATOR"
                    buffer = buffer & vbCrLf & Line
                    isBUFFERcomplete = False ' accumulo e non scrivo
                End Select
                VItable(lastindexRow).RowNumber = VItable(lastindexRow).RowNumber + 1
                VItable(lastindexRow).target = outputtarget
                'TO DO  gestione buffer in base alla politica di raggruppamento
            End Select
          End If
        End If
        If Not isVIchecked Then
          'controllo matrice FItable ----------------->  FItable NUOVO
          ' in questo ramo entro solo all'inizio della ricerca, il buffer � quindi vuoto
          
          ' controllo anche variabile IsSubSection per filtrare la FItable
          ' se il campo target � vuoto inserisco nella prima riga utile
          ' utilizzato per le subsection, in questo caso ha senso utilizzare lines_up per inserire
          ' subito dopo la linea che individua la sezione
          
          If UBound(FItable) = 0 Then
            buffer = Line
          End If
          bolInsideFIcicle = False
          somethingfound = False
          For i = 1 To UBound(FItable)
            If Not FItable(i).isSubSection Then
              bolInsideFIcicle = True
              If FItable(i).target = "" Then
                buffer = before_up & chkempty(FItable(i).lines_up, "U") & after_up & ModifyLine_OF(Line, FItable(i).infoModify, filenameI, lineNumber) & before_down & chkempty(FItable(i).lines_down, "D") & after_down
                'AC 07/06/10 problema DCL subito sotto proc (inserimento contemporaneo)
                If Not isSerchingTerminator Then
                    isBUFFERcomplete = True ' ---> scrittura su file output
                End If
                If FItable(i).isUnique Then
                  ' elimino riga da FItable
                  removeItemFromFITable FItable, i
                  If i < indexPartialSection Then
                      indexPartialSection = indexPartialSection - 1
                  End If
                  Exit For
                End If
              Else
                isPLIcommentOpened = appoCommentOpened
                outputtarget = findTarget_OF(Line, FItable(i).target, FItable(i).CommentType, FItable(i).isCaseSensitive)
                'AC 04/05/10
                If FIindex = -1 Then
                    buffer = ""
                End If
                'AC 07/04/10
                ' Gestione macro tramite variabile cntPliMacro
                ' se outputtarget = "", ma sono dentro costrutto Macro (IF,WHILE,SELECT -> cntPliMacro > 0)
                ' forzo outoutputtarget simulando la  stringa non trovata
                If outputtarget = "" And cntPliMacro > 0 Then
                    outputtarget = FItable(i).target
                End If
                If cntPliMacro < 0 Then    '=>  WARNING
                    AggLog "[Object: " & filenameI & ", LineNumber: " & lineNumber & "] error: " & "Problems in managing macro inside program, please check it!" & vbCrLf, MadrdF_Incapsulatore.RTErr
                End If
                Select Case outputtarget
                  Case "" 'inserisco linea o proseguo per la ricerca del terminatore
                    If Not buffer = "" And somethingfound Then
'IRIS-DB not clear as the encaps anyhow works fine AggLog "[Object: " & filenameI & ", LineNumber: " & lineNumber & "] error: SEARCH CONFLICT !! " & vbCrLf, MadrdF_Incapsulatore.RTErr
                    End If
                    somethingfound = True
                    Select Case FItable(i).grouping_policy
                      Case "ONEROW", "TARGET"
                        buffer = before_up & chkempty(FItable(i).lines_up, "U") & after_up & ModifyLine_OF(Line, FItable(i).infoModify, filenameI, lineNumber) & before_down & chkempty(FItable(i).lines_down, "D") & after_down
                        isBUFFERcomplete = True ' ---> scrittura su file output
                        If FItable(i).isUnique Then
                          ' elimino riga da FItable
                          removeItemFromFITable FItable, i
                          If i < indexPartialSection Then
                            indexPartialSection = indexPartialSection - 1
                          End If
                          Exit For
                        End If
                      Case "TERMINATOR"
                        'cerco il terminatore sulla stessa riga su cui ho trovato target
                        'NB: IPOTIZZO DI NON POTER AVERE TERMINATORE DAVANTI A TARGET
                        isPLIcommentOpened = appoCommentOpened
                        outputtarget = findTarget_OF(Line, FItable(i).terminator, FItable(i).CommentType, FItable(i).isCaseSensitive)
                        If outputtarget = "" Then
                          buffer = before_up & chkempty(FItable(i).lines_up, "U") & after_up & ModifyLine_OF(Line, FItable(i).infoModify, filenameI, lineNumber) & before_down & chkempty(FItable(i).lines_down, "D") & after_down
                          isBUFFERcomplete = True ' ---> scrittura su file output
                          If FItable(i).isUnique Then
                            ' elimino riga da FItable
                            removeItemFromFITable FItable, i
                            If i < indexPartialSection Then
                              indexPartialSection = indexPartialSection - 1
                            End If
                            Exit For
                          End If
                        Else
                          ' accumulo finch� ricerca non � finita
                          If Not buffer = "" And Not buffer = Line Then
                            buffer = buffer & vbCrLf & Line
                          Else
                            buffer = Line
                          End If
                          isBUFFERcomplete = False
                          isSerchingTerminator = True
                          ' la ricerca deve ripartire dalla linea successiva
                          ' sposto la ricerca sulla var di appoggio FIinprogress
                          FIinprogress.actionFail = ""
                          FIinprogress.actionOK = ""
                          FIinprogress.lines_up = FItable(i).lines_up
                          FIinprogress.lines_down = FItable(i).lines_down
                          FIinprogress.infoModify = FItable(i).infoModify
                          FIinprogress.grouping_policy = FItable(i).grouping_policy
                          FIinprogress.terminator = FItable(i).terminator
                          FIinprogress.RowNumber = lineNumber + 1
                          FIinprogress.target = FItable(i).terminator
                          FIinprogress.CommentType = FItable(i).CommentType
                          FIinprogress.isCaseSensitive = FItable(i).isCaseSensitive
                          FIinprogress.infoModify = FItable(i).infoModify
                          FIindex = i
                          terminatoroffset = 1
                        End If
                    End Select
                  Case FItable(i).target ' target non trovato sulla riga
                    If buffer = "" Then
                        buffer = Line
                        isBUFFERcomplete = True ' ---> scrittura su file output
                    End If
                  Case Else
                    ' la ricerca deve ripartire dalla linea successiva
                    ' sposto la ricerca sulla var di appoggio FIinprogress
                    If Not buffer = "" And somethingfound Then
                        AggLog "[Object: " & filenameI & ", LineNumber: " & lineNumber & "] error: SEARCH CONFLICT !! " & vbCrLf, MadrdF_Incapsulatore.RTErr
                    End If
                    somethingfound = True
                    buffer = Line ' accumulo, il buffer � vuoto
                    If FItable(i).grouping_policy = "ONEROW" Then
                      isBUFFERcomplete = True ' ---> scrittura su file output
                    Else
                      isBUFFERcomplete = False
                    End If
                    FIinprogress.actionFail = ""
                    FIinprogress.actionOK = ""
                    FIinprogress.lines_up = FItable(i).lines_up
                    FIinprogress.lines_down = FItable(i).lines_down
                    FIinprogress.infoModify = FItable(i).infoModify
                    FIinprogress.grouping_policy = FItable(i).grouping_policy
                    FIinprogress.terminator = FItable(i).terminator
                    FIinprogress.RowNumber = lineNumber + 1
                    FIinprogress.target = outputtarget
                    FIinprogress.CommentType = FItable(i).CommentType
                    FIinprogress.isCaseSensitive = FItable(i).isCaseSensitive
                    FIinprogress.infoModify = FItable(i).infoModify
                    FIindex = i
                End Select
              End If ' target = ""
            End If ' if che verifica is SubSection
          Next i
          ' dopo il ciclo for verifico che almeno uno volta sia entrato nella ricerca di un target
          If Not bolInsideFIcicle Then
            buffer = Line
            isBUFFERcomplete = True ' ---> scrittura su file output
          End If
        End If
      End If
    End If  ' isBuffering
    'Aggiornamento campi sottosezione
    For i = 1 To UBound(FItable)
      If FItable(i).isSubSection Then  ' vediamo se siamo all'inizo della sezione
        '' GESTIONE SUBSECTION
        If IsSectionInProgress Then
          isPLIcommentOpened = appoCommentOpened
          outputtarget = findTarget_OF(Line, FItable(i).sectionTerminator, FItable(i).CommentType, FItable(i).isCaseSensitive)
          If outputtarget = "" Then
            FItable(i).isSubSection = False ' da questo momento abilito la ricerca del target
            IsSectionInProgress = False
          End If
        '******* AC 07/04/10 *****************
        ElseIf isPartialSectionFound And i = indexPartialSection Then
            isPartialSectionFound = False ' cerchiamo solo su riga successiva
            isPLIcommentOpened = appoCommentOpened
            outputtarget = findTarget_OF(Line, wordPartialSection, FItable(i).CommentType, FItable(i).isCaseSensitive)
            If outputtarget = "" Then
              ' AC 13/01/09
              If FItable(i).sectionTerminator = "" Then
                FItable(i).isSubSection = False ' da questo momento abilito la ricerca del target
              Else 'cerchiamo il terminatore
                isPLIcommentOpened = appoCommentOpened
                outputtarget = findTarget_OF(Line, FItable(i).sectionTerminator, FItable(i).CommentType, FItable(i).isCaseSensitive)
                If outputtarget = "" Then
                  FItable(i).isSubSection = False ' da questo momento abilito la ricerca del target
                Else
                  IsSectionInProgress = True
                End If
              End If
            End If
        Else
          isPLIcommentOpened = appoCommentOpened
          outputtarget = findTarget_OF(Line, FItable(i).startSection, FItable(i).CommentType, FItable(i).isCaseSensitive)
          If outputtarget = "" Then
            ' AC 13/01/09
            If FItable(i).sectionTerminator = "" Then
              FItable(i).isSubSection = False ' da questo momento abilito la ricerca del target
            Else 'cerchiamo il terminatore
              isPLIcommentOpened = appoCommentOpened
              outputtarget = findTarget_OF(Line, FItable(i).sectionTerminator, FItable(i).CommentType, FItable(i).isCaseSensitive)
              If outputtarget = "" Then
                FItable(i).isSubSection = False ' da questo momento abilito la ricerca del target
              Else
                IsSectionInProgress = True
              End If
            End If
          ElseIf Not outputtarget = FItable(i).startSection Then  ' --->  trovata una parte della label sezione
            'FItable(i).startSection = outputtarget  ' cercheremo sulla riga successiva la parte rimanente della label
            isPartialSectionFound = True
            indexPartialSection = i
            wordPartialSection = outputtarget
          End If
        End If
    
      ElseIf Len(FItable(i).endSection) Then  ' se � specificato una stringa di fine sezione, la cerco per disabilitare
                                              ' la ricerca del target
        isPLIcommentOpened = appoCommentOpened
        outputtarget = findTarget_OF(Line, FItable(i).endSection, FItable(i).CommentType, FItable(i).isCaseSensitive)
        If outputtarget = "" Then
          FItable(i).isSubSection = True ' la sezione oggetto della ricerca del target � terminata
        End If
      End If
    Next i
    'Controllo se riga � stata processata per i commenti PLI
    If Not IsLineAnalized Then
      'check comment --> lancio la findTarget per intercettare i commenti (come un lavaggio a vuoto con la lavatrice)
      findTarget_OF Line, "", "PLI", True
    End If
    ' ***************** SCRITTURA SU FILE DI OUTPUT *****************
    If isBUFFERcomplete Then
      Print #FDout, buffer
      'clean varie
      buffer = ""
      before_up = ""
      after_up = ""
      before_down = ""
      after_down = ""
    End If
    '****************************************************************
  Wend
  Close FDin
  Close FDout
  For i = 1 To UBound(FItable)
    If FItable(i).isUnique Then 'se trovato doveva essere cancellato
        If Not FItable(i).target = "" Then 'And Not FItable(i).target = "DCL PLITDLI EXT ENTRY" Then
'IRIS-DB not clear as the encaps anyhow works fine AggLog "[Object: " & filenameI & "] Target not found: " & FItable(i).target & vbCrLf, MadrdF_Incapsulatore.RTErr
        ElseIf Not FItable(i).startSection = "" Then
            AggLog "[Object: " & filenameI & "] Target not found: " & FItable(i).startSection & vbCrLf, MadrdF_Incapsulatore.RTErr
            m_fun.WriteLog "[Object: " & filenameI & "] Target not found: " & FItable(i).startSection & vbCrLf & FItable(i).lines_up, "MadrdM_Incapsulatore"
        End If
    End If
  Next i
  Exit Function
HandleError:
  AggLog "[Object: " & filenameI & ", LineNumber: " & lineNumber & "] error: " & err.Description & vbCrLf, MadrdF_Incapsulatore.RTErr
  'MsgBox ("error: " & err.Description)
  If err.Number = 123 Then 'next token
    Resume Next
  Else
    Close FDin
    Close FDout
  End If
End Function

Function ReplaceElseThen(Line As String) As String
  ReplaceElseThen = Line
  If InStr(Line, " THEN ") > 0 Then
    ReplaceElseThen = Replace(Line, "THEN", "    ")
  ElseIf InStr(Line, " ELSE ") > 0 Then
    ReplaceElseThen = Replace(Line, "ELSE", "    ")
  End If
End Function

Function ModifyLine_OF(Line As String, infoModify As String, pFileName As String, nline As Long, Optional perror As String = "") As String
  Dim poscstart As Integer, poscend As Integer
  Dim strToReplace As String
  Dim xline As String 'IRIS-DB
  
  ModifyLine_OF = Line
  Select Case infoModify
    Case ""
      Exit Function
    Case "CBL", "CBLTAG"
      If Not (Line = "" Or Mid(Line, 7, 1) = "*") Then
        ModifyLine_OF = START_TAG & IIf(Len(START_TAG) < 6, Space(6 - Len(START_TAG)), "") & "*" & Right(ReplaceElseThen(Line), Len(Line) - 7)
      ElseIf Not Mid(Line, 7, 1) = "*" Then
        ModifyLine_OF = START_TAG & IIf(Len(START_TAG) < 6, Space(6 - Len(START_TAG)), "") & "*"
      End If
      'IRIS-DB start
      If Not Mid(Line, 7, 1) = "*" Then
        If Len(ModifyLine_OF) > 72 Then
          xline = "LN" & nline
          ModifyLine_OF = Left(ModifyLine_OF, 72) & xline & Space(8 - Len(xline))
        Else
          ModifyLine_OF = ModifyLine_OF & Space(72 - Len(ModifyLine_OF)) & "LN" & nline
        End If
      End If
      'IRIS-DB end
    Case "PLI_COMMENT"
      ModifyLine_OF = PLICommentLine(Line, pFileName, nline)
      Exit Function
    Case "PLI"
      ' commento completo su linea
      While InStr(Line, "/*") > 0 And InStr(Line, "*/") > 0
        poscstart = InStr(Line, "/*")
        poscend = InStr(Line, "*/")
        If poscend > poscstart Then
          strToReplace = Mid(Line, poscstart, poscend - poscstart + 2)
          Line = Replace(Line, strToReplace, Space(Len(strToReplace)))
        End If
      Wend
      ' sbiancamento IF cond THEN/THEN
      ' la funzione ripulisce la linea, il ritorno (strToReplace) non ci serve
      strToReplace = MadrdM_Incapsulatore.findThen(Line)
      ModifyLine_OF = Line
    Case "XOPTSCBL", "XOPTSPLI"
      ' modifica per eliminare "DLI" dalla riga
      ModifyLine_OF = Elimina_DLI_in_Xopts_OF(Line, infoModify)
      Exit Function
    Case Else
      ' asteriscamento dei parametri nei MAIN PLI
      If Len(infoModify) > 13 Then
        Select Case (Left(infoModify, 14))
          Case "PCBPARAMETERS:"
              Dim error As String
              ModifyLine_OF = Asterisca_Parameters_in_Main_OF(Line, Right(infoModify, Len(infoModify) - 14), error)
              If error = "LENERROR" Then
                AggLog "[Object: " & pFileName & ", LineNumber: " & nline & ", Info: " & infoModify & "] Error: characters after column 72 " & vbCrLf, MadrdF_Incapsulatore.RTErr
                perror = "LENERROR"
              End If
          Case Else
          '**********  TODO : WARNING
        End Select
      Else
          '**********  TODO : WARNING
      End If
  End Select
End Function

Sub CostruisciOperazioneTemplate_OF(xIstr As String, idpgm As Long, pIdOggetto As Long, pRiga As Long, ordPcb As Long)
  Dim tb As Recordset, tb1 As Recordset
  Dim wOpe As String, wOpeS As String, TipIstr As String
  Dim k As Long, K1 As Integer
  Dim wUtil As Boolean, wFlagBoolean As Boolean
  Dim i As Integer, j As Integer
  Dim checkColl As Boolean
  Dim maxId As Long
  Dim wIstr As String, wDecod As String, wIstrS As String, wDecodS As String, pcbDyn As String
  Dim maxPcbDyn As Integer, pcbDynInt As Integer
  Dim wOpeSave As String
  Dim y As Integer
  ' per comporre stringa if dell'incapsulato, versione qual e squal
  Dim IstrIncaps As String, IstrSIncaps As String
  ' contiene la molteplicit� di istruzioni, attinge dalla tabella Istruzioni_Clone
  Dim cloniIstruzione() As String
  Dim cloniSegmenti() As String
  Dim combinazioni As Boolean
  Dim maxLogicOp As Integer
  Dim Operatore As String
  Dim StringToClear As String
  Dim nomeRoutCompleto As String
  Dim templateName As String  'SQ - per la gestione degli errori
  Dim xprogramm As String
  On Error GoTo ErrH
  'AC 02/11/06
  Dim varOp As String, combOp As String, varIstr As String, combIstr As String
  Dim ks As Integer
  Dim varCmd As String, combCmd As String, varSeg As String, combSeg As String
  
  Dim CombPLevel() As String, CombPLevel_s() As String, CombSqual() As String, CombQual() As String, wIstrsOld As String
  
  ' nuove varibili operatore
  Dim CombPLevelOper() As String, ComboPLevelTempOper As String, ComboPLevelAppoOper As String
  Dim bolMoltOpLogic As Boolean, jj As Integer
  
  Dim ComboPLevelTemp As String, ComboPLevelAppo As String, nFBms As Integer, wRecIn As String, jk As Integer
  Dim Rtb As RichTextBox, appocomb As String, appoFinalString As String, checksqual As Boolean, posEndLine As String
  ' tengo traccia di una qualsiasi molteplicit� sui livelli
  Dim moltLevel As Boolean, moltLevelesimo() As Boolean, ij As Integer, moltLevThen As Boolean
 
  'AC 19/06/07  arriva la booleana
  Dim OperatoreLogico As String, ultimoOperatore As Boolean, varLogicop As String, combLogicop As String
  Dim OperatoreLogicoSimb As String, moltBOL() As Boolean, bolmoltBOL As Boolean
  Dim bolMultipleCcod As Boolean
  Dim CCodeSqual() As String, z1 As Integer, numCombinazioni As Integer
  Dim zIstr As String
  
  'AC 29/07/09
  'Dim moltpcb() As String, areMorePCB As Boolean, pj As Integer
  'Dim varpcb As String, combpcb As String
  'ReDim moltpcb(0)
  'areMorePCB = False
  
  '********* CREA MOLTEPLICITA' PCB per test
  'ReDim moltpcb(2)
  '****************************
           
  Set Rtb = MadrdF_Incapsulatore.RTBR
  nFBms = FreeFile
  '---- nuovo template
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVELOPE"
  Open templateName For Input As nFBms
  
  While Not EOF(nFBms)
    If Not ComboPLevelTempOper = "" Then
      ComboPLevelTempOper = ComboPLevelTempOper & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTempOper = ComboPLevelTempOper & wRecIn
  Wend
  Close #nFBms
  '-------------------------------
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVEL"
  Open templateName For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTemp = "" Then
      ComboPLevelTemp = ComboPLevelTemp & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTemp = ComboPLevelTemp & wRecIn
  Wend
  
  jk = 1
  ' AC : wMoltIstr(0), wMoltIstr(2) -> squalificata , wMoltIstr(1),wMoltIstr(3) -> qualificata , se QualAndUnqual tutte e quattro
  
  'Ac per la gestione delle combinazioni
  ' righe = livelli, colonne = max operatori logici
  ' la matrice contiene gli indici degli operatori che per ogni livello e operatore logico
  ' devono essere presi in considerazione per la decodifica
  ' ad ogni ciclo di combinazione si aggiorna la matrice e si crea una nuova decodifica combinando
  ' le info sui vari livelli e operatori logici
  ' la decodifica � inserita nella collection di wMoltIstr
  
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
    If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) > maxLogicOp Then
      maxLogicOp = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef)
    End If
  Next k
  
  ReDim CCodeSqual(0)
  
  ReDim MatriceCombinazioni(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr), maxLogicOp)
  If maxLogicOp > 0 Then
    ReDim MatriceCombinazioniBOL(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr), maxLogicOp - 1)
  End If
  
  ' array combinazioni command code : per ogni livello mi dice
  ' sequenza command code che deve essere considerate
  ReDim CombinazioniComCode(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  ReDim Combinazionisegmenti(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  ReDim moltLevelesimo(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
    For K1 = 1 To maxLogicOp
      MatriceCombinazioni(k, K1) = 1
      If Not K1 = maxLogicOp Then
        MatriceCombinazioniBOL(k, K1) = 1
      End If
    Next K1
  Next k
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
    CombinazioniComCode(k) = 1
    Combinazionisegmenti(k) = 1
  Next k
  CreaArrayCloniIstruzione cloniIstruzione, pIdOggetto, pRiga
  'CreaArrayCloniPCB moltpcb, pIdOggetto, pRiga
  CreaArrayCloniSegmenti cloniSegmenti, MadrdM_Incapsulatore.TabIstr
  
  
  
  'se ho solo due molteplicit� e se ho l'unificazione di GH e G -> normalizzo ad un'unica istruzione
  If UBound(cloniIstruzione) = 1 And Not isUnificaGH Then
    If Replace(cloniIstruzione(0), "GH", "G") = Replace(cloniIstruzione(1), "GH", "G") Then
      ReDim Preserve cloniIstruzione(0)
    End If
  End If
  isMoltIstr = False
  If UBound(cloniIstruzione) > 0 Then
    isMoltIstr = True
  End If
  
  '***** determiniamo se deve essere generata
  '***** sia la versione qualificata che squalificata
  Set tb = m_fun.Open_Recordset("select * from PsDLI_IstrDett_Liv where " & _
                                "IdOggetto =" & pIdOggetto & " and Idpgm = " & idpgm & " and Riga = " & pRiga & " and " & _
                                "Livello = " & UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  If Not tb.EOF Then
    QualAndUnqual = tb!QualAndUnqual
  Else
    QualAndUnqual = False
  End If
  tb.Close
  
  Set wMoltIstr(0) = New Collection
  Set wMoltIstr(1) = New Collection
  Set wMoltIstr(2) = New Collection
  Set wMoltIstr(3) = New Collection
  StringToClear = ""
  ReDim wNomeRout(0)
  
  ' Ciclo esterno su istruzione
  ReDim Preserve CombSqual(0)
  
  'se ho solo due molteplicit� e se ho l'unificazione di GH e G -> normalizzo ad un'unica istruzione
  
  For i = 0 To UBound(cloniIstruzione)
    'DOPCB  ciclo for moltpcb
    'For pj = 0 To UBound(moltpcb)
    If isUnificaGH Then
      xIstr = cloniIstruzione(i)
      zIstr = cloniIstruzione(i)
    Else
      xIstr = Replace(cloniIstruzione(i), "GH", "G")
      zIstr = cloniIstruzione(i)
    End If
    TipIstr = Left(xIstr, 2)
    TipIstr = getTipIstr(xIstr, TipIstr, wUtil)
  
    bolMultipleCcod = CheckCombCCode(xIstr)
    gbIstr = xIstr
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
      CombinazioniComCode(k) = 1
    Next k
  
    'riapplico il calcolo del NomeRout per ogni Molteplicit� di istruzione
    If UBound(MadrdM_Incapsulatore.TabIstr.DBD) > 0 Then
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
        If InStr(MadrdM_Incapsulatore.TabIstr.DBD(1).nome, "RKKUNL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKLUNL", xIstr, SwTp)
        ElseIf InStr(MadrdM_Incapsulatore.TabIstr.DBD(1).nome, "RKGIRL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKOIRL", xIstr, SwTp)
        Else
          nomeRoutine = m_fun.Genera_Nome_Routine(MadrdM_Incapsulatore.TabIstr.DBD(1).nome, xIstr, SwTp)
        End If
      Else
        nomeRoutine = m_fun.Genera_Nome_Routine(MadrdM_Incapsulatore.TabIstr.DBD(1).nome, xIstr, SwTp)
        'aggiornalistaDB (TabIstr.Dbd(1).nome)
      End If
    Else
      nomeRoutine = m_fun.Genera_Nome_Routine("", xIstr, SwTp)
    End If
   
    'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    'stefano: ho dovuta commentarla perch� creava delle istruzioni non valide (AND senza IF) quando
    ' c'� molteplicit� di squalificata insieme alla molteplicit� di funzione; fondamentalmente la molteplicit�
    ' di funzione viene gestita sotto quella di squalificata; dovrebbe essere invertita o creato un vettore per ogni
    ' occorrenza di funzione.
    'If TipIstr <> "GU" And TipIstr <> "GN" And TipIstr <> "GP" Then QualAndUnqual = False
    
    combinazioni = True
    IstrIncaps = ""
    Dcombination = False
    numCombinazioni = 0
    While combinazioni
      'AC 01/02/08
      bolMultipleCcod = CheckDcomb(CombinazioniComCode)
      'AC 25/01/08
      numCombinazioni = numCombinazioni + 1
      For k = 1 To UBound(moltLevelesimo)
        moltLevelesimo(k) = False
      Next k
      moltLevThen = False
      multiOpGen = False
      'AC 02/11/06
      ReDim CombPLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
      ' alberto, sulle istruzioni OPEN e CLSE dava errore per cui l'ho cambiata..
      If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) > 0 Then
        ReDim CombPLevel_s(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) - 1)
      End If
      IstrIncaps = ""
      
      If UBound(cloniIstruzione) > 0 Then
        varIstr = Restituisci_varFunz(pIdOggetto, pRiga)
        combIstr = "'" & zIstr & "'"
      End If
      
      'AC test 29/97/09 TEST TEST
      'varpcb = MadrdM_Incapsulatore.TabIstr.PCB
      'If UBound(moltpcb) > 0 Then
      '  areMorePCB = True
      '  combpcb = moltpcb(pj)
      'End If
      '****************
      If isOldIncapsMethod Then
        wOpe = "<pgm>" & nomeRoutine & "</pgm><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
      Else
        If UBound(MadrdM_Incapsulatore.TabIstr.DBD) Then
          wOpe = "<dbd>" & MadrdM_Incapsulatore.TabIstr.DBD(1).nome & "</dbd><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
        Else
          wOpe = "<istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
        End If
      End If
      'AC 29/07/09  in test
      'wOpe = wOpe & "<numpcb>" & CStr(pj) & "</numpcb>"
      '***********************
     
      'AC 24/10/08
      'SENSEG
      If Not MadrdM_Incapsulatore.TabIstr.SenSeg = "" Then
        If MadrdM_Incapsulatore.TabIstr.SenSeg = "ALL" Then
          wOpe = wOpe & "<sseg></sseg>"
        Else
          wOpe = wOpe & "<sseg>" & MadrdM_Incapsulatore.TabIstr.SenSeg & "</sseg>"
        End If
      End If
     
      For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)   ' ciclo su livelli
        ComboPLevelAppo = ComboPLevelTemp
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 28-07-06
        ' OK: REPL CON SSA QUALIFICATA DA RETURN CODE "AJ"
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 19-07-07
        ' REPL CON PATH-CALL (C.C. "D")!
        'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
          If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm = -1 Then
            wOpe = wOpe & "<level" & k & "><segm>" & "DYNAMIC" & "</segm>"
          Else
            wOpe = wOpe & "<level" & k & "><segm>" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "</segm>"
            If NumToken(cloniSegmenti(k)) > 1 Then
              'varseg = MadrdM_Incapsulatore.TabIstr.BlkIstr(K).ssaName & "(1:8)"
              combSeg = "'" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "'"
              varSeg = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, "1", CStr(Len(combSeg) - 2), TipoFile, "SSA", pIdOggetto) '"8"
              moltLevelesimo(k) = True
            Else
              varSeg = ""
            End If
            aggiornalistaSeg (MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment)
          End If
        Else
          If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
            If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm = -1 Then
              wOpe = wOpe & "<level1><segm>" & "DYNAMIC" & "</segm>"
            Else
              wOpe = wOpe & "<level1><segm>" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "</segm>"
              If NumToken(cloniSegmenti(k)) > 1 Then
                'varseg = MadrdM_Incapsulatore.TabIstr.BlkIstr(K).ssaName & "(1:8)"
                combSeg = "'" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "'"
                varSeg = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, "1", CStr(Len(combSeg) - 2), TipoFile, "SSA", pIdOggetto) '"8"
                moltLevelesimo(k) = True
              Else
                varSeg = ""
              End If
              aggiornalistaSeg (MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment)
            End If
          End If
        End If
        
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "POS" Then 'IRIS-DB
          If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record <> "" Then
            If Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment) <> "" Then
              wOpe = wOpe & "<area>" & "AREA" & "</area>"
            End If
          End If
        End If
        
        'AC 10/04/2007 spostata per non sommare a wopes la <ccode> del wope
        If QualAndUnqual And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
          wOpeS = wOpe   ' allineiamo  la wOpeS  alla wOpe che si differenziano solo per ultimo livello
          For ks = 1 To k - 1
            CombPLevel_s(ks) = CombPLevel(ks)
          Next ks
          ' allineiamo le stringhe
        End If
                            
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
          If Trim(Replace(Replace(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod), "*", ""), "-", "")) <> "" Then
            If Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment) <> "" Then
              wOpe = wOpe & "<ccode>" & Trim(Replace(Replace(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod), "*", ""), "-", "")) & "</ccode>"
            End If
          End If
        End If
                
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB ' "ISRT" AC 30/07/07
          If NumToken(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search) > 1 Then
            'varcmd = MadrdM_Incapsulatore.TabIstr.BlkIstr(K).ssaName & "(9:" & Len(TokenIesimo(MadrdM_Incapsulatore.TabIstr.BlkIstr(K).Search, CombinazioniComCode(K))) & ")"
            varCmd = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, "9", Len(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod)), TipoFile, "SSA", pIdOggetto)
            combCmd = "'" & TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod) & "'"
            If bolMultipleCcod Then
              moltLevelesimo(k) = True
            End If
          Else
            varCmd = ""
            bolMultipleCcod = False
          End If
        End If
        '*********************************
                      
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' QUALIFICAZIONE:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' AC se entrambe le versione wOpe � quella qualificata e wOpeS quella
        ' squalificata, altrimenti se unica sempre in wOpe
        
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB   ' AC 230707 (xIstr = "ISRT" And K < UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
          If QualAndUnqual And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
            'parte SQUALIFICATA
            ' la wOpeS si ferma qui, wOpeS e wOpe differiscono solo per questo livello
           
            'AC 04/01/08
            ' gestione combinazioni command code per squalificata
            ' ho gi� calcolato combinazione ccode
            ' non essendo abbinata ad altre molteplicit� la stessa combinazione
            ' potrebbe ripresentarsi ciclicamente -> uso array CCodeSqual

            If bolMultipleCcod Then
              If Not (InStr(combCmd, "-") > 0 Or InStr(combCmd, " ") > 0) Then
                For z1 = 1 To UBound(CCodeSqual)
                  If CCodeSqual(z1) = combCmd Then
                    Exit For
                  End If
                Next z1
                If z1 > UBound(CCodeSqual) Then
                  ReDim CCodeSqual(UBound(CCodeSqual) + 1)
                  CCodeSqual(UBound(CCodeSqual)) = combCmd
                  Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVEL"
                  ComboPLevelAppo = Rtb.text
                  ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", " (" & varCmd)
                  ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd & " )")
                  If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                    ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
                  Else
                    ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
                  End If
                  If QualAndUnqual Then
                    ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
                  End If
                  wOpeS = wOpe ' attacco anche il pezzo relativo a ccode su ultimo livello
                  ReDim CombPLevel_s(k)
                  CombPLevel_s(k) = ComboPLevelAppo
                End If
              End If
            End If
            '__________________________________________
            'parte QUALIFICATA
            wFlagBoolean = False
            ' determino se su livelli precedenti ho molteplicit� operatore
            If k > 1 Then
              For ij = 1 To k - 1
                If moltLevelesimo(ij) = True Then
                  moltLevel = True
                  Exit For
                End If
              Next ij
            End If
            
            ' ------------CICLO SU OPERATORI LOGICI---------------
            bolMoltOpLogic = False
            bolmoltBOL = False
            ultimoOperatore = False
            ReDim MoltOpLogic(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))
            If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) - 1)
            End If
            ReDim CombPLevelOper(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))
            For K1 = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef)
              'AC 12/07
              ' ripulisco la maschera del template COMBOPLEVELOPE
              ComboPLevelAppoOper = ComboPLevelTempOper
              ComboPLevelAppo = ComboPLevelTemp
              If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                wOpe = wOpe & "<key>" & MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                Operatore = TokenIesimo(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                If multiOp Then
                  varOp = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, CStr(Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                  ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                  If TipoFile = "PLI" Then
                    combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                           & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                           & "   " & varOp & " = IRIS_" & Operatore & "3"
                  Else
                    combOp = "REV" & Operatore & "1" & vbCrLf _
                           & "    " & Space(Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                           & "    " & Space(Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                  End If
                  moltLevelesimo(k) = True
                  MoltOpLogic(K1) = True
                Else
                  varOp = ""
                End If
                If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                  OperatoreLogicoSimb = TokenIesimo(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, MatriceCombinazioniBOL(k, K1))
                  OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                  If multiOp Then
                    varLogicop = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, CStr(Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                    combLogicop = "'" & OperatoreLogicoSimb & "'"
                    moltLevelesimo(k) = True
                    moltBOL(K1) = True
                  Else
                    varLogicop = ""
                  End If
                  If Not OperatoreLogico = ")" Then
                    wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                    wFlagBoolean = True
                  Else
                    ultimoOperatore = True
                  End If
                Else
                  varLogicop = ""
                End If
                'AC 03/07/06
                'ultimo
                  
                bolMoltOpLogic = False
                bolmoltBOL = False
                If K1 > 1 Then
                  For ij = 1 To K1 - 1
                    If MoltOpLogic(ij) = True Then
                      bolMoltOpLogic = True
                      Exit For
                    End If
                  Next ij
                  For ij = 1 To K1 - 1
                    If moltBOL(ij) = True Then
                      bolmoltBOL = True
                      Exit For
                    End If
                  Next ij
                End If
                '---------- molteplicit� operatore --------
                If Not varOp = "" Then
                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", "(" & varOp)
                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp & " )")
                 'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                 If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                 Else
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                 End If
'                 If QualAndUnqual Then
'                   ComboPLevelAppoOper = Replace(ComboPLevelAppo, "<I%IND-3>", "")
'                 End If
'                 CombPLevelOper(k1) = ComboPLevelAppoOper
'                Else
'                 CombPLevelOper(k1) = ComboPLevelAppoOper
                 End If
                ' booleano
                '---------- molteplicit� booleana --------
                If Not varLogicop = "" Then
                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", "(" & varLogicop)
                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop & " )")
                 'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                 If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                 Else
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                 End If
                End If
                
                If QualAndUnqual Then
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                End If
                CombPLevelOper(K1) = ComboPLevelAppoOper
                
             '-------------------------------------------
               End If
               ' nel caso di parentesi sull'operatore logico interrompo il ciclo
               If ultimoOperatore Then Exit For
             Next K1
             
             '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
             
             Rtb.text = ComboPLevelAppo
             For jj = 1 To UBound(CombPLevelOper)
              MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
             Next jj
             ComboPLevelAppo = Rtb.text
             
             If Not varCmd = "" And bolMultipleCcod Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", "(" & varCmd)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd & " )")
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
            If Not varSeg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", "(" & varSeg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg & " )")
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
         Else  ' o la versione squalificata o quella squalificata
         
           ' AC il campo parentesi e la trasformazione in stringa del booleano Qualificazione
           If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Parentesi <> "(" Then ' Or Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).SsaName) = "<none>" Then
             OnlySqual = True
             ReDim CombPLevelOper(0)
           Else   ' Qualificata
             OnlyQual = True
             wFlagBoolean = False
             If k > 1 Then
              For ij = 1 To k - 1
               If moltLevelesimo(ij) = True Then
                 moltLevel = True
                 Exit For
               End If
              Next ij
             End If
             ' ----- CICLO OPERATORI LOGICI
             bolmoltBOL = False
             ultimoOperatore = False
             bolMoltOpLogic = False
             ReDim MoltOpLogic(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))
             If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) - 1)
             End If
             ReDim CombPLevelOper(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))
             For K1 = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef)
               ComboPLevelAppoOper = ComboPLevelTempOper
               If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                 wOpe = wOpe & "<key>" & MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                 Operatore = TokenIesimo(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                 wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                 If multiOp Then  ' AC 020807 And Not TipIstr = "UP"
                  'varop = MadrdM_Incapsulatore.TabIstr.BlkIstr(K).ssaName & "(" & Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
                  varOp = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, CStr(Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                  ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                  If TipoFile = "PLI" Then
                    combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "3"
                  Else
                    combOp = "REV" & Operatore & "1" & vbCrLf _
                    & "    " & Space(Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                    & "    " & Space(Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                  End If
                  moltLevelesimo(k) = True
                  MoltOpLogic(K1) = True
                 Else
                    varOp = ""
                 End If
                 If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                    OperatoreLogicoSimb = TokenIesimo(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, MatriceCombinazioniBOL(k, K1))
                    OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                    If multiOp Then    ' AC 020807 And Not TipIstr = "UP"
                      varLogicop = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, CStr(Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                      combLogicop = "'" & OperatoreLogicoSimb & "'"
                      moltLevelesimo(k) = True
                      moltBOL(K1) = True
                    Else
                      varLogicop = ""
                    End If
                    If Not OperatoreLogico = ")" Then
                      wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                      wFlagBoolean = True
                    Else
                      ultimoOperatore = True
                    End If
                 Else
                  varLogicop = ""
                 End If
                 
                  '------------------------------------------------------
                  bolMoltOpLogic = False
                  bolmoltBOL = False
                  If K1 > 1 Then
                   For ij = 1 To K1 - 1
                     If MoltOpLogic(ij) = True Then
                       bolMoltOpLogic = True
                       Exit For
                     End If
                   Next
                   For ij = 1 To K1 - 1
                    If moltBOL(ij) = True Then
                      bolmoltBOL = True
                      Exit For
                    End If
                    Next
                  End If
                  If Not varOp = "" Then
                    ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", "(" & varOp)
                    ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp & " )")
                    If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                    Else
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                    End If
'                    If QualAndUnqual Then
'                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
'                    End If
'                    CombPLevelOper(k1) = ComboPLevelAppoOper
'                  Else
'                      CombPLevelOper(k1) = ComboPLevelAppoOper
                  End If
                  '---------- molteplicit� booleana --------
                  If Not varLogicop = "" Then
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", "(" & varLogicop)
                   ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop & " )")
                   'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                   If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                   Else
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                   End If
                  End If
                  
                  If QualAndUnqual Then
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                  End If
                  CombPLevelOper(K1) = ComboPLevelAppoOper
          '-------------------------------------------------------
               End If
                If ultimoOperatore Then Exit For
             Next K1
          End If '-- squal o qual
          
          '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
          Rtb.text = ComboPLevelAppo
          For jj = 1 To UBound(CombPLevelOper)
           MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
          Next
          ComboPLevelAppo = Rtb.text
          
          If Not varCmd = "" And bolMultipleCcod Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", "(" & varCmd)
            ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd & " )")
            If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
              ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
            Else
              ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
            End If
            If QualAndUnqual Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
            End If
            CombPLevel(k) = ComboPLevelAppo
          Else
            CombPLevel(k) = ComboPLevelAppo
          End If
          If Not varSeg = "" Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", "(" & varSeg)
            ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg & " )")
            If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
              ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
            Else
              ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
            End If
            If QualAndUnqual Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
            End If
            CombPLevel(k) = ComboPLevelAppo
          Else
            CombPLevel(k) = ComboPLevelAppo
          End If
        End If   '-- una sola versione o due versioni
       End If
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        ' SQ 19-07-07
        ' REPL CON PATH-CALL (C.C. "D")!
        'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
         wOpe = wOpe & "</level" & k & ">"
         If QualAndUnqual And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
           wOpeS = wOpeS & "</level" & k & ">"
         End If
        Else
         If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
           wOpe = wOpe & "</level1>"
           If QualAndUnqual And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
             wOpeS = wOpeS & "</level1>"
           End If
         End If
        End If
     Next k   ' FINE CICLO LIVELLI
     
     wOpe = wOpe & "</livelli>"
     
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
       If Trim(MadrdM_Incapsulatore.TabIstr.keyfeedback) <> "" Then
         wOpe = wOpe & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
       End If
     End If
     
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
       If Len(MadrdM_Incapsulatore.TabIstr.procSeq) Then
         wOpe = wOpe & "<procseq>" & MadrdM_Incapsulatore.TabIstr.procSeq & "</procseq>"
       End If
     End If

     If xIstr = "OPEN" Then
       If Len(MadrdM_Incapsulatore.TabIstr.procOpt) Then
         wOpe = wOpe & "<procopt>" & MadrdM_Incapsulatore.TabIstr.procOpt & "</procopt>"
       End If
     End If
     Dim PcbSeqVacante As Integer, indicevacante As Integer
    'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
    ' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpe
      If ordPcb = 0 Then
        pcbDyn = ""
        maxPcbDyn = 0
     '________________________________________________________________
     Else
        PcbSeqVacante = 0
        For k = 1 To UBound(OpeIstrRt)
           If wOpe = OpeIstrRt(k).Decodifica Then
              If OpeIstrRt(k).ordPcb = ordPcb Then
                 'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
                 'verifico se � la prima
                 pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
                 pcbDynInt = OpeIstrRt(k).pcbDyn
                 maxPcbDyn = 0
                 Exit For
               '___________________________
               ' nuovo ramo: se trovo un pcbseq occupato da istruzione con pcb = 0 lo registro
               ' come possibile pcbseq utile nel caso non si trovi un corrispondente
               ' per questo pcb
               
               ElseIf OpeIstrRt(k).ordPcb = 0 Then
                  'AC 28/05/10
                  ' prendiano questo pcbDyn, posto libero se non si trova corrispondente
                  PcbSeqVacante = OpeIstrRt(k).pcbDyn
                  indicevacante = k
               '___________________________
              Else
                 If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
                 'trovata un'istruzione identica, ma con diverso pcb
                    maxPcbDyn = OpeIstrRt(k).pcbDyn
                 End If
              End If
           End If
        Next k
        'AC nel caso di fine ciclo senza un corrispondente
        'se c'era un pcbseq assegnato al pcb = 0 me ne approprio
        If k = UBound(OpeIstrRt) + 1 And PcbSeqVacante > 0 Then
          maxPcbDyn = 0
          pcbDyn = Format(PcbSeqVacante, "000")
          OpeIstrRt(indicevacante).ordPcb = ordPcb
        End If
     End If
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
     If maxPcbDyn > 0 Then
        pcbDyn = Format(maxPcbDyn + 1, "000")
        pcbDynInt = maxPcbDyn + 1
     End If
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
      ' aggiunge il pcb dinamico
      If Len(pcbDyn) Then
        wOpe = wOpe & "<pcbseq>" & pcbDyn & "</pcbseq>"
        'AggLog "[Object: " & pIdOggetto & "] New Instruction: <pcbseq>" & pcbDyn & "</pcbseq>", MadrdF_Incapsulatore.RTErr
      Else
        ' se � la prima istruzione di quel tipo oppure AC 28/05/10 ha ordpcb = 0 (cio� pcbDyn = "")
        wOpe = wOpe & "<pcbseq>" & "001" & "</pcbseq>"
        pcbDynInt = 1
      End If
    End If
   
    wDecod = wOpe
    Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpe & "'")
    If tb.EOF Then
       Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
         maxId = 1
       Else
         maxId = tb1(0) + 1
       End If
       tb.AddNew
       'SQ tmp:
       'm_fun.WriteLog "##" & wOpe
       tb!IdIstruzione = maxId
       tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
       wIstr = tb!Codifica
       tb!Decodifica = wOpe
       tb!ImsDB = True
       tb!ImsDC = False
       tb!CICS = SwTp
   'stefano: routine multiple
       nomeRoutCompleto = Genera_Counter(nomeRoutine)
       tb!nomeRout = nomeRoutCompleto
       tb.Update
       tb.Close
      ' Mauro 28/04/2009
''    'pcb dinamico
''       k = UBound(OpeIstrRt) + 1
''       ReDim Preserve OpeIstrRt(k)
''       OpeIstrRt(k).Decodifica = wOpeSave
''       OpeIstrRt(k).Istruzione = wIstr
''       OpeIstrRt(k).ordPcb = ordPcb
''       OpeIstrRt(k).pcbDyn = pcbDynInt
     Else
       wIstr = tb!Codifica
       nomeRoutCompleto = tb!nomeRout
       ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
       nomeRoutine = tb!nomeRout
       tb.Close
     End If
     
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     'SQ CPY-ISTR
     'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
     Set tb = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrCodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
     If tb.EOF Then
       'SQ 16-11-06 - EMBEDDED
       'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & nomeroutcompleto & "')"
       'SQ CPY-ISTR
       'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeroutcompleto) & "')"
       m_fun.FnConnection.Execute "INSERT INTO PsDLI_IstrCodifica VALUES(" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeRoutCompleto) & "')"
       ' Mauro 28/04/2009 : pcb dinamico
       k = UBound(OpeIstrRt) + 1
       ReDim Preserve OpeIstrRt(k)
       OpeIstrRt(k).Decodifica = wOpeSave
       OpeIstrRt(k).Istruzione = wIstr
       OpeIstrRt(k).ordPcb = ordPcb
       OpeIstrRt(k).pcbDyn = pcbDynInt
     End If
     tb.Close
     If QualAndUnqual Then
       wOpeS = wOpeS & "</livelli>"
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
      If Trim(MadrdM_Incapsulatore.TabIstr.keyfeedback) <> "" Then
        wOpeS = wOpeS & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
      End If
'procseq: sarebbe meglio metterlo in MadrdM_Incapsulatore.TabIstr, ma per ora facciamo cos� (procseq
' ci servir� anche nella generazione routine
      If UBound(MadrdM_Incapsulatore.TabIstr.psb) Then
         If Len(MadrdM_Incapsulatore.TabIstr.procSeq) Then
             wOpeS = wOpeS & "<procseq>" & MadrdM_Incapsulatore.TabIstr.procSeq & "</procseq>"
         End If
         If Len(MadrdM_Incapsulatore.TabIstr.procOpt) And xIstr = "OPEN" Then
             wOpeS = wOpeS & "<procopt>" & MadrdM_Incapsulatore.TabIstr.procOpt & "</procopt>"
         End If
      End If
     End If
     
    'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
    ' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpeS
     'AC 28/05/10   pcb = 0 ---->>>  pcbseq = 1 ______________________
     If ordPcb = 0 Then
        pcbDyn = ""
        maxPcbDyn = 0
     '________________________________________________________________
     Else
        PcbSeqVacante = 0
        For k = 1 To UBound(OpeIstrRt)
            If wOpeS = OpeIstrRt(k).Decodifica Then
               If OpeIstrRt(k).ordPcb = ordPcb Then
                  'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
                  'verifico se � la prima
                  pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
                  pcbDynInt = OpeIstrRt(k).pcbDyn
                  maxPcbDyn = 0
                  Exit For
               '___________________________
               ' nuovo ramo: se trovo un pcbseq occupato da istruzione con pcb = 0 lo registro
               ' come possibile pcbseq utile nel caso non si trovi un corrispondente
               ' per questo pcb
               
               ElseIf OpeIstrRt(k).ordPcb = 0 Then
                  'AC 28/05/10
                  ' prendiano questo pcbDyn, posto libero se non si trova corrispondente
                  PcbSeqVacante = OpeIstrRt(k).pcbDyn
                  indicevacante = k
               '___________________________
               Else
                  If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
                  'trovata un'istruzione identica, ma con diverso pcb
                     maxPcbDyn = OpeIstrRt(k).pcbDyn
                  End If
               End If
            End If
        Next k
     End If
     
     'AC nel caso di fine ciclo senza un corrispondente
     'se c'era un pcbseq assegnato al pcb = 0 me ne approprio
     If k = UBound(OpeIstrRt) + 1 And PcbSeqVacante > 0 Then
        maxPcbDyn = 0
        pcbDyn = Format(PcbSeqVacante, "000")
        OpeIstrRt(indicevacante).ordPcb = ordPcb
     End If
     
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
     If maxPcbDyn > 0 Then
        pcbDyn = Format(maxPcbDyn + 1, "000")
        pcbDynInt = maxPcbDyn + 1
     End If
     ' aggiunge il pcb dinamico
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
      If Len(pcbDyn) Then
        wOpeS = wOpeS & "<pcbseq>" & pcbDyn & "</pcbseq>"
        'AggLog "[Object: " & pIdOggetto & "] New Instruction: <pcbseq>" & pcbDyn & "</pcbseq>", MadrdF_Incapsulatore.RTErr
      Else
        ' se � la prima istruzione di quel tipo oppure AC 28/05/10 ha ordpcb = 0 (cio� pcbDyn = "")
        wOpeS = wOpeS & "<pcbseq>" & "001" & "</pcbseq>"
        pcbDynInt = 1
      End If
     End If
     
     wDecodS = wOpeS
     Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpeS & "'")
     
     If tb.EOF Then
       Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
          maxId = 1
       Else
          maxId = tb1(0) + 1
       End If
       tb.AddNew
       tb!IdIstruzione = maxId
       tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
       wIstrS = tb!Codifica
       tb!Decodifica = wOpeS
       tb!ImsDB = True
       tb!ImsDC = False
       tb!CICS = SwTp
   'stefano: routine multiple
       nomeRoutCompleto = Genera_Counter(nomeRoutine)
       tb!nomeRout = nomeRoutCompleto
       tb.Update
       tb.Close
       ' Mauro 28/04/2009
''    'pcb dinamico
''       k = UBound(OpeIstrRt) + 1
''       ReDim Preserve OpeIstrRt(k)
''       OpeIstrRt(k).Decodifica = wOpeSave
''       OpeIstrRt(k).Istruzione = wIstr
''       OpeIstrRt(k).ordPcb = ordPcb
''       OpeIstrRt(k).pcbDyn = pcbDynInt
     Else
       wIstrS = tb!Codifica
       nomeRoutCompleto = tb!nomeRout
       ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
       nomeRoutine = tb!nomeRout
       tb.Close
     End If
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     'virgilio CPY-ISTR
     'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
     Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
     If tb.EOF Then
       'virgilio CPY-ISTR
       'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeroutcompleto & "')"
       m_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeRoutCompleto & "')"
       ' Mauro 28/04/2009 : pcb dinamico
       k = UBound(OpeIstrRt) + 1
       ReDim Preserve OpeIstrRt(k)
       OpeIstrRt(k).Decodifica = wOpeSave
       OpeIstrRt(k).Istruzione = wIstr
       OpeIstrRt(k).ordPcb = ordPcb
       OpeIstrRt(k).pcbDyn = pcbDynInt
     End If
     tb.Close
    End If
    'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    'BACO: per ora non viene gestita la molteplicit� sulla ISRT per i livelli qualificati
    ' ma in realt� non ha senso (di solito l'inserimento viene fatto qualificando un
    ' livello precedente per uguale, in modo da puntare un solo parent
    
    'AC 22/02/10 rimosse limitazioni REPL DLET
    'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
     combinazioni = AggiornaCombinazione(maxLogicOp)
    'Else
     'combinazioni = False
    'End If
    
    moltLevel = False
     ' aggiornamento collection
     If Not StringToClear = "" Then
      IstrIncaps = Replace(IstrIncaps, StringToClear, "")
      IstrSIncaps = Replace(IstrSIncaps, StringToClear, "")
     End If
     
     If QualAndUnqual Then
       ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
       checkColl = True
       
       For j = 1 To wMoltIstr(3).Count
        If wMoltIstr(3).item(j) = wIstr Then
          checkColl = False
          Exit For
        End If
       Next j
       If checkColl Then
        wMoltIstr(1).Add IstrIncaps, wDecod
        wMoltIstr(3).Add wIstr, wDecod
        aggiornalistaCodOp (wIstr)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeRoutCompleto
       End If
       
       ' 0 e 2  da wOpeS, indice ks dell'array e da IstrSIncaps
       checkColl = True
       checksqual = True
       For j = 1 To wMoltIstr(2).Count
'        If wMoltIstr(2).Item(j) = OpeIstrRt(ks).Istruzione Then
        If wMoltIstr(2).item(j) = wIstrS Then
          checkColl = False
          checksqual = False
          Exit For
        End If
       Next j
       If checkColl Then
        wMoltIstr(0).Add IstrSIncaps, wDecod
        wMoltIstr(2).Add wIstrS, wDecodS
        aggiornalistaCodOp (wIstrS)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeRoutCompleto
       End If
     Else
         ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
         checkColl = True
         For j = 1 To wMoltIstr(3).Count
'          If wMoltIstr(3).Item(j) = OpeIstrRt(k).Istruzione Then
          If wMoltIstr(3).item(j) = wIstr Then
            checkColl = False
            Exit For
          End If
         Next
         If checkColl Then
          wMoltIstr(1).Add IstrIncaps, wDecod
          wMoltIstr(3).Add wIstr, wDecod
          aggiornalistaCodOp (wIstr)
          y = y + 1
          ReDim Preserve wNomeRout(y)
          wNomeRout(y) = nomeRoutCompleto
         End If
       
     End If
     
     ' livello COMBQUAL -- costruisce elemento array combqual
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
      Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL"
      ' nel caso di linguaggi con then (vedi PLI), sui template gestiamo la riga dello then con il tag <then>
      ' negli altri casi l'if sotto � ininfluente
      For ij = 1 To UBound(moltLevelesimo)
        If moltLevelesimo(ij) = True Then
          moltLevThen = True
          Exit For
        End If
      Next
      If QualAndUnqual Or UBound(cloniIstruzione) > 0 Or moltLevThen Then  'Or areMorePCB
        MadrdM_GenRout.changeTag Rtb, "<then>", ""
      End If
      If QualAndUnqual Then
        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
      Else
        ' gestisco indentazione move
        If multiOpGen Or UBound(cloniIstruzione) > 0 Then   'Or areMorePCB
          MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        End If
      End If
      
      'if or end per l'istruzione e relativo end if di chiusura
      If QualAndUnqual Then
        MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), 1)
        MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 1)
        'in test AC 29/07/09
        'MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<P%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<P%"), 1)
      Else
        'in test AC 29/07/09 **********
        'If UBound(cloniIstruzione) > 0 Then  'metto and per il pcb
        '    MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<P%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<P%"), 1)
        'Else 'metto IF per il pcb
        '    MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<P%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<P%"), 2)
        'End If
        '*****************************
        MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), 2)
        ' end if
        ' AC 18/06/07  nel caso che le molteplicit� siano ignorate (vedi ISRT)
        ' non devo lasciare l'end if finale
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB  'AC 200707
          If multiOpGen Or UBound(cloniIstruzione) > 0 Then 'Or areMorePCB
            MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 2)
          Else
             MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 1)
          End If
        ElseIf UBound(cloniIstruzione) > 0 Then  'Or areMorePCB ' potremmo avere molteplicit� istr con istr diverse da GU,GN e GP
          MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 2)
        Else
          MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 1)
        End If
      End If
      
      If moltLevThen Or numCombinazioni = 1 Then  'AC 25/01/08 dovrebbe gestire la mancanza di molteplicit�
      
        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
        
        'AC 29/07/09
        'MadrdM_GenRout.changeTag Rtb, "<VARPCB>", varpcb, True
        'MadrdM_GenRout.changeTag Rtb, "<COMBPCB>", combpcb, True
        
        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPERB>", Replace(wIstr, ".", "")
         'AC 13/05/10 _________________________________
        If isMoltIstr Or isMoltPcb Then
            If Len(nomeRoutine) < 8 Then
                xprogramm = Space(8 - Len(nomeRoutine))
            Else
                xprogramm = ""
                If InStr(m_fun.FnNomeDB, "prj-GRZ") Then
                   nomeRoutine = IIf(Len(nomeRoutine) > 7, Left(nomeRoutine, 7), nomeRoutine)
                End If
            End If
            MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & xprogramm
        End If
        '_____________________________________________
      
      End If
      
      If EmbeddedRoutine Then
        MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
      Else
        MadrdM_GenRout.changeTag Rtb, "<standard>", ""
      End If
      
      
      For k = 1 To UBound(CombPLevel)
        MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
      Next k
      
      appocomb = Rtb.text
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFQUAL"
      Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFQUAL"
      MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
      ReDim Preserve CombQual(jk)
      CombQual(jk) = Rtb.text
      
      If QualAndUnqual And checksqual Then ' due parti, la seconda � squalificata
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
        Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL"
        MadrdM_GenRout.changeTag Rtb, "<then>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), 1)
        MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 1)
        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
        'AC 29/07/09
        'MadrdM_GenRout.changeTag Rtb, "<VARPCB>", varpcb, True
        'MadrdM_GenRout.changeTag Rtb, "<COMBPCB>", combpcb, True
        
        'AC 13/05/10 _________________________________
        If isMoltIstr Or isMoltPcb Then
            If Len(nomeRoutine) < 8 Then
                xprogramm = Space(8 - Len(nomeRoutine))
            Else
                xprogramm = ""
                If InStr(m_fun.FnNomeDB, "prj-GRZ") Then
                   nomeRoutine = IIf(Len(nomeRoutine) > 7, Left(nomeRoutine, 7), nomeRoutine)
                End If
            End If
            MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & xprogramm
         End If
        '_____________________________________________
        
        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstrS
        wIstrsOld = wIstrS
        If EmbeddedRoutine Then
          MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
        Else
          MadrdM_GenRout.changeTag Rtb, "<standard>", ""
        End If
        For k = 1 To UBound(CombPLevel_s)
          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel_s(k), True
        Next k
        appocomb = Rtb.text
        
        Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFSQUAL"
        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
        ReDim Preserve CombSqual(UBound(CombSqual) + 1)
        CombSqual(UBound(CombSqual)) = Rtb.text
      ElseIf Not QualAndUnqual Then

      End If
      jk = jk + 1
      '**********
   Wend
  'TO DOPCB  ciclo for moltpcb end
  'Next pj 'ciclo su molteplicit� pcb
  Next i  'ciclo su molteplicit� istruzioni
  'sostituzioni nel file principale
    
  'stefanopul
  'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\MAINCOMB"
  Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\MAINCOMB"
  'AC 20/03/09
  Dim dataType As String
  If Left(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName, 1) = "'" And _
     Right(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName, 1) = "'" Then
    dataType = "X"
  Else
    getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName), pIdOggetto, 0, dataType, "", -1
  End If
   MadrdM_GenRout.changeTag Rtb, "<SSA-INSPECT>", "SUBSTR(" & IIf(dataType = "X", "", "IRIS_") & _
                      MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName & ",9," & _
                      Len(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Search, 1, bolMultipleCcod)) + 1 & ")"
   MadrdM_GenRout.changeTag Rtb, "<LENINSPECT>", Len(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Search, 1, bolMultipleCcod)) + 1
   For i = 1 To UBound(CombQual)
     MadrdM_GenRout.changeTag Rtb, "<<IFQUAL(i)>>", CombQual(i)
   Next i
   If QualAndUnqual Then
    For i = 1 To UBound(CombSqual)
      MadrdM_GenRout.changeTag Rtb, "<<IFSQUAL(i)>>", CombSqual(i)
    Next i
    MadrdM_GenRout.changeTag Rtb, "<condsqual>", "" ' altrimenti istr corrispondenti eliminate
   End If
   appoFinalString = Rtb.text
  
   While InStr(appoFinalString, vbCrLf) > 0
     posEndLine = InStr(1, appoFinalString, vbCrLf)
     insertLineOF Testo, Left(appoFinalString, posEndLine - 1), pIdOggetto, indentkey
     appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
   Wend
   If Not Trim(appoFinalString) = "" Then
     insertLineOF Testo, appoFinalString, pIdOggetto, indentkey
   End If
  Close #nFBms
  
  Exit Sub
ErrH:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & pIdOggetto & "] Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & pIdOggetto & "] unexpected error on 'CostruisciOperazioneTemplate'.", MadrdF_Incapsulatore.RTErr
  End If
  'Resume Next
End Sub

Function CostruisciOperazioneTemplateOF_EXEC(xIstr As String, Index As Integer, idpgm As Long, pIdOggetto As Long, pRiga As Long, ordPcb As Long)
  Dim tb As Recordset, tb1 As Recordset
  Dim wOpe As String, wOpeS As String, TipIstr As String
  Dim k As Long
  Dim xope As String
  Dim K1 As Integer
  Dim wUtil As Boolean, wFlagBoolean As Boolean
  Dim i As Integer, j As Integer
  Dim checkColl As Boolean
  Dim extraIndent As Integer
  Dim maxId As Long
  Dim wIstr, wDecod As String, wIstrS As String, wDecodS As String, pcbDyn As String
  Dim maxPcbDyn As Integer, pcbDynInt As Integer
  Dim wOpeSave As String
  Dim y As Integer
  ' per comporre stringa if dell'incapsulato, versione qual e squal
  Dim IstrIncaps As String, IstrSIncaps As String, StrAnd As String
  ' contiene la molteplicit� di istruzioni, attinge dalla tabella Istruzioni_Clone
  Dim cloniIstruzione() As String
  Dim cloniSegmenti() As String
  Dim combinazioni As Boolean
  Dim maxLogicOp As Integer, maxBoolean As Integer
  Dim Operatore As String
  Dim StringToClear As String
  Dim ifAnd As String
  Dim nomeRoutCompleto As String, wPath As String
  Dim zIstr As String
  On Error GoTo ErrH
  'AC 02/11/06
  
  Dim varOp As String, combOp As String, varIstr As String, combIstr As String
  Dim varop_s As String, combop_s As String, ks As Integer
  Dim varCmd As String, combCmd As String, varSeg As String, combSeg As String
  Dim varcmd_s As String, combcmd_s As String
  
  Dim CombPLevel() As String, CombPLevel_s() As String, CombSqual() As String, CombQual() As String, wIstrsOld As String
  
  ' nuove varibili operatore
  Dim CombPLevelOper() As String, ComboPLevelTempOper As String, ComboPLevelAppoOper As String
  Dim bolMoltOpLogic As Boolean, jj As Integer
  
  Dim ComboPLevelTemp As String, ComboPLevelAppo As String, nFBms As Integer, wRecIn As String, jk As Integer
  Dim Rtb  As RichTextBox, appocomb As String, appoFinalString As String, checksqual As Boolean, posEndLine As String
  ' tango traccia di una qualsiasi molteplicit� sui livelli
  Dim moltLevel As Boolean, moltLevelesimo() As Boolean, ij As Integer, moltLevThen As Boolean
  
  'AC 19/06/07  arriva la booleana
  Dim OperatoreLogico As String, ultimoOperatore As Boolean, varLogicop As String, combLogicop As String
  Dim OperatoreLogicoSimb As String, moltBOL() As Boolean, bolmoltBOL As Boolean
  Dim bolMultipleCcod As Boolean
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  wPath = m_fun.FnPathPrj
  
  nFBms = FreeFile
  
  '---- nuovo template
  Open wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVELOPE" For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTempOper = "" Then
      ComboPLevelTempOper = ComboPLevelTempOper & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTempOper = ComboPLevelTempOper & wRecIn
  Wend
  Close #nFBms
  '-------------------------------
  
  Open wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVEL" For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTemp = "" Then
      ComboPLevelTemp = ComboPLevelTemp & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTemp = ComboPLevelTemp & wRecIn
  Wend
  
  jk = 1
  
  ' AC : wMoltIstr(0), wMoltIstr(2) -> squalificata , wMoltIstr(1),wMoltIstr(3) -> qualificata , se QualAndUnqual tutte e quattro
  
  'Ac per la gestione delle combinazioni
  ' righe = livelli, colonne = max operatori logici
  ' la matrice contiene gli indici degli operatori che per ogni livello e operatore logico
  ' devono essere presi in considerazione per la decodifica
  ' ad ogni ciclo di combinazione si aggiorna la matrice e si crea una nuova decodifica combinando
  ' le info sui vari livelli e operatori logici
  ' la decodifica � inserita nella collection di wMoltIstr
  
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
    If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) > maxLogicOp Then
     maxLogicOp = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef)
    End If
  Next
  
  ReDim MatriceCombinazioni(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr), maxLogicOp)
  If maxLogicOp > 0 Then
    ReDim MatriceCombinazioniBOL(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr), maxLogicOp - 1)
  End If
  
  ' array combinazioni command code : per ogni livello mi dice
  ' sequenza command code che deve essere considerate
  ReDim CombinazioniComCode(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  ReDim MadrdM_Incapsulatore.Combinazionisegmenti(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  ReDim moltLevelesimo(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
    For K1 = 1 To maxLogicOp
      MatriceCombinazioni(k, K1) = 1
      If Not K1 = maxLogicOp Then
        MatriceCombinazioniBOL(k, K1) = 1
      End If
    Next
  Next
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
    CombinazioniComCode(k) = 1
    MadrdM_Incapsulatore.Combinazionisegmenti(k) = 1
  Next
  CreaArrayCloniIstruzione cloniIstruzione, pIdOggetto, pRiga
  CreaArrayCloniSegmenti cloniSegmenti, MadrdM_Incapsulatore.TabIstr
  
  isMoltIstr = False
  If UBound(cloniIstruzione) > 0 Then
    isMoltIstr = True
  End If
  
  'SQ ancora!!??? abbiamo letto l'istruzione all'inizio
  '***** determiniamo se deve essere generata
  '***** sia la versione qualificata che squalificata
  Set tb = m_fun.Open_Recordset("select * from PsDLI_IstrDett_Liv where IdOggetto =" & pIdOggetto & " and Riga = " & pRiga & " and Livello = " & UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
  If Not tb.EOF Then
    QualAndUnqual = tb!QualAndUnqual
  Else
    QualAndUnqual = False
  End If
  tb.Close
  
  Set wMoltIstr(0) = New Collection
  Set wMoltIstr(1) = New Collection
  Set wMoltIstr(2) = New Collection
  Set wMoltIstr(3) = New Collection
  StringToClear = ""
  ReDim wNomeRout(0)
  
  ' Ciclo esterno su istruzione
  ReDim Preserve CombSqual(0)
  
  For i = 0 To UBound(cloniIstruzione)
    ' Mauro 18/11/2009
    If isUnificaGH Then
      xIstr = cloniIstruzione(i)
      zIstr = cloniIstruzione(i)
    Else
      xIstr = Replace(cloniIstruzione(i), "GH", "G")
      zIstr = cloniIstruzione(i)
    End If
    TipIstr = Left(xIstr, 2)
    If MadrdM_Incapsulatore.TabIstr.isGsam Then
       TipIstr = "GS"
    ElseIf xIstr = "GU" Or xIstr = "GHU" Then
       TipIstr = "GU"
    ElseIf xIstr = "POS" Then 'IRIS-DB
      TipIstr = "PS" 'IRIS-DB
    ElseIf xIstr = "ISRT" Then
      TipIstr = "UP"
    ElseIf xIstr = "DLET" Then
       TipIstr = "UP"
    ElseIf xIstr = "REPL" Then
       TipIstr = "UP"
    ElseIf xIstr = "GNP" Or xIstr = "GHNP" Then
       TipIstr = "GP"
    ElseIf xIstr = "GN" Or xIstr = "GHN" Then
       TipIstr = "GN"
    ElseIf xIstr = "PCB" Then
       wUtil = True
       TipIstr = "SH"
    ElseIf xIstr = "TERM" Then
       wUtil = True
       TipIstr = "TM"
    ElseIf xIstr = "CHKP" Then
       wUtil = True
       TipIstr = "CP"
    ElseIf xIstr = "QRY" Then
       wUtil = True
       TipIstr = "QY"
    ElseIf xIstr = "SYNC" Or xIstr = "SYMCHKP" Then
       wUtil = True
       TipIstr = "SY"
    ElseIf xIstr = "XRST" Then
       wUtil = True
       TipIstr = "XR"
    ElseIf xIstr = "SCHED" Then
       wUtil = True
       TipIstr = "SC"
    Else
       'm_fun.WriteLog "Incapsulatore - Costruisci Operazione " & vbCrLf & "Istruzione non riconosciuta " & xIstr, "DR - Incapsulatore"
    End If
  
    bolMultipleCcod = CheckCombCCode(xIstr)
    gbIstr = xIstr
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
      CombinazioniComCode(k) = 1
    Next
    
    'riapplico il calcolo del NomeRout per ogni Molteplicit� di istruzione
    If UBound(MadrdM_Incapsulatore.TabIstr.DBD) > 0 Then
      ' Mauro 05/06/2008 : Se � un DBD Index, il nome routine lo devo costruire con il DBD Fisico
      Dim rsDBD As Recordset
      Dim NomeDbD As String
      
      NomeDbD = MadrdM_Incapsulatore.TabIstr.DBD(1).nome
      Set rsDBD = m_fun.Open_Recordset("select idoggetto from Bs_Oggetti where " & _
                                       "idoggetto = " & MadrdM_Incapsulatore.TabIstr.DBD(1).Id & " and tipo = 'DBD' and tipo_dbd = 'IND'")
      If rsDBD.RecordCount Then
        NomeDbD = Restituisci_DBD_Da_DBDIndex(rsDBD!IdOggetto)
      End If
      rsDBD.Close
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
        If InStr(NomeDbD, "RKKUNL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKLUNL", xIstr, SwTp)
        ElseIf InStr(NomeDbD, "RKGIRL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKOIRL", xIstr, SwTp)
        Else
          nomeRoutine = m_fun.Genera_Nome_Routine(NomeDbD, xIstr, SwTp)
        End If
      Else
        'nomeRoutine = m_fun.Genera_Nome_Routine(MadrdM_Incapsulatore.TabIstr.DBD(1).nome, xIstr, SwTp)
        nomeRoutine = m_fun.Genera_Nome_Routine(NomeDbD, xIstr, SwTp)
        'aggiornalistaDB (MadrdM_Incapsulatore.TabIstr.Dbd(1).nome)
      End If
    Else
      nomeRoutine = m_fun.Genera_Nome_Routine("", xIstr, SwTp)
    End If
   'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
   'stefano: ho dovuta commentarla perch� creava delle istruzioni non valide (AND senza IF) quando
   ' c'� molteplicit� di squalificata insieme alla molteplicit� di funzione; fondamentalmente la molteplicit�
   ' di funzione viene gestita sotto quella di squalificata; dovrebbe essere invertita o creato un vettore per ogni
   ' occorrenza di funzione.
   'If TipIstr <> "GU" And TipIstr <> "GN" And TipIstr <> "GP" Then QualAndUnqual = False
    
   combinazioni = True
   IstrIncaps = ""
   Dcombination = False
   
   While combinazioni
   
   multiOpGen = False
   'AC 02/11/06
   ReDim CombPLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
   ' alberto, sulle istruzioni OPEN e CLSE dava errore per cui l'ho cambiata..
   If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) > 0 Then
     ReDim CombPLevel_s(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) - 1)
   End If
     IstrIncaps = ""
     If UBound(cloniIstruzione) > 0 Then
        varIstr = Restituisci_varFunz(pIdOggetto, pRiga)
        combIstr = "'" & zIstr & "'"
     End If
     If isOldIncapsMethod Then
      wOpe = "<pgm>" & nomeRoutine & "</pgm><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
     Else
      'SQ PEZZA: GESTIRE BENE... (ATTENZIONE E' ANCHE IN UN ALTRO PUNTO...
      If UBound(MadrdM_Incapsulatore.TabIstr.DBD) Then
      wOpe = "<dbd>" & MadrdM_Incapsulatore.TabIstr.DBD(1).nome & "</dbd><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
      Else
        wOpe = "<istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
     End If
     End If
     
     'AC 24/10/08
     'SENSEG
     If Not MadrdM_Incapsulatore.TabIstr.SenSeg = "" Then
        If MadrdM_Incapsulatore.TabIstr.SenSeg = "ALL" Then
            wOpe = wOpe & "<sseg></sseg>"
        Else
            wOpe = wOpe & "<sseg>" & MadrdM_Incapsulatore.TabIstr.SenSeg & "</sseg>"
        End If
     End If
     
     
     For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)   ' ciclo su livelli
        ComboPLevelAppo = ComboPLevelTemp
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 28-07-06
        ' OK: REPL CON SSA QUALIFICATA DA RETURN CODE "AJ"
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 19-07-07
        ' REPL CON PATH-CALL (C.C. "D")!
        'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
          If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm = -1 Then
             wOpe = wOpe & "<level" & k & "><segm>" & "DYNAMIC" & "</segm>"
          Else
             wOpe = wOpe & "<level" & k & "><segm>" & TokenIesimo(cloniSegmenti(k), MadrdM_Incapsulatore.Combinazionisegmenti(k)) & "</segm>"
             If NumToken(cloniSegmenti(k)) > 1 Then
              'varseg = MadrdM_Incapsulatore.TabIstr.BlkIstr(K).ssaName & "(1:8)"
              combSeg = "'" & TokenIesimo(cloniSegmenti(k), MadrdM_Incapsulatore.Combinazionisegmenti(k)) & "'"
              'varseg = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(K).ssaName, "1", CStr(Len(combseg) - 2), TipoFile, "seg") '"8"
              'AC 06/03/08 se ho molteplicit� in segment ho nome della variabile
              varSeg = MadrdM_Incapsulatore.TabIstr.BlkIstr(k).nomeVarSeg
              moltLevelesimo(k) = True
             Else
              varSeg = ""
             End If
             aggiornalistaSeg (MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment)
          End If
        Else
          If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
            If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm = -1 Then
               wOpe = wOpe & "<level1><segm>" & "DYNAMIC" & "</segm>"
            Else
               wOpe = wOpe & "<level1><segm>" & TokenIesimo(cloniSegmenti(k), MadrdM_Incapsulatore.Combinazionisegmenti(k)) & "</segm>"
               If NumToken(cloniSegmenti(k)) > 1 Then
                'varseg = MadrdM_Incapsulatore.TabIstr.BlkIstr(K).ssaName & "(1:8)"
                combSeg = "'" & TokenIesimo(cloniSegmenti(k), MadrdM_Incapsulatore.Combinazionisegmenti(k)) & "'"
                'varseg = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(K).ssaName, "1", CStr(Len(combseg) - 2), TipoFile, "SSA") '"8"
                'AC 06/03/08 se ho molteplicit� in segment ho nome della variabile
                varSeg = MadrdM_Incapsulatore.TabIstr.BlkIstr(k).nomeVarSeg
                moltLevelesimo(k) = True
               Else
                varSeg = ""
               End If
               aggiornalistaSeg (MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment)
            End If
          End If
        End If
        
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "POS" Then 'IRIS-DB
          If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record <> "" Then
             If Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment) <> "" Then
                 wOpe = wOpe & "<area>" & "AREA" & "</area>"
             End If
          End If
        End If
        
        'AC 10/04/2007 spostata per non sommare a wopes la <ccode> del wope
        If QualAndUnqual And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
          wOpeS = wOpe   ' allineiamo  la wOpeS  alla wOpe che si differenziano solo per ultimo livello
          'varop_s = varop
          'combop_s = combop
          For ks = 1 To k - 1
            CombPLevel_s(ks) = CombPLevel(ks)
          Next
          ' allineiamo le stringhe
        End If
                            
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
          If Trim(Replace(Replace(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search, MadrdM_Incapsulatore.CombinazioniComCode(k), bolMultipleCcod), "*", ""), "-", "")) <> "" Then
             If Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment) <> "" Then
                wOpe = wOpe & "<ccode>" & Trim(Replace(Replace(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search, MadrdM_Incapsulatore.CombinazioniComCode(k), bolMultipleCcod), "*", ""), "-", "")) & "</ccode>"
             End If
          End If
        End If
                
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB ' "ISRT" AC 30/07/07
          If NumToken(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search) > 1 Then
            'varcmd = MadrdM_Incapsulatore.TabIstr.BlkIstr(K).ssaName & "(9:" & Len(TokenIesimo(MadrdM_Incapsulatore.TabIstr.BlkIstr(K).Search, CombinazioniComCode(K))) & ")"
            varCmd = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, "9", Len(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search, MadrdM_Incapsulatore.CombinazioniComCode(k), bolMultipleCcod)), TipoFile, "SSA", pIdOggetto)
            combCmd = "'" & TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search, MadrdM_Incapsulatore.CombinazioniComCode(k), bolMultipleCcod) & "'"
            If bolMultipleCcod Then
              moltLevelesimo(k) = True
            End If
          Else
            varCmd = ""
          End If
        End If
          '*********************************
                      
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' QUALIFICAZIONE:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' AC se entrambe le versione wOpe � quella qualificata e wOpeS quella
        ' squalificata, altrimenti se unica sempre in wOpe
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB   ' AC 230707 (xIstr = "ISRT" And K < UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
         If QualAndUnqual And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
          'parte SQUALIFICATA
          ' la wOpeS si ferma qui, wOpeS e wOpe differiscono solo per questo livello
           
          'parte QUALIFICATA
          wFlagBoolean = False
          ' determino se su livelli precedenti ho molteplicit� operatore
             If k > 1 Then
              For ij = 1 To k - 1
                If moltLevelesimo(ij) = True Then
                 moltLevel = True
                 Exit For
                End If
              Next
             End If
             ' ------------CICLO SU OPERATORI LOGICI---------------
             bolMoltOpLogic = False
             bolmoltBOL = False
             ultimoOperatore = False
             ReDim MoltOpLogic(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))
             If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) - 1)
             End If
             ReDim CombPLevelOper(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))
             For K1 = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef)
               'AC 12/07
               ' ripulisco la maschera del template COMBOPLEVELOPE
               ComboPLevelAppoOper = ComboPLevelTempOper
               ComboPLevelAppo = ComboPLevelTemp
               If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                 wOpe = wOpe & "<key>" & MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                 Operatore = TokenIesimo(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                 wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                 If multiOp Then
                  varOp = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, CStr(Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                  ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                  If TipoFile = "PLI" Then
                    combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "3"
                  Else
                    combOp = "REV" & Operatore & "1" & vbCrLf _
                    & "    " & Space(Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                    & "    " & Space(Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                  End If
                  moltLevelesimo(k) = True
                  MoltOpLogic(K1) = True
                 Else
                    varOp = ""
                 End If
                 If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                     OperatoreLogicoSimb = TokenIesimo(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, MatriceCombinazioniBOL(k, K1))
                     OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                     If multiOp Then
                      varLogicop = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, CStr(Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                      combLogicop = "'" & OperatoreLogicoSimb & "'"
                      moltLevelesimo(k) = True
                      moltBOL(K1) = True
                     Else
                      varLogicop = ""
                     End If
                     If Not OperatoreLogico = ")" Then
                       wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                       wFlagBoolean = True
                     Else
                       ultimoOperatore = True
                     End If
                 Else
                  varLogicop = ""
                 End If
                'AC 03/07/06
                'ultimo
                  
                bolMoltOpLogic = False
                bolmoltBOL = False
                If K1 > 1 Then
                 For ij = 1 To K1 - 1
                   If MoltOpLogic(ij) = True Then
                     bolMoltOpLogic = True
                     Exit For
                   End If
                 Next
                 For ij = 1 To K1 - 1
                   If moltBOL(ij) = True Then
                     bolmoltBOL = True
                     Exit For
                   End If
                 Next
                End If
                '---------- molteplicit� operatore --------
                If Not varOp = "" Then
                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", varOp)
                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp)
                 'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                 If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                 Else
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                 End If
'                 If QualAndUnqual Then
'                   ComboPLevelAppoOper = Replace(ComboPLevelAppo, "<I%IND-3>", "")
'                 End If
'                 CombPLevelOper(k1) = ComboPLevelAppoOper
'                Else
'                 CombPLevelOper(k1) = ComboPLevelAppoOper
                 End If
                ' booleano
                '---------- molteplicit� booleana --------
                If Not varLogicop = "" Then
                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", varLogicop)
                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop)
                 'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                 If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                 Else
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                 End If
                End If
                
                If QualAndUnqual Then
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                End If
                CombPLevelOper(K1) = ComboPLevelAppoOper
                
             '-------------------------------------------
               End If
               ' nel caso di parentesi sull'operatore logico interrompo il ciclo
               If ultimoOperatore Then Exit For
             Next K1
             
             '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
             
             Rtb.text = ComboPLevelAppo
             For jj = 1 To UBound(CombPLevelOper)
              MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
             Next
             ComboPLevelAppo = Rtb.text
             
             If Not varCmd = "" And bolMultipleCcod Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
            If Not varSeg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varSeg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg)
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
         Else  ' o la versione squalificata o quella squalificata
         
          ' AC il campo parentesi e la trasformazione in stringa del booleano Qualificazione
          If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Parentesi <> "(" Then ' Or Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).SsaName) = "<none>" Then
             OnlySqual = True
             ReDim CombPLevelOper(0)
          Else   ' Qualificata
             OnlyQual = True
             wFlagBoolean = False
             If k > 1 Then
              For ij = 1 To k - 1
               If moltLevelesimo(ij) = True Then
                 moltLevel = True
                 Exit For
               End If
              Next
             End If
             ' ----- CICLO OPERATORI LOGICI
             bolmoltBOL = False
             ultimoOperatore = False
             bolMoltOpLogic = False
             ReDim MoltOpLogic(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))
             If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) - 1)
             End If
             ReDim CombPLevelOper(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))
             For K1 = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef)
               ComboPLevelAppoOper = ComboPLevelTempOper
               If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                 wOpe = wOpe & "<key>" & MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                 Operatore = TokenIesimo(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                 wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                 If multiOp Then  ' AC 020807 And Not TipIstr = "UP"
                  'varop = MadrdM_Incapsulatore.TabIstr.BlkIstr(K).ssaName & "(" & Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
                  varOp = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, CStr(Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                  ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                  If TipoFile = "PLI" Then
                    combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "3"
                  Else
                    combOp = "REV" & Operatore & "1" & vbCrLf _
                    & "    " & Space(Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                    & "    " & Space(Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                  End If
                  moltLevelesimo(k) = True
                  MoltOpLogic(K1) = True
                 Else
                    varOp = ""
                 End If
                 If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                    OperatoreLogicoSimb = TokenIesimo(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, MatriceCombinazioniBOL(k, K1))
                    OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                    If multiOp Then    ' AC 020807 And Not TipIstr = "UP"
                      varLogicop = buildSubstringFromType(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, CStr(Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                      combLogicop = "'" & OperatoreLogicoSimb & "'"
                      moltLevelesimo(k) = True
                      moltBOL(K1) = True
                    Else
                      varLogicop = ""
                    End If
                    If Not OperatoreLogico = ")" Then
                      wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                      wFlagBoolean = True
                    Else
                      ultimoOperatore = True
                    End If
                 Else
                  varLogicop = ""
                 End If
                
                 
                  '------------------------------------------------------
                  bolMoltOpLogic = False
                  bolmoltBOL = False
                  If K1 > 1 Then
                   For ij = 1 To K1 - 1
                     If MoltOpLogic(ij) = True Then
                       bolMoltOpLogic = True
                       Exit For
                     End If
                   Next
                   For ij = 1 To K1 - 1
                    If moltBOL(ij) = True Then
                      bolmoltBOL = True
                      Exit For
                    End If
                    Next
                  End If
                  If Not varOp = "" Then
                    ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", varOp)
                    ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp)
                    If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                    Else
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                    End If
'                    If QualAndUnqual Then
'                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
'                    End If
'                    CombPLevelOper(k1) = ComboPLevelAppoOper
'                  Else
'                      CombPLevelOper(k1) = ComboPLevelAppoOper
                  End If
                  '---------- molteplicit� booleana --------
                  If Not varLogicop = "" Then
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", varLogicop)
                   ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop)
                   'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                   If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                   Else
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                   End If
                  End If
                  
                  If QualAndUnqual Then
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                  End If
                  CombPLevelOper(K1) = ComboPLevelAppoOper
          '-------------------------------------------------------
               End If
                If ultimoOperatore Then Exit For
             Next K1
          End If '-- squal o qual
          
          
          '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
             
          Rtb.text = ComboPLevelAppo
          For jj = 1 To UBound(CombPLevelOper)
           MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
          Next
          ComboPLevelAppo = Rtb.text
          
          
          If Not varCmd = "" And bolMultipleCcod Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
            ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
            If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
              ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
            Else
              ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
            End If
            If QualAndUnqual Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
            End If
            CombPLevel(k) = ComboPLevelAppo
          Else
              CombPLevel(k) = ComboPLevelAppo
          End If
          If Not varSeg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varSeg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg)
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
        End If   '-- una sola versione o due versioni
       End If
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        ' SQ 19-07-07
        ' REPL CON PATH-CALL (C.C. "D")!
        'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
         wOpe = wOpe & "</level" & k & ">"
         If QualAndUnqual And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
           wOpeS = wOpeS & "</level" & k & ">"
         End If
        Else
         If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
           wOpe = wOpe & "</level1>"
           If QualAndUnqual And k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then
             wOpeS = wOpeS & "</level1>"
           End If
         End If
        End If
     Next k   ' FINE CICLO LIVELLI
     
     wOpe = wOpe & "</livelli>"
     
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
      If Trim(MadrdM_Incapsulatore.TabIstr.keyfeedback) <> "" Then
         wOpe = wOpe & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
      End If
     End If
     
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
      If Len(MadrdM_Incapsulatore.TabIstr.procSeq) Then
         wOpe = wOpe & "<procseq>" & MadrdM_Incapsulatore.TabIstr.procSeq & "</procseq>"
      End If
     End If

     If xIstr = "OPEN" Then
      If Len(MadrdM_Incapsulatore.TabIstr.procOpt) Then
         wOpe = wOpe & "<procopt>" & MadrdM_Incapsulatore.TabIstr.procOpt & "</procopt>"
      End If
     End If
     
'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpe
     For k = 1 To UBound(OpeIstrRt)
        If wOpe = OpeIstrRt(k).Decodifica Then
           If OpeIstrRt(k).ordPcb = ordPcb Then
              'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
              'verifico se � la prima
              pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
              pcbDynInt = OpeIstrRt(k).pcbDyn
              maxPcbDyn = 0
              Exit For
           Else
              If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
              'trovata un'istruzione identica, ma con diverso pcb
                 maxPcbDyn = OpeIstrRt(k).pcbDyn
              End If
           End If
        End If
     Next k
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
     If maxPcbDyn > 0 Then
        pcbDyn = Format(maxPcbDyn + 1, "000")
        pcbDynInt = maxPcbDyn + 1
     End If
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
     ' aggiunge il pcb dinamico
      If Len(pcbDyn) Then
         wOpe = wOpe & "<pcbseq>" & pcbDyn & "</pcbseq>"
      Else
      ' se � la prima istruzione di quel tipo
         wOpe = wOpe & "<pcbseq>001</pcbseq>"
         pcbDynInt = 1
      End If
     End If
    
     wDecod = wOpe
     Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpe & "'")
     
     'If xope = "" Then
     If tb.EOF Then
       Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
          maxId = 1
       Else
          maxId = tb1(0) + 1
       End If
       tb.AddNew
       tb!IdIstruzione = maxId
       tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
       wIstr = tb!Codifica
       tb!Decodifica = wOpe
       tb!ImsDB = True
       tb!ImsDC = False
       tb!CICS = SwTp
   'stefano: routine multiple
       nomeRoutCompleto = Genera_Counter(nomeRoutine)
       tb!nomeRout = nomeRoutCompleto
       tb.Update
          ' Mauro 28/04/2009
'          'pcb dinamico
'          k = UBound(OpeIstrRt) + 1
'          ReDim Preserve OpeIstrRt(k)
'          OpeIstrRt(k).Decodifica = wOpeSave
'          OpeIstrRt(k).Istruzione = wIstr
'          OpeIstrRt(k).ordPcb = ordPcb
'          OpeIstrRt(k).pcbDyn = pcbDynInt
     Else
       wIstr = tb!Codifica
       nomeRoutCompleto = tb!nomeRout
       ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
       nomeRoutine = tb!nomeRout
        End If
        tb.Close
     
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     'SQ CPY-ISTR
     'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
     Set tb = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrCodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
     If tb.EOF Then
        'SQ 16-11-06 - EMBEDDED
        'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & nomeroutcompleto & "')"
        'SQ CPY-ISTR
        'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeroutcompleto) & "')"
        m_fun.FnConnection.Execute "INSERT INTO PsDLI_IstrCodifica VALUES(" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeRoutCompleto) & "')"
          'Mauro 28/04/2009 : pcb dinamico
          k = UBound(OpeIstrRt) + 1
          ReDim Preserve OpeIstrRt(k)
          OpeIstrRt(k).Decodifica = wOpeSave
          OpeIstrRt(k).Istruzione = wIstr
          OpeIstrRt(k).ordPcb = ordPcb
          OpeIstrRt(k).pcbDyn = pcbDynInt
     End If
     tb.Close
     If QualAndUnqual Then
       wOpeS = wOpeS & "</livelli>"
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
      If Trim(MadrdM_Incapsulatore.TabIstr.keyfeedback) <> "" Then
        wOpeS = wOpeS & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
      End If
'procseq: sarebbe meglio metterlo in MadrdM_Incapsulatore.TabIstr, ma per ora facciamo cos� (procseq
' ci servir� anche nella generazione routine
      If UBound(MadrdM_Incapsulatore.TabIstr.psb) Then
         If Len(MadrdM_Incapsulatore.TabIstr.procSeq) Then
             wOpeS = wOpeS & "<procseq>" & MadrdM_Incapsulatore.TabIstr.procSeq & "</procseq>"
         End If
         If Len(MadrdM_Incapsulatore.TabIstr.procOpt) And xIstr = "OPEN" Then
             wOpeS = wOpeS & "<procopt>" & MadrdM_Incapsulatore.TabIstr.procOpt & "</procopt>"
         End If
      End If
     End If

'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpeS
     For k = 1 To UBound(OpeIstrRt)
        If wOpeS = OpeIstrRt(k).Decodifica Then
           If OpeIstrRt(k).ordPcb = ordPcb Then
              'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
              'verifico se � la prima
              pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
              pcbDynInt = OpeIstrRt(k).pcbDyn
              maxPcbDyn = 0
              Exit For
           Else
              If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
              'trovata un'istruzione identica, ma con diverso pcb
                 maxPcbDyn = OpeIstrRt(k).pcbDyn
              End If
           End If
        End If
     Next k
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
     If maxPcbDyn > 0 Then
        pcbDyn = Format(maxPcbDyn + 1, "000")
        pcbDynInt = maxPcbDyn + 1
     End If
     ' aggiunge il pcb dinamico
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
      If Len(pcbDyn) Then
         wOpeS = wOpeS & "<pcbseq>" & pcbDyn & "</pcbseq>"
      Else
      ' se � la prima istruzione di quel tipo
         wOpeS = wOpeS & "<pcbseq>" & "001" & "</pcbseq>"
         pcbDynInt = 1
      End If
     End If
     
     wDecodS = wOpeS
     Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpeS & "'")
     
     'If xope = "" Then
     If tb.EOF Then
         

       Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
          maxId = 1
       Else
          maxId = tb1(0) + 1
       End If
       tb.AddNew
       tb!IdIstruzione = maxId
       tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
       wIstrS = tb!Codifica
       tb!Decodifica = wOpeS
       tb!ImsDB = True
       tb!ImsDC = False
       tb!CICS = SwTp
   'stefano: routine multiple
       nomeRoutCompleto = Genera_Counter(nomeRoutine)
       tb!nomeRout = nomeRoutCompleto
       tb.Update
       tb.Close
    'pcb dinamico
       k = UBound(OpeIstrRt) + 1
       ReDim Preserve OpeIstrRt(k)
       OpeIstrRt(k).Decodifica = wOpeSave
       OpeIstrRt(k).Istruzione = wIstr
       OpeIstrRt(k).ordPcb = ordPcb
       OpeIstrRt(k).pcbDyn = pcbDynInt
     Else
       wIstrS = tb!Codifica
       nomeRoutCompleto = tb!nomeRout
       ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
       nomeRoutine = tb!nomeRout
       tb.Close
     End If
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     'virgilio CPY-ISTR
     'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
     Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
     If tb.EOF Then
        'virgilio CPY-ISTR
        'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeroutcompleto & "')"
        m_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeRoutCompleto & "')"
     End If
     tb.Close
    End If
    'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    'BACO: per ora non viene gestita la molteplicit� sulla ISRT per i livelli qualificati
    ' ma in realt� non ha senso (di solito l'inserimento viene fatto qualificando un
    ' livello precedente per uguale, in modo da puntare un solo parent
    
    'AC 22/02/10 rimosse limitazioni REPL DLET
    'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
     combinazioni = AggiornaCombinazione(maxLogicOp)
    'Else
     'combinazioni = False
    'End If
    
    moltLevel = False
     ' aggiornamento collection
     If Not StringToClear = "" Then
      IstrIncaps = Replace(IstrIncaps, StringToClear, "")
      IstrSIncaps = Replace(IstrSIncaps, StringToClear, "")
     End If
     
     If QualAndUnqual Then
       ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
       checkColl = True
       
       For j = 1 To wMoltIstr(3).Count
        
        If wMoltIstr(3).item(j) = wIstr Then
          checkColl = False
          
          Exit For
        End If
       Next
       If checkColl Then
        wMoltIstr(1).Add IstrIncaps, wDecod
        wMoltIstr(3).Add wIstr, wDecod
        aggiornalistaCodOp (wIstr)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeRoutCompleto
       End If
       
       ' 0 e 2  da wOpeS, indice ks dell'array e da IstrSIncaps
       checkColl = True
       checksqual = True
       For j = 1 To wMoltIstr(2).Count
'        If wMoltIstr(2).Item(j) = OpeIstrRt(ks).Istruzione Then
        If wMoltIstr(2).item(j) = wIstrS Then
          checkColl = False
          checksqual = False
          Exit For
        End If
       Next
       If checkColl Then
        wMoltIstr(0).Add IstrSIncaps, wDecod
        wMoltIstr(2).Add wIstrS, wDecodS
        aggiornalistaCodOp (wIstrS)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeRoutCompleto
       End If
     Else
         ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
         checkColl = True
         For j = 1 To wMoltIstr(3).Count
'          If wMoltIstr(3).Item(j) = OpeIstrRt(k).Istruzione Then
          If wMoltIstr(3).item(j) = wIstr Then
            checkColl = False
            Exit For
          End If
         Next
         If checkColl Then
          wMoltIstr(1).Add IstrIncaps, wDecod
          wMoltIstr(3).Add wIstr, wDecod
          aggiornalistaCodOp (wIstr)
          y = y + 1
          ReDim Preserve wNomeRout(y)
          wNomeRout(y) = nomeRoutCompleto
         End If
       
     End If
     
     ' livello COMBQUAL -- costruisce elemento array combqual
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL_EXEC"
      ' nel caso di linguaggi con then (vedi PLI), sui template gestiamo la riga dello then con il tag <then>
      ' negli altri casi l'if sotto � ininfluente
      For ij = 1 To UBound(moltLevelesimo)
        If moltLevelesimo(ij) = True Then
          moltLevThen = True
          Exit For
        End If
      Next
      If QualAndUnqual Or UBound(cloniIstruzione) > 0 Or moltLevThen Then
        MadrdM_GenRout.changeTag Rtb, "<then>", ""
      End If
      If QualAndUnqual Then
        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
      Else
        ' gestisco indentazione move
        If multiOpGen Or UBound(cloniIstruzione) > 0 Then ' ne lascio solo una
          MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        End If
      End If
      
      'if or end per l'istruzione e relativo end if di chiusura
      If QualAndUnqual Then
        MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), 1)
        MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 1)
      Else
        MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), 2)
        ' end if
        ' AC 18/06/07  nel caso che le molteplicit� siano ignorate (vedi ISRT)
        ' non devo lasciare l'end if finale
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB  'AC 200707
          If multiOpGen Or UBound(cloniIstruzione) > 0 Then
            MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 2)
          Else
             MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 1)
          End If
        ElseIf UBound(cloniIstruzione) > 0 Then ' potremmo avere molteplicit� istr con istr diverse da GU,GN e GP
          MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 2)
        Else
          MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 1)
        End If
      End If
      
      MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
      MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
      MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
      MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
      MadrdM_GenRout.changeTag Rtb, "<CODICEOPERB>", Replace(wIstr, ".", "")
      'AC 12/05/10 ______
      If isMoltIstr Or isMoltPcb Then
        If Len(nomeRoutine) < 8 And InStr(m_fun.FnNomeDB, "prj-GRZ") = 0 Then
          MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & Space(8 - Len(nomeRoutine))
        Else
          MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine
        End If
      End If
      '__________________
      If EmbeddedRoutine Then
        MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
      Else
        MadrdM_GenRout.changeTag Rtb, "<standard>", ""
      End If
      
      For k = 1 To UBound(CombPLevel)
        MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
      Next
      
      appocomb = Rtb.text
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFQUAL"
      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFQUAL"
      MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
      ReDim Preserve CombQual(jk)
      CombQual(jk) = Rtb.text
      
      If QualAndUnqual And checksqual Then ' due parti, la seconda � squalificata
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL_EXEC"
        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<A%"), 1)
        MadrdM_GenRout.changeTag Rtb, MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(MadrdM_Incapsulatore.FindCompleteTag(Rtb.text, "<B%"), 1)
        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstrS
        wIstrsOld = wIstrS
        If EmbeddedRoutine Then
          MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
        Else
          MadrdM_GenRout.changeTag Rtb, "<standard>", ""
        End If
        For k = 1 To UBound(CombPLevel_s)
          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel_s(k), True
        Next
        appocomb = Rtb.text
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFSQUAL"
        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
        ReDim Preserve CombSqual(UBound(CombSqual) + 1)
        CombSqual(UBound(CombSqual)) = Rtb.text
      ElseIf Not QualAndUnqual Then
'       ' anche nella parte squalificata metto la stessa cosa della qualificata
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
'        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
'        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
'        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
'        For k = 1 To UBound(CombPLevel_s)
'          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
'        Next
'        appocomb = Rtb.text
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
'        ReDim Preserve CombSqual(jk)
'        CombSqual(jk) = Rtb.text
      End If
      jk = jk + 1
      '**********
    Wend
  Next i  'ciclo su molteplicit� istruzioni
  'sostituzioni nel file principale
  
  'stefanopul
  'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\MAINCOMB"
  Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\MAINCOMB"
  'AC 20/03/09  ----  commentata 22/07/10
'  Dim dataType As String
'  If Left(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName, 1) = "'" And _
'     Right(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName, 1) = "'" Then
'    dataType = "X"
'  Else
'    getAreaInfo Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName), pIdOggetto, 0, dataType, "", -1
'  End If
 ' ----> commentata 22/07/10
'  MadrdM_GenRout.changeTag Rtb, "<SSA-INSPECT>", "SUBSTR(" & IIf(dataType = "X", "", "IRIS_") & _
'                  MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName & ",9," & _
'                  Len(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Search, 1, bolMultipleCcod)) + 1 & ")"
  For i = 1 To UBound(CombQual)
    MadrdM_GenRout.changeTag Rtb, "<<IFQUAL(i)>>", CombQual(i)
  Next i
  If QualAndUnqual Then
    For i = 1 To UBound(CombSqual)
      MadrdM_GenRout.changeTag Rtb, "<<IFSQUAL(i)>>", CombSqual(i)
    Next i
    MadrdM_GenRout.changeTag Rtb, "<condsqual>", "" ' altrimenti istr corrispondenti eliminate
  End If
  appoFinalString = Rtb.text
  
  While InStr(appoFinalString, vbCrLf) > 0
    posEndLine = InStr(appoFinalString, vbCrLf)
    insertLineOF Testo, Left(appoFinalString, posEndLine - 1), pIdOggetto, indentkey
    appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
  Wend
   If Not Trim(appoFinalString) = "" Then
     insertLineOF Testo, appoFinalString, pIdOggetto, indentkey
   End If
  Close #nFBms
  
  'VItable(UBound(VItable)).lines_down = VItable(UBound(VItable)).lines_down & vbCrLf & Testo
  
  Exit Function
ErrH:
  MsgBox err.Description
  'insertLine testo, Rtb.text
  Resume Next
End Function

Function CostruisciCall_CALLTemplate_OF(pTesto As String, wIstruzione, typeIncaps As String, wIstrDli As String, rst As Recordset, RTBErr As RichTextBox, Optional iscopy As Boolean = False, Optional seqInstr As Integer = 0) As String
  Dim k As Integer, K1 As Integer, k2 As Integer, i As Integer
  Dim wstr As String, wStr1 As String, nPcb As String, testoCall As String
  Dim s() As String
  Dim wKeyName As String
  'Parametri incapsulatore
  Dim pmNomeDbd As String, pmCallRout As String, pmDecIstr As String
  Dim Rtb  As RichTextBox, wPath As String, appoFinalString As String, posEndLine As String
  Dim Rtb2  As RichTextBox
  Dim segLevel() As String, levelKey() As String, appoLevelString As String
  Dim segLevel2() As String
  Dim templateName As String
  Dim templateName2 As String 'IRIS-DB
  Dim appoXRST As String, appoXRSTret As String
  Dim xprogramm As String
  Dim dataType As String
  Dim tb1 As Recordset
  Dim h As Integer
  Dim BolNoRev As Boolean, BolPtr As Boolean
  Dim arrSavedArea() As String, arrSavedAreaRet() As String
  Dim NumParam As Integer
  Dim hasSsa As Boolean
  hasSsa = False
  
  On Error GoTo catch
  
  testoCall = testoCall & pTesto
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  Set Rtb2 = MadrdF_Incapsulatore.RTBModifica 'IRIS-DB
  
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\MAINCALL" & IIf(EmbeddedRoutine, "_EMBEDDED", "")
  Rtb.LoadFile templateName
  
  If MadrdM_Incapsulatore.TabIstr.istr = "PCB" Then
    MadrdM_GenRout.changeTag Rtb, "<NEW_PCB>", ""
  End If
  DoEvents
  'GSAM fa un giro particolare
  If MadrdM_Incapsulatore.TabIstr.isGsam Then
    ' purtroppo va impostata alla OPEN, mentre la psdli_istrdett_liv non c'� per la open
    ' bisogna fare la seguente forzatura
    ' E' una grossa forzatura, ma evita di cambiare troppe cose
    Set tb1 = m_fun.Open_Recordset("select dataarea from psdli_istrdett_liv as a, psdli_istruzioni as b where a.idoggetto = b.idoggetto and a.riga = b.riga and b.idoggetto = " & rst!IdOggetto & " and numpcb = '" & MadrdM_Incapsulatore.TabIstr.pcb & "'")
    If Not tb1.EOF Then
      If Len(tb1!DataArea) Then
        MadrdM_GenRout.changeTag Rtb, "<RECLENGSAM>", tb1!DataArea
      End If
    End If
    tb1.Close
  End If
  If isPliCallinsideSelect Then
    MadrdM_GenRout.changeTag Rtb, "<insideselect>", ""
  End If
  If Not EmbeddedRoutine Then
    '***** per embedded spostato su commoninit
    If Not MadrdM_Incapsulatore.TabIstr.pcb = "" Then
      MadrdM_GenRout.changeTag Rtb, "<PCB>", Replace(MadrdM_Incapsulatore.TabIstr.pcb, "$", "")
    End If
    If Not MadrdM_Incapsulatore.TabIstr.numpcb = 0 And Not MadrdM_Incapsulatore.TabIstr.istr = "TERM" Then
      MadrdM_GenRout.changeTag Rtb, "<NUMPCB>", MadrdM_Incapsulatore.TabIstr.numpcb
    End If
   
    '10/07/09 tag <WR> su MAINCALL
    If Not (MadrdM_Incapsulatore.TabIstr.istr = "GU" Or MadrdM_Incapsulatore.TabIstr.istr = "GN" Or MadrdM_Incapsulatore.TabIstr.istr = "GNP" Or _
       MadrdM_Incapsulatore.TabIstr.istr = "GHU" Or MadrdM_Incapsulatore.TabIstr.istr = "GHN" Or MadrdM_Incapsulatore.TabIstr.istr = "GHNP" Or MadrdM_Incapsulatore.TabIstr.istr = "POS") Then 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<WR>", ""
    End If
    'Salvataggio parziale
    appoFinalString = Rtb.text
   
    ReDim segLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
    ReDim segLevel2(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)) 'IRIS-DB
    
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLV"
      templateName2 = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLV2" 'IRIS-DB
      Rtb.LoadFile templateName
      Rtb2.LoadFile templateName2
      
      'If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment = "" Then
      '  AggLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!Riga & " missing segment", MadrdF_Incapsulatore.RTErr
      'End If
     
      If UBound(MadrdM_Incapsulatore.TabIstr.psb) Then
        ' Mauro 16/12/2008
        If MadrdM_Incapsulatore.TabIstr.numpcb = 0 And UBound(MadrdM_Incapsulatore.TabIstr.DBD) Then
          h = Restituisci_Livello_Segmento_dbd(MadrdM_Incapsulatore.TabIstr.DBD(1).Id, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment)
        Else
          h = Restituisci_Livello_Segmento(MadrdM_Incapsulatore.TabIstr.psb(1).Id, MadrdM_Incapsulatore.TabIstr.numpcb, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment, 0)
        End If
      Else
        'stefano: istruzioni senza psb, assegna comunque un default
        'h = -1
        If UBound(MadrdM_Incapsulatore.TabIstr.DBD) Then
          'MG
          'h = Restituisci_Livello_Segmento_dbd(MadrdM_Incapsulatore.TabIstr.dbd(1).Id, MadrdM_Incapsulatore.TabIstr.BlkIstr(K).Segment, 0)
          If Not MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm = 0 Then
            segLev = 0
            getSegLevel (CLng(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm))
            h = segLev - 1
          Else
            h = 0
          End If
        Else
          h = -1
        End If
      End If
      If Not MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm = 0 Then
        If IsSegObsolete(CLng(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm)) Then
            AggLog "[Object: " & rst!IdOggetto & "] Riga: " & rst!Riga & ", segment " & MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment & " is obsolete", MadrdF_Incapsulatore.RTErr
        End If
      End If
      
     
      If h >= 0 Then
        MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
      Else
      'IRIS-DB Il livello non ci pu� essere per le istruzioni senza SSA, per ora controllo solo sulla base di quelle che trovo
        If rst!Istruzione <> "ROLB" Then 'IRIS-DB
          m_fun.WriteLog "Incaps: Obj(" & rst!IdOggetto & ") Row( " & rst!Riga & " missing level", "MadrdM_Incapsulatore"
          AggLog "[Object: " & rst!IdOggetto & " riga: " & rst!Riga & " ] - Missing level", MadrdF_Incapsulatore.RTErr
          MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
        End If 'IRIS-DB
      End If
      MadrdM_GenRout.changeTag Rtb, "<LEVEL>", k
     
      'tag <WR> su SEGMENTLV
      If Not (MadrdM_Incapsulatore.TabIstr.istr = "GU" Or MadrdM_Incapsulatore.TabIstr.istr = "GN" Or MadrdM_Incapsulatore.TabIstr.istr = "GNP" Or _
         MadrdM_Incapsulatore.TabIstr.istr = "GHU" Or MadrdM_Incapsulatore.TabIstr.istr = "GHN" Or MadrdM_Incapsulatore.TabIstr.istr = "GHNP" Or MadrdM_Incapsulatore.TabIstr.istr = "POS") Then 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<WR>", ""
      End If
      appoLevelString = Rtb.text
     
      ReDim levelKey(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))

      If rst!valida Then 'IRIS-DB
      
       For K1 = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef)
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\LEVKEY"
        Rtb.LoadFile templateName
       
        If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart <> 0 Then
          If Not MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName = "" Then
            MadrdM_GenRout.changeTag Rtb, "<SSANAME>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName
            MadrdM_GenRout.changeTag Rtb2, "<SSANAME>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName 'IRIS-DB
            If k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then 'IRIS-DB
              hasSsa = True 'IRIS-DB
              MadrdM_GenRout.changeTag Rtb2, ",", ");" 'IRIS-DB
            End If 'IRIS-DB
          End If
          If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart = 0 Then
            AggLog "[Object: " & rst!IdOggetto & "] Riga: " & rst!Riga & " KEYSTART(" & K1 & "), level " & k & " missing.", MadrdF_Incapsulatore.RTErr
          End If
          If Not iscopy Then
            MadrdM_GenRout.changeTag Rtb, "<KEYSTART>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart
          Else
            MadrdM_GenRout.changeTag Rtb, "<copy>", ""
            MadrdM_GenRout.changeTag Rtb, "<KEYSTARTC>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyStart
          End If
          MadrdM_GenRout.changeTag Rtb, "<KEYLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen
          MadrdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(k, "#0")
          MadrdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(K1, "#0")
          
          'AC 20/03/09
          If Left(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, 1) = "'" And Right(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, 1) = "'" Then
            dataType = "X"
          Else
            getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName), rst!IdOggetto, 0, dataType, "", -1
          End If
          BolNoRev = False
          If dataType = "X" Then
            MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
            BolNoRev = True
          Else
            MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
          End If
        End If
        levelKey(K1) = Rtb.text
       Next K1
      End If 'IRIS-DB
      ' riprendo da SEGMENTLV
      Rtb.text = appoLevelString
     
      For i = 1 To K1 - 1
        MadrdM_GenRout.changeTag Rtb, "<<LEVKEY(i)>>", levelKey(i)
      Next i
      
      If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName) Then
        MadrdM_GenRout.changeTag Rtb, "<SSANAME>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName
        MadrdM_GenRout.changeTag Rtb2, "<SSANAME>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName 'IRIS-DB
        If k = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) Then 'IRIS-DB
          hasSsa = True
          MadrdM_GenRout.changeTag Rtb2, ",", ");" 'IRIS-DB
        End If 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<SSALEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).SsaLen
        MadrdM_GenRout.changeTag Rtb, "<FLEVELSA>", Format(k, "#0")
        'AC 20/03/09
        'AC 05/02/10
        Dim area As String
        
        If Left(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, 1) = "'" And Right(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, 1) = "'" Then
            dataType = "X"
        Else
            getAreaType Trim(Replace(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, "'", "")), rst!IdOggetto, 0, dataType, "", -1
        End If
        
        BolNoRev = False
        If dataType = "X" Then
          MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
          BolNoRev = True
        Else
          MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
        End If
      End If
      
      segLevel(k) = Rtb.text
      segLevel2(k) = Rtb2.text 'IRIS-DB
      If Left(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, 1) = "'" And Right(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).ssaName, 1) = "'" Then
        segLevel(k) = Replace(segLevel(k), "IRIS_", "")
      End If
      If MadrdM_Incapsulatore.TabIstr.isGsam Then
        segLevel(k) = Replace(segLevel(k), "<REV>_", "")
      End If
      'AC 19/03/08
      'segLevel(k) = Replace(segLevel(k), "IRIS_", "")
      segLevel(k) = Replace(segLevel(k), "<REV>", "IRIS")
    Next k
     
     'recupero maincall
     If MadrdM_Incapsulatore.TabIstr.isGsam Then
        appoFinalString = Replace(appoFinalString, "<REV>_", "")
     End If
     Rtb.text = appoFinalString
     If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) > 0 Then
      'AC tolta il 19/05/08
      'If Not (MadrdM_Incapsulatore.TabIstr.istr = "GU" Or MadrdM_Incapsulatore.TabIstr.istr = "GN" Or MadrdM_Incapsulatore.TabIstr.istr = "GNP") Then
        If Not MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).recordReaddress = "" Then
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).recordReaddress)
        Else
           MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(Replace(Replace(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record, ".", "_"), "$", ""))
        End If
        MadrdM_GenRout.changeTag Rtb, "<RECORD>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record
        MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr), "#0")
        'MadrdM_GenRout.changeTag Rtb, "<REV>", "REV"
        'AC 19/03/09
        
        getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record), rst!IdOggetto, 0, dataType, "", -1
        If dataType = "X" Then
            MadrdM_GenRout.changeTag Rtb, "<REV>", ""
            BolNoRev = True
            MadrdM_GenRout.changeTag Rtb, "<NOPTR>", ""
        Else
            If dataType = "PTR" Then
              BolPtr = True
              MadrdM_GenRout.changeTag Rtb, "<PTR>", ""
            Else
              MadrdM_GenRout.changeTag Rtb, "<NOPTR>", ""
            End If
            MadrdM_GenRout.changeTag Rtb, "<REV>", "IRIS_"
        End If
      'End If
     End If
     For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
      MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV(i)>>", segLevel(k)
      MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV2(i)>>", segLevel2(k) 'IRIS-DB
     Next
     
     
     'IRIS-DB istruzioni con SSA dinamica "forzate", cio� quelle su cui Reviser non ha capito la molteplicit�; forza la prima SSA nella molteplicit�
     'IRIS-DB: rimosso in quanto trattato a parte
''''     If Not rst!valida Then
''''       If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) > 0 Then
''''         If MadrdM_Incapsulatore.TabIstr.BlkIstr(1).ssaName <> "" Then
''''           MadrdM_GenRout.changeTag Rtb, "<DYNAMIC-SSA>", MadrdM_Incapsulatore.TabIstr.BlkIstr(1).ssaName
''''         Else
''''           MadrdM_GenRout.changeTag Rtb, "<DYNAMIC-SSA>", "NO-SSA" 'cos� d� errore in compilazione, vediamo se ci pu� stare, comunque il truschino la trasforma in dinamica
''''         End If
''''       Else
''''         MadrdM_GenRout.changeTag Rtb, "<DYNAMIC-SSA>", "NO-SSA" 'cos� d� errore in compilazione, vediamo se ci pu� stare, comunque il truschino la trasforma in dinamica
''''       End If
''''       MadrdM_GenRout.changeTag Rtb, "<EMPTY>", ""
''''     End If
     
     'SQ 6-10-06
     'MOVE PCB "VERO" nel nostro: (per l'ultimo segmento letto, per es...)
     
      MadrdM_GenRout.changeTag Rtb, "<LANGUAGE>", TipoFile 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", MadrdM_Incapsulatore.TabIstr.istr 'IRIS-DB
      If MadrdM_Incapsulatore.TabIstr.istr <> "PCB" And MadrdM_Incapsulatore.TabIstr.istr <> "SCHE" Then 'IRIS-DB
       MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", CInt(Right(TabDec(1).Codifica, 6)) 'IRIS-DB
      End If 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<SEGMENT>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).segment 'IRIS-DB
      If Len(MadrdM_Incapsulatore.TabIstr.pcb) And Not MadrdM_Incapsulatore.TabIstr.istr = "TERM" Then
      'IRIS-DB per ora lo cambio solo qui
        Set tb1 = m_fun.Open_Recordset("select OrigPCB from IRIS_DYN_PCB where idpgm = " & rst!IdOggetto & " and TargPCB = '" & MadrdM_Incapsulatore.TabIstr.pcb & "'") 'IRIS-DB
        If Not tb1.EOF Then 'IRIS-DB
          MadrdM_GenRout.changeTag Rtb, "<PCBCALL>", tb1!origPCB 'IRIS-DB
          tb1.Close
          'IRIS-DB updates the table for the DBD name
          m_fun.FnConnection.Execute "Update IRIS_DYN_PCB set TargDBD = '" & nomeRoutine & "' where idpgm = " & rst!IdOggetto & " and TargPCB = '" & MadrdM_Incapsulatore.TabIstr.pcb & "'" 'IRIS-DB
        Else 'IRIS-DB
          MadrdM_GenRout.changeTag Rtb, "<PCBCALL>", MadrdM_Incapsulatore.TabIstr.pcb
          tb1.Close
        End If
      ElseIf MadrdM_Incapsulatore.TabIstr.istr = "PCB" Then 'IRIS-DB
        Set tb1 = m_fun.Open_Recordset("select Statement from psdli_istruzioni where idoggetto = " & rst!IdOggetto & " and riga = " & rst!Riga) 'IRIS-DB
        If Not tb1.EOF Then 'IRIS-DB
          If Len(tb1!statement) Then 'IRIS-DB
            s = Split(tb1!statement, "ADDRESS ") 'IRIS-DB
            MadrdM_GenRout.changeTag Rtb, "<PCBCALL>", s(0) 'IRIS-DB
            MadrdM_GenRout.changeTag Rtb, "<RECORD>", "ADDRESS " & s(1) 'IRIS-DB
          End If 'IRIS-DB
        End If 'IRIS-DB
        tb1.Close 'IRIS-DB
      
      End If
    
     '****** fine parte embedded spostata su COMMONINIT
     End If
     
     ReDim segLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
     ReDim segLevel2(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)) 'IRIS-DB
     
     
     If SwTp Then
           '*****************************
           If m_fun.FnNomeDB = "Prj-Perot-POC.mty" Then  ' variante PEROT
            If MadrdM_Incapsulatore.TabIstr.istr = "PCB" Or MadrdM_Incapsulatore.TabIstr.istr = "CHKP" Or MadrdM_Incapsulatore.TabIstr.istr = "ROLB" Or MadrdM_Incapsulatore.TabIstr.istr = "TERM" Then
            'come versione batch
              MadrdM_GenRout.changeTag Rtb, "<SWTPAREA>", "LNKDB-AREA"
            End If
           End If
           '*******************************
           MadrdM_GenRout.changeTag Rtb, "<SWTPAREA>", "DFHEIBLK, LNKDB-AREA"
     Else
           MadrdM_GenRout.changeTag Rtb, "<SWTPAREA>", "LNKDB-AREA"
     End If
     
     'Salvataggio parziale
     appoFinalString = Rtb.text
    
  
     templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLVREV"
     Rtb.LoadFile templateName
     If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) > 0 Then
      If (MadrdM_Incapsulatore.TabIstr.istr = "GU" Or MadrdM_Incapsulatore.TabIstr.istr = "GN" Or MadrdM_Incapsulatore.TabIstr.istr = "GNP" Or _
        MadrdM_Incapsulatore.TabIstr.istr = "GHN" Or MadrdM_Incapsulatore.TabIstr.istr = "GHNP" Or MadrdM_Incapsulatore.TabIstr.istr = "GHU" Or MadrdM_Incapsulatore.TabIstr.istr = "POS") Then 'IRIS-DB
        'tag <RD> su SEGMENTLVREV
        MadrdM_GenRout.changeTag Rtb, "<RD>", ""
        MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr), "#0")
        
        'AC 06/10/2010
        If BolPtr Then
          MadrdM_GenRout.changeTag Rtb, "<POINTER>", ""
        Else
          MadrdM_GenRout.changeTag Rtb, "<STD>", ""
        End If
        
        If (Not MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).recordReaddress = "" And Not BolPtr) Then
          getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).recordReaddress), rst!IdOggetto, 0, dataType, "?USAGE", -1
          If dataType = "XVAR" Then
            AggLog "[Object: " & rst!IdOggetto & "] WARNING - the type of variable " & MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).recordReaddress & _
                                                                        " is VARCHAR.", MadrdF_Incapsulatore.RTErr
          End If
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).recordReaddress)
        Else
          getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record), rst!IdOggetto, 0, dataType, "?USAGE", -1
          If dataType = "XVAR" Then
            AggLog "[Object: " & rst!IdOggetto & "] WARNING - the type of variable " & MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record & _
                                                                        " is VARCHAR.", MadrdF_Incapsulatore.RTErr
          End If
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record)
        End If
        
        
        If Not MadrdM_Incapsulatore.TabIstr.istr = "ISRT" Then
         MadrdM_GenRout.changeTag Rtb, "<RECORD>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record
        End If
        If MadrdM_Incapsulatore.TabIstr.isGsam Then
          MadrdM_GenRout.changeTag Rtb, "<RECLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).areaLen
        End If
      End If
     End If
     segLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)) = Rtb.text
     segLevel2(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)) = Rtb2.text 'IRIS-DB
     '***********************************************************************
     If MadrdM_Incapsulatore.TabIstr.istr = "XRST" Or MadrdM_Incapsulatore.TabIstr.istr = "CHKP" Then
      ReDim arrSavedArea(UBound(MadrdM_Incapsulatore.TabIstr.SavedAreas))
      ReDim arrSavedAreaRet(UBound(MadrdM_Incapsulatore.TabIstr.SavedAreas))
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SAVEAREA"
      For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.SavedAreas)
        Rtb.LoadFile templateName
        If UBound(MadrdM_Incapsulatore.TabIstr.SavedAreasLen) >= k Then
          MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", MadrdM_Incapsulatore.TabIstr.SavedAreasLen(k)
        Else
          'MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", "" ' TO DO
        End If
        MadrdM_GenRout.changeTag Rtb, "<SAVEAREA(i)>", MadrdM_Incapsulatore.TabIstr.SavedAreas(k)
        MadrdM_GenRout.changeTag Rtb, "<i>", k
        arrSavedArea(k) = Rtb.text
      Next k
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IDAREA"
      Rtb.LoadFile templateName
      If Len(MadrdM_Incapsulatore.TabIstr.IdAreaLen) Then
          MadrdM_GenRout.changeTag Rtb, "<LENAREA>", MadrdM_Incapsulatore.TabIstr.IdAreaLen
      End If
      MadrdM_GenRout.changeTag Rtb, "<IDAREA>", MadrdM_Incapsulatore.TabIstr.IdArea
      For k = 1 To UBound(arrSavedArea)
        MadrdM_GenRout.changeTag Rtb, "<<AREE(i)>>", arrSavedArea(k)
      Next
      appoXRST = Rtb.text
      '------------------------------------------
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SAVEAREARET"
        For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.SavedAreas)
          Rtb.LoadFile templateName
          If UBound(MadrdM_Incapsulatore.TabIstr.SavedAreasLen) >= k Then
            MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", MadrdM_Incapsulatore.TabIstr.SavedAreasLen(k)
          Else
            'MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", "" ' TO DO
          End If
          MadrdM_GenRout.changeTag Rtb, "<SAVEAREA(i)>", MadrdM_Incapsulatore.TabIstr.SavedAreas(k)
          MadrdM_GenRout.changeTag Rtb, "<i>", k
          arrSavedAreaRet(k) = Rtb.text
        Next
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IDAREARET"
        Rtb.LoadFile templateName
        If Len(MadrdM_Incapsulatore.TabIstr.IdAreaLen) Then
            MadrdM_GenRout.changeTag Rtb, "<LENAREA>", MadrdM_Incapsulatore.TabIstr.IdAreaLen
        End If
        MadrdM_GenRout.changeTag Rtb, "<IDAREA>", MadrdM_Incapsulatore.TabIstr.IdArea
        For k = 1 To UBound(arrSavedAreaRet)
          MadrdM_GenRout.changeTag Rtb, "<<AREERET(i)>>", arrSavedAreaRet(k)
        Next
        appoXRSTret = Rtb.text
      
    End If
         
     Rtb.text = appoFinalString
     
     If Len(appoXRST) Then
        MadrdM_GenRout.changeTag Rtb, "<<IDAREA>>", appoXRST
     End If
     If Len(appoXRSTret) Then
        MadrdM_GenRout.changeTag Rtb, "<<IDAREARET>>", appoXRSTret
     End If
     'AC 24/10/07
'     For K = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
'      MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLVREV(i)>>", segLevel(K)
'     Next
      'AC 19/03/09
      If BolNoRev Then
        segLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)) = Replace(segLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)), "IRIS_", "")
      End If
      MadrdM_GenRout.changeTag Rtb, "<SEGMENTLVREV>", segLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
      
     'testoCall = testoCall & CreaSp(wIdent) & vbCrLf
     
     If MadrdM_Incapsulatore.TabIstr.keyfeedback <> "" Then
        getAreaType Trim(MadrdM_Incapsulatore.TabIstr.keyfeedback), rst!IdOggetto, 0, dataType, "", -1
        If dataType = "X" Then
            MadrdM_GenRout.changeTag Rtb, "<REVFBK>", ""
        Else
            MadrdM_GenRout.changeTag Rtb, "<REVFBK>", "IRIS_"
        End If
        MadrdM_GenRout.changeTag Rtb, "<KEYFDBCK>", Trim(MadrdM_Incapsulatore.TabIstr.keyfeedback)
     End If
     
     If MadrdM_Incapsulatore.TabIstr.isGsam Then
        MadrdM_GenRout.changeTag Rtb, "DIBSTAT<Conditioned1>", "$STATUS_CODE"
     End If
     
     If Not MadrdM_Incapsulatore.TabIstr.istr = "TERM" Then  ' and Len(MadrdM_Incapsulatore.TabIstr.PCB)
       ' scrivo move anche se non ho pcb
       MadrdM_GenRout.changeTag Rtb, "<DLZPCB>", MadrdM_Incapsulatore.TabIstr.pcb
     End If
     
    ' tag a disposizione in particolare per PLI)
     For i = 1 To UBound(routinePLI)
      If nomeRoutine = routinePLI(i) Then
        Exit For
      End If
     Next
     If i = UBound(routinePLI) + 1 Then
      ReDim Preserve routinePLI(UBound(routinePLI) + 1)
      routinePLI(UBound(routinePLI)) = nomeRoutine
      'MadrdM_GenRout.changeTag Rtb, "<XEPROGRAMMA>", nomeroutine
     End If
     If Len(nomeRoutine) < 8 Then
        xprogramm = Space(8 - Len(nomeRoutine))
     Else
        xprogramm = ""
        If InStr(m_fun.FnNomeDB, "prj-GRZ") Then
           nomeRoutine = IIf(Len(nomeRoutine) > 7, Left(nomeRoutine, 7), nomeRoutine)
        End If
     End If
     If Not (isMoltIstr Or isMoltPcb) Then
        MadrdM_GenRout.changeTag Rtb, "<if not molt istr>", ""
     End If
     MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & xprogramm
     NumParam = UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr) + 3 'IRIS-DB : num-param = ssa number + iris area + pcb + io area
     MadrdM_GenRout.changeTag Rtb, "<NUMPARAM>", CStr(NumParam)
     MadrdM_GenRout.changeTag Rtb, "<CALLID>", CStr(seqInstr)
     MadrdM_GenRout.changeTag Rtb, "<VARPCB>", MadrdM_Incapsulatore.TabIstr.pcb
     If Len(MadrdM_Incapsulatore.TabIstr.Pcbred) Then
      MadrdM_GenRout.changeTag Rtb, "<XREDEFPCB>", MadrdM_Incapsulatore.TabIstr.Pcbred
      End If
      If opencomment Then
         MadrdM_GenRout.changeTag Rtb, "<opencomment>", opencommentstr
      End If
     
     'SILVIA 9-6-2008
      If MadrdM_Incapsulatore.TabIstr.istr = "GU" Or MadrdM_Incapsulatore.TabIstr.istr = "GN" Or MadrdM_Incapsulatore.TabIstr.istr = "GNP" Or _
        MadrdM_Incapsulatore.TabIstr.istr = "GHU" Or MadrdM_Incapsulatore.TabIstr.istr = "GHN" Or MadrdM_Incapsulatore.TabIstr.istr = "GHNP" Or MadrdM_Incapsulatore.TabIstr.istr = "POS" Then 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<Conditioned2>", ""
''         If TipoFile = "PLI" Or TipoFile = "INC" Then
''           MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", "DIBSEGM = LNKDB_PCBSEGNM;"
''         Else
''           MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", "MOVE LNKDB_PCBSEGNM TO DIBSEGM"
''         End If
''      Else
''         MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", ""
      End If
    ''
     
     appoFinalString = Rtb.text
    ' sostituzione
    
'    If TipoFile = "PLI" Or TipoFile = "INC" Then 'IRIS-DB
'      s = Split(testoCall, vbCrLf) 'IRIS-DB
'      testoCall = "" 'IRIS-DB
'      For j = 0 To UBound(s) 'IRIS-DB
'        insertLineOF testoCall, s(j), rst!IdOggetto, indentkey - 1 'IRIS-DB EMPIRICO
'      Next 'IRIS-DB
'    End If 'IRIS-DB
    
    s = Split(appoFinalString, vbCrLf)
    For j = 0 To UBound(s)
      insertLineOF testoCall, s(j), rst!IdOggetto, indentkey
    Next
     'IRIS-DB fix for calls without SSA
    If Not hasSsa Then
       testoCall = Replace(testoCall, MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record & _
                   ",", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record & ");") 'IRIS-DB
    End If 'IRIS-DB
    
     CostruisciCall_CALLTemplate_OF = testoCall
     s = Split(CostruisciCall_CALLTemplate_OF, vbCrLf)
    If s(UBound(s)) = "" Then                                   'IRIS-DB
      ReDim Preserve s(UBound(s) - 1)                           'IRIS-DB
      CostruisciCall_CALLTemplate_OF = Left(CostruisciCall_CALLTemplate_OF, Len(CostruisciCall_CALLTemplate_OF) - 2) 'IRIS-DB
    End If                                                      'IRIS-DB
     
     
     AddRighe = AddRighe + UBound(s) + 1 'IRIS-DB PDMC
  Exit Function
catch:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & rst!IdOggetto & "] Template: " & templateName & " or " & templateName2 & " not found.", MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & rst!IdOggetto & "] unexpected error on 'CostruisciOperazioneTemplate'.", MadrdF_Incapsulatore.RTErr
  End If
  Resume Next
End Function


Function CostruisciCall_CALLTemplateOF_EXEC(pTesto As String, wIstruzione, typeIncaps As String, wIstrDli As String, rst As Recordset, RTBErr As RichTextBox, Optional iscopy As Boolean = False, Optional seqInstr As Integer = 0) As String
  Dim k As Integer, K1 As Integer, k2 As Integer, i As Integer
  Dim wstr As String, wStr1 As String, nPcb As String, testoCall As String
  Dim s() As String
  Dim wKeyName As String
  'Parametri incapsulatore
  Dim pmNomeDbd As String, pmCallRout As String, pmDecIstr As String
  Dim Rtb  As RichTextBox, wPath As String, appoFinalString As String, posEndLine As String
  Dim segLevel() As String, levelKey() As String, appoLevelString As String
  Dim templateName As String
  Dim appoXRST As String, appoXRSTret As String
  Dim rsOccurs As Recordset, Parentesi As Integer
  Dim BolNoRev As Boolean
  Dim dataType As String
  Dim CMPAT_Yes As Boolean
  Dim psbName As String
  Dim rsFeedback As Recordset
    
  On Error GoTo catch
  
  testoCall = testoCall & pTesto
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\MAINCALL_EXEC" & IIf(EmbeddedRoutine, "_EMBEDDED", "")
  
  Rtb.LoadFile templateName
  
  'GSAM fa un giro particolare
  If MadrdM_Incapsulatore.TabIstr.isGsam Then
    ' purtroppo va impostata alla OPEN, mentre la psdli_istrdett_liv non c'� per la open
    ' bisogna fare la seguente forzatura
    ' E' una grossa forzatura, ma evita di cambiare troppe cose
    Dim tb1 As Recordset
    Set tb1 = m_fun.Open_Recordset("select dataarea from psdli_istrdett_liv as a, psdli_istruzioni as b where a.idoggetto = b.idoggetto and a.riga = b.riga and b.idoggetto = " & rst!IdOggetto & " and numpcb = '" & MadrdM_Incapsulatore.TabIstr.pcb & "'")
    If Not tb1.EOF Then
      If Len(tb1!DataArea) Then
        MadrdM_GenRout.changeTag Rtb, "<RECLENGSAM>", tb1!DataArea
      End If
    End If
    tb1.Close
  End If
  If isPliCallinsideSelect Then
    MadrdM_GenRout.changeTag Rtb, "<insideselect>", ""
  End If
  'If Not EmbeddedRoutine Then
  '***** per embedded spostato su commoninit
  If Not MadrdM_Incapsulatore.TabIstr.pcb = "" Then
    MadrdM_GenRout.changeTag Rtb, "<PCB>", MadrdM_Incapsulatore.TabIstr.pcb
    Rtb.text = Replace(Rtb.text, "LNKDB_FEEDBACK", "IRIS_PCBS(LNKDB_NUMPCB)")
  End If
  If Not MadrdM_Incapsulatore.TabIstr.numpcb = 0 And Not MadrdM_Incapsulatore.TabIstr.istr = "TERM" Then
    MadrdM_GenRout.changeTag Rtb, "<NUMPCB>", MadrdM_Incapsulatore.TabIstr.numpcb
  End If
   
  'AC 30/08/10 valorizzazioni di DLZDIB eliminate per la TERM
  If Not MadrdM_Incapsulatore.TabIstr.istr = "TERM" Then
    MadrdM_GenRout.changeTag Rtb, "<NOTERM>", ""
  End If
  If MadrdM_Incapsulatore.TabIstr.istr = "TERM" Then 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<TERMIRIS>", "" 'IRIS-DB
  End If 'IRIS-DB
   
  If MadrdM_Incapsulatore.TabIstr.istr = "SCHED" Then 'IRIS-DB
    Set tb1 = m_fun.Open_Recordset("select Statement from psdli_istruzioni where idoggetto = " & rst!IdOggetto & " and riga = " & rst!Riga) 'IRIS-DB
    If Not tb1.EOF Then 'IRIS-DB
      If Len(tb1!statement) Then 'IRIS-DB
        psbName = Replace(Replace(tb1!statement, "(", ""), ")", "") 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<PSBNAME>", psbName 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<SCHEDIRIS>", "" 'IRIS-DB
      End If 'IRIS-DB
    End If 'IRIS-DB
    tb1.Close 'IRIS-DB
  End If
   
   
  'MadrdM_GenRout.changeTag Rtb, "<SEGMENT> ", MadrdM_Incapsulatore.TabIstr.BlkIstr(1).Segment
  'MadrdM_GenRout.changeTag Rtb, "<SEGLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(1).SegLen
  'IRIS-DB If Not (MadrdM_Incapsulatore.TabIstr.istr = "GU" Or MadrdM_Incapsulatore.TabIstr.istr = "GN" Or MadrdM_Incapsulatore.TabIstr.istr = "GNP" Or MadrdM_Incapsulatore.TabIstr.istr = "GHNP" Or MadrdM_Incapsulatore.TabIstr.istr = "GHU") Then
  If MadrdM_Incapsulatore.TabIstr.istr = "XRST" Or MadrdM_Incapsulatore.TabIstr.istr = "CHKP" Or MadrdM_Incapsulatore.TabIstr.istr = "SYMCHKP" Then 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<AREAINTO>", "IRIS-DUMMY-AREA"
  Else
    'MadrdM_GenRout.changeTag Rtb, "<AREAINTO>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record
  End If
  MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
  If InStr(m_fun.FnNomeDB, "prj-GRZ") Then
    nomeRoutine = IIf(Len(nomeRoutine) > 7, Left(nomeRoutine, 7), nomeRoutine)
  End If
  
  If Not (isMoltIstr Or isMoltPcb) Then
    MadrdM_GenRout.changeTag Rtb, "<if not molt istr>", ""
  End If
  
  If Len(nomeRoutine) < 8 And InStr(m_fun.FnNomeDB, "prj-GRZ") = 0 Then
    MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & Space(8 - Len(nomeRoutine))
  Else
    MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine
  End If
  If Len(nomeRoutine) < 8 Then
    MadrdM_GenRout.changeTag Rtb, "<PROGRAMMAF8>", nomeRoutine & Space(8 - Len(nomeRoutine))
  Else
    MadrdM_GenRout.changeTag Rtb, "<PROGRAMMAF8>", nomeRoutine
  End If
  If Len(MadrdM_Incapsulatore.TabIstr.Pcbred) Then
    MadrdM_GenRout.changeTag Rtb, "<XREDEFPCB>", MadrdM_Incapsulatore.TabIstr.Pcbred
  End If
  
  'IRIS-DB: supporto della key feedback area
  If Len(MadrdM_Incapsulatore.TabIstr.keyfeedback) Then
     MadrdM_GenRout.changeTag Rtb, "<KFB_SET>", "SET IRIS-KFB-YES TO TRUE"
    'IRIS-DB: forse sarebbe meglio includerlo nella Tabistr, ma � troppo rischioso, bisognerebbe cambiare
    '         tutte le sue propagazioni; lo prendo da rst che comunque � sempre in linea (potrebbe anche
    '         essere usato sempre lui, invece di Tabistr, bah)
    If Trim(rst!KeyfeedBackLen) <> "" Then
      MadrdM_GenRout.changeTag Rtb, "<KFB_LEN_SET>", "MOVE " & Trim(rst!KeyfeedBackLen) & " TO IRIS-KFB-LENGTH"
    Else 'IRIS-DB: it seems that without a specific length, it sets the length of the area by default
      MadrdM_GenRout.changeTag Rtb, "<KFB_LEN_SET>", "MOVE LENGTH OF " & Trim(MadrdM_Incapsulatore.TabIstr.keyfeedback) & " TO IRIS-KFB-LENGTH"
    End If
    MadrdM_GenRout.changeTag Rtb, "<KFB_EXEC>", MadrdM_Incapsulatore.TabIstr.keyfeedback
  End If
  
'  If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(1).KeyDef) Then
'    MadrdM_GenRout.changeTag Rtb, "<KEYNAME>", MadrdM_Incapsulatore.TabIstr.BlkIstr(1).KeyDef(1).key
'    MadrdM_GenRout.changeTag Rtb, "<KEYLEN> ", MadrdM_Incapsulatore.TabIstr.BlkIstr(1).KeyDef(1).KeyLen
'    MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", MadrdM_Incapsulatore.TabIstr.BlkIstr(1).KeyDef(1).KeyVal
'    MadrdM_GenRout.changeTag Rtb, "<KEYOPER>", MadrdM_Incapsulatore.TabIstr.BlkIstr(1).KeyDef(1).op
'  End If
  '+++++++++++++++++++++++++++++++++++++++++++++++
  appoFinalString = Rtb.text
  ReDim segLevel(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
    
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLV_EXEC"
    Rtb.LoadFile templateName
    
    If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment = "" Then
      'AggLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!Riga & " missing segment", MadrdF_Incapsulatore.RTErr
    End If
     
    Dim h As Integer
    If UBound(MadrdM_Incapsulatore.TabIstr.psb) Then
      ' Mauro 16/12/2008
      If MadrdM_Incapsulatore.TabIstr.numpcb = 0 And UBound(MadrdM_Incapsulatore.TabIstr.DBD) Then
        h = Restituisci_Livello_Segmento_dbd(MadrdM_Incapsulatore.TabIstr.DBD(1).Id, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment)
      Else
        If k = 1 Then
          ' Mauro 04/11/2009: Se il PGM � un CICS, CMPAT va ignorato!!
          CMPAT_Yes = IsCMPAT(MadrdM_Incapsulatore.TabIstr.psb(1).Id, rst!IdOggetto)
          If Not CMPAT_Yes Then MadrdM_Incapsulatore.TabIstr.numpcb = MadrdM_Incapsulatore.TabIstr.numpcb + 1
        End If
        h = Restituisci_Livello_Segmento(MadrdM_Incapsulatore.TabIstr.psb(1).Id, MadrdM_Incapsulatore.TabIstr.numpcb, MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment, 0)
      End If
    Else
      'stefano: istruzioni senza psb, assegna comunque un default
      'h = -1
      If UBound(MadrdM_Incapsulatore.TabIstr.DBD) Then
        'MG
        'h = Restituisci_Livello_Segmento_dbd(MadrdM_Incapsulatore.TabIstr.dbd(1).Id, MadrdM_Incapsulatore.TabIstr.BlkIstr(K).Segment, 0)
        segLev = 0
        MadrdM_Incapsulatore.getSegLevel (CLng(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm))
        h = segLev - 1
      Else
        h = -1
      End If
    End If
    If Not MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm = 0 Then
        If IsSegObsolete(CLng(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).idsegm)) Then
            AggLog "[Object: " & rst!IdOggetto & "] Riga: " & rst!Riga & ", segment " & MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment & " is obsolete", MadrdF_Incapsulatore.RTErr
        End If
    End If
    If h >= 0 Then
      MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
    Else
      m_fun.WriteLog "Incaps: Obj(" & rst!IdOggetto & ") Row( " & rst!Riga & " missing level", "MadrdM_Incapsulatore"
      MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
    End If
    MadrdM_GenRout.changeTag Rtb, "<LEVEL>", k
    MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", MadrdM_Incapsulatore.TabIstr.istr 'IRIS-DB
    If MadrdM_Incapsulatore.TabIstr.istr <> "PCB" And MadrdM_Incapsulatore.TabIstr.istr <> "SCHE" Then 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", CInt(Right(TabDec(1).Codifica, 6)) 'IRIS-DB
    End If 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<SEGMENT>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).segment
    MadrdM_GenRout.changeTag Rtb, "<SEGLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).SegLen
   
    appoLevelString = Rtb.text
    'AC 21/05/08
    appoLevelString = Replace(appoLevelString, "IRIS_$", "IRIS_")
   
    ReDim levelKey(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef))
    If UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef) > 0 Then
      For K1 = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef)
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\LEVKEY_EXEC"
        Rtb.LoadFile templateName
        'IRIS-DB MadrdM_GenRout.changeTag Rtb, "<KEYLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen
        If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen) Then 'IRIS-DB
        'IRIS-DB solo se numerico
          If IsNumeric(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen) Then 'IRIS-DB
            If MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen > 0 Then 'IRIS-DB
              MadrdM_GenRout.changeTag Rtb, "<KEYLEN>", "(1:" & MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyLen & ")" 'IRIS-DB
            End If 'IRIS-DB
          End If 'IRIS-DB
        End If 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<KEYNAME>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).key
        If InStr(Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), "(") Then
          ' Mauro 05/08/2008
          Parentesi = InStr(Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal) & "(", "(")
          Set rsOccurs = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where " & _
                        "idoggetto = " & rst!IdOggetto & " and nomecmp = '" & Left(Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), Parentesi - 1) & "'")
          If rsOccurs.RecordCount = 0 Then
            MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal)
            If Left(Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), 1) = "'" And Right(Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), 1) = "'" Then
              MadrdM_GenRout.changeTag Rtb, "<REVK>", ""
            Else
              MadrdM_GenRout.changeTag Rtb, "<REVK>", "IRIS_"
            End If
          Else
            If rsOccurs!pointer Then
              Rtb.text = "IRIS_PTR = ADDR(" & MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal & ");" & vbCrLf & Rtb.text
              MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", Left(Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), Parentesi - 1)
              MadrdM_GenRout.changeTag Rtb, "<REVK>", "IRIS_"
            Else
              MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal)
              MadrdM_GenRout.changeTag Rtb, "<REVK>", ""
            End If
          End If
          rsOccurs.Close
        Else
          MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal)
          If Left(Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), 1) = "'" And Right(Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), 1) = "'" Then
            MadrdM_GenRout.changeTag Rtb, "<REVK>", ""
          Else
            ' Mauro 15/01/2009: TMP - Calcolare la lista dei IRIS_ una sola volta all'inizio
            'Dim dataType As String
            getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), rst!IdOggetto, 0, dataType, "", -1
            If dataType = "X" Then
              MadrdM_GenRout.changeTag Rtb, "<REVK>", ""
            Else
              MadrdM_GenRout.changeTag Rtb, "<REVK>", "IRIS_"
            End If
          End If
        End If
        MadrdM_GenRout.changeTag Rtb, "<KEYOPER>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).KeyDef(K1).op
        MadrdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(k, "#0")
        MadrdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(K1, "#0")
        'End If
        levelKey(K1) = Rtb.text
      Next K1
     
      ' riprendo da SEGMENTLV
      Rtb.text = appoLevelString
   
      For i = 1 To K1 - 1
        'AC 21/05/08
        levelKey(i) = Replace(levelKey(i), "IRIS_$", "IRIS_")
        MadrdM_GenRout.changeTag Rtb, "<<LEVKEY_EXEC(i)>>", levelKey(i)
      Next i
    ElseIf InStr(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Search, "C") > 0 Then
      ReDim levelKey(1)
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\LEVKEY_EXEC"
      Rtb.LoadFile templateName
      MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", GetKeysFromId(rst!IdOggetto, rst!idpgm, rst!Riga)
      MadrdM_GenRout.changeTag Rtb, "<IDXLEVEL>", "1"
      MadrdM_GenRout.changeTag Rtb, "<IDXKEY>", "1"
      levelKey(1) = Rtb.text
      
      ' riprendo da SEGMENTLV
      Rtb.text = appoLevelString
      'AC 21/05/08
      levelKey(1) = Replace(levelKey(1), "IRIS_$", "IRIS_")
      ' Mauro 16/11/2010: TMP - Calcolare la lista dei IRIS_ una sola volta all'inizio
      getAreaType Trim(GetKeysFromId(rst!IdOggetto, rst!idpgm, rst!Riga)), rst!IdOggetto, 0, dataType, "", -1
      If dataType = "X" Then
        levelKey(1) = Replace(levelKey(1), "<REVK>", "")
      Else
        levelKey(1) = Replace(levelKey(1), "<REVK>", "IRIS_")
      End If
      'AC 06/10/2008
      'levelKey(1) = Replace(levelKey(1), "<REVK>", "")
      MadrdM_GenRout.changeTag Rtb, "<<LEVKEY_EXEC(i)>>", levelKey(1)
    End If
               
    ' <XRECORD> A1 (insieme ad A2 alternativo a B)
    'AC 07/01/08  versione move area dentro SEGMENTLV per tutti i livelli
    If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record) Then
      'AC tolta il 19/05/08
      'If Not (MadrdM_Incapsulatore.TabIstr.istr = "GU" Or MadrdM_Incapsulatore.TabIstr.istr = "GN" Or MadrdM_Incapsulatore.TabIstr.istr = "GNP") Then
      ' Mauro 07/08/2008 : Nuova gestione per Occurs
      Parentesi = InStr(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record, "(")
      If Parentesi Then
        Set rsOccurs = m_fun.Open_Recordset("select * from mgdata_cmp_DCL where " & _
                        "idoggetto = " & rst!IdOggetto & " and nomecmp = '" & Trim(Left(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record, Parentesi - 1)) & "'")
        If rsOccurs.RecordCount = 0 Then
          If Not MadrdM_Incapsulatore.TabIstr.BlkIstr(k).recordReaddress = "" Then
            MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).recordReaddress)
          Else
            MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record)
          End If
          'MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
          'AC 20/03/09
          getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record), rst!IdOggetto, 0, dataType, "", -1
          BolNoRev = False
          If dataType = "X" Then
            MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
            BolNoRev = True
          Else
             MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
          End If
        Else
          If rsOccurs!pointer Then
            Rtb.text = "IRIS_PTR = ADDR(" & MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record & ");" & vbCrLf & Rtb.text
            MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(Left(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record, Parentesi - 1))
            MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
          Else
            MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record)
            MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
          End If
        End If
      Else
        If Not MadrdM_Incapsulatore.TabIstr.BlkIstr(k).recordReaddress = "" Then
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).recordReaddress)
        Else
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record)
        End If
        'MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
        'AC 20/03/09
        getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record), rst!IdOggetto, 0, dataType, "", -1
        BolNoRev = False
        If dataType = "X" Then
          MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
          BolNoRev = True
        Else
           MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
        End If
      End If
      MadrdM_GenRout.changeTag Rtb, "<RECORD>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record
      MadrdM_GenRout.changeTag Rtb, "<AREAINTO>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record
      'End If
      MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(k, "#0")
    End If
    segLevel(k) = Rtb.text
    segLevel(k) = Replace(segLevel(k), "<REV>", "REV")
    segLevel(k) = Replace(segLevel(k), "IRIS_$", "IRIS_")
  Next k
  
  If MadrdM_Incapsulatore.TabIstr.istr = "XRST" Or MadrdM_Incapsulatore.TabIstr.istr = "CHKP" Or MadrdM_Incapsulatore.TabIstr.istr = "SYMCHKP" Then
    Dim arrSavedArea() As String, arrSavedAreaRet() As String
    ReDim arrSavedArea(UBound(MadrdM_Incapsulatore.TabIstr.SavedAreas))
    ReDim arrSavedAreaRet(UBound(MadrdM_Incapsulatore.TabIstr.SavedAreas))
    
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SAVEAREA_EXEC" 'IRIS-DB
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.SavedAreas)
      Rtb.LoadFile templateName
      'IRIS-DB If UBound(MadrdM_Incapsulatore.TabIstr.SavedAreasLen) >= k Then
      'IRIS-DB  MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", MadrdM_Incapsulatore.TabIstr.SavedAreasLen(k)
      'IRIS-DB Else
        'MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", "" ' TO DO
      'IRIS-DB End If
      MadrdM_GenRout.changeTag Rtb, "<SAVEAREA(i)>", MadrdM_Incapsulatore.TabIstr.SavedAreas(k)
      MadrdM_GenRout.changeTag Rtb, "<i>", k
      arrSavedArea(k) = Rtb.text
    Next
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IDAREA"
    Rtb.LoadFile templateName
    If Len(MadrdM_Incapsulatore.TabIstr.IdAreaLen) Then
      MadrdM_GenRout.changeTag Rtb, "<LENAREA>", MadrdM_Incapsulatore.TabIstr.IdAreaLen
    End If
    'IRIS-DB MadrdM_GenRout.changeTag Rtb, "<IDAREA>", MadrdM_Incapsulatore.TabIstr.IdArea
    For k = 1 To UBound(arrSavedArea)
      MadrdM_GenRout.changeTag Rtb, "<<AREE(i)>>", arrSavedArea(k)
    Next
    appoXRST = Rtb.text
    '--------------------------------
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SAVEAREARET"
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.SavedAreas)
      Rtb.LoadFile templateName
      If UBound(MadrdM_Incapsulatore.TabIstr.SavedAreasLen) >= k Then
        MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", MadrdM_Incapsulatore.TabIstr.SavedAreasLen(k)
      Else
        'MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", "" ' TO DO
      End If
      MadrdM_GenRout.changeTag Rtb, "<SAVEAREA(i)>", MadrdM_Incapsulatore.TabIstr.SavedAreas(k)
      MadrdM_GenRout.changeTag Rtb, "<i>", k
      arrSavedAreaRet(k) = Rtb.text
    Next
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IDAREARET"
    Rtb.LoadFile templateName
    If Len(MadrdM_Incapsulatore.TabIstr.IdAreaLen) Then
      MadrdM_GenRout.changeTag Rtb, "<LENAREA>", MadrdM_Incapsulatore.TabIstr.IdAreaLen
    End If
    'IRIS-DB MadrdM_GenRout.changeTag Rtb, "<IDAREA>", MadrdM_Incapsulatore.TabIstr.IdArea
    For k = 1 To UBound(arrSavedAreaRet)
      MadrdM_GenRout.changeTag Rtb, "<<AREERET(i)>>", arrSavedAreaRet(k)
    Next
    appoXRSTret = Rtb.text
  End If
     
  'IRIS-DB all next IF is new, it could be cleaned better...
  If MadrdM_Incapsulatore.TabIstr.istr = "XRST" Or MadrdM_Incapsulatore.TabIstr.istr = "CHKP" Or MadrdM_Incapsulatore.TabIstr.istr = "SYMCHKP" Then
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLV2_EXEC" 'IRIS-DB
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.SavedAreas)
      Rtb.LoadFile templateName
      MadrdM_GenRout.changeTag Rtb, "<SAVEAREA(i)>", MadrdM_Incapsulatore.TabIstr.SavedAreas(k)
      MadrdM_GenRout.changeTag Rtb, "<i>", k
      arrSavedArea(k) = Rtb.text
    Next
  Else
  'IRIS-DB override any former area substitution, this should also be cleaned up, but for now it is ok
    ReDim arrSavedArea(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr))
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLV2_EXEC" 'IRIS-DB
    For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
      If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record) > 0 Then
        Rtb.LoadFile templateName
        MadrdM_GenRout.changeTag Rtb, "<SAVEAREA(i)>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record
        arrSavedArea(k) = Rtb.text
      End If
    Next
  End If
  'recuper maincall
  Rtb.text = appoFinalString
  If Len(appoXRST) Then
    MadrdM_GenRout.changeTag Rtb, "<<IDAREA>>", appoXRST
    MadrdM_GenRout.changeTag Rtb, "<IDAREA>", MadrdM_Incapsulatore.TabIstr.IdArea 'IRIS-DB
    For k = 1 To UBound(arrSavedArea)
      MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV2_EXEC(i)>>", arrSavedArea(k)
    Next
  End If
  If Len(appoXRSTret) Then
    MadrdM_GenRout.changeTag Rtb, "<<IDAREARET>>", appoXRSTret
  End If
  If SwTp Then
    MadrdM_GenRout.changeTag Rtb, "<SWTPAREA>", "DFHEIBLK, "
  Else
    MadrdM_GenRout.changeTag Rtb, "<SWTPAREA>", ""
  End If
  
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
     MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV_EXEC(i)>>", segLevel(k)
     If Len(arrSavedArea(k)) Then 'IRIS-DB: multi INTO parameters
       MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV2_EXEC(i)>>", arrSavedArea(k)
     End If
  Next
  
  MadrdM_GenRout.changeTag Rtb, "<Conditioned1>", ""
  
  ' <XRECORD> A2 (insieme ad A1 alternativo a B)
  'AC 07/01/08 versione con <<SEGMENTLVIRIS_EXEC(i)>> e area passata per tutti i livelli
  appoFinalString = Rtb.text
     
  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLVREV"
    Rtb.LoadFile templateName

    '<NONE>
    If Len(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record) Then
      If (MadrdM_Incapsulatore.TabIstr.istr = "GU" Or MadrdM_Incapsulatore.TabIstr.istr = "GN" Or MadrdM_Incapsulatore.TabIstr.istr = "GNP" Or MadrdM_Incapsulatore.TabIstr.istr = "GHNP" Or MadrdM_Incapsulatore.TabIstr.istr = "GHU" Or MadrdM_Incapsulatore.TabIstr.istr = "POS") Then 'IRIS-DB
        'tag <RD> su SEGMENTLVREV
        MadrdM_GenRout.changeTag Rtb, "<RD>", ""
        MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(k, "#0")
        
        If Not MadrdM_Incapsulatore.TabIstr.BlkIstr(k).recordReaddress = "" Then
          getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).recordReaddress), rst!IdOggetto, 0, dataType, "?USAGE", -1
          If dataType = "XVAR" Then
            AggLog "[Object: " & rst!IdOggetto & "] WARNING - the type of variable " & Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).recordReaddress) & _
                                                                        " is VARCHAR.", MadrdF_Incapsulatore.RTErr
          End If
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).recordReaddress)
        Else
          getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record), rst!IdOggetto, 0, dataType, "?USAGE", -1
          If dataType = "XVAR" Then
            AggLog "[Object: " & rst!IdOggetto & "] WARNING - the type of variable " & Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record) & _
                                                                        " is VARCHAR.", MadrdF_Incapsulatore.RTErr
          End If
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record)
        End If

        'AC 20/03/09
        getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Record), rst!IdOggetto, 0, dataType, "", -1
        BolNoRev = False
        If dataType = "X" Then
          BolNoRev = True
        Else
          BolNoRev = False
        End If
        MadrdM_GenRout.changeTag Rtb, "<RECORD>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).Record
        If MadrdM_Incapsulatore.TabIstr.isGsam Then
          MadrdM_GenRout.changeTag Rtb, "<RECLEN>", MadrdM_Incapsulatore.TabIstr.BlkIstr(k).areaLen
        End If
      End If
    End If
    segLevel(k) = Rtb.text
    If BolNoRev Then
      segLevel(k) = Replace(segLevel(k), "IRIS_", "")
    End If
  Next k

  Rtb.text = appoFinalString

  For k = 1 To UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)
    segLevel(k) = Replace(segLevel(k), "IRIS_$", "IRIS_")
    MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLVREV(i)>>", segLevel(k)
  Next k
  
  If MadrdM_Incapsulatore.TabIstr.keyfeedback <> "" Then
    getAreaType Trim(MadrdM_Incapsulatore.TabIstr.keyfeedback), rst!IdOggetto, 0, dataType, "", -1
    If dataType = "X" Then
        MadrdM_GenRout.changeTag Rtb, "<REVFBK>", ""
    Else
        MadrdM_GenRout.changeTag Rtb, "<REVFBK>", "IRIS_"
    End If
    MadrdM_GenRout.changeTag Rtb, "<KEYFDBCK>", Trim(MadrdM_Incapsulatore.TabIstr.keyfeedback)
  End If
   
  'SILVIA 9-6-2008
  If MadrdM_Incapsulatore.TabIstr.istr = "GU" Or MadrdM_Incapsulatore.TabIstr.istr = "GN" Or MadrdM_Incapsulatore.TabIstr.istr = "GNP" Or _
    MadrdM_Incapsulatore.TabIstr.istr = "GHU" Or MadrdM_Incapsulatore.TabIstr.istr = "GHN" Or MadrdM_Incapsulatore.TabIstr.istr = "GHNP" Or MadrdM_Incapsulatore.TabIstr.istr = "POS" Then 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<Conditioned2>", ""
''    If TipoFile = "PLI" Or TipoFile = "INC" Then
''      MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", "DIBSEGM = LNKDB_PCBSEGNM;"
''    Else
''      MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", "MOVE LNKDB_PCBSEGNM TO DIBSEGM"
''    End If
''  Else
''    MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", ""
  End If
  ''
  appoFinalString = Rtb.text
  ' sostituzione
  
  ' tag a disposizione in particolare per PLI)
  For i = 1 To UBound(routinePLI)
    If nomeRoutine = routinePLI(i) Then
      Exit For
    End If
  Next
  If i = UBound(routinePLI) + 1 Then
    ReDim Preserve routinePLI(UBound(routinePLI) + 1)
    routinePLI(UBound(routinePLI)) = nomeRoutine
    'MadrdM_GenRout.changeTag Rtb, "<XEPROGRAMMA>", nomeroutine
  End If

  s = Split(appoFinalString, vbCrLf)
  For j = 0 To UBound(s)
    insertLineOF testoCall, s(j), rst!IdOggetto, indentkey
  Next

  CostruisciCall_CALLTemplateOF_EXEC = testoCall
  s = Split(CostruisciCall_CALLTemplateOF_EXEC, vbCrLf)
   
  ' IRIS-DB CostruisciCall_CALLTemplateOF_EXEC = Left(CostruisciCall_CALLTemplateOF_EXEC, Len(CostruisciCall_CALLTemplateOF_EXEC) - 2)
  If s(UBound(s)) = "" Then                                   'IRIS-DB
    ReDim Preserve s(UBound(s) - 1)                           'IRIS-DB
    CostruisciCall_CALLTemplateOF_EXEC = Left(CostruisciCall_CALLTemplateOF_EXEC, Len(CostruisciCall_CALLTemplateOF_EXEC) - 2) 'IRIS-DB
  End If                                                      'IRIS-DB
  
  'AddRighe = AddRighe + UBound(s)
  'IRIS-DB: reinserito
  AddRighe = AddRighe + UBound(s) + 1
   
Exit Function
catch:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & rst!IdOggetto & "] Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & rst!IdOggetto & "] unexpected error - " & err.Description, MadrdF_Incapsulatore.RTErr
  End If
  Resume Next
End Function
Function removeItemFromFITable(Source() As InfoFindAndInsert, Index As Integer) As Boolean
Dim i As Integer
    For i = Index To UBound(Source) - 1
        Source(i) = Source(i + 1)
    Next
    ReDim Preserve Source(UBound(Source) - 1)
End Function
'SQ: provvisorio per "," come separatore... farne uno con argomento opzionale...
'ritorna la parola successiva (space unico separatore)
'Considera come delimitatore di "token" le parentesi e gli apici singoli...
'Effetto Collaterale: mangia...
Function nextToken_OF(inputString As String)
  Dim level, i, j As Integer
  Dim currentChar As String
  
  'Mangio i bianchi davanti (primo giro...)
  inputString = LTrim(inputString)
  If (Len(inputString) > 0) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
     Case "("
        i = 2
        level = 1
        nextToken_OF = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
          Case "("
              level = level + 1
              nextToken_OF = nextToken_OF & currentChar
          Case ")"
              level = level - 1
              nextToken_OF = nextToken_OF & currentChar
          Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nextToken_OF = nextToken_OF & currentChar
              Else
                err.Raise 123, "nextToken_OF", "syntax error on " & inputString
                Exit Function
              End If
          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
     Case "'"
        inputString = Mid(inputString, 2)
        i = InStr(inputString, "'")
        If i Then
          nextToken_OF = Left(inputString, InStr(inputString, "'") - 1)
          inputString = Mid(inputString, InStr(inputString, "'") + 1)
        Else
          'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
          inputString = "''" & inputString
          err.Raise 123, "nextToken_OF", "syntax error on " & inputString
          Exit Function
        End If
     Case Else
        i = InStr(inputString, " ")
        j = InStr(inputString, "(")
        If ((i > 0 And j > 0 And j < i) Or (i = 0 And j > 0)) Then i = j
        If (i > 0) Then
            nextToken_OF = Left(inputString, i - 1)
            inputString = Trim(Mid(inputString, i))
        Else
            nextToken_OF = inputString
            inputString = ""
        End If
    End Select
   End If
End Function
Public Sub insertExternalEntry_OF(pIdOggetto As Long, pRTBErr As RichTextBox)
  Dim testoext As String
  Dim i As Integer
  Dim keyword As String
  Dim numRiga As Long
  
  If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
    testoext = " DCL SE0R001 ENTRY OPTIONS(COBOL);"
    testoext = testoext & Space(72 - (Len(testoext) + Len("/*" & Trim(START_TAG) & "*/"))) & "/*" & Trim(START_TAG) & "*/"
  ElseIf InStr(m_fun.FnNomeDB, "prj-GRZ") Then
    testoext = testoext & " /*" & String(62, "-") & "*" & Trim(START_TAG) & "*/" & vbCrLf & _
                          " /*" & Space(2) & "IO-ROUTINES DYNAMIC ENTRY" & Space(40) & "*/" & vbCrLf & _
                          " /*" & String(62, "-") & "*" & Trim(START_TAG) & "*/" & vbCrLf
    testoext = testoext & " DCL XRIORTN ENTRY OPTIONS(FETCHABLE);"
    testoext = testoext & Space(34 - (Len("/*" & Trim(START_TAG) & "*/"))) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
    testoext = testoext & " /*" & String(62, "-") & "*" & Trim(START_TAG) & "*/" & vbCrLf
  Else
    'SQ deve essere tutto esternalizzato!
    For i = 1 To UBound(routinePLI)
    ' IRIS-DB needed in PL/I
    '  testoext = testoext & " DCL " & routinePLI(i) & " EXTERNAL ENTRY;" & Space(43 - Len(routinePLI(i))) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
      testoext = testoext & " DCL " & routinePLI(i) & " EXTERNAL ENTRY OPTIONS(COBOL);" & vbCrLf
    Next
    'mangio fine linea finale
    If Len(testoext) Then
      testoext = Left(testoext, Len(testoext) - 2)
    End If
    testoext = testoext & vbCrLf & " /* IRISDB END ---------------------------------*/"
    'IRIS-DB: molto empirico, ma purtroppo la gestione della FITABLE rende impossibile la modifica nel posto corretto
    '         Per ora sommo due fisso (le due righe sono aggiunte una qui sopra, l'altra al ritorno da questo paragrafo
    '         e testo solo le righe "piene" perch� sono quelle in cui in precedenza l'aggiornamento era stato in alto (sopra le altre istruzioni)
    m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = OffsetEncaps + 2 where idoggetto = " & pIdOggetto & " and OffsetEncaps > 0"
    
  End If
  
  'MadrdM_GenRout.changeTag RTBMod, "<externalentry>", testoext
  'Verifico se c'� gi� item nella FItable con target DLI, in quel caso aggiungo la parte
  'delle externalentry alle 'lines_up'
  For i = 1 To UBound(FItable)
    If FItable(i).target = "DCL" Then
        FItable(i).lines_up = FItable(i).lines_up & vbCrLf & testoext
        Exit For
    End If
  Next
  If i = UBound(FItable) + 1 Then
    'nuovo item FItable sul modello della parte "INTERFACE COPY USED BY PROGRAM TO ROUTINES"
    ReDim Preserve FItable(UBound(FItable) + 1)
    keyword = "DCL"
    FItable(UBound(FItable)).CommentType = "PLI"
    FItable(UBound(FItable)).isCaseSensitive = False
    FItable(UBound(FItable)).infoModify = "PLI"
    FItable(UBound(FItable)).grouping_policy = "TERMINATOR"
    FItable(UBound(FItable)).terminator = ";"
    FItable(UBound(FItable)).target = "DCL"
    FItable(UBound(FItable)).isUnique = True ' la sostituzione vale solo per la prima occorrenza
    FItable(UBound(FItable)).lines_up = testoext
  End If
End Sub

Public Sub infoLineFromPos_OF(RTBsource As RichTextBox, pPos As Long, linea As String, nextstart As Long, indent As Integer)
  Dim pstart As Long, pend As Long
  Dim appolinea As String
  For pstart = pPos To pPos - 160 Step -1
    RTBsource.SelStart = pstart
    RTBsource.SelLength = 2
    If RTBsource.SelText = vbCrLf Then
      Exit For
    End If
  Next pstart
  ' selezione parte da prima posizione riga successiva
  pend = RTBsource.find(vbCrLf, pstart + 1)
  RTBsource.SelStart = pstart + 1
  RTBsource.SelLength = pend - pstart
  linea = RTBsource.SelText
  appolinea = LTrim(linea)
  nextstart = pend + 2
  'indent = pPos - 4 - RTBsource.SelStart
  indent = Len(linea) - Len(appolinea)
End Sub

Function Asterisca_Parameters_in_Main_OF(filerow As String, parameters As String, Optional mode As String = "BOTH") As String
    Dim s() As String, firstparam As String, lastparam As String
    Dim bottompos As String, appoREP As String, i As Integer, j As Integer, appoline As String, truncline As String
    Dim appoforblank As String, comments As String, filerowclear As String, splitComment() As String
    If parameters = "" Then
        Exit Function
    End If
'    If Len(filerow) > 72 Then
'        filerowclear = Left(filerow, 72)
'        comments = Right(filerow, Len(filerow) - 72)
'    Else
'        filerowclear = filerow
'    End If
    filerowclear = filerow
    s = Split(filerowclear, vbCrLf)
    ReDim splitComment(UBound(s))
    For i = 0 To UBound(s)
        If Len(s(i)) > 72 Then
            splitComment(i) = Right(s(i), Len(s(i)) - 72)
            s(i) = Left(s(i), 72)
        End If
    Next
        'ricostruisco linea senza commenti
    filerowclear = ""
    For i = 0 To UBound(s)
        filerowclear = filerowclear & s(i) & vbCrLf
    Next i
    filerowclear = Left(filerowclear, Len(filerowclear) - 2)
    
    s = Split(parameters, ",")
    firstparam = s(0)
    lastparam = s(UBound(s))
    bottompos = InStrRev(filerowclear, lastparam)
    If bottompos > 0 Then
        appoREP = Replace(filerowclear, firstparam, "/*" & firstparam, 1, 1)
        Asterisca_Parameters_in_Main_OF = RTrim(Replace(appoREP, lastparam, lastparam & "*/", bottompos, 1))
        Asterisca_Parameters_in_Main_OF = Left(appoREP, bottompos - 1) & Asterisca_Parameters_in_Main_OF
    End If
    s = Split(Asterisca_Parameters_in_Main_OF, vbCrLf)
    For i = 0 To UBound(s)
        s(i) = RTrim(s(i))
        If Len(s(i)) > 72 Then
            appoline = s(i)
            Do While Len(appoline) > 72
               If Not InStrRev(appoline, ",", 72) > 0 Then
                Exit Do
               Else
                truncline = truncline & Right(appoline, Len(appoline) - InStrRev(appoline, ",", 72))
                appoline = Left(appoline, InStrRev(appoline, ",", 72))
               End If
            Loop
            appoforblank = LTrim(appoline)
            If Len(appoline) > Len(appoforblank) + 1 Then
                truncline = Space(Len(appoline) - Len(appoforblank)) & truncline
            ElseIf InStr(appoline, "/*") > 0 Then
                truncline = Space(InStr(appoline, "/*") - 1) & truncline
            End If
            If Len(appoline) > 72 Or Len(truncline) > 72 Then
                mode = "LENERROR"
            Else  'scalo i valori dell'array
                ReDim Preserve s(UBound(s) + 1)
                ReDim Preserve splitComment(UBound(s) + 1)
                
                For j = i + 1 To UBound(s) - 1
                    s(j + 1) = s(j)
                    splitComment(j + 1) = splitComment(j)
                Next
                s(i) = appoline
                s(i + 1) = truncline
                splitComment(i + 1) = ""
            End If
        End If
    Next
    Asterisca_Parameters_in_Main_OF = s(0)
    If Not Trim(splitComment(0)) = "" And Not mode = "LENERROR" Then
        Asterisca_Parameters_in_Main_OF = Asterisca_Parameters_in_Main_OF & Space(72 - Len(Asterisca_Parameters_in_Main_OF)) & splitComment(0)
    End If
    For i = 1 To UBound(s)
        If Trim(splitComment(i)) = "" Then
            Asterisca_Parameters_in_Main_OF = Asterisca_Parameters_in_Main_OF & vbCrLf & s(i)
        ElseIf Not Len(s(i)) > 72 Then
            Asterisca_Parameters_in_Main_OF = Asterisca_Parameters_in_Main_OF & vbCrLf & s(i) & Space(72 - Len(s(i))) & splitComment(i)
        End If
    Next
End Function

Function PLICommentLine(filerow As String, pFileName As String, nline As Long) As String
Dim posOpenComment As Integer, preComment As String, Comment As String
Dim appofilerow As String, postcomment As String, firstcolchar As String
  firstcolchar = Left(filerow, 1)
  If Not firstcolchar = " " Then
    appofilerow = Right(filerow, Len(filerow) - 1)
  Else
    appofilerow = filerow
  End If
  If Len(appofilerow) > 72 Then
    postcomment = Mid(appofilerow, 73, Len(appofilerow))
    appofilerow = Left(appofilerow, 72)
  End If
  'non contemplato il caso in cui riga chiude commento iniziato in riga precedente
  If InStr(appofilerow, "/*") Then
    posOpenComment = InStr(appofilerow, "/*")
    preComment = Left(appofilerow, posOpenComment - 1)
    Comment = Right(appofilerow, Len(appofilerow) - posOpenComment + 1)
    If Len(RTrim(preComment)) + Len(RTrim(Comment)) < 62 Then
        PLICommentLine = " /*" & RTrim(preComment) & " */" & RTrim(Comment) 'IRIS-DB
    ElseIf Len(Trim(preComment)) + Len(RTrim(Comment)) < 62 Then
        PLICommentLine = " /*" & Trim(preComment) & " */" & RTrim(Comment) 'IRIS-DB
    ElseIf Len(Trim(preComment)) + Len(RTrim(Comment)) < 67 Then
        PLICommentLine = " /*" & Trim(preComment) & " */" & RTrim(Comment)
    Else
        PLICommentLine = appofilerow
        AggLog "[File: " & pFileName & "] Comment line: is not possible to comment line number: " & nline, MadrdF_Incapsulatore.RTErr
    End If
  ElseIf Len(RTrim(appofilerow)) < 62 Then
    PLICommentLine = " /*" & RTrim(appofilerow) & " */" 'IRIS-DB
  ElseIf Len(Trim(appofilerow)) < 62 Then
    PLICommentLine = " /*" & Trim(appofilerow) & " */"
  ElseIf Len(Trim(appofilerow)) < 67 Then
    PLICommentLine = " /*" & Trim(appofilerow) & " */"
  Else
    PLICommentLine = appofilerow
    AggLog "[File: " & pFileName & "] Comment line failure: is not possible to comment line number: " & nline, MadrdF_Incapsulatore.RTErr
  End If
  ' AC ritocchino finale
  If Len(postcomment) Then
    PLICommentLine = PLICommentLine & Space(72 - Len(PLICommentLine)) & postcomment
  End If
  'rimettiamo davanti il primo carattere
  If Not firstcolchar = " " Then
    PLICommentLine = firstcolchar & LTrim(PLICommentLine)
  End If
End Function

Function Elimina_DLI_in_Xopts_OF(filerow As String, progtype As String) As String
  Dim token As String, altrotoken As String
  Dim newrow As String, newrow2 As String
  Dim sellen As Long, postoken As Long
  Dim strcomment As String
  Dim strspace As String
  Dim SelStart As Long, i As Long, a As Long
  Dim arrDirett() As String, Header As String
  Dim arrParentesi() As String, traParentesi As String
  Dim isDotVir As Boolean, appo As String
  
  progtype = Right(progtype, Len(progtype) - 5)
  If progtype = "PLI" Then
    strcomment = " /*----------------------------------------------------------------*/"
  ElseIf progtype = "CBL" Then
    strcomment = "      *----------------------------------------------------------------*"
  End If
  
  '''''''''''''''''''''''''''''''''''''''''''''''''
  ' Mauro 01/07/2009
  ' Non posso usare lo SPLIT!!!! Ci sono le virgole dentro le parentesi
  ''arrDirett() = Split(filerow, ",")
  appo = ""
  ReDim arrDirett(0)
  filerow = RTrim(filerow)
  'If progtype = "CBL" Then
    filerow = Left(filerow, 72)
  'End If
  For i = 1 To Len(filerow)
    appo = appo & Mid(filerow, i, 1)
    If Mid(filerow, i, 1) = "," Then
      arrDirett(UBound(arrDirett)) = RTrim(Left(appo, Len(appo) - 1))
      ReDim Preserve arrDirett(UBound(arrDirett) + 1)
      appo = ""
    ElseIf Mid(filerow, i, 1) = "(" Then
      Do Until Mid(filerow, i, 1) = ")"
        i = i + 1
        appo = appo & Mid(filerow, i, 1)
      Loop
    End If
  Next i
  arrDirett(UBound(arrDirett)) = appo
  
  For i = UBound(arrDirett) To 0 Step -1
    If i = 0 Then
      arrDirett(i) = RTrim(arrDirett(i))
      Header = RTrim(Left(arrDirett(i), InStrRev(arrDirett(i), " ")))
      arrDirett(i) = Trim(Mid(arrDirett(i), InStrRev(arrDirett(i), " ")))
    End If
    If i = UBound(arrDirett) Then
      arrDirett(i) = Trim(arrDirett(i))
      isDotVir = False
      If Right(arrDirett(i), 1) = ";" Then
        arrDirett(i) = Left(arrDirett(i), Len(arrDirett(i)) - 1)
        isDotVir = True
      End If
    End If
    If InStr(arrDirett(i), "(") Then
      traParentesi = Mid(arrDirett(i), InStr(arrDirett(i), "("))
      traParentesi = Replace(Replace(traParentesi, "(", ""), ")", "")
      arrParentesi() = Split(traParentesi, ",")
      For a = UBound(arrParentesi) To 0 Step -1
        If Trim(arrParentesi(a)) = "FDUMP" Or _
           Trim(arrParentesi(a)) = "OFFSET" Or _
           Trim(arrParentesi(a)) = "SYM" Or _
           Trim(arrParentesi(a)) = "UE" Or _
           Trim(arrParentesi(a)) = "DLI" Then
          arrParentesi(a) = ""
        End If
      Next a
      ' RIATTACCO I PEZZI DELL'ARRAY CHE AVEVO SPLITTATO PRIMA
      traParentesi = ""
      For a = 0 To UBound(arrParentesi)
        If arrParentesi(a) <> "" Then
          traParentesi = traParentesi & Trim(arrParentesi(a)) & ","
        End If
      Next a
      If traParentesi = "" Then
        arrDirett(i) = ""
      Else
        arrDirett(i) = Left(arrDirett(i), InStr(arrDirett(i), "(")) & Left(traParentesi, Len(traParentesi) - 1) & ")"
      End If
    Else
      If Trim(arrDirett(i)) = "FDUMP" Or _
         Trim(arrDirett(i)) = "OFFSET" Or _
         Trim(arrDirett(i)) = "SYM" Then
        arrDirett(i) = ""
      End If
    End If
  Next i
  ' RIATTACCO I PEZZI DELL'ARRAY CHE AVEVO SPLITTATO PRIMA
  For i = 0 To UBound(arrDirett)
    If arrDirett(i) <> "" Then
      newrow = newrow & Trim(arrDirett(i)) & ","
    End If
  Next i
  If newrow = "" Then
    ' COMMENTO TUTTA LA RIGA
    newrow = strcomment
  Else
    ' Aggiungo l'intestazione delle direttive e tolgo l'ultima virgola
    newrow = Header & " " & Left(newrow, Len(newrow) - 1)
    If isDotVir Then
      newrow = newrow & ";"
    End If
  End If
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  newrow = RTrim(newrow)
  Elimina_DLI_in_Xopts_OF = newrow
End Function

Function getPCDRedFromPCB_OF(pPCB As String, pIdOggetto As Long, ProcName As String, Optional lenArea As Long = 0) As String
  Dim rsData As Recordset
  
  Set rsData = m_fun.Open_Recordset("select Nome,Byte,proc from PsData_Cmp WHERE IdOggetto=" & pIdOggetto & " AND (Nomered='" & pPCB & "' or Nomered='(" & pPCB & ")')")
  If Not rsData.EOF Then
    lenArea = rsData!Byte
    getPCDRedFromPCB_OF = rsData!nome
    ProcName = rsData!proc
  End If
  rsData.Close
End Function

Sub AddFIrecordTypeProc(MainName As String)
  Dim i As Integer, j As Integer
  Dim Testo As String
  Dim IRIS_() As String
  Dim IRIS__() As String
    
  For i = 1 To UBound(ArrProc)
    Testo = ""
    'IRIS-DB forzato alla situazione della PROC trovata perch� L&G ha PROC con nomi diversi dal file, ma sono sempre validi
'''    If ArrProc(i) = MainName Or ArrProc(i) = "" Then
      ' aggiungiamo a FItable di InitCopyIndex
      IRIS_ = Split(ArrProcLines(i), vbCrLf)
      For j = 0 To UBound(IRIS_)
        If Len(IRIS_(j)) Then
          'IRIS-DB per ora veramente molto fisso (solo per SSA)
          If Len(IRIS_(j)) > 71 Then
            IRIS__ = Split(IRIS_(j), "BASED")
            Testo = Testo & vbCrLf & IRIS__(0) & vbCrLf & "     BASED" & IRIS__(1)
          Else
            Testo = Testo & vbCrLf & IRIS_(j)
          End If
        End If
      Next j
      FItable(InitCopyIndex).lines_up = Testo
'''    Else
'''      ReDim Preserve FItable(UBound(FItable) + 1)
'''      FItable(UBound(FItable)).isCaseSensitive = False 'da verificare
'''      FItable(UBound(FItable)).CommentType = "PLI"
'''      'FItable(UBound(FItable)).infoModify = "PLI"
'''      FItable(UBound(FItable)).infoModify = ""
'''      FItable(UBound(FItable)).target = ""
'''      FItable(UBound(FItable)).isSubSection = True
'''      FItable(UBound(FItable)).startSection = ArrProc(i) & " : PROC"
'''      FItable(UBound(FItable)).sectionTerminator = ";"
'''      FItable(UBound(FItable)).endSection = "END " & ArrProc(i)
'''      'testo = ArrProcLines(i)
'''      'While (Left(testo, 2) = vbCrLf)
'''      '  testo = Right(testo, Len(testo) - 2)
'''      'Wend
'''      IRIS_ = Split(ArrProcLines(i), vbCrLf)
'''      For j = 0 To UBound(IRIS_)
'''        If Len(IRIS_(j)) Then
'''          Testo = Testo & vbCrLf & IRIS_(j)
'''        End If
'''      Next j
'''      FItable(UBound(FItable)).lines_up = Testo
'''      FItable(UBound(FItable)).isUnique = True ' la sostituzione vale solo per la prima occorrenza
'''    End If
  Next i
End Sub

Function GetProcName(pIdOggetto As Long, pNome As String) As String()
  Dim rsData As Recordset, rsCopy As Recordset
  Dim appoProc() As String
  ReDim appoProc(0)
  Set rsData = m_fun.Open_Recordset("select proc from PsData_Cmp WHERE " & _
                                    "IdOggetto=" & pIdOggetto & " AND Nome = '" & pNome & "'")
  While Not rsData.EOF
    ReDim Preserve appoProc(UBound(appoProc) + 1)
    appoProc(UBound(appoProc)) = IIf(IsNull(rsData!proc), "", rsData!proc)
    rsData.MoveNext
  Wend
  rsData.Close
  ' Giro sulle INCLUDE
  Set rsData = m_fun.Open_Recordset("SELECT a.IdOggetto From PsData_Cmp as a,PsRel_Obj as b" & _
                                  " where b.idOggettoC = " & pIdOggetto & " and b.relazione='INC' And" & _
                                  " a.IdOggetto=b.IdOggettoR and a.Nome = '" & pNome & "'")
  While Not rsData.EOF
      Set rsCopy = m_fun.Open_Recordset("select ReplacingVar from PsPgm_Copy as a,Bs_Oggetti as b WHERE " & _
                                    "a.IdPgm=" & pIdOggetto & " AND a.copy = b.Nome and b.IdOggetto = " & rsData!IdOggetto)
      If Not rsCopy.EOF Then
        ReDim Preserve appoProc(UBound(appoProc) + 1)
        appoProc(UBound(appoProc)) = IIf(IsNull(rsCopy!ReplacingVar), "", rsCopy!ReplacingVar)
      End If
      rsData.MoveNext
  Wend
  rsData.Close
  GetProcName = appoProc
End Function

Sub AdjustToken(plinetoken As String, pline As String)
  If Len(plinetoken) > 1 Then
    '1) i due punti devono essere gestiti come un token distinto
    If Right(plinetoken, 1) = ":" Then
      plinetoken = Left(plinetoken, Len(plinetoken) - 1)
      pline = ": " & pline
    End If
    '2) la virgola come primo carattere la elimino
    If Left(plinetoken, 1) = "," Then
      plinetoken = Right(plinetoken, Len(plinetoken) - 1)
    End If
    '3) punto e virgola attaccato ad ultima parola
    If Right(plinetoken, 1) = ";" Then
      plinetoken = Left(plinetoken, Len(plinetoken) - 1)
      pline = "; " & pline
    End If
    '4) virgola separatore di due token
    If InStr(plinetoken, ",") > 0 Then
      pline = Right(plinetoken, Len(plinetoken) - InStr(plinetoken, ",")) & " " & pline
      plinetoken = Left(plinetoken, InStr(plinetoken, ",") - 1)
    End If
    '5) punto e virgola con attaccato qualcos'altro (commento o numero linea ad esempio)
    If Left(plinetoken, 1) = ";" Then
      pline = Right(plinetoken, Len(plinetoken) - 1)
      plinetoken = ";"
    End If
     '6) punto attaccato ad ultima parola trovata
    If Right(plinetoken, 1) = "." Then
       plinetoken = Left(plinetoken, Len(plinetoken) - 1)
      pline = ". " & pline
    End If
    '7) due punti con attaccato qualcos'altro (commento o numero linea ad esempio)
    If Left(plinetoken, 1) = ":" Then
      pline = Right(plinetoken, Len(plinetoken) - 1) & " " & pline
      plinetoken = ":"
    End If
    '8) due punti separatore di due token
    If InStr(plinetoken, ":") > 0 And Not plinetoken = ":" Then
      pline = ":" & Right(plinetoken, Len(plinetoken) - InStr(plinetoken, ":")) & " " & pline
      plinetoken = Left(plinetoken, InStr(plinetoken, ":") - 1)
    End If
  End If
End Sub

Sub CheckProc(Line As String, proc As String)
  Dim i As Integer
  
  For i = 1 To UBound(ArrProc)
    If ArrProc(i) = proc Then
      ArrProcLines(i) = ArrProcLines(i) & vbCrLf & Line
      Exit For
    End If
  Next i
  If i = UBound(ArrProc) + 1 Then
    ReDim Preserve ArrProc(UBound(ArrProc) + 1)
    ReDim Preserve ArrProcLines(UBound(ArrProcLines) + 1)
    ArrProc(UBound(ArrProc)) = proc
    ArrProcLines(UBound(ArrProcLines)) = Line
  End If
End Sub

' Mauro 04/11/2009
Public Function IsCMPAT(psb As Long, IdObj As Long) As Boolean
  Dim rs As Recordset, rsObj As Recordset
  
  Set rs = m_fun.Open_Recordset("Select Min(NumPcb) From PsDli_Psb Where idOggetto = " & psb)
  If rs.Fields(0) > 1 Then
    Set rsObj = m_fun.Open_Recordset("select cics from bs_oggetti where idoggetto = " & IdObj & " and cics = true")
    IsCMPAT = rsObj.RecordCount = 0
    rsObj.Close
  Else
    IsCMPAT = True
  End If
  rs.Close
End Function
