VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form MadrdF_Check_Routine 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Check/Manage Routines"
   ClientHeight    =   5415
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   10605
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5415
   ScaleWidth      =   10605
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComDlg.CommonDialog CmDlg 
      Left            =   5520
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin RichTextLib.RichTextBox rtbText 
      Height          =   315
      Left            =   900
      TabIndex        =   34
      Top             =   6180
      Visible         =   0   'False
      Width           =   315
      _ExtentX        =   556
      _ExtentY        =   556
      _Version        =   393217
      TextRTF         =   $"MadrdF_Check_Routine.frx":0000
   End
   Begin VB.Frame Frame3 
      Caption         =   "Language"
      Height          =   705
      Left            =   60
      TabIndex        =   21
      Top             =   60
      Width           =   10455
      Begin VB.OptionButton optPLI 
         Caption         =   "PLI"
         Height          =   192
         Left            =   1980
         TabIndex        =   23
         Top             =   300
         Width           =   735
      End
      Begin VB.OptionButton optCobol 
         Caption         =   "Cobol"
         Height          =   192
         Left            =   840
         TabIndex        =   22
         Top             =   300
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4515
      Left            =   45
      TabIndex        =   0
      Top             =   840
      Width           =   10485
      _ExtentX        =   18494
      _ExtentY        =   7964
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   882
      TabCaption(0)   =   "Check Encapsulation  Routine Coherency"
      TabPicture(0)   =   "MadrdF_Check_Routine.frx":0082
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblPath_RT"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblPath_IN"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame5"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdShow_NONIN"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdShow_IN"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmdShow_RT"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Frame1"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cmdCheck_Unused"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "cmdCheck_MISS"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "cmdStart"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "txtPath_RT"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtPath_IN"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "cmdREV_"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Frame7"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Frame16"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Frame17"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Frame21"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).ControlCount=   17
      TabCaption(1)   =   "Management Routines/Instructions"
      TabPicture(1)   =   "MadrdF_Check_Routine.frx":009E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdSplitRout"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame20"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame13"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Frame11"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Frame10(0)"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "Frame9"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "cmdTemplate"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "txtChar"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "cmdCheckFormat"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "cmbCombine"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "cmdCheckRout"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "txtInstrxRout"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).Control(12)=   "chkDelInstr"
      Tab(1).Control(12).Enabled=   0   'False
      Tab(1).Control(13)=   "chkInsert"
      Tab(1).Control(13).Enabled=   0   'False
      Tab(1).Control(14)=   "cmdImportInstr"
      Tab(1).Control(14).Enabled=   0   'False
      Tab(1).Control(15)=   "cmdSplitIstr"
      Tab(1).Control(15).Enabled=   0   'False
      Tab(1).Control(16)=   "txtPath_ROUT"
      Tab(1).Control(16).Enabled=   0   'False
      Tab(1).Control(17)=   "txtPath_INSTR"
      Tab(1).Control(17).Enabled=   0   'False
      Tab(1).Control(18)=   "Label9"
      Tab(1).Control(18).Enabled=   0   'False
      Tab(1).Control(19)=   "Label3"
      Tab(1).Control(19).Enabled=   0   'False
      Tab(1).Control(20)=   "Label2"
      Tab(1).Control(20).Enabled=   0   'False
      Tab(1).Control(21)=   "Label1"
      Tab(1).Control(21).Enabled=   0   'False
      Tab(1).ControlCount=   22
      TabCaption(2)   =   "Check Call Incapsulated"
      TabPicture(2)   =   "MadrdF_Check_Routine.frx":00BA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmdShowCallIncaps"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "txtTag"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "Frame4"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "cmdCallIncaps"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "txtPath_CallIncaps"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "Label5"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "Label4"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).ControlCount=   7
      TabCaption(3)   =   " Refresh Facilities"
      TabPicture(3)   =   "MadrdF_Check_Routine.frx":00D6
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Command1"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "CmdCopyTables"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "CmdUpdatePcb"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).Control(3)=   "CmdCallParamCheck"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).Control(4)=   "CmdPcbAllign"
      Tab(3).Control(4).Enabled=   0   'False
      Tab(3).Control(5)=   "CmdRefrComp"
      Tab(3).Control(5).Enabled=   0   'False
      Tab(3).Control(6)=   "Frame18"
      Tab(3).Control(6).Enabled=   0   'False
      Tab(3).Control(7)=   "Frame8"
      Tab(3).Control(7).Enabled=   0   'False
      Tab(3).Control(8)=   "Frame6"
      Tab(3).Control(8).Enabled=   0   'False
      Tab(3).Control(9)=   "cmdCompare"
      Tab(3).Control(9).Enabled=   0   'False
      Tab(3).Control(10)=   "ecxst"
      Tab(3).Control(10).Enabled=   0   'False
      Tab(3).Control(11)=   "CmdGenReport"
      Tab(3).Control(11).Enabled=   0   'False
      Tab(3).Control(12)=   "TxtAfterPath"
      Tab(3).Control(12).Enabled=   0   'False
      Tab(3).Control(13)=   "TxtBeforePath"
      Tab(3).Control(13).Enabled=   0   'False
      Tab(3).Control(14)=   "LblPCbAlign"
      Tab(3).Control(14).Enabled=   0   'False
      Tab(3).Control(15)=   "Label11"
      Tab(3).Control(15).Enabled=   0   'False
      Tab(3).Control(16)=   "Label10"
      Tab(3).Control(16).Enabled=   0   'False
      Tab(3).ControlCount=   17
      Begin VB.CommandButton Command1 
         Caption         =   "Load Pcb/Psb tables for refresh"
         Height          =   495
         Left            =   -71040
         TabIndex        =   73
         Top             =   3750
         Width           =   1455
      End
      Begin VB.CommandButton CmdCopyTables 
         Caption         =   "Pcb/Psb Tables Backup"
         Height          =   495
         Left            =   -71040
         TabIndex        =   72
         Top             =   3195
         Width           =   1455
      End
      Begin VB.CommandButton CmdUpdatePcb 
         Caption         =   "Update Instructions"
         Height          =   495
         Left            =   -69480
         TabIndex        =   71
         Top             =   3750
         Width           =   1455
      End
      Begin VB.CommandButton CmdCallParamCheck 
         Caption         =   "Check Calls params"
         Height          =   495
         Left            =   -66840
         TabIndex        =   70
         Top             =   3120
         Width           =   1695
      End
      Begin VB.CommandButton CmdPcbAllign 
         Caption         =   "Allign Pcb number"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   -69480
         TabIndex        =   69
         Top             =   3195
         Width           =   1455
      End
      Begin VB.CommandButton CmdRefrComp 
         Caption         =   "Start Files Analysis"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   -74865
         TabIndex        =   68
         ToolTipText     =   "Feeds Repository Tables with called/generated Routines"
         Top             =   3195
         Width           =   1455
      End
      Begin VB.Frame Frame21 
         Caption         =   "Frame16"
         Height          =   615
         Left            =   3060
         TabIndex        =   67
         Top             =   3645
         Width           =   30
      End
      Begin VB.CommandButton cmdSplitRout 
         Caption         =   "Split in smaller Routines"
         Height          =   495
         Left            =   -66360
         TabIndex        =   66
         ToolTipText     =   "Split Routines in Instructions"
         Top             =   1665
         Width           =   1455
      End
      Begin VB.Frame Frame20 
         Height          =   105
         Left            =   -66765
         TabIndex        =   65
         Top             =   2520
         Width           =   2115
      End
      Begin VB.Frame Frame18 
         Caption         =   "Frame18"
         Height          =   1185
         Left            =   -73230
         TabIndex        =   64
         Top             =   3090
         Width           =   30
      End
      Begin VB.Frame Frame17 
         Height          =   105
         Left            =   8415
         TabIndex        =   63
         Top             =   2610
         Width           =   1815
      End
      Begin VB.Frame Frame16 
         Caption         =   "Frame16"
         Height          =   615
         Left            =   8130
         TabIndex        =   62
         Top             =   3645
         Width           =   30
      End
      Begin VB.Frame Frame13 
         Caption         =   "Frame13"
         Height          =   1185
         Left            =   -71310
         TabIndex        =   61
         Top             =   3105
         Width           =   30
      End
      Begin VB.Frame Frame11 
         Caption         =   "Frame11"
         Height          =   1155
         Left            =   -73170
         TabIndex        =   60
         Top             =   3105
         Width           =   30
      End
      Begin VB.Frame Frame10 
         Height          =   3405
         Index           =   0
         Left            =   -66960
         TabIndex        =   59
         Top             =   1020
         Width           =   30
      End
      Begin VB.Frame Frame9 
         Height          =   105
         Left            =   -74850
         TabIndex        =   58
         Top             =   2505
         Width           =   7695
      End
      Begin VB.Frame Frame8 
         Height          =   1545
         Left            =   -66960
         TabIndex        =   57
         Top             =   1080
         Width           =   30
      End
      Begin VB.Frame Frame7 
         Caption         =   "Frame7"
         Height          =   1725
         Left            =   8100
         TabIndex        =   56
         Top             =   1020
         Width           =   30
      End
      Begin VB.Frame Frame6 
         Height          =   105
         Left            =   -74880
         TabIndex        =   55
         Top             =   2655
         Width           =   7845
      End
      Begin VB.CommandButton cmdCompare 
         Caption         =   "Compare"
         Height          =   495
         Left            =   -73050
         TabIndex        =   54
         ToolTipText     =   "Split Results Comparison"
         Top             =   3750
         Width           =   1455
      End
      Begin VB.CommandButton ecxst 
         Caption         =   "Split"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   -73065
         TabIndex        =   52
         ToolTipText     =   "Separate original and additional statemens"
         Top             =   3195
         Width           =   1455
      End
      Begin VB.CommandButton CmdGenReport 
         Caption         =   "Report"
         Height          =   495
         Left            =   -74880
         TabIndex        =   53
         Top             =   3750
         Width           =   1455
      End
      Begin VB.TextBox TxtAfterPath 
         Height          =   375
         Left            =   -74910
         TabIndex        =   49
         Top             =   1965
         Width           =   7815
      End
      Begin VB.TextBox TxtBeforePath 
         Height          =   375
         Left            =   -74880
         TabIndex        =   48
         Top             =   1065
         Width           =   7815
      End
      Begin VB.CommandButton cmdTemplate 
         Caption         =   "Template"
         Height          =   525
         Left            =   -72990
         TabIndex        =   47
         Top             =   3135
         Width           =   1455
      End
      Begin VB.TextBox txtChar 
         Height          =   375
         Left            =   -65460
         TabIndex        =   45
         Top             =   2970
         Width           =   795
      End
      Begin VB.CommandButton cmdCheckFormat 
         Caption         =   "Check Format"
         Height          =   525
         Left            =   -66360
         TabIndex        =   44
         Top             =   3690
         Width           =   1575
      End
      Begin VB.CommandButton cmdREV_ 
         Caption         =   "TMP REV_"
         Height          =   495
         Left            =   8580
         TabIndex        =   36
         Top             =   1230
         Width           =   1455
      End
      Begin VB.CommandButton cmbCombine 
         Caption         =   "Re-Combine Original Routines"
         Height          =   495
         Left            =   -72975
         TabIndex        =   35
         ToolTipText     =   "Split Routines in Instructions"
         Top             =   3765
         Width           =   1455
      End
      Begin VB.CommandButton cmdShowCallIncaps 
         Caption         =   "Show Incapsulated Calls"
         Height          =   495
         Left            =   -68520
         TabIndex        =   33
         ToolTipText     =   "Feeds Repository Tables with called/generated Routines"
         Top             =   3300
         Width           =   1455
      End
      Begin VB.TextBox txtTag 
         Height          =   375
         Left            =   -68040
         MaxLength       =   6
         TabIndex        =   31
         Top             =   2520
         Width           =   975
      End
      Begin VB.Frame Frame4 
         Height          =   105
         Left            =   -74880
         TabIndex        =   29
         Top             =   3060
         Width           =   7815
      End
      Begin VB.CommandButton cmdCallIncaps 
         Caption         =   "Start Files Analysis"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   -74880
         TabIndex        =   28
         ToolTipText     =   "Feeds Repository Tables with called/generated Routines"
         Top             =   3300
         Width           =   1455
      End
      Begin VB.TextBox txtPath_CallIncaps 
         Height          =   375
         Left            =   -74880
         TabIndex        =   27
         Top             =   1080
         Width           =   7815
      End
      Begin VB.CommandButton cmdCheckRout 
         Caption         =   "Check Routines in Instructions"
         Height          =   495
         Left            =   -74820
         TabIndex        =   26
         ToolTipText     =   "Split Routines in Instructions"
         Top             =   3735
         Width           =   1455
      End
      Begin VB.TextBox txtInstrxRout 
         Height          =   375
         Left            =   -65430
         TabIndex        =   24
         Top             =   930
         Width           =   765
      End
      Begin VB.CheckBox chkDelInstr 
         Caption         =   "Delete instructions"
         Height          =   315
         Left            =   -71085
         TabIndex        =   20
         Top             =   3375
         Width           =   2055
      End
      Begin VB.CheckBox chkInsert 
         Caption         =   "Insert new instructions"
         Height          =   315
         Left            =   -71085
         TabIndex        =   19
         Top             =   3075
         Width           =   2055
      End
      Begin VB.CommandButton cmdImportInstr 
         Caption         =   "Import Instructions in Routine"
         Default         =   -1  'True
         Height          =   495
         Left            =   -71085
         TabIndex        =   18
         ToolTipText     =   "Import Instructions in Routine"
         Top             =   3735
         Width           =   1455
      End
      Begin VB.CommandButton cmdSplitIstr 
         Caption         =   "Split Routines"
         Height          =   495
         Left            =   -74820
         TabIndex        =   17
         ToolTipText     =   "Split Routines in Instructions"
         Top             =   3165
         Width           =   1455
      End
      Begin VB.TextBox txtPath_ROUT 
         Height          =   375
         Left            =   -74880
         TabIndex        =   14
         Top             =   1080
         Width           =   7815
      End
      Begin VB.TextBox txtPath_INSTR 
         Height          =   375
         Left            =   -74880
         TabIndex        =   13
         Top             =   1980
         Width           =   7815
      End
      Begin VB.Frame Frame2 
         Height          =   125
         Index           =   0
         Left            =   -74880
         TabIndex        =   12
         Top             =   3060
         Width           =   7815
      End
      Begin VB.TextBox txtPath_IN 
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   7815
      End
      Begin VB.TextBox txtPath_RT 
         Height          =   375
         Left            =   120
         TabIndex        =   8
         Top             =   1980
         Width           =   7815
      End
      Begin VB.CommandButton cmdStart 
         Caption         =   "Start Files Analysis"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   135
         TabIndex        =   7
         ToolTipText     =   "Feeds Repository Tables with called/generated Routines"
         Top             =   3735
         Width           =   1455
      End
      Begin VB.CommandButton cmdCheck_MISS 
         Caption         =   "Show Missing Routines"
         Height          =   495
         Left            =   4050
         TabIndex        =   6
         ToolTipText     =   "Feeds Repository Tables with called/generated Routines"
         Top             =   3150
         Width           =   1455
      End
      Begin VB.CommandButton cmdCheck_Unused 
         Caption         =   "Show Unused Routines"
         Height          =   495
         Left            =   5670
         TabIndex        =   5
         ToolTipText     =   "Feeds Repository Tables with called/generated Routines"
         Top             =   3135
         Width           =   1455
      End
      Begin VB.Frame Frame1 
         Height          =   105
         Left            =   120
         TabIndex        =   4
         Top             =   2595
         Width           =   7815
      End
      Begin VB.CommandButton cmdShow_RT 
         Caption         =   "Show Routine Results"
         Height          =   495
         Left            =   3180
         TabIndex        =   3
         ToolTipText     =   "Feeds Repository Tables with called/generated Routines"
         Top             =   3735
         Width           =   1455
      End
      Begin VB.CommandButton cmdShow_IN 
         Caption         =   "Show Encaps Results"
         Height          =   495
         Left            =   4860
         TabIndex        =   2
         ToolTipText     =   "Feeds Repository Tables with called/generated Routines"
         Top             =   3735
         Width           =   1455
      End
      Begin VB.CommandButton cmdShow_NONIN 
         Caption         =   "Non-Encaps Results"
         Height          =   495
         Left            =   6540
         TabIndex        =   1
         Top             =   3735
         Width           =   1455
      End
      Begin VB.Frame Frame5 
         Height          =   1335
         Left            =   8460
         TabIndex        =   37
         Top             =   1050
         Width           =   1695
         Begin VB.CheckBox chkchL 
            Caption         =   "Check3"
            Height          =   255
            Left            =   1320
            TabIndex        =   40
            Top             =   720
            Width           =   255
         End
         Begin VB.CheckBox chkdelX 
            Caption         =   "Check2"
            Height          =   255
            Left            =   720
            TabIndex        =   39
            Top             =   720
            Width           =   255
         End
         Begin VB.CheckBox chkMP 
            Caption         =   "Check1"
            Height          =   255
            Left            =   120
            TabIndex        =   38
            Top             =   720
            Width           =   255
         End
         Begin VB.Label Label8 
            Alignment       =   2  'Center
            Caption         =   "chL"
            Height          =   255
            Left            =   1320
            TabIndex        =   43
            Top             =   960
            Width           =   255
         End
         Begin VB.Label Label7 
            Alignment       =   2  'Center
            Caption         =   "delX"
            Height          =   255
            Left            =   600
            TabIndex        =   42
            Top             =   960
            Width           =   375
         End
         Begin VB.Label Label6 
            Caption         =   "MP"
            Height          =   255
            Left            =   120
            TabIndex        =   41
            Top             =   960
            Width           =   255
         End
      End
      Begin VB.Label LblPCbAlign 
         Alignment       =   2  'Center
         Caption         =   "Pcb/PSB Alignment"
         Height          =   255
         Left            =   -71040
         TabIndex        =   74
         Top             =   2880
         Width           =   3015
      End
      Begin VB.Label Label11 
         Caption         =   "Incapsulated After Refresh Directory"
         Height          =   255
         Left            =   -74880
         TabIndex        =   51
         Top             =   1725
         Width           =   2775
      End
      Begin VB.Label Label10 
         Caption         =   "Incapsulated Before Refresh Directory"
         Height          =   255
         Left            =   -74880
         TabIndex        =   50
         Top             =   825
         Width           =   3255
      End
      Begin VB.Label Label9 
         Caption         =   "Limit of Characters for line"
         Height          =   585
         Left            =   -66780
         TabIndex        =   46
         Top             =   2970
         Width           =   855
      End
      Begin VB.Label Label5 
         Caption         =   "Tag "
         Height          =   315
         Left            =   -68460
         TabIndex        =   32
         Top             =   2580
         Width           =   435
      End
      Begin VB.Label Label4 
         Caption         =   "Incapsulated Files Directory"
         Height          =   255
         Left            =   -74880
         TabIndex        =   30
         Top             =   840
         Width           =   2175
      End
      Begin VB.Label Label3 
         Caption         =   "Number of Instructions for Routine"
         Height          =   555
         Left            =   -66840
         TabIndex        =   25
         Top             =   780
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Routine Files Directory"
         Height          =   255
         Left            =   -74880
         TabIndex        =   16
         Top             =   840
         Width           =   2175
      End
      Begin VB.Label Label1 
         Caption         =   "Instruction Files Directory"
         Height          =   255
         Left            =   -74880
         TabIndex        =   15
         Top             =   1740
         Width           =   2175
      End
      Begin VB.Label lblPath_IN 
         Caption         =   "Incapsulated Files Directory"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   840
         Width           =   2175
      End
      Begin VB.Label lblPath_RT 
         Caption         =   "Routine Files Directory"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1740
         Width           =   2175
      End
   End
End
Attribute VB_Name = "MadrdF_Check_Routine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Mauro 29/08/2007 : Stringa che contiene la lista delle istruzioni importate
Public arrInstrImp As String
Dim instrExist() As String
Dim language As String ' pu� essere CBL o PLI
Dim NumIstrxRout As Integer
Dim Progressivo As Long
Dim MissingRout() As String
Dim isRoutCics As Boolean
Dim varUsing As String

' Mauro 18/01/2008
Dim wCaller As String, wImsrtName As String
Dim wOperation As String, lineCallAster As String
Dim lineCallIncaps As String, wArea As String
Dim wPCB As String, wSSA_Name As String
Dim wLevel As String, wDlzPcb As String
Dim wKeyValue As String
Dim wDlzPCB_TO As String, wDataArea_TO As String

Private Sub cmbCombine_Click()
  If optCobol Then
    language = "CBL"
  ElseIf OptPLI Then
    language = "PLI"
  End If

  If Len(Trim(txtPath_ROUT)) Then
    Screen.MousePointer = vbHourglass
    Combine_Routine txtPath_ROUT
    Screen.MousePointer = vbNormal
    MsgBox "Routines Complete.", vbOKOnly + vbInformation
  Else
    MsgBox "Insert correct value in Routine Files Directory.", vbInformation, "i-4.Migration"
  End If
End Sub

Private Sub cmdCallIncaps_Click()
  If optCobol Then
    language = "CBL"
  ElseIf OptPLI Then
    language = "PLI"
  End If
  
  If Len(Trim(txtPath_CallIncaps)) Then
    Screen.MousePointer = vbHourglass
    On Error GoTo noTable
    If Len(txtPath_CallIncaps) Then
      m_fun.FnConnection.Execute "DELETE * FROM TMP_CallIncaps"
    End If
    check_Call_Incaps txtPath_CallIncaps
    Screen.MousePointer = vbNormal
    MsgBox "Operation Complete.", vbOKOnly + vbInformation
  Else
    MsgBox "Insert correct value for check directory", vbInformation, "i-4.Migration"
  End If
  Exit Sub
noTable:
  Screen.MousePointer = vbNormal
  MsgBox "You need a Check-Repository", vbExclamation, "i-4.Migration"
End Sub

Private Sub CmdCallParamCheck_Click()
  Dim rs As Recordset
  Dim nomeChiamante As String
  Dim numCallParameters As Integer, numUsingParameters As Integer
  Dim sp() As String
  
  If MsgBox("Delete old datas?", vbYesNo, Dli2Rdbms.drNomeProdotto) = vbYes Then
    m_fun.FnConnection.Execute "Delete From TMP_CheckParamNumber"
  End If
  Set rs = m_fun.Open_Recordset("SELECT a.Programma, a.IdOggetto, a.Parametri, b.Parameters from PsCall as a, PsPgm as b, Bs_Oggetti as c " & _
                                " where b.IdOggetto = c.IdOggetto and ""'"" & Trim(c.Nome) & ""'"" = Trim(a.Programma) order by a.Programma")
  While Not rs.EOF
    nomeChiamante = NamePgmfromIdPgm(rs!IdOggetto)
    sp = Split(rs!parameters, ",")
    numUsingParameters = UBound(sp) + 1
    ' non so se sempre uno spazio o pi� tra un parametro e l'altro (uso custom funz)
    numCallParameters = CountParametersBySpace(rs!parametri)
    m_fun.FnConnection.Execute "Insert Into TMP_CheckParamNumber (Programma,Chiamante,ParametriCall,ParametriPgm,NumCallparam,NumPgmParam) " & _
                               "VALUES ('" & Replace(rs!Programma, "'", "") & "','" & nomeChiamante & "','" & rs!parametri & "','" & rs!parameters & "'," & numCallParameters & "," & numUsingParameters & ")"
    rs.MoveNext
  Wend
  rs.Close
  MsgBox "Analysis completed", vbInformation
End Sub

Function CountParametersBySpace(ByRef parametri As String) As Integer
  Dim posSpace As Integer, incrSpace As Integer
  Dim IsLastParameter As Boolean
  
  parametri = Trim(parametri)
  If Right(parametri, 1) = "." Then
    parametri = Left(parametri, Len(parametri) - 1)
  End If
  parametri = Trim(parametri)
  While Not parametri = "" And Not IsLastParameter
    CountParametersBySpace = CountParametersBySpace + 1
    posSpace = InStr(parametri, " ")
    If posSpace Then
      incrSpace = 1
      If Len(parametri) >= posSpace + incrSpace Then
        Do While Mid(parametri, posSpace + incrSpace, 1) = " "
          incrSpace = incrSpace + 1
          If Len(parametri) < posSpace + incrSpace Then
            Exit Do
          End If
        Loop
      End If
      If posSpace + incrSpace - 1 < Len(parametri) Then
        parametri = Right(parametri, Len(parametri) - (posSpace + incrSpace - 1))
        If Left(parametri, 3) = "OF " Then
          parametri = Trim(Right(parametri, Len(parametri) - 3))
          CountParametersBySpace = CountParametersBySpace - 1
        End If
      End If
    Else
      IsLastParameter = True
      If Left(parametri, 1) = "(" And Right(parametri, 1) = ")" Then
        CountParametersBySpace = CountParametersBySpace - 1
      End If
    End If
  Wend
End Function

Private Sub cmdCheck_MISS_Click()
  Dim rs As Recordset, rs2 As Recordset
  Dim text As String, textDiff As String, textmore As String
  Dim counter As Integer, counter2 As Integer, counter3 As Integer
  Dim currPgm1 As String, currPgm2 As String, currPgm3 As String
  On Error GoTo catch
  
  Screen.MousePointer = vbHourglass
  Set rs = m_fun.Open_Recordset("SELECT DISTINCT routine,NomeRout,pgm FROM TMP_checkIncaps order by pgm")
  While Not rs.EOF
    Set rs2 = m_fun.Open_Recordset("SELECT pgm FROM TMP_checkRoutine WHERE routine = '" & rs!Routine & "'")
    If rs2.EOF Then
      counter = counter + 1
      If Right(rs!PGM, Len(rs!PGM) - InStrRev(rs!PGM, "\")) <> currPgm1 Then
        currPgm1 = Right(rs!PGM, Len(rs!PGM) - InStrRev(rs!PGM, "\"))
        text = text & vbCrLf & "---- " & currPgm1 & " ----" & vbCrLf
      End If
      text = text & rs!Routine & " [" & rs!nomeRout & "]" & vbCrLf
    ElseIf rs2.RecordCount = 1 Then
      If (Right(rs2!PGM, 8) <> rs!nomeRout & "I") Then
        counter2 = counter2 + 1
        If Right(rs!PGM, Len(rs!PGM) - InStrRev(rs!PGM, "\")) <> currPgm2 Then
          currPgm2 = Right(rs!PGM, Len(rs!PGM) - InStrRev(rs!PGM, "\"))
          textDiff = textDiff & vbCrLf & "---- " & currPgm2 & " ----" & vbCrLf
        End If
        textDiff = textDiff & rs!Routine & " [" & rs!nomeRout & "] ----> [" & Left(Right(rs2!PGM, 8), 7) & "]" & vbCrLf
      End If
    Else   '
'      counter3 = counter3 + 1
'      If Right(rs!PGM, Len(rs!PGM) - InStrRev(rs!PGM, "\")) <> currPgm3 Then
'        currPgm3 = Right(rs!PGM, Len(rs!PGM) - InStrRev(rs!PGM, "\"))
'        textmore = textmore & vbCrLf & "---- " & currPgm3 & " ----" & vbCrLf
'      End If
'      textmore = textmore & rs!Routine & " [" & rs!nomeRout & "]" & "(" & rs2.RecordCount & ")" & vbCrLf
'      While Not rs2.EOF
'        textmore = textmore & "[" & Left(Right(rs2!PGM, 8), 7) & "];"
'        rs2.MoveNext
'      Wend
'      textmore = textmore & vbCrLf & vbCrLf
    End If
    rs2.Close
    rs.MoveNext
  Wend
  rs.Close
  
  If counter > 0 Then
    text = "------ Missing Routines --------" & vbCrLf & text
  End If
  
  If counter2 > 0 Then
    textDiff = "------ Misplaced Routines --------" & vbCrLf & textDiff
  End If
'  If counter3 > 0 Then
'    textmore = "------ Routines with more versions --------" & vbCrLf & textmore
'  End If
  
  Screen.MousePointer = vbDefault
  
  MsgBox "Missing Routines: " & counter & vbCrLf & _
         "Misplaced Routines: " & counter2, vbInformation, "i-4.Migration"
  '& vbCrLf & "Routines with more versions: " & counter3
  
  ' Mauro 07/12/2007 : Perch� visualizzare la lista vuota?!?!
  If counter Or counter2 Then
    m_fun.Show_ShowText text & vbCrLf & _
                        textDiff & vbCrLf & _
                        textmore, "Missing Routines"
  End If
  'm_fun.Show_DisplayTab "TMP_checkRoutine", "Generated Routines"
  
  Exit Sub
catch:
  Screen.MousePointer = vbDefault
  MsgBox err.Description
End Sub

Private Sub cmdCheck_Unused_Click()
  Dim rs As Recordset, rs2 As Recordset
  Dim text As String
  Dim counter As Integer, IsFounded As Boolean
  
  On Error GoTo catch
  
  Screen.MousePointer = vbHourglass
  
  Set rs = m_fun.Open_Recordset("SELECT DISTINCT routine,pgm FROM TMP_checkRoutine")
  While Not rs.EOF
    Set rs2 = m_fun.Open_Recordset("SELECT  routine,NomeRout FROM TMP_checkIncaps " & _
      " WHERE routine='" & rs!Routine & "'")
    If rs2.RecordCount = 0 Then
      counter = counter + 1
      text = text & rs!Routine & " [" & Right(rs!PGM, 8) & "]" & vbCrLf
    Else
      IsFounded = False
      Do While Not rs2.EOF
        If (Right(rs!PGM, 8) = rs2!nomeRout & "I") Then
          IsFounded = True
          Exit Do
        End If
        rs2.MoveNext
      Loop
      If Not IsFounded Then
        text = text & rs!Routine & " [" & Right(rs!PGM, 8) & "]" & vbCrLf
      End If
      '----
    End If
    rs2.Close
    rs.MoveNext
  Wend
  rs.Close
  
  Screen.MousePointer = vbDefault
  
  MsgBox "Unused Routines: " & counter, vbInformation, "i-4.Migration"
  
  ' Mauro 07/12/2007 : Perch� visualizzare la lista vuota?!?!
  If counter Then
    m_fun.Show_ShowText text, "Unused Routines"
  End If
  
  Exit Sub
catch:
  Screen.MousePointer = vbDefault
  MsgBox err.Description
End Sub

Private Sub cmdCheckRout_Click()
  ' Funziona sia per CBL che per PLI
  If optCobol Then
    language = "CBL"
  ElseIf OptPLI Then
    language = "PLI"
  End If
     
  If Len(Trim(txtPath_INSTR)) > 0 And Len(Trim(txtPath_ROUT)) > 0 Then
    Screen.MousePointer = vbHourglass
    ReDim MissingRout(0)
    arrInstrImp = "ROUTINE MISSING" & vbCrLf
    Check_Routines_in_Instructions txtPath_INSTR, txtPath_ROUT
    Screen.MousePointer = vbNormal
    m_fun.Show_ShowText arrInstrImp, ""
  Else
    MsgBox "Insert correct value for manage directories.", vbInformation, "i-4.Migration"
  End If
End Sub

Public Sub Check_Routines_in_Instructions(pathInstr As String, pathRout As String)
  Dim i As Long
  Dim fileList As Collection

  Set fileList = New Collection
  
  searchFiles pathInstr, fileList

  For i = 1 To fileList.Count
    If fileList.item(i) <> "." And fileList.item(i) <> ".." Then
      If language = "CBL" Then
        checkRoutines fileList.item(i), pathRout
      ElseIf language = "PLI" Then
        checkRoutines_PLI fileList.item(i), pathRout
      End If
    End If
  Next i
End Sub

Public Sub checkRoutines(fileList As String, pathRout As String)
  Dim fileIn As Integer
  Dim Line As String
  Dim txtRout As String
  Dim txtFile As String
  Dim i As Long
        
  fileIn = FreeFile
  Open fileList For Input As fileIn
  Line Input #fileIn, Line
  Do Until EOF(fileIn)
    If Mid(Line, 7, 16) = "*  Coding:      " Then
      txtRout = Trim(Mid(Line, 17, InStr(17, Line, "-") - 17))
      Exit Do
    End If
    Line Input #fileIn, Line
  Loop
  Close fileIn
  
  txtFile = Dir(pathRout & "\" & txtRout & ".*")
  If txtFile = "" Then
    For i = 1 To UBound(MissingRout)
      If txtRout = MissingRout(i) Then
        Exit For
      End If
    Next i
    If i > UBound(MissingRout) Then
      ReDim Preserve MissingRout(UBound(MissingRout) + 1)
      MissingRout(UBound(MissingRout)) = txtRout
      arrInstrImp = arrInstrImp & txtRout & vbCrLf
    End If
  End If
  
End Sub

Public Sub checkRoutines_PLI(fileList As String, pathRout As String)
  Dim fileIn As Integer
  Dim Line As String
  Dim txtRout As String
  Dim txtFile As String
  Dim i As Long
        
  fileIn = FreeFile
  Open fileList For Input As fileIn
  Line Input #fileIn, Line
  Do Until EOF(fileIn)
    If Mid(Line, 7, 16) = "*  Coding:      " Then
      txtRout = Trim(Mid(Line, 18, InStr(18, Line, "-") - 18))
      Exit Do
    End If
    Line Input #fileIn, Line
  Loop
  Close fileIn
  
  txtFile = Dir(pathRout & "\" & txtRout & ".*")
  If txtFile = "" Then
    For i = 1 To UBound(MissingRout)
      If txtRout = MissingRout(i) Then
        Exit For
      End If
    Next i
    If i > UBound(MissingRout) Then
      ReDim Preserve MissingRout(UBound(MissingRout) + 1)
      MissingRout(UBound(MissingRout)) = txtRout
      arrInstrImp = arrInstrImp & txtRout & vbCrLf
    End If
  End If
End Sub

Private Sub cmdCheckFormat_Click()
  Dim FD As Long, FD2 As Long
  Dim Line As String
  Dim lineNumber As Long
  Dim FileName As String
  Dim NewFileName As String
  Dim i As Long
  Dim fileList As Collection
  Dim FileDir As String
  Dim str As String
  Dim Ok As String, Controllare As String, Spazi As String
  Dim NomeFile As String
  Dim NewFileDir As String
  Dim ShowText As String
  Dim NRiga As Long
  Dim Righe As String
  Dim NumChar As Integer
  Dim contr As Integer
  Dim Count As Integer
  Dim lung As Integer
    
  On Error GoTo ErrorHandler
  
  Set fileList = New Collection
  If txtPath_ROUT.text = "" Then
    MsgBox "Insert a directory", vbInformation, "i-4.Migration"
    Exit Sub
  End If
  If txtChar.text = "" Or IsNumeric(txtChar.text) = False Then
    MsgBox "Insert a limit of characters in integer", vbInformation, "i-4.Migration"
    Exit Sub
  End If
  NumChar = txtChar.text
  If NumChar < 0 Then
    MsgBox "Insert a limit of characters in integer", vbInformation, "i-4.Migration"
    Exit Sub
  End If
  Screen.MousePointer = vbHourglass
  Righe = ""
  FileDir = txtPath_ROUT.text
  NewFileDir = FileDir & "\output"
  searchFiles NewFileDir, fileList
  For i = 1 To fileList.Count
    Kill fileList.item(1)
    fileList.Remove (1)
  Next i
  searchFiles FileDir, fileList
  If Dir(NewFileDir, vbDirectory) = "" Then
    MkDir NewFileDir
  End If
  For i = 1 To fileList.Count
    If fileList.item(i) <> "." And fileList.item(i) <> ".." Then
      NRiga = 0
      contr = 0
      FD = FreeFile
      FileName = fileList.item(i)
      NomeFile = Mid(FileName, InStrRev(FileName, "\") + 1)
      Open FileName For Input As FD
      str = ""
      While Not EOF(FD)
        Do Until EOF(FD)
          Line Input #FD, Line
          NRiga = NRiga + 1
          If Len(Line) > NumChar Then
            lung = lung + 1
            For Count = NumChar + 1 To Len(Line)
              If Not Mid(Line, Count, 1) = " " Then
                contr = contr + 1
                Righe = Righe & "   " & NRiga & vbCrLf
              End If
            Next Count
            lineNumber = lineNumber + 1
            If contr = 0 Then
              If Not EOF(FD) Then
                str = str & Mid(Line, 1, NumChar) & vbCrLf
              Else
                str = str & Mid(Line, 1, NumChar)
              End If
            Else
              If Not EOF(FD) Then
                str = str & Line & vbCrLf
              Else
                str = str & Line
              End If
            End If
          Else
            If Not EOF(FD) Then
              str = str & Line & vbCrLf
            Else
              str = str & Line
            End If
          End If
        Loop
        If Not lung = 0 Then
          If contr = 0 Then
            FD2 = FreeFile
            NewFileName = NewFileDir & "\" & NomeFile
            Open NewFileName For Output As FD2
            Print #FD2, str
            Close #FD2
            Spazi = Spazi & "* " & NomeFile & vbCrLf
          Else
            Controllare = Controllare & "* " & NomeFile & " on line:" & vbCrLf & Righe & vbCrLf
            Righe = ""
          End If
        Else
          NewFileName = NewFileDir & "\" & NomeFile
          Ok = Ok & "* " & NomeFile & vbCrLf
        End If
        lung = 0
        contr = 0
      Wend
      Close #FD
    End If
  Next i
  ShowText = "--- Programs with strings more than " & NumChar & " characters ---" & vbCrLf
  If Not Controllare = "" Then
    ShowText = ShowText & Controllare & vbCrLf
  Else
    ShowText = ShowText & " - no programs -" & vbCrLf
  End If
  ShowText = ShowText & "--- Programs that were cleared to the excessive spaces ---" & vbCrLf
  If Not Spazi = "" Then
    ShowText = ShowText & Spazi & vbCrLf
  Else
    ShowText = ShowText & " - no programs -" & vbCrLf & vbCrLf
  End If
  ShowText = ShowText & "--- Programs moved with no changes ---" & vbCrLf
  If Not Ok = "" Then
    ShowText = ShowText & Ok & vbCrLf
  Else
    ShowText = ShowText & " - no programs -" & vbCrLf & vbCrLf
  End If
  Screen.MousePointer = vbNormal
  m_fun.Show_ShowText ShowText
  Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MsgBox "Insert a directory", vbInformation, "i-4.Migration"
  End If
  Screen.MousePointer = vbNormal
  'Resume Next
End Sub

Private Sub cmdCompare_Click()
  ExternalEdit m_fun.FnCompareEditor
End Sub

Private Sub CmdCopyTables_Click()
  
  On Error GoTo messageFailure
  If MsgBox("This command is going to Backup Psb and Pcb tables deleting previous backup, continue ?", vbYesNo, "Tables backup") = vbYes Then
    DropPcbTables
    'create copy of "before refresh" version
    m_fun.FnConnection.Execute "SELECT * INTO PsDLI_IstruzioniBrefr From PsDLI_Istruzioni"
    m_fun.FnConnection.Execute "SELECT * INTO PsDLI_IstrPSBbrefr FROM PsDLI_IstrPSB"
  End If
  Exit Sub
messageFailure:
  MsgBox "Impossible to create a backup copy of the tables", vbCritical
End Sub

Private Sub DropPcbTables()
  On Error GoTo IgnoreStat
  m_fun.FnConnection.Execute "DROP TABLE PsDLI_IstruzioniBrefr"
  m_fun.FnConnection.Execute "DROP TABLE PsDLI_IstrPSBbrefr"
  Exit Sub
IgnoreStat:
  Resume Next
End Sub

Private Sub CmdGenReport_Click()
  Dim rsB As Recordset, rsA As Recordset
  Dim namePgmBefore As String, namePgmAfter As String
  Dim refOrdinaleBefore As Long, refOrdinaleMoltBefore As Integer, refRoutineB As String
  Dim refOrdinaleAfter As Long, refOrdinaleMoltAfter As Integer, refRoutineA As String
  Dim IsAllineato As Boolean, moveNextCountB As Integer, moveNextCountA As Integer
  Dim i As Integer, WarningText As String, FinalText As String, EqualText As String, NewText As String
  Dim offsetOrdinaleBefore As Integer, offsetOrdinaleAfter As Integer, IsEqual As Boolean
  Dim offsetMoltBefore As Integer, offsetMoltAfter As Integer
  Dim IsProgramStart As Boolean ' mi indica se entrambi i recordset sono su primo record di un programma
  Dim countEqual As Integer, countNew As Integer, countModified As Integer
  Dim lastModified As String, ValidText As String
  
  Set rsA = m_fun.Open_Recordset("SELECT DISTINCT pgm from TMP_CheckIncapsArefr")
  If Not rsA.EOF Then
    ValidText = "Valid programs in repository: " & rsA.RecordCount & vbCrLf & vbCrLf
  End If
  rsA.Close
  
  Set rsA = m_fun.Open_Recordset("SELECT * FROM TMP_CheckIncapsArefr order by pgm, ordinale, ordinaleMolt")
  Set rsB = m_fun.Open_Recordset("SELECT * FROM TMP_CheckIncapsBrefr order by pgm, ordinale, ordinaleMolt")
  IsProgramStart = True
  IsEqual = True
  While (Not rsA.EOF And Not rsB.EOF)
    If Not (namePgmBefore = "") Then
      If namePgmBefore = namePgmAfter And Not namePgmBefore = NamePgmfromPath(rsB!PGM) And Not namePgmAfter = NamePgmfromPath(rsA!PGM) Then
        'verifichiamo qui, nel salto programma, se programmi precedenti uguali
        If IsEqual Then
          EqualText = EqualText & namePgmBefore & vbCrLf
          countEqual = countEqual + 1
        End If
        IsEqual = False
      End If
    End If
    namePgmBefore = NamePgmfromPath(rsB!PGM)
    namePgmAfter = NamePgmfromPath(rsA!PGM)
    If rsA!ordinale = 1 Then
      offsetOrdinaleAfter = 0
    End If
    If rsB!ordinale = 1 Then
      offsetOrdinaleBefore = 0
    End If
    If rsA!ordinaleMolt = 1 Then
      offsetMoltAfter = 0
    End If
    If rsB!ordinaleMolt = 1 Then
      offsetMoltBefore = 0
    End If
    If namePgmBefore = namePgmAfter And rsA!ordinale = 1 And rsB!ordinale = 1 And rsA!ordinaleMolt = 1 And rsB!ordinaleMolt = 1 Then
      IsEqual = True
    End If
    Select Case StrComp(namePgmBefore, namePgmAfter, vbTextCompare)
      Case 0  'siamo allineati come nome programma
        refOrdinaleBefore = rsB!ordinale
        refOrdinaleAfter = rsA!ordinale
        If (refOrdinaleAfter - offsetOrdinaleAfter) = (refOrdinaleBefore - offsetOrdinaleBefore) Then
          refOrdinaleMoltBefore = rsB!ordinaleMolt
          refOrdinaleMoltAfter = rsA!ordinaleMolt
          If rsA!ordinaleMolt - offsetMoltAfter = rsB!ordinaleMolt - offsetMoltBefore Then
            refRoutineB = rsB!Routine
            refRoutineA = rsA!Routine
            If rsA!Routine = rsB!Routine Then
              ' siamo allineati
              rsA.MoveNext
              rsB.MoveNext
            Else
              IsEqual = False
              IsAllineato = False
              ' disallineamento codifiche (in mezzo) DA GESTIRE
              '
              
              ' gestiamo fino ad una istruzione sopra o sotto (cancellata o inserita)
              ' vado un ordinale sotto before per vedere se � uguale ad after (1)
              ' altrimenti un ordinale sotto after per vedere se � uguale before  (2)
              ' altrimenti un ordinale sotto tutti e due per vedere se sono uguali (3)
              ' quindi 1) eliminata(quella di before)  2) inserita (quella di after)
              '        3) modificata
              moveNextCountB = 0
              ReDim arrRoutine(0)
              
              Do While (rsB!ordinale = refOrdinaleBefore And Not rsB.EOF)
                ReDim Preserve arrRoutine(UBound(arrRoutine) + 1)
                arrRoutine(UBound(arrRoutine)) = rsB!Routine
                rsB.MoveNext
                moveNextCountB = moveNextCountB + 1
                
                If rsB!Routine = rsA!Routine And rsB!ordinale = refOrdinaleBefore Then 'caso di molteplicit� diverse
                  ' ci siamo riallineati
                  IsAllineato = True
                  offsetOrdinaleBefore = offsetOrdinaleBefore + 1
                  ' segnaliamo la cancellazione di tutte le molteplicita saltate
                  ' sono contenute in arrRoutine
                  WarningText = "Program " & NamePgmfromPath(rsB!PGM) & ": Instruction number " & refOrdinaleBefore & _
                                ",  routines deleted in refresh program: " & vbCrLf
                  For i = 1 To UBound(arrRoutine)
                    WarningText = WarningText & " " & arrRoutine(i) & ","
                  Next i
                  'offest molteplicita before
                  offsetMoltBefore = UBound(arrRoutine)
                  WarningText = Left(WarningText, Len(WarningText) - 1) & "." & vbCrLf & vbCrLf
                  FinalText = FinalText & WarningText
                  If Not NamePgmfromPath(rsB!PGM) = lastModified Then
                    lastModified = NamePgmfromPath(rsB!PGM)
                    countModified = countModified + 1
                  End If
                  Exit Do
                End If
              Loop
              If (rsB!ordinale <> refOrdinaleBefore And Not rsB.EOF And NamePgmfromPath(rsB!PGM) = namePgmBefore) Then
                'siamo sull'istruzione successiva dello stesso programma confrontiamo
                ' solo solo disallineamento su molt = 1  **** CORRETTO (v. parti commentate) e se l'istruzione attuale
                ' ha molt = 1  RESTRIZIONE RIMOSSA*********
                If refOrdinaleMoltBefore = 1 Then
                  'Dim oldOrdinaleB As Long
                  'oldOrdinaleB = rsB!ordinale
                  'rsB.MoveNext
                  'If Not rsB!ordinale = oldOrdinaleB Then
                    ' torniamo alla precedente e controlliamo riallineamento istruzione
                    'rsB.MovePrevious
                    If rsB!Routine = rsA!Routine Then
                      ' istruzione eliminata (c'� in Before ma non in after)
                      ' segnalazione
                      WarningText = "Program " & NamePgmfromPath(rsB!PGM) & ": Instruction number  before refresh " & CStr(CInt(rsB!ordinale) - 1) & _
                                    ", routine  " & refRoutineB & " was deleted" & vbCrLf & vbCrLf
                      FinalText = FinalText & WarningText
                      If Not NamePgmfromPath(rsB!PGM) = lastModified Then
                        lastModified = NamePgmfromPath(rsB!PGM)
                        countModified = countModified + 1
                      End If
                      IsAllineato = True
                      offsetOrdinaleBefore = offsetOrdinaleBefore + 1
                    End If
                  'Else
                    'rsB.MovePrevious
                  'End If
                End If
                ' se non ho allineato mi muovo su after (istr/molteplicit� aggiunte)
                If Not IsAllineato Then
                  For i = 1 To moveNextCountB
                    ' mi riporto alla situazione iniziale
                    rsB.MovePrevious
                  Next i
                  moveNextCountA = 0
                  Do While (rsA!ordinale = refOrdinaleAfter And Not rsA.EOF)
                    ReDim arrRoutine(0)
                    ReDim Preserve arrRoutine(UBound(arrRoutine) + 1)
                    arrRoutine(UBound(arrRoutine)) = rsA!Routine
                    rsA.MoveNext
                    moveNextCountA = moveNextCountA + 1
                    
                    If rsB!Routine = rsA!Routine And rsA!ordinale = refOrdinaleAfter Then
                      ' ci siamo riallineati
                      IsAllineato = True
                      'offsetOrdinaleAfter = offsetOrdinaleAfter + 1
                        
                      ' segnaliamo l'inserimento di tutte le molteplicita saltate
                      ' sono contenute in arrRoutine
                      WarningText = "Program " & NamePgmfromPath(rsA!PGM) & ": Instruction number " & rsA!ordinale & _
                                    ", more routines for this instruction. " & vbCrLf
                       
                      For i = 1 To UBound(arrRoutine)
                        WarningText = WarningText & " " & arrRoutine(i) & ","
                      Next i
                      offsetMoltAfter = UBound(arrRoutine)
                      WarningText = Left(WarningText, Len(WarningText) - 1) & "." & vbCrLf & vbCrLf
                      FinalText = FinalText & WarningText
                      If Not NamePgmfromPath(rsA!PGM) = lastModified Then
                        lastModified = NamePgmfromPath(rsA!PGM)
                        countModified = countModified + 1
                      End If
                      Exit Do
                    End If
                  Loop
                  If (rsA!ordinale <> refOrdinaleAfter And Not rsA.EOF And NamePgmfromPath(rsA!PGM) = namePgmAfter) Then
                    'siamo sull'istruzione successiva dello stesso programma confrontiamo
                    ' solo solo disallineamento su molt = 1 **** CORRETTO (v. parti commentate) e se l'istruzione attuale
                    ' ha molt = 1 RESTRIZIONE RIMOSSA
                    If refOrdinaleMoltAfter = 1 Then
                      'Dim oldOrdinaleA As Long
                      'oldOrdinaleA = rsA!ordinale
                      'rsA.MoveNext
                      'If Not rsA!ordinale = oldOrdinaleA Then
                        ' torniamo alla precedente e controlliamo riallineamento istruzione
                        'rsA.MovePrevious
                        If rsA!Routine = rsB!Routine Then
                          ' istruzione inserita (c'� in after ma non in Before)
                          ' segnalazione
                          WarningText = "Program " & NamePgmfromPath(rsB!PGM) & ": Instruction number after refresh " & str(CInt(rsA!ordinale) - moveNextCountA) & _
                                        ", routine  " & refRoutineA & " was added" & vbCrLf & vbCrLf
                          FinalText = FinalText & WarningText
                          If Not NamePgmfromPath(rsB!PGM) = lastModified Then
                            lastModified = NamePgmfromPath(rsB!PGM)
                            countModified = countModified + 1
                          End If
                          IsAllineato = True
                          offsetOrdinaleAfter = offsetOrdinaleAfter + 1
                        End If
                      'Else
                        'rsA.MovePrevious
                      'End If
                    End If
                  End If
                End If ' chiusura secondo controllo (su After)
                If Not IsAllineato Then
                  For i = 1 To moveNextCountA
                    ' mi riporto alla situazione iniziale di After
                    rsA.MovePrevious
                  Next i
                  ReDim arrBAllign(0)
                  ReDim arrAAllign(0)
                  ReDim Preserve arrBAllign(UBound(arrBAllign) + 1)
                  arrBAllign(UBound(arrBAllign)) = refRoutineB
                  ReDim Preserve arrAAllign(UBound(arrAAllign) + 1)
                  arrAAllign(UBound(arrAAllign)) = refRoutineA
                  Do While (Not rsA.EOF And Not rsB.EOF)
                    rsA.MoveNext
                    rsB.MoveNext
                    If Not rsA.EOF And Not rsB.EOF Then
                      If NamePgmfromPath(rsA!PGM) = NamePgmfromPath(rsB!PGM) And NamePgmfromPath(rsA!PGM) = namePgmAfter And _
                         rsA!ordinale = rsB!ordinale And _
                         rsA!ordinaleMolt = rsB!ordinaleMolt Then
                        
                        If rsA!Routine = rsB!Routine Then
                          IsAllineato = True
                          If UBound(arrBAllign) = 1 Then
                            ' segnalazione di modifica del record precedente
                            WarningText = "Program " & namePgmBefore & "  instruction number " & refOrdinaleBefore & _
                                          " : routine " & refOrdinaleMoltBefore & " was modified." & vbCrLf & _
                                          "Routine before refresh: " & refRoutineB & " ,routine after refresh: " & refRoutineA & vbCrLf & _
                                          "as an alternative an instruction was deleted and another was added " & vbCrLf & vbCrLf
                                     
                            FinalText = FinalText & WarningText
                          Else
                            WarningText = "Program " & namePgmBefore & "  from instruction number" & refOrdinaleBefore & _
                                          " : some routines were modified." & vbCrLf
                            For i = 1 To UBound(arrBAllign)
                              WarningText = WarningText & "Routine before refresh: " & arrBAllign(i) & " ,routine after refresh: " & arrAAllign(i) & vbCrLf
                            Next i
                            WarningText = WarningText & vbCrLf
                            FinalText = FinalText & WarningText
                          End If
                          If Not namePgmBefore = lastModified Then
                            lastModified = namePgmBefore
                            countModified = countModified + 1
                          End If
                          Exit Do
                        Else 'accumulo routine non allineate, le usero se si mantiene allineamento su numero istruzioni/molteplicit�
                          ReDim Preserve arrBAllign(UBound(arrBAllign) + 1)
                          arrBAllign(UBound(arrBAllign)) = rsB!Routine
                          ReDim Preserve arrAAllign(UBound(arrAAllign) + 1)
                          arrAAllign(UBound(arrAAllign)) = rsA!Routine
                        End If
                      Else
                        If Not NamePgmfromPath(rsA!PGM) = namePgmAfter And Not NamePgmfromPath(rsB!PGM) = namePgmBefore Then
                          ' sono rimasti allineati come numero di istruzioni anche se non � stata trovata corrispondenza
                          IsAllineato = True
                          'elenco modifiche
                          If UBound(arrBAllign) = 1 Then
                            ' segnalazione di modifica del record precedente
                            WarningText = "Program " & namePgmBefore & "  instruction number " & refOrdinaleBefore & _
                                          " : routine " & refOrdinaleMoltBefore & " was modified." & vbCrLf & _
                                          "Routine before refresh: " & refRoutineB & " ,routine after refresh: " & refRoutineA & vbCrLf & _
                                          "as an alternative an instruction was deleted and another was added " & vbCrLf & vbCrLf
                                   
                            FinalText = FinalText & WarningText
                            If Not namePgmBefore = lastModified Then
                              lastModified = namePgmBefore
                              countModified = countModified + 1
                            End If
                          Else
                            WarningText = "Program " & namePgmBefore & "  from instruction number" & refOrdinaleBefore & _
                                          " : some routines were modified." & vbCrLf
                            For i = 1 To UBound(arrBAllign)
                              WarningText = WarningText & "Routine before refresh: " & arrBAllign(i) & " ,routine after refresh: " & arrAAllign(i) & vbCrLf
                            Next i
                            WarningText = WarningText & vbCrLf
                            FinalText = FinalText & WarningText
                            If Not namePgmBefore = lastModified Then
                              lastModified = namePgmBefore
                              countModified = countModified + 1
                            End If
                          End If
                          Exit Do
                        Else ' perso allineamento, non ha senso confronto delle codifiche, riallineamento sotto
                          IsAllineato = False
                          ReDim arrBAllign(0)
                          ReDim arrAAllign(0)
                          Exit Do
                        End If
                      End If
                    End If
                  Loop
                End If
                If Not IsAllineato Then
                  'segnalazione di non allineamento istruzioni
                  WarningText = "Program " & namePgmBefore & ", is impossible to find an allignment from instruction " & refOrdinaleBefore & vbCrLf & vbCrLf
                               
                  FinalText = FinalText & WarningText
                  If Not namePgmBefore = lastModified Then
                    lastModified = namePgmBefore
                    countModified = countModified + 1
                  End If
                  ' spostamento su Before fino a programma successivo
                  If Not rsB.EOF Then
                    While (NamePgmfromPath(rsB!PGM) = namePgmBefore)
                      rsB.MoveNext
                    Wend
                  End If
                  'spostamento su After fino a programma successivo
                  If Not rsA.EOF Then
                    While (NamePgmfromPath(rsA!PGM) = namePgmAfter)
                      rsA.MoveNext
                    Wend
                  End If
                  IsProgramStart = True
                  'TODO controlli se siamo ARRIVATI in fondo a recordset
                End If
              End If  ' chiusura controllo disallineamento routine
            End If
          Else  ' ordinali diversi --- salto programma in una delle due tabelle
            IsEqual = False
            If Not NamePgmfromPath(rsA!PGM) = namePgmAfter Then
              'after ne ha di meno, quindi rimosse
              ' segnalo istruzioni rimosse
              WarningText = "Program " & namePgmBefore & ":  some instructions was deleted." & vbCrLf & vbCrLf
              FinalText = FinalText & WarningText
              If Not namePgmBefore = lastModified Then
                lastModified = namePgmBefore
                countModified = countModified + 1
              End If
              'faccio saltare before al programma successivo
              While (NamePgmfromPath(rsB!PGM) = namePgmBefore And Not rsB.EOF)
                rsB.MoveNext
              Wend
              IsProgramStart = True
            Else
              'before ne ha di meno, quindi aggiunte
              ' segnalo istruzioni aggiunte
              WarningText = "Program " & namePgmAfter & ":  some instructions was added." & vbCrLf & vbCrLf
              FinalText = FinalText & WarningText
              If Not namePgmAfter = lastModified Then
                lastModified = namePgmAfter
                countModified = countModified + 1
              End If
              'faccio saltare after al programma successivo
              While (NamePgmfromPath(rsA!PGM) = namePgmAfter And Not rsA.EOF)
                rsA.MoveNext
              Wend
              IsProgramStart = True
            End If
          End If
        Else ' perso allineamento istruzioni per differenza di molteplicit�
             ' oppure differenza del numero di istruzioni e salto a nuovo programma
          If rsB!ordinale > rsA!ordinale Then
            ' due casi  1) molteplicit� in pi� su after a parit� di ordinale
            '           2) oppure after � gi� su nuovo programma
            If NamePgmfromPath(rsA!PGM) = namePgmAfter Then '(caso 1)
              ' segnaliamo che sull'istruzione ci sono pi� molteplicita
              '            su programma after
              WarningText = "Program " & namePgmAfter & ": Instruction number " & rsA!ordinale & _
                            " , more routines for this instruction. " & vbCrLf & vbCrLf
              FinalText = FinalText & WarningText
              If Not namePgmAfter = lastModified Then
                lastModified = namePgmAfter
                countModified = countModified + 1
              End If
              ' avanzo su after fino ad ordinale successivo
              While (rsA!ordinale = refOrdinaleAfter And Not rsA.EOF)
                rsA.MoveNext
              Wend
              'TODO gestire se siamo a fine recordset
            Else
              ' segnaliamo che programma before ha pi� istruzioni
              WarningText = "Program " & namePgmBefore & ":  some instructions was deleted." & vbCrLf & vbCrLf
              FinalText = FinalText & WarningText
              If Not namePgmBefore = lastModified Then
                lastModified = namePgmBefore
                countModified = countModified + 1
              End If
              ' avanzo su before fino a programma successivo
              While (NamePgmfromPath(rsB!PGM) = namePgmBefore And Not rsB.EOF)
                rsB.MoveNext
              Wend
              IsProgramStart = True
            End If
          Else
            ' due casi  1) molteplicit� in pi� su before a parit� di ordinale
            '           2) oppure before � gi� su nuovo programma
            If NamePgmfromPath(rsB!PGM) = namePgmBefore Then '(caso 1)
              ' segnaliamo che sull'istruzione ci sono pi� molteplicita
              '            su programma before
              WarningText = "Program " & namePgmBefore & ": Instruction number " & refOrdinaleBefore & _
                            " , less routines for this instruction. " & vbCrLf & vbCrLf
              FinalText = FinalText & WarningText
              If Not namePgmBefore = lastModified Then
                lastModified = namePgmBefore
                countModified = countModified + 1
              End If
              ' avanzo su before fino ad ordinale successivo
              While (rsB!ordinale = refOrdinaleBefore And Not rsB.EOF)
                rsB.MoveNext
              Wend
              IsProgramStart = True
            Else
              ' segnaliamo che programma after ha pi� istruzioni
              WarningText = "Program " & namePgmAfter & ":  some instructions was added." & vbCrLf & vbCrLf
              FinalText = FinalText & WarningText
              If Not namePgmAfter = lastModified Then
                lastModified = namePgmAfter
                countModified = countModified + 1
              End If
              ' avanzo su after fino a programma successivo
              While (NamePgmfromPath(rsA!PGM) = namePgmAfter And Not rsA.EOF)
                rsA.MoveNext
              Wend
              IsProgramStart = True
              'se siamo a fine record faccio qui il controllo
            End If
          End If
        End If
          
      Case 1
        If rsA!ordinale = 1 And rsA!ordinaleMolt = 1 And rsB!ordinale = 1 And rsB!ordinaleMolt = 1 Then 'If IsProgramStart Then 'nella tabella after manca programma
          ' segnaliamo programma  come nuovo
          NewText = NewText & namePgmAfter & vbCrLf
          countNew = countNew + 1
          ' avanzo su after fino a programma successivo
          Do While (Not rsA.EOF)
            If Not NamePgmfromPath(rsA!PGM) = namePgmAfter Then
              Exit Do
            End If
            rsA.MoveNext
          Loop
        Else 'il programma di after ha pi� istruzioni (o molteplicit� sull'ultima)
          WarningText = "Program " & namePgmAfter & ": some instructions was added or there are more routines in the last instruction." & vbCrLf & vbCrLf
          FinalText = FinalText & WarningText
          If Not namePgmAfter = lastModified Then
            lastModified = namePgmAfter
            countModified = countModified + 1
          End If
          ' avanzo su after fino a programm successivo
          Do While (Not rsA.EOF)
            If Not NamePgmfromPath(rsA!PGM) = namePgmAfter Then
              Exit Do
            End If
            rsA.MoveNext
          Loop
          IsProgramStart = True
        End If

      Case -1
        If rsA!ordinale = 1 And rsA!ordinaleMolt = 1 And rsB!ordinale = 1 And rsB!ordinaleMolt = 1 Then 'If IsProgramStart Then 'nella tabella after � stato inserito nuovo programma
          ' segnaliamo programma before come non refreshato
          
          'WarningText = "Program " & namePgmBefore & " wasn't refreshed." & vbCrLf & vbCrLf
          'FinalText = FinalText & WarningText
          
          ' avanzo su before fino a programm successivo
          Do While (Not rsB.EOF)
            If Not NamePgmfromPath(rsB!PGM) = namePgmBefore Then
              Exit Do
            End If
            rsB.MoveNext
          Loop
        Else 'il programma di before ha pi� istruzioni (o molteplicit� sull'ultima)
          WarningText = "Program " & namePgmBefore & ": some instructions was deleted or there are less routines in the last instruction." & vbCrLf & vbCrLf
          FinalText = FinalText & WarningText
          If Not namePgmBefore = lastModified Then
            lastModified = namePgmBefore
            countModified = countModified + 1
          End If
          ' avanzo su before fino a programm successivo
          Do While (Not rsB.EOF)
            If Not NamePgmfromPath(rsB!PGM) = namePgmBefore Then
              Exit Do
            End If
            rsB.MoveNext
          Loop
          IsProgramStart = True
        End If
    End Select
  Wend
  ' Controllare se siamo alla fine sia su after e before, oppure su uno solo dei due
  If rsB.EOF And Not rsA.EOF Then ' cerchiamo tutti i programmi aggiunti
    If namePgmAfter = namePgmBefore Then
      'Sono state aggiunte istruzioni
      WarningText = "Program " & namePgmAfter & ": some instructions was added or there are more routines in the last instruction." & vbCrLf & vbCrLf
      FinalText = FinalText & WarningText
      If Not namePgmAfter = lastModified Then
        lastModified = namePgmAfter
        countModified = countModified + 1
      End If
    Else
      namePgmAfter = NamePgmfromPath(rsA!PGM)
      NewText = NewText & namePgmAfter & vbCrLf
      countNew = countNew + 1
    End If
    rsA.MoveNext
    While Not rsA.EOF
      If Not NamePgmfromPath(rsA!PGM) = namePgmAfter Then
        namePgmAfter = NamePgmfromPath(rsA!PGM)
        NewText = NewText & namePgmAfter & vbCrLf
        countNew = countNew + 1
      End If
      rsA.MoveNext
    Wend
  ElseIf rsA.EOF And Not rsB.EOF Then ' cerchiamo tutti i programmi mancanti
    If namePgmAfter = namePgmBefore Then
      'Sono state aggiunte istruzioni
      WarningText = "Program " & namePgmAfter & ": some instructions was deleted or there are less routines in the last instruction." & vbCrLf & vbCrLf
      FinalText = FinalText & WarningText
      If Not namePgmAfter = lastModified Then
        lastModified = namePgmAfter
        countModified = countModified + 1
      End If
    Else
      namePgmBefore = NamePgmfromPath(rsB!PGM)
      
      'WarningText = "Program " & namePgmBefore & " wasn't refreshed." & vbCrLf & vbCrLf
      'FinalText = FinalText & WarningText
    End If
    rsB.MoveNext
    While Not rsB.EOF
      If Not NamePgmfromPath(rsB!PGM) = namePgmBefore Then
        namePgmBefore = NamePgmfromPath(rsB!PGM)
        'WarningText = "Program " & namePgmAfter & " wasn't refreshed." & vbCrLf & vbCrLf
        'FinalText = FinalText & WarningText
      End If
      rsB.MoveNext
    Wend
  End If
  rsA.Close
  rsB.Close
  'Stampa finale
  FinalText = "Programs Modified(" & countModified & "): " & vbCrLf & _
               FinalText & vbCrLf
  If Len(NewText) Then
    NewText = "New Programs(" & countNew & "): " & vbCrLf & _
               NewText & vbCrLf
    FinalText = NewText & FinalText
  End If
  If Len(EqualText) Then
    EqualText = "Programs not modified(" & countEqual & "): " & vbCrLf & _
                 EqualText & vbCrLf
    FinalText = EqualText & FinalText
  End If
  If Len(ValidText) Then
    FinalText = ValidText & FinalText
  End If
  m_fun.Show_ShowText FinalText, "Report"
End Sub

Private Function NamePgmfromPath(path As String) As String
  If InStrRev(path, "\") Then
    NamePgmfromPath = Right(path, Len(path) - InStrRev(path, "\"))
  End If
End Function

Private Function NamePgmfromIdPgm(Id As Long) As String
  Dim rsA As Recordset
  
  Set rsA = m_fun.Open_Recordset("SELECT Nome FROM Bs_Oggetti where IdOggetto = " & Id)
  If Not rsA.EOF Then
    NamePgmfromIdPgm = rsA!nome
  End If
  rsA.Close
End Function

Function getPSBBrefr(pIdPgm, pRiga) As String
  Dim rsA As Recordset
  
  Set rsA = m_fun.Open_Recordset("SELECT IdPsb FROM PsDLI_IstrPSBbrefr where IdPgm = " & pIdPgm & " and Riga = " & pRiga)
  While Not rsA.EOF
    getPSBBrefr = getPSBBrefr & rsA!IdPSB & ";"
    rsA.MoveNext
  Wend
  rsA.Close
End Function

Public Function IsPrjDBExistingTable(ByVal tableName As String) As Boolean
  Dim ConnectString As String
  Dim ADOXConnection As Object
  Dim ADODBConnection As Object
  Dim Table As Variant

  ConnectString = m_fun.FnConnection
  
  Set ADOXConnection = CreateObject("ADOX.Catalog")
  Set ADODBConnection = CreateObject("ADODB.Connection")
  ADODBConnection.Open ConnectString
  ADOXConnection.ActiveConnection = ADODBConnection
  For Each Table In ADOXConnection.Tables
    If LCase(Table.name) = LCase(tableName) Then
      IsPrjDBExistingTable = True
      Exit For
    End If
  Next
  ADODBConnection.Close
End Function

Private Sub CmdPcbAllign_Click()
  Dim rsB As Recordset, rsA As Recordset
  Dim idPgmBefore As Long, idPgmAfter As Long
  Dim refStatementBefore As String
  Dim refStatementAfter As String
  Dim IsAllineato As Boolean, moveNextCountB As Integer, moveNextCountA As Integer
  Dim i As Integer, WarningText As String, FinalText As String, EqualText As String, NewText As String
  Dim offsetOrdinaleBefore As Integer, offsetOrdinaleAfter As Integer, IsEqual As Boolean
  Dim offsetMoltBefore As Integer, offsetMoltAfter As Integer
  Dim IsProgramStart As Boolean ' mi indica se entrambi i recordset sono su primo record di un programma
  Dim countEqual As Integer, countNew As Integer, countModified As Integer
  Dim lastModified As String, ValidText As String
  Dim IdAdded() As Long, IdRemoved() As Long
  Dim idDiff As Long, txtAllignment As String
  
  ReDim IdAdded(0)
  ReDim IdRemoved(0)
  
  If Not IsPrjDBExistingTable("PsdLi_IstruzioniBrefr") Then
    MsgBox ("You didn't Backup the previous version of Instruction table, please import it from another database using the botton: " & Command1.Caption)
    Exit Sub
  End If
  If Not IsPrjDBExistingTable("PsdLi_IstrPSBBrefr") Then
    MsgBox ("You didn't Backup the previous version of PSB table, please import it from another database using the botton: " & Command1.Caption)
    Exit Sub
  End If
  m_fun.FnConnection.Execute "Delete from RefreshIstruzioniPcb"
  Set rsA = m_fun.Open_Recordset("SELECT * FROM PsdLi_Istruzioni order by Idpgm, riga")
  Set rsB = m_fun.Open_Recordset("SELECT * FROM PsdLi_IstruzioniBrefr order by Idpgm, riga")
  IsProgramStart = True
  IsEqual = True
  While (Not rsA.EOF And Not rsB.EOF)
    If Not (idPgmBefore = 0) Then
      If idPgmBefore = idPgmAfter And Not idPgmBefore = rsB!idPgm And Not idPgmAfter = rsA!idPgm Then
        'verifichiamo qui, nel salto programma, se programmi precedenti uguali
        If IsEqual Then
          EqualText = EqualText & idPgmBefore & vbCrLf
          countEqual = countEqual + 1
        End If
        IsEqual = False
      End If
    End If
    idPgmBefore = rsB!idPgm
    idPgmAfter = rsA!idPgm
    idDiff = (idPgmBefore - idPgmAfter)
    Select Case idDiff
      Case 0  'siamo allineati come ID programma
        refStatementBefore = rsB!Istruzione & " " & rsB!statement
        refStatementAfter = rsA!Istruzione & " " & rsA!statement
        If refStatementAfter = refStatementBefore Then
          ' siamo allineati
          If Not IsNull(rsB!ordPcb_MG) Then
            txtAllignment = txtAllignment & vbCrLf & _
                            "ID " & rsA!idPgm & " Instruction " & refStatementAfter & vbCrLf & _
                            "  Old Row " & rsB!Riga & " Old numPcb " & rsB!ordPcb_MG & " New Row " & rsA!Riga & " New numPcb " & rsA!ordPcb
            m_fun.FnConnection.Execute "Insert Into RefreshIstruzioniPcb (Nome,PCK,idPgm,riga,NEW_RIGA,statement,istruzione,PSBList) " & _
                                       "VALUES ('" & NamePgmfromIdPgm(rsA!idPgm) & "'," & rsB!ordPcb_MG & "," & rsA!idPgm & "," & rsB!Riga & "," & _
                                       rsA!Riga & ",'" & Replace(rsA!statement, "'", "''") & "','" & _
                                       rsA!Istruzione & "','" & getPSBBrefr(rsA!idPgm, rsB!Riga) & " ')"
          End If
          'AC  inserimento lista PSB utilizzando le info sulle righe (old/new) e IdPgm
          rsA.MoveNext
          rsB.MoveNext
        Else
          IsEqual = False
          IsAllineato = False
      
          ' gestiamo fino ad una istruzione sopra o sotto (cancellata o inserita)
          ' vado una riga sotto before per vedere se � uguale ad after (1)
          ' altrimenti una riga sotto after per vedere se � uguale before  (2)
          ' altrimenti una riga sotto tutti e due per vedere se sono uguali (3)
          ' quindi 1) eliminata(quella di before)  2) inserita (quella di after)
          '        3) modificata
          moveNextCountB = 0
          ReDim arrRoutine(0)
          
          rsB.MoveNext
          moveNextCountB = moveNextCountB + 1
                  
          If rsB!idPgm = idPgmAfter And rsB!Istruzione & " " & rsB!statement = refStatementAfter Then
            ' ci siamo riallineati
            IsAllineato = True
            offsetOrdinaleBefore = offsetOrdinaleBefore + 1
            If Not IsNull(rsB!ordPcb_MG) Then
              txtAllignment = txtAllignment & vbCrLf & _
                              "ID " & rsA!idPgm & " Instruction " & refStatementAfter & vbCrLf & _
                              "  Old Row " & rsB!Riga & " Old numPcb " & rsB!ordPcb_MG & " New Row " & rsA!Riga & " New numPcb " & rsA!ordPcb
              m_fun.FnConnection.Execute "Insert Into RefreshIstruzioniPcb (Nome,PCK,idPgm,riga,NEW_RIGA,statement,istruzione) " & _
                                         "VALUES ('" & NamePgmfromIdPgm(rsA!idPgm) & "'," & rsB!ordPcb_MG & "," & rsA!idPgm & "," & rsB!Riga & "," & rsA!Riga & _
                                         ",'" & Replace(rsA!statement, "'", "''") & "','" & rsA!Istruzione & "','" & getPSBBrefr(rsA!idPgm, rsB!Riga) & " ')"
            End If
            'AC  inerimento lista PSB utilizzando le info sulle righe (old/new) e IdPgm
            
            WarningText = "Program ID " & rsB!idPgm & ": Instruction  " & refStatementBefore & _
                          " deleted in refresh program. " & vbCrLf & vbCrLf
            'offest molteplicita before
            FinalText = FinalText & WarningText
          End If
                  
          ' se non ho allineato mi muovo su after (istr/molteplicit� aggiunte)
          If Not IsAllineato Then
            For i = 1 To moveNextCountB
              ' mi riporto alla situazione iniziale
              rsB.MovePrevious
            Next i
            moveNextCountA = 0
            rsA.MoveNext
            moveNextCountA = moveNextCountA + 1
            
            If rsA!idPgm = idPgmBefore And rsA!Istruzione & " " & rsA!statement = refStatementBefore Then
              ' ci siamo riallineati
              IsAllineato = True
              'offsetOrdinaleAfter = offsetOrdinaleAfter + 1
              If Not IsNull(rsB!ordPcb_MG) Then
                txtAllignment = txtAllignment & vbCrLf & _
                                "ID " & rsA!idPgm & " Instruction " & refStatementAfter & vbCrLf & _
                                "  Old Row " & rsB!Riga & " Old numPcb " & rsB!ordPcb_MG & " New Row " & rsA!Riga & " New numPcb " & rsA!ordPcb
                m_fun.FnConnection.Execute "Insert Into RefreshIstruzioniPcb (Nome,PCK,idPgm,riga,NEW_RIGA,statement,istruzione) " & _
                                           "VALUES ('" & NamePgmfromIdPgm(rsA!idPgm) & "'," & rsB!ordPcb_MG & "," & rsA!idPgm & "," & rsB!Riga & "," & rsA!Riga & _
                                           ",'" & Replace(rsA!statement, "'", "''") & "','" & rsA!Istruzione & "','" & getPSBBrefr(rsA!idPgm, rsB!Riga) & " ')"
              End If
              'AC  inserimento lista PSB utilizzando le info sulle righe (old/new) e IdPgm
              WarningText = "Program ID " & rsA!idPgm & ":" & refStatementAfter & " added in program." & vbCrLf & vbCrLf
              FinalText = FinalText & WarningText
            End If
          End If ' chiusura secondo controllo (su After)
          If Not IsAllineato Then
            For i = 1 To moveNextCountA
              ' mi riporto alla situazione iniziale di After
              rsA.MovePrevious
            Next i
            rsA.MoveNext
            rsB.MoveNext
            If Not rsA.EOF And Not rsB.EOF Then
              If rsA!idPgm = rsB!idPgm And _
                 (rsA!Istruzione & " " & rsA!statement) = (rsB!Istruzione & " " & rsB!statement) Then
                IsAllineato = True
                ' segnalazione di modifica del record precedente
                WarningText = "Program " & idPgmBefore & "  instruction  " & refStatementBefore & _
                              "(row:" & rsB!Riga & ") was substituted with Instruction " & refStatementAfter & " (row:" & rsA!Riga & ")." & vbCrLf & vbCrLf
                FinalText = FinalText & WarningText
              ElseIf Not rsA!idPgm = idPgmAfter And Not rsB!idPgm = idPgmBefore Then
                ' sono rimasti allineati come numero di istruzioni anche se non � stata trovata corrispondenza
                IsAllineato = True
                WarningText = "Program " & idPgmBefore & "  instruction  " & refStatementBefore & _
                              "(row:" & rsB!Riga & ") was substituted with Instruction " & refStatementAfter & " (row:" & rsA!Riga & ")." & vbCrLf & vbCrLf
                FinalText = FinalText & WarningText
              ElseIf Not rsA!idPgm = idPgmAfter Then
                'riallineo before (passo a id successivo)-> istruzioni eliminate
                WarningText = "Program " & idPgmBefore & " one or more instructions was deleted  " & vbCrLf & vbCrLf
                Do While Not rsB.EOF
                  rsB.MoveNext
                  If rsB!idPgm <> idPgmBefore Then
                    Exit Do
                  End If
                Loop
              Else
                'riallineo after  (passo a id successivo)-> istruzioni aggiunte
                WarningText = "Program " & idPgmBefore & " one or more instructions was added   " & vbCrLf & vbCrLf
                Do While Not rsA.EOF
                  rsA.MoveNext
                  If rsA!idPgm <> idPgmAfter Then
                    Exit Do
                  End If
                Loop
              End If
            End If
          End If
        End If
      
      Case Is > 0
        ' inseriti programmi
        'scorro after finch� id >=
        'lista dei programmi(id) nuovi
        ReDim Preserve IdAdded(UBound(IdAdded) + 1)
        IdAdded(UBound(IdAdded)) = rsA!idPgm
        Do While Not rsA.EOF
          rsA.MoveNext
          If Not rsA.EOF Then
            If rsA!idPgm >= idPgmBefore Then
              Exit Do
            Else
              ReDim Preserve IdAdded(UBound(IdAdded) + 1)
              IdAdded(UBound(IdAdded)) = rsA!idPgm
            End If
          End If
        Loop
    
      Case Is < 0
        ' eliminati programmi
        'scorro before finch� id >=
        'lista dei programmi(id) mancanti
        ReDim Preserve IdRemoved(UBound(IdRemoved) + 1)
        IdRemoved(UBound(IdRemoved)) = rsB!idPgm
        Do While Not rsB.EOF
          rsB.MoveNext
          If rsB!idPgm <> idPgmAfter Then
            Exit Do
          Else
            ReDim Preserve IdRemoved(UBound(IdRemoved) + 1)
            IdRemoved(UBound(IdRemoved)) = rsB!idPgm
          End If
        Loop
    End Select
  Wend
  rsA.Close
  rsB.Close
  'Stampa finale
  FinalText = "***** Modifid,added and deleted instructions *****" & vbCrLf & vbCrLf & FinalText
  m_fun.Show_ShowText FinalText & vbCrLf & "***** List of PCB to update from previous project *****" & vbCrLf & txtAllignment, "Report"
End Sub

Private Sub CmdRefrComp_Click()
  Dim checkIncapsB As Boolean, checkIncapsA As Boolean, checkRoutine As Boolean
  
  If optCobol Then
    language = "CBL"
  ElseIf OptPLI Then
    language = "PLI"
  End If
  
  If Len(Trim(TxtBeforePath)) Or Len(Trim(TxtAfterPath)) Then
    Screen.MousePointer = vbHourglass
    On Error GoTo noTable
    If Len(TxtBeforePath) Then
      If MsgBox("Do you want to delete CheckIncapsBrefr table?", vbYesNo + vbInformation) = vbYes Then
        m_fun.FnConnection.Execute "DELETE * FROM TMP_checkIncapsBrefr"
      End If
      checkIncapsB = True
    End If
    If Len(TxtAfterPath) Then
      If MsgBox("Do you want to delete CheckIncapsArefr  table?", vbYesNo + vbInformation) = vbYes Then
        m_fun.FnConnection.Execute "DELETE * FROM TMP_checkIncapsArefr"
      End If
      checkIncapsA = True
    End If
    
    DLLExtParser.check_Routine_Incaps_Refresh TxtBeforePath, TxtAfterPath, language
    Screen.MousePointer = vbNormal
    MsgBox "Operation Complete.", vbOKOnly + vbInformation
  Else
    MsgBox "Insert correct value for check directories.", vbInformation, "i-4.Migration"
  End If
  Exit Sub
noTable:
  Screen.MousePointer = vbNormal
  MsgBox "You need a Check-Repository", vbExclamation, "i-4.Migration"
End Sub

'''''''''''''''''''''''''''''''''''
' SQ 17-12-08
' TMP: GESTIONE REV_ & PROC
'''''''''''''''''''''''''''''''''''
Private Sub cmdREV__Click()
  ' Funziona sia per CBL che per PLI
  Dim checkIncaps As Boolean, checkRoutine As Boolean, options As String
  
  options = Replace(str(chkMP) & str(chkdelX) & str(chkchL), " ", "")
  If Len(Trim(txtPath_IN)) Or Len(Trim(txtPath_RT)) Then
    Screen.MousePointer = vbHourglass
    
    DLLExtParser.patchREV_X txtPath_IN, txtPath_RT, checkIncaps, checkRoutine, language, options
    
    Screen.MousePointer = vbNormal
    MsgBox "Operation Complete.", vbOKOnly + vbInformation
  Else
    MsgBox "Insert correct value for check directories.", vbInformation, "i-4.Migration"
    End If
  Exit Sub
noTable:
  Screen.MousePointer = vbNormal
  MsgBox "You need a Check-Repository", vbExclamation, "i-4.Migration"
End Sub

Private Sub cmdShow_IN_Click()
  m_fun.Show_DisplayTab "TMP_checkIncaps", "Encapsulation Routines"
End Sub

Private Sub cmdShow_NONIN_Click()
  m_fun.Show_DisplayTab "TMP_checknonIncaps", "Non Encapsulation Routines"
End Sub

Private Sub cmdShow_RT_Click()
  m_fun.Show_DisplayTab "TMP_checkRoutine", "Generated Routines"
End Sub

Private Sub cmdShowCallIncaps_Click()
  m_fun.Show_DisplayTab "TMP_CallIncaps", "Incapsulated Routines"
End Sub

Private Sub cmdSplitRout_Click()
  ' Funziona sia per CBL che per PLI
  If optCobol Then
    language = "CBL"
  ElseIf OptPLI Then
    language = "PLI"
  End If
  
  ' Mauro: Temporaneo!!!!
  ' Da gestire una nuova variabile d'ambiente, come anche per il nome delle routine generate
  If txtInstrxRout = "" Then
    NumIstrxRout = 20
  Else
    NumIstrxRout = CInt(txtInstrxRout)
  End If
  
  If Len(Trim(txtPath_ROUT)) Then
    Screen.MousePointer = vbHourglass
    Split_Small_Routine txtPath_ROUT
    Screen.MousePointer = vbNormal
    MsgBox "Routines Complete.", vbOKOnly + vbInformation
  Else
    MsgBox "Insert correct value in Routine Files Directory.", vbInformation, "i-4.Migration"
  End If
End Sub

Private Sub cmdStart_Click()
  ' Funziona sia per CBL che per PLI
  Dim checkIncaps As Boolean, checkRoutine As Boolean
  
  If optCobol Then
    language = "CBL"
  ElseIf OptPLI Then
    language = "PLI"
  End If
  
  If Len(Trim(txtPath_IN)) Or Len(Trim(txtPath_RT)) Then
    Screen.MousePointer = vbHourglass
    On Error GoTo noTable
    If Len(txtPath_IN) Then
      If MsgBox("Do you want to delete CheckIncaps table?", vbYesNo + vbInformation) = vbYes Then
        m_fun.FnConnection.Execute "DELETE * FROM TMP_checkIncaps"
        m_fun.FnConnection.Execute "DELETE * FROM TMP_checkNonIncaps"
        m_fun.FnConnection.Execute "DELETE * FROM TMP_CallIncaps"
      End If
      checkIncaps = True
    End If
    If Len(txtPath_RT) Then
      If MsgBox("Do you want to delete CheckRoutine table?", vbYesNo + vbInformation) = vbYes Then
        m_fun.FnConnection.Execute "DELETE * FROM TMP_checkRoutine"
      End If
      checkRoutine = True
    End If
    DLLExtParser.check_Routine_Incaps txtPath_IN, txtPath_RT, checkIncaps, checkRoutine, language
    Screen.MousePointer = vbNormal
    MsgBox "Operation Complete.", vbOKOnly + vbInformation
  Else
    MsgBox "Insert correct value for check directories.", vbInformation, "i-4.Migration"
  End If
  Exit Sub
noTable:
  Screen.MousePointer = vbNormal
  MsgBox "You need a Check-Repository", vbExclamation, "i-4.Migration"
End Sub

Private Sub cmdSplitIstr_Click()
  ' Funziona sia per CBL che per PLI
  
  If optCobol Then
    language = "CBL"
  ElseIf OptPLI Then
    language = "PLI"
  End If
  
  If Len(Trim(txtPath_ROUT)) Then
    Screen.MousePointer = vbHourglass
    Split_Instructions_in_Routine txtPath_ROUT
    Screen.MousePointer = vbNormal
    MsgBox "Split Complete.", vbOKOnly + vbInformation
  Else
    MsgBox "Insert correct value in Routine Files Directory.", vbInformation, "i-4.Migration"
  End If
End Sub

Public Sub Split_Instructions_in_Routine(pathRout As String)
  Dim i As Long
  Dim fileList As Collection
  Dim dirFile As String

  Set fileList = New Collection
  
  searchFiles pathRout, fileList

  For i = 1 To fileList.Count
    If fileList.item(i) <> "." And fileList.item(i) <> ".." Then
      dirFile = Left(fileList.item(i), InStrRev(fileList.item(i), "\") - 1)
      If language = "CBL" Then
        SplitRoutine fileList.item(i), dirFile
      ElseIf language = "PLI" Then
        SplitRoutine_PLI fileList.item(i), dirFile
      End If
    End If
  Next i
End Sub

Public Sub searchFiles(dirName As String, fileList As Collection)
  Dim CurrentFILE As String, currentDir As String, currentFileName As String
  Dim IdOggetto As Long
  Dim dirList As New Collection

  dirList.Add dirName & "\"
  While dirList.Count
    currentDir = dirList.item(1)
    dirList.Remove 1
    CurrentFILE = Dir(currentDir, vbDirectory)
    While Len(CurrentFILE)
      If CurrentFILE <> "." And CurrentFILE <> ".." And CurrentFILE <> "Out" Then
        If GetAttr(currentDir & CurrentFILE) = vbDirectory Then  'add the subdirectory
          dirList.Add currentDir & CurrentFILE & "\"
        Else
          fileList.Add currentDir & CurrentFILE
        End If
      End If
      Debug.Print CurrentFILE
      CurrentFILE = Dir
    Wend
  Wend
End Sub

' Mauro 29/08/2007 : Creo un filettino per ogni istruzione che trovo all'interno delle routine generate
Public Sub SplitRoutine(file As String, PathFile As String)
  Dim inFile As Integer, outFile As Integer
  Dim Line As String, oldLine As String
  Dim Testo As String
  Dim newFile As String

  On Error GoTo ErrorHandler
    
  inFile = FreeFile
  
  Open file For Input As inFile
  Line Input #inFile, Line
  Do Until EOF(inFile)
    'If Left(UCase(Line), 22) = "      *  CALL  :      " Then
    If Mid(UCase(Line), 7, 16) = "*  CALL  :      " Then
      If Len(newFile) Then
        outFile = FreeFile
        Open PathFile & "\Instructions\" & newFile For Output As outFile
        Print #outFile, Testo
        Close #outFile
      End If
      newFile = Trim(Mid(Line, 23, 10))
      Testo = oldLine
    Else
      If Len(newFile) Then
        Testo = Testo & vbCrLf & oldLine
      End If
    End If
    oldLine = Line
    Line Input #inFile, Line
  Loop
  If Len(newFile) Then
    Testo = Testo & vbCrLf & oldLine & vbCrLf & Line
    outFile = FreeFile
    Open PathFile & "\Instructions\" & newFile For Output As outFile
    Print #outFile, Testo
    Close #outFile
  End If
  Close #inFile
  Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir PathFile & "\Instructions\"
    Resume
  Else
    MsgBox "File: " & file & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

' Mauro 21/02/2008 : Creo un filettino per ogni istruzione che trovo all'interno delle routine generate
Public Sub SplitRoutine_PLI(file As String, PathFile As String)
  Dim inFile As Integer, outFile As Integer
  Dim Line As String, oldLine As String
  Dim Testo As String
  Dim newFile As String

  On Error GoTo ErrorHandler

  inFile = FreeFile

  Open file For Input As inFile
  Line Input #inFile, Line
  Do Until EOF(inFile)
    If Left(Trim(UCase(Line)), 16) = "*  CALL  :      " Then
      If Len(newFile) Then
        outFile = FreeFile
        Open PathFile & "\Instructions\" & newFile For Output As outFile
        Print #outFile, Testo
        Close #outFile
      End If
      newFile = Trim(Mid(Trim(Line), 17, 10))
      Testo = oldLine
    Else
      If Len(newFile) Then
        If Left(Line, 69) = " /*----------------------------------------------------------------*/" Then
          Line = ""
          Exit Do
        End If
        Testo = Testo & vbCrLf & oldLine
      End If
    End If
    oldLine = Line
    Line Input #inFile, Line
  Loop
  
  If Len(newFile) Then
    ' in PLI l'ultima riga � sempre la end della routine -> non la devo considerare
    Testo = Testo & vbCrLf & oldLine ' & vbCrLf & Line
    outFile = FreeFile
    Open PathFile & "\Instructions\" & newFile For Output As outFile
    Print #outFile, Testo
    Close #outFile
  End If
  Close #inFile
  
  Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir PathFile & "\Instructions\"
    Resume
  Else
    MsgBox "File: " & file & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

' Mauro 29/08/2007 : Ricerco le istruzioni e le inserisco nella routine generata
Private Sub cmdImportInstr_Click()
  ' Funziona sia per CBL che per PLI
  If optCobol Then
    language = "CBL"
  ElseIf OptPLI Then
    language = "PLI"
  End If
    
  If Len(Trim(txtPath_INSTR)) > 0 And Len(Trim(txtPath_ROUT)) > 0 Then
    Screen.MousePointer = vbHourglass
    arrInstrImp = "ROUTINE        INSTRUCTIONS   STATUS" & vbCrLf
    importInstructions txtPath_INSTR, txtPath_ROUT
    Screen.MousePointer = vbNormal
    m_fun.Show_ShowText arrInstrImp, ""
  Else
    MsgBox "Insert correct value for manage directories.", vbInformation, "i-4.Migration"
  End If
End Sub

Public Sub importInstructions(pathInstructions As String, pathRoutines As String)
  Dim i As Long
  Dim routList As Collection, instrList As Collection
  Dim instrRoutList As Collection
  Dim dirFile As String
  Dim rsInstr As Recordset
  Dim nomeRout As String, pathRout As String
  Dim isTrovato As Boolean

  Set routList = New Collection
  Set instrList = New Collection
  
  searchFiles pathRoutines, routList
  searchFiles pathInstructions, instrList

  For i = 1 To routList.Count
    If routList.item(i) <> "." And routList.item(i) <> ".." Then
      ReDim instrExist(0)
      dirFile = Left(routList.item(i), InStrRev(routList.item(i), "\") - 1)
      If language = "CBL" Then
        impInstr routList.item(i), dirFile, instrList
      ElseIf language = "PLI" Then
        impInstr_PLI routList.item(i), dirFile, instrList
      End If
      ' Cerco sulla tabella le istruzioni associate alla routine interessata
      nomeRout = Right(routList.item(i), Len(routList.item(i)) - InStrRev(routList.item(i), "\"))
      nomeRout = Left(nomeRout, InStr(nomeRout & ".", ".") - 1)
      
      ' Inserimento di nuove istruzioni
      ' Controllo esclusivo per Sungard
      If chkInsert And nomeRout <> "RIFUNDGU" And nomeRout <> "RIFUNDGN" Then
        Set instrRoutList = New Collection
        Set rsInstr = m_fun.Open_Recordset("Select codifica from mgdli_decodificaistr Where nomerout like '%" & nomeRout & "%'")
        If Not rsInstr.EOF Then
          Do Until rsInstr.EOF
            ' Controllo se l'istruzione � gi� presente nella routine
            isTrovato = False
            For j = 1 To UBound(instrExist)
              If instrExist(j) = rsInstr!Codifica Then
                isTrovato = True
                Exit For
              End If
            Next j
            ' Se non c'�, faccio un altro ciclo per rintracciarla tra i file-Istruzione
            If Not isTrovato Then
              For j = 1 To instrList.Count
                If Right(instrList.item(j), Len(instrList.item(j)) - InStrRev(instrList.item(j), "\")) = rsInstr!Codifica Then
                  instrRoutList.Add instrList.item(j)
                  Exit For
                End If
              Next j
              ' Se non c'� tra i file, la segnalo nel report finale
              If j = instrList.Count + 1 Then
                arrInstrImp = arrInstrImp & nomeRout & Space(15 - Len(nomeRout))
                arrInstrImp = arrInstrImp & rsInstr!Codifica & Space(15 - Len(rsInstr!Codifica))
                arrInstrImp = arrInstrImp & "MISSING" & vbCrLf
              End If
            End If
            rsInstr.MoveNext
          Loop
          ' Se ho trovato delle istruzioni mancanti nella routine, le aggiungo
          If instrRoutList.Count Then
            dirFile = dirFile & "\Out"
            pathRout = dirFile & "\" & Right(routList.item(i), Len(routList.item(i)) - InStrRev(routList.item(i), "\"))
            If language = "CBL" Then
              ImportInstrinRoutine pathRout, dirFile, instrRoutList
            ElseIf language = "PLI" Then
              ImportInstrinRoutine_PLI pathRout, dirFile, instrRoutList
            End If
          End If
        End If
        rsInstr.Close
      End If
      
      ' Cancellazione di istruzioni
      If chkDelInstr And nomeRout <> "RIFUNDGU" And nomeRout <> "RIFUNDGN" Then
        Set instrRoutList = New Collection
        Set rsInstr = m_fun.Open_Recordset("Select codifica from mgdli_decodificaistr Where nomerout like '%" & nomeRout & "%'")
        If Not rsInstr.EOF Then

          For j = 1 To UBound(instrExist)
            ' Controllo se l'istruzione � presente nel repository
            isTrovato = False
            rsInstr.MoveFirst
            Do Until rsInstr.EOF
              If instrExist(j) = rsInstr!Codifica Then
                isTrovato = True
                Exit Do
              End If
              rsInstr.MoveNext
            Loop

            ' Se non c'�, lo butto in lista per cancellarlo dalla routine
            If Not isTrovato Then
              If Mid(instrExist(j), 5, 2) = "99" Then
                isTrovato = True
              End If
              If Not isTrovato Then
                instrRoutList.Add instrExist(j)
              End If
            End If
          Next j

          ' Se ho trovato delle istruzioni mancanti nella routine, le aggiungo
          If instrRoutList.Count Then
            dirFile = dirFile & "\Out"
            pathRout = dirFile & "\" & Right(routList.item(i), Len(routList.item(i)) - InStrRev(routList.item(i), "\"))
            If language = "CBL" Then
              DeleteInstrinRoutine pathRout, dirFile, instrRoutList
            ElseIf language = "PLI" Then
              DeleteInstrinRoutine_PLI pathRout, dirFile, instrRoutList
            End If
          End If
        End If
        rsInstr.Close
      End If
    End If
  Next i
End Sub

Public Sub impInstr(Routine As String, dirRout As String, instrList As Collection)
  Dim inFile As Integer, outFile As Integer, instrFile As Integer
  Dim Line As String, lineInstr As String
  Dim i As Integer
  Dim Instruction As String
  Dim isTrovato As Boolean
  Dim modPrev As Boolean

  On Error GoTo ErrorHandler
    
  inFile = FreeFile
  Open Routine For Input As inFile
  outFile = FreeFile
  Open dirRout & "\out\" & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) For Output As outFile
  
  Line Input #inFile, Line
  Do Until EOF(inFile)
    'If Left(UCase(Line), 22) = "      *  CALL  :      " Then
    If Mid(UCase(Line), 7, 16) = "*  CALL  :      " Then
      Instruction = Trim(Mid(Line, 23))
      ReDim Preserve instrExist(UBound(instrExist) + 1)
      instrExist(UBound(instrExist)) = Instruction
      ' Controllo se esiste un file con il nome dell'istruzione
      isTrovato = False
      For i = 1 To instrList.Count
        If Right(instrList.item(i), Len(instrList.item(i)) - InStrRev(instrList.item(i), "\")) = Instruction Then
          isTrovato = True
          Exit For
        End If
      Next i
      arrInstrImp = arrInstrImp & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) & Space(15 - Len(Right(Routine, Len(Routine) - InStrRev(Routine, "\"))))
      arrInstrImp = arrInstrImp & Instruction & Space(15 - Len(Instruction))
      If isTrovato Then
        arrInstrImp = arrInstrImp & "MODIFIED" & vbCrLf
        instrFile = FreeFile
        Open instrList.item(i) For Input As instrFile
        If Not modPrev Then
          Line Input #instrFile, lineInstr
          'Line Input #instrFile, lineInstr
        End If
        Line Input #instrFile, lineInstr
        Do Until EOF(instrFile)
          Print #outFile, lineInstr
          Line Input #instrFile, lineInstr
        Loop
        Print #outFile, lineInstr
        Close #instrFile
        modPrev = True
      Else
        arrInstrImp = arrInstrImp & "ORIGINAL" & vbCrLf
        'stefano: rimette quanto perduto
        If modPrev Then
          Print #outFile, ""
          Print #outFile, "      *------------------------------------------------------------*"
        End If
        Print #outFile, Line
        modPrev = False
      End If
    Else
    'stefano
      If Not modPrev Then
        Print #outFile, Line
      End If
    End If
    Line Input #inFile, Line
  Loop
  'stefano
  If Not modPrev Then
    Print #outFile, Line
  End If
  
  Close #inFile
  Close #outFile
  Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir dirRout & "\Out\"
    Resume
  Else
    MsgBox "Routine: " & Routine & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

Public Sub impInstr_PLI(Routine As String, dirRout As String, instrList As Collection)
  Dim inFile As Integer, outFile As Integer, instrFile As Integer
  Dim Line As String, lineInstr As String
  Dim i As Integer
  Dim Instruction As String
  Dim isTrovato As Boolean
  Dim modPrev As Boolean

  On Error GoTo ErrorHandler
    
  inFile = FreeFile
  Open Routine For Input As inFile
  outFile = FreeFile
  Open dirRout & "\out\" & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) For Output As outFile
  
  Line Input #inFile, Line
  Do Until EOF(inFile)
    'If Mid(UCase(Line), 7, 16) = "*  CALL  :      " Then
    If Left(Trim(UCase(Line)), 16) = "*  CALL  :      " Then
      'Instruction = Trim(Mid(Line, 23))
      Instruction = Mid(Trim(Line), 17)
      ReDim Preserve instrExist(UBound(instrExist) + 1)
      instrExist(UBound(instrExist)) = Instruction
      ' Controllo se esiste un file con il nome dell'istruzione
      isTrovato = False
      For i = 1 To instrList.Count
        If Right(instrList.item(i), Len(instrList.item(i)) - InStrRev(instrList.item(i), "\")) = Instruction Then
          isTrovato = True
          Exit For
        End If
      Next i
      arrInstrImp = arrInstrImp & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) & Space(15 - Len(Right(Routine, Len(Routine) - InStrRev(Routine, "\"))))
      arrInstrImp = arrInstrImp & Instruction & Space(15 - Len(Instruction))
      If isTrovato Then
        arrInstrImp = arrInstrImp & "MODIFIED" & vbCrLf
        instrFile = FreeFile
        Open instrList.item(i) For Input As instrFile
        If Not modPrev Then
          Line Input #instrFile, lineInstr
          'Line Input #instrFile, lineInstr
        End If
        Line Input #instrFile, lineInstr
        Do Until EOF(instrFile)
          Print #outFile, lineInstr
          Line Input #instrFile, lineInstr
        Loop
        Print #outFile, lineInstr
        Close #instrFile
        modPrev = True
      Else
        arrInstrImp = arrInstrImp & "ORIGINAL" & vbCrLf
        'stefano: rimette quanto perduto
        If modPrev Then
          Print #outFile, ""
          Print #outFile, "    /*------------------------------------------------------------*"
        End If
        Print #outFile, Line
        modPrev = False
      End If
    Else
    'stefano
      If Not modPrev Then
        Print #outFile, Line
      End If
    End If
    Line Input #inFile, Line
  Loop
  'stefano
  If Not modPrev Then
    Print #outFile, Line
  End If
  
  Close #inFile
  Close #outFile
Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir dirRout & "\Out\"
    Resume
  Else
    MsgBox "Routine: " & Routine & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

' Mauro 29/08/2007 : Ruotine che, presa una routine generata e un elenco di istruzioni
'                    inserisce le istruzioni nella routine
Public Sub ImportInstrinRoutine(Routine As String, dirRout As String, instrList As Collection)
  Dim inFile As Integer, outFile As Integer, instrFile As Integer
  Dim Line As String, lineInstr As String
  Dim i As Integer
  Dim Instruction As String
  Dim isTrovato As Boolean
  Dim routInstr As String
  Dim Riga As String

  On Error GoTo ErrorHandler
    
  inFile = FreeFile
  Open Routine For Input As inFile
  outFile = FreeFile
  Open dirRout & "\out\" & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) For Output As outFile
  
  Do Until EOF(inFile)
    Line Input #inFile, Line
    Print #outFile, Line
  
    ' Importazione COMMENTO/INTESTAZIONE
    ' Solo per COBOL
    If Mid(UCase(Line), 7, 10) = "**  #CODE#" Then
      For i = 1 To instrList.Count
        isTrovato = False
        Instruction = Right(instrList.item(i), Len(instrList.item(i)) - InStrRev(instrList.item(i), "\"))
        Print #outFile, ""
        instrFile = FreeFile
        Open instrList.item(i) For Input As instrFile
        Line Input #instrFile, lineInstr
        Do Until EOF(instrFile)
          If isTrovato Then
            Riga = "      *------------------------------------------------------------*"
            If lineInstr = Riga Then
              Exit Do
            End If
            Print #outFile, lineInstr
          End If
          'If Left(UCase(lineInstr), 22) = "      *  Coding:      " Then
          If Mid(UCase(lineInstr), 7, 16) = "*  CODING:      " Then
            routInstr = Mid(lineInstr, 23)
            Print #outFile, "      *  " & Instruction & " - " & routInstr
            isTrovato = True
          End If
          Line Input #instrFile, lineInstr
        Loop
        Close #instrFile
      Next i
    End If
    
    ' Importazione IF ISTRUZIONE
    If Mid(UCase(Line), 7, 10) = "*    #OPE#" Then
      For i = 1 To instrList.Count
        Instruction = Right(instrList.item(i), Len(instrList.item(i)) - InStrRev(instrList.item(i), "\"))
        Print #outFile, "           IF LNKDB-OPERATION = '" & Instruction & "' "
        Print #outFile, "              PERFORM 9999-ROUT-" & Replace(Instruction, ".", "")
        Print #outFile, "              PERFORM 2000-MAIN-001"
        Print #outFile, "              PERFORM 3000-MAIN-END"
        Print #outFile, "           END-IF"
      Next i
    End If
  Loop
  
  ' Importazione PERFORM ISTRUZIONE : Butto tutto in fondo alla routine
  For i = 1 To instrList.Count
    Instruction = Right(instrList.item(i), Len(instrList.item(i)) - InStrRev(instrList.item(i), "\"))
    arrInstrImp = arrInstrImp & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) & Space(15 - Len(Right(Routine, Len(Routine) - InStrRev(Routine, "\"))))
    arrInstrImp = arrInstrImp & Instruction & Space(15 - Len(Instruction))
    arrInstrImp = arrInstrImp & "IMPORTED" & vbCrLf
    instrFile = FreeFile
    Open instrList.item(i) For Input As instrFile
    Do Until EOF(instrFile)
      Line Input #instrFile, lineInstr
      Print #outFile, lineInstr
    Loop
    Close #instrFile
  Next i
  
  Close #inFile
  Close #outFile
Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir dirRout & "\Out\"
    Resume
  Else
    MsgBox "Routine: " & Routine & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

' Mauro 21/02/2008 : Ruotine che, presa una routine generata e un elenco di istruzioni
'                    inserisce le istruzioni nella routine
Public Sub ImportInstrinRoutine_PLI(Routine As String, dirRout As String, instrList As Collection)
  Dim inFile As Integer, outFile As Integer, instrFile As Integer
  Dim Line As String, lineInstr As String
  Dim i As Integer
  Dim Instruction As String
  Dim isTrovato As Boolean
  Dim routInstr As String
  Dim Riga As String

  On Error GoTo ErrorHandler
    
  inFile = FreeFile
  Open Routine For Input As inFile
  outFile = FreeFile
  Open dirRout & "\out\" & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) For Output As outFile
  
  Do Until EOF(inFile)
    Line Input #inFile, Line
    Print #outFile, Line
  
    ' Importazione IF ISTRUZIONE
    If Left(UCase(Line), 13) = "  /*    #OPE#" Then
      For i = 1 To instrList.Count
        Instruction = Right(instrList.item(i), Len(instrList.item(i)) - InStrRev(instrList.item(i), "\"))
        Print #outFile, "     IF LNKDB_OPERATION = '" & Instruction & "' THEN DO;"
        Print #outFile, "        CALL ROUT_" & Replace(Instruction, ".", "") & ";"
        Print #outFile, "        CALL PREPARE_EXIT;"
        Print #outFile, "        RETURN;"
        Print #outFile, "     END;"
      Next i
    End If
  Loop
  ' Importazione PERFORM ISTRUZIONE : Butto tutto in fondo alla routine
  For i = 1 To instrList.Count
    Instruction = Right(instrList.item(i), Len(instrList.item(i)) - InStrRev(instrList.item(i), "\"))
    arrInstrImp = arrInstrImp & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) & Space(15 - Len(Right(Routine, Len(Routine) - InStrRev(Routine, "\"))))
    arrInstrImp = arrInstrImp & Instruction & Space(15 - Len(Instruction))
    arrInstrImp = arrInstrImp & "IMPORTED" & vbCrLf
    instrFile = FreeFile
    Open instrList.item(i) For Input As instrFile
    Do Until EOF(instrFile)
      Line Input #instrFile, lineInstr
      Print #outFile, lineInstr
    Loop
    Close #instrFile
  Next i
  
  Close #inFile
  Close #outFile
Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir dirRout & "\Out\"
    Resume
  Else
    MsgBox "Routine: " & Routine & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

' Mauro 03/09/2007 : Ruotine che, presa una routine generata e un elenco di istruzioni
'                    cancella le istruzioni dalla routine
Public Sub DeleteInstrinRoutine(Routine As String, dirRout As String, instrList As Collection)
  Dim inFile As Integer, outFile As Integer, instrFile As Integer
  Dim Line As String, lineInstr As String
  Dim i As Integer
  Dim Instruction As String, oldLine As String
  Dim isTrovato As Boolean
  Dim routInstr As String

  On Error GoTo ErrorHandler
  
  inFile = FreeFile
  Open Routine For Input As inFile
  outFile = FreeFile
  Open dirRout & "\out\" & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) For Output As outFile
    
  Line Input #inFile, Line
  Do Until EOF(inFile)
    
    Select Case True
      
      'Case Left(Line, 9) = "      *  " And Mid(Line, 20, 3) = " - "
      Case Mid(Line, 7, 3) = "*  " And Mid(Line, 20, 3) = " - "
        For i = 1 To instrList.Count
          isTrovato = False
          If Mid(Line, 10, InStr(10, Line, " ") - 10) = instrList.item(i) Then
            isTrovato = True
            Exit For
          End If
        Next i
        If isTrovato Then
          Do Until EOF(inFile)
            Line Input #inFile, Line
            'If Line = "      **" Or (Left(Line, 9) = "      *  " And Mid(Line, 20, 3) = " - ") Then
            If Mid(Line, 7, 2) = "**" Or (Mid(Line, 7, 3) = "*  " And Mid(Line, 20, 3) = " - ") Then
              Exit Do
            End If
          Loop
        Else
          Print #outFile, Line
          Line Input #inFile, Line
        End If
        
      'Case Left(UCase(Line), 33) = "           IF LNKDB-OPERATION = '"
      Case Mid(UCase(Line), 7, 27) = "     IF LNKDB-OPERATION = '"
        For i = 1 To instrList.Count
          isTrovato = False
          If Mid(Line, 34, InStr(10, Line, "'") - 23) = instrList.item(i) Then
            isTrovato = True
            Exit For
          End If
        Next i
        If isTrovato Then
          Do Until EOF(inFile)
            Line Input #inFile, Line
            If Mid(UCase(Line), 7, 36) = "     MOVE 'NOOP' TO LNKDB-CODE-ERROR" Or _
               Mid(UCase(Line), 7, 27) = "     IF LNKDB-OPERATION = '" Then
              Exit Do
            End If
          Loop
        Else
          Print #outFile, Line
          Line Input #inFile, Line
        End If
      
      'Case Left(UCase(Line), 22) = "      *  CALL  :      "
      Case Mid(UCase(Line), 7, 16) = "*  CALL  :      "
        For i = 1 To instrList.Count
          isTrovato = False
          If Trim(Mid(Line, 20)) = instrList.item(i) Then
            isTrovato = True
            Exit For
          End If
        Next i
        If isTrovato Then
          arrInstrImp = arrInstrImp & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) & Space(15 - Len(Right(Routine, Len(Routine) - InStrRev(Routine, "\"))))
          arrInstrImp = arrInstrImp & instrList.item(i) & Space(15 - Len(instrList.item(i)))
          arrInstrImp = arrInstrImp & "DELETED" & vbCrLf
          Do Until EOF(inFile)
            Line Input #inFile, Line
            'If oldLine = "      *------------------------------------------------------------*" And _
            '  Left(UCase(Line), 22) = "      *  CALL  :      " Then
            If Mid(oldLine, 7) = "*------------------------------------------------------------*" And _
              Mid(UCase(Line), 7, 16) = "*  CALL  :      " Then
              Exit Do
            Else
              oldLine = ""
            End If
            'If Line = "      *------------------------------------------------------------*" Then
            If Mid(Line, 7) = "*------------------------------------------------------------*" Then
              oldLine = Line
            End If
          Loop
        Else
          If Len(oldLine) Then
            Print #outFile, oldLine
            oldLine = ""
          End If
          Print #outFile, Line
          Line Input #inFile, Line
        End If
    
      Case Else
        If Len(oldLine) Then
          Print #outFile, oldLine
          oldLine = ""
        End If
        'If Line = "      *------------------------------------------------------------*" Then
        If Mid(Line, 7) = "*------------------------------------------------------------*" Then
          oldLine = Line
        Else
          Print #outFile, Line
        End If
        Line Input #inFile, Line

    End Select
  Loop
  Print #outFile, Line
  
  Close #inFile
  Close #outFile
Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir dirRout & "\Out\"
    Resume
  Else
    MsgBox "Routine: " & Routine & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

' Mauro 21/02/2008 : Ruotine che, presa una routine generata e un elenco di istruzioni
'                    cancella le istruzioni dalla routine
Public Sub DeleteInstrinRoutine_PLI(Routine As String, dirRout As String, instrList As Collection)
  Dim inFile As Integer, outFile As Integer, instrFile As Integer
  Dim Line As String, lineInstr As String
  Dim i As Integer
  Dim Instruction As String, oldLine As String
  Dim isTrovato As Boolean
  Dim routInstr As String

  On Error GoTo ErrorHandler
  
  inFile = FreeFile
  Open Routine For Input As inFile
  outFile = FreeFile
  Open dirRout & "\out\" & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) For Output As outFile
    
  Line Input #inFile, Line
  Do Until EOF(inFile)

    Select Case True
      Case Mid(UCase(Line), 7, 27) = "     IF LNKDB_OPERATION = '"
        For i = 1 To instrList.Count
          isTrovato = False
          If Mid(Line, 34, InStr(10, Line, "'") - 23) = instrList.item(i) Then
            isTrovato = True
            Exit For
          End If
        Next i
        If isTrovato Then
          Do Until EOF(inFile)
            Line Input #inFile, Line
            If Left(UCase(Line), 29) = "  LNKDB_CODE_ERROR = 'NOOP' ;" Or _
               Mid(UCase(Line), 7, 27) = "     IF LNKDB_OPERATION = '" Then
              Exit Do
            End If
          Loop
        Else
          Print #outFile, Line
          Line Input #inFile, Line
        End If

      Case Mid(UCase(Line), 7, 16) = "*  CALL  :      "
        For i = 1 To instrList.Count
          isTrovato = False
          If Trim(Mid(Line, 20)) = instrList.item(i) Then
            isTrovato = True
            Exit For
          End If
        Next i
        If isTrovato Then
          arrInstrImp = arrInstrImp & Right(Routine, Len(Routine) - InStrRev(Routine, "\")) & Space(15 - Len(Right(Routine, Len(Routine) - InStrRev(Routine, "\"))))
          arrInstrImp = arrInstrImp & instrList.item(i) & Space(15 - Len(instrList.item(i)))
          arrInstrImp = arrInstrImp & "DELETED" & vbCrLf
          Do Until EOF(inFile)
            Line Input #inFile, Line
            If (Left(oldLine, 6) = "    /*------------------------------------------------------------*" And _
               Mid(UCase(Line), 7, 16) = "*  CALL  :      ") Or _
               (Left(oldLine, 69) = " /*----------------------------------------------------------------*/" And _
               UCase(Left(Line, 69)) = " /*             R E T U R N   T O   C A L L E R                    */") Then
              Exit Do
            Else
              oldLine = ""
            End If
            If Left(Line, 67) = "    /*------------------------------------------------------------*" Or _
               Left(Line, 69) = " /*----------------------------------------------------------------*/" Then
              oldLine = Line
            End If
          Loop
        Else
          If Len(oldLine) Then
            Print #outFile, oldLine
            oldLine = ""
          End If
          Print #outFile, Line
          Line Input #inFile, Line
        End If

      Case Else
        If Len(oldLine) Then
          Print #outFile, oldLine
          oldLine = ""
        End If
        If Left(Line, 67) = "    /*------------------------------------------------------------*" Or _
           Left(Line, 69) = " /*----------------------------------------------------------------*/" Then
          oldLine = Line
        Else
          Print #outFile, Line
        End If
        Line Input #inFile, Line
    End Select
  Loop
  Print #outFile, Line
  
  Close #inFile
  Close #outFile
  Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir dirRout & "\Out\"
    Resume
  Else
    MsgBox "Routine: " & Routine & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

Public Sub Split_Small_Routine(pathRout As String)
  Dim i As Long, a As Long
  Dim fileList As Collection
  Dim dirFile As String
  Dim FileName As String
  Dim isTrovato As Boolean
  Dim dbName() As TypeProgr

  Set fileList = New Collection
  
  searchFiles pathRout, fileList
    
  ReDim dbName(0)
  For i = 1 To fileList.Count
    If fileList.item(i) <> "." And fileList.item(i) <> ".." Then
      dirFile = Left(fileList.item(i), InStrRev(fileList.item(i), "\") - 1)
      FileName = Right(fileList.item(i), Len(fileList.item(i)) - InStrRev("\" & fileList.item(i), "\") + 1)
      isTrovato = False
      For a = 1 To UBound(dbName)
        If dbName(a).FileName = Left(FileName, 6) Then
          isTrovato = True
          Exit For
        End If
      Next a
      If Not isTrovato Then
        a = UBound(dbName) + 1
        ReDim Preserve dbName(a)
        dbName(a).FileName = Left(FileName, 6)
        dbName(a).NumProgressivo = 0
      End If
      Progressivo = dbName(a).NumProgressivo
      If language = "CBL" Then
        SplitSmallRoutine fileList.item(i), dirFile
      ElseIf language = "PLI" Then
        If Dli2Rdbms.drNomeDB = "Prj_BBV.mty" Then
          Progressivo = 0
        End If
        SplitSmallRoutine_PLI fileList.item(i), dirFile
      End If
      dbName(a).NumProgressivo = Progressivo
    End If
  Next i
End Sub

Public Sub SplitSmallRoutine(file As String, PathFile As String)
  Dim inFile As Integer, outFile As Integer
  Dim Line As String, oldLine As String
  Dim i As Long, a As Long
  Dim Testo As String
  Dim newFile As String, FileName As String
  Dim Intestazione As String, I_Parte As String, II_Parte As String
  Dim instrList() As Instructions
  Dim isTrovato As Boolean
  Dim Commenti As String, IfPerform As String, Istruzioni As String
  Dim estensione As String
  
  ReDim instrList(0)
  On Error GoTo ErrorHandler
    
  inFile = FreeFile
  
  Open file For Input As inFile
  Line Input #inFile, Line
  
  ' Carico l'intestazione da inserire poi in tutte le nuove routine
  Do Until EOF(inFile)
    Intestazione = Intestazione & Line & vbCrLf
    'If Left(UCase(Line), 16) = "      **  #CODE#" Then
    If Mid(UCase(Line), 7, 10) = "**  #CODE#" Then
      Exit Do
    End If
    Line Input #inFile, Line
  Loop
  If Not EOF(inFile) Then
    Line Input #inFile, Line
  End If
  
  ' Carico tutti i commenti delle istruzioni
  Do Until EOF(inFile)
    If Len(Line) Then
      'If Left(Line, 9) = "      *  " And Mid(Line, 20, 3) = " - " Then
      If Mid(Line, 7, 3) = "*  " And Mid(Line, 20, 3) = " - " Then
        ReDim Preserve instrList(UBound(instrList) + 1)
        instrList(UBound(instrList)).nome = Mid(Line, 10, 10)
        ' In testa al commento c'� sempre una riga vuota
        instrList(UBound(instrList)).Commento = vbCrLf & Line & vbCrLf
      Else
        'If Left(Line, 8) = "      **" Then
        If Mid(Line, 7, 2) = "**" Then
          Exit Do
        Else
          instrList(UBound(instrList)).Commento = instrList(UBound(instrList)).Commento & Line & vbCrLf
        End If
      End If
    End If
    Line Input #inFile, Line
  Loop
  
  ' Carico la parte compresa tra i commenti e le IF, da inserire poi in tutte le nuove routine
  Do Until EOF(inFile)
    I_Parte = I_Parte & Line & vbCrLf
    'If Left(UCase(Line), 16) = "      *    #OPE#" Then
    If Mid(UCase(Line), 7, 10) = "*    #OPE#" Then
      Exit Do
    End If
    If Mid(UCase(Line), 8, 25) = "PROCEDURE DIVISION USING " Then
      If Mid(UCase(Trim(Line)), 26, 12) = "DFHCOMMAREA." Then
        isRoutCics = True
        varUsing = ""
      Else
        isRoutCics = False
        varUsing = Mid(Trim(Line), 26)
        If Right(varUsing, 1) = "." Then
          varUsing = Left(varUsing, Len(varUsing) - 1)
        End If
      End If
    End If
    Line Input #inFile, Line
  Loop
  If Not EOF(inFile) Then
    Line Input #inFile, Line
  End If
  
  ' Carico tutte le IF per le chiamate alle istruzioni
  Do Until EOF(inFile)
    'If Left(UCase(Line), 32) = "           IF LNKDB-OPERATION = " Then
    If Mid(UCase(Line), 7, 26) = "     IF LNKDB-OPERATION = " Then
      ' Ciclo per rintracciare l'istruzione nella struttura d'appoggio
      isTrovato = False
      For i = 1 To UBound(instrList)
        If instrList(i).nome = Mid(Line, 34, 10) Then
          isTrovato = True
          Exit For
        End If
      Next i
      If Not isTrovato Then
        i = UBound(instrList) + 1
        ReDim Preserve instrList(i)
        instrList(i).nome = Mid(Line, 34, 10)
        instrList(i).Commento = ""
      End If
      instrList(i).If_Perform = Line & vbCrLf
    Else
      'If Left(UCase(Line), 43) = "           MOVE 'NOOP' TO LNKDB-CODE-ERROR." Then
      If Mid(UCase(Line), 7, 37) = "     MOVE 'NOOP' TO LNKDB-CODE-ERROR." Then
        Exit Do
      Else
        instrList(i).If_Perform = instrList(i).If_Perform & Line & vbCrLf
      End If
    End If
    Line Input #inFile, Line
  Loop
  
  ' Carico la parte compresa tra le IF  e le istruzioni, da inserire poi in tutte le nuove routine
  Do Until EOF(inFile)
    'If Left(UCase(Line), 22) = "      *  CALL  :      " Then
    If Mid(UCase(Line), 7, 16) = "*  CALL  :      " Then
      ' Trovata la prima istruzione, devo anche cancellare la riga di trattini che la precede
      Exit Do
    End If
    If Mid(Line, 7) = "*------------------------------------------------------------*" Then
      oldLine = Line
    Else
      If Len(oldLine) Then
        II_Parte = II_Parte & oldLine & vbCrLf
        oldLine = ""
      End If
      II_Parte = II_Parte & Line & vbCrLf
    End If
    Line Input #inFile, Line
  Loop
  
  ' Carico tutte le istruzioni
  Do Until EOF(inFile)
    'If Left(UCase(Line), 22) = "      *  CALL  :      " Then
    If Mid(UCase(Line), 7, 16) = "*  CALL  :      " Then
      ' Ciclo per rintracciare l'istruzione nella struttura d'appoggio
      isTrovato = False
      For i = 1 To UBound(instrList)
        If instrList(i).nome = Mid(Line, 23, 10) Then
          isTrovato = True
          Exit For
        End If
      Next i
      If Not isTrovato Then
        i = UBound(instrList) + 1
        ReDim Preserve instrList(i)
        instrList(i).nome = Mid(Line, 23, 10)
        instrList(i).Commento = ""
        instrList(i).If_Perform = ""
      End If
    End If
    If Mid(Line, 7) = "*------------------------------------------------------------*" Then
      oldLine = Line
    Else
      If Len(oldLine) Then
        instrList(i).Istruzione = instrList(i).Istruzione & oldLine & vbCrLf
        oldLine = ""
      End If
      instrList(i).Istruzione = instrList(i).Istruzione & Line & vbCrLf
    End If
    Line Input #inFile, Line
  Loop
  instrList(i).Istruzione = instrList(i).Istruzione & Line & vbCrLf
  Close #inFile
  
  ' Creo le sottoroutine
  Commenti = ""
  IfPerform = ""
  Istruzioni = ""
  FileName = Right(file, Len(file) - InStrRev("\" & file, "\") + 1)
  If InStr(FileName, ".") Then
    estensione = Right(FileName, Len(FileName) - InStr(FileName, "."))
  Else
    estensione = ""
  End If
  FileName = Left(FileName, InStr(FileName & ".", ".") - 1)
  
  newFile = Left(FileName, 6) & Format(Progressivo, "00")
  For i = 1 To UBound(instrList)
    If Len(instrList(i).If_Perform) Then
      Commenti = Commenti & instrList(i).Commento
      IfPerform = IfPerform & instrList(i).If_Perform
      Istruzioni = Istruzioni & instrList(i).Istruzione
      instrList(i).Routine = newFile
    End If
    
    If (i Mod NumIstrxRout) = 0 Or i = UBound(instrList) Then
      Testo = Replace(Intestazione, FileName, newFile) & Commenti & Replace(I_Parte, FileName, newFile) & IfPerform & Replace(II_Parte, FileName, newFile) & Istruzioni
      outFile = FreeFile
      Open PathFile & "\Out\" & newFile & "." & estensione For Output As outFile
      Print #outFile, Testo
      Close #outFile
      
      Commenti = ""
      IfPerform = ""
      Istruzioni = ""
      Progressivo = Progressivo + 1
      newFile = Left(FileName, 6) & Format(Progressivo, "00")
    End If
  Next i
  
  Testo = ""
  For i = 1 To UBound(instrList)
    If Len(instrList(i).If_Perform) Then
      If isRoutCics Then
        Testo = Testo & "           IF LNKDB-OPERATION = '" & instrList(i).nome & "' " & vbCrLf
        Testo = Testo & "              MOVE '" & instrList(i).Routine & "' TO WS-CALLED" & vbCrLf
        Testo = Testo & "              CALL WS-CALLED USING DFHEIBLK DFHCOMMAREA" & vbCrLf
        Testo = Testo & "           END-IF" & vbCrLf
      Else
        Testo = Testo & "           IF LNKDB-OPERATION = '" & instrList(i).nome & "' " & vbCrLf
        Testo = Testo & "              CALL '" & instrList(i).Routine & "' USING " & varUsing & vbCrLf
        Testo = Testo & "           END-IF" & vbCrLf
      End If
    End If
  Next i
  If Len(Testo) Then
    Testo = Intestazione & I_Parte & Testo & "           GOBACK."
    ' Creo il driver per richiamare le sottoroutine
    outFile = FreeFile
    Open PathFile & "\Out\" & FileName & "_Temp." & estensione For Output As outFile
    Print #outFile, Testo
    Close #outFile
    
    ' Ripulisco tutte le EXEC SQL INCLUDE
    inFile = FreeFile
    Open PathFile & "\Out\" & FileName & "_Temp." & estensione For Input As inFile
    outFile = FreeFile
    Open PathFile & "\Out\" & FileName & "." & estensione For Output As outFile
    Do Until EOF(inFile)
      Line Input #inFile, Line
      If (Mid(Line, 7, 1) = "*" Or InStr(Line, " INCLUDE ") = 0) And _
         (Mid(Line, 7, 1) = "*" Or InStr(Line, " 4000-INIT-AREAS.") = 0) Then
        Print #outFile, Line
      End If
    Loop
    Close #inFile
    Close #outFile
    Kill PathFile & "\Out\" & FileName & "_Temp." & estensione
  End If
  Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir PathFile & "\Out\"
    Resume
  Else
    MsgBox "File: " & file & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

Public Sub SplitSmallRoutine_PLI(file As String, PathFile As String)
  Dim inFile As Integer, outFile As Integer
  Dim Line As String, oldLine As String
  Dim i As Long, a As Long
  Dim Testo As String
  Dim newFile As String, FileName As String
  Dim Intestazione As String, I_Parte As String
  Dim instrList() As Instructions
  Dim isTrovato As Boolean
  Dim IfPerform As String, Istruzioni As String
  Dim estensione As String
  Dim ExtEntry As String
  
  On Error GoTo ErrorHandler
  
  ReDim instrList(0)
  inFile = FreeFile
  
  Open file For Input As inFile
  Line Input #inFile, Line
  
  ' Carico l'intestazione da inserire poi in tutte le nuove routine
  Do Until EOF(inFile)
    Intestazione = Intestazione & Line & vbCrLf
    If Mid(UCase(Line), 8, 15) = " /*    #OPE# */" Then
      Exit Do
    End If
    Line Input #inFile, Line
  Loop
  If Not EOF(inFile) Then
    Line Input #inFile, Line
  End If
  
  ' Carico tutte le IF per le chiamate alle istruzioni
  Do Until EOF(inFile)
    If Mid(UCase(Line), 7, 26) = "     IF LNKDB_OPERATION = " Then
      ' Ciclo per rintracciare l'istruzione nella struttura d'appoggio
      isTrovato = False
      For i = 1 To UBound(instrList)
        If instrList(i).nome = Mid(Line, 34, 10) Then
          isTrovato = True
          Exit For
        End If
      Next i
      If Not isTrovato Then
        i = UBound(instrList) + 1
        ReDim Preserve instrList(i)
        instrList(i).nome = Mid(Line, 34, 10)
        instrList(i).Commento = ""
      End If
      instrList(i).If_Perform = Line & vbCrLf
    Else
      If UCase(Mid(Line, 1, 29)) = "  LNKDB_CODE_ERROR = 'NOOP' ;" Then
        Exit Do
      Else
        instrList(i).If_Perform = instrList(i).If_Perform & Line & vbCrLf
      End If
    End If
    Line Input #inFile, Line
  Loop
  
  ' Carico la parte compresa tra le IF  e le istruzioni, da inserire poi in tutte le nuove routine
  Do Until EOF(inFile)
    If Mid(UCase(Line), 7, 16) = "*  CALL  :      " Then
      ' Trovata la prima istruzione, devo anche cancellare la riga di trattini che la precede
      Exit Do
    End If
    If Left(Line, 67) = "    /*------------------------------------------------------------*" Then
      oldLine = Line
    Else
      If Len(oldLine) Then
        I_Parte = I_Parte & oldLine & vbCrLf
        oldLine = ""
      End If
      I_Parte = I_Parte & Line & vbCrLf
    End If
    Line Input #inFile, Line
  Loop
  
  ' Carico tutte le istruzioni
  Do Until EOF(inFile)
    If UCase(Left(Line, 69)) = " /*             R E T U R N   T O   C A L L E R                    */" Then
      Exit Do
    End If
    If Mid(UCase(Line), 7, 16) = "*  CALL  :      " Then
      ' Ciclo per rintracciare l'istruzione nella struttura d'appoggio
      isTrovato = False
      For i = 1 To UBound(instrList)
        If instrList(i).nome = Mid(Line, 23, 10) Then
          isTrovato = True
          Exit For
        End If
      Next i
      If Not isTrovato Then
        i = UBound(instrList) + 1
        ReDim Preserve instrList(i)
        instrList(i).nome = Mid(Line, 23, 10)
        instrList(i).Commento = ""
        instrList(i).If_Perform = ""
      End If
    End If
    If Left(Line, 67) = "    /*------------------------------------------------------------*" Or _
       Left(Line, 69) = " /*----------------------------------------------------------------*/" Then
      oldLine = Line
    Else
      If Len(oldLine) Then
        instrList(i).Istruzione = instrList(i).Istruzione & oldLine & vbCrLf
        oldLine = ""
      End If
      instrList(i).Istruzione = instrList(i).Istruzione & Line & vbCrLf
    End If
    Line Input #inFile, Line
  Loop
  'instrList(i).Istruzione = instrList(i).Istruzione & Line & vbCrLf
  Close #inFile
  
  ' Creo le sottoroutine
  IfPerform = ""
  Istruzioni = ""
  ExtEntry = ""
  FileName = Right(file, Len(file) - InStrRev("\" & file, "\") + 1)
  If InStr(FileName, ".") Then
    estensione = Right(FileName, Len(FileName) - InStr(FileName, "."))
  Else
    estensione = ""
  End If
  FileName = Left(FileName, InStr(FileName & ".", ".") - 1)
  
  If Dli2Rdbms.drNomeDB = "Prj_BBV.mty" Then
    newFile = Left(FileName, 4) & Progressivo + 1 & Mid(FileName, 6, 2)
  Else
    newFile = Left(FileName, 6) & Format(Progressivo, "00")
  End If
  For i = 1 To UBound(instrList)
    If Len(instrList(i).If_Perform) Then
      IfPerform = IfPerform & instrList(i).If_Perform
      Istruzioni = Istruzioni & instrList(i).Istruzione
      instrList(i).Routine = newFile
    End If
    
    If (i Mod NumIstrxRout) = 0 Or i = UBound(instrList) Then
      Testo = Replace(Intestazione, FileName, newFile) & IfPerform & Replace(I_Parte, FileName, newFile) & Istruzioni
      ' E' la chiusura del file! Deve esserci su tutti i PLI
      Testo = Testo & "  RETURN;" & vbCrLf & vbCrLf
      Testo = Testo & " /*----------------------------------------------------------------*/" & vbCrLf
      Testo = Testo & " /*             R E T U R N   T O   C A L L E R                    */" & vbCrLf
      Testo = Testo & " /*----------------------------------------------------------------*/" & vbCrLf
      Testo = Testo & " RETURN;" & vbCrLf
      Testo = Testo & " END " & UCase(newFile) & ";"
      
      outFile = FreeFile
      Open PathFile & "\Out\" & newFile & "." & estensione For Output As outFile
      Print #outFile, Testo
      Close #outFile
      ExtEntry = ExtEntry & "  DCL   " & newFile & " EXTERNAL ENTRY;" & vbCrLf
      
      IfPerform = ""
      Istruzioni = ""
      Progressivo = Progressivo + 1
      If Dli2Rdbms.drNomeDB = "Prj_BBV.mty" Then
        newFile = Left(FileName, 4) & Progressivo + 1 & Mid(FileName, 6, 2)
      Else
        newFile = Left(FileName, 6) & Format(Progressivo, "00")
      End If
    End If
  Next i
  
  Testo = ""
  For i = 1 To UBound(instrList)
    If Len(instrList(i).If_Perform) Then
      Testo = Testo & "           IF LNKDB_OPERATION = '" & instrList(i).nome & "' THEN DO;" & vbCrLf
      Testo = Testo & "                    CALL " & instrList(i).Routine & " (LNKDB_AREA);" & vbCrLf
      Testo = Testo & "                    RETURN;" & vbCrLf
      Testo = Testo & "                END;" & vbCrLf
    End If
  Next i
  If Len(Testo) Then
    Testo = Intestazione & Testo & vbCrLf
    Testo = Testo & "  RETURN;" & vbCrLf & vbCrLf
    Testo = Testo & " /*----------------------------------------------------------------*/" & vbCrLf
    Testo = Testo & " /*             R E T U R N   T O   C A L L E R                    */" & vbCrLf
    Testo = Testo & " /*----------------------------------------------------------------*/" & vbCrLf
    Testo = Testo & " RETURN;" & vbCrLf
    Testo = Testo & " END " & UCase(FileName) & ";"
    
    Testo = Testo & vbCrLf & ExtEntry
    ' Creo il driver per richiamare le sottoroutine
    outFile = FreeFile
    Open PathFile & "\Out\" & FileName & "." & estensione For Output As outFile
    Print #outFile, Testo
    Close #outFile
    
''    ' Ripulisco tutte le EXEC SQL INCLUDE
''    inFile = FreeFile
''    Open PathFile & "\Out\" & Filename & "_Temp." & estensione For Input As inFile
''    outFile = FreeFile
''    Open PathFile & "\Out\" & Filename & "." & estensione For Output As outFile
''    Do Until EOF(inFile)
''      Line Input #inFile, Line
''      If (Mid(Line, 7, 1) = "*" Or InStr(Line, " INCLUDE ") = 0) And _
''         (Mid(Line, 7, 1) = "*" Or InStr(Line, " 4000-INIT-AREAS.") = 0) Then
''        Print #outFile, Line
''      End If
''    Loop
''    Close #inFile
''    Close #outFile
''    Kill PathFile & "\Out\" & Filename & "_Temp." & estensione
  End If
  Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir PathFile & "\Out\"
    Resume
  Else
    MsgBox "File: " & file & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

Private Sub CmdTemplate_Click()
  Dim checkRoutine As Boolean
  
  If optCobol Then
    language = "CBL"
  ElseIf OptPLI Then
    language = "PLI"
  End If
  
  If Len(Trim(txtPath_ROUT)) Then
    Screen.MousePointer = vbHourglass
    Split_Instructions_in_Template txtPath_ROUT
    Screen.MousePointer = vbNormal
    MsgBox "Split Complete.", vbOKOnly + vbInformation
  Else
    MsgBox "Insert correct value in Routine Files Directory.", vbInformation, "i-4.Migration"
  End If
End Sub

Public Sub Split_Instructions_in_Template(pathRout As String)
  Dim i As Long
  Dim fileList As Collection
  Dim dirFile As String

  Set fileList = New Collection
  
  searchFiles pathRout, fileList

  For i = 1 To fileList.Count
    If fileList.item(i) <> "." And fileList.item(i) <> ".." Then
      dirFile = Left(fileList.item(i), InStrRev(fileList.item(i), "\") - 1)
      If language = "CBL" Then
        SplitTemplate fileList.item(i), dirFile
      ElseIf language = "PLI" Then
        SplitTemplate_PLI fileList.item(i), dirFile
      End If
    End If
  Next i
End Sub

'Claudio 01/03/2010 : Creo un filettino di template per ogni routine
Public Sub SplitTemplate(file As String, PathFile As String)
  Dim inFile As Integer, outTemplate As Integer
  Dim Line As String
  Dim FileName As String
  Dim isOK As Boolean
  Dim noOpString As String
  
  On Error GoTo ErrorHandler

  inFile = FreeFile
  Open file For Input As inFile
  Line Input #inFile, Line
  FileName = Right(file, Len(file) - InStrRev(file, "\"))
  outTemplate = FreeFile
  Open PathFile & "\Routines\" & FileName For Output As outTemplate

  isOK = True
  Do Until EOF(inFile)
    'If Left(UCase(Line), 22) = "      *  CALL  :      " Then
    If Mid(UCase(Line), 8, 24) = "IDENTIFICATION DIVISION." Then isOK = True
    If Mid(UCase(Line), 7, 10) = "**  #CODE#" Then isOK = False
    If Mid(UCase(Line), 8, 21) = "ENVIRONMENT DIVISION." Then isOK = True
    If Mid(UCase(Line), 7, 10) = "*    #OPE#" Then isOK = False
    noOpString = "MOVE 'NOOP' TO LNKDB-CODE-ERROR"
    If Mid(UCase(Line), 12, Len(noOpString)) = noOpString Then isOK = True
    If Mid(UCase(Line), 7, 16) = "*  CALL  :      " Then isOK = False
    If Mid(UCase(Line), 7, 10) = "**  #CODE#" Then
      Print #outTemplate, Line
    End If
    If Mid(UCase(Line), 7, 10) = "*    #OPE#" Then
      Print #outTemplate, Line
    End If
    If isOK Then
      Print #outTemplate, Line
    End If
    Line Input #inFile, Line
  Loop
  Close #outTemplate
  Close #inFile
  Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir PathFile & "\Routines\"
    Resume
  Else
    MsgBox "File: " & file & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

'Claudio 01/03/2010 : Creo un filettino di template per ogni routine
Public Sub SplitTemplate_PLI(file As String, PathFile As String)
  Dim inFile As Integer, outTemplate As Integer
  Dim Line As String
  Dim FileName As String
  Dim isOK As Boolean
  Dim nomeRoutine As String
  
  On Error GoTo ErrorHandler

  inFile = FreeFile
  Open file For Input As inFile
  Line Input #inFile, Line
  FileName = Right(file, Len(file) - InStrRev(file, "\"))
  outTemplate = FreeFile
  Open PathFile & "\Routines\" & FileName For Output As outTemplate

  isOK = True
  Do Until EOF(inFile)
    If Mid(UCase(Line), 9, 49) = ": PROC (LNKDB_AREA) OPTIONS(REENTRANT,FETCHABLE);" Then
      nomeRoutine = Mid(Line, 2, 7)
      isOK = True
    End If
    If Mid(UCase(Line), 2, 10) = "/*  #CODE#" Then isOK = False
    If InStr(Line, "/*                     RECORDS STRUCTURE") Then isOK = True
    If InStr(Line, "*    #OPE#") Then isOK = False
    If InStr(Line, "LNKDB_CODE_ERROR = 'NOOP'") Then isOK = True
    If InStr(Line, "*  Call  :  ") Then isOK = False
    If InStr(Line, "/*  #CODE#") Then
      Print #outTemplate, Line
    End If
    If InStr(Line, "*    #OPE#") Then
      Print #outTemplate, Line
    End If
    If isOK Then
      Print #outTemplate, Line
    End If
    Line Input #inFile, Line
    If InStr(Line, "END " & nomeRoutine) Then
      Print #outTemplate, Line
    End If
  Loop
  Close #outTemplate
  Close #inFile
  
  Exit Sub
ErrorHandler:
  If err.Number = "76" Then
    MkDir PathFile & "\Routines\"
    Resume
  Else
    MsgBox "File: " & file & " - " & err.Description, vbOKOnly, "i-4.Migration"
  End If
End Sub

Private Sub CmdUpdatePcb_Click()
  Dim rs As Recordset, rsPSB As Recordset
  Dim arrPsb() As String, i As Integer, resp As Integer
  
  resp = MsgBox("This command is going to update Instruction and Psb table with information from Refresh procedure, are you sure ?", vbDefaultButton2 + vbYesNo)
  If resp = 7 Then
    Exit Sub
  End If
  m_fun.FnConnection.Execute "UPDATE psDli_istruzioni AS a, psDli_IstruzioniBrefr AS b, RefreshIstruzioniPcb AS c SET A.ordpcb_mg = b.ordpcb_mg " & _
                             "WHERE c.idpgm = b.idpgm And c.riga = b.riga And a.riga = c.new_riga And a.idpgm = b.idpgm"
  Set rs = m_fun.Open_Recordset("SELECT  idPgm,riga,NEW_RIGA,PSBList FROM  RefreshIstruzioniPcb")
  While Not rs.EOF
    arrPsb = Split(rs!PSBList, ";")
    For i = 0 To UBound(arrPsb)
      If Not Trim(arrPsb(i)) = "" Then
        Set rsPSB = m_fun.Open_Recordset("Select IdPsb from PsDli_IstrPSB where IdPgm = " & rs!idPgm & " and Riga = " & rs!NEW_RIGA & _
                                         " and IdPsb = " & arrPsb(i))
        If rsPSB.EOF Then
        ' inserimento riga (IdOggetto ???)
          m_fun.FnConnection.Execute "Insert Into PsDli_IstrPSB (IdPgm,IdOggetto,Riga,IDPsb) " & _
                                     "VALUES (" & rs!idPgm & "," & rs!idPgm & "," & rs!NEW_RIGA & "," & arrPsb(i) & ")"
        End If
        rsPSB.Close
      End If
    Next i
    rs.MoveNext
  Wend
  rs.Close
End Sub

Private Sub Command1_Click()
  Dim resp As Integer
  
  On Error GoTo messageFailure
  resp = MsgBox("This command is going to Backup Psb and Pcb tables deleting previous backup, continue ?", vbYesNo, "Tables backup")
  If resp = 6 Then
    CmDlg.Filter = "i-4.Migration Project DB *.Mty|*.Mty|Access db *.Mdb|*.Mdb|"
  
    CmDlg.FilterIndex = 1  'Sets default filter to *.mty
    CmDlg.ShowOpen
    If Trim(CmDlg.FileName) <> "" Then
      If InStr(CmDlg.FileName, "*") = 0 Then
        DropPcbTables
        'copy of PsDLI_Istruzioni structure
        m_fun.FnConnection.Execute "SELECT * INTO PsDLI_IstruzioniBrefr From PsDLI_Istruzioni Where 1 = 2 "
        ' fill the table with PsDLI_ISTRUZIONI from the other db
        m_fun.FnConnection.Execute "Insert Into PsDLI_IstruzioniBrefr Select * From PsDLI_Istruzioni In '" & CmDlg.FileName & "'", , adExecuteNoRecords
        'copy of PsDLI_IstrPSB structure
        m_fun.FnConnection.Execute "SELECT * INTO PsDLI_IstrPSBBrefr From PsDLI_IstrPSB Where 1 = 2 "
        ' fill the table with PsDLI_ISTRUZIONI from the other db
        m_fun.FnConnection.Execute "Insert Into PsDLI_IstrPSBBrefr Select * From PsDLI_IstrPSB In '" & CmDlg.FileName & "'", , adExecuteNoRecords
      End If
    End If
  End If
  Exit Sub
messageFailure:
  MsgBox "Impossible to create a backup copy of the tables", vbCritical
End Sub

Private Sub ecxst_Click()
  Dim path As String
  
  If optCobol Then
    MsgBox "PLI ONLY"
    Exit Sub
  ElseIf OptPLI Then
    language = "PLI"
  End If
  
  If Len(Trim(TxtBeforePath)) > 0 And Len(Trim(TxtAfterPath)) > 0 Then
    Screen.MousePointer = vbHourglass
    extr_Instructions TxtBeforePath
    extr_Instructions TxtAfterPath
    Screen.MousePointer = vbNormal
    MsgBox "Split Complete.", vbOKOnly + vbInformation
  Else
    If Len(Trim(TxtAfterPath)) Then
      Screen.MousePointer = vbHourglass
      extr_Instructions TxtAfterPath
      Screen.MousePointer = vbNormal
      MsgBox "Split Complete.", vbOKOnly + vbInformation
    Else
      If Len(Trim(TxtBeforePath)) Then
        Screen.MousePointer = vbHourglass
        extr_Instructions TxtBeforePath
        Screen.MousePointer = vbNormal
        MsgBox "Split Complete.", vbOKOnly + vbInformation
      Else
        MsgBox "Insert correct value in Routine Files Directory.", vbInformation, "i-4.Migration"
      End If
    End If
  End If
End Sub

Public Sub extr_Instructions(path As String)
  Dim i As Long
  Dim fileList As Collection
  Dim dirFile As String

  Set fileList = New Collection
  
  searchFiles path, fileList

  For i = 1 To fileList.Count
    If fileList.item(i) <> "." And fileList.item(i) <> ".." Then
      dirFile = Left(fileList.item(i), InStrRev(fileList.item(i), "\") - 1)
      If language = "CBL" Then
        SplitTemplate fileList.item(i), dirFile
      ElseIf language = "PLI" Then
        extr_PLI fileList.item(i), dirFile
      End If
    End If
  Next i
End Sub

Public Sub extr_PLI(file As String, path As String)
  Dim inFile As Integer, outextr As Integer, orignalfile As Integer
  Dim Line As String, oldLine As String
  Dim FileName As String
  Dim isOK As Boolean
  Dim esok As Boolean
  Dim nomeRoutine As String
  Dim inputstr As String

  On Error GoTo ErrorHandler

  inFile = FreeFile
  Open file For Input As inFile
  FileName = Right(file, Len(file) - InStrRev(file, "\"))
  outextr = FreeFile
  Open path & "\BPHX\" & FileName For Output As outextr
  orignalfile = FreeFile
  Open path & "\original\" & FileName For Output As orignalfile
  Do Until EOF(inFile)
  Line Input #inFile, Line

  inputstr = Line

  If isBphxcomment(inputstr) Then
    Print #outextr, inputstr
    esok = True
  Else
    esok = False
  End If

  If Not esok Then
    If isStartComment(inputstr) Then
      isOK = True
      esok = True
    Else
      esok = False
    End If
  End If

  If isOK Then
    If isEndComment(inputstr) Then
      isOK = False
      esok = True
      Print #outextr, inputstr
    Else
      esok = True
    End If
  End If

  If isOK Then
    Print #outextr, inputstr
  End If

  If esok = False Then
    Print #orignalfile, inputstr
  End If

  Loop
  Close #outextr
  Close #orignalfile
  Close #inFile

ErrorHandler:
  If err.Number = "76" Then
    MkDir path & "\BPHX\"
    MkDir path & "\original\"
    Resume
  End If
End Sub

Private Function isBphxcomment(inputstr As String) As Boolean
  If InStr(inputstr, "*BPHX*") Then
    isBphxcomment = True
  End If
End Function

Private Function isStartComment(inputstr As String) As Boolean
  Dim inputStrA As String

  inputStrA = LTrim(inputstr)
  If Left(inputStrA, 2) = "/*" Then
    inputStrA = Mid(inputStrA, 3)
    inputStrA = LTrim(inputStrA)
    If Left(inputStrA, 5) = "BPHX " Then
      inputStrA = Mid(inputStrA, 5)
      inputStrA = LTrim(inputStrA)
      If Left(inputStrA, 5) = "START" Then
        If Mid(inputStrA, 6, 1) = " " Or Mid(inputStrA, 6) = "" Then
          isStartComment = True
        End If
      End If
    End If
  End If
End Function

Private Function isEndComment(inputstr As String) As Boolean
  Dim inputStrA As String

  inputStrA = LTrim(inputstr)
  If Left(inputStrA, 2) = "/*" Then
    inputStrA = Mid(inputStrA, 3)
    inputStrA = LTrim(inputStrA)
    If Left(inputStrA, 5) = "BPHX " Then
      inputStrA = Mid(inputStrA, 5)
      inputStrA = LTrim(inputStrA)
      If Left(inputStrA, 3) = "END" Then
        If Mid(inputStrA, 4, 1) = " " Or Mid(inputStrA, 4) = "" Then
          isEndComment = True
        End If
      End If
    End If
  End If
End Function

Private Sub Form_Load()
  txtInstrxRout = 20
  txtTag = "BPHX"
    
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
End Sub

Public Sub check_Call_Incaps(incapsDir As String)
  Dim i As Long
  Dim fileList As Collection
  
  On Error GoTo catch
  Set fileList = New Collection
  
  ''''''''''''''''''''
  'ANALISI CALL INCAPS
  ''''''''''''''''''''
  searchFiles incapsDir, fileList
  
  For i = 1 To fileList.Count
    If fileList.item(i) <> "." And fileList.item(i) <> ".." Then
      If language = "CBL" Then
        checkCallIncaps fileList.item(i)
      ElseIf language = "PLI" Then
        checkCallIncaps_PLI fileList.item(i)
      End If
    End If
  Next i
  
  Exit Sub
catch:
  MsgBox err.Description
End Sub

' Mauro 18/01/2008 : Identifica le CALL nelle routine incapsulate
Public Sub checkCallIncaps(FileName As String)
  Dim FD As Long
  Dim Line As String, token As String
  Dim Riga As Long
  Dim lineNumber As Long
  Dim tag As String
  Dim Codifica As String
  
  On Error GoTo parseErr
  
  lineCallAster = ""
  lineCallIncaps = ""
  wCaller = ""
  wImsrtName = ""
  wOperation = ""
  wSSA_Name = ""
  wArea = ""
  wPCB = ""
  wLevel = ""
  wDlzPcb = ""
  wKeyValue = ""
  wDataArea_TO = ""
  wDlzPCB_TO = ""
  
  lineNumber = 0
  FD = FreeFile
  Open FileName For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(Line) = 0 Then
      Line Input #FD, Line
      lineNumber = lineNumber + 1
      tag = Left(Line, 4)
      Line = Mid(Line, 7)
    End If
    
    If Left(Line, 1) = "*" And InStr(Line, "CALL ") Then
      Riga = lineNumber
      lineCallAster = Trim(Mid(Line, 2, 65)) 'elimino etichette!
      If Not EOF(FD) Then
        Line Input #FD, Line
        lineNumber = lineNumber + 1
        tag = Left(Line, 4)
        Line = Mid(Line, 7)
      End If
      Do Until Left(Line, 1) <> "*" Or EOF(FD)
        lineCallAster = lineCallAster & " " & Trim(Mid(Line, 2, 65))
        Line Input #FD, Line
        lineNumber = lineNumber + 1
        tag = Left(Line, 4)
        Line = Mid(Line, 7)
      Loop
      
      Do Until EOF(FD)
''        If tag <> txtTag Then
''          Exit Do
''        Else
          ' Ho appena caricato la vecchia CALL asteriscata, adesso mi carico gli altri parametri
          token = nextToken_new(Line) 'MANTIENE GLI APICI!!
          If token = "MOVE" Then
            ''''''''''''''''''''''''''''''''
            Codifica = nextToken(Line)
            While token <> "TO"
              token = nextToken(Line)
              If Len(token) = 0 Then  'fine linea!
                'forzo l'entrata:
                Line = "************"
                While Mid(Line, 7, 1) = "*"
                  Line Input #FD, Line
                  lineNumber = lineNumber + 1
                  tag = Left(Line, 4)
                  Line = Mid(Line, 7)
                Wend
              End If
            Wend
            'Campo destinazione:
            token = nextToken(Line)
            If token = "IMSRTNAME" Or token = "IMSRTNAME." Then
              wImsrtName = wImsrtName & IIf(Len(wImsrtName), ",", "") & Codifica
            ElseIf token = "LNKDB-CALLER" Or token = "LNKDB-CALLER." Then
              wCaller = wCaller & IIf(Len(wCaller), ",", "") & Codifica
            ElseIf token = "LNKDB-OPERATION" Or token = "LNKDB-OPERATION." Then
              wOperation = wOperation & IIf(Len(wOperation), ",", "") & Codifica
            ElseIf token = "LNKDB-NUMPCB" Or token = "LNKDB-NUMPCB." Then
              wPCB = wPCB & IIf(Len(wPCB), ",", "") & Codifica
            ElseIf token = "LNKDB-SEGLEVEL" Then
              wLevel = wLevel & IIf(Len(wLevel), ",", "") & Codifica
            ElseIf token = "LNKDB-DATA-AREA" Then
              wArea = wArea & IIf(Len(wArea), ",", "") & Codifica
            ElseIf token = "LNKDB-SSA-AREA" Then
              wSSA_Name = wSSA_Name & IIf(Len(wSSA_Name), ",", "") & Codifica
            ElseIf token = "LNKDB-DLZPCB" Then
              wDlzPcb = wDlzPcb & IIf(Len(wDlzPcb), ",", "") & Codifica
            ElseIf token = "LNKDB-KEYVALUE" Then
              wKeyValue = wKeyValue & IIf(Len(wKeyValue), ",", "") & Codifica
            ElseIf Len(token) = 0 Then
              'forzo l'entrata:
              Line = "************"
              While Mid(Line, 7, 1) = "*"
                Line Input #FD, Line
                lineNumber = lineNumber + 1
                tag = Left(Line, 4)
                Line = Mid(Line, 7)
              Wend
              token = nextToken(Line)
              If token = "IMSRTNAME" Or token = "IMSRTNAME." Then
                wImsrtName = wImsrtName & IIf(Len(wImsrtName), ",", "") & Codifica
              ElseIf token = "LNKDB-CALLER" Or token = "LNKDB-CALLER." Then
                wCaller = wCaller & IIf(Len(wCaller), ",", "") & Codifica
              ElseIf token = "LNKDB-OPERATION" Or token = "LNKDB-OPERATION." Then
                wOperation = wOperation & IIf(Len(wOperation), ",", "") & Codifica
              ElseIf token = "LNKDB-NUMPCB" Or token = "LNKDB-NUMPCB." Then
                wPCB = wPCB & IIf(Len(wPCB), ",", "") & Codifica
              ElseIf token = "LNKDB-SEGLEVEL" Then
                wLevel = wLevel & IIf(Len(wLevel), ",", "") & Codifica
              ElseIf token = "LNKDB-DATA-AREA" Then
                wArea = wArea & IIf(Len(wArea), ",", "") & Codifica
              ElseIf token = "LNKDB-SSA-AREA" Then
                wSSA_Name = wSSA_Name & IIf(Len(wSSA_Name), ",", "") & Codifica
              ElseIf token = "LNKDB-DLZPCB" Then
                wDlzPcb = wDlzPcb & IIf(Len(wDlzPcb), ",", "") & Codifica
              ElseIf token = "LNKDB-KEYVALUE" Then
                wKeyValue = wKeyValue & IIf(Len(wKeyValue), ",", "") & Codifica
              ElseIf Len(token) = 0 Then
                MsgBox "ATTENTION! IT'S NOT OK: Still new line - PGM: " & FileName & " - ROW: " & lineNumber
              End If
            End If
            ''''''''''''''''''''''''''''''''
            Line = ""
          ElseIf token = "CALL" Then
            lineCallIncaps = token & " " & Trim(Line)
            Line Input #FD, Line
            lineNumber = lineNumber + 1
            tag = Left(Line, 4)
            Line = Mid(Line, 7)
            Do Until EOF(FD)
              If tag <> txtTag Or InStr(Line, "MOVE") Or (Left(Line, 1) = "*" And InStr(Line, "CALL ")) Then
                Exit Do
              End If
              lineCallIncaps = lineCallIncaps & " " & Trim(Mid(Line, 2, 65))
              Line Input #FD, Line
              lineNumber = lineNumber + 1
              tag = Left(Line, 4)
              Line = Mid(Line, 7)
            Loop
            ' Devo intercettare queste due MOVE
            'MOVE LNKDB-DATA-AREA(1) TO MF-CEVT
            'MOVE LNKDB-DLZPCB       TO MF-CEVT-PCB
            Do Until EOF(FD)
              If tag <> txtTag Or (Left(Line, 1) = "*" And InStr(Line, "CALL ")) Then
                Exit Do
              End If
              token = nextToken_new(Line) 'MANTIENE GLI APICI!!
              If token = "MOVE" Then
                ''''''''''''''''''''''''''''''''
                Codifica = nextToken(Line)
                While token <> "TO"
                  token = nextToken(Line)
                  If Len(token) = 0 Then  'fine linea!
                    'forzo l'entrata:
                    Line = "************"
                    While Mid(Line, 7, 1) = "*"
                      Line Input #FD, Line
                      lineNumber = lineNumber + 1
                      tag = Left(Line, 4)
                      Line = Mid(Line, 7)
                    Wend
                  End If
                Wend
                If Codifica = "LNKDB-DATA-AREA" Then
                  token = nextToken(Line)
                  wDataArea_TO = wDataArea_TO & IIf(Len(wDataArea_TO), ",", "") & token
                ElseIf Codifica = "LNKDB-DLZPCB" Then
                  token = nextToken(Line)
                  wDlzPCB_TO = wDlzPCB_TO & IIf(Len(wDlzPCB_TO), ",", "") & token
                End If
              End If
              Line Input #FD, Line
              lineNumber = lineNumber + 1
              tag = Left(Line, 4)
              Line = Mid(Line, 7)
            Loop
          Else
            Line = ""
          End If
          If tag <> txtTag Or (Left(Line, 1) = "*" And InStr(Line, "CALL ")) Then
            SalvaCallIncaps Mid(FileName, Len(txtPath_CallIncaps) + 1), Riga
            Exit Do
          End If
          If Not EOF(FD) And Line = "" Then
            Line Input #FD, Line
            lineNumber = lineNumber + 1
            tag = Left(Line, 4)
            Line = Mid(Line, 7)
          End If
''        End If
      Loop
    Else
      Line = ""
    End If
  Wend
  Close FD
  
  On Error Resume Next
  Exit Sub
  
parseErr:   'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(Line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, Line
      lineNumber = lineNumber + 1
      tag = Left(Line, 4)
      Line = Mid(Line, 7)
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(Line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, Line
        lineNumber = lineNumber + 1
        tag = Left(Line, 4)
        Line = Mid(Line, 7)
        If Left(Line, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          MsgBox err.Description
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        MsgBox err.Description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    MsgBox err.Description
    Resume Next
  Else
    MsgBox err.Description
    Close FD
  End If
End Sub

' Mauro 13/02/2008 : Identifica le CALL nelle routine incapsulate
Public Sub checkCallIncaps_PLI(FileName As String)
  Dim FD As Long
  Dim Line As String, token As String
  Dim Riga As Long
  Dim lineNumber As Long
  Dim tag As String
  Dim Codifica As String
  
  On Error GoTo parseErr
  
  lineCallAster = ""
  lineCallIncaps = ""
  wCaller = ""
  wImsrtName = ""
  wOperation = ""
  wSSA_Name = ""
  wArea = ""
  wPCB = ""
  wLevel = ""
  wDlzPcb = ""
  wKeyValue = ""
  wDataArea_TO = ""
  wDlzPCB_TO = ""
  
  lineNumber = 0
  FD = FreeFile
  Open FileName For Input As FD
  While Not EOF(FD)
    DoEvents  'valutare se tenere
    'line fa da "flag": leggo una nuova riga solo se NON valorizzata
    '(gia' letta da una routine precedente...)
    If Len(Line) = 0 Then
      Line Input #FD, Line
      lineNumber = lineNumber + 1
    End If
    
    If InStr(Line, "/* " & txtTag) > 0 And InStr(Line, "START") > 0 Then
      Do Until InStr(Line, txtTag) > 0 And InStr(Line, "END") > 0 And InStr(Line, "*/") > 0
        Line Input #FD, Line
        lineNumber = lineNumber + 1
        If InStr(Line, "CALL ") Then
          Riga = lineNumber
          lineCallAster = Trim(Mid(Line, 2, 65)) 'elimino etichette!
          Do Until InStr(Line, txtTag) > 0 And InStr(Line, "END") > 0 And InStr(Line, "*/") > 0
            lineCallAster = lineCallAster & " " & Trim(Mid(Line, 2, 65))
            Line Input #FD, Line
            lineNumber = lineNumber + 1
          Loop
          Exit Do
        End If
      Loop
      
      If Len(lineCallAster) Then
        Do Until EOF(FD) Or InStr(token & "   " & Line, "/*   " & txtTag & "  END")
          If InStr(Line, "/* " & txtTag & " START         */") Then
            Line Input #FD, Line
            lineNumber = lineNumber + 1
            Do Until EOF(FD)
              ' Ho appena caricato la vecchia CALL asteriscata, adesso mi carico gli altri parametri
              token = nextToken_new(Line) 'MANTIENE GLI APICI!!
              If token = "LNKDB_CALLER" Then
                Do Until Codifica = "="
                  Codifica = nextToken_new(Line)
                Loop
                If Line = "" Then
                  Line Input #FD, Line
                  lineNumber = lineNumber + 1
                  Codifica = Trim(Line)
                Else
                  Codifica = nextToken_new(Line)
                End If
                wCaller = wCaller & IIf(Len(wCaller), ",", "") & Replace(Codifica, ";", "")
              ElseIf token = "LNKDB_OPERATION" Then
                Do Until Codifica = "="
                  Codifica = nextToken_new(Line)
                Loop
                If Line = "" Then
                  Line Input #FD, Line
                  lineNumber = lineNumber + 1
                  Codifica = Trim(Line)
                Else
                  Codifica = nextToken_new(Line)
                End If
                wOperation = wOperation & IIf(Len(wOperation), ",", "") & Replace(Codifica, ";", "")
              ElseIf token = "LNKDB_PCB" Then
                Do Until Codifica = "="
                  Codifica = nextToken_new(Line)
                Loop
                If Line = "" Then
                  Line Input #FD, Line
                  lineNumber = lineNumber + 1
                  Codifica = Trim(Line)
                Else
                  Codifica = nextToken_new(Line)
                End If
                wPCB = wPCB & IIf(Len(wPCB), ",", "") & Replace(Codifica, ";", "")
              ElseIf token = "LNKDB_SEGLEVEL" Then
                Do Until Codifica = "="
                  Codifica = nextToken_new(Line)
                Loop
                If Line = "" Then
                  Line Input #FD, Line
                  lineNumber = lineNumber + 1
                  Codifica = Trim(Line)
                Else
                  Codifica = nextToken_new(Line)
                End If
                wLevel = wLevel & IIf(Len(wLevel), ",", "") & Replace(Codifica, ";", "")
              ElseIf token = "LNKDB_DATA_AREA" Then
                Do Until Codifica = "="
                  Codifica = nextToken_new(Line)
                Loop
                If Line = "" Then
                  Line Input #FD, Line
                  lineNumber = lineNumber + 1
                  Codifica = Trim(Line)
                Else
                  Codifica = nextToken_new(Line)
                End If
                wArea = wArea & IIf(Len(wArea), ",", "") & Replace(Codifica, ";", "")
              ElseIf token = "LNKDB_SSA_AREA" Then
                Do Until Codifica = "="
                  Codifica = nextToken_new(Line)
                Loop
                If Line = "" Then
                  Line Input #FD, Line
                  lineNumber = lineNumber + 1
                  Codifica = Trim(Line)
                Else
                  Codifica = nextToken_new(Line)
                End If
                wSSA_Name = wSSA_Name & IIf(Len(wSSA_Name), ",", "") & Replace(Codifica, ";", "")
              ElseIf token = "LNKDB_DLZPCB" Then
                Do Until Codifica = "="
                  Codifica = nextToken_new(Line)
                Loop
                If Line = "" Then
                  Line Input #FD, Line
                  lineNumber = lineNumber + 1
                  Codifica = Trim(Line)
                Else
                  Codifica = nextToken_new(Line)
                End If
                wDlzPcb = wDlzPcb & IIf(Len(wDlzPcb), ",", "") & Replace(Codifica, ";", "")
              ElseIf token = "LNKDB_KEYVALUE" Then
                Do Until Codifica = "="
                  Codifica = nextToken_new(Line)
                Loop
                If Line = "" Then
                  Line Input #FD, Line
                  lineNumber = lineNumber + 1
                  Codifica = Trim(Line)
                Else
                  Codifica = nextToken_new(Line)
                End If
                wKeyValue = wKeyValue & IIf(Len(wKeyValue), ",", "") & Replace(Codifica, ";", "")
              ElseIf token = "CALL" Then
                lineCallIncaps = token & " " & Trim(Line)
              Else
                If Line = "=" Then
                  Line Input #FD, Line
                  lineNumber = lineNumber + 1
                  Line = "= " & Trim(Line)
                End If
                If InStr(Line, "SUBSTR(LNKDB_DLZPCB") Then
                  wDlzPCB_TO = wDlzPCB_TO & IIf(Len(wDlzPCB_TO), ",", "") & Replace(token & " " & Trim(Line), ";", "")
                ElseIf InStr(Line, "SUBSTR(LNKDB_DATA_AREA") Then
                  wDataArea_TO = wDataArea_TO & IIf(Len(wDataArea_TO), ",", "") & Replace(token & " " & Trim(Line), ";", "")
                End If
              End If
              ''''''''''''''''''''''''''''''''
              If InStr(token & "   " & Line, "/*   " & txtTag & "  END") Then
                SalvaCallIncaps Mid(FileName, Len(txtPath_CallIncaps) + 1), Riga
                Exit Do
              End If
              Line = ""
              If Not EOF(FD) And Line = "" Then
                Line Input #FD, Line
                lineNumber = lineNumber + 1
              End If
            Loop
          Else
            Line = ""
          End If
          If Not EOF(FD) And Line = "" Then
            Line Input #FD, Line
            lineNumber = lineNumber + 1
          End If
        Loop
      End If
    Else
      Line = ""
    End If
  Wend
  Close FD
  
  On Error Resume Next
  Exit Sub
  
parseErr:   'Gestito a livello superiore...
  If err.Number = 123 Then
    'Eccezione nextToken: (probabilmente ho token=IF e linea che inizia con "("...)
    If Left(Line, 1) = "(" Then
      'OK: non analizziamo tutte le IF, ecc... quindi procediamo
      'Butto anche via la successiva...
      Line Input #FD, Line
      lineNumber = lineNumber + 1
      tag = Left(Line, 4)
      Line = Mid(Line, 7)
      token = "IF" 'forzo a IF...
    Else
      'SQ 9-02-06 - creare un 126...
      'VALUE che va a capo...
      If Left(Line, 2) = "''" Then
        'Butto anche via la successiva...
        Line Input #FD, Line
        lineNumber = lineNumber + 1
        tag = Left(Line, 4)
        Line = Mid(Line, 7)
        If Left(Line, 1) = "-" Then
          'OK
          token = "VALUE" 'va nell'else: OK...
        Else
          MsgBox err.Description
        End If
      Else
        'gestire
        'GbErrLevel = "P-0"
        MsgBox err.Description
      End If
    End If
    Resume Next
  ElseIf err.Number = 124 Then  'parseCall... gestire meglio
    MsgBox err.Description
    Resume Next
  Else
    MsgBox err.Description
    Close FD
  End If
End Sub

Private Sub SalvaCallIncaps(nomePgm As String, numRiga As Long)
  Dim rs As Recordset
    
  Set rs = m_fun.Open_Recordset("SELECT * FROM TMP_CallIncaps")
  rs.AddNew
  
  rs!nomePgm = nomePgm
  rs!Riga = numRiga
  rs!CallAsteriscata = lineCallAster
  rs!Operation = wOperation
  rs!IMSRtName = wImsrtName
  rs!caller = wCaller
  rs!ssaName = wSSA_Name
  rs!area = wArea
  rs!PCB = wPCB
  rs!CallIncapsulata = lineCallIncaps
  rs!level = wLevel
  rs!DlzPcb = wDlzPcb
  rs!keyValue = wKeyValue
  rs!dataarea_to = wDataArea_TO
  rs!dlzpcb_to = wDlzPCB_TO
  
  rs.Update
  rs.Close
  
  lineCallAster = ""
  lineCallIncaps = ""
  wCaller = ""
  wImsrtName = ""
  wOperation = ""
  wSSA_Name = ""
  wArea = ""
  wPCB = ""
  wLevel = ""
  wDlzPcb = ""
  wKeyValue = ""
  wDlzPCB_TO = ""
  wDataArea_TO = ""
End Sub

Public Sub Combine_Routine(pathRoutines As String)
  Dim i As Long
  Dim fileList As Collection
  Dim FileName As String

  On Error GoTo catch
  Set fileList = New Collection

  ''''''''''''''''''''
  'ANALISI CALL INCAPS
  ''''''''''''''''''''
  searchFiles pathRoutines, fileList

  For i = 1 To fileList.Count
    If fileList.item(i) <> "." And fileList.item(i) <> ".." Then
      FileName = Mid(fileList.item(i), InStrRev(fileList.item(i), "\") + 1)
      If language = "CBL" Then
        If Not IsNumeric(Mid(FileName, 7, 2)) Then
          CombineRout fileList.item(i), pathRoutines & IIf(Right(pathRoutines, 1) = "\", "", "\")
        End If
      ElseIf language = "PLI" Then
        If Mid(FileName, 5, 1) = 0 Then
          CombineRout_PLI fileList.item(i), pathRoutines & IIf(Right(pathRoutines, 1) = "\", "", "\")
        End If
      End If
    End If
  Next i

  Exit Sub
catch:
  MsgBox err.Description
End Sub

Public Sub CombineRout(FileName As String, pathRout As String)
  Dim inFile As Integer, outFile As Integer
  Dim inLine As String, outLine As String
  Dim nomeFileOut As String, pathFileOut As String
  'Dim rtbFile As RichTextBox, isTrovato As Boolean
  Dim rtbFile As String, isTrovato As Boolean
  Dim estensione As String, RoutImp() As String
  Dim Idx As Long, i As Long
  Dim comments As String, ifWorking As String, Instructions As String
  Dim nomeFileInput As String
  
  On Error GoTo ErrorHandler
  ReDim RoutImp(0)
  inFile = FreeFile
  'Set rtbFile = rtbText
  'rtbFile.text = ""
  rtbFile = ""
  nomeFileInput = Mid(FileName, Len(pathRout) + 1)
  If InStr(nomeFileInput, ".") Then
    nomeFileInput = Left(nomeFileInput, InStr(nomeFileInput, ".") - 1)
  End If
  
  Open FileName For Input As inFile
  Line Input #inFile, inLine
  Do Until EOF(inFile)
    If Mid(inLine, 7, 26) = "     IF LNKDB-OPERATION = " Then
      Line Input #inFile, inLine
      ' Se la RichTextBox � vuota, mi carico tutto il file, per usarlo come Template-Routine
      estensione = IIf(InStr(FileName, "."), Right(FileName, 3), "")
      nomeFileOut = Mid(inLine, InStr(inLine, "'") + 1, 8)
      isTrovato = False
      For i = 1 To UBound(RoutImp)
        If RoutImp(i) = nomeFileOut Then
          isTrovato = True
          Exit For
        End If
      Next i
      If Not isTrovato Then
        ReDim Preserve RoutImp(UBound(RoutImp) + 1)
        RoutImp(UBound(RoutImp)) = nomeFileOut
        comments = ""
        ifWorking = ""
        Instructions = ""

        pathFileOut = pathRout & nomeFileOut & IIf(Len(estensione), ".", "") & estensione
        outFile = FreeFile
        Open pathFileOut For Input As outFile
        Line Input #outFile, outLine
        If rtbFile = "" Then
          Do Until EOF(outFile)
            rtbFile = rtbFile & IIf(Len(rtbFile), vbCrLf, "") & outLine
            Line Input #outFile, outLine
          Loop
          
          rtbFile = Replace(rtbFile, nomeFileOut, nomeFileInput)
        Else
          Do Until Mid(outLine, 7, 10) = "**  #CODE#"
            Line Input #outFile, outLine
          Loop
          Line Input #outFile, outLine
          ' COMMENTI
          'idx = rtbFile.find("**  #CODE#")
          'idx = rtbFile.find(vbCrLf, idx)
          Idx = InStr(rtbFile, "**  #CODE#")
          Idx = InStr(Idx, rtbFile, vbCrLf)
          Do Until Mid(outLine, 7, 2) = "**"
            comments = comments & vbCrLf & outLine
            Line Input #outFile, outLine
          Loop
          rtbFile = Left(rtbFile, Idx - 1) & comments & Mid(rtbFile, Idx + 2)

          Do Until Mid(outLine, 7, 10) = "*    #OPE#"
            Line Input #outFile, outLine
          Loop
          Line Input #outFile, outLine
          ' If LNKDB-OPERATION
          'idx = rtbFile.find("*    #OPE#")
          'idx = rtbFile.find(vbCrLf, idx)
          Idx = InStr(rtbFile, "*    #OPE#")
          Idx = InStr(Idx, rtbFile, vbCrLf)
          Do Until Mid(outLine, 7, 36) = "     MOVE 'NOOP' TO LNKDB-CODE-ERROR"
            ifWorking = ifWorking & vbCrLf & outLine
            Line Input #outFile, outLine
          Loop
          rtbFile = Left(rtbFile, Idx - 1) & ifWorking & Mid(rtbFile, Idx + 2)

          Do Until Mid(UCase(outLine), 7, 16) = "*  CALL  :      "
            Line Input #outFile, outLine
          Loop
          Instructions = Instructions & vbCrLf & "      *------------------------------------------------------------*"
          ' ISTRUZIONE: Le accodo al testo gi� importato
          Do Until EOF(outFile)
            Instructions = Instructions & vbCrLf & outLine
            Line Input #outFile, outLine
          Loop
          rtbFile = rtbFile & Instructions
        End If
        Close outFile
      End If
    End If
    Line Input #inFile, inLine
  Loop
  Close inFile
  
  outFile = FreeFile
  pathFileOut = pathRout & "Out\" & Right(FileName, Len(FileName) - InStrRev(FileName, "\"))
  Open pathFileOut For Output As outFile
  Print #outFile, rtbFile
  Close outFile
  Exit Sub
ErrorHandler:
  If err.Number = 76 Then
    m_fun.MkDir_API pathRout & "Out\"
    Resume
  Else
    MsgBox err.Description
  End If
End Sub

Public Sub CombineRout_PLI(FileName As String, pathRout As String)
  Dim inFile As Integer, outFile As Integer
  Dim inLine As String, outLine As String
  Dim nomeFileOut As String, pathFileOut As String
  Dim rtbFile As String, isTrovato As Boolean
  Dim RTBR As RichTextBox
  Dim estensione As String, RoutImp() As String
  Dim Idx As Long, i As Long
  Dim ifWorking As String, Instructions As String
  Dim nomeFileInput As String
  Dim oldLine As String
  
  On Error GoTo ErrorHandler
  ReDim RoutImp(0)
  inFile = FreeFile
  rtbFile = ""
  Set RTBR = rtbText
  nomeFileInput = Mid(FileName, Len(pathRout) + 1)
  If InStr(nomeFileInput, ".") Then
    nomeFileInput = Left(nomeFileInput, InStr(nomeFileInput, ".") - 1)
  End If
  
  Open FileName For Input As inFile
  Line Input #inFile, inLine
  Do Until EOF(inFile)
    If Mid(inLine, 7, 26) = "     IF LNKDB_OPERATION = " Then
      Line Input #inFile, inLine
      ' Se la RichTextBox � vuota, mi carico tutto il file, per usarlo come Template-Routine
      estensione = IIf(InStr(FileName, "."), Right(FileName, 3), "")
      ' Temporaneo: senza apici per evidenziare la routine chiamata � un p� un problema
      nomeFileOut = Mid(Trim(inLine), InStr(Trim(inLine), " ") + 1, 8)
      isTrovato = False
      For i = 1 To UBound(RoutImp)
        If RoutImp(i) = nomeFileOut Then
          isTrovato = True
          Exit For
        End If
      Next i
      If Not isTrovato Then
        ReDim Preserve RoutImp(UBound(RoutImp) + 1)
        RoutImp(UBound(RoutImp)) = nomeFileOut
        ifWorking = ""
        Instructions = ""

        pathFileOut = pathRout & Trim(nomeFileOut) & IIf(Len(estensione), ".", "") & estensione
        outFile = FreeFile
        Open pathFileOut For Input As outFile
        Line Input #outFile, outLine
        If rtbFile = "" Then
          oldLine = ""
          Do Until EOF(outFile)
            If Len(oldLine) Then
              If UCase(Left(outLine, 69)) = " /*             R E T U R N   T O   C A L L E R                    */" Then
                rtbFile = rtbFile & IIf(Len(rtbFile), vbCrLf, "") & "#INSTR#"
                rtbFile = rtbFile & IIf(Len(rtbFile), vbCrLf, "") & oldLine
                rtbFile = rtbFile & IIf(Len(rtbFile), vbCrLf, "") & outLine
                oldLine = ""
              Else
                rtbFile = rtbFile & IIf(Len(rtbFile), vbCrLf, "") & oldLine
                rtbFile = rtbFile & IIf(Len(rtbFile), vbCrLf, "") & outLine
                oldLine = ""
              End If
            Else
              rtbFile = rtbFile & IIf(Len(rtbFile), vbCrLf, "") & outLine
            End If
            Line Input #outFile, outLine
            If Left(outLine, 69) = " /*----------------------------------------------------------------*/" Then
              oldLine = outLine
              Line Input #outFile, outLine
            End If
          Loop
          
          rtbFile = Replace(rtbFile, Trim(nomeFileOut), Trim(nomeFileInput))
        Else
          Do Until Mid(outLine, 8, 15) = " /*    #OPE# */"
            Line Input #outFile, outLine
          Loop
          Line Input #outFile, outLine
          ' If LNKDB-OPERATION
          Idx = InStr(rtbFile, " /*    #OPE# */")
          Idx = InStr(Idx, rtbFile, vbCrLf)
          Do Until EOF(outFile) Or UCase(Left(outLine, 29)) = "  LNKDB_CODE_ERROR = 'NOOP' ;"
            ifWorking = ifWorking & vbCrLf & outLine
            Line Input #outFile, outLine
          Loop
          rtbFile = Left(rtbFile, Idx - 1) & ifWorking & Mid(rtbFile, Idx + 2)

          Do Until EOF(outFile) Or Mid(UCase(outLine), 7, 16) = "*  CALL  :      "
            Line Input #outFile, outLine
          Loop
          Instructions = Instructions & vbCrLf & "     /*------------------------------------------------------------*"
          ' ISTRUZIONE: Le accodo al testo gi� importato
          Do Until EOF(outFile) Or Left(outLine, 69) = " /*----------------------------------------------------------------*/"
            Instructions = Instructions & vbCrLf & outLine
            Line Input #outFile, outLine
          Loop
          RTBR.text = rtbFile
          changeTag RTBR, "#INSTR#", Instructions & vbCrLf & "#INSTR#"
          rtbFile = RTBR.text
        End If
        Close outFile
      End If
    End If
    Line Input #inFile, inLine
  Loop
  Close inFile
  
  outFile = FreeFile
  rtbFile = Replace(rtbFile, "#INSTR#", "")
  pathFileOut = pathRout & "Out\" & Right(FileName, Len(FileName) - InStrRev(FileName, "\"))
  Open pathFileOut For Output As outFile
  Print #outFile, rtbFile
  Close outFile
  Exit Sub
ErrorHandler:
  If err.Number = 76 Then
    m_fun.MkDir_API pathRout & "Out\"
    Resume
  Else
    MsgBox err.Description
  End If
End Sub

Sub ExternalEdit(strTxtEditor As String)
  Dim strFileSel As String
  
  strFileSel = TxtBeforePath & "\BPHX" & " " & TxtAfterPath & "\BPHX"

  If Len(Trim(TxtBeforePath)) > 0 And Len(Trim(TxtAfterPath)) > 0 Then
    Shell m_fun.FnCompareEditor & " " & strFileSel, vbMaximizedFocus
  Else
    MsgBox "Incorrect Directory Name!", vbOKOnly + vbExclamation, "i-4.Migration"
  End If
End Sub
