VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MaimdC_Menu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_ImErrCodice As String
Private m_ImErrMsg As String
Private m_ImFinestra As Object
Private m_ImObjList As Object
Private m_ImTop As Long
Private m_ImLeft As Long
Private m_ImWidth As Long
Private m_ImHeight As Long
Private m_ImDatabase As ADOX.Catalog
Private m_ImNomeDB As String
Private m_ImConnection As ADODB.Connection
Private m_ImConnDBSys As ADODB.Connection
Private m_ImConnState As Long
Private m_ImPathDB As String
Private m_ImTipoDB As String
Private m_ImTipoMigrazione As String
Private m_ImPathDef As String
Private m_ImImgDir As String
Private m_ImParent As Long
Private m_ImId As Integer
Private m_ImLabel As String
Private m_ImToolTiptext As String
Private m_ImPicture As String
Private m_ImPictureEn As String
Private m_ImM1Name As String
Private m_ImM1SubName As String
Private m_ImM1Level As Long
Private m_ImM2Name As String
Private m_ImM3Name As String
Private m_ImM3ButtonType As String
Private m_ImM3SubName As String
Private m_ImM4Name As String
Private m_ImM4ButtonType As String
Private m_ImM4SubName As String
Private m_ImFunzione As String
Private m_ImDllName As String
Private m_ImTipoFinIm As String
Private m_ImCollectionParam As Collection
Private m_ImAsterixForDelete As String
Private m_ImAsterixForWhere As String
Private m_ImProvider As e_Provider
Private m_ImIdOggetto_Add As Long
Private m_ImNomeProdotto As String
Private m_ImUnitDef As String
Private m_ImMenuIcon As String

Dim ImIndMnu As Integer
Dim ImIndImg As Integer

Public Function AttivaFunzione(Funzione As String, cParam As Collection, formParent As Object, Optional ByVal IdOggetto As Long, Optional ByVal IdButton As Long) As Boolean
  Dim wResp As Variant
  On Error GoTo errorHandler
  If cParam.Count > 0 Then
     ImIdOggetto_Add = cParam.item(1)
  End If
  Select Case Trim(UCase(Funzione))
    Case "MIGR_MFS2BMS"
        SetParent MaimdF_Mappe.hwnd, formParent.hwnd
        MaimdF_Mappe.Show
        MaimdF_Mappe.Move 0, 0, formParent.Width, formParent.Height
      
    Case "IMS_INCAPS"
        SetParent MaimdF_Incapsulatore.hwnd, formParent.hwnd
        MaimdF_Incapsulatore.Show
        MaimdF_Incapsulatore.Move 0, 0, formParent.Width, formParent.Height
       
    Case "IMS_GENROUT"
        SetParent MaimdF_GenRout.hwnd, formParent.hwnd
        MaimdF_GenRout.Show
        MaimdF_GenRout.Move 0, 0, formParent.Width, formParent.Height
       
    Case Else
   
  End Select
   Exit Function
errorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002"
  
End Function

Public Function Move_To_Button(Direction As String) As Boolean
  'Direction =
  '
  '1)First    : Si muove al primo bottone dell'array
  '2)Next     : Si muove al successivo
  '3)Previous : Si muove al precedente
  '4)Last     : Si muove all'ultimo
  
  If Not SwMenu Then
    Move_To_Button = Carica_Menu_DC
  End If
  
  'seleziona l'indice
  Select Case UCase(Trim(Direction))
    Case "FIRST"
      ImIndMnu = 1
    Case "NEXT"
      ImIndMnu = ImIndMnu + 1
    Case "PREVIOUS"
      ImIndMnu = ImIndMnu - 1
    Case "LAST"
      ImIndMnu = UBound(ImMenu)
  End Select
    
  If ImIndMnu > UBound(ImMenu) Then
    Move_To_Button = False
    Exit Function
  End If
  
  'assegna alle variabili public il contenuto del 1� elemento dell'array
  ImId = ImMenu(ImIndMnu).Id
  ImLabel = ImMenu(ImIndMnu).Label
  ImToolTiptext = ImMenu(ImIndMnu).ToolTipText
  ImPicture = ImMenu(ImIndMnu).Picture
  ImPictureEn = ImMenu(ImIndMnu).PictureEn
  ImM1Name = ImMenu(ImIndMnu).M1Name
  ImM1SubName = ImMenu(ImIndMnu).M1SubName
  ImM1Level = ImMenu(ImIndMnu).M1Level
  ImM2Name = ImMenu(ImIndMnu).M2Name
  ImM3Name = ImMenu(ImIndMnu).M3Name
  ImM3ButtonType = ImMenu(ImIndMnu).M3ButtonType
  ImM3SubName = ImMenu(ImIndMnu).M3SubName
  ImM4Name = ImMenu(ImIndMnu).M4Name
  ImM4ButtonType = ImMenu(ImIndMnu).M4ButtonType
  ImM4SubName = ImMenu(ImIndMnu).M4SubName
  ImFunzione = ImMenu(ImIndMnu).Funzione
  ImDllName = ImMenu(ImIndMnu).DllName
  ImTipoFinIm = ImMenu(ImIndMnu).TipoFinIm
  
  Move_To_Button = True
End Function

Public Sub InitDll(curDllFunzioni As MaFndC_Funzioni)
   Set m_fun = curDllFunzioni
  Carica_Menu_DC
End Sub

Private Sub Class_Initialize()
  Set m_ImsDll_DC = Me
End Sub

Public Function CreaTableIms() As Boolean

End Function

Public Property Let ImErrCodice(mVar As String)
    m_ImErrCodice = mVar
End Property

Public Property Get ImErrCodice() As String
    ImErrCodice = m_ImErrCodice
End Property

'IMERRMSG
Public Property Let ImErrMsg(mVar As String)
    m_ImErrMsg = mVar
End Property

Public Property Get ImErrMsg() As String
    ImErrMsg = m_ImErrMsg
End Property

'IMFINESTRA
Public Property Set ImFinestra(mVar As Object)
   Set m_ImFinestra = mVar
End Property

Public Property Get ImFinestra() As Object
   Set ImFinestra = m_ImFinestra
End Property

'IMOBJLIST
Public Property Set ImObjList(mVar As Object)
   Set m_ImObjList = mVar
End Property

Public Property Get ImObjList() As Object
   Set ImObjList = m_ImObjList
End Property

'IMTOP
Public Property Let ImTop(mVar As Long)
    m_ImTop = mVar
End Property

Public Property Get ImTop() As Long
    ImTop = m_ImTop
End Property

'IMLEFT
Public Property Let ImLeft(mVar As Long)
    m_ImLeft = mVar
End Property

Public Property Get ImLeft() As Long
    ImLeft = m_ImLeft
End Property

'IMWIDTH
Public Property Let ImWidth(mVar As Long)
    m_ImWidth = mVar
End Property

Public Property Get ImWidth() As Long
    ImWidth = m_ImWidth
End Property

'IMHEIGHT
Public Property Let ImHeight(mVar As Long)
    m_ImHeight = mVar
End Property

Public Property Get ImHeight() As Long
    ImHeight = m_ImHeight
End Property

'IMDATABASE
Public Property Set ImDatabase(mVar As ADOX.Catalog)
   Set m_ImDatabase = mVar
End Property

Public Property Get ImDatabase() As ADOX.Catalog
   Set ImDatabase = m_ImDatabase
End Property

'IMNOMEDB
Public Property Let ImNomeDB(mVar As String)
    m_ImNomeDB = mVar
End Property

Public Property Get ImNomeDB() As String
    ImNomeDB = m_ImNomeDB
End Property

'IMCONNECTION
Public Property Set ImConnection(mVar As ADODB.Connection)
   Set m_ImConnection = mVar
End Property

Public Property Get ImConnection() As ADODB.Connection
   Set ImConnection = m_ImConnection
End Property

'IMCONNDBSYS
Public Property Set ImConnDBSys(mVar As ADODB.Connection)
   Set m_ImConnDBSys = mVar
End Property

Public Property Get ImConnDBSys() As ADODB.Connection
   Set ImConnDBSys = m_ImConnDBSys
End Property

'IMCONNSTATE
Public Property Let ImConnState(mVar As Long)
    m_ImConnState = mVar
End Property

Public Property Get ImConnState() As Long
    ImConnState = m_ImConnState
End Property

'IMPATHDB
Public Property Let ImPathDB(mVar As String)
    m_ImPathDB = mVar
End Property

Public Property Get ImPathDB() As String
    ImPathDB = m_ImPathDB
End Property

'IMTIPODB
Public Property Let ImTipoDB(mVar As String)
    m_ImTipoDB = mVar
End Property

Public Property Get ImTipoDB() As String
    ImTipoDB = m_ImTipoDB
End Property

'IMTIPOMIGRAZIONE
Public Property Let ImTipoMigrazione(mVar As String)
    m_ImTipoMigrazione = mVar
End Property

Public Property Get ImTipoMigrazione() As String
    ImTipoMigrazione = m_ImTipoMigrazione
End Property

'IMPATHDEF
Public Property Let ImPathDef(mVar As String)
    m_ImPathDef = mVar
End Property

Public Property Get ImPathDef() As String
    ImPathDef = m_ImPathDef
End Property

'IMIMGDIR
Public Property Let ImImgDir(mVar As String)
    m_ImImgDir = mVar
End Property

Public Property Get ImImgDir() As String
    ImImgDir = m_ImImgDir
End Property

'IMPARENT
Public Property Let ImParent(mVar As Long)
    m_ImParent = mVar
End Property

Public Property Get ImParent() As Long
    ImParent = m_ImParent
End Property

'IMID
Public Property Let ImId(mVar As Integer)
    m_ImId = mVar
End Property

Public Property Get ImId() As Integer
    ImId = m_ImId
End Property

'IMLABEL
Public Property Let ImLabel(mVar As String)
    m_ImLabel = mVar
End Property

Public Property Get ImLabel() As String
    ImLabel = m_ImLabel
End Property

'IMTOOLTIPTEXT
Public Property Let ImToolTiptext(mVar As String)
    m_ImToolTiptext = mVar
End Property

Public Property Get ImToolTiptext() As String
    ImToolTiptext = m_ImToolTiptext
End Property

'IMPICTURE
Public Property Let ImPicture(mVar As String)
    m_ImPicture = mVar
End Property

Public Property Get ImPicture() As String
    ImPicture = m_ImPicture
End Property

'IMPICTUREEN
Public Property Let ImPictureEn(mVar As String)
    m_ImPictureEn = mVar
End Property

Public Property Get ImPictureEn() As String
    ImPictureEn = m_ImPictureEn
End Property

'IMM1NAME
Public Property Let ImM1Name(mVar As String)
    m_ImM1Name = mVar
End Property

Public Property Get ImM1Name() As String
    ImM1Name = m_ImM1Name
End Property

'IMM1SUBNAME
Public Property Let ImM1SubName(mVar As String)
    m_ImM1SubName = mVar
End Property

Public Property Get ImM1SubName() As String
    ImM1SubName = m_ImM1SubName
End Property

'IMM1LEVEL
Public Property Let ImM1Level(mVar As Long)
    m_ImM1Level = mVar
End Property

Public Property Get ImM1Level() As Long
    ImM1Level = m_ImM1Level
End Property

'IMM2NAME
Public Property Let ImM2Name(mVar As String)
    m_ImM2Name = mVar
End Property

Public Property Get ImM2Name() As String
    ImM2Name = m_ImM2Name
End Property

'IMM3NAME
Public Property Let ImM3Name(mVar As String)
    m_ImM3Name = mVar
End Property

Public Property Get ImM3Name() As String
    ImM3Name = m_ImM3Name
End Property

'IMM3BUTTONTYPE
Public Property Let ImM3ButtonType(mVar As String)
    m_ImM3ButtonType = mVar
End Property

Public Property Get ImM3ButtonType() As String
    ImM3ButtonType = m_ImM3ButtonType
End Property

'IMM3SUBNAME
Public Property Let ImM3SubName(mVar As String)
    m_ImM3SubName = mVar
End Property

Public Property Get ImM3SubName() As String
    ImM3SubName = m_ImM3SubName
End Property

'IMM4NAME
Public Property Let ImM4Name(mVar As String)
    m_ImM4Name = mVar
End Property

Public Property Get ImM4Name() As String
    ImM4Name = m_ImM4Name
End Property

'IMM4BUTTONTYPE
Public Property Let ImM4ButtonType(mVar As String)
    m_ImM4ButtonType = mVar
End Property

Public Property Get ImM4ButtonType() As String
    ImM4ButtonType = m_ImM4ButtonType
End Property

'IMM4SUBNAME
Public Property Let ImM4SubName(mVar As String)
    m_ImM4SubName = mVar
End Property

Public Property Get ImM4SubName() As String
    ImM4SubName = m_ImM4SubName
End Property

'IMFUNZIONE
Public Property Let ImFunzione(mVar As String)
    m_ImFunzione = mVar
End Property

Public Property Get ImFunzione() As String
    ImFunzione = m_ImFunzione
End Property

'IMDLLNAME
Public Property Let ImDllName(mVar As String)
    m_ImDllName = mVar
End Property

Public Property Get ImDllName() As String
    ImDllName = m_ImDllName
End Property

'IMTIPOFINIM
Public Property Let ImTipoFinIm(mVar As String)
    m_ImTipoFinIm = mVar
End Property

Public Property Get ImTipoFinIm() As String
    ImTipoFinIm = m_ImTipoFinIm
End Property

'IMCOLLECTIONPARAM
Public Property Set ImCollectionParam(mVar As Collection)
   Set m_ImCollectionParam = mVar
End Property

Public Property Get ImCollectionParam() As Collection
   Set ImCollectionParam = m_ImCollectionParam
End Property

'IMASTERIXFORDELETE
Public Property Let ImAsterixForDelete(mVar As String)
    m_ImAsterixForDelete = mVar
End Property

Public Property Get ImAsterixForDelete() As String
    ImAsterixForDelete = m_ImAsterixForDelete
End Property

'IMASTERIXFORWHERE
Public Property Let ImAsterixForWhere(mVar As String)
    m_ImAsterixForWhere = mVar
End Property

Public Property Get ImAsterixForWhere() As String
    ImAsterixForWhere = m_ImAsterixForWhere
End Property

'IMPROVIDER
Public Property Let ImProvider(mVar As e_Provider)
    m_ImProvider = mVar
End Property

Public Property Get ImProvider() As e_Provider
    ImProvider = m_ImProvider
End Property

'IMIDOGGETTO_ADD
Public Property Let ImIdOggetto_Add(mVar As Long)
    m_ImIdOggetto_Add = mVar
End Property

Public Property Get ImIdOggetto_Add() As Long
    ImIdOggetto_Add = m_ImIdOggetto_Add
End Property

'IMNOMEPRODOTTO
Public Property Let ImNomeProdotto(mVar As String)
    m_ImNomeProdotto = mVar
End Property

Public Property Get ImNomeProdotto() As String
    ImNomeProdotto = m_ImNomeProdotto
End Property

'IMUNITDEF
Public Property Let ImUnitDef(mVar As String)
    m_ImUnitDef = mVar
End Property

Public Property Get ImUnitDef() As String
    ImUnitDef = m_ImUnitDef
End Property

'IMMENUICON
Public Property Let ImMenuIcon(mVar As String)
    m_ImMenuIcon = mVar
End Property

Public Property Get ImMenuIcon() As String
    ImMenuIcon = m_ImMenuIcon
End Property



