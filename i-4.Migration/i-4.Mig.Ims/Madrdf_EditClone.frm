VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Madrdf_EditClone 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Add Clone"
   ClientHeight    =   10515
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   9855
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10515
   ScaleWidth      =   9855
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      Caption         =   "Decoding Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6735
      Left            =   60
      TabIndex        =   8
      Top             =   120
      Width           =   9735
      Begin VB.TextBox txtNumPcb 
         Height          =   315
         Left            =   7440
         TabIndex        =   38
         Top             =   240
         Width           =   435
      End
      Begin VB.ComboBox cmbInstr 
         Height          =   315
         Left            =   1425
         Style           =   2  'Dropdown List
         TabIndex        =   27
         Top             =   240
         Width           =   1095
      End
      Begin VB.ComboBox cmbDBD 
         Height          =   315
         Left            =   3465
         Style           =   2  'Dropdown List
         TabIndex        =   26
         Top             =   240
         Width           =   2715
      End
      Begin VB.TextBox txtPCBSeq 
         Height          =   315
         Left            =   8805
         TabIndex        =   25
         Top             =   240
         Width           =   435
      End
      Begin VB.Frame Frame1 
         Caption         =   "Levels"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3495
         Left            =   120
         TabIndex        =   17
         Top             =   600
         Width           =   9495
         Begin VB.CommandButton cmdDelLevel 
            Caption         =   "Delete Level"
            Enabled         =   0   'False
            Height          =   375
            Left            =   8280
            Style           =   1  'Graphical
            TabIndex        =   34
            ToolTipText     =   "Add New Level"
            Top             =   1080
            Width           =   1035
         End
         Begin VB.TextBox txtLevel 
            Height          =   315
            Left            =   1080
            TabIndex        =   32
            Top             =   840
            Width           =   435
         End
         Begin VB.CommandButton cmdAddLevel 
            Caption         =   "Add Level"
            Enabled         =   0   'False
            Height          =   375
            Left            =   8280
            Style           =   1  'Graphical
            TabIndex        =   31
            ToolTipText     =   "Add New Level"
            Top             =   240
            Width           =   1035
         End
         Begin VB.ComboBox cmbSegment 
            BeginProperty DataFormat 
               Type            =   1
               Format          =   ""
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1040
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Left            =   1080
            Style           =   2  'Dropdown List
            TabIndex        =   21
            Top             =   420
            Width           =   2955
         End
         Begin VB.ComboBox cmbCommCode 
            Height          =   315
            Left            =   3180
            Style           =   2  'Dropdown List
            TabIndex        =   20
            Top             =   840
            Width           =   855
         End
         Begin VB.CheckBox chkArea 
            Alignment       =   1  'Right Justify
            Caption         =   "Id-Area"
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   5160
            TabIndex        =   19
            Top             =   480
            Width           =   1035
         End
         Begin VB.CommandButton cmdSaveLevel 
            Caption         =   "Save Level"
            Height          =   375
            Left            =   8280
            Style           =   1  'Graphical
            TabIndex        =   18
            ToolTipText     =   "Add New Level"
            Top             =   660
            Width           =   1035
         End
         Begin MSComctlLib.ListView lswLevel 
            Height          =   1770
            Left            =   120
            TabIndex        =   22
            Top             =   1560
            Width           =   9225
            _ExtentX        =   16272
            _ExtentY        =   3122
            View            =   3
            LabelEdit       =   1
            Sorted          =   -1  'True
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            SmallIcons      =   "ImageList1"
            ForeColor       =   12582912
            BackColor       =   -2147483624
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Level"
               Object.Width           =   1765
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Segment"
               Object.Width           =   1411
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Comm. Code"
               Object.Width           =   1766
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   2
               SubItemIndex    =   3
               Text            =   "Qual."
               Object.Width           =   31751
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   2
               SubItemIndex    =   4
               Text            =   "Id-Area"
               Object.Width           =   0
            EndProperty
         End
         Begin VB.Label Label11 
            Caption         =   "Level:"
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   180
            TabIndex        =   33
            Top             =   900
            Width           =   675
         End
         Begin VB.Label Label5 
            Caption         =   "Segment:"
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   180
            TabIndex        =   24
            Top             =   480
            Width           =   675
         End
         Begin VB.Label Label6 
            Caption         =   "Command Code:"
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   1920
            TabIndex        =   23
            Top             =   900
            Width           =   1215
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Keys"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2355
         Left            =   120
         TabIndex        =   9
         Top             =   4200
         Width           =   9495
         Begin VB.CommandButton CmdDelKey 
            Caption         =   "Delete Key"
            Enabled         =   0   'False
            Height          =   375
            Left            =   2280
            TabIndex        =   37
            Top             =   1800
            Width           =   1035
         End
         Begin VB.CommandButton CmdAddKey 
            Caption         =   "Add Key"
            Enabled         =   0   'False
            Height          =   375
            Left            =   2280
            TabIndex        =   36
            Top             =   840
            Width           =   1035
         End
         Begin VB.CommandButton cmdSaveKey 
            Caption         =   "Save Key"
            Enabled         =   0   'False
            Height          =   375
            Left            =   2280
            Style           =   1  'Graphical
            TabIndex        =   35
            ToolTipText     =   "Add New Key"
            Top             =   1320
            Width           =   1035
         End
         Begin VB.ComboBox cmbKey 
            Enabled         =   0   'False
            Height          =   315
            Left            =   900
            Style           =   2  'Dropdown List
            TabIndex        =   12
            Top             =   480
            Width           =   2475
         End
         Begin VB.ComboBox cmbOp 
            Enabled         =   0   'False
            Height          =   315
            Left            =   900
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   1080
            Width           =   1095
         End
         Begin VB.ComboBox cmbBool 
            Enabled         =   0   'False
            Height          =   315
            Left            =   900
            Style           =   2  'Dropdown List
            TabIndex        =   10
            Top             =   1680
            Width           =   1095
         End
         Begin MSComctlLib.ListView lswKey 
            Height          =   1890
            Left            =   3540
            TabIndex        =   13
            Top             =   300
            Width           =   5805
            _ExtentX        =   10239
            _ExtentY        =   3334
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   -2147483624
            BorderStyle     =   1
            Appearance      =   1
            Enabled         =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Key"
               Object.Width           =   1765
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Operator"
               Object.Width           =   1411
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Boolean"
               Object.Width           =   1766
            EndProperty
         End
         Begin VB.Label Label7 
            Caption         =   "Key:"
            Enabled         =   0   'False
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   180
            TabIndex        =   16
            Top             =   540
            Width           =   375
         End
         Begin VB.Label Label8 
            Caption         =   "Operator:"
            Enabled         =   0   'False
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   180
            TabIndex        =   15
            Top             =   1140
            Width           =   735
         End
         Begin VB.Label Label9 
            Caption         =   "Boolean:"
            Enabled         =   0   'False
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   180
            TabIndex        =   14
            Top             =   1740
            Width           =   675
         End
      End
      Begin VB.Label lblNumPcb 
         Caption         =   "Num. PCB:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   6480
         TabIndex        =   39
         Top             =   300
         Width           =   795
      End
      Begin VB.Label label2 
         Caption         =   "Instruction:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   525
         TabIndex        =   30
         Top             =   300
         Width           =   795
      End
      Begin VB.Label label3 
         Caption         =   "DBD:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   2985
         TabIndex        =   29
         Top             =   300
         Width           =   375
      End
      Begin VB.Label Label4 
         Caption         =   "PCBSeq:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   8025
         TabIndex        =   28
         Top             =   300
         Width           =   675
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   8880
      Picture         =   "Madrdf_EditClone.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   9420
      Width           =   825
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   7920
      Picture         =   "Madrdf_EditClone.frx":1D2A
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   9420
      Width           =   825
   End
   Begin VB.Frame Frame3 
      Caption         =   "Decoding"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   60
      TabIndex        =   1
      Top             =   7140
      Width           =   9735
      Begin VB.TextBox txtCodifica 
         Height          =   315
         Left            =   1020
         TabIndex        =   5
         Top             =   1380
         Width           =   1515
      End
      Begin VB.TextBox txtDecoding 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   1035
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   240
         Width           =   9435
      End
      Begin VB.Label Label10 
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   6960
         TabIndex        =   7
         Top             =   1440
         Width           =   2295
      End
      Begin VB.Label Label1 
         Caption         =   "Instruction:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   180
         TabIndex        =   6
         Top             =   1440
         Width           =   795
      End
   End
   Begin VB.CommandButton cmdGeneraDecode 
      Caption         =   "Generate Decode"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Generate Decoding"
      Top             =   9420
      Width           =   1785
   End
End
Attribute VB_Name = "Madrdf_EditClone"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public IdOggetto As Long
Public idPgm As Long
Public Riga As Long
Private Type tpKey
  key As String
  oper As String
  booleano As String
End Type
Private Type tpSegment
  numLevel As Integer
  nomeSegm As String
  CommCode As String
  Id_Area As Boolean
  Qual As Boolean
  key() As tpKey
End Type
Private Type Decoding
  Instruction As String
  DBD As String
  PCBSeq As Integer
  segment() As tpSegment
End Type
Dim strClone As Decoding

'Private Sub chkQual_Click()
'  Dim listx As ListItem
'
'  If chkQual.value Then
'    Label7.Enabled = True
'    cmbKey.Enabled = True
'    Label8.Enabled = True
'    cmbOp.Enabled = True
'    Label9.Enabled = True
'    cmbBool.Enabled = True
'    cmdSaveKey.Enabled = True
'    lswKey.Enabled = True
'    carica_Chiavi GetIdSegmentoFromNome(cmbSegment.text, cmbDBD.text)
'    Set listx = lswKey.ListItems.Add(, , "")
'    listx.SubItems(1) = ""
'    listx.SubItems(1) = ""
'  Else
'    lswKey.ListItems.Clear
'    Label7.Enabled = False
'    cmbKey.Enabled = False
'    Label8.Enabled = False
'    cmbOp.Enabled = False
'    Label9.Enabled = False
'    cmbBool.Enabled = False
'    cmdSaveKey.Enabled = False
'    lswKey.Enabled = False
'  End If
'End Sub

Private Sub cmbDBD_Click()
  Dim listx As ListItem
  
  If cmbDBD.text <> strClone.DBD Then
    carica_Segmenti GetIdOggettoFromNome(cmbDBD.text)
    strClone.DBD = cmbDBD.text
    If Len(cmbDBD.text) Then
      lswLevel.ListItems.Clear
      txtLevel = "1"
      Set listx = lswLevel.ListItems.Add(, , "1")
      listx.SubItems(1) = ""
      listx.SubItems(2) = ""
      listx.SubItems(3) = "False"
      listx.SubItems(4) = "False"
    End If
  End If
End Sub

Private Sub cmbInstr_Click()
  strClone.Instruction = cmbInstr.text
End Sub

Private Sub CmdAddKey_Click()
Dim listx As ListItem
  'lo facciamo nella save
  ReDim Preserve strClone.segment(UBound(strClone.segment)).key(UBound(strClone.segment(UBound(strClone.segment)).key) + 1)
  'If Not lswKey.ListItems.Count = 1 Then
    Set listx = lswKey.ListItems.Add(, , lswKey.ListItems.Count + 1)
    listx.Selected = True
  'End If
  'lswKey.ListItems.item(lswKey.ListItems.Count).Selected = True
  listx = ""
  listx.SubItems(1) = ""
  listx.SubItems(2) = ""
  cmdSaveKey.Enabled = True
  CmdAddKey.Enabled = False
  AbilitaKeyFrame
'  cmbKey.SelText = " "
'  cmbOp.text = ""
'  cmbBool.text = ""
End Sub

Private Sub cmdAddLevel_Click()
  Dim listx As ListItem
  
  'lo facciamo durante la save
  'ReDim Preserve strClone.segment(UBound(strClone.segment) + 1)
  
  Set listx = lswLevel.ListItems.Add(, , lswLevel.ListItems.Count + 1)
  listx.SubItems(1) = ""
  listx.SubItems(2) = ""
  listx.SubItems(3) = "False"
  listx.SubItems(4) = "False"
  lswLevel.ListItems.item(lswLevel.ListItems.Count).Selected = True
  cmbSegment.text = cmbSegment.list(0)
  cmbCommCode.text = cmbCommCode.list(0)
  'ChkQual.value = False
  chkArea.Value = False
  cmdAddLevel.Enabled = False
  cmdDelLevel.Enabled = False
  cmdSaveLevel.Enabled = True
  txtLevel.text = CInt(txtLevel.text) + 1
  lswKey.ListItems.Clear
  DisabilitaKeyFrame
End Sub

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub CmdDelKey_Click()
' elimino livello dalla strutura
  ReDim Preserve strClone.segment(UBound(strClone.segment)).key(UBound(strClone.segment(UBound(strClone.segment)).key) - 1)
  ' elimino livello dalla lista
  lswKey.ListItems.Remove (lswLevel.ListItems.Count)
  CmdDelKey.Enabled = False
  CmdAddKey.Enabled = True
End Sub

Private Sub cmdDelLevel_Click()
  ' elimino livello dalla strutura
  ReDim Preserve strClone.segment(UBound(strClone.segment) - 1)
  ' elimino livello dalla lista
  lswLevel.ListItems.Remove (lswLevel.ListItems.Count)
  lswKey.ListItems.Clear
  cmdDelLevel.Enabled = False
  txtLevel.text = CInt(txtLevel.text) - 1
  If CInt(txtLevel.text) = 0 Then cmbDBD.Enabled = True
End Sub

Private Sub cmdGeneraDecode_Click()
  Dim Decode As String
  Dim rs As Recordset, i As Integer, j As Integer
  Decode = ""
  
  If Len(cmbDBD.text) Then
    Decode = "<dbd>" & cmbDBD.text & "</dbd>"
  Else
    MsgBox "Please insert a DBD name", vbInformation
    Exit Sub
  End If
  
  If Len(cmbInstr.text) Then
    ' Genera Nome Istruzione
    Decode = Decode & "<istr>" & Left(Trim(cmbInstr.text) & "....", 4) & "</istr>"
    Else
    MsgBox "Please insert a type of instruction", vbInformation
    Exit Sub
  End If
  If UBound(strClone.segment) > 0 Then
    Decode = Decode & "<livelli>"
  End If
  For i = 1 To UBound(strClone.segment)
    Decode = Decode & "<level" & i & ">"
    Decode = Decode & "<segm>"
    Decode = Decode & strClone.segment(i).nomeSegm
    Decode = Decode & "</segm>"
    If strClone.segment(i).Id_Area Then
      Decode = Decode & "<area>AREA</area>"
    End If
    For j = 1 To UBound(strClone.segment(i).key)
      If Not Trim(strClone.segment(i).key(j).key) = "" Then
        Decode = Decode & "<key>"
        Decode = Decode & strClone.segment(i).key(j).key
        Decode = Decode & "</key>"
        Decode = Decode & "<oper>"
        Decode = Decode & strClone.segment(i).key(j).oper
        Decode = Decode & "</oper>"
        If Not Trim(strClone.segment(i).key(j).booleano) = "" And Not j = UBound(strClone.segment(i).key) Then
          Decode = Decode & "<bool>"
          Decode = Decode & strClone.segment(i).key(j).booleano
          Decode = Decode & "</bool>"
        End If
      End If
    Next j
    Decode = Decode & "</level" & i & ">"
  Next i
  If UBound(strClone.segment) > 0 Then
    Decode = Decode & "</livelli>"
  End If
  
  Decode = Decode & "<pcbseq>" & Format(txtPCBSeq, "000") & "</pcbseq>"
  
  txtDecoding = Decode
  
  Set rs = m_fun.Open_Recordset("select codifica from MgDLI_DecodificaIstr where " & _
                                "decodifica = '" & Decode & "'")
  If rs.RecordCount Then
    txtCodifica = rs!Codifica
    Label10 = ""
  Else
    txtCodifica = genera_Codifica(cmbInstr.text)
    Label10 = "NEW INSTRUCTION"
  End If
  rs.Close
End Sub

Private Sub cmdOk_Click()
  Dim risp As Integer
  
  If Len(Trim(txtDecoding)) Then
    risp = MsgBox("Do you want to save Decoding?", vbYesNoCancel + vbInformation, Madrdf_EditClone.Caption)
    If risp = vbYes Then
      If is_AddClone Then
        add_Clone
      Else
        modify_Clone
      End If
    End If
    If risp <> vbCancel Then
      is_AddClone = False
      Unload Me
    End If
  Else
    MsgBox "Wrong Decoding. Clone not saved!", vbOKOnly + vbExclamation, Madrdf_EditClone.Caption
  End If
End Sub

Private Sub cmdSaveKey_Click()
  Dim listx As ListItem
  If cmbKey.text = "" Then
    MsgBox "Please,insert a value for the Key", vbInformation
    Exit Sub
  ElseIf cmbOp.text = "" Then
    MsgBox "Please,insert a value for the Operator", vbInformation
    Exit Sub
  ElseIf Not Trim(cmbBool.text) = "" Then
    'booleano -> chiave deve essere completata
    MsgBox "Add a Key or the boolean operator will be ignored", vbInformation
    CmdAddKey.Enabled = True
    DisabilitaKeyFrame
  Else
    'chiave completa
    CmdAddKey.Enabled = False
    DisabilitaKeyFrame
  End If
  lswKey.SelectedItem = cmbKey.text
  lswKey.SelectedItem.SubItems(1) = cmbOp.text
  lswKey.SelectedItem.SubItems(2) = cmbBool.text
  lswLevel.SelectedItem.SubItems(3) = True
'  lswLevel.SelectedItem.SubItems(3) = IIf(chkQual.value = 1, "True", "False")
'  lswLevel.SelectedItem.SubItems(4) = IIf(chkArea.value = 1, "True", "False")
  

  ReDim Preserve strClone.segment(UBound(strClone.segment)).key(UBound(strClone.segment(UBound(strClone.segment)).key) + 1)
  strClone.segment(UBound(strClone.segment)).key(UBound(strClone.segment(UBound(strClone.segment)).key)).key = cmbKey.text
  strClone.segment(UBound(strClone.segment)).key(UBound(strClone.segment(UBound(strClone.segment)).key)).oper = cmbOp.text
  strClone.segment(UBound(strClone.segment)).key(UBound(strClone.segment(UBound(strClone.segment)).key)).booleano = cmbBool.text
  
  cmdSaveKey.Enabled = False
  CmdDelKey.Enabled = True
End Sub
Private Sub AbilitaKeyFrame()
  Label7.Enabled = True
  Label8.Enabled = True
  Label9.Enabled = True
  cmbKey.Enabled = True
  cmbOp.Enabled = True
  cmbBool.Enabled = True
End Sub
Private Sub DisabilitaKeyFrame()
  Label7.Enabled = False
  Label8.Enabled = False
  Label9.Enabled = False
  cmbKey.Enabled = False
  cmbOp.Enabled = False
  cmbBool.Enabled = False
End Sub
Private Sub cmdSaveLevel_Click()
'  If Int(txtLevel) > lswLevel.ListItems.Count Then
'    MsgBox ""
'    Exit Sub
'  End If
  Dim IsThereAkey As Boolean
  If cmbDBD.text = "" Then
    MsgBox "You need to insert e DBD and a segment to save a new level", vbInformation
    Exit Sub
  ElseIf cmbSegment.text = "" Then
    MsgBox "You need to insert e segment to save a new level", vbInformation
    Exit Sub
  End If
  
  lswLevel.SelectedItem = txtLevel.text
  lswLevel.SelectedItem.SubItems(1) = cmbSegment.text
  lswLevel.SelectedItem.SubItems(2) = cmbCommCode.text
  lswLevel.SelectedItem.SubItems(3) = False 'IIf(ChkQual.value = 1, "True", "False")
  lswLevel.SelectedItem.SubItems(4) = IIf(chkArea.Value = 1, "True", "False")
  
  'Costruisco decodifica sulla mia variabile
  If UBound(strClone.segment) = 0 Then
    'salviamo elementi sopra i livelli e blocchiamo
    'saranno abilitati solo se si cancellano tutti i livelli
    strClone.DBD = cmbDBD.text
    strClone.Instruction = cmbInstr.text
    cmbDBD.Enabled = False
    'cmbInstr.Enabled = False
  End If
  
  'Gestione struttura
  ReDim Preserve strClone.segment(UBound(strClone.segment) + 1)
  strClone.segment(UBound(strClone.segment)).nomeSegm = cmbSegment.text
  strClone.segment(UBound(strClone.segment)).numLevel = UBound(strClone.segment)
  
  ReDim strClone.segment(UBound(strClone.segment)).key(0)
  
  
  If chkArea Then
    strClone.segment(UBound(strClone.segment)).Id_Area = True
  Else
    strClone.segment(UBound(strClone.segment)).Id_Area = False
  End If
  
  'gestione pulsanti
  cmdAddLevel.Enabled = True
  cmdDelLevel.Enabled = True
  cmdSaveLevel.Enabled = False
  'abilitazione maschera chiave
  
  CmdDelKey.Enabled = False
  CmdAddKey.Enabled = True
  '--------------------
 
  '--------------------
  cmdSaveKey.Enabled = False
  
  IsThereAkey = carica_Chiavi(GetIdSegmentoFromNome(cmbSegment.text, cmbDBD.text))
  If IsThereAkey Then
    CmdAddKey.Enabled = True
  Else
    MsgBox "No key to add for this segment", vbInformation
  End If
  
End Sub

Private Sub Form_Load()
  
  lswLevel.ColumnHeaders(1).Width = (lswLevel.Width - 50) / 7
  lswLevel.ColumnHeaders(2).Width = (lswLevel.Width - 50) / 7 * 3
  lswLevel.ColumnHeaders(3).Width = (lswLevel.Width - 50) / 7
  lswLevel.ColumnHeaders(4).Width = (lswLevel.Width - 50) / 7
  lswLevel.ColumnHeaders(5).Width = (lswLevel.Width - 50) / 7
  
  lswKey.ColumnHeaders(1).Width = (lswKey.Width - 50) / 4 * 2
  lswKey.ColumnHeaders(2).Width = (lswKey.Width - 50) / 4
  lswKey.ColumnHeaders(3).Width = (lswKey.Width - 50) / 4
  
  carica_Istruzioni
  carica_DBD
  carica_CommCode
  carica_Operatori
  carica_Booleani
  txtPCBSeq = 1
  strClone.DBD = ""
  ReDim strClone.segment(0)
  DisabilitaKeyFrame
End Sub

Public Sub carica_Istruzioni()
  cmbInstr.Clear
'  cmbInstr.AddItem "APSB"
'  cmbInstr.AddItem "AUTH"
'  cmbInstr.AddItem "CHKP"
'  cmbInstr.AddItem "CHNG"
'  cmbInstr.AddItem "CKPT"
'  cmbInstr.AddItem "CLSE"
  cmbInstr.AddItem "DLET"
'  cmbInstr.AddItem "DPSB"
'  cmbInstr.AddItem "GCMD"
  cmbInstr.AddItem "GHN"
  cmbInstr.AddItem "GHNP"
  cmbInstr.AddItem "GHU"
'  cmbInstr.AddItem "GMSG"
  cmbInstr.AddItem "GN"
  cmbInstr.AddItem "GNP"
  cmbInstr.AddItem "GU"
  cmbInstr.AddItem "POS" 'IRIS-DB
'  cmbInstr.AddItem "GSCD"
'  cmbInstr.AddItem "ICMD"
'  cmbInstr.AddItem "INIT"
'  cmbInstr.AddItem "INQY"
  cmbInstr.AddItem "ISRT"
'  cmbInstr.AddItem "LOG"
'  cmbInstr.AddItem "OPEN"
'  cmbInstr.AddItem "PCB"
'  cmbInstr.AddItem "RCMD"
  cmbInstr.AddItem "REPL"
'  cmbInstr.AddItem "ROLB"
'  cmbInstr.AddItem "ROLL"
'  cmbInstr.AddItem "ROLS"
'  cmbInstr.AddItem "SYNC"
'  cmbInstr.AddItem "TERM"
'  cmbInstr.AddItem "XRST"
'  cmbInstr.AddItem "PURG"
'  cmbInstr.AddItem "SETB"
'  cmbInstr.AddItem "SETO"
'  cmbInstr.AddItem "SETS"
'  cmbInstr.AddItem "SETU"
End Sub

Public Sub carica_CommCode()
  cmbCommCode.Clear
  cmbCommCode.AddItem " "
  cmbCommCode.AddItem "D"
  cmbCommCode.AddItem "F"
  cmbCommCode.AddItem "L"
End Sub

Public Sub carica_Operatori()
  cmbOp.Clear
  cmbOp.AddItem " "
  cmbOp.AddItem "EQ"
  cmbOp.AddItem "GT"
  cmbOp.AddItem "GE"
  cmbOp.AddItem "LT"
  cmbOp.AddItem "LE"
  cmbOp.AddItem "NE"
End Sub

Public Sub carica_Booleani()
  cmbBool.Clear
  cmbBool.AddItem " "
  cmbBool.AddItem "AND"
  cmbBool.AddItem "OR"
End Sub

Public Sub carica_DBD()
  Dim rs As Recordset
  
  cmbDBD.Clear
  Set rs = m_fun.Open_Recordset("select dbd_name from psdli_dbd where to_migrate order by dbd_name")
  Do Until rs.EOF
    cmbDBD.AddItem rs!dbd_name
    rs.MoveNext
  Loop
  rs.Close
End Sub

Public Function genera_Codifica(Istruzione As String) As String
  Dim rs As Recordset
  Dim maxId As Long
  
  Set rs = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
  If rs.EOF Then
    maxId = 1
  Else
    maxId = rs(0) + 1
  End If
  rs.Close
  
  genera_Codifica = Left(Trim(Istruzione) & "....", 4) & Format(maxId, "000000")
End Function

Public Sub carica_Segmenti(idDBD As Long)
  Dim rs As Recordset
  
  cmbSegment.Clear
  cmbSegment.AddItem " "
  'Set rs = m_fun.Open_Recordset("select nome from psdli_segmenti where idoggetto = " & idDBD & " order by nome")
  Set rs = m_fun.Open_Recordset("select nome from psdli_segmenti where idoggetto = " & idDBD & " order by nome")
  Do Until rs.EOF
    cmbSegment.AddItem rs!nome
    rs.MoveNext
  Loop
  rs.Close
End Sub

Public Function carica_Chiavi(idSegment As String) As Boolean
  Dim rs As Recordset
  
  cmbKey.Clear
  cmbKey.AddItem " "
  Set rs = m_fun.Open_Recordset("select nome from psdli_field where idsegmento = " & idSegment)
  Do Until rs.EOF
    carica_Chiavi = True
    cmbKey.AddItem rs!nome
    rs.MoveNext
  Loop
  rs.Close
End Function
Function IsOkPcbIstr(pIdPgm As Long, pIdOggetto As Long, pRiga As Long) As Boolean
Dim rs As Recordset
  Set rs = m_fun.Open_Recordset("select ordPcb from PsDLi_Istruzioni where idPgm = " & pIdPgm & " and IdOggetto = " _
                                 & pIdOggetto & " and Riga = " & pRiga)
  If Not rs.EOF Then
    If Not IsNull(rs!ordPcb) Then
      If Not rs!ordPcb = 0 Then
        IsOkPcbIstr = True
      End If
    End If
  End If
End Function
Sub add_Clone()
  Dim rs As Recordset, tb1 As Recordset, maxId As Long, SwTp As Boolean
  Set rs = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & idPgm)
  If rs.RecordCount > 0 Then
    SwTp = rs!Cics
  Else
    SwTp = False
  End If
  rs.Close
  
  Set rs = m_fun.Open_Recordset("select * from mgdli_istrcodifica where " & _
                                "idpgm = " & idPgm & " and idoggetto = " & IdOggetto & " and " & _
                                "riga = " & Riga & " and decodifica = '" & txtDecoding & "'")
  If rs.RecordCount = 0 Then
    rs.AddNew
  End If
  rs!idPgm = idPgm
  rs!IdOggetto = IdOggetto
  rs!Riga = Riga
  rs!Decodifica = txtDecoding
  rs!Status = "A"
  If IsNumeric(txtNumPcb.text) Then
    rs!PCB = txtNumPcb.text
    If Not IsOkPcbIstr(idPgm, IdOggetto, Riga) Then
      MsgBox "Please insert a pcb value for this instruction in the Corrector. It's necessary to manage pcb molteplicity", vbInformation
    End If
  ElseIf Not Trim(txtNumPcb.text) = "" Then
    MsgBox "Invalid value for pcb: decode saved without pcb number !", vbExclamation
  End If
  rs.Update
  rs.Close
                                
  Set rs = m_fun.Open_Recordset("select * from MgDLI_DecodificaIstr where " & _
                                "codifica = '" & txtCodifica & "' and decodifica = '" & txtDecoding & "'")
  If rs.RecordCount = 0 Then
    Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
    'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
    If tb1.EOF Then
       maxId = 1
    Else
       maxId = tb1(0) + 1
    End If
      
       
    rs.AddNew
    rs!IdIstruzione = maxId
    rs!Codifica = txtCodifica
    rs!Decodifica = txtDecoding
    rs!ImsDB = True
    rs!ImsDC = False
    rs!Cics = False
    rs!nomeRout = m_fun.Genera_Nome_Routine(cmbDBD.text, cmbInstr.text, SwTp)
    
    rs.Update
  End If
  rs.Close
    
  Set rs = m_fun.Open_Recordset("select * from PsDLI_IstrCodifica where " & _
                                "idoggetto = " & IdOggetto & " and idpgm = " & idPgm & " and " & _
                                "riga = " & Riga & " and codifica = '" & txtCodifica & "'")
  If rs.RecordCount = 0 Then
    rs.AddNew
  End If
  rs!idPgm = idPgm
  rs!IdOggetto = IdOggetto
  rs!Riga = Riga
  rs!Codifica = txtCodifica
  rs!nomeRout = ""
  
  rs.Update
  rs.Close
  
  ' Ricarica la lista dei cloni
  'MadrdF_Corrector.lswRighe_ItemClick MadrdF_Corrector.lswRighe.SelectedItem
End Sub

Sub modify_Clone()

End Sub


Private Sub lswKey_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswKey.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswKey.SortOrder = Order
  lswKey.SortKey = key - 1
  lswKey.Sorted = True
End Sub

Private Sub lswLevel_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswLevel.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswLevel.SortOrder = Order
  lswLevel.SortKey = key - 1
  lswLevel.Sorted = True
End Sub

Private Sub lswLevel_ItemClick(ByVal item As MSComctlLib.ListItem)
  'On Error Resume Next
'  txtLevel.text = lswLevel.SelectedItem
'  cmbSegment.text = IIf(Trim(lswLevel.SelectedItem.SubItems(1)) = "", cmbSegment.list(0), lswLevel.SelectedItem.SubItems(1))
'  cmbCommCode.text = IIf(Trim(lswLevel.SelectedItem.SubItems(2)) = "", cmbCommCode.list(0), lswLevel.SelectedItem.SubItems(2))
'  ChkQual.value = IIf(lswLevel.SelectedItem.SubItems(3) = "True", 1, 0)
'  chkArea.value = IIf(lswLevel.SelectedItem.SubItems(4) = "True", 1, 0)
End Sub

Private Sub txtPCBSeq_Change()
  strClone.PCBSeq = txtPCBSeq.text
End Sub
