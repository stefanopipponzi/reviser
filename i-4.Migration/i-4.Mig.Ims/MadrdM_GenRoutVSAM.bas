Attribute VB_Name = "MadrdM_GenRoutVSAM"
Option Explicit
Global GBCodNomeRout As String
Global GBCodNumIstr As String
Global isCicsVsam As Boolean, dotAfterRoutine As Boolean
Dim BlkIni As Integer, BlkFin As Integer, BlkStep As Integer
Dim level As Integer

Dim nomeTRed() As String, nomeTabIndPart As String

Public Function getRoutine_VSAM(RTRoutine As RichTextBox, NewIstr As String) As Boolean
  Dim wIstr As String, wIstruzione As String, xIstruzione As String
  Dim vPos As Double, wfine As Double
  Dim Testo As String
  Dim testo1 As String
  
  
  On Error GoTo ErrorHandler
  isCicsVsam = wIstrCod(totIstr).isCics
  wIstr = wIstrCod(totIstr).istr
  wIstruzione = NewIstr
  xIstruzione = wIstrCod(totIstr).Decodifica
  GBCodNomeRout = Trim(wIstr) & Mid(wIstruzione, 5)
  GBCodNumIstr = Mid(wIstruzione, 5)
  GbTestoFetch = ""
  
  If Not RoutineOnly Then
    ' TAG #CODE# *************************************************************** CODE
    vPos = RTRoutine.find("#CODE#", 1)
    If Not vPos = -1 Then
      vPos = RTRoutine.find(vbCrLf, vPos + 1)
      vPos = vPos + 2
      Testo = vbCrLf & "      *  " & Left(wIstruzione & Space(4), 10) & " - "
      Testo = Testo & DecIstr(xIstruzione) & vbCrLf
      RTRoutine.SelStart = vPos
      RTRoutine.SelLength = 0
      RTRoutine.SelText = Testo
    End If
    
    ' TAG #OPE# *************************************************************** OPE
    vPos = RTRoutine.find("#OPE#", 1)
    If Not vPos = -1 Then
      vPos = RTRoutine.find(vbCrLf, vPos + 1) + 2
    
      Select Case RoutLang
        Case "PLI"
          Testo = insertPLIOpe(wIstruzione, GBCodNomeRout)
        Case Else
          Testo = insertCobolOpe(wIstruzione, GBCodNomeRout)
      End Select
      RTRoutine.SelStart = vPos
      RTRoutine.SelLength = 0
      RTRoutine.SelText = Testo
    End If
    wfine = Len(RTRoutine.text)
  End If
  
  BlkIni = 1
  BlkFin = UBound(MadrdM_GenRout.TabIstr.BlkIstr)
  BlkStep = 1
  
  ' Mauro 17/01/2008 : Nuova gestione Template Routine gi� esistenti
  Dim fileRout As String, Line As String
  Dim numFile As Integer
  fileRout = Dir(Dli2Rdbms.drPathDb & "\input-prj\Template\imsdb\standard\Instructions\" & wIstruzione)
  If Len(fileRout) = 0 Or Not isInstrTemplate Then
    ''''''''''''''''''''
    ' INIZIO ROUTINE
    ''''''''''''''''''''
    If Not RoutLang = "PLI" Then
      Testo = vbCrLf & "      *------------------------------------------------------------*" & vbCrLf & _
                       "      *  Call  :      " & wIstruzione & "  " & vbCrLf & _
                       "      *  Coding:      " & DecIstr(xIstruzione) & vbCrLf & _
                       "      *------------------------------------------------------------*" & vbCrLf
      testo1 = "       9999-ROUT-" & GBCodNomeRout & " SECTION." & vbCrLf
    Else  'PLI
      Testo = vbCrLf & "    /*------------------------------------------------------------*" & vbCrLf & _
                     "      *  Call  :      " & wIstruzione & "  " & vbCrLf & _
                     "      *  Coding:      " & DecIstr(xIstruzione) & vbCrLf & _
                     "      *-------------------------------------------------------------*/" & vbCrLf
      testo1 = " ROUT_" & GBCodNomeRout & ": PROC;" & vbCrLf
    End If
    
    Comment = Testo
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Mauro 04/10/2007 : ATTENZIONE!!!!!!!!!!!!!!
    ' Ho asteriscato tutte le chiamate alle routine PLI
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    dotAfterRoutine = True
    Select Case wIstr
      Case "DLET"
        If MadrdM_GenRout.TabIstr.BlkIstr(1).idtable Then
          Testo = getDLET_VSAM(RTRoutine)
        Else
          m_fun.WriteLog "Error on istruction: " & MadrdM_GenRout.TabIstr.istr & " - routine: " & GBCodNomeRout ' MadrdM_GenRout.TabIstr.BlkIstr(1).nomeRout
          AggLog "Error on istruction: " & MadrdM_GenRout.TabIstr.istr & " - routine: " & GBCodNomeRout, MadrdF_GenRout.RTBErr2
        End If
      Case "ISRT"
        If MadrdM_GenRout.TabIstr.isGsam Then
          Testo = getISRT_GSAM
        Else
          Testo = getISRT_VSAM(RTRoutine)
        End If
      Case "REPL"
        If MadrdM_GenRout.TabIstr.BlkIstr(1).idtable Then
          Testo = getREPL_VSAM(RTRoutine)
          dotAfterRoutine = False
        Else
          m_fun.WriteLog "Error on istruction: " & MadrdM_GenRout.TabIstr.istr & " - routine: " & GBCodNomeRout ' MadrdM_GenRout.TabIstr.BlkIstr(1).nomeRout
          AggLog "Error on istruction: " & MadrdM_GenRout.TabIstr.istr & " - routine: " & GBCodNomeRout, MadrdF_GenRout.RTBErr2
        End If
      Case "GU"
        If Not RoutLang = "PLI" Then
          Testo = Testo & testo1 & getGU_VSAM(RTRoutine, "GU")
        Else 'PLI
          'Testo = Testo & testo1 & getGU_VSAM_PLI(RTRoutine, "GU")
          'Testo = Testo & " END ROUT_" & GBCodNomeRout & ";" & vbCrLf
        End If
      Case "GHU"
        If Not RoutLang = "PLI" Then
          Testo = Testo & testo1 & getGU_VSAM(RTRoutine, "GU")
  '      Else
  '        Testo = Testo & testo1 & getGU_DB2_PLI(RTRoutine, "GU", True)
  '        Testo = Testo & " END ROUT_" & GBCodNomeRout & ";" & vbCrLf
        End If
      Case "GN"
        If MadrdM_GenRout.TabIstr.isGsam Then
          Testo = getGN_GSAM
        Else
          If UBound(MadrdM_GenRout.TabIstr.BlkIstr) Then  'And Len(MadrdM_GenRout.TabIstr.BlkIstr(1).ssaName) > 0 Then
            'stefano: il procseq ora sar� gestito direttamente nella costruisci_where visto
            ' che dobbiamo fare comunque la ricerca per maggiore della chiave precedente.
            'tmp
            'testo = testo & getGU_DB2_old(RTRoutine, "GN")
            'If Len(MadrdM_GenRout.TabIstr.BlkIstr(1).ssaName) > 0 Then
            If Len(MadrdM_GenRout.TabIstr.BlkIstr(1).segment) Then
              If Not RoutLang = "PLI" Then
                Testo = Testo & testo1 & getGN_VSAM(RTRoutine, "GN")
                'stefano: secondo me non serviva mai il punto c'era gi�
                'Testo = Testo & Space(11) & "." & vbCrLf
  '            Else
  '              Testo = Testo & testo1 & getGN_DB2_PLI(RTRoutine, "GN")
  '              Testo = Testo & " END ROUT_" & GBCodNomeRout & ";" & vbCrLf
              End If
  '          Else
  '            If Not RoutLang = "PLI" Then
  '              Testo = Testo & testo1 & getGNsq_DB2(RTRoutine, "GN")
  '            Else
  '              Testo = Testo & testo1 & getGNsq_DB2_PLI(RTRoutine, "GN")
  '            End If
            End If
          Else
  '          If Not RoutLang = "PLI" Then
  '            Testo = Testo & testo1 & getGNsq_DB2(RTRoutine, "GN")
  '          Else
  '            Testo = Testo & testo1 & getGNsq_DB2_PLI(RTRoutine, "GN")
  '          End If
          End If
        End If
      Case "GHN"
        If UBound(MadrdM_GenRout.TabIstr.BlkIstr) Then
          If Len(MadrdM_GenRout.TabIstr.BlkIstr(1).segment) Then
            If Not RoutLang = "PLI" Then
              Testo = Testo & testo1 & getGN_VSAM(RTRoutine, "GN")
  '          Else
  '            Testo = Testo & testo1 & getGN_DB2_PLI(RTRoutine, "GN", True)
  '            Testo = Testo & " END ROUT_" & GBCodNomeRout & ";" & vbCrLf
            End If
          Else
  '          If Not RoutLang = "PLI" Then
  '            Testo = Testo & testo1 & getGNsq_DB2(RTRoutine, "GN", True)
  '          Else
  '            Testo = Testo & testo1 & getGNsq_DB2_PLI(RTRoutine, "GN", True)
  '          End If
          End If
        Else
  '        If Not RoutLang = "PLI" Then
  '          Testo = Testo & testo1 & getGNsq_DB2(RTRoutine, "GN", True)
  '        Else
  '          Testo = Testo & testo1 & getGNsq_DB2_PLI(RTRoutine, "GN", True)
  '        End If
        End If
      Case "GNP"
        If Not RoutLang = "PLI" Then
          If Len(MadrdM_GenRout.TabIstr.BlkIstr(1).segment) Then
            Testo = Testo & testo1 & getGNP_VSAM(RTRoutine, "GNP")
  '        Else
  '          Testo = Testo & testo1 & getGNsq_DB2(RTRoutine, "GNP")
          End If
  '      Else
  '        Testo = Testo & testo1 & getGNP_DB2_PLI(RTRoutine, "GNP")
  '        Testo = Testo & " END ROUT_" & GBCodNomeRout & ";" & vbCrLf
        End If
      Case "GHNP"
        If Not RoutLang = "PLI" Then
          Testo = Testo & testo1 & getGNP_VSAM(RTRoutine, "GNP")
  '      Else
  '        Testo = Testo & testo1 & getGNP_DB2_PLI(RTRoutine, "GNP", True)
  '        Testo = Testo & " END ROUT_" & GBCodNomeRout & ";" & vbCrLf
        End If
      Case "TERM", "PCB", "QRY", "SYNC"
        If Not RoutLang = "PLI" Then
          Testo = Testo & testo1 & getUTIL_DB2(Trim(UCase(wIstr)))
  '      Else
  '        Testo = Testo & testo1 & getUTIL_DB2_PLI(Trim(UCase(wIstr)))
  '        Testo = Testo & " END ROUT_" & GBCodNomeRout & ";" & vbCrLf
        End If
      Case "CLSE"
         Testo = getCLSE_GSAM
      Case "OPEN"
         Testo = getOPEN_GSAM
    End Select
    If Not RoutLang = "PLI" Then
      If dotAfterRoutine Then
        Testo = Testo & Space(11) & "." & vbCrLf
      Else
        Testo = Testo & vbCrLf
      End If
    End If
  Else
    ' Il testo viene preso direttamente dal template
    Testo = ""
    numFile = FreeFile
    Open Dli2Rdbms.drPathDb & "\input-prj\Template\imsdb\standard\Instructions\" & wIstruzione For Input As numFile
    Line Input #numFile, Line
    Testo = Testo & Line
    Do Until EOF(numFile)
      Line Input #numFile, Line
      Testo = Testo & vbCrLf & Line
    Loop
    Close #numFile
    If Not RoutineOnly Then
      Testo = Testo & vbCrLf
    End If
  End If
  
  If Not RoutineOnly Then
    RTRoutine.SelStart = wfine
    RTRoutine.SelLength = 0
    RTRoutine.SelText = Testo
  Else
    RTRoutine.text = Testo & vbCrLf & GbTestoFetch
    RTRoutine.SaveFile MadrdM_GenRout.Percorso & wIstruzione, 1
    If m_fun.FnNomeDB = "Prj-Perot-POC.mty" Then
      Select Case wIstrCod(totIstr).Liv(UBound(wIstrCod(totIstr).Liv)).Segm
        Case "CUSADR", "CUSBRN", "CUSHED", "EMSMSA"
          loadTemplate RTRoutine, MadrdM_GenRout.Percorso & wIstruzione
          RTRoutine.SaveFile MadrdM_GenRout.Percorso & wIstruzione, 1
      End Select
    End If
  End If
     
  GbFileRedOcc = ""  'Attenzione a brasare, questo fa da flag...
    
  Exit Function
ErrorHandler:
  Select Case err.Number
    Case 75
      m_fun.MkDir_API Dli2Rdbms.drPathDb & "\Output-Prj\VSAM"
      m_fun.MkDir_API Dli2Rdbms.drPathDb & "\Output-Prj\VSAM\BtRout"
      m_fun.MkDir_API Dli2Rdbms.drPathDb & "\Output-Prj\VSAM\CxRout"
      m_fun.MkDir_API Dli2Rdbms.drPathDb & "\Output-Prj\VSAM\Cbl"
      m_fun.MkDir_API Dli2Rdbms.drPathDb & "\Output-Prj\VSAM\Cpy"
      Resume
    Case Else
      AggLog "[" & GBCodNomeRout & "] - " & err.Description, MadrdF_GenRout.RTBErr
    'resume
  End Select
End Function

Public Function getGN_VSAM(rtbFile As RichTextBox, tipoistr As String) As String
  Dim k As Long, i As Long, j As Long
  Dim k1 As Integer, indEnd As Integer, indentGNsqKeyOp As Integer, indentGN As Integer, indentGNsq As Integer
  Dim nomet As String, NomePk As String, nomeKey As String, Testo As String
  Dim keySecFlagPrimoLiv As Boolean, keySecFlagUltimoLiv As Boolean
  Dim rsIndPart As Recordset
  Dim instrLevel As MadrdM_GenRout.BlkIstrDli
  Dim qualified As Boolean, isKeyOp As Boolean, keyPrimUguale As Boolean
  Dim isDBDindex As Boolean
  Dim indentkeyop As Integer, strCheckFile As String, strline As String, numFile As Integer
  Dim oper As String, IsCheckParent As Boolean
  Dim NomeTPadre As String, NomeTFiglio As String
  Dim parentTable As String, nomeIndexPadre As String, nomeIndexFiglio As String
  Dim isFatherQual As Boolean, isSunQual As Boolean, isoperG As Boolean
  indentGen = 0
  IstrDb2 = tipoistr
    
  If InStr(MadrdM_GenRout.TabIstr.BlkIstr(1).CodCom, "D") Then
    level = 1
  Else
    level = UBound(MadrdM_GenRout.TabIstr.BlkIstr)
  End If
  
  If Not EmbeddedRoutine Then
    Testo = Space(11) & "MOVE '" & tipoistr & "' TO WK-ISTR" & vbCrLf
  End If
  
  
  If UBound(MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef) > 0 And level > 1 Then
    oper = MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef(1).op
    If oper = "<=" Or oper = "=" Or oper = ">=" Then
      IsCheckParent = True
    End If
  End If
  
  For level = level To UBound(MadrdM_GenRout.TabIstr.BlkIstr)
    '''''''''''''
    'init:
    '''''''''''''
    keySecFlagUltimoLiv = False
        
    'SQ D
    indEnd = level
    instrLevel = MadrdM_GenRout.TabIstr.BlkIstr(indEnd)
    'AC dbindex
    isDBDindex = wIstrCod(totIstr).isDbindex
    
    If InStr(MadrdM_GenRout.TabIstr.BlkIstr(1).CodCom, "D") And level > 1 Then
      'Lettura solo se il precedente livello andato a buon fine
      Testo = Testo & Space(11) & "IF VSC-OK" & vbCrLf
    End If
    
    If Not EmbeddedRoutine Then
      If UBound(MadrdM_GenRout.TabIstr.BlkIstr) Then
        Testo = Testo & Space(11) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
        Testo = Testo & Space(6) & "*" & vbCrLf
      End If
      Testo = Testo & Space(11) & "PERFORM 9000-MANAGE-PCB" & vbCrLf
    End If
    
    If Not indEnd > 0 Then  'SQ - portare fuori
      m_fun.WriteLog "Instruction: " & GBCodNumIstr & " - not converted", "Routines Generation"
      Exit Function
    End If
              
    'diff
    If UBound(instrLevel.KeyDef) Then
      
      qualified = True 'portare dentro la IF
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        For i = 1 To UBound(instrLevel.KeyDef)
          If instrLevel.KeyDef(i).KeyType <> "F" Then
            keySecFlagUltimoLiv = True
        
            Set rsIndPart = m_fun.Open_Recordset( _
                        "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & db & "Tabelle as b " & _
                        "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
                        " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")

            If Not rsIndPart.EOF Then
              If rsIndPart!NomeTab <> instrLevel.Tavola Then
                nomeTabIndPart = rsIndPart!NomeTab
              End If
            End If
            rsIndPart.Close
            isKeyOp = instrLevel.KeyDef(1).op <> ">"
          End If
        Next i
      Else
        keySecFlagUltimoLiv = False
        'operatore gi� per maggiore, non deve creare la doppia declare
        'SP 11/9 : ANCHE PER UGUALE NON DEVE CREARE LA DOPPIA DECLARE, LO DEVE FARE SOLO PER >=!!! (quelli per <, <= li vedremo poi, ma non dovrebbero dare grossi problemi)
        isKeyOp = instrLevel.KeyDef(1).op <> ">="
        'SP 11/9 Flag per forzare fetch a vuoto anche per primaria!!!
        'stefano: 1/9/2007: a quanto pare dopo due anni ancora non funziona.... (c'� la doppia declare)
        keyPrimUguale = instrLevel.KeyDef(1).op = "="
      End If
    Else
      qualified = False
    End If
            
    'diff
    indentGN = 3
    If isKeyOp Then
      indentGNsq = 3
    Else
      indentGNsq = 6
    End If
    indentGNsqKeyOp = 6
      
    Testo = Testo & Space(11) & "INITIALIZE RD-" & instrLevel.segment & vbCrLf
    Testo = Testo & Space(11) & "MOVE LNKDB-SEGLEVEL(" & indEnd & ") TO LNKDB-STKLEV(LNKDB-NUMSTACK)" & vbCrLf & Space(6) & "*" & vbCrLf
    
    'AC 15/11/07 spostato primo giro - tolto indentGNsqKeyOp da indentazione
    
    '**************** PADRE  ***************************
    
    For k = 1 To 1
      ''''''''''''''''''''''''''''Kt = k  '??????????????????????????????????????????????????
      nomet = MadrdM_GenRout.TabIstr.BlkIstr(k).Tavola
      If Trim(nomet) = "" Then
        nomet = "[NO-TABLE-FOR:" & MadrdM_GenRout.TabIstr.BlkIstr(k).segment & "]"
      End If
      NomePk = MadrdM_GenRout.TabIstr.BlkIstr(k).pKey
      Testo = Testo & Space(11) & "MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
      'squalificata solo sull'ultimo livello, imposta la chiave primaria ereditata
      If UBound(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef) = 0 And k = BlkFin Then
        Testo = Testo & Space(11) & "PERFORM SETC-WKEYWKEY" & vbCrLf
        Testo = Testo & Space(11) & "MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO K01-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
      End If
      
      '******************** gestione chiavi PADRE ***********************
      
      For k1 = 1 To UBound(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef)
        If Len(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key) Then
          isFatherQual = True
          If MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).op = ">" Or MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).op = ">=" Then
            isoperG = True
          End If
          'stefano: non sono sicuro, ma non capisco perch� facesse la replace al contrario
          nomeKey = Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key, "_", "-")
          NomePk = nomeKey
          Testo = Testo & Space(11) & "MOVE LNKDB-KEYVALUE(" & Trim(str(k)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
          Testo = Testo & Space(16) & "KRD-" & nomeKey & vbCrLf
          'SP 12/9: indici particolari
          If Len(nomeTabIndPart) And k = indEnd Then
            Testo = Testo & Space(11) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          End If
          Testo = Testo & Space(11) & "PERFORM SETK-WKUTWKEY" & vbCrLf
          If nomeTabIndPart <> "" And k = indEnd Then
            Testo = Testo & Space(11) & "MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'stefano 24/8/07 gestione field
          If MadrdM_GenRout.TabIstr.BlkIstr(k).pKey = MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key Or MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyType = "F" Then
            Testo = Testo & Space(11) & "PERFORM SETC-WKEYWKEY" & vbCrLf
          End If
          If MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyType = "F" Then
            Testo = Testo & Space(11) & "MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).pKey, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).pKey, "_", "-") & vbCrLf
            Testo = Testo & Space(11) & "MOVE KRR-" & Replace(NomePk, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(NomePk, "_", "-") & vbCrLf
            NomePk = MadrdM_GenRout.TabIstr.BlkIstr(k).pKey
          Else
            Testo = Testo & Space(11) & "MOVE KRR-" & Replace(NomePk, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(NomePk, "_", "-") & vbCrLf
          End If
        End If
      Next k1
      '************************** chiavi padre fine ***********************
    Next k
    '***************** FINE PADRE *******************************
    Testo = Testo & Space(6) & "*" & vbCrLf

    'SQ 31-07-06 - C.C. "F"
    If InStr(instrLevel.CodCom, "F") Then
      'Sistemare INDENT!
    Else
      'SQ 10-09-07 C.C. "D"
      'Apriamo sempre il cursore sui livelli precedenti!
      If level = UBound(MadrdM_GenRout.TabIstr.BlkIstr) Then
        Testo = Testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES " & vbCrLf
        Testo = Testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GBCodNumIstr & vbCrLf
      End If
    End If
    
    If qualified Then
      Testo = Testo & Space(14) & "IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) NOT = 'GU' AND 'ISRT'" & vbCrLf
      Testo = Testo & Space(14) & "                          AND 'GN' AND 'REPL' AND 'DLET'" & vbCrLf
      If Not isKeyOp Then
        Testo = Testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
      End If
    Else
      'SP 14/9 problema su GHU+REPL+GHN+REPL
      Testo = Testo & Space(11) & "   IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) NOT = 'GU' AND 'ISRT'" & vbCrLf
      Testo = Testo & Space(11) & "                             AND 'GN' AND 'REPL' AND 'DLET'" & vbCrLf
      Testo = Testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
    End If
        
    'SQ - sto pezzo probabilmente � da tenere fuori ciclo... giusto?
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Segmento "parent":
    'stefanopippo: 15/08/2005: se su chiave secondaria (sempre sul primo livello!) non fa restore (� inutile)
    keySecFlagPrimoLiv = False
    If UBound(MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef) Then
      keySecFlagPrimoLiv = MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef(1).key <> MadrdM_GenRout.TabIstr.BlkIstr(1).pKey
    End If
    If Not keySecFlagPrimoLiv Then
      parentTable = getParentTable(CLng(MadrdM_GenRout.TabIstr.BlkIstr(1).idsegm))  'togliere cast e cambiare function
      If Len(parentTable) Then
        'SEGMENTO NON RADICE: RESTORE
        Testo = Testo & Space(11 + indentGNsq) & "MOVE '" & parentTable & "' TO WK-NAMETABLE" & vbCrLf
        Testo = Testo & Space(11 + indentGNsq) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
      End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If UBound(instrLevel.KeyDef) Then
      'nuovo sistema: si intende squalificata quando lo � sull'ultimo livello
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        'stefano: gestione field per verificare se � un field e non una chiave secondaria
        '!!!!!! attenzione !!!!!
        For i = 1 To UBound(instrLevel.KeyDef)
          If instrLevel.KeyDef(i).KeyType <> "F" Then
            keySecFlagUltimoLiv = True
            Set rsIndPart = m_fun.Open_Recordset( _
                    "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & db & "Tabelle as b " & _
                    "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
                    " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")
            If Not rsIndPart.EOF Then
              If rsIndPart!NomeTab <> instrLevel.Tavola Then
                nomeTabIndPart = rsIndPart!NomeTab
              End If
            End If
            rsIndPart.Close
          End If
        Next i
      Else
        keySecFlagUltimoLiv = False
      End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' RESTORE CHIAVE:
    ' Attenzione: per ottimizzare posso appoggiare il nome della tabella in MadrdM_GenRout.TabIstr.BlkIstr(0)! (valutare)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'For K = 1 To indEnd
    'AC  12/12/2007
    If Not isFatherQual Then
      Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE LOW-VALUE TO KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(1).pKey, "_", "-") & vbCrLf
      Testo = Testo & Space(11 + indentGNsqKeyOp) & " MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(1).pKey, "_", "-") & " TO K01-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
    End If
    
    'AC 15/11/07 per rottura
    If indEnd = 1 Then
      Testo = Testo & Space(11 + indentGNsqKeyOp) & "CONTINUE" & vbCrLf
    End If
    For k = 2 To indEnd
      ''''''''''''''''''''''''''''Kt = k  '??????????????????????????????????????????????????
      nomet = MadrdM_GenRout.TabIstr.BlkIstr(k).Tavola
      If Trim(nomet) = "" Then
        nomet = "[NO-TABLE-FOR:" & MadrdM_GenRout.TabIstr.BlkIstr(k).segment & "]"
      End If
      NomePk = MadrdM_GenRout.TabIstr.BlkIstr(k).pKey
      Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
      'squalificata solo sull'ultimo livello, imposta la chiave primaria ereditata
      If UBound(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef) = 0 And k = BlkFin Then
        Testo = Testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETC-WKEYWKEY" & vbCrLf
        Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE LOW-VALUE TO " & vbCrLf _
                      & Space(11 + indentGNsqKeyOp) & MadrdM_GenRout.TabIstr.BlkIstr(k).Tavola & "-KEY OF KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
        Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO K01-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
      Else
        isSunQual = True
      End If
     
      For k1 = 1 To UBound(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef)
        If Len(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key) Then
          If MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).op = ">" Or MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).op = ">=" Then
            isoperG = True
          End If
          'stefano: non sono sicuro, ma non capisco perch� facesse la replace al contrario
          nomeKey = Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key, "_", "-")
          NomePk = nomeKey
          Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(str(k)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
          Testo = Testo & Space(16 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf
          'SP 12/9: indici particolari
          If Len(nomeTabIndPart) And k = indEnd Then
            Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          End If
          Testo = Testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY" & vbCrLf
          If nomeTabIndPart <> "" And k = indEnd Then
            Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'stefano 24/8/07 gestione field
          If MadrdM_GenRout.TabIstr.BlkIstr(k).pKey = MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key Or MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyType = "F" Then
            Testo = Testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETC-WKEYWKEY" & vbCrLf
            'AC 28/11/2007
            Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE LOW-VALUE TO KRR-" & Replace(NomePk, "_", "-") & vbCrLf
          End If
          If MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyType = "F" Then
            Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).pKey, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).pKey, "_", "-") & vbCrLf
            Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(NomePk, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(NomePk, "_", "-") & vbCrLf
            NomePk = MadrdM_GenRout.TabIstr.BlkIstr(k).pKey
          Else
            Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(NomePk, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(NomePk, "_", "-") & vbCrLf
          End If
        End If
      Next k1
    Next k
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    'diff
    Testo = Testo & Space(14) & "ELSE" & vbCrLf
    If Not isKeyOp Then
      Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE 'O' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
    End If
    'da impostare diversamente in funzione del tipo lettura (qual/squal, operatore, ecc.)
    'per ora usa la primaria (vedi commentone sopra): CAMBIATO: VALE PER TUTTE LE PRIMARIE
    ' MA NON PER LE SECONDARIE, che fanno la fetch a vuoto
    'MODIFICATO ANCORA: ANCHE PER LE SECONDARIE, CHE FANNO COMUNQUE POI LA FETCH A VUOTO
    Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
    If Not keySecFlagUltimoLiv Then
      Testo = Testo & Space(11 + indentGNsqKeyOp) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
    Else
      NomePk = Replace(instrLevel.KeyDef(1).key, "_", "-")
      Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-STKPTGKEY(LNKDB-NUMSTACK) TO " & vbCrLf
      Testo = Testo & Space(16 + indentGNsqKeyOp) & "KRR-" & Replace(NomePk, "_", "-") & vbCrLf
    End If
    Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(NomePk, "_", "-") & " TO K01-" & Replace(NomePk, "_", "-") & vbCrLf
    'SP 9/9 pcb dinamico
    
    'stefano: gestione field: deve sempre includere i field nelle if
    For k1 = 1 To UBound(instrLevel.KeyDef)
      j = k1
      If instrLevel.KeyDef(k1).KeyType = "F" Or k1 > 1 Then
        If Trim(instrLevel.KeyDef(k1).key) <> "" Then
          nomeKey = instrLevel.KeyDef(k1).key
          NomePk = instrLevel.KeyDef(k1).NameAlt
          Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(str(indEnd)) & ", " & Format(j, "#0") & ") TO " & vbCrLf
          Testo = Testo & Space(13 + indentGNsqKeyOp) & "KRD-" & Replace(nomeKey, "_", "-") & vbCrLf
          'SP 12/9: indici particolari
          If nomeTabIndPart <> "" Then
            Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          End If
          Testo = Testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY" & vbCrLf
          If nomeTabIndPart <> "" Then
            Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
          End If
          Testo = Testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(nomeKey, "_", "-") & " TO K" & Format(instrLevel.KeyDef(k1).KeyNum, "00") & "-" & Replace(nomeKey, "_", "-") & vbCrLf
        End If
      End If
    Next k1
    Testo = Testo & Space(11 + indentGN) & "END-IF" & vbCrLf
    
    'UTILIZZO DI ARRAY ANCHE PER CASI "NORMALI"
    ReDim Preserve nomeTRed(1)
    nomeTRed(1) = instrLevel.Tavola
    
    'AC parte cics
    If isCicsVsam Then
      Testo = Testo & Space(11) & "EXEC CICS IGNORE CONDITION NOTFND" & vbCrLf
      Testo = Testo & Space(11) & "END-EXEC" & vbCrLf
    End If
    
    Testo = Testo & Space(6) & "*" & vbCrLf
    
    If Not isCicsVsam Then
      Testo = Testo & Space(11 + indentGN) & "PERFORM " & nomet & "-OPEN" & vbCrLf
      Testo = Testo & Space(11 + indentGN) & "IF VSC-OK" & vbCrLf
    End If
    
    
    Testo = Testo & Space(14 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
    indentkeyop = 3
    
    ''If isFatherQual Then
    'AC altro pezzo che si differenzia per la versione batch e quella cics
    If Not isCicsVsam Then
      'AC 28/11/07 invertiti key equql q key less
      If (isSunQual Or (indEnd = 1 And isFatherQual)) And Not isoperG Then
        Testo = Testo & Space(14 + indentGN + indentkeyop) & "START " & nomet & " KEY EQUAL" & vbCrLf _
                    & Space(14 + indentGN + indentkeyop) & Space(3) & "K01-" & Replace(NomePk, "_", "-") & vbCrLf
      Else
        Testo = Testo & Space(14 + indentGN + indentkeyop) & "START " & nomet & " KEY NOT LESS" & vbCrLf _
                    & Space(14 + indentGN + indentkeyop) & Space(3) & "K01-" & Replace(NomePk, "_", "-") & vbCrLf
      End If
      'AC 29/11/2007
      If indEnd = 1 Then
        Testo = Testo & Space(14 + indentGN + indentkeyop) & "MOVE " & nomet & "-STATUS TO WK-VSAMCODE" & vbCrLf
        If isFatherQual And Not isoperG Then
          Testo = Testo & Space(14 + indentGN + indentkeyop) & "IF VSC-OK" & vbCrLf _
                        & Space(14 + indentGN + indentkeyop) & "   READ " & nomet & " NEXT INTO RR-" & nomet & vbCrLf _
                        & Space(14 + indentGN + indentkeyop) & "   MOVE " & nomet & "-STATUS TO WK-VSAMCODE" & vbCrLf _
                        & Space(14 + indentGN + indentkeyop) & "END-IF" & vbCrLf
        End If
      End If
    Else
      Testo = Testo & Space(14) & "EXEC CICS STARTBR DATASET ('" & nomet & "')" & vbCrLf _
                    & Space(14) & "                  RIDFLD(" & nomet & "-KEY OF K01-" & Replace(NomePk, "_", "-") & ")" & vbCrLf
      If (isSunQual Or indEnd = 1) Then
        Testo = Testo & Space(14) & "                  EQUAL" & vbCrLf
      Else
        Testo = Testo & Space(14) & "                  GTEQ" & vbCrLf
      End If
      Testo = Testo & Space(14) & "END-EXEC" & vbCrLf & vbCrLf
    End If
    Testo = Testo & Space(14 + indentGN) & "ELSE" & vbCrLf
    If Not isCicsVsam Then
      If (isSunQual Or (indEnd = 1 And isFatherQual)) And Not isoperG Then
        Testo = Testo & Space(17 + indentGN) & "START " & nomet & " KEY NOT LESS " & vbCrLf _
                    & Space(17 + indentGN) & Space(3) & "K01-" & Replace(NomePk, "_", "-") & vbCrLf
      Else
        Testo = Testo & Space(17 + indentGN) & "START " & nomet & " KEY EQUAL " & vbCrLf _
                    & Space(17 + indentGN) & Space(3) & "K01-" & Replace(NomePk, "_", "-") & vbCrLf
      End If
      ''If indEnd = 1 And isFatherQual Then
      If indEnd = 1 Then
        ''Testo = Testo & Space(14 + indentGN) & "   IF K01-" & Replace(NomePk, "_", "-") & " NOT = LOW-VALUE" & vbCrLf _
        ''              & Space(14 + indentGN) & "      MOVE " & NomeT & "-STATUS TO WK-VSAMCODE" & vbCrLf
        Testo = Testo & Space(14 + indentGN) & "   MOVE " & nomet & "-STATUS TO WK-VSAMCODE" & vbCrLf
        If isFatherQual And Not isoperG Then
          Testo = Testo & Space(14 + indentGN) & "   IF VSC-OK" & vbCrLf _
                        & Space(14 + indentGN) & "      READ " & nomet & " NEXT INTO RR-" & nomet & vbCrLf _
                        & Space(14 + indentGN) & "      MOVE " & nomet & "-STATUS TO WK-VSAMCODE" & vbCrLf _
                        & Space(14 + indentGN) & "   END-IF" & vbCrLf
        End If
      End If
      Testo = Testo & Space(14 + indentGN) & "END-IF" & vbCrLf _
      & Space(14 + indentGN) & "MOVE " & nomet & "-STATUS TO WK-VSAMCODE" & vbCrLf & vbCrLf
    Else
      Testo = Testo & Space(14 + indentGN) & "EXEC CICS STARTBR DATASET ('" & nomet & "')" & vbCrLf _
                    & Space(14 + indentGN) & "                  RIDFLD(" & nomet & "-KEY OF K01-" & Replace(NomePk, "_", "-") & ")" & vbCrLf
      If (isSunQual Or indEnd = 1) Then
        Testo = Testo & Space(14 + indentGN) & "                  GTEQ" & vbCrLf
      Else
        Testo = Testo & Space(14 + indentGN) & "                  EQUAL" & vbCrLf
      End If
      Testo = Testo & Space(14 + indentGN) & "END-EXEC" & vbCrLf & vbCrLf
                    'Ac prima di gteq
'                    & Space(14 + indentGN) & "                  KEYLENGTH (" & NomeT & "-LUNGHEZZA-KEY)" & vbCrLf _
'                    & Space(14 + indentGN) & "                  GENERIC" & vbCrLf
    End If
                        
    'diff
    If Not keySecFlagUltimoLiv And Not keyPrimUguale Then
      Testo = Testo & Space(17) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
      Testo = Testo & Space(17) & "MOVE " & GBCodNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
    End If
    
    Testo = Testo & Space(14 + indentGN) & "END-IF" & vbCrLf
        
    If InStr(instrLevel.CodCom, "F") Then
      'Sistemare INDENT!
    Else
      'SQ 10-09-07 C.C. "D"
      'Apriamo sempre il cursore sui livelli precedenti!
      If level = UBound(MadrdM_GenRout.TabIstr.BlkIstr) Then
        Testo = Testo & Space(11) & "END-IF" & vbCrLf
      End If
    End If
    Testo = Testo & Space(6) & "*" & vbCrLf
    If isCicsVsam Then
      Testo = Testo & Space(11) & "MOVE EIBRCODE  TO ITERCICS-EIBRCODE" & vbCrLf
      Testo = Testo & Space(11) & "IF ITERCICS-RETURN-NORMAL" & vbCrLf
    Else
      Testo = Testo & Space(11) & "IF VSC-OK" & vbCrLf
    End If
        
    If isCicsVsam Then
      Testo = Testo & Space(11) & "EXEC CICS IGNORE CONDITION NOTFND" & vbCrLf
      Testo = Testo & Space(11) & "END-EXEC" & vbCrLf
    End If
    'Testo = Testo & Space(11) & "   MOVE K01-" & Replace(NomePk, "_", "-") & " TO " & NomeT & "-KEY OF RR-" & NomeT & vbCrLf
    If Not isCicsVsam Then
      Testo = Testo & Space(11) & "   READ " & nomet & " NEXT INTO RR-" & nomet & vbCrLf
      Testo = Testo & Space(11) & "   MOVE " & nomet & "-STATUS TO WK-VSAMCODE" & vbCrLf
      If Not IsCheckParent Then
        'Testo = Testo & Space(11) & "   IF VSC-NOT-FOUND" & vbCrLf
        'AC 21/12/2007
        Testo = Testo & Space(11) & "   IF VSC-EOF" & vbCrLf
        Testo = Testo & Space(11) & "      MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        If Not isCicsVsam Then
          Testo = Testo & Space(11) & "      CLOSE " & nomet & vbCrLf
        End If
        Testo = Testo & Space(11) & "   END-IF" & vbCrLf
      Else
        Testo = Testo & Space(6) & "*    CHECK READ/PARENTAGE:" & vbCrLf
        ' AC copiato da GNP
        ' Mauro 12/11/2007 : Funziona solo se ci sono 2 livelli
        'NomeT = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).Tavola
        'NomePk = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).pKey
        
        NomeTPadre = MadrdM_GenRout.TabIstr.BlkIstr(1).Tavola
        NomeTFiglio = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).Tavola
        nomeIndexFiglio = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).pKey
        NomePk = Replace(MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef(1).key, "_", "-")
        Testo = Testo & Space(14) & "IF K01-" & NomePk & " NOT EQUAL" & vbCrLf
        'Testo = Testo & Space(17) & "KY-" & NomeTPadre & "-KEY OF RR-" & NomeTFiglio & vbCrLf
        Testo = Testo & Space(17) & "KY-" & NomeTPadre & "-KEY OF K01-" & nomeIndexFiglio & vbCrLf
        'Testo = Testo & Space(17) & "OR VSC-NOT-FOUND" & vbCrLf
        'AC 21/12/2007
        Testo = Testo & Space(17) & "OR VSC-EOF" & vbCrLf
        Testo = Testo & Space(17) & "MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        If Not isCicsVsam Then
          Testo = Testo & Space(17) & "CLOSE " & nomet & vbCrLf
        End If
        'Testo = Testo & Space(17) & "SET VSC-NOT-FOUND TO TRUE" & vbCrLf
        Testo = Testo & Space(17) & "SET VSC-EOF TO TRUE" & vbCrLf
        Testo = Testo & Space(14) & "END-IF" & vbCrLf
      End If
    Else
      Testo = Testo & Space(11) & "EXEC CICS READNEXT INTO (RR-" & nomet & ")" & vbCrLf _
                    & Space(11) & "                   DATASET('" & nomet & "')" & vbCrLf _
                    & Space(11) & "                   RIDFLD(" & nomet & "-KEY OF K01-" & Replace(NomePk, "_", "-") & ")" & vbCrLf _
                    & Space(11) & "END-EXEC" & vbCrLf & vbCrLf
       'AC
       '& Space(11) & "                   LENGTH (" & NomeT & "-LUNGHEZZA)" & vbCrLf _
       '& Space(11) & "                   KEYLENGTH (" & NomeT & "-LUNGHEZZA-KEY)" & vbCrLf
      Testo = Testo & Space(11) & "MOVE EIBRCODE  TO ITERCICS-EIBRCODE" & vbCrLf
    End If
    Testo = Testo & Space(11) & "END-IF" & vbCrLf
    Testo = Testo & Space(6) & "*" & vbCrLf
    
    nomet = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).Tavola
    NomePk = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).pKey
    
    If MadrdM_GenRout.TabIstr.BlkIstr(indEnd).idsegm Then
      If isCicsVsam Then
        Testo = Testo & Space(11) & "IF ITERCICS-RETURN-NORMAL" & vbCrLf
      Else
        Testo = Testo & Space(11) & "IF VSC-OK" & vbCrLf
      End If
      Testo = Testo & Space(11) & "   MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
      Testo = Testo & Space(11) & "   MOVE " & GBCodNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
      If Not isDBDindex Then
        Testo = Testo & Space(11) & "   PERFORM " & Replace(nomeTRed(1), "_", "-") & "-TO-RD" & vbCrLf
      Else
        Testo = Testo & Space(11) & "   PERFORM " & Replace(nomeTRed(1), "_", "-") & "-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & "-TO-RD" & vbCrLf
      End If
      Testo = Testo & Space(11) & "   MOVE '" & nomeTRed(1) & "' TO WK-NAMETABLE " & vbCrLf
      Testo = Testo & Space(11) & "   PERFORM SETK-ADLWKEY" & vbCrLf
      'la chiave deve essere salvata anche se non c'era il blocco chiavi;
      'ad esempio per una GN totalmente squalificata, seguita
      'da una GNP, deve essere comunque recuperata la chiave letta con la GN
      'If Len(nomeTabIndPart) Then
      '  Testo = Testo & Space(6) & "*" & Space(7) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
      'Else
      '  Testo = Testo & Space(6) & "*" & Space(7) & "MOVE '" & nomeTRed(1) & "' TO WK-NAMETABLE" & vbCrLf
      'End If
      'Testo = Testo & Space(6) & "*" & Space(7) & "PERFORM SETK-WKEYWKUT" & vbCrLf
      If Len(nomeTabIndPart) Then
        Testo = Testo & Space(14) & "MOVE '" & nomeTRed(1) & "' TO WK-NAMETABLE" & vbCrLf
      End If
      Testo = Testo & Space(14) & "MOVE '" & Format(level, "00") & "' TO LNKDB-PCBSEGLV" & vbCrLf
      Testo = Testo & Space(14) & "MOVE '" & instrLevel.segment & "' TO WK-SEGMENT" & vbCrLf
      'Testo = Testo & Space(11) & "END-IF" & vbCrLf
      
      Testo = Testo & vbCrLf
      'Testo = Testo & Space(11) & "IF WK-VSAMCODE = '00'" & vbCrLf
      Testo = Testo & Space(11) & "   MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
      Testo = Testo & Space(11) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf
      If keySecFlagUltimoLiv Then
        Testo = Testo & Space(11) & "   PERFORM SETA-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
        Testo = Testo & Space(11) & "   MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)" & vbCrLf
        Testo = Testo & Space(11) & "   MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & " TO KRDP-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
        Testo = Testo & Space(11) & "   MOVE KRDP-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & "  TO LNKDB-FDBKEY" & vbCrLf
      End If
      'Testo = Testo & Space(11) & "END-IF" & vbCrLf
    End If
    'Testo = Testo & Space(11) & "IF WK-VSAMCODE = '00'" & vbCrLf
    Testo = Testo & Space(14) & "MOVE RD-" & instrLevel.segment & " TO LNKDB-DATA-AREA(" & UBound(MadrdM_GenRout.TabIstr.BlkIstr) & ")" & vbCrLf
    Testo = Testo & Space(11) & "END-IF" & vbCrLf
  Next level
'''  If Not isCicsVsam Then
'''    numfile = FreeFile
'''    Open m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE_VSAM\COBOL\OPENFILE-VSAM" For Input As numfile
'''    While Not EOF(numfile)
'''      Line Input #numfile, strline
'''      strCheckFile = strCheckFile & vbCrLf & strline
'''    Wend
'''    Close numfile
'''    strCheckFile = Replace(strCheckFile, "<NOMEVSAM>", NomeT)
'''    strCheckFile = Replace(strCheckFile, "<NOMEROUT>", GBCodNomeRout)
'''  End If
  'getGN_VSAM = Testo & strCheckFile
  getGN_VSAM = Testo
End Function

Public Function getGNP_VSAM(rtbFile As RichTextBox, tipoistr As String) As String
  Dim k As Long, i As Long
  Dim k1 As Integer, indEnd As Integer
  Dim nomet As String, NomePk As String, nomeKey As String, Testo As String
  Dim keySecFlagPrimoLiv As Boolean, keySecFlagUltimoLiv As Boolean
  Dim rsIndPart As Recordset
  Dim instrLevel As MadrdM_GenRout.BlkIstrDli
  Dim qualified As Boolean, isDBDindex As Boolean
  Dim strCheckFile As String, strline As String, numFile As Integer
  Dim VSAMPadre As String, VSAMFiglio As String
  Dim keyPadre As String, keyFiglio As String
  Dim indentSQ As Integer
  Dim NomeTkey As String
  
  indentGen = 0
  IstrDb2 = tipoistr
    
  If InStr(MadrdM_GenRout.TabIstr.BlkIstr(1).CodCom, "D") Then
    level = 1
  Else
    level = UBound(MadrdM_GenRout.TabIstr.BlkIstr)
  End If
  
  If Not EmbeddedRoutine Then
    Testo = Space(11) & "MOVE '" & tipoistr & "' TO WK-ISTR" & vbCrLf
  End If
  
  For level = level To UBound(MadrdM_GenRout.TabIstr.BlkIstr)
    '''''''''''''
    'init:
    '''''''''''''
    keySecFlagUltimoLiv = False
        
    indEnd = level
    instrLevel = MadrdM_GenRout.TabIstr.BlkIstr(indEnd)
    isDBDindex = wIstrCod(totIstr).isDbindex
    
    If InStr(MadrdM_GenRout.TabIstr.BlkIstr(1).CodCom, "D") And level > 1 Then
      'Lettura solo se il precedente livello andato a buon fine
      Testo = Testo & Space(11) & "IF VSC-OK" & vbCrLf
    End If
    
    If Not EmbeddedRoutine Then
      If UBound(MadrdM_GenRout.TabIstr.BlkIstr) Then
        Testo = Testo & Space(11) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
      End If
      Testo = Testo & Space(11) & "PERFORM 9000-MANAGE-PCB" & vbCrLf
    End If
    
    If Not indEnd > 0 Then  'SQ - portare fuori
      m_fun.WriteLog "Instruction: " & GBCodNumIstr & " - not converted", "Routines Generation"
      Exit Function
    End If
              
    'diff
    If UBound(instrLevel.KeyDef) Then
      
      qualified = True 'portare dentro la IF
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        For i = 1 To UBound(instrLevel.KeyDef)
          If instrLevel.KeyDef(i).KeyType <> "F" Then
            keySecFlagUltimoLiv = True
        
            Set rsIndPart = m_fun.Open_Recordset( _
                        "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & db & "Tabelle as b " & _
                        "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
                        " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")

            If Not rsIndPart.EOF Then
              If rsIndPart!NomeTab <> instrLevel.Tavola Then
                nomeTabIndPart = rsIndPart!NomeTab
              End If
            End If
            rsIndPart.Close
          End If
        Next i
      Else
        keySecFlagUltimoLiv = False
      End If
    Else
      qualified = False
    End If
      
    Testo = Testo & Space(11) & "INITIALIZE RD-" & instrLevel.segment & vbCrLf
    Testo = Testo & Space(11) & "MOVE LNKDB-SEGLEVEL(" & indEnd & ") TO LNKDB-STKLEV(LNKDB-NUMSTACK)" & vbCrLf & vbCrLf
    Testo = Testo & Space(6) & "*    PARENTAGE: recupero chiave PARENT" & vbCrLf
        
    If UBound(MadrdM_GenRout.TabIstr.BlkIstr) > 0 Then
      If Len(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).pKey) Then
        Testo = Testo & Space(11) & "INITIALIZE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).pKey, "_", "-") & vbCrLf
      Else
        AggLog "###" & wIstrCod(totIstr).Istruzione & "### WITHOUT KEY!!!", MadrdF_GenRout.RTBErr2
      End If
    Else
      AggLog "###" & wIstrCod(totIstr).Istruzione & "### WITHOUT KEY!!!", MadrdF_GenRout.RTBErr2
    End If
    
    'SQ - sto pezzo probabilmente � da tenere fuori ciclo... giusto?
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Segmento "parent":
    'stefanopippo: 15/08/2005: se su chiave secondaria (sempre sul primo livello!) non fa restore (� inutile)
    keySecFlagPrimoLiv = False
    If UBound(MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef) Then
      keySecFlagPrimoLiv = MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef(1).key <> MadrdM_GenRout.TabIstr.BlkIstr(1).pKey
    End If
    If Not keySecFlagPrimoLiv Then 'se squalificata, se primaria o se solo field (= a squalificata per noi)
      'ULTIMO LIVELLO QUALIFICATO: NON CI SARA' RESTORE, QUINDI MI SERVE QUI!
      nomet = getParentTable(CLng(MadrdM_GenRout.TabIstr.BlkIstr(1).idsegm)) 'togliere cast e cambiare function
      NomeTkey = getParentKey(CLng(MadrdM_GenRout.TabIstr.BlkIstr(1).idsegm))
      If Len(nomet) Then
        'SEGMENTO NON RADICE: RESTORE
        Testo = Testo & Space(11) & "MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
        Testo = Testo & Space(11) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
      End If
    End If
        
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' RESTORE CHIAVE:
    ' Attenzione: per ottimizzare posso appoggiare il nome della tabella in TabIstr.BlkIstr(0)! (valutare)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    For k = 1 To indEnd
      ''''''''''''''''''''''''''''Kt = k  '??????????????????????????????????????????????????
      nomet = MadrdM_GenRout.TabIstr.BlkIstr(k).Tavola
      NomeTkey = MadrdM_GenRout.TabIstr.BlkIstr(k).pKey
      If Trim(nomet) = "" Then
        nomet = "[NO-TABLE-FOR:" & MadrdM_GenRout.TabIstr.BlkIstr(k).segment & "]"
      End If
      NomePk = MadrdM_GenRout.TabIstr.BlkIstr(k).pKey
      'Testo = Testo & Space(11 + indentGN) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
      'squalificata solo sull'ultimo livello, imposta la chiave primaria ereditata
      'If UBound(TabIstr.BlkIstr(K).KeyDef) = 0 And K = BlkFin Then
      If UBound(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef) = 0 And k = BlkFin And k = 1 Then
        Testo = Testo & Space(11) & "MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
        Testo = Testo & Space(11) & "PERFORM SETC-WKEYWKEY" & vbCrLf
        Testo = Testo & Space(11) & "MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO K01-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
      End If
      For k1 = 1 To UBound(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef)
        If Len(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key) Then
          nomeKey = Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key, "_", "-")
          'SQ
          'NomePk = TabIstr.BlkIstr(k).KeyDef(k1).NameAlt
          If MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyType <> "F" Then
            NomePk = nomeKey ' perch� lo fa??? non � sempre vero!
          End If
          Testo = Testo & Space(11) & "MOVE LNKDB-KEYVALUE(" & Trim(str(k)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
          Testo = Testo & Space(16) & "KRD-" & nomeKey & vbCrLf
'''          'SP 12/9: indici particolari
'''          If Len(nomeTabIndPart) And K = indEnd Then
'''            Testo = Testo & Space(11 + indentGN) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
'''          End If
          Testo = Testo & Space(11) & "MOVE '" & MadrdM_GenRout.TabIstr.BlkIstr(k).Tavola & "' TO WK-NAMETABLE" & vbCrLf
          Testo = Testo & Space(11) & "PERFORM SETK-WKUTWKEY" & vbCrLf

          'stefano 24/8/07 gestione field
          'stefano 6/10/07 ancora non va bene, basta con le pignolerie
          If MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key = MadrdM_GenRout.TabIstr.BlkIstr(k).pKey Then
            If UBound(MadrdM_GenRout.TabIstr.BlkIstr) > k Then
              Testo = Testo & Space(11) & "MOVE '" & MadrdM_GenRout.TabIstr.BlkIstr(k + 1).Tavola & "' TO WK-NAMETABLE" & vbCrLf
              Testo = Testo & Space(11) & "PERFORM SETC-WKEYWKEY" & vbCrLf
              Testo = Testo & Space(11) & "MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(k + 1).pKey, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(k + 1).pKey, "_", "-") & vbCrLf
            Else
              Testo = Testo & Space(11) & "MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).pKey, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).pKey, "_", "-") & vbCrLf
            End If
          Else

            Testo = Testo & Space(11) & "MOVE KRR-" & Replace(nomeKey, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(nomeKey, "_", "-") & vbCrLf
          End If
        End If
      Next k1
    Next k
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Testo = Testo & Space(6) & "*" & vbCrLf
    
    'diff
    'SQ 31-07-06 - C.C. "F"
    If InStr(instrLevel.CodCom, "F") Then
      'indentGN = 0
      'Sistemare INDENT!
    Else
      Testo = Testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
      Testo = Testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GBCodNumIstr & vbCrLf
      'Indent:
      'indentGN = 3
    End If
    Testo = Testo & Space(11) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
    Testo = Testo & Space(11) & "MOVE " & GBCodNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
    
    Testo = Testo & Space(6) & "*" & vbCrLf
    
    keyPadre = Replace(MadrdM_GenRout.TabIstr.BlkIstr(1).pKey, "_", "-")
    keyFiglio = Replace(MadrdM_GenRout.TabIstr.BlkIstr(2).pKey, "_", "-")
    VSAMPadre = Replace(MadrdM_GenRout.TabIstr.BlkIstr(1).Tavola, "_", "-")
    VSAMFiglio = Replace(MadrdM_GenRout.TabIstr.BlkIstr(2).Tavola, "_", "-")
    

    If Not isCicsVsam Then
      Testo = Testo & Space(14) & "PERFORM " & VSAMFiglio & "-OPEN" & vbCrLf
      
  
      Testo = Testo & Space(14) & "IF VSC-OK" & vbCrLf
      
  
      Testo = Testo & Space(17 + indentSQ) & "START " & VSAMFiglio & " KEY NOT LESS" & vbCrLf
      Testo = Testo & Space(30) & VSAMFiglio & "-KEY OF K01-" & keyFiglio & vbCrLf
      Testo = Testo & Space(17 + indentSQ) & "MOVE " & VSAMFiglio & "-STATUS TO WK-VSAMCODE" & vbCrLf
    Else 'cics
      'AC 03/12/2007
      ' soluzione uno Testo = Testo & Space(11) & "MOVE " & VSAMFiglio& "-KEY OF K01-" & keyFiglio & " TO WK-VSAMTPKEY
      '              e WK-VSAMTPKEY dentro RIDFLD
      ' soluzione due: uso di OF dentro RIDFLD
      
      Testo = Testo & Space(14 + indentSQ) & "EXEC CICS IGNORE CONDITION NOTFND" & vbCrLf
      Testo = Testo & Space(14 + indentSQ) & "END-EXEC" & vbCrLf & vbCrLf
      Testo = Testo & Space(14 + indentSQ) & "EXEC CICS STARTBR DATASET('" & VSAMFiglio & "')" & vbCrLf _
                      & Space(14 + indentSQ) & "                RIDFLD(" & VSAMFiglio & "-KEY OF K01-" & keyFiglio & ")" & vbCrLf _
                      & Space(14 + indentSQ) & "                GTEQ" & vbCrLf _
                      & Space(14 + indentSQ) & "END-EXEC" & vbCrLf & vbCrLf
      Testo = Testo & Space(14 + indentSQ) & "MOVE EIBRCODE  TO ITERCICS-EIBRCODE" & vbCrLf
                'AC prima di gteq
'                & Space(14 + indentSQ) & "                KEYLENGTH (" & VSAMFiglio & "-LUNGHEZZA-KEY)" & vbCrLf _
'                & Space(14 + indentSQ) & "                GENERIC" & vbCrLf
    End If
    Testo = Testo & Space(17) & "END-IF" & vbCrLf

    Testo = Testo & Space(14) & "END-IF" & vbCrLf

    Testo = Testo & Space(6) & "*" & vbCrLf
    
    If isCicsVsam Then
      Testo = Testo & Space(11) & "IF ITERCICS-RETURN-NORMAL " & vbCrLf
    Else
      Testo = Testo & Space(11) & "IF VSC-OK " & vbCrLf
    End If
    
    Testo = Testo & Space(14) & "READ " & VSAMFiglio & " NEXT INTO RR-" & VSAMFiglio & vbCrLf
    Testo = Testo & Space(14) & "MOVE " & VSAMFiglio & "-STATUS TO WK-VSAMCODE" & vbCrLf
    Testo = Testo & Space(6) & "*    CHECK READ/PARENTAGE:" & vbCrLf
    ' Mauro 12/11/2007 : Funziona solo se ci sono 2 livelli
    
    Dim NomeTPadre As String, NomeTFiglio As String
    NomeTPadre = MadrdM_GenRout.TabIstr.BlkIstr(1).Tavola
    NomeTFiglio = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).Tavola
    NomePk = MadrdM_GenRout.TabIstr.BlkIstr(1).pKey
    Testo = Testo & Space(14) & "IF K01-" & NomePk & " NOT EQUAL" & vbCrLf
    Testo = Testo & Space(17) & nomet & "-KEY OF K01-" & NomeTkey & vbCrLf
    Testo = Testo & Space(17) & "OR VSC-NOT-FOUND" & vbCrLf
    Testo = Testo & Space(17) & "MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
    If Not isCicsVsam Then
      Testo = Testo & Space(17) & "CLOSE " & nomet & vbCrLf
    End If
    Testo = Testo & Space(17) & "SET VSC-NOT-FOUND TO TRUE" & vbCrLf
    Testo = Testo & Space(14) & "END-IF" & vbCrLf
    Testo = Testo & Space(11) & "END-IF" & vbCrLf
    Testo = Testo & Space(6) & "*" & vbCrLf
    
    If MadrdM_GenRout.TabIstr.BlkIstr(indEnd).idsegm Then
      If isCicsVsam Then
        Testo = Testo & Space(11) & "IF ITERCICS-RETURN-NORMAL " & vbCrLf
      Else
        Testo = Testo & Space(11) & "IF VSC-OK " & vbCrLf
      End If
      Testo = Testo & Space(11) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
      Testo = Testo & Space(11) & "MOVE " & GBCodNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
      If Not isDBDindex Then
        Testo = Testo & Space(11) & "   PERFORM " & Replace(nomet, "_", "-") & "-TO-RD" & vbCrLf
      Else
        Testo = Testo & Space(11) & "   PERFORM " & Replace(nomet, "_", "-") & "-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & "-TO-RD" & vbCrLf
      End If
      Testo = Testo & Space(11) & "   MOVE '" & nomet & "' TO WK-NAMETABLE " & vbCrLf
      Testo = Testo & Space(11) & "   PERFORM SETK-ADLWKEY" & vbCrLf

      Testo = Testo & Space(14) & "PERFORM SETK-WKEYWKUT" & vbCrLf
      If Len(nomeTabIndPart) Then
        Testo = Testo & Space(14) & "MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
      End If
      Testo = Testo & Space(14) & "MOVE KRDP-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO LNKDB-FDBKEY" & vbCrLf
      Testo = Testo & Space(14) & "MOVE '" & Format(level, "00") & "' TO LNKDB-PCBSEGLV" & vbCrLf
      Testo = Testo & Space(14) & "MOVE '" & nomet & "' TO WK-SEGMENT" & vbCrLf
      Testo = Testo & Space(6) & "*" & vbCrLf
      Testo = Testo & Space(11) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf
      Testo = Testo & Space(6) & "*" & vbCrLf
      If keySecFlagUltimoLiv Then
        Testo = Testo & Space(11) & "   PERFORM SETA-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
        Testo = Testo & Space(11) & "   MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)" & vbCrLf
        Testo = Testo & Space(11) & "   MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & " TO KRDP-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
        Testo = Testo & Space(11) & "   MOVE KRDP-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & "  TO LNKDB-FDBKEY" & vbCrLf
      End If
    End If
    Testo = Testo & Space(14) & "MOVE RD-" & instrLevel.segment & " TO LNKDB-DATA-AREA(" & UBound(MadrdM_GenRout.TabIstr.BlkIstr) & ")" & vbCrLf
    Testo = Testo & Space(11) & "END-IF" & vbCrLf
  Next level
  getGNP_VSAM = Testo
End Function

Public Function getGU_VSAM(rtbFile As RichTextBox, tipoistr As String) As String
  Dim k As Long
  Dim i As Integer, k1 As Integer, indEnd As Integer
  Dim nomet As String, NomePk As String, nomeKey As String, Testo As String
  Dim keySecFlagPrimoLiv As Boolean, keySecFlagUltimoLiv As Boolean
  Dim flagIns As Boolean
  Dim rsIndPart As Recordset
  Dim instrLevel As MadrdM_GenRout.BlkIstrDli
  Dim bolDbindex As Boolean
  Dim strCheckFile As String, strline As String, numFile As Integer
  Dim IsCheckParent As Boolean, oper As String, NomeTPadre As String, NomeTFiglio As String
  Dim nomeIndexFiglio As String
  indentGen = 0
  level = 0
  
  If UBound(MadrdM_GenRout.TabIstr.BlkIstr) Then
    If InStr(MadrdM_GenRout.TabIstr.BlkIstr(1).CodCom, "D") And tipoistr <> "ISRT" Then
      level = 1
    Else
      level = UBound(MadrdM_GenRout.TabIstr.BlkIstr)
    End If
  End If
  
  If Not EmbeddedRoutine Then
    Testo = Space(11) & "MOVE '" & tipoistr & "' TO WK-ISTR" & vbCrLf
  End If
  
  If UBound(MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef) > 0 And level > 1 Then
      oper = MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef(1).op
      If oper = "<=" Or oper = "=" Or oper = ">=" Then
        IsCheckParent = True
      End If
  End If
  
  For level = level To UBound(MadrdM_GenRout.TabIstr.BlkIstr)
    '''''''''''''
    'init:
    '''''''''''''
    keySecFlagUltimoLiv = False
      
    If tipoistr <> "ISRT" Then
      indEnd = level
      instrLevel = MadrdM_GenRout.TabIstr.BlkIstr(indEnd)
      bolDbindex = wIstrCod(totIstr).isDbindex
      IstrDb2 = tipoistr
      
      If Not EmbeddedRoutine Then
        Testo = Testo & Space(11) & "INITIALIZE RD-" & instrLevel.segment & vbCrLf
      End If
      If level > 1 And InStr(MadrdM_GenRout.TabIstr.BlkIstr(1).CodCom, "D") Then
        'Lettura solo se il precedente livello andato a buon fine
        Testo = Testo & Space(11) & "IF VSC-OK" & vbCrLf
      End If
                  
      If Not EmbeddedRoutine Then
        If UBound(MadrdM_GenRout.TabIstr.BlkIstr) Then
          Testo = Testo & Space(11) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
        End If
        If UBound(instrLevel.KeyDef) Then
          If Not instrLevel.KeyDef(1).op = "=" Then
            Testo = Testo & Space(11) & "MOVE '" & instrLevel.segment & "' TO WK-SEGMENT" & vbCrLf
          End If
        End If
'        Testo = Testo & Space(6) & "*" & vbCrLf
'        If Not isCicsVsam Then
'          Testo = Testo & Space(11) & "PERFORM 9999-" & GBCodNomeRout & "-OPEN-FILE" & vbCrLf
'        End If
        Testo = Testo & Space(6) & "*" & vbCrLf
        Testo = Testo & Space(11) & "PERFORM 9000-MANAGE-PCB" & vbCrLf
      End If
        
      flagIns = False
    Else
      indEnd = UBound(MadrdM_GenRout.TabIstr.BlkIstr) - 1
      instrLevel = MadrdM_GenRout.TabIstr.BlkIstr(indEnd)
      Testo = Testo & Space(11) & "INITIALIZE RD-" & instrLevel.segment & vbCrLf  'SQ - Ci vuole?
      flagIns = True
    End If
    
    If Not indEnd > 0 Then
      AggLog "[" & wIstrCod(totIstr).Istruzione & "] Instruction not converted: GU totally unqualified.", MadrdF_GenRout.RTBErr
      Exit Function
    End If
    Testo = Testo & Space(11) & "MOVE LNKDB-SEGLEVEL(" & indEnd & ") TO LNKDB-STKLEV(LNKDB-NUMSTACK)" & vbCrLf
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    keySecFlagPrimoLiv = False
    If UBound(MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef) Then
      keySecFlagPrimoLiv = MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef(1).key <> MadrdM_GenRout.TabIstr.BlkIstr(1).pKey
    End If
    If Not keySecFlagPrimoLiv Then
      nomet = getParentTable(CLng(MadrdM_GenRout.TabIstr.BlkIstr(1).idsegm))
      If Len(nomet) Then
        'SEGMENTO NON RADICE: RESTORE
        Testo = Testo & Space(11) & "MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
        Testo = Testo & Space(11) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
      End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If UBound(instrLevel.KeyDef) Then
      'nuovo sistema: si intende squalificata quando lo � sull'ultimo livello
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        keySecFlagUltimoLiv = True
        'SQ attenzione!!!!!!!!!!!! PROBABILEMENTE CI VA LIKE %AUTOMATIC% !!!!!!!!!!!!!!!!
        Set rsIndPart = m_fun.Open_Recordset( _
          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & db & "Tabelle as b " & _
          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
          " and b.idObjSource = " & mIdDBDCorrente & " and b.TAG = 'AUTOMATIC'")

        If Not rsIndPart.EOF Then
          If rsIndPart!NomeTab <> instrLevel.Tavola Then
            nomeTabIndPart = rsIndPart!NomeTab
          End If
        End If
        rsIndPart.Close
      Else
        keySecFlagUltimoLiv = False
      End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' RESTORE CHIAVE:
    ' Attenzione: per ottimizzare posso appoggiare il nome della tabella in MadrdM_GenRout.TabIstr.BlkIstr(0)! (valutare)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    For k = 1 To indEnd
      nomet = MadrdM_GenRout.TabIstr.BlkIstr(k).Tavola
      If Trim(nomet) = "" Then
        nomet = "[NO-TABLE-FOR:" & MadrdM_GenRout.TabIstr.BlkIstr(k).segment & "]"
      End If
      NomePk = MadrdM_GenRout.TabIstr.BlkIstr(k).pKey
      If Not EmbeddedRoutine Then
        Testo = Testo & Space(11) & "MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
      End If
      'squalificata solo sull'ultimo livello, imposta la chiave primaria ereditata
      If UBound(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef) = 0 And k = BlkFin Then
        Testo = Testo & Space(11) & "PERFORM SETC-WKEYWKEY" & vbCrLf
        Testo = Testo & Space(11) & "MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO K01-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
      End If
      For k1 = 1 To UBound(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef)
        If Len(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key) Then
          nomeKey = Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key, "_", "-")
          NomePk = nomeKey
          Testo = Testo & Space(11) & "MOVE LNKDB-KEYVALUE(" & Trim(str(k)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
          Testo = Testo & Space(16) & "KRD-" & nomeKey & vbCrLf
          If Len(nomeTabIndPart) And k = indEnd Then
            Testo = Testo & Space(11) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          End If
          Testo = Testo & Space(11) & "PERFORM SETK-WKUTWKEY" & vbCrLf
          If nomeTabIndPart <> "" And k = indEnd Then
            Testo = Testo & Space(11) & "MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
          End If
          If MadrdM_GenRout.TabIstr.BlkIstr(k).pKey = MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).key Or MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyType = "F" Then
            Testo = Testo & Space(11) & "PERFORM SETC-WKEYWKEY" & vbCrLf
          End If
          If MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyType = "F" Then
            Testo = Testo & Space(11) & "MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).pKey, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(k).pKey, "_", "-") & vbCrLf
          End If
          Testo = Testo & Space(11) & "MOVE KRR-" & Replace(NomePk, "_", "-") & " TO K" & Format(MadrdM_GenRout.TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & Replace(NomePk, "_", "-") & vbCrLf
        End If
      Next k1
    Next k
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    'UTILIZZO DI ARRAY ANCHE PER CASI "NORMALI"
    ReDim Preserve nomeTRed(1)
    nomeTRed(1) = instrLevel.Tavola
    If Not isCicsVsam Then
      Testo = Testo & Space(11) & "PERFORM " & instrLevel.Tavola & "-OPEN." & vbCrLf
      If UBound(instrLevel.KeyDef) Then
        'AC 21/11/07 da RR instr.tavola  a K01 instr.pKey
        Select Case instrLevel.KeyDef(1).op
          Case "="
            ' non facciamo la start
          Case ">"
            Testo = Testo & Space(11) & "START " & instrLevel.Tavola & " KEY GREATER THAN K01-" & instrLevel.pKey & vbCrLf
            Testo = Testo & Space(11) & "MOVE " & instrLevel.Tavola & "-STATUS TO WK-VSAMCODE" & vbCrLf
          Case ">="
            Testo = Testo & Space(11) & "START " & instrLevel.Tavola & " KEY NOT LESS K01-" & instrLevel.pKey & vbCrLf
            Testo = Testo & Space(11) & "MOVE " & instrLevel.Tavola & "-STATUS TO WK-VSAMCODE" & vbCrLf
          Case Else
            Testo = Testo & Space(11) & "START " & instrLevel.Tavola & " KEY EQUAL K01-" & instrLevel.pKey & vbCrLf
            Testo = Testo & Space(11) & "MOVE " & instrLevel.Tavola & "-STATUS TO WK-VSAMCODE" & vbCrLf
        End Select
        
      Else ' AC questa dovrebbe essere la versione squalificata
        Testo = Testo & Space(11) & "MOVE LOW-VALUE TO " & instrLevel.Tavola & "-KEY OF K01-" & instrLevel.pKey & vbCrLf
        Testo = Testo & Space(11) & "START " & instrLevel.Tavola & " KEY NOT LESS  K01-" & instrLevel.pKey & vbCrLf
        Testo = Testo & Space(11) & "MOVE " & instrLevel.Tavola & "-STATUS TO WK-VSAMCODE" & vbCrLf
      End If
    Else
      '*********************************************************************
    
      ' parte cics
    End If
    
    If isCicsVsam Then
    ' parte CICS
      
      Testo = Testo & Space(11) & "EXEC CICS IGNORE CONDITION NOTFND" & vbCrLf
      Testo = Testo & Space(11) & "END-EXEC" & vbCrLf
                    
      Testo = Testo & Space(11) & "EXEC CICS READNEXT INTO(RR-" & nomet & ")" & vbCrLf _
                    & Space(11) & "                   DATASET('" & nomet & "')" & vbCrLf _
                    & Space(11) & "                   RIDFLD(" & nomet & "-KEY OF K01-" & instrLevel.pKey & ")" & vbCrLf _
                    & Space(11) & "END-EXEC." & vbCrLf & vbCrLf
      'AC prima di end exec
                     '& Space(11) & "                   LENGTH    (" & NomeT & "-LUNGHEZZA)" & vbCrLf _
                    '& Space(11) & "                   KEYLENGTH (" & NomeT & "-LUNGHEZZA-KEY)" & vbCrLf _
      'AC 22/11/07
      Testo = Testo & Space(11) & "MOVE EIBRCODE  TO ITERCICS-EIBRCODE" & vbCrLf
    Else ' parte batch
      Testo = Testo & Space(11) & "IF VSC-OK" & vbCrLf
      If UBound(instrLevel.KeyDef) Then
        If instrLevel.KeyDef(1).op = "=" Then  ' read secca
          Testo = Testo & Space(11) & "   READ " & nomet & " INTO RR-" & instrLevel.segment & vbCrLf
        Else        ' read next (in precedenza per questo caso ho fatto la start)
          Testo = Testo & Space(11) & "   READ " & nomet & " NEXT INTO RR-" & instrLevel.segment & vbCrLf
        End If
      Else ' ramo squalificata -> read next
        Testo = Testo & Space(11) & "   READ " & nomet & " NEXT INTO RR-" & instrLevel.segment & vbCrLf
      End If
      ' comune a tutte le read
      Testo = Testo & Space(11) & "   MOVE " & nomet & "-STATUS TO WK-VSAMCODE" & vbCrLf
      'Testo = Testo & Space(11) & "END-IF" & vbCrLf & vbCrLf
    End If
    'esce se richiamata dalla insert: ATTENZIONE: PROBABILMENTE E' UN "NEXT FOR"!!!!!
    If flagIns Then
      getGU_VSAM = Testo
      Exit Function
    End If
     
    nomet = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).Tavola
    NomePk = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).pKey
    If MadrdM_GenRout.TabIstr.BlkIstr(indEnd).idsegm Then
      'Ac 22/11/07 adesso per la versione testo ITERCICS-EIBRCODE
      If isCicsVsam Then
        Testo = Testo & Space(11) & "IF ITERCICS-RETURN-NORMAL" & vbCrLf
      Else
        Testo = Testo & Space(11) & "IF VSC-OK" & vbCrLf
      End If
      'AC 21/12/2007
      If IsCheckParent Then

        Testo = Testo & Space(6) & "*    CHECK READ/PARENTAGE:" & vbCrLf
        ' AC copiato da GNP
        ' Mauro 12/11/2007 : Funziona solo se ci sono 2 livelli
        'NomeT = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).Tavola
        'NomePk = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).pKey
        
        NomeTPadre = MadrdM_GenRout.TabIstr.BlkIstr(1).Tavola
        NomeTFiglio = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).Tavola
        nomeIndexFiglio = MadrdM_GenRout.TabIstr.BlkIstr(indEnd).pKey
        NomePk = Replace(MadrdM_GenRout.TabIstr.BlkIstr(1).KeyDef(1).key, "_", "-")
        Testo = Testo & Space(14) & "IF K01-" & NomePk & " NOT EQUAL" & vbCrLf
        'Testo = Testo & Space(17) & "KY-" & NomeTPadre & "-KEY OF RR-" & NomeTFiglio & vbCrLf
        Testo = Testo & Space(17) & "KY-" & NomeTPadre & "-KEY OF K01-" & nomeIndexFiglio & vbCrLf
        'Testo = Testo & Space(17) & "OR VSC-NOT-FOUND" & vbCrLf
        'AC 21/12/2007
        Testo = Testo & Space(17) & "OR VSC-EOF" & vbCrLf
        Testo = Testo & Space(17) & "MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        If Not isCicsVsam Then
          Testo = Testo & Space(17) & "CLOSE " & nomet & vbCrLf
        End If
        'Testo = Testo & Space(17) & "SET VSC-NOT-FOUND TO TRUE" & vbCrLf
        Testo = Testo & Space(17) & "SET VSC-NOT-FOUND TO TRUE" & vbCrLf
        Testo = Testo & Space(14) & "END-IF" & vbCrLf
      End If
      
      
      
      
      If Not bolDbindex Then
        Testo = Testo & Space(14) & "   PERFORM " & Replace(nomeTRed(1), "_", "-") & "-TO-RD" & vbCrLf
      Else
        Testo = Testo & Space(14) & "   PERFORM " & Replace(nomeTRed(1), "_", "-") & "-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & "-TO-RD" & vbCrLf
      End If
      Testo = Testo & Space(14) & "   MOVE '" & nomeTRed(1) & "' TO WK-NAMETABLE " & vbCrLf
      Testo = Testo & Space(14) & "   PERFORM SETK-ADLWKEY" & vbCrLf
      'If instrLevel.KeyDef(1).op = "=" Then
        Testo = Testo & Space(17) & "MOVE '" & instrLevel.segment & "' TO WK-SEGMENT" & vbCrLf
      'Else
      '  Testo = Testo & Space(17) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
      '  Testo = Testo & Space(17) & "PERFORM SETK-WKEYWKUT" & vbCrLf
      '  Testo = Testo & Space(17) & "MOVE KRDP-" & NomePk & " TO LNKDB-PCBFDBKEY" & vbCrLf
      'End If
      Testo = Testo & Space(17) & "MOVE '" & Format(level, "00") & "' TO LNKDB-PCBSEGLV" & vbCrLf
      'Testo = Testo & Space(14) & "END-IF" & vbCrLf
      
      Testo = Testo & vbCrLf
      'Testo = Testo & Space(14) & "IF WK-VSAMCODE = '00'" & vbCrLf
      Testo = Testo & Space(14) & "   MOVE '" & nomet & "' TO WK-NAMETABLE" & vbCrLf
      Testo = Testo & Space(14) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf
      If keySecFlagUltimoLiv Then
        Testo = Testo & Space(14) & "   PERFORM SETA-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
        Testo = Testo & Space(14) & "   MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)" & vbCrLf
        If instrLevel.KeyDef(1).op = "=" Then
          Testo = Testo & Space(14) & "   MOVE KRR-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & " TO KRDP-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
        End If
        Testo = Testo & Space(14) & "   MOVE KRDP-" & Replace(MadrdM_GenRout.TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & "  TO LNKDB-FDBKEY" & vbCrLf
      End If
      'Testo = Testo & Space(14) & "END-IF" & vbCrLf
    End If
     
    Testo = Testo & Space(14) & "   MOVE LNKDB-KEYNAME(" & indEnd & ", 1) TO WK-KEYNAME" & vbCrLf
    Testo = Testo & Space(14) & "   MOVE LNKDB-KEYOPER(" & indEnd & ", 1) TO WK-KEYOPER" & vbCrLf
    Testo = Testo & Space(14) & "   MOVE LNKDB-KEYVALUE(" & indEnd & ", 1) TO WK-KEYVALUE" & vbCrLf
    Testo = Testo & Space(14) & "   MOVE WK-SSA TO LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
     
    If InStr(MadrdM_GenRout.TabIstr.BlkIstr(1).CodCom, "D") Then
      Testo = Testo & Space(17) & "STRING  RD-" & MadrdM_GenRout.TabIstr.BlkIstr(1).segment & vbCrLf
      For i = 2 To indEnd
        Testo = Testo & Space(17) & "        RD-" & MadrdM_GenRout.TabIstr.BlkIstr(i).segment & vbCrLf
      Next i
      Testo = Testo & Space(17) & "  DELIMITED BY SIZE" & vbCrLf
      Testo = Testo & Space(17) & "  INTO LNKDB-DATA-AREA(" & UBound(MadrdM_GenRout.TabIstr.BlkIstr) & ")" & vbCrLf
    Else
      Testo = Testo & Space(17) & "MOVE RD-" & instrLevel.segment & " TO LNKDB-DATA-AREA(" & UBound(MadrdM_GenRout.TabIstr.BlkIstr) & ")" & vbCrLf
    End If
    Testo = Testo & Space(14) & "END-IF" & vbCrLf
    If Not isCicsVsam Then
      Testo = Testo & Space(6) & "*" & vbCrLf
      Testo = Testo & Space(14) & "CLOSE " & instrLevel.Tavola & vbCrLf
      Testo = Testo & Space(11) & "END-IF" & vbCrLf
    End If
  
  Next level
  getGU_VSAM = Testo

End Function

Public Function getDLET_VSAM(rtbFile As RichTextBox) As String
  Dim rs As Recordset
  Dim RtbPerform As RichTextBox, RtbOpen As RichTextBox
  Dim wPath As String
  
  On Error GoTo err
  wPath = m_fun.FnPathPrj
  
  If isCicsVsam Then
    ' CICS
    Set RtbPerform = MadrdF_GenRout.RtbPerform  ' rtb conterr� il testo della perform della routine
    RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE_VSAM\COBOL\DLET\DLET-VSAM-TP"
    dotAfterRoutine = False
  Else
    ' BATCH
    Set RtbPerform = MadrdF_GenRout.RtbPerform  ' rtb conterr� il testo della perform della routine
    RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE_VSAM\COBOL\DLET\DLET-VSAM-BATCH"
    dotAfterRoutine = False
'''    Set RtbOpen = MadrdF_GenRout.RtbSql ' rtb conterr� la open del file VSAM
'''    RtbOpen.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE_VSAM\COBOL\OPENFILE-VSAM"
'''    changeTag RtbOpen, "<NOMEVSAM>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).Tavola, "_", "-")
'''    changeTag RtbOpen, "<NOMEROUT>", GBCodNomeRout
'''    changeTag RtbPerform, "<OPENFILE-VSAM>", RtbOpen.text
  End If
  'AC move chiave
  changeTag RtbPerform, "<PK-NAME>", MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).pKey
  
  changeTag RtbPerform, "<COMMENT>", Comment
  changeTag RtbPerform, "<NUM-ISTR>", GBCodNomeRout
  changeTag RtbPerform, "<NOMEVSAM>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).Tavola, "_", "-")
  changeTag RtbPerform, "<FILEVSAM>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).Tavola, "_", "-")
  Dim rsPcb As Recordset
  Set rsPcb = m_fun.Open_Recordset("select ordinale from psdli_psbseg where segmento = '" & MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).segment & "'")
  If rsPcb.RecordCount Then
    changeTag RtbPerform, "<NUM-SEGM>", rsPcb!ordinale
  Else
    changeTag RtbPerform, "<NUM-SEGM>", 1
  End If
  rsPcb.Close
  changeTag RtbPerform, "<NOME-SEGM>", MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).segment
 
  cleanAllTags RtbPerform
  getDLET_VSAM = RtbPerform.text
  Exit Function
err:
  AggLog err.Description, MadrdF_GenRout.RtLog
End Function

Public Function getISRT_VSAM(rtbFile As RichTextBox) As String
  Dim rs As Recordset
  Dim RtbPerform As RichTextBox, RtbOpen As RichTextBox
  Dim wPath As String
  'Dim isCics As Boolean
   
'  isCics = False
'  Set rs = m_fun.Open_Recordset("select Cics from MgDLI_DecodificaIstr where " & _
'                                         "codifica = '" & MadrdF_GenRout.lswSlotMachine.SelectedItem.ListSubItems(1) & "'")
'  If rs.RecordCount Then
'    isCics = rs!Cics
'  End If
'  rs.Close
  
  wPath = m_fun.FnPathPrj
  
  If isCicsVsam Then
    ' CICS
    Set RtbPerform = MadrdF_GenRout.RtbPerform  ' rtb conterr� il testo della perform della routine
    RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE_VSAM\COBOL\ISRT\ISRT-VSAM-TP"
    dotAfterRoutine = False
  Else
    ' BATCH
    Set RtbPerform = MadrdF_GenRout.RtbPerform  ' rtb conterr� il testo della perform della routine
    RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE_VSAM\COBOL\ISRT\ISRT-VSAM-BATCH"
    dotAfterRoutine = False
    
'''    Set RtbOpen = MadrdF_GenRout.RtbSql ' rtb conterr� la open del file VSAM
'''    RtbOpen.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE_VSAM\COBOL\OPENFILE-VSAM"
'''    changeTag RtbOpen, "<NOMEVSAM>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).Tavola, "_", "-")
'''    changeTag RtbOpen, "<NOMEROUT>", GBCodNomeRout
'''    changeTag RtbPerform, "<OPENFILE-VSAM>", RtbOpen.text
  End If

  changeTag RtbPerform, "<COMMENT>", Comment
  changeTag RtbPerform, "<NUM-ISTR>", GBCodNomeRout
  changeTag RtbPerform, "<NOMEVSAM>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).Tavola, "_", "-")
  changeTag RtbPerform, "<NOMEVSAM_01>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).pKey, "_", "-")
  changeTag RtbPerform, "<FILEVSAM>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).Tavola, "_", "-")
  Dim rsPcb As Recordset
  Set rsPcb = m_fun.Open_Recordset("select ordinale from psdli_psbseg where segmento = '" & MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).segment & "'")
  If rsPcb.RecordCount Then
    changeTag RtbPerform, "<NUM-SEGM>", rsPcb!ordinale
  Else
    changeTag RtbPerform, "<NUM-SEGM>", 1
  End If
  rsPcb.Close
  changeTag RtbPerform, "<NOME-SEGM>", MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).segment
 
  cleanAllTags RtbPerform
  getISRT_VSAM = RtbPerform.text
End Function

Public Function getREPL_VSAM(rtbFile As RichTextBox) As String
  Dim rs As Recordset
  Dim RtbPerform As RichTextBox, RtbOpen As RichTextBox
  Dim wPath As String
  'Dim isCics As Boolean
  
'  isCics = False
'  Set rs = m_fun.Open_Recordset("select Cics from MgDLI_DecodificaIstr where " & _
'                                         "codifica = '" & MadrdF_GenRout.lswSlotMachine.SelectedItem.ListSubItems(1) & "'")
'  If rs.RecordCount Then
'    isCics = rs!Cics
'  End If
'  rs.Close
  
  wPath = m_fun.FnPathPrj
  
  If isCicsVsam Then
    Set RtbPerform = MadrdF_GenRout.RtbPerform ' rtb conterra il testo della perform della routine
    RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE_VSAM\COBOL\REPL\REPL-VSAM-TP"
  Else
    Set RtbPerform = MadrdF_GenRout.RtbPerform ' rtb conterra il testo della perform della routine
    RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE_VSAM\COBOL\REPL\REPL-VSAM-BATCH"
    
'''    Set RtbOpen = MadrdF_GenRout.RtbSql
'''    RtbOpen.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE_VSAM\COBOL\OPENFILE-VSAM"
'''    changeTag RtbOpen, "<NOMEVSAM>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).Tavola, "_", "-")
'''    changeTag RtbOpen, "<NOMEROUT>", GBCodNomeRout
'''    changeTag RtbPerform, "<OPENFILE-VSAM>", RtbOpen.text
  End If
  
  changeTag RtbPerform, "<COMMENT>", Comment
  changeTag RtbPerform, "<NUM-ISTR>", GBCodNomeRout
  changeTag RtbPerform, "<NOMEVSAM>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).Tavola, "_", "-")
  changeTag RtbPerform, "<FILEVSAM>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).Tavola, "_", "-")
  Dim rsPcb As Recordset
  Set rsPcb = m_fun.Open_Recordset("select ordinale from psdli_psbseg where segmento = '" & MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).segment & "'")
  If rsPcb.RecordCount Then
    changeTag RtbPerform, "<NUM-SEGM>", rsPcb!ordinale
  Else
    changeTag RtbPerform, "<NUM-SEGM>", 1
  End If
  rsPcb.Close
  changeTag RtbPerform, "<NOME-SEGM>", MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).segment
  changeTag RtbPerform, "<NOMEKEY>", Replace(MadrdM_GenRout.TabIstr.BlkIstr(UBound(MadrdM_GenRout.TabIstr.BlkIstr)).pKey, "_", "-")
 
  cleanTags RtbPerform
  getREPL_VSAM = RtbPerform.text
End Function

Public Function TestoDelChild_VSAMCics(Rtb As RichTextBox) As String
  Dim NomeDelRout As String, Testo As String, wPath As String, Percorso As String, keyValue As String

  Dim RtbDel As RichTextBox
  'SQ 31-08
  Dim tb1 As Recordset, tb2 As Recordset

  Dim Pos1 As Variant

  Dim k As Integer, k1 As Integer, k2 As Integer, k3 As Integer, Kstart As Integer
  Dim TABCorr() As MadrdM_GenRout.PSB_N_A
  Dim SwCorr As Boolean
  Dim TestoWhere As String
  Dim lenkey As String, rsColonne As Recordset
  Dim testoLoop As String

  On Error GoTo ErrorHandler
  'SP corso
  If TipoDB <> "VSAM" Then
    TestoDelChild_VSAMCics = ""
    Exit Function
  Else
    'NomeDelRout = nomeRoutine
    'Mid$(NomeDelRout, 5, 2) = "DL"

    wPath = m_fun.FnPathPrj
    Set RtbDel = MadrdF_GenRout.RtbDel
    RtbDel.text = Rtb.text

    'SP: gestione dbd storici
    For k = 1 To UBound(suffissiStorici)
      Set tb1 = m_fun.Open_Recordset("select a.nome as nome, a.idorigine as idorigine, a.idobjsource as idobjsource, a.idtable as idtable from " & db & "tabelle as a, psdli_DBD as b, bs_oggetti as c" & _
               " where b.DBD_name = '" & suffissiStorici(k) & "' and a.idobjsource = c.idoggetto and c.nome = b.DBD_name order by idtable")
      If tb1.RecordCount Then
        If k = 1 Then
          Testo = Testo & Space(5) & "EVALUATE WK-NAMETABLE" & vbCrLf
        End If
        While Not tb1.EOF

          Kstart = 0
          k1 = 0
          ReDim TABCorr(k1)
          Set tb2 = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & tb1!idorigine)
          TABCorr(k1).nome = tb2!nome
          SwCorr = True
          While SwCorr
            SwCorr = False
            k2 = UBound(TABCorr)
            For k3 = Kstart To k2
              Set tb2 = m_fun.Open_Recordset("select * from psdli_segmenti " & _
                  "where IdOggetto = " & tb1!IdObjSource & " and parent = '" & TABCorr(k3).nome & _
                  "' and idsegmento not in (select idsegmento from mgdli_segmenti)")
              While Not tb2.EOF
                k1 = k1 + 1
                ReDim Preserve TABCorr(k1)
                SwCorr = True
                TABCorr(k1).Id = tb2!idSegmento
                TABCorr(k1).nome = tb2!nome
                tb2.MoveNext
              Wend
            Next k3
            Kstart = k3
          Wend
          If UBound(TABCorr) Then
            Testo = Testo & Space(13) & "WHEN '" & tb1!nome & "'" & vbCrLf
            Testo = Testo & Space(17) & "EXEC CICS IGNORE CONDITION NOTFND" & vbCrLf
            Testo = Testo & Space(17) & "                           ENDFILE" & vbCrLf
            Testo = Testo & Space(17) & "END-EXEC" & vbCrLf
          Else
            Testo = Testo & Space(13) & "WHEN '" & tb1!nome & "'" & vbCrLf
            Testo = Testo & Space(16) & "CONTINUE" & vbCrLf
          End If
          Dim rsIndex1 As Recordset, rsIndex2 As Recordset
          For k3 = UBound(TABCorr) To 1 Step -1
            Set tb2 = m_fun.Open_Recordset("select * from " & db & "tabelle where idorigine = " & TABCorr(k3).Id)
            If tb2.RecordCount > 0 And tb2!tag <> "OLD" Then
              Set rsIndex1 = m_fun.Open_Recordset("select nome from " & db & "Index where idtable = " & tb1!idtable & " and tipo='P'")
              Set rsIndex2 = m_fun.Open_Recordset("select nome,IdIndex from " & db & "Index where idtable = " & tb2!idtable & " and tipo='P'")
              ' lunghezza key figlio
              Set rsColonne = m_fun.Open_Recordset("select a.Lunghezza from " & db & "Colonne as a," & db & "IdxCol as b where a.idcolonna = b.idcolonna and b.IdIndex = " _
                                                    & rsIndex2!idindex)
              If Not rsColonne.EOF Then
                lenkey = rsColonne!Lunghezza
              End If
              
              
              'Testo = Testo & Space(17) & "MOVE K01-" & Replace(getPrimaryKeyByIdTable(tb1!IdTable, TABLE_CURRENT), "_", "-") & " TO K01-" & Replace(getPrimaryKeyByIdTable(tb2!IdTable, TABLE_CURRENT), "_", "-") & vbCrLf
              Testo = Testo & Space(17) & "MOVE K01-" & rsIndex1!nome & " TO K01-" & rsIndex2!nome & vbCrLf
              Testo = Testo & Space(17) & "EXEC CICS STARTBR DATASET ('" & tb2!nome & "')" & vbCrLf
              Testo = Testo & Space(17) & "                  RIDFLD(" & tb1!nome & "-KEY OF K01-" & rsIndex1!nome & ")" & vbCrLf
              Testo = Testo & Space(17) & "                  GTEQ " & vbCrLf
              Testo = Testo & Space(17) & "END-EXEC" & vbCrLf
              Testo = Testo & Space(17) & "MOVE EIBRCODE  TO ITERCICS-EIBRCODE" & vbCrLf
              Testo = Testo & Space(17) & "IF ITERCICS-RETURN-NORMAL" & vbCrLf
              Testo = Testo & Space(17) & "  PERFORM LOOP-READNEXT-FIGLIO-" & k3 & "-" & k & vbCrLf
              Testo = Testo & Space(17) & "    UNTIL ITERCICS-RETURN-EOF" & vbCrLf
              'chiave parziale figlio
              Testo = Testo & Space(17) & "       OR KY-" & tb1!nome & "-KEY OF K01-" & rsIndex2!nome & vbCrLf _
                            & Space(17) & "   GREATER " & tb1!nome & "-KEY OF K01-" & rsIndex1!nome & vbCrLf
              Testo = Testo & Space(17) & "   IF ITERCICS-RETURN-NORMAL OR" & vbCrLf
              Testo = Testo & Space(17) & "      ITERCICS-RETURN-EOF    OR" & vbCrLf
              ' chiave parziale figlio
              Testo = Testo & Space(17) & "      KY-" & tb1!nome & "-KEY OF K01-" & rsIndex2!nome & vbCrLf _
                            & Space(17) & "   GREATER " & tb1!nome & "-KEY OF K01-" & rsIndex1!nome & vbCrLf
              Testo = Testo & Space(17) & "      EXEC CICS ENDBR DATASET ('" & tb2!nome & "')" & vbCrLf
              Testo = Testo & Space(17) & "      END-EXEC" & vbCrLf
              Testo = Testo & Space(17) & "   END-IF" & vbCrLf
              ''Testo = Testo & Space(17) & "ELSE" & vbCrLf
      '*      qui si potrebbe segnalare che non sono stati trovati
      '*      record da cancellare per il figlio 1
      '*      oppure non fare niente
              Testo = Testo & Space(17) & "END-IF" & vbCrLf
            End If
            tb2.Close
          Next k3
          For k3 = UBound(TABCorr) To 1 Step -1
            Set tb2 = m_fun.Open_Recordset("select * from " & db & "tabelle where idorigine = " & TABCorr(k3).Id)
            If tb2.RecordCount > 0 And tb2!tag <> "OLD" Then
              
              Set rsIndex1 = m_fun.Open_Recordset("select nome from " & db & "Index where idtable = " & tb1!idtable & " and tipo='P'")
              Set rsIndex2 = m_fun.Open_Recordset("select nome,IdIndex from " & db & "Index where idtable = " & tb2!idtable & " and tipo='P'")
              ' lunghezza key figlio
              Set rsColonne = m_fun.Open_Recordset("select a.Lunghezza from " & db & "Colonne as a," & db & "IdxCol as b where a.idcolonna = b.idcolonna and b.IdIndex = " _
                                                    & rsIndex2!idindex)
              If Not rsColonne.EOF Then
                lenkey = rsColonne!Lunghezza
              End If
              testoLoop = testoLoop & Space(8) & "LOOP-READNEXT-FIGLIO-" & k3 & "-" & k & "." & vbCrLf

              testoLoop = testoLoop & Space(17) & "EXEC CICS READNEXT INTO (RR-" & tb2!nome & ")" & vbCrLf _
                            & Space(17) & "          DATASET('" & tb2!nome & "')" & vbCrLf _
                            & Space(17) & "          RIDFLD(" & tb2!nome & "-KEY OF K01-" & rsIndex2!nome & ")" & vbCrLf _
                            & Space(17) & "END-EXEC" & vbCrLf

              testoLoop = testoLoop & Space(17) & "MOVE EIBRCODE  TO ITERCICS-EIBRCODE" & vbCrLf
              testoLoop = testoLoop & Space(17) & "IF ITERCICS-RETURN-NORMAL OR" & vbCrLf
              testoLoop = testoLoop & Space(17) & "   ITERCICS-RETURN-EOF" & vbCrLf
              ' chiave parziale figlio
              testoLoop = testoLoop & Space(17) & "   IF KY-" & tb1!nome & "-KEY OF K01-" & rsIndex2!nome & " =" & vbCrLf _
                            & Space(23) & tb1!nome & "-KEY OF K01-" & rsIndex1!nome & vbCrLf

                                                '*memorizzare chiave del puntamento

              testoLoop = testoLoop & Space(17) & "     EXEC CICS READ INTO(RR-" & tb2!nome & ")" & vbCrLf
              testoLoop = testoLoop & Space(17) & "                 DATASET ('" & tb2!nome & "')" & vbCrLf
              'testoLoop = testoLoop & Space(17) & "                   LENGTH    (lunghezza record VSAM)" & vbCrLf
              testoLoop = testoLoop & Space(17) & "                 RIDFLD(" & tb2!nome & "-KEY OF K01-" & rsIndex2!nome & ")" & vbCrLf
              'testoLoop = testoLoop & Space(17) & "                   KEYLENGTH (" & lenkey & ")" & vbCrLf
              testoLoop = testoLoop & Space(17) & "                 UPDATE" & vbCrLf
              testoLoop = testoLoop & Space(17) & "     END-EXEC" & vbCrLf
              testoLoop = testoLoop & Space(17) & "     MOVE EIBRCODE  TO ITERCICS-EIBRCODE" & vbCrLf
              testoLoop = testoLoop & Space(17) & "     IF ITERCICS-RETURN-NORMAL" & vbCrLf
              testoLoop = testoLoop & Space(17) & "       EXEC CICS DELETE DATASET ('" & tb2!nome & "')" & vbCrLf
              testoLoop = testoLoop & Space(17) & "       END-EXEC" & vbCrLf
              testoLoop = testoLoop & Space(17) & "       MOVE EIBRCODE  TO ITERCICS-EIBRCODE" & vbCrLf
              testoLoop = testoLoop & Space(17) & "       IF ITERCICS-RETURN-NORMAL" & vbCrLf

                                                '*risistemare chiave del puntamento
              testoLoop = testoLoop & Space(17) & "          CONTINUE" & vbCrLf
              testoLoop = testoLoop & Space(17) & "       END-IF" & vbCrLf
              ''testoLoop = testoLoop & Space(17) & "     ELSE" & vbCrLf
                                                  'segnalazione errore + abend (?) oppure non fare niente
              testoLoop = testoLoop & Space(17) & "     END-IF" & vbCrLf
              testoLoop = testoLoop & Space(17) & "   END-IF" & vbCrLf
              ''testoLoop = testoLoop & Space(17) & "ELSE" & vbCrLf
                                              'segnalazione errore + abend (?) oppure non fare niente
              testoLoop = testoLoop & Space(17) & "END-IF" & vbCrLf
              testoLoop = testoLoop & Space(17) & "." & vbCrLf
            End If
            tb2.Close
          Next k3
          tb1.MoveNext
        Wend
        If k = UBound(suffissiStorici) Then
          Testo = Testo & Space(11) & "END-EVALUATE" & vbCrLf
        End If
        Testo = Testo & testoLoop
      End If
      Pos1 = RtbDel.find("*#DEL-CHILD-VSAM-CICS#", 0)
      If Pos1 > 0 Then RtbDel.SelText = Testo
    Next k
    TestoDelChild_VSAMCics = Testo
  End If

  Exit Function
ErrorHandler:
  AggLog err.Description, MadrdF_GenRout.RtLog
End Function

Public Function TestoDelChild_VSAMBatch(Rtb As RichTextBox) As String
  Dim NomeDelRout As String, Testo As String, wPath As String, Percorso As String, keyValue As String

  Dim RtbDel As RichTextBox
  'SQ 31-08
  Dim tb1 As Recordset, tb2 As Recordset

  Dim Pos1 As Variant

  Dim k As Integer, k1 As Integer, k2 As Integer, k3 As Integer, Kstart As Integer
  Dim TABCorr() As MadrdM_GenRout.PSB_N_A
  Dim SwCorr As Boolean
  Dim TestoWhere As String, testoLoop As String

  On Error GoTo ErrorHandler
  'SP corso
  If TipoDB <> "VSAM" Then
    TestoDelChild_VSAMBatch = ""
    Exit Function
  Else
    'NomeDelRout = nomeRoutine
    'Mid$(NomeDelRout, 5, 2) = "DL"

    wPath = m_fun.FnPathPrj
    Set RtbDel = MadrdF_GenRout.RtbDel
    RtbDel.text = Rtb.text

    'SP: gestione dbd storici
    For k = 1 To UBound(suffissiStorici)
      Set tb1 = m_fun.Open_Recordset("select a.nome as nome, a.idorigine as idorigine, a.idobjsource as idobjsource, a.idtable as idtable from " & db & "tabelle as a, psdli_DBD as b, bs_oggetti as c" & _
               " where b.DBD_name = '" & suffissiStorici(k) & "' and a.idobjsource = c.idoggetto and c.nome = b.DBD_name order by idtable")
      If tb1.RecordCount Then
        If k = 1 Then
          Testo = Testo & Space(5) & "EVALUATE WK-NAMETABLE" & vbCrLf
        End If
        While Not tb1.EOF

          Kstart = 0
          k1 = 0
          ReDim TABCorr(k1)
          Set tb2 = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & tb1!idorigine)
          TABCorr(k1).nome = tb2!nome
          SwCorr = True
          While SwCorr
            SwCorr = False
            k2 = UBound(TABCorr)
            For k3 = Kstart To k2
              Set tb2 = m_fun.Open_Recordset("select * from psdli_segmenti " & _
                  "where IdOggetto = " & tb1!IdObjSource & " and parent = '" & TABCorr(k3).nome & _
                  "' and idsegmento not in (select idsegmento from mgdli_segmenti)")
              While Not tb2.EOF
                k1 = k1 + 1
                ReDim Preserve TABCorr(k1)
                SwCorr = True
                TABCorr(k1).Id = tb2!idSegmento
                TABCorr(k1).nome = tb2!nome
                tb2.MoveNext
              Wend
            Next k3
            Kstart = k3
          Wend
          If UBound(TABCorr) Then
            Testo = Testo & Space(13) & "WHEN '" & tb1!nome & "'" & vbCrLf
          Else
            Testo = Testo & Space(13) & "WHEN '" & tb1!nome & "'" & vbCrLf
            Testo = Testo & Space(16) & "CONTINUE" & vbCrLf
          End If
          Dim rsIndex1 As Recordset, rsIndex2 As Recordset
          For k3 = UBound(TABCorr) To 1 Step -1
            Set tb2 = m_fun.Open_Recordset("select * from " & db & "tabelle where idorigine = " & TABCorr(k3).Id)
            'stefanopippo: 17/08/2005: gestisce le tabelle gi� esistenti evitando
            'la delete;

            If tb2.RecordCount > 0 And tb2!tag <> "OLD" Then
              
              Set rsIndex1 = m_fun.Open_Recordset("select nome from " & db & "Index where idtable = " & tb1!idtable & " and tipo='P'")
              Set rsIndex2 = m_fun.Open_Recordset("select nome from " & db & "Index where idtable = " & tb2!idtable & " and tipo='P'")
              
             
              Testo = Testo & Space(17) & "MOVE K01-" & rsIndex1!nome & " TO K01-" & rsIndex2!nome & vbCrLf
              Testo = Testo & Space(17) & "START " & tb2!nome & " KEY NOT LESS " & vbCrLf
              Testo = Testo & Space(20) & tb1!nome & "-KEY OF K01-" & rsIndex1!nome & vbCrLf
              Testo = Testo & Space(17) & "MOVE " & tb2!nome & "-STATUS TO WK-VSAMCODE" & vbCrLf
              Testo = Testo & Space(17) & "IF  WK-VSAMCODE = ZERO" & vbCrLf
              Testo = Testo & Space(17) & "  PERFORM LOOP-READNEXT-FIGLIO-" & k3 & "-" & k & vbCrLf
              Testo = Testo & Space(17) & "UNTIL WK-VSAMCODE = '10'" & vbCrLf
              'CHIAVE-PARZIALE-FIGLIO GREATER
              Testo = Testo & Space(17) & "       OR KY-" & tb1!nome & "-KEY OF K01-" & rsIndex2!nome & vbCrLf _
                            & Space(24) & tb1!nome & "-KEY OF K01-" & rsIndex1!nome & vbCrLf
              ''Testo = Testo & Space(17) & "ELSE" & vbCrLf
      '*      qui si potrebbe segnalare che non sono stati trovati
      '*      record da cancellare per il figlio 1
      '*      oppure non fare niente
              Testo = Testo & Space(17) & "END-IF" & vbCrLf

            End If
          Next k3
          tb2.Close
          For k3 = UBound(TABCorr) To 1 Step -1
            Set tb2 = m_fun.Open_Recordset("select * from " & db & "tabelle where idorigine = " & TABCorr(k3).Id)
            
            If tb2.RecordCount > 0 And tb2!tag <> "OLD" Then
              
              Set rsIndex1 = m_fun.Open_Recordset("select nome from " & db & "Index where idtable = " & tb1!idtable & " and tipo='P'")
              Set rsIndex2 = m_fun.Open_Recordset("select nome from " & db & "Index where idtable = " & tb2!idtable & " and tipo='P'")

              testoLoop = testoLoop & Space(8) & "LOOP-READNEXT-FIGLIO-" & k3 & "-" & k & "." & vbCrLf

              testoLoop = testoLoop & Space(17) & "READ " & tb2!nome & " NEXT INTO RR-" & tb2!nome & vbCrLf
              testoLoop = testoLoop & Space(17) & "MOVE " & tb2!nome & "-STATUS TO WK-VSAMCODE" & vbCrLf
              testoLoop = testoLoop & Space(17) & "IF WK-VSAMCODE = '10'" & vbCrLf
              testoLoop = testoLoop & Space(17) & "   NEXT SENTENCE" & vbCrLf
              testoLoop = testoLoop & Space(17) & "ELSE" & vbCrLf
              testoLoop = testoLoop & Space(17) & "   IF WK-VSAMCODE = ZERO" & vbCrLf
              'CHIAVE-PARZIALE-FIGLIO
              testoLoop = testoLoop & Space(17) & "       IF KY-" & tb1!nome & "-KEY OF K01-" & rsIndex2!nome & " = " & vbCrLf _
                                    & Space(27) & tb1!nome & "-KEY OF K01-" & rsIndex1!nome & vbCrLf
              testoLoop = testoLoop & Space(17) & "           DELETE " & tb2!nome & vbCrLf
              testoLoop = testoLoop & Space(17) & "           MOVE " & tb2!nome & "-STATUS TO WK-VSAMCODE" & vbCrLf
              testoLoop = testoLoop & Space(17) & "           IF WK-VSAMCODE NOT = ZERO" & vbCrLf
                        'segnalaz. errore + abend (?) oppure non fare niente
              testoLoop = testoLoop & Space(17) & "           END-IF" & vbCrLf
              testoLoop = testoLoop & Space(17) & "       END-IF" & vbCrLf
              testoLoop = testoLoop & Space(17) & "   ElSE" & vbCrLf
              testoLoop = testoLoop & Space(17) & "      segnalazione errore + abend (?) oppure IMPOSTARE '10'" & vbCrLf
              testoLoop = testoLoop & Space(17) & "      NELLO STATUS CODE DI RITORNO PER CHIUDERE IL LOOP DI" & vbCrLf
              testoLoop = testoLoop & Space(17) & "      LETTURA" & vbCrLf
              testoLoop = testoLoop & Space(17) & "   END-IF" & vbCrLf
              testoLoop = testoLoop & Space(17) & "END-IF" & vbCrLf
              testoLoop = testoLoop & Space(17) & "." & vbCrLf
            End If
            tb2.Close
          Next k3
          
          tb1.MoveNext
        Wend
        If k = UBound(suffissiStorici) Then
          Testo = Testo & Space(11) & "END-EVALUATE" & vbCrLf
        End If
        Testo = Testo & testoLoop
      End If
      Pos1 = RtbDel.find("*#DEL-CHILD-VSAM-CICS#", 0)
      If Pos1 > 0 Then RtbDel.SelText = Testo
    Next k
    TestoDelChild_VSAMBatch = Testo
  End If

  Exit Function
ErrorHandler:
  AggLog err.Description, MadrdF_GenRout.RtLog
End Function
