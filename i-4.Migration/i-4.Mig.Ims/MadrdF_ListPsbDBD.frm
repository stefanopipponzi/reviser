VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadrdF_ListPsbDBD 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choose..."
   ClientHeight    =   5115
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4155
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5115
   ScaleWidth      =   4155
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lswList 
      Height          =   4875
      Left            =   60
      TabIndex        =   2
      Top             =   90
      Width           =   2985
      _ExtentX        =   5265
      _ExtentY        =   8599
      View            =   3
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Id"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Name"
         Object.Width           =   3528
      EndProperty
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "&Ok"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   3150
      Picture         =   "MadrdF_ListPsbDBD.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   90
      Width           =   945
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   3150
      Picture         =   "MadrdF_ListPsbDBD.frx":1D42
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1350
      Width           =   945
   End
End
Attribute VB_Name = "MadrdF_ListPsbDBD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private i_IdLista As String
Private i_NomeLista As String

Private Sub cmdCancel_Click()
  i_IdLista = ""
  i_NomeLista = ""
 
  Unload Me
End Sub

Private Sub cmdOpen_Click()
'''  Dim i As Long
'''  If gbTypeToLoad = "SEG" Then
'''    i_IdLista = ""
'''    i_NomeLista = ""
'''    For i = 1 To lswList.ListItems.count
'''      If lswList.ListItems(i).Selected Then
'''        i_IdLista = IIf(Len(i_IdLista), i_IdLista & ";", "") & lswList.ListItems(i)
'''        i_NomeLista = IIf(Len(i_NomeLista), i_NomeLista & ";", "") & lswList.ListItems(i).ListSubItems(1).text
'''      End If
'''    Next i
'''  End If

  Me.Hide
End Sub

Private Sub Form_Load()
  ld_Lista
End Sub

Public Sub ld_Lista()
  Dim rs As Recordset
  Dim wId As String
  Dim wNome As String
  Dim widDbd As String
  Dim i As Long
  
  widDbd = ""
  
  For i = 1 To UBound(Istruzione.Database)
    widDbd = widDbd & " or IdOggetto = " & Istruzione.Database(i).IdOggetto
  Next i
  
  widDbd = Mid(widDbd, 5)
  
  Select Case gbTypeToLoad
    Case "PSB"
      Me.Caption = "Choose Psb association..."
      Set rs = m_fun.Open_Recordset("Select * From Bs_Oggetti Where Tipo = '" & gbTypeToLoad & "' order by nome")
      wId = "idOggetto"
      wNome = "Nome"
       
    Case "DBD"
      Me.Caption = "Choose Dbd association..."
      Set rs = m_fun.Open_Recordset("Select * From Bs_Oggetti Where Tipo = '" & gbTypeToLoad & "' order by nome")
      wId = "idOggetto"
      wNome = "Nome"
       
    Case "SEG" 'segmenti
      If widDbd <> "" Then
        Me.Caption = "Choose Segment association..."
        Set rs = m_fun.Open_Recordset("Select * From psDli_segmenti Where " & widDbd)
        wId = "idSegmento"
        wNome = "Nome"
      Else
        Exit Sub
      End If
  End Select
  
  lswList.ListItems.Clear
     
  While Not rs.EOF
    lswList.ListItems.Add , , Format(rs.Fields(wId), "000000")
    lswList.ListItems(lswList.ListItems.Count).ListSubItems.Add , , rs.Fields(wNome)
    
    rs.MoveNext
  Wend
  
  rs.Close
  
  ' Mauro 05/12/2007 : Devo mantenere selezionati i segmenti che ho gi� nella text
  Dim Segm() As String
  Dim a As Long
  If gbTypeToLoad = "SEG" Then
    lswList.ListItems(1).Selected = False
    Segm = Split(MadrdF_Corrector.txtsegm.text & ";", ";")
    For i = 0 To UBound(Segm) - 1
      For a = 1 To lswList.ListItems.Count
        If lswList.ListItems(a).ListSubItems(1) = Segm(i) Then
          lswList.ListItems(a).Selected = True
          Exit For
        End If
      Next a
    Next i
  End If
End Sub

Property Get Id() As String
  Dim i As Long
  
  For i = 1 To lswList.ListItems.Count
    If lswList.ListItems(i).Selected Then
      'Id = i_IdLista & "#" & Id
      Id = lswList.ListItems(i) & "#" & Id
    End If
  Next i
  
  If gbTypeToLoad <> "SEG" Then
    Id = Mid(Id, 2) & "#"
  End If
End Property
Public Sub CheckObsolete()
  Dim i As Long
  Dim c As Long
  Dim Id As String
  Dim listaseg As String
  Dim Lunghezza As String
  Dim listaid As String
  Dim nomeseg As String
  Dim scelta As Integer
  Dim msg As String
  For i = 1 To lswList.ListItems.Count
   If lswList.ListItems(i).Selected Then
    nomeseg = lswList.ListItems(i).ListSubItems(1).text
     Id = lswList.ListItems(i)
     Set rs = m_fun.Open_Recordset("Select nome From MgDLI_Segmenti Where IdSegmento = " & Id & "  and Correzione = 'o' ")
     If rs.RecordCount Then
      listaseg = listaseg + nomeseg & ","
      listaid = listaid + Id & ","
      c = 1
     End If
     rs.Close
   End If
  Next i
  If c > 0 Then
   Lunghezza = Len(listaseg) - 1
    listaseg = (Mid(listaseg, 1, Lunghezza))

     msg = "Some segments are 'obsolete': " & listaseg & "." & _
                  "Do you want to insert them as well?"
      scelta = MsgBox(msg, vbYesNo + vbInformation, Dli2Rdbms.drNomeProdotto)
   End If
   If scelta = vbNo Then
    For i = 1 To lswList.ListItems.Count
     If lswList.ListItems(i).Selected Then
      nomeseg = lswList.ListItems(i).ListSubItems(1).text
       Id = lswList.ListItems(i)
        If InStr(listaid, Id) Then
          lswList.ListItems(i).Selected = False
        End If
      End If
    Next i
   End If
End Sub
Property Get SelName() As String
  Dim i As Long
  
  For i = 1 To lswList.ListItems.Count
    If lswList.ListItems(i).Selected Then
      'SelName = i_NomeLista & "#" & SelName
      SelName = lswList.ListItems(i).ListSubItems(1) & "#" & SelName
    End If
  Next i
End Property

Private Sub lswList_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswList.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswList.SortOrder = Order
  lswList.SortKey = key - 1
  lswList.Sorted = True
End Sub

Private Sub lswList_ItemClick(ByVal item As MSComctlLib.ListItem)
  i_IdLista = item.text
  i_NomeLista = item.ListSubItems(1).text
End Sub
