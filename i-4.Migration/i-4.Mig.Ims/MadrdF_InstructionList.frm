VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadrdF_InstructionList 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choose..."
   ClientHeight    =   4530
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3510
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4530
   ScaleWidth      =   3510
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   2490
      Picture         =   "MadrdF_InstructionList.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   120
      Width           =   945
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   2490
      Picture         =   "MadrdF_InstructionList.frx":1D42
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1320
      Width           =   945
   End
   Begin MSComctlLib.ListView lswInstr 
      Height          =   4335
      Left            =   60
      TabIndex        =   2
      Top             =   60
      Width           =   2355
      _ExtentX        =   4154
      _ExtentY        =   7646
      View            =   3
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Key             =   "INSTR"
         Text            =   "Instructions"
         Object.Width           =   7708
      EndProperty
   End
End
Attribute VB_Name = "MadrdF_InstructionList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdOk_Click()
  If lswInstr.ListItems.Count Then
    Madrdf_EditDecod.txtInstruction = lswInstr.SelectedItem
  Else
    Madrdf_EditDecod.txtInstruction = ""
  End If
  Unload Me
End Sub

Private Sub Form_Load()
  Carica_Lista
  Me.Show
End Sub

Public Sub Carica_Lista()
  Dim rs As Recordset
  
  lswInstr.ColumnHeaders(1).Width = lswInstr.Width
  Set rs = m_fun.Open_Recordset("select distinct(codifica) from mgdli_decodificaistr " & _
                                "order by codifica")
  Do Until rs.EOF
    lswInstr.ListItems.Add , , rs!Codifica
    rs.MoveNext
  Loop
  rs.Close
End Sub

Private Sub lswInstr_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswInstr.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswInstr.SortOrder = Order
  lswInstr.SortKey = key - 1
  lswInstr.Sorted = True
End Sub

Private Sub lswInstr_DblClick()
  cmdOk_Click
End Sub
