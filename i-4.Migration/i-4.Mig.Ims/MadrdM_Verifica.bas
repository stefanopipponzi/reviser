Attribute VB_Name = "MadrdM_Verifica"
Option Explicit

Type Obj
  idOggetto As Long
  nome As String
  tipo As String
  DLI As Boolean
  Directory As String
End Type

Global Oggetti_TOTAL() As Obj
Global Oggetti_SELECT() As Obj
'silvia: filter
Global Oggetti_FILTER() As Obj


'Type per la lista dei PSB presenti nel progetto fisicamente e non
Type PSBL
  nome As String
  Directory As String
  Esistente As Boolean
End Type

Global PSBList() As PSBL

Global NomePsb As String
Global DirectoryPSB As String
Global IdPSB As Long
Global IdOgg As Long

'silvia
Global Oggetto_ID() As String
Global StrIdOggetti As String
Global FinalS As String
Global AppStr As String, AppStr2 As String

Public Sub Carica_Array_Oggetti_Errori(TipoErr As String)
  Dim r As Recordset
  Dim i As Long
  Dim strfrom As String
  
  Select Case UCase(Trim(TipoErr))
    Case "TOTAL"
      ReDim Oggetti_TOTAL(0)
      ReDim Oggetto_ID(0)
      If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
        Set r = m_fun.Open_Recordset( _
          "Select idOggetto, nome, tipo, directory_input From BS_oggetti where (ims or dli) and notes='CONV' Order By nome")
      Else
        Set r = m_fun.Open_Recordset( _
          "Select idOggetto, nome, tipo, directory_input From BS_oggetti where ims or dli Order By nome")
      End If
      While Not r.EOF
        DoEvents
        ReDim Preserve Oggetti_TOTAL(UBound(Oggetti_TOTAL) + 1)
        ''SILVIA
        ReDim Preserve Oggetto_ID(UBound(Oggetto_ID) + 1)
        Oggetto_ID(UBound(Oggetto_ID)) = r!idOggetto
        ''
        Oggetti_TOTAL(UBound(Oggetti_TOTAL)).idOggetto = r!idOggetto
        Oggetti_TOTAL(UBound(Oggetti_TOTAL)).nome = r!nome
        Oggetti_TOTAL(UBound(Oggetti_TOTAL)).tipo = r!tipo
        Oggetti_TOTAL(UBound(Oggetti_TOTAL)).DLI = True
        Oggetti_TOTAL(UBound(Oggetti_TOTAL)).Directory = Crea_Directory_Progetto(r!Directory_Input, Dli2Rdbms.drPathDef)
        
        r.MoveNext
      Wend
      r.Close
      
    Case "SELECT"
      ReDim Oggetti_SELECT(0)
      ReDim Oggetto_ID(0)
      For i = 1 To Dli2Rdbms.drObjList.ListItems.Count
        If Dli2Rdbms.drObjList.ListItems(i).Selected Then
          
          ReDim Preserve Oggetti_SELECT(UBound(Oggetti_SELECT) + 1)
          Oggetti_SELECT(UBound(Oggetti_SELECT)).idOggetto = Val(Dli2Rdbms.drObjList.ListItems(i).text)
          ''SILVIA
          ReDim Preserve Oggetto_ID(UBound(Oggetto_ID) + 1)
          Oggetto_ID(UBound(Oggetto_ID)) = Oggetti_SELECT(UBound(Oggetti_SELECT)).idOggetto
          ''
          Oggetti_SELECT(UBound(Oggetti_SELECT)).nome = Dli2Rdbms.drObjList.ListItems(i).ListSubItems(NAME_OBJ)
          Oggetti_SELECT(UBound(Oggetti_SELECT)).tipo = Dli2Rdbms.drObjList.ListItems(i).ListSubItems(TYPE_OBJ)
          Oggetti_SELECT(UBound(Oggetti_SELECT)).DLI = IIf(Dli2Rdbms.drObjList.ListItems(i).ListSubItems(DLI_OBJ) = "X", True, False)
          Oggetti_SELECT(UBound(Oggetti_SELECT)).Directory = Dli2Rdbms.drObjList.ListItems(i).ListSubItems(DIR_OBJ)
          
        End If
      Next i
     
    'silvia : filter
    Case "FILTER"
      ReDim Oggetti_FILTER(0)
      If AppStr2 <> "" Then
        strfrom = " BS_Oggetti, PsRel_Obj "
      Else
        strfrom = " BS_Oggetti"
      End If
      
      If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
        Set r = m_fun.Open_Recordset( _
          "Select idOggetto, nome, tipo, directory_input From " & strfrom & " where (ims or dli) and notes='CONV' " & FinalS & " Order By nome")
      Else
        Set r = m_fun.Open_Recordset( _
          "Select idOggetto, nome, tipo, directory_input From " & strfrom & "  where ims or dli " & FinalS & " Order By nome")
      End If
      While Not r.EOF
        ReDim Preserve Oggetti_FILTER(UBound(Oggetti_FILTER) + 1)
        Oggetti_FILTER(UBound(Oggetti_FILTER)).idOggetto = r!idOggetto
        Oggetti_FILTER(UBound(Oggetti_FILTER)).nome = r!nome
        Oggetti_FILTER(UBound(Oggetti_FILTER)).tipo = r!tipo
        Oggetti_FILTER(UBound(Oggetti_FILTER)).DLI = True
        Oggetti_FILTER(UBound(Oggetti_FILTER)).Directory = Crea_Directory_Progetto(r!Directory_Input, Dli2Rdbms.drPathDef)
        
        r.MoveNext
      Wend
      r.Close
      
  End Select
End Sub

'Public Sub Carica_Lista_Oggetti(SQL As Boolean, StringaSQL As String, LSW As ListView, Arr() As Obj, ClearList As Boolean)
Public Sub Carica_Lista_Oggetti(LSW As ListView, Arr() As Obj)
  Dim i As Long
  Dim j As Long
  Dim Exist As Boolean
  Dim r As Recordset
  Dim istrValide As Long, istrTotali As Long, IstrValideTot As Long
  Static Order As String
  
  MadrdF_Verifica.lswMsg.ListItems.Clear
  IstrValideTot = 0
  
  LSW.ListItems.Clear
  Order = LSW.SortOrder
  LSW.SortOrder = 0
  LSW.Sorted = False
  
  For i = 1 To UBound(Arr)
    'Controlla se � gia presente in lista il nome
    For j = 1 To LSW.ListItems.Count
      If Trim(LSW.ListItems(j).text) = Trim(Arr(i).nome) Then
        Exist = True
        Exit For
      End If
    Next j
    
    If Exist Then
      Exist = False
    Else
      LSW.ListItems.Add , , Arr(i).nome
      LSW.ListItems(LSW.ListItems.Count).tag = Arr(i).idOggetto
      LSW.ListItems(LSW.ListItems.Count).SubItems(1) = Arr(i).tipo
      If Arr(i).DLI Then
        LSW.ListItems(LSW.ListItems.Count).SubItems(2) = "X"
      Else
        LSW.ListItems(LSW.ListItems.Count).SubItems(2) = ""
      End If
      If MadrdF_Verifica.chkDetails Then
        istrValide = 0
        If Arr(i).tipo = "CPY" Or Arr(i).tipo = "INC" Then
          Set r = m_fun.Open_Recordset("select valida from psdli_istruzioni where " & _
                                       "idoggetto = " & Arr(i).idOggetto & " and idpgm > 0 and to_migrate")
        Else
          Set r = m_fun.Open_Recordset("SELECT valida FROM psdli_istruzioni WHERE " & _
                                       "idPgm = " & Arr(i).idOggetto & " and to_migrate")
        End If
        
        Do Until r.EOF
          If r!valida Then
            istrValide = istrValide + 1
          End If
          r.MoveNext
        Loop
        istrTotali = r.RecordCount
        r.Close
        If istrValide = istrTotali Then
          LSW.ListItems(LSW.ListItems.Count).SubItems(3) = "X"
          IstrValideTot = IstrValideTot + 1
        Else
          LSW.ListItems(LSW.ListItems.Count).SubItems(3) = ""
        End If
        LSW.ListItems(LSW.ListItems.Count).SubItems(4) = istrValide & "/" & istrTotali
      End If
      LSW.ListItems(LSW.ListItems.Count).SubItems(5) = "-"
      If MadrdF_Verifica.chkPSB Then
        Set r = m_fun.Open_Recordset("select b.nome from psrel_obj as a, bs_oggetti as b where " & _
                                     "a.idoggettoc = " & Arr(i).idOggetto & " and relazione = 'PSB' and a.idoggettoR = b.idoggetto")
        If r.RecordCount Then
          LSW.ListItems(LSW.ListItems.Count).SubItems(6) = r!nome
        Else
          LSW.ListItems(LSW.ListItems.Count).SubItems(6) = ""
        End If
      End If
      LSW.ListItems(LSW.ListItems.Count).SubItems(7) = Arr(i).idOggetto
    End If
  Next i
  
  LSW.SortOrder = Order
  LSW.Sorted = True
  If LSW.ListItems.Count Then
    MadrdF_Verifica.lblObj = LSW.ListItems(1).tag & " - " & LSW.ListItems(1).text
    If MadrdF_Verifica.chkDetails Then
      MadrdF_Verifica.lblValid = IstrValideTot
    Else
      MadrdF_Verifica.lblValid = "-"
    End If
  End If
  MadrdF_Verifica.lblList = LSW.ListItems.Count
End Sub

Public Sub Carica_Segnalazioni_Da_IdOggetto(LSW As ListView, idOggetto As Long)
  Dim r As Recordset
''  Dim i As Long
''  Dim Bool As Boolean
  Dim Mess As String
  Dim Icona As Long
  
  LSW.ListItems.Clear
  Set r = m_fun.Open_Recordset("Select Distinct IdOggetto,Codice,Riga From BS_Segnalazioni Where " & _
                               "(IdOggetto = " & idOggetto & " or IdOggettoRel = " & idOggetto & ") and " & _
                               "(codice like 'I%%' or codice in ('F04','F06','F07','F08','F09','F10','F29','F30','H10','H30','P12')) " & _
                               "order by riga")
    
  Do Until r.EOF
''    Bool = False
''    For i = 1 To LSW.ListItems.count
''      If Trim(LSW.ListItems(i).text) = Trim(Mess) Then
''        Bool = True
''        Exit For
''      End If
''    Next i
    
''    If Bool = False Then
      Mess = Componi_Messaggio_Errore(r!idOggetto, r!Codice, r!Riga)
      Icona = Restituisci_Gravita_Errore(r!idOggetto, r!Riga, r!Codice)
      LSW.ListItems.Add , , Mess, , Icona
      LSW.ListItems(LSW.ListItems.Count).SubItems(1) = r!Riga
      LSW.ListItems(LSW.ListItems.Count).tag = r!idOggetto & "/" & r!Codice & "/" & r!Riga
''    End If
    
    r.MoveNext
  Loop
  r.Close
End Sub

'Restituisce il long relativo alla image list
'1 = Informational
'2 = Warning
'3 = Several
'6 = ERROR
Public Function Restituisci_Gravita_Errore(idOggetto As Long, Riga As Long, Codice As String) As Long
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Segnalazioni Where " & _
                               "IdOggetto = " & idOggetto & " And Riga = " & Riga & " And Codice = '" & Codice & "'")
  
  If r.RecordCount Then
    'E' un solo record
    Select Case Trim(UCase(r!Gravita))
      Case "I" 'Information
        Restituisci_Gravita_Errore = 1
      Case "W" 'Warning
        Restituisci_Gravita_Errore = 2
      Case "E"
        Restituisci_Gravita_Errore = 6
      Case "S"
        Restituisci_Gravita_Errore = 3
    End Select
  End If
  r.Close
  
End Function

'Restituisce il long relativo alla image list
'1 = Informational
'2 = Warning
'3 = Several
'6 = ERROR
Public Function Restituisci_Codice_Errore_Da_IdOggetto(idOggetto As Long, Riga As Long, Codice As String) As String
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Segnalazioni Where " & _
                               "IdOggetto = " & idOggetto & " And Riga = " & Riga & " And Codice = '" & Codice & "'")
  If r.RecordCount Then
    'E' un solo record
    Restituisci_Codice_Errore_Da_IdOggetto = r!Codice
  End If
  r.Close
  
End Function

Public Sub Carica_Lista_PSB(LSW As ListView, tipo As String, lbl As Frame)
  Dim i As Long
  Dim j As Long
  Dim Icona As Long
  Dim rs As Recordset
  
  LSW.ListItems.Clear
  
  ' Mauro 25/01/2008 : Ho asteriscato tutto, tanto usa sempre e solo il caso "INTERNAL"
''  Select Case Trim(UCase(tipo))
''    Case "ALL"
''      For i = 1 To UBound(PSBList)
''        If PSBList(i).Esistente = True Then
''          Icona = 5
''        Else
''          Icona = 4
''        End If
''        LSW.ListItems.Add , , PSBList(i).nome, , Icona
''        LSW.ListItems(LSW.ListItems.count).tag = PSBList(i).Directory
''      Next i
''
''    Case "INTERNAL"
       Set rs = m_fun.Open_Recordset("Select * From BS_Oggetti Where Tipo = 'PSB' Order by Nome")
       While Not rs.EOF
         LSW.ListItems.Add , , rs!nome, , 5
         'LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , m_fun.Crea_Directory_Progetto(rs!Directory_Input, Dli2Rdbms.drPathDb)
         LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , m_fun.Crea_Directory_Progetto(rs!Directory_Input, Dli2Rdbms.drPathDef)
         rs.MoveNext
       Wend
       rs.Close
       
''    Case "EXTERNAL"
''       For i = 1 To UBound(PSBList)
''        If PSBList(i).Esistente = False Then
''          LSW.ListItems.Add , , PSBList(i).nome, , 4
''          LSW.ListItems(LSW.ListItems.count).ListSubItems.Add , , PSBList(i).Directory
''        End If
''      Next i
''  End Select
  ''lbl.Caption = "PSB File found in project N� " & LSW.ListItems.count
End Sub

Public Sub Carica_Dipendenze_PROGRAMMI(NomeOgg As String, IdOgg As Long, LSW As ListView, LswCal As ListView)
  Dim r As Recordset
  Dim c As Long
  
  LSW.ListItems.Clear
  LswCal.ListItems.Clear
  MadrdF_Verifica.txtPsb.text = ""
  
  'Aggiunge il chiamante primario
  LswCal.ListItems.Add , , NomeOgg
  LswCal.ListItems(LswCal.ListItems.Count).ListSubItems.Add , , "PRIMARY"
  'MadrdF_Verifica.txtPsb.Text = ""
  
  Set r = m_fun.Open_Recordset("Select b.nome as PSB ,a.IdOggettoR as IdOggettoR From PSRel_Obj as a,bs_oggetti as b Where " & _
                               "IdOggettoC = " & IdOgg & " And Relazione = 'PSB' and a.IdOggettoR = b.idOggetto")
  If r.RecordCount Then
    LSW.ListItems.Add , , r!psb
    
    LSW.ListItems(LSW.ListItems.Count).tag = r!idOggettoR
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , "0"
    
    'ASSEGNA IL PSB ALLA TEXTBOX
    MadrdF_Verifica.txtPsb.text = r!psb
    r.Close
    Exit Sub
  Else
    'isDC?
    r.Close
  End If
  
  Set r = m_fun.Open_Recordset("Select a.IdOggettoC as IdOggettoC, b.Nome as Nome From PSRel_Obj as a,Bs_Oggetti as b Where " & _
                               "IdOggettoR = " & IdOgg & " and b.IdOggetto = a.IdOggettoC And Relazione = 'CAL'")
  While Not r.EOF
    LswCal.ListItems.Add , , r!nome
    LswCal.ListItems(LswCal.ListItems.Count).ListSubItems.Add , , "CALL"
    
    Trova_PSB_Dipendente LswCal, LSW, r!idOggettoC, 1
    
    r.MoveNext
    c = c + 1
    DoEvents
  Wend
  r.Close
  
  If MadrdF_Verifica.txtPsb.text = "" Then
    
    '*******************  PSB con stesso nome programma
    Set r = m_fun.Open_Recordset("select idOggetto from bs_oggetti where idOggetto = " & IdOgg & " and Ims")
      
    If Not r.EOF Then
      r.Close
      ReDim psbPgm(0)
      Set r = m_fun.Open_Recordset("select idOggetto from bs_oggetti where " & _
                                   "nome = '" & NomeOgg & "' and tipo = 'PSB'")
      If r.RecordCount = 0 Then
        Set r = m_fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where " & _
                                     "b.source = '" & NomeOgg & "' and b.load=a.nome and a.tipo = 'PSB'")
      End If
      If r.RecordCount Then
        MadrdF_Verifica.txtPsb.text = NomeOgg
      Else
         LSW.ListItems.Add , , "No PSB Linking to this program..."
      End If
      r.Close
    Else
      r.Close
      LSW.ListItems.Add , , "No PSB Linking to this program..."
    End If
  End If
  
End Sub

Public Sub Carica_Dipendenze_PROGRAMMILoad(NomeOgg As String, IdOgg As Long, LSW As ListView, LswCal As ListView)
  Dim r As Recordset
  Dim c As Long
  Dim i As Long
  
  LSW.ListItems.Clear
  LswCal.ListItems.Clear
  MadrdF_Verifica.txtPsb.text = ""
  
  'Aggiunge il chiamante primario
  LswCal.ListItems.Add , , NomeOgg
  LswCal.ListItems(LswCal.ListItems.Count).ListSubItems.Add , , "PRIMARY"
  MadrdF_Verifica.txtPsb.text = ""
  
  '******** eliminato giro PSRel_Obj
  Set r = m_fun.Open_Recordset("Select distinct b.nome as PSB ,a.IdPsb as IdPsb From PsDli_IstrPSB as a,bs_oggetti as b Where " & _
                               "a.Idpgm = " & IdOgg & "  and a.IdPsb = b.idOggetto")
  If Not r.EOF Then
    LSW.ListItems.Add , , r!psb
    
    LSW.ListItems(LSW.ListItems.Count).tag = r!IdPSB
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , "0"
    
    'ASSEGNA IL PSB ALLA TEXTBOX
''    If MadrdF_Verifica.txtPsb.text = "" Then
''      MadrdF_Verifica.txtPsb.text = r!psb
''    Else
      MadrdF_Verifica.txtPsb.text = MadrdF_Verifica.txtPsb.text & IIf(Len(MadrdF_Verifica.txtPsb.text), ";", "") & r!psb
''    End If
    
    r.Close
    Exit Sub
  Else
    r.Close
  End If
      
  If MadrdF_Verifica.txtPsb.text = "" Then
    
    '*******************  PSB con stesso nome programma
    Set r = m_fun.Open_Recordset("select idOggetto from bs_oggetti where " & _
                                          "idOggetto = " & IdOgg & " and Ims")
      
    If Not r.EOF Then
      ReDim psbPgm(0)
      Set r = m_fun.Open_Recordset("select idOggetto from bs_oggetti where " & _
                                   "nome = '" & NomeOgg & "' and tipo = 'PSB'")
      If r.RecordCount = 0 Then
        Set r = m_fun.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where " & _
                                     "b.source = '" & NomeOgg & "' and b.load=a.nome and a.tipo = 'PSB'")
      End If
      If r.RecordCount Then
        MadrdF_Verifica.txtPsb.text = NomeOgg
      Else
        LSW.ListItems.Add , , "No PSB Linking to this program..."
      End If
    Else
      LSW.ListItems.Add , , "No PSB Linking to this program..."
    End If
    r.Close
        
  End If
  
''  'Carica le liste con 25 item di default
''  For i = 1 To 25 - LSW.ListItems.count
''    LSW.ListItems.Add , , ""
''    LSW.ListItems(LSW.ListItems.count).ListSubItems.Add , , ""
''    DoEvents
''  Next i
''
''  For i = 1 To 25 - LswCal.ListItems.count
''    LswCal.ListItems.Add , , ""
''    LswCal.ListItems(LswCal.ListItems.count).ListSubItems.Add , , ""
''    DoEvents
''  Next i
End Sub

Public Sub Trova_PSB_Dipendente(LswCal As ListView, LSW As ListView, IdOgg As Long, Contatore As Long)
  Dim r As Recordset
  Dim R1 As Recordset
  Dim myCount As Long
  
  Static Count As Long
  
  If Contatore = 0 Then Count = 1
  
  myCount = Contatore + 1
  
  Set r = m_fun.Open_Recordset("Select a.*,b.Nome From PsRel_Obj as a,Bs_Oggetti as b Where " & _
                               "a.IdOggettoC = " & IdOgg & " And a.Relazione = 'PSB' and a.IdOggettoR = b.IdOggetto ")
  If r.RecordCount Then
    
    LSW.ListItems.Add , , r!nome '& ".PSB"
    LSW.ListItems(LSW.ListItems.Count).tag = r!idOggettoR
    
    'LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , Count
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , Contatore
    
    'ASSEGNA IL PSB ALLA TEXTBOX
    'AC
    'MadrdF_Verifica.txtPsb.Text = r!NomeComponente & ".PSB"
    
    r.Close
    
  Else
    If Count > 5 Then
      'LSW.ListItems.Add , , "No PSB Linking to this program..."
      'AC
      'MadrdF_Verifica.txtPsb.Text = ""
      Exit Sub
    End If
  
    'Cerca Oggetti di tipo CAL e rilancia la funzione
    Set R1 = m_fun.Open_Recordset("Select a.IdOggettoC as IdOggettoC, b.Nome as Nome From PSRel_Obj as a,Bs_Oggetti as b Where " & _
                                  "IdOggettoR = " & IdOgg & " and b.IdOggetto = a.IdOggettoC And Relazione = 'CAL'")
    If R1.RecordCount Then
      While Not R1.EOF
        Count = Count + 1
        'Contatore = Count
        
        LswCal.ListItems.Add , , R1!nome '& ".PSB"
        LswCal.ListItems(LswCal.ListItems.Count).ListSubItems.Add , , "CALL"
        
        'ASSEGNA IL PSB ALLA TEXTBOX
        ''MadrdF_Verifica.txtPsb.Text = R1!NomeComponente & ".PSB"  ???
        
        'Trova_PSB_Dipendente LswCal, LSW, R1!IdOggettoC, Contatore
        Trova_PSB_Dipendente LswCal, LSW, R1!idOggettoC, myCount
        
        R1.MoveNext
        
        If Count > 5 Then Exit Sub
        
      Wend
      If Count > 5 Then
        LSW.ListItems.Add , , "No PSB Linking to this program..."
        'MadrdF_Verifica.txtPsb.Text = ""
        Exit Sub
      End If
      
      R1.Close
    Else
      LSW.ListItems.Add , , "No PSB Linking to this program..."
      'AC
      'MadrdF_Verifica.txtPsb.Text = ""
    End If
  End If
End Sub

Public Function Conta_Righe_File(Percorso As String) As Long
  Dim Righe As Long
  Dim numFile As Long
  Dim Line As String
  
  numFile = FreeFile
  
  'conta le righe
  Open Percorso For Input As #numFile
  
  Do While Not EOF(numFile)
    Line Input #numFile, Line
    Righe = Righe + 1
  Loop
  
  Close #numFile
  
  Conta_Righe_File = Righe
End Function

Public Sub Scrivi_Relazione_PSB(idOggetto As Long, IdPSB As Long, NomePsb As String)
  Dim r As Recordset
  
  'SG: Elimina l'associazione dei psb
  m_fun.FnConnection.Execute "delete  * From PsRel_Obj Where IdOggettoC = " & idOggetto & " And relazione = 'PSB'"
  
  Set r = m_fun.Open_Recordset("Select * From PsRel_Obj Where " & _
                               "IdOggettoC = " & idOggetto & " And IdOggettoR = " & IdPSB & " And Relazione = 'PSB'")
  
  If r.RecordCount = 0 Then
    r.AddNew
  End If
  r!idOggettoC = idOggetto
  r!idOggettoR = IdPSB
  r!Relazione = "PSB"
  r!NomeComponente = NomePsb
  r!Utilizzo = "PSB"
  
  r.Update
  r.Close
End Sub

'''Public Sub check_Move_Op_SSA(WidOggetto As Long)
'''  Dim rs As Recordset
'''  Dim rsSSA As Recordset
'''  Dim cSSA As New Collection
'''  Dim rsArea As Recordset
'''  Dim rsCmp As Recordset
'''  Dim wFound As Boolean
'''  Dim rsLink As Recordset
'''  Dim cValue As String
'''  Dim i As Long
'''
'''  Screen.MousePointer = vbHourglass
'''
'''  Set rs = m_fun.Open_Recordset("Select Distinct NomeSSA From PsDli_IstrDett_liv Where " & _
'''                                "idOggetto = " & WidOggetto & " and nomeSsa <> ''")
'''  Do Until rs.EOF
'''    'Apre la struttura dell'SSA
'''    Set rsArea = m_fun.Open_Recordset("Select * From PsData_Cmp Where " & _
'''                                      "Nome = '" & Trim(rs!NomeSSA) & "' And IdOggetto = " & WidOggetto)
'''    'SG : Ricerca il campo nelle copy linkate da questo id oggetto
'''    If rsArea.RecordCount = 0 Then
'''      Set rsLink = m_fun.Open_Recordset("Select * From PsRel_Obj Where " & _
'''                                        "IdOggettoC = " & WidOggetto & " And Relazione = 'CPY'")
'''      Do Until rsLink.EOF
'''        Set rsCmp = m_fun.Open_Recordset("Select * From PsDATA_Cmp Where " & _
'''                                         "Nome = '" & Trim(rs!NomeSSA) & "' And IdOggetto = " & rsLink!IdOggettoR)
'''        If rsCmp.RecordCount Then
'''           Set rsArea = rsCmp
'''           Exit Do
'''        End If
'''        rsLink.MoveNext
'''      Loop
'''      rsLink.Close
'''    End If
'''
'''    wFound = False
'''    'Ho la struttura dell'SSA
'''    If rsArea.RecordCount Then
'''      Set rsSSA = m_fun.Open_Recordset("Select * From PsData_Cmp Where " & _
'''                                       "IdOggetto = " & rsArea!idOggetto & " And IdArea = " & rsArea!IdArea & _
'''                                       " Order by Ordinale")
'''      Do Until rsSSA.EOF
'''        cValue = Replace(rsSSA!Valore, "'", "")
'''        wFound = False
'''
'''        If Trim(cValue) = "=" Then
'''          wFound = True
'''        ElseIf Trim(cValue) = ">" Then
'''          wFound = True
'''        ElseIf Trim(cValue) = ">=" Then
'''          wFound = True
'''        ElseIf Trim(cValue) = "=>" Then
'''          wFound = True
'''        ElseIf Trim(cValue) = "<" Then
'''          wFound = True
'''        ElseIf Trim(cValue) = "<=" Then
'''          wFound = True
'''        ElseIf Trim(cValue) = "=<" Then
'''          wFound = True
'''        ElseIf Trim(cValue) = "<>" Then
'''          wFound = True
'''        End If
'''
'''        If wFound Then
'''          If Trim(rsSSA!nome) <> "FILLER" Then
'''            wFound = False
'''
'''            For i = 1 To cSSA.count
'''              If Trim(rsSSA!nome) = Trim(cSSA(i)) Then
'''                wFound = True
'''                Exit For
'''              End If
'''            Next i
'''
'''            If Not wFound Then
'''              cSSA.Add Trim(rsSSA!nome)
'''            End If
'''          End If
'''        End If
'''
'''        rsSSA.MoveNext
'''      Loop
'''    End If
'''    rsArea.Close
'''    rs.MoveNext
'''  Loop
'''  rs.Close
'''
'''  'Legge il file riga a riga
'''  Dim Percorso As String
'''  Dim nfile As Long
'''  Dim sLine As String
'''  Dim fLen As Long
'''  Dim wWord As String
'''  Dim NomeFile As String
'''
'''  nfile = FreeFile
'''
'''  Percorso = m_fun.Restituisci_Directory_Da_IdOggetto(WidOggetto)
'''  NomeFile = m_fun.Restituisci_NomeOgg_Da_IdOggetto(WidOggetto)
'''
'''  fLen = FileLen(Percorso & "\" & NomeFile)
'''
'''  Open Percorso & "\" & NomeFile For Input As nfile
'''
'''  Do While Not EOF(nfile)
'''
'''    Line Input #nfile, sLine
'''
'''    If Mid(sLine, 7, 1) <> "*" Then
'''      sLine = Mid(sLine, 1, 72)
'''      sLine = Mid(sLine, 8)
'''
'''      If InStr(1, sLine, "MOVE ") Then
'''        If InStr(1, sLine, " TO ") Then
'''          wWord = Trim(Mid(sLine, InStr(1, sLine, " TO ") + 4)) & " "
'''          wWord = Trim(Mid(wWord, 1, InStr(1, wWord, " ")))
'''          wWord = Trim(Replace(wWord, ".", ""))
'''
'''          For i = 1 To cSSA.count
'''            If wWord = cSSA(i) Then
'''              'Scrive il file
'''              m_fun.WriteLog NomeFile & " --> " & Percorso, , "C:\MoveSSA.txt"
'''              Exit Do
'''            End If
'''          Next i
'''        End If
'''      End If
'''
'''    End If
'''    DoEvents
'''  Loop
'''  Close nfile
'''  Screen.MousePointer = vbDefault
'''End Sub

'silvia 4-2-2008
Sub InsertPSB_from_List()
  Dim i As Long
  Dim Line As String, dialogFileName As String
  Dim isLabel As Boolean
  Dim cParam As Collection
  
  Dim Riga() As String
  Dim objList() As String 'solo il nome � pericoloso: dovra avere anche la directory
  Dim rs As Recordset
  Dim idOggettoC, idOggettoR As Long
  Dim strIns As String
  Dim strPGMnf As String
  Dim strPSBnf As String
  Dim strlog As String
  Dim strlog1 As String
  Dim Componente As String
  Dim Count As Integer
  
  On Error GoTo ErrorHandler
  
  If m_fun.FnProcessRunning Then
    m_fun.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''
  'Gestione inserimento lista oggetti da file
  ''''''''''''''''''''''''''''''''''''''''''''''
  MadrdF_Verifica.ComD.ShowOpen

  'dialogFileName = LoadCommDialog(True, "Select a file", "All Files", "*")
  dialogFileName = MadrdF_Verifica.ComD.FileName
  If Len(dialogFileName) > 0 Then
    objList = readFile(dialogFileName)
    Screen.MousePointer = vbHourglass
    m_fun.FnActiveWindowsBool = True
    m_fun.FnProcessRunning = True
    
    If MsgBox("Do you want to insert the list of " & UBound(objList) & " objects ", vbQuestion + vbYesNo, "Import PSB") = vbYes Then
      For i = 1 To UBound(objList)
        Riga() = Split(objList(i), " ")
        If UBound(Riga()) = 1 Then
          'count = count + 1
          m_fun.FnActiveWindowsBool = False
          
          Set rs = m_fun.Open_Recordset("SELECT * FROM Bs_Oggetti WHERE Nome = '" & Trim(Riga(0)) & "'")
          If rs.RecordCount Then
            idOggettoC = rs!idOggetto
          Else
            idOggettoC = 0
            strPGMnf = strPGMnf & Riga(0) & " (PSB: " & Riga(1) & ")" & vbCrLf
          End If
          rs.Close
          Set rs = m_fun.Open_Recordset("SELECT * FROM Bs_Oggetti WHERE Nome = '" & Trim(Riga(1)) & "'")
          If rs.RecordCount Then
            idOggettoR = rs!idOggetto
            Componente = rs!nome
          Else
            idOggettoR = 0
            strPSBnf = strPSBnf & Riga(1) & " (PGM: " & Riga(0) & ")" & vbCrLf
          End If
          rs.Close
          If idOggettoC > 0 And idOggettoR > 0 Then
            Set rs = m_fun.Open_Recordset("SELECT * FROM PsRel_Obj WHERE idOggettoC=" & idOggettoC & " AND idOggettoR=" & idOggettoR)
            If rs.RecordCount Then
              strlog1 = strlog1 & Riga(0) & " - " & Riga(1) & vbCrLf
            Else
              rs.AddNew
              rs!idOggettoC = idOggettoC
              rs!idOggettoR = idOggettoR
              rs!Relazione = "PSB"
              rs!NomeComponente = Componente
              rs!Utilizzo = "PSB-IMP"
              rs.Update
              strIns = strIns & Riga(0) & " - " & Riga(1) & vbCrLf
            End If
            rs.Close
          End If
        Else
          strlog = strlog & "WRONG LINE : " & objList(i) & vbCrLf
          'DLLFunzioni.WriteLog "!!!!!!!!!!LIST ERROR: WRONG LINE : " & objList(i)
        End If
      Next i
      If strPGMnf <> "" Then
        strPGMnf = "---PGM NOT FOUND---" & vbCrLf & strPGMnf
      End If
      If strPSBnf <> "" Then
        strPSBnf = "---PSB NOT FOUND---" & vbCrLf & strPSBnf
      End If
      If strIns <> "" Then
        strIns = "INSERT RELATION PGM - PSB: " & vbCrLf & strIns
      End If
      If strlog1 <> "" Then
        strlog1 = "Relation already exist from PGM - PSB" & vbCrLf & strlog1
      End If
    
      Screen.MousePointer = vbDefault
      m_fun.Show_ShowText strPGMnf & vbCrLf & strPSBnf & vbCrLf & strIns & vbCrLf & strlog1 & vbCrLf & strlog, "PSB Import Log"
      m_fun.FnActiveWindowsBool = True
      'Screen.MousePointer = vbDefault
      
      m_fun.FnActiveWindowsBool = False
      m_fun.FnProcessRunning = False
      
      End If
    End If
    Screen.MousePointer = vbDefault
  Exit Sub
ErrorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002", "Import PSB", "InsertPSB_from_List", err.Number, err.Description, True
End Sub

Sub InsertPCB_from_List()
  Dim i As Long
  Dim Line As String, dialogFileName As String
  Dim isLabel As Boolean
  Dim cParam As Collection
  
  Dim Riga() As String
  Dim objList() As String 'solo il nome � pericoloso: dovra avere anche la directory
  Dim rs As Recordset
  Dim idPgm As Long, idOggetto As Long
  Dim strIns As String
  Dim strPGMnf As String
  Dim strObjnf As String
  Dim strlog As String
  Dim strlog1 As String
  Dim Componente As String
  Dim Count As Integer
  
  On Error GoTo ErrorHandler
  
  If m_fun.FnProcessRunning Then
    m_fun.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''
  'Gestione inserimento lista oggetti da file
  ''''''''''''''''''''''''''''''''''''''''''''''
  MadrdF_Verifica.ComD.ShowOpen

  'dialogFileName = LoadCommDialog(True, "Select a file", "All Files", "*")
  dialogFileName = MadrdF_Verifica.ComD.FileName
  If Len(dialogFileName) > 0 Then
    objList = readFile(dialogFileName)
    Screen.MousePointer = vbHourglass
    m_fun.FnActiveWindowsBool = True
    m_fun.FnProcessRunning = True
    
    If MsgBox("Do you want to insert the list of " & UBound(objList) & " objects ", vbQuestion + vbYesNo, "Import PSB") = vbYes Then
      For i = 1 To UBound(objList)
        Riga() = Split(objList(i), " ")
        If UBound(Riga()) = 3 Then
          'count = count + 1
          m_fun.FnActiveWindowsBool = False
          
         'leggo id PGm
          Set rs = m_fun.Open_Recordset("SELECT * FROM Bs_Oggetti WHERE Nome = '" & Trim(Riga(0)) & "'")
          If rs.RecordCount Then
            idPgm = rs!idOggetto
            Componente = rs!nome
          Else
            idPgm = 0
            strPGMnf = strPGMnf & Riga(1) & " (PGM: " & Riga(0) & ")" & vbCrLf
          End If
          
          'leggo id Oggetto
          Set rs = m_fun.Open_Recordset("SELECT * FROM Bs_Oggetti WHERE Nome = '" & Trim(Riga(1)) & "'")
          If rs.RecordCount Then
            idOggetto = rs!idOggetto
          Else
            idOggetto = 0
            strObjnf = strObjnf & Riga(0) & " (OBJ: " & Riga(1) & ")" & vbCrLf
          End If
          rs.Close
          
          If idPgm > 0 And idOggetto > 0 Then
            Set rs = m_fun.Open_Recordset("SELECT * FROM PsDLI_Istruzioni WHERE idPgm=" & idPgm & " AND idOggetto=" & idOggetto & " AND Riga=" & Trim(Riga(2)))
            If rs.RecordCount = 0 Then
              strlog1 = strlog1 & Riga(0) & " - " & Riga(1) & " - " & Riga(2) & vbCrLf
            Else
              rs!idPgm = idPgm
              rs!idOggetto = idOggetto
              rs!Riga = Trim(Riga(2))
              rs!ordPcb_MG = Trim(Riga(3))
              rs.Update
              strIns = strIns & Riga(0) & " - " & Riga(1) & " - " & Riga(2) & vbCrLf
            End If
            rs.Close
          End If
        Else
          strlog = strlog & "WRONG LINE : " & objList(i) & vbCrLf
          'DLLFunzioni.WriteLog "!!!!!!!!!!LIST ERROR: WRONG LINE : " & objList(i)
        End If
      Next i
      If strPGMnf <> "" Then
        strPGMnf = "---PGM NOT FOUND---" & vbCrLf & strPGMnf
      End If
      If strObjnf <> "" Then
        strObjnf = "---OBJ NOT FOUND---" & vbCrLf & strObjnf
      End If
      If strIns <> "" Then
        strIns = "UPDATED INSTRUCTION PGM - OBJ: " & vbCrLf & strIns
      End If
      If strlog1 <> "" Then
        strlog1 = "Instruction not exist from PGM - OBJ" & vbCrLf & strlog1
      End If
    
      Screen.MousePointer = vbDefault
      m_fun.Show_ShowText strPGMnf & vbCrLf & strObjnf & vbCrLf & strIns & vbCrLf & strlog1 & vbCrLf & strlog, "PCB Import Log"
      m_fun.FnActiveWindowsBool = True
      'Screen.MousePointer = vbDefault
      
      m_fun.FnActiveWindowsBool = False
      m_fun.FnProcessRunning = False
      
      End If
    End If
    Screen.MousePointer = vbDefault
  Exit Sub
ErrorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002", "Import PCB", "InsertPCB_from_List", err.Number, err.Description, True
End Sub

Sub ExportPCB_from_DB()
  Dim rs As Recordset
  Dim idPgm As Long, idOggetto As Long
  Dim strIns As String, strPGMnf As String, strObjnf As String
  Dim strlog As String
  Dim nomePgm As String, nomeObj As String
  Dim iAnswer As Integer
  Dim iRiga As Integer, iOrdPCBmg As Integer
  Dim outFile As Integer
  
  On Error GoTo ErrorHandler
  
  If m_fun.FnProcessRunning Then
    m_fun.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  m_fun.FnActiveWindowsBool = True
  m_fun.FnProcessRunning = True
  
  iAnswer = MsgBox("Do you want to export the list of all PCB or only PCB manual modified?" & vbCrLf & _
                   "Press YES for All - NO for only Manual - CANCEL to not countinue", vbYesNoCancel, "Export PCB")
  Select Case iAnswer
    Case 6 'SI
      m_fun.FnActiveWindowsBool = False
      ' fare left join --> se nome  = "" segnalo
      'leggo tutti i PCB
      Set rs = m_fun.Open_Recordset("SELECT A.Nome AS NOMEPGM, B.Nome AS NOMEOBJ,C.Riga, C.OrdPCB_mg,C.idpgm,C.idoggetto" & _
                                    " FROM (PsDLI_Istruzioni AS C LEFT JOIN BS_Oggetti AS A ON C.idPgm = A.IdOggetto)" & _
                                    " LEFT JOIN BS_Oggetti AS B ON C.IdOggetto = B.IdOggetto" & _
                                    " WHERE C.IDPGM > 0")
      If rs.RecordCount Then
        outFile = FreeFile
        ' chiedere percorso a Mauino
        Open m_fun.FnPathPrj & "\Documents\txt\ExportPCB" & Replace(Date, "/", "") & ".txt" For Output As outFile
        While Not rs.EOF
          idPgm = rs!idPgm
          idOggetto = rs!idOggetto
          nomePgm = rs!nomePgm
          nomeObj = rs!nomeObj
          iRiga = rs!Riga
          If IsNull(rs!ordPcb_MG) Then
            iOrdPCBmg = 0
          Else
            iOrdPCBmg = rs!ordPcb_MG
          End If
          
          If Trim(nomePgm) = "" Then
            strPGMnf = strPGMnf & "PGM NAME NOT FOUND FOR IDPGM:" & idPgm & vbCrLf
          ElseIf Trim(nomeObj) = "" Then
            strObjnf = strObjnf & "OBJ NAME NOT FOUND FOR IDOBJ:" & idOggetto & vbCrLf
          Else
            Print #outFile, nomePgm & " " & nomeObj & " " & iRiga & " " & iOrdPCBmg
            strIns = strIns & nomePgm & " - " & nomeObj & " - " & iRiga & vbCrLf
          End If
          rs.MoveNext
        Wend
        Close #outFile
      End If
      rs.Close
    Case 7 'NO
      m_fun.FnActiveWindowsBool = False
      'leggo solo quell i caricati a mano
      ' fare left join --> se nome  = "" segnalo
      Set rs = m_fun.Open_Recordset("SELECT A.Nome AS NOMEPGM, B.Nome AS NOMEOBJ,C.Riga, C.OrdPCB_mg,C.idpgm,C.idoggetto" & _
                                    " FROM (PsDLI_Istruzioni AS C LEFT JOIN BS_Oggetti AS A ON C.idPgm = A.IdOggetto)" & _
                                    " LEFT JOIN BS_Oggetti AS B ON C.IdOggetto = B.IdOggetto" & _
                                    " WHERE ORDPCB_MG > 0 AND C.IDPGM > 0")
      If rs.RecordCount Then
        outFile = FreeFile
        ' chiedere percorso a Mauino
        Open m_fun.FnPathPrj & "\Instructions\ExportPCBm" & Replace(Date, "/", "") & ".txt" For Output As outFile
        While Not rs.EOF
          idPgm = rs!idPgm
          idOggetto = rs!idOggetto
          nomePgm = rs!nomePgm
          nomeObj = rs!nomeObj
          iRiga = rs!Riga
          iOrdPCBmg = rs!ordPcb_MG
          
          If Trim(nomePgm) = "" Then
            strPGMnf = strPGMnf & "PGM NAME NOT FOUND FOR IDPGM:" & idPgm & vbCrLf
          ElseIf Trim(nomeObj) = "" Then
            strObjnf = strObjnf & "OBJ NAME NOT FOUND FOR IDOBJ:" & idOggetto & vbCrLf
          Else
            Print #outFile, nomePgm & " " & nomeObj & " " & iRiga & " " & iOrdPCBmg
            strIns = strIns & nomePgm & " - " & nomeObj & " - " & iRiga & vbCrLf
          End If
          rs.MoveNext
        Wend
        Close #outFile
      Else
        strlog = strlog & "No PCB manual modified" & vbCrLf
      End If
      rs.Close
    Case 2 'ANNULLA
      m_fun.FnActiveWindowsBool = False
      m_fun.FnProcessRunning = False
      Screen.MousePointer = vbDefault
      Exit Sub
  End Select
       
  If strPGMnf <> "" Then
    strPGMnf = "---PGM NOT FOUND---" & vbCrLf & _
               strPGMnf
  End If
  If strObjnf <> "" Then
    strObjnf = "---OBJ NOT FOUND---" & vbCrLf & _
               strObjnf
  End If
  If strIns <> "" Then
    strIns = "EXPORTED INSTRUCTION PGM - OBJ: " & vbCrLf & _
             strIns
  End If
  
  Screen.MousePointer = vbDefault
  
  m_fun.Show_ShowText strPGMnf & vbCrLf & _
                      strObjnf & vbCrLf & _
                      strIns & vbCrLf & _
                      strlog, "PCB Export Log"
  m_fun.FnActiveWindowsBool = True
  
  m_fun.FnActiveWindowsBool = False
  m_fun.FnProcessRunning = False

  Screen.MousePointer = vbDefault
  Exit Sub
ErrorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002", "Import PCB", "ExportPCB_from_DB", err.Number, err.Description, True
End Sub

Sub ExportPSB_from_DB()
  Dim i As Long
  Dim Line As String, dialogFileName As String
  Dim isLabel As Boolean
  Dim cParam As Collection
  
  Dim Riga() As String
  Dim objList() As String 'solo il nome � pericoloso: dovra avere anche la directory
  Dim rs As Recordset
  Dim idPgm, idOggetto As Long
  Dim strIns As String
  Dim strPGMnf As String
  Dim strObjnf As String
  Dim strlog As String
  Dim nomePgm As String
  Dim nomeObj As String
  Dim Count As Integer
  Dim outFile As Integer
  
  On Error GoTo ErrorHandler
  
  If m_fun.FnProcessRunning Then
    m_fun.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  m_fun.FnActiveWindowsBool = True
  m_fun.FnProcessRunning = True
   
  If MsgBox("Do you want to export the list of all PSB?", vbYesNo, "Export PSB") = vbYes Then
    m_fun.FnActiveWindowsBool = False
    ' fare left join --> se nome  = "" segnalo
    'leggo tutti i PCB
    Set rs = m_fun.Open_Recordset("SELECT A.Nome AS NOMEPGM, B.Nome AS NOMEOBJ,C.idOggettoC,C.idoggettoR" & _
                                  " FROM (PsRel_Obj AS C LEFT JOIN BS_Oggetti AS A ON C.idOggettoC = A.IdOggetto)" & _
                                  " LEFT JOIN BS_Oggetti AS B ON C.IdOggettoR = B.IdOggetto" & _
                                  " WHERE RELAZIONE = 'PSB'")
    'sono un tot..siamo sicuri che devo exportare tutto????
    If rs.RecordCount Then
      outFile = FreeFile
      ' chiedere percorso a Mauino
      Open m_fun.FnPathPrj & "\Documents\txt\ExportPSB" & Replace(Date, "/", "") & ".txt" For Output As outFile
      While Not rs.EOF
        idPgm = rs!idOggettoC
        idOggetto = rs!idOggettoR
        If Not (IsNull(rs!nomePgm)) Then
          nomePgm = rs!nomePgm
        Else
          nomePgm = ""
        End If
        If Not (IsNull(rs!nomeObj)) Then
          nomeObj = rs!nomeObj
        Else
          nomeObj = ""
        End If
            
        If Trim(nomePgm) = "" Then
          strPGMnf = strPGMnf & "PGM NAME NOT FOUND FOR IDPGM:" & idPgm & vbCrLf
        ElseIf Trim(nomeObj) = "" Then
          strObjnf = strObjnf & "OBJ NAME NOT FOUND FOR IDOBJ:" & idOggetto & vbCrLf
        Else
          Print #outFile, nomePgm & " " & nomeObj
          strIns = strIns & nomePgm & " - " & nomeObj & vbCrLf
        End If
        rs.MoveNext
      Wend
      Close #outFile
    End If
    rs.Close
  Else
    'ANNULLA
    m_fun.FnActiveWindowsBool = False
    m_fun.FnProcessRunning = False
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
  
  If strPGMnf <> "" Then
    strPGMnf = "---PGM NOT FOUND---" & vbCrLf & _
               strPGMnf
  End If
  If strObjnf <> "" Then
    strObjnf = "---OBJ NOT FOUND---" & vbCrLf & _
               strObjnf
  End If
  If strIns <> "" Then
    strIns = "EXPORTED RELATION PGM - OBJ: " & vbCrLf & _
             strIns
  End If

  Screen.MousePointer = vbDefault
  
  m_fun.Show_ShowText strPGMnf & vbCrLf & _
                      strObjnf & vbCrLf & _
                      strIns & vbCrLf & _
                      strlog, "PSB Export Log"
  m_fun.FnActiveWindowsBool = True
  
  m_fun.FnActiveWindowsBool = False
  m_fun.FnProcessRunning = False

  Screen.MousePointer = vbDefault
  Exit Sub
ErrorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002", "Import PCB", "ExportPSB_from_DB", err.Number, err.Description, True
End Sub

'MF - 11-07-07
'Gestione lettura da file
''''''''''''''''''''''''''''''''''''''''''''''
Public Function readFile(dialogFileName As String) As String()
  Dim values() As String
  Dim FD As Long
  Dim Line As String
  
  ReDim values(0)
  
  FD = FreeFile
  Open dialogFileName For Input As FD
  While Not EOF(FD)
    Line Input #FD, Line
    If Len(Trim(Line)) Then
      ReDim Preserve values(UBound(values) + 1)
      values(UBound(values)) = Trim(Line)
    Else
      'riga vuota: buttiamo via
    End If
  Wend
  Close FD
    
  readFile = values
End Function
