VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadmdF_PropertyTable 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Table Properties: "
   ClientHeight    =   7125
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   10455
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7125
   ScaleWidth      =   10455
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdUP 
      Caption         =   "/\"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   9960
      TabIndex        =   32
      Tag             =   "fixed"
      ToolTipText     =   "Move Up"
      Top             =   1350
      Width           =   420
   End
   Begin VB.CommandButton cmdDOWN 
      Caption         =   "\/"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   9960
      TabIndex        =   31
      Tag             =   "fixed"
      ToolTipText     =   "Move Down"
      Top             =   1980
      Width           =   420
   End
   Begin VB.Frame Frame1 
      Caption         =   "Routine Templates:"
      ForeColor       =   &H00C00000&
      Height          =   1035
      Left            =   40
      TabIndex        =   24
      Top             =   6000
      Width           =   10365
      Begin VB.TextBox TxtRead 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   6960
         TabIndex        =   29
         Top             =   540
         Width           =   3300
      End
      Begin VB.TextBox TxtWrite 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   3555
         TabIndex        =   27
         Top             =   540
         Width           =   3300
      End
      Begin VB.TextBox TxTLoad 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   150
         TabIndex        =   25
         Top             =   540
         Width           =   3300
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Read:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   6960
         TabIndex        =   30
         Top             =   270
         Width           =   435
      End
      Begin VB.Label Label42 
         AutoSize        =   -1  'True
         Caption         =   "Write:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   3570
         TabIndex        =   28
         Top             =   270
         Width           =   570
      End
      Begin VB.Label Label41 
         AutoSize        =   -1  'True
         Caption         =   "Load:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   150
         TabIndex        =   26
         Top             =   300
         Width           =   435
      End
   End
   Begin VB.TextBox txtCommento 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   6030
      MaxLength       =   255
      TabIndex        =   20
      Text            =   " "
      Top             =   5010
      Width           =   4335
   End
   Begin VB.TextBox TxtDef 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   3720
      MaxLength       =   26
      TabIndex        =   19
      Text            =   " "
      Top             =   5010
      Width           =   1155
   End
   Begin VB.ComboBox cboType 
      BackColor       =   &H00FFFFC0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   360
      ItemData        =   "MadmdF_PropertyTable.frx":0000
      Left            =   1020
      List            =   "MadmdF_PropertyTable.frx":000A
      TabIndex        =   17
      Top             =   4500
      Width           =   2295
   End
   Begin VB.ComboBox CmbFormat 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   315
      ItemData        =   "MadmdF_PropertyTable.frx":0017
      Left            =   1020
      List            =   "MadmdF_PropertyTable.frx":0019
      Style           =   1  'Simple Combo
      TabIndex        =   9
      Top             =   5520
      Width           =   3855
   End
   Begin VB.TextBox txtDecimal 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   2670
      MaxLength       =   1
      TabIndex        =   8
      Text            =   " "
      Top             =   5010
      Width           =   615
   End
   Begin VB.TextBox txtDim 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1020
      MaxLength       =   9
      TabIndex        =   7
      Text            =   " "
      Top             =   5010
      Width           =   945
   End
   Begin VB.TextBox txtLabel 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   6030
      MaxLength       =   255
      TabIndex        =   6
      Text            =   " "
      Top             =   5520
      Width           =   4335
   End
   Begin VB.TextBox txtDescrizioneCol 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   885
      Left            =   6030
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   3990
      Width           =   4335
   End
   Begin VB.ComboBox CmbNull 
      BackColor       =   &H00FFFFC0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   360
      ItemData        =   "MadmdF_PropertyTable.frx":001B
      Left            =   3690
      List            =   "MadmdF_PropertyTable.frx":0025
      TabIndex        =   4
      Top             =   4500
      Width           =   1185
   End
   Begin VB.TextBox txtNomeCol 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1020
      TabIndex        =   3
      Top             =   3990
      Width           =   3855
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   10455
      _ExtentX        =   18441
      _ExtentY        =   688
      ButtonWidth     =   2011
      ButtonHeight    =   635
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "New"
            Key             =   "NEWCOL"
            Object.ToolTipText     =   "Create new column..."
            ImageIndex      =   6
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Erase"
            Key             =   "ERASE"
            Object.ToolTipText     =   "Erase current Column"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Index"
            Key             =   "IDX"
            Object.ToolTipText     =   "Index viewer"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Idx Ref"
            Key             =   "IDXCOLUMN"
            ImageIndex      =   7
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8820
      Top             =   2820
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_PropertyTable.frx":0032
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_PropertyTable.frx":018C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_PropertyTable.frx":02E6
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_PropertyTable.frx":0440
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_PropertyTable.frx":059A
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_PropertyTable.frx":06F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_PropertyTable.frx":084E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lswTbStruct 
      Height          =   3495
      Left            =   30
      TabIndex        =   0
      Top             =   420
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   6165
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   16777152
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Field Name"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Type"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Description"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Description"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   4000
      TabIndex        =   23
      Top             =   0
      Width           =   975
   End
   Begin VB.Label lblComment 
      AutoSize        =   -1  'True
      Caption         =   "Comment"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   5070
      TabIndex        =   22
      Top             =   5070
      Width           =   915
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Description"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   0
      TabIndex        =   21
      Top             =   0
      Width           =   975
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Def"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   3330
      TabIndex        =   18
      Top             =   5070
      Width           =   345
   End
   Begin VB.Label Label31 
      AutoSize        =   -1  'True
      Caption         =   "Format"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   60
      TabIndex        =   16
      Top             =   5580
      Width           =   915
   End
   Begin VB.Label Label28 
      AutoSize        =   -1  'True
      Caption         =   "Type"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   60
      TabIndex        =   15
      Top             =   4530
      Width           =   915
   End
   Begin VB.Label Label27 
      AutoSize        =   -1  'True
      Caption         =   "Decimal"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   2010
      TabIndex        =   14
      Top             =   5070
      Width           =   645
   End
   Begin VB.Label Label29 
      AutoSize        =   -1  'True
      Caption         =   "Length"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   60
      TabIndex        =   13
      Top             =   5070
      Width           =   915
   End
   Begin VB.Label Label30 
      AutoSize        =   -1  'True
      Caption         =   "Null"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   3330
      TabIndex        =   12
      Top             =   4560
      Width           =   360
   End
   Begin VB.Label Label32 
      AutoSize        =   -1  'True
      Caption         =   "Description"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   5070
      TabIndex        =   11
      Top             =   4080
      Width           =   915
   End
   Begin VB.Label Label33 
      AutoSize        =   -1  'True
      Caption         =   "Label"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   5070
      TabIndex        =   10
      Top             =   5595
      Width           =   915
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Field Name"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   75
      TabIndex        =   2
      Top             =   4050
      Width           =   915
   End
End
Attribute VB_Name = "MadmdF_PropertyTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim pLoad As Boolean

Private Sub cboType_Click()
   Dim rs As ADODB.Recordset
  
   'Va in update sul db
   Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
   
   If rs.RecordCount > 0 Then
      
      rs!Tipo = cboType.text
      
      rs.Update
      rs.Close
      
      lswTbStruct.SelectedItem.ListSubItems(1).text = cboType.text
   Else
      'Errore : Deve essersi perso l'indice
      Stop
   End If

End Sub

Private Sub cboType_KeyPress(KeyAscii As Integer)
   KeyAscii = 0
End Sub

Private Sub CmbFormat_Change()
   Dim rs As ADODB.Recordset
   
   If pLoad = False Then
      pStatus_Column = True
   End If
   
   'Va in update sul db
   Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
   
   If rs.RecordCount > 0 Then
      
      rs!Formato = CmbFormat.text
      
      rs.Update
      rs.Close
      
      lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).text = txtNomeCol.text
   Else
      'Errore : Deve essersi perso l'indice
      
   End If
End Sub

Private Sub CmbNull_Click()
   Dim rs As ADODB.Recordset
  
   'Va in update sul db
   Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
   
   If rs.RecordCount > 0 Then
      
       If Trim(UCase(cmbNull.text)) = "YES" Then
          rs!Null = True
       Else
          rs!Null = False
       End If
      
      rs.Update
      rs.Close
      
      If pLoad = False Then
         pStatus_Column = True
      End If
   Else
      'Errore : Deve essersi perso l'indice
      
   End If
     
End Sub

Private Sub CmbNull_KeyPress(KeyAscii As Integer)
   KeyAscii = 0
End Sub

Private Sub cmdUP_Click()
Dim app1a As String, app1b As String
Dim app2a As String, app2b As String
Dim app3a As String, app3b As String
Dim app4a As String, app4b As String
Dim app5a As String, app5b As String
Dim app6a As String, app7b As String

    pOrdinal_Column = True
    If lswTbStruct.ListItems.count > 0 Then
        If lswTbStruct.ListItems.count > 1 And lswTbStruct.SelectedItem.Index <> 1 Then
            app1a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1)
            app1b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index)
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).text = app1a
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).text = app1b
             
            app2a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).SubItems(1)
            app2b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(1)
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(1) = app2a
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).SubItems(1) = app2b
             
            app3a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).SubItems(2)
            app3b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(2)
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(2) = app3a
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).SubItems(2) = app3b

            app4a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).Tag
            app4b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Tag
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Tag = app4a
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).Tag = app4b

            app5a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).Key
            app5b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Key
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Key = app5a & "X"
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).Key = app5b
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Key = app5a
'
'            app6a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).SubItems(5)
'            app6b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(5)
'             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(5) = app6a
'             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).SubItems(5) = app6b
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Selected = False
             lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index - 1).Selected = True
            lswTbStruct.Refresh
        End If
    End If
End Sub

Private Sub cmdDOWN_Click()
Dim app1a As String, app1b As String
Dim app2a As String, app2b As String
Dim app3a As String, app3b As String
Dim app4a As String, app4b As String
Dim app5a As String, app5b As String
Dim app6a As String, app7b As String

    pOrdinal_Column = True
    If lswTbStruct.ListItems.count > 0 Then
      If lswTbStruct.ListItems.count > 1 And lswTbStruct.SelectedItem <> lswTbStruct.ListItems(lswTbStruct.ListItems.count) Then
        app1a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1)
        app1b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index)
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).text = app1a
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).text = app1b
         
        app2a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).SubItems(1)
        app2b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(1)
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(1) = app2a
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).SubItems(1) = app2b
         
        app3a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).SubItems(2)
        app3b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(2)
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(2) = app3a
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).SubItems(2) = app3b

        app4a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).Tag
        app4b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Tag
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Tag = app4a
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).Tag = app4b

        app5a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).Key
        app5b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Key
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Key = app5a & "X"
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).Key = app5b
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Key = app5a
'
'        app6a = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).SubItems(5)
'        app6b = lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(5)
'         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).SubItems(5) = app6a
'         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).SubItems(5) = app6b
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).Selected = False
         lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index + 1).Selected = True
        lswTbStruct.Refresh
      End If
    End If
End Sub

Private Sub Form_Load()
   pLoad = True
   pForeign_Colonna_IsUsed = False
   
   'Setta il parent della form
   'SetParent Me.hwnd, DataMan.DmParent
  
   'ResizeControls Me 'ALE
   
   Me.Caption = "DB-Engine: " & TipoDB & " - Table Properties. Table: [" & pNome_TBCorrente & "] - DB: [" & pNome_DBCorrente & "]"
   
   'resize lista
   'lswTbStruct.ColumnHeaders(3).Width = lswTbStruct.Width - lswTbStruct.ColumnHeaders(1).Width - lswTbStruct.ColumnHeaders(2).Width - 270
   
   lswTbStruct.ColumnHeaders(3).Width = withTot(lswTbStruct)
   
   'Carica la lista delle colonne
   Load_Lista_Colonne

   'Carica Combo tipo secondo il tipo DB
   Carica_Combo_Tipo TipoDB, cboType
   
   If lswTbStruct.ListItems.count > 0 Then
      lswTbStruct_ItemClick lswTbStruct.ListItems(1)
   End If
   
   'Avvia la transazione
   On Error Resume Next
       DataMan.DmConnection.CommitTrans
   On Error GoTo 0
   DataMan.DmConnection.BeginTrans
   pStatus_Column = False
   pLoad = False
   
   m_fun.AddActiveWindows Me
End Sub


Public Sub Load_Lista_Colonne()
   Dim i As Long
   Dim IndexImageKey As Long
      'IndexImageKey
      'PK = 1
      'SK = 2 (KEY)
      'CLUSTERING = 0(Solo DB2)
   Dim rs As ADODB.Recordset
   
   lswTbStruct.ListItems.Clear
   
   Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & _
                                                          "_Colonne Where IdTable = " & pIndex_TbCorrente & _
                                                          " Order by Ordinale")
   
   If rs.RecordCount > 0 Then
      While Not rs.EOF
         IndexImageKey = get_Tipo_Key(rs!Tipo)
         
         If IndexImageKey = 1 Then 'PRIMARY-KEY
            lswTbStruct.ListItems.Add , rs!IdColonna & "#IDCOL", rs!Nome, , IndexImageKey
         Else
            lswTbStruct.ListItems.Add , rs!IdColonna & "#IDCOL", rs!Nome
         End If
         lswTbStruct.ListItems(lswTbStruct.ListItems.count).Tag = rs!IdTable & "#IDTAB"
         lswTbStruct.ListItems(lswTbStruct.ListItems.count).ListSubItems.Add , , TN(rs!Tipo)
         lswTbStruct.ListItems(lswTbStruct.ListItems.count).ListSubItems.Add , , TN(rs!Descrizione)
         
         rs.MoveNext
      Wend
   End If
   
   rs.Close
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
   'Richiesta di salvataggio
   Dim wScelta As Long
   
   If pStatus_Column = True Then
      wScelta = MsgBox("Do you want to save this table structure?", vbQuestion + vbYesNo, DataMan.DmNomeProdotto)
      
      If wScelta = vbYes Then
         DataMan.DmConnection.CommitTrans
      Else
         DataMan.DmConnection.RollbackTrans
      End If
   End If
   
   If pOrdinal_Column = True Then
      If MsgBox("Do you want save the Columns in this Order? ", vbQuestion + vbYesNo, DataMan.DmNomeProdotto) = vbYes Then
        updateOrdinal
      End If
      pOrdinal_Column = False
   End If
   
End Sub
Public Sub updateOrdinal()
''''
Dim i As Long
Dim rs As New Recordset
Dim IdColonna As String
Dim IdTable As String
Dim pos As Long

  For i = 1 To lswTbStruct.ListItems.count
    pos = InStr(lswTbStruct.ListItems(i).Key, "#IDCOL")
    IdColonna = Mid(lswTbStruct.ListItems(i).Key, 1, pos - 1)
    pos = InStr(lswTbStruct.ListItems(i).Tag, "#IDTAB")
    IdTable = Mid(lswTbStruct.ListItems(i).Tag, 1, pos - 1)
    Set rs = m_fun.Open_Recordset("Select Ordinale From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdTable = " & IdTable & " and IdColonna = " & IdColonna)
    rs!Ordinale = i
    rs.Update
    rs.Close
  Next
    
End Sub
Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
   m_fun.RemoveActiveWindows Me
   
   SetFocusWnd m_fun.FnParent
End Sub

Private Sub lswTbStruct_ItemClick(ByVal Item As MSComctlLib.ListItem)
   'Ricerca all'interno della classe la colonna e spara le info in mappa
   Dim i As Long
   Dim cId As Long
   Dim rs As ADODB.Recordset
   
   pIndex_ColonnaCorrente = Mid(Item.Key, 1, InStr(1, Item.Key, "#") - 1)
   pNome_ColonnaCorrente = Item.text
   
   Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne " & _
                                           " Where IdColonna = " & pIndex_ColonnaCorrente)
   
   If rs.RecordCount > 0 Then
      txtDecimal = TN(rs!Decimali)
      txtDescrizioneCol = TN(rs!Descrizione)
      txtDim = TN(rs!lunghezza)
      txtLabel = TN(rs!Etichetta)
      'txtCommento = TN(rs!Commento)
      TxtDef = TN(rs!defaultValue)
      TxTLoad = TN(rs!RoutLoad)
      txtNomeCol = TN(rs!Nome)
      TxtRead = TN(rs!RoutRead)
      TxtWrite = TN(rs!RoutWrite)
      CmbFormat.text = TN(rs!Formato)
      cboType.text = TN(rs!Tipo)
      
      If TN(rs!Null) = True Then
         cmbNull.ListIndex = 0
      Else
         cmbNull.ListIndex = 1
      End If
   End If
   
   rs.Close
   
   'Controlla se la colonna � usata come chiave Foreign
   Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_IdxCol " & _
                                          "Where ForIdColonna = " & pIndex_ColonnaCorrente)
                                          
   If rs.RecordCount > 0 Then
      Toolbar1.Buttons(6).Enabled = True
      pForeign_Colonna_IsUsed = True
   Else
      Toolbar1.Buttons(6).Enabled = False
      pForeign_Colonna_IsUsed = False
   End If
   
   rs.Close
   
   If pForeign_Colonna_IsUsed = False Then
      'Controlla anche se � usata come un semplice indice
      Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_IdxCol " & _
                                             "Where IdColonna = " & pIndex_ColonnaCorrente)
                                             
      If rs.RecordCount > 0 Then
         Toolbar1.Buttons(6).Enabled = True
         pIndexKey_Colonna_IsUsed = True
      Else
         Toolbar1.Buttons(6).Enabled = False
         pIndexKey_Colonna_IsUsed = False
      End If
      
      rs.Close
   End If
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case Trim(UCase(Button.Key))
      
      Case "NEWCOL"
         pStatus_Column = True
         Create_New_Colonna
         
      Case "ERASE"
         pStatus_Column = True
         Erase_Colonna
         
      Case "IDX"
         'MadmdF_Index.Show
         SetParent MadmdF_Index.hwnd, formParentResize.hwnd
         MadmdF_Index.Show
         MadmdF_Index.Move 0, 0, formParentResize.Width, formParentResize.Height
      
      Case "IDXCOLUMN"
         'MadmdF_IdxColumn.Show vbModal
         SetParent MadmdF_IdxColumn.hwnd, formParentResize.hwnd
         SetParent MadmdF_Index.hwnd, formParentResize.hwnd
         MadmdF_IdxColumn.Show
         MadmdF_IdxColumn.Move 0, 0, formParentResize.Width, formParentResize.Height

         
         
   End Select
End Sub

Public Sub Erase_Colonna()
   Dim wScelta As Long
   
   If pForeign_Colonna_IsUsed = True Or pIndexKey_Colonna_IsUsed = True Then
      wScelta = MsgBox("WARNING!!! This Column is used to define an index," & vbCrLf & "Do you want to erase current Column?", vbQuestion + vbYesNo, DataMan.DmNomeProdotto)
   Else
      wScelta = MsgBox("Do you want to erase current Column?", vbQuestion + vbYesNo, DataMan.DmNomeProdotto)
   End If
   
   If wScelta = vbYes Then
      'Elimina la colonna corrente Nel Database
      Elimina_Colonna pIndex_ColonnaCorrente, pIndex_TbCorrente
      
      'Elimina a Video la colonna
      lswTbStruct.ListItems.Remove (lswTbStruct.SelectedItem.Index)
      
      If lswTbStruct.ListItems.count > 0 Then
         lswTbStruct_ItemClick lswTbStruct.ListItems(1)
         lswTbStruct.SetFocus
         lswTbStruct.SelectedItem.EnsureVisible
      End If
   End If
End Sub

Public Sub Create_New_Colonna()
   Dim i As Long
   Dim rs As ADODB.Recordset
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
'''   Select Case Trim(UCase(TipoDB))
'''      Case "DB2"
'''         PrefDb = "DMDB2"
'''      Case "ORACLE"
'''         PrefDb = "DMORC"
'''      Case "VSAM"
'''         PrefDb = "DMVSAM"
'''   End Select
   
   'Trova il max id da mettere per le colonne
   i = get_IdColonna_Libero
   
   Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne Where IdTable = " & pIndex_TbCorrente)
   
   rs.AddNew
      rs!IdColonna = i
      rs!IdTable = pIndex_TbCorrente
      rs!Ordinale = rs.RecordCount + 1
      rs!idTableJoin = pIndex_TableJoinCorrente
      rs!IdCmp = -1
      rs!Nome = "NEWCOLUMN" & Format(i, "0000")
      rs!lunghezza = 0
      rs!Decimali = 0
      rs!Formato = ""
      rs!Etichetta = ""
      rs!Null = False
      rs!Descrizione = "Put here column's description..."
      rs!Tag = ""
      rs!Tipo = "VARCHAR"
      rs!RoutLoad = ""
      rs!RoutRead = ""
      rs!RoutWrite = ""
   rs.Update
   rs.Close

   
   'Aggiorna il video
   lswTbStruct.ListItems.Add , i & "#IDCOL", "NEWCOLUMN" & Format(i, "0000")
   lswTbStruct.ListItems(lswTbStruct.ListItems.count).ListSubItems.Add , , "VARCHAR"
   lswTbStruct.ListItems(lswTbStruct.ListItems.count).ListSubItems.Add , , "Put here column's description..."
End Sub

Public Function get_OrdinaleColonna(idx As Long) As Long
   Dim rs As ADODB.Recordset
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
'''   Select Case Trim(UCase(TipoDB))
'''      Case "DB2"
'''         PrefDb = "DMDB2"
'''      Case "ORACLE"
'''         PrefDb = "DMORC"
'''      Case "VSAM"
'''         PrefDb = "DMVSAM"
'''   End Select
   
   Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne Where IdColonna = " & idx)
   
   If rs.RecordCount > 0 Then
      get_OrdinaleColonna = rs.RecordCount
   Else
      get_OrdinaleColonna = 0
   End If
   rs.Close
End Function

Public Function get_IdColonna_Libero()
  Dim rs As Recordset
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
   
  'SQ ?? DataMan.DmConnection.BeginTrans
   
   Set rs = m_fun.Open_Recordset("Select MAX(IdColonna) From " & PrefDb & "_Colonne")
'
'   rs.AddNew
'      rs!Nome = "GETMAXID"
'   rs.Update
'
'   IdFree = rs!IdTable
   get_IdColonna_Libero = ("0" & rs.fields(0).Value) + 1
   
   'Cancella
   'SQ ?? DataMan.DmConnection.RollbackTrans
   
   rs.Close
   
End Function

Private Sub txtDecimal_KeyUp(KeyCode As Integer, Shift As Integer)
   Dim rs As ADODB.Recordset
   Dim Adesso As String
   Dim txtdecnum As Long
   txtdecnum = val(txtDecimal)
   txtDecimal = Trim(str(txtdecnum))
   
   Select Case KeyCode
        Case 13
        Case Else
           pStatus_Column = True
           'Va in update sul db
           Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
           
           If rs.RecordCount > 0 Then
              Adesso = Now
              
              rs!Decimali = txtDecimal.text
              
              
              rs.Update
              rs.Close
              
           Else
              'Errore : Deve essersi perso l'indice del db
              Stop
           End If
     End Select
End Sub

Private Sub TxtDef_KeyUp(KeyCode As Integer, Shift As Integer)
   Dim rs As ADODB.Recordset
   Dim Adesso As String
   
   Select Case KeyCode
        Case 13
        Case Else
           pStatus_Column = True
           'Va in update sul db
           Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
           
           If rs.RecordCount > 0 Then
              Adesso = Now
              
              rs!default = TxtDef.text
              
              
              rs.Update
              rs.Close
              
           Else
              'Errore : Deve essersi perso l'indice del db
              Stop
           End If
     End Select

End Sub

Private Sub txtDescrizioneCol_KeyUp(KeyCode As Integer, Shift As Integer)
   Dim rs As ADODB.Recordset
   
   Select Case KeyCode
        Case 13
        Case Else
           pStatus_Column = True
           'Va in update sul db
           Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
           
           If rs.RecordCount > 0 Then
     
              rs!Descrizione = txtDescrizioneCol.text
             
              rs.Update
              rs.Close
              
              lswTbStruct.SelectedItem.ListSubItems(2).text = txtDescrizioneCol.text
           Else
              'Errore : Deve essersi perso l'indice
              Stop
           End If
     End Select
End Sub

Private Sub txtDim_KeyUp(KeyCode As Integer, Shift As Integer)
   Dim rs As ADODB.Recordset
   Dim TxtDimNum As Long
   
   TxtDimNum = val(txtDim)
   txtDim = Trim(str(TxtDimNum))
   Select Case KeyCode
        Case 13
        Case Else
           pStatus_Column = True
           'Va in update sul db
           Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
           
           If rs.RecordCount > 0 Then

              rs!lunghezza = txtDim.text
              
              
              rs.Update
              rs.Close
              
           Else
              'Errore : Deve essersi perso l'indice del db
              Stop
           End If
     End Select
End Sub

Private Sub txtLabel_KeyUp(KeyCode As Integer, Shift As Integer)
   Dim rs As ADODB.Recordset
   
   Select Case KeyCode
        Case 13
        Case Else
           pStatus_Column = True
           'Va in update sul db
           Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
           
           If rs.RecordCount > 0 Then
 
              rs!Etichetta = txtLabel.text
              
              
              rs.Update
              rs.Close
              
           Else
              'Errore : Deve essersi perso l'indice del db
              Stop
           End If
     End Select
End Sub

Private Sub TXTLoad_KeyUp(KeyCode As Integer, Shift As Integer)
   Dim rs As ADODB.Recordset
  
   Select Case KeyCode
        Case 13
        Case Else
           pStatus_Column = True
           'Va in update sul db
           Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
           
           If rs.RecordCount > 0 Then

              rs!RoutLoad = TxTLoad.text
              
              
              rs.Update
              rs.Close
              
           Else
              'Errore : Deve essersi perso l'indice del db
              Stop
           End If
     End Select
End Sub

Private Sub txtNomeCol_KeyUp(KeyCode As Integer, Shift As Integer)
   Dim rs As ADODB.Recordset
   
   Select Case KeyCode
        Case 13
        Case Else
           pStatus_Column = True
           'Va in update sul db
           Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
           
           If rs.RecordCount > 0 Then
              
              rs!Nome = txtNomeCol.text
              
              rs.Update
              rs.Close
              
              lswTbStruct.ListItems(lswTbStruct.SelectedItem.Index).text = txtNomeCol.text
           Else
              'Errore : Deve essersi perso l'indice
              Stop
           End If
     End Select
End Sub

Private Sub TxtRead_KeyUp(KeyCode As Integer, Shift As Integer)
   Dim rs As ADODB.Recordset
   
   Select Case KeyCode
        Case 13
        Case Else
           pStatus_Column = True
           'Va in update sul db
           Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
           
           If rs.RecordCount > 0 Then
              
              rs!RoutRead = TxtRead.text
              
              
              rs.Update
              rs.Close
              
           Else
              'Errore : Deve essersi perso l'indice del db
              Stop
           End If
     End Select
End Sub

Private Sub TxtWrite_KeyUp(KeyCode As Integer, Shift As Integer)
   Dim rs As ADODB.Recordset

   Select Case KeyCode
        Case 13
        Case Else
           pStatus_Column = True
           'Va in update sul db
           Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Colonne Where IdColonna = " & pIndex_ColonnaCorrente)
           
           If rs.RecordCount > 0 Then

              rs!RoutWrite = TxtWrite.text
              
              
              rs.Update
              rs.Close
              
           Else
              'Errore : Deve essersi perso l'indice del db
              Stop
           End If
     End Select
End Sub
