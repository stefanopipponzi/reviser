VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form MadrdF_GenRout 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " IMS/DB - Routine Generator"
   ClientHeight    =   8370
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   11385
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   11385
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.ListView lswRout 
      Height          =   915
      Left            =   420
      TabIndex        =   53
      Top             =   9000
      Visible         =   0   'False
      Width           =   1995
      _ExtentX        =   3519
      _ExtentY        =   1614
      SortKey         =   3
      View            =   3
      SortOrder       =   -1  'True
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      Enabled         =   0   'False
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Decodifica"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Codifica"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "DBD"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Key             =   "Routine"
         Text            =   "Routine"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Details"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Routine for:"
      ForeColor       =   &H00C00000&
      Height          =   1215
      Left            =   7650
      TabIndex        =   47
      Top             =   480
      Width           =   2415
      Begin VB.OptionButton optType 
         Caption         =   "Selected DBD"
         ForeColor       =   &H00C00000&
         Height          =   195
         Index           =   0
         Left            =   270
         TabIndex        =   50
         Top             =   260
         Width           =   1365
      End
      Begin VB.OptionButton optType 
         Caption         =   "Selected PGM"
         ForeColor       =   &H00C00000&
         Height          =   195
         Index           =   1
         Left            =   270
         TabIndex        =   49
         Top             =   560
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton optType 
         Caption         =   "ALL Instructions"
         ForeColor       =   &H00C00000&
         Height          =   195
         Index           =   2
         Left            =   270
         TabIndex        =   48
         Top             =   860
         Width           =   1575
      End
   End
   Begin VB.Frame FrameSlotMachine 
      Height          =   5475
      Left            =   60
      TabIndex        =   42
      Top             =   480
      Width           =   7515
      Begin MSComctlLib.ListView lswSlotMachine 
         Height          =   4875
         Left            =   45
         TabIndex        =   46
         Top             =   120
         Width           =   7380
         _ExtentX        =   13018
         _ExtentY        =   8599
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "COD"
            Text            =   "Decodes"
            Object.Width           =   7708
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "INSTR"
            Text            =   "Instructions"
            Object.Width           =   7708
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "DBD"
            Text            =   "DBD"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Routine"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.CommandButton cmdReport 
         Caption         =   "Report"
         Enabled         =   0   'False
         Height          =   300
         Left            =   6240
         TabIndex        =   52
         Top             =   5100
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdUpdate 
         Caption         =   "Edit"
         Height          =   300
         Left            =   1920
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   5100
         Width           =   1035
      End
      Begin VB.CommandButton cmdInsert 
         Caption         =   "Add"
         Height          =   300
         Left            =   3120
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   5100
         Width           =   1005
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Delete"
         Height          =   300
         Left            =   4320
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   5100
         Width           =   1005
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Instruction Filter"
      ForeColor       =   &H00C00000&
      Height          =   1215
      Left            =   10140
      TabIndex        =   40
      Top             =   480
      Width           =   1185
      Begin VB.ListBox lstIstr 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   960
         Left            =   60
         Style           =   1  'Checkbox
         TabIndex        =   41
         Top             =   180
         Width           =   1065
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "DB"
      Height          =   405
      Left            =   7680
      TabIndex        =   37
      Top             =   5700
      Width           =   3615
      Begin VB.OptionButton optVSAM 
         Caption         =   "VSAM"
         ForeColor       =   &H00400000&
         Height          =   255
         Left            =   2580
         TabIndex        =   51
         Top             =   120
         Width           =   855
      End
      Begin VB.OptionButton optDB2 
         Caption         =   "DB2"
         ForeColor       =   &H00400000&
         Height          =   255
         Left            =   480
         TabIndex        =   39
         Top             =   120
         Value           =   -1  'True
         Width           =   795
      End
      Begin VB.OptionButton optOra 
         Caption         =   "ORACLE"
         ForeColor       =   &H00400000&
         Height          =   255
         Left            =   1380
         TabIndex        =   38
         Top             =   120
         Width           =   975
      End
   End
   Begin RichTextLib.RichTextBox Rtb 
      Height          =   375
      Left            =   5880
      TabIndex        =   32
      Top             =   2880
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MadrdF_GenRout.frx":0000
   End
   Begin RichTextLib.RichTextBox RtbOccCopy 
      Height          =   375
      Left            =   1680
      TabIndex        =   31
      Top             =   7320
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MadrdF_GenRout.frx":0082
   End
   Begin RichTextLib.RichTextBox RtbInsert 
      Height          =   375
      Left            =   3360
      TabIndex        =   30
      Top             =   7320
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MadrdF_GenRout.frx":010B
   End
   Begin RichTextLib.RichTextBox RtbSqlAppo 
      Height          =   375
      Left            =   6600
      TabIndex        =   29
      Top             =   7320
      Visible         =   0   'False
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MadrdF_GenRout.frx":0193
   End
   Begin RichTextLib.RichTextBox RtbDisp 
      Height          =   375
      Left            =   360
      TabIndex        =   28
      Top             =   7320
      Visible         =   0   'False
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MadrdF_GenRout.frx":021C
   End
   Begin RichTextLib.RichTextBox RtbUpdate 
      Height          =   495
      Left            =   3240
      TabIndex        =   27
      Top             =   7680
      Visible         =   0   'False
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      _Version        =   393217
      TextRTF         =   $"MadrdF_GenRout.frx":02A2
   End
   Begin RichTextLib.RichTextBox RtbOccurs 
      Height          =   375
      Left            =   1680
      TabIndex        =   26
      Top             =   7800
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MadrdF_GenRout.frx":032A
   End
   Begin RichTextLib.RichTextBox RtbCiclaLivello 
      Height          =   375
      Left            =   5280
      TabIndex        =   25
      Top             =   7320
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MadrdF_GenRout.frx":03B2
   End
   Begin RichTextLib.RichTextBox RtbCiclica 
      Height          =   375
      Left            =   5040
      TabIndex        =   24
      Top             =   7680
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MadrdF_GenRout.frx":0440
   End
   Begin RichTextLib.RichTextBox RtbPerform 
      Height          =   375
      Left            =   1560
      TabIndex        =   23
      Top             =   7440
      Visible         =   0   'False
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MadrdF_GenRout.frx":04C9
   End
   Begin RichTextLib.RichTextBox RtbRedCopy 
      Height          =   375
      Left            =   480
      TabIndex        =   22
      Top             =   7320
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MadrdF_GenRout.frx":0552
   End
   Begin RichTextLib.RichTextBox RtbRed 
      Height          =   375
      Left            =   600
      TabIndex        =   21
      Top             =   7800
      Visible         =   0   'False
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MadrdF_GenRout.frx":05DB
   End
   Begin RichTextLib.RichTextBox RtbSql 
      Height          =   375
      Left            =   6360
      TabIndex        =   20
      Top             =   7680
      Visible         =   0   'False
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MadrdF_GenRout.frx":0660
   End
   Begin VB.ListBox LstFunction 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   1635
      Left            =   7680
      Style           =   1  'Checkbox
      TabIndex        =   17
      Top             =   6660
      Width           =   3645
   End
   Begin RichTextLib.RichTextBox RtbDel 
      Height          =   315
      Left            =   3480
      TabIndex        =   16
      Top             =   7320
      Visible         =   0   'False
      Width           =   1425
      _ExtentX        =   2514
      _ExtentY        =   556
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MadrdF_GenRout.frx":06E5
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   11385
      _ExtentX        =   20082
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   12
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Object.Visible         =   0   'False
            Key             =   "Camera"
            Object.ToolTipText     =   "Prepare Routines Generation"
            ImageKey        =   "Camera"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SlotMachine"
            Object.ToolTipText     =   "Slot Machine"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Forward"
            Object.ToolTipText     =   "Routines Generation"
            ImageKey        =   "Forward"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Movie"
            Object.ToolTipText     =   "Photo && Routines Generation"
            ImageKey        =   "Movie"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "CHECK_RT_IN"
            Object.ToolTipText     =   "Check/Manage Routines"
            ImageKey        =   "Check"
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Text_Edit"
            Object.ToolTipText     =   "View with Text Editor"
            ImageKey        =   "Edit"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Edit_Instr"
                  Text            =   "View Instruction"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Edit_Rout"
                  Text            =   "View Routine"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   3
            Style           =   3
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Instr_Pgm"
            Object.ToolTipText     =   "Instructions Reports"
            ImageIndex      =   8
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Instr_Pgm"
                  Text            =   "List Cross Pgm - Instructions"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Report_Instr"
                  Text            =   "Report Instruction Details"
               EndProperty
            EndProperty
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTBErr2 
      Height          =   1155
      Left            =   45
      TabIndex        =   13
      Top             =   7170
      Width           =   7515
      _ExtentX        =   13256
      _ExtentY        =   2037
      _Version        =   393217
      BackColor       =   16777152
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   10000
      TextRTF         =   $"MadrdF_GenRout.frx":0756
   End
   Begin RichTextLib.RichTextBox RTBErr 
      Height          =   1095
      Left            =   45
      TabIndex        =   12
      Top             =   6000
      Width           =   7515
      _ExtentX        =   13256
      _ExtentY        =   1931
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   10000
      TextRTF         =   $"MadrdF_GenRout.frx":07D8
   End
   Begin RichTextLib.RichTextBox RtLog 
      Height          =   2505
      Left            =   7650
      TabIndex        =   8
      Top             =   2760
      Width           =   3675
      _ExtentX        =   6482
      _ExtentY        =   4419
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   3
      TextRTF         =   $"MadrdF_GenRout.frx":085A
   End
   Begin RichTextLib.RichTextBox RTBR 
      Height          =   285
      Left            =   3750
      TabIndex        =   7
      Top             =   2910
      Visible         =   0   'False
      Width           =   1155
      _ExtentX        =   2037
      _ExtentY        =   503
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MadrdF_GenRout.frx":08DC
   End
   Begin MSComctlLib.TreeView TreeView2 
      Height          =   5385
      Left            =   3720
      TabIndex        =   6
      Top             =   480
      Visible         =   0   'False
      Width           =   3795
      _ExtentX        =   6694
      _ExtentY        =   9499
      _Version        =   393217
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   5385
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Visible         =   0   'False
      Width           =   3465
      _ExtentX        =   6112
      _ExtentY        =   9499
      _Version        =   393217
      Indentation     =   1058
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   4935
      Top             =   5190
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_GenRout.frx":094B
            Key             =   "Camera"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_GenRout.frx":0A5D
            Key             =   "Forward"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_GenRout.frx":0B6F
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_GenRout.frx":0D49
            Key             =   "Movie"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_GenRout.frx":1063
            Key             =   "Check"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_GenRout.frx":20B5
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_GenRout.frx":345E7
            Key             =   "Edit"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_GenRout.frx":370F1
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_GenRout.frx":37543
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTB_Trig 
      Height          =   375
      Left            =   8040
      TabIndex        =   33
      Top             =   7380
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MadrdF_GenRout.frx":3924D
   End
   Begin VB.Frame Frame2 
      Caption         =   "Language"
      Height          =   405
      Left            =   7680
      TabIndex        =   34
      Top             =   6195
      Width           =   3612
      Begin VB.OptionButton OptPLI 
         Caption         =   "PLI"
         Height          =   192
         Left            =   2340
         TabIndex        =   36
         Top             =   150
         Width           =   735
      End
      Begin VB.OptionButton Optother 
         Caption         =   "Cobol"
         Height          =   192
         Left            =   1020
         TabIndex        =   35
         Top             =   150
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.Label Label13 
      Alignment       =   2  'Center
      Caption         =   "/"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   10560
      TabIndex        =   19
      Top             =   5370
      Width           =   165
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   0
      TabIndex        =   18
      Top             =   0
      Width           =   555
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Total Instructions:"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   7650
      TabIndex        =   15
      Top             =   1800
      Width           =   2415
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   10740
      TabIndex        =   11
      Top             =   5370
      Width           =   555
   End
   Begin VB.Label Label10 
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Processed Instructions:"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   7650
      TabIndex        =   10
      Top             =   5370
      Width           =   2265
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   9990
      TabIndex        =   9
      Top             =   5370
      Width           =   555
   End
   Begin VB.Label txtDupInstr 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   10140
      TabIndex        =   4
      Top             =   2370
      Width           =   1185
   End
   Begin VB.Label Label5 
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Duplicate Instructions:"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   7650
      TabIndex        =   3
      Top             =   2370
      Width           =   2415
   End
   Begin VB.Label txtProcInstr 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   10140
      TabIndex        =   2
      Top             =   2070
      Width           =   1185
   End
   Begin VB.Label Label3 
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Processed Instructions:"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   7650
      TabIndex        =   1
      Top             =   2070
      Width           =   2415
   End
   Begin VB.Label txtTotInstr 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   10140
      TabIndex        =   0
      Top             =   1800
      Width           =   1185
   End
End
Attribute VB_Name = "MadrdF_GenRout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'SQ: utilizzare questo metodo:
'Const ..._CHK = 0
'Const ..._CHK = 1
'Const ..._CHK = 2
'Const ..._CHK = 3
Const ROUT_ONLY_CHK = 4
Const SELECT_STAR_CHK = 5
' Mauro 12/03/2010
Const OLD_METHOD = 6
Const EXEC_METHOD = 7
' Mauro 11/06/2008
Const TABLE_JOIN = 8

'SQ 17-08-07
Const SELECTED_DBD_OPT = 0
Const SELECTED_PGM_OPT = 1
Const ALL_INSTRUCTIONS_OPT = 2

'SQ casino! c'� gi� "DB": Private TipoDb As String

'SQ 30-08-07 (forse TMP)...
Private isSlotMachine As Boolean

'MF - 31-08-07 Per listView della SlotMachine
Private FieldNames() As Field

'silvia
Dim OnlySelected As Boolean
Dim StrFilename As String

Private Type Field
  Decodes As String
  Instruction As String
End Type

Private Sub cmdDelete_Click()
  Dim i As Long, Count As Long
  Dim risp As Integer
  Dim rs As Recordset
  Dim rsCloni As Recordset, rsMod As Recordset
  
  Dim rispMgDLI As Integer
  On Error GoTo errorHandler
  
  ' Mauro 08/11/2007 : Cancellazione istruzioni su Repository
  risp = MsgBox("Do you want to delete these instructions also on Repository?", _
                vbYesNo + vbInformation + vbDefaultButton2, MadrdF_GenRout.Caption)
  For i = lswSlotMachine.ListItems.Count To 1 Step -1
    If lswSlotMachine.ListItems(i).Selected Then
      If risp = vbYes Then
        Set rs = m_fun.Open_Recordset("select * from mgdli_decodificaistr where " & _
                                      "codifica = '" & lswSlotMachine.ListItems(i).ListSubItems(1) & "'")
        If rs.RecordCount Then
          ' Mauro 25/01/2008 : Notifico le cancellazioni anche sulla Tabella Modifiche-Cloni
          Set rsCloni = m_fun.Open_Recordset("select * from psdli_istrcodifica where " & _
                                             "codifica = '" & lswSlotMachine.ListItems(i).ListSubItems(1) & "'")
          Do Until rsCloni.EOF
            Set rsMod = m_fun.Open_Recordset("select * from mgdli_istrcodifica where " & _
                              "idpgm = " & rsCloni!idPgm & " and idoggetto = " & rsCloni!IdOggetto & " and " & _
                              "riga = " & rsCloni!Riga & " and decodifica = '" & lswSlotMachine.ListItems(i) & "'")
            If rsMod.RecordCount Then
              Do Until rsMod.EOF
                ' Controllo se � stato inserito a mano
                If rsMod!Status = "A" Then
                  rsMod.Delete
                End If
                rsMod.MoveNext
              Loop
            Else
              rsMod.AddNew
              
              rsMod!idPgm = rsCloni!idPgm
              rsMod!IdOggetto = rsCloni!IdOggetto
              rsMod!Riga = rsCloni!Riga
              rsMod!Decodifica = rs!Decodifica
              rsMod!Status = "D" ' Delete
              
              rsMod.Update
            End If
            rsMod.Close
            rsCloni.MoveNext
          Loop
          rsCloni.Close
          rs.Delete
        End If
        rs.Close
        ' Controllo se ci sono altri programmi che usano il clone...
        Set rs = m_fun.Open_Recordset("select * from psdli_istrcodifica where " & _
                                      "codifica = '" & lswSlotMachine.ListItems(i).ListSubItems(1) & "'")
                                      
        rispMgDLI = 0
        If rs.RecordCount = 0 Then
          ' Non ci sono altri programmi che utilizzano il clone num! Vuoi eliminare la decodifica?
          rispMgDLI = MsgBox("Instruction " & lswSlotMachine.ListItems(i).ListSubItems(1) & " isn't used by other programs! Do you want to delete decode?", _
                              vbYesNo + vbInformation + vbDefaultButton2, MadrdF_GenRout.Caption)
          If rispMgDLI = vbYes Then
            ' ...Cancello la decodifica
            m_fun.FnConnection.Execute "delete * from mgdli_decodificaistr where " & _
                                       "codifica = '" & lswSlotMachine.ListItems(i).ListSubItems(1) & "'"
          End If
        Else
          'Il clone num � utilizzata in altri tot programmi! Vuoi cancellare tutti i cloni e la decodifica?
          rispMgDLI = MsgBox("Instruction " & lswSlotMachine.ListItems(i).ListSubItems(1) & " is used by other " & rs.RecordCount & " programs! Do you want to delete them all?", _
                              vbYesNo + vbInformation + vbDefaultButton2, MadrdF_GenRout.Caption)
          If rispMgDLI = vbYes Then
            ' ...Cancello la decodifica
            m_fun.FnConnection.Execute "delete * from mgdli_decodificaistr where " & _
                                       "codifica = '" & lswSlotMachine.ListItems(i).ListSubItems(1) & "'"
            m_fun.FnConnection.Execute "delete * from psdli_istrcodifica where " & _
                                       "codifica = '" & lswSlotMachine.ListItems(i).ListSubItems(1) & "'"
          End If
        End If
        rs.Close
      End If
      lswSlotMachine.ListItems.Remove (i)
      Count = Count + 1
    End If
  Next i
  txtTotInstr = txtTotInstr - Count
  
  Exit Sub
errorHandler:
  MsgBox err.Number & " " & err.Description
End Sub

Private Sub cmdInsert_Click()
  Madrdf_EditDecod.addInstr
End Sub

Private Sub cmdReport_Click()
  Dim risp As String
  
  risp = MsgBox("Do you want to print the selected instructions only?", vbYesNoCancel + vbQuestion, Dli2Rdbms.drNomeProdotto)
  OnlySelected = risp = vbYes
  
  If risp <> vbCancel Then
    StrFilename = Dli2Rdbms.drPathDb & "\Documents\Excel\" & Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Istr.txt"
    ReportExceIstrDLI
    
    'SQ dava errore se non c'erano righe!
    Rows("1:1").Select
    With ActiveWindow
      .SplitColumn = 0
      .SplitRow = 1
    End With
    ActiveWindow.FreezePanes = True
    
    Screen.MousePointer = vbNormal
  End If
End Sub

Private Sub cmdUpdate_Click()
  Dim i As Long

  If lswSlotMachine.ListItems.Count Then
    If lswSlotMachine.SelectedItem Is Nothing Then
      MsgBox "You must select a object in a list, before...", vbInformation, "System Object"
      Exit Sub
    Else
      Madrdf_EditDecod.editInstr lswSlotMachine.SelectedItem
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  ' Ridimensionamento dei controlli SlotMachine
  lswSlotMachine.ColumnHeaders(1).Width = lswSlotMachine.Width * 0.6
  lswSlotMachine.ColumnHeaders(2).Width = lswSlotMachine.Width * 0.15
  lswSlotMachine.ColumnHeaders(3).Width = lswSlotMachine.Width * 0.1
  lswSlotMachine.ColumnHeaders(4).Width = lswSlotMachine.Width * 0.1
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub lstIstr_ItemCheck(item As Integer)
  Dim i As Integer
  
  'Gestione "ALL"
  If item = 0 Then
    If lstIstr.Selected(item) Then
      For i = 1 To lstIstr.ListCount - 1
        lstIstr.Selected(i) = False
      Next i
    End If
  Else
    lstIstr.Selected(0) = False 'deselezione "ALL"
  End If
End Sub

Private Sub lswSlotMachine_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswSlotMachine.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswSlotMachine.SortOrder = Order
  lswSlotMachine.SortKey = key - 1
  lswSlotMachine.Sorted = True
End Sub

Private Sub lswSlotMachine_DblClick()
  cmdUpdate_Click
End Sub

Private Sub optDB2_Click()
  'SQ casino! c'� gi� "DB": TipoDb = "DMDB2_"
  db = "DMDB2_"
  TipoDB = "DB2"
End Sub

Private Sub optOra_Click()
  'SQ casino! c'� gi� "DB": TipoDb = "DMORC_"
  db = "DMORC_"
  TipoDB = "Oracle"
End Sub

Private Sub LstFunction_ItemCheck(item As Integer)
  'ATTENZIONE, POSIZIONALE: pericolosissimo!
  'MadrdM_Incapsulatore.EmbeddedRoutine = LstFunction.Selected(EMBEDDED_CHK)
  'If LstFunction.ListCount > EMBEDDED_CHK Then
  '  EmbeddedRoutine = LstFunction.Selected(EMBEDDED_CHK)
  'End If
   
  Select Case item
    Case 0
      LstFunction.Selected(1) = False
    Case 1
      LstFunction.Selected(0) = False
    Case 2
      LstFunction.Selected(3) = False
    Case 3
      LstFunction.Selected(2) = False
    Case ROUT_ONLY_CHK
      RoutineOnly = LstFunction.Selected(ROUT_ONLY_CHK)
    Case SELECT_STAR_CHK
      SelectStar = LstFunction.Selected(SELECT_STAR_CHK)
    Case OLD_METHOD
      isOldMethod = LstFunction.Selected(OLD_METHOD)
    Case EXEC_METHOD
      isExec = LstFunction.Selected(EXEC_METHOD)
    ' Mauro 11/06/2008
    Case TABLE_JOIN
      isJoin = LstFunction.Selected(TABLE_JOIN)
  End Select
End Sub

Private Sub Optother_Click()
  RoutLang = ""
End Sub

Private Sub OptPLI_Click()
  RoutLang = "PLI"
End Sub

Private Sub optVSAM_Click()
  db = "DMDB2_"
  TipoDB = "VSAM"
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim i As Integer
  
  On Error GoTo ErrNext
  
  Select Case Button.key
    'gloria 23/02/2010: non pi� utilizzato
'    Case "Camera"
'      isSlotMachine = False
'      FrameSlotMachine.Visible = False
'      'Label7.Visible = True
'      'Label8.Visible = True
'      TreeView1.Visible = True
'      TreeView2.Visible = True
'      getPhoto
'      End If
    Case "SlotMachine"
      Elimina_Flickering MadrdF_GenRout.hwnd
      Screen.MousePointer = vbHourglass
      'MF 31-08-07 Gestione pulsante SlotMachine
      isSlotMachine = True
      lswSlotMachine.ListItems.Clear
      ' Ridimensionamento dei controlli SlotMachine
      lswSlotMachine.ColumnHeaders(1).Width = lswSlotMachine.Width * 0.6
      lswSlotMachine.ColumnHeaders(2).Width = lswSlotMachine.Width * 0.15
      lswSlotMachine.ColumnHeaders(3).Width = lswSlotMachine.Width * 0.1
      lswSlotMachine.ColumnHeaders(4).Width = lswSlotMachine.Width * 0.1
      FrameSlotMachine.Visible = True
      TreeView1.Visible = False
      TreeView2.Visible = False
      
      If lstIstr.Selected(0) Then
        getDecodes ""
      Else
        If lstIstr.SelCount Then
          For i = 1 To lstIstr.ListCount - 1
            If lstIstr.Selected(i) Then
              getDecodes Left(lstIstr.list(i) & "....", 4)
            End If
          Next i
        Else
          MsgBox "No instruction selected from ""Instruction Filter"" ", vbInformation, "i-4.Migration"
          lstIstr.SetFocus
        End If
      End If
      Screen.MousePointer = vbDefault
      Terminate_Flickering
    Case "Forward"
      getEnvironmentParameters
      ' Mauro 17/01/2008 : Nuova gestione Template Routine gi� esistenti
      isOnce = True
      isInstrTemplate = False
      isRoutTemplate = False
      ' Mauro 06/11/2008
      isCondGE = IIf(m_fun.LeggiParam("IMSDB_CONDGE") = "No", False, True)
      ' Mauro 05/10/2009
      isWithHold = IIf(m_fun.LeggiParam("IMSDB-WITHHOLD") = "No", False, True)
      ' Mauro 22/07/2009
      ReDim arrCRS_(0)
      ReDim arrWK_PLev(0)
      
      If isSlotMachine Then
        routinesGeneration_SlotMachine
      'Else
      '  routinesGeneration_Photo
      End If
    Case "Movie"
'''      '''''''''''''''''''''''''''
'''      ' SQ 17-08-07
'''      ' Photo + Generazione
'''      '''''''''''''''''''''''''''
'''      getPhoto
'''      '+
'''      routinesGeneration_Photo
      Elimina_Flickering MadrdF_GenRout.hwnd
      Screen.MousePointer = vbHourglass
      'MF 31-08-07 Gestione pulsante SlotMachine
      isSlotMachine = True
      lswSlotMachine.ListItems.Clear
      ' Ridimensionamento dei controlli SlotMachine
      lswSlotMachine.ColumnHeaders(1).Width = lswSlotMachine.Width * 0.6
      lswSlotMachine.ColumnHeaders(2).Width = lswSlotMachine.Width * 0.15
      lswSlotMachine.ColumnHeaders(3).Width = lswSlotMachine.Width * 0.1
      lswSlotMachine.ColumnHeaders(4).Width = lswSlotMachine.Width * 0.1
      FrameSlotMachine.Visible = True
      TreeView1.Visible = False
      TreeView2.Visible = False
      If lstIstr.Selected(0) Then
        getDecodes ""
      Else
        If lstIstr.SelCount Then
          For i = 1 To lstIstr.ListCount - 1
            If lstIstr.Selected(i) Then
              getDecodes Left(lstIstr.list(i) & "....", 4)
            End If
          Next i
        Else
          MsgBox "No instruction selected from ""Instruction Filter"" ", vbInformation, "i-4.Migration"
          lstIstr.SetFocus
        End If
      End If
      Screen.MousePointer = vbDefault
      Terminate_Flickering
      
      For i = 1 To lswSlotMachine.ListItems.Count
        lswSlotMachine.ListItems(i).Selected = True
      Next i
  
      'lswSlotMachine.Refresh
      ' Con questo comando le ordino per Routine
      lswSlotMachine_ColumnClick lswSlotMachine.ColumnHeaders(2)
          
      getEnvironmentParameters
      ' Mauro 17/01/2008 : Nuova gestione Template Routine gi� esistenti
      isOnce = True
      isInstrTemplate = False
      isRoutTemplate = False
      ' Mauro 06/11/2008
      isCondGE = IIf(m_fun.LeggiParam("IMSDB_CONDGE") = "No", False, True)
      ' Mauro 05/10/2009
      isWithHold = IIf(m_fun.LeggiParam("IMSDB-WITHHOLD") = "No", False, True)
      ' Mauro 22/07/2009
      ReDim arrCRS_(0)
      ReDim arrWK_PLev(0)
      
      If isSlotMachine Then
        routinesGeneration_SlotMachine
      'Else
      '  routinesGeneration_Photo
      End If
    Case "CHECK_RT_IN"
      '''''''''''''''''''''''''''
      'SQ 9-08-07
      ' Analisi INCAPSULATI/ROUTINES: COERENZA ROUTINE
      '''''''''''''''''''''''''''
      MadrdF_Check_Routine.Show vbModal
    Case "Text_Edit"
      If lswSlotMachine.ListItems.Count Then
        For i = 1 To lswSlotMachine.ListItems.Count
          If lswSlotMachine.ListItems(i).Selected Then
            Text_Editor_Instr lswSlotMachine.ListItems(i).ListSubItems(1)
            Exit For
          End If
        Next i
      End If
    Case "Instr_Pgm"
      Report_Instr
  End Select
  
  Exit Sub
ErrNext:
  If err.Number = 35602 Then
    Resume Next
  Else
    AggLog err.Description, MadrdF_GenRout.RTBErr
    Resume Next
  End If
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Dim i As Integer
    
  On Error GoTo errorHandler
  Screen.MousePointer = vbHourglass
   
  Select Case ButtonMenu.key
    Case "Edit_Instr"
      If lswSlotMachine.ListItems.Count Then
        For i = 1 To lswSlotMachine.ListItems.Count
          If lswSlotMachine.ListItems(i).Selected Then
            Text_Editor_Instr lswSlotMachine.ListItems(i).ListSubItems(1)
            Exit For
          End If
        Next i
      End If
    Case "Edit_Rout"
      If lswSlotMachine.ListItems.Count Then
        For i = 1 To lswSlotMachine.ListItems.Count
          If lswSlotMachine.ListItems(i).Selected Then
            Text_Editor_Rout lswSlotMachine.ListItems(i).ListSubItems(1), lswSlotMachine.ListItems(i).ListSubItems(3)
            Exit For
          End If
        Next i
      End If
    Case "Instr_Pgm"
      ListaIstr
    Case "Report_Instr"
      Report_Instr
  End Select
  Screen.MousePointer = vbNormal
  
  Exit Sub
errorHandler:
  If err.Number = 35602 Then
    Resume Next
  Else
    AggLog err.Description, MadrdF_GenRout.RTBErr
    Resume Next
  End If
End Sub

Private Sub ListaIstr()
  Dim appExcel As New Excel.Application, cartExcel As Excel.Workbook, foglioExcel As Excel.Worksheet
  Dim nomeFoglio As String, FileName As String
  Dim decCount As Integer, i As Integer
  
  Set cartExcel = appExcel.Workbooks.Add
  decCount = 0
  For i = 1 To lswSlotMachine.ListItems.Count
    If lswSlotMachine.ListItems(i).Selected Then
      nomeFoglio = lswSlotMachine.ListItems(i).ListSubItems(1)
      decCount = decCount + 1
      Set foglioExcel = cartExcel.Worksheets.Add
      foglioExcel.name = nomeFoglio
      appExcel.Visible = True
      foglioExcel.Cells(1, 1) = "Instruction"
      foglioExcel.Cells(1, 2) = "io-routine"
      foglioExcel.Cells(1, 3) = "Program"
      foglioExcel.Cells(1, 4) = "Copy"
      foglioExcel.Cells(1, 5) = "Row"
      foglioExcel.Rows("2:2").Select
      appExcel.ActiveWindow.FreezePanes = True
      foglioExcel.Columns("A:A").ColumnWidth = 12
      foglioExcel.Columns("B:B").ColumnWidth = 11
      foglioExcel.Columns("C:C").ColumnWidth = 16
      foglioExcel.Columns("D:D").ColumnWidth = 13
      foglioExcel.Columns("E:E").ColumnWidth = 15
      foglioExcel.Rows("1:1").Select
      appExcel.Selection.Font.Bold = True
      foglioExcel.Range("A1:E1").Select
      foglioExcel.Range("A1:E1").AutoFilter
      With appExcel.Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorLight2
        .TintAndShade = 0.399975585192419
        .PatternTintAndShade = 0
      End With
      
      foglioExcel.Cells.Select
      With appExcel.Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
      End With
      setFoglio foglioExcel, nomeFoglio, appExcel
    End If
  Next i
  
  If decCount > 0 Then
    appExcel.Sheets("Foglio1").Select
    appExcel.ActiveWindow.SelectedSheets.Delete
    appExcel.Sheets("Foglio2").Select
    appExcel.ActiveWindow.SelectedSheets.Delete
    appExcel.Sheets("Foglio3").Select
    appExcel.ActiveWindow.SelectedSheets.Delete
    FileName = Dli2Rdbms.drPathDb & "\Documents\excel\" & Format(Now, "dd.mm.yyyy.hh.mm.ss") & ".pgm"
    cartExcel.SaveAs FileName
  Else
    MsgBox "Please, select one or more instruction from the list." & vbCrLf & _
           "- Use 'Instruction Filters' in order to select the instructions to be loaded.", vbInformation, "i-4.Migration"
  End If
End Sub

Public Sub setFoglio(foglioExcel As Excel.Worksheet, nomeFoglio As String, appExcel As Excel.Application)
  Dim rs As Recordset
  Dim i As Integer, w As Integer
  
  w = 1
  i = 1
  Set rs = m_fun.Open_Recordset("SELECT a.codifica as Instr, a.NomeRout as Rout, b.nome as Pgm, d.nome as Copy, a.riga as Riga FROM " & _
                                "psDli_istrCodifica as a, Bs_Oggetti as b, MgDLI_DecodificaIstr as c, Bs_Oggetti as d WHERE " & _
                                "c.codifica = '" & nomeFoglio & "' and a.idPgm = b.idoggetto And a.Codifica = c.Codifica And " & _
                                "a.idOggetto = d.idOggetto ORDER BY a.codifica")
  While Not rs.EOF
    If w Mod 2 = 1 Then    'controlla quali sono dispari
      foglioExcel.Range(w + 1 & ":" & w + 1).Select
      With appExcel.Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorDark1
        .TintAndShade = 0
        .PatternTintAndShade = 0
      End With
    Else
      foglioExcel.Range(w + 1 & ":" & w + 1).Select
      With appExcel.Selection.Interior
        .Pattern = xlSolid
        .PatternColorIndex = xlAutomatic
        .Color = 49407
        .TintAndShade = 0
        .PatternTintAndShade = 0
      End With
    End If

    foglioExcel.Cells(i + 1, 1) = rs!InStr
    foglioExcel.Cells(i + 1, 2) = rs!Rout
    foglioExcel.Cells(i + 1, 3) = rs!PGM
    If rs!PGM = rs!Copy Then
      foglioExcel.Cells(i + 1, 4) = "'-"
    Else
      foglioExcel.Cells(i + 1, 4) = rs!Copy
    End If
    foglioExcel.Cells(i + 1, 5) = rs!Riga

    w = w + 1
    i = i + 1
    rs.MoveNext
  Wend
  rs.Close
End Sub

Private Sub Form_Load()
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
  
  LstFunction.AddItem "Generate RDBMS Routines"
  LstFunction.AddItem "Generate DLI Routines"
  LstFunction.AddItem "Use CALL method into routines"
  LstFunction.AddItem "Use EXEC method into routines"
  LstFunction.AddItem "Generate 'Routine File' Only"
  LstFunction.AddItem "Use of 'SELECT *'"
  ' Mauro 12/03/2010
  LstFunction.AddItem "Generate routines in OLD VERSION"
  ' Mauro 08/04/2008
  LstFunction.AddItem "Generate routines for EXEC method"
  ' Mauro 11/06/2008
  LstFunction.AddItem "Generate routines with JOIN"
  
  LstFunction.Selected(0) = True
  LstFunction.Selected(2) = True
  'SQ 17-08-07
  LstFunction.Selected(ROUT_ONLY_CHK) = True
  ' Mauro 11/06/2008
  LstFunction.Selected(TABLE_JOIN) = False
  ' Mauro 12/03/2010
  LstFunction.Selected(OLD_METHOD) = False
  
  'SQ 17-08-07 - Filtro Istruzioni
  lstIstr.AddItem """ALL"""
  Dim rs As Recordset
  Set rs = m_fun.Open_Recordset("SELECT DISTINCT left(codifica,4) as istruzione FROM MgDli_DecodificaIstr WHERE " & _
                                "ImsDB ORDER BY 1")
  While Not rs.EOF
    If Len(rs!Istruzione) Then
      lstIstr.AddItem Replace(rs!Istruzione, ".", "")
    End If
    rs.MoveNext
  Wend
  rs.Close
  'Inizio: "ALL"
  lstIstr.Selected(0) = True
  'Come si visualizza il primo!?
  'lstIstr
  
  'DEFAULT DB:
  optDB2_Click
  optDB2.Value = True
  
  'DEFAULT LINGUAGGIO:
  Optother_Click
  RoutLang = ""
  
  '''''''''''''''''
  ' Tutti i "LeggiParam" necessari: letti una volta sola!
  ' SQ 30-08-06
  '''''''''''''''''
  getEnvironmentParameters
  ' Mauro 12/05/2008
  If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
    LstFunction.Selected(EXEC_METHOD) = True
  End If
  
  'MF 31-08-07 Gestione pulsante SlotMachine
  isSlotMachine = True
  lswSlotMachine.ListItems.Clear
  ' Ridimensionamento dei controlli SlotMachine
  lswSlotMachine.ColumnHeaders(1).Width = lswSlotMachine.Width * 0.6
  lswSlotMachine.ColumnHeaders(2).Width = lswSlotMachine.Width * 0.15
  lswSlotMachine.ColumnHeaders(3).Width = lswSlotMachine.Width * 0.1
  lswSlotMachine.ColumnHeaders(4).Width = lswSlotMachine.Width * 0.1
  FrameSlotMachine.Visible = True
  TreeView1.Visible = False
  TreeView2.Visible = False
End Sub

'''SP 2/6/2006 - festa della repubblica
'''Ex FORM
''Public Sub getPhoto()
''  Dim inCondition As String
''  Dim rsIstruzioni As Recordset
''  Dim Testo As String, testo2 As String, testo3 As String
''  Dim vPos As Long
''  Dim k As Integer, k1 As Integer, k2 As Integer
''
''  Dim wRout As String, wdb As String
''  Dim Swoper As Boolean, Swprog As Boolean, SwOpe As Boolean, SwOpx As Boolean, existNode As Boolean
''  Dim wIstrDec As String
''  Dim wOnum As Integer
''  'Dim xwIstr As String, xTrsope As String
''
''  Dim idField As Long
''  Dim sDbd() As Long
''
''  Dim wProgramma As Variant
''
''  Dim wEx As Long
''  Dim wFlag As Boolean
''  Dim oldIstr As String ', currentType As String,
''  Dim instructionFilter As String
''
''  Dim xdnomeseg As String, xdDb As String, xdKeyField As String, xdsrchFields As String
''  Dim isbolinkeylevel As Boolean
''  On Error GoTo errRout
''
''  RtLog.text = ""
''  RTBErr.text = ""
''  RTBErr2.text = ""
''
''  wOnum = 0
''  TreeView1.Nodes.Clear
''  TreeView2.Nodes.Clear
''
''  AggLog "Starting Instructions Analysis...", RtLog
''  'currentType = IIf(Optother, "CBL", "PLI")
''
''  'riazzero il flag di generazione
''  'serve solo per verifica generazione istruzione, in quanto la
''  'lettura ora � ordinata per istruzione; per ora viene lasciato, da togliere
''  ' per velocizzare
''  'm_fun.FnConnection.Execute "Update MgDli_decodificaIstr Set Generato = false Where ImsDb = true"
''
''  txtTotInstr = 0
''  txtProcInstr = 0
''  txtDupInstr = 0
''
''  Screen.MousePointer = vbHourglass
''
''  Dim indOgg As Integer
''
''  'SQ 17-08-07 - filtro ISTRUZIONI:
''  If lstIstr.Selected(0) Then
''    'Set rsIstruzioni = m_fun.Open_Recordset("select distinct a.idPgm,a.idOggetto,a.riga,a.istruzione,a.keyfeedback,Codifica,b.NomeRout from PsDLI_Istruzioni as a, PsDLI_IstrCodifica as b where a.idPgm=b.idPgm AND a.IdOggetto=b.IdOggetto AND a.riga=b.riga and Valida and ImsDB order by codifica, a.idPgm, a.idoggetto, a.riga")
''    instructionFilter = ""  '"ALL": no filter
''  Else
''    If lstIstr.SelCount Then
''      instructionFilter = " AND z.istruzione IN ("
''      For k = 1 To lstIstr.ListCount - 1
''        If lstIstr.Selected(k) Then
''          instructionFilter = instructionFilter & "'" & lstIstr.list(k) & "',"
''        End If
''      Next k
''      instructionFilter = Left(instructionFilter, Len(instructionFilter) - 1) & ")" 'rimozione ultima virgola + parentesi IN condition
''    Else
''      MsgBox "No instruction selected from ""Instruction Filter"" ", vbInformation, "i-4.Migration"
''      lstIstr.SetFocus
''      Screen.MousePointer = vbNormal
''      Exit Sub
''    End If
''  End If
''
''  If optType.item(0) Then
''    ''''''''''''''''''''''''''''''
''    ' DBD:
''    ''''''''''''''''''''''''''''''
''    For indOgg = 1 To Dli2Rdbms.drObjList.ListItems.Count
''      If Dli2Rdbms.drObjList.ListItems(indOgg).Selected Then
''        If Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ) = "DBD" Then
''          inCondition = inCondition & Dli2Rdbms.drObjList.ListItems(indOgg) & ","
''        Else
''          MsgBox Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(NAME_OBJ) & " is not a DBD", vbExclamation
''          Screen.MousePointer = 0
''          Exit Sub
''        End If
''      End If
''    Next indOgg
''    If Len(inCondition) Then
''      'rimuovo l'ultima virgola:
''      inCondition = Left(inCondition, Len(inCondition) - 1)
''      'SQ ??? non funge! ha l'ORDER BY SU a.idOggetto,a.riga che non ho nella distinct!!!!
''      'Set rsIstruzioni = m_fun.Open_Recordset("select distinct b.*,Codifica,NomeRout from PsDLI_IstrDBD as a,PsDLI_Istruzioni as b,PsDLI_IstrCodifica as c where a.idDBD in (" & inCondition & ") AND a.IdOggetto=b.IdOggetto AND a.riga=b.riga AND a.IdOggetto=c.IdOggetto AND a.riga=c.riga And Valida and ImsDB order by codifica, a.idoggetto, a.riga")
''      Set rsIstruzioni = m_fun.Open_Recordset( _
''                         "select distinct z.*,Codifica,c.nomerout " & _
''                         "from PsDLI_IstrDBD as a,PsDLI_Istruzioni as z,PsDLI_IstrCodifica as c WHERE " & _
''                         "a.idDBD in (" & inCondition & ") AND " & _
''                         "a.IdOggetto = z.IdOggetto AND a.riga = z.riga AND a.idpgm = z.idpgm and " & _
''                         "a.IdOggetto = c.IdOggetto AND a.riga = c.riga And a.Idpgm = c.Idpgm and " & _
''                         "Valida and ImsDB " & instructionFilter & " ORDER BY c.nomerout, codifica")
''    Else
''      MsgBox "No DBDs selected", vbInformation, "i-4.Migration"
''      Screen.MousePointer = vbNormal
''      Exit Sub
''    End If
''  ElseIf optType.item(1) Then
''    ''''''''''''''''''''''''''''''
''    ' CBL:
''    ''''''''''''''''''''''''''''''
''    For indOgg = 1 To Dli2Rdbms.drObjList.ListItems.Count
''      If Dli2Rdbms.drObjList.ListItems(indOgg).Selected Then
''        'If Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ) = currentType Then 'SQ - Tutti i PGM (altri linguaggi...)
''        If Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ) = "CBL" Or _
''          Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ) = "PLI" Or _
''          Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ) = "EZT" Then
''          inCondition = inCondition & Dli2Rdbms.drObjList.ListItems(indOgg) & ","
''        Else
''          'MsgBox Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(NAME_OBJ) & " is not a Source Object: " & Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ), vbExclamation, "i-4.Migration"
''          AggLog Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(NAME_OBJ) & " is not a Source Object: " & Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ), RTBErr2
''        End If
''      End If
''    Next indOgg
''    If Len(inCondition) Then
''      'rimuovo l'ultima virgola:
''      inCondition = Left(inCondition, Len(inCondition) - 1)
''      Set rsIstruzioni = m_fun.Open_Recordset("SELECT z.*,Codifica,b.nomerout FROM PsDli_Istruzioni as z,PsDli_IstrCodifica as b WHERE " & _
''                                              "z.idPgm = b.idPgm AND z.idOggetto = b.idOggetto AND z.riga = b.riga AND z.idPgm in (" & inCondition & ") and " & _
''                                              "Valida and ImsDB " & instructionFilter & _
''                                              " ORDER BY b.nomerout, codifica, z.idPgm, z.idoggetto, z.riga")
''    Else
''      MsgBox "No Programs selected", vbInformation, "i-4.Migration"
''      Screen.MousePointer = vbNormal
''      Exit Sub
''    End If
''  ElseIf optType.item(2) Then
''    ''''''''''''''''''''''''
''    ' ALL:
''    ''''''''''''''''''''''''
''    Set rsIstruzioni = m_fun.Open_Recordset( _
''                      "select distinct z.idPgm,z.idOggetto,z.riga,z.istruzione,z.keyfeedback,b.nomerout,Codifica from " & _
''                      "PsDLI_Istruzioni as z, PsDLI_IstrCodifica as b where " & _
''                      "z.idPgm=b.idPgm AND z.IdOggetto=b.IdOggetto AND z.riga=b.riga and Valida and ImsDB " & instructionFilter & _
''                      " ORDER BY b.nomerout,codifica, z.idPgm, z.idoggetto, z.riga")
''  Else
''    'NO SELEZIONE
''    MsgBox "No criteria selected.", vbInformation, "i-4.Migration"
''    Exit Sub
''  End If
''
''  txtTotInstr = txtTotInstr + rsIstruzioni.RecordCount
''  ''''''''''''''''''''''''''''''''''
''  ' Per ogni singola Istruzione:
''  ''''''''''''''''''''''''''''''''''
''  k = 0
''  Dim oldOggetto As Integer
''  Dim wCics As Boolean
''  oldOggetto = 0
''  'azzera tutto
''  ReDim wIstrCod(0)
''  While Not rsIstruzioni.EOF
''    DoEvents
''
''    'estrae alcune informazioni dalla bs_oggetti, ma solo se non cambia il programma
''    'si potrebbe mettere in join con la rsistruzioni, ma la join comincerebbe a diventare troppo grande
''
''    'ordinato per istruzione, dovrebbe essere pi� veloce
''    If rsIstruzioni!Codifica <> oldIstr Then
''      'SQ CPY-ISTR
''      idPgm = rsIstruzioni!idPgm
''      oldIstr = rsIstruzioni!Codifica
''      k2 = 0
''
'''      xwIstr = rsIstruzioni!Istruzione
''      wRout = GetNomeRoutineFromCodifica(rsIstruzioni!Codifica)
''
''      Dim tbCodifica As Recordset
''      'Set TbCodifica = m_fun.Open_Recordset("select * from MgDLI_DecodificaIstr where codifica = '" & rsIstruzioni!codifica & "' and generato = false")
''      Set tbCodifica = m_fun.Open_Recordset("select * from MgDLI_DecodificaIstr where codifica = '" & rsIstruzioni!Codifica & "'")
''      If tbCodifica.EOF Then
''         'MsgBox rsIstruzioni!Codifica & " missing in MGDLI_DECODIFICAISTR", vbExclamation
''         AggLog rsIstruzioni!Codifica & " missing in MGDLI_DECODIFICAISTR", RTBErr2
''         Screen.MousePointer = 0
''         ' stefano: devo commentare per poter continuare ad elaborare
''         'Exit Sub
''      'End If
''      Else
''      '  si pu� togliere se rallenta troppo
'''        TbCodifica!generato = True
'''        TbCodifica.Update
''
''        k = k + 1
''        ReDim Preserve wIstrCod(k)
''
''        'AC 16/01/08 db direttamente dalla decodifica, nel caso di vecchie versioni
''        ' manteniamo anche questo metodo
''        wdb = GetDecodificaParam(0, "<dbd>", tbCodifica!Decodifica)
''        If wdb = "" Then ' vecchie decodifiche
''          sDbd = getDBDByIdOggettoRiga(rsIstruzioni!idPgm, rsIstruzioni!idOggetto, rsIstruzioni!Riga)
''          If UBound(sDbd) > 0 Then
''            wdb = Restituisci_NomeOgg_Da_IdOggetto(sDbd(1))
''          Else
''            wdb = ""  'che succede in questo caso?
''          End If
''        End If
''
''        'verifica GSAM
''        Dim tb As Recordset, tbXd As Recordset
''        Set tb = m_fun.Open_Recordset("select * from bs_oggetti where tipo_dbd = 'GSA' and nome = '" & wdb & "'")
''        wIstrCod(k).isGsam = Not tb.EOF
''        tb.Close
''
''        'verifica db index
''        Set tb = m_fun.Open_Recordset("select * from bs_oggetti where tipo_dbd = 'IND' and nome = '" & wdb & "'")
''        wIstrCod(k).isDbindex = Not tb.EOF
''        tb.Close
''
''        If wIstrCod(k).isDbindex Then
''          Set tb = m_fun.Open_Recordset("select s.nome as nomeseg,s.IdOggetto,x.srchFields,x.nome as nomexd from PsDli_Segmenti as s,PsDli_XDField as x where x.Lchild = '" & wdb & "' and s.IdSegmento = x.Idsegmento")
''          If Not tb.EOF Then
''            xdnomeseg = tb!nomeseg
''            xdKeyField = tb!nomexd
''            xdsrchFields = tb!srchFields
''            Set tbXd = m_fun.Open_Recordset("select nome from Bs_Oggetti where IdOggetto = " & tb!idOggetto)
''            If Not tbXd.EOF Then
''              xdDb = tbXd!nome
''            End If
''            tbXd.Close
''          End If
''          tb.Close
''        End If
''
''        wIstrCod(k).Istruzione = rsIstruzioni!Codifica
''        ' leggiamo da decodifica
''        'wIstrCod(K).istr = rsIstruzioni!Istruzione
''        'wIstrCod(K).Routine = wRout
''
''        If Not wIstrCod(k).isDbindex Then
''          wIstrCod(k).ndb = wdb
''        Else
''          wIstrCod(k).ndb = xdDb
''          wIstrCod(k).nXDDb = wdb
''        End If
''        wIstrCod(k).Decodifica = tbCodifica!Decodifica
''        'stefano: inizializza per evitare i soliti errori, non so come fare altrimenti, ci vuole un esperto
''        ReDim wIstrCod(k).Liv(0)
''        'lasciata per usi futuri, anche se improbabili
''        wIstrCod(k).SwTrattata = True
''        wIstrCod(k).NumChiamate = 1
''        wIstrCod(k).isCics = tbCodifica!Cics
''        wIstrCod(k).idPgm = rsIstruzioni!idPgm
''        wIstrCod(k).IdOgg = rsIstruzioni!idOggetto
''        wIstrCod(k).Riga = rsIstruzioni!Riga
''        'da impostare; per ora lascio cos�, tanto non serviva a niente
''        wIstrCod(k).Programma = "Generic"
''        wIstrCod(k).Routine = wRout
''
''        'AC info lette da decodifica (oltre al nome del db)
''        wIstrCod(k).istr = Replace(GetDecodificaParam(0, "<istr>", wIstrCod(k).Decodifica), ".", "")
''        'wIstrCod(K).Routine = Replace(GetDecodificaParam(0, "<pgm>", wIstrCod(K).Decodifica), ".", "")
''
''        k1 = 1
''        While InStr(wIstrCod(k).Decodifica, "<level" & k1) > 0
''          ReDim Preserve wIstrCod(k).Liv(k1)
''          ReDim wIstrCod(k).Liv(k1).Chiave(0)
''          If Not wIstrCod(k).isDbindex Then
''           wIstrCod(k).Liv(k1).Segm = GetDecodificaParam(k1, "<segm>", wIstrCod(k).Decodifica)
''          ElseIf k1 = 1 Then
''           wIstrCod(k).Liv(k1).Segm = xdnomeseg
''          End If
''          If GetDecodificaParam(k1, "<area>", wIstrCod(k).Decodifica) = "AREA" Then
''            wIstrCod(k).Liv(k1).area = True
''          End If
''          wIstrCod(k).Liv(k1).ccode = GetDecodificaParam(k1, "<ccode>", wIstrCod(k).Decodifica)
''          isbolinkeylevel = True
''          k2 = 1
''          While isbolinkeylevel
''            If Not GetDecodificaParam(k1, "<key>", wIstrCod(k).Decodifica, k2) = "" Then
''              ReDim Preserve wIstrCod(k).Liv(k1).Chiave(k2)
''              wIstrCod(k).Liv(k1).Chiave(k2).key = GetDecodificaParam(k1, "<key>", wIstrCod(k).Decodifica, k2)
''              wIstrCod(k).Liv(k1).Chiave(k2).oper = GetDecodificaParam(k1, "<oper>", wIstrCod(k).Decodifica, k2)
''              wIstrCod(k).Liv(k1).Chiave(k2).Bool = GetDecodificaParam(k1, "<bool>", wIstrCod(k).Decodifica, k2)
''              If wIstrCod(k).Liv(k1).Chiave(k2).Bool = "" Then
''                isbolinkeylevel = False
''              Else
''                k2 = k2 + 1
''              End If
''            Else
''              isbolinkeylevel = False
''            End If
''          Wend
''          k1 = k1 + 1
''        Wend
''        If InStr(1, wIstrCod(k).Decodifica, "FDBKEY") > 0 Then
''          wIstrCod(k).Fdbkey = True
''        End If
''        wIstrCod(k).procSeq = GetDecodificaParam(0, "<procseq>", wIstrCod(k).Decodifica)
''        If UCase(wIstrCod(k).procSeq) = "NONE" Then
''          wIstrCod(k).procSeq = ""
''        End If
''        wIstrCod(k).procOpt = GetDecodificaParam(0, "<procopt>", wIstrCod(k).Decodifica)
''        wIstrCod(k).PCBSeq = GetDecodificaParam(0, "<pcbseq>", wIstrCod(k).Decodifica)
''        txtProcInstr = txtProcInstr + 1
''      End If 'stefano inserita per non fermare la generazione alla prima istruzione non trovata
''    Else
''      If UBound(wIstrCod) >= k Then
''        wIstrCod(k).NumChiamate = wIstrCod(k).NumChiamate + 1
'''      ReDim Preserve wIstrCod(k).Oper(k1).Programma(k2)
'''      wIstrCod(k).Oper(k1).Programma(k2) = wProgramma
''      End If
''      txtDupInstr = txtDupInstr + 1
''    End If
''    rsIstruzioni.MoveNext
''  Wend
''  rsIstruzioni.Close
''
''  For k = 1 To UBound(wIstrCod)
''
'''stefano: per ora lo tolgo, visto che non serve pi�!; da reinserire con lettura
''' sulla mgdli_decodificaistr delle istruzioni con flag_generato <> 0
''
'''    'SQ 10-08
'''    'DISABILITATO LOG SOTTO!!!!!!!!!!!!!!!!!!
'''    'SOLO I PRIMI 50!
'''    If Not wIstrCod(k).SwTrattata Then
'''      If k < 50 Then
'''        vPos = Len(RTBErr2.Text)
'''        RTBErr2.SelStart = vPos
'''        RTBErr2.SelLength = 0
'''        RTBErr2.SelText = wIstrCod(k).Istruzione & "-" & wIstrCod(k).Decodifica & vbCrLf
'''      ElseIf k = 50 Then
'''        vPos = Len(RTBErr2.Text)
'''        RTBErr2.SelStart = vPos
'''        RTBErr2.SelLength = 0
'''        RTBErr2.SelText = "... too many errors (more than 50)..."
'''      End If
'''    End If
''    Dim xsInstr As String
''    If Len(wIstrCod(k).Routine) Then
''      If wIstrCod(k).isCics Then
''        xsInstr = "T#" & Left(wIstrCod(k).Istruzione, 4)
''      Else
''        xsInstr = "B#" & Left(wIstrCod(k).Istruzione, 4)
''      End If
''
''      'STEFANO G. Univocit�
''      existNode = False
''      Dim exKey As String
''
''      'Univocit�
''      For wEx = 1 To TreeView2.Nodes.Count
''        If TreeView2.Nodes(wEx).text = wIstrCod(k).ndb & " - " & wIstrCod(k).Routine Then
''          existNode = True
''          exKey = TreeView2.Nodes(wEx).key
''          Exit For
''        End If
''      Next wEx
''
''      If Not existNode Then
''        TreeView2.Nodes.Add , , "#" & xsInstr & "#" & wIstrCod(k).Routine, wIstrCod(k).ndb & " - " & wIstrCod(k).Routine
''        TreeView2.Nodes.Add "#" & xsInstr & "#" & wIstrCod(k).Routine, tvwChild, wIstrCod(k).Istruzione, wIstrCod(k).Istruzione
''      Else
''        TreeView2.Nodes.Add exKey, tvwChild, wIstrCod(k).Istruzione, wIstrCod(k).Istruzione
''      End If
''
'''      If InStr(1, wIstrCod(k).Routine, "UTIL") > 0 Then
'''        TreeView2.Nodes.Add wIstrCod(k).Istruzione, tvwChild, wIstrCod(k).Istruzione & "-", "<NOOP>" & "(Id:-1 Riga: -1)"
'''      Else
''         'stefano: voleva solo l'ultimo, da reimpostare meglio
'''        For k1 = 1 To UBound(wIstrCod(k).Oper)
'''          TreeView2.Nodes.Add wIstrCod(k).Istruzione, tvwChild, wIstrCod(k).Istruzione & "-" & wIstrCod(k).Oper(k1).Cod, wIstrCod(k).Oper(k1).Cod & "(Id:" & wIstrCod(k).Oper(k1).IdOgg & " Riga:" & wIstrCod(k).Oper(k1).Riga & ")"
'''        Next k1
'''      End If
''      k1 = UBound(wIstrCod(k).Liv)
''      If k1 > 0 Then
''        k2 = UBound(wIstrCod(k).Liv(k1).Chiave)
''        If k2 > 0 Then
''          'SQ CPY-ISTR
''          'TreeView2.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "-" & wIstrCod(K).Liv(k1).Chiave(k2).Oper, wIstrCod(K).Liv(k1).Chiave(k2).Oper & "(Id:" & wIstrCod(K).IdOgg & " Riga:" & wIstrCod(K).Riga & ")"
''          TreeView2.Nodes.Add wIstrCod(k).Istruzione, tvwChild, wIstrCod(k).Istruzione & "-" & wIstrCod(k).Liv(k1).Chiave(k2).oper, wIstrCod(k).Liv(k1).Chiave(k2).oper & "(IdPgm:" & wIstrCod(k).idPgm & " Id:" & wIstrCod(k).IdOgg & " Riga:" & wIstrCod(k).Riga & ")"
''        Else
''          'SQ CPY-ISTR
''          'TreeView2.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "-" & "<NOOP>", "<NOOP>" & "(Id:" & wIstrCod(K).IdOgg & " Riga:" & wIstrCod(K).Riga & ")"
''          TreeView2.Nodes.Add wIstrCod(k).Istruzione, tvwChild, wIstrCod(k).Istruzione & "-" & "<NOOP>", "<NOOP>" & "(IdPgm:" & wIstrCod(k).idPgm & " Id:" & wIstrCod(k).IdOgg & " Riga:" & wIstrCod(k).Riga & ")"
''        End If
''      Else
''        k1 = 1
''        'SQ CPY-ISTR
''        'TreeView2.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "-" & "<NOLEVEL>", "<NOLEVEL>" & "(Id:" & wIstrCod(K).IdOgg & " Riga:" & wIstrCod(K).Riga & ")"
''        TreeView2.Nodes.Add wIstrCod(k).Istruzione, tvwChild, wIstrCod(k).Istruzione & "-" & "<NOLEVEL>", "<NOLEVEL>" & "(IdPgm:" & wIstrCod(k).idPgm & " Id:" & wIstrCod(k).IdOgg & " Riga:" & wIstrCod(k).Riga & ")"
''      End If
''    End If
''
''    If wIstrCod(k).NumChiamate > 0 Then
''      TreeView1.Nodes.Add , , wIstrCod(k).Istruzione, wIstrCod(k).Routine & " (" & wIstrCod(k).Istruzione & ") <" & wIstrCod(k).NumChiamate & ">"
''      k1 = UBound(wIstrCod(k).Liv)
''      If k1 > 0 Then
''        k2 = UBound(wIstrCod(k).Liv(k1).Chiave)
''        wProgramma = wIstrCod(k).Programma
''        If k2 > 0 Then
''          TreeView1.Nodes.Add wIstrCod(k).Istruzione, tvwChild, wIstrCod(k).Istruzione & wIstrCod(k).Liv(k1).Chiave(k2).oper, wIstrCod(k).Liv(k1).Chiave(k2).oper
''          TreeView1.Nodes.Add wIstrCod(k).Istruzione & wIstrCod(k).Liv(k1).Chiave(k2).oper, tvwChild, wIstrCod(k).Istruzione & wIstrCod(k).Liv(k1).Chiave(k2).oper & wProgramma, wProgramma
''        Else
''          TreeView1.Nodes.Add wIstrCod(k).Istruzione, tvwChild, wIstrCod(k).Istruzione & "<NOOP>", "<NOOP>"
''          TreeView1.Nodes.Add wIstrCod(k).Istruzione & "<NOOP>", tvwChild, wIstrCod(k).Istruzione & "<NOOP>" & wProgramma, wProgramma
''        End If
''      Else
''        TreeView1.Nodes.Add wIstrCod(k).Istruzione, tvwChild, wIstrCod(k).Istruzione & "<NOLEVEL>", "<NOLEVEL>"
''        TreeView1.Nodes.Add wIstrCod(k).Istruzione & "<NOLEVEL>", tvwChild, wIstrCod(k).Istruzione & "<NOLEVEL>" & "Generic", "Generic"
''      End If
''    End If
''    Me.Refresh
''  Next k
''
''  Screen.MousePointer = vbDefault
''  AggLog "Instructions Analysis Ended.", RtLog
''Exit Sub
''errRout:
''  If err.Number = 35602 Then
''    Resume Next
''  Else
''    MsgBox err.Description
''  End If
''  Resume Next
''End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''
'MF 31/08/07
'SlotMachine: Carica lista di decodifiche
'''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub getDecodes(Instruction As String)
  Dim rd As Recordset, rdbd As Recordset
  Dim listd As ListItem
  Dim nomeRoutine As String
  Dim inCondition, instructionFilter As String
  Dim k As Long
  Dim indOgg As Integer
  
  txtTotInstr = 0
  txtProcInstr = 0
  txtDupInstr = 0

  If Instruction = "" Then
    instructionFilter = ""
  Else
    instructionFilter = " and codifica like '" & Instruction & "%'"
  End If
    
  If optType.item(0) Then
    ''''''''''''''''''''''''''''''
    ' DBD:
    ''''''''''''''''''''''''''''''
    For indOgg = 1 To Dli2Rdbms.drObjList.ListItems.Count
      If Dli2Rdbms.drObjList.ListItems(indOgg).Selected Then
        If Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ) = "DBD" Then
          inCondition = inCondition & Dli2Rdbms.drObjList.ListItems(indOgg) & ","
        Else
          MsgBox Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(NAME_OBJ) & " is not a DBD", vbExclamation
          Screen.MousePointer = 0
          Exit Sub
        End If
      End If
    Next indOgg
    If Len(inCondition) Then
      'rimuovo l'ultima virgola:
      inCondition = Left(inCondition, Len(inCondition) - 1)
      Set rd = m_fun.Open_Recordset( _
                  "select * from MgDLI_DecodificaIstr where " & _
                  "imsDB and codifica in (" & _
                  "select distinct b.codifica from PsDLI_IstrDBD as a,PsDLI_IstrCodifica as b WHERE " & _
                  "a.idDBD in (" & inCondition & ") AND " & _
                  "a.IdOggetto = b.IdOggetto AND a.riga = b.riga And a.Idpgm = b.Idpgm" & instructionFilter & _
                  ") order by nomerout, codifica")
    Else
      MsgBox "No DBDs selected", vbInformation, "i-4.Migration"
      Screen.MousePointer = vbNormal
      Exit Sub
    End If
  ElseIf optType.item(1) Then
    ''''''''''''''''''''''''''''''
    ' CBL:
    ''''''''''''''''''''''''''''''
    For indOgg = 1 To Dli2Rdbms.drObjList.ListItems.Count
      If Dli2Rdbms.drObjList.ListItems(indOgg).Selected Then
        If Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ) = "CBL" Or _
           Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ) = "PLI" Or _
           Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ) = "EZT" Then
          inCondition = inCondition & Dli2Rdbms.drObjList.ListItems(indOgg) & ","
        Else
          AggLog Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(NAME_OBJ) & " is not a Source Object: " & Dli2Rdbms.drObjList.ListItems(indOgg).ListSubItems(TYPE_OBJ), RTBErr2
        End If
      End If
    Next indOgg
    If Len(inCondition) Then
      Set rd = m_fun.Open_Recordset("select * from mgdli_decodificaistr where " & _
                     "ImsDB and codifica in (" & _
                     "select distinct codifica from psdli_istrcodifica where " & _
                     "idpgm in (" & inCondition & ")" & instructionFilter & _
                     ")ORDER BY nomerout, codifica")
    Else
      'SQ MsgBox "No Programs selected", vbInformation, "i-4.Migration"
      Screen.MousePointer = vbNormal
      Exit Sub
    End If
  ElseIf optType.item(2) Then
    ''''''''''''''''''''''''
    ' ALL:
    ''''''''''''''''''''''''
    Set rd = m_fun.Open_Recordset("Select * from MgDLI_DecodificaIstr where imsdb " & instructionFilter & " order by nomerout, codifica")
  Else
    'NO SELEZIONE
    MsgBox "No criteria selected.", vbInformation, "i-4.Migration"
    Exit Sub
  End If
  
  Do Until rd.EOF
    'tmp
    If Len(rd!nomeRout) Then
      If m_fun.FnNomeDB = "Prj-Perot-POC.mty" Then
        nomeRoutine = Left(rd!nomeRout & "", 2)
      ElseIf m_fun.FnNomeDB = "prj_BBV.mty" Then
        nomeRoutine = Mid(rd!nomeRout & "", 2, 5)
      ElseIf UCase(m_fun.FnNomeDB) = "ALENIA.MTY" Then
        nomeRoutine = Mid(rd!nomeRout & "", 2, 4)
      ElseIf Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
        nomeRoutine = "DI" & Mid(rd!nomeRout & "", 5, 1)
      Else
        nomeRoutine = Mid(rd!nomeRout & "", 3, 4)
      End If
      
      Set rdbd = m_fun.Open_Recordset("Select * from PsDli_DBD WHERE " & _
                                      "route_name = '" & nomeRoutine & "' and obsolete = false and to_migrate " & _
                                      "ORDER BY DBD_NAME")
      Set listd = lswSlotMachine.ListItems.Add(, , rd!Decodifica)
      listd.SubItems(1) = rd!Codifica & ""
      listd.SubItems(2) = rdbd!dbd_name & ""
      listd.SubItems(3) = rd!nomeRout & ""
      
      rdbd.Close
    End If
    rd.MoveNext
  Loop
  txtTotInstr = txtTotInstr + rd.RecordCount
  rd.Close
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''''''''
''Public Sub routinesGeneration_Photo()
''  Dim i As Integer, y As Integer
''  Dim key As String, istr As String, nomeDb As String
''  Dim idOggettoTxt As String, rigaTxt As String, operator As String
''  Dim oldNomeRout As String, ii As Integer, ij As Integer
''  Dim a As Long, rsTable As Recordset, rsDb As Recordset
''  Dim Suffix As String, NomeCpyP As String
''  Dim wEnv As Collection
''
''  Set CopyP = New Collection
''  ReDim TabOk(0)
''  ReDim IstrOk(0)
''  ReDim TabOk2(0)
''  ReDim IstrOk2(0)
''  ReDim IstrOkDet2(0)
''  idxTabOkMax = 0
''  idxTabOkMax2 = 0
''
''  'SQ RoutLang = IIf(OptPLI.Value, "PLI", "")
''
''  'passa l'informazione sul db
''  'SQ casino: c'� gi� DB: setDBName = TipoDb
''
''  totIstr = 0
''  RtLog.text = ""
''
''  Label11 = txtProcInstr
''  Label9 = 0
''
''  swCall = LstFunction.Selected(2)
''
''  AggLog " --> Routines creation has started...", RtLog
''  lastk = False
''  firstk = False
''
''  For i = 1 To TreeView2.Nodes.Count
''
''    'stefanopippo: treeview2.nodes.count non � il numero totale delle istruzioni
''    'contiene anche le righe per i livelli
''    'Label9 = k
''    'Label9.Refresh
''    key = Left(TreeView2.Nodes(i).key, 8)
''
''    If Left(key, 1) = "#" And Mid(key, 3, 1) = "#" And Mid(key, 8, 1) = "#" Then
''      firstk = False
''      lastk = False
''      nomeRoutine = Mid(TreeView2.Nodes(i).key, 9)
''      'SQ - EMBEDDED
''      EmbeddedRoutine = nomeRoutine = "<EMBEDDED>"
''      nomeDb = TreeView2.Nodes(i).text
''      y = InStr(nomeDb, " ")
''      nomeDb = Left(nomeDb, y - 1)
''      gbCurInstr = Replace(Mid(key, 4, 4), ".", "")
''      ' stefano: fa schifo, ma finch� non tiriamo via sta gestione "castrante" della treeview
''      ' basata su nomi fissi....
''      isGsam = "GS" = Right(TreeView2.Nodes(i).text, 2)
''      SwTp = Left(key, 3) = "#T#"
''
''      If nomeRoutine <> oldNomeRout Then
''        If LstFunction.Selected(1) Then
''          initRoutinesModule_DLI RTBR, nomeDb
''        Else
''          'SQ - EMBEDDED
''          If Not EmbeddedRoutine And Not RoutineOnly Then
''            ' Mauro 24/04/2008 : Nuova gestione Copy P*
''            For a = 1 To CopyP.Count
''              If RoutLang = "PLI" Then
''                changeTag RTBR, "*#COPY_PD_DB2#", "%INCLUDE " & CopyP.item(a) & "." & vbCrLf & "*#COPY_PD_DB2#"
''              Else
''                changeTag RTBR, "*#COPY_PD_DB2#", "EXEC SQL INCLUDE " & CopyP.item(a) & " END-EXEC." & vbCrLf & "*#COPY_PD_DB2#"
''              End If
''            Next a
''            ' Mauro 24/04/2008 : Nuova gestione Copy P*
''            Set CopyP = New Collection
''            RTBR.text = Replace(RTBR.text, "*#COPY_PD_DB2#", "")
''
''            initRoutinesModule_DB2 nomeDb, RTBR
''          Else
''            MadrdM_GenRout.Percorso = Dli2Rdbms.drPathDb & "\output-prj\" & TipoDB & IIf(SwTp, "\CxRout\", "\BtRout\")
''          End If
''        End If
''
''        oldNomeRout = nomeRoutine
''      End If
''    Else
''      key = TreeView2.Nodes(i).key
''      If InStr(key, "-") = 0 Then
''        AggLog "  Instruction: " & key, RtLog
''        istr = Trim(key)
''      Else
''        If firstk Then
''          firstk = False
''        Else
''          firstk = True
''        End If
''        ' controllo se ultimo della serie
''        For ii = i + 1 To TreeView2.Nodes.Count
''          key = TreeView2.Nodes(ii).key
''          If Left(key, 1) = "#" And Mid(key, 3, 1) = "#" And Mid(key, 8, 1) = "#" Then Exit For
''        Next ii
''        If ii < TreeView2.Nodes.Count Then
''        ' vediamo se l'ultimo della serie
''          For ij = ii - 1 To i + 1 Step -1
''            key = TreeView2.Nodes(ij).key
''            If Not InStr(key, "-") = 0 Then
''              Exit For
''            End If
''          Next ij
''          If ij = i Then
''            lastk = True
''          End If
''        Else
''          lastk = True
''        End If
''        key = TreeView2.Nodes(i).text
''        'SQ CPY-ISTR
''        'idPgm:
''        y = InStr(key, ":")
''        idPgm = Mid(key, y + 1, InStr(key, " ") - y)
''        'RoutLang = typeFromId(idPgm)
''        'idOggetto:
''        'idOggettoTxt = Mid(key, InStr(key, "(") + 4)
''        'y = InStr(idOggettoTxt, " ")
''        'If y > 0 Then idOggettoTxt = Left(idOggettoTxt, y - 1)
''        'mIdOggettoCorrente = Val(idOggettoTxt)
''        y = InStr(y + 1, key, ":")
''        mIdOggettoCorrente = Mid(key, y + 1, InStr(y, key, " ") - y)
''        'Riga:
''        'rigaTxt = Mid(key, InStr(key, "Riga:") + 5)
''        'y = InStr(rigaTxt, ")")
''        'If y > 0 Then rigaTxt = Left(rigaTxt, y - 1)
''        'mRiga = Val(rigaTxt)
''        y = InStr(y + 1, key, ":")
''        mRiga = Mid(key, y + 1, InStr(y, key, ")") - 1 - y)
''
''        'operatore:
''        'SQ - A cosa serve?!!!!!!
''        'y = InStr(key, "Id")
''        'y = InStr(key, "IdPgm")
''        'If y < 4 Then
''        '  operator = ""
''        'Else
''        '  If InStr(key, "<NOOP>") Then
''        '     operator = Left(key, 6)
''        '  Else
''        '     operator = Left(key, 2)
''        '  End If
''        'End If
''
''        totIstr = totIstr + 1
''        nomeRoutine = Mid(TreeView2.Nodes(i).parent.parent.key, 9)
''
''        ' Mauro 27/05/2008 : Nuova gestione CopyP per Template Routine gi� esistenti
''        For a = 1 To UBound(wIstrCod(totIstr).Liv)
''          If Len(wIstrCod(totIstr).Liv(a).Segm) Then
''            Set rsTable = m_fun.Open_Recordset( _
''                         "select a.idtable, a.idtablejoin, a.tag from " & _
''                         db & "tabelle as a, psdli_segmenti as b, bs_oggetti as c where " & _
''                         "c.nome = '" & wIstrCod(totIstr).ndb & "' and c.tipo = 'DBD' and c.idoggetto = b.idoggetto and " & _
''                         "b.nome = '" & wIstrCod(totIstr).Liv(a).Segm & "' and a.idorigine = b.idsegmento and " & _
''                         "(a.tag like '%OCCURS%' or a.tag like '%DISPATCHER%')")
''          Else
''            Set rsTable = m_fun.Open_Recordset( _
''                         "select a.idtable, a.idtablejoin, a.tag from " & _
''                         db & "tabelle as a, psdli_segmenti as b, bs_oggetti as c where " & _
''                         "c.nome = '" & wIstrCod(totIstr).ndb & "' and c.tipo = 'DBD' and c.idoggetto = b.idoggetto and " & _
''                         "a.idorigine = b.idsegmento and " & _
''                         "(a.tag like '%OCCURS%' or a.tag like '%DISPATCHER%')")
''          End If
''          Do Until rsTable.EOF
''            ' Se � OCCURS, devo prendere la tabella padre da cui � stato generato
''            If InStr(rsTable!tag, "OCCURS") Then
'''              Set rsDb = m_fun.Open_Recordset("select a.idtable as idtb, a.nome as tablename, b.nome as dbname from " & _
'''                                              DB & "tabelle as a, " & DB & "db as b where " & _
'''                                              "a.idtable = " & rsTable!idtablejoin & " and a.iddb = b.iddb")
''              Set rsDb = m_fun.Open_Recordset("select idtable, nome, iddb from " & _
''                                              db & "tabelle where " & _
''                                              "idtable = " & rsTable!idtablejoin)
''            Else
'''              Set rsDb = m_fun.Open_Recordset("select a.idtable as idtb, a.nome as tablename, b.nome as dbname from " & _
'''                                              DB & "tabelle as a, " & DB & "db as b where " & _
'''                                              "a.idtable = " & rsTable!idTable & " and a.iddb = b.iddb")
''              Set rsDb = m_fun.Open_Recordset("select idtable, nome, iddb from " & _
''                                              db & "tabelle where " & _
''                                              "idtable = " & rsTable!idtable)
''            End If
''            If rsDb.RecordCount Then
'''              Select Case wIstrCod(totIstr).istr
'''                Case "GN", "GHN", "GU", "GHU", "GNP", "GHNP"
'''                  Suffix = "PS"
'''                Case "ISRT"
'''                  Suffix = "PI"
'''                Case "DLET"
'''                  Suffix = "PD"
'''                Case "REPL"
'''                  Suffix = "PR"
'''                Case Else
'''                  Suffix = ""
'''              End Select
'''              If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
'''                NomeCpyP = Left(rsDb!dbName, 3) & Suffix & Mid(rsDb!tableName, 3, 3)
'''              Else
'''                NomeCpyP = Suffix & Right(getAlias_tabelle(rsDb!idTb), 6)
'''                If NomeCpyP = Suffix Then
'''                  NomeCpyP = Suffix & Right(getNome_tabelle(rsDb!idTb), 6)
'''                End If
'''              End If
''              Set wEnv = New Collection
''              wEnv.Add rsDb
''              wEnv.Add wIstrCod(totIstr).istr
''
''              NomeCpyP = MadrdM_GenRout.getEnvironmentParam_SQ(pCopyParam, "tabelle", wEnv)
''
''              ' Mauro
''              'If Suffix = "PD" And InStr(rsTable!tag, "OCCURS") Then
''              If wIstrCod(totIstr).istr = "DLET" And InStr(rsTable!tag, "OCCURS") Then
''              Else
''                Dim isTrovato As Boolean, b As Long
''                isTrovato = False
''                For b = 1 To CopyP.Count
''                  If NomeCpyP = CopyP.item(b) Then
''                    isTrovato = True
''                  End If
''                Next b
''                If Not isTrovato Then
''                  CopyP.Add NomeCpyP, NomeCpyP
''                End If
''              End If
''            End If
''            rsDb.Close
''            rsTable.MoveNext
''          Loop
''          rsTable.Close
''        Next a
''
''        ' Mauro 17/01/2008 : Nuova gestione Template Routine gi� esistenti
''        Dim fileRout As String
''        fileRout = Dir(Dli2Rdbms.drPathDb & "\input-prj\Template\imsdb\standard\Instructions\" & istr)
''        If Len(fileRout) And isOnce Then
''          isOnce = False
''          ' Cambiare anche la msgbox in routinesGeneration_SlotMachine
''          If MsgBox("I-O Routine Templates found. Do you want to use them?", vbInformation + vbYesNo, MadrdF_GenRout.Caption) = vbYes Then
''            isInstrTemplate = True
''          Else
''            isInstrTemplate = False
''          End If
''        End If
''
''        If LstFunction.Selected(1) Then
''          'SQ
''          createRoutine_DLI RTBR, istr
''        ElseIf TipoDB <> "VSAM" Then
''          createRoutine_DB2 RTBR, istr
''        Else
''          'createRoutine_VSAM RTBR, istr
''          createRoutine_SQ RTBR, istr
''        End If
''
''        Label9 = totIstr
''        Label9.Refresh
''      End If
''    End If
''  Next i
''  ' costruisce la select
''
''  'SQ prova
''  If Not RoutineOnly Then
''
''    ' Mauro 24/04/2008 : Nuova gestione Copy P*
''    For i = 1 To CopyP.Count
''      If RoutLang = "PLI" Then
''        changeTag RTBR, "*#COPY_PD_DB2#", "%INCLUDE " & CopyP.item(i) & ";" & vbCrLf & "*#COPY_PD_DB2#"
''      Else
''        changeTag RTBR, "*#COPY_PD_DB2#", "EXEC SQL INCLUDE " & CopyP.item(i) & " END-EXEC." & vbCrLf & "*#COPY_PD_DB2#"
''      End If
''    Next i
''
''    'Salva l'ultima routine fuori ciclo
''    If Len(RTBR.text) Then
''      ' Mauro 20/02/2008 : Se � PLI, devo eliminare il tag per l'inserimento delle istruzioni
''      If RoutLang = "PLI" Then
''        RTBR.text = Replace(RTBR.text, "#INSTR#", "")
''      End If
''      ' Mauro 24/04/2008 : Nuova gestione Copy P*
''      Set CopyP = New Collection
''      RTBR.text = Replace(RTBR.text, "*#COPY_PD_DB2#", "")
''
''      RTBR.SaveFile RTBR.FileName, 1
''      RTBR.text = ""
''    End If
''  End If
''
''  'stefano: provo con le copy!!!
''  If Len(GbTestoRedOcc) Then
''    RTBR.text = GbTestoRedOcc
''    Dim pos As Integer
''    pos = InStr(Dli2Rdbms.drNomeDB, ".")
''    'If DB = "DMDB2_" Then
''      'RTBR.SaveFile Dli2Rdbms.drPathDb & "\" & Left(Dli2Rdbms.drNomeDB, pos - 1) & "\output-prj\db2\cpy\" & GbFileRedOcc, 1
''      RTBR.SaveFile Dli2Rdbms.drPathDb & "\output-prj\" & TipoDB & "\cpy\" & GbFileRedOcc, 1
''    'Else
''      'RTBR.SaveFile Dli2Rdbms.drPathDb & "\" & Left(Dli2Rdbms.drNomeDB, pos - 1) & "\output-prj\oracle\cpy\" & GbFileRedOcc, 1
''      'RTBR.SaveFile Dli2Rdbms.drPathDb & "\output-prj\oracle\cpy\" & GbFileRedOcc, 1
''    'End If
''  End If
'''???
'''''''      GbTestoRedOcc = ""
'''''''      GbFileRedOcc = ""
'''''''      RTBR.Text = ""
''
''  AggLog " --> Routines has generated.", MadrdF_GenRout.RtLog
''End Sub

Public Sub routinesGeneration_SlotMachine()
  Dim tb As Recordset, tbXd As Recordset
  Dim i As Long
  Dim tbCodifica As Recordset
  Dim rsRout As Recordset
  Dim oldNomeRout As String, nomeDb As String
  Dim istr() As Istruzioni, Rout As String
  Dim listx As ListItem
  Dim fileRout As String
  
  RtLog.text = ""
  
  ReDim TabOk(0)
  ReDim IstrOk(0)
  ReDim TabOk2(0)
  ReDim IstrOk2(0)
  ReDim IstrOkDet2(0)
  idxTabOkMax = 0
  idxTabOkMax2 = 0
  
  totIstr = 0
  ReDim wIstrCod(0)
  lswRout.ListItems.Clear
   
  AggLog " --> Routines creation has started...", RtLog
    
  ' Mauro 31/01/2008: Giro preliminare per importare tutte le istruzioni da generare
  For i = 1 To lswSlotMachine.ListItems.Count
    If lswSlotMachine.ListItems(i).Selected Then
      Set listx = lswRout.ListItems.Add(, , lswSlotMachine.ListItems(i))
      listx.SubItems(1) = lswSlotMachine.ListItems(i).SubItems(1)
      listx.SubItems(2) = lswSlotMachine.ListItems(i).SubItems(2)
      listx.SubItems(3) = lswSlotMachine.ListItems(i).SubItems(3)
    End If
  Next i
     
  lswRout.Refresh
  ' Con questo comando le ordino per Routine
  lswRout_ColumnClick lswRout.ColumnHeaders(4)
  
  oldNomeRout = ""
  ReDim istr(0)
  Label11 = lswRout.ListItems.Count
  Label9 = 0
  
  For i = 1 To lswRout.ListItems.Count
    totIstr = totIstr + 1
    ReDim Preserve wIstrCod(totIstr)
    ReDim idTable_D(0)
          
    If Not RoutineOnly Then
      If oldNomeRout <> lswRout.ListItems(i).SubItems(3) Then
        ' Mauro 28/08/2009 : Nuova gestione Template in Input-prj per I-O Routines
        If Len(oldNomeRout) Then
          fileRout = Dir(Dli2Rdbms.drPathDb & "\input-prj\Template\imsdb\standard\Temp_Rout\" & oldNomeRout & ".cbl")
          If Len(fileRout) And isOnce Then
            isOnce = False
            ' Cambiare anche la msgbox in routinesGeneration_Photo
            If MsgBox("I-O Routine Templates found. Do you want to use them?", vbInformation + vbYesNo, MadrdF_GenRout.Caption) = vbYes Then
              isRoutTemplate = True
            Else
              isRoutTemplate = False
            End If
          End If
        End If
        
        If isRoutTemplate And Len(fileRout) Then
          Dim Fs As Object
          Set Fs = CreateObject("Scripting.FileSystemObject")
          Fs.copyfile Dli2Rdbms.drPathDb & "\input-prj\Template\imsdb\standard\Temp_Rout\" & oldNomeRout & ".cbl", Dli2Rdbms.drPathDb & "\output-prj\" & TipoDB & IIf(SwTp, "\CxRout\", "\BtRout\") & oldNomeRout & ".cbl"
        Else
          If UBound(istr) Then
            GenRoutine_SlotMachine oldNomeRout, istr
          End If
        End If
        oldNomeRout = lswRout.ListItems(i).SubItems(3)
        ReDim istr(0)
      End If
      ReDim Preserve istr(UBound(istr) + 1)
      istr(UBound(istr)).Codifica = lswRout.ListItems(i).SubItems(1)
      istr(UBound(istr)).Decodifica = lswRout.ListItems(i)
      istr(UBound(istr)).DBD = lswRout.ListItems(i).SubItems(2)
      getInfoRowByDecodifica istr(UBound(istr)).Codifica, idPgm, mIdOggettoCorrente, mRiga
    Else
      isJoin = LstFunction.Selected(TABLE_JOIN)
      isOldMethod = LstFunction.Selected(OLD_METHOD)
      wIstrCod(totIstr).Istruzione = lswRout.ListItems(i).SubItems(1)
      ' AC 10/09/07 modificata getDecoding  ,16/01/08 modificata again
      wIstrCod(totIstr) = getDecoding(lswRout.ListItems(i))
      wIstrCod(totIstr).Istruzione = lswRout.ListItems(i).SubItems(1)
      wIstrCod(totIstr).Routine = lswRout.ListItems(i).SubItems(3)
      If wIstrCod(totIstr).nDb = "" Then
        wIstrCod(totIstr).nDb = lswRout.ListItems(i).SubItems(2)
      End If
      ' Mauro 23/07/2008
      'wIstrCod(totIstr).isGsam = CheckGsam(lswRout.ListItems(i).SubItems(2))
      wIstrCod(totIstr).isGsam = CheckGsam(wIstrCod(totIstr).nDb)
      Set tbCodifica = m_fun.Open_Recordset("select Cics,nomerout from MgDLI_DecodificaIstr where " & _
                                            "codifica = '" & wIstrCod(totIstr).Istruzione & "'")
      If Not tbCodifica.EOF Then
        wIstrCod(totIstr).IsCICS = tbCodifica!Cics
        'wIstrCod(totIstr).Routine = TbCodifica!nomeRout
      End If
      tbCodifica.Close
      
      SwTp = wIstrCod(totIstr).IsCICS
      ' AC 10/09/07  isDBIndex
      'CheckDbIndex MadrdF_GenRout.lswSlotMachine.ListItems(i).ListSubItems(2), wIstrCod(totIstr)
      CheckDbIndex wIstrCod(totIstr).nDb, wIstrCod(totIstr)
      getInfoRowByDecodifica wIstrCod(totIstr).Istruzione, idPgm, mIdOggettoCorrente, mRiga
    
      AggLog "  Instruction: " & lswRout.ListItems(i).SubItems(1), RtLog
     
      'SQ amico Alpis, non l'hai provata neanche una volta?!
      'ReDim wIstrCod(totIstr).Liv(0)
    
      ' Mauro 17/01/2008 : Nuova gestione Template Routine gi� esistenti
      fileRout = Dir(Dli2Rdbms.drPathDb & "\input-prj\Template\imsdb\standard\Instructions\" & wIstrCod(totIstr).Istruzione)
      If Len(fileRout) And isOnce Then
        isOnce = False
        ' Cambiare anche la msgbox in routinesGeneration_Photo
        If MsgBox("I-O Routine Templates found. Do you want to use them?", vbInformation + vbYesNo, MadrdF_GenRout.Caption) = vbYes Then
          isInstrTemplate = True
        Else
          isInstrTemplate = False
        End If
      End If
      'SQ RoutLang = IIf(OptPLI.Value, "PLI", "")
      createRoutine_SQ RTBR, ""
      
      Label9 = totIstr
      Label9.Refresh
    End If
  Next i
      
  If Not RoutineOnly And UBound(istr) Then
    GenRoutine_SlotMachine oldNomeRout, istr
  End If
  AggLog " --> Routines has generated.", RtLog
End Sub

Private Sub lswRout_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String

  Order = lswRout.SortOrder

  key = ColumnHeader.index

  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If

  lswRout.SortOrder = Order
  lswRout.SortKey = key - 1
  lswRout.Sorted = True
End Sub

Sub Report_Instr()
  Dim risp As String
  
  risp = MsgBox("Do you want to print the selected instructions only?", vbYesNoCancel + vbQuestion, "i-4.Migration")
  OnlySelected = risp = vbYes
  
  If risp <> vbCancel Then
    StrFilename = Dli2Rdbms.drPathDb & "\Documents\Excel\" & Format(Now, "dd.mm.yyyy.hh.mm.ss") & "Istr.txt"
    ReportExceIstrDLI
    
    'SQ dava errore se non c'erano righe!
    Rows("1:1").Select
    With ActiveWindow
      .SplitColumn = 0
      .SplitRow = 1
    End With
    ActiveWindow.FreezePanes = True
    
    Screen.MousePointer = vbNormal
  End If
End Sub

'silvia 14-12-2007
Function ReportExceIstrDLI()
  Dim c As Object, r As Object, i As Long, j As Long, i_liv As Long, k As Long
  Dim mylistview As ListView
  Dim Istruzione As DefOperaz
  Dim rigaFile As String, rigaliv As String, Riga As String, rigachiave As String
  Dim numFile As Integer
  Dim Instruction As String, descdecod As String, txtdecod() As String
  Dim DBD As String, txtRoutine As String
  Dim txtliv As String
  Dim strfilexls As String, wbookname As String
  Dim rs As Recordset
  Dim SegmSrc As String, SegmTrgt As String
  
  On Error GoTo erro
  
  strfilexls = Replace(StrFilename, ".txt", ".xls")
  Screen.MousePointer = vbHourglass
  Set mylistview = lswSlotMachine
  ' Mauro 29/10/2008
  'rigaFile = "INSTRUCTION|ROUTINE|DESC.DECOD|ISTR|DBD|PROCSEQ|LIVELLO|SEGMENTO|IO-AREA|CHIAVE|OPERATORE|BOOLEANO|PCBSEQ"
  rigaFile = "INSTRUCTION|ROUTINE|DESC.DECOD|ISTR|DBD|PROCSEQ|SEGM.SOURCE|SEGM.TARGET|NUM.LIVELLI|LIVELLO|SEGMENTO|IO-AREA|CHIAVE|OPERATORE|BOOLEANO|PCBSEQ"
  numFile = FreeFile
  Open StrFilename For Output As numFile
  Print #numFile, rigaFile
  
  For i = 1 To mylistview.ListItems.Count
    'If that item should be exported
    If (OnlySelected And mylistview.ListItems(i).Selected) Or Not OnlySelected Then

      Istruzione = getDecoding(mylistview.ListItems(i))
      Instruction = mylistview.ListItems(i).ListSubItems(1).text
      descdecod = ""
      descdecod = vbCrLf & "      *  " & Left(Instruction & Space(4), 10) & " - "

      descdecod = descdecod & DecIstr(mylistview.ListItems(i)) & vbCrLf
      
      'descdecod = DecIstr(mylistview.ListItems(i))
      txtdecod() = Split(descdecod, vbCrLf)
      descdecod = ""
      For k = 2 To UBound(txtdecod)
        descdecod = descdecod & " " & Mid(txtdecod(k), 1, 72)
      Next k
      ' Secondo giro per ripulire gli spazi e gli asterischi
      txtdecod() = Split(descdecod, "*")
      descdecod = ""
      For k = 1 To UBound(txtdecod)
        descdecod = descdecod & "* " & Trim(Mid(txtdecod(k), 1, 72)) & " "
      Next k
      descdecod = Replace(descdecod, "SEQUENCE-", "")
      ' Nella nuova Decodifica, il DBD non � pi� sulla ListView
      If Len(Istruzione.nDb) Then
        DBD = Istruzione.nDb
      Else
        DBD = mylistview.ListItems(i).ListSubItems(2).text
      End If
      txtRoutine = mylistview.ListItems(i).ListSubItems(3).text
      
      ' Mauro 29/10/2008: Ricerca segmenti del PROCSEQ
      If Len(Istruzione.procSeq) Then
        Set rs = m_fun.Open_Recordset("SELECT a.segmento as SegmSrc, b.nome as SegmTrgt " & _
                                      "FROM PsDLI_XDField as a, PsDLI_Segmenti as b " & _
                                      "WHERE a.lchild ='" & Istruzione.procSeq & "' and a.idsegmento = b.idsegmento")
        If rs.RecordCount Then
          If rs!SegmSrc & "" = "#import#" Then
            SegmSrc = ""
          Else
            SegmSrc = rs!SegmSrc & ""
          End If
          SegmTrgt = rs!SegmTrgt & ""
        End If
        rs.Close
      End If
      'SILVIA 11-6-2008
      'Riga = Instruction & "|" & Istruzione.Routine & "|" & descdecod & "|" & Istruzione.istr & "|" & DBD & "|" & Istruzione.procSeq    '& istruzione.Liv(0) & "|" & istruzione.Liv.Segm & "|" & istruzione.Liv.Chiave & "|" & istruzione.Liv.Chiave.oper & "|" & istruzione.Liv.Chiave.bool & "|" & istruzione.PcbSeq
      Riga = Instruction & "|" & txtRoutine & "|" & descdecod & "|" & Istruzione.istr & "|" & DBD & "|" & Istruzione.procSeq & "|" & SegmSrc & "|" & SegmTrgt & "|" & UBound(Istruzione.Liv)    '& istruzione.Liv(0) & "|" & istruzione.Liv.Segm & "|" & istruzione.Liv.Chiave & "|" & istruzione.Liv.Chiave.oper & "|" & istruzione.Liv.Chiave.bool & "|" & istruzione.PcbSeq
      If UBound(Istruzione.Liv) > 0 Then
        For i_liv = 1 To UBound(Istruzione.Liv)
           rigaliv = i_liv & "|" & Istruzione.Liv(i_liv).Segm & "|" & IIf(Istruzione.Liv(i_liv).area, "X", "") & "|"
           If UBound(Istruzione.Liv(i_liv).Chiave) > 0 Then
             For j = 1 To UBound(Istruzione.Liv(i_liv).Chiave)
               rigachiave = rigaliv & Istruzione.Liv(i_liv).Chiave(j).key & "|" & Istruzione.Liv(i_liv).Chiave(j).oper & "|" & Istruzione.Liv(i_liv).Chiave(j).Bool
               rigaFile = Riga & "|" & rigachiave & "|" & Istruzione.PCBSeq
               Print #numFile, rigaFile
             Next j
           Else
             rigaFile = Riga & "|" & rigaliv & "|||" & Istruzione.PCBSeq
             Print #numFile, rigaFile
           End If
        Next i_liv
      Else
        rigaFile = Riga & "|||||" & Istruzione.PCBSeq
        Print #numFile, rigaFile
      End If
    End If
  Next i
  Print #numFile, ""
  Close numFile
  Workbooks.Application.Quit

  Workbooks.OpenText FileName:=StrFilename, _
    Origin:=xlMSDOS, StartRow:=1, dataType:=xlDelimited, TextQualifier:= _
    xlNone, ConsecutiveDelimiter:=False, Tab:=False, Semicolon:=False, Comma _
    :=False, Space:=False, Other:=True, OtherChar:="|", FieldInfo:=Array( _
    Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1), Array(5, 1), Array(6, 1), Array(7, 1), _
    Array(8, 1), Array(9, 1), Array(10, 1), Array(11, 1), Array(12, 1), Array(13, 1), Array(14, 1)), TrailingMinusNumbers:=True
  Columns("A:A").EntireColumn.AutoFit
  Columns("B:B").EntireColumn.AutoFit
  Columns("C:C").EntireColumn.AutoFit
  Columns("D:D").EntireColumn.AutoFit
  Columns("E:E").EntireColumn.AutoFit
  Columns("F:F").EntireColumn.AutoFit
  Columns("G:G").EntireColumn.AutoFit
  Columns("H:H").EntireColumn.AutoFit
  Columns("I:I").EntireColumn.AutoFit
  Columns("J:J").EntireColumn.AutoFit
  Columns("K:K").EntireColumn.AutoFit
  Columns("L:L").EntireColumn.AutoFit
  Columns("M:M").EntireColumn.AutoFit
  Columns("N:N").EntireColumn.AutoFit
  Columns("O:O").EntireColumn.AutoFit
  Columns("P:P").EntireColumn.AutoFit
  Range("A1:P1").Select
  Selection.Font.Bold = True
  With Selection.Interior
    .ColorIndex = 35
    .Pattern = xlSolid
  End With
  With Selection.Borders(xlEdgeLeft)
    .LineStyle = xlContinuous
    .Weight = xlThin
  End With
  With Selection.Borders(xlEdgeTop)
    .LineStyle = xlContinuous
    .Weight = xlThin
  End With
  With Selection.Borders(xlEdgeBottom)
    .LineStyle = xlContinuous
    .Weight = xlThin
  End With
  With Selection.Borders(xlEdgeRight)
    .LineStyle = xlContinuous
    .Weight = xlThin
  End With
  With Selection.Borders(xlInsideVertical)
    .LineStyle = xlContinuous
    .Weight = xlThin
  End With

  Workbooks.item(1).SaveAs FileName:=strfilexls, _
    FileFormat:=xlNormal, Password:="", WriteResPassword:="", _
    ReadOnlyRecommended:=False, CreateBackup:=False
            
  Workbooks.Application.Sheets.Select
  Workbooks.Application.Sheets(1).name = "DLI Instructions"
  Selection.AutoFilter
  Workbooks.item(1).Save
  
  Workbooks.Application.Visible = True

  Kill (StrFilename)

  Screen.MousePointer = vbNormal

  Exit Function
erro:
  MsgBox "Error " & err.Number & " while create report Excel:" & vbCrLf & _
         vbCrLf & _
         err.Description, vbCritical, "Export list"
  Screen.MousePointer = vbNormal
End Function
