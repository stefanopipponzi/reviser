VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MaimdF_GenRout 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "IMS/DC - Routine Generator"
   ClientHeight    =   7215
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   11685
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7215
   ScaleWidth      =   11685
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox OnlySelected 
      Caption         =   "Only Programs selected"
      ForeColor       =   &H00C00000&
      Height          =   195
      Left            =   7680
      TabIndex        =   13
      Top             =   1560
      Value           =   1  'Checked
      Width           =   2475
   End
   Begin MSComctlLib.ListView lswCampi 
      Height          =   2715
      Left            =   3840
      TabIndex        =   21
      Top             =   870
      Width           =   3795
      _ExtentX        =   6694
      _ExtentY        =   4789
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nome"
         Object.Width           =   3528
      EndProperty
   End
   Begin VB.OptionButton optRDBMS 
      Caption         =   "CICS - Routines"
      ForeColor       =   &H00C00000&
      Height          =   195
      Left            =   7710
      TabIndex        =   19
      Top             =   1890
      Value           =   -1  'True
      Visible         =   0   'False
      Width           =   1875
   End
   Begin RichTextLib.RichTextBox RtbDel 
      Height          =   975
      Left            =   3780
      TabIndex        =   18
      Top             =   2160
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   1720
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MaimdF_GenRout.frx":0000
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Camera"
            Object.ToolTipText     =   "Start SQL Dialects Finder"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Forward"
            Object.ToolTipText     =   "Start Routines migration"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Close"
            Object.ToolTipText     =   "Exit"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Combined"
            Object.ToolTipText     =   "Create Send and Receive combined"
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTBErr2 
      Height          =   1695
      Left            =   0
      TabIndex        =   15
      Top             =   5490
      Width           =   7605
      _ExtentX        =   13414
      _ExtentY        =   2990
      _Version        =   393217
      BackColor       =   16777152
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   10000
      TextRTF         =   $"MaimdF_GenRout.frx":0086
   End
   Begin RichTextLib.RichTextBox RTBErr 
      Height          =   1875
      Left            =   0
      TabIndex        =   14
      Top             =   3600
      Width           =   7605
      _ExtentX        =   13414
      _ExtentY        =   3307
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   10000
      TextRTF         =   $"MaimdF_GenRout.frx":0108
   End
   Begin RichTextLib.RichTextBox RtLog 
      Height          =   4305
      Left            =   7620
      TabIndex        =   8
      Top             =   2880
      Width           =   3705
      _ExtentX        =   6535
      _ExtentY        =   7594
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   3
      TextRTF         =   $"MaimdF_GenRout.frx":018A
   End
   Begin RichTextLib.RichTextBox RTBR 
      Height          =   975
      Left            =   5070
      TabIndex        =   7
      Top             =   2460
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   1720
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MaimdF_GenRout.frx":020C
   End
   Begin MSComctlLib.TreeView TreeView2 
      Height          =   2745
      Left            =   30
      TabIndex        =   5
      Top             =   840
      Width           =   3795
      _ExtentX        =   6694
      _ExtentY        =   4842
      _Version        =   393217
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   5055
      Top             =   3870
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaimdF_GenRout.frx":0292
            Key             =   "Camera"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaimdF_GenRout.frx":03A4
            Key             =   "Forward"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaimdF_GenRout.frx":04B6
            Key             =   "Close"
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTBOutMap 
      Height          =   972
      Left            =   360
      TabIndex        =   22
      Top             =   2400
      Visible         =   0   'False
      Width           =   2268
      _ExtentX        =   3995
      _ExtentY        =   1720
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MaimdF_GenRout.frx":0690
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      BackColor       =   &H8000000D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Routines - Instructions"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   405
      Left            =   3840
      TabIndex        =   20
      Top             =   450
      Width           =   3795
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Total Instructions"
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   7650
      TabIndex        =   17
      Top             =   450
      Width           =   2475
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Total Unique Instructions"
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   7650
      TabIndex        =   12
      Top             =   2190
      Width           =   2505
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   10170
      TabIndex        =   11
      Top             =   2190
      Width           =   1155
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Processed Instructions"
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   7650
      TabIndex        =   10
      Top             =   2535
      Width           =   2505
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   10170
      TabIndex        =   9
      Top             =   2520
      Width           =   1155
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      BackColor       =   &H8000000D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Routines - Instructions"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   405
      Left            =   30
      TabIndex        =   6
      Top             =   450
      Width           =   3795
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   345
      Left            =   10140
      TabIndex        =   4
      Top             =   1110
      Width           =   1185
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Duplicate Instructions"
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   7650
      TabIndex        =   3
      Top             =   1140
      Width           =   2475
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   10140
      TabIndex        =   2
      Top             =   780
      Width           =   1185
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Processed Instructions"
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   7650
      TabIndex        =   1
      Top             =   795
      Width           =   2475
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   10140
      TabIndex        =   0
      Top             =   450
      Width           =   1185
   End
End
Attribute VB_Name = "MaimdF_GenRout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  'Variabili per l'analisi
  Dim Wkey As String
  Dim indOgg As Long
  Dim rsObj As Recordset, rs As Recordset
  Dim TbOggElab As Recordset
  Dim OldNomeRoutine As String
  Dim Percorso As String
  Dim k As Long
  Dim i As Long
  Dim wIdOggetto As Long, vPos As Long
  Dim IdObjElab() As Long
  Dim criterio As String
  
  On Error Resume Next
  
  Select Case Button.key
    Case "Camera" 'Parte l'analisi
      RtLog.text = ""
      RTBErr.text = ""
      RTBErr2.text = ""
      TreeView2.Nodes.Clear
      
      AggLog "Starting Istructions' Analysis...", RtLog
  
      Label2 = 0
      Label4 = 0
      Label6 = 0
      ReDim IdObjElab(0)
      Set rsObj = m_fun.Open_Recordset("Select IdOggetto From Bs_Oggetti Where Tipo = 'MFS' Order By IdOggetto")
      Do Until rsObj.EOF
        ReDim Preserve IdObjElab(UBound(IdObjElab) + 1)
        IdObjElab(UBound(IdObjElab)) = rsObj!IdOggetto
        
        rsObj.MoveNext
      Loop
      rsObj.Close
      'Scrive la Treeviewf
      TreeView2.Nodes.Clear
      Wkey = "SEND"
      TreeView2.Nodes.Add , , Wkey, Create_NomeRoutine_IMSDC("ISRT")
           
      For indOgg = 1 To UBound(IdObjElab)
        'TMP � ritornato l'originale
        Set TbOggElab = m_fun.Open_Recordset("select * from bs_oggetti where tipo = 'MFS' And IdOggetto = " & IdObjElab(indOgg))
        Label2 = Label2 + TbOggElab.RecordCount
       
        Wkey = TbOggElab!IdOggetto & "$" & TbOggElab!nome & "#" & Format(indOgg, "00") & "?SEND"
        TreeView2.Nodes.Add "SEND", tvwChild, Wkey, TbOggElab!nome
        TbOggElab.Close
      Next indOgg
      
      Wkey = "RECEIVE"
      TreeView2.Nodes.Add , , Wkey, Create_NomeRoutine_IMSDC("GU")
           
      For indOgg = 1 To UBound(IdObjElab)
        Set TbOggElab = m_fun.Open_Recordset("select * from bs_oggetti where tipo = 'MFS' And IdOggetto = " & IdObjElab(indOgg))
        Label2 = Label2 + TbOggElab.RecordCount
       
        Wkey = TbOggElab!IdOggetto & "$" & TbOggElab!nome & "#" & Format(indOgg, "00") & "?RECEIVE"
        TreeView2.Nodes.Add "RECEIVE", tvwChild, Wkey, TbOggElab!nome
        TbOggElab.Close
      Next indOgg
      AggLog "Instructions Analysis Ended...", RtLog

    Case "Forward" 'Genera le routine
      If UCase(Left(m_fun.FnNomeDB, 7)) = "SUNGARD" Then
        bolsungard = True
      End If
      evaluate = False
      ultimappar = False
      Percorso = m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\input-prj\Template\imsdc\standard\routines"
      RtLog.text = ""
      Label11 = TreeView2.Nodes.Count
      Label9 = 0

      AggLog " --> Routines creation has started...", RtLog
      For k = 1 To TreeView2.Nodes.Count
        Label9 = k
        Label9.Refresh
        Wkey = TreeView2.Nodes(k).key
        
        If k = TreeView2.Nodes.Count Then
          ultimappar = True
        End If
        
        If InStr(1, Wkey, "$") Then wIdOggetto = Left(Wkey, InStr(1, Wkey, "$") - 1)
        Set rs = m_fun.Open_Recordset("select Mapping from BS_Oggetti where IdOggetto =" & wIdOggetto)
        If Not rs.EOF Then
          mapping = rs!mapping
        End If
        rs.Close
        
        If InStr(1, Wkey, "?") = 0 Then
          nomeRoutine = TreeView2.Nodes(k).text
          
          If TreeView2.Nodes(k).key = "SEND" Then SwSend = True
          If TreeView2.Nodes(k).key = "RECEIVE" Then
            evaluate = False
            SwSend = False
          End If
  
          If nomeRoutine <> OldNomeRoutine Then
            'cancellazioni inutili
            If optRDBMS.Value Then
              vPos = RTBR.find("COPY #CICS-MAP#.", 1)
              RTBR.SelText = ""
              vPos = RTBR.find("#FORMAT-AREAS#.", 1)
              RTBR.SelText = ""
              vPos = RTBR.find("#FIELD#.", 1)
              RTBR.SelText = ""
              vPos = RTBR.find("*#BEGINFIELD#", 1)
              RTBR.SelText = ""
              vPos = RTBOutMap.find("*#BEGINFIELD#", 1)
              RTBOutMap.SelText = ""
              vPos = RTBR.find("#COPYMOVE#.", 1)
              RTBR.SelText = ""
              vPos = RTBR.find("#OPEREC#", 1)
              RTBR.SelText = ""
               
              'Genera routine RDBMS
              Crea_Routine_DLI_CICS RTBR, nomeRoutine, ""
            End If
            OldNomeRoutine = nomeRoutine
          End If
        Else
          gbCurInstr = Trim(UCase(Mid(Wkey, InStr(1, Wkey, "?") + 1)))
  
          If gbCurInstr = "SEND" Then
            SwSend = True
          ElseIf gbCurInstr = "RECEIVE" Then
            SwSend = False
          End If
          '*******
          If SwSend Then
            If TreeView2.Nodes(k + 1).key = "RECEIVE" Then
              ultimappar = True
            End If
          End If
          '***********
          CaricaIstruzioneMappa_DLI_CICS wIdOggetto, RTBR, RTBOutMap, gbCurInstr, TreeView2.Nodes(k).text
  
          vPos = RTBOutMap.find("#COPYMOVE#.", 1)
          RTBOutMap.SelText = ""
          vPos = RTBOutMap.find("#OPEREC#", 1)
          RTBOutMap.SelText = ""
          vPos = RTBOutMap.find("*#BEGINFIELD#", 1)
          RTBOutMap.SelText = ""
          RTBOutMap.SaveFile RTBOutMap.FileName, 1
          '***************
        End If
      Next k
       
      'salvataggio ultima routine mappa
      RTBOutMap.SaveFile RTBOutMap.FileName, 1

      'Salva l'ultima routine fuori ciclo (altrimenti non la salverebbe)
      If RTBR.text <> "" Then
        RTBR.SaveFile RTBR.FileName, 1
        
        vPos = RTBR.find("COPY #CICS-MAP#.", 1)
        RTBR.SelText = ""
        vPos = RTBR.find("#FORMAT-AREAS#.", 1)
        RTBR.SelText = ""
        
        If SwSend Then
          vPos = RTBR.find("#FIELD#.", 1)
          RTBR.SelText = ""
          vPos = RTBR.find("*#BEGINFIELD#", 1)
          RTBR.SelText = ""
          vPos = RTBR.find("#COPYMOVE#.", 1)
          RTBR.SelText = ""
        Else
          vPos = RTBR.find("#OPEREC#", 1)
          RTBR.SelText = ""
        End If
        RTBR.SaveFile RTBR.FileName, 1
      End If
      RTBR.text = ""
      AggLog " --> Routines has generated...", MaimdF_GenRout.RtLog
    
    Case "Close" 'Esce
      Unload Me
    Case "Combined" 'Send e Receive unite
      Percorso = m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\input-prj\Template\imsdc\standard\routines"
      RtLog.text = ""
      AggLog " --> Routines creation has started...", RtLog
      'IRIS-DB conversione solo oggetti selezionati
      criterio = ""
      If OnlySelected Then
        For i = 1 To Dli2Rdbms.drObjList.ListItems.Count
          If Dli2Rdbms.drObjList.ListItems(i).Selected Then
            If Trim(criterio) <> "" Then
              criterio = criterio & ", " & Val(Dli2Rdbms.drObjList.ListItems(i).text)
            Else
              criterio = Val(Dli2Rdbms.drObjList.ListItems(i).text)
            End If
          End If
        Next i
        If Len(Trim(criterio)) = 0 Then
          AggLog "Nessun oggetto selezionato!", RtLog
          Exit Sub
        End If
        criterio = " and IdOggetto in (" & criterio & ") "
      End If
      Set rsObj = m_fun.Open_Recordset("Select IdOggetto, Nome, Mapping From Bs_Oggetti Where Tipo = 'MFS' " & criterio & " Order By IdOggetto")
      Do Until rsObj.EOF
        mapping = rsObj!mapping
        wIdOggetto = rsObj!IdOggetto
  'IRIS-DB print maps not supported for now
        Set rs = Open_Recordset("select * from PsDli_MFS where idOggetto = " & wIdOggetto & " and instr(DevType,'3270P') > 1")
        If Not rs.EOF Then
          AggLog "MFS id " & wIdOggetto & " containing printer maps, not supported yet", MaimdF_GenRout.RtLog
        Else
          CreaRoutineUnified_DLI_CICS wIdOggetto, RTBR, RTBOutMap, rsObj!nome
          RTBOutMap.SaveFile RTBOutMap.FileName, 1
        End If
        rs.Close
        rsObj.MoveNext
      Loop
      rsObj.Close
      RTBR.text = ""
      AggLog " --> Routines has generated...", MaimdF_GenRout.RtLog
  End Select
End Sub

Private Sub Form_Load()
  Dim k As Double
 
  Dim PFreq As String
  
  Set TbOperaz = m_fun.Open_Recordset("select * from MGDli_DecodificaIstr where " & _
                                               "ImsDC = true order by IdIstruzione")
  If TbOperaz.RecordCount = 0 Then
    'MF 01/08/07 Se TbOperaz.RecordCount = 0 non aggiungeva la AddActiveWindows
    m_fun.AddActiveWindows Me
    m_fun.FnActiveWindowsBool = True
    Exit Sub
  End If
  k = 0
  ReDim wIstrCod_DC(k)
  TbOperaz.MoveFirst

  While Not TbOperaz.EOF
    k = k + 1
    ReDim Preserve wIstrCod_DC(k)
    
    wIstrCod_DC(k).Istruzione = TbOperaz!Codifica
    wIstrCod_DC(k).Decodifica = TbOperaz!Decodifica

    TbOperaz.MoveNext
  Wend
  TbOperaz.Close
  
  lswCampi.ColumnHeaders(1).Width = lswCampi.Width - 270
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Private Sub TreeView2_NodeClick(ByVal Node As MSComctlLib.Node)
  Dim wMapId As Long, wType As String
  
  If Trim(UCase(Node.key)) = "RECEIVE" Then
    lswCampi.ListItems.Clear
    Exit Sub
  End If
  
  If Trim(UCase(Node.key)) = "SEND" Then
    lswCampi.ListItems.Clear
    Exit Sub
  End If
  
  wMapId = Mid(Node.key, 1, InStr(1, Node.key, "$") - 1)
  wType = Mid(Node.key, InStr(1, Node.key, "?") + 1)
  
  load_Campi_Mappa wMapId, wType
End Sub

Public Sub load_Campi_Mappa(wMapId As Long, wType As String)
  Dim rs As Recordset, wRec As String
  
  lswCampi.ListItems.Clear
  Set rs = m_fun.Open_Recordset("Select * From PsDLI_MFSCmp Where IdOggetto = " & wMapId)
  Do Until rs.EOF
    If Trim(UCase(wType)) = "SEND" Then wRec = "O-" & rs!nome & " --> " & rs!nome & "O"
    
    If Trim(UCase(wType)) = "RECEIVE" Then wRec = "I-" & rs!nome & " <-- " & rs!nome & "I"
    
    lswCampi.ListItems.Add , , wRec
    
    rs.MoveNext
  Loop
  rs.Close
End Sub
