VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MadrdF_Inc 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Opzioni Per Generazione e Incapsulamento Routine I-O"
   ClientHeight    =   8340
   ClientLeft      =   1365
   ClientTop       =   1425
   ClientWidth     =   13065
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   8340
   ScaleWidth      =   13065
   ShowInTaskbar   =   0   'False
   Begin RichTextLib.RichTextBox RTBDivISTR 
      Height          =   675
      Left            =   6450
      TabIndex        =   26
      Top             =   7350
      Visible         =   0   'False
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   1191
      _Version        =   393217
      ReadOnly        =   -1  'True
      ScrollBars      =   3
      RightMargin     =   20000
      TextRTF         =   $"MadrdF_Inc.frx":0000
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTBModifica 
      Height          =   675
      Left            =   5400
      TabIndex        =   19
      Top             =   7380
      Visible         =   0   'False
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   1191
      _Version        =   393217
      ReadOnly        =   -1  'True
      ScrollBars      =   3
      RightMargin     =   60000
      TextRTF         =   $"MadrdF_Inc.frx":00EA
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.FileListBox File1 
      Height          =   1065
      Left            =   3540
      TabIndex        =   18
      Top             =   6930
      Visible         =   0   'False
      Width           =   1665
   End
   Begin VB.Frame Frame5 
      Caption         =   "Oggetti Selezionati"
      Enabled         =   0   'False
      ForeColor       =   &H8000000D&
      Height          =   6315
      Left            =   8400
      TabIndex        =   15
      Top             =   390
      Width           =   3135
      Begin MSComctlLib.ListView LstVOggSel 
         Height          =   5925
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   2865
         _ExtentX        =   5054
         _ExtentY        =   10451
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Id"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nome"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame FraAggiorna 
      Caption         =   "Opzioni Di Aggiornamento : Oggetti Da Trattare"
      ForeColor       =   &H8000000D&
      Height          =   4485
      Left            =   4140
      TabIndex        =   8
      Top             =   390
      Width           =   4215
      Begin VB.Frame Frame1 
         ForeColor       =   &H8000000D&
         Height          =   4065
         Left            =   210
         TabIndex        =   9
         Top             =   210
         Width           =   3795
         Begin VB.Frame Frame 
            Caption         =   "Analisi Istruzioni File Da Sorgenti"
            ForeColor       =   &H8000000D&
            Height          =   525
            Left            =   120
            TabIndex        =   23
            Top             =   3420
            Width           =   3555
            Begin MSComctlLib.ProgressBar ProgT 
               Height          =   225
               Left            =   150
               TabIndex        =   24
               Top             =   210
               Width           =   2595
               _ExtentX        =   4577
               _ExtentY        =   397
               _Version        =   393216
               Appearance      =   1
               Min             =   1
               Max             =   5000
               Scrolling       =   1
            End
            Begin VB.Label lblT 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0 %"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   2820
               TabIndex        =   25
               Top             =   210
               Width           =   615
            End
         End
         Begin VB.Frame FrameP 
            Caption         =   "Incapsulamento File Da Sorgenti"
            ForeColor       =   &H8000000D&
            Height          =   525
            Left            =   120
            TabIndex        =   20
            Top             =   2850
            Width           =   3555
            Begin MSComctlLib.ProgressBar ProgGen 
               Height          =   225
               Left            =   150
               TabIndex        =   21
               Top             =   210
               Width           =   2595
               _ExtentX        =   4577
               _ExtentY        =   397
               _Version        =   393216
               Appearance      =   1
               Min             =   1
               Max             =   5000
               Scrolling       =   1
            End
            Begin VB.Label lblP 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0 %"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   2820
               TabIndex        =   22
               Top             =   210
               Width           =   615
            End
         End
         Begin VB.Frame Frame3 
            Height          =   1875
            Left            =   120
            TabIndex        =   12
            Top             =   930
            Width           =   3555
            Begin VB.OptionButton OptScelti 
               Caption         =   "Tutti gli oggetti (per categoria)"
               Enabled         =   0   'False
               ForeColor       =   &H8000000D&
               Height          =   195
               Left            =   210
               TabIndex        =   14
               Top             =   0
               Width           =   2415
            End
            Begin MSComctlLib.TreeView TROggetti 
               Height          =   1455
               Left            =   120
               TabIndex        =   13
               Top             =   300
               Width           =   3285
               _ExtentX        =   5794
               _ExtentY        =   2566
               _Version        =   393217
               Indentation     =   529
               LineStyle       =   1
               Style           =   7
               Checkboxes      =   -1  'True
               Appearance      =   1
               Enabled         =   0   'False
            End
         End
         Begin VB.OptionButton OptTUTTI 
            Caption         =   "Tutti gli oggetti"
            ForeColor       =   &H8000000D&
            Height          =   195
            Left            =   330
            TabIndex        =   11
            Top             =   270
            Value           =   -1  'True
            Width           =   1755
         End
         Begin VB.OptionButton OptSel 
            Caption         =   "Solo oggetti selezionati"
            Enabled         =   0   'False
            ForeColor       =   &H8000000D&
            Height          =   345
            Left            =   330
            TabIndex        =   10
            Top             =   540
            Width           =   2025
         End
      End
   End
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   240
      TabIndex        =   7
      Top             =   7710
      Visible         =   0   'False
      Width           =   1965
   End
   Begin VB.DirListBox Dir1 
      Height          =   765
      Left            =   240
      TabIndex        =   6
      Top             =   6930
      Visible         =   0   'False
      Width           =   1935
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   13065
      _ExtentX        =   23045
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons(1)"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Start"
            Object.ToolTipText     =   "Avvia Incapsulamento Routine I/O"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Help"
            Object.ToolTipText     =   "Help"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Close"
            Object.ToolTipText     =   "Esci"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   2940
      Top             =   6930
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin VB.FileListBox FileLog 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2220
      Pattern         =   "*.log"
      TabIndex        =   3
      Top             =   7710
      Visible         =   0   'False
      Width           =   705
   End
   Begin VB.Frame Frame6 
      Caption         =   "Routine I/O Directory"
      ForeColor       =   &H8000000D&
      Height          =   2835
      Left            =   30
      TabIndex        =   2
      Top             =   2040
      Width           =   4065
      Begin VB.ListBox ListaRoutine 
         Height          =   2400
         Left            =   120
         TabIndex        =   17
         Top             =   300
         Width           =   3825
      End
   End
   Begin VB.Frame FrameLog 
      Caption         =   "Log Routine I-O"
      ForeColor       =   &H8000000D&
      Height          =   1815
      Left            =   30
      TabIndex        =   0
      Top             =   4890
      Width           =   8325
      Begin RichTextLib.RichTextBox RTLog 
         Height          =   1425
         Left            =   150
         TabIndex        =   1
         Top             =   240
         Width           =   8025
         _ExtentX        =   14155
         _ExtentY        =   2514
         _Version        =   393217
         Enabled         =   -1  'True
         ScrollBars      =   2
         TextRTF         =   $"MadrdF_Inc.frx":01D4
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Index           =   1
      Left            =   2970
      Top             =   7470
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Inc.frx":02BE
            Key             =   "Index"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Inc.frx":0498
            Key             =   "Erase"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Inc.frx":08EC
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Inc.frx":09FE
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Inc.frx":0B10
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Inc.frx":0CEA
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Inc.frx":1F6E
            Key             =   "Help"
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTRoutine 
      Height          =   765
      Left            =   2220
      TabIndex        =   5
      Top             =   6930
      Visible         =   0   'False
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1349
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   20000
      TextRTF         =   $"MadrdF_Inc.frx":2080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "MadrdF_Inc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'variabili public per memorizzare le opzioni di incapsulamento
Public TuttiOgg As Boolean
Public OggSelez As Boolean
Public SelGuidata As Boolean
Public SelCBL As Boolean
Public SelCPY As Boolean
Public SelRout As Boolean

Dim Record As Recordset
Dim Rst As Recordset
Dim AddRighe As Long
Dim wIdent As Integer
Dim wPunto As Boolean
Dim j As Long

'queste variabili servono a definire il metodo di incapsulamento delle istruzioni : EXEC o CALL.
Dim Exec As Boolean
Dim Cal As Boolean
Dim OldName As String
Dim OldName1 As String
Dim MaxSeg As Integer
Dim NomeProg As String
Dim gIstruzione As String

Private Sub ChkDbCbl_Click()
  If ChkDbCbl = vbChecked Then
     SEGN_X = "ITER-X*"
     Segn_T = "ITER-T*"
     Segn_P = "ITER-P*"
     SEGN_L = "ITER-L*"
  Else
     SEGN_X = "ITER-X "
     Segn_T = "ITER-T "
     Segn_P = "ITER-P "
     SEGN_L = "ITER-L "
  End If
End Sub

Private Sub ListaRoutine_dblClick()
    Dim File As String
    Dim i As Integer
    
    frmRoutineIO.Show
    
    frmRoutineIO.FraRoutIO.Visible = True
    
    File1.Path = PathPrj & "\Output-Prj\RoutIMS"
    
    File = ListaRoutine.List(ListaRoutine.ListIndex)
    
    For i = 0 To File1.ListCount
        If File1.List(i) = File Then
            File1.Selected(i) = True
            File1_DblClick
        End If
    Next
End Sub

Private Sub OptScelti_Click()
    TROggetti.Enabled = True
    OptSel.Value = False
    OptTUTTI.Value = False
End Sub

Private Sub OptSel_Click()
    TROggetti.Enabled = False
    OptScelti.Value = False
    
    Dim List As Integer
    Dim i As Integer
    Dim cont As Integer
    
    For i = 1 To FrmMigrList.PrjList.ListItems.Count
        If FrmMigrList.PrjList.ListItems(i).Selected = True Then
            With LstVOggSel.ListItems.Add(, , FrmMigrList.PrjList.ListItems(i).Text)
               .SubItems(1) = FrmMigrList.PrjList.ListItems(i).SubItems(1)
            End With
        End If
    
    Next
    
    If LstVOggSel.ListItems.Count = 0 Then
        Call MsgBox("Nessun oggetto selezionato...", vbInformation, "DLI2RDBMS Client")
        OptSel.Value = False
    End If
   
    
End Sub

Private Sub OptTutti_Click()
    TROggetti.Enabled = False
    OptScelti.Value = False
End Sub

Function ControllaUIB() As Boolean
Dim pPos As Variant
Dim pStart As Variant
Dim pEnd As Variant
Dim pInit As Variant
Dim PFine As Variant

ControllaUIB = True

pEnd = Len(RTBModifica.Text)
pPos = 1
While pPos > 0
  pPos = RTBModifica.Find(" ADDRESS ", pPos + 1, pEnd)
  If pPos > 0 Then
    PFine = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
    For pInit = pPos To pPos - 80 Step -1
      If Mid$(RTBModifica.Text, pInit, 2) = vbCrLf Then
         Exit For
      End If
    Next pInit
    pPos = RTBModifica.Find(" SET ", pInit, PFine)
    If pPos > 0 Then
       pPos = RTBModifica.Find(" OF ", pInit, PFine)
       If pPos > 0 Then
         pPos = RTBModifica.Find(" PCB", pInit, PFine)
         If pPos > 0 Then
            RTBModifica.SelStart = pInit + 7
            RTBModifica.SelLength = 1
            RTBModifica.SelText = "*"
         Else
           pPos = RTBModifica.Find(" BPCB", pInit, PFine)
           If pPos > 0 Then
              RTBModifica.SelStart = pInit + 7
              RTBModifica.SelLength = 1
              RTBModifica.SelText = "*"
           End If
         End If
       End If
    End If
    pPos = PFine + 1
  End If
Wend

pPos = 1
While pPos > 0
  pPos = RTBModifica.Find(" UIBPCBAL ", pPos + 1, pEnd)
  If pPos > 0 Then
    PFine = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
    For pInit = pPos To pPos - 80 Step -1
      If Mid$(RTBModifica.Text, pInit, 2) = vbCrLf Then
         Exit For
      End If
    Next pInit
    pPos = RTBModifica.Find(" MOVE ", pInit, PFine)
    If pPos > 0 Then
       RTBModifica.SelStart = pInit + 7
       RTBModifica.SelLength = 1
       RTBModifica.SelText = "*"
    End If
    pPos = PFine + 1
  End If
Wend

pPos = 1
While pPos > 0
  pPos = RTBModifica.Find(" BPCBPTR", pPos + 1, pEnd)
  If pPos > 0 Then
    PFine = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
    For pInit = pPos To pPos - 80 Step -1
      If Mid$(RTBModifica.Text, pInit, 2) = vbCrLf Then
         Exit For
      End If
    Next pInit
    pPos = RTBModifica.Find(" MOVE ", pInit, PFine)
    If pPos > 0 Then
       RTBModifica.SelStart = pInit + 7
       RTBModifica.SelLength = 1
       RTBModifica.SelText = "*"
    End If
    pPos = PFine + 1
  End If
Wend

pPos = 1
While pPos > 0
  pPos = RTBModifica.Find(" BPCB1PTR", pPos + 1, pEnd)
  If pPos > 0 Then
    PFine = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
    For pInit = pPos To pPos - 80 Step -1
      If Mid$(RTBModifica.Text, pInit, 2) = vbCrLf Then
         Exit For
      End If
    Next pInit
    pPos = RTBModifica.Find(" MOVE ", pInit, PFine)
    If pPos > 0 Then
       RTBModifica.SelStart = pInit + 7
       RTBModifica.SelLength = 1
       RTBModifica.SelText = "*"
    End If
    pPos = PFine + 1
  End If
Wend

pPos = 1
While pPos > 0
  pPos = RTBModifica.Find(" BPCB2PTR", pPos + 1, pEnd)
  If pPos > 0 Then
    PFine = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
    For pInit = pPos To pPos - 80 Step -1
      If Mid$(RTBModifica.Text, pInit, 2) = vbCrLf Then
         Exit For
      End If
    Next pInit
    pPos = RTBModifica.Find(" MOVE ", pInit, PFine)
    If pPos > 0 Then
       RTBModifica.SelStart = pInit + 7
       RTBModifica.SelLength = 1
       RTBModifica.SelText = "*"
    End If
    pPos = PFine + 1
  End If
Wend

pPos = 1
While pPos > 0
  pPos = RTBModifica.Find(" FCNOTOPEN", pPos + 1, pEnd)
  If pPos > 0 Then
    PFine = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
    For pInit = pPos To pPos - 80 Step -1
      If Mid$(RTBModifica.Text, pInit, 2) = vbCrLf Then
         Exit For
      End If
    Next pInit
    pPos = RTBModifica.Find(".", pInit, PFine)
    While pPos < 1
       PFine = RTBModifica.Find(vbCrLf, PFine + 2, pEnd)
       pPos = RTBModifica.Find(".", pInit, PFine)
    Wend
    pPos = RTBModifica.Find(" IF ", pInit, PFine)
    If pPos > 0 Then
       PFine = PFine - 2
       pPos = pInit - 4
       pPos = RTBModifica.Find(vbCrLf, pPos + 1, PFine)
       While pPos > 0
         RTBModifica.SelStart = pPos + 8
         RTBModifica.SelLength = 1
         RTBModifica.SelText = "*"
         pPos = RTBModifica.Find(vbCrLf, pPos + 1, PFine)
       Wend
    End If
    pPos = PFine + 1
  End If
Wend

pPos = 1
While pPos > 0
  pPos = RTBModifica.Find(" FCINVREQ", pPos + 1, pEnd)
  If pPos > 0 Then
    PFine = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
    For pInit = pPos To pPos - 80 Step -1
      If Mid$(RTBModifica.Text, pInit, 2) = vbCrLf Then
         Exit For
      End If
    Next pInit
    pPos = RTBModifica.Find(".", pInit, PFine)
    While pPos < 1
       PFine = RTBModifica.Find(vbCrLf, PFine + 2, pEnd)
       pPos = RTBModifica.Find(".", pInit, PFine)
    Wend
    pPos = RTBModifica.Find(" IF ", pInit, PFine)
    If pPos > 0 Then
       PFine = PFine - 2
       pPos = pInit - 4
       pPos = RTBModifica.Find(vbCrLf, pPos + 1, PFine)
       While pPos > 0
         RTBModifica.SelStart = pPos + 8
         RTBModifica.SelLength = 1
         RTBModifica.SelText = "*"
         pPos = RTBModifica.Find(vbCrLf, pPos + 1, PFine)
       Wend
    End If
    pPos = PFine + 1
  End If
Wend

pPos = 1
While pPos > 0
  pPos = RTBModifica.Find(" FCINVPCB", pPos + 1, pEnd)
  If pPos > 0 Then
    PFine = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
    For pInit = pPos To pPos - 80 Step -1
      If Mid$(RTBModifica.Text, pInit, 2) = vbCrLf Then
         Exit For
      End If
    Next pInit
    pPos = RTBModifica.Find(".", pInit, PFine)
    While pPos < 1
       PFine = RTBModifica.Find(vbCrLf, PFine + 2, pEnd)
       pPos = RTBModifica.Find(".", pInit, PFine)
    Wend
    pPos = RTBModifica.Find(" IF ", pInit, PFine)
    If pPos > 0 Then
       PFine = PFine - 2
       pPos = pInit - 4
       pPos = RTBModifica.Find(vbCrLf, pPos + 1, PFine)
       While pPos > 0
         RTBModifica.SelStart = pPos + 8
         RTBModifica.SelLength = 1
         RTBModifica.SelText = "*"
         pPos = RTBModifica.Find(vbCrLf, pPos + 1, PFine)
       Wend
    End If
    pPos = PFine + 1
  End If
Wend


'pPos = 1
'While pPos > 0
'  pPos = RTBModifica.Find("DLITCBL", pPos + 1, pEnd)
'  If pPos > 0 Then
'    PFine = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
'    For pInit = pPos To pPos - 80 Step -1
'      If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then
'         Exit For
'      End If
'    Next pInit
'    pPos = RTBModifica.Find(".", pInit, PFine)
'    While pPos < 1
'       PFine = RTBModifica.Find(vbCrLf, PFine + 2, pEnd)
'       pPos = RTBModifica.Find(".", pInit, PFine)
'    Wend
'    pPos = RTBModifica.Find(" ENTRY ", pInit, PFine)
'    If pPos > 0 Then
'       PFine = PFine - 2
'       pPos = pInit - 4
'       pPos = RTBModifica.Find(vbCrLf, pPos + 1, PFine)
'       While pPos > 0
'         RTBModifica.SelStart = pPos + 8
'         RTBModifica.SelLength = 1
'         RTBModifica.SelText = "*"
'         pPos = RTBModifica.Find(vbCrLf, pPos + 1, PFine)
'       Wend
'       pPos = RTBModifica.Find(vbCrLf, PFine - 5, PFine + 4)
'       RTBModifica.SelText = vbCrLf & Space$(11) & "MOVE SPACES TO LNKDB-STATO.  " & vbCrLf
'       pPos = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
'       PFine = pPos
'    End If
'    pPos = PFine + 1
'  End If
'Wend
'


End Function
Function CambiaParole() As Boolean
Dim Ind As Integer
Dim K As Variant

Dim pEnd As Variant
Dim pPos As Variant
Dim pStart As Variant
Dim pInit As Variant
Dim ModSw As Boolean
Dim SwTolta As Boolean
Dim Num01 As Integer
Dim xpos As Variant
Dim xLen As Variant

Dim wStr As String

Num01 = 0
If ChkDbCbl = vbChecked Then
  pEnd = Len(RTBModifica.Text)
  
  pPos = 1
  pPos = RTBModifica.Find(" PROCEDURE ", pPos + 1, pEnd)
  pEnd = pPos - 20
  
  pPos = 1
  pPos = RTBModifica.Find(" LINKAGE ", pPos + 1, pEnd)
  pPos = RTBModifica.Find(" COPY DLIUIB", pPos + 1, pEnd)
  If pPos > 0 Then
     For pStart = pPos To pPos - 160 Step -1
         If Mid$(RTBModifica.Text, pStart, 2) = vbCrLf Then Exit For
     Next pStart
     RTBModifica.SelStart = pStart + 1
     RTBModifica.SelLength = 7
     RTBModifica.SelText = SEGN_L
  End If
  
'  pPos = 1
'  pPos = RTBModifica.Find(" LINKAGE ", pPos + 1, pEnd)
'
'  While pPos > 0
'     pPos = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
'     If pPos > 0 Then
'        RTBModifica.SelStart = pPos + 8
'        RTBModifica.SelLength = 1
'        If RTBModifica.SelText <> "*" Then
'           RTBModifica.SelStart = pPos + 9
'           RTBModifica.SelLength = 2
'           If RTBModifica.SelText = "01" Then
'              Num01 = Num01 + 1
'           End If
'           If Num01 > 1 Then
'              RTBModifica.SelStart = pPos + 2
'              RTBModifica.SelLength = 7
'              RTBModifica.SelText = Segn_L
'           End If
'        End If
'     End If
'  Wend
End If

For Ind = 1 To UBound(INCATabParole)
  pEnd = Len(RTBModifica.Text)
  pPos = 1
  pPos = RTBModifica.Find(" PROCEDURE ", pPos + 1, pEnd)
  While pPos > 0
     pPos = RTBModifica.Find(" " & INCATabParole(Ind - 1).Origine & " ", pPos + 1, pEnd)
     If pPos > 0 Then
       xpos = RTBModifica.SelStart
       xLen = RTBModifica.SelLength
       ModSw = True
       For pInit = pPos To pPos - 80 Step -1
          If Mid$(RTBModifica.Text, pInit, 2) = vbCrLf Then
             If Mid$(RTBModifica.Text, pInit + 8, 1) = "*" Then
                ModSw = False
             End If
             Exit For
          End If
       Next pInit
       If ModSw Then
          xLen = xpos - pInit + 1 + Len(INCATabParole(Ind - 1).Origine)
          RTBModifica.SelText = " " & INCATabParole(Ind - 1).Dest & " " & vbCrLf & Space$(xLen)
       End If
     End If
  Wend
Next Ind

CambiaParole = True

'For Ind = 1 To UBound(CmpPcb)
'  For K = 1 To 9
'    If Trim(CmpPcb(Ind).Campo(K)) <> "" And Trim(CmpPcb(Ind).Campo(K)) <> "FILLER" Then
'       wStr = " " & Trim(CmpPcb(Ind).Campo(K))
'       pEnd = Len(RTBModifica.text)
'       pPos = 1
'       pPos = RTBModifica.Find(" PROCEDURE ", pPos + 1, pEnd)
'       While pPos > 0
'          pPos = RTBModifica.Find(wStr, pPos + 1, pEnd)
'          If pPos > 0 Then
'               Select Case Mid$(RTBModifica.text, pPos + Len(wStr) + 1, 1)
'                 Case " ", ",", ".", Mid$(vbCrLf, 1, 1)
'                    Select Case K
'                       Case 3
'                          For pStart = pPos To pPos - 160 Step -1
'                             If Mid$(RTBModifica.text, pStart, 2) = vbCrLf Then Exit For
'                          Next pStart
'                          If Mid$(RTBModifica.text, pStart + 8, 1) <> "*" Then
'                             RTBModifica.SelText = " LNKDB-RDBSTAT" & vbCrLf & Space$(15)
'                          End If
'                       Case 6
'                          For pStart = pPos To pPos - 160 Step -1
'                             If Mid$(RTBModifica.text, pStart, 2) = vbCrLf Then Exit For
'                          Next pStart
'                          If Mid$(RTBModifica.text, pStart + 8, 1) <> "*" Then
'                             RTBModifica.SelText = " LNKDB-RDBSEGM" & vbCrLf & Space$(15)
'                          End If
'                       Case 9
'                          For pStart = pPos To pPos - 160 Step -1
'                             If Mid$(RTBModifica.text, pStart, 2) = vbCrLf Then Exit For
'                          Next pStart
'                          If Mid$(RTBModifica.text, pStart + 8, 1) <> "*" Then
'                             RTBModifica.SelText = " LNKDB-FDBKEY" & vbCrLf & Space$(15)
'                          End If
'                       Case Else
'                           For pStart = pPos To pPos - 160 Step -1
'                               If Mid$(RTBModifica.text, pStart, 2) = vbCrLf Then Exit For
'                           Next pStart
'                           RTBModifica.SelStart = pStart + 1
'                           RTBModifica.SelLength = 6
'                           RTBModifica.SelText = "ITER-A"
'                     End Select
'                  Case Else
''                    For pStart = pPos To pPos - 160 Step -1
''                        If Mid$(RTBModifica.text, pStart, 2) = vbCrLf Then Exit For
''                    Next pStart
''                    RTBModifica.SelStart = pStart + 1
''                    RTBModifica.SelLength = 6
''                    RTBModifica.SelText = "ITER-?"
'                End Select
'          End If
'       Wend
'    End If
'  Next K
'Next Ind

pEnd = Len(RTBModifica.Text)
pPos = 1
pPos = RTBModifica.Find(" PROCEDURE ", pPos + 1, pEnd)
If pPos > 0 Then
   pInit = pPos
   pEnd = RTBModifica.Find(".", pInit + 1, pEnd)
   For Ind = 1 To UBound(CmpPcb)
     pPos = RTBModifica.Find(CmpPcb(Ind).NomePCB, pInit, pEnd)
     If pPos > 0 Then
        SwTolta = False
        RTBModifica.SelText = Space$(Len(CmpPcb(Ind).NomePCB))
        For K = pPos - 1 To pPos - 80 Step -1
           RTBModifica.SelStart = K
           RTBModifica.SelLength = 1
           Select Case RTBModifica.SelText
             Case " "
             Case ","
                RTBModifica.SelText = " "
                SwTolta = True
                Exit For
             Case Else
                Exit For
           End Select
        Next K
        If Not SwTolta Then
          For K = pPos + Len(CmpPcb(Ind).NomePCB) To pEnd
            RTBModifica.SelStart = K
            RTBModifica.SelLength = 1
            Select Case RTBModifica.SelText
              Case " "
              Case "."
                 pPos = RTBModifica.Find("USING", pInit, pEnd)
                 RTBModifica.SelText = Space$(5)
                 Exit For
              Case ","
                RTBModifica.SelText = " "
                Exit For
              Case Else
                Exit For
            End Select
          Next K
        End If
     End If
   Next Ind
End If
    
End Function
Function AsteriscaIstruzione() As Long
   Dim NumRighe As Long
   Dim pStart As Variant
   Dim pEnd As Variant
   Dim pPos As Variant
   Dim wPosizione As Long
   Dim Flag As Boolean
   Dim wResp As Boolean
   Dim Tb As Recordset
   Dim wIdSegm As Variant
   Dim OldNomeRout As String
   Dim wTipoCall As String
   Dim wStr As String
   Dim K As Integer
   
   OldNomeRout = NomeRoutine
   If OldNomeRout = "" Then
      OldNomeRout = OldNomeRout
   End If
   
   wResp = CaricaIstruzione
   NomeRoutine = ""
   If UBound(TabIstr.BlkIstr) > 0 Then
       NomeRoutine = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).NomeRout
   End If
   
   If NomeRoutine = "" Then
       Set Tb = DbPrj.OpenRecordset("select * from dettistr where idoggetto = " & Rst!IdOggetto & " and nomerout > ' '")
       If Tb.RecordCount > 0 Then
          NomeRoutine = Trim(Tb!NomeRout)
       End If
   End If
   If NomeRoutine = "" Then
       NomeRoutine = OldNomeRout
       AsteriscaIstruzione = 0
       Exit Function
   End If
   
   If OldNomeRout <> "" Then
      If OldNomeRout <> NomeRoutine Then
         RTRoutine.SaveFile PathPrj & "\output-prj\routims\" & Trim(OldNomeRout) & ".cbl", 1
         If Dir(PathPrj & "\output-prj\routims\" & Trim(NomeRoutine) & ".cbl") = "" Then
            wResp = ControllaGenRout
         End If
         RTRoutine.LoadFile PathPrj & "\output-prj\routims\" & Trim(NomeRoutine) & ".cbl"
      End If
   Else
     If Dir(PathPrj & "\output-prj\routims\" & Trim(NomeRoutine) & ".cbl") = "" Then
        wResp = ControllaGenRout
      End If
      pPos = RTRoutine.Find(NomeRoutine, 0)
      If pPos < 0 Then
      
         RTRoutine.LoadFile PathPrj & "\output-prj\routims\" & Trim(NomeRoutine) & ".cbl"
      End If
   End If
  
   NumRighe = Rst!Riga + AddRighe
   
   wTipoCall = "CBLTDLI"
   
   wResp = TROVA_RIGA(RTBModifica, wTipoCall, NumRighe)
          

   If wResp Then
     pPos = RTBModifica.SelStart
     wIdent = 0
     
     For pStart = pPos To pPos - 160 Step -1
       wIdent = wIdent + 1
       If Mid$(RTBModifica.Text, pStart, 2) = vbCrLf Then Exit For
     Next pStart
     wIdent = wIdent - 2
   
     pStart = pStart + 2
     RTBModifica.SelStart = pStart - 1
     RTBModifica.SelLength = 7
     If Trim(TabIstr.Istr) = "PCB" Or TabIstr.Istr = "TERM" Then
        If Trim(TabIstr.Istr) = "PCB" Then
           RTBModifica.SelText = "ITER-P*"
        Else
           RTBModifica.SelText = "ITER-T*"
        End If
     Else
        If Rst!FlgIstr = True Then
           RTBModifica.SelText = "ITER-C*"
        Else
           RTBModifica.SelText = "ITER-N*"
        End If
     End If
     pEnd = RTBModifica.Find(vbCrLf, pStart + 1)
     If wTipoCall = " EXEC " Then
       pEnd = RTBModifica.Find("END-EXEC", pPos + 1, , rtfWholeWord)
       pEnd = RTBModifica.Find(vbCrLf, pEnd + 1)
     Else
       Flag = True
       pEnd = pStart + 1
       pEnd = RTBModifica.Find(vbCrLf, pPos + 1)
       wStr = Mid$(RTBModifica.Text, pPos, pEnd - pPos + 1)
       If InStr(wStr, ".") Then
          Flag = False
       End If
       pPos = pEnd + 1
       While Flag = True
         pEnd = RTBModifica.Find(vbCrLf, pPos + 1)
         wStr = Mid$(RTBModifica.Text, pPos + 2, pEnd - pPos - 1)
         If Len(wStr) > 11 Then wStr = Trim(Mid$(wStr, 12, Len(wStr) - 11))
         If InStr(wStr, ".") > 0 Then
            Flag = False
            pEnd = pPos - 1
         Else
            K = InStr(wStr, " ")
            If K > 0 Then wStr = Trim(Mid$(wStr, 1, K - 1))
            If SeWordCobol(wStr) Then
               Flag = False
               pEnd = pPos - 1
            End If
         End If
         pPos = pEnd + 1
       Wend
     End If
     
     wPosizione = pEnd + 1
     pEnd = pEnd - 2
     pPos = pStart
     pPos = pStart + 1
     Flag = True
     While Flag
       pPos = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
       If pPos = -1 Then
         Flag = False
       End If
       If Flag Then
         RTBModifica.SelStart = pPos + 2
         RTBModifica.SelLength = 7
         If TabIstr.Istr = "PCB" Or TabIstr.Istr = "TERM" Then
           If Trim(TabIstr.Istr) = "PCB" Then
              RTBModifica.SelText = "ITER-P*"
           Else
              RTBModifica.SelText = "ITER-T*"
           End If
         Else
           If Rst!FlgIstr = True Then
              RTBModifica.SelText = "ITER-C*"
           Else
              RTBModifica.SelText = "ITER-N*"
           End If
         End If
       End If
     Wend
     
     AsteriscaIstruzione = wPosizione
  
  Else
     AsteriscaIstruzione = 0
  
  End If
End Function
Function AsteriscaEntry() As Long
   Dim NumRighe As Long
   Dim pStart As Variant
   Dim pEnd As Variant
   Dim pPos As Variant
   Dim wPosizione As Long
   Dim Flag As Boolean
   Dim wResp As Boolean
   Dim Tb As Recordset
   Dim wIdSegm As Variant
   Dim OldNomeRout As String
   Dim wTipoCall As String
   Dim wStr As String
   Dim K As Integer
   Dim testo As String
   
   wPosizione = 0
   pPos = RTBModifica.Find(" 'DLITCBL' ", 1)
   
   If pPos > 0 Then
     
     For pStart = pPos To pPos - 160 Step -1
       If Mid$(RTBModifica.Text, pStart, 2) = vbCrLf Then Exit For
     Next pStart
   
     RTBModifica.SelStart = pStart + 1
     RTBModifica.SelLength = 7
     RTBModifica.SelText = Segn_P
       
       Flag = True
       pPos = pStart + 2
       pEnd = pStart + 1
       pEnd = RTBModifica.Find(vbCrLf, pPos + 1)
       wStr = Mid$(RTBModifica.Text, pPos, pEnd - pPos + 1)
       If InStr(wStr, ".") Then
          Flag = False
       End If
       pPos = pEnd + 1
       While Flag = True
         pEnd = RTBModifica.Find(vbCrLf, pPos + 1)
         wStr = Mid$(RTBModifica.Text, pPos + 2, pEnd - pPos - 1)
         wStr = Mid$(wStr & Space$(72), 1, 72)
         wStr = Trim(Mid$(wStr, 12, Len(wStr) - 11))
         If InStr(wStr, ".") > 0 Then
            Flag = False
         Else
            K = InStr(wStr, " ")
            If K > 0 Then wStr = Trim(Mid$(wStr, 1, K - 1))
            If SeWordCobol(wStr) Then
               Flag = False
               pEnd = pPos - 1
            End If
         End If
         pPos = pEnd + 1
       Wend
       wPosizione = pEnd + 1
       testo = vbCrLf
       testo = testo & "ITER-P*   IL COMANDO ENTRY NON E' PIU' NECESSARIO"
       testo = testo & vbCrLf & "ITER-P* -----------------------------------------------------------"
       testo = testo & vbCrLf & "ITER-P     MOVE SPACES TO LNKDB-STATO"
       
       AddRighe = AddRighe + 3
       RTBModifica.SelStart = wPosizione
       RTBModifica.SelLength = 0
    
       RTBModifica.SelText = testo
       
     End If
     
     AsteriscaEntry = wPosizione

 
End Function
Function CaricaIstruzione() As Boolean
Dim Tb As Recordset
Dim Tb1 As Recordset
Dim Tb2 As Recordset

Dim IndI As Integer
Dim K As Integer
Dim K1 As Integer
Dim K2 As Integer
Dim k3 As Integer

Dim vEnd As Double
Dim vPos As Double
Dim vPos1 As Double
Dim vStart As Double
Dim wStr As String

ReDim TabIstr.BlkIstr(0)

Set Tb = DbPrj.OpenRecordset("select * from Dettistr where idoggetto = " & Rst!IdOggetto _
            & " and riga = " & Rst!Riga _
            & " and progressivo = " & Rst!Progressivo _
            & " order by livello")
            
TabIstr.DBD = Rst!NomeDb
TabIstr.Istr = Rst!Istruzione
TabIstr.PsbId = Rst!IdPSB
Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where idoggetto = " & TabIstr.PsbId)
If Tb1.RecordCount > 0 Then
    TabIstr.PSBName = Tb1!Nome
End If
TabIstr.PCB = Trim(Rst!PCB & " ")
Tb1.Close
If Tb.RecordCount > 0 Then
Tb.MoveFirst
IndI = 0

While Not Tb.EOF
   IndI = IndI + 1
   ReDim Preserve TabIstr.BlkIstr(IndI)
   k3 = 0
   ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
   
   TabIstr.BlkIstr(IndI).SsaName = Trim(Tb!SsaName & " ")
   TabIstr.BlkIstr(IndI).Parentesi = Trim(Tb!qualificazione)
   k3 = k3 + 1
   ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)

   TabIstr.BlkIstr(IndI).KeyDef(k3).Op = Trim(Tb!Operatore & " ")
   TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal = Trim(Tb!Valore & " ")
   
   K = InStr(TabIstr.BlkIstr(IndI).KeyDef(k3).key, " AND ")
   If K > 0 Then
     TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal = Mid$(TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal, 1, K)
   End If
   K = InStr(TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal, " OR ")
   If K > 0 Then
     TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal = Mid$(TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal, 1, K)
   End If

   TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = Tb!KeyLen
   TabIstr.BlkIstr(IndI).KeyDef(k3).KeyBool = ""
   TabIstr.BlkIstr(IndI).KeyDef(k3).KeyNum = 1
   
   TabIstr.BlkIstr(IndI).SegLen = Tb!SegLen
   TabIstr.BlkIstr(IndI).Search = Tb!condizione
   TabIstr.BlkIstr(IndI).NomeRout = Trim(Tb!NomeRout)
   TabIstr.BlkIstr(IndI).KeyDef(k3).Xname = Tb!Xname
   Set Tb1 = DbPrj.OpenRecordset("select * from field where idfield = " & val(Tb!IdField & " "))
   If Tb1.RecordCount > 0 Then
     If Not Tb!Xname Then
        TabIstr.BlkIstr(IndI).KeyDef(k3).key = Tb1!Nome
     Else
        TabIstr.BlkIstr(IndI).KeyDef(k3).key = Tb1!Xname
     End If
   End If
   Set Tb1 = DbPrj.OpenRecordset("select * from strutture where idstruttura = " & Tb!IdStruttura)
   If Tb1.RecordCount > 0 Then
      TabIstr.BlkIstr(IndI).Record = Tb1!Struttura
   End If
   Set Tb1 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb!IdSegm)
   If Tb1.RecordCount > 0 Then
      TabIstr.BlkIstr(IndI).Segment = Tb1!Nome
      TabIstr.BlkIstr(IndI).IdSegm = Tb!IdSegm
      Set Tb1 = DbPrj.OpenRecordset("select * from DMTavole where idsegm = " & Tb!IdSegm)
      If Tb1.RecordCount > 0 Then
         TabIstr.BlkIstr(IndI).Tavola = Tb1!Nome
      End If
   End If
   Tb1.Close
   Set Tb2 = DbPrj.OpenRecordset("select * from Dettistr2 where idoggetto = " & Rst!IdOggetto _
            & " and riga = " & Rst!Riga _
            & " and livello = " & Tb!Livello _
            & " order by progressivo")
   If Tb2.RecordCount > 0 Then
      Tb2.MoveFirst
      k3 = 1
      While Not Tb2.EOF
         k3 = k3 + 1
         ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
         TabIstr.BlkIstr(IndI).KeyDef(k3).Op = Trim(Tb2!Operatore & " ")
         TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal = Trim(Tb2!Valore & " ")
         TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = Trim(Tb2!KeyLen)  ' "" ?
         TabIstr.BlkIstr(IndI).KeyDef(k3).Xname = Tb2!Xname
         Set Tb1 = DbPrj.OpenRecordset("select * from field where idfield = " & val(Tb2!IdField & " "))
         If Tb1.RecordCount > 0 Then
            If Trim(Tb1!Xname) = "" Then
               TabIstr.BlkIstr(IndI).KeyDef(k3).key = Tb1!Nome
            Else
               TabIstr.BlkIstr(IndI).KeyDef(k3).key = Tb1!Xname
            End If
         Else
            k3 = k3
         End If
         If Tb2!And Then
            TabIstr.BlkIstr(IndI).KeyDef(k3).KeyBool = "AND"
         Else
            TabIstr.BlkIstr(IndI).KeyDef(k3).KeyBool = "OR"
         End If
         TabIstr.BlkIstr(IndI).KeyDef(k3).KeyNum = CarNumKey(IndI)

         Tb2.MoveNext
      Wend
      Tb2.Close
   End If
   
   Tb.MoveNext
Wend
End If
SwTp = False
Set Tb = DbPrj.OpenRecordset("select * from proprieta where idoggetto = " & Rst!IdOggetto)
If Tb.RecordCount > 0 Then
   SwTp = Tb!TP
End If
Tb.Close

CaricaIstruzione = True

For K = 1 To UBound(CmpPcb)
   If Trim(TabIstr.PCB) = CmpPcb(K).NomePCB Then
      Exit Function
   End If
Next K
K = UBound(CmpPcb) + 1
ReDim Preserve CmpPcb(K)
CmpPcb(K).NomePCB = Trim(TabIstr.PCB)

vEnd = RTBModifica.Find(" PROCEDURE ", 1)
If vEnd = -1 Then vEnd = Len(RTBModifica.Text)
vStart = RTBModifica.Find(" LINKAGE ", 1)
If vStart = -1 Then Exit Function

wStr = " " & Trim(TabIstr.PCB) & "."

vPos = RTBModifica.Find(wStr, vStart, vEnd)
If vPos > 0 Then
  For vPos1 = vPos To 1 Step -1
     If Mid$(RTBModifica.Text, vPos1, 2) = vbCrLf Then
       Exit For
     End If
  Next vPos1
  RTBModifica.SelStart = vPos1 + 1
  RTBModifica.SelLength = 7
  RTBModifica.SelText = SEGN_L
  vPos1 = RTBModifica.Find(vbCrLf, vPos + 2, vEnd)
  For K1 = 1 To 9
     vPos1 = vPos1 + 2
     vPos = RTBModifica.Find(vbCrLf, vPos1, vEnd)
     RTBModifica.SelStart = vPos1
     RTBModifica.SelLength = vPos - vPos1
     wStr = RTBModifica.SelText
     vPos1 = vPos
     wStr = LTrim(Mid$(wStr, 8))
     If InStr(wStr, " PROCEDURE ") > 0 Then Exit For
     If InStr(wStr, " 01 ") > 0 Then Exit For
     If InStr(wStr, "*") > 0 Then Exit For
     RTBModifica.SelLength = 7
     RTBModifica.SelText = SEGN_L
     
     K2 = InStr(wStr, " ")
     If K2 > 0 Then
       wStr = LTrim(Mid$(wStr, K2))
       K2 = InStr(wStr, " ")
       If K2 > 0 Then wStr = Trim(Mid$(wStr, 1, K2 - 1))
       CmpPcb(K).Campo(K1) = wStr
     End If
  Next K1
End If


End Function
Function CarNumKey(Ind) As Integer
Dim NumKey As Integer
Dim K1 As Integer
Dim k3 As Integer

NumKey = 0

k3 = UBound(TabIstr.BlkIstr(Ind).KeyDef)

For K1 = 1 To k3
  If TabIstr.BlkIstr(Ind).KeyDef(K1).key = TabIstr.BlkIstr(Ind).KeyDef(k3).key Then
     NumKey = NumKey + 1
  End If
Next K1

CarNumKey = NumKey

End Function
Sub INSERT_PCB(wpos As Long)
   Dim testo As String
   Dim NomePsb As String
   Dim K As Integer
   Dim vPos As Double
   Dim wFine As Double
   Dim SwPunto As Boolean
   
'    If Exec Then
'       k = InStr(rst!statment, " PSB")
'       NomePsb = Trim(Mid$(rst!statment, k + 4))
'       k = InStr(NomePsb, ")")
'       NomePsb = UCase(Mid$(NomePsb, 2, k - 2))
'
'    Else
'       NomePsb = TabIstr.PSBName
'       k = InStr(NomePsb, ".")
'       NomePsb = Mid$(NomePsb, 1, k - 1)
'    End If
'    Testo = vbCrLf & Space$(wIdent) & "MOVE 'PCB-" & NomePsb & "'  TO OPERAZIONE OF LNKDBRT-AREA" & vbCrLf
'    Testo = Testo & Space$(wIdent) & "MOVE '00'  TO RDBSEGLV OF LNKDBRT-AREA" & vbCrLf
'    Testo = Testo & Space$(wIdent) & "EXEC CICS LINK PROGRAM('" & NomeRoutine & "')" & vbCrLf
'    Testo = Testo & Space$(wIdent + 5) & "COMMAREA(LNKDBRT-AREA)" & vbCrLf
'    Testo = Testo & Space$(wIdent + 5) & "LENGTH(LEN-LNKDBRT-AREA)" & vbCrLf
'    Testo = Testo & Space$(wIdent) & "END-EXEC"
'    If wPunto Then
'       Testo = Testo & "."
'    End If
'    AddRighe = AddRighe + 6
'
'    RTBModifica.SelStart = wpos
'    RTBModifica.SelLength = 0
''
'    RTBModifica.SelText = Testo
'
'
'    wFine = Len(RTRoutine.text)
'    vPos = RTRoutine.Find("PCB-" & NomePsb, 1, wFine)
'    If vPos = -1 Then
'      vPos = RTRoutine.Find("#OPE#", 1)
'      vPos = RTRoutine.Find(vbCrLf, vPos + 1)
'      vPos = vPos + 2
'      Testo = Space$(12) & "IF OPERAZIONE OF DFHCOMMAREA = 'PCB-" & NomePsb & "' " & vbCrLf
'      Testo = Testo & Space$(15) & "PERFORM ROUT-PCB-" & NomePsb & vbCrLf
'      Testo = Testo & Space$(18) & "THRU EX-ROUT-PCB-" & NomePsb & vbCrLf
'      Testo = Testo & Space$(15) & "GO TO MAIN-001." & vbCrLf
'
'      RTRoutine.SelStart = vPos
'      RTRoutine.SelLength = 0
'      RTRoutine.SelText = Testo
'      wFine = Len(RTRoutine.text)
'      Testo = vbCrLf & "      *------------------------------------------------------------*" & vbCrLf
'      Testo = Testo & "      *    Chiamata di schedulazione psb: " & NomePsb & "  " & vbCrLf
'      Testo = Testo & "      *------------------------------------------------------------*" & vbCrLf
''      Testo = Testo & Space$(7) & "ROUT-PCB-" & NomePsb & "." & vbCrLf
'      Testo = Testo & Space$(11)
''      If Exec Then
'         Testo = Testo & "EXEC DLI SCHEDULE PSB(" & NomePsb & ") END-EXEC. " & vbCrLf
'
''      Else
''         Testo = Testo & "MOVE '" & NomePsb & "' TO PCB-NAME" & vbCrLf & Space$(11)
'''         Testo = Testo & "CALL 'CBLTDLI' USING OP-PCB, PCB-NAME, ADDRESS OF UIB." & vbCrLf
''      End If
'      Testo = Testo & Space$(7) & "EX-ROUT-PCB-" & NomePsb & "." & vbCrLf
'      Testo = Testo & Space$(11) & "EXIT." & vbCrLf & vbCrLf
'      RTRoutine.SelLength = 0
'      RTRoutine.SelStart = wFine
'      RTRoutine.SelText = Testo
'    End If
        
    SwPunto = False
'    If OptExec Then
'       NomePsb = TabIstr.PSBName
'       K = InStr(NomePsb, ".")
'       NomePsb = Mid$(NomePsb, 1, K - 1)
    
'       testo = vbCrLf & Segn_P & Space$(wIdent - 7) & "EXEC DLI SCHEDULE PSB (" & NomePsb & ") END-EXEC"
'       If wPunto Then
'          testo = testo & "."
'          SwPunto = True
'       End If
'       testo = testo & vbCrLf
'       AddRighe = AddRighe + 1
'    Else
       testo = vbCrLf
'    End If
    testo = testo & "ITER-P*   IL COMANDO DLI PCB DOVRA' ESSERE SOSTITUITO IN FASE"
    testo = testo & vbCrLf & "ITER-P*   DI MIGRAZIONE VERSO RDBMS CON OPPORTUNO COMANDO"
    testo = testo & vbCrLf & "ITER-P*   DI BEGIN TRANSACTION"
    testo = testo & vbCrLf & "ITER-P* -----------------------------------------------------------"
    testo = testo & vbCrLf & "ITER-P     MOVE SPACES TO LNKDB-STATO"
    
    If SwPunto Then
       testo = testo & "."
    End If
    
    AddRighe = AddRighe + 5
    RTBModifica.SelStart = wpos
    RTBModifica.SelLength = 0
    
    RTBModifica.SelText = testo

End Sub

Sub INSERT_GU(wpos As Long)
   
   Dim K As Integer
   Dim wIstr As String
   Dim testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   
   wIstr = Trim(Rst!Istruzione)
   
     wIstruzione = OpeIstrRt(CostruisciOperazione(Trim(wIstr), 1)).Istruzione
     
     testo = CostruisciCall(wIstruzione)
        
     RTBModifica.SelStart = wpos
     RTBModifica.SelLength = 0
     RTBModifica.SelText = testo
     wpos = wpos + Len(testo)
        
     ChiudiIf (wpos)
     wResp = AggiornaRout

End Sub

Sub INSERT_DLET(wpos As Long)
   Dim K As Integer
   Dim wIstr As String
   Dim testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   
   wIstr = Rst!Istruzione
     
     wIstruzione = OpeIstrRt(CostruisciOperazione(Trim(wIstr), 1)).Istruzione

     testo = CostruisciCall(wIstruzione)
        
     RTBModifica.SelStart = wpos
     RTBModifica.SelLength = 0
     RTBModifica.SelText = testo
     wpos = wpos + Len(testo)
        
     ChiudiIf (wpos)
     wResp = AggiornaRout
End Sub
Function CostruisciCall(wIstruzione) As String
Dim testo As String
Dim K As Integer
Dim K1 As Integer
Dim K2 As Integer

Dim wStr As String
Dim wStr1 As String
     
     If UBound(TabIstr.BlkIstr) = 0 Then
                testo = vbCrLf & "ITER-N" & "* --------------------------------------------------------------"
        testo = testo & vbCrLf & "ITER-N" & "*   IL COMANDO DLI CHE PRECEDE NON E' STATO DECODIFICATO"
        testo = testo & vbCrLf & "ITER-N" & "*                     CORRETTAMENTE! "
        testo = testo & vbCrLf & "ITER-N" & "* --------------------------------------------------------------"
        testo = testo & vbCrLf & "ITER-N" & "*   IL COMANDO CHE SEGUE E' INTENZIONALMENTE ERRATO (VA RIMOSSO)"
        testo = testo & vbCrLf & "ITER-N" & "*       ED E' STATO INSERITO PER RICHIAMARE L'ATTENZIONE"
        testo = testo & vbCrLf & "ITER-N" & "* --------------------------------------------------------------"
        testo = testo & vbCrLf & "ITER-N" & "      MOVE CAMPO-CHE-NON-ESISTE TO LNKDB-CHIAMANTE"
        testo = testo & vbCrLf & "ITER-N" & "* --------------------------------------------------------------"
        AddRighe = AddRighe + 9
        CostruisciCall = testo
        Exit Function
     End If
        
     testo = vbCrLf
     testo = testo & CreaSp(wIdent) & "INITIALIZE LNKDB-COMANDO " & vbCrLf
     testo = testo & CreaSp(wIdent) & "MOVE '" & Trim(NomeProg) & "' TO LNKDB-CHIAMANTE " & vbCrLf
     testo = testo & CreaSp(wIdent) & "MOVE '00'  TO LNKDB-RDBSEGLV " & vbCrLf
     
     AddRighe = AddRighe + 4
     
     testo = testo & CreaSp(wIdent) & "MOVE '" & wIstruzione & "'  TO LNKDB-OPERAZIONE" & vbCrLf
     AddRighe = AddRighe + 1

     wStr = TabIstr.PCB
     If val(wStr) > 0 Then
        wStr = "'" & wStr & "'"
     Else
        wStr = wStr
     End If
     testo = testo & CreaSp(wIdent) & "MOVE " & wStr & "  TO LNKDB-NUMPCB" & vbCrLf
     AddRighe = AddRighe + 1

     For K = 1 To UBound(TabIstr.BlkIstr)

        If TabIstr.BlkIstr(K).SegLen <> "" And TabIstr.BlkIstr(K).SegLen <> "<none>" Then
           testo = testo & CreaSp(wIdent) & "MOVE " & TabIstr.BlkIstr(K).SegLen & " TO LNKDB-AREALENGTH(" & Format(K, "#0") & ")" & vbCrLf
           AddRighe = AddRighe + 1
        Else
           If TabIstr.BlkIstr(K).Record <> "" Then
              testo = testo & CreaSp(wIdent) & "MOVE LENGTH OF " & TabIstr.BlkIstr(K).Record & " TO LNKDB-AREALENGTH(" & Format(K, "#0") & ")" & vbCrLf
              AddRighe = AddRighe + 1
           End If
        End If
        For K1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
          wStr = TabIstr.BlkIstr(K).KeyDef(K1).KeyVal
          K2 = InStr(wStr, " AND ")
          If K2 > 0 Then
             wStr = Mid$(wStr, 1, K2 - 1)
          End If
          K2 = InStr(wStr, " OR ")
          If K2 > 0 Then
             wStr = Mid$(wStr, 1, K2 - 1)
          End If
          If TabIstr.BlkIstr(K).KeyDef(K1).KeyLen <> "" And TabIstr.BlkIstr(K).KeyDef(K1).KeyLen <> "<none>" Then
             wStr1 = TabIstr.BlkIstr(K).KeyDef(K1).KeyLen
             K2 = InStr(wStr1, ",")
             If K2 > 0 Then wStr1 = Mid$(wStr1, 1, K2 - 1)
             testo = testo & CreaSp(wIdent) & "MOVE " & wStr1 & " TO LNKDB-KEYLENGTH(" & Format(K, "#0") & ", " & Format(K1, "#0") & ")" & vbCrLf
             AddRighe = AddRighe + 1
          Else
             If wStr <> "" Then
                If wStr = "'" Then
                  testo = testo & CreaSp(wIdent) & "MOVE " & str(Len(Trim(wStr)) - 2) & " TO LNKDB-KEYLENGTH(" & Format(K, "#0") & ", " & Format(K1, "#0") & ")" & vbCrLf
                  AddRighe = AddRighe + 1
'                Else
'                  testo = testo & CreaSp(wIdent) & "MOVE LENGTH OF " & TabIstr.BlkIstr(K).KeyDef(K1).key & " TO LNKDB-KEYLENGTH(" & Format(K, "#0") & ", " & Format(K1, "#0") & ")" & vbCrLf
                End If
             End If
          End If
          If wStr <> "" Then
             testo = testo & CreaSp(wIdent) & "MOVE " & wStr & " TO LNKDB-KEYVALUE(" & Format(K, "#0") & ", " & Format(K1, "#0") & ")" & vbCrLf
             AddRighe = AddRighe + 1
          End If
          If TabIstr.BlkIstr(K).KeyDef(K1).Op <> "" Then
             testo = testo & CreaSp(wIdent) & "MOVE '" & TabIstr.BlkIstr(K).KeyDef(K1).Op & "' TO LNKDB-KEYOPER(" & Format(K, "#0") & ", " & Format(K1, "#0") & ")" & vbCrLf
             AddRighe = AddRighe + 1
          End If
        Next K1
        If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & CreaSp(wIdent) & "MOVE " & TabIstr.BlkIstr(K).Record & " TO LNKDB-AREA-DATI(" & Format(K, "#0") & ")" & vbCrLf
           AddRighe = AddRighe + 1
        End If
        If TabIstr.BlkIstr(K).SsaName <> "" And TabIstr.BlkIstr(K).SsaName <> "<none>" Then
           testo = testo & SEGN_X & Space$(wIdent - 7) & "MOVE " & TabIstr.BlkIstr(K).SsaName & " TO LNKDB-SSA-AREA(" & Format(K, "#0") & ")" & vbCrLf
           AddRighe = AddRighe + 1
        End If
     Next K
     If UBound(CmpPcb) > 0 Then
       If OptExec Then
         testo = testo & CreaSp(wIdent) & "MOVE " & CreaNumStack(TabIstr.PCB) & " TO LNKDB-NUMSTACK" & vbCrLf
         AddRighe = AddRighe + 1
       Else
         testo = testo & SEGN_X & Space$(wIdent - 7) & "SET ADDRESS OF " & TabIstr.PCB & " TO AREA-POINTER-R" & vbCrLf
         AddRighe = AddRighe + 1
         testo = testo & SEGN_X & Space$(wIdent - 7) & "MOVE AREA-POINTER TO LNKDB-PCB-POINTER" & vbCrLf
         AddRighe = AddRighe + 1
       End If
     End If
'     testo = testo & Segn_X & Space$(wIdent - 7) & "MOVE DLZDIB TO LNKDB-DLZDIB " & vbCrLf
     AddRighe = AddRighe + 1
     
     If SwTp Then
        testo = testo & CreaSp(wIdent) & "EXEC CICS LINK PROGRAM('" & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).NomeRout & "')" & vbCrLf
        testo = testo & CreaSp(wIdent + 5) & "COMMAREA(LNKDB-AREA)" & vbCrLf
        testo = testo & CreaSp(wIdent + 5) & "LENGTH(LEN-LNKDB-AREA)" & vbCrLf
        testo = testo & CreaSp(wIdent) & "END-EXEC" & vbCrLf
        AddRighe = AddRighe + 4
     Else
        testo = testo & CreaSp(wIdent) & "CALL '" & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).NomeRout & "' USING" & vbCrLf
        testo = testo & CreaSp(wIdent + 5) & "LNKDB-AREA" & vbCrLf
        AddRighe = AddRighe + 2
     End If
     For K = 1 To UBound(TabIstr.BlkIstr)
        If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & CreaSp(wIdent) & "MOVE LNKDB-AREA-DATI(" & Format(K, "#0") & ") TO " & TabIstr.BlkIstr(K).Record & vbCrLf
           AddRighe = AddRighe + 1
        End If
     Next K
     testo = testo & SEGN_X & Space$(wIdent - 7) & "MOVE LNKDB-DLZDIB TO DLZDIB " & vbCrLf
     AddRighe = AddRighe + 1

     CostruisciCall = testo
End Function



Sub INSERT_ISRT(wpos As Long)
   Dim K As Integer
   Dim wIstr As String
   Dim testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   
   wIstr = Rst!Istruzione
     
     wIstruzione = OpeIstrRt(CostruisciOperazione(Trim(wIstr), 1)).Istruzione
     testo = CostruisciCall(wIstruzione)
     
     RTBModifica.SelStart = wpos
     RTBModifica.SelLength = 0
     RTBModifica.SelText = testo
     wpos = wpos + Len(testo)
        
     ChiudiIf (wpos)
     wResp = AggiornaRout
End Sub

Sub INSERT_REPL(wpos As Long)
   Dim K As Integer
   Dim wIstr As String
   Dim testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   
   wIstr = Rst!Istruzione
     wIstruzione = OpeIstrRt(CostruisciOperazione(Trim(wIstr), 1)).Istruzione
     
     testo = CostruisciCall(wIstruzione)
     RTBModifica.SelStart = wpos
     RTBModifica.SelLength = 0
     RTBModifica.SelText = testo
     wpos = wpos + Len(testo)
        
     ChiudiIf (wpos)
     wResp = AggiornaRout
End Sub



Sub INSERT_GN(wpos As Long)
   Dim K As Integer
   Dim wIstr As String
   Dim testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   
   wIstr = Trim(Rst!Istruzione)
     wIstruzione = OpeIstrRt(CostruisciOperazione(Trim(wIstr), 1)).Istruzione
     
     testo = CostruisciCall(wIstruzione)
     RTBModifica.SelStart = wpos
     RTBModifica.SelLength = 0
     RTBModifica.SelText = testo
     wpos = wpos + Len(testo)
        
     ChiudiIf (wpos)
     wResp = AggiornaRout
End Sub
Sub INSERT_NODECOD(wpos As Long)
    Dim testo As String
    
    
            testo = vbCrLf & "ITER-N" & "* --------------------------------------------------------------"
    testo = testo & vbCrLf & "ITER-N" & "*   IL COMANDO DLI CHE PRECEDE NON E' STATO DECODIFICATO"
    testo = testo & vbCrLf & "ITER-N" & "*                     CORRETTAMENTE! "
    testo = testo & vbCrLf & "ITER-N" & "* --------------------------------------------------------------"
    testo = testo & vbCrLf & "ITER-N" & "*   IL COMANDO CHE SEGUE E' INTENZIONALMENTE ERRATO (VA RIMOSSO)"
    testo = testo & vbCrLf & "ITER-N" & "*       ED E' STATO INSERITO PER RICHIAMARE L'ATTENZIONE"
    testo = testo & vbCrLf & "ITER-N" & "* --------------------------------------------------------------"
    testo = testo & vbCrLf & "ITER-N" & "      MOVE CAMPO-CHE-NON-ESISTE TO LNKDB-CHIAMANTE"
    testo = testo & vbCrLf & "ITER-N" & "* --------------------------------------------------------------"
    
    AddRighe = AddRighe + 9
    RTBModifica.SelStart = wpos
    RTBModifica.SelLength = 0
    
    RTBModifica.SelText = testo
     
End Sub
Sub INSERT_TERM(wpos As Long)
    Dim testo As String
'    If OptExec Then
'      testo = vbCrLf & Segn_T & Space$(wIdent - 7) & "EXEC DLI TERMINATE END-EXEC"
'      If wPunto Then
'        testo = testo & "."
'      End If
'      testo = testo & vbCrLf
'      AddRighe = AddRighe + 1
'    Else
      testo = vbCrLf
'    End If
    testo = testo & "ITER-T*   IL COMANDO DLI TERM DOVRA' ESSERE SOSTITUITO IN FASE"
    testo = testo & vbCrLf & "ITER-T*   DI MIGRAZIONE VERSO RDBMS CON OPPORTUNO COMANDO"
    testo = testo & vbCrLf & "ITER-T*   DI SYNCPOINT / ROLLBACK"
    testo = testo & vbCrLf & "ITER-T* -----------------------------------------------------------"
    testo = testo & vbCrLf & "ITER-T     MOVE SPACES TO LNKDB-STATO"
    If SwTp Then
       testo = testo & vbCrLf & "ITER-T     EXEC CICS SYNCPOINT END-EXEC"
       AddRighe = AddRighe + 1
    End If
    If wPunto Then
       testo = testo & "."
    End If
    
    AddRighe = AddRighe + 5
    RTBModifica.SelStart = wpos
    RTBModifica.SelLength = 0
    
    RTBModifica.SelText = testo
     
End Sub

Function CostruisciOperazione(xIstr, index) As Double
  Dim wOpe As String
  Dim K As Double
  Dim xope As String
  Dim Tb As Recordset
  Dim K1 As Integer
  
  If UBound(OpeIstrRt) = 0 Then
     Set Tb = DbPrj.OpenRecordset("select * from Taboperaz order by NumIstr")
     If Tb.RecordCount > 0 Then
       While Not Tb.EOF
         K = Tb!NumIstr
         ReDim Preserve OpeIstrRt(K)
         OpeIstrRt(K).Decodifica = Tb!Decodifica
         OpeIstrRt(K).Istruzione = Tb!Istruzione
         Tb.MoveNext
       Wend
     End If
     Tb.Close
  End If
  
  wOpe = NomeRoutine & Mid$(Trim(xIstr) & "....", 1, 4) ' & Trim(TabIstr.PCB)
        
  For K = 1 To UBound(TabIstr.BlkIstr)
     wOpe = wOpe & "#(" & TabIstr.BlkIstr(K).Tavola
     If TabIstr.BlkIstr(K).Parentesi = "(" Or Trim(TabIstr.BlkIstr(K).SsaName) = "" Then
 '       Select Case Trim(TabIstr.BlkIstr(K).Op)
 '          Case "="
 '             wOpe = wOpe & "EQ"
 '          Case ">"
 '             wOpe = wOpe & "GT"
 '          Case "<"
 '             wOpe = wOpe & "LT"
 '          Case ">=", "=>"
 '             wOpe = wOpe & "GE"
 '          Case "<=", "=<"
 '             wOpe = wOpe & "LE"
 '          Case "<>", "!="
 '             wOpe = wOpe & "NE"
 '          Case Else
 '               wOpe = wOpe & TabIstr.BlkIstr(K).Op
 '       End Select
        For K1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
          If Trim(TabIstr.BlkIstr(K).KeyDef(K1).KeyBool) <> "" Then
            wOpe = wOpe & "." & Trim(TabIstr.BlkIstr(K).KeyDef(K1).KeyBool)
          End If
          If Trim(TabIstr.BlkIstr(K).KeyDef(K1).key) <> "" Then
            wOpe = wOpe & "-" & Trim(TabIstr.BlkIstr(K).KeyDef(K1).key)
          End If
        Next K1
        Select Case Trim(TabIstr.BlkIstr(K).Search)
            Case "(None)", ""
               wOpe = wOpe & ")"
            Case Else
              wOpe = wOpe & ")<" & Trim(TabIstr.BlkIstr(K).Search) & ">"
        End Select
     Else
        wOpe = wOpe & ")"
     End If
  Next K
  xope = ""
  For K = 1 To UBound(OpeIstrRt)
     If wOpe = OpeIstrRt(K).Decodifica Then
        xope = OpeIstrRt(K).Istruzione
        Exit For
     End If
  Next K
  If xope = "" Then
    K = UBound(OpeIstrRt) + 1
    ReDim Preserve OpeIstrRt(K)
    Me.Caption = "N� Operazioni differenti: " & str(K)
    Me.Refresh
    OpeIstrRt(K).Decodifica = wOpe
    OpeIstrRt(K).Istruzione = Mid$(Trim(xIstr) & "....", 1, 4) & Format(UBound(OpeIstrRt), "000000")
    Set Tb = DbPrj.OpenRecordset("select * from TabOperaz order by NumIstr")
    Tb.AddNew
      Tb!NumIstr = K
      Tb!Istruzione = OpeIstrRt(K).Istruzione
      Tb!Decodifica = OpeIstrRt(K).Decodifica
    Tb.Update
    Tb.Close
  End If
  CostruisciOperazione = K

End Function

Function AggiornaRout() As Boolean
   
   Dim K As Integer
   Dim K1 As Integer
   Dim IndIstr As Double
   Dim wIstr As String
   Dim wIstruzione As String
   Dim xIstruzione As String
   Dim nIstruzione As String
   Dim vPos As Double
   Dim wFine As Double
   Dim testo As String
   Dim SwInsIstr As Boolean
   Dim NumPCB As String
   Dim wXistr As String
   Dim pXistr As Integer
   
   If ChkDbCbl = vbChecked Then
      Select Case TIPO_RDBMS
         Case "VSAM"
             AggiornaRout = AggRoutVSAM()
         Case "DB2"
             AggiornaRout = AggRoutDB2()
         Case Else
             AggiornaRout = False
      End Select
      Exit Function
   End If
   
   wIstr = Rst!Istruzione
   If UBound(TabIstr.BlkIstr) = 0 Then
      AggiornaRout = False
      Exit Function
   End If
   IndIstr = CostruisciOperazione(wIstr, K)
   wIstruzione = OpeIstrRt(IndIstr).Istruzione
   xIstruzione = OpeIstrRt(IndIstr).Decodifica
   nIstruzione = Trim(wIstr) & Mid$(wIstruzione, 5)
   GbNumIstr = Mid$(wIstruzione, 5)
   
   wFine = Len(RTRoutine.Text)
   vPos = RTRoutine.Find("'" & wIstruzione & "'", 1, wFine)
   
   If vPos = -1 Then
      SwInsIstr = True
   Else
      SwInsIstr = False
   End If
   If SwInsIstr Then
      vPos = RTRoutine.Find("#CODICE#", 1)
      vPos = RTRoutine.Find(vbCrLf, vPos + 1)
      vPos = vPos + 2
      testo = vbCrLf & "      **  " & Mid$(wIstruzione & Space$(4), 1, 10) & " - "
      wXistr = xIstruzione
      pXistr = InStr(wXistr, "#")
      While pXistr > 0
         testo = testo & Mid$(wXistr, 1, pXistr - 1) & vbCrLf
         wXistr = Space$(6) & "*" & Space$(16) & Mid$(wXistr, pXistr + 1)
         pXistr = InStr(wXistr, "#")
      Wend
      testo = testo & wXistr & vbCrLf
      RTRoutine.SelStart = vPos
      RTRoutine.SelLength = 0
      RTRoutine.SelText = testo
   End If
   
   If SwInsIstr = -1 Then
      vPos = RTRoutine.Find("#OPE#", 1)
      vPos = RTRoutine.Find(vbCrLf, vPos + 1)
      vPos = vPos + 2
      testo = Space$(12) & "IF LNKDB-OPERAZIONE = '" & wIstruzione & "' " & vbCrLf
      testo = testo & Space$(15) & "PERFORM ROUT-" & nIstruzione & vbCrLf
      testo = testo & Space$(18) & "THRU EX-ROUT-" & nIstruzione & vbCrLf
      testo = testo & Space$(15) & "GO TO MAIN-001." & vbCrLf
      RTRoutine.SelStart = vPos
      RTRoutine.SelLength = 0
      RTRoutine.SelText = testo
      
      wFine = Len(RTRoutine.Text)
      testo = vbCrLf & "      *------------------------------------------------------------*" & vbCrLf
      testo = testo & "      *  Chiamata : " & wIstruzione & "  " & vbCrLf
      testo = testo & "      *  Codifica : " & xIstruzione & "  " & vbCrLf
      testo = testo & "      *------------------------------------------------------------*" & vbCrLf
      testo = testo & Space$(7) & "ROUT-" & nIstruzione & "." & vbCrLf
      
      testo = testo & AggRoutSSA(nIstruzione)
            
      If OptExec Then
         For K = 1 To UBound(TabIstr.BlkIstr)
            If TabIstr.BlkIstr(K).Record <> "" Then
              testo = testo & Space$(11) & "MOVE LNKDB-AREALENGTH(" & Format(K, "#0") & ") TO WK-AREALENGTH-" & Format(K, "00") & vbCrLf
              testo = testo & Space$(11) & "MOVE LNKDB-AREA-DATI(" & Format(K, "#0") & ") TO WK-AREA-DATI-" & Format(K, "00") & vbCrLf
            End If
            For K1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
              If TabIstr.BlkIstr(K).KeyDef(K1).key <> "" Then
                 testo = testo & Space$(11) & "MOVE LNKDB-KEYVALUE(" & Format(K, "#0") & ", " & Format(K1, "#0") & ") TO WK-KEYVALUE(" & Format(K1, "00") & ")" & vbCrLf
                 testo = testo & Space$(11) & "MOVE LNKDB-KEYLENGTH(" & Format(K, "#0") & ", " & Format(K1, "#0") & ") TO WK-KEYLENGTH(" & Format(K1, "00") & ")" & vbCrLf
              End If
            Next K1
         Next K
         
         testo = testo & Space$(11)
         If Mid$(TabIstr.PCB, 1, 1) < "0" And _
            Mid$(TabIstr.PCB, 1, 1) > "9" Then
            NumPCB = Format(val(Mid(TabIstr.PCB, 4)), "00")
         Else
            NumPCB = Format(val(TabIstr.PCB), "00")
         End If
         Select Case Trim(UCase(wIstr))
            Case "DLET"
               testo = testo & "EXEC DLI DELETE USING PCB (" & NumPCB & ") " & vbCrLf
            Case "ISRT"
               testo = testo & "EXEC DLI INSERT USING PCB (" & NumPCB & ") " & vbCrLf
            Case "REPL"
               testo = testo & "EXEC DLI REPLACE USING PCB (" & NumPCB & ") " & vbCrLf
            Case "GU", "GHU"
               testo = testo & "EXEC DLI GET UNIQUE USING PCB (" & NumPCB & ") " & vbCrLf
            Case "GN", "GHN"
               testo = testo & "EXEC DLI GET NEXT USING PCB (" & NumPCB & ") " & vbCrLf
            Case "GNP", "GHNP"
               testo = testo & "EXEC DLI GET NEXT IN PARENT USING PCB (" & NumPCB & ") " & vbCrLf
            Case Else
               testo = testo & "EXEC DLI " & wIstr & " USING PCB (" & NumPCB & ") " & vbCrLf
         End Select
         For K = 1 To UBound(TabIstr.BlkIstr)
            If TabIstr.BlkIstr(K).Segment <> "" Then
               testo = testo & Space$(15) & "SEGMENT ( " & TabIstr.BlkIstr(K).Segment & " ) " & vbCrLf
            End If
            If TabIstr.BlkIstr(K).Record <> "" Then
               Select Case Trim(wIstr)
                  Case "GU", "GHU", "GN", "GHN", "GNP", "GHNP"
                     testo = testo & Space$(18) & "INTO ("
                  Case Else
                     testo = testo & Space$(18) & "FROM ("
               End Select
               testo = testo & "WK-AREA-DATI-" & Format(K, "00") & " )" & vbCrLf
               testo = testo & Space$(18) & "SEGLENGTH ( WK-AREALENGTH-" & Format(K, "00") & " )" & vbCrLf
               
               If InStr(wIstr, "H") > 0 Then
                   testo = testo & Space$(18) & "LOCKED" & vbCrLf
               End If
            End If
            If TabIstr.BlkIstr(K).KeyDef(1).key <> "" Then
              testo = testo & Space$(18) & "WHERE ( "
              For K1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
                If TabIstr.BlkIstr(K).KeyDef(K1).key <> "" Then
                   testo = testo & Space$(28) & TabIstr.BlkIstr(K).KeyDef(K1).KeyBool
                   testo = testo & " " & TabIstr.BlkIstr(K).KeyDef(K1).key
                   testo = testo & " " & TabIstr.BlkIstr(K).KeyDef(K1).Op & " "
                   testo = testo & "WK-KEYVALUE(" & Format(K1, "00") & " )" & vbCrLf
                   testo = testo & Space$(18) & "FIELDLENGTH ( WK-KEYLENGTH(" & Format(K1, "00") & ")" & vbCrLf
                End If
              Next K1
              testo = testo & " )" & vbCrLf
            End If
            If TabIstr.BlkIstr(K).Search <> "" Then
               testo = testo & Space$(21) & TabIstr.BlkIstr(K).Search & vbCrLf
            End If
         Next K
         
         testo = testo & Space$(11) & "END-EXEC. " & vbCrLf
                
      Else
         testo = testo & Space$(11) & "MOVE LNKDB-PCB-POINTER TO WK-POINTER-N" & vbCrLf
         testo = testo & Space$(11) & "SET WK-POINTER TO ADDRESS OF WK-PCB" & vbCrLf
         testo = testo & Space$(11)
         Select Case Trim(UCase(wIstr))
            Case "DLET"
               testo = testo & "CALL 'CBLTDLI' USING OP-DLET WK-PCB " & vbCrLf
            Case "ISRT"
               testo = testo & "CALL 'CBLTDLI' USING OP-ISRT WK-PCB " & vbCrLf
            Case "REPL"
               testo = testo & "CALL 'CBLTDLI' USING OP-REPL WK-PCB " & vbCrLf
            Case "GU"
               testo = testo & "CALL 'CBLTDLI' USING OP-GU   WK-PCB " & vbCrLf
            Case "GHU"
               testo = testo & "CALL 'CBLTDLI' USING OP-GHU  WK-PCB " & vbCrLf
            Case "GN"
               testo = testo & "CALL 'CBLTDLI' USING OP-GN   WK-PCB " & vbCrLf
            Case "GHN"
               testo = testo & "CALL 'CBLTDLI' USING OP-GHN  WK-PCB " & vbCrLf
            Case "GNP"
               testo = testo & "CALL 'CBLTDLI' USING OP-GNP  WK-PCB " & vbCrLf
            Case "GHNP"
               testo = testo & "CALL 'CBLTDLI' USING OP-GHNP WK-PCB " & vbCrLf
            Case Else
               testo = testo & "CALL 'CBLTDLI' USING OP-XXXX WK-PCB " & vbCrLf
         End Select
         For K = 1 To UBound(TabIstr.BlkIstr)
            If TabIstr.BlkIstr(K).Record <> "" Then
               testo = testo & "                            LNKDB-AREA-DATI(" & Format(K, "#0") & ") "
            End If
         Next K
         For K = 1 To UBound(TabIstr.BlkIstr)
            If TabIstr.BlkIstr(K).SsaName <> "" Then
               testo = testo & vbCrLf & "                            LNKDB-SSA-AREA(" & Format(K, "#0") & ") "
            End If
         Next K
         testo = testo & "." & vbCrLf
      
      End If
      For K = 1 To UBound(TabIstr.BlkIstr)
         If TabIstr.BlkIstr(K).Record <> "" Then
            testo = testo & Space$(11) & "MOVE WK-AREA-DATI-" & Format(K, "00") & " TO LNKDB-AREA-DATI(" & Format(K, "#0") & ")." & vbCrLf
         End If
      Next K
      
      testo = testo & Space$(7) & "EX-ROUT-" & nIstruzione & "." & vbCrLf
      testo = testo & Space$(11) & "EXIT." & vbCrLf & vbCrLf
      RTRoutine.SelLength = 0
      RTRoutine.SelStart = wFine
      RTRoutine.SelText = testo
   End If

   AggiornaRout = True
End Function
Function AggRoutVSAM() As Boolean
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim wIstr As String
   Dim wIstruzione As String
   Dim xIstruzione As String
   Dim nIstruzione As String
   Dim vPos As Double
   Dim wFine As Double
   Dim testo As String
   Dim SwInsIstr As Boolean
   Dim NumPCB As String
   Dim Tb1 As Recordset
   Dim Tb2 As Recordset
   Dim wIdDb As Variant
   Dim WIdSeg As Variant
   Dim wXistr As String
   Dim pXistr As Integer

   wIstr = Rst!Istruzione
   If UBound(TabIstr.BlkIstr) = 0 Then
      AggRoutVSAM = False
      Exit Function
   End If
   IndIstr = CostruisciOperazione(wIstr, K)
   wIstruzione = OpeIstrRt(IndIstr).Istruzione
   xIstruzione = OpeIstrRt(IndIstr).Decodifica
   nIstruzione = Trim(wIstr) & Mid$(wIstruzione, 5)
   gIstruzione = nIstruzione
   wFine = Len(RTRoutine.Text)
   vPos = RTRoutine.Find("'" & wIstruzione & "'", 1, wFine)
   
   If vPos = -1 Then
      SwInsIstr = True
   Else
      SwInsIstr = False
   End If
   If SwInsIstr Then
      vPos = RTRoutine.Find("#CODICE#", 1)
      vPos = RTRoutine.Find(vbCrLf, vPos + 1)
      vPos = vPos + 2
      testo = vbCrLf & "      **  " & Mid$(wIstruzione & Space$(4), 1, 10) & " - "
      wXistr = xIstruzione
      pXistr = InStr(wXistr, "#")
      While pXistr > 0
         testo = testo & Mid$(wXistr, 1, pXistr - 1) & vbCrLf
         wXistr = Space$(6) & "*" & Space$(16) & Mid$(wXistr, pXistr + 1)
         pXistr = InStr(wXistr, "#")
      Wend
      testo = testo & wXistr & vbCrLf
      RTRoutine.SelStart = vPos
      RTRoutine.SelLength = 0
      RTRoutine.SelText = testo
   End If
   
   If SwInsIstr = -1 Then
      vPos = RTRoutine.Find("#OPE#", 1)
      vPos = RTRoutine.Find(vbCrLf, vPos + 1)
      vPos = vPos + 2
      testo = Space$(12) & "IF LNKDB-OPERAZIONE = '" & wIstruzione & "' " & vbCrLf
      testo = testo & Space$(15) & "PERFORM ROUT-" & nIstruzione & vbCrLf
      testo = testo & Space$(18) & "THRU EX-ROUT-" & nIstruzione & vbCrLf
      testo = testo & Space$(15) & "GO TO MAIN-001." & vbCrLf
      RTRoutine.SelStart = vPos
      RTRoutine.SelLength = 0
      RTRoutine.SelText = testo
      
      wFine = Len(RTRoutine.Text)
      testo = vbCrLf & "      *------------------------------------------------------------*" & vbCrLf
      testo = testo & "      *  Chiamata : " & wIstruzione & "  " & vbCrLf
      testo = testo & "      *  Codifica : "
      wXistr = xIstruzione
      pXistr = InStr(wXistr, "#")
      While pXistr > 0
         testo = testo & Mid$(wXistr, 1, pXistr - 1) & vbCrLf
         wXistr = Space$(6) & "*" & Space$(16) & Mid$(wXistr, pXistr + 1)
         pXistr = InStr(wXistr, "#")
      Wend
      testo = testo & wXistr & vbCrLf
      testo = testo & "      *------------------------------------------------------------*" & vbCrLf
      testo = testo & Space$(7) & "ROUT-" & nIstruzione & "." & vbCrLf
      
      testo = testo & AggRoutSSA(nIstruzione)
            
      Select Case Trim(UCase(wIstr))
            Case "DLET"
              testo = testo & AggRtVsmDlet()
            Case "ISRT"
              testo = testo & AggRtVsmIsrt()
            Case "REPL"
              testo = testo & AggRtVsmRepl()
            Case "GU"
              testo = testo & AggRtVsmGu()
            Case "GHU"
              testo = testo & AggRtVsmGhu()
            Case "GN"
              testo = testo & AggRtVsmGn()
            Case "GHN"
              testo = testo & AggRtVsmGhn()
            Case "GNP"
              testo = testo & AggRtVsmGnp()
            Case "GHNP"
              testo = testo & AggRtVsmGhnp()
      End Select
      
      testo = testo & Space$(7) & "EX-ROUT-" & nIstruzione & "." & vbCrLf
      testo = testo & Space$(11) & "EXIT." & vbCrLf & vbCrLf
      RTRoutine.SelLength = 0
      RTRoutine.SelStart = wFine
      RTRoutine.SelText = testo
   End If

   AggRoutVSAM = True
End Function
Function AggRoutDB2() As Boolean
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim wIstr As String
   Dim wIstruzione As String
   Dim xIstruzione As String
   Dim nIstruzione As String
   Dim vPos As Double
   Dim wFine As Double
   Dim testo As String
   Dim SwInsIstr As Boolean
   Dim NumPCB As String
   Dim Tb1 As Recordset
   Dim Tb2 As Recordset
   Dim wIdDb As Variant
   Dim WIdSeg As Variant
   Dim wXistr As String
   Dim pXistr As Integer

   wIstr = Rst!Istruzione
   If UBound(TabIstr.BlkIstr) = 0 Then
      AggRoutDB2 = False
      Exit Function
   End If
   IndIstr = CostruisciOperazione(wIstr, K)
   wIstruzione = OpeIstrRt(IndIstr).Istruzione
   xIstruzione = OpeIstrRt(IndIstr).Decodifica
   nIstruzione = Trim(wIstr) & Mid$(wIstruzione, 5)
   GbNumIstr = Mid$(wIstruzione, 5)
   gIstruzione = nIstruzione
   wFine = Len(RTRoutine.Text)
   
   vPos = RTRoutine.Find("'" & wIstruzione & "'", 1, wFine)
   
   If vPos = -1 Then
      SwInsIstr = True
   Else
      SwInsIstr = False
   End If
   If SwInsIstr Then
      vPos = RTRoutine.Find("#CODICE#", 1)
      vPos = RTRoutine.Find(vbCrLf, vPos + 1)
      vPos = vPos + 2
      testo = vbCrLf & "      **  " & Mid$(wIstruzione & Space$(4), 1, 10) & " - "
      testo = testo & DecIstr(xIstruzione)
      RTRoutine.SelStart = vPos
      RTRoutine.SelLength = 0
      RTRoutine.SelText = testo
      
   End If
   
   If SwInsIstr = -1 Then
      vPos = RTRoutine.Find("#OPE#", 1)
      vPos = RTRoutine.Find(vbCrLf, vPos + 1)
      vPos = vPos + 2
      testo = Space$(12) & "IF LNKDB-OPERAZIONE = '" & wIstruzione & "' " & vbCrLf
      testo = testo & Space$(15) & "PERFORM ROUT-" & nIstruzione & vbCrLf
      testo = testo & Space$(18) & "THRU EX-ROUT-" & nIstruzione & vbCrLf
      testo = testo & Space$(15) & "GO TO MAIN-001." & vbCrLf
      RTRoutine.SelStart = vPos
      RTRoutine.SelLength = 0
      RTRoutine.SelText = testo
      
      wFine = Len(RTRoutine.Text)
      testo = vbCrLf & "      *------------------------------------------------------------*" & vbCrLf
      testo = testo & "      *  Chiamata : " & wIstruzione & "  " & vbCrLf
      testo = testo & "      *  Codifica : " & DecIstr(xIstruzione)
      testo = testo & "      *------------------------------------------------------------*" & vbCrLf
      testo = testo & Space$(7) & "ROUT-" & nIstruzione & "." & vbCrLf
      
      testo = testo & AggRoutSSA(nIstruzione)
            
      Select Case Trim(UCase(wIstr))
            Case "DLET"
              testo = testo & AggRtDb2Dlet()
            Case "ISRT"
              testo = testo & AggRtDb2Isrt()
            Case "REPL"
              testo = testo & AggRtDb2Repl()
            Case "GU"
              testo = testo & AggRtDb2Gu()
            Case "GHU"
              MsgBox "Istruzione GHU inaspettata"
              Stop
            Case "GN"
              testo = testo & AggRtDb2Gn()
            Case "GHN"
              MsgBox "Istruzione GHN inaspettata"
              Stop
            Case "GNP"
              testo = testo & AggRtDb2Gnp()
            Case "GHNP"
              MsgBox "Istruzione GHNP inaspettata"
              Stop
      End Select
      
      testo = testo & Space$(7) & "EX-ROUT-" & nIstruzione & "." & vbCrLf
      testo = testo & Space$(11) & "EXIT." & vbCrLf & vbCrLf
      RTRoutine.SelLength = 0
      RTRoutine.SelStart = wFine
      RTRoutine.SelText = testo
   End If

   AggRoutDB2 = True
End Function
Function DecIstr(Stringa) As String
Dim testo As String
Dim wXistr As String
Dim K As Integer
Dim pXistr As Integer
Dim pYistr As Integer
Dim Righe() As String
Dim K1 As Integer

      K1 = 0
      ReDim Righe(K1)


      wXistr = Stringa
      
      K = InStr(wXistr, "GN..")
      If K > 0 Then Mid$(wXistr, K, 4) = "GN@@"
      K = InStr(wXistr, "GNP.")
      If K > 0 Then Mid$(wXistr, K, 4) = "GNP@"
      K = InStr(wXistr, "GU..")
      If K > 0 Then Mid$(wXistr, K, 4) = "GU@@"
       
      pXistr = InStr(wXistr, "#")
      pYistr = InStr(wXistr, ".")
      K = 0
      
      If pYistr > 0 And pXistr > 0 Then
        If pXistr < pYistr Then
           K = pXistr
        Else
           K = pYistr
        End If
      Else
        If pXistr > 0 Then
           K = pXistr
        End If
        If pYistr > 0 Then
           K = pYistr
        End If
      End If
      
      While K > 0
         K1 = UBound(Righe) + 1
         ReDim Preserve Righe(K1)
         Righe(K1) = Mid$(wXistr, 1, K)
         wXistr = Mid$(wXistr, K + 1)
         pXistr = InStr(wXistr, "#")
         pYistr = InStr(wXistr, ".")
         K = 0
      
         If pYistr > 0 And pXistr > 0 Then
           If pXistr < pYistr Then
              K = pXistr
           Else
              K = pYistr
           End If
         Else
           If pXistr > 0 Then
              K = pXistr
           End If
           If pYistr > 0 Then
              K = pYistr
           End If
         End If
      Wend
      
      K1 = UBound(Righe) + 1
      ReDim Preserve Righe(K1)
      Righe(K1) = wXistr
      
      testo = Righe(1) & vbCrLf
      For K = 2 To UBound(Righe)
         K1 = 16
         If Mid$(Righe(K), 1, 1) = "." Then K1 = 16 + 10
         If Mid$(Righe(K), 1, 4) = "AND-" Then K1 = 16 + 6
         If Mid$(Righe(K), 1, 3) = "OR-" Then K1 = 16 + 7
         
         testo = testo & Space$(6) & "*" & Space$(K1) & Righe(K) & vbCrLf
      Next K

      K = InStr(testo, "@")
      While K > 0
        Mid$(testo, K, 1) = "."
        K = InStr(testo, "@")
      Wend
      K = InStr(testo, "#")
      While K > 0
        Mid$(testo, K, 1) = " "
        K = InStr(testo, "#")
      Wend

DecIstr = testo

End Function
Function AggRtDb2Isrt() As String
   
   Dim K As Integer
   Dim testo As String
   
   testo = InitIstr("ISRT", "S")
   If UBound(TabIstr.BlkIstr) > 1 Then
      AggRtDb2Isrt = AggRtDb2IsrtQ
      Exit Function
   End If
   
   If Trim(TabIstr.BlkIstr(1).KeyDef(1).key) <> "" Then
      AggRtDb2Isrt = AggRtDb2IsrtQ
      Exit Function
   End If
   
   testo = testo & Space$(6) & "*" & vbCrLf
   testo = testo & Space$(11) & "PERFORM REST-FDBKEY THRU EX-REST-FDBKEY" & vbCrLf
   testo = testo & Space$(11) & "MOVE '" & TabIstr.BlkIstr(1).Tavola & "' TO WK-NOMETABLE" & vbCrLf
   testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(1).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(1).Segment & "-TO-ADL" & vbCrLf
   testo = testo & Space$(11) & "PERFORM SETK-WKEYADL THRU EX-SETK-WKEYADL" & vbCrLf
   testo = testo & Space$(6) & "*" & vbCrLf
   
   testo = testo & Space$(11) & "EXEC SQL INSERT " & vbCrLf
   testo = testo & Space$(15) & "INTO" & vbCrLf & CostruisciFrom(1, 1, False)
'   Testo = Testo & Space$(15) & "(" & vbCrLf & CostruisciSel(1, 1) & Space$(15) & ")" & vbCrLf
   testo = testo & Space$(15) & "VALUES (" & vbCrLf & CostruisciInto(1, 1, False) & Space$(22) & ")" & vbCrLf
   testo = testo & Space$(11) & "END-EXEC." & vbCrLf
   
   testo = testo & Space$(11) & "MOVE SQLCODE TO WK-SQLCODE." & vbCrLf
   testo = testo & Space$(6) & "* " & vbCrLf

   
   AggRtDb2Isrt = testo

End Function
Function AggRtDb2Dlet() As String
   
   Dim K As Integer
   Dim testo As String
   Dim TestoWhere As String
   
   testo = InitIstr("DLET", "S")
   
   If UBound(TabIstr.BlkIstr) > 1 Then
      AggRtDb2Dlet = AggRtDb2DletQ
      Exit Function
   End If
   
   If Trim(TabIstr.BlkIstr(1).KeyDef(1).key) <> "" Then
      AggRtDb2Dlet = AggRtDb2DletQ
      Exit Function
   End If
   
   testo = testo & Space$(6) & "*" & vbCrLf
   testo = testo & Space$(11) & "PERFORM REST-FDBKEY THRU EX-REST-FDBKEY" & vbCrLf
   testo = testo & Space$(11) & "MOVE WKEY-" & TabIstr.BlkIstr(1).Tavola & " TO WK01-" & TabIstr.BlkIstr(1).Tavola & vbCrLf
   testo = testo & Space$(11) & "MOVE '" & TabIstr.BlkIstr(1).Tavola & "' TO WK-NOMETABLE" & vbCrLf
   testo = testo & Space$(6) & "*" & vbCrLf
   
   testo = testo & Space$(11) & "EXEC SQL DELETE " & vbCrLf
   testo = testo & Space$(15) & "FROM" & vbCrLf & CostruisciFrom(1, 1, True)
   testo = testo & Space$(15) & "WHERE" & vbCrLf & CostruisciWhereP(1, 1)
   testo = testo & Space$(11) & "END-EXEC." & vbCrLf
   
   testo = testo & Space$(11) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
   testo = testo & Space$(11) & "PERFORM DEL-CHILD THRU EX-DEL-CHILD." & vbCrLf
   testo = testo & Space$(6) & "* " & vbCrLf

   AggRtDb2Dlet = testo

End Function
Function AggRtDb2DletQ() As String
   
   Dim K As Integer
   Dim testo As String
   
   testo = InitIstr("DLET", "S")
      
   testo = testo & IstruzioneNonSviluppata("DELETE Qualificata")

   AggRtDb2DletQ = testo

End Function
Function AggRtDb2ReplQ() As String
   
   Dim K As Integer
   Dim testo As String
   
   testo = InitIstr("DLET", "S")
   testo = testo & IstruzioneNonSviluppata("REPLACE Qualificata")

   AggRtDb2ReplQ = testo

End Function

Function AggRtDb2IsrtQ() As String
   
   Dim K As Integer
   Dim testo As String
   
   testo = InitIstr("ISRT", "S")
   
   testo = testo & IstruzioneNonSviluppata("INSERT Qualificata")

   AggRtDb2IsrtQ = testo

End Function
Function AggRtDb2Repl() As String
   
   Dim K As Integer
   Dim testo As String
   
   testo = InitIstr("REPL", "S")
   If UBound(TabIstr.BlkIstr) > 1 Then
      AggRtDb2Repl = AggRtDb2ReplQ
      Exit Function
   End If
   
   If Trim(TabIstr.BlkIstr(1).KeyDef(1).key) <> "" Then
      AggRtDb2Repl = AggRtDb2ReplQ
      Exit Function
   End If
   
   testo = testo & Space$(6) & "*" & vbCrLf
   testo = testo & Space$(11) & "PERFORM REST-FDBKEY THRU EX-REST-FDBKEY" & vbCrLf
   testo = testo & Space$(11) & "MOVE '" & TabIstr.BlkIstr(1).Tavola & "' TO WK-NOMETABLE" & vbCrLf
   testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(1).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(1).Segment & "-TO-ADL" & vbCrLf
   testo = testo & Space$(11) & "PERFORM SETK-WKEYADL THRU EX-SETK-WKEYADL" & vbCrLf
   testo = testo & Space$(6) & "*" & vbCrLf
   
   testo = testo & Space$(11) & "EXEC SQL UPDATE " & vbCrLf
   testo = testo & Space$(15) & CostruisciFrom(1, 1, True)
   testo = testo & Space$(15) & "SET " & vbCrLf & CostruisciSet(1, 1)
   testo = testo & Space$(15) & CostruisciWhereP(1, 1)
   testo = testo & Space$(11) & "END-EXEC." & vbCrLf
   
   testo = testo & Space$(11) & "MOVE SQLCODE TO WK-SQLCODE." & vbCrLf
   testo = testo & Space$(6) & "* " & vbCrLf
   
   AggRtDb2Repl = testo

End Function
Function AggRtDb2Gu() As String
   
   Dim K As Integer
   Dim K1 As Integer
   Dim Tb1 As Recordset
   Dim NomeT As String
   Dim NomeKey As String
   Dim AliasT As String
   Dim Kt As Integer
   Dim testo As String
   Dim AndOr As String
   
   Dim TestoSel As String
   Dim TestoInto As String
   Dim TestoFrom As String
   Dim TestoWhere As String
   
   Dim Operat As String
   
   testo = InitIstr("GU", "S")
   
   If Trim(TabIstr.BlkIstr(1).Segment) = "" Then
      AggRtDb2Gu = AggRtDb2GuNq
      Exit Function
   End If
   
   
   
   TestoSel = Space$(6) & "* " & vbCrLf & Space$(11) _
                               & "EXEC SQL SELECT " & vbCrLf _
                               & CostruisciSel(1, UBound(TabIstr.BlkIstr))
   
   TestoInto = Space$(15) & "INTO " & vbCrLf _
                          & CostruisciInto(1, UBound(TabIstr.BlkIstr), True)
                          
   
   TestoFrom = Space$(15) & "FROM " & vbCrLf _
                          & CostruisciFrom(1, UBound(TabIstr.BlkIstr), True)
   
   TestoWhere = Space$(15) & "WHERE " & vbCrLf _
                           & CostruisciWhere(1, UBound(TabIstr.BlkIstr))

   testo = testo & Space$(6) & "*" & vbCrLf
   
   For K = 1 To UBound(TabIstr.BlkIstr)
   
      Kt = K
      NomeT = TabIstr.BlkIstr(Kt).Tavola
      
      testo = testo & Space$(11) & "MOVE '" & NomeT & "' TO WK-NOMETABLE" & vbCrLf
      For K1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
        If Trim(TabIstr.BlkIstr(K).KeyDef(K1).key) <> "" Then
          NomeKey = TabIstr.BlkIstr(K).KeyDef(K1).key
          Set Tb1 = DbPrj.OpenRecordset("select* from dmindex where idsegm = " & TabIstr.BlkIstr(K).IdSegm & " and nomekey = '" & NomeKey & "' order by ordinale")
          If Tb1.RecordCount = 0 Then
             testo = testo & Space$(11) & "MOVE LNKDB-KEYVALUE(" & Trim(str(K)) & ", " & Format(K1, "#0") & ") TO " & vbCrLf
             testo = testo & Space$(16) & "WKUT-" & TabIstr.BlkIstr(K).Tavola & vbCrLf
             testo = testo & Space$(11) & "PERFORM SETK-WKUTWKEY THRU EX-SETK-WKUTWKEY " & vbCrLf
             testo = testo & Space$(11) & "PERFORM SETC-WKEYWKEY THRU EX-SETC-WKEYWKEY " & vbCrLf
             testo = testo & Space$(11) & "MOVE WKEY-" & NomeT & " TO WK" & Format(TabIstr.BlkIstr(K).KeyDef(K1).KeyNum, "00") & "-" & NomeT & vbCrLf
          Else
             testo = testo & Space$(11) & "MOVE LNKDB-KEYVALUE(" & Trim(str(K)) & ", " & Format(K1, "#0") & ") TO " & vbCrLf
             testo = testo & Space$(16) & "WXX-" & NomeKey & vbCrLf
             testo = testo & Space$(11) & "PERFORM SETK-" & NomeKey & " THRU EX-SETK-" & NomeKey & vbCrLf
             testo = testo & Space$(11) & "MOVE WXX-" & NomeKey & " TO W" & Format(TabIstr.BlkIstr(K).KeyDef(K1).KeyNum, "00") & "-" & NomeKey & vbCrLf
          End If
        End If
      Next K1
   Next K
   
   testo = testo & TestoSel & TestoInto & TestoFrom & TestoWhere
     
   testo = testo & Space$(11) & "END-EXEC." & vbCrLf
    
   testo = testo & Space$(11) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
   testo = testo & Space$(6) & "* " & vbCrLf
   testo = testo & Space$(11) & "IF  WK-SQLCODE = -811 THEN" & vbCrLf
   testo = testo & Space$(13) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
   testo = testo & Space$(11) & "END-IF" & vbCrLf
   
   testo = testo & Space$(11) & "IF WK-SQLCODE NOT EQUAL ZERO THEN" & vbCrLf
   testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & " " & vbCrLf
   testo = testo & Space$(11) & "END-IF" & vbCrLf
   
   testo = testo & Space$(6) & "* " & vbCrLf
   For K = 1 To UBound(TabIstr.BlkIstr)
      NomeT = TabIstr.BlkIstr(K).Tavola
      If TabIstr.BlkIstr(K).Record <> "" Then
         testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
         testo = testo & Space$(11) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
         testo = testo & Space$(11) & "PERFORM SETK-ADLWKEY THRU EX-SETK-ADLWKEY" & vbCrLf
         testo = testo & Space$(6) & "* " & vbCrLf
      End If
   Next K
   testo = testo & Space$(11) & "PERFORM STORE-FDBKEY THRU EX-STORE-FDBKEY" & vbCrLf
   testo = testo & Space$(11) & "MOVE WKEY-" & NomeT & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)." & vbCrLf
     
   testo = testo & Space$(6) & "* " & vbCrLf
     
   AggRtDb2Gu = testo

End Function
Function CostruisciFrom(IndStart As Integer, IndEnd As Integer, SwAlias As Boolean) As String

Dim K As Integer
Dim testo As String
Dim AliasT As String
Dim Tb1 As Recordset

testo = ""

   For K = IndStart To IndEnd
      AliasT = "T" & Format(K, "#0")
      Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where nome = '" & TabIstr.BlkIstr(K).Tavola & "'")
      
      testo = testo & Space$(20) & "FASIADMN." & TabIstr.BlkIstr(K).Tavola
      If SwAlias Then
         testo = testo & " " & AliasT
      End If
      If K <> IndEnd Then
         testo = testo & " ,"
      End If
      testo = testo & vbCrLf
   Next K
   
   CostruisciFrom = testo
End Function

Function CostruisciInto(IndStart As Integer, IndEnd As Integer, NullIndicator) As String

Dim K As Integer
Dim testo As String
Dim K1 As Integer
Dim Tb1 As Recordset
Dim AliasT As String

testo = ""

   For K = IndStart To IndEnd
      AliasT = "T" & Format(K, "#0")
      Set Tb1 = DbPrj.OpenRecordset("select * from dmcolonne where idsegm = " & TabIstr.BlkIstr(K).IdSegm)
      If Tb1.RecordCount > 0 Then
        Tb1.MoveLast
        Tb1.MoveFirst
        K1 = 0
        While Not Tb1.EOF
          K1 = K1 + 1
          testo = testo & Space$(20) & ":ADL-" & TabIstr.BlkIstr(K).Tavola & "." & Mid$(Tb1!Colonna & Space$(20), 1, 19)
          If NullIndicator Then
             testo = testo & vbCrLf & Space$(25) & ":WK-I-" & Mid$(Tb1!Colonna & Space$(20), 1, 19)
          End If
          If Tb1.RecordCount <> K1 Or K <> IndEnd Then
             testo = testo & " ," & vbCrLf
          End If
          testo = testo & vbCrLf
          Tb1.MoveNext
        Wend
      End If
   Next K
   
   CostruisciInto = testo
End Function

Function CostruisciSel(IndStart As Integer, IndEnd As Integer) As String

Dim K As Integer
Dim testo As String
Dim K1 As Integer
Dim Tb1 As Recordset
Dim AliasT As String


testo = ""

   For K = IndStart To IndEnd
      AliasT = "T" & Format(K, "#0")
      Set Tb1 = DbPrj.OpenRecordset("select * from dmcolonne where idsegm = " & TabIstr.BlkIstr(K).IdSegm)
      If Tb1.RecordCount > 0 Then
        Tb1.MoveLast
        Tb1.MoveFirst
        K1 = 0
        While Not Tb1.EOF
          K1 = K1 + 1
          testo = testo & Space$(20) & AliasT & "." & MettiUnderScore(Tb1!Colonna)
          If Tb1.RecordCount <> K1 Or K <> IndEnd Then
             testo = testo & ","
          End If
          testo = testo & vbCrLf
          Tb1.MoveNext
        Wend
      Else
        testo = testo & Space$(20) & AliasT & ".* " & vbCrLf
      End If
   Next K
   
   CostruisciSel = testo
End Function
Function CostruisciSet(IndStart As Integer, IndEnd As Integer) As String

Dim K As Integer
Dim testo As String
Dim K1 As Integer
Dim Tb1 As Recordset
Dim AliasT As String

testo = ""

   For K = IndStart To IndEnd
      AliasT = "T" & Format(K, "#0")
      Set Tb1 = DbPrj.OpenRecordset("select * from dmcolonne where idsegm = " & TabIstr.BlkIstr(K).IdSegm)
      If Tb1.RecordCount > 0 Then
        Tb1.MoveLast
        Tb1.MoveFirst
        K1 = 0
        While Not Tb1.EOF
          K1 = K1 + 1
          testo = testo & Space$(20) & AliasT & "." & MettiUnderScore(Tb1!Colonna) & " = " & vbCrLf
          testo = testo & Space$(24) & ":ADL-" & TabIstr.BlkIstr(K).Tavola & "." & Trim(Tb1!Colonna)
          If Tb1.RecordCount <> K1 Or K <> IndEnd Then
             testo = testo & ","
          End If
          testo = testo & vbCrLf
          Tb1.MoveNext
        Wend
      Else
        testo = testo & Space$(20) & AliasT & ".* " & vbCrLf
      End If
   Next K
   
   CostruisciSet = testo
End Function

Function CostruisciWhere(IndStart As Integer, IndEnd As Integer) As String

Dim K As Integer
Dim testo As String
Dim K1 As Integer
Dim AndOr As String
Dim Tb1 As Recordset
Dim AliasT As String
Dim NomeKey As String
Dim Ope As String
Dim kNumRec As Integer
testo = ""

AndOr = "     (     "

For K = IndStart To IndEnd
   AliasT = "T" & Format(K, "#0")
   For K1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
     If Trim(TabIstr.BlkIstr(K).KeyDef(K1).KeyVal) <> "" Then
       NomeKey = TabIstr.BlkIstr(K).KeyDef(K1).key
       
       Set Tb1 = DbPrj.OpenRecordset("select* from dmindex where idsegm = " & TabIstr.BlkIstr(K).IdSegm & " and nomekey = '" & NomeKey & "' order by ordinale")
       If Tb1.RecordCount > 0 Then
         ' chiave secondaria
         Tb1.MoveLast
         Tb1.MoveFirst
         kNumRec = 0
         While Not Tb1.EOF
            kNumRec = kNumRec + 1
            Ope = TabIstr.BlkIstr(K).KeyDef(K1).Op
            Select Case Trim(Ope)
               Case ">"
                  If kNumRec < Tb1.RecordCount Then
                     Ope = ">="
                  End If
            End Select
            testo = testo & Space$(20) & AndOr & AliasT & "." & MettiUnderScore(ControllaNomeKey(Tb1!NomeColonna, Ope)) & " " & Ope & vbCrLf
            testo = testo & Space$(31) & ":W" & Format(TabIstr.BlkIstr(K).KeyDef(K1).KeyNum, "00") & "-" & TabIstr.BlkIstr(K).KeyDef(K1).key & "." & Mid$(Tb1!NomeColonna & Space$(20), 1, 19) & vbCrLf
            If TabIstr.BlkIstr(K).KeyDef(K1).KeyBool = "AND" Then
               AndOr = "       AND "
            End If
            If TabIstr.BlkIstr(K).KeyDef(K1).KeyBool = "OR" Then
               AndOr = "       OR  "
            End If
            Tb1.MoveNext
         Wend
         testo = Mid$(testo, 1, Len(testo) - 2) & " ) " & vbCrLf
         AndOr = " AND (     "
       Else
         '   chiave primaria
         Set Tb1 = DbPrj.OpenRecordset("select * from dmcolonne where pkey > 0 and idsegm = " & TabIstr.BlkIstr(K).IdSegm & " ")
         Tb1.MoveLast
         Tb1.MoveFirst
         kNumRec = 0
         While Not Tb1.EOF
            kNumRec = kNumRec + 1
            Ope = TabIstr.BlkIstr(K).KeyDef(K1).Op
            Select Case Trim(Ope)
               Case ">"
                  If kNumRec < Tb1.RecordCount Then
                     Ope = ">="
                  End If
            End Select
            testo = testo & Space$(20) & AndOr & AliasT & "." & MettiUnderScore(Tb1!Colonna) & " " & Ope & vbCrLf
            testo = testo & Space$(31) & ":WK" & Format(TabIstr.BlkIstr(K).KeyDef(K1).KeyNum, "00") & "-" & TabIstr.BlkIstr(K).Tavola & "." & Mid$(Tb1!Colonna & Space$(20), 1, 19) & vbCrLf
            AndOr = "       AND "
            Tb1.MoveNext
         Wend
         testo = Mid$(testo, 1, Len(testo) - 2) & " ) " & vbCrLf
         AndOr = " AND (     "
       End If
     End If
   Next K1
Next K
If Trim(testo) = "" Then
   testo = CostruisciWhereP(IndStart, IndEnd)
End If
CostruisciWhere = testo
End Function
Function CostruisciWhereP(IndStart As Integer, IndEnd As Integer) As String

Dim K As Integer
Dim testo As String
Dim K1 As Integer
Dim AndOr As String
Dim Tb1 As Recordset
Dim AliasT As String
Dim NomeKey As String
Dim Ope As String
Dim kNumRec As Integer
testo = ""

AndOr = "     (     "

For K = IndStart To IndEnd
   AliasT = "T" & Format(K, "#0")
   Set Tb1 = DbPrj.OpenRecordset("select * from dmcolonne where pkey > 0 and idsegm = " & TabIstr.BlkIstr(K).IdSegm & " ")
   Tb1.MoveFirst
   While Not Tb1.EOF
     Ope = "="
     testo = testo & Space$(20) & AndOr & AliasT & "." & MettiUnderScore(Tb1!Colonna) & " " & Ope & vbCrLf
     testo = testo & Space$(31) & ":WK01-" & TabIstr.BlkIstr(K).Tavola & "." & Mid$(Tb1!Colonna & Space$(20), 1, 19) & vbCrLf
     AndOr = "       AND "
     Tb1.MoveNext
   Wend
   testo = Mid$(testo, 1, Len(testo) - 2) & " ) " & vbCrLf
Next K

CostruisciWhereP = testo

End Function
Function ControllaNomeKey(Chiave, Ope) As String
  Dim NuovoNome As String
  Select Case Chiave
     Case "AA29LK31", "AA29LK71"
        NuovoNome = "AA29LK01"
        Ope = " LIKE "
     Case "AA29LK32", "AA29LK72"
        NuovoNome = "AA29LK02"
        Ope = " LIKE "
     Case "AA29LK33", "AA29LK73"
        NuovoNome = "AA29LK03"
        Ope = " LIKE "
     Case "AA29LK34", "AA29LK74"
        NuovoNome = "AA29LK04"
        Ope = " LIKE "
     Case "AA29LK35", "AA29LK75"
        NuovoNome = "AA29LK05"
        Ope = " LIKE "
     Case "AA29LK36", "AA29LK76"
        NuovoNome = "AA29LK06"
        Ope = " LIKE "
     Case "AA29LK37", "AA29LK77"
        NuovoNome = "AA29LK07"
        Ope = " LIKE "
     Case "AA29LK38", "AA29LK78"
        NuovoNome = "AA29LK08"
        Ope = " LIKE "
     Case "AA29LK39", "AA29LK79"
        NuovoNome = "AA29LK09"
        Ope = " LIKE "
     Case "AA29LK3A", "AA29LK7A"
        NuovoNome = "AA29LK1A"
        Ope = " LIKE "
     Case "AA29LK3B", "AA29LK7B"
        NuovoNome = "AA29LK1B"
        Ope = " LIKE "
     Case "AA29LK3C", "AA29LK7C"
        NuovoNome = "AA29LK1C"
        Ope = " LIKE "
     Case "IA31LK31", "IA31LK71"
        NuovoNome = "IA31LK01"
        Ope = " LIKE "
     Case "IA31LK32", "IA31LK72"
        NuovoNome = "IA31LK02"
        Ope = " LIKE "
     Case "IA31LK33", "IA31LK73"
        NuovoNome = "IA31LK03"
        Ope = " LIKE "
     Case "IA31LK34", "IA31LK74"
        NuovoNome = "IA31LK04"
        Ope = " LIKE "
     Case "IA31LK35", "IA31LK75"
        NuovoNome = "IA31LK05"
        Ope = " LIKE "
     Case "IA31LK36", "IA31LK76"
        NuovoNome = "IA31LK06"
        Ope = " LIKE "
     Case "IA31LK37", "IA31LK77"
        NuovoNome = "IA31LK07"
        Ope = " LIKE "
     Case "IA31LK38", "IA31LK78"
        NuovoNome = "IA31LK08"
        Ope = " LIKE "
     Case "IA31LK39", "IA31LK79"
        NuovoNome = "IA31LK09"
        Ope = " LIKE "
     Case "IA31LK3A", "IA31LK7A"
        NuovoNome = "IA31LK1A"
        Ope = " LIKE "
     Case "IA31LK3B", "IA31LK7B"
        NuovoNome = "IA31LK1B"
        Ope = " LIKE "
     Case "IA31LK3C", "IA31LK7C"
        NuovoNome = "IA31LK1C"
        Ope = " LIKE "
     Case Else
        NuovoNome = Chiave
  End Select
  ControllaNomeKey = NuovoNome
End Function
Function AggRtDb2GuNq() As String
   Dim testo As String
   testo = IstruzioneNonSviluppata("GU Non Qualificata")
   AggRtDb2GuNq = testo
End Function

Function IstruzioneNonSviluppata(wStringa) As String

Dim testo As String

           testo = "      *------------------------------------------------------------*" & vbCrLf
   testo = testo & "      *  Attenzione la Routine " & wStringa & "  non �" & vbCrLf
   testo = testo & "      *  stata ancora implementata!  L'istruzione successiva" & vbCrLf
   testo = testo & "      *  � stata inserita per generare volutamente un incident!!" & vbCrLf
   testo = testo & "            COMPUTE USERDUMP = USER + DUMP." & vbCrLf
   testo = testo & "      *------------------------------------------------------------*" & vbCrLf
   
   IstruzioneNonSviluppata = testo

End Function

Function AggRtDb2Gn() As String
   
   Dim K As Integer
   Dim K1 As Integer
   Dim Kt As Integer
   Dim NomeT As String
   Dim NomeKey As String
   Dim testo As String
   Dim TestoSel As String
   Dim TestoInto As String
   Dim TestoFrom As String
   Dim TestoWhere As String
   
   Dim Tb1 As Recordset
   
   testo = InitIstr("GN", "S")
   If TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm = 0 Then
      AggRtDb2Gn = AggRtDb2GnNQ
      Exit Function
   End If

   testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(WK-NUMSTACK) = SPACES OR " & vbCrLf
   testo = testo & Space$(11) & "   LNKDB-STKNUMISTR(WK-NUMSTACK) NOT EQUAL " & GbNumIstr & vbCrLf
   
   testo = testo & Space$(14) & "MOVE 'S' TO LNKDB-STKCURSOR(WK-NUMSTACK)" & vbCrLf
   testo = testo & Space$(14) & "MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(WK-NUMSTACK)" & vbCrLf
   
' **************************************************

   TestoSel = Space$(6) & "* " & vbCrLf & Space$(13) _
                               & "EXEC SQL DECLARE C" & GbNumIstr & " CURSOR FOR SELECT " & vbCrLf _
                               & CostruisciSel(1, UBound(TabIstr.BlkIstr))
   
   TestoInto = Space$(15) & "INTO " & vbCrLf _
                          & CostruisciInto(1, UBound(TabIstr.BlkIstr), True)
   
   TestoFrom = Space$(15) & "FROM " & vbCrLf _
                          & CostruisciFrom(1, UBound(TabIstr.BlkIstr), True)
   
    
   TestoWhere = Space$(15) & "WHERE " & vbCrLf _
                           & CostruisciWhere(1, UBound(TabIstr.BlkIstr))

   testo = testo & Space$(6) & "*" & vbCrLf
   
   For K = 1 To UBound(TabIstr.BlkIstr)
   
      Kt = K
      NomeT = TabIstr.BlkIstr(Kt).Tavola
      
      testo = testo & Space$(11) & "MOVE '" & NomeT & "' TO WK-NOMETABLE" & vbCrLf
      For K1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
        If Trim(TabIstr.BlkIstr(K).KeyDef(K1).key) <> "" Then
          NomeKey = TabIstr.BlkIstr(K).KeyDef(K1).key
          Set Tb1 = DbPrj.OpenRecordset("select* from dmindex where idsegm = " & TabIstr.BlkIstr(K).IdSegm & " and nomekey = '" & NomeKey & "' order by ordinale")
          If Tb1.RecordCount = 0 Then
             testo = testo & Space$(13) & "MOVE LNKDB-KEYVALUE(" & Trim(str(K)) & ", " & Format(K1, "#0") & ") TO " & vbCrLf
             testo = testo & Space$(19) & "WKUT-" & TabIstr.BlkIstr(K).Tavola & vbCrLf
             testo = testo & Space$(13) & "PERFORM SETK-WKUTWKEY THRU EX-SETK-WKUTWKEY " & vbCrLf
             testo = testo & Space$(13) & "PERFORM SETC-WKEYWKEY THRU EX-SETC-WKEYWKEY " & vbCrLf
             testo = testo & Space$(13) & "MOVE WKEY-" & NomeT & " TO WK" & Format(TabIstr.BlkIstr(K).KeyDef(K1).KeyNum, "00") & "-" & NomeT & vbCrLf
          Else
             testo = testo & Space$(13) & "MOVE LNKDB-KEYVALUE(" & Trim(str(K)) & ", " & Format(K1, "#0") & ") TO " & vbCrLf
             testo = testo & Space$(19) & "WXX-" & NomeKey & vbCrLf
             testo = testo & Space$(13) & "PERFORM SETK-" & NomeKey & " THRU EX-SETK-" & NomeKey & vbCrLf
             testo = testo & Space$(13) & "MOVE WXX-" & NomeKey & " TO W" & Format(TabIstr.BlkIstr(K).KeyDef(K1).KeyNum, "00") & "-" & NomeKey & vbCrLf
          End If
        End If
      Next K1
   Next K
   
   testo = testo & TestoSel & TestoFrom & TestoWhere
     
   testo = testo & Space$(13) & "END-EXEC" & vbCrLf
    
   testo = testo & Space$(13) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
   testo = testo & Space$(6) & "* " & vbCrLf
   
   testo = testo & Space$(13) & "IF WK-SQLCODE NOT EQUAL ZERO THEN" & vbCrLf
   testo = testo & Space$(16) & "GO TO EX-ROUT-" & gIstruzione & " " & vbCrLf
   testo = testo & Space$(13) & "END-IF" & vbCrLf
   
   testo = testo & Space$(6) & "* " & vbCrLf
   
   testo = testo & Space$(13) & "EXEC SQL OPEN C" & GbNumIstr & vbCrLf
   testo = testo & Space$(13) & "END-EXEC" & vbCrLf
    
   testo = testo & Space$(13) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
   testo = testo & Space$(6) & "* " & vbCrLf
   
   testo = testo & Space$(13) & "IF WK-SQLCODE NOT EQUAL ZERO THEN" & vbCrLf
   testo = testo & Space$(16) & "GO TO EX-ROUT-" & gIstruzione & " " & vbCrLf
   testo = testo & Space$(13) & "END-IF" & vbCrLf
   
   testo = testo & Space$(6) & "* " & vbCrLf
  

' **************************************************
      
   testo = testo & Space$(11) & "END-IF" & vbCrLf
   
   TestoSel = Space$(6) & "* " & vbCrLf & Space$(11) _
                               & "EXEC SQL FETCH C" & GbNumIstr & vbCrLf
   
   testo = testo & TestoSel & TestoInto
   testo = testo & Space$(11) & "END-EXEC." & vbCrLf
   testo = testo & Space$(11) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
   testo = testo & Space$(6) & "* " & vbCrLf
   
   testo = testo & Space$(11) & "IF WK-SQLCODE NOT EQUAL ZERO THEN" & vbCrLf
   testo = testo & Space$(13) & "EXEC SQL CLOSE C" & GbNumIstr & vbCrLf
   testo = testo & Space$(13) & "END-EXEC" & vbCrLf
   testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & " " & vbCrLf
   testo = testo & Space$(11) & "END-IF" & vbCrLf
   
   testo = testo & Space$(6) & "* " & vbCrLf
   
   For K = 1 To UBound(TabIstr.BlkIstr)
      NomeT = TabIstr.BlkIstr(K).Tavola
      If TabIstr.BlkIstr(K).Record <> "" Then
         testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
         testo = testo & Space$(11) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
         testo = testo & Space$(11) & "PERFORM SETK-ADLWKEY THRU EX-SETK-ADLWKEY" & vbCrLf
         testo = testo & Space$(6) & "* " & vbCrLf
      End If
   Next K
   testo = testo & Space$(11) & "PERFORM STORE-FDBKEY THRU EX-STORE-FDBKEY" & vbCrLf
   testo = testo & Space$(11) & "MOVE WKEY-" & NomeT & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)." & vbCrLf
     
   testo = testo & Space$(6) & "* " & vbCrLf
   
   
   
   AggRtDb2Gn = testo

End Function
Function AggRtDb2GnNQ() As String
   
   Dim K As Integer
   Dim testo As String
   
   testo = InitIstr("GN", "N")
   testo = testo & IstruzioneNonSviluppata("GN non Qualificata ")
   
   AggRtDb2GnNQ = testo

End Function
Function AggRtDb2GnpNQ() As String
   
   Dim K As Integer
   Dim testo As String
   
   testo = InitIstr("GNP", "N")
   testo = testo & IstruzioneNonSviluppata("GNP non Qualificata ")
   
   AggRtDb2GnpNQ = testo

End Function

Function AggRtDb2Gnp() As String
   
   Dim K As Integer
   Dim K1 As Integer
   Dim Kt As Integer
   Dim NomeT As String
   Dim NomeKey As String
   Dim testo As String
   Dim TestoSel As String
   Dim TestoInto As String
   Dim TestoFrom As String
   Dim TestoWhere As String
   
   Dim Tb1 As Recordset
   
   testo = InitIstr("GNP", "S")
   If TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm = 0 Then
      AggRtDb2Gnp = AggRtDb2GnpNQ
      Exit Function
   End If

   testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(WK-NUMSTACK) = SPACES OR " & vbCrLf
   testo = testo & Space$(11) & "   LNKDB-STKNUMISTR(WK-NUMSTACK) NOT EQUAL " & GbNumIstr & vbCrLf
   
   testo = testo & Space$(14) & "MOVE 'S' TO LNKDB-STKCURSOR(WK-NUMSTACK)" & vbCrLf
   testo = testo & Space$(14) & "MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(WK-NUMSTACK)" & vbCrLf
   
' **************************************************

   TestoSel = Space$(6) & "* " & vbCrLf & Space$(13) _
                               & "EXEC SQL DECLARE C" & GbNumIstr & " CURSOR FOR SELECT " & vbCrLf _
                               & CostruisciSel(1, UBound(TabIstr.BlkIstr))
   
   TestoInto = Space$(15) & "INTO " & vbCrLf _
                          & CostruisciInto(1, UBound(TabIstr.BlkIstr), True)
   
   TestoFrom = Space$(15) & "FROM " & vbCrLf _
                          & CostruisciFrom(1, UBound(TabIstr.BlkIstr), True)
   
   TestoWhere = Space$(15) & "WHERE " & vbCrLf _
                           & CostruisciWhere(1, UBound(TabIstr.BlkIstr))

   testo = testo & Space$(6) & "*" & vbCrLf
   
   For K = 1 To UBound(TabIstr.BlkIstr)
   
      Kt = K
      NomeT = TabIstr.BlkIstr(Kt).Tavola
      
      testo = testo & Space$(11) & "MOVE '" & NomeT & "' TO WK-NOMETABLE" & vbCrLf
      For K1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
        If Trim(TabIstr.BlkIstr(K).KeyDef(K1).key) <> "" Then
          NomeKey = TabIstr.BlkIstr(K).KeyDef(K1).key
          Set Tb1 = DbPrj.OpenRecordset("select* from dmindex where idsegm = " & TabIstr.BlkIstr(K).IdSegm & " and nomekey = '" & NomeKey & "' order by ordinale")
          If Tb1.RecordCount = 0 Then
             testo = testo & Space$(13) & "MOVE LNKDB-KEYVALUE(" & Trim(str(K)) & ", " & Format(K1, "#0") & ") TO " & vbCrLf
             testo = testo & Space$(19) & "WKUT-" & TabIstr.BlkIstr(K).Tavola & vbCrLf
             testo = testo & Space$(13) & "PERFORM SETK-WKUTWKEY THRU EX-SETK-WKUTWKEY " & vbCrLf
             testo = testo & Space$(13) & "PERFORM SETC-WKEYWKEY THRU EX-SETC-WKEYWKEY " & vbCrLf
             testo = testo & Space$(13) & "MOVE WKEY-" & NomeT & " TO WK" & Format(TabIstr.BlkIstr(K).KeyDef(K1).KeyNum, "00") & "-" & NomeT & vbCrLf
          Else
             testo = testo & Space$(13) & "MOVE LNKDB-KEYVALUE(" & Trim(str(K)) & ", " & Format(K1, "#0") & ") TO " & vbCrLf
             testo = testo & Space$(19) & "WXX-" & NomeKey & vbCrLf
             testo = testo & Space$(13) & "PERFORM SETK-" & NomeKey & " THRU EX-SETK-" & NomeKey & vbCrLf
             testo = testo & Space$(13) & "MOVE WXX-" & NomeKey & " TO W" & Format(TabIstr.BlkIstr(K).KeyDef(K1).KeyNum, "00") & "-" & NomeKey & vbCrLf
          End If
        End If
      Next K1
   Next K
   
   testo = testo & TestoSel & TestoFrom & TestoWhere
     
   testo = testo & Space$(13) & "END-EXEC" & vbCrLf
    
   testo = testo & Space$(13) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
   testo = testo & Space$(6) & "* " & vbCrLf
   
   testo = testo & Space$(13) & "IF WK-SQLCODE NOT EQUAL ZERO THEN" & vbCrLf
   testo = testo & Space$(16) & "GO TO EX-ROUT-" & gIstruzione & " " & vbCrLf
   testo = testo & Space$(13) & "END-IF" & vbCrLf
   
   testo = testo & Space$(6) & "* " & vbCrLf
   
   testo = testo & Space$(13) & "EXEC SQL OPEN C" & GbNumIstr & vbCrLf
   testo = testo & Space$(13) & "END-EXEC" & vbCrLf
    
   testo = testo & Space$(13) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
   testo = testo & Space$(6) & "* " & vbCrLf
   
   testo = testo & Space$(13) & "IF WK-SQLCODE NOT EQUAL ZERO THEN" & vbCrLf
   testo = testo & Space$(16) & "GO TO EX-ROUT-" & gIstruzione & " " & vbCrLf
   testo = testo & Space$(13) & "END-IF" & vbCrLf
   
   testo = testo & Space$(6) & "* " & vbCrLf
  

' **************************************************
      
   testo = testo & Space$(11) & "END-IF" & vbCrLf
   
   TestoSel = Space$(6) & "* " & vbCrLf & Space$(11) _
                               & "EXEC SQL FETCH C" & GbNumIstr & vbCrLf
   
   testo = testo & TestoSel & TestoInto
   testo = testo & Space$(11) & "END-EXEC." & vbCrLf
   testo = testo & Space$(11) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
   testo = testo & Space$(6) & "* " & vbCrLf
   
   testo = testo & Space$(11) & "IF WK-SQLCODE NOT EQUAL ZERO THEN" & vbCrLf
   testo = testo & Space$(13) & "EXEC SQL CLOSE C" & GbNumIstr & vbCrLf
   testo = testo & Space$(13) & "END-EXEC" & vbCrLf
   testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & " " & vbCrLf
   testo = testo & Space$(11) & "END-IF" & vbCrLf
   
   testo = testo & Space$(6) & "* " & vbCrLf
   
   For K = 1 To UBound(TabIstr.BlkIstr)
      NomeT = TabIstr.BlkIstr(K).Tavola
      If TabIstr.BlkIstr(K).Record <> "" Then
         testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
         testo = testo & Space$(11) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
         testo = testo & Space$(11) & "PERFORM SETK-ADLWKEY THRU EX-SETK-ADLWKEY" & vbCrLf
         testo = testo & Space$(6) & "* " & vbCrLf
      End If
   Next K
   testo = testo & Space$(11) & "PERFORM STORE-FDBKEY THRU EX-STORE-FDBKEY" & vbCrLf
   testo = testo & Space$(11) & "MOVE WKEY-" & NomeT & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)." & vbCrLf
     
   testo = testo & Space$(6) & "* " & vbCrLf
   
   AggRtDb2Gnp = testo

End Function

Function AggRtVsmIsrt() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
   If Trim(TabIstr.BlkIstr(1).Segment) = "" Then
      AggRtVsmIsrt = AggRtVsmIsrtNQ
      Exit Function
   End If
   
   testo = InitIstr("ISRT", "S")

   
   For K = 1 To UBound(TabIstr.BlkIstr)
      
      
      If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & Space$(11) & "MOVE '" & TabIstr.BlkIstr(K).Tavola & "' TO WK-DATASET" & vbCrLf
           testo = testo & ScriviVsm(K, "H")
           testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(13) & "PERFORM SET-DBGKEY THRU EX-SET-DBGKEY" & vbCrLf
           testo = testo & Space$(13) & "PERFORM INSERT-GUIDA THRU EX-INSERT-GUIDA" & vbCrLf
           testo = testo & Space$(13) & "IF WK-RETRESP = " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(16) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
           testo = testo & Space$(16) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(16) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(16) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
           testo = testo & Space$(16) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(13) & "ELSE" & vbCrLf
           testo = testo & Space$(16) & "MOVE " & StringaTpBt("203") & " TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(13) & "END-IF" & vbCrLf
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(13) & "MOVE " & StringaTpBt("203") & " TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf

       Else
           testo = testo & PuntaGuida(K, " ")
           testo = testo & Space$(11) & "IF WK-RETRESP NOT EQUAL ZERO " & vbCrLf
           testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
           
       End If
       
       testo = testo & Space$(6) & "*" & vbCrLf
           
   Next K
      

   AggRtVsmIsrt = testo
   
End Function
Function StringaTpBt(wStr) As String
Dim wResp As String
wResp = wStr

If TipoRoutine(NomeRoutine) <> "T" Then
  Select Case wStr
      Case "ZERO"
         wResp = "'00'"
      Case "201"
         wResp = "'A1'"
      Case "202"
         wResp = "'A2'"
      Case "203"
         wResp = "'A3'"
      Case "204"
         wResp = "'A4'"
      Case "205"
         wResp = "'A5'"
      Case Else
         MsgBox "Che codice �?"
  End Select
End If

StringaTpBt = wResp


End Function
Function AggRtVsmDletNQ() As String
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant

   testo = InitIstr("DLET", "N")
   testo = testo & Space$(11) & "PERFORM LEGGI-GUIDA THRU EX-LEGGI-GUIDA." & vbCrLf
   testo = testo & DeleteVsm(0, "G")
   testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
   testo = testo & Space$(13) & "PERFORM DELETE-GUIDA THRU EX-DELETE-GUIDA" & vbCrLf
   testo = testo & Space$(11) & "ELSE " & vbCrLf
   testo = testo & Space$(13) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "END-IF." & vbCrLf
       
   AggRtVsmDletNQ = testo
End Function
Function AggRtVsmReplNQ() As String
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   Dim wIdOggetto As Variant
   
   testo = InitIstr("REPL", "N")
   testo = testo & Space$(11) & "PERFORM LEGGI-GUIDA THRU EX-LEGGI-GUIDA" & vbCrLf

   testo = testo & UpdateVsm(0, "N")
   
   testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
   testo = testo & EvalToADL
   testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "ELSE " & vbCrLf
   testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "END-IF." & vbCrLf
   
   AggRtVsmReplNQ = testo
End Function
Function AggRtVsmIsrtNQ() As String
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant

   testo = InitIstr("ISRT", "N")
   
   testo = testo & ScriviVsm(0, "H")
   
   testo = testo & Space$(11) & "PERFORM INSERT-GUIDA THRU EX-INSERT-GUIDA."

   testo = testo & Space$(11) & "IF WK-RETRESP = " & StringaTpBt("ZERO") & vbCrLf
   testo = testo & EvalToADL
   testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "ELSE " & vbCrLf
   testo = testo & Space$(14) & "MOVE WK-RETRESP TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "END-IF." & vbCrLf

   
   AggRtVsmIsrtNQ = testo
End Function
Function AggRtVsmGuNQ() As String
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant

   testo = InitIstr("GU", "N")
   testo = testo & "PERFORM LEGGI-GUIDA THRU EX-LEGGI-GUIDA" & vbCrLf

   testo = testo & LeggiVsm(K, " ")

   testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
   testo = testo & EvalToADL
   testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "ELSE " & vbCrLf
   testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "END-IF." & vbCrLf
   
   AggRtVsmGuNQ = testo
End Function
Function AggRtVsmGhuNQ() As String
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant

   testo = InitIstr("GHU", "N")
   testo = testo & "PERFORM LEGGI-GUIDA THRU EX-LEGGI-GUIDA" & vbCrLf
   testo = testo & LeggiVsm(K, "H")

   testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
   testo = testo & EvalToADL
   testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "ELSE " & vbCrLf
   testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "END-IF." & vbCrLf
   
   AggRtVsmGhuNQ = testo
End Function
Function EvalToADL() As String

Dim Tb1 As Recordset
Dim Tb2 As Recordset
Dim testo As String
Dim wIdOggetto As Variant

Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
If Tb1.RecordCount = 0 Then
   EvalToADL = "Ma perch� non c'e' il DB?"
   Exit Function
End If
wIdOggetto = Tb1!IdOggetto
Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")
If Tb1.RecordCount = 0 Then
   EvalToADL = "Ma perch� non ci sono Segmenti per il DB?"
   Exit Function
End If

Tb1.MoveFirst
testo = Space$(14) & "EVALUATE WK-DATASET" & vbCrLf
While Not Tb1.EOF
   Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
   testo = testo & Space$(16) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
   testo = testo & Space$(18) & "PERFORM " & Tb2!Nome & "-TO-WK " & vbCrLf
   testo = testo & Space$(21) & "THRU EX-" & Tb2!Nome & "-TO-WK " & vbCrLf
   testo = testo & Space$(18) & "MOVE WK-" & Tb2!Nome & " TO LNKDB-AREA-DATI(1)" & vbCrLf
   testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
   testo = testo & Space$(14) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
   testo = testo & Space$(14) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
   Tb1.MoveNext
Wend
testo = testo & Space$(14) & "END-EVALUATE" & vbCrLf

EvalToADL = testo

End Function
Function AggRtVsmGhu2K() As String
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant

   Set Tb2 = DbPrj.OpenRecordset("select * from dmindex where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and nomekey like '" & TabIstr.BlkIstr(1).key & "*'")
   testo = ""
   If TipoRoutine(NomeRoutine) = "T" Then
        
         testo = testo & Space$(11) & "MOVE '" & Tb2!NomeKey & "' TO WK-DATASET." & vbCrLf
         If Trim(TabIstr.BlkIstr(1).Parentesi) = "(" Then
             testo = testo & Space$(11) & "MOVE LNKDB-KEYVALUE(1) TO WK-KEYVALUE" & vbCrLf
         Else
             testo = testo & Space$(11) & "MOVE LOW-VALUE TO WK-KEYVALUE" & vbCrLf
         End If
         
         testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(1).Tavola & " TO WK-AREALENGTH" & vbCrLf
         testo = testo & Space$(6) & "* " & vbCrLf
         testo = testo & Space$(11) & "EXEC CICS UNLOCK" & vbCrLf
         testo = testo & Space$(21) & "FILE ( WK-DATASET )" & vbCrLf
         testo = testo & Space$(21) & "RESP ( WK-RESP )" & vbCrLf
         testo = testo & Space$(11) & "END-EXEC " & vbCrLf
         testo = testo & Space$(6) & "* " & vbCrLf
         testo = testo & Space$(11) & "EVALUATE LNKDB-SSA-OPER(1)" & vbCrLf
         testo = testo & Space$(14) & "WHEN '= '" & vbCrLf
         testo = testo & Space$(16) & "EXEC CICS READ" & vbCrLf
         testo = testo & Space$(21) & "FILE ( WK-DATASET )" & vbCrLf
         testo = testo & Space$(21) & "INTO ( ADL-" & TabIstr.BlkIstr(1).Tavola & " )" & vbCrLf
         testo = testo & Space$(21) & "RIDFLD ( WK-KEYVALUE )" & vbCrLf
         testo = testo & Space$(21) & "UPDATE" & vbCrLf
         testo = testo & Space$(21) & "EQUAL" & vbCrLf
         testo = testo & Space$(21) & "RESP ( WK-RESP )" & vbCrLf
         testo = testo & Space$(16) & "END-EXEC " & vbCrLf
         testo = testo & Space$(14) & "WHEN OTHER" & vbCrLf
         testo = testo & Space$(16) & "EXEC CICS READ" & vbCrLf
         testo = testo & Space$(21) & "FILE ( WK-DATASET )" & vbCrLf
         testo = testo & Space$(21) & "INTO ( ADL-" & TabIstr.BlkIstr(1).Tavola & " )" & vbCrLf
         testo = testo & Space$(21) & "RIDFLD ( WK-KEYVALUE )" & vbCrLf
         testo = testo & Space$(21) & "UPDATE" & vbCrLf
         testo = testo & Space$(21) & "GTEQ" & vbCrLf
         testo = testo & Space$(21) & "RESP ( WK-RESP )" & vbCrLf
         testo = testo & Space$(16) & "END-EXEC " & vbCrLf
         testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
         
         testo = testo & Space$(11) & "IF WK-RESP = 15 " & vbCrLf
         testo = testo & Space$(13) & "MOVE ZERO TO WK-RESP" & vbCrLf
         testo = testo & Space$(11) & "END-IF" & vbCrLf
         testo = testo & Space$(11) & "IF WK-RESP = 0" & vbCrLf
         testo = testo & Space$(13) & "PERFORM " & TabIstr.BlkIstr(1).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(1).Segment & "-TO-WK" & vbCrLf
         testo = testo & Space$(13) & "MOVE WK-" & TabIstr.BlkIstr(1).Segment & " TO LNKDB-AREA-DATI(1)" & vbCrLf
         testo = testo & Space$(13) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
         testo = testo & Space$(13) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
         testo = testo & Space$(13) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
         testo = testo & Space$(13) & "MOVE ZERO TO WK-RETRESP" & vbCrLf
         testo = testo & Space$(11) & "ELSE" & vbCrLf
         testo = testo & Space$(13) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
         testo = testo & Space$(11) & "END-IF." & vbCrLf
   Else
         MsgBox "Batch: ghu2k"
   End If
   AggRtVsmGhu2K = testo
End Function
Function AggRtVsmGhn2K() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant

   Set Tb2 = DbPrj.OpenRecordset("select * from dmindex where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and nomekey like '" & TabIstr.BlkIstr(1).key & "*'")
   testo = ""
   
   If TipoRoutine(NomeRoutine) = "T" Then
       testo = testo & Space$(11) & "MOVE '" & Tb2!NomeKey & "' TO WK-DATASET." & vbCrLf
       testo = testo & Space$(11) & "MOVE LNKDB-STKFDBKEY(LNKDB-NUMSTACK) TO WK-KEYVALUE" & vbCrLf
       testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(1).Tavola & " TO WK-AREALENGTH" & vbCrLf
       testo = testo & Space$(6) & "* " & vbCrLf
       testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
       testo = testo & Space$(13) & "EXEC CICS ENDBR" & vbCrLf
       testo = testo & Space$(20) & "FILE (  WK-DATASET ) " & vbCrLf
       testo = testo & Space$(20) & "RESP (WK-RESP )" & vbCrLf
       testo = testo & Space$(13) & "END-EXEC" & vbCrLf
       testo = testo & Space$(13) & "EXEC CICS STARTBR" & vbCrLf
       testo = testo & Space$(23) & "FILE ( WK-DATASET )" & vbCrLf
       testo = testo & Space$(23) & "RIDFLD ( WK-KEYVALUE )" & vbCrLf
       testo = testo & Space$(23) & "GTEQ" & vbCrLf
       testo = testo & Space$(23) & "RESP ( WK-RESP )" & vbCrLf
       testo = testo & Space$(13) & "END-EXEC" & vbCrLf
       testo = testo & Space$(13) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK) " & vbCrLf
       testo = testo & Space$(11) & "END-IF" & vbCrLf
       testo = testo & Space$(11) & "IF WK-RESP = 15 " & vbCrLf
       testo = testo & Space$(13) & "MOVE ZERO TO WK-RESP" & vbCrLf
       testo = testo & Space$(11) & "END-IF" & vbCrLf
       testo = testo & Space$(6) & "* " & vbCrLf
       testo = testo & Space$(11) & "IF WK-RESP = 0 " & vbCrLf
       testo = testo & Space$(13) & "EXEC CICS READNEXT" & vbCrLf
       testo = testo & Space$(23) & "FILE ( WK-DATASET )" & vbCrLf
       testo = testo & Space$(23) & "INTO ( ADL-" & TabIstr.BlkIstr(1).Tavola & " )" & vbCrLf
       testo = testo & Space$(23) & "RIDFLD ( WK-KEYVALUE )" & vbCrLf
'       testo = testo & Space$(23) & "UPDATE " & vbCrLf
       testo = testo & Space$(23) & "RESP ( WK-RESP )" & vbCrLf
       testo = testo & Space$(13) & "END-EXEC" & vbCrLf
       testo = testo & Space$(11) & "END-IF" & vbCrLf
       testo = testo & Space$(11) & "IF WK-RESP = 15 " & vbCrLf
       testo = testo & Space$(13) & "MOVE ZERO TO WK-RESP" & vbCrLf
       testo = testo & Space$(11) & "END-IF" & vbCrLf
    
       testo = testo & Space$(11) & "IF WK-RESP = 0" & vbCrLf
       testo = testo & Space$(13) & "PERFORM " & TabIstr.BlkIstr(1).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(1).Segment & "-TO-WK" & vbCrLf
       testo = testo & Space$(13) & "MOVE WK-" & TabIstr.BlkIstr(1).Segment & " TO LNKDB-AREA-DATI(1)" & vbCrLf
       testo = testo & Space$(13) & "MOVE ZERO TO WK-RETRESP" & vbCrLf
       testo = testo & Space$(11) & "ELSE" & vbCrLf
       testo = testo & Space$(13) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
       testo = testo & Space$(11) & "END-IF." & vbCrLf
   Else
       MsgBox "Batch: ghn2k"
   End If
   AggRtVsmGhn2K = testo
End Function
Function AggRtVsmGn2K() As String
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant

   Set Tb2 = DbPrj.OpenRecordset("select * from dmindex where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and nomekey like '" & TabIstr.BlkIstr(1).key & "*'")
   testo = ""
   If TipoRoutine(NomeRoutine) = "T" Then
    
       testo = testo & Space$(11) & "MOVE '" & Tb2!NomeKey & "' TO WK-DATASET." & vbCrLf
       testo = testo & Space$(11) & "MOVE LNKDB-STKFDBKEY(LNKDB-NUMSTACK) TO WK-KEYVALUE" & vbCrLf
       testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(1).Tavola & " TO WK-AREALENGTH" & vbCrLf
       testo = testo & Space$(6) & "* " & vbCrLf
       testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
       testo = testo & Space$(13) & "EXEC CICS ENDBR" & vbCrLf
       testo = testo & Space$(20) & "FILE (  WK-DATASET ) " & vbCrLf
       testo = testo & Space$(20) & "RESP (WK-RESP )" & vbCrLf
       testo = testo & Space$(13) & "END-EXEC" & vbCrLf
       testo = testo & Space$(13) & "EXEC CICS STARTBR" & vbCrLf
       testo = testo & Space$(23) & "FILE ( WK-DATASET )" & vbCrLf
       testo = testo & Space$(23) & "RIDFLD ( WK-KEYVALUE )" & vbCrLf
       testo = testo & Space$(23) & "GTEQ" & vbCrLf
       testo = testo & Space$(23) & "RESP ( WK-RESP )" & vbCrLf
       testo = testo & Space$(13) & "END-EXEC" & vbCrLf
       testo = testo & Space$(13) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK) " & vbCrLf
       testo = testo & Space$(11) & "END-IF" & vbCrLf
       testo = testo & Space$(11) & "IF WK-RESP = 15 " & vbCrLf
       testo = testo & Space$(13) & "MOVE ZERO TO WK-RESP" & vbCrLf
       testo = testo & Space$(11) & "END-IF" & vbCrLf
       testo = testo & Space$(6) & "* " & vbCrLf
       testo = testo & Space$(11) & "IF WK-RESP = 0 " & vbCrLf
       testo = testo & Space$(13) & "EXEC CICS READNEXT" & vbCrLf
       testo = testo & Space$(23) & "FILE ( WK-DATASET )" & vbCrLf
       testo = testo & Space$(23) & "INTO ( ADL-" & TabIstr.BlkIstr(1).Tavola & " )" & vbCrLf
       testo = testo & Space$(23) & "RIDFLD ( WK-KEYVALUE )" & vbCrLf
       testo = testo & Space$(23) & "RESP ( WK-RESP )" & vbCrLf
       testo = testo & Space$(13) & "END-EXEC" & vbCrLf
       testo = testo & Space$(11) & "END-IF" & vbCrLf
       testo = testo & Space$(11) & "IF WK-RESP = 15 " & vbCrLf
       testo = testo & Space$(13) & "MOVE ZERO TO WK-RESP" & vbCrLf
       testo = testo & Space$(11) & "END-IF" & vbCrLf
       
       testo = testo & Space$(11) & "IF WK-RESP = 0 " & vbCrLf
       testo = testo & Space$(13) & "EXEC CICS READNEXT" & vbCrLf
       testo = testo & Space$(23) & "FILE ( WK-DATASET )" & vbCrLf
       testo = testo & Space$(23) & "INTO ( ADL-" & TabIstr.BlkIstr(1).Tavola & " )" & vbCrLf
       testo = testo & Space$(23) & "RIDFLD ( WK-KEYVALUE )" & vbCrLf
       testo = testo & Space$(23) & "RESP ( WK-RESP )" & vbCrLf
       testo = testo & Space$(13) & "END-EXEC" & vbCrLf
       testo = testo & Space$(11) & "END-IF" & vbCrLf
       testo = testo & Space$(11) & "IF WK-RESP = 15 " & vbCrLf
       testo = testo & Space$(13) & "MOVE ZERO TO WK-RESP" & vbCrLf
       testo = testo & Space$(11) & "END-IF" & vbCrLf
    
       testo = testo & Space$(11) & "IF WK-RESP = 0" & vbCrLf
       testo = testo & Space$(13) & "PERFORM " & TabIstr.BlkIstr(1).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(1).Segment & "-TO-WK" & vbCrLf
       testo = testo & Space$(13) & "MOVE WK-" & TabIstr.BlkIstr(1).Segment & " TO LNKDB-AREA-DATI(1)" & vbCrLf
       testo = testo & Space$(13) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
       testo = testo & Space$(13) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
       testo = testo & Space$(13) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
       testo = testo & Space$(13) & "MOVE ZERO TO WK-RETRESP" & vbCrLf
       testo = testo & Space$(11) & "ELSE" & vbCrLf
       testo = testo & Space$(13) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
       testo = testo & Space$(11) & "END-IF." & vbCrLf
   Else
       MsgBox "Batch: gn2k"
   End If
   AggRtVsmGn2K = testo
End Function
Function AggRtVsmGu2K() As String
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant

   Set Tb2 = DbPrj.OpenRecordset("select * from dmindex where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and nomekey like '" & TabIstr.BlkIstr(1).key & "*'")
   testo = ""
   If TipoRoutine(NomeRoutine) = "T" Then
        
        testo = testo & Space$(11) & "MOVE '" & Tb2!NomeKey & "' TO WK-DATASET." & vbCrLf
        
        If Trim(TabIstr.BlkIstr(1).Parentesi) = "(" Then
            testo = testo & Space$(11) & "MOVE LNKDB-KEYVALUE(1) TO WK-KEYVALUE" & vbCrLf
        Else
            testo = testo & Space$(11) & "MOVE LOW-VALUE TO WK-KEYVALUE" & vbCrLf
        End If
        testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(1).Tavola & " TO WK-AREALENGTH" & vbCrLf
        testo = testo & Space$(6) & "* " & vbCrLf
        testo = testo & Space$(11) & "EVALUATE LNKDB-SSA-OPER(1)" & vbCrLf
        testo = testo & Space$(14) & "WHEN '= '" & vbCrLf
        testo = testo & Space$(16) & "EXEC CICS READ" & vbCrLf
        testo = testo & Space$(21) & "FILE ( WK-DATASET )" & vbCrLf
        testo = testo & Space$(21) & "INTO ( ADL-" & TabIstr.BlkIstr(1).Tavola & " )" & vbCrLf
        testo = testo & Space$(21) & "RIDFLD ( WK-KEYVALUE )" & vbCrLf
        testo = testo & Space$(21) & "EQUAL" & vbCrLf
        testo = testo & Space$(21) & "RESP ( WK-RESP )" & vbCrLf
        testo = testo & Space$(16) & "END-EXEC " & vbCrLf
        testo = testo & Space$(14) & "WHEN OTHER" & vbCrLf
        testo = testo & Space$(16) & "EXEC CICS READ" & vbCrLf
        testo = testo & Space$(21) & "FILE ( WK-DATASET )" & vbCrLf
        testo = testo & Space$(21) & "INTO ( ADL-" & TabIstr.BlkIstr(1).Tavola & " )" & vbCrLf
        testo = testo & Space$(21) & "RIDFLD ( WK-KEYVALUE )" & vbCrLf
        testo = testo & Space$(21) & "GTEQ" & vbCrLf
        testo = testo & Space$(21) & "RESP ( WK-RESP )" & vbCrLf
        testo = testo & Space$(16) & "END-EXEC " & vbCrLf
        testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
        testo = testo & Space$(11) & "IF WK-RESP = 15 " & vbCrLf
        testo = testo & Space$(13) & "MOVE ZERO TO WK-RESP" & vbCrLf
        testo = testo & Space$(11) & "END-IF" & vbCrLf
                   
        testo = testo & Space$(11) & "IF WK-RESP = 0" & vbCrLf
        testo = testo & Space$(13) & "PERFORM " & TabIstr.BlkIstr(1).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(1).Segment & "-TO-WK" & vbCrLf
        testo = testo & Space$(13) & "MOVE WK-" & TabIstr.BlkIstr(1).Segment & " TO LNKDB-AREA-DATI(1)" & vbCrLf
        testo = testo & Space$(13) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space$(13) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space$(13) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
        testo = testo & Space$(13) & "MOVE ZERO TO WK-RETRESP" & vbCrLf
        testo = testo & Space$(11) & "ELSE" & vbCrLf
        testo = testo & Space$(13) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
        testo = testo & Space$(11) & "END-IF." & vbCrLf
   Else
        MsgBox "Batch: gu2k"
   End If
   AggRtVsmGu2K = testo
   
End Function


Function AggRtVsmDlet() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
   If Trim(TabIstr.BlkIstr(1).Segment) = "" Then
      AggRtVsmDlet = AggRtVsmDletNQ
      Exit Function
   End If
   
   testo = InitIstr("DLET", "N")
   
   For K = 1 To UBound(TabIstr.BlkIstr)
      testo = testo & PuntaGuida(K, " ")
        If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & DeleteVsm(K, "G")
           testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(13) & "PERFORM DELETE-GUIDA THRU EX-DELETE-GUIDA" & vbCrLf
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(13) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
         Else
           testo = testo & Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
         End If
       testo = testo & Space$(6) & "*" & vbCrLf
           
   Next K
     

   AggRtVsmDlet = testo
   
End Function
Function AggRtVsmRepl() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
      
   If Trim(TabIstr.BlkIstr(1).Segment) = "" Then
      AggRtVsmRepl = AggRtVsmReplNQ
      Exit Function
   End If
   
   testo = InitIstr("REPL", "S")

   
   For K = 1 To UBound(TabIstr.BlkIstr)
       testo = testo & PuntaGuida(K, " ")
      
      If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & LeggiVsm(K, "H")
           testo = testo & UpdateVsm(K, " ")
           testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(14) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
           testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf

       Else
           testo = testo & Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
           
       End If
       testo = testo & Space$(6) & "*" & vbCrLf

   Next K
      

   AggRtVsmRepl = testo
   
End Function

Function AggRtVsmGu() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim TbT As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
   If Trim(TabIstr.BlkIstr(1).Segment) = "" Then
      AggRtVsmGu = AggRtVsmGuNQ
      Exit Function
   End If
   
   testo = InitIstr("GU", "S")

   If TabIstr.BlkIstr(1).Parentesi = "(" Then
      Set Tb2 = DbPrj.OpenRecordset("select * from dmindex where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and nomekey like '" & TabIstr.BlkIstr(1).key & "*'")
      If Tb2.RecordCount > 0 Then
         testo = testo & AggRtVsmGu2K
         AggRtVsmGu = testo
         Exit Function
      End If
 '     Set Tb2 = DbPrj.OpenRecordset("select * from field where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and xname = '" & TabIstr.BlkIstr(1).key & "'")
      Set Tb2 = DbPrj.OpenRecordset("select * from field where xname = '" & TabIstr.BlkIstr(1).key & "'")
      If Tb2.RecordCount > 0 Then
         If Tb2!Pointer = "INDX" Then
            testo = testo & Space$(11) & "  Attenzione al Nome Key 2 e NomeSEg" & vbCrLf
            testo = testo & AggRtVsmGu2K
            AggRtVsmGu = testo
            Exit Function
         End If
      End If
   End If

   For K = 1 To UBound(TabIstr.BlkIstr)
      testo = testo & PuntaGuida(K, " ")
      
      If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & LeggiVsm(K, " ")

           testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(14) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
           testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
       Else
           testo = testo & Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
       End If
       testo = testo & Space$(6) & "*" & vbCrLf
           
   Next K
     

   AggRtVsmGu = testo
   
End Function
Function MoveExGn(Ind) As String

   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
   testo = Space$(13) & "MOVE LNKDB-STKFDBKEY(LNKDB-NUMSTACK) TO " & vbCrLf
   testo = testo & Space$(17) & "WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " " & vbCrLf
     
     OldSegm = -1
     Set Tb2 = DbPrj.OpenRecordset("select * from DMColonne where Pkey > 0 and idsegm = " & Trim(TabIstr.BlkIstr(Ind).IdSegm) & " order by ordinale")
     If Tb2.RecordCount > 0 Then
        Tb2.MoveFirst
        While Not Tb2.EOF
          If OldSegm <> Tb2!IdSegmProv Then
            OldSegm = Tb2!IdSegmProv
            If OldSegm <> TabIstr.BlkIstr(Ind).IdSegm Then
               Set Tb3 = DbPrj.OpenRecordset("select * from dmtavole where idsegm = " & OldSegm)
               testo = testo & Space$(13) & "MOVE WKEYP-" & Tb3!Nome & " OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
               testo = testo & Space$(19) & "TO WKEYP-" & Tb3!Nome & " OF WKEY-" & Tb3!Nome & vbCrLf
               Tb3.Close
            End If
          End If
          Tb2.MoveNext
        Wend
     End If
     
     Tb2.Close
     MoveExGn = testo

End Function
Function ContrExGn(Ind) As String

   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   Dim SwIf As Boolean
   SwIf = False
   
     testo = ""
     OldSegm = -1
     Set Tb2 = DbPrj.OpenRecordset("select * from DMColonne where Pkey > 0 and idsegm = " & Trim(TabIstr.BlkIstr(Ind).IdSegm) & " order by ordinale")
     
     If Tb2.RecordCount > 0 Then
        Tb2.MoveFirst
        While Not Tb2.EOF
          If OldSegm <> Tb2!IdSegmProv Then
            OldSegm = Tb2!IdSegmProv
            If OldSegm <> TabIstr.BlkIstr(Ind).IdSegm Then
               Set Tb3 = DbPrj.OpenRecordset("select * from dmtavole where idsegm = " & OldSegm)
               If SwIf Then
                  testo = testo & Space$(13) & "OR "
               Else
                  SwIf = True
                  testo = testo & Space$(11) & "IF "
               End If
               testo = testo & "WKEYP-" & Tb3!Nome & " OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " NOT EQUAL " & vbCrLf
               testo = testo & Space$(19) & "WKEYP-" & Tb3!Nome & " OF WKEY-" & Tb3!Nome & vbCrLf
               Tb3.Close
            End If
          End If
          Tb2.MoveNext
        Wend
        If SwIf Then
           testo = testo & Space$(13) & "MOVE " & StringaTpBt("204") & " TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(13) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "END-IF" & vbCrLf
        Else
          Ind = Ind
        End If
     Else
       Ind = Ind
     End If
     
     Tb2.Close
     
     ContrExGn = testo

End Function
Function InitIstr(wIstr, wQual) As String
   Dim testo As String
   
   testo = Space$(11) & "MOVE '" & wIstr & "' TO WK-ISTR." & vbCrLf
   
   testo = testo & Space$(11) & "MOVE ZERO TO WK-IND" & vbCrLf
   testo = testo & Space$(11) & "MOVE 15   TO WK-NUMSTACK" & vbCrLf
   testo = testo & Space$(11) & "PERFORM UNTIL WK-IND > 15 " & vbCrLf
   testo = testo & Space$(13) & "IF LNKDB-NUMPCB = LNKDB-STKNUMPCB(WK-IND) OR " & vbCrLf
   testo = testo & Space$(15) & "LNKDB-STKNUMPCB(WK-IND) = SPACES THEN" & vbCrLf
   testo = testo & Space$(17) & "MOVE WK-IND TO WK-NUMSTACK" & vbCrLf
   testo = testo & Space$(17) & "MOVE 30 TO WK-IND" & vbCrLf
   testo = testo & Space$(13) & "END-IF" & vbCrLf
   testo = testo & Space$(11) & "END-PERFORM." & vbCrLf
   
   testo = testo & Space$(6) & "*" & vbCrLf
   
   InitIstr = testo

End Function
Function PuntaGuida(Ind, Tipo) As String

   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
   If Trim(Tipo) <> "" Then
     OldSegm = -1
     Set Tb2 = DbPrj.OpenRecordset("select * from DMColonne where Pkey > 0 and idsegm = " & Trim(TabIstr.BlkIstr(Ind).IdSegm) & " order by ordinale")
     If Tb2.RecordCount > 0 Then
        Tb2.MoveFirst
        While Not Tb2.EOF
          If OldSegm <> Tb2!IdSegmProv Then
            OldSegm = Tb2!IdSegmProv
            If OldSegm <> TabIstr.BlkIstr(Ind).IdSegm Then
               Set Tb3 = DbPrj.OpenRecordset("select * from dmtavole where idsegm = " & OldSegm)
               testo = testo & Space$(13) & "MOVE WKEYP-" & Tb3!Nome & " OF WKEY-" & Tb3!Nome & vbCrLf
               testo = testo & Space$(19) & "TO WKEYP-" & Tb3!Nome & " OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
               Tb3.Close
            End If
          End If
          Tb2.MoveNext
        Wend
        Tb2.MoveFirst
        While Not Tb2.EOF
          If Tb2!IdSegmProv <> TabIstr.BlkIstr(Ind).IdSegm Then
            Set Tb3 = DbPrj.OpenRecordset("select * from dmtavole where idsegm = " & OldSegm)
            testo = testo & Space$(13) & "MOVE " & Tb2!Colonna & " OF WKEY-" & Tb3!Nome & vbCrLf
            testo = testo & Space$(19) & "TO " & Tb2!Colonna & " OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
            Tb3.Close
          End If
          Tb2.MoveNext
        Wend
     End If
     Tb2.Close
     PuntaGuida = testo
     Exit Function
   End If
   
   If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
     testo = Space$(13) & "MOVE '" & TabIstr.BlkIstr(Ind).Tavola & "' TO WK-DATASET" & vbCrLf
     testo = testo & Space$(13) & "MOVE 'S' TO WK-PUNTUALE " & vbCrLf
     
     If Trim(TabIstr.BlkIstr(Ind).Parentesi) = "(" Then
        testo = testo & Space$(13) & "MOVE LNKDB-KEYVALUE(" & Format(Ind, "#0") & ") TO WK-KEYVALUE" & vbCrLf
     Else
        testo = testo & Space$(13) & "MOVE LNKDB-STKFDBKEY(LNKDB-NUMSTACK) TO WK-KEYVALUE" & vbCrLf
     End If
   
     testo = testo & Space$(13) & "MOVE WK-KEYVALUE TO" & vbCrLf & Space$(16) & "WKEYP-" & TabIstr.BlkIstr(Ind).Tavola & " OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
     testo = testo & Space$(13) & "MOVE LENGTH OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYLENGTH" & vbCrLf
     OldSegm = -1
     Set Tb2 = DbPrj.OpenRecordset("select * from DMColonne where Pkey > 0 and idsegm = " & Trim(TabIstr.BlkIstr(Ind).IdSegm) & " order by ordinale")
     If Tb2.RecordCount > 0 Then
        Tb2.MoveFirst
        While Not Tb2.EOF
          If OldSegm <> Tb2!IdSegmProv Then
            OldSegm = Tb2!IdSegmProv
            If OldSegm <> TabIstr.BlkIstr(Ind).IdSegm Then
               Set Tb3 = DbPrj.OpenRecordset("select * from dmtavole where idsegm = " & OldSegm)
               testo = testo & Space$(13) & "MOVE WKEYP-" & Tb3!Nome & " OF WKEY-" & Tb3!Nome & vbCrLf
               testo = testo & Space$(19) & "TO WKEYP-" & Tb3!Nome & " OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
               Tb3.Close
            End If
          End If
          Tb2.MoveNext
        Wend
        Tb2.MoveFirst
        While Not Tb2.EOF
          If Tb2!IdSegmProv <> TabIstr.BlkIstr(Ind).IdSegm Then
            Set Tb3 = DbPrj.OpenRecordset("select * from dmtavole where idsegm = " & OldSegm)
            testo = testo & Space$(13) & "MOVE " & Tb2!Colonna & " OF WKEY-" & Tb3!Nome & vbCrLf
            testo = testo & Space$(19) & "TO " & Tb2!Colonna & " OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
            Tb3.Close
          End If
          Tb2.MoveNext
        Wend
     End If
     Tb2.Close
     If Trim(TabIstr.BlkIstr(Ind).Parentesi) = "(" Then
        testo = testo & Space$(13) & "MOVE LNKDB-SSA-OPER(" & Format(Ind, "#0") & ")"
        testo = testo & " TO WK-OPERATORE" & vbCrLf
     Else
        testo = testo & Space$(13) & "MOVE 'GTEQ' TO WK-OPERATORE" & vbCrLf
     End If
     testo = testo & Space$(13) & "PERFORM LEGGI-GUIDA THRU EX-LEGGI-GUIDA" & vbCrLf
   Else
    MsgBox "PuntaGuida: Tavola non presente !"
   End If
   
   PuntaGuida = testo

End Function
Function CreaKeyConc(wIdSegm, wTavola) As String

   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
     
     testo = ""
     OldSegm = -1
     Set Tb2 = DbPrj.OpenRecordset("select * from DMColonne where Pkey > 0 and idsegm = " & wIdSegm & " order by ordinale")
     If Tb2.RecordCount > 0 Then
        Tb2.MoveFirst
        While Not Tb2.EOF
          If Tb2!IdSegmProv <> wIdSegm Then
'            Set Tb3 = DbPrj.OpenRecordset("select * from dmtavole where idsegm = " & OldSegm)
            testo = testo & Space$(13) & "MOVE " & Tb2!Colonna & " OF WKEY-" & wTavola & vbCrLf
            testo = testo & Space$(19) & "TO " & Tb2!Colonna & " OF ADL-" & wTavola & vbCrLf
'            Tb3.Close
          End If
          Tb2.MoveNext
        Wend
     End If
     Tb2.Close
     CreaKeyConc = testo
   
End Function

Function AggRtVsmGhu() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
   If Trim(TabIstr.BlkIstr(1).Segment) = "" Then
      AggRtVsmGhu = AggRtVsmGhuNQ
      Exit Function
   End If
   
   testo = InitIstr("GHU", "S")
              
   If TabIstr.BlkIstr(1).Parentesi = "(" Then

      Set Tb2 = DbPrj.OpenRecordset("select * from dmindex where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and nomekey like '" & TabIstr.BlkIstr(1).key & "*'")
      If Tb2.RecordCount > 0 Then
         testo = testo & AggRtVsmGhu2K
         AggRtVsmGhu = testo
         Exit Function
      End If
      Set Tb2 = DbPrj.OpenRecordset("select * from field where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and xname = '" & TabIstr.BlkIstr(1).key & "'")
      If Tb2.RecordCount > 0 Then
         If Tb2!Pointer = "INDX" Then
            testo = testo & Space$(11) & "  Attenzione al Nome Key 2 " & vbCrLf
            testo = testo & AggRtVsmGhu2K
            AggRtVsmGhu = testo
            Exit Function
         End If
      End If

   End If
   
   For K = 1 To UBound(TabIstr.BlkIstr)
      testo = testo & PuntaGuida(K, " ")
      
      If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & LeggiVsm(K, "H")
           testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(14) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
           testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
       Else
           testo = testo & Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
       End If
       testo = testo & Space$(6) & "*" & vbCrLf
   Next K

   AggRtVsmGhu = testo
   
End Function
Function DeleteVsm(Ind, Tipo) As String

Dim Tb1 As Recordset
Dim Tb2 As Recordset
Dim testo As String

 testo = Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
 testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
 testo = testo & Space$(11) & "END-IF" & vbCrLf
  
 If TipoRoutine(NomeRoutine) <> "T" Then
   DeleteVsm = testo & DeleteVsmBt(Ind, Tipo)
   Exit Function
 End If
 
 testo = testo & Space$(6) & "* ROUTINE DI CANCELLAZIONE VSAM GENERIC" & vbCrLf
 
 If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
    testo = testo & Space$(11) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & TabIstr.BlkIstr(Ind).Segment & vbCrLf
    testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL" & vbCrLf
    testo = testo & CreaKeyConc(TabIstr.BlkIstr(Ind).IdSegm, TabIstr.BlkIstr(Ind).Tavola)
    testo = testo & Space$(11) & "PERFORM REST-DBGKEY THRU EX-REST-DBGKEY" & vbCrLf
    testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-AREALENGTH" & vbCrLf
    testo = testo & Space$(11) & "MOVE LENGTH OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYLENGTH" & vbCrLf
    testo = testo & Space$(11) & "MOVE WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYVALUE" & vbCrLf
    testo = testo & Space$(11) & "EXEC CICS DELETE " & vbCrLf
    testo = testo & Space$(15) & "FILE ( WK-DATASET ) " & vbCrLf
    testo = testo & Space$(15) & "RIDFLD ( WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(15) & "KEYLENGTH ( WK-KEYLENGTH )" & vbCrLf
    testo = testo & Space$(18) & "RESP ( WK-RESP )" & vbCrLf
    testo = testo & Space$(11) & "END-EXEC. " & vbCrLf
 Else
    Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
    wIdOggetto = Tb1!IdOggetto
    Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

    Tb1.MoveFirst
    testo = testo & Space$(11) & "EVALUATE WK-DATASET" & vbCrLf
    If Ind = 0 Then Ind = 1
    While Not Tb1.EOF
        testo = testo & Space$(13) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
        Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
        testo = testo & Space$(15) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & Tb2!Nome & vbCrLf
        testo = testo & Space$(15) & "PERFORM " & Tb2!Nome & "-TO-ADL THRU EX-" & Tb2!Nome & "-TO-ADL" & vbCrLf
        testo = testo & CreaKeyConc(Tb1!IdSegm, Tb1!Nome)
        testo = testo & Space$(15) & "PERFORM REST-DBGKEY THRU EX-REST-DBGKEY" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF ADL-" & Tb1!Nome & " TO WK-AREALENGTH" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF WKEY-" & Tb1!Nome & " TO WK-KEYLENGTH" & vbCrLf
        testo = testo & Space$(15) & "MOVE WKEY-" & Tb1!Nome & " TO WK-KEYVALUE" & vbCrLf
        testo = testo & Space$(15) & "EXEC CICS DELETE " & vbCrLf
        testo = testo & Space$(25) & "FILE ( WK-DATASET ) " & vbCrLf
        testo = testo & Space$(25) & "RIDFLD ( WKEY-" & Tb1!Nome & " )" & vbCrLf
        testo = testo & Space$(25) & "KEYLENGTH ( WK-KEYLENGTH )" & vbCrLf
'        testo = testo & Space$(25) & "GENERIC" & vbCrLf
        testo = testo & Space$(25) & "RESP ( WK-RESP )" & vbCrLf
        testo = testo & Space$(15) & "END-EXEC " & vbCrLf
      Tb1.MoveNext
    Wend
    testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
 End If

 DeleteVsm = testo
 
End Function
Function DeleteVsmBt(Ind, Tipo) As String

Dim Tb1 As Recordset
Dim Tb2 As Recordset
Dim testo As String

 
 testo = Space$(6) & "* ROUTINE DI CANCELLAZIONE VSAM " & vbCrLf
 
 
 If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
    testo = testo & Space$(11) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & TabIstr.BlkIstr(Ind).Segment & vbCrLf
    testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL" & vbCrLf
    testo = testo & CreaKeyConc(TabIstr.BlkIstr(Ind).IdSegm, TabIstr.BlkIstr(Ind).Tavola)
    testo = testo & Space$(11) & "PERFORM REST-DBGKEY THRU EX-REST-DBGKEY" & vbCrLf
    testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-AREALENGTH" & vbCrLf
    testo = testo & Space$(11) & "MOVE LENGTH OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYLENGTH" & vbCrLf
    testo = testo & Space$(11) & "MOVE WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYVALUE" & vbCrLf
    testo = testo & Space$(11) & "MOVE WK-KEYVALUE TO " & TabIstr.BlkIstr(Ind).Tavola & "-KEY" & vbCrLf
    testo = testo & Space$(11) & "INSPECT WK-KEYVALUE REPLACING " & vbCrLf
    testo = testo & Space$(15) & "ALL LOW-VALUE BY HIGH-VALUE" & vbCrLf
    testo = testo & Space$(11) & "START " & TabIstr.BlkIstr(Ind).Tavola & " KEY NOT LESS " & TabIstr.BlkIstr(Ind).Tavola & "-KEY" & vbCrLf
    testo = testo & Space$(11) & "END-START" & vbCrLf
    testo = testo & Space$(11) & "IF WK-RESP = '00' " & vbCrLf
    testo = testo & Space$(13) & "PERFORM UNTIL " & TabIstr.BlkIstr(Ind).Tavola & "-KEY > WK-KEYVALUE" & vbCrLf
    testo = testo & Space$(15) & "READ " & TabIstr.BlkIstr(Ind).Tavola & " NEXT RECORD " & vbCrLf
    testo = testo & Space$(18) & "INTO ADL-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(15) & "END-READ" & vbCrLf
    testo = testo & Space$(15) & "IF WK-RESP = '00' AND" & vbCrLf
    testo = testo & Space$(17) & TabIstr.BlkIstr(Ind).Tavola & "-KEY NOT GREATER WK-KEYVALUE" & vbCrLf
    testo = testo & Space$(18) & "DELETE " & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(18) & "END-DELETE" & vbCrLf
    testo = testo & Space$(15) & "END-IF" & vbCrLf
    testo = testo & Space$(13) & "END-PERFORM" & vbCrLf
    testo = testo & Space$(11) & "END-IF" & vbCrLf
 Else
    Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
    wIdOggetto = Tb1!IdOggetto
    Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

    Tb1.MoveFirst
    testo = testo & Space$(11) & "EVALUATE WK-DATASET" & vbCrLf
    If Ind = 0 Then Ind = 1
    While Not Tb1.EOF
        testo = testo & Space$(13) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
        Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
        testo = testo & Space$(15) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & Tb2!Nome & vbCrLf
        testo = testo & Space$(15) & "PERFORM " & Tb2!Nome & "-TO-ADL THRU EX-" & Tb2!Nome & "-TO-ADL" & vbCrLf
        testo = testo & CreaKeyConc(Tb1!IdSegm, Tb1!Nome)
        testo = testo & Space$(15) & "PERFORM REST-DBGKEY THRU EX-REST-DBGKEY" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF ADL-" & Tb1!Nome & " TO WK-AREALENGTH" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF WKEY-" & Tb1!Nome & " TO WK-KEYLENGTH" & vbCrLf
        testo = testo & Space$(15) & "MOVE WKEY-" & Tb1!Nome & " TO WK-KEYVALUE" & vbCrLf
      
    testo = testo & Space$(11) & "MOVE WK-KEYVALUE TO " & Tb1!Nome & "-KEY" & vbCrLf
    testo = testo & Space$(11) & "INSPECT WK-KEYVALUE REPLACING " & vbCrLf
    testo = testo & Space$(15) & "ALL LOW-VALUE BY HIGH-VALUE" & vbCrLf
    testo = testo & Space$(11) & "START " & Tb1!Nome & " KEY NOT LESS " & Tb1!Nome & "-KEY" & vbCrLf
    testo = testo & Space$(11) & "END-START" & vbCrLf
    testo = testo & Space$(11) & "IF WK-RESP = '00' " & vbCrLf
    testo = testo & Space$(13) & "PERFORM UNTIL " & Tb1!Nome & "-KEY > WK-KEYVALUE" & vbCrLf
    testo = testo & Space$(15) & "READ " & Tb1!Nome & " NEXT RECORD " & vbCrLf
    testo = testo & Space$(18) & "INTO ADL-" & Tb1!Nome & vbCrLf
    testo = testo & Space$(15) & "END-READ" & vbCrLf
    testo = testo & Space$(15) & "IF WK-RESP = '00' AND" & vbCrLf
    testo = testo & Space$(17) & Tb1!Nome & "-KEY NOT GREATER WK-KEYVALUE" & vbCrLf
    testo = testo & Space$(18) & "DELETE " & Tb1!Nome & vbCrLf
    testo = testo & Space$(18) & "END-DELETE" & vbCrLf
    testo = testo & Space$(15) & "END-IF" & vbCrLf
    testo = testo & Space$(13) & "END-PERFORM" & vbCrLf
    testo = testo & Space$(11) & "END-IF" & vbCrLf
      
      
      Tb1.MoveNext
    Wend
    testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
 End If

 DeleteVsmBt = testo
 
End Function

Function ScriviVsm(Ind, Tipo) As String

Dim testo As String
Dim Tb1 As Recordset
Dim Tb2 As Recordset

 
 testo = testo & Space$(6) & "*" & vbCrLf
 If TipoRoutine(NomeRoutine) <> "T" Then
    ScriviVsm = testo & ScriviVsmBt(Ind, Tipo)
    Exit Function
 End If
 
 If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
    testo = testo & Space$(11) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & TabIstr.BlkIstr(Ind).Segment & vbCrLf
    testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL" & vbCrLf
    testo = testo & PuntaGuida(Ind, Tipo)

    testo = testo & Space$(11) & "PERFORM SET-WKKEY THRU EX-SET-WKKEY" & vbCrLf
    testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-AREALENGTH" & vbCrLf
    testo = testo & Space$(11) & "MOVE LENGTH OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYLENGTH" & vbCrLf
    testo = testo & Space$(11) & "EXEC CICS WRITE " & vbCrLf
    testo = testo & Space$(15) & "FILE ( WK-DATASET ) " & vbCrLf
    testo = testo & Space$(18) & "FROM ( "
    testo = testo & "ADL-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(15) & "RIDFLD ( WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(15) & "LENGTH ( WK-AREALENGTH )" & vbCrLf
    testo = testo & Space$(18) & "RESP ( WK-RESP )" & vbCrLf
    testo = testo & Space$(11) & "END-EXEC. " & vbCrLf
    testo = testo & Space$(11) & "MOVE WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYVALUE" & vbCrLf
 Else
    Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
    wIdOggetto = Tb1!IdOggetto
    Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

    Tb1.MoveFirst
    testo = testo & Space$(11) & "EVALUATE WK-DATASET" & vbCrLf
    While Not Tb1.EOF
        If Ind = 0 Then Ind = 1
        testo = testo & Space$(13) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
        Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
        testo = testo & Space$(15) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & Tb2!Nome & vbCrLf
        testo = testo & Space$(15) & "PERFORM " & Tb2!Nome & "-TO-ADL THRU EX-" & Tb2!Nome & "-TO-ADL" & vbCrLf
        testo = testo & Space$(11) & "PERFORM REST-DBGKEY THRU EX-REST-DBGKEY" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF ADL-" & Tb1!Nome & " TO WK-AREALENGTH" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF WKEY-" & Tb1!Nome & " TO WK-KEYLENGTH" & vbCrLf
        testo = testo & Space$(15) & "EXEC CICS WRITE " & vbCrLf
        testo = testo & Space$(25) & "FILE ( WK-DATASET ) " & vbCrLf
        testo = testo & Space$(25) & "FROM ( "
        testo = testo & "ADL-" & Tb1!Nome & " )" & vbCrLf
        testo = testo & Space$(15) & "RIDFLD ( WKEY-" & Tb1!Nome & " )" & vbCrLf
        testo = testo & Space$(15) & "LENGTH ( WK-AREALENGTH )" & vbCrLf
        testo = testo & Space$(15) & "KEYLENGTH ( WK-KEYLENGTH )" & vbCrLf
        testo = testo & Space$(25) & "RESP ( WK-RESP )" & vbCrLf
        testo = testo & Space$(15) & "END-EXEC " & vbCrLf
        testo = testo & Space$(11) & "MOVE WKEY-" & Tb1!Nome & " TO WK-KEYVALUE" & vbCrLf
      Tb1.MoveNext
    Wend
    testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
 End If
 ScriviVsm = testo
 
End Function
Function ScriviVsmBt(Ind, Tipo) As String

Dim testo As String
Dim Tb1 As Recordset
Dim Tb2 As Recordset

 
 testo = ""
 
 If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
    testo = testo & Space$(11) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & TabIstr.BlkIstr(Ind).Segment & vbCrLf
    testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL" & vbCrLf
    testo = testo & PuntaGuida(Ind, Tipo)

    testo = testo & Space$(11) & "PERFORM SET-WKKEY THRU EX-SET-WKKEY" & vbCrLf
    testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-AREALENGTH" & vbCrLf
    testo = testo & Space$(11) & "MOVE LENGTH OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYLENGTH" & vbCrLf
    testo = testo & Space$(11) & "WRITE REC-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(15) & "FROM ADL-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(11) & "END-WRITE " & vbCrLf
    testo = testo & Space$(11) & "MOVE WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYVALUE" & vbCrLf
 Else
    Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
    wIdOggetto = Tb1!IdOggetto
    Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

    Tb1.MoveFirst
    testo = testo & Space$(11) & "EVALUATE WK-DATASET" & vbCrLf
    While Not Tb1.EOF
        If Ind = 0 Then Ind = 1
        testo = testo & Space$(13) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
        Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
        testo = testo & Space$(15) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & Tb2!Nome & vbCrLf
        testo = testo & Space$(15) & "PERFORM " & Tb2!Nome & "-TO-ADL THRU EX-" & Tb2!Nome & "-TO-ADL" & vbCrLf
        testo = testo & Space$(11) & "PERFORM REST-DBGKEY THRU EX-REST-DBGKEY" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF ADL-" & Tb1!Nome & " TO WK-AREALENGTH" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF WKEY-" & Tb1!Nome & " TO WK-KEYLENGTH" & vbCrLf
        testo = testo & Space$(15) & "WRITE REC-" & Tb1!Nome & vbCrLf
        testo = testo & Space$(25) & "FROM ADL-" & Tb1!Nome & vbCrLf
        testo = testo & Space$(15) & "END-WRITE " & vbCrLf
        testo = testo & Space$(11) & "MOVE WKEY-" & Tb1!Nome & " TO WK-KEYVALUE" & vbCrLf
      Tb1.MoveNext
    Wend
    testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
 End If
 ScriviVsmBt = testo
 
End Function

Function UpdateVsm(Ind, Tipo) As String

Dim testo As String
Dim Tb1 As Recordset
Dim Tb2 As Recordset
Dim wIdOggetto As Variant

 testo = Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
 testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
 testo = testo & Space$(11) & "END-IF" & vbCrLf

 
 testo = testo & Space$(11) & "PERFORM SET-FBKEY THRU EX-SET-FBKEY" & vbCrLf
 
 If TipoRoutine(NomeRoutine) <> "T" Then
    UpdateVsm = testo & UpdateVsmBt(Ind, Tipo)
    Exit Function
 End If
 
 If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
    testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-AREALENGTH" & vbCrLf
    testo = testo & Space$(11) & "EXEC CICS READ" & vbCrLf
    testo = testo & Space$(15) & "FILE ( WK-DATASET ) " & vbCrLf
    testo = testo & Space$(15) & "LENGTH ( WK-AREALENGTH ) " & vbCrLf
    testo = testo & Space$(15) & "RIDFLD ( WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " ) " & vbCrLf
    testo = testo & Space$(18) & "INTO ( "
    testo = testo & "ADL-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(18) & "RESP ( WK-RESP )" & vbCrLf
    testo = testo & Space$(18) & "EQUAL" & vbCrLf
    testo = testo & Space$(18) & "UPDATE" & vbCrLf
    testo = testo & Space$(11) & "END-EXEC" & vbCrLf
    
    testo = testo & Space$(11) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & TabIstr.BlkIstr(Ind).Segment & vbCrLf
    testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL" & vbCrLf
    testo = testo & CreaKeyConc(TabIstr.BlkIstr(Ind).IdSegm, TabIstr.BlkIstr(Ind).Tavola)
    testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-AREALENGTH" & vbCrLf
    testo = testo & Space$(11) & "EXEC CICS REWRITE " & vbCrLf
    testo = testo & Space$(15) & "FILE ( WK-DATASET ) " & vbCrLf
    testo = testo & Space$(18) & "FROM ( "
    testo = testo & "ADL-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(18) & "RESP ( WK-RESP )" & vbCrLf
    testo = testo & Space$(11) & "END-EXEC. " & vbCrLf
 Else
    Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
    wIdOggetto = Tb1!IdOggetto
    Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

    Tb1.MoveFirst
    testo = testo & Space$(11) & "EVALUATE WK-DATASET" & vbCrLf
    While Not Tb1.EOF
        If Ind = 0 Then Ind = 1
        testo = testo & Space$(13) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
        Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
        testo = testo & Space$(15) & "MOVE LENGTH OF ADL-" & Tb1!Nome & " TO WK-AREALENGTH" & vbCrLf
    
        testo = testo & Space$(11) & "EXEC CICS READ" & vbCrLf
        testo = testo & Space$(15) & "FILE ( WK-DATASET ) " & vbCrLf
        testo = testo & Space$(15) & "LENGTH ( WK-AREALENGTH ) " & vbCrLf
        testo = testo & Space$(15) & "RIDFLD ( WKEY-" & Tb1!Nome & " ) " & vbCrLf
        testo = testo & Space$(18) & "INTO ( "
        testo = testo & "ADL-" & Tb1!Nome & " )" & vbCrLf
        testo = testo & Space$(18) & "RESP ( WK-RESP )" & vbCrLf
        testo = testo & Space$(18) & "EQUAL" & vbCrLf
        testo = testo & Space$(18) & "UPDATE" & vbCrLf
        testo = testo & Space$(11) & "END-EXEC" & vbCrLf
        
        testo = testo & Space$(15) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & Tb2!Nome & vbCrLf
        testo = testo & Space$(15) & "PERFORM " & Tb2!Nome & "-TO-ADL THRU EX-" & Tb2!Nome & "-TO-ADL" & vbCrLf
        testo = testo & CreaKeyConc(Tb1!IdSegm, Tb1!Nome)
        testo = testo & Space$(15) & "EXEC CICS REWRITE " & vbCrLf
        testo = testo & Space$(25) & "FILE ( WK-DATASET ) " & vbCrLf
        testo = testo & Space$(25) & "FROM ( "
        testo = testo & "ADL-" & Tb1!Nome & " )" & vbCrLf
        testo = testo & Space$(25) & "RESP ( WK-RESP )" & vbCrLf
        testo = testo & Space$(15) & "END-EXEC " & vbCrLf
      Tb1.MoveNext
    Wend
    testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
 
 End If

 UpdateVsm = testo
 
End Function
Function UpdateVsmBt(Ind, Tipo) As String

Dim testo As String
Dim Tb1 As Recordset
Dim Tb2 As Recordset
Dim wIdOggetto As Variant

 
testo = ""

 If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
    testo = testo & Space$(11) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & TabIstr.BlkIstr(Ind).Segment & vbCrLf
    testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL" & vbCrLf
    testo = testo & CreaKeyConc(TabIstr.BlkIstr(Ind).IdSegm, TabIstr.BlkIstr(Ind).Tavola)
    testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-AREALENGTH" & vbCrLf
    testo = testo & Space$(11) & "REWRITE REC-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(18) & "FROM ADL-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(11) & "END-REWRITE " & vbCrLf
 Else
    Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
    wIdOggetto = Tb1!IdOggetto
    Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

    Tb1.MoveFirst
    testo = testo & Space$(11) & "EVALUATE WK-DATASET" & vbCrLf
    While Not Tb1.EOF
        If Ind = 0 Then Ind = 1
        testo = testo & Space$(13) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
        Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
        testo = testo & Space$(15) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & Tb2!Nome & vbCrLf
        testo = testo & Space$(15) & "PERFORM " & Tb2!Nome & "-TO-ADL THRU EX-" & Tb2!Nome & "-TO-ADL" & vbCrLf
        testo = testo & CreaKeyConc(Tb1!IdSegm, Tb1!Nome)
        testo = testo & Space$(15) & "MOVE LENGTH OF ADL-" & Tb1!Nome & " TO WK-AREALENGTH" & vbCrLf
        testo = testo & Space$(15) & "REWRITE REC-" & Tb1!Nome & vbCrLf
        testo = testo & Space$(25) & "FROM ADL-" & Tb1!Nome & vbCrLf
        testo = testo & Space$(15) & "END-REWRITE " & vbCrLf
      Tb1.MoveNext
    Wend
    testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
 
 End If

 UpdateVsmBt = testo
 
End Function


Function LeggiVsmGn(Ind, Tipo) As String

Dim testo As String
Dim Tb1 As Recordset
Dim Tb2 As Recordset
Dim wIdOggetto As Variant

 testo = Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
 testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
 testo = testo & Space$(11) & "END-IF" & vbCrLf

 testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-AREALENGTH" & vbCrLf
 testo = testo & Space$(11) & "MOVE LENGTH OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYLENGTH" & vbCrLf

 testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
 testo = testo & Space$(13) & "PERFORM SET-FBKEY THRU EX-SET-FBKEY" & vbCrLf
 
If TipoRoutine(NomeRoutine) = "T" Then
  If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
    
       testo = testo & Space$(13) & "EXEC CICS ENDBR" & vbCrLf
       testo = testo & Space$(20) & "FILE (  WK-DATASET ) " & vbCrLf
       testo = testo & Space$(20) & "RESP (WK-RESP )" & vbCrLf
       testo = testo & Space$(13) & "END-EXEC" & vbCrLf
    
    testo = testo & Space$(13) & "EXEC CICS STARTBR " & vbCrLf
    testo = testo & Space$(15) & "FILE ( WK-DATASET ) " & vbCrLf
    testo = testo & Space$(18) & "RIDFLD ( WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(18) & "KEYLENGTH ( WK-KEYLENGTH )" & vbCrLf
    testo = testo & Space$(18) & "GTEQ" & vbCrLf
    testo = testo & Space$(18) & "RESP ( WK-RESP )" & vbCrLf
    testo = testo & Space$(13) & "END-EXEC" & vbCrLf
    
    testo = testo & Space$(13) & "IF WK-RESP = 0 THEN" & vbCrLf
    testo = testo & Space$(15) & "EXEC CICS READNEXT " & vbCrLf
    testo = testo & Space$(18) & "FILE ( WK-DATASET ) " & vbCrLf
    testo = testo & Space$(18) & "INTO ( "
    testo = testo & "ADL-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(18) & "RIDFLD ( WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(18) & "KEYLENGTH ( WK-KEYLENGTH )" & vbCrLf
    testo = testo & Space$(18) & "RESP ( WK-RESP )" & vbCrLf
    testo = testo & Space$(15) & "END-EXEC" & vbCrLf
    testo = testo & Space$(13) & "END-IF" & vbCrLf
    
    testo = testo & Space$(11) & "ELSE " & vbCrLf
    
    testo = testo & MoveExGn(Ind)
    
    testo = testo & Space$(11) & "END-IF" & vbCrLf
    
    testo = testo & Space$(11) & "IF WK-RESP = 0 THEN" & vbCrLf
    testo = testo & Space$(13) & "EXEC CICS READNEXT " & vbCrLf
    testo = testo & Space$(15) & "FILE ( WK-DATASET ) " & vbCrLf
    testo = testo & Space$(18) & "INTO ( "
    testo = testo & "ADL-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(18) & "RIDFLD ( WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(18) & "KEYLENGTH ( WK-KEYLENGTH )" & vbCrLf
    testo = testo & Space$(18) & "RESP ( WK-RESP )" & vbCrLf
    testo = testo & Space$(13) & "END-EXEC" & vbCrLf
    testo = testo & Space$(11) & "END-IF" & vbCrLf
        
    testo = testo & Space$(11) & "MOVE WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYVALUE" & vbCrLf
     
  Else
     
    Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
    wIdOggetto = Tb1!IdOggetto
    Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

    Tb1.MoveFirst
    testo = testo & Space$(11) & "EVALUATE WK-DATASET" & vbCrLf
    While Not Tb1.EOF
        If Ind = 0 Then Ind = 1
        testo = testo & Space$(13) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
        Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
        testo = testo & Space$(15) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & Tb2!Nome & vbCrLf
        testo = testo & Space$(15) & "PERFORM " & Tb2!Nome & "-TO-ADL THRU EX-" & Tb2!Nome & "-TO-ADL" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF ADL-" & Tb1!Nome & " TO WK-AREALENGTH" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF WKEY-" & Tb1!Nome & " TO WK-KEYLENGTH" & vbCrLf
        testo = testo & Space$(15) & "EXEC CICS READ " & vbCrLf
        testo = testo & Space$(25) & "FILE ( WK-DATASET ) " & vbCrLf
        testo = testo & Space$(25) & "INTO ( "
        testo = testo & "ADL-" & Tb1!Nome & " )" & vbCrLf
        testo = testo & Space$(25) & "LENGTH ( WK-AREALENGTH )" & vbCrLf
        testo = testo & Space$(25) & "RIDFLD ( WKEY-" & Tb1!Nome & " )" & vbCrLf
        testo = testo & Space$(25) & "KEYLENGTH ( WK-KEYLENGTH )" & vbCrLf
        testo = testo & Space$(25) & "RESP ( WK-RESP )" & vbCrLf
        If Tipo = "H" Then
          testo = testo & Space$(25) & "UPDATE" & vbCrLf
        End If
        testo = testo & Space$(15) & "END-EXEC " & vbCrLf
        testo = testo & Space$(11) & "MOVE WKEY-" & Tb1!Nome & " TO WK-KEYVALUE" & vbCrLf
      Tb1.MoveNext
    Wend
    testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
 
  End If

Else

  If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
    testo = testo & Space$(11) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & TabIstr.BlkIstr(Ind).Segment & vbCrLf
    testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL" & vbCrLf
    testo = testo & Space$(11) & "MOVE ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO REC-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(11) & "START " & TabIstr.BlkIstr(Ind).Tavola & " KEY NOT LESS " & TabIstr.BlkIstr(Ind).Tavola & "-KEY" & vbCrLf
    testo = testo & Space$(15) & "END-START" & vbCrLf
    testo = testo & Space$(11) & "IF WK-RESP = '00' THEN" & vbCrLf
    testo = testo & Space$(13) & "READ " & TabIstr.BlkIstr(Ind).Tavola & " NEXT RECORD" & vbCrLf
    testo = testo & Space$(16) & "INTO ADL-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(13) & "END-READ" & vbCrLf
    testo = testo & Space$(11) & "END-IF" & vbCrLf
    testo = testo & Space$(11) & "ELSE " & vbCrLf
    
    testo = testo & MoveExGn(Ind)
    
    testo = testo & Space$(11) & "END-IF" & vbCrLf
    
    testo = testo & Space$(11) & "IF WK-RESP = '00' THEN" & vbCrLf
    testo = testo & Space$(13) & "READ " & TabIstr.BlkIstr(Ind).Tavola & " NEXT RECORD" & vbCrLf
    testo = testo & Space$(16) & "INTO ADL-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(13) & "END-READ" & vbCrLf
    testo = testo & Space$(11) & "END-IF" & vbCrLf
    
    testo = testo & Space$(11) & "MOVE " & TabIstr.BlkIstr(Ind).Tavola & "-KEY TO WK-KEYVALUE" & vbCrLf
  
  Else
    Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
    wIdOggetto = Tb1!IdOggetto
    Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

    Tb1.MoveFirst
    testo = testo & Space$(11) & "EVALUATE WK-DATASET" & vbCrLf
    While Not Tb1.EOF
        If Ind = 0 Then Ind = 1
        testo = testo & Space$(13) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
        Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
        testo = testo & Space$(15) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & Tb2!Nome & vbCrLf
        testo = testo & Space$(15) & "PERFORM " & Tb2!Nome & "-TO-ADL THRU EX-" & Tb2!Nome & "-TO-ADL" & vbCrLf
        testo = testo & Space$(15) & "MOVE ADL-" & Tb1!Nome & " TO REC-" & Tb1!Nome & vbCrLf
        testo = testo & Space$(11) & "START " & Tb1!Nome & " KEY EQUAL " & Tb1!Nome & "-KEY" & vbCrLf
        testo = testo & Space$(15) & "END-START" & vbCrLf
        testo = testo & Space$(11) & "IF WK-RESP = '00' THEN" & vbCrLf
        testo = testo & Space$(13) & "READ " & Tb1!Nome & " NEXT RECORD" & vbCrLf
        testo = testo & Space$(16) & "INTO ADL-" & Tb1!Nome & vbCrLf
        testo = testo & Space$(13) & "END-READ" & vbCrLf
        testo = testo & Space$(11) & "END-IF" & vbCrLf
        testo = testo & Space$(11) & "MOVE " & Tb1!Nome & "-KEY TO WK-KEYVALUE" & vbCrLf
      Tb1.MoveNext
    Wend
    testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
 
  End If
End If
LeggiVsmGn = testo
 
End Function
Function LeggiVsm(Ind, Tipo) As String

Dim testo As String
Dim Tb1 As Recordset
Dim Tb2 As Recordset
Dim wIdOggetto As Variant

 testo = Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
 testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
 testo = testo & Space$(11) & "END-IF" & vbCrLf

 
 testo = testo & Space$(11) & "PERFORM SET-FBKEY THRU EX-SET-FBKEY" & vbCrLf
 
If TipoRoutine(NomeRoutine) = "T" Then
  If Tipo = "H" Then
    testo = testo & Space$(6) & "* " & vbCrLf
    testo = testo & Space$(11) & "EXEC CICS UNLOCK" & vbCrLf
    testo = testo & Space$(15) & "FILE ( WK-DATASET ) " & vbCrLf
    testo = testo & Space$(18) & "RESP ( WK-RESP )" & vbCrLf
    testo = testo & Space$(11) & "END-EXEC. " & vbCrLf
    testo = testo & Space$(6) & "* " & vbCrLf
  End If
  If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
    testo = testo & Space$(11) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & TabIstr.BlkIstr(Ind).Segment & vbCrLf
    testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL" & vbCrLf
    testo = testo & Space$(11) & "MOVE LENGTH OF ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-AREALENGTH" & vbCrLf
    testo = testo & Space$(11) & "MOVE LENGTH OF WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYLENGTH" & vbCrLf
        
    testo = testo & Space$(11) & "EXEC CICS READ " & vbCrLf
    testo = testo & Space$(15) & "FILE ( WK-DATASET ) " & vbCrLf
    testo = testo & Space$(18) & "INTO ( "
    testo = testo & "ADL-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(18) & "LENGTH ( WK-AREALENGTH )" & vbCrLf
    testo = testo & Space$(18) & "RIDFLD ( WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " )" & vbCrLf
    testo = testo & Space$(18) & "KEYLENGTH ( WK-KEYLENGTH )" & vbCrLf
    testo = testo & Space$(18) & "RESP ( WK-RESP )" & vbCrLf
    If Tipo = "H" Then
      testo = testo & Space$(18) & "UPDATE" & vbCrLf
    End If
    testo = testo & Space$(11) & "END-EXEC. " & vbCrLf
    testo = testo & Space$(11) & "MOVE WKEY-" & TabIstr.BlkIstr(Ind).Tavola & " TO WK-KEYVALUE" & vbCrLf
  Else
    Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
    wIdOggetto = Tb1!IdOggetto
    Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

    Tb1.MoveFirst
    testo = testo & Space$(11) & "EVALUATE WK-DATASET" & vbCrLf
    While Not Tb1.EOF
        If Ind = 0 Then Ind = 1
        testo = testo & Space$(13) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
        Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
        testo = testo & Space$(15) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & Tb2!Nome & vbCrLf
        testo = testo & Space$(15) & "PERFORM " & Tb2!Nome & "-TO-ADL THRU EX-" & Tb2!Nome & "-TO-ADL" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF ADL-" & Tb1!Nome & " TO WK-AREALENGTH" & vbCrLf
        testo = testo & Space$(15) & "MOVE LENGTH OF WKEY-" & Tb1!Nome & " TO WK-KEYLENGTH" & vbCrLf
        testo = testo & Space$(15) & "EXEC CICS READ " & vbCrLf
        testo = testo & Space$(25) & "FILE ( WK-DATASET ) " & vbCrLf
        testo = testo & Space$(25) & "INTO ( "
        testo = testo & "ADL-" & Tb1!Nome & " )" & vbCrLf
        testo = testo & Space$(25) & "LENGTH ( WK-AREALENGTH )" & vbCrLf
        testo = testo & Space$(25) & "RIDFLD ( WKEY-" & Tb1!Nome & " )" & vbCrLf
        testo = testo & Space$(25) & "KEYLENGTH ( WK-KEYLENGTH )" & vbCrLf
        testo = testo & Space$(25) & "RESP ( WK-RESP )" & vbCrLf
        If Tipo = "H" Then
          testo = testo & Space$(25) & "UPDATE" & vbCrLf
        End If
        testo = testo & Space$(15) & "END-EXEC " & vbCrLf
        testo = testo & Space$(11) & "MOVE WKEY-" & Tb1!Nome & " TO WK-KEYVALUE" & vbCrLf
      Tb1.MoveNext
    Wend
    testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
 
  End If
Else

  If Trim(TabIstr.BlkIstr(Ind).Tavola) <> "" Then
    testo = testo & Space$(11) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & TabIstr.BlkIstr(Ind).Segment & vbCrLf
    testo = testo & Space$(11) & "PERFORM " & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL THRU EX-" & TabIstr.BlkIstr(Ind).Segment & "-TO-ADL" & vbCrLf
    testo = testo & Space$(11) & "MOVE ADL-" & TabIstr.BlkIstr(Ind).Tavola & " TO REC-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(11) & "START " & TabIstr.BlkIstr(Ind).Tavola & " KEY EQUAL " & TabIstr.BlkIstr(Ind).Tavola & "-KEY" & vbCrLf
    testo = testo & Space$(15) & "END-START" & vbCrLf
    testo = testo & Space$(11) & "IF WK-RESP = '00' THEN" & vbCrLf
    testo = testo & Space$(13) & "READ " & TabIstr.BlkIstr(Ind).Tavola & " NEXT RECORD" & vbCrLf
    testo = testo & Space$(16) & "INTO ADL-" & TabIstr.BlkIstr(Ind).Tavola & vbCrLf
    testo = testo & Space$(13) & "END-READ" & vbCrLf
    testo = testo & Space$(11) & "END-IF" & vbCrLf
    testo = testo & Space$(11) & "MOVE " & TabIstr.BlkIstr(Ind).Tavola & "-KEY TO WK-KEYVALUE" & vbCrLf
  Else
    Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
    wIdOggetto = Tb1!IdOggetto
    Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

    Tb1.MoveFirst
    testo = testo & Space$(11) & "EVALUATE WK-DATASET" & vbCrLf
    While Not Tb1.EOF
        If Ind = 0 Then Ind = 1
        testo = testo & Space$(13) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
        Set Tb2 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Tb1!IdSegm)
        testo = testo & Space$(15) & "MOVE LNKDB-AREA-DATI(" & Format(Ind, "#0") & ") TO WK-" & Tb2!Nome & vbCrLf
        testo = testo & Space$(15) & "PERFORM " & Tb2!Nome & "-TO-ADL THRU EX-" & Tb2!Nome & "-TO-ADL" & vbCrLf
        testo = testo & Space$(15) & "MOVE ADL-" & Tb1!Nome & " TO REC-" & Tb1!Nome & vbCrLf
        testo = testo & Space$(11) & "START " & Tb1!Nome & " KEY EQUAL " & Tb1!Nome & "-KEY" & vbCrLf
        testo = testo & Space$(15) & "END-START" & vbCrLf
        testo = testo & Space$(11) & "IF WK-RESP = '00' THEN" & vbCrLf
        testo = testo & Space$(13) & "READ " & Tb1!Nome & " NEXT RECORD" & vbCrLf
        testo = testo & Space$(16) & "INTO ADL-" & Tb1!Nome & vbCrLf
        testo = testo & Space$(13) & "END-READ" & vbCrLf
        testo = testo & Space$(11) & "END-IF" & vbCrLf
        testo = testo & Space$(11) & "MOVE " & Tb1!Nome & "-KEY TO WK-KEYVALUE" & vbCrLf
      Tb1.MoveNext
    Wend
    testo = testo & Space$(11) & "END-EVALUATE" & vbCrLf
 
  End If
End If
LeggiVsm = testo
 
End Function

Function AggRtVsmGhn() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
   If Trim(TabIstr.BlkIstr(1).Segment) = "" Then
      AggRtVsmGhn = AggRtVsmGhnNQ
      Exit Function
   End If
      
   testo = InitIstr("GHN", "S")
   If UBound(TabIstr.BlkIstr) = 1 And TabIstr.BlkIstr(1).Parentesi <> "(" Then
     AggRtVsmGhn = testo & AggRtVsmGnNq1()
     Exit Function
   End If

   If TabIstr.BlkIstr(1).Parentesi = "(" Then
   
      Set Tb2 = DbPrj.OpenRecordset("select * from dmindex where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and nomekey like '" & TabIstr.BlkIstr(1).key & "*'")
      If Tb2.RecordCount > 0 Then
         testo = testo & AggRtVsmGhn2K
         AggRtVsmGhn = testo
         Exit Function
      End If
      Set Tb2 = DbPrj.OpenRecordset("select * from field where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and nome = '" & TabIstr.BlkIstr(1).key & "'")
      If Tb2.RecordCount > 0 Then
         If Tb2!Pointer <> "INDX" Then
            testo = testo & Space$(11) & "  Attenzione al Nome Key 2 " & vbCrLf
            testo = testo & AggRtVsmGhn2K
            AggRtVsmGhn = testo
            Exit Function
         End If
      End If
   End If
   
   testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
   
   For K = 1 To UBound(TabIstr.BlkIstr)
      
      testo = testo & PuntaGuida(K, " ")
      
      If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(13) & "MOVE '" & TabIstr.BlkIstr(K).Tavola & "' TO WK-DATASET" & vbCrLf
           testo = testo & Space$(11) & "END-IF " & vbCrLf
           testo = testo & LeggiVsmGn(K, "H")
           testo = testo & Space$(11) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & ContrExGn(K)
              
           testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(14) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
       Else
           testo = testo & Space$(13) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(15) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
           testo = testo & Space$(13) & "END-IF" & vbCrLf
           
       End If
       testo = testo & Space$(6) & "*" & vbCrLf
           
   Next K
   
   
   AggRtVsmGhn = testo
   
End Function
Function AggRtVsmGhnNQ() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
      
   testo = InitIstr("GHN", "N")

   testo = testo & Space$(11) & "MOVE 'GT' TO WK-OPERATORE" & vbCrLf
   testo = testo & Space$(11) & "PERFORM LEGGI-GUIDA THRU EX-LEGGI-GUIDA." & vbCrLf
   testo = testo & LeggiVsm(K, "H")
           
   testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
   testo = testo & EvalToADL
   testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "ELSE " & vbCrLf
   testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "END-IF." & vbCrLf
   
   AggRtVsmGhnNQ = testo
   
End Function

Function AggRtVsmGhnp() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
   If Trim(TabIstr.BlkIstr(1).Segment) = "" Then
      AggRtVsmGhnp = AggRtVsmGhnpNQ
      Exit Function
   End If
  
   testo = InitIstr("GHNP", "S")
   If UBound(TabIstr.BlkIstr) = 1 And TabIstr.BlkIstr(1).Parentesi <> "(" Then
     AggRtVsmGhnp = testo & AggRtVsmGnNq1()
     Exit Function
   End If
   
   
   
   testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
   
   For K = 1 To UBound(TabIstr.BlkIstr)
      testo = testo & PuntaGuida(K, " ")
      
      If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(13) & "MOVE '" & TabIstr.BlkIstr(K).Tavola & "' TO WK-DATASET" & vbCrLf
           testo = testo & Space$(11) & "END-IF " & vbCrLf
           testo = testo & LeggiVsm(K, "H")
           testo = testo & Space$(11) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & ContrExGn(K)

           testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(14) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
       Else
           testo = testo & Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
           testo = testo & Space$(11) & "END-IF" & vbCrLf
           
       End If
       testo = testo & Space$(6) & "*" & vbCrLf
           
   Next K


   AggRtVsmGhnp = testo
   
End Function
Function AggRtVsmGhnpNQ() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
  
   testo = InitIstr("GHNP", "N")
   testo = testo & Space$(11) & "MOVE 'GT' TO WK-OPERATORE" & vbCrLf

   testo = testo & Space$(11) & "PERFORM LEGGI-GUIDA THRU EX-LEGGI-GUIDA." & vbCrLf
   testo = testo & LeggiVsm(K, "H")

   testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
   testo = testo & EvalToADL
   testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "ELSE " & vbCrLf
   testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "END-IF." & vbCrLf
   
   AggRtVsmGhnpNQ = testo
   
End Function

Function AggRtVsmGnp() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
     
   If Trim(TabIstr.BlkIstr(1).Segment) = "" Then
      AggRtVsmGnp = AggRtVsmGnpNQ
      Exit Function
   End If
 
   testo = InitIstr("GNP", "S")
   testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
   
   If UBound(TabIstr.BlkIstr) = 1 And TabIstr.BlkIstr(1).Parentesi <> "(" Then
     AggRtVsmGnp = testo & AggRtVsmGnNq1()
     Exit Function
   End If
   
   
   For K = 1 To UBound(TabIstr.BlkIstr)
      testo = testo & PuntaGuida(K, " ")
      
      If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(13) & "MOVE '" & TabIstr.BlkIstr(K).Tavola & "' TO WK-DATASET" & vbCrLf
           testo = testo & Space$(11) & "END-IF " & vbCrLf
           testo = testo & LeggiVsmGn(K, " ")
           testo = testo & Space$(11) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & ContrExGn(K)

           testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(14) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
       Else
           testo = testo & Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
           testo = testo & Space$(11) & "END-IF" & vbCrLf
       End If
       testo = testo & Space$(6) & "*" & vbCrLf
           
   Next K

   AggRtVsmGnp = testo
   
End Function
Function AggRtVsmGnpNQ() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
     
 
   testo = InitIstr("GNP", "N")

   testo = testo & Space$(11) & "MOVE 'GT' TO WK-OPERATORE" & vbCrLf
   testo = testo & Space$(11) & "PERFORM LEGGI-GUIDA THRU EX-LEGGI-GUIDA." & vbCrLf
   testo = testo & LeggiVsm(K, " ")

   testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
   testo = testo & EvalToADL
   testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "ELSE " & vbCrLf
   testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "END-IF." & vbCrLf
   

   AggRtVsmGnpNQ = testo
   
End Function

Function AggRtVsmGn() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
   If Trim(TabIstr.BlkIstr(1).Segment) = "" Then
      AggRtVsmGn = AggRtVsmGnNQ
      Exit Function
   End If
   testo = InitIstr("GN", "S")
   
   If TabIstr.BlkIstr(1).Parentesi = "(" Then
      
      Set Tb2 = DbPrj.OpenRecordset("select * from dmindex where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and nomekey like '" & TabIstr.BlkIstr(1).key & "*'")
      If Tb2.RecordCount > 0 Then
        testo = testo & AggRtVsmGn2K
        AggRtVsmGn = testo
        Exit Function
      End If
      Set Tb2 = DbPrj.OpenRecordset("select * from field where idsegm = " & TabIstr.BlkIstr(1).IdSegm & " and nome = '" & TabIstr.BlkIstr(1).key & "'")
      If Tb2.RecordCount > 0 Then
         If Tb2!Pointer <> "INDX" Then
            testo = testo & Space$(11) & "  Attenzione al Nome Key 2 " & vbCrLf
            testo = testo & AggRtVsmGn2K
            AggRtVsmGn = testo
            Exit Function
         End If
      End If
   End If
   
   If UBound(TabIstr.BlkIstr) = 1 And TabIstr.BlkIstr(1).Parentesi <> "(" Then
     AggRtVsmGn = testo & AggRtVsmGnNq1()
     Exit Function
   End If
   
   testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
   For K = 1 To UBound(TabIstr.BlkIstr)
      testo = testo & PuntaGuida(K, " ")
      
      If TabIstr.BlkIstr(K).Record <> "" Then
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(13) & "MOVE '" & TabIstr.BlkIstr(K).Tavola & "' TO WK-DATASET" & vbCrLf
           testo = testo & Space$(11) & "END-IF " & vbCrLf
           testo = testo & LeggiVsmGn(K, " ")
           testo = testo & Space$(11) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & ContrExGn(K)
           testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(14) & "PERFORM " & TabIstr.BlkIstr(K).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(K).Segment & "-TO-WK" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-" & TabIstr.BlkIstr(K).Segment & " TO LNKDB-AREA-DATI(" & Format(K) & ")" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space$(14) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
           testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "ELSE " & vbCrLf
           testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
           testo = testo & Space$(11) & "END-IF." & vbCrLf
       Else
           testo = testo & Space$(11) & "IF WK-RETRESP NOT EQUAL " & StringaTpBt("ZERO") & vbCrLf
           testo = testo & Space$(13) & "GO TO EX-ROUT-" & gIstruzione & vbCrLf
           testo = testo & Space$(11) & "END-IF" & vbCrLf
       End If
       testo = testo & Space$(6) & "*" & vbCrLf
           
   Next K


   AggRtVsmGn = testo
   
End Function
Function AggRtVsmGnNq1() As String

Dim testo As String
Dim Tb1 As Recordset
Dim Tb2 As Recordset
Dim wIdOggetto As Variant
   
   testo = ""
   If TipoRoutine(NomeRoutine) = "T" Then
        testo = Space$(11) & "MOVE '" & TabIstr.BlkIstr(1).Tavola & "' TO WK-DATASET" & vbCrLf
        testo = testo & Space$(11) & "MOVE 1 TO WK-IND" & vbCrLf
        testo = testo & Space$(11) & "PERFORM UNTIL WK-IND GREATER 20" & vbCrLf
        testo = testo & Space$(14) & "IF WK-DATASET = LNKDB-TABNAME(WK-IND)" & vbCrLf
        testo = testo & Space$(16) & "IF LNKDB-TABNUM(WK-IND) = LNKDB-STKNUM(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space$(18) & "MOVE LNKDB-STKNAME(LNKDB-NUMSTACK) TO WK-DATASET" & vbCrLf
        testo = testo & Space$(16) & "END-IF" & vbCrLf
        testo = testo & Space$(16) & "MOVE 50 TO WK-IND" & vbCrLf
        testo = testo & Space$(14) & "END-IF" & vbCrLf
        testo = testo & Space$(14) & "ADD 1 TO WK-IND" & vbCrLf
        testo = testo & Space$(11) & "END-PERFORM" & vbCrLf
        testo = testo & Space$(6) & "* " & vbCrLf
        testo = testo & Space$(11) & "MOVE LNKDB-STKFDBKEY(LNKDB-NUMSTACK) TO WK-KEYVALUE" & vbCrLf
        testo = testo & Space$(6) & "* " & vbCrLf
        testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) NOT EQUAL 'S'" & vbCrLf
        testo = testo & Space$(14) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        
       testo = testo & Space$(14) & "EXEC CICS ENDBR" & vbCrLf
       testo = testo & Space$(20) & "FILE (  WK-DATASET ) " & vbCrLf
       testo = testo & Space$(20) & "RESP (WK-RESP )" & vbCrLf
       testo = testo & Space$(14) & "END-EXEC" & vbCrLf
        
        testo = testo & Space$(14) & "EXEC CICS STARTBR" & vbCrLf
        testo = testo & Space$(24) & "FILE (WK-DATASET)" & vbCrLf
        testo = testo & Space$(24) & "RIDFLD (WK-KEYVALUE)" & vbCrLf
        testo = testo & Space$(24) & "GTEQ" & vbCrLf
        testo = testo & Space$(24) & "RESP (WK-RESP)" & vbCrLf
        testo = testo & Space$(14) & "END-EXEC" & vbCrLf
        testo = testo & Space$(14) & "IF WK-RESP = 15 THEN" & vbCrLf
        testo = testo & Space$(16) & "MOVE ZERO TO WK-RESP" & vbCrLf
        testo = testo & Space$(14) & "END-IF" & vbCrLf
        testo = testo & Space$(14) & "IF WK-RESP = 0" & vbCrLf
        testo = testo & Space$(16) & "EXEC CICS READNEXT" & vbCrLf
        testo = testo & Space$(26) & "FILE (WK-DATASET)" & vbCrLf
        testo = testo & Space$(26) & "RIDFLD (WK-KEYVALUE)" & vbCrLf
        testo = testo & Space$(26) & "INTO (ADL-" & TabIstr.BlkIstr(1).Tavola & ")" & vbCrLf
        testo = testo & Space$(26) & "RESP (WK-RESP)" & vbCrLf
        testo = testo & Space$(16) & "END-EXEC" & vbCrLf
        testo = testo & Space$(14) & "END-IF" & vbCrLf
        testo = testo & Space$(14) & "IF WK-RESP = 15 THEN" & vbCrLf
        testo = testo & Space$(16) & "MOVE ZERO TO WK-RESP" & vbCrLf
        testo = testo & Space$(14) & "END-IF" & vbCrLf
        testo = testo & Space$(11) & "END-IF" & vbCrLf
        testo = testo & Space$(6) & "* " & vbCrLf
        
        testo = testo & Space$(11) & "IF WK-RESP = 0" & vbCrLf
        testo = testo & Space$(14) & "EXEC CICS READNEXT" & vbCrLf
        testo = testo & Space$(24) & "FILE (WK-DATASET)" & vbCrLf
        testo = testo & Space$(24) & "RIDFLD (WK-KEYVALUE)" & vbCrLf
        testo = testo & Space$(24) & "INTO (ADL-" & TabIstr.BlkIstr(1).Tavola & ")" & vbCrLf
        testo = testo & Space$(24) & "RESP (WK-RESP)" & vbCrLf
        testo = testo & Space$(14) & "END-EXEC" & vbCrLf
        testo = testo & Space$(11) & "END-IF" & vbCrLf
        testo = testo & Space$(11) & "IF WK-RESP = 15 THEN" & vbCrLf
        testo = testo & Space$(14) & "MOVE ZERO TO WK-RESP" & vbCrLf
        testo = testo & Space$(11) & "END-IF" & vbCrLf
     
        testo = testo & Space$(11) & "IF WK-RESP = DFHRESP(NORMAL)" & vbCrLf
        testo = testo & Space$(14) & "PERFORM " & TabIstr.BlkIstr(1).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(1).Segment & "-TO-WK" & vbCrLf
        testo = testo & Space$(14) & "MOVE WK-" & TabIstr.BlkIstr(1).Segment & " TO LNKDB-AREA-DATI(" & Format(1) & ")" & vbCrLf
        testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space$(14) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space$(14) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
        testo = testo & Space$(14) & "MOVE ZERO TO WK-RETRESP" & vbCrLf
        testo = testo & Space$(11) & "ELSE " & vbCrLf
        testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
        testo = testo & Space$(11) & "END-IF." & vbCrLf
    Else
        testo = Space$(11) & "MOVE '" & TabIstr.BlkIstr(1).Tavola & "' TO WK-DATASET" & vbCrLf
        testo = testo & Space$(11) & "MOVE 1 TO WK-IND" & vbCrLf
        testo = testo & Space$(11) & "PERFORM UNTIL WK-IND GREATER 20" & vbCrLf
        testo = testo & Space$(14) & "IF WK-DATASET = LNKDB-TABNAME(WK-IND)" & vbCrLf
        testo = testo & Space$(16) & "IF LNKDB-TABNUM(WK-IND) = LNKDB-STKNUM(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space$(18) & "MOVE LNKDB-STKNAME(LNKDB-NUMSTACK) TO WK-DATASET" & vbCrLf
        testo = testo & Space$(16) & "END-IF" & vbCrLf
        testo = testo & Space$(16) & "MOVE 50 TO WK-IND" & vbCrLf
        testo = testo & Space$(14) & "END-IF" & vbCrLf
        testo = testo & Space$(14) & "ADD 1 TO WK-IND" & vbCrLf
        testo = testo & Space$(11) & "END-PERFORM" & vbCrLf
        testo = testo & Space$(6) & "* " & vbCrLf
        testo = testo & Space$(11) & "MOVE LNKDB-STKFDBKEY(LNKDB-NUMSTACK) TO WK-KEYVALUE" & vbCrLf
        testo = testo & Space$(6) & "* " & vbCrLf
        testo = testo & Space$(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) NOT EQUAL 'S'" & vbCrLf
        testo = testo & Space$(14) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        
        testo = testo & Space$(14) & "EVALUATE WK-DATASET" & vbCrLf
    
       Set Tb1 = DbPrj.OpenRecordset("select * from oggetti where tipo = 'DBD' and nome = '" & Trim(TabIstr.DBD) & ".DBD'")
       wIdOggetto = Tb1!IdOggetto
       Set Tb1 = DbPrj.OpenRecordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & wIdOggetto & ")")

       Tb1.MoveFirst
       While Not Tb1.EOF
          testo = testo & Space$(16) & "WHEN '" & Tb1!Nome & "'" & vbCrLf
          
          testo = testo & Space$(18) & "MOVE WK-KEYVALUE TO " & Tb1!Nome & "-KEY" & vbCrLf
          testo = testo & Space$(18) & "START " & Tb1!Nome & " KEY NOT LESS " & Tb1!Nome & "-KEY" & vbCrLf
          testo = testo & Space$(18) & "END-START" & vbCrLf
          testo = testo & Space$(18) & "IF WK-RESP = '22' THEN" & vbCrLf
          testo = testo & Space$(20) & "MOVE '00' TO WK-RESP" & vbCrLf
          testo = testo & Space$(18) & "END-IF" & vbCrLf
          testo = testo & Space$(18) & "IF WK-RESP = '00'" & vbCrLf
          testo = testo & Space$(20) & "READ " & Tb1!Nome & " NEXT RECORD" & vbCrLf
          testo = testo & Space$(22) & "INTO ADL-" & Tb1!Nome & vbCrLf
          testo = testo & Space$(20) & "END-READ" & vbCrLf
          testo = testo & Space$(18) & "END-IF" & vbCrLf
          testo = testo & Space$(18) & "IF WK-RESP = '22' THEN" & vbCrLf
          testo = testo & Space$(20) & "MOVE '00' TO WK-RESP" & vbCrLf
          testo = testo & Space$(18) & "END-IF" & vbCrLf
        
          testo = testo & Space$(18) & "IF WK-RESP = '00'" & vbCrLf
          testo = testo & Space$(20) & "READ " & Tb1!Nome & " NEXT RECORD" & vbCrLf
          testo = testo & Space$(22) & "INTO ADL-" & Tb1!Nome & vbCrLf
          testo = testo & Space$(20) & "END-READ" & vbCrLf
          testo = testo & Space$(18) & "END-IF" & vbCrLf
          testo = testo & Space$(18) & "IF WK-RESP = '22' THEN" & vbCrLf
          testo = testo & Space$(20) & "MOVE '00' TO WK-RESP" & vbCrLf
          testo = testo & Space$(18) & "END-IF" & vbCrLf
          testo = testo & Space$(18) & "MOVE " & Tb1!Nome & "-KEY TO WK-KEYVALUE" & vbCrLf
          Tb1.MoveNext
        Wend
          
        testo = testo & Space$(14) & "END-EVALUATE" & vbCrLf
        testo = testo & Space$(11) & "END-IF" & vbCrLf
        
        testo = testo & Space$(11) & "IF WK-RESP = '00'" & vbCrLf
        testo = testo & Space$(14) & "PERFORM " & TabIstr.BlkIstr(1).Segment & "-TO-WK THRU EX-" & TabIstr.BlkIstr(1).Segment & "-TO-WK" & vbCrLf
        testo = testo & Space$(14) & "MOVE WK-" & TabIstr.BlkIstr(1).Segment & " TO LNKDB-AREA-DATI(" & Format(1) & ")" & vbCrLf
        testo = testo & Space$(14) & "MOVE WK-DATASET TO LNKDB-STKNAME(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space$(14) & "MOVE WK-KEYVALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space$(14) & "PERFORM REST-FBKEY THRU EX-REST-FBKEY" & vbCrLf
        testo = testo & Space$(14) & "MOVE '00' TO WK-RETRESP" & vbCrLf
        testo = testo & Space$(11) & "ELSE " & vbCrLf
        testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
        testo = testo & Space$(11) & "END-IF." & vbCrLf

    End If
    
AggRtVsmGnNq1 = testo

End Function
Function AggRtVsmGnNQ() As String
   
   Dim K As Integer
   Dim IndIstr As Double
   Dim testo As String
   Dim Tb3 As Recordset
   Dim Tb2 As Recordset
   Dim WIdSeg As Variant
   Dim OldSegm As Variant
   
   testo = InitIstr("GN", "N")
   
   testo = testo & Space$(11) & "MOVE 'GT' TO WK-OPERATORE" & vbCrLf
   testo = testo & Space$(11) & "PERFORM LEGGI-GUIDA THRU EX-LEGGI-GUIDA." & vbCrLf
   testo = testo & LeggiVsm(K, " ")

   testo = testo & Space$(11) & "IF WK-RESP = " & StringaTpBt("ZERO") & vbCrLf
   testo = testo & EvalToADL
   testo = testo & Space$(14) & "MOVE " & StringaTpBt("ZERO") & " TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "ELSE " & vbCrLf
   testo = testo & Space$(14) & "MOVE WK-RESP TO WK-RETRESP" & vbCrLf
   testo = testo & Space$(11) & "END-IF." & vbCrLf

   AggRtVsmGnNQ = testo
   
End Function

Function AggRoutSSA(wIstruzione) As String

Dim Stringa As String
Dim K As Integer
Dim y As Integer

Stringa = ""
If ChkTestRout.Value = Unchecked Then
   AggRoutSSA = Stringa
   Exit Function
End If

    For K = 1 To UBound(TabIstr.BlkIstr)
       If TabIstr.BlkIstr(K).Segment <> "" Then
          Stringa = Stringa & Space$(11) & "MOVE '" & Trim(TabIstr.BlkIstr(K).Segment) & "' TO WK-SSA(1:8)" & vbCrLf
       End If
       Stringa = Stringa & Space$(11) & "IF WK-SSA <> LNKDB-SSA-AREA(" & Format(K, "#0") & ") " & vbCrLf
       Stringa = Stringa & Space$(13) & "MOVE 'DLSA' TO LNKDB-CODICE-ERRORE" & vbCrLf
       Stringa = Stringa & Space$(13) & "PERFORM ROUT-ERRORE THRU EX-ROUT-ERRORE" & vbCrLf
       Stringa = Stringa & Space$(13) & "GO TO EX-ROUT-" & wIstruzione & "." & vbCrLf
    Next K
    
    AggRoutSSA = Stringa

End Function
Sub ChiudiIf(wpos)
   
   Dim K As Integer
   Dim testo As String
   
   
      If wPunto Then
         testo = vbCrLf & Space$(wIdent) & ". "
         RTBModifica.SelStart = wpos
         RTBModifica.SelLength = 0
         RTBModifica.SelText = testo
         AddRighe = AddRighe + 1
         wpos = wpos + Len(testo)
      End If

End Sub

Function ControllaGenRout() As Boolean
Dim K As Integer
Dim y As Integer
Dim swFlag As Boolean
Dim RST1 As Recordset
Dim RST2 As Recordset
Dim wNomeRoutine As String
Dim NomeSegm As String
Dim LenSegm As String
Dim Nomdb As String
Dim K1 As Integer
Dim NumSegm As String


        ListaRoutine.AddItem Trim(NomeRoutine) & ".cbl"
        NumSegm = NomeRoutine
        For K1 = 1 To Len(NumSegm)
          If Mid$(NumSegm, K1, 1) < "0" Or Mid$(NumSegm, K1, 1) > "9" Then
             Mid$(NumSegm, K1, 1) = " "
          End If
        Next
        NumSegm = Trim(NumSegm)
        If val(NumSegm) <> 0 Then
           Set RST2 = DbPrj.OpenRecordset("SELECT * FROM Segmenti WHERE idoggetto = " & val(NumSegm))
           LenSegm = 0
           If RST2.RecordCount > 0 Then
              RST2.MoveFirst
              While Not RST2.EOF
                 If RST2!MaxLen > LenSegm Then
                    LenSegm = RST2!MaxLen
                 End If
                 RST2.MoveNext
              Wend
           End If
           NomeSegm = "ALL"
           Set RST1 = DbPrj.OpenRecordset("select * from oggetti where idoggetto = " & NumSegm)
           Nomdb = RST1!Nome
           K1 = InStr(Nomdb, ".")
           Nomdb = Mid$(Nomdb, 1, K1 - 1)
'           Nomdb = TabIstr.DBD
        Else
           NomeSegm = "UNKNOW"
           LenSegm = 64
           Nomdb = "UNKNOW"
        End If
        Select Case TipoRoutine(NomeRoutine)
             Case "G"
                  If ChkDbCbl = vbChecked Then
                     Select Case TIPO_RDBMS
                        Case Is = "VSAM"
                           RTRoutine.LoadFile (App.Path & "\routi-o\routvsm.cbl")
                        Case Is = "DB2"
                           RTRoutine.LoadFile (App.Path & "\routi-o\routdb2.cbl")
                     End Select
                  Else
                     RTRoutine.LoadFile (App.Path & "\routi-o\routims.cbl")
                  End If
             Case "T"
                  If ChkDbCbl = vbChecked Then
                     Select Case TIPO_RDBMS
                        Case Is = "VSAM"
                          RTRoutine.LoadFile (App.Path & "\routi-o\routTPvsm.cbl")
                        Case Is = "DB2"
                          RTRoutine.LoadFile (App.Path & "\routi-o\routTPdb2.cbl")
                     End Select
                  Else
                     RTRoutine.LoadFile (App.Path & "\routi-o\routTPims.cbl")
                  End If
             Case "B"
                  If ChkDbCbl = vbChecked Then
                     Select Case TIPO_RDBMS
                        Case Is = "VSAM"
                           RTRoutine.LoadFile (App.Path & "\routi-o\routBTvsm.cbl")
                        Case Is = "DB2"
                           RTRoutine.LoadFile (App.Path & "\routi-o\routBTdb2.cbl")
                     End Select
                  Else
                     RTRoutine.LoadFile (App.Path & "\routi-o\routBTims.cbl")
                  End If
        End Select
        Call Crea_Routine_Ext(RTRoutine, NomeRoutine, Date, Nomdb, NomeSegm, LenSegm)
  ControllaGenRout = True
End Function



Function SelPar(Stringa, Ident) As String

Dim wStringa As String
Dim K As Integer
Dim wStr As String

wStr = ""

wStringa = Stringa
If Ident = "SEARCH" Then
   If InStr(Stringa, " LAST") > 0 Then
      SelPar = "LAST"
      Exit Function
   End If
   If InStr(Stringa, " FIRST") > 0 Then
      SelPar = "FIRST"
      Exit Function
   End If
   SelPar = ""
   Exit Function
End If

K = InStr(wStringa, Ident)
If K = 0 Then
  SelPar = ""
  Exit Function
End If
If K > 1 Then
Select Case Mid$(wStringa, K - 1, 1)
  Case " ", "(", ","
  Case Else
    SelPar = ""
    Exit Function
End Select
End If
wStringa = Trim(Mid$(wStringa, K + Len(Ident)))
If Mid$(wStringa, 1, 1) = "=" Then
   wStringa = Mid$(wStringa, 2)
End If
Select Case Mid$(wStringa, 1, 1)
  Case "("
    K = InStr(wStringa, ")")
    If K = 0 Then
      wStringa = RTrim(wStringa) & ")"
      K = InStr(wStringa, ")")
    End If
    wStr = Mid$(wStringa, 2, K - 2)
  Case "'"
    K = InStr(Mid$(wStringa, 2), "'")
    wStr = Mid$(wStringa, 2, K - 1)
  Case Else
    K = InStr(wStringa, " ")
    If K > 0 Then
       wStringa = Mid$(wStringa, 1, K)
    End If
    K = InStr(wStringa, ",")
    If K > 0 Then
       wStringa = Mid$(wStringa, 1, K)
    End If
    wStr = wStringa
End Select
K = InStr(wStr, ")")
If K > 0 Then
   If K > Len(wStr) Then
      wStr = wStr
   End If
   wStr = Mid$(wStr, 1, K)
End If
SelPar = wStr

End Function

Sub Controlla_CBL_CPY()
    '2) Controlla se l'istruzione Corrente fa parte Del CBL o la CPY gi� aperta, se si non carica il CBL o CPY
        'ma li aggiorna:
        Dim NCBLcpy As String
        Dim Tip As String
        Dim K As Integer
        
        Dim w As Long
        Dim ww As Long
        
        Dim r As Recordset
        
    Set r = DbPrj.OpenRecordset("SELECT * FROM Oggetti WHERE Idoggetto = " & Record!IdOggetto)
    If r.RecordCount > 0 Then
        r.MoveFirst
        NCBLcpy = r!Nome
        NomeProg = r!Nome
        K = InStr(NomeProg, ".")
        If K > 0 Then NomeProg = Trim(Mid$(NomeProg, 1, K - 1))
        
        DirCBLcpy = r!Directory
        w = InStr(1, DirCBLcpy, "\")
        While w > 0
                ww = w
                w = InStr(w + 1, DirCBLcpy, "\")
        Wend
        
        'qui ottengo la directori di origine del file
        DIROr = Mid(DirCBLcpy, ww + 1)
        
        Tip = r!Tipo
        
        If RTBModifica.Text = "" Then
            RTBModifica.LoadFile (DirCBLcpy & "\" & NCBLcpy)
            If r!Tipo <> "CPY" Then Inizializza_Copy
        Else
            If RTBModifica.FileName <> DirCBLcpy & "\" & NCBLcpy Then
                    'Salva il file
                    Crea_Directory_Cbl_Cpy 'crea le directory CBLOut e CPYOut se non esistono
                    
                    RTBModifica.SaveFile OldName, 1
                    If Tip = "CBL" Then
                            'inserisce i CBL modificate anche nella tabella oggetti del DB
                            
                            Call CREA_CBO_IN_TAB_OGGETTI(OldName1, r!dtparsing)
                    Else
                            'inserisce le CPY modificate anche nella tabella oggetti del DB
                            Call CREA_CPO_IN_TAB_OGGETTI(OldName1, r!dtparsing)
                    End If
                    
                    RTBModifica.LoadFile (DirCBLcpy & "\" & NCBLcpy)
                    If r!Tipo <> "CPY" Then Inizializza_Copy
            End If
        End If
        OldName = DirCBLcpy & "\Out\" & NCBLcpy
        OldName1 = NCBLcpy
     End If
End Sub

Sub Crea_Directory_Cbl_Cpy()
    '1) Controlla se esistono le 2 directory di output Routine, se no le crea:
            If Dir(DirCBLcpy & "\Out", vbDirectory) = "" Then
                'crea directory
                MkDir DirCBLcpy & "\Out"
            End If
            
'            If Dir$(PathPrj & "\CPYOut", vbDirectory) = "" Then
'                'crea directory
'                MkDir PathPrj & "\CPYOut"
'            End If
End Sub

Sub Inizializza_Copy()
    'controlla se il link con la copy LNKDBRT � presente nel file
    Dim t, w As Long
    Dim testo As String
    
    ReDim CmpPcb(0)
    
    t = RTBModifica.Find("COPY LNKDBRT", 1)
    RTBModifica.Refresh
    AddRighe = 0
    
    If t = -1 Then 'non esiste, inserire
        w = RTBModifica.Find(" WORKING-STORAGE ", 1)
        For t = w To 1 Step -1
          If Mid$(RTBModifica.Text, t, 2) = vbCrLf Then
             If Mid$(RTBModifica.Text, t + 8, 1) = "*" Then
                w = RTBModifica.Find(" WORKING-STORAGE ", w + 1)
             End If
             Exit For
          End If
        Next t
        w = RTBModifica.Find(vbCrLf, w)
        w = w + 1
        RTBModifica.SelStart = w
        RTBModifica.SelLength = 0
        testo = vbCrLf & ITER_I & "*--------------------------------------------------------------*"
        testo = testo & vbCrLf & ITER_I & "*  COPY UTILIZZATA PER L'INTERFACCIA CON LE ROUTINES DI I-0"
        testo = testo & vbCrLf & ITER_I & "*        BY I-TER TECNOLOGIE SRL"
        testo = testo & vbCrLf & ITER_I & "*--------------------------------------------------------------*"
        testo = testo & vbCrLf & ITER_I & " 01  IT-UIBRC            PIC  X(1)      VALUE LOW-VALUE."
        testo = testo & vbCrLf & ITER_I & " 01  LEN-LNKDB-AREA      PIC S9(5) COMP VALUE +15100."
        testo = testo & vbCrLf & ITER_I & " 01  AREA-POINTER        PIC S9(8) COMP."
        testo = testo & vbCrLf & ITER_I & " 01  AREA-POINTER-R REDEFINES AREA-POINTER POINTER."
        testo = testo & vbCrLf & ITER_I & " 01  LNKDB-AREA."
        testo = testo & vbCrLf & ITER_I & "     COPY LNKDBRT. "
        testo = testo & vbCrLf & ITER_I & "*--------------------------------------------------------------*"
        RTBModifica.SelText = testo
    End If
    AddRighe = 11

    t = AsteriscaEntry
    
    
End Sub


Function SelezionaIstruzioni() As Boolean
  Dim Criterio As String
  Dim IstrSql As String
  Dim Tb1 As Recordset
  Dim Str1 As String
  Dim K As Integer
  
  SelezionaIstruzioni = True
  ListaRoutine.Clear
  
  ' in criterio inserire clausola where su tavola istruzioni
  Str1 = Trim(DBDNome)
  If InStr(Str1, ",") > 0 Then
     Criterio = "(Nomedb = '"
  Else
     Criterio = "Nomedb = '"
  End If
  While Len(Str1) > 0
     K = InStr(Str1, ",")
     If K = 0 Then K = Len(Str1) + 1
     Criterio = Criterio & Mid$(Str1, 1, K - 1)
     Str1 = Mid$(Str1, K + 1)
     K = InStr(Criterio, ".")
     Criterio = Mid$(Criterio, 1, K - 1) & "'"
     If Len(Str1) > 0 Then
        Criterio = Criterio & " OR nomedb = '"
     End If
  Wend
  
  If Mid$(Criterio, 1, 1) = "(" Then
     Criterio = Criterio & ") "
  End If
  
  Str1 = ""
  If SelCBL Then
     Str1 = "'CBL'"
  End If
  If SelCPY Then
    If Str1 = "" Then
       Str1 = "'CPY'"
    Else
       Str1 = Str1 & " OR tipo = 'CPY'"
    End If
  End If
  If SelRout Then
    Str1 = ""
  End If
  If Trim(Str1) <> "" Then
    If InStr(Str1, " OR ") > 0 Then
      Str1 = "(Tipo = " & Str1 & ")"
    Else
      Str1 = "Tipo = " & Str1
    End If
  End If
  If Trim(Str1) <> "" Then
     Criterio = Criterio & " AND IDOGGETTO IN (SELECT IDOGGETTO FROM OGGETTI WHERE " & Str1 & ")"
  End If
  Str1 = ""
  If OggSelez And LstVOggSel.ListItems.Count > 0 Then
     For K = 1 To LstVOggSel.ListItems.Count
       If Trim(Str1) <> "" Then
          Str1 = Str1 & " OR idoggetto = " & LstVOggSel.ListItems(K).Text
       Else
          Str1 = " " & LstVOggSel.ListItems(K).Text
       End If
     Next K
  End If
  If Trim(Str1) <> "" Then
    If InStr(Str1, " OR ") > 0 Then
      Str1 = "(idoggetto = " & Str1 & ")"
    Else
      Str1 = "idoggetto = " & Str1
    End If
  End If
  If Trim(Str1) <> "" Then
     Criterio = Criterio & " AND IDOGGETTO IN (SELECT IDOGGETTO FROM OGGETTI WHERE " & Str1 & ")"
  End If
  
  
  If Trim(Criterio) <> "" Then
     Criterio = " Where flgistr = 0 and " & Criterio
  End If
  
  IstrSql = "select distinct Nomerout from Dettistr where flgistr = true"
 
  Set Record = DbPrj.OpenRecordset(IstrSql)
  If Record.RecordCount > 0 Then
    Record.MoveLast
     Record.MoveFirst
     While Not Record.EOF
 '      Set Tb1 = DbPrj.OpenRecordset("select * from segmenti where idsegm = " & Record!IdSegm)
       If Trim(Record!NomeRout) <> "" Then
          ListaRoutine.AddItem Trim(Record!NomeRout) & ".cbl"
       End If
       Record.MoveNext
     Wend
  Else
     SelezionaIstruzioni = False
     Exit Function
  End If
  IstrSql = "select distinct idoggetto from istruzioni  "  ' & Trim(Criterio)
  Set Record = DbPrj.OpenRecordset(IstrSql)
  If Record.RecordCount = 0 Then
     SelezionaIstruzioni = False
  Else
     Record.MoveLast
     Record.MoveFirst
  End If
End Function

Sub CREA_RIO_IN_TAB_OGGETTI(List As ListBox)
    Dim Num_Routine As Integer
    Dim i, j As Integer
    Dim File As String
    Dim Exist As Boolean
    
    Dim rs As Recordset
    
    Num_Routine = List.ListCount
    
    For i = 0 To Num_Routine
        File = List.List(i)
        'controlla se esiste gi� nella tabella oggetti
        Set rs = DbPrj.OpenRecordset("SELECT * FROM Oggetti WHERE Nome = '" & File & "'")
        
        If rs.RecordCount > 0 Then
        'vedere se � giusto che vada in edit
            rs.Edit
                rs!Nome = File
                rs!Tipo = "RIO"
                rs!Directory = PathPrj & "\Output-Prj\RoutIMS"
            rs.Update
        Else
            If File = "" Then
                Exit For
            End If
            rs.AddNew
                rs!Nome = File
                rs!Tipo = "RIO"
                rs!Directory = PathPrj & "\Output-Prj\RoutIMS"
                rs!NumRighe = 0
                rs!dtimport = Now
            rs.Update
        End If
    Next
End Sub

Sub SALVA_FILE_LOG()
    Dim K As Integer
    Dim str As String
    
    For K = 0 To FileLog.ListCount - 1
      str = "OK"
    Next
    
    RTLog.SaveFile (Dir1.Path & "\" & "RtnLog" & K + 1 & ".log")
    FileLog.Refresh
End Sub

Sub STAMPA_FILE_LOG()
    Call STAMPA_DA_RICHTEXTBOX(RTLog, CommonDialog1)
End Sub

Sub STAMPA_ROUTINE()
    Call STAMPA_DA_RICHTEXTBOX(RTRoutine, CommonDialog1)
End Sub

Private Sub Dir1_Change()
    File1.Path = Dir1.Path
    FileLog.Path = Dir1.Path
    
    If File1.ListCount = 0 Then
        Toolbar1.Buttons.Item(5).ButtonMenus.Item(1).Enabled = False
        Toolbar1.Buttons.Item(5).ButtonMenus.Item(2).Enabled = False
        Toolbar1.Buttons.Item(5).Enabled = False
        frmRoutineIO.RoutineVIs.Text = ""
        Exit Sub
    Else
        frmRoutineIO.RoutineVIs.LoadFile (Dir1.Path & "\" & File1.List(0))
        Toolbar1.Buttons.Item(5).ButtonMenus.Item(1).Enabled = True
        Toolbar1.Buttons.Item(5).ButtonMenus.Item(2).Enabled = True
        Toolbar1.Buttons.Item(5).Enabled = True
       
    End If
End Sub

Private Sub Drive1_Change()
    On Error GoTo ERR
    Dir1.Path = Drive1.Drive
    If Dir1.Path = "d:\" Then
        Call MsgBox("Impossibile Accedere In Scrittura a " & Drive1.Drive & "\" & vbCrLf & vbCrLf & "Periferica Non Pronta", vbCritical, "DLI2RDB Client")
        Dir1.Path = "c:\"
        Drive1.Drive = "c"
    End If
    
    Exit Sub

ERR:
    If ERR.Number = 68 Then
        Call MsgBox("Impossibile Accedere In Scrittura a " & Drive1.Drive & "\" & vbCrLf & "Periferica Non Pronta", vbCritical, "DLI2RDB Client")
    End If
End Sub

Private Sub File1_DblClick()
    frmRoutineIO.FraRoutIO.Visible = True
    If Mid(File1.FileName, Len(File1.FileName) - 2, 3) = "cbl" Then
        frmRoutineIO.RoutineVIs.LoadFile (Dir1.Path & "\" & File1.FileName)
    End If
    
    frmRoutineIO.FraRoutIO.Caption = "Routine I/O : " & File1.FileName
    RTLog.Text = RTLog.Text & Now & " - Caricamento File : " & Dir1.Path & "\" & File1.FileName & "..." & vbCrLf
    
    Toolbar1.Buttons.Item(5).ButtonMenus.Item(1).Enabled = True
    Toolbar1.Buttons.Item(5).ButtonMenus.Item(2).Enabled = True
    Toolbar1.Buttons.Item(5).Enabled = True
    Toolbar1.Buttons.Item(4).Enabled = True
    
End Sub

Sub Resize()
    On Error Resume Next
    Me.Move GbLeft, GbTop, GbWidth, GbHeight
    
End Sub

Private Sub Form_Load()
        
    
    Dim PercorsoSorg As String
    SEGN_X = "ITER-X "
    SEGN_L = "ITER-L "
    Segn_P = "ITER-P "
    Segn_T = "ITER-T "
    
    On Error GoTo ER
    'CARICA ROUTINE NELLA RICHTEXTBOX
    PercorsoSorg = App.Path & "\routi-o\routims.cbl"
    Dim g, h As Integer
    Dim percors, d As String
    
     
    g = 1
    For h = 1 To Len(nDbPrj)
        h = g
        g = InStr(g + 1, nDbPrj, "\")
        
        If g = 0 Then
            percors = Mid(nDbPrj, 1, h) & "Output-Prj\RoutIMS"
            d = Mid(nDbPrj, 1, h - 1)
            Exit For
        End If
    Next
    Dir1.Path = percors
    File1.Pattern = "*.cbl"
    
    frmRoutineIO.FraRoutIO.Caption = "Routine I/O BASE"
    
    frmRoutineIO.RoutineVIs.Text = ""
    
    Toolbar1.Buttons.Item(5).ButtonMenus.Item(1).Enabled = False
    Toolbar1.Buttons.Item(5).ButtonMenus.Item(2).Enabled = False
    Toolbar1.Buttons.Item(4).Enabled = False
    Toolbar1.Buttons.Item(5).Enabled = False
    
    GbFinIncaps = True
    
    'carica treeview
    TROggetti.Nodes.Add , , "A", "Origine"
    TROggetti.Nodes.Add "A", tvwChild, "A1", "Programmi CBL"
    TROggetti.Nodes.Add "A", tvwChild, "A2", "File CPY"
    TROggetti.Nodes.Add , , "B", "Destinazione"
    TROggetti.Nodes.Add "B", tvwChild, "B1", "Routine I/O"
    

    
Exit Sub

ER:
    If ERR.Number = 76 Then
        'crea directory se non esiste
        MkDir (d & "\Output-Prj")
        MkDir (d & "\Output-Prj\RoutIMS")
        Dir1.Path = percors
        File1.Pattern = "*.cbl"
    End If
End Sub

Private Sub Form_Resize()
    frmIncaps.Resize
End Sub

Private Sub Form_Unload(Cancel As Integer)
    GbFinIncaps = False
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    Select Case ButtonMenu.key
        Case "PrintLog"
            Call STAMPA_FILE_LOG
        Case "PrintRoutine"
            Call STAMPA_ROUTINE
    End Select
End Sub

Sub CONTROLLA_OPZIONI_GENERAZIONE()
'memorizza nelle variabili lo stato di selezione delle varie opzioni.
    If OptScelti.Value = True Then
        SelGuidata = True
        
        If TROggetti.Nodes.Item("A1").Checked = True Then
            SelCBL = True
        Else
            SelCBL = False
        End If
        
        If TROggetti.Nodes.Item("A2").Checked = True Then
            SelCPY = True
        Else
            SelCPY = False
        End If
        
        If TROggetti.Nodes.Item("B1").Checked = True Then
            SelRout = True
        Else
            SelRout = False
        End If
    Else
        SelGuidata = False
    End If
    
    If OptSel.Value = True Then
        OggSelez = True
    Else
        OggSelez = False
    End If
    
    If OptTUTTI.Value = True Then
        TuttiOgg = True
    Else
        TuttiOgg = False
    End If
End Sub

Function TROVA_RIGA(RTBox As RichTextBox, Chiave As String, Riga As Long) As Boolean

   Dim FoundPos As Long
   Dim FoundLine As Long
   Dim Flag As String
   Dim testo As String
   FoundPos = 0
   Flag = True
   
   While Flag
     FoundPos = RTBox.Find(Trim(Chiave), FoundPos + 1)
'     RTBox.SetFocus
     If FoundPos = -1 Then
        Flag = False
     End If

     If FoundPos <> -1 Then

       FoundLine = RTBox.GetLineFromChar(FoundPos) + 1
      
       If FoundLine = Riga Then
            TROVA_RIGA = True
            Exit Function
       End If

       If FoundLine > Riga Then
          Flag = False
       End If
     End If
   Wend

   FoundPos = 0
   Flag = True
   
   If Chiave = " EXEC " Then
     Chiave = "CBLTDLI"
   Else
     Chiave = " EXEC "
   End If
   
   While Flag
     FoundPos = RTBox.Find(Trim(Chiave), FoundPos + 1)
 '    RTBox.SetFocus
     If FoundPos = -1 Then
        testo = "Oggetto: " & RTBox.FileName & " - Riferimento Riga(" & Riga & ") NON Trovato" & vbCrLf
        FoundLine = Len(RTLog.Text)
        RTLog.SelStart = FoundLine
        RTLog.SelLength = 0
        RTLog.SelText = testo
        RTLog.Refresh
        TROVA_RIGA = False
        Exit Function
     End If

     If FoundPos <> -1 Then

       FoundLine = RTBox.GetLineFromChar(FoundPos) + 1
      
       If FoundLine = Riga Then
            TROVA_RIGA = True
            Exit Function
       End If

       If FoundLine > Riga Then
          testo = "Oggetto: " & RTBox.FileName & " - Riferimento Riga(" & Riga & ") NON Trovato" & vbCrLf
          FoundLine = Len(RTLog.Text)
          RTLog.SelStart = FoundLine
          RTLog.SelLength = 0
          RTLog.SelText = testo
          RTLog.Refresh
          TROVA_RIGA = False
          Exit Function
       End If
     End If
   Wend
End Function

Sub CREA_CBO_IN_TAB_OGGETTI(oldn As Variant, DtP As Variant)
    Dim rOgg As Recordset
    
    Set rOgg = DbPrj.OpenRecordset("SELECT * FROM Oggetti WHERE Tipo = 'CBO' AND Nome ='" & oldn & "'")
            If rOgg.RecordCount > 0 Then
                 rOgg.Edit
                    rOgg!Nome = oldn
                    rOgg!Tipo = "CBO"
                    rOgg!Directory = DirCBLcpy & "\Out"
                    rOgg!NumRighe = 0
                    rOgg!dtparsing = DtP
                    rOgg!dtimport = Now
                rOgg.Update
            Else
                If OldName = "" Then
                    Exit Sub
                End If
                rOgg.AddNew
                    rOgg!Nome = oldn
                    rOgg!Tipo = "CBO"
                    rOgg!Directory = DirCBLcpy & "\Out"
                    rOgg!NumRighe = 0
                    rOgg!dtparsing = DtP
                    rOgg!dtimport = Now
                rOgg.Update
            End If
End Sub

Sub CREA_CPO_IN_TAB_OGGETTI(oldn As Variant, DtP As Variant)
    Dim rOgg As Recordset
    
    Set rOgg = DbPrj.OpenRecordset("SELECT * FROM Oggetti WHERE Tipo = 'CPO' AND Nome ='" & oldn & "'")
            If rOgg.RecordCount > 0 Then
                 rOgg.Edit
                    rOgg!Nome = oldn
                    rOgg!Tipo = "CPO"
                    rOgg!Directory = DirCBLcpy & "\Out"
                    rOgg!NumRighe = 0
                    rOgg!dtparsing = DtP
                    rOgg!dtimport = Now
                rOgg.Update
            Else
                If OldName = "" Then
                    Exit Sub
                End If
                rOgg.AddNew
                    rOgg!Nome = oldn
                    rOgg!Tipo = "CPO"
                    rOgg!Directory = DirCBLcpy & "\Out"
                    rOgg!NumRighe = 0
                    rOgg!dtparsing = DtP
                    rOgg!dtimport = Now
                rOgg.Update
            End If
End Sub
