Attribute VB_Name = "MaimdM_Incapsulatore"
Option Explicit

''SQ merge Public NomeProg  As String
''SQ merge Public oldName  As String
''SQ merge Public oldName1 As String
''SQ merge Public wPunto As Boolean
Public Posizione_DC As Long
''SQ merge Public AddRighe As Long
''SQ merge Public nomeRoutine As String
Public wIdent As String

''SQ merge Public SwTp As Boolean
Public SwSend As Boolean

''SQ merge Public OpeIstrRt() As t_OpeIstr
Public CurCodiceOp As String
Global bolsungard As Boolean
Global TipoFileDC As String

Dim START_TAG As String, END_TAG As String
'SQ
Dim riga_GOBACK As Long

'ac ContainCHKP
Public Function ContainCHKP(pIdOggetto As Integer) As Boolean
  Dim rst As Recordset
  
  Set rst = m_fun.Open_Recordset("Select * from PSDli_Istruzioni where " & _
                                 "idoggetto = " & pIdOggetto & " and ImsDC = true and " & _
                                 "Istruzione = 'CHKP' order by riga")
  If Not rst.EOF Then
    ContainCHKP = True
  Else
    ContainCHKP = False
  End If
  rst.Close
End Function

Public Sub loadEnvironmentParameters_DC()
  START_TAG = Left(m_fun.LeggiParam("IMSDC-INCAPS-START-TAG") & Space(6), 6)
  If Len(START_TAG) > 6 Then
    START_TAG = Left(START_TAG, 6)
  ElseIf Len(START_TAG) < 6 Then
    START_TAG = START_TAG & Space(6 - Len(START_TAG))
  End If
  END_TAG = Left(m_fun.LeggiParam("IMSDC-INCAPS-END-TAG") & Space(8), 8)
  END_TAG = Trim(END_TAG)
End Sub

Public Function Seleziona_Istruzioni(Selezione As String, rIst As Recordset) As Long
  Dim criterio As String
  Dim K1 As Long
  
  Select Case Selezione
    Case "SRC" 'Tutti i programmi
      Set rIst = m_fun.Open_Recordset("Select idoggetto, Nome,ims  From BS_Oggetti Where Dli = true and tipo = 'CBL' Order By IdOggetto")
    
    Case "SEL" 'Solo i programmi Selezionati
      If m_ImsDll_DC.ImObjList.ListItems.Count = 0 Then
        Exit Function
      End If
      
      For K1 = 1 To m_ImsDll_DC.ImObjList.ListItems.Count
        If m_ImsDll_DC.ImObjList.ListItems(K1).Selected = True Then
          If Trim(criterio) <> "" Then
            criterio = criterio & ", " & Val(m_ImsDll_DC.ImObjList.ListItems(K1).text)
          Else
            criterio = Val(m_ImsDll_DC.ImObjList.ListItems(K1).text)
          End If
        End If
      Next K1
      
      criterio = " IdOggetto in (" & criterio & ") "
      
      Set rIst = m_fun.Open_Recordset("Select idoggetto,NOme,Ims From BS_Oggetti Where " & criterio & " And Dli = True Order By IdOggetto")
  End Select
  
  'Si posiziona al primo recordset
  If rIst.RecordCount Then
    rIst.MoveFirst
    Seleziona_Istruzioni = rIst.RecordCount
  Else
    Seleziona_Istruzioni = 0
  End If
  
End Function

Public Function Restituisci_Selezione_DC(TR As TreeView) As String
  Dim i As Long
  
  For i = 1 To TR.Nodes.Count
    If TR.Nodes(i).Checked = True Then
      Restituisci_Selezione_DC = Trim(UCase(TR.Nodes(i).key))
      Exit For
    End If
  Next i
End Function
Sub AsteriscaArraySpa(pArrFile() As String, pspalen As Integer)
  Dim Index As Integer, bolLink As Boolean, bolProc As Boolean
  Dim strcomarea As String
  bolLink = False
  bolProc = False
  Index = 1
  strcomarea = "BPHX-R 01  DFHCOMMAREA          PIC X(" & pspalen & ")."
  Do While (Index <= UBound(pArrFile)) And Not bolLink
    If InStr(8, pArrFile(Index), "LINKAGE SECTION") Then
      pArrFile(Index) = "BPHX-R*" & Right(pArrFile(Index), Len(pArrFile(Index)) - 7)
      bolLink = True
    Else
      Index = Index + 1
    End If
  Loop
  Index = Index + 1
  Do While (Index <= UBound(pArrFile)) And Not bolProc
   If InStr(8, pArrFile(Index), "PROCEDURE DIVISION") Then
    pArrFile(Index) = "BPHX-R LINKAGE SECTION." & Space$(49) & vbCrLf _
                        & "BPHX-R" & Space$(66) & vbCrLf & strcomarea & Space$(72 - Len(strcomarea)) & vbCrLf _
                        & "BPHX-R" & Space$(66) & vbCrLf _
                        & "BPHX-R*" & Right(pArrFile(Index), Len(pArrFile(Index)) - 7)
    If InStr(289, pArrFile(Index), ".") Then
      pArrFile(Index) = pArrFile(Index) & vbCrLf & "BPHX-R PROCEDURE DIVISION USING DFHCOMMAREA." _
                                         & Space$(28)
      bolProc = True
    Else
      Index = Index + 1
      While Not InStr(8, pArrFile(Index), ".") > 0
        pArrFile(Index) = "BPHX-R*" & Right(pArrFile(Index), Len(pArrFile(Index)) - 7)
        Index = Index + 1
      Wend
      ' ultima asterisco e attacco
      pArrFile(Index) = "BPHX-R*" & Right(pArrFile(Index), Len(pArrFile(Index)) - 7)
      pArrFile(Index) = pArrFile(Index) & vbCrLf & "BPHX-R PROCEDURE DIVISION USING DFHCOMMAREA." _
                                         & Space$(28)
      bolProc = True
    End If
   Else
    Index = Index + 1
   End If
  Loop
End Sub

Function isMain(IdOggetto As Long) As Boolean
  Dim tb As Recordset, caller As Long
  
  isMain = True
  Set tb = m_fun.Open_Recordset("SELECT IdOggettoC FROM PsRel_Obj WHERE IdoggettoR = " & IdOggetto)
  If Not tb.EOF Then
    caller = tb!idOggettoC
    tb.Close
    Set tb = m_fun.Open_Recordset("SELECT IdOggetto FROM Bs_Oggetti WHERE " & _
                                  "IdOggetto = " & caller & " and Tipo not in ('JCL','PRC','MBR')")
    If Not tb.EOF Then
      isMain = False
    End If
  End If
  tb.Close
End Function

Sub AsteriscaArraySunga(pArrFile() As String)
  Dim Index As Integer, bolLink As Boolean, bolProc As Boolean, bolgoback As Boolean
  Dim appostr As String, endtag As String
  'stefano: sicuramente � una lettura che si pu� evitare, prendendo l'informazione da fuori
  Dim tb As Recordset
  Dim bolDC As Boolean

  Index = 1

  Set tb = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & GbIdOggetto)
  'gestire eventuale errore e togliere boldc
  If Not tb.EOF Then
    bolDC = tb!ims
  End If
  tb.Close
  
  If Not bolDC And Not isMain(GbIdOggetto) Then  ' se batch e programma chiamato esco e non asterisco linkage
    Exit Sub
  End If
  Do While (Index <= UBound(pArrFile)) And Not bolLink
    If InStr(8, pArrFile(Index), "LINKAGE SECTION") And Not Mid(pArrFile(Index), 7, 1) = "*" Then
      'pArrFile(Index) = "BPHX-R*" & Right(pArrFile(Index), Len(pArrFile(Index)) - 7)
      'non capivo a cosa serviva...
      If Len(pArrFile(Index)) > 72 Then
        endtag = Right(pArrFile(Index), Len(pArrFile(Index)) - 72)
      Else
        endtag = ""
      End If
      pArrFile(Index) = Left(pArrFile(Index), 72)
      pArrFile(Index) = FormatTag(Right(pArrFile(Index), Len(pArrFile(Index)) - 7), endtag)
      'stefano: attenzione, formattag aggiungeva una riga, per ora la tolgo...
      pArrFile(Index) = Replace(pArrFile(Index), vbCrLf, "")
      pArrFile(Index) = Left(pArrFile(Index), 6) & "*" & Right(pArrFile(Index), Len(pArrFile(Index)) - 7)
      bolLink = True
    Else
      Index = Index + 1
    End If
  Loop
  'stefano
  If Not bolDC Then Exit Sub

  Index = Index + 1
  
  Do While (Index <= UBound(pArrFile)) And Not bolProc
   If InStr(8, pArrFile(Index), "PROCEDURE DIVISION") Then
    pArrFile(Index) = FormatTag("LINKAGE SECTION.") & FormatTag("") _
                    & FormatTag("01  DFHCOMMAREA.") _
                    & FormatTag("    COPY DCSOCK.") & FormatTag("") _
                    & pArrFile(Index)
    'stefano: sistema vecchio, ma mi tocca usarlo...
    AddRighe = AddRighe + 5
    bolProc = True
                        
  '  If InStr(289, pArrFile(Index), ".") Then
  '    pArrFile(Index) = pArrFile(Index) & vbCrLf & "BPHX-R PROCEDURE DIVISION USING DFHCOMMAREA." _
  '                                       & Space$(28)
  '    bolProc = True
  '  Else
  '    Index = Index + 1
  '    While Not InStr(8, pArrFile(Index), ".") > 0
  '      pArrFile(Index) = "BPHX-R*" & Right(pArrFile(Index), Len(pArrFile(Index)) - 7)
  '      Index = Index + 1
  '    Wend
  '    ' ultima asterisco e attacco
  '    pArrFile(Index) = "BPHX-R*" & Right(pArrFile(Index), Len(pArrFile(Index)) - 7)
  '    pArrFile(Index) = pArrFile(Index) & vbCrLf & "BPHX-R PROCEDURE DIVISION USING DFHCOMMAREA." _
  '                                       & Space$(28)
  '    bolProc = True
  '  End If
   Else
    Index = Index + 1
   End If
  Loop
  
  Do While (Index <= UBound(pArrFile)) And Not bolgoback
   If InStr(8, pArrFile(Index), "GOBACK") Then
    If Len(pArrFile(Index)) > 72 Then
      endtag = Right(pArrFile(Index), Len(pArrFile(Index)) - 72)
    Else
      endtag = ""
    End If
    pArrFile(Index) = Left(pArrFile(Index), 72)
    appostr = FormatTag(Right(pArrFile(Index), Len(pArrFile(Index)) - 7), endtag)
    pArrFile(Index) = Left(appostr, 6) & "*" & Right(appostr, Len(appostr) - 7) _
    & FormatTag("") & FormatTag("    COPY DCEXIT.") & FormatTag("")
    'stefano: sistema vecchio, ma mi tocca usarlo...
    AddRighe = AddRighe + 4
    bolgoback = True
   Else
    Index = Index + 1
   End If
  Loop
End Sub

Public Function Controlla_CBL_CPY_DC(RT As RichTextBox, Record As Recordset, typeIncaps As String) As String
  '2) Controlla se l'istruzione Corrente fa parte Del CBL o la CPY gi� aperta, se si non carica il CBL o CPY
  'ma li aggiorna:
  Dim NCBLcpy As String
  Dim Tip As String
  Dim k As Integer
  Dim DirCBLcpy As String
  Dim dirOr As String
  Dim wstr As String
  Dim pos4 As Long
  Dim w As Long
  Dim ww As Long
  Dim Percorso As String, noinstruct As Boolean, isDc As Boolean
  
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("SELECT * FROM PsDli_Istruzioni WHERE Idoggetto = " & Record!IdOggetto & " and ImsDc")
  If r.EOF Then
    noinstruct = True
  End If
  r.Close
  'AC 14/7/06
  
  Set r = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & Record!IdOggetto)
  If r.RecordCount > 0 Then
    TipoFileDC = r!tipo 'IRIS-DB
    If TipoFileDC = "CPY" Then TipoFileDC = "CBL" 'IRIS-DB
    If TipoFileDC = "INC" Then TipoFileDC = "PLI" 'IRIS-DB
    isDc = r!ims
    If Trim(r!estensione) = "" Then
       NCBLcpy = r!nome
       NomeProg = r!nome
    Else
       NCBLcpy = r!nome & "." & r!estensione
       NomeProg = r!nome & "." & r!estensione
    End If
    
    k = InStr(NomeProg, ".")
    
    If k > 0 Then NomeProg = Trim(Mid$(NomeProg, 1, k - 1))
    
    DirCBLcpy = m_fun.Crea_Directory_Progetto(r!Directory_Input, m_ImsDll_DC.ImPathDef)
    
    w = InStr(1, DirCBLcpy, "\")
    
    While w > 0
      ww = w
      w = InStr(w + 1, DirCBLcpy, "\")
    Wend
    
    'qui ottengo la directory di origine del file
    dirOr = Mid(DirCBLcpy, ww + 1)
    '**************  nuovo percorso  Incapsulator/Nome_directory_origine_programma
    wstr = m_ImsDll_DC.ImNomeDB
    pos4 = InStr(wstr, ".")
    If pos4 > 0 Then wstr = Mid$(wstr, 1, pos4 - 1)
    
    Percorso = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\incapsulator"

  If Dir(Percorso, vbDirectory) = "" Then MkDir Percorso
    Percorso = Percorso & "\" & dirOr
    If Dir(Percorso, vbDirectory) = "" Then MkDir Percorso
        
    '********************************
    Tip = r!tipo
    'stefano: mah!, che casino; che senso ha??
    'SQ: PROVA!
    RT.text = "" 'Ac lo tolgo
    
    If RT.text = "" Then
      If MaimdF_Incapsulatore.Optsource Then
        RT.LoadFile (DirCBLcpy & "\" & NCBLcpy)
        Controlla_CBL_CPY_DC = DirCBLcpy & "\" & NCBLcpy
      Else
        
      End If
    Else
      If RT.FileName <> DirCBLcpy & "\" & NCBLcpy Then
        'Salva il file
        Crea_Directory_Cbl_Cpy (DirCBLcpy) 'crea le directory CBLOut e CPYOut se non esistono

        RT.SaveFile OldName, 1

        If Tip = "CBL" Then
          'inserisce i CBL modificate anche nella tabella oggetti del DB

        Else
          'inserisce le CPY modificate anche nella tabella oggetti del DB

        End If

        If MaimdF_Incapsulatore.Optsource Then
          RT.LoadFile (DirCBLcpy & "\" & NCBLcpy)
          Controlla_CBL_CPY_DC = DirCBLcpy & "\" & NCBLcpy
        End If
      End If
    End If
    If Dir(DirCBLcpy & "\Out", vbDirectory) = "" Then
      MkDir DirCBLcpy & "\Out"
    End If
    
   
    'oldName = DirCBLcpy & "\Out\" & NCBLcpy
    OldName = Percorso & "\" & NCBLcpy
    OldName1 = NCBLcpy
    On Error GoTo err
    If Not MaimdF_Incapsulatore.Optsource Then
      RT.LoadFile (OldName)
      Controlla_CBL_CPY_DC = OldName
    End If
    If r!tipo <> "CPY" Then
      If Not noinstruct Or isDc Then
        Inizializza_Copy_IMSDC RT
      End If
    End If
   End If
   Exit Function
err:
   MsgBox "Incapsulated version of file not found", vbCritical, "File not found"
End Function

Public Sub Inizializza_Copy_IMSDC(RT As RichTextBox)
    'controlla se il link con la copy LNKDBRT � presente nel file
    Dim t, w As Long
    Dim Testo As String
    Dim s() As String
    Dim nfile As Long
    Dim lFile As Long
    Dim sLine As String
    
    ReDim CmpPcb(0)
    
    t = RT.find("COPY LNKDCRT", 1)
    RT.Refresh
    'stefano: attenzione!!!!!!!!!! per Sungard funziona per altri no!!!!
    'AddRighe = 0
    
    If t = -1 Then
        w = RT.find(" WORKING-STORAGE ", 1)
        For t = w To 1 Step -1
          If Mid$(RT.text, t, 2) = vbCrLf Then
             If Mid$(RT.text, t + 8, 1) = "*" Then
                w = RT.find(" WORKING-STORAGE ", w + 1)
             End If
             
             Exit For
          End If
        Next t
        
        w = RT.find(vbCrLf, w)
        w = w + 1
        RT.SelStart = w
        RT.SelLength = 0
        
        'Apre il template e lo incolla
        If Dir(m_fun.FnPathPrj & "\input-prj\Template\imsdc\standard\incaps\InitCpyRoutIMSDC.tpl", vbNormal) <> "" Then
            nfile = FreeFile
            lFile = FileLen(m_fun.FnPathPrj & "\input-prj\Template\imsdc\standard\incaps\InitCpyRoutIMSDC.tpl")
            
            Open m_fun.FnPathPrj & "\input-prj\Template\imsdc\standard\incaps\InitCpyRoutIMSDC.tpl" For Binary As nfile
            
'IRIS-DB            Testo = vbCrLf
            Testo = "" 'IRIS-DB
            
            While Loc(nfile) < lFile
                Line Input #nfile, sLine
                
                'Trascodifica le label
                If InStr(1, sLine, "#LABEL_AUTHOR#") > 0 Then
                    sLine = Replace(sLine, "#LABEL_AUTHOR#", m_fun.LABEL_AUTHOR)
                    Testo = Testo & vbCrLf & sLine
                    
                ElseIf InStr(1, sLine, "#LABEL_INFO#") > 0 Then
                    sLine = Replace(sLine, "#LABEL_INFO#", m_fun.LABEL_INFO)
                    Testo = Testo & vbCrLf & sLine
                    
                ElseIf InStr(1, sLine, "#LABEL_INCAPS#") > 0 Then
                    sLine = Replace(sLine, "#LABEL_INCAPS#", m_fun.LABEL_INCAPS)
                    Testo = Testo & vbCrLf & sLine
                Else
                    Testo = Testo & LABEL_INFO & vbCrLf & sLine
                End If
                
            Wend
            
            Close nfile
        Else
            Testo = vbCrLf
            Testo = Testo & LABEL_INFO & "*------------------------------------------------------" & vbCrLf
            Testo = Testo & LABEL_INFO & "*         MISSING TEMPLATE : InitCpyRoutIMSDC.tpl " & vbCrLf
            Testo = Testo & LABEL_INFO & "*------------------------------------------------------" & vbCrLf
        End If
        
        RT.SelText = Testo
        
        s = Split(Testo, vbCrLf)
        'stefano: l'ho dovuto spostare in basso per sungard, per cui devo fare cos�; ora per Alenia
        ' non funziona
        AddRighe = AddRighe + UBound(s) + 1 'IRIS-DB
    End If
End Sub

Public Function AsteriscaIstruzione_DC(totoffset As Long, rst As Recordset, RTBModifica As RichTextBox, typeIncaps As String, pCallKey As String) As Long
   Dim NumRighe As Long
   Dim pstart As Variant
   Dim pend As Variant
   Dim pPos As Variant
   Dim wPosizione As Long
   Dim flag As Boolean
   Dim wResp As Boolean
   Dim tb As Recordset
   Dim wVbCrLf As String
   Dim wIdSegm As Variant
   Dim oldNomeRout As String
   Dim wTipoCall As String
   Dim wstr As String
   Dim k As Integer
   Dim pCount As Long, offsetendtag As Integer, lenline As Integer
   
  'SQ - controllo fuori!
   If pCallKey = "" Then
    Exit Function
   End If
   
   oldNomeRout = nomeRoutine
  
   wResp = CaricaIstruzione_DC(rst, RTBModifica, typeIncaps)

   nomeRoutine = ""
   
   If UBound(TabIstr_DC.BlkIstr) > 0 Then
     nomeRoutine = TabIstr_DC.BlkIstr(0).nomeRout
      
     'aggiorna il campo nome routine
     rst!nomeRout = nomeRoutine
     rst.Update
   End If
   
   If nomeRoutine = "" Then
     nomeRoutine = oldNomeRout
   End If
   
   'AC 16/01/07 introdotto offset
   NumRighe = rst!Riga + AddRighe + totoffset
   
   ' AC wTipoCall = "CBLTDLI"
   wTipoCall = pCallKey
   
   wResp = TROVA_RIGA(RTBModifica, wTipoCall, NumRighe, RTBModifica.SelStart)
   
   'IRIS-DB modificata la gestione di wIdent (che poi si doveva chiamare al limite wIndent), perch� totalmente insensata
   'IRIS-DB per ora cerca solo la CALL CBLTDLI su una sola riga
   If wResp Then
     pPos = RTBModifica.SelStart
     'IRIS-DB wIdent = 0
     
     For pstart = pPos To pPos - 160 Step -1
       'IRIS-DB wIdent = wIdent + 1
       RTBModifica.SelStart = pstart
       RTBModifica.SelLength = 2
       
       If RTBModifica.SelText = vbCrLf Then
         Exit For
       End If
       
     Next pstart
     'IRIS-DB wIdent = wIdent - 1 - Len(wTipoCall)
     'IRIS-DB If wIdent < 12 Then   'SQ a Virgilio?!
     'IRIS-DB   wIdent = wIdent + (11 - wIdent) 'kansas city
     'IRIS-DB End If
     wIdent = 0
     pPos = RTBModifica.find("CALL", pstart)
     If pPos < 12 And TipoFile = "CBL" Then 'IRIS-DB
       wIdent = 11 'IRIS-DB per ora non gestiamo eccezioni
     Else
       wIdent = pPos - pstart - 1
     End If
     
     If TipoFileDC = "PLI" Then
       RTBModifica.SelStart = pstart + 1
       pstart = pstart + 2
       RTBModifica.SelLength = 0
     Else
       RTBModifica.SelStart = pstart + 1
       pstart = pstart + 2
       RTBModifica.SelLength = 7
     End If
     
     If rst!valida = True Then
       If TipoFileDC = "PLI" Then
         RTBModifica.SelText = " /* " & START_TAG & " START ------------------------*" & vbCrLf
       Else
         RTBModifica.SelText = START_TAG & "*"
       End If
     Else
       If TipoFileDC = "PLI" Then
         RTBModifica.SelText = " /* " & m_fun.LABEL_NONDECODE & " START ------------------------*" & vbCrLf
       Else
         RTBModifica.SelText = m_fun.LABEL_NONDECODE & "*"
       End If
     End If

     pend = RTBModifica.find(vbCrLf, pstart + 1)
    'SQ 27-07-06
     lenline = pend - pstart + 1
     
     If wTipoCall = " EXEC " Or TipoFileDC = "PLI" Then 'IRIS-DB
       If TipoFileDC = "PLI" Then
         pend = RTBModifica.find(";", pPos + 1)
         pend = RTBModifica.find(vbCrLf, pend + 1)
       Else
         pend = RTBModifica.find("END-EXEC", pPos + 1, , rtfWholeWord)
         pend = RTBModifica.find(vbCrLf, pend + 1)
       End If
     Else
       flag = True
       pend = pstart + 1
       pend = RTBModifica.find(vbCrLf, pPos + 1)
      '''''''''''''''''''''''''''''''''''''''''''''''
      'SQ 27-07-06
      ' Gestione TAG a colonna 72
      ' Se non valorizzato => rimane l'eventuale originale TAG
      '''''''''''''''''''''''''''''''''''''''''''''''
      'IRIS-DB per ora non lo usa sul DC END_TAG = "LN" & rst!Riga 'IRIS-DB
       If Len(Trim(END_TAG)) Then
         If lenline >= 72 Then
           RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
           'RTBModifica.SelLength = 8
           RTBModifica.SelLength = (lenline - 72)
           RTBModifica.SelText = END_TAG
           offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
         Else
           RTBModifica.SelLength = 0
           RTBModifica.SelText = Space(72 - lenline) & END_TAG
         End If
       End If
       wstr = Mid$(RTBModifica.text, pPos, pend - pPos + 1)
       If InStr(wstr, ".") Then
          flag = False
       End If
       pPos = pend + 1
       While flag = True
         pend = RTBModifica.find(vbCrLf, pPos + 1)
         wstr = Mid$(RTBModifica.text, pPos + 2, pend - pPos - 1)
         'stefano: due cose: non funziona quando si scrive prima di colonna 12, cosa possibile
         ' con il nuovo cobol e non gestisce le righe asteriscate: lo correggo.
         'If Len(wStr) > 11 Then wStr = Trim(Mid$(wStr, 12, Len(wStr) - 11))
         If Len(wstr) > 7 Then
          If Mid(wstr, 7, 1) <> "*" Then
           If Len(wstr) > 7 Then wstr = Trim(Mid$(wstr, 8, Len(wstr) - 7))
         'IRIS-DB: da rivedere, non funziona se il punto � su una istruzione successiva alla CALL senza punto, lo includerebbe nella CALL...
            If InStr(wstr, ".") > 0 Then
               flag = False
               'IRIS-DB start
               k = InStr(wstr, " ")
               If k > 0 Then wstr = Trim(Mid$(wstr, 1, k - 1))
               If m_fun.SeWordCobol(Replace(wstr, ".", "")) Then 'IRIS-DB it should use the line number in Ps_call.... ho messo la replace per ora
                  flag = False
                  pend = pPos - 1
               End If
               'If m_fun.SeWordCobol(Trim(Left(wstr, Len(wstr) - 1))) Then 'IRIS-DB it should use the line number in Ps_call....
               '   pend = pPos - 1
               'End If
               'IRIS-DB end
            Else
               k = InStr(wstr, " ")
               If k > 0 Then wstr = Trim(Mid$(wstr, 1, k - 1))
               'IRIS-DB If m_fun.SeWordCobol(wstr) Then 'IRIS-DB it should use the line number in Ps_call....
               If m_fun.SeWordCobol(wstr) And wstr <> "END-CALL" Then 'IRIS-DB pezza
                  flag = False
                  pend = pPos - 1
               End If
            End If
           End If
          End If
          pPos = pend + 1
       Wend
     End If
     
     If TipoFileDC = "PLI" Then 'IRIS-DB
       RTBModifica.SelStart = pend + 2
       RTBModifica.SelLength = 0
       RTBModifica.SelText = " * ---------------------------------------*/" & vbCrLf
       pPos = RTBModifica.find(vbCrLf, pend + 2)
       AsteriscaIstruzione_DC = pPos + 2
       AddRighe = AddRighe + 2 'IRIS-DB: aggiunge le righe di commento
     Else
     
         wPosizione = pend + 1
         pend = pend - 2
         'pPos = pStart
         pPos = pstart + 1
         flag = True
         'IRIS-DB While flag
         Do While flag 'IRIS-DB
           pPos = RTBModifica.find(vbCrLf, pPos + 1, pend)
        
           If pPos = -1 Then
             flag = False
           End If
           If flag Then
           'IRIS-DB  RTBModifica.SelStart = pPos + 2
           'IRIS-DB RTBModifica.SelLength = 7
           'IRIS-DB  If Trim(RTBModifica.SelText) = vbCrLf Then
           'IRIS-DB     wVbCrLf = vbCrLf & Space(5)
           'IRIS-DB     pCount = pCount + 5
           'IRIS-DB Else
           'IRIS-DB     wVbCrLf = ""
           'IRIS-DB End If
             Dim irisPos As Variant
             irisPos = RTBModifica.find(vbCrLf, pPos + 2) 'IRIS-DB
             RTBModifica.SelStart = pPos + 2 'IRIS-DB
             irisPos = irisPos - (pPos + 2) 'IRIS-DB
             If irisPos < 7 Then        'IRIS-DB
                'IRIS-DB Exit Do 'IRIS-DB
                RTBModifica.SelLength = irisPos      'IRIS-DB
                RTBModifica.SelText = "" 'IRIS-DB
                pCount = 7 - irisPos
                RTBModifica.SelLength = 0      'IRIS-DB
             Else                                      'IRIS-DB
                RTBModifica.SelLength = 7 'IRIS-DB
             End If                                    'IRIS-DB
             wVbCrLf = ""                           'IRIS-DB
                   
             If Mid(RTBModifica.SelText, 7, 1) <> "*" Then
                If rst!valida = True Then
                    RTBModifica.SelText = START_TAG & "*" & wVbCrLf
                Else
                    RTBModifica.SelText = m_fun.LABEL_NONDECODE & "*" & wVbCrLf
                End If
                '''''''''''''''''''''''''''''''''''''''''''''''
                'SQ 27-07-06
                ' Gestione TAG a colonna 72
                '''''''''''''''''''''''''''''''''''''''''''''''
                If Len(Trim(END_TAG)) Then
                  lenline = RTBModifica.find(vbCrLf, pPos + 1) - pPos - 2
                  If lenline >= 72 Then
                    RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
                    RTBModifica.SelLength = lenline - 72
                    RTBModifica.SelText = END_TAG
                    offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
                  Else
                    RTBModifica.SelLength = 0
                    RTBModifica.SelText = Space(72 - lenline) & END_TAG
                  End If
                End If
             End If
           End If
          
         'IRIS-DB Wend
         Loop 'IRIS-DB
        If offsetendtag >= 2 Then
          offsetendtag = offsetendtag - 2
        End If
        If offsetendtag <= 0 Then
          offsetendtag = 0
        End If
        AsteriscaIstruzione_DC = wPosizione + pCount + offsetendtag
    End If
  Else
    AsteriscaIstruzione_DC = 0
  End If
End Function

Public Function AsteriscaEntry(totoffset As Long, Riga As Long, RTBModifica As RichTextBox, typeIncaps As String, pCallKey As String) As Long
  Dim NumRighe As Long
  Dim pstart As Variant
  Dim pend As Variant
  Dim pPos As Variant
  Dim wPosizione As Long
  Dim flag As Boolean
  Dim wResp As Boolean
  Dim tb As Recordset
  Dim wVbCrLf As String
  Dim wIdSegm As Variant
  Dim oldNomeRout As String
  Dim wTipoCall As String
  Dim wstr As String
  Dim k As Integer
  Dim pCount As Long, offsetendtag As Integer, lenline As Integer, bolDC As Boolean, tbIMS As Recordset
   
  'SQ - 'sti controlli vanno fatti nel chiamante!
  If pCallKey = "" Then
    Exit Function
  End If
  
  'SQ - lo sappiamo gi� se � DC... recordSet principale...
  Set tbIMS = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & GbIdOggetto)
  'gestire eventuale errore e togliere boldc
  If Not tbIMS.EOF Then
    bolDC = tbIMS!ims
  End If
  tbIMS.Close
  
  Set tbIMS = m_fun.Open_Recordset("SELECT r.IdOggettoC FROM PsRel_Obj as r,Bs_Oggetti as o WHERE r.IdoggettoR = " & GbIdOggetto & " and r.Utilizzo = 'CALL' and o.IdOggetto = r.IdOggettoC and o.tipo = 'CBL'")
  If Not tbIMS.EOF Then
    AsteriscaEntry = 1
    Exit Function
  End If
   
  'AC 16/01/07 introdotto offset
  NumRighe = Riga + AddRighe + totoffset
  'SQ - NON SI PUO' METTERE 4! APPENA UNO CAMBIA QUALSIASI COSA NON FUNZIONA PIU'!!!!!!!!!!!!
  'If bolDC Then NumRighe = NumRighe - 4   ' ho asteriscato goback in anticipo 'And bolsungard
  ' AC wTipoCall = "CBLTDLI"
  wTipoCall = pCallKey
   
  'SQ - perch� LA DEVE CERCARE LA RIGA?
  wResp = TROVA_RIGA(RTBModifica, wTipoCall, NumRighe, RTBModifica.SelStart)
   
  If wResp Then
    pPos = RTBModifica.SelStart
    wIdent = 0
   
    For pstart = pPos To pPos - 160 Step -1
      wIdent = wIdent + 1
      RTBModifica.SelStart = pstart
      RTBModifica.SelLength = 2
      If RTBModifica.SelText = vbCrLf Then
        Exit For
      End If
    Next pstart
    wIdent = wIdent - 1 - Len(wTipoCall)
   
    'SQ
    'RTBModifica.SelStart = pStart + 1
    pstart = pstart + 1
    RTBModifica.SelStart = pstart
    RTBModifica.SelLength = 7
    RTBModifica.SelText = START_TAG & "*"
    'SQ
    'pEnd = RTBModifica.Find(vbCrLf, pStart + 1)
    pend = RTBModifica.find(vbCrLf, pstart)
    'SQ 27-07-06
    lenline = pend - pstart + 1
     
    If wTipoCall = " EXEC " Then 'SQ? SONO NELLA ENTRY?!
      pend = RTBModifica.find("END-EXEC", pPos + 1, , rtfWholeWord)
      pend = RTBModifica.find(vbCrLf, pend + 1)
    Else
      flag = True
      'SQ pEnd = pStart + 1 '?!
      'SQ pEnd = RTBModifica.Find(vbCrLf, pPos + 1)  'SQ ancora?!
      '''''''''''''''''''''''''''''''''''''''''''''''
      'SQ 27-07-06
      ' Gestione TAG a colonna 72
      ' Se non valorizzato => rimane l'eventuale originale TAG
      '''''''''''''''''''''''''''''''''''''''''''''''
      If Len(Trim(END_TAG)) Then
        If lenline >= 72 Then
          RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
          'RTBModifica.SelLength = 8
          RTBModifica.SelLength = (lenline - 72)
          RTBModifica.SelText = END_TAG
          offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
        Else
         RTBModifica.SelLength = 0
         RTBModifica.SelText = Space(72 - lenline) & END_TAG
        End If
      End If
      'pPos: inizio ENTRY ; pEnd: fine riga
      wstr = Mid(RTBModifica.text, pPos, pend - pPos + 1) 'SQ resto della riga... da ENTRY in poi...(?)
      If InStr(wstr, ".") Then 'SQ ???
        flag = False
      End If
      'SQ
      'pPos = pEnd + 1
      pPos = pend + 2 'Nuova riga
      
      'SQ questo cicletto cos'�? Serve per settare pPos e pEnd finali!
      While flag
        pend = RTBModifica.find(vbCrLf, pPos + 1)
        wstr = Mid(RTBModifica.text, pPos + 2, pend - pPos - 1)
        If Len(wstr) > 11 Then wstr = Trim(Mid(wstr, 12, Len(wstr) - 11))
        If InStr(wstr, ".") > 0 Then
           flag = False
        Else
           k = InStr(wstr, " ")
           If k > 0 Then wstr = Trim(Left(wstr, k - 1))
           If m_fun.SeWordCobol(wstr) Then
              flag = False
              pend = pPos - 1
           End If
        End If
        pPos = pend + 1
      Wend
    End If
     
    'stefano: se la riga � una sola????
    ' ma perch� lo rifa, lo ha fatto due volte!!!!
     wPosizione = pend + 1
     pend = pend - 2
     pPos = pstart  'SQ: inizio prima linea
     'SQ pPos = pStart + 1  'SQ????????????????????????????????
     flag = True
     While flag
      'SQ
      'pPos = RTBModifica.Find(vbCrLf, pPos + 1, pEnd)
      pPos = RTBModifica.find(vbCrLf, pPos, pend)
    
       If pPos = -1 Then
         flag = False
       End If
       If flag Then
         RTBModifica.SelStart = pPos + 2  'SQ riga successiva
         RTBModifica.SelLength = 7
         
         
         If Trim(RTBModifica.SelText) = vbCrLf Then
            wVbCrLf = vbCrLf & Space(5)
            pCount = pCount + 5
         Else
            wVbCrLf = ""
         End If
            
         If Mid(RTBModifica.SelText, 7, 1) <> "*" Then
          RTBModifica.SelText = START_TAG & "*" & wVbCrLf
          '''''''''''''''''''''''''''''''''''''''''''''''
          'SQ 27-07-06
          ' Gestione TAG a colonna 72
          '''''''''''''''''''''''''''''''''''''''''''''''
          If Len(Trim(END_TAG)) Then
            lenline = RTBModifica.find(vbCrLf, pPos + 1) - pPos - 2
            If lenline >= 72 Then
              RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
              RTBModifica.SelLength = lenline - 72
              RTBModifica.SelText = END_TAG
              offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
            Else
              RTBModifica.SelLength = 0
              RTBModifica.SelText = Space(72 - lenline) & END_TAG
            End If
          End If
         End If
       End If
       'SQ
       pPos = pPos + 2  'Nuova riga
    Wend
    
    If offsetendtag >= 2 Then
      offsetendtag = offsetendtag - 2
    End If
    If offsetendtag <= 0 Then
      offsetendtag = 0
    End If
    AsteriscaEntry = wPosizione + pCount + offsetendtag
  Else
    AsteriscaEntry = 0
  End If
End Function

Function CaricaIstruzione_DC(rst As Recordset, RTBModifica As RichTextBox, typeIncaps As String) As Boolean
  Dim tb As Recordset
  Dim tb1 As Recordset
  Dim tb2 As Recordset
  Dim tb3 As Recordset
  
  Dim IndI As Integer
  Dim k As Integer
  Dim K1 As Integer
  Dim k2 As Integer
  Dim k3 As Integer
  
  Dim u As Long
  Dim wXDField As Boolean
  
  Dim vEnd As Double
  Dim vPos As Double
  Dim vPos1 As Double
  Dim vStart As Double
  Dim wstr As String
  Dim TbLenField As Recordset
  
  Dim sDbd() As Long
  Dim sPsb() As Long
  Dim rsApp As Recordset
  
  Dim TbKey As Recordset
  
  ReDim TabIstr_DC.BlkIstr(0)
  ReDim TabIstr_DC.DBD(0)
  ReDim TabIstr_DC.psb(0)
  ReDim TabIstr_DC.SavedAreas(0)

  SwTp = Is_CICS(rst!IdOggetto)

  
'  Set tb = m_fun.Open_Recordset("select * from PsDli_IstrDett_Liv where idoggetto = " & rst!IdOggetto _
'              & " and riga = " & rst!Riga _
'              & " order by livello")
'
  '''''''''''''''DEVE CARICARE PI� DBD e pi�PSB
  'stefano: questo non serve a niente per il DC
  sDbd = getDBDByIdOggettoRiga(rst!idpgm, rst!IdOggetto, rst!Riga)
  sPsb = getPSBByIdOggettoRiga(rst!idpgm, rst!IdOggetto, rst!Riga)
  
  For u = 1 To UBound(sDbd)
    ReDim Preserve TabIstr_DC.DBD(u)
    TabIstr_DC.DBD(u).Id = sDbd(u)
    TabIstr_DC.DBD(u).nome = m_fun.Restituisci_NomeOgg_Da_IdOggetto(sDbd(u))
    TabIstr_DC.DBD(u).tipo = TYPE_DBD
  Next u
    
  For u = 1 To UBound(sPsb)
    ReDim Preserve TabIstr_DC.psb(u)
    TabIstr_DC.psb(u).Id = sPsb(u)
    TabIstr_DC.psb(u).nome = m_fun.Restituisci_NomeOgg_Da_IdOggetto(sPsb(u))
    TabIstr_DC.psb(u).tipo = TYPE_PSB
  Next u
  
  TabIstr_DC.pcb = Trim(rst!numpcb & " ")
  
  TabIstr_DC.istr = Trim(rst!Istruzione)

  TabIstr_DC.keyfeedback = ""
 
  'stefano: gi� fatto qualche riga sopra, o no???
  SwTp = m_fun.Is_CICS(rst!IdOggetto)
       
  TabIstr_DC.BlkIstr(IndI).nomeRout = Create_NomeRoutine_IMSDC(TabIstr_DC.istr)
  
  'SG: Deve accedere a liv selezionando i record della checkpoint con ssaname = <none>
  '...DA FARE...
  '...
  Set rsApp = m_fun.Open_Recordset("Select * From PsDli_Istrdett_Liv Where IdOggetto = " & rst!IdOggetto & " And Riga = " & rst!Riga & " And (NomeSSA = '' or NomeSSA is null) order by Livello")
  
  If rsApp.RecordCount > 0 Then
      
      ' AC lo farei per tutti
      'If TabIstr_DC.Istr = "CHKP" Or TabIstr_DC.Istr = "REST" Or TabIstr_DC.Istr = "XRST" Then
       
      While Not rsApp.EOF
         ReDim Preserve TabIstr_DC.SavedAreas(UBound(TabIstr_DC.SavedAreas) + 1)
         TabIstr_DC.SavedAreas(UBound(TabIstr_DC.SavedAreas)) = rsApp!DataArea
         
         rsApp.MoveNext
      Wend
      'End If
      
  End If
  
  rsApp.Close
  CaricaIstruzione_DC = True

End Function

Public Function TROVA_RIGA(RTBox As RichTextBox, Chiave As String, Riga As Long, Optional ByVal StartFind As Long = 0) As Boolean
   Dim FoundPos As Long
   Dim FoundLine As Long
   Dim flag As String
   Dim Testo As String
   
   FoundPos = StartFind
   flag = True
   
   While flag
    'SQ non posso cercare solo "ENTRY"!!!!!!!!!!!!!! senza neanche spazi!
     FoundPos = RTBox.find(Trim(Chiave), FoundPos + 1)
'     RTBox.SetFocus
     If FoundPos = -1 Then
     'IRIS-DB se non trovato, cerca con doppio apice visto che il parser carica in tabella il doppio apice come singolo apice,
     '   cambiando di fatto il vero contenuto del sorgente, bah
        FoundPos = RTBox.find(Trim(Replace(Chiave, "'", """")), FoundPos + 1)
        If FoundPos = -1 Then
          flag = False
        Else
          Chiave = Replace(Chiave, "'", """")
        End If
     End If

     If FoundPos <> -1 Then
       FoundLine = RTBox.GetLineFromChar(FoundPos) + 1
       
       If FoundLine = Riga Then
          TROVA_RIGA = True
          Exit Function
       End If
       
       If FoundLine > Riga Then
          flag = False
       End If
     End If
   Wend

   FoundPos = 0
   flag = True
   
   If Chiave = " EXEC " Then
     Chiave = "CBLTDLI"
   Else
     Chiave = " EXEC "
   End If
   
   While flag
     FoundPos = RTBox.find(Trim(Chiave), FoundPos + 1)
 
     If FoundPos = -1 Then
        TROVA_RIGA = False
        Exit Function
     End If

     If FoundPos <> -1 Then

       FoundLine = RTBox.GetLineFromChar(FoundPos) + 1
      
       If FoundLine = Riga Then
            TROVA_RIGA = True
            Exit Function
       End If

       If FoundLine > Riga Then
          TROVA_RIGA = False
          Exit Function
       End If
     End If
   Wend
End Function
Public Function FormatTag(Line As String, Optional pendtag As String = "") As String
  Dim lenline As Integer
  
  lenline = Len(Line)
  If pendtag = "" Then
    Line = START_TAG & " " & Line & Space(65 - lenline) & END_TAG & vbCrLf
  Else
    Line = START_TAG & " " & Line & Space(65 - lenline) & pendtag & vbCrLf
  End If
  FormatTag = Line
End Function


Public Function CountIris(RTBox As RichTextBox, Chiave As String, Riga As Long, Optional ByVal StartFind As Long = 0) As Integer
   Dim FoundPos As Long
   Dim FoundLine As Long
   Dim flag As String
   Dim Testo As String
   
   FoundPos = StartFind
   flag = True
   CountIris = 0
   
   While flag
'     FoundPos = RTBox.find(Trim(Chiave), FoundPos + 1)
     FoundPos = RTBox.find(Chiave, FoundPos + 1)
     If FoundPos = -1 Then
        flag = False
     End If

     If FoundPos <> -1 Then
       FoundLine = RTBox.GetLineFromChar(FoundPos) + 1
       If FoundLine < Riga Then
         CountIris = CountIris + 1
       Else
         flag = False
       End If
     End If
   Wend

End Function

Public Sub INSERT_IMSDC_SEND(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String, thereisChkp As Boolean, spa As Boolean)
   Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   Dim wRetCodePcb As String
   Dim wMapSet As String
   
   'AC
   Dim FlagSend As String
   If thereisChkp Then
     FlagSend = "1"
   Else
     FlagSend = "0"
   End If
   
   wRetCodePcb = rst!numpcb & "(11:2)"
   
   codOperazione = "DC-" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   
   wIstr = Trim(rst!Istruzione)
      
   wNomeRoutine = "'" & Create_NomeRoutine_IMSDC(wIstr) & "'"
   'AC 14/7/06
   'IRIS-DB impostato in AsteriscaIstruzione_DC wIdent = 12
   'IRIS-DB start
'''''   If Not (spa And rst!MapName = "") Then
'''''    ' serve per mantenere gli aggiornamenti della change e le stampe di pi� pagine
'''''    'IRIS-DB Testo = vbCrLf & Testo & START_TAG & Space(65) & END_TAG & vbCrLf
'''''    'IRIS-DB Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE '" & NomeProg & "' TO LNKDC-CALLER")
'''''     Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "MOVE '" & NomeProg & "' TO LNKDC-CALLER") 'IRIS-DB
'''''
'''''    Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE '" & codOperazione & "' TO LNKDC-OPERATION")
'''''
'''''    If Trim(rst!MapName) <> "" Then
'''''       Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & rst!MapName & " TO LNKDC-MSGNAME")
'''''    Else
'''''       ' Testo = Testo & FormatTag(Space$(wIdent - 7) & "[MFS-MAP-NAME NOT FOUND!!!]")
'''''       ' nel caso di stampe cicliche sulla stessa stampante e sulla stessa MFS il DLI non necessita di questa area
'''''    End If
'''''
'''''    wMapSet = rst!mapset & ""
'''''
'''''    If Trim(wMapSet) <> "" Then
'''''       Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & wMapSet & " TO LNKDC-MAPSET")
'''''    End If
'''''
'''''    If Not bolsungard Then
'''''
'''''      If Trim(rst!maparea) <> "" Then
'''''         Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & rst!maparea & " TO LNKDC-MFSMAP-AREA")
'''''      Else
'''''         Testo = Testo & FormatTag(Space$(wIdent - 7) & "[MFS-MAP-AREA NOT FOUND!!!]")
'''''      End If
'''''
'''''    Else
'''''        Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & rst!maparea & " TO LNKDC-SKT-PASS-AREA")
'''''    End If
'''''
'''''    'IRIS-DB Testo = Testo & START_TAG & Space(65) & END_TAG & vbCrLf
'''''    Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNKDC-AREA")
'''''    'IRIS-DB Testo = Testo & START_TAG & Space(65) & END_TAG & vbCrLf
'''''
'''''    Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-PCB TO " & rst!numpcb)
'''''  Else
'''''    Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "MOVE " & rst!maparea & " TO DFHCOMMAREA")
'''''  End If
   If TipoFileDC = "PLI" Then 'IRIS-DB
     If typeIncaps = "ONLINE" Then
       Testo = Space$(wIdent) & "IRIS_WS_RTN = 'IRISSEND';" & vbCrLf
     Else
       Testo = Space$(wIdent) & "IRIS_WS_RTN = 'IRISUTIL';" & vbCrLf
     End If
     Testo = Testo & Space$(wIdent) & "IRIS_IMS_FUNCTION = 'ISRT';" & vbCrLf
     If Len(Trim(rst!MapName)) Then
       Testo = Testo & Space$(wIdent) & "IRIS_PARAM_NUM = 3;" & vbCrLf
     Else
       Testo = Testo & Space$(wIdent) & "IRIS_PARAM_NUM = 2;" & vbCrLf
     End If
     Testo = Testo & Space$(wIdent) & "IRISIORT = 'IRISSEND';" & vbCrLf
     Testo = Testo & Space$(wIdent) & "CALL IRISIORT (IRIS_WORK_AREA," & vbCrLf
     Testo = Testo & Space$(wIdent) & "               " & rst!numpcb & "," & vbCrLf
     If Len(Trim(rst!MapName)) Then
       Testo = Testo & Space$(wIdent) & "               " & rst!maparea & "," & vbCrLf
       Testo = Testo & Space$(wIdent) & "               " & rst!MapName & ");" & vbCrLf
     Else
       Testo = Testo & Space$(wIdent) & "               " & rst!maparea & ");" & vbCrLf
     End If
     Testo = Testo & " /* " & START_TAG & " END -----------------------------*/" & vbCrLf
   Else
     If typeIncaps = "ONLINE" Then
       Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISSND-RTN TO TRUE") 'IRIS-DB
     Else
       Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISUTIL-RTN TO TRUE") 'IRIS-DB
     End If
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-ISRT TO TRUE") 'IRIS-DB
     If Len(Trim(rst!MapName)) Then
       Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 3 TO IRIS-PARAM-NUM") 'IRIS-DB
     Else
       Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 2 TO IRIS-PARAM-NUM") 'IRIS-DB
     End If
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL IRIS-WS-RTN  USING IRIS-WORK-AREA") 'IRIS-DB
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!numpcb) 'IRIS-DB
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!maparea) 'IRIS-DB
     If Len(Trim(rst!MapName)) Then
       Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!MapName) 'IRIS-DB
     End If
  'IRIS-DB end
     If wPunto Then
        Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
     End If
     Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB
   End If
   
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub

Function togliapici(Source As String) As String
  If Left(Source, 1) = "'" And Right(Source, 1) = "'" Then
    togliapici = Mid(Source, 2, Len(Source) - 1)
  Else
    togliapici = Source
  End If
End Function

Public Sub INSERT_IMSDC_RECEIVE(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String, spa As Boolean) ', pModName As String)
  Dim k As Integer
  Dim wIstr As String
  Dim Testo As String
  Dim wIstruzione As String
  Dim wResp As Boolean
  Dim codOperazione As String
  Dim s() As String
  Dim wNomeRoutine As String
  Dim wRetCodePcb As String
  Dim wSpa As String
  Dim rstIstr As Recordset
   
  wRetCodePcb = rst!numpcb & "(11:2)"
 
  codOperazione = "DC-" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
 
  wIstr = Trim(rst!Istruzione)
    
  wNomeRoutine = "'" & Create_NomeRoutine_IMSDC(wIstr) & "'"
  'IRIS-DB impostato in AsteriscaIstruzione_DC wIdent = 12
 
'IRIS-DB  If Not spa Then
  'IRIS-DB Testo = vbCrLf & Testo & FormatTag("")
  'IRIS-DB  Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE '" & NomeProg & "' TO LNKDC-CALLER")
  If TipoFileDC = "PLI" Then 'IRIS-DB
    If typeIncaps = "ONLINE" Then
      Testo = Space$(wIdent) & "IRIS_WS_RTN = 'IRISRCVE';" & vbCrLf
    Else
      Testo = Space$(wIdent) & "IRIS_WS_RTN = 'IRISUTIL';" & vbCrLf
    End If
  Else
    If typeIncaps = "ONLINE" Then
      Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISRCV-RTN TO TRUE") 'IRIS-DB
    Else
      Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISUTIL-RTN TO TRUE") 'IRIS-DB
    End If
  End If
   
    'IRIS-DB Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE '" & codOperazione & "' TO LNKDC-OPERATION")
  If TipoFileDC = "PLI" Then 'IRIS-DB
    Testo = Testo & Space$(wIdent) & "IRIS_IMS_FUNCTION = 'GU';" & vbCrLf
    Testo = Testo & Space$(wIdent) & "IRIS_PARAM_NUM = 2;" & vbCrLf
  Else
    Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-GU TO TRUE") 'IRIS-DB
    Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 2 TO IRIS-PARAM-NUM") 'IRIS-DB
  End If
   
    'IRIS-DB If bolsungard Then
      'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE WS-SCODE         TO  LNKDC-STCODE")
      'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE TRIG-DATA        TO  LNKDC-TRIG-DATA")
      'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE MTP-SOCKET-AREA  TO  LNKDC-SOCKET-AREA")
    'IRIS-DB End If
   
'IRIS-DB    Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNKDC-AREA")
  If TipoFileDC = "PLI" Then 'IRIS-DB
    Testo = Testo & Space$(wIdent) & "IRISIORT = 'IRISRCVE';" & vbCrLf
    Testo = Testo & Space$(wIdent) & "CALL IRISIORT (IRIS_WORK_AREA," & vbCrLf
    Testo = Testo & Space$(wIdent) & "               " & rst!numpcb & "," & vbCrLf
    Testo = Testo & Space$(wIdent) & "               " & rst!maparea & ");" & vbCrLf
  Else
    Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL IRIS-WS-RTN  USING IRIS-WORK-AREA") 'IRIS-DB
    Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!numpcb) 'IRIS-DB
    Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!maparea) 'IRIS-DB
  End If

    'AC 08/02/07
'IRIS-DB    If Not bolsungard Then
      'IRIS-DB Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-MFSMAP-AREA TO " & rst!maparea)
'IRIS-DB      Testo = Testo & FormatTag("")
'IRIS-DB    Else
'IRIS-DB      Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-SKT-PASS-AREA TO " & rst!maparea)
'IRIS-DB      Testo = Testo & FormatTag("")
'IRIS-DB    End If

    'IRIS-DB Testo = Testo & FormatTag("")
    'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-RETCODE TO " & wRetCodePcb)
'IRIS-DB    Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-PCB TO " & rst!numpcb)
'IRIS-DB  Else
    'wSpa = Trim(Replace(rst!Statement, rst!NumPCB, ""))
'IRIS-DB    wSpa = rst!maparea
'IRIS-DB    Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "MOVE DFHCOMMAREA TO " & wSpa)
'IRIS-DB  End If
  If wPunto And TipoFileDC = "CBL" Then
    Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
  End If
   
  If TipoFileDC = "PLI" Then 'IRIS-DB
    Testo = Testo & " /* " & START_TAG & " END -----------------------------*/" & vbCrLf
  Else
    Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB
  End If
   
  RTBModifica.SelStart = wPos
  RTBModifica.SelLength = 0
  RTBModifica.SelText = Testo
  
  wPos = wPos + Len(Testo)
   
  s = Split(Testo, vbCrLf)
    
  AddRighe = AddRighe + UBound(s)
End Sub

Public Sub INSERT_IMSDC_ENTRY(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, firstparameter As String)
  Dim wIstr As String
  Dim Testo As String
  Dim wIstruzione As String
  Dim wResp As Boolean
  Dim s() As String
  Dim rstIstr As Recordset
  
  'stefano: la COPY non serve a niente, da togliere anche per Alenia
  If bolsungard Then
    Testo = Testo & FormatTag("")
    Testo = Testo & FormatTag(Space(wIdent - 6) & "COPY DCSTCOD.")
    RTBModifica.SelStart = wPos
    RTBModifica.SelLength = 0
    RTBModifica.SelText = Testo
   
    wPos = wPos + Len(Testo)
   
    s = Split(Testo, vbCrLf)
     
    AddRighe = AddRighe + UBound(s)
    Exit Sub
  End If
  wIdent = 12
  
  Testo = Testo & vbCrLf
  Testo = Testo & FormatTag(Space$(wIdent - 6) & "COPY ENTRYDLI.")
  Testo = Testo & FormatTag(Space$(wIdent - 6) & "MOVE WK-CICS-TERMID TO " & firstparameter & "(1:8)")
  Testo = Testo & FormatTag("")
   
  If wPunto Then
    Testo = Testo & FormatTag(Space$(wIdent - 6) & ". ")
  End If
   
  RTBModifica.SelStart = wPos
  RTBModifica.SelLength = 0
  RTBModifica.SelText = Testo
   
  wPos = wPos + Len(Testo)
  
  s = Split(Testo, vbCrLf)
     
  AddRighe = AddRighe + UBound(s)
End Sub

Public Sub INSERT_IMSDC_XSRT(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String)
    Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   Dim cc As Long
   
   'AC alternativamente LEN oppure AREA
   Dim lnkfield As String
   
   Dim wRetCodePcb As String
   If Len(rst!numpcb) Then
    wRetCodePcb = rst!numpcb & "(11:2)"
   Else
    wRetCodePcb = "[TO-SET]"
   End If
   codOperazione = "DC" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   
   wIstr = Trim(rst!Istruzione)
      
   'IRIS-DB wNomeRoutine = Create_NomeRoutine_IMSDC(wIstr) & wIstr
   
   'IRIS-DB Testo = Testo & vbCrLf & FormatTag(Space$(wIdent - 7))
   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "INITIALIZE LNKDC-AREA")
   'Testo = Testo & FormatTag(Space$(wIdent - 7))
      
   cc = 0
   'IRIS-DB start
   k = UBound(TabIstr_DC.SavedAreas) + 2
   Testo = Testo & vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-XRST TO TRUE")
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRISUTIL-RTN TO TRUE")
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & k & " TO IRIS-PARAM-NUM") 'IRIS-DB
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL IRIS-WS-RTN USING IRIS-WORK-AREA")
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "                       " & rst!numpcb)
   'IRIS-DB end
   
   'stefano: prima e dopo
   For k = 1 To UBound(TabIstr_DC.SavedAreas)
      If k Mod 2 = 0 Then
         lnkfield = "AREA"
      Else
         cc = cc + 1
         lnkfield = "LEN "
      End If
      
'IRIS-DB      Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(k) & " TO LNK15-" & lnkfield & "(" & cc & ")")
      If k = UBound(TabIstr_DC.SavedAreas) Then 'IRIS-DB
         Testo = Testo & FormatTag(Space$(wIdent - 7) & "                       " & TabIstr_DC.SavedAreas(k) & IIf(wPunto, ".", "")) 'IRIS-DB
      Else
         Testo = Testo & FormatTag(Space$(wIdent - 7) & "                       " & TabIstr_DC.SavedAreas(k)) 'IRIS-DB
      End If 'IRIS-DB
   Next k
   'IRIS-DB start
''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDB-ACTIVE TO LNK15-FBK-AREA")
''   Testo = Testo & FormatTag(Space$(wIdent - 7))
''   'stefano: XRST non � una call CICS
''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL '" & wNomeRoutine & "' USING LNK15-AREA-X") 'virgilio
''   Testo = Testo & FormatTag(Space$(wIdent - 7))
''
''   cc = 0
''
''   For k = 1 To UBound(TabIstr_DC.SavedAreas)
''      If k Mod 2 = 0 Then
''         lnkfield = "AREA"
''      Else
''         cc = cc + 1
''         lnkfield = "LEN "
''      End If
''
''      Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNK15-" & lnkfield & "(" & cc & ") TO " & TabIstr_DC.SavedAreas(k))
''   Next k
''
''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNK15-FBK-AREA TO LNKDB-ACTIVE")
''   Testo = Testo & FormatTag(Space$(wIdent - 7))
''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNK15-RETCODE TO " & wRetCodePcb)
''   If wPunto Then
''     Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
''   End If
   'IRIS-DB end
   
   Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB removes the last CRLF
      
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub

Public Sub INSERT_IMSDC_CHECKPOINT(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String)
   Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   Dim cc As Long
   Dim wRetCodePcb As String
   
   'AC alternativamente LEN oppure AREA
   Dim lnkfield As String
   If Len(rst!numpcb) Then
    wRetCodePcb = rst!numpcb & "(11:2)"
   Else
    wRetCodePcb = "[TO-SET]"
   End If

   codOperazione = "DC" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   
   wIstr = Trim(rst!Istruzione)
      
'IRIS-DB   wNomeRoutine = Create_NomeRoutine_IMSDC(wIstr) & wIstr
   
'IRIS-DB   Testo = vbCrLf & FormatTag("")
   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "INITIALIZE LNKDC-AREA")
'IRIS-DB   Testo = Testo & FormatTag(Space$(wIdent - 7) & "INITIALIZE LNK15-SAVAREA")
   'stefano: a che servivano???
   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 'DC' TO LNK15-DB-DC")
   'Testo = Testo & FormatTag("")
   
   
   '********** saved areas *********
   cc = 0
   'IRIS-DB start
   k = UBound(TabIstr_DC.SavedAreas) + 2
   Testo = Testo & vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-CHKP TO TRUE")
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRISUTIL-RTN TO TRUE")
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & k & " TO IRIS-PARAM-NUM") 'IRIS-DB
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL IRIS-WS-RTN USING IRIS-WORK-AREA")
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "                       " & rst!numpcb)
   'IRIS-DB end
      
   'SG : muove le saved area
   For k = 1 To UBound(TabIstr_DC.SavedAreas)
      If k Mod 2 = 0 Then
         lnkfield = "AREA"
      Else
         cc = cc + 1
         lnkfield = "LEN "
      End If
      
'IRIS-DB      Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(k) & " TO LNK15-" & lnkfield & "(" & cc & ")")
      If k = UBound(TabIstr_DC.SavedAreas) Then 'IRIS-DB
         Testo = Testo & FormatTag(Space$(wIdent - 7) & "                       " & TabIstr_DC.SavedAreas(k) & IIf(wPunto, ".", "")) 'IRIS-DB
      Else
         Testo = Testo & FormatTag(Space$(wIdent - 7) & "                       " & TabIstr_DC.SavedAreas(k)) 'IRIS-DB
      End If 'IRIS-DB
   Next k
   
   'IRIS-DB start
   
''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDB-ACTIVE TO LNK15-FBK-AREA")
''   Testo = Testo & FormatTag("")
''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL '" & wNomeRoutine & "' USING LNK15-AREA-X") 'virgilio
''
''   Testo = Testo & FormatTag("")
''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNK15-RETCODE TO " & wRetCodePcb)
''
''   If wPunto Then
''      Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
''   End If
   
   'IRIS-DB end
   
   Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB removes the last CRLF
      
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub

Public Sub INSERT_IMSDC_ROLL(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String)
  Dim k As Integer
  Dim wIstr As String
  Dim Testo As String
  Dim wIstruzione As String
  Dim wResp As Boolean
  Dim codOperazione As String
  Dim s() As String
  Dim wNomeRoutine As String
  Dim cc As Long
  Dim c2 As Long
  Dim wRetCodePcb As String
   
   If Len(rst!numpcb) Then
    wRetCodePcb = rst!numpcb & "(11:2)"
   Else
    wRetCodePcb = "[TO-SET]"
   End If
   codOperazione = "DC-" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   'IRIS-DB wIdent = 12
   wIstr = Trim(rst!Istruzione)
      
   wNomeRoutine = "'" & Create_NomeRoutine_IMSDC(wIstr) & "'"
   
   'IRIS-DB Testo = vbCrLf & FormatTag("")
   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "INITIALIZE LNKDC-AREA")
   
   
'''   Testo = Testo & FormatTag("")
'''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE '" & "DC" & "' TO LNK15-DB-DC ")
   
   
   'Testo = Testo & FormatTag("")
   'IRIS-DB Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNKDC-AREA")
   Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNKDC-AREA") 'IRIS-DB
   
   'IRIS-DB Testo = Testo & FormatTag("")
   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-RETCODE TO " & wRetCodePcb)
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-PCB TO " & rst!numpcb)
   
   If wPunto Then
      Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
   End If
   
   Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB
   
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub

Public Sub INSERT_IMSDC_ROLB(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String)
    Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   Dim cc As Long
   Dim c2 As Long
   Dim wRetCodePcb As String
   
   If Len(rst!numpcb) Then
    wRetCodePcb = rst!numpcb & "(11:2)"
   Else
    wRetCodePcb = "[TO-SET]"
   End If
   codOperazione = "DC-" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   'IRIS-DB wIdent = 12
   wIstr = Trim(rst!Istruzione)
      
   wNomeRoutine = "'" & Create_NomeRoutine_IMSDC(wIstr) & "'"
   
   'IRIS-DB Testo = vbCrLf & FormatTag("")
   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "INITIALIZE LNKDC-AREA")
   
   
'''   Testo = Testo & FormatTag("")
'''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE '" & "DC" & "' TO LNK15-DB-DC ")
'''
'''   Testo = Testo & FormatTag("")
'''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE EIBTRNID TO LNK15-TRANSID ")
   'Testo = Testo & FormatTag("")
   'IRIS-DB Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE '" & NomeProg & "' TO LNKDC-CALLER")
   Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "MOVE '" & NomeProg & "' TO LNKDC-CALLER") 'IRIS-DB
   
   'IRIS-DB Testo = Testo & FormatTag("")
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNKDC-AREA")
   
   'IRIS-DB Testo = Testo & FormatTag("")
   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-RETCODE TO " & wRetCodePcb)
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-PCB TO " & rst!numpcb)
   
   If wPunto Then
      Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
   End If
   
   Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB
   
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub

Public Sub INSERT_IMSDC_CHNG(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String)
    Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   Dim cc As Long
   Dim c2 As Long
   Dim wRetCodePcb As String
   Dim tb As Recordset 'IRIS-DB
   
   If Len(rst!numpcb) Then
    wRetCodePcb = rst!numpcb & "(11:2)"
   Else
    wRetCodePcb = "[TO-SET]"
   End If
    
   codOperazione = "DC" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   
   wIstr = Trim(rst!Istruzione)
'''''IRIS-DB inizio nuova gestione
''''   wNomeRoutine = "'" & Create_NomeRoutine_IMSDC(wIstr) & "'"
''''
''''   'IRIS-DB Testo = vbCrLf & FormatTag("")
''''   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "INITIALIZE LNKDC-AREA")
''''   'IRIS-DB Testo = Testo & FormatTag("")
''''
''''  'IRIS-DB Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(1) & " TO LNKDC-CHNGDEST")
''''   Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(1) & " TO LNKDC-CHNGDEST") 'IRIS-DB
'''''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE EIBTRNID TO LNKDC-TRANSID")
''''   'IRIS-DB Testo = Testo & FormatTag("")
''''
''''   'IRIS-DB Testo = Testo & FormatTag("")
'''''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNKDC-AREA")
'''''''
'''''''   Testo = Testo & FormatTag("")
'''''''
'''''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNK15-RETCODE TO " & wRetCodePcb)
'''''''
'''''''   If wPunto Then
'''''''      Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
'''''''   End If
'''''''
'''''IRIS-DB reinserite le righe seguenti che erano state commentate non so perch�
''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNKDC-AREA")
''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNK15-RETCODE TO " & wRetCodePcb)
'''''IRIS-DB fine nuova gestione
''''
   'IRIS-DB for now it manages just the basic parameters of the CHNG
   If TipoFileDC = "PLI" Then
     If typeIncaps = "ONLINE" Then
       Testo = Space$(wIdent) & "IRIS_WS_RTN = 'IRISSEND';" & vbCrLf
     Else
       Testo = Space$(wIdent) & "IRIS_WS_RTN = 'IRISUTIL';" & vbCrLf
     End If
     Testo = Testo & Space$(wIdent) & "IRIS_IMS_FUNCTION = 'CHNG';" & vbCrLf
     Testo = Testo & Space$(wIdent) & "IRIS_PARAM_NUM = 2;" & vbCrLf
     Testo = Testo & Space$(wIdent) & "IRISIORT = 'IRISSEND';" & vbCrLf
     Testo = Testo & Space$(wIdent) & "CALL IRISIORT (IRIS_WORK_AREA," & vbCrLf
   'IRIS-DB not sure why it is in the Liv, it should use Map Area like the other IMSDC instructions
     Set tb = m_fun.Open_Recordset("select DataArea from PsDli_IstrDett_Liv where idPgm = " & rst!idpgm & " and idOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga & " and livello = 1")
     If Not tb.EOF Then
       If Len(tb!DataArea) Then
         Testo = Testo & Space$(wIdent) & "               " & rst!numpcb & "," & vbCrLf
         Testo = Testo & Space$(wIdent) & "               " & tb!DataArea & ");" & vbCrLf
       Else
         Testo = Testo & Space$(wIdent) & "               " & rst!numpcb & ");" & vbCrLf
         m_fun.WriteLog "Encaps IMSDC CHNG - Missing DataArea in PsDli_IstrDett_Liv - idpgm:(" & rst!idpgm & ") - idoggetto:(" & rst!IdOggetto & " - riga:(" & rst!Riga & ")" 'IRIS-DB
       End If
     Else
       Testo = Testo & Space$(wIdent) & "               " & rst!numpcb & ");" & vbCrLf
       m_fun.WriteLog "Encaps IMSDC CHNG - Missing DataArea in PsDli_IstrDett_Liv - idpgm:(" & rst!idpgm & ") - idoggetto:(" & rst!IdOggetto & " - riga:(" & rst!Riga & ")" 'IRIS-DB
     End If
     tb.Close
     Testo = Testo & " /* " & START_TAG & " END -----------------------------*/" & vbCrLf
   Else
     If typeIncaps = "ONLINE" Then
       Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISSND-RTN TO TRUE") 'IRIS-DB
     Else
       Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISUTIL-RTN TO TRUE") 'IRIS-DB
     End If
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-CHNG TO TRUE") 'IRIS-DB
'tolto   Testo = Testo & FormatTag(Space$(wIdent - 7) & "ADD 1 TO IRIS-TASK-NUMBER") 'IRIS-DB
'tolto   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE IRIS-TASK-NUMBER TO IRIS-TASK-NUM") 'IRIS-DB
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 2 TO IRIS-PARAM-NUM") 'IRIS-DB
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL IRIS-WS-RTN  USING IRIS-WORK-AREA") 'IRIS-DB
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!numpcb) 'IRIS-DB
   'IRIS-DB not sure why it is in the Liv, it should use Map Area like the other IMSDC instructions
     Set tb = m_fun.Open_Recordset("select DataArea from PsDli_IstrDett_Liv where idPgm = " & rst!idpgm & " and idOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga & " and livello = 1") 'IRIS-DB
     If Not tb.EOF Then
       If Len(tb!DataArea) Then 'IRIS-DB
         Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & tb!DataArea) 'IRIS-DB
       Else
         m_fun.WriteLog "Encaps IMSDC CHNG - Missing DataArea in PsDli_IstrDett_Liv - idpgm:(" & rst!idpgm & ") - idoggetto:(" & rst!IdOggetto & " - riga:(" & rst!Riga & ")" 'IRIS-DB
       End If
     Else 'IRIS-DB
       m_fun.WriteLog "Encaps IMSDC CHNG - Missing DataArea in PsDli_IstrDett_Liv - idpgm:(" & rst!idpgm & ") - idoggetto:(" & rst!IdOggetto & " - riga:(" & rst!Riga & ")" 'IRIS-DB
     End If 'IRIS-DB
     tb.Close 'IRIS-DB
     If wPunto Then
       Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
     End If
     Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB
   End If
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub

Public Sub INSERT_IMSDC_PURG(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String)
    Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   
   Dim wRetCodePcb As String
   If Len(rst!numpcb) Then
    wRetCodePcb = rst!numpcb & "(11:2)"
   Else
    wRetCodePcb = "[TO-SET]"
   End If
   codOperazione = "DC" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   
   wIstr = Trim(rst!Istruzione)
      
'''''IRIS-DB inizio
   
'''''   wNomeRoutine = "'" & Create_NomeRoutine_IMSDC(wIstr) & "'"
'''''
'''''   'IRIS-DB Testo = vbCrLf & FormatTag("")
'''''   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "INITIALIZE LNKDC-AREA") & vbCrLf
'''''   'Testo = Testo & FormatTag("")
'''''
'''''
'''''   'IRIS-DB Testo = Testo & FormatTag("")
'''''   'IRIS-DB Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNKDC-AREA")
'''''    Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNKDC-AREA") 'IRIS-DB
'''''
'''''   'IRIS-DB Testo = Testo & FormatTag("")
'''''   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-RETCODE TO " & wRetCodePcb)
'''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-PCB TO " & rst!numpcb)
   'IRIS-DB for now it manages just the basic parameters of the PURG: MODNAME to add in case it is used (not in Fedex)
   If typeIncaps = "ONLINE" Then
     Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISSND-RTN TO TRUE") 'IRIS-DB
   Else
     Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISUTIL-RTN TO TRUE") 'IRIS-DB
   End If
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-PURG TO TRUE") 'IRIS-DB
   'IRIS-DB not sure why it is in the Liv, it should use Map Area like the other IMSDC instructions; level = 3 for some strange reason, kept anyhow as is
   Dim tb As Recordset 'IRIS-DB
   Dim okTran As Boolean
   okTran = False
   Set tb = m_fun.Open_Recordset("select DataArea from PsDli_IstrDett_Liv where idPgm = " & rst!idpgm & " and idOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga & " and livello = 3") 'IRIS-DB
   If Not tb.EOF Then
     If Len(tb!DataArea) Then 'IRIS-DB
       okTran = True
     End If
   End If 'IRIS-DB
   If okTran Then
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 2 TO IRIS-PARAM-NUM") 'IRIS-DB
   Else
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 1 TO IRIS-PARAM-NUM") 'IRIS-DB
   End If
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL IRIS-WS-RTN  USING IRIS-WORK-AREA") 'IRIS-DB
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!numpcb) 'IRIS-DB
   If okTran Then
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & tb!DataArea) 'IRIS-DB
   End If
   tb.Close 'IRIS-DB
   
'''''IRIS-DB fine
   
   If wPunto Then
      Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
   End If
   
   Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB
   
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub
Public Sub INSERT_IMSDC_ROLS(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String) 'IRIS-DB
   Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   
   Dim wRetCodePcb As String
   If Len(rst!numpcb) Then
    wRetCodePcb = rst!numpcb & "(11:2)"
   Else
    wRetCodePcb = "[TO-SET]"
   End If
   codOperazione = "DC" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   
   wIstr = Trim(rst!Istruzione)
      
'''   If typeIncaps = "ONLINE" Then
'''     Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISSND-RTN TO TRUE")
'''   Else
     Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISUTIL-RTN TO TRUE")
'''   End If
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-ROLS TO TRUE")
   'IRIS-DB not sure why it is in the Liv, it should use Map Area like the other IMSDC instructions; level = 3 for some strange reason, kept anyhow as is
   Dim tb As Recordset
   Dim okTran As Boolean
   okTran = False
   Set tb = m_fun.Open_Recordset("select DataArea from PsDli_IstrDett_Liv where idPgm = " & rst!idpgm & " and idOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga & " and livello = 3")
   If Not tb.EOF Then
     If Len(tb!DataArea) Then
       okTran = True
     End If
   End If
   If okTran Then
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 2 TO IRIS-PARAM-NUM")
   Else
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 1 TO IRIS-PARAM-NUM")
   End If
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL IRIS-WS-RTN  USING IRIS-WORK-AREA")
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!numpcb)
   If okTran Then
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & tb!DataArea)
   End If
   tb.Close
   
   If wPunto Then
      Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
   End If
   
   Testo = Left(Testo, Len(Testo) - 2)
   
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub
Public Sub INSERT_IMSDC_SETS(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String) 'IRIS-DB
   Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   
   Dim wRetCodePcb As String
   If Len(rst!numpcb) Then
    wRetCodePcb = rst!numpcb & "(11:2)"
   Else
    wRetCodePcb = "[TO-SET]"
   End If
   codOperazione = "DC" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   
   wIstr = Trim(rst!Istruzione)
      
'''   If typeIncaps = "ONLINE" Then
'''     Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISSND-RTN TO TRUE")
'''   Else
     Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISUTIL-RTN TO TRUE")
'''   End If
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-SETS TO TRUE")
   'IRIS-DB not sure why it is in the Liv, it should use Map Area like the other IMSDC instructions; level = 3 for some strange reason, kept anyhow as is
   Dim tb As Recordset
   Dim okTran As Boolean
   okTran = False
   Set tb = m_fun.Open_Recordset("select DataArea from PsDli_IstrDett_Liv where idPgm = " & rst!idpgm & " and idOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga & " and livello = 3")
   If Not tb.EOF Then
     If Len(tb!DataArea) Then
       okTran = True
     End If
   End If
   If okTran Then
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 2 TO IRIS-PARAM-NUM")
   Else
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 1 TO IRIS-PARAM-NUM")
   End If
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL IRIS-WS-RTN  USING IRIS-WORK-AREA")
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!numpcb)
   If okTran Then
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & tb!DataArea)
   End If
   tb.Close
   
   If wPunto Then
      Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
   End If
   
   Testo = Left(Testo, Len(Testo) - 2)
   
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub
Public Sub InserisciPrimaRigaAllineaSuccessive(Rtb As RichTextBox, target As String, text As String)
  Dim posizione As Long
  Dim crLfPosition As Long
  Dim indentedText As String
  Dim Start As String
      
  While True
    posizione = Rtb.find(target, posizione + Len(indentedText))
    Select Case posizione
      Case -1
        Exit Sub
      Case 0
        indentedText = text
      Case Else
        crLfPosition = InStrRev(Rtb.text, vbCrLf, posizione)
        If crLfPosition = 0 Then
          'Linea 1: non ho il fine linea...
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(posizione))
          Start = 1
        Else
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(posizione - crLfPosition - 1))
          Start = crLfPosition + 1
        End If
    End Select
    Rtb.SelStart = Start
    Rtb.SelLength = Len(target) + posizione - Start
    Rtb.SelText = indentedText
  Wend
End Sub

Public Sub INSERT_DCCLTS(pRTBModifica As RichTextBox)
  Dim TextField As String
  
  TextField = FormatTag("    CALL DCCLTS") & "GOBACK"
  InserisciPrimaRigaAllineaSuccessive pRTBModifica, "GOBACK", TextField

  TextField = FormatTag("    CALL DCCLTS") & "STOPRUN"
  InserisciPrimaRigaAllineaSuccessive pRTBModifica, "STOPRUN", TextField
End Sub

Function FindPunto(pIdOggetto As Long, pRiga As Long) As Boolean
  Dim tb As Recordset
  
  FindPunto = False
  Set tb = m_fun.Open_Recordset("select * from PsCall where " & _
                                "IdOggetto = " & pIdOggetto & " and Riga = " & pRiga)
  If Not tb.EOF Then
    If InStr(tb!parametri, ".") > 0 Then
      FindPunto = True
    End If
  'SQ
  'Else
    'COME SI FA A FARE FINTA DI NIENTE?????????????????????????????????!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    'NON C'E' L'ISTRUZIONE SUL DATABASE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  End If
  tb.Close
End Function

Public Sub INSERT_IMSDC_AUTH(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String)
  Dim k As Integer
  Dim wIstr As String
  Dim Testo As String
  Dim wIstruzione As String
  Dim wResp As Boolean
  Dim codOperazione As String
  Dim s() As String
  Dim wNomeRoutine As String
  Dim cc As Long
  Dim c2 As Long
  Dim wRetCodePcb As String
   
   If Len(rst!numpcb) Then
    wRetCodePcb = rst!numpcb & "(11:2)"
   Else
    wRetCodePcb = "[TO-SET]"
   End If
   codOperazione = "DC-" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   
   wIstr = Trim(rst!Istruzione)
      
   wNomeRoutine = "'" & Create_NomeRoutine_IMSDC(wIstr) & "'"
   
   'IRIS-DB Testo = vbCrLf & FormatTag("")
   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "INITIALIZE LNKDC-AREA")
   
   'Testo = Testo & m_fun.LABEL_INCAPS & vbCrLf
   'Testo = Testo & m_fun.LABEL_INCAPS & Space$(wIdent - 7) & "MOVE '" & NomeProg & "' TO LNK15-TRANSID" & vbCrLf
   'Testo = Testo & m_fun.LABEL_INCAPS & Space$(wIdent - 7) & "MOVE '" & codOperazione & "' TO LNK15-DEST" & vbCrLf
   
   
   'IRIS-DB Testo = Testo & START_TAG & Space(65) & END_TAG & vbCrLf
               
   'IRIS-DB Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE '" & TabIstr_DC.SavedAreas(1) & "' TO LNK15-PARMAREA ")
   Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "MOVE '" & TabIstr_DC.SavedAreas(1) & "' TO LNK15-PARMAREA ") 'IRIS-DB
   
      
   'cc = 1
   'c2 = 1
   
   'SG : muove le saved area
   'For k = 1 To UBound(TabIstr_DC.SavedAreas)
      'Testo = Testo & m_fun.LABEL_INCAPS & Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(k) & " TO LNKDC-SAVED-AREAS(" & cc & ", " & c2 & ")" & vbCrLf
      
      'If k Mod 2 = 0 Then
         'cc = cc + 1
         'c2 = 1
      'Else
         'c2 = c2 + 1
      'End If
   'Next k
   
   '***********
   
   'IRIS-DB Testo = Testo & START_TAG & Space(65) & END_TAG & vbCrLf
   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING  DFHEIBLK LNKDC-AREA")
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING LNKDC-AREA") 'virgilio
   
   'IRIS-DB Testo = Testo & START_TAG & Space(65) & END_TAG & vbCrLf
   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-RETCODE TO " & wRetCodePcb)
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-PCB TO " & rst!numpcb)
   
   If wPunto Then
      Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
   End If
   
   Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB
   
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub
 
Public Sub INSERT_IMSDC_INQY(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String)
   Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   Dim cc As Long
   Dim c2 As Long
   Dim tbArea As Recordset
   Dim tbCmpArea As Recordset
   Dim wLenAreas1 As Integer
   Dim wLenAreas2 As Integer
   'AC da analisi non produce returncode
''   Dim wRetCodePcb As String
''
''   If Len(rst!NumPCB) Then
''    wRetCodePcb = rst!NumPCB & "(11:2)"
''   Else
''    wRetCodePcb = "[TO-SET]"
''   End If
   codOperazione = "DC" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
   
   wIstr = Trim(rst!Istruzione)
'IRIS-DB: tutto cambiato
'''''   wNomeRoutine = "'" & Create_NomeRoutine_IMSDC(wIstr) & "'"
'''''
'''''   'IRIS-DB Testo = vbCrLf & FormatTag("")
'''''   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "INITIALIZE LNKDC-AREA")
'''''   'Testo = Testo & FormatTag("")
'''''
'''''   Set tbArea = m_fun.Open_Recordset("select * from PsData_Area where livello = 1 and nome = '" & TabIstr_DC.SavedAreas(1) & "'")
'''''   If Not tbArea.EOF Then
'''''      Set tbCmpArea = m_fun.Open_Recordset("select * from PsData_Cmp where livello = 1 and nome = '" & TabIstr_DC.SavedAreas(1) & "' and Idoggetto = " & tbArea!IdOggetto & " and IdArea = " & tbArea!IdArea)
'''''      If Not tbCmpArea.EOF Then
'''''        wLenAreas1 = tbCmpArea!Byte
'''''      Else
'''''        MsgBox "Cmp from area: " & TabIstr_DC.SavedAreas(1) & "missing", vbCritical
'''''        wLenAreas1 = 0
'''''      End If
'''''      tbArea.Close
'''''      tbCmpArea.Close
'''''   Else
'''''      MsgBox "Field from area: " & TabIstr_DC.SavedAreas(1) & "missing", vbCritical
'''''      wLenAreas1 = 0
'''''   End If
'''''
'''''   Set tbArea = m_fun.Open_Recordset("select * from PsData_Area where livello = 1 and nome = '" & TabIstr_DC.SavedAreas(2) & "'")
'''''   If Not tbArea.EOF Then
'''''      Set tbCmpArea = m_fun.Open_Recordset("select * from PsData_Cmp where livello = 1 and nome = '" & TabIstr_DC.SavedAreas(2) & "' and Idoggetto = " & tbArea!IdOggetto & " and IdArea = " & tbArea!IdArea)
'''''      If Not tbCmpArea.EOF Then
'''''        wLenAreas2 = tbCmpArea!Byte
'''''      Else
'''''        MsgBox "Cmp from area: " & TabIstr_DC.SavedAreas(2) & "missing", vbCritical
'''''        wLenAreas2 = 0
'''''      End If
'''''      tbArea.Close
'''''      tbCmpArea.Close
'''''   Else
'''''      MsgBox "Field from area: " & TabIstr_DC.SavedAreas(2) & "missing", vbCritical
'''''      wLenAreas2 = 0
'''''   End If
'''''
'''''   'IRIS-DBTesto = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(1) & " TO LNK15-PARMAREA(1:" & wLenAreas1 & ")")
'''''   Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(1) & " TO LNK15-PARMAREA(1:" & wLenAreas1 & ")") 'IRIS-DB
'''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(2) & " TO LNK15-PARMAREA(" & (wLenAreas1 + 1) & ":" & wLenAreas2 & ") ")
'''''
'''''
'''''' virgilio
''''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(1) & " TO LNK15-PARMAREA(1:136)")
''''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(2) & " TO LNK15-PARMAREA(137:132) ")
'''''
'''''
'''''   '***********
'''''
'''''   'IRIS-DB Testo = Testo & FormatTag("")
'''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNK15-AREA")
'''''
'''''' virgilio
''''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & "LNK15-PARMAREA(1:136) TO " & TabIstr_DC.SavedAreas(1))
''''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & "LNK15-PARMAREA(137:268) TO " & TabIstr_DC.SavedAreas(2))
'''''
'''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & "LNK15-PARMAREA(1:" & wLenAreas1 & ") TO " & TabIstr_DC.SavedAreas(1))
'''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & "LNK15-PARMAREA(" & (wLenAreas1 + 1) & ":" & wLenAreas2 & ") TO " & TabIstr_DC.SavedAreas(2))
   
   If typeIncaps = "ONLINE" Then
     Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISSND-RTN TO TRUE") 'IRIS-DB
   Else
     Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISUTIL-RTN TO TRUE") 'IRIS-DB
   End If
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-INQY TO TRUE") 'IRIS-DB
   'IRIS-DB not sure why it is in the Liv, it should use Map Area like the other IMSDC instructions; level = 3 for some strange reason, kept anyhow as is
   Dim tb As Recordset 'IRIS-DB
   Dim okTran As Boolean
   okTran = False
   Set tb = m_fun.Open_Recordset("select DataArea from PsDli_IstrDett_Liv where idPgm = " & rst!idpgm & " and idOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga & " and livello = 2") 'IRIS-DB
   If Not tb.EOF Then
     If Len(tb!DataArea) Then 'IRIS-DB
       okTran = True
     End If
   End If 'IRIS-DB
   If okTran Then
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 2 TO IRIS-PARAM-NUM") 'IRIS-DB
   Else
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 1 TO IRIS-PARAM-NUM") 'IRIS-DB
   End If
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL IRIS-WS-RTN  USING IRIS-WORK-AREA") 'IRIS-DB
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!numpcb) 'IRIS-DB
   If okTran Then
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & tb!DataArea) 'IRIS-DB
   End If
   tb.Close 'IRIS-DB
   
   If wPunto Then
      Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
   End If
   
   Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB
   
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub
   
' Public Sub INSERT_IMSDC_ICMD(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String) 'IRIS-DB
Public Sub INSERT_IMSDC_CMD(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String)
    Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   Dim cc As Long
   Dim c2 As Long
   Dim wRetCodePcb As String
   
'IRIS-DB removing anything
''''''   If Len(rst!numpcb) Then
''''''    'wRetCodePcb = rst!NumPCB & "(11:2)" virgilio
''''''    wRetCodePcb = rst!numpcb & "(65:4)"
''''''   Else
''''''    wRetCodePcb = "[TO-SET]"
''''''   End If
''''''  'IRIS-DB codOperazione = "DC" & Replace(Mid(CurCodiceOp, 1, 4), ".", "")
''''''   codOperazione = "DC" & Replace(Mid(CurCodiceOp, 1, 3), ".", "") 'IRIS-DB
''''''
''''''   wIstr = Trim(rst!Istruzione)
''''''
''''''   wNomeRoutine = "'" & Create_NomeRoutine_IMSDC(wIstr) & "'"
''''''
''''''   'IRIS-DB Testo = vbCrLf & FormatTag("")
''''''   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "INITIALIZE LNKDC-AREA")
''''''   'Testo = Testo & FormatTag("")
''''''
''''''   '************ AC: no saved area - move campi di intersse
''''''
''''''   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE '" & TabIstr_DC.SavedAreas(1) & "' TO LNK15-PARMAREA ") virgilio
''''''   'IRIS-DB Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(1) & " TO LNKDC-PARMAREA ")
''''''   Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "MOVE " & TabIstr_DC.SavedAreas(1) & " TO LNKDC-PARMAREA ") 'IRIS-DB
''''''
''''''
''''''   '***********
''''''
''''''   'IRIS-DB Testo = Testo & FormatTag("")
''''''   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING DFHEIBLK LNKDC-AREA")
''''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL " & wNomeRoutine & " USING LNKDC-AREA") 'virgilio
''''''
''''''   'IRIS-DB Testo = Testo & FormatTag("")
''''''   'Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-RETCODE TO " & wRetCodePcb)
''''''   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE LNKDC-PCB TO " & rst!numpcb)
   
   wIstr = Trim(rst!Istruzione)
   
   Testo = vbCrLf & FormatTag(Space$(wIdent - 7) & "SET IRISUTIL-RTN TO TRUE") 'IRIS-DB
   If wIstr = "GCMD" Then
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-GCMD TO TRUE") 'IRIS-DB
   Else
     Testo = Testo & FormatTag(Space$(wIdent - 7) & "SET IRIS-FUNC-CMD TO TRUE") 'IRIS-DB
   End If
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "MOVE 2 TO IRIS-PARAM-NUM") 'IRIS-DB
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "CALL IRIS-WS-RTN  USING IRIS-WORK-AREA") 'IRIS-DB
   Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & rst!numpcb) 'IRIS-DB
   'IRIS-DB not sure why it is in the Liv, it should use Map Area like the other IMSDC instructions
   Dim tb As Recordset 'IRIS-DB
   'IRIS-DB for some reasons, the parser could have set level 1 or level 3
   'IRIS-DB Set tb = m_fun.Open_Recordset("select DataArea from PsDli_IstrDett_Liv where idPgm = " & rst!idPgm & " and idOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga & " and livello = 1") 'IRIS-DB
   Set tb = m_fun.Open_Recordset("select DataArea from PsDli_IstrDett_Liv where idPgm = " & rst!idpgm & " and idOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga) 'IRIS-DB
   If Not tb.EOF Then
     If Len(tb!DataArea) Then 'IRIS-DB
       Testo = Testo & FormatTag(Space$(wIdent - 7) & "                        " & tb!DataArea) 'IRIS-DB
     Else
       m_fun.WriteLog "Encaps IMSDC CMD - Missing DataArea in PsDli_IstrDett_Liv - idpgm:(" & rst!idpgm & ") - idoggetto:(" & rst!IdOggetto & " - riga:(" & rst!Riga & ")" 'IRIS-DB
     End If
   Else 'IRIS-DB
     m_fun.WriteLog "Encaps IMSDC CMD - Missing DataArea in PsDli_IstrDett_Liv - idpgm:(" & rst!idpgm & ") - idoggetto:(" & rst!IdOggetto & " - riga:(" & rst!Riga & ")" 'IRIS-DB
   End If 'IRIS-DB
   tb.Close 'IRIS-DB
   
   If wPunto Then
      Testo = Testo & FormatTag(Space$(wIdent - 7) & ". ")
   End If
   
   Testo = Left(Testo, Len(Testo) - 2) 'IRIS-DB
   
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub

Public Function Create_Chiave_decodifca(xIstr As String) As Double
   Dim wOpe As String
   Dim k As Double
   Dim xope As String
   Dim tb As Recordset
   Dim K1 As Integer
   Dim TipIstr As String
   Dim wUtil As Boolean
   Dim orIstr As String
   
   If UBound(OpeIstrRt) = 0 Then
      Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr order by idistruzione")
      If tb.RecordCount > 0 Then
        While Not tb.EOF
          k = tb!IdIstruzione
          
          ReDim Preserve OpeIstrRt(k)
          OpeIstrRt(k).Decodifica = tb!Decodifica
          OpeIstrRt(k).Istruzione = tb!Codifica
          tb.MoveNext
        Wend
      End If
      tb.Close
   End If
   
   TipIstr = Mid$(xIstr, 1, 2)
   orIstr = xIstr
   
   If xIstr = "GU" Then
     TipIstr = "RGU"
   ElseIf xIstr = "ISRT" Then
     TipIstr = "SISR"
   ElseIf xIstr = "GN" Then
     TipIstr = "RGN"
   ElseIf xIstr = "CHKP" Then
     TipIstr = "RCHKP"
   ElseIf xIstr = "XSRT" Then
     TipIstr = "RXSRT"
   Else
   'IRIS-DB: addedd support for all other instructions, they are already filtered from the Parser
     TipIstr = "R" & xIstr 'IRIS-DB
     'IRIS-DB m_fun.WriteLog "Incapsulatore - Create_Chiave_Decodifica " & vbCrLf & "Istruzione non riconosciuta " & xIstr, "DR - Incapsulatore"
   End If
   
   nomeRoutine = Create_NomeRoutine_IMSDC(xIstr)
   wOpe = nomeRoutine & Mid$(Trim(orIstr) & "....", 1, 4)
   
   xope = ""
   
   For k = 1 To UBound(OpeIstrRt)
      If wOpe = OpeIstrRt(k).Decodifica Then
         xope = OpeIstrRt(k).Istruzione
         
         CurCodiceOp = OpeIstrRt(k).Istruzione
     
         Create_Chiave_decodifca = k
         Exit Function
      End If
   Next k
   
   k = UBound(OpeIstrRt) + 1
   ReDim Preserve OpeIstrRt(k)
   
   OpeIstrRt(k).Decodifica = wOpe
   OpeIstrRt(k).Istruzione = Mid$(Trim(orIstr) & "....", 1, 4) & Format(UBound(OpeIstrRt), "000000")
   
   Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr order by idistruzione")
   
   tb.AddNew
     tb!IdIstruzione = k
     tb!Codifica = OpeIstrRt(k).Istruzione
     
     CurCodiceOp = OpeIstrRt(k).Istruzione
     
     tb!Decodifica = OpeIstrRt(k).Decodifica
     tb!ImsDB = False
     tb!ImsDC = True
     tb!nomeRout = "IRISUTIL" 'IRIS-DB
   
   tb.Update
   tb.Close
   
   Create_Chiave_decodifca = k
End Function

Public Function getMapsetFromIdMap(wMapId As Long) As String
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("Select * From PsCom_Obj Where " & _
                                "IdOggettoC = " & wMapId & " And Relazione = 'MPS'")
  If rs.RecordCount Then
    getMapsetFromIdMap = rs!NomeComponente
  End If
  rs.Close
End Function
'SQ
Public Sub changeGOBACK(RTBModifica As RichTextBox, namePgm As String)
  Dim arrFile() As String, numFile As Integer, Riga As Integer, i As Integer
  Dim CurrRiga As String
  Dim IOPCB As String
  Dim bolEntry As Boolean
  Dim rsEntry As Recordset
  Dim dynamicSSA As Boolean
  dynamicSSA = m_fun.LeggiParam("IMSDB-DYNAMICSSA")
  IOPCB = "Missing IOPCB"
  Set rsEntry = m_fun.Open_Recordset("Select PCB from PsIMS_Entry where " & _
                                    "idoggetto = " & GbIdOggetto & " and numpcb = 1")
  If Not rsEntry.EOF Then
    If Len(rsEntry!pcb) > 0 Then
      IOPCB = rsEntry!pcb
    Else
      m_fun.WriteLog "Encaps IMSDC GOBACK - Missing IO-PCB in PsIMS_Entry - idpgm:(" & GbIdOggetto & ")"
    End If
  Else
    m_fun.WriteLog "Encaps IMSDC GOBACK - Missing IO-PCB in PsIMS_Entry - idpgm:(" & GbIdOggetto & ")"
  End If
  rsEntry.Close
  
  RTBModifica.SaveFile namePgm & "appo", 1
  Riga = 1
  ReDim arrFile(Riga)
  numFile = FreeFile
  Open namePgm & "appo" For Input As numFile
  Line Input #numFile, arrFile(Riga)
  bolEntry = False
  Do Until EOF(numFile)
    Riga = Riga + 1
    ReDim Preserve arrFile(Riga)
    Line Input #numFile, arrFile(Riga)
    'IRIS-DB non pi� necessaria
'''''    If InStr(8, arrFile(Riga), " COPY IRISGLOB") Then 'IRIS-DB
'''''       Riga = Riga + 1
'''''       ReDim Preserve arrFile(Riga)
'''''       arrFile(Riga) = Trim(START_TAG) & "     COPY IRISDCW."
'''''    End If
    If InStr(8, arrFile(Riga), "IRIS-FUNC-DLIT") Then
       bolEntry = True
    End If
    If bolEntry = True And InStr(8, arrFile(Riga), "CALL IRIS-WS-RTN") Then
       CurrRiga = arrFile(Riga)
       arrFile(Riga) = "IRISDB     SET IRIS-DFH-POINTER TO ADDRESS OF DFHEIBLK" 'IRIS-DB fisso per ora
       If Not dynamicSSA Then
         Riga = Riga + 1
         ReDim Preserve arrFile(Riga)
         arrFile(Riga) = "IRISDB     SET IRIS-ONLINE TO TRUE" 'IRIS-DB fisso per ora
       End If
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = CurrRiga
    End If
    If bolEntry = True And Left(arrFile(Riga), 6) <> "IRISDB" Then 'IRIS-DB veramente semplicistico, da rivedere (ad esempio il tag potrebbe essere un altro)
       CurrRiga = arrFile(Riga)
       arrFile(Riga) = Trim(START_TAG) & "     SET IRISROUT-RTN TO TRUE"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = Trim(START_TAG) & "     SET IRIS-FUNC-ENTR TO TRUE"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = Trim(START_TAG) & "     MOVE 2 TO IRIS-PARAM-NUM"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = Trim(START_TAG) & "     CALL IRIS-WS-RTN  USING DFHEIBLK"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = Trim(START_TAG) & "                             DFHCOMMAREA"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = Trim(START_TAG) & "                             IRIS-WORK-AREA"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = Trim(START_TAG) & "                             " & IOPCB & "." 'IRIS-DB the point is anyhow ok, as the ENTRY has to have the dot.
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = CurrRiga
       bolEntry = False
    End If
    If Mid(arrFile(Riga), 7, 1) = " " Then 'IRIS-DB gestione per ora semplificata del commento, potrebbe esserci il trattino di continuazione
       If InStr(8, arrFile(Riga), " GOBACK ") Or InStr(8, arrFile(Riga), " GOBACK.") Or Right(arrFile(Riga), 6) = "GOBACK" Then
          CurrRiga = arrFile(Riga)
          arrFile(Riga) = Trim(START_TAG) & "     SET IRISSND-RTN TO TRUE"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = Trim(START_TAG) & "     SET IRIS-FUNC-GOBA TO TRUE"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = Trim(START_TAG) & "     MOVE 1 TO IRIS-PARAM-NUM"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = Trim(START_TAG) & "     CALL IRIS-WS-RTN  USING IRIS-WORK-AREA"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = Trim(START_TAG) & "                             " & IOPCB
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = CurrRiga
       End If
    End If
  Loop
  Close numFile
    
'IRIS-DB: tutto spostato sopra per fare almeno un giro in meno , anche se in realt� ne fa comunque uno di troppo
'''''  'stefano: attenzione
'''''  'AddRighe = 0
'''''
'''''  '''''''''''''''''''''''''''''''''''''AsteriscaArraySunga ArrFile
'''''  Dim index As Integer, bolLink As Boolean, bolProc As Boolean, bolgoback As Boolean
'''''  Dim appostr As String, endtag As String
'''''  Dim tb As Recordset
'''''  Dim bolDC As Boolean, bolCics As Boolean, rowcommonarea As String
'''''
'''''  index = 1

'IRIS-DB: chiamata solo se online IMSDC, quindi non serve ulteriore check

'''''  Set tb = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & GbIdOggetto)
'''''  'gestire eventuale errore e togliere boldc
'''''  If Not tb.EOF Then
'''''    bolDC = tb!ims
'''''    bolCics = tb!Cics
'''''  End If
'''''  tb.Close
'''''
'''''  If Not bolDC And Not isMain(GbIdOggetto) Then  ' se batch e programma chiamato esco e non asterisco linkage
'''''    Exit Sub
'''''  End If
  
  'IRIS-DB: tiene solo la gestione della GOBACK
  
'''''  Do While (index <= UBound(arrFile)) And Not bolLink
'''''    If InStr(8, arrFile(index), "LINKAGE SECTION") And Not Mid(arrFile(index), 7, 1) = "*" Then
'''''      'arrFile(Index) = "BPHX-R*" & Right(arrFile(Index), Len(arrFile(Index)) - 7)
'''''      'non capivo a cosa serviva...
'''''      If Len(arrFile(index)) > 72 Then
'''''        endtag = Right(arrFile(index), Len(arrFile(index)) - 72)
'''''      Else
'''''        endtag = ""
'''''      End If
'''''      arrFile(index) = Left(arrFile(index), 72)
'''''      arrFile(index) = FormatTag(Right(arrFile(index), Len(arrFile(index)) - 7), endtag)
'''''      'stefano: attenzione, formattag aggiungeva una riga, per ora la tolgo...
'''''      arrFile(index) = Replace(arrFile(index), vbCrLf, "")
'''''      arrFile(index) = Left(arrFile(index), 6) & "*" & Right(arrFile(index), Len(arrFile(index)) - 7)
'''''      bolLink = True
'''''    Else
'''''      index = index + 1
'''''    End If
'''''  Loop
'''''  bolLink = False
'''''
'''''  Do While (index <= UBound(arrFile)) And Not bolLink
'''''    If InStr(8, arrFile(index), "DFHCOMMAREA") And Not Mid(arrFile(index), 7, 1) = "*" Then
'''''      'arrFile(Index) = "BPHX-R*" & Right(arrFile(Index), Len(arrFile(Index)) - 7)
'''''      'non capivo a cosa serviva...
'''''      rowcommonarea = Mid(arrFile(index), 8, IIf(Len(arrFile(index)) > 72, 65, Len(arrFile(index)) - 7))
'''''      If Len(arrFile(index)) > 72 Then
'''''        endtag = Right(arrFile(index), Len(arrFile(index)) - 72)
'''''      Else
'''''        endtag = ""
'''''      End If
'''''      arrFile(index) = Left(arrFile(index), 72)
'''''      arrFile(index) = FormatTag(Right(arrFile(index), Len(arrFile(index)) - 7), endtag)
'''''      'stefano: attenzione, formattag aggiungeva una riga, per ora la tolgo...
'''''      arrFile(index) = Replace(arrFile(index), vbCrLf, "")
'''''      arrFile(index) = Left(arrFile(index), 6) & "*" & Right(arrFile(index), Len(arrFile(index)) - 7)
'''''      bolLink = True
'''''    'AC 30/07/07
'''''    ElseIf InStr(8, arrFile(index), "PROCEDURE DIVISION") And Not Mid(arrFile(index), 7, 1) = "*" Then
'''''      bolLink = True
'''''    Else
'''''      index = index + 1
'''''    End If
'''''  Loop
'''''
'''''  'alberto
'''''  If bolCics Then
'''''   index = index + 1
'''''
'''''   Do While (index <= UBound(arrFile)) And Not bolProc
'''''    If InStr(8, arrFile(index), "PROCEDURE DIVISION") Then
'''''     arrFile(index) = FormatTag("LINKAGE SECTION.") & FormatTag("") _
'''''                     & FormatTag(Trim(rowcommonarea)) & FormatTag("") _
'''''                     & arrFile(index)
'''''     AddRighe = AddRighe + 4
'''''     bolProc = True
'''''    Else
'''''     index = index + 1
'''''    End If
'''''   Loop
'''''   bolProc = False
'''''  'stefano
'''''  End If
'''''  If bolDC Then
'''''
'''''   index = index + 1
'''''
'''''   Do While (index <= UBound(arrFile)) And Not bolProc
'''''    If InStr(8, arrFile(index), "PROCEDURE DIVISION") Then
'''''     arrFile(index) = FormatTag("LINKAGE SECTION.") & FormatTag("") _
'''''                     & FormatTag("01  DFHCOMMAREA.") _
'''''                     & FormatTag("    COPY DCSOCK.") & FormatTag("") _
'''''                     & arrFile(index)
'''''     'stefano: sistema vecchio, ma mi tocca usarlo...
'''''     AddRighe = AddRighe + 5
'''''     bolProc = True
'''''
'''''
'''''    Else
'''''     index = index + 1
'''''    End If
'''''   Loop
'''''

'IRIS-DB: da rifare: ci pu� essere pi� di un GOBACK e ci possono essere GOBACK asteriscati

''''''   Do While (index <= UBound(arrFile)) And Not bolgoback
''''''    If InStr(8, arrFile(index), "GOBACK") Then
''''''     If Len(arrFile(index)) > 72 Then
''''''       endtag = Right(arrFile(index), Len(arrFile(index)) - 72)
''''''     Else
''''''       endtag = ""
''''''     End If
''''''
'''''''     arrFile(Index) = Left(arrFile(Index), 72)
'''''''     appostr = FormatTag(Right(arrFile(Index), Len(arrFile(Index)) - 7), endtag)
'''''''     arrFile(Index) = Left(appostr, 6) & "*" & Right(appostr, Len(appostr) - 7) _
'''''''     & FormatTag("") & FormatTag("    COPY DCEXIT.") & FormatTag("")
''''''
''''''
''''''     arrFile(index) = FormatTag("    COPY DCEXIT.")
''''''     arrFile(index) = Left(arrFile(index), Len(arrFile(index)) - 2)
''''''
''''''     'stefano: sistema vecchio, ma mi tocca usarlo...
''''''
'''''''     AddRighe = AddRighe + 4
''''''     bolgoback = True
''''''    Else
''''''     index = index + 1
''''''    End If
''''''   Loop
''''''  End If

  numFile = FreeFile
  Open namePgm & "appo" For Output As numFile
  For i = 1 To UBound(arrFile)
    Print #numFile, arrFile(i)
  Next i
  Close numFile
  
  RTBModifica.LoadFile namePgm & "appo"
  Kill namePgm & "appo"
End Sub
Public Sub changeGOBACKPLI(RTBModifica As RichTextBox, namePgm As String, IdOggetto As Long)
  Dim arrFile() As String, numFile As Integer, Riga As Integer, i As Integer
  Dim CurrRiga As String
  Dim IOPCB As String
  Dim bolEntry As Boolean
  Dim rsEntry As Recordset
  Dim rsmain As Recordset
  Dim pcb() As String
  Dim pgmName As String
      
  On Error GoTo ErrGOBAPLI

  IOPCB = "Missing IOPCB"
  Set rsmain = m_fun.Open_Recordset("SELECT * FROM PsPgm WHERE Idoggetto = " & IdOggetto)
  If Not rsmain.EOF Then
    If Not rsmain!parameters = "" Then
      pcb = Split(rsmain!parameters, ",")
      IOPCB = Trim(pcb(0))
    End If
  End If
  rsmain.Close
  
  Set rsmain = m_fun.Open_Recordset("SELECT * FROM bs_oggetti WHERE Idoggetto = " & IdOggetto)
  If Not rsmain.EOF Then
    pgmName = rsmain!nome
  End If
  rsmain.Close
  
  RTBModifica.SaveFile namePgm & "appo", 1
  Riga = 1
  ReDim arrFile(Riga)
  numFile = FreeFile
  Open namePgm & "appo" For Input As numFile
  Line Input #numFile, arrFile(Riga)
  bolEntry = False
  Do Until EOF(numFile)
    Riga = Riga + 1
    ReDim Preserve arrFile(Riga)
    Line Input #numFile, arrFile(Riga)
    If InStr(arrFile(Riga), "IRIS_IMS_FUNCTION = 'DLIT'") Then
       bolEntry = True
       CurrRiga = arrFile(Riga)
       arrFile(Riga) = " IRIS_DFH_POINTER = ADDR(DFHEIBLK);"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " IRIS_PROGRAM_TYPE = 'T';"
       AddRighe = AddRighe + 1
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = CurrRiga
    End If
    If bolEntry = True And InStr(arrFile(Riga), "/* IRISDB MAIN END */") Then 'IRIS-DB veramente semplicistico, da rivedere
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " /* IRISDC ROUT START */"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " IRIS_WS_RTN = 'IRISROUT';"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " IRIS_IMS_FUNCTION = 'ENTR';"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " IRIS_PARAM_NUM = 2;"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " IRISIORT = 'IRISROUT';"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " CALL IRISIORT (DFHEIBLK,"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = "                DFHCOMMAREA,"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = "                IRIS_WORK_AREA,"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = "                " & IOPCB & ");"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " /* IRISDC ROUT END */"
    End If
    If bolEntry = True And (InStr(arrFile(Riga), " PROC;") Or InStr(arrFile(Riga), " PROC ;") Or InStr(arrFile(Riga), ":PROC;") Or InStr(arrFile(Riga), "END " & pgmName & ";")) Then 'IRIS-DB: veramente semplicistico... almeno dovrebbe gestire se c'� una sola PROC
       CurrRiga = arrFile(Riga)
       arrFile(Riga) = " /* IRISDC GOBA START */"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " IRIS_WS_RTN = 'IRISSEND';"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " IRIS_IMS_FUNCTION = 'GOBA';"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " IRIS_PARAM_NUM = 1;"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " IRISIORT = 'IRISSEND';"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " CALL IRISIORT (IRIS_WORK_AREA,"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = "                " & IOPCB & ");"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = " /* IRISDC GOBA END */"
       Riga = Riga + 1
       ReDim Preserve arrFile(Riga)
       arrFile(Riga) = CurrRiga
       bolEntry = False
     End If
  Loop
  Close numFile
  numFile = FreeFile
  Open namePgm & "appo" For Output As numFile
  For i = 1 To UBound(arrFile)
    Print #numFile, arrFile(i)
  Next i
  Close numFile
  
  RTBModifica.LoadFile namePgm & "appo"
  Kill namePgm & "appo"
  Exit Sub
ErrGOBAPLI:
  m_fun.WriteLog "changeGOBACKPLI - Error - idoggetto:(" & IdOggetto & ")"
End Sub

Public Sub INSERT_NODECOD_DC(wPos As Long, RTBModifica As RichTextBox, rst As Recordset, typeIncaps As String) 'IRIS-DB
    Dim k As Integer
   Dim wIstr As String
   Dim Testo As String
   Dim wIstruzione As String
   Dim wResp As Boolean
   Dim codOperazione As String
   Dim s() As String
   Dim wNomeRoutine As String
   Dim cc As Long
   Dim c2 As Long
   Dim wRetCodePcb As String
   
   Testo = vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
   Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "*   OBSOLETE INSTRUCTION"
   Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
   Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "     MOVE 'GE' TO " & Trim(rst!numpcb) & "(11:2)"

   If wPunto Then
      Testo = Testo & "."
   End If
   
   'Testo = Left(Testo, Len(Testo) - 2)
   
   RTBModifica.SelStart = wPos
   RTBModifica.SelLength = 0
   RTBModifica.SelText = Testo
   
   wPos = wPos + Len(Testo)
   
   s = Split(Testo, vbCrLf)
     
   AddRighe = AddRighe + UBound(s)
End Sub

