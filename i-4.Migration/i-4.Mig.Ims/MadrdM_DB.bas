Attribute VB_Name = "MadrdM_DB"
Option Explicit

Public Function Exist_Copy(NCopy As String) As Boolean
  Dim r As Recordset
  Set r = m_fun.Open_Recordset_Sys("Select * From BS_Oggetti Where Nome = '" & NCopy & "' And Tipo = 'CPY'")
  Exist_Copy = r.RecordCount
  r.Close
End Function

Public Function Exist_PSB(NPSB As String) As Boolean
  'Controlla se esiste la copy all'interno dell'archivio
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & NPSB & "' And Tipo = 'PSB'")
  Exist_PSB = r.RecordCount > 0
  r.Close
End Function

Function Crea_Directory_Progetto(Directory As String, PathD As String) As String
  Dim WDollaro As Long
    
  WDollaro = InStr(1, Directory, "$")
  If WDollaro <> 0 Then
    Crea_Directory_Progetto = PathD & Trim(Mid(Directory, WDollaro + 1))
    Exit Function
  End If
End Function

Public Function Crea_Directory_Parametrica(Directory As String, GbPathDef As String) As String
   Dim WSlch As Long
   Dim Old As Long
   
   Dim DirP As String
   
   Dim i As Long
   Dim j As Long
   
   'Toglie la prima parte del percorso che trova uguale al contenuto di GbPathDef
   i = InStr(1, Directory, GbPathDef)
   
   If i <> 0 Then
    DirP = Mid(Directory, Len(GbPathDef) + 1)
   Else
    If InStr(1, Directory, "\\") <> 0 Then
      For j = 1 To Len(GbPathDef)
        If Mid(GbPathDef, Len(GbPathDef) - j, 1) = "\" Then
          Exit For
        End If
      Next j
      
      If InStr(1, Directory, "\" & Mid(GbPathDef, j)) <> 0 Then
        DirP = Mid(Directory, InStr(1, Directory, "\" & Mid(GbPathDef, j)) + j + 1)
      End If
    End If
   End If
   
   Crea_Directory_Parametrica = "$" & DirP
End Function
Public Function Restituisci_NomeOgg_Da_IdOggetto(Id As Long) As String
  'restituisce la directory dall'id oggetto secondo il tipo INPUT,OUTPUT
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Id)
  If r.RecordCount Then Restituisci_NomeOgg_Da_IdOggetto = Trim(r!nome)
  r.Close 'ALE 28/04/2006
End Function

Public Function Restituisci_TipoOgg_Da_IdOggetto(Id As Long) As String
  'restituisce la directory dall'id oggetto secondo il tipo INPUT,OUTPUT
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & Id)
  If r.RecordCount Then
    Restituisci_TipoOgg_Da_IdOggetto = r!tipo
  End If
  r.Close
End Function

Public Function Restituisci_Percorso_Da_NomeOggetto(nome As String) As String
  'restituisce la directory dall'id oggetto secondo il tipo INPUT,OUTPUT
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select Directory_Input, estensione From BS_Oggetti Where Nome = '" & nome & "' and tipo = 'CPY'")
  If r.RecordCount Then
    Restituisci_Percorso_Da_NomeOggetto = Crea_Directory_Progetto(r!Directory_Input, Dli2Rdbms.drPathDef) & "\" & nome
    If Len(r!estensione) Then
      Restituisci_Percorso_Da_NomeOggetto = Restituisci_Percorso_Da_NomeOggetto & "." & r!estensione
    End If
  End If
  r.Close
End Function

Public Function Componi_Messaggio_Errore(IdObj As Long, Code As String, Riga As Long) As String
  Dim r As Recordset, R1 As Recordset
  Dim Testo As String
  Dim Var1 As Variant 'non sono stringhe?!
  Dim Var2 As Variant
  Dim App As String
  Dim i As Long
  Dim sx As String
  Dim dx As String
  
  Set r = m_fun.Open_Recordset("Select * From BS_Segnalazioni Where Codice = '" & Code & "' And IdOggetto = " & IdObj & " And Riga = " & Riga)
  If r.RecordCount Then
    '� un solo record
    'App = Mid(r!Codice, Len(r!Codice) - 3, 3) 'Prende gli ultimi 4 caratteri meno l'ultimo che rappresentano il codice puro
    
    App = r!Codice
    
    Set R1 = m_fun.Open_Recordset_Sys("Select * From BS_Messaggi Where Codice = '" & App & "'")
    If R1.RecordCount Then
      'deve essere solo uno
      Testo = R1!Testo
    End If
    R1.Close
    
    'prende le variabili Var<x>
    Var1 = Trim(r!Var1)
    Var2 = Trim(r!Var2 & "")
    
    'Sostituisce Var1
    i = InStr(1, Testo, "%VAR1%")
    If i <> 0 Then
      If i > 1 Then
        sx = Mid(Testo, 1, i - 2)
      Else
        sx = ""
      End If
      dx = Mid(Testo, i + 7)
      Testo = sx & " " & Var1 & " " & dx
    End If
    
    'Sostituisce Var2
    'i = InStr(testo, "%VAR2%")
    'If i Then
      'sx = Left(testo, i - 2)
      'dx = Mid(testo, i + 7)
      'testo = sx & " " & Var2 & " " & dx
    'End If
    Testo = Replace(Testo, "%VAR2%", Var2 & "")
  End If
  r.Close

  Componi_Messaggio_Errore = Trim(Testo)
End Function

Public Function Is_External_Obj(idOggetto As Long) As Boolean
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & idOggetto)
  If r.RecordCount Then
    Is_External_Obj = False
  Else
    Is_External_Obj = True
  End If
  r.Close
End Function
