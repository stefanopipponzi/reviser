Attribute VB_Name = "MaimdM_Genrout"
Option Explicit

Global TbIstr As Recordset
Global TbDett As Recordset
Global TbOgg As Recordset
Global tbSegm As Recordset
Global TbField As Recordset
Global TbOperaz As Recordset
Global TbOggElab As Recordset
Global TbRela As Recordset
Global TbPgm As Recordset

Global GbIntestazionePerform As String

Public Type DefOperatori
  Cod As String
  IdOgg As String
  Riga As String
  Programma() As String
End Type

Public Type Indenta_Liv
  livello As Integer
  Indet As Integer
End Type

Public Type DefOperaz_DC
  Istruzione As String
  Routine As String
  nDb As String
  Decodifica As String
  oper() As DefOperatori
  SwTrattata As Boolean
  NumChiamate As Integer
  IsCICS As Boolean
End Type

Public wIstrCod_DC() As DefOperaz_DC

'SQ merge Public gbCurInstr As String
'SQ merge Public GbNumDial As Long
'SQ merge Public mIdDBDCorrente As Long
'SQ merge Public mIdOggettoCorrente As Long
'SQ merge Public GbNumIstr As String
'SQ merge Public Gistruzione As String

Global evaluate As Boolean
Global ultimappar As Boolean

Global RoutineMap As String
Global NameStructI As String
Global NameStructO As String
Global NameMoveI As String
Global NameMoveO  As String
Global mapping As Boolean
Global typeTempl As String
Global lasttranid As String
'contiene l'istruzione ImsDC
Public ImsDCIstr As IstrImsDC

Public ColCampiDflattr As Collection
Private testoPFKFLD As String, lenPFKField As Integer

Public Function SubToken(ptoken As String, pintervallo() As String) As String
  If pintervallo(0) = "" And pintervallo(1) = "" Then
    SubToken = ptoken
  ElseIf Not pintervallo(0) = "" And pintervallo(1) = "" Then
    SubToken = IIf(Len(ptoken) >= CInt(pintervallo(0)), Left(ptoken, CInt(pintervallo(0))), ptoken)
  ElseIf pintervallo(0) = "" And Not pintervallo(1) = "" Then
    SubToken = IIf(Len(ptoken) >= CInt(pintervallo(1)), Right(ptoken, CInt(pintervallo(1))), ptoken)
  ElseIf Len(ptoken) >= CInt(pintervallo(0)) And Len(ptoken) >= (CInt(pintervallo(0)) + CInt(pintervallo(1)) - 1) Then
    SubToken = Mid(ptoken, pintervallo(0), pintervallo(1))
  End If
End Function

Public Function RetrieveParameterValues(sParameterKey As String) As Variant
  Dim rParameterValue As Recordset, rSave As Recordset

  Dim arrParameterValues() As String
  Dim i As Integer
    
    On Error GoTo EH

    Set rParameterValue = m_fun.Open_Recordset_Sys("Select [Parameters].ID, [Parameters].ParameterName, " & _
        "[Parameters].ParameterLabel, [Parameters].ParameterDescription, " & _
        "[Parameters].ParameterTypeID, [Parameters].ParameterSelectTypeID, " & _
        "ParameterTypes.ParameterTypeName, ParameterSelectTypes.ParameterSelectTypeName " & _
        "From [Parameters], ParameterTypes, ParameterSelectTypes  " & _
        "WHERE " & _
        "ParameterSelectTypes.ID = [Parameters].ParameterSelectTypeID AND " & _
        "ParameterTypes.ID = [Parameters].ParameterTypeID AND [Parameters].ParameterName = '" & sParameterKey & "' ORDER BY [Parameters].ParameterName ")

    If rParameterValue.RecordCount > 0 Then
        Set rSave = m_fun.Open_Recordset("SELECT BS_Parametri.ID, BS_Parametri.ParameterSystemID, BS_Parametri.ParameterValue FROM BS_Parametri WHERE ParameterSystemID = " & rParameterValue.Fields("ID"))
        If rSave.RecordCount > 0 Then
            i = 0
            ReDim arrParameterValues(0 To rSave.RecordCount - 1)
            Do While Not rSave.EOF
                arrParameterValues(i) = rSave.Fields("ParameterValue").Value
                i = i + 1
                rSave.MoveNext
            Loop
        Else
        
            ReDim arrParameterValues(0)
            
            arrParameterValues(0) = "PARAMETER VALUES NOT SET IN LOCAL PROJECT FOR KEY: " & sParameterKey

        End If
        rSave.Close
        Set rSave = Nothing
    Else
        ReDim arrParameterValues(0)
        arrParameterValues(0) = "PARAMETER KEY NOT FOUND IN SYSTEM.MDB FOR: " & sParameterKey
    End If

    rParameterValue.Close
    Set rParameterValue = Nothing

    RetrieveParameterValues = arrParameterValues

    Exit Function
EH: MsgBox err.Description, vbOKOnly, err.Source
    err.Clear
    
    If Not rSave Is Nothing Then
        rSave.Close
        Set rSave = Nothing
    End If
    If Not rParameterValue Is Nothing Then
        rParameterValue.Close
        Set rParameterValue = Nothing
    End If
    
End Function

Public Function getEnvironmentParam(ByVal parameter As String, envType As String, environment As Collection) As String
  Dim token As String
  Dim subTokens() As String
  'intervallo:
  Dim intervallo() As String, wstr As String, K1 As Integer
  Dim rsMFLD As New Recordset
  Dim thisItem As String
  
  On Error GoTo errdb
  
  'enviromnent
  '1= IdOggetto
  '2= nome del file FMS
  
  Select Case envType
    Case "mappe"
       While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = UCase(getEnvironmentParam & token)
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          If InStr(1, token, "#") Then
            intervallo = Split(subTokens(1), "-")
            'IRIS-DB if the name is shorter than the requested part, takes the full name to avoid duplicates
            'IRIS-DB per ora fa una iffaccia, non capisco perch� abbiano deciso di usare una collection per 3 valori, senza neanche creare le chiave
            If subTokens(0) = "FMT-NAME" Then
              thisItem = environment.item(3)
            Else
              thisItem = environment.item(2)
            End If
            If Len(thisItem) < intervallo(1) Then
              ReDim Preserve intervallo(1)
              intervallo(0) = ""
              intervallo(1) = ""
            End If
          Else
            ReDim Preserve intervallo(1)
            intervallo(0) = ""
            intervallo(1) = ""
          End If
          Select Case subTokens(0)
            Case "FMT-NAME"
'              Set rsMFLD = m_fun.Open_Recordset("Select FMTName from PsDLI_MFS where IdOggetto = " & environment.Item(1) & " and DevType = '(3270,2)'")
'              If Not rsMFLD.EOF Then
'                getEnvironmentParam = getEnvironmentParam & SubToken(rsMFLD!FMTName, intervallo)
'              End If
'              rsMFLD.Close
               getEnvironmentParam = UCase(getEnvironmentParam & SubToken(environment.item(3), intervallo))
 
            Case "MFS-FILENAME"
              getEnvironmentParam = UCase(getEnvironmentParam & SubToken(environment.item(2), intervallo))
            
            Case "PROJECT-NAME"
              wstr = m_ImsDll_DC.ImNomeDB
              K1 = InStr(wstr, ".")
              If K1 > 0 Then wstr = Mid$(wstr, 1, K1 - 1)
              getEnvironmentParam = UCase(getEnvironmentParam & SubToken(wstr, intervallo))
            End Select
        End If
      Wend
    End Select
  Exit Function
errdb:
  If err.Number = 13 Then
    'sbagliato l'intervallo numerico
    MsgBox "Wrong definition for " & parameter, vbExclamation
  Else
    MsgBox err.Description
    'Resume
  End If
  getEnvironmentParam = ""
End Function
Public Function Transcode_Istruction_DLI_DC(wIstr As String) As String
    Dim xwIstr As String
    
    Select Case Trim(UCase(wIstr))
        Case "GU"
            xwIstr = "RGU"
        
        Case "GN"
          xwIstr = "RGN"
       
        Case "ISRT"
          xwIstr = "SISR"
        
        Case Else
          xwIstr = "DCUK"
          m_fun.WriteLog "ImsDC : Transcode_Istruction_DLI_DC - Function - Istruzione non riconosciuta : " & wIstr, "DR - GenRout"
    End Select
    
    Transcode_Istruction_DLI_DC = xwIstr
End Function

Public Function Create_NomeRoutine_IMSDC(wIstruzione As String) As String
   Dim wApp As Variant
   
   Dim wRet As String
   
   wRet = "[ROUTCALL-NO-CRITERIA-DEFINITION]"
    
   wApp = m_fun.RetrieveParameterValues("IMSDC-ROUTCALL-VAR-KEY")
    
   If InStr(1, wApp(0), "PARAMETER KEY NOT FOUND") > 0 Then
        Create_NomeRoutine_IMSDC = wRet
        Exit Function
   End If
    
   If InStr(1, wApp(0), "PARAMETER VALUES NOT SET") > 0 Then
        Create_NomeRoutine_IMSDC = wRet
        Exit Function
   End If
   
   Select Case wIstruzione
      Case "GU"
         Create_NomeRoutine_IMSDC = m_fun.pNomeRoutineDC_Receive
      Case "GN"
         Create_NomeRoutine_IMSDC = m_fun.pNomeRoutineDC_Receive
      Case "ISRT"
         Create_NomeRoutine_IMSDC = m_fun.pNomeRoutineDC_Send
      Case "CHKP", "XSRT"
         Create_NomeRoutine_IMSDC = "DC"
      'AC : implementazione altre istruzioni
      Case "XRST"
        Create_NomeRoutine_IMSDC = "DC"
      Case "ROLL"
        Create_NomeRoutine_IMSDC = "DCROLL"
      Case "ROLB"
        Create_NomeRoutine_IMSDC = "DCROLB"
      Case "CHNG"
        Create_NomeRoutine_IMSDC = "DC"
      Case "PURG"
        Create_NomeRoutine_IMSDC = "DC"
      Case "AUTH"
        Create_NomeRoutine_IMSDC = "DCAUTH"
      Case "INQY"
        Create_NomeRoutine_IMSDC = "DCINQY"
'IRIS-DB      Case "ICMD"
'IRIS-DB        Create_NomeRoutine_IMSDC = "DCICMD"
      Case "CMD" 'IRIS-DB
        Create_NomeRoutine_IMSDC = "DCCMD" 'IRIS-DB
                                     
        '*******************************
   
   End Select
End Function

Public Sub Crea_Routine_DLI_DC(RTBR As RichTextBox, nroutine As String, nDb As String)
    Dim wstr As String
    Dim pos4 As Long
    Dim Percorso As String
    
    'carica Template routine
    Load_Template_Routine_DLI_DC RTBR, SwTp
    
    'Modifica l'intestazione
    Insert_Intestazione_Routine_DC RTBR, nroutine, nDb
    
    wstr = m_ImsDll_DC.ImNomeDB
    pos4 = InStr(wstr, ".")
    If pos4 > 0 Then wstr = Mid$(wstr, 1, pos4 - 1)
    
    Percorso = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\dli\"
    
    RTBR.SaveFile Percorso & Trim(nroutine) & ".cbl", 1
    RTBR.LoadFile Percorso & Trim(nroutine) & ".cbl"
End Sub

Public Sub Load_Template_Routine_DLI_DC(RTBR As RichTextBox, SwTp As Boolean)
    Dim wOpRt As String
    Dim wPath As String
    Dim a As Long
    
    If RTBR.text <> "" Then
       RTBR.SaveFile RTBR.FileName, 1
    End If
    
    wPath = m_fun.FnPathPrj
    
    If SwTp Then
       
'stefanopul
'      RTBR.LoadFile wPath & "\input-prj\routbs\routtpIMSDC.cbl"
      RTBR.LoadFile wPath & "\input-prj\Template\imsdc\standard\routines\routtpIMSDC.cbl"
           
    Else
      
'stefanopul
     ' RTBR.LoadFile wPath & "\input-prj\routbs\routbtIMSDC.cbl"
      RTBR.LoadFile wPath & "\input-prj\Template\imsdc\standard\routines\routbtIMSDC.cbl"
           
    End If

    AggLog " --> Load Routine Template...", MaimdF_GenRout.RtLog
End Sub

Public Sub Load_Template_Routine_DLI_CICS(RTBR As RichTextBox, SwSend As Boolean)
    Dim wOpRt As String
    Dim wPath As String
    Dim a As Long
    
    On Error GoTo fileErr
    
    If Len(RTBR.text) Then
      RTBR.SaveFile RTBR.FileName, 1
    End If
    
    If SwSend Then
'stefanopul
'      wPath = m_fun.FnPathPrj & "\input-prj\routbs\RoutDC_SENDGen.cbl"
      wPath = m_fun.FnPathPrj & "\input-prj\Template\imsdc\standard\routines\RoutDC_SENDGen.cbl"
    Else
'stefanopul
 '     wPath = m_fun.FnPathPrj & "\input-prj\routbs\RoutDC_RECEIVEGen.cbl"
      wPath = m_fun.FnPathPrj & "\input-prj\Template\imsdc\standard\routines\RoutDC_RECEIVEGen.cbl"
    End If
    
    RTBR.LoadFile wPath
    
    AggLog " --> Load Routine Template...", MaimdF_GenRout.RtLog
    Exit Sub
fileErr:
    If err.Number = 75 Then
      MsgBox "Template file not found: '" & wPath & "'", vbExclamation, "i-4: routine generator"
    Else
      MsgBox "tmp: " & err.Description, vbCritical, "i-4.Migration: routine generator"
    End If
    err.Raise err.Number
End Sub
Public Sub Load_Template_Routine_DLI_CICSOneMap(RTBOutMap As RichTextBox, SwSend As Boolean, Optional swUnified As Boolean)
    Dim wOpRt As String
    Dim wPath As String
    Dim a As Long
    
    If RTBOutMap.text <> "" Then
       RTBOutMap.SaveFile RTBOutMap.FileName, 1
    End If
    
    wPath = m_fun.FnPathPrj
    
    If Not swUnified Then 'IRIS-DB check if this works even without value
      If SwSend Then
        If Not mapping Then
          RTBOutMap.LoadFile wPath & "\input-prj\Template\imsdc\standard\routines\RoutDC_SEND" & typeTempl & ".cbl"
        Else
          RTBOutMap.LoadFile wPath & "\input-prj\Template\imsdc\standard\routines\RoutDC_SENDJ.cbl"
        End If
      Else
        If Not mapping Then
          RTBOutMap.LoadFile wPath & "\input-prj\Template\imsdc\standard\routines\RoutDC_RECEIVEM.cbl"
        Else
          RTBOutMap.LoadFile wPath & "\input-prj\Template\imsdc\standard\routines\RoutDC_RECEIVEJ.cbl"
        End If
      End If
    Else
      RTBOutMap.LoadFile wPath & "\input-prj\Template\imsdc\standard\routines\RoudDC_SENDRECEIVEM.cbl"
    End If

    'AggLog " --> Load Routine Template...", MaimdF_GenRout.RtLog
End Sub

Public Sub Insert_Intestazione_Routine_DC(RTBR As RichTextBox, nroutine As String, nDb As String)
    Dim pos2 As Long
    Dim pos6 As Long
    Dim posFine As Long
    Dim posFine1 As Long
    Dim pos3 As Long
    
    'INSERISCE LE DATE DI CREAZIONE E DI MODIFICA
    pos2 = RTBR.find("DATA-CREAZ", 0)
    If pos2 > -1 Then
      RTBR.SelText = Format(Date, "DD MM YYYY")
    End If
    
    pos2 = RTBR.find("AUTHOR-RTE", 0)
    If pos2 > -1 Then
      RTBR.SelText = m_fun.LABEL_AUTHOR
    End If
    
    pos2 = RTBR.find("DATA-MODIF", 0)
    If pos2 > -1 Then
      RTBR.SelText = Format(Date, "DD MM YYYY")
    End If
    
    'INSERISCE IL NOME DELLA ROUTINE ALL'INIZIO
    pos6 = RTBR.find("NOME-RTE", 0)
    If pos6 > -1 Then
      RTBR.SelText = nroutine
    End If
    
    posFine = RTBR.find("FINE-ROUTINES", 0)
    
    posFine1 = RTBR.find("NOME-RTE", 0)
    
    If posFine1 > -1 Then
      RTBR.SelText = nroutine
    End If
    
    'INSERISCE IL NOME DEL DATABASE DLI
    
    pos3 = RTBR.find("DB-DLI", 0)
    If pos3 > -1 Then
      RTBR.SelText = nDb
    End If
End Sub

Public Sub Insert_Intestazione_Routine_CICS(RTBR As RichTextBox, nroutine As String, nDb As String)
    Dim pos2 As Long
    Dim pos6 As Long
    Dim posFine As Long
    Dim posFine1 As Long
    Dim pos3 As Long
    
    'INSERISCE LE DATE DI CREAZIONE E DI MODIFICA
    pos2 = RTBR.find("DATA-CREAZ", 0)
    If pos2 > -1 Then
      RTBR.SelText = Format(Date, "DD MM YYYY")
    End If
    
    pos2 = RTBR.find("AUTHOR-RTE", 0)
    If pos2 > -1 Then
      RTBR.SelText = m_fun.LABEL_AUTHOR
    End If
    
    pos2 = RTBR.find("DATA-MODIF", 0)
    If pos2 > -1 Then
      RTBR.SelText = Format(Date, "DD MM YYYY")
    End If
    
    'INSERISCE IL NOME DELLA ROUTINE ALL'INIZIO
    pos6 = RTBR.find("NOME-RTE", 0)
    If pos6 > -1 Then
      RTBR.SelText = nroutine
    End If
    
    posFine = RTBR.find("FINE-ROUTINES", 0)
    
    posFine1 = RTBR.find("NOME-RTE", 0)
    
    If posFine1 > -1 Then
      RTBR.SelText = nroutine
    End If
    
    'INSERISCE IL NOME DEL DATABASE DLI
    
    pos3 = RTBR.find("DB-DLI", 0)
    If pos3 > -1 Then
      RTBR.SelText = nDb
    End If
End Sub

Public Function CaricaIstruzioneMappa_DLI_CICS(wIdOggetto As Long, rtbroutine As RichTextBox, rtbOneMap As RichTextBox, NewIstr As String, MapName As String) As Boolean
    Dim tb As Recordset, rs As Recordset
    Dim tb1 As Recordset
    Dim tb2 As Recordset
    Dim TbKey As Recordset
    Dim FTMrecordset As Recordset
    
    Dim IndI As Integer
    Dim k As Integer
    Dim K1 As Integer
    Dim k2 As Integer
    Dim k3 As Integer
    
    Dim vEnd As Double
    Dim vPos As Double
    Dim vPos1 As Double
    Dim vStart As Double
    Dim wstr As String
    Dim rst As Recordset
    Dim xrout As String
    Dim TbPrg As Recordset
    
    Dim sDbd() As Long
    Dim sPsb() As Long
    
    Dim sCurIdTable As Long
    
    
    Dim colInfo As New Collection
    Dim fmtName As String, rsMFLD As Recordset, msginput As String
    
    colInfo.Add wIdOggetto
    colInfo.Add MapName
    colInfo.Add " "
    
    wstr = m_ImsDll_DC.ImNomeDB
    K1 = InStr(wstr, ".")
    If K1 > 0 Then wstr = Mid$(wstr, 1, K1 - 1)
    
    '********
    Set FTMrecordset = m_fun.Open_Recordset("select * from PsDLI_MFS where idoggetto = " & wIdOggetto) '& " and DevType = '(3270,2)'")
    While Not FTMrecordset.EOF
     fmtName = FTMrecordset!fmtName
     Set rs = m_fun.Open_Recordset("select DevType from PsDli_MFS  where FMTName ='" & fmtName & "'")
      If Not rs.EOF Then
        If InStr(rs!DevType, "3270,2") > 0 Then
          typeTempl = "M"
        Else
          typeTempl = "P"
        End If
      End If
     rs.Close
     colInfo.Remove (3)
     colInfo.Add fmtName
    
     If SwSend Then
       RoutineMap = getEnvironmentParam(RetrieveParameterValues("IMSDC-SENDMAP")(0), "mappe", colInfo)
     Else
       RoutineMap = getEnvironmentParam(RetrieveParameterValues("IMSDC-RECEIVEMAP")(0), "mappe", colInfo)
     End If
     
     NameStructI = getEnvironmentParam(RetrieveParameterValues("IMSDC-IMSSTRUCTUREI")(0), "mappe", colInfo)
     NameStructO = getEnvironmentParam(RetrieveParameterValues("IMSDC-IMSSTRUCTUREO")(0), "mappe", colInfo)
     NameMoveI = getEnvironmentParam(RetrieveParameterValues("IMSDC-COPYMOVEI")(0), "mappe", colInfo)
     NameMoveO = getEnvironmentParam(RetrieveParameterValues("IMSDC-COPYMOVEO")(0), "mappe", colInfo)
     
     
     ' creazione routine singola mappa
     Crea_Routine_DLI_CICSOneMap rtbOneMap, RoutineMap, "", False
         
'     If InStr(rtbroutine.filename, RoutineMap) = 0 Then
'        rtbroutine.SaveFile rtbroutine.filename, 1
'        rtbroutine.LoadFile m_ImsDll_DC.ImPathDB & "\" & wStr & "\output-prj\cics\Cbl\" & RoutineMap & ".cbl"
'     End If
     If InStr(rtbroutine.FileName, nomeRoutine) = 0 Then
       rtbroutine.SaveFile rtbroutine.FileName, 1
       rtbroutine.LoadFile m_ImsDll_DC.ImPathDB & "\" & wstr & "\output-prj\cics\" & nomeRoutine & ".cbl"
    End If
         
     ImsDCIstr.istr = Trim(NewIstr)
     ImsDCIstr.IdOggetto = wIdOggetto
     
     'Carica i campi
     ReDim ImsDCIstr.Campi(0)
     
     Set rst = m_fun.Open_Recordset("select * from PsDLI_MFSCmp where FMTName = '" & fmtName & "' and DevType = '(3270,2)' Order by Riga,Colonna")
    
     
     While Not rst.EOF
       If Not Left(rst!nome, 1) = "#" Then
         ReDim Preserve ImsDCIstr.Campi(UBound(ImsDCIstr.Campi) + 1)
         ImsDCIstr.Campi(UBound(ImsDCIstr.Campi)) = rst!nome
       End If
       rst.MoveNext
     Wend
     
     Set rsMFLD = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where FMTName = '" & fmtName & "' And DevType = 'INPUT'")
     If Not rsMFLD.EOF Then
      msginput = rsMFLD!MSGName
     End If
     rsMFLD.Close
     If Not mapping Then
      AggRoutDLI_CICS wIdOggetto, rst, rtbroutine, rtbOneMap, fmtName, msginput 'm_fun.Restituisci_NomeOgg_Da_IdOggetto(wIdOggetto)
     Else
     '****** gestione routine aree xml/java
     End If
     '************** RTBOutMap
      vPos = rtbOneMap.find("#COPYMOVE#.", 1)
      rtbOneMap.SelText = ""
          
      vPos = rtbOneMap.find("#OPEREC#", 1)
     rtbOneMap.SelText = ""
      
      
      vPos = rtbOneMap.find("*#BEGINFIELD#", 1)
      rtbOneMap.SelText = ""
      rtbOneMap.SaveFile rtbroutine.FileName, 1
      'RTBOutMap.SaveFile RTBOutMap.filename, 1
     
     
     '*************
     FTMrecordset.MoveNext
    Wend
    FTMrecordset.Close
    CaricaIstruzioneMappa_DLI_CICS = True
End Function
Public Function CreaRoutineUnified_DLI_CICS(wIdOggetto As Long, RTCopy As RichTextBox, rtbOneMap As RichTextBox, MapName As String) As Boolean 'IRIS-DB all function

    Dim tb As Recordset, rs As Recordset
    Dim tb1 As Recordset
    Dim tb2 As Recordset
    Dim TbKey As Recordset
    Dim FTMrecordset As Recordset
    
    Dim IndI As Integer
    Dim k As Integer
    Dim K1 As Integer
    Dim k2 As Integer
    Dim k3 As Integer
    
    Dim vEnd As Double
    Dim vPos As Double
    Dim vPos1 As Double
    Dim vStart As Double
    Dim wstr As String
    Dim rst As Recordset
    Dim xrout As String
    Dim TbPrg As Recordset
    
    Dim sDbd() As Long
    Dim sPsb() As Long
    
    Dim sCurIdTable As Long
    
    
    Dim colInfo As New Collection
    Dim fmtName As String, rsMFLD As Recordset, msginput As String
    Dim wMapName As String
    On Error GoTo errFun 'IRIS-DB
    colInfo.Add wIdOggetto
    colInfo.Add MapName
    
    colInfo.Add " "
    
    wstr = m_ImsDll_DC.ImNomeDB
    K1 = InStr(wstr, ".")
    If K1 > 0 Then wstr = Mid$(wstr, 1, K1 - 1)
    
    '********
    Set FTMrecordset = m_fun.Open_Recordset("select * from PsDLI_MFS where idoggetto = " & wIdOggetto) '& " and DevType = '(3270,2)'")
    While Not FTMrecordset.EOF
     fmtName = FTMrecordset!fmtName
     'IRIS-DB non so perch� non lo facesse gi� prendiamoci questo rischio....
     colInfo.Remove (3)
     colInfo.Add fmtName
     Set rs = m_fun.Open_Recordset("select DevType from PsDli_MFS  where FMTName ='" & fmtName & "'")
      If Not rs.EOF Then
        If InStr(rs!DevType, "3270,2") > 0 Then
          typeTempl = "M"
        Else
          typeTempl = "P"
        End If
      End If
     rs.Close
     RoutineMap = getEnvironmentParam(RetrieveParameterValues("IMSDC-SENDRECEIVEMAP")(0), "mappe", colInfo)
     NameStructI = getEnvironmentParam(RetrieveParameterValues("IMSDC-IMSSTRUCTUREI")(0), "mappe", colInfo)
     NameStructO = getEnvironmentParam(RetrieveParameterValues("IMSDC-IMSSTRUCTUREO")(0), "mappe", colInfo)
     NameMoveI = getEnvironmentParam(RetrieveParameterValues("IMSDC-COPYMOVEI")(0), "mappe", colInfo)
     NameMoveO = getEnvironmentParam(RetrieveParameterValues("IMSDC-COPYMOVEO")(0), "mappe", colInfo)
     wMapName = Left(getEnvironmentParam(RetrieveParameterValues("IMSDC-BMSMAP")(0), "mappe", colInfo), 7)

     If m_ImsDll_DC.ImNomeDB = "LandG.mty" Then 'IRIS-DB: pezza per evitare di riconsegnare tutti i programmi a L&G; da rimuovere in seguito
       If FTMrecordset.RecordCount = 1 Then
         Set rs = m_fun.Open_Recordset("Select Nome from Bs_oggetti where idoggetto = " & wIdOggetto)
         If Not rs.EOF Then
           NameMoveI = rs!nome & "MI"
           NameMoveO = rs!nome & "MO"
           NameStructI = rs!nome & "SI"
           NameStructO = rs!nome & "SO"
         End If
         rs.Close
       End If
     End If
     
     ' creazione routine singola mappa
     If FTMrecordset.AbsolutePosition = 1 Then
       Crea_Routine_DLI_CICSOneMap rtbOneMap, RoutineMap, "", True
     End If
         
     'ImsDCIstr.istr = Trim(NewIstr)
     ImsDCIstr.IdOggetto = wIdOggetto
     
     'Carica i campi
     ReDim ImsDCIstr.Campi(0)
     
     Set rst = m_fun.Open_Recordset("select * from PsDLI_MFSCmp where FMTName = '" & fmtName & "' and DevType = '(3270,2)' Order by Riga,Colonna")
         
     While Not rst.EOF
       If Not Left(rst!nome, 1) = "#" Then
         ReDim Preserve ImsDCIstr.Campi(UBound(ImsDCIstr.Campi) + 1)
         ImsDCIstr.Campi(UBound(ImsDCIstr.Campi)) = rst!nome
       End If
       rst.MoveNext
     Wend
     
     Set rsMFLD = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where FMTName = '" & fmtName & "' And DevType = 'INPUT'")
     If Not rsMFLD.EOF Then
      msginput = rsMFLD!MSGName
     End If
     rsMFLD.Close
     AggRoutDLI_CICS_Unified wIdOggetto, rst, RTCopy, rtbOneMap, fmtName, msginput, FTMrecordset.RecordCount, FTMrecordset.AbsolutePosition 'IRIS-DB: aggiunto numero formati per gestione multimappa
     '****** gestione routine aree xml/java
     '************** RTBOutMap
''      vPos = rtbOneMap.find("#ATTR-AREAS#", 1)
''      rtbOneMap.SelText = ""
     If FTMrecordset.RecordCount = 1 Then
       vPos = rtbOneMap.find("#COPYMOVE#.", 1)
       rtbOneMap.SelText = ""
       vPos = rtbOneMap.find("#OPEREC#", 1)
       rtbOneMap.SelText = ""
       vPos = rtbOneMap.find("#OPE#", 1)
       rtbOneMap.SelText = ""
       vPos = rtbOneMap.find("*#BEGINFIELD#", 1)
       rtbOneMap.SelText = ""
       vPos = rtbOneMap.find("#SEND-PARA#", 1)
       rtbOneMap.SelText = ""
       vPos = rtbOneMap.find("#RECEIVE-PARA#", 1)
       rtbOneMap.SelText = ""
     Else
       vPos = rtbOneMap.find("       ANALYZE-PFK.", 1)
       rtbOneMap.SelText = "       ANALYZE-PFK-" & wMapName & "."
       vPos = rtbOneMap.find("       EX-ANALYZE-PFK.", 1)
       rtbOneMap.SelText = "       EX-ANALYZE-PFK-" & wMapName & "."
       If FTMrecordset.AbsolutePosition <> FTMrecordset.RecordCount Then
         RTCopy.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdc\standard\routines\DC_PFK.cbl"
         vPos = rtbOneMap.find("       ANALYZE-PFK-" & wMapName & ".", 1)
         rtbOneMap.SelText = RTCopy.text & vbCrLf & "       ANALYZE-PFK-" & wMapName & "."
       End If
       rtbOneMap.text = Replace(rtbOneMap.text, " TO WMFS-PFK" & vbCrLf, " TO WMFS-PFK-" & wMapName & vbCrLf)
       vPos = rtbOneMap.find("#SEND-AREAS#" & vbCrLf, 1)
       rtbOneMap.SelText = ""
       vPos = rtbOneMap.find("#RECEIVE-AREAS#" & vbCrLf, 1)
       rtbOneMap.SelText = ""
     End If
      'IRIS attenzione rtbOneMap.SaveFile rtbroutine.FileName, 1
      'RTBOutMap.SaveFile RTBOutMap.filename, 1
     '*************
     FTMrecordset.MoveNext
    Wend
    If FTMrecordset.RecordCount > 1 Then
      vPos = rtbOneMap.find("       01 WMFS-PFK              PIC X(#LENPFK#)." & vbCrLf, 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("#ATTR-AREAS#" & vbCrLf, 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("#SEND-AREAS#" & vbCrLf, 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("        01 WS-SEND-AREA." & vbCrLf, 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("#RECEIVE-AREAS#" & vbCrLf, 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("        01 WS-RECEIVE-AREA." & vbCrLf, 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("#MULTIOPE#" & vbCrLf, 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("#MULTIOPER#" & vbCrLf, 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("#SEND-PARA#", 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("#RECEIVE-PARA#", 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("#COPYMOVE#.", 1)
      rtbOneMap.SelText = ""
      vPos = rtbOneMap.find("#OPE#", 1)
      rtbOneMap.SelText = ""
    End If
    
    FTMrecordset.Close
    CreaRoutineUnified_DLI_CICS = True
    Exit Function
errFun:
    m_fun.WriteLog "ImsDC : CreaRoutineUnified_DLI_CICS - Function - Errore : " & err, "DR - GenRout"
End Function

Function generate_Areas_Attr(fmtName As String, NumFmt As Integer) 'IRIS-DB
  Dim rsFmt As Recordset
  Dim rscampi As Recordset
  Dim rsfield As Recordset
  Dim Testo As String
  Dim AttrValue As Long
  Dim MSGName As String
  On Error GoTo errFun 'IRIS-DB
  Testo = ""
  Set rsFmt = m_fun.Open_Recordset("SELECT * FROM PsDLI_MFSMsg where FMTName = '" & fmtName & "' and Devtype = 'OUTPUT'")
  While Not rsFmt.EOF
    MSGName = rsFmt!MSGName
    Set rscampi = m_fun.Open_Recordset("SELECT * FROM PsDLI_MFSmsgCmp where FMTName = '" & fmtName & "' and MSGName = '" & MSGName & "' and Attributo like '%YES%' order by Ordinale")
    While Not rscampi.EOF
      If Not (Left(rscampi!nome, 1) = "#") Then
        Set rsfield = m_fun.Open_Recordset("SELECT Attributo FROM psdli_mfscmp where FMTName = '" & fmtName & "' and Nome = '" & rscampi!nome & "'")
        If Not rsfield.EOF Then
          AttrValue = 192
          If InStr(rsfield!Attributo, "MOD") > 0 Then AttrValue = AttrValue + 1
          If InStr(rsfield!Attributo, "DET") > 0 Then AttrValue = AttrValue + 2
          If InStr(rsfield!Attributo, "DRK") > 0 Then AttrValue = AttrValue + 4
          If InStr(rsfield!Attributo, "BRT") > 0 Then AttrValue = AttrValue + 8
          If InStr(rsfield!Attributo, "NUM") > 0 Then AttrValue = AttrValue + 16
          If InStr(rsfield!Attributo, "PROT") > 0 Then AttrValue = AttrValue + 32
          If Testo = "" Then
            If NumFmt = 1 Then
              Testo = Testo & Space(7) & "01 WS-ATTR-AREA." & vbCrLf
            Else
              Testo = Testo & Space(7) & "01 WS-ATTR-AREA-" & fmtName & "." & vbCrLf
            End If
          End If
          If Not InStr(Testo, "INIT-" & Trim(rscampi!nome) & IIf(rscampi!ordinaleDo > 0, "1", "") & "-ATTR") > 0 Then
            Testo = Testo & Space(10) & "03 INIT-" & Trim(rscampi!nome) & IIf(rscampi!ordinaleDo > 0, "1", "") & "-ATTR" & Space(IIf(Len(Trim(rscampi!nome)) < 9, 9, Len(Trim(rscampi!nome)) + 1) - Len(Trim(rscampi!nome))) & "PIC 9(4) COMP VALUE " & AttrValue & "." & vbCrLf
          End If
        End If
        rsfield.Close
      End If
      rscampi.MoveNext
    Wend
    rscampi.Close
    rsFmt.MoveNext
  Wend
  rsFmt.Close
  
  generate_Areas_Attr = Testo
  Exit Function
errFun:
  m_fun.WriteLog "ImsDC : generate_Areas_Attr - Function - Errore : " & err, "DR - GenRout"
End Function

Function generate_Areas_CopyMap(pwMapName As String, retAreaIms As String, retCpyIms As String, msginput As String)
Dim env As Collection, rscampi As Recordset
  
Dim MapName As String
Dim fmtAreas As String
Dim m_fun As New MaFndC_Funzioni
  On Error GoTo errFun 'IRIS-DB
   
  Set env = New Collection  'resetto;
  env.Add pwMapName
  
  If ImsDCIstr.istr = "SEND" Then
      retAreaIms = NameStructO
  Else
      retAreaIms = NameStructI
  End If
  retCpyIms = pwMapName
  If ImsDCIstr.istr = "RECEIVE" Then
    Set rscampi = m_fun.Open_Recordset( _
    "SELECT * FROM PsDLI_MFSmsgCmp where FMTName = '" & pwMapName & _
    "' and MSGName ='" & msginput & "'")
    If rscampi.EOF Then
      generate_Areas_CopyMap = ""
      Exit Function
    End If
  End If
        
  'IRIS-DB fmtAreas = "01 " & retAreaIms & "-AREA." & vbCrLf
  'IRIS-DB fmtAreas = fmtAreas & "           COPY " & retAreaIms & "." & vbCrLf
  'IRIS-DB fmtAreas = fmtAreas & vbCrLf
  fmtAreas = retAreaIms  'IRIS-DB
  
  generate_Areas_CopyMap = fmtAreas
  Exit Function
errFun:
  m_fun.WriteLog "ImsDC : generate_Areas_CopyMap - Function - Errore : " & err, "DR - GenRout"
End Function

Public Function CalcolaLenPDpage(pFMT As String, ordinale As Integer, pMSGName As String) As Integer
  Dim rsMFLD As Recordset
  Dim totale As Integer, msgIO As String
  
  totale = 4
  If pMSGName = "INPUT" Then
    Set rsMFLD = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where " & _
                                      "FMTname = '" & pFMT & "' And DevType = 'INPUT '")
    If Not rsMFLD.EOF Then
      msgIO = rsMFLD!MSGName
    End If
    rsMFLD.Close
  Else
    msgIO = pMSGName
  End If
  
  Set rsMFLD = m_fun.Open_Recordset("Select * from PsDLI_MFSmsgCmp where " & _
                                    "FMTName = '" & pFMT & "' And lPage = " & ordinale & _
                                    " and MSGName = '" & msgIO & "'")
  Do Until rsMFLD.EOF
    If Not rsMFLD!nome = "SYSMSG" And (rsMFLD!Funzione = "" Or IsNull(rsMFLD!Funzione)) Then
      totale = totale + rsMFLD!Lunghezza * rsMFLD!Occurs
      If Not rsMFLD!Attributo = "" Then
        totale = totale + 2 * rsMFLD!Occurs
      End If
    End If
    rsMFLD.MoveNext
  Loop
  rsMFLD.Close
  CalcolaLenPDpage = totale
End Function

Private Function assignTranId(ByRef plasttranid As String) As String
  Dim found As Boolean, rs As Recordset
  
  found = False
  If plasttranid = "" Then
    plasttranid = "0000"
  End If
  Do Until found
    plasttranid = Format(CInt(plasttranid) + 1, "0000")
    Set rs = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where TranId ='" & plasttranid & "'")
    If rs.EOF Then
      assignTranId = plasttranid
      found = True
    End If
    rs.Close
  Loop
End Function

Public Function AggRoutDLI_CICS(pIdOggetto As Long, rst As Recordset, RTRoutine As RichTextBox, RTOneMapRoutine As RichTextBox, wMapName As String, pMSGInput As String) As Boolean
  Dim wIstr As String
  Dim vPos As Double
  Dim wfine As Double
  Dim Testo As String
  Dim fmtAreas As String
  Dim TextField As String
  
  Dim ImsMap As String
  Dim ImsCpyMap As String
  Dim wCampospaces As Integer
  Dim rsfield As Recordset
  Dim nameCicsO As String
  Dim nameCicsI As String, nameCics As String, wMapNameMSGout As String, wMapNameMSGin As String
  Dim CondField As String, CondValue As String, basePage As String, lenPage As String, posCond As String
  Dim fmtName As String, tranid As String, extraIndent As Integer, strpunto As String
  
  Dim rsMFLD As Recordset, rsMSGout As Recordset, rsMSGcmp As Recordset
  Dim i As Integer, LenCond As Integer, OffsetCond As Integer, OrdCond As Integer, namecopyCics As String
  Dim colInfo As Collection, TbOggetti As Recordset
   
   If bolsungard Then
    extraIndent = 3
   End If
    
   Set colInfo = New Collection
   Set TbOggetti = Open_Recordset("select * from BS_oggetti where idoggetto = " & pIdOggetto)
   If Not TbOggetti.EOF Then
    nameCics = CStr(TbOggetti!nome)
    colInfo.Add pIdOggetto
    colInfo.Add nameCics
    colInfo.Add " "
   End If
  TbOggetti.Close
   nameCics = wMapName
   ' nome copy come nome esterno mappa (stabilito da enviroment)
   namecopyCics = nameCics
   wIstr = Trim(UCase(ImsDCIstr.istr))  'serve?
   
   If wIstr = "SEND" Then
    GbIntestazionePerform = "SEND-MAP-"
   ElseIf wIstr = "RECEIVE" Then
      GbIntestazionePerform = "RECEIVE-MAP-" '& wMapName & "-"
   Else
      GbIntestazionePerform = "PREPARE-MAP-"
   End If
   
   wfine = Len(RTRoutine.text)
   
   ' sostituiamo il testo nella routine della singola mappa
   If Len(Trim(wMapName)) Then
      Testo = vbCrLf & "      **  " & ImsDCIstr.istr & " istruction for map: " & wMapName
   End If
   
   'Icolla copy mappe Cics
   vPos = RTOneMapRoutine.find("#CICS-MAP#.", 1)
  
   'con routine singola mappa non attacco pi� tag
   RTOneMapRoutine.SelText = namecopyCics & "." & vbCrLf
   
   'Genera 01 + Copy Formati Ims
   vPos = RTOneMapRoutine.find("#FORMAT-AREAS#", 1) 'IRIS-DB
      
   fmtAreas = generate_Areas_CopyMap(wMapName, ImsMap, ImsCpyMap, pMSGInput)
  
   'RTOneMapRoutine.SelText = Space(17) & fmtAreas
   RTOneMapRoutine.SelText = fmtAreas
   
   If bolsungard And wIstr = "SEND" Then
   ' sostituzione #F-AREA#
      vPos = RTOneMapRoutine.find("#F-AREA#", 1)
      RTOneMapRoutine.SelStart = vPos
      RTOneMapRoutine.SelLength = 8
      RTOneMapRoutine.SelText = ImsMap
   End If
   
   
   'Sostituzione #CODE#
   
   vPos = RTOneMapRoutine.find("#CODE#", 1)
   vPos = RTOneMapRoutine.find(vbCrLf, vPos + 1)
   vPos = vPos + 2
   
   RTOneMapRoutine.SelStart = vPos
   RTOneMapRoutine.SelLength = 0
   RTOneMapRoutine.SelText = Testo & vbCrLf
      
   'Sostituzione #OPE#
   Set rsMFLD = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where FMTName = '" & wMapName & "' and DevType = 'INPUT'")
   If Not rsMFLD.EOF Then
      wMapNameMSGin = rsMFLD!MSGName
   End If
   rsMFLD.Close
   Set rsMFLD = m_fun.Open_Recordset("Select MSGName,b.Devtype as Type,TranId from PsDLI_mfsMSG as a ,PsDLI_MFS as b where a.FMTName = '" & wMapName & "' and a.DevType = 'OUTPUT' and  a.FMTName = b.FMTname")
   If Not rsMFLD.EOF Then
     wMapNameMSGout = rsMFLD!MSGName
   End If
    ' prima in quella generale
    If wIstr = "SEND" Then
      While Not rsMFLD.EOF
      ' devo ciclare su tutte le msg di output
        vPos = RTRoutine.find("#OPE#", 1)
        vPos = RTRoutine.find(vbCrLf, vPos + 1)
        vPos = vPos + 2
        '********* AC 01/03/07
        If ultimappar Then
          Testo = Space(11) & "EVALUATE IRIS-MODNAME" & vbCrLf 'IRIS-DB
          ultimappar = False
        Else
          Testo = ""
        End If
       '***************
        Testo = Testo & Space(12) & "  WHEN '" & rsMFLD!MSGName & "' " & vbCrLf
        'AC call alla routine della
        
        If InStr(rsMFLD!Type, "3270,2") > 0 Or rsMFLD!Type = "3270" Then
          ' routine diretta
          Testo = Testo & Space(15) & "  CALL '" & RoutineMap & "' USING DFHEIBLK AREA-LNK-PASS"
        Else ' per ora tutto il resto � stampa
          tranid = IIf(IsNull(rsMFLD!tranid), "0000", rsMFLD!tranid)
          If tranid = "0000" Or tranid = "" Then
              tranid = assignTranId(lasttranid)
              rsMFLD!tranid = tranid
              rsMFLD.Update
          End If
          Testo = Testo & Space(6) & "*          TRANID = " & tranid & "  PGM = " & wMapName & vbCrLf
          Testo = Testo & Space(15) & "  EXEC CICS START TRANSID('" & tranid & "') FROM(AREA-LNK-PASS)" & vbCrLf
          Testo = Testo & Space(18) & "  LENGTH(2057) TERMID(LNKDC-CHNGDEST) END-EXEC"
        End If
        Testo = Testo & vbCrLf
        '*************
        If Not evaluate Then
          Testo = Testo & Space(15) & "WHEN OTHER" & vbCrLf
          Testo = Testo & Space(18) & "MOVE 'SI' TO DC-PCB-STATUS-CODE" & vbCrLf
          Testo = Testo & Space(12) & "END-EVALUATE" & vbCrLf
          evaluate = True
        End If
        '***************
        RTRoutine.SelStart = vPos
        RTRoutine.SelLength = 0
        RTRoutine.SelText = Testo
        rsMFLD.MoveNext
      Wend
      rsMFLD.Close
      
    Else
      rsMFLD.Close
      vPos = RTRoutine.find("#OPE#", 1)
      vPos = RTRoutine.find(vbCrLf, vPos + 1)
      vPos = vPos + 2
      If ultimappar Then
        Testo = Space(11) & "EVALUATE LNKDC-MAPSETNAME" & vbCrLf
      Else
        Testo = ""
      End If
'      Set rsMFLD = m_fun.Open_Recordset("Select SOR from PsMFS_MSG_LPAGE where MSG = '" & wMapNameMSGin & "'")
'      If rsMFLD.EOF Then
'        rsMFLD.Close
'        Set rsMFLD = m_fun.Open_Recordset("Select FMTName from PsDLI_MFS where IdOggetto = " & pIdOggetto)
'        'FMT
'        If Not rsMFLD.EOF Then
'          Testo = Testo & Space(15) & "WHEN '" & rsMFLD!FMtName & "'" & vbCrLf
'        End If
'        rsMFLD.Close
        Testo = Testo & Space(15) & "WHEN '" & wMapName & "'" & vbCrLf
'      Else
'        While Not rsMFLD.EOF
'          Testo = Testo & Space(15) & "WHEN '" & rsMFLD!SOR & "'" & vbCrLf
'        rsMFLD.MoveNext
'        Wend
'        rsMFLD.Close
'      End If
      
      'Testo = Testo & Space(18) & "CALL " & wIstr & "-" & wMapName & " USING DFHEIBLK AREA-LNK-PASS" & vbCrLf
      ' routine diretta
      Testo = Testo & Space(18) & "CALL '" & RoutineMap & "' USING DFHEIBLK AREA-LNK-PASS" & vbCrLf
      If Not evaluate Then
        Testo = Testo & Space(15) & "WHEN OTHER" & vbCrLf
        Testo = Testo & Space(18) & "MOVE '  ' TO DC-PCB-STATUS-CODE" & vbCrLf
        Testo = Testo & Space(12) & "END-EVALUATE" & vbCrLf
        evaluate = True
      End If
      RTRoutine.SelStart = vPos
      RTRoutine.SelLength = 0
      RTRoutine.SelText = Testo
    End If
    
    
    
    'poi in quella della singola mappa
    vPos = RTOneMapRoutine.find("#OPE#", 1)
    vPos = RTOneMapRoutine.find(vbCrLf, vPos + 1)
    vPos = vPos + 2
    Testo = ""
    
    If wIstr = "SEND" Then
    ' evaluate esterno
      Testo = Testo & Space(12 + extraIndent) & "EVALUATE IRIS-MODNAME" & vbCrLf 'IRIS-DB
      Set rsMSGout = m_fun.Open_Recordset("Select MSGNAME from PsDli_mfsMSG where FMTName = '" & wMapName & "' and DevType = 'OUTPUT'")
      While Not rsMSGout.EOF
        
        Testo = Testo & Space(15 + extraIndent) & "WHEN '" & rsMSGout!MSGName & "'" & vbCrLf
        Set rsMFLD = m_fun.Open_Recordset("Select * from PsMFS_MSG_LPAGE where MSG = '" & rsMSGout!MSGName & "' order by Ordinale")
        If rsMFLD.EOF Then
          ' devo verificare se manca lpage ma c'� cmq dpage
          rsMFLD.Close
          Set rsMFLD = m_fun.Open_Recordset("Select DPageName from PsDLI_MFSCmp where FMTName = '" & wMapName & "'")
          If Not rsMFLD.EOF Then
            If Not IsNull(rsMFLD!DPageName) Then
              If Not rsMFLD!DPageName = "" Then
                  Testo = Testo & Space(18 + extraIndent) & "MOVE '" & Left(rsMFLD!DPageName, 7) & "' TO WS-MAPNAME" & vbCrLf
''''                  Testo = Testo & Space(18 + extraIndent) & "MOVE '" & rsMFLD!DPageName & "O' TO WK-FROMAREA" & vbCrLf
''''                  Testo = Testo & Space(18 + extraIndent) & "MOVE '" & rsMFLD!DPageName & "I' TO WK-INTOAREA" & vbCrLf
              Else
                  Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "' TO WS-MAPNAME" & vbCrLf
''''                  Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "O' TO WK-FROMAREA" & vbCrLf
''''                  Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "I' TO WK-INTOAREA" & vbCrLf
              End If
            Else
              Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "' TO WS-MAPNAME" & vbCrLf
''''              Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "O' TO WK-FROMAREA" & vbCrLf
''''              Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "I' TO WK-INTOAREA" & vbCrLf
            End If
          Else
            Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "' TO WS-MAPNAME" & vbCrLf
''''            Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "O' TO WK-FROMAREA" & vbCrLf
''''            Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "I' TO WK-INTOAREA" & vbCrLf
          End If
''''          Testo = Testo & Space(18 + extraIndent) & "MOVE LNKDC-MFSMAP-AREA TO " _
''''                 & Replace(rsMSGout!MSGName, "@", "") & "-OUT" & vbCrLf
''''          Testo = Testo & vbCrLf
          rsMFLD.Close
        Else
          
          CondField = IIf(IsNull(rsMFLD!COND_Field), "", rsMFLD!COND_Field)
         If Not CondField = "" Then ' una sola lpage
           
           If InStr(1, CondField, "(") Then
                    posCond = Replace(Right(CondField, Len(CondField) + 1 - InStr(1, CondField, "(")), ")", ":1)")
                    CondField = Left(CondField, InStr(1, CondField, "(") - 1)
            End If
           Set rsMSGcmp = m_fun.Open_Recordset("Select Lunghezza,Ordinale from PsDLI_MFSmsgcmp where MSGName = '" & rsMSGout!MSGName & "' and Nome='" & CondField & "' order by ordinale")
           If Not rsMSGcmp.EOF Then
            LenCond = rsMSGcmp!Lunghezza
            OrdCond = rsMSGcmp!ordinale
           End If
           rsMSGcmp.Close
           OffsetCond = 0
           Set rsMSGcmp = m_fun.Open_Recordset("Select Lunghezza from PsDLI_MFSmsgcmp where MSGName = '" & rsMSGout!MSGName & "' and (Funzione = '' or Funzione is Null) and ordinale < " & OrdCond)
           While Not rsMSGcmp.EOF
            OffsetCond = OffsetCond + rsMSGcmp!Lunghezza
            rsMSGcmp.MoveNext
           Wend
           rsMSGcmp.Close
           ' FILLER iniziale
           OffsetCond = OffsetCond + 4
           
'            Testo = Testo & Space(12) & "MOVE LNKDC-MFSMAP-AREA TO " _
'                  & NameStructO & "-AREA" & vbCrLf
            Testo = Testo & vbCrLf
            Testo = Testo & Space(6) & "*--------------------------------------------- " & vbCrLf
            Testo = Testo & Space(6) & "*--------------PAGE SELECTION----------------- " & vbCrLf
            Testo = Testo & Space(6) & "*--------------------------------------------- " & vbCrLf
            
'            Testo = Testo & Space(18) & "EVALUATE O-" & CondField & "1 OF " _
'                                      & Replace(rsMSGout!MSGName, "@", "") & "-OUT" & posCond & vbCrLf
                                      '& NameStructO & "-AREA" & posCond & vbCrLf
                                      
            Testo = Testo & Space(18 + extraIndent) & "EVALUATE LNKDC-MFSMAP-AREA(" & OffsetCond + 1 & ":" & LenCond & ")" & vbCrLf
            
            basePage = 1
            While Not rsMFLD.EOF
              Testo = Testo & Space(21 + extraIndent) & "WHEN " & rsMFLD!COND_Value & vbCrLf
              
              Testo = Testo & Space(24 + extraIndent) & "MOVE '" & rsMFLD!SOR & "' TO WS-MAPNAME" & vbCrLf
              Testo = Testo & Space(24 + extraIndent) & "MOVE '" & rsMFLD!SOR & "O' TO WS-FROMAREA" & vbCrLf
              Testo = Testo & Space(24 + extraIndent) & "MOVE '" & rsMFLD!SOR & "I' TO WS-INTOAREA" & vbCrLf
              lenPage = CalcolaLenPDpage(wMapName, rsMFLD!ordinale, rsMSGout!MSGName)
              'AC 26/10/07
              'Testo = Testo & Space(24 + extraindent) & "MOVE " & basePage & Space(11 - Len(basePage)) & "TO BASE-PAGE" & vbCrLf
             'Testo = Testo & Space(24 + extraindent) & "MOVE " & lenPage & Space(11 - Len(lenPage)) & "TO LEN-PAGE" & vbCrLf
            rsMFLD.MoveNext
            basePage = CInt(basePage) + CInt(lenPage)
            Wend
            rsMFLD.Close
            Testo = Testo & Space(21 + extraIndent) & "WHEN OTHER" & vbCrLf
            Testo = Testo & Space(24 + extraIndent) & "MOVE 'SP' TO DC-PCB-STATUS-CODE" & vbCrLf
            Testo = Testo & Space(18 + extraIndent) & "END-EVALUATE " & vbCrLf & vbCrLf
'            Testo = Testo & Space(18 + extraindent) & "MOVE LNKDC-MFSMAP-AREA" & vbCrLf & Space(21) & "TO " _
'                   & Replace(rsMSGout!MSGName, "@", "") & "-OUT(BASE-PAGE:LEN-PAGE)" & vbCrLf
            Testo = Testo & Space(18 + extraIndent) & "MOVE LNKDC-MFSMAP-AREA" & vbCrLf & Space(21) & "TO " _
                   & wMapName & "-OUT" & vbCrLf
            Testo = Testo & vbCrLf
          'End If
        Else
          If Not IIf(IsNull(rsMFLD!SOR), "", rsMFLD!SOR) = "" Then
            Testo = Testo & Space(18 + extraIndent) & "MOVE '" & rsMFLD!SOR & "' TO WS-MAPNAME" & vbCrLf
            Testo = Testo & Space(18 + extraIndent) & "MOVE '" & rsMFLD!SOR & "O' TO WS-FROMAREA" & vbCrLf
            Testo = Testo & Space(18 + extraIndent) & "MOVE '" & rsMFLD!SOR & "I' TO WS-INTOAREA" & vbCrLf
          Else
            Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "' TO WS-MAPNAME" & vbCrLf
            Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "O' TO WS-FROMAREA" & vbCrLf
            Testo = Testo & Space(18 + extraIndent) & "MOVE '" & wMapName & "I' TO WS-INTOAREA" & vbCrLf
          End If
          Testo = Testo & Space(18 + extraIndent) & "MOVE LNKDC-MFSMAP-AREA TO " _
                 & Replace(rsMSGout!MSGName, "@", "") & "-OUT" & vbCrLf
                '& NameStructO & "-AREA" & vbCrLf
          Testo = Testo & vbCrLf
        End If
      
  
       End If
       rsMSGout.MoveNext
      Wend
      rsMSGout.Close
      Testo = Testo & Space(15 + extraIndent) & "WHEN OTHER" & vbCrLf
      Testo = Testo & Space(18 + extraIndent) & "MOVE 'SP' TO DC-PCB-STATUS-CODE" & vbCrLf
      Testo = Testo & Space(12 + extraIndent) & "END-EVALUATE " & vbCrLf & vbCrLf
    End If
     
    Testo = Testo & Space(12 + extraIndent) & "PERFORM " & GbIntestazionePerform & wMapName & vbCrLf
    Testo = Testo & Space(15 + extraIndent) & "THRU " & GbIntestazionePerform & wMapName & "-EX" & vbCrLf
    strpunto = "."
    
'    If bolsungard Then
'      strpunto = ""
'    End If
    Testo = Testo & Space(12 + extraIndent) & "GO TO MAIN-END" & strpunto & vbCrLf
    'If wIstr = "RECEIVE" Then
    '   Testo = Testo & Space(12) & vbCrLf & "MOVE SPACES TO DC-PCB-STATUS-CODE"
    'End If
    Testo = Testo & vbCrLf
    RTOneMapRoutine.SelStart = vPos
    RTOneMapRoutine.SelLength = 0
    RTOneMapRoutine.SelText = Testo
    
    Testo = vbCrLf & "      *----------------------------------------------------------------*" & vbCrLf
    Testo = Testo & "      *  Operation  : " & wIstr & Space(49 - Len(wIstr)) & "*" & vbCrLf
    Testo = Testo & "      *  On Map     : " & wMapName & Space(49 - Len(wMapName)) & "*" & vbCrLf
    Testo = Testo & "      *----------------------------------------------------------------*" & vbCrLf
    
    If wIstr = "RECEIVE" Then
       'wFine = Len(RTRoutine.text)
       wfine = RTOneMapRoutine.find("#OPEREC#", 1)
    Else
       wfine = RTOneMapRoutine.find("*#BEGINFIELD#", 1)
    End If
         

     Select Case Trim(UCase(wIstr))
           Case "SEND", "RECEIVE"
              Testo = Testo & AggRtImsDC_CICS(pIdOggetto, rst, wFmtName, ImsCpyMap, wIstr, 1, 1)
     End Select
     
   Testo = Testo & Space(7) & GbIntestazionePerform & wMapName & "-EX." & vbCrLf
   Testo = Testo & Space(11) & "EXIT." & vbCrLf & vbCrLf
   
   RTOneMapRoutine.SelLength = 0
   RTOneMapRoutine.SelStart = wfine
   RTOneMapRoutine.SelText = Testo
   'AC*****************  Move low value
   
   If wIstr = "SEND" Then
    nameCicsO = wMapName & "O"
    nameCicsI = wMapName & "I"
    'SQ - PROBLEMA!!!!!!!!!!!!!!!!!!!!!!!!!!!
    'La PERFORM viene fatta comunque!!!!!!!!!
    If Not ColCampiDflattr.Count = 0 Then
      TextField = "DFLT-ATTR-" & wMapName & "." & vbCrLf
      TextField = TextField & Space(11) & "MOVE LOW-VALUE TO "
      wCampospaces = 0
      For i = 1 To ColCampiDflattr.Count
        TextField = TextField & Space(wCampospaces) & ColCampiDflattr.item(i) & "A OF " & nameCicsI & vbCrLf
        wCampospaces = 29
      Next i
      TextField = Left(TextField, Len(TextField) - 2) & "." & vbCrLf
      TextField = TextField & Space(7) & "EX-DFLT-ATTR-" & wMapName & "." & vbCrLf & Space(11) & "EXIT." & vbCrLf
    End If
  End If
  If wIstr = "SEND" Then
    vPos = RTOneMapRoutine.find("#FIELD#.", 1)
    RTOneMapRoutine.SelStart = vPos
    RTOneMapRoutine.SelLength = 8
    RTOneMapRoutine.SelText = TextField
    '*************
    vPos = RTOneMapRoutine.find("#COPYMOVE#.", 1)
    RTOneMapRoutine.SelStart = vPos
    RTOneMapRoutine.SelLength = 11
    RTOneMapRoutine.SelText = "COPY " & NameMoveO & "."
  Else
    Dim arrTasti(24) As String, j As Integer, maxlenPFK As Integer
    RTOneMapRoutine.text = RTOneMapRoutine.text & vbCrLf
    vPos = Len(RTOneMapRoutine.text)
    RTOneMapRoutine.SelStart = vPos
    RTOneMapRoutine.SelText = Space(10) & "COPY " & NameMoveI & "." & vbCrLf
    'gestione PFK
  'IRIS-DB per ora tolgo il controllo, al limite si dovrebbe fare per tutti i tipi di 3270, ma in generale se ci sono righe nella tabella, sono per forza tasti funzione
  ' IRIS-DB Set rst = m_fun.Open_Recordset("select * from PsDLI_MFSPfK where FMTName = '" & wMapName & "' and Dev = '(3270,2)' ")
    Set rst = m_fun.Open_Recordset("select * from PsDLI_MFSPfK where FMTName = '" & wFmtName & "'")
    While Not rst.EOF
      If IsNumeric(rst!Tasto) Then
        arrTasti(CInt(rst!Tasto)) = rst!Valore
      End If
      If Len(rst!Valore) - 2 > maxlenPFK Then
        maxlenPFK = Len(rst!Valore) - 2
      End If
      rst.MoveNext
    Wend
    rst.Close
    For j = 1 To 24
      vPos = RTOneMapRoutine.find("#value" & j & "#", 1)
      If vPos = -1 Then Exit For
      RTOneMapRoutine.SelStart = vPos
      RTOneMapRoutine.SelLength = 7 + Len(CStr(j))
      If Not arrTasti(j) = "" Then
        RTOneMapRoutine.SelText = arrTasti(j)
      Else
        RTOneMapRoutine.SelText = "LOW-VALUE"
      End If
    Next
    If testoPFKFLD = "" Then ' pagina singola
      Set rst = m_fun.Open_Recordset("select PFKField from PsDLI_MFS where FMTName = '" & wFmtName & "' and DevType = '(3270,2)' ")
      If Not rst.EOF Then
        Set rsfield = m_fun.Open_Recordset("select Lunghezza,Value from PsDLI_MFSmsgCmp where FMTName = '" & wFmtName & "'  and Nome ='" & rst!PFKField & "'")
        If Not rsfield.EOF Then
          vPos = RTOneMapRoutine.find("#LENPFK#", 1)
          RTOneMapRoutine.SelStart = vPos
          RTOneMapRoutine.SelLength = 8  'Len(CStr(rsField!Lunghezza))
          RTOneMapRoutine.SelText = IIf(rsfield!Lunghezza <= 0, maxlenPFK, rsfield!Lunghezza)
          
          '*************** DFVALUE
          vPos = RTOneMapRoutine.find("#dfvalue#", 1)
          RTOneMapRoutine.SelStart = vPos
          RTOneMapRoutine.SelLength = 9
          
          If Not IsNull(rsfield!Value) And Not rsfield!Value = "" Then
            'IRIS-DB RTOneMapRoutine.SelText = "MOVE " & rsField!Value & " TO WMFS-PFK" 'IRIS-DB
            RTOneMapRoutine.SelText = rsfield!Value 'IRIS-DB
          Else
            RTOneMapRoutine.SelText = "LOW-VALUE" 'IRIS-DB
          End If
          
        Else
          vPos = RTOneMapRoutine.find("#LENPFK#", 1)
          RTOneMapRoutine.SelStart = vPos
          RTOneMapRoutine.SelLength = 8  'Len(CStr(rsField!Lunghezza))
          RTOneMapRoutine.SelText = "2"
          
          
          vPos = RTOneMapRoutine.find("#dfvalue#", 1)
          RTOneMapRoutine.SelStart = vPos
          RTOneMapRoutine.SelLength = 9
          RTOneMapRoutine.SelText = "LOW-VALUE" 'IRIS-DB
        End If
        rsfield.Close
      
    Else
      vPos = RTOneMapRoutine.find("#LENPFK#", 1)
      RTOneMapRoutine.SelStart = vPos
      RTOneMapRoutine.SelLength = 8  'Len(CStr(rsField!Lunghezza))
      RTOneMapRoutine.SelText = "2"
      vPos = RTOneMapRoutine.find("#dfvalue#", 1)
      RTOneMapRoutine.SelStart = vPos
      RTOneMapRoutine.SelLength = 9
      RTOneMapRoutine.SelText = "LOW-VALUE" 'IRIS-DB
    End If
    rst.Close
  Else  ' multipagina
      vPos = RTOneMapRoutine.find("#LENPFK#", 1)
      RTOneMapRoutine.SelStart = vPos
      RTOneMapRoutine.SelLength = 8  'Len(CStr(rsField!Lunghezza))
      RTOneMapRoutine.SelText = IIf(lenPFKField <= 0, maxlenPFK, lenPFKField)
      
      
      vPos = RTOneMapRoutine.find("#dfvalue#", 1)
      RTOneMapRoutine.SelStart = vPos
      RTOneMapRoutine.SelLength = 9
      RTOneMapRoutine.SelText = testoPFKFLD
    End If
  End If
  
End Function
Public Function AggRoutDLI_CICS_Unified(pIdOggetto As Long, rst As Recordset, RTCopy As RichTextBox, RTOneMapRoutine As RichTextBox, wFmtName As String, pMSGInput As String, NumFmt As Integer, CurrFmt As Integer) As Boolean
  Dim wIstr As String
  Dim vPos As Double
  Dim wfine As Double
  Dim Testo As String
  Dim fmtAreas As String
  Dim TextField As String
  
  Dim ImsMap As String
  Dim ImsCpyMap As String
  Dim wCampospaces As Integer
  Dim rsfield As Recordset
  Dim nameCicsO As String
  Dim nameCicsI As String, nameCics As String, wMapNameMSGout As String, wMapNameMSGin As String
  Dim CondField As String, CondValue As String, basePage As String, lenPage As String, posCond As String
  Dim fmtName As String, tranid As String, extraIndent As Integer, strpunto As String
  Dim CondOper As String
  Dim rsMFLD As Recordset, rsMSGout As Recordset, rsMSGcmp As Recordset
  Dim i As Integer, LenCond As Integer, OffsetCond As Integer, OrdCond As Integer, namecopyCics As String
  
  Dim wMapName As String
'IRIS-DB
   Dim colInfo As Collection
   Dim wMapsetName As String
   On Error GoTo errFun 'IRIS-DB
   
   Set colInfo = New Collection
   Set TbOggetti = Open_Recordset("select * from BS_oggetti where idoggetto = " & pIdOggetto)
   If Not TbOggetti.EOF Then
    nameCics = CStr(TbOggetti!nome)
    colInfo.Add pIdOggetto
    colInfo.Add nameCics
    colInfo.Add wFmtName
   End If
  TbOggetti.Close
  wMapsetName = getEnvironmentParam(RetrieveParameterValues("IMSDC-BMSMAPSET")(0), "mappe", colInfo)
  If wMapsetName = "" Then wMapsetName = wFmtName
  wMapName = Left(getEnvironmentParam(RetrieveParameterValues("IMSDC-BMSMAP")(0), "mappe", colInfo), 7)
  If m_ImsDll_DC.ImNomeDB = "LandG.mty" Then 'IRIS-DB: pezza per evitare di riconsegnare tutti i programmi a L&G; da rimuovere in seguito
    Set TbOggetti = Open_Recordset("select * from PsDLI_MFS where idoggetto = " & pIdOggetto)
    If TbOggetti.RecordCount = 1 Then
      wMapName = wMapsetName & "R"
    End If
    TbOggetti.Close
  End If
'IRIS-DB end
  
  If CurrFmt = 1 Then
    vPos = RTOneMapRoutine.find("#CICS-MAPSET#", 1)
    RTOneMapRoutine.SelText = wMapsetName
    vPos = RTOneMapRoutine.find("#CICS-MAPSET#", 1)
    RTOneMapRoutine.SelText = wMapsetName
  End If
  
  'IRIS-DB Genera campi valore iniziale attributo per gestione OR logica
  fmtAreas = generate_Areas_Attr(wFmtName, NumFmt)
  If NumFmt = 1 Then 'IRIS-DB gestione multimappa
    vPos = RTOneMapRoutine.find("#ATTR-AREAS#", 1) 'IRIS-DB
    RTOneMapRoutine.SelText = fmtAreas
  Else
    vPos = RTOneMapRoutine.find("#ATTR-AREAS#" & vbCrLf, 1)  'IRIS-DB
    RTOneMapRoutine.SelText = fmtAreas & "#ATTR-AREAS#" & vbCrLf
  End If
  'RTCopy.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Output-Prj\Cics\Cpy\" & fmtAreas
  'RTOneMapRoutine.SelText = RTCopy.text
  
  'Genera 01 + Copy Formati Ims
  vPos = RTOneMapRoutine.find("#SEND-AREAS#", 1) 'IRIS-DB
  ImsDCIstr.istr = "SEND"
  fmtAreas = generate_Areas_CopyMap(wFmtName, ImsMap, ImsCpyMap, pMSGInput)
  'RTOneMapRoutine.SelText = fmtAreas
  RTCopy.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Output-Prj\Cics\Cpy\" & fmtAreas
  If NumFmt = 1 Then 'IRIS-DB gestione multimappa
    RTOneMapRoutine.SelText = RTCopy.text
  Else
    vPos = RTOneMapRoutine.find(" 01 WS-SEND-AREA.", 1) 'IRIS-DB
    RTOneMapRoutine.SelText = " 01 WS-SEND-AREA-" & wMapName & "." & vbCrLf & RTCopy.text & "        01 WS-SEND-AREA." & vbCrLf & "#SEND-AREAS#"
    'RTOneMapRoutine.SelText = RTCopy.text & "        01 WS-SEND-AREA." & vbCrLf & "#SEND-AREAS#" & vbCrLf
  End If
  
  vPos = RTOneMapRoutine.find("#RECEIVE-AREAS#", 1) 'IRIS-DB
  ImsDCIstr.istr = "RECEIVE"
  fmtAreas = generate_Areas_CopyMap(wFmtName, ImsMap, ImsCpyMap, pMSGInput)
  'RTOneMapRoutine.SelText = fmtAreas
  RTCopy.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Output-Prj\Cics\Cpy\" & fmtAreas
  If NumFmt = 1 Then 'IRIS-DB gestione multimappa
    RTOneMapRoutine.SelText = RTCopy.text
  Else
    vPos = RTOneMapRoutine.find(" 01 WS-RECEIVE-AREA.", 1) 'IRIS-DB
    RTOneMapRoutine.SelText = " 01 WS-RECEIVE-AREA-" & wMapName & "." & vbCrLf & RTCopy.text & "        01 WS-RECEIVE-AREA." & vbCrLf & "#RECEIVE-AREAS#"
  End If
  
  'Sostituzione #OPE#
  Set rsMFLD = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where FMTName = '" & wFmtName & "' and DevType = 'INPUT'")
  If Not rsMFLD.EOF Then
     wMapNameMSGin = rsMFLD!MSGName
  End If
  rsMFLD.Close
  Set rsMFLD = m_fun.Open_Recordset("Select MSGName,b.Devtype as Type,TranId from PsDLI_mfsMSG as a ,PsDLI_MFS as b where a.FMTName = '" & wFmtName & "' and a.DevType = 'OUTPUT' and  a.FMTName = b.FMTname")
  If Not rsMFLD.EOF Then
    wMapNameMSGout = rsMFLD!MSGName
  End If
   
  If CurrFmt = 1 Then
    vPos = RTOneMapRoutine.find("#OPE#", 1)
    vPos = RTOneMapRoutine.find(vbCrLf, vPos + 1)
    vPos = vPos + 2
    Testo = ""
    Testo = Testo & Space(11 + extraIndent) & "IF IRIS-FUNC-ISRT" & vbCrLf 'IRIS-DB
    Testo = Testo & Space(14 + extraIndent) & "EVALUATE IRIS-MODNAME" & vbCrLf 'IRIS-DB
  Else
    vPos = RTOneMapRoutine.find("#MULTIOPE#", 1)
  End If
  Set rsMSGout = m_fun.Open_Recordset("Select MSGNAME from PsDli_mfsMSG where FMTName = '" & wFmtName & "' and DevType = 'OUTPUT'")
  While Not rsMSGout.EOF
    Testo = Testo & Space(17 + extraIndent) & "WHEN '" & rsMSGout!MSGName & "'" & vbCrLf
    'IRIS-DB: per la multi-FMT per ora gestisco solo senza LPAGE!!!!! (tanto L&G non la usa)
    Set rsMFLD = m_fun.Open_Recordset("Select * from PsMFS_MSG_LPAGE where MSG = '" & rsMSGout!MSGName & "' order by Ordinale")
    If rsMFLD.EOF Then
        ' devo verificare se manca lpage ma c'� cmq dpage
      rsMFLD.Close
      Set rsMFLD = m_fun.Open_Recordset("Select DPageName from PsDLI_MFSCmp where FMTName = '" & wFmtName & "'")
      If Not rsMFLD.EOF Then
        If Not IsNull(rsMFLD!DPageName) Then
          If Not rsMFLD!DPageName = "" Then
            Testo = Testo & Space(20 + extraIndent) & "MOVE '" & Left(rsMFLD!DPageName, 7) & "' TO WS-MAPNAME" & vbCrLf
          Else
            Testo = Testo & Space(20 + extraIndent) & "MOVE '" & wMapName & "' TO WS-MAPNAME" & vbCrLf
          End If
        Else
          Testo = Testo & Space(20 + extraIndent) & "MOVE '" & wMapName & "' TO WS-MAPNAME" & vbCrLf
        End If
      Else
        Testo = Testo & Space(20 + extraIndent) & "MOVE '" & wMapName & "' TO WS-MAPNAME" & vbCrLf
      End If
      rsMFLD.Close
      If NumFmt > 1 Then
        Testo = Testo & Space(20 + extraIndent) & "PERFORM SEND-MAP-" & wMapName & vbCrLf
        Testo = Testo & Space(23 + extraIndent) & "THRU SEND-MAP-" & wMapName & "-EX" & vbCrLf
        If CurrFmt = 1 Then
          Testo = Testo & "#MULTIOPE#" & vbCrLf
        End If
      End If
    Else
      CondField = IIf(IsNull(rsMFLD!COND_Field), "", rsMFLD!COND_Field)
      CondValue = IIf(IsNull(rsMFLD!COND_Value), "", rsMFLD!COND_Value)
      CondOper = IIf(IsNull(rsMFLD!COND_OP), "", rsMFLD!COND_OP)
      If Not CondField = "" Then ' una sola lpage
        If Not IsNumeric(CondField) Then
          If InStr(1, CondField, "(") Then
            posCond = Replace(Right(CondField, Len(CondField) + 1 - InStr(1, CondField, "(")), ")", ":1)")
            CondField = Left(CondField, InStr(1, CondField, "(") - 1)
          End If
          Set rsMSGcmp = m_fun.Open_Recordset("Select Lunghezza,Ordinale from PsDLI_MFSmsgcmp where MSGName = '" & rsMSGout!MSGName & "' and Nome='" & CondField & "' order by ordinale")
          If Not rsMSGcmp.EOF Then
            LenCond = rsMSGcmp!Lunghezza
            OrdCond = rsMSGcmp!ordinale
          End If
          rsMSGcmp.Close
          OffsetCond = 0
          Set rsMSGcmp = m_fun.Open_Recordset("Select Lunghezza from PsDLI_MFSmsgcmp where MSGName = '" & rsMSGout!MSGName & "' and (Funzione = '' or Funzione is Null) and ordinale < " & OrdCond)
          While Not rsMSGcmp.EOF
            OffsetCond = OffsetCond + rsMSGcmp!Lunghezza
            rsMSGcmp.MoveNext
          Wend
          rsMSGcmp.Close
        Else
          OffsetCond = CInt(CondField)
          LenCond = Len(Replace(CondValue, "'", ""))
        End If
         ' FILLER iniziale
        OffsetCond = OffsetCond + 4
        Testo = Testo & vbCrLf
        Testo = Testo & Space(6) & "*--------------------------------------------- " & vbCrLf
        Testo = Testo & Space(6) & "*--------------PAGE SELECTION----------------- " & vbCrLf
        Testo = Testo & Space(6) & "*--------------------------------------------- " & vbCrLf
        Testo = Testo & Space(20 + extraIndent) & "EVALUATE IRIS-MESSAGE-AREA(" & OffsetCond + 1 & ":" & LenCond & ")" & vbCrLf
          
        basePage = 1
        While Not rsMFLD.EOF
        'IRIS-DB : manca la gestione dell'operatore che non � detto che sia "=" !!!!
          If Len(rsMFLD!COND_Value) Then
            Testo = Testo & Space(23 + extraIndent) & "WHEN " & rsMFLD!COND_Value & vbCrLf
          Else
            Testo = Testo & Space(23 + extraIndent) & "WHEN OTHER" & vbCrLf
          End If
          Testo = Testo & Space(26 + extraIndent) & "MOVE '" & rsMFLD!SOR & "' TO WS-MAPNAME" & vbCrLf
          Testo = Testo & Space(26 + extraIndent) & "MOVE '" & rsMFLD!SOR & "O' TO WS-FROMAREA" & vbCrLf
          Testo = Testo & Space(26 + extraIndent) & "MOVE '" & rsMFLD!SOR & "I' TO WS-INTOAREA" & vbCrLf
          lenPage = CalcolaLenPDpage(wFmtName, rsMFLD!ordinale, rsMSGout!MSGName)
          rsMFLD.MoveNext
          basePage = CInt(basePage) + CInt(lenPage)
        Wend
        rsMFLD.Close
'IRIS-DB        Testo = Testo & Space(23 + extraIndent) & "WHEN OTHER" & vbCrLf
'IRIS-DB        Testo = Testo & Space(26 + extraIndent) & "MOVE 'SP' TO DC-PCB-STATUS-CODE" & vbCrLf
        Testo = Testo & Space(20 + extraIndent) & "END-EVALUATE " & vbCrLf & vbCrLf
        Testo = Testo & Space(20 + extraIndent) & "MOVE IRIS-MESSAGE-AREA" & vbCrLf & Space(21) & "(1:LENGTH OF WS-SEND-AREA" & IIf(NumFmt > 1, "-" & wMapName, "") & ") TO " & wMapName & "-OUT" & vbCrLf
        Testo = Testo & vbCrLf
      Else
        If Not IIf(IsNull(rsMFLD!SOR), "", rsMFLD!SOR) = "" Then
          Testo = Testo & Space(20 + extraIndent) & "MOVE '" & rsMFLD!SOR & "' TO WS-MAPNAME" & vbCrLf
          Testo = Testo & Space(20 + extraIndent) & "MOVE '" & rsMFLD!SOR & "O' TO WS-FROMAREA" & vbCrLf
          Testo = Testo & Space(20 + extraIndent) & "MOVE '" & rsMFLD!SOR & "I' TO WS-INTOAREA" & vbCrLf
        Else
          Testo = Testo & Space(20 + extraIndent) & "MOVE '" & wMapName & "' TO WS-MAPNAME" & vbCrLf
          Testo = Testo & Space(20 + extraIndent) & "MOVE '" & wMapName & "O' TO WS-FROMAREA" & vbCrLf
          Testo = Testo & Space(20 + extraIndent) & "MOVE '" & wMapName & "I' TO WS-INTOAREA" & vbCrLf
        End If
        Testo = Testo & Space(20 + extraIndent) & "MOVE LNKDC-MFSMAP-AREA TO " _
               & Replace(rsMSGout!MSGName, "@", "") & "-OUT" & vbCrLf
        Testo = Testo & vbCrLf
      End If
    End If
    rsMSGout.MoveNext
  Wend
  rsMSGout.Close
  If CurrFmt = 1 Then
    Testo = Testo & Space(17 + extraIndent) & "WHEN OTHER" & vbCrLf
    Testo = Testo & Space(20 + extraIndent) & "MOVE 'SP' TO DC-PCB-STATUS-CODE" & vbCrLf
    Testo = Testo & Space(14 + extraIndent) & "END-EVALUATE" & vbCrLf
  Else
    RTOneMapRoutine.SelStart = vPos
    RTOneMapRoutine.SelLength = 0
    RTOneMapRoutine.SelText = Testo
    Testo = ""
    vPos = RTOneMapRoutine.find("#MULTIOPER#", 1)
  End If
  If NumFmt = 1 Then
    Testo = Testo & vbCrLf
    Testo = Testo & Space(14 + extraIndent) & "PERFORM SEND-MAP-" & wMapName & vbCrLf
    Testo = Testo & Space(17 + extraIndent) & "THRU SEND-MAP-" & wMapName & "-EX" & vbCrLf
  End If
  If CurrFmt = 1 Then
    Testo = Testo & Space(11 + extraIndent) & "ELSE" & vbCrLf
  End If
  If NumFmt > 1 Then
    If CurrFmt = 1 Then
      Testo = Testo & Space(14 + extraIndent) & "EVALUATE IRIS-DC-MSG-NAME" & vbCrLf
    End If
    Set rsMSGout = m_fun.Open_Recordset("Select MSGNAME from PsDli_mfsMSG where FMTName = '" & wFmtName & "' and DevType = 'INPUT'") 'IRIS-DB: da rivedere se IRIS framework la mappa di input come dovrebbe
    While Not rsMSGout.EOF
      Testo = Testo & Space(17 + extraIndent) & "WHEN '" & rsMSGout!MSGName & "'" & vbCrLf
      Testo = Testo & Space(20 + extraIndent) & "MOVE '" & wMapName & "' TO WS-MAPNAME" & vbCrLf
      Testo = Testo & Space(20 + extraIndent) & "PERFORM RECEIVE-MAP-" & wMapName & vbCrLf
      Testo = Testo & Space(23 + extraIndent) & "THRU RECEIVE-MAP-" & wMapName & "-EX" & vbCrLf
      rsMSGout.MoveNext
    Wend
    rsMSGout.Close
    Testo = Testo & "#MULTIOPER#" & vbCrLf
    If CurrFmt = 1 Then
      Testo = Testo & Space(14 + extraIndent) & "END-EVALUATE" & vbCrLf
    End If
  Else
    Testo = Testo & Space(14 + extraIndent) & "PERFORM RECEIVE-MAP-" & wMapName & vbCrLf
    Testo = Testo & Space(17 + extraIndent) & "THRU RECEIVE-MAP-" & wMapName & "-EX" & vbCrLf
  End If
  If CurrFmt = 1 Then
    Testo = Testo & Space(11 + extraIndent) & "END-IF" & vbCrLf
    strpunto = "."
    Testo = Testo & Space(11 + extraIndent) & "GO TO MAIN-END" & strpunto & vbCrLf
    Testo = Testo & vbCrLf
  End If
  RTOneMapRoutine.SelStart = vPos
  If CurrFmt > 1 Then
    RTOneMapRoutine.SelLength = 13
  Else
    RTOneMapRoutine.SelLength = 0
  End If
  RTOneMapRoutine.SelText = Testo
    
'SEND
  Testo = "      *----------------------------------------------------------------*" & vbCrLf
  Testo = Testo & "      *  Operation  : SEND                                             *" & vbCrLf
  Testo = Testo & "      *  On Map     : " & wMapName & Space(49 - Len(wMapName)) & "*" & vbCrLf
  Testo = Testo & "      *----------------------------------------------------------------*" & vbCrLf
  
  wfine = RTOneMapRoutine.find("#SEND-PARA#", 1)
  
  wIstr = "SEND"
  GbIntestazionePerform = "SEND-MAP-"
  ImsDCIstr.istr = wIstr
  ImsCpyMap = "Unified" 'IRIS-DB non so qual era lo scopo di questo campo all'inizio, ma visto che non serve lo uso a piacere
  Testo = Testo & AggRtImsDC_CICS(pIdOggetto, rst, wFmtName, ImsCpyMap, NumFmt, CurrFmt, wIstr)
  Testo = Testo & Space(7) & "SEND-MAP-" & wMapName & "-EX." & vbCrLf
  Testo = Testo & Space(11) & "EXIT." & vbCrLf
 
  RTOneMapRoutine.SelLength = 0
  RTOneMapRoutine.SelStart = wfine
  RTOneMapRoutine.SelText = Testo
 
'RECEIVE
  Testo = "      *----------------------------------------------------------------*" & vbCrLf
  Testo = Testo & "      *  Operation  : RECEIVE                                          *" & vbCrLf
  Testo = Testo & "      *  On Map     : " & wMapName & Space(49 - Len(wMapName)) & "*" & vbCrLf
  Testo = Testo & "      *----------------------------------------------------------------*" & vbCrLf
  
  wfine = RTOneMapRoutine.find("#RECEIVE-PARA#", 1)
  wIstr = "RECEIVE"
  GbIntestazionePerform = "RECEIVE-MAP-"
  ImsDCIstr.istr = wIstr
  ImsCpyMap = "Unified" 'IRIS-DB non so qual era lo scopo di questo campo all'inizio, ma visto che non serve lo uso a piacere
  Testo = Testo & AggRtImsDC_CICS(pIdOggetto, rst, wFmtName, ImsCpyMap, NumFmt, CurrFmt, wIstr)
   
  Testo = Testo & Space(7) & GbIntestazionePerform & wMapName & "-EX." & vbCrLf
  Testo = Testo & Space(11) & "EXIT." & vbCrLf
 
  RTOneMapRoutine.SelLength = 0
  RTOneMapRoutine.SelStart = wfine
  RTOneMapRoutine.SelText = Testo
 
  Dim arrTasti(24) As String, j As Integer, maxlenPFK As Integer
  RTOneMapRoutine.text = RTOneMapRoutine.text & vbCrLf
  'gestione PFK
  'IRIS-DB per ora tolgo il controllo, al limite si dovrebbe fare per tutti i tipi di 3270, ma in generale se ci sono righe nella tabella, sono per forza tasti funzione
  ' IRIS-DB Set rst = m_fun.Open_Recordset("select * from PsDLI_MFSPfK where FMTName = '" & wMapName & "' and Dev = '(3270,2)' ")
  Set rst = m_fun.Open_Recordset("select * from PsDLI_MFSPfK where FMTName = '" & wFmtName & "'")
  While Not rst.EOF
    If IsNumeric(rst!Tasto) Then
      arrTasti(CInt(rst!Tasto)) = rst!Valore
    End If
    If Len(rst!Valore) - 2 > maxlenPFK Then
      maxlenPFK = Len(rst!Valore) - 2
    End If
    rst.MoveNext
  Wend
  rst.Close
  For j = 1 To 24
    vPos = RTOneMapRoutine.find("#value" & j & "#", 1)
    If vPos = -1 Then Exit For
    RTOneMapRoutine.SelStart = vPos
    RTOneMapRoutine.SelLength = 7 + Len(CStr(j))
    If Not arrTasti(j) = "" Then
      RTOneMapRoutine.SelText = arrTasti(j)
    Else
      RTOneMapRoutine.SelText = "LOW-VALUE"
    End If
  Next
  If testoPFKFLD = "" Then ' pagina singola
    Set rst = m_fun.Open_Recordset("select PFKField from PsDLI_MFS where FMTName = '" & wFmtName & "' and DevType = '(3270,2)' ")
    If Not rst.EOF Then
      Set rsfield = m_fun.Open_Recordset("select Lunghezza,Value from PsDLI_MFSmsgCmp where FMTName = '" & wFmtName & "'  and Nome ='" & rst!PFKField & "'")
      If Not rsfield.EOF Then
        If NumFmt = 1 Then 'IRIS-DB gestione multimappa
          vPos = RTOneMapRoutine.find("#LENPFK#", 1)
          RTOneMapRoutine.SelStart = vPos
          RTOneMapRoutine.SelLength = 8  'Len(CStr(rsField!Lunghezza))
          RTOneMapRoutine.SelText = IIf(rsfield!Lunghezza <= 0, maxlenPFK, rsfield!Lunghezza)
        Else
          vPos = RTOneMapRoutine.find("01 WMFS-PFK ", 1)
          RTOneMapRoutine.SelStart = vPos
          RTOneMapRoutine.SelLength = 42
          RTOneMapRoutine.SelText = "01 WMFS-PFK-" & Trim(wMapName) & Space(13 - Len(Trim(wMapName))) & "PIC X(" & IIf(rsfield!Lunghezza <= 0, maxlenPFK, rsfield!Lunghezza) & ")." & vbCrLf & "       01 WMFS-PFK              PIC X(#LENPFK#)."
        End If
        
        '*************** DFVALUE
        vPos = RTOneMapRoutine.find("#dfvalue#", 1)
        RTOneMapRoutine.SelStart = vPos
        RTOneMapRoutine.SelLength = 9
        
        If Not IsNull(rsfield!Value) And Not rsfield!Value = "" Then
          'IRIS-DB RTOneMapRoutine.SelText = "MOVE " & rsField!Value & " TO WMFS-PFK" 'IRIS-DB
          RTOneMapRoutine.SelText = rsfield!Value 'IRIS-DB
        Else
          RTOneMapRoutine.SelText = "SPACE" 'IRIS-DB rimesso a space da LOW-VALUE
        End If
        
      Else
        If NumFmt = 1 Then 'IRIS-DB gestione multimappa
          vPos = RTOneMapRoutine.find("#LENPFK#", 1)
          RTOneMapRoutine.SelStart = vPos
          RTOneMapRoutine.SelLength = 8  'Len(CStr(rsField!Lunghezza))
          RTOneMapRoutine.SelText = "2"
        Else
          vPos = RTOneMapRoutine.find("01 WMFS-PFK ", 1)
          RTOneMapRoutine.SelStart = vPos
          RTOneMapRoutine.SelLength = 42
          RTOneMapRoutine.SelText = "01 WMFS-PFK-" & Trim(wMapName) & Space(13 - Len(Trim(wMapName))) & "PIC X(2)." & vbCrLf & "       01 WMFS-PFK              PIC X(#LENPFK#)."
        End If
        vPos = RTOneMapRoutine.find("#dfvalue#", 1)
        If Not vPos = -1 Then
          RTOneMapRoutine.SelStart = vPos
          RTOneMapRoutine.SelLength = 9
          RTOneMapRoutine.SelText = "SPACE" 'IRIS-DB rimesso a space da LOW-VALUE
        End If
      End If
      rsfield.Close
    
    Else
      If NumFmt = 1 Then 'IRIS-DB gestione multimappa
        vPos = RTOneMapRoutine.find("#LENPFK#", 1)
        RTOneMapRoutine.SelStart = vPos
        RTOneMapRoutine.SelLength = 8  'Len(CStr(rsField!Lunghezza))
        RTOneMapRoutine.SelText = "2"
      Else
        vPos = RTOneMapRoutine.find("01 WMFS-PFK ", 1)
        RTOneMapRoutine.SelStart = vPos
        RTOneMapRoutine.SelLength = 42
        RTOneMapRoutine.SelText = "01 WMFS-PFK-" & Trim(wMapName) & Space(13 - Len(Trim(wMapName))) & "PIC X(2)." & vbCrLf & "       01 WMFS-PFK              PIC X(#LENPFK#)."
      End If
      vPos = RTOneMapRoutine.find("#dfvalue#", 1)
      RTOneMapRoutine.SelStart = vPos
      RTOneMapRoutine.SelLength = 9
      RTOneMapRoutine.SelText = "SPACE" 'IRIS-DB rimesso a space da LOW-VALUE
    End If
    rst.Close
  Else  ' multipagina
    If NumFmt = 1 Then 'IRIS-DB gestione multimappa
      vPos = RTOneMapRoutine.find("#LENPFK#", 1)
      RTOneMapRoutine.SelStart = vPos
      RTOneMapRoutine.SelLength = 8  'Len(CStr(rsField!Lunghezza))
      RTOneMapRoutine.SelText = IIf(lenPFKField <= 0, maxlenPFK, lenPFKField)
    Else
      vPos = RTOneMapRoutine.find("01 WMFS-PFK ", 1)
      RTOneMapRoutine.SelStart = vPos
      RTOneMapRoutine.SelLength = 42
      RTOneMapRoutine.SelText = "01 WMFS-PFK-" & Trim(wMapName) & Space(13 - Len(Trim(wMapName))) & "PIC X(" & IIf(lenPFKField <= 0, maxlenPFK, lenPFKField) & ")." & vbCrLf & "       01 WMFS-PFK              PIC X(#LENPFK#)."
    End If
    vPos = RTOneMapRoutine.find("#dfvalue#", 1)
    RTOneMapRoutine.SelStart = vPos
    RTOneMapRoutine.SelLength = 9
    RTOneMapRoutine.SelText = testoPFKFLD
  End If
'COPY MOVE
  nameCicsO = wMapName & "O"
  nameCicsI = wMapName & "I"
  If Not ColCampiDflattr.Count = 0 Then
    TextField = "DFLT-ATTR-" & wMapName & "." & vbCrLf
    TextField = TextField & Space(11) & "MOVE LOW-VALUE TO "
    wCampospaces = 0
    For i = 1 To ColCampiDflattr.Count
      TextField = TextField & Space(wCampospaces) & ColCampiDflattr.item(i) & "A OF " & nameCicsI & vbCrLf
      wCampospaces = 29
    Next i
    TextField = Left(TextField, Len(TextField) - 2) & "." & vbCrLf
    TextField = TextField & Space(7) & "EX-DFLT-ATTR-" & wMapName & "." & vbCrLf & Space(11) & "EXIT." & vbCrLf
  End If
  vPos = RTOneMapRoutine.find("#FIELD#.", 1)
  RTOneMapRoutine.SelStart = vPos
  RTOneMapRoutine.SelLength = 8
  RTOneMapRoutine.SelText = TextField 'IRIS-DB: for now not used with multifmt, because L&G is not using DFLT-ATTR at all; in any case the field ColCampiDflattr.Count is never set anywhere & "#FIELD#." & vbCrLf
  '*************
  vPos = RTOneMapRoutine.find("#COPYMOVE#.", 1)
  RTOneMapRoutine.SelStart = vPos
  RTOneMapRoutine.SelLength = 13
'  RTOneMapRoutine.SelText = " COPY " & NameMoveI & "."
'  RTOneMapRoutine.SelText = vbCrLf & Space(11) & "COPY " & NameMoveO & "." & vbCrLf
  RTCopy.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Output-Prj\Cics\Cpy\" & NameMoveI
  RTCopy.text = Replace(RTCopy.text, "IRIS-WS-MESSAGE-AREA", "WS-RECEIVE-AREA" & IIf(NumFmt > 1, "-" & wMapName, ""))
  If NumFmt > 1 Then
    RTCopy.text = Replace(RTCopy.text, "MOVE WMFS-PFK ", "MOVE WMFS-PFK-" & wMapName & " ")
    RTCopy.text = Replace(RTCopy.text, " TO I-PFKY", vbCrLf & Space(15) & "TO I-PFKY OF WS-RECEIVE-AREA-" & wMapName & " ")
  End If
  RTOneMapRoutine.SelText = RTCopy.text
  RTCopy.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Output-Prj\Cics\Cpy\" & NameMoveO
  RTOneMapRoutine.SelText = RTCopy.text & "#COPYMOVE#." & vbCrLf
  Exit Function
errFun:
  m_fun.WriteLog "ImsDC : AggRoutDLI_CICS_Unified - Function - Errore : " & err, "DR - GenRout"
  Resume Next
End Function
Function getPFKField(wMapName As String, plunghezza As Integer, lpage As Integer) As String
Dim rst As Recordset, rsfield As Recordset

  Set rst = m_fun.Open_Recordset("select PFKField from PsDLI_MFS where FMTName = '" & wMapName & "' and DevType = '(3270,2)' ")
    If Not rst.EOF Then
      Set rsfield = m_fun.Open_Recordset("select Lunghezza,Value from PsDLI_MFSmsgCmp where FMTName = '" & wMapName & "'  and Nome ='" & rst!PFKField & "' and LPage =" & lpage)
      If Not rsfield.EOF Then
        getPFKField = rsfield!Value
        plunghezza = rsfield!Lunghezza
      Else
        getPFKField = "LOW-VALUE"
        plunghezza = 0
      End If
      rsfield.Close
    End If
  rst.Close
End Function
Public Function AggRtImsDC(Optional ByVal wIstrType As String = "GENERIC") As String
   Dim Testo As String

   Testo = Testo & Space(6) & "*" & vbCrLf
   
   Select Case Trim(ImsDCIstr.istr)
      Case "GU", "GN"
         Testo = Testo & Space(13) & "EXEC CICS RECEIVE MAP(LNKDC-MOD-MAPPS(1))" & vbCrLf
         Testo = Testo & Space(13) & "                  MAPSET(LNKDC-MOD-MAPPS(1))" & vbCrLf
         Testo = Testo & Space(13) & "END-EXEC." & vbCrLf
         Testo = Testo & Space(6) & "*" & vbCrLf
         Testo = Testo & Space(11) & "PERFORM MOVE-BMS2MFS THRU MOVE-BMS2MFS-EX." & vbCrLf
         
      Case "ISRT"
         Testo = Testo & Space(11) & "PERFORM MOVE-MFS2BMS THRU MOVE-MFS2BMS-EX." & vbCrLf
         Testo = Testo & Space(6) & "*" & vbCrLf
         Testo = Testo & Space(13) & "EXEC CICS SEND MAP(LNKDC-MOD-MAPPS(1))" & vbCrLf
         Testo = Testo & Space(13) & "               MAPSET(LNKDC-MOD-MAPPS(1))" & vbCrLf
         Testo = Testo & Space(13) & "END-EXEC." & vbCrLf
   
   End Select
   
   Testo = Testo & Space(6) & "* " & vbCrLf
   
   AggRtImsDC = Testo
End Function

Function GetCmpCursor(fmt As String, Optional lpage = "") As String
  Dim rsCmp As Recordset, querydpage As String
  Dim firstunprot As String, cmpcursor As String, findfirst As Boolean, findcursor As Boolean
  Dim cursor As String
  Dim cursor2 As String
    
  Set rsCmp = m_fun.Open_Recordset("select [cursor] from PsDli_MFS where FMTName = '" & fmt & "'")
  If Not rsCmp.EOF Then
    cursor = rsCmp!cursor
  End If
  rsCmp.Close
  If Not lpage = "" Then
    querydpage = " and DPageName ='" & lpage & "'"
  End If
  Set rsCmp = m_fun.Open_Recordset("Select nome,riga,colonna,attributo,occurs from PsDli_MFSCmp where FMTName = '" & fmt & "'" & querydpage & " order by OrdinaleCmp")
  While Not rsCmp.EOF
    If Not findfirst Then
      If Not InStr(rsCmp!Attributo, "PROT") > 0 And Not Left(rsCmp!nome, 1) = "#" Then
        firstunprot = rsCmp!nome
        If rsCmp!Occurs > 1 Then
          firstunprot = firstunprot & "01"
        End If
        findfirst = True
      End If
    End If
    If Not findcursor Then
      If InStr(cursor, "(" & Format(rsCmp!Riga, "000") & ",") > 0 Or InStr(cursor, "(" & Format(rsCmp!Riga, "00") & ",") > 0 Or InStr(cursor, "(" & rsCmp!Riga & ",") > 0 Then
         If InStr(cursor, "," & Format(rsCmp!colonna, "000") & ")") > 0 Or InStr(cursor, "," & Format(rsCmp!colonna, "00") & ")") > 0 Or InStr(cursor, "," & rsCmp!colonna & ")") > 0 Or InStr(cursor, "," & Format(rsCmp!colonna, "000") & ",") > 0 Or InStr(cursor, "," & Format(rsCmp!colonna, "00") & ",") > 0 Or InStr(cursor, "," & rsCmp!colonna & ",") > 0 Then 'IRIS-DB
           cmpcursor = rsCmp!nome
           If rsCmp!Occurs > 1 Then
            cmpcursor = cmpcursor & "01"
           End If
           findcursor = True
         End If
      End If
    End If
    rsCmp.MoveNext
  Wend
  rsCmp.Close
  If Not cmpcursor = "" Then
    GetCmpCursor = cmpcursor
  Else
    GetCmpCursor = firstunprot
  End If
End Function

Public Function AggRtImsDC_CICS(pIdOggetto As Long, rs As Recordset, wFmtName As String, wCicsName As String, NumFmt As Integer, CurrFmt As Integer, Optional ByVal wIstrType As String = "GENERIC") As String
  Dim Testo As String
  Dim wCampo As String
  Dim wMapCics As String
  Dim TextReceive As String
  Dim wMapSetCics As String
  
  Dim TextField As String
  Dim rsMFLD As Recordset
  Dim nameCicsO As String, nameCicsI As String, nameCics As String, CicsPage As String, CondField As String
  'AC 01/08/2006
  Dim Occurs As Integer, ColOccurs As Collection, i As Integer, j As Integer, prog As Integer
  Dim cicsoccurs As String, numdpage As Integer
  Dim basePage As String, lenPage As String, posCond As String, msginput As String, msgOutput As String
  Dim morepagein As Boolean, morepageout As Boolean, rsLpage As Recordset
  'Ac 21/05/07
  Dim cmpcursor As String, k As Integer
   
'IRIS-DB
   Dim colInfo As Collection
   Dim wMapsetName As String
   Dim wMapName As String
   On Error GoTo errFun 'IRIS-DB
   wMapName = wFmtName 'IRIS-DB
   
   Set colInfo = New Collection
   Set TbOggetti = Open_Recordset("select * from BS_oggetti where idoggetto = " & pIdOggetto)
   If Not TbOggetti.EOF Then
    nameCics = CStr(TbOggetti!nome)
    colInfo.Add pIdOggetto
    colInfo.Add nameCics
    colInfo.Add wMapName
   End If
  TbOggetti.Close
  wMapsetName = getEnvironmentParam(RetrieveParameterValues("IMSDC-BMSMAPSET")(0), "mappe", colInfo)
  If wMapsetName = "" Then wMapsetName = wMapName
  wMapName = getEnvironmentParam(RetrieveParameterValues("IMSDC-BMSMAP")(0), "mappe", colInfo)
  If m_ImsDll_DC.ImNomeDB = "LandG.mty" Then 'IRIS-DB: pezza per evitare di riconsegnare tutti i programmi a L&G; da rimuovere in seguito
    Set TbOggetti = Open_Recordset("select * from PsDLI_MFS where idoggetto = " & pIdOggetto)
    If TbOggetti.RecordCount = 1 Then
      wMapName = wMapsetName & "R"
    End If
    TbOggetti.Close
  End If
'IRIS-DB end
   
  wCampo = "[RETRIEVE-FROM-DB]"
  Testo = Testo & Space(7) & GbIntestazionePerform & wMapName & "." & vbCrLf
  Testo = Testo & vbCrLf
  '***************
  
'  Set rsMFLD = m_fun.Open_Recordset("Select FMTName from PsDLI_MFS where IdOggetto = " & pIdOggetto & " and DevType = '(3270,2)'")
'  If Not rsMFLD.EOF Then
'    nameCicsO = rsMFLD!FMtName & "O"
'    nameCicsI = rsMFLD!FMtName & "I"
'  Else
'    'debug
'    DoEvents
'  End If

  testoPFKFLD = ""

  Set rsMFLD = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where FMTName = '" & wFmtName & "' And DevType = 'INPUT'")
  If Not rsMFLD.EOF Then
    msginput = rsMFLD!MSGName
  End If
  rsMFLD.Close
  Set rsMFLD = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where FMTName = '" & wFmtName & "' And DevType = 'OUTPUT'")
  If Not rsMFLD.EOF Then
    msgOutput = rsMFLD!MSGName
  End If
  rsMFLD.Close
  
  Set rsMFLD = m_fun.Open_Recordset("Select * from PsMFS_MSG_LPAGE where MSG ='" & msgOutput & "' order by Ordinale")
  If rsMFLD.EOF Or rsMFLD.RecordCount = 1 Then
    morepageout = False
    rsMFLD.Close
    Set rsMFLD = m_fun.Open_Recordset("Select DPageName from PsDLI_MFSCmp where FMTName = '" & wFmtName & "'")
    If Not rsMFLD.EOF Then
      If Not IsNull(rsMFLD!DPageName) Then
       If Not rsMFLD!DPageName = "" Then
          nameCicsO = Left(rsMFLD!DPageName, 7) & "O"
       Else
          nameCicsO = wMapName & "O"
       End If
      Else
        nameCicsO = wMapName & "O"
      End If
    Else
      nameCicsO = wMapName & "O"
    End If
    rsMFLD.Close
  Else
    morepageout = True
    rsMFLD.Close
    ' gestire multipagina output
  End If
  
  If Not msginput = "" Then
    Set rsMFLD = m_fun.Open_Recordset("Select * from PsMFS_MSG_LPAGE where MSG ='" & msginput & "' order by Ordinale")
    If rsMFLD.EOF Or rsMFLD.RecordCount = 1 Then
      morepagein = False
      rsMFLD.Close
      Set rsMFLD = m_fun.Open_Recordset("Select DPageName from PsDLI_MFSCmp where FMTName = '" & wFmtName & "'")
      If Not rsMFLD.EOF Then
        If Not IsNull(rsMFLD!DPageName) Then
         If Not rsMFLD!DPageName = "" Then
            nameCicsI = Left(rsMFLD!DPageName, 7) & "I"
         Else
            nameCicsI = wMapName & "I"
         End If
        Else
          nameCicsI = wMapName & "I"
        End If
      Else
        nameCicsI = wMapName & "I"
      End If
      rsMFLD.Close
    Else
      morepagein = True
      ' gestire multipagina input
    End If
  Else
    nameCicsI = Left(nameCicsO, Len(nameCicsO) - 1) & "I"
  End If
  
  'rsMFLD.Close
  nameCics = wMapName
  'nameCics = Replace(nameCics, "CYF", "CY")
                  
  If Trim(ImsDCIstr.istr) = "SEND" Then
    Testo = Testo & Space(11) & "MOVE 'N' TO FLG-CURS-OK." & vbCrLf
    Testo = Testo & Space(11) & "PERFORM GET-TMDTMFS THRU EX-GET-TMDTMFS." & vbCrLf
    If wCicsName = "Unified" Then
      Testo = Testo & Space(11) & "MOVE IRIS-MESSAGE-AREA(1:LENGTH OF WS-SEND-AREA" & IIf(NumFmt > 1, "-" & wMapName, "") & ")" & vbCrLf
      If NumFmt = 1 Then 'IRIS-DB
        Testo = Testo & Space(11) & "  TO      WS-SEND-AREA(1:LENGTH OF WS-SEND-AREA)" & vbCrLf
      Else
        Testo = Testo & Space(11) & "  TO WS-SEND-AREA-" & Trim(wMapName) & "(1:LENGTH OF WS-SEND-AREA" & IIf(NumFmt > 1, "-" & wMapName, "") & ")" & vbCrLf
      End If
    End If
    Testo = Testo & Space(11) & "PERFORM MOVE-TO-" & wMapName & " THRU EX-MOVE-TO-" & wMapName & "." & vbCrLf
    
     'AC 06/02/07 Perform attributi portata dentro copy move
  End If
                   
  Set rsMFLD = m_fun.Open_Recordset("Select * from PsDLI_MFSmsgCmp where FMTName = '" & wFmtName _
      & "' and MSGName ='" & msgOutput & "' order by Ordinale")

  If Not rsMFLD.EOF Then
    rsMFLD.MoveLast
    numdpage = rsMFLD!lpage '� il MAX
    rsMFLD.MoveFirst
  End If
  Set ColCampiDflattr = New Collection
  While Not rsMFLD.EOF
    wCampo = Trim(IIf(rsMFLD!NomeBMS = "" Or IsNull(rsMFLD!NomeBMS), rsMFLD!nome, rsMFLD!NomeBMS))
    'If Len(wCampo) >= 8 Then
      'wCampo = Left(wCampo, 7)
    'End If
    If wCampo <> "DFHMSD" And wCampo <> "DFHMDI" And wCampo <> "DFHMDF" And Not Left(wCampo, 1) = "#" Then
            Select Case Trim(ImsDCIstr.istr)
               Case "RECEIVE"
                   'TextReceive = TextReceive & Space(11) & "MOVE " & wCampo & "I" & Space(10 - Len(wCampo)) & "TO I-" & wCampo & vbCrLf
               Case "SEND"
               'AC 06/02/07 move portate dentro copy move
            End Select
    End If
    If Not rsMFLD.EOF Then
     rsMFLD.MoveNext
    End If
  Wend
      
      Select Case Trim(ImsDCIstr.istr)
         Case "RECEIVE"
            TextReceive = TextReceive & Space(6) & "*   -------------------" & vbCrLf
            TextReceive = TextReceive & Space(6) & "*   EXECUTE RECEIVE MAP" & vbCrLf
            TextReceive = TextReceive & Space(6) & "*   -------------------" & vbCrLf & vbCrLf
         
            'If morepagein Then
              
              Set rsLpage = m_fun.Open_Recordset("Select distinct DPageName from PsDli_MFSCmp where FMTName = '" & wFmtName & "' and DPageName is not Null")
              If rsLpage.RecordCount > 1 Then
                TextReceive = TextReceive & Space(11) & "EVALUATE IRIS-MODNAME" & vbCrLf
                While Not rsLpage.EOF
                  TextReceive = TextReceive & Space(14) & "WHEN '" & Left(rsLpage!DPageName, 7) & "'" & vbCrLf
                  TextReceive = TextReceive & Space(17) & "EXEC CICS RECEIVE MAP('" & Left(rsLpage!DPageName, 7) & "')" & vbCrLf
                  TextReceive = TextReceive & Space(17) & "                  MAPSET('" & wMapsetName & "')" & vbCrLf
                  'TextReceive = TextReceive & Space(13) & "                  INTO(LNKDC-INTOAREA)" & vbCrLf
                  'TextReceive = TextReceive & Space(13) & "                  INTO(" & nameCicsI & ")" & vbCrLf
                  TextReceive = TextReceive & Space(17) & "                  RESP(RESPONSEX)" & vbCrLf 'IRIS-DB
                  TextReceive = TextReceive & Space(17) & "END-EXEC" & vbCrLf & vbCrLf
                  
                  TextReceive = TextReceive & Space(17) & "MOVE SPACES TO DC-PCB-STATUS-CODE" & vbCrLf & vbCrLf
''''                  TextReceive = TextReceive & Space(19) & "IF RESPONSEX = DFHRESP(MAPFAIL)" & vbCrLf
''''                  TextReceive = TextReceive & Space(19) & "   MOVE '  ' TO DC-PCB-STATUS-CODE" & vbCrLf
''''                  TextReceive = TextReceive & Space(19) & "   MOVE " & rsLpage!DPageName & "I(21:69) TO LNKDC-MFSMAP-AREA(13:69)" & vbCrLf
''''                  TextReceive = TextReceive & Space(19) & "   GO TO " & GbIntestazionePerform & wMapName & "-" & "EX" & vbCrLf
''''                  TextReceive = TextReceive & Space(19) & "END-IF" & vbCrLf & vbCrLf
                  rsLpage.MoveNext
                Wend
                TextReceive = TextReceive & Space(14) & "WHEN OTHER" & vbCrLf
                TextReceive = TextReceive & Space(17) & "MOVE 'RP' TO DC-PCB-STATUS-CODE" & vbCrLf
                TextReceive = TextReceive & Space(11) & "END-EVALUATE." & vbCrLf
              ElseIf rsLpage.RecordCount = 1 And Not rsLpage!DPageName = "" Then
                TextReceive = TextReceive & Space(11) & "EXEC CICS RECEIVE MAP('" & Left(rsLpage!DPageName, 7) & "')" & vbCrLf
                TextReceive = TextReceive & Space(11) & "                  MAPSET('" & wMapsetName & "')" & vbCrLf
                TextReceive = TextReceive & Space(11) & "                  RESP(RESPONSEX)" & vbCrLf 'IRIS-DB
                TextReceive = TextReceive & Space(11) & "END-EXEC." & vbCrLf & vbCrLf
                
                TextReceive = TextReceive & Space(11) & "MOVE SPACES TO DC-PCB-STATUS-CODE." & vbCrLf & vbCrLf
''''                TextReceive = TextReceive & Space(11) & "IF RESPONSEX = DFHRESP(MAPFAIL)" & vbCrLf
''''                TextReceive = TextReceive & Space(11) & "   MOVE '  ' TO DC-PCB-STATUS-CODE" & vbCrLf
''''                TextReceive = TextReceive & Space(11) & "   MOVE " & rsLpage!DPageName & "I(21:69) TO LNKDC-MFSMAP-AREA(13:69)" & vbCrLf
''''                TextReceive = TextReceive & Space(11) & "   GO TO " & GbIntestazionePerform & wMapName & "-" & "EX" & vbCrLf
''''                TextReceive = TextReceive & Space(11) & "END-IF." & vbCrLf & vbCrLf
              Else
                TextReceive = TextReceive & Space(11) & "EXEC CICS RECEIVE MAP('" & wMapName & "')" & vbCrLf
                TextReceive = TextReceive & Space(11) & "                  MAPSET('" & wMapsetName & "')" & vbCrLf
                TextReceive = TextReceive & Space(11) & "                  RESP(RESPONSEX)" & vbCrLf 'IRIS-DB
                TextReceive = TextReceive & Space(11) & "END-EXEC." & vbCrLf & vbCrLf
                
                TextReceive = TextReceive & Space(11) & "MOVE SPACES TO DC-PCB-STATUS-CODE." & vbCrLf & vbCrLf
''''                TextReceive = TextReceive & Space(11) & "IF RESPONSEX = DFHRESP(MAPFAIL)" & vbCrLf
''''                TextReceive = TextReceive & Space(11) & "   MOVE '  ' TO DC-PCB-STATUS-CODE" & vbCrLf
''''                TextReceive = TextReceive & Space(11) & "   MOVE " & wMapName & "I(21:69) TO LNKDC-MFSMAP-AREA(13:69)" & vbCrLf
''''                TextReceive = TextReceive & Space(11) & "   GO TO " & GbIntestazionePerform & wMapName & "-" & "EX" & vbCrLf
''''                TextReceive = TextReceive & Space(11) & "END-IF." & vbCrLf & vbCrLf
              End If
              rsLpage.Close
            
            

            TextReceive = TextReceive & Space(11) & "IF RESPONSEX = DFHRESP(INVREQ)" & vbCrLf
            TextReceive = TextReceive & Space(11) & "   MOVE 'ZZ' TO DC-PCB-STATUS-CODE" & vbCrLf
            TextReceive = TextReceive & Space(11) & "   GO TO " & GbIntestazionePerform & wMapName & "-" & "EX" & vbCrLf
            TextReceive = TextReceive & Space(11) & "END-IF." & vbCrLf & vbCrLf

            TextReceive = TextReceive & Space(11) & "IF RESPONSEX = DFHRESP(INVMPSZ)" & vbCrLf
            TextReceive = TextReceive & Space(11) & "   MOVE 'ZZ' TO DC-PCB-STATUS-CODE" & vbCrLf
            TextReceive = TextReceive & Space(11) & "   GO TO " & GbIntestazionePerform & wMapName & "-" & "EX" & vbCrLf
            TextReceive = TextReceive & Space(11) & "END-IF." & vbCrLf & vbCrLf
            
            TextReceive = TextReceive & "      *   THE NEXT STATEMENTS WILL BE REALLY ACTIVATED ONLY IN CASE OF" & vbCrLf
            TextReceive = TextReceive & "      *   CONVERSATIONAL APPROACH" & vbCrLf
            TextReceive = TextReceive & Space(11) & "IF EIBAID = DFHCLEAR" & vbCrLf
            'IRIS-DB TextReceive = TextReceive & Space(11) & "   MOVE 'QC' TO DC-PCB-STATUS-CODE" & vbCrLf
            TextReceive = TextReceive & Space(11) & "   GO TO RECEIVE-MAP-" & wMapName & "-EX" & vbCrLf
            TextReceive = TextReceive & Space(11) & "END-IF." & vbCrLf & vbCrLf

            TextReceive = TextReceive & Space(11) & "PERFORM ANALYZE-PFK" & IIf(NumFmt > 1, "-" & wMapName, "") & " THRU EX-ANALYZE-PFK" & IIf(NumFmt > 1, "-" & wMapName, "") & vbCrLf
            TextReceive = TextReceive & Space(11) & "PERFORM MOVE-FROM-" & wMapName & " THRU EX-MOVE-FROM-" & wMapName & vbCrLf

            'SQ
            If morepagein = False Then
              If Not msginput = "" Then
               'IRIS-DB TextReceive = TextReceive & Space(11) & "MOVE "
                'IRIS-DB  & NameStructI & "-AREA" & " TO LNKDC-MFSMAP-AREA." & vbCrLf
                If wCicsName = "Unified" Then
                  If NumFmt = 1 Then 'IRIS-DB
                    TextReceive = TextReceive & Space(11) & "COMPUTE IN-LL = LENGTH OF WS-RECEIVE-AREA" & vbCrLf
                    TextReceive = TextReceive & Space(11) & "MOVE   WS-RECEIVE-AREA(1:IN-LL OF WS-RECEIVE-AREA)" & vbCrLf
                    TextReceive = TextReceive & Space(11) & "  TO IRIS-MESSAGE-AREA(1:IN-LL OF WS-RECEIVE-AREA)" & vbCrLf
                  Else
                    TextReceive = TextReceive & Space(11) & "COMPUTE IN-LL OF WS-RECEIVE-AREA-" & wMapName & " =" & vbCrLf
                    TextReceive = TextReceive & Space(11) & "       LENGTH OF WS-RECEIVE-AREA-" & wMapName & vbCrLf
                    TextReceive = TextReceive & Space(11) & "MOVE WS-RECEIVE-AREA-" & wMapName & "(1:IN-LL" & vbCrLf & Space(12) & " OF WS-RECEIVE-AREA-" & wMapName & ")" & vbCrLf
                    TextReceive = TextReceive & Space(11) & "  TO IRIS-MESSAGE-AREA(1:IN-LL OF WS-RECEIVE-AREA-" & wMapName & ")" & vbCrLf
                  End If
                Else
                  TextReceive = TextReceive & Space(11) & "COMPUTE IN-LL = LENGTH OF IRIS-WS-MESSAGE-AREA" & vbCrLf
                  TextReceive = TextReceive & Space(11) & "MOVE IRIS-WS-MESSAGE-AREA(1:IN-LL OF IRIS-WS-MESSAGE-AREA)" & vbCrLf
                  TextReceive = TextReceive & Space(11) & "  TO IRIS-MESSAGE-AREA(1:IN-LL OF IRIS-WS-MESSAGE-AREA)" & vbCrLf
                End If
              End If
            Else
              TextReceive = TextReceive & Space(6) & "*--------------------------------------------- " & vbCrLf
              TextReceive = TextReceive & Space(6) & "*-----------------PAGE SELECTION---------------" & vbCrLf
              TextReceive = TextReceive & Space(6) & "*--------------------------------------------- " & vbCrLf
              TextReceive = TextReceive & Space(12) & "EVALUATE IRIS-MODNAME" & vbCrLf
              basePage = 1
              testoPFKFLD = "EVALUATE IRIS-MODNAME" & vbCrLf

              Set rsMFLD = m_fun.Open_Recordset("Select * from PsMFS_MSG_LPAGE where MSG ='" & msginput & "' order by Ordinale")
              While Not rsMFLD.EOF
                TextReceive = TextReceive & Space(15) & "WHEN '" & rsMFLD!SOR & "'" & vbCrLf
                lenPage = CalcolaLenPDpage(wFmtName, rsMFLD!ordinale, "INPUT")
                ' AC 25/10/2007
                testoPFKFLD = testoPFKFLD & Space(17) & "WHEN '" & rsMFLD!SOR & "'" & vbCrLf
                ' move nel tasto PFKFLD
                testoPFKFLD = testoPFKFLD & Space(20) & "MOVE " & getPFKField(wFmtName, lenPFKField, rsMFLD!ordinale) & " TO WMFS-PFK" & IIf(NumFmt > 1, "-" & wMapName, "") & vbCrLf
                basePage = 1
                TextReceive = TextReceive & Space(18) & "MOVE " & basePage & Space(11 - Len(basePage)) & "TO BASE-PAGE" & vbCrLf
                TextReceive = TextReceive & Space(18) & "MOVE " & lenPage & Space(11 - Len(lenPage)) & "TO LEN-PAGE" & vbCrLf
                basePage = CInt(basePage) + CInt(lenPage)
                rsMFLD.MoveNext
              Wend
              testoPFKFLD = testoPFKFLD & Space(14) & "END-EVALUATE"
              rsMFLD.Close
              TextReceive = TextReceive & Space(15) & "WHEN OTHER" & vbCrLf
              TextReceive = TextReceive & Space(18) & "MOVE 'RP' TO DC-PCB-STATUS-CODE" & vbCrLf
              TextReceive = TextReceive & Space(12) & "END-EVALUATE " & vbCrLf & vbCrLf
              TextReceive = TextReceive & Space(11) & "MOVE SPACES TO LNKDC-MFSMAP-AREA " & vbCrLf
              TextReceive = TextReceive & Space(11) & "MOVE " _
                                                    & NameStructI & "-AREA" & "(BASE-PAGE:LEN-PAGE) TO LNKDC-MFSMAP-AREA." & vbCrLf
            End If
            TextReceive = TextReceive & Space(11) & "." & vbCrLf
            
            
         Case "SEND"
          Testo = Testo & Space(6) & "*--------------------------------------------- " & vbCrLf
          Testo = Testo & Space(6) & "*---------------EXECUTE-SEND-MAP--------------- " & vbCrLf
          Testo = Testo & Space(6) & "*--------------------------------------------- " & vbCrLf
          'If morepageout Then
          Set rsLpage = m_fun.Open_Recordset("Select distinct DPageName from PsDli_MFSCmp where FMTName = '" & wFmtName & "' and DPageName is not Null")
          If rsLpage.RecordCount > 1 Then
            Testo = Testo & Space(11) & "EVALUATE WS-MAPNAME" & vbCrLf
            While Not rsLpage.EOF
              cmpcursor = GetCmpCursor(wFmtName, rsLpage!DPageName)
              Testo = Testo & Space(14) & "WHEN '" & Left(rsLpage!DPageName, 7) & "'" & vbCrLf
              If Not cmpcursor = "" And Not InStr(cmpcursor, "#") > 0 Then 'IRIS-DB esclude i literal
                Testo = Testo & Space(17) & "IF FLG-CURS-OK = 'N'" & vbCrLf
                Testo = Testo & Space(17) & "   MOVE -1 TO " & cmpcursor & "L" & vbCrLf
                Testo = Testo & Space(17) & "END-IF" & vbCrLf
'''''''                Testo = Testo & Space(17) & "MOVE O-CURFLD TO IMS-CURPOS" & vbCrLf
'''''''             MOVE O-CURFLD TO IMS-CURPOS
'''''''             MOVE -1       TO CICS-CURPOS
'''''''         IF CURSOR-ROW > 0 AND
'''''''            CURSOR-COL > 0
'''''''           COMPUTE CICS-CURPOS = ( CURSOR-ROW - 1 ) * 80 +
'''''''                   CURSOR-COL - 1
'''''''         ELSE
'''''''               MOVE -1   TO CICS-CURPOS
'''''''         END-IF
              End If
              Testo = Testo & Space(17) & "EXEC CICS SEND MAP('" & Left(rsLpage!DPageName, 7) & "')" & vbCrLf
              Testo = Testo & Space(17) & "               MAPSET('" & wMapsetName & "')" & vbCrLf
            ' Testo = Testo & Space(15) & "               FROM  (WK-FROMAREA)" & vbCrLf
              Testo = Testo & Space(17) & "               ERASE CURSOR" & vbCrLf
              Testo = Testo & Space(17) & "               RESP  (RESPONSEX)" & vbCrLf
              Testo = Testo & Space(17) & "END-EXEC" & vbCrLf & vbCrLf
              rsLpage.MoveNext
            Wend
            Testo = Testo & Space(14) & "WHEN OTHER" & vbCrLf
            Testo = Testo & Space(17) & "MOVE 'SP' TO DC-PCB-STATUS-CODE" & vbCrLf
            Testo = Testo & Space(11) & "END-EVALUATE." & vbCrLf
          ElseIf rsLpage.RecordCount = 1 And Not rsLpage!DPageName = "" Then
''''            Testo = Testo & Space(6) & "*   ----------------" & vbCrLf
''''            Testo = Testo & Space(6) & "*   EXECUTE SEND MAP" & vbCrLf
''''            Testo = Testo & Space(6) & "*   ----------------" & vbCrLf & vbCrLf
            cmpcursor = GetCmpCursor(wFmtName, rsLpage!DPageName)
            If Not cmpcursor = "" And Not InStr(cmpcursor, "#") > 0 Then 'IRIS-DB esclude i literal
              Testo = Testo & Space(11) & "IF FLG-CURS-OK = 'N'" & vbCrLf
              Testo = Testo & Space(11) & "   MOVE -1 TO " & cmpcursor & "L" & vbCrLf
              Testo = Testo & Space(11) & "END-IF" & vbCrLf
            End If
            Testo = Testo & Space(11) & "EXEC CICS SEND MAP('" & Left(rsLpage!DPageName, 7) & "')" & vbCrLf
            Testo = Testo & Space(11) & "               MAPSET('" & wMapsetName & "')" & vbCrLf
            'Testo = Testo & Space(11) & "               FROM  (WK-FROMAREA)" & vbCrLf
            Testo = Testo & Space(11) & "               ERASE CURSOR" & vbCrLf
            Testo = Testo & Space(11) & "               RESP  (RESPONSEX)" & vbCrLf
            Testo = Testo & Space(11) & "END-EXEC." & vbCrLf & vbCrLf
            
          Else
''''            Testo = Testo & Space(6) & "*   ----------------" & vbCrLf
''''            Testo = Testo & Space(6) & "*   EXECUTE SEND MAP" & vbCrLf
''''            Testo = Testo & Space(6) & "*   ----------------" & vbCrLf & vbCrLf
            cmpcursor = GetCmpCursor(wFmtName)
            If Not cmpcursor = "" And Not InStr(cmpcursor, "#") > 0 Then 'IRIS-DB esclude i literal
              Testo = Testo & Space(11) & "IF FLG-CURS-OK = 'N'" & vbCrLf
              Testo = Testo & Space(11) & "   MOVE -1 TO " & cmpcursor & "L" & vbCrLf
              Testo = Testo & Space(11) & "END-IF" & vbCrLf
            End If
            Testo = Testo & Space(11) & "EXEC CICS SEND MAP('" & wMapName & "')" & vbCrLf
            Testo = Testo & Space(11) & "               MAPSET('" & wMapsetName & "')" & vbCrLf
            'Testo = Testo & Space(11) & "               FROM  (WK-FROMAREA)" & vbCrLf
            Testo = Testo & Space(11) & "               ERASE CURSOR" & vbCrLf
            Testo = Testo & Space(11) & "               RESP  (RESPONSEX)" & vbCrLf
            Testo = Testo & Space(11) & "END-EXEC." & vbCrLf & vbCrLf
          End If
          rsLpage.Close
                                                      
''''          Testo = Testo & Space(11) & "MOVE WS-MAPNAME TO LNKDC-MAPNAME" & vbCrLf
''''          Testo = Testo & Space(11) & "MOVE WK-INTOAREA TO LNKDC-INTOAREA" & vbCrLf
''''          Testo = Testo & Space(11) & "MOVE '" & wMapName & "' TO LNKDC-MAPSETNAME" & vbCrLf
          Testo = Testo & Space(11) & "MOVE SPACES TO DC-PCB-STATUS-CODE." & vbCrLf & vbCrLf 'IRIS-DB
          
          Testo = Testo & Space(11) & "IF RESPONSEX = DFHRESP(INVREQ)" & vbCrLf
          Testo = Testo & Space(11) & "   MOVE 'ZZ' TO DC-PCB-STATUS-CODE" & vbCrLf
          Testo = Testo & Space(11) & "END-IF." & vbCrLf & vbCrLf
          
          Testo = Testo & Space(11) & "IF RESPONSEX = DFHRESP(INVMPSZ)" & vbCrLf
          Testo = Testo & Space(11) & "   MOVE 'ZZ' TO DC-PCB-STATUS-CODE" & vbCrLf
          Testo = Testo & Space(11) & "END-IF." & vbCrLf
      End Select
   
  Testo = Testo & TextReceive & vbCrLf
    
  AggRtImsDC_CICS = Testo
  Exit Function
errFun:
  m_fun.WriteLog "ImsDC : AggRtImsDC_CICS - Function - Errore : " & err, "DR - GenRout"
End Function
Public Function isCiclicTag(tag As String) As String
  isCiclicTag = Right(tag, 4) = "(i)>"
End Function
Public Sub changeTag_DC(Rtb As RichTextBox, tag As String, text As String)
  Dim posizione As Long
  Dim crLfPosition As Long
  Dim indentedText As String, nofinalindent As Boolean
    
  If Right(text, 2) = vbCrLf Then
    nofinalindent = True
  End If
  If isCiclicTag(tag) Then
    If Len(text) Then text = text & vbCrLf & tag 'se text="" e' un clean!...
  End If
  While True
    posizione = Rtb.find(tag, posizione + Len(indentedText))
    Select Case posizione
      Case -1
        Exit Sub
      Case 0
        indentedText = text
      Case Else
        crLfPosition = InStrRev(Rtb.text, vbCrLf, posizione)
        If crLfPosition = 0 Then
          'Linea 1: non ho il fine linea...
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(posizione))
          If nofinalindent Then
            indentedText = Left(indentedText, Len(indentedText) - posizione)
          End If
        Else
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(posizione - crLfPosition - 1))
          If nofinalindent Then
            indentedText = Left(indentedText, Len(indentedText) - (posizione - crLfPosition - 1))
          End If
        End If
    End Select
    Rtb.SelStart = posizione
    Rtb.SelLength = Len(tag)
    Rtb.SelText = indentedText
  Wend
End Sub
Function IsCampoInDFLD(pFMT As String, pDevType As String, pCampo As String, pDPage As Integer) As Boolean
  Dim rscampi As New Recordset
  
  Set rscampi = m_fun.Open_Recordset( _
    "Select Nome from PsDLI_MFScmp where FMTName = '" & pFMT _
    & "' and Nome ='" & Replace(pCampo, "'", "''") & "' and Dpage =" & pDPage) 'and DevType ='" & pDevType & "'
  IsCampoInDFLD = Not rscampi.EOF
  rscampi.Close
End Function
Public Function formatMove(textmove As String) As String
Dim appotext As String, indent As Integer
Dim pos1, pos2 As Integer
appotext = LTrim(textmove)
indent = Len(textmove) - Len(appotext)
  If Len(textmove) > 64 Then
    formatMove = Left(textmove, InStr(textmove, " TO ")) & vbCrLf _
    & Space(indent + 3) & Right(textmove, Len(textmove) - InStr(textmove, " TO "))
  Else
    formatMove = textmove
  End If
  'IRIS-DB: long literal support; it should be better to create a function for it TODO
  If InStr(formatMove, "'") > 0 Then
    pos1 = InStr(formatMove, "'")
    pos2 = InStr(Mid(formatMove, pos1 + 1), "'")
    If (pos1 + pos2) > 64 Then
      formatMove = Left(formatMove, 66) & vbCrLf & "-" & Space(indent - 1 + 3) & "'" & Right(formatMove, 73)
    End If
  End If
End Function
Public Function formatInspect(textmove As String) As String
Dim appotext As String, indent As Integer
appotext = LTrim(textmove)
indent = Len(textmove) - Len(appotext)
  If Len(textmove) > 64 Then
    formatInspect = Left(textmove, InStr(textmove, " REPLACING ")) & vbCrLf _
    & Space(indent + 3) & Right(textmove, Len(textmove) - InStr(textmove, " REPLACING "))
  Else
    formatInspect = textmove
  End If
End Function
Public Sub GeneraCopyMove_DLI_CICS(pwOggetto As String, RTBR As RichTextBox, pFMT As String)
  Dim rscampi As New Recordset, rsPKField As New Recordset, rsMSGout As Recordset
  Dim m_fun As New MaFndC_Funzioni
  Dim TextField As String, wCampo As String, FileOutputname As String, FILLchar As String
  Dim nameCicsO As String, nameCicsI As String, nameStrutI As String, nameStrutO As String
  'AC 21/7/2006
  Dim ColOccurs As Collection, Occurs As Integer, i As Integer, j As Integer, prog As String
  Dim PFKField As String, cicsoccurs As String
  Dim ordlpage As Integer, numdpage As Integer, msginput As String, msgOutput As String
  
  Dim nameCics As String
  Dim colInfo As New Collection, namefileout As String, namefilein As String
  Dim indlpage As Integer, rsLpage As Recordset, rsOrdLPage As Recordset
  Dim ordlpageCICS As Integer, wcampoCICS As String, wcampoFMS As String
  Dim pos As Integer
  Dim ColOccursValue As Collection 'IRIS-DB
  Dim OccursValue As String 'IRIS-DB
  On Error GoTo errFun 'IRIS-DB
   
  colInfo.Add GbIdOggetto
  colInfo.Add pwOggetto
  colInfo.Add pFMT
  
  nameCics = getEnvironmentParam(RetrieveParameterValues("IMSDC-BMSMAP")(0), "mappe", colInfo)
  
  namefilein = getEnvironmentParam(RetrieveParameterValues("IMSDC-COPYMOVEI")(0), "mappe", colInfo)
  namefileout = getEnvironmentParam(RetrieveParameterValues("IMSDC-COPYMOVEO")(0), "mappe", colInfo)
  nameStrutI = getEnvironmentParam(RetrieveParameterValues("IMSDC-IMSSTRUCTUREI")(0), "mappe", colInfo)
  nameStrutO = getEnvironmentParam(RetrieveParameterValues("IMSDC-IMSSTRUCTUREO")(0), "mappe", colInfo)
  If m_ImsDll_DC.ImNomeDB = "LandG.mty" Then 'IRIS-DB: pezza per evitare di riconsegnare tutti i programmi a L&G; da rimuovere in seguito
    Set rscampi = Open_Recordset("select * from PsDLI_MFS where idoggetto = " & GbIdOggetto)
    If rscampi.RecordCount = 1 Then
      nameCics = pwOggetto & "R"
      namefilein = pwOggetto & "MI"
      namefileout = pwOggetto & "MO"
      nameStrutI = pwOggetto & "SI"
      nameStrutO = pwOggetto & "SO"
    End If
    rscampi.Close
  End If

  Set rscampi = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where FMTName = '" & pFMT & "' And DevType = 'INPUT'")
  If Not rscampi.EOF Then
    msginput = rscampi!MSGName
  End If
  rscampi.Close
  Set rscampi = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where FMTName = '" & pFMT & "' And DevType = 'OUTPUT'")
  If Not rscampi.EOF Then
    msgOutput = rscampi!MSGName
  End If
  rscampi.Close
  
  Set rsPKField = m_fun.Open_Recordset("Select PFKField from PsDLI_MFS where FMTName = '" & pFMT & "'  and PFKField is not null") 'and DevType = '(3270,2)'
  If Not rsPKField.EOF Then
    PFKField = rsPKField!PFKField
  End If
  rsPKField.Close
  
  Set rscampi = m_fun.Open_Recordset("Select Fill from PsDLI_mfsMSG where FMTName = '" & pFMT & "' and DevType = 'OUTPUT'")
  If Not rscampi.EOF Then
    If (Left(rscampi!Fill, 1) = "'" And Len(rscampi!Fill) >= 3) Or (Not Left(rscampi!Fill, 1) = "'" And Not Left(rscampi!Fill, 1) = "C") Then
      If rscampi!Fill = "NULL" Then
        FILLchar = "NULL"
      ElseIf Left(rscampi!Fill, 1) = "'" Then
        FILLchar = Mid(rscampi!Fill, InStr(1, rscampi!Fill, "'"), 3)
      Else
        FILLchar = "'" & rscampi!Fill & "'" ' in origine senza apici
      End If
    ElseIf rscampi!Fill = "" Then
      FILLchar = "NULL"
    ElseIf Left(rscampi!Fill, 1) = "C" Then
      FILLchar = Right(rscampi!Fill, Len(rscampi!Fill) - 1)
      If FILLchar = "'" Then
        FILLchar = "' '"
      End If
    End If
  End If
  If FILLchar = "'PT'" Then FILLchar = "NULL"
  If FILLchar = "''" Then FILLchar = "' '"
  rscampi.Close
  
  Set rscampi = m_fun.Open_Recordset("Select * from PsDLI_MFSmsgCmp where FMTName = '" & pFMT _
    & "' and MSGName ='" & msginput & "' order by LPage,Ordinale")
  'apriamo campi
  If Not rscampi.EOF Then
    rscampi.MoveLast        'MAX LPage
    numdpage = rscampi!lpage
    rscampi.MoveFirst
  End If
    
  TextField = " MOVE-FROM-" & nameCics & "." & vbCrLf
  If numdpage > 1 Then
    rscampi.Close
    
    TextField = TextField & "     EVALUATE IRIS-MODNAME" & vbCrLf
      Set rsLpage = m_fun.Open_Recordset("Select distinct DPageName from PsDli_MFSCmp where FMTName = '" & pFMT & "'")
      '& "' order by DPage")
      While Not rsLpage.EOF
        TextField = TextField & "       WHEN '" & Left(rsLpage!DPageName, 7) & "'" & vbCrLf
        Set rsOrdLPage = m_fun.Open_Recordset("Select Ordinale from PsMFS_Msg_LPage where MSG = '" & msginput _
        & "' And Sor = '" & rsLpage!DPageName & "'")
        ordlpage = rsOrdLPage!ordinale
        rsOrdLPage.Close
        
        nameCicsI = Left(rsLpage!DPageName, 7) & "I"
        Set rscampi = m_fun.Open_Recordset("Select * from PsDLI_MFSmsgCmp where FMTName = '" & pFMT _
          & "' and MSGName ='" & msginput & "' and LPage = " & ordlpage & "  order by Ordinale")
        While Not rscampi.EOF
          If IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Or rscampi!nome = PFKField Then
            wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
            If (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) And Not wCampo = PFKField And Not wCampo = "SYSMSG" Then
            ' gestione do-enddo
              If rscampi!Occurs > 1 Then
                ordlpage = rscampi!lpage
                Occurs = rscampi!Occurs
                Set ColOccurs = New Collection
                ColOccurs.Add wCampo
                rscampi.MoveNext
                Do While Not rscampi.EOF
                  If rscampi!Occurs = Occurs And rscampi!lpage = ordlpage Then
                    wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
                    If IsCampoInDFLD(pFMT, "(3270,2)", wCampo, rscampi!lpage) Then
                      ColOccurs.Add wCampo
                    End If
                    rscampi.MoveNext
                  Else
                    rscampi.MovePrevious
                    Exit Do
                  End If
                Loop
              ' uscita mi riposiziono sul precedente e faccio ciclo
                For i = 1 To Occurs
                  prog = Format(i, "00")
                  For j = 1 To ColOccurs.Count
                    
                    'AC 24/10/2007
'                    TextField = TextField & formatMove(Space(10) & "MOVE " & cicsoccurs & prog & "I OF " _
'                    & nameCicsI & " TO I-" & ColOccurs.Item(j) & ordlpage & " OF " & nameStrutI & "-AREA(" & i & ")" & vbCrLf)
                     cicsoccurs = ColOccurs.item(j) & IIf(ordlpage = 10, 0, ordlpage)
                     TextField = TextField & formatMove(Space(10) & "MOVE " & cicsoccurs & prog & "I OF " _
                    & nameCicsI & " TO I-" & ColOccurs.item(j) & "1 OF " & Replace(msginput, "@", "") & "-PG" & ordlpage & "(" & i & ")" & vbCrLf)
                  Next
                Next
              Else ' gestione solita (occurs = 1)
                'AC 24/10/2007
                wcampoFMS = wCampo
                wCampo = wCampo & IIf(rscampi!lpage = 10, 0, rscampi!lpage)
'                TextField = TextField & formatMove(Space(10) & "MOVE " & wCampo & "I OF " _
'              & nameCicsI & " TO I-" & wCampo & " OF " & nameStrutI & "-AREA" & vbCrLf)
                TextField = TextField & formatMove(Space(10) & "MOVE " & wCampo & "I OF " _
              & nameCicsI & " TO I-" & wcampoFMS & " OF " & Replace(msginput, "@", "") & "-PG" & ordlpage & vbCrLf)
              End If
              ElseIf wCampo = PFKField Then 'ECCEZIONE PFK
'            If numdpage > 1 Then
'              wCampo = wCampo & IIf(rscampi!Lpage = 10, 0, rscampi!Lpage)
'            End If
            TextField = TextField & formatMove(Space(10) & "MOVE WMFS-PFK" _
            & " TO I-" & wCampo & " OF " & Replace(msginput, "@", "") & "-PG" & ordlpage & vbCrLf)
           End If
          End If
        If Not rscampi.EOF Then
           rscampi.MoveNext
        End If
      Wend
      rscampi.Close
      rsLpage.MoveNext
    Wend 'lpage
    rsLpage.Close
    TextField = TextField & "     END-EVALUATE." & vbCrLf
  Else  ' single page
    '''controllare
     Set rsLpage = m_fun.Open_Recordset("Select DPageName from PsDli_MFSCmp where FMTName = '" & pFMT & "' and DPage = 1")
      If Not rsLpage.EOF Then
       If Not IsNull(rsLpage!DPageName) Then
        If Not rsLpage!DPageName = "" Then
          nameCicsO = Left(rsLpage!DPageName, 7) & "O"
          nameCicsI = Left(rsLpage!DPageName, 7) & "I"
        Else
          nameCicsO = nameCics & "O"
          nameCicsI = nameCics & "I"
        End If
       Else
        nameCicsO = nameCics & "O"
        nameCicsI = nameCics & "I"
       End If
      Else
       nameCicsO = nameCics & "O"
       nameCicsI = nameCics & "I"
      End If
    rsLpage.Close
    While Not rscampi.EOF
      If IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Or rscampi!nome = PFKField Then
        wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
        'IRIS-DB tolta gestione SYSMSG
        If (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) And Not wCampo = PFKField Then
        ' gestione do-enddo
          If rscampi!Occurs > 1 Then
            ordlpage = rscampi!lpage
            Occurs = rscampi!Occurs
            Set ColOccurs = New Collection
  '          If Len(wCampo) >= 8 Then
  '            wCampo = Left(wCampo, 7)
  '          End If
            If IsCampoInDFLD(pFMT, "(3270,2)", wCampo, rscampi!lpage) Then
              ColOccurs.Add wCampo
            End If
            rscampi.MoveNext
            Do While Not rscampi.EOF
              If rscampi!Occurs = Occurs And rscampi!lpage = ordlpage Then
                'wCampo = Trim(rsCampi!Nome)
                wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
  '              If Len(wCampo) >= 8 Then
  '                wCampo = Left(wCampo, 7)
  '              End If
                
                If IsCampoInDFLD(pFMT, "(3270,2)", wCampo, rscampi!lpage) Then
                  ColOccurs.Add wCampo
                End If
                rscampi.MoveNext
              Else
                rscampi.MovePrevious
                Exit Do
              End If
            Loop
            ' uscita mi riposiziono sul precedente e faccio ciclo
            For i = 1 To Occurs
              prog = Format(i, "00")
              For j = 1 To ColOccurs.Count
                ' AC 31/7/2006
                'If Len(ColOccurs.Item(j)) < 5 Then
                  cicsoccurs = ColOccurs.item(j) '& String(5 - Len(ColOccurs.Item(j)), "X")
                'Else
                  'cicsoccurs = Left(ColOccurs.Item(j), 5)
                'End If
                If numdpage > 1 Then
                  'Mid(cicsoccurs, 5, 1) = IIf(ordlpage = 10, 0, ordlpage)
                  cicsoccurs = cicsoccurs & IIf(ordlpage = 10, 0, ordlpage)
                End If
                'IRIS-DB for now it does not support the default value (see below few line)
                'TextField = TextField & Space(4) & "MOVE " & ColOccurs.Item(j) & prog & "I OF "
                TextField = TextField & formatMove(Space(6) & "MOVE " & cicsoccurs & prog & "I OF " _
                & nameCicsI & " TO I-" & ColOccurs.item(j) & "1 OF IRIS-WS-MESSAGE-AREA(" & i & ")" & vbCrLf)
              Next
            Next
          Else ' gestione solita (occurs = 1)
            If numdpage > 1 Then
              'If Len(wCampo) < 7 Then
                wCampo = wCampo & IIf(rscampi!lpage = 10, 0, rscampi!lpage)
              'Else
                'Mid(wCampo, Len(wCampo), 1) = IIf(rsCampi!lPage = 10, "0", CStr(rsCampi!lPage))
              'End If
            End If
            'IRIS-DB gestione value nella MID
            If Len(rscampi!Value) Then
              TextField = TextField & formatMove(Space(6) & "IF " & wCampo & "L OF " _
              & nameCicsI & " = ZERO" & vbCrLf)
              If rscampi!Value = "' '" Then ' forzatura per la gestione del low-value non supportato (Fedex)
                TextField = TextField & formatMove(Space(9) & "MOVE LOW-VALUE TO" & vbCrLf)
              Else
                TextField = TextField & formatMove(Space(9) & "MOVE " & rscampi!Value & " TO" & vbCrLf)
              End If
              TextField = TextField & formatMove(Space(9) & " I-" & wCampo & " OF IRIS-WS-MESSAGE-AREA(1:1)" & vbCrLf)
              TextField = TextField & formatMove(Space(6) & "ELSE" & vbCrLf)
              pos = 9
            Else
              pos = 6
            End If
            TextField = TextField & formatMove(Space(pos) & "MOVE " & wCampo & "I OF " _
            & nameCicsI & " TO I-" & wCampo & " OF IRIS-WS-MESSAGE-AREA" & vbCrLf)
            'IRIS-DB test per l'attributo MOD nel qual caso inizializza a space
            Set rsLpage = m_fun.Open_Recordset("Select Attributo from PsDli_MFSCmp where FMTName = '" & pFMT & "' and Nome = '" & wCampo & "'") 'IRIS-DB visto che lo fa anche in altri punti, per ora prendo solo il primo (non so come puntare quello giusto)
            If Not rsLpage.EOF Then
              If InStr(rsLpage!Attributo, "MOD") > 0 Then
                TextField = TextField & formatMove(Space(pos) & "INSPECT I-" & wCampo & " OF IRIS-WS-MESSAGE-AREA" & vbCrLf)
                TextField = TextField & formatMove(Space(pos) & " REPLACING ALL LOW-VALUE BY SPACE" & vbCrLf)
                If InStr(rscampi!Attributo, "YES") > 0 Then
                  TextField = TextField & formatMove(Space(pos) & "MOVE SPACE TO I-" & wCampo & "-ATTR" & vbCrLf)
                  TextField = TextField & formatMove(Space(pos) & "  OF IRIS-WS-MESSAGE-AREA" & vbCrLf)
                End If
              End If
            End If
            If pos = 9 Then
              TextField = TextField & formatMove(Space(6) & "END-IF" & vbCrLf)
            End If
          End If
        ElseIf wCampo = PFKField Then 'ECCEZIONE PFK
          If numdpage > 1 Then
              'If Len(wCampo) < 7 Then
                wCampo = wCampo & IIf(rscampi!lpage = 10, 0, rscampi!lpage)
              'Else
                'Mid(wCampo, Len(wCampo), 1) = IIf(rsCampi!lPage = 10, "0", CStr(rsCampi!lPage))
              'End If
          End If
          TextField = TextField & formatMove(Space(6) & "MOVE WMFS-PFK" _
          & " TO I-" & wCampo & vbCrLf) 'IRIS-DB
          'IRIS-DB & " TO I-" & wCampo & " OF " & nameStrutI & "-AREA" & vbCrLf)
        End If
      Else
      'IRIS-DB per ora lo gestisco solo qui, come visto in UPS
        If Not Left(rscampi!nome, 1) = "#" And rscampi!Fill <> "" Then
          TextField = TextField & formatMove(Space(6) & "MOVE " & Replace(rscampi!Fill, "C'", "'") & " TO I-" & Trim(rscampi!nome) & vbCrLf)
        End If
      End If
      If Not rscampi.EOF Then
         rscampi.MoveNext
      End If
    Wend
    rscampi.Close
    TextField = TextField & Space(6) & "." & vbCrLf
  End If '  gestione single page or multipage
  
  TextField = TextField & " EX-MOVE-FROM-" & nameCics & "." & vbCrLf
  TextField = TextField & Space(6) & "EXIT." & vbCrLf
      
  '****************  OUTPUT  **************************
   
'stefanopul
    'RTBR.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Input-Prj\CPY_BASE\CPY_BASE_MFS"
    RTBR.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Input-Prj\Template\imsdc\standard\fields\CPY_BASE_MFS"
        
    '<STRUTTURA-ASSOCIATA>.
    changeTag_DC RTBR, "<STRUTTURA-ASSOCIATA>.", TextField
    changeTag_DC RTBR, "<MFS-NAME>", pwOggetto
    RTBR.SaveFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Output-Prj\Cics\Cpy\" & namefilein, 1
    
               
    TextField = " MOVE-TO-" & nameCics & "." & vbCrLf
    TextField = TextField & "     EVALUATE IRIS-MODNAME" & vbCrLf 'IRIS-DB
    
    
    ' se diversi msgout basta che uno sia multipage per considerare anche gli altri multipage
    ' sposto a monte il controllo anzich� farlo per ogni singola msg
    ' in questo modo ridondanza ma nomi giusti per i campi della copy cics
'    Set rscampi = m_fun.Open_Recordset("Select * from PsDLI_MFSmsgCmp where FMTName = '" & pFMT _
'        & "' and IdOggetto =" & GbIdOggetto & " order by LPage,Ordinale")
'    If Not rscampi.EOF Then
'        rscampi.MoveLast
'        numdpage = rscampi!Lpage
'    End If
'    rscampi.Close
    
    Set rsMSGout = m_fun.Open_Recordset("Select MSGname from PsDLI_MFSmsg where FMTName = '" & pFMT _
      & "' and DevType = 'OUTPUT'")
      
    While Not rsMSGout.EOF
      TextField = TextField & "         WHEN '" & rsMSGout!MSGName & "'" & vbCrLf
      Set rscampi = m_fun.Open_Recordset("Select * from PsDLI_MFSmsgCmp where FMTName = '" & pFMT _
        & "' and MSGName ='" & rsMSGout!MSGName & "' order by LPage,Ordinale")
      ' AC 23/10/2007 rimesso numdpage effettivo della msgout -- verificare effetti
      If Not rscampi.EOF Then
        rscampi.MoveLast
        numdpage = rscampi!lpage
        rscampi.MoveFirst
      End If
      
      ' DA  formato MFS a BMS  -  campi funzione sostituiti da campi standard - controllo FILL
      If numdpage > 1 Then
        rscampi.Close
        TextField = TextField & "            EVALUATE WS-MAPNAME" & vbCrLf
        Set rsLpage = m_fun.Open_Recordset("Select distinct DPageName from PsDli_MFSCmp where FMTName = '" & pFMT & "'")
        '& "' order by DPage")
        While Not rsLpage.EOF
          TextField = TextField & "               WHEN '" & Left(rsLpage!DPageName, 7) & "'" & vbCrLf
          Set rsOrdLPage = m_fun.Open_Recordset("Select Ordinale from PsMFS_Msg_LPage where MSG = '" & rsMSGout!MSGName _
          & "' And Sor = '" & rsLpage!DPageName & "'")
          ordlpage = rsOrdLPage!ordinale
          rsOrdLPage.Close
          ' l'ordine della pagina che interessa � quello dentro la BMS
          Set rsOrdLPage = m_fun.Open_Recordset("Select DPage from PsDLI_MFSCmp where FMTName = '" & pFMT _
          & "' and DPageName = '" & rsLpage!DPageName & "'")
          ordlpageCICS = rsOrdLPage!DPage
          rsOrdLPage.Close
          nameCicsO = Left(rsLpage!DPageName, 7) & "O"
          nameCicsI = Left(rsLpage!DPageName, 7) & "I"
          'nameCicsI = nameCics & IIf(ordlpage = 10, "0", ordlpage) & "I"
          TextField = TextField & "                  MOVE LOW-VALUE TO " & nameCicsO & vbCrLf
          Set rscampi = m_fun.Open_Recordset("Select * from PsDLI_MFSmsgCmp where FMTName = '" & pFMT _
            & "' and MSGName ='" & rsMSGout!MSGName & "' and LPage = " & ordlpage & "  order by Ordinale")
            While Not rscampi.EOF
              If IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Or rscampi!nome = "SYSMSG" Or Not IIf(IsNull(rscampi!Funzione), "", rscampi!Funzione) = "" Then
                
                wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
                'IRIS-DB If (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) And Not wCampo = "SYSMSG" Then
                If (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) Then 'IRIS-DB
                  If rscampi!Occurs > 1 Then
                    Occurs = rscampi!Occurs
                    Set ColOccurs = New Collection
                    Set ColOccursValue = New Collection
                    ColOccurs.Add wCampo
                    OccursValue = rscampi!Value
                    ColOccursValue.Add OccursValue
                    rscampi.MoveNext
                    Do While Not rscampi.EOF
                      If rscampi!Occurs = Occurs And rscampi!lpage = ordlpage Then
                        wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
                        If IsCampoInDFLD(pFMT, "(3270,2)", wCampo, rscampi!lpage) Then
                          ColOccurs.Add wCampo
                          OccursValue = rscampi!Value
                          ColOccursValue.Add OccursValue
                        End If
                        rscampi.MoveNext
                      Else
                        rscampi.MovePrevious
                        Exit Do
                      End If
                    Loop
                    For i = 1 To Occurs
                      prog = Format(i, "00")
                      For j = 1 To ColOccurs.Count
                        cicsoccurs = ColOccurs.item(j) '& String(5 - Len(ColOccurs.Item(j)), "X")
                        cicsoccurs = cicsoccurs & IIf(ordlpageCICS = 10, 0, ordlpageCICS)
                        
'                        If Len(ColOccurs.Item(j)) + Len(Replace(rsMSGout!MSGName, "@", "")) < 11 Then
'                          TextField = TextField & Space(18) & "IF O-" & ColOccurs.Item(j) & ordlpage & " OF " & Replace(rsMSGout!MSGName, "@", "") & "-OUT(" & i & ") NOT EQUAL SPACE" & vbCrLf
'                          TextField = TextField & Space(21) & "AND LOW-VALUE THEN" & vbCrLf
'                        Else
'                          TextField = TextField & Space(18) & "IF O-" & ColOccurs.Item(j) & ordlpage & " OF " & Replace(rsMSGout!MSGName, "@", "") & "-OUT(" & i & ")" & vbCrLf _
'                          & Space(19) & "NOT EQUAL SPACE AND LOW-VALUE THEN" & vbCrLf
'                        End If
                        
                        ' da 21 a 18
'                        TextField = TextField & formatMove(Space(18) & "MOVE O-" & ColOccurs.Item(j) & ordlpage & " OF " _
'                        & Replace(rsMSGout!MSGName, "@", "") & "-OUT(" & i & ") TO " & cicsoccurs & prog & "O OF " & nameCicsO & vbCrLf)
                        If Not InStr(ColOccursValue.item(j), "'") > 0 Then
                          TextField = TextField & formatInspect(Space(18) & "INSPECT O-" & ColOccurs.item(j) & "1 OF " _
                          & Replace(rsMSGout!MSGName, "@", "") & "-OUT-PG" & ordlpage & "(" & i & ") REPLACING ALL LOW-VALUE BY SPACE" & vbCrLf)
                          TextField = TextField & formatMove(Space(18) & "MOVE O-" & ColOccurs.item(j) & "1 OF " _
                          & Replace(rsMSGout!MSGName, "@", "") & "-OUT-PG" & ordlpage & "(" & i & ") TO " & cicsoccurs & prog & "O OF " & nameCicsO & vbCrLf)
                        Else
                          TextField = TextField & formatMove(Space(18) & "MOVE " & ColOccursValue.item(j) & " TO " & cicsoccurs & prog & "O OF " & nameCicsO & vbCrLf)
                        End If
                        
'                        If Not FILLchar = "NULL" Then
'                          TextField = TextField & Space(18) & "ELSE" & vbCrLf _
'                          & Space(21) & "MOVE ALL " & FILLchar _
'                          & " TO " & cicsoccurs & prog & "O OF " & nameCicsO & vbCrLf
'                        End If
                        
'                        TextField = TextField & Space(18) & "END-IF" & vbCrLf
                      Next
                    Next
                  Else
                   'wCampo = wCampo & IIf(rscampi!lPage = 10, 0, rscampi!lPage)
                   wcampoCICS = wCampo & ordlpageCICS
                   wCampo = wCampo '& ordlpage
                   
'                   If Len(wCampo) + Len(Replace(rsMSGout!MSGName, "@", "")) < 15 Then
'                    TextField = TextField & Space(18) & "IF O-" & wCampo & " OF " & Replace(rsMSGout!MSGName, "@", "") & "-OUT NOT EQUAL SPACE" & vbCrLf
'                    TextField = TextField & Space(21) & "AND LOW-VALUE" & vbCrLf
'                   Else
'                    TextField = TextField & Space(18) & "IF O-" & wCampo & " OF " & Replace(rsMSGout!MSGName, "@", "") & "-OUT" & vbCrLf _
'                          & Space(19) & "NOT EQUAL SPACE AND LOW-VALUE THEN" & vbCrLf
'                   End If
                   
                   ' da 21 a 18
'                   TextField = TextField & formatMove(Space(18) & "MOVE O-" & wCampo & " OF " _
'                   & Replace(rsMSGout!MSGName, "@", "") & "-OUT TO " & wcampoCICS & "O OF " & nameCicsO & vbCrLf)
                   ' AC diversa gestione degli occurs quindi dell'OF - vedi struttura
                   If Not InStr(rscampi!Value, "'") > 0 Then
                     TextField = TextField & formatInspect(Space(18) & "INSPECT O-" & wCampo & " OF " _
                     & Replace(rsMSGout!MSGName, "@", "") & "-OUT-PG" & ordlpage & " REPLACING ALL LOW-VALUE BY SPACE" & vbCrLf)
                     TextField = TextField & formatMove(Space(18) & "MOVE O-" & wCampo & " OF " _
                     & Replace(rsMSGout!MSGName, "@", "") & "-OUT-PG" & ordlpage & " TO " & wcampoCICS & "O OF " & nameCicsO & vbCrLf)
                   Else
                     TextField = TextField & formatMove(Space(18) & "MOVE " & rscampi!Value & " TO " & wcampoCICS & "O OF " & nameCicsO & vbCrLf)
                   End If
                   
                   
                
'                   If Not FILLchar = "NULL" Then
'                     TextField = TextField & Space(18) & "ELSE" & vbCrLf _
'                     & Space(21) & "MOVE ALL " & FILLchar _
'                     & " TO " & wcampoCICS & "O OF " & nameCicsO & vbCrLf
'                   End If
'                   TextField = TextField & Space(18) & "END-IF" & vbCrLf
                   End If
              ElseIf rscampi!Funzione = "TIME" Then
                TextField = TextField & Space(18) & "MOVE WMFS-TIME"
                TextField = TextField & " TO " & wCampo & ordlpageCICS & "O OF " & nameCicsO & vbCrLf
              ElseIf InStr(1, rscampi!Funzione, "DATE") Then
                TextField = TextField & Space(18) & "MOVE WMFS-DATE"
                TextField = TextField & " TO " & wCampo & ordlpageCICS & "O OF " & nameCicsO & vbCrLf
              'ElseIf rscampi!Funzione = "SCA" Then
                'TextField = TextField & Space(18) & "MOVE WMFS-SCA"
                'TextField = TextField & " TO " & wCampo & IIf(rscampi!lPage = 10, 0, rscampi!lPage) & "O OF " & nameCicsO & vbCrLf
              ElseIf InStr(1, rscampi!Funzione, "LTNAME") Then
                'IRIS-DB TextField = TextField & Space(18) & "MOVE EIBTRMID"
                TextField = TextField & Space(18) & "MOVE DC-PCB-LTERM-NAME"
                TextField = TextField & " TO " & wCampo & ordlpageCICS & "O OF " & nameCicsO & vbCrLf
                'IRIS-DB
''''              ElseIf wCampo = "SYSMSG" Then
''''                TextField = TextField & Space(18) & "MOVE SPACES"
''''                TextField = TextField & " TO " & wCampo & ordlpageCICS & "O OF " & nameCicsO & vbCrLf
              End If
              
              End If
            If Not rscampi.EOF Then
              rscampi.MoveNext
            End If
          Wend
          rscampi.Close
          ' Ciclo ancora sui campi per move su attributo
          Set rscampi = m_fun.Open_Recordset("Select * from PsDLI_MFSmsgCmp where FMTName = '" & pFMT _
            & "' and MSGName ='" & rsMSGout!MSGName & "' and LPage = " & ordlpage & " and Attributo like '%YES%'  order by Ordinale")
          While Not rscampi.EOF
            wCampo = rscampi!nome
            'IRIS-DB: in realt� non � l'unica forma di definizione, potrebbero esserci: "(YES,nn)","NO,nn","(nn)"; tutti casi in cui andrebbe gestito l'attributo esteso; da rivedere TODO
            'If rscampi!Attributo = "YES" And (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) And IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Then
            If InStr(rscampi!Attributo, "YES") > 0 And (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) And IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Then
              If rscampi!Occurs > 1 Then
                Occurs = rscampi!Occurs
                Set ColOccurs = New Collection
                ColOccurs.Add wCampo
                rscampi.MoveNext
                Do While Not rscampi.EOF
                  If rscampi!Occurs = Occurs Then
                    wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
                    'If rscampi!Attributo = "YES" And IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Then
                    If InStr(rscampi!Attributo, "YES") > 0 And IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Then
                      ColOccurs.Add wCampo
                    End If
                    rscampi.MoveNext
                  Else
                    rscampi.MovePrevious
                    Exit Do
                  End If
                Loop
          ' uscita mi riposiziono sul precedente e faccio ciclo
                For i = 1 To Occurs
                  prog = Format(i, "00")
                  For j = 1 To ColOccurs.Count
                    cicsoccurs = ColOccurs.item(j) & IIf(ordlpageCICS = 10, 0, ordlpage) & prog
                    '**************************************
                    
                    TextField = TextField & "*                " & String(28 + Len(cicsoccurs), "-") & vbCrLf
                    TextField = TextField & "*                MOVE MAP-AREA - FIELD : " & cicsoccurs & vbCrLf
                    TextField = TextField & "*                " & String(28 + Len(cicsoccurs), "-") & vbCrLf & vbCrLf
                    
                    TextField = TextField & Space(18) & "MOVE 'N' TO FLG-CURS" & vbCrLf 'IRIS-DB
                    
                    If Len(ColOccurs.item(j)) + Len(Replace(rsMSGout!MSGName, "@", "")) < 11 Then 'ordlpage &
                      TextField = TextField & Space(18) & "MOVE O-" & ColOccurs.item(j) & "1-ATTR OF " _
                      & Replace(rsMSGout!MSGName, "@", "") & "-OUT(" & CStr(i) & ") TO HALF-WORD-X" & vbCrLf & vbCrLf 'IRIS-DB
                    Else   ' & ordlpage 24/10/07 anche qui tolto ordinale
                      TextField = TextField & Space(18) & "MOVE O-" & ColOccurs.item(j) & "1-ATTR OF " _
                      & Replace(rsMSGout!MSGName, "@", "") & "-OUT-PG" & ordlpage & "(" & CStr(i) & ")" & vbCrLf & Space(21) & "TO HALF-WORD-X" & vbCrLf & vbCrLf 'IRIS-DB
                    End If
                    TextField = TextField & Space(18) & "PERFORM SPLIT-HWRD THRU SPLIT-HWRD-EX" & vbCrLf & vbCrLf
                
                    TextField = TextField & Space(18) & "MOVE INIT-" & ColOccurs.item(j) & "1-ATTR TO OLD-ATTR-9" & vbCrLf 'IRIS-DB
                    TextField = TextField & Space(18) & "PERFORM LOGICAL-OR THRU LOGICAL-OR-EX" & vbCrLf & vbCrLf 'IRIS-SB
                    
                    TextField = TextField & Space(18) & "IF HALF-WORD-R GREATER 160" & vbCrLf
                    TextField = TextField & Space(18) & "    PERFORM  FND-ATTRIB THRU FND-ATTRIB-EX" & vbCrLf
                    ' inserire of qualcosa
                    TextField = TextField & Space(18) & "    MOVE CX-ATTRIB TO " & cicsoccurs & "A OF " _
                                                      & nameCicsI & vbCrLf
                    TextField = TextField & Space(18) & "END-IF" & vbCrLf & vbCrLf
                    '**************
                    TextField = TextField & Space(18) & "IF HALF-WORD-R LESS  0" & vbCrLf
                    TextField = TextField & Space(18) & "    PERFORM  NEG-ATTRIB THRU NEG-ATTRIB-EX" & vbCrLf
                    ' dopo of inserire valore corretto
                    TextField = TextField & Space(18) & "    MOVE CX-ATTRIB TO " & cicsoccurs & "A OF " _
                                                      & nameCicsI & vbCrLf
                    TextField = TextField & Space(18) & "END-IF" & vbCrLf & vbCrLf
                    '**************
                    TextField = TextField & Space(18) & "IF FLG-CURS = 'Y'" & vbCrLf
                    TextField = TextField & Space(18) & "    MOVE -1  TO " & cicsoccurs & "L OF " _
                                               & nameCicsI & vbCrLf
                    TextField = TextField & Space(18) & "    MOVE 'Y' TO FLG-CURS-OK" & vbCrLf
                    TextField = TextField & Space(18) & "ELSE" & vbCrLf
                    ' MFFDDHI deve essere generato
                    TextField = TextField & Space(18) & "    MOVE  0 TO " & cicsoccurs & "L OF " _
                                               & nameCicsI & vbCrLf
                    TextField = TextField & Space(18) & "END-IF" & vbCrLf & vbCrLf
                  Next
                Next
              Else ' gestione  (occurs = 1)
                wcampoFMS = wCampo
                wCampo = wCampo & IIf(ordlpage = 10, 0, ordlpage) 'AC 24/10/07
                TextField = TextField & "*                " & String(28 + Len(wCampo), "-") & vbCrLf
                TextField = TextField & "*                MOVE MAP-AREA - FIELD : " & wcampoFMS & vbCrLf
                TextField = TextField & "*                " & String(28 + Len(wCampo), "-") & vbCrLf & vbCrLf
                
                TextField = TextField & Space(18) & "MOVE 'N' TO FLG-CURS" & vbCrLf 'IRIS-DB
                
                TextField = TextField & Space(18) & "MOVE O-" & wcampoFMS & "-ATTR OF " _
                  & Replace(rsMSGout!MSGName, "@", "") & "-OUT-PG" & ordlpage & " TO HALF-WORD-X" & vbCrLf & vbCrLf 'IRIS-DB
                  
                TextField = TextField & Space(18) & "PERFORM SPLIT-HWRD THRU SPLIT-HWRD-EX" & vbCrLf & vbCrLf
                
                TextField = TextField & Space(18) & "MOVE INIT-" & wcampoFMS & "-ATTR TO OLD-ATTR-9" & vbCrLf 'IRIS-DB
                TextField = TextField & Space(18) & "PERFORM LOGICAL-OR THRU LOGICAL-OR-EX" & vbCrLf & vbCrLf 'IRIS-SB
                
                TextField = TextField & Space(18) & "IF HALF-WORD-R GREATER 160" & vbCrLf
                TextField = TextField & Space(18) & "    PERFORM  FND-ATTRIB THRU FND-ATTRIB-EX" & vbCrLf
                ' inserire of qualcosa
                TextField = TextField & Space(18) & "    MOVE CX-ATTRIB TO " & wCampo & "A OF " _
                                                  & nameCicsI & vbCrLf
                TextField = TextField & Space(18) & "END-IF" & vbCrLf & vbCrLf
                '**************
                TextField = TextField & Space(18) & "IF HALF-WORD-R LESS  0" & vbCrLf
                TextField = TextField & Space(18) & "    PERFORM  NEG-ATTRIB THRU NEG-ATTRIB-EX" & vbCrLf
                ' dopo of inserire valore corretto
                TextField = TextField & Space(18) & "    MOVE CX-ATTRIB TO " & wCampo & "A OF " _
                                                  & nameCicsI & vbCrLf
                TextField = TextField & Space(18) & "END-IF" & vbCrLf & vbCrLf
                                                
                '**************
                TextField = TextField & Space(18) & "IF FLG-CURS = 'Y'" & vbCrLf
                TextField = TextField & Space(18) & "    MOVE -1  TO " & wCampo & "L OF " _
                                           & nameCicsI & vbCrLf
                TextField = TextField & Space(18) & "    MOVE 'Y' TO FLG-CURS-OK" & vbCrLf
                TextField = TextField & Space(18) & "ELSE" & vbCrLf
              
                TextField = TextField & Space(18) & "    MOVE  0 TO " & wCampo & "L OF " _
                                           & nameCicsI & vbCrLf
                TextField = TextField & Space(18) & "END-IF" & vbCrLf & vbCrLf
              End If
            End If ' attributo YES  DFLD YES
            If Not rscampi.EOF Then
              rscampi.MoveNext
            End If
          Wend
          'rsCampi.Close
          rsLpage.MoveNext
        Wend
        TextField = TextField & Space(12) & "END-EVALUATE" & vbCrLf
      Else ' una sola pagina
      
        Set rsLpage = m_fun.Open_Recordset("Select DPageName from PsDli_MFSCmp where FMTName = '" & pFMT & "' and DPage = 1")
        If Not rsLpage.EOF Then
         If Not IsNull(rsLpage!DPageName) Then
          If Not rsLpage!DPageName = "" Then
            nameCicsO = Left(rsLpage!DPageName, 7) & "O"
            nameCicsI = Left(rsLpage!DPageName, 7) & "I"
          Else
            nameCicsO = nameCics & "O"
            nameCicsI = nameCics & "I"
          End If
         Else
          nameCicsO = nameCics & "O"
          nameCicsI = nameCics & "I"
         End If
        Else
         nameCicsO = nameCics & "O"
         nameCicsI = nameCics & "I"
        End If
        rsLpage.Close
        TextField = TextField & "            MOVE LOW-VALUE TO " & nameCicsO & vbCrLf
        While Not rscampi.EOF
          If IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Or rscampi!nome = "SYSMSG" Or Not IIf(IsNull(rscampi!Funzione), "", rscampi!Funzione) = "" Then
            wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
            'If Len(wCampo) >= 8 Then
               'wCampo = Left(wCampo, 7)
            'End If
            'IRIS-DB If (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) And Not wCampo = "SYSMSG" Then
            If (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) Then
              If rscampi!Occurs > 1 Then
                    
                ordlpage = rscampi!lpage
                Occurs = rscampi!Occurs
                Set ColOccurs = New Collection
                Set ColOccursValue = New Collection
                ColOccurs.Add wCampo
                OccursValue = rscampi!Value
                ColOccursValue.Add OccursValue
                rscampi.MoveNext
                Do While Not rscampi.EOF
                  If rscampi!Occurs = Occurs And rscampi!lpage = ordlpage Then
                    'wCampo = Trim(rsCampi!Nome)
                    wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
                    'If Len(wCampo) >= 8 Then
                      'wCampo = Left(wCampo, 7)
                    'End If
                    If IsCampoInDFLD(pFMT, "(3270,2)", wCampo, rscampi!lpage) Then
                      ColOccurs.Add wCampo
                      OccursValue = rscampi!Value
                      ColOccursValue.Add OccursValue
                    End If
                    rscampi.MoveNext
                  Else
                    rscampi.MovePrevious
                    Exit Do
                  End If
                Loop
                For i = 1 To Occurs
                  prog = Format(i, "00")
                  For j = 1 To ColOccurs.Count
                    'If Len(ColOccurs.Item(j)) < 5 Then
                      cicsoccurs = ColOccurs.item(j) '& String(5 - Len(ColOccurs.Item(j)), "X")
                    'Else
                      'cicsoccurs = Left(ColOccurs.Item(j), 5)
                    'End If
'                    If Len(ColOccurs.Item(j)) + Len(Replace(rsMSGout!MSGName, "@", "")) < 18 Then
'                      TextField = TextField & Space(12) & "IF O-" & ColOccurs.Item(j) & " OF " & Replace(rsMSGout!MSGName, "@", "") & "-OUT(" & i & ") NOT EQUAL SPACE" & vbCrLf
'                      TextField = TextField & Space(15) & "AND LOW-VALUE THEN" & vbCrLf
'                    Else
'                      TextField = TextField & Space(12) & "IF O-" & ColOccurs.Item(j) & " OF " & Replace(rsMSGout!MSGName, "@", "") & "-OUT(" & i & ")" & vbCrLf _
'                      & Space(13) & "NOT EQUAL SPACE AND LOW-VALUE THEN" & vbCrLf
'                    End If
                    ' da 15 a 12
                    If Not InStr(ColOccursValue.item(j), "'") > 0 Then
                      TextField = TextField & formatInspect(Space(12) & "INSPECT O-" & ColOccurs.item(j) & "1 OF " _
                      & Replace(rsMSGout!MSGName, "@", "") & "-OUT(" & i & ") REPLACING ALL LOW-VALUE BY SPACE" & vbCrLf)
                      TextField = TextField & formatMove(Space(12) & "MOVE O-" & ColOccurs.item(j) & "1 OF " _
                      & Replace(rsMSGout!MSGName, "@", "") & "-OUT(" & i & ") TO " & cicsoccurs & prog & "O OF " & nameCicsO & vbCrLf)
                    Else
                      TextField = TextField & formatMove(Space(12) & "MOVE " & ColOccursValue.item(j) & " TO " & cicsoccurs & prog & "O OF " & nameCicsO & vbCrLf)
                    End If
'                    If Not FILLchar = "NULL" Then
'                      TextField = TextField & Space(12) & "ELSE" & vbCrLf _
'                      & Space(15) & "MOVE ALL " & FILLchar _
'                      & " TO " & cicsoccurs & prog & "O OF " & nameCicsO & vbCrLf
'                    End If
'                    TextField = TextField & Space(12) & "END-IF" & vbCrLf
                  Next
                Next
              Else
               If numdpage > 1 Then
                 'If Len(wCampo) < 7 Then
                   wCampo = wCampo & IIf(rscampi!lpage = 10, 0, rscampi!lpage)
                 'Else
                   'Mid(wCampo, Len(wCampo), 1) = IIf(rsCampi!lPage = 10, "0", CStr(rsCampi!lPage))
                 'End If
               End If
'               If Len(wCampo) + Len(Replace(rsMSGout!MSGName, "@", "")) < 21 Then
'                TextField = TextField & Space(12) & "IF O-" & wCampo & " OF " & Replace(rsMSGout!MSGName, "@", "") & "-OUT NOT EQUAL SPACE" & vbCrLf
'                TextField = TextField & Space(15) & "AND LOW-VALUE THEN" & vbCrLf
'               Else
'                TextField = TextField & Space(12) & "IF O-" & wCampo & " OF " & Replace(rsMSGout!MSGName, "@", "") & "-OUT(" & i & ")" & vbCrLf _
'                          & Space(13) & "NOT EQUAL SPACE AND LOW-VALUE THEN" & vbCrLf
'               End If
               ' da 15 a 12
               If Not InStr(rscampi!Value, "'") > 0 Then
                 TextField = TextField & formatInspect(Space(12) & "INSPECT O-" & wCampo & " OF " _
                 & Replace(rsMSGout!MSGName, "@", "") & "-OUT REPLACING ALL LOW-VALUE BY SPACE" & vbCrLf)
                 TextField = TextField & formatMove(Space(12) & "MOVE O-" & wCampo & " OF " _
                 & Replace(rsMSGout!MSGName, "@", "") & "-OUT TO " & wCampo & "O OF " & nameCicsO & vbCrLf)
               Else
                 TextField = TextField & formatMove(Space(12) & "MOVE " & rscampi!Value & " TO " & wCampo & "O OF " & nameCicsO & vbCrLf)
               End If
                 
'               If Not FILLchar = "NULL" Then
'                 TextField = TextField & Space(12) & "ELSE" & vbCrLf _
'                 & Space(15) & "MOVE ALL " & FILLchar _
'                 & " TO " & wCampo & "O OF " & nameCicsO & vbCrLf
'               End If
'               TextField = TextField & Space(12) & "END-IF" & vbCrLf
              End If
            ElseIf rscampi!Funzione = "TIME" Then
               TextField = TextField & Space(12) & "MOVE WMFS-TIME"
               If numdpage > 1 Then
                TextField = TextField & " TO " & wCampo & IIf(rscampi!lpage = 10, 0, rscampi!lpage) & "O OF " & nameCicsO & vbCrLf
               Else
                TextField = TextField & " TO " & wCampo & "O OF " & nameCicsO & vbCrLf
               End If
            ElseIf InStr(1, rscampi!Funzione, "DATE") Then
               TextField = TextField & Space(12) & "MOVE WMFS-DATE"
               If numdpage > 1 Then
                TextField = TextField & " TO " & wCampo & IIf(rscampi!lpage = 10, 0, rscampi!lpage) & "O OF " & nameCicsO & vbCrLf
               Else
                TextField = TextField & " TO " & wCampo & "O OF " & nameCicsO & vbCrLf
               End If
            ElseIf rscampi!Funzione = "SCA" Then
               'TextField = TextField & Space(12) & "MOVE WMFS-SCA"
               'If numdpage > 1 Then
                'TextField = TextField & " TO " & wCampo & IIf(rscampi!lPage = 10, 0, rscampi!lPage) & "O OF " & nameCicsO & vbCrLf
               'Else
                'TextField = TextField & " TO " & wCampo & "O OF " & nameCicsO & vbCrLf
               'End If
            ElseIf InStr(1, rscampi!Funzione, "LTNAME") Then
               'IRIS-DB TextField = TextField & Space(12) & "MOVE EIBTRMID"
               TextField = TextField & Space(12) & "MOVE DC-PCB-LTERM-NAME"
               If numdpage > 1 Then
                TextField = TextField & " TO " & wCampo & IIf(rscampi!lpage = 10, 0, rscampi!lpage) & "O OF " & nameCicsO & vbCrLf
               Else
                TextField = TextField & " TO " & wCampo & "O OF " & nameCicsO & vbCrLf
               End If
               'IRIS-DB
'''''            ElseIf wCampo = "SYSMSG" Then
'''''               TextField = TextField & Space(12) & "MOVE SPACES"
'''''               If numdpage > 1 Then
'''''                TextField = TextField & " TO " & wCampo & IIf(rscampi!lpage = 10, 0, rscampi!lpage) & "O OF " & nameCicsO & vbCrLf
'''''               Else
'''''                TextField = TextField & " TO " & wCampo & "O OF " & nameCicsO & vbCrLf
'''''               End If
            End If
          End If
          If Not rscampi.EOF Then
            rscampi.MoveNext
          End If
        Wend
         ' Ciclo ancora sui campi per move su attributo
          Set rscampi = m_fun.Open_Recordset("Select * from PsDLI_MFSmsgCmp where FMTName = '" & pFMT _
            & "' and MSGName ='" & rsMSGout!MSGName & "' and Attributo like '%YES%'  order by Ordinale")
          While Not rscampi.EOF
            wCampo = rscampi!nome
            'IRIS-DB: in realt� non � l'unica forma di definizione, potrebbero esserci: "(YES,nn)","NO,nn","(nn)"; tutti casi in cui andrebbe gestito l'attributo esteso; da rivedere TODO
            'If rscampi!Attributo = "YES" And (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) And IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Then
            If InStr(rscampi!Attributo, "YES") > 0 And (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) And IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Then
              If rscampi!Occurs > 1 Then
                Occurs = rscampi!Occurs
                Set ColOccurs = New Collection
                ColOccurs.Add wCampo
                rscampi.MoveNext
                Do While Not rscampi.EOF
                  If rscampi!Occurs = Occurs Then
                    wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
                    'If rscampi!Attributo = "YES" And IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Then
                    If InStr(rscampi!Attributo, "YES") > 0 And IsCampoInDFLD(pFMT, "(3270,2)", rscampi!nome, rscampi!lpage) Then
                      ColOccurs.Add wCampo
                    End If
                    rscampi.MoveNext
                  Else
                    rscampi.MovePrevious
                    Exit Do
                  End If
                Loop
          ' uscita mi riposiziono sul precedente e faccio ciclo
                For i = 1 To Occurs
                  prog = Format(i, "00")
                  For j = 1 To ColOccurs.Count
                    cicsoccurs = ColOccurs.item(j) & prog
                    '**************************************
                    
                    TextField = TextField & "*   " & String(28 + Len(cicsoccurs), "-") & vbCrLf
                    TextField = TextField & "*   MOVE MAP-AREA - FIELD : " & cicsoccurs & vbCrLf
                    TextField = TextField & "*   " & String(28 + Len(cicsoccurs), "-") & vbCrLf & vbCrLf
                
                    TextField = TextField & Space(12) & "MOVE 'N' TO FLG-CURS" & vbCrLf 'IRIS-DB
                 
                    TextField = TextField & Space(12) & "MOVE O-" & ColOccurs.item(j) & "1-ATTR OF " _
                    & Replace(rsMSGout!MSGName, "@", "") & "-OUT(" & CStr(i) & ") TO HALF-WORD-X" & vbCrLf & vbCrLf 'IRIS-DB
                    TextField = TextField & Space(12) & "PERFORM SPLIT-HWRD THRU SPLIT-HWRD-EX" & vbCrLf & vbCrLf
                
                    TextField = TextField & Space(12) & "MOVE INIT-" & ColOccurs.item(j) & "1-ATTR TO OLD-ATTR-9" & vbCrLf 'IRIS-DB
                    TextField = TextField & Space(12) & "PERFORM LOGICAL-OR THRU LOGICAL-OR-EX" & vbCrLf & vbCrLf 'IRIS-SB
                    
                    TextField = TextField & Space(12) & "IF HALF-WORD-R GREATER 160" & vbCrLf
                    TextField = TextField & Space(12) & "    PERFORM  FND-ATTRIB THRU FND-ATTRIB-EX" & vbCrLf
                    ' inserire of qualcosa
                    TextField = TextField & Space(12) & "    MOVE CX-ATTRIB TO " & cicsoccurs & "A OF " _
                                                      & nameCicsI & vbCrLf
                    TextField = TextField & Space(12) & "END-IF" & vbCrLf & vbCrLf
                    '**************
                    TextField = TextField & Space(12) & "IF HALF-WORD-R LESS  0" & vbCrLf
                    TextField = TextField & Space(12) & "    PERFORM  NEG-ATTRIB THRU NEG-ATTRIB-EX" & vbCrLf
                    ' dopo of inserire valore corretto
                    TextField = TextField & Space(12) & "    MOVE CX-ATTRIB TO " & cicsoccurs & "A OF " _
                                                      & nameCicsI & vbCrLf
                    TextField = TextField & Space(12) & "END-IF" & vbCrLf & vbCrLf
                    '**************
                    TextField = TextField & Space(12) & "IF FLG-CURS = 'Y'" & vbCrLf
                    TextField = TextField & Space(12) & "    MOVE -1  TO " & cicsoccurs & "L OF " _
                                               & nameCicsI & vbCrLf
                    TextField = TextField & Space(12) & "    MOVE 'Y' TO FLG-CURS-OK" & vbCrLf
                    TextField = TextField & Space(12) & "ELSE" & vbCrLf
                    ' MFFDDHI deve essere generato
                    TextField = TextField & Space(12) & "    MOVE  0 TO " & cicsoccurs & "L OF " _
                                               & nameCicsI & vbCrLf
                    TextField = TextField & Space(12) & "END-IF" & vbCrLf & vbCrLf
                  Next
                Next
              Else ' gestione  (occurs = 1)
                TextField = TextField & "*   " & String(28 + Len(wCampo), "-") & vbCrLf
                TextField = TextField & "*   MOVE MAP-AREA - FIELD : " & wCampo & vbCrLf
                TextField = TextField & "*   " & String(28 + Len(wCampo), "-") & vbCrLf & vbCrLf
                
                TextField = TextField & Space(12) & "MOVE 'N' TO FLG-CURS" & vbCrLf 'IRIS-DB
                TextField = TextField & Space(12) & "MOVE O-" & wCampo & "-ATTR OF " _
                  & Replace(rsMSGout!MSGName, "@", "") & "-OUT TO HALF-WORD-X" & vbCrLf & vbCrLf 'IRIS-DB
                  
                TextField = TextField & Space(12) & "PERFORM SPLIT-HWRD THRU SPLIT-HWRD-EX" & vbCrLf & vbCrLf
                
                TextField = TextField & Space(12) & "MOVE INIT-" & wCampo & "-ATTR TO OLD-ATTR-9" & vbCrLf 'IRIS-DB
                TextField = TextField & Space(12) & "PERFORM LOGICAL-OR THRU LOGICAL-OR-EX" & vbCrLf & vbCrLf 'IRIS-SB
                
                TextField = TextField & Space(12) & "IF HALF-WORD-R GREATER 160" & vbCrLf
                TextField = TextField & Space(12) & "    PERFORM  FND-ATTRIB THRU FND-ATTRIB-EX" & vbCrLf
                ' inserire of qualcosa
                TextField = TextField & Space(12) & "    MOVE CX-ATTRIB TO " & wCampo & "A OF " _
                                                  & nameCicsI & vbCrLf
                TextField = TextField & Space(12) & "END-IF" & vbCrLf & vbCrLf
                '**************
                TextField = TextField & Space(12) & "IF HALF-WORD-R LESS  0" & vbCrLf
                TextField = TextField & Space(12) & "    PERFORM  NEG-ATTRIB THRU NEG-ATTRIB-EX" & vbCrLf
                ' dopo of inserire valore corretto
                TextField = TextField & Space(12) & "    MOVE CX-ATTRIB TO " & wCampo & "A OF " _
                                                  & nameCicsI & vbCrLf
                TextField = TextField & Space(12) & "END-IF" & vbCrLf & vbCrLf
                                                
                '**************
                TextField = TextField & Space(12) & "IF FLG-CURS = 'Y'" & vbCrLf
                TextField = TextField & Space(12) & "    MOVE -1  TO " & wCampo & "L OF " _
                                           & nameCicsI & vbCrLf
                TextField = TextField & Space(12) & "    MOVE 'Y' TO FLG-CURS-OK" & vbCrLf
                TextField = TextField & Space(12) & "ELSE" & vbCrLf
              
                TextField = TextField & Space(12) & "    MOVE  0 TO " & wCampo & "L OF " _
                                           & nameCicsI & vbCrLf
                TextField = TextField & Space(12) & "END-IF" & vbCrLf & vbCrLf
              End If
            End If ' attributo YES  DFLD YES
            If Not rscampi.EOF Then
              rscampi.MoveNext
            End If
          Wend
      End If
      rscampi.Close
      rsMSGout.MoveNext
     Wend
     rsMSGout.Close
     TextField = TextField & Space(5) & "END-EVALUATE." & vbCrLf
     TextField = TextField & " EX-MOVE-TO-" & nameCics & "." & vbCrLf
     TextField = TextField & Space(5) & "EXIT." & vbCrLf
  'stefanopul
    'RTBR.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Input-Prj\CPY_BASE\CPY_BASE_MFS"
    RTBR.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Input-Prj\Template\imsdc\standard\fields\CPY_BASE_MFS"
                   
    '<STRUTTURA-ASSOCIATA>.
    changeTag_DC RTBR, "<STRUTTURA-ASSOCIATA>.", TextField
    changeTag_DC RTBR, "<MFS-NAME>", pwOggetto
    RTBR.SaveFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Output-Prj\Cics\Cpy\" & namefileout, 1
  Exit Sub
errFun:
  m_fun.WriteLog "ImsDC : GeneraCopyMove_DLI_CICS - Function - Errore : " & err, "DR - GenRout"
'  Resume Next
End Sub

Public Sub GeneraCopyCicsJ_DLI_CICS(pwOggetto As String, RTBR As RichTextBox, pFMT As String)
  Dim Testo As String
  Dim rscampi As Recordset
  
  RTBR.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Input-Prj\Template\imsdc\standard\fields\CPY_BASE_MFS"
  changeTag_DC RTBR, "<MFS-NAME>", pwOggetto
  
  Testo = Testo & " 01  " & pFMT & "I." & vbCrLf
  'Testo = Testo & "     05  FILLER            PIC  X(0012)." & vbCrLf
  Set rscampi = m_fun.Open_Recordset("Select Nome, Lunghezza from PsDLI_MFScmp where FMTName = '" & pFMT & "' order by OrdinaleCmp")
  While Not rscampi.EOF
    Testo = Testo & "*    -------------------------------" & vbCrLf
    'Testo = Testo & "     05  " & rscampi!Nome & "L  PIC S9(0004) COMP." & vbCrLf
    'Testo = Testo & "     05  " & rscampi!Nome & "F  PIC  X(0001)." & vbCrLf
    'Testo = Testo & "     05  FILLER REDEFINES " & rscampi!Nome & "F." & vbCrLf
    'Testo = Testo & "         10  " & rscampi!Nome & "A PIC  X(0001)." & vbCrLf
    Testo = Testo & "     05  " & rscampi!nome & "I  PIC  X(" & Format(rscampi!Lunghezza, "0000") & ")." & vbCrLf
    rscampi.MoveNext
  Wend
  Testo = Testo & " 01  " & pFMT & "O REDEFINES " & pFMT & "I." & vbCrLf
  'Testo = Testo & "     05  FILLER            PIC  X(0012)." & vbCrLf
  rscampi.MoveFirst
  While Not rscampi.EOF
    Testo = Testo & "*    -------------------------------" & vbCrLf
    'Testo = Testo & "     05  FILLER            PIC  X(0003)." & vbCrLf
    Testo = Testo & "     05  " & rscampi!nome & "O  PIC  X(" & Format(rscampi!Lunghezza, "0000") & ")." & vbCrLf
    rscampi.MoveNext
  Wend
  rscampi.Close
  Testo = Testo & "*    -------------------------------" & vbCrLf
  changeTag_DC RTBR, "<STRUTTURA-ASSOCIATA>.", Testo
  RTBR.SaveFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Output-Prj\Cics\Cpy\" & pFMT, 1
End Sub
Public Sub GeneraCopyMappa_DLI_CICS(pwOggetto As String, RTBR As RichTextBox, pFMT As String)
  Dim rsArea As Recordset, RsTabelle As Recordset, rscampi As Recordset, rsMSGout As Recordset
  Dim FileName As String, wNomeSeg As String, wMApArea As String
  Dim Istruzione As String, wCampo As String, nomeFileOutput As String, Testo As String
  Dim env As Collection
  ''SQreview Dim DataMan As New MadmdC_Menu
  Dim LenCampo As Integer
  ' 21/7/2006
  Dim StrutOccurs As Boolean, Occurs As Integer, numdpage As Integer, ordinaleDo As Integer
  'SQ 8-09-06
  Dim lpage As Integer  'per "rottura" di codice: inserimento FILLER...
  Dim colInfo As New Collection, msginput As String, msgOutput As String
  Dim LenMax As Integer, lenMsg As Integer, nocampi As Boolean
  Dim IndentNoOccurs As String, IndentOccursGroup As String, IndentOccursField As String
  On Error GoTo errdb
  
  colInfo.Add GbIdOggetto
  colInfo.Add pwOggetto
  colInfo.Add pFMT
  
  Set rscampi = m_fun.Open_Recordset("Select MSGName from PsDLI_mfsMSG where FMTName = '" & pFMT & "' And DevType = 'INPUT'")
  If Not rscampi.EOF Then
    msginput = rscampi!MSGName
  End If
  rscampi.Close
  

   
  '***************   INPUT *******************
'stefanopul
  'RTBR.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Input-Prj\CPY_BASE\CPY_BASE_MFS"
  RTBR.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Input-Prj\Template\imsdc\standard\fields\CPY_BASE_MFS"
  'DmNomeDB
  changeTag_DC RTBR, "<MFS-NAME>", pwOggetto
  
  Set rscampi = m_fun.Open_Recordset( _
    "SELECT * FROM PsDLI_MFSmsgCmp where FMTName = '" & pFMT & _
    "' and MSGName ='" & msginput & "' order by LPage,Ordinale,ordinaleDo")
  
  If Not rscampi.EOF Then
    rscampi.MoveLast
    numdpage = rscampi!lpage
    rscampi.MoveFirst
  Else
    nocampi = True
  End If
  
  StrutOccurs = False
  'SQ
  lpage = -1 'init
 
  If numdpage > 1 Then
    Testo = Testo & "   05 AREA-" & Replace(msginput, "@", "") & Space(30 - Len(Replace(msginput, "@", ""))) & "PIC X(<MAXLENIN>)." & vbCrLf
  End If
  
  While Not rscampi.EOF
    '''''''''''''''''''''''''''''''''''''''''''''''''
    ' L'area � l'unione delle aree di ogni pagina...
    ' Meglio separare i livelli 01!!!
    ' SQ 8-09-06
    '''''''''''''''''''''''''''''''''''''''''''''''''
    If rscampi!lpage <> lpage Then
      lpage = rscampi!lpage
      If Not numdpage > 1 Then
        Testo = Testo & "   05 IN-LL " & Space(26) & "PIC S9(4) COMP." & vbCrLf 'IRIS-DB
        Testo = Testo & "   05 FILLER" & Space(26) & "PIC X(2)." & vbCrLf 'IRIS-DB
      Else
        Testo = Testo & "   05 " & Replace(msginput, "@", "") & "-PG" & lpage & " REDEFINES AREA-" & Replace(msginput, "@", "") & "." & vbCrLf
        'IRIS-DB Testo = Testo & "      10 FILLER" & Space(26) & "PIC X(4)." & vbCrLf
        Testo = Testo & "      10 IN-LL " & Space(26) & "PIC S9(4) COMP." & vbCrLf 'IRIS-DB
        Testo = Testo & "      10 FILLER" & Space(26) & "PIC X(2)." & vbCrLf 'IRIS-DB
      End If
      
      If lenMsg > LenMax Then
        LenMax = lenMsg
      End If
      lenMsg = 0
      lenMsg = lenMsg + 4
    End If
    
    'wCampo = Trim(rsCampi!Nome)
    wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
    If rscampi!ordinaleDo > 0 Then wCampo = wCampo & "1" 'IRIS-DB
'    If Len(wCampo) >= 8 Then
'      wCampo = Left(wCampo, 7)
'    End If
    'IRIS-DB tolta esclusione SYSMSG
    If (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) _
    And Not Left(rscampi!nome, 1) = "'" And Not InStr(1, rscampi!nome, "LTH=") > 0 _
    And Not Left(rscampi!nome, 1) = "#" Then
      If Not rscampi!Occurs > 1 Then
        StrutOccurs = False
        
        '**********  MULTIPAGINA CAMPO SEMPLICE ***************************
        ' prima: campi con a seguire numero pagina
        ' dopo: campi senza numero ma dentro redifines
        
        ' prima
'        If numdpage > 1 Then
'            wCampo = wCampo & IIf(rscampi!Lpage = 10, 0, rscampi!Lpage)
'        End If
        ' dopo
       
        If numdpage > 1 Then
            IndentNoOccurs = "   10"
        Else
            IndentNoOccurs = "05"
        End If
        
        If Not (Trim(rscampi!Attributo) = "" Or IsNull(rscampi!Attributo) Or Trim(rscampi!Attributo) = "NO") Then 'IRIS-DB per ora gestisce solo l'attributo esteso per l'allineamento dell'area, non lo converte al corrispondente valore CICS
           LenCampo = CInt(Trim(rscampi!Lunghezza))
           If InStr(rscampi!Attributo, ",1") > 0 Then 'IRIS-DB: it is currently supporting only one extended attribute, to review
            Testo = Testo & "   " & IndentNoOccurs & " I-" & wCampo & "-EATTR" & Space(24 - Len(wCampo)) & "PIC X(2)." & vbCrLf
            LenCampo = LenCampo - 2
           End If
           Testo = Testo & "   " & IndentNoOccurs & " I-" & wCampo & "-ATTR" & Space(25 - Len(wCampo)) & "PIC X(2)." & vbCrLf
           LenCampo = LenCampo - 2
        Else
            LenCampo = CInt(Trim(rscampi!Lunghezza))
        End If
        Testo = Testo & "   " & IndentNoOccurs & " I-" & wCampo & Space(30 - Len(wCampo)) & "PIC X(" & LenCampo & ")"
        'AC 29/10/07 ho introdotto la REDEFINES allora devo togliere 'VALUE'
        If Not numdpage > 1 Then
          If Not rscampi!Value = "" Then
            If Len(rscampi!Value) < 7 Then
              Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & "VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
            Else
              Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & vbCrLf & "        VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
            End If
          End If
        End If
        lenMsg = lenMsg + rscampi!Lunghezza
        Testo = Testo & "." & vbCrLf
      Else ' occurs > 1
         ' prima
'        If numdpage > 1 Then
'          wCampo = wCampo & IIf(rscampi!Lpage = 10, 0, rscampi!Lpage)
'        End If
         
        If numdpage > 1 Then
          IndentOccursField = "   15"
          IndentOccursGroup = "10"
        Else
          IndentOccursField = "   10"
          IndentOccursGroup = "05"
        End If
        If StrutOccurs And Occurs = rscampi!Occurs And ordinaleDo = rscampi!ordinaleDo Then
        'altri livelli 10  (15 per il multipagina)
          If Not (Trim(rscampi!Attributo) = "" Or IsNull(rscampi!Attributo) Or Trim(rscampi!Attributo) = "NO") Then 'IRIS-DB
            LenCampo = CInt(Trim(rscampi!Lunghezza))
            If InStr(rscampi!Attributo, ",1") > 0 Then 'IRIS-DB: it is currently supporting only one extended attribute, to review
              Testo = Testo & "      " & IndentOccursField & " I-" & wCampo & "-EATTR" & Space(21 - Len(wCampo)) & "PIC X(2)." & vbCrLf
              LenCampo = LenCampo - 2
            End If
            Testo = Testo & "      " & IndentOccursField & " I-" & wCampo & "-ATTR" & Space(22 - Len(wCampo)) & "PIC X(2)." & vbCrLf
            LenCampo = LenCampo - 2
          Else
            LenCampo = CInt(Trim(rscampi!Lunghezza))
          End If
          Testo = Testo & "      " & IndentOccursField & " I-" & wCampo & Space(27 - Len(wCampo)) & "PIC X(" & LenCampo & ")"
          ' AC multipage -> no value
          If Not numdpage > 1 Then
            If Not rscampi!Value = "" Then
              If Len(rscampi!Value) < 7 Then
                Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & "VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
              Else
                Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & vbCrLf & "        VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
              End If
            End If
          End If
          lenMsg = lenMsg + rscampi!Lunghezza
          Testo = Testo & "." & vbCrLf
        Else  ' livello 05 e primo livello 10 (livello 10 e primo livello 15 per il multipagina)
          Occurs = rscampi!Occurs
          ordinaleDo = rscampi!ordinaleDo
          StrutOccurs = True
          Testo = Testo & "   " & IndentOccursGroup & " I-" & wCampo & "-DO" & "  OCCURS " & Occurs & "." & vbCrLf
          If Not (Trim(rscampi!Attributo) = "" Or IsNull(rscampi!Attributo) Or Trim(rscampi!Attributo) = "NO") Then 'IRIS-DB
            LenCampo = CInt(Trim(rscampi!Lunghezza))
            If InStr(rscampi!Attributo, ",1") > 0 Then 'IRIS-DB: it is currently supporting only one extended attribute, to review
             Testo = Testo & "      " & IndentOccursField & " I-" & wCampo & "-EATTR" & Space(21 - Len(wCampo)) & "PIC X(2)." & vbCrLf
             LenCampo = LenCampo - 2
            End If
            Testo = Testo & "      " & IndentOccursField & " I-" & wCampo & "-ATTR" & Space(22 - Len(wCampo)) & "PIC X(2)." & vbCrLf
            LenCampo = LenCampo - 2
          Else
            LenCampo = CInt(Trim(rscampi!Lunghezza))
          End If
          Testo = Testo & "      " & IndentOccursField & "  I-" & wCampo & Space(27 - Len(wCampo)) & "PIC X(" & LenCampo & ")"
          If Not rscampi!Value = "" Then
            If Len(rscampi!Value) < 7 Then
              Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & "VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
            Else
              Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & vbCrLf & "        VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
            End If
          End If
          lenMsg = lenMsg + rscampi!Lunghezza
          Testo = Testo & "." & vbCrLf
        End If
      End If
      
    ElseIf Left(rscampi!nome, 1) = "'" Or InStr(1, rscampi!nome, "LTH=") > 0 Then
      If numdpage > 1 Then
          IndentNoOccurs = "   10"
      Else
          IndentNoOccurs = "05"
      End If
      Testo = Testo & "   " & IndentNoOccurs & " FILLER" & Space(26) & "PIC X(" & CStr(rscampi!Lunghezza) & ")"
      If Not rscampi!Value = "" Then
        If Len(rscampi!Value) < 7 Then
          Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & "VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
        Else
          Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & vbCrLf & "        VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
        End If
      End If
      Testo = Testo & "." & vbCrLf
    ElseIf Left(rscampi!nome, 1) = "#" Then
      If numdpage > 1 Then
          IndentNoOccurs = "   10"
      Else
          IndentNoOccurs = "05"
      End If
      Testo = Testo & "   " & IndentNoOccurs & " FILLER" & Space(26) & "PIC X(" & CStr(rscampi!Lunghezza) & ")"
      If Not rscampi!Value = "" Then
        If Len(rscampi!Value) < 7 Then
          Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & "VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
        Else
          Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & vbCrLf & "        VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
        End If
      End If
      Testo = Testo & "." & vbCrLf
    End If
    rscampi.MoveNext
  Wend
  rscampi.Close
  
  
  Testo = Replace(Testo, "<MAXLENIN>", LenMax)
  
  'nomeFileOutput = Left(pwOggetto, 2) & "SI" & IIf(Len(pwOggetto) < 6, Right(pwOggetto, Len(pwOggetto) - 2), Right(pwOggetto, 4))
  nomeFileOutput = getEnvironmentParam(RetrieveParameterValues("IMSDC-IMSSTRUCTUREI")(0), "mappe", colInfo)
  If m_ImsDll_DC.ImNomeDB = "LandG.mty" Then 'IRIS-DB: pezza per evitare di riconsegnare tutti i programmi a L&G; da rimuovere in seguito
    Set rscampi = Open_Recordset("select * from PsDLI_MFS where idoggetto = " & GbIdOggetto)
    If rscampi.RecordCount = 1 Then
      nomeFileOutput = pwOggetto & "SI"
    End If
    rscampi.Close
  End If
  'InserisciArea rsArea, RTBR
  If nocampi Then
    changeTag_DC RTBR, "<STRUTTURA-ASSOCIATA>.", vbCrLf
  Else
    changeTag_DC RTBR, "<STRUTTURA-ASSOCIATA>.", Testo
  End If
  RTBR.SaveFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Output-Prj\Cics\Cpy\" & nomeFileOutput, 1
  
  
  LenMax = 0
  lenMsg = 0

  '********************************************
  '***************   OUTPUT *******************
  StrutOccurs = False
'stefanopul
  'RTBR.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Input-Prj\CPY_BASE\CPY_BASE_MFS"
  RTBR.LoadFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Input-Prj\Template\imsdc\standard\fields\CPY_BASE_MFS"
  'DmNomeDB
  changeTag_DC RTBR, "<MFS-NAME>", pwOggetto
  
  'SQ
  
  Testo = ""
  LenMax = 0
  Testo = Testo & "   05 " & pFMT & "-OUT" & Space(31 - Len(pFMT)) & "PIC X(<MAXLEN>)." & vbCrLf
  Set rsMSGout = m_fun.Open_Recordset( _
    "Select MSGName from PsDLI_MFSmsg where FMTName = '" & pFMT & _
    "' and DevType ='OUTPUT'")
  While Not rsMSGout.EOF
    lenMsg = 0
    lpage = -1 'init
    Testo = Testo & "   05 " & Replace(rsMSGout!MSGName, "@", "") & "-OUT<pag1>" & " REDEFINES " & pFMT & "-OUT." & vbCrLf
    Set rscampi = m_fun.Open_Recordset( _
      "Select * from PsDLI_MFSmsgCmp where FMTName = '" & pFMT & _
      "' and IdOggetto =" & GbIdOggetto & " and MSGName ='" & rsMSGout!MSGName & "' order by Ordinale ")
    If Not rscampi.EOF Then
      rscampi.MoveLast
      numdpage = rscampi!lpage
    End If
    rscampi.Close
    Set rscampi = m_fun.Open_Recordset( _
      "Select * from PsDLI_MFSmsgCmp where FMTName = '" & pFMT & _
      "' and MSGName ='" & rsMSGout!MSGName & "' and trim(value) = ''order by Ordinale ") 'IRIS-DB: aggiunta esclusione dei campi con value = literal
'    If Not rscampi.EOF Then
'      rscampi.MoveLast
'      numdpage = rscampi!lPage
'      rscampi.MoveFirst
'    End If
    While Not rscampi.EOF
      '''''''''''''''''''''''''''''''''''''''''''''''''
      ' L'area � l'unione delle aree di ogni pagina...
      ' Meglio separare i livelli 01!!!
      ' SQ 8-09-06
      '''''''''''''''''''''''''''''''''''''''''''''''''
      If rscampi!lpage <> lpage Then
        lpage = rscampi!lpage
        If numdpage > 1 Then
          If lpage = 1 Then
            Testo = Replace(Testo, "<pag1>", "-PG1")
          Else
            Testo = Testo & "   05 " & Replace(rsMSGout!MSGName, "@", "") & "-OUT-PG" & lpage & " REDEFINES " & pFMT & "-OUT." & vbCrLf
          End If
        Else
          Testo = Replace(Testo, "<pag1>", "")
        End If
        Testo = Testo & "      10 FILLER" & Space(26) & "PIC X(4)." & vbCrLf
        If lenMsg > LenMax Then
          LenMax = lenMsg
        End If
        lenMsg = 0
        lenMsg = lenMsg + 4
      End If
      
      wCampo = Trim(IIf(rscampi!NomeBMS = "" Or IsNull(rscampi!NomeBMS), rscampi!nome, rscampi!NomeBMS))
      If rscampi!ordinaleDo > 0 Then wCampo = wCampo & "1" 'IRIS-DB
      
      'IRIS-DB tolta esclusione SYSMSG
      If (rscampi!Funzione = "" Or IsNull(rscampi!Funzione)) _
      And Not Left(rscampi!nome, 1) = "'" And Not InStr(1, rscampi!nome, "LTH=") > 0 _
      And Not Left(rscampi!nome, 1) = "#" Then
        If Not rscampi!Occurs > 1 Then
          StrutOccurs = False
          If numdpage > 1 Then
            'If Len(wCampo) < 7 Then
              wCampo = wCampo '& IIf(rscampi!Lpage = 10, 0, rscampi!Lpage) progressivo scomparso
                              ' dopo inserimento redefines
            'Else
              'Mid(wCampo, Len(wCampo), 1) = IIf(rsCampi!lPage = 10, "0", CStr(rsCampi!lPage))
            'End If
          End If
          'IRIS-DB ATTRB=NO � come mancante If Not (Trim(rscampi!Attributo) = "" Or IsNull(rscampi!Attributo)) Then
          If Not (Trim(rscampi!Attributo) = "" Or IsNull(rscampi!Attributo) Or Trim(rscampi!Attributo) = "NO") Then 'IRIS-DB
              LenCampo = CInt(Trim(rscampi!Lunghezza))
              If InStr(rscampi!Attributo, ",1") > 0 Then 'IRIS-DB: it is currently supporting only one extended attribute, to review
                Testo = Testo & "      10 O-" & wCampo & "-EATTR" & Space(24 - Len(wCampo)) & "PIC X(2)." & vbCrLf
                LenCampo = LenCampo - 2
              End If
              Testo = Testo & "      10 O-" & wCampo & "-ATTR" & Space(25 - Len(wCampo)) & "PIC X(2)." & vbCrLf
              LenCampo = LenCampo - 2
          Else
              LenCampo = CInt(Trim(rscampi!Lunghezza))
          End If
          Testo = Testo & "      10 O-" & wCampo & Space(30 - Len(wCampo)) & "PIC X(" & LenCampo & ")." & vbCrLf
          lenMsg = lenMsg + rscampi!Lunghezza
        Else
          If numdpage > 1 Then
              wCampo = wCampo '& IIf(rscampi!Lpage = 10, 0, rscampi!Lpage) tolto dopo redefines pagina
          End If
          If StrutOccurs And Occurs = rscampi!Occurs And ordinaleDo = rscampi!ordinaleDo Then 'altri livelli 15
            'IRIS-DB ATTRB=NO � come mancante If Not (Trim(rscampi!Attributo) = "" Or IsNull(rscampi!Attributo)) Then
            If Not (Trim(rscampi!Attributo) = "" Or IsNull(rscampi!Attributo) Or Trim(rscampi!Attributo) = "NO") Then 'IRIS-DB
              LenCampo = CInt(Trim(rscampi!Lunghezza))
              If InStr(rscampi!Attributo, ",1") > 0 Then 'IRIS-DB: it is currently supporting only one extended attribute, to review
                Testo = Testo & "         15 O-" & wCampo & "-EATTR" & Space(21 - Len(wCampo)) & "PIC X(2)." & vbCrLf
                LenCampo = LenCampo - 2
              End If
              Testo = Testo & "         15 O-" & wCampo & "-ATTR" & Space(22 - Len(wCampo)) & "PIC X(2)." & vbCrLf
              LenCampo = LenCampo - 2
            Else
              LenCampo = CInt(Trim(rscampi!Lunghezza))
            End If
            Testo = Testo & "         15 O-" & wCampo & Space(27 - Len(wCampo)) & "PIC X(" & LenCampo & ")." & vbCrLf
            lenMsg = lenMsg + (rscampi!Lunghezza) * Occurs
          Else  ' livello    10 e primo livello 15
            Occurs = rscampi!Occurs
            ordinaleDo = rscampi!ordinaleDo
            StrutOccurs = True
            Testo = Testo & "      10 O-" & wCampo & "-DO" & "  OCCURS " & Occurs & "." & vbCrLf
            If Not (Trim(rscampi!Attributo) = "" Or IsNull(rscampi!Attributo)) Then
              LenCampo = CInt(Trim(rscampi!Lunghezza))
              If InStr(rscampi!Attributo, ",1") > 0 Then 'IRIS-DB: it is currently supporting only one extended attribute, to review
                Testo = Testo & "         15 O-" & wCampo & "-EATTR" & Space(21 - Len(wCampo)) & "PIC X(2)." & vbCrLf
                 LenCampo = LenCampo - 2
              End If
              Testo = Testo & "         15 O-" & wCampo & "-ATTR" & Space(22 - Len(wCampo)) & "PIC X(2)." & vbCrLf
              LenCampo = LenCampo - 2
            Else
              LenCampo = CInt(Trim(rscampi!Lunghezza))
            End If
            Testo = Testo & "         15 O-" & wCampo & Space(27 - Len(wCampo)) & "PIC X(" & LenCampo & ")." & vbCrLf
            lenMsg = lenMsg + (rscampi!Lunghezza) * Occurs
          End If
        End If
      ElseIf Left(rscampi!nome, 1) = "'" Or InStr(1, rscampi!nome, "LTH=") > 0 Then
        Testo = Testo & "      10 FILLER" & Space(26) & "PIC X(" & CStr(rscampi!Lunghezza) & ")"
        If Not rscampi!Value = "" Then
          If Len(rscampi!Value) < 7 Then
            Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & "VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
          Else
            Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & vbCrLf & "        VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
          End If
          lenMsg = lenMsg + (rscampi!Lunghezza)
        End If
        Testo = Testo & "." & vbCrLf
      ElseIf Left(rscampi!nome, 1) = "#" Then
        Testo = Testo & "      10 FILLER" & Space(26) & "PIC X(" & CStr(rscampi!Lunghezza) & ")"
        If Not rscampi!Value = "" Then
          If Len(rscampi!Value) < 7 Then
            Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & "VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
          Else
            Testo = Testo & Space(5 - Len(rscampi!Lunghezza)) & vbCrLf & "        VALUE " & IIf(rscampi!Value = "' '", "SPACE", rscampi!Value)
          End If
        End If
        Testo = Testo & "." & vbCrLf
        lenMsg = lenMsg + (rscampi!Lunghezza)
      ElseIf rscampi!Funzione = "SCA" Then
        Testo = Testo & "      10 FILL-SPA" & Space(24) & "PIC X(" & CStr(rscampi!Lunghezza) & ")." & vbCrLf
        lenMsg = lenMsg + (rscampi!Lunghezza)
      End If
      rscampi.MoveNext
    Wend
    rscampi.Close
    If lenMsg > LenMax Then
      LenMax = lenMsg
    End If
    rsMSGout.MoveNext
 Wend
 '<STRUTTURA-ASSOCIATA>.
  'nomeFileOutput = Left(pwOggetto, 2) & "SO" & IIf(Len(pwOggetto) < 6, Right(pwOggetto, Len(pwOggetto) - 2), Right(pwOggetto, 4))
  nomeFileOutput = getEnvironmentParam(RetrieveParameterValues("IMSDC-IMSSTRUCTUREO")(0), "mappe", colInfo)
  If m_ImsDll_DC.ImNomeDB = "LandG.mty" Then 'IRIS-DB: pezza per evitare di riconsegnare tutti i programmi a L&G; da rimuovere in seguito
    Set rscampi = Open_Recordset("select * from PsDLI_MFS where idoggetto = " & GbIdOggetto)
    If rscampi.RecordCount = 1 Then
      nomeFileOutput = pwOggetto & "SO"
    End If
    rscampi.Close
  End If
  'InserisciArea rsArea, RTBR
  Testo = Replace(Testo, "<MAXLEN>", LenMax)
  changeTag_DC RTBR, "<STRUTTURA-ASSOCIATA>.", Testo
  RTBR.SaveFile m_ImsDll_DC.ImPathDB & "\" & Replace(m_ImsDll_DC.ImNomeDB, ".mty", "", , , vbTextCompare) & "\Output-Prj\Cics\Cpy\" & nomeFileOutput, 1
  
  Exit Sub
errdb:
  Select Case err.Number
    Case 75
      err.Raise 75, , "Template """ & m_fun.FnPathPrj & "\Input-Prj\Template\imsdc\standard\fields\CPY_BASE_MFS"" not found."
    Case 7676
      err.Raise err
    Case Else
      MsgBox err.Description
      'Resume
  End Select
End Sub
Function Indenta(Liv As Integer, Ind_Liv() As Indenta_Liv) As Long
  Dim x As Integer
  
  For x = 1 To UBound(Ind_Liv)
    If Ind_Liv(x).livello = Int(Liv) Then
       Indenta = Ind_Liv(x).Indet
       Exit For
    End If
  Next x
End Function
Sub InserisciArea(prsArea As Recordset, rtbFile As RichTextBox)
  Dim tbSegVirt As Recordset, TbOg As Recordset
  Dim Testo As String, Struttura As String
  Dim PosTesto As Long, PosInit As Long
  Dim nome As String, str As String, Str1 As String, Field As String, wStr1 As String
  Dim LenCmp As Integer
  Dim Decimali As String
  Dim LenRiga As Integer, LenR As Integer, k As Integer, x As Integer, i As Integer, MaxLen As Integer
  Dim Cont As String
  Dim bol As Boolean
  Dim Ind_Liv() As Indenta_Liv
  MaxLen = 0
  
  ReDim Ind_Liv(0)
  LenRiga = 72

    '''''''''''''''''''''''''''''''
    For k = 1 To prsArea.RecordCount
      
      If UBound(Ind_Liv) = 0 Then
         ReDim Preserve Ind_Liv(UBound(Ind_Liv) + 1)
         Ind_Liv(UBound(Ind_Liv)).livello = Int(prsArea!livello)
         Ind_Liv(UBound(Ind_Liv)).Indet = 0
      Else
        For x = 1 To UBound(Ind_Liv)
          If Ind_Liv(x).livello = Int(prsArea!livello) Then
            bol = True
            Exit For
          End If
        Next x
        If Not bol Then
           ReDim Preserve Ind_Liv(UBound(Ind_Liv) + 1)
           Ind_Liv(UBound(Ind_Liv)).livello = Int(prsArea!livello)
           Ind_Liv(UBound(Ind_Liv)).Indet = Ind_Liv(UBound(Ind_Liv) - 1).Indet + 3
        End If
      End If
      bol = False
      prsArea.MoveNext
    Next k
    prsArea.MoveFirst
    '**************************
    'salto il vero livello 01:
    prsArea.MoveNext
    
    While Not prsArea.EOF
      LenCmp = prsArea!Lunghezza
      If prsArea!Decimali <> 0 Then
        Decimali = "V9(" & prsArea!Decimali & ")"
      Else
        Decimali = ""
      End If
      If LenCmp = "0" Then
        If prsArea!Occurs = "1" Or prsArea!Occurs = "0" Then
          If Trim(prsArea!nomeRed) <> "" Then
             Field = Space(Indenta(Int(prsArea!livello), Ind_Liv)) & Format(prsArea!livello, "00") & " " & prsArea!nome & " REDEFINES " & prsArea!nomeRed & "."
          Else
             Field = Space(Indenta(Int(prsArea!livello), Ind_Liv)) & Format(prsArea!livello, "00") & " " & prsArea!nome & "."
          End If
        Else
          Field = Space(Indenta(Int(prsArea!livello), Ind_Liv)) & Format(prsArea!livello, "00") & " " & prsArea!nome & Space(3) & "OCCURS " & prsArea!Occurs & " TIMES."
        End If
      Else
        If prsArea!Segno = "S" Then
           If Len(prsArea!nome) > 30 Then
             Field = Space(Indenta(Int(prsArea!livello), Ind_Liv)) & Format(prsArea!livello, "00") & " " & prsArea!nome & "  PIC S" & prsArea!tipo & "(" & LenCmp & ")"
           Else
             Field = Space(Indenta(Int(prsArea!livello), Ind_Liv)) & Format(prsArea!livello, "00") & " " & prsArea!nome & Space(30 - Len(prsArea!nome)) & "  PIC S" & prsArea!tipo & "(" & LenCmp & ")"
           End If
        Else
           If Len(prsArea!nome) > 30 Then
             Field = Space(Indenta(Int(prsArea!livello), Ind_Liv)) & Format(prsArea!livello, "00") & " " & prsArea!nome & "  PIC " & prsArea!tipo & "(" & LenCmp & ")"
           Else
             Field = Space(Indenta(Int(prsArea!livello), Ind_Liv)) & Format(prsArea!livello, "00") & " " & prsArea!nome & Space(30 - Len(prsArea!nome)) & "  PIC " & prsArea!tipo & "(" & LenCmp & ")"
           End If
        End If
        
        If prsArea!Occurs = "0" Or prsArea!Occurs = "1" Then
           Field = Field & "."
        Else
          Field = Field & Space(5) & "OCCURS " & prsArea!Occurs & " TIMES." & vbCrLf
        End If
      End If
     
      If Len(Field) > LenRiga Then
        k = InStr(Field, "PIC")
        If k <> 0 Then
         str = RTrim(Mid(Field, 1, k - 1))
         Str1 = RTrim(Mid(Field, k, Len(Field)))
         
         Field = str & Space(LenRiga - Len(str)) & vbCrLf
         
         For x = 1 To Len(str)
          If Mid(str, 1, 1) <> " " Then
            Str1 = Space(x + 2) & Str1
            
            Field = Field & Str1 & Space(LenRiga - Len(Str1)) & vbCrLf
            Exit For
          Else
            str = Mid(str, 2)
          End If
         Next x
        End If
      Else
        Struttura = Struttura & Field & vbCrLf
      
      End If
      prsArea.MoveNext
    Wend
    changeTag_DC rtbFile, "<STRUTTURA-ASSOCIATA>.", Struttura
End Sub

Public Sub Crea_Routine_DLI_CICS(RTBR As RichTextBox, nroutine As String, nDb As String)
    Dim wstr As String
    Dim pos4 As Long
    Dim Percorso As String
    Dim PercorsoCics As String
    Dim vPos As Long
    Dim PrjName As String
    
  On Error GoTo templErr
  
  'carica Template routine
  ' per la routine generale
  Load_Template_Routine_DLI_CICS RTBR, SwSend
  
  On Error GoTo routErr
  'Modifica l'intestazione
  Insert_Intestazione_Routine_CICS RTBR, nroutine, nDb
  
  wstr = m_ImsDll_DC.ImNomeDB
  pos4 = InStr(wstr, ".")
  If pos4 > 0 Then wstr = Mid$(wstr, 1, pos4 - 1)
  
  PercorsoCics = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\cics"
  Percorso = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\cics\Cbl"
  If Dir(PercorsoCics, vbDirectory) = "" Then MkDir PercorsoCics
  If Dir(Percorso, vbDirectory) = "" Then MkDir Percorso
  
  RTBR.SaveFile Percorso & "\" & Trim(nroutine) & ".cbl", 1
  RTBR.LoadFile Percorso & "\" & Trim(nroutine) & ".cbl"
    
  Exit Sub
routErr:
   MsgBox "tmp: " & err.Description, vbInformation, "i-4.Migration: routine generator"
templErr:
    'messaggio gi� inviato dal chiamato...
End Sub
Public Sub Crea_Routine_DLI_CICSOneMap(RTBR As RichTextBox, nroutine As String, nDb As String, Optional swUnified As Boolean)
    Dim wstr As String
    Dim pos4 As Long
    Dim Percorso As String
    Dim PercorsoCics As String
    Dim vPos As Long
    On Error GoTo errFun 'IRIS-DB
    
    'carica Template routine per la singola mappa
    Load_Template_Routine_DLI_CICSOneMap RTBR, SwSend, swUnified
    
    'Modifica l'intestazione
    Insert_Intestazione_Routine_CICS RTBR, nroutine, nDb
    
    wstr = m_ImsDll_DC.ImNomeDB
    pos4 = InStr(wstr, ".")
    If pos4 > 0 Then wstr = Mid$(wstr, 1, pos4 - 1)
    
    PercorsoCics = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\cics"
    Percorso = m_ImsDll_DC.ImPathDef & "\" & wstr & "\output-prj\cics\Cbl"
    
    If Dir(PercorsoCics, vbDirectory) = "" Then MkDir PercorsoCics
    If Dir(Percorso, vbDirectory) = "" Then MkDir Percorso
        
    RTBR.SaveFile Percorso & "\" & Trim(nroutine) & ".cbl", 1
    RTBR.LoadFile Percorso & "\" & Trim(nroutine) & ".cbl"
    Exit Sub
errFun:
    m_fun.WriteLog "ImsDC : Crea_Routine_DLI_CICSOneMap - Function - Errore : " & err, "DR - GenRout"
End Sub

