VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form MadrdF_Verifica 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " DLI Instructions Managing"
   ClientHeight    =   8055
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   11535
   ForeColor       =   &H00C00000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   8055
   ScaleWidth      =   11535
   ShowInTaskbar   =   0   'False
   Begin MSFlexGridLib.MSFlexGrid filter_list 
      Height          =   915
      Left            =   60
      TabIndex        =   28
      Top             =   1500
      Visible         =   0   'False
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   1614
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      BackColor       =   16777152
      BackColorBkg    =   -2147483633
      FocusRect       =   2
      HighLight       =   2
   End
   Begin VB.Frame Frame2 
      Caption         =   "Object Details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   915
      Left            =   4980
      TabIndex        =   20
      Top             =   480
      Width           =   6495
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Valid:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   3360
         TabIndex        =   27
         Top             =   600
         Width           =   1035
      End
      Begin VB.Label lblValid 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4500
         TabIndex        =   26
         Top             =   600
         Width           =   1395
      End
      Begin VB.Label lblList 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4500
         TabIndex        =   24
         Top             =   360
         Width           =   1395
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Object in list:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   3360
         TabIndex        =   23
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label lblObj 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1500
         TabIndex        =   22
         Top             =   360
         Width           =   1515
      End
      Begin VB.Label Label3 
         Caption         =   "Object Selected:"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   360
         Width           =   1275
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Select Object for Analysis"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   915
      Left            =   60
      TabIndex        =   14
      Top             =   480
      Width           =   4875
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Refresh List"
         Height          =   225
         Left            =   3360
         TabIndex        =   29
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdDecode 
         Caption         =   "Check Decodes"
         Height          =   225
         Left            =   3360
         TabIndex        =   25
         Top             =   600
         Width           =   1335
      End
      Begin VB.CheckBox chkDetails 
         Caption         =   "Load Details"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   2100
         TabIndex        =   18
         Top             =   600
         Value           =   1  'Checked
         Width           =   1275
      End
      Begin VB.CheckBox chkPSB 
         Caption         =   "Load PSB"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   2100
         TabIndex        =   17
         Top             =   300
         Value           =   1  'Checked
         Width           =   1095
      End
      Begin VB.OptionButton optSel 
         Caption         =   "Only selected object"
         ForeColor       =   &H00C00000&
         Height          =   225
         Left            =   120
         TabIndex        =   16
         Top             =   300
         Width           =   1815
      End
      Begin VB.OptionButton optAll 
         Caption         =   "All DLI Programs"
         ForeColor       =   &H00C00000&
         Height          =   225
         Left            =   120
         TabIndex        =   15
         Top             =   600
         Width           =   1875
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "PSB Association"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1215
      Left            =   4980
      TabIndex        =   8
      Top             =   5100
      Width           =   4875
      Begin VB.CommandButton cmdAssocia 
         Caption         =   "Add PSB"
         Height          =   435
         Left            =   3060
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Associate PSB"
         Top             =   180
         Width           =   1575
      End
      Begin VB.TextBox txtPsb 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1020
         TabIndex        =   10
         Top             =   420
         Width           =   1875
      End
      Begin VB.CommandButton cmdDisass 
         Caption         =   "Delete PSB Relations"
         Height          =   435
         Left            =   3060
         TabIndex        =   9
         ToolTipText     =   "Unassociate PSB"
         Top             =   660
         Width           =   1575
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "PSB Name:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   540
         Width           =   825
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Correlated Programs"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1635
      Left            =   4980
      TabIndex        =   5
      Top             =   6360
      Width           =   4875
      Begin MSComctlLib.ListView lswCall 
         Height          =   1245
         Left            =   2460
         TabIndex        =   6
         Top             =   240
         Width           =   2235
         _ExtentX        =   3942
         _ExtentY        =   2196
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Program Call"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Relation"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView lswDep 
         Height          =   1245
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   2196
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "PSB Name"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Calling Level"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame lblPSb 
      Caption         =   "PSB in Project"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   2895
      Left            =   9930
      TabIndex        =   3
      Top             =   5100
      Width           =   1545
      Begin VB.CommandButton cmLoad 
         Caption         =   "Load List"
         Height          =   345
         Left            =   120
         TabIndex        =   19
         Top             =   2460
         Width           =   1335
      End
      Begin MSComctlLib.ListView lswPSBHD 
         Height          =   2115
         Left            =   120
         TabIndex        =   4
         Top             =   270
         Width           =   1275
         _ExtentX        =   2249
         _ExtentY        =   3731
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         Icons           =   "ImgLista"
         SmallIcons      =   "ImgLista"
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Percorso"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList ImgLista 
      Left            =   10200
      Top             =   8400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":059C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":0B38
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":10D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":122E
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":1388
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog ComD 
      Left            =   9420
      Top             =   8820
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   11
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Analyser"
            Object.ToolTipText     =   "Automatic istruction corrector"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Decodes"
            Object.ToolTipText     =   "Create Decodes"
            ImageIndex      =   10
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PARSER2"
            Object.ToolTipText     =   "Second Level Parser"
            ImageIndex      =   21
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Import"
            Object.ToolTipText     =   "Import"
            ImageIndex      =   23
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   5
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "IMPPSB"
                  Text            =   "Import PSB..."
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "IMPPCB"
                  Text            =   "Import PCB..."
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
               EndProperty
               BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXPPSB"
                  Text            =   "Export PSB..."
               EndProperty
               BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXPPCB"
                  Text            =   "Export PCB..."
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Filter"
            Object.ToolTipText     =   "Filter Configuration"
            ImageIndex      =   25
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "FilterApp"
            Object.ToolTipText     =   "Filter Execution"
            ImageIndex      =   24
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "XLS_report"
            Object.ToolTipText     =   "Encapsulation Report"
            ImageIndex      =   7
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "DLI Errors"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   3645
      Left            =   4980
      TabIndex        =   0
      Top             =   1440
      Width           =   6495
      Begin MSComctlLib.ListView lswMsg 
         Height          =   3225
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   6255
         _ExtentX        =   11033
         _ExtentY        =   5689
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         Icons           =   "ImgLista"
         SmallIcons      =   "ImgLista"
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Error"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Row"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   300
      Top             =   8280
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   25
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":1924
            Key             =   "Index"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":1AFE
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":2098
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":331C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":3770
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":3882
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":3A5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":3D80
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":41D4
            Key             =   "Chiudi_2"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":4330
            Key             =   "Lampo"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":448C
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":45E8
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":4A3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":4BA4
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":4FFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":5158
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":52C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":5854
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":5CA8
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":60FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":6550
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":6E2A
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":1EB64
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":1EE7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Verifica.frx":1EFD8
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lswOggetti 
      Height          =   6525
      Left            =   60
      TabIndex        =   13
      Top             =   1500
      Width           =   4845
      _ExtentX        =   8546
      _ExtentY        =   11509
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   16777152
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   8
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Name"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Key             =   "Togg"
         Text            =   "Type"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   2
         Text            =   "DLI"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Text            =   "Valid"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   4
         Text            =   "Instruction"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   5
         Text            =   "Decodes"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "PSB"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "ID"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "MadrdF_Verifica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public objectList As Collection
Dim w As Long
Dim t As Long
Dim Press As Long
Dim PressF As Long
Dim TipoSel As String

Private Sub cmd_filterapp_Click()
  FilterApp
End Sub

Sub FilterApp()
  Dim Prop() As String
  Dim Cont As Long
  Dim SQL As String, SQL2 As String
  Dim i As Long
  
  Static Order As String
  Order = lswOggetti.SortOrder
  lswOggetti.SortOrder = 0

  
  Screen.MousePointer = vbHourglass
  
  SQL = ""
  Cont = 0
  AppStr = ""
  AppStr2 = ""
  FinalS = ""
  
  If PressF = tbrUnpressed Then
    StrIdOggetti = ""
    For i = 1 To UBound(Oggetto_ID)
      StrIdOggetti = StrIdOggetti & Oggetto_ID(i) & ","
    Next i
    If StrIdOggetti <> "" Then
      StrIdOggetti = Mid(StrIdOggetti, 1, Len(StrIdOggetti) - 1)
      StrIdOggetti = "(" & StrIdOggetti & ")"
    End If
    PressF = tbrPressed
    Toolbar1.Buttons(10).Value = tbrPressed
    Toolbar1.Buttons(9).Visible = True
    ReDim Prop(0)
    'memorizza le variabili di query
    filter_list.Row = 1
    For i = 0 To filter_list.Cols - 1
      filter_list.Col = i
      ReDim Preserve Prop(UBound(Prop) + 1)
      Prop(UBound(Prop)) = filter_list.text
    Next i
      
      'crea la query
    For i = 1 To UBound(Prop)
      If Prop(i) <> "" Then
        
        Prop(i) = Sostituisci_ASTER_PERC(Prop(i))
        
        Select Case i
          Case 1
          
            If InStr(1, Prop(i), "%") <> 0 Then
              SQL = "Nome Like '" & Prop(i) & "'"
            Else
              SQL = "Nome = '" & Prop(i) & "'"
            End If
            Cont = Cont + 1
            
          Case 2
          
            If InStr(1, Prop(i), "%") <> 0 Then
              SQL = "Tipo Like '" & Prop(i) & "'"
            Else
              SQL = "Tipo = '" & Prop(i) & "'"
            End If
            Cont = Cont + 1
                      
          Case 3
          
            If InStr(1, Prop(i), "X") <> 0 Then
              SQL = "DLI = True '" & Prop(i) & "'"
            'Else
            '  SQL = "DLI = '" & Prop(i) & "'"
            End If
            Cont = Cont + 1
            
          Case 4
'            If InStr(1, Prop(i), "X") <> 0 Then
'              SQL2 = " valida = True"
'            'Else
'            '  SQL2 = ""
'            End If
'            Cont = Cont + 1
          
          Case 5
'
          Case 6
          
          Case 7
          
            If InStr(1, Prop(i), "%") <> 0 Then
              AppStr2 = " AND idoggetto = IdOggettoC AND  relazione = 'PSB'  AND NomeComponente Like  '" & Prop(i) & "' "
            Else
              AppStr2 = " AND idoggetto = IdOggettoC AND  relazione = 'PSB'  AND NomeComponente = '" & Prop(i) & "'"
            End If
                         
        End Select
        
        If Cont > 0 Then
          AppStr = " And " & SQL
        End If
        '''silvia
        If StrIdOggetti <> "" Then
          AppStr = AppStr & " and IdOggetto in " & StrIdOggetti
        End If
        '''
        FinalS = FinalS & AppStr & AppStr2
      End If
    Next i
    
    'visualizza la lista oggetti
    filter_list.Visible = False
    lswOggetti.Visible = True
    
    'disabilita il bottone di filtro
     
    ReDim Oggetti_TOTAL(0)
    
    If FinalS <> "" Then
      'Carica solo gli oggetti selezionati
      Carica_Array_Oggetti_Errori "FILTER"
      
      'carica la lista oggetti
      Carica_Lista_Oggetti lswOggetti, Oggetti_FILTER
    Else
      OptSelection TipoSel
    End If
  Else
    PressF = tbrUnpressed
    Toolbar1.Buttons(10).Value = tbrUnpressed
    If optAll.Value = True Then
       TipoSel = "TOTAL"
    End If
    If optSel.Value = True Then
      TipoSel = "SELECT"
    End If
    OptSelection TipoSel
  End If
  Carica_altre_Liste
  Screen.MousePointer = vbDefault
  lswOggetti.Sorted = Order
End Sub

Public Function Sostituisci_ASTER_PERC(stringa) As String
  Dim i As Long
  Dim ch As String
  Dim App As String
  
  For i = 1 To Len(stringa)
    ch = Mid(stringa, i, 1)
    
    If ch = "*" Then
      ch = "%"
    End If
    
    App = App & ch
  Next i
  
  Sostituisci_ASTER_PERC = App
End Function

Private Sub cmdfilter_Click()
  Filter
End Sub

Sub Filter()
  Dim i As Integer
'  If Press = 0 Then
'    Press = 1
    filter_list.Visible = True
    lswOggetti.Visible = False
    Toolbar1.Buttons(9).Visible = False
    Toolbar1.Buttons(10).Enabled = True
    Toolbar1.Buttons(10).Value = tbrUnpressed
    PressF = 0
    'cmd_filterapp.Enabled = True
    'cmdfilter.Picture = cmdfilter.DownPicture
'  Else
'    Press = 0
'    filter_list.Visible = False
'    lswOggetti.Visible = True
'    Toolbar1.Buttons(9).Image = 26
'    Toolbar1.Buttons(10).Enabled = False
'    'Toolbar1.Buttons(10).Value = tbrUnpressed
'    'cmd_filterapp.Enabled = False
'  End If
  
  StrIdOggetti = ""
  'imposta la riga di intestazione
  filter_list.Row = 0
  
  'imposta le colonne
  
  For i = 0 To lswOggetti.ColumnHeaders.Count - 1
    filter_list.Col = i
    filter_list.text = lswOggetti.ColumnHeaders(i + 1).text
    filter_list.ColWidth(i) = lswOggetti.ColumnHeaders(i + 1).Width
  Next i
  
End Sub

Private Sub cmdDecode_Click()
  Dim i As Long
  Dim r As Recordset, rs As Recordset
  
  Screen.MousePointer = vbHourglass
  For i = 1 To lswOggetti.ListItems.Count
    lswOggetti.ListItems(i).SubItems(5) = ""
    If lswOggetti.ListItems(i).SubItems(3) = "X" Then
      'SQ 15-04-08: AGGIUNTO TO_MIGRATE
      If lswOggetti.ListItems(i).SubItems(1) = "CPY" Or lswOggetti.ListItems(i).SubItems(1) = "INC" Then
        Set r = m_fun.Open_Recordset("select * from psdli_istruzioni where " & _
                                     "idoggetto=" & lswOggetti.ListItems(i).tag & " and idpgm > 0 AND to_migrate")
      Else
        Set r = m_fun.Open_Recordset("SELECT * FROM psdli_istruzioni WHERE " & _
                                     "idPgm=" & lswOggetti.ListItems(i).tag & " And to_migrate")
      End If
      If r.RecordCount Then
        Set rs = m_fun.Open_Recordset("select distinct riga from psdli_istrcodifica where " & _
                                      "idpgm=" & lswOggetti.ListItems(i).tag)
        If rs.RecordCount = r.RecordCount Then
          lswOggetti.ListItems(i).SubItems(5) = "X"
        End If
      End If
    End If
  Next i
  Screen.MousePointer = vbDefault
End Sub

Private Sub cmdRefresh_Click()
  Static Order As String, key As Integer
  
  Order = lswOggetti.SortOrder
  key = lswOggetti.SortKey
  lswOggetti.SortOrder = 0
  lswOggetti_ColumnClick lswOggetti.ColumnHeaders(1)
  If optAll.Value Then
    optAll_Click
  End If
  If optSel.Value Then
    optSel_Click
  End If
  lswOggetti.SortOrder = Order
  lswOggetti.SortKey = key
End Sub

Private Sub cmLoad_Click()
  Screen.MousePointer = vbHourglass
  Elimina_Flickering Me.hwnd
  
  ReDim Oggetti_TOTAL(0)
  ReDim Oggetti_SELECT(0)
  DoEvents

  'Carica Gli array in memoria degli oggetti e le loro tipologie di errore
  '1) Carica la lista Totale degli oggetti con errore
  'AC SELECT al posto di TOTAL
  Carica_Array_Oggetti_Errori "SELECT"
 
  'Carica la lista con tutti gli oggetti aventi una tipologia di errore
'  If UBound(Oggetti_SELECT) Then
    'Carica solo gli oggetti selezionati
    ' DEFAULT
    optSel.Value = True
'  Else
'    'Carica tutti gli oggetti
'    optAll.Value = True
'  End If

  'Carica le segnalazioni del 1� Elemento
  If lswOggetti.ListItems.Count Then
    Carica_Segnalazioni_Da_IdOggetto lswMsg, Val(lswOggetti.ListItems(1).tag)
    Dli2Rdbms.drIdOggetto = Val(lswOggetti.ListItems(1).tag)
    lswOggetti.ListItems(1).Selected = True
  End If
  
  'Carica solo i PSB Interni al Project
  Carica_Lista_PSB lswPSBHD, "INTERNAL", lblPSb
  
  ''lblPSb.Caption = "PSB File found in project N� " & lswPSBHD.ListItems.count
  
  'Carica di default le dipendenze del primo elemento
  If lswOggetti.ListItems.Count Then
    Carica_Dipendenze_PROGRAMMI lswOggetti.ListItems(1).text, Val(lswOggetti.ListItems(1).tag), lswDep, lswCall
  End If
  
  Terminate_Flickering
  Screen.MousePointer = vbDefault
End Sub

Private Sub filter_list_KeyPress(KeyAscii As Integer)
  With filter_list
    Select Case KeyAscii
      Case 8
        If Not .text = "" Then
          .text = Left(.text, Len(.text) - 1)
        End If
      Case 9 ' Tab
        If .Col + 1 = .Cols Then
          .Col = 0
          If .Row + 1 = .Rows Then
            .Row = 0
          Else
            .Row = .Row + 1
          End If
        Else
          .Col = .Col + 1
        End If
      Case Else
        .text = .text & chr(KeyAscii)
    End Select
  End With
End Sub

Private Sub filter_list_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  Dim XN As Long, yN As Long
  
  Dim i As Long
  Dim Filter_List_Column As Long
  filter_list.BackColor = lswOggetti.BackColor
  filter_list.CellBackColor = lswOggetti.BackColor
  filter_list.Refresh
  'assegna alla globale la colonna selezionata
  Filter_List_Column = filter_list.ColSel
  'txtFilter = ""
  'txtFilter.Visible = True
  'txtFilter.SetFocus
  
'  'prende le coordinate
  For i = 0 To filter_list.Cols - 1
    If i < Filter_List_Column Then
      filter_list.Col = i
      filter_list.CellBackColor = lswOggetti.BackColor
      filter_list.Row = 1
      XN = XN + filter_list.CellWidth
    Else
      Exit For
    End If
  Next i
  'filter_list.TextMatrix(1,i)
  filter_list.Row = 0
  filter_list.Col = 0
  yN = filter_list.CellHeight
  
  filter_list.Row = 1
  filter_list.Col = Filter_List_Column
  filter_list.CellBackColor = vbWhite
  filter_list.Refresh
End Sub


Private Sub Form_Activate()
  'Controlla se la DLL Parser � presente
  If Not Verifica_Esistenza_DLLParser Then
    Unload Me
  End If
End Sub

Private Sub cmdDisass_Click()
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From PsRel_Obj Where " & _
                               "IdOggettoC = " & Dli2Rdbms.drIdOggetto & " And Relazione = 'PSB'")
  While Not r.EOF
    r.Delete adAffectCurrent
    r.Update
    
    'Devo scrivere L'errore
    m_fun.WriteLog "VERIFICA - cmdDisass_Click", "DR - Verifica"
    ' Stop 'Chi lo scrive (il parsing?)
    
    r.MoveNext
  Wend
  r.Close
End Sub

Private Sub Form_Load()
  Screen.MousePointer = vbHourglass
  Elimina_Flickering Me.hwnd
  
  ReDim Oggetti_SELECT(0)
  ReDim Oggetti_TOTAL(0)
  ReDim Oggetti_FILTER(0)
  ReDim Oggetto_ID(0)
  
  'Carica solo gli oggetti selezionati
  Carica_Array_Oggetti_Errori "SELECT"

  'Carica la lista con tutti gli oggetti aventi una tipologia di errore
'  If UBound(Oggetti_SELECT) Then
    'Carica solo gli oggetti selezionati
    ' DEFAULT
    optSel.Value = True
'  Else
'    'Carica tutti gli oggetti
'    optAll.Value = True
'  End If
  'SILVIA 11-04-2008
  Carica_altre_Liste
    
  Terminate_Flickering
  Screen.MousePointer = vbDefault
End Sub

'SILVIA 11-04-2008
Sub Carica_altre_Liste()
  'Carica le segnalazioni del 1� Elemento
  If lswOggetti.ListItems.Count Then
    Carica_Segnalazioni_Da_IdOggetto lswMsg, Val(lswOggetti.ListItems(1).tag)
    Dli2Rdbms.drIdOggetto = Val(lswOggetti.ListItems(1).tag)
    
    'Carica le dipendenze
    Carica_Dipendenze_PROGRAMMI lswOggetti.ListItems(1).text, Dli2Rdbms.drIdOggetto, lswDep, lswCall
    lblObj.Caption = lswOggetti.ListItems(1).tag & " - " & lswOggetti.ListItems(1).text
  End If
   
  'MF 01/08/07 Mancava AddActiveWindows
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
  'Carica le dipendenze
  If lswOggetti.ListItems.Count Then
    Carica_Dipendenze_PROGRAMMILoad lswOggetti.ListItems(1).text, lswOggetti.ListItems(1).tag, lswDep, lswCall
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  'Resize delle ListView
  lswOggetti.ColumnHeaders(1).Width = (lswOggetti.Width - 270) / 10 * 2
  lswOggetti.ColumnHeaders(2).Width = (lswOggetti.Width - 270) / 10
  lswOggetti.ColumnHeaders(3).Width = (lswOggetti.Width - 270) / 10
  lswOggetti.ColumnHeaders(4).Width = (lswOggetti.Width - 270) / 10
  lswOggetti.ColumnHeaders(5).Width = (lswOggetti.Width - 270) / 10 * 2
  lswOggetti.ColumnHeaders(6).Width = (lswOggetti.Width - 270) / 10
  lswOggetti.ColumnHeaders(7).Width = (lswOggetti.Width - 270) / 10 * 2
  
  lswMsg.ColumnHeaders(1).Width = (lswMsg.Width - 270) / 8 * 7
  lswMsg.ColumnHeaders(2).Width = (lswMsg.Width - 270) / 8

  lswPSBHD.ColumnHeaders(1).Width = lswPSBHD.Width - 270

  lswDep.ColumnHeaders(1).Width = (lswDep.Width - 270) / 2
  lswDep.ColumnHeaders(2).Width = (lswDep.Width - 270) / 2
  
  lswCall.ColumnHeaders(2).Width = (lswCall.Width - 270) / 2
  lswCall.ColumnHeaders(2).Width = (lswCall.Width - 270) / 2
End Sub

Private Sub lswCall_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswCall.SortOrder
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswCall.SortOrder = Order
  lswCall.SortKey = key - 1
  lswCall.Sorted = True
End Sub

Private Sub lswDep_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswDep.SortOrder
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswDep.SortOrder = Order
  lswDep.SortKey = key - 1
  lswDep.Sorted = True
End Sub

Private Sub LswDep_ItemClick(ByVal item As MSComctlLib.ListItem)
  If InStr(1, UCase(item.text), "NO PSB") = 0 Then
    txtPsb.text = item.text
  End If
End Sub

Private Sub lswMsg_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswMsg.SortOrder
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswMsg.SortOrder = Order
  lswMsg.SortKey = key - 1
  lswMsg.Sorted = True
End Sub

Private Sub lswOggetti_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswOggetti.SortOrder
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswOggetti.SortOrder = Order
  lswOggetti.SortKey = key - 1
  lswOggetti.Sorted = True
End Sub

Private Sub lswOggetti_DblClick()
  If lswOggetti.ListItems.Count Then
    Load MadrdF_Corrector
    SetParent MadrdF_Corrector.hwnd, Dli2Rdbms.drParent
    MadrdF_Corrector.Show
    MadrdF_Corrector.Move 0, 0, Dli2Rdbms.drWidth, Dli2Rdbms.drHeight
  End If
End Sub

Private Sub lswOggetti_ItemClick(ByVal item As MSComctlLib.ListItem)
  If lswOggetti.ListItems.Count Then
    Screen.MousePointer = vbHourglass
    
    'Carica La lista delle segnalazioni
    Carica_Segnalazioni_Da_IdOggetto lswMsg, Val(item.tag)
    Dli2Rdbms.drIdOggetto = Val(item.tag)
    
    'Carica le dipendenze
    Carica_Dipendenze_PROGRAMMI item.text, Dli2Rdbms.drIdOggetto, lswDep, lswCall
    lblObj.Caption = item.tag & " - " & item.text
    
    Screen.MousePointer = vbDefault
  End If
End Sub

Private Sub lswPSBHD_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswPSBHD.SortOrder
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswPSBHD.SortOrder = Order
  lswPSBHD.SortKey = key - 1
  lswPSBHD.Sorted = True
End Sub

Private Sub LswPSBHD_ItemClick(ByVal item As MSComctlLib.ListItem)
  txtPsb.text = item.text
  DirectoryPSB = item.tag
End Sub

Private Sub optAll_Click()
  If Toolbar1.Buttons(10).Value = tbrPressed Then
    TipoSel = "FILTER"
  Else
    TipoSel = "TOTAL"
  End If
  If filter_list.Visible = True Then
    Carica_Array_Oggetti_Errori TipoSel
  Else
    OptSelection TipoSel
  End If
End Sub

Private Sub optSel_Click()
  If Toolbar1.Buttons(10).Value = tbrPressed Then
    TipoSel = "FILTER"
  Else
    TipoSel = "SELECT"
  End If
  OptSelection TipoSel
End Sub

Sub OptSelection(TipoSel As String)
  Screen.MousePointer = vbHourglass
  
  ReDim Oggetti_SELECT(0)
  ReDim Oggetti_TOTAL(0)

  'Carica solo gli oggetti selezionati
  Carica_Array_Oggetti_Errori TipoSel

  txtPsb.text = ""
  lswDep.ListItems.Clear
  lswCall.ListItems.Clear
  lswOggetti.ListItems.Clear
  
  'Carica_Lista_Oggetti False, "", lswOggetti, Oggetti_SELECT, True
  If TipoSel = "SELECT" Then
    Carica_Lista_Oggetti lswOggetti, Oggetti_SELECT
  End If
  If TipoSel = "TOTAL" Then
    Carica_Lista_Oggetti lswOggetti, Oggetti_TOTAL
  End If
  If TipoSel = "FILTER" Then
    Carica_Lista_Oggetti lswOggetti, Oggetti_FILTER
  End If
  Screen.MousePointer = vbDefault

End Sub


Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim rs As Recordset
  Dim i As Integer
  Dim scelta As Long
  
  Select Case Trim(UCase(Button.key))
    Case "ANALYSER" ' Correttore semi-automatico
      If lswOggetti.ListItems.Count Then
        MadrdF_Corrector.Show
        SetParent MadrdF_Corrector.hwnd, Dli2Rdbms.drParent
        MadrdF_Corrector.Show
        MadrdF_Corrector.Move 0, 0, Dli2Rdbms.drWidth, Dli2Rdbms.drHeight
      End If
    Case "DECODES"
      ' Mauro 18/11/2009
      isUnificaGH = IIf(UCase(m_fun.LeggiParam("IMSDB-GH")) = "YES", True, False)
      ' Generatore di Cloni
      Screen.MousePointer = vbHourglass
      'PBar.Max = lswOggetti.ListItems.count
      'PBar.Value = 0
      'PbarIstr.Max = 1
      'PbarIstr.Value = 0
      For i = 1 To lswOggetti.ListItems.Count
        If lswOggetti.ListItems(i).Selected Then
          TipoFile = lswOggetti.ListItems(i).ListSubItems(1)
          MadrdM_Incapsulatore.createDecodes lswOggetti.ListItems(i).tag
          'If PBar.Max < PBar.Value + 1 Then
          '  PBar.Value = PBar.Max
          'Else
          '  PBar.Value = PBar.Value + 1
          'End If
        End If
      Next i
      'PBar.Value = 0
      Screen.MousePointer = vbDefault
      MsgBox "Creation Decodes complete", vbOKOnly + vbInformation, Dli2Rdbms.drNomeProdotto
    Case "PARSER2"
      parser2
'    Case "IMPPSB"
'      InsertPSB_from_List
'    Case "IMPPCB"
'      InsertPCB_from_List
    'SILVIA gestione filtro
    Case "FILTER"
      Filter
    Case "FILTERAPP"
     FilterApp
    Case "XLS_REPORT"
      scelta = MsgBox("Program/io-routine cross reference report." & vbCrLf & "Do you want to create a separated sheet per each program?", vbYesNoCancel + vbQuestion, Dli2Rdbms.drNomeProdotto)
      If scelta = vbNo Then
        XLSall
      ElseIf scelta = vbYes Then
        XLS
      End If
  End Select
End Sub

Private Sub XLSall()
  Dim i As Integer, j As Integer
  Dim appExcel As New Excel.Application
  Dim cartExcel As Excel.Workbook
  Dim foglioExcel As Excel.Worksheet
  Dim nomePgm As String
  Dim idPgm As Integer
  Dim list As Integer
  Dim indfile As String
  Dim scelta2 As Integer
 
  w = 1
  Screen.MousePointer = vbHourglass
  Set cartExcel = appExcel.Workbooks.Add
  Set foglioExcel = cartExcel.Worksheets.Add
  appExcel.Visible = False
  foglioExcel.name = "Programs list"
  'SQ tmp
  'scelta2 = MsgBox("Report from Incapsulated programs?", vbYesNo + vbQuestion, Dli2Rdbms.drNomeProdotto)
  scelta2 = vbNo
  If scelta2 = vbNo Then
    foglioExcel.Cells(1, 1) = "Pgm"
    foglioExcel.Cells(1, 2) = "Line"
    foglioExcel.Cells(1, 3) = "Instruction"
    foglioExcel.Cells(1, 4) = "io-routine"
    foglioExcel.Cells(1, 5) = "Name"
    foglioExcel.Cells(1, 6) = "IdObject"
    foglioExcel.Rows("2:2").Select
    appExcel.ActiveWindow.FreezePanes = True
    foglioExcel.Columns("A:A").ColumnWidth = 10
    foglioExcel.Columns("B:B").ColumnWidth = 11
    foglioExcel.Columns("C:C").ColumnWidth = 16
    foglioExcel.Columns("D:D").ColumnWidth = 13
    foglioExcel.Columns("E:E").ColumnWidth = 15
    foglioExcel.Columns("F:F").ColumnWidth = 15
    foglioExcel.Rows("1:1").Select
  Else
    foglioExcel.Cells(1, 1) = "Pgm"
    foglioExcel.Cells(1, 2) = "Line"
    foglioExcel.Cells(1, 3) = "Encoded Routine"
    foglioExcel.Cells(1, 4) = "Routine Group"
    foglioExcel.Rows("2:2").Select
    appExcel.ActiveWindow.FreezePanes = True
    foglioExcel.Columns("A:A").ColumnWidth = 10
    foglioExcel.Columns("B:B").ColumnWidth = 6
    foglioExcel.Columns("C:C").ColumnWidth = 15
    foglioExcel.Columns("D:D").ColumnWidth = 15
    foglioExcel.Rows("1:1").Select
  End If
  '_____________________________________
  appExcel.Selection.Font.Bold = True
  With appExcel.Selection.Interior
    .Pattern = xlSolid
    .PatternColorIndex = xlAutomatic
    .ThemeColor = xlThemeColorLight2
    .TintAndShade = 0.399975585192419
    .PatternTintAndShade = 0
  End With
  foglioExcel.Cells.Select
  With appExcel.Selection
    .HorizontalAlignment = xlCenter
    .VerticalAlignment = xlBottom
    .WrapText = False
    .Orientation = 0
    .AddIndent = False
    .IndentLevel = 0
    .ShrinkToFit = False
    .ReadingOrder = xlContext
    .MergeCells = False
  End With
  '_____________________________________
  For i = 1 To lswOggetti.ListItems.Count
    If lswOggetti.ListItems(i).Selected Then
      nomePgm = lswOggetti.ListItems(i).text
      idPgm = lswOggetti.ListItems(i).ListSubItems(7)
      If lswOggetti.ListItems(i).Selected Then
        '????
      End If
      setFoglio foglioExcel, nomePgm, idPgm, scelta2
    End If
  Next i
    Screen.MousePointer = vbDefault
 If w > 1 Then
  appExcel.Sheets("Foglio1").Select
  appExcel.ActiveWindow.SelectedSheets.Delete
  appExcel.Sheets("Foglio2").Select
  appExcel.ActiveWindow.SelectedSheets.Delete
  appExcel.Sheets("Foglio3").Select
  appExcel.ActiveWindow.SelectedSheets.Delete
  indfile = Dli2Rdbms.drPathDb & "\Documents\excel\" & Format(Now, "dd.mm.yyyy.hh.mm.ss") & ".xls"
  cartExcel.SaveAs indfile
  appExcel.Visible = True
  Else
   MsgBox "Impossible to find an entry in tables for selected programs", vbInformation, Dli2Rdbms.drNomeProdotto
  End If
End Sub
  
Public Sub setFoglio(foglioExcel As Excel.Worksheet, nomePgm As String, Id As Integer, scelta As Integer)
  Dim rs As Recordset
  Dim appExcel As New Excel.Application
  Dim cartExcel As Excel.Workbook
  Dim istrcodifica As Long
  
  If scelta = vbYes Then
    'Set rs = m_fun.Open_Recordset("SELECT a.Pgm, a.line, a.routine, a.NomeRout, b.notes FROM TMP_checkIncaps as a, BS_Oggetti as b WHERE b.nome = Right(a.pgm,8) and Right(a.pgm,8) = '" & nomePgm & "' ORDER BY b.notes,a.routine")
    Set rs = m_fun.Open_Recordset("SELECT Pgm, line, routine, NomeRout FROM TMP_checkIncaps where Right(pgm,8) = '" & nomePgm & "' ORDER BY  routine")
  Else
    Set rs = m_fun.Open_Recordset("SELECT a.idPgm, a.riga, a.codifica, a.NomeRout, b.nome, b.IdOggetto FROM psDli_istrCodifica as a, BS_Oggetti as b WHERE idPgm= " & Id & " and a.idOggetto = b.idOggetto ORDER BY codifica")  'and  b.notes is not null and  not Left(b.notes,3)= 'POC' and  not Right(b.notes,3) = 'POC'and b.tipo  = 'PLI'
  End If
  While Not rs.EOF
    If scelta = vbYes Then
      foglioExcel.Cells(w + 1, 1) = nomePgm
      foglioExcel.Cells(w + 1, 2) = rs!Line
      foglioExcel.Cells(w + 1, 3) = rs!Routine
      foglioExcel.Cells(w + 1, 4) = rs!nomeRout & "I"
    Else
      foglioExcel.Cells(w + 1, 1) = nomePgm
      foglioExcel.Cells(w + 1, 2) = rs!Riga
      foglioExcel.Cells(w + 1, 3) = rs!Codifica
      foglioExcel.Cells(w + 1, 4) = rs!nomeRout
      foglioExcel.Cells(w + 1, 5) = rs!nome
      foglioExcel.Cells(w + 1, 6) = rs!IdOggetto
    End If
'        Dim CR, Even_Color, Odd_Color As Long
'        Even_Color = RGB(240, 240, 210) ' colore righe pari
'        Odd_Color = RGB(255, 255, 255) ' colore righe dispari
'
'        If w Mod 2 = 1 Then    'controlla quali sono dispari
'          foglioExcel.Rows(w + 1 & ":" & w + 1).Select
'            With Selection.Interior
'                .Pattern = xlSolid
'                .PatternColorIndex = xlAutomatic
'                '.ThemeColor = xlThemeColorDark1
'                .Color = Even_Color
'                .TintAndShade = 0
'                .PatternTintAndShade = 0
'            End With
'        Else
'          foglioExcel.Rows(w + 1 & ":" & w + 1).Select
'            With Selection.Interior
'                .Pattern = xlSolid
'                .PatternColorIndex = xlAutomatic
'                '.Color = 49407
'                .Color = Odd_Color
'                .TintAndShade = 0
'                .PatternTintAndShade = 0
'            End With
'        End If
    w = w + 1

    rs.MoveNext
  Wend
  
  rs.Close
End Sub

Private Sub XLS()
  Dim i As Integer, j As Integer
  Dim appExcel As New Excel.Application
  Dim cartExcel As Excel.Workbook
  Dim foglioExcel As Excel.Worksheet
  Dim nomePgm As String
  Dim idPgm As Integer
  Dim indfile As String
  
  appExcel.DisplayAlerts = False
  w = 1
  Screen.MousePointer = vbHourglass
  Set cartExcel = appExcel.Workbooks.Add
  For i = 1 To lswOggetti.ListItems.Count
    If lswOggetti.ListItems(i).Selected Then
      nomePgm = lswOggetti.ListItems(i).text
      idPgm = lswOggetti.ListItems(i).ListSubItems(7)
      If lswOggetti.ListItems(i).Selected Then
        Set foglioExcel = cartExcel.Worksheets.Add
        foglioExcel.name = nomePgm
        foglioExcel.Cells(1, 1) = "Pgm"
        foglioExcel.Cells(1, 2) = "Copy"
        foglioExcel.Cells(1, 3) = "Line"
        foglioExcel.Cells(1, 4) = "Instruction"
        foglioExcel.Cells(1, 5) = "io-routine"
        foglioExcel.Rows("2:2").Select
        appExcel.ActiveWindow.FreezePanes = True
        foglioExcel.Columns("A:A").ColumnWidth = 10
        foglioExcel.Columns("B:B").ColumnWidth = 11
        foglioExcel.Columns("C:C").ColumnWidth = 6
        foglioExcel.Columns("D:D").ColumnWidth = 13
        foglioExcel.Columns("E:E").ColumnWidth = 15
        foglioExcel.Rows("1:1").Select
        appExcel.Selection.Font.Bold = True
        With appExcel.Selection.Interior
          .Pattern = xlSolid
          .PatternColorIndex = xlAutomatic
          .ThemeColor = xlThemeColorLight2
          .TintAndShade = 0.399975585192419
          .PatternTintAndShade = 0
        End With
        foglioExcel.Cells.Select
        With appExcel.Selection
          .HorizontalAlignment = xlCenter
          .VerticalAlignment = xlBottom
          .WrapText = False
          .Orientation = 0
          .AddIndent = False
          .IndentLevel = 0
          .ShrinkToFit = False
          .ReadingOrder = xlContext
          .MergeCells = False
        End With
      End If
      setFogliol foglioExcel, nomePgm, idPgm
    End If
  Next i
  Screen.MousePointer = vbDefault
  appExcel.DisplayAlerts = False
  If w > 1 Then
    appExcel.Sheets("Foglio1").Select
    appExcel.ActiveWindow.SelectedSheets.Delete
    appExcel.Sheets("Foglio2").Select
    appExcel.ActiveWindow.SelectedSheets.Delete
    appExcel.Sheets("Foglio3").Select
    appExcel.ActiveWindow.SelectedSheets.Delete
    indfile = Dli2Rdbms.drPathDb & "\Documents\excel\" & Format(Now, "dd.mm.yyyy.hh.mm.ss") & ".xls"
    cartExcel.SaveAs indfile
    appExcel.Visible = True
  Else
     MsgBox "Impossible to find an entry in tables for selected programs", vbInformation, Dli2Rdbms.drNomeProdotto
 End If
End Sub
  
Public Sub setFogliol(foglioExcel As Excel.Worksheet, nomePgm As String, Id As Integer)
  Dim rs As Recordset
  Dim appExcel As New Excel.Application
  Dim cartExcel As Excel.Workbook
  Dim istrcodifica As Long
    
  Set rs = m_fun.Open_Recordset("SELECT a.idPgm, a.riga, a.codifica, a.NomeRout, b.nome, b.IdOggetto FROM psDli_istrCodifica as a, BS_Oggetti as b WHERE idPgm= " & Id & " and a.idOggetto = b.idOggetto ORDER BY codifica")
  w = 1
  While Not rs.EOF
    foglioExcel.Cells(w + 1, 1) = nomePgm
    If rs!idPgm = rs!IdOggetto Then
      foglioExcel.Cells(w + 1, 2) = "'-"
    Else
      foglioExcel.Cells(w + 1, 2) = rs!nome
    End If
    foglioExcel.Cells(w + 1, 3) = rs!Riga
    foglioExcel.Cells(w + 1, 4) = rs!Codifica
    foglioExcel.Cells(w + 1, 5) = rs!nomeRout

    w = w + 1
    rs.MoveNext
  Wend
   If w = 1 Then
    foglioExcel.Delete
  End If
  rs.Close
End Sub

Private Sub Form_Unload(Cancel As Integer)
  ReDim Oggetti_TOTAL(0)
  ReDim Oggetti_SELECT(0)
  
  SetFocusWnd m_fun.FnParent
  
  m_fun.FnActiveWindowsBool = False
  m_fun.FnProcessRunning = False
  m_fun.RemoveActiveWindows Me
End Sub

''Private Sub cmdSeleziona_Click()
''  Dim SQL As String
''
''  SQL = Crea_SQL
''
''  'Carica la lista degli oggetti
''  If SQL = "" Then
''    Carica_Lista_Oggetti False, SQL, lswOggetti, Oggetti_ERROR, True
''  Else
''    Carica_Lista_Oggetti True, SQL, lswOggetti, Oggetti_ERROR, True
''  End If
''End Sub
''
''Private Function Crea_SQL() As String
''  Dim Texto As String
''  Dim App As String
''
''  Texto = "Where"
''
''  If ChkDBD.Value = 1 Then
''    If Trim(UCase(Texto)) = "WHERE" Then
''      App = " Codice = 'F06'"
''    Else
''      App = " Or Codice = 'F06'"
''    End If
''
''    Texto = Texto & App
''  End If
''
''  If ChkDliUnk.Value = 1 Then
''    If Trim(UCase(Texto)) = "WHERE" Then
''      App = " Codice = 'F09'"
''    Else
''      App = " Or Codice = 'F09'"
''    End If
''
''    Texto = Texto & App
''  End If
''
''  If chkInclude.Value = 1 Then
''    If Trim(UCase(Texto)) = "WHERE" Then
''      App = " Codice = 'F02'"
''    Else
''      App = " Or Codice = 'F02'"
''    End If
''
''    Texto = Texto & App
''  End If
''
''  If ChkNonDec.Value = 1 Then
''    If Trim(UCase(Texto)) = "WHERE" Then
''      App = " Codice = 'F08'"
''    Else
''      App = " Or Codice = 'F08'"
''    End If
''
''    Texto = Texto & App
''  End If
''
''  If chkPSB.Value = 1 Then
''    If Trim(UCase(Texto)) = "WHERE" Then
''      App = " Codice = 'F04'"
''    Else
''      App = " Or Codice = 'F04'"
''    End If
''
''    Texto = Texto & App
''  End If
''
''  If ChkPSBAss.Value = 1 Then
''    If Trim(UCase(Texto)) = "WHERE" Then
''      App = " Codice = 'F07'"
''    Else
''      App = " Or Codice = 'F07'"
''    End If
''
''    Texto = Texto & App
''  End If
''
''  If ChkSegment.Value = 1 Then
''    If Trim(UCase(Texto)) = "WHERE" Then
''      App = " Codice = 'F10'"
''    Else
''      App = " Or Codice = 'F10'"
''    End If
''
''    Texto = Texto & App
''  End If
''
''  If Trim(UCase(Texto)) = "WHERE" Then
''    Crea_SQL = ""
''  Else
''    Texto = "Select * From BS_Segnalazioni " & Texto
''
''    Crea_SQL = Texto
''  End If
''End Function

Private Sub cmdAssocia_Click()
  Dim OldIdOgg As Long
  Dim cParam As Collection
  
  Screen.MousePointer = vbHourglass
  
  IdOgg = Dli2Rdbms.drIdOggetto
  'SQ 15-10-15
  IdPSB = Restituisci_IdOggetto_Da_Nome(NomePsb, "PSB")
  
  If IdPSB Then
    
    '1) Scrive la relazione
    Scrivi_Relazione_PSB IdOgg, IdPSB, NomePsb
    
    '2) Lancia il parsing
    OldIdOgg = DLLExtParser.PsIdOggetto_Add
    DLLExtParser.PsIdOggetto_Add = IdOgg
    Set cParam = New Collection
    cParam.Add IdOgg
    
    DLLExtParser.AttivaFunzione ("PARSER_DLI"), cParam
    
    DLLExtParser.PsIdOggetto_Add = OldIdOgg
      
    '3) Elimina le segnalazioni PSB Non associato
    Dli2Rdbms.drConnection.Execute "Delete * From BS_Segnalazioni Where IdOggetto = " & IdOgg & " And Codice = 'F07'"
  
    '4) Aggiorna la Lista degli oggetti
    lswOggetti.SelectedItem.SubItems(6) = NomePsb
    
  Else
    MsgBox "PSB Find Error... Launch parsing again...", vbExclamation, Dli2Rdbms.drNomeProdotto
  End If
  
  Screen.MousePointer = vbDefault
End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.key
  Case "IMPPSB"
    InsertPSB_from_List
  Case "IMPPCB"
    InsertPCB_from_List
  Case "EXPPSB"
    ExportPSB_from_DB
  Case "EXPPCB"
    ExportPCB_from_DB
  End Select
End Sub

Private Sub txtPsb_Change()
  NomePsb = Trim(txtPsb.text)
End Sub

Public Sub Aggiungi_Oggetto_PSB_In_DB()
  Dim rTree As Recordset, rOgg As Recordset
  Dim Directory As String
  Dim Liv1 As String, Liv2 As String
  
  Set rOgg = m_fun.Open_Recordset("Select * From BS_Oggetti Where " & _
                                  "Area_Appartenenza = 'SOURCE' And Tipo = 'PSB'")
  
  If rOgg.RecordCount Then
    
    Directory = rOgg!Directory_Input
    Liv1 = rOgg!Livello1
    Liv2 = rOgg!Livello2
    
  Else
    'Devo andarmi a prendere la directory del PSB selezionato
    Directory = Crea_Directory_Parametrica(DirectoryPSB, Dli2Rdbms.drPathDef)
    Liv1 = "PSB"
    Liv2 = Directory
    
    'Aggiunge nella tabella TreeView i 2 record per creare il ramo PSB
'    Set rTree =m_fun.open_recordset("Select * From BS_TreeView Where Testo = 'PSB' And NRelative = 'SOURCE' And Key = 'SOURCE#PSB' And Tipo = 4 And Livello = 'L1' And Image = 4")
'
'    If rTree.RecordCount = 0 Then
'      rTree.AddNew
'
'      rTree!Id = Dli2RdbMs.DrTreePrj.Nodes.Count + 1
'      rTree!Testo = "PSB"
'      rTree!NRelative = "SOURCE"
'      rTree!key = "SOURCE#PSB"
'      rTree!Tipo = 4
'      rTree!Livello = "L1"
'      rTree!Image = 4
'
'      rTree.Update
'
'      rTree.AddNew
'
'      rTree!Id = Dli2RdbMs.DrTreePrj.Nodes.Count + 2
'      rTree!Testo = "$\PSB"
'      rTree!NRelative = "SOURCE#PSB"
'      rTree!key = "SOURCE#PSB@$\PSB"
'      rTree!Tipo = 4
'      rTree!Livello = "L2"
'      rTree!Image = 2
'
'      rTree.Update
'    End If
  End If
  rOgg.Close
  
  ''Aggiungi_Oggetto_PSB_In_DB2 Directory, Liv1, Liv2
  
End Sub

' Mauro 13/03/2008: Copiato dal Relation Error; Lancia il parser di secondo livello
Public Sub parser2()
  'Parser degli oggetti selezionati
  Dim i As Long
  Dim cParam As Collection
  Set objectList = New Collection
  On Error GoTo errorHandler
  
  'lbParser.Caption = ""
  If m_fun.FnProcessRunning Then
    m_fun.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  
  m_fun.FnActiveWindowsBool = True
  m_fun.FnProcessRunning = True
  
   '23-01-06: faceva dentro il parsing 1000 volte le stesse cose...
  DLLExtParser.AttivaFunzione "INIT_PARSING", cParam
  For i = 1 To lswOggetti.ListItems.Count
    If lswOggetti.ListItems(i).Selected Then
      If lswOggetti.ListItems(i).ListSubItems(1) <> "INC" And _
         lswOggetti.ListItems(i).ListSubItems(1) <> "CPY" Then
        If isNewObject(CLng(lswOggetti.ListItems(i).tag)) Then
          Set cParam = New Collection
          cParam.Add CLng(lswOggetti.ListItems(i).tag)
          m_fun.FnActiveWindowsBool = False
          DLLExtParser.AttivaFunzione "PARSE_OBJECT_SECOND_LEVEL", cParam
          
          m_fun.FnActiveWindowsBool = True
        End If
      End If
    End If
  Next i
  
  m_fun.FnActiveWindowsBool = False
  m_fun.FnProcessRunning = False
  
  Screen.MousePointer = vbDefault
  MsgBox "Second Level Parsing concluded", vbOKOnly + vbInformation, Dli2Rdbms.drNomeProdotto
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "ML04E"
  'lbParser.Caption = "Second Level Parsing NOT concluded"
End Sub

Function isNewObject(IdOggetto As Long) As Boolean
  On Error GoTo errorHandler
  
  objectList.Add IdOggetto, CStr(IdOggetto)
  isNewObject = True
  
  Exit Function
errorHandler:
  isNewObject = False
End Function

Private Sub txtPsb_DblClick()
  Dim IdPSB As Long
  
  If Trim(txtPsb.text) <> "" Then
    IdPSB = Restituisci_IdOggetto_Da_Nome(Trim(txtPsb.text), "PSB")
    m_fun.Show_ObjViewer IdPSB
  End If
End Sub
