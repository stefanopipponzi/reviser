VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadrdF_Tools 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Tools & Verify"
   ClientHeight    =   5895
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8580
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5895
   ScaleWidth      =   8580
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Errors List"
      ForeColor       =   &H8000000D&
      Height          =   3225
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   8325
      Begin MSComctlLib.ProgressBar PBParsing 
         Height          =   195
         Left            =   5850
         TabIndex        =   7
         Top             =   2010
         Width           =   2325
         _ExtentX        =   4101
         _ExtentY        =   344
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin VB.OptionButton OptSel 
         Caption         =   "No DBD association to CBL"
         Height          =   255
         Index           =   4
         Left            =   5820
         TabIndex        =   6
         Tag             =   "NODBP"
         Top             =   1650
         Width           =   2355
      End
      Begin VB.OptionButton OptSel 
         Caption         =   "Non decode Segments"
         Height          =   255
         Index           =   3
         Left            =   5820
         TabIndex        =   5
         Tag             =   "NOSEG"
         Top             =   1296
         Width           =   2025
      End
      Begin VB.OptionButton OptSel 
         Caption         =   "DBD Missing"
         Height          =   255
         Index           =   2
         Left            =   5820
         TabIndex        =   4
         Tag             =   "NODBD"
         Top             =   944
         Width           =   1335
      End
      Begin VB.OptionButton OptSel 
         Caption         =   "Non Decode Istructions"
         Height          =   255
         Index           =   1
         Left            =   5820
         TabIndex        =   3
         Tag             =   "NODEC"
         Top             =   592
         Width           =   2085
      End
      Begin VB.OptionButton OptSel 
         Caption         =   "No PSB association to CBL"
         Height          =   255
         Index           =   0
         Left            =   5820
         TabIndex        =   2
         Tag             =   "NOPSB"
         Top             =   240
         Width           =   2355
      End
      Begin MSComctlLib.ListView LswOggetti 
         Height          =   2925
         Left            =   120
         TabIndex        =   1
         Top             =   210
         Width           =   5565
         _ExtentX        =   9816
         _ExtentY        =   5159
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "IdOggetto"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Object's Name"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Directory"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ProgressBar PBOther 
         Height          =   195
         Left            =   5850
         TabIndex        =   8
         Top             =   2280
         Width           =   2325
         _ExtentX        =   4101
         _ExtentY        =   344
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBTotale 
         Height          =   195
         Left            =   5850
         TabIndex        =   9
         Top             =   2550
         Width           =   2325
         _ExtentX        =   4101
         _ExtentY        =   344
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin VB.Label lblStatus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "STATUS"
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   6615
         TabIndex        =   10
         Top             =   2850
         Width           =   675
      End
   End
End
Attribute VB_Name = "MadrdF_Tools"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Call ResizeControls(Me)
  
  'resize della listview
  LswOggetti.ColumnHeaders(3).Width = LswOggetti.Width - LswOggetti.ColumnHeaders(1).Width - LswOggetti.ColumnHeaders(2).Width - 270
End Sub
