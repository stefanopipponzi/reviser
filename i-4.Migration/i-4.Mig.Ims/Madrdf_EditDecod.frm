VERSION 5.00
Begin VB.Form Madrdf_EditDecod 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Edit Decode"
   ClientHeight    =   3765
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8790
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3765
   ScaleWidth      =   8790
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtRoutine 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   405
      Left            =   1200
      TabIndex        =   12
      Top             =   2160
      Width           =   2415
   End
   Begin VB.CheckBox chkSave 
      Caption         =   "Save element on Repository"
      CausesValidation=   0   'False
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   120
      TabIndex        =   11
      Top             =   3300
      Width           =   4575
   End
   Begin VB.Frame Frame1 
      Height          =   45
      Left            =   40
      TabIndex        =   10
      Top             =   2700
      Width           =   8620
   End
   Begin VB.CheckBox chkRepository 
      Caption         =   "Search elements on Repository (use ""%"" for a ""like"" query)"
      CausesValidation=   0   'False
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   120
      TabIndex        =   9
      Top             =   2820
      Width           =   4575
   End
   Begin VB.CommandButton cmdListInstr 
      Caption         =   "..."
      Height          =   435
      Left            =   3720
      TabIndex        =   8
      Top             =   1650
      Width           =   375
   End
   Begin VB.TextBox txtDBD 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   405
      Left            =   6240
      TabIndex        =   7
      Top             =   1665
      Width           =   2415
   End
   Begin VB.TextBox txtInstruction 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   405
      Left            =   1200
      TabIndex        =   5
      Top             =   1665
      Width           =   2415
   End
   Begin VB.TextBox txtDecoding 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   1395
      Left            =   1200
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   120
      Width           =   7515
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   7800
      Picture         =   "Madrdf_EditDecod.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2880
      Width           =   825
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Confirm"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   6840
      Picture         =   "Madrdf_EditDecod.frx":1D2A
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   2880
      Width           =   825
   End
   Begin VB.Label Label4 
      Caption         =   "Routine"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   2220
      Width           =   615
   End
   Begin VB.Label label3 
      Caption         =   "DBD"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   5760
      TabIndex        =   6
      Top             =   1740
      Width           =   375
   End
   Begin VB.Label label2 
      Caption         =   "Instruction"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1740
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Decoding"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "Madrdf_EditDecod"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'MF 31-08-07 Nuovo Form
Option Explicit
Private updateDec As Boolean
Public index As Long

Private Sub chkRepository_Click()
  If chkRepository.value Then
    'chkSave.Value = 0
    chkSave.Enabled = False
  Else
    chkSave.Enabled = True
  End If
End Sub

Private Sub cmdExit_Click()
  Unload Me
End Sub

Private Sub cmdListInstr_Click()
  Load MadrdF_InstructionList
End Sub

Private Sub cmdOk_Click()
  Dim i As Long
  Dim listd As ListItem
  Dim rsInstr As Recordset, rsDBD As Recordset, tb1 As Recordset
  Dim query As String
  Dim Routine As String
  Dim risp As String
  Dim maxId As Long
  On Error GoTo ErrorHandler
  
  MadrdF_GenRout.txtTotInstr = "0"
  Screen.MousePointer = vbHourglass
  If updateDec Then
    ' Mauro 07/11/2007 : Salvo modifiche sul DB
    ' EDIT
    If chkSave Then
      Set rsInstr = m_fun.Open_Recordset("select * from mgdli_decodificaistr where " & _
                                         "codifica = '" & txtInstruction & "'")
      If rsInstr.RecordCount Then
        risp = MsgBox("This Instruction already exists, do you want to overwrite?", vbYesNoCancel + vbQuestion)
        If risp = vbYes Then
          rsInstr!Decodifica = txtDecoding
          rsInstr!ImsDB = -1
          rsInstr!ImsDC = 0
          rsInstr!Cics = 0
          ' Mauro 06/02/2008 : Il NomeRout sulla decodifica va a morire
          'rsInstr!nomeRout = DecPgm(txtDecoding)
          rsInstr!nomeRout = txtRoutine
          
          rsInstr.Update
        ElseIf risp = vbCancel Then
          Screen.MousePointer = vbDefault
          Exit Sub
        End If
      Else
        Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
          maxId = 1
       Else
          maxId = tb1(0) + 1
       End If
       
       
        rsInstr.AddNew
        rsInstr!IdIstruzione = maxId
        rsInstr!Codifica = UCase(txtInstruction)
        rsInstr!Decodifica = txtDecoding
        rsInstr!ImsDB = -1
        rsInstr!ImsDC = 0
        rsInstr!Cics = 0
        ' Mauro 06/02/2008 : Il NomeRout sulla decodifica va a morire
        'rsInstr!nomeRout = DecPgm(txtDecoding)
        rsInstr!nomeRout = txtRoutine
        
        rsInstr.Update
        rsInstr.Close
        
        MsgBox "New instruction inserted correctly!", vbOKOnly + vbInformation
        
        Set listd = MadrdF_GenRout.lswSlotMachine.ListItems.Add(, , txtDecoding)
        listd.SubItems(1) = txtInstruction
        listd.SubItems(2) = txtDBD
        listd.SubItems(3) = txtRoutine
        MadrdF_GenRout.txtTotInstr = MadrdF_GenRout.txtTotInstr + 1
        Unload Me
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
      rsInstr.Close
    End If
    MadrdF_GenRout.lswSlotMachine.ListItems(index).text = txtDecoding
    MadrdF_GenRout.lswSlotMachine.ListItems(index).SubItems(1) = txtInstruction
    MadrdF_GenRout.lswSlotMachine.ListItems(index).SubItems(2) = txtDBD
    MadrdF_GenRout.lswSlotMachine.ListItems(index).SubItems(3) = txtRoutine
    Unload Me
  Else
    ' ADD
    If chkRepository Then
      'silvia 23-5-2008 aggiungo filtro sulla routine
      'If Len(txtInstruction) Or Len(txtDecoding) Then
      If Len(txtInstruction) Or Len(txtDecoding) Or Len(txtRoutine) Then
        query = "select * from mgdli_decodificaistr where"
        If Len(txtInstruction) Then
          query = query & " codifica like '" & txtInstruction & "'"
          If Len(txtDecoding) Then
            query = query & " AND decodifica like '" & txtDecoding & "'"
          End If
        ElseIf Len(txtDecoding) Then
          query = query & " decodifica like '" & txtDecoding & "'"
        'silvia 23-5-2008 aggiungo filtro sulla routine
        ElseIf Len(txtRoutine) Then
          query = query & " nomerout like '" & txtRoutine & "'"
        End If
        Set rsInstr = m_fun.Open_Recordset(query)
        If rsInstr.RecordCount Then
          Do Until rsInstr.EOF
            MadrdF_GenRout.txtTotInstr = MadrdF_GenRout.txtTotInstr + 1
            Set listd = MadrdF_GenRout.lswSlotMachine.ListItems.Add(, , rsInstr!Decodifica)
            listd.SubItems(1) = rsInstr!Codifica & ""
            listd.SubItems(3) = rsInstr!nomeRout & ""
            listd.SubItems(2) = ""
            ' Mi ricavo il nome del DBD associato
            If Len(rsInstr!nomeRout) Then
              Routine = Mid(rsInstr!nomeRout & "", 3, 4)
              Set rsDBD = m_fun.Open_Recordset( _
                "Select dbd_name from PsDli_DBD WHERE " & _
                " route_name = '" & Routine & "' and not obsolete and to_migrate")
              If rsDBD.RecordCount Then
                listd.SubItems(2) = rsDBD!dbd_name
              End If
              rsDBD.Close
            End If
            rsInstr.MoveNext
          Loop
          rsInstr.Close
        Else
          MsgBox "Object not found in repository!", vbOKOnly + vbInformation, Dli2Rdbms.drNomeProdotto
          Screen.MousePointer = vbDefault
          Exit Sub
        End If
      Else
        MsgBox "Value missing for Decoding or Instruction!", vbOKOnly + vbInformation, Dli2Rdbms.drNomeProdotto
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
    Else
      ' Mauro 08/11/2007 : Gestione salvataggio in Repository
      If chkSave Then
        Set rsInstr = m_fun.Open_Recordset("select * from mgdli_decodificaistr where " & _
                                           "codifica = '" & txtInstruction & "'")
        If rsInstr.RecordCount Then
          risp = MsgBox("This Instruction already exists, do you want to overwrite?", vbYesNoCancel + vbQuestion)
          If risp = vbYes Then
            rsInstr!Decodifica = txtDecoding
            rsInstr!ImsDB = -1
            rsInstr!ImsDC = 0
            rsInstr!Cics = 0
            ' Mauro 06/02/2008 : Il NomeRout sulla decodifica va a morire
            'rsInstr!nomeRout = DecPgm(txtDecoding)
            rsInstr!nomeRout = txtRoutine
            
            rsInstr.Update
          ElseIf risp = vbCancel Then
            Screen.MousePointer = vbDefault
            Exit Sub
          End If
        Else
           Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
        
           If tb1.EOF Then
            maxId = 1
           Else
            maxId = tb1(0) + 1
          End If
        
        'SQ tmp:
        'm_fun.WriteLog "##" & wOpe
        
          rsInstr.AddNew
          rsInstr!IdIstruzione = maxId
          rsInstr!Codifica = UCase(txtInstruction)
          rsInstr!Decodifica = txtDecoding
          rsInstr!ImsDB = -1
          rsInstr!ImsDC = 0
          rsInstr!Cics = 0
          ' Mauro 06/02/2008 : Il NomeRout sulla decodifica va a morire
          'rsInstr!nomeRout = DecPgm(txtDecoding)
          rsInstr!nomeRout = txtRoutine
          
          rsInstr.Update
          rsInstr.Close
          
          MsgBox "New instruction inserted correctly!", vbOKOnly + vbInformation
        End If
        rsInstr.Close
      End If
      Set listd = MadrdF_GenRout.lswSlotMachine.ListItems.Add(, , txtDecoding)
      listd.SubItems(1) = txtInstruction
      listd.SubItems(2) = txtDBD
      listd.SubItems(3) = txtRoutine
      MadrdF_GenRout.txtTotInstr = MadrdF_GenRout.txtTotInstr + 1
    End If
  End If
  Screen.MousePointer = vbDefault
  Unload Me
Exit Sub
ErrorHandler:
  MsgBox err.Number & " - " & err.Description
End Sub

Public Sub editInstr(item As ListItem)
  updateDec = True
  chkRepository.Visible = False
  cmdListInstr.Visible = False
  txtInstruction.Enabled = False
  index = item.index

  txtDecoding = item.text
  txtInstruction = item.ListSubItems(1).text
  txtDBD = item.ListSubItems(2).text
  txtRoutine = item.ListSubItems(3).text
  
  Me.Caption = "Edit decodes"
  Me.Show
End Sub

Public Sub addInstr()
  updateDec = False
  chkRepository.Visible = True
  chkRepository.value = 1
  chkSave.Enabled = False
  cmdListInstr.Visible = True
  txtInstruction.Enabled = True
  
  txtDecoding = ""
  txtInstruction = ""
  txtDBD = ""
  txtRoutine = ""
  
  Me.Caption = "Add decodes"
  Me.Show
End Sub

'Public Function DecPgm(stringa As String) As String
'  Dim Testo As String
'  Dim k1, k2 As Integer
'
'  k1 = InStr(stringa, "<pgm>")
'  If k1 > 0 Then
'     k2 = InStr(k1 + 1, stringa, "</pgm>")
'     If k2 > 0 Then
'        Testo = Mid(stringa, k1 + 5, k2 - (k1 + 5))
'     End If
'  End If
'  DecPgm = Testo
'End Function
