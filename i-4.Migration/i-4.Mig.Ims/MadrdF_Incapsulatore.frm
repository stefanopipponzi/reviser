VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadrdF_Incapsulatore 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "IMS/DB - Objects Encapsulator"
   ClientHeight    =   7170
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8895
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7170
   ScaleWidth      =   8895
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   195
      Left            =   1920
      TabIndex        =   22
      Top             =   5040
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Frame Frame5 
      Caption         =   "Action"
      ForeColor       =   &H00000080&
      Height          =   855
      Left            =   60
      TabIndex        =   18
      Top             =   3573
      Width           =   3375
      Begin VB.OptionButton optNoDecode 
         Caption         =   "Encaps only"
         Height          =   255
         Left            =   1800
         TabIndex        =   21
         Top             =   480
         Width           =   1335
      End
      Begin VB.OptionButton optAll 
         Caption         =   "Encapsulation"
         Height          =   192
         Left            =   180
         TabIndex        =   20
         Top             =   240
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton optDecodesOnly 
         Caption         =   "Decodes only"
         Height          =   192
         Left            =   1800
         TabIndex        =   19
         Top             =   240
         Width           =   1332
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Encapsulation Source Options"
      Enabled         =   0   'False
      ForeColor       =   &H00000080&
      Height          =   612
      Left            =   60
      TabIndex        =   15
      Top             =   4440
      Width           =   3375
      Begin VB.OptionButton OptEncaps 
         Caption         =   "Encapsulated"
         Enabled         =   0   'False
         Height          =   192
         Left            =   1800
         TabIndex        =   17
         Top             =   240
         Width           =   1332
      End
      Begin VB.OptionButton Optsource 
         Caption         =   "Native"
         Enabled         =   0   'False
         Height          =   192
         Left            =   180
         TabIndex        =   16
         Top             =   240
         Value           =   -1  'True
         Width           =   852
      End
   End
   Begin RichTextLib.RichTextBox RTBModifica 
      Height          =   285
      Left            =   3000
      TabIndex        =   12
      Top             =   4680
      Visible         =   0   'False
      Width           =   315
      _ExtentX        =   556
      _ExtentY        =   503
      _Version        =   393217
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MadrdF_Incapsulatore.frx":0000
   End
   Begin RichTextLib.RichTextBox RTRoutine 
      Height          =   285
      Left            =   2520
      TabIndex        =   11
      Top             =   4680
      Visible         =   0   'False
      Width           =   315
      _ExtentX        =   556
      _ExtentY        =   503
      _Version        =   393217
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MadrdF_Incapsulatore.frx":0082
   End
   Begin RichTextLib.RichTextBox RTFile 
      Height          =   285
      Left            =   2160
      TabIndex        =   10
      Top             =   4680
      Visible         =   0   'False
      Width           =   315
      _ExtentX        =   556
      _ExtentY        =   503
      _Version        =   393217
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MadrdF_Incapsulatore.frx":0104
   End
   Begin VB.Frame Frame4 
      Caption         =   "Activity Log"
      ForeColor       =   &H00000080&
      Height          =   4455
      Left            =   3480
      TabIndex        =   6
      Top             =   597
      Width           =   5385
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   255
         Left            =   90
         TabIndex        =   8
         Top             =   4155
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ListView LvObject 
         Height          =   3495
         Left            =   90
         TabIndex        =   7
         Top             =   240
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   6165
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "ID"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Object"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Time"
            Object.Width           =   5293
         EndProperty
      End
      Begin MSComctlLib.ProgressBar PbarIstr 
         Height          =   255
         Left            =   90
         TabIndex        =   13
         Top             =   3885
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Error Log"
      ForeColor       =   &H00000080&
      Height          =   1965
      Left            =   3480
      TabIndex        =   5
      Top             =   5160
      Width           =   5385
      Begin RichTextLib.RichTextBox RTErr 
         Height          =   1605
         Left            =   60
         TabIndex        =   9
         Top             =   270
         Width           =   5235
         _ExtentX        =   9234
         _ExtentY        =   2831
         _Version        =   393217
         BackColor       =   -2147483624
         ScrollBars      =   2
         TextRTF         =   $"MadrdF_Incapsulatore.frx":0186
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Analysis Function"
      ForeColor       =   &H00000080&
      Height          =   1965
      Left            =   60
      TabIndex        =   3
      Top             =   5160
      Width           =   3375
      Begin VB.ListBox LstFunction 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   1185
         ItemData        =   "MadrdF_Incapsulatore.frx":0208
         Left            =   60
         List            =   "MadrdF_Incapsulatore.frx":020A
         Style           =   1  'Checkbox
         TabIndex        =   4
         Top             =   240
         Width           =   3195
      End
      Begin RichTextLib.RichTextBox RTBR 
         Height          =   285
         Left            =   480
         TabIndex        =   14
         Top             =   1560
         Visible         =   0   'False
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   503
         _Version        =   393217
         Enabled         =   -1  'True
         TextRTF         =   $"MadrdF_Incapsulatore.frx":020C
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Select Object for Analysis"
      ForeColor       =   &H00000080&
      Height          =   2955
      Left            =   60
      TabIndex        =   1
      Top             =   570
      Width           =   3375
      Begin MSComctlLib.TreeView TVSelObj 
         Height          =   2535
         Left            =   60
         TabIndex        =   2
         Top             =   240
         Width           =   3195
         _ExtentX        =   5636
         _ExtentY        =   4471
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   529
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Start"
            Object.ToolTipText     =   "Start Encapsulating"
            ImageKey        =   "Attiva"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Help"
            Object.ToolTipText     =   "Help"
            ImageKey        =   "Help"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Delete"
            Object.ToolTipText     =   "Delete Decodes"
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   2760
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Incapsulatore.frx":027B
            Key             =   "Attiva"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Incapsulatore.frx":0595
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Incapsulatore.frx":06A7
            Key             =   "Printer"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Incapsulatore.frx":09CB
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MadrdF_Incapsulatore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'SQ: utilizzare questo metodo:
'Const ..._CHK = 0
'Const ..._CHK = 1
'Const ..._CHK = 2
'Const ..._CHK = 3
Const EMBEDDED_CHK = 4

Dim NumObjInParser As Double
Dim decodesOnly As Boolean
Dim Nodecode As Boolean
Dim FullIncaps As Boolean ' <UNIFICAINCAPS>

Private Sub Command1_Click()
  Dim rsEntry As Recordset, rsEntry2 As Recordset

     Set rsEntry = m_fun.Open_Recordset("Select riga from PSDli_Istruzioni where idoggetto = 1514 and Istruzione = 'ISRT'")
     While Not rsEntry.EOF
      m_fun.FnConnection.Execute "Delete from PsDLi_Istruzioni_Clone where Riga = " & rsEntry!Riga & " and Idpgm = 1514"
      m_fun.FnConnection.Execute "Update PsDLi_IstrDett_Key set Operatore = 'EQ' where Riga = " & rsEntry!Riga & " and Idpgm = 1514"
      rsEntry.MoveNext
     Wend
    rsEntry.Close
'  Wend
End Sub

'SQ: BUTTARE VIA --^
Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub LstFunction_Click()
  Dim K1 As Integer
   
  'ATTENZIONE, POSIZIONALE: pericolosissimo!
  'MadrdM_Incapsulatore.EmbeddedRoutine = LstFunction.Selected(EMBEDDED_CHK)
  If LstFunction.ListCount > EMBEDDED_CHK Then
    EmbeddedRoutine = LstFunction.Selected(EMBEDDED_CHK)
  End If
End Sub

Private Sub updateEntryOff(pIdOggetto As Long, pRiga As Long, offset As Long)
  Dim rsEntry As Recordset
  
  Set rsEntry = m_fun.Open_Recordset("Select * from PS_OBJROWOFFSET where " & _
                                     "idoggetto = " & pIdOggetto & " and riga = " & pRiga)
  If Not rsEntry.EOF Then
    rsEntry!offsetEncaps = rsEntry!offsetEncaps + offset
  Else
    rsEntry.AddNew
    rsEntry!IdOggetto = pIdOggetto
    rsEntry!Riga = pRiga
    rsEntry!offsetEncaps = offset
  End If
  
  rsEntry.Update
  rsEntry.Close
End Sub

Private Sub LvObject_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = LvObject.SortOrder
  
  key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  LvObject.SortOrder = Order
  LvObject.SortKey = key - 1
  LvObject.Sorted = True
End Sub

Private Sub LvObject_DblClick()
  Dim IdObj As Long, pathIncaps As String
  
  On Error GoTo errorHandler
  
  If LvObject.ListItems.Count And Not decodesOnly Then
    IdObj = LvObject.SelectedItem
    pathIncaps = LvObject.SelectedItem.SubItems(1)
    ' FnExternalEditor dovr� essere cambiato con il Beyond Compare appena verr� passato sulla Funzioni
    ' Al momento non � stato fatto perch� senn� ci perdiamo la compatibilit� binaria
    If Not (IdObj = 0 Or pathIncaps = "") Then
      CompareFiles m_fun.FnCompareEditor, IdObj, pathIncaps
    Else
      MsgBox "No files to compare", vbCritical, "Compare failure"
    End If
  End If
  
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  MsgBox err.Description, vbExclamation, "Attention!"
End Sub

Private Sub optAll_Click()
  decodesOnly = False
  Frame1.Enabled = True
  Frame6.Enabled = False
  OptEncaps.Enabled = False
  Optsource.Enabled = False
  LstFunction.Enabled = True
  Nodecode = False
  FullIncaps = True
End Sub

Private Sub optDecodesOnly_Click()
  decodesOnly = True
  Frame1.Enabled = False  'dei nomi a sti frame?!
  Frame6.Enabled = False
  OptEncaps.Enabled = False
  Optsource.Enabled = False
  Dim i As Integer
  LstFunction.Enabled = False
  FullIncaps = False
End Sub

Private Sub optNoDEcode_Click()
  decodesOnly = False
  Nodecode = True
  FullIncaps = False
End Sub
Private Function WMIncaps(pIdOggetto) As Boolean
Dim rst As Recordset
    WMIncaps = False
    If UCase(Left(m_fun.FnNomeDB, 7)) = "WALMART" Then
        Set rst = m_fun.Open_Recordset("Select IdOggetto from PsDLI_IstrDBD where IdOggetto = " & pIdOggetto & _
        " and IdDBD in  (Select IdOggetto from Bs_Oggetti where (Nome = 'P1E' or Nome = 'WCP1E'))")
        If Not rst.EOF Then
            WMIncaps = True
        End If
    End If
End Function
Private Function WM_Istr_TO_INCAPS(pIstruzione) As Integer
Dim rst As Recordset, likeBatch As Boolean
    WM_Istr_TO_INCAPS = 0
    Select Case (pIstruzione)
        Case "GHN", "GHNP", "GHU", "GN", "GNP", "GU", "POS" 'IRIS-DB
            WM_Istr_TO_INCAPS = 1
        Case "DLET", "ISRT", "REPL"
            WM_Istr_TO_INCAPS = 2
    End Select
    'ISRT e famiglia trattate come istruzioni di lettura per CICS background e BATCH
    'TODO sostituire  "True" con controllo
    Set rst = m_fun.Open_Recordset("Select Program from Tier1_Inventory  where Program = '" & NomeProg & _
        "' and CType = 'Cics Screen'")
    If rst.EOF Then
       likeBatch = True
    Else
       likeBatch = False
    End If
    If WM_Istr_TO_INCAPS = 2 And likeBatch Then
        WM_Istr_TO_INCAPS = 1
    End If
End Function
Private Function isIstrDli(pIdOggetto, pRiga) As Boolean
Dim rst As Recordset
    isIstrDli = False
    Set rst = m_fun.Open_Recordset("Select IdOggetto from PsExec where IdOggetto = " & pIdOggetto & _
                                    " and Riga = " & pRiga & " and Area = 'DLI'")
    If Not rst.EOF Then
        isIstrDli = True
        rst.Close
    Else
       rst.Close
        Set rst = m_fun.Open_Recordset("Select IdOggetto from PsCall where IdOggetto = " & pIdOggetto & _
                                        " and Riga = " & pRiga & " and (Programma like '''CBLTDLI''' or Programma like '''AERTDLI''')")
        If Not rst.EOF Then
            isIstrDli = True
        End If
    End If
End Function

' Nuovo incapsulamento
Private Sub Toolbar1_ButtonClick(ByVal Button_selection As MSComctlLib.Button)
  Dim rst As Recordset, rIst As Recordset, rstCloni As Recordset, rsoffset As Recordset
  Dim typeIncaps As String, wIstr As String, Selezione As String, lastPos As Long
  Dim MaxVal As Long
  Dim nomeRoutine As String, totaloffset As Long, rsEntry As Recordset, rigaEntry As Long, upentry As Boolean
  Dim idPgmCpy As Long
  ' AC da questo array discriminiamo copy da programmi
  Dim arrayPGM() As Long
  Dim arrayPGMInfo() As Boolean
  Dim rsPgm As Recordset, i As Integer, startPLIkey As Long, startPLIkeyRet As Long
  Dim NomeProgappo As String, offsetentry As Long
  Dim isValidOneCaller As Boolean, isTheSameCod As Boolean
  Dim vresp As Integer, FDin As Long, filenameI As String
  Dim Line As String, lineprec As String, lineNumber As Long
  Dim istrType As Integer
  Dim isFileNotFound As Boolean
  Dim isUnificated As Boolean
  Dim seqInstr As Integer
  On Error GoTo errors
  
  'AC 04/08/10   tag  <UNIFICAINCAPS>  identifica le modifiche per unificare l'incapsulatore
  
  ' Mauro 18/11/2009
  isUnificaGH = IIf(UCase(m_fun.LeggiParam("IMSDB-GH")) = "YES", True, False)
  '''''''''''''''''''''''''''''''''''''''''''''''''
  'SQ 30-10-07
  ' SOLO DECODIFICHE: NO INCAPSULAMENTO
  '''''''''''''''''''''''''''''''''''''''''''''''''
  If decodesOnly Or FullIncaps Then  ' <UNIFICAINCAPS>: aggiunta condizione in or
    '------------- TMP TMP TMP
    'vresp = MsgBox("ATTENZIONE!! Volete incapsulare con il nuovo metodo (nome del dbd inserito nella decodifica e perdita della compatibilit� con le vecchie decodifiche)?", vbYesNo, "Tipo di decodifica")
    'If vresp = vbYes Then
      isOldIncapsMethod = False
    'Else
      'isOldIncapsMethod = True
    'End If
    '--------------------------------------------------------
    createDecodes
    If decodesOnly Then
      Exit Sub
    End If
  End If

  ReDim arayPGM(0)
  Screen.MousePointer = vbHourglass
  RTErr.text = ""

  Select Case Button_selection.key
    Case "Start"
      '------------- TMP TMP TMP
      'vresp = MsgBox("ATTENZIONE!! Volete incapsulare con il nuovo metodo (nome del dbd inserito nella decodifica e perdita della compatibilit� con le vecchie decodifiche)?", vbYesNo, "Tipo di decodifica")
      'If vresp = vbYes Then
      isOldIncapsMethod = False
      'Else
        'isOldIncapsMethod = True
      'End If

      typeIncaps = "RDBMS"
      If LstFunction.Selected(2) Then
        swCall = True
      ElseIf LstFunction.Selected(3) Then
        swCall = False
      End If

      If LstFunction.Selected(1) Then
        MsgBox "Please use the botton at the top of the window"
'        If MsgBox("Warning. You are resetting all DECODING: you could need to INCAPSULATE the sources again. " & vbCrLf & "Are you sure?", vbYesNo + vbQuestion + vbCritical, "i-4.Migration: Encapsulator") = vbYes Then
'          m_fun.FnConnection.Execute "DELETE * From MgDli_decodificaIstr Where ImsDB = true"
'        End If
      End If

      '''''''''''''''''''''''''''''''''''''''''''''
      ' SELEZIONE PROGRAMMI: in rIst
      '''''''''''''''''''''''''''''''''''''''''''''
      Selezione = Restituisci_Selezione(TVSelObj)
      'SQ - HA SENSO PARTIRE DALLA BS_OGGETTI? FORSE CI SONO PGM SENZA ISTR (neanche in CPY) da MODIFICARE UGUALMENTE. CHIARIRE
      Dim criterio As String, criterioU As String
      Select Case Selezione
        Case "'SEL'" 'Solo i programmi Selezionati
          For i = 1 To Dli2Rdbms.drObjList.ListItems.Count
            If Dli2Rdbms.drObjList.ListItems(i).Selected Then
              If Trim(criterio) <> "" Then
                criterio = criterio & ", " & Val(Dli2Rdbms.drObjList.ListItems(i).text)
              Else
                criterio = Val(Dli2Rdbms.drObjList.ListItems(i).text)
              End If
            End If
          Next i
          If Len(Trim(criterio)) = 0 Then
            'dare messaggio
            Exit Sub
          End If
          criterioU = " a.IdOggetto in (" & criterio & ") "
          criterio = " IdOggetto in (" & criterio & ") "
          ''''''''''''''''''''''''''''''''''''
          'PGM con DLI a programma o in COPY: LA UNION "DEVE" ESSERE SUPERFLUA: ANCHE SE IL DLI E' IN COPY, IL PGM DEVE ESSERE MARCHIATO "DLI"!!!!!!!
          ''''''''''''''''''''''''''''''''''''
         Set rIst = m_fun.Open_Recordset("SELECT idoggetto,tipo,cics FROM BS_Oggetti WHERE " & _
                                         criterio & " AND Dli Order By IdOggetto" _
            & " UNION SELECT a.idoggetto as IdOggetto,a.tipo as tipo,a.cics as cics FROM BS_Oggetti as a,PsRel_Obj as b,BS_Oggetti as c WHERE " & _
            criterioU & " And not a.Dli and b.IdOggettoC = a.IdOggetto and (b.Utilizzo = 'COPY' or b.Utilizzo = 'PLI-INCLUDE') and c.IdOggetto = b.IdOggettoR and c.DLi Order By IdOggetto")
        Case Else 'Tutti i programmi
          Set rIst = m_fun.Open_Recordset("Select idoggetto,tipo,cics From BS_Oggetti Where " & _
            "Dli and tipo in (" & Selezione & ") Order By IdOggetto" _
            & " UNION Select a.idoggetto as IdOggetto,a.tipo as tipo,a.cics as cics From BS_Oggetti as a,PsRel_Obj as b,BS_Oggetti as c where " & _
            " not a.Dli and a.tipo in (" & Selezione & ") and b.IdOggettoC = a.IdOggetto and (b.Utilizzo = 'COPY' or b.Utilizzo = 'PLI-INCLUDE') and c.IdOggetto = b.IdOggettoR and c.DLi Order By IdOggetto")
      End Select

      PBar.Max = rIst.RecordCount
      PBar.Value = 0
      PbarIstr.Max = 1
      PbarIstr.Value = 0

      'SQ !!!!!!!!!!!!! sotto viene RIPULITO!!!!!!!!!!!!!!!!!!
      'Se arrivo dal meno IMS/DB to VSAM setto la directory per recuperare i template
      If fromMenu = "VSAM" Then
        dirincaps = "\VSAM"
      Else
        dirincaps = ""
      End If
      WMId = 0
      While Not rIst.EOF
        seqInstr = 0
        'WMId = 0
        isPLIcommentOpened = False
        ReDim FItable(0)
        '***********
        OldName = ""
        TipoFile = rIst!tipo
        If IsAlwaysBatch Then
          SwTp = False
        Else
          SwTp = rIst!CICS
        End If
        upentry = False
        startPLIkey = 1
        ReDim routinePLI(0)
        ReDim arrayPGM(0)
        ReDim LNKDBNumstack(0)

        'se agisco sul sorgente originale azzero gli offset
        If Optsource Then
          m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = 0 where idoggetto = " & rIst!IdOggetto
        End If

        If Not dirincaps = "\VSAM" Then
          dirincaps = ""
        End If
        If TipoFile = "PLI" Then
          dirincaps = "\PLI"
        End If

        'Analizza e inserisce le varie istruzioni e chiamate nel CBL.
        'Per� non tocca il CBL originale, ma lo copia, modificato, nella directory: CBLOut o CPYOut

        'NUOVA VERSIONE OF
        
        'WALLMART - Feb 2010
        
        If WMIncaps(rIst!IdOggetto) Then
          WM_CBL_COMMON_COPYBOOK rIst, typeIncaps
          isWALMARTpgm = True
        Else
          Controlla_CBL_CPY_OF RTBModifica, RTErr, rIst, typeIncaps  'carica o salva il file in base al fatto che sia o meno gi� caricato
          isWALMARTpgm = False
        End If
        'TO DO ---> verificare se Entry richiede riga su FItable

        '''''''''''''''''''''''''''''''''''''''''''''''''
        ' ELIMINAZIONE elementi dalla PsDli_IstrCodifica
        ' !attenzione... rimangono "orfani" nella MgDli_Decodifica...
        '''''''''''''''''''''''''''''''''''''''''''''''''
        'AC __________________________
        'incapsulamento senza decodifica non cancella decodifiche
        If Not Nodecode And Not FullIncaps Then
          m_fun.FnConnection.Execute "DELETE FROM PsDli_IstrCodifica Where IdOggetto = " & rIst!IdOggetto
        End If
        
        '______________________________________________
        isUnificated = True '<UNIFICAINCAPS>: d'ora in poi ci serve il ramo che non ricalcola decodifiche
                        ' manteniamo anche il vecchio ramo, quello che si ricalcola le decodifiche
                        ' anche se non ci entriamo mai.
        '---------------------------------------------

        Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE " & _
                                       "idoggetto=" & rIst!IdOggetto & " AND idPgm <> 0 ORDER BY riga") 'And not ImsDC
        If (TipoFile = "CPY" Or TipoFile = "INC") And Not rst.EOF Then
          'SQ 9-11-07 - baco... TMP
          rst.Close
          Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE " & _
                                         "idoggetto=" & rIst!IdOggetto & " AND idPgm <> 0 AND to_migrate")
          idPgmCpy = rst!idpgm
          rst.Close
          Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE " & _
                                         "idoggetto = " & rIst!IdOggetto & " AND idPgm = " & idPgmCpy & _
                                         " ORDER BY riga") 'And not ImsDC
          'SQ 9-11-07 - baco... TMP
          Set rsPgm = m_fun.Open_Recordset("SELECT distinct IdPGM FROM PSDli_Istruzioni WHERE " & _
                                           "idoggetto=" & rIst!IdOggetto & " AND to_migrate and valida")  'And not ImsDC
          While Not rsPgm.EOF
            If Not rsPgm!idpgm = 0 Then
              ReDim Preserve arrayPGM(UBound(arrayPGM) + 1)
              arrayPGM(UBound(arrayPGM)) = rsPgm!idpgm
            End If
            rsPgm.MoveNext
          Wend
          rsPgm.Close
        Else
          ' potenziali danni --- serve per move del nome programma nella var WS-Caller
          ' (solo se istruzioni in copy)
          If istrIncopy(rIst!IdOggetto) Then    'SQ c'� gi� sopra!!!
            If Not TipoFile = "PLI" Then
              InsertMovePgmName RTBModifica, namePgmFromId(rIst!IdOggetto)
            End If
          End If
          'ReDim arrayPGM(1)
        End If

        j = 0
        ' 3) inserimento delle istruzioni:
        If rst.RecordCount Then
          PbarIstr.Max = rst.RecordCount
        End If

        'pcb dinamico
        ReDim OpeIstrRt(0)

        Set distinctCod = New Collection
        Set distinctSeg = New Collection
        Set distinctDB = New Collection
        '''''''''''''''''''
        'CICLO ISTRUZIONI
        '''''''''''''''''''
        '*********************** AC *********************************
        ReDim VItable(0)  ' per l'incapsulamento delle istruzioni
        
        FDin = FreeFile
        lineNumber = 0
        filenameI = DirCBLcpy & "\" & NCBLcpyOF
        isFileNotFound = False
        Open filenameI For Input As FDin
        
        Do While Not rst.EOF
          If isFileNotFound Then
            Exit Do
          End If
          '****************************  WALMART  ****************************
          IsWMIncaps = False
          If isWALMARTpgm And isIstrDli(rst!IdOggetto, rst!Riga) Then    ' TODO if WM and .....
            IsWMIncaps = True
            While Not (EOF(FDin) Or lineNumber = rst!Riga - 1)
              Line Input #FDin, Line
              lineNumber = lineNumber + 1
            Wend
            lineprec = Line  ' per vedere se ci sono if/else
            If Not EOF(FDin) Then
              Line Input #FDin, Line  'linea dell'istruzione
              lineNumber = lineNumber + 1
            'Else
              'gestire fine file inatteso
            End If
          
            If toMigrate(rst!IdOggetto, rst!Riga, rst!to_migrate, arrayPGM, arrayPGMInfo) Then
              posizione_DB = AsteriscaIstruzioneOF(rst, Line, lineprec, typeIncaps, rst!isExec)
            Else
              posizione_DB = 0
            End If
            istrType = WM_Istr_TO_INCAPS(rst!Istruzione)
            If istrType = 1 Or istrType = 2 Then
              IncapsWM rst, arrayPGM, arrayPGMInfo, posizione_DB, istrType, isExec
            End If
          Else
          '*********** RAMO PRINCIPALE ************
             '********************************************************************
          
          
          'Verificare se serve ancora offsetMacro
          '********************************************
          Set rsoffset = m_fun.Open_Recordset("Select * from PS_objRowOffset where " & _
                                              "idoggetto = " & rst!IdOggetto & " and riga =" & rst!Riga)
          If Not rsoffset.EOF Then
            totaloffset = IIf(IsNull(rsoffset!offsetMacro), 0, rsoffset!offsetMacro) + rsoffset!offsetEncaps
          Else
            rsoffset.Close
            Set rsoffset = m_fun.Open_Recordset("Select * from PS_objRowOffset")
            rsoffset.AddNew
            rsoffset!IdOggetto = rst!IdOggetto
            rsoffset!Riga = rst!Riga
          End If
          '*******************************************

          If Not rst!ImsDC Then
            If rst!Riga > rigaEntry And Not upentry Then 'IRIS-DB per caso va bene anche quando non c'� la Entry come nelle EXEC DLI
              updateEntryOff rIst!IdOggetto, rigaEntry, AddRighe
              upentry = True
            End If
            wPunto = False

            Dim rsCall As Recordset
            'Valorizziamo wPunto (se presente diventa il terminatore dell'istruzione)
            '****************************** WPUNTO ******************
            'IRIS-DB Sembra che il parser non setti correttamente il flag che dice che � una CBLTDLI. Girata la IF per mettere il default a CBLTDLI
'IRIS-DB            If rst!isCBL Then
'IRIS-DB              Set rsCall = m_fun.Open_Recordset("Select * from PSCall where " & _
'IRIS-DB                                                "idoggetto = " & rst!IdOggetto & " And Riga = " & rst!Riga)
'IRIS-DB              If Not rsCall.EOF Then
'IRIS-DB                If InStr(rsCall!parametri, ".") Then wPunto = True
'IRIS-DB              End If
'IRIS-DB              rsCall.Close
'IRIS-DB            ElseIf rst!isExec And Not dirincaps = "\PLI" Then
'IRIS-DB              Set rsCall = m_fun.Open_Recordset("Select Dot from PSExec where " & _
'IRIS-DB                                                "idoggetto = " & rst!IdOggetto & " And Riga = " & rst!Riga)
'IRIS-DB              If Not rsCall.EOF Then
'IRIS-DB                 wPunto = rsCall!Dot
'IRIS-DB              End If
'IRIS-DB              rsCall.Close
'IRIS-DB            End If
            If rst!isExec And Not dirincaps = "\PLI" Then
              Set rsCall = m_fun.Open_Recordset("Select Dot from PSExec where " & _
                                                "idoggetto = " & rst!IdOggetto & " And Riga = " & rst!Riga)
              If Not rsCall.EOF Then
                 wPunto = rsCall!Dot
              End If
              rsCall.Close
            Else
              Set rsCall = m_fun.Open_Recordset("Select * from PSCall where " & _
                                                "idoggetto = " & rst!IdOggetto & " And Riga = " & rst!Riga)
              If Not rsCall.EOF Then
                If InStr(rsCall!parametri, ".") Then wPunto = True
              End If
              rsCall.Close
            End If
            '**********************************************************
            isPLI = False
            If dirincaps = "\PLI" Then
              dirincaps = ""
            End If
            While Not (EOF(FDin) Or lineNumber = rst!Riga - 1)
              Line Input #FDin, Line
              lineNumber = lineNumber + 1
            Wend
            lineprec = Line  ' per vedere se ci sono if/else
            If Not EOF(FDin) Then
              Line Input #FDin, Line  'linea dell'istruzione
              lineNumber = lineNumber + 1
            'Else
              'gestire fine file inatteso
            End If
          
            If toMigrate(rst!IdOggetto, rst!Riga, rst!to_migrate, arrayPGM, arrayPGMInfo) Then
              Select Case TipoFile
                Case "EZT"
                  posizione_DB = AsteriscaIstruzione_EZT(totaloffset, rst, RTBModifica, typeIncaps)
  
                Case "PLI", "INC"
                  dirincaps = "\PLI"
                  isPLI = True
                  If PLIsaveBackKey = "RET" Then
                    startPLIkeyRet = startPLIkey
                    insertPLISaveBack "RETURN;", startPLIkeyRet, rst!Riga + AddRighe + totaloffset, RTBModifica
                    startPLIkeyRet = startPLIkey
                    insertPLISaveBack "RETURN(", startPLIkeyRet, rst!Riga + AddRighe + totaloffset, RTBModifica
                    startPLIkeyRet = startPLIkey
                    insertPLISaveBack "RETURN (", startPLIkeyRet, rst!Riga + AddRighe + totaloffset, RTBModifica
                  Else
                    insertPLISaveBack PLIsaveBackKey, startPLIkey, rst!Riga + AddRighe + totaloffset, RTBModifica
                  End If
                  'AC AsteriscaIstruzioneOF_PLI setta alcuni campi della VItable (ad es. la stringa target)
                  posizione_DB = AsteriscaIstruzioneOF_PLI(rst, Line, lineprec, typeIncaps, rst!isExec)
                  If posizione_DB > 0 Then
                    startPLIkey = posizione_DB
                  End If
                  'Ac temp
                  'posizione = 1
                Case Else
                  posizione_DB = AsteriscaIstruzioneOF(rst, Line, lineprec, typeIncaps, rst!isExec)
              End Select
            Else
              posizione_DB = 0
            End If
            If posizione_DB > 0 Then
              ' nel caso di copy e programmi chiamanti il controllo � successivo
              ' e dipende dal programma chiamante
              isValidOneCaller = False
              If UBound(arrayPGM) = 1 Then
                If arrayPGMInfo(1) Then
                  isValidOneCaller = True
                End If
              End If
              'IRIS-DB: usare le non valide come dinamiche
              'If (rst!valida And Not rst!obsolete) Or UBound(arrayPGM) > 1 Or isValidOneCaller Then
              If Not rst!obsolete Or UBound(arrayPGM) > 1 Or isValidOneCaller Then
                If Optsource Then
                  rsoffset!offsetEncaps = AddRighe
                Else
                  rsoffset!offsetEncaps = IIf(IsNull(rsoffset!offsetEncaps), 0, rsoffset!offsetEncaps) + AddRighe
                End If
                 
                If Trim(rst!Istruzione) = "QUERY" Then
                  wIstr = "QRY"
                Else
                  wIstr = Trim(rst!Istruzione)
                End If

                If TipoFile <> "EZT" Then
                  'AC 26/11/2007  arrayPGM anche per "INC"
                  If Not UBound(arrayPGM) > 1 Then
                    If UBound(arrayPGM) = 0 Then   ' programma
                      '->>>>>>***********  ENTRATA STANDARD ************<<<<<<<<-
                      CostruisciCommonInitializeTemplate_OF rst
                      If Not rst!isExec Then
                      'IRIS-DB gestione istruzione non valida come dinamica
                        If rst!valida Then 'IRIS-DB
                          If Nodecode Or isUnificated Then  ' <UNIFICAINCAPS> aggiunta condizione in or
                            CaricaDecodifiche rst
                            seqInstr = seqInstr + 1
                            CostruisciOperazioneTemplateNoDecode Trim(wIstr), 1, rst!idpgm, rst!IdOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
                          Else
                            ' Mauro 16/10/2007
                            CostruisciOperazioneTemplate_OF Trim(wIstr), rst!idpgm, rst!IdOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
                          End If
                        Else 'IRIS-DB
                          CostruisciOperazioneTemplateNoValid Trim(wIstr), rst!idpgm, rst!IdOggetto, rst!Riga 'IRIS-DB
                        End If 'IRIS-DB
                        If err.Number > 0 Then CostruisciOperazioneTemplate_OF Trim(wIstr), rst!idpgm, rst!IdOggetto, rst!Riga, 0 & rst!ordPcb
                      Else ' ->>>>>> ingresso exec
                        'AC 10/09708 _____________________
                        If Nodecode Or isUnificated Then ' <UNIFICAINCAPS> aggiunta condizione in or
                          CaricaDecodifiche rst
                          CostruisciOperazioneTemplateNoDecode_EXEC Trim(wIstr), 1, rst!idpgm, rst!IdOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
                        Else
                          CostruisciOperazioneTemplateOF_EXEC Trim(wIstr), 1, rst!idpgm, rst!IdOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
                        End If
                        If err.Number > 0 Then CostruisciOperazioneTemplateOF_EXEC Trim(wIstr), 1, rst!idpgm, rst!IdOggetto, rst!Riga, 0 & rst!ordPcb
                      End If
                      '***************************************************
                    Else ' copy chiamata da un solo programma
                      ' devo chiamare la stessa funzione COmmonInitialize dei programmi
                      ' ma la riga dove si valorizza il WS-Caller non deve essere scritta
                      ' manometto il nome del programma in modo che diventi un tag e la riga non venga
                      ' scritta, poi lo ripristino

                      'AC 27/11/2007
                      CaricaIstruzioneC rst, arrayPGM(1)
                      NomeProgappo = NomeProg
                      NomeProg = "<todelete>"
                      CostruisciCommonInitializeTemplate_OF rst
                      NomeProg = NomeProgappo
                      ' Mauro 16/10/2007
                      If Not rst!isExec Then
                        CostruisciOperazioneTemplate_OF Trim(wIstr), arrayPGM(1), rst!IdOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
                      Else
                        CostruisciOperazioneTemplateOF_EXEC Trim(wIstr), 1, arrayPGM(1), rst!IdOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
                      End If
                      If err.Number > 0 Then CostruisciOperazioneTemplate_OF Trim(wIstr), arrayPGM(1), rst!IdOggetto, rst!Riga, 0 & rst!ordPcb
                    End If
                    'IRIS-DB it passes the instruction sequence
                    INSERT_DECOD_OF posizione_DB, RTBModifica, RTErr, rst, typeIncaps, seqInstr
                  Else
                    'isTheSameCod = CheckArrayPgm(arrayPGM, rst!idOggetto, rst!Riga)
                    isTheSameCod = False
                    If isTheSameCod Then
                      CaricaIstruzioneC rst, arrayPGM(1)
                      NomeProgappo = NomeProg
                      NomeProg = "<todelete>"
                      CostruisciCommonInitializeTemplate_OF rst
                      NomeProg = NomeProgappo
                      ' Mauro 16/10/2007
                      If Not rst!isExec Then
                        CostruisciOperazioneTemplate_OF Trim(wIstr), arrayPGM(1), rst!IdOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
                      Else
                        CostruisciOperazioneTemplateOF_EXEC Trim(wIstr), 1, arrayPGM(1), rst!IdOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
                      End If
                      If err.Number > 0 Then CostruisciOperazioneTemplate_OF Trim(wIstr), arrayPGM(1), rst!IdOggetto, rst!Riga, 0 & rst!ordPcb
                      INSERT_DECOD posizione_DB, RTBModifica, RTErr, rst, typeIncaps
                    Else
                      codrif = ""
                      checkcod = True
                      firstIF = ""
                      Testo = ""
                      AddrighebefIstr = AddRighe
                      firstindex = True
                      counttomigrate = 0
                      For i = 1 To UBound(arrayPGM)
                        'rst!idpgm = arrayPGM(i)
                        If arrayPGMInfo(i) Then
                          counttomigrate = counttomigrate + 1
                          CaricaIstruzioneC rst, arrayPGM(i)
                          If Not i = UBound(arrayPGM) Then
                            INSERT_DECODC posizione_DB, RTBModifica, RTErr, rst, typeIncaps, i, arrayPGM(i)
                          Else
                            INSERT_DECODC posizione_DB, RTBModifica, RTErr, rst, typeIncaps, -1, arrayPGM(i)
                          End If
                          indentkey = indentkey - 3
                        End If
                      Next i
                      'RTBModifica.SelStart = wPos
                      RTBModifica.SelStart = posizione_DB
                      RTBModifica.SelLength = 0
                      RTBModifica.SelText = Testo
                      'wPos = wPos + Len(Testo)
                      posizione_DB = posizione_DB + Len(Testo)
                    End If
                  End If
                End If
                lastPos = posizione_DB
              Else
                If TipoFile <> "EZT" Then
                  INSERT_NODECOD_OF rst
                  If rst!obsolete = False Then ' IRIS-DB solo le non obsolete devono segnalare errore
                    MadrdM_GenRout.AggLog "[Object: " & rst!IdOggetto & "-Row( " & rst!Riga & ")]-warning: not valid instruction ", RTErr
                  End If
                  lastPos = posizione_DB
                End If
              End If
            Else
              'SQ???
              If rst!to_migrate Then
                'SQ gi� segnalato dalla routine chiamata...
                'm_fun.WriteLog "Instruction not found: Obj(" & rIst!idOggetto & ") Row( " & rst!Riga & ")", "MadrdF_Incapsulatore"
              End If
            End If
          Else
            If Optsource Then
              rsoffset!offsetEncaps = AddRighe
            Else
              rsoffset!offsetEncaps = IIf(IsNull(rsoffset!offsetEncaps), 0, rsoffset!offsetEncaps) + AddRighe
            End If
          End If
          rsoffset.Update
          rsoffset.Close
          
    End If
        rst.MoveNext

        PbarIstr.Value = PbarIstr.Value + 1
        Loop
        
        Close (FDin)
        
        If TipoFile = "PLI" Or TipoFile = "INC" Then
          'AC 17/12/2008
          insertREV rIst!IdOggetto, TestoREV, RTBModifica
        End If
        If TipoFile = "PLI" Then
          'AC 09/01/08
          ' per le external entry consideriamo anche le routine chiamate da copy
          upadateArrayNomeRoutinePLI rIst!IdOggetto
          insertExternalEntry_OF rIst!IdOggetto, RTErr
        End If

        If EmbeddedRoutine Then
          Insert_ProcedureTemplate RTBModifica, RTErr
          Insert_TagRoutine RTBModifica
        End If
        '---------------------------
        
        PbarIstr.Value = 0

        With LvObject.ListItems.Add(, , rIst!IdOggetto)
          .ToolTipText = "Double click for compare with original source"
          .SubItems(1) = Replace(OldName, m_fun.FnPathPrj, "...")
          .SubItems(2) = Now
        End With

        LvObject.ListItems(LvObject.ListItems.Count).Selected = True
        LvObject.Refresh
        LvObject.SelectedItem.EnsureVisible
        TransformFile_OF DirCBLcpy & "\" & NCBLcpyOF, OldName
        RTBModifica.LoadFile OldName 'IRIS-DB
        EncapsENTRYTDLI RTBModifica, rIst!IdOggetto 'IRIS-DB
        RTBModifica.SaveFile OldName, 1 'IRIS-DB
        EncapsENTRYTDLIPLI RTBModifica, rIst!IdOggetto 'IRIS-DB
        RTBModifica.SaveFile OldName, 1 'IRIS-DB
        'IRIS-DB gestione brutale solo per DSHS
        If Left(m_fun.FnNomeDB, 4) = "DSHS" Then 'IRIS-DB
        'IRIS-DB gestione speciale per i batch EXEC DLI che non hanno n� Entry, n� Sched esplicita
          RTBModifica.LoadFile OldName 'IRIS-DB
          EncapsSCHEDEXECDLI RTBModifica, rIst!IdOggetto 'IRIS-DB
          RTBModifica.SaveFile OldName, 1 'IRIS-DB
          RTBModifica.LoadFile OldName 'IRIS-DB
          EncapsGOBACK RTBModifica, rIst!IdOggetto 'IRIS-DB
          RTBModifica.SaveFile OldName, 1 'IRIS-DB
          RTBModifica.LoadFile OldName 'IRIS-DB
          EncapsRETURN RTBModifica, rIst!IdOggetto 'IRIS-DB
          RTBModifica.SaveFile OldName, 1 'IRIS-DB
          RTBModifica.LoadFile OldName 'IRIS-DB
          EncapsRETURNTRANSID RTBModifica, rIst!IdOggetto 'IRIS-DB
          RTBModifica.SaveFile OldName, 1 'IRIS-DB
          RTBModifica.LoadFile OldName 'IRIS-DB
          EncapsXCTL RTBModifica, rIst!IdOggetto 'IRIS-DB
          RTBModifica.SaveFile OldName, 1 'IRIS-DB
          RTBModifica.LoadFile OldName 'IRIS-DB
          EncapsABEND RTBModifica, rIst!IdOggetto 'IRIS-DB
          RTBModifica.SaveFile OldName, 1 'IRIS-DB
        End If 'IRIS-DB

        rIst.MoveNext

        If PBar.Max < PBar.Value + 1 Then
          PBar.Value = PBar.Max
        Else
          PBar.Value = PBar.Value + 1
        End If
      Wend
      rIst.Close
      'If Not OldName = "" Then
        'RTBModifica.SaveFile OldName, 1
      'End If
      PBar.Value = 0

    Case "Close"
      Unload Me

  End Select

  Screen.MousePointer = vbDefault
  Exit Sub
errors:
  If err.Number = 53 Then 'file not found
    isFileNotFound = True
    Resume Next
  End If
  Resume Next
End Sub
Public Sub IncapsWM(rst As Recordset, arrayPGM() As Long, arrayPGMInfo() As Boolean, posizione As Long, ptype As Integer, pIsExec As Boolean)
'*******************************************

   Dim isValidOneCaller As Boolean
   Dim wIstr As String, lastPos As Long
            '**********************************************************
            
            If posizione > 0 Then
              ' nel caso di copy e programmi chiamanti il controllo � successivo
              ' e dipende dal programma chiamante
              isValidOneCaller = False
              If UBound(arrayPGM) = 1 Then
                If arrayPGMInfo(1) Then
                  isValidOneCaller = True
                End If
              End If
              If (rst!valida And Not rst!obsolete) Or UBound(arrayPGM) > 1 Or isValidOneCaller Then
                
                 
                If Trim(rst!Istruzione) = "QUERY" Then
                  wIstr = "QRY"
                Else
                  wIstr = Trim(rst!Istruzione)
                End If
                CostruisciAll_WM_OF rst, ptype, pIsExec
                  'INSERT_DECOD_OF posizione, RTBModifica, RTErr, rst, "CBL"
                lastPos = posizione
              Else
                  INSERT_NODECOD_OF rst
                  MadrdM_GenRout.AggLog "[Object: " & rst!IdOggetto & "-Row( " & rst!Riga & ")]-warning: not valid instruction ", RTErr
                  lastPos = posizione
               
              End If
            Else
              'SQ???
              If rst!to_migrate Then
                'SQ gi� segnalato dalla routine chiamata...
                'm_fun.WriteLog "Instruction not found: Obj(" & rIst!idOggetto & ") Row( " & rst!Riga & ")", "MadrdF_Incapsulatore"
              End If
            End If
        

End Sub

' Vecchio Metodo
'''Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
'''''****************** TEMP TEMP **********
''' Tollbar1_ButtonClick_OF Button
''' Exit Sub
'''''***************** TEMP TEMP **********
'''  Dim rst As Recordset, rIst As Recordset, rstCloni As Recordset, rsOffset As Recordset
'''  Dim typeIncaps As String, wIstr As String, Selezione As String, lastPos As Long
'''  Dim MaxVal As Long
'''  Dim nomeRoutine As String, Totaloffset As Long, rsEntry As Recordset, rigaentry As Long, upentry As Boolean
'''  Dim idpgmcpy As Long
'''  ' AC da questo array discriminiamo copy da programmi
'''  Dim arrayPGM() As Long
'''  Dim arrayPGMInfo() As Boolean
'''  Dim rsPgm As Recordset, i As Integer, startPLIkey As Long, startPLIkeyRet As Long
'''  Dim NomeProgappo As String, offsetentry As Long
'''  Dim isValidOneCaller As Boolean, isTheSameCod As Boolean
'''  Dim vresp As Integer
'''
'''  On Error Resume Next
'''
'''  ' Mauro 18/11/2009
'''  isUnificaGH = IIf(UCase(m_fun.LeggiParam("IMSDB-GH")) = "YES", True, False)
'''  '''''''''''''''''''''''''''''''''''''''''''''''''
'''  'SQ 30-10-07
'''  ' SOLO DECODIFICHE: NO INCAPSULAMENTO
'''  '''''''''''''''''''''''''''''''''''''''''''''''''
'''  If decodesOnly Then
'''    '------------- TMP TMP TMP
'''    'vresp = MsgBox("ATTENTION!! Do you want to encapsulate with the new method (name of dbd included in decoding and loss of compatibility with the old decoding)?", vbYesNo, Dli2Rdbms.drNomeProdotto)
'''    'If vresp = vbYes Then
'''      isOldIncapsMethod = False
'''    'Else
'''      'isOldIncapsMethod = True
'''    'End If
'''    '--------------------------------------------------------
'''    createDecodes
'''    Exit Sub
'''  End If
'''
'''  ReDim arayPGM(0)
'''  Screen.MousePointer = vbHourglass
'''  RTErr.text = ""
'''  'RTB.text = ""
'''
'''  Select Case Button.key
'''    Case "Start"
'''      '------------- TMP TMP TMP
'''      'vresp = MsgBox("ATTENTION!! Do you want to encapsulate with the new method (name of dbd included in decoding and loss of compatibility with the old decoding)?", vbYesNo, Dli2Rdbms.drNomeProdotto)
'''      'If vresp = vbYes Then
'''        isOldIncapsMethod = False
'''      'Else
'''      '  isOldIncapsMethod = True
'''      'End If
'''      '--------------------------------------------------------
'''      'ora l'incapsulamento � unico: sempre RDBMS, che � quello corretto
'''      'TypeIncaps = "DLI"
'''      typeIncaps = "RDBMS"
'''
'''      If LstFunction.Selected(2) Then
'''        swCall = True
'''      ElseIf LstFunction.Selected(3) Then
'''        swCall = False
'''      End If
'''
'''      If LstFunction.Selected(1) Then
'''        If MsgBox("Warning. You are resetting all DECODING: you could need to INCAPSULATE the sources again. " & vbCrLf & "Are you sure?", vbYesNo + vbQuestion + vbCritical, "i-4.Migration: Encapsulator") = vbYes Then
'''          m_fun.FnConnection.Execute "DELETE * From MgDli_decodificaIstr Where ImsDB = true"
'''        End If
'''      End If
'''
'''      '''''''''''''''''''''''''''''''''''''''''''''
'''      ' SELEZIONE PROGRAMMI: in rIst
'''      '''''''''''''''''''''''''''''''''''''''''''''
'''      Selezione = Restituisci_Selezione(TVSelObj)
'''      'SQ - HA SENSO PARTIRE DALLA BS_OGGETTI? FORSE CI SONO PGM SENZA ISTR (neanche in CPY) da MODIFICARE UGUALMENTE. CHIARIRE
'''      Dim criterio As String, criterioU As String
'''      Select Case Selezione
'''        Case "'SEL'" 'Solo i programmi Selezionati
'''          For i = 1 To Dli2Rdbms.drObjList.ListItems.Count
'''            If Dli2Rdbms.drObjList.ListItems(i).Selected Then
'''              If Trim(criterio) <> "" Then
'''                criterio = criterio & ", " & Val(Dli2Rdbms.drObjList.ListItems(i).text)
'''              Else
'''                criterio = Val(Dli2Rdbms.drObjList.ListItems(i).text)
'''              End If
'''            End If
'''          Next i
'''          If Len(Trim(criterio)) = 0 Then
'''            'dare messaggio
'''            Exit Sub
'''          End If
'''          criterioU = " a.IdOggetto in (" & criterio & ") "
'''          criterio = " IdOggetto in (" & criterio & ") "
'''          ''''''''''''''''''''''''''''''''''''
'''          'PGM con DLI a programma o in COPY: LA UNION "DEVE" ESSERE SUPERFLUA: ANCHE SE IL DLI E' IN COPY, IL PGM DEVE ESSERE MARCHIATO "DLI"!!!!!!!
'''          ''''''''''''''''''''''''''''''''''''
'''          Set rIst = m_fun.Open_Recordset("SELECT idoggetto,tipo,cics FROM BS_Oggetti WHERE " & _
'''            criterio & " AND Dli Order By IdOggetto" _
'''            & " UNION SELECT a.idoggetto as IdOggetto,a.tipo as tipo,a.cics as cics FROM BS_Oggetti as a,PsRel_Obj as b,BS_Oggetti as c WHERE " & _
'''            criterioU & " And not a.Dli and b.IdOggettoC = a.IdOggetto and (b.Utilizzo = 'COPY' or b.Utilizzo = 'PLI-INCLUDE') and c.IdOggetto = b.IdOggettoR and c.DLi Order By IdOggetto")
'''        Case Else 'Tutti i programmi
'''          Set rIst = m_fun.Open_Recordset("Select idoggetto,tipo,cics From BS_Oggetti Where " & _
'''            "Dli and tipo in (" & Selezione & ") Order By IdOggetto" _
'''            & " UNION Select a.idoggetto as IdOggetto,a.tipo as tipo,a.cics as cics From BS_Oggetti as a,PsRel_Obj as b,BS_Oggetti as c where " & _
'''            " not a.Dli and a.tipo in (" & Selezione & ") and b.IdOggettoC = a.IdOggetto and (b.Utilizzo = 'COPY' or b.Utilizzo = 'PLI-INCLUDE') and c.IdOggetto = b.IdOggettoR and c.DLi Order By IdOggetto")
'''      End Select
'''
'''      PBar.Max = rIst.RecordCount
'''      PBar.Value = 0
'''      PbarIstr.Max = 1
'''      PbarIstr.Value = 0
'''
'''      'SQ !!!!!!!!!!!!! sotto viene RIPULITO!!!!!!!!!!!!!!!!!!
'''      'Se arrivo dal meno IMS/DB to VSAM setto la directory per recuperare i template
'''      If fromMenu = "VSAM" Then
'''        dirincaps = "\VSAM"
'''      Else
'''        dirincaps = ""
'''      End If
'''
'''      While Not rIst.EOF
'''        OldName = ""
'''        TipoFile = rIst!tipo
'''        If IsAlwaysBatch Then
'''          SwTp = False
'''        Else
'''          SwTp = rIst!Cics
'''        End If
'''        upentry = False
'''        startPLIkey = 1
'''        ReDim routinePLI(0)
'''        ReDim arrayPGM(0)
'''        ReDim LNKDBNumstack(0)
'''        ' Mauro 28/04/2009 : Pcb dinamico
'''        ReDim OpeIstrRt(0)
'''
'''        'se agisco sul sorgente originale azzero gli offset
'''        If Optsource Then
'''          m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = 0 where idoggetto = " & rIst!idOggetto
'''        End If
'''
'''        dirincaps = ""  'SQ??? � settato sopra (\VSAM)
'''        If TipoFile = "PLI" Then
'''          dirincaps = "\PLI"
'''        End If
'''
'''        'Analizza e inserisce le varie istruzioni e chiamate nel CBL.
'''        'Per� non tocca il CBL originale, ma lo copia, modificato, nella directory: CBLOut o CPYOut
'''        Controlla_CBL_CPY RTBModifica, RTErr, rIst, typeIncaps  'carica o salva il file in base al fatto che sia o meno gi� caricato
'''
'''        Set rsEntry = m_fun.Open_Recordset("SELECT * FROM PsPgm WHERE idoggetto = " & rIst!idOggetto & " and comando ='ENTRY'")
'''        If Not rsEntry.EOF Then
'''          rigaentry = rsEntry!Riga
'''        End If
'''        rsEntry.Close
'''
'''        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''        ' SQ - La gestione della PS_objRowOffset NON deve essere fatta cos�!
'''        '      Cos� � ingestibile... oltre che scorretta...
'''        ' Ci vanno le righe aggiunte nella "riga" in cui sono state aggiunte!, non le istruzioni
'''        ' Cos� devo aggiungere il record della Entry, se non IMSDC sbaglia!!!!!!!!!!!!!!!!!
'''        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''        'pezza: lo devo fare sempre... pulire sotto
'''        If istrIncopy(rIst!idOggetto) Then
'''          offsetentry = AddRighe + 1
'''        Else
'''          offsetentry = AddRighe
'''        End If
'''        If rigaentry Then
'''          updateEntryOff rIst!idOggetto, rigaentry, offsetentry
'''          upentry = True
'''        End If
'''        '''m_fun.FnConnection.Execute "update ps_objrowoffset set offsetencaps = offsetencaps + " & AddRighe & " Where idoggetto = " & rIst!idOggetto
'''        'SP 4/9: intabella i PCB
'''        'CreaTabPCB RTBModifica, rIst!IdOggetto
'''
'''        '''''''''''''''''''''''''''''''''''''''''''''''''
'''        ' ELIMINAZIONE elementi dalla PsDli_IstrCodifica
'''        ' !attenzione... rimangono "orfani" nella MgDli_Decodifica...
'''        '''''''''''''''''''''''''''''''''''''''''''''''''
'''        'AC __________________________
'''        'incapsulamento senza decodifica non cancella decodifiche
'''        If Not Nodecode Then
'''          m_fun.FnConnection.Execute "DELETE FROM PsDli_IstrCodifica Where IdOggetto = " & rIst!idOggetto
'''        End If
'''
'''        'SP corso : per ora non escludo quelle IMSDC, quindi le asterisco
'''        'SQ: NON FUNZIONANO LE OBSOLETE UNKNOWN...
'''        'Set rst = m_fun.Open_Recordset("Select * from PSDli_Istruzioni where idoggetto = " & rIst!IdOggetto & " And ImsDB order by riga")
'''        'SQ CPY-ISTR
'''        'Per le copy: idPgm=0 da scartare: record "matrice"
'''        Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE " & _
'''                                        "idoggetto=" & rIst!idOggetto & " AND idPgm <> 0 ORDER BY riga") 'And not ImsDC
'''        If (TipoFile = "CPY" Or TipoFile = "INC") And Not rst.EOF Then
'''          'SQ 9-11-07 - baco... TMP
'''          rst.Close
'''          Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE idoggetto=" & rIst!idOggetto & " AND idPgm <> 0 AND to_migrate")
'''          idpgmcpy = rst!idPgm
'''          rst.Close
'''          Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE " & _
'''                                          "idoggetto=" & rIst!idOggetto & " AND idPgm = " & idpgmcpy & _
'''                                          " ORDER BY riga") 'And not ImsDC
'''          'SQ 9-11-07 - baco... TMP
'''          Set rsPgm = m_fun.Open_Recordset("SELECT distinct IdPGM FROM PSDli_Istruzioni WHERE " & _
'''                                           "idoggetto=" & rIst!idOggetto & " AND to_migrate and valida")  'And not ImsDC
'''          While Not rsPgm.EOF
'''            If Not rsPgm!idPgm = 0 Then
'''              ReDim Preserve arrayPGM(UBound(arrayPGM) + 1)
'''              arrayPGM(UBound(arrayPGM)) = rsPgm!idPgm
'''            End If
'''            rsPgm.MoveNext
'''          Wend
'''        Else
'''          ' potenziali danni --- serve per move del nome programma nella var WS-Caller
'''          ' (solo se istruzioni in copy)
'''          If istrIncopy(rIst!idOggetto) Then    'SQ c'� gi� sopra!!!
'''            If Not TipoFile = "PLI" Then
'''              InsertMovePgmName RTBModifica, namePgmFromId(rIst!idOggetto)
'''            End If
'''          End If
'''          'ReDim arrayPGM(1)
'''        End If
'''
'''        j = 0
'''        ' 3) inserimento delle istruzioni:
'''        If rst.RecordCount Then
'''          PbarIstr.Max = rst.RecordCount
'''        End If
'''
'''        'pcb dinamico
'''        ReDim OpeIstrRt(0)
'''
'''        Set distinctCod = New Collection
'''        Set distinctSeg = New Collection
'''        Set distinctDB = New Collection
'''        '''''''''''''''''''
'''        'CICLO ISTRUZIONI
'''        '''''''''''''''''''
'''        While Not rst.EOF
'''          Set rsOffset = m_fun.Open_Recordset("Select * from PS_objRowOffset where " & _
'''                                              "idoggetto = " & rst!idOggetto & " and riga =" & rst!Riga)
'''          If Not rsOffset.EOF Then
'''            Totaloffset = IIf(IsNull(rsOffset!offsetMacro), 0, rsOffset!offsetMacro) + rsOffset!OffsetEncaps
'''          Else
'''            rsOffset.Close
'''            Set rsOffset = m_fun.Open_Recordset("Select * from PS_objRowOffset")
'''            rsOffset.AddNew
'''            rsOffset!idOggetto = rst!idOggetto
'''            rsOffset!Riga = rst!Riga
'''          End If
'''          If Not rst!ImsDC Then
'''            If rst!Riga > rigaentry And Not upentry Then
'''              updateEntryOff rIst!idOggetto, rigaentry, AddRighe
'''              upentry = True
'''            End If
'''            wPunto = False
'''            'stefano: nuova gestione
'''            'SQ OK, comunque per Asteriscare leggiamo il file (e l'eventuale punto!)
'''            'If InStr(rst!Statement, ".") > 0 Then wPunto = True
'''            Dim rsCall As Recordset
'''
'''            If rst!isCBL Then
'''              Set rsCall = m_fun.Open_Recordset("Select * from PSCall where " & _
'''                                                         "idoggetto = " & rst!idOggetto & " And Riga = " & rst!Riga)
'''              If Not rsCall.EOF Then
'''                If InStr(rsCall!parametri, ".") Then wPunto = True
'''              End If
'''              rsCall.Close
'''            ElseIf rst!isExec And Not dirincaps = "\PLI" Then
'''              Set rsCall = m_fun.Open_Recordset("Select Dot from PSExec where " & _
'''                                                "idoggetto = " & rst!idOggetto & " And Riga = " & rst!Riga)
'''              If Not rsCall.EOF Then
'''                 wPunto = rsCall!Dot
'''              End If
'''              rsCall.Close
'''            End If
'''
'''            'SQ
'''            'else
'''              '????????????????
'''          'End If
'''          'SQ chiudere i recordset!
'''
'''          'If rst!to_migrate Then
'''          isPLI = False
'''          If dirincaps = "\PLI" Then
'''            dirincaps = ""
'''          End If
'''          If toMigrate(rst!idOggetto, rst!Riga, rst!to_migrate, arrayPGM, arrayPGMInfo) Then
'''            Select Case TipoFile
'''              Case "EZT"
'''                posizione = AsteriscaIstruzione_EZT(Totaloffset, rst, RTBModifica, typeIncaps)
'''              Case "PLI", "INC"
'''                dirincaps = "\PLI"
'''                isPLI = True
'''
'''                If PLIsaveBackKey = "RET" Then
'''                  startPLIkeyRet = startPLIkey
'''                  insertPLISaveBack "RETURN;", startPLIkeyRet, rst!Riga + AddRighe + Totaloffset, RTBModifica
'''                  startPLIkeyRet = startPLIkey
'''                  insertPLISaveBack "RETURN(", startPLIkeyRet, rst!Riga + AddRighe + Totaloffset, RTBModifica
'''                  startPLIkeyRet = startPLIkey
'''                  insertPLISaveBack "RETURN (", startPLIkeyRet, rst!Riga + AddRighe + Totaloffset, RTBModifica
'''                Else
'''                  insertPLISaveBack PLIsaveBackKey, startPLIkey, rst!Riga + AddRighe + Totaloffset, RTBModifica
'''                End If
'''
'''                posizione = AsteriscaIstruzione_PLI(Totaloffset, rst, RTBModifica, typeIncaps, rst!isExec)
'''                If posizione > 0 Then
'''                  startPLIkey = posizione
'''                End If
'''              Case Else
'''                posizione = AsteriscaIstruzione(Totaloffset, rst, RTBModifica, typeIncaps)
'''            End Select
'''          Else
'''            posizione = 0
'''          End If
'''          If posizione > 0 Then
'''             ' nel caso di copy e programmi chiamanti il controllo � successivo
'''             ' e dipende dal programma chiamante
'''             isValidOneCaller = False
'''             If UBound(arrayPGM) = 1 Then
'''              If arrayPGMInfo(1) Then
'''                isValidOneCaller = True
'''              End If
'''             End If
'''             If (rst!valida And Not rst!obsolete) Or UBound(arrayPGM) > 1 Or isValidOneCaller Then
'''                If Optsource Then
'''                  rsOffset!OffsetEncaps = AddRighe
'''                Else
'''                  rsOffset!OffsetEncaps = IIf(IsNull(rsOffset!OffsetEncaps), 0, rsOffset!OffsetEncaps) + AddRighe
'''                End If
'''
'''                If Trim(rst!Istruzione) = "QUERY" Then
'''                  wIstr = "QRY"
'''                Else
'''                  wIstr = Trim(rst!Istruzione)
'''                End If
'''
'''                If TipoFile <> "EZT" Then
'''                  'AC 26/11/2007  arrayPGM anche per "INC"
'''
'''                  If Not UBound(arrayPGM) > 1 Then
'''                    If UBound(arrayPGM) = 0 Then   ' programma
'''
'''                      '->>>>>>***********  ENTRATA STANDARD ************<<<<<<<<-
'''                      CostruisciCommonInitializeTemplate rst
'''                      If Not rst!isExec Then
'''                        If Nodecode Then
'''                          CaricaDecodifiche rst
'''                          CostruisciOperazioneTemplateNoDecode Trim(wIstr), 1, rst!idPgm, rst!idOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
'''                        Else
'''                          ' Mauro 16/10/2007
'''                          CostruisciOperazioneTemplate Trim(wIstr), 1, rst!idPgm, rst!idOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
'''                        End If
'''                        If err.Number > 0 Then CostruisciOperazioneTemplate Trim(wIstr), 1, rst!idPgm, rst!idOggetto, rst!Riga, 0 & rst!ordPcb
'''                      Else ' ->>>>>> ingresso exec
'''                        'AC 10/09708 _____________________
'''                        If Nodecode Then
'''                          CaricaDecodifiche rst
'''                          CostruisciOperazioneTemplateNoDecode_EXEC Trim(wIstr), 1, rst!idPgm, rst!idOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
'''                        Else
'''                          CostruisciOperazioneTemplate_EXEC Trim(wIstr), 1, rst!idPgm, rst!idOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
'''                        End If
'''                        If err.Number > 0 Then CostruisciOperazioneTemplate_EXEC Trim(wIstr), 1, rst!idPgm, rst!idOggetto, rst!Riga, 0 & rst!ordPcb
'''                      End If
'''                      '***************************************************
'''                    Else ' copy chiamata da un solo programma
'''                      ' devo chiamare la stessa funzione COmmonInitialize dei programmi
'''                      ' ma la riga dove si valorizza il WS-Caller non deve essere scritta
'''                      ' manometto il nome del programma in modo che diventi un tag e la riga non venga
'''                      ' scritta, poi lo ripristino
'''
'''                      'AC 27/11/2007
'''                      CaricaIstruzioneC rst, arrayPGM(1)
'''                      NomeProgappo = NomeProg
'''                      NomeProg = "<todelete>"
'''                      CostruisciCommonInitializeTemplate rst
'''                      NomeProg = NomeProgappo
'''                      ' Mauro 16/10/2007
'''                      If Not rst!isExec Then
'''                        CostruisciOperazioneTemplate Trim(wIstr), 1, arrayPGM(1), rst!idOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
'''                      Else
'''                        CostruisciOperazioneTemplate_EXEC Trim(wIstr), 1, arrayPGM(1), rst!idOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
'''                      End If
'''                      If err.Number > 0 Then CostruisciOperazioneTemplate Trim(wIstr), 1, arrayPGM(1), rst!idOggetto, rst!Riga, 0 & rst!ordPcb
'''                    End If
'''                    INSERT_DECOD posizione, RTBModifica, RTErr, rst, typeIncaps
'''                  Else
'''                    'isTheSameCod = CheckArrayPgm(arrayPGM, rst!idOggetto, rst!Riga)
'''                    isTheSameCod = False
'''                    If isTheSameCod Then
'''                      CaricaIstruzioneC rst, arrayPGM(1)
'''                      NomeProgappo = NomeProg
'''                      NomeProg = "<todelete>"
'''                      CostruisciCommonInitializeTemplate rst
'''                      NomeProg = NomeProgappo
'''                      ' Mauro 16/10/2007
'''                      If Not rst!isExec Then
'''                        CostruisciOperazioneTemplate Trim(wIstr), 1, arrayPGM(1), rst!idOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
'''                      Else
'''                        CostruisciOperazioneTemplate_EXEC Trim(wIstr), 1, arrayPGM(1), rst!idOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
'''                      End If
'''                      If err.Number > 0 Then CostruisciOperazioneTemplate Trim(wIstr), 1, arrayPGM(1), rst!idOggetto, rst!Riga, 0 & rst!ordPcb
'''                      INSERT_DECOD posizione, RTBModifica, RTErr, rst, typeIncaps
'''                    Else
'''                      codrif = ""
'''                      checkcod = True
'''                      firstIF = ""
'''                      Testo = ""
'''                      AddrighebefIstr = AddRighe
'''                      firstindex = True
'''                      counttomigrate = 0
'''                      For i = 1 To UBound(arrayPGM)
'''                        'rst!idpgm = arrayPGM(i)
'''                        If arrayPGMInfo(i) Then
'''                          counttomigrate = counttomigrate + 1
'''                          CaricaIstruzioneC rst, arrayPGM(i)
'''                          If Not i = UBound(arrayPGM) Then
'''                            INSERT_DECODC posizione, RTBModifica, RTErr, rst, typeIncaps, i, arrayPGM(i)
'''                          Else
'''                            INSERT_DECODC posizione, RTBModifica, RTErr, rst, typeIncaps, -1, arrayPGM(i)
'''                          End If
'''
'''                          indentkey = indentkey - 3
'''
'''                        End If
'''                      Next
'''                      'RTBModifica.SelStart = wPos
'''                      RTBModifica.SelStart = posizione
'''                      RTBModifica.SelLength = 0
'''                      RTBModifica.SelText = Testo
'''                      'wPos = wPos + Len(Testo)
'''                      posizione = posizione + Len(Testo)
'''                    End If
'''                  End If
'''                End If
'''                lastPos = posizione
'''              Else
'''                If TipoFile <> "EZT" Then
'''                  INSERT_NODECOD posizione, RTBModifica, rst
'''                  MadrdM_GenRout.AggLog "[Object: " & rst!idOggetto & "-Row( " & rst!Riga & ")]-warning: not valid instruction ", RTErr
'''                  lastPos = posizione
'''                End If
'''              End If
'''            Else
'''              'SQ???
'''              If rst!to_migrate Then
'''                'SQ gi� segnalato dalla routine chiamata...
'''                'm_fun.WriteLog "Instruction not found: Obj(" & rIst!idOggetto & ") Row( " & rst!Riga & ")", "MadrdF_Incapsulatore"
'''              End If
'''            End If
'''          Else
'''            If Optsource Then
'''              rsOffset!OffsetEncaps = AddRighe
'''            Else
'''              rsOffset!OffsetEncaps = IIf(IsNull(rsOffset!OffsetEncaps), 0, rsOffset!OffsetEncaps) + AddRighe
'''            End If
'''          End If
'''          rsOffset.Update
'''          rsOffset.Close
'''          rst.MoveNext
'''
'''          PbarIstr.Value = PbarIstr.Value + 1
'''        Wend
'''        If TipoFile = "PLI" Or TipoFile = "INC" Then
'''            'AC 17/12/2008
'''          insertREV rIst!idOggetto, TestoREV, RTBModifica
'''        End If
'''        If TipoFile = "PLI" Then
'''          'AC 09/01/08
'''          ' per le external entry consideriamo anche le routine chiamate da copy
'''          upadateArrayNomeRoutinePLI rIst!idOggetto
'''          insertExternalEntry RTBModifica, rIst!idOggetto, RTErr
'''        End If
'''
'''        If EmbeddedRoutine Then
'''          Insert_ProcedureTemplate RTBModifica, RTErr
'''          Insert_TagRoutine RTBModifica
'''        End If
'''        PbarIstr.Value = 0
'''
'''        'CambiaParole RTBModifica, TypeIncaps
'''        'ControllaUIB RTBModifica, TypeIncaps
'''
'''        'SQ: sospesa gestione PCB...
'''        'CambiaENTRYTDLI TBModifica, typeIncaps
'''        'If swCall Then
'''        ' CambiaPCB RTBModifica, typeIncaps, rIst!IdOggetto
'''        'End If
'''        If Not OldName = "" Then
'''          RTBModifica.SaveFile OldName, 1
'''        End If
'''
'''        With LvObject.ListItems.Add(, , rIst!idOggetto)
'''          .ToolTipText = "Double click for compare with original source"
'''          .SubItems(1) = Replace(OldName, m_fun.FnPathPrj, "...")
'''          .SubItems(2) = Now
'''        End With
'''
'''        LvObject.ListItems(LvObject.ListItems.Count).Selected = True
'''        LvObject.Refresh
'''        LvObject.SelectedItem.EnsureVisible
'''        rIst.MoveNext
'''
'''        If PBar.Max < PBar.Value + 1 Then
'''          PBar.Value = PBar.Max
'''        Else
'''          PBar.Value = PBar.Value + 1
'''        End If
'''      Wend
'''      rIst.Close
'''      'If Not OldName = "" Then
'''        'RTBModifica.SaveFile OldName, 1
'''      'End If
'''
'''      PBar.Value = 0
'''    Case "Close"
'''      Unload Me
'''
'''  End Select
'''
'''  Screen.MousePointer = vbDefault
'''End Sub

Public Sub Resize()
  On Error Resume Next
  Me.Move Dli2Rdbms.drLeft, Dli2Rdbms.drTop, Dli2Rdbms.drWidth, Dli2Rdbms.drHeight
  Frame2.Move 30, 400, Frame2.Width, (Dli2Rdbms.drHeight - 400 - Toolbar1.Height) / 3 * 2
  TVSelObj.Move 120, 240, Frame2.Width - 240, Frame2.Height - 360
  Frame1.Move 30, 430 + Frame2.Height, Frame1.Width, ((Dli2Rdbms.drHeight - 340) / 3) - 60
  LstFunction.Move 120, 240, Frame1.Width - 240, Frame1.Height - 360
  Frame4.Move Frame4.Left, Frame2.Top, Dli2Rdbms.drWidth - Frame4.Left - 120, Frame2.Height
  LvObject.Move 120, 240, Frame4.Width - 240, Frame4.Height - 420 - PBar.Height - PbarIstr.Height - 60
  PBar.Move LvObject.Left, LvObject.Top + LvObject.Height + 30, LvObject.Width, PBar.Height
  PbarIstr.Move LvObject.Left, PBar.Top + PBar.Height + 30, LvObject.Width, PbarIstr.Height
  Frame3.Move Frame3.Left, Frame1.Top, Dli2Rdbms.drWidth - Frame3.Left - 120, Frame1.Height
  RTErr.Move 120, 240, Frame3.Width - 240, Frame3.Height - 360
End Sub

Private Sub Form_Load()
  '''''''''''''''''''''''''''''''''''''''''''''
  'ATTENZIONE, POSIZIONALE: pericolosissimo!
  ' Usare costanti tipo "EMBEDDED_CHK"
  '''''''''''''''''''''''''''''''''''''''''''''
  LstFunction.AddItem "DLI Features to Rdbms"
  LstFunction.Selected(0) = True
  LstFunction.AddItem "Clear Instructions Key table"
  LstFunction.AddItem "Encapsulate programs for CALL method"
  LstFunction.Selected(2) = True
  LstFunction.AddItem "Encapsulate programs for EXEC method"
  'EMBEDDED_CHK=4
  LstFunction.AddItem "Encapsulate programs for EMBEDDED routines"
  LstFunction.Selected(EMBEDDED_CHK) = False
  EmbeddedRoutine = False

  swCall = True
  
  NumObjInParser = 0

  TVSelObj.Nodes.Add , , "SEL", "Only Selected Object"
  TVSelObj.Nodes(1).Checked = True
  TVSelObj.Nodes.Add , , "SRC", "Sources"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CBL", "Cobol Program"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CPY", "Cobol Copy"
  TVSelObj.Nodes.Add "SRC", tvwChild, "PLI", "PLI Program"
  TVSelObj.Nodes.Add "SRC", tvwChild, "INC", "PLI Include"
  
  If Dli2Rdbms.drTipoFinIm <> "" Then
    Dli2Rdbms.drFinestra.ListItems.Add , , "Dli2RDbMs: Incasulator Window Loaded at " & Time
  End If
  
  '''''''''''''''''''''''''''''''
  ' SQ 26-07-06
  '''''''''''''''''''''''''''''''
  loadEnvironmentParameters
  
  '''''''''''''''''''''''''''''''
  ' SQ 30-10-07
  ' Decodes Only
  '''''''''''''''''''''''''''''''
  optAll_Click
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
  If Dli2Rdbms.drTipoFinIm <> "" Then
    Dli2Rdbms.drFinestra.ListItems.Add , , "Dli2RDbMs: Incapsulator Window Unloaded at " & Time
  End If
  
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub TVSelObj_NodeCheck(ByVal Node As MSComctlLib.Node)
  Dim K1 As Integer, k2 As Integer
  Dim Wkey As String
  Dim wCheck As Variant
  Dim tb As Recordset

  Wkey = Node.key
  wCheck = Node.Checked
  
  NumObjInParser = 0
  
  Select Case Node.key
    Case "SEL"
      If Node.Checked = True Then
        For K1 = 1 To Dli2Rdbms.drObjList.ListItems.Count
          If Dli2Rdbms.drObjList.ListItems(K1).Selected = True Then
            NumObjInParser = NumObjInParser + 1
          End If
        Next K1
      End If
       
      If TVSelObj.Nodes(Node.Index + 1).Checked = True Then
        TVSelObj.Nodes(Node.Index + 1).Checked = False ' Sources
        TVSelObj.Nodes(Node.Index + 2).Checked = False ' CBL
        TVSelObj.Nodes(Node.Index + 3).Checked = False ' CPY
        TVSelObj.Nodes(Node.Index + 4).Checked = False ' PLI
        TVSelObj.Nodes(Node.Index + 5).Checked = False ' INC
      End If
     
    Case "CBL"
      TVSelObj.Nodes(Node.Index - 1).Checked = Node.Checked
      TVSelObj.Nodes(1).Checked = False
       
    ' Mauro 08/11/2007 : Aggiunta gestione per CPY, PLI e INC
    Case "CPY"
      TVSelObj.Nodes(Node.Index - 2).Checked = Node.Checked
      TVSelObj.Nodes(1).Checked = False
    
    Case "PLI"
      TVSelObj.Nodes(Node.Index - 3).Checked = Node.Checked
      TVSelObj.Nodes(1).Checked = False
    
    Case "INC"
      TVSelObj.Nodes(Node.Index - 4).Checked = Node.Checked
      TVSelObj.Nodes(1).Checked = False
       
    Case Else
      For K1 = Node.Index + 1 To Node.Index + Node.Children
        TVSelObj.Nodes(K1).Checked = Node.Checked
      Next K1
      TVSelObj.Nodes(1).Checked = False
  End Select
        
  If TVSelObj.Nodes(1).Checked = False Then
    For K1 = 2 To TVSelObj.Nodes.Count
      If TVSelObj.Nodes(K1).Checked = True Then
        Wkey = TVSelObj.Nodes(K1).key
        Set tb = m_fun.Open_Recordset("Select * from bs_Oggetti where tipo = '" & Wkey & "'")
        If tb.RecordCount Then
          NumObjInParser = NumObjInParser + tb.RecordCount
        End If
        tb.Close
      End If
    Next K1
  End If
  
  Frame4.Caption = "Activite log for " & NumObjInParser & " Objects"
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''
' SQ tmp: buttare via quella dentro il _click
'''''''''''''''''''''''''''''''''''''''''''''''
Private Sub createDecodes()
  Dim rs As Recordset
  Dim criterio As String
  Dim i As Long
  Dim selIncaps As String

  Screen.MousePointer = vbHourglass
  LvObject.ListItems.Clear

  ''''''''''''''''''''''''
  ' RESET DECODIFICHE
  ''''''''''''''''''''''''
  If LstFunction.Selected(1) And Not FullIncaps Then
    If MsgBox("Warning. You are resetting all DECODING: you could need to INCAPSULATE the sources again. " & vbCrLf & "Are you sure?", vbYesNo + vbQuestion + vbCritical, "i-4.Migration: Encapsulator") = vbYes Then
      m_fun.FnConnection.Execute "DELETE * From MgDli_decodificaIstr Where ImsDB"
    End If
  End If

  ''''''''''''''''''''''''
  ' LISTA OGGETTI
  ''''''''''''''''''''''''
  selIncaps = Restituisci_Selezione(TVSelObj)
  Select Case selIncaps
    Case "'SEL'" 'Solo i programmi Selezionati
      For i = 1 To Dli2Rdbms.drObjList.ListItems.Count
        If Dli2Rdbms.drObjList.ListItems(i).Selected Then
           If Trim(criterio) <> "" Then
             criterio = criterio & ", " & Val(Dli2Rdbms.drObjList.ListItems(i).text)
           Else
             criterio = Val(Dli2Rdbms.drObjList.ListItems(i).text)
           End If
        End If
      Next i
      If Len(Trim(criterio)) = 0 Then
        'dare messaggio
        Exit Sub
      End If
      Set rs = m_fun.Open_Recordset("SELECT idoggetto,tipo,cics,nome FROM BS_Oggetti WHERE " & _
                                    "idOggetto in (" & criterio & ") And Dli Order By IdOggetto")
    ' Mauro 08/11/2007 : Aggiunta gestione per CPY, PLI e INC
''    Case "SRC" 'Tutti i programmi
''      'Set rs = m_fun.Open_Recordset("Select distinct idoggetto  From BS_Oggetti Where Dli = true Order By IdOggetto") 'virgilio
''      ''''''''''''''''''
''      'SQ SOLO I CBL?! E SOTTO NON C'E' LO STESSO FILTRO?!
''      ''''''''''''''''''
''      Set rs = m_fun.Open_Recordset( _
''        "Select idoggetto,tipo,cics,nome From BS_Oggetti Where Dli and tipo='CBL' Order By IdOggetto")
    Case Else 'Tutti i programmi
      Set rs = m_fun.Open_Recordset("Select idoggetto, tipo, cics, nome From BS_Oggetti Where " & _
                                    "Dli and tipo in (" & selIncaps & ") Order By IdOggetto")
  End Select

  PBar.Max = rs.RecordCount
  PBar.Value = 0
  PbarIstr.Max = 1
  PbarIstr.Value = 0

  While Not rs.EOF
    TipoFile = rs!tipo
    'AC 07/04/08
    If IsAlwaysBatch Then
      SwTp = False
    Else
      SwTp = rs!CICS
    End If
    If decodesOnly And Not FullIncaps Then
      LvObject.ListItems.Add(, , rs!IdOggetto).SubItems(1) = Replace(rs!nome, m_fun.FnPathPrj, "...")
      LvObject.ListItems(LvObject.ListItems.Count).ToolTipText = "Double click for compare with original source"
      LvObject.ListItems(LvObject.ListItems.Count).SubItems(2) = Now
  
      LvObject.ListItems(LvObject.ListItems.Count).Selected = True
      LvObject.Refresh
      LvObject.SelectedItem.EnsureVisible
    End If

    MadrdM_Incapsulatore.createDecodes rs!IdOggetto

    If PBar.Max < PBar.Value + 1 Then
      PBar.Value = PBar.Max
    Else
      PBar.Value = PBar.Value + 1
    End If
    rs.MoveNext
  Wend
  rs.Close

  PBar.Value = 0
  Screen.MousePointer = vbDefault
End Sub
