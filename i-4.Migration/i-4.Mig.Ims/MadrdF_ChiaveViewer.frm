VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form MadrdF_ChiaveViewer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Areas Viewer"
   ClientHeight    =   5310
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6810
   Icon            =   "MadrdF_ChiaveViewer.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5310
   ScaleWidth      =   6810
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOpen 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   5850
      Picture         =   "MadrdF_ChiaveViewer.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   4410
      Width           =   855
   End
   Begin RichTextLib.RichTextBox rtKey 
      Height          =   825
      Left            =   60
      TabIndex        =   0
      Top             =   90
      Width           =   6645
      _ExtentX        =   11721
      _ExtentY        =   1455
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"MadrdF_ChiaveViewer.frx":1E8C
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin RichTextLib.RichTextBox rtStruct 
      Height          =   3345
      Left            =   60
      TabIndex        =   1
      Top             =   990
      Width           =   6645
      _ExtentX        =   11721
      _ExtentY        =   5900
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"MadrdF_ChiaveViewer.frx":1F0C
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "MadrdF_ChiaveViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOpen_Click()
   Unload Me
End Sub

Public Function compose_key() As String
  Dim Testo As String
  Dim i As Long
  
  'stefano: non aveva senso, ora va un po' meglio, anche se sarebbe da migliorare...
  'testo = ""
  Testo = Istruzione.SSA(AULivello).NomeSegmento
  Testo = Testo & Istruzione.SSA(AULivello).CommandCode
  'If Istruzione.SSA(AULivello).Chiavi Is Nothing Then Exit Function
  For i = 1 To UBound(Istruzione.SSA(AULivello).Chiavi)
    If i = 1 Then
      Testo = Testo & "("
    End If
    'testo = testo & Format(i, "00") & ")"
    Testo = Testo & Istruzione.SSA(AULivello).Chiavi(i).key
    Testo = Testo & " " & Istruzione.SSA(AULivello).Chiavi(i).Operatore
    Testo = Testo & " " & Istruzione.SSA(AULivello).Chiavi(i).Valore.Valore
    If UBound(Istruzione.SSA(AULivello).OpLogico) >= i Then
      Testo = Testo & " " & Istruzione.SSA(AULivello).OpLogico(i)
    End If
    Testo = Testo & " "
    If i = UBound(Istruzione.SSA(AULivello).Chiavi) Then
      Testo = Testo & ")"
    End If
  Next i

  compose_key = Testo
End Function

Public Function compose_keyStr(wId As Long, wIdArea As Long) As String
  Dim Testo As String
  Dim rs As Recordset, rsc As Recordset
  Dim wIndent As Long
  Dim oldLev As Long
  
  Set rs = m_fun.Open_Recordset("Select * From PsData_Cmp Where IdOggetto = " & wId & " And IdArea = " & wIdArea & " Order by Ordinale")
  While Not rs.EOF
    If oldLev < rs!livello Then
      wIndent = wIndent + 3
    Else
      If oldLev > rs!livello Then wIndent = wIndent - 3
    End If
    oldLev = rs!livello
    'livello e nome
    Testo = Testo & Space(wIndent) & Format(rs!livello, "00") & " " & rs!nome
    If rs!tipo <> "A" Then
      'PIC, tipo e segno
      Testo = Testo & " PIC " & IIf(rs!Segno = "S", "S", "") & rs!tipo
      'parentesi e picture
      Testo = Testo & "(" & rs!Lunghezza & ")" & IIf(rs!Decimali <> 0, "V" & rs!tipo & "(" & rs!Decimali & ")", "")
      'valore; se ce mettete gli spazi sul db non ve la prendete con me
      If Trim(rs!Valore) <> "" Then
        Testo = Testo & " VALUE " & rs!Valore
      End If
    End If
    Testo = Testo & "." & vbCrLf
    If rs!idCopy > 0 Then
      Set rsc = m_fun.Open_Recordset("Select * From PsData_Cmp Where idOggetto = " & rs!idCopy & " order by ordinale")
      While Not rsc.EOF
        If oldLev < rsc!livello Then
          wIndent = wIndent + 3
        Else
          If oldLev > rsc!livello Then wIndent = wIndent - 3
        End If
        oldLev = rsc!livello
        'livello e nome
        Testo = Testo & Space(wIndent) & Format(rsc!livello, "00") & " " & rsc!nome
        If rsc!tipo <> "A" Then
          'PIC, tipo e segno
          Testo = Testo & " PIC " & IIf(rsc!Segno = "S", "S", "") & rsc!tipo
          'parentesi e picture
          Testo = Testo & "(" & rsc!Lunghezza & ")" & IIf(rsc!Decimali <> 0, "V" & rsc!tipo & "(" & rsc!Decimali & ")", "")
          'valore
          If Trim(rsc!Valore) <> "" Then
            Testo = Testo & " VALUE " & rsc!Valore
          End If
        End If
        Testo = Testo & "." & vbCrLf
        rsc.MoveNext
      Wend
      rsc.Close
    End If
    rs.MoveNext
  Wend
  rs.Close
  compose_keyStr = Testo
End Function

Private Sub Form_Load()
  rtKey.text = compose_key
  rtStruct.text = compose_keyStr(Istruzione.SSA(AULivello).idOggetto, Istruzione.SSA(AULivello).IdArea)
End Sub
