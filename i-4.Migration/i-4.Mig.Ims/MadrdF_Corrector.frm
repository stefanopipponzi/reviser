VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form MadrdF_Corrector 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " DLI Instructions corrector"
   ClientHeight    =   8415
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   12165
   Icon            =   "MadrdF_Corrector.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8415
   ScaleWidth      =   12165
   ShowInTaskbar   =   0   'False
   Begin TabDlg.SSTab SSTab4 
      Height          =   4455
      Left            =   60
      TabIndex        =   35
      Top             =   480
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   7858
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   128
      TabCaption(0)   =   "Text Editor"
      TabPicture(0)   =   "MadrdF_Corrector.frx":0442
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "RTIncaps"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Clones Managing"
      TabPicture(1)   =   "MadrdF_Corrector.frx":045E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lswCloni"
      Tab(1).Control(1)=   "cmdAddClone"
      Tab(1).Control(2)=   "cmdDelClone"
      Tab(1).Control(3)=   "cmdEditClone"
      Tab(1).Control(4)=   "chkAllClones"
      Tab(1).ControlCount=   5
      Begin VB.CheckBox chkAllClones 
         Alignment       =   1  'Right Justify
         Caption         =   "All Clones"
         ForeColor       =   &H00C00000&
         Height          =   225
         Left            =   -69060
         TabIndex        =   79
         Top             =   4020
         Width           =   1095
      End
      Begin VB.CommandButton cmdEditClone 
         Caption         =   "Edit"
         Enabled         =   0   'False
         Height          =   345
         Left            =   -70680
         Picture         =   "MadrdF_Corrector.frx":047A
         TabIndex        =   77
         ToolTipText     =   "Edit Clone"
         Top             =   3960
         Width           =   975
      End
      Begin VB.CommandButton cmdDelClone 
         Caption         =   "Delete"
         Enabled         =   0   'False
         Height          =   345
         Left            =   -71820
         Picture         =   "MadrdF_Corrector.frx":08BC
         TabIndex        =   76
         ToolTipText     =   "Delete Clone"
         Top             =   3960
         Width           =   975
      End
      Begin VB.CommandButton cmdAddClone 
         Caption         =   "Add"
         Height          =   345
         Left            =   -72960
         Picture         =   "MadrdF_Corrector.frx":0CFE
         TabIndex        =   75
         ToolTipText     =   "Add Clone"
         Top             =   3960
         Width           =   975
      End
      Begin RichTextLib.RichTextBox RTIncaps 
         Height          =   3990
         Left            =   60
         TabIndex        =   36
         Top             =   360
         Width           =   7170
         _ExtentX        =   12647
         _ExtentY        =   7038
         _Version        =   393217
         BackColor       =   4210752
         HideSelection   =   0   'False
         ScrollBars      =   3
         RightMargin     =   80000
         TextRTF         =   $"MadrdF_Corrector.frx":1140
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ListView lswCloni 
         Height          =   3450
         Left            =   -74880
         TabIndex        =   74
         Top             =   420
         Width           =   7035
         _ExtentX        =   12409
         _ExtentY        =   6085
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "Clone"
            Text            =   "Clone"
            Object.Width           =   1765
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "Decodes"
            Text            =   "Decodes"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3975
      Left            =   7500
      TabIndex        =   5
      Top             =   480
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   7011
      _Version        =   393216
      TabOrientation  =   1
      Style           =   1
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Level details"
      TabPicture(0)   =   "MadrdF_Corrector.frx":11C0
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fraSSA"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Instruction Syntax"
      TabPicture(1)   =   "MadrdF_Corrector.frx":11DC
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Label3"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "FraIstr"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      Begin VB.Frame fraSSA 
         BorderStyle     =   0  'None
         Height          =   3525
         Left            =   -74940
         TabIndex        =   37
         Top             =   60
         Width           =   4425
         Begin TabDlg.SSTab SSTab3 
            Height          =   3435
            Left            =   0
            TabIndex        =   38
            Top             =   60
            Width           =   4395
            _ExtentX        =   7752
            _ExtentY        =   6059
            _Version        =   393216
            TabOrientation  =   2
            Tabs            =   2
            TabHeight       =   520
            ForeColor       =   128
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Body"
            TabPicture(0)   =   "MadrdF_Corrector.frx":11F8
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "ImgSegmenti"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "Label2(10)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "Label2(8)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "Label2(5)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "Label2(3)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblIstrLev(0)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "Label2(0)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "CmdAddlev"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "ChkBoth"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "ChkQual"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtStrutt"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtSegLen"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtsegm"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "cmdSegmFind"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtSSAName"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "cmdKey"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtComCode"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).ControlCount=   17
            TabCaption(1)   =   "Key"
            TabPicture(1)   =   "MadrdF_Corrector.frx":1214
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "fraKey"
            Tab(1).ControlCount=   1
            Begin VB.Frame fraKey 
               BorderStyle     =   0  'None
               Height          =   3225
               Left            =   -74580
               TabIndex        =   49
               Top             =   120
               Width           =   3915
               Begin VB.CommandButton cmdOpMulti 
                  Caption         =   "..."
                  Height          =   375
                  Left            =   1980
                  TabIndex        =   58
                  ToolTipText     =   "Manage Multi operators for current level istruction..."
                  Top             =   1125
                  Width           =   375
               End
               Begin VB.TextBox txtKeyName 
                  BackColor       =   &H80000018&
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   780
                  TabIndex        =   57
                  Top             =   690
                  Width           =   3030
               End
               Begin VB.TextBox txtOpLogico 
                  Alignment       =   2  'Center
                  BackColor       =   &H00FFFFC0&
                  ForeColor       =   &H00C00000&
                  Height          =   330
                  Left            =   3450
                  TabIndex        =   56
                  Top             =   2880
                  Width           =   330
               End
               Begin VB.TextBox txtKeyLen 
                  BackColor       =   &H00FFFFC0&
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   2700
                  TabIndex        =   55
                  Top             =   2022
                  Width           =   1080
               End
               Begin VB.TextBox txtValue 
                  BackColor       =   &H00FFFFC0&
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   810
                  TabIndex        =   54
                  Top             =   1578
                  Width           =   2970
               End
               Begin VB.TextBox txtOp 
                  BackColor       =   &H00FFFFC0&
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   810
                  TabIndex        =   53
                  Top             =   1125
                  Width           =   1110
               End
               Begin VB.TextBox txtKeyStart 
                  BackColor       =   &H00FFFFC0&
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   810
                  TabIndex        =   52
                  Top             =   2022
                  Width           =   1110
               End
               Begin VB.TextBox txtKeyMask 
                  BackColor       =   &H00FFFFC0&
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   825
                  TabIndex        =   51
                  Top             =   2466
                  Width           =   2970
               End
               Begin VB.CommandButton CmdAddkey 
                  Caption         =   "Add Key"
                  Enabled         =   0   'False
                  Height          =   315
                  Left            =   120
                  TabIndex        =   50
                  Top             =   2880
                  Width           =   972
               End
               Begin VB.Label lblKeyLev 
                  Alignment       =   2  'Center
                  BackColor       =   &H80000003&
                  Caption         =   "Current key level: "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00FFFFFF&
                  Height          =   285
                  Index           =   0
                  Left            =   0
                  TabIndex        =   67
                  Top             =   360
                  Width           =   3855
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Key Len."
                  ForeColor       =   &H00C00000&
                  Height          =   225
                  Index           =   9
                  Left            =   2040
                  TabIndex        =   66
                  Top             =   2145
                  Width           =   645
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Oper."
                  ForeColor       =   &H00C00000&
                  Height          =   225
                  Index           =   7
                  Left            =   300
                  TabIndex        =   65
                  Top             =   1200
                  Width           =   435
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "SSA value"
                  ForeColor       =   &H00C00000&
                  Height          =   195
                  Index           =   6
                  Left            =   45
                  TabIndex        =   64
                  Top             =   1680
                  Width           =   750
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Key Name"
                  ForeColor       =   &H00C00000&
                  Height          =   195
                  Index           =   4
                  Left            =   30
                  TabIndex        =   63
                  Top             =   780
                  Width           =   735
               End
               Begin VB.Label lblIstrLev 
                  Alignment       =   2  'Center
                  BackColor       =   &H8000000D&
                  Caption         =   "Current istruction level: "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00FFFFFF&
                  Height          =   285
                  Index           =   1
                  Left            =   0
                  TabIndex        =   62
                  Top             =   30
                  Width           =   3855
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Key start"
                  ForeColor       =   &H00C00000&
                  Height          =   225
                  Index           =   12
                  Left            =   120
                  TabIndex        =   61
                  Top             =   2160
                  Width           =   690
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Key Value"
                  ForeColor       =   &H00C00000&
                  Height          =   195
                  Index           =   13
                  Left            =   60
                  TabIndex        =   60
                  Top             =   2580
                  Width           =   720
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Logical Operator"
                  ForeColor       =   &H00C00000&
                  Height          =   195
                  Index           =   11
                  Left            =   2160
                  TabIndex        =   59
                  Top             =   2970
                  Width           =   1170
               End
            End
            Begin VB.TextBox txtComCode 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   300
               Left            =   1410
               TabIndex        =   48
               Top             =   2010
               Width           =   2520
            End
            Begin VB.CommandButton cmdKey 
               Caption         =   "..."
               Height          =   300
               Left            =   3600
               TabIndex        =   47
               Top             =   1650
               Width           =   345
            End
            Begin VB.TextBox txtSSAName 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   300
               Left            =   1410
               TabIndex        =   46
               Top             =   1650
               Width           =   2100
            End
            Begin VB.CommandButton cmdSegmFind 
               Caption         =   "..."
               Height          =   300
               Left            =   3600
               TabIndex        =   45
               Top             =   540
               Width           =   345
            End
            Begin VB.TextBox txtsegm 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   300
               Left            =   1410
               TabIndex        =   44
               Top             =   540
               Width           =   2100
            End
            Begin VB.TextBox txtSegLen 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   300
               Left            =   1410
               TabIndex        =   43
               Top             =   900
               Width           =   2520
            End
            Begin VB.TextBox txtStrutt 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   300
               Left            =   1410
               TabIndex        =   42
               Top             =   1260
               Width           =   2520
            End
            Begin VB.CheckBox ChkQual 
               Alignment       =   1  'Right Justify
               Caption         =   "Qualified"
               ForeColor       =   &H00C00000&
               Height          =   252
               Left            =   585
               TabIndex        =   41
               Top             =   2400
               Width           =   1020
            End
            Begin VB.CheckBox ChkBoth 
               Alignment       =   1  'Right Justify
               Caption         =   "Both versions"
               ForeColor       =   &H00C00000&
               Height          =   252
               Left            =   2580
               TabIndex        =   40
               Top             =   2400
               Width           =   1332
            End
            Begin VB.CommandButton CmdAddlev 
               Caption         =   "Add Level"
               Enabled         =   0   'False
               Height          =   315
               Left            =   540
               TabIndex        =   39
               Top             =   2820
               Width           =   1095
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Comm. Codes"
               ForeColor       =   &H00C00000&
               Height          =   195
               Index           =   0
               Left            =   375
               TabIndex        =   73
               Top             =   2040
               Width           =   975
            End
            Begin VB.Label lblIstrLev 
               Alignment       =   2  'Center
               BackColor       =   &H8000000D&
               Caption         =   "Current istruction level: "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   285
               Index           =   0
               Left            =   360
               TabIndex        =   72
               Top             =   90
               Width           =   3915
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "SSA Field"
               ForeColor       =   &H00C00000&
               Height          =   195
               Index           =   3
               Left            =   630
               TabIndex        =   71
               Top             =   1680
               Width           =   720
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Segment"
               ForeColor       =   &H00C00000&
               Height          =   225
               Index           =   5
               Left            =   690
               TabIndex        =   70
               Top             =   600
               Width           =   660
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Data Area"
               ForeColor       =   &H00C00000&
               Height          =   225
               Index           =   8
               Left            =   570
               TabIndex        =   69
               Top             =   1320
               Width           =   780
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Seg. Len."
               ForeColor       =   &H00C00000&
               Height          =   195
               Index           =   10
               Left            =   660
               TabIndex        =   68
               Top             =   960
               Width           =   690
            End
            Begin VB.Image ImgSegmenti 
               Height          =   285
               Left            =   4020
               Stretch         =   -1  'True
               Top             =   570
               Width           =   285
            End
         End
      End
      Begin VB.Frame FraIstr 
         BorderStyle     =   0  'None
         Caption         =   "..."
         Height          =   3435
         Left            =   120
         TabIndex        =   6
         Top             =   120
         Width           =   4365
         Begin VB.CommandButton cmdOperatore 
            Caption         =   "..."
            Height          =   288
            Left            =   1860
            TabIndex        =   24
            Top             =   90
            Width           =   252
         End
         Begin VB.TextBox TxtInstr 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   288
            Left            =   840
            TabIndex        =   23
            Top             =   90
            Width           =   975
         End
         Begin VB.TextBox txtIstruzione 
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   990
            Left            =   0
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   12
            Top             =   2400
            Width           =   4320
         End
         Begin VB.CommandButton cmdAddPsb 
            Caption         =   "Add"
            Height          =   300
            Left            =   90
            TabIndex        =   11
            Top             =   1980
            Width           =   876
         End
         Begin VB.CommandButton cmdRemovePsb 
            Caption         =   "Remove"
            Height          =   300
            Left            =   1140
            TabIndex        =   10
            Top             =   1980
            Width           =   876
         End
         Begin VB.CommandButton cmdAddDBD 
            Caption         =   "Add"
            Height          =   300
            Left            =   2325
            TabIndex        =   9
            Top             =   1980
            Width           =   876
         End
         Begin VB.CommandButton cmdRemoveDBD 
            Caption         =   "Remove"
            Height          =   300
            Left            =   3360
            TabIndex        =   8
            Top             =   1980
            Width           =   876
         End
         Begin VB.TextBox TxtPCB 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   288
            Left            =   3270
            TabIndex        =   7
            Top             =   90
            Width           =   975
         End
         Begin MSComctlLib.ListView lswDBD 
            Height          =   1185
            Left            =   2220
            TabIndex        =   13
            Top             =   720
            Width           =   2115
            _ExtentX        =   3731
            _ExtentY        =   2090
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "PsbName"
               Object.Width           =   2540
            EndProperty
         End
         Begin MSComctlLib.ListView lswPsb 
            Height          =   1185
            Left            =   0
            TabIndex        =   14
            Top             =   720
            Width           =   2115
            _ExtentX        =   3731
            _ExtentY        =   2090
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "PsbName"
               Object.Width           =   2540
            EndProperty
         End
         Begin VB.Label LblPCB 
            Caption         =   "Instruction"
            ForeColor       =   &H00C00000&
            Height          =   195
            Index           =   0
            Left            =   30
            TabIndex        =   22
            Top             =   120
            Width           =   735
         End
         Begin VB.Label Label2 
            Caption         =   "PSB"
            ForeColor       =   &H00C00000&
            Height          =   195
            Index           =   1
            Left            =   30
            TabIndex        =   17
            Top             =   480
            Width           =   675
         End
         Begin VB.Label Label2 
            Caption         =   "DBD"
            ForeColor       =   &H00C00000&
            Height          =   195
            Index           =   2
            Left            =   2280
            TabIndex        =   16
            Top             =   480
            Width           =   645
         End
         Begin VB.Label LblPCB 
            Caption         =   "PCB Number"
            ForeColor       =   &H00C00000&
            Height          =   195
            Index           =   14
            Left            =   2220
            TabIndex        =   15
            Top             =   120
            Width           =   975
         End
      End
      Begin VB.Label Label3 
         BackColor       =   &H8000000D&
         Caption         =   "ewwe"
         Height          =   252
         Left            =   120
         TabIndex        =   21
         Top             =   120
         Width           =   732
      End
   End
   Begin VB.CheckBox CheckToMigrate 
      Alignment       =   1  'Right Justify
      Caption         =   "To migrate"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   9900
      TabIndex        =   26
      Top             =   4560
      Width           =   1095
   End
   Begin VB.CheckBox CheckObsolete 
      Alignment       =   1  'Right Justify
      Caption         =   "Obsolete"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   8640
      TabIndex        =   25
      Top             =   4560
      Width           =   975
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Save"
      Height          =   285
      Left            =   11220
      Picture         =   "MadrdF_Corrector.frx":1230
      TabIndex        =   19
      ToolTipText     =   "Save Corrections"
      Top             =   4560
      Width           =   855
   End
   Begin VB.CheckBox chkIstrFLAG 
      Alignment       =   1  'Right Justify
      Caption         =   "Valid"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   7500
      TabIndex        =   18
      Top             =   4560
      Width           =   735
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   12165
      _ExtentX        =   21458
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons(1)"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Find"
            Object.ToolTipText     =   "Find selected text"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "OPENTEXTED"
            Object.ToolTipText     =   "Open selected source with text editor..."
            ImageIndex      =   9
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Copybooks"
            Object.ToolTipText     =   "Expand/Hide Copybooks"
            ImageIndex      =   11
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Hide_Copybooks"
            Object.ToolTipText     =   "Hide Copybooks"
            ImageIndex      =   10
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2415
      Top             =   10920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   20
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":1672
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":17CE
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":192A
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":1EC2
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":21DE
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":233A
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":2792
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":28EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":2D4E
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":2EB2
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":3312
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":3766
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":38C2
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":3A1E
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":3B7A
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":4646
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":4A9A
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":4EEC
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":533E
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":5790
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTSalva 
      Height          =   885
      Left            =   1155
      TabIndex        =   1
      Top             =   10695
      Visible         =   0   'False
      Width           =   1065
      _ExtentX        =   1879
      _ExtentY        =   1561
      _Version        =   393217
      RightMargin     =   60000
      TextRTF         =   $"MadrdF_Corrector.frx":5BE2
   End
   Begin RichTextLib.RichTextBox RTCpyApp 
      Height          =   885
      Left            =   60
      TabIndex        =   0
      Top             =   10695
      Visible         =   0   'False
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   1561
      _Version        =   393217
      ScrollBars      =   3
      RightMargin     =   60000
      TextRTF         =   $"MadrdF_Corrector.frx":5C64
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Index           =   0
      Left            =   11130
      Top             =   11190
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":5CE6
            Key             =   "Checkmrk"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":6000
            Key             =   "New"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":6112
            Key             =   "Misc39b"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":642C
            Key             =   "Erase02"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":6746
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":6858
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":6A32
            Key             =   "Find"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":6B44
            Key             =   "Copy"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Index           =   1
      Left            =   4440
      Top             =   10890
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":6C56
            Key             =   "Checkmrk"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":6F70
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":7082
            Key             =   "Find"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":7194
            Key             =   "New"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":72A6
            Key             =   "Erase02"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":75C0
            Key             =   "Misc39b"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":78DA
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":79EC
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":7BC6
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":A6D0
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":AB22
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadrdF_Corrector.frx":BB74
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTPGMBase 
      Height          =   975
      Left            =   3150
      TabIndex        =   3
      Top             =   10710
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   1720
      _Version        =   393217
      ScrollBars      =   3
      RightMargin     =   60000
      TextRTF         =   $"MadrdF_Corrector.frx":C44E
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TreeView TRWIstruzione 
      Height          =   3480
      Left            =   7500
      TabIndex        =   4
      Top             =   4860
      Width           =   4605
      _ExtentX        =   8123
      _ExtentY        =   6138
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab SSTab2 
      Height          =   3405
      Left            =   30
      TabIndex        =   20
      Top             =   4980
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   6006
      _Version        =   393216
      TabOrientation  =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Instructions list"
      TabPicture(0)   =   "MadrdF_Corrector.frx":C4CE
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lswRighe"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Log Viewer"
      TabPicture(1)   =   "MadrdF_Corrector.frx":C4EA
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "LswLog"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   975
         Left            =   120
         TabIndex        =   31
         Top             =   2040
         Width           =   2910
         Begin VB.CheckBox chkErrInstr 
            Caption         =   "Invalid only"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   1620
            TabIndex        =   78
            Top             =   660
            Width           =   1092
         End
         Begin VB.OptionButton optIMSDC 
            Caption         =   "IMS/DC"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   60
            TabIndex        =   34
            Top             =   660
            Width           =   885
         End
         Begin VB.OptionButton optIMSDB 
            Caption         =   "IMS/DB"
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   60
            TabIndex        =   33
            Top             =   360
            Width           =   885
         End
         Begin VB.OptionButton optAllInstr 
            Caption         =   "All Instructions"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   60
            TabIndex        =   32
            Top             =   180
            Width           =   1476
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Instruction Errors"
         ForeColor       =   &H00C00000&
         Height          =   945
         Left            =   3180
         TabIndex        =   27
         Top             =   2040
         Width           =   4065
         Begin VB.TextBox txtWarning 
            Appearance      =   0  'Flat
            BackColor       =   &H80000014&
            BorderStyle     =   0  'None
            ForeColor       =   &H00000080&
            Height          =   615
            Left            =   90
            MultiLine       =   -1  'True
            TabIndex        =   28
            Top             =   240
            Width           =   3825
         End
      End
      Begin MSComctlLib.ListView lswRighe 
         Height          =   1890
         Left            =   120
         TabIndex        =   29
         Top             =   120
         Width           =   7125
         _ExtentX        =   12568
         _ExtentY        =   3334
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "id"
            Text            =   "IdPgm"
            Object.Width           =   1765
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "IdOggetto"
            Object.Width           =   1765
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "Riga"
            Text            =   "Line"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Key             =   "istr"
            Text            =   "Instruction"
            Object.Width           =   1766
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Key             =   "sintax"
            Text            =   "Statement"
            Object.Width           =   31751
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Key             =   "idpsb"
            Text            =   "ID PSB"
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.ListView LswLog 
         Height          =   2895
         Left            =   -74880
         TabIndex        =   30
         Top             =   120
         Width           =   7125
         _ExtentX        =   12568
         _ExtentY        =   5106
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "LogText"
            Object.Width           =   5292
         EndProperty
      End
   End
End
Attribute VB_Name = "MadrdF_Corrector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public mSel_Idx_Database As Long
Public mSel_Idx_Psb As Long
Public RtBefore As String


Private Sub loadSelectedKey()
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "KEY", txtKeyName.text
  'Controllo "Istruzione"...
  Associa_Dato_In_Memoria AULivello, "KEY", txtKeyName.text
  
  'SG : Carica i dati riferiti alla chiave
  txtKeyLen = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Valore.Lunghezza
  txtOp = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Operatore
  txtValue = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Valore.FieldValue
  txtKeyLen.text = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Valore.Lunghezza
  txtKeyStart.text = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Valore.KeyStart
  txtKeyMask.text = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Valore.Valore
   
  txtSSAName.text = Istruzione.SSA(AULivello).ssaName
   
  txtValue.SelStart = 0
  txtValue.SelLength = Len(txtValue.text)
  
  'SG : Carica l'operatore logico di congiunzione se c'�
  'attenzione l'operatore logico � riferito al successivo : L'ultimo della catena non avr� operatore
  'Es: FTCHIA>=VALUE & FTCHIA2<=VALUE
  txtOpLogico.text = ""
  If UBound(Istruzione.SSA(AULivello).OpLogico) >= AUSelIndexKey Then
    txtOpLogico.text = Istruzione.SSA(AULivello).OpLogico(AUSelIndexKey)
  End If
End Sub

' Mauro 12/05/2008
Private Sub chkAllClones_Click()
  Dim rs As Recordset
  Dim IdOgg As Long, idPgm As Long, Riga As Long
  
  idPgm = lswRighe.SelectedItem
  IdOgg = lswRighe.SelectedItem.ListSubItems(L_IDOGG)
  Riga = lswRighe.SelectedItem.ListSubItems(L_LINE)
  lswCloni.ListItems.Clear
  If chkAllClones.Value = 1 Then
    Set rs = m_fun.Open_Recordset( _
                   "select b.codifica, b.decodifica, a.riga, a.idoggetto from psdli_istrcodifica as a, mgdli_Decodificaistr as b where " & _
                   "a.codifica = b.codifica and a.idpgm = " & idPgm)
  Else
    Set rs = m_fun.Open_Recordset( _
                   "select b.codifica, b.decodifica, a.riga, a.idoggetto from psdli_istrcodifica as a, mgdli_Decodificaistr as b where " & _
                   "a.codifica = b.codifica and a.idoggetto = " & IdOgg & " and a.idpgm = " & idPgm & " and " & _
                   "a.riga = " & Riga)
  End If
  
  Do Until rs.EOF
    MadrdF_Corrector.lswCloni.ListItems.Add , , rs!Codifica
    MadrdF_Corrector.lswCloni.ListItems(MadrdF_Corrector.lswCloni.ListItems.Count).SubItems(1) = rs!Decodifica
    MadrdF_Corrector.lswCloni.ListItems(MadrdF_Corrector.lswCloni.ListItems.Count).tag = idPgm & "/" & rs!IdOggetto & "/" & rs!Riga
    rs.MoveNext
  Loop
  rs.Close
End Sub

Private Sub ChkBoth_Click()
  If ChkBoth.Value = 1 Then
    ChkQual.Value = 1
    'Associa_Dato_In_TreeView TRWIstruzione, AULivello, "CKQ" & Format(AULivello, "00"), ChkQual.Value
    Associa_Dato_In_Memoria AULivello, "CKQ", ChkQual.Value
  End If
  'Associa_Dato_In_TreeView TRWIstruzione, AULivello, "CKB" & Format(AULivello, "00"), ChkBoth.Value
  Associa_Dato_In_Memoria AULivello, "CKB", ChkBoth.Value
End Sub

Private Sub chkQual_Click()
  'Associa_Dato_In_TreeView TRWIstruzione, AULivello, "CKQ" & Format(AULivello, "00"), ChkQual.Value
  Associa_Dato_In_Memoria AULivello, "CKQ", ChkQual.Value
End Sub

Private Sub cmdAddClone_Click()
  is_AddClone = True
  ' Mauro 07/05/2008 : Aggiunto l'IdPgm nella lista
  'Madrdf_EditClone.idOggetto = Val(lswRighe.ListItems(lswRighe.SelectedItem.index))
  'Madrdf_EditClone.idPgm = Val(Trim(Mid(lswRighe.ListItems(lswRighe.SelectedItem.index).ToolTipText, InStr(lswRighe.ListItems(lswRighe.SelectedItem.index).ToolTipText, " "))))
  'Madrdf_EditClone.Riga = Val(lswRighe.ListItems(lswRighe.SelectedItem.index).ListSubItems(1))
  Madrdf_EditClone.idPgm = Val(lswRighe.ListItems(lswRighe.SelectedItem.index))
  Madrdf_EditClone.IdOggetto = Val(lswRighe.ListItems(lswRighe.SelectedItem.index).ListSubItems(L_IDOGG))
  Madrdf_EditClone.Riga = Val(lswRighe.ListItems(lswRighe.SelectedItem.index).ListSubItems(L_LINE))
  
  Load Madrdf_EditClone
  Madrdf_EditClone.Show vbModal
End Sub

Private Sub CmdAddlev_Click()
  Dim rsLiv As Recordset
  Dim newLevel As Integer
  
  newLevel = 1
  ' Mauro 07/05/2008 : e l'IdPgm?!?!
  'Set rsLiv = m_fun.Open_Recordset("Select Max(Livello) as maxl from PsDLI_IstrDett_Liv Where " & _
  '                                 "IdOggetto = " & Dli2Rdbms.drIdOggetto & " and Riga = " & Istruzione.Riga)
  Set rsLiv = m_fun.Open_Recordset("Select Max(Livello) as maxl from PsDLI_IstrDett_Liv Where " & _
                                   "IdOggetto = " & lswRighe.SelectedItem.ListSubItems(L_IDOGG) & " and " & _
                                   "idpgm = " & lswRighe.SelectedItem & " and Riga = " & Istruzione.Riga)
  If Not IsNull(rsLiv!maxl) Then
    newLevel = rsLiv!maxl + 1
  End If
  rsLiv.Close
  Set rsLiv = m_fun.Open_Recordset("Select * from PsDLI_IstrDett_Liv")
  rsLiv.AddNew
  rsLiv!livello = newLevel
  ' Mauro 07/05/2008
  'rsLiv!idOggetto = Dli2Rdbms.drIdOggetto
  'rsLiv!idPgm = Dli2Rdbms.drIdOggetto
  rsLiv!IdOggetto = lswRighe.SelectedItem.ListSubItems(L_IDOGG)
  rsLiv!idPgm = lswRighe.SelectedItem
  rsLiv!Riga = Istruzione.Riga
  rsLiv.Update
  rsLiv.Close
  RefreshRiga
End Sub

Private Sub CmdAddKey_Click()
  Dim rsKey As Recordset
  Dim newKey As Integer, Liv As Integer
  Dim wIdOgg As Long, wIdPgm As Long, wRiga As Long
    
  If TRWIstruzione.SelectedItem Is Nothing Then
    MsgBox "Invalid Instruction Level!", vbCritical, "DLI Instructions Corrector"
    Exit Sub
  End If
  Liv = Val(Mid(TRWIstruzione.Nodes(TRWIstruzione.SelectedItem.index).key, 4))
  If Liv = 0 Then
    MsgBox "Invalid Instruction Level!", vbCritical, "DLI Instructions Corrector"
    Exit Sub
  End If
  
  wIdPgm = lswRighe.SelectedItem
  wIdOgg = lswRighe.SelectedItem.ListSubItems(L_IDOGG)
  wRiga = Istruzione.Riga
  newKey = 1
  ' Mauro 07/05/2008
  'Set rsKey = m_fun.Open_Recordset("Select Max(Progressivo) as maxk from PsDLI_IstrDett_Key Where " & _
  '                                 "IdOggetto = " & Dli2Rdbms.drIdOggetto & " and Riga = " & Istruzione.Riga & "  and " & _
  '                                 "livello = " & Liv)
  Set rsKey = m_fun.Open_Recordset("Select Max(Progressivo) as maxk from PsDLI_IstrDett_Key Where " & _
                                   "idpgm = " & wIdPgm & " and IdOggetto = " & wIdOgg & " and Riga = " & wRiga & " and " & _
                                   "livello = " & Liv)
  If Not IsNull(rsKey!maxk) Then
    newKey = rsKey!maxk + 1
  End If
  rsKey.Close
  
''  ' Mauro 19/12/2007 : Lo devo fare! mi serve l'idPgm!!!
''  Set rsKey = m_fun.Open_Recordset("Select idpgm from PsDLI_IstrDett_Key Where " & _
''                                   "IdOggetto = " & Dli2Rdbms.drIdOggetto & " and Riga = " & Istruzione.Riga)
''  If rsKey.RecordCount Then
''    wIdOgg = Dli2Rdbms.drIdOggetto
''    wIdPgm = rsKey!idPgm
''    wRiga = Istruzione.Riga
''  End If
''  rsKey.Close
  
  Set rsKey = m_fun.Open_Recordset("Select * from PsDLI_IstrDett_Key")
  rsKey.AddNew
  rsKey!Progressivo = newKey
  rsKey!livello = Liv
  rsKey!IdOggetto = wIdOgg
  rsKey!idPgm = wIdPgm
  rsKey!Riga = wRiga
  rsKey.Update
  rsKey.Close
  RefreshRiga
End Sub

Private Sub cmdDelClone_Click()
  Dim risp As Integer, rispMgDLI As Integer
  Dim rs As Recordset
  Dim i As Long
  Dim IdOgg As Long, idPgm As Long, Riga As Long
  
  On Error GoTo errorHandler
  risp = MsgBox("Do you want to delete these Clones?", vbYesNo + vbInformation + vbDefaultButton2, MadrdF_Corrector.Caption)
  For i = lswCloni.ListItems.Count To 1 Step -1
    If lswCloni.ListItems(i).Selected Then
      If risp = vbYes Then
        ' Cancello il clone selezionato dalla PsDLI_IstrCodifica
        ' Mauro 07/05/2008
        'IdOgg = Val(lswRighe.SelectedItem)
        'idPgm = Val(Trim(Mid(lswRighe.SelectedItem.ToolTipText, InStr(lswRighe.SelectedItem.ToolTipText, " "))))
        'Riga = Val(lswRighe.SelectedItem.ListSubItems(1))
        idPgm = Val(lswRighe.SelectedItem)
        IdOgg = Val(lswRighe.SelectedItem.ListSubItems(L_IDOGG))
        Riga = Val(lswRighe.SelectedItem.ListSubItems(L_LINE))
                
        ' Notifico la cancellazione sulla Tabella delle Modifiche-Cloni MgDLI_IstrCodifica
        Set rs = m_fun.Open_Recordset("select * from MgDLI_IstrCodifica where " & _
                                      "idoggetto = " & IdOgg & " and idpgm = " & idPgm & " And riga = " & Riga & " and " & _
                                      "decodifica = '" & lswCloni.SelectedItem.SubItems(1) & "'")
        If rs.RecordCount Then
          rs.Delete
        Else
          rs.AddNew
          
          rs!idPgm = idPgm
          rs!IdOggetto = IdOgg
          rs!Riga = Riga
          rs!Decodifica = lswCloni.SelectedItem.SubItems(1)
          rs!Status = "D"
          
          rs.Update
        End If
        rs.Close
        
        m_fun.FnConnection.Execute "delete * from psdli_istrcodifica where " & _
                                   "idoggetto = " & IdOgg & " and idpgm = " & idPgm & " And riga = " & Riga & " and " & _
                                   "codifica = '" & lswCloni.ListItems(i) & "'"
        
        ' Controllo se ci sono altri programmi che usano il clone...
        Set rs = m_fun.Open_Recordset("select * from psdli_istrcodifica where " & _
                                      "codifica = '" & lswCloni.ListItems(i) & "'")
                                      
        rispMgDLI = 0
        If rs.RecordCount = 0 Then
          rispMgDLI = MsgBox("Clone " & lswCloni.ListItems(i) & " isn't used by other programs! Do you want to delete decode?", _
                             vbYesNo + vbInformation + vbDefaultButton2, MadrdF_Corrector.Caption)
          If rispMgDLI = vbYes Then
            ' ...Cancello la decodifica
            m_fun.FnConnection.Execute "delete * from mgdli_decodificaistr where " & _
                                       "codifica = '" & lswCloni.ListItems(i) & "'"
          End If
        Else
          rispMgDLI = MsgBox("Clone " & lswCloni.ListItems(i) & " is used by other " & rs.RecordCount & " programs! Do you want to delete them all?", _
                             vbYesNo + vbInformation + vbDefaultButton2, MadrdF_Corrector.Caption)
          If rispMgDLI = vbYes Then
            ' ...Cancello la decodifica
            m_fun.FnConnection.Execute "delete * from mgdli_decodificaistr where " & _
                                       "codifica = '" & lswCloni.ListItems(i) & "'"
            m_fun.FnConnection.Execute "delete * from psdli_istrcodifica where " & _
                                       "codifica = '" & lswCloni.ListItems(i) & "'"
          End If
        End If
        lswCloni.ListItems.Remove (i)
      End If
    End If
  Next i
  
  Exit Sub
errorHandler:
  MsgBox err.Number & " " & err.Description
End Sub

Private Sub cmdOperatore_Click()
  GbIntFormSel = 1
  MadrdF_Operatori.Show
End Sub

Private Sub cmdRemoveDBD_Click()
  Dim i As Long
   
  If lswDBD.ListItems.Count Then
    'SQ no comment...
    'For i = UBound(Istruzione.Database) To mSel_Idx_Database + 1 Step - 1
    For i = mSel_Idx_Database + 1 To UBound(Istruzione.Database)
      'If i > 1 Then
      Istruzione.Database(i - 1) = Istruzione.Database(i)
      'End If
    Next i
    ReDim Preserve Istruzione.Database(UBound(Istruzione.Database) - 1)
    'Remove a video
    lswDBD.ListItems.Remove mSel_Idx_Database
  End If
End Sub

Private Sub chkIstrFLAG_Click()
  Aggiorna_Icone_TreeView_Istruzione chkIstrFLAG.Value, TRWIstruzione
End Sub

Private Sub cmdAddDBD_Click()
  gbTypeToLoad = "DBD"
  
  Dim oj As New MadrdF_ListPsbDBD
  Dim i As Long
  Dim s() As String
  Dim Id() As String
  
  oj.Show vbModal
  
  If oj.Id <> "#" Then
    s = Split(oj.SelName, "#")
    Id = Split(oj.Id, "#")
    
    If UBound(s) Then
      For i = 0 To UBound(s) - 1
        lswDBD.ListItems.Add , , s(i)
        lswDBD.ListItems(lswDBD.ListItems.Count).tag = Format(Id(i), "######")
      Next i
    End If
    
    'associa il dato in memoria
    Associa_Dato_In_Memoria AULivello, "DBD", ""
  End If
  
  Set oj = Nothing
End Sub

Private Sub cmdAddPsb_Click()
  gbTypeToLoad = "PSB"
  
  Dim oj As New MadrdF_ListPsbDBD
  Dim i As Long
  Dim s() As String
  Dim Id() As String
  
  oj.Show vbModal
  
  If oj.Id <> "#" Then
    s = Split(oj.SelName, "#")
    Id = Split(oj.Id, "#")
    
    If UBound(s) > 0 Then
      For i = 0 To UBound(s) - 1
        lswPsb.ListItems.Add , , s(i)
        lswPsb.ListItems(lswPsb.ListItems.Count).tag = Format(Id(i), "######")
      Next i
    End If
    
    'associa il dato in memoria
    Associa_Dato_In_Memoria AULivello, "PSB", ""
  End If
  
  Set oj = Nothing
End Sub

Private Sub cmdKey_Click()
  GbIntFormSel = 0
  MadrdF_ChiaveViewer.Show
End Sub

Private Sub cmdOpMulti_Click()
  GbIntFormSel = 0
  MadrdF_Operatori.Show
End Sub

Private Sub cmdRemovePsb_Click()
  Dim i As Long
  
  If lswPsb.ListItems.Count Then
    'SG : Elimina il riferimento al dbd
    For i = UBound(Istruzione.psb) To mSel_Idx_Psb Step -1
      If i > 1 Then
        Istruzione.psb(i - 1) = Istruzione.psb(i)
      End If
    Next i
    
    ReDim Preserve Istruzione.psb(UBound(Istruzione.psb) - 1)
    
    'Remove a video
    lswPsb.ListItems.Remove mSel_Idx_Psb
  End If
End Sub

Private Sub cmdSegmFind_Click()
  Dim oj As New MadrdF_ListPsbDBD
  Dim i As Long
  Dim s() As String
  Dim Id() As String
  Dim id1 As String
  
  gbTypeToLoad = "SEG"
  
  oj.Show vbModal
  
  oj.CheckObsolete
  id1 = oj.Id
  If id1 <> "#" Then
    s = Split(oj.SelName, "#")
    Id = Split(id1, "#")
    
    If UBound(s) Then
      txtsegm.text = ""
      txtsegm.tag = ""
      For i = 0 To UBound(s) - 1
        txtsegm.text = IIf(Len(txtsegm.text), txtsegm.text & ";", "") & s(i)
        txtsegm.tag = IIf(Len(txtsegm.tag), txtsegm.tag & ";", "") & Format(Id(i), "######")
      Next i
    End If
    
    txtsegm.tag = GestDuplicati("SEG", txtsegm.text)
    
    'associa il dato in memoria
    'alpitour: 5/8/2005: ma perch�???????
    'Associa_Dato_In_Memoria AULivello, "SEG", ""
    Associa_Dato_In_TreeView TRWIstruzione, AULivello, "SEG" & Format(AULivello, "00"), txtsegm.text
    Associa_Dato_In_Memoria AULivello, "SEG", txtsegm.text
  End If
  
  Set oj = Nothing
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  idLevel = 0
  Set collExpandedCopy = Nothing
  nrExpandedCopy = 0
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  
  'Resize delle liste
  LswLog.ColumnHeaders(1).Width = LswLog.Width
  
  'resize delle liste
  lswDBD.ColumnHeaders(1).Width = lswDBD.Width
  lswPsb.ColumnHeaders(1).Width = lswPsb.Width
  
  lswCloni.ColumnHeaders(1).Width = lswCloni.Width / 6
  lswCloni.ColumnHeaders(2).Width = lswCloni.Width / 6 * 15
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False

  SetFocusWnd m_fun.FnParent
End Sub

Private Sub lswCloni_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswCloni.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswCloni.SortOrder = Order
  lswCloni.SortKey = key - 1
  lswCloni.Sorted = True
End Sub

Private Sub lswDBD_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswDBD.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswDBD.SortOrder = Order
  lswDBD.SortKey = key - 1
  lswDBD.Sorted = True
End Sub

Private Sub lswDBD_DblClick()
  If lswDBD.ListItems.Count > 0 Then
    'MadrdF_PSB_DBD_Load.load_psb_dbd lswDBD.SelectedItem, lswDBD.SelectedItem.Tag
    m_fun.Show_ObjViewer lswDBD.SelectedItem.tag
  End If
End Sub

Private Sub lswDBD_ItemClick(ByVal item As MSComctlLib.ListItem)
  mSel_Idx_Database = item.index
End Sub

Private Sub LswLog_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = LswLog.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  LswLog.SortOrder = Order
  LswLog.SortKey = key - 1
  LswLog.Sorted = True
End Sub

Private Sub lswPsb_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswPsb.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswPsb.SortOrder = Order
  lswPsb.SortKey = key - 1
  lswPsb.Sorted = True
End Sub

Private Sub lswPsb_DblClick()
  If lswPsb.ListItems.Count > 0 Then
    'MadrdF_PSB_DBD_Load.load_psb_dbd lswPsb.SelectedItem, lswPsb.SelectedItem.Tag
    m_fun.Show_ObjViewer lswPsb.SelectedItem.tag
  End If
End Sub

Private Sub lswPsb_ItemClick(ByVal item As MSComctlLib.ListItem)
  mSel_Idx_Psb = item.index
End Sub

Private Sub lswRighe_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswRighe.SortOrder
  
  key = ColumnHeader.index
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswRighe.SortOrder = Order
  lswRighe.SortKey = key - 1
  lswRighe.Sorted = True
End Sub

Private Sub lswRighe_DblClick()
  'Si posiziona sulla RichTextBox all'istruzione selezionata
  Dim Riga As String
  Dim i As Long, k As Long
  Dim x() As String, y() As String
    
  If copyExpanded Then
    RTIncaps.text = ""
    RTIncaps.text = RtBefore
    RTIncaps.SelStart = 1
    RTIncaps.SelLength = Len(RTIncaps.text)
    RTIncaps.SelColor = vbGreen
    RTIncaps.find " ", 0, 100
  
    idLevel = 0
    
    LswLog.ListItems.Add , , "Expansion Level nr. " & idLevel
   
    Set collExpandedCopy = Nothing
    nrExpandedCopy = 0
    copyExpanded = False
   
    Toolbar1.Buttons("Copybooks").Enabled = True
    Toolbar1.Buttons("Hide_Copybooks").Enabled = False
  End If
    
  If lswRighe.ListItems.Count Then
    ' Mauro 29/09/2010
    ' Voglio caricare una copy, ma sono sul pgm
    If Not Dli2Rdbms.drIdOggetto = lswRighe.SelectedItem.ListSubItems(L_IDOGG) And Not copyFileLoad Then
      Carica_File_In_RichTextBox LswLog, RTIncaps, lswRighe.SelectedItem.ListSubItems(L_IDOGG)
      copyFileLoad = True
      IdCopyFileLoad = lswRighe.SelectedItem.ListSubItems(L_IDOGG)
      RTIncaps.SelStart = 1
      RTIncaps.SelLength = Len(RTIncaps.text)
      RTIncaps.SelColor = vbGreen
      copyExpanded = False
    ' Voglio caricare il pgm, ma sono su una copy
    ElseIf Dli2Rdbms.drIdOggetto = lswRighe.SelectedItem.ListSubItems(L_IDOGG) And copyFileLoad Then
      Carica_File_In_RichTextBox LswLog, RTIncaps, lswRighe.SelectedItem.ListSubItems(L_IDOGG)
      copyFileLoad = False
      RTIncaps.SelStart = 1
      RTIncaps.SelLength = Len(RTIncaps.text)
      RTIncaps.SelColor = vbGreen
    ' Voglio caricare una copy, ma sono su un'altra copy
    ElseIf IdCopyFileLoad <> lswRighe.SelectedItem.ListSubItems(L_IDOGG) And copyFileLoad Then
      Carica_File_In_RichTextBox LswLog, RTIncaps, lswRighe.SelectedItem.ListSubItems(L_IDOGG)
      copyFileLoad = True
      IdCopyFileLoad = lswRighe.SelectedItem.ListSubItems(L_IDOGG)
      RTIncaps.SelStart = 1
      RTIncaps.SelLength = Len(RTIncaps.text)
      RTIncaps.SelColor = vbGreen
      copyExpanded = False
    End If
    
    If Not copyExpanded Then
      'prima riporto il colore delle righe al default
      RTIncaps.SelStart = 1
      RTIncaps.SelLength = Len(RTIncaps.text)
      RTIncaps.SelColor = vbGreen

      x = Split(arrayRighe(lswRighe.SelectedItem.SubItems(L_LINE) - 1), ";")
      y = Split(arrayRighe(lswRighe.SelectedItem.SubItems(L_LINE) + lswRighe.SelectedItem.tag - 2), ";")
      Riga = Mid(RTIncaps.text, x(0) + 1, (y(1) - x(0)))
      RTIncaps.SelStart = x(0) + 1
      RTIncaps.SelLength = y(1) - x(0)
      RTIncaps.SelColor = vbWhite
      RTIncaps.SelLength = 0
    Else
      ' Mauro 07/05/2008
      'selRiga RTIncaps, (Val(lineeIstr(lswRighe.SelectedItem.index)) - 1), lswRighe.SelectedItem.SubItems(2), lswRighe.SelectedItem.tag
      selRiga RTIncaps, (Val(lineeIstr(lswRighe.SelectedItem.index)) - 1), lswRighe.SelectedItem.SubItems(L_INSTR), lswRighe.SelectedItem.tag
    End If
    CmdAddlev.Enabled = True
    CmdAddKey.Enabled = True
  End If
End Sub

Public Sub lswRighe_ItemClick(ByVal item As MSComctlLib.ListItem)
  Dim pIdPgm As Long, strpgm As String
'  'Si posiziona sulla RichTextBox all'istruzione selezionata
'  Dim Riga As String
'  Dim i As Long
'  Dim k As Long
'  Dim x() As String
'  Dim y() As String
'
''  riga = Item.Tag ' si tratta del numero di riga scritto in esadecimale
''  i = RTIncaps.Find(riga, 1)
'  If Not copyExpanded Then
'    'ALE vado al numero di riga scritto nel campo x della lsw:
''    x = Split(arrayRighe(lswRighe.SelectedItem.SubItems(1) - 1), ";")
''    RTIncaps.Find lswRighe.SelectedItem.SubItems(2), x(0), x(1)
'
'    x = Split(arrayRighe(lswRighe.SelectedItem.SubItems(1) - 1), ";")
'    y = Split(arrayRighe(lswRighe.SelectedItem.SubItems(1) + lswRighe.SelectedItem.Tag - 2), ";")
'    Riga = Mid(RTIncaps.Text, x(0) + 1, (y(1) - x(0)))
'    'RTIncaps.Find Riga, x(0) - 1, y(1) + 1
'    RTIncaps.SelStart = x(0) + 1
'    RTIncaps.SelLength = y(1) - x(0)
'    RTIncaps.SelColor = vbWhite
'    RTIncaps.SelLength = 0
'  Else
''    x = Split(arrayRighe(lineeIstr(lswRighe.SelectedItem.Index) - 1), ";")
''    RTIncaps.Find lswRighe.SelectedItem.SubItems(2), x(0), x(1) 'lswRighe.SelectedItem.SubItems(2)
'     selRiga RTIncaps, (Val(lineeIstr(lswRighe.SelectedItem.Index)) - 1), lswRighe.SelectedItem.SubItems(2), lswRighe.SelectedItem.Tag
'
'  End If
  ' Mauro 07/05/2008
  'strpgm = Trim(Right(lswRighe.SelectedItem.ToolTipText, Len(lswRighe.SelectedItem.ToolTipText) - InStr(lswRighe.SelectedItem.ToolTipText, ":")))
  'pIdPGM = CLng(strpgm)
  'pIdPGM = CLng(lswRighe.SelectedItem)
  'CaricaWarnings lswRighe.SelectedItem, lswRighe.SelectedItem.SubItems(1), pIdPGM
  CaricaWarnings lswRighe.SelectedItem.SubItems(L_IDOGG), lswRighe.SelectedItem.SubItems(L_LINE), lswRighe.SelectedItem
  CmdAddlev.Enabled = True
  CmdAddKey.Enabled = True
  ReDim Mappa_Immagini_TreeView(0)
  RefreshRiga
    
  'Carica_ListaCloni lswRighe.SelectedItem, lswRighe.SelectedItem.SubItems(1), pIdPGM
  ' Mauro 22/05/2008 : Devo caricare i cloni solo se non c'� il check valorizzato
  If chkAllClones.Value = 0 Then
    Carica_ListaCloni lswRighe.SelectedItem.SubItems(L_IDOGG), lswRighe.SelectedItem.SubItems(L_LINE), lswRighe.SelectedItem
  End If
End Sub

Public Function selRiga(RT As RichTextBox, numRiga As Long, istruz As String, totRighe As Long)
  'Idx = RT.Find(vbCrLf, OldPos) 'ALE
  'array dalla riga 1 alla fine contenenti gli  indici del carattere capo linea e fine linea
  'es:rt.Find("GDEL",x1,x2)
  Dim Idx As Long
  Dim Riga As Long
  Dim x1 As Long
  Dim x2 As Long
  Dim i As Long
  Dim numRiga2 As Long
  Dim xInit As Long
  Dim xFine As Long
  Dim strRiga As String
  On Error GoTo errorHandler
  
  numRiga2 = numRiga + totRighe - 1
  i = 0
  x1 = 1
  Do Until Idx = -1
    Idx = RT.find(vbCrLf, x1)
    x2 = Idx
    Riga = RT.GetLineFromChar(Idx)
    If Riga = numRiga Then
      xInit = x1
'      RT.Find istruz, x1, x2
'      Exit Do
    End If
    If Riga = numRiga2 Then
      xFine = x2
'      strRiga = Mid(RTIncaps.Text, xInit + 1, (xFine - xInit))
'      RT.Find strRiga, xInit, xFine
      RT.SelStart = xInit
      RT.SelLength = xFine - xInit
      RT.SelColor = vbWhite
      RT.SelLength = 0
      Exit Do
    End If
    x1 = x2 + 2
  Loop
  Exit Function
errorHandler:
  Resume Next
End Function

Private Sub Form_Load()
'''  'AC 03/08/07
'''  'dichiarazione costanti listview istruzioni
'''  Const L_ID = 0
'''  Const L_LINE = 1
'''  Const L_INSTRUCT = 2
'''  Const L_STATEMENT = 3
'''  Const L_IDPSB = 4
  
  ' Mauro 15/12/2005: disabilito i tasti di default
  copyExpanded = False
  copyFileLoad = False
  IdCopyFileLoad = 0
  'Toolbar1.ImageList = imlToolbarIcons(1)
  'Toolbar1.Buttons(1).Image = 3
  'Toolbar1.Buttons(2).Image = 9
  'Toolbar1.Buttons(3).Image = 11
  'Toolbar1.Buttons(4).Image = 12
  cmdConferma.Enabled = False
  cmdAddDBD.Enabled = False
  cmdRemoveDBD.Enabled = False
  cmdAddPsb.Enabled = False
  cmdRemovePsb.Enabled = False
  cmdOpMulti.Enabled = False
  cmdSegmFind.Enabled = False
  cmdKey.Enabled = False
  
  LswLog.ListItems.Clear
  
  gIdDbd = -1
  gIdPsb = -1
  gIdField = -1
  
  lblIstrLev(0).Caption = "Current Level instruction <not selected>"
  lblIstrLev(1).Caption = "Current Level instruction <not selected>"
  
  RTIncaps.text = "SELCOLOR"
  RTIncaps.SelStart = 1
  RTIncaps.SelLength = Len(RTIncaps.text)
  RTIncaps.SelColor = vbGreen
  
  'Setta il parent
  'SetParent Me.hwnd, Dli2RdbMs.drParent
  
  'm_fun.FnHeight = MadrdF_Verifica.Height 'ALE?????
  
  ' m_fun.ResizeControls Me
  
  '2) Carica la lista con tutte le istruzioni
  optAllInstr.Value = True 'settando questo fa scattare il corrispondente evento click
  chkErrInstr.Value = 0
  'ALE**********************
  Elimina_Flickering Me.hwnd
  TRWIstruzione.Nodes.Clear
  'Carica il file nella RichTextBox
  Carica_File_In_RichTextBox LswLog, RTIncaps, Dli2Rdbms.drIdOggetto
  RTIncaps.SelStart = 1
  RTIncaps.SelLength = Len(RTIncaps.text)
  RTIncaps.SelColor = vbGreen
  Carica_ListView_Istruzioni lswRighe, Dli2Rdbms.drIdOggetto, 1, RTIncaps
  RTIncaps.SelStart = 0
  RTIncaps.SelLength = 0
  LswLog.ListItems.Add , , "Loading " & LswLog.ListItems.Count & " Instruction(s) in list...."
  Terminate_Flickering
  '*************************
    
  Me.Caption = "IMS/DB/DC Corrector - Object : " & m_fun.Restituisci_NomeOgg_Da_IdOggetto(Dli2Rdbms.drIdOggetto)
  
  'Posiziona la lista sull'ultimo elemento
  LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
  'ale
  '  FraRighe.Move FraLog.Left, FraLog.Top
  '  FraIstr.Left = fraSSA.Left
  '  FraIstr.Top = fraSSA.Top
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub

Public Sub CaricaWarnings(IdOggetto As Long, Riga As Long, idPgm As Long)
  Dim r As Recordset
  Dim Mess As String
  Dim i As Long

  txtWarning.text = ""
  If idPgm = IdOggetto Then
    Set r = m_fun.Open_Recordset("Select Distinct IdOggetto, Codice, Riga From BS_Segnalazioni Where IdOggetto = " & IdOggetto & " and Riga = " & Riga & " order by codice desc")
  Else
    Set r = m_fun.Open_Recordset("Select Distinct IdOggetto, Codice, Riga From BS_Segnalazioni Where (IdOggetto = " & idPgm & " and  IdOggettoRel = " & IdOggetto & ") and Riga = " & Riga & " order by codice desc")
  End If
  Do Until r.EOF
    Mess = Componi_Messaggio_Errore(r!IdOggetto, r!Codice, r!Riga)
    txtWarning = txtWarning & " - " & Mess & vbCrLf
    r.MoveNext
  Loop
  r.Close
End Sub

Public Sub optAllInstr_Click()
  Carica_ListView_Istruzioni lswRighe, Dli2Rdbms.drIdOggetto, 1, RTIncaps
End Sub

Private Sub optIMSDB_Click()
  Carica_ListView_Istruzioni lswRighe, Dli2Rdbms.drIdOggetto, 2, RTIncaps
End Sub

Private Sub optIMSDC_Click()
  Carica_ListView_Istruzioni lswRighe, Dli2Rdbms.drIdOggetto, 3, RTIncaps
End Sub

Private Sub chkErrInstr_Click()
  If optAllInstr Then
    Carica_ListView_Istruzioni lswRighe, Dli2Rdbms.drIdOggetto, 1, RTIncaps
  ElseIf optIMSDB Then
    Carica_ListView_Istruzioni lswRighe, Dli2Rdbms.drIdOggetto, 2, RTIncaps
  Else
    Carica_ListView_Istruzioni lswRighe, Dli2Rdbms.drIdOggetto, 3, RTIncaps
  End If
End Sub

Private Sub LswLog_ItemClick(ByVal item As MSComctlLib.ListItem)
  Dim Marcatore As String
  
  Select Case Trim(UCase(Mid(item.tag, 1, 4)))
    Case "LOAD" ' Ha caricato un file
      'Formalismo : LOAD[Percorso]
    
    Case "EXPD" ' Ha trovato la copy e l'ha espansa
      'Formalismo : EXPD[6byte marcatore][percorso]
      Marcatore = Mid(item.tag, 5, 6)
            
    Case "EXPE" ' Non ha espanso la copy
      'Formalismo : EXPD[6byte marcatore][Nome]
      Marcatore = Mid(item.tag, 5, 6)
      
    Case "FIND" ' Testo trovato
      'Formalismo : FIND[6byte marcatore]
      Marcatore = Mid(item.tag, 5, 6)
    
    Case "SELT" ' Testo selezionato
      'Formalismo : SELT[6byte marcatore]
      Marcatore = Mid(item.tag, 5, 6)
      
  End Select
  
  If Trim(Marcatore) <> "" Then
    RTIncaps.find Marcatore, 1
  End If
  
  If RTIncaps.SelStart Then
    RTIncaps.SelStart = RTIncaps.SelStart - 80
    RTIncaps.SelLength = 80
  End If
End Sub

Public Sub RefreshRiga()
  Dim NumeroLivelli As Long
  Dim ExRiga As Boolean
  Dim istruz As String
  Dim rpos As Long
  Dim rs As New Recordset
  Dim rsCall As New Recordset
  Dim Rigaistruzione As String
  Dim IstrValida As Boolean

  Dim i As Long
  
  On Error GoTo subErr
  
  'mauro: 15/12/2005: Abilito i tasti solo se seleziono un'istruzione
  cmdConferma.Enabled = True
  cmdAddDBD.Enabled = True
  cmdRemoveDBD.Enabled = True
  cmdAddPsb.Enabled = True
  cmdRemovePsb.Enabled = True
  cmdOpMulti.Enabled = True
  cmdSegmFind.Enabled = True
  cmdKey.Enabled = True
  
  'pulisce tutte le textbox
  ImgSegmenti.Picture = Nothing
  Cancella_TextBox_Istruzione
  
  'Carica_Array_Istruzione lswRighe.SelectedItem.ListSubItems(1), Dli2Rdbms.drIdOggetto
  'Carica_Istruzione Replace(lswRighe.SelectedItem.ToolTipText, "IdPgm: ", ""), lswRighe.SelectedItem.ListSubItems(1), Dli2Rdbms.drIdOggetto
  'AC
  ' Mauro 07/05/2008
  'Carica_Istruzione Replace(lswRighe.SelectedItem.ToolTipText, "IdPgm: ", ""), lswRighe.SelectedItem.ListSubItems(1), lswRighe.SelectedItem
  Carica_Istruzione lswRighe.SelectedItem, lswRighe.SelectedItem.ListSubItems(L_LINE), lswRighe.SelectedItem.ListSubItems(L_IDOGG)
  
  'AC
  If UBound(Istruzione.SSA) > 0 Then
    fraSSA.Caption = "Level 01"
    lblIstrLev(0).Caption = "Current Instruction Level 01"
    lblIstrLev(1).Caption = "Current Instruction Level 01"
    lblKeyLev(0).Caption = "Current key level <not selected>"
    AULivello = 1
    Carica_TextBox_Istruzione "LEV01"
    ' Mauro 16/10/2007 : Non disabilito tutto il pannello, ma solo i singoli campi
    'SSTab3.Enabled = True
    txtsegm.Enabled = True
    cmdSegmFind.Enabled = True
    txtSegLen.Enabled = True
    txtStrutt.Enabled = True
    txtSSAName.Enabled = True
    cmdKey.Enabled = True
    txtComCode.Enabled = True
    ChkQual.Enabled = True
    ChkBoth.Enabled = True
    SSTab3.TabEnabled(1) = True
  Else
    lblIstrLev(0).Caption = "No Current Instruction Level"
    lblIstrLev(1).Caption = "No Current Instruction Level"
    lblKeyLev(0).Caption = "Current key level <not selected>"
    AULivello = 1
    ' Mauro 16/10/2007 : Non disabilito tutto il pannello, ma solo i singoli campi
    'SSTab3.Enabled = False
    txtsegm.Enabled = False
    cmdSegmFind.Enabled = False
    txtSegLen.Enabled = False
    txtStrutt.Enabled = False
    txtSSAName.Enabled = False
    cmdKey.Enabled = False
    txtComCode.Enabled = False
    ChkQual.Value = 0
    ChkQual.Enabled = False
    ChkBoth.Value = 0
    ChkBoth.Enabled = False
    SSTab3.TabEnabled(1) = False
  End If

  '**************
  ' Mauro 07/05/2008
  'IstrValida = Carica_TreeView_Dettaglio_Istruzioni(TRWIstruzione, lswRighe.SelectedItem.ListSubItems(1))
  IstrValida = Carica_TreeView_Dettaglio_Istruzioni(TRWIstruzione, lswRighe.SelectedItem.ListSubItems(L_LINE))
  
  'Load generalit� istruzioni
  lswDBD.ListItems.Clear
  For i = 1 To UBound(Istruzione.Database)
    lswDBD.ListItems.Add , , Istruzione.Database(i).nome
    lswDBD.ListItems(lswDBD.ListItems.Count).tag = Format(Istruzione.Database(i).IdOggetto, "######")
  Next i
  
  lswPsb.ListItems.Clear
  For i = 1 To UBound(Istruzione.psb)
    lswPsb.ListItems.Add , , Istruzione.psb(i).nome
    lswPsb.ListItems(lswPsb.ListItems.Count).tag = Format(Istruzione.psb(i).IdOggetto, "######")
  Next i
  
  'Incolla l'icona giusta sulla TreeView e abilita i bottoni
  'txtIstruzione = Istruzione.Istruzione
  
  '****
  ' Mauro 07/05/2008
  'Set rs = m_fun.Open_Recordset("Select isExec from PsDLI_Istruzioni where " & _
  '                                       "IdPgm = " & Replace(lswRighe.SelectedItem.ToolTipText, "IdPgm: ", "") & " And " & _
  '                                       "IdOggetto = " & Dli2Rdbms.drIdOggetto & " and riga = " & Istruzione.Riga)
  Set rs = m_fun.Open_Recordset("Select isExec from PsDLI_Istruzioni where " & _
                                         "IdPgm = " & lswRighe.SelectedItem & " And " & _
                                         "IdOggetto = " & lswRighe.SelectedItem.ListSubItems(L_IDOGG) & " and riga = " & Istruzione.Riga)
  If Not rs.EOF Then
    If rs!isExec Then
      Set rsCall = m_fun.Open_Recordset("Select Comando,Istruzione from PsExec where " & _
                                        "Area = 'DLI' and IdOggetto = " & Dli2Rdbms.drIdOggetto & " and " & _
                                        "riga = " & Istruzione.Riga)
      If Not rsCall.EOF Then
        Rigaistruzione = "EXEC DLI " & _
                         IIf(rsCall!Comando = "SCHD" Or rsCall!Comando = "SCHEDULE", "SCHD PSB", rsCall!Comando) & vbCrLf & _
                         rsCall!Istruzione & vbCrLf & _
                         "END-EXEC"
      End If
      rsCall.Close
    Else
      Set rsCall = m_fun.Open_Recordset("Select Programma,Parametri from PsCall where " & _
                                        "IdOggetto = " & Dli2Rdbms.drIdOggetto & " and riga = " & Istruzione.Riga)
      If Not rsCall.EOF Then
        Rigaistruzione = "CALL " & Trim(rsCall!Programma) & " USING " & Trim(rsCall!parametri)
      End If
      rsCall.Close
    End If
    rs.Close
  End If
  txtIstruzione = Rigaistruzione
 
  'Se l'istruzione � valida chekka la check bo
  chkIstrFLAG.Value = 0
  
  If Istruzione.valida Then
    chkIstrFLAG.Value = 1
  Else
    chkIstrFLAG.Value = 0
  End If
  If Istruzione.obsolete Then
    CheckObsolete.Value = 1
  Else
    CheckObsolete.Value = 0
  End If
  If Istruzione.to_migrate Then
    CheckToMigrate.Value = 1
  Else
    CheckToMigrate.Value = 0
  End If
  Exit Sub
subErr:
  MsgBox "tmp: " & err.Description
  'Resume
End Sub

Private Sub RTIncaps_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
'  Seleziona_Word_E_MarcaWord RTIncaps, LswLog
End Sub

Private Sub RTIncaps_KeyPress(KeyAscii As Integer)
  'gloria
  If KeyAscii = 6 Then
    AUSelText = RTIncaps.SelText
    m_fun.FindTxt AUSelText
    AUSelText = m_fun.txtModif
    If m_fun.txtSearch Then
      Avvia_Ricerca_Ricorsiva_Parola RTIncaps, LswLog, AUSelText
      SSTab2.Tab = 1
    End If
  End If
 
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim oFindForm As Form, i As Long
  
  On Error Resume Next
  Select Case Button.key
    Case "Find"
      'MF 02/08/07
      'Gestione ricerca tramite apertura form TxtFind
      AUSelText = RTIncaps.SelText
      m_fun.FindTxt AUSelText
      AUSelText = m_fun.txtModif
      If m_fun.txtSearch Then
        Avvia_Ricerca_Ricorsiva_Parola RTIncaps, LswLog, AUSelText
        SSTab2.Tab = 1
      End If
  
    Case "OPENTEXTED"
      'Lancia Il text editor
      m_fun.Show_TextEditor Dli2Rdbms.drIdOggetto, , , Me
    
    Case "Copybooks"
      copyExpanded = True
  
      If idLevel = 0 Then
        RtBefore = RTIncaps.text
      End If
      If lswRighe.ListItems.Count Then
        ReDim lineeIstr(lswRighe.ListItems.Count)
        For i = 1 To lswRighe.ListItems.Count
          ' Mauro 07/05/2008
          'lineeIstr(i) = lswRighe.ListItems(i).SubItems(1)
          lineeIstr(i) = lswRighe.ListItems(i).SubItems(L_LINE)
        Next i
      End If
      If Restituisci_TipoOgg_Da_IdOggetto(Dli2Rdbms.drIdOggetto) = "PLI" Then
        Expand_cpyNew LswLog, RTCpyApp, RTIncaps, "INCLUDE"
      Else
        Expand_cpyNew LswLog, RTCpyApp, RTIncaps, " COPY "
      End If
      
      LswLog.ListItems.Add , , "Expansion Level nr. " & idLevel
       
      If nrExpandedCopy > 0 Then
        Toolbar1("Hide_Copybooks").Enabled = True
      Else
        Toolbar1("Hide_Copybooks").Enabled = False
      End If
   
    Case "Hide_Copybooks"
      If Not copyExpanded Then Exit Sub
      
      RTIncaps.text = ""
      RTIncaps.text = RtBefore
      RTIncaps.SelStart = 1
      RTIncaps.SelLength = Len(RTIncaps.text)
      RTIncaps.SelColor = vbGreen
      RTIncaps.find " ", 0, 100
     
      idLevel = 0
      
      LswLog.ListItems.Add , , "Expansion Level nr. " & idLevel
      
      Set collExpandedCopy = Nothing
      nrExpandedCopy = 0
      copyExpanded = False
      
      Toolbar1("Copybooks").Enabled = True
      Toolbar1("Hide_Copybooks").Enabled = False
  End Select
End Sub

Private Sub TRWIstruzione_Collapse(ByVal Node As MSComctlLib.Node)
  TRWIstruzione_NodeClick Node
End Sub

Private Sub TRWIstruzione_Expand(ByVal Node As MSComctlLib.Node)
  TRWIstruzione_NodeClick Node
End Sub

Private Sub TRWIstruzione_NodeClick(ByVal Node As MSComctlLib.Node)
  Dim i As Long
  
  Select Case UCase(Mid(Node.key, 1, 3))
    Case "LEV" 'Siamo su un livello 'Carica Le text Box
      'Carica le text Box
      Cancella_TextBox_Istruzione
      
      Carica_TextBox_Istruzione Node.key
      
      AULivello = Val(Mid(Node.key, 4))
      
      lblIstrLev(0).Caption = "Current level instruction : " & Format(AULivello, "00")
      lblIstrLev(1).Caption = "Current level instruction : " & Format(AULivello, "00")
      lblKeyLev(0).Caption = "Current key level : " & Format(AUSelIndexKey, "00")
      
    Case "DBD"
      Cancella_TextBox_Istruzione
    
    Case "PSB"
      Cancella_TextBox_Istruzione
    
    Case "IST"
      'AC
      'Cancella_TextBox_Istruzione
    
    Case Else
      If Trim(Node.key) <> "" Then
        'Sono in un livello dell'istruzione
        'Torna in su fino al primo nodo
        For i = Node.index To 1 Step -1
          If Trim(UCase(Mid(TRWIstruzione.Nodes(i).key, 1, 3))) = "LEV" Then
            AULivello = Val(Mid(Node.key, 4))
            
            Carica_TextBox_Istruzione TRWIstruzione.Nodes(i).key
            
            lblIstrLev(0).Caption = "Current instruction level : " & Format(AULivello, "00")
            lblIstrLev(1).Caption = "Current instruction level : " & Format(AULivello, "00")
            
            lblKeyLev(0).Caption = "Current key level : " & Format(AUSelIndexKey, "00")
            Exit For
          End If
        Next i
      Else
        'Sono sulle Info dell'istruzione
        'Cancella_TextBox_Istruzione
        Exit Sub
      End If
  End Select
End Sub

Private Sub Carica_TextBox_Istruzione(Nodo As String)
  Dim Liv As Long, i As Long, Idx As Long
  Dim mNode As Node
  
  'Prende il nodo selezionato
  Set mNode = TRWIstruzione.SelectedItem
  
  Cancella_TextBox_Istruzione
  
  'Prende il Livello
  Liv = Val(Mid(Nodo, 4))
  AULivello = Liv
  If Not mNode Is Nothing Then
    AUSelIndexKey = Val(mNode.tag)
  Else
    AUSelIndexKey = 1
  End If
  
  For i = 1 To UBound(Istruzione.SSA)
    If Istruzione.SSA(i).livello = Liv Then
      Idx = i
    End If
  Next i
  
  If Idx = 0 Then
    Idx = Liv
  End If
  'txtsegm = Istruzione.SSA(Idx).NomeSegmento
  If Len(Istruzione.SSA(Idx).NomeSegmentiD) Then
    txtsegm = Istruzione.SSA(Idx).NomeSegmentiD
  Else
    txtsegm = Istruzione.SSA(Idx).NomeSegmento
  End If
  If Len(Istruzione.SSA(Idx).LenSegmentiD) Then
    txtSegLen = Istruzione.SSA(Idx).LenSegmentiD
  Else
    txtSegLen = Istruzione.SSA(Idx).LenSegmento
  End If
  txtStrutt = Istruzione.SSA(Idx).Struttura
  txtSSAName = Istruzione.SSA(Idx).ssaName
  
  ChkQual.Value = IIf(Istruzione.SSA(Idx).Qualificatore, 1, 0)
  ChkBoth.Value = IIf(Istruzione.SSA(Idx).BothQualAndUnqual, 1, 0)
  
  If UBound(Istruzione.SSA(Idx).Chiavi) = 1 Or (UBound(Istruzione.SSA(Idx).Chiavi) > 1 And AUSelIndexKey = 0) Then
    'carica l'unica chiave che esiste
    AUSelIndexKey = 1
  End If
  'SQ - non va bene...
  If AUSelIndexKey > 0 And UBound(Istruzione.SSA(Idx).Chiavi) Then
    txtKeyName = Istruzione.SSA(Idx).Chiavi(AUSelIndexKey).key
    loadSelectedKey
  End If
  
  txtComCode.text = Istruzione.SSA(Idx).CommandCode

  fraSSA.Caption = "Level " & Format(Liv, "00")

  ImgSegmenti.Picture = Nothing
  
  ' Mauro 07/05/2008 : Ma cos'� sta roba?!?! Richiama una routine vuota
  '''SetFormByIstruzione (Idx)
End Sub

'''Public Sub SetFormByIstruzione(index As Long)
'''End Sub

Private Sub Cancella_TextBox_Istruzione()
  'fraSSA.Caption = "Level [Not Selected]"
  txtSegLen = ""
  txtsegm = ""
  txtStrutt = ""
  txtKeyName.text = ""
  txtKeyLen = ""
  txtValue = ""
  txtComCode = ""
  txtOp = ""
  txtSSAName = ""
  txtKeyMask = ""
  txtKeyStart = ""
  'ChkQual.Value = 0
  'ChkBoth.Value = 0
  txtOpLogico = ""
  
  'lblIstrLev(0).Caption = "Current Level instruction <not selected>"
  'lblIstrLev(1).Caption = "Current Level instruction <not selected>"
  'lblKeyLev(0).Caption = "Current key level <not selected>"
End Sub

Private Sub txtComCode_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "CON" & Format(AUSelIndexKey, "00"), txtComCode.text
  Associa_Dato_In_Memoria AULivello, "COD", txtComCode.text
End Sub

Private Sub txtKeyLen_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "KYL" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtKeyLen.text
  Associa_Dato_In_Memoria AULivello, "KYL", txtKeyLen.text
End Sub

Private Sub txtKeyMask_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "VAL" & Format(AULivello, "00") & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtKeyMask.text
  Associa_Dato_In_Memoria AULivello, "VAL", txtKeyMask.text
End Sub

Private Sub txtKeyName_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "KEY" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtKeyName.text
  Associa_Dato_In_Memoria AULivello, "KEY", txtKeyName.text
End Sub

Private Sub txtKeyStart_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "KYS" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtKeyStart.text
  Associa_Dato_In_Memoria AULivello, "KYS", txtKeyStart.text
End Sub

Private Sub txtOp_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "OPE" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtOp.text
  Associa_Dato_In_Memoria AULivello, "OPE", txtOp.text
End Sub

Private Sub txtOpLogico_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "OPL" & Format(AULivello, "00"), txtOpLogico.text
  Associa_Dato_In_Memoria AULivello, "OPL", txtOpLogico.text
End Sub

Private Sub txtSegLen_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "SGL" & Format(AULivello, "00"), txtSegLen.text
  Associa_Dato_In_Memoria AULivello, "SGL", txtSegLen.text
End Sub

Private Sub txtsegm_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "SEG" & Format(AULivello, "00"), txtsegm.text
   
  txtsegm.tag = GestDuplicati("SEG", txtsegm.text)
  
  Associa_Dato_In_Memoria AULivello, "SEG", txtsegm.text
End Sub

Private Sub txtSSAName_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "SSA" & Format(AULivello, "00"), txtSSAName.text
  Associa_Dato_In_Memoria AULivello, "SSA", txtSSAName.text
End Sub

Private Sub txtStrutt_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "STR" & Format(AULivello, "00"), txtStrutt.text
  Associa_Dato_In_Memoria AULivello, "STR", txtStrutt.text
End Sub

Private Sub txtValue_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "KYV" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtValue.text
  Associa_Dato_In_Memoria AULivello, "KYV", txtValue.text
End Sub

Public Function GestDuplicati(wTipo As String, wName As String) As Long
  Dim rs As Recordset
  Dim sWhere As String
  Dim wNotFound As Boolean
  Dim Segm() As String
  Dim i As Long
   
  If gIdDbd <> -1 Then
    sWhere = " AND IdOggetto = " & gIdDbd
  End If
   
  Select Case Trim(wTipo)
    Case "SEG" 'Segmento
      ' Mauro 05/12/2007 : Possono esserci pi� segmenti separati da ";"
      txtSegLen.text = ""
      Segm = Split(wName & ";", ";")
      For i = 0 To UBound(Segm) - 1
      
        'Set rs = m_fun.Open_Recordset("Select * From PSDLI_Segmenti Where Nome = '" & wName & "'" & sWhere)
        Set rs = m_fun.Open_Recordset("Select * From PSDLI_Segmenti Where Nome = '" & Segm(i) & "'" & sWhere)
        
        Select Case rs.RecordCount
          Case 0
            ImgSegmenti.Picture = ImageList1.ListImages.item(10).Picture
            ImgSegmenti.ToolTipText = "This segment does not exists..."
            GestDuplicati = -1
            wNotFound = True
             
          Case 1
            ImgSegmenti.Picture = ImageList1.ListImages.item(14).Picture
            GestDuplicati = rs!idSegmento
            ImgSegmenti.ToolTipText = "This segment is unique under this database..."
            txtSegLen.text = IIf(Len(txtSegLen.text), txtSegLen.text & ";", "") & rs!MaxLen
            
            Associa_Dato_In_TreeView TRWIstruzione, AULivello, "SGL" & Format(AULivello, "00"), txtSegLen.text
            Associa_Dato_In_Memoria AULivello, "SGL", txtSegLen.text
             
          Case Else '>1
            ImgSegmenti.Picture = ImageList1.ListImages.item(17).Picture
            GestDuplicati = -1
            ImgSegmenti.ToolTipText = "WARNING! This segment name is duplicated into some databases..."
        
        End Select
        
        rs.Close
      Next i
  End Select
   
  If wNotFound Then
    wNotFound = False
    Select Case Trim(wTipo)
      Case "SEG" 'Segmento
        Set rs = m_fun.Open_Recordset("Select * From PSDLI_Segmenti Where Nome = '" & wName & "'")
        Select Case rs.RecordCount
          Case 0
            ImgSegmenti.Picture = ImageList1.ListImages.item(10).Picture
            GestDuplicati = -1
            wNotFound = True
             
          Case 1
            ImgSegmenti.Picture = ImageList1.ListImages.item(14).Picture
            GestDuplicati = rs!idSegmento
             
          Case Else '>1
            ImgSegmenti.Picture = ImageList1.ListImages.item(17).Picture
            GestDuplicati = -1
            
      End Select
      rs.Close
    End Select
  End If
End Function

Private Function IsValidSplit(Source As String) As Boolean
  Dim s() As String
  Dim i As Integer
  
  If InStr(1, Source, ",") Or InStr(1, Source, ".") Then
    IsValidSplit = False
    Exit Function
  End If
  IsValidSplit = True
  s = Split(Source, ";")
  For i = 0 To UBound(s)
    If Not IsNumeric(s(i)) Then
      IsValidSplit = False
      Exit For
    End If
  Next
End Function

Private Function IsValidPCB(Source As String) As Boolean

  If InStr(1, Source, ",") Or InStr(1, Source, ".") Then
    IsValidPCB = False
    Exit Function
  Else
    If IsNumeric(Source) Then
      IsValidPCB = True
    Else
      IsValidPCB = False
    End If
  End If
End Function

Private Sub cmdConferma_Click()
  Dim mainIstruzione As String
  Dim ColRigaOggetto As New Collection
   
  On Error GoTo errorHandler
  Screen.MousePointer = vbHourglass
  'If Not Trim(TxtPCB.Text) = "" And Not IsValidSplit(Trim(TxtPCB.Text)) Then
  If Not Trim(TxtPCB.text) = "" And Not IsValidPCB(Trim(TxtPCB.text)) Then
    MsgBox "Invalid PCB Number(s) or PCB numbers not separated with semicolons", vbCritical, "PCB Number"
    TxtPCB.SetFocus
  Else
    'Conferma_Convalida_Istruzione Dli2Rdbms.drIdOggetto, Istruzione.Riga, CBool(chkIstrFLAG.Value)
    Conferma_Convalida_Istruzione Dli2Rdbms.drIdOggetto, Istruzione.Riga, CBool(chkIstrFLAG.Value), CBool(CheckObsolete.Value), CBool(CheckToMigrate.Value), mainIstruzione
    If Not (UCase(GbPrevIstr) = UCase(mainIstruzione)) Then
      ' Mauro 07/05/2008
      'ColRigaOggetto.Add (lswRighe.SelectedItem.SubItems(1))
      'ColRigaOggetto.Add (lswRighe.SelectedItem)
      'ColRigaOggetto.Add (Right(lswRighe.SelectedItem.ToolTipText, Len(lswRighe.SelectedItem.ToolTipText) - 7))
      ColRigaOggetto.Add lswRighe.SelectedItem.SubItems(L_LINE)
      ColRigaOggetto.Add lswRighe.SelectedItem.SubItems(L_IDOGG)
      ColRigaOggetto.Add lswRighe.SelectedItem
      ColRigaOggetto.Add ""
      ColRigaOggetto.Add "NOPARSCLONE"
      DLLExtParser.AttivaFunzione "PARSER2", ColRigaOggetto
      ' Mauro 07/05/208
      'lswRighe.SelectedItem.SubItems(2) = firstElement(Trim(TxtInstr.text))
      lswRighe.SelectedItem.SubItems(L_INSTR) = firstElement(Trim(TxtInstr.text))
    ElseIf Not (GbPrevPCB = Trim(TxtPCB.text)) And Not Trim(TxtPCB.text) = "" Then
      If Not Trim(TxtPCB.text) = 0 Then
        Riparser_after_PCBChange Trim(TxtPCB.text), Istruzione.pcbName
        ' Mauro 07/05/2008
        'CaricaWarnings lswRighe.SelectedItem, lswRighe.SelectedItem.SubItems(1), Dli2Rdbms.drIdOggetto
        CaricaWarnings lswRighe.SelectedItem.SubItems(L_IDOGG), lswRighe.SelectedItem.SubItems(L_LINE), lswRighe.SelectedItem
      Else ' AC pcb = 0 non accettato, ripristiniamo il vecchio valore senza riparserare
        MsgBox "0 is not a correct value for PCB number: please insert a value greater then 0", vbCritical, "PCB error"
        TxtPCB.text = GbPrevPCB
      End If
    End If
    RefreshRiga
  End If
  Screen.MousePointer = vbDefault
  Exit Sub
errorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002"
End Sub
