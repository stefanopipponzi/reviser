Attribute VB_Name = "MadrdM_Incapsulatore"
Option Explicit

'SQ - 17-07-06
'Ex modulo _Incaps
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Global nomeRoutine As String
Global GbNumIstr As String
Global percorsoPSB As String
Global m_ImsDll As New MadrdC_Menu
Global PLIsaveBackKey As String
Global testoback As String
Global routinePLI() As String
Global TipoFile As String
'VARIABILI PER DEFINIRE L'ESITO DELLA VERIFICA DEI PSB E DELLE ISTRUZIONI
'IN BASE A QUESTE VERRANNO CARICATE LE IMMAGINI NELLA TREEVIEW
Global PSBFlag As Boolean
Global DBDFlag  As Boolean
Global IstrFlag As Boolean
Global SegmFlag As Boolean
'FLAG PER NON FARE PARTIRE LA VERIFICA OGNI VOLTA CHE LA FINESTRA DIVENTA ATTIVA.........
Global IncaSwitch As Boolean
'variabili per la verifica
Global switch As Boolean
Global InfoFlag As Long
'nome da passare all'editor
Global NomeEDitor As String
'variabili per controllo sul numero degli oggetti
Global OGGLista As Long
Global OGG_V_ERR As Long
'serve per contare gli oggetti che non hanno errori di tipo 'V'
Global OGG_Count As Long
'Global fromMenu As String
Global dirincaps As String

'Ac for decodec
Global codrif As String
Global checkcod As Boolean
Global firstIF As String
Global endstatement As String
Global TestoFirst As String
Global firstindex As Boolean
Global counttomigrate As Integer
'************************************************
'*** indica i PSB che non ci sono fisicamente ***
'************************************************
Public Type ID_PSB_M
  Id As Variant
  Num As Variant
End Type

Public Type PSB_M
  Num As Long
  nome As String
  id_PSB() As ID_PSB_M
  Num_ID As Variant
End Type
'************************************************

'************************************************
'*** indica i PSB non ASSOCIATI *****************
'************************************************
Type PSB_N_A
  nome As String
  Id As Long
End Type

'************************************************
'*** indica le dipendenze dei PSB ***************
'************************************************
Public Type PSB_DIP
  Id As Variant
  nome As String
  TipoChiamata As String
End Type

Global PSB_MANCANTI() As PSB_M
Global PSB_NON_ASS() As PSB_N_A

'************************************************
'*** indica i DBD mancanti fisicamente **********
'************************************************
Type DBD_M
  NomeDbD As String
  IdOgg As Long
  NomeOgg As String
  TipoOgg As String
  Directory As String
End Type

Global DBD_MANCANTI() As DBD_M

'indica le dipendenze dei psb
Global DIPENDENZE_PSB() As PSB_DIP

'serve per tenere traccia delle righe aggiunte espandendo la cpy
Global RighePiu As Variant

'indica quale bottone per il controllo della molteplicit� � stato premuto
'es. segmenti, strutture, key ecc...
Global Selezione As String

'serve per tenere in memoria l'ultima textbox attiva e quale bottome cmdMostra � attivo
Global txtFlag As Integer
Global MostraFlag As String

'serve per determinare memorizzare il valore precedente
Public ValPrevious As String

'indica L ' IDOGGETTO dell'istruzione selezionata nella form IncapsAUTO e la riga di origine,
'la riga attuale e la chiave
Global ID_ISTRUZIONE As Variant
Global RigaOR_ISTRUZIONE As Long
Global CHIAVE_ISTRUZIONE As Variant
Global RigaC_ISTRUZIONE As Long
Global TESTO_ISTRUZIONE   As String

'indica L 'ssa cliccata sulla treeview, servir� per effettuare le modifiche
Global ssa As Long

'****************************************************
'costanti per puntare il nodo giusto  della treeview
Public Const Segm_Const = 4
Public Const Key_Const = 7
Public Const Strutt_Const = 6
Public Const Condiz_Const = 10
Public Const Val_Const = 9
Public Const Oper_Const = 8
Public Const Routine_Const = 11
Public Const Qualif_Const = 5
'togliere dopo un test effettivo di non utilizzo
'****************************************************
'
'*****************************************************************************
Type BlkIstrKey
  key As String
  xName As Boolean
  op As String
  KeyVal As String
  KeyLen As String
  KeyBool As String
  KeyNum As Integer
End Type

Type BlkIstrDli
  ssaName As String
  SsaLen As Integer
  segment As String
  Tavola As String
  idsegm As Double
  SegmVarName As String
  Record As String
  recordReaddress As String ' solo PLI
  SegLen As String
  CodCom As String
  Parentesi As String
  Search As String
  nomeRout As String
  valida As Boolean
  KeyDef() As BlkIstrKey
End Type

''Global Ta() As DefOperaz

''''Type IstrDli
''''  Dbd As String
''''  PCB As String
''''  Istr As String
''''  PSBName As String
''''  PsbId As Double
''''  BlkIstr() As BlkIstrDli
''''End Type

'****************************************************
'**** indica per ogni istruzione i suoi dettagli ****
'****************************************************
Type RIGHE_CALL
  Sintassi As String
  Id As Variant
  Riga As Variant
  RigaOrigine As Variant
  Istruzione As Variant
  IdStrutture As Variant
  IdPSB As Variant
  idsegm As Variant
  LungSegmento As Variant
  LungKey As Variant
End Type

'****************************************************
'**** indica per ogni istruzione i suoi livelli ***** da fare:
'****************************************************
Type ISTR_LIVEL
  Id As Variant
  Riga As Variant
  livello As Variant
  IdStruttura As Variant
  idsegm As Variant
  idField As Variant
  operator As Variant
  Valore As Variant
  condizione As Variant
End Type

Type xCmpPcb
  NomePCB As String
  Campo(9) As String
End Type

Global CmpPcb() As xCmpPcb

Global LivelloIstr() As ISTR_LIVEL
Global RigheAnalisi() As RIGHE_CALL

''''''''Global TabIstr As IstrDli
Global Percorso As String
Global rs As Recordset, rs_NDb As Recordset

'flag che serve per azzerare la variabile statica indice
Global BFlag As Boolean
Type wOpeIstrRt
  Istruzione As String
  Decodifica As String
  ordPcb As Long
  pcbDyn As Integer
End Type
Global OpeIstrRt() As wOpeIstrRt

'indica la directory di origine del file da incapsulare e la sua directory
Global dirOr As String
Global DirCBLcpy As String

'serve per tenere il conto delle righe da aggiungere alle istruzioni dopo aver espanso le CPY nel programma stesso
Type wCPY
  IdObj As Long
  RigaCpy As Long
  NumRighe As Long
  NomeCpy As String
End Type
Global Exp_CPY() As wCPY

'contiene le istruzioni non codificate di un'eventuale copy contenente il DLI
Type IstrCpy
  IdOggetto As Long
  Riga As Long
  Statment As String
  RigaCpyBase As Long
  RigaCpyExp As Long
  Istruzione As String
End Type
Global IstrCPYDli() As IstrCpy
Type KeyFDVsam
  nome As String
  tipo As String
  Start As Integer
  Len As Integer
End Type
Global IDPGMIncaps As Long
Global DBDNameIncaps  As String
Global NomeRoutineIncaps As String
Global KeyLenGuida As Integer

'mantiene il numero del livello di chiamata nelle routine DB2
Global LNKDBNumstack() As String
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public NomeProg As String
Public OldName As String
Public OldName1 As String

'Variabili Per incapsulatore
Public AddRighe As Long
Public AddrighebefIstr As Long
Public wIdent As Integer
Public wPunto As Boolean
Public j As Long
Public posizione_DB As Long

Public SwLink As Boolean

Public Type BlkIstrKeyLocal
  key As String
  xName As Boolean
  NameAlt As String
  op As String
  KeyVal As String
  KeyLen As String
  KeyBool As String
  KeyNum As Integer
  KeyVarField As String
  KeyStart As Long
  OpLogic As String
End Type

Public Type BlkIstrDliLocal
  ssaName As String
  SsaLen As Integer
  segment As String
  SegmentM As String
  nomeVarSeg As String ' serve per la molteplicit� delle EXEC
  Tavola As String
  idsegm As Long
  Record As String
  recordReaddress As String 'PLI
  SegLen As Long
  CodCom As String
  Parentesi As String
  Search As String
  nomeRout As String
  valida As Boolean
  KeyDef() As BlkIstrKeyLocal
'GSAM: lunghezza dell'area diversa da quella del segmento
  areaLen As Long
End Type
Public Type IstrDliLocal
  SenSeg As String 'AC 24/10/2008
  DBD() As t_Ogg
  pcb As String
  Pcbred As String
  numpcb As Long
  istr As String
  istrOriginalMolt As String
  psb() As t_Ogg
  BlkIstr() As BlkIstrDliLocal
  keyfeedback As String
  SavedAreas() As String
'modifiche AC
'XRST CHKP
  SavedAreasLen() As String
  IdArea As String
  IdAreaLen As String
'Auth
  authClassname As String
  authResource As String
'Inquy
  inqyAibsfunc As String
  inqyAibrsnm1 As String
  procSeq As String
  procOpt As String
  isGsam As Boolean
End Type

'AC  09/09/08 __________________
'struttura per riconoscere molteplicit�
Public Type BlkIstrKeyLocalBol
  MoltKey As Boolean
  MoltOpLogic As Boolean
  MoltKevVal As Boolean
End Type
Public Type BlkIstrDliLocalBol
  MoltSegment As Boolean
  MoltCodCom As Boolean
  MoltKeyDef() As BlkIstrKeyLocalBol
End Type
Public Type IstrDliLocalBol
  istr As String
  dbdname As String
  BlkIstr() As BlkIstrDliLocalBol
  keyfeedback As String
  routname As String
End Type


'_________________________________

Global TabIstr As IstrDliLocal

'AC 14/07/08  gestione incapsulamento da decodifiche
Global TabDec() As DefOperaz
Global MoltTabDec() As IstrDliLocalBol

'var per molteplicit�
Global bolMoltistr As Boolean
Global bolMoltseg() As Boolean 'array livello
Global bolMoltCC() As Boolean 'array livello
Global bolMoltKey() As Boolean 'matrice livello,sottolivello
Global bolMoltOpLogic As Boolean 'matrice livello,sottolivello

'________________________________

'Global TabIstr As Istruzione
Global Testo As String
Global QualAndUnqual As Boolean
Global OnlyQual As Boolean, OnlySqual As Boolean
Global wMoltIstr(4) As Collection
Global wNomeRout() As String

'SP 4/9
Global origPCB()

Public Type tbSegm
  IdFiglio As Long
  NomeFiglio As String
  NomeParent As String
  IdParent As Long
  idDBD As Long
  areaLen As Long
  KeyLen As Long
  keyName As String
  ssaName As String
  op As String
End Type


'*********************  AC per gestione "ON FLY" dell'incapsulamento **********
                 
Type InfoVerifyAndInsert
   RowNumber As Long  ' insert solo se in questa posizione vengono trovati i token
   target As String   '  stringa da ricercare
   lines_up As String     ' linee da inserire SOPRA se verifica ok
   lines_down As String     ' linee da inserire SOTTO se verifica ok
   infoModify  As String ' valorizzata con una stringa se linea originale (o gruppo di linee)deve essere modificata
   grouping_policy As String ' decide come raggruppare le righe:
                             ' a) TARGET raggruppa le righe finche non trova tutto il target specificato
                             ' b) TERMINATOR raggruppa le righe finche non trova la stringa target e
                             '    successivamente il terminatore specificato dal campo terminator
                             ' c) ONEROW considera solo ultima riga che completa il target ricercato
                             ' d) NUMBER considera le righe specificate da numlines
   terminator As String      ' da considerare solo se grouping_policy = "TERMINATOR"
   numlines As Integer       ' numero di linee da raggruppare (la prima � quella che contiene il TARGET)
   actionOK As String   ' azione/messaggio nel caso di stringa trovata
   actionFail As String ' azione/messaggio nel caso di string non trovata
   CommentType As String     ' "CBL","PLI"  indica il tipo di commento che invalida la riga
   isCaseSensitive As Boolean  ' se la ricerca della stringa � case sensitive
 End Type

Type InfoFindAndInsert
   target As String   '  stringa da ricercare
   lines_up As String     ' linee da inserire SOPRA se verifica ok
   lines_down As String     ' linee da inserire SOTTO se verifica ok
   infoModify  As String ' valorizzata con una stringa se linea originale (o gruppo di linee)deve essere modificata
   grouping_policy As String ' decide come raggruppare le righe:
                             ' a) TARGET raggruppa le righe finche non trova tutto il target specificato
                             ' b) TERMINATOR raggruppa le righe finche non trova la stringa target e
                             '    successivamente il terminatore specificato dal campo terminator
                             ' c) ONEROW considera solo ultima riga che completa il target ricercato
                             ' d) NUMBER considera le righe specificate da numlines
   terminator As String      ' da considerare solo se grouping_policy = "TERMINATOR"
   numlines As Integer       ' numero di linee da raggruppare (la prima � quella che contiene il TARGET)
   isUnique As Boolean       'mi dice se una volta trovata la stringa non � pi� necessario fare la ricerca
                             ' sulle altre linee e quindi la riga va eliminata da FItable
   CommentType As String     ' "CBL","PLI"  indica il tipo di commento che invalida la riga
   isSubSection As Boolean   ' valorizzata a true se il target non va cercato su tutto il doc, ma solo
                             ' su una sottosezione delimitata da  startSection ed endSection
   startSection As String
   sectionTerminator As String 'se presente la sezione � validata solo se trovo la startSection
                               ' e poi il sectionTerminator (es definizione di PROC)
   endSection As String
   isCaseSensitive As Boolean  ' se la ricerca della stringa � case sensitive
                           
End Type
Global isPLIcommentOpened As Boolean
Global cntPliMacro As Integer
Global VItable() As InfoVerifyAndInsert
Global FItable() As InfoFindAndInsert


'********************* "ON FLY"  end section **********
'stefano
Global mSegmentoPadre As tbSegm
  'molteplicit�
Global multiOp As Boolean
  'molteplicit� generale sull'istruzione
Global multiOpGen As Boolean
Global distinctCod As Collection, distinctSeg As Collection, distinctDB As Collection
Public MatriceCombinazioni() As Integer
Public CombinazioniOpLogici() As Integer
Public CombinazioniComCode() As Integer
Public Combinazionisegmenti() As Integer
Public Dcombination As Boolean
'AC 19/06/07
Public MatriceCombinazioniBOL() As Integer
'SQ 26-07-06 - Sostituire completamente la gestione del Load_Labels nella Funzioni
Global START_TAG As String, END_TAG As String
Global IsAlwaysBatch As Boolean
Dim wPath As String
Public EmbeddedRoutine As Boolean
Public indentkey As Integer
Public indentkeyoriginal As Integer
Global segLev As Integer
Global isPLI As Boolean
Global isPliCallinsideSelect As Boolean
Global opencomment As Boolean
Global opencommentstr As String
Global gbIstr As String
Global isOldIncapsMethod As Boolean
Global TestoREV As String
' Mauro 18/11/2009 : Opzione Convert GH in G
Public isUnificaGH As Boolean

'''*****************************************
''' nel caso di un programma verifica se istruzione � da migrare
''' nel caso di copy con pi� programmi chiamanti verifica che per almeno uno sia da migrare
'''nel caso di copy valorizza array parallelo con informazione su valida e obsoleta

Function toMigrate(IdOggetto As Long, Riga As Long, rstmigrate As Boolean, PGM() As Long, PgmInfo() As Boolean) As Boolean
Dim rs As Recordset, i As Integer
  ReDim PgmInfo(UBound(PGM))
  If UBound(PGM) > 0 Then
    For i = 1 To UBound(PGM)
      Set rs = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE idoggetto=" & IdOggetto & " AND idPgm = " & PGM(i) & " AND riga = " & Riga)
      If Not rs.EOF Then
        If rs!to_migrate Then
          toMigrate = True
        End If
        If rs!valida And Not rs!obsolete Then
          PgmInfo(i) = True
        Else
          PgmInfo(i) = False
        End If
      End If
      rs.Close
    Next i
  Else
    toMigrate = rstmigrate
  End If
End Function

Function PliWordCommented(startpos As Long, endpos As Long, Pliwordpos As Long, RT As RichTextBox)
  Dim iniziocommento As Long, finecommento As Long, oldiniziocommento As Long
  
  While iniziocommento < Pliwordpos And Not iniziocommento = -1
    oldiniziocommento = iniziocommento
    iniziocommento = RT.find("/*", startpos, endpos)
    startpos = iniziocommento + 2
  Wend
  If Not (iniziocommento = -1 And oldiniziocommento = 0) Then
    finecommento = RT.find("*/", oldiniziocommento, endpos)
  End If
  If Pliwordpos > oldiniziocommento And Pliwordpos < finecommento Then
    PliWordCommented = True
  End If
End Function

Function PliWordIntoSelect(startpos As Long, endpos As Long, Pliwordpos As Long, RT As RichTextBox)
  Dim inizioselect As Long, fineselect As Long, oldinizioselect As Long
  Dim findend As Boolean, startendpos As Long
  
  While inizioselect < Pliwordpos And Not inizioselect = -1
    oldinizioselect = inizioselect
    inizioselect = RT.find(" SELECT(", startpos, Pliwordpos)
    startpos = inizioselect + 2
  Wend
  startendpos = oldinizioselect
  While Not (inizioselect = -1 And oldinizioselect = 0) And (Not findend)
    fineselect = RT.find(" END;", startendpos, endpos)
    If RT.find("/*   BPHX  END", fineselect, fineselect + 100) = -1 Then
      findend = True
    Else
      startendpos = fineselect + 2
    End If
  Wend
  If Pliwordpos > oldinizioselect And Pliwordpos < fineselect Then
    PliWordIntoSelect = True
  End If
End Function
  
Public Function FormatTag(Line As String, Optional pendtag As String = "") As String
  Dim lenline As Integer
  
  lenline = Len(Line)
  If pendtag = "" Then
    Line = START_TAG & " " & Line & Space(65 - lenline) & END_TAG & vbCrLf
  Else
    Line = START_TAG & " " & Line & Space(65 - lenline) & pendtag & vbCrLf
  End If
  FormatTag = Line
End Function

Private Function findReservedWord(beginp As Long, endp As Long, RT As RichTextBox, rw As String) As Boolean
  If InStr(Mid(RT.text, beginp + 1, endp - beginp), " THEN ") Then
    rw = "THEN"
    findReservedWord = True
  ElseIf InStr(Mid(RT.text, beginp + 1, endp - beginp), " ELSE ") Then
    rw = "ELSE"
    findReservedWord = True
  Else
    findReservedWord = False
  End If
End Function

Public Sub insertPLISaveBack(PLIsaveBackKey As String, startpos As Long, endposrighe As Long, RT As RichTextBox)
  Dim wResp As Boolean, endpos As Long, find As Boolean, alwaysinsert As Boolean, validpoint As Boolean
  Dim w As Long, t As Long, h As Long, s() As String, reservedWord As String, testothen As String
  Dim iniziocommento As Long, finecommento As Long, oldiniziocommento As Long
  
  alwaysinsert = True
  If Not endposrighe = 0 Then
    wResp = TROVA_RIGA(RT, "CALL", endposrighe, RT.SelStart)
    endpos = RT.SelStart
  Else
    endpos = Len(RT.text)
  End If
  While Not w = -1
    validpoint = False
    While Not validpoint
      w = RT.find(PLIsaveBackKey, startpos, endpos)
      If Not w = -1 Then
        ' ciclo per catturare l'inizio commento pi� vicino alla parola chiave
        oldiniziocommento = iniziocommento
        While iniziocommento < w And Not iniziocommento = -1
          oldiniziocommento = iniziocommento
          iniziocommento = RT.find("/*", startpos, endpos)
          startpos = iniziocommento + 2
        Wend
        If Not iniziocommento = -1 Then
          finecommento = RT.find("*/", oldiniziocommento, endpos)
        End If
        If Not (w > oldiniziocommento And w < finecommento) Then
          For t = w To 1 Step -1
            If Mid(RT.text, t, 2) = vbCrLf Then
              validpoint = True
              Exit For
            End If
          Next t
        Else
          startpos = finecommento + 1
        End If
      Else
        validpoint = True ' per uscire dal ciclo validpoint con w= -1
      End If
    Wend
    If PLIsaveBackKey = "END " Then
      If InStr(Mid(RT.text, w + 3, 20), NomeProg) > 0 Then
        alwaysinsert = True
      Else
        alwaysinsert = False
        startpos = w + 2
      End If
    End If
    If Not w = -1 And alwaysinsert Then
      If findReservedWord(t, w, RT, reservedWord) Then
        testothen = "          " & reservedWord & vbCrLf & testoback
        h = RT.find(reservedWord, t, w)
        RT.SelStart = h
        RT.SelLength = Len(reservedWord)
        RT.SelText = ""
        RT.SelStart = t + 1
        RT.SelLength = 0
        RT.SelText = testothen
        s = Split(testothen, vbCrLf)
        startpos = w + 2 + Len(testothen)
      Else
        RT.SelStart = t + 1
        RT.SelLength = 0
        RT.SelText = testoback
        s = Split(testoback, vbCrLf)
        startpos = w + 2 + Len(testoback)
      End If
      AddRighe = AddRighe + UBound(s)
      find = True
      iniziocommento = 0
    End If
  Wend
  If Not find Then
    RT.SelStart = RT.SelStart - 2
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''
' GESTIONE CHIAMATE STATICHE:
' -> programma "bridge" BPHXPTC
''
' SQ 13-05-08
' (per ora solo Reale; se OK per tutti o flag
'''''''''''''''''''''''''''''''''''''''''
Public Sub insertExternalEntry(RTBMod As RichTextBox, pIdOggetto As Long, pRTBErr As RichTextBox)
  Dim testoext As String
  Dim i As Integer
  
  If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
    'If Len(routinePLI(i)) > 30 Then
    '  AggLog "[Object: " & pidOggetto & "] ROUTINE NAME: " & routinePLI(i), pRTBErr
    '  routinePLI(i) = Left(routinePLI(i), 30)
    'End If
    'testoext = testoext & " DCL " & routinePLI(i) & " ENTRY OPTIONS(COBOL);" & Space(31 - Len(routinePLI(i))) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
    testoext = " DCL SE0R001 ENTRY OPTIONS(COBOL);"
    testoext = testoext & Space(72 - (Len(testoext) + Len("/*" & Trim(START_TAG) & "*/"))) & "/*" & Trim(START_TAG) & "*/"
  ElseIf InStr(m_fun.FnNomeDB, "prj-GRZ") Then
    testoext = testoext & " /*" & String(62, "-") & "*" & Trim(START_TAG) & "*/" & vbCrLf & _
                          " /*" & Space(2) & "IO-ROUTINES DYNAMIC ENTRY" & Space(40) & "*/" & vbCrLf & _
                          " /*" & String(62, "-") & "*" & Trim(START_TAG) & "*/" & vbCrLf
    'For i = 1 To UBound(routinePLI)
    '  testoext = testoext & "/* DCL " & routinePLI(i) & " EXTERNAL ENTRY;" _
    '                      & Space(43 - Len(routinePLI(i))) & Trim(START_TAG) & "*/" & vbCrLf
    'Next i
    testoext = testoext & " DCL XRIORTN ENTRY OPTIONS(FETCHABLE);"
    testoext = testoext & Space(34 - (Len("/*" & Trim(START_TAG) & "*/"))) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
    testoext = testoext & " /*" & String(62, "-") & "*" & Trim(START_TAG) & "*/" & vbCrLf
  Else
    'SQ deve essere tutto esternalizzato!
    For i = 1 To UBound(routinePLI)
      testoext = testoext & " DCL " & routinePLI(i) & " EXTERNAL ENTRY;" _
                          & Space(43 - Len(routinePLI(i))) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
    Next i
    'mangio fine linea finale
    If Len(testoext) Then
      testoext = Left(testoext, Len(testoext) - 2)
    End If
  End If
  
  MadrdM_GenRout.changeTag RTBMod, "<externalentry>", testoext
End Sub

Sub upadateArrayNomeRoutinePLI(pIdOggetto As Long)
  Dim r As Recordset
  Dim isOK As Boolean, i As Integer, k As Integer
  
  Set r = m_fun.Open_Recordset("SELECT * FROM PsDli_Istruzioni WHERE IdPgm = " & pIdOggetto & " and not IdOggetto = " & pIdOggetto)
  While Not r.EOF
    isOK = CaricaNomiRoutineInCopy(pIdOggetto, r!IdOggetto, r!Riga, r!Istruzione)
    For i = 1 To UBound(TabIstr.BlkIstr)
      For k = 1 To UBound(routinePLI)
        If TabIstr.BlkIstr(i).nomeRout = routinePLI(k) Then Exit For
      Next k
      If k = UBound(routinePLI) + 1 Then
        ReDim Preserve routinePLI(UBound(routinePLI) + 1)
        routinePLI(UBound(routinePLI)) = TabIstr.BlkIstr(i).nomeRout
      End If
    Next i
    r.MoveNext
  Wend
  r.Close
End Sub

Public Sub Controlla_CBL_CPY(RT As RichTextBox, RTBErr As RichTextBox, rs As Recordset, typeIncaps As String)
  '2) Controlla se l'istruzione Corrente fa parte Del CBL o la CPY gi� aperta, se si non carica il CBL o CPY ma li aggiorna:
  Dim NCBLcpy As String
  Dim Tip As String
  Dim k As Integer
  Dim w As Long
  Dim ww As Long, wstr As String, pos4 As Integer
  Dim r As Recordset, rmain As Recordset
  Dim isVSAMbatch As Boolean
  Dim strcomment As String
  
  AddRighe = 0
  
  Set r = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & rs!IdOggetto)
  If r.RecordCount Then
    If Trim(r!estensione) = "" Then
      NCBLcpy = r!nome
    Else
      NCBLcpy = r!nome & "." & r!estensione
    End If
    NomeProg = r!nome
    
    DirCBLcpy = Crea_Directory_Progetto(r!Directory_Input, Dli2Rdbms.drPathDef)
    
    w = InStr(1, DirCBLcpy, "\")
    While w > 0
      ww = w
      w = InStr(w + 1, DirCBLcpy, "\")
    Wend
    
    'qui ottengo la directory di origine del file
    dirOr = Mid(DirCBLcpy, ww + 1)
    
    Tip = r!tipo
    RT.text = ""
    
    '**************  nuovo percorso destinazione  Incapsulator/Nome_directory_origine_programma
    Percorso = m_fun.FnPathDB & "\output-prj\incapsulator"
    If Dir(Percorso, vbDirectory) = "" Then MkDir Percorso
    Percorso = Percorso & "\" & dirOr
    If Dir(Percorso, vbDirectory) = "" Then MkDir Percorso
    '********************************
    On Error GoTo err
    
    isVSAMbatch = r!Batch And dirincaps = "\VSAM"
    
    If RT.text = "" Then
      If MadrdF_Incapsulatore.Optsource Then
        RT.LoadFile DirCBLcpy & "\" & NCBLcpy
      Else
        RT.LoadFile Percorso & "\" & NCBLcpy
      End If
      
      'AC 07/07/09
      'asteriscamento parametri della MAIN PROc
      ' controllo sul db
      Set rmain = m_fun.Open_Recordset("SELECT * FROM PsPgm WHERE Idoggetto = " & rs!IdOggetto)
      If Not rmain.EOF Then
        If rmain!Main Then
          If RT.find("MAIN", 1, 5400) > 0 Then
            Dim posmain As Long, t As Integer
            Dim posstart As Long, posend As Long
            posmain = RT.find("MAIN", 1, 5400)
            For t = posmain To 1 Step -1
              If Mid(RT.text, t, 2) = vbCrLf Then
                posstart = t
                posend = RT.find(vbCrLf, t + 4)
                RT.SelStart = posstart + 2
                RT.SelLength = posend - (posstart + 2)
                If Not rmain!parameters = "" Then
                  If Not Len(RT.SelText) > 76 Then
                    RT.SelText = Replace(RT.SelText, rmain!parameters, "/*" & rmain!parameters & "*/")
                  Else
                    RT.SelText = Replace(RT.SelText, rmain!parameters, "/*" & rmain!parameters & "*/")
                    RT.SelStart = posstart + 2
                    RT.SelLength = posend - (posstart + 2)
                    RT.SelText = Replace(RT.SelText, ";     ", ";")
                  End If
                End If
                Exit For
              End If
            Next t
            If t = 0 Then
              posstart = 1
              posend = RT.find(vbCrLf, t + 4)
              RT.SelStart = 1
              RT.SelLength = posend - 1
              If Not rmain!parameters = "" Then
                If Not Len(RT.SelText) > 76 Then
                  RT.SelText = Replace(RT.SelText, rmain!parameters, "/*" & rmain!parameters & "*/")
                Else
                  RT.SelText = Replace(RT.SelText, rmain!parameters, "/*" & rmain!parameters & "*/")
                  RT.SelStart = 1
                  RT.SelLength = posend - 1
                  RT.SelText = Replace(RT.SelText, ";     ", ";")
                End If
              End If
            End If
          End If
        End If
      End If
      rmain.Close
      
      'silvia 16-1-2008 : elimina dal file copia le direttive di compilazione DLI dalla XOPTS
      'If RT.find("XOPTS", 1, 72) > 0 Then
      ' Mauro: 72 = 1 riga; 5400 = 75 righe
      If RT.find("XOPTS", 1, 5400) > 0 Then
        Elimina_DLI_in_Xopts RT, r!tipo
      End If
      
      If r!tipo <> "CPY" And r!tipo <> "INC" Then
        If r!tipo = "EZT" Then
          Inizializza_Copy_IMSDB_EZT RT, RTBErr
        Else
          Inizializza_Copy_IMSDB RT, RTBErr, r!tipo, rs!IdOggetto, isVSAMbatch
          If isVSAMbatch And EmbeddedRoutine Then
            Inizializza_Data_Division RT, RTBErr, r!tipo, rs!IdOggetto, "INCAPS"
            Inizializza_File_Control RT, RTBErr, r!tipo, rs!IdOggetto, "INCAPS"
          End If
        End If
      ElseIf r!tipo = "INC" Then
        'AC 18/07/08 per le INC inserisco varibili AREA e KEYVALUE
        Inizializza_PLIINC_IMSDB RT, RTBErr, rs!IdOggetto
      End If
    Else
      If RT.FileName <> DirCBLcpy & "\" & NCBLcpy Then
        'Salva il file
        Crea_Directory_Cbl_Cpy (DirCBLcpy) 'crea le directory CBLOut e CPYOut se non esistono
        
        RT.SaveFile OldName, 1
        
        If MadrdF_Incapsulatore.Optsource Then
          RT.LoadFile DirCBLcpy & "\" & NCBLcpy
        Else
          RT.LoadFile Percorso & "\" & NCBLcpy
        End If
        'silvia 16-1-2008 : elimina dal file copia le direttive di compilazione DLI dalla XOPTS
        'If RT.find("XOPTS", 1, 72) > 0 Then
        ' Mauro: 72 = 1 riga; 5400 = 75 righe
        If RT.find("XOPTS", 1, 5400) > 0 Then
           Elimina_DLI_in_Xopts RT, r!tipo
        End If
        If r!tipo <> "CPY" Then
          If r!tipo = "EZT" Then
            Inizializza_Copy_IMSDB_EZT RT, RTBErr
          Else
            Inizializza_Copy_IMSDB RT, RTBErr, r!tipo, rs!IdOggetto
            'If r!Batch Then
            If isVSAMbatch And EmbeddedRoutine Then
              Inizializza_Data_Division RT, RTBErr, r!tipo, rs!IdOggetto, "INCAPS"
              Inizializza_File_Control RT, RTBErr, r!tipo, rs!IdOggetto, "INCAPS"
            End If
          End If
        End If
      End If
    End If
   
    'oldName = DirCBLcpy & "\Out\" & NCBLcpy
    OldName = Percorso & "\" & NCBLcpy
    OldName1 = NCBLcpy
  End If
  Exit Sub
err:
  MsgBox "Incapsulated IMS-DC version of file not found", vbCritical, "File not found"
End Sub

Sub Elimina_DLI_in_Xopts(RT As RichTextBox, tipo As String)
  Dim filerow As String, token As String, altrotoken As String
  Dim newrow As String, newrow2 As String
  Dim sellen As Long, postoken As Long
  Dim strcomment As String
  Dim strspace As String
  Dim SelStart As Long, i As Long, a As Long
  Dim arrDirett() As String, Header As String
  Dim arrParentesi() As String, traParentesi As String
  Dim isDotVir As Boolean, appo As String
  
  If tipo = "CBL" Or tipo = "CPY" Then
    strspace = "       "
    strcomment = "     /*---------------------------------------------------------------*/"
  End If
  If tipo = "INC" Or tipo = "PLI" Then
    strspace = ""
    strcomment = " /*----------------------------------------------------------------*/"
  End If
 
  sellen = RT.find(vbCrLf, 1)
  If sellen > 72 Then
    sellen = 72
  End If
  filerow = Left(RT.text, sellen)
  
  ' Mauro
  SelStart = 0
  Do Until (InStr(filerow, "XOPTS") > 0 Or InStr(filerow, "SYM") > 0 Or _
            InStr(filerow, "FDUMP") > 0 Or InStr(filerow, "OFFSET") > 0 Or _
            InStr(filerow, "LANGUAGE") > 0) _
            And Mid(filerow, 7, 1) <> "*"
    SelStart = sellen
    sellen = RT.find(vbCrLf, SelStart + 1)
    If sellen > SelStart + 72 Then
      sellen = SelStart + 72
    End If
    filerow = Mid(RT.text, SelStart + 3, sellen - SelStart)
  Loop
  ' elimino il vbcrlf
  filerow = Replace(filerow, vbCrLf, "")
  
  '''''''''''''''''''''''''''''''''''''''''''''''''
  ' Mauro 01/07/2009
  ' Non posso usare lo SPLIT!!!! Ci sono le virgole dentro le parentesi
  ''arrDirett() = Split(filerow, ",")
  appo = ""
  ReDim arrDirett(0)
  filerow = RTrim(filerow)
  For i = 1 To Len(filerow)
    appo = appo & Mid(filerow, i, 1)
    If Mid(filerow, i, 1) = "," Then
      arrDirett(UBound(arrDirett)) = RTrim(Left(appo, Len(appo) - 1))
      ReDim Preserve arrDirett(UBound(arrDirett) + 1)
      appo = ""
    ElseIf Mid(filerow, i, 1) = "(" Then
      Do Until Mid(filerow, i, 1) = ")"
        i = i + 1
        appo = appo & Mid(filerow, i, 1)
      Loop
    End If
  Next i
  arrDirett(UBound(arrDirett)) = appo
  
  For i = UBound(arrDirett) To 0 Step -1
    If i = 0 Then
      Header = RTrim(Left(arrDirett(i), InStrRev(arrDirett(i), " ")))
      arrDirett(i) = Trim(Mid(arrDirett(i), InStrRev(arrDirett(i), " ")))
    End If
    If i = UBound(arrDirett) Then
      isDotVir = False
      If Right(arrDirett(i), 1) = ";" Then
        arrDirett(i) = Left(arrDirett(i), Len(arrDirett(i)) - 1)
        isDotVir = True
      End If
    End If
    If InStr(arrDirett(i), "(") Then
      traParentesi = Mid(arrDirett(i), InStr(arrDirett(i), "("))
      traParentesi = Replace(Replace(traParentesi, "(", ""), ")", "")
      arrParentesi() = Split(traParentesi, ",")
      For a = UBound(arrParentesi) To 0 Step -1
        If Trim(arrParentesi(a)) = "FDUMP" Or _
           Trim(arrParentesi(a)) = "OFFSET" Or _
           Trim(arrParentesi(a)) = "SYM" Or _
           Trim(arrParentesi(a)) = "UE" Or _
           Trim(arrParentesi(a)) = "DLI" Then
          arrParentesi(a) = ""
        End If
      Next a
      ' RIATTACCO I PEZZI DELL'ARRAY CHE AVEVO SPLITTATO PRIMA
      traParentesi = ""
      For a = 0 To UBound(arrParentesi)
        If arrParentesi(a) <> "" Then
          traParentesi = traParentesi & Trim(arrParentesi(a)) & ","
        End If
      Next a
      If traParentesi = "" Then
        arrDirett(i) = ""
      Else
        arrDirett(i) = Left(arrDirett(i), InStr(arrDirett(i), "(")) & Left(traParentesi, Len(traParentesi) - 1) & ")"
      End If
    Else
      If Trim(arrDirett(i)) = "FDUMP" Or _
         Trim(arrDirett(i)) = "OFFSET" Or _
         Trim(arrDirett(i)) = "SYM" Then
        arrDirett(i) = ""
      End If
    End If
  Next i
  ' RIATTACCO I PEZZI DELL'ARRAY CHE AVEVO SPLITTATO PRIMA
  For i = 0 To UBound(arrDirett)
    If arrDirett(i) <> "" Then
      newrow = newrow & Trim(arrDirett(i)) & ","
    End If
  Next i
  If newrow = "" Then
    ' COMMENTO TUTTA LA RIGA
    newrow = strcomment
  Else
    ' Aggiungo l'intestazione delle direttive e tolgo l'ultima virgola
    newrow = Header & " " & Left(newrow, Len(newrow) - 1)
    If isDotVir Then
      newrow = newrow & ";"
    End If
  End If
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
  Dim chr As String
  sellen = 1
  chr = ""
  ' Avevo provato con la RT.find, ma per qualche oscuro motivo sbagliava
  Do Until chr = vbCrLf
    sellen = sellen + 1
    chr = Mid(RT.text, SelStart + sellen, 2)
  Loop
  RT.SelStart = IIf(SelStart = 0, 0, SelStart + 2)
  ' Devo togliere 1, senn� mi mangio il vbCrLf
  RT.SelLength = IIf(RT.SelStart > 72, sellen - 2, sellen - 1)
  If Len(newrow) < sellen - SelStart Then newrow = newrow & Space(sellen - SelStart - Len(newrow))
  newrow = RTrim(newrow)
  RT.SelText = newrow
End Sub

Function nextToken_new(inputString As String)
  Dim level, i, j As Integer
  Dim currentChar As String
  
  'Mangio i bianchi e virgole davanti (primo giro...)
  inputString = LTrim(inputString)
  If (Len(inputString) > 0) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "("
        i = 2
        level = 1
        nextToken_new = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "("
              level = level + 1
              nextToken_new = nextToken_new & currentChar
            Case ")"
              level = level - 1
              nextToken_new = nextToken_new & currentChar
            Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nextToken_new = nextToken_new & currentChar
              Else
                err.Raise 123, "nextToken_new", "syntax error on " & inputString
                Exit Function
              End If
          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
      Case "'"
        'Ritorno gli apici
        ''''inputString = Mid(inputString, 2)
        i = InStr(2, inputString, "'")
        If i Then
          'nextToken_newnew = Left(inputString, InStr(inputString, "'") - 1)
          nextToken_new = Left(inputString, i)
          inputString = Mid(inputString, i + 1)
        Else
          'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
          inputString = "''" & inputString
          
          err.Raise 123, "nextToken_new", "syntax error on " & inputString
          Exit Function
        End If
      Case Else
'''      i = InStr(inputString, " ")
'''      j = InStr(inputString, "(")
'''      If ((i > 0 And j > 0 And j < i) Or (i = 0 And j > 0)) Then i = j
'''      If (i > 0) Then
'''        nextToken_newnew = Left(inputString, i - 1)
'''        inputString = Trim(Mid(inputString, i))
'''      Else
'''        nextToken_newnew = inputString
'''        inputString = ""
'''      End If
'Ricerca primo separatore: " " o ","
      For i = 1 To Len(inputString)
        currentChar = Mid(inputString, i, 1)
        Select Case currentChar
          Case " ", "'"
            nextToken_new = Left(inputString, i - 1)
            inputString = Trim(Mid(inputString, i))
            Exit Function
          Case "("
            nextToken_new = Left(inputString, i - 1)
            inputString = Trim(Mid(inputString, i))
            'pezza: se ho pippo(...),... rimane una virgola iniziale:
            'If Left(inputString, 1) = "," Then inputString = Trim(Mid(inputString, 2))
            Exit Function
          Case ","
            nextToken_new = Left(inputString, i - 1)
            inputString = Trim(Mid(inputString, i + 1))
            Exit Function
          Case Else
            'avanza pure
        End Select
      Next i
      'nessun separatore:
      nextToken_new = inputString
      inputString = ""
    End Select
  End If
  'inizia con un terminatore?!
  If Left(inputString, 1) = "," Then
    inputString = Mid(inputString, 2)
  End If
End Function

Sub Crea_Directory_Cbl_Cpy(DirCBLcpy As String)
  '1) Controlla se esistono le 2 directory di output Routine, se no le crea:
  If Dir(DirCBLcpy & "\Out", vbDirectory) = "" Then
    'crea directory
    MkDir DirCBLcpy & "\Out"
  End If
End Sub

Private Function getLevKey(IdOggetto As Long, ssaName As String) As String()
  'accedere alla psdata_cmp con idoggetto e ssaname
  'ricavare i valori che vanno da dopo l'operatore di confronto
  'fino al campo che ha ')'.
  'nel caso di '*' ricavare i valori nuovamente
  'L'array parte da 1 come da STANDARD
  Dim rs As Recordset
  Dim strQRY As String
  Dim boolOperatore As Boolean
  Dim levKey() As String
  Dim indice As Integer
  
  strQRY = " SELECT * from psdata_cmp " & _
           " Where idOggetto =" & IdOggetto & " and idarea in (" & _
           " select idArea from psdata_cmp " & _
           " where idoggetto=" & IdOggetto & " and nome='" & ssaName & "')" & _
           " and nome<>'" & ssaName & "' order by ordinale "
  
  Set rs = m_fun.Open_Recordset(strQRY)
  
  ReDim Preserve levKey(0)
  
  If Not rs.EOF Then
    boolOperatore = False
    indice = 1
    While Not rs.EOF
      If InStr(1, rs!Valore, "(") = 0 And InStr(1, rs!Valore, ")") = 0 And InStr(1, rs!Valore, "*") = 0 Then
        Select Case Right(Trim(rs!Valore), 2)
          Case "EQ", "GE", "GT", "LE", "LT", "NE"
            boolOperatore = True
          Case Else
            If boolOperatore Then
              ReDim Preserve levKey(indice)
              If levKey(indice) = "" Then
                levKey(indice) = rs!nome
              Else
                levKey(indice) = levKey(indice) & " AND " & rs!nome
              End If
            End If
        End Select
      Else
        'buttare solo le parantesi.... attenzione al *
        If InStr(1, rs!Valore, "*") > 0 Then
          indice = indice + 1
        End If
      End If
      rs.MoveNext
    Wend
  End If
  rs.Close
  getLevKey = levKey
End Function
Public Function IsSegObsolete(Id As Long) As Boolean
Dim rs As Recordset
    IsSegObsolete = False
    Set rs = m_fun.Open_Recordset(" Select  Correzione from MgDLI_Segmenti where IdSegmento=" & Id)
    If Not rs.EOF Then
        If rs!correzione = "O" Then
            IsSegObsolete = True
        End If
    End If
End Function

Public Sub getSegLevel(Id As Long, Optional nome As String = "")
'ricava il livello del segmento passato in input dalla
'asteriscaIstruzione_EZT
  Dim rs As Recordset
  Dim strQRY As String
  
  If nome = "" Then
    strQRY = " Select * from PSDLI_Segmenti where IdSegmento=" & Id
  Else
    strQRY = " Select * from PSDLI_Segmenti where IdOggetto=" & Id & _
             " and Nome='" & nome & "'"
  End If
  
  Set rs = m_fun.Open_Recordset(strQRY)

  If Not rs.EOF Then
    segLev = segLev + 1
    If rs!parent <> "0" Then
      getSegLevel rs!IdOggetto, rs!parent: Exit Sub
    End If
  End If
  rs.Close
End Sub

Private Function CheckDFHEIBLK(pIdOggetto As Long) As Boolean
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("select nome from PsData_Cmp where IdOggetto = " & pIdOggetto & " and nome = 'DFHEIBLK'")
  If Not rs.EOF Then
    CheckDFHEIBLK = True
  End If
  rs.Close
End Function

Public Sub Inizializza_PLIINC_IMSDB(RT As RichTextBox, RTBErr As RichTextBox, pIdOggetto As Long)
  Dim t As Long, w As Long
  Dim Testo As String
  Dim s() As String
  Dim nfile As Long
  Dim lFile As Long
  Dim sLine As String
  Dim templateName As String
  Dim keyword As String
  Dim boladdtesto As Boolean
  Dim pstart As Long
  
  boladdtesto = True
  ReDim CmpPcb(0)
  keyword = " DCL "
  w = RT.find(keyword, 1)
  
  If w = 1 Then
    AggLog "[Object: " & pIdOggetto & "] DCL label not find " & vbCrLf, MadrdF_Incapsulatore.RTErr
    Exit Sub
  End If
  
  While PliWordCommented(1, Len(RT.text), w, RT)
    w = RT.find(keyword, w + 1)
  Wend
  
  If w = -1 Then
    w = RT.find(" DCL ", 1)
    While PliWordCommented(1, Len(RT.text), w, RT)
      w = RT.find(keyword, w + 1)
    Wend
    w = RT.find(";", w + 1)
  End If
    
  w = RT.find(vbCrLf, w)
  w = w + 1
  w = w - 3
  For pstart = w To w - 160 Step -1
    RT.SelStart = pstart
    RT.SelLength = 2
    If RT.SelText = vbCrLf Or pstart = 0 Then
      w = pstart - 1
      Exit For
    End If
  Next pstart
  TestoREV = ""
  boladdtesto = insertPLI_INC_PtrVariables_OF(TestoREV, pIdOggetto, RT)
     
  RT.SelStart = w
  RT.SelLength = 0
        
End Sub

Public Sub Inizializza_Copy_IMSDB(RT As RichTextBox, RTBErr As RichTextBox, ptype As String, pIdOggetto As Long, Optional VsamBatch As Boolean = False)
  'controlla se il link con la copy LNKDBRT � presente nel file
  Dim t As Long, w As Long
  Dim Testo As String
  Dim s() As String
  Dim nfile As Long, nfile2 As Long
  Dim lFile As Long
  Dim sLine As String
  Dim templateName As String
  Dim keyword As String
  Dim boladdtesto As Boolean
  Dim pstart As Long
  
  boladdtesto = True
  ReDim CmpPcb(0)
  keyword = " WORKING-STORAGE "
  If ptype = "PLI" Then
    'AC 10/12/2007
    keyword = " DCL "
    'keyword = " BUILTIN;"
  End If
  
  w = RT.find(keyword, 1)
  If Not ptype = "PLI" Then
    
    For t = w To 1 Step -1
      If Mid(RT.text, t, 2) = vbCrLf Then
        If Mid(RT.text, t + 8, 1) = "*" Then
          w = RT.find(keyword, w + 1)
        End If
        Exit For
      End If
    Next t
  Else ' ramo PLI
     'AC 110/12/2007 asteriscata perc� prima doppia ricerca BUILTIN; e BUILTIN senza punto e virgola
'    If w = -1 Then 'BUILTIN; non trovato , cerco Builtin e dove finisce
'      w = RT.find("BUILTIN", 1)
'      keyword = "BUILTIN"
'      While PliWordCommented(1, Len(RT.text), w, RT)
'        w = RT.find(keyword, w + 1)
'      Wend
'      w = RT.find(";", w + 1)
'    Else
'      'verifico che non sia commentato AC 10/12/2007 : questa parte rimane
      If w = 1 Then
        AggLog "[Object: " & pIdOggetto & "] DCL label not find " & vbCrLf, MadrdF_Incapsulatore.RTErr
        Exit Sub
      End If
      While PliWordCommented(1, Len(RT.text), w, RT)
        w = RT.find(keyword, w + 1)
      Wend
      If w = -1 Then
        w = RT.find(" DCL ", 1)
        While PliWordCommented(1, Len(RT.text), w, RT)
          w = RT.find(keyword, w + 1)
        Wend
        w = RT.find(";", w + 1)
      End If
'    End If
    
  End If
  w = RT.find(vbCrLf, w)
  w = w + 1
  
  If ptype = "PLI" Then
    w = w - 3
    For pstart = w To w - 160 Step -1
      RT.SelStart = pstart
      RT.SelLength = 2
      If RT.SelText = vbCrLf Or pstart = 0 Then
        w = pstart - 1
        Exit For
      End If
    Next pstart
  End If
  
  t = -1
  If Not ptype = "PLI" Then
    t = RT.find("COPY LNKDBRT", 1)
  End If
  RT.Refresh
  AddRighe = 0
  
  If t = -1 Then
    'Template di Working:
    If EmbeddedRoutine Then
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\WorkingPgmDB"
    Else
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\InitCpyRoutIMSDB.tpl"
    End If
    
    If Len(Dir(templateName, vbNormal)) Then
      If Not dirincaps = "\PLI" Then  'CBL
        nfile = FreeFile
        lFile = FileLen(templateName)
        
        Open templateName For Binary As nfile
        Testo = vbCrLf
        While Loc(nfile) < lFile
          Line Input #nfile, sLine
          'Trascodifica le label
          If InStr(1, sLine, "#LABEL_AUTHOR#") Then
            sLine = Replace(sLine, "#LABEL_AUTHOR#", m_fun.LABEL_AUTHOR)
            Testo = Testo & vbCrLf & sLine
          ElseIf InStr(1, sLine, "#LABEL_INFO#") Then
            sLine = Replace(sLine, "#LABEL_INFO#", m_fun.LABEL_INFO)
            Testo = Testo & vbCrLf & sLine
          ElseIf InStr(1, sLine, "#LABEL_INCAPS#") Then
            sLine = Replace(sLine, "#LABEL_INCAPS#", m_fun.LABEL_INCAPS)
            Testo = Testo & vbCrLf & sLine
          Else
            Testo = Testo & LABEL_INFO & vbCrLf & sLine
          End If
          'testo = testo & vbCrLf & sLine
        Wend
        Close nfile
      End If
    Else ' NO TEMPLATE PRINCIPALE
      'SQ - E' sempre bello scrivere dentro i programmi generati!
      AggLog "[Object: " & pIdOggetto & "] TEMPLATE NOT FOUND: " & templateName, RTBErr
      Testo = vbCrLf
      Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
      Testo = Testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
      Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
    End If
      
    If ptype = "PLI" Then
      TestoREV = ""
      boladdtesto = insertPLIPtrVariables(TestoREV, pIdOggetto, RT)
    Else
      RT.SelStart = w
      RT.SelLength = 0
      RT.SelText = Testo
      s = Split(Testo, vbCrLf)
      AddRighe = AddRighe + UBound(s)
    End If
  End If
  
  ' Sono gi� dentro la WORKING-STORAGE, quindi ne approfitto per inserire i FILE-STATUS
  If VsamBatch And EmbeddedRoutine Then
    Inizializza_File_Status RT, RTBErr, ptype, pIdOggetto, w, "INCAPS"
  End If
  
End Sub

Public Sub Inizializza_File_Status(RT As RichTextBox, RTBErr As RichTextBox, ptype As String, pIdOggetto As Long, w As Long, Tool As String)

  Dim rsIstr As Recordset, rsVSAM As Recordset
  Dim i As Long
  Dim templateName As String, sLine As String, idSegmenti() As String, s() As String
  Dim nfile As Integer, lFile As Integer
  
  Set rsIstr = m_fun.Open_Recordset( _
    "SELECT DISTINCT b.idsegmentid FROM PSDLI_Istruzioni as a,PSDLI_IstrDett_Liv as b where " & _
    "a.idpgm = " & pIdOggetto & " and a.valida and a.to_migrate and " & _
    "a.idpgm = b.idpgm and a.idoggetto = b.idoggetto and a.riga = b.riga")
  Do Until rsIstr.EOF
    If InStr(rsIstr!IdSegmentiD, ";") Then
      idSegmenti = Split("0" & rsIstr!IdSegmentiD, ";")
    Else
      ReDim idSegmenti(0)
      'SQ pezza: ci sono istruzioni valide con idSegmentiD=null che non dovrebbero esserci (c'� anche la GN sq di Pippo...)
      '-> len(idSegmentiD)
      idSegmenti(0) = "0" & rsIstr!IdSegmentiD
    End If
    For i = 0 To UBound(idSegmenti)
      If CLng(idSegmenti(i)) Then
        Set rsVSAM = m_fun.Open_Recordset("select nome from DMDB2_Tabelle where idorigine = " & idSegmenti(i))
        If rsVSAM.RecordCount Then
          templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\COPY_FILE_STATUS.cpy"
          If Len(Dir(templateName, vbNormal)) Then
            nfile = FreeFile
            lFile = FileLen(templateName)

            Open templateName For Binary As nfile
            Testo = vbCrLf
            While Loc(nfile) < lFile
              Line Input #nfile, sLine

              'Trascodifica le label
              If InStr(1, sLine, "<NOMEVSAM>") Then
                sLine = Replace(sLine, "<NOMEVSAM>", rsVSAM!nome)
                Testo = Testo & vbCrLf & START_TAG & Mid(sLine, Len(START_TAG) + 1)
              Else
                Testo = Testo & vbCrLf & START_TAG & Mid(sLine, Len(START_TAG) + 1)
              End If
            Wend
            Close nfile
          Else
            'SQ - E' sempre bello scrivere dentro i programmi generati!
            AggLog "[Object: " & pIdOggetto & "] TEMPLATE NOT FOUND: " & templateName, RTBErr
            Testo = vbCrLf
            Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
            Testo = Testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
            Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
          End If
'            If ptype = "PLI" Then
'              boladdtesto = insertPLIPtrVariables(Testo, pIdOggetto, RT)
'            End If

          RT.SelStart = w
          RT.SelLength = 0

          RT.SelText = Testo
          s = Split(Testo, vbCrLf)
          AddRighe = AddRighe + UBound(s)
        End If
        rsVSAM.Close
      End If
    Next i
    rsIstr.MoveNext
  Loop
  rsIstr.Close
End Sub

Public Sub Inizializza_Data_Division(RT As RichTextBox, RTBErr As RichTextBox, ptype As String, pIdOggetto As Long, Tool As String)
  Dim t, t2, w, w2, w3 As Long
  Dim Testo As String
  Dim s() As String
  Dim nfile As Long, lFile As Long
  Dim sLine As String, templateName As String
  Dim keyword, keyword2, keyword3, section, token As String
  Dim boladdtesto As Boolean
  Dim k As Integer, filerow As String, stringTOCheck As String
  Dim isDivision As Boolean, isFile As Boolean, startw As Long, iscommento As Boolean
  Dim absoluteStart As Long

  On Error GoTo catch

  startw = 1
  boladdtesto = True
  ReDim CmpPcb(0)
  'keyword = " FILE SECTION."

  While Not isDivision
    While Not isFile
      keyword = " FILE "

      w = RT.find(keyword, startw)
      If w = -1 Then
        Exit Sub
      End If
      'If Not ptype = "PLI" Then
      iscommento = True
      While iscommento
        For t = w To 1 Step -1
          If Mid(RT.text, t, 2) = vbCrLf Then
            If Not Mid(RT.text, t + 8, 1) = "*" Then
              iscommento = False
            End If
            Exit For
          End If
        Next t
        If iscommento Then
          w = RT.find(keyword, w + 1)
          If w = -1 Then
            Exit Sub
          End If
        End If
       Wend
       ' controllo che sulla riga di FILE la parola successiva non sia diversa da section
       t2 = RT.find(vbCrLf, w)
       RT.SelStart = t + 1
       RT.SelLength = t2 - (t + 1)
       filerow = RT.SelText
       filerow = Left(RT.SelText, 72)
       token = ""
       While Not (token = "FILE" Or filerow = "")
         token = MadrdM_GenRout.nextToken(filerow)
       Wend
       token = MadrdM_GenRout.nextToken(filerow)
       If Not (token = "" Or token = "SECTION" Or token = "SECTION.") Then
         startw = w + 2
       Else
         isFile = True
         absoluteStart = t + 1
       End If
     Wend

      keyword2 = " SECTION"
      w2 = RT.find(keyword2, w)
      If w2 = -1 Then
        Exit Sub
      End If
      For t = w2 To 1 Step -1
        If Mid(RT.text, t, 2) = vbCrLf Then
          If Mid(RT.text, t + 8, 1) = "*" Then
            w2 = RT.find(keyword, w2 + 1)
            If w2 = -1 Then
              Exit Sub
            End If
          End If
          Exit For
        End If
      Next t
      keyword3 = "."
      w3 = RT.find(keyword3, w2)
      If w3 = -1 Then
        Exit Sub
      End If
      For t = w3 To 1 Step -1
        If Mid(RT.text, t, 2) = vbCrLf Then
          If Mid(RT.text, t + 8, 1) = "*" Then
            w3 = RT.find(keyword, w3 + 1)
            If w3 = -1 Then
              Exit Sub
            End If
          End If
          Exit For
        End If
      Next t
      RT.SelStart = absoluteStart
      RT.SelLength = w3 - absoluteStart + 1
      section = Split(RT.SelText, vbCrLf)
      For k = 0 To UBound(section)
        stringTOCheck = stringTOCheck & Left(section(k), 72)
      Next k
      token = ""
      While Not (token = "FILE" Or stringTOCheck = "")
        token = MadrdM_GenRout.nextToken(stringTOCheck) ' pulisco "FILE"
      Wend
      token = MadrdM_GenRout.nextToken(stringTOCheck)
      If token = "SECTION" Then
        token = MadrdM_GenRout.nextToken(stringTOCheck)
        If token = "." Then
          isDivision = True
        Else
          isFile = False
        End If
      ElseIf token = "SECTION." Then
        isDivision = True
      Else
        isFile = False
      End If
    'End If
    startw = w3
  Wend

  w = RT.find(vbCrLf, w3)
  w = w + 1

  Dim rsIstr As Recordset, rsVSAM As Recordset
  Dim idSegmenti() As String, i As Long
  Set rsIstr = m_fun.Open_Recordset("select distinct b.idsegmentid from PSDLI_Istruzioni as a,PSDLI_IstrDett_Liv as b where " & _
                                             "a.idpgm = " & pIdOggetto & " and a.valida and a.to_migrate and " & _
                                             "a.idpgm = b.idpgm and a.idoggetto = b.idoggetto and a.riga = b.riga")
  Do Until rsIstr.EOF
    If InStr(rsIstr!IdSegmentiD, ";") Then
      idSegmenti = Split("0" & rsIstr!IdSegmentiD, ";")
    Else
      ReDim idSegmenti(0)
      idSegmenti(0) = "0" & rsIstr!IdSegmentiD
    End If
    For i = 0 To UBound(idSegmenti)
      If CLng(idSegmenti(i)) Then
        Set rsVSAM = m_fun.Open_Recordset("select nome from DMDB2_Tabelle where idorigine = " & idSegmenti(i))
        If rsVSAM.RecordCount Then
          templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\COPY_DATA_DIVISION.cpy"

          If Len(Dir(templateName, vbNormal)) Then
            nfile = FreeFile
            lFile = FileLen(templateName)

            Open templateName For Binary As nfile
            Testo = vbCrLf
            While Loc(nfile) < lFile
              Line Input #nfile, sLine

              'Trascodifica le label
              If InStr(1, sLine, "<NOMEVSAM>") Then
                sLine = Replace(sLine, "<NOMEVSAM>", rsVSAM!nome)
                Testo = Testo & vbCrLf & START_TAG & Mid(sLine, Len(START_TAG) + 1)
              Else
                Testo = Testo & vbCrLf & START_TAG & Mid(sLine, Len(START_TAG) + 1)
              End If
            Wend
            Close nfile
          Else
            'SQ - E' sempre bello scrivere dentro i programmi generati!
            AggLog "[Object: " & pIdOggetto & "] TEMPLATE NOT FOUND: " & templateName, RTBErr
            'RTBErr.text = "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
            Testo = vbCrLf
            Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
            Testo = Testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
            Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
          End If
'            If ptype = "PLI" Then
'              boladdtesto = insertPLIPtrVariables(Testo, pIdOggetto, RT)
'            End If

          RT.SelStart = w
          RT.SelLength = 0

          If boladdtesto Then
            RT.SelText = Testo
            s = Split(Testo, vbCrLf)
            AddRighe = AddRighe + UBound(s)
          End If
        End If
        rsVSAM.Close
      End If
    Next i
    rsIstr.MoveNext
  Loop
  rsIstr.Close
'  End If
  Exit Sub
catch:
  'RTBErr.text = "* Object " & pIdOggetto & ": unexpected error." & vbCrLf
  AggLog "[Object: " & pIdOggetto & "] unexpected error on 'Inizializza_Data_Division'", RTBErr
End Sub

Public Sub Inizializza_File_Control(RT As RichTextBox, RTBErr As RichTextBox, ptype As String, pIdOggetto As Long, Tool As String)
  Dim t, w As Long
  Dim Testo As String
  Dim s() As String
  Dim nfile As Long
  Dim lFile As Long
  Dim sLine As String
  Dim templateName As String
  Dim keyword As String
  Dim boladdtesto As Boolean

  boladdtesto = True
  ReDim CmpPcb(0)
  keyword = " FILE-CONTROL."
    w = RT.find(keyword, 1)
    If Not ptype = "PLI" Then

      For t = w To 1 Step -1
        If Mid(RT.text, t, 2) = vbCrLf Then
          If Mid(RT.text, t + 8, 1) = "*" Then
            w = RT.find(keyword, w + 1)
          End If
          Exit For
        End If
      Next t

    End If
    w = RT.find(vbCrLf, w)
    w = w + 1

    Dim rsIstr As Recordset, rsVSAM As Recordset
    Dim idSegmenti() As String, i As Long
    Set rsIstr = m_fun.Open_Recordset("select distinct b.idsegmentid from PSDLI_Istruzioni as a,PSDLI_IstrDett_Liv as b where " & _
                                               "a.idpgm = " & pIdOggetto & " and a.valida and a.to_migrate and " & _
                                               "a.idpgm = b.idpgm and a.idoggetto = b.idoggetto and a.riga = b.riga")
    Do Until rsIstr.EOF
      If InStr(rsIstr!IdSegmentiD, ";") Then
        idSegmenti = Split("0" & rsIstr!IdSegmentiD, ";")
      Else
        ReDim idSegmenti(0)
        idSegmenti(0) = "0" & rsIstr!IdSegmentiD
      End If
      For i = 0 To UBound(idSegmenti)
        If CLng(idSegmenti(i)) Then
          Set rsVSAM = m_fun.Open_Recordset("select nome from DMDB2_Tabelle where idorigine = " & idSegmenti(i))
          If rsVSAM.RecordCount Then
            templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\COPY_FILE_CONTROL.cpy"

            If Len(Dir(templateName, vbNormal)) Then
              nfile = FreeFile
              lFile = FileLen(templateName)

              Open templateName For Binary As nfile
              Testo = vbCrLf
              While Loc(nfile) < lFile
                Line Input #nfile, sLine

                'Trascodifica le label
                If InStr(1, sLine, "<NOMEVSAM>") Then
                  sLine = Replace(sLine, "<NOMEVSAM>", rsVSAM!nome)
                  Testo = Testo & vbCrLf & START_TAG & Mid(sLine, Len(START_TAG) + 1)
                Else
                  Testo = Testo & vbCrLf & START_TAG & Mid(sLine, Len(START_TAG) + 1)
                End If
              Wend
              Close nfile
            Else
              'SQ - E' sempre bello scrivere dentro i programmi generati!
              AggLog "[Object: " & pIdOggetto & "] TEMPLATE NOT FOUND: " & templateName, RTBErr
              Testo = vbCrLf
              Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
              Testo = Testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
              Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
            End If
'            If ptype = "PLI" Then
'              boladdtesto = insertPLIPtrVariables(Testo, pIdOggetto, RT)
'            End If

            RT.SelStart = w
            RT.SelLength = 0

            If boladdtesto Then
              RT.SelText = Testo
              s = Split(Testo, vbCrLf)
              AddRighe = AddRighe + UBound(s)
            End If
          End If
          rsVSAM.Close
        End If
      Next i
      rsIstr.MoveNext
    Loop
    rsIstr.Close
'  End If
End Sub

Function insertPLIPtrVariables(Testo As String, poggetto As Long, RT As RichTextBox) As Boolean
  Dim rsNumPcb As Recordset, t As Long, w As Long, s() As String
  Dim lenAreaRED As Long, Pcbred As String, testosave As String, bolpcb As Boolean, bolSSA As Boolean
  Dim bolArea As Boolean, areaRed As String, ColPcbRed() As String, i As Integer
  Dim areaFeedRed As String
  Dim statement1 As String, statement2 As String
  Dim margine As Integer, indent As Integer, templateName As String
  Dim nfile As Long, nfile2 As Long, nfile3 As Long
  Dim lFile As Long
  Dim sLine As String
  Dim TemplateNameAppo As String, sLinePCBLIST As String, sLineAppo As String
  Dim ArrayPCB() As String, TestoPCBList As String, k As Integer
  Dim isPCBAREAto_manage As Boolean, pcbareas As String, sLinePCBAREA As String, testoAREAPCB As String
  Dim rsPtr As Recordset
  On Error GoTo err

  ReDim ArrayPCB(0)

  Set rsPtr = m_fun.Open_Recordset("select * from PsPgm where idoggetto = " & poggetto)
  If Not rsPtr.EOF Then
    If rsPtr!Main Then
      isPCBAREAto_manage = True
      pcbareas = rsPtr!parameters
      While Not pcbareas = ""
        ReDim Preserve ArrayPCB(UBound(ArrayPCB) + 1)
        If InStr(pcbareas, ",") > 0 Then
          ArrayPCB(UBound(ArrayPCB)) = Left(pcbareas, InStr(pcbareas, ",") - 1)
          pcbareas = Right(pcbareas, Len(pcbareas) - InStr(pcbareas, ","))
        Else
          ArrayPCB(UBound(ArrayPCB)) = pcbareas
          pcbareas = ""
        End If
      Wend
    End If
  End If
  rsPtr.Close
    
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps\PLI\InitCpyRoutIMSDB.tpl"
  If Len(Dir(templateName, vbNormal)) Then
    nfile = FreeFile
    lFile = FileLen(templateName)
    Open templateName For Binary As nfile
    Testo = vbCrLf
    While Loc(nfile) < lFile
      Line Input #nfile, sLine
      '**** nuovo template PCB_AREA_ASS
      If InStr(1, sLine, "<<PCB_AREA_ASS>>") Then
        If isPCBAREAto_manage Then
          TemplateNameAppo = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\PCB_AREA_ASS.tpl"
          nfile2 = FreeFile
          Open TemplateNameAppo For Binary As nfile2
          If Not EOF(nfile2) Then
            While Not EOF(nfile2)
              Line Input #nfile2, sLinePCBAREA
              If InStr(1, sLinePCBAREA, "<NUMPCBENTRIES>") Then
                sLinePCBAREA = Replace(sLinePCBAREA, "<NUMPCBENTRIES>", Format(UBound(ArrayPCB), "00"))
                testoAREAPCB = testoAREAPCB & vbCrLf & sLinePCBAREA
              ElseIf InStr(1, sLinePCBAREA, "<<PCBLIST>>") Then
                TemplateNameAppo = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps" & dirincaps & "\PCBLIST.tpl"
                nfile3 = FreeFile
                Open TemplateNameAppo For Binary As nfile3
                If Not EOF(nfile3) Then
                  Line Input #nfile3, sLinePCBLIST
                  For k = 1 To UBound(ArrayPCB)
                    sLineAppo = Replace(sLinePCBLIST, "<PCBNAME>", ArrayPCB(k))
                    sLineAppo = Replace(sLineAppo, "<PCBINDEX>", k)
                    sLineAppo = Replace(sLineAppo, "<SPACES_A>", Space(27 - Len(ArrayPCB(k)) - IIf(k < 10, 1, 4)))
                    TestoPCBList = TestoPCBList & vbCrLf & sLineAppo
                  Next k
                  testoAREAPCB = testoAREAPCB & TestoPCBList
                End If
                Close nfile3
              Else
                testoAREAPCB = testoAREAPCB & vbCrLf & sLinePCBAREA
              End If
            Wend
          End If
          Close nfile2
          Testo = Testo & testoAREAPCB
        End If
        '****
      Else
        Testo = Testo & LABEL_INFO & vbCrLf & sLine
      End If
    Wend
    Close nfile
  Else
    'SQ - E' sempre bello scrivere dentro i programmi generati!
    'AggLog "[Object: " & poggetto & "] TEMPLATE NOT FOUND: " & templatename, RTBErr
    Testo = vbCrLf
    Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
    Testo = Testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
    Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
  End If

  ReDim ColPcbRed(0)
  testosave = vbCrLf '& " /* " & START_TAG & " START              */"
  
  'IRIS_PTR
  'Mauro 04/08/2008
  
  'SQ usare count(*): dovrebbe fare molto prima!
  Set rsPtr = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & poggetto & " and pointer")
  If rsPtr.RecordCount Then
    statement1 = " DCL IRIS_PTR POINTER;"
    ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
    ColPcbRed(UBound(ColPcbRed)) = "IRIS_PTR"
    margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
    testosave = testosave & vbCrLf & _
                statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
  End If
  'SQ
  rsPtr.Close
  
  'SQ tutti 'sti blocchi sotto, non sono tutti uguali? Non possono essere "unificati"?
  
  'NUMPCB
  Set rsNumPcb = m_fun.Open_Recordset("select distinct NUMPcb from PsDli_Istruzioni where IdOggetto = " & poggetto & " and  to_migrate = true and obsolete = false")
  bolpcb = Not rsNumPcb.EOF
  While Not rsNumPcb.EOF
    If Not IsNull(rsNumPcb!numpcb) Then
      If Not rsNumPcb!numpcb = "" Then
        ' leanAreaRED variabile da valorizzare
        Pcbred = getPCDRedFromPCB(rsNumPcb!numpcb, poggetto, lenAreaRED)
        If Not Pcbred = "" Then
          For i = 1 To UBound(ColPcbRed)
           If ColPcbRed(i) = Trim(Pcbred) Then
            Exit For
           End If
          Next i
          If i = UBound(ColPcbRed) + 1 Then
            testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(Pcbred) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & Pcbred & "));        /*" & Trim(START_TAG) & "*/"
            ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
            ColPcbRed(UBound(ColPcbRed)) = Trim(Pcbred)
          End If
        End If
      Else
'        lenAreaRED = getPCBlen(rsNumPcb!NumPCB, poggetto)
'        testosave = testosave & vbCrLf & "     DCL IRIS_" & rsNumPcb!NumPCB & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & rsNumPcb!NumPCB & "));"
'        ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
'        ColPcbRed(UBound(ColPcbRed)) = Trim(Pcbred)
      End If
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
  ' DATA AREA
  'AC 09/01/08 query per IdPgm = poggetto (considero anche istruzioni in copy)
  'Set rsNumPcb = m_fun.Open_Recordset("select distinct DataArea,IdOggetto from PsDli_IstrDett_Liv where IdPgm = " & poggetto)
  'AC 18/07/08 considero solo quelle del programma: IdPgm = poggetto IdOggetto = poggetto
  Set rsNumPcb = m_fun.Open_Recordset("select distinct trim(DataArea) as DataArea,IdOggetto from PsDli_IstrDett_Liv where IdPgm = " & poggetto & " and IdOggetto = " & poggetto)
  bolArea = Not rsNumPcb.EOF
  While Not rsNumPcb.EOF
    statement1 = ""
    statement2 = ""
    'SQ: condizioni da portare nella WHERE CONDITION!
    If Not IsNull(rsNumPcb!DataArea) And Len(rsNumPcb!DataArea) Then
'      testosave = testosave & vbCrLf & "     DCL PTR_SAVE_" & rsNumPcb!NumPCB & "            PTR;"
      areaRed = findReadd2(rsNumPcb!DataArea, poggetto, rsNumPcb!IdOggetto, lenAreaRED)
      If Not areaRed = "" Then
        For i = 1 To UBound(ColPcbRed)
          If ColPcbRed(i) = Trim(areaRed) Then Exit For
        Next i
        If i = UBound(ColPcbRed) + 1 Then
          testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(areaRed) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & areaRed & "));"
          ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
          ColPcbRed(UBound(ColPcbRed)) = Trim(areaRed)
        End If
      Else
        Dim ProcName As String
        Dim dataType As String
        'SQ [01] (ottiene: byte, type, procedure)
        'lenAreaRED = getAreaLen(rsNumPcb!DataArea, rsNumPcb!idOggetto)
        getAreaInfo rsNumPcb!DataArea, rsNumPcb!IdOggetto, lenAreaRED, dataType, ProcName
        '''''''''''''''''''''''''''''''''
        'SQ tmp!!! ex 27
        ' SQ 12-05-08 VEDI SOtto
        '''''''''''''''''''''''''''''''''
        'testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(rsNumPcb!dataarea) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & rsNumPcb!dataarea & "));" _
        '                      & Space(33 - 2 * Len(Trim(rsNumPcb!dataarea)) - Len(CStr(lenAreaRED))) & "/*" & Trim(START_TAG) & "*/"
        ' Mauro 01/08/2008
        If InStr(rsNumPcb!DataArea, "(") Then
          Dim DataArea As String, DataAreaAdd As String
          Dim rsOccurs As Recordset
          
          DataAreaAdd = Trim(Left(rsNumPcb!DataArea, InStr(rsNumPcb!DataArea, "(") - 1))
          DataArea = Trim(Replace(Left(rsNumPcb!DataArea, InStr(rsNumPcb!DataArea, "(") - 1), ".", "_"))
          For i = 1 To UBound(ColPcbRed)
            If ColPcbRed(i) = DataArea Then Exit For
          Next i
          If i = UBound(ColPcbRed) + 1 Then
            Set rsOccurs = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & rsNumPcb!IdOggetto & " and nomecmp = '" & DataArea & "'")
            If rsOccurs.RecordCount = 0 Then
              statement1 = " DCL IRIS_" & DataArea & "(" & getAreaoccurs(DataArea, rsNumPcb!IdOggetto) & ") CHAR(" & lenAreaRED & ")"
              'statement2 = " BASED(ADDR(" & DataArea & "));"
              statement2 = " BASED(ADDR(" & DataAreaAdd & "));"
            Else
              If rsOccurs!pointer Then
                statement1 = " DCL IRIS_" & DataArea & " CHAR(" & lenAreaRED & ")"
                statement2 = " BASED(IRIS_PTR);"
              End If
            End If
            rsOccurs.Close
          End If
        Else
          'SQ [02] (I campi "X" non li aggiungiamo!)
          If dataType <> "X" Then
            For i = 1 To UBound(ColPcbRed)
              If ColPcbRed(i) = Trim(rsNumPcb!DataArea) Then Exit For
            Next i
            If i = UBound(ColPcbRed) + 1 Then
              statement1 = " DCL IRIS_" & rsNumPcb!DataArea & " CHAR(" & lenAreaRED & ")"
              statement2 = " BASED(ADDR(" & rsNumPcb!DataArea & "));"
            End If
          End If
        End If
        If Len(statement1) > 0 And Len(statement2) > 0 Then
          margine = 72 - (Len(statement1 & statement2) + Len("/*" & Trim(START_TAG) & "*/"))
          If margine > 0 Then
            '1 riga:
            testosave = testosave & vbCrLf & _
                        statement1 & statement2 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
          Else
            '2 righe:
            indent = 3 'costante
            margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
            testosave = testosave & vbCrLf & _
                        statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
            margine = 72 - (Len(statement2) + Len("/*" & Trim(START_TAG) & "*/"))
            testosave = testosave & vbCrLf & _
                        Space(indent) & statement2 & Space(margine - indent) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
          End If
        End If
        ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
        ColPcbRed(UBound(ColPcbRed)) = rsNumPcb!DataArea
      End If
      'AC 27/05/08
'      If lenAreaRED = 0 Then
'        AggLog "[Object: " & poggetto & "] Lenght Area Redefine = 0", MadrdF_Incapsulatore.RTErr
'      End If
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
  'KEYFEEDBACK
  Set rsNumPcb = m_fun.Open_Recordset("select distinct Keyfeedback,IdOggetto from PsDli_Istruzioni where IdPgm = " & poggetto)
  While Not rsNumPcb.EOF
    If Not IsNull(rsNumPcb!keyfeedback) Then
      If Not rsNumPcb!keyfeedback = "" Then
        lenAreaRED = getAreaLen(rsNumPcb!keyfeedback, rsNumPcb!IdOggetto)
        For i = 1 To UBound(ColPcbRed)
          If ColPcbRed(i) = Trim(rsNumPcb!keyfeedback) Then Exit For
        Next i
        If i = UBound(ColPcbRed) + 1 Then
          testosave = testosave & vbCrLf & _
                      " DCL IRIS_" & Trim(rsNumPcb!keyfeedback) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & rsNumPcb!keyfeedback & "));" _
                      & Space(27 - 2 * Len(Trim(rsNumPcb!keyfeedback)) - Len(CStr(lenAreaRED))) & "/*" & Trim(START_TAG) & "*/"
          ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
          ColPcbRed(UBound(ColPcbRed)) = Trim(rsNumPcb!keyfeedback)
        End If
'        If lenAreaRED = 0 Then
'          AggLog "[Object: " & poggetto & "] Lenght Area Redefine = 0" & vbCrLf, MadrdF_Incapsulatore.RTErr
'        End If
      End If
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
'  ' SSA
  Set rsNumPcb = m_fun.Open_Recordset("select distinct trim(NomeSSA) as NomeSSA from PsDli_IstrDett_Liv where IdOggetto = " & poggetto)
  bolSSA = Not rsNumPcb.EOF
  While Not rsNumPcb.EOF
    If Not (rsNumPcb!NomeSSA = "" Or Left(rsNumPcb!NomeSSA, 1) = "'") Then
      'AC 14/05/2009
      lenAreaRED = getAreaLen(rsNumPcb!NomeSSA, poggetto)
      'testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(rsNumPcb!NomeSSA) & " CHAR(100) BASED(ADDR(" & rsNumPcb!NomeSSA & "));"
      testosave = testosave & vbCrLf & _
                  " DCL IRIS_" & Trim(rsNumPcb!NomeSSA) & " CHAR(" & CStr(lenAreaRED) & ") BASED(ADDR(" & rsNumPcb!NomeSSA & "));" & _
                  Space(33 - Len(CStr(lenAreaRED)) - 2 * Len(rsNumPcb!NomeSSA)) & "/*" & Trim(START_TAG) & "*/"
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
 
  ' AC 10/12/2007  KEYVALUE
  'AC 09/01/08 query per IdPgm = poggetto (considero anche istruzioni in copy)
  'Set rsNumPcb = m_fun.Open_Recordset("select distinct Valore,IdOggetto from PsDli_IstrDett_Key where IdPgm = " & poggetto)
  'AC 18/07/08 considero solo quelle del programma: IdPgm = poggetto IdOggetto = poggetto
  Set rsNumPcb = m_fun.Open_Recordset("select distinct trim(Valore) as valore,IdOggetto from PsDli_IstrDett_Key where IdPgm = " & poggetto & " and IdOggetto = " & poggetto)
  While Not rsNumPcb.EOF
    statement1 = ""
    statement2 = ""
    If Not (rsNumPcb!Valore = "" Or Left(Trim(rsNumPcb!Valore), 1) = "'") Then
      'lenAreaRED = getAreaLen(rsNumPcb!Valore, rsNumPcb!idOggetto)
      getAreaInfo rsNumPcb!Valore, rsNumPcb!IdOggetto, lenAreaRED, dataType, ProcName
      
      ''''''''''''''''''''''''''''''''''''''''
      'SQ TMP!!!!!!!!!!!!!!!!!!!! ERA 27...
      'fare funzioncina generica/comune (vedi writeLine o qualcosa di simile nel COBOL)!
      ''''''''''''''''''''''''''''''''''''''''
      'testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(rsNumPcb!Valore) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & Trim(rsNumPcb!Valore) & "));" _
      '            & Space(27 - 2 * Len(Trim(rsNumPcb!Valore)) - Len(CStr(lenAreaRED))) & "/*" & Trim(START_TAG) & "*/"
      If InStr(rsNumPcb!Valore, "(") Then
        Dim Valore As String, ValoreAdd As String
        ValoreAdd = Trim(Left(rsNumPcb!Valore, InStr(rsNumPcb!Valore, "(") - 1))
        Valore = Trim(Replace(Left(rsNumPcb!Valore, InStr(rsNumPcb!Valore, "(") - 1), ".", "_"))
        Set rsOccurs = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & rsNumPcb!IdOggetto & " and nomecmp = '" & Valore & "'")
        If rsOccurs.RecordCount = 0 Then
          statement1 = " DCL IRIS_" & Valore & "(" & getAreaoccurs(Valore, rsNumPcb!IdOggetto) & ") CHAR(" & lenAreaRED & ")"
          'statement2 = " BASED(ADDR(" & Valore & "));"
          statement2 = " BASED(ADDR(" & ValoreAdd & "));"
        Else
          If rsOccurs!pointer Then
            statement1 = " DCL IRIS_" & Valore & " CHAR(" & lenAreaRED & ")"
            statement2 = " BASED(IRIS_PTR);"
          End If
        End If
        rsOccurs.Close
      Else
        'SQ [02] (I campi "X" non li aggiungiamo!)
        If dataType <> "X" Then
          statement1 = " DCL IRIS_" & Trim(Replace(rsNumPcb!Valore, ".", "_")) & " CHAR(" & lenAreaRED & ")"
          'statement2 = " BASED(ADDR(" & Trim(Replace(rsNumPcb!Valore, ".", "_")) & "));"
          'AC 06/04/09  tolto il replace . _ per il based address
          statement2 = " BASED(ADDR(" & rsNumPcb!Valore & "));"
        End If
      End If
      If Len(statement1) > 0 And Len(statement2) > 0 Then
        'FUNZ:
        margine = 72 - (Len(statement1 & statement2) + Len("/*" & Trim(START_TAG) & "*/"))
        If margine > 0 Then
          '1 riga:
          testosave = testosave & vbCrLf & _
                      statement1 & statement2 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
        Else
            '2 righe:
            indent = 3 'costante
            margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
            testosave = testosave & vbCrLf & _
                        statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
            margine = 72 - (Len(statement2) + Len("/*" & Trim(START_TAG) & "*/"))
            testosave = testosave & vbCrLf & _
                        Space(indent) & statement2 & Space(margine - indent) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
        End If
      End If
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
  'AC 09/01/08 query per IdPgm = poggetto (considero anche istruzioni in copy)
  Set rsNumPcb = m_fun.Open_Recordset("select riga from PsDli_Istruzioni where " & _
                                      "IdPgm = " & poggetto & " and  to_migrate = true and obsolete = false and Valida = true")
  insertPLIPtrVariables = Not rsNumPcb.EOF
  rsNumPcb.Close
  
  '  testoback = testoback & "              /* " & START_TAG & " END             */" & vbCrLf
  
  ' external entry - riempita con il nome di tutte le routine dopo
  ' l'incapsulamento delle istruzioni (si perde la traslazione reale delle istruzioni
  testosave = testosave & vbCrLf & _
              "<externalentry>"
  testosave = testosave & vbCrLf '& " /* " & START_TAG & " END                */"


'AC 08/10/08 il testosave per Reale Mutua non pu� essere svuotato,
'ci serve il tag <externalentry> da riempire con " DCL SE0R001 ENTRY OPTIONS(COBOL);"
  If Not bolpcb And Not bolSSA And Not bolArea And Not Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
    testosave = ""  'SQ: chiarire
  End If
  
  'Ac 21/05/08
  testosave = Replace(testosave, "IRIS_$", "IRIS_")
  
  Testo = Testo & testosave
  Exit Function
err:
  'SQ
  AggLog "[Object: " & poggetto & "] VARIABLES INSERT ERROR", MadrdF_Incapsulatore.RTErr
End Function

Sub insertREV(IdOggetto As Long, c As String, RT As RichTextBox)
  Dim keyword As String
  Dim w As Long, pstart As Long
  Dim s() As String
  Dim numRiga As Long
  
  keyword = " DCL "
  w = RT.find(keyword, 1)
  If w = 1 Then
    AggLog "[Object: " & IdOggetto & "] DCL label not find " & vbCrLf, MadrdF_Incapsulatore.RTErr
    Exit Sub
  End If
  While PliWordCommented(1, Len(RT.text), w, RT)
    w = RT.find(keyword, w + 1)
  Wend
  If w = -1 Then
    w = RT.find(" DCL ", 1)
    While PliWordCommented(1, Len(RT.text), w, RT)
      w = RT.find(keyword, w + 1)
    Wend
    w = RT.find(";", w + 1)
  End If
  w = RT.find(vbCrLf, w)
  w = w + 1
  w = w - 3
  For pstart = w To w - 160 Step -1
    RT.SelStart = pstart
    RT.SelLength = 2
    If RT.SelText = vbCrLf Or pstart = 0 Then
      w = pstart - 1
      Exit For
    End If
  Next pstart

  RT.SelText = c
  'IRIS-DB: it checks if inserted lines are before or after the DLI instructions, adjusting the counter correspondingly
  '         un po' empirico perch� dipende da dove si arriva, ma per ora lasciamo cos�
  numRiga = RT.GetLineFromChar(w)
  m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = OffsetEncaps - " & AddRighe & " where idoggetto = " & IdOggetto & " and Riga < " & numRiga

  s = Split(c, vbCrLf)
  AddRighe = AddRighe + UBound(s)
End Sub

'SQ: chiarire cosa cambia con quella "PLI non INC"
Function insertPLI_INC_PtrVariables(Testo As String, poggetto As Long, RT As RichTextBox) As Boolean
  Dim rsNumPcb As Recordset, t As Long, w As Long, s() As String
  Dim lenAreaRED As Long, Pcbred As String, testosave As String, bolpcb As Boolean, bolSSA As Boolean
  Dim bolArea As Boolean, areaRed As String, ColPcbRed() As String, i As Integer
  Dim areaFeedRed As String
  Dim statement1 As String, statement2 As String
  Dim margine As Integer, indent As Integer
  'SQ [01]
  Dim ProcName As String
  Dim dataType As String

  'AC 19/07/08
  ' Versione per le INC, solo AREE e KEYVALUE (tolte dal programma chiamante)
  On Error GoTo err
  
  ReDim ColPcbRed(0)
  testosave = vbCrLf '& " /* " & START_TAG & " START              */"
  
  ' Mauro 04/08/2008
  Dim rsPtr As Recordset
  Set rsPtr = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & poggetto & " and pointer")
  If rsPtr.RecordCount Then
    statement1 = " DCL IRIS_PTR POINTER;"
    ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
    ColPcbRed(UBound(ColPcbRed)) = "IRIS_PTR"
    margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
    testosave = testosave & vbCrLf & _
                statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
  End If
  
  ' DATA AREA
  Set rsNumPcb = m_fun.Open_Recordset("select distinct trim(DataArea) as DataArea,IdOggetto from PsDli_IstrDett_Liv where  IdOggetto = " & poggetto)
  If Not rsNumPcb.EOF Then
    bolArea = True
  End If
  While Not rsNumPcb.EOF
    statement1 = ""
    statement2 = ""
    If Not IsNull(rsNumPcb!DataArea) And Len(rsNumPcb!DataArea) Then
      areaRed = findReadd2(rsNumPcb!DataArea, poggetto, rsNumPcb!IdOggetto, lenAreaRED)
      If Not areaRed = "" Then
        For i = 1 To UBound(ColPcbRed)
          If ColPcbRed(i) = Trim(areaRed) Then Exit For
        Next i
        If i = UBound(ColPcbRed) + 1 Then
          testosave = testosave & vbCrLf & " DCL IRIS_" & Trim(areaRed) & " CHAR(" & lenAreaRED & ") BASED(ADDR(" & areaRed & "));"
          ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
          ColPcbRed(UBound(ColPcbRed)) = Trim(areaRed)
        End If
      Else
        'SQ [01]
        'lenAreaRED = getAreaLen(rsNumPcb!DataArea, rsNumPcb!idOggetto)
        getAreaInfo rsNumPcb!DataArea, rsNumPcb!IdOggetto, lenAreaRED, dataType, ProcName
        ' Mauro 01/08/2008
        If InStr(rsNumPcb!DataArea, "(") Then
          Dim DataArea As String
          Dim rsOccurs As Recordset
          DataArea = Trim(Replace(Left(rsNumPcb!DataArea, InStr(rsNumPcb!DataArea, "(") - 1), ".", "_"))
          For i = 1 To UBound(ColPcbRed)
            If ColPcbRed(i) = Trim(areaRed) Then Exit For
          Next i
          If i = UBound(ColPcbRed) + 1 Then
            Set rsOccurs = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & rsNumPcb!IdOggetto & " and nomecmp = '" & DataArea & "'")
            If rsOccurs.RecordCount = 0 Then
              statement1 = " DCL IRIS_" & DataArea & "(" & getAreaoccurs(DataArea, rsNumPcb!IdOggetto) & ") CHAR(" & lenAreaRED & ")"
              statement2 = " BASED(ADDR(" & DataArea & "));"
            Else
              If rsOccurs!pointer Then
                statement1 = " DCL IRIS_" & DataArea & " CHAR(" & lenAreaRED & ")"
                statement2 = " BASED(IRIS_PTR);"
              End If
            End If
            rsOccurs.Close
          End If
        Else
          'SQ [02]
          If dataType <> "X" Then
            For i = 1 To UBound(ColPcbRed)
              If ColPcbRed(i) = Trim(areaRed) Then Exit For
            Next i
            If i = UBound(ColPcbRed) + 1 Then
              statement1 = " DCL IRIS_" & Trim(rsNumPcb!DataArea) & " CHAR(" & lenAreaRED & ")"
              statement2 = " BASED(ADDR(" & rsNumPcb!DataArea & "));"
            End If
          End If
        End If
        If Len(statement1) > 0 Or Len(statement2) > 0 Then
          margine = 72 - (Len(statement1 & statement2) + Len("/*" & Trim(START_TAG) & "*/"))
          If margine > 0 Then
            '1 riga:
            testosave = testosave & vbCrLf & _
                        statement1 & statement2 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
          Else
            '2 righe:
            indent = 3 'costante
            margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
            testosave = testosave & vbCrLf & _
                        statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
            margine = 72 - (Len(statement2) + Len("/*" & Trim(START_TAG) & "*/"))
            testosave = testosave & vbCrLf & _
                        Space(indent) & statement2 & Space(margine - indent) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
          End If
        End If
        ReDim Preserve ColPcbRed(UBound(ColPcbRed) + 1)
        ColPcbRed(UBound(ColPcbRed)) = Trim(rsNumPcb!DataArea)
      End If
      
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
  'KEY
  Set rsNumPcb = m_fun.Open_Recordset("select distinct trim(Valore) as valore,IdOggetto from PsDli_IstrDett_Key where IdOggetto = " & poggetto)
  While Not rsNumPcb.EOF
    statement1 = ""
    statement2 = ""
    If Not (rsNumPcb!Valore = "" Or Left(Trim(rsNumPcb!Valore), 1) = "'") Then
      'SQ [01]
      'lenAreaRED = getAreaLen(rsNumPcb!Valore, rsNumPcb!idOggetto)
      getAreaInfo rsNumPcb!Valore, rsNumPcb!IdOggetto, lenAreaRED, dataType, ProcName
      If InStr(rsNumPcb!Valore, "(") Then
        Dim Valore As String
        Valore = Trim(Replace(Left(rsNumPcb!Valore, InStr(rsNumPcb!Valore, "(") - 1), ".", "_"))
        Set rsOccurs = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where idoggetto = " & rsNumPcb!IdOggetto & " and nomecmp = '" & Valore & "'")
        If rsOccurs.RecordCount = 0 Then
          statement1 = " DCL IRIS_" & Valore & "(" & getAreaoccurs(Valore, rsNumPcb!IdOggetto) & ") CHAR(" & lenAreaRED & ")"
          statement2 = " BASED(ADDR(" & Valore & "));"
        Else
          If rsOccurs!pointer Then
            statement1 = " DCL IRIS_" & Valore & " CHAR(" & lenAreaRED & ")"
            statement2 = " BASED(IRIS_PTR);"
          End If
        End If
        rsOccurs.Close
      Else
        'SQ [02]
        If dataType <> "X" Then
          statement1 = " DCL IRIS_" & Trim(Replace(rsNumPcb!Valore, ".", "_")) & " CHAR(" & lenAreaRED & ")"
          statement2 = " BASED(ADDR(" & Trim(Replace(rsNumPcb!Valore, ".", "_")) & "));"
        End If
      End If
      
      If Len(statement1) > 0 And Len(statement2) > 0 Then
        'FUNZ:
        margine = 72 - (Len(statement1 & statement2) + Len("/*" & Trim(START_TAG) & "*/"))
        If margine > 0 Then
          '1 riga:
          testosave = testosave & vbCrLf & _
                        statement1 & statement2 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
        Else
            '2 righe:
            indent = 3 'costante
            margine = 72 - (Len(statement1) + Len("/*" & Trim(START_TAG) & "*/"))
            testosave = testosave & vbCrLf & _
                        statement1 & Space(margine) & "/*" & Trim(START_TAG) & "*/"
            margine = 72 - (Len(statement2) + Len("/*" & Trim(START_TAG) & "*/"))
            testosave = testosave & vbCrLf & _
                        Space(indent) & statement2 & Space(margine - indent) & "/*" & Trim(START_TAG) & "*/" & vbCrLf
        End If
      End If
    End If
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  
  'AC  query per IdOggetto = poggetto
  Set rsNumPcb = m_fun.Open_Recordset("select riga from PsDli_Istruzioni where IdOggetto = " & poggetto & " and  to_migrate = true and obsolete = false and Valida = true")
  If Not rsNumPcb.EOF Then
    insertPLI_INC_PtrVariables = True
  Else
    insertPLI_INC_PtrVariables = False
  End If
  rsNumPcb.Close
 
  
  'testosave = testosave & vbCrLf & "<externalentry>"
  testosave = testosave & vbCrLf '& " /* " & START_TAG & " END                */"

  If Not bolpcb And Not bolSSA And Not bolArea Then
    testosave = ""
  End If
  
  'Ac 21/05/08
  testosave = Replace(testosave, "IRIS_$", "IRIS_")
  
  Testo = Testo & testosave
  Exit Function
err:
  'SQ
  AggLog "[Object: " & poggetto & "] VARIABLES INSERT ERROR", MadrdF_Incapsulatore.RTErr
End Function
Public Sub Inizializza_Copy_IMSDB_EZT(RT As RichTextBox, RTBErr As RichTextBox)
    Dim t, w As Long
    Dim Testo As String
    Dim s() As String
    Dim nfile As Long
    Dim lFile As Long
    Dim sLine As String
    Dim templateName As String
    
    'stefano: deve cambiare la definizione dei file dbd in aree di di working
    ' per ora faccio finta che l'offset macro non esista...
    
    ReDim CmpPcb(0)
    Dim wJI() As String
    Dim x As Long
    
  On Error GoTo ErrJob
''''    t = RT.Find("COPY LNKDBRT", 1)
''''    RT.Refresh
''''    AddRighe = 0
    
''''    If t = -1 Then
        'wJI = Split(sLine, " ")
        'w = RT.Find("JOB INPUT", 1) 'MATTEOOOO
        Dim foundx As Boolean
        foundx = False
        w = 1
        'stefano: un po meglio, ma ancora non gestisce le asteriscate, ecc.
        While Not foundx And w < Len(RT.text)
          w = RT.find("JOB ", w + 1)
          x = RT.find(vbCrLf, w) 'VIRGILIOOO
          x = RT.find("INPUT", w, x)
          If x > 0 Then
           foundx = True
          End If
        Wend
''''        For t = w To 1 Step -1
''''          If Mid(RT.text, t, 2) = vbCrLf Then
''''             If Mid(RT.text, t + 8, 1) = "*" Then
''''                w = RT.Find(" WORKING-STORAGE ", w + 1)
''''             End If
''''             Exit For
''''          End If
''''        Next t
        
        w = RT.find(vbCrLf, w - 2)
        w = w - 1
        RT.SelStart = w
        RT.SelLength = 0
        
        'Template di Working:
        If EmbeddedRoutine Then
          templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\EZT\WorkingPgmEZTDB"
        Else
          templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps\EZT\InitCpyRoutIMSDBezt.tpl"
        End If
        
        If Len(Dir(templateName, vbNormal)) Then
          nfile = FreeFile
          lFile = FileLen(templateName)
          
          Open templateName For Binary As nfile
          Testo = vbCrLf
          While Loc(nfile) < lFile
            Line Input #nfile, sLine
            
            
            'Trascodifica le label
            If InStr(1, sLine, "#LABEL_AUTHOR#") > 0 Then
                sLine = Replace(sLine, "#LABEL_AUTHOR#", m_fun.LABEL_AUTHOR)
                Testo = Testo & vbCrLf & sLine
                
            ElseIf InStr(1, sLine, "#LABEL_INFO#") > 0 Then
                sLine = Replace(sLine, "#LABEL_INFO#", m_fun.LABEL_INFO)
                Testo = Testo & vbCrLf & sLine
                
            ElseIf InStr(1, sLine, "#LABEL_INCAPS#") > 0 Then
                sLine = Replace(sLine, "#LABEL_INCAPS#", m_fun.LABEL_INCAPS)
                Testo = Testo & vbCrLf & sLine
            Else
                Testo = Testo & LABEL_INFO & vbCrLf & sLine
            End If
            
            
            'testo = testo & vbCrLf & sLine
          Wend
          Close nfile
        Else
          'SQ - E' sempre bello scrivere dentro i programmi generati!
          AggLog "* TEMPLATE NOT FOUND: " & templateName, RTBErr
          Testo = vbCrLf
          Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
          Testo = Testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
          Testo = Testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
        End If
        
        RT.SelText = Testo
        
        s = Split(Testo, vbCrLf)
        AddRighe = UBound(s)
''''    End If
        Exit Sub
ErrJob:
  MsgBox "JOB INPUT not found "

End Sub

Public Function Restituisci_Selezione(TR As TreeView) As String
  Dim Selezionati As String
  Dim i As Long, j As Long
  
  For i = 1 To TR.Nodes.Count
    If TR.Nodes(i).Checked Then
      Selezionati = Selezionati & IIf(Len(Selezionati), ",'", "'") & Trim(UCase(TR.Nodes(i).key)) & "'"
      If Selezionati = "'SRC'" Then
        Exit For
      End If
    End If
  Next i
  If Selezionati = "'SRC'" Then
    Selezionati = ""
    For j = 1 To TR.Nodes(i).Children
      If TR.Nodes(i + j).Checked Then
        Selezionati = Selezionati & IIf(Len(Selezionati), ",'", "'") & TR.Nodes(i + j).key & "'"
      End If
    Next j
  End If
  Restituisci_Selezione = Selezionati
End Function

Function getPCDRedFromPCB(pPCB As String, pIdOggetto As Long, Optional lenArea As Long = 0) As String
  Dim rsData As Recordset
  
  Set rsData = m_fun.Open_Recordset("select Nome,Byte from PsData_Cmp WHERE " & _
                                    "IdOggetto=" & pIdOggetto & " AND (Nomered='" & pPCB & "' or Nomered='(" & pPCB & ")')")
  If Not rsData.EOF Then
    lenArea = rsData!Byte
    getPCDRedFromPCB = rsData!nome
  End If
  rsData.Close
End Function

Function getPCBlen(pPCB As String, pIdOggetto As Long) As Integer
  Dim rsData As Recordset
  
  Set rsData = m_fun.Open_Recordset("select Byte from PsData_Cmp where Nome = '" & pPCB & "'  and IdOggetto = " & pIdOggetto)
  If Not rsData.EOF Then
    getPCBlen = CInt(rsData!Byte)
  End If
  rsData.Close
End Function

Function findReadd2(DataArea As String, pIdOggetto As Long, pidOggettoC As Long, Optional lenArea As Long = 0) As String
  Dim rsData As Recordset
  
  Set rsData = m_fun.Open_Recordset("select nome,valore from PsData_CmpMoved where " & _
                                    "nome = '" & DataArea & "' and Left(valore,5) = 'ADDR(' and IdPgm = " & pIdOggetto)
  If Not rsData.EOF Then
    findReadd2 = Mid(rsData!Valore, 6, Len(rsData!Valore) - 6)
    lenArea = getAreaLen(findReadd2, pidOggettoC)
  End If
  rsData.Close
End Function

Function findReadd(DataArea As String, pIdOggetto As Long, Optional lenArea As Long = 0) As String
  Dim rsData As Recordset
  
  Set rsData = m_fun.Open_Recordset("select nome,valore from PsData_CmpMoved where " & _
                                    "nome = '" & DataArea & "' and Left(valore,5) = 'ADDR(' and IdPgm = " & pIdOggetto)
  If Not rsData.EOF Then
    findReadd = Mid(rsData!Valore, 6, Len(rsData!Valore) - 6)
    lenArea = getAreaLen(findReadd, pIdOggetto)
  End If
  rsData.Close
End Function

Function getAreaoccurs(pArea As String, pIdOggetto As Long, Optional pIdPgm As Long = -1) As Long
  Dim rsData As Recordset, pAreaOld As String, warningOccurs As String
  Dim message As Boolean
  On Error GoTo err
  
  pAreaOld = pArea
  If InStr(pArea, ".") > 0 Then
    pArea = Right(pArea, Len(pArea) - InStr(pArea, "."))
  End If
  If InStr(2, pArea, "(") > 0 Then
    pArea = Left(pArea, InStr(2, pArea, "(") - 1)
    warningOccurs = "-warning: Area " & pArea & " is an occursed area "
  End If
  If pIdPgm = -1 Then
    Set rsData = m_fun.Open_Recordset("select occurs from PsData_Cmp where Nome = '" & Trim(pArea) & "'  and IdOggetto = " & pIdOggetto)
  Else
    Set rsData = m_fun.Open_Recordset("SELECT occurs From PsData_Cmp where " & _
                                      "idOggetto = " & pIdPgm & " And Nome = '" & Trim(pArea) & "'" & _
                                      " UNION SELECT Byte From PsData_Cmp as a,PsRel_Obj as b where " & _
                                      "b.idOggettoC = " & pIdPgm & " and b.relazione='INC' And" & _
                                      " a.IdOggetto=b.IdOggettoR and a.Nome = '" & Trim(pArea) & "'")
  End If
  If Not rsData.EOF Then
    getAreaoccurs = rsData!Occurs
  End If
  rsData.Close
  ''''' Reale mutua
  If getAreaoccurs = 0 Then
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'SQ 21-04-08
    ' Metodo non molto scientifico, ma "sufficientemente" sicuro:
    ' Cerco il campo sulle aree "UFFICIALI"
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set rsData = m_fun.Open_Recordset("select distinct occurs from MgData_Cmp where Nome = '" & Trim(pArea) & "' and occurs > 0 order by occurs desc")
    If Not rsData.EOF Then
      getAreaoccurs = rsData!Occurs
      If rsData.RecordCount > 1 Then
        AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " warning: Area " & pArea & " not found in program... Multiple values found; used: " & getAreaoccurs, MadrdF_Incapsulatore.RTErr
        message = True
      End If
    Else
      rsData.Close 'SQ: serve?
      Set rsData = m_fun.Open_Recordset("select distinct occurs from PsData_Cmp where Nome = '" & Trim(pArea) & "' and occurs > 0 order by occurs desc")
      If Not rsData.EOF Then
        getAreaoccurs = rsData!Occurs
        If rsData.RecordCount > 1 Then
          ' Mauro 16/07/2008: **********Start**************
          If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
            Set rsData = m_fun.Open_Recordset("select distinct Byte from PsData_Cmp_incaps where idoggetto = " & pIdOggetto & " and Nome = '" & Trim(pArea) & "'  and Byte > 0 order by byte desc")
            If Not rsData.EOF Then
              getAreaoccurs = rsData!Byte
              If rsData.RecordCount > 1 Then
                AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " warning: Area " & pArea & " not found in program... Multiple values found; used: " & getAreaoccurs, MadrdF_Incapsulatore.RTErr
                message = True
              End If
            Else
              AggLog "[Object: " & pIdOggetto & "]-warning: Area " & pArea & " not found in program... Multiple values found; used: " & getAreaoccurs, MadrdF_Incapsulatore.RTErr
              message = True
            End If
            ' Mauro 16/07/2008: ********** End **************
          Else
            AggLog "[Object: " & pIdOggetto & "]-warning: Area " & pArea & " not found in program... Multiple values found; used: " & getAreaoccurs, MadrdF_Incapsulatore.RTErr
            message = True
          End If
        End If
      ElseIf Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
        Set rsData = m_fun.Open_Recordset("select distinct occurs from PsData_Cmp_incaps where Nome = '" & Trim(pArea) & "' and occurs > 0 order by occurs desc")
        If Not rsData.EOF Then
          getAreaoccurs = rsData!Byte
          If rsData.RecordCount > 1 Then
            AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " warning: Area " & pArea & " not found in program... Multiple values found; used: " & getAreaoccurs, MadrdF_Incapsulatore.RTErr
            message = True
          End If
        Else
          AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " Area " & pArea & " not found in program... Variable defined with occurs=0!", MadrdF_Incapsulatore.RTErr
          message = True
        End If
      Else
        AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " Area " & pArea & " not found in program... Variable defined with occurs=0!", MadrdF_Incapsulatore.RTErr
        message = True
      End If
    End If
    rsData.Close
  End If
  If Not message And Not warningOccurs = "" Then  ' devo segnalare il warning per le occurs
    AggLog "[Object: " & pIdOggetto & "]" & warningOccurs, MadrdF_Incapsulatore.RTErr
  End If
  pArea = pAreaOld
  Exit Function
err:
  AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " Area " & pArea & " program error... Variable defined with occurs=0!", MadrdF_Incapsulatore.RTErr
End Function

'SQ: copione di getAreaLen...
Sub getAreaInfo(pArea As String, pIdOggetto As Long, areaLen As Long, dataType As String, ProcName As String, Optional pIdPgm As Long = -1)
  Dim rsData As Recordset, pAreaOld As String, warningOccurs As String
  Dim message As Boolean
  
  'init
  areaLen = 0
  dataType = ""
  'AC 18/02/10
  
  
  'ProcName = ""
  
  On Error GoTo err
  
  pAreaOld = pArea  'SQ che tristezza... (no dai, scherzo: per� passare i parametri "byval", se non li si vuole "sporcare")
  
  If InStr(pArea, ".") > 0 Then
    pArea = Right(pArea, Len(pArea) - InStr(pArea, "."))
  End If
  If InStr(2, pArea, "(") > 0 Then
    pArea = Left(pArea, InStr(2, pArea, "(") - 1)
    warningOccurs = "-warning: Area " & pArea & " is an occursed area "
  End If
  If pIdPgm = -1 Then
    'SQ [01]
    pArea = Replace(pArea, "'", "")
    Set rsData = m_fun.Open_Recordset("SELECT [Byte], tipo, proc,[usage], 'PGM' as itemType from PsData_Cmp where " & _
                                      "Nome = '" & Trim(pArea) & "' and IdOggetto = " & pIdOggetto)
    '''''''''''''''''''''''''''''''''''''''''''''''
    ' Cos'� pIdPGM? Sembra non venga mai usato...
    ' Cos� non va sulle copy!!!
    '''''''''''''''''''''''''''''''''''''''''''''''
    If rsData.EOF Then
      rsData.Close ' serve?
      'Cerco in copy:
      Set rsData = m_fun.Open_Recordset( _
        "SELECT Byte,tipo,proc,[usage],'CPY' as itemType,nomeComponente as copyName From PsData_Cmp as a,PsRel_Obj as b " & _
        "WHERE b.idOggettoC = " & pIdOggetto & " and b.relazione='INC' And" & _
        " a.IdOggetto=b.IdOggettoR and a.Nome = '" & Trim(pArea) & "'")
    End If
    'SQ 17-12-08
  Else
    'SQ buttare: mai utilizzato!
    Set rsData = m_fun.Open_Recordset( _
      "SELECT Byte,tipo,proc,[usage],'PGM' as itemType From PsData_Cmp " & _
      "Where idOggetto = " & pIdPgm & " And Nome = '" & Trim(pArea) & "'" & _
      " UNION " & _
      "SELECT Byte,tipo,proc,[usage],'CPY' as itemType From PsData_Cmp as a,PsRel_Obj as b" & _
      " where b.idOggettoC = " & pIdPgm & " and b.relazione='INC' And" & _
      " a.IdOggetto=b.IdOggettoR and a.Nome = '" & Trim(pArea) & "'")
  End If
  If Not rsData.EOF Then
    areaLen = rsData!Byte
    dataType = rsData!tipo & ""
    
    '___________AC 09/06/10 uso il parametro Procname per chiedere la verifica del campo Usage
    If dataType = "X" And ProcName = "?USAGE" Then
        dataType = dataType & (rsData!Usage & "")
    End If
    '_________________________
    
    'SQ [01]
    'PROC (PLI)
    'AC 18/02/10
    If ProcName = "" Then 'valorizzata selo se entro con argomento Procname = ""
        If rsData!itemType = "PGM" Then
          ProcName = rsData!proc & ""
        Else
          'DCL in copy: cerco la PROC a cui appartiene la COPY
          Dim rs As Recordset
          'TMP: sfruttata tabella esistente... rinominare campi!!
          Set rs = m_fun.Open_Recordset("SELECT replacingvar as [proc] FROM PsPgm_Copy WHERE idPgm=" & pIdOggetto & " AND copy='" & rsData!copyName & "'")
          If Not rs.EOF Then
            ProcName = rs!proc & ""
          End If
          rs.Close
        End If
    End If
    
  Else
    'SQ: pu� essere dentro copy...
    DoEvents
  End If
  rsData.Close
  '''''''''''''''''''''''''''''''''''''''''
  ' SQ [01] DEBUG
  '''''''''''''''''''''''''''''''''''''''''
  'IRIS-DB m_fun.WriteLog "_1_|" & pIdOggetto & "|" & pArea & "|" & areaLen & "|" & dataType & "|" & ProcName
  '''''''''''''''''''''''''''''''''''''''''
    
  ''''''''''''''''''''''''''''''''''''''''
  ' Info non trovate. Ricerca "pezza":
  ''''''''''''''''''''''''''''''''''''''''
  

 
  '--------------
'  If areaLen = 0 Then
'    AggLog "[Object: " & pIdOggetto & "]-warning: Area " & pArea & " not found in program", MadrdF_Incapsulatore.RTErr
'    message = True
'  End If
  

  
  
  ' NON ENTRIAMO PIU' QUI
  If areaLen = 0 Then
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'SQ 21-04-08
    ' Metodo non molto scientifico, ma "sufficientemente" sicuro:
    ' Cerco il campo sulle aree "UFFICIALI"
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set rsData = m_fun.Open_Recordset("select distinct Byte from MgData_Cmp where Nome = '" & Trim(pArea) & "' and Byte > 0 order by byte desc")
    If Not rsData.EOF Then
      areaLen = rsData!Byte
      If rsData.RecordCount > 1 Then
        AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " warning: Area " & pArea & " not found in program... Multiple values found; used: " & areaLen, MadrdF_Incapsulatore.RTErr
        message = True
      End If
    Else
      rsData.Close 'SQ: serve?
      Set rsData = m_fun.Open_Recordset("select distinct Byte from PsData_Cmp where Nome = '" & Trim(pArea) & "'  and Byte > 0 order by byte desc")
      If Not rsData.EOF Then
        areaLen = rsData!Byte
        If rsData.RecordCount > 1 Then
          ' Mauro 16/07/2008: **********Start**************
          If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
            Set rsData = m_fun.Open_Recordset("select distinct Byte from PsData_Cmp_incaps where idoggetto = " & pIdOggetto & " and Nome = '" & Trim(pArea) & "'  and Byte > 0 order by byte desc")
            If Not rsData.EOF Then
              areaLen = rsData!Byte
              If rsData.RecordCount > 1 Then
                AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " warning: Area " & pArea & " not found in program... Multiple values found; used: " & areaLen, MadrdF_Incapsulatore.RTErr
                message = True
              End If
            Else
              AggLog "[Object: " & pIdOggetto & "]-warning: Area " & pArea & " not found in program... Multiple values found; used: " & areaLen, MadrdF_Incapsulatore.RTErr
              message = True
            End If
            ' Mauro 16/07/2008: ********** End **************
          Else
            AggLog "[Object: " & pIdOggetto & "]-warning: Area " & pArea & " not found in program... Multiple values found; used: " & areaLen, MadrdF_Incapsulatore.RTErr
            message = True
          End If
        End If
      ElseIf Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
        Set rsData = m_fun.Open_Recordset("select distinct Byte from PsData_Cmp_incaps where Nome = '" & Trim(pArea) & "'  and Byte > 0 order by byte desc")
        If Not rsData.EOF Then
          areaLen = rsData!Byte
          If rsData.RecordCount > 1 Then
            AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " warning: Area " & pArea & " not found in program... Multiple values found; used: " & areaLen, MadrdF_Incapsulatore.RTErr
            message = True
          End If
        Else
          AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " Area " & pArea & " not found in program... Variable defined with len=0!", MadrdF_Incapsulatore.RTErr
          message = True
        End If
      Else
        AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " Area " & pArea & " not found in program... Variable defined with len=0!", MadrdF_Incapsulatore.RTErr
        message = True
      End If
    End If
    rsData.Close
    '''''''''''''''''''''''''''''''''''''''''
    ' SQ [01] DEBUG
    '''''''''''''''''''''''''''''''''''''''''
    'IRIS-DB m_fun.WriteLog "_2_|" & pIdOggetto & "|" & pArea & "|" & areaLen & "|" & dataType & "|" & ProcName
    '''''''''''''''''''''''''''''''''''''''''
  End If
  If Not message And Not warningOccurs = "" Then  ' devo segnalare il warning per le occurs
    AggLog "[Object: " & pIdOggetto & "]" & warningOccurs, MadrdF_Incapsulatore.RTErr
  End If
  pArea = pAreaOld
  
  Exit Sub
err:
  AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " Area " & pArea & " program error... Variable defined with len=0!", MadrdF_Incapsulatore.RTErr
End Sub

Sub getAreaType(pArea As String, pIdOggetto As Long, areaLen As Long, dataType As String, ProcName As String, Optional pIdPgm As Long = -1)
  Dim rsData As Recordset, pAreaOld As String, warningOccurs As String
  Dim message As Boolean
  
  'init
  areaLen = 0
  dataType = ""
  'AC 18/02/10
  'ProcName = ""
  
  On Error GoTo err
  
  pAreaOld = pArea  'SQ che tristezza... (no dai, scherzo: per� passare i parametri "byval", se non li si vuole "sporcare")
  
  If InStr(pArea, ".") > 0 Then
    pArea = Right(pArea, Len(pArea) - InStr(pArea, "."))
  End If
  If InStr(2, pArea, "(") > 0 Then
    pArea = Left(pArea, InStr(2, pArea, "(") - 1)
    warningOccurs = "-warning: Area " & pArea & " is an occursed area "
  End If
  If pIdPgm = -1 Then
    'SQ [01]
    pArea = Replace(pArea, "'", "")
    Set rsData = m_fun.Open_Recordset("SELECT [Byte], tipo, proc,[usage], 'PGM' as itemType from PsData_Cmp where " & _
                                      "Nome = '" & Trim(pArea) & "' and IdOggetto = " & pIdOggetto)
    '''''''''''''''''''''''''''''''''''''''''''''''
    ' Cos'� pIdPGM? Sembra non venga mai usato...
    ' Cos� non va sulle copy!!!
    '''''''''''''''''''''''''''''''''''''''''''''''
    If rsData.EOF Then
      rsData.Close ' serve?
      'Cerco in copy:
      Set rsData = m_fun.Open_Recordset( _
        "SELECT Byte,tipo,proc,[usage],'CPY' as itemType,nomeComponente as copyName From PsData_Cmp as a,PsRel_Obj as b " & _
        "WHERE b.idOggettoC = " & pIdOggetto & " and b.relazione='INC' And" & _
        " a.IdOggetto=b.IdOggettoR and a.Nome = '" & Trim(pArea) & "'")
    End If
    'SQ 17-12-08
  Else
    'SQ buttare: mai utilizzato!
    Set rsData = m_fun.Open_Recordset( _
      "SELECT Byte,tipo,proc,[usage],'PGM' as itemType From PsData_Cmp " & _
      "Where idOggetto = " & pIdPgm & " And Nome = '" & Trim(pArea) & "'" & _
      " UNION " & _
      "SELECT Byte,tipo,proc,[usage],'CPY' as itemType From PsData_Cmp as a,PsRel_Obj as b" & _
      " where b.idOggettoC = " & pIdPgm & " and b.relazione='INC' And" & _
      " a.IdOggetto=b.IdOggettoR and a.Nome = '" & Trim(pArea) & "'")
  End If
  If Not rsData.EOF Then
    areaLen = rsData!Byte
    If Not rsData!Usage = "PTR" Then
      dataType = rsData!tipo & ""
    Else
      dataType = "PTR"
    End If
    
    '___________AC 09/06/10 uso il parametro Procname per chiedere la verifica del campo Usage
    If dataType = "X" And ProcName = "?USAGE" Then
      dataType = dataType & (rsData!Usage & "")
    End If
  End If
  rsData.Close
  Exit Sub
err:
  AggLog "[Object: " & pIdOggetto & "] Area " & pArea & " program error... type not found", MadrdF_Incapsulatore.RTErr
End Sub

Function getAreaLen(pArea As String, pIdOggetto As Long, Optional pIdPgm As Long = -1) As Long
  Dim rsData As Recordset, pAreaOld As String, warningOccurs As String
  Dim message As Boolean
  
  On Error GoTo err
    
  pAreaOld = pArea
  If InStr(pArea, ".") > 0 Then
    pArea = Right(pArea, Len(pArea) - InStr(pArea, "."))
  End If
  If InStr(2, pArea, "(") > 0 Then
    pArea = Left(pArea, InStr(2, pArea, "(") - 1)
    warningOccurs = "-warning: Area " & pArea & " is an occursed area "
  End If
  If Not InStr(pArea, "!!") > 0 Then
    If pIdPgm = -1 Then
      Set rsData = m_fun.Open_Recordset("select Byte from PsData_Cmp where Nome = '" & Trim(pArea) & "'  and IdOggetto = " & pIdOggetto)
    Else
      Set rsData = m_fun.Open_Recordset("SELECT Byte From PsData_Cmp " & _
                                    "Where idOggetto = " & pIdPgm & " And Nome = '" & Trim(pArea) & "'" & _
                                    " UNION " & _
                                    "SELECT Byte From PsData_Cmp as a,PsRel_Obj as b" & _
                                    " where b.idOggettoC = " & pIdPgm & " and b.relazione='INC' And" & _
                                    " a.IdOggetto=b.IdOggettoR and a.Nome = '" & Trim(pArea) & "'")
    End If
    If Not rsData.EOF Then
      getAreaLen = rsData!Byte
    End If
    rsData.Close
  Else
    getAreaLen = -11 ' codifichiamo concatenazione non risolta
  End If
  ''''' Reale mutua
  If getAreaLen = 0 Then
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'SQ 21-04-08
    ' Metodo non molto scientifico, ma "sufficientemente" sicuro:
    ' Cerco il campo sulle aree "UFFICIALI"
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Set rsData = m_fun.Open_Recordset("select distinct Byte from MgData_Cmp where Nome = '" & Trim(pArea) & "' and Byte > 0 order by byte desc")
    If Not rsData.EOF Then
      getAreaLen = rsData!Byte
      If rsData.RecordCount > 1 Then
        AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " warning: Area " & pArea & " not found in program... Multiple values found; used: " & getAreaLen, MadrdF_Incapsulatore.RTErr
        message = True
      End If
    Else
      rsData.Close 'SQ: serve?
      Set rsData = m_fun.Open_Recordset("select distinct Byte from PsData_Cmp where Nome = '" & Trim(pArea) & "'  and Byte > 0 order by byte desc")
      If Not rsData.EOF Then
        getAreaLen = rsData!Byte
        If rsData.RecordCount > 1 Then
          ' Mauro 16/07/2008: **********Start**************
          If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
            Set rsData = m_fun.Open_Recordset("select distinct Byte from PsData_Cmp_incaps where idoggetto = " & pIdOggetto & " and Nome = '" & Trim(pArea) & "'  and Byte > 0 order by byte desc")
            If Not rsData.EOF Then
              getAreaLen = rsData!Byte
              If rsData.RecordCount > 1 Then
                AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " warning: Area " & pArea & " not found in program... Multiple values found; used: " & getAreaLen, MadrdF_Incapsulatore.RTErr
                message = True
              End If
            Else
              AggLog "[Object: " & pIdOggetto & "]-warning: Area " & pArea & " not found in program... Multiple values found; used: " & getAreaLen, MadrdF_Incapsulatore.RTErr
              message = True
            End If
            ' Mauro 16/07/2008: ********** End **************
          Else
            AggLog "[Object: " & pIdOggetto & "]-warning: Area " & pArea & " not found in program... Multiple values found; used: " & getAreaLen, MadrdF_Incapsulatore.RTErr
            message = True
          End If
        End If
      ElseIf Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
        Set rsData = m_fun.Open_Recordset("select distinct Byte from PsData_Cmp_incaps where Nome = '" & Trim(pArea) & "'  and Byte > 0 order by byte desc")
        If Not rsData.EOF Then
          getAreaLen = rsData!Byte
          If rsData.RecordCount > 1 Then
            AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " warning: Area " & pArea & " not found in program... Multiple values found; used: " & getAreaLen, MadrdF_Incapsulatore.RTErr
            message = True
          End If
        Else
          AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " Area " & pArea & " not found in program... Variable defined with len=0!", MadrdF_Incapsulatore.RTErr
          message = True
        End If
      Else
        AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " Area " & pArea & " not found in program... Variable defined with len=0!", MadrdF_Incapsulatore.RTErr
        message = True
      End If
    End If
    rsData.Close
  End If
  If getAreaLen = -11 Then
    getAreaLen = 0
    AggLog "[Object: " & pIdOggetto & "] Concatenation for Area " & pArea & " not solved ... Variable defined with len=0!", MadrdF_Incapsulatore.RTErr
    message = True
  End If
  If Not message And Not warningOccurs = "" Then  ' devo segnalare il warning per le occurs
    AggLog "[Object: " & pIdOggetto & "]" & warningOccurs, MadrdF_Incapsulatore.RTErr
  End If
  pArea = pAreaOld
  
  '''''''''''''''''''''''''''''''''''''''''
  ' SQ [01] DEBUG
  '''''''''''''''''''''''''''''''''''''''''
  'IRIS-DB m_fun.WriteLog "_0_|" & pIdOggetto & "|" & pArea & "|" & getAreaLen & "|" & " " & "|" & " "
  Exit Function
err:
  AggLog "[Object: " & pIdOggetto & "]" & warningOccurs & " Area " & pArea & " program error... Variable defined with len=0!", MadrdF_Incapsulatore.RTErr
End Function

Function GetSenSeg(IdPSB As Long, numpcb As Integer, idDBD As Long) As String
  Dim rsSenSeg As Recordset, rsSegmenti As Recordset, NumSegmenti As Integer, idSeg As Long
'  Set rsSegmenti = m_fun.Open_Recordset("select Count(*) as totSeg from PsDli_Segmenti where IdOggetto = " & idDBD)
'  If Not rsSegmenti.EOF Then
'    NumSegmenti = rsSegmenti!totSeg
'  End If
'  rsSegmenti.Close
  Set rsSenSeg = m_fun.Open_Recordset("select ordinale,Segmento from PsDli_PsbSeg where " & _
                                      "IdOggetto = " & IdPSB & " and NumPcb = " & numpcb & " order by ordinale")
  'AC 20/11/08 tolta opzione ALL  ... li metto comunque tutti per preservare l'ordine
'  If Not rsSenSeg.EOF Then
'    If NumSegmenti = rsSenSeg.RecordCount Then
'      GetSenSeg = "ALL"
'      Exit Function
'    End If
'  End If
  ' Mauro 04/03/2010 : Controllo se i segmenti sono obsoleti, senn� rischio di generare decodifiche diverse
  While Not rsSenSeg.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select idsegmento from MgDli_Segmenti where " & _
                                          "IdOggetto = " & idDBD & " and nome = '" & rsSenSeg!Segmento & "' and " & _
                                          "Correzione = 'O'")
    If rsSegmenti.EOF Then
      rsSegmenti.Close
      Set rsSegmenti = m_fun.Open_Recordset("select idsegmento from PsDli_Segmenti where " & _
                                            "IdOggetto = " & idDBD & " and nome = '" & rsSenSeg!Segmento & "'")
      If Not rsSegmenti.EOF Then
        idSeg = rsSegmenti!idSegmento
        rsSegmenti.Close
        Set rsSegmenti = m_fun.Open_Recordset("select count(*) as Ordinale from PsDli_Segmenti where " & _
                                              "IdOggetto = " & idDBD & " and IdSegmento < " & idSeg)
        If Not rsSegmenti.EOF Then
        'IRIS-DB: usa il nome invece dell'ordinale nel DBD, per ora non sono rimosse le letture sopra che diventano inutili
        'IRIS-DB: potrebbe dare problemi di spazio in futuro, per ora va bene
          'IRIS-DB GetSenSeg = GetSenSeg & IIf(Len(GetSenSeg), ",", "") & Format(rsSegmenti!ordinale + 1, "00")
          GetSenSeg = GetSenSeg & IIf(Len(GetSenSeg), ",", "") & Trim(rsSenSeg!Segmento) 'IRIS-DB
        End If
        rsSegmenti.Close
      End If
    End If
    rsSenSeg.MoveNext
  Wend
  rsSenSeg.Close
  'If Len(GetSenSeg) Then
  '  GetSenSeg = Left(GetSenSeg, Len(GetSenSeg) - 1)
  'End If
End Function
Function GetSegmentsFromDbd(idDBD As Long, Istruzione As String) As String 'IRIS-DB
  Dim rsSegmenti As Recordset
  Set rsSegmenti = m_fun.Open_Recordset("select Nome, Parent from PsDli_Segmenti where idOggetto = " & idDBD & " order by idsegmento") 'IRIS-DB ordinati come nel DBD
  While Not rsSegmenti.EOF
    If Not ((Istruzione = "GNP" Or Istruzione = "GHNP") And rsSegmenti!parent = "0") Then
      GetSegmentsFromDbd = GetSegmentsFromDbd & IIf(Len(GetSegmentsFromDbd), ",", "") & Trim(rsSegmenti!nome)
    End If
    rsSegmenti.MoveNext
  Wend
  rsSegmenti.Close
End Function

Function CaricaIstruzione(rst As Recordset) As Boolean
  Dim tb As Recordset, tb1 As Recordset, rs As Recordset
  Dim IndI As Integer, k As Integer, K1 As Integer, k2 As Integer, k3 As Integer, z As Integer
  Dim u As Long
  Dim wXDField As Boolean
  Dim vEnd As Double, vPos As Double, vPos1 As Double, vStart As Double
  Dim wstr As String
  Dim TbLenField As Recordset
  Dim sDbd() As Long
  Dim sPsb() As Long
  Dim sSegD() As String
  Dim TbKey As Recordset
  
  ReDim TabIstr.BlkIstr(0)
  ReDim TabIstr.DBD(0)
  ReDim TabIstr.psb(0)
  
  
  
  SwTp = False
  
  'AC 07/04/08
  If Not IsAlwaysBatch Then
    'SQ - Mi sembra strano che non abbiamo ancora letto la BS_OGGETTI...
    'Set tb = m_fun.Open_Recordset("select * from BS_Oggetti where idoggetto = " & rst!idoggetto)
    Set tb = m_fun.Open_Recordset("select * from BS_Oggetti where idoggetto = " & rst!idpgm)
    If tb.RecordCount Then
      SwTp = tb!CICS
    End If
    tb.Close
  End If
  
  '''''''''''''''DEVE CARICARE PI� DBD e pi�PSB
  sDbd = getDBDByIdOggettoRiga(rst!idpgm, rst!IdOggetto, rst!Riga)
  sPsb = getPSBByIdOggettoRiga(rst!idpgm, rst!IdOggetto, rst!Riga)
  
  For u = 1 To UBound(sDbd)
    ReDim Preserve TabIstr.DBD(u)
    TabIstr.DBD(u).Id = sDbd(u)
    TabIstr.DBD(u).nome = Restituisci_NomeOgg_Da_IdOggetto(sDbd(u))
    TabIstr.DBD(u).tipo = TYPE_DBD
  Next u
  
  'per ora � senza vettore
  TabIstr.isGsam = False
  If UBound(TabIstr.DBD) Then
    Set tb1 = m_fun.Open_Recordset("select * from BS_Oggetti where tipo_DBD = 'GSA' and nome = '" & TabIstr.DBD(1).nome & "'")
    TabIstr.isGsam = Not tb1.EOF
    tb1.Close
  End If
  
  For u = 1 To UBound(sPsb)
    ReDim Preserve TabIstr.psb(u)
    TabIstr.psb(u).Id = sPsb(u)
    TabIstr.psb(u).nome = Restituisci_NomeOgg_Da_IdOggetto(sPsb(u))
    TabIstr.psb(u).tipo = TYPE_PSB
  Next u
  
  TabIstr.pcb = rst!numpcb & ""
  If Not IsNull(rst!numpcb) Then
    TabIstr.Pcbred = getPCDRedFromPCB(rst!numpcb, rst!IdOggetto)
  End If
  If TabIstr.Pcbred = "" Then
    TabIstr.Pcbred = TabIstr.pcb
  End If
  
  'SQ
  ' Mauro 16/10/2007
  On Error Resume Next
  TabIstr.numpcb = IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
  If err.Number > 0 Then TabIstr.numpcb = 0 & rst!ordPcb
  On Error GoTo 0
  
  If UBound(TabIstr.psb) Then
    TabIstr.procSeq = Restituisci_ProcSeq(TabIstr.psb(1).Id, TabIstr.numpcb)
    TabIstr.procOpt = Restituisci_ProcOpt(TabIstr.psb(1).Id, TabIstr.numpcb)
  Else
    TabIstr.procSeq = ""
    TabIstr.procOpt = ""
  End If
  
  If Trim(rst!Istruzione) = "QUERY" Then
    TabIstr.istr = "QRY"
    TabIstr.istrOriginalMolt = "QRY"
  Else
    TabIstr.istr = IIf(isUnificaGH, Trim(rst!Istruzione & ""), Replace(Trim(rst!Istruzione & ""), "GH", "G"))
    TabIstr.istrOriginalMolt = Trim(rst!Istruzione & "")
  End If
  
  'ginevra: keyfeedback
  If Trim(rst!keyfeedback) <> "" Then
    TabIstr.keyfeedback = Trim(rst!keyfeedback)
  Else
    TabIstr.keyfeedback = ""
  End If
    
  'XRST - carico aree
  If TabIstr.istr = "XRST" Or TabIstr.istr = "CHKP" Then
    Dim maparea As String, MapName As String, mapset As String
    maparea = IIf(IsNull(rst!maparea), "", rst!maparea)
    MapName = IIf(IsNull(rst!MapName), "", rst!MapName)
    mapset = IIf(IsNull(rst!mapset), "", rst!mapset)
    If InStr(maparea, ";") > 0 Then
      TabIstr.IdArea = Left(maparea, InStr(maparea, ";") - 1)
      TabIstr.IdAreaLen = Right(maparea, Len(maparea) - InStr(maparea, ";"))
    Else
      TabIstr.IdArea = maparea
      TabIstr.IdAreaLen = ""
    End If
    ReDim TabIstr.SavedAreas(0)
    ReDim TabIstr.SavedAreasLen(0)
    While Len(MapName)
      ReDim Preserve TabIstr.SavedAreas(UBound(TabIstr.SavedAreas) + 1)
      If InStr(MapName, ";") > 0 Then
        TabIstr.SavedAreas(UBound(TabIstr.SavedAreas)) = Left(MapName, InStr(MapName, ";") - 1)
        MapName = Right(MapName, Len(MapName) - InStr(MapName, ";"))
      Else
        TabIstr.SavedAreas(UBound(TabIstr.SavedAreas)) = MapName
        MapName = ""
      End If
    Wend
    While Len(mapset)
      ReDim Preserve TabIstr.SavedAreasLen(UBound(TabIstr.SavedAreasLen) + 1)
      If InStr(mapset, ";") > 0 Then
        TabIstr.SavedAreasLen(UBound(TabIstr.SavedAreasLen)) = Left(mapset, InStr(mapset, ";") - 1)
        mapset = Right(mapset, Len(mapset) - InStr(mapset, ";"))
      Else
        TabIstr.SavedAreasLen(UBound(TabIstr.SavedAreasLen)) = mapset
        mapset = ""
      End If
    Wend
  End If
    
  If TabIstr.istr = "CHKP" Or TabIstr.istr = "TERM" Or _
     TabIstr.istr = "SYNC" Or TabIstr.istr = "PCB" Or _
     TabIstr.istr = "QRY" Then
    ' Mauro 15/06/2007 : Id del programma chiamante
    'SwTp = m_fun.Is_CICS(rst!IdOggetto)
   
    'AC 07/04/08
    If IsAlwaysBatch Then
      SwTp = False
    Else
      SwTp = m_fun.Is_CICS(rst!idpgm)
    End If
       
    ' Lo faccio dopo! A cosa mi serve farlo qui?
    ' stefano: ma come fa a funzionare che indi non � impostato? Oppure zero va bene come valore?
'''    If UBound(TabIstr.DBD) > 0 Then
'''      TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, TabIstr.istr, SwTp)
'''    Else
'''      TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("", TabIstr.istr, SwTp)
'''    End If
  End If
  
  'SQ CPY-ISTR
  Set tb = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrDett_Liv WHERE " & _
                     "idPgm=" & rst!idpgm & " AND idoggetto=" & rst!IdOggetto & " and riga = " & rst!Riga & " order by livello")
  If tb.RecordCount Then
    IndI = 0
    
    While Not tb.EOF
      IndI = IndI + 1
      ReDim Preserve TabIstr.BlkIstr(IndI)
      k3 = 0
      ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
       
      ' Mauro 15/06/2007 : Id del programma chiamante
      'SwTp = m_fun.Is_CICS(tb!IdOggetto)
      If IsAlwaysBatch Then
        SwTp = False
      Else
        SwTp = m_fun.Is_CICS(tb!idpgm)
      End If
             
      TabIstr.BlkIstr(IndI).ssaName = Trim(tb!NomeSSA & " ")
      ' lunghezza area (impostat solo per i GSAM per ora)
      If Not IsNull(tb!areaLen) And tb!areaLen > 0 Then
        TabIstr.BlkIstr(IndI).areaLen = tb!areaLen
      Else
        TabIstr.BlkIstr(IndI).areaLen = 0
      End If
      '<NONE>
      If Len(TabIstr.BlkIstr(IndI).ssaName) Then
        'stefano: SSA costante
        If InStr(TabIstr.BlkIstr(IndI).ssaName, "'") > 0 Then
          TabIstr.BlkIstr(IndI).SsaLen = Len(TabIstr.BlkIstr(IndI).ssaName) - 2 'toglie gli apici
        Else
          TabIstr.BlkIstr(IndI).SsaLen = m_fun.TrovaLenSSA(TabIstr.BlkIstr(IndI).ssaName, rst!IdOggetto)
        End If
      End If
       
      'SQ tmp...
      TabIstr.BlkIstr(IndI).Parentesi = IIf(tb!Qualificazione, "(", " ")
                     
      TabIstr.BlkIstr(IndI).SegLen = tb!SegLen
      TabIstr.BlkIstr(IndI).Search = IIf(IsNull(tb!condizione), "", tb!condizione)
      While InStr(TabIstr.BlkIstr(IndI).Search, ";;") > 0
        TabIstr.BlkIstr(IndI).Search = Replace(TabIstr.BlkIstr(IndI).Search, ";;", ";")
      Wend

      If UBound(TabIstr.DBD) > 0 Then
        ' Mauro 05/06/2008 : Se � un DBD Index, il nome routine lo devo costruire con il DBD Fisico
        Dim rsDBD As Recordset
        Dim NomeDbD As String
        
        NomeDbD = TabIstr.DBD(1).nome
        Set rsDBD = m_fun.Open_Recordset("select idoggetto from Bs_Oggetti where " & _
                                         "idoggetto = " & TabIstr.DBD(1).Id & " and tipo = 'DBD' and tipo_dbd = 'IND'")
        If rsDBD.RecordCount Then
          NomeDbD = Restituisci_DBD_Da_DBDIndex(rsDBD!IdOggetto)
        End If
        rsDBD.Close
        If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
          If InStr(NomeDbD, "RKKUNL") Then
            TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("RKLUNL", TabIstr.istr, SwTp)
          ElseIf InStr(NomeDbD, "RKGIRL") Then
            TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("RKOIRL", TabIstr.istr, SwTp)
          Else
            TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(NomeDbD, TabIstr.istr, SwTp)
          End If
        Else
          'TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, TabIstr.istr, SwTp)
          TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(NomeDbD, TabIstr.istr, SwTp)
        End If
      Else
        TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("", TabIstr.istr, SwTp)
      End If
      'alpitour: 5/8/2005 forzato diversamente; la gestione della data area andava fatta meglio
      ' o perlomeno completata prima di renderla effettiva;
      ' in questo modo si sono aumentate le mancanze, in quanto la generazione routine
      ' faceva un ragionamento diverso
      'If tb!StrIdOggetto > 0 Then
      If Len(tb!DataArea) Then
        TabIstr.BlkIstr(IndI).Record = tb!DataArea & ""
        TabIstr.BlkIstr(IndI).recordReaddress = findReadd(tb!DataArea, rst!IdOggetto)
      End If
       
      TabIstr.SenSeg = ""
      If tb!idSegmento = -1 Then
        TabIstr.BlkIstr(IndI).segment = tb!VariabileSegmento
        TabIstr.BlkIstr(IndI).idsegm = tb!idSegmento
      Else
        If IsNull(tb!idSegmento) Then
          'virgilio
          'IRIS-DB non serve per istruzioni sul PCB di IO, cio� senza segmento; per ora lo faccio su quelle che ho trovato
          If TabIstr.istr <> "ROLB" Then 'IRIS-DB
            m_fun.WriteLog "Per l'istruzione " & TabIstr.istr & " dell'oggetto " & tb!IdOggetto & " l'idsegmento � Null", " incapsulatore.caricaistruzione "
          End If
          Exit Function
        Else
          Set tb1 = m_fun.Open_Recordset("select * from PsDli_Segmenti where idsegmento = " & tb!idSegmento)
          If Not tb1.EOF Then
            TabIstr.BlkIstr(IndI).segment = tb1!nome
            TabIstr.BlkIstr(IndI).idsegm = tb!idSegmento
          End If
          tb1.Close
          '________________ Ac 24/10/2008 SENSEG
          If IndI = 1 And tb!idSegmento = 0 Then
            ' Mauro 18/11/2009
            'If rst!Istruzione = "GNP" Or rst!Istruzione = "GN" Then
            If rst!Istruzione = "GNP" Or rst!Istruzione = "GN" Or _
               rst!Istruzione = "GHNP" Or rst!Istruzione = "GHN" Then
              ' Mauro 07/01/2009
              'TabIstr.SenSeg = GetSenSeg(TabIstr.psb(1).Id, rst!OrdPcb, TabIstr.DBD(1).Id)
              If UBound(TabIstr.psb) And UBound(TabIstr.DBD) Then
                Dim arCheck() As String, IsTocheck As Boolean
                IsTocheck = True
                ReDim arCheck(0)
                CreaArrayCloniPCB arCheck, rst!IdOggetto, rst!Riga, IsTocheck
                If arCheck(0) = "CLONI" Then
                  'demandiamo la costruzione della TabIstr.SenSeg al CreateDEcode
                Else ' 1 PCB
                  If TabIstr.numpcb <> 0 Then 'IRIS-DB se non ho il PCB prendo tutti i segmenti del DBD (da rivedere poi in fase di verifica manuale)
                    TabIstr.SenSeg = GetSenSeg(TabIstr.psb(1).Id, 0 & TabIstr.numpcb, TabIstr.DBD(1).Id)
                  Else
                    TabIstr.SenSeg = GetSegmentsFromDbd(TabIstr.DBD(1).Id, rst!Istruzione)
                  End If
                  'IRIS-DB: if only one segment, ovverride the fully unqualified with a simple unqualified on that segment
                  If InStr(TabIstr.SenSeg, ",") = 0 Then
                    Set tb1 = m_fun.Open_Recordset("select * from PsDli_Segmenti where IdOggetto = " & TabIstr.DBD(1).Id & " and nome = '" & Trim(TabIstr.SenSeg) & "'")
                    If Not tb1.EOF Then
                      TabIstr.BlkIstr(IndI).segment = tb1!nome
                      TabIstr.BlkIstr(IndI).idsegm = tb1!idSegmento
                      tb!idSegmento = tb1!idSegmento
                      tb!IdSegmentiD = tb1!idSegmento
                      TabIstr.SenSeg = ""
                    End If
                    tb1.Close
                  End If
                End If
              Else 'IRIS-DB: segnala la mancanza del PSB/DBD perch� l'istruzione viene senza senseg
                m_fun.WriteLog "Missing PSB/DBD for program(" & rst!idpgm & ") riga(" & rst!Riga & ")", " incapsulatore.caricaistruzione "
              End If
            End If
          End If
          '____________________________________
          If Not IsNull(tb!IdSegmentiD) Then
            'STEFANO: verificarla meglio
            'If Not tb!idSegmentiD = "" Then
            If Not tb!IdSegmentiD = "" And Not tb!IdSegmentiD = "0" Then
              If InStr(tb!IdSegmentiD, ";") > 0 Then ' se ho molteplicit� seg
                TabIstr.BlkIstr(IndI).nomeVarSeg = tb!VariabileSegmento & ""
              End If
              sSegD = Split(tb!IdSegmentiD, ";")
              For z = 0 To UBound(sSegD)
                Set tb1 = m_fun.Open_Recordset("select * from PsDli_Segmenti where idsegmento = " & CLng(sSegD(z)))
                If Not tb1.EOF Then
                  TabIstr.BlkIstr(IndI).SegmentM = TabIstr.BlkIstr(IndI).SegmentM & tb1!nome & ";"
                Else
                  'SQ
                  AggLog "[IdPgm: " & rst!idpgm & "] - ID Segment: " & sSegD(z) & " not found", MadrdF_Incapsulatore.RTErr
                End If
                tb1.Close
              Next z
              If Len(TabIstr.BlkIstr(IndI).SegmentM) Then
                TabIstr.BlkIstr(IndI).SegmentM = Left(TabIstr.BlkIstr(IndI).SegmentM, Len(TabIstr.BlkIstr(IndI).SegmentM) - 1)
              End If
            Else
              TabIstr.BlkIstr(IndI).SegmentM = ""
            End If
          End If
        End If
      End If
           
      Set rs = m_fun.Open_Recordset("select * from DMDB2_Tabelle where idorigine = " & tb!idSegmento)
      If rs.RecordCount Then
        TabIstr.BlkIstr(IndI).Tavola = rs!nome
      Else
'        Stop
      End If
      rs.Close
      
      'SQ CPY-ISTR
      Set rs = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrDett_Key WHERE " & _
                                "idPgm=" & rst!idpgm & " AND idoggetto=" & rst!IdOggetto & _
                                " AND riga= " & rst!Riga & " AND livello=" & tb!livello & " ORDER BY progressivo")
      If rs.RecordCount Then
        k3 = 0
        While Not rs.EOF
          k3 = k3 + 1
             
          ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
          TabIstr.BlkIstr(IndI).KeyDef(k3).KeyBool = Trim(tb!Qualificazione)
          'TabIstr.BlkIstr(IndI).KeyDef(k3).Op = m_fun.TrattaOp(Tb2!Operatore & "")
          TabIstr.BlkIstr(IndI).KeyDef(k3).op = rs!Operatore & ""
          TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal = Trim(Replace(rs!Valore, ".", "_")) & ""
          TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = Trim(rs!KeyLen)
          TabIstr.BlkIstr(IndI).KeyDef(k3).KeyStart = rs!KeyStart
          '8-05-06
          TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVarField = rs!keyvalore
          TabIstr.BlkIstr(IndI).KeyDef(k3).xName = rs!xName
          TabIstr.BlkIstr(IndI).KeyDef(k3).OpLogic = Trim(rs!booleano)
           
          If rs!idField > 0 Then
            Set tb1 = m_fun.Open_Recordset("select * from PsDLi_Field where idfield = " & Val(rs!idField & " "))
            wXDField = False
          Else
            Set tb1 = m_fun.Open_Recordset("select * from PsDLi_XDField where idXDField = " & Abs(Val(rs!idField)))
            wXDField = True
          End If
            
          If tb1.RecordCount Then
            If Not wXDField Then
              'ginevra: l'ho tolto per il momento: serviva?
              'If Trim(Tb1!ptr_xNome) = "" Then
                TabIstr.BlkIstr(IndI).KeyDef(k3).key = tb1!nome
              'Else
              '  TabIstr.BlkIstr(IndI).KeyDef(k3).key = Tb1!ptr_xNome
              'End If
            Else
              TabIstr.BlkIstr(IndI).KeyDef(k3).key = tb1!nome
            End If
            
            'STEFANO TROVA KEYLEN
            '<NONE>
            If Len(TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen) Then
              If Val(rs!idField) <> 0 Then
                'Recupera la lunghezza del field
                Set TbLenField = m_fun.Open_Recordset("Select * From PsDli_Field Where IdField = " & Val(rs!idField))
                
                If TbLenField.RecordCount Then
                  'IRIS-DB almeno segnalare l'incongruenza visto che � una cosa molto anomala, in DLI darebbe errore grave
                  If TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen <> TbLenField!Lunghezza And Not rst!isExec Then
                    m_fun.WriteLog "Field Length Mismatch Idpgm(" & rst!idpgm & ") Line(" & rst!Riga & ") DBD Field Length(" & TbLenField!Lunghezza & ") SSA Field Length(" & TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen & ")", " incapsulatore.caricaistruzione "
                  End If
                  TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = TbLenField!Lunghezza
                End If
                
                TbLenField.Close
              End If
            End If
          Else
            k3 = k3
          End If
          'Silvia by SQ - 23-05-08
          TabIstr.BlkIstr(IndI).KeyDef(k3).KeyNum = CarNumKey(TabIstr.BlkIstr(IndI), k3)

          rs.MoveNext
        Wend
        rs.Close
      End If
      tb.MoveNext
    Wend
  End If
  CaricaIstruzione = True
  ' Mauro 04/02/2010  + AC 23/02/10  controllo WalMart
  If Not isWALMARTpgm Then
'IRIS-DB not clear why, removed for now...     Unifica_GNP
  End If
End Function

Sub Unifica_GNP()
  Dim level As BlkIstrDliLocal, levelqual As BlkIstrDliLocal
  Dim idField As Long
  
  If TabIstr.istr = "GNP" And UBound(TabIstr.BlkIstr) > 1 Then
    level = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr))
    If UBound(TabIstr.BlkIstr) = 2 Then
      If isRoot(TabIstr.BlkIstr(1).idsegm) Then
        ReDim TabIstr.BlkIstr(1)
        TabIstr.BlkIstr(1) = level
      Else
        idField = MadrdM_Corrector.Restituisci_IdField_DaNome(CStr(TabIstr.BlkIstr(1).idsegm), TabIstr.BlkIstr(1).KeyDef(1).key)
        If Not IsPrimaryKeyLevel(idField) Then
          ReDim TabIstr.BlkIstr(1)
          TabIstr.BlkIstr(1) = level
        End If
      End If
    ElseIf UBound(TabIstr.BlkIstr) = 3 Then
      levelqual = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr) - 1)
      If UBound(levelqual.KeyDef) > 0 Then 'qualificata su secondo
        idField = MadrdM_Corrector.Restituisci_IdField_DaNome(CStr(levelqual.idsegm), levelqual.KeyDef(1).key)
        If IsPrimaryKeyLevel(idField) Then
          ReDim TabIstr.BlkIstr(2)
          TabIstr.BlkIstr(1) = levelqual
          TabIstr.BlkIstr(2) = level
        Else
          ReDim TabIstr.BlkIstr(1)
          TabIstr.BlkIstr(1) = level
        End If
      Else
        ReDim TabIstr.BlkIstr(1)
        TabIstr.BlkIstr(1) = level
      End If
    End If
  End If
End Sub

Function IsPrimaryKeyLevel(pIdField As Long) As Boolean
  Dim rs As Recordset, rs1 As Recordset, rs2 As Recordset, idindex As Long, idtable As Long
  Dim IsFound As Boolean
  
  Set rs = m_fun.Open_Recordset("Select IdIndex,IdTable from DMDB2_index where IdOrigine = " & pIdField & _
                                " and Tipo = 'P'")
  If Not rs.EOF Then
    idindex = rs!idindex
    idtable = rs!idtable
    Set rs1 = m_fun.Open_Recordset("Select IdColonna,IdTable from DMDB2_idxCol where IdIndex = " & idindex & _
                              " and IdTable = " & idtable)
    IsFound = False
    While Not rs1.EOF And Not IsFound
      Set rs2 = m_fun.Open_Recordset("Select IdColonna from DMDB2_Colonne where IdColonna = " & rs1!IdColonna & _
                            " and IdTable = " & rs1!idtable & " And idtable = IdTableJoin")
      If Not rs2.EOF Then
        IsFound = True
        IsPrimaryKeyLevel = True
      End If
      rs2.Close
      rs1.MoveNext
    Wend
    rs1.Close
  End If
  rs.Close
End Function

Function isRoot(pidSeg As Long) As Boolean
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("Select Parent from PsDLI_Segmenti where IdSegmento = " & pidSeg)
  If Not rs.EOF Then
    If rs!parent = "0" Then
      isRoot = True
    End If
  End If
  rs.Close
End Function

Sub CaricaDecod(Cod As String, decod As String, OneTabdecod As DefOperaz)
  Dim K1 As Integer, k2 As Integer
  Dim isbolinkeylevel As Boolean

  OneTabdecod.istr = Replace(GetDecodificaParam(0, "<istr>", decod), ".", "")
  OneTabdecod.Codifica = Cod
  OneTabdecod.nDb = GetDecodificaParam(0, "<dbd>", decod)
  K1 = 1
  ReDim Preserve OneTabdecod.Liv(0)
  While InStr(decod, "<level" & K1) > 0
    ReDim Preserve OneTabdecod.Liv(K1)
    ReDim OneTabdecod.Liv(K1).Chiave(0)
    OneTabdecod.Liv(K1).Segm = GetDecodificaParam(K1, "<segm>", decod)
    If GetDecodificaParam(K1, "<area>", decod) = "AREA" Then
       OneTabdecod.Liv(K1).area = True
    End If
    OneTabdecod.Liv(K1).ccode = GetDecodificaParam(K1, "<ccode>", decod)
    isbolinkeylevel = True
    k2 = 1
    While isbolinkeylevel
      If Not GetDecodificaParam(K1, "<key>", decod, k2) = "" Then
        ReDim Preserve OneTabdecod.Liv(K1).Chiave(k2)
        OneTabdecod.Liv(K1).Chiave(k2).key = GetDecodificaParam(K1, "<key>", decod, k2)
        OneTabdecod.Liv(K1).Chiave(k2).oper = GetDecodificaParam(K1, "<oper>", decod, k2)
        OneTabdecod.Liv(K1).Chiave(k2).Bool = GetDecodificaParam(K1, "<bool>", decod, k2)
        If OneTabdecod.Liv(K1).Chiave(k2).Bool = "" Then
          isbolinkeylevel = False
        Else
          k2 = k2 + 1
        End If
      Else
        isbolinkeylevel = False
      End If
    Wend
    K1 = K1 + 1
  Wend
  If InStr(1, decod, "FDBKEY") > 0 Then
    OneTabdecod.Fdbkey = True
  End If
  OneTabdecod.procSeq = GetDecodificaParam(0, "<procseq>", decod)
  If UCase(OneTabdecod.procSeq) = "NONE" Then
    OneTabdecod.procSeq = ""
  End If
  OneTabdecod.procOpt = GetDecodificaParam(0, "<procopt>", OneTabdecod.Decodifica)
  OneTabdecod.PCBSeq = GetDecodificaParam(0, "<pcbseq>", OneTabdecod.Decodifica)
End Sub

Sub InfoDettLiv(pIdPgm As Long, pIdOggetto As Long, pRiga As Long, maxlevel As Integer, IsSqual As Boolean)
  Dim tb As Recordset
  
  Set tb = m_fun.Open_Recordset("select iif(max(livello) is null, 0, max(livello)) as MaxLivello from " & _
                                "Psdli_IstrDett_Liv  where " & _
                                "idoggetto = " & pIdOggetto & " and idpgm = " & pIdPgm & " and " & _
                                "riga = " & pRiga)
  If Not tb.EOF Then
    maxlevel = tb!MaxLivello
    If maxlevel > 0 Then
      tb.Close
      Set tb = m_fun.Open_Recordset("select QualAndUnqual from " & _
                                    "Psdli_IstrDett_Liv  where " & _
                                    "idoggetto = " & pIdOggetto & " and idpgm = " & pIdPgm & " and " & _
                                    "riga = " & pRiga & " and Livello = " & maxlevel)
      If Not tb.EOF Then
        IsSqual = tb!QualAndUnqual
      End If
    End If
  End If
  tb.Close
End Sub

Function CaricaDecodifiche(rst As Recordset) As Boolean
  Dim tb As Recordset, TabDecSqual() As DefOperaz ' array appo per le squalificate
  Dim i As Integer, h As Integer, maxlevel As Integer, IsSqual As Boolean, indexutil As Integer
  Dim currseg As String
  Dim z As Integer, zdb As Integer, K1 As Integer, k2 As Integer
  
  'Tabdec:  esplosione delle decodifiche, vengono esplicitati i campi

  'MolTabDec: array con schema delle molteplicit�, un elemento per ogni molteplicit� di istruzione
  'e booleani per identificare molteplicit� degli altri elementi
  ReDim TabDec(0)
  Set tb = m_fun.Open_Recordset("select b.decodifica as decodifica,b.nomerout as nomerout, b.codifica as codifica from " & _
                                "Psdli_IstrCodifica as a, MGdli_DecodificaIstr as b where " & _
                                "a.idoggetto = " & rst!IdOggetto & " and a.idpgm = " & rst!idpgm & " and " & _
                                "a.riga = " & rst!Riga & " and a.codifica = b.codifica order by decodifica ")
    
  While Not tb.EOF
    ReDim Preserve TabDec(UBound(TabDec) + 1)
    CaricaDecod tb!Codifica, tb!Decodifica, TabDec(UBound(TabDec))
    TabDec(UBound(TabDec)).Routine = tb!nomeRout
    tb.MoveNext
  Wend
  tb.Close
  
  If UBound(TabDec) = 0 Then
    Exit Function
  End If
  
  ' AC 08/11/10 ________________________________
  InfoDettLiv rst!idpgm, rst!IdOggetto, rst!Riga, maxlevel, IsSqual
  'maxlevel
  If IsSqual Then   ' riordino l'array e metto in fondo le squalificate (And maxlevel > 1)
    i = 1
    'maxlevel potrebbe essere pi� basso nel caso di GNP con il buco
    If UBound(TabDec(1).Liv) < maxlevel Then
      maxlevel = maxlevel - 1
      If UBound(TabDec(1).Liv) < maxlevel Then
        maxlevel = maxlevel - 1
      End If
    End If
    ReDim TabDecSqual(0)
    indexutil = UBound(TabDec)
    While (i < indexutil)
      
      If UBound(TabDec(i).Liv(maxlevel).Chiave) = 0 Then
        ReDim Preserve TabDecSqual(UBound(TabDecSqual) + 1)
        TabDecSqual(UBound(TabDecSqual)) = TabDec(i)
        'scaliamo
        For h = i To indexutil - 1
          TabDec(h) = TabDec(h + 1)
        Next h
        indexutil = indexutil - 1
      Else
        i = i + 1
      End If
    Wend
    h = 1
    For i = (UBound(TabDec) - UBound(TabDecSqual) + 1) To UBound(TabDec)
      TabDec(i) = TabDecSqual(h)
      h = h + 1
    Next i
  End If
  '------------------------
  
  i = 1
  'AC 15/11/2010 --- commento perch� sopra versione migliorata
  
  'Se in testa ho codifiche squalificate le metto in coda
  ' Testo sempre il primno perch� scalo in coda
  
'  If (UBound(TabDec(1).Liv)) > 0 Then
'    While (UBound(TabDec(1).Liv(1).Chiave) = 0 And i <= UBound(TabDec))
'      ReDim Preserve TabDec(UBound(TabDec) + 1)
'      TabDec(UBound(TabDec)) = TabDec(1)
'      For h = 2 To UBound(TabDec)
'        TabDec(h - 1) = TabDec(h)
'      Next h
'      ReDim Preserve TabDec(UBound(TabDec) - 1)
'      i = i + 1
'    Wend
'  End If
  
  ' creare schema delle molteplicit�
  ' stabilisco quali elementi (se ci sono) rimangono costanti a parit� di istruzioni
  ' per questi non introduco variabili nell'incapsulato
  ' il confronto si fa sul primo elemto della struttura che contiene le decodifiche
  ' fino a che l'istruzione non cambia, poi sul primo elemento della istruzione nuova
  ' la molteplicit� � valutata per ogni istruzione differente
  ReDim MoltTabDec(1)
  'Inizializzazione primo elemento array
  ReDim Preserve MoltTabDec(1).BlkIstr(UBound(TabDec(1).Liv))
  For K1 = 1 To UBound(TabDec(1).Liv)
    ReDim Preserve MoltTabDec(1).BlkIstr(K1).MoltKeyDef(UBound(TabDec(1).Liv(K1).Chiave))
  Next K1
  z = 1 'z cambia appena trovo una nuova istruzione
  zdb = 1 ' cambia appena trovo un nuovo db (molteplicit� pcb)
  
  MoltTabDec(1).istr = TabDec(1).istr
  MoltTabDec(1).dbdname = TabDec(1).nDb
  For i = 2 To UBound(TabDec)
    If Not TabDec(i).nDb = TabDec(zdb).nDb Then
      zdb = i
        'Inizializzazione elemento array
        ReDim Preserve MoltTabDec(UBound(MoltTabDec) + 1)
        ReDim Preserve MoltTabDec(UBound(MoltTabDec)).BlkIstr(UBound(TabDec(i).Liv))
        For K1 = 1 To UBound(TabDec(i).Liv)
          ReDim Preserve MoltTabDec(UBound(MoltTabDec)).BlkIstr(K1).MoltKeyDef(UBound(TabDec(i).Liv(K1).Chiave))
        Next K1
        MoltTabDec(UBound(MoltTabDec)).istr = TabDec(i).istr
        MoltTabDec(UBound(MoltTabDec)).dbdname = TabDec(i).nDb
    Else
      If Not TabDec(i).istr = TabDec(z).istr Then
        z = i
        'Inizializzazione elemento array
        ReDim Preserve MoltTabDec(UBound(MoltTabDec) + 1)
        ReDim Preserve MoltTabDec(UBound(MoltTabDec)).BlkIstr(UBound(TabDec(i).Liv))
        For K1 = 1 To UBound(TabDec(i).Liv)
          ReDim Preserve MoltTabDec(UBound(MoltTabDec)).BlkIstr(K1).MoltKeyDef(UBound(TabDec(i).Liv(K1).Chiave))
        Next K1
        MoltTabDec(UBound(MoltTabDec)).istr = TabDec(i).istr
        MoltTabDec(UBound(MoltTabDec)).dbdname = TabDec(i).nDb
        
      Else ' confronto campi
        For K1 = 1 To UBound(TabDec(i).Liv) 'ciclo sui livelli
          'confronto segmento e command code
          If K1 <= UBound(TabDec(z).Liv) Then
            If Not TabDec(i).Liv(K1).Segm = TabDec(z).Liv(K1).Segm Then
              MoltTabDec(UBound(MoltTabDec)).BlkIstr(K1).MoltSegment = True
            End If
            If Not TabDec(i).Liv(K1).ccode = TabDec(z).Liv(K1).ccode Then
              MoltTabDec(UBound(MoltTabDec)).BlkIstr(K1).MoltCodCom = True
            End If
          End If
          For k2 = 1 To UBound(TabDec(i).Liv(K1).Chiave)
            If k2 <= UBound(TabDec(z).Liv(K1).Chiave) Then
              If Not TabDec(i).Liv(K1).Chiave(k2).oper = TabDec(z).Liv(K1).Chiave(k2).oper Then
                MoltTabDec(UBound(MoltTabDec)).BlkIstr(K1).MoltKeyDef(k2).MoltKey = True
              End If
              'If Not (TabDec(i).Liv(k1).Chiave(k2).Bool = "" Or TabDec(z).Liv(k1).Chiave(k2).Bool = "") Then
              If Not TabDec(i).Liv(K1).Chiave(k2).Bool = TabDec(z).Liv(K1).Chiave(k2).Bool Then
                MoltTabDec(UBound(MoltTabDec)).BlkIstr(K1).MoltKeyDef(k2).MoltOpLogic = True
              End If
              'End If
              If Not TabDec(i).Liv(K1).Chiave(k2).key = TabDec(z).Liv(K1).Chiave(k2).key Then
                MoltTabDec(UBound(MoltTabDec)).BlkIstr(K1).MoltKeyDef(k2).MoltKevVal = True
              End If
            End If
          Next k2
        Next K1
      End If
    End If
  Next i
  Dim rifDB As String, rifIstr As String, IsOnlyOneDb As Boolean, IsOnlyOneIstr As Boolean
  IsOnlyOneDb = True
  IsOnlyOneIstr = True
  rifDB = MoltTabDec(1).dbdname
  rifIstr = MoltTabDec(1).istr
  For i = 2 To UBound(MoltTabDec)
   If Not MoltTabDec(i).dbdname = rifDB Then
    IsOnlyOneDb = False
   End If
   If Not MoltTabDec(i).istr = rifIstr Then
    IsOnlyOneIstr = False
   End If
  Next i
  If IsOnlyOneIstr Then
    For i = 1 To UBound(MoltTabDec)
      MoltTabDec(i).istr = ""
    Next i
  End If
  If IsOnlyOneDb Then
    For i = 1 To UBound(MoltTabDec)
      MoltTabDec(i).dbdname = ""
    Next i
  End If
End Function

Function CaricaNomiRoutineInCopy(pIdPgm As Long, pIdOggetto As Long, pRiga As Long, PIstr As String) As Boolean
  Dim tb As Recordset, tb1 As Recordset, rs As Recordset
  Dim IndI As Integer, k As Integer, K1 As Integer, k2 As Integer, k3 As Integer, z As Integer
  Dim u As Long
  Dim wXDField As Boolean
  Dim vEnd As Double, vPos As Double, vPos1 As Double, vStart As Double
  Dim wstr As String
  Dim TbLenField As Recordset
  Dim sDbd() As Long
  Dim TbKey As Recordset
  
  ReDim TabIstr.BlkIstr(0)
  ReDim TabIstr.DBD(0)
  
  'SQ CPY-ISTR
  Set tb = m_fun.Open_Recordset( _
    "SELECT * FROM PsDli_IstrDett_Liv WHERE " & _
    "idPgm=" & pIdPgm & " AND  idoggetto=" & pIdOggetto & " and riga = " & pRiga & " order by livello")
             
  '''''''''''''''DEVE CARICARE PI� DBD e pi�PSB
  sDbd = getDBDByIdOggettoRiga(pIdPgm, pIdOggetto, pRiga)
  
  
  For u = 1 To UBound(sDbd)
    ReDim Preserve TabIstr.DBD(u)
    'TabIstr.dbd(u).Id = sDbd(u)
    TabIstr.DBD(u).nome = Restituisci_NomeOgg_Da_IdOggetto(sDbd(u))
    'TabIstr.dbd(u).tipo = TYPE_DBD
  Next u
  
  If Trim(PIstr) = "QUERY" Then
    TabIstr.istr = "QRY"
  Else
    TabIstr.istr = Trim(PIstr)
  End If
  
  
  If TabIstr.istr = "CHKP" Or TabIstr.istr = "TERM" Or _
     TabIstr.istr = "SYNC" Or TabIstr.istr = "PCB" Or _
     TabIstr.istr = "QRY" Then
   If Not IsAlwaysBatch Then
    SwTp = m_fun.Is_CICS(pIdPgm)
   Else
    SwTp = False
   End If
       
    ' stefano: ma come fa a funzionare che indi non � impostato? Oppure zero va bene come valore?
    If UBound(TabIstr.DBD) > 0 Then
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
        If InStr(TabIstr.DBD(1).nome, "RKKUNL") Then
          TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("RKLUNL", TabIstr.istr, SwTp)
        ElseIf InStr(TabIstr.DBD(1).nome, "RKGIRL") Then
          TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("RKOIRL", TabIstr.istr, SwTp)
        Else
          TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, TabIstr.istr, SwTp)
        End If
      Else
        TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, TabIstr.istr, SwTp)
      End If
    Else
      TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("", TabIstr.istr, SwTp)
    End If
  End If
  
  IndI = 0
  
  While Not tb.EOF
    IndI = IndI + 1
    ReDim Preserve TabIstr.BlkIstr(IndI)
    If Not IsAlwaysBatch Then
      SwTp = m_fun.Is_CICS(tb!idpgm)
    Else
      SwTp = False
    End If
    If UBound(TabIstr.DBD) > 0 Then
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
        If InStr(TabIstr.DBD(1).nome, "RKKUNL") Then
          TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("RKLUNL", TabIstr.istr, SwTp)
        ElseIf InStr(TabIstr.DBD(1).nome, "RKGIRL") Then
          TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("RKOIRL", TabIstr.istr, SwTp)
        Else
          TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, TabIstr.istr, SwTp)
        End If
      Else
        TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, TabIstr.istr, SwTp)
      End If
    Else
      TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("", TabIstr.istr, SwTp)
    End If
    tb.MoveNext
  Wend
  CaricaNomiRoutineInCopy = True
 
End Function

'SQ cos'�?
'METTERE COMMENTI, PER FAVORE
'SQ: non facciamo funzioni se non serve il risultato.
Function CaricaIstruzioneC(rst As Recordset, idpgm As Long) As Boolean
  Dim tb As Recordset, tb1 As Recordset, rs As Recordset, rsPgm As Recordset
  Dim IndI As Integer
  Dim k As Integer, K1 As Integer, k2 As Integer, k3 As Integer, z As Integer
  
  Dim u As Long
  Dim wXDField As Boolean
  
  Dim vEnd As Double, vPos As Double, vPos1 As Double, vStart As Double
  Dim wstr As String
  Dim TbLenField As Recordset
  
  Dim sDbd() As Long
  Dim sPsb() As Long
  Dim sSegD() As String
  
  Dim TbKey As Recordset
  
  ReDim TabIstr.BlkIstr(0)
  ReDim TabIstr.DBD(0)
  ReDim TabIstr.psb(0)
  
  'SQ vediamo (portarlo nel ciclo esterno):
  DoEvents
  
  SwTp = False
  
  'AC 07/08/08
  If Not IsAlwaysBatch Then
    ' Mauro 15/06/2007 : Id del programma chiamante
    'Set tb = m_fun.Open_Recordset("select * from BS_Oggetti where idoggetto = " & rst!IdOggetto)
    Set tb = m_fun.Open_Recordset("select * from BS_Oggetti where idoggetto = " & idpgm)
    If Not tb.EOF Then
       SwTp = tb!CICS
    End If
    tb.Close
  End If
  
  'SQ CPY-ISTR
  Set tb = m_fun.Open_Recordset( _
    "SELECT * FROM PsDli_IstrDett_Liv WHERE " & _
    "idPgm=" & idpgm & " AND idoggetto=" & rst!IdOggetto & " and riga = " & rst!Riga & " order by livello")
             
  '''''''''''''''DEVE CARICARE PI� DBD e pi�PSB
  sDbd = getDBDByIdOggettoRiga(idpgm, rst!IdOggetto, rst!Riga)
  sPsb = getPSBByIdOggettoRiga(idpgm, rst!IdOggetto, rst!Riga)
  
  For u = 1 To UBound(sDbd)
    ReDim Preserve TabIstr.DBD(u)
    TabIstr.DBD(u).Id = sDbd(u)
    TabIstr.DBD(u).nome = Restituisci_NomeOgg_Da_IdOggetto(sDbd(u))
    TabIstr.DBD(u).tipo = TYPE_DBD
  Next u
'per ora � senza vettore
  TabIstr.isGsam = False
  If UBound(TabIstr.DBD) > 0 Then
    Set tb1 = m_fun.Open_Recordset("select * from BS_Oggetti where tipo_DBD = 'GSA' and nome = '" & TabIstr.DBD(1).nome & "'")
    TabIstr.isGsam = Not tb1.EOF
    tb1.Close
  End If
  For u = 1 To UBound(sPsb)
    ReDim Preserve TabIstr.psb(u)
    TabIstr.psb(u).Id = sPsb(u)
    TabIstr.psb(u).nome = Restituisci_NomeOgg_Da_IdOggetto(sPsb(u))
    TabIstr.psb(u).tipo = TYPE_PSB
  Next u
  '**********
  'info dalla tabella PsdLi_istruzioni : occorre la riga con l'idpgm corretto
  Set rsPgm = m_fun.Open_Recordset("Select * from PsDLI_Istruzioni WHERE idPgm=" & idpgm & " AND idoggetto=" & rst!IdOggetto & _
        " AND riga= " & rst!Riga)
  If Not rsPgm.EOF Then
    TabIstr.pcb = rsPgm!numpcb & ""
 
    If Not IsNull(rsPgm!numpcb) Then
      TabIstr.Pcbred = getPCDRedFromPCB(rsPgm!numpcb, rst!IdOggetto)
    End If
    If TabIstr.Pcbred = "" Then
      TabIstr.Pcbred = TabIstr.pcb
    End If
    'SQ
    ' Mauro 16/10/2007
    On Error Resume Next
    TabIstr.numpcb = IIf((0 & rsPgm!ordPcb_MG) > 0, 0 & rsPgm!ordPcb_MG, 0 & rsPgm!ordPcb)
    If err.Number > 0 Then TabIstr.numpcb = 0 & rsPgm!ordPcb
    On Error GoTo 0
    If Trim(rsPgm!Istruzione) = "QUERY" Then
      TabIstr.istr = "QRY"
      TabIstr.istrOriginalMolt = "QRY"
    Else
      TabIstr.istr = Trim(rsPgm!Istruzione)
      TabIstr.istrOriginalMolt = Trim(rsPgm!Istruzione)
    End If
  End If
  rsPgm.Close
  '*****************
  If UBound(TabIstr.psb) Then
     TabIstr.procSeq = Restituisci_ProcSeq(TabIstr.psb(1).Id, TabIstr.numpcb)
     TabIstr.procOpt = Restituisci_ProcOpt(TabIstr.psb(1).Id, TabIstr.numpcb)
  Else
     TabIstr.procSeq = ""
     TabIstr.procOpt = ""
  End If
  
  'ginevra: keyfeedback
  If Trim(rst!keyfeedback) <> "" Then
     TabIstr.keyfeedback = Trim(rst!keyfeedback)
  Else
     TabIstr.keyfeedback = ""
  End If
    
  If TabIstr.istr = "CHKP" Or TabIstr.istr = "TERM" Or _
     TabIstr.istr = "SYNC" Or TabIstr.istr = "PCB" Or _
     TabIstr.istr = "QRY" Then
   
   ' Mauro 15/06/2007 : Id del programma chiamante
   'SwTp = m_fun.Is_CICS(rst!IdOggetto)
   'AC 19/06/07
   'spostato sopra per tutte le istruzioni
   'SwTp = m_fun.Is_CICS(rst!idpgm)
       
    ' stefano: ma come fa a funzionare che indi non � impostato? Oppure zero va bene come valore?
    If UBound(TabIstr.DBD) > 0 Then
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
        If InStr(TabIstr.DBD(1).nome, "RKKUNL") Then
          TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("RKLUNL", TabIstr.istr, SwTp)
        ElseIf InStr(TabIstr.DBD(1).nome, "RKGIRL") Then
          TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("RKOIRL", TabIstr.istr, SwTp)
        Else
          TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, TabIstr.istr, SwTp)
        End If
      Else
        TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, TabIstr.istr, SwTp)
      End If
    Else
      TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("", TabIstr.istr, SwTp)
    End If
  End If
  
  If tb.RecordCount Then
    IndI = 0
    While Not tb.EOF
       IndI = IndI + 1
       ReDim Preserve TabIstr.BlkIstr(IndI)
       k3 = 0
       ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
       
       ' Mauro 15/06/2007 : Id del programma chiamante
       'SwTp = m_fun.Is_CICS(tb!IdOggetto)
       'SwTp = m_fun.Is_CICS(tb!idpgm)
       
       TabIstr.BlkIstr(IndI).ssaName = Trim(tb!NomeSSA & " ")
       ' lunghezza area (impostat solo per i GSAM per ora)
       If Not IsNull(tb!areaLen) And tb!areaLen > 0 Then
          TabIstr.BlkIstr(IndI).areaLen = tb!areaLen
       Else
          TabIstr.BlkIstr(IndI).areaLen = 0
       End If
       '<NONE>
       If Len(TabIstr.BlkIstr(IndI).ssaName) Then
        'stefano: SSA costante
         If InStr(TabIstr.BlkIstr(IndI).ssaName, "'") > 0 Then
           TabIstr.BlkIstr(IndI).SsaLen = Len(TabIstr.BlkIstr(IndI).ssaName) - 2 'toglie gli apici
         Else
           TabIstr.BlkIstr(IndI).SsaLen = m_fun.TrovaLenSSA(TabIstr.BlkIstr(IndI).ssaName, rst!IdOggetto)
         End If
       End If
       
       'SQ tmp...
       TabIstr.BlkIstr(IndI).Parentesi = IIf(tb!Qualificazione, "(", " ")
                     
       TabIstr.BlkIstr(IndI).SegLen = tb!SegLen
       TabIstr.BlkIstr(IndI).Search = IIf(IsNull(tb!condizione), "", tb!condizione)
       While InStr(TabIstr.BlkIstr(IndI).Search, ";;") > 0
        TabIstr.BlkIstr(IndI).Search = Replace(TabIstr.BlkIstr(IndI).Search, ";;", ";")
       Wend

       If UBound(TabIstr.DBD) > 0 Then
         If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
           If InStr(TabIstr.DBD(1).nome, "RKKUNL") Then
             TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("RKLUNL", TabIstr.istr, SwTp)
           ElseIf InStr(TabIstr.DBD(1).nome, "RKGIRL") Then
             TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("RKOIRL", TabIstr.istr, SwTp)
           Else
             TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, TabIstr.istr, SwTp)
           End If
         Else
           TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, TabIstr.istr, SwTp)
         End If
       Else
         TabIstr.BlkIstr(IndI).nomeRout = m_fun.Genera_Nome_Routine("", TabIstr.istr, SwTp)
       End If
       'alpitour: 5/8/2005 forzato diversamente; la gestione della data area andava fatta meglio
       ' o perlomeno completata prima di renderla effettiva;
       ' in questo modo si sono aumentate le mancanze, in quanto la generazione routine
       ' faceva un ragionamento diverso
       'If tb!StrIdOggetto > 0 Then
       If Len(tb!DataArea) Then
         TabIstr.BlkIstr(IndI).Record = tb!DataArea & ""
         TabIstr.BlkIstr(IndI).recordReaddress = findReadd(tb!DataArea, rst!IdOggetto)
       End If
       TabIstr.SenSeg = ""
       If tb!idSegmento = -1 Then
          TabIstr.BlkIstr(IndI).segment = tb!VariabileSegmento
          TabIstr.BlkIstr(IndI).idsegm = tb!idSegmento
       Else
          If IsNull(tb!idSegmento) Then
            'virgilio
            m_fun.WriteLog "Per l'istruzione " & TabIstr.istr & " dell'oggetto " & tb!IdOggetto & " l'idsegmento � Null", " incapsulatore.caricaistruzione "
            Exit Function
          Else
            Set tb1 = m_fun.Open_Recordset("select * from PsDli_Segmenti where idsegmento = " & tb!idSegmento)
            If Not tb1.EOF Then
               TabIstr.BlkIstr(IndI).segment = tb1!nome
               TabIstr.BlkIstr(IndI).idsegm = tb!idSegmento
            End If
            tb1.Close
             '________________ Ac 24/10/2008 SENSEG
            If IndI = 1 And tb!idSegmento = 0 Then
              ' Mauro 18/11/2009
              'If rst!Istruzione = "GNP" Or rst!Istruzione = "GN" Then
              If rst!Istruzione = "GNP" Or rst!Istruzione = "GN" Or _
                 rst!Istruzione = "GHNP" Or rst!Istruzione = "GHN" Then
                ' Mauro 07/01/2009
                'TabIstr.SenSeg = GetSenSeg(TabIstr.psb(1).Id, rst!OrdPcb, TabIstr.DBD(1).Id)
                TabIstr.SenSeg = GetSenSeg(TabIstr.psb(1).Id, 0 & TabIstr.numpcb, TabIstr.DBD(1).Id)
              End If
            End If
          '____________________________________
            If Not IsNull(tb!IdSegmentiD) Then
              'STEFANO: verificarla meglio
              'If Not tb!idSegmentiD = "" Then
              If Not tb!IdSegmentiD = "" And Not tb!IdSegmentiD = "0" Then
                If InStr(tb!IdSegmentiD, ";") > 0 Then
                  TabIstr.BlkIstr(IndI).nomeVarSeg = tb!VariabileSegmento
                End If
                sSegD = Split(tb!IdSegmentiD, ";")
                For z = 0 To UBound(sSegD)
                  Set tb1 = m_fun.Open_Recordset("select * from PsDli_Segmenti where idsegmento = " & CLng(sSegD(z)))
                  If Not tb1.EOF Then
                    TabIstr.BlkIstr(IndI).SegmentM = TabIstr.BlkIstr(IndI).SegmentM & tb1!nome & ";"
                  End If
                  tb1.Close
                Next z
                TabIstr.BlkIstr(IndI).SegmentM = Left(TabIstr.BlkIstr(IndI).SegmentM, Len(TabIstr.BlkIstr(IndI).SegmentM) - 1)
              Else
                TabIstr.BlkIstr(IndI).SegmentM = ""
              End If
            End If
          End If
        End If
      
       
       Set rs = m_fun.Open_Recordset("select * from DMDB2_Tabelle where idorigine = " & tb!idSegmento)
       If rs.RecordCount > 0 Then
          TabIstr.BlkIstr(IndI).Tavola = rs!nome
       Else
 '         Stop
        'SQ stop cosa?! forse c'� qualche problema!? (� giusto che vada secco sul DB2? verificare)
       End If
       rs.Close
      
       'SQ CPY-ISTR
       Set rs = m_fun.Open_Recordset( _
        "SELECT * FROM PsDli_IstrDett_Key WHERE idPgm=" & idpgm & " AND idoggetto=" & rst!IdOggetto & _
        " AND riga= " & rst!Riga & " AND livello=" & tb!livello & " ORDER BY progressivo")
       If rs.RecordCount > 0 Then
          k3 = 0
          While Not rs.EOF
             k3 = k3 + 1
             
             ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyBool = Trim(tb!Qualificazione)
             'TabIstr.BlkIstr(IndI).KeyDef(k3).Op = m_fun.TrattaOp(Tb2!Operatore & "")
             TabIstr.BlkIstr(IndI).KeyDef(k3).op = rs!Operatore & ""
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal = Trim(Replace(rs!Valore, ".", "_")) & ""
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = Trim(rs!KeyLen)
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyStart = rs!KeyStart
             '8-05-06
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVarField = rs!keyvalore
             TabIstr.BlkIstr(IndI).KeyDef(k3).xName = rs!xName
             TabIstr.BlkIstr(IndI).KeyDef(k3).OpLogic = Trim(rs!booleano)
             
             If rs!idField > 0 Then
                Set tb1 = m_fun.Open_Recordset("select * from PsDLi_Field where idfield = " & Val(rs!idField & " "))
                wXDField = False
             Else
                Set tb1 = m_fun.Open_Recordset("select * from PsDLi_XDField where idXDField = " & Abs(Val(rs!idField)))
                wXDField = True
             End If
             
              If tb1.RecordCount > 0 Then
                If Not wXDField Then
                'ginevra: l'ho tolto per il momento: serviva?
                '    If Trim(Tb1!ptr_xNome) = "" Then
                     TabIstr.BlkIstr(IndI).KeyDef(k3).key = tb1!nome
                '    Else
                '       TabIstr.BlkIstr(IndI).KeyDef(k3).key = Tb1!ptr_xNome
                '    End If
                Else
                    TabIstr.BlkIstr(IndI).KeyDef(k3).key = tb1!nome
                End If
                
                'STEFANO TROVA KEYLEN
                '<NONE>
                If Len(TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen) Then
                  If Val(rs!idField) <> 0 Then
                    'Recupera la lunghezza del field
                    Set TbLenField = m_fun.Open_Recordset("Select * From PsDli_Field Where IdField = " & Val(rs!idField))
                    
                    If TbLenField.RecordCount > 0 Then
                        TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = TbLenField!Lunghezza
                    End If
                    
                    TbLenField.Close
                  End If
                End If
             Else
                k3 = k3
             End If
             'Silvia by SQ
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyNum = CarNumKey(TabIstr.BlkIstr(IndI), k3)
    
             rs.MoveNext
          Wend
      End If
      rs.Close
      tb.MoveNext
    Wend
  End If
  
  CaricaIstruzioneC = True
 
End Function

Public Sub infoLineFromPos(RTBsource As RichTextBox, pPos As Long, linea As String, nextstart As Long, indent As Integer)
  Dim pstart As Long, pend As Long, appolinea As String
  
  For pstart = pPos To pPos - 160 Step -1
    RTBsource.SelStart = pstart
    RTBsource.SelLength = 2
    If RTBsource.SelText = vbCrLf Then
      Exit For
    End If
  Next pstart
  ' selezione parte da prima posizione riga successiva
  pend = RTBsource.find(vbCrLf, pstart + 1)
  RTBsource.SelStart = pstart + 1
  RTBsource.SelLength = pend - pstart
  linea = RTBsource.SelText
  nextstart = pend + 2
  appolinea = LTrim(linea)
  'indent = pPos - 4 - RTBsource.SelStart
  indent = Len(linea) - Len(appolinea)
End Sub

Public Sub infoLineprecFromPos(RTBsource As RichTextBox, pPos As Long, lineaprec As String, nextstart As Long, indent As Integer)
  Dim pstart As Long, pend As Long, pos As Long, popencomm As Long, bkuppstart As Long
  Dim lineTmp As String
  Dim isComment As Boolean
        
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  '''''''''' VECCHIA VERSIONE
  For pstart = pPos To pPos - 160 Step -1
    RTBsource.SelStart = pstart
    RTBsource.SelLength = 2
    If RTBsource.SelText = vbCrLf Then
      Exit For
    End If
  Next pstart
    
  'pos = la fine della riga precedente alla EXEC DLI
  pos = pstart - 2
  For pstart = pos To 1 Step -1
    RTBsource.SelStart = pstart
    RTBsource.SelLength = 2
    If RTBsource.SelText = "*/" Then
      isComment = True ' il CR utile va cercato sopra
      Do Until RTBsource.SelText = "/*" Or pstart = 1
        pstart = pstart - 1
        RTBsource.SelStart = pstart
        RTBsource.SelLength = 2
      Loop
      popencomm = pstart
    ElseIf RTBsource.SelText = vbCrLf Then
      If Not isComment Then
        Exit For
      Else
        ' Verifico se sulla riga dove inizia commento c'� qualche istruzione a sinistra
        ' se si il vbCrLf � quello giusto, altrimenti lo salto e cerco il precedente
        bkuppstart = pstart
        RTBsource.SelStart = pstart + 2
        If Not popencomm = pstart Then
            RTBsource.SelLength = popencomm - pstart - 2
            If Not Trim(RTBsource.SelText) = "" Then
                pstart = bkuppstart
                Exit For
            Else
                pstart = bkuppstart
                pstart = pstart - 2
                isComment = False ' proseguo il ciclo for per trovare il CR precedente
            End If
        Else
            pstart = bkuppstart
            pstart = pstart - 2
            isComment = False ' proseguo il ciclo for per trovare il CR precedente
        End If
      End If
    End If
  Next pstart
    
  ' selezione parte da prima posizione riga successiva
  pend = RTBsource.find(vbCrLf, pstart + 1)
  RTBsource.SelStart = pstart + 1
  RTBsource.SelLength = pend - pstart
  lineaprec = RTBsource.SelText
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  '''''''''''''''' NUOVA VERSIONE''''''''''''' DA SISTEMARE '''''''''''''''''''''''
''  For pstart = pPos To pPos - 160 Step -1
''    RTBsource.SelStart = pstart
''    RTBsource.SelLength = 2
''    If RTBsource.SelText = vbCrLf Then
''      Exit For
''    End If
''  Next pstart
''
''  'pos = la fine della riga precedente alla EXEC DLI
''  pos = pstart - 2
''  lineTmp = ""
''  isComment = False
''  Do Until lineTmp <> "" And Not isComment
''    For pstart = pos To 1 Step -1
''      If RTBsource.SelText = vbCrLf Then
''        Exit For
''      End If
''    Next pstart
''    If pos - pstart > 72 Then
''      RTBsource.SelStart = RTBsource.SelStart + 2
''      RTBsource.SelLength = 72
''      lineTmp = RTBsource.SelText
''    Else
''      rtbsource.selstart= rtbsource.selstart +2
''      RTBsource.SelLength = pos - rtbsource.selstart
''    End If
''    If InStr(lineTmp, "*/") Then
''      isComment = True
''    Else
''    End If
''  Loop
''
''  ' selezione parte da prima posizione riga successiva
''  pend = RTBsource.find(vbCrLf, pstart + 1)
''  RTBsource.SelStart = pstart + 1
''  RTBsource.SelLength = pend - pstart
''  lineaprec = RTBsource.SelText
  
End Sub

Public Function findThen(plinea As String) As String
  Dim posTHEN As Integer, posELSE As Integer
  Dim poswhen As Integer, posother As Integer, posIF As Integer
  Dim Parentesi As Integer, criterio As String
  
  posTHEN = InStr(plinea, " THEN ")
  If posTHEN = 0 Then
    posTHEN = InStr(plinea, " THEN")
  End If
  posIF = InStr(plinea, " IF ")
  posELSE = InStr(plinea, " ELSE ")
  If posELSE = 0 Then
    posELSE = InStr(plinea, " ELSE")
  End If
  poswhen = InStr(plinea, " WHEN")
  posother = InStr(plinea, " OTHER ")
  If posTHEN > 0 Then
    If Not posIF > 0 Then
        findThen = Space(posTHEN - 1) & " THEN " & vbCrLf
    Else
        findThen = Left(plinea, posTHEN + 4) & vbCrLf
    End If
    plinea = Space(posTHEN + 5) & Right(plinea, Len(plinea) - posTHEN - 5)
    AddRighe = AddRighe + 1
  ElseIf posELSE > 0 Then
    findThen = Space(posELSE - 1) & " ELSE " & vbCrLf
    plinea = Space(posELSE + 5) & Right(plinea, Len(plinea) - posELSE - 5)
    AddRighe = AddRighe + 1
  ElseIf poswhen > 0 Then
    ' Non mi serve solo la parola WHEN, ma anche tutto il criterio associato
    Parentesi = InStr(poswhen, plinea, ")")
    criterio = Mid(plinea, 1, Parentesi)
    findThen = criterio & " " & vbCrLf
    plinea = Space(Parentesi + 1) & Right(plinea, Len(plinea) - Parentesi - 1)
    AddRighe = AddRighe + 1
  ElseIf posother > 0 Then
    findThen = Space(posother - 1) & " OTHER " & vbCrLf
    AddRighe = AddRighe + 1
  Else
    findThen = ""
  End If
End Function

Public Function findThenDo(plinea As String) As String
  Dim posTHEN As Integer, posELSE As Integer, posdo As Integer, posdotv As Integer
  Dim poswhen As Integer, posother As Integer
  Dim Parentesi As Integer, criterio As String
  
  posTHEN = InStr(plinea, " THEN ")
  If posTHEN = 0 Then
    posTHEN = InStr(plinea, " THEN")
  End If
  posELSE = InStr(plinea, " ELSE ")
  If posELSE = 0 Then
    posELSE = InStr(plinea, " ELSE")
  End If
  poswhen = InStr(plinea, " WHEN")
  posother = InStr(plinea, " OTHER ")
  posdo = InStr(plinea, " DO")
  posdotv = InStr(plinea, ";")
  If posTHEN > 0 And posdo = 0 And posdotv = 0 Then
    findThenDo = Space(posTHEN - 1) & " THEN " & vbCrLf
  ElseIf posELSE > 0 And posdo = 0 And posdotv = 0 Then
    findThenDo = Space(posELSE - 1) & " ELSE " & vbCrLf
  ElseIf poswhen > 0 And posdo = 0 And posdotv = 0 Then
    ' Non mi serve solo la parola WHEN, ma anche tutto il criterio associato
    Parentesi = InStr(poswhen, plinea, ")")
    criterio = Mid(plinea, 1, Parentesi)
    findThenDo = criterio & " " & vbCrLf
  ElseIf posother > 0 And posdo = 0 And posdotv = 0 Then
    findThenDo = Space(posother - 1) & " OTHER " & vbCrLf
  Else
    findThenDo = ""
  End If
End Function

Public Function findComment(plinea As String, Optional oldfc As Integer = 0) As Integer
  Dim startcomm As Integer, endcomm As Integer, startcall As Integer, wordtoreplace As String
  findComment = 0
  opencomment = False
  startcomm = InStr(plinea, "/*")
  endcomm = InStr(plinea, "*/")
  If startcomm > 0 And endcomm > 0 Then
    wordtoreplace = Mid(plinea, startcomm, endcomm - startcomm + 2)
    plinea = Replace(plinea, wordtoreplace, Space(Len(wordtoreplace)))
  ElseIf startcomm > 0 Then
    If Len(plinea) < 73 Then
      opencommentstr = Right(plinea, Len(plinea) - startcomm + 1)
    Else
      opencommentstr = Right(plinea, 72 - startcomm + 1)
    End If
    plinea = Left(plinea, startcomm - 1) & Space(72 - startcomm + 1) & Right(plinea, Len(plinea) - 72)
    opencomment = True
    findComment = -1
  ElseIf endcomm > 0 Then
    plinea = Space(endcomm + 1) & Right(plinea, Len(plinea) - endcomm - 1)
    findComment = 1
  ElseIf oldfc = -1 Then ' commento aperto e non chiuso  ---> riga deve essere sbiancata
    plinea = Space(72) & Right(plinea, Len(plinea) - 72)
  End If
End Function

Public Function AsteriscaIstruzione_PLI(ptotoffset As Long, rst As Recordset, RTBModifica As RichTextBox, typeIncaps As String, isExec As Boolean) As Long
Dim NumRighe As Long, wPosizione As Long, pCount As Long
Dim pstart As Long, pend As Variant, pPos As Long, wIdSegm As Variant
Dim flag As Boolean, wResp As Boolean
Dim tb As Recordset
Dim wVbCrLf As String
Dim oldNomeRout As String, wTipoCall As String, wstr As String
Dim k As Integer, numline As Integer
Dim startkey As Long, offsetendtag As Integer, rsCall As Recordset, windentfor As Integer
Dim linea As String, lineacomm1 As String, lineacomm2 As String, noindent As Integer, i As Integer
Dim selstart1 As Long, selstart2 As Long, sellen1 As Integer, sellen2 As Integer
Dim lendiff As Integer, elsethen As String
Dim appoPos As Long, appoPstart As Long
Dim delcomment As Integer
  If isExec Then
    Set rsCall = m_fun.Open_Recordset("select linee from PsExec where IdOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga)
    wTipoCall = "EXEC"
  Else
    Set rsCall = m_fun.Open_Recordset("select linee from PsCall where IdOggetto = " & rst!IdOggetto & " and riga = " & rst!Riga)
    wTipoCall = "CALL"
  End If
  If Not rsCall.EOF Then
    numline = rsCall!linee
  End If
  'alpitour 5/8/2005: se non valida � inutile fare tutti i giri
  If rst!valida Then
    wResp = CaricaIstruzione(rst)
  End If

  'SQ: tutto sto casino per nomeroutine?
  oldNomeRout = nomeRoutine
  nomeRoutine = ""
  If rst!valida Then
    If UBound(TabIstr.BlkIstr) > 0 Then
      nomeRoutine = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).nomeRout
    End If
  End If
  If nomeRoutine = "" Then
    nomeRoutine = oldNomeRout
  End If
  
  NumRighe = rst!Riga + AddRighe + ptotoffset

  'SQ - ? tmp per le EXEC?
  
  'wresp = TROVA_RIGA(RTBModifica, wTipoCall, IIf(rst!idpgm <> rst!idOggetto, NumRighe - AddRighe, NumRighe), RTBModifica.SelStart)
  If wTipoCall = "EXEC" Then
    wResp = TROVA_RIGA_EXECDLI(RTBModifica, wTipoCall, NumRighe, RTBModifica.SelStart)
  Else
    wResp = TROVA_RIGA(RTBModifica, wTipoCall, NumRighe, RTBModifica.SelStart)
  End If
  If wResp Then
    isPliCallinsideSelect = False
    pPos = RTBModifica.SelStart
    ' asteriscata per reale mutua -- RIPRISTINARE
'   If PliWordIntoSelect(1, Len(RTBModifica), pPos, RTBModifica) Then
'      isPliCallinsideSelect = True
'   End If
    '***********************************************
    Dim lineaprec As String, elsethen1 As String
    infoLineprecFromPos RTBModifica, pPos, lineaprec, pstart, indentkey
    
'    appoPos = pPos
'    appoPstart = pstart
'    '***********************************************
'    'AC 08/10/08
'    ' PEzza funziona se tra then e call c'� una sola riga commentata
'    ' oppure se il commento � a destra o sinistra dello then
'    If InStr(lineaprec, "/*") > 0 And InStr(lineaprec, "*/") > 0 Then
'        If Trim(Left(lineaprec, InStr(lineaprec, "/*") - 1)) = "" And Trim(Right(Left(lineaprec, 72), 72 - InStr(lineaprec, "*/") - 1)) = "" Then
'            pPos = RTBModifica.SelStart
'            infoLineprecFromPos RTBModifica, pPos, lineaprec, pstart, indentkey
'        End If
'    End If
'    pPos = appoPos
'    pstart = appoPstart
    
    infoLineFromPos RTBModifica, pPos, linea, pstart, indentkey
    'opencomment = findComment(linea) valorizzato dentro findComment
    delcomment = findComment(linea)
    elsethen = findThen(linea)
    If Not elsethen = "" Then
      isPliCallinsideSelect = True
    Else
      elsethen1 = findThenDo(lineaprec)
      If Not elsethen1 = "" Then
        isPliCallinsideSelect = True
      End If
      elsethen = findThen(linea)
    End If
    If indentkey < 1 Then indentkey = 2
    lineacomm1 = elsethen & Space(indentkey) & "/* " & IIf(Len(Trim(START_TAG)), START_TAG, "BPHX") & "  START *" & vbCrLf & linea
    If numline = 1 Then
      'lineacomm1 = lineacomm1 & vbCrLf & Space(indentkey) & "* " & IIf(Len(Trim(END_TAG)), END_TAG, START_TAG) & " END    */ "
      lineacomm1 = lineacomm1 & vbCrLf & Space(indentkey) & "* ---------------------------------------*/ "
      selstart1 = RTBModifica.SelStart
      sellen1 = RTBModifica.SelLength
    Else
      selstart1 = RTBModifica.SelStart
      sellen1 = RTBModifica.SelLength
      For i = 2 To numline
        infoLineFromPos RTBModifica, pstart, linea, pstart, noindent
        'opencomment = findComment(linea)
        delcomment = findComment(linea, delcomment)
        If i = numline Then
          'lineacomm2 = linea & vbCrLf & Space(indentkey) & "* " & IIf(Len(Trim(END_TAG)), END_TAG, START_TAG) & " END    */ "
          lineacomm2 = linea & vbCrLf & Space(indentkey) & "* ---------------------------------------*/ "
        Else
          lineacomm2 = linea
        End If
        lendiff = Len(lineacomm2) - RTBModifica.SelLength
        RTBModifica.SelText = lineacomm2
      Next i
    End If
    'a questo punto in pstart dovrei avere la posizione della riga successiva
    'sommero tutto ci� che � stato aggiunto
     
    ' mi riposiziono per sostituire l'asterisco di apertura
    RTBModifica.SelStart = selstart1
    RTBModifica.SelLength = sellen1
    lendiff = lendiff + Len(lineacomm1) - RTBModifica.SelLength
    RTBModifica.SelText = lineacomm1
    'infoLineFromPos RTBModifica, pStart + lendiff, linea, pStart, noindent
    
    AddRighe = AddRighe + 2
     
    AsteriscaIstruzione_PLI = pstart + lendiff
  Else
    AsteriscaIstruzione_PLI = 0
    'SQ 18-04-08
    AggLog "[Object: " & rst!IdOggetto & " - row: " & rst!Riga & "(" & NumRighe & ")] Instruction not found!", MadrdF_Incapsulatore.RTErr
  End If
End Function

Public Function istrIncopy(pIdOggetto As Long) As Boolean
Dim rst As Recordset
  
  Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE " & _
                                 "idPgm =" & pIdOggetto & " AND idOggetto <> " & pIdOggetto)
  If Not rst.EOF Then
    istrIncopy = True
  End If
  rst.Close
End Function

Public Sub InsertMovePgmName(RTBModifica As RichTextBox, namePgm As String)
Dim pos As Long
Dim arrFile() As String, numFile As Integer, Riga As Integer, i As Integer
Dim Index As Integer, bolProc As Boolean, rtbTesto As String
Dim statement As String, token As String

  If InStr(namePgm, ".") Then
    namePgm = Left(namePgm, InStr(namePgm, ".") - 1)
  End If
'  RTBModifica.SaveFile namepgm & "appo", 1

 ' riga = 1
  arrFile = Split(RTBModifica.text, vbCrLf)
'  numfile = FreeFile
  
'  Open namepgm & "appo" For Input As numfile
'  Line Input #numfile, ArrFile(riga)
'  Do Until EOF(numfile)
'    riga = riga + 1
'    ReDim Preserve ArrFile(riga)
'    Line Input #numfile, ArrFile(riga)
'  Loop
'  Close numfile

  bolProc = False
  Index = 1
  Do While (Index <= UBound(arrFile)) And Not bolProc
   If InStr(8, arrFile(Index), "PROCEDURE") And Not Mid(arrFile(Index), 7, 1) = "*" Then
    statement = Right(arrFile(Index), Len(arrFile(Index)) - InStr(8, arrFile(Index), "PROCEDURE") + 1)
    token = MadrdM_GenRout.nextToken(statement)
    token = MadrdM_GenRout.nextToken(statement)
    If token = "DIVISION" Or token = "DIVISION." Then
      While Not InStr(8, arrFile(Index), ".") > 0
          Index = Index + 1
      Wend
      arrFile(Index) = arrFile(Index) & vbCrLf & "BPHX-R     MOVE '" & namePgm & "' TO LNKDB-CALLER."
      bolProc = True
    Else ' proseguo comn riga successiva
      Index = Index + 1
    End If
   Else ' proseguo comn riga successiva
    Index = Index + 1
   End If
  Loop
'  numfile = FreeFile
'  Open namepgm & "appo" For Output As numfile
'  For i = 1 To UBound(ArrFile)
'    Print #numfile, ArrFile(i)
'  Next i
'  Close numfile
'  RTBModifica.LoadFile namepgm & "appo"
'IRIS-DB: questa cazzata consuma il 99% del tempo speso nell'incapsulamento
  rtbTesto = arrFile(0)
  For Index = 1 To UBound(arrFile)
    rtbTesto = rtbTesto & vbCrLf & arrFile(Index)
  Next Index
  RTBModifica.text = rtbTesto
'  Kill namepgm & "appo"
  AddRighe = AddRighe + 1
End Sub

Public Function AsteriscaIstruzione(ptotoffset As Long, rst As Recordset, RTBModifica As RichTextBox, typeIncaps As String) As Long
  Dim NumRighe As Long, wPosizione As Long, pCount As Long
  Dim pstart As Variant, pend As Variant, pPos As Variant, wIdSegm As Variant
  Dim flag As Boolean, wResp As Boolean
  Dim tb As Recordset
  Dim wVbCrLf As String
  Dim oldNomeRout As String, wTipoCall As String, wstr As String
  Dim k As Integer
  Dim startkey As Long, offsetendtag As Integer
  ' Mauro 31/07/2008
  Dim posTHEN As Long, wstart As Long

  'SQ - Portato fuori!
  'If Not rst!to_migrate Then
  '  Exit Function
  'End If

  'alpitour 5/8/2005: se non valida � inutile fare tutti i giri
  If rst!valida Then
    wResp = CaricaIstruzione(rst)
  End If

  'SQ: tutto sto casino per nomeroutine?
  oldNomeRout = nomeRoutine
  nomeRoutine = ""
  If rst!valida Then
    If UBound(TabIstr.BlkIstr) > 0 Then
      nomeRoutine = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).nomeRout
    End If
  End If
  If nomeRoutine = "" Then
    nomeRoutine = oldNomeRout
  End If

  'AC 16/01/07 introdotto offset
  'NumRighe = rst!Riga + AddRighe + IIf(IsNull(rst!OffsetRIgaIncaps), 0, rst!OffsetRIgaIncaps)
  'stefano: 5/7/7 eliminato offset delle macro
  'NumRighe = rst!riga + AddRighe + ptotoffset
  NumRighe = rst!Riga + AddRighe

  'SQ - ? tmp per le EXEC?
  wTipoCall = "CALL"
  'SQ CPY-ISTR (TMP...)
  wResp = TROVA_RIGA(RTBModifica, wTipoCall, NumRighe, RTBModifica.SelStart)
  'wResp = TROVA_RIGA(RTBModifica, wTipoCall, IIf(rst!idPgm <> rst!idOggetto, NumRighe - AddRighe, NumRighe), RTBModifica.SelStart)
  
  If wResp Then
    pPos = RTBModifica.SelStart
    wIdent = 0
    startkey = pPos
    For pstart = pPos To pPos - 160 Step -1
      wIdent = wIdent + 1
      RTBModifica.SelStart = pstart
      RTBModifica.SelLength = 2
      If RTBModifica.SelText = vbCrLf Or pstart = 0 Then
        Exit For
      End If
    Next pstart
    
    ' Mauro 31/07/2008 : THEN EXEC DLI - Separare
    posTHEN = 0
    wstart = pstart
    RTBModifica.SelStart = pstart
    RTBModifica.SelLength = pPos - pstart + 1
    posTHEN = InStr(RTBModifica.SelText, " THEN ")
    If posTHEN = 0 Then
      posTHEN = InStr(RTBModifica.SelText, " ELSE ")
    End If
    If posTHEN > 0 Then
      RTBModifica.SelText = Left(RTBModifica.SelText, posTHEN + 5) & vbCrLf & Space(posTHEN + 3)
      pstart = pstart + posTHEN + 5
      AddRighe = AddRighe + 1
      NumRighe = NumRighe + 1
    End If
    
    wIdent = wIdent - 1
    'stefano: per ora semplifico e vado pi� in l�
    wIdent = wIdent + 6

'    If TypeIncaps = "RDBMS" Then
'      AsteriscaIstruzione = pStart - 1
'      Exit Function
'    End If
      
    If Not pstart = 0 Then
      RTBModifica.SelStart = pstart + 1
    End If
    ' Mauro 31/07/2008
    If posTHEN = 0 Then
      indentkey = startkey - RTBModifica.SelStart
    Else
      indentkey = startkey - wstart - 1
    End If
    pstart = pstart + 2
          
    'SQ portare sotto
    Dim lenline As Integer

    RTBModifica.SelLength = 7

    If rst!valida Then
      Select Case Trim(UCase(TabIstr.istr))
        Case "PCB"
          RTBModifica.SelText = m_fun.LABEL_PCB & "*"
        Case "TERM"
          RTBModifica.SelText = m_fun.LABEL_TERM & "*"
        Case "CHKP"
          RTBModifica.SelText = m_fun.LABEL_CHKP & "*"
        Case "ROLB", "ROLL"
          RTBModifica.SelText = m_fun.LABEL_ROLL & "*"
        Case "QRY"
          RTBModifica.SelText = m_fun.LABEL_QUERY & "*"
        Case "SYNC"
          RTBModifica.SelText = m_fun.LABEL_SYNC & "*"
        Case Else
          'SQ 27-07-06
          'Se START_TAG non valorizzato => rimane l'eventuale originale TAG
          If Len(Trim(START_TAG)) Then
            RTBModifica.SelText = START_TAG & "*"
          Else
            RTBModifica.SelText = Left(RTBModifica.SelText, 6) & "*"
          End If
      End Select
    Else
      'SP 11/9 gestione template ad hoc, per ora non gestisco variabile
      'SP corso
      'If rst!template <> "" Then
      '  RTBModifica.SelText = "BPHX-T*"
      'Else
        RTBModifica.SelText = m_fun.LABEL_NONDECODE & "*"
      'End If
    End If

    pend = RTBModifica.find(vbCrLf, pstart + 1)
    'SQ 27-07-06
    lenline = pend - pstart + 1

    If wTipoCall = " EXEC " Then
      pend = RTBModifica.find("END-EXEC", pPos + 1, , rtfWholeWord)
      pend = RTBModifica.find(vbCrLf, pend + 1)
    Else
      flag = True
'      pEnd = pStart + 1
      pend = RTBModifica.find(vbCrLf, pPos + 1)
      '''''''''''''''''''''''''''''''''''''''''''''''
      'SQ 27-07-06
      ' Gestione TAG a colonna 72
      ' Se non valorizzato => rimane l'eventuale originale TAG
      '''''''''''''''''''''''''''''''''''''''''''''''
      If Len(Trim(END_TAG)) Then
        If lenline >= 72 Then
          RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
          'RTBModifica.SelLength = 8
          RTBModifica.SelLength = (lenline - 72)
          RTBModifica.SelText = END_TAG
          offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
        Else
          RTBModifica.SelLength = 0
          RTBModifica.SelText = Space(72 - lenline) & END_TAG
        End If
      End If

      wstr = Mid(RTBModifica.text, pPos, pend - pPos + 1)
      If InStr(wstr, ".") Then
        flag = False
      End If
      pPos = pend + 1
      While flag
        pend = RTBModifica.find(vbCrLf, pPos + 1)
        wstr = Mid(RTBModifica.text, pPos + 2, pend - pPos - 1)
        'SP 4/9
        Dim savewstr As String
        savewstr = wstr
        ' alpitour: 5/8/2005: mi sembrava di averla gi� fatta questa modifica per la fase 1
        If Len(wstr) > 6 Then
          If Mid(wstr, 7, 1) <> "*" Then
            If Len(wstr) > 11 Then wstr = Trim(Mid(wstr, 12, Len(wstr) - 11))
            ' da rivedere: se trovava punto considerava la stringa chiusa; non va bene
            ' quando c'� l'istruzione senza punto e la riga dopo lo ha, considera la riga successiva
            ' come facente parte dell'istruzione
            'If InStr(wStr, ".") > 0 Then
            '  Flag = False
            'Else
            k = InStr(wstr, " ")
            If k > 0 Then wstr = Trim(Left(wstr, k - 1))
            'SP 27/9: gestione punto dopo parola cobol (praticamente solo END-IF e simili)
            If wstr <> "." Then
              If Right(wstr, 1) = "." Then wstr = Left(wstr, InStr(1, wstr, ".") - 1)
            End If
            If m_fun.SeWordCobol(wstr) And Not wstr = "END-CALL" Then
              flag = False
              pend = pPos - 1
            End If
            'End If
            'SP 4/9
            'If InStr(wstr, ".") > 0 Then
            If InStr(savewstr, ".") > 0 Then
              flag = False
            End If
          End If
        End If
        pPos = pend + 1
      Wend
    End If

    wPosizione = pend + 1
    pend = pend - 2
    pPos = pstart
    pPos = pstart + 1
    flag = True
    While flag
      pPos = RTBModifica.find(vbCrLf, pPos + 1, pend - 1)
      If pPos = -1 Then
        flag = False
      End If

      If flag Then
        RTBModifica.SelStart = pPos + 2
        RTBModifica.SelLength = 7

'        If Trim(RTBModifica.SelText) = vbCrLf Then
        ' controllo per righe "quasi" vuote
        If InStr(RTBModifica.SelText, vbCrLf) Then
          wVbCrLf = vbCrLf & Space(5)
          pCount = pCount + 5
        Else
          wVbCrLf = ""
        End If

        'If rst!Valida = True Then
        ' ginevra: se gi� asteriscato, non lo cambia
        If Mid(RTBModifica.SelText, 7, 1) <> "*" Then
          'alpitour 5/8/2005: se non valida � inutile fare tutti i giri
          If rst!valida Then
            Select Case Trim(UCase(TabIstr.istr))
              Case "PCB"
                RTBModifica.SelText = m_fun.LABEL_PCB & "*" & wVbCrLf
              Case "TERM"
                RTBModifica.SelText = m_fun.LABEL_TERM & "*" & wVbCrLf
              Case "CHKP"
                RTBModifica.SelText = m_fun.LABEL_CHKP & "*" & wVbCrLf
              Case "QRY"
                RTBModifica.SelText = m_fun.LABEL_QUERY & "*" & wVbCrLf
              Case "SYNC"
                RTBModifica.SelText = m_fun.LABEL_SYNC & "*" & wVbCrLf
              Case Else
                'SQ 27-07-06
                'Se START_TAG non valorizzato => rimane l'eventuale originale TAG
                If Len(Trim(START_TAG)) Then
                  RTBModifica.SelText = START_TAG & "*" & wVbCrLf
                Else
                  RTBModifica.SelText = Left(RTBModifica.SelText, 6) & "*" & wVbCrLf
                End If
            End Select
            '''''''''''''''''''''''''''''''''''''''''''''''
            'SQ 27-07-06
            ' Gestione TAG a colonna 72
            '''''''''''''''''''''''''''''''''''''''''''''''
            If Len(Trim(END_TAG)) Then
              lenline = RTBModifica.find(vbCrLf, pPos + 1) - pPos - 2
              If lenline >= 72 Then
                RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
                RTBModifica.SelLength = lenline - 72
                RTBModifica.SelText = END_TAG
                offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
              Else
                RTBModifica.SelLength = 0
                RTBModifica.SelText = Space(72 - lenline) & END_TAG
              End If
            End If
          Else
            RTBModifica.SelText = m_fun.LABEL_NONDECODE & "*" & wVbCrLf
          End If
        End If
      End If
    Wend
    If offsetendtag > 2 Then
      offsetendtag = offsetendtag - 2
    End If
    AsteriscaIstruzione = wPosizione + pCount + offsetendtag
  Else
    AsteriscaIstruzione = 0

    AggLog "[Object: " & rst!IdOggetto & " - row: " & rst!Riga & "(" & NumRighe & ")] Instruction not found!", MadrdF_Incapsulatore.RTErr
  End If
End Function

Public Function AsteriscaIstruzione_EZT(ptotoffset As Long, rst As Recordset, RTBModifica As RichTextBox, typeIncaps As String) As Long
  Dim NumRighe As Long, wPosizione As Long, pCount As Long
  Dim pstart As Variant, pend As Variant, pPos As Variant, wIdSegm As Variant
  Dim flag As Boolean, wResp As Boolean
  Dim tb As Recordset
  Dim wVbCrLf As String
  Dim oldNomeRout As String, wTipoCall() As String, wstr As String
  Dim k As Integer, i As Integer
  Dim startkey As Long, offsetendtag As Integer
  Dim Percorso As String
  Dim finalStringTmpl As String
  Dim wPath As String
  Dim Rtb  As RichTextBox
  Dim vectorRighe() As String
  Dim Righe As Integer
  
  On Error GoTo ErrH

  'alpitour 5/8/2005: se non valida � inutile fare tutti i giri
  If rst!valida Then
    wResp = CaricaIstruzione(rst)
  End If

  'SQ: tutto sto casino per nomeroutine?
  oldNomeRout = nomeRoutine
  nomeRoutine = ""
  If rst!valida Then
    If UBound(TabIstr.BlkIstr) > 0 Then
      nomeRoutine = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).nomeRout
    End If
  End If
  If nomeRoutine = "" Then
    nomeRoutine = oldNomeRout
  End If

  'AC 16/01/07 introdotto offset
  'NumRighe = rst!Riga + AddRighe + IIf(IsNull(rst!OffsetRIgaIncaps), 0, rst!OffsetRIgaIncaps)
  NumRighe = rst!Riga + AddRighe + ptotoffset

  'tipo di chiamata
  ReDim wTipoCall(1)
  wTipoCall(0) = "DLI"
  wTipoCall(1) = "RETRIEVE"
  
  'trova il numero di riga da asteriscare
  For i = 0 To UBound(wTipoCall)
    wResp = TROVA_RIGA(RTBModifica, wTipoCall(i), IIf(rst!idpgm <> rst!IdOggetto, NumRighe - AddRighe, NumRighe), RTBModifica.SelStart)
    If wResp Then Exit For
  Next i
  
  If wResp Then
     pPos = RTBModifica.SelStart
     wIdent = 0
     startkey = pPos
     For pstart = pPos To pPos - 160 Step -1
       wIdent = wIdent + 1
       RTBModifica.SelStart = pstart
       RTBModifica.SelLength = 2
       If RTBModifica.SelText = vbCrLf Then
         Exit For
       End If
     Next pstart

     wIdent = wIdent - 1
     'stefano: per ora semplifico e vado pi� in l�
     wIdent = wIdent + 6
     
     RTBModifica.SelStart = pstart + 1
     indentkey = startkey - RTBModifica.SelStart
     pstart = pstart + 2
     'SQ portare sotto
     Dim lenline As Integer

     RTBModifica.SelLength = 1

    If TabIstr.istr = "CHKP" Or TabIstr.istr = "XRST" Or TabIstr.istr = "FOR" Or rst!obsolete = True Then
'stefano: bisogna mettere nei parametri anche un TAG per le NON decodificate, in modo da ritrovarel
        RTBModifica.SelText = "*" & "ITER-N" & " " & RTBModifica.SelText
    Else
        RTBModifica.SelText = "*" & START_TAG & " " & RTBModifica.SelText
    End If

    pend = RTBModifica.find(vbCrLf, pstart + 1)
    lenline = pend - pstart + 1

      flag = True
'       pEnd = pStart + 1
'gi� fatto
      'pEnd = RTBModifica.Find(vbCrLf, pPos + 1)
      '''''''''''''''''''''''''''''''''''''''''''''''
      'SQ 27-07-06
      ' Gestione TAG a colonna 72
      ' Se non valorizzato => rimane l'eventuale originale TAG
      '''''''''''''''''''''''''''''''''''''''''''''''
      If Len(Trim(END_TAG)) Then
        If lenline >= 72 Then
          RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
          'RTBModifica.SelLength = 8
          RTBModifica.SelLength = (lenline - 72)
          RTBModifica.SelText = END_TAG
          offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
        Else
         RTBModifica.SelLength = 0
         RTBModifica.SelText = Space(72 - lenline) & END_TAG
        End If
      End If

      wstr = Mid(RTBModifica.text, pPos, pend - pPos + 1)
      If InStr(wstr, " +") > 0 Then
        flag = True
'stefano: PER ORA NON GESTISCO PI� DI DUE RIGHE, ALTRIMENTI BISOGNA USARE LA PSCALL
        Dim pend2 As Long
        While flag
          pend2 = RTBModifica.find(vbCrLf, pend + 2)
          wstr = Left(Trim(Mid(RTBModifica.text, pend + 3, pend2 + 1 - pend - 3)), 1)
'funziona solo con "*" a colonna 1
          If wstr <> "*" Then
             RTBModifica.SelStart = pend + 2
             RTBModifica.SelLength = 1
    'per ora rifaccio solo questa
             If TabIstr.istr = "CHKP" Or TabIstr.istr = "XRST" Or TabIstr.istr = "FOR" Or rst!obsolete = True Then
                RTBModifica.SelText = "*" & "ITER-N" & " " & RTBModifica.SelText
             Else
                RTBModifica.SelText = "*" & START_TAG & " " & RTBModifica.SelText
             End If
             'aggiunge il tag
'             pEnd = pend2 + 8
          Else
'             pEnd = pend2
          End If
          pend2 = RTBModifica.find(vbCrLf, pend + 2)
'          wstr = Left(Trim(Mid(RTBModifica.text, pEnd + 3, pend2 + 1)), 1)
          'chiude il ciclo' NON GESTITO, per ora mi fermo a due righe
          'If Trim(wstr) <> "*" And InStr(Mid(RTBModifica.text, pEnd + 3, pend2 + 1 - pEnd - 3), "+") = 0 Then flag = False
          pend = pend2
          flag = False
        Wend
      End If
      pPos = pend + 1
      ' ma perch� non usare il numero di righe inserito nella pscall? Sta l� apposta
'      While flag
'         pEnd = RTBModifica.Find(vbCrLf, pPos + 1)
'         wstr = Mid(RTBModifica.text, pPos + 2, pEnd - pPos - 1)
'         'SP 4/9
'         Dim savewstr As String
'         savewstr = wstr
'         'stefano: ERA TUTTA IN COBOL!!!!!!!
'         ' alpitour: 5/8/2005: mi sembrava di averla gi� fatta questa modifica per la fase 1
'          If Len(wstr) > 6 Then
'             If Mid(wstr, 7, 1) <> "*" Then
'              If Len(wstr) > 11 Then wstr = Trim(Mid(wstr, 12, Len(wstr) - 11))
'              K = InStr(wstr, " ")
'              If K > 0 Then wstr = Trim(Left(wstr, K - 1))
'              'SP 27/9: gestione punto dopo parola cobol (praticamente solo END-IF e simili)
'              If wstr <> "." Then
'                 If Right(wstr, 1) = "." Then wstr = Left(wstr, InStr(1, wstr, ".") - 1)
'              End If
'              If m_fun.SeWordCobol(wstr) And Not wstr = "END-CALL" Then
'                flag = False
'                pEnd = pPos - 1
'              End If
'              If InStr(savewstr, ".") > 0 Then
'                flag = False
'              End If
'            End If
'          End If
'          pPos = pEnd + 1
'       Wend

     wPosizione = pend + 1
     pend = pend - 2
     pPos = pstart
     pPos = pstart + 1
     flag = True
     
'       While flag
'         pPos = RTBModifica.Find(vbCrLf, pPos + 1, pEnd - 1)
'         If pPos = -1 Then
'           flag = False
'         End If
'
'         If flag Then
'           RTBModifica.SelStart = pPos + 2
'           RTBModifica.SelLength = 1
'
'  '         If Trim(RTBModifica.SelText) = vbCrLf Then
'           ' controllo per righe "quasi" vuote
'           If InStr(RTBModifica.SelText, vbCrLf) Then
'              wVbCrLf = vbCrLf & Space(5)
'              pCount = pCount + 5
'           Else
'              wVbCrLf = ""
'           End If
'
'           'If rst!Valida = True Then
'           'stefano: che c'entrava (colonna 7) con l'EZT??? Quante cose nuove gi� da buttar via!!!!!!
'           If Left(Trim(RTBModifica.SelText), 1) <> "*" Then
'             If rst!valida Then
'                If Len(Trim(START_TAG)) Then
'                  RTBModifica.SelText = "*" & START_TAG & wVbCrLf
'                Else
'                  'RTBModifica.SelText = Left(RTBModifica.SelText, 6) & "*" & wVbCrLf
'                End If
'              '''''''''''''''''''''''''''''''''''''''''''''''
'              'SQ 27-07-06
'              ' Gestione TAG a colonna 72
'              '''''''''''''''''''''''''''''''''''''''''''''''
'              If Len(Trim(END_TAG)) Then
'                lenline = RTBModifica.Find(vbCrLf, pPos + 1) - pPos - 2
'                If lenline >= 72 Then
'                  RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
'                  RTBModifica.SelLength = lenline - 72
'                  RTBModifica.SelText = END_TAG
'                  offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
'                Else
'                  RTBModifica.SelLength = 0
'                  RTBModifica.SelText = Space(72 - lenline) & END_TAG
'                End If
'              End If
'            Else
'               RTBModifica.SelText = "*" & m_fun.LABEL_NONDECODE & wVbCrLf
'            End If
'          End If
'        End If
'      Wend
    If offsetendtag > 2 Then
      offsetendtag = offsetendtag - 2
    End If
    
    If TabIstr.istr = "CHKP" Or TabIstr.istr = "XRST" Or TabIstr.istr = "FOR" Or rst!obsolete = True Then
       AsteriscaIstruzione_EZT = wPosizione + pCount + offsetendtag
       Exit Function
    End If
       
    
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''
''' provare ad inserire qui il blocco del template'
''' e aggiungere l'offset correttamente '''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    wPath = m_fun.FnPathPrj
    Set Rtb = MadrdF_Incapsulatore.RTBR
    
    Percorso = wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\EZT\COMMONINIT_ETZ"
    Rtb.LoadFile Percorso
  
    finalStringTmpl = vbCrLf & Rtb.text
    
    'nome programma
    finalStringTmpl = Replace(finalStringTmpl, "<NAMEPGM>", "'" & Trim(NomeProg) & "'")
    
    'nome routine
    finalStringTmpl = Replace(finalStringTmpl, "<ROUTNAME>", nomeRoutine)
    
    'operatore
    ' Mauro 16/10/2007
    On Error Resume Next
    finalStringTmpl = Replace(finalStringTmpl, "<OPERAT>", "'" & getIstruzione_ETZ(TabIstr.istr, 1, rst!idpgm, rst!IdOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)) & "'")
    If err.Number > 0 Then
      finalStringTmpl = Replace(finalStringTmpl, "<OPERAT>", "'" & getIstruzione_ETZ(TabIstr.istr, 1, rst!idpgm, rst!IdOggetto, rst!Riga, 0 & rst!ordPcb) & "'")
    End If
    On Error GoTo ErrH
    
    'numero pcb
    finalStringTmpl = Replace(finalStringTmpl, "<NPCB>", TabIstr.numpcb)
    
'''cicla tante volte quante sono le SSA
    Dim jxz As Integer
    Dim sLevel As Integer
    Dim posSx As Integer
    Dim posDx As Integer
    Dim aggiunta As String
    For jxz = 1 To UBound(TabIstr.BlkIstr)
      'livello del segmento     <FLEVEL>
      segLev = 0
      getSegLevel (CLng(TabIstr.BlkIstr(jxz).idsegm))
      If jxz = UBound(TabIstr.BlkIstr) Then
        If TabIstr.BlkIstr(jxz).Record <> "" Then
           finalStringTmpl = Replace(finalStringTmpl, "<LEVELMAX>", jxz)
           finalStringTmpl = Replace(finalStringTmpl, "<DATAAREA>", TabIstr.BlkIstr(jxz).Record)
        End If
      End If
      
'''                     jxz         numero di cond. su SSA
'      Dim Stringa() As String
      Dim zzz As Integer
'      Stringa = getLevKey(rst!idOggetto, TabIstr.BlkIstr(jxz).ssaName)
'      If UBound(Stringa) > 0 Then
'        For zzz = 1 To UBound(TabIstr.BlkIstr(jxz).KeyDef)
'          finalStringTmpl = Replace(finalStringTmpl, "<LEVKEY>", Stringa(zzz))
'          ' lo rif� per i cicli successivi
'          finalStringTmpl = Replace(finalStringTmpl, "<INDICE-FLEVEL>", jxz)
'          finalStringTmpl = Replace(finalStringTmpl, "<INDICE-FLEVEL-R>", zzz)
'          If UBound(Stringa) > 1 And zzz <> UBound(Stringa) Then
'            aggiunta = vbCrLf & "LNKDB-KEYVALUE<INDICE-FLEVEL>, <INDICE-FLEVEL-R> =  <LEVKEY> "
'            posSx = InStr(1, finalStringTmpl, Stringa(zzz) & vbCrLf) + Len(Stringa(zzz))
'            finalStringTmpl = Mid(finalStringTmpl, 1, posSx) & aggiunta & Mid(finalStringTmpl, posSx, Len(finalStringTmpl))
'          End If
'        Next
'      Else
'        'squalificata
'        finalStringTmpl = Mid(finalStringTmpl, 1, InStr(1, finalStringTmpl, "LNKDB-KEYVALUE") - 1) & vbCrLf & Mid(finalStringTmpl, InStr(1, finalStringTmpl, "<FLEVELSA>") + 10)
'      End If
Dim Cont As Integer
      If UBound(TabIstr.BlkIstr(jxz).KeyDef) > 0 Then
        For zzz = 1 To UBound(TabIstr.BlkIstr(jxz).KeyDef)
          Cont = Cont + 1
          finalStringTmpl = Replace(finalStringTmpl, "<LEVKEY" & jxz & "-" & zzz & ">", "REVSSAKEYVAL<LEVEL" & jxz & "-" & zzz & ">")
          finalStringTmpl = Replace(finalStringTmpl, "<LEVEL" & jxz & "-" & zzz & ">", jxz)
          finalStringTmpl = Replace(finalStringTmpl, "<LEVEL" & jxz & "-" & zzz & "R>", zzz)
          finalStringTmpl = Replace(finalStringTmpl, "<POSKEY" & jxz & "-" & zzz & ">", TabIstr.BlkIstr(jxz).KeyDef(zzz).KeyStart - 1)
          finalStringTmpl = Replace(finalStringTmpl, "<LENKEY" & jxz & "-" & zzz & ">", TabIstr.BlkIstr(jxz).KeyDef(zzz).KeyLen)
'stefano: visto che non mi piaceva proprio e NON funzionava prendendo il posx un po' a caso.
'  ho fatto un template bruttissimo, ma esterno, che dovr� essere sostituito con un template vettoriale
'  a cura di Alberto
'          If UBound(TabIstr.BlkIstr(jxz).KeyDef) > zzz Then
'          ' lo rif� per i cicli successivi
'            aggiunta = "LNKDB-KEYVALUE<INDICE-FLEVEL><INDICE-FLEVEL-R> = <LEVKEY> " & vbCrLf
'            posSx = InStr(1, finalStringTmpl, Replace(" = REVSSAKEYVAL<NUMKEY>", "<NUMKEY>", Cont)) + 17
'            finalStringTmpl = Mid(finalStringTmpl, 1, posSx) & aggiunta & Mid(finalStringTmpl, posSx, Len(finalStringTmpl))
'            aggiunta = vbCrLf & "DEFINE REVSSAKEYVAL<NUMKEY> REVSSA +<POSKEY> <LENKEY> A"
'            'stefano: forzatissimo, ma non � colpa mia, toglie due invii
'            posSx = InStr(1, finalStringTmpl, "REVSSA =") - 5
'            finalStringTmpl = Mid(finalStringTmpl, 1, posSx) & aggiunta & Mid(finalStringTmpl, posSx, Len(finalStringTmpl))
'          End If
        Next
      Else
        'squalificata
        'finalStringTmpl = Mid(finalStringTmpl, 1, InStr(1, finalStringTmpl, "LNKDB-KEYVALUE") - 1) & vbCrLf & Mid(finalStringTmpl, InStr(1, finalStringTmpl, "<FLEVELSA>") + 10)
      End If
      
      
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

      'nome ssa     <FLEVELSA>
      Dim tokenStat As String
      Dim s As String
      'stefano: ma il parser che ci sta a fare?
'      s = rst!statement
'      tokenStat = nextToken(s)
'      tokenStat = nextToken(s)
      If segLev > 0 Then
        finalStringTmpl = Replace(finalStringTmpl, "<SEGLEVEL" & jxz & ">", "'" & Right("00" & segLev, 2) & "'")
      End If
      finalStringTmpl = Replace(finalStringTmpl, "<LEVEL" & jxz & ">", jxz)
      If TabIstr.BlkIstr(jxz).ssaName <> "" Then
        finalStringTmpl = Replace(finalStringTmpl, "<SSA" & jxz & ">", TabIstr.BlkIstr(jxz).ssaName)
      End If
      'gi� fatto sopra (ora � indice FLEVEL)
      'finalStringTmpl = Replace(finalStringTmpl, "<NUMERO-SSA>", UBound(TabIstr.BlkIstr))
      'finalStringTmpl = Replace(finalStringTmpl, "<INDICE-SSA>", jxz)
      
      'se ci sono + SSA riscrive la parte del template
      'stefano: � inutile avere un template esterno se poi si ricostruisce da dentro,
      ' appena possibile buttiamo via sta routine e usiamo quella standard
'      aggiunta = ""
'      posSx = 0
'      If UBound(TabIstr.BlkIstr) > 1 And jxz <> UBound(TabIstr.BlkIstr) Then
'        aggiunta = vbCrLf & "LNKDB-SEGLEVEL<INDICE-FLEVEL>    =  <FLEVEL> "
'        aggiunta = aggiunta & vbCrLf & "LNKDB-KEYVALUE<INDICE-FLEVEL><INDICE-FLEVEL-R> =  <LEVKEY> "
'        aggiunta = aggiunta & vbCrLf & "LNKDB-SSA-AREA<INDICE-FLEVEL>    =  <FLEVELSA>" & vbCrLf
'
'        posSx = InStr(1, finalStringTmpl, TabIstr.BlkIstr(jxz).ssaName & vbCrLf) + 6
'        finalStringTmpl = Mid(finalStringTmpl, 1, posSx) & aggiunta & Mid(finalStringTmpl, posSx, Len(finalStringTmpl))
'        finalStringTmpl = Mid(finalStringTmpl, 1, posSx) & aggiunta & Mid(finalStringTmpl, posSx, Len(finalStringTmpl))
'      End If
      
    Next
    
    'status code
    finalStringTmpl = Replace(finalStringTmpl, "<PCBSTCD>", getStatusCode(rst!IdOggetto, TabIstr.DBD(1).nome))
    
    vectorRighe = Split(finalStringTmpl, vbCrLf)
    'toglie le righe non sostituite
    k = 0
    finalStringTmpl = ""
    For i = 0 To UBound(vectorRighe)
     If Not InStr(vectorRighe(i), "<") > 0 Then
        k = k + 1
        finalStringTmpl = finalStringTmpl & vectorRighe(i) & vbCrLf
     End If
    Next
      
    Righe = Len(finalStringTmpl)
'toglie 1 perch� sovrascrive un carriage return, per cui un riga sull'originale viene mangiata (perch� poi???)
    AddRighe = AddRighe + k - 1
    
    RTBModifica.SelText = finalStringTmpl
    
    AsteriscaIstruzione_EZT = wPosizione + pCount + offsetendtag + Righe
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''' fine '''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

  Else
    AsteriscaIstruzione_EZT = 0
    Debug.Print "Instruction not found: " & rst!Istruzione & " at line --> " & NumRighe
  End If
  
  Exit Function
ErrH:
  MsgBox err.Description
  Resume Next
End Function

Private Function getStatusCode(IdOggetto As Long, DBD As String)
  Dim rs As Recordset
  
  On Error GoTo ErrH
  Set rs = m_fun.Open_Recordset("Select Status_Code from PSEZ_FILE where " & _
                                "IdOggetto =" & IdOggetto & " and dbd = '" & DBD & "'")
  If Not rs.EOF Then
    getStatusCode = rs!Status_Code
  Else
    getStatusCode = ""
  End If
  rs.Close
  Exit Function
ErrH:
  MsgBox err.Description & vbCrLf & _
         "[MadrdM_Incapsulatore.getStatusCode]"
  getStatusCode = ""
End Function

Sub INSERT_NODECOD(wPos As Long, RTBModifica As RichTextBox, rst As Recordset)
  Dim Testo As String
    
  ' istruzioni con template fisso
  'If rst!template <> "" And rst!template <> "OBSO" Then
  If Len(rst!template) Then
    Dim testo1 As String, testo2 As String, testo3 As String
    Dim i As Long, j As Long, k As Long
    testo1 = ""
    testo2 = ""
    testo3 = ""
    testo1 = RTBModifica.text
    RTBModifica.LoadFile m_fun.FnPathPrj & "\input-prj\istruzioni\" & rst!template
    testo3 = RTBModifica.text
    i = RTBModifica.find("#WS#", 0)
    If i > -1 Then
      i = RTBModifica.find(vbCrLf, i)
      j = RTBModifica.find("#END-WS#", i + 2)
      RTBModifica.SelStart = i + 2
      RTBModifica.SelLength = j - i - 3
      testo2 = RTBModifica.SelText
      i = RTBModifica.find(vbCrLf, j)
      j = Len(RTBModifica.text)
      RTBModifica.SelStart = i + 2
      RTBModifica.SelLength = j - i + 3
      testo3 = RTBModifica.SelText
      RTBModifica.text = testo1
      i = RTBModifica.find(" WORKING-STORAGE ", 1)
      If i > 0 Then
        i = RTBModifica.find(vbCrLf, i)
        RTBModifica.SelStart = i + 2
        RTBModifica.SelLength = 0
        RTBModifica.SelText = testo2
        wPos = wPos + Len(testo2)
      End If
    Else
      RTBModifica.text = testo1
    End If
    testo1 = RTBModifica.text
    RTBModifica.text = testo2
    j = 0
    i = 999
    k = 0
    While i <> -1
      i = RTBModifica.find(vbCrLf, j)
      If i <> -1 Then
        k = k + 1
      End If
      j = i + 2
    Wend
    AddRighe = AddRighe + k
    RTBModifica.text = testo3
    j = 0
    i = 999
    k = 0
    While i <> -1
      i = RTBModifica.find(vbCrLf, j)
      If i <> -1 Then
        k = k + 1
      End If
      j = i + 2
    Wend
    AddRighe = AddRighe + k - 1
    Testo = vbCrLf & testo3
    RTBModifica.text = testo1
  Else
    'SP segmenti e DBD obsoleti
    'If rst!template = "OBSO" Then
    If rst!obsolete Then
      Testo = vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
      Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "*   OBSOLETE INSTRUCTION"
      Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
      'SQ 28-07-06 - Per le istruzioni sconosciute
      'If rst!Istruzione = "ISRT" Or rst!Istruzione = "DLET" Or rst!Istruzione = "REPL" Then
      'IRIS-DB: removing the forced abend
'''''      If rst!Istruzione = "ISRT" Or rst!Istruzione = "DLET" Or rst!Istruzione = "REPL" Or Len(rst!Istruzione & "") = 0 Then
'''''        If SwTp Then
'''''          Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "     EXEC CICS ABEND ABCODE('OBSO') END-EXEC"
'''''        Else
'''''          Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "     CALL 'OBSO'"
'''''        End If
'''''      Else
        'Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "     MOVE 'XX' TO " & Trim(rst!numpcb) & "(11:2)" & IIf(wPunto, ".", "") 'IRIS-DB changed from 'GE' as possible for any instruction
        Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "     MOVE 'GE' TO " & Trim(rst!numpcb) & "(11:2)" & IIf(wPunto, ".", "") 'IRIS-DB changed back to 'GE' as requested from customer
'''''      End If
      AddRighe = AddRighe + 4
    Else
      If TipoFile = "PLI" Or TipoFile = "INC" Then
        Testo = vbCrLf & " /* " & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------*/"
        Testo = Testo & vbCrLf & " /* " & m_fun.LABEL_NONDECODE & "    WARNING: NOT SOLVED INSTRUCTION                       */"
        Testo = Testo & vbCrLf & " /* " & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------*/"
      Else
        Testo = vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
        Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "*   WARNING: NOT SOLVED INSTRUCTION"
        Testo = Testo & vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
      End If
      Testo = Testo & vbCrLf
      AddRighe = AddRighe + 4
    End If
  End If
  
  RTBModifica.SelStart = wPos
  RTBModifica.SelLength = 0
  
  RTBModifica.SelText = Testo
End Sub

Sub Insert_ProcedureTemplate(RTBModifica As RichTextBox, RTBErr As RichTextBox)
  Dim Rtb As RichTextBox, Testo As String, s() As String, i As Integer
  Dim rsSegmenti As Recordset, wEnv As Collection
  Dim templateName As String
  
  On Error GoTo catch
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  
  'stefanopul
'  wpath = m_fun.FnPathPrj & "\input-prj\Template\INCAPS\ProcedurePgmDB"
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\ProcedurePgmDB"
  Rtb.LoadFile templateName
    
  For i = 1 To distinctDB.Count
    Set wEnv = New Collection
    wEnv.Add distinctDB.item(i)
    Testo = Testo & "BPHX-C      EXEC SQL INCLUDE " & MadrdM_GenRout.getEnvironmentParam_SQ(kCopyParam, "dbd", wEnv) & " END-EXEC."
    If Not i = distinctDB.Count Then
      Testo = Testo & vbCrLf
    End If
  Next
  'testo = Left(testo, Len(testo) - 2)
  MadrdM_GenRout.changeTag Rtb, "<INCLUDE_COPY-K>", Testo
  Testo = ""
      
  For i = 1 To distinctSeg.Count
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where Nome = '" & distinctSeg.item(i) & "'")
    Set wEnv = New Collection
    wEnv.Add Null
    wEnv.Add rsSegmenti
    Testo = Testo & "BPHX-C     EXEC SQL INCLUDE " & MadrdM_GenRout.getEnvironmentParam_SQ(mCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf
    rsSegmenti.Close
  Next
  Testo = Left(Testo, Len(Testo) - 2)
  MadrdM_GenRout.changeTag Rtb, "<INCLUDE_COPY-M>", Testo
  
  Testo = Rtb.text
  s = Split(Testo, vbCrLf)
     
  AddRighe = AddRighe + UBound(s)
  
  
  RTBModifica.SelStart = Len(RTBModifica.text)
  RTBModifica.SelLength = 0
  RTBModifica.SelText = Testo
  'wPos = wPos + Len(testo)
  
   '**********
  Testo = ""
  For i = 1 To distinctSeg.Count
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where Nome = '" & distinctSeg.item(i) & "'")
    Set wEnv = New Collection
    wEnv.Add Null
    wEnv.Add rsSegmenti
    Testo = Testo & "BPHX-I     EXEC SQL INCLUDE " & MadrdM_GenRout.getEnvironmentParam_SQ(tCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf
    rsSegmenti.Close
  Next
  Testo = Left(Testo, Len(Testo) - 2)
  MadrdM_GenRout.changeTag RTBModifica, "<INCLUDE_COPY-T>", Testo
  Testo = ""
  For i = 1 To distinctSeg.Count
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where Nome = '" & distinctSeg.item(i) & "'")
    Set wEnv = New Collection
    wEnv.Add Null
    wEnv.Add rsSegmenti
    Testo = Testo & "BPHX-I     EXEC SQL INCLUDE " & MadrdM_GenRout.getEnvironmentParam_SQ(xCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf
    rsSegmenti.Close
  Next
  Testo = Left(Testo, Len(Testo) - 2)
  MadrdM_GenRout.changeTag RTBModifica, "<INCLUDE_COPY-X>", Testo
  
  Testo = ""
  For i = 1 To distinctSeg.Count
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where Nome = '" & distinctSeg.item(i) & "'")
    Set wEnv = New Collection
    wEnv.Add Null
    wEnv.Add rsSegmenti
    Testo = Testo & "BPHX-I     EXEC SQL INCLUDE " & MadrdM_GenRout.getEnvironmentParam_SQ(cCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf
    rsSegmenti.Close
  Next
  Testo = Left(Testo, Len(Testo) - 2)
  MadrdM_GenRout.changeTag RTBModifica, "<INCLUDE_COPY-C>", Testo
  Exit Sub
catch:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "* Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  ElseIf err.Number = 75 Then
    AggLog "* Template: " & templateName & " - " & err.Description, MadrdF_Incapsulatore.RTErr
  Else
    AggLog "* unexpected error: " & err.Description, MadrdF_Incapsulatore.RTErr
  End If
End Sub

Sub Insert_TagRoutine(RTBModifica As RichTextBox)
  Dim i As Integer, Testo As String
  
  Testo = Testo & vbCrLf
  AddRighe = AddRighe + 1
  For i = 1 To distinctCod.Count
    Testo = Testo & "<" & distinctCod.item(i) & ">" & vbCrLf
    AddRighe = AddRighe + 1
  Next
      
  RTBModifica.SelStart = Len(RTBModifica.text)
  RTBModifica.SelLength = 0
  RTBModifica.SelText = Testo

End Sub

Sub INSERT_DECOD(wPos As Long, RTBModifica As RichTextBox, RTBErr As RichTextBox, rs As Recordset, typeIncaps As String)
  Dim wIstruzione As String, statement As String
  Dim blankspace As String
  On Error GoTo catch
  
  ' nella vecchia versione no template
  If Not rs!isExec Then
    Testo = CostruisciCall_CALLTemplate(Testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr)
  Else
    Testo = CostruisciCall_CALLTemplate_EXEC(Testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr)
  End If
  
  RTBModifica.SelStart = wPos
  RTBModifica.SelLength = 0
  RTBModifica.SelText = Testo
  wPos = wPos + Len(Testo)
         
  'TMP - DEBUG:
'  If wPunto Then
'    'SQ
'    'statement = START_TAG & Space(wIdent) & "."
'    '?wIdent
'    statement = START_TAG & Space(11 - Len(START_TAG)) & "."
'    statement = statement & Space(72 - Len(statement)) & END_TAG
'    RTBModifica.SelStart = wPos
'    RTBModifica.SelLength = 0
'    RTBModifica.SelText = statement
'    wPos = wPos + Len(statement)
'  End If
 'tilvia
 ' indentkey = 4
  If (indentkey - Len(START_TAG) - 3) > 0 Then
    blankspace = Space(indentkey - Len(START_TAG))
  Else
    blankspace = Space(3)
  End If
  If wPunto Then
    'SQ
    ''' PERCHE ? tilvia ?
    'tilvia
    'statement = START_TAG & blankspace & "."
    'statement = START_TAG & Space(wIdent) & "."
    '?wIdent
    If rs!isExec Then
      ' Mauro 29/07/2008
      If TipoFile = "CBL" Or TipoFile = "CPY" Then
        'statement = Space(indentkey - 3) & "END;" & vbCrLf
        'AddRighe = AddRighe + 1
        statement = START_TAG & blankspace & "."
      End If
    Else
      statement = START_TAG & blankspace & "."
    End If
  ' Mauro 29/07/2008
  'Else
    'If rs!isExec Then
      'statement = Space(indentkey - 3) & "END;" & vbCrLf
      'AddRighe = AddRighe + 1
    'Else
      'AC 30/04708
      'statement = START_TAG & blankspace & ""
    'End If
  End If
  'AC  19/03/09  ?????????????????? non mi sovviene il motivo, ho asteriscato l'IF sotto
  'If Not rs!isExec Then
  '  statement = statement & Space(72 - Len(statement)) & END_TAG
  'End If
  
  'AC 30/04708
  If Not statement = "" Then
    RTBModifica.SelStart = wPos
    RTBModifica.SelLength = 0
    RTBModifica.SelText = statement
    wPos = wPos + Len(statement)
  Else
    'AddRighe = AddRighe - 1
  End If
  
  'SQ 18-04-08
  Exit Sub
catch:
  AggLog "[Object: " & rs!IdOggetto & " - Row: " & rs!Riga & "] INSERT DECOD ERROR", MadrdF_Incapsulatore.RTErr
End Sub

Function getorcaller(idpgm As Long) As String
  Dim namepg As String
  namepg = Trim(namePgmFromId(idpgm))
  If InStr(namepg, ".") > 0 Then
   namepg = Left(namepg, InStr(namepg, ".") - 1)
  End If
  If TipoFile = "CPY" Then
    getorcaller = vbCrLf & FormatTag(Space(8) & "OR  '" & namepg & "'")
    getorcaller = Left(getorcaller, Len(getorcaller) - 2)
  Else ' include PLI
    getorcaller = vbCrLf & Space(8) & "||  '" & namepg & "'"
  End If
End Function

Public Function namePgmFromId(pIdPgm As Long)
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & pIdPgm)
  If Not r.EOF Then
    If Trim(r!estensione) = "" Then
      namePgmFromId = r!nome
    Else
      namePgmFromId = r!nome & "." & r!estensione
    End If
  End If
  r.Close
End Function

Function getIfCaller(idpgm As Long) As String
  Dim namepg As String

  namepg = Trim(namePgmFromId(idpgm))
  If InStr(namepg, ".") > 0 Then
    namepg = Left(namepg, InStr(namepg, ".") - 1)
  End If
  If TipoFile = "CPY" Then
    getIfCaller = vbCrLf & FormatTag(Space(indentkey - 7) & "IF LNKDB-CALLER = '" & namepg & "'")
    indentkey = indentkey + 3
    getIfCaller = Left(getIfCaller, Len(getIfCaller) - 2)
  Else ' include PLI
    getIfCaller = Space(indentkey) & "IF LNKDB_CALLER = '" & namepg & "'"
    'getifcaller = vbCrLf & Space(indentkey) & "IF LNKDB_CALLER = '" & namepg & "'"
    '& vbCrLf & Space(indentkey) & "THEN DO;"
    indentkey = indentkey + 3
  End If
End Function

Function getRowNumber(Source As String) As Integer
  Dim s() As String
  
  s = Split(Source, vbCrLf)
  getRowNumber = UBound(s)
End Function

Function getEndOfIfStat(rs As Recordset) As String
  If wPunto Then
    If rs!isExec Then
      getEndOfIfStat = Space(indentkey - 3) & "END;" & vbCrLf
      'AddRighe = AddRighe + 1
    Else
      getEndOfIfStat = START_TAG & Space(indentkey - Len(START_TAG) - 3) & "END-IF."
    End If
  Else
    If rs!isExec Then
      getEndOfIfStat = Space(indentkey - 3) & "END;" & vbCrLf
      'AddRighe = AddRighe + 1
    Else
      getEndOfIfStat = START_TAG & Space(indentkey - Len(START_TAG) - 3) & "END-IF" & vbCrLf
    End If
  End If
End Function

Sub INSERT_DECODC(wPos As Long, RTBModifica As RichTextBox, RTBErr As RichTextBox, rs As Recordset, typeIncaps As String, Index As Integer, idpgmind As Long)
  Dim wIstruzione As String, statement As String, s() As String
  Dim lastrow As String, lenrow As Integer
  Dim tb As Recordset
  Dim firstIFblock As String, otherIFblock As String, codindex As String, varthendo As String
  Dim oldAddRighe As Integer
  On Error GoTo catch
  
  'AC 09/05708
  'Nuova VERSIONE
  'cerca di accorpare incapsulati con la stessa codifica
  'accorpa solo i primi, finche codifiche sono uguali li accorpa in un solo if
  'con i programmi chiamanti in OR
  'se sono tutte uguali  l'if scompare e rimane un unico incapsulato
  'appena trova una codifica diversa, scrive l'if per la prima codifica (evemtualmente con
  'una serie di programmi in OR e da quel momento in poi ad ogni programma
  ' corrisponde un if
  
  If Not rs!isExec Then
    varthendo = ""
  Else
    varthendo = vbCrLf & Space(indentkey) & "THEN DO;"
  End If
  
  '3 casi:  a) primo elemento (quello di riferimento per la codifica)
  '         b) elementi che potrebbero avere la  stessa decodifica
  '         c) saltato il controllo sulla codifica (salta sul primo elemento con
  '         codifica differente), da l� in poi si fa un if per ogni elemento
  'If index = 1 Then
  If firstindex Then
    firstindex = False
    ' subito costruisco testo
    CostruisciCommonInitializeTemplateC rs
    If Not rs!isExec Then
      CostruisciOperazioneTemplate Trim(wIstruzione), 1, idpgmind, rs!IdOggetto, rs!Riga, IIf((0 & rs!ordPcb_MG) > 0, 0 & rs!ordPcb_MG, 0 & rs!ordPcb)
    Else
      CostruisciOperazioneTemplate_EXEC Trim(wIstruzione), 1, idpgmind, rs!IdOggetto, rs!Riga, IIf((0 & rs!ordPcb_MG) > 0, 0 & rs!ordPcb_MG, 0 & rs!ordPcb)
    End If
    If err.Number > 0 Then CostruisciOperazioneTemplate Trim(wIstruzione), 1, idpgmind, rs!IdOggetto, rs!Riga, 0 & rs!ordPcb
    If Not rs!isExec Then
      Testo = CostruisciCall_CALLTemplate(Testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr, True)
    Else
      Testo = CostruisciCall_CALLTemplate_EXEC(Testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr, True)
    End If
    
    ' mettiamo da parte
    TestoFirst = Testo
    Testo = ""
    
    ' imposto la codifica di riferimento
    Set tb = m_fun.Open_Recordset("select Codifica from Psdli_istrCodifica where IdPgm = " & idpgmind & " and IdOggetto = " & rs!IdOggetto & " and Riga = " & rs!Riga & " order by codifica")
    While Not tb.EOF
      codrif = codrif & tb!Codifica ' nel caso di molteplicit� le concateno
      tb.MoveNext
    Wend
    firstIF = getIfCaller(idpgmind)
  Else ' dal secondo programma chiamante in poi, controllo se codifiche uguali oppure diverse
    If checkcod Then
      ' in questo flusso le codifiche dei programmi precedenti (per una data riga) sono uguali
      ' oppure sono al secondo programma e verifico che le codifiche siano ugulai a quelle del primo
      
      ' salvataggio di addrighe (incapsulamento senza if che potrebbero non servire)
      oldAddRighe = AddRighe
      ' COSTRUZIONE DEL TESTO PER QUESTO INCAPSULATO
      '********************************************
      indentkey = indentkey + 3
      CostruisciCommonInitializeTemplateC rs
      If Not rs!isExec Then
        CostruisciOperazioneTemplate Trim(wIstruzione), 1, idpgmind, rs!IdOggetto, rs!Riga, IIf((0 & rs!ordPcb_MG) > 0, 0 & rs!ordPcb_MG, 0 & rs!ordPcb)
      Else
        CostruisciOperazioneTemplate_EXEC Trim(wIstruzione), 1, idpgmind, rs!IdOggetto, rs!Riga, IIf((0 & rs!ordPcb_MG) > 0, 0 & rs!ordPcb_MG, 0 & rs!ordPcb)
      End If
      If err.Number > 0 Then CostruisciOperazioneTemplate Trim(wIstruzione), 1, idpgmind, rs!IdOggetto, rs!Riga, 0 & rs!ordPcb
      If Not rs!isExec Then
        Testo = CostruisciCall_CALLTemplate(Testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr, True)
      Else
        Testo = CostruisciCall_CALLTemplate_EXEC(Testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr, True)
      End If
      indentkey = indentkey - 3
      '*********************************************
      
      '***** costruzione della stringa delle codifiche concatenando le molteplicit�
      Set tb = m_fun.Open_Recordset("select Codifica from Psdli_istrCodifica where IdPgm = " & idpgmind & " and IdOggetto = " & rs!IdOggetto & " and Riga = " & rs!Riga & " order by codifica")
      While Not tb.EOF
        codindex = codindex & tb!Codifica
        tb.MoveNext
      Wend
      
      If codrif = codindex Then
      ' aggiorniamo la if nel caso le successive siano diverse
        If Not Index = -1 Then ' non � l'ultimo
          firstIF = firstIF & getorcaller(idpgmind) ' aggiorno if con gli or
          Testo = ""                                ' il testo costruito non mi serve perch� uguale a TestoFirst
        Else
          Testo = TestoFirst                        ' � l'ultimo e riprendo TestoFirst
        End If
        ' sono state aggiunte righe che non saranno inserite
        AddRighe = oldAddRighe                       ' il conteggio reale delle righe � su oldAddRighe
        indentkey = indentkey + 3
      Else
        checkcod = False
        firstIFblock = firstIF & varthendo               ' devo sistemare la if del blocco precedente
        otherIFblock = getIfCaller(idpgmind) & varthendo ' costruisco subito la if per l'altro blocco
        endstatement = getEndOfIfStat(rs)                ' costruisco l'end per i vari casi (copy o include .. con punto o senza punto
        
        'indentazione
        If TipoFile = "CPY" Then
          TestoFirst = Replace(Left(TestoFirst, Len(TestoFirst) - 2), vbCrLf & START_TAG, vbCrLf & START_TAG & Space(3)) & vbCrLf
        Else
          TestoFirst = Replace(Left(TestoFirst, Len(TestoFirst) - 2), vbCrLf, vbCrLf & Space(3)) & vbCrLf
        End If
        Testo = firstIFblock & TestoFirst & endstatement & otherIFblock & Testo & endstatement
        'AddRighe = AddRighe + index + IIf(Len(varthendo), 1, 0) + 2
        
        'counttomigrate: progressivo del programma chiamante
        'AC 17/12/08 asterisco versione in teoria testata
        'AddRighe = AddRighe + counttomigrate + IIf(Len(varthendo), 1, 0) + 2
        
        '        = righe  if   + then do; se c'�           + 2 end di chiusura dell'if
        
        AddRighe = AddRighe + counttomigrate + 2 * IIf(Len(varthendo), 1, 0)
        
        
        
        'AddRighe = AddRighe + getrownumber(firstIFblock & endstatement & otherIFblock)
        'AddRighe = AddRighe + getrownumber(endstatement)
      End If
      
    Else
      If TipoFile = "CPY" Then
        AddRighe = AddrighebefIstr ' l'esatto conteggio esce direttamente da Costruisci Call
      Else
        AddRighe = AddrighebefIstr + 1
      End If
      otherIFblock = getIfCaller(idpgmind) & varthendo
      Testo = Testo & otherIFblock
      'AddRighe = AddRighe + getrownumber(otherIFblock)
      'indentkey = indentkey + 3
      CostruisciCommonInitializeTemplateC rs
      If Not rs!isExec Then
        CostruisciOperazioneTemplate Trim(wIstruzione), 1, idpgmind, rs!IdOggetto, rs!Riga, IIf((0 & rs!ordPcb_MG) > 0, 0 & rs!ordPcb_MG, 0 & rs!ordPcb)
      Else
        CostruisciOperazioneTemplate_EXEC Trim(wIstruzione), 1, idpgmind, rs!IdOggetto, rs!Riga, IIf((0 & rs!ordPcb_MG) > 0, 0 & rs!ordPcb_MG, 0 & rs!ordPcb)
      End If
      If err.Number > 0 Then CostruisciOperazioneTemplate Trim(wIstruzione), 1, idpgmind, rs!IdOggetto, rs!Riga, 0 & rs!ordPcb
      If Not rs!isExec Then
        Testo = CostruisciCall_CALLTemplate(Testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr, True)
      Else
        Testo = CostruisciCall_CALLTemplate_EXEC(Testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr, True)
      End If
      'indentkey = indentkey - 3
      endstatement = getEndOfIfStat(rs)
      Testo = Testo & endstatement
      ' esce gi� con riga in pi� per l'end if
      'AddRighe = AddRighe + 1 'getrownumber(endstatement)
    End If
  End If
  
  VItable(UBound(VItable)).lines_down = VItable(UBound(VItable)).lines_down & vbCrLf & Testo
  
  ' fuori ciclo, alla fine
'  RTBModifica.SelStart = wPos
'  RTBModifica.SelLength = 0
'  RTBModifica.SelText = Testo
'  wPos = wPos + Len(Testo)
       
  
'  If Not rs!isExec Then
'    statement = statement & Space(72 - Len(statement)) & END_TAG
'  End If
'  RTBModifica.SelStart = wPos
'  RTBModifica.SelLength = 0
'  RTBModifica.SelText = statement
'  wPos = wPos + Len(statement)
  
  'SQ 18-04-08
  Exit Sub
catch:
  AggLog "[Object: " & rs!IdOggetto & "] INSERT DECODC ERROR", MadrdF_Incapsulatore.RTErr
End Sub

Function NumToken(Source As String) As Integer
' num elementi separati da virgola
Dim psource As String
  NumToken = 1
  psource = Source
  psource = RTrim(psource)
  If Not Right(psource, 1) = ";" Then
    psource = Source
  Else
    psource = Left(psource, Len(psource) - 1)
  End If
  While InStr(1, psource, ";")
    NumToken = NumToken + 1
    psource = Right(psource, Len(psource) - InStr(1, psource, ";"))
  Wend
End Function
Function TokenIesimo(Source As String, pos As Integer) As String
' numerazione da 1
Dim i As Integer
Dim psource As String
  'elemento unico
  psource = Source
  multiOp = False
  psource = RTrim(psource)
  If Not Right(psource, 1) = ";" Then
    psource = Source
  Else
    psource = Left(psource, Len(psource) - 1)
  End If
  For i = 1 To pos
    If InStr(1, psource, ";") = 0 Then
       TokenIesimo = psource
       Exit Function
    End If
    multiOp = True
    multiOpGen = True
    TokenIesimo = Left(psource, InStr(1, psource, ";") - 1)
    psource = Right(psource, Len(psource) - InStr(1, psource, ";"))
  Next
End Function

Function TokenIesimoCC(Source As String, pos As Integer, moltp As Boolean) As String
'versione solo per i command code
'ho verificato a priori se ci sono molteplicit� reali

' numerazione da 1
Dim i As Integer
Dim psource As String
  'elemento unico
  psource = Source
  multiOp = False
  psource = RTrim(psource)
  If Not Right(psource, 1) = ";" Then
    psource = Source
  Else
    psource = Left(psource, Len(psource) - 1)
  End If
  For i = 1 To pos
    If InStr(1, psource, ";") = 0 Then
       TokenIesimoCC = psource
       Exit Function
    End If
    If moltp Then
      multiOp = True
      multiOpGen = True
    End If
    TokenIesimoCC = Left(psource, InStr(1, psource, ";") - 1)
    psource = Right(psource, Len(psource) - InStr(1, psource, ";"))
  Next
End Function
Function AggiornaCombinazione(pmaxoperatori As Integer) As Boolean
Dim lowelement As Integer, i As Integer, j As Integer, maxoperatori As Integer
Dim maxcommand As Integer
Dim aggiornato As Boolean
  aggiornato = False
  AggiornaCombinazione = False
  For i = 1 To UBound(TabIstr.BlkIstr)
    If Not aggiornato Then
      lowelement = Combinazionisegmenti(i)
      lowelement = lowelement + 1
      If lowelement <= NumToken(TabIstr.BlkIstr(i).SegmentM) Then
         Combinazionisegmenti(i) = lowelement
         aggiornato = True
         Exit For
      Else
        Combinazionisegmenti(i) = 1  ' equivale ad azzerare
      End If
    End If
  Next
'  If gbIstr = "ISRT" Then
'    AggiornaCombinazione = aggiornato
'    Exit Function
'  End If
  If Not aggiornato Then
    ' azzero segmenti
'    For i = 1 To UBound(TabIstr.BlkIstr)
'      Combinazionisegmenti(i) = 1
'    Next
    For i = 1 To UBound(TabIstr.BlkIstr)
      If Not aggiornato Then
        For j = 1 To UBound(TabIstr.BlkIstr(i).KeyDef)
           lowelement = MatriceCombinazioni(i, j)
           lowelement = lowelement + 1
           ' AC 05/07/06 devo usare indice i per livello e j per la chiave
           If UBound(TabIstr.BlkIstr(i).KeyDef) > 0 Then
           maxoperatori = NumToken(TabIstr.BlkIstr(i).KeyDef(j).op)
           If lowelement <= maxoperatori Then
              MatriceCombinazioni(i, j) = lowelement
              aggiornato = True
              Exit For
           Else
              MatriceCombinazioni(i, j) = 1  ' equivale ad azzerare
           End If
           End If
        Next j
      Else
        Exit For
      End If
    Next i
    
    ' Volendo gestire anche molteplicit� su op logici occorre un'altra
    ' matrice di dimensioni pari a MatriceCombinazioni
    ' in pratica si ha prima l'azzeramento di MatriceCombinazioni come sotto
    ' poi si fa il ciclo di aggiornamento sulla nuova matrice (come per MatriceCombinazioni)
    ' il pezzo sotto riazzera la nuova matrice prima di incrementare
    ' la combinazione dei comcode
    If Not aggiornato Then
      For i = 1 To UBound(TabIstr.BlkIstr)
        If Not aggiornato Then
          For j = 1 To UBound(TabIstr.BlkIstr(i).KeyDef) - 1
             lowelement = MatriceCombinazioniBOL(i, j)
             lowelement = lowelement + 1
             ' AC 05/07/06 devo usare indice i per livello e j per la chiave
             If UBound(TabIstr.BlkIstr(i).KeyDef) > 0 Then
              maxoperatori = NumToken(TabIstr.BlkIstr(i).KeyDef(j).OpLogic)
              If lowelement <= maxoperatori Then
                 MatriceCombinazioniBOL(i, j) = lowelement
                 aggiornato = True
                 Exit For
              Else
                 MatriceCombinazioniBOL(i, j) = 1  ' equivale ad azzerare
              End If
             End If
          Next j
        Else
          Exit For
        End If
      Next i
      
      ' AC 03/07/06  finite le combinazioni su operatori li azzero
      ' e incremento array con combinazioni commandcode
      If Not aggiornato Then
        ' riporto a 1 combinazione operatori
        For i = 1 To UBound(TabIstr.BlkIstr)
          For j = 1 To pmaxoperatori
            MatriceCombinazioni(i, j) = 1
          Next
        Next
        For i = 1 To UBound(TabIstr.BlkIstr)
          lowelement = CombinazioniComCode(i)
          lowelement = lowelement + 1
          maxcommand = NumToken(TabIstr.BlkIstr(i).Search)
          If lowelement <= maxcommand Then
            CombinazioniComCode(i) = lowelement
            'Controllo Dcombination
            If CheckDcomb(CombinazioniComCode) Then
              aggiornato = True
              Exit For
            End If
          Else
            CombinazioniComCode(i) = 1
          End If
        Next
      End If
    End If
  End If
  AggiornaCombinazione = aggiornato
End Function
Function AggiornaCombinazione_ETZ(pmaxoperatori As Integer) As Boolean
Dim lowelement As Integer, i As Integer, j As Integer, maxoperatori As Integer
Dim maxcommand As Integer
Dim aggiornato As Boolean
  aggiornato = False
  AggiornaCombinazione_ETZ = False
  For i = 1 To UBound(TabIstr.BlkIstr)
    If Not aggiornato Then
      For j = 1 To UBound(TabIstr.BlkIstr(i).KeyDef)
         lowelement = MatriceCombinazioni(i, j)
         lowelement = lowelement + 1
         ' AC 05/07/06 devo usare indice i per livello e j per la chiave
         If UBound(TabIstr.BlkIstr(i).KeyDef) > 0 Then
         maxoperatori = NumToken(TabIstr.BlkIstr(i).KeyDef(j).op)
         If lowelement <= maxoperatori Then
            MatriceCombinazioni(i, j) = lowelement
            aggiornato = True
            Exit For
         Else
            MatriceCombinazioni(i, j) = 1  ' equivale ad azzerare
         End If
         End If
      Next j
    Else
      Exit For
    End If
  Next i
  
  ' Volendo gestire anche molteplicit� su op logici occorre un'altra
  ' matrice di dimensioni pari a MatriceCombinazioni
  ' in pratica si ha prima l'azzeramento di MatriceCombinazioni come sotto
  ' poi si fa il ciclo di aggiornamento sulla nuova matrice (come per MatriceCombinazioni)
  ' il pezzo sotto riazzera la nuova matrice prima di incrementare
  ' la combinazione dei comcode
  
  
  ' AC 03/07/06  finite le combinazioni su operatori li azzero
  ' e incremento array con combinazioni commandcode
  If Not aggiornato Then
    ' riporto a 1 combinazione operatori
    For i = 1 To UBound(TabIstr.BlkIstr)
      For j = 1 To pmaxoperatori
        MatriceCombinazioni(i, j) = 1
      Next
    Next
    For i = 1 To UBound(TabIstr.BlkIstr)
      lowelement = CombinazioniComCode(i)
      lowelement = lowelement + 1
      maxcommand = NumToken(TabIstr.BlkIstr(i).Search)
      If lowelement <= maxcommand Then
        CombinazioniComCode(i) = lowelement
        'Controllo Dcombination
        If CheckDcomb(CombinazioniComCode) Then
          aggiornato = True
          Exit For
        End If
      Else
        CombinazioniComCode(i) = 1
      End If
    Next
  End If
  AggiornaCombinazione_ETZ = aggiornato
End Function

Function CheckDcomb(ArrComCode() As Integer) As Boolean
  Dim k As Integer, SumD As Integer
  
  For k = 1 To UBound(ArrComCode) - 1
    If InStr(1, TokenIesimoCC(TabIstr.BlkIstr(k).Search, ArrComCode(k), False), "D") Then
      SumD = SumD + 1
    End If
  Next k
  If SumD = 0 Or SumD = UBound(ArrComCode) - 1 Then  ' nessuna D
    CheckDcomb = True
  Else
    CheckDcomb = False
  End If
End Function

Sub CreaArrayCloniIstruzione(ByRef arraySource() As String, pIdOggetto As Long, pRiga As Long)
  Dim tbCloni As Recordset
  Dim i As Integer
  
  Set tbCloni = m_fun.Open_Recordset("select * from PsDLI_Istruzioni_Clone where " & _
                                     "IdOggetto = " & pIdOggetto & " and Riga = " & pRiga & " and Istruzione is not Null order by IdClone")
  ReDim arraySource(0)
  arraySource(0) = TabIstr.istrOriginalMolt
  While Not tbCloni.EOF
    If Not tbCloni!Istruzione = "" Then
      ReDim Preserve arraySource(UBound(arraySource) + 1)
      arraySource(UBound(arraySource)) = tbCloni!Istruzione
    End If
    tbCloni.MoveNext
  Wend
  tbCloni.Close
End Sub

Sub CreaArrayCloniPCB(ByRef arraySource() As String, pIdOggetto As Long, pRiga As Long, Optional IsOnlyCheck As Boolean = False)
  Dim tbCloni As Recordset
  Dim i As Integer
  
  Set tbCloni = m_fun.Open_Recordset("select * from PsDLI_Istruzioni_Clone where " & _
                                     "IdOggetto = " & pIdOggetto & " and Riga = " & pRiga & " and ordPcb is not Null order by IdClone")
  ReDim arraySource(0)
  arraySource(0) = TabIstr.numpcb
  While Not tbCloni.EOF
    If IsOnlyCheck Then
      arraySource(0) = "CLONI"
      Exit Sub
    End If
    If Not tbCloni!ordPcb = "" Then
      ReDim Preserve arraySource(UBound(arraySource) + 1)
      arraySource(UBound(arraySource)) = tbCloni!ordPcb
    End If
    tbCloni.MoveNext
  Wend
  tbCloni.Close
End Sub

Sub CreaArrayCloniSegmenti(ByRef cloniSegmenti() As String, TabIstr As IstrDliLocal)
  Dim i As Integer
  
  ReDim cloniSegmenti(UBound(TabIstr.BlkIstr))
  For i = 1 To UBound(TabIstr.BlkIstr)
    cloniSegmenti(i) = TabIstr.BlkIstr(i).SegmentM
  Next i
End Sub

Sub aggiornalistaCodOp(item As String)
  On Error GoTo alreadyPresent
  
  distinctCod.Add item, item
  
  Exit Sub
alreadyPresent:
  'ok
End Sub

Sub aggiornalistaSeg(item As String)
  On Error GoTo err
  distinctSeg.Add item, item
  
  Exit Sub
err:

End Sub

Sub aggiornalistaDB(item As String)
On Error GoTo err
  distinctDB.Add item, item
 
  Exit Sub
err:

End Sub

Function buildSubstringFromType(strname As String, pstart As String, plen As String, ptype As String, ptypestr As String, pIdOggetto As Long) As String
  Dim dataType As String
  
  Select Case ptype
    Case "PLI", "INC"
      If ptypestr = "SSA" Then
        'AC 20/03/09
        If Left(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).ssaName, 1) = "'" And Right(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).ssaName, 1) = "'" Then
          dataType = "X"
        Else
          getAreaType Trim(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).ssaName), pIdOggetto, 0, dataType, "", -1
        End If
        If dataType = "X" Then
          buildSubstringFromType = "SUBSTR(" & strname & "," & pstart & "," & plen & ")"
        Else
          buildSubstringFromType = "SUBSTR(IRIS_" & strname & "," & pstart & "," & plen & ")"
        End If
      Else
        buildSubstringFromType = "SUBSTR(" & strname & "," & pstart & "," & plen & ")"
      End If
    Case Else
      buildSubstringFromType = strname & "(" & pstart & ":" & plen & ")"
  End Select
End Function

Function buildLogicalOpEncaps(op As String, ssa As String, IdOgg As Long, Riga As Long)
  Select Case op
    Case "+", "|"
      buildLogicalOpEncaps = "REVOR1" & vbCrLf & "    " & Space(Len(ssa)) & "      " & "OR REVOR2"
    Case "*", "&"
      buildLogicalOpEncaps = "REVAND1" & vbCrLf & "    " & Space(Len(ssa)) & "      " & "OR REVAND2"
    Case "#"
      buildLogicalOpEncaps = "REVIAND"
    Case ")"
      buildLogicalOpEncaps = "REVRP"
    Case Else
      buildLogicalOpEncaps = "MISS"
      m_fun.WriteLog "Incaps: Obj(" & IdOgg & ") Row( " & Riga & ") Wrong logical operatore", "MadrdM_Incapsulatore"
  End Select
End Function

Function buildLogicalOpFromType(op As String, ptype As String)
  Select Case ptype
    Case "PLI"
      Select Case op
        Case "OR"
          'IRIS-DB buildLogicalOpFromType = "!"
          buildLogicalOpFromType = "|"
      End Select
    Case Else
      Select Case op
        Case "OR"
          buildLogicalOpFromType = "OR"
      End Select
  End Select
End Function

Function CheckCombCCode(istr As String) As Boolean
  Dim lowelement As Integer, i As Integer, maxcommand As Integer, aggiornato As Boolean
  'If istr = "ISRT" Then
  '  CheckCombCCode = False
  '  Exit Function
  'End If
  For i = 1 To UBound(TabIstr.BlkIstr)
    lowelement = CombinazioniComCode(i)
    lowelement = lowelement + 1
    maxcommand = NumToken(TabIstr.BlkIstr(i).Search)
    If lowelement <= maxcommand Then
      CombinazioniComCode(i) = lowelement
      'Controllo Dcombination
      If CheckDcomb(CombinazioniComCode) Then
        aggiornato = True
        Exit For
      End If
    Else
      CombinazioniComCode(i) = 1
    End If
  Next i
  CheckCombCCode = aggiornato
End Function

Function CostruisciOperazioneTemplateNoDecode_EXEC(xIstr As String, Index As Integer, idpgm As Long, pIdOggetto As Long, pRiga As Long, ordPcb As Long)
  Dim tb As Recordset, tb1 As Recordset
  Dim wOpe As String, wOpeS As String, TipIstr As String
  Dim k As Long
  Dim xope As String
  Dim K1 As Integer
  Dim wUtil As Boolean, wFlagBoolean As Boolean
  Dim i As Integer, j As Integer
  Dim checkColl As Boolean
  Dim extraIndent As Integer
  Dim maxId As Long
  Dim wIstr, wDecod As String, wIstrS As String, wDecodS As String, pcbDyn As String
  Dim maxPcbDyn As Integer, pcbDynInt As Integer
  Dim wOpeSave As String
  Dim y As Integer
  Dim numIstr As Integer 'IRIS-DB
  Dim codFunc As String 'IRIS-DB
  ' per comporre stringa if dell'incapsulato, versione qual e squal
  Dim IstrIncaps As String, IstrSIncaps As String, StrAnd As String
  ' contiene la molteplicit� di istruzioni, attinge dalla tabella Istruzioni_Clone
  Dim cloniIstruzione() As String
  Dim cloniSegmenti() As String
  Dim combinazioni As Boolean
  Dim maxLogicOp As Integer, maxBoolean As Integer
  Dim Operatore As String
  Dim StringToClear As String
  Dim ifAnd As String
  Dim nomeRoutCompleto As String
  Dim templateName As String
  On Error GoTo ErrH
  'AC 02/11/06
  
  Dim varOp As String, combOp As String, varIstr As String, combIstr As String
  Dim varop_s As String, combop_s As String, ks As Integer
  Dim varCmd As String, combCmd As String, varSeg As String, combSeg As String
  Dim varcmd_s As String, combcmd_s As String
  Dim xprogramm As String
  Dim CombPLevel() As String, CombPLevel_s() As String, CombSqual() As String, CombQual() As String, wIstrsOld As String
  
  ' nuove varibili operatore
  Dim CombPLevelOper() As String, ComboPLevelTempOper As String, ComboPLevelAppoOper As String
  Dim bolMoltOpLogic As Boolean, jj As Integer
  
  Dim ComboPLevelTemp As String, ComboPLevelAppo As String, nFBms As Integer, wRecIn As String, jk As Integer
  Dim Rtb  As RichTextBox, appocomb As String, appoFinalString As String, checksqual As Boolean, posEndLine As String
  ' tango traccia di una qualsiasi molteplicit� sui livelli
  Dim moltLevel As Boolean, moltLevelesimo() As Boolean, ij As Integer, moltLevThen As Boolean
  
  'AC 19/06/07  arriva la booleana
  Dim OperatoreLogico As String, ultimoOperatore As Boolean, varLogicop As String, combLogicop As String
  Dim OperatoreLogicoSimb As String, moltBOL() As Boolean, bolmoltBOL As Boolean
  Dim bolMultipleCcod As Boolean
  Dim CCodeSqual() As String, z1 As Integer, numCombinazioni As Integer
      
  Dim rsDBD As Recordset, NomeDbD As String
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  wPath = m_fun.FnPathPrj
  
  nFBms = FreeFile

  multiOpGen = False
  '---- nuovo template
  Open wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVELOPE" For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTempOper = "" Then
      ComboPLevelTempOper = ComboPLevelTempOper & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTempOper = ComboPLevelTempOper & wRecIn
  Wend
  Close #nFBms
  '-------------------------------
  
  Open wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVEL" For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTemp = "" Then
      ComboPLevelTemp = ComboPLevelTemp & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTemp = ComboPLevelTemp & wRecIn
  Wend
  
  jk = 1
  ReDim moltLevelesimo(UBound(TabIstr.BlkIstr))
  ReDim Preserve CombSqual(0)
  'SQ ancora!!??? abbiamo letto l'istruzione all'inizio
  '***** determiniamo se deve essere generata
  '***** sia la versione qualificata che squalificata
  Set tb = m_fun.Open_Recordset("select * from PsDLI_IstrDett_Liv where " & _
                                "IdOggetto =" & pIdOggetto & " and Riga = " & pRiga & " and " & _
                                "Livello = " & UBound(TabIstr.BlkIstr))
  If Not tb.EOF Then
    QualAndUnqual = tb!QualAndUnqual
  Else
    QualAndUnqual = False
  End If
  tb.Close
  
  StringToClear = ""
  ReDim wNomeRout(0)
  
  ' Ciclo esterno su istruzione
  ReDim Preserve CombSqual(0)
  
  'TO DO   CICLO COMBINAZIONI su struttura delle decodifiche
  ' si parte da istruzione
  j = 1 '(indice di TabDec si incrementa sempre da dove � arrivato anche se cambia istruzione)
  numCombinazioni = 0
  
  MadrdM_Incapsulatore_OF.isMoltIstr = False
  If UBound(MoltTabDec) > 1 Then
    For i = 1 To UBound(MoltTabDec)
     If i = 1 Then
     'IRIS-DB: solite pezze per l'eccessiva duplicazione delle funzioni (EXEC duplicato da CBLTDLI, assurdo)
      If MoltTabDec(i).istr <> "" Then
        xIstr = MoltTabDec(i).istr
      End If
     ElseIf MoltTabDec(i).istr <> xIstr Then
      Exit For
     End If
    Next
    If Not i > UBound(MoltTabDec) Then
      MadrdM_Incapsulatore_OF.isMoltIstr = True
    End If
  End If
  
  Dim xdb As String
  MadrdM_Incapsulatore_OF.isMoltPcb = False
  If UBound(MoltTabDec) > 1 Then
    For i = 1 To UBound(MoltTabDec)
     If i = 1 Then
      xdb = MoltTabDec(i).dbdname
     ElseIf MoltTabDec(i).dbdname <> xdb Then
      Exit For
     End If
    Next
    If Not i > UBound(MoltTabDec) Then
      MadrdM_Incapsulatore_OF.isMoltPcb = True
    End If
  End If
  If MadrdM_Incapsulatore_OF.isMoltPcb Then
    Dim listPcb() As String
    Dim listPcbChecs() As Integer
    FillListPcb idpgm, pIdOggetto, pRiga, listPcb, listPcbChecs, UBound(MoltTabDec)
  End If
  
  For i = 1 To UBound(MoltTabDec)
    numCombinazioni = numCombinazioni + 1
     'IRIS-DB: solite pezze per l'eccessiva duplicazione delle funzioni (EXEC duplicato da CBLTDLI, assurdo)
    If MoltTabDec(i).istr <> "" Then
      xIstr = MoltTabDec(i).istr '  TO DO ricavare istruzione da decodifica
    End If
    xdb = MoltTabDec(i).dbdname
    TipIstr = Left(xIstr, 2)
    If TabIstr.isGsam Then
      TipIstr = "GS"
    ElseIf xIstr = "GU" Or xIstr = "GHU" Then
      TipIstr = "GU"
    ElseIf xIstr = "ISRT" Then
      TipIstr = "UP"
    ElseIf xIstr = "DLET" Then
      TipIstr = "UP"
    ElseIf xIstr = "REPL" Then
      TipIstr = "UP"
    ElseIf xIstr = "GNP" Or xIstr = "GHNP" Then
      TipIstr = "GP"
    ElseIf xIstr = "GN" Or xIstr = "GHN" Then
      TipIstr = "GN"
    ElseIf xIstr = "POS" Then 'IRIS-DB
      TipIstr = "PS" 'IRIS-DB
    ElseIf xIstr = "PCB" Then
      wUtil = True
      TipIstr = "SH"
    ElseIf xIstr = "TERM" Then
      wUtil = True
      TipIstr = "TM"
    ElseIf xIstr = "CHKP" Then
      wUtil = True
      TipIstr = "CP"
    ElseIf xIstr = "QRY" Then
      wUtil = True
      TipIstr = "QY"
    ElseIf xIstr = "SYNC" Or xIstr = "SYMCHKP" Then
      wUtil = True
      TipIstr = "SY"
    ElseIf xIstr = "XRST" Then
      wUtil = True
      TipIstr = "XR"
    ElseIf xIstr = "SCHED" Then
      wUtil = True
      TipIstr = "SC"
    Else
      'm_fun.WriteLog "Incapsulatore - Costruisci Operazione " & vbCrLf & "Istruzione non riconosciuta " & xIstr, "DR - Incapsulatore"
    End If
    
    gbIstr = xIstr
    'riapplico il calcolo del NomeRout per ogni Molteplicit� di istruzione
    If UBound(TabIstr.DBD) > 0 Then
      ' Mauro 05/06/2008 : Se � un DBD Index, il nome routine lo devo costruire con il DBD Fisico
      NomeDbD = TabIstr.DBD(1).nome
      Set rsDBD = m_fun.Open_Recordset("select idoggetto from Bs_Oggetti where " & _
                                       "idoggetto = " & TabIstr.DBD(1).Id & " and tipo = 'DBD' and tipo_dbd = 'IND'")
      If rsDBD.RecordCount Then
        NomeDbD = Restituisci_DBD_Da_DBDIndex(rsDBD!IdOggetto)
      End If
      rsDBD.Close
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
        If InStr(NomeDbD, "RKKUNL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKLUNL", xIstr, SwTp)
        ElseIf InStr(NomeDbD, "RKGIRL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKOIRL", xIstr, SwTp)
        Else
          nomeRoutine = m_fun.Genera_Nome_Routine(NomeDbD, xIstr, SwTp)
        End If
      Else
        'nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, xIstr, SwTp)
        nomeRoutine = m_fun.Genera_Nome_Routine(NomeDbD, xIstr, SwTp)
        'aggiornalistaDB (TabIstr.Dbd(1).nome)
      End If
    Else
      nomeRoutine = m_fun.Genera_Nome_Routine("", xIstr, SwTp)
    End If
    If UBound(TabDec) > 0 Then 'AC 04/08/10
      If Not TabDec(j).Routine = "" Then
        nomeRoutine = TabDec(j).Routine
      End If
    End If
    'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    'stefano: ho dovuta commentarla perch� creava delle istruzioni non valide (AND senza IF) quando
    ' c'� molteplicit� di squalificata insieme alla molteplicit� di funzione; fondamentalmente la molteplicit�
    ' di funzione viene gestita sotto quella di squalificata; dovrebbe essere invertita o creato un vettore per ogni
    ' occorrenza di funzione.
    'If TipIstr <> "GU" And TipIstr <> "GN" And TipIstr <> "GP" Then QualAndUnqual = False
    
    combinazioni = True
    IstrIncaps = ""
    Dcombination = False
    If Not MadrdM_Incapsulatore_OF.isMoltPcb Then
      xdb = TabDec(j).nDb ' in questo modo seconda parte sempre verificata
    End If
    If Not MadrdM_Incapsulatore_OF.isMoltIstr Then
      xIstr = TabDec(j).istr ' in questo modo prima parte sempre verificata
    End If
    Do While (TabDec(j).istr = xIstr And TabDec(j).nDb = xdb)  '<MOLTPCB>  aggiunta condizione in and
      wIstr = TabDec(j).Codifica
      numIstr = CInt(Right(wIstr, 6)) 'IRIS-DB
      codFunc = TabDec(j).istr 'IRIS-DB
      If codFunc = "SCHE" Then codFunc = "SCHD"
      multiOpGen = False
      'AC 02/11/06
      ReDim CombPLevel(UBound(TabIstr.BlkIstr))
      ' alberto, sulle istruzioni OPEN e CLSE dava errore per cui l'ho cambiata..
      If UBound(TabIstr.BlkIstr) > 0 Then
        ReDim CombPLevel_s(UBound(TabIstr.BlkIstr) - 1)
      End If
      IstrIncaps = ""
     '''''' <MOLTPCB> nuova versione
'     If UBound(MoltTabDec) > 1 Then  ' ho molt su istruzione
'        varIstr = Restituisci_varFunz(pIdOggetto, pRiga)  ' � la variabile che contiene istruzione
'        combIstr = "'" & xIstr & "'"
'     End If
     If MadrdM_Incapsulatore_OF.isMoltIstr And Not MadrdM_Incapsulatore_OF.isMoltPcb Then
      varIstr = Restituisci_varFunz(pIdOggetto, pRiga)  ' � la variabile che contiene istruzione
      combIstr = "'" & xIstr & "'"
     ElseIf Not MadrdM_Incapsulatore_OF.isMoltIstr And MadrdM_Incapsulatore_OF.isMoltPcb Then
      'varIstr = Restituisci_varPcb(pIdOggetto, pRiga)  ' � la variabile che contiene pcb
      varIstr = "LNKDB_NUMPCB"  ' � la variabile che contiene pcb
      combIstr = Restituisci_numPcb(idpgm, pIdOggetto, pRiga, xdb, listPcb, listPcbChecs) '  numero pcb
      If combIstr = "" Then
        AggLog "[Object: " & pIdOggetto & ",Row: " & pRiga & "] Errors in pcb molteplicity managing ('CostruisciOperazioneTemplate').", MadrdF_Incapsulatore.RTErr
      End If
     ElseIf MadrdM_Incapsulatore_OF.isMoltIstr And MadrdM_Incapsulatore_OF.isMoltPcb Then
      varIstr = Restituisci_varFunz(pIdOggetto, pRiga)
      combIstr = "'" & xIstr & "' AND LNKDB_NUMPCB = " & Restituisci_numPcb(idpgm, pIdOggetto, pRiga, xdb, listPcb, listPcbChecs)
      'combIstr = "'" & xIstr & "' AND " & Restituisci_varPcb(pIdOggetto, pRiga) & " = " & Restituisci_numPcb(idPgm, pIdOggetto, pRiga, xdb, listPcb, listPcbChecs)
      If Right(combIstr, 1) = "=" Then
        AggLog "[Object: " & pIdOggetto & ",Row: " & pRiga & "] Errors in pcb molteplicity managing ('CostruisciOperazioneTemplate').", MadrdF_Incapsulatore.RTErr
      End If
     End If
     
      For k = 1 To UBound(TabIstr.BlkIstr)   ' ciclo su livelli
        'numcombinazioni = numcombinazioni + 1
        ComboPLevelAppo = ComboPLevelTemp
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 28-07-06
        ' OK: REPL CON SSA QUALIFICATA DA RETURN CODE "AJ"
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 19-07-07
        ' REPL CON PATH-CALL (C.C. "D")!
        'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If UBound(MoltTabDec(i).BlkIstr) > 0 Then
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
            If Not TabIstr.BlkIstr(k).idsegm = -1 Then
              If MoltTabDec(i).BlkIstr(k).MoltSegment Then  '
                multiOpGen = True
                combSeg = "'" & TabDec(j).Liv(k).Segm & "'"  ' � il valore che ho nella decodifica identificata da indice j
                'varseg = buildSubstringFromType(TabIstr.BlkIstr(K).ssaName, "1", CStr(Len(combseg) - 2), TipoFile, "seg") '"8"
                'AC 06/03/08 se ho molteplicit� in segment ho nome della variabile
                varSeg = TabIstr.BlkIstr(k).nomeVarSeg
                moltLevelesimo(k) = True
              Else
                varSeg = ""
              End If
              aggiornalistaSeg TabDec(j).Liv(k).Segm
            End If
          Else
            If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(TabIstr.BlkIstr) Then
              If Not TabIstr.BlkIstr(k).idsegm = -1 Then
                If MoltTabDec(i).BlkIstr(k).MoltSegment Then
                  multiOpGen = True
                  combSeg = "'" & TabDec(j).Liv(k).Segm & "'" ' � il valore che ho nella decodifica identificata da indice j
                  'varseg = buildSubstringFromType(TabIstr.BlkIstr(K).ssaName, "1", CStr(Len(combseg) - 2), TipoFile, "SSA") '"8"
                  'AC 06/03/08 se ho molteplicit� in segment ho nome della variabile
                  varSeg = TabIstr.BlkIstr(k).nomeVarSeg
                  moltLevelesimo(k) = True
                Else
                  varSeg = ""
                End If
                aggiornalistaSeg TabDec(j).Liv(k).Segm
              End If
            End If
          End If
        End If
        'AC 10/04/2007 spostata per non sommare a wopes la <ccode> del wope
        If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
          For ks = 1 To k - 1
            CombPLevel_s(ks) = CombPLevel(ks)
          Next ks
          ' allineiamo le stringhe
        End If
                
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
          If MoltTabDec(i).BlkIstr(k).MoltCodCom Then
            multiOpGen = True
            'varcmd = TabIstr.BlkIstr(K).ssaName & "(9:" & Len(TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K))) & ")"
            combCmd = "'" & getCCodefromDecCCode(TabDec(j).Liv(k).ccode, idpgm, pIdOggetto, pRiga, k) & "'"
            If combCmd = "''" Then
              combCmd = "' '"
            End If
            varCmd = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, "9", Len(combCmd) - 2, TipoFile, "SSA", pIdOggetto)
            'If bolMultipleCcod Then
              'moltlevelesimo(K) = True
            'End If
            bolMultipleCcod = True
            moltLevelesimo(k) = True
          Else
            varCmd = ""
            bolMultipleCcod = False
          End If
        End If
        '*********************************
                      
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' QUALIFICAZIONE:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' AC se entrambe le versione wOpe � quella qualificata e wOpeS quella
        ' squalificata, altrimenti se unica sempre in wOpe
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then   'IRIS-DB
         If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
          If bolMultipleCcod Then
            If Not (InStr(combCmd, "-") > 0 Or InStr(combCmd, " ") > 0) Then
              For z1 = 1 To UBound(CCodeSqual)
                If CCodeSqual(z1) = combCmd Then
                  Exit For
                End If
              Next z1
              If z1 > UBound(CCodeSqual) Then
                ReDim CCodeSqual(UBound(CCodeSqual) + 1)
                CCodeSqual(UBound(CCodeSqual)) = combCmd
                Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVEL"
                ComboPLevelAppo = Rtb.text
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
                ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
                If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
                Else
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
                End If
                If QualAndUnqual Then
                  ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
                End If
                wOpeS = wOpe ' attacco anche il pezzo relativo a ccode su ultimo livello
                ReDim CombPLevel_s(k)
                CombPLevel_s(k) = ComboPLevelAppo
              End If
            End If
          End If
          'parte SQUALIFICATA
          ' la wOpeS si ferma qui, wOpeS e wOpe differiscono solo per questo livello
           
          'parte QUALIFICATA
          wFlagBoolean = False
          ' determino se su livelli precedenti ho molteplicit� operatore
             If k > 1 Then
              For ij = 1 To k - 1
                If moltLevelesimo(ij) = True Then
                 moltLevel = True
                 Exit For
                End If
              Next ij
             End If
             ' ------------CICLO SU OPERATORI LOGICI---------------
             bolMoltOpLogic = False
             bolmoltBOL = False
             ultimoOperatore = False
             ReDim MoltOpLogic(UBound(TabIstr.BlkIstr(k).KeyDef))
             If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(TabIstr.BlkIstr(k).KeyDef) - 1)
             End If
             ReDim CombPLevelOper(UBound(TabIstr.BlkIstr(k).KeyDef))
             For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
               'AC 12/07
               ' ripulisco la maschera del template COMBOPLEVELOPE
               ComboPLevelAppoOper = ComboPLevelTempOper
               ComboPLevelAppo = ComboPLevelTemp
               If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                 Operatore = TabDec(j).Liv(k).Chiave(K1).oper ' operatore associato a decodifica j
                 If MoltTabDec(i).BlkIstr(k).MoltKeyDef(K1).MoltKey Then
                  multiOpGen = True
                  varOp = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                  ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                  If TipoFile = "PLI" Then
                    combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "3"
                  Else
                    combOp = "REV" & Operatore & "1" & vbCrLf _
                    & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                    & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                  End If
                  moltLevelesimo(k) = True
                  MoltOpLogic(K1) = True
                 Else
                    varOp = ""
                 End If
                 If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                     OperatoreLogicoSimb = TabDec(j).Liv(k).Chiave(K1).Bool
                     OperatoreLogico = detranscode_booleano(OperatoreLogicoSimb, idpgm, pIdOggetto, pRiga, k, K1)
                     If multiOp Then
                      varLogicop = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                      'comblogicop = "'" & OperatoreLogicoSimb & "'"
                      combLogicop = "'" & OperatoreLogico & "'"
                      moltLevelesimo(k) = True
                      moltBOL(K1) = True
                     Else
                      varLogicop = ""
                     End If
                     If Not OperatoreLogico = ")" Then
                       wFlagBoolean = True
                     Else
                       ultimoOperatore = True
                     End If
                 Else
                  varLogicop = ""
                 End If
                'AC 03/07/06
                'ultimo
                  
                bolMoltOpLogic = False
                bolmoltBOL = False
                If K1 > 1 Then
                 For ij = 1 To K1 - 1
                   If MoltOpLogic(ij) = True Then
                     bolMoltOpLogic = True
                     Exit For
                   End If
                 Next
                 For ij = 1 To K1 - 1
                   If moltBOL(ij) = True Then
                     bolmoltBOL = True
                     Exit For
                   End If
                 Next
                End If
                '---------- molteplicit� operatore --------
                If Not varOp = "" Then
                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", "(" & varOp)
                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp & " )")
                 
'                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", varOp)
'                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp)

                 'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                 If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                 Else
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                 End If
'                 If QualAndUnqual Then
'                   ComboPLevelAppoOper = Replace(ComboPLevelAppo, "<I%IND-3>", "")
'                 End If
'                 CombPLevelOper(k1) = ComboPLevelAppoOper
'                Else
'                 CombPLevelOper(k1) = ComboPLevelAppoOper
                 End If
                ' booleano
                '---------- molteplicit� booleana --------
                If Not varLogicop = "" Then
                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", varLogicop)
                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop)
                 'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                 If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                 Else
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                 End If
                End If
                
                If QualAndUnqual Then
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                End If
                CombPLevelOper(K1) = ComboPLevelAppoOper
                
             '-------------------------------------------
               End If
               ' nel caso di parentesi sull'operatore logico interrompo il ciclo
               If ultimoOperatore Then Exit For
             Next K1
             
             '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
             
             Rtb.text = ComboPLevelAppo
             For jj = 1 To UBound(CombPLevelOper)
              MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
             Next
             ComboPLevelAppo = Rtb.text
             
             If Not varCmd = "" And bolMultipleCcod Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
              If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
            If Not varSeg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varSeg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg)
              If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
         Else  ' o la versione squalificata o quella squalificata
         
          ' AC il campo parentesi e la trasformazione in stringa del booleano Qualificazione
          If TabIstr.BlkIstr(k).Parentesi <> "(" Then ' Or Trim(TabIstr.BlkIstr(k).SsaName) = "<none>" Then
             OnlySqual = True
             ReDim CombPLevelOper(0)
          Else   ' Qualificata
             OnlyQual = True
             wFlagBoolean = False
             If k > 1 Then
              For ij = 1 To k - 1
               If moltLevelesimo(ij) = True Then
                 moltLevel = True
                 Exit For
               End If
              Next
             End If
             ' ----- CICLO OPERATORI LOGICI
             bolmoltBOL = False
             ultimoOperatore = False
             bolMoltOpLogic = False
             ReDim MoltOpLogic(UBound(TabIstr.BlkIstr(k).KeyDef))
             If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(TabIstr.BlkIstr(k).KeyDef) - 1)
             End If
             ReDim CombPLevelOper(UBound(TabIstr.BlkIstr(k).KeyDef))
             For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
               ComboPLevelAppoOper = ComboPLevelTempOper
               If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) And K1 <= UBound(TabDec(j).Liv(k).Chiave) And K1 <= UBound(MoltTabDec(i).BlkIstr(k).MoltKeyDef) Then
                 Operatore = TabDec(j).Liv(k).Chiave(K1).oper ' operatore associato a decodifica j
                 If MoltTabDec(i).BlkIstr(k).MoltKeyDef(K1).MoltKey Then   ' AC 020807 And Not TipIstr = "UP"
                  multiOpGen = True
                  'varop = TabIstr.BlkIstr(K).ssaName & "(" & Int(TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
                  varOp = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                  ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                  If TipoFile = "PLI" Then
                    combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "3"
                  Else
                    combOp = "REV" & Operatore & "1" & vbCrLf _
                    & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                    & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                  End If
                  moltLevelesimo(k) = True
                  MoltOpLogic(K1) = True
                 Else
                    varOp = ""
                 End If
                 If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                    OperatoreLogicoSimb = TabDec(j).Liv(k).Chiave(K1).Bool
                    'OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                     OperatoreLogico = detranscode_booleano(OperatoreLogicoSimb, idpgm, pIdOggetto, pRiga, k, K1)
                    'If multiOp Then    ' AC 020807 And Not TipIstr = "UP"
                    If MoltTabDec(i).BlkIstr(k).MoltKeyDef(K1).MoltOpLogic Then
                      varLogicop = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                      combLogicop = "'" & OperatoreLogico & "'"
                      moltLevelesimo(k) = True
                      moltBOL(K1) = True
                    Else
                      varLogicop = ""
                    End If
                    If Not OperatoreLogico = ")" Then
                      wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                      wFlagBoolean = True
                    Else
                      ultimoOperatore = True
                    End If
                 Else
                  varLogicop = ""
                 End If
                
                 
                  '------------------------------------------------------
                  bolMoltOpLogic = False
                  bolmoltBOL = False
                  If K1 > 1 Then
                   For ij = 1 To K1 - 1
                     If MoltOpLogic(ij) = True Then
                       bolMoltOpLogic = True
                       Exit For
                     End If
                   Next
                   For ij = 1 To K1 - 1
                    If moltBOL(ij) = True Then
                      bolmoltBOL = True
                      Exit For
                    End If
                    Next
                  End If
                  If Not varOp = "" Then
                    varOp = ""
                    ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", "(" & varOp)
                    ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp & " )")
'                    ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", varOp)
'                    ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp)
                    If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                    Else
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                    End If
'                    If QualAndUnqual Then
'                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
'                    End If
'                    CombPLevelOper(k1) = ComboPLevelAppoOper
'                  Else
'                      CombPLevelOper(k1) = ComboPLevelAppoOper
                  End If
                  '---------- molteplicit� booleana --------
                  If Not varLogicop = "" Then
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", varLogicop)
                   ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop)
                   'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                   If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                   Else
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                   End If
                  End If
                  
                  If QualAndUnqual Then
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                  End If
                  CombPLevelOper(K1) = ComboPLevelAppoOper
          '-------------------------------------------------------
               End If
                If ultimoOperatore Then Exit For
             Next K1
          End If '-- squal o qual
          
          
          '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
             
          Rtb.text = ComboPLevelAppo
          For jj = 1 To UBound(CombPLevelOper)
           MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
          Next
          ComboPLevelAppo = Rtb.text
          
          
          If Not varCmd = "" And bolMultipleCcod Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
            ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
            If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
              ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
            Else
              ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
            End If
            If QualAndUnqual Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
            End If
            CombPLevel(k) = ComboPLevelAppo
          Else
              CombPLevel(k) = ComboPLevelAppo
          End If
          If Not varSeg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varSeg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg)
              If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
        End If   '-- una sola versione o due versioni
       End If
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     Next k   ' FINE CICLO LIVELLI
     
'    If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
'     combinazioni = AggiornaCombinazione(maxLogicOp)
'    Else
'     combinazioni = False
'    End If
    moltLevel = False
    nomeRoutCompleto = Genera_Counter(nomeRoutine)
    ' nomeRoutCompleto = Check_Dyn_PCB(nomeRoutine, idpgm, IdOggetto, Riga) 'IRIS-DB: check the dynamic PCB
     
     ' livello COMBQUAL -- costruisce elemento array combqual
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL_EXEC"
      ' nel caso di linguaggi con then (vedi PLI), sui template gestiamo la riga dello then con il tag <then>
      ' negli altri casi l'if sotto � ininfluente
      For ij = 1 To UBound(moltLevelesimo)
        If moltLevelesimo(ij) = True Then
          moltLevThen = True
          Exit For
        End If
      Next
      If QualAndUnqual Or UBound(MoltTabDec) > 1 Or moltLevThen Then
        MadrdM_GenRout.changeTag Rtb, "<then>", ""
      End If
      If QualAndUnqual Then
        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
      Else
        ' gestisco indentazione move
        If multiOpGen Or UBound(MoltTabDec) > 1 Then ' ne lascio solo una
          MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        End If
      End If
      
      'if or end per l'istruzione e relativo end if di chiusura
      If QualAndUnqual Then
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
      Else
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 2)
        ' end if
        ' AC 18/06/07  nel caso che le molteplicit� siano ignorate (vedi ISRT)
        ' non devo lasciare l'end if finale
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then   'IRIS-DB
          If multiOpGen Or UBound(MoltTabDec) > 1 Then
            MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
          Else
             MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
          End If
        ElseIf UBound(MoltTabDec) > 1 Then ' potremmo avere molteplicit� istr con istr diverse da GU,GN e GP
          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
        Else
          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
        End If
      End If
      
      MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
      MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
      'If InStr(nomeRoutCompleto, "1:8") Then 'IRIS-DB
      '  MadrdM_GenRout.changeTag Rtb, "'<PROGRAMMA>'", nomeRoutCompleto 'IRIS-DB
      'Else 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
      'End If 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
      MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", codFunc 'IRIS-DB
      If codFunc <> "PCB" And codFunc <> "SCHE" Then 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", numIstr 'IRIS-DB
      End If 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<CODICEOPERB>", Replace(wIstr, ".", "")
      'AC 12/05/10 ______
      If MadrdM_Incapsulatore_OF.isMoltIstr Or MadrdM_Incapsulatore_OF.isMoltPcb Then
        If Len(nomeRoutine) < 8 And InStr(m_fun.FnNomeDB, "prj-GRZ") = 0 Then
          MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & Space(8 - Len(nomeRoutine))
        Else
          MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine
        End If
      End If
      '__________________
      
      If EmbeddedRoutine Then
        MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
      Else
        MadrdM_GenRout.changeTag Rtb, "<standard>", ""
      End If
      
      For k = 1 To UBound(CombPLevel)
        MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
      Next
      
      appocomb = Rtb.text
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFQUAL"
      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFQUAL"
      MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
      ReDim Preserve CombQual(jk)
      CombQual(jk) = Rtb.text
      
      If QualAndUnqual And checksqual Then ' due parti, la seconda � squalificata
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL_EXEC"
        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
        'If InStr(nomeRoutCompleto, "1:8") Then 'IRIS-DB
        '  MadrdM_GenRout.changeTag Rtb, "'<PROGRAMMA>'", nomeRoutCompleto 'IRIS-DB
        'Else 'IRIS-DB
          MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
        'End If 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstrS
        'AC 13/05/10 _________________________________
        If MadrdM_Incapsulatore_OF.isMoltIstr Or MadrdM_Incapsulatore_OF.isMoltPcb Then
            If Len(nomeRoutine) < 8 Then
                xprogramm = Space(8 - Len(nomeRoutine))
            Else
                xprogramm = ""
                If InStr(m_fun.FnNomeDB, "prj-GRZ") Then
                   nomeRoutine = IIf(Len(nomeRoutine) > 7, Left(nomeRoutine, 7), nomeRoutine)
                End If
            End If
            MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & xprogramm
        End If
        
        wIstrsOld = wIstrS
        If EmbeddedRoutine Then
          MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
        Else
          MadrdM_GenRout.changeTag Rtb, "<standard>", ""
        End If
        For k = 1 To UBound(CombPLevel_s)
          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel_s(k), True
        Next
        appocomb = Rtb.text
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFSQUAL"
        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
        ReDim Preserve CombSqual(UBound(CombSqual) + 1)
        CombSqual(UBound(CombSqual)) = Rtb.text
      ElseIf Not QualAndUnqual Then
'       ' anche nella parte squalificata metto la stessa cosa della qualificata
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
'        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
'        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
'        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
'        For k = 1 To UBound(CombPLevel_s)
'          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
'        Next
'        appocomb = Rtb.text
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
'        ReDim Preserve CombSqual(jk)
'        CombSqual(jk) = Rtb.text
      End If
      jk = jk + 1
      '**********
      j = j + 1 'AGGIORNAMNETO INDICE DECODIFICA
      If j > UBound(TabDec) Then
        Exit Do
      End If
    Loop
   Next
  
   'stefanopul
   'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\MAINCOMB"
   Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\MAINCOMB"
   'AC 20/03/09
  Dim dataType As String
  If Left(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName, 1) = "'" And _
     Right(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName, 1) = "'" Then
    dataType = "X"
  Else
    getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName), pIdOggetto, 0, dataType, "", -1
  End If
   If TipoFile = "PLI" Then
    MadrdM_GenRout.changeTag Rtb, "<SSA-INSPECT>", "SUBSTR(" & IIf(dataType = "X", "", "IRIS_") & _
                      MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName & ",9," & _
                      Len(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Search, 1, bolMultipleCcod)) + 1 & ")"
   Else
    MadrdM_GenRout.changeTag Rtb, "<SSA-INSPECT>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).ssaName & "(9:" & Len(TokenIesimoCC(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Search, 1, bolMultipleCcod)) + 1 & ")"
   End If
   For i = 1 To UBound(CombQual)
     MadrdM_GenRout.changeTag Rtb, "<<IFQUAL(i)>>", CombQual(i)
   Next
   If QualAndUnqual Then
    For i = 1 To UBound(CombSqual)
      MadrdM_GenRout.changeTag Rtb, "<<IFSQUAL(i)>>", CombSqual(i)
    Next
    MadrdM_GenRout.changeTag Rtb, "<condsqual>", "" ' altrimenti istr corrispondenti eliminate
   End If
   appoFinalString = Rtb.text
  
   While InStr(appoFinalString, vbCrLf) > 0
     posEndLine = InStr(appoFinalString, vbCrLf)
     insertLine Testo, Left(appoFinalString, posEndLine - 1), pIdOggetto, indentkey
     appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
   Wend
   If Not Trim(appoFinalString) = "" Then
     insertLine Testo, appoFinalString, pIdOggetto, indentkey
   End If
  Close #nFBms
  
  Exit Function
ErrH:
  If err.Number = 67 Then 'IRIS-DB too many files
   err.Clear              'IRIS-DB
   nFBms = FreeFile(1)    'IRIS-DB
   If err.Number = 67 Then 'IRIS-DB
      AggLog "[Object: " & pIdOggetto & "] Too many open files when trying ot load the template: " & templateName, MadrdF_Incapsulatore.RTErr 'IRIS-DB
   Else 'IRIS-DB
      Resume Next 'IRIS-DB
   End If 'IRIS-DB
  End If 'IRIS-DB
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & pIdOggetto & "] Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & pIdOggetto & "] unexpected error on 'CostruisciOperazioneTemplate'.", MadrdF_Incapsulatore.RTErr
  End If
 'Resume Next
End Function
Function CostruisciOperazioneTemplateNoDecode(xIstr As String, Index As Integer, idpgm As Long, pIdOggetto As Long, pRiga As Long, ordPcb As Long)
  Dim tb, tb1 As Recordset
  Dim wOpe As String, wOpeS As String, TipIstr As String
  Dim k As Long
  Dim xope As String
  Dim K1 As Integer
  Dim wUtil As Boolean, wFlagBoolean As Boolean
  Dim i As Integer, j As Integer
  Dim checkColl As Boolean
  Dim extraIndent As Integer
  Dim maxId As Long
  Dim wIstr, wDecod As String, wIstrS As String, wDecodS As String, pcbDyn As String
  Dim numIstr As Integer 'IRIS-DB
  Dim codFunc As String 'IRIS-DB
  Dim maxPcbDyn As Integer, pcbDynInt As Integer
  Dim wOpeSave As String
  Dim y As Integer
  ' per comporre stringa if dell'incapsulato, versione qual e squal
  Dim IstrIncaps As String, IstrSIncaps As String, StrAnd As String
  ' contiene la molteplicit� di istruzioni, attinge dalla tabella Istruzioni_Clone
  Dim cloniIstruzione() As String
  Dim cloniSegmenti() As String
  Dim combinazioni As Boolean
  Dim maxLogicOp As Integer, maxBoolean As Integer
  Dim Operatore As String
  Dim StringToClear As String
  Dim ifAnd As String
  Dim nomeRoutCompleto As String
  Dim templateName As String  'SQ - per la gestione degli errori
  Dim xprogramm As String, xdb As String
  Dim s() As String 'IRIS-DB
  Dim tb2 As Recordset 'IRIS-DB
  On Error GoTo ErrH
  'AC 02/11/06
  
  Dim varOp As String, combOp As String, varIstr As String, combIstr As String
  Dim varop_s As String, combop_s As String, ks As Integer
  Dim varCmd As String, combCmd As String, varSeg As String, combSeg As String
  Dim varcmd_s As String, combcmd_s As String
  
  Dim CombPLevel() As String, CombPLevel_s() As String, CombSqual() As String, CombQual() As String, wIstrsOld As String
  
  ' nuove varibili operatore
  Dim CombPLevelOper() As String, ComboPLevelTempOper As String, ComboPLevelAppoOper As String
  Dim bolMoltOpLogic As Boolean, jj As Integer
  
  Dim ComboPLevelTemp As String, ComboPLevelAppo As String, nFBms As Integer, wRecIn As String, jk As Integer
  Dim Rtb  As RichTextBox, appocomb As String, appoFinalString As String, checksqual As Boolean, posEndLine As String
  ' tango traccia di una qualsiasi molteplicit� sui livelli
  Dim moltLevel As Boolean, moltLevelesimo() As Boolean, ij As Integer, moltLevThen As Boolean
 
  'AC 19/06/07  arriva la booleana
  Dim OperatoreLogico As String, ultimoOperatore As Boolean, varLogicop As String, combLogicop As String
  Dim OperatoreLogicoSimb As String, moltBOL() As Boolean, bolmoltBOL As Boolean
  Dim bolMultipleCcod As Boolean
  Dim CCodeSqual() As String, z1 As Integer, numCombinazioni As Integer
  
  ReDim CombQual(0) 'IRIS-DB
  Dim IRISMultBol As Boolean
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  
  nFBms = FreeFile
  
   multiOpGen = False
  
  '---- nuovo template
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVELOPE"
  Open templateName For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTempOper = "" Then
      ComboPLevelTempOper = ComboPLevelTempOper & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTempOper = ComboPLevelTempOper & wRecIn
  Wend
  Close #nFBms
  '-------------------------------
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVEL"
  Open templateName For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTemp = "" Then
      ComboPLevelTemp = ComboPLevelTemp & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTemp = ComboPLevelTemp & wRecIn
  Wend
  
  jk = 1
  ReDim moltLevelesimo(UBound(TabIstr.BlkIstr))
  ReDim CCodeSqual(0)
  
  'NOTA  squalificata deve essere considerata su tutti i livelli 'IRIS-DB: condivido, per ora la considera solo sull'ultimo livello...
  '***** determiniamo se deve essere generata
  '***** sia la versione qualificata che squalificata
  Set tb = m_fun.Open_Recordset("select * from PsDLI_IstrDett_Liv where IdOggetto =" & pIdOggetto & " and Idpgm = " & idpgm & " and Riga = " & pRiga & " and Livello = " & UBound(TabIstr.BlkIstr))
  If Not tb.EOF Then
    QualAndUnqual = tb!QualAndUnqual
  Else
    QualAndUnqual = False
  End If
  tb.Close
  
 
  
  ' Ciclo esterno su istruzione
  
  
  'TO DO   CICLO COMBINAZIONI su struttura delle decodifiche
  ' si parte da istruzione
  numCombinazioni = 0
  j = 1 '(indice di TabDec si incrementa sempre da dove � arrivato anche se cambia istruzione)
  
  MadrdM_Incapsulatore_OF.isMoltIstr = False
  If UBound(MoltTabDec) > 1 Then
    For i = 1 To UBound(MoltTabDec)
     If i = 1 Then
      xIstr = MoltTabDec(i).istr
     ElseIf MoltTabDec(i).istr <> xIstr Then
      Exit For
     End If
    Next
    If Not i > UBound(MoltTabDec) Then
      MadrdM_Incapsulatore_OF.isMoltIstr = True
    End If
  End If
  
'IRIS-DB: lo chiama isMoltPcb, ma in realt� corrisponde a Multi-DBD, per cui lo uso in quel modo
  MadrdM_Incapsulatore_OF.isMoltPcb = False
  If UBound(MoltTabDec) > 1 Then
    For i = 1 To UBound(MoltTabDec)
     If i = 1 Then
      xdb = MoltTabDec(i).dbdname
     ElseIf MoltTabDec(i).dbdname <> xdb Then
      Exit For
     End If
    Next
    If Not i > UBound(MoltTabDec) Then
      MadrdM_Incapsulatore_OF.isMoltPcb = True
    End If
  End If
  If MadrdM_Incapsulatore_OF.isMoltPcb Then
    Dim listPcb() As String
    Dim listPcbChecs() As Integer
    FillListPcb idpgm, pIdOggetto, pRiga, listPcb, listPcbChecs, UBound(MoltTabDec)
  End If
  
  ReDim Preserve CombSqual(0)
  
  For i = 1 To UBound(MoltTabDec)
    numCombinazioni = numCombinazioni + 1
    xIstr = MoltTabDec(i).istr '  TO DO ricavare istruzione da decodifica
    xdb = MoltTabDec(i).dbdname
    If xIstr = "" Then ' non ho molt su istruzione allora la prendo da TabDec
      xIstr = TabDec(1).istr
    End If
    TipIstr = Left(xIstr, 2)
    If TabIstr.isGsam Then
       TipIstr = "GS"
    ElseIf xIstr = "GU" Or xIstr = "GHU" Then
       TipIstr = "GU"
    ElseIf xIstr = "POS" Then 'IRIS-DB
       TipIstr = "PS" 'IRIS-DB
    ElseIf xIstr = "ISRT" Then
      TipIstr = "UP"
    ElseIf xIstr = "DLET" Then
       TipIstr = "UP"
    ElseIf xIstr = "REPL" Then
       TipIstr = "UP"
    ElseIf xIstr = "GNP" Or xIstr = "GHNP" Then
       TipIstr = "GP"
    ElseIf xIstr = "GN" Or xIstr = "GHN" Then
       TipIstr = "GN"
    ElseIf xIstr = "PCB" Then
       wUtil = True
       TipIstr = "SH"
    ElseIf xIstr = "TERM" Then
       wUtil = True
       TipIstr = "TM"
    ElseIf xIstr = "CHKP" Then
       wUtil = True
       TipIstr = "CP"
    ElseIf xIstr = "QRY" Then
       wUtil = True
       TipIstr = "QY"
    ElseIf xIstr = "SYNC" Or xIstr = "SYMCHKP" Then
       wUtil = True
       TipIstr = "SY"
    ElseIf xIstr = "XRST" Then
       wUtil = True
       TipIstr = "XR"
    ElseIf xIstr = "SCHED" Then
       wUtil = True
       TipIstr = "SC"
    Else
      'm_fun.WriteLog "Incapsulatore - Costruisci Operazione " & vbCrLf & "Istruzione non riconosciuta " & xIstr, "DR - Incapsulatore"
    End If
    gbIstr = xIstr
  
    'riapplico il calcolo del NomeRout per ogni Molteplicit� di istruzione
    If UBound(TabIstr.DBD) > 0 Then
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
        If InStr(TabIstr.DBD(1).nome, "RKKUNL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKLUNL", xIstr, SwTp)
        ElseIf InStr(TabIstr.DBD(1).nome, "RKGIRL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKOIRL", xIstr, SwTp)
        Else
          nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, xIstr, SwTp)
        End If
      Else
        nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, xIstr, SwTp)
        'aggiornalistaDB (TabIstr.Dbd(1).nome)
      End If
    Else
      nomeRoutine = m_fun.Genera_Nome_Routine("", xIstr, SwTp)
    End If
    If UBound(TabDec) > 0 Then
      If Not TabDec(j).Routine = "" Then
        nomeRoutine = TabDec(j).Routine
      End If
    End If
   
   'Ciclo sulle decodifiche, cio� sulla struttura TabDecod
    
    If UBound(TabDec) = 0 Then
      Exit Function
    End If
    If Not MadrdM_Incapsulatore_OF.isMoltPcb Then
      xdb = TabDec(j).nDb ' in questo modo seconda parte sempre verificata
    End If
    If Not MadrdM_Incapsulatore_OF.isMoltIstr Then
      xIstr = TabDec(j).istr ' in questo modo prima parte sempre verificata
    End If
    
    Do While (TabDec(j).istr = xIstr And TabDec(j).nDb = xdb)  '<MOLTPCB>  aggiunta condizione in and
    
    'AC 02/11/06
     ReDim CombPLevel(UBound(TabIstr.BlkIstr))
     If UBound(TabIstr.BlkIstr) > 0 Then
      ReDim CombPLevel_s(UBound(TabIstr.BlkIstr) - 1)
     End If
     IstrIncaps = ""
     
     '''''' <MOLTPCB> nuova versione
'     If UBound(MoltTabDec) > 1 Then  ' ho molt su istruzione
'        varIstr = Restituisci_varFunz(pIdOggetto, pRiga)  ' � la variabile che contiene istruzione
'        combIstr = "'" & xIstr & "'"
'     End If
     If MadrdM_Incapsulatore_OF.isMoltIstr And Not MadrdM_Incapsulatore_OF.isMoltPcb Then
      varIstr = Restituisci_varFunz(pIdOggetto, pRiga)  ' � la variabile che contiene istruzione
      combIstr = "'" & xIstr & "'"
     ElseIf Not MadrdM_Incapsulatore_OF.isMoltIstr And MadrdM_Incapsulatore_OF.isMoltPcb Then
     'IRIS-DB gestito il PCB come multi-DBD, perch� tanto non esiste il concetto di multi-PCB in realt�
      'IRIS-DB varIstr = "LNKDB_NUMPCB"  ' � la variabile che contiene pcb
      'IRIS-DB combIstr = Restituisci_numPcb(idPgm, pIdOggetto, pRiga, xdb, listPcb, listPcbChecs) ' numero pcb
      varIstr = Restituisci_varPcb(pIdOggetto, pRiga) & "(1:" & Len(Trim(TabDec(j).nDb)) & ")" ' IRIS-DB
      combIstr = "'" & Trim(TabDec(j).nDb) & "'" 'IRIS-DB
     ElseIf MadrdM_Incapsulatore_OF.isMoltIstr And MadrdM_Incapsulatore_OF.isMoltPcb Then
     'IRIS-DB: da rivedere quando succede
      varIstr = Restituisci_varFunz(pIdOggetto, pRiga)
      combIstr = "'" & xIstr & "' AND LNKDB_NUMPCB = " & Restituisci_numPcb(idpgm, pIdOggetto, pRiga, xdb, listPcb, listPcbChecs)
      'combIstr = "'" & xIstr & "' AND " & Restituisci_varPcb(pIdOggetto, pRiga) & " = " & Restituisci_numPcb(idPgm, pIdOggetto, pRiga, xdb, listPcb, listPcbChecs)
     End If
     
     wIstr = TabDec(j).Codifica
     numIstr = CInt(Right(wIstr, 6)) 'IRIS-DB
     codFunc = TabDec(j).istr 'IRIS-DB
     
     For k = 1 To UBound(TabIstr.BlkIstr)   ' ciclo su livelli
         IRISMultBol = False 'IRIS-DB
           ComboPLevelAppo = ComboPLevelTemp
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 28-07-06
        ' OK: REPL CON SSA QUALIFICATA DA RETURN CODE "AJ"
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 19-07-07
        ' REPL CON PATH-CALL (C.C. "D")!
        If UBound(MoltTabDec(i).BlkIstr) > 0 Then
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
            If Not TabIstr.BlkIstr(k).idsegm = -1 Then
               If MoltTabDec(i).BlkIstr(k).MoltSegment Then  '
                multiOpGen = True
                combSeg = "'" & TabDec(j).Liv(k).Segm & "'"  ' � il valore che ho nella decodifica identificata da indice j
                varSeg = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, "1", CStr(Len(combSeg) - 2), TipoFile, "SSA", pIdOggetto)
                moltLevelesimo(k) = True
               Else
                varSeg = ""
               End If
               aggiornalistaSeg (TabDec(j).Liv(k).Segm)
            End If
          Else
            If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(TabIstr.BlkIstr) Then
              If Not TabIstr.BlkIstr(k).idsegm = -1 Then
                 If MoltTabDec(i).BlkIstr(k).MoltSegment Then
                  multiOpGen = True
                  combSeg = "'" & TabDec(j).Liv(k).Segm & "'" ' � il valore che ho nella decodifica identificata da indice j
                  varSeg = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, "1", CStr(Len(combSeg) - 2), TipoFile, "SSA", pIdOggetto) '"8"
                  moltLevelesimo(k) = True
                 Else
                  varSeg = ""
                 End If
                 aggiornalistaSeg (TabDec(j).Liv(k).Segm)
              End If
            End If
          End If
        End If
        
        'TODO considerare la squalif per tutti i livelli
        'IRIS-DB: importante mancanza, potrebbe causare problemi!!!!
        If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
          For ks = 1 To k - 1
            CombPLevel_s(ks) = CombPLevel(ks)
          Next
        End If
                
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
          If UBound(MoltTabDec(i).BlkIstr) >= k Then
            If MoltTabDec(i).BlkIstr(k).MoltCodCom Then
            'TODO variabile booleana per i coomand code
              multiOpGen = True
              'IRIS-DB combCmd = "'" & getCCodefromDecCCode(TabDec(j).Liv(k).ccode, idPgm, pIdOggetto, pRiga) & "'"
              'IRIS-DB start
              combCmd = getCCodefromDecCCode(TabDec(j).Liv(k).ccode, idpgm, pIdOggetto, pRiga, k) 'IRIS-DB mancava il livello!!!!!
              If Not UBound(TabDec(j).Liv(k).Chiave) > 0 Then
                combCmd = Trim(Replace(combCmd, "-", " "))
                If combCmd = "*" Then
                  combCmd = "'" & " " & "'"
                Else
                  combCmd = "'" & combCmd & "'"
                End If
              Else
                If Trim(combCmd) = "" Then
                  combCmd = "'" & "*-" & "'"
                Else
                  combCmd = "'" & combCmd & "'"
                End If
              End If
              'IRIS-DB end
              
              If combCmd = "''" Then
                  combCmd = "' '"
              End If
              varCmd = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, "9", CStr(Len(combCmd) - 2), TipoFile, "SSA", pIdOggetto)
              'If bolMultipleCcod Then
                'moltlevelesimo(K) = True
              'End If
              bolMultipleCcod = True
              moltLevelesimo(k) = True
            Else
              varCmd = ""
              bolMultipleCcod = False
            End If
          Else
            varCmd = ""
            bolMultipleCcod = False
          End If
        End If
          '*********************************
                      
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' QUALIFICAZIONE:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' AC se entrambe le versione wOpe � quella qualificata e wOpeS quella
        ' squalificata, altrimenti se unica sempre in wOpe
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then   ' IRIS-DB
         If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
          If bolMultipleCcod Then
          'IRIS-DB questa IF non � mai vera visto che sopra costruisce il campo togliendo gli spazi e mettendo i trattini al command code "pulito" che viene dalla decodifica...
          'IRIS-DB l'ho resa falsa io, per cui per ora la escludo
           'IRIS-DB If Not (InStr(combCmd, "-") > 0 Or InStr(combCmd, " ") > 0) Then
            If InStr(combCmd, "-") > 99999 Then
              m_fun.WriteLog "Incaps: Obj(" & pIdOggetto & ") Row( " & pRiga & ") Unexpected command condition! Command code: " & combCmd, "MadrdM_Incapsulatore" 'IRIS-DB per assicurarsi che non si verifichi
              For z1 = 1 To UBound(CCodeSqual)
                If CCodeSqual(z1) = combCmd Then
                  Exit For
                End If
              Next z1
              If z1 > UBound(CCodeSqual) Then
                ReDim CCodeSqual(UBound(CCodeSqual) + 1)
                CCodeSqual(UBound(CCodeSqual)) = combCmd
                Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVEL"
                ComboPLevelAppo = Rtb.text
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
                ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
                If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
                Else
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
                End If
                If QualAndUnqual Then
                  ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
                End If
                wOpeS = wOpe ' attacco anche il pezzo relativo a ccode su ultimo livello
                ReDim CombPLevel_s(k)
                CombPLevel_s(k) = ComboPLevelAppo
              End If
            End If
          End If
          
           
           
           
          '__________________________________________
          'parte QUALIFICATA
          wFlagBoolean = False
          ' determino se su livelli precedenti ho molteplicit� operatore
          
          'TODO queste variabili devo trovarle valorizzate con il nuovo metodo
             If k > 1 Then
              For ij = 1 To k - 1
                If moltLevelesimo(ij) = True Then
                 moltLevel = True
                 Exit For
                End If
              Next
             End If
             ' ------------CICLO SU OPERATORI LOGICI---------------
             bolMoltOpLogic = False
             bolmoltBOL = False
             ultimoOperatore = False
             ReDim MoltOpLogic(UBound(TabIstr.BlkIstr(k).KeyDef))
             If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(TabIstr.BlkIstr(k).KeyDef) - 1)
             End If
             ReDim CombPLevelOper(UBound(TabIstr.BlkIstr(k).KeyDef))
             For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
               'AC 12/07
               ' ripulisco la maschera del template COMBOPLEVELOPE
               ComboPLevelAppoOper = ComboPLevelTempOper
               ComboPLevelAppo = ComboPLevelTemp
               If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) And UBound(TabDec(j).Liv(k).Chiave) >= K1 Then
                 Operatore = TabDec(j).Liv(k).Chiave(K1).oper ' operatore associato a decodifica j
         'IRIS-DB: non capisco perch�!!!        If UBound(MoltTabDec(i).BlkIstr(k).MoltKeyDef) >= K1 Then
          'IRIS-DB come sopra        If MoltTabDec(i).BlkIstr(k).MoltKeyDef(K1).MoltKey Then
          'IRIS-DB: ho capito il perch�, � un problema architetturale, la MoltTabDec contiene un flag per ogni molteplicit�, ma non la carica bene, perch�
          'IRIS-DB  presume che le varie decodifiche siano della stessa lunghezza, ma in presenza di molteplicit� di booleana non � vero
                 If MoltTabDec(i).BlkIstr(k).MoltKeyDef(K1).MoltOpLogic Then
                   multiOpGen = True
                   varOp = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                   ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                   If TipoFile = "PLI" Then
                     combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                     & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                     & "   " & varOp & " = IRIS_" & Operatore & "3"
                   Else
                     combOp = "REV" & Operatore & "1" & vbCrLf _
                     & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                     & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                   End If
                   moltLevelesimo(k) = True
                   MoltOpLogic(K1) = True
                   IRISMultBol = True 'IRIS-DB molto rischioso
          'IRIS-DB        Else
          'IRIS-DB           varOp = ""
          'IRIS-DB        End If
                 End If
                 If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                     OperatoreLogicoSimb = TabDec(j).Liv(k).Chiave(K1).Bool
                     OperatoreLogico = detranscode_booleano(OperatoreLogicoSimb, idpgm, pIdOggetto, pRiga, k, K1)
                     'IRIS-DB If multiOp Then
                     If InStr(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, ";") Then 'IRIS-DB multiOp non impostato mai in questo giro...
                      varLogicop = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                      'comblogicop = "'" & OperatoreLogicoSimb & "'"
                      'IRIS-DB combLogicop = "'" & OperatoreLogico & "'"
                      combLogicop = buildLogicalOpEncaps(OperatoreLogico, TabIstr.BlkIstr(k).ssaName, pIdOggetto, pRiga) 'IRIS-DB
                      moltLevelesimo(k) = True
                      moltBOL(K1) = True
                      IRISMultBol = True 'IRIS-DB
                     Else
                      varLogicop = ""
                     End If
                     If Not OperatoreLogico = ")" Then
                       wFlagBoolean = True
                     Else
                       ultimoOperatore = True
                     End If
                 Else
                  varLogicop = ""
                 End If
                'AC 03/07/06
                'ultimo
                  
                bolMoltOpLogic = False
                bolmoltBOL = False
                If K1 > 1 Then
                 For ij = 1 To K1 - 1
                   If MoltOpLogic(ij) = True Then
                     bolMoltOpLogic = True
                     Exit For
                   End If
                 Next
                 For ij = 1 To K1 - 1
                   If moltBOL(ij) = True Then
                     bolmoltBOL = True
                     Exit For
                   End If
                 Next
                End If
                '---------- molteplicit� operatore --------
                If Not varOp = "" Then
                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", "(" & varOp)
                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp & " )")
                 'ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", varOp)
                 'ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp)
                 
                 'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                 If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                 Else
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                 End If
                End If
                ' booleano
                '---------- molteplicit� booleana --------
                If Not varLogicop = "" Then
                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", varLogicop)
                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop)
                 'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                 If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                 Else
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                 End If
                End If
                
                If QualAndUnqual Then
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                End If
                CombPLevelOper(K1) = ComboPLevelAppoOper
                
             '-------------------------------------------
               Else ' giro squalificata (si cerca sulla chiave ma non c'�)
                 checksqual = True
               End If
               ' nel caso di parentesi sull'operatore logico interrompo il ciclo
               If ultimoOperatore Then Exit For
             Next K1
             
             '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
             
             Rtb.text = ComboPLevelAppo
             For jj = 1 To UBound(CombPLevelOper)
              MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
             Next
             ComboPLevelAppo = Rtb.text
             
             If Not varCmd = "" And bolMultipleCcod Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
              If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
            If Not varSeg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varSeg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg)
              If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
         Else  ' o la versione squalificata o quella squalificata
         
          ' AC il campo parentesi e la trasformazione in stringa del booleano Qualificazione
          If TabIstr.BlkIstr(k).Parentesi <> "(" Then ' Or Trim(TabIstr.BlkIstr(k).SsaName) = "<none>" Then
             OnlySqual = True
             ReDim CombPLevelOper(0)
          Else   ' Qualificata
             OnlyQual = True
             wFlagBoolean = False
             If k > 1 Then
              For ij = 1 To k - 1
               If moltLevelesimo(ij) = True Then
                 moltLevel = True
                 Exit For
               End If
              Next
             End If
             ' ----- CICLO OPERATORI LOGICI
             bolmoltBOL = False
             ultimoOperatore = False
             bolMoltOpLogic = False
             ReDim MoltOpLogic(UBound(TabIstr.BlkIstr(k).KeyDef))
             If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(TabIstr.BlkIstr(k).KeyDef) - 1)
             End If
             ReDim CombPLevelOper(UBound(TabIstr.BlkIstr(k).KeyDef))
             For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
               ComboPLevelAppoOper = ComboPLevelTempOper
               'IRIS-DB If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) And K1 <= UBound(TabDec(j).Liv(k).Chiave) And K1 <= UBound(MoltTabDec(i).BlkIstr(k).MoltKeyDef) Then
               If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) And K1 <= UBound(TabDec(j).Liv(k).Chiave) Then 'IRIS-DB
                 
                 Operatore = TabDec(j).Liv(k).Chiave(K1).oper ' operatore associato a decodifica j
                 'IRIS-DB If MoltTabDec(i).BlkIstr(k).MoltKeyDef(K1).MoltKey Then   ' AC 020807 And Not TipIstr = "UP"
                 If InStr(TabIstr.BlkIstr(k).KeyDef(K1).op, ";") Then 'IRIS-DB
                  'varop = TabIstr.BlkIstr(K).ssaName & "(" & Int(TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
                  multiOpGen = True
                  varOp = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                  ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                  If TipoFile = "PLI" Then
                    combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "3"
                  Else
                    combOp = "REV" & Operatore & "1" & vbCrLf _
                    & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                    & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                  End If
                  moltLevelesimo(k) = True
                  MoltOpLogic(K1) = True
                  IRISMultBol = True 'IRIS-DB molto rischioso
                 Else
                   varOp = ""
                 End If 'IRIS-DB
                 'IRIS-DB If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                  If InStr(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, ";") Then 'IRIS-DB
                    OperatoreLogicoSimb = TabDec(j).Liv(k).Chiave(K1).Bool
                    'OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                     OperatoreLogico = detranscode_booleano(OperatoreLogicoSimb, idpgm, pIdOggetto, pRiga, k, K1)
                    'If multiOp Then    ' AC 020807 And Not TipIstr = "UP"
                    'IRIS-DB If MoltTabDec(i).BlkIstr(k).MoltKeyDef(K1).MoltOpLogic Then
                    If InStr(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, ";") Then 'IRIS-DB
                      multiOpGen = True 'IRIS-DB: attenzione, potrebbe cambiare molte cose, ma serve altrimenti le END-IF se ne vanno
                      varLogicop = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                      'IRIS-DB combLogicop = "'" & OperatoreLogico & "'"
                      'IRIS-DB combinazione di booleane
                      combLogicop = buildLogicalOpEncaps(OperatoreLogico, TabIstr.BlkIstr(k).ssaName, pIdOggetto, pRiga) 'IRIS-DB
                      moltLevelesimo(k) = True
                      moltBOL(K1) = True
                      IRISMultBol = True 'IRIS-DB
                    Else
                      varLogicop = ""
                    End If
                    If Not OperatoreLogico = ")" Then
                      wFlagBoolean = True
                    Else
                      ultimoOperatore = True
                    End If
                 Else
                  varLogicop = ""
                 End If
                
                 
                  '------------------------------------------------------
                  bolMoltOpLogic = False
                  bolmoltBOL = False
                  If K1 > 1 Then
                   For ij = 1 To K1 - 1
                     If MoltOpLogic(ij) = True Then
                       bolMoltOpLogic = True
                       Exit For
                     End If
                   Next
                   For ij = 1 To K1 - 1
                    If moltBOL(ij) = True Then
                      bolmoltBOL = True
                      Exit For
                    End If
                    Next
                  End If
                  If Not varOp = "" Then
                    ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", "(" & varOp)
                    ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp & " )")
                    
                    'ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", varOp)
                    'ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp)
                    If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                    Else
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                    End If
'                    If QualAndUnqual Then
'                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
'                    End If
'                    CombPLevelOper(k1) = ComboPLevelAppoOper
'                  Else
'                      CombPLevelOper(k1) = ComboPLevelAppoOper
                  End If
                  '---------- molteplicit� booleana --------
                  If Not varLogicop = "" Then
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", varLogicop)
                   ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop)
                   'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                   If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                   Else
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                   End If
                  End If
                  
                  If QualAndUnqual Then
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                  End If
                  CombPLevelOper(K1) = ComboPLevelAppoOper
          '-------------------------------------------------------
               End If
                If ultimoOperatore Then Exit For
             Next K1
          End If '-- squal o qual
          
          
          '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
             
          Rtb.text = ComboPLevelAppo
          For jj = 1 To UBound(CombPLevelOper)
           MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
          Next
          ComboPLevelAppo = Rtb.text
          
          
          If Not varCmd = "" And bolMultipleCcod Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
            ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
            'IRIS-DB pezza a colori molto rischiosa
            'IRIS-DB If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
            If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or IRISMultBol Then 'AND -- alternativa 1 'IRIS-DB
              ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
            Else
              ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
            End If
            If QualAndUnqual Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
            End If
            CombPLevel(k) = ComboPLevelAppo
          Else
              CombPLevel(k) = ComboPLevelAppo
          End If
          If Not varSeg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varSeg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg)
              If UBound(MoltTabDec) > 1 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
        End If   '-- una sola versione o due versioni
       End If
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     Next k   ' FINE CICLO LIVELLI
     
     
'    If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
'     combinazioni = AggiornaCombinazione(maxLogicOp)
'    Else
'     combinazioni = False
'    End If

    moltLevel = False
    nomeRoutCompleto = Genera_Counter(nomeRoutine)
    'nomeRoutCompleto = Check_Dyn_PCB(nomeRoutine, idpgm, pIdOggetto, pRiga) 'IRIS-DB: check the dynamic PCB
         
    
     ' livello COMBQUAL -- costruisce elemento array combqual
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
      Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL"
      ' nel caso di linguaggi con then (vedi PLI), sui template gestiamo la riga dello then con il tag <then>
      ' negli altri casi l'if sotto � ininfluente
      For ij = 1 To UBound(moltLevelesimo)
        If moltLevelesimo(ij) = True Then
          moltLevThen = True
          Exit For
        End If
      Next
      
      If QualAndUnqual Or UBound(MoltTabDec) > 1 Or moltLevThen Then
        MadrdM_GenRout.changeTag Rtb, "<then>", ""
      End If
      If QualAndUnqual Then
        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
      Else
        ' gestisco indentazione move
        If multiOpGen Or UBound(MoltTabDec) > 1 Then ' ne lascio solo una
           MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        End If
      End If
      
      'if or end per l'istruzione e relativo end if di chiusura
      'IRIS-DB skipped for now in PLI
      'IRIS-DB ripristinato, da vedere se funziona su tutti
      'If TipoFile = "PLI" Then
      'Else
        If QualAndUnqual Then
          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
        Else
          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 2)
          ' end if
          ' AC 18/06/07  nel caso che le molteplicit� siano ignorate (vedi ISRT)
          ' non devo lasciare l'end if finale
          
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
            If multiOpGen Or UBound(MoltTabDec) > 1 Then
              MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
            Else
               MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
            End If
          ElseIf UBound(MoltTabDec) > 1 Then ' potremmo avere molteplicit� istr con istr diverse da GU,GN e GP
            MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
          Else
            MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
          End If
        End If
      'End If
      'If moltlevthen Or numcombinazioni = 1 Then  'AC 25/01/08 dovrebbe gestire la mancanza di molteplicit�
      
        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
        'If InStr(nomeRoutCompleto, "1:8") Then 'IRIS-DB
        '  MadrdM_GenRout.changeTag Rtb, "'<PROGRAMMA>'", nomeRoutCompleto 'IRIS-DB
        'Else 'IRIS-DB
          MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
        'End If 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
        'IRIS-DB: forzatura per evitare modifiche pesanti al truschino o al db
        '         la cosa migliore sarebbe passare il parametro alla routine per la gestione
        '         Comunque qui in Reviser andrebbe meglio analizzato in fase di parser e inserito nella
        '         psDli_IstrDett_Liv che al momento non viene creata per i GSAM (o su una nuova colonna della PsDli_Istruzioni)
        If Trim(codFunc) = "OPEN" Then
          Set tb1 = m_fun.Open_Recordset("select Statement from psdli_istruzioni where idoggetto = " & pIdOggetto & " and Riga = " & pRiga)
          If Not tb1.EOF Then
            If Len(tb1!statement) Then
              s = Split(tb1!statement, " ")
              If UBound(s) > 1 Then
                'IRIS-DB: per ora non considera sinonimi, sarebbe da spostare tutto nel parser
                Set tb2 = m_fun.Open_Recordset("select Valore from psData_Cmp where Nome = '" & Trim(s(2)) & "'")
                If Not tb2.EOF Then
                  If Len(tb2!Valore) Then
                    codFunc = codFunc & "-" & Trim(Replace(tb2!Valore, "'", ""))
                  End If
                End If
                tb2.Close
              End If
            End If
          End If
          tb1.Close
        
          'IRIS-DB qui deve splittare statement dalla PsDLI_Istruzioni per cercare se c'�
          '  un parametro ulteriore, in caso positivo cercare il parametro sulla PsData_Cmp
          '  e se risolto con 'IN' o 'OUT' attaccarlo in fondo a codFunc, tipo "IRIS-FUNC-OPEN-OUT"
        End If
        MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", codFunc 'IRIS-DB
        If codFunc <> "PCB" Then 'IRIS-DB
          MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", numIstr 'IRIS-DB
        End If 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPERB>", Replace(wIstr, ".", "")
        'IRIS-DB: command code for PLI - start
        If TipoFile = "PLI" Then
          Dim cleanCcode As String
          Dim singCcode() As Byte
          Dim Ccodeindex As Integer
          Dim Ccodeflag As String
          Ccodeindex = 0
          For k = 1 To UBound(TabIstr.BlkIstr)
            cleanCcode = Replace(Replace(TabIstr.BlkIstr(k).Search, "-", ""), "*", "")
            If Len(cleanCcode) > 0 Then
              singCcode = StrConv(cleanCcode, vbFromUnicode)
              For Ccodeindex = 0 To UBound(singCcode)
                If chr(singCcode(Ccodeindex)) <> ";" Then
              'IRIS-DB: piccola pezza per rimettere i tag, andrebbe fatto con un tag del tag, ma per ora lascio cos� TODO
                  If InStr(Rtb.text, "<I%IND-3><I2%IND-3>") > 0 Then
                    MadrdM_GenRout.changeTag Rtb, "<CCODE>", "IRIS_CODE_" & chr(singCcode(Ccodeindex)) & "(" & k & ") = '" & chr(singCcode(Ccodeindex)) & "';<I%IND-3><I2%IND-3><standard>" & vbCrLf & "<CCODE>"
                  Else
                    MadrdM_GenRout.changeTag Rtb, "<CCODE>", "IRIS_CODE_" & chr(singCcode(Ccodeindex)) & "(" & k & ") = '" & chr(singCcode(Ccodeindex)) & "';<I%IND-3><standard>" & vbCrLf & "<CCODE>"
                  End If
                End If
              Next Ccodeindex
            End If
          Next k
        End If
        'IRIS-DB: command code for PLI - end
        
        'AC 13/05/10 _________________________________
        
        If MadrdM_Incapsulatore_OF.isMoltIstr Or MadrdM_Incapsulatore_OF.isMoltPcb Then
            If Len(nomeRoutine) < 8 Then
                xprogramm = Space(8 - Len(nomeRoutine))
            Else
                xprogramm = ""
                If InStr(m_fun.FnNomeDB, "prj-GRZ") Then
                   nomeRoutine = IIf(Len(nomeRoutine) > 7, Left(nomeRoutine, 7), nomeRoutine)
                End If
            End If
            MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & xprogramm
        End If
      
      'End If
      
      
      If EmbeddedRoutine Then
        MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
      Else
        MadrdM_GenRout.changeTag Rtb, "<standard>", ""
      End If
      
      
      For k = 1 To UBound(CombPLevel)
        MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
      Next
      appocomb = Rtb.text
     
      If Not (QualAndUnqual And checksqual) Then
        Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFQUAL"
        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
        ReDim Preserve CombQual(jk)
        CombQual(jk) = Rtb.text
      End If
      If QualAndUnqual And checksqual Then ' due parti, la seconda � squalificata
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
        'IRIS-DB: ma perch� se lo ha gi� fatto? Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
'        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
'        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
'        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
'        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
'        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
'        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
'        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
        'AC 13/05/10 _________________________________
        If MadrdM_Incapsulatore_OF.isMoltIstr Or MadrdM_Incapsulatore_OF.isMoltPcb Then
            If Len(nomeRoutine) < 8 Then
                xprogramm = Space(8 - Len(nomeRoutine))
            Else
                xprogramm = ""
                If InStr(m_fun.FnNomeDB, "prj-GRZ") Then
                   nomeRoutine = IIf(Len(nomeRoutine) > 7, Left(nomeRoutine, 7), nomeRoutine)
                End If
            End If
            MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & xprogramm
        End If
        '_____________________________________________

        wIstrsOld = wIstrS
        If EmbeddedRoutine Then
          MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
        Else
          MadrdM_GenRout.changeTag Rtb, "<standard>", ""
        End If
        'IRIS-DB: crea duplicazioni, per ora l'ho modificato per partire dal secondo, da rivedere
        'IRIS-DB For k = 1 To UBound(CombPLevel_s)
        For k = 2 To UBound(CombPLevel_s)
          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel_s(k), True
        Next
        appocomb = Rtb.text
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
        Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFSQUAL"
        'AC pezza, verificare perche non sparisce il tag <then>
        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", Replace(appocomb, "<then>", "")
        ReDim Preserve CombSqual(UBound(CombSqual) + 1)
        CombSqual(UBound(CombSqual)) = Rtb.text
      ElseIf Not QualAndUnqual Then
'       ' anche nella parte squalificata metto la stessa cosa della qualificata
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
'        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
'        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
'        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
'        For k = 1 To UBound(CombPLevel_s)
'          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
'        Next
'        appocomb = Rtb.text
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
'        ReDim Preserve CombSqual(jk)
'        CombSqual(jk) = Rtb.text
      End If
      jk = jk + 1
      '**********
      j = j + 1 'AGGIORNAMNETO INDICE DECODIFICA
      If j > UBound(TabDec) Then
        Exit Do
      End If
    Loop
   Next
    
   'stefanopul
   'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\MAINCOMB"
   Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\MAINCOMB"
   'AC 20/03/09
  Dim dataType As String
  If Left(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName, 1) = "'" And _
     Right(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName, 1) = "'" Then
    dataType = "X"
  Else
    getAreaType Trim(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName), pIdOggetto, 0, dataType, "", -1
  End If
   If TipoFile = "PLI" Then
    MadrdM_GenRout.changeTag Rtb, "<SSA-INSPECT>", "SUBSTR(" & IIf(dataType = "X", "", "IRIS_") & _
                      MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).ssaName & ",9," & _
                      Len(TokenIesimoCC(MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).Search, 1, bolMultipleCcod)) + 1 & ")"
   Else
    MadrdM_GenRout.changeTag Rtb, "<SSA-INSPECT>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).ssaName & "(9:" & Len(TokenIesimoCC(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Search, 1, bolMultipleCcod)) + 1 & ")"
   End If
   'IRIS-DB per ora metto una pezza (solo 3 istruzioni in tutta l'applicazione), ma non � chiaro perch� vada in questa direzione, da rianalizzare, lascio un log
   If UBound(CombQual) > 0 Then 'IRIS-DB
     For i = 1 To UBound(CombQual)
       MadrdM_GenRout.changeTag Rtb, "<<IFQUAL(i)>>", CombQual(i)
     Next
   Else 'IRIS-DB
     AggLog "[Object: " & pIdOggetto & "] Combqual not initialized in 'CostruisciOperazioneTemplate'.", MadrdF_Incapsulatore.RTErr 'IRIS-DB
     m_fun.WriteLog "Incaps: Obj(" & pIdOggetto & ") Row( " & pRiga & ") Combqual not initialized", "MadrdM_Incapsulatore"
   End If 'IRIS-DB
   If QualAndUnqual Then
    For i = 1 To UBound(CombSqual)
      MadrdM_GenRout.changeTag Rtb, "<<IFSQUAL(i)>>", CombSqual(i)
    Next
    MadrdM_GenRout.changeTag Rtb, "<condsqual>", "" ' altrimenti istr corrispondenti eliminate
   End If
   appoFinalString = Rtb.text
  
   While InStr(appoFinalString, vbCrLf) > 0
     posEndLine = InStr(1, appoFinalString, vbCrLf)
     insertLine Testo, Left(appoFinalString, posEndLine - 1), pIdOggetto, indentkey
     appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
   Wend
   If Not Trim(appoFinalString) = "" Then
     insertLine Testo, appoFinalString, pIdOggetto, indentkey
   End If
  Close #nFBms
  
  Exit Function
ErrH:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & pIdOggetto & "] Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & pIdOggetto & "] unexpected error on 'CostruisciOperazioneTemplate'.", MadrdF_Incapsulatore.RTErr
  End If
  'Resume Next
End Function
Function CostruisciOperazioneTemplateNoValid(xIstr As String, idpgm As Long, pIdOggetto As Long, pRiga As Long) 'IRIS-DB
  
  Dim Rtb  As RichTextBox
  Dim tb As Recordset
  Dim appocomb As String, appoFinalString As String, checksqual As Boolean, posEndLine As String

  On Error GoTo ErrH
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  
  Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\DYNAMICSSA"
  Set tb = m_fun.Open_Recordset("select nome from bs_oggetti as a, PsDLI_IstrDBD as b where b.IdOggetto =" & pIdOggetto & " and b.Idpgm = " & idpgm & " and b.Riga = " & pRiga & " and a.idoggetto = b.idDBD")
  If Not tb.EOF Then
    MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", tb!nome
  Else
    MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", "MISSINGDBD"
  End If
  tb.Close
  MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", xIstr
  appoFinalString = Rtb.text
  While InStr(appoFinalString, vbCrLf) > 0
    posEndLine = InStr(1, appoFinalString, vbCrLf)
    insertLine Testo, Left(appoFinalString, posEndLine - 1), pIdOggetto, indentkey
    appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
  Wend
  If Not Trim(appoFinalString) = "" Then
    insertLine Testo, appoFinalString, pIdOggetto, indentkey
  End If
    
  Exit Function
ErrH:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & pIdOggetto & "] Template: DYNAMICSSA not found.", MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & pIdOggetto & "] unexpected error on 'CostruisciOperazioneTemplateNoValid'.", MadrdF_Incapsulatore.RTErr
  End If
  'Resume Next
End Function

Function CostruisciOperazioneTemplate(xIstr As String, Index As Integer, idpgm As Long, pIdOggetto As Long, pRiga As Long, ordPcb As Long)
  Dim tb, tb1 As Recordset
  Dim wOpe As String, wOpeS As String, TipIstr As String
  Dim k As Long
  Dim xope As String
  Dim K1 As Integer
  Dim wUtil As Boolean, wFlagBoolean As Boolean
  Dim i As Integer, j As Integer
  Dim checkColl As Boolean
  Dim extraIndent As Integer
  Dim maxId As Long
  Dim wIstr, wDecod As String, wIstrS As String, wDecodS As String, pcbDyn As String
  Dim maxPcbDyn As Integer, pcbDynInt As Integer
  Dim wOpeSave As String
  Dim y As Integer
  ' per comporre stringa if dell'incapsulato, versione qual e squal
  Dim IstrIncaps As String, IstrSIncaps As String, StrAnd As String
  ' contiene la molteplicit� di istruzioni, attinge dalla tabella Istruzioni_Clone
  Dim cloniIstruzione() As String
  Dim cloniSegmenti() As String
  Dim combinazioni As Boolean
  Dim maxLogicOp As Integer, maxBoolean As Integer
  Dim Operatore As String
  Dim StringToClear As String
  Dim ifAnd As String
  Dim nomeRoutCompleto As String
  Dim templateName As String  'SQ - per la gestione degli errori
  
  On Error GoTo ErrH
  'AC 02/11/06
  
  Dim varOp As String, combOp As String, varIstr As String, combIstr As String
  Dim varop_s As String, combop_s As String, ks As Integer
  Dim varCmd As String, combCmd As String, varSeg As String, combSeg As String
  Dim varcmd_s As String, combcmd_s As String
  
  Dim CombPLevel() As String, CombPLevel_s() As String, CombSqual() As String, CombQual() As String, wIstrsOld As String
  
  ' nuove varibili operatore
  Dim CombPLevelOper() As String, ComboPLevelTempOper As String, ComboPLevelAppoOper As String
  Dim bolMoltOpLogic As Boolean, jj As Integer
  
  Dim ComboPLevelTemp As String, ComboPLevelAppo As String, nFBms As Integer, wRecIn As String, jk As Integer
  Dim Rtb  As RichTextBox, appocomb As String, appoFinalString As String, checksqual As Boolean, posEndLine As String
  ' tango traccia di una qualsiasi molteplicit� sui livelli
  Dim moltLevel As Boolean, moltLevelesimo() As Boolean, ij As Integer, moltLevThen As Boolean
 
  'AC 19/06/07  arriva la booleana
  Dim OperatoreLogico As String, ultimoOperatore As Boolean, varLogicop As String, combLogicop As String
  Dim OperatoreLogicoSimb As String, moltBOL() As Boolean, bolmoltBOL As Boolean
  Dim bolMultipleCcod As Boolean
  Dim CCodeSqual() As String, z1 As Integer, numCombinazioni As Integer
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  
  nFBms = FreeFile
  
  '---- nuovo template
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVELOPE"
  Open templateName For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTempOper = "" Then
      ComboPLevelTempOper = ComboPLevelTempOper & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTempOper = ComboPLevelTempOper & wRecIn
  Wend
  Close #nFBms
  '-------------------------------
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVEL"
  Open templateName For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTemp = "" Then
      ComboPLevelTemp = ComboPLevelTemp & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTemp = ComboPLevelTemp & wRecIn
  Wend
  
  jk = 1
  
  ' AC : wMoltIstr(0), wMoltIstr(2) -> squalificata , wMoltIstr(1),wMoltIstr(3) -> qualificata , se QualAndUnqual tutte e quattro
  
  'Ac per la gestione delle combinazioni
  ' righe = livelli, colonne = max operatori logici
  ' la matrice contiene gli indici degli operatori che per ogni livello e operatore logico
  ' devono essere presi in considerazione per la decodifica
  ' ad ogni ciclo di combinazione si aggiorna la matrice e si crea una nuova decodifica combinando
  ' le info sui vari livelli e operatori logici
  ' la decodifica � inserita nella collection di wMoltIstr
  
  For k = 1 To UBound(TabIstr.BlkIstr)
    If UBound(TabIstr.BlkIstr(k).KeyDef) > maxLogicOp Then
     maxLogicOp = UBound(TabIstr.BlkIstr(k).KeyDef)
    End If
  Next
  
  ReDim CCodeSqual(0)
  
  ReDim MatriceCombinazioni(UBound(TabIstr.BlkIstr), maxLogicOp)
  If maxLogicOp > 0 Then
    ReDim MatriceCombinazioniBOL(UBound(TabIstr.BlkIstr), maxLogicOp - 1)
  End If
  
  ' array combinazioni command code : per ogni livello mi dice
  ' sequenza command code che deve essere considerate
  ReDim CombinazioniComCode(UBound(TabIstr.BlkIstr))
  ReDim Combinazionisegmenti(UBound(TabIstr.BlkIstr))
  ReDim moltLevelesimo(UBound(TabIstr.BlkIstr))
  For k = 1 To UBound(TabIstr.BlkIstr)
    For K1 = 1 To maxLogicOp
      MatriceCombinazioni(k, K1) = 1
      If Not K1 = maxLogicOp Then
        MatriceCombinazioniBOL(k, K1) = 1
      End If
    Next
  Next
  For k = 1 To UBound(TabIstr.BlkIstr)
    CombinazioniComCode(k) = 1
    Combinazionisegmenti(k) = 1
  Next
  CreaArrayCloniIstruzione cloniIstruzione, pIdOggetto, pRiga
  CreaArrayCloniSegmenti cloniSegmenti, TabIstr
  
  
  '***** determiniamo se deve essere generata
  '***** sia la versione qualificata che squalificata
  Set tb = m_fun.Open_Recordset("select * from PsDLI_IstrDett_Liv where IdOggetto =" & pIdOggetto & " and Idpgm = " & idpgm & " and Riga = " & pRiga & " and Livello = " & UBound(TabIstr.BlkIstr))
  If Not tb.EOF Then
    QualAndUnqual = tb!QualAndUnqual
  Else
    QualAndUnqual = False
  End If
  tb.Close
  
  Set wMoltIstr(0) = New Collection
  Set wMoltIstr(1) = New Collection
  Set wMoltIstr(2) = New Collection
  Set wMoltIstr(3) = New Collection
  StringToClear = ""
  ReDim wNomeRout(0)
  
  ' Ciclo esterno su istruzione
  ReDim Preserve CombSqual(0)
  
  For i = 0 To UBound(cloniIstruzione)
    xIstr = cloniIstruzione(i)
    TipIstr = Left(xIstr, 2)
    If TabIstr.isGsam Then
       TipIstr = "GS"
    ElseIf xIstr = "GU" Or xIstr = "GHU" Then
       TipIstr = "GU"
    ElseIf xIstr = "POS" Then 'IRIS-DB
      TipIstr = "PS" 'IRIS-DB
    ElseIf xIstr = "ISRT" Then
      TipIstr = "UP"
    ElseIf xIstr = "DLET" Then
       TipIstr = "UP"
    ElseIf xIstr = "REPL" Then
       TipIstr = "UP"
    ElseIf xIstr = "GNP" Or xIstr = "GHNP" Then
       TipIstr = "GP"
    ElseIf xIstr = "GN" Or xIstr = "GHN" Then
       TipIstr = "GN"
    ElseIf xIstr = "PCB" Then
       wUtil = True
       TipIstr = "SH"
    ElseIf xIstr = "TERM" Then
       wUtil = True
       TipIstr = "TM"
    ElseIf xIstr = "CHKP" Then
       wUtil = True
       TipIstr = "CP"
    ElseIf xIstr = "QRY" Then
       wUtil = True
       TipIstr = "QY"
    ElseIf xIstr = "SYNC" Or xIstr = "SYMCHKP" Then
       wUtil = True
       TipIstr = "SY"
    ElseIf xIstr = "XRST" Then
       wUtil = True
       TipIstr = "XR"
    ElseIf xIstr = "SCHED" Then
       wUtil = True
       TipIstr = "SC"
    Else
       'm_fun.WriteLog "Incapsulatore - Costruisci Operazione " & vbCrLf & "Istruzione non riconosciuta " & xIstr, "DR - Incapsulatore"
    End If
  
    bolMultipleCcod = CheckCombCCode(xIstr)
    gbIstr = xIstr
    For k = 1 To UBound(TabIstr.BlkIstr)
      CombinazioniComCode(k) = 1
    Next
  
    'riapplico il calcolo del NomeRout per ogni Molteplicit� di istruzione
    If UBound(TabIstr.DBD) > 0 Then
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
        If InStr(TabIstr.DBD(1).nome, "RKKUNL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKLUNL", xIstr, SwTp)
        ElseIf InStr(TabIstr.DBD(1).nome, "RKGIRL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKOIRL", xIstr, SwTp)
        Else
          nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, xIstr, SwTp)
        End If
      Else
        nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, xIstr, SwTp)
        'aggiornalistaDB (TabIstr.Dbd(1).nome)
      End If
    Else
      nomeRoutine = m_fun.Genera_Nome_Routine("", xIstr, SwTp)
    End If
   
   'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
   'stefano: ho dovuta commentarla perch� creava delle istruzioni non valide (AND senza IF) quando
   ' c'� molteplicit� di squalificata insieme alla molteplicit� di funzione; fondamentalmente la molteplicit�
   ' di funzione viene gestita sotto quella di squalificata; dovrebbe essere invertita o creato un vettore per ogni
   ' occorrenza di funzione.
   'If TipIstr <> "GU" And TipIstr <> "GN" And TipIstr <> "GP" Then QualAndUnqual = False
    
   combinazioni = True
   IstrIncaps = ""
   Dcombination = False
   numCombinazioni = 0
   While combinazioni
    'AC 01/02/08
    bolMultipleCcod = CheckDcomb(CombinazioniComCode)
    'AC 25/01/08
    numCombinazioni = numCombinazioni + 1
    For k = 1 To UBound(moltLevelesimo)
      moltLevelesimo(k) = False
    Next
    moltLevThen = False
    multiOpGen = False
   'AC 02/11/06
    ReDim CombPLevel(UBound(TabIstr.BlkIstr))
   ' alberto, sulle istruzioni OPEN e CLSE dava errore per cui l'ho cambiata..
    If UBound(TabIstr.BlkIstr) > 0 Then
     ReDim CombPLevel_s(UBound(TabIstr.BlkIstr) - 1)
    End If
     IstrIncaps = ""
     If UBound(cloniIstruzione) > 0 Then
        varIstr = Restituisci_varFunz(pIdOggetto, pRiga)
        combIstr = "'" & xIstr & "'"
     End If
     If isOldIncapsMethod Then
      wOpe = "<pgm>" & nomeRoutine & "</pgm><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
     Else
      If UBound(TabIstr.DBD) Then
        wOpe = "<dbd>" & TabIstr.DBD(1).nome & "</dbd><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
      Else
        wOpe = "<istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
      End If
     End If
     
     
     'AC 24/10/08
     'SENSEG
     If Not TabIstr.SenSeg = "" Then
        If TabIstr.SenSeg = "ALL" Then
            wOpe = wOpe & "<sseg></sseg>"
        Else
            wOpe = wOpe & "<sseg>" & TabIstr.SenSeg & "</sseg>"
        End If
     End If
     
     
     For k = 1 To UBound(TabIstr.BlkIstr)   ' ciclo su livelli
        ComboPLevelAppo = ComboPLevelTemp
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 28-07-06
        ' OK: REPL CON SSA QUALIFICATA DA RETURN CODE "AJ"
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 19-07-07
        ' REPL CON PATH-CALL (C.C. "D")!
        'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then  'IRIS-DB
          If TabIstr.BlkIstr(k).idsegm = -1 Then
             wOpe = wOpe & "<level" & k & "><segm>" & "DYNAMIC" & "</segm>"
          Else
             wOpe = wOpe & "<level" & k & "><segm>" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "</segm>"
             If NumToken(cloniSegmenti(k)) > 1 Then
              'varseg = TabIstr.BlkIstr(K).ssaName & "(1:8)"
              combSeg = "'" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "'"
              varSeg = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, "1", CStr(Len(combSeg) - 2), TipoFile, "SSA", pIdOggetto) '"8"
              moltLevelesimo(k) = True
             Else
              varSeg = ""
             End If
             aggiornalistaSeg (TabIstr.BlkIstr(k).segment)
          End If
        Else
          If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(TabIstr.BlkIstr) Then
            If TabIstr.BlkIstr(k).idsegm = -1 Then
               wOpe = wOpe & "<level1><segm>" & "DYNAMIC" & "</segm>"
            Else
               wOpe = wOpe & "<level1><segm>" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "</segm>"
               If NumToken(cloniSegmenti(k)) > 1 Then
                'varseg = TabIstr.BlkIstr(K).ssaName & "(1:8)"
                combSeg = "'" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "'"
                varSeg = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, "1", CStr(Len(combSeg) - 2), TipoFile, "SSA", pIdOggetto) '"8"
                moltLevelesimo(k) = True
               Else
                varSeg = ""
               End If
               aggiornalistaSeg (TabIstr.BlkIstr(k).segment)
            End If
          End If
        End If
        
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "POS" Then  'IRIS-DB
          If TabIstr.BlkIstr(k).Record <> "" Then
             If Trim(TabIstr.BlkIstr(k).segment) <> "" Then
                 wOpe = wOpe & "<area>" & "AREA" & "</area>"
             End If
          End If
        End If
        
        'AC 10/04/2007 spostata per non sommare a wopes la <ccode> del wope
        If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
          wOpeS = wOpe   ' allineiamo  la wOpeS  alla wOpe che si differenziano solo per ultimo livello
          'varop_s = varop
          'combop_s = combop
          For ks = 1 To k - 1
            CombPLevel_s(ks) = CombPLevel(ks)
          Next
          ' allineiamo le stringhe
        End If
                            
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
          If Trim(Replace(Replace(TokenIesimoCC(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod), "*", ""), "-", "")) <> "" Then
             If Trim(TabIstr.BlkIstr(k).segment) <> "" Then
                wOpe = wOpe & "<ccode>" & Trim(Replace(Replace(TokenIesimoCC(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod), "*", ""), "-", "")) & "</ccode>"
             End If
          End If
        End If
                
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
          If NumToken(TabIstr.BlkIstr(k).Search) > 1 Then
            'varcmd = TabIstr.BlkIstr(K).ssaName & "(9:" & Len(TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K))) & ")"
            varCmd = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, "9", Len(TokenIesimoCC(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod)), TipoFile, "SSA", pIdOggetto)
            combCmd = "'" & TokenIesimoCC(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod) & "'"
            If bolMultipleCcod Then
              moltLevelesimo(k) = True
            End If
          Else
            varCmd = ""
            bolMultipleCcod = False
          End If
        End If
          '*********************************
                      
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' QUALIFICAZIONE:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' AC se entrambe le versione wOpe � quella qualificata e wOpeS quella
        ' squalificata, altrimenti se unica sempre in wOpe
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
         If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
          'parte SQUALIFICATA
          ' la wOpeS si ferma qui, wOpeS e wOpe differiscono solo per questo livello
           
          'AC 04/01/08
          ' gestione combinazioni command code per squalificata
          ' ho gi� calcolato combinazione ccode
          ' non essendo abbinata ad altre molteplicit� la stessa combinazione
          ' potrebbe ripresentarsi ciclicamente -> uso array CCodeSqual

          If bolMultipleCcod Then
            If Not (InStr(combCmd, "-") > 0 Or InStr(combCmd, " ") > 0) Then
              For z1 = 1 To UBound(CCodeSqual)
                If CCodeSqual(z1) = combCmd Then
                  Exit For
                End If
              Next z1
              If z1 > UBound(CCodeSqual) Then
                ReDim CCodeSqual(UBound(CCodeSqual) + 1)
                CCodeSqual(UBound(CCodeSqual)) = combCmd
                Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVEL"
                ComboPLevelAppo = Rtb.text
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
                ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
                If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
                Else
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
                End If
                If QualAndUnqual Then
                  ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
                End If
                wOpeS = wOpe ' attacco anche il pezzo relativo a ccode su ultimo livello
                ReDim CombPLevel_s(k)
                CombPLevel_s(k) = ComboPLevelAppo
              End If
            End If
          End If
          
           
           
           
          '__________________________________________
          'parte QUALIFICATA
          wFlagBoolean = False
          ' determino se su livelli precedenti ho molteplicit� operatore
             If k > 1 Then
              For ij = 1 To k - 1
                If moltLevelesimo(ij) = True Then
                 moltLevel = True
                 Exit For
                End If
              Next
             End If
             ' ------------CICLO SU OPERATORI LOGICI---------------
             bolMoltOpLogic = False
             bolmoltBOL = False
             ultimoOperatore = False
             ReDim MoltOpLogic(UBound(TabIstr.BlkIstr(k).KeyDef))
             If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(TabIstr.BlkIstr(k).KeyDef) - 1)
             End If
             ReDim CombPLevelOper(UBound(TabIstr.BlkIstr(k).KeyDef))
             For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
               'AC 12/07
               ' ripulisco la maschera del template COMBOPLEVELOPE
               ComboPLevelAppoOper = ComboPLevelTempOper
               ComboPLevelAppo = ComboPLevelTemp
               If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                 wOpe = wOpe & "<key>" & TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                 Operatore = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                 wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                 If multiOp Then
                  varOp = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                  ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                  If TipoFile = "PLI" Then
                    combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "3"
                  Else
                    combOp = "REV" & Operatore & "1" & vbCrLf _
                    & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                    & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                  End If
                  moltLevelesimo(k) = True
                  MoltOpLogic(K1) = True
                 Else
                    varOp = ""
                 End If
                 If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                     OperatoreLogicoSimb = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, MatriceCombinazioniBOL(k, K1))
                     OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                     If multiOp Then
                      varLogicop = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                      combLogicop = "'" & OperatoreLogicoSimb & "'"
                      moltLevelesimo(k) = True
                      moltBOL(K1) = True
                     Else
                      varLogicop = ""
                     End If
                     If Not OperatoreLogico = ")" Then
                       wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                       wFlagBoolean = True
                     Else
                       ultimoOperatore = True
                     End If
                 Else
                  varLogicop = ""
                 End If
                'AC 03/07/06
                'ultimo
                  
                bolMoltOpLogic = False
                bolmoltBOL = False
                If K1 > 1 Then
                 For ij = 1 To K1 - 1
                   If MoltOpLogic(ij) = True Then
                     bolMoltOpLogic = True
                     Exit For
                   End If
                 Next
                 For ij = 1 To K1 - 1
                   If moltBOL(ij) = True Then
                     bolmoltBOL = True
                     Exit For
                   End If
                 Next
                End If
                '---------- molteplicit� operatore --------
                If Not varOp = "" Then
                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", varOp)
                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp)
                 'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                 If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                 Else
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                 End If
'                 If QualAndUnqual Then
'                   ComboPLevelAppoOper = Replace(ComboPLevelAppo, "<I%IND-3>", "")
'                 End If
'                 CombPLevelOper(k1) = ComboPLevelAppoOper
'                Else
'                 CombPLevelOper(k1) = ComboPLevelAppoOper
                 End If
                ' booleano
                '---------- molteplicit� booleana --------
                If Not varLogicop = "" Then
                 ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", varLogicop)
                 ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop)
                 'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                 If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                 Else
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                 End If
                End If
                
                If QualAndUnqual Then
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                End If
                CombPLevelOper(K1) = ComboPLevelAppoOper
                
             '-------------------------------------------
               End If
               ' nel caso di parentesi sull'operatore logico interrompo il ciclo
               If ultimoOperatore Then Exit For
             Next K1
             
             '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
             
             Rtb.text = ComboPLevelAppo
             For jj = 1 To UBound(CombPLevelOper)
              MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
             Next
             ComboPLevelAppo = Rtb.text
             
             If Not varCmd = "" And bolMultipleCcod Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
            If Not varSeg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varSeg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg)
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
         Else  ' o la versione squalificata o quella squalificata
         
          ' AC il campo parentesi e la trasformazione in stringa del booleano Qualificazione
          If TabIstr.BlkIstr(k).Parentesi <> "(" Then ' Or Trim(TabIstr.BlkIstr(k).SsaName) = "<none>" Then
             OnlySqual = True
             ReDim CombPLevelOper(0)
          Else   ' Qualificata
             OnlyQual = True
             wFlagBoolean = False
             If k > 1 Then
              For ij = 1 To k - 1
               If moltLevelesimo(ij) = True Then
                 moltLevel = True
                 Exit For
               End If
              Next
             End If
             ' ----- CICLO OPERATORI LOGICI
             bolmoltBOL = False
             ultimoOperatore = False
             bolMoltOpLogic = False
             ReDim MoltOpLogic(UBound(TabIstr.BlkIstr(k).KeyDef))
             If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(TabIstr.BlkIstr(k).KeyDef) - 1)
             End If
             ReDim CombPLevelOper(UBound(TabIstr.BlkIstr(k).KeyDef))
             For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
               ComboPLevelAppoOper = ComboPLevelTempOper
               If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                 wOpe = wOpe & "<key>" & TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                 Operatore = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                 wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                 If multiOp Then  ' AC 020807 And Not TipIstr = "UP"
                  'varop = TabIstr.BlkIstr(K).ssaName & "(" & Int(TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
                  varOp = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                  ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                  If TipoFile = "PLI" Then
                    combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                    & "   " & varOp & " = IRIS_" & Operatore & "3"
                  Else
                    combOp = "REV" & Operatore & "1" & vbCrLf _
                    & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                    & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                  End If
                  moltLevelesimo(k) = True
                  MoltOpLogic(K1) = True
                 Else
                    varOp = ""
                 End If
                 If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                    OperatoreLogicoSimb = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, MatriceCombinazioniBOL(k, K1))
                    OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                    If multiOp Then    ' AC 020807 And Not TipIstr = "UP"
                      varLogicop = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                      combLogicop = "'" & OperatoreLogicoSimb & "'"
                      moltLevelesimo(k) = True
                      moltBOL(K1) = True
                    Else
                      varLogicop = ""
                    End If
                    If Not OperatoreLogico = ")" Then
                      wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                      wFlagBoolean = True
                    Else
                      ultimoOperatore = True
                    End If
                 Else
                  varLogicop = ""
                 End If
                
                 
                  '------------------------------------------------------
                  bolMoltOpLogic = False
                  bolmoltBOL = False
                  If K1 > 1 Then
                   For ij = 1 To K1 - 1
                     If MoltOpLogic(ij) = True Then
                       bolMoltOpLogic = True
                       Exit For
                     End If
                   Next
                   For ij = 1 To K1 - 1
                    If moltBOL(ij) = True Then
                      bolmoltBOL = True
                      Exit For
                    End If
                    Next
                  End If
                  If Not varOp = "" Then
                    ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", varOp)
                    ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp)
                    If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                    Else
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                    End If
'                    If QualAndUnqual Then
'                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
'                    End If
'                    CombPLevelOper(k1) = ComboPLevelAppoOper
'                  Else
'                      CombPLevelOper(k1) = ComboPLevelAppoOper
                  End If
                  '---------- molteplicit� booleana --------
                  If Not varLogicop = "" Then
                   ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", varLogicop)
                   ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop)
                   'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                   If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                   Else
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                   End If
                  End If
                  
                  If QualAndUnqual Then
                     ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                  End If
                  CombPLevelOper(K1) = ComboPLevelAppoOper
          '-------------------------------------------------------
               End If
                If ultimoOperatore Then Exit For
             Next K1
          End If '-- squal o qual
          
          
          '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
             
          Rtb.text = ComboPLevelAppo
          For jj = 1 To UBound(CombPLevelOper)
           MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
          Next
          ComboPLevelAppo = Rtb.text
          
          
          If Not varCmd = "" And bolMultipleCcod Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
            ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
            If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
              ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
            Else
              ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
            End If
            If QualAndUnqual Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
            End If
            CombPLevel(k) = ComboPLevelAppo
          Else
              CombPLevel(k) = ComboPLevelAppo
          End If
          If Not varSeg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varSeg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg)
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(k) = ComboPLevelAppo
            Else
              CombPLevel(k) = ComboPLevelAppo
            End If
        End If   '-- una sola versione o due versioni
       End If
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        ' SQ 19-07-07
        ' REPL CON PATH-CALL (C.C. "D")!
        'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then  'IRIS-DB
         wOpe = wOpe & "</level" & k & ">"
         If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
           wOpeS = wOpeS & "</level" & k & ">"
         End If
        Else
         If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(TabIstr.BlkIstr) Then
           wOpe = wOpe & "</level1>"
           If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
             wOpeS = wOpeS & "</level1>"
           End If
         End If
        End If
     Next k   ' FINE CICLO LIVELLI
     
     wOpe = wOpe & "</livelli>"
     
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
      If Trim(TabIstr.keyfeedback) <> "" Then
         wOpe = wOpe & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
      End If
     End If
     
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
      If Len(TabIstr.procSeq) Then
         wOpe = wOpe & "<procseq>" & TabIstr.procSeq & "</procseq>"
      End If
     End If

     If xIstr = "OPEN" Then
      If Len(TabIstr.procOpt) Then
         wOpe = wOpe & "<procopt>" & TabIstr.procOpt & "</procopt>"
      End If
     End If
     
    'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
    ' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpe
     For k = 1 To UBound(OpeIstrRt)
        If wOpe = OpeIstrRt(k).Decodifica Then
           If OpeIstrRt(k).ordPcb = ordPcb Then
              'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
              'verifico se � la prima
              pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
              pcbDynInt = OpeIstrRt(k).pcbDyn
              maxPcbDyn = 0
              Exit For
           Else
              If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
              'trovata un'istruzione identica, ma con diverso pcb
                 maxPcbDyn = OpeIstrRt(k).pcbDyn
              End If
           End If
        End If
     Next k
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
     If maxPcbDyn > 0 Then
        pcbDyn = Format(maxPcbDyn + 1, "000")
        pcbDynInt = maxPcbDyn + 1
     End If
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
      ' aggiunge il pcb dinamico
      If Len(pcbDyn) Then
        wOpe = wOpe & "<pcbseq>" & pcbDyn & "</pcbseq>"
        'AggLog "[Object: " & pIdOggetto & "] New Instruction: <pcbseq>" & pcbDyn & "</pcbseq>", MadrdF_Incapsulatore.RTErr
      Else
        ' se � la prima istruzione di quel tipo
        wOpe = wOpe & "<pcbseq>" & "001" & "</pcbseq>"
        pcbDynInt = 1
      End If
     End If
    
     wDecod = wOpe
     Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpe & "'")
     
     'If xope = "" Then
     If tb.EOF Then
       Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
          maxId = 1
       Else
          maxId = tb1(0) + 1
       End If
       tb.AddNew
       'SQ tmp:
       'm_fun.WriteLog "##" & wOpe
       tb!IdIstruzione = maxId
       tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
       wIstr = tb!Codifica
       tb!Decodifica = wOpe
       tb!ImsDB = True
       tb!ImsDC = False
       tb!CICS = SwTp
   'stefano: routine multiple
       nomeRoutCompleto = Genera_Counter(nomeRoutine)
       tb!nomeRout = nomeRoutCompleto
       tb.Update
       tb.Close
      ' Mauro 28/04/2009
''    'pcb dinamico
''       k = UBound(OpeIstrRt) + 1
''       ReDim Preserve OpeIstrRt(k)
''       OpeIstrRt(k).Decodifica = wOpeSave
''       OpeIstrRt(k).Istruzione = wIstr
''       OpeIstrRt(k).ordPcb = ordPcb
''       OpeIstrRt(k).pcbDyn = pcbDynInt
     Else
       wIstr = tb!Codifica
       nomeRoutCompleto = tb!nomeRout
       ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
       nomeRoutine = tb!nomeRout
       tb.Close
     End If
     
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     'SQ CPY-ISTR
     'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
     Set tb = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrCodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
     If tb.EOF Then
       'SQ 16-11-06 - EMBEDDED
       'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & nomeroutcompleto & "')"
       'SQ CPY-ISTR
       'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeroutcompleto) & "')"
       m_fun.FnConnection.Execute "INSERT INTO PsDLI_IstrCodifica VALUES(" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeRoutCompleto) & "')"
       ' Mauro 28/04/2009 : pcb dinamico
       k = UBound(OpeIstrRt) + 1
       ReDim Preserve OpeIstrRt(k)
       OpeIstrRt(k).Decodifica = wOpeSave
       OpeIstrRt(k).Istruzione = wIstr
       OpeIstrRt(k).ordPcb = ordPcb
       OpeIstrRt(k).pcbDyn = pcbDynInt
     End If
     tb.Close
     If QualAndUnqual Then
       wOpeS = wOpeS & "</livelli>"
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
      If Trim(TabIstr.keyfeedback) <> "" Then
        wOpeS = wOpeS & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
      End If
'procseq: sarebbe meglio metterlo in tabistr, ma per ora facciamo cos� (procseq
' ci servir� anche nella generazione routine
      If UBound(TabIstr.psb) Then
         If Len(TabIstr.procSeq) Then
             wOpeS = wOpeS & "<procseq>" & TabIstr.procSeq & "</procseq>"
         End If
         If Len(TabIstr.procOpt) And xIstr = "OPEN" Then
             wOpeS = wOpeS & "<procopt>" & TabIstr.procOpt & "</procopt>"
         End If
      End If
     End If
      


'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpeS
     For k = 1 To UBound(OpeIstrRt)
        If wOpeS = OpeIstrRt(k).Decodifica Then
           If OpeIstrRt(k).ordPcb = ordPcb Then
              'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
              'verifico se � la prima
              pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
              pcbDynInt = OpeIstrRt(k).pcbDyn
              maxPcbDyn = 0
              Exit For
           Else
              If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
              'trovata un'istruzione identica, ma con diverso pcb
                 maxPcbDyn = OpeIstrRt(k).pcbDyn
              End If
           End If
        End If
     Next k
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
     If maxPcbDyn > 0 Then
        pcbDyn = Format(maxPcbDyn + 1, "000")
        pcbDynInt = maxPcbDyn + 1
     End If
     ' aggiunge il pcb dinamico
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
      If Len(pcbDyn) Then
        wOpeS = wOpeS & "<pcbseq>" & pcbDyn & "</pcbseq>"
        'AggLog "[Object: " & pIdOggetto & "] New Instruction: <pcbseq>" & pcbDyn & "</pcbseq>", MadrdF_Incapsulatore.RTErr
      Else
        ' se � la prima istruzione di quel tipo
        wOpeS = wOpeS & "<pcbseq>" & "001" & "</pcbseq>"
        pcbDynInt = 1
      End If
     End If
     
     wDecodS = wOpeS
     Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpeS & "'")
     
     'If xope = "" Then
     If tb.EOF Then
         

       Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
          maxId = 1
       Else
          maxId = tb1(0) + 1
       End If
       tb.AddNew
       tb!IdIstruzione = maxId
       tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
       wIstrS = tb!Codifica
       tb!Decodifica = wOpeS
       tb!ImsDB = True
       tb!ImsDC = False
       tb!CICS = SwTp
   'stefano: routine multiple
       nomeRoutCompleto = Genera_Counter(nomeRoutine)
       tb!nomeRout = nomeRoutCompleto
       tb.Update
       tb.Close
       ' Mauro 28/04/2009
''    'pcb dinamico
''       k = UBound(OpeIstrRt) + 1
''       ReDim Preserve OpeIstrRt(k)
''       OpeIstrRt(k).Decodifica = wOpeSave
''       OpeIstrRt(k).Istruzione = wIstr
''       OpeIstrRt(k).ordPcb = ordPcb
''       OpeIstrRt(k).pcbDyn = pcbDynInt
     Else
       wIstrS = tb!Codifica
       nomeRoutCompleto = tb!nomeRout
       ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
       nomeRoutine = tb!nomeRout
       tb.Close
     End If
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     'virgilio CPY-ISTR
     'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
     Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
     If tb.EOF Then
       'virgilio CPY-ISTR
       'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeroutcompleto & "')"
       m_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeRoutCompleto & "')"
       ' Mauro 28/04/2009 : pcb dinamico
       k = UBound(OpeIstrRt) + 1
       ReDim Preserve OpeIstrRt(k)
       OpeIstrRt(k).Decodifica = wOpeSave
       OpeIstrRt(k).Istruzione = wIstr
       OpeIstrRt(k).ordPcb = ordPcb
       OpeIstrRt(k).pcbDyn = pcbDynInt
     End If
     tb.Close
    End If
    'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    'BACO: per ora non viene gestita la molteplicit� sulla ISRT per i livelli qualificati
    ' ma in realt� non ha senso (di solito l'inserimento viene fatto qualificando un
    ' livello precedente per uguale, in modo da puntare un solo parent
    If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
     combinazioni = AggiornaCombinazione(maxLogicOp)
    Else
     combinazioni = False
    End If
    moltLevel = False
     ' aggiornamento collection
     If Not StringToClear = "" Then
      IstrIncaps = Replace(IstrIncaps, StringToClear, "")
      IstrSIncaps = Replace(IstrSIncaps, StringToClear, "")
     End If
     
     If QualAndUnqual Then
       ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
       checkColl = True
       
       For j = 1 To wMoltIstr(3).Count
        
        If wMoltIstr(3).item(j) = wIstr Then
          checkColl = False
          
          Exit For
        End If
       Next
       If checkColl Then
        wMoltIstr(1).Add IstrIncaps, wDecod
        wMoltIstr(3).Add wIstr, wDecod
        aggiornalistaCodOp (wIstr)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeRoutCompleto
       End If
       
       ' 0 e 2  da wOpeS, indice ks dell'array e da IstrSIncaps
       checkColl = True
       checksqual = True
       For j = 1 To wMoltIstr(2).Count
'        If wMoltIstr(2).Item(j) = OpeIstrRt(ks).Istruzione Then
        If wMoltIstr(2).item(j) = wIstrS Then
          checkColl = False
          checksqual = False
          Exit For
        End If
       Next
       If checkColl Then
        wMoltIstr(0).Add IstrSIncaps, wDecod
        wMoltIstr(2).Add wIstrS, wDecodS
        aggiornalistaCodOp (wIstrS)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeRoutCompleto
       End If
     Else
         ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
         checkColl = True
         For j = 1 To wMoltIstr(3).Count
'          If wMoltIstr(3).Item(j) = OpeIstrRt(k).Istruzione Then
          If wMoltIstr(3).item(j) = wIstr Then
            checkColl = False
            Exit For
          End If
         Next
         If checkColl Then
          wMoltIstr(1).Add IstrIncaps, wDecod
          wMoltIstr(3).Add wIstr, wDecod
          aggiornalistaCodOp (wIstr)
          y = y + 1
          ReDim Preserve wNomeRout(y)
          wNomeRout(y) = nomeRoutCompleto
         End If
       
     End If
     
     ' livello COMBQUAL -- costruisce elemento array combqual
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
      Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL"
      ' nel caso di linguaggi con then (vedi PLI), sui template gestiamo la riga dello then con il tag <then>
      ' negli altri casi l'if sotto � ininfluente
      For ij = 1 To UBound(moltLevelesimo)
        If moltLevelesimo(ij) = True Then
          moltLevThen = True
          Exit For
        End If
      Next
      If QualAndUnqual Or UBound(cloniIstruzione) > 0 Or moltLevThen Then
        MadrdM_GenRout.changeTag Rtb, "<then>", ""
      End If
      If QualAndUnqual Then
        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
      Else
        ' gestisco indentazione move
        If multiOpGen Or UBound(cloniIstruzione) > 0 Then ' ne lascio solo una
          MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        End If
      End If
      
      'if or end per l'istruzione e relativo end if di chiusura
      If QualAndUnqual Then
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
      Else
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 2)
        ' end if
        ' AC 18/06/07  nel caso che le molteplicit� siano ignorate (vedi ISRT)
        ' non devo lasciare l'end if finale
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then  'IRIS-DB
          If multiOpGen Or UBound(cloniIstruzione) > 0 Then
            MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
          Else
             MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
          End If
        ElseIf UBound(cloniIstruzione) > 0 Then ' potremmo avere molteplicit� istr con istr diverse da GU,GN e GP
          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
        Else
          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
        End If
      End If
      
      If moltLevThen Or numCombinazioni = 1 Then  'AC 25/01/08 dovrebbe gestire la mancanza di molteplicit�
      
        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPERB>", Replace(wIstr, ".", "")
      
      End If
      
      If EmbeddedRoutine Then
        MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
      Else
        MadrdM_GenRout.changeTag Rtb, "<standard>", ""
      End If
      
      
      For k = 1 To UBound(CombPLevel)
        MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
      Next
      
      appocomb = Rtb.text
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFQUAL"
      Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFQUAL"
      MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
      ReDim Preserve CombQual(jk)
      CombQual(jk) = Rtb.text
      
      If QualAndUnqual And checksqual Then ' due parti, la seconda � squalificata
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
        Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL"
        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstrS
        wIstrsOld = wIstrS
        If EmbeddedRoutine Then
          MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
        Else
          MadrdM_GenRout.changeTag Rtb, "<standard>", ""
        End If
        For k = 1 To UBound(CombPLevel_s)
          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel_s(k), True
        Next
        appocomb = Rtb.text
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
        Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFSQUAL"
        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
        ReDim Preserve CombSqual(UBound(CombSqual) + 1)
        CombSqual(UBound(CombSqual)) = Rtb.text
      ElseIf Not QualAndUnqual Then
'       ' anche nella parte squalificata metto la stessa cosa della qualificata
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
'        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
'        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
'        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
'        For k = 1 To UBound(CombPLevel_s)
'          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
'        Next
'        appocomb = Rtb.text
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
'        ReDim Preserve CombSqual(jk)
'        CombSqual(jk) = Rtb.text
      End If
      jk = jk + 1
      '**********
   Wend
  Next i  'ciclo su molteplicit� istruzioni
  'sostituzioni nel file principale
    
   'stefanopul
   'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\MAINCOMB"
   Rtb.LoadFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\MAINCOMB"
   MadrdM_GenRout.changeTag Rtb, "<SSA-INSPECT>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).ssaName & "(9:" & Len(TokenIesimoCC(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Search, 1, bolMultipleCcod)) + 1 & ")"
   For i = 1 To UBound(CombQual)
     MadrdM_GenRout.changeTag Rtb, "<<IFQUAL(i)>>", CombQual(i)
   Next
   If QualAndUnqual Then
    For i = 1 To UBound(CombSqual)
      MadrdM_GenRout.changeTag Rtb, "<<IFSQUAL(i)>>", CombSqual(i)
    Next
    MadrdM_GenRout.changeTag Rtb, "<condsqual>", "" ' altrimenti istr corrispondenti eliminate
   End If
   appoFinalString = Rtb.text
  
   While InStr(appoFinalString, vbCrLf) > 0
     posEndLine = InStr(1, appoFinalString, vbCrLf)
     insertLine Testo, Left(appoFinalString, posEndLine - 1), pIdOggetto, indentkey
     appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
   Wend
   If Not Trim(appoFinalString) = "" Then
     insertLine Testo, appoFinalString, pIdOggetto, indentkey
   End If
  Close #nFBms
  
  Exit Function
ErrH:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & pIdOggetto & "] Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & pIdOggetto & "] unexpected error on 'CostruisciOperazioneTemplate'.", MadrdF_Incapsulatore.RTErr
  End If
  'Resume Next
End Function

Function CostruisciOperazioneTemplate_EXEC(xIstr As String, Index As Integer, idpgm As Long, pIdOggetto As Long, pRiga As Long, ordPcb As Long)
  Dim tb, tb1 As Recordset
  Dim wOpe As String, wOpeS As String, TipIstr As String
  Dim k As Long
  Dim xope As String
  Dim K1 As Integer
  Dim wUtil As Boolean, wFlagBoolean As Boolean
  Dim i As Integer, j As Integer
  Dim checkColl As Boolean
  Dim extraIndent As Integer
  Dim maxId As Long
  Dim wIstr, wDecod As String, wIstrS As String, wDecodS As String, pcbDyn As String
  Dim maxPcbDyn As Integer, pcbDynInt As Integer
  Dim wOpeSave As String
  Dim y As Integer
  ' per comporre stringa if dell'incapsulato, versione qual e squal
  Dim IstrIncaps As String, IstrSIncaps As String, StrAnd As String
  ' contiene la molteplicit� di istruzioni, attinge dalla tabella Istruzioni_Clone
  Dim cloniIstruzione() As String
  Dim cloniSegmenti() As String
  Dim combinazioni As Boolean
  Dim maxLogicOp As Integer, maxBoolean As Integer
  Dim Operatore As String
  Dim StringToClear As String
  Dim ifAnd As String
  Dim nomeRoutCompleto As String
  
  On Error GoTo ErrH
  'AC 02/11/06
  Dim varOp As String, combOp As String, varIstr As String, combIstr As String
  Dim varop_s As String, combop_s As String, ks As Integer
  Dim varCmd As String, combCmd As String, varSeg As String, combSeg As String
  Dim varcmd_s As String, combcmd_s As String
  
  Dim CombPLevel() As String, CombPLevel_s() As String, CombSqual() As String, CombQual() As String, wIstrsOld As String
  
  ' nuove varibili operatore
  Dim CombPLevelOper() As String, ComboPLevelTempOper As String, ComboPLevelAppoOper As String
  Dim bolMoltOpLogic As Boolean, jj As Integer
  
  Dim ComboPLevelTemp As String, ComboPLevelAppo As String, nFBms As Integer, wRecIn As String, jk As Integer
  Dim Rtb  As RichTextBox, appocomb As String, appoFinalString As String, checksqual As Boolean, posEndLine As String
  ' tango traccia di una qualsiasi molteplicit� sui livelli
  Dim moltLevel As Boolean, moltLevelesimo() As Boolean, ij As Integer, moltLevThen As Boolean
  
  'AC 19/06/07  arriva la booleana
  Dim OperatoreLogico As String, ultimoOperatore As Boolean, varLogicop As String, combLogicop As String
  Dim OperatoreLogicoSimb As String, moltBOL() As Boolean, bolmoltBOL As Boolean
  Dim bolMultipleCcod As Boolean
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  wPath = m_fun.FnPathPrj
  
  nFBms = FreeFile
  
  '---- nuovo template
  Open wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVELOPE" For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTempOper = "" Then
      ComboPLevelTempOper = ComboPLevelTempOper & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTempOper = ComboPLevelTempOper & wRecIn
  Wend
  Close #nFBms
  '-------------------------------
  
  Open wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBOPLEVEL" For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTemp = "" Then
      ComboPLevelTemp = ComboPLevelTemp & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTemp = ComboPLevelTemp & wRecIn
  Wend
  
  jk = 1
  
  ' AC : wMoltIstr(0), wMoltIstr(2) -> squalificata , wMoltIstr(1),wMoltIstr(3) -> qualificata , se QualAndUnqual tutte e quattro
  
  'Ac per la gestione delle combinazioni
  ' righe = livelli, colonne = max operatori logici
  ' la matrice contiene gli indici degli operatori che per ogni livello e operatore logico
  ' devono essere presi in considerazione per la decodifica
  ' ad ogni ciclo di combinazione si aggiorna la matrice e si crea una nuova decodifica combinando
  ' le info sui vari livelli e operatori logici
  ' la decodifica � inserita nella collection di wMoltIstr
  
  For k = 1 To UBound(TabIstr.BlkIstr)
    If UBound(TabIstr.BlkIstr(k).KeyDef) > maxLogicOp Then
      maxLogicOp = UBound(TabIstr.BlkIstr(k).KeyDef)
    End If
  Next k
  
  ReDim MatriceCombinazioni(UBound(TabIstr.BlkIstr), maxLogicOp)
  If maxLogicOp > 0 Then
    ReDim MatriceCombinazioniBOL(UBound(TabIstr.BlkIstr), maxLogicOp - 1)
  End If
  
  ' array combinazioni command code : per ogni livello mi dice
  ' sequenza command code che deve essere considerate
  ReDim CombinazioniComCode(UBound(TabIstr.BlkIstr))
  ReDim Combinazionisegmenti(UBound(TabIstr.BlkIstr))
  ReDim moltLevelesimo(UBound(TabIstr.BlkIstr))
  For k = 1 To UBound(TabIstr.BlkIstr)
    For K1 = 1 To maxLogicOp
      MatriceCombinazioni(k, K1) = 1
      If Not K1 = maxLogicOp Then
        MatriceCombinazioniBOL(k, K1) = 1
      End If
    Next K1
  Next k
  For k = 1 To UBound(TabIstr.BlkIstr)
    CombinazioniComCode(k) = 1
    Combinazionisegmenti(k) = 1
  Next k
  CreaArrayCloniIstruzione cloniIstruzione, pIdOggetto, pRiga
  CreaArrayCloniSegmenti cloniSegmenti, TabIstr
  
  'SQ ancora!!??? abbiamo letto l'istruzione all'inizio
  '***** determiniamo se deve essere generata
  '***** sia la versione qualificata che squalificata
  Set tb = m_fun.Open_Recordset("select * from PsDLI_IstrDett_Liv where IdOggetto =" & pIdOggetto & " and Riga = " & pRiga & " and Livello = " & UBound(TabIstr.BlkIstr))
  If Not tb.EOF Then
    QualAndUnqual = tb!QualAndUnqual
  Else
    QualAndUnqual = False
  End If
  tb.Close
  
  Set wMoltIstr(0) = New Collection
  Set wMoltIstr(1) = New Collection
  Set wMoltIstr(2) = New Collection
  Set wMoltIstr(3) = New Collection
  StringToClear = ""
  ReDim wNomeRout(0)
  
  ' Ciclo esterno su istruzione
  ReDim Preserve CombSqual(0)
  
  For i = 0 To UBound(cloniIstruzione)
    xIstr = cloniIstruzione(i)
    TipIstr = Left(xIstr, 2)
    If TabIstr.isGsam Then
      TipIstr = "GS"
    ElseIf xIstr = "GU" Or xIstr = "GHU" Then
      TipIstr = "GU"
    ElseIf xIstr = "POS" Then 'IRIS-DB
      TipIstr = "PS" 'IRIS-DB
    ElseIf xIstr = "ISRT" Then
      TipIstr = "UP"
    ElseIf xIstr = "DLET" Then
      TipIstr = "UP"
    ElseIf xIstr = "REPL" Then
      TipIstr = "UP"
    ElseIf xIstr = "GNP" Or xIstr = "GHNP" Then
      TipIstr = "GP"
    ElseIf xIstr = "GN" Or xIstr = "GHN" Then
      TipIstr = "GN"
    ElseIf xIstr = "PCB" Then
      wUtil = True
      TipIstr = "SH"
    ElseIf xIstr = "TERM" Then
      wUtil = True
      TipIstr = "TM"
    ElseIf xIstr = "CHKP" Then
      wUtil = True
      TipIstr = "CP"
    ElseIf xIstr = "QRY" Then
      wUtil = True
      TipIstr = "QY"
    ElseIf xIstr = "SYNC" Or xIstr = "SYMCHKP" Then
      wUtil = True
      TipIstr = "SY"
    ElseIf xIstr = "XRST" Then
      wUtil = True
      TipIstr = "XR"
    ElseIf xIstr = "SCHED" Then
      wUtil = True
      TipIstr = "SC"
    Else
      'm_fun.WriteLog "Incapsulatore - Costruisci Operazione " & vbCrLf & "Istruzione non riconosciuta " & xIstr, "DR - Incapsulatore"
    End If
  
    bolMultipleCcod = CheckCombCCode(xIstr)
    gbIstr = xIstr
    For k = 1 To UBound(TabIstr.BlkIstr)
      CombinazioniComCode(k) = 1
    Next k
    
    'riapplico il calcolo del NomeRout per ogni Molteplicit� di istruzione
    If UBound(TabIstr.DBD) > 0 Then
      ' Mauro 05/06/2008 : Se � un DBD Index, il nome routine lo devo costruire con il DBD Fisico
      Dim rsDBD As Recordset
      Dim NomeDbD As String
      
      NomeDbD = TabIstr.DBD(1).nome
      Set rsDBD = m_fun.Open_Recordset("select idoggetto from Bs_Oggetti where " & _
                                       "idoggetto = " & TabIstr.DBD(1).Id & " and tipo = 'DBD' and tipo_dbd = 'IND'")
      If rsDBD.RecordCount Then
        NomeDbD = Restituisci_DBD_Da_DBDIndex(rsDBD!IdOggetto)
      End If
      rsDBD.Close
      
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
        If InStr(TabIstr.DBD(1).nome, "RKKUNL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKLUNL", xIstr, SwTp)
        ElseIf InStr(TabIstr.DBD(1).nome, "RKGIRL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKOIRL", xIstr, SwTp)
        Else
          nomeRoutine = m_fun.Genera_Nome_Routine(NomeDbD, xIstr, SwTp)
        End If
      Else
        'nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, xIstr, SwTp)
        nomeRoutine = m_fun.Genera_Nome_Routine(NomeDbD, xIstr, SwTp)
        'aggiornalistaDB (TabIstr.Dbd(1).nome)
      End If
    Else
      nomeRoutine = m_fun.Genera_Nome_Routine("", xIstr, SwTp)
    End If
    'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    'stefano: ho dovuta commentarla perch� creava delle istruzioni non valide (AND senza IF) quando
    ' c'� molteplicit� di squalificata insieme alla molteplicit� di funzione; fondamentalmente la molteplicit�
    ' di funzione viene gestita sotto quella di squalificata; dovrebbe essere invertita o creato un vettore per ogni
    ' occorrenza di funzione.
    'If TipIstr <> "GU" And TipIstr <> "GN" And TipIstr <> "GP" Then QualAndUnqual = False
    
    combinazioni = True
    IstrIncaps = ""
    Dcombination = False
   
    While combinazioni
      multiOpGen = False
      'AC 02/11/06
      ReDim CombPLevel(UBound(TabIstr.BlkIstr))
      ' alberto, sulle istruzioni OPEN e CLSE dava errore per cui l'ho cambiata..
      If UBound(TabIstr.BlkIstr) > 0 Then
        ReDim CombPLevel_s(UBound(TabIstr.BlkIstr) - 1)
      End If
      IstrIncaps = ""
      If UBound(cloniIstruzione) > 0 Then
        varIstr = Restituisci_varFunz(pIdOggetto, pRiga)
        combIstr = "'" & xIstr & "'"
      End If
      If isOldIncapsMethod Then
        wOpe = "<pgm>" & nomeRoutine & "</pgm><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
      Else
        'SQ PEZZA: GESTIRE BENE... (ATTENZIONE E' ANCHE IN UN ALTRO PUNTO...
        If UBound(TabIstr.DBD) Then
          wOpe = "<dbd>" & TabIstr.DBD(1).nome & "</dbd><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
        Else
          wOpe = "<istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
        End If
      End If
     
      'AC 24/10/08
      'SENSEG
      If Not TabIstr.SenSeg = "" Then
        If TabIstr.SenSeg = "ALL" Then
          wOpe = wOpe & "<sseg></sseg>"
        Else
          wOpe = wOpe & "<sseg>" & TabIstr.SenSeg & "</sseg>"
        End If
      End If
     
      For k = 1 To UBound(TabIstr.BlkIstr)   ' ciclo su livelli
        ComboPLevelAppo = ComboPLevelTemp
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 28-07-06
        ' OK: REPL CON SSA QUALIFICATA DA RETURN CODE "AJ"
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 19-07-07
        ' REPL CON PATH-CALL (C.C. "D")!
        'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
          If TabIstr.BlkIstr(k).idsegm = -1 Then
            wOpe = wOpe & "<level" & k & "><segm>" & "DYNAMIC" & "</segm>"
          Else
            wOpe = wOpe & "<level" & k & "><segm>" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "</segm>"
            If NumToken(cloniSegmenti(k)) > 1 Then
              'varseg = TabIstr.BlkIstr(K).ssaName & "(1:8)"
              combSeg = "'" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "'"
              'varseg = buildSubstringFromType(TabIstr.BlkIstr(K).ssaName, "1", CStr(Len(combseg) - 2), TipoFile, "seg") '"8"
              'AC 06/03/08 se ho molteplicit� in segment ho nome della variabile
              varSeg = TabIstr.BlkIstr(k).nomeVarSeg
              moltLevelesimo(k) = True
            Else
              varSeg = ""
            End If
            aggiornalistaSeg (TabIstr.BlkIstr(k).segment)
          End If
        Else
          If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(TabIstr.BlkIstr) Then
            If TabIstr.BlkIstr(k).idsegm = -1 Then
              wOpe = wOpe & "<level1><segm>" & "DYNAMIC" & "</segm>"
            Else
              wOpe = wOpe & "<level1><segm>" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "</segm>"
              If NumToken(cloniSegmenti(k)) > 1 Then
                'varseg = TabIstr.BlkIstr(K).ssaName & "(1:8)"
                combSeg = "'" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "'"
                'varseg = buildSubstringFromType(TabIstr.BlkIstr(K).ssaName, "1", CStr(Len(combseg) - 2), TipoFile, "SSA") '"8"
                'AC 06/03/08 se ho molteplicit� in segment ho nome della variabile
                varSeg = TabIstr.BlkIstr(k).nomeVarSeg
                moltLevelesimo(k) = True
              Else
                varSeg = ""
              End If
              aggiornalistaSeg (TabIstr.BlkIstr(k).segment)
            End If
          End If
        End If
        
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "POS" Then 'IRIS-DB
          If TabIstr.BlkIstr(k).Record <> "" Then
            If Trim(TabIstr.BlkIstr(k).segment) <> "" Then
              wOpe = wOpe & "<area>" & "AREA" & "</area>"
            End If
          End If
        End If
        
        'AC 10/04/2007 spostata per non sommare a wopes la <ccode> del wope
        If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
          wOpeS = wOpe   ' allineiamo  la wOpeS  alla wOpe che si differenziano solo per ultimo livello
          'varop_s = varop
          'combop_s = combop
          For ks = 1 To k - 1
            CombPLevel_s(ks) = CombPLevel(ks)
          Next ks
          ' allineiamo le stringhe
        End If
                            
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
          If Trim(Replace(Replace(TokenIesimoCC(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod), "*", ""), "-", "")) <> "" Then
            If Trim(TabIstr.BlkIstr(k).segment) <> "" Then
              wOpe = wOpe & "<ccode>" & Trim(Replace(Replace(TokenIesimoCC(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod), "*", ""), "-", "")) & "</ccode>"
            End If
          End If
        End If
                
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
          If NumToken(TabIstr.BlkIstr(k).Search) > 1 Then
            'varcmd = TabIstr.BlkIstr(K).ssaName & "(9:" & Len(TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K))) & ")"
            varCmd = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, "9", Len(TokenIesimoCC(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod)), TipoFile, "SSA", pIdOggetto)
            combCmd = "'" & TokenIesimoCC(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod) & "'"
            If bolMultipleCcod Then
              moltLevelesimo(k) = True
            End If
          Else
            varCmd = ""
          End If
        End If
        '*********************************
                      
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' QUALIFICAZIONE:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' AC se entrambe le versione wOpe � quella qualificata e wOpeS quella
        ' squalificata, altrimenti se unica sempre in wOpe
        
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
          If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
            'parte SQUALIFICATA
            ' la wOpeS si ferma qui, wOpeS e wOpe differiscono solo per questo livello
           
            'parte QUALIFICATA
            wFlagBoolean = False
            ' determino se su livelli precedenti ho molteplicit� operatore
              If k > 1 Then
                For ij = 1 To k - 1
                  If moltLevelesimo(ij) = True Then
                    moltLevel = True
                    Exit For
                  End If
                Next
              End If
              ' ------------CICLO SU OPERATORI LOGICI---------------
              bolMoltOpLogic = False
              bolmoltBOL = False
              ultimoOperatore = False
              ReDim MoltOpLogic(UBound(TabIstr.BlkIstr(k).KeyDef))
              If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
                ReDim moltBOL(UBound(TabIstr.BlkIstr(k).KeyDef) - 1)
              End If
              ReDim CombPLevelOper(UBound(TabIstr.BlkIstr(k).KeyDef))
              For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
                'AC 12/07
                ' ripulisco la maschera del template COMBOPLEVELOPE
                ComboPLevelAppoOper = ComboPLevelTempOper
                ComboPLevelAppo = ComboPLevelTemp
                If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                  wOpe = wOpe & "<key>" & TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                  Operatore = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                  wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                  If multiOp Then
                    varOp = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                    ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                    If TipoFile = "PLI" Then
                      combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf & _
                                "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf & _
                                "   " & varOp & " = IRIS_" & Operatore & "3"
                    Else
                      combOp = "REV" & Operatore & "1" & vbCrLf & _
                               "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf & _
                               "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                    End If
                    moltLevelesimo(k) = True
                    MoltOpLogic(K1) = True
                  Else
                    varOp = ""
                  End If
                  If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                    OperatoreLogicoSimb = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, MatriceCombinazioniBOL(k, K1))
                    OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                    If multiOp Then
                      varLogicop = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                      combLogicop = "'" & OperatoreLogicoSimb & "'"
                      moltLevelesimo(k) = True
                      moltBOL(K1) = True
                    Else
                      varLogicop = ""
                    End If
                    If Not OperatoreLogico = ")" Then
                      wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                      wFlagBoolean = True
                    Else
                      ultimoOperatore = True
                    End If
                  Else
                    varLogicop = ""
                  End If
                  'AC 03/07/06
                  'ultimo
                  bolMoltOpLogic = False
                  bolmoltBOL = False
                  If K1 > 1 Then
                    For ij = 1 To K1 - 1
                      If MoltOpLogic(ij) Then
                        bolMoltOpLogic = True
                        Exit For
                      End If
                    Next ij
                    For ij = 1 To K1 - 1
                      If moltBOL(ij) Then
                        bolmoltBOL = True
                        Exit For
                      End If
                    Next ij
                  End If
                  '---------- molteplicit� operatore --------
                  If Not varOp = "" Then
                    ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", varOp)
                    ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp)
                    'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                    If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                    Else
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                    End If
'                    If QualAndUnqual Then
'                      ComboPLevelAppoOper = Replace(ComboPLevelAppo, "<I%IND-3>", "")
'                    End If
'                    CombPLevelOper(k1) = ComboPLevelAppoOper
'                  Else
'                    CombPLevelOper(k1) = ComboPLevelAppoOper
                  End If
                  ' booleano
                  '---------- molteplicit� booleana --------
                  If Not varLogicop = "" Then
                    ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", varLogicop)
                    ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop)
                    'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                    If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                    Else
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                    End If
                  End If
                
                  If QualAndUnqual Then
                    ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                  End If
                  CombPLevelOper(K1) = ComboPLevelAppoOper
                  '-------------------------------------------
                End If
                ' nel caso di parentesi sull'operatore logico interrompo il ciclo
                If ultimoOperatore Then Exit For
              Next K1
             
              '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
              Rtb.text = ComboPLevelAppo
              For jj = 1 To UBound(CombPLevelOper)
                MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
              Next jj
              ComboPLevelAppo = Rtb.text
             
              If Not varCmd = "" And bolMultipleCcod Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
                ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
                If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
                Else
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
                End If
                If QualAndUnqual Then
                  ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
                End If
                CombPLevel(k) = ComboPLevelAppo
              Else
                CombPLevel(k) = ComboPLevelAppo
              End If
              If Not varSeg = "" Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varSeg)
                ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg)
                If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
                Else
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
                End If
                If QualAndUnqual Then
                  ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
                End If
                CombPLevel(k) = ComboPLevelAppo
              Else
                CombPLevel(k) = ComboPLevelAppo
              End If
            Else  ' o la versione squalificata o quella squalificata
              ' AC il campo parentesi e la trasformazione in stringa del booleano Qualificazione
              If TabIstr.BlkIstr(k).Parentesi <> "(" Then ' Or Trim(TabIstr.BlkIstr(k).SsaName) = "<none>" Then
                OnlySqual = True
                ReDim CombPLevelOper(0)
              Else   ' Qualificata
                OnlyQual = True
                wFlagBoolean = False
                If k > 1 Then
                  For ij = 1 To k - 1
                    If moltLevelesimo(ij) Then
                      moltLevel = True
                      Exit For
                    End If
                  Next ij
                End If
                ' ----- CICLO OPERATORI LOGICI
                bolmoltBOL = False
                ultimoOperatore = False
                bolMoltOpLogic = False
                ReDim MoltOpLogic(UBound(TabIstr.BlkIstr(k).KeyDef))
                If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
                  ReDim moltBOL(UBound(TabIstr.BlkIstr(k).KeyDef) - 1)
                End If
                ReDim CombPLevelOper(UBound(TabIstr.BlkIstr(k).KeyDef))
                For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
                  ComboPLevelAppoOper = ComboPLevelTempOper
                  If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                    wOpe = wOpe & "<key>" & TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                    Operatore = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                    wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                    If multiOp Then  ' AC 020807 And Not TipIstr = "UP"
                      'varop = TabIstr.BlkIstr(K).ssaName & "(" & Int(TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
                      varOp = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                      ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                      If TipoFile = "PLI" Then
                        combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf & _
                                 "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf & _
                                 "   " & varOp & " = IRIS_" & Operatore & "3"
                      Else
                        combOp = "REV" & Operatore & "1" & vbCrLf & _
                                 "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf & _
                                 "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                      End If
                      moltLevelesimo(k) = True
                      MoltOpLogic(K1) = True
                    Else
                      varOp = ""
                    End If
                    If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                      OperatoreLogicoSimb = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, MatriceCombinazioniBOL(k, K1))
                      OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                      If multiOp Then    ' AC 020807 And Not TipIstr = "UP"
                        varLogicop = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                        combLogicop = "'" & OperatoreLogicoSimb & "'"
                        moltLevelesimo(k) = True
                        moltBOL(K1) = True
                      Else
                        varLogicop = ""
                      End If
                      If Not OperatoreLogico = ")" Then
                        wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                        wFlagBoolean = True
                      Else
                        ultimoOperatore = True
                      End If
                    Else
                      varLogicop = ""
                    End If
                    '------------------------------------------------------
                    bolMoltOpLogic = False
                    bolmoltBOL = False
                    If K1 > 1 Then
                      For ij = 1 To K1 - 1
                        If MoltOpLogic(ij) Then
                          bolMoltOpLogic = True
                          Exit For
                        End If
                      Next ij
                      For ij = 1 To K1 - 1
                        If moltBOL(ij) Then
                          bolmoltBOL = True
                          Exit For
                        End If
                      Next ij
                    End If
                    If Not varOp = "" Then
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VAROP>", varOp)
                      ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBOP>", combOp)
                      If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or bolmoltBOL Then 'AND -- alternativa 1
                        ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 1))
                      Else
                        ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<A%"), 2))
                      End If
'                      If QualAndUnqual Then
'                        ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
'                      End If
'                      CombPLevelOper(k1) = ComboPLevelAppoOper
'                    Else
'                      CombPLevelOper(k1) = ComboPLevelAppoOper
                    End If
                    '---------- molteplicit� booleana --------
                    If Not varLogicop = "" Then
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<VARLOGICOP>", varLogicop)
                      ComboPLevelAppoOper = ReplaceTagtoo(ComboPLevelAppoOper, "<COMBLOGICOP>", combLogicop)
                      'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
                      If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or bolMoltOpLogic Or Not varOp = "" Or bolmoltBOL Then 'AND -- alternativa 1
                        ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 1))
                      Else
                        ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, FindCompleteTag(ComboPLevelAppoOper, "<B%"), GetIdentificator(FindCompleteTag(ComboPLevelAppoOper, "<B%"), 2))
                      End If
                    End If
                  
                    If QualAndUnqual Then
                      ComboPLevelAppoOper = Replace(ComboPLevelAppoOper, "<I%IND-3>", "")
                    End If
                    CombPLevelOper(K1) = ComboPLevelAppoOper
                    '-------------------------------------------------------
                  End If
                  If ultimoOperatore Then Exit For
                Next K1
              End If '-- squal o qual
          
              '----- sostituzione in ComboPLevelAppo del tag ciclico <<combologicop(i)>>
              Rtb.text = ComboPLevelAppo
              For jj = 1 To UBound(CombPLevelOper)
                MadrdM_GenRout.changeTag Rtb, "<<COMBOLOGICOP(i)>>", CombPLevelOper(jj)
              Next
              ComboPLevelAppo = Rtb.text
              
              If Not varCmd = "" And bolMultipleCcod Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varCmd)
                ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combCmd)
                If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Then 'AND -- alternativa 1
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
                Else
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
                End If
                If QualAndUnqual Then
                  ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
                End If
                CombPLevel(k) = ComboPLevelAppo
              Else
                CombPLevel(k) = ComboPLevelAppo
              End If
              If Not varSeg = "" Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varSeg)
                ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combSeg)
                If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltLevel Or Not varOp = "" Or (Not varCmd = "" And bolMultipleCcod) Then 'AND -- alternativa 1
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
                Else
                  ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
                End If
                If QualAndUnqual Then
                  ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
                End If
                CombPLevel(k) = ComboPLevelAppo
              Else
                CombPLevel(k) = ComboPLevelAppo
              End If
            End If   '-- una sola versione o due versioni
          End If
          'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
          ' SQ 19-07-07
          ' REPL CON PATH-CALL (C.C. "D")!
          'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then 'IRIS-DB
            wOpe = wOpe & "</level" & k & ">"
            If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
              wOpeS = wOpeS & "</level" & k & ">"
            End If
          Else
            If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(TabIstr.BlkIstr) Then
              wOpe = wOpe & "</level1>"
              If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
                wOpeS = wOpeS & "</level1>"
              End If
            End If
          End If
        Next k   ' FINE CICLO LIVELLI
        wOpe = wOpe & "</livelli>"
        
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
          If Trim(TabIstr.keyfeedback) <> "" Then
            wOpe = wOpe & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
          End If
        End If
     
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
          If Len(TabIstr.procSeq) Then
            wOpe = wOpe & "<procseq>" & TabIstr.procSeq & "</procseq>"
          End If
        End If
    
        If xIstr = "OPEN" Then
          If Len(TabIstr.procOpt) Then
            wOpe = wOpe & "<procopt>" & TabIstr.procOpt & "</procopt>"
          End If
        End If
     
        'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
        ' ora � solo per il programma in esame
        pcbDyn = ""
        pcbDynInt = 0
        maxPcbDyn = 0
        wOpeSave = wOpe
        For k = 1 To UBound(OpeIstrRt)
          If wOpe = OpeIstrRt(k).Decodifica Then
            If OpeIstrRt(k).ordPcb = ordPcb Then
              'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
              'verifico se � la prima
              pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
              pcbDynInt = OpeIstrRt(k).pcbDyn
              maxPcbDyn = 0
              Exit For
            Else
              If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
                'trovata un'istruzione identica, ma con diverso pcb
                maxPcbDyn = OpeIstrRt(k).pcbDyn
              End If
            End If
          End If
        Next k
        'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
        If maxPcbDyn > 0 Then
          pcbDyn = Format(maxPcbDyn + 1, "000")
          pcbDynInt = maxPcbDyn + 1
        End If
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
          ' aggiunge il pcb dinamico
          If Len(pcbDyn) Then
            wOpe = wOpe & "<pcbseq>" & pcbDyn & "</pcbseq>"
            'AggLog "[Object: " & pIdOggetto & "] New Instruction: <pcbseq>" & pcbDyn & "</pcbseq>", MadrdF_Incapsulatore.RTErr
          Else
            ' se � la prima istruzione di quel tipo
            wOpe = wOpe & "<pcbseq>001</pcbseq>"
            pcbDynInt = 1
          End If
        End If
    
        wDecod = wOpe
        Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpe & "'")
        
        'If xope = "" Then
        If tb.EOF Then
          Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
          'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
          If tb1.EOF Then
            maxId = 1
          Else
            maxId = tb1(0) + 1
          End If
          tb.AddNew
          tb!IdIstruzione = maxId
          tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
          wIstr = tb!Codifica
          tb!Decodifica = wOpe
          tb!ImsDB = True
          tb!ImsDC = False
          tb!CICS = SwTp
          'stefano: routine multiple
          nomeRoutCompleto = Genera_Counter(nomeRoutine)
          tb!nomeRout = nomeRoutCompleto
          tb.Update
          ' Mauro 28/04/2009
'          'pcb dinamico
'          k = UBound(OpeIstrRt) + 1
'          ReDim Preserve OpeIstrRt(k)
'          OpeIstrRt(k).Decodifica = wOpeSave
'          OpeIstrRt(k).Istruzione = wIstr
'          OpeIstrRt(k).ordPcb = ordPcb
'          OpeIstrRt(k).pcbDyn = pcbDynInt
        Else
          wIstr = tb!Codifica
          nomeRoutCompleto = tb!nomeRout
          ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
          nomeRoutine = tb!nomeRout
        End If
        tb.Close
     
        'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
        'SQ CPY-ISTR
        'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
        Set tb = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrCodifica WHERE " & _
                                      "idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & _
                                      " and codifica = '" & wIstr & "'")
        If tb.EOF Then
          'SQ 16-11-06 - EMBEDDED
          'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & nomeroutcompleto & "')"
          'SQ CPY-ISTR
          'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeroutcompleto) & "')"
          m_fun.FnConnection.Execute "INSERT INTO PsDLI_IstrCodifica VALUES(" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeRoutCompleto) & "')"
          'Mauro 28/04/2009 : pcb dinamico
          k = UBound(OpeIstrRt) + 1
          ReDim Preserve OpeIstrRt(k)
          OpeIstrRt(k).Decodifica = wOpeSave
          OpeIstrRt(k).Istruzione = wIstr
          OpeIstrRt(k).ordPcb = ordPcb
          OpeIstrRt(k).pcbDyn = pcbDynInt
        End If
        tb.Close
        If QualAndUnqual Then
          wOpeS = wOpeS & "</livelli>"
          'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
            If Trim(TabIstr.keyfeedback) <> "" Then
              wOpeS = wOpeS & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
            End If
            'procseq: sarebbe meglio metterlo in tabistr, ma per ora facciamo cos� (procseq
            ' ci servir� anche nella generazione routine
            If UBound(TabIstr.psb) Then
              If Len(TabIstr.procSeq) Then
                wOpeS = wOpeS & "<procseq>" & TabIstr.procSeq & "</procseq>"
              End If
              If Len(TabIstr.procOpt) And xIstr = "OPEN" Then
                wOpeS = wOpeS & "<procopt>" & TabIstr.procOpt & "</procopt>"
              End If
            End If
          End If
 
          'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
          ' ora � solo per il programma in esame
          pcbDyn = ""
          pcbDynInt = 0
          maxPcbDyn = 0
          wOpeSave = wOpeS
          For k = 1 To UBound(OpeIstrRt)
            If wOpeS = OpeIstrRt(k).Decodifica Then
              If OpeIstrRt(k).ordPcb = ordPcb Then
                'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
                'verifico se � la prima
                pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
                pcbDynInt = OpeIstrRt(k).pcbDyn
                maxPcbDyn = 0
                Exit For
              Else
                If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
                  'trovata un'istruzione identica, ma con diverso pcb
                  maxPcbDyn = OpeIstrRt(k).pcbDyn
                End If
              End If
            End If
          Next k
          'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
          If maxPcbDyn > 0 Then
            pcbDyn = Format(maxPcbDyn + 1, "000")
            pcbDynInt = maxPcbDyn + 1
          End If
          ' aggiunge il pcb dinamico
          'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
            If Len(pcbDyn) Then
              wOpeS = wOpeS & "<pcbseq>" & pcbDyn & "</pcbseq>"
              'AggLog "[Object: " & pIdOggetto & "] New Instruction: <pcbseq>" & pcbDyn & "</pcbseq>", MadrdF_Incapsulatore.RTErr
            Else
              ' se � la prima istruzione di quel tipo
              wOpeS = wOpeS & "<pcbseq>" & "001" & "</pcbseq>"
              pcbDynInt = 1
            End If
          End If
     
          wDecodS = wOpeS
          Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpeS & "'")
           
          'If xope = "" Then
          If tb.EOF Then
            Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
            'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
            If tb1.EOF Then
              maxId = 1
            Else
              maxId = tb1(0) + 1
            End If
            tb.AddNew
            tb!IdIstruzione = maxId
            tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
            wIstrS = tb!Codifica
            tb!Decodifica = wOpeS
            tb!ImsDB = True
            tb!ImsDC = False
            tb!CICS = SwTp
            'stefano: routine multiple
            nomeRoutCompleto = Genera_Counter(nomeRoutine)
            tb!nomeRout = nomeRoutCompleto
            tb.Update
            ' Mauro 28/04/2009
'            'pcb dinamico
'            k = UBound(OpeIstrRt) + 1
'            ReDim Preserve OpeIstrRt(k)
'            OpeIstrRt(k).Decodifica = wOpeSave
'            OpeIstrRt(k).Istruzione = wIstr
'            OpeIstrRt(k).ordPcb = ordPcb
'            OpeIstrRt(k).pcbDyn = pcbDynInt
          Else
            wIstrS = tb!Codifica
            nomeRoutCompleto = tb!nomeRout
            ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
            nomeRoutine = tb!nomeRout
          End If
          tb.Close
          'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
          'virgilio CPY-ISTR
          'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
          Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica WHERE " & _
                                        "idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & _
                                        " and codifica = '" & wIstrS & "'")
          If tb.EOF Then
            'virgilio CPY-ISTR
            'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeroutcompleto & "')"
            m_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeRoutCompleto & "')"
            ' Mauro 28/04/2009 : pcb dinamico
            k = UBound(OpeIstrRt) + 1
            ReDim Preserve OpeIstrRt(k)
            OpeIstrRt(k).Decodifica = wOpeSave
            OpeIstrRt(k).Istruzione = wIstr
            OpeIstrRt(k).ordPcb = ordPcb
            OpeIstrRt(k).pcbDyn = pcbDynInt
          End If
          tb.Close
        End If
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        'BACO: per ora non viene gestita la molteplicit� sulla ISRT per i livelli qualificati
        ' ma in realt� non ha senso (di solito l'inserimento viene fatto qualificando un
        ' livello precedente per uguale, in modo da puntare un solo parent
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
          combinazioni = AggiornaCombinazione(maxLogicOp)
        Else
          combinazioni = False
        End If
        moltLevel = False
        ' aggiornamento collection
        If Not StringToClear = "" Then
          IstrIncaps = Replace(IstrIncaps, StringToClear, "")
          IstrSIncaps = Replace(IstrSIncaps, StringToClear, "")
        End If
     
        If QualAndUnqual Then
          ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
          checkColl = True
          For j = 1 To wMoltIstr(3).Count
            If wMoltIstr(3).item(j) = wIstr Then
              checkColl = False
              Exit For
            End If
          Next j
          If checkColl Then
            wMoltIstr(1).Add IstrIncaps, wDecod
            wMoltIstr(3).Add wIstr, wDecod
            aggiornalistaCodOp (wIstr)
            y = y + 1
            ReDim Preserve wNomeRout(y)
            wNomeRout(y) = nomeRoutCompleto
          End If
          
          ' 0 e 2  da wOpeS, indice ks dell'array e da IstrSIncaps
          checkColl = True
          checksqual = True
          For j = 1 To wMoltIstr(2).Count
'            If wMoltIstr(2).Item(j) = OpeIstrRt(ks).Istruzione Then
            If wMoltIstr(2).item(j) = wIstrS Then
              checkColl = False
              checksqual = False
              Exit For
            End If
          Next j
          If checkColl Then
            wMoltIstr(0).Add IstrSIncaps, wDecod
            wMoltIstr(2).Add wIstrS, wDecodS
            aggiornalistaCodOp (wIstrS)
            y = y + 1
            ReDim Preserve wNomeRout(y)
            wNomeRout(y) = nomeRoutCompleto
          End If
        Else
          ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
          checkColl = True
          For j = 1 To wMoltIstr(3).Count
'            If wMoltIstr(3).Item(j) = OpeIstrRt(k).Istruzione Then
            If wMoltIstr(3).item(j) = wIstr Then
              checkColl = False
              Exit For
            End If
          Next j
          If checkColl Then
            wMoltIstr(1).Add IstrIncaps, wDecod
            wMoltIstr(3).Add wIstr, wDecod
            aggiornalistaCodOp (wIstr)
            y = y + 1
            ReDim Preserve wNomeRout(y)
            wNomeRout(y) = nomeRoutCompleto
          End If
        End If
     
        ' livello COMBQUAL -- costruisce elemento array combqual
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL_EXEC"
        ' nel caso di linguaggi con then (vedi PLI), sui template gestiamo la riga dello then con il tag <then>
        ' negli altri casi l'if sotto � ininfluente
        For ij = 1 To UBound(moltLevelesimo)
          If moltLevelesimo(ij) = True Then
            moltLevThen = True
            Exit For
          End If
        Next ij
        If QualAndUnqual Or UBound(cloniIstruzione) > 0 Or moltLevThen Then
          MadrdM_GenRout.changeTag Rtb, "<then>", ""
        End If
        If QualAndUnqual Then
          MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
          MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        Else
          ' gestisco indentazione move
          If multiOpGen Or UBound(cloniIstruzione) > 0 Then ' ne lascio solo una
            MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
          End If
        End If
      
        'if or end per l'istruzione e relativo end if di chiusura
        If QualAndUnqual Then
          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
        Else
          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 2)
          ' end if
          ' AC 18/06/07  nel caso che le molteplicit� siano ignorate (vedi ISRT)
          ' non devo lasciare l'end if finale
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB  'AC 200707
            If multiOpGen Or UBound(cloniIstruzione) > 0 Then
              MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
            Else
               MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
            End If
          ElseIf UBound(cloniIstruzione) > 0 Then ' potremmo avere molteplicit� istr con istr diverse da GU,GN e GP
            MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
          Else
            MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
          End If
        End If
        
        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPERB>", Replace(wIstr, ".", "")
      
        If EmbeddedRoutine Then
          MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
        Else
          MadrdM_GenRout.changeTag Rtb, "<standard>", ""
        End If
        
        For k = 1 To UBound(CombPLevel)
          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
        Next k
        
        appocomb = Rtb.text
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFQUAL"
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFQUAL"
        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
        ReDim Preserve CombQual(jk)
        CombQual(jk) = Rtb.text
      
      If QualAndUnqual And checksqual Then ' due parti, la seconda � squalificata
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMBQUAL_EXEC"
        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varIstr, True
        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combIstr, True
        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutCompleto
        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstrS
        wIstrsOld = wIstrS
        If EmbeddedRoutine Then
          MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
        Else
          MadrdM_GenRout.changeTag Rtb, "<standard>", ""
        End If
        For k = 1 To UBound(CombPLevel_s)
          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel_s(k), True
        Next
        appocomb = Rtb.text
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IFSQUAL"
        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
        ReDim Preserve CombSqual(UBound(CombSqual) + 1)
        CombSqual(UBound(CombSqual)) = Rtb.text
      ElseIf Not QualAndUnqual Then
'        ' anche nella parte squalificata metto la stessa cosa della qualificata
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
'        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
'        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
'        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
'        For k = 1 To UBound(CombPLevel_s)
'          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
'        Next
'        appocomb = Rtb.text
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
'        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
'        ReDim Preserve CombSqual(jk)
'        CombSqual(jk) = Rtb.text
      End If
      jk = jk + 1
      '**********
    Wend
  Next i  'ciclo su molteplicit� istruzioni
  'sostituzioni nel file principale
  
  'stefanopul
  'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\MAINCOMB"
  Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\MAINCOMB"
  MadrdM_GenRout.changeTag Rtb, "<SSA-INSPECT>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).ssaName & "(9:" & Len(TokenIesimoCC(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Search, 1, bolMultipleCcod)) + 1 & ")"
  For i = 1 To UBound(CombQual)
    MadrdM_GenRout.changeTag Rtb, "<<IFQUAL(i)>>", CombQual(i)
  Next i
  If QualAndUnqual Then
    For i = 1 To UBound(CombSqual)
      MadrdM_GenRout.changeTag Rtb, "<<IFSQUAL(i)>>", CombSqual(i)
    Next i
    MadrdM_GenRout.changeTag Rtb, "<condsqual>", "" ' altrimenti istr corrispondenti eliminate
  End If
  appoFinalString = Rtb.text
  
  While InStr(appoFinalString, vbCrLf) > 0
    posEndLine = InStr(appoFinalString, vbCrLf)
    insertLine Testo, Left(appoFinalString, posEndLine - 1), pIdOggetto, indentkey
    appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
  Wend
  If Not Trim(appoFinalString) = "" Then
    insertLine Testo, appoFinalString, pIdOggetto, indentkey
  End If
  Close #nFBms
  
  Exit Function
ErrH:
  MsgBox err.Description
  'insertLine testo, Rtb.text
  Resume Next
End Function
Function getIstruzione_ETZ(xIstr As String, Index As Integer, idpgm As Long, _
                           pIdOggetto As Long, pRiga As Long, ordPcb As Long) As String
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''   copia della funzione ''''CostruisciOperazioneTemplate'''' alleggerita ''''
''''   ritorna l'istruzione                                                  ''''
''''   es. 'GU.0001'                                                         ''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim tb, tb1 As Recordset
  Dim wOpe As String, wOpeS As String, TipIstr As String
  Dim k As Long
  Dim xope As String
  Dim K1 As Integer
  Dim wUtil As Boolean, wFlagBoolean As Boolean
  Dim i As Integer, j As Integer
  Dim checkColl As Boolean
  Dim extraIndent As Integer
  Dim maxId As Long
  Dim wIstr, wDecod As String, wIstrS As String, wDecodS As String, pcbDyn As String
  Dim maxPcbDyn As Integer, pcbDynInt As Integer
  Dim wOpeSave As String
  Dim y As Integer
  ' per comporre stringa if dell'incapsulato, versione qual e squal
  Dim IstrIncaps As String, IstrSIncaps As String, StrAnd As String
  ' contiene la molteplicit� di istruzioni, attinge dalla tabella Istruzioni_Clone
  Dim cloniIstruzione() As String
  Dim combinazioni As Boolean
  Dim maxLogicOp As Integer, maxBoolean As Integer
  Dim Operatore As String
  Dim StringToClear As String
  Dim ifAnd As String
  Dim nomeRoutCompleto As String
  
  'AC 02/11/06
  
  Dim varOp As String, combOp As String, varIstr As String, combIstr As String
  Dim varop_s As String, combop_s As String, ks As Integer
  
  Dim CombPLevel() As String, CombPLevel_s() As String, CombSqual() As String, CombQual() As String, wIstrsOld As String
  
  Dim ComboPLevelTemp As String, ComboPLevelAppo As String, nFBms As Integer, wRecIn As String, jk As Integer
  Dim Rtb  As RichTextBox, appocomb As String, appoFinalString As String, checksqual As Boolean, posEndLine As String
    
  jk = 1
   
  For k = 1 To UBound(TabIstr.BlkIstr)
    If UBound(TabIstr.BlkIstr(k).KeyDef) > maxLogicOp Then
     maxLogicOp = UBound(TabIstr.BlkIstr(k).KeyDef)
    End If
  Next
  
  ReDim MatriceCombinazioni(UBound(TabIstr.BlkIstr), maxLogicOp)
  ReDim CombinazioniOpLogici(UBound(TabIstr.BlkIstr), maxLogicOp)
  ReDim CombinazioniComCode(UBound(TabIstr.BlkIstr))
  
  For k = 1 To UBound(TabIstr.BlkIstr)
    For K1 = 1 To maxLogicOp
      MatriceCombinazioni(k, K1) = 1
      CombinazioniOpLogici(k, K1) = 1
    Next
  Next
  For k = 1 To UBound(TabIstr.BlkIstr)
    CombinazioniComCode(k) = 1
  Next
  CreaArrayCloniIstruzione cloniIstruzione, pIdOggetto, pRiga
  
  '***** determiniamo se deve essere generata
  '***** sia la versione qualificata che squalificata
  Set tb = m_fun.Open_Recordset("select * from PsDLI_IstrDett_Liv where IdOggetto =" & pIdOggetto & " and Riga = " & pRiga & " and Livello = " & UBound(TabIstr.BlkIstr))
  If Not tb.EOF Then
    QualAndUnqual = tb!QualAndUnqual
  Else
    QualAndUnqual = False
  End If
  tb.Close
  
  Set wMoltIstr(0) = New Collection
  Set wMoltIstr(1) = New Collection
  Set wMoltIstr(2) = New Collection
  Set wMoltIstr(3) = New Collection
  StringToClear = ""
  ReDim wNomeRout(0)
  
  ' Ciclo esterno su istruzione
  ReDim Preserve CombSqual(0)
  
  For i = 0 To UBound(cloniIstruzione)
    xIstr = cloniIstruzione(i)
    TipIstr = Left(xIstr, 2)
    If TabIstr.isGsam Then
       TipIstr = "GS"
    ElseIf xIstr = "GU" Or xIstr = "GHU" Then
       TipIstr = "GU"
    ElseIf xIstr = "ISRT" Then
      TipIstr = "UP"
    ElseIf xIstr = "POS" Then 'IRIS-DB
      TipIstr = "PS" 'IRIS-DB
    ElseIf xIstr = "DLET" Then
       TipIstr = "UP"
    ElseIf xIstr = "REPL" Then
       TipIstr = "UP"
    ElseIf xIstr = "GNP" Or xIstr = "GHNP" Then
       TipIstr = "GP"
    ElseIf xIstr = "GN" Or xIstr = "GHN" Then
       TipIstr = "GN"
    ElseIf xIstr = "PCB" Then
       wUtil = True
       TipIstr = "SH"
    ElseIf xIstr = "TERM" Then
       wUtil = True
       TipIstr = "TM"
    ElseIf xIstr = "CHKP" Then
       wUtil = True
       TipIstr = "CP"
    ElseIf xIstr = "QRY" Then
       wUtil = True
       TipIstr = "QY"
    ElseIf xIstr = "SYNC" Then
       wUtil = True
       TipIstr = "SY"
    Else
       'm_fun.WriteLog "Incapsulatore - Costruisci Operazione " & vbCrLf & "Istruzione non riconosciuta " & xIstr, "DR - Incapsulatore"
    End If
  
    'riapplico il calcolo del NomeRout per ogni Molteplicit� di istruzione
    If UBound(TabIstr.DBD) > 0 Then
      If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
        If InStr(TabIstr.DBD(1).nome, "RKKUNL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKLUNL", xIstr, SwTp)
        ElseIf InStr(TabIstr.DBD(1).nome, "RKGIRL") Then
          nomeRoutine = m_fun.Genera_Nome_Routine("RKOIRL", xIstr, SwTp)
        Else
          nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, xIstr, SwTp)
        End If
      Else
        nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, xIstr, SwTp)
        'aggiornalistaDB (TabIstr.Dbd(1).nome)
      End If
    Else
      nomeRoutine = m_fun.Genera_Nome_Routine("", xIstr, SwTp)
    End If
   
   'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
   'stefano: ho dovuta commentarla perch� creava delle istruzioni non valide (AND senza IF) quando
   ' c'� molteplicit� di squalificata insieme alla molteplicit� di funzione; fondamentalmente la molteplicit�
   ' di funzione viene gestita sotto quella di squalificata; dovrebbe essere invertita o creato un vettore per ogni
   ' occorrenza di funzione.
   'If TipIstr <> "GU" And TipIstr <> "GN" And TipIstr <> "GP" Then QualAndUnqual = False
    
   combinazioni = True
   IstrIncaps = ""
   Dcombination = False
   
    While combinazioni
   
    'AC 02/11/06
    ReDim CombPLevel(UBound(TabIstr.BlkIstr))
    ' alberto, sulle istruzioni OPEN e CLSE dava errore per cui l'ho cambiata..
    If UBound(TabIstr.BlkIstr) > 0 Then
      ReDim CombPLevel_s(UBound(TabIstr.BlkIstr) - 1)
    End If
    IstrIncaps = ""
    If UBound(cloniIstruzione) > 0 Then
       varIstr = Restituisci_varFunz(pIdOggetto, pRiga)
       combIstr = "'" & xIstr & "'"
    End If
    If isOldIncapsMethod Then
      wOpe = "<pgm>" & nomeRoutine & "</pgm><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
    Else
      wOpe = "<dbd>" & TabIstr.DBD(1).nome & "</dbd><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
    End If
     
    For k = 1 To UBound(TabIstr.BlkIstr)   ' ciclo su livelli
      ComboPLevelAppo = ComboPLevelTemp
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' SQ 28-07-06
      ' OK: REPL CON SSA QUALIFICATA DA RETURN CODE "AJ"
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
        If TabIstr.BlkIstr(k).idsegm = -1 Then
           wOpe = wOpe & "<level" & k & "><segm>" & "DYNAMIC" & "</segm>"
        Else
           wOpe = wOpe & "<level" & k & "><segm>" & TabIstr.BlkIstr(k).segment & "</segm>"
           
           aggiornalistaSeg (TabIstr.BlkIstr(k).segment)
        End If
      Else
        If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(TabIstr.BlkIstr) Then
          If TabIstr.BlkIstr(k).idsegm = -1 Then
             wOpe = wOpe & "<level1><segm>" & "DYNAMIC" & "</segm>"
          Else
             wOpe = wOpe & "<level1><segm>" & TabIstr.BlkIstr(k).segment & "</segm>"
             aggiornalistaSeg (TabIstr.BlkIstr(k).segment)
          End If
        End If
      End If
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "POS" Then 'IRIS-DB
        If TabIstr.BlkIstr(k).Record <> "" Then
           If Trim(TabIstr.BlkIstr(k).segment) <> "" Then
               wOpe = wOpe & "<area>" & "AREA" & "</area>"
           End If
        End If
      End If
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
        If Trim(Replace(Replace(TokenIesimo(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k)), "*", ""), "-", "")) <> "" Then
           If Trim(TabIstr.BlkIstr(k).segment) <> "" Then
              wOpe = wOpe & "<ccode>" & Trim(Replace(Replace(TokenIesimo(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k)), "*", ""), "-", "")) & "</ccode>"
           End If
        End If
      End If
        
      'AC 03/07/06
      'IstrIncaps deve tenere conto del controllo sul command code
      'If SSAName(9:Len(Search))
      ' lo metto prima degli operatori
      'ultimo
      'If k > 1 Then ifAnd = "AND" 'Else determinato dalla presenza o meno di istr multipla
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
        If NumToken(TabIstr.BlkIstr(k).Search) > 1 Then
          varOp = TabIstr.BlkIstr(k).ssaName & "(9:" & Len(TokenIesimo(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k))) & ")"
          combOp = "'" & TokenIesimo(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k)) & "'"
        End If
      End If
          '*********************************
      If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
        wOpeS = wOpe   ' allineiamo  la wOpeS  alla wOpe che si differenziano solo per ultimo livello
        'varop_s = varop
        'combop_s = combop
        For ks = 1 To k - 1
          CombPLevel_s(ks) = CombPLevel(ks)
        Next
        ' allineiamo le stringhe
      End If
        
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' QUALIFICAZIONE:
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' AC se entrambe le versione wOpe � quella qualificata e wOpeS quella
      ' squalificata, altrimenti se unica sempre in wOpe
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or (xIstr = "ISRT" And k < UBound(TabIstr.BlkIstr)) Or xIstr = "POS" Then 'IRIS-DB
       If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
        'parte SQUALIFICATA
        ' la wOpeS si ferma qui, wOpeS e wOpe differiscono solo per questo livello
         
        'parte QUALIFICATA
         wFlagBoolean = False
         For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
           If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) Then
             If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
               wOpe = wOpe & "<key>" & TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
               Operatore = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
               wOpe = wOpe & "<oper>" & Operatore & "</oper>"
               wOpe = wOpe & "<bool>" & transcode_booleano(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) & "</bool>"
               wFlagBoolean = True
             Else
               wOpe = wOpe & "<key>" & TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
               Operatore = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
               wOpe = wOpe & "<oper>" & Operatore & "</oper>"
             End If
           'AC 03/07/06
           'ultimo
             If multiOp Then
               ' da riparametrizzare
               'If (wIdent + extraIndent + Len(ifAnd) + 38 + Len(TabIstr.BlkIstr(k).SSAName)) > 72 Then
               varOp = TabIstr.BlkIstr(k).ssaName & "(" & Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2 & ":2)"
               combOp = "IRIS" & Operatore & "1" & vbCrLf _
               & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     OR REV" & Operatore & "2" & vbCrLf _
               & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     OR REV" & Operatore & "3"
            End If
          End If
        Next K1
        If Not varOp = "" Then
          ComboPLevelAppo = Replace(ComboPLevelAppo, "<VAROP>", varOp)
          ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBOP>", combOp)
          If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or k > 1 Then 'AND -- alternativa 1
            ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 1))
          Else
            ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 2))
          End If
          If QualAndUnqual Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
          End If
          CombPLevel(k) = ComboPLevelAppo
        Else
          CombPLevel(k) = ComboPLevelAppo
        End If
      Else  ' o la versione squalificata o quella squalificata
          ' AC il campo parentesi e la trasformazione in stringa del booleano Qualificazione
        If TabIstr.BlkIstr(k).Parentesi <> "(" Then ' Or Trim(TabIstr.BlkIstr(k).SsaName) = "<none>" Then
           OnlySqual = True
        Else   ' Qualificata
          OnlyQual = True
          wFlagBoolean = False
          For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
            If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) Then
              If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                 wOpe = wOpe & "<key>" & TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                 Operatore = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                 wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                 wOpe = wOpe & "<bool>" & transcode_booleano(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) & "</bool>"
                 wFlagBoolean = True
              Else
                 wOpe = wOpe & "<key>" & TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                 Operatore = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                 wOpe = wOpe & "<oper>" & Operatore & "</oper>"
              End If
              If multiOp Then
                 varOp = TabIstr.BlkIstr(k).ssaName & "(" & Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2 & ":2)"
                 combOp = "IRIS" & Operatore & "1" & vbCrLf _
                 & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     OR REV" & Operatore & "2" & vbCrLf _
                 & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     OR REV" & Operatore & "3"
              End If
            End If
          Next K1
        End If '-- squal o qual
        If Not varOp = "" Then
          ComboPLevelAppo = Replace(ComboPLevelAppo, "<VAROP>", varOp)
          ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBOP>", combOp)
          If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or k > 1 Then 'AND -- alternativa 1
            ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 1))
          Else
            ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 2))
          End If
          If QualAndUnqual Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
          End If
          CombPLevel(k) = ComboPLevelAppo
        Else
          CombPLevel(k) = ComboPLevelAppo
        End If
      End If   '-- una sola versione o due versioni
    End If
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
     wOpe = wOpe & "</level" & k & ">"
     If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
       wOpeS = wOpeS & "</level" & k & ">"
     End If
    Else
     If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(TabIstr.BlkIstr) Then
       wOpe = wOpe & "</level1>"
       If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
         wOpeS = wOpeS & "</level1>"
       End If
     End If
    End If
  Next k   ' FINE CICLO LIVELLI
     
  wOpe = wOpe & "</livelli>"
  
  'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
  If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
    If Trim(TabIstr.keyfeedback) <> "" Then
      wOpe = wOpe & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
    End If
  End If
  
  'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
  If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
    If Len(TabIstr.procSeq) Then
      wOpe = wOpe & "<procseq>" & TabIstr.procSeq & "</procseq>"
    End If
  End If

  If xIstr = "OPEN" Then
    If Len(TabIstr.procOpt) Then
      wOpe = wOpe & "<procopt>" & TabIstr.procOpt & "</procopt>"
    End If
  End If
     
  'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
  ' ora � solo per il programma in esame
    pcbDyn = ""
    pcbDynInt = 0
    maxPcbDyn = 0
    wOpeSave = wOpe
    For k = 1 To UBound(OpeIstrRt)
       If wOpe = OpeIstrRt(k).Decodifica Then
          If OpeIstrRt(k).ordPcb = ordPcb Then
             'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
             'verifico se � la prima
             pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
             pcbDynInt = OpeIstrRt(k).pcbDyn
             maxPcbDyn = 0
             Exit For
          Else
             If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
             'trovata un'istruzione identica, ma con diverso pcb
                maxPcbDyn = OpeIstrRt(k).pcbDyn
             End If
          End If
       End If
    Next k
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
    If maxPcbDyn > 0 Then
       pcbDyn = Format(maxPcbDyn + 1, "000")
       pcbDynInt = maxPcbDyn + 1
    End If
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
     ' aggiunge il pcb dinamico
      If Len(pcbDyn) Then
         wOpe = wOpe & "<pcbseq>" & pcbDyn & "</pcbseq>"
         'AggLog "[Object: " & pIdOggetto & "] New Instruction: <pcbseq>" & pcbDyn & "</pcbseq>", MadrdF_Incapsulatore.RTErr
      Else
      ' se � la prima istruzione di quel tipo
         wOpe = wOpe & "<pcbseq>" & "001" & "</pcbseq>"
         pcbDynInt = 1
      End If
    End If
    
    wDecod = wOpe
    Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpe & "'")
     
    'If xope = "" Then
    If tb.EOF Then
      Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
      'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
      If tb1.EOF Then
         maxId = 1
      Else
         maxId = tb1(0) + 1
      End If
      tb.AddNew
      tb!IdIstruzione = maxId
      tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
      wIstr = tb!Codifica
      tb!Decodifica = wOpe
      tb!ImsDB = True
      tb!ImsDC = False
      tb!CICS = SwTp
  'stefano: routine multiple
      nomeRoutCompleto = Genera_Counter(nomeRoutine)
      tb!nomeRout = nomeRoutCompleto
      tb.Update
      tb.Close
      ' Mauro 28/04/2009
''   'pcb dinamico
''      k = UBound(OpeIstrRt) + 1
''      ReDim Preserve OpeIstrRt(k)
''      OpeIstrRt(k).Decodifica = wOpeSave
''      OpeIstrRt(k).Istruzione = wIstr
''      OpeIstrRt(k).ordPcb = ordPcb
''      OpeIstrRt(k).pcbDyn = pcbDynInt
    Else
      wIstr = tb!Codifica
      nomeRoutCompleto = tb!nomeRout
      ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
      nomeRoutine = tb!nomeRout
      tb.Close
    End If
     
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     'SQ CPY-ISTR
     'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
    Set tb = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrCodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
    If tb.EOF Then
       'SQ 16-11-06 - EMBEDDED
       'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & nomeroutcompleto & "')"
       'SQ CPY-ISTR
       'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeroutcompleto) & "')"
       m_fun.FnConnection.Execute "INSERT INTO PsDLI_IstrCodifica VALUES(" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeRoutCompleto) & "')"
       ' Mauro 28/04/2009 : pcb dinamico
       k = UBound(OpeIstrRt) + 1
       ReDim Preserve OpeIstrRt(k)
       OpeIstrRt(k).Decodifica = wOpeSave
       OpeIstrRt(k).Istruzione = wIstr
       OpeIstrRt(k).ordPcb = ordPcb
       OpeIstrRt(k).pcbDyn = pcbDynInt
    End If
    tb.Close
    If QualAndUnqual Then
      wOpeS = wOpeS & "</livelli>"
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
        If Trim(TabIstr.keyfeedback) <> "" Then
          wOpeS = wOpeS & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
        End If
'procseq: sarebbe meglio metterlo in tabistr, ma per ora facciamo cos� (procseq
' ci servir� anche nella generazione routine
        If UBound(TabIstr.psb) Then
          If Len(TabIstr.procSeq) Then
            wOpeS = wOpeS & "<procseq>" & TabIstr.procSeq & "</procseq>"
          End If
          If Len(TabIstr.procOpt) And xIstr = "OPEN" Then
            wOpeS = wOpeS & "<procopt>" & TabIstr.procOpt & "</procopt>"
          End If
        End If
     End If
    'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
    ' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpeS
     For k = 1 To UBound(OpeIstrRt)
       If wOpeS = OpeIstrRt(k).Decodifica Then
         If OpeIstrRt(k).ordPcb = ordPcb Then
           'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
           'verifico se � la prima
           pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
           pcbDynInt = OpeIstrRt(k).pcbDyn
           maxPcbDyn = 0
           Exit For
         Else
           If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
           'trovata un'istruzione identica, ma con diverso pcb
              maxPcbDyn = OpeIstrRt(k).pcbDyn
           End If
         End If
       End If
     Next k
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
     If maxPcbDyn > 0 Then
        pcbDyn = Format(maxPcbDyn + 1, "000")
        pcbDynInt = maxPcbDyn + 1
     End If
     ' aggiunge il pcb dinamico
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then 'IRIS-DB
      If Len(pcbDyn) Then
         wOpeS = wOpeS & "<pcbseq>" & pcbDyn & "</pcbseq>"
         'AggLog "[Object: " & pIdOggetto & "] New Instruction: <pcbseq>" & pcbDyn & "</pcbseq>", MadrdF_Incapsulatore.RTErr
      Else
      ' se � la prima istruzione di quel tipo
         wOpeS = wOpeS & "<pcbseq>" & "001" & "</pcbseq>"
         pcbDynInt = 1
      End If
     End If
     
     wDecodS = wOpeS
     Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpeS & "'")
     
     'If xope = "" Then
     If tb.EOF Then
         

       Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
          maxId = 1
       Else
          maxId = tb1(0) + 1
       End If
       tb.AddNew
       tb!IdIstruzione = maxId
       tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
       wIstrS = tb!Codifica
       tb!Decodifica = wOpeS
       tb!ImsDB = True
       tb!ImsDC = False
       tb!CICS = SwTp
   'stefano: routine multiple
       nomeRoutCompleto = Genera_Counter(nomeRoutine)
       tb!nomeRout = nomeRoutCompleto
       tb.Update
       tb.Close
       ' Mauro 28/04/2009
''    'pcb dinamico
''       k = UBound(OpeIstrRt) + 1
''       ReDim Preserve OpeIstrRt(k)
''       OpeIstrRt(k).Decodifica = wOpeSave
''       OpeIstrRt(k).Istruzione = wIstr
''       OpeIstrRt(k).ordPcb = ordPcb
''       OpeIstrRt(k).pcbDyn = pcbDynInt
     Else
       wIstrS = tb!Codifica
       nomeRoutCompleto = tb!nomeRout
       ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
       nomeRoutine = tb!nomeRout
       tb.Close
     End If
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
     If tb.EOF Then
       m_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeRoutCompleto & "')"
       ' Mauro 28/04/2009 : pcb dinamico
       k = UBound(OpeIstrRt) + 1
       ReDim Preserve OpeIstrRt(k)
       OpeIstrRt(k).Decodifica = wOpeSave
       OpeIstrRt(k).Istruzione = wIstr
       OpeIstrRt(k).ordPcb = ordPcb
       OpeIstrRt(k).pcbDyn = pcbDynInt
     End If
     tb.Close
    End If
    'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    'BACO: per ora non viene gestita la molteplicit� sulla ISRT per i livelli qualificati
    ' ma in realt� non ha senso (di solito l'inserimento viene fatto qualificando un
    ' livello precedente per uguale, in modo da puntare un solo parent
    If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "POS" Then 'IRIS-DB
     combinazioni = AggiornaCombinazione_ETZ(maxLogicOp)
    Else
     combinazioni = False
    End If
     
     ' aggiornamento collection
     If Not StringToClear = "" Then
      IstrIncaps = Replace(IstrIncaps, StringToClear, "")
      IstrSIncaps = Replace(IstrSIncaps, StringToClear, "")
     End If
     
     If QualAndUnqual Then
       ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
       checkColl = True
       
       For j = 1 To wMoltIstr(3).Count
        
        If wMoltIstr(3).item(j) = wIstr Then
          checkColl = False
          
          Exit For
        End If
       Next
       If checkColl Then
        wMoltIstr(1).Add IstrIncaps, wDecod
        wMoltIstr(3).Add wIstr, wDecod
        aggiornalistaCodOp (wIstr)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeRoutCompleto
       End If
       
       ' 0 e 2  da wOpeS, indice ks dell'array e da IstrSIncaps
       checkColl = True
       checksqual = True
       For j = 1 To wMoltIstr(2).Count
'        If wMoltIstr(2).Item(j) = OpeIstrRt(ks).Istruzione Then
        If wMoltIstr(2).item(j) = wIstrS Then
          checkColl = False
          checksqual = False
          Exit For
        End If
       Next
       If checkColl Then
        wMoltIstr(0).Add IstrSIncaps, wDecod
        wMoltIstr(2).Add wIstrS, wDecodS
        aggiornalistaCodOp (wIstrS)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeRoutCompleto
       End If
     Else
         ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
         checkColl = True
         For j = 1 To wMoltIstr(3).Count
'          If wMoltIstr(3).Item(j) = OpeIstrRt(k).Istruzione Then
          If wMoltIstr(3).item(j) = wIstr Then
            checkColl = False
            Exit For
          End If
         Next
         If checkColl Then
          wMoltIstr(1).Add IstrIncaps, wDecod
          wMoltIstr(3).Add wIstr, wDecod
          aggiornalistaCodOp (wIstr)
          y = y + 1
          ReDim Preserve wNomeRout(y)
          wNomeRout(y) = nomeRoutCompleto
         End If
       
     End If
     ' livello COMBQUAL -- costruisce elemento array combqual
'stefanopul
'''''''      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
'''''''      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\COMBQUAL"
'''''''      If QualAndUnqual Then
'''''''        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
'''''''        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
'''''''      Else
'''''''        ' gestisco indentazione move
'''''''        If multiOp Or UBound(cloniIstruzione) > 0 Then ' ne lascio solo una
'''''''          MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
'''''''        End If
'''''''      End If
'''''''
'''''''      'if or end per l'istruzione e relativo end if di chiusura
'''''''      If QualAndUnqual Then
'''''''        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
'''''''        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
'''''''      Else
'''''''        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 2)
'''''''        ' end if
'''''''        If multiOp Or UBound(cloniIstruzione) > 0 Then
'''''''          MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
'''''''        Else
'''''''           MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
'''''''        End If
'''''''      End If
      
'''''''      MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
'''''''      MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
'''''''      MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
'''''''      MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
'''''''      MadrdM_GenRout.changeTag Rtb, "<CODICEOPERB>", Replace(wIstr, ".", "")
'''''''
'''''''      If EmbeddedRoutine Then
'''''''        MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
'''''''      Else
'''''''        MadrdM_GenRout.changeTag Rtb, "<standard>", ""
'''''''      End If
      
''''''
''''''      For K = 1 To UBound(CombPLevel)
''''''        MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(K), True
''''''      Next
      
      
      
''''''      appocomb = Rtb.text
'''''''stefanopul
''''''      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFQUAL"
''''''      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\IFQUAL"
''''''      MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
''''''      ReDim Preserve CombQual(jk)
''''''      CombQual(jk) = Rtb.text
''''''
''''''      If QualAndUnqual And checksqual Then ' due parti, la seconda � squalificata
''''''        'stefanopul
''''''        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
''''''        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\COMBQUAL"
''''''        MadrdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
''''''        MadrdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
''''''        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
''''''        MadrdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
''''''        MadrdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
''''''        MadrdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
''''''        MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
''''''        MadrdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstrS
''''''        wIstrsOld = wIstrS
''''''        For K = 1 To UBound(CombPLevel_s)
''''''          MadrdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel_s(K), True
''''''        Next
''''''        appocomb = Rtb.text
''''''        'stefanopul
''''''        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
''''''        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\IFSQUAL"
''''''        MadrdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
''''''        ReDim Preserve CombSqual(UBound(CombSqual) + 1)
''''''        CombSqual(UBound(CombSqual)) = Rtb.text
''''''      ElseIf Not QualAndUnqual Then
''''''      End If
      jk = jk + 1
      '**********
   Wend
  Next i  'ciclo su molteplicit� istruzioni
  'sostituzioni nel file principale
  
  getIstruzione_ETZ = wIstr
End Function

Sub CostruisciCommonInitializeTemplate(rst As Recordset)
 Dim Rtb  As RichTextBox, wPath As String, appoFinalString As String, posEndLine As String
 Dim segLevel() As String, levelKey() As String, appoLevelString As String, k As Integer
 Dim K1 As Integer, i As Integer
  Dim templateName As String
  Dim appoXRST As String
  On Error GoTo catch
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMMONINIT"
  Rtb.LoadFile templateName
  
  MadrdM_GenRout.changeTag Rtb, "<NAMEPGM>", Trim(NomeProg)
  If isPliCallinsideSelect Then
    MadrdM_GenRout.changeTag Rtb, "<insideselect>", ""
  End If
  appoFinalString = Rtb.text
  Testo = vbCrLf
  insertLine Testo, "", rst!IdOggetto
  
  '************  embedded *********
  If EmbeddedRoutine Then
     MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
    If Not TabIstr.pcb = "" Then
       MadrdM_GenRout.changeTag Rtb, "<PCB>", TabIstr.pcb
    End If
    If Not TabIstr.numpcb = 0 Then
      MadrdM_GenRout.changeTag Rtb, "<NUMPCB>", TabIstr.numpcb
    End If
   
  'Salvataggio parziale
    appoFinalString = Rtb.text
   
    ReDim segLevel(UBound(TabIstr.BlkIstr))
  
    For k = 1 To UBound(TabIstr.BlkIstr)
      ' GSAM to Sequential: da adeguare per GSAM to RDBMS
      If TabIstr.isGsam Then
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLV_GSAM"
      Else
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\" & dirincaps & "SEGMENTLV"
      End If
      Rtb.LoadFile templateName
      
      If TabIstr.BlkIstr(k).segment = "" Then
          'AggLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!Riga & " missing segment", MadrdF_Incapsulatore.RTErr
      End If
       
       Dim h As Integer
       If UBound(TabIstr.psb) Then
         ' Mauro 16/12/2008
         If TabIstr.numpcb = 0 And UBound(TabIstr.DBD) Then
           h = Restituisci_Livello_Segmento_dbd(TabIstr.DBD(1).Id, TabIstr.BlkIstr(k).segment)
         Else
           h = Restituisci_Livello_Segmento(TabIstr.psb(1).Id, TabIstr.numpcb, TabIstr.BlkIstr(k).segment, 0)
         End If
       Else
          'stefano: istruzioni senza psb, assegna comunque un default
          'h = -1
          If UBound(TabIstr.DBD) Then
             h = Restituisci_Livello_Segmento_dbd(TabIstr.DBD(1).Id, TabIstr.BlkIstr(k).segment, 0)
          Else
             h = -1
          End If
       End If
       
       If h >= 0 Then
           MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
       Else
           m_fun.WriteLog "Incaps: Obj(" & rst!IdOggetto & ") Row( " & rst!Riga & " missing level", "MadrdM_Incapsulatore"
           MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
       End If
       MadrdM_GenRout.changeTag Rtb, "<LEVEL>", k
       
       appoLevelString = Rtb.text
       
       ReDim levelKey(UBound(TabIstr.BlkIstr(k).KeyDef))
  
       For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\LEVKEY"
         Rtb.LoadFile templateName
         
         If TabIstr.BlkIstr(k).KeyDef(K1).KeyStart <> 0 Then
               MadrdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(k).ssaName
               MadrdM_GenRout.changeTag Rtb, "<KEYSTART>", TabIstr.BlkIstr(k).KeyDef(K1).KeyStart
               MadrdM_GenRout.changeTag Rtb, "<KEYLEN>", TabIstr.BlkIstr(k).KeyDef(K1).KeyLen
               MadrdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(k, "#0")
               MadrdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(K1, "#0")
         End If
         levelKey(K1) = Rtb.text
       Next K1
       
       ' riprendo da SEGMENTLV
       Rtb.text = appoLevelString
       
       For i = 1 To K1 - 1
        MadrdM_GenRout.changeTag Rtb, "<<LEVKEY(i)>>", levelKey(i)
       Next
       
       'AC 24/10/07 spostato da segmlev in maincall e solo per ultimo livello
'       If Len(TabIstr.BlkIstr(K).Record) Then
'          MadrdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(K).Record
'          MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(K, "#0")
'          If TabIstr.isGsam Then
'             MadrdM_GenRout.changeTag Rtb, "<RECLEN>", TabIstr.BlkIstr(K).AreaLen
'          End If
'       End If
       
       If Len(TabIstr.BlkIstr(k).ssaName) Then
          MadrdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(k).ssaName
          MadrdM_GenRout.changeTag Rtb, "<SSALEN>", TabIstr.BlkIstr(k).SsaLen
          MadrdM_GenRout.changeTag Rtb, "<FLEVELSA>", Format(k, "#0")
       End If
       
       segLevel(k) = Rtb.text
       
    Next k
       
       'recuper maincall
       Rtb.text = appoFinalString
       'Rtb.SaveFile m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\logpar ", 1
       For k = 1 To UBound(TabIstr.BlkIstr)
        MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV(i)>>", segLevel(k)
       Next
       If UBound(TabIstr.BlkIstr) > 0 Then
           MadrdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record
           MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(UBound(TabIstr.BlkIstr), "#0")
           If TabIstr.isGsam Then
              MadrdM_GenRout.changeTag Rtb, "<RECLEN>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).areaLen
           End If
       End If
        MadrdM_GenRout.changeTag Rtb, "<LANGUAGE>", TipoFile 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", TabIstr.istr 'IRIS-DB
        If TabIstr.istr <> "PCB" And TabIstr.istr <> "SCHE" Then 'IRIS-DB
          MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", CInt(Right(TabDec(1).Codifica, 6)) 'IRIS-DB
        End If 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<SEGMENT>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).segment 'IRIS-DB
       'MOVE PCB "VERO" nel nostro: (per l'ultimo segmento letto, per es...)
        If Len(TabIstr.pcb) Then
          MadrdM_GenRout.changeTag Rtb, "<PCBCALL>", TabIstr.pcb
        End If
       appoFinalString = Rtb.text
  Else
       ' nel caso non embedded rendo disponibili dei tag al programmatore
       ' in questo modo inserendo i tag nel template p�o accedere ai
       ' campi della tabistr
       If Not rst!isExec Then
        MadrdM_GenRout.changeTag Rtb, "<XPCB>", TabIstr.pcb
        MadrdM_GenRout.changeTag Rtb, "<XNUMPCB>", TabIstr.numpcb
       End If
       appoFinalString = Rtb.text
  End If
  
  While InStr(appoFinalString, vbCrLf) > 0
    posEndLine = InStr(1, appoFinalString, vbCrLf)
    insertLine Testo, Left(appoFinalString, posEndLine - 1), rst!IdOggetto, indentkey
    appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
  Wend

  'insertLine testo, ""
  If Not Trim(appoFinalString) = "" Then
    insertLine Testo, appoFinalString, rst!IdOggetto, indentkey
  End If
  Exit Sub
catch:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & rst!IdOggetto & "] Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  ElseIf err.Number = 75 Then
    AggLog "[Object: " & rst!IdOggetto & "] Template: " & templateName & " - " & err.Description, MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & rst!IdOggetto & "] unexpected error: " & err.Description, MadrdF_Incapsulatore.RTErr
  End If
End Sub

Sub CostruisciCommonInitializeTemplateC(rst As Recordset)
 Dim Rtb  As RichTextBox, wPath As String, appoFinalString As String, posEndLine As String
 Dim segLevel() As String, levelKey() As String, appoLevelString As String, k As Integer
 Dim K1 As Integer, i As Integer
 
  Set Rtb = MadrdF_Incapsulatore.RTBR
  wPath = m_fun.FnPathPrj
  'stefanopul
  'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMMONINIT"
  Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\COMMONINIT"
  
  ' per le copy non muoviamo niente in LNKDB-CALLEr, rimane il nome del chiamante
  'MadrdM_GenRout.changeTag Rtb, "<NAMEPGM>", Trim(NomeProg)
  If isPliCallinsideSelect Then
    MadrdM_GenRout.changeTag Rtb, "<insideselect>", ""
  End If
  
  appoFinalString = Rtb.text
  Testo = Testo & vbCrLf
  insertLine Testo, "", rst!IdOggetto
  
  '************  embedded *********
  If EmbeddedRoutine Then
     MadrdM_GenRout.changeTag Rtb, "<embedded>", ""
    If Not TabIstr.pcb = "" Then
       MadrdM_GenRout.changeTag Rtb, "<PCB>", TabIstr.pcb
    End If
    If Not TabIstr.numpcb = 0 Then
      MadrdM_GenRout.changeTag Rtb, "<NUMPCB>", TabIstr.numpcb
    End If
   
  'Salvataggio parziale
    appoFinalString = Rtb.text
    'appofinalString = appofinalString & Rtb.text
   
    ReDim segLevel(UBound(TabIstr.BlkIstr))
  
    For k = 1 To UBound(TabIstr.BlkIstr)
      ' GSAM to Sequential: da adeguare per GSAM to RDBMS
      If TabIstr.isGsam Then
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\SEGMENTLV_GSAM"
      Else
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\SEGMENTLV"
      End If
      
      DoEvents
       If TabIstr.BlkIstr(k).segment = "" Then
          'AggLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!Riga & " missing segment", MadrdF_Incapsulatore.RTErr
       End If
       
       Dim h As Integer
       If UBound(TabIstr.psb) Then
          ' Mauro 16/12/2008
          If TabIstr.numpcb = 0 And UBound(TabIstr.DBD) Then
            h = Restituisci_Livello_Segmento_dbd(TabIstr.DBD(1).Id, TabIstr.BlkIstr(k).segment)
          Else
            h = Restituisci_Livello_Segmento(TabIstr.psb(1).Id, TabIstr.numpcb, TabIstr.BlkIstr(k).segment, 0)
          End If
       Else
          'stefano: istruzioni senza psb, assegna comunque un default
          'h = -1
          If UBound(TabIstr.DBD) Then
             h = Restituisci_Livello_Segmento_dbd(TabIstr.DBD(1).Id, TabIstr.BlkIstr(k).segment, 0)
          Else
             h = -1
          End If
       End If
       
       If h >= 0 Then
           MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
       Else
           m_fun.WriteLog "Incaps: Obj(" & rst!IdOggetto & ") Row( " & rst!Riga & " missing level", "MadrdM_Incapsulatore"
           MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
       End If
       MadrdM_GenRout.changeTag Rtb, "<LEVEL>", k
       
       appoLevelString = Rtb.text
       
       ReDim levelKey(UBound(TabIstr.BlkIstr(k).KeyDef))
  
       For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
         Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\LEVKEY"
         MadrdM_GenRout.changeTag Rtb, "<copy>", ""
         If TabIstr.BlkIstr(k).KeyDef(K1).KeyStart <> 0 Then
               MadrdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(k).ssaName
               MadrdM_GenRout.changeTag Rtb, "<KEYSTARTC>", TabIstr.BlkIstr(k).KeyDef(K1).KeyStart
               MadrdM_GenRout.changeTag Rtb, "<KEYLEN>", TabIstr.BlkIstr(k).KeyDef(K1).KeyLen
               MadrdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(k, "#0")
               MadrdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(K1, "#0")
         End If
         levelKey(K1) = Rtb.text
       Next K1
       
       ' riprendo da SEGMENTLV
       Rtb.text = appoLevelString
       
       For i = 1 To K1 - 1
        MadrdM_GenRout.changeTag Rtb, "<<LEVKEY(i)>>", levelKey(i)
       Next
       
       'AC 24/10/2007 tolto da seglev e messo in main call con controllo diverso
'       If Len(TabIstr.BlkIstr(K).Record) Then
'          MadrdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(K).Record
'          MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(K, "#0")
'          If TabIstr.isGsam Then
'             MadrdM_GenRout.changeTag Rtb, "<RECLEN>", TabIstr.BlkIstr(K).AreaLen
'          End If
'       End If
       
       If Len(TabIstr.BlkIstr(k).ssaName) Then
          MadrdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(k).ssaName
          MadrdM_GenRout.changeTag Rtb, "<FLEVELSA>", Format(k, "#0")
       End If
       
       segLevel(k) = Rtb.text
       
    Next k
       
       'recupero maincall
       Rtb.text = appoFinalString
       If UBound(TabIstr.BlkIstr) > 0 Then
        MadrdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record
        MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(UBound(TabIstr.BlkIstr), "#0")
        If TabIstr.isGsam Then
           MadrdM_GenRout.changeTag Rtb, "<RECLEN>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).areaLen
        End If
       End If
       
       For k = 1 To UBound(TabIstr.BlkIstr)
        MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV(i)>>", segLevel(k)
       Next
       MadrdM_GenRout.changeTag Rtb, "<LANGUAGE>", TipoFile 'IRIS-DB
       MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", TabIstr.istr 'IRIS-DB
       If TabIstr.istr <> "PCB" And TabIstr.istr <> "SCHE" Then 'IRIS-DB
         MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", CInt(Right(TabDec(1).Codifica, 6)) 'IRIS-DB
       End If 'IRIS-DB
       MadrdM_GenRout.changeTag Rtb, "<SEGMENT>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).segment 'IRIS-DB
       'SQ 6-10-06
       'MOVE PCB "VERO" nel nostro: (per l'ultimo segmento letto, per es...)
        If Len(TabIstr.pcb) Then
          MadrdM_GenRout.changeTag Rtb, "<PCBCALL>", TabIstr.pcb
        End If
       appoFinalString = Rtb.text
  Else
       ' nel caso non embedded rendo disponibili dei tag al programmatore
       ' in questo modo inserendo i tag nel template p�o accedere ai
       ' campi della tabistr
        MadrdM_GenRout.changeTag Rtb, "<XPCB>", TabIstr.pcb
        MadrdM_GenRout.changeTag Rtb, "<XNUMPCB>", TabIstr.numpcb
        appoFinalString = Rtb.text
  End If
  
  While InStr(1, appoFinalString, vbCrLf) > 0
    posEndLine = InStr(1, appoFinalString, vbCrLf)
    insertLine Testo, Left(appoFinalString, posEndLine - 1), rst!IdOggetto, indentkey
    appoFinalString = Right(appoFinalString, Len(appoFinalString) - posEndLine - 1)
  Wend

  'insertLine testo, ""
  If Not Trim(appoFinalString) = "" Then
    insertLine Testo, appoFinalString, rst!IdOggetto, indentkey
  End If
End Sub

Function Genera_Counter(nomeRoutine)
  Dim rsCount As Recordset
  Dim rsRout As Recordset
  Dim pcbName As String
  Dim k As Integer
  Dim y As Integer
  
  'SQ - 24-07-06
  'Errore impossibile da trovare! Non genera routine e incapsula male se non ritorno un Genera_Counter <> ""!
  'Genera_Counter = ""
  Genera_Counter = nomeRoutine
  
  Set rsCount = m_fun.Open_Recordset("select * from bs_parametri where typekey = 'COUNTER'")
  If Not rsCount.EOF Then
    'verifica quanto sono le routine attuali
    'attacca solo se pu�
    If Len(nomeRoutine) <= 6 Then
      While Genera_Counter = nomeRoutine 'Or rsRout.EOF
        'cerca routine con counter = k
        Set rsRout = m_fun.Open_Recordset("select count(*) as totistr from mgdli_decodificaistr where nomerout = '" & nomeRoutine & Format(k, "00") & "'")
        If rsRout!totIstr >= Int(rsCount!ParameterValue) Then
           k = k + 1
        Else
          Genera_Counter = nomeRoutine & Format(k, "00")
        End If
        rsRout.Close
      Wend
    End If
  End If
  rsCount.Close
End Function

'IRIS-DB whole function: overrides with PCB area in case of Dynamic PCB usage
Function Check_Dyn_PCB(nomeRoutine, idpgm As Long, IdOgg As Long, Riga As Long)
  Dim rsRout As Recordset
  Dim pcbName As String
  
  Check_Dyn_PCB = nomeRoutine
  Set rsRout = m_fun.Open_Recordset("select * from PsDli_Istruzioni where IdPgm = " & idpgm & " and IdOggetto = " & IdOgg & " and Riga = " & Riga)
  ' per ora non gestiamo il non trovato, che � praticamente impossibile
  If Not rsRout.EOF Then
    pcbName = rsRout!numpcb
    rsRout.Close
    Set rsRout = m_fun.Open_Recordset("select * from IRIS_DYN_PCB where IdPgm = " & idpgm & " and TargPCB = '" & pcbName & "'")
    If Not rsRout.EOF Then
      Check_Dyn_PCB = rsRout!origPCB & "(1:8)"
    End If
  End If
  rsRout.Close
End Function

Function CostruisciCall_CALLTemplate(pTesto As String, wIstruzione, typeIncaps As String, wIstrDli As String, rst As Recordset, RTBErr As RichTextBox, Optional iscopy As Boolean = False) As String
  Dim k As Integer, K1 As Integer, k2 As Integer, i As Integer
  Dim wstr As String, wStr1 As String, nPcb As String, testoCall As String
  Dim s() As String
  Dim wKeyName As String
  'Parametri incapsulatore
  Dim pmNomeDbd As String, pmCallRout As String, pmDecIstr As String
  Dim Rtb  As RichTextBox, wPath As String, appoFinalString As String, posEndLine As String
  Dim segLevel() As String, levelKey() As String, appoLevelString As String
  Dim templateName As String
  Dim appoXRST As String, appoXRSTret As String
  Dim xprogramm As String
  Dim dataType As String
  
  On Error GoTo catch
  
  testoCall = testoCall & pTesto
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\MAINCALL" & IIf(EmbeddedRoutine, "_EMBEDDED", "")
  Rtb.LoadFile templateName
  
  DoEvents
  'GSAM fa un giro particolare
  If TabIstr.isGsam Then
    ' purtroppo va impostata alla OPEN, mentre la psdli_istrdett_liv non c'� per la open
    ' bisogna fare la seguente forzatura
    ' E' una grossa forzatura, ma evita di cambiare troppe cose
    Dim tb1 As Recordset
    Set tb1 = m_fun.Open_Recordset("select dataarea from psdli_istrdett_liv as a, psdli_istruzioni as b where a.idoggetto = b.idoggetto and a.riga = b.riga and b.idoggetto = " & rst!IdOggetto & " and numpcb = '" & TabIstr.pcb & "'")
    If Not tb1.EOF Then
      If Len(tb1!DataArea) Then
        MadrdM_GenRout.changeTag Rtb, "<RECLENGSAM>", tb1!DataArea
      End If
    End If
    tb1.Close
  End If
  If isPliCallinsideSelect Then
    MadrdM_GenRout.changeTag Rtb, "<insideselect>", ""
  End If
  If Not EmbeddedRoutine Then
    '***** per embedded spostato su commoninit
    If Not TabIstr.pcb = "" Then
      MadrdM_GenRout.changeTag Rtb, "<PCB>", Replace(TabIstr.pcb, "$", "")
    End If
    If Not TabIstr.numpcb = 0 And Not TabIstr.istr = "TERM" Then
      MadrdM_GenRout.changeTag Rtb, "<NUMPCB>", TabIstr.numpcb
    End If
   
    '10/07/09 tag <WR> su MAINCALL
    If Not (TabIstr.istr = "GU" Or TabIstr.istr = "GN" Or TabIstr.istr = "GNP" Or _
      TabIstr.istr = "GHU" Or TabIstr.istr = "GHN" Or TabIstr.istr = "GHNP" Or xIstr = "POS") Then 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<WR>", ""
    End If
   
    'Salvataggio parziale
    appoFinalString = Rtb.text
    ReDim segLevel(UBound(TabIstr.BlkIstr))
    For k = 1 To UBound(TabIstr.BlkIstr)
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLV"
      Rtb.LoadFile templateName
      
      If TabIstr.BlkIstr(k).segment = "" Then
        'AggLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!Riga & " missing segment", MadrdF_Incapsulatore.RTErr
      End If
     
      Dim h As Integer
      If UBound(TabIstr.psb) Then
        ' Mauro 16/12/2008
        If TabIstr.numpcb = 0 And UBound(TabIstr.DBD) Then
          h = Restituisci_Livello_Segmento_dbd(TabIstr.DBD(1).Id, TabIstr.BlkIstr(k).segment)
        Else
          h = Restituisci_Livello_Segmento(TabIstr.psb(1).Id, TabIstr.numpcb, TabIstr.BlkIstr(k).segment, 0)
        End If
      Else
        'stefano: istruzioni senza psb, assegna comunque un default
        'h = -1
        If UBound(TabIstr.DBD) Then
          'MG
          'h = Restituisci_Livello_Segmento_dbd(TabIstr.dbd(1).Id, TabIstr.BlkIstr(K).Segment, 0)
          segLev = 0
          getSegLevel (CLng(TabIstr.BlkIstr(k).idsegm))
          h = segLev - 1
        Else
          h = -1
        End If
      End If
     
      If h >= 0 Then
        MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
      Else
        m_fun.WriteLog "Incaps: Obj(" & rst!IdOggetto & ") Row( " & rst!Riga & " missing level", "MadrdM_Incapsulatore"
        MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
      End If
      MadrdM_GenRout.changeTag Rtb, "<LEVEL>", k
     
      'tag <WR> su SEGMENTLV
      If Not (TabIstr.istr = "GU" Or TabIstr.istr = "GN" Or TabIstr.istr = "GNP" Or _
        TabIstr.istr = "GHU" Or TabIstr.istr = "GHN" Or TabIstr.istr = "GHNP" Or xIstr = "POS") Then 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<WR>", ""
      End If
     
      appoLevelString = Rtb.text
     
      ReDim levelKey(UBound(TabIstr.BlkIstr(k).KeyDef))

      For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\LEVKEY"
        Rtb.LoadFile templateName
       
        If TabIstr.BlkIstr(k).KeyDef(K1).KeyStart <> 0 Then
          If Not TabIstr.BlkIstr(k).ssaName = "" Then
            MadrdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(k).ssaName
          End If
          If Not iscopy Then
            MadrdM_GenRout.changeTag Rtb, "<KEYSTART>", TabIstr.BlkIstr(k).KeyDef(K1).KeyStart
          Else
            MadrdM_GenRout.changeTag Rtb, "<copy>", ""
            MadrdM_GenRout.changeTag Rtb, "<KEYSTARTC>", TabIstr.BlkIstr(k).KeyDef(K1).KeyStart
          End If
          MadrdM_GenRout.changeTag Rtb, "<KEYLEN>", TabIstr.BlkIstr(k).KeyDef(K1).KeyLen
          MadrdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(k, "#0")
          MadrdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(K1, "#0")
        End If
        levelKey(K1) = Rtb.text
      Next K1
     
      ' riprendo da SEGMENTLV
      Rtb.text = appoLevelString
     
      For i = 1 To K1 - 1
        MadrdM_GenRout.changeTag Rtb, "<<LEVKEY(i)>>", levelKey(i)
      Next i
      
      If Len(TabIstr.BlkIstr(k).ssaName) Then
        MadrdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(k).ssaName
        MadrdM_GenRout.changeTag Rtb, "<SSALEN>", TabIstr.BlkIstr(k).SsaLen
        MadrdM_GenRout.changeTag Rtb, "<FLEVELSA>", Format(k, "#0")
      End If
     
      segLevel(k) = Rtb.text
      If Left(TabIstr.BlkIstr(k).ssaName, 1) = "'" And Right(TabIstr.BlkIstr(k).ssaName, 1) = "'" Then
        segLevel(k) = Replace(segLevel(k), "IRIS_", "")
      End If
      If TabIstr.isGsam Then
        segLevel(k) = Replace(segLevel(k), "<IRIS>_", "")
      End If
      'AC 19/03/08
      'segLevel(k) = Replace(segLevel(k), "IRIS_", "")
      segLevel(k) = Replace(segLevel(k), "<IRIS>", "IRIS")
    Next k
     
    'recupera maincall
    If TabIstr.isGsam Then
      appoFinalString = Replace(appoFinalString, "<IRIS>_", "")
    End If
    Rtb.text = appoFinalString
    If UBound(TabIstr.BlkIstr) > 0 Then
      'AC tolta il 19/05/08
      'If Not (TabIstr.istr = "GU" Or TabIstr.istr = "GN" Or TabIstr.istr = "GNP") Then
      If Not TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).recordReaddress = "" Then
        MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).recordReaddress)
      Else
        MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(Replace(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record, "$", ""))
      End If
      MadrdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record
      MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(UBound(TabIstr.BlkIstr), "#0")
      'MadrdM_GenRout.changeTag Rtb, "<IRIS>", "IRIS"
      'AC 19/03/09
        
      Dim BolNoRev As Boolean
      getAreaType Trim(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record), rst!IdOggetto, 0, dataType, "", -1
      If dataType = "X" Then
        MadrdM_GenRout.changeTag Rtb, "<IRIS>", ""
        BolNoRev = True
      Else
        MadrdM_GenRout.changeTag Rtb, "<IRIS>", "IRIS_"
      End If
      'End If
    End If
    For k = 1 To UBound(TabIstr.BlkIstr)
      MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV(i)>>", segLevel(k)
    Next k
    MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", TabIstr.istr 'IRIS-DB
    If TabIstr.istr <> "PCB" And TabIstr.istr <> "SCHE" Then 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", numIstr 'IRIS-DB
    End If 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<LANGUAGE>", TipoFile 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", TabIstr.istr 'IRIS-DB
    If TabIstr.istr <> "PCB" And TabIstr.istr <> "SCHE" Then 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", CInt(Right(TabDec(1).Codifica, 6)) 'IRIS-DB
    End If 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<SEGMENT>", MadrdM_Incapsulatore.TabIstr.BlkIstr(UBound(MadrdM_Incapsulatore.TabIstr.BlkIstr)).segment 'IRIS-DB
    'SQ 6-10-06
    'MOVE PCB "VERO" nel nostro: (per l'ultimo segmento letto, per es...)
     
    If Len(TabIstr.pcb) And Not TabIstr.istr = "TERM" Then
      MadrdM_GenRout.changeTag Rtb, "<PCBCALL>", TabIstr.pcb
    End If
    '****** fine parte embedded spostata su COMMONINIT
  End If
     
  ReDim segLevel(UBound(TabIstr.BlkIstr))
  If SwTp Then
    '*****************************
    If m_fun.FnNomeDB = "Prj-Perot-POC.mty" Then  ' variante PEROT
      If TabIstr.istr = "PCB" Or TabIstr.istr = "CHKP" Or TabIstr.istr = "ROLB" Or TabIstr.istr = "TERM" Then
        'come versione batch
        MadrdM_GenRout.changeTag Rtb, "<SWTPAREA>", "LNKDB-AREA"
      End If
    End If
    '*******************************
    MadrdM_GenRout.changeTag Rtb, "<SWTPAREA>", "DFHEIBLK, LNKDB-AREA"
  Else
    MadrdM_GenRout.changeTag Rtb, "<SWTPAREA>", "LNKDB-AREA"
  End If
     
  'Salvataggio parziale
  appoFinalString = Rtb.text
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLVREV"
  Rtb.LoadFile templateName
     
  If UBound(TabIstr.BlkIstr) > 0 Then
    If (TabIstr.istr = "GU" Or TabIstr.istr = "GN" Or TabIstr.istr = "GNP" Or TabIstr.istr = "GHN" Or TabIstr.istr = "GHNP" Or TabIstr.istr = "GHU" Or xIstr = "POS") Then 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(UBound(TabIstr.BlkIstr), "#0")
      'tag <RD> su SEGMENTLVREV
      MadrdM_GenRout.changeTag Rtb, "<RD>", ""
      If Not TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).recordReaddress = "" Then
        MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).recordReaddress)
      Else
        MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record)
      End If
      If Not TabIstr.istr = "ISRT" Then
        MadrdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record
      End If
      If TabIstr.isGsam Then
        MadrdM_GenRout.changeTag Rtb, "<RECLEN>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).areaLen
      End If
    End If
  End If
  segLevel(UBound(TabIstr.BlkIstr)) = Rtb.text
  '***********************************************************************
  If TabIstr.istr = "XRST" Or TabIstr.istr = "CHKP" Then
    Dim arrSavedArea() As String, arrSavedAreaRet() As String
    ReDim arrSavedArea(UBound(TabIstr.SavedAreas))
    ReDim arrSavedAreaRet(UBound(TabIstr.SavedAreas))
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SAVEAREA"
    For k = 1 To UBound(TabIstr.SavedAreas)
      Rtb.LoadFile templateName
      If UBound(TabIstr.SavedAreasLen) >= k Then
        MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", TabIstr.SavedAreasLen(k)
      Else
        'MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", "" ' TO DO
      End If
      MadrdM_GenRout.changeTag Rtb, "<SAVEAREA(i)>", TabIstr.SavedAreas(k)
      MadrdM_GenRout.changeTag Rtb, "<i>", k
      arrSavedArea(k) = Rtb.text
    Next k
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IDAREA"
    Rtb.LoadFile templateName
    If Len(TabIstr.IdAreaLen) Then
      MadrdM_GenRout.changeTag Rtb, "<LENAREA>", TabIstr.IdAreaLen
    End If
    MadrdM_GenRout.changeTag Rtb, "<IDAREA>", TabIstr.IdArea
    For k = 1 To UBound(arrSavedArea)
      MadrdM_GenRout.changeTag Rtb, "<<AREE(i)>>", arrSavedArea(k)
    Next k
    appoXRST = Rtb.text
    '------------------------------------------
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SAVEAREARET"
    For k = 1 To UBound(TabIstr.SavedAreas)
      Rtb.LoadFile templateName
      If UBound(TabIstr.SavedAreasLen) >= k Then
        MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", TabIstr.SavedAreasLen(k)
      Else
        'MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", "" ' TO DO
      End If
      MadrdM_GenRout.changeTag Rtb, "<SAVEAREA(i)>", TabIstr.SavedAreas(k)
      MadrdM_GenRout.changeTag Rtb, "<i>", k
      arrSavedAreaRet(k) = Rtb.text
    Next k
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IDAREARET"
    Rtb.LoadFile templateName
    If Len(TabIstr.IdAreaLen) Then
      MadrdM_GenRout.changeTag Rtb, "<LENAREA>", TabIstr.IdAreaLen
    End If
    MadrdM_GenRout.changeTag Rtb, "<IDAREA>", TabIstr.IdArea
    For k = 1 To UBound(arrSavedAreaRet)
      MadrdM_GenRout.changeTag Rtb, "<<AREERET(i)>>", arrSavedAreaRet(k)
    Next k
    appoXRSTret = Rtb.text
  End If
    
  Rtb.text = appoFinalString
  
  If Len(appoXRST) Then
    MadrdM_GenRout.changeTag Rtb, "<<IDAREA>>", appoXRST
  End If
  If Len(appoXRSTret) Then
    MadrdM_GenRout.changeTag Rtb, "<<IDAREARET>>", appoXRSTret
  End If
  'AC 24/10/07
'  For K = 1 To UBound(TabIstr.BlkIstr)
'    MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLVREV(i)>>", segLevel(K)
'  Next k
  'AC 19/03/09
  If BolNoRev Then
    segLevel(UBound(TabIstr.BlkIstr)) = Replace(segLevel(UBound(TabIstr.BlkIstr)), "IRIS_", "")
  End If
  MadrdM_GenRout.changeTag Rtb, "<SEGMENTLVREV>", segLevel(UBound(TabIstr.BlkIstr))
   
  'testoCall = testoCall & CreaSp(wIdent) & vbCrLf
  
  If TabIstr.keyfeedback <> "" Then
    getAreaType Trim(TabIstr.keyfeedback), rst!IdOggetto, 0, dataType, "", -1
    If dataType = "X" Then
      MadrdM_GenRout.changeTag Rtb, "<REVFBK>", ""
    Else
      MadrdM_GenRout.changeTag Rtb, "<REVFBK>", "IRIS_"
    End If
    MadrdM_GenRout.changeTag Rtb, "<KEYFDBCK>", Trim(TabIstr.keyfeedback)
  End If
  
  If TabIstr.isGsam Then
    MadrdM_GenRout.changeTag Rtb, "DIBSTAT<Conditioned1>", "$STATUS_CODE"
  End If
     
  If Not TabIstr.istr = "TERM" Then  ' and Len(TabIstr.PCB)
    ' scrivo move anche se non ho pcb
    MadrdM_GenRout.changeTag Rtb, "<DLZPCB>", TabIstr.pcb
  End If
   
  ' tag a disposizione in particolare per PLI)
  For i = 1 To UBound(routinePLI)
    If nomeRoutine = routinePLI(i) Then Exit For
  Next i
  If i = UBound(routinePLI) + 1 Then
    ReDim Preserve routinePLI(UBound(routinePLI) + 1)
    routinePLI(UBound(routinePLI)) = nomeRoutine
    'MadrdM_GenRout.changeTag Rtb, "<XEPROGRAMMA>", nomeroutine
  End If
  If Len(nomeRoutine) < 8 Then
    xprogramm = Space(8 - Len(nomeRoutine))
  Else
    xprogramm = ""
    If InStr(m_fun.FnNomeDB, "prj-GRZ") Then
      nomeRoutine = IIf(Len(nomeRoutine) > 7, Left(nomeRoutine, 7), nomeRoutine)
    End If
  End If
  MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & xprogramm
  MadrdM_GenRout.changeTag Rtb, "<VARPCB>", TabIstr.pcb
  If Len(TabIstr.Pcbred) Then
    MadrdM_GenRout.changeTag Rtb, "<XREDEFPCB>", TabIstr.Pcbred
  End If
  If opencomment Then
    MadrdM_GenRout.changeTag Rtb, "<opencomment>", opencommentstr
  End If
     
  'SILVIA 9-6-2008
  If TabIstr.istr = "GU" Or TabIstr.istr = "GN" Or TabIstr.istr = "GNP" Or _
     TabIstr.istr = "GHU" Or TabIstr.istr = "GHN" Or TabIstr.istr = "GHNP" Or xIstr = "POS" Then 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<Conditioned2>", ""
''    If TipoFile = "PLI" Or TipoFile = "INC" Then
''      MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", "DIBSEGM = LNKDB_PCBSEGNM;"
''    Else
''      MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", "MOVE LNKDB_PCBSEGNM TO DIBSEGM"
''    End If
''  Else
''    MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", ""
  End If
     
  appoFinalString = Rtb.text
  ' sostituzione
  s = Split(appoFinalString, vbCrLf)
  For j = 0 To UBound(s)
    insertLine testoCall, s(j), rst!IdOggetto, indentkey
  Next j
  
  CostruisciCall_CALLTemplate = testoCall
  s = Split(CostruisciCall_CALLTemplate, vbCrLf)
  
  AddRighe = AddRighe + UBound(s)
  Exit Function
catch:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & rst!IdOggetto & "] Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & rst!IdOggetto & "] unexpected error on 'CostruisciOperazioneTemplate'.", MadrdF_Incapsulatore.RTErr
  End If
End Function

Function GetKeysFromId(pIdOggetto As Long, pIdPgm As Long, pRiga As Long) As String
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("Select Keys From PsDLI_Istruzioni Where " & _
                                "IdOggetto = " & pIdOggetto & " And IdPgm = " & pIdPgm & " And Riga = " & pRiga)
  If Not rs.EOF Then
    GetKeysFromId = IIf(IsNull(rs!Keys), "", rs!Keys)
  End If
  rs.Close
End Function

Function CostruisciCall_CALLTemplate_EXEC(pTesto As String, wIstruzione, typeIncaps As String, wIstrDli As String, rst As Recordset, RTBErr As RichTextBox, Optional iscopy As Boolean = False) As String
  Dim k As Integer, K1 As Integer, k2 As Integer, i As Integer
  Dim wstr As String, wStr1 As String, nPcb As String, testoCall As String
  Dim s() As String
  Dim wKeyName As String
  'Parametri incapsulatore
  Dim pmNomeDbd As String, pmCallRout As String, pmDecIstr As String
  Dim Rtb  As RichTextBox, wPath As String, appoFinalString As String, posEndLine As String
  Dim segLevel() As String, levelKey() As String, appoLevelString As String
  Dim templateName As String
  Dim appoXRST As String, appoXRSTret As String
  Dim rsOccurs As Recordset, Parentesi As Integer
  Dim BolNoRev As Boolean
  Dim dataType As String
  On Error GoTo catch
  
  testoCall = testoCall & pTesto
  
  Set Rtb = MadrdF_Incapsulatore.RTBR
  
  templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\MAINCALL_EXEC" & IIf(EmbeddedRoutine, "_EMBEDDED", "")
  
  Rtb.LoadFile templateName
  
  'GSAM fa un giro particolare
  If TabIstr.isGsam Then
    ' purtroppo va impostata alla OPEN, mentre la psdli_istrdett_liv non c'� per la open
    ' bisogna fare la seguente forzatura
    ' E' una grossa forzatura, ma evita di cambiare troppe cose
    Dim tb1 As Recordset
    Set tb1 = m_fun.Open_Recordset("select dataarea from psdli_istrdett_liv as a, psdli_istruzioni as b where a.idoggetto = b.idoggetto and a.riga = b.riga and b.idoggetto = " & rst!IdOggetto & " and numpcb = '" & TabIstr.pcb & "'")
    If Not tb1.EOF Then
      If Len(tb1!DataArea) Then
        MadrdM_GenRout.changeTag Rtb, "<RECLENGSAM>", tb1!DataArea
      End If
    End If
    tb1.Close
  End If
  If isPliCallinsideSelect Then
    MadrdM_GenRout.changeTag Rtb, "<insideselect>", ""
  End If
  'If Not EmbeddedRoutine Then
  '***** per embedded spostato su commoninit
  If Not TabIstr.pcb = "" Then
    MadrdM_GenRout.changeTag Rtb, "<PCB>", TabIstr.pcb
  End If
  If Not TabIstr.numpcb = 0 And Not TabIstr.istr = "TERM" Then
    MadrdM_GenRout.changeTag Rtb, "<NUMPCB>", TabIstr.numpcb
  End If
  
  'AC 30/08/10 valorizzazioni di DLZDIB eliminate per la TERM
  If Not TabIstr.istr = "TERM" Then
    MadrdM_GenRout.changeTag Rtb, "<NOTERM>", ""
  End If
   
  'MadrdM_GenRout.changeTag Rtb, "<SEGMENT> ", TabIstr.BlkIstr(1).Segment
  'MadrdM_GenRout.changeTag Rtb, "<SEGLEN>", TabIstr.BlkIstr(1).SegLen
  If Not (TabIstr.istr = "GU" Or TabIstr.istr = "GN" Or TabIstr.istr = "GNP" Or TabIstr.istr = "GHNP" Or TabIstr.istr = "GHU" Or xIstr = "POS") Then 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<AREAINTO>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record
  End If
  MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", UBound(TabIstr.BlkIstr)

  If InStr(m_fun.FnNomeDB, "prj-GRZ") Then
    nomeRoutine = IIf(Len(nomeRoutine) > 7, Left(nomeRoutine, 7), nomeRoutine)
  End If
  
  If Len(nomeRoutine) < 8 And InStr(m_fun.FnNomeDB, "prj-GRZ") = 0 Then
    MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine & Space(8 - Len(nomeRoutine))
  Else
    MadrdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeRoutine
  End If
  If Len(nomeRoutine) < 8 Then
    MadrdM_GenRout.changeTag Rtb, "<PROGRAMMAF8>", nomeRoutine & Space(8 - Len(nomeRoutine))
  Else
    MadrdM_GenRout.changeTag Rtb, "<XROGRAMMAF8>", nomeRoutine
  End If
  If Len(TabIstr.Pcbred) Then
    MadrdM_GenRout.changeTag Rtb, "<XREDEFPCB>", TabIstr.Pcbred
  End If
  
'  If UBound(TabIstr.BlkIstr(1).KeyDef) Then
'    MadrdM_GenRout.changeTag Rtb, "<KEYNAME>", TabIstr.BlkIstr(1).KeyDef(1).key
'    MadrdM_GenRout.changeTag Rtb, "<KEYLEN> ", TabIstr.BlkIstr(1).KeyDef(1).KeyLen
'    MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", TabIstr.BlkIstr(1).KeyDef(1).KeyVal
'    MadrdM_GenRout.changeTag Rtb, "<KEYOPER>", TabIstr.BlkIstr(1).KeyDef(1).op
'  End If
  '+++++++++++++++++++++++++++++++++++++++++++++++
  appoFinalString = Rtb.text
  ReDim segLevel(UBound(TabIstr.BlkIstr))
  
  For k = 1 To UBound(TabIstr.BlkIstr)
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLV_EXEC"
    Rtb.LoadFile templateName
    
    If TabIstr.BlkIstr(k).segment = "" Then
      'AggLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!Riga & " missing segment", MadrdF_Incapsulatore.RTErr
    End If
     
    Dim h As Integer
    If UBound(TabIstr.psb) Then
      ' Mauro 16/12/2008
      If TabIstr.numpcb = 0 And UBound(TabIstr.DBD) Then
        h = Restituisci_Livello_Segmento_dbd(TabIstr.DBD(1).Id, TabIstr.BlkIstr(k).segment)
      Else
        h = Restituisci_Livello_Segmento(TabIstr.psb(1).Id, TabIstr.numpcb, TabIstr.BlkIstr(k).segment, 0)
      End If
    Else
      'stefano: istruzioni senza psb, assegna comunque un default
      'h = -1
      If UBound(TabIstr.DBD) Then
        'MG
        'h = Restituisci_Livello_Segmento_dbd(TabIstr.dbd(1).Id, TabIstr.BlkIstr(K).Segment, 0)
        segLev = 0
        getSegLevel (CLng(TabIstr.BlkIstr(k).idsegm))
        h = segLev - 1
      Else
        h = -1
      End If
    End If
   
    If h >= 0 Then
      MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
    Else
      m_fun.WriteLog "Incaps: Obj(" & rst!IdOggetto & ") Row( " & rst!Riga & " missing level", "MadrdM_Incapsulatore"
      MadrdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
    End If
    MadrdM_GenRout.changeTag Rtb, "<LEVEL>", k
    MadrdM_GenRout.changeTag Rtb, "<CODFUNC>", TabIstr.istr 'IRIS-DB
    If TabIstr.istr <> "PCB" And TabIstr.istr <> "SCHE" Then 'IRIS-DB
      MadrdM_GenRout.changeTag Rtb, "<NUMOPER>", CInt(Right(TabDec(1).Codifica, 6)) 'IRIS-DB
    End If 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<SEGMENT>", TabIstr.BlkIstr(k).segment
    MadrdM_GenRout.changeTag Rtb, "<SEGLEN>", TabIstr.BlkIstr(k).SegLen
    
    
    'tag <WR> su SEGMENTLV_EXEC
     If Not (TabIstr.istr = "GU" Or TabIstr.istr = "GN" Or TabIstr.istr = "GNP" Or _
        TabIstr.istr = "GHU" Or TabIstr.istr = "GHN" Or TabIstr.istr = "GHNP" Or xIstr = "POS") Then 'IRIS-DB
        MadrdM_GenRout.changeTag Rtb, "<WR>", ""
     End If
   
    appoLevelString = Rtb.text
    'AC 21/05/08
    appoLevelString = Replace(appoLevelString, "IRIS_$", "IRIS_")
   
    ReDim levelKey(UBound(TabIstr.BlkIstr(k).KeyDef))
    If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
      For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
        templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\LEVKEY_EXEC"
        Rtb.LoadFile templateName
        MadrdM_GenRout.changeTag Rtb, "<KEYLEN>", TabIstr.BlkIstr(k).KeyDef(K1).KeyLen
        MadrdM_GenRout.changeTag Rtb, "<KEYNAME>", TabIstr.BlkIstr(k).KeyDef(K1).key
        If InStr(Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), "(") Then
          ' Mauro 05/08/2008
          Parentesi = InStr(Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal) & "(", "(")
          Set rsOccurs = m_fun.Open_Recordset("select * from MgData_Cmp_DCL where " & _
                        "idoggetto = " & rst!IdOggetto & " and nomecmp = '" & Left(Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), Parentesi - 1) & "'")
          If rsOccurs.RecordCount = 0 Then
            MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal)
            If Left(Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), 1) = "'" And Right(Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), 1) = "'" Then
              MadrdM_GenRout.changeTag Rtb, "<REVK>", ""
            Else
              MadrdM_GenRout.changeTag Rtb, "<REVK>", "IRIS_"
            End If
          Else
            If rsOccurs!pointer Then
              Rtb.text = "IRIS_PTR = ADDR(" & TabIstr.BlkIstr(k).KeyDef(K1).KeyVal & ");" & vbCrLf & Rtb.text
              MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", Left(Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), Parentesi - 1)
              MadrdM_GenRout.changeTag Rtb, "<REVK>", "IRIS_"
            Else
              MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal)
              MadrdM_GenRout.changeTag Rtb, "<REVK>", ""
            End If
          End If
          rsOccurs.Close
        Else
          MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal)
          If Left(Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), 1) = "'" And Right(Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), 1) = "'" Then
            MadrdM_GenRout.changeTag Rtb, "<REVK>", ""
          Else
            ' Mauro 15/01/2009: TMP - Calcolare la lista dei IRIS_ una sola volta all'inizio
            getAreaType Trim(TabIstr.BlkIstr(k).KeyDef(K1).KeyVal), rst!IdOggetto, 0, dataType, "", -1
            If dataType = "X" Then
              MadrdM_GenRout.changeTag Rtb, "<REVK>", ""
            Else
              MadrdM_GenRout.changeTag Rtb, "<REVK>", "IRIS_"
            End If
          End If
        End If
        MadrdM_GenRout.changeTag Rtb, "<KEYOPER>", TabIstr.BlkIstr(k).KeyDef(K1).op
        MadrdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(k, "#0")
        MadrdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(K1, "#0")
        'End If
        levelKey(K1) = Rtb.text
      Next K1
     
      ' riprendo da SEGMENTLV
      Rtb.text = appoLevelString
   
      For i = 1 To K1 - 1
        'AC 21/05/08
        levelKey(i) = Replace(levelKey(i), "IRIS_$", "IRIS_")
        MadrdM_GenRout.changeTag Rtb, "<<LEVKEY_EXEC(i)>>", levelKey(i)
      Next i
    ElseIf InStr(TabIstr.BlkIstr(k).Search, "C") > 0 Then
      ReDim levelKey(1)
      templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\LEVKEY_EXEC"
      Rtb.LoadFile templateName
      MadrdM_GenRout.changeTag Rtb, "<KEYVALUE>", GetKeysFromId(rst!IdOggetto, rst!idpgm, rst!Riga)
      MadrdM_GenRout.changeTag Rtb, "<IDXLEVEL>", "1"
      MadrdM_GenRout.changeTag Rtb, "<IDXKEY>", "1"
      levelKey(1) = Rtb.text
      
      ' riprendo da SEGMENTLV
      Rtb.text = appoLevelString
      'AC 21/05/08
      levelKey(1) = Replace(levelKey(1), "IRIS_$", "IRIS_")
      ' Mauro 16/11/2010: TMP - Calcolare la lista dei IRIS_ una sola volta all'inizio
      getAreaType Trim(GetKeysFromId(rst!IdOggetto, rst!idpgm, rst!Riga)), rst!IdOggetto, 0, dataType, "", -1
      If dataType = "X" Then
        levelKey(1) = Replace(levelKey(1), "<REVK>", "")
      Else
        levelKey(1) = Replace(levelKey(1), "<REVK>", "IRIS_")
      End If
      'AC 06/10/2008
      'levelKey(1) = Replace(levelKey(1), "<REVK>", "")
      MadrdM_GenRout.changeTag Rtb, "<<LEVKEY_EXEC(i)>>", levelKey(1)
    End If
               
    ' <XRECORD> A1 (insieme ad A2 alternativo a B)
    'AC 07/01/08  versione move area dentro SEGMENTLV per tutti i livelli
    If Len(TabIstr.BlkIstr(k).Record) Then
      'AC tolta il 19/05/08
      'If Not (TabIstr.istr = "GU" Or TabIstr.istr = "GN" Or TabIstr.istr = "GNP") Then
      ' Mauro 07/08/2008 : Nuova gestione per Occurs
      Parentesi = InStr(TabIstr.BlkIstr(k).Record, "(")
      If Parentesi Then
        Set rsOccurs = m_fun.Open_Recordset("select * from mgdata_cmp_DCL where " & _
                        "idoggetto = " & rst!IdOggetto & " and nomecmp = '" & Trim(Left(TabIstr.BlkIstr(k).Record, Parentesi - 1)) & "'")
        If rsOccurs.RecordCount = 0 Then
          If Not TabIstr.BlkIstr(k).recordReaddress = "" Then
            MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(TabIstr.BlkIstr(k).recordReaddress)
          Else
            MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(TabIstr.BlkIstr(k).Record)
          End If
          'MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
          'AC 20/03/09
          getAreaType Trim(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record), rst!IdOggetto, 0, dataType, "", -1
          BolNoRev = False
          If dataType = "X" Then
            MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
            BolNoRev = True
          Else
            MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
          End If
        Else
          If rsOccurs!pointer Then
            Rtb.text = "IRIS_PTR = ADDR(" & TabIstr.BlkIstr(k).Record & ");" & vbCrLf & Rtb.text
            MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(Left(TabIstr.BlkIstr(k).Record, Parentesi - 1))
            MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
          Else
            MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(TabIstr.BlkIstr(k).Record)
            MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
          End If
        End If
      Else
        If Not TabIstr.BlkIstr(k).recordReaddress = "" Then
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(TabIstr.BlkIstr(k).recordReaddress)
        Else
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(TabIstr.BlkIstr(k).Record)
        End If
        'MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
        'AC 20/03/09
        getAreaType Trim(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record), rst!IdOggetto, 0, dataType, "", -1
        BolNoRev = False
        If dataType = "X" Then
          MadrdM_GenRout.changeTag Rtb, "<REVL>", ""
          BolNoRev = True
        Else
           MadrdM_GenRout.changeTag Rtb, "<REVL>", "IRIS_"
        End If
      End If
      MadrdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(k).Record
      MadrdM_GenRout.changeTag Rtb, "<AREAINTO>", TabIstr.BlkIstr(k).Record
      'End If
      MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(k, "#0")
    End If
    segLevel(k) = Rtb.text
    segLevel(k) = Replace(segLevel(k), "<IRIS>", "IRIS")
    segLevel(k) = Replace(segLevel(k), "IRIS_$", "IRIS_")
  Next k
  
  If TabIstr.istr = "XRST" Or TabIstr.istr = "CHKP" Then
    Dim arrSavedArea() As String, arrSavedAreaRet() As String
    ReDim arrSavedArea(UBound(TabIstr.SavedAreas))
    ReDim arrSavedAreaRet(UBound(TabIstr.SavedAreas))
    
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SAVEAREA"
    For k = 1 To UBound(TabIstr.SavedAreas)
      Rtb.LoadFile templateName
      If UBound(TabIstr.SavedAreasLen) >= k Then
        MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", TabIstr.SavedAreasLen(k)
      Else
        'MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", "" ' TO DO
      End If
      MadrdM_GenRout.changeTag Rtb, "<SAVEAREA(i)>", TabIstr.SavedAreas(k)
      MadrdM_GenRout.changeTag Rtb, "<i>", k
      arrSavedArea(k) = Rtb.text
    Next k
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IDAREA"
    Rtb.LoadFile templateName
    If Len(TabIstr.IdAreaLen) Then
      MadrdM_GenRout.changeTag Rtb, "<LENAREA>", TabIstr.IdAreaLen
    End If
    MadrdM_GenRout.changeTag Rtb, "<IDAREA>", TabIstr.IdArea
    For k = 1 To UBound(arrSavedArea)
      MadrdM_GenRout.changeTag Rtb, "<<AREE(i)>>", arrSavedArea(k)
    Next k
    appoXRST = Rtb.text
    '--------------------------------
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SAVEAREARET"
    For k = 1 To UBound(TabIstr.SavedAreas)
      Rtb.LoadFile templateName
      If UBound(TabIstr.SavedAreasLen) >= k Then
        MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", TabIstr.SavedAreasLen(k)
      Else
        'MadrdM_GenRout.changeTag Rtb, "<LENSAVEAREA(i)>", "" ' TO DO
      End If
      MadrdM_GenRout.changeTag Rtb, "<SAVEAREA(i)>", TabIstr.SavedAreas(k)
      MadrdM_GenRout.changeTag Rtb, "<i>", k
      arrSavedAreaRet(k) = Rtb.text
    Next k
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\IDAREARET"
    Rtb.LoadFile templateName
    If Len(TabIstr.IdAreaLen) Then
      MadrdM_GenRout.changeTag Rtb, "<LENAREA>", TabIstr.IdAreaLen
    End If
    MadrdM_GenRout.changeTag Rtb, "<IDAREA>", TabIstr.IdArea
    For k = 1 To UBound(arrSavedAreaRet)
      MadrdM_GenRout.changeTag Rtb, "<<AREERET(i)>>", arrSavedAreaRet(k)
    Next k
    appoXRSTret = Rtb.text
  End If
     
  'recuper maincall
  Rtb.text = appoFinalString
  If Len(appoXRST) Then
    MadrdM_GenRout.changeTag Rtb, "<<IDAREA>>", appoXRST
  End If
  If Len(appoXRSTret) Then
    MadrdM_GenRout.changeTag Rtb, "<<IDAREARET>>", appoXRSTret
  End If
  
  If SwTp Then
    MadrdM_GenRout.changeTag Rtb, "<SWTPAREA>", "DFHEIBLK, "
  Else
    MadrdM_GenRout.changeTag Rtb, "<SWTPAREA>", ""
  End If
  
  For k = 1 To UBound(TabIstr.BlkIstr)
    MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLV_EXEC(i)>>", segLevel(k)
  Next k
  
  MadrdM_GenRout.changeTag Rtb, "<Conditioned1>", ""
  
  ' <XRECORD> A2 (insieme ad A1 alternativo a B)
  'AC 07/01/08 versione con <<SEGMENTLVIRIS_EXEC(i)>> e area passata per tutti i livelli
  appoFinalString = Rtb.text
     
  For k = 1 To UBound(TabIstr.BlkIstr)
    templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS" & dirincaps & "\SEGMENTLVREV"
    Rtb.LoadFile templateName

    '<NONE>
    If Len(TabIstr.BlkIstr(k).Record) Then
      If (TabIstr.istr = "GU" Or TabIstr.istr = "GN" Or TabIstr.istr = "GNP" Or TabIstr.istr = "GHNP" Or TabIstr.istr = "GHU" Or TabIstr.istr = "GHN" Or xIstr = "POS") Then 'IRIS-DB
        'tag <RD> su SEGMENTLVREV
        MadrdM_GenRout.changeTag Rtb, "<RD>", ""
        MadrdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(k, "#0")
        If Not TabIstr.BlkIstr(k).recordReaddress = "" Then
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(TabIstr.BlkIstr(k).recordReaddress)
        Else
          MadrdM_GenRout.changeTag Rtb, "<XRECORD>", Trim(TabIstr.BlkIstr(k).Record)
        End If
        'AC 20/03/09
        getAreaType Trim(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Record), rst!IdOggetto, 0, dataType, "", -1
        BolNoRev = False
        If dataType = "X" Then
          BolNoRev = True
        Else
          BolNoRev = False
        End If
        MadrdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(k).Record
        If TabIstr.isGsam Then
          MadrdM_GenRout.changeTag Rtb, "<RECLEN>", TabIstr.BlkIstr(k).areaLen
        End If
      End If
    End If
    segLevel(k) = Rtb.text
    If BolNoRev Then
        segLevel(k) = Replace(segLevel(k), "IRIS_", "")
    End If
  Next k

  Rtb.text = appoFinalString

  For k = 1 To UBound(TabIstr.BlkIstr)
    segLevel(k) = Replace(segLevel(k), "IRIS_$", "IRIS_")
   
    MadrdM_GenRout.changeTag Rtb, "<<SEGMENTLVREV(i)>>", segLevel(k)
  Next k
  
  'AC 25/03/09
  If TabIstr.keyfeedback <> "" Then
    getAreaType Trim(TabIstr.keyfeedback), rst!IdOggetto, 0, dataType, "", -1
    If dataType = "X" Then
      MadrdM_GenRout.changeTag Rtb, "<REVFBK>", ""
    Else
      MadrdM_GenRout.changeTag Rtb, "<REVFBK>", "IRIS_"
    End If
    MadrdM_GenRout.changeTag Rtb, "<KEYFDBCK>", Trim(TabIstr.keyfeedback)
  End If
   
  'SILVIA 9-6-2008
  If TabIstr.istr = "GU" Or TabIstr.istr = "GN" Or TabIstr.istr = "GNP" Or _
    TabIstr.istr = "GHU" Or TabIstr.istr = "GHN" Or TabIstr.istr = "GHNP" Or xIstr = "POS" Then 'IRIS-DB
    MadrdM_GenRout.changeTag Rtb, "<Conditioned2>", ""
''    If TipoFile = "PLI" Or TipoFile = "INC" Then
''      MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", "DIBSEGM = LNKDB_PCBSEGNM;"
''    Else
''      MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", "MOVE LNKDB_PCBSEGNM TO DIBSEGM"
''    End If
''  Else
''    MadrdM_GenRout.changeTag Rtb, "<PCDSEGMN>", ""
  End If
  ''
  appoFinalString = Rtb.text
  ' sostituzione
  
  ' tag a disposizione in particolare per PLI)
  For i = 1 To UBound(routinePLI)
    If nomeRoutine = routinePLI(i) Then
      Exit For
    End If
  Next i
  If i = UBound(routinePLI) + 1 Then
    ReDim Preserve routinePLI(UBound(routinePLI) + 1)
    routinePLI(UBound(routinePLI)) = nomeRoutine
    'MadrdM_GenRout.changeTag Rtb, "<XEPROGRAMMA>", nomeroutine
  End If

  s = Split(appoFinalString, vbCrLf)
  For j = 0 To UBound(s)
    insertLine testoCall, s(j), rst!IdOggetto, indentkey
  Next j

  CostruisciCall_CALLTemplate_EXEC = testoCall
  s = Split(CostruisciCall_CALLTemplate_EXEC, vbCrLf)
   
  AddRighe = AddRighe + UBound(s)
   
Exit Function
catch:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & rst!IdOggetto & "] Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & rst!IdOggetto & "] unexpected error - " & err.Description, MadrdF_Incapsulatore.RTErr
  End If
End Function

Public Function FindCompleteTag(target As String, tag As String) As String
  Dim pos As Integer, char As String
  
  'SQ va in loop se non c'� il tag!!!!!!!!!!!!!
  'verificare la fine del file
  FindCompleteTag = tag
  pos = InStr(target, tag) + Len(tag)
  Do While posizione_DB > 0
    FindCompleteTag = FindCompleteTag & Mid(target, pos, 1)
    If Mid(target, pos, 1) = ">" Then
      Exit Do
    End If
    pos = pos + 1
  Loop
End Function

Public Function Transcode_Istruction_DLI(wIstr As String) As String
  Dim xwIstr As String

  Select Case Left(wIstr, 4)
    Case "GU..", "GHU."
      xwIstr = "GU"
    Case "GN..", "GHN."
      xwIstr = "GN"
    Case "GNP.", "GHNP"
      xwIstr = "GP"
    Case "POS."
      xwIstr = "PS"
    Case "ISRT", "DLET", "REPL"
      xwIstr = "UP"
    Case "PCB.", "SCH.", "TERM", "CHKP", "SYNC", "QRY.", "QUERY", "SYMCHKP"
      xwIstr = Left(wIstr, 4)
    Case Else
      xwIstr = "UK"
      'm_fun.WriteLog "MadrdM_Incapsulatore.Transcode_Istruction_DLI - Function - Istruzione non riconosciuta : " & wIstr, "DR - GenRout"
  End Select

  Transcode_Istruction_DLI = xwIstr
End Function

Public Function getTotalIndent(Line As String) As Integer
  Dim posind As Integer, posminor As Integer, posmaior As Integer
  Dim tag As String
  
  While InStr(1, Line, "%IND") > 0
    posind = InStr(1, Line, "%IND")
    posminor = posind - 1
    Do While posind > 0 And posminor > 1
      If Mid(Line, posminor, 1) = "<" Then
        Exit Do
      End If
      posminor = posminor - 1
    Loop
    posmaior = posind + 1
    Do While posind > 0 And Not posmaior > Len(Line)
      If Mid(Line, posmaior, 1) = ">" Then
        Exit Do
      End If
      posmaior = posmaior + 1
    Loop
    tag = Mid(Line, posminor, posmaior - posminor + 1)
    getTotalIndent = getTotalIndent + GetIndent(tag)
    Line = Replace(Line, tag, "")
   Wend
End Function

Public Function getIndentAtEnd(ByVal Line As String) As String
  Dim lenline As Integer, tag As String

  lenline = Len(Line)
  While Mid(Line, Len(Line), 1) = ">"
    lenline = lenline - 1
    Do While lenline
      If Mid(Line, lenline, 1) = "<" Then
        tag = Mid(Line, lenline, Len(Line) - lenline + 1)
        Exit Do
      End If
      lenline = lenline - 1
    Loop
    If InStr(1, tag, "IND") > 0 Then
      getIndentAtEnd = tag & getIndentAtEnd
    End If
    Line = Left(Line, Len(Line) - Len(tag))
  Wend
End Function

Public Function ReplaceTagtoo(inputstr As String, target As String, subst As String) As String
  Dim tagind As String, s() As String, i As Integer
  
  tagind = getIndentAtEnd(inputstr)
  inputstr = Replace(inputstr, target, subst)
  s = Split(inputstr, vbCrLf)
  For i = 0 To UBound(s) - 1
    s(i) = s(i) & tagind
    ReplaceTagtoo = ReplaceTagtoo & s(i) & vbCrLf
  Next i
  ReplaceTagtoo = ReplaceTagtoo & s(UBound(s))
End Function

Public Function GetIdentificator(tag As String, pos As Integer) As String
  Dim posperc As Integer, posslash As Integer, posend As Integer
  
  posperc = InStr(tag, "%")
  posslash = InStr(tag, "/")
  posend = Len(tag)
  If pos = 1 Then
    GetIdentificator = Mid(tag, posperc + 1, posslash - posperc - 1)
  Else
    GetIdentificator = Mid(tag, posslash + 1, posend - posslash - 1)
  End If
End Function

Public Function GetIndent(tag As String) As Integer
  Dim moltp As Integer, posind As Integer, posclose As Integer
  
  posind = InStr(tag, "IND")
  If Mid(tag, posind + 1, 1) = "+" Then
    moltp = 1
  Else
    moltp = -1
  End If
  GetIndent = moltp * CInt(Mid(tag, posind + 4, Len(tag) - posind - 4))
End Function

Public Function TROVA_RIGA(RTBox As RichTextBox, Chiave As String, Riga As Long, Optional ByVal StartFind As Long = 0) As Boolean
  Dim FoundPos As Long, FoundLine As Long
  Dim flag As String
  Dim Testo As String
  
  FoundPos = StartFind
  flag = True  'SQ: facciamo davvero?!

  While flag
    FoundPos = RTBox.find(Trim(Chiave), FoundPos + 1)
'    RTBox.SetFocus
    If FoundPos = -1 Then
      flag = False
    End If

    If FoundPos <> -1 Then
      FoundLine = RTBox.GetLineFromChar(FoundPos) + 1
      If FoundLine = Riga Then
        TROVA_RIGA = True
        Exit Function
      End If
      If FoundLine > Riga Then
         flag = False
      End If
    End If
  Wend

  FoundPos = 0
  flag = True
   
  'stefano: ma perch�???????????????????????????????????????????
  If Chiave = " EXEC " Then
    Chiave = "CBLTDLI"
  Else
    Chiave = " EXEC "
  End If

  While flag
    FoundPos = RTBox.find(Trim(Chiave), FoundPos + 1)
    If FoundPos = -1 Then
      TROVA_RIGA = False
      Exit Function
    End If
    If FoundPos <> -1 Then
      FoundLine = RTBox.GetLineFromChar(FoundPos) + 1
      If FoundLine = Riga Then
        TROVA_RIGA = True
        Exit Function
      End If
      If FoundLine > Riga Then
         TROVA_RIGA = False
         Exit Function
      End If
    End If
  Wend
End Function

Public Function TROVA_RIGA_EXECDLI(RTBox As RichTextBox, Chiave As String, Riga As Long, Optional ByVal StartFind As Long = 0) As Boolean
  Dim FoundPos As Long, FoundLine As Long
  Dim flag As String
  Dim Testo As String
  
  FoundPos = StartFind
  flag = True  'SQ: facciamo davvero?!

  While flag
    FoundPos = RTBox.find(Trim(Chiave), FoundPos + 1)
'    RTBox.SetFocus
    If FoundPos = -1 Then
      flag = False
    End If

    If FoundPos <> -1 Then
      FoundLine = RTBox.GetLineFromChar(FoundPos) + 1
      If FoundLine = Riga Then
        FoundPos = RTBox.find(" DLI", FoundPos + 1)
        If FoundPos <> -1 Then
          FoundLine = RTBox.GetLineFromChar(FoundPos) + 1
          If FoundLine = Riga Then
            TROVA_RIGA_EXECDLI = True
            Exit Function
          Else
            Exit Function
          End If
        Else
          Exit Function
        End If
      End If

      If FoundLine > Riga Then
        flag = False
      End If
    End If
  Wend

  FoundPos = 0
  flag = True
  
  'stefano: ma perch�???????????????????????????????????????????
  If Chiave = " EXEC " Then
    Chiave = "CBLTDLI"
  Else
    Chiave = " EXEC "
  End If

  While flag
    FoundPos = RTBox.find(Trim(Chiave), FoundPos + 1)
    If FoundPos = -1 Then
      TROVA_RIGA_EXECDLI = False
      Exit Function
    End If
    If FoundPos <> -1 Then
      FoundLine = RTBox.GetLineFromChar(FoundPos) + 1
      If FoundLine = Riga Then
        TROVA_RIGA_EXECDLI = True
        Exit Function
      End If
      If FoundLine > Riga Then
        TROVA_RIGA_EXECDLI = False
        Exit Function
      End If
    End If
  Wend
End Function

'copia ma con BlkIstrDliLocal... MA NON CREDO SERVA A QUALCUNO!!!!????
Public Function CarNumKey(livello As BlkIstrDliLocal, livKey As Integer) As Integer
  Dim numKey As Integer
  Dim i As Integer
  
  'SILVIA
  '...
  'For k1 = 1 To k3
  
  For i = livKey To 1 Step -1
    If livello.KeyDef(i - 1).key = livello.KeyDef(livKey).key And livello.KeyDef(i).key <> "" Then
      'silvia
      numKey = livello.KeyDef(i - 1).KeyNum + 1
      Exit For
    End If
  Next i
  If numKey = 0 Then
    numKey = 1
  End If

  CarNumKey = numKey
End Function

''Function ControllaUIB(RTBModifica As RichTextBox, typeIncaps As String) As Boolean
''  Dim pPos As Variant
''  Dim pstart As Variant
''  Dim pend As Variant
''  Dim pInit As Variant
''  Dim PFine As Variant
''
''  ControllaUIB = True
''
''  If typeIncaps <> "DLI" Then
''    pend = Len(RTBModifica.text)
''    pPos = 1
''    While pPos > 0
''      pPos = RTBModifica.find(" ADDRESS ", pPos + 1, pend)
''      If pPos > 0 Then
''        PFine = RTBModifica.find(vbCrLf, pPos + 1, pend)
''        For pInit = pPos To pPos - 80 Step -1
''          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then Exit For
''        Next pInit
''        pPos = RTBModifica.find(" SET ", pInit, PFine)
''        If pPos > 0 Then
''          pPos = RTBModifica.find(" OF ", pInit, PFine)
''          If pPos > 0 Then
''            pPos = RTBModifica.find(" PCB", pInit, PFine)
''            If pPos > 0 Then
''              RTBModifica.SelStart = pInit + 7
''              RTBModifica.SelLength = 1
''              RTBModifica.SelText = "*"
''            Else
''              pPos = RTBModifica.find(" BPCB", pInit, PFine)
''              If pPos > 0 Then
''                RTBModifica.SelStart = pInit + 7
''                RTBModifica.SelLength = 1
''                RTBModifica.SelText = "*"
''              End If
''            End If
''          End If
''        End If
''        pPos = PFine + 1
''      End If
''    Wend
''  End If
''
''  If typeIncaps <> "DLI" Then
''    pPos = 1
''    While pPos > 0
''      pPos = RTBModifica.find(" UIBPCBAL ", pPos + 1, pend)
''      If pPos > 0 Then
''        PFine = RTBModifica.find(vbCrLf, pPos + 1, pend)
''        For pInit = pPos To pPos - 80 Step -1
''          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then Exit For
''        Next pInit
''        pPos = RTBModifica.find(" MOVE ", pInit, PFine)
''        If pPos > 0 Then
''          RTBModifica.SelStart = pInit + 7
''          RTBModifica.SelLength = 1
''          RTBModifica.SelText = "*"
''        End If
''        pPos = PFine + 1
''      End If
''    Wend
''  End If
''
''  If typeIncaps <> "DLI" Then
''    pPos = 1
''    While pPos > 0
''      pPos = RTBModifica.find(" BPCBPTR", pPos + 1, pend)
''      If pPos > 0 Then
''        PFine = RTBModifica.find(vbCrLf, pPos + 1, pend)
''        For pInit = pPos To pPos - 80 Step -1
''          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then Exit For
''        Next pInit
''        pPos = RTBModifica.find(" MOVE ", pInit, PFine)
''        If pPos > 0 Then
''          RTBModifica.SelStart = pInit + 7
''          RTBModifica.SelLength = 1
''          RTBModifica.SelText = "*"
''        End If
''        pPos = PFine + 1
''      End If
''    Wend
''  End If
''
''  If typeIncaps <> "DLI" Then
''    pPos = 1
''    While pPos > 0
''      pPos = RTBModifica.find(" BPCB1PTR", pPos + 1, pend)
''      If pPos > 0 Then
''        PFine = RTBModifica.find(vbCrLf, pPos + 1, pend)
''        For pInit = pPos To pPos - 80 Step -1
''          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then Exit For
''        Next pInit
''        pPos = RTBModifica.find(" MOVE ", pInit, PFine)
''        If pPos > 0 Then
''          RTBModifica.SelStart = pInit + 7
''          RTBModifica.SelLength = 1
''          RTBModifica.SelText = "*"
''        End If
''        pPos = PFine + 1
''      End If
''    Wend
''  End If
''
''  If typeIncaps <> "DLI" Then
''    pPos = 1
''    While pPos > 0
''      pPos = RTBModifica.find(" BPCB2PTR", pPos + 1, pend)
''      If pPos > 0 Then
''        PFine = RTBModifica.find(vbCrLf, pPos + 1, pend)
''        For pInit = pPos To pPos - 80 Step -1
''          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then Exit For
''        Next pInit
''        pPos = RTBModifica.find(" MOVE ", pInit, PFine)
''        If pPos > 0 Then
''          RTBModifica.SelStart = pInit + 7
''          RTBModifica.SelLength = 1
''          RTBModifica.SelText = "*"
''        End If
''        pPos = PFine + 1
''      End If
''    Wend
''  End If
''
''  If typeIncaps <> "DLI" Then
''    pPos = 1
''    While pPos > 0
''      pPos = RTBModifica.find(" FCNOTOPEN", pPos + 1, pend)
''      If pPos > 0 Then
''        PFine = RTBModifica.find(vbCrLf, pPos + 1, pend)
''        For pInit = pPos To pPos - 80 Step -1
''          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then Exit For
''        Next pInit
''        pPos = RTBModifica.find(".", pInit, PFine)
''        While pPos < 1
''          PFine = RTBModifica.find(vbCrLf, PFine + 2, pend)
''          pPos = RTBModifica.find(".", pInit, PFine)
''        Wend
''        pPos = RTBModifica.find(" IF ", pInit, PFine)
''        If pPos > 0 Then
''          PFine = PFine - 2
''          pPos = pInit - 4
''          pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
''          While pPos > 0
''            RTBModifica.SelStart = pPos + 8
''            RTBModifica.SelLength = 1
''            RTBModifica.SelText = "*"
''            pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
''          Wend
''        End If
''        pPos = PFine + 1
''      End If
''    Wend
''  End If
''
''  If typeIncaps <> "DLI" Then
''    pPos = 1
''    While pPos > 0
''      pPos = RTBModifica.find(" FCINVREQ", pPos + 1, pend)
''      If pPos > 0 Then
''        PFine = RTBModifica.find(vbCrLf, pPos + 1, pend)
''        For pInit = pPos To pPos - 80 Step -1
''          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then Exit For
''        Next pInit
''        pPos = RTBModifica.find(".", pInit, PFine)
''        While pPos < 1
''          PFine = RTBModifica.find(vbCrLf, PFine + 2, pend)
''          pPos = RTBModifica.find(".", pInit, PFine)
''        Wend
''        pPos = RTBModifica.find(" IF ", pInit, PFine)
''        If pPos > 0 Then
''          PFine = PFine - 2
''          pPos = pInit - 4
''          pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
''          While pPos > 0
''            RTBModifica.SelStart = pPos + 8
''            RTBModifica.SelLength = 1
''            RTBModifica.SelText = "*"
''            pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
''          Wend
''        End If
''        pPos = PFine + 1
''      End If
''    Wend
''  End If
''
''  If typeIncaps <> "DLI" Then
''    pPos = 1
''    While pPos > 0
''      pPos = RTBModifica.find(" FCINVPCB", pPos + 1, pend)
''      If pPos > 0 Then
''        PFine = RTBModifica.find(vbCrLf, pPos + 1, pend)
''        For pInit = pPos To pPos - 80 Step -1
''          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then Exit For
''        Next pInit
''        pPos = RTBModifica.find(".", pInit, PFine)
''        While pPos < 1
''          PFine = RTBModifica.find(vbCrLf, PFine + 2, pend)
''          pPos = RTBModifica.find(".", pInit, PFine)
''        Wend
''        pPos = RTBModifica.find(" IF ", pInit, PFine)
''        If pPos > 0 Then
''          PFine = PFine - 2
''          pPos = pInit - 4
''          pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
''          While pPos > 0
''            RTBModifica.SelStart = pPos + 8
''            RTBModifica.SelLength = 1
''            RTBModifica.SelText = "*"
''            pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
''          Wend
''        End If
''        pPos = PFine + 1
''      End If
''    Wend
''  End If
''
''  pPos = 1
''  While pPos > 0
''    pPos = RTBModifica.find("DLITCBL", pPos + 1, pend)
''    If pPos > 0 Then
''      PFine = RTBModifica.find(vbCrLf, pPos + 1, pend)
''      For pInit = pPos To pPos - 80 Step -1
''        If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then Exit For
''      Next pInit
''      pPos = RTBModifica.find(".", pInit, PFine)
''      While pPos < 1
''        PFine = RTBModifica.find(vbCrLf, PFine + 2, pend)
''        pPos = RTBModifica.find(".", pInit, PFine)
''      Wend
''      pPos = RTBModifica.find(" ENTRY ", pInit, PFine)
''      If pPos > 0 Then
''        PFine = PFine - 2
''        pPos = pInit - 4
''        pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
''        While pPos > 0
''          pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
''        Wend
''        pPos = RTBModifica.find(vbCrLf, PFine - 5, PFine + 4)
''        If typeIncaps <> "DLI" Then
''          RTBModifica.SelText = vbCrLf & Space$(11) & "MOVE SPACES TO LNKDB-STATO LNKDB-IMSRTNAME-QUAL2. " & vbCrLf
''        Else
''          RTBModifica.SelText = vbCrLf & Space$(11) & "MOVE SPACES TO LNKDB-IMSRTNAME-QUAL2.  " & vbCrLf
''        End If
''        pPos = RTBModifica.find(vbCrLf, pPos + 1, pend)
''        PFine = pPos
''      End If
''      pPos = PFine + 1
''    End If
''  Wend
''End Function

''Function CambiaParole(RTBModifica As RichTextBox, typeIncaps As String, wWhere As String) As Boolean
''  Dim Ind As Integer
''  Dim k As Variant
''
''  Dim pend As Variant
''  Dim pPos As Variant
''  Dim pstart As Variant
''  Dim pInit As Variant
''  Dim ModSw As Boolean
''  Dim SwTolta As Boolean
''  Dim Num01 As Integer
''  Dim xpos As Variant
''  Dim xLen As Variant
''  Dim i As Long
''  Dim idx01 As Long
''
''  Dim pGo As Long
''
''  Dim wLine As String
''
''  Dim NumApi As Integer
''  Dim wstr As String
''
''  If wWhere = "ALL" Then
''    pstart = 1
''  Else
''    pstart = RTBModifica.find(" " & wWhere, 1)
''
''    Do While pstart > 0
''      pend = RTBModifica.find(vbCrLf, pstart) - 1
''
''      For i = 1 To 80
''        RTBModifica.SelStart = pstart - i
''        RTBModifica.SelLength = 2
''        If RTBModifica.SelText = vbCrLf Then
''          pInit = i + 1
''          Exit For
''        End If
''      Next i
''      RTBModifica.SelStart = pInit
''      RTBModifica.SelLength = pend - pInit
''
''      wLine = RTBModifica.SelText
''
''      If Mid(wLine, 7, 1) <> "*" Then
''        If wWhere = "PROCEDURE" Then
''          idx01 = RTBModifica.find(" DIVISION", pstart)
''        End If
''        If wWhere = "LINKAGE" Then
''          idx01 = RTBModifica.find(" SECTION", pstart)
''        End If
''        If wWhere = "WORKING-STORAGE" Then
''          idx01 = RTBModifica.find(" SECTION", pstart)
''        End If
''        If idx01 > 0 Then
''          pGo = pend
''          Exit Do
''        End If
''      End If
''
''      pstart = RTBModifica.find(" " & wWhere, pend)
''    Loop
''  End If
''End Function

''Function CreaSp(NumB, Optional ByVal wLabel As String) As String
''  If wLabel = "" Then wLabel = START_TAG
''  CreaSp = wLabel & Space(NumB - 7)
''End Function

Public Function Restituisci_Livello_Segmento_dbd(widDbd As Long, wNomeSeg As String, Optional ByVal NON_SETTARE_OptionalInterno As Boolean = True) As Long
  Dim rs As Recordset
  Static wLevel As Long

  If NON_SETTARE_OptionalInterno Then
    wLevel = 0
  End If

  Set rs = m_fun.Open_Recordset("Select * From PsRel_Dliseg Where " & _
                                "IdOggetto = " & widDbd & " And Child = '" & wNomeSeg & "'")
  If rs.RecordCount Then
    If rs!father <> 0 Then
      wLevel = wLevel + 1
      wLevel = Restituisci_Livello_Segmento_dbd(widDbd, rs!father, False)
      Restituisci_Livello_Segmento_dbd = wLevel
    Else
      Restituisci_Livello_Segmento_dbd = wLevel
      Exit Function
    End If
  End If

  rs.Close
End Function

Public Function Restituisci_Livello_Segmento(IdPSB As Long, numpcb As Long, wNomeSeg As String, wLevel As Integer) As Integer
  Dim rs As Recordset
  Dim parent As String
  
  ' Mauro :
  ' sono costretto a lasciare le close sulle ELSE a causa della close per il giro del II livello
  Set rs = m_fun.Open_Recordset("Select Parent From PsDLI_PsbSeg Where " & _
                                "IdOggetto = " & IdPSB & " And NumPcb = " & numpcb & " And Segmento = '" & wNomeSeg & "'")
  If Not rs.EOF Then
    If Not rs!parent = 0 Then
      wLevel = wLevel + 1
      parent = rs!parent
      rs.Close ' chiudo prima di riaprirne un altro
      'ricorsiva per livelli > 2
      wLevel = Restituisci_Livello_Segmento(IdPSB, numpcb, parent, wLevel)
    Else
      rs.Close
    End If
  Else
    rs.Close
  End If
  Restituisci_Livello_Segmento = wLevel
End Function

Public Function Restituisci_ProcSeq(IdPSB As Long, numpcb As Long) As String
  Dim rs As Recordset
  Dim wProcSeq As String
  
  Set rs = m_fun.Open_Recordset("Select ProcSeq From PsDLI_Psb Where IdOggetto = " & IdPSB & " And NumPcb = " & numpcb)
  If Not rs.EOF Then
    'stefano: ma quando � null e quando invece � una stringa vuota? Fatemelo sapere
    If IsNull(rs!procSeq) Then
      wProcSeq = ""
    Else
      wProcSeq = IIf(rs!procSeq = "None", "", rs!procSeq)
    End If
  Else
    wProcSeq = ""
  End If
  rs.Close
  Restituisci_ProcSeq = wProcSeq
End Function

Public Function Restituisci_ProcOpt(IdPSB As Long, numpcb As Long) As String
  Dim rs As Recordset
  Dim wProcOpt As String
  Set rs = m_fun.Open_Recordset("Select ProcOpt From PsDLI_Psb Where IdOggetto = " & IdPSB & " And NumPcb = " & numpcb)

  If Not rs.EOF Then
    'stefano: ma quando � null e quando invece � una stringa vuota? Fatemelo sapere
    If IsNull(rs!procOpt) Then
      wProcOpt = ""
    Else
      wProcOpt = rs!procOpt
    End If
  Else
    wProcOpt = ""
  End If
  rs.Close
  Restituisci_ProcOpt = wProcOpt
End Function

Public Function Restituisci_varFunz(IdOgg As Long, Riga As Long) As String
  Dim rs As Recordset
  Dim wFunz As String
  
  Set rs = m_fun.Open_Recordset("Select parametri From PsCall Where IdOggetto = " & IdOgg & " And Riga = " & Riga)
  If Not rs.EOF Then
    If IsNull(rs!parametri) Then
      wFunz = "[VARIABLE MISSING]"
      m_fun.WriteLog "Incapsulatore - Restituisci_varFunz " & vbCrLf & _
                     "Variabile dell'istruzione non trovata " & IdOgg & " " & Riga, "DR - Incapsulatore"
    Else
      'stefano: parm-count
      'wFunz = Left(rs!parametri, InStr(rs!parametri, " ") - 1)
      wFunz = nextToken(rs!parametri)
      If getFieldType(wFunz, IdOgg) = "9" Then
         wFunz = nextToken(Right(rs!parametri, Len(rs!parametri) - InStr(rs!parametri, " ") + 1))
      End If
    End If
  Else
    wFunz = "[VARIABLE MISSING]"
    m_fun.WriteLog "Incapsulatore - Restituisci_varFunz " & vbCrLf & _
                   "Variabile dell'istruzione non trovata " & IdOgg & " " & Riga, "DR - Incapsulatore"
  End If
  rs.Close
  Restituisci_varFunz = wFunz
End Function

Public Function Restituisci_varPcb(IdOgg As Long, Riga As Long) As String
  Dim rs As Recordset
  Dim wFunz As String
  
  Set rs = m_fun.Open_Recordset("Select NumPcb From PsDLi_Istruzioni Where IdOggetto = " & IdOgg & " And Riga = " & Riga)
  If Not rs.EOF Then
    Restituisci_varPcb = IIf(IsNull(rs!numpcb), "", rs!numpcb)
  End If
  rs.Close
End Function

Public Function Restituisci_numPcb(idpgm As Long, IdOgg As Long, Riga As Long, dbName As String, list() As String, listNumberChecks() As Integer) As String
  Dim rs As Recordset, ISpcbIstructionused As Boolean
  Dim IdPSB As Long, i As Integer, j As Integer, numpcb As String
  Dim wFunz As String
  
  'AC
  Set rs = m_fun.Open_Recordset("Select Pcb from MgdLi_IstrCodifica Where IdPgm = " & idpgm & " and IdOggetto = " & IdOgg & " And Riga = " & Riga _
                              & " and Left(Decodifica," & CStr(11 + Len(dbName)) & ") = '<dbd>" & dbName & "</dbd>'")
  If Not rs.EOF Then
    Restituisci_numPcb = rs!pcb
  Else
    If Not ISpcbIstructionused Then
      rs.Close
      Set rs = m_fun.Open_Recordset("Select ordPcb from PsdLi_Istruzioni Where IdPgm = " & idpgm & " and IdOggetto = " & IdOgg & " And Riga = " & Riga)
      If Not rs.EOF Then
        Restituisci_numPcb = rs!ordPcb
      End If
      ISpcbIstructionused = True
    End If
  End If
  rs.Close
  If Restituisci_numPcb = 0 Then
    AggLog "[Pgm: " & idpgm & ",Row: " & Riga & "] Pcb number = 0: not valid to manage pcb molteplicity!", MadrdF_Incapsulatore.RTErr
  End If
End Function

Sub FillListPcb(idpgm As Long, IdOgg As Long, Riga As Long, list() As String, listNumberChecks() As Integer, combinazioni As Integer)
  Dim rs As Recordset, ratio As Integer, i As Integer
  
  ReDim list(0)
  Set rs = m_fun.Open_Recordset("Select OrdPcb From PsDLi_Istruzioni Where idpgm = " & idpgm & " and IdOggetto = " & IdOgg & " And Riga = " & Riga)
  If Not rs.EOF Then
    list(0) = IIf(IsNull(rs!ordPcb), "", rs!ordPcb)
  End If
  rs.Close
  Set rs = m_fun.Open_Recordset("Select OrdPcb From PsDLi_Istruzioni_Clone Where IdPgm = " & idpgm & " and IdOggetto = " & IdOgg & " And Riga = " & Riga & " and ordPcb is not null")
  While Not rs.EOF
    ReDim Preserve list(UBound(list) + 1)
    list(UBound(list)) = IIf(IsNull(rs!ordPcb), "", rs!ordPcb)
    rs.MoveNext
  Wend
  ratio = combinazioni / (UBound(list) + 1)  ' ratio � il numero di volte che il numPcb dovr� essere utilizzato
  ReDim listNumberChecks(UBound(list))
  For i = 0 To UBound(listNumberChecks)
    listNumberChecks(i) = ratio
  Next i
  rs.Close
End Sub

'stefano
Function getFieldType(fieldName As String, IdOgg As Long) As String
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("SELECT Tipo From PsData_Cmp Where idOggetto = " & IdOgg & " And nome = '" & fieldName & "'" & _
                                " UNION " & _
                                "SELECT a.tipo From PsData_Cmp as a,PsRel_Obj as b" & _
                                "  where b.idOggettoC = " & IdOgg & " and b.relazione='CPY' And a.IdOggetto=b.IdOggettoR and a.nome = '" & fieldName & "'")
  If rs.RecordCount Then
    getFieldType = rs!tipo
  Else
    getFieldType = ""
  End If
  rs.Close
End Function

Function getTipIstr(pXistr As String, initTipIstr As String, pwUtil As Boolean) As String
  getTipIstr = initTipIstr
  If TabIstr.isGsam Then
    getTipIstr = "GS"
  ElseIf pXistr = "GU" Or pXistr = "GHU" Then
    getTipIstr = "GU"
  ElseIf pXistr = "POS" Then 'IRIS-DB
    getTipIstr = "PS" 'IRIS-DB
  ElseIf pXistr = "ISRT" Then
    getTipIstr = "UP"
  ElseIf pXistr = "DLET" Then
    getTipIstr = "UP"
  ElseIf pXistr = "REPL" Then
    getTipIstr = "UP"
  ElseIf pXistr = "GNP" Or pXistr = "GHNP" Then
    getTipIstr = "GP"
  ElseIf pXistr = "GN" Or pXistr = "GHN" Then
    getTipIstr = "GN"
  ElseIf pXistr = "PCB" Then
    pwUtil = True
    getTipIstr = "SH"
  ElseIf pXistr = "TERM" Then
    pwUtil = True
    getTipIstr = "TM"
  ElseIf pXistr = "CHKP" Then
    pwUtil = True
    getTipIstr = "CP"
  ElseIf pXistr = "QRY" Then
    pwUtil = True
    getTipIstr = "QY"
  ElseIf pXistr = "SYNC" Or pXistr = "SYMCHKP" Then
    pwUtil = True
    getTipIstr = "SY"
  ElseIf pXistr = "XRST" Then
    pwUtil = True
    getTipIstr = "XR"
  ElseIf pXistr = "SCHED" Then
    pwUtil = True
    getTipIstr = "SC"
  Else
    'm_fun.WriteLog "Incapsulatore - Costruisci Operazione " & vbCrLf & _
                    "Istruzione non riconosciuta " & pxIstr, "DR - Incapsulatore"
  End If
End Function

Public Sub CambiaENTRYTDLI(RT As RichTextBox, typeIncaps As String)
  Dim idx01 As Long, idxStart As Long, idxStop As Long
  Dim i As Long
  Dim wSpc As Long
  
  'SP 27/9 forzature alpitour
  Dim idxWk As Long, idxProc As Long
  
  'SP 4/9: NON BASTA! Bisogna vedere se � asteriscata! In tutte le ricerche mancava
  Dim entryOk As Boolean, entryKo As Boolean
  
  entryOk = False
  entryKo = False
  i = 1
  While Not entryOk And Not entryKo
    idx01 = RT.find("ENTRY ", i)
    If idx01 > 0 Then
      For idxStart = idx01 To idx01 - 80 Step -1
        RT.SelStart = idxStart
        RT.SelLength = 2
        If RT.SelText = vbCrLf Then Exit For
      Next idxStart
      RT.SelStart = idxStart + 7
      RT.SelLength = 1
      If RT.SelText <> "*" Then
        entryOk = True
      Else
        i = idx01 + 6
      End If
    Else
      entryKo = True
    End If
  Wend
    
  If idx01 > 0 Then
    idx01 = RT.find("'DLITCBL'", idx01)
    If idx01 > 0 Then
      'Torna indietro
      For idxStart = idx01 To idx01 - 80 Step -1
        RT.SelStart = idxStart
        RT.SelLength = 2
        If RT.SelText = vbCrLf Then Exit For
      Next idxStart
      idxStop = RT.find(".", idx01)
      idxStart = idxStart - 1
      
      Do While idxStart > 0
        If idxStart > 0 And idxStop > 0 Then
          'asterisca
          RT.SelStart = idxStart + 2
          RT.SelLength = 7
          RT.SelText = m_fun.LABEL_ENTRY & "*"
        End If
        idxStart = RT.find(vbCrLf, idxStart + 2)
        If idxStart > idxStop Then Exit Do
      Loop
      
      'SP: 4/9: provo, a volte non funziona....
      'idxStop = RT.Find(vbCrLf, idxStart + 2)
      idxStop = RT.find(vbCrLf, idxStop)
       
      'Si posiziona alla fine
      RT.SelStart = idxStop + 2
      RT.SelLength = 0
      RT.SelText = m_fun.LABEL_ENTRY & "     COPY " & m_fun.COPY_NAME_ENTRYDLI & "." & vbCrLf
      
      'SP 27/9: specifica per Alpitour: istruzione DB2 per problemi di BIND
      'ovviamente da togliere poi
      If Not SwTp Then
        idxStop = RT.find("SQLCA", 1) 'cerco se gi� SQL
        If idxStop < 1 Then
          idxStop = RT.find("05  IMSRTNAME-QUAL2   PIC X(02)  VALUE SPACES.", 1) 'cerca posizione ws
          If idxStop > 0 Then
            idxStop = RT.find(vbCrLf, idxStop)
            RT.SelStart = idxStop + 2
            RT.SelLength = 0
            RT.SelText = m_fun.LABEL_ENTRY & " 01 ITER-BPHX-DATA PIC X(010)." & vbCrLf & _
                         m_fun.LABEL_ENTRY & "     EXEC SQL INCLUDE SQLCA END-EXEC." & vbCrLf
            idxStop = RT.find("COPY ENTRYDLI.", idxStop) 'cerco se gi� SQL
            If idxStop > 0 Then
              idxStop = RT.find(vbCrLf, idxStop)
              RT.SelStart = idxStop + 2
              RT.SelLength = 0
              RT.SelText = m_fun.LABEL_ENTRY & "     EXEC SQL SET :ITER-BPHX-DATA = CURRENT DATE END-EXEC." & vbCrLf
            End If
          End If
        End If
      End If
    End If
  End If
End Sub
'IRIS-DB the whole function
Public Sub EncapsENTRYTDLI(RT As RichTextBox, IdOgg As Long)
  Dim idx01 As Long, idxStart As Long, idxStop As Long
  Dim i As Long
  Dim wSpc As Long
  Dim rsEntry As Recordset
  Dim rsPSB As Recordset
  Dim psbName As String
  Dim idx02, idx03, idx04 As Long 'IRIS-DB
  'SP 27/9 forzature alpitour
  Dim idxWk As Long, idxProc As Long
  
  'SP 4/9: NON BASTA! Bisogna vedere se � asteriscata! In tutte le ricerche mancava
  Dim entryOk As Boolean, entryKo As Boolean
  Dim lastPCB As String 'IRIS-DB
  Dim CMPAT_Yes As Boolean
  Dim PsbId As Long
  Dim addIOPcb As Long
  Dim dynamicSSA As Boolean
  Dim param As String
    
  entryOk = False
  entryKo = False
  i = 1
  'IRIS-DB: questo primo ciclo � abbastanza inutile, basterebbe cercare 'DLITCBL'
  While Not entryOk And Not entryKo
    idx01 = RT.find("ENTRY ", i)
    If idx01 > 0 Then
      For idxStart = idx01 To idx01 - 80 Step -1
        RT.SelStart = idxStart
        RT.SelLength = 2
        If RT.SelText = vbCrLf Then Exit For
      Next idxStart
      RT.SelStart = idxStart + 7
      RT.SelLength = 1
      If RT.SelText <> "*" Then
        entryOk = True
      Else
        i = idx01 + 6
      End If
    Else
      entryKo = True 'IRIS-DB da gestire
    End If
  Wend
    
  If idx01 > 0 Then
    idx01 = RT.find("'DLITCBL'", idx01)
    If idx01 > 0 Then
      'Torna indietro
      For idxStart = idx01 To idx01 - 80 Step -1
        RT.SelStart = idxStart
        RT.SelLength = 2
        If RT.SelText = vbCrLf Then Exit For
      Next idxStart
      entryOk = False
      entryKo = False
      i = 1
      'IRIS-DB modificato in quanto il punto potrebbe non esserci (� obbligatorio, ma il compilatore d� solo warning in caso di assenza)
      Set rsEntry = m_fun.Open_Recordset("Select PCB from PsIMS_Entry where idoggetto = " & IdOgg & " order by numpcb desc") 'IRIS-DB: legge anche in fondo, da spostare in futuro
      If Not rsEntry.EOF Then
        lastPCB = rsEntry!pcb
      Else
         m_fun.WriteLog "encapsEntry - Not found PsIMS_Entry - idoggetto (" & IdOgg
      End If
      rsEntry.Close
      While Not entryOk And Not entryKo
        'IRIS-DB point not mandatory idx01 = RT.find(".", idxStart)
        idx02 = RT.find(lastPCB & " ", idxStart) 'IRIS-DB
        If idx02 = -1 Then idx02 = 999999999 'IRIS-DB
        idx03 = RT.find(lastPCB & ".", idxStart) 'IRIS-DB
        If idx03 = -1 Then idx03 = 999999999 'IRIS-DB
        idx04 = RT.find(lastPCB & vbCrLf, idxStart) 'IRIS-DB
        If idx04 = -1 Then idx04 = 999999999 'IRIS-DB
        'IRIS-DB all next IF
        If idx02 > idx03 Then
          If idx03 > idx04 Then
            idx01 = idx04
          Else
            idx01 = idx03
          End If
        Else
          If idx02 > idx04 Then
            idx01 = idx04
          Else
            idx01 = idx02
          End If
        End If
        If idx01 > 0 And idx01 < 999999999 Then 'IRIS-DB
          For idxStart = idx01 To idx01 - 80 Step -1
            RT.SelStart = idxStart
            RT.SelLength = 2
            If RT.SelText = vbCrLf Then Exit For
          Next idxStart
          RT.SelStart = idxStart + 7
          RT.SelLength = 1
          If RT.SelText <> "*" Then
            entryOk = True
          Else
            idxStart = idx01 + 6
          End If
        Else
          entryKo = True 'IRIS-DB da gestire
        End If
      Wend
      idxStart = idxStart - 1
      
      idxStop = RT.find(vbCrLf, idxStart + 2)
      'idxStop = RT.find(vbCrLf, idxStop)
      'IRIS-DB cerca se c'� un punto solo soletto nella prossima riga, nel qual caso lo considera parte della ENTRY. Non controlla righe successive o righe asteriscate
      idxStart = RT.find(".", idxStop + 9)
      If Trim(Mid(RT.text, idxStop + 10, idxStart - idxStop - 9)) = "" Then ' cerca se ci sono solo spazi tra il byte 7 - cos� esclude asterischi - e il punto
        idxStop = RT.find(vbCrLf, idxStart) ' se s�, include anche il punto nella Entry
      End If
      'Si posiziona alla fine
      RT.SelStart = idxStop + 2
      RT.SelLength = 0
      'IRIS-DB: attenzione che prende solo il primo in tabella in caso di PSB multipli
      Set rsPSB = m_fun.Open_Recordset("Select a.nome as PSBName, b.IdPSB as PsbId from bs_oggetti as a, PsDLI_IstrPSB as b  where " & _
                                        "a.idoggetto = b.IdPSB and b.idpgm = " & IdOgg)
      If Not rsPSB.EOF Then
         psbName = rsPSB!psbName
         PsbId = rsPSB!PsbId
         rsPSB.Close
      Else
      'IRIS-DB: faccio una pezza per non cambiare tutto: ci sono programmi che hanno solo la entry per cui non sono presenti nelle istruzioni
         rsPSB.Close
         Set rsPSB = m_fun.Open_Recordset("Select a.nome as PSBName, b.IdOggettoR as PsbId from bs_oggetti as a, PsRel_Obj as b where " & _
                                        "a.idoggetto = b.IdOggettoR and b.idOggettoC = " & IdOgg & " and Relazione = 'PSB'")
         If Not rsPSB.EOF Then
            psbName = rsPSB!psbName
            PsbId = rsPSB!PsbId
         Else
            psbName = "MISSING"
            PsbId = 0
         End If
         rsPSB.Close
      End If
      RT.SelText = "IRISDB     SET IRIS-FUNC-DLIT TO TRUE" & vbCrLf _
                 & "IRISDB     SET IRISUTIL-RTN TO TRUE" & vbCrLf _
                 & "IRISDB     MOVE '" & psbName & "' TO IRIS-PSB-NAME" & vbCrLf
      AddRighe = 3 'IRIS-DB reinizializza perch� � fatta in fondo
      
      'IRIS-DB: non chiaro come potesse funzionare prima di usare la string, in caso di parametro vuoto
      ' dynamicSSA = m_fun.LeggiParam("IMSDB-DYNAMICSSA")
      param = m_fun.LeggiParam("IMSDB-DYNAMICSSA")
      If Not Len(param) Then
        dynamicSSA = False
      Else
        dynamicSSA = param
      End If
      
      If Not dynamicSSA Then
        CMPAT_Yes = True
        Set rsPSB = m_fun.Open_Recordset("Select NumPcb, TypePcb, DbdName from PsDLI_PSB  where idOggetto = " & PsbId)
        If rsPSB.EOF Then
         'IRIS-DB only IO PCB present
          RT.SelText = RT.SelText & "IRISDB     SET IRIS-PCB-TYPE-TP(1) TO TRUE" & vbCrLf
        Else
          While Not rsPSB.EOF
            If rsPSB!numpcb = 1 Then
              CMPAT_Yes = False
            End If
            If CMPAT_Yes And rsPSB!numpcb = 2 Then
              CMPAT_Yes = False
              RT.SelText = RT.SelText & "IRISDB     SET IRIS-PCB-TYPE-TP(1) TO TRUE" & vbCrLf
              AddRighe = AddRighe + 1 'IRIS-DB
            End If
            If rsPSB!TypePcb = "TP" Then
              RT.SelText = RT.SelText & "IRISDB     SET IRIS-PCB-TYPE-TP(" & rsPSB!numpcb & ") TO TRUE" & vbCrLf
            Else
              RT.SelText = RT.SelText & "IRISDB     SET IRIS-PCB-TYPE-DB(" & rsPSB!numpcb & ") TO TRUE" & vbCrLf
              RT.SelText = RT.SelText & "IRISDB     MOVE '" & rsPSB!dbdname & "' TO IRIS-PCB-DBD(" & rsPSB!numpcb & ")" & vbCrLf
              AddRighe = AddRighe + 1 'IRIS-DB
            End If
            AddRighe = AddRighe + 1 'IRIS-DB
            rsPSB.MoveNext
          Wend
        End If
        rsPSB.Close
      End If
      RT.SelText = RT.SelText & "IRISDB     SET IRIS-MAIN-" & TipoFile & " TO TRUE" & vbCrLf 'IRIS-DB
      AddRighe = AddRighe + 1 'IRIS-DB
      Set rsEntry = m_fun.Open_Recordset("Select PCB from PsIMS_Entry where " & _
                                        "idoggetto = " & IdOgg)
      If Not rsEntry.EOF Then
         RT.SelText = RT.SelText & "IRISDB     MOVE " & rsEntry.RecordCount & " TO IRIS-PARAM-NUM" & vbCrLf
      Else
         RT.SelText = RT.SelText & "IRISDB     MOVE 1 TO IRIS-PARAM-NUM" & vbCrLf
      End If
      AddRighe = AddRighe + 1 'IRIS-DB
      RT.SelText = RT.SelText & "IRISDB     CALL IRIS-WS-RTN USING IRIS-WORK-AREA"
      AddRighe = AddRighe + 1 'IRIS-DB
      While Not rsEntry.EOF
        RT.SelText = RT.SelText & vbCrLf & "IRISDB                 ADDRESS OF " & rsEntry!pcb
        AddRighe = AddRighe + 1 'IRIS-DB
        rsEntry.MoveNext
      Wend
      RT.SelText = RT.SelText & "." & vbCrLf
      rsEntry.Close
      
      m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = OffsetEncaps + " & AddRighe & " where idoggetto = " & IdOgg  'IRIS-DB
      
    End If
  End If
End Sub
'IRIS-DB all the function
Public Sub EncapsSCHEDEXECDLI(RT As RichTextBox, IdOgg As Long)
  Dim idx01 As Long, idxStart As Long, idxStop As Long
  Dim i As Long
  Dim wSpc As Long
  Dim rsOgg As Recordset
  Dim rsPSB As Recordset
  Dim rsPgm As Recordset
  Dim psbName As String
  Dim idx02, idx03, idx04 As Long
  Dim idxWk As Long, idxProc As Long
  
  Dim entryOk As Boolean, entryKo As Boolean
  Dim lastPCB As String
  Dim CMPAT_Yes As Boolean
  Dim PsbId As Long
  Dim addIOPcb As Long
  Dim dynamicSSA As Boolean
  Dim hasCBLTDLI As Boolean
  Dim IsCICS As Boolean
  Dim ArrayPCB() As String
  Dim param As String
      
  'IRIS-DB: verifica se il programma usa le EXEC DLI e se � un batch, nel qual caso aggiunge la SCHED in testa
  'IRIS-DB: modificato: testa se c'� gi� una SCHED, altrimenti inserisce la sched anche se � un CICS
''  Set rsOgg = m_fun.Open_Recordset("Select * from bs_oggetti where idoggetto = " & IdOgg)
''  If Not rsOgg.EOF Then
''     If rsOgg!Cics Then
''       Exit Sub
''     Else
''       rsOgg.Close
''       Set rsOgg = m_fun.Open_Recordset("Select * from PsDLI_istruzioni where idpgm = " & IdOgg & " and isexec = true")
''       If rsOgg.EOF Then
''         Exit Sub
''       End If
''     End If
''  Else
''    m_fun.WriteLog "EncapsSCHEDEXECDLI - Not found Bs_oggetti - idoggetto (" & IdOgg & ")"
''    Exit Sub
''  End If
''  rsOgg.Close
  'IRIS-DB: basta una sola EXEC DLI per considerarlo tutto EXEC, da rivedere per i casi misti
  'IRIS-DB: rimosso proprio per gestire i casi misti, e specificato solo per i batch (sui CICS dovremo mettere la READQ)
''  Set rsOgg = m_fun.Open_Recordset("Select * from PsDLI_istruzioni where idpgm = " & IdOgg & " and isexec = true")
''  If rsOgg.EOF Then
''    rsOgg.Close
''    Exit Sub
''  End If
''  rsOgg.Close
''  Set rsOgg = m_fun.Open_Recordset("Select * from PsDLI_istruzioni where idpgm = " & IdOgg & " and istruzione = 'SCHED'")
''  If Not rsOgg.EOF Then
''    rsOgg.Close
''    Exit Sub
''  End If
''  rsOgg.Close
  IsCICS = False
  Set rsOgg = m_fun.Open_Recordset("Select * from bs_oggetti where idoggetto = " & IdOgg)
  If Not rsOgg.EOF Then
     If rsOgg!CICS Then
       IsCICS = True
     End If
  Else
    m_fun.WriteLog "EncapsSCHEDEXECDLI - Not found Bs_oggetti - idoggetto (" & IdOgg & ")"
    Exit Sub
  End If
  rsOgg.Close
  Set rsOgg = m_fun.Open_Recordset("Select * from PsDLI_istruzioni where idpgm = " & IdOgg & " and isexec = false")
  If rsOgg.EOF Then
    hasCBLTDLI = False
  Else
    hasCBLTDLI = True
  End If
  rsOgg.Close
  
  entryOk = False
  entryKo = False
  i = 1
  While Not entryOk And Not entryKo
    idx01 = RT.find("PROCEDURE ", i)
    If idx01 > 0 Then
      For idxStart = idx01 To idx01 - 80 Step -1
        RT.SelStart = idxStart
        RT.SelLength = 2
        If RT.SelText = vbCrLf Then Exit For
      Next idxStart
      RT.SelStart = idxStart + 7
      RT.SelLength = 1
      If RT.SelText <> "*" Then
        entryOk = True
      Else
        i = idx01 + 6
      End If
    Else
      entryKo = True 'IRIS-DB da gestire
      m_fun.WriteLog "EncapsSCHEDEXECDLI - PROCEDURE DIVISION not found - IdOggetto (" & IdOgg & ")"
      Exit Sub
    End If
  Wend
    
  If idx01 > 0 Then
    entryOk = False
    entryKo = False
    i = idx01
    While Not entryOk And Not entryKo
      idx01 = RT.find(".", i)
      If idx01 > 0 Then
        For idxStart = idx01 To idx01 - 80 Step -1
          RT.SelStart = idxStart
          RT.SelLength = 2
          If RT.SelText = vbCrLf Then Exit For
        Next idxStart
        RT.SelStart = idxStart + 7
        RT.SelLength = 1
        If RT.SelText <> "*" Then
          entryOk = True
        Else
          i = idx01 + 6
        End If
      Else
        entryKo = True 'IRIS-DB da gestire
        m_fun.WriteLog "EncapsSCHEDEXECDLI - Dot not found after PROCEDURE DIVISION - IdOggetto (" & IdOgg & ")"
        Exit Sub
      End If
    Wend
  End If
    
  idxStop = RT.find(vbCrLf, idxStart)
  RT.SelStart = idxStop + 2
  RT.SelLength = 0
 'IRIS-DB: attenzione che prende solo il primo in tabella in caso di PSB multipli
  Set rsPSB = m_fun.Open_Recordset("Select a.nome as PSBName, b.IdPSB as PsbId from bs_oggetti as a, PsDLI_IstrPSB as b  where " & _
                              "a.idoggetto = b.IdPSB and b.idpgm = " & IdOgg)
  If Not rsPSB.EOF Then
    psbName = rsPSB!psbName
    PsbId = rsPSB!PsbId
    rsPSB.Close
  Else
   'IRIS-DB: faccio una pezza per non cambiare tutto: ci sono programmi che hanno solo la entry per cui non sono presenti nelle istruzioni
    rsPSB.Close
    Set rsPSB = m_fun.Open_Recordset("Select a.nome as PSBName, b.IdOggettoR as PsbId from bs_oggetti as a, PsRel_Obj as b where " & _
                                     "a.idoggetto = b.IdOggettoR and b.idOggettoC = " & IdOgg & " and Relazione = 'PSB'")
    If Not rsPSB.EOF Then
      psbName = rsPSB!psbName
      PsbId = rsPSB!PsbId
    Else
      psbName = "MISSING"
      PsbId = 0
    End If
    rsPSB.Close
  End If
  If Not IsCICS Then
    If Not hasCBLTDLI Then
      RT.SelText = "IRISDB     SET IRIS-FUNC-SCHD TO TRUE" & vbCrLf
      RT.SelText = RT.SelText & "IRISDB     SET IRISUTIL-RTN TO TRUE" & vbCrLf
      RT.SelText = RT.SelText & "IRISDB     MOVE '" & psbName & "' TO IRIS-PSB-NAME-COMM" & vbCrLf
      RT.SelText = RT.SelText & "IRISDB     SET IRIS-EXECDLI TO TRUE" & vbCrLf
      RT.SelText = RT.SelText & "IRISDB     CALL IRIS-WS-RTN USING IRIS-WORK-AREA" & vbCrLf
      RT.SelText = RT.SelText & "IRISDB                            DLZDIB" & vbCrLf
      RT.SelText = RT.SelText & "IRISDB                            IRIS-PSB-NAME-COMM." & vbCrLf
      AddRighe = 7
    Else
      RT.SelText = "IRISDB     SET IRIS-FUNC-DLIT TO TRUE" & vbCrLf _
                 & "IRISDB     SET IRISUTIL-RTN TO TRUE" & vbCrLf _
                 & "IRISDB     MOVE '" & psbName & "' TO IRIS-PSB-NAME" & vbCrLf
      AddRighe = 3
      'IRIS-DB: non chiaro come potesse funzionare prima di usare la string, in caso di parametro vuoto
      ' dynamicSSA = m_fun.LeggiParam("IMSDB-DYNAMICSSA")
      param = m_fun.LeggiParam("IMSDB-DYNAMICSSA")
      If Not Len(param) Then
        dynamicSSA = False
      Else
        dynamicSSA = param
      End If
      
      If Not dynamicSSA Then
        CMPAT_Yes = True
        Set rsPSB = m_fun.Open_Recordset("Select NumPcb, DbdName, TypePcb from PsDLI_PSB  where idOggetto = " & PsbId)
        If rsPSB.EOF Then
         'IRIS-DB only IO PCB present
          RT.SelText = RT.SelText & "IRISDB     SET IRIS-PCB-TYPE-TP(1) TO TRUE" & vbCrLf
        Else
          While Not rsPSB.EOF
            If rsPSB!numpcb = 1 Then
              CMPAT_Yes = False
            End If
            If CMPAT_Yes And rsPSB!numpcb = 2 Then
              CMPAT_Yes = False
              RT.SelText = RT.SelText & "IRISDB     SET IRIS-PCB-TYPE-TP(1) TO TRUE" & vbCrLf
              AddRighe = AddRighe + 1
            End If
            If rsPSB!TypePcb = "TP" Then
              RT.SelText = RT.SelText & "IRISDB     SET IRIS-PCB-TYPE-TP(" & rsPSB!numpcb & ") TO TRUE" & vbCrLf
            Else
              If rsPSB!TypePcb = "GSAM" Then
                RT.SelText = RT.SelText & "IRISDB     SET IRIS-PCB-TYPE-GSAM(" & rsPSB!numpcb & ") TO TRUE" & vbCrLf
                RT.SelText = RT.SelText & "IRISDB     MOVE '" & rsPSB!dbdname & "' TO IRIS-PCB-DBD(" & rsPSB!numpcb & ")" & vbCrLf
              Else
                RT.SelText = RT.SelText & "IRISDB     SET IRIS-PCB-TYPE-DB(" & rsPSB!numpcb & ") TO TRUE" & vbCrLf
              End If
            End If
            AddRighe = AddRighe + 1
            rsPSB.MoveNext
          Wend
        End If
        rsPSB.Close
      End If
      Set rsPgm = m_fun.Open_Recordset("Select parameters from PsPgm where " & _
                                        "idoggetto = " & IdOgg)
      If Not rsPgm.EOF Then
        ArrayPCB = Split(rsPgm!parameters, ",")
        RT.SelText = RT.SelText & "IRISDB     MOVE " & UBound(ArrayPCB) + 1 & " TO IRIS-PARAM-NUM" & vbCrLf
      Else
        m_fun.WriteLog "EncapsSCHEDEXECDLI - rsPgm not found - IdOggetto (" & IdOgg & ")"
        Exit Sub
      End If
      rsPgm.Close
      AddRighe = AddRighe + 1
      RT.SelText = RT.SelText & "IRISDB     SET IRIS-CBLTDLI TO TRUE" & vbCrLf
      AddRighe = AddRighe + 1
      RT.SelText = RT.SelText & "IRISDB     CALL IRIS-WS-RTN USING IRIS-WORK-AREA"
      AddRighe = AddRighe + 1
      i = 0
      While i <= UBound(ArrayPCB)
        RT.SelText = RT.SelText & vbCrLf & "IRISDB                 ADDRESS OF " & ArrayPCB(i)
        AddRighe = AddRighe + 1
        i = i + 1
      Wend
      RT.SelText = RT.SelText & "." & vbCrLf
    End If
  Else
    RT.SelText = "FIXPCB     SET IRISTSQU-RTN TO TRUE" & vbCrLf
    RT.SelText = RT.SelText & "FIXPCB     SET IRIS-FUNC-READQ TO TRUE" & vbCrLf
    RT.SelText = RT.SelText & "FIXPCB     CALL IRIS-WS-RTN USING DFHEIBLK" & vbCrLf
    RT.SelText = RT.SelText & "FIXPCB                            IRIS-WORK-AREA." & vbCrLf
    AddRighe = 4
  End If
  m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = OffsetEncaps + " & AddRighe & " where idoggetto = " & IdOgg ' IRISDB: � all'inizio, per cui vanno sommati tutti (anche il primo con chiave = 0)
End Sub
'IRIS-DB all the function
Public Sub EncapsGOBACK(RT As RichTextBox, IdOgg As Long)
  Dim idx01 As Long, idxStart As Long, idxStop As Long
  Dim i As Long
  Dim rsOgg As Recordset
  Dim rsPgm As Recordset
  Dim namePgm As String
  Dim arrFile() As String, numFile As Integer, Riga As Long
  Dim CurrRiga As String
  
  On Error GoTo errEncapsGOBACK
  
  Set rsOgg = m_fun.Open_Recordset("Select * from bs_oggetti where idoggetto = " & IdOgg)
  If rsOgg.EOF Then
    m_fun.WriteLog "EncapsGOBACK - Not found Bs_oggetti - idoggetto (" & IdOgg & ")"
    rsOgg.Close
    Exit Sub
  End If
  Set rsPgm = m_fun.Open_Recordset("Select * from IRIS_PGM_TSQ where PgmName = '" & rsOgg!nome & "'")
  If rsPgm.EOF Then
    rsOgg.Close
    rsPgm.Close
    Exit Sub
  End If
  namePgm = rsOgg!nome
  rsOgg.Close
  rsPgm.Close
  
  RT.SaveFile namePgm & "appo", 1
  Riga = 1
  ReDim arrFile(Riga)
  numFile = FreeFile
  Open namePgm & "appo" For Input As numFile
  Line Input #numFile, arrFile(Riga)
  AddRighe = 0
  Do Until EOF(numFile)
    Riga = Riga + 1
    ReDim Preserve arrFile(Riga)
    Line Input #numFile, arrFile(Riga)
    If Mid(arrFile(Riga), 7, 1) = " " Then 'IRIS-DB gestione per ora semplificata del commento, potrebbe esserci il trattino di continuazione
      idx01 = InStr(8, arrFile(Riga), " GOBACK ")
      If idx01 = 0 Then idx01 = InStr(8, arrFile(Riga), " GOBACK.")
      If idx01 = 0 Then idx01 = Right(arrFile(Riga), 6) = "GOBACK"
      If idx01 = -1 Then idx01 = 1
      If idx01 > 0 Then
        idxStart = Riga
        CurrRiga = arrFile(Riga)
        arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "SET IRISTSQU-RTN TO TRUE"
        Riga = Riga + 1
        ReDim Preserve arrFile(Riga)
        arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "SET IRIS-FUNC-DELETEQ TO TRUE"
        Riga = Riga + 1
        ReDim Preserve arrFile(Riga)
        arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "CALL IRIS-WS-RTN USING DFHEIBLK"
        Riga = Riga + 1
        ReDim Preserve arrFile(Riga)
        arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "     IRIS-WORK-AREA"
        Riga = Riga + 1
        ReDim Preserve arrFile(Riga)
        arrFile(Riga) = CurrRiga
        AddRighe = AddRighe + 4
      End If
    End If
  Loop
  Close numFile
    
  numFile = FreeFile
  Open namePgm & "appo" For Output As numFile
  For i = 1 To UBound(arrFile)
    Print #numFile, arrFile(i)
  Next i
  Close numFile
  
  RT.LoadFile namePgm & "appo"
  Kill namePgm & "appo"
  m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = OffsetEncaps + " & AddRighe & " where idoggetto = " & IdOgg & " and (Riga + OffsetEncaps + OffsetMacro) > " & idxStart
  Exit Sub

errEncapsGOBACK:
  m_fun.WriteLog "EncapsGOBACK - Unexpected error on - idoggetto (" & IdOgg & ")"
  Close numFile
  Kill namePgm & "appo"
End Sub
'IRIS-DB all the function
Public Sub EncapsRETURN(RT As RichTextBox, IdOgg As Long)
  Dim idx01 As Long, idxStart As Long, idxStop As Long
  Dim i As Long
  Dim idxReturn As Long
  Dim rsOgg As Recordset
  Dim rsPgm As Recordset
  Dim rsExec As Recordset
  Dim rsoffset As Recordset
  Dim namePgm As String
  Dim arrFile() As String, numFile As Integer, Riga As Long
  Dim CurrRiga As String
  Dim arrExec() As Long
  
  On Error GoTo errEncapsRETURN
      
  Set rsOgg = m_fun.Open_Recordset("Select * from bs_oggetti where idoggetto = " & IdOgg)
  If rsOgg.EOF Then
    m_fun.WriteLog "EncapsRETURN - Not found Bs_oggetti - idoggetto (" & IdOgg & ")"
    rsOgg.Close
    Exit Sub
  End If
  Set rsPgm = m_fun.Open_Recordset("Select * from IRIS_PGM_TSQ where PgmName = '" & rsOgg!nome & "'")
  If rsPgm.EOF Then
    rsOgg.Close
    rsPgm.Close
    Exit Sub
  End If
  namePgm = rsOgg!nome
  rsOgg.Close
  rsPgm.Close
  
  i = 0
  ReDim arrExec(i)
  Set rsExec = m_fun.Open_Recordset("Select * from PsExec where idoggetto = " & IdOgg & " and Area = 'CICS' and Comando = 'RETURN' and (trim(Istruzione) = '' or left(ltrim(istruzione),4) = 'RESP')")
  While Not rsExec.EOF
    i = i + 1
    ReDim Preserve arrExec(i)
    arrExec(i) = rsExec!Riga
    rsExec.MoveNext
  Wend
  rsExec.Close
  If i < 1 Then ' no RETURNs in the program
    Exit Sub
  End If
   
  For idxReturn = 1 To UBound(arrExec)
    idxStart = arrExec(idxReturn)
    RT.SaveFile namePgm & "appo", 1
    Riga = 1
    ReDim arrFile(Riga)
    numFile = FreeFile
    Open namePgm & "appo" For Input As numFile
    Line Input #numFile, arrFile(Riga)
    AddRighe = 0
    Do Until EOF(numFile)
      Riga = Riga + 1
      ReDim Preserve arrFile(Riga)
      Line Input #numFile, arrFile(Riga)
      'IRIS-DB: soluzione molto brutta ma necessaria perch� la Ps_objRowOffset contiene solo le istruzioni DLI
      '         per cui non si riesce ad usarla neanche con complicati algoritmi se ci sono API da aggiornare
      '         dopo l'ultima istruzione DLI
      '         Andrebbe aggiunta la lunghezza di ogni istruzione alla tabella, ma diventa troppo lungo
      idx01 = Left(arrFile(Riga), 7) = "IRISDB "
      If idx01 = 0 Then idx01 = Left(arrFile(Riga), 7) = "IRISXX*"
      If idx01 = 0 Then idx01 = Left(arrFile(Riga), 6) = "FIXPCB"
      If idx01 = -1 Then arrExec(idxReturn) = arrExec(idxReturn) + 1
      If Riga = arrExec(idxReturn) Then
        idx01 = InStr(8, arrFile(Riga), " EXEC ")
        If idx01 = 0 Then
          m_fun.WriteLog "EncapsRETURN - Mismatch between PsExec and number of line: " & Riga
        Else
          CurrRiga = arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "SET IRISTSQU-RTN TO TRUE"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "SET IRIS-FUNC-DELETEQ TO TRUE"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "CALL IRIS-WS-RTN USING DFHEIBLK"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "     IRIS-WORK-AREA"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = CurrRiga
          AddRighe = AddRighe + 4
        End If
      End If
    Loop
    Close numFile
      
    numFile = FreeFile
    Open namePgm & "appo" For Output As numFile
    For i = 1 To UBound(arrFile)
      Print #numFile, arrFile(i)
    Next i
    Close numFile
   
    RT.LoadFile namePgm & "appo"
    Kill namePgm & "appo"
    Close numFile
    m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = OffsetEncaps + " & AddRighe & " where idoggetto = " & IdOgg & " and Riga > " & idxStart
  Next idxReturn
  Exit Sub

errEncapsRETURN:
  m_fun.WriteLog "EncapsRETURN - Unexpected error on - idoggetto (" & IdOgg & ")"
  Close numFile
  Kill namePgm & "appo"
End Sub
'IRIS-DB all the function
Public Sub EncapsRETURNTRANSID(RT As RichTextBox, IdOgg As Long)
  Dim idx01 As Long, idxStart As Long, idxStop As Long
  Dim i As Long
  Dim idxReturn As Long
  Dim rsOgg As Recordset
  Dim rsExec As Recordset
  Dim rsoffset As Recordset
  Dim namePgm As String
  Dim arrFile() As String, numFile As Integer, Riga As Long
  Dim CurrRiga As String
  Dim arrExec() As Long
  
  On Error GoTo errEncapsRETURNTRANSID
      
  Set rsOgg = m_fun.Open_Recordset("Select * from bs_oggetti where idoggetto = " & IdOgg)
  If rsOgg.EOF Then
    m_fun.WriteLog "EncapsRETURNTRANSID - Not found Bs_oggetti - idoggetto (" & IdOgg & ")"
    rsOgg.Close
    Exit Sub
  End If
  namePgm = rsOgg!nome
  rsOgg.Close
  
  i = 0
  ReDim arrExec(i)
  Set rsExec = m_fun.Open_Recordset("Select * from PsExec where idoggetto = " & IdOgg & " and Area = 'CICS' and Comando = 'RETURN' and InStr(1, istruzione, 'TRANSID')")
  While Not rsExec.EOF
    i = i + 1
    ReDim Preserve arrExec(i)
    arrExec(i) = rsExec!Riga
    rsExec.MoveNext
  Wend
  rsExec.Close
  If i < 1 Then ' no RETURNs in the program
    Exit Sub
  End If
   
  For idxReturn = 1 To UBound(arrExec)
    idxStart = arrExec(idxReturn)
    RT.SaveFile namePgm & "appo", 1
    Riga = 1
    ReDim arrFile(Riga)
    numFile = FreeFile
    Open namePgm & "appo" For Input As numFile
    Line Input #numFile, arrFile(Riga)
    AddRighe = 0
    Do Until EOF(numFile)
      Riga = Riga + 1
      ReDim Preserve arrFile(Riga)
      Line Input #numFile, arrFile(Riga)
      'IRIS-DB: soluzione molto brutta ma necessaria perch� la Ps_objRowOffset contiene solo le istruzioni DLI
      '         per cui non si riesce ad usarla neanche con complicati algoritmi se ci sono API da aggiornare
      '         dopo l'ultima istruzione DLI
      '         Andrebbe aggiunta la lunghezza di ogni istruzione alla tabella, ma diventa troppo lungo
      idx01 = Left(arrFile(Riga), 7) = "IRISDB "
      If idx01 = 0 Then idx01 = Left(arrFile(Riga), 7) = "IRISXX*"
      If idx01 = 0 Then idx01 = Left(arrFile(Riga), 6) = "FIXPCB"
      If idx01 = -1 Then arrExec(idxReturn) = arrExec(idxReturn) + 1
      If Riga = arrExec(idxReturn) Then
        idx01 = InStr(8, arrFile(Riga), " EXEC ")
        If idx01 = 0 Then
          m_fun.WriteLog "EncapsRETURNTRANSID - Mismatch between PsExec and number of line: " & Riga
        Else
          CurrRiga = arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "SET IRISTSQU-RTN TO TRUE"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "SET IRIS-FUNC-DELETEQ TO TRUE"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "CALL IRIS-WS-RTN USING DFHEIBLK"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "     IRIS-WORK-AREA"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = CurrRiga
          AddRighe = AddRighe + 4
        End If
      End If
    Loop
    Close numFile
      
    numFile = FreeFile
    Open namePgm & "appo" For Output As numFile
    For i = 1 To UBound(arrFile)
      Print #numFile, arrFile(i)
    Next i
    Close numFile
   
    RT.LoadFile namePgm & "appo"
    Kill namePgm & "appo"
    Close numFile
    m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = OffsetEncaps + " & AddRighe & " where idoggetto = " & IdOgg & " and Riga > " & idxStart
  Next idxReturn
  Exit Sub

errEncapsRETURNTRANSID:
  m_fun.WriteLog "EncapsRETURNTRANSID - Unexpected error on - idoggetto (" & IdOgg & ")"
  Close numFile
  Kill namePgm & "appo"
End Sub
'IRIS-DB all the function
Public Sub EncapsXCTL(RT As RichTextBox, IdOgg As Long)
  Dim idx01 As Long, idxStart As Long, idxStop As Long
  Dim i As Long
  Dim idxGoback As Long
  Dim rsOgg As Recordset
  Dim rsPgm As Recordset
  Dim rsExec As Recordset
  Dim rsoffset As Recordset
  Dim namePgm As String
  Dim arrFile() As String, numFile As Integer, Riga As Long
  Dim CurrRiga As String
  Dim arrExec() As Long
  
  On Error GoTo errEncapsXCTL

  Set rsOgg = m_fun.Open_Recordset("Select * from bs_oggetti where idoggetto = " & IdOgg)
  If rsOgg.EOF Then
    m_fun.WriteLog "EncapsXCTL - Not found Bs_oggetti - idoggetto (" & IdOgg & ")"
    rsOgg.Close
    Exit Sub
  End If
  namePgm = rsOgg!nome
  i = 0
  ReDim arrExec(i)
  Set rsExec = m_fun.Open_Recordset("Select * from PsExec where idoggetto = " & IdOgg & " and Area = 'CICS' and Comando = 'XCTL'")
  While Not rsExec.EOF
    i = i + 1
    ReDim Preserve arrExec(i)
    arrExec(i) = rsExec!Riga
    rsExec.MoveNext
  Wend
  rsExec.Close
  If i < 1 Then ' no CICS ABENDs in the program
    Exit Sub
  End If
   
  For idxGoback = 1 To UBound(arrExec)
    idxStart = arrExec(idxGoback)
    RT.SaveFile namePgm & "appo", 1
    Riga = 1
    ReDim arrFile(Riga)
    numFile = FreeFile
    Open namePgm & "appo" For Input As numFile
    Line Input #numFile, arrFile(Riga)
    AddRighe = 0
    Do Until EOF(numFile)
      Riga = Riga + 1
      ReDim Preserve arrFile(Riga)
      Line Input #numFile, arrFile(Riga)
      'IRIS-DB: soluzione molto brutta ma necessaria perch� la Ps_objRowOffset contiene solo le istruzioni DLI
      '         per cui non si riesce ad usarla neanche con complicati algoritmi se ci sono API da aggiornare
      '         dopo l'ultima istruzione DLI
      '         Andrebbe aggiunta la lunghezza di ogni istruzione alla tabella, ma diventa troppo lungo
      idx01 = Left(arrFile(Riga), 7) = "IRISDB "
      If idx01 = 0 Then idx01 = Left(arrFile(Riga), 7) = "IRISXX*"
      If idx01 = 0 Then idx01 = Left(arrFile(Riga), 6) = "FIXPCB"
      If idx01 = -1 Then arrExec(idxGoback) = arrExec(idxGoback) + 1
      If Riga = arrExec(idxGoback) Then
        idx01 = InStr(8, arrFile(Riga), " EXEC ")
        If idx01 = 0 Then
          m_fun.WriteLog "EncapsXCTL - Mismatch between PsExec and number of line: " & Riga
        Else
          CurrRiga = arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "SET IRISTSQU-RTN TO TRUE"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "SET IRIS-FUNC-DELETEQ TO TRUE"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "CALL IRIS-WS-RTN USING DFHEIBLK"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "     IRIS-WORK-AREA"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = CurrRiga
          AddRighe = AddRighe + 4
        End If
      End If
    Loop
    Close numFile
      
    numFile = FreeFile
    Open namePgm & "appo" For Output As numFile
    For i = 1 To UBound(arrFile)
      Print #numFile, arrFile(i)
    Next i
    Close numFile
   
    RT.LoadFile namePgm & "appo"
    Kill namePgm & "appo"
    Close numFile
    m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = OffsetEncaps + " & AddRighe & " where idoggetto = " & IdOgg & " and Riga > " & idxStart
  Next idxGoback
  Exit Sub

errEncapsXCTL:
  m_fun.WriteLog "EncapsXCTL - Unexpected error on - idoggetto (" & IdOgg & ")"
  Close numFile
  Kill namePgm & "appo"
End Sub
'IRIS-DB all the function
Public Sub EncapsABEND(RT As RichTextBox, IdOgg As Long)
  Dim idx01 As Long, idxStart As Long, idxStop As Long
  Dim i As Long
  Dim idxGoback As Long
  Dim rsOgg As Recordset
  Dim rsPgm As Recordset
  Dim rsExec As Recordset
  Dim rsoffset As Recordset
  Dim namePgm As String
  Dim arrFile() As String, numFile As Integer, Riga As Long
  Dim CurrRiga As String
  Dim arrExec() As Long
  
  On Error GoTo errEncapsABEND

  Set rsOgg = m_fun.Open_Recordset("Select * from bs_oggetti where idoggetto = " & IdOgg)
  If rsOgg.EOF Then
    m_fun.WriteLog "EncapsABEND - Not found Bs_oggetti - idoggetto (" & IdOgg & ")"
    rsOgg.Close
    Exit Sub
  End If
  namePgm = rsOgg!nome
  i = 0
  ReDim arrExec(i)
  Set rsExec = m_fun.Open_Recordset("Select * from PsExec where idoggetto = " & IdOgg & " and Area = 'CICS' and Comando = 'ABEND'")
  While Not rsExec.EOF
    i = i + 1
    ReDim Preserve arrExec(i)
    arrExec(i) = rsExec!Riga
    rsExec.MoveNext
  Wend
  rsExec.Close
  If i < 1 Then ' no CICS ABENDs in the program
    Exit Sub
  End If
   
  For idxGoback = 1 To UBound(arrExec)
    idxStart = arrExec(idxGoback)
    RT.SaveFile namePgm & "appo", 1
    Riga = 1
    ReDim arrFile(Riga)
    numFile = FreeFile
    Open namePgm & "appo" For Input As numFile
    Line Input #numFile, arrFile(Riga)
    AddRighe = 0
    Do Until EOF(numFile)
      Riga = Riga + 1
      ReDim Preserve arrFile(Riga)
      Line Input #numFile, arrFile(Riga)
      'IRIS-DB: soluzione molto brutta ma necessaria perch� la Ps_objRowOffset contiene solo le istruzioni DLI
      '         per cui non si riesce ad usarla neanche con complicati algoritmi se ci sono API da aggiornare
      '         dopo l'ultima istruzione DLI
      '         Andrebbe aggiunta la lunghezza di ogni istruzione alla tabella, ma diventa troppo lungo
      idx01 = Left(arrFile(Riga), 7) = "IRISDB "
      If idx01 = 0 Then idx01 = Left(arrFile(Riga), 7) = "IRISXX*"
      If idx01 = 0 Then idx01 = Left(arrFile(Riga), 6) = "FIXPCB"
      If idx01 = -1 Then arrExec(idxGoback) = arrExec(idxGoback) + 1
      If Riga = arrExec(idxGoback) Then
        idx01 = InStr(8, arrFile(Riga), " EXEC ")
        If idx01 = 0 Then
          m_fun.WriteLog "EncapsABEND - Mismatch between PsExec and number of line: " & Riga
        Else
          CurrRiga = arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "SET IRISTSQU-RTN TO TRUE"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "SET IRIS-FUNC-DELETEQ TO TRUE"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "CALL IRIS-WS-RTN USING DFHEIBLK"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = "FIXPCB     " & Space(idx01 - 11) & "     IRIS-WORK-AREA"
          Riga = Riga + 1
          ReDim Preserve arrFile(Riga)
          arrFile(Riga) = CurrRiga
          AddRighe = AddRighe + 4
        End If
      End If
    Loop
    Close numFile
      
    numFile = FreeFile
    Open namePgm & "appo" For Output As numFile
    For i = 1 To UBound(arrFile)
      Print #numFile, arrFile(i)
    Next i
    Close numFile
   
    RT.LoadFile namePgm & "appo"
    Kill namePgm & "appo"
    Close numFile
    m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = OffsetEncaps + " & AddRighe & " where idoggetto = " & IdOgg & " and Riga > " & idxStart
  Next idxGoback
  Exit Sub

errEncapsABEND:
  m_fun.WriteLog "EncapsABEND - Unexpected error on - idoggetto (" & IdOgg & ")"
  Close numFile
  Kill namePgm & "appo"
End Sub
Public Sub SpostaPCB2WorkingStorage(RT As RichTextBox, typeIncaps As String, cPcb As Collection)
  Dim idx01 As Long, idxStart As Long, idxStop As Long
  Dim i As Long
  Dim wLine As String
  Dim wApp As String
  Dim wWord As String
  
  'SP exec dli
  If Not swCall Then Exit Sub
  idx01 = 1
  idxStop = RT.find(vbCrLf, idx01)
  
  Do While idx01 > 0
    RT.SelStart = idx01 + 2
    RT.SelLength = idxStop - idx01 + 2
    wLine = RT.SelText
    If Mid(wLine, 7, 1) <> "*" Then
      If InStr(1, wLine, "WORKING-STORAGE ") > 0 Then
        If InStr(1, wLine, " SECTION") > 0 Then
          'Si sposta alla riga successiva
          idxStop = idxStop + 2
          Exit Do
        End If
      End If
    End If
    idx01 = RT.find(vbCrLf, idx01 + 1)
    idxStop = RT.find(vbCrLf, idx01 + 1)
  Loop
   
  RT.SelStart = idxStop
  RT.SelLength = 0
  
  wLine = ""
  For i = 1 To cPcb.Count
    wLine = wLine & m_fun.LABEL_INFO & "     " & cPcb(i) & vbCrLf
  Next i
  RT.SelText = wLine
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 26-07-06
' Caricare qui (UNA VOLTA PER TUTTE) tutti gli "Environment Parameters" necessari
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub loadEnvironmentParameters()
  'Dim parameterName As String
  IsAlwaysBatch = IIf(UCase(m_fun.LeggiParam("IMSDB_ALWAYS_BATCH")) = "NO" Or UCase(m_fun.LeggiParam("IMSDB_ALWAYS_BATCH")) = "", False, True)
  START_TAG = Left(m_fun.LeggiParam("IMS-INCAPS-START-TAG") & Space(6), 6)
  END_TAG = Left(m_fun.LeggiParam("IMS-INCAPS-END-TAG") & Space(8), 8)
  xCopyParam = m_fun.LeggiParam("X-COPY")
  tCopyParam = m_fun.LeggiParam("T-COPY")
  cCopyParam = m_fun.LeggiParam("C-COPY")
  kCopyParam = m_fun.LeggiParam("K-COPY")
  pCopyParam = m_fun.LeggiParam("P-COPY")
  mCopyParam = m_fun.LeggiParam("MOVE-COPY")
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 26-07-06
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function insertLine(inputText As String, Line As String, pIdOggetto As Long, Optional indent As Integer = 7) As Boolean
  Dim statement As String, extraIndent As Integer, LenbefTrim As Integer, appoIndent As Integer
  Dim startTag As String, lenUtil As Integer
  insertLine = True
  
  On Error GoTo errinsert
  extraIndent = getTotalIndent(Line) ' ripulisce anche dai tag indent
  ' se rimangono tag non risolti non inserisco linea
  If (InStr(1, Line, "<") > 0 And InStr(1, Line, ">") > 0) Or Trim(Line) = "" Then
    insertLine = False
    Exit Function
  End If
  If isPLI Then
    If indent + extraIndent > 0 Then
      statement = Space(indent + extraIndent) & Line
    Else
      LenbefTrim = Len(Line)
      Line = LTrim(Line)
      statement = Space(indent + extraIndent + (LenbefTrim - Len(Line))) & Line
    End If
  Else
    startTag = START_TAG  'Copia: viene modificato
    
    ' startTag formattato a sette caratteri
    If Len(startTag) > 7 Then
      startTag = Left(startTag, 7)
    ElseIf Len(startTag) < 7 Then
      startTag = startTag & Space(7 - Len(startTag))
    End If
    ' tolgo spazi davanti a linea e li sommo all'indentazione
    LenbefTrim = Len(Line)
    Line = LTrim(Line)
    appoIndent = indent + (LenbefTrim - Len(Line))
    'IRIS-DB non capisco cosa cazzo c'entri questo controllo, per cui lo tolgo
'IRIS-DB If Len(Line) > Len(startTag) - extraIndent Then
'IRIS-DB      If Not (appoIndent - Len(startTag) + extraIndent) < 0 Then
        'statement = startTag & Space(indent - 7 + extraindent) & Right(Line, Len(Line) - Len(startTag))
        statement = startTag & Space(appoIndent - Len(startTag) + extraIndent) & Line
'IRIS-DB      Else
        'statement = startTag & Space(indent - 7) & Right(Line, Len(Line) - Len(startTag) + extraindent)
'IRIS-DB      End If
'IRIS-DB    Else
'IRIS-DB      statement = startTag & Space(appoIndent - 7) & Line
'IRIS-DB    End If
  End If
  'stefano: lo ho reinserito, altrimenti considera > 72 anche quelli con spazi in fondo
  statement = RTrim(statement)
  'stefano: sono entrato a gamba tesa, ma non mi � chiaro questo giro...
  If Len(statement) >= 73 Then
    'a capo automatico...
    'stefano: VALE SOLO PER IL COBOL, per ora...
     Dim j, k As Integer
     Dim splitLine As String
     Dim splitLine2 As String
     j = Len(RTrim(statement))
     splitLine = (RTrim(statement))
     k = 72
     splitLine2 = Space(72)
     Do While Mid(splitLine, j, 1) <> " " Or j > 72
      Mid(splitLine2, k, 1) = Mid(splitLine, j, 1)
      Mid(splitLine, j, 1) = " "
      k = k - 1
      j = j - 1
     Loop
     If j > 0 Then
         ' la prima parte di if � pezza per exec PLI
        If Left(Trim(splitLine2), 1) = "'" And InStr(splitLine, "('") > 0 Then
            Dim strParam As String, numSpaces As Integer, oldLen As Integer
            strParam = Trim(Right(splitLine, Len(splitLine) - InStr(splitLine, "('") + 1))
            If Len(strParam) < 10 Then
                strParam = strParam & Space(10 - Len(strParam))
            End If
            splitLine = Left(splitLine, InStr(splitLine, "('") - 1)
            oldLen = Len(splitLine2)
            splitLine2 = Trim(splitLine2)
            If InStr(splitLine, "CALL") > 0 Then
                numSpaces = InStr(splitLine, "CALL") - 1
            Else
                numSpaces = oldLen - Len(splitLine2)
            End If
            splitLine2 = Space(numSpaces) & strParam & splitLine2
            statement = RTrim(splitLine) & Space(72 - Len(RTrim(splitLine))) & END_TAG
            
            inputText = inputText & statement & vbCrLf
            If Len(RTrim(splitLine2)) < 72 Then
                statement = RTrim(splitLine2) & Space(72 - Len(RTrim(splitLine2))) & END_TAG
            Else
                statement = Right(RTrim(splitLine2), 72)
            End If
            inputText = inputText & statement & vbCrLf
        Else
            statement = RTrim(splitLine) & Space(72 - Len(RTrim(splitLine))) & END_TAG
            inputText = inputText & statement & vbCrLf
            'space(20) arbitratrio
            statement = startTag & Space(20) & LTrim(splitLine2) & Space(72 - Len(LTrim(splitLine2))) & END_TAG
            'AC 17/06/08 ritocco space(20) se necessario
            lenUtil = Len(startTag & Space(20) & LTrim(splitLine2))
            If lenUtil > 72 Then
              statement = startTag & Space(20 - (lenUtil - 72)) & LTrim(splitLine2) & Space(72 - Len(LTrim(splitLine2))) & END_TAG
            End If
            inputText = inputText & statement & vbCrLf
        End If
        
     End If
  Else
    If Not isPLI Then
      statement = statement & Space(72 - Len(statement)) & END_TAG
    End If
    inputText = inputText & statement & vbCrLf
  End If
  Exit Function
errinsert:
AggLog "[Object: " & pIdOggetto & "] unexpected error on 'insertLine'.", MadrdF_Incapsulatore.RTErr
End Function

Private Sub insertLineOld(inputText As String, Line As String, Optional indent As Integer = 7)
  Dim statement As String
  'If wLabel = "" Then wLabel = START_TAG
  statement = START_TAG & Space(indent - 7) & Line
  If Len(statement) >= 72 Then
    'a capo automatico...
  Else
    statement = statement & Space(72 - Len(statement)) & END_TAG
    inputText = inputText & statement & vbCrLf
  End If
End Sub

''''''''''''''''''''''''''''''''''
' SQ TMP - non ne potevo pi�...
' Pezzo di Incapsulatore che non c'entra niente con l'incapsulatore!
''''''''''''''''''''''''''''''''''
Public Sub createDecodes(IdOggetto As Long)
  Dim rst As Recordset
  Dim wIstr As String
  Dim idPgmCpy As Long
  'AC da questo array discriminiamo copy da programmi
  Dim arrayPGM() As Long
  Dim rsPgm As Recordset, wResp As Boolean

  On Error GoTo catch
  
  ReDim arayPGM(0)
  ReDim routinePLI(0)
  ReDim arrayPGM(0)
  ReDim LNKDBNumstack(0)
  '''''''''''''''''''''''''''''''''''''''''''''''''
  ' ELIMINAZIONE elementi dalla PsDli_IstrCodifica
  ' !attenzione... rimangono "orfani" nella MgDli_Decodifica!
  '''''''''''''''''''''''''''''''''''''''''''''''''
  m_fun.FnConnection.Execute "DELETE FROM PsDli_IstrCodifica WHERE IdOggetto = " & IdOggetto
  
  'BUTTARE
  'IRIS-DB: non so perch� aveva tolto l'esclusione di quelle IMSDC, invece � giusto escluderle, altrimenti crea decodifiche inutili e non valide. Comunque metterei pi� = imsdb che <> imsdc
''''  Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE " & _
''''                                 "idoggetto=" & IdOggetto & " AND idPgm <> 0 ORDER BY riga") 'And not ImsDC
  Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE " & _
                                 "idoggetto=" & IdOggetto & " AND idPgm <> 0 and ImsDB ORDER BY riga")
  'SQ - e le INCLUDE PLI?!
  If TipoFile = "CPY" And Not rst.EOF Then
    idPgmCpy = rst!idpgm
    rst.Close
  'IRIS-DB: non so perch� aveva tolto l'esclusione di quelle IMSDC, invece � giusto escluderle, altrimenti crea decodifiche inutili e non valide. Comunque metterei pi� = imsdb che <> imsdc
''''    Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE " & _
''''                                   "idoggetto=" & IdOggetto & " AND idPgm = " & idPgmCpy & _
''''                                   " ORDER BY riga") 'And not ImsDC
''''    Set rsPgm = m_fun.Open_Recordset("SELECT distinct IdPGM FROM PSDli_Istruzioni WHERE " & _
''''                                     "idoggetto=" & IdOggetto)  'And not ImsDC
    Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE " & _
                                   "idoggetto=" & IdOggetto & " AND idPgm = " & idPgmCpy & _
                                   " and ImsDB ORDER BY riga")
    Set rsPgm = m_fun.Open_Recordset("SELECT distinct IdPGM FROM PSDli_Istruzioni WHERE " & _
                                     "idoggetto=" & IdOggetto & " and ImsDB")
    While Not rsPgm.EOF
      If Not rsPgm!idpgm = 0 Then
        ReDim Preserve arrayPGM(UBound(arrayPGM) + 1)
        arrayPGM(UBound(arrayPGM)) = rsPgm!idpgm
      End If
      rsPgm.MoveNext
    Wend
    rsPgm.Close
  End If
  
  If rst.RecordCount Then
    MadrdF_Incapsulatore.PbarIstr.Max = rst.RecordCount
  End If
  MadrdF_Incapsulatore.PbarIstr.Value = 0
  
  'pcb dinamico
  ReDim OpeIstrRt(0)
  
  Set distinctCod = New Collection
  Set distinctSeg = New Collection
  Set distinctDB = New Collection

  While Not rst.EOF
    If rst!to_migrate And rst!valida And Not rst!obsolete Then
      'Buttare:
      Select Case TipoFile
        Case "EZT"
        'Case "PLI"
        Case Else
            wResp = CaricaIstruzione(rst)  ' if Then in origine
            wIstr = IIf(isUnificaGH, Trim(rst!Istruzione & ""), Replace(Trim(rst!Istruzione & ""), "GH", "G"))
            If wIstr = "QUERY" Then wIstr = "QRY"
            createDecode wIstr, 1, rst!idpgm, IdOggetto, rst!Riga, IIf((0 & rst!ordPcb_MG) > 0, 0 & rst!ordPcb_MG, 0 & rst!ordPcb)
          'End If  'CaricaIstruzione
        End Select
    End If
    MadrdF_Incapsulatore.PbarIstr.Value = MadrdF_Incapsulatore.PbarIstr.Value + 1
    rst.MoveNext
  Wend
  rst.Close
  Exit Sub
catch:
  'tmp
  MsgBox "TMP: object " & IdOggetto & "-" & err.Description
  'Resume
End Sub

Function getCCodefromDecCCode(decCCode As String, idpgm As Long, IdOggetto As Long, Riga As Long, livello As Long) As String 'IRIS-DB aggiunto il livello!!!!
  Dim tb As Recordset
  Dim ccode As String, ccelement As String, condizione As String
  
  Set tb = m_fun.Open_Recordset("SELECT condizione from PsDli_IstrDett_Liv where idpgm = " & idpgm & " and idoggetto=" & IdOggetto & " and riga = " & Riga & " and livello = " & livello)
  If Not tb.EOF Then
    condizione = tb!condizione
  End If
  'AC 02/09/09 attenzione (per incapsulamento da decodifiche)
  If condizione = "" Then
    getCCodefromDecCCode = decCCode
    Exit Function
  End If
  Do While Len(condizione)
    If InStr(condizione, ";") Then
      ccelement = Left(condizione, InStr(condizione, ";") - 1)
      condizione = Right(condizione, Len(condizione) - InStr(condizione, ";"))
    Else
      ccelement = condizione
      condizione = ""
    End If
    If Replace(Replace(ccelement, "*", ""), "-", "") = decCCode Then
      getCCodefromDecCCode = ccelement
      Exit Do
    End If
  Loop
End Function
Function RoutineFromCheckPcb_Psb(listDBD() As t_Ogg, ordPcb As Long, listpsb() As t_Ogg, istr As String, SwTp As Boolean, db As String) As String
Dim i As Integer, j As Integer, tb As Recordset, IsFound As Boolean
  For i = 1 To UBound(listpsb)
    If Not IsFound Then
      For j = 1 To UBound(listDBD)
        Set tb = m_fun.Open_Recordset("select idOggetto from PsDLI_Psb where IdOggetto =" & listpsb(i).Id & " and NumPcb = " & ordPcb & " and DBdName = '" & listDBD(j).nome & "'")
        If Not tb.EOF Then
          IsFound = True
          RoutineFromCheckPcb_Psb = m_fun.Genera_Nome_Routine(listDBD(j).nome, istr, SwTp)
          db = listDBD(j).nome
          Exit For
        End If
      Next j
    End If
  Next i
  'AC 03/09/10 in fase di parser salvati solo i pcb coerenti (collegati cio� ad un db che contiene il segmento)
  ' se nome routine vuoto vuol dire che dopo il parser � stato tolto qualcosa (psb o dbd dal corrector)

End Function


''''''''''''''''''''''''''''''''''''''''''''''
' SQ 26-10-07
' smembratura "CostruisciOperazioneTemplate":
' Una funzione non pu� fare 18 cose diverse!
''''''''''''''''''''''''''''''''''''''''''''''
Function createDecode(xIstr As String, Index As Integer, idpgm As Long, pIdOggetto As Long, pRiga As Long, ordPcb As Long)
  Dim tb As Recordset, tb1 As Recordset
  Dim wOpe As String, wOpeS As String, TipIstr As String
  Dim k As Long
  Dim xope As String
  Dim K1 As Integer
  Dim wUtil As Boolean, wFlagBoolean As Boolean
  Dim i As Integer, j As Integer, jj As Integer
  Dim checkColl As Boolean
  Dim extraIndent As Integer
  Dim maxId As Long
  Dim wIstr, wDecod As String, wIstrS As String, wDecodS As String, pcbDyn As String
  Dim maxPcbDyn As Integer, pcbDynInt As Integer
  Dim wOpeSave As String
  Dim y As Integer
  ' per comporre stringa if dell'incapsulato, versione qual e squal
  Dim IstrIncaps As String, IstrSIncaps As String, StrAnd As String
  ' contiene la molteplicit� di istruzioni, attinge dalla tabella Istruzioni_Clone
  Dim cloniIstruzione() As String, cloniSegmenti() As String, cloniPCB() As String
  Dim combinazioni As Boolean
  Dim maxLogicOp As Integer, maxBoolean As Integer
  Dim Operatore As String
  Dim StringToClear As String
  Dim ifAnd As String
  Dim nomeRoutCompleto As String
  Dim templateName As String  'SQ - per la gestione degli errori
  Dim dbdname As String
  Dim isExec As Boolean 'IRIS-DB
  
  On Error GoTo ErrH
  'AC 02/11/06
  
  Dim varOp As String, combOp As String, varIstr As String, combIstr As String
  Dim varop_s As String, combop_s As String, ks As Integer
  Dim varCmd As String, combCmd As String, varSeg As String, combSeg As String
  Dim varcmd_s As String, combcmd_s As String
  
  Dim CombPLevel() As String, CombPLevel_s() As String, CombSqual() As String, CombQual() As String, wIstrsOld As String
  
  ' nuove varibili operatore
  Dim CombPLevelOper() As String, ComboPLevelTempOper As String, ComboPLevelAppoOper As String
  Dim bolMoltOpLogic As Boolean, jz As Integer
  
  Dim ComboPLevelTemp As String, ComboPLevelAppo As String, nFBms As Integer, wRecIn As String, jk As Integer
  Dim appocomb As String, appoFinalString As String, checksqual As Boolean, posEndLine As String
  ' tango traccia di una qualsiasi molteplicit� sui livelli
  Dim moltLevel As Boolean, moltLevelesimo() As Boolean, ij As Integer, moltLevThen As Boolean
  
  'AC 19/06/07  arriva la booleana
  Dim OperatoreLogico As String, ultimoOperatore As Boolean, varLogicop As String, combLogicop As String
  Dim OperatoreLogicoSimb As String, moltBOL() As Boolean, bolmoltBOL As Boolean
  Dim bolMultipleCcod As Boolean
  Dim p As Integer ' contatore molt pcb
  Dim pordPcb As Long
  
  For k = 1 To UBound(TabIstr.BlkIstr)
    If UBound(TabIstr.BlkIstr(k).KeyDef) > maxLogicOp Then
      maxLogicOp = UBound(TabIstr.BlkIstr(k).KeyDef)
    End If
  Next k
  
  ReDim MatriceCombinazioni(UBound(TabIstr.BlkIstr), maxLogicOp)
  If maxLogicOp > 0 Then
    ReDim MatriceCombinazioniBOL(UBound(TabIstr.BlkIstr), maxLogicOp - 1)
  End If
  
  ' sequenza command code che deve essere considerate
  ReDim CombinazioniComCode(UBound(TabIstr.BlkIstr))
  ReDim Combinazionisegmenti(UBound(TabIstr.BlkIstr))
  ReDim moltLevelesimo(UBound(TabIstr.BlkIstr))
  For k = 1 To UBound(TabIstr.BlkIstr)
    For K1 = 1 To maxLogicOp
      MatriceCombinazioni(k, K1) = 1
      If Not K1 = maxLogicOp Then
        MatriceCombinazioniBOL(k, K1) = 1
      End If
    Next K1
  Next k
  For k = 1 To UBound(TabIstr.BlkIstr)
    CombinazioniComCode(k) = 1
    Combinazionisegmenti(k) = 1
  Next k
  CreaArrayCloniIstruzione cloniIstruzione, pIdOggetto, pRiga
  CreaArrayCloniPCB cloniPCB, pIdOggetto, pRiga ' <moltpcb>
  CreaArrayCloniSegmenti cloniSegmenti, TabIstr
  
  '***** determiniamo se deve essere generata
  '***** sia la versione qualificata che squalificata
  Set tb = m_fun.Open_Recordset("select * from PsDLI_IstrDett_Liv where IdOggetto =" & pIdOggetto & " and Riga = " & pRiga & " and Livello = " & UBound(TabIstr.BlkIstr))
  If Not tb.EOF Then
    QualAndUnqual = tb!QualAndUnqual
  Else
    QualAndUnqual = False
  End If
  tb.Close
  'IRIS-DB: extract the DLI API type
  Set tb = m_fun.Open_Recordset("select * from PsDLI_Istruzioni where IdOggetto =" & pIdOggetto & " and Riga = " & pRiga)
  If Not tb.EOF Then
    isExec = tb!isExec
  Else
    isExec = False
  End If
  tb.Close
   
  Set wMoltIstr(0) = New Collection
  Set wMoltIstr(1) = New Collection
  Set wMoltIstr(2) = New Collection
  Set wMoltIstr(3) = New Collection
  StringToClear = ""
  ReDim wNomeRout(0)
  
  ' Ciclo esterno su istruzione
  ReDim Preserve CombSqual(0)
  For i = 0 To UBound(cloniIstruzione)
    xIstr = IIf(isUnificaGH, cloniIstruzione(i), Replace(cloniIstruzione(i), "GH", "G"))
    TipIstr = Left(xIstr, 2)
    If TabIstr.isGsam Then
      TipIstr = "GS"
    ElseIf xIstr = "GU" Or xIstr = "GHU" Then
      TipIstr = "GU"
    ElseIf xIstr = "POS" Then 'IRIS-DB
      TipIstr = "PS" 'IRIS-DB
    ElseIf xIstr = "ISRT" Then
      TipIstr = "UP"
    ElseIf xIstr = "DLET" Then
      TipIstr = "UP"
    ElseIf xIstr = "REPL" Then
      TipIstr = "UP"
    ElseIf xIstr = "GNP" Or xIstr = "GHNP" Then
      TipIstr = "GP"
    ElseIf xIstr = "GN" Or xIstr = "GHN" Then
      TipIstr = "GN"
    ElseIf xIstr = "PCB" Then
      wUtil = True
      TipIstr = "SH"
    ElseIf xIstr = "TERM" Then
      wUtil = True
      TipIstr = "TM"
    ElseIf xIstr = "CHKP" Then
      wUtil = True
      TipIstr = "CP"
    ElseIf xIstr = "QRY" Then
      wUtil = True
      TipIstr = "QY"
    ElseIf xIstr = "SYNC" Or xIstr = "SYMCHKP" Then
      wUtil = True
      TipIstr = "SY"
    ElseIf xIstr = "XRST" Then
      wUtil = True
      TipIstr = "XR"
    ElseIf xIstr = "SCHED" Then
      wUtil = True
      TipIstr = "SC"
    Else
      'm_fun.WriteLog "Incapsulatore - Costruisci Operazione " & vbCrLf & "Istruzione non riconosciuta " & xIstr, "DR - Incapsulatore"
    End If
  
    bolMultipleCcod = CheckCombCCode(xIstr)
    gbIstr = xIstr
    For p = 0 To UBound(cloniPCB)
      pordPcb = CInt(cloniPCB(p))
      For k = 1 To UBound(TabIstr.BlkIstr)
        'IRIS-DB: da capire perch� reinizializza quanto appena fatto da CheckCombCcode;
        '         in ogni caso la molteplicit� sul command code non viene gestita correttamente
        '         (non considera il non-command-code come una molteplicit� possibile, mentre lo � in caso di alternativa con un command code,
        '          ad esempio: "*-;*L" vuol dire alternativa tra il non-command-code e la Last
        CombinazioniComCode(k) = 1
      Next k
      
      'SQ nomerout � nella BlkIstr?!!!!!!!!!!!!!!! e l'incapsulatore prende quello dell'ultimo livello!?
      'riapplico il calcolo del NomeRout per ogni Molteplicit� di istruzione
      If UBound(TabIstr.DBD) Then
        dbdname = TabIstr.DBD(1).nome
        If InStr(Dli2Rdbms.drNomeDB, "prj-GRZ") Then
          If InStr(TabIstr.DBD(1).nome, "RKKUNL") Then
            nomeRoutine = m_fun.Genera_Nome_Routine("RKLUNL", xIstr, SwTp)
          ElseIf InStr(TabIstr.DBD(1).nome, "RKGIRL") Then
            nomeRoutine = m_fun.Genera_Nome_Routine("RKOIRL", xIstr, SwTp)
          Else
            If UBound(TabIstr.DBD) > 1 And UBound(cloniPCB) Then
              nomeRoutine = RoutineFromCheckPcb_Psb(TabIstr.DBD, pordPcb, TabIstr.psb, xIstr, SwTp, dbdname)
            Else
              nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, xIstr, SwTp)
            End If
          End If
        Else
          If UBound(TabIstr.DBD) > 1 And UBound(cloniPCB) Then
            nomeRoutine = RoutineFromCheckPcb_Psb(TabIstr.DBD, pordPcb, TabIstr.psb, xIstr, SwTp, dbdname)
          Else
            nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(1).nome, xIstr, SwTp)
          End If
          'aggiornalistaDB (TabIstr.Dbd(1).nome)
        End If
      Else
        nomeRoutine = m_fun.Genera_Nome_Routine("", xIstr, SwTp)
      End If
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      'stefano: ho dovuta commentarla perch� creava delle istruzioni non valide (AND senza IF) quando
      ' c'� molteplicit� di squalificata insieme alla molteplicit� di funzione; fondamentalmente la molteplicit�
      ' di funzione viene gestita sotto quella di squalificata; dovrebbe essere invertita o creato un vettore per ogni
      ' occorrenza di funzione.
      'If TipIstr <> "GU" And TipIstr <> "GN" And TipIstr <> "GP" Then QualAndUnqual = False
      
      combinazioni = True
      IstrIncaps = ""
      Dcombination = False
      
      While combinazioni
        multiOpGen = False
        'AC 02/11/06
        ReDim CombPLevel(UBound(TabIstr.BlkIstr))
        ' alberto, sulle istruzioni OPEN e CLSE dava errore per cui l'ho cambiata..
        If UBound(TabIstr.BlkIstr) > 0 Then
          ReDim CombPLevel_s(UBound(TabIstr.BlkIstr) - 1)
        End If
        IstrIncaps = ""
        If UBound(cloniIstruzione) > 0 Then
          varIstr = Restituisci_varFunz(pIdOggetto, pRiga)
          combIstr = "'" & xIstr & "'"
        End If
        
        If isOldIncapsMethod Then
          wOpe = "<pgm>" & nomeRoutine & "</pgm><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
        Else
          'SQ 9-04-08 PEZZA... GESTIRE
          If UBound(TabIstr.DBD) Then
            wOpe = "<dbd>" & dbdname & "</dbd><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
          Else
            'TERM,SCHED,ECC...
            wOpe = "<istr>" & Left(Trim(xIstr) & "....", 4) & "</istr><livelli>"
          End If
        End If
         
        'AC 24/10/08 e 09/11/10
        'SENSEG (no per le scritture)
        If UBound(cloniPCB) > 0 And Not (xIstr = "REPL" Or xIstr = "DLET" Or xIstr = "ISRT") Then ' se molt pcb gestiamo senseg qui
          For jz = 1 To UBound(TabIstr.DBD)
            If TabIstr.DBD(jz).nome = dbdname Then
              Exit For
            End If
          Next
          TabIstr.SenSeg = GetSenSeg(TabIstr.psb(1).Id, 0 & pordPcb, TabIstr.DBD(jz).Id)
        End If
        
        If Not TabIstr.SenSeg = "" Then
          If TabIstr.SenSeg = "ALL" Then
            wOpe = wOpe & "<sseg></sseg>"
          Else
            wOpe = wOpe & "<sseg>" & TabIstr.SenSeg & "</sseg>"
          End If
        End If
       
        For k = 1 To UBound(TabIstr.BlkIstr)   ' ciclo su livelli
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' SQ 28-07-06
          ' OK: REPL CON SSA QUALIFICATA DA RETURN CODE "AJ"
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' SQ 19-07-07
          ' REPL CON PATH-CALL (C.C. "D")!
          'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then
            If TabIstr.BlkIstr(k).idsegm = -1 Then
              wOpe = wOpe & "<level" & k & "><segm>" & "DYNAMIC" & "</segm>"
            Else
              wOpe = wOpe & "<level" & k & "><segm>" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "</segm>"
              'IRIS-DB pezza a colori per situazione non supportata (multisegmenti su diversi DBD)
              If Combinazionisegmenti(k) > 1 And UBound(TabIstr.DBD) >= Combinazionisegmenti(k) Then 'IRIS-DB
                nomeRoutine = m_fun.Genera_Nome_Routine(TabIstr.DBD(Combinazionisegmenti(k)).nome, xIstr, SwTp) 'IRIS-DB
                wOpe = Replace(wOpe, dbdname, TabIstr.DBD(Combinazionisegmenti(k)).nome) 'IRIS-DB
                dbdname = TabIstr.DBD(Combinazionisegmenti(k)).nome 'IRIS-DB
              End If 'IRIS-DB
              If NumToken(cloniSegmenti(k)) > 1 Then
                'varseg = TabIstr.BlkIstr(K).ssaName & "(1:8)"
                combSeg = "'" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "'"
                varSeg = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, "1", CStr(Len(combSeg) - 2), TipoFile, "SSA", pIdOggetto) '"8"
                moltLevelesimo(k) = True
              Else
                varSeg = ""
              End If
              aggiornalistaSeg (TabIstr.BlkIstr(k).segment)
            End If
          Else
            If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(TabIstr.BlkIstr) Then
              If TabIstr.BlkIstr(k).idsegm = -1 Then
                wOpe = wOpe & "<level1><segm>" & "DYNAMIC" & "</segm>"
              Else
                wOpe = wOpe & "<level1><segm>" & TokenIesimo(cloniSegmenti(k), Combinazionisegmenti(k)) & "</segm>"
                aggiornalistaSeg (TabIstr.BlkIstr(k).segment)
              End If
            End If
          End If
          
          'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "POS" Then
            If TabIstr.BlkIstr(k).Record <> "" Then
              If Trim(TabIstr.BlkIstr(k).segment) <> "" Then
                wOpe = wOpe & "<area>" & "AREA" & "</area>"
              End If
            End If
          End If
          
          'AC 10/04/2007 spostata per non sommare a wopes la <ccode> del wope
          'IRIS-DB manca la gestione di squalificate intermedie
          If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
            wOpeS = wOpe   ' allineiamo  la wOpeS  alla wOpe che si differenziano solo per ultimo livello
          End If
                              
          'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then
            If Trim(Replace(Replace(TokenIesimoCC(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod), "*", ""), "-", "")) <> "" Then
              If Trim(TabIstr.BlkIstr(k).segment) <> "" Then
                'IRIS-DB: when EXEC DLI and ccode "C" it changes the decod resolving the concatenation
                If InStr(TabIstr.BlkIstr(k).Search, "C") > 0 And isExec Then
                   
                End If
                wOpe = wOpe & "<ccode>" & Trim(Replace(Replace(TokenIesimoCC(TabIstr.BlkIstr(k).Search, CombinazioniComCode(k), bolMultipleCcod), "*", ""), "-", "")) & "</ccode>"
                'IRIS-DB anche il command code deve essere incluso nella gestione della squalificata: il command code viene prima di essa potenzialmente...
                'IRIS-DB comunque da verificare bene perch� pu� creare problemi, ad esempio per ricoperture di un command code con spaces: in questo caso il command code non andrebbe trattato
                'IRIS-DB ma comunque l'unico problema dovrebbe essere quello di creare una decodifica in pi� inutile
                If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then 'IRIS-DB
                  wOpeS = wOpe   ' IRIS-DB
                End If 'IRIS-DB
              End If
            End If
          End If
                        
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' QUALIFICAZIONE:
          ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' AC se entrambe le versione wOpe � quella qualificata e wOpeS quella
          ' squalificata, altrimenti se unica sempre in wOpe
          'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then   ' AC 230707 (xIstr = "ISRT" And K < UBound(TabIstr.BlkIstr)
            If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
            'parte SQUALIFICATA
            ' la wOpeS si ferma qui, wOpeS e wOpe differiscono solo per questo livello
             
            'parte QUALIFICATA
            wFlagBoolean = False
            ' determino se su livelli precedenti ho molteplicit� operatore
            If k > 1 Then
              For ij = 1 To k - 1
                If moltLevelesimo(ij) Then
                  moltLevel = True
                  Exit For
                End If
              Next ij
            End If
            
            ' ------------CICLO SU OPERATORI LOGICI---------------
            bolMoltOpLogic = False
            bolmoltBOL = False
            ultimoOperatore = False
            ReDim MoltOpLogic(UBound(TabIstr.BlkIstr(k).KeyDef))
            If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
              ReDim moltBOL(UBound(TabIstr.BlkIstr(k).KeyDef) - 1)
            End If
            ReDim CombPLevelOper(UBound(TabIstr.BlkIstr(k).KeyDef))
            For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
              If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                wOpe = wOpe & "<key>" & TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                Operatore = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                  OperatoreLogicoSimb = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, MatriceCombinazioniBOL(k, K1))
                  OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                  If multiOp Then
                    varLogicop = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                    combLogicop = "'" & OperatoreLogicoSimb & "'"
                    moltLevelesimo(k) = True
                    moltBOL(K1) = True
                  Else
                    varLogicop = ""
                  End If
                  If Not OperatoreLogico = ")" Then
                    wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                    wFlagBoolean = True
                  Else
                    ultimoOperatore = True
                  End If
                Else
                  varLogicop = ""
                End If
                'AC 03/07/06
                'ultimo
                 
                bolMoltOpLogic = False
                bolmoltBOL = False
                If K1 > 1 Then
                  For ij = 1 To K1 - 1
                    If MoltOpLogic(ij) Then
                      bolMoltOpLogic = True
                      Exit For
                    End If
                  Next ij
                  For ij = 1 To K1 - 1
                    If moltBOL(ij) Then
                      bolmoltBOL = True
                      Exit For
                    End If
                  Next ij
                End If
              End If
              ' nel caso di parentesi sull'operatore logico interrompo il ciclo
              If ultimoOperatore Then Exit For
              Next K1
            Else  ' o la versione squalificata o quella squalificata
              ' AC il campo parentesi e la trasformazione in stringa del booleano Qualificazione
              If TabIstr.BlkIstr(k).Parentesi <> "(" Then ' Or Trim(TabIstr.BlkIstr(k).SsaName) = "<none>" Then
                OnlySqual = True
                ReDim CombPLevelOper(0)
              Else   ' Qualificata
                OnlyQual = True
                wFlagBoolean = False
                If k > 1 Then
                  For ij = 1 To k - 1
                    If moltLevelesimo(ij) Then
                      moltLevel = True
                      Exit For
                    End If
                  Next ij
                End If
                ' ----- CICLO OPERATORI LOGICI
                bolmoltBOL = False
                ultimoOperatore = False
                bolMoltOpLogic = False
                ReDim MoltOpLogic(UBound(TabIstr.BlkIstr(k).KeyDef))
                If UBound(TabIstr.BlkIstr(k).KeyDef) > 0 Then
                  ReDim moltBOL(UBound(TabIstr.BlkIstr(k).KeyDef) - 1)
                End If
                ReDim CombPLevelOper(UBound(TabIstr.BlkIstr(k).KeyDef))
                For K1 = 1 To UBound(TabIstr.BlkIstr(k).KeyDef)
                  ComboPLevelAppoOper = ComboPLevelTempOper
                  If Len(TabIstr.BlkIstr(k).KeyDef(K1).key) Then
                    wOpe = wOpe & "<key>" & TabIstr.BlkIstr(k).KeyDef(K1).key & "</key>"
                    Operatore = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).op, MatriceCombinazioni(k, K1))
                    wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                    If multiOp Then  ' AC 020807 And Not TipIstr = "UP"
                      'varop = TabIstr.BlkIstr(K).ssaName & "(" & Int(TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
                      varOp = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) - 2), "2", TipoFile, "SSA", pIdOggetto)
                      ' Mauro 18/02/2008: ripeto l'oggetto della If su tutti gli operatori
                      If TipoFile = "PLI" Then
                        combOp = "IRIS_" & Operatore & "1 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                        & "   " & varOp & " = IRIS_" & Operatore & "2 " & buildLogicalOpFromType("OR", TipoFile) & vbCrLf _
                        & "   " & varOp & " = IRIS_" & Operatore & "3"
                      Else
                        combOp = "REV" & Operatore & "1" & vbCrLf _
                        & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "2" & vbCrLf _
                        & "    " & Space(Len(TabIstr.BlkIstr(k).ssaName)) & "     " & buildLogicalOpFromType("OR", TipoFile) & " REV" & Operatore & "3"
                      End If
                      moltLevelesimo(k) = True
                      MoltOpLogic(K1) = True
                    Else
                      varOp = ""
                    End If
                    If Len(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic) Then
                      OperatoreLogicoSimb = TokenIesimo(TabIstr.BlkIstr(k).KeyDef(K1).OpLogic, MatriceCombinazioniBOL(k, K1))
                      OperatoreLogico = transcode_booleano(OperatoreLogicoSimb)
                      If multiOp Then    ' AC 020807 And Not TipIstr = "UP"
                        varLogicop = buildSubstringFromType(TabIstr.BlkIstr(k).ssaName, CStr(Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyStart) + Int(TabIstr.BlkIstr(k).KeyDef(K1).KeyLen)), "1", TipoFile, "SSA", pIdOggetto)
                        combLogicop = "'" & OperatoreLogicoSimb & "'"
                        moltLevelesimo(k) = True
                        moltBOL(K1) = True
                      Else
                        varLogicop = ""
                      End If
                      If Not OperatoreLogico = ")" Then
                        wOpe = wOpe & "<bool>" & OperatoreLogico & "</bool>"
                        wFlagBoolean = True
                      Else
                        ultimoOperatore = True
                      End If
                    Else
                      varLogicop = ""
                    End If
                  
                    bolMoltOpLogic = False
                    bolmoltBOL = False
                    If K1 > 1 Then
                      For ij = 1 To K1 - 1
                        If MoltOpLogic(ij) Then
                          bolMoltOpLogic = True
                          Exit For
                        End If
                      Next ij
                      For ij = 1 To K1 - 1
                        If moltBOL(ij) Then
                          bolmoltBOL = True
                          Exit For
                        End If
                      Next ij
                    End If
                  End If
                  If ultimoOperatore Then Exit For
                Next K1
              End If '-- squal o qual
            End If   '-- una sola versione o due versioni
          End If
          'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
          ' SQ 19-07-07
          ' REPL CON PATH-CALL (C.C. "D")!
          'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "REPL" Or xIstr = "POS" Then
            wOpe = wOpe & "</level" & k & ">"
            If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
              wOpeS = wOpeS & "</level" & k & ">"
            End If
          Else
            If (xIstr = "REPL" Or xIstr = "DLET") And k = UBound(TabIstr.BlkIstr) Then
              wOpe = wOpe & "</level1>"
              If QualAndUnqual And k = UBound(TabIstr.BlkIstr) Then
                wOpeS = wOpeS & "</level1>"
              End If
            End If
          End If
        Next k   ' FINE CICLO LIVELLI
       
        wOpe = wOpe & "</livelli>"
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then
          If Trim(TabIstr.keyfeedback) <> "" Then
            wOpe = wOpe & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
          End If
        End If
       
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then
          If Len(TabIstr.procSeq) Then
            wOpe = wOpe & "<procseq>" & TabIstr.procSeq & "</procseq>"
          End If
        End If
  
        If xIstr = "OPEN" Then
          If Len(TabIstr.procOpt) Then
            wOpe = wOpe & "<procopt>" & TabIstr.procOpt & "</procopt>"
          End If
        End If
       
         Dim PcbSeqVacante As Integer, indicevacante As Integer
    'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
    ' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpe
      If ordPcb = 0 Then
        pcbDyn = ""
        maxPcbDyn = 0
     '________________________________________________________________
     Else
        PcbSeqVacante = 0
        For k = 1 To UBound(OpeIstrRt)
           If wOpe = OpeIstrRt(k).Decodifica Then
              If OpeIstrRt(k).ordPcb = ordPcb Then
                 'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
                 'verifico se � la prima
                 pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
                 pcbDynInt = OpeIstrRt(k).pcbDyn
                 maxPcbDyn = 0
                 Exit For
               '___________________________
               ' nuovo ramo: se trovo un pcbseq occupato da istruzione con pcb = 0 lo registro
               ' come possibile pcbseq utile nel caso non si trovi un corrispondente
               ' per questo pcb
               
               ElseIf OpeIstrRt(k).ordPcb = 0 Then
                  'AC 28/05/10
                  ' prendiano questo pcbDyn, posto libero se non si trova corrispondente
                  PcbSeqVacante = OpeIstrRt(k).pcbDyn
                  indicevacante = k
               '___________________________
              Else
                 If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
                 'trovata un'istruzione identica, ma con diverso pcb
                    maxPcbDyn = OpeIstrRt(k).pcbDyn
                 End If
              End If
           End If
        Next k
        'AC nel caso di fine ciclo senza un corrispondente
        'se c'era un pcbseq assegnato al pcb = 0 me ne approprio
        If k = UBound(OpeIstrRt) + 1 And PcbSeqVacante > 0 Then
            maxPcbDyn = 0
            pcbDyn = Format(PcbSeqVacante, "000")
            OpeIstrRt(indicevacante).ordPcb = ordPcb
        End If
     End If
        'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
        If maxPcbDyn > 0 Then
          pcbDyn = Format(maxPcbDyn + 1, "000")
          pcbDynInt = maxPcbDyn + 1
        End If
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then
          ' aggiunge il pcb dinamico
          If Len(pcbDyn) Then
            wOpe = wOpe & "<pcbseq>" & pcbDyn & "</pcbseq>"
            'AggLog "[Object: " & pIdOggetto & "] New Instruction: <pcbseq>" & pcbDyn & "</pcbseq>", MadrdF_Incapsulatore.RTErr
          Else
            ' se � la prima istruzione di quel tipo
            wOpe = wOpe & "<pcbseq>" & "001" & "</pcbseq>"
            pcbDynInt = 1
          End If
        End If
      
        '''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Repository:
        ' se nuova decodifica ==> INSERIMENTO in mgdli_decodificaistr
        ' => INSERIMENTO in PsDli_IstrCodifica
        '''''''''''''''''''''''''''''''''''''''''''''''''''
        Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpe & "'")
        If tb.EOF Then
          Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
          'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
          If tb1.EOF Then
            maxId = 1
          Else
            maxId = tb1(0) + 1
          End If
          tb.AddNew
          'SQ debug
          'm_fun.WriteLog "##" & wOpe
          tb!IdIstruzione = maxId
          tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
          wIstr = tb!Codifica
          tb!Decodifica = wOpe
          tb!ImsDB = True
          tb!ImsDC = False
          tb!CICS = SwTp
          'stefano: routine multiple
          nomeRoutCompleto = Genera_Counter(nomeRoutine)
          tb!nomeRout = nomeRoutCompleto
          tb.Update
  ''        'pcb dinamico
  ''        k = UBound(OpeIstrRt) + 1
  ''        ReDim Preserve OpeIstrRt(k)
  ''        OpeIstrRt(k).Decodifica = wOpeSave
  ''        OpeIstrRt(k).Istruzione = wIstr
  ''        OpeIstrRt(k).ordPcb = ordPcb
  ''        OpeIstrRt(k).pcbDyn = pcbDynInt
          'AC  teniamo traccia del clone pcb nella tabella IStrCodifica (quella delle variazioni)
          
        Else
          wIstr = tb!Codifica
          nomeRoutCompleto = tb!nomeRout
          ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
          nomeRoutine = tb!nomeRout
        End If
        tb.Close
       
        'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
        Set tb = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrCodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
        If tb.EOF Then
          m_fun.FnConnection.Execute "INSERT INTO PsDLI_IstrCodifica VALUES(" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeRoutCompleto) & "')"
          ' Mauro 28/04/2009 : pcb dinamico
          k = UBound(OpeIstrRt) + 1
          ReDim Preserve OpeIstrRt(k)
          OpeIstrRt(k).Decodifica = wOpeSave
          OpeIstrRt(k).Istruzione = wIstr
          OpeIstrRt(k).ordPcb = pordPcb
          OpeIstrRt(k).pcbDyn = pcbDynInt
        End If
        tb.Close
        If p > 0 Then
          Set tb = m_fun.Open_Recordset("SELECT * FROM MgDLI_IstrCodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and Decodifica = '" & wOpe & "'")
          If tb.EOF Then
            m_fun.FnConnection.Execute "INSERT INTO MgDLI_IstrCodifica (IdPgm,IdOggetto,Riga,Decodifica,Pcb) " & _
                                      "VALUES(" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & _
                                      wOpe & "'," & pordPcb & ")"
          End If
         End If
        
        wDecod = wOpe
      
        '''''''''''''''''''''''''''
        ' Versione Squalificata:
        ' SQ non ho guardato per niente ma...
        ' ho gi� la decodifica intera: non basta usare solo il pezzo...
        '''''''''''''''''''''''''''
        If QualAndUnqual Then
          wOpeS = wOpeS & "</livelli>"
          'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then
            If Trim(TabIstr.keyfeedback) <> "" Then
              wOpeS = wOpeS & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
            End If
            'procseq: sarebbe meglio metterlo in tabistr, ma per ora facciamo cos� (procseq
            ' ci servir� anche nella generazione routine
            If UBound(TabIstr.psb) Then
              If Len(TabIstr.procSeq) Then
                wOpeS = wOpeS & "<procseq>" & TabIstr.procSeq & "</procseq>"
              End If
              If Len(TabIstr.procOpt) And xIstr = "OPEN" Then
                wOpeS = wOpeS & "<procopt>" & TabIstr.procOpt & "</procopt>"
              End If
            End If
          End If
        
          'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
          ' ora � solo per il programma in esame
          pcbDyn = ""
          pcbDynInt = 0
          maxPcbDyn = 0
          wOpeSave = wOpeS
          For k = 1 To UBound(OpeIstrRt)
            If wOpeS = OpeIstrRt(k).Decodifica Then
              If OpeIstrRt(k).ordPcb = pordPcb Then
                'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
                'verifico se � la prima
                pcbDyn = Format(OpeIstrRt(k).pcbDyn, "000")
                pcbDynInt = OpeIstrRt(k).pcbDyn
                maxPcbDyn = 0
                Exit For
              Else
                If OpeIstrRt(k).pcbDyn > maxPcbDyn Then
                  'trovata un'istruzione identica, ma con diverso pcb
                  maxPcbDyn = OpeIstrRt(k).pcbDyn
                End If
              End If
            End If
          Next k
          'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
          If maxPcbDyn > 0 Then
            pcbDyn = Format(maxPcbDyn + 1, "000")
            pcbDynInt = maxPcbDyn + 1
          End If
          ' aggiunge il pcb dinamico
          'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
          If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Or xIstr = "POS" Then
            If Len(pcbDyn) Then
              wOpeS = wOpeS & "<pcbseq>" & pcbDyn & "</pcbseq>"
              'AggLog "[Object: " & pIdOggetto & "] New Instruction: <pcbseq>" & pcbDyn & "</pcbseq>", MadrdF_Incapsulatore.RTErr
            Else
              ' se � la prima istruzione di quel tipo
              wOpeS = wOpeS & "<pcbseq>" & "001" & "</pcbseq>"
              pcbDynInt = 1
            End If
          End If
       
          wDecodS = wOpeS
          '''''''''''''''''''''''''''''''''''''''''''''''''''
          ' Repository:
          ' se nuova decodifica ==> INSERIMENTO in mgdli_decodificaistr
          ' => INSERIMENTO in PsDli_IstrCodifica
          ' E' uguale al pezzo sopra! ---> FUNCTION
          '''''''''''''''''''''''''''''''''''''''''''''''''''
          Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpeS & "'")
          If tb.EOF Then
            Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
            'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
            If tb1.EOF Then
              maxId = 1
            Else
              maxId = tb1(0) + 1
            End If
            tb.AddNew
            tb!IdIstruzione = maxId
            tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
            wIstrS = tb!Codifica
            tb!Decodifica = wOpeS
            tb!ImsDB = True
            tb!ImsDC = False
            tb!CICS = SwTp
            'stefano: routine multiple
            nomeRoutCompleto = Genera_Counter(nomeRoutine)
            tb!nomeRout = nomeRoutCompleto
            tb.Update
  '          tb.Close
            ' Mauro 28/04/2009
  ''          'pcb dinamico
  ''          k = UBound(OpeIstrRt) + 1
  ''          ReDim Preserve OpeIstrRt(k)
  ''          OpeIstrRt(k).Decodifica = wOpeSave
  ''          OpeIstrRt(k).Istruzione = wIstr
  ''          OpeIstrRt(k).ordPcb = ordPcb
  ''          OpeIstrRt(k).pcbDyn = pcbDynInt
          Else
            wIstrS = tb!Codifica
            nomeRoutCompleto = tb!nomeRout
            ' Mauro 05/06/2008 : il nomeroutine, se c'�, � quello della Decodifica
            nomeRoutine = tb!nomeRout
          End If
          tb.Close
          '
          Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
          If tb.EOF Then
            m_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeRoutCompleto & "')"
            ' Mauro 28/04/2009 : pcb dinamico
            k = UBound(OpeIstrRt) + 1
            ReDim Preserve OpeIstrRt(k)
            OpeIstrRt(k).Decodifica = wOpeSave
            OpeIstrRt(k).Istruzione = wIstr
            OpeIstrRt(k).ordPcb = pordPcb
            OpeIstrRt(k).pcbDyn = pcbDynInt
          End If
          tb.Close
        End If
      
        ''''''''''''''''''''
        ' SOPRA HO SCRITTO SUL DB...
        ' ma sono nel ciclo delle molteplicit�...
        ''''''''''''''''''''
      
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        'BACO: per ora non viene gestita la molteplicit� sulla ISRT per i livelli qualificati
        ' ma in realt� non ha senso (di solito l'inserimento viene fatto qualificando un
        ' livello precedente per uguale, in modo da puntare un solo parent
        
        'AC 24/02/10  tolto filtro
        'If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
          combinazioni = AggiornaCombinazione(maxLogicOp)
        'Else
          'combinazioni = False
        'End If
        
        moltLevel = False
        ' aggiornamento collection
        If Not StringToClear = "" Then
          IstrIncaps = Replace(IstrIncaps, StringToClear, "")
          IstrSIncaps = Replace(IstrSIncaps, StringToClear, "")
        End If
      
        If QualAndUnqual Then
          ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
          checkColl = True
          For j = 1 To wMoltIstr(3).Count
            If wMoltIstr(3).item(j) = wIstr Then
              checkColl = False
              Exit For
            End If
          Next j
          If checkColl Then
            wMoltIstr(1).Add IstrIncaps, wDecod
            wMoltIstr(3).Add wIstr, wDecod
            aggiornalistaCodOp (wIstr)
            y = y + 1
            ReDim Preserve wNomeRout(y)
            wNomeRout(y) = nomeRoutCompleto
          End If
         
          ' 0 e 2  da wOpeS, indice ks dell'array e da IstrSIncaps
          checkColl = True
          checksqual = True
          For j = 1 To wMoltIstr(2).Count
    '        If wMoltIstr(2).Item(j) = OpeIstrRt(ks).Istruzione Then
            If wMoltIstr(2).item(j) = wIstrS Then
              checkColl = False
              checksqual = False
              Exit For
            End If
          Next j
          If checkColl Then
            wMoltIstr(0).Add IstrSIncaps, wDecod
            wMoltIstr(2).Add wIstrS, wDecodS
            aggiornalistaCodOp (wIstrS)
            y = y + 1
            ReDim Preserve wNomeRout(y)
            wNomeRout(y) = nomeRoutCompleto
          End If
        Else
          ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
          checkColl = True
          For j = 1 To wMoltIstr(3).Count
  '          If wMoltIstr(3).Item(j) = OpeIstrRt(k).Istruzione Then
            If wMoltIstr(3).item(j) = wIstr Then
              checkColl = False
              Exit For
            End If
          Next j
          If checkColl Then
            wMoltIstr(1).Add IstrIncaps, wDecod
            wMoltIstr(3).Add wIstr, wDecod
            aggiornalistaCodOp (wIstr)
            y = y + 1
            ReDim Preserve wNomeRout(y)
            wNomeRout(y) = nomeRoutCompleto
          End If
        End If
       
        ' livello COMBQUAL -- costruisce elemento array combqual
        ' nel caso di linguaggi con then (vedi PLI), sui template gestiamo la riga dello then con il tag <then>
        ' negli altri casi l'if sotto � ininfluente
        For ij = 1 To UBound(moltLevelesimo)
          If moltLevelesimo(ij) Then
            moltLevThen = True
            Exit For
          End If
        Next ij
  
        ReDim Preserve CombQual(jk)
        If QualAndUnqual And checksqual Then ' due parti, la seconda � squalificata
          wIstrsOld = wIstrS
          ReDim Preserve CombSqual(UBound(CombSqual) + 1)
        End If
        jk = jk + 1
      Wend
    Next p  'ciclo su pcb
  Next i  'ciclo su molteplicit� istruzioni
  
  Exit Function
ErrH:
  If err.Number = 76 Or err.Number = 75 Then 'file not found
    AggLog "[Object: " & pIdOggetto & "] Template: " & templateName & " not found.", MadrdF_Incapsulatore.RTErr
  Else
    AggLog "[Object: " & pIdOggetto & "] unexpected error on 'createDecode'.", MadrdF_Incapsulatore.RTErr
  End If
  'Resume Next
End Function

Function CheckArrayPgm(parray() As Long, pIdOggetto As Long, pRiga As Long) As Boolean
  Dim i As Integer, tb As Recordset, cod1 As String, Cod As String
  
  i = 1
  Set tb = m_fun.Open_Recordset("select Codifica from Psdli_istrCodifica where IdPgm = " & parray(1) & " and IdOggetto = " & pIdOggetto & " and Riga = " & pRiga)
  If Not tb.EOF Then
    cod1 = tb!Codifica
    Cod = cod1
  Else
    CheckArrayPgm = False
    Exit Function
  End If
  
  While cod1 = Cod And i < UBound(parray())
    i = i + 1
    Set tb = m_fun.Open_Recordset("select Codifica from Psdli_istrCodifica where IdPgm = " & parray(i) & " and IdOggetto = " & pIdOggetto & " and Riga = " & pRiga)
    If Not tb.EOF Then
      Cod = tb!Codifica
    End If
  Wend
  If i = UBound(parray) And cod1 = Cod Then ' decodifiche tutte uguali
    CheckArrayPgm = True
  End If
End Function

' Mauro 23/04/2008
Sub CompareFiles(PathCompare As String, IdObj As Long, FileIncaps As String)
  Dim strIncaps As String, strOrig As String
  Dim nameFile As String
  Dim rs As Recordset
  
  ' File incapsulato
  strIncaps = """" & Replace(FileIncaps, "...", Dli2Rdbms.drPathDb) & """"
  
  ' ricerco il file originale
  Set rs = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & IdObj)
  If rs.RecordCount Then
    strOrig = Replace(rs!Directory_Input, "$", Dli2Rdbms.drPathDef)
    strOrig = strOrig & "\" & rs!nome & IIf(Len(rs!estensione), ".", "") & rs!estensione
    strOrig = """" & strOrig & """"
  End If
  rs.Close
  
  Shell PathCompare & " " & strIncaps & " " & strOrig, vbMaximizedFocus
End Sub

' Mauro 05/06/2008 : Routine che ricerca il nome del DBD Fisico associato al DBD Index
Function Restituisci_DBD_Da_DBDIndex(idDBD As Long) As String
  Dim rsDBD As Recordset
  
  Set rsDBD = m_fun.Open_Recordset("select d.nome " & _
               "from bs_oggetti as a, psdli_xdfield as b, psdli_segmenti as c, bs_oggetti as d where " & _
               "a.idoggetto = " & idDBD & " and a.nome = b.lchild and b.idsegmento = c.idsegmento " & _
               "and c.idoggetto = d.idoggetto")
  If rsDBD.RecordCount Then
    Restituisci_DBD_Da_DBDIndex = rsDBD!nome
  Else
    ' segnalazione
  End If
  rsDBD.Close
End Function
