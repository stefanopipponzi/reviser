VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MaimdF_Incapsulatore 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "IMS/DC - Objects Encapsulator"
   ClientHeight    =   5340
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8895
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5340
   ScaleWidth      =   8895
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame5 
      Caption         =   "Encapsulation Source Options"
      ForeColor       =   &H00000080&
      Height          =   612
      Left            =   0
      TabIndex        =   14
      Top             =   3360
      Width           =   3435
      Begin VB.OptionButton OptEncaps 
         Caption         =   "Encapsulated"
         Height          =   192
         Left            =   1800
         TabIndex        =   16
         Top             =   240
         Width           =   1332
      End
      Begin VB.OptionButton Optsource 
         Caption         =   "Native"
         Height          =   192
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Value           =   -1  'True
         Width           =   852
      End
   End
   Begin RichTextLib.RichTextBox RTBModifica 
      Height          =   525
      Left            =   1320
      TabIndex        =   12
      Top             =   5460
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MaimdF_Incapsulatore.frx":0000
   End
   Begin RichTextLib.RichTextBox RTRoutine 
      Height          =   525
      Left            =   690
      TabIndex        =   11
      Top             =   5460
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MaimdF_Incapsulatore.frx":0082
   End
   Begin RichTextLib.RichTextBox RTFile 
      Height          =   525
      Left            =   60
      TabIndex        =   10
      Top             =   5460
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MaimdF_Incapsulatore.frx":0104
   End
   Begin VB.Frame Frame4 
      Caption         =   "Activity Log"
      ForeColor       =   &H00000080&
      Height          =   3135
      Left            =   3480
      TabIndex        =   6
      Top             =   450
      Width           =   5385
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   255
         Left            =   90
         TabIndex        =   8
         Top             =   2790
         Width           =   5175
         _ExtentX        =   9128
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ListView LvObject 
         Height          =   2295
         Left            =   90
         TabIndex        =   7
         Top             =   240
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   4048
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "ID"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Object"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Time"
            Object.Width           =   2822
         EndProperty
      End
      Begin MSComctlLib.ProgressBar PbarIstr 
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   2520
         Width           =   5175
         _ExtentX        =   9128
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Error Log"
      ForeColor       =   &H00000080&
      Height          =   1725
      Left            =   3480
      TabIndex        =   5
      Top             =   3600
      Width           =   5385
      Begin RichTextLib.RichTextBox RTErr 
         Height          =   1365
         Left            =   60
         TabIndex        =   9
         Top             =   270
         Width           =   5235
         _ExtentX        =   9234
         _ExtentY        =   2408
         _Version        =   393217
         BackColor       =   -2147483624
         Enabled         =   -1  'True
         TextRTF         =   $"MaimdF_Incapsulatore.frx":0186
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Analysis Function"
      ForeColor       =   &H00000080&
      Height          =   1368
      Left            =   0
      TabIndex        =   3
      Top             =   3960
      Width           =   3435
      Begin VB.ListBox LstFunction 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   285
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   4
         Top             =   240
         Width           =   3195
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Select Object for Analysis"
      ForeColor       =   &H00000080&
      Height          =   2772
      Left            =   0
      TabIndex        =   1
      Top             =   450
      Width           =   3435
      Begin MSComctlLib.TreeView TVSelObj 
         Height          =   2745
         Left            =   120
         TabIndex        =   2
         Top             =   270
         Width           =   3195
         _ExtentX        =   5636
         _ExtentY        =   4842
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   529
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   336
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8892
      _ExtentX        =   15690
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Start"
            Object.ToolTipText     =   "Start Encapsulating"
            ImageKey        =   "Attiva"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Help"
            Object.ToolTipText     =   "Help"
            ImageKey        =   "Help"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   2760
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaimdF_Incapsulatore.frx":0208
            Key             =   "Attiva"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaimdF_Incapsulatore.frx":0522
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaimdF_Incapsulatore.frx":0634
            Key             =   "Printer"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MaimdF_Incapsulatore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim NumObjInParser As Double
Dim NumFeatures As Integer

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub LstFunction_Click()
  Dim K1 As Integer

  NumFeatures = 0
  For K1 = 0 To LstFunction.ListCount - 1
    If LstFunction.Selected(K1) = True Then
      NumFeatures = NumFeatures + 1
    End If
  Next K1
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim rIstCBL  As Recordset, rsoffset As Recordset, rsEntry As Recordset
  Dim rst As Recordset
  Dim typeIncaps As String
  Dim MaxVal As Long
  Dim Selezione As String
  Dim totaloffset As Long, incapsEntry As Boolean, rigaEntry As Long, lenentry As Integer
  Dim firstparameter As String
  
  'Ac chiave della call e booleano per capire se nel programma c'� una Chkp (v. send)
  Dim CallKey As String
  Dim bolContainCHKP As Boolean, namePgm As String
   
  Dim Modname As String
  Dim recPsCall As Recordset
  Dim bolspa As Boolean
  Dim spalen As Integer
  Dim rspa As Recordset
  Dim rsTempOff As Recordset
  Dim rig As Long
  Dim ArrFileSpa() As String, numfileSpa As Integer, rigaSpa As Integer, iSpa As Integer
  
  On Error Resume Next
  
  Screen.MousePointer = vbHourglass
  
  Select Case Button.key
    Case "Start"
      DoEvents
    
      If LstFunction.Selected(0) Then
        Exit Sub
      End If
       
      If LstFunction.Selected(1) Then
        typeIncaps = "CICS"
      Else
        typeIncaps = "DC"
      End If
      
      If LstFunction.Selected(2) Then
        m_fun.FnConnection.Execute "Delete " & m_fun.FnAsterixForDelete & " From MgDli_decodificaIstr"
      End If
      
      ReDim OpeIstrRt(0)
      
      Selezione = Restituisci_Selezione_DC(TVSelObj)
      MaxVal = Seleziona_Istruzioni(Selezione, rIstCBL)
      If MaxVal = 0 Then
        MaxVal = 1
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
      
      PBar.Max = MaxVal
      PBar.Value = 0
      PbarIstr.Max = 1
      PbarIstr.Value = 0
      
      If UCase(Left(m_fun.FnNomeDB, 7)) = "SUNGARD" Then
        bolsungard = True
      End If
      
      Do Until rIstCBL.EOF
        incapsEntry = False
        AddRighe = 0
        ReDim LNKDBNumstack(0)
        
        If Optsource Then
          m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = 0 where idoggetto = " & rIstCBL!IdOggetto
        End If
        'Analizza e inserisce le varie istruzioni e chiamate nel CBL
        'Per� non tocca il CBL originale, ma lo copia, modificato, nella directory : CBLOut o CPYOut

        namePgm = Controlla_CBL_CPY_DC(RTBModifica, rIstCBL, typeIncaps)  'carica o salva il file in base al fatto che sia o meno gi� caricato
        
        'Ac per gestire le code di TS  nel programma(DCCLTS) e nella send (flag-send)
        'AC gestione asterisco su ENTRY
        Set rsEntry = m_fun.Open_Recordset("Select * from PsPgm where " & _
                                           "idoggetto = " & rIstCBL!IdOggetto & " and comando ='ENTRY'")
        rigaEntry = 0 'IRIS-DB
        If Not rsEntry.EOF Then
          rigaEntry = rsEntry!Riga
          lenentry = rsEntry!linee
          If InStr(rsEntry!parameters, ",") > 0 Then
            firstparameter = Left(rsEntry!parameters, InStr(rsEntry!parameters, ",") - 1)
          Else
            firstparameter = rsEntry!parameters
          End If
        End If
        rsEntry.Close 'IRIS-DB
        Set rst = m_fun.Open_Recordset("Select * from PSDli_Istruzioni where " & _
                                       "idoggetto = " & rIstCBL!IdOggetto & " order by riga")
        '3) inserimento delle istruzioni:
        PbarIstr.Max = rst.RecordCount
                  
        '**************** AC modifica spa
        bolspa = False
        Set rspa = m_fun.Open_Recordset("SELECT SPALEN FROM IMS_Catalog where " & _
                                        "PSB = '" & rIstCBL!nome & "' and SPALEN is not Null")
        If Not rspa.EOF Then
          If rspa!spalen Then
            bolspa = True
            spalen = rspa!spalen
          End If
        End If
        rspa.Close
        '*******************
          
        'ATTENZIONE!! Se si elimina questo controllo ricordare di lasciare il ramo ELSE
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        GbIdOggetto = rIstCBL!IdOggetto 'stefano
'IRIS-DB: spostata in fondo        changeGOBACK RTBModifica, namePgm
        'stefano: la entry la deve asteriscare anche se � l'unica istruzione.., inoltre ha gi� inserito le copy
        ' modificona schifosa da fare in maniera intelligente
''        If rst.EOF Then
''          m_fun.FnConnection.Execute "UPDATE ps_objrowoffset SET offsetencaps = offsetencaps + " & AddRighe & " WHERE idoggetto = " & rIstCBL!IdOggetto
''          Set rsoffset = m_fun.Open_Recordset("Select * from PS_objRowOffset where idoggetto = " & rIstCBL!IdOggetto & " and riga =" & rigaEntry)
''          If Not rsoffset.EOF Then
''            totaloffset = IIf(IsNull(rsoffset!offsetMacro), 0, rsoffset!offsetMacro) + rsoffset!offsetEncaps
''          Else
''            rsoffset.Close
''            Set rsoffset = m_fun.Open_Recordset("Select * from PS_objRowOffset")
''            rsoffset.AddNew
''            rsoffset!IdOggetto = rst!IdOggetto
''            rsoffset!Riga = rst!Riga
''            'bisogna allineare le righe nuove all'offset preesistente sul db2 to oracle, prendendolo dal
''            'record precedente!!!
''            Set rsTempOff = m_fun.Open_Recordset("Select max(riga) as maxR from PS_objRowOffset where idoggetto = " & rIstCBL!IdOggetto & " and riga < " & rigaEntry)
''            If Not rsTempOff.EOF Then
''              rig = rsTempOff!maxR
''              Set rsTempOff = m_fun.Open_Recordset("Select offsetMacro, offsetEncaps from PS_objRowOffset where idoggetto = " & rIstCBL!IdOggetto & " and riga = " & rig)
''              If Not rsTempOff.EOF Then
''                rsoffset!offsetMacro = rsTempOff!offsetMacro
''                rsoffset!offsetEncaps = rsTempOff!offsetEncaps
''              End If
''              rsTempOff.Close
''            End If
''            rsoffset.Update
''            rsoffset.Close
''          End If
''          CallKey = "ENTRY"
''          AddRighe = 0 'gi� sommato nella tabella
''          Posizione_DC = AsteriscaEntry(totaloffset, rigaEntry, RTBModifica, typeIncaps, CallKey)
''          If Posizione_DC = 0 Then
''            m_fun.WriteLog "Incaps: Obj(" & rIstCBL!IdOggetto & ") ENTRY Row( " & rigaEntry & "non trovata", "MadrdF_Incapsulatore"
''          End If
''          incapsEntry = True
''        End If
        'IRIS-DB Entry to keep end
          
        Do Until rst.EOF
          ' AC asterisco la entry
        'IRIS-DB Entry to keep start
''          If rst!Riga > rigaEntry And Not incapsEntry Then
''            Set rsoffset = m_fun.Open_Recordset("Select * from PS_objRowOffset where idoggetto = " & rst!IdOggetto & " and riga =" & rigaEntry)
''            If Not rsoffset.EOF Then
''              totaloffset = IIf(IsNull(rsoffset!offsetMacro), 0, rsoffset!offsetMacro) + IIf(IsNull(rsoffset!offsetEncaps), 0, rsoffset!offsetEncaps)
''            Else
''              rsoffset.Close
''              Set rsoffset = m_fun.Open_Recordset("Select * from PS_objRowOffset")
''              rsoffset.AddNew
''              rsoffset!IdOggetto = rst!IdOggetto
''              rsoffset!Riga = rigaEntry
''            End If
''            CallKey = "ENTRY"
''            wPunto = FindPunto(rst!IdOggetto, rigaEntry)  'SQ  ?????!!!! Abbiamo la ENTRY nella PsCall?!
''            If Optsource Then
''              rsoffset!offsetEncaps = AddRighe
''            Else
''              rsoffset!offsetEncaps = IIf(IsNull(rsoffset!offsetEncaps), 0, rsoffset!offsetEncaps) + AddRighe
''            End If
''            If istrIncopy(rst!IdOggetto) And OptEncaps.Value Then
''              rigaEntry = rigaEntry + 1
''            End If
''            Posizione_DC = AsteriscaEntry(totaloffset, rigaEntry, RTBModifica, typeIncaps, CallKey)
''            'stefano: da fare solo per imsdc!!
''            If Posizione_DC > 0 Then
''              If rIstCBL!Ims Then
''                INSERT_IMSDC_ENTRY Posizione_DC, RTBModifica, rst, firstparameter
''              End If
''            Else
''              m_fun.WriteLog "Incaps: Obj(" & rIstCBL!IdOggetto & ") ENTRY Row( " & rst!Riga & "non trovata", "MadrdF_Incapsulatore"
''            End If
''            rsoffset.Update
''            rsoffset.Close
''            incapsEntry = True
''          End If
        'IRIS-DB Entry to keep end
          Set rsoffset = m_fun.Open_Recordset("Select * from PS_objRowOffset where idoggetto = " & rst!IdOggetto & " and riga =" & rst!Riga) 'And not ImsDC
          If Not rsoffset.EOF Then
            totaloffset = IIf(IsNull(rsoffset!offsetMacro), 0, rsoffset!offsetMacro) + IIf(IsNull(rsoffset!offsetEncaps), 0, rsoffset!offsetEncaps)
          Else
            'IRIS-DB: se non trovato deve dare messaggio di errore, � grave se non � stato gi� inserito in incapsulamento DB, perch� in ogni caso vuol dire che � disallineato
            m_fun.WriteLog "IMSDC Incaps: Obj(" & rIstCBL!IdOggetto & ") Row( " & rst!Riga & "non trovata su PS_objRowOffset", "MadrdF_Incapsulatore"
            rsoffset.Close
            Set rsoffset = m_fun.Open_Recordset("Select * from PS_objRowOffset")
            rsoffset.AddNew
            rsoffset!IdOggetto = rst!IdOggetto
            rsoffset!Riga = rst!Riga
            totaloffset = 0 'IRIS-DB
          End If
          'IRIS-DB non pu� farlo se vuole aggiornare dopo!     rsoffset.Close
          If Not rst!ImsDB Then
            Set recPsCall = m_fun.Open_Recordset("Select Programma from PsCall where idoggetto = " & rst!IdOggetto & " and Riga = " & rst!Riga)
            If Not recPsCall.EOF Then
              CallKey = recPsCall!Programma
            End If
            recPsCall.Close
            '***************
            wPunto = FindPunto(rst!IdOggetto, rst!Riga)
            If Optsource Then
              rsoffset!offsetEncaps = AddRighe
            Else
              rsoffset!offsetEncaps = IIf(IsNull(rsoffset!offsetEncaps), 0, rsoffset!offsetEncaps) + AddRighe
            End If
            Posizione_DC = AsteriscaIstruzione_DC(totaloffset, rst, RTBModifica, typeIncaps, CallKey)
            If Posizione_DC Then
              'IRIS-DB If rst!valida Then ' Ormai � unico sia per DLI che per RDBMS Or TypeIncaps = "RDBMS" Then ' Fabio  : Or TypeIncaps = "DLI" Then
              If rst!valida And Not rst!obsolete Then 'IRIS-DB
                Create_Chiave_decodifca Trim(rst!Istruzione)
                'IRIS-DB: used typeIncaps - as not used before - for setting Online or Batch
                If rIstCBL!ims Then 'IRIS-DB
                  typeIncaps = "ONLINE" 'IRIS-DB
                Else 'IRIS-DB
                  typeIncaps = "BATCH" 'IRIS-DB
                End If 'IRIS-DB
                Select Case Trim(rst!Istruzione)
                  Case "GU"
                    INSERT_IMSDC_RECEIVE Posizione_DC, RTBModifica, rst, typeIncaps, bolspa
                  Case "GN"
                    INSERT_IMSDC_RECEIVE Posizione_DC, RTBModifica, rst, typeIncaps, False
                  Case "ISRT"
                    Modname = rst!MapName
                    INSERT_IMSDC_SEND Posizione_DC, RTBModifica, rst, typeIncaps, bolContainCHKP, bolspa
                  Case "CHKP"
                    INSERT_IMSDC_CHECKPOINT Posizione_DC, RTBModifica, rst, typeIncaps
                  Case "XRST"
                    INSERT_IMSDC_XSRT Posizione_DC, RTBModifica, rst, typeIncaps
                  Case "ROLL", "ROLB"
                    INSERT_IMSDC_ROLL Posizione_DC, RTBModifica, rst, typeIncaps
                  Case "CHNG"
                    INSERT_IMSDC_CHNG Posizione_DC, RTBModifica, rst, typeIncaps
                  Case "PURG"
                    INSERT_IMSDC_PURG Posizione_DC, RTBModifica, rst, typeIncaps
                  Case "AUTH"
                    INSERT_IMSDC_AUTH Posizione_DC, RTBModifica, rst, typeIncaps
                  Case "INQY"
                    INSERT_IMSDC_INQY Posizione_DC, RTBModifica, rst, typeIncaps
'IRIS-DB                  Case "ICMD"
'IRIS-DB                    INSERT_IMSDC_ICMD Posizione_DC, RTBModifica, rst, typeIncaps
                  Case "CMD" 'IRIS-DB
                    INSERT_IMSDC_CMD Posizione_DC, RTBModifica, rst, typeIncaps 'IRIS-DB
                  Case "GCMD" 'IRIS-DB uses the same of CMD (actually all instructions are very similar..
                    INSERT_IMSDC_CMD Posizione_DC, RTBModifica, rst, typeIncaps 'IRIS-DB
                  Case "CHNG"
                    INSERT_IMSDC_CHNG Posizione_DC, RTBModifica, rst, typeIncaps
                  Case "ROLS" 'IRIS-DB
                    INSERT_IMSDC_ROLS Posizione_DC, RTBModifica, rst, typeIncaps 'IRIS-DB
                  Case "SETS" 'IRIS-DB
                    INSERT_IMSDC_SETS Posizione_DC, RTBModifica, rst, typeIncaps 'IRIS-DB
                  Case Else
                    m_fun.WriteLog "Incaps: idoggetto(" & rst!IdOggetto & ") Riga(" & rst!Riga & ") Istruzione " & Trim(rst!Istruzione) & " non gestita", "MadrdF_Incapsulatore"
                End Select
              Else
                If rst!obsolete Then 'IRIS-DB
                  INSERT_NODECOD_DC Posizione_DC, RTBModifica, rst, typeIncaps
                Else
                  m_fun.WriteLog "Incaps: Obj(" & rIstCBL!IdOggetto & ") Row( " & rst!Riga & "non trovata", "MadrdF_Incapsulatore"
                End If
              End If
            Else
              m_fun.WriteLog "Incaps: Obj(" & rIstCBL!IdOggetto & ") Row( " & rst!Riga & "non trovata", "MadrdF_Incapsulatore"
            End If
          Else
            If Optsource Then
              rsoffset!offsetEncaps = AddRighe
            Else
              rsoffset!offsetEncaps = IIf(IsNull(rsoffset!offsetEncaps), 0, rsoffset!offsetEncaps) + AddRighe
            End If
          End If
          rsoffset.Update
          rsoffset.Close 'IRIS-DB
          rst.MoveNext
          PbarIstr.Value = PbarIstr.Value + 1
        Loop
        
        If rigaEntry > 0 And rIstCBL!ims Then changeGOBACK RTBModifica, namePgm  'IRIS-DB: ripristinato solo per i main online, messo in fondo per evitare problemi con Addrighe
        If tipofiledc = "PLI" And rIstCBL!ims Then changeGOBACKPLI RTBModifica, namePgm, ristcbl!IdOggetto 
      
        PbarIstr.Value = 0
        RTBModifica.SaveFile OldName, 1
                  
        With LvObject.ListItems.Add(, , rIstCBL!IdOggetto)
          .SubItems(1) = OldName
          .SubItems(2) = Now
        End With
        
        LvObject.ListItems(LvObject.ListItems.Count).Selected = True
        LvObject.Refresh
        LvObject.SelectedItem.EnsureVisible
        If PBar.Max < PBar.Value + 1 Then
          PBar.Value = PBar.Max
        Else
          PBar.Value = PBar.Value + 1
        End If
        
        '****************** AC
        If bolspa Then  ' trattamento spa
          rigaSpa = 1
          ReDim ArrFileSpa(rigaSpa)
          numfileSpa = FreeFile
          Open OldName For Input As numfileSpa
          Line Input #numfileSpa, ArrFileSpa(rigaSpa)
          Do Until EOF(numfileSpa)
            rigaSpa = rigaSpa + 1
            ReDim Preserve ArrFileSpa(rigaSpa)
            Line Input #numfileSpa, ArrFileSpa(rigaSpa)
          Loop
          Close numfileSpa
          AsteriscaArraySpa ArrFileSpa, spalen
          numfileSpa = FreeFile
          Open OldName For Output As numfileSpa
          For iSpa = 1 To UBound(ArrFileSpa)
            Print #numfileSpa, ArrFileSpa(iSpa)
          Next iSpa
          Close numfileSpa
        End If
        rIstCBL.MoveNext
      Loop
      rIstCBL.Close
      PBar.Value = 0
     
    Case "Close"
      Unload Me
      
    Case "Help"
      'Da fare: Aggiunge il codice per il pulsante 'Help'.
  End Select
  
  Screen.MousePointer = vbDefault
End Sub

Public Sub Resize()
  On Error Resume Next
  Me.Move m_ImsDll_DC.ImLeft, m_ImsDll_DC.ImTop, m_ImsDll_DC.ImWidth, m_ImsDll_DC.ImHeight
  Frame2.Move 30, 400, Frame2.Width, (m_ImsDll_DC.ImHeight - 400 - Toolbar1.Height) / 3 * 2
  TVSelObj.Move 120, 240, Frame2.Width - 240, Frame2.Height - 360
  Frame1.Move 30, 430 + Frame2.Height, Frame1.Width, ((m_ImsDll_DC.ImHeight - 340) / 3) - 60
  LstFunction.Move 120, 240, Frame1.Width - 240, Frame1.Height - 360
  Frame4.Move Frame4.Left, Frame2.Top, m_ImsDll_DC.ImWidth - Frame4.Left - 120, Frame2.Height
  LvObject.Move 120, 240, Frame4.Width - 240, Frame4.Height - 420 - PBar.Height - PbarIstr.Height - 60
  PBar.Move LvObject.Left, LvObject.Top + LvObject.Height + 30, LvObject.Width, PBar.Height
  PbarIstr.Move LvObject.Left, PBar.Top + PBar.Height + 30, LvObject.Width, PbarIstr.Height
  Frame3.Move Frame3.Left, Frame1.Top, m_ImsDll_DC.ImWidth - Frame3.Left - 120, Frame1.Height
  RTErr.Move 120, 240, Frame3.Width - 240, Frame3.Height - 360
End Sub

Private Sub Form_Load()
  LstFunction.AddItem "Generate copy with name of routines"
  LstFunction.AddItem "IMS/DC Features to CICS"
  LstFunction.AddItem "Clear Instructions Key table"
  
  LstFunction.Selected(1) = True
  NumFeatures = 1
  NumObjInParser = 0

  TVSelObj.Nodes.Add , , "SEL", "Only Selected Object"
  TVSelObj.Nodes.Add , , "SRC", "Sources"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CBL", "Cobol Program"
   
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
  
  loadEnvironmentParameters_DC
End Sub

Private Sub Form_Unload(Cancel As Integer)
  If m_ImsDll_DC.ImTipoFinIm <> "" Then
    m_ImsDll_DC.ImFinestra.ListItems.Add , , "Ims2MTP : Incasulator Window Unloaded at " & Time
  End If
  
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub TVSelObj_NodeCheck(ByVal Node As MSComctlLib.Node)
  Dim K1 As Integer
  Dim Wkey As String
  Dim wCheck As Variant
  Dim tb As Recordset
  
  Wkey = Node.key
  wCheck = Node.Checked
  
  NumObjInParser = 0
  
  Select Case Node.key
    Case "SEL"
      If Node.Checked Then
        For K1 = 1 To m_ImsDll_DC.ImObjList.ListItems.Count
          If m_ImsDll_DC.ImObjList.ListItems(K1).Selected Then
            NumObjInParser = NumObjInParser + 1
          End If
        Next K1
      End If
      
      If TVSelObj.Nodes(Node.Index + 1).Checked = True Then
        TVSelObj.Nodes(Node.Index + 1).Checked = False
        TVSelObj.Nodes(Node.Index + 2).Checked = False
      End If
     
    Case "CBL"
      TVSelObj.Nodes(Node.Index - 1).Checked = Node.Checked
      TVSelObj.Nodes(1).Checked = False
      
    Case Else
      For K1 = Node.Index + 1 To Node.Index + Node.Children
        TVSelObj.Nodes(K1).Checked = Node.Checked
      Next K1
      TVSelObj.Nodes(1).Checked = False
  End Select
        
  If Not TVSelObj.Nodes(1).Checked Then
    For K1 = 2 To TVSelObj.Nodes.Count
      If TVSelObj.Nodes(K1).Checked Then
        Wkey = TVSelObj.Nodes(K1).key
        If Wkey = "MPS" Then
          Wkey = "BMS"
        End If
        Set tb = m_fun.Open_Recordset("Select * from bs_Oggetti where tipo = '" & Wkey & "'")
        If tb.RecordCount Then
          tb.MoveLast
          NumObjInParser = NumObjInParser + tb.RecordCount
          tb.Close
        End If
        tb.Close
      End If
    Next K1
  End If
  
  Frame4.Caption = "Activite log for " & NumObjInParser & " Objects"
End Sub
