VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MavsdC_Menu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_drIdOggetto As Long
Private m_drErrCodice As String
Private m_drErrMsg As String
Private m_drFinestra As Object
Private m_drObjList As Object
Private m_drTreePrj As Object
Private m_drTop As Long
Private m_drLeft As Long
Private m_drWidth As Long
Private m_drHeight As Long
Private m_drDatabase As ADOX.Catalog
Private m_drConnection As ADODB.Connection
Private m_drConnState As Long
Private m_drNomeDB As String
Private m_drTipoMigrazione As String
Private m_drNomeOggetto As String
Private m_drPathDb As String
Private m_drPathDef As String
Private m_drUnitDef As String
Private m_drPathPrd As String
Private m_drTipoDB As String
Private m_drNomeProdotto As String
Private m_drImgDir As String
Private m_drParent As Long
Private m_drId As Long
Private m_drLabel As String
Private m_drToolTiptext As String
Private m_drPicture As String
Private m_drPictureEn As String
Private m_drM1Name As String
Private m_drM1SubName As String
Private m_drM1Level As Long
Private m_drM2Name As String
Private m_drM3Name As String
Private m_drM3ButtonType As String
Private m_drM3SubName As String
Private m_drM4Name As String
Private m_drM4ButtonType As String
Private m_drM4SubName As String
Private m_drFunzione As String
Private m_drDllName As String
Private m_drTipoFinIm As String
Private m_drFunctionCur As String
Private m_drFunctionStatus As String
Private m_drCollectionParam As collection
Private m_drAsterixForDelete As String
Private m_drAsterixForWhere As String
Private m_drProvider As e_Provider

'Parametri
Private m_DrCallDli As String 'EXEC = EXEC,CALL = CALL
Private m_DrCallRtn As String 'EXEC = EXEC CICS, CALL = CALL
Private m_DrMaxLevIstr As Long 'Numero
Private m_DrIMSUsing As String 'UNI = IMS valide sia TP che BATCH, DIV = IMS Divise per TP e per BATCH
Private m_DrUsing As String 'CPY = Usa Copy, PRE = Usa precompilatore
Private m_DrTPRout As String
Private m_DrBTRout As String
Private m_DrGNRout As String

Dim drIndMnu As Long
Dim drIndImg As Long
Public DLLFunzioni As New MaFndC_Funzioni


Public Function AttivaFunzione(Funzione As String, cParam As collection, formParent As Object, Optional IdButton As String) As Boolean
  Dim b As Boolean
  
  On Error GoTo ErrorHandler
  Select Case Trim(UCase(Funzione))
    Case "SHOW_TOOLS"
      If drConnState = adStateOpen Then
        'ALE QUI
        SetParent MavsdF_Verifica.hwnd, formParent.hwnd
        MavsdF_Verifica.Show
        MavsdF_Verifica.Move 0, 0, formParent.Width, formParent.Height
        
      Else
         MsgBox "You must select a Valid Project...", vbExclamation, VSAM2Rdbms.drNomeProdotto
      End If
      
    Case "SHOW_CORRAUTO"
       If drConnState = adStateOpen Then
          If VSAM2Rdbms.drIdOggetto <> 0 And Trim(UCase(Restituisci_TipoOgg_Da_IdOggetto(VSAM2Rdbms.drIdOggetto))) = "CBL" Then
            SetParent MavsdF_Corrector.hwnd, formParent.hwnd
            MavsdF_Corrector.Show
            MavsdF_Corrector.Move 0, 0, formParent.Width, formParent.Height
          Else
            MsgBox "You must select a Valid Object [ex. CBL]...", vbExclamation, VSAM2Rdbms.drNomeProdotto
            Exit Function
          End If
       Else
         MsgBox "You must select a Valid Object [ex. CBL]...", vbExclamation, VSAM2Rdbms.drNomeProdotto
       End If
    
    Case "SHOW_MIGRATOR"
      If drConnState = adStateOpen Then
         SetParent MavsdF_GenRout.hwnd, formParent.hwnd
         MavsdF_GenRout.Show
         MavsdF_GenRout.Move 0, 0, formParent.Width, formParent.Height
      End If
    
    Case "SHOW_INCAPSULATORE"
      If drConnState = adStateOpen Then
         SetParent MavsdF_Incapsulatore.hwnd, formParent.hwnd
         MavsdF_Incapsulatore.Show
         MavsdF_Incapsulatore.Move 0, 0, formParent.Width, formParent.Height
      End If
    
    Case "SHOW_VSAM2Rdbms_PARAMETER"
       If drConnState = adStateOpen Then
          SetParent MavsdF_Parametri.hwnd, formParent.hwnd
          MavsdF_Parametri.Show
          MavsdF_Parametri.Move 0, 0, formParent.Width, formParent.Height
       End If
    
  End Select
  
    Exit Function
ErrorHandler:
  Screen.MousePointer = vbDefault
  DLLFunzioni.FnProcessRunning = False
  DLLFunzioni.Show_MsgBoxError "RC0002"
End Function

Public Function Move_To_Button(Direction As String) As Boolean
  'Direction =
  '
  '1)First    : Si muove al primo bottone dell'array
  '2)Next     : Si muove al successivo
  '3)Previous : Si muove al precedente
  '4)Last     : Si muove all'ultimo
  
  If Not SwMenu Then
    Move_To_Button = Carica_Menu
  End If
  
  'seleziona l'indice
  Select Case UCase(Trim(Direction))
    Case "FIRST"
      drIndMnu = 1
    Case "NEXT"
      drIndMnu = drIndMnu + 1
    Case "PREVIOUS"
      drIndMnu = drIndMnu - 1
    Case "LAST"
      drIndMnu = UBound(drMenu)
  End Select
    
  If drIndMnu > UBound(drMenu) Then
    Move_To_Button = False
    Exit Function
  End If
  
  'assegna alle variabili public il contenuto dell'elemento puntato dell'array
  drId = drMenu(drIndMnu).Id
  drLabel = drMenu(drIndMnu).Label
  drToolTiptext = drMenu(drIndMnu).ToolTipText
  drPicture = drMenu(drIndMnu).Picture
  drPictureEn = drMenu(drIndMnu).PictureEn
  drM1Name = drMenu(drIndMnu).M1Name
  drM1SubName = drMenu(drIndMnu).M1SubName
  drM1Level = drMenu(drIndMnu).M1Level
  drM2Name = drMenu(drIndMnu).M2Name
  drM3Name = drMenu(drIndMnu).M3Name
  drM3ButtonType = drMenu(drIndMnu).M3ButtonType
  drM3SubName = drMenu(drIndMnu).M3SubName
  drM4Name = drMenu(drIndMnu).M4Name
  drM4ButtonType = drMenu(drIndMnu).M4ButtonType
  drM4SubName = drMenu(drIndMnu).M4SubName
  drFunzione = drMenu(drIndMnu).Funzione
  drDllName = drMenu(drIndMnu).DllName
  drTipoFinIm = drMenu(drIndMnu).TipoFinIm
  
  Move_To_Button = True
End Function


Public Sub InitDll(curDllFunzioni As MaFndC_Funzioni)
   Set m_fun = curDllFunzioni
 Call Carica_Menu
End Sub

Private Sub Class_Initialize()
  Set VSAM2Rdbms = Me
End Sub

Public Sub Carica_Parametri(ArrayDBD() As String, ArrOR() As String, ArrNew() As String)
  'Carica i Parametri
  Carica_Parametri_Da_DB ArrayDBD, ArrOR, ArrNew
End Sub

'****************************************************
'***** DEFINIZIONE PROPERTY GET/LET/SET *************
'****************************************************
'DRERRCODICE
Public Property Let drErrCodice(mVar As String)
    m_drErrCodice = mVar
End Property

Public Property Get drErrCodice() As String
    drErrCodice = m_drErrCodice
End Property

'DRERRMSG
Public Property Let drErrMsg(mVar As String)
    m_drErrMsg = mVar
End Property

Public Property Get drErrMsg() As String
    drErrMsg = m_drErrMsg
End Property

'DRFINESTRA
Public Property Set drFinestra(mVar As Object)
   Set m_drFinestra = mVar
End Property

Public Property Get drFinestra() As Object
   Set drFinestra = m_drFinestra
End Property

'DROBJLIST
Public Property Set drObjList(mVar As Object)
   Set m_drObjList = mVar
End Property

Public Property Get drObjList() As Object
   Set drObjList = m_drObjList
End Property

'DRTREEPRJ
Public Property Set DrTreePrj(mVar As Object)
   Set m_drTreePrj = mVar
End Property

Public Property Get DrTreePrj() As Object
   Set DrTreePrj = m_drTreePrj
End Property

'DRTOP
Public Property Let drTop(mVar As Long)
    m_drTop = mVar
End Property

Public Property Get drTop() As Long
    drTop = m_drTop
End Property

'DRLEFT
Public Property Let drLeft(mVar As Long)
    m_drLeft = mVar
End Property

Public Property Get drLeft() As Long
    drLeft = m_drLeft
End Property

'DRWIDTH
Public Property Let drWidth(mVar As Long)
    m_drWidth = mVar
End Property

Public Property Get drWidth() As Long
    drWidth = m_drWidth
End Property

'DRHEIGHT
Public Property Let drHeight(mVar As Long)
    m_drHeight = mVar
End Property

Public Property Get drHeight() As Long
    drHeight = m_drHeight
End Property

'DRDATABASE
Public Property Set drDatabase(mVar As ADOX.Catalog)
   Set m_drDatabase = mVar
End Property

Public Property Get drDatabase() As ADOX.Catalog
   Set drDatabase = m_drDatabase
End Property

'DRCONNECTION
Public Property Set drConnection(mVar As ADODB.Connection)
   Set m_drConnection = mVar
End Property

Public Property Get drConnection() As ADODB.Connection
   Set drConnection = m_drConnection
End Property

'DRCONNSTATE
Public Property Let drConnState(mVar As Long)
    m_drConnState = mVar
End Property

Public Property Get drConnState() As Long
    drConnState = m_drConnState
End Property

'DRNOMEDB
Public Property Let drNomeDB(mVar As String)
    m_drNomeDB = mVar
End Property

Public Property Get drNomeDB() As String
    drNomeDB = m_drNomeDB
End Property

'DRTIPOMIGRAZIONE
Public Property Let drTipoMigrazione(mVar As String)
    m_drTipoMigrazione = mVar
End Property

Public Property Get drTipoMigrazione() As String
    drTipoMigrazione = m_drTipoMigrazione
End Property

'DRNOMEOGGETTO
Public Property Let drNomeOggetto(mVar As String)
    m_drNomeOggetto = mVar
End Property

Public Property Get drNomeOggetto() As String
    drNomeOggetto = m_drNomeOggetto
End Property

'DRPATHDB
Public Property Let drPathDb(mVar As String)
    m_drPathDb = mVar
End Property

Public Property Get drPathDb() As String
    drPathDb = m_drPathDb
End Property

'DRPATHDEF
Public Property Let drPathDef(mVar As String)
    m_drPathDef = mVar
End Property

Public Property Get drPathDef() As String
    drPathDef = m_drPathDef
End Property

'DRUNITDEF
Public Property Let drUnitDef(mVar As String)
    m_drUnitDef = mVar
End Property

Public Property Get drUnitDef() As String
    drUnitDef = m_drUnitDef
End Property

'DRPATHPRD
Public Property Let drPathPrd(mVar As String)
    m_drPathPrd = mVar
End Property

Public Property Get drPathPrd() As String
    drPathPrd = m_drPathPrd
End Property

'DRTIPODB
Public Property Let drTipoDB(mVar As String)
    m_drTipoDB = mVar
End Property

Public Property Get drTipoDB() As String
    drTipoDB = m_drTipoDB
End Property

'DRNOMEPRODOTTO
Public Property Let drNomeProdotto(mVar As String)
    m_drNomeProdotto = mVar
End Property

Public Property Get drNomeProdotto() As String
    drNomeProdotto = m_drNomeProdotto
End Property

'DRIMGDIR
Public Property Let drImgDir(mVar As String)
    m_drImgDir = mVar
End Property

Public Property Get drImgDir() As String
    drImgDir = m_drImgDir
End Property

'DRPARENT
Public Property Let drParent(mVar As Long)
    m_drParent = mVar
End Property

Public Property Get drParent() As Long
    drParent = m_drParent
End Property

'DRID
Public Property Let drId(mVar As Long)
    m_drId = mVar
End Property

Public Property Get drId() As Long
    drId = m_drId
End Property

'DRLABEL
Public Property Let drLabel(mVar As String)
    m_drLabel = mVar
End Property

Public Property Get drLabel() As String
    drLabel = m_drLabel
End Property

'DRTOOLTIPTEXT
Public Property Let drToolTiptext(mVar As String)
    m_drToolTiptext = mVar
End Property

Public Property Get drToolTiptext() As String
    drToolTiptext = m_drToolTiptext
End Property

'DRPICTURE
Public Property Let drPicture(mVar As String)
    m_drPicture = mVar
End Property

Public Property Get drPicture() As String
    drPicture = m_drPicture
End Property

'DRPICTUREEN
Public Property Let drPictureEn(mVar As String)
    m_drPictureEn = mVar
End Property

Public Property Get drPictureEn() As String
    drPictureEn = m_drPictureEn
End Property

'DRM1NAME
Public Property Let drM1Name(mVar As String)
    m_drM1Name = mVar
End Property

Public Property Get drM1Name() As String
    drM1Name = m_drM1Name
End Property

'DRM1SUBNAME
Public Property Let drM1SubName(mVar As String)
    m_drM1SubName = mVar
End Property

Public Property Get drM1SubName() As String
    drM1SubName = m_drM1SubName
End Property

'DRM1LEVEL
Public Property Let drM1Level(mVar As Long)
    m_drM1Level = mVar
End Property

Public Property Get drM1Level() As Long
    drM1Level = m_drM1Level
End Property

'DRM2NAME
Public Property Let drM2Name(mVar As String)
    m_drM2Name = mVar
End Property

Public Property Get drM2Name() As String
    drM2Name = m_drM2Name
End Property

'DRM3NAME
Public Property Let drM3Name(mVar As String)
    m_drM3Name = mVar
End Property

Public Property Get drM3Name() As String
    drM3Name = m_drM3Name
End Property

'DRM3BUTTONTYPE
Public Property Let drM3ButtonType(mVar As String)
    m_drM3ButtonType = mVar
End Property

Public Property Get drM3ButtonType() As String
    drM3ButtonType = m_drM3ButtonType
End Property

'DRM3SUBNAME
Public Property Let drM3SubName(mVar As String)
    m_drM3SubName = mVar
End Property

Public Property Get drM3SubName() As String
    drM3SubName = m_drM3SubName
End Property

'DRM4NAME
Public Property Let drM4Name(mVar As String)
    m_drM4Name = mVar
End Property

Public Property Get drM4Name() As String
    drM4Name = m_drM4Name
End Property

'DRM4BUTTONTYPE
Public Property Let drM4ButtonType(mVar As String)
    m_drM4ButtonType = mVar
End Property

Public Property Get drM4ButtonType() As String
    drM4ButtonType = m_drM4ButtonType
End Property

'DRM4SUBNAME
Public Property Let drM4SubName(mVar As String)
    m_drM4SubName = mVar
End Property

Public Property Get drM4SubName() As String
    drM4SubName = m_drM4SubName
End Property

'DRFUNZIONE
Public Property Let drFunzione(mVar As String)
    m_drFunzione = mVar
End Property

Public Property Get drFunzione() As String
    drFunzione = m_drFunzione
End Property

'DRDLLNAME
Public Property Let drDllName(mVar As String)
    m_drDllName = mVar
End Property

Public Property Get drDllName() As String
    drDllName = m_drDllName
End Property

'DRTIPOFINIM
Public Property Let drTipoFinIm(mVar As String)
    m_drTipoFinIm = mVar
End Property

Public Property Get drTipoFinIm() As String
    drTipoFinIm = m_drTipoFinIm
End Property

'DRFUNCTIONCUR
Public Property Let drFunctionCur(mVar As String)
    m_drFunctionCur = mVar
End Property

Public Property Get drFunctionCur() As String
    drFunctionCur = m_drFunctionCur
End Property

'DRFUNCTIONSTATUS
Public Property Let drFunctionStatus(mVar As String)
    m_drFunctionStatus = mVar
End Property

Public Property Get drFunctionStatus() As String
    drFunctionStatus = m_drFunctionStatus
End Property

'DRCOLLECTIONPARAM
Public Property Set drCollectionParam(mVar As collection)
   Set m_drCollectionParam = mVar
End Property

Public Property Get drCollectionParam() As collection
   Set drCollectionParam = m_drCollectionParam
End Property

'DRASTERIXFORDELETE
Public Property Let drAsterixForDelete(mVar As String)
    m_drAsterixForDelete = mVar
End Property

Public Property Get drAsterixForDelete() As String
    drAsterixForDelete = m_drAsterixForDelete
End Property

'DRASTERIXFORWHERE
Public Property Let drAsterixForWhere(mVar As String)
    m_drAsterixForWhere = mVar
End Property

Public Property Get drAsterixForWhere() As String
    drAsterixForWhere = m_drAsterixForWhere
End Property

'DRPROVIDER
Public Property Let drProvider(mVar As e_Provider)
    m_drProvider = mVar
End Property

Public Property Get drProvider() As e_Provider
    drProvider = m_drProvider
End Property

'DRCALLDLI
Public Property Let DrCallDli(mVar As String)
    m_DrCallDli = mVar
End Property

Public Property Get DrCallDli() As String
    DrCallDli = m_DrCallDli
End Property

'DRCALLRTN
Public Property Let DrCallRtn(mVar As String)
    m_DrCallRtn = mVar
End Property

Public Property Get DrCallRtn() As String
    DrCallRtn = m_DrCallRtn
End Property

'DRMAXLEVISTR
Public Property Let DrMaxLevIstr(mVar As Long)
    m_DrMaxLevIstr = mVar
End Property

Public Property Get DrMaxLevIstr() As Long
    DrMaxLevIstr = m_DrMaxLevIstr
End Property

'DRIMSUSING
Public Property Let DrIMSUsing(mVar As String)
    m_DrIMSUsing = mVar
End Property

Public Property Get DrIMSUsing() As String
    DrIMSUsing = m_DrIMSUsing
End Property

'DRUSING
Public Property Let DrUsing(mVar As String)
    m_DrUsing = mVar
End Property

Public Property Get DrUsing() As String
    DrUsing = m_DrUsing
End Property

'DRTPROUT
Public Property Let DrTPRout(mVar As String)
    m_DrTPRout = mVar
End Property

Public Property Get DrTPRout() As String
    DrTPRout = m_DrTPRout
End Property

'DRBTROUT
Public Property Let DrBTRout(mVar As String)
    m_DrBTRout = mVar
End Property

Public Property Get DrBTRout() As String
    DrBTRout = m_DrBTRout
End Property

'DRGNROUT
Public Property Let DrGNRout(mVar As String)
    m_DrGNRout = mVar
End Property

Public Property Get DrGNRout() As String
    DrGNRout = m_DrGNRout
End Property

'DRIDOGGETTO
Public Property Let drIdOggetto(mVar As Long)
    m_drIdOggetto = mVar
End Property

Public Property Get drIdOggetto() As Long
    drIdOggetto = m_drIdOggetto
End Property


