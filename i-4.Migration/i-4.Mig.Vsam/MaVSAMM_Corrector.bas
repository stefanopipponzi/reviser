Attribute VB_Name = "MaVSAMM_Corrector"
Option Explicit

Public arrayRighe() As String 'ale 02/05/2006
Public lineeIstr() As String
Public copyExpanded As Boolean

Global AUSelText As String
Global AURigaSel As Long

'AC per verivicare se PCB � cambiato
Global GbPrevPCB As String
Global GbPrevIstr As String
Global GbNomePCB As String
Global GbDefaultLevel As Boolean
' per gestire con un unico form scelta operatori ed istruzioni
Global GbIntFormSel As Integer
'Serve per selezionare una voce sulla lista delle molteplicit�
Global AUMoltSel As String

'SG : Indica l'index in memoria dove � mappata la chiave
Global AUSelIndexKey As Long

'Serve per sapere se nella form devo caricare i segmenti, i qualif ecc...
Global AUKeyMolt As String
        'SEG : Segmenti
        'KEY : Key
        'QLF : Qualificatori
        'OPE : Operatori
        'VAL : Valori

'Serve per sapere su che livello mi trovo nella TreeView
Global AULivello As Long

'Serve per fotografare la situazione delle immagini della treeview prima
'di cominciare a fare tutte le modifiche
Public Mappa_Immaggini_TreeView() As Long

Enum e_DBDType
   DBD_GSAM = 0 ^ 2
   DBD_HDAM = 1 ^ 2
   DBD_HIDAM = 2 ^ 2
   DBD_INDEX = 3 ^ 2
   DBD_UNKNOWED_TYPE = 4 ^ 2
End Enum

'Type per contenere i dati dell'istruzione
''''Enum t_type
''''    TYPE_CBL = 2 ^ 0
''''    TYPE_DBD = 2 ^ 1
''''    TYPE_PSB = 2 ^ 2
''''End Enum

Type tObj
    idOggetto As Long
    nome As String
    Tipo As t_type
End Type

Type SSAIstr
  Livello As Long
  Segmento As String
  SegLen As String
  ssaName As String
  Struttura As String
  key As String
  KeyLen As String
  KeyStart As String
  KeyMask As String
  OPERATORI As String
  Valore As String
  Ccode As String
End Type

Type tl_SSA
   idOggetto As Long
   IdArea As Long
   ssaName As String
   Moved As Boolean
   ssaStream As String
   idSegmento As Long
   NomeSegmento As String
   LenSegmento As Long
   Struttura As String
   Qualificatore As Boolean
   BothQualAndUnqual As Boolean
   Livello As Long
   Chiavi() As t_Key
   CommandCode As String
   OpLogico() As String
   valida As Boolean
End Type

Type Istr
  Istruzione As String
  riga As Long
  Tipologia As String
  MapName As String
  ImsDB As Boolean
  ImsDC As Boolean
  DBD_Type As e_DBDType
  Database() As tObj
  psb() As tObj
  valida As Boolean
  SSA() As tl_SSA
  pcbName As String '2-5-06
  to_migrate As Boolean
  obsolete As Boolean
End Type

Global Istruzione As Istr

'***********************************************************************
'*** indica la molteplicit� dei segmenti di una stessa SSA  ************
'***********************************************************************
Public Type MOLTEPLICITA
    idOggetto As Long
    riga As Long
    Livello As Long
    nome As String
End Type

Global SEGMENTI() As MOLTEPLICITA
Global FIELD() As MOLTEPLICITA
Global FIELDLEN() As MOLTEPLICITA
Global OPERATORI() As MOLTEPLICITA
Global VALORI() As MOLTEPLICITA
Global QUALIFICATORI() As MOLTEPLICITA

'Id dell'istruzione corrente
Public gIdDbd As Long
Public gIdPsb As Long
Public gIdField As Long
Public RTStart As Long
'...

Public Sub Carica_File_In_RichTextBox(LSW As ListView, RT As RichTextBox, idOggetto As Long)
  Dim r As ADODB.Recordset
  Dim Directory As String
  Dim wExt As String
  
  Set r = m_dllFunctions.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & idOggetto)
  
  If r.RecordCount > 0 Then
    
    'E' un record solo
    
    If Trim(r!Estensione) <> "" Then
      wExt = "." & r!Estensione
    Else
      wExt = ""
    End If
    
    Directory = Crea_Directory_Progetto(r!Directory_Input, VSAM2Rdbms.drPathDef) & "\" & r!nome & wExt
    
    LSW.ListItems.Add , , "Loading File " & r!nome & "..."
    LSW.ListItems(LSW.ListItems.Count).Tag = "LOAD" & Directory
    
    'Carica il file nella rich
    RT.LoadFile Directory
    
    'ALE memorizza le righe con gli indici di capo linea e fine linea
    indiciRighe RT
    
    r.Close
  End If
End Sub
Public Sub indiciRighe(RT As RichTextBox)
 'Idx = RT.Find(vbCrLf, OldPos) 'ALE
 'array dalla riga 1 alla fine contenenti gli  indici del carattere capo linea e fine linea
 'es:rt.Find("GDEL",x1,x2)
 Dim Idx As Long
 Dim riga As Long
 Dim x1 As Long
 Dim x2 As Long
 Dim i As Long
 On Error GoTo ErrorHandler
 
  i = 0
  x1 = 1
  ReDim arrayRighe(0)
  Do Until Idx = -1
    Idx = RT.find(vbCrLf, x1)
    x2 = Idx
    'riga = RT.GetLineFromChar(idx)
    If x2 - x1 < 2 Then
        x2 = x1 + 2
            ReDim Preserve arrayRighe(i) 'riga
        arrayRighe(i) = x1 & ";" & x2
            i = i + 1
    x1 = x2
    Else
    ReDim Preserve arrayRighe(i) 'riga
    arrayRighe(i) = x1 & ";" & x2
        i = i + 1
    x1 = x2 + 2
    End If

  Loop
  Exit Sub
ErrorHandler:
Resume Next
  
End Sub



'SG : Spostati in funzioni
''''''Public Function getDBDByIdOggettoRiga(WidOggetto As Long, wRiga As Long) As Long()
''''''    Dim str() As Long
''''''    Dim rs As adodb.Recordset
''''''
''''''    ReDim str(0)
''''''
''''''    Set rs = m_dllFunctions.Open_Recordset("Select * From PsDLI_IstrDBD Where IdOggetto = " & WidOggetto & " And Riga = " & wRiga)
''''''
''''''    If rs.RecordCount > 0 Then
''''''        rs.MoveFirst
''''''
''''''        While Not rs.EOF
''''''            ReDim Preserve str(UBound(str) + 1)
''''''            str(UBound(str)) = rs!IdDBD
''''''
''''''            rs.MoveNext
''''''        Wend
''''''    End If
''''''
''''''    rs.Close
''''''
''''''    getDBDByIdOggettoRiga = str
''''''End Function
''''''
''''''
''''''Public Function getPSBByIdOggettoRiga(WidOggetto As Long, wRiga As Long) As Long()
''''''    Dim str() As Long
''''''    Dim rs As adodb.Recordset
''''''    Dim rsPcb As adodb.Recordset
''''''
''''''    ReDim str(0)
''''''
''''''    Set rs = m_dllFunctions.Open_Recordset("Select * From PsDLI_IstrPSB Where IdOggetto = " & WidOggetto & " And Riga = " & wRiga)
''''''
''''''    If rs.RecordCount > 0 Then
''''''        rs.MoveFirst
''''''
''''''        While Not rs.EOF
''''''            ReDim Preserve str(UBound(str) + 1)
''''''            str(UBound(str)) = rs!IdPSB
''''''
''''''            rs.MoveNext
''''''        Wend
''''''    End If
''''''
''''''    rs.Close
''''''
''''''    getPSBByIdOggettoRiga = str
''''''End Function

Public Sub Carica_ListView_Istruzioni(LSW As ListView, idOggetto As Long, Opzioni As Integer, RT As RichTextBox)
  Dim r As ADODB.Recordset, rs As ADODB.Recordset, rGen As ADODB.Recordset
  Dim Reset As Boolean
  Dim ssaName As String
  Dim ssaIdArea As Long, ssaIdOggetto As Long, ordinale As Integer
  Dim BolExitWhilegen As Boolean, BolMoved As Boolean
  
  LSW.ListItems.Clear
  
  Reset = True
  
  'SQ CPY-ISTR
  If Opzioni = 1 Then  'se � stato selezionato l'option button optAllInstr
    If Not MaVSAMF_Corrector.chkErrInstr.Value = 1 Then
      ' And a.Istruzione <> 'TERM'
      Set r = m_dllFunctions.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE a.idPgm > 0 AND a.IdOggetto = " & idOggetto & "  and  a.idoggetto = b.idoggetto and a.riga = b.riga Order By a.Riga")
    Else
      ' And a.Istruzione <> 'TERM'
      Set r = m_dllFunctions.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE a.idPgm > 0 AND a.IdOggetto = " & idOggetto & " AND NOT VALIDA  and  a.idoggetto = b.idoggetto and a.riga = b.riga Order By a.Riga")
    End If
  ElseIf Opzioni = 2 Then 'se � stato selezionato l'option button IMS/DB
      If Not MaVSAMF_Corrector.chkErrInstr.Value = 1 Then
      ' And a.Istruzione <> 'TERM'
      Set r = m_dllFunctions.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE a.idPgm > 0 AND a.IdOggetto = " & idOggetto & "  and  a.idoggetto = b.idoggetto and a.riga = b.riga and a.ImsDB Order By a.Riga")
    Else
      ' And a.Istruzione <> 'TERM'
      Set r = m_dllFunctions.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE a.idPgm > 0 AND a.IdOggetto = " & idOggetto & " AND NOT VALIDA  and  a.idoggetto = b.idoggetto and a.riga = b.riga and a.ImsDB Order By a.Riga")
    End If
  Else 'Opzioni = 3 'se � stato selezionato l'option button IMS/DC
    If Not MaVSAMF_Corrector.chkErrInstr.Value = 1 Then
    ' And a.Istruzione <> 'TERM'
      Set r = m_dllFunctions.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE a.idPgm > 0 AND a.IdOggetto = " & idOggetto & "  and  a.idoggetto = b.idoggetto and a.riga = b.riga  and not a.ImsDB Order By a.Riga")
    Else
      ' And a.Istruzione <> 'TERM'
      Set r = m_dllFunctions.Open_Recordset("Select a.*, b.linee From PSDLI_Istruzioni as a,PSCall as b WHERE a.idPgm > 0 AND a.IdOggetto = " & idOggetto & " AND NOT VALIDA  and  a.idoggetto = b.idoggetto and a.riga = b.riga  and not a.ImsDB Order By a.Riga")
    End If
  End If
    
    BolExitWhilegen = False
    BolMoved = False
    While Not r.EOF
      Screen.MousePointer = vbHourglass
        
      '***********************  AC  ***************
      BolExitWhilegen = False
      BolMoved = False
      'stefano: un dato in pi�....
      'Set rGen = m_dllFunctions.Open_Recordset("SELECT StrIdOggetto,StrIdArea,NomeSSA FROM PSDli_IstrDett_Liv WHERE IdOggetto = " & r!IdOggetto & " AND Riga = " & r!Riga & " ORDER BY Livello")
      Set rGen = m_dllFunctions.Open_Recordset("SELECT StrIdOggetto,StrIdArea,NomeSSA,moved FROM PSDli_IstrDett_Liv WHERE IdOggetto = " & r!idOggetto & " AND Riga = " & r!riga & " ORDER BY Livello")
      While Not rGen.EOF And Not BolExitWhilegen

        If Not IsNull(rGen!strIdOggetto) Then
            ssaIdOggetto = rGen!strIdOggetto
        End If
          
        ssaName = IIf(IsNull(rGen!NomeSSA), "", rGen!NomeSSA)
          
        If Not IsNull(rGen!Stridarea) Then
          ssaIdArea = rGen!Stridarea
        End If
        
        'stefano: ma perch� non usava direttamente il valore estratto dal parser e messo
        ' nella psdli_istrdett_liv? Visto che non funzionava, per ora ho fatto il giro
        ' pi� semplice
'        Set rs = m_dllFunctions.Open_Recordset("Select Ordinale From PsData_Cmp Where idOggetto = " & ssaIdOggetto & " And IdArea = " & ssaIdArea & " And Nome = '" & Trim(SSAName) & "'")
'        If rs.RecordCount > 0 Then
'          ordinale = rs!ordinale
'        End If
'        rs.Close
'
'        Dim sqlUnion As String
'
'        Set rs = m_dllFunctions.Open_Recordset("SELECT IdCopy from PsData_RelCpy where IdOggetto = " & ssaIdOggetto & " And IdArea = " & ssaIdArea)
'        If Not rs.EOF Then
'
'        sqlUnion = " UNION SELECT 2 as AUX,ordinale,livello,segno,byte,lunghezza,valore,nomeRed,nome From PsData_Cmp " & _
'                                "  where idOggetto = " & rs!idCopy & " And IdArea = " & ssaIdArea & " and Moved order by AUX,ORDINALE"
'        End If
'        rs.Close
'        Set rs = m_dllFunctions.Open_Recordset("SELECT 1 as AUX ,ordinale as ORDINALE,livello,segno,byte,lunghezza,valore,nomeRed,nome From PsData_Cmp Where idOggetto = " & ssaIdOggetto & " And IdArea = " & ssaIdArea & " And ordinale >= " & ordinale & _
'                                " and Moved " & sqlUnion)
'
'
'        If Not rs.EOF Then
'            BolMoved = True
'            BolExitWhilegen = True
'        End If
'        rs.Close
        
        'stefano: semplificato, non so se va bene sempre (parser interno al corrector?)
        If rGen!Moved Then
            BolMoved = True
            'stefano: a che serve questo? Non viene usato praticamente mai...
            BolExitWhilegen = True
        End If
        
        rGen.MoveNext
      Wend
      rGen.Close
       
      '*******************************
      LSW.ListItems.Add , , r!idOggetto
      LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , r!riga
      LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , r!Istruzione & ""
      'LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , r!IdPSB & ""
      LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , r!statement
      
      'ALE
      LSW.ListItems(LSW.ListItems.Count).Tag = r!linee
      'SQ CPY-ISTR (tmp)
      LSW.ListItems(LSW.ListItems.Count).ToolTipText = "IdPgm: " & r!idpgm
      
      If r!valida Then
        If BolMoved Then
          LSW.ListItems(LSW.ListItems.Count).SmallIcon = 19
        Else
          LSW.ListItems(LSW.ListItems.Count).SmallIcon = 8
        End If
      Else
        If BolMoved Then
          LSW.ListItems(LSW.ListItems.Count).SmallIcon = 20
        Else
          LSW.ListItems(LSW.ListItems.Count).SmallIcon = 11
        End If
      End If
      
      'Marca Righe ALE 01/05/2006
      'LSW.ListItems(LSW.ListItems.Count).Tag = Marca_Istruzione_In_RichTextBox(RT, r!Riga, Reset)
      
      r.MoveNext

      Screen.MousePointer = vbDefault
      Reset = False
    Wend
    r.Close

  

End Sub

Public Function Marca_Istruzione_In_RichTextBox(RT As RichTextBox, riga As Long, Reset As Boolean) As String
  Dim Idx As Long
  Dim Start As Long
  Dim PosRiga As Long
  Dim Marcatore As String
  Dim i As Long
  Static Oldpos As Long
 
  Marcatore = "L" & String(5 - Len(Hex(riga)), "0") & Hex(riga)
  
  Marca_Istruzione_In_RichTextBox = Marcatore
  
  If Reset = True Then
    Oldpos = 1
  End If
  
  Idx = RT.find(vbCrLf, Oldpos)
  
  Do While Idx <> -1
    PosRiga = RT.GetLineFromChar(Idx)
    
    If PosRiga = riga - 2 Then
      Start = Idx
      Exit Do
    End If
    
    Idx = RT.find(vbCrLf, Idx + 2)
  Loop
  
  'Va avanti di 2 posizione e seleziona 6 byte
  RT.SelStart = Start + 2
  RT.SelLength = 6
  
  RT.SelText = Marcatore
  
  RT.SelStart = 0
  RT.SelLength = 0
  
End Function

Public Sub Expand_CPY(LSW As ListView, RTApp As RichTextBox, RTSource As RichTextBox)
  Dim RTPosition As Long
  Dim RTFine As Long
  Dim i As Long
  Dim Cpy As Long
  Dim Accapo As Long
  Dim NomeCOPY As String
  Dim FlagStart As Boolean
  Dim CopyPath  As String
  Dim CopyFind As Boolean
  
  Dim MarcatoreCPY As String
  Dim riga As Long
  Dim LenCpy As Long
  Dim Count As Long
  
  FlagStart = False
  LSW.ListItems.Add , , "Expanding Copy..."
  
  'Cerca le copy nella RichTextBox
  Cpy = RTSource.find(" COPY ", 1)
  CopyFind = False
  
  While Cpy <> -1
    NomeCOPY = ""
    
    'Controlla che non sia asteriscato
    For i = 1 To 100
      RTSource.SelStart = Cpy - i
      RTSource.SelLength = 2
      
      If RTSource.SelText = vbCrLf Then
        Accapo = Cpy - i + 1
        Exit For
      End If
    Next i
    
    RTSource.SelStart = Accapo + 6
    RTSource.SelLength = 1
    
    If RTSource.SelText <> "*" Then
      'Trova il nome della Copy
      For i = 1 To 100
        RTSource.SelStart = Cpy + 4 + i
        RTSource.SelLength = 1
        
        If RTSource.SelText <> " " And RTSource.SelText <> "." And RTSource.SelText <> "'" Then
          NomeCOPY = NomeCOPY & RTSource.SelText
          FlagStart = True
        Else
          If FlagStart = True Then
            Exit For
          End If
        End If
      Next i
        
       'Toglie eventuali punti
       If Mid(NomeCOPY, Len(NomeCOPY), 1) = "." Then
         NomeCOPY = Mid(NomeCOPY, 1, Len(NomeCOPY) - 1)
       End If
       
       'Toglie eventuali apici
       If Mid(NomeCOPY, Len(NomeCOPY), 1) = "'" Then
         NomeCOPY = Mid(NomeCOPY, 1, Len(NomeCOPY) - 1)
       End If
       
       If Mid(NomeCOPY, 1, 1) = "'" Then
         NomeCOPY = Mid(NomeCOPY, 2)
       End If
       
       
       'Prende il path della copy
       CopyPath = Restituisci_Percorso_Da_NomeOggetto(NomeCOPY)
       
       riga = RTSource.GetLineFromChar(Cpy)
       
       
       
       MarcatoreCPY = "C" & String(5 - Len(Hex(riga)), "0") & Hex(riga)
       
       If Trim(CopyPath) <> "" Then
         'Espande la copy :
          'CARICA LA COPY NELLA TEXT DI APPOGGIO
          RTApp.LoadFile CopyPath
          
          LenCpy = Len(RTApp.text)
          
          'trova RTPosition
          For i = 1 To 100
             RTSource.SelStart = Cpy + i
             RTSource.SelLength = 2
             
             If RTSource.SelText = vbCrLf Then
               RTPosition = Cpy + i + 2
               Exit For
             End If
          Next i
          
          'Copia la copy nella RichTextBox principale
          RTSource.SelStart = RTPosition
          RTSource.SelLength = 0
          
          RTSource.SelColor = vbWhite
          RTSource.SelText = MarcatoreCPY & vbCrLf & RTApp.text & vbCrLf & "CPYFN" & vbCrLf
          
          
          RTFine = RTPosition + Len(RTApp.text) + 11
          
          'aggiorna linee istruzioni ALE
          Dim rigaIn As Long
          Dim rigaOut As Long
          rigaIn = riga 'RTSource.GetLineFromChar(RTPosition)
          rigaOut = RTSource.GetLineFromChar(RTFine)
          aggiornaLineeIstruz rigaIn, rigaOut
          
          RTSource.SelStart = RTPosition
          RTSource.SelLength = 0
          RTSource.SelColor = vbWhite
          
          LSW.ListItems.Add , , "Copy " & NomeCOPY & " Expanded..."
          LSW.ListItems(LSW.ListItems.Count).Tag = "EXPD" & MarcatoreCPY & CopyPath
          
          CopyFind = True
          
      Else
        If NomeCOPY <> ":" And NomeCOPY <> "=" And NomeCOPY <> " " Then
          LSW.ListItems.Add , , "Copy " & NomeCOPY & " Not Found..."
          LSW.ListItems(LSW.ListItems.Count).Tag = "EXPE" & MarcatoreCPY & NomeCOPY
        End If
      End If
    Else
      LenCpy = 0
    End If
    
    FlagStart = False
    
    Cpy = RTSource.find(" COPY ", LenCpy + 14 + Cpy + 6)
    
    Count = Count + 1
  Wend
  
  If CopyFind = False Then
    LSW.ListItems.Add , , "No Copy Find to expand..."
  End If

End Sub

Public Sub aggiornaLineeIstruz(pIn As Long, pOut As Long)
Dim i As Long
  For i = 1 To UBound(lineeIstr)
    If lineeIstr(i) > pIn Then
      lineeIstr(i) = lineeIstr(i) + (pOut - pIn)
    End If
  Next
End Sub


Public Function Carica_TreeView_Dettaglio_Istruzioni(TR As TreeView, riga As Long) As Boolean
  'Oltre a caricare la treeview ritorna un valore booleano che indica se l'istruzione � valida o meno
  Dim i As Long
  Dim wApp As String
  Dim j As Long
  Dim sKey As String, Kiave As String, grpKey As String, ParKey As String, grpKey2 As String
  
  Dim ImgIcon As Long
    'ImgIcon = 11 : Errore
    'ImgIcon = 8  : OK
    'ImgIcon = 2  : SSA(n)
    'ImgIcon = 12 : Molteplicit�
    
  On Error GoTo ErrorHandler

  'Carica la prima parte dell'array DBD,PSB ecc...
  TR.Nodes.Clear
  TR.Nodes.Add , , "ISTR ", "Istruction Information :", 14
  TR.Nodes.Add "ISTR ", tvwChild, , "Object : " & Trim(Restituisci_NomeOgg_Da_IdOggetto(VSAM2Rdbms.drIdOggetto)), 15
  TR.Nodes.Add "ISTR ", tvwChild, , "Istruction : " & Trim(Istruzione.Tipologia), 16
  
  If Istruzione.ImsDB Then
      TR.Nodes.Add "ISTR ", tvwChild, , "Istruction Type : IMS/DB", 18
  ElseIf Istruzione.ImsDC Then
      TR.Nodes.Add "ISTR ", tvwChild, , "Istruction Type : IMS/DC", 18
  Else
      TR.Nodes.Add "ISTR ", tvwChild, , "Istruction Type : UNKNOWED", 11
  End If
  
  TR.Nodes.Add "ISTR ", tvwChild, , "Line : " & Trim(Str(Istruzione.riga)), 13
 
  Select Case Trim(UCase(Istruzione.Tipologia))
    Case "TERM", "PCB", "ROLL", "ROLB", "SYNC"
      '
    Case Else
      If UBound(Istruzione.Database) > 0 Then
        For i = 1 To UBound(Istruzione.Database)
          wApp = wApp & ";" & Istruzione.Database(i).nome
        Next i
        TR.Nodes.Add "ISTR ", tvwChild, "DBD  ", "DBD : " & Mid(wApp, 2), 3
      Else
        If Istruzione.ImsDB = True Then
         TR.Nodes.Add "ISTR ", tvwChild, "DBD  ", "DBD : ", 11
        End If
      End If
  End Select
  
  If UBound(Istruzione.psb) > 0 Then
    wApp = ""
    For i = 1 To UBound(Istruzione.psb)
      wApp = wApp & ";" & Istruzione.psb(i).nome
    Next i
    TR.Nodes.Add "ISTR ", tvwChild, "PSB  ", "PSB : " & Mid(wApp, 2), 3
  Else
     TR.Nodes.Add "ISTR ", tvwChild, "PSB  ", "PSB : ", 11
  End If
  
  Select Case Trim(UCase(Istruzione.Tipologia))
    Case "TERM", "PCB", "ROLL", "ROLB", "OPEN", "CLSE", "REST", "SYNC"
      Exit Function
  End Select
  
  If UBound(Istruzione.SSA) Then
  For i = 1 To UBound(Istruzione.SSA)
    ParKey = "LEV" & Format(i, "00")

    '***********************
    '**** L I V E L L I ****
    '***********************
    TR.Nodes.Add , , ParKey, "LEVEL : " & Format(Istruzione.SSA(i).Livello, "00"), 2
    
    If Istruzione.ImsDB = True Then
       '*************************
       '**** S E G M E N T O ****
       '*************************
       Kiave = "SEG" & Format(i, "00")
       If Trim(Istruzione.SSA(i).NomeSegmento) <> "" Then
         ImgIcon = 8
       Else
         ImgIcon = 11
       End If
   
       TR.Nodes.Add ParKey, tvwChild, Kiave, "Segment : " & Istruzione.SSA(i).NomeSegmento, ImgIcon
       
       '*********************
       '**** S E G L E N ****
       '*********************
       Kiave = "SGL" & Format(i, "00")
       If Trim(Istruzione.SSA(i).LenSegmento) <> "" Then
         ImgIcon = 8
       Else
         ImgIcon = 11
       End If
       
       TR.Nodes.Add ParKey, tvwChild, Kiave, "Segment Len: " & Istruzione.SSA(i).LenSegmento, ImgIcon
    End If
    
    '***************************
    '**** S T R U T T U R A ****
    '***************************
    If Trim(Istruzione.SSA(i).Struttura) <> "" Then
      ImgIcon = 8
    Else
      ImgIcon = 11
    End If
    
    '*******************************
    Dim rs As ADODB.Recordset
    Dim ordinale As Integer
    Dim BolMoved As Boolean
  
   BolMoved = False
  'SQ - 13-12-05: gestione strutture tipo:
  '01 PIPPO.
  '   COPY BLUTO.
  'N.B.: l'ordinale di BLUTO parte da 1, come il livello 01... aggiungo AUX nella select per fare un ordinamento corretto... (order by UNICO sulla UNION)
  'Set rs = m_Fun.Open_Recordset("Select * From PsData_Cmp Where idOggetto = " & wIdOggetto & " And IdArea = " & wIdArea & " And ordinale >= " & ordinale & " order by Ordinale")
  
   'stefano: per ora lo prendo dal moved caricato: perch� no?
'    Set rs = m_dllFunctions.Open_Recordset("Select Ordinale From PsData_Cmp Where idOggetto = " & Istruzione.SSA(i).IdOggetto & " And IdArea = " & Istruzione.SSA(i).IdArea & " And Nome = '" & Trim(Istruzione.SSA(i).SSAName) & "'")
'    If rs.RecordCount > 0 Then
'      ordinale = rs!ordinale
'    End If
'    rs.Close
'
'
'    Dim sqlUnion As String
'
'        Set rs = m_dllFunctions.Open_Recordset("SELECT IdCopy from PsData_RelCpy where IdOggetto = " & Istruzione.SSA(i).IdOggetto & " And IdArea = " & Istruzione.SSA(i).IdArea)
'        If Not rs.EOF Then
'
'        sqlUnion = " UNION SELECT 2 as AUX,ordinale,livello,segno,byte,lunghezza,valore,nomeRed,nome From PsData_Cmp " & _
'                                "  where idOggetto = " & rs!idCopy & " And IdArea = " & Istruzione.SSA(i).IdArea & " and Moved order by AUX,ORDINALE"
'        End If
'        rs.Close
'        Set rs = m_dllFunctions.Open_Recordset("SELECT 1 as AUX ,ordinale as ORDINALE,livello,segno,byte,lunghezza,valore,nomeRed,nome From PsData_Cmp Where idOggetto = " & Istruzione.SSA(i).IdOggetto & " And IdArea = " & Istruzione.SSA(i).IdArea & " And ordinale >= " & ordinale & _
'                                " and Moved " & sqlUnion)
'    If Not rs.EOF Then
'         BolMoved = True
'    End If
'    rs.Close
'stefano: lo carico da quello del db, serve a poco, ma comunque....
    BolMoved = Istruzione.SSA(i).Moved
    
    '*******************************
    
    Kiave = "STR" & Format(i, "00")
    TR.Nodes.Add ParKey, tvwChild, Kiave, "Data Area : " & Istruzione.SSA(i).Struttura, ImgIcon
    Kiave = "MAP" & Format(i, "00")
    If Istruzione.ImsDC And Istruzione.Tipologia = "ISRT" Then
        TR.Nodes.Add ParKey, tvwChild, Kiave, "MapName : " & Istruzione.MapName, ImgIcon
    End If
    '*************************
    '**** S S A   N A M E ****
    '*************************
    Kiave = "SSA" & Format(i, "00")

    If Trim(Istruzione.SSA(i).ssaName) <> "" Then
      If Not BolMoved Then
        ImgIcon = 8
      Else
        ImgIcon = 19
      End If
    Else
      If Not BolMoved Then
        ImgIcon = 11
      Else
        ImgIcon = 20
      End If
    End If
    
    If Trim(UCase(Istruzione.SSA(i).ssaName)) <> "" Then
       TR.Nodes.Add ParKey, tvwChild, Kiave, "SSA Name : " & Istruzione.SSA(i).ssaName, ImgIcon
      If MaVSAMF_Corrector.ChkQual.Value Then
         If UBound(Istruzione.SSA(i).Chiavi) > 1 Then
           'Crea il nodo contenete le chiavi dell'SSA
           grpKey = Kiave
           
           For j = 1 To UBound(Istruzione.SSA(i).Chiavi)
              '*********************
              '**** C H I A V I ****
              '*********************
              Kiave = "KEY" & Format(j, "00")
              
              If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                ImgIcon = 8
              Else
                ImgIcon = 11
              End If
              
              sKey = Istruzione.SSA(i).Chiavi(j).key
              
              
              TR.Nodes.Add grpKey, tvwChild, Kiave, "Key : " & sKey, ImgIcon
              TR.Nodes(TR.Nodes.Count).Tag = j
              
              grpKey2 = Kiave
              
              '*************************
              '**** K E Y S T A R T ****
              '*************************
               Kiave = "KYS" & Format(j, "00")
           
               If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                 ImgIcon = 8
               Else
                 ImgIcon = 11
               End If
               
               'SG : Concatena nella treeview le chiavi se sono + di una
               sKey = Istruzione.SSA(i).Chiavi(j).Valore.KeyStart
               
               TR.Nodes.Add grpKey2, tvwChild, Kiave, "Key Start : " & sKey, ImgIcon
               TR.Nodes(TR.Nodes.Count).Tag = j
               
               '*********************
               '**** K E Y L E N ****
               '*********************
               Kiave = "KYL" & Format(j, "00")
           
               If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                 ImgIcon = 8
               Else
                 ImgIcon = 11
               End If
               
               'SG : Concatena nella treeview le chiavi se sono + di una
               sKey = Istruzione.SSA(i).Chiavi(j).Valore.Lunghezza
               
               TR.Nodes.Add grpKey2, tvwChild, Kiave, "Key Len : " & sKey, ImgIcon
               TR.Nodes(TR.Nodes.Count).Tag = j
               
               '***************************
               '**** O P E R A T O R I ****
               '***************************
               Kiave = "OPE" & Format(j, "00")
           
               If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                 ImgIcon = 8
               Else
                 ImgIcon = 11
               End If
               
               'SG : Concatena nella treeview le chiavi se sono + di una
               sKey = Istruzione.SSA(i).Chiavi(j).Operatore
               
               TR.Nodes.Add grpKey2, tvwChild, Kiave, "Operators : " & sKey, ImgIcon
               TR.Nodes(TR.Nodes.Count).Tag = j
               
               '*********************
               '**** V A L O R I ****
               '*********************
               Kiave = "VAL" & Format(j, "00")
           
               If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                 ImgIcon = 8
               Else
                 ImgIcon = 11
               End If
               
                'SG : Concatena nella treeview le chiavi se sono + di una
               sKey = Trim(Istruzione.SSA(i).Chiavi(j).Valore.Valore)
               
               TR.Nodes.Add grpKey2, tvwChild, Kiave, "Key Value: " & sKey, ImgIcon
               TR.Nodes(TR.Nodes.Count).Tag = j
               
               '*********************
               '**** V A L K E Y ****
               '*********************
               Kiave = "KYV" & Format(j, "00")
           
               If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                 ImgIcon = 8
               Else
                 ImgIcon = 11
               End If
               
                'SG : Concatena nella treeview le chiavi se sono + di una
               sKey = Trim(Istruzione.SSA(i).Chiavi(j).Valore.fieldValue)
               
               TR.Nodes.Add grpKey2, tvwChild, Kiave, "Value : " & sKey, ImgIcon
               TR.Nodes(TR.Nodes.Count).Tag = j
               
              '*************************
              '**** O P L O G I C O ****
              '*************************
              If UBound(Istruzione.SSA(i).OpLogico) >= j Then
                 Kiave = "OPL" & Format(i, "00")
                 
                 If UBound(Istruzione.SSA(i).OpLogico) > 0 Then
                    ImgIcon = 8
                 Else
                    ImgIcon = 11
                 End If
                  
                 'SG : Concatena gli operatori
                 sKey = Trim(Istruzione.SSA(i).OpLogico(j))
                 
                 TR.Nodes.Add grpKey, tvwChild, Kiave, "Logic Op : " & sKey, ImgIcon
                 TR.Nodes(TR.Nodes.Count).Tag = j
              End If
           Next j
         Else
           If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
              j = 1
              grpKey = Kiave
              
              'C'� una sola chiave (non booleana)
              '*********************
              '**** C H I A V I ****
              '*********************
              Kiave = "KEY" & Format(j, "00") & Format(i, "00")
              
              If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                ImgIcon = 8
              Else
                ImgIcon = 11
              End If
              
              sKey = Istruzione.SSA(i).Chiavi(j).key
              
              TR.Nodes.Add grpKey, tvwChild, Kiave, "Key : " & sKey, ImgIcon
              TR.Nodes(TR.Nodes.Count).Tag = j
              
              grpKey2 = Kiave
              
              '*************************
              '**** K E Y S T A R T ****
              '*************************
               Kiave = "KYS" & Format(j, "00") & Format(i, "00")
           
               If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                 ImgIcon = 8
               Else
                 ImgIcon = 11
               End If
               
               'SG : Concatena nella treeview le chiavi se sono + di una
               sKey = Istruzione.SSA(i).Chiavi(j).Valore.KeyStart
               
               TR.Nodes.Add grpKey, tvwChild, Kiave, "Key Start : " & sKey, ImgIcon
               TR.Nodes(TR.Nodes.Count).Tag = j
               
               '*********************
               '**** K E Y L E N ****
               '*********************
               Kiave = "KYL" & Format(j, "00") & Format(i, "00")
           
               If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                 ImgIcon = 8
               Else
                 ImgIcon = 11
               End If
               
               'SG : Concatena nella treeview le chiavi se sono + di una
               sKey = Istruzione.SSA(i).Chiavi(j).Valore.Lunghezza
               
               TR.Nodes.Add grpKey, tvwChild, Kiave, "Key Len : " & sKey, ImgIcon
               TR.Nodes(TR.Nodes.Count).Tag = j
               
               '***************************
               '**** O P E R A T O R I ****
               '***************************
               Kiave = "OPE" & Format(j, "00") & Format(i, "00")
           
               If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                 ImgIcon = 8
               Else
                 ImgIcon = 11
               End If
               
               'SG : Concatena nella treeview le chiavi se sono + di una
               sKey = Istruzione.SSA(i).Chiavi(j).Operatore
               
               TR.Nodes.Add grpKey, tvwChild, Kiave, "Operators : " & sKey, ImgIcon
               TR.Nodes(TR.Nodes.Count).Tag = j
               
               '*********************
               '**** V A L O R I ****
               '*********************
               Kiave = "VAL" & Format(j, "00") & Format(i, "00")
           
               If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                 ImgIcon = 8
               Else
                 ImgIcon = 11
               End If
               
                'SG : Concatena nella treeview le chiavi se sono + di una
               sKey = Trim(Istruzione.SSA(i).Chiavi(j).Valore.Valore)
               
               TR.Nodes.Add grpKey, tvwChild, Kiave, "Key Value: " & sKey, ImgIcon
               TR.Nodes(TR.Nodes.Count).Tag = j
               
               '*********************
               '**** V A L K E Y ****
               '*********************
               Kiave = "KYV" & Format(j, "00") & Format(i, "00")
           
               If UBound(Istruzione.SSA(i).Chiavi) > 0 Then
                 ImgIcon = 8
               Else
                 ImgIcon = 11
               End If
               
                'SG : Concatena nella treeview le chiavi se sono + di una
               ''''''sKey = Trim(Istruzione.SSA(i).Chiavi(j).Valore.FieldValue)
               ''''''TR.Nodes.Add grpKey, tvwChild, Kiave, "Value: " & sKey, ImgIcon
               ''''''TR.Nodes(TR.Nodes.Count).Tag = j
           End If
         End If  ' ChkQual
       End If
    End If 'SSA = <NONE>
    
    '*****************************
    '**** C O N D I Z I O N E ****
    '*****************************
    Kiave = "CON" & Format(i, "00")

   ' If Trim(Istruzione.SSA(i).CommandCode) <> "" Then
      ImgIcon = 8
    'Else
   '   ImgIcon = 11
   ' End If

    TR.Nodes.Add ParKey, tvwChild, Kiave, "Comm. Codes: " & Istruzione.SSA(i).CommandCode, ImgIcon
  Next i
  End If
  
  'Carica l'array delle immagini
  ReDim Mappa_Immaggini_TreeView(0)

  For i = 1 To TR.Nodes.Count
    ReDim Preserve Mappa_Immaggini_TreeView(UBound(Mappa_Immaggini_TreeView) + 1)
    Mappa_Immaggini_TreeView(UBound(Mappa_Immaggini_TreeView)) = TR.Nodes(i).Image
  Next i

  'Restituisce il valore
  Carica_TreeView_Dettaglio_Istruzioni = Istruzione.valida
Exit Function
ErrorHandler:
  If Err.Number = 35602 Then
    Kiave = Kiave & Format(j, "00")
    Resume
  End If
End Function

Public Sub Controlla_Moltiplicita_SSA_Di_Livello(Id As Variant, riga As Long)
    'verr� inserita nella routine che si lancia quando si clicca sui vari livelli
    Dim r As Recordset

    '***********************
    '*** S E G M E N T I ***
    '***********************
    Set r = m_dllFunctions.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'SG' AND Riga = " & riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst
      
      ReDim SEGMENTI(0)
      
      If r.RecordCount > 1 Then
        While Not r.EOF
          'Carica la type dei Segmenti
          ReDim Preserve SEGMENTI(UBound(SEGMENTI) + 1)
          SEGMENTI(UBound(SEGMENTI)).idOggetto = r!idOggetto
          SEGMENTI(UBound(SEGMENTI)).riga = r!riga
          SEGMENTI(UBound(SEGMENTI)).Livello = r!Livello
          SEGMENTI(UBound(SEGMENTI)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
 
    '***********************
    '*** F I E L D (KEY) ***
    '***********************
    Set r = m_dllFunctions.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'FL' AND Riga = " & riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst
      
      ReDim FIELD(0)
      
      If r.RecordCount > 1 Then
        While Not r.EOF
          'carica la type
          ReDim Preserve FIELD(UBound(FIELD) + 1)
          FIELD(UBound(FIELD)).idOggetto = r!idOggetto
          FIELD(UBound(FIELD)).riga = r!riga
          FIELD(UBound(FIELD)).Livello = r!Livello
          FIELD(UBound(FIELD)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
    

    '********************************
    '*** F I E L D L E N (KEYLEN) ***
    '********************************
    Set r = m_dllFunctions.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'FN' AND Riga = " & riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst
      
      ReDim FIELDLEN(0)
      
      If r.RecordCount > 1 Then
        While Not r.EOF
          'carica la type
          ReDim Preserve FIELDLEN(UBound(FIELDLEN) + 1)
          FIELDLEN(UBound(FIELDLEN)).idOggetto = r!idOggetto
          FIELDLEN(UBound(FIELDLEN)).riga = r!riga
          FIELDLEN(UBound(FIELDLEN)).Livello = r!Livello
          FIELDLEN(UBound(FIELDLEN)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
    
    '*************************
    '*** O P E R A T O R I ***
    '*************************
    Set r = m_dllFunctions.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'OP' AND Riga = " & riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst

      If r.RecordCount > 1 Then
        ReDim OPERATORI(0)
        
        While Not r.EOF
          'carica la type
          ReDim Preserve OPERATORI(UBound(OPERATORI) + 1)
          OPERATORI(UBound(OPERATORI)).idOggetto = r!idOggetto
          OPERATORI(UBound(OPERATORI)).riga = r!riga
          OPERATORI(UBound(OPERATORI)).Livello = r!Livello
          OPERATORI(UBound(OPERATORI)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
    
    '*********************************
    '*** Q U A L I F I C A T O R I ***
    '*********************************
    Set r = m_dllFunctions.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'PA' AND Riga = " & riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst

      If r.RecordCount > 1 Then
        ReDim QUALIFICATORI(0)
        While Not r.EOF
          'carica la type
          ReDim Preserve QUALIFICATORI(UBound(QUALIFICATORI) + 1)
          QUALIFICATORI(UBound(QUALIFICATORI)).idOggetto = r!idOggetto
          QUALIFICATORI(UBound(QUALIFICATORI)).riga = r!riga
          QUALIFICATORI(UBound(QUALIFICATORI)).Livello = r!Livello
          QUALIFICATORI(UBound(QUALIFICATORI)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
    
    '*******************
    '*** V A L O R I ***
    '*******************
    Set r = m_dllFunctions.Open_Recordset("SELECT * FROM PSDli_DettParIstr WHERE idoggetto = " & Id & " AND Tipo = 'VL' AND Riga = " & riga)
    
    If r.RecordCount > 0 Then
      r.MoveLast
      r.MoveFirst

      If r.RecordCount > 1 Then
        ReDim VALORI(0)
        While Not r.EOF
          'carica la type
          ReDim Preserve VALORI(UBound(VALORI) + 1)
          VALORI(UBound(VALORI)).idOggetto = r!idOggetto
          VALORI(UBound(VALORI)).riga = r!riga
          VALORI(UBound(VALORI)).Livello = r!Livello
          VALORI(UBound(VALORI)).nome = r!Valore
          
          r.MoveNext
        Wend
      End If
      
      r.Close
    End If
End Sub

Public Sub Seleziona_Word_E_MarcaWord(RT As RichTextBox, LSW As ListView)
  Dim Marcatore As String
  Dim i As Long
  Dim riga As Long
  
  Dim LenWord As Long
  
  If Trim(RT.SelText) <> "" Then
    AUSelText = Trim(RT.SelText)
    
    LenWord = RT.SelLength
  
    riga = RT.GetLineFromChar(RT.SelStart)
    AURigaSel = riga
    Marcatore = "S" & String(5 - Len(Hex(riga)), "0") & Hex(riga)
    
    'LSW.ListItems.Add , , "Select Text : [" & AUSelText & "]"
    LSW.ListItems(LSW.ListItems.Count).Tag = "SELT" & Marcatore
    
    Call Marca_Linea_Da_Posizione(RT, RT.SelStart, Marcatore, LenWord)
    
    LSW.ListItems(LSW.ListItems.Count).EnsureVisible
  End If
End Sub

Public Sub Avvia_Ricerca_Ricorsiva_Parola(RT As RichTextBox, LSW As ListView, Parola As String)
  Dim Idx As Long, IdxFine As Long
  Dim Marcatore As String, text As String
  Dim riga As Long
  Dim LenWord As Long, LenRiga As Long
  
  Idx = RT.find(Parola, 1)
  LSW.ListItems.Add , , "Select Text : [" & AUSelText & "]"
  
  While Idx <> -1
    riga = RT.GetLineFromChar(Idx)
    Marcatore = "S" & String(5 - Len(Hex(riga)), "0") & Hex(riga)
    
    'MF 02/08/07
    'Visualizza l'intera riga in cui viene trovata la Parola cercata
   
    LenWord = Len(Parola)
    
    If riga <> AURigaSel Then
      IdxFine = RT.find(vbCrLf, Idx)
      Call Marca_Linea_Da_Posizione(RT, Idx, Marcatore, LenWord)
      
      RT.SelStart = RTStart
      RT.SelLength = IdxFine - RTStart
      LSW.ListItems.Add , , "Line " & riga & " : " & Trim(RT.SelText)
      LSW.ListItems(LSW.ListItems.Count).Tag = "FIND" & Marcatore
    End If
    
    Idx = RT.find(Parola, Idx + Len(Parola))
  Wend

  LSW.ListItems(LSW.ListItems.Count).EnsureVisible
End Sub

Public Sub Marca_Linea_Da_Posizione(RT As RichTextBox, pos As Long, Marcatore As String, LenWord As Long)
  Dim i As Long, j As Long
  
  For i = 1 To 1000
    If pos < i Then Exit Sub
    
    RT.SelStart = pos - i
    RT.SelLength = 2
    
    If RT.SelText = vbCrLf Then
      RT.SelStart = pos - i + 1
      RT.SelLength = 6
      RT.SelText = Marcatore
      Exit For
    End If
  Next i
  
  RTStart = RT.SelStart
  
  RT.SelStart = pos
  RT.SelLength = LenWord
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ex Carica_Array_Istruzione - NON ERA USATO COME ARRAY!!!!!!!!!!!!!!!!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub Carica_Istruzione(riga As Long, idOggetto As Long)
  Dim i As Long, K As Long, j As Long, w As Long, y As Long, Q As Long
  Dim r As Recordset, rGen As Recordset, rAll As Recordset, rSeg As Recordset
  Dim rQualif As Recordset, rStrutt As Recordset, rKey As Recordset, rOp As Recordset
  Dim rVal As Recordset, rCond As Recordset
  
  Dim TotLivelli As Long
  
  Dim NomeSegm As String
  Dim LungSegmento As String
  Dim NomeStr As String
  Dim sDbd() As Long
  Dim sPsb() As Long
  
  ReDim Istruzione.SSA(0)
  
  i = 1 'Contatore SSA
  y = 1 'Contatore Struttura
  
  'Prende le informazioni base dell'istruzione
  Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_Istruzioni Where IdOggetto = " & idOggetto & " And Riga = " & riga)
  If r.RecordCount Then
    ReDim Preserve Istruzione.SSA(0)
    Istruzione.Istruzione = r!statement
    Istruzione.Tipologia = IIf(IsNull(r!Istruzione), "", r!Istruzione)
    '2-5-06
    Istruzione.pcbName = IIf(IsNull(r!NumPCB), "", r!NumPCB)
    If Not IsNull(r!MapName) Then
        Istruzione.MapName = r!MapName
    Else
        Istruzione.MapName = ""
    End If
    Istruzione.riga = riga
    Istruzione.valida = r!valida
    Istruzione.to_migrate = r!to_migrate
    Istruzione.obsolete = r!obsolete
    
    sDbd = m_dllFunctions.getDBDByIdOggettoRiga(r!idOggetto, r!riga)
   
    For w = 0 To UBound(sDbd)
      ReDim Preserve Istruzione.Database(w)
      Istruzione.Database(w).idOggetto = sDbd(w)
      Istruzione.Database(w).nome = Restituisci_NomeOgg_Da_IdOggetto(sDbd(w))
      Istruzione.Database(w).Tipo = TYPE_DBD
    Next w
    
    'Setta l'id database univoco
    If UBound(sDbd) = 1 Then gIdDbd = sDbd(1)
    
    sPsb = m_dllFunctions.getPSBByIdOggettoRiga(r!idOggetto, r!riga)
     
    For w = 0 To UBound(sPsb)
      ReDim Preserve Istruzione.psb(w)
      Istruzione.psb(w).idOggetto = sPsb(w)
      Istruzione.psb(w).nome = Restituisci_NomeOgg_Da_IdOggetto(sPsb(w))
      Istruzione.psb(w).Tipo = TYPE_PSB
    Next w
    
    Istruzione.ImsDB = r!ImsDB
    Istruzione.ImsDC = r!ImsDC
    
    'AC
    MaVSAMF_Corrector.TxtPCB.text = IIf(IsNull(r!ordPcb), "", (r!ordPcb))
    MaVSAMF_Corrector.TxtInstr.text = IIf(IsNull(r!Istruzione), "", r!Istruzione)
    
    GbPrevPCB = MaVSAMF_Corrector.TxtPCB.text
    GbPrevIstr = FirstElement(MaVSAMF_Corrector.TxtInstr.text)
    r.Close
  End If
  
    ' AC carica anche Isruzioni da tabella clone
    Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_Istruzioni_Clone Where IdOggetto = " & idOggetto & " And Riga = " & riga & " and  Istruzione is not Null  ")
    While Not r.EOF
      MaVSAMF_Corrector.TxtInstr.text = MaVSAMF_Corrector.TxtInstr.text & ";" & r!Istruzione
      r.MoveNext
    Wend
    r.Close
    
    'Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_Istruzioni_Clone Where IdOggetto = " & IdOggetto & " And Riga = " & Riga & " and OrdPCB is not Null ")
    
    'While Not r.EOF
     '  MaVSAMF_Corrector.TxtPCB.Text = MaVSAMF_Corrector.TxtPCB.Text & ";" & r!OrdPCB
     '  r.MoveNext
    'Wend
    'r.Close
     
  'Carica i vari livelli di SSA
  Set rGen = m_dllFunctions.Open_Recordset("SELECT Distinct IdOggetto,Riga,Livello FROM PSDli_IstrDett_Liv WHERE IdOggetto = " & idOggetto & " AND Riga = " & riga & " ORDER BY Livello")
  
  If rGen.RecordCount > 0 Then
    
    
    'Prende il numero di livelli
    TotLivelli = rGen.RecordCount
    
    While Not rGen.EOF
      ReDim Preserve Istruzione.SSA(i)
      Istruzione.SSA(i).Livello = rGen!Livello
      
      Set rAll = m_dllFunctions.Open_Recordset("Select * From PsDli_IstrDett_Liv Where IdOggetto = " & rGen!idOggetto & " And Riga = " & rGen!riga & " And Livello = " & rGen!Livello)
      If rAll.RecordCount > 0 Then
        If rAll.RecordCount > 1 Then
            m_dllFunctions.WriteLog "Carica_Array_Istruzione", "DR - Auto"
        End If
        
        While Not rAll.EOF
          j = 1 'Contatore Key
          K = 1 'Contatore Segmento
          w = 1 'Contatore Qualificatore
          Q = 1 'Contatore Valori
          
          If Not IsNull(rAll!strIdOggetto) Then
            Istruzione.SSA(i).idOggetto = rAll!strIdOggetto
          End If
          
          Istruzione.SSA(i).ssaName = IIf(IsNull(rAll!NomeSSA), "", rAll!NomeSSA)
          
          If Not IsNull(rAll!Stridarea) Then
            Istruzione.SSA(i).IdArea = rAll!Stridarea
          End If
          
          '***********************
          '*** S E G M E N T O ***
          '***********************
          If Not IsNull(rAll!idSegmento) Then
            Istruzione.SSA(i).NomeSegmento = Restituisci_Dati_Segmento_Da_IdSegm(rAll!idSegmento, "NOME")
            Istruzione.SSA(i).idSegmento = rAll!idSegmento
            '*** S E G L E N ***
            Istruzione.SSA(i).LenSegmento = rAll!SegLen
          End If
          '*************************
          '*** S T R U T T U R A ***
          '*************************
          Istruzione.SSA(i).Struttura = rAll!dataarea & ""
          '******************************
          '*** C O M M A N D   C O D E ***
          '******************************
          Istruzione.SSA(i).CommandCode = rAll!condizione & ""
          
          Istruzione.SSA(i).Qualificatore = rAll!Qualificazione
          
          Istruzione.SSA(i).BothQualAndUnqual = rAll!QualAndUnqual
          
    'stefano: visto che ce l'ho.....
          Istruzione.SSA(i).Moved = rAll!Moved
          
          '*************
          '*** K E Y ***
          '*************
          Carica_Istruzione_KEY rAll!idOggetto, rAll!riga, rAll!Livello, i
          '*************
          '*** ORD PCB ***
          '*************
          rAll.MoveNext
        Wend
        
        rAll.Close
      End If
      
      '**********************************
      '*****  F  I  N  E  ***************
      '**********************************
      i = i + 1
      rGen.MoveNext
    Wend
    
    rGen.Close
  End If
End Sub

Public Function Restituisci_DatiKey(IdField As Long, xName As Boolean, TipoRestituito As String) As Variant
  Dim r As ADODB.Recordset
  
  If IdField < 0 Then
      Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_XDField Where IdXDField = " & Abs(IdField))
  Else
      Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_Field Where IdField = " & IdField)
  End If
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    Select Case Trim(UCase(TipoRestituito))
      Case "NOME"
        If xName = False Then
          Restituisci_DatiKey = r!nome
        Else
          Restituisci_DatiKey = r!ptr_xNome
        End If
      
      Case "LUNGHEZZA"
        Restituisci_DatiKey = r!Lunghezza
    End Select
    
    r.Close
  Else
         If TipoRestituito = "LUNGHEZZA" Then Restituisci_DatiKey = 0
         If TipoRestituito = "NOME" Then Restituisci_DatiKey = ""
  End If
End Function

Public Sub Associa_Dato_In_TreeView(TR As TreeView, Livello As Long, key As String, Dato As String)
  Dim i As Long
  Dim Start As Long
  Dim Fin As Long
  
  Select Case Trim(UCase(key))
    Case "DBD"
      For i = 1 To TR.Nodes.Count
        If Trim(UCase(TR.Nodes(i).key)) = Trim(UCase(key)) Then
          TR.Nodes(i).text = "DBD : " & Dato
          
          Exit For
        End If
      Next i
      
    Case "PSB"
      For i = 1 To TR.Nodes.Count
        If Trim(UCase(TR.Nodes(i).key)) = Trim(UCase(key)) Then
          TR.Nodes(i).text = "PSB : " & Dato
          
          Exit For
        End If
      Next i
      
    
    Case Else
      If TR.Nodes.Count = 0 Then Exit Sub
      
      Start = 0
      For i = 1 To TR.Nodes.Count
        If Val(Mid(TR.Nodes(i).key, 4)) = Livello Then
          Start = i
          Exit For
        End If
      Next i
      
      Fin = 0
      For i = 1 To TR.Nodes.Count
        If Mid(TR.Nodes(i).key, 1, 3) = "LEV" Then
         If Val(Mid(TR.Nodes(i).key, 4)) > Livello Then
           Fin = i
           Exit For
         End If
        End If
        
      Next i
      
      If Fin = 0 Then Fin = i
      
      If Fin > TR.Nodes.Count Then Fin = TR.Nodes.Count
      
      If Start = 0 Then Start = 1
      
      For i = Start To Fin
        If TR.Nodes(i).key = key Then
          TR.Nodes(i).text = Mid(TR.Nodes(i).text, 1, InStr(1, TR.Nodes(i).text, ":")) & " " & UCase(Dato)
          Exit Sub
        End If
      Next i
  End Select
End Sub
Public Sub Associa_Dato_In_Memoria(Livello As Long, key As String, Dato As String)
  Dim i As Long, Idx As Long
  
  Select Case Trim(UCase(key))
    Case "PSB"
      For i = 1 To MaVSAMF_Corrector.lswPsb.ListItems.Count
         ReDim Preserve Istruzione.psb(i)
         Istruzione.psb(i).idOggetto = MaVSAMF_Corrector.lswPsb.ListItems(i).Tag
         Istruzione.psb(i).nome = MaVSAMF_Corrector.lswPsb.ListItems(i).text
         Istruzione.psb(i).Tipo = TYPE_PSB
      Next i
      
      gIdPsb = Restituisci_IdOggetto_Da_Nome(Dato, "PSB")
    
    Case "DBD"
      For i = 1 To MaVSAMF_Corrector.lswDBD.ListItems.Count
         ReDim Preserve Istruzione.Database(i)
         Istruzione.Database(i).idOggetto = MaVSAMF_Corrector.lswDBD.ListItems(i).Tag
         Istruzione.Database(i).nome = MaVSAMF_Corrector.lswDBD.ListItems(i).text
         Istruzione.Database(i).Tipo = TYPE_DBD
      Next i
      
      'Recupera l'id del database
      gIdDbd = Restituisci_IdOggetto_Da_Nome(Dato, "DBD")
    
    Case Else
    'SQ
    If UBound(Istruzione.SSA) Then
      For i = 1 To UBound(Istruzione.SSA)
        If Istruzione.SSA(i).Livello = Livello Then
          Idx = i
          Exit For
        End If
      Next i
    
      Select Case Trim(UCase(key))
        Case "SEG"
          Istruzione.SSA(i).NomeSegmento = Dato
        Case "SSA"
          Istruzione.SSA(i).ssaName = Dato
        Case "SGL"
          Istruzione.SSA(i).LenSegmento = Dato
        Case "KEY"
          Istruzione.SSA(i).Chiavi(AUSelIndexKey).key = Dato
        Case "KYV"
          Istruzione.SSA(i).Chiavi(AUSelIndexKey).Valore.fieldValue = Dato
        Case "OPE"
          Istruzione.SSA(i).Chiavi(AUSelIndexKey).Operatore = Dato
        Case "KYL"
          Istruzione.SSA(i).Chiavi(AUSelIndexKey).Valore.Lunghezza = Dato
        Case "KYS"
          If Len(Dato) Then
            Istruzione.SSA(i).Chiavi(AUSelIndexKey).Valore.KeyStart = Dato
          Else
            Istruzione.SSA(i).Chiavi(AUSelIndexKey).Valore.KeyStart = 0
          End If
        Case "COD"
          Istruzione.SSA(i).CommandCode = Dato
        Case "VAL"
          Istruzione.SSA(i).Chiavi(AUSelIndexKey).Valore.Valore = Dato
        Case "STR"
          Istruzione.SSA(i).Struttura = Dato
         Case "OPL"
           Istruzione.SSA(i).OpLogico(AUSelIndexKey) = Dato
         Case "CKB"
           Istruzione.SSA(i).BothQualAndUnqual = Dato
         Case "CKQ"
           Istruzione.SSA(i).Qualificatore = Dato
      End Select
    End If
  End Select
End Sub

Public Function Restituisci_Dati_Segmento_Da_IdSegm(Id As Long, TipoRestituito As String) As Variant
  Dim r As ADODB.Recordset
  
  Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_Segmenti Where IdSegmento = " & Id)
  If r.RecordCount > 0 Then
    Select Case UCase(Trim(TipoRestituito))
      Case "NOME"
        Restituisci_Dati_Segmento_Da_IdSegm = Trim(r!nome)
      Case "LUNGHEZZA"
        Restituisci_Dati_Segmento_Da_IdSegm = Trim(r!MaxLen)
    End Select
    r.Close
  End If
End Function

Public Function Restituisci_Dati_Struttura(IdObj As Long, riga As Long, Livello As Long) As Variant
  Dim r As ADODB.Recordset
  
  'SG : Set r = m_dllFunctions.Open_Recordset("SELECT * FROM PsData_Cmp WHERE IdOggetto = " & IdObj & " And IdArea = " & IdArea & " And Livello = 1")
  'Prendo dalla tabella Area
  
  Set r = m_dllFunctions.Open_Recordset("SELECT * FROM PsDli_IstrDett_Liv WHERE IdOggetto = " & IdObj & " And Riga = " & riga & " And Livello = " & Livello)
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    Restituisci_Dati_Struttura = Trim(r!dataarea)

    r.Close
  End If
End Function

Public Sub Carica_Istruzione_CCODE(idOggetto As Long, riga As Long, Livello As Long, ContSSA As Long)
  Dim rKey As ADODB.Recordset
  Dim i As Long
  
  Set rKey = m_dllFunctions.Open_Recordset("Select * From PsDli_IstrDett_Liv Where IdOggetto = " & idOggetto & " And Riga = " & riga & " And Livello = " & Livello)
          
  If rKey.RecordCount > 0 Then
    rKey.MoveFirst
    
    If rKey.RecordCount > 1 Then
       m_dllFunctions.WriteLog "Carica_Istruzione_CCODE", "DR - Auto"
    End If
    
    If Not IsNull(rKey!condizione) Then
         Istruzione.SSA(ContSSA).CommandCode = rKey!condizione
    End If
  End If
  
  rKey.Close
End Sub

Public Sub Carica_Istruzione_KEY(idOggetto As Long, riga As Long, Livello As Long, ContSSA As Long)
  Dim rKey As ADODB.Recordset
  
  Set rKey = m_dllFunctions.Open_Recordset("Select * From PsDli_IstrDett_Key Where IdOggetto = " & idOggetto & " And Riga = " & riga & " And Livello = " & Livello & " Order By Progressivo")
  
  ReDim Preserve Istruzione.SSA(ContSSA).Chiavi(0)
  ReDim Preserve Istruzione.SSA(ContSSA).OpLogico(0)
   
  If rKey.RecordCount > 0 Then
    rKey.MoveFirst
  
   While Not rKey.EOF
      ReDim Preserve Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi) + 1)
      Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).key = Trim(Restituisci_DatiKey(rKey!IdField, rKey!xName, "NOME"))
      
      If rKey!KeyLen = 0 Or Trim(rKey!KeyLen) = "" Then
         'Prende dal field dli la lunghezza
         Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.Lunghezza = Trim(Restituisci_DatiKey(rKey!IdField, rKey!xName, "LUNGHEZZA"))
         
         'Aggiorna
         rKey!KeyLen = Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.Lunghezza
         rKey.Update
      Else
         Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.Lunghezza = rKey!KeyLen
      End If
      
      Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.Valore = rKey!Valore & ""
      Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Operatore = rKey!Operatore
      Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.fieldValue = rKey!keyvalore & ""
      
      If Not IsNull(rKey!KeyStart) Then
         Istruzione.SSA(ContSSA).Chiavi(UBound(Istruzione.SSA(ContSSA).Chiavi)).Valore.KeyStart = rKey!KeyStart
      End If
      
      If Trim(rKey!booleano) <> "" Then
         ReDim Preserve Istruzione.SSA(ContSSA).OpLogico(UBound(Istruzione.SSA(ContSSA).OpLogico) + 1)
         Istruzione.SSA(ContSSA).OpLogico(UBound(Istruzione.SSA(ContSSA).OpLogico)) = rKey!booleano
      End If
      
      rKey.MoveNext
   Wend
   
    rKey.Close
  End If
End Sub
Public Function FirstElement(Source As String) As String
  Dim s() As String
  Dim i As Integer
  FirstElement = ""
  s = Split(Source, ";")
  If UBound(s) >= 0 Then
    FirstElement = s(0)
  End If
End Function

Public Sub Conferma_Convalida_Istruzione(idOggetto As Long, riga As Long, ValoreCheck As Boolean, CheckObsolete As Boolean, CheckToMigrate)
  Dim i As Long
  Dim j As Integer
  Dim r As ADODB.Recordset
  Dim scelta As Long, IdSegm As Long, IdField As Long, IdDBD As Long
  Dim SceltaSegm As Long
  Dim SceltaField As Long
  Dim sDbd() As Long
  Dim sPsb() As Long
  Dim rs As ADODB.Recordset
  Dim BolSegValid As Boolean
  Dim BolkeyValid As Boolean
  Dim BolValidBefore
  Dim s() As String
  Dim primaIstr As String
  Dim primoPCB As String, idpgm As Long
  
  SceltaSegm = vbNo
  SceltaField = vbNo
  idpgm = Replace(MaVSAMF_Corrector.lswRighe.SelectedItem.ToolTipText, "IdPgm: ", "") 'provvisorio!
  primaIstr = FirstElement(Trim(MaVSAMF_Corrector.TxtInstr.text))
  'primoPCB = FirstElement(Trim(MaVSAMF_Corrector.TxtPCB.Text))
  
  'Salva i dati nella tabella Istruzioni (Livello 1)
  Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_Istruzioni Where IdOggetto = " & idOggetto & " And Riga = " & riga)
  If r.RecordCount > 0 Then
    BolValidBefore = r!valida
    r!valida = ValoreCheck
    
    r!obsolete = CheckObsolete
    r!to_migrate = CheckToMigrate
    r!Correzione = True
    r!ordPcb = IIf(Trim(MaVSAMF_Corrector.TxtPCB.text) = "", Null, Trim(MaVSAMF_Corrector.TxtPCB.text))
    r!Istruzione = IIf(primaIstr = "", Null, primaIstr)
    r.Update
    
  End If
  r.Close
  
  m_dllFunctions.FnConnection.Execute "Delete * From PSDli_Istruzioni_Clone Where IdOggetto = " & idOggetto & " And Riga = " & riga
  ' Lettura e scrittura cloni
  j = 1
  Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_Istruzioni_Clone Where IdOggetto = " & idOggetto & " And Riga = " & riga)
  
  ' istruzioni
  s = Split(Trim(MaVSAMF_Corrector.TxtInstr.text), ";")
  For i = 1 To UBound(s)
    r.AddNew
    r!idOggetto = idOggetto
    r!idpgm = idpgm
    r!riga = riga
    r!IdClone = j
    r!Istruzione = s(i)
    j = j + 1
  Next
  ' num PCB
  's = Split(Trim(MaVSAMF_Corrector.TxtPCB.Text), ";")
  'For i = 1 To UBound(s)
    'r.AddNew
    'r!IdOggetto = IdOggetto
    'r!Riga = Riga
    'r!IdClone = j
    'r!OrdPcb = s(i)
    'j = j + 1
  'Next
  If j > 1 Then
    r.Update
  End If
  r.Close
  
  'Scrive i dbd e i psb
  m_dllFunctions.FnConnection.Execute "Delete * From PSDli_IstrDBD Where IdOggetto = " & idOggetto & " And Riga = " & riga
  
  For i = 1 To UBound(Istruzione.Database)
    IdDBD = Istruzione.Database(1).idOggetto
    'SG : prende il dbd primo che � quello giusto
    
    Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_IstrDBD Where IdOggetto = " & idOggetto & " And Riga = " & riga & " And IdDbd = " & Istruzione.Database(i).idOggetto)
    If r.RecordCount = 0 Then
      r.AddNew
      '**********
      
      '*********
      r!idOggetto = idOggetto
      r!riga = riga
      r!IdDBD = Istruzione.Database(i).idOggetto
      
      r.Update
    End If
    r.Close
    
   'Scrive in relazioni Obj
   Set rs = m_dllFunctions.Open_Recordset("Select * From PsRel_Obj Where IdOggettoC = " & idOggetto & " And IdOggettoR = " & IdDBD)
   If rs.RecordCount = 0 Then
      rs.AddNew
      
      rs!IdOggettoC = idOggetto
      rs!IdOggettoR = IdDBD
      rs!Relazione = "DBD"
      rs!Utilizzo = "IMS-DBD"
      rs!NomeComponente = m_dllFunctions.Restituisci_NomeOgg_Da_IdOggetto(idOggetto)
      
      rs.Update
   End If
   rs.Close
  Next i
  
  m_dllFunctions.FnConnection.Execute "Delete * From PSDli_IstrPSB Where IdOggetto = " & idOggetto & idOggetto & " And Riga = " & riga
  
  For i = 1 To UBound(Istruzione.psb)
    IdPSB = Istruzione.psb(1).idOggetto
    'SG : prende il dbd primo che � quello giusto
    
    Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_Istrpsb Where IdOggetto = " & idOggetto & " And Riga = " & riga & " And Idpsb = " & Istruzione.psb(i).idOggetto)
  
    If r.RecordCount = 0 Then
      r.AddNew
      
      r!idOggetto = idOggetto
      r!riga = riga
      r!IdPSB = Istruzione.psb(i).idOggetto
      
      r.Update
   End If
   
   'SG :Scrive in relazioni Obj
   Set rs = m_dllFunctions.Open_Recordset("Select * From PsRel_Obj Where IdOggettoC = " & idOggetto & " And IdOggettoR = " & IdPSB)
      
   If rs.RecordCount = 0 Then
      rs.AddNew
      
      rs!IdOggettoC = idOggetto
      rs!IdOggettoR = IdPSB
      rs!Relazione = "PSB"
      rs!Utilizzo = "PSB"
      rs!NomeComponente = m_dllFunctions.Restituisci_NomeOgg_Da_IdOggetto(idOggetto)
      
      rs.Update
   End If
   
   rs.Close
  Next i
  
  For i = 1 To UBound(Istruzione.SSA)
    Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_IstrDett_Liv Where IdOggetto = " & idOggetto & " And Riga = " & riga & " And Livello = " & Istruzione.SSA(i).Livello)
  
    If r.RecordCount > 0 Then
      IdSegm = Restituisci_IdSegm_DaNome(idOggetto, Istruzione.SSA(i).NomeSegmento, IdDBD)
      
      If (GbPrevPCB = Trim(MaVSAMF_Corrector.TxtPCB.text)) Then ' non sto riparser perch� cambiato num PCB
        If Istruzione.Tipologia <> "REPL" And Istruzione.Tipologia <> "DLET" Then
           If IdSegm = 0 And SceltaSegm = vbNo Then
             scelta = MsgBox("Segment not Found..." & vbCrLf & "Continue saving instruction ? ", vbYesNo, VSAM2Rdbms.drNomeProdotto)
               
             If scelta = vbNo Then
               Exit Sub
             Else
               SceltaSegm = vbYes
             End If
           End If
        End If
      End If
      
      r!idSegmento = IdSegm
      r!SegLen = Istruzione.SSA(i).LenSegmento
      r!condizione = Istruzione.SSA(i).CommandCode & ""
      r!Qualificazione = Istruzione.SSA(i).Qualificatore
      r!QualAndUnqual = Istruzione.SSA(i).BothQualAndUnqual
      'r!qualificazione = MaVSAMF_Corrector.ChkQual.Value
      'r!QualAndUnqual = MaVSAMF_Corrector.ChkBoth.Value
      r!dataarea = Istruzione.SSA(i).Struttura
      r!NomeSSA = Istruzione.SSA(i).ssaName
      r!valida = ValoreCheck
      BolSegValid = r!valida
      r!Correzione = True
      
      r.Update
      r.Close
      
       
     Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_IstrDett_Key Where IdOggetto = " & idOggetto & " And Riga = " & riga & " And Livello = " & Istruzione.SSA(i).Livello)
   
     'alpitour: 5/8/2005: MANCAVA IL CICLO PER LE BOOLEANE!!!!
    
       
       
       While Not r.EOF
       r!Valore = Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Valore.Valore
       r!Operatore = Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Operatore
       r!KeyStart = Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Valore.KeyStart
       r!keyvalore = Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Valore.fieldValue
       ReDim Preserve Istruzione.SSA(i).OpLogico(r.AbsolutePosition)
       r!booleano = Istruzione.SSA(i).OpLogico(r.AbsolutePosition)
       
       IdField = Restituisci_IdField_DaNome(IdSegm, Istruzione.SSA(i).Chiavi(r.AbsolutePosition).key)
       If (GbPrevPCB = Trim(MaVSAMF_Corrector.TxtPCB.text)) Then ' non sto riparser perch� cambiato num PCB
         If Istruzione.Tipologia <> "REPL" And Istruzione.Tipologia <> "DLET" Then
            If IdField = 0 And SceltaField = vbNo Then
              scelta = MsgBox("Field (Key) not Found..." & vbCrLf & "Continue saving instruction ? ", vbYesNo, m_dllFunctions.FnNomeProdotto)
      
              If scelta = vbNo Then
                Exit Sub
              Else
                SceltaField = vbYes
              End If
            End If
         End If
       End If
       r!IdField = IdField
       r!KeyLen = Istruzione.SSA(i).Chiavi(r.AbsolutePosition).Valore.Lunghezza
       
       r!valida = ValoreCheck
       BolkeyValid = r!valida
       r!Correzione = True
       
       r.Update
       r.MoveNext
       Wend
       r.Close
     
   End If
  Next i
  
  ' AC
  ' If BolkeyValid And BolSegValid Then
  '  Set r = m_dllFunctions.Open_Recordset("Select * From PsDli_Istruzioni Where IdOggetto = " & IdOggetto & " And Riga = " & Riga)
  '  If Not r.EOF Then
  '      If Not r!Valid Then
  '          r!Valid = True
            'Refresh
  '          Carica_ListView_Istruzioni MaVSAMF_Corrector.lswRighe, IdOggetto, True, MaVSAMF_Corrector.RTIncaps
 '           Carica_Array_Istruzione Riga, IdOggetto
   '     End If
  '  End If
  '  r.Close
'  End If
'AC
  'If Not (ValoreCheck = BolValidBefore) Then
    'Refresh
    If ValoreCheck Then
      If MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 8 Or MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 11 Then
        MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 8
      Else
        MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 19
      End If
    Else
        If MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 8 Or MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 11 Then
        MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 11
      Else
        MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 20
      End If
    End If
    
    'Carica_Array_Istruzione Riga, IdOggetto
  'End If

  'Controlla se tutte le istruzioni del programma sono giuste
  Set r = m_dllFunctions.Open_Recordset("Select * From PsDli_Istruzioni Where IdOggetto = " & idOggetto & " And Valida = False")
  
  If r.RecordCount > 0 Then
      ValoreCheck = False
  ElseIf r.RecordCount = 0 Then
      ValoreCheck = True
  End If
  
  '*************************************************************************
  '******** Cancella le segnalazioni di istruzione non decodificata ********
  '*************************************************************************
  If ValoreCheck Then
    Set r = m_dllFunctions.Open_Recordset("Select * From BS_Segnalazioni Where IdOggetto = " & idOggetto & " And Codice = 'F08'")
    While Not r.EOF
      r.Delete
      r.MoveNext
    Wend
    r.Close
    
    'Elimina da Bs_Oggetti errLevel
    Set r = m_dllFunctions.Open_Recordset("Select * From BS_Segnalazioni Where IdOggetto = " & idOggetto & " And Codice = 'F08'")
    
    If r.RecordCount = 0 Then
         
      Set r = m_dllFunctions.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & idOggetto)
      
      If r.RecordCount > 0 Then
         r!ErrLevel = ""
         
         'Aggiorna il video
         m_dllFunctions.FnObjList.SelectedItem.SmallIcon = 6
         m_dllFunctions.FnObjList.SelectedItem.ListSubItems(3) = ""
         
         r.Update
      End If
    End If
    If MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 8 Or MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 11 Then
        MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 8
    Else
        MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 19
    End If
   
  Else
    If MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 8 Or MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 11 Then
        MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 11
    Else
        MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 20
    End If
   
    'Scrive la segnalazione di istruzione non decodificata
    Set r = m_dllFunctions.Open_Recordset("Select * From BS_Segnalazioni Where IdOggetto = " & idOggetto & " And Codice = 'F08' and Riga = " & riga)
    
    If r.RecordCount = 0 Then
      r.AddNew
    End If
    
    r!idOggetto = idOggetto
    r!riga = riga
    r!Codice = "F08"
    r!Origine = "AT"
    r!Data_Segnalazione = Now
    r!Tipologia = "AUTO"
    r!Gravita = "E"
      
    r.Update
    
    'La scrive anche su BS_Oggetti : errLevel
    Set r = m_dllFunctions.Open_Recordset("Select * From BS_Oggetti Where IdOggetto = " & idOggetto)
      
   If r.RecordCount > 0 Then
      'If Trim(r!ErrLevel) = "" Then
         r!ErrLevel = "F08"
         m_dllFunctions.FnObjList.SelectedItem.SmallIcon = 5
         m_dllFunctions.FnObjList.SelectedItem.ListSubItems(3) = "F-8"
         r.Update
      'End If
   End If
  End If
End Sub

Public Function Restituisci_IdOggetto_Da_Nome(nome As String, wTipo As String) As Long
  Dim r As ADODB.Recordset
  
  If Trim(wTipo) = "" Then
   Set r = m_dllFunctions.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & nome & "'")
  Else
   Set r = m_dllFunctions.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & nome & "' And Tipo = '" & wTipo & "'")
  End If
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    'E' un record solo
    Restituisci_IdOggetto_Da_Nome = r!idOggetto
    
    r.Close
  End If
End Function

Public Sub Aggiorna_Icone_TreeView_Istruzione(ValoreChk As Long, TRW As TreeView)
  Dim i As Long
  
  If ValoreChk = 1 Then
    For i = 1 To TRW.Nodes.Count
      Select Case Trim(UCase(TRW.Nodes(i).key))
        Case "DBD", "PSB"
          'If ValoreChk = 1 Then
            'TRW.Nodes(i).Image = 8
          'End If
        
        Case "", "ISTR"
          'Non mi importa niente !!!
          
        Case Else
          If InStr(1, Trim(UCase(TRW.Nodes(i).key)), "LEV") = 0 Then
              'stefano: provo....
              If InStr(1, TRW.Nodes(i).text, "SSA Name") > 0 Then
                If MaVSAMF_Corrector.lswRighe.SelectedItem.Icon = 8 Or MaVSAMF_Corrector.lswRighe.SelectedItem.Icon = 11 Then
                 TRW.Nodes(i).Image = 8
'                Else
'                 TRW.Nodes(i).Image = 19
                End If
              Else
                TRW.Nodes(i).Image = 8
              End If
         End If
      End Select
      
    Next i
  Else
    For i = 1 To TRW.Nodes.Count
      TRW.Nodes(i).Image = Mappa_Immaggini_TreeView(i)
    Next
  End If
  If ValoreChk = 1 Then
        If MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 8 Or MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 11 Then
          MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 8
        Else
          MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 19
        End If
      Else
        If MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 8 Or MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 11 Then
          MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 11
        Else
          MaVSAMF_Corrector.lswRighe.SelectedItem.SmallIcon = 20
        End If
      End If
End Sub

Public Function Restituisci_IdSegm_DaNome(idOggetto As Long, nome As String, widDbd As Long) As Long
  Dim rs As ADODB.Recordset
  
  Set rs = m_dllFunctions.Open_Recordset("Select * From PSDli_Segmenti Where Nome = '" & nome & "' And IdOggetto = " & widDbd)
  
  If rs.RecordCount > 0 Then
    rs.MoveFirst
    
    If rs.RecordCount = 1 Then
      Restituisci_IdSegm_DaNome = rs!idSegmento
    Else
      'Molteplicit� : Impossibile
    End If
    
    rs.Close
  End If
End Function

Public Function Restituisci_IdField_DaNome(IdSegm As Long, nome As String) As Long
  Dim rf As ADODB.Recordset
  Dim rs As ADODB.Recordset
  
  Set rf = m_dllFunctions.Open_Recordset("Select * From PSDli_Field Where IdSegmento = " & IdSegm & " And Nome = '" & nome & "'")
  
  If rf.RecordCount > 0 Then
    rf.MoveFirst
    
    Restituisci_IdField_DaNome = rf!IdField
    
    rf.Close
  ElseIf rf.RecordCount = 0 Then
      Set rs = m_dllFunctions.Open_Recordset("Select * From PSDli_XDField Where IdSegmento = " & IdSegm & " And Nome = '" & nome & "'")
  
      If rs.RecordCount > 0 Then
         Restituisci_IdField_DaNome = -1 * rs!IdXDField
      End If
      
      rs.Close
  End If

End Function

Public Sub Launch_TextEditor(rtSel As String)
    Dim rf As ADODB.Recordset
    Dim wApp1 As String
    Dim wApp2 As String
    Dim cObj As Object
    
    'Normalizza la stringa
    wApp1 = Replace(rtSel, ".", "")
    wApp2 = Replace(wApp1, ",", "")
    wApp1 = Replace(wApp2, ":", "")
    wApp2 = Replace(wApp1, "(", "")
    wApp1 = Replace(wApp2, ")", "")
    wApp2 = Replace(wApp1, "'", "")
    wApp1 = Replace(wApp2, "=", "")
    wApp2 = Replace(wApp1, "+", "")
    
    Set rf = m_dllFunctions.Open_Recordset("Select * From BS_Oggetti Where Nome = '" & wApp2 & "'")
    
    If rf.RecordCount > 0 Then
      rf.MoveFirst
      
      If rf.RecordCount > 1 Then
        'Ci sono oggetti con lo stesso nome e pi� tipi
        m_dllFunctions.Show_MsgBoxError "RC000I"
        
        MaVSAMF_Molteplicita.Show vbModal
      End If
    
      m_dllFunctions.Show_TextEditor rf!idOggetto, , False
    End If
    
    rf.Close
End Sub
Sub Riparser_after_PCBChange(pNumPCB As Integer, pNamePCB As String)
  Dim colIdPSB As New collection
  Dim i As Integer
  Dim Istruzione As String, parametri As String
  'SQ CPY-ISTR
  Dim idpgm As Long, idOggetto As Long, riga As Long
  Dim rs As Recordset

  If Not MaVSAMF_Corrector.lswRighe.SelectedItem Is Nothing Then
    'SQ CPY-ISTR
    idpgm = Replace(MaVSAMF_Corrector.lswRighe.SelectedItem.ToolTipText, "IdPgm: ", "") 'provvisorio!
    idOggetto = MaVSAMF_Corrector.lswRighe.SelectedItem
    riga = MaVSAMF_Corrector.lswRighe.SelectedItem.SubItems(1)
    Istruzione = MaVSAMF_Corrector.lswRighe.SelectedItem.SubItems(2)
    parametri = MaVSAMF_Corrector.lswRighe.SelectedItem.SubItems(3)
  End If
  
  If MaVSAMF_Corrector.lswPsb.ListItems.Count Then
    For i = 1 To MaVSAMF_Corrector.lswPsb.ListItems.Count
      'SQ - andava in overflow... buttata via, tanto non serviva a nessun altro
      'IdPSB = IdPSBfromName(MaVSAMF_Corrector.lswPsb.ListItems(i))
      'If Not IdPSB = -1 Then
      '  colIdPSB.Add (IdPSB)
      'End If
      Set rs = m_dllFunctions.Open_Recordset("Select IdOggetto From BS_Oggetti Where Nome= '" & MaVSAMF_Corrector.lswPsb.ListItems(i) & "' and Tipo='PSB'")
      If rs.RecordCount Then
        colIdPSB.Add CLng(rs!idOggetto)
      Else
        '??
      End If
      rs.Close
    Next
    'SQ portato nel Parser... se no non viene mai aggiornata...
    'DliInitParserRiga idPgm, idOggetto, riga 'Mettere nel Parser!
    
    'chiamata funz parser che valorizza array PSB per CBLII
    'chiamata funz parser che valorizza struttura con PCB
    'SQ CPY-ISTR
    'DLLExtParser.ParserAfterPCBNumber colIdPSB, pNumPCB, pNamePCB, istruzione, parametri, idOggetto, riga
    DLLExtParser.ParserAfterPCBNumber colIdPSB, pNumPCB, pNamePCB, Istruzione, parametri, idpgm, idOggetto, riga
  Else
    'gestire...
  End If
End Sub
