VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MavsdF_Parametri 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Parameters"
   ClientHeight    =   5115
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8625
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5115
   ScaleWidth      =   8625
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FraWord 
      BackColor       =   &H8000000A&
      Caption         =   "Word Changing"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   2775
      Left            =   4770
      TabIndex        =   2
      Top             =   420
      Visible         =   0   'False
      Width           =   3855
      Begin VB.TextBox txtWord 
         Height          =   285
         Left            =   2040
         TabIndex        =   4
         Top             =   2670
         Visible         =   0   'False
         Width           =   1635
      End
      Begin MSComctlLib.ListView LswWord 
         Height          =   2415
         Left            =   150
         TabIndex        =   3
         Top             =   270
         Width           =   3555
         _ExtentX        =   6271
         _ExtentY        =   4260
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Original Word"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "New Word"
            Object.Width           =   3528
         EndProperty
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8625
      _ExtentX        =   15214
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Save"
            Object.ToolTipText     =   "Save"
            ImageKey        =   "Save"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TRWParametri 
      Height          =   4665
      Left            =   0
      TabIndex        =   0
      Top             =   420
      Width           =   4725
      _ExtentX        =   8334
      _ExtentY        =   8229
      _Version        =   393217
      Indentation     =   617
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Checkboxes      =   -1  'True
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   120
      Top             =   5250
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Parametri.frx":0000
            Key             =   "Save"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MavsdF_Parametri"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public ColListaSel As Long
Public IdxRiga As Long

'Array Di appoggio
Private AppDBD() As String
Private AppOr() As String
Private AppNew() As String

Private Sub Form_Resize()
    ResizeForm Me
End Sub

Private Sub LswWord_Click()
  If txtWord.Visible = True Then
    
    Select Case ColListaSel
      Case 1
        LswWord.ListItems(IdxRiga).text = txtWord.text
        
      Case 2
        LswWord.ListItems(IdxRiga).ListSubItems(1).text = txtWord.text
        
    End Select
    
    txtWord.text = ""
    txtWord.Visible = False
  End If
End Sub

Private Sub LswWord_DblClick()
  If LswWord.SelectedItem.Index > 1 Then
    If LswWord.ListItems(LswWord.SelectedItem.Index - 1).text <> "" Then
      txtWord.Move LswWord.Left + LswWord.ColumnHeaders(ColListaSel).Left + 50, LswWord.ListItems(LswWord.SelectedItem.Index).Top + LswWord.ListItems(LswWord.SelectedItem.Index).Height + 50, LswWord.ColumnHeaders(ColListaSel).Width
      IdxRiga = LswWord.SelectedItem.Index
      txtWord.Visible = True
      txtWord.text = ""
      
      txtWord.text = LswWord.SelectedItem.text
      
      txtWord.SetFocus
    End If
  Else
    txtWord.Move LswWord.Left + LswWord.ColumnHeaders(ColListaSel).Left + 50, LswWord.ListItems(LswWord.SelectedItem.Index).Top + LswWord.ListItems(LswWord.SelectedItem.Index).Height + 50, LswWord.ColumnHeaders(ColListaSel).Width
    IdxRiga = LswWord.SelectedItem.Index
    txtWord.Visible = True
    txtWord.text = ""
    
    txtWord.text = LswWord.SelectedItem.text
    
    txtWord.SetFocus
  End If
End Sub

Private Sub txtWord_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then
    
    Select Case ColListaSel
      Case 1
        LswWord.ListItems(IdxRiga).text = txtWord.text
      
      Case 2
        LswWord.ListItems(IdxRiga).ListSubItems(1).text = txtWord.text
        
    End Select
    
    txtWord.Visible = False
    txtWord.text = ""
    
  End If
End Sub

Private Sub LswWord_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  ColListaSel = Restituisci_Colonna_Lista_Selezionata(LswWord, x)
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  On Error Resume Next
  Select Case Button.key
    Case "Save"
      Screen.MousePointer = vbHourglass
      
      Call Salva_Parametri(TRWParametri, LswWord)
      
      Screen.MousePointer = vbDefault
  End Select
End Sub

Private Sub Form_Load()
  Carica_Parametri_Da_DB AppDBD, AppOr, AppNew
  Carica_TreeView_Parametri TRWParametri, LswWord
  'Resize della lista
  LswWord.ColumnHeaders(1).Width = (LswWord.Width - 270) / 2
  LswWord.ColumnHeaders(2).Width = (LswWord.Width - 270) / 2
End Sub

Private Sub Form_Activate()
  'Controlla se la DLL Parser � presente
  If Not Verifica_Esistenza_DLLParser Then
    MsgBox "Parser DLL Not found or corrupted...", vbCritical, VSAM2Rdbms.drNomeProdotto
    Unload Me
  End If
End Sub

Private Sub TRWParametri_DblClick()
  Dim Valore As String
  Dim Texto As String
  
  Select Case TRWParametri.SelectedItem.key
    Case "LEVNUM"
      Valore = InputBox("Insert Max Instruction Level Calling...", VSAM2Rdbms.drNomeProdotto)
      
      'controlli sul valore
      If Not IsNumeric(Valore) Then
        Call MsgBox("You must insert a numeric value...", vbExclamation, VSAM2Rdbms.drNomeProdotto)
        TRWParametri.SelectedItem.text = "Number : 0"
        Exit Sub
      End If
      
      TRWParametri.SelectedItem.text = "Number : " & Valore
      VSAM2Rdbms.DrMaxLevIstr = Valore
      
      FraWord.Visible = False
  
    Case "WRD"
      FraWord.Visible = True
    
    Case "ROUTTP"
      Valore = InputBox("Insert New Format for CICS Routine's name...", VSAM2Rdbms.drNomeProdotto)
      
      Texto = Mid(TRWParametri.SelectedItem.text, 1, InStr(1, TRWParametri.SelectedItem.text, ":"))

      TRWParametri.SelectedItem.text = Texto & " " & UCase(Trim(Valore))
      
      VSAM2Rdbms.DrTPRout = UCase(Trim(Valore))
      
    Case "ROUTBT"
      Valore = InputBox("Insert New Format for BATCH Routine's name...", VSAM2Rdbms.drNomeProdotto)
      
      Texto = Mid(TRWParametri.SelectedItem.text, 1, InStr(1, TRWParametri.SelectedItem.text, ":"))

      TRWParametri.SelectedItem.text = Texto & " " & UCase(Trim(Valore))
      
      VSAM2Rdbms.DrBTRout = UCase(Trim(Valore))
      
    Case "ROUTGN"
      Valore = InputBox("Insert New Format for General Routine's name...", VSAM2Rdbms.drNomeProdotto)
      
      Texto = Mid(TRWParametri.SelectedItem.text, 1, InStr(1, TRWParametri.SelectedItem.text, ":"))

      TRWParametri.SelectedItem.text = Texto & " " & UCase(Trim(Valore))
      
      VSAM2Rdbms.DrGNRout = UCase(Trim(Valore))
  End Select
  
End Sub

Private Sub TRWParametri_NodeCheck(ByVal Node As MSComctlLib.Node)
  Dim i As Long
  Dim Cont As Long
  Dim AllBool As Boolean
  
  If Node.key <> "WRD" Then
    FraWord.Visible = False
  End If
  
  'Controlla lo stato dei check
  If Node.key = "DBD" Then
    For i = 1 To TRWParametri.Nodes.Count
      If Mid(TRWParametri.Nodes(i).key, 1, 2) = "DB" Then
        TRWParametri.Nodes(i).Checked = Node.Checked
      End If
    Next i
  End If
  
  AllBool = True
  
  For i = 1 To TRWParametri.Nodes.Count
    If Mid(TRWParametri.Nodes(i).key, 1, 2) = "DB" Then
      Cont = Cont + 1
    End If
  Next i
  
  If Mid(Node.key, 1, 2) = "DB" Then
    If Node.Checked = False Then
      TRWParametri.Nodes(1).Checked = False
    Else
      For i = 2 To Cont
       If TRWParametri.Nodes(i).Checked = False Then
        AllBool = False
        Exit For
       End If
      Next i
    
      If AllBool = True Then
        TRWParametri.Nodes(1).Checked = True
      Else
        TRWParametri.Nodes(1).Checked = False
      End If
    End If
  End If
  
  Select Case Trim(UCase(Node.key))
    Case "DLICALL"
      VSAM2Rdbms.DrCallDli = "CALL"
      TRWParametri.Nodes(Node.Index + 1).Checked = False
      
    Case "DLIEXEC"
      VSAM2Rdbms.DrCallDli = "EXEC"
      TRWParametri.Nodes(Node.Index - 1).Checked = False
      
    Case "RTNCALL"
      VSAM2Rdbms.DrCallRtn = "CALL"
      TRWParametri.Nodes(Node.Index + 1).Checked = False
      
    Case "RTNEXEC"
      VSAM2Rdbms.DrCallRtn = "EXEC"
      TRWParametri.Nodes(Node.Index - 1).Checked = False
      
    Case "IMSUNI"
      VSAM2Rdbms.DrIMSUsing = "UNI"
      TRWParametri.Nodes(Node.Index + 1).Checked = False
      
    Case "IMSDIV"
      VSAM2Rdbms.DrIMSUsing = "DIV"
      TRWParametri.Nodes(Node.Index - 1).Checked = False
      
    Case "USGCPY"
      VSAM2Rdbms.DrUsing = "CPY"
      TRWParametri.Nodes(Node.Index + 1).Checked = False
      
    Case "USGPRE"
      VSAM2Rdbms.DrUsing = "PRE"
      TRWParametri.Nodes(Node.Index - 1).Checked = False
      
    Case "LEVNUM"
      VSAM2Rdbms.DrMaxLevIstr = Val(Trim(Mid(Node.text, InStr(1, Node.text, ":") + 1)))
  End Select
End Sub

Private Sub TRWParametri_NodeClick(ByVal Node As MSComctlLib.Node)
  
  Select Case Trim(UCase(Node.key))
    Case "DLICALL"
      VSAM2Rdbms.DrCallDli = "CALL"
      TRWParametri.Nodes(Node.Index + 1).Checked = False
      
    Case "DLIEXEC"
      VSAM2Rdbms.DrCallDli = "EXEC"
      TRWParametri.Nodes(Node.Index - 1).Checked = False
      
    Case "RTNCALL"
      VSAM2Rdbms.DrCallRtn = "CALL"
      TRWParametri.Nodes(Node.Index + 1).Checked = False
      
    Case "RTNEXEC"
      VSAM2Rdbms.DrCallRtn = "EXEC"
      TRWParametri.Nodes(Node.Index - 1).Checked = False
      
    Case "IMSUNI"
      VSAM2Rdbms.DrIMSUsing = "UNI"
      TRWParametri.Nodes(Node.Index + 1).Checked = False
      
    Case "IMSDIV"
      VSAM2Rdbms.DrIMSUsing = "DIV"
      TRWParametri.Nodes(Node.Index - 1).Checked = False
      
    Case "USGCPY"
      VSAM2Rdbms.DrUsing = "CPY"
      TRWParametri.Nodes(Node.Index + 1).Checked = False
      
    Case "USGPRE"
      VSAM2Rdbms.DrUsing = "PRE"
      TRWParametri.Nodes(Node.Index - 1).Checked = False
      
    Case "LEVNUM"
      VSAM2Rdbms.DrMaxLevIstr = Val(Trim(Mid(Node.text, InStr(1, Node.text, ":") + 1)))
  End Select
  
  If Node.key <> "WRD" Then
    FraWord.Visible = False
  Else
    FraWord.Visible = True
  End If
End Sub

