Attribute VB_Name = "MaVSAMM_00"
Option Explicit

Global Const SYSTEM_DIR = "i-4.Mig.cfg"

Public gbTypeToLoad As String

'Template per i PCB
Type t_PcbStructDett
   wPicture As String
   wTipo As String
   wLunghezza As String
   wSegno As String
   wUsage As String
End Type

Type t_PcbStruct
   FixedLength As Long
   Details() As t_PcbStructDett
End Type

Type t_PcbTemplate
   IO_Pcb As t_PcbStruct
   ALT_Pcb As t_PcbStruct
   STD_Pcb As t_PcbStruct
End Type

Enum e_TypePcb
   IO_Pcb = 2 ^ 0
   ALTERNATE_PCB = 2 ^ 1
   STANDARD_PCB = 2 ^ 2
   IS_NOT_A_PCB = 2 ^ 3
End Enum
'Type Totale per i menu
Type MaM1
  Id  As Long
  Label As String
  ToolTipText As String
  Picture As String
  PictureEn As String
  M1Name As String
  M1SubName As String
  M1Level As Long
  M2Name As String
  M3Name As String
  M3ButtonType As String
  M3SubName As String
  M4Name As String
  M4ButtonType As String
  M4SubName As String
  Funzione As String
  DllName As String
  TipoFinIm As String
End Type

'SG : Indica la modalit� di incapsulamento/generazione (CALL o EXEC)
Public swCall As Boolean

Public Const WM_SETFOCUS As Long = &H7
Public Const WM_ACTIVATE As Long = &H6

Global drMenu() As MaM1

Global VSAM2Rdbms As New MaVSAMC_Menu
Global m_dllFunctions As New MaFndC_Funzioni

Global SwMenu As Boolean

Global ConnectionState As Boolean

Type WdCgh
  Origine  As String
  Nuova As String
End Type

Public DrNomiDBD() As String         'Nomi DBD
Public DrWordChange() As WdCgh       'Parole da cambiare

Public Declare Function SetFocusWnd Lib "user32.dll" Alias "SetFocus" _
  (ByVal hwnd As Long) As Long
Public Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Public Declare Function SendMessage Lib "user32" _
   Alias "SendMessageA" _
  (ByVal hwnd As Long, _
   ByVal wMsg As Long, _
   ByVal wParam As Long, _
   lParam As Any) As Long

Public Sub Carica_TreeView_Parametri(TR As TreeView, LSW As ListView)
  Dim i As Long
  Dim MaxItem As Long
  
  TR.Nodes.Clear
  
  TR.Nodes.Add , , "DBD", "Database List"
    'Aggiunge la lista dei database
    Call Aggiungi_Lista_DBD_In_TreeView(TR, "DBD")
    
  TR.Nodes.Add , , "DLI", "Dli Calling"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "DLI", tvwChild, "DLICALL", "CALL"
    TR.Nodes.Add "DLI", tvwChild, "DLIEXEC", "EXEC"
  
  TR.Nodes.Add , , "RTF", "Routine Name's Format"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "RTF", tvwChild, "ROUTTP", "CICS : " & VSAM2Rdbms.DrTPRout
    TR.Nodes.Add "RTF", tvwChild, "ROUTBT", "BATCH : " & VSAM2Rdbms.DrBTRout
    TR.Nodes.Add "RTF", tvwChild, "ROUTGN", "GENERAL : " & VSAM2Rdbms.DrGNRout
  
  TR.Nodes.Add , , "RTN", "Routine Calling"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "RTN", tvwChild, "RTNCALL", "CALL"
    TR.Nodes.Add "RTN", tvwChild, "RTNEXEC", "EXEC CICS"
  
  TR.Nodes.Add , , "IMS", "IMS DIFFERENCES"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "IMS", tvwChild, "IMSUNI", "WHETHER IMS TP/BATCH VALID"
    TR.Nodes.Add "IMS", tvwChild, "IMSDIV", "IMS TP/BATCH DIFFERENT"
    
  TR.Nodes.Add , , "USG", "USING"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "USG", tvwChild, "USGCPY", "USING COPY"
    TR.Nodes.Add "USG", tvwChild, "USGPRE", "USING PRECOMPILE"
    
  TR.Nodes.Add , , "LEV", "Max Istruction Level Call"
  TR.Nodes(TR.Nodes.Count).Checked = True
    TR.Nodes.Add "LEV", tvwChild, "LEVNUM", "Number : "
    
  TR.Nodes.Add , , "WRD", "Changing Word"
  TR.Nodes(TR.Nodes.Count).Checked = True
    
  Call Chekka_TreeView_Parametri(TR, LSW)
  
  'Carica la lista delle parole da cambiare in lista
  LSW.ListItems.Clear
  For i = 1 To UBound(DrWordChange)
    LSW.ListItems.Add , , DrWordChange(i).Origine
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , DrWordChange(i).Nuova
    MaxItem = MaxItem + 1
  Next i
  
  'Carica gli Item rimanenti fino a 50 di default nella lista
  For i = 1 To 50 - MaxItem
    LSW.ListItems.Add , , ""
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , ""
  Next i
End Sub

Public Sub Aggiungi_Lista_DBD_In_TreeView(TR As TreeView, Padre As String)
  Dim r As ADODB.Recordset
  Dim i As Long
  Dim key As String
  Dim Cont As Long
  
  Dim Idx As Long
  Dim Stringa As String
  Dim Parametro As String
  Dim Valore As String
  
  ReDim DrNomiDBD(0)
  
  'Controlla se ci sono dei parametri salvati
  Set r = m_dllFunctions.Open_Recordset("Select * From BS_Parametri Where Tipo = 'DRDBD'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    'Carica l'array in memoria
    Stringa = r!Valore
    
    Idx = InStr(1, Stringa, ";")
    
    While Idx <> 0
      
      Parametro = Mid(Stringa, 1, Idx - 1)
      
      ReDim Preserve DrNomiDBD(UBound(DrNomiDBD) + 1)
      DrNomiDBD(UBound(DrNomiDBD)) = Parametro
      
      Stringa = Mid(Stringa, Idx + 1)
      
      Idx = InStr(1, Stringa, ";")
    Wend
    
    r.Close
  End If
  
  If UBound(DrNomiDBD) = 0 Then 'Carica dal DB
    Cont = 1
    
    Set r = m_dllFunctions.Open_Recordset("Select * From BS_Oggetti Where Tipo = 'DBD' And Tipo_DBD = 'HID' Order By IdOggetto")
    
    If r.RecordCount > 0 Then
      r.MoveFirst
      
      While Not r.EOF
        key = "DB" & Format(Cont, "00") & r!nome
        
        TR.Nodes.Add Padre, tvwChild, key, r!nome
        
        r.MoveNext
      Wend
      r.Close
    End If
  Else  'Carica dai parametri
    For i = 1 To UBound(DrNomiDBD)
        key = "DB" & Format(i, "00") & DrNomiDBD(i)
        
        TR.Nodes.Add Padre, tvwChild, key, Mid(DrNomiDBD(i), 1, InStr(1, DrNomiDBD(i), "=") - 1)
        
        If Mid(DrNomiDBD(i), InStr(1, DrNomiDBD(i), "=") + 1) = "1" Then
          TR.Nodes(TR.Nodes.Count).Checked = True
        Else
          TR.Nodes(TR.Nodes.Count).Checked = False
        End If
    Next i
  End If
End Sub

Public Sub Carica_Parametri_Da_DB(ArrayDBD() As String, ArrOR() As String, ArrNew() As String)
  Dim r As ADODB.Recordset
  Dim Idx As Long
  Dim Stringa As String
  Dim Parametro As String
  Dim Valore As String
  
  'Parametri Globali
  Set r = m_dllFunctions.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'DRPARAM'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    'Deve essere un solo record
    Stringa = r!ParameterValue
    
    Idx = InStr(1, Stringa, ";")
    
    While Idx <> 0
      
      Parametro = Mid(Stringa, 1, Idx - 1)
      
      Valore = Mid(Parametro, InStr(1, Parametro, "=") + 1)
      Parametro = Mid(Parametro, 1, InStr(1, Parametro, "=") - 1)
      
      Select Case Trim(UCase(Parametro))
        'Assegna i parametri
        Case "DLICALL"
          If Valore = 1 Then
            VSAM2Rdbms.DrCallDli = "CALL"
          End If
          
        Case "DLIEXEC"
           If Valore = 1 Then
            VSAM2Rdbms.DrCallDli = "EXEC"
          End If
          
        Case "RTNCALL"
          If Valore = 1 Then
            VSAM2Rdbms.DrCallRtn = "CALL"
          End If
          
        Case "RTNEXEC"
          If Valore = 1 Then
            VSAM2Rdbms.DrCallRtn = "EXEC"
          End If
          
        Case "IMSUNI"
          If Valore = 1 Then
            VSAM2Rdbms.DrIMSUsing = "UNI"
          End If
          
        Case "IMSDIV"
          If Valore = 1 Then
            VSAM2Rdbms.DrIMSUsing = "DIV"
          End If
          
        Case "USGCPY"
          If Valore = 1 Then
            VSAM2Rdbms.DrUsing = "CPY"
          End If
          
        Case "USGPRE"
          If Valore = 1 Then
            VSAM2Rdbms.DrUsing = "PRE"
          End If
          
        Case "LEVNUM"
          VSAM2Rdbms.DrMaxLevIstr = Valore
        
        Case "ROUTTP"
          VSAM2Rdbms.DrTPRout = Valore
        Case "ROUTBT"
          VSAM2Rdbms.DrBTRout = Valore
        Case "ROUTGN"
          VSAM2Rdbms.DrGNRout = Valore
          
      End Select
      
      Stringa = Mid(Stringa, Idx + 1)
      
      Idx = InStr(1, Stringa, ";")
    Wend
    
    r.Close
  Else
    'Carica i default
    VSAM2Rdbms.DrCallDli = "EXEC"
    VSAM2Rdbms.DrCallRtn = "CALL"
    VSAM2Rdbms.DrIMSUsing = "UNI"
    VSAM2Rdbms.DrUsing = "CPY"
    VSAM2Rdbms.DrMaxLevIstr = 0
    VSAM2Rdbms.DrTPRout = "RTTP0000"
    VSAM2Rdbms.DrBTRout = "RTBT0000"
    VSAM2Rdbms.DrGNRout = "RTGN0000"
  End If
  
  'Nomi Database
  'Carica i nomi nell'array della classe e poi li copia in quello passato dal MAIN
  Set r = m_dllFunctions.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'DRDBD'")
    
  ReDim DrNomiDBD(0)
     
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    Stringa = r!ParameterValue
    
    Idx = InStr(1, Stringa, ";")
    
    While Idx <> 0
      
      Parametro = Mid(Stringa, 1, Idx - 1)
      
      Valore = Mid(Parametro, InStr(1, Parametro, "=") + 1)
      Parametro = Mid(Parametro, 1, InStr(1, Parametro, "=") - 1)
      
      If Valore = 1 Then
        ReDim Preserve DrNomiDBD(UBound(DrNomiDBD) + 1)
        DrNomiDBD(UBound(DrNomiDBD)) = Parametro
      End If
      
      Stringa = Mid(Stringa, Idx + 1)
      
      Idx = InStr(1, Stringa, ";")
    Wend
    
  End If
  
  Call Carica_Array_DBD_Da_Convertire(ArrayDBD)
  
  'Carica le parole da cambiare
  Set r = m_dllFunctions.Open_Recordset("Select * From BS_Parametri Where TypeKey = 'DRWORD'")
    
  ReDim DrWordChange(0)
     
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    Stringa = r!ParameterValue
    
    Idx = InStr(1, Stringa, ";")
    
    While Idx <> 0
      
      Parametro = Mid(Stringa, 1, Idx - 1)
      
      Valore = Mid(Parametro, InStr(1, Parametro, "=") + 1)
      Parametro = Mid(Parametro, 1, InStr(1, Parametro, "=") - 1)
     
      ReDim Preserve DrWordChange(UBound(DrWordChange) + 1)
      DrWordChange(UBound(DrWordChange)).Origine = Parametro
      DrWordChange(UBound(DrWordChange)).Nuova = Valore
      
      Stringa = Mid(Stringa, Idx + 1)
      
      Idx = InStr(1, Stringa, ";")
    Wend
    
  End If
  
  Call Carica_Array_Word_Da_Cambiare(ArrOR, ArrNew)
  
End Sub

Public Sub Carica_Array_DBD_Da_Convertire(Arr() As String)
  Dim i As Long
  
  ReDim Arr(0)
  
  For i = 1 To UBound(DrNomiDBD)
    ReDim Preserve Arr(UBound(Arr) + 1)
    Arr(UBound(Arr)) = DrNomiDBD(i)
  Next i
End Sub

Public Sub Carica_Array_Word_Da_Cambiare(ArrOR() As String, ArrNew() As String)
  Dim i As Long
  
  ReDim ArrOR(0)
  ReDim ArrNew(0)
  
  For i = 1 To UBound(DrWordChange)
    ReDim Preserve ArrOR(UBound(ArrOR) + 1)
    ReDim Preserve ArrNew(UBound(ArrNew) + 1)
    ArrOR(UBound(ArrOR)) = DrWordChange(i).Origine
    ArrNew(UBound(ArrNew)) = DrWordChange(i).Nuova
  Next i
End Sub

Public Sub Chekka_TreeView_Parametri(TR As TreeView, LSW As ListView)
  Dim i As Long
  
 
  For i = 1 To TR.Nodes.Count
    Select Case Trim(UCase(TR.Nodes(i).key))
      Case "DLICALL"
        If Trim(UCase(VSAM2Rdbms.DrCallDli)) = "CALL" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "DLIEXEC"
        If Trim(UCase(VSAM2Rdbms.DrCallDli)) = "EXEC" Then
          TR.Nodes(i).Checked = True
        End If
      
      Case "RTNCALL"
        If Trim(UCase(VSAM2Rdbms.DrCallRtn)) = "CALL" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "RTNEXEC"
        If Trim(UCase(VSAM2Rdbms.DrCallRtn)) = "EXEC" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "IMSUNI"
        If Trim(UCase(VSAM2Rdbms.DrIMSUsing)) = "UNI" Then
          TR.Nodes(i).Checked = True
        End If
      
      Case "IMSDIV"
        If Trim(UCase(VSAM2Rdbms.DrIMSUsing)) = "DIV" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "USGCPY"
        If Trim(UCase(VSAM2Rdbms.DrUsing)) = "CPY" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "USGPRE"
        If Trim(UCase(VSAM2Rdbms.DrUsing)) = "PRE" Then
          TR.Nodes(i).Checked = True
        End If
        
      Case "LEVNUM"
        TR.Nodes(i).text = "Number : " & VSAM2Rdbms.DrMaxLevIstr
        TR.Nodes(i).Checked = True
    End Select
  Next i
  
  'Checcka i DBD
  Dim AllBool As Boolean
  
  AllBool = True
  
  For i = 2 To TR.Nodes.Count
    If Mid(TR.Nodes(i).key, 1, 2) = "DB" Then
      If TR.Nodes(i).Checked = False Then
        AllBool = False
        Exit For
      End If
    End If
  Next i
  
  If AllBool = True Then
    TR.Nodes(1).Checked = True
  End If
  
End Sub

Public Sub Salva_Parametri(TR As TreeView, LSW As ListView)
  Dim r As ADODB.Recordset
  
  Dim i As Long
  Dim Valore As Long
  
  Dim ListaDBD() As String
  
  Dim Stringa As String
  
  ReDim ListaDBD(0)
  
  For i = 2 To TR.Nodes.Count
    If Mid(TR.Nodes(i).key, 1, 2) = "DB" Then
      ReDim Preserve ListaDBD(UBound(ListaDBD) + 1)
      
      If TR.Nodes(i).Checked = True Then
        Valore = 1
      Else
        Valore = 0
      End If
      
      ListaDBD(UBound(ListaDBD)) = TR.Nodes(i).text & "=" & Valore
    End If
  Next i
  
  'Compone la stringa
  For i = 1 To UBound(ListaDBD)
    Stringa = Stringa & ListaDBD(i) & ";"
  Next i
  
  Set r = m_dllFunctions.Open_Recordset("Select * From BS_Parametri Where Tipo = 'DRDBD'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    r!Valore = Stringa
    r.Update
    
    r.Close
  Else
    r.AddNew
    r!Tipo = "DRDBD"
    r!Valore = Stringa
    r.Update
  End If
  
  Stringa = ""
  
  'Compone la stringa
  For i = 1 To LSW.ListItems.Count
    If Trim(LSW.ListItems(i).text) <> "" Then
      Stringa = Stringa & LSW.ListItems(i).text & "=" & LSW.ListItems(i).ListSubItems(1).text & ";"
    End If
  Next i
  
  Set r = m_dllFunctions.Open_Recordset("Select * From BS_Parametri Where Tipo = 'DRWORD'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    r!Valore = Stringa
    r.Update
    
    r.Close
  Else
    r.AddNew
    r!Tipo = "DRWORD"
    r!Valore = Stringa
    r.Update
  End If
  
  
  '********************************************
  '*** PARTE CHE Salva I PARAMETRI GLOBALI ***
  '********************************************
  Stringa = ""
  
  If VSAM2Rdbms.DrCallDli = "EXEC" Then
    Stringa = "DLICALL=0;DLIEXEC=1;"
  End If
  
  If VSAM2Rdbms.DrCallDli = "CALL" Then
    Stringa = "DLICALL=1;DLIEXEC=0;"
  End If
  
  If VSAM2Rdbms.DrCallRtn = "CALL" Then
    Stringa = Stringa & "RTNCALL=1;RTNEXEC=0;"
  End If
  
  If VSAM2Rdbms.DrCallRtn = "EXEC" Then
    Stringa = Stringa & "RTNCALL=0;RTNEXEC=1;"
  End If
  
  If VSAM2Rdbms.DrIMSUsing = "UNI" Then
    Stringa = Stringa & "IMSUNI=1;IMSDIV=0;"
  End If
  
  If VSAM2Rdbms.DrIMSUsing = "DIV" Then
    Stringa = Stringa & "IMSUNI=0;IMSDIV=1;"
  End If
  
  If VSAM2Rdbms.DrUsing = "CPY" Then
    Stringa = Stringa & "USGCPY=1;USGPRE=0;"
  End If
  
  If VSAM2Rdbms.DrUsing = "PRE" Then
    Stringa = Stringa & "USGCPY=0;USGPRE=1;"
  End If
  
  Stringa = Stringa & "ROUTTP=" & VSAM2Rdbms.DrTPRout & ";"
  Stringa = Stringa & "ROUTBT=" & VSAM2Rdbms.DrBTRout & ";"
  Stringa = Stringa & "ROUTGN=" & VSAM2Rdbms.DrGNRout & ";"
  
  Stringa = Stringa & "LEVNUM=" & VSAM2Rdbms.DrMaxLevIstr
  
  Set r = m_dllFunctions.Open_Recordset("Select * From BS_Parametri Where Tipo = 'DRPARAM'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    'Edita
    r!Valore = Stringa
    r.Update
    
    r.Close
  Else
    'Aggiunge
    r.AddNew
    r!Tipo = "DRPARAM"
    r!Valore = Stringa
    r.Update
  End If
End Sub

Public Function Restituisci_Colonna_Lista_Selezionata(LSW As ListView, x As Single) As Long
  Dim IdxRiga As Long
  Dim Somma As Single
  Dim OldSomma As Single
  Dim i As Long
  
  Somma = 0
  
  IdxRiga = LSW.SelectedItem.Index
  
  'Trova la colonna selezionata
  For i = 1 To LSW.ColumnHeaders.Count
    OldSomma = Somma
    
    If x > OldSomma And x < Somma + LSW.ColumnHeaders(i).Width Then
      Restituisci_Colonna_Lista_Selezionata = i
      Exit Function
    End If
    
    Somma = Somma + LSW.ColumnHeaders(i).Width
  Next i
End Function

Public Function Trova_Primo_Carattere_In_Avanti(RT As RichTextBox, Idx As Long) As Long
  Dim i As Long
  
  For i = 1 To 100
    RT.SelStart = Idx + i
    RT.SelLength = 1
    
    If RT.SelText <> " " Then
      Trova_Primo_Carattere_In_Avanti = Idx + i
      Exit For
    End If
  Next i
End Function
'stefanopul
'Public Function is_Pcb(WidOggetto As Long, wNome As String) As e_TypePcb
'   Dim rs As ADODB.Recordset
'   Dim wIdArea As Long
'   Dim wPcbStructure As t_PcbTemplate
'   Dim wPcb As e_TypePcb
'   Dim wPicture As String
'   Dim wFlagPcbIO As Boolean
'   Dim wFlagPcbALT As Boolean
'   Dim wFlagPcbSTD As Boolean
'   Dim cc As Long
'   Dim SumStructure As Long
'   Dim rsCopy As ADODB.Recordset
'   Dim linkIdOgg As Long
'
'   'Dovr� essere esternalizzata i un template
'   wPcbStructure = load_PcbStructure_Template()
'   linkIdOgg = WidOggetto
'
'   Set rs = m_dllFunctions.Open_Recordset("Select * From PsData_Cmp Where Nome = '" & Trim(wNome) & "' And IdOggetto = " & WidOggetto)
'   If rs.RecordCount > 0 Then
'      wIdArea = rs!IdArea
'   Else
'      'Controlla se esiste in una copy linkata dal programma
'         'SP corso : pazzesco, ma chi l'ha fatto, paperoga? Funzionava solo se la copy aveva
'         'lo stesso nome del LIVELLO 01!!!!!!!!!!!!!!!!!!!!!!!!!!
'      'Set rsCopy = m_dllFunctions.Open_Recordset("Select * From PsRel_Obj Where IdOggettoC = " & WidOggetto & " And Relazione = 'CPY'")
'      Set rsCopy = m_dllFunctions.Open_Recordset("Select * From PsRel_Obj Where IdOggettoC = " & WidOggetto & " and NomeComponente = '" & Trim(wNome) & "' And Relazione = 'CPY'")
'
'      If rsCopy.RecordCount > 0 Then
'         Do While Not rsCopy.EOF
'         'SP corso : pazzesco, ma chi l'ha fatto, paperoga? Funzionava solo se la copy aveva
'         'lo stesso nome del LIVELLO 01!!!!!!!!!!!!!!!!!!!!!!!!!!
'            'Set rs = m_dllFunctions.Open_Recordset("Select * From PsData_Cmp Where Nome = '" & Trim(wNome) & "' And IdOggetto = " & rsCopy!IdOggettoR)
'            Set rs = m_dllFunctions.Open_Recordset("Select * From PsData_Cmp Where livello = 1 And IdOggetto = " & rsCopy!IdOggettoR)
'
'            If rs.RecordCount > 0 Then
'               wIdArea = rs!IdArea
'               linkIdOgg = rs!IdOggetto
'               Exit Do
'            End If
'
'            rsCopy.MoveNext
'         Loop
'      End If
'
'      rsCopy.Close
'
'      If wIdArea = 0 Then
'         is_Pcb = IS_NOT_A_PCB
'         Exit Function
'      End If
'   End If
'   rs.Close
'
'   wPcb = True
'   Set rs = m_dllFunctions.Open_Recordset("Select * From PsData_Cmp Where IdOggetto = " & linkIdOgg & " And idArea = " & wIdArea & _
'                                 " And Nome <> 'FILLER' And Livello <> 88 And Livello <> 77 And Tipo <> 'A' Order by Ordinale")
'   If rs.RecordCount > 0 Then
'      If rs.RecordCount = 1 Then
'         is_Pcb = IS_NOT_A_PCB
'         Exit Function
'      End If
'
'      'rs.MoveNext 'avanza di uno
'
'      wFlagPcbIO = True
'
'      Do While Not rs.EOF
'         'Testa il PCB-IO
'         If rs.AbsolutePosition <= UBound(wPcbStructure.IO_Pcb.Details) Then
'            If rs!segno <> wPcbStructure.IO_Pcb.Details(rs.AbsolutePosition).wSegno Then
'               wFlagPcbIO = False
'               Exit Do
'            End If
'
'            If rs!Lunghezza <> wPcbStructure.IO_Pcb.Details(rs.AbsolutePosition).wLunghezza Then
'               wFlagPcbIO = False
'               Exit Do
'            End If
'
'            If rs!Tipo <> wPcbStructure.IO_Pcb.Details(rs.AbsolutePosition).wTipo Then
'               wFlagPcbIO = False
'               Exit Do
'            End If
'
'            If rs!Usage <> wPcbStructure.IO_Pcb.Details(rs.AbsolutePosition).wUsage Then
'               wFlagPcbIO = False
'               Exit Do
'            End If
'         End If
'
'         rs.MoveNext
'      Loop
'
'      'Testa il PCB Alternate
'      wFlagPcbALT = True
'      rs.MoveFirst
'      'rs.MoveNext
'
'      Do While Not rs.EOF
'         If rs.AbsolutePosition <= UBound(wPcbStructure.ALT_Pcb.Details) Then
'            If rs!segno <> wPcbStructure.ALT_Pcb.Details(rs.AbsolutePosition).wSegno Then
'               wFlagPcbALT = False
'               Exit Do
'            End If
'
'            If rs!Lunghezza <> wPcbStructure.ALT_Pcb.Details(rs.AbsolutePosition).wLunghezza Then
'               wFlagPcbALT = False
'               Exit Do
'            End If
'
'            If rs!Tipo <> wPcbStructure.ALT_Pcb.Details(rs.AbsolutePosition).wTipo Then
'               wFlagPcbALT = False
'               Exit Do
'            End If
'
'            If rs!Usage <> wPcbStructure.ALT_Pcb.Details(rs.AbsolutePosition).wUsage Then
'               wFlagPcbALT = False
'               Exit Do
'            End If
'         End If
'
'         rs.MoveNext
'      Loop
'
'      If wFlagPcbALT Then
'         If rs.RecordCount <> UBound(wPcbStructure.ALT_Pcb.Details) Then
'            wFlagPcbALT = False
'         End If
'      End If
'
'      'Testa il PCB standard
'      wFlagPcbSTD = True
'      rs.MoveFirst
'      'rs.MoveNext
'
'      Do While Not rs.EOF
'         If rs.AbsolutePosition <= UBound(wPcbStructure.STD_Pcb.Details) Then
'            If rs!segno <> wPcbStructure.STD_Pcb.Details(rs.AbsolutePosition).wSegno Then
'               wFlagPcbSTD = False
'               Exit Do
'            End If
'
'            If rs!Lunghezza <> wPcbStructure.STD_Pcb.Details(rs.AbsolutePosition).wLunghezza Then
'               wFlagPcbSTD = False
'               Exit Do
'            End If
'
'            If rs!Tipo <> wPcbStructure.STD_Pcb.Details(rs.AbsolutePosition).wTipo Then
'               wFlagPcbSTD = False
'               Exit Do
'            End If
'
'            If rs!Usage <> wPcbStructure.STD_Pcb.Details(rs.AbsolutePosition).wUsage Then
'               wFlagPcbSTD = False
'               Exit Do
'            End If
'         End If
'
'         rs.MoveNext
'      Loop
'   End If
'   rs.Close
'
'   If wFlagPcbALT = False And wFlagPcbIO = False And wFlagPcbSTD = False Then
'      wPcb = IS_NOT_A_PCB
'   Else
'      If wFlagPcbALT Then wPcb = ALTERNATE_PCB
'      If wFlagPcbIO Then wPcb = IO_Pcb
'      If wFlagPcbSTD Then wPcb = STANDARD_PCB
'
'      is_Pcb = wPcb
'      Exit Function
'   End If
'
'   If wPcb = IS_NOT_A_PCB Then
'      'Controlla ulteriormente se viene almeno rispettato (8-2-2) + Lunghezza totale 24 STD - 28 I/O
'      Set rs = m_dllFunctions.Open_Recordset("Select * From PsData_Cmp Where IdOggetto = " & linkIdOgg & " And idArea = " & wIdArea & _
'                                 " And Livello <> 88 And Livello <> 77 and Tipo <> 'A' Order by Ordinale")
'
'      If rs.RecordCount = 1 Then
'         is_Pcb = IS_NOT_A_PCB
'         Exit Function
'      End If
'
'      If rs.RecordCount >= 3 Then
'         rs.MoveFirst
'         cc = 1
'         wFlagPcbSTD = True
'
'         'STD PCB : 8-2-2 Fisso obbligatorio
'         If rs!Byte = 0 Then 'Non � giusta questa if (risolvere il parsing che incolla 0)
'            If wPcbStructure.STD_Pcb.Details(1).wLunghezza <> rs!Lunghezza Then wFlagPcbSTD = False
'         Else
'            If wPcbStructure.STD_Pcb.Details(1).wLunghezza <> rs!Byte Then wFlagPcbSTD = False
'         End If
'
'         rs.MoveNext
'         If rs!Byte = 0 Then 'Non � giusta questa if (risolvere il parsing che incolla 0)
'            If wPcbStructure.STD_Pcb.Details(2).wLunghezza <> rs!Lunghezza Then wFlagPcbSTD = False
'         Else
'            If wPcbStructure.STD_Pcb.Details(2).wLunghezza <> rs!Byte Then wFlagPcbSTD = False
'         End If
'         rs.MoveNext
'
'         If rs!Byte = 0 Then 'Non � giusta questa if (risolvere il parsing che incolla 0)
'            If wPcbStructure.STD_Pcb.Details(2).wLunghezza <> rs!Lunghezza Then wFlagPcbSTD = False
'         Else
'            If wPcbStructure.STD_Pcb.Details(2).wLunghezza <> rs!Byte Then wFlagPcbSTD = False
'         End If
'         rs.MoveNext
'
'         While Not rs.EOF
'            If cc + 3 <> rs.RecordCount Then 'Salta l'ultimo perch� � variabile
'               SumStructure = SumStructure + rs!Byte
'            End If
'
'            cc = cc + 1
'            rs.MoveNext
'         Wend
'
'         If SumStructure < wPcbStructure.STD_Pcb.FixedLength Then
'            wFlagPcbSTD = False
'         End If
'
'         'PCB ALTERNATE
'         rs.MoveFirst
'         cc = 1
'         wFlagPcbALT = True
'
'         'ALT PCB : 8-2-2 Fisso obbligatorio
'         If wPcbStructure.ALT_Pcb.Details(1).wLunghezza <> rs!Byte Then wFlagPcbALT = False
'         rs.MoveNext
'         If wPcbStructure.ALT_Pcb.Details(2).wLunghezza <> rs!Byte Then wFlagPcbALT = False
'         rs.MoveNext
'         If wPcbStructure.ALT_Pcb.Details(3).wLunghezza <> rs!Byte Then wFlagPcbALT = False
'         rs.MoveNext
'
'         While Not rs.EOF
'            If cc + 3 <> rs.RecordCount Then 'Salta l'ultimo perch� � variabile
'               SumStructure = SumStructure + rs!Byte
'            End If
'
'            cc = cc + 1
'            rs.MoveNext
'         Wend
'
'         If SumStructure < wPcbStructure.STD_Pcb.FixedLength Then
'            wFlagPcbALT = False
'         End If
'
'         'PCB I/O
'         rs.MoveFirst
'         cc = 1
'         wFlagPcbIO = True
'
'         'STD PCB : 8-2-2 Fisso obbligatorio
'         If wPcbStructure.IO_Pcb.Details(1).wLunghezza <> rs!Byte Then wFlagPcbIO = False
'         rs.MoveNext
'         If wPcbStructure.IO_Pcb.Details(2).wLunghezza <> rs!Byte Then wFlagPcbIO = False
'         rs.MoveNext
'         If wPcbStructure.IO_Pcb.Details(3).wLunghezza <> rs!Byte Then wFlagPcbIO = False
'         rs.MoveNext
'
'         While Not rs.EOF
'            If cc + 3 <> rs.RecordCount Then 'Salta l'ultimo perch� � variabile
'               SumStructure = SumStructure + rs!Byte
'            End If
'
'            cc = cc + 1
'            rs.MoveNext
'         Wend
'
'         If SumStructure < wPcbStructure.IO_Pcb.FixedLength Then
'            wFlagPcbIO = False
'         End If
'      End If
'
'      rs.Close
'   End If
'
'   If wFlagPcbALT = False And wFlagPcbIO = False And wFlagPcbSTD = False Then
'      wPcb = IS_NOT_A_PCB
'   Else
'      If wFlagPcbALT Then wPcb = ALTERNATE_PCB
'      If wFlagPcbIO Then wPcb = IO_Pcb
'      If wFlagPcbSTD Then wPcb = STANDARD_PCB
'   End If
'
'   is_Pcb = wPcb
'End Function
'stefanopul
'Public Function load_PcbStructure_Template() As t_PcbTemplate
'   Dim wApp As t_PcbTemplate
'   Dim wFile As String
'   Dim nFile As Long
'   Dim fLen As Long
'   Dim sLine As String
'   Dim wPcbIo As Boolean
'   Dim wPcbAlt As Boolean
'   Dim wPcbStd As Boolean
'   Dim s() As String
'   Dim Fixed As String
'
'   'Apre il file contenente i template
'   wFile = m_dllFunctions.FnPathPrd & "\System\Masks\pcb.msk"
'
'   nFile = FreeFile
'   Open wFile For Binary As nFile
'
'   fLen = FileLen(wFile)
'
'   ReDim wApp.IO_Pcb.Details(0)
'   ReDim wApp.ALT_Pcb.Details(0)
'   ReDim wApp.STD_Pcb.Details(0)
'
'   While Loc(nFile) < fLen
'      Line Input #nFile, sLine
'
'      If Mid(sLine, 1, 1) <> "*" And Trim(sLine) <> "" Then
'         If InStr(1, sLine, "<PCB-IO>") > 0 Then wPcbIo = True
'         If InStr(1, sLine, "</PCB-IO>") > 0 Then wPcbIo = False
'         If InStr(1, sLine, "<PCB-ALT>") > 0 Then wPcbAlt = True
'         If InStr(1, sLine, "</PCB-ALT>") > 0 Then wPcbAlt = False
'         If InStr(1, sLine, "<PCB-STD>") > 0 Then wPcbStd = True
'         If InStr(1, sLine, "</PCB-STD>") > 0 Then wPcbStd = False
'
'         s = Split(Trim(sLine), " ")
'
'         If s(0) = "L1" Then
'            If InStr(1, sLine, " FIX:") > 0 Then
'               Fixed = Replace(s(2), "FIX:", "")
'            End If
'         End If
'
'         'Carica i vari template
'         If wPcbIo Then
'            If s(0) <> "L1" Then
'               ReDim Preserve wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details) + 1)
'               wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details)).wSegno = s(1)
'               wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details)).wTipo = s(2)
'               wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details)).wLunghezza = s(3)
'               wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details)).wUsage = s(4)
'               wApp.IO_Pcb.FixedLength = Fixed
'
'               wApp.IO_Pcb.Details(UBound(wApp.IO_Pcb.Details)).wPicture = Replace(s(1), "N", "") & s(2) & "(" & s(3) & ") " & s(4)
'            End If
'         End If
'
'         If wPcbAlt Then
'            If s(0) <> "L1" Then
'               ReDim Preserve wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details) + 1)
'               wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details)).wSegno = s(1)
'               wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details)).wTipo = s(2)
'               wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details)).wLunghezza = s(3)
'               wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details)).wUsage = s(4)
'               wApp.ALT_Pcb.FixedLength = Fixed
'
'               wApp.ALT_Pcb.Details(UBound(wApp.ALT_Pcb.Details)).wPicture = Replace(s(1), "N", "") & s(2) & "(" & s(3) & ") " & s(4)
'            End If
'         End If
'
'         If wPcbStd Then
'            If s(0) <> "L1" Then
'               ReDim Preserve wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details) + 1)
'               wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details)).wSegno = s(1)
'               wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details)).wTipo = s(2)
'               wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details)).wLunghezza = s(3)
'               wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details)).wUsage = s(4)
'               wApp.STD_Pcb.FixedLength = Fixed
'
'               wApp.STD_Pcb.Details(UBound(wApp.STD_Pcb.Details)).wPicture = Replace(s(1), "N", "") & s(2) & "(" & s(3) & ") " & s(4)
'            End If
'         End If
'      End If
'   Wend
'
'   Close nFile
'
'   load_PcbStructure_Template = wApp
'End Function

'SQ: 28-07-05
Public Function getEnvironmentParam_SQ(ByVal parameter As String, envType As String, environment As collection) As String
  Dim token As String
  Dim subTokens() As String
  'intervallo:
  Dim intervallo() As String
  
  On Error GoTo errdb
  
  Select Case envType
    Case "colonne"
      'environment = rsColonne,rsTabelle
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "GROUP-NAME"
              'token = getGroupName_colonne(environment.Item(1)!IdOggetto)
            Case "COLUMN-NAME"
              token = environment.Item(1)!nome
            Case "ALIAS"
              If m_dllFunctions.FnNomeDB = "banca.mty" Then
                token = Left(environment.Item(2)!nome, 2) & "N" & Mid(environment.Item(2)!nome, 10)
              Else
                'fare...
              End If
            Case Else
              MsgBox "Variable unknown: " & subTokens(0)
          End Select
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        End If
      Wend
    Case "tabelle"
      'environment = rsTabelle,rsSegmenti
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "ROUTE-NAME"
              token = getRouteName_tabelle(environment.Item(2)!idOggetto)
            Case "SEGM-PROGR"
              token = getSegProgr_tabelle(environment.Item(2))
            Case "SEGM-NAME"
              token = environment.Item(2)!nome
            Case "TABLE-NAME"
              token = getTableName_tabelle(environment.Item(2)!idOggetto)
            Case "ALIAS"
              If m_dllFunctions.FnNomeDB = "banca.mty" Then
                token = Left(environment.Item(2)!nome, 2) & "N" & Mid(environment.Item(1)!nome, 10)
              Else
                'fare...
              End If
            Case "NOME-AREA"  'Area Cobol associata al segmento:
              'token = getNomeArea_tabelle(environment.Item(2)!IdOggetto)
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        End If
      Wend
    Case "dbd"
      'environment = nome DBD
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "ROUTE-NAME"
              token = getRouteName_dbd(environment.Item(1))
            Case "TABLE-NAME"
              token = getTableName_dbd(environment.Item(1))
            Case "DBD-NAME"
              token = environment.Item(1)
            'Case "SEGM-PROGR"
            '  token = getSegProgr_tabelle(environment.Item(2))
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        End If
      Wend
    Case "else"
      '...
  End Select
  Exit Function
errdb:
  If Err.Number = 13 Then
    'sbagliato l'intervallo numerico
    MsgBox "Wrong definition for " & parameter, vbExclamation
  Else
    MsgBox Err.Description
    'Resume
  End If
  getEnvironmentParam_SQ = ""
End Function
'SQ
Public Function getRouteName_dbd(NomeDbD As String) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB

  Set r = m_dllFunctions.Open_Recordset("SELECT Route_name from psDli_DBD where DBD_name='" & NomeDbD & "'")
  
  If r.RecordCount Then
    getRouteName_dbd = r(0)
  Else
    getRouteName_dbd = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getRouteName_dbd = ""
End Function
'SQ
Public Function getTableName_dbd(NomeDbD As String) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB

  Set r = m_dllFunctions.Open_Recordset("SELECT Table_name from psDli_DBD where DBD_name='" & NomeDbD & "'")
  
  If r.RecordCount Then
    getTableName_dbd = r(0)
  Else
    getTableName_dbd = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getTableName_dbd = ""
End Function
'SQ
Public Function getSegProgr_tabelle(rs As ADODB.Recordset) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB
  
  If InStr(rs.Source, " MgDLI_Segmenti ") Then
    Set r = m_dllFunctions.Open_Recordset("SELECT count(*) from MgDLI_Segmenti where idOggetto = " & rs!idOggetto & " AND idSegmento <= " & rs!idSegmento)
  Else
    Set r = m_dllFunctions.Open_Recordset("SELECT count(*) from PsDLI_Segmenti where idOggetto = " & rs!idOggetto & " AND idSegmento <= " & rs!idSegmento)
  End If
  If r.RecordCount Then
    getSegProgr_tabelle = Format(r(0), "000")
  Else
    getSegProgr_tabelle = "000"
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getSegProgr_tabelle = "000"
End Function
'SQ
Public Function getRouteName_tabelle(IdDBD As Long) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB

  Set r = m_dllFunctions.Open_Recordset("SELECT Route_name from psDli_DBD,bs_oggetti where bs_oggetti.nome=psDli_DBD.DBD_name AND bs_oggetti.idOggetto = " & IdDBD)

  If r.RecordCount Then
    getRouteName_tabelle = r(0)
  Else
    getRouteName_tabelle = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getRouteName_tabelle = ""
End Function
'SQ
Public Function getTableName_tabelle(IdDBD As Long) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB

  Set r = m_dllFunctions.Open_Recordset("SELECT Table_name from psDli_DBD,bs_oggetti where bs_oggetti.nome=psDli_DBD.DBD_name AND bs_oggetti.idOggetto = " & IdDBD)
  If r.RecordCount Then
    getTableName_tabelle = r(0)
  Else
    getTableName_tabelle = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getTableName_tabelle = ""
End Function
'SQ
Public Function getDliCopies_BPER(rsTabelle As ADODB.Recordset) As String
  Dim inCondition As String
  Dim rsCopies As ADODB.Recordset
  
  On Error GoTo errdb
  
  inCondition = rsTabelle!IdOrigine
  rsTabelle.MoveNext
  While Not rsTabelle.EOF
    inCondition = inCondition & "," & rsTabelle!IdOrigine
    rsTabelle.MoveNext
  Wend
  'ripristiono per il chiamante
  rsTabelle.MoveFirst
  
  Set rsCopies = m_dllFunctions.Open_Recordset("select distinct nome from bs_oggetti as a,MgRel_SegAree as b where StrIdOggetto=IdOggetto AND " & _
                                                 "IdSegmento IN (" & inCondition & ")")
  getDliCopies_BPER = "COPY " & rsCopies(0) & "."
  rsCopies.MoveNext
  While Not rsCopies.EOF
    getDliCopies_BPER = getDliCopies_BPER & vbCrLf & Space(11) & "COPY " & rsCopies(0) & "."
    rsCopies.MoveNext
  Wend
  rsCopies.Close
  
  Exit Function
errdb:
  MsgBox Err.Description
  getDliCopies_BPER = ""
End Function
