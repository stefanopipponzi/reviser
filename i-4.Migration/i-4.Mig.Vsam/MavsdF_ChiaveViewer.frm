VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form MavsdF_ChiaveViewer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Areas Viewer"
   ClientHeight    =   5310
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6810
   Icon            =   "MavsdF_ChiaveViewer.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5310
   ScaleWidth      =   6810
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOpen 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   5850
      Picture         =   "MavsdF_ChiaveViewer.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   4410
      Width           =   855
   End
   Begin RichTextLib.RichTextBox rtKey 
      Height          =   825
      Left            =   60
      TabIndex        =   0
      Top             =   90
      Width           =   6645
      _ExtentX        =   11721
      _ExtentY        =   1455
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"MavsdF_ChiaveViewer.frx":1E8C
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin RichTextLib.RichTextBox rtStruct 
      Height          =   3345
      Left            =   60
      TabIndex        =   1
      Top             =   990
      Width           =   6645
      _ExtentX        =   11721
      _ExtentY        =   5900
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"MavsdF_ChiaveViewer.frx":1F0C
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "MavsdF_ChiaveViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOpen_Click()
   Unload Me
End Sub

Public Function compose_key() As String
   Dim testo As String
   Dim rs As Recordset
   Dim i  As Long
   Dim wSpace As Long
   
   'stefano: non aveva senso, ora va un po' meglio, anche se sarebbe da migliorare...
   'testo = ""
   testo = Istruzione.SSA(AULivello).NomeSegmento
   testo = testo & Istruzione.SSA(AULivello).CommandCode
   'If Istruzione.SSA(AULivello).Chiavi Is Nothing Then Exit Function
   For i = 1 To UBound(Istruzione.SSA(AULivello).Chiavi)
      If i = 1 Then
         testo = testo & "("
      End If
      'testo = testo & Format(i, "00") & ")"
      testo = testo & Istruzione.SSA(AULivello).Chiavi(i).key
      'wSpace = 12 - Len(Istruzione.SSA(AULivello).Chiavi(i).key)
      'testo = testo & wSpace & " " & Istruzione.SSA(AULivello).Chiavi(i).Operatore & Space(2 - Len(Istruzione.SSA(AULivello).Chiavi(i).Operatore))
      testo = testo & " " & Istruzione.SSA(AULivello).Chiavi(i).Operatore
      testo = testo & " " & Istruzione.SSA(AULivello).Chiavi(i).Valore.Valore
      If UBound(Istruzione.SSA(AULivello).OpLogico) >= i Then
         testo = testo & " " & Istruzione.SSA(AULivello).OpLogico(i)
      End If
      testo = testo & " "
      If i = UBound(Istruzione.SSA(AULivello).Chiavi) Then
         testo = testo & ")"
      End If
   Next i

   compose_key = testo
End Function

Public Function compose_keyStr(wId As Long, wIdArea As Long) As String
   Dim testo As String
   Dim rs As Recordset, rsc As Recordset
   'Dim i  As Long
   Dim wIndent As Long
   Dim oldLev As Long
   'Dim cc As Long
   'Dim MinLev As Long
   'Dim wSpace As Long
   'Dim wFactor As Long
   'Dim wFactorPic As Long
   
 'stefano: probabilmente era difficile farlo pi� contorto...
 ' non bastava leggere le informazioni e metterle a video
'   cc = 0
'   testo = ""
'   oldLev = 0
'
'   Set rs = m_fun.Open_Recordset("Select * From PsData_Cmp Where IdOggetto = " & wId & " And IdArea = " & wIdArea & " Order by Ordinale")
'
'   If rs.RecordCount > 0 Then
'      rs.MoveFirst
'     wIndent = 2
'
'      Do While Not rs.EOF
'         If Len(rs!nome) > wFactor Then wFactor = Len(rs!nome)
'         If Len(rs!Picture) > wFactorPic Then wFactorPic = Len(rs!Picture)
'
'         If oldLev < rs!Livello Then
'            cc = cc + 1
'         Else
'            If oldLev <> rs!Livello Then cc = cc - 1
'         End If
'
'         If wIndent < cc * 2 Then wIndent = cc * 2
'         oldLev = rs!Livello
'
'         rs.MoveNext
'      Loop
'
'      wFactor = wFactor + wIndent
'      wFactorPic = wFactorPic
'   End If
'
'   wIndent = 0
'   cc = 0
'   oldLev = 0
'
'   If rs.RecordCount > 0 Then
'      rs.MoveFirst
'      MinLev = rs!Livello
'
'      Do While Not rs.EOF
'         If rs!Livello < MinLev Then
'            Exit Do
'         End If
'
'         If oldLev < rs!Livello Then
'            cc = cc + 1
'            wIndent = cc * 2
'         Else
'            If oldLev <> rs!Livello Then
'               cc = cc - 1
'            End If
'
'            wIndent = cc * 2
'         End If
'
'         testo = testo & Space(wIndent) & Format(rs!Livello, "00")
'         testo = testo & " " & rs!nome
'
'         'Calcola wSpace
'         wSpace = wFactor - wIndent - Len(rs!nome)
'
'         If Trim(rs!Picture) <> "" Then
'            testo = testo & Space(wSpace) & " PIC " & rs!Picture
'
'            wSpace = wFactorPic - Len(rs!Picture)
'
'            If Trim(rs!Valore) <> "" Then
'               testo = testo & Space(wSpace) & " VALUE " & rs!Valore
'            End If
'         End If
'
'         testo = testo & vbCrLf
'
'         oldLev = rs!Livello
'
'         If MinLev > rs!Livello Then
'            MinLev = rs!Livello
'         End If
'
'         rs.MoveNext
'      Loop
'   End If
'
'   rs.Close
'
'   cc = 0
'   testo = ""
'   oldLev = 0
   
   Set rs = m_fun.Open_Recordset("Select * From PsData_Cmp Where IdOggetto = " & wId & " And IdArea = " & wIdArea & " Order by Ordinale")
   
   While Not rs.EOF
      If oldLev < rs!Livello Then
         wIndent = wIndent + 3
      Else
         If oldLev > rs!Livello Then wIndent = wIndent - 3
      End If
      oldLev = rs!Livello
      'livello e nome
      testo = testo & Space(wIndent) & Format(rs!Livello, "00") & " " & rs!nome
      If rs!Tipo <> "A" Then
         'PIC, tipo e segno
         testo = testo & " PIC " & IIf(rs!Segno = "S", "S", "") & rs!Tipo
         'parentesi e picture
         testo = testo & "(" & rs!Lunghezza & ")" & IIf(rs!Decimali <> 0, "V" & rs!Tipo & "(" & rs!Decimali & ")", "")
         'valore; se ce mettete gli spazi sul db non ve la prendete con me
         If Trim(rs!Valore) <> "" Then
            testo = testo & " VALUE " & rs!Valore
         End If
      End If
      testo = testo & "." & vbCrLf
      If rs!idCopy > 0 Then
         Set rsc = m_fun.Open_Recordset("Select * From PsData_Cmp Where idOggetto = " & rs!idCopy & " order by ordinale")
         While Not rsc.EOF
            If oldLev < rsc!Livello Then
               wIndent = wIndent + 3
            Else
               If oldLev > rsc!Livello Then wIndent = wIndent - 3
            End If
            oldLev = rsc!Livello
            'livello e nome
            testo = testo & Space(wIndent) & Format(rsc!Livello, "00") & " " & rsc!nome
            If rsc!Tipo <> "A" Then
               'PIC, tipo e segno
               testo = testo & " PIC " & IIf(rsc!Segno = "S", "S", "") & rsc!Tipo
               'parentesi e picture
               testo = testo & "(" & rsc!Lunghezza & ")" & IIf(rsc!Decimali <> 0, "V" & rsc!Tipo & "(" & rsc!Decimali & ")", "")
               'valore
               If Trim(rsc!Valore) <> "" Then
                  testo = testo & " VALUE " & rsc!Valore
               End If
            End If
            testo = testo & "." & vbCrLf
            rsc.MoveNext
         Wend
         rsc.Close
      End If
      rs.MoveNext
   Wend
   rs.Close
   compose_keyStr = testo
End Function

Private Sub Form_Load()
   rtKey.text = compose_key
   rtStruct.text = compose_keyStr(Istruzione.SSA(AULivello).idOggetto, Istruzione.SSA(AULivello).IdArea)
End Sub
