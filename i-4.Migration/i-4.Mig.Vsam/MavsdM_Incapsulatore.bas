Attribute VB_Name = "MavsdM_Incapsulatore"
Option Explicit

'SQ - 17-07-06
'Ex modulo _Incaps
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Global nomeroutine As String
Global GbNumIstr As String
Global percorsoPSB As String
Global m_ImsDll As New MavsdC_Menu

'VARIABILI PER DEFINIRE L'ESITO DELLA VERIFICA DEI PSB E DELLE ISTRUZIONI
'IN BASE A QUESTE VERRANNO CARICATE LE IMMAGINI NELLA TREEVIEW
Global PSBFlag As Boolean
Global DBDFlag  As Boolean
Global IstrFlag As Boolean
Global SegmFlag As Boolean
'FLAG PER NON FARE PARTIRE LA VERIFICA OGNI VOLTA CHE LA FINESTRA DIVENTA ATTIVA.........
Global IncaSwitch As Boolean
'variabili per la verifica
Global switch As Boolean
Global InfoFlag As Long
'nome da passare all'editor
Global NomeEDitor As String
'variabili per controllo sul numero degli oggetti
Global OGGLista As Long
Global OGG_V_ERR As Long
'serve per contare gli oggetti che non hanno errori di tipo 'V'
Global OGG_Count As Long
'************************************************
'*** indica i PSB che non ci sono fisicamente ***
'************************************************
Public Type ID_PSB_M
    Id As Variant
    Num As Variant
End Type

Public Type PSB_M
    Num As Long
    nome As String
    id_PSB() As ID_PSB_M
    Num_ID As Variant
End Type
'************************************************

'************************************************
'*** indica i PSB non ASSOCIATI *****************
'************************************************
Type PSB_N_A
    nome As String
    Id As Long
End Type

'************************************************
'*** indica le dipendenze dei PSB ***************
'************************************************
Public Type PSB_DIP
    Id As Variant
    nome As String
    TipoChiamata As String
End Type

Global PSB_MANCANTI() As PSB_M
Global PSB_NON_ASS() As PSB_N_A

'************************************************
'*** indica i DBD mancanti fisicamente **********
'************************************************
Type DBD_M
    NomeDbD As String
    IdOgg As Long
    NomeOgg As String
    TipoOgg As String
    Directory As String
End Type

Global DBD_MANCANTI() As DBD_M

'indica le dipendenze dei psb
Global DIPENDENZE_PSB() As PSB_DIP

'serve per tenere traccia delle righe aggiunte espandendo la cpy
Global RighePiu As Variant


'indica quale bottone per il controllo della molteplicit� � stato premuto
'es. segmenti, strutture, key ecc...
Global Selezione As String

'serve per tenere in memoria l'ultima textbox attiva e quale bottome cmdMostra � attivo
Global txtFlag As Integer
Global MostraFlag As String

'serve per determinare memorizzare il valore precedente
Public ValPrevious As String

'indica L ' IDOGGETTO dell'istruzione selezionata nella form IncapsAUTO e la riga di origine,
'la riga attuale e la chiave
Global ID_ISTRUZIONE As Variant
Global RigaOR_ISTRUZIONE As Long
Global CHIAVE_ISTRUZIONE As Variant
Global RigaC_ISTRUZIONE As Long
Global TESTO_ISTRUZIONE   As String

'indica L 'ssa cliccata sulla treeview, servir� per effettuare le modifiche
Global SSA As Long

'****************************************************
'costanti per puntare il nodo giusto  della treeview
Public Const Segm_Const = 4
Public Const Key_Const = 7
Public Const Strutt_Const = 6
Public Const Condiz_Const = 10
Public Const Val_Const = 9
Public Const Oper_Const = 8
Public Const Routine_Const = 11
Public Const Qualif_Const = 5
'togliere dopo un test effettivo di non utilizzo
'****************************************************
'
'*****************************************************************************
Type BlkIstrKey
    key As String
    xName As Boolean
    Op As String
    KeyVal As String
    KeyLen As String
    KeyBool As String
    KeyNum As Integer
End Type

Type BlkIstrDli
    ssaName As String
    SsaLen As Integer
    Segment As String
    Tavola As String
    IdSegm As Double
    Record As String
    SegLen As String
    CodCom As String
    Parentesi As String
    Search As String
    NomeRout As String
    valida As Boolean
    KeyDef() As BlkIstrKey
End Type

''''Type IstrDli
''''    Dbd As String
''''    PCB As String
''''    Istr As String
''''    PSBName As String
''''    PsbId As Double
''''    BlkIstr() As BlkIstrDli
''''End Type

'****************************************************
'**** indica per ogni istruzione i suoi dettagli ****
'****************************************************
Type RIGHE_CALL
    Sintassi As String
    Id As Variant
    riga As Variant
    RigaOrigine As Variant
    Istruzione As Variant
    IdStrutture As Variant
    IdPSB As Variant
    IdSegm As Variant
    LungSegmento As Variant
    LungKey As Variant
End Type

'****************************************************
'**** indica per ogni istruzione i suoi livelli ***** da fare:
'****************************************************
Type ISTR_LIVEL
    Id As Variant
    riga As Variant
    Livello As Variant
    IdStruttura As Variant
    IdSegm As Variant
    IdField As Variant
    operator As Variant
    Valore As Variant
    condizione As Variant
End Type
Type xCmpPcb
    NomePCB As String
    Campo(9) As String
End Type

Global CmpPcb() As xCmpPcb

Global LivelloIstr() As ISTR_LIVEL
Global RigheAnalisi() As RIGHE_CALL

''''''''Global TabIstr As IstrDli
Global percorso As String
Global rs, rs_NDb As Recordset

'flag che serve per azzerare la variabile statica indice
Global BFlag As Boolean
Type wOpeIstrRt
   Istruzione As String
   Decodifica As String
   ordPcb As Long
   pcbDyn As Integer
End Type
Global OpeIstrRt() As wOpeIstrRt

'indica la directori di origine del file da incapsulare e la sua directory
Global DIROr As String
Global DirCBLcpy As String

Private Const PERCORSO_TEMPLATE = "Input-Prj\TEMPLATE\Imsdb\esternalizzazione\Incaps\VSAM"

'serve per tenere il conto delle righe da aggiungere alle istruzioni dopo aver espanso le CPY nel programma stesso
Type wCPY
     IdObj As Long
     RigaCpy As Long
     NumRighe As Long
     NomeCpy As String
End Type
Global Exp_CPY() As wCPY

'contiene le istruzioni non codificate di un'eventuale copy contenente il DLI
Type IstrCpy
     idOggetto As Long
     riga As Long
     Statment As String
     RigaCpyBase As Long
     RigaCpyExp As Long
     Istruzione As String
End Type
Global IstrCPYDli() As IstrCpy
Type KeyFDVsam
   nome As String
   Tipo As String
   Start As Integer
   Len As Integer
End Type
Global IDPGMIncaps As Long
Global DBDNameIncaps  As String
Global NomeRoutineIncaps As String
Global KeyLenGuida As Integer

'mantiene il numero del livello di chiamata nelle routine DB2
Global LNKDBNumstack() As String
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public NomeProg As String
Public OldName As String
Public OldName1 As String

'Variabili Per incapsulatore
Public AddRighe As Long
Public wIdent As Integer
Public wPunto As Boolean
Public j As Long
Public posizione As Long

Public SwLink As Boolean

Public Type BlkIstrKeyLocal
    key As String
    xName As Boolean
    NameAlt As String
    Op As String
    KeyVal As String
    KeyLen As String
    KeyBool As String
    KeyNum As Integer
    KeyVarField As String
    KeyStart As Long
    OpLogic As String
End Type

Public Type BlkIstrDliLocal
    ssaName As String
    SsaLen As Integer
    Segment As String
    SegmentM As String
    Tavola As String
    IdSegm As Double
    Record As String
    SegLen As Long
    CodCom As String
    Parentesi As String
    Search As String
    NomeRout As String
    valida As Boolean
    KeyDef() As BlkIstrKeyLocal
'GSAM: lunghezza dell'area diversa da quella del segmento
    AreaLen As Long
End Type
Public Type IstrDliLocal
    dbd() As t_Ogg
    PCB As String
    PCBRed As String
    NumPCB As Long
    Istr As String
    psb() As t_Ogg
    BlkIstr() As BlkIstrDliLocal
    keyfeedback As String
    SavedAreas() As String
'modifiche AC
'Auth
    authClassname As String
    authResource As String
'Inquy
   inqyAibsfunc As String
   inqyAibrsnm1 As String
    procSeq As String
    procOpt As String
    isGsam As Boolean
End Type
Global TabIstr As IstrDliLocal
'Global TabIstr As Istruzione
Global testo As String
Global QualAndUnqual As Boolean
Global OnlyQual As Boolean, OnlySqual As Boolean
Global wMoltIstr(4) As collection
Global wNomeRout() As String

'SP 4/9
Global origPCB()

Public Type tbSegm
   IdFiglio As Long
   NomeFiglio As String
   NomeParent As String
   IdParent As Long
   IdDBD As Long
   AreaLen As Long
   KeyLen As Long
   keyName As String
   ssaName As String
   Op As String
End Type

'stefano
Global mSegmentoPadre As tbSegm
  'molteplicit�
Global multiOp As Boolean
  'molteplicit� generale sull'istruzione
Global multiOpGen As Boolean
Global distinctCod As collection, distinctSeg As collection, distinctDB As collection
Private MatriceCombinazioni() As Integer
Private CombinazioniOpLogici() As Integer
Private CombinazioniComCode() As Integer
Private Combinazionisegmenti() As Integer
Private Dcombination As Boolean
'SQ 26-07-06 - Sostituire completamente la gestione del Load_Labels nella Funzioni
Dim START_TAG As String, END_TAG As String
Dim wPath As String
Public EmbeddedRoutine As Boolean
Public indentkey As Integer
Private segLev As Integer
Global isPLI As Boolean

Private Function settaCorrettaRigaSelect(lineaInput As String, colName As collection) As String
  Dim splFind() As String
  Dim splCrlf() As String
  Dim x As Integer
  Dim token As String
  Dim link As String
  Dim retVal As String
  Dim rs As Recordset
  Dim strSQL As String
  
On Error GoTo ErrH
  '1)con il campo dopo ASSIGN TO accedere alla tabella
  '  PSRel_File e ricavare DsnName
  '  con il dsnName accedere alla tabella DMVSM_Archivio
  '  e ricavare il campo Nome
  '  con il Nome accedere alla tabella DMVSAM_NomiVSAM
  '  e verificare se il campo ToMigrate � flaggato
  '  se si asteriscare tutta la select.
  ''''Attenzione alle eventuali righe gi� asteriscate


  Dim prevValue As String
  'cicla sull'input
  splFind = Split(lineaInput, " ")
  
  For x = 0 To UBound(splFind)
    If Trim(splFind(x)) <> "" Then
      If prevValue = "SELECT" Then
        'ricava la stringa da comparare nella funzione FD
        token = splFind(x)
      ElseIf prevValue = "TO" Then
        splFind(x) = Replace(Replace(splFind(x), ".", ""), vbCrLf, "")
        'ricava la condizione di where
        link = splFind(x)
        Exit For
      End If
      prevValue = splFind(x)
    End If
  Next
  x = 0

  strSQL = " SELECT c.To_Migrate from " & _
           " psrel_file as a, " & _
           " dmvsm_archivio as b, " & _
           " dmvsm_nomivsam As c " & _
           " where a.dsnname = b.nome and " & _
           " b.nome = c.nomevsam " & _
           " and a.link ='" & link & "'"
  Set rs = m_fun.Open_Recordset(strSQL)

  If Not rs.EOF Then
    If rs.Fields(0) = True Then
      'aggiunge il controllo da fare nella funzione FD
      colName.Add token
      'se � su + linee
      splCrlf = Split(lineaInput, vbCrLf)
      If UBound(splCrlf) > 0 Then
        For x = 0 To UBound(splCrlf)
          If Mid(splCrlf(x), 7, 1) <> "*" Then
            splCrlf(x) = Left(splCrlf(x), 6) & "*" & Mid(splCrlf(x), 8)
          End If
          If retVal = "" Then
            retVal = splCrlf(x)
          Else
            retVal = retVal & vbCrLf & splCrlf(x)
          End If
        Next
        
      Else
        If Mid(lineaInput, 7, 1) <> "*" Then
          retVal = Left(lineaInput, 6) & "*" & Mid(lineaInput, 8)
        End If
      End If
    Else
      retVal = lineaInput
    End If
  Else
    retVal = lineaInput
  End If

  settaCorrettaRigaSelect = retVal

  Exit Function
ErrH:
  'gestire l'errore
End Function
Private Function settaCorrettaRigaFD(lineaInput As String, colWS As collection, colN As collection) As String
'verifica se il nome contenuto nella colN � presente nella lineaInput
'se si asterisca tutto. Okkio alle linee gi� asteriscate
'Carica inoltre la struttura nella collection colWS
  Dim retVal As String
  Dim splFD() As String
  Dim splCrlf() As String
  Dim x As Integer, j As Integer
  Dim bOk As Boolean, bNumeric As Boolean
  Dim prevValue As String
  
  
  
'  settaCorrettaRigaFD = lineaInput
'  Exit Function
  
  
  splFD = Split(lineaInput)
  bOk = False
  
  'cicla sulla riga fd e controlla se uno dei valori contenuti
  'nella collection � presente
  For x = 0 To UBound(splFD)
    If Trim(splFD(x)) <> "" Then
      If prevValue = "FD" Then
        For j = 1 To colN.Count
          If colN.Item(j) = Replace(Replace(splFD(x), vbCrLf, ""), ".", "") Then bOk = True: Exit For
        Next
        Exit For
      End If
      prevValue = splFD(x)
    End If
  Next
  x = 0
  
  If bOk Then
    'asterisca tutto
    splCrlf = Split(lineaInput, vbCrLf)
    If UBound(splCrlf) > 0 Then
      For x = 0 To UBound(splCrlf)
        If IsNumeric(Mid(Trim(splCrlf(x)), 2, 2)) Or bNumeric Then
          bNumeric = True
          colWS.Add splCrlf(x)
        End If
        If Mid(splCrlf(x), 7, 1) <> "*" Then
          splCrlf(x) = Left(splCrlf(x), 6) & "*" & Mid(splCrlf(x), 8)
        End If
        If retVal = "" Then
          retVal = splCrlf(x)
        Else
          retVal = retVal & vbCrLf & splCrlf(x)
        End If
      Next
    Else          'poco probabile... per�
      If Mid(lineaInput, 7, 1) <> "*" Then
        retVal = Left(lineaInput, 6) & "*" & Mid(lineaInput, 8)
      End If
    End If
  End If
    'inserisce i numerici nella collection
  

settaCorrettaRigaFD = retVal


End Function


Public Function IncapsIstr() As String


End Function



Public Sub Controlla_CBL_CPY(RT As RichTextBox, RTBErr As RichTextBox, rs As Recordset, typeIncaps As String)
  '2) Controlla se l'istruzione Corrente fa parte Del CBL o la CPY gi� aperta, se si non carica il CBL o CPY
  'ma li aggiorna:
  Dim NCBLcpy As String
  Dim dirPath As String
  Dim dirPathOpen As String
  Dim dirPathOther As String
  Dim temp As String
  Dim linea As String
  
  Dim free As Integer
  Dim indice As Integer
  Dim i As Integer
  Dim linee As Integer
  Dim x As Integer
  Dim riga As Integer
  Dim righeAggiunte As Integer
  
  Dim isFd As Boolean
  Dim ws As Boolean

  Dim r As Recordset
    
  Dim collection As New collection               'carica tutte le righe del file
  Dim collectionWS As New collection             'carica la parte di codice da inserire dopo la ws
  Dim collectionName As New collection           'carica i nomi da confrontare nell'FD
  Dim collectionTemplateOpen As New collection   'carica il template x le Open/Close
  Dim collectiontemplateOther As New collection  'carica il template x le altre istruzioni
  

On Error GoTo Err
  

''  AddRighe = 0
  Set r = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & rs!idOggetto)
  If r.RecordCount Then
    If Trim(r!Estensione) = "" Then
       NCBLcpy = r!nome
       NomeProg = r!nome
    Else
       NCBLcpy = r!nome & "." & r!Estensione
       NomeProg = r!nome & "." & r!Estensione
    End If

    DirCBLcpy = Crea_Directory_Progetto(r!Directory_Input, VSAM2Rdbms.drPathDef)

    'ritorna il percorso del progetto
    'es: c:\progetti\PIPPO\
    dirPath = VSAM2Rdbms.drPathDef & Mid(r!Directory_Input, InStr(1, r!Directory_Input, "\"), InStr(InStr(1, r!Directory_Input, "\") + 1, r!Directory_Input, "\") - InStr(1, r!Directory_Input, "\") + 1)
    dirPath = dirPath & PERCORSO_TEMPLATE
    If Dir(dirPath, vbDirectory) = "" Then
      MsgBox "Directory: " & vbCrLf & dirPath & vbCrLf & "Not found. Encapsulation aborted.", vbInformation, "Encapsulation aborted"
      Exit Sub
    End If
    r.Close
    
    'carica il template OPEN-CLOSE
    dirPathOpen = dirPath & "\OPEN-CLOSE"
    free = FreeFile
    Open dirPathOpen For Input As free
    While Not EOF(free)
      Line Input #free, linea
      collectionTemplateOpen.Add linea
    Wend
    linea = ""
    
    'carica il template READ-WRITE
    dirPathOther = dirPath & "\READ-WRITE"
    free = FreeFile
    Open dirPathOpen For Input As free
    While Not EOF(free)
      Line Input #free, linea
      collectiontemplateOther.Add linea
    Wend
    linea = ""
    
    'cicla sul file
    free = FreeFile
    Open DirCBLcpy & "\" & NCBLcpy For Input As free
    'setta i valori iniziali
    linee = 0
    i = 1
    temp = ""
    isFd = False
    ws = False
    linea = ""
    righeAggiunte = 0
    
    'prende le istruzioni da incapsulare
    Set r = m_fun.Open_Recordset("SELECT * FROM PSVSM_ISTRUZIONI WHERE " & _
            " Idoggetto = " & rs!idOggetto & " order by Riga, RelRow")
    
    If r.RecordCount > 0 Then r.MoveFirst
    
    Do While Not EOF(free)
      Line Input #free, linea
        If ws Then
        ''' inserimento controllo istruzioni da asteriscare
          If collection.Count = r!riga + collectionWS.Count + righeAggiunte Then
            riga = r!riga
            While r!riga = riga
              If r!Istruzione = "OPEN" Or r!Istruzione = "CLOSE" Then
                collection.Add Left(linea, 6) & "*" & Mid(linea, 8)
                'temp = IncapsIstr(r, collectionName, collectionTemplateOpen, righeAggiunte)
              Else
                'verifica se � su + linee
                'in questo caso asterisca tutte le linee
                If r!linee = 1 Then ''ok solo su una linea
                  collection.Add Left(linea, 6) & "*" & Mid(linea, 8)
                Else
                  'cicla sul numero di linee e le asterisca
                  For x = 1 To r!linee
                    collection.Add Left(linea, 6) & "*" & Mid(linea, 8)
                    Line Input #free, linea
                  Next
                End If
                'temp = IncapsIstr(r, collectionName, collectiontemplateOther, righeAggiunte)
                collection.Add temp
              End If
              r.MoveNext
              riga = r!riga
            Wend
          Else
            collection.Add linea
          End If
        ElseIf InStr(1, linea, ".") = 0 Then
          If InStr(1, Mid(linea, 7, 66), "FD") > 0 And isFd Then
            temp = settaCorrettaRigaFD(temp, collectionWS, collectionName)
''Debug.Print "funzione fd: " & vbCrLf & temp
            collection.Add CStr(temp)
            temp = ""
            linee = 1
          End If
          If (Mid(linea, 7, 1) = "*" Or Trim(linea) = "") And Not isFd Then
            collection.Add CStr(linea)
          Else
            If temp <> "" Then
              temp = temp & vbCrLf & linea
            Else
              temp = linea
            End If
            linee = linee + 1
          End If
          
        Else
          If InStr(1, Mid(linea, 7, 66), "WORKING-STORAGE") > 0 Then
            ws = True
            If InStr(1, temp, "FD") > 0 Then
              'funzione x fd temp
              temp = settaCorrettaRigaFD(temp, collectionWS, collectionName)
''Debug.Print "funzione fd: " & vbCrLf & temp
              collection.Add CStr(temp)
              collection.Add CStr(linea) '''& vbCrLf
              ''aggiungere linee struttura
              For x = 1 To collectionWS.Count
                collection.Add collectionWS.Item(x)
              Next
            End If
            'funzione x caricare fd in ws
          ElseIf Replace(temp, vbCrLf, "") <> "" Then
              temp = temp & vbCrLf & linea
              linee = linee + 1
              If InStr(1, temp, "SELECT") > 0 Then
''Debug.Print "funzione select: " & vbCrLf & temp
                temp = settaCorrettaRigaSelect(temp, collectionName)
                'funzione x select temp
                collection.Add CStr(temp)
              End If
              If Not isFd Then
                linee = 0
                temp = ""
              End If
          Else
          '  Debug.Print "controllo secco!"
            If InStr(1, Mid(linea, 7, 66), "FD") > 0 Then
              isFd = True
            Else
              'controllo su resto
              If InStr(1, linea, "SELECT") > 0 Then
''Debug.Print "funzione select: " & vbCrLf & linea
                linea = settaCorrettaRigaSelect(linea, collectionName)
              'funzione x select linea
              End If
              collection.Add CStr(linea)
            End If
            
            If isFd Then
              If temp = "" Then
                temp = linea
              Else
                temp = temp & vbCrLf & linea
              End If
              linee = linee + 1
            Else
              linee = 0
            End If
            
          End If

        End If
     i = i + 1
   Loop
  
    free = FreeFile
    Open "c:\___AAA1___test.txt" For Output As free
  
    For i = 1 To collection.Count
      Debug.Print collection(i)
      'Write #free, "pippo"
      Print #free, CStr(collection(i))
    Next
    Close
  End If
  Exit Sub
    '***********************************************
    '***********************************************
    
    
''''
''''    If RT.text = "" Then
''''      If MavsdF_Incapsulatore.Optsource Then
''''        RT.LoadFile DirCBLcpy & "\" & NCBLcpy
''''      Else
''''        RT.LoadFile percorso & "\" & NCBLcpy
''''      End If
''''
''''      If r!Tipo <> "CPY" Then
''''          If r!Tipo = "EZT" Then
''''            Inizializza_Copy_IMSDB_EZT RT, RTBErr
''''          Else
''''            Inizializza_Copy_IMSDB RT, RTBErr, r!Tipo, rs!idOggetto
''''          End If
''''      End If
''''    Else
''''      If RT.fileName <> DirCBLcpy & "\" & NCBLcpy Then
''''        'Salva il file
''''        Crea_Directory_Cbl_Cpy (DirCBLcpy) 'crea le directory CBLOut e CPYOut se non esistono
''''
''''        RT.SaveFile OldName, 1
''''
''''        If Tip = "CBL" Then
''''          'inserisce i CBL modificate anche nella tabella oggetti del DB
''''        Else
''''          'inserisce le CPY modificate anche nella tabella oggetti del DB
''''        End If
''''
''''        If MavsdF_Incapsulatore.Optsource Then
''''          RT.LoadFile DirCBLcpy & "\" & NCBLcpy
''''        Else
''''          RT.LoadFile percorso & "\" & NCBLcpy
''''        End If
''''        If r!Tipo <> "CPY" Then
''''          If r!Tipo = "EZT" Then
''''            Inizializza_Copy_IMSDB_EZT RT, RTBErr
''''          Else
''''            Inizializza_Copy_IMSDB RT, RTBErr, r!Tipo, rs!idOggetto
''''          End If
''''        End If
''''      End If
''''    End If
''''
''''
''''    'oldName = DirCBLcpy & "\Out\" & NCBLcpy
''''    OldName = percorso & "\" & NCBLcpy
''''    OldName1 = NCBLcpy
''''    On Error GoTo Err
''''   End If
''''   Exit Sub
Err:
   MsgBox "Error in Incapsulatore.Controlla_CBL_CPY:" & Err.Number & vbCrLf & Err.Description, vbCritical, "Error"
End Sub

Sub Crea_Directory_Cbl_Cpy(DirCBLcpy As String)
    '1) Controlla se esistono le 2 directory di output Routine, se no le crea:
    If Dir(DirCBLcpy & "\Out", vbDirectory) = "" Then
      'crea directory
      MkDir DirCBLcpy & "\Out"
    End If
End Sub

Private Function getLevKey(idOggetto As Long, ssaName As String) As String()
  'accedere alla psdata_cmp con idoggetto e ssaname
  'ricavare i valori che vanno da dopo l'operatore di confronto
  'fino al campo che ha ')'.
  'nel caso di '*' ricavare i valori nuovamente
  'L'array parte da 1 come da STANDARD
  Dim rs As Recordset
  Dim strQRY As String
  Dim boolOperatore As Boolean
  Dim levKey() As String
  Dim indice As Integer
  
  strQRY = " SELECT * from psdata_cmp " & _
           " Where idOggetto =" & idOggetto & " and idarea in (" & _
           " select idArea from psdata_cmp " & _
           " where idoggetto=" & idOggetto & " and nome='" & ssaName & "')" & _
           " and nome<>'" & ssaName & "' order by ordinale "
  
  Set rs = m_fun.Open_Recordset(strQRY)
  
  ReDim Preserve levKey(0)
  
  If Not rs.EOF Then
    boolOperatore = False
    indice = 1
    While Not rs.EOF
      If InStr(1, rs!Valore, "(") = 0 And InStr(1, rs!Valore, ")") = 0 And InStr(1, rs!Valore, "*") = 0 Then
        Select Case Right(Trim(rs!Valore), 2)
          Case "EQ", "GE", "GT", "LE", "LT", "NE"
            boolOperatore = True
          Case Else
            If boolOperatore Then
              ReDim Preserve levKey(indice)
              If levKey(indice) = "" Then
                levKey(indice) = rs!nome
              Else
                levKey(indice) = levKey(indice) & " AND " & rs!nome
              End If
            End If
        End Select
      Else
        'buttare solo le parantesi.... attenzione al *
        If InStr(1, rs!Valore, "*") > 0 Then
          indice = indice + 1
        End If
      End If
      rs.MoveNext
    Wend
  End If
  getLevKey = levKey
  
End Function

Private Sub getSegLevel(Id As Long, Optional nome As String = "")
'ricava il livello del segmento passato in input dalla
'asteriscaIstruzione_EZT

  Dim rs As Recordset
  Dim strQRY As String
  
  If nome = "" Then
    strQRY = " Select * from PSDLI_Segmenti where IdSegmento=" & Id
  Else
    strQRY = " Select * from PSDLI_Segmenti where IdOggetto=" & Id & _
             " and Nome='" & nome & "'"
  End If
  
  Set rs = m_fun.Open_Recordset(strQRY)

  If Not rs.EOF Then
    segLev = segLev + 1
    If rs!parent <> "0" Then
      getSegLevel rs!idOggetto, rs!parent: Exit Sub
    End If
  End If
  rs.Close
  
End Sub

Public Sub Inizializza_Copy_IMSDB(RT As RichTextBox, RTBErr As RichTextBox, ptype As String, pIdOggetto As Long)
    'controlla se il link con la copy LNKDBRT � presente nel file
    Dim t, w As Long
    Dim testo As String
    Dim s() As String
    Dim nFile As Long
    Dim lfile As Long
    Dim sLine As String
    Dim templateName As String
    Dim keyword As String
    
    ReDim CmpPcb(0)
    keyword = " WORKING-STORAGE "
    If ptype = "PLI" Then
      keyword = " BUILTIN"
    End If
    
    t = -1
    If Not ptype = "PLI" Then
      t = RT.find("COPY LNKDBRT", 1)
    End If
    RT.Refresh
    AddRighe = 0
    
    If t = -1 Then
        w = RT.find(keyword, 1)
        For t = w To 1 Step -1
          If Mid(RT.text, t, 2) = vbCrLf Then
             If Not ptype = "PLI" Then
             If Mid(RT.text, t + 8, 1) = "*" Then
                 w = RT.find(keyword, w + 1)
              End If
             Else
              If InStr(Mid(RT.text, t + 1, 72), "/*") > 0 Or InStr(Mid(RT.text, t + 1, 72), "*/") > 0 Then
                 w = RT.find(keyword, w + 1)
              End If
             End If
             Exit For
          End If
        Next t
        
        w = RT.find(vbCrLf, w)
        w = w + 1
        
        If Not ptype = "PLI" Then
          'Template di Working:
          If EmbeddedRoutine Then
  'stefanopul
            'templateName = m_fun.FnPathPrj & "\input-prj\Template\INCAPS\WorkingPgmDB"
            templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\WorkingPgmDB"
          Else
  'stefanopul
            'templateName = m_fun.FnPathPrj & "\input-prj\routbs\InitCpyRoutIMSDB.tpl"
            templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps\InitCpyRoutIMSDB.tpl"
          End If
          
          If Len(Dir(templateName, vbNormal)) Then
            nFile = FreeFile
            lfile = FileLen(templateName)
            
            Open templateName For Binary As nFile
            testo = vbCrLf
            While Loc(nFile) < lfile
              Line Input #nFile, sLine
              
              'Trascodifica le label
              If InStr(1, sLine, "#LABEL_AUTHOR#") > 0 Then
                  sLine = Replace(sLine, "#LABEL_AUTHOR#", m_fun.LABEL_AUTHOR)
                  testo = testo & vbCrLf & sLine
                  
              ElseIf InStr(1, sLine, "#LABEL_INFO#") > 0 Then
                  sLine = Replace(sLine, "#LABEL_INFO#", m_fun.LABEL_INFO)
                  testo = testo & vbCrLf & sLine
                  
              ElseIf InStr(1, sLine, "#LABEL_INCAPS#") > 0 Then
                  sLine = Replace(sLine, "#LABEL_INCAPS#", m_fun.LABEL_INCAPS)
                  testo = testo & vbCrLf & sLine
              Else
                  testo = testo & LABEL_INFO & vbCrLf & sLine
              End If
              
              
              'testo = testo & vbCrLf & sLine
            Wend
            Close nFile
          Else
            'SQ - E' sempre bello scrivere dentro i programmi generati!
            RTBErr.text = "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
            testo = vbCrLf
            testo = testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
            testo = testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
            testo = testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
          End If
        End If
        If ptype = "PLI" Then
          insertPLIPtrVariables testo, pIdOggetto, RT
        End If
        
        RT.SelStart = w
        RT.SelLength = 0
        RT.SelText = testo
        
        s = Split(testo, vbCrLf)
        AddRighe = AddRighe + UBound(s)
        
    End If
End Sub

Sub insertPLIPtrVariables(testo As String, poggetto As Long, RT As RichTextBox)
  Dim rsNumPcb As Recordset, testoback As String, t As Long, w As Long, s() As String

  testo = testo & vbCrLf & "     %INCLUDE LNKDBRT;"
  Set rsNumPcb = m_fun.Open_Recordset("select distinct NUMPcb from PsDli_Istruzioni where IdOggetto = " & poggetto)
  While Not rsNumPcb.EOF
    testo = testo & vbCrLf & "     DCL PTR_SAVE_" & rsNumPcb!NumPCB & "            PTR;"
    testoback = testoback & "      " & rsNumPcb!NumPCB & "        = PTR_SAVE_" & rsNumPcb!NumPCB & ";" & vbCrLf
    rsNumPcb.MoveNext
  Wend
  rsNumPcb.Close
  w = RT.find("RETURN;", 1)
  If Not w = -1 Then
    For t = w To 1 Step -1
      If Mid(RT.text, t, 2) = vbCrLf Then
        If InStr(Mid(RT.text, t + 1, 72), "/*") > 0 Or InStr(Mid(RT.text, t + 1, 72), "*/") > 0 Then
          w = RT.find("RETURN;", w + 1)
        End If
        Exit For
      End If
    Next t
    RT.SelStart = t + 1
    RT.SelLength = 0
    RT.SelText = testoback
    s = Split(testoback, vbCrLf)
    AddRighe = AddRighe + UBound(s)
  End If
End Sub

Public Sub Inizializza_Copy_IMSDB_EZT(RT As RichTextBox, RTBErr As RichTextBox)
    Dim t As Long, w As Long
    Dim testo As String
    Dim s() As String
    Dim nFile As Long
    Dim lfile As Long
    Dim sLine As String
    Dim templateName As String
    
    'stefano: deve cambiare la definizione dei file dbd in aree di di working
    ' per ora faccio finta che l'offset macro non esista...
    
    
    
    ReDim CmpPcb(0)
    Dim wJI() As String
    Dim x As Long
  On Error GoTo ErrJob
''''    t = RT.Find("COPY LNKDBRT", 1)
''''    RT.Refresh
''''    AddRighe = 0
    
''''    If t = -1 Then
        'wJI = Split(sLine, " ")
        'w = RT.Find("JOB INPUT", 1) 'MATTEOOOO
        Dim foundx As Boolean
        foundx = False
        w = 1
        'stefano: un po meglio, ma ancora non gestisce le asteriscate, ecc.
        While Not foundx
          w = RT.find("JOB ", w + 1)
          x = RT.find(vbCrLf, w) 'VIRGILIOOO
          x = RT.find("INPUT", w, x)
          If x > 0 Then
           foundx = True
          End If
        Wend
''''        For t = w To 1 Step -1
''''          If Mid(RT.text, t, 2) = vbCrLf Then
''''             If Mid(RT.text, t + 8, 1) = "*" Then
''''                w = RT.Find(" WORKING-STORAGE ", w + 1)
''''             End If
''''             Exit For
''''          End If
''''        Next t
        
        w = RT.find(vbCrLf, w - 2)
        w = w - 1
        RT.SelStart = w
        RT.SelLength = 0
        
        'Template di Working:
        If EmbeddedRoutine Then
          templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\EZT\WorkingPgmEZTDB"
        Else
          templateName = m_fun.FnPathPrj & "\input-prj\Template\imsdb\standard\incaps\EZT\InitCpyRoutIMSDBezt.tpl"
        End If
        
        If Len(Dir(templateName, vbNormal)) Then
          nFile = FreeFile
          lfile = FileLen(templateName)
          
          Open templateName For Binary As nFile
          testo = vbCrLf
          While Loc(nFile) < lfile
            Line Input #nFile, sLine
            
            'Trascodifica le label
            If InStr(1, sLine, "#LABEL_AUTHOR#") > 0 Then
                sLine = Replace(sLine, "#LABEL_AUTHOR#", m_fun.LABEL_AUTHOR)
                testo = testo & vbCrLf & sLine
                
            ElseIf InStr(1, sLine, "#LABEL_INFO#") > 0 Then
                sLine = Replace(sLine, "#LABEL_INFO#", m_fun.LABEL_INFO)
                testo = testo & vbCrLf & sLine
                
            ElseIf InStr(1, sLine, "#LABEL_INCAPS#") > 0 Then
                sLine = Replace(sLine, "#LABEL_INCAPS#", m_fun.LABEL_INCAPS)
                testo = testo & vbCrLf & sLine
            Else
                testo = testo & LABEL_INFO & vbCrLf & sLine
            End If
            
            
            'testo = testo & vbCrLf & sLine
          Wend
          Close nFile
        Else
          'SQ - E' sempre bello scrivere dentro i programmi generati!
          RTBErr.text = "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
          testo = vbCrLf
          testo = testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
          testo = testo & m_fun.LABEL_INFO & "* TEMPLATE NOT FOUND: " & templateName & vbCrLf
          testo = testo & m_fun.LABEL_INFO & "*------------------------------------------------------" & vbCrLf
        End If
        
        RT.SelText = testo
        
        s = Split(testo, vbCrLf)
        AddRighe = UBound(s)
''''    End If
        Exit Sub
ErrJob:
  MsgBox "JOB INPUT not found "

End Sub

Public Function Seleziona_Istruzioni(Selezione As String, rIst As Recordset) As Long
  Dim criterio As String
  Dim k1 As Long
  
  criterio = ""
  
  Select Case Selezione
    Case "SRC" 'Tutti i programmi
      'Set rIst = m_fun.Open_Recordset("Select distinct idoggetto  From BS_Oggetti Where Dli = true Order By IdOggetto") 'virgilio
      Set rIst = m_fun.Open_Recordset("Select distinct idoggetto  From BS_Oggetti Where Dli = true and tipo = 'CBL' Order By IdOggetto")
    Case "SEL" 'Solo i programmi Selezionati
      For k1 = 1 To VSAM2Rdbms.drObjList.ListItems.Count
        If VSAM2Rdbms.drObjList.ListItems(k1).Selected = True Then
           If Trim(criterio) <> "" Then
              criterio = criterio & ", " & Val(VSAM2Rdbms.drObjList.ListItems(k1).text)
           Else
              criterio = Val(VSAM2Rdbms.drObjList.ListItems(k1).text)
           End If
        End If
      Next k1
      
      If Trim(criterio) = "" Then Exit Function
      
      criterio = " IdOggetto in (" & criterio & ") "
      
      Set rIst = m_fun.Open_Recordset("Select distinct idoggetto From BS_Oggetti Where " & criterio & " And Dli = True Order By IdOggetto")
      
  End Select
  
  'Si posiziona al primo recordset
  If rIst.RecordCount > 0 Then
    rIst.MoveLast
    Seleziona_Istruzioni = rIst.RecordCount
  Else
    Seleziona_Istruzioni = 0
  End If
  
End Function

Public Function Restituisci_Selezione(TR As TreeView) As String
  Dim i As Long
  
  For i = 1 To TR.Nodes.Count
    If TR.Nodes(i).Checked Then
      Restituisci_Selezione = Trim(UCase(TR.Nodes(i).key))
      Exit For
    End If
  Next i
End Function

Function getPCDRedFromPCB(pPCB As String, pIdOggetto As Long) As String
  Dim rsData As Recordset
  
  Set rsData = m_fun.Open_Recordset("select Nome from PsData_Cmp where Nomered = '" & pPCB & "' or Nomered = '(" & pPCB & ")' and IdOggetto = " & pIdOggetto)
  If Not rsData.EOF Then
    getPCDRedFromPCB = rsData!nome
  End If
  rsData.Close
End Function

Function CaricaIstruzione(rst As Recordset) As Boolean
  Dim tb As Recordset
  Dim tb1 As Recordset
  Dim tb2 As Recordset
  Dim tb3 As Recordset
  
  Dim IndI As Integer
  Dim K As Integer
  Dim k1 As Integer
  Dim k2 As Integer
  Dim k3 As Integer
  Dim z As Integer
  
  Dim u As Long
  Dim wXDField As Boolean
  
  Dim vEnd As Double
  Dim vPos As Double
  Dim vPos1 As Double
  Dim vStart As Double
  Dim wstr As String
  Dim TbLenField As Recordset
  
  Dim sDbd() As Long
  Dim sPsb() As Long
  Dim sSegD() As String
  
  Dim TbKey As Recordset
  
  ReDim TabIstr.BlkIstr(0)
  ReDim TabIstr.dbd(0)
  ReDim TabIstr.psb(0)
  
  SwTp = False
  Set tb = m_fun.Open_Recordset("select * from BS_Oggetti where idoggetto = " & rst!idOggetto)
  If tb.RecordCount > 0 Then
     SwTp = tb!Cics
  End If
  tb.Close
  
  'SQ CPY-ISTR
  Set tb = m_fun.Open_Recordset( _
    "SELECT * FROM PsDli_IstrDett_Liv WHERE " & _
    "idPgm=" & rst!idpgm & " AND idoggetto=" & rst!idOggetto & " and riga = " & rst!riga & " order by livello")
             
  '''''''''''''''DEVE CARICARE PI� DBD e pi�PSB
  sDbd = m_fun.getDBDByIdOggettoRiga(rst!idOggetto, rst!riga)
  sPsb = m_fun.getPSBByIdOggettoRiga(rst!idOggetto, rst!riga)
  
  For u = 1 To UBound(sDbd)
    ReDim Preserve TabIstr.dbd(u)
    TabIstr.dbd(u).Id = sDbd(u)
    TabIstr.dbd(u).nome = Restituisci_NomeOgg_Da_IdOggetto(sDbd(u))
    TabIstr.dbd(u).Tipo = TYPE_DBD
  Next u
'per ora � senza vettore
  TabIstr.isGsam = False
  If UBound(TabIstr.dbd) > 0 Then
    Set tb1 = m_fun.Open_Recordset("select * from BS_Oggetti where tipo_DBD = 'GSA' and nome = '" & TabIstr.dbd(1).nome & "'")
    TabIstr.isGsam = Not tb1.EOF
    tb1.Close
  End If
  For u = 1 To UBound(sPsb)
    ReDim Preserve TabIstr.psb(u)
    TabIstr.psb(u).Id = sPsb(u)
    TabIstr.psb(u).nome = Restituisci_NomeOgg_Da_IdOggetto(sPsb(u))
    TabIstr.psb(u).Tipo = TYPE_PSB
  Next u
  
  TabIstr.PCB = rst!NumPCB & ""
  TabIstr.PCBRed = getPCDRedFromPCB(rst!NumPCB, rst!idOggetto)
  'SQ
  TabIstr.NumPCB = IIf(IsNull(rst!ordPcb), 0, rst!ordPcb)
  
  If UBound(TabIstr.psb) Then
     TabIstr.procSeq = Restituisci_ProcSeq(TabIstr.psb(1).Id, TabIstr.NumPCB)
     TabIstr.procOpt = Restituisci_ProcOpt(TabIstr.psb(1).Id, TabIstr.NumPCB)
  Else
     TabIstr.procSeq = ""
     TabIstr.procOpt = ""
  End If
  
  If Trim(rst!Istruzione) = "QUERY" Then
    TabIstr.Istr = "QRY"
  Else
    TabIstr.Istr = Trim(rst!Istruzione)
  End If
  
  'ginevra: keyfeedback
  If Trim(rst!keyfeedback) <> "" Then
     TabIstr.keyfeedback = Trim(rst!keyfeedback)
  Else
     TabIstr.keyfeedback = ""
  End If
    
  If TabIstr.Istr = "CHKP" Or TabIstr.Istr = "TERM" Or _
     TabIstr.Istr = "SYNC" Or TabIstr.Istr = "PCB" Or _
     TabIstr.Istr = "QRY" Then
   
   SwTp = m_fun.Is_CICS(rst!idOggetto)
       
   ' stefano: ma come fa a funzionare che indi non � impostato? Oppure zero va bene come valore?
   If UBound(TabIstr.dbd) > 0 Then
      TabIstr.BlkIstr(IndI).NomeRout = m_fun.Genera_Nome_Routine(TabIstr.dbd(1).nome, TabIstr.Istr, SwTp)
    Else
      TabIstr.BlkIstr(IndI).NomeRout = m_fun.Genera_Nome_Routine("", TabIstr.Istr, SwTp)
    End If
  End If
  
  If tb.RecordCount > 0 Then
    tb.MoveFirst
    IndI = 0
    
    While Not tb.EOF
       IndI = IndI + 1
       ReDim Preserve TabIstr.BlkIstr(IndI)
       k3 = 0
       ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
       
       SwTp = m_fun.Is_CICS(tb!idOggetto)
       
       TabIstr.BlkIstr(IndI).ssaName = Trim(tb!NomeSSA & " ")
       ' lunghezza area (impostat solo per i GSAM per ora)
       If Not IsNull(tb!AreaLen) And tb!AreaLen > 0 Then
          TabIstr.BlkIstr(IndI).AreaLen = tb!AreaLen
       Else
          TabIstr.BlkIstr(IndI).AreaLen = 0
       End If
       '<NONE>
       If Len(TabIstr.BlkIstr(IndI).ssaName) Then
       'stefano: SSA costante
          If InStr(TabIstr.BlkIstr(IndI).ssaName, "'") > 0 Then
             TabIstr.BlkIstr(IndI).SsaLen = Len(TabIstr.BlkIstr(IndI).ssaName) - 2 'toglie gli apici
          Else
            TabIstr.BlkIstr(IndI).SsaLen = m_fun.TrovaLenSSA(TabIstr.BlkIstr(IndI).ssaName, rst!idOggetto)
          End If
       End If
       
       'SQ tmp...
       TabIstr.BlkIstr(IndI).Parentesi = IIf(tb!Qualificazione, "(", " ")
                     
       TabIstr.BlkIstr(IndI).SegLen = tb!SegLen
       TabIstr.BlkIstr(IndI).Search = IIf(IsNull(tb!condizione), "", tb!condizione)

       If UBound(TabIstr.dbd) > 0 Then
         TabIstr.BlkIstr(IndI).NomeRout = m_fun.Genera_Nome_Routine(TabIstr.dbd(1).nome, TabIstr.Istr, SwTp)
       Else
         TabIstr.BlkIstr(IndI).NomeRout = m_fun.Genera_Nome_Routine("", TabIstr.Istr, SwTp)
       End If
       'alpitour: 5/8/2005 forzato diversamente; la gestione della data area andava fatta meglio
       ' o perlomeno completata prima di renderla effettiva;
       ' in questo modo si sono aumentate le mancanze, in quanto la generazione routine
       ' faceva un ragionamento diverso
       'If tb!StrIdOggetto > 0 Then
       If Len(tb!dataarea) Then
         TabIstr.BlkIstr(IndI).Record = tb!dataarea & ""
       End If
       
       If tb!idSegmento = -1 Then
          TabIstr.BlkIstr(IndI).Segment = tb!VariabileSegmento
          TabIstr.BlkIstr(IndI).IdSegm = tb!idSegmento
       Else
          If IsNull(tb!idSegmento) Then
            'virgilio
            m_fun.WriteLog "Per l'istruzione " & TabIstr.Istr & " dell'oggetto " & tb!idOggetto & " l'idsegmento � Null", " incapsulatore.caricaistruzione "
            Exit Function
          Else
            Set tb1 = m_fun.Open_Recordset("select * from PsDli_Segmenti where idsegmento = " & tb!idSegmento)
            If Not tb1.EOF Then
               TabIstr.BlkIstr(IndI).Segment = tb1!nome
               TabIstr.BlkIstr(IndI).IdSegm = tb!idSegmento
            End If
            tb1.Close
            If Not IsNull(tb!idSegmentiD) Then
              If Not tb!idSegmentiD = "" Then
                sSegD = Split(tb!idSegmentiD, ";")
                For z = 0 To UBound(sSegD)
                  Set tb1 = m_fun.Open_Recordset("select * from PsDli_Segmenti where idsegmento = " & CLng(sSegD(z)))
                  If Not tb1.EOF Then
                    TabIstr.BlkIstr(IndI).SegmentM = TabIstr.BlkIstr(IndI).SegmentM & tb1!nome & ";"
                  End If
                  tb1.Close
                Next
                TabIstr.BlkIstr(IndI).SegmentM = Left(TabIstr.BlkIstr(IndI).SegmentM, Len(TabIstr.BlkIstr(IndI).SegmentM) - 1)
              Else
                TabIstr.BlkIstr(IndI).SegmentM = ""
              End If
            End If
          End If
        End If
            
       Set tb3 = m_fun.Open_Recordset("select * from DMDB2_Tabelle where idorigine = " & tb!idSegmento)
       If tb3.RecordCount > 0 Then
          TabIstr.BlkIstr(IndI).Tavola = tb3!nome
       Else
 '         Stop
       End If
      
       'SQ CPY-ISTR
       Set tb2 = m_fun.Open_Recordset( _
        "SELECT * FROM PsDli_IstrDett_Key WHERE idPgm=" & rst!idpgm & " AND idoggetto=" & rst!idOggetto & _
        " AND riga= " & rst!riga & " AND livello=" & tb!Livello & " ORDER BY progressivo")
       
       If tb2.RecordCount > 0 Then
          k3 = 0
          While Not tb2.EOF
             k3 = k3 + 1
             
             ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyBool = Trim(tb!Qualificazione)
             'TabIstr.BlkIstr(IndI).KeyDef(k3).Op = m_fun.TrattaOp(Tb2!Operatore & "")
             TabIstr.BlkIstr(IndI).KeyDef(k3).Op = tb2!Operatore & ""
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal = tb2!Valore & ""
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = Trim(tb2!KeyLen)
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyStart = tb2!KeyStart
             '8-05-06
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVarField = tb2!keyvalore
             TabIstr.BlkIstr(IndI).KeyDef(k3).xName = tb2!xName
             TabIstr.BlkIstr(IndI).KeyDef(k3).OpLogic = Trim(tb2!booleano)
             
             If tb2!IdField > 0 Then
                Set tb1 = m_fun.Open_Recordset("select * from PsDLi_Field where idfield = " & Val(tb2!IdField & " "))
                wXDField = False
             Else
                Set tb1 = m_fun.Open_Recordset("select * from PsDLi_XDField where idXDField = " & Abs(Val(tb2!IdField)))
                wXDField = True
             End If
             
              If tb1.RecordCount > 0 Then
                If Not wXDField Then
                'ginevra: l'ho tolto per il momento: serviva?
                '    If Trim(Tb1!ptr_xNome) = "" Then
                     TabIstr.BlkIstr(IndI).KeyDef(k3).key = tb1!nome
                '    Else
                '       TabIstr.BlkIstr(IndI).KeyDef(k3).key = Tb1!ptr_xNome
                '    End If
                Else
                    TabIstr.BlkIstr(IndI).KeyDef(k3).key = tb1!nome
                End If
                
                'STEFANO TROVA KEYLEN
                '<NONE>
                If Len(TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen) Then
                  If Val(tb2!IdField) <> 0 Then
                    'Recupera la lunghezza del field
                    Set TbLenField = m_fun.Open_Recordset("Select * From PsDli_Field Where IdField = " & Val(tb2!IdField))
                    
                    If TbLenField.RecordCount > 0 Then
                        TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = TbLenField!Lunghezza
                    End If
                    
                    TbLenField.Close
                  End If
                End If
             Else
                k3 = k3
             End If
             
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyNum = CarNumKey(IndI)
    
             tb2.MoveNext
          Wend
          tb2.Close
       End If
       
       tb.MoveNext
    Wend
  End If

  
  CaricaIstruzione = True
 
End Function

Public Sub infoLineFromPos(RTBsource As RichTextBox, pPos As Long, linea As String, nextstart As Long, indent As Integer)
  Dim pStart As Long, pEnd As Long
  
  For pStart = pPos To pPos - 160 Step -1
    RTBsource.SelStart = pStart
    RTBsource.SelLength = 2
    If RTBsource.SelText = vbCrLf Then
      Exit For
    End If
  Next pStart
  ' selezione parte da prima posizione riga successiva
  pEnd = RTBsource.find(vbCrLf, pStart + 1)
  RTBsource.SelStart = pStart + 1
  RTBsource.SelLength = pEnd - pStart
  linea = RTBsource.SelText
  nextstart = pEnd + 2
  indent = pPos - RTBsource.SelStart
End Sub

Public Function AsteriscaIstruzione_PLI(ptotoffset As Long, rst As Recordset, RTBModifica As RichTextBox, typeIncaps As String) As Long
  Dim NumRighe As Long, wPosizione As Long, pCount As Long
  Dim pStart As Long, pEnd As Variant, pPos As Long, wIdSegm As Variant
  Dim flag As Boolean, wresp As Boolean
  Dim tb As Recordset
  Dim wVbCrLf As String
  Dim OldNomeRout As String, wTipoCall As String, wstr As String
  Dim K As Integer, numline As Integer
  Dim startkey As Long, offsetendtag As Integer, rsCall As Recordset, windentfor As Integer
  Dim linea As String, lineacomm1 As String, lineacomm2 As String, noindent As Integer, i As Integer
  Dim selstart1 As Long, selstart2 As Long, sellen1 As Integer, sellen2 As Integer
  Dim lendiff As Integer
    
  Set rsCall = m_fun.Open_Recordset("select linee from PsCall where IdOggetto = " & rst!idOggetto & " and riga = " & rst!riga)
  If Not rsCall.EOF Then
    numline = rsCall!linee
  End If
  'alpitour 5/8/2005: se non valida � inutile fare tutti i giri
  If rst!valida Then
    wresp = CaricaIstruzione(rst)
  End If

  'SQ: tutto sto casino per nomeroutine?
  OldNomeRout = nomeroutine
  nomeroutine = ""
  If rst!valida Then
    If UBound(TabIstr.BlkIstr) > 0 Then
      nomeroutine = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).NomeRout
    End If
  End If
  If nomeroutine = "" Then
    nomeroutine = OldNomeRout
  End If
  
  NumRighe = rst!riga + AddRighe + ptotoffset

  'SQ - ? tmp per le EXEC?
  wTipoCall = "CALL"
  wresp = TROVA_RIGA(RTBModifica, wTipoCall, IIf(rst!idpgm <> rst!idOggetto, NumRighe - AddRighe, NumRighe), RTBModifica.SelStart)

  If wresp Then
     pPos = RTBModifica.SelStart
     infoLineFromPos RTBModifica, pPos, linea, pStart, indentkey
     
     lineacomm1 = Space(indentkey) & "/* " & IIf(Len(Trim(START_TAG)), START_TAG, "BPHX") & vbCrLf & linea
     If numline = 1 Then
      lineacomm1 = lineacomm1 & vbCrLf & Space(indentkey) & IIf(Len(Trim(END_TAG)), END_TAG, START_TAG) & "*/ "
       selstart1 = RTBModifica.SelStart
      sellen1 = RTBModifica.SelLength
      'RTBModifica.SelText = lineacomm1
     Else
      selstart1 = RTBModifica.SelStart
      sellen1 = RTBModifica.SelLength
      For i = 2 To numline
        infoLineFromPos RTBModifica, pStart, linea, pStart, noindent
      Next
      lineacomm2 = linea & vbCrLf & Space(indentkey) & IIf(Len(Trim(END_TAG)), END_TAG, START_TAG) & "*/ "
      lendiff = Len(lineacomm2) - RTBModifica.SelLength
      RTBModifica.SelText = lineacomm2
     End If
     'a questo punto in pstart dovrei avere la posizione della riga successiva
     'sommero tutto ci� che � stato aggiunto
     
     ' mi riposiziono per sostituire l'asterisco di apertura
     RTBModifica.SelStart = selstart1
     RTBModifica.SelLength = sellen1
     lendiff = lendiff + Len(lineacomm1) - RTBModifica.SelLength
     RTBModifica.SelText = lineacomm1
     'infoLineFromPos RTBModifica, pStart + lendiff, linea, pStart, noindent
     AddRighe = AddRighe + 2
     
     AsteriscaIstruzione_PLI = pStart + lendiff
  Else
    AsteriscaIstruzione_PLI = 0
    Debug.Print "Instruction not found: " & rst!Istruzione & " at line --> " & NumRighe
  End If
End Function


Public Function AsteriscaIstruzione(ptotoffset As Long, rst As Recordset, RTBModifica As RichTextBox, typeIncaps As String) As Long
  Dim NumRighe As Long, wPosizione As Long, pCount As Long
  Dim pStart As Variant, pEnd As Variant, pPos As Variant, wIdSegm As Variant
  Dim flag As Boolean, wresp As Boolean
  Dim tb As Recordset
  Dim wVbCrLf As String
  Dim OldNomeRout As String, wTipoCall As String, wstr As String
  Dim K As Integer
  Dim startkey As Long, offsetendtag As Integer

  'SQ - Portato fuori!
  'If Not rst!to_migrate Then
  '  Exit Function
  'End If


  'alpitour 5/8/2005: se non valida � inutile fare tutti i giri
  If rst!valida Then
    wresp = CaricaIstruzione(rst)
  End If

  'SQ: tutto sto casino per nomeroutine?
  OldNomeRout = nomeroutine
  nomeroutine = ""
  If rst!valida Then
    If UBound(TabIstr.BlkIstr) > 0 Then
      nomeroutine = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).NomeRout
    End If
  End If
  If nomeroutine = "" Then
    nomeroutine = OldNomeRout
  End If

  'AC 16/01/07 introdotto offset
  'NumRighe = rst!Riga + AddRighe + IIf(IsNull(rst!OffsetRIgaIncaps), 0, rst!OffsetRIgaIncaps)
  NumRighe = rst!riga + AddRighe + ptotoffset

  'SQ - ? tmp per le EXEC?
  wTipoCall = "CALL"
  'SQ CPY-ISTR (TMP...)
  wresp = TROVA_RIGA(RTBModifica, wTipoCall, NumRighe, RTBModifica.SelStart)
  'wResp = TROVA_RIGA(RTBModifica, wTipoCall, IIf(rst!idPgm <> rst!idOggetto, NumRighe - AddRighe, NumRighe), RTBModifica.SelStart)

  If wresp Then
     pPos = RTBModifica.SelStart
     wIdent = 0
     startkey = pPos
     For pStart = pPos To pPos - 160 Step -1
       wIdent = wIdent + 1
       RTBModifica.SelStart = pStart
       RTBModifica.SelLength = 2
       If RTBModifica.SelText = vbCrLf Then
         Exit For
       End If
     Next pStart

     wIdent = wIdent - 1
     'stefano: per ora semplifico e vado pi� in l�
     wIdent = wIdent + 6

'     If TypeIncaps = "RDBMS" Then
'        AsteriscaIstruzione = pStart - 1
'        Exit Function
'     End If
'
     RTBModifica.SelStart = pStart + 1
     indentkey = startkey - RTBModifica.SelStart
     pStart = pStart + 2
     'SQ portare sotto
     Dim lenline As Integer

     RTBModifica.SelLength = 7

    If rst!valida Then
      Select Case Trim(UCase(TabIstr.Istr))
         Case "PCB"
            RTBModifica.SelText = m_fun.LABEL_PCB & "*"
         Case "TERM"
            RTBModifica.SelText = m_fun.LABEL_TERM & "*"
         Case "CHKP"
            RTBModifica.SelText = m_fun.LABEL_CHKP & "*"
         Case "ROLB", "ROLL"
            RTBModifica.SelText = m_fun.LABEL_ROLL & "*"
         Case "QRY"
            RTBModifica.SelText = m_fun.LABEL_QUERY & "*"
         Case "SYNC"
            RTBModifica.SelText = m_fun.LABEL_SYNC & "*"
         Case Else
            'SQ 27-07-06
            'Se START_TAG non valorizzato => rimane l'eventuale originale TAG
            If Len(Trim(START_TAG)) Then
              RTBModifica.SelText = START_TAG & "*"
            Else
              RTBModifica.SelText = Left(RTBModifica.SelText, 6) & "*"
            End If
      End Select
    Else
    'SP 11/9 gestione template ad hoc, per ora non gestisco variabile
    'SP corso
      'If rst!template <> "" Then
      '  RTBModifica.SelText = "BPHX-T*"
      'Else
        RTBModifica.SelText = m_fun.LABEL_NONDECODE & "*"
      'End If
    End If

    pEnd = RTBModifica.find(vbCrLf, pStart + 1)
    'SQ 27-07-06
    lenline = pEnd - pStart + 1

    If wTipoCall = " EXEC " Then
       pEnd = RTBModifica.find("END-EXEC", pPos + 1, , rtfWholeWord)
       pEnd = RTBModifica.find(vbCrLf, pEnd + 1)
    Else
      flag = True
'       pEnd = pStart + 1
      pEnd = RTBModifica.find(vbCrLf, pPos + 1)
      '''''''''''''''''''''''''''''''''''''''''''''''
      'SQ 27-07-06
      ' Gestione TAG a colonna 72
      ' Se non valorizzato => rimane l'eventuale originale TAG
      '''''''''''''''''''''''''''''''''''''''''''''''
      If Len(Trim(END_TAG)) Then
        If lenline >= 72 Then
          RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
          'RTBModifica.SelLength = 8
          RTBModifica.SelLength = (lenline - 72)
          RTBModifica.SelText = END_TAG
          offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
        Else
         RTBModifica.SelLength = 0
         RTBModifica.SelText = Space(72 - lenline) & END_TAG
        End If
      End If

      wstr = Mid(RTBModifica.text, pPos, pEnd - pPos + 1)
      If InStr(wstr, ".") Then
        flag = False
      End If
      pPos = pEnd + 1
      While flag
         pEnd = RTBModifica.find(vbCrLf, pPos + 1)
         wstr = Mid(RTBModifica.text, pPos + 2, pEnd - pPos - 1)
         'SP 4/9
         Dim savewstr As String
         savewstr = wstr
         ' alpitour: 5/8/2005: mi sembrava di averla gi� fatta questa modifica per la fase 1
          If Len(wstr) > 6 Then
             If Mid(wstr, 7, 1) <> "*" Then
                If Len(wstr) > 11 Then wstr = Trim(Mid(wstr, 12, Len(wstr) - 11))
 ' da rivedere: se trovava punto considerava la stringa chiusa; non va bene
 ' quando c'� l'istruzione senza punto e la riga dopo lo ha, considera la riga successiva
 ' come facente parte dell'istruzione
 '        If InStr(wStr, ".") > 0 Then
 '           Flag = False
 '        Else
                K = InStr(wstr, " ")
                If K > 0 Then wstr = Trim(Left(wstr, K - 1))
                'SP 27/9: gestione punto dopo parola cobol (praticamente solo END-IF e simili)
                If wstr <> "." Then
                   If Right(wstr, 1) = "." Then wstr = Left(wstr, InStr(1, wstr, ".") - 1)
                End If
                If m_fun.SeWordCobol(wstr) And Not wstr = "END-CALL" Then
                  flag = False
                  pEnd = pPos - 1
                End If
 '        End If
         'SP 4/9
                'If InStr(wstr, ".") > 0 Then
                If InStr(savewstr, ".") > 0 Then
                   flag = False
                End If
             End If
          End If
          pPos = pEnd + 1
       Wend
     End If

     wPosizione = pEnd + 1
     pEnd = pEnd - 2
     pPos = pStart
     pPos = pStart + 1
     flag = True
     While flag
       pPos = RTBModifica.find(vbCrLf, pPos + 1, pEnd - 1)
       If pPos = -1 Then
         flag = False
       End If

       If flag Then
         RTBModifica.SelStart = pPos + 2
         RTBModifica.SelLength = 7

'         If Trim(RTBModifica.SelText) = vbCrLf Then
         ' controllo per righe "quasi" vuote
         If InStr(RTBModifica.SelText, vbCrLf) Then
            wVbCrLf = vbCrLf & Space(5)
            pCount = pCount + 5
         Else
            wVbCrLf = ""
         End If

         'If rst!Valida = True Then
         ' ginevra: se gi� asteriscato, non lo cambia
         If Mid(RTBModifica.SelText, 7, 1) <> "*" Then
'alpitour 5/8/2005: se non valida � inutile fare tutti i giri
           If rst!valida Then
             Select Case Trim(UCase(TabIstr.Istr))
                Case "PCB"
                   RTBModifica.SelText = m_fun.LABEL_PCB & "*" & wVbCrLf
                Case "TERM"
                   RTBModifica.SelText = m_fun.LABEL_TERM & "*" & wVbCrLf
                Case "CHKP"
                   RTBModifica.SelText = m_fun.LABEL_CHKP & "*" & wVbCrLf
                Case "QRY"
                   RTBModifica.SelText = m_fun.LABEL_QUERY & "*" & wVbCrLf
                Case "SYNC"
                   RTBModifica.SelText = m_fun.LABEL_SYNC & "*" & wVbCrLf
                Case Else
                   'SQ 27-07-06
                  'Se START_TAG non valorizzato => rimane l'eventuale originale TAG
                  If Len(Trim(START_TAG)) Then
                    RTBModifica.SelText = START_TAG & "*" & wVbCrLf
                  Else
                    RTBModifica.SelText = Left(RTBModifica.SelText, 6) & "*" & wVbCrLf
                  End If
            End Select
            '''''''''''''''''''''''''''''''''''''''''''''''
            'SQ 27-07-06
            ' Gestione TAG a colonna 72
            '''''''''''''''''''''''''''''''''''''''''''''''
            If Len(Trim(END_TAG)) Then
              lenline = RTBModifica.find(vbCrLf, pPos + 1) - pPos - 2
              If lenline >= 72 Then
                RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
                RTBModifica.SelLength = lenline - 72
                RTBModifica.SelText = END_TAG
                offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
              Else
                RTBModifica.SelLength = 0
                RTBModifica.SelText = Space(72 - lenline) & END_TAG
              End If
            End If
          Else
           'SP 11/9 gestione template ad hoc, per ora non gestisco variabile
           'SP corso
              'If rst!template <> "" Then
              '  RTBModifica.SelText = "BPHX-T*" & wVbCrLf
              'Else
                RTBModifica.SelText = m_fun.LABEL_NONDECODE & "*" & wVbCrLf
              'End If
          End If
        End If
      End If
    Wend
    If offsetendtag > 2 Then
      offsetendtag = offsetendtag - 2
    End If
    AsteriscaIstruzione = wPosizione + pCount + offsetendtag
  Else
    AsteriscaIstruzione = 0
    Debug.Print "Instruction not found: " & rst!Istruzione & " at line --> " & NumRighe
  End If
End Function


Public Function AsteriscaIstruzione_EZT(ptotoffset As Long, rst As Recordset, _
                                        RTBModifica As RichTextBox, typeIncaps As String) As Long
  Dim NumRighe As Long, wPosizione As Long, pCount As Long
  Dim pStart As Variant, pEnd As Variant, pPos As Variant, wIdSegm As Variant
  Dim flag As Boolean, wresp As Boolean
  Dim tb As Recordset
  Dim wVbCrLf As String
  Dim OldNomeRout As String, wTipoCall() As String, wstr As String
  Dim K As Integer, i As Integer
  Dim startkey As Long, offsetendtag As Integer
  Dim percorso As String
  Dim finalStringTmpl As String
  Dim wPath As String
  Dim Rtb  As RichTextBox
  Dim vectorRighe() As String
  Dim righe As Integer
  
  On Error GoTo ErrH

  'alpitour 5/8/2005: se non valida � inutile fare tutti i giri
  If rst!valida Then
    wresp = CaricaIstruzione(rst)
  End If

  'SQ: tutto sto casino per nomeroutine?
  OldNomeRout = nomeroutine
  nomeroutine = ""
  If rst!valida Then
    If UBound(TabIstr.BlkIstr) > 0 Then
      nomeroutine = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).NomeRout
    End If
  End If
  If nomeroutine = "" Then
    nomeroutine = OldNomeRout
  End If

  'AC 16/01/07 introdotto offset
  'NumRighe = rst!Riga + AddRighe + IIf(IsNull(rst!OffsetRIgaIncaps), 0, rst!OffsetRIgaIncaps)
  NumRighe = rst!riga + AddRighe + ptotoffset

  'tipo di chiamata
  ReDim wTipoCall(1)
  wTipoCall(0) = "DLI"
  wTipoCall(1) = "RETRIEVE"
  
  'trova il numero di riga da asteriscare
  For i = 0 To UBound(wTipoCall)
    wresp = TROVA_RIGA(RTBModifica, wTipoCall(i), IIf(rst!idpgm <> rst!idOggetto, NumRighe - AddRighe, NumRighe), RTBModifica.SelStart)
    If wresp Then Exit For
  Next
  
  If wresp Then
     pPos = RTBModifica.SelStart
     wIdent = 0
     startkey = pPos
     For pStart = pPos To pPos - 160 Step -1
       wIdent = wIdent + 1
       RTBModifica.SelStart = pStart
       RTBModifica.SelLength = 2
       If RTBModifica.SelText = vbCrLf Then
         Exit For
       End If
     Next pStart

     wIdent = wIdent - 1
     'stefano: per ora semplifico e vado pi� in l�
     wIdent = wIdent + 6
     
     RTBModifica.SelStart = pStart + 1
     indentkey = startkey - RTBModifica.SelStart
     pStart = pStart + 2
     'SQ portare sotto
     Dim lenline As Integer

     RTBModifica.SelLength = 1

    If TabIstr.Istr = "CHKP" Or TabIstr.Istr = "XRST" Or TabIstr.Istr = "FOR" Or rst!obsolete = True Then
'stefano: bisogna mettere nei parametri anche un TAG per le NON decodificate, in modo da ritrovarel
        RTBModifica.SelText = "*" & "ITER-N" & " " & RTBModifica.SelText
    Else
        RTBModifica.SelText = "*" & START_TAG & " " & RTBModifica.SelText
    End If

    pEnd = RTBModifica.find(vbCrLf, pStart + 1)
    lenline = pEnd - pStart + 1

      flag = True
'       pEnd = pStart + 1
'gi� fatto
      'pEnd = RTBModifica.Find(vbCrLf, pPos + 1)
      '''''''''''''''''''''''''''''''''''''''''''''''
      'SQ 27-07-06
      ' Gestione TAG a colonna 72
      ' Se non valorizzato => rimane l'eventuale originale TAG
      '''''''''''''''''''''''''''''''''''''''''''''''
      If Len(Trim(END_TAG)) Then
        If lenline >= 72 Then
          RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
          'RTBModifica.SelLength = 8
          RTBModifica.SelLength = (lenline - 72)
          RTBModifica.SelText = END_TAG
          offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
        Else
         RTBModifica.SelLength = 0
         RTBModifica.SelText = Space(72 - lenline) & END_TAG
        End If
      End If

      wstr = Mid(RTBModifica.text, pPos, pEnd - pPos + 1)
      If InStr(wstr, " +") > 0 Then
        flag = True
'stefano: PER ORA NON GESTISCO PI� DI DUE RIGHE, ALTRIMENTI BISOGNA USARE LA PSCALL
        Dim pend2 As Long
        While flag
          pend2 = RTBModifica.find(vbCrLf, pEnd + 2)
          wstr = Left(Trim(Mid(RTBModifica.text, pEnd + 3, pend2 + 1 - pEnd - 3)), 1)
'funziona solo con "*" a colonna 1
          If wstr <> "*" Then
             RTBModifica.SelStart = pEnd + 2
             RTBModifica.SelLength = 1
    'per ora rifaccio solo questa
             If TabIstr.Istr = "CHKP" Or TabIstr.Istr = "XRST" Or TabIstr.Istr = "FOR" Or rst!obsolete = True Then
                RTBModifica.SelText = "*" & "ITER-N" & " " & RTBModifica.SelText
             Else
                RTBModifica.SelText = "*" & START_TAG & " " & RTBModifica.SelText
             End If
             'aggiunge il tag
'             pEnd = pend2 + 8
          Else
'             pEnd = pend2
          End If
          pend2 = RTBModifica.find(vbCrLf, pEnd + 2)
'          wstr = Left(Trim(Mid(RTBModifica.text, pEnd + 3, pend2 + 1)), 1)
          'chiude il ciclo' NON GESTITO, per ora mi fermo a due righe
          'If Trim(wstr) <> "*" And InStr(Mid(RTBModifica.text, pEnd + 3, pend2 + 1 - pEnd - 3), "+") = 0 Then flag = False
          pEnd = pend2
          flag = False
        Wend
      End If
      pPos = pEnd + 1
      ' ma perch� non usare il numero di righe inserito nella pscall? Sta l� apposta
'      While flag
'         pEnd = RTBModifica.Find(vbCrLf, pPos + 1)
'         wstr = Mid(RTBModifica.text, pPos + 2, pEnd - pPos - 1)
'         'SP 4/9
'         Dim savewstr As String
'         savewstr = wstr
'         'stefano: ERA TUTTA IN COBOL!!!!!!!
'         ' alpitour: 5/8/2005: mi sembrava di averla gi� fatta questa modifica per la fase 1
'          If Len(wstr) > 6 Then
'             If Mid(wstr, 7, 1) <> "*" Then
'              If Len(wstr) > 11 Then wstr = Trim(Mid(wstr, 12, Len(wstr) - 11))
'              K = InStr(wstr, " ")
'              If K > 0 Then wstr = Trim(Left(wstr, K - 1))
'              'SP 27/9: gestione punto dopo parola cobol (praticamente solo END-IF e simili)
'              If wstr <> "." Then
'                 If Right(wstr, 1) = "." Then wstr = Left(wstr, InStr(1, wstr, ".") - 1)
'              End If
'              If m_fun.SeWordCobol(wstr) And Not wstr = "END-CALL" Then
'                flag = False
'                pEnd = pPos - 1
'              End If
'              If InStr(savewstr, ".") > 0 Then
'                flag = False
'              End If
'            End If
'          End If
'          pPos = pEnd + 1
'       Wend

     wPosizione = pEnd + 1
     pEnd = pEnd - 2
     pPos = pStart
     pPos = pStart + 1
     flag = True
     
'       While flag
'         pPos = RTBModifica.Find(vbCrLf, pPos + 1, pEnd - 1)
'         If pPos = -1 Then
'           flag = False
'         End If
'
'         If flag Then
'           RTBModifica.SelStart = pPos + 2
'           RTBModifica.SelLength = 1
'
'  '         If Trim(RTBModifica.SelText) = vbCrLf Then
'           ' controllo per righe "quasi" vuote
'           If InStr(RTBModifica.SelText, vbCrLf) Then
'              wVbCrLf = vbCrLf & Space(5)
'              pCount = pCount + 5
'           Else
'              wVbCrLf = ""
'           End If
'
'           'If rst!Valida = True Then
'           'stefano: che c'entrava (colonna 7) con l'EZT??? Quante cose nuove gi� da buttar via!!!!!!
'           If Left(Trim(RTBModifica.SelText), 1) <> "*" Then
'             If rst!valida Then
'                If Len(Trim(START_TAG)) Then
'                  RTBModifica.SelText = "*" & START_TAG & wVbCrLf
'                Else
'                  'RTBModifica.SelText = Left(RTBModifica.SelText, 6) & "*" & wVbCrLf
'                End If
'              '''''''''''''''''''''''''''''''''''''''''''''''
'              'SQ 27-07-06
'              ' Gestione TAG a colonna 72
'              '''''''''''''''''''''''''''''''''''''''''''''''
'              If Len(Trim(END_TAG)) Then
'                lenline = RTBModifica.Find(vbCrLf, pPos + 1) - pPos - 2
'                If lenline >= 72 Then
'                  RTBModifica.SelStart = RTBModifica.SelStart - (lenline - 72)
'                  RTBModifica.SelLength = lenline - 72
'                  RTBModifica.SelText = END_TAG
'                  offsetendtag = offsetendtag + Len(END_TAG) - lenline + 72
'                Else
'                  RTBModifica.SelLength = 0
'                  RTBModifica.SelText = Space(72 - lenline) & END_TAG
'                End If
'              End If
'            Else
'               RTBModifica.SelText = "*" & m_fun.LABEL_NONDECODE & wVbCrLf
'            End If
'          End If
'        End If
'      Wend
    If offsetendtag > 2 Then
      offsetendtag = offsetendtag - 2
    End If
    
    If TabIstr.Istr = "CHKP" Or TabIstr.Istr = "XRST" Or TabIstr.Istr = "FOR" Or rst!obsolete = True Then
       AsteriscaIstruzione_EZT = wPosizione + pCount + offsetendtag
       Exit Function
    End If
       
    
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''
''' provare ad inserire qui il blocco del template'
''' e aggiungere l'offset correttamente '''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    wPath = m_fun.FnPathPrj
    Set Rtb = MavsdF_Incapsulatore.RTBR
    
    percorso = wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\EZT\COMMONINIT_ETZ"
    Rtb.LoadFile percorso
  
    finalStringTmpl = vbCrLf & Rtb.text
    
    'nome programma
    finalStringTmpl = Replace(finalStringTmpl, "<NAMEPGM>", "'" & Trim(NomeProg) & "'")
    
    'nome routine
    finalStringTmpl = Replace(finalStringTmpl, "<ROUTNAME>", nomeroutine)
    
    'operatore
    finalStringTmpl = Replace(finalStringTmpl, "<OPERAT>", "'" & getIstruzione_ETZ(TabIstr.Istr, 1, rst!idpgm, rst!idOggetto, rst!riga, rst!ordPcb) & "'")
    
    'numero pcb
    finalStringTmpl = Replace(finalStringTmpl, "<NPCB>", TabIstr.NumPCB)
    
'''cicla tante volte quante sono le SSA
    Dim jxz As Integer
    Dim sLevel As Integer
    Dim posSx As Integer
    Dim posDx As Integer
    Dim aggiunta As String
    For jxz = 1 To UBound(TabIstr.BlkIstr)
      'livello del segmento     <FLEVEL>
      segLev = 0
      getSegLevel (CLng(TabIstr.BlkIstr(jxz).IdSegm))
      If jxz = UBound(TabIstr.BlkIstr) Then
        If TabIstr.BlkIstr(jxz).Record <> "" Then
           finalStringTmpl = Replace(finalStringTmpl, "<LEVELMAX>", jxz)
           finalStringTmpl = Replace(finalStringTmpl, "<DATAAREA>", TabIstr.BlkIstr(jxz).Record)
        End If
      End If
      
'''                     jxz         numero di cond. su SSA
'      Dim Stringa() As String
      Dim zzz As Integer
'      Stringa = getLevKey(rst!idOggetto, TabIstr.BlkIstr(jxz).ssaName)
'      If UBound(Stringa) > 0 Then
'        For zzz = 1 To UBound(TabIstr.BlkIstr(jxz).KeyDef)
'          finalStringTmpl = Replace(finalStringTmpl, "<LEVKEY>", Stringa(zzz))
'          ' lo rif� per i cicli successivi
'          finalStringTmpl = Replace(finalStringTmpl, "<INDICE-FLEVEL>", jxz)
'          finalStringTmpl = Replace(finalStringTmpl, "<INDICE-FLEVEL-R>", zzz)
'          If UBound(Stringa) > 1 And zzz <> UBound(Stringa) Then
'            aggiunta = vbCrLf & "LNKDB-KEYVALUE<INDICE-FLEVEL>, <INDICE-FLEVEL-R> =  <LEVKEY> "
'            posSx = InStr(1, finalStringTmpl, Stringa(zzz) & vbCrLf) + Len(Stringa(zzz))
'            finalStringTmpl = Mid(finalStringTmpl, 1, posSx) & aggiunta & Mid(finalStringTmpl, posSx, Len(finalStringTmpl))
'          End If
'        Next
'      Else
'        'squalificata
'        finalStringTmpl = Mid(finalStringTmpl, 1, InStr(1, finalStringTmpl, "LNKDB-KEYVALUE") - 1) & vbCrLf & Mid(finalStringTmpl, InStr(1, finalStringTmpl, "<FLEVELSA>") + 10)
'      End If
Dim Cont As Integer
      If UBound(TabIstr.BlkIstr(jxz).KeyDef) > 0 Then
        For zzz = 1 To UBound(TabIstr.BlkIstr(jxz).KeyDef)
          Cont = Cont + 1
          finalStringTmpl = Replace(finalStringTmpl, "<LEVKEY" & jxz & "-" & zzz & ">", "REVSSAKEYVAL<LEVEL" & jxz & "-" & zzz & ">")
          finalStringTmpl = Replace(finalStringTmpl, "<LEVEL" & jxz & "-" & zzz & ">", jxz)
          finalStringTmpl = Replace(finalStringTmpl, "<LEVEL" & jxz & "-" & zzz & "R>", zzz)
          finalStringTmpl = Replace(finalStringTmpl, "<POSKEY" & jxz & "-" & zzz & ">", TabIstr.BlkIstr(jxz).KeyDef(zzz).KeyStart - 1)
          finalStringTmpl = Replace(finalStringTmpl, "<LENKEY" & jxz & "-" & zzz & ">", TabIstr.BlkIstr(jxz).KeyDef(zzz).KeyLen)
'stefano: visto che non mi piaceva proprio e NON funzionava prendendo il posx un po' a caso.
'  ho fatto un template bruttissimo, ma esterno, che dovr� essere sostituito con un template vettoriale
'  a cura di Alberto
'          If UBound(TabIstr.BlkIstr(jxz).KeyDef) > zzz Then
'          ' lo rif� per i cicli successivi
'            aggiunta = "LNKDB-KEYVALUE<INDICE-FLEVEL><INDICE-FLEVEL-R> = <LEVKEY> " & vbCrLf
'            posSx = InStr(1, finalStringTmpl, Replace(" = REVSSAKEYVAL<NUMKEY>", "<NUMKEY>", Cont)) + 17
'            finalStringTmpl = Mid(finalStringTmpl, 1, posSx) & aggiunta & Mid(finalStringTmpl, posSx, Len(finalStringTmpl))
'            aggiunta = vbCrLf & "DEFINE REVSSAKEYVAL<NUMKEY> REVSSA +<POSKEY> <LENKEY> A"
'            'stefano: forzatissimo, ma non � colpa mia, toglie due invii
'            posSx = InStr(1, finalStringTmpl, "REVSSA =") - 5
'            finalStringTmpl = Mid(finalStringTmpl, 1, posSx) & aggiunta & Mid(finalStringTmpl, posSx, Len(finalStringTmpl))
'          End If
        Next
      Else
        'squalificata
        'finalStringTmpl = Mid(finalStringTmpl, 1, InStr(1, finalStringTmpl, "LNKDB-KEYVALUE") - 1) & vbCrLf & Mid(finalStringTmpl, InStr(1, finalStringTmpl, "<FLEVELSA>") + 10)
      End If
      
      
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

      'nome ssa     <FLEVELSA>
      Dim tokenStat As String
      Dim s As String
      'stefano: ma il parser che ci sta a fare?
'      s = rst!statement
'      tokenStat = nextToken(s)
'      tokenStat = nextToken(s)
      If segLev > 0 Then
        finalStringTmpl = Replace(finalStringTmpl, "<SEGLEVEL" & jxz & ">", "'" & Right("00" & segLev, 2) & "'")
      End If
      finalStringTmpl = Replace(finalStringTmpl, "<LEVEL" & jxz & ">", jxz)
      If TabIstr.BlkIstr(jxz).ssaName <> "" Then
        finalStringTmpl = Replace(finalStringTmpl, "<SSA" & jxz & ">", TabIstr.BlkIstr(jxz).ssaName)
      End If
      'gi� fatto sopra (ora � indice FLEVEL)
      'finalStringTmpl = Replace(finalStringTmpl, "<NUMERO-SSA>", UBound(TabIstr.BlkIstr))
      'finalStringTmpl = Replace(finalStringTmpl, "<INDICE-SSA>", jxz)
      
      'se ci sono + SSA riscrive la parte del template
      'stefano: � inutile avere un template esterno se poi si ricostruisce da dentro Reviser,
      ' appena possibile buttiamo via sta routine e usiamo quella standard
'      aggiunta = ""
'      posSx = 0
'      If UBound(TabIstr.BlkIstr) > 1 And jxz <> UBound(TabIstr.BlkIstr) Then
'        aggiunta = vbCrLf & "LNKDB-SEGLEVEL<INDICE-FLEVEL>    =  <FLEVEL> "
'        aggiunta = aggiunta & vbCrLf & "LNKDB-KEYVALUE<INDICE-FLEVEL><INDICE-FLEVEL-R> =  <LEVKEY> "
'        aggiunta = aggiunta & vbCrLf & "LNKDB-SSA-AREA<INDICE-FLEVEL>    =  <FLEVELSA>" & vbCrLf
'
'        posSx = InStr(1, finalStringTmpl, TabIstr.BlkIstr(jxz).ssaName & vbCrLf) + 6
'        finalStringTmpl = Mid(finalStringTmpl, 1, posSx) & aggiunta & Mid(finalStringTmpl, posSx, Len(finalStringTmpl))
'        finalStringTmpl = Mid(finalStringTmpl, 1, posSx) & aggiunta & Mid(finalStringTmpl, posSx, Len(finalStringTmpl))
'      End If
      
    Next
    
    'status code
    finalStringTmpl = Replace(finalStringTmpl, "<PCBSTCD>", getStatusCode(rst!idOggetto, TabIstr.dbd(1).nome))
    
    vectorRighe = Split(finalStringTmpl, vbCrLf)
    'toglie le righe non sostituite
    K = 0
    finalStringTmpl = ""
    For i = 0 To UBound(vectorRighe)
     If Not InStr(vectorRighe(i), "<") > 0 Then
        K = K + 1
        finalStringTmpl = finalStringTmpl & vectorRighe(i) & vbCrLf
     End If
    Next
      
    righe = Len(finalStringTmpl)
'toglie 1 perch� sovrascrive un carriage return, per cui un riga sull'originale viene mangiata (perch� poi???)
    AddRighe = AddRighe + K - 1
    
    RTBModifica.SelText = finalStringTmpl
    
    AsteriscaIstruzione_EZT = wPosizione + pCount + offsetendtag + righe
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''' fine '''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

  Else
    AsteriscaIstruzione_EZT = 0
    Debug.Print "Instruction not found: " & rst!Istruzione & " at line --> " & NumRighe
  End If
  
  Exit Function
ErrH:
  MsgBox Err.Description
  Resume Next
End Function

Private Function getStatusCode(idOggetto As Long, dbd As String)
  Dim rs As Recordset
  
  On Error GoTo ErrH
  Set rs = m_fun.Open_Recordset("Select Status_Code from PSEZ_FILE where " & _
                                "IdOggetto =" & idOggetto & " and dbd='" & dbd & "'")
  If Not rs.EOF Then
    getStatusCode = rs!Status_Code
  Else
    getStatusCode = ""
  End If
  rs.Close

  Exit Function
ErrH:
  MsgBox Err.Description & vbCrLf & "[MavsdM_Incapsulatore.getStatusCode]"
  getStatusCode = ""
End Function

Sub INSERT_NODECOD(wPos As Long, RTBModifica As RichTextBox, rst As Recordset)
    Dim testo As String
    
' istruzioni con template fisso
    'If rst!template <> "" And rst!template <> "OBSO" Then
    If Len(rst!Template) Then
        Dim testo1 As String, testo2 As String, testo3 As String
        Dim i As Long, j As Long, K As Long
        testo1 = ""
        testo2 = ""
        testo3 = ""
        testo1 = RTBModifica.text
        RTBModifica.LoadFile m_fun.FnPathPrj & "\input-prj\istruzioni\" & rst!Template
        testo3 = RTBModifica.text
        i = RTBModifica.find("#WS#", 0)
        If i > -1 Then
           i = RTBModifica.find(vbCrLf, i)
           j = RTBModifica.find("#END-WS#", i + 2)
           RTBModifica.SelStart = i + 2
           RTBModifica.SelLength = j - i - 3
           testo2 = RTBModifica.SelText
           i = RTBModifica.find(vbCrLf, j)
           j = Len(RTBModifica.text)
           RTBModifica.SelStart = i + 2
           RTBModifica.SelLength = j - i + 3
           testo3 = RTBModifica.SelText
           RTBModifica.text = testo1
           i = RTBModifica.find(" WORKING-STORAGE ", 1)
           If i > 0 Then
              i = RTBModifica.find(vbCrLf, i)
              RTBModifica.SelStart = i + 2
              RTBModifica.SelLength = 0
              RTBModifica.SelText = testo2
              wPos = wPos + Len(testo2)
           End If
        Else
           RTBModifica.text = testo1
        End If
        testo1 = RTBModifica.text
        RTBModifica.text = testo2
        j = 0
        i = 999
        K = 0
        While i <> -1
            i = RTBModifica.find(vbCrLf, j)
            If i <> -1 Then
                K = K + 1
            End If
            j = i + 2
        Wend
        AddRighe = AddRighe + K
        RTBModifica.text = testo3
        j = 0
        i = 999
        K = 0
        While i <> -1
            i = RTBModifica.find(vbCrLf, j)
            If i <> -1 Then
                K = K + 1
            End If
            j = i + 2
        Wend
        AddRighe = AddRighe + K - 1
        testo = vbCrLf & testo3
        RTBModifica.text = testo1
    Else
    'SP segmenti e DBD obsoleti
       'If rst!template = "OBSO" Then
        If rst!obsolete Then
                testo = vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
        testo = testo & vbCrLf & m_fun.LABEL_NONDECODE & "*   OBSOLETE DBD OR SEGMENT"
        testo = testo & vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
        'SQ 28-07-06 - Per le istruzioni sconosciute
        'If rst!Istruzione = "ISRT" Or rst!Istruzione = "DLET" Or rst!Istruzione = "REPL" Then
        If rst!Istruzione = "ISRT" Or rst!Istruzione = "DLET" Or rst!Istruzione = "REPL" Or Len(rst!Istruzione & "") = 0 Then
           If SwTp Then
              testo = testo & vbCrLf & m_fun.LABEL_NONDECODE & "     EXEC CICS ABEND ABCODE('OBSO') END-EXEC"
           Else
              testo = testo & vbCrLf & m_fun.LABEL_NONDECODE & "     CALL 'OBSO'"
           End If
        Else
           testo = testo & vbCrLf & m_fun.LABEL_NONDECODE & "     MOVE 'GE' TO ST-CODE" & Right(Trim(rst!NumPCB), 2)
        End If
        AddRighe = AddRighe + 4
       Else
                testo = vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
        testo = testo & vbCrLf & m_fun.LABEL_NONDECODE & "*   THE PREVIOUS COMMAND MUST BE SOLVED IN CORRECTOR."
        testo = testo & vbCrLf & m_fun.LABEL_NONDECODE & "*                     WARNING! "
        testo = testo & vbCrLf & m_fun.LABEL_NONDECODE & "* --------------------------------------------------------------"
        AddRighe = AddRighe + 4
       End If
    End If
    
    RTBModifica.SelStart = wPos
    RTBModifica.SelLength = 0
    
    RTBModifica.SelText = testo
    
End Sub

Sub Insert_ProcedureTemplate(RTBModifica As RichTextBox, RTBErr As RichTextBox)
  Dim Rtb As RichTextBox, testo As String, s() As String, i As Integer
  Dim rsSegmenti As Recordset, wEnv As collection

  Set Rtb = MavsdF_Incapsulatore.RTBR
  'stefanopul
'  wpath = m_fun.FnPathPrj & "\input-prj\Template\INCAPS\ProcedurePgmDB"
  wPath = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\ProcedurePgmDB"
  'SQ - 16-11-06: EMBEDDED
  'Gestire TEMPLATE NON ESISTENTI!
  If Len(Dir(wPath)) Then
    Rtb.LoadFile wPath
  Else
    RTBErr.text = RTBErr.text & "* TEMPLATE NOT FOUND: " & wPath & vbCrLf
  End If
    
  For i = 1 To distinctDB.Count
    Set wEnv = New collection
    wEnv.Add distinctDB.Item(i)
    testo = testo & "BPHX-C     EXEC SQL INCLUDE " & MavsdM_GenRout.getEnvironmentParam_SQ(kCopyParam, "dbd", wEnv) & " END-EXEC."
    If Not i = distinctDB.Count Then
      testo = testo & vbCrLf
    End If
  Next
  'testo = Left(testo, Len(testo) - 2)
  MavsdM_GenRout.changeTag Rtb, "<INCLUDE_COPY-K>", testo
  testo = ""
      
  For i = 1 To distinctSeg.Count
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where Nome = '" & distinctSeg.Item(i) & "'")
    Set wEnv = New collection
    wEnv.Add Null
    wEnv.Add rsSegmenti
    testo = testo & "BPHX-C     EXEC SQL INCLUDE " & MavsdM_GenRout.getEnvironmentParam_SQ(mCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf
    rsSegmenti.Close
  Next
  testo = Left(testo, Len(testo) - 2)
  MavsdM_GenRout.changeTag Rtb, "<INCLUDE_COPY-M>", testo
  
  testo = Rtb.text
  s = Split(testo, vbCrLf)
     
  AddRighe = AddRighe + UBound(s)
  
  
  RTBModifica.SelStart = Len(RTBModifica.text)
  RTBModifica.SelLength = 0
  RTBModifica.SelText = testo
  'wPos = wPos + Len(testo)
  
   '**********
  testo = ""
  For i = 1 To distinctSeg.Count
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where Nome = '" & distinctSeg.Item(i) & "'")
    Set wEnv = New collection
    wEnv.Add Null
    wEnv.Add rsSegmenti
    testo = testo & "BPHX-I     EXEC SQL INCLUDE " & MavsdM_GenRout.getEnvironmentParam_SQ(tCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf
    rsSegmenti.Close
  Next
  testo = Left(testo, Len(testo) - 2)
  MavsdM_GenRout.changeTag RTBModifica, "<INCLUDE_COPY-T>", testo
  testo = ""
  For i = 1 To distinctSeg.Count
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where Nome = '" & distinctSeg.Item(i) & "'")
    Set wEnv = New collection
    wEnv.Add Null
    wEnv.Add rsSegmenti
    testo = testo & "BPHX-I     EXEC SQL INCLUDE " & MavsdM_GenRout.getEnvironmentParam_SQ(xCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf
    rsSegmenti.Close
  Next
  testo = Left(testo, Len(testo) - 2)
  MavsdM_GenRout.changeTag RTBModifica, "<INCLUDE_COPY-X>", testo
  
  testo = ""
  For i = 1 To distinctSeg.Count
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where Nome = '" & distinctSeg.Item(i) & "'")
    Set wEnv = New collection
    wEnv.Add Null
    wEnv.Add rsSegmenti
    testo = testo & "BPHX-I     EXEC SQL INCLUDE " & MavsdM_GenRout.getEnvironmentParam_SQ(cCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf
    rsSegmenti.Close
  Next
  testo = Left(testo, Len(testo) - 2)
  MavsdM_GenRout.changeTag RTBModifica, "<INCLUDE_COPY-C>", testo
      
End Sub
Sub Insert_TagRoutine(RTBModifica As RichTextBox)
  Dim i As Integer, testo As String
  
  testo = testo & vbCrLf
  AddRighe = AddRighe + 1
  For i = 1 To distinctCod.Count
    'testo = testo & "BPHX-C     <" & distinctCod.Item(i) & ">" & vbCrLf
    testo = testo & "<" & distinctCod.Item(i) & ">" & vbCrLf
    AddRighe = AddRighe + 1
  Next
      
  RTBModifica.SelStart = Len(RTBModifica.text)
  RTBModifica.SelLength = 0
  RTBModifica.SelText = testo
  'wPos = wPos + Len(testo)
End Sub
Sub INSERT_DECOD(wPos As Long, RTBModifica As RichTextBox, RTBErr As RichTextBox, rs As Recordset, typeIncaps As String)
  Dim wIstruzione As String, statement As String
    
  ' nella vecchia versione no template
  testo = CostruisciCall_CALLTemplate(testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr)
   
  RTBModifica.SelStart = wPos
  RTBModifica.SelLength = 0
  RTBModifica.SelText = testo
  wPos = wPos + Len(testo)
       
  If wPunto Then
    'SQ
    'statement = START_TAG & Space(wIdent) & "."
    '?wIdent
    statement = START_TAG & Space(11 - Len(START_TAG)) & "."
    statement = statement & Space(72 - Len(statement)) & END_TAG
    RTBModifica.SelStart = wPos
    RTBModifica.SelLength = 0
    RTBModifica.SelText = statement
    wPos = wPos + Len(statement)
  End If
End Sub
Sub INSERT_DECODC(wPos As Long, RTBModifica As RichTextBox, RTBErr As RichTextBox, rs As Recordset, typeIncaps As String)
  Dim wIstruzione As String, statement As String
    
  ' nella vecchia versione no template
  testo = CostruisciCall_CALLTemplate(testo, wIstruzione, typeIncaps, rs!Istruzione, rs, RTBErr)
  'insertline ENDIF
  ' TO DDDOOOOOOO
  RTBModifica.SelStart = wPos
  RTBModifica.SelLength = 0
  RTBModifica.SelText = testo
  wPos = wPos + Len(testo)
       
  If wPunto Then
    'SQ
    'statement = START_TAG & Space(wIdent) & "."
    '?wIdent
    statement = START_TAG & Space(11 - Len(START_TAG)) & "."
    statement = statement & Space(72 - Len(statement)) & END_TAG
    RTBModifica.SelStart = wPos
    RTBModifica.SelLength = 0
    RTBModifica.SelText = statement
    wPos = wPos + Len(statement)
  End If
End Sub

Function NumToken(Source As String) As Integer
' num elementi separati da virgola
Dim psource As String
  NumToken = 1
  psource = Source
  While InStr(1, psource, ";")
      NumToken = NumToken + 1
      psource = Right(psource, Len(psource) - InStr(1, psource, ";"))
  Wend
End Function
Function TokenIesimo(Source As String, pos As Integer) As String
' numerazione da 1
Dim i As Integer
Dim psource As String
  'elemento unico
  psource = Source
  multiOp = False
  For i = 1 To pos
    If InStr(1, psource, ";") = 0 Then
       TokenIesimo = psource
       Exit Function
    End If
    multiOp = True
    multiOpGen = True
    TokenIesimo = Left(psource, InStr(1, psource, ";") - 1)
    psource = Right(psource, Len(psource) - InStr(1, psource, ";"))
  Next
End Function
Function AggiornaCombinazione(pmaxoperatori As Integer) As Boolean
Dim lowelement As Integer, i As Integer, j As Integer, maxoperatori As Integer
Dim maxcommand As Integer
Dim aggiornato As Boolean
  aggiornato = False
  AggiornaCombinazione = False
  For i = 1 To UBound(TabIstr.BlkIstr)
    If Not aggiornato Then
      lowelement = Combinazionisegmenti(i)
      lowelement = lowelement + 1
      If lowelement <= NumToken(TabIstr.BlkIstr(i).SegmentM) Then
         Combinazionisegmenti(i) = lowelement
         aggiornato = True
         Exit For
      Else
        Combinazionisegmenti(i) = 1  ' equivale ad azzerare
      End If
    End If
  Next
  If Not aggiornato Then
    ' azzero segmenti
'    For i = 1 To UBound(TabIstr.BlkIstr)
'      Combinazionisegmenti(i) = 1
'    Next
    For i = 1 To UBound(TabIstr.BlkIstr)
      If Not aggiornato Then
        For j = 1 To UBound(TabIstr.BlkIstr(i).KeyDef)
           lowelement = MatriceCombinazioni(i, j)
           lowelement = lowelement + 1
           ' AC 05/07/06 devo usare indice i per livello e j per la chiave
           If UBound(TabIstr.BlkIstr(i).KeyDef) > 0 Then
           maxoperatori = NumToken(TabIstr.BlkIstr(i).KeyDef(j).Op)
           If lowelement <= maxoperatori Then
              MatriceCombinazioni(i, j) = lowelement
              aggiornato = True
              Exit For
           Else
              MatriceCombinazioni(i, j) = 1  ' equivale ad azzerare
           End If
           End If
        Next j
      Else
        Exit For
      End If
    Next i
    
    ' Volendo gestire anche molteplicit� su op logici occorre un'altra
    ' matrice di dimensioni pari a MatriceCombinazioni
    ' in pratica si ha prima l'azzeramento di MatriceCombinazioni come sotto
    ' poi si fa il ciclo di aggiornamento sulla nuova matrice (come per MatriceCombinazioni)
    ' il pezzo sotto riazzera la nuova matrice prima di incrementare
    ' la combinazione dei comcode
    
    
    ' AC 03/07/06  finite le combinazioni su operatori li azzero
    ' e incremento array con combinazioni commandcode
    If Not aggiornato Then
      ' riporto a 1 combinazione operatori
      For i = 1 To UBound(TabIstr.BlkIstr)
        For j = 1 To pmaxoperatori
          MatriceCombinazioni(i, j) = 1
        Next
      Next
      For i = 1 To UBound(TabIstr.BlkIstr)
        lowelement = CombinazioniComCode(i)
        lowelement = lowelement + 1
        maxcommand = NumToken(TabIstr.BlkIstr(i).Search)
        If lowelement <= maxcommand Then
          CombinazioniComCode(i) = lowelement
          'Controllo Dcombination
          If CheckDcomb(CombinazioniComCode) Then
            aggiornato = True
            Exit For
          End If
        Else
          CombinazioniComCode(i) = 1
        End If
      Next
    End If
  End If
  AggiornaCombinazione = aggiornato
End Function
Function AggiornaCombinazione_ETZ(pmaxoperatori As Integer) As Boolean
Dim lowelement As Integer, i As Integer, j As Integer, maxoperatori As Integer
Dim maxcommand As Integer
Dim aggiornato As Boolean
  aggiornato = False
  AggiornaCombinazione_ETZ = False
  For i = 1 To UBound(TabIstr.BlkIstr)
    If Not aggiornato Then
      For j = 1 To UBound(TabIstr.BlkIstr(i).KeyDef)
         lowelement = MatriceCombinazioni(i, j)
         lowelement = lowelement + 1
         ' AC 05/07/06 devo usare indice i per livello e j per la chiave
         If UBound(TabIstr.BlkIstr(i).KeyDef) > 0 Then
         maxoperatori = NumToken(TabIstr.BlkIstr(i).KeyDef(j).Op)
         If lowelement <= maxoperatori Then
            MatriceCombinazioni(i, j) = lowelement
            aggiornato = True
            Exit For
         Else
            MatriceCombinazioni(i, j) = 1  ' equivale ad azzerare
         End If
         End If
      Next j
    Else
      Exit For
    End If
  Next i
  
  ' Volendo gestire anche molteplicit� su op logici occorre un'altra
  ' matrice di dimensioni pari a MatriceCombinazioni
  ' in pratica si ha prima l'azzeramento di MatriceCombinazioni come sotto
  ' poi si fa il ciclo di aggiornamento sulla nuova matrice (come per MatriceCombinazioni)
  ' il pezzo sotto riazzera la nuova matrice prima di incrementare
  ' la combinazione dei comcode
  
  
  ' AC 03/07/06  finite le combinazioni su operatori li azzero
  ' e incremento array con combinazioni commandcode
  If Not aggiornato Then
    ' riporto a 1 combinazione operatori
    For i = 1 To UBound(TabIstr.BlkIstr)
      For j = 1 To pmaxoperatori
        MatriceCombinazioni(i, j) = 1
      Next
    Next
    For i = 1 To UBound(TabIstr.BlkIstr)
      lowelement = CombinazioniComCode(i)
      lowelement = lowelement + 1
      maxcommand = NumToken(TabIstr.BlkIstr(i).Search)
      If lowelement <= maxcommand Then
        CombinazioniComCode(i) = lowelement
        'Controllo Dcombination
        If CheckDcomb(CombinazioniComCode) Then
          aggiornato = True
          Exit For
        End If
      Else
        CombinazioniComCode(i) = 1
      End If
    Next
  End If
  AggiornaCombinazione_ETZ = aggiornato
End Function





Function CheckDcomb(ArrComCode() As Integer) As Boolean
Dim K As Integer, SumD As Integer
  For K = 1 To UBound(ArrComCode) - 1
   If InStr(1, TokenIesimo(TabIstr.BlkIstr(K).Search, ArrComCode(K)), "D") Then
    SumD = SumD + 1
   End If
  Next
  If SumD = 0 Or SumD = UBound(ArrComCode) - 1 Then  ' nessuna D
    CheckDcomb = True
  Else
    CheckDcomb = False
  End If
End Function

Sub CreaArrayCloniIstruzione(ByRef arraySource() As String, pIdOggetto As Long, pRiga As Long)
  Dim tbCloni As Recordset
  Dim i As Integer
  
  Set tbCloni = m_fun.Open_Recordset("select * from PsDLI_Istruzioni_Clone where " & _
                                     "IdOggetto = " & pIdOggetto & " and Riga = " & pRiga & " order by IdClone")
  ReDim arraySource(tbCloni.RecordCount)
  arraySource(0) = TabIstr.Istr
  i = 1
  While Not tbCloni.EOF
    arraySource(i) = tbCloni!Istruzione
    i = i + 1
    tbCloni.MoveNext
  Wend
  tbCloni.Close
End Sub

Sub CreaArrayCloniSegmenti(ByRef cloniSegmenti() As String, TabIstr As IstrDliLocal)
  Dim i As Integer
  
  ReDim cloniSegmenti(UBound(TabIstr.BlkIstr))
  For i = 1 To UBound(TabIstr.BlkIstr)
    cloniSegmenti(i) = TabIstr.BlkIstr(i).SegmentM
  Next i
End Sub

Sub aggiornalistaCodOp(Item As String)
  On Error GoTo alreadyPresent
  
  distinctCod.Add Item, Item
  
  Exit Sub
alreadyPresent:
  'ok
End Sub

Sub aggiornalistaSeg(Item As String)
  On Error GoTo Err
  distinctSeg.Add Item, Item
  
  Exit Sub
Err:

End Sub

Sub aggiornalistaDB(Item As String)
  On Error GoTo Err
  distinctDB.Add Item, Item
 
  Exit Sub
Err:

End Sub
'SQ CPY-ISTR
'Function CostruisciOperazioneTemplate(xIstr As String, Index As Integer, pIdOggetto As Long, pRiga As Long, ordPcb As Long)
Function CostruisciOperazioneTemplate(xIstr As String, Index As Integer, idpgm As Long, pIdOggetto As Long, pRiga As Long, ordPcb As Long)
  Dim tb As Recordset, tb1 As Recordset
  Dim wOpe As String, wOpeS As String, TipIstr As String
  Dim K As Long
  Dim xope As String
  Dim k1 As Integer
  Dim wUtil As Boolean, wFlagBoolean As Boolean
  Dim i As Integer, j As Integer
  Dim checkColl As Boolean
  Dim extraindent As Integer
  Dim maxId As Long
  Dim wIstr, wDecod As String, wIstrS As String, wDecodS As String, pcbDyn As String
  Dim maxPcbDyn As Integer, pcbDynInt As Integer
  Dim wOpeSave As String
  Dim y As Integer
  ' per comporre stringa if dell'incapsulato, versione qual e squal
  Dim IstrIncaps As String, IstrSIncaps As String, StrAnd As String
  ' contiene la molteplicit� di istruzioni, attinge dalla tabella Istruzioni_Clone
  Dim cloniIstruzione() As String
  Dim cloniSegmenti() As String
  Dim combinazioni As Boolean
  Dim maxLogicOp As Integer, maxboolean As Integer
  Dim Operatore As String
  Dim StringToClear As String
  Dim ifAnd As String
  Dim nomeroutcompleto As String
  
  On Error GoTo ErrH
  'AC 02/11/06
  
  Dim varop As String, combop As String, varistr As String, combistr As String
  Dim varop_s As String, combop_s As String, ks As Integer
  Dim varcmd As String, combcmd As String, varseg As String, combseg As String
  Dim varcmd_s As String, combcmd_s As String
  
  Dim CombPLevel() As String, CombPLevel_s() As String, CombSqual() As String, CombQual() As String, wIstrsOld As String
  
  Dim ComboPLevelTemp As String, ComboPLevelAppo As String, nFBms As Integer, wRecIn As String, jk As Integer
  Dim Rtb  As RichTextBox, appocomb As String, appofinalString As String, checksqual As Boolean, posendline As String
  ' tango traccia di una qualsiasi molteplicit� sui livelli
  Dim moltlevel As Boolean, moltlevelesimo() As Boolean, ij As Integer
  Set Rtb = MavsdF_Incapsulatore.RTBR
  wPath = m_fun.FnPathPrj

  
  nFBms = FreeFile
  'stefanopul
  'Open wpath & "\input-prj\Template\INCAPS\COMBOPLEVEL" For Input As nFBms
  Open wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\COMBOPLEVEL" For Input As nFBms
  While Not EOF(nFBms)
    If Not ComboPLevelTemp = "" Then
      ComboPLevelTemp = ComboPLevelTemp & vbCrLf
    End If
    Line Input #nFBms, wRecIn
    ComboPLevelTemp = ComboPLevelTemp & wRecIn
  Wend
  
  
  jk = 1
  
  ' AC : wMoltIstr(0), wMoltIstr(2) -> squalificata , wMoltIstr(1),wMoltIstr(3) -> qualificata , se QualAndUnqual tutte e quattro
  
  'Ac per la gestione delle combinazioni
  ' righe = livelli, colonne = max operatori logici
  ' la matrice contiene gli indici degli operatori che per ogni livello e operatore logico
  ' devono essere presi in considerazione per la decodifica
  ' ad ogni ciclo di combinazione si aggiorna la matrice e si crea una nuova decodifica combinando
  ' le info sui vari livelli e operatori logici
  ' la decodifica � inserita nella collection di wMoltIstr
  
  For K = 1 To UBound(TabIstr.BlkIstr)
    If UBound(TabIstr.BlkIstr(K).KeyDef) > maxLogicOp Then
     maxLogicOp = UBound(TabIstr.BlkIstr(K).KeyDef)
    End If
  Next
  
  
 
  ReDim MatriceCombinazioni(UBound(TabIstr.BlkIstr), maxLogicOp)
  ReDim CombinazioniOpLogici(UBound(TabIstr.BlkIstr), maxLogicOp)
  ' array combinazioni command code : per ogni livello mi dice
  ' sequenza command code che deve essere considerate
  ReDim CombinazioniComCode(UBound(TabIstr.BlkIstr))
  ReDim Combinazionisegmenti(UBound(TabIstr.BlkIstr))
  ReDim moltlevelesimo(UBound(TabIstr.BlkIstr))
  For K = 1 To UBound(TabIstr.BlkIstr)
    For k1 = 1 To maxLogicOp
      MatriceCombinazioni(K, k1) = 1
      CombinazioniOpLogici(K, k1) = 1
    Next
  Next
  For K = 1 To UBound(TabIstr.BlkIstr)
    CombinazioniComCode(K) = 1
    Combinazionisegmenti(K) = 1
  Next
  CreaArrayCloniIstruzione cloniIstruzione, pIdOggetto, pRiga
  CreaArrayCloniSegmenti cloniSegmenti, TabIstr
'  For K = 1 To UBound(cloniSegmenti)
'    If NumToken(cloniSegmenti(K)) > maxsegment Then
'     maxsegment = NumToken(cloniSegmenti(K))
'    End If
'  Next
  
  '***** determiniamo se deve essere generata
  '***** sia la versione qualificata che squalificata
  Set tb = m_fun.Open_Recordset("select * from PsDLI_IstrDett_Liv where IdOggetto =" & pIdOggetto & " and Riga = " & pRiga & " and Livello = " & UBound(TabIstr.BlkIstr))
  If Not tb.EOF Then
    QualAndUnqual = tb!QualAndUnqual
  Else
    QualAndUnqual = False
  End If
  tb.Close
  
  Set wMoltIstr(0) = New collection
  Set wMoltIstr(1) = New collection
  Set wMoltIstr(2) = New collection
  Set wMoltIstr(3) = New collection
  StringToClear = ""
  ReDim wNomeRout(0)
  
  ' Ciclo esterno su istruzione
  ReDim Preserve CombSqual(0)
  
  For i = 0 To UBound(cloniIstruzione)
    xIstr = cloniIstruzione(i)
    TipIstr = Left(xIstr, 2)
    If TabIstr.isGsam Then
       TipIstr = "GS"
    ElseIf xIstr = "GU" Or xIstr = "GHU" Then
       TipIstr = "GU"
    ElseIf xIstr = "ISRT" Then
      TipIstr = "UP"
    ElseIf xIstr = "DLET" Then
       TipIstr = "UP"
    ElseIf xIstr = "REPL" Then
       TipIstr = "UP"
    ElseIf xIstr = "GNP" Or xIstr = "GHNP" Then
       TipIstr = "GP"
    ElseIf xIstr = "GN" Or xIstr = "GHN" Then
       TipIstr = "GN"
    ElseIf xIstr = "PCB" Then
       wUtil = True
       TipIstr = "SH"
    ElseIf xIstr = "TERM" Then
       wUtil = True
       TipIstr = "TM"
    ElseIf xIstr = "CHKP" Then
       wUtil = True
       TipIstr = "CP"
    ElseIf xIstr = "QRY" Then
       wUtil = True
       TipIstr = "QY"
    ElseIf xIstr = "SYNC" Then
       wUtil = True
       TipIstr = "SY"
    Else
       m_fun.WriteLog "Incapsulatore - Costruisci Operazione " & vbCrLf & "Istruzione non riconosciuta " & xIstr, "DR - Incapsulatore"
    End If
  
    'riapplico il calcolo del NomeRout per ogni Molteplicit� di istruzione
    If UBound(TabIstr.dbd) > 0 Then
      nomeroutine = m_fun.Genera_Nome_Routine(TabIstr.dbd(1).nome, xIstr, SwTp)
      'aggiornalistaDB (TabIstr.Dbd(1).nome)
    Else
      nomeroutine = m_fun.Genera_Nome_Routine("", xIstr, SwTp)
    End If
   
   'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
   If TipIstr <> "GU" And TipIstr <> "GN" And TipIstr <> "GP" Then QualAndUnqual = False
    
   combinazioni = True
   IstrIncaps = ""
   Dcombination = False
   
   While combinazioni
   
   multiOpGen = False
   'AC 02/11/06
   ReDim CombPLevel(UBound(TabIstr.BlkIstr))
   ' alberto, sulle istruzioni OPEN e CLSE dava errore per cui l'ho cambiata..
   If UBound(TabIstr.BlkIstr) > 0 Then
     ReDim CombPLevel_s(UBound(TabIstr.BlkIstr) - 1)
   End If
     IstrIncaps = ""
     If UBound(cloniIstruzione) > 0 Then
        varistr = Restituisci_varFunz(pIdOggetto, pRiga)
        combistr = "'" & xIstr & "'"
     End If
     wOpe = "<pgm>" & nomeroutine & "</pgm><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr>"
     wOpe = wOpe & "<livelli>"
     
     For K = 1 To UBound(TabIstr.BlkIstr)   ' ciclo su livelli
     
        ComboPLevelAppo = ComboPLevelTemp
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' SQ 28-07-06
        ' OK: REPL CON SSA QUALIFICATA DA RETURN CODE "AJ"
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
          If TabIstr.BlkIstr(K).IdSegm = -1 Then
             wOpe = wOpe & "<level" & K & "><segm>" & "DYNAMIC" & "</segm>"
          Else
             wOpe = wOpe & "<level" & K & "><segm>" & TokenIesimo(cloniSegmenti(K), Combinazionisegmenti(K)) & "</segm>"
             If NumToken(cloniSegmenti(K)) > 1 Then
              varseg = TabIstr.BlkIstr(K).ssaName & "(1:8)"
              combseg = "'" & TokenIesimo(cloniSegmenti(K), Combinazionisegmenti(K)) & "'"
              moltlevelesimo(K) = True
             Else
              varseg = ""
             End If
             aggiornalistaSeg (TabIstr.BlkIstr(K).Segment)
          End If
        Else
          If (xIstr = "REPL" Or xIstr = "DLET") And K = UBound(TabIstr.BlkIstr) Then
            If TabIstr.BlkIstr(K).IdSegm = -1 Then
               wOpe = wOpe & "<level1><segm>" & "DYNAMIC" & "</segm>"
            Else
               wOpe = wOpe & "<level1><segm>" & TokenIesimo(cloniSegmenti(K), Combinazionisegmenti(K)) & "</segm>"
               If NumToken(cloniSegmenti(K)) > 1 Then
                varseg = TabIstr.BlkIstr(K).ssaName & "(1:8)"
                combseg = "'" & TokenIesimo(cloniSegmenti(K), Combinazionisegmenti(K)) & "'"
                moltlevelesimo(K) = True
               Else
                varseg = ""
               End If
               aggiornalistaSeg (TabIstr.BlkIstr(K).Segment)
            End If
          End If
        End If
        
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Then
          If TabIstr.BlkIstr(K).Record <> "" Then
             If Trim(TabIstr.BlkIstr(K).Segment) <> "" Then
                 wOpe = wOpe & "<area>" & "AREA" & "</area>"
             End If
          End If
        End If
        
        'AC 10/04/2007 spostata per non sommare a wopes la <ccode> del wope
        If QualAndUnqual And K = UBound(TabIstr.BlkIstr) Then
          wOpeS = wOpe   ' allineiamo  la wOpeS  alla wOpe che si differenziano solo per ultimo livello
          'varop_s = varop
          'combop_s = combop
          For ks = 1 To K - 1
            CombPLevel_s(ks) = CombPLevel(ks)
          Next
          
          ' allineiamo le stringhe
        End If
        
        
        
        
        'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
          If Trim(Replace(Replace(TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K)), "*", ""), "-", "")) <> "" Then
             If Trim(TabIstr.BlkIstr(K).Segment) <> "" Then
                wOpe = wOpe & "<ccode>" & Trim(Replace(Replace(TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K)), "*", ""), "-", "")) & "</ccode>"
             End If
          End If
        End If
        
        'AC 03/07/06
        'IstrIncaps deve tenere conto del controllo sul command code
        'If SSAName(9:Len(Search))
        ' lo metto prima degli operatori
        'ultimo
        'If k > 1 Then ifAnd = "AND" 'Else determinato dalla presenza o meno di istr multipla
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
          If NumToken(TabIstr.BlkIstr(K).Search) > 1 Then
            varcmd = TabIstr.BlkIstr(K).ssaName & "(9:" & Len(TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K))) & ")"
            combcmd = "'" & TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K)) & "'"
            moltlevelesimo(K) = True
          Else
            varcmd = ""
          End If
        End If
          '*********************************
        
        
        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' QUALIFICAZIONE:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' AC se entrambe le versione wOpe � quella qualificata e wOpeS quella
        ' squalificata, altrimenti se unica sempre in wOpe
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
        If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or (xIstr = "ISRT" And K < UBound(TabIstr.BlkIstr)) Then
         If QualAndUnqual And K = UBound(TabIstr.BlkIstr) Then
          'parte SQUALIFICATA
          ' la wOpeS si ferma qui, wOpeS e wOpe differiscono solo per questo livello
           
          'parte QUALIFICATA
          wFlagBoolean = False
             For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
               If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
                 If Len(TabIstr.BlkIstr(K).KeyDef(k1).OpLogic) Then
                     wOpe = wOpe & "<key>" & TabIstr.BlkIstr(K).KeyDef(k1).key & "</key>"
                     Operatore = TokenIesimo(TabIstr.BlkIstr(K).KeyDef(k1).Op, MatriceCombinazioni(K, k1))
                     wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                     wOpe = wOpe & "<bool>" & transcode_booleano(TabIstr.BlkIstr(K).KeyDef(k1).OpLogic) & "</bool>"
                    wFlagBoolean = True
                 Else
                     wOpe = wOpe & "<key>" & TabIstr.BlkIstr(K).KeyDef(k1).key & "</key>"
                     Operatore = TokenIesimo(TabIstr.BlkIstr(K).KeyDef(k1).Op, MatriceCombinazioni(K, k1))
                     wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                 End If
                'AC 03/07/06
                'ultimo
                 If multiOp Then
                    ' da riparametrizzare
                    'If (wIdent + extraIndent + Len(ifAnd) + 38 + Len(TabIstr.BlkIstr(k).SSAName)) > 72 Then
                       varop = TabIstr.BlkIstr(K).ssaName & "(" & Int(TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
                       combop = "REV" & Operatore & "1" & vbCrLf _
                       & "    " & Space(Len(TabIstr.BlkIstr(K).ssaName)) & "     OR REV" & Operatore & "2" & vbCrLf _
                       & "    " & Space(Len(TabIstr.BlkIstr(K).ssaName)) & "     OR REV" & Operatore & "3"
                       moltlevelesimo(K) = True
                    'Else
                       'varop = TabIstr.BlkIstr(k).SSAName & "(" & Int(TabIstr.BlkIstr(k).KeyDef(k1).KeyStart) - 2 & ":2)"
                       'combop = "REV" & Operatore & "1 OR REV" & Operatore & "2 OR REV" & Operatore & "3"
                    'End If
                 Else
                    varop = ""
                 End If
               End If
             Next k1
             If K > 1 Then
              For ij = 1 To K - 1
                If moltlevelesimo(ij) = True Then
                  moltlevel = True
                  Exit For
                End If
              Next
             End If
             If Not varop = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VAROP>", varop)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBOP>", combop)
              'attenzione che se la molteplicit� � solo sul secondo livello, NON FUNZIONA
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltlevel Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(K) = ComboPLevelAppo
             Else
              CombPLevel(K) = ComboPLevelAppo
             End If
             If Not varcmd = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varcmd)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combcmd)
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltlevel Or Not varop = "" Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(K) = ComboPLevelAppo
            Else
              CombPLevel(K) = ComboPLevelAppo
            End If
            If Not varseg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varseg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combseg)
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltlevel Or Not varop = "" Or Not varcmd = "" Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(K) = ComboPLevelAppo
            Else
              CombPLevel(K) = ComboPLevelAppo
            End If
         Else  ' o la versione squalificata o quella squalificata
         
          ' AC il campo parentesi e la trasformazione in stringa del booleano Qualificazione
          If TabIstr.BlkIstr(K).Parentesi <> "(" Then ' Or Trim(TabIstr.BlkIstr(k).SsaName) = "<none>" Then
             OnlySqual = True
          Else   ' Qualificata
             OnlyQual = True
             wFlagBoolean = False
             For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
               If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
                 If Len(TabIstr.BlkIstr(K).KeyDef(k1).OpLogic) Then
                    wOpe = wOpe & "<key>" & TabIstr.BlkIstr(K).KeyDef(k1).key & "</key>"
                    Operatore = TokenIesimo(TabIstr.BlkIstr(K).KeyDef(k1).Op, MatriceCombinazioni(K, k1))
                    wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                    wOpe = wOpe & "<bool>" & transcode_booleano(TabIstr.BlkIstr(K).KeyDef(k1).OpLogic) & "</bool>"
                    wFlagBoolean = True
                 Else
                    wOpe = wOpe & "<key>" & TabIstr.BlkIstr(K).KeyDef(k1).key & "</key>"
                    Operatore = TokenIesimo(TabIstr.BlkIstr(K).KeyDef(k1).Op, MatriceCombinazioni(K, k1))
                    wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                 End If
                'IstrIncaps = IstrIncaps & CreaSp(wIdent + extraIndent) & "AND " & TabIstr.BlkIstr(k).SsaName & "(" & FindOpPosition(pIdOggetto, pRiga, k1) & ":2)" & " = REV" & Operatore & " " & vbCrLf
                'sp: basta vado avanti
               
                 'AC 03/07/06
                 'stefano; ultimo: provo
                 'If (k1 > 1 Or MoltCCode Or MoltIstruzione) Then ifAnd = "AND" Else ifAnd = "IF"
                 If multiOp Then
                 'stefano: provo a spezzare solo quando serve
                 'alberto: prch� una var globale valorizzata dentro funzione generica ?
                    'If (wIdent + extraIndent + Len(ifAnd) + 38 + Len(TabIstr.BlkIstr(k).SSAName)) > 72 Then
                    
                       varop = TabIstr.BlkIstr(K).ssaName & "(" & Int(TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
                       combop = "REV" & Operatore & "1" & vbCrLf _
                       & "    " & Space(Len(TabIstr.BlkIstr(K).ssaName)) & "     OR REV" & Operatore & "2" & vbCrLf _
                       & "    " & Space(Len(TabIstr.BlkIstr(K).ssaName)) & "     OR REV" & Operatore & "3"
                       moltlevelesimo(K) = True
'                    Else
'                       varop = TabIstr.BlkIstr(k).SSAName & "(" & Int(TabIstr.BlkIstr(k).KeyDef(k1).KeyStart) - 2 & ":2)"
'                       combop = "REV" & Operatore & "1 OR REV" & Operatore & "2 OR REV" & Operatore & "3"
'
'                    End If
                 Else
                    varop = ""
                 End If
               End If
             Next k1
          End If '-- squal o qual
          If K > 1 Then
            For ij = 1 To K - 1
              If moltlevelesimo(ij) = True Then
                moltlevel = True
                Exit For
              End If
            Next
          End If
          If Not varop = "" Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<VAROP>", varop)
            ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBOP>", combop)
            If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltlevel Then 'AND -- alternativa 1
              ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 1))
            Else
              ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 2))
            End If
            If QualAndUnqual Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
            End If
            CombPLevel(K) = ComboPLevelAppo
          Else
              CombPLevel(K) = ComboPLevelAppo
          End If
          If Not varcmd = "" Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARCMD>", varcmd)
            ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBCMD>", combcmd)
            If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltlevel Or Not varop = "" Then 'AND -- alternativa 1
              ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 1))
            Else
              ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<C%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<C%"), 2))
            End If
            If QualAndUnqual Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
            End If
            CombPLevel(K) = ComboPLevelAppo
          Else
              CombPLevel(K) = ComboPLevelAppo
          End If
          If Not varseg = "" Then
              ComboPLevelAppo = Replace(ComboPLevelAppo, "<VARSEG>", varseg)
              ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBSEG>", combseg)
              If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or moltlevel Or Not varop = "" Or Not varcmd = "" Then 'AND -- alternativa 1
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 1))
              Else
                ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<E%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<E%"), 2))
              End If
              If QualAndUnqual Then
                ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
              End If
              CombPLevel(K) = ComboPLevelAppo
            Else
              CombPLevel(K) = ComboPLevelAppo
            End If
        End If   '-- una sola versione o due versioni
       End If
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
       If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        wOpe = wOpe & "</level" & K & ">"
        If QualAndUnqual And K = UBound(TabIstr.BlkIstr) Then
          wOpeS = wOpeS & "</level" & K & ">"
        End If
       Else
        If (xIstr = "REPL" Or xIstr = "DLET") And K = UBound(TabIstr.BlkIstr) Then
          wOpe = wOpe & "</level1>"
          If QualAndUnqual And K = UBound(TabIstr.BlkIstr) Then
            wOpeS = wOpeS & "</level1>"
          End If
        End If
       End If
     Next K   ' FINE CICLO LIVELLI
     
     wOpe = wOpe & "</livelli>"
     
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
      If Trim(TabIstr.keyfeedback) <> "" Then
         wOpe = wOpe & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
      End If
     End If
     
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
      If Len(TabIstr.procSeq) Then
         wOpe = wOpe & "<procseq>" & TabIstr.procSeq & "</procseq>"
      End If
     End If

     If xIstr = "OPEN" Then
      If Len(TabIstr.procOpt) Then
         wOpe = wOpe & "<procopt>" & TabIstr.procOpt & "</procopt>"
      End If
     End If
     
'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpe
     For K = 1 To UBound(OpeIstrRt)
        If wOpe = OpeIstrRt(K).Decodifica Then
           If OpeIstrRt(K).ordPcb = ordPcb Then
              'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
              'verifico se � la prima
              pcbDyn = Format(OpeIstrRt(K).pcbDyn, "000")
              pcbDynInt = OpeIstrRt(K).pcbDyn
              maxPcbDyn = 0
              Exit For
           Else
              If OpeIstrRt(K).pcbDyn > maxPcbDyn Then
              'trovata un'istruzione identica, ma con diverso pcb
                 maxPcbDyn = OpeIstrRt(K).pcbDyn
              End If
           End If
        End If
     Next K
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
     If maxPcbDyn > 0 Then
        pcbDyn = Format(maxPcbDyn + 1, "000")
        pcbDynInt = maxPcbDyn + 1
     End If
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
     ' aggiunge il pcb dinamico
      If Len(pcbDyn) Then
         wOpe = wOpe & "<pcbseq>" & pcbDyn & "</pcbseq>"
      Else
      ' se � la prima istruzione di quel tipo
         wOpe = wOpe & "<pcbseq>" & "001" & "</pcbseq>"
         pcbDynInt = 1
      End If
     End If
    
     wDecod = wOpe
     Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpe & "'")
     
     'If xope = "" Then
     If tb.EOF Then
       Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
          maxId = 1
       Else
          maxId = tb1(0) + 1
       End If
       tb.AddNew
       tb!IdIstruzione = maxId
       tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
       wIstr = tb!Codifica
       tb!Decodifica = wOpe
       tb!ImsDB = True
       tb!ImsDC = False
       tb!Cics = SwTp
   'stefano: routine multiple
       nomeroutcompleto = Genera_Counter(nomeroutine)
       tb!NomeRout = nomeroutcompleto
       tb.Update
       tb.Close
    'pcb dinamico
       K = UBound(OpeIstrRt) + 1
       ReDim Preserve OpeIstrRt(K)
       OpeIstrRt(K).Decodifica = wOpeSave
       OpeIstrRt(K).Istruzione = wIstr
       OpeIstrRt(K).ordPcb = ordPcb
       OpeIstrRt(K).pcbDyn = pcbDynInt
     Else
       wIstr = tb!Codifica
       nomeroutcompleto = tb!NomeRout
       tb.Close
     End If
     
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     'SQ CPY-ISTR
     'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
     Set tb = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrCodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
     If tb.EOF Then
        'SQ 16-11-06 - EMBEDDED
        'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & nomeroutcompleto & "')"
        'SQ CPY-ISTR
        'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeroutcompleto) & "')"
        m_fun.FnConnection.Execute "INSERT INTO PsDLI_IstrCodifica VALUES(" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeroutcompleto) & "')"
     End If
     tb.Close
     If QualAndUnqual Then
       wOpeS = wOpeS & "</livelli>"
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
      If Trim(TabIstr.keyfeedback) <> "" Then
        wOpeS = wOpeS & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
      End If
'procseq: sarebbe meglio metterlo in tabistr, ma per ora facciamo cos� (procseq
' ci servir� anche nella generazione routine
      If UBound(TabIstr.psb) Then
         If Len(TabIstr.procSeq) Then
             wOpeS = wOpeS & "<procseq>" & TabIstr.procSeq & "</procseq>"
         End If
         If Len(TabIstr.procOpt) And xIstr = "OPEN" Then
             wOpeS = wOpeS & "<procopt>" & TabIstr.procOpt & "</procopt>"
         End If
      End If
     End If
      


'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpeS
     For K = 1 To UBound(OpeIstrRt)
        If wOpeS = OpeIstrRt(K).Decodifica Then
           If OpeIstrRt(K).ordPcb = ordPcb Then
              'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
              'verifico se � la prima
              pcbDyn = Format(OpeIstrRt(K).pcbDyn, "000")
              pcbDynInt = OpeIstrRt(K).pcbDyn
              maxPcbDyn = 0
              Exit For
           Else
              If OpeIstrRt(K).pcbDyn > maxPcbDyn Then
              'trovata un'istruzione identica, ma con diverso pcb
                 maxPcbDyn = OpeIstrRt(K).pcbDyn
              End If
           End If
        End If
     Next K
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
     If maxPcbDyn > 0 Then
        pcbDyn = Format(maxPcbDyn + 1, "000")
        pcbDynInt = maxPcbDyn + 1
     End If
     ' aggiunge il pcb dinamico
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
      If Len(pcbDyn) Then
         wOpeS = wOpeS & "<pcbseq>" & pcbDyn & "</pcbseq>"
      Else
      ' se � la prima istruzione di quel tipo
         wOpeS = wOpeS & "<pcbseq>" & "001" & "</pcbseq>"
         pcbDynInt = 1
      End If
     End If
     
     wDecodS = wOpeS
     Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpeS & "'")
     
     'If xope = "" Then
     If tb.EOF Then
         

       Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
          maxId = 1
       Else
          maxId = tb1(0) + 1
       End If
       tb.AddNew
       tb!IdIstruzione = maxId
       tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
       wIstrS = tb!Codifica
       tb!Decodifica = wOpeS
       tb!ImsDB = True
       tb!ImsDC = False
   'stefano: routine multiple
       nomeroutcompleto = Genera_Counter(nomeroutine)
       tb!NomeRout = nomeroutcompleto
       tb.Update
       tb.Close
    'pcb dinamico
       K = UBound(OpeIstrRt) + 1
       ReDim Preserve OpeIstrRt(K)
       OpeIstrRt(K).Decodifica = wOpeSave
       OpeIstrRt(K).Istruzione = wIstr
       OpeIstrRt(K).ordPcb = ordPcb
       OpeIstrRt(K).pcbDyn = pcbDynInt
     Else
       wIstrS = tb!Codifica
       nomeroutcompleto = tb!NomeRout
       tb.Close
     End If
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     'virgilio CPY-ISTR
     'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
     Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
     If tb.EOF Then
        'virgilio CPY-ISTR
        'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeroutcompleto & "')"
        m_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeroutcompleto & "')"
     End If
     tb.Close
    End If
    'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    'BACO: per ora non viene gestita la molteplicit� sulla ISRT per i livelli qualificati
    ' ma in realt� non ha senso (di solito l'inserimento viene fatto qualificando un
    ' livello precedente per uguale, in modo da puntare un solo parent
    If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Then
     combinazioni = AggiornaCombinazione(maxLogicOp)
    Else
     combinazioni = False
    End If
    moltlevel = False
     ' aggiornamento collection
     If Not StringToClear = "" Then
      IstrIncaps = Replace(IstrIncaps, StringToClear, "")
      IstrSIncaps = Replace(IstrSIncaps, StringToClear, "")
     End If
     
     If QualAndUnqual Then
       ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
       checkColl = True
       
       For j = 1 To wMoltIstr(3).Count
        
        If wMoltIstr(3).Item(j) = wIstr Then
          checkColl = False
          
          Exit For
        End If
       Next
       If checkColl Then
        wMoltIstr(1).Add IstrIncaps, wDecod
        wMoltIstr(3).Add wIstr, wDecod
        aggiornalistaCodOp (wIstr)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeroutcompleto
       End If
       
       ' 0 e 2  da wOpeS, indice ks dell'array e da IstrSIncaps
       checkColl = True
       checksqual = True
       For j = 1 To wMoltIstr(2).Count
'        If wMoltIstr(2).Item(j) = OpeIstrRt(ks).Istruzione Then
        If wMoltIstr(2).Item(j) = wIstrS Then
          checkColl = False
          checksqual = False
          Exit For
        End If
       Next
       If checkColl Then
        wMoltIstr(0).Add IstrSIncaps, wDecod
        wMoltIstr(2).Add wIstrS, wDecodS
        aggiornalistaCodOp (wIstrS)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeroutcompleto
       End If
     Else
         ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
         checkColl = True
         For j = 1 To wMoltIstr(3).Count
'          If wMoltIstr(3).Item(j) = OpeIstrRt(k).Istruzione Then
          If wMoltIstr(3).Item(j) = wIstr Then
            checkColl = False
            Exit For
          End If
         Next
         If checkColl Then
          wMoltIstr(1).Add IstrIncaps, wDecod
          wMoltIstr(3).Add wIstr, wDecod
          aggiornalistaCodOp (wIstr)
          y = y + 1
          ReDim Preserve wNomeRout(y)
          wNomeRout(y) = nomeroutcompleto
         End If
       
     End If
     ' livello COMBQUAL -- costruisce elemento array combqual
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\COMBQUAL"
      If QualAndUnqual Then
        MavsdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MavsdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
      Else
        ' gestisco indentazione move
        If multiOpGen Or UBound(cloniIstruzione) > 0 Then ' ne lascio solo una
          MavsdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        End If
      End If
      
      'if or end per l'istruzione e relativo end if di chiusura
      If QualAndUnqual Then
        MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
        MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
      Else
        MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 2)
        ' end if
        If multiOpGen Or UBound(cloniIstruzione) > 0 Then
          MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
        Else
           MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
        End If
      End If
      
      MavsdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
      MavsdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
      MavsdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
      MavsdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
      MavsdM_GenRout.changeTag Rtb, "<CODICEOPERB>", Replace(wIstr, ".", "")
      
      If EmbeddedRoutine Then
        MavsdM_GenRout.changeTag Rtb, "<embedded>", ""
      Else
        MavsdM_GenRout.changeTag Rtb, "<standard>", ""
      End If
            
      For K = 1 To UBound(CombPLevel)
        MavsdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(K), True
      Next
      
      appocomb = Rtb.text
'stefanopul
      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFQUAL"
      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\IFQUAL"
      MavsdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
      ReDim Preserve CombQual(jk)
      CombQual(jk) = Rtb.text
      
      If QualAndUnqual And checksqual Then ' due parti, la seconda � squalificata
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\COMBQUAL"
        MavsdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
        MavsdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
        MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
        MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
        MavsdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
        MavsdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
        MavsdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
        MavsdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstrS
        wIstrsOld = wIstrS
        If EmbeddedRoutine Then
          MavsdM_GenRout.changeTag Rtb, "<embedded>", ""
        Else
          MavsdM_GenRout.changeTag Rtb, "<standard>", ""
        End If
        For K = 1 To UBound(CombPLevel_s)
          MavsdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel_s(K), True
        Next
        appocomb = Rtb.text
        'stefanopul
        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\IFSQUAL"
        MavsdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
        ReDim Preserve CombSqual(UBound(CombSqual) + 1)
        CombSqual(UBound(CombSqual)) = Rtb.text
      ElseIf Not QualAndUnqual Then
'       ' anche nella parte squalificata metto la stessa cosa della qualificata
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
'        MavsdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
'        MavsdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
'        MavsdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
'        MavsdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
'        For k = 1 To UBound(CombPLevel_s)
'          MavsdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(k), True
'        Next
'        appocomb = Rtb.text
'        Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
'        MavsdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
'        ReDim Preserve CombSqual(jk)
'        CombSqual(jk) = Rtb.text
      End If
      jk = jk + 1
      '**********
   Wend
  Next i  'ciclo su molteplicit� istruzioni
  'sostituzioni nel file principale
  
  
   'stefanopul
   'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\MAINCOMB"
   Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\MAINCOMB"
   MavsdM_GenRout.changeTag Rtb, "<SSA-INSPECT>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).ssaName & "(9:" & Len(TokenIesimo(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Search, 1)) + 1 & ")"
   For i = 1 To UBound(CombQual)
     MavsdM_GenRout.changeTag Rtb, "<<IFQUAL(i)>>", CombQual(i)
   Next
   If QualAndUnqual Then
    For i = 1 To UBound(CombSqual)
      MavsdM_GenRout.changeTag Rtb, "<<IFSQUAL(i)>>", CombSqual(i)
    Next
    MavsdM_GenRout.changeTag Rtb, "<condsqual>", "" ' altrimenti istr corrispondenti eliminate
   End If
   appofinalString = Rtb.text
  
   While InStr(appofinalString, vbCrLf) > 0
     posendline = InStr(1, appofinalString, vbCrLf)
     insertLine testo, Left(appofinalString, posendline - 1), indentkey
     appofinalString = Right(appofinalString, Len(appofinalString) - posendline - 1)
   Wend
   If Not Trim(appofinalString) = "" Then
     insertLine testo, appofinalString, indentkey
   End If
  Close #nFBms
  
  Exit Function
ErrH:
  MsgBox Err.Description
  'insertLine testo, Rtb.text
  Resume Next
End Function

Function getIstruzione_ETZ(xIstr As String, Index As Integer, idpgm As Long, _
                           pIdOggetto As Long, pRiga As Long, ordPcb As Long) As String
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''   copia della funzione ''''CostruisciOperazioneTemplate'''' alleggerita ''''
''''   ritorna l'istruzione                                                  ''''
''''   es. 'GU.0001'                                                         ''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim tb, tb1 As Recordset
  Dim wOpe As String, wOpeS As String, TipIstr As String
  Dim K As Long
  Dim xope As String
  Dim k1 As Integer
  Dim wUtil As Boolean, wFlagBoolean As Boolean
  Dim i As Integer, j As Integer
  Dim checkColl As Boolean
  Dim extraindent As Integer
  Dim maxId As Long
  Dim wIstr, wDecod As String, wIstrS As String, wDecodS As String, pcbDyn As String
  Dim maxPcbDyn As Integer, pcbDynInt As Integer
  Dim wOpeSave As String
  Dim y As Integer
  ' per comporre stringa if dell'incapsulato, versione qual e squal
  Dim IstrIncaps As String, IstrSIncaps As String, StrAnd As String
  ' contiene la molteplicit� di istruzioni, attinge dalla tabella Istruzioni_Clone
  Dim cloniIstruzione() As String
  Dim combinazioni As Boolean
  Dim maxLogicOp As Integer, maxboolean As Integer
  Dim Operatore As String
  Dim StringToClear As String
  Dim ifAnd As String
  Dim nomeroutcompleto As String
  
  
  
  'AC 02/11/06
  
  Dim varop As String, combop As String, varistr As String, combistr As String
  Dim varop_s As String, combop_s As String, ks As Integer
  
  Dim CombPLevel() As String, CombPLevel_s() As String, CombSqual() As String, CombQual() As String, wIstrsOld As String
  
  Dim ComboPLevelTemp As String, ComboPLevelAppo As String, nFBms As Integer, wRecIn As String, jk As Integer
  Dim Rtb  As RichTextBox, appocomb As String, appofinalString As String, checksqual As Boolean, posendline As String
  
  
  jk = 1
   
  For K = 1 To UBound(TabIstr.BlkIstr)
    If UBound(TabIstr.BlkIstr(K).KeyDef) > maxLogicOp Then
     maxLogicOp = UBound(TabIstr.BlkIstr(K).KeyDef)
    End If
  Next
  
  ReDim MatriceCombinazioni(UBound(TabIstr.BlkIstr), maxLogicOp)
  ReDim CombinazioniOpLogici(UBound(TabIstr.BlkIstr), maxLogicOp)
  ReDim CombinazioniComCode(UBound(TabIstr.BlkIstr))
  
  For K = 1 To UBound(TabIstr.BlkIstr)
    For k1 = 1 To maxLogicOp
      MatriceCombinazioni(K, k1) = 1
      CombinazioniOpLogici(K, k1) = 1
    Next
  Next
  For K = 1 To UBound(TabIstr.BlkIstr)
    CombinazioniComCode(K) = 1
  Next
  CreaArrayCloniIstruzione cloniIstruzione, pIdOggetto, pRiga
  
  '***** determiniamo se deve essere generata
  '***** sia la versione qualificata che squalificata
  Set tb = m_fun.Open_Recordset("select * from PsDLI_IstrDett_Liv where IdOggetto =" & pIdOggetto & " and Riga = " & pRiga & " and Livello = " & UBound(TabIstr.BlkIstr))
  If Not tb.EOF Then
    QualAndUnqual = tb!QualAndUnqual
  Else
    QualAndUnqual = False
  End If
  tb.Close
  
  Set wMoltIstr(0) = New collection
  Set wMoltIstr(1) = New collection
  Set wMoltIstr(2) = New collection
  Set wMoltIstr(3) = New collection
  StringToClear = ""
  ReDim wNomeRout(0)
  
  ' Ciclo esterno su istruzione
  ReDim Preserve CombSqual(0)
  
  For i = 0 To UBound(cloniIstruzione)
    xIstr = cloniIstruzione(i)
    TipIstr = Left(xIstr, 2)
    If TabIstr.isGsam Then
       TipIstr = "GS"
    ElseIf xIstr = "GU" Or xIstr = "GHU" Then
       TipIstr = "GU"
    ElseIf xIstr = "ISRT" Then
      TipIstr = "UP"
    ElseIf xIstr = "DLET" Then
       TipIstr = "UP"
    ElseIf xIstr = "REPL" Then
       TipIstr = "UP"
    ElseIf xIstr = "GNP" Or xIstr = "GHNP" Then
       TipIstr = "GP"
    ElseIf xIstr = "GN" Or xIstr = "GHN" Then
       TipIstr = "GN"
    ElseIf xIstr = "PCB" Then
       wUtil = True
       TipIstr = "SH"
    ElseIf xIstr = "TERM" Then
       wUtil = True
       TipIstr = "TM"
    ElseIf xIstr = "CHKP" Then
       wUtil = True
       TipIstr = "CP"
    ElseIf xIstr = "QRY" Then
       wUtil = True
       TipIstr = "QY"
    ElseIf xIstr = "SYNC" Then
       wUtil = True
       TipIstr = "SY"
    Else
       m_fun.WriteLog "Incapsulatore - Costruisci Operazione " & vbCrLf & "Istruzione non riconosciuta " & xIstr, "DR - Incapsulatore"
    End If
  
    'riapplico il calcolo del NomeRout per ogni Molteplicit� di istruzione
    If UBound(TabIstr.dbd) > 0 Then
      nomeroutine = m_fun.Genera_Nome_Routine(TabIstr.dbd(1).nome, xIstr, SwTp)
      'aggiornalistaDB (TabIstr.Dbd(1).nome)
    Else
      nomeroutine = m_fun.Genera_Nome_Routine("", xIstr, SwTp)
    End If
   
   'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
   If TipIstr <> "GU" And TipIstr <> "GN" And TipIstr <> "GP" Then QualAndUnqual = False
    
   combinazioni = True
   IstrIncaps = ""
   Dcombination = False
   
    While combinazioni
   
    'AC 02/11/06
    ReDim CombPLevel(UBound(TabIstr.BlkIstr))
    ' alberto, sulle istruzioni OPEN e CLSE dava errore per cui l'ho cambiata..
    If UBound(TabIstr.BlkIstr) > 0 Then
      ReDim CombPLevel_s(UBound(TabIstr.BlkIstr) - 1)
    End If
    IstrIncaps = ""
    If UBound(cloniIstruzione) > 0 Then
       varistr = Restituisci_varFunz(pIdOggetto, pRiga)
       combistr = "'" & xIstr & "'"
    End If
    wOpe = "<pgm>" & nomeroutine & "</pgm><istr>" & Left(Trim(xIstr) & "....", 4) & "</istr>"
    wOpe = wOpe & "<livelli>"
     
    For K = 1 To UBound(TabIstr.BlkIstr)   ' ciclo su livelli
      ComboPLevelAppo = ComboPLevelTemp
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' SQ 28-07-06
      ' OK: REPL CON SSA QUALIFICATA DA RETURN CODE "AJ"
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If TabIstr.BlkIstr(K).IdSegm = -1 Then
           wOpe = wOpe & "<level" & K & "><segm>" & "DYNAMIC" & "</segm>"
        Else
           wOpe = wOpe & "<level" & K & "><segm>" & TabIstr.BlkIstr(K).Segment & "</segm>"
           
           aggiornalistaSeg (TabIstr.BlkIstr(K).Segment)
        End If
      Else
        If (xIstr = "REPL" Or xIstr = "DLET") And K = UBound(TabIstr.BlkIstr) Then
          If TabIstr.BlkIstr(K).IdSegm = -1 Then
             wOpe = wOpe & "<level1><segm>" & "DYNAMIC" & "</segm>"
          Else
             wOpe = wOpe & "<level1><segm>" & TabIstr.BlkIstr(K).Segment & "</segm>"
             aggiornalistaSeg (TabIstr.BlkIstr(K).Segment)
          End If
        End If
      End If
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Then
        If TabIstr.BlkIstr(K).Record <> "" Then
           If Trim(TabIstr.BlkIstr(K).Segment) <> "" Then
               wOpe = wOpe & "<area>" & "AREA" & "</area>"
           End If
        End If
      End If
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If Trim(Replace(Replace(TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K)), "*", ""), "-", "")) <> "" Then
           If Trim(TabIstr.BlkIstr(K).Segment) <> "" Then
              wOpe = wOpe & "<ccode>" & Trim(Replace(Replace(TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K)), "*", ""), "-", "")) & "</ccode>"
           End If
        End If
      End If
        
      'AC 03/07/06
      'IstrIncaps deve tenere conto del controllo sul command code
      'If SSAName(9:Len(Search))
      ' lo metto prima degli operatori
      'ultimo
      'If k > 1 Then ifAnd = "AND" 'Else determinato dalla presenza o meno di istr multipla
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If NumToken(TabIstr.BlkIstr(K).Search) > 1 Then
          varop = TabIstr.BlkIstr(K).ssaName & "(9:" & Len(TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K))) & ")"
          combop = "'" & TokenIesimo(TabIstr.BlkIstr(K).Search, CombinazioniComCode(K)) & "'"
        End If
      End If
          '*********************************
      If QualAndUnqual And K = UBound(TabIstr.BlkIstr) Then
        wOpeS = wOpe   ' allineiamo  la wOpeS  alla wOpe che si differenziano solo per ultimo livello
        'varop_s = varop
        'combop_s = combop
        For ks = 1 To K - 1
          CombPLevel_s(ks) = CombPLevel(ks)
        Next
        ' allineiamo le stringhe
      End If
        
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' QUALIFICAZIONE:
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' AC se entrambe le versione wOpe � quella qualificata e wOpeS quella
      ' squalificata, altrimenti se unica sempre in wOpe
        
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or (xIstr = "ISRT" And K < UBound(TabIstr.BlkIstr)) Then
       If QualAndUnqual And K = UBound(TabIstr.BlkIstr) Then
        'parte SQUALIFICATA
        ' la wOpeS si ferma qui, wOpeS e wOpe differiscono solo per questo livello
         
        'parte QUALIFICATA
         wFlagBoolean = False
         For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
           If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
             If Len(TabIstr.BlkIstr(K).KeyDef(k1).OpLogic) Then
               wOpe = wOpe & "<key>" & TabIstr.BlkIstr(K).KeyDef(k1).key & "</key>"
               Operatore = TokenIesimo(TabIstr.BlkIstr(K).KeyDef(k1).Op, MatriceCombinazioni(K, k1))
               wOpe = wOpe & "<oper>" & Operatore & "</oper>"
               wOpe = wOpe & "<bool>" & transcode_booleano(TabIstr.BlkIstr(K).KeyDef(k1).OpLogic) & "</bool>"
               wFlagBoolean = True
             Else
               wOpe = wOpe & "<key>" & TabIstr.BlkIstr(K).KeyDef(k1).key & "</key>"
               Operatore = TokenIesimo(TabIstr.BlkIstr(K).KeyDef(k1).Op, MatriceCombinazioni(K, k1))
               wOpe = wOpe & "<oper>" & Operatore & "</oper>"
             End If
           'AC 03/07/06
           'ultimo
             If multiOp Then
               ' da riparametrizzare
               'If (wIdent + extraIndent + Len(ifAnd) + 38 + Len(TabIstr.BlkIstr(k).SSAName)) > 72 Then
               varop = TabIstr.BlkIstr(K).ssaName & "(" & Int(TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
               combop = "REV" & Operatore & "1" & vbCrLf _
               & "    " & Space(Len(TabIstr.BlkIstr(K).ssaName)) & "     OR REV" & Operatore & "2" & vbCrLf _
               & "    " & Space(Len(TabIstr.BlkIstr(K).ssaName)) & "     OR REV" & Operatore & "3"
            End If
          End If
        Next k1
        If Not varop = "" Then
          ComboPLevelAppo = Replace(ComboPLevelAppo, "<VAROP>", varop)
          ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBOP>", combop)
          If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or K > 1 Then 'AND -- alternativa 1
            ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 1))
          Else
            ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 2))
          End If
          If QualAndUnqual Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
          End If
          CombPLevel(K) = ComboPLevelAppo
        Else
          CombPLevel(K) = ComboPLevelAppo
        End If
      Else  ' o la versione squalificata o quella squalificata
          ' AC il campo parentesi e la trasformazione in stringa del booleano Qualificazione
        If TabIstr.BlkIstr(K).Parentesi <> "(" Then ' Or Trim(TabIstr.BlkIstr(k).SsaName) = "<none>" Then
           OnlySqual = True
        Else   ' Qualificata
          OnlyQual = True
          wFlagBoolean = False
          For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
            If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
              If Len(TabIstr.BlkIstr(K).KeyDef(k1).OpLogic) Then
                 wOpe = wOpe & "<key>" & TabIstr.BlkIstr(K).KeyDef(k1).key & "</key>"
                 Operatore = TokenIesimo(TabIstr.BlkIstr(K).KeyDef(k1).Op, MatriceCombinazioni(K, k1))
                 wOpe = wOpe & "<oper>" & Operatore & "</oper>"
                 wOpe = wOpe & "<bool>" & transcode_booleano(TabIstr.BlkIstr(K).KeyDef(k1).OpLogic) & "</bool>"
                 wFlagBoolean = True
              Else
                 wOpe = wOpe & "<key>" & TabIstr.BlkIstr(K).KeyDef(k1).key & "</key>"
                 Operatore = TokenIesimo(TabIstr.BlkIstr(K).KeyDef(k1).Op, MatriceCombinazioni(K, k1))
                 wOpe = wOpe & "<oper>" & Operatore & "</oper>"
              End If
              If multiOp Then
                 varop = TabIstr.BlkIstr(K).ssaName & "(" & Int(TabIstr.BlkIstr(K).KeyDef(k1).KeyStart) - 2 & ":2)"
                 combop = "REV" & Operatore & "1" & vbCrLf _
                 & "    " & Space(Len(TabIstr.BlkIstr(K).ssaName)) & "     OR REV" & Operatore & "2" & vbCrLf _
                 & "    " & Space(Len(TabIstr.BlkIstr(K).ssaName)) & "     OR REV" & Operatore & "3"
              End If
            End If
          Next k1
        End If '-- squal o qual
        If Not varop = "" Then
          ComboPLevelAppo = Replace(ComboPLevelAppo, "<VAROP>", varop)
          ComboPLevelAppo = ReplaceTagtoo(ComboPLevelAppo, "<COMBOP>", combop)
          If UBound(cloniIstruzione) > 0 Or QualAndUnqual Or K > 1 Then 'AND -- alternativa 1
            ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 1))
          Else
            ComboPLevelAppo = Replace(ComboPLevelAppo, FindCompleteTag(ComboPLevelAppo, "<A%"), GetIdentificator(FindCompleteTag(ComboPLevelAppo, "<A%"), 2))
          End If
          If QualAndUnqual Then
            ComboPLevelAppo = Replace(ComboPLevelAppo, "<I%IND-3>", "")
          End If
          CombPLevel(K) = ComboPLevelAppo
        Else
          CombPLevel(K) = ComboPLevelAppo
        End If
      End If   '-- una sola versione o due versioni
    End If
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
     wOpe = wOpe & "</level" & K & ">"
     If QualAndUnqual And K = UBound(TabIstr.BlkIstr) Then
       wOpeS = wOpeS & "</level" & K & ">"
     End If
    Else
     If (xIstr = "REPL" Or xIstr = "DLET") And K = UBound(TabIstr.BlkIstr) Then
       wOpe = wOpe & "</level1>"
       If QualAndUnqual And K = UBound(TabIstr.BlkIstr) Then
         wOpeS = wOpeS & "</level1>"
       End If
     End If
    End If
  Next K   ' FINE CICLO LIVELLI
     
  wOpe = wOpe & "</livelli>"
  
  'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
  If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
    If Trim(TabIstr.keyfeedback) <> "" Then
      wOpe = wOpe & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
    End If
  End If
  
  'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
  If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
    If Len(TabIstr.procSeq) Then
      wOpe = wOpe & "<procseq>" & TabIstr.procSeq & "</procseq>"
    End If
  End If

  If xIstr = "OPEN" Then
    If Len(TabIstr.procOpt) Then
      wOpe = wOpe & "<procopt>" & TabIstr.procOpt & "</procopt>"
    End If
  End If
     
'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
' ora � solo per il programma in esame
    pcbDyn = ""
    pcbDynInt = 0
    maxPcbDyn = 0
    wOpeSave = wOpe
    For K = 1 To UBound(OpeIstrRt)
       If wOpe = OpeIstrRt(K).Decodifica Then
          If OpeIstrRt(K).ordPcb = ordPcb Then
             'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
             'verifico se � la prima
             pcbDyn = Format(OpeIstrRt(K).pcbDyn, "000")
             pcbDynInt = OpeIstrRt(K).pcbDyn
             maxPcbDyn = 0
             Exit For
          Else
             If OpeIstrRt(K).pcbDyn > maxPcbDyn Then
             'trovata un'istruzione identica, ma con diverso pcb
                maxPcbDyn = OpeIstrRt(K).pcbDyn
             End If
          End If
       End If
    Next K
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
    If maxPcbDyn > 0 Then
       pcbDyn = Format(maxPcbDyn + 1, "000")
       pcbDynInt = maxPcbDyn + 1
    End If
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
     ' aggiunge il pcb dinamico
      If Len(pcbDyn) Then
         wOpe = wOpe & "<pcbseq>" & pcbDyn & "</pcbseq>"
      Else
      ' se � la prima istruzione di quel tipo
         wOpe = wOpe & "<pcbseq>" & "001" & "</pcbseq>"
         pcbDynInt = 1
      End If
    End If
    
    wDecod = wOpe
    Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpe & "'")
     
    'If xope = "" Then
    If tb.EOF Then
      Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
      'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
      If tb1.EOF Then
         maxId = 1
      Else
         maxId = tb1(0) + 1
      End If
      tb.AddNew
      tb!IdIstruzione = maxId
      tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
      wIstr = tb!Codifica
      tb!Decodifica = wOpe
      tb!ImsDB = True
      tb!ImsDC = False
      tb!Cics = SwTp
  'stefano: routine multiple
      nomeroutcompleto = Genera_Counter(nomeroutine)
      tb!NomeRout = nomeroutcompleto
      tb.Update
      tb.Close
   'pcb dinamico
      K = UBound(OpeIstrRt) + 1
      ReDim Preserve OpeIstrRt(K)
      OpeIstrRt(K).Decodifica = wOpeSave
      OpeIstrRt(K).Istruzione = wIstr
      OpeIstrRt(K).ordPcb = ordPcb
      OpeIstrRt(K).pcbDyn = pcbDynInt
    Else
      wIstr = tb!Codifica
      nomeroutcompleto = tb!NomeRout
      tb.Close
    End If
     
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     'SQ CPY-ISTR
     'Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
    Set tb = m_fun.Open_Recordset("SELECT * FROM PsDli_IstrCodifica WHERE idPgm=" & idpgm & " AND idoggetto=" & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstr & "'")
    If tb.EOF Then
       'SQ 16-11-06 - EMBEDDED
       'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & nomeroutcompleto & "')"
       'SQ CPY-ISTR
       'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeroutcompleto) & "')"
       m_fun.FnConnection.Execute "INSERT INTO PsDLI_IstrCodifica VALUES(" & idpgm & "," & pIdOggetto & "," & pRiga & ",'" & wIstr & "','" & IIf(EmbeddedRoutine, "<EMBEDDED>", nomeroutcompleto) & "')"
    End If
    tb.Close
    If QualAndUnqual Then
      wOpeS = wOpeS & "</livelli>"
      'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
      If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
        If Trim(TabIstr.keyfeedback) <> "" Then
          wOpeS = wOpeS & "<fdbkey>" & "FDBKEY" & "</fdbkey>"
        End If
'procseq: sarebbe meglio metterlo in tabistr, ma per ora facciamo cos� (procseq
' ci servir� anche nella generazione routine
        If UBound(TabIstr.psb) Then
          If Len(TabIstr.procSeq) Then
            wOpeS = wOpeS & "<procseq>" & TabIstr.procSeq & "</procseq>"
          End If
          If Len(TabIstr.procOpt) And xIstr = "OPEN" Then
            wOpeS = wOpeS & "<procopt>" & TabIstr.procOpt & "</procopt>"
          End If
        End If
     End If
'stefano: pcb dinamico: la OpeIstrRt prima era per tutta la mgdli_decodificaistr,
' ora � solo per il programma in esame
     pcbDyn = ""
     pcbDynInt = 0
     maxPcbDyn = 0
     wOpeSave = wOpeS
     For K = 1 To UBound(OpeIstrRt)
       If wOpeS = OpeIstrRt(K).Decodifica Then
         If OpeIstrRt(K).ordPcb = ordPcb Then
           'trovata un'istruzione identica, con lo stesso pcb nello stesso programma
           'verifico se � la prima
           pcbDyn = Format(OpeIstrRt(K).pcbDyn, "000")
           pcbDynInt = OpeIstrRt(K).pcbDyn
           maxPcbDyn = 0
           Exit For
         Else
           If OpeIstrRt(K).pcbDyn > maxPcbDyn Then
           'trovata un'istruzione identica, ma con diverso pcb
              maxPcbDyn = OpeIstrRt(K).pcbDyn
           End If
         End If
       End If
     Next K
     'verifica se bisogna inserire una nuova istruzione per il pcb dinamico
     If maxPcbDyn > 0 Then
        pcbDyn = Format(maxPcbDyn + 1, "000")
        pcbDynInt = maxPcbDyn + 1
     End If
     ' aggiunge il pcb dinamico
     'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
     If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Or xIstr = "ISRT" Then
      If Len(pcbDyn) Then
         wOpeS = wOpeS & "<pcbseq>" & pcbDyn & "</pcbseq>"
      Else
      ' se � la prima istruzione di quel tipo
         wOpeS = wOpeS & "<pcbseq>" & "001" & "</pcbseq>"
         pcbDynInt = 1
      End If
     End If
     
     wDecodS = wOpeS
     Set tb = m_fun.Open_Recordset("select * from mgdli_decodificaistr where decodifica = '" & wOpeS & "'")
     
     'If xope = "" Then
     If tb.EOF Then
         

       Set tb1 = m_fun.Open_Recordset("select iif(max(idistruzione) is null,0,max(idistruzione)) from mgdli_decodificaistr")
       'stefano: inutile, perch� non esce mai per EOF, ma comunque lasciamola
       If tb1.EOF Then
          maxId = 1
       Else
          maxId = tb1(0) + 1
       End If
       tb.AddNew
       tb!IdIstruzione = maxId
       tb!Codifica = Left(Trim(xIstr) & "....", 4) & Format(maxId, "000000")
       wIstrS = tb!Codifica
       tb!Decodifica = wOpeS
       tb!ImsDB = True
       tb!ImsDC = False
   'stefano: routine multiple
       nomeroutcompleto = Genera_Counter(nomeroutine)
       tb!NomeRout = nomeroutcompleto
       tb.Update
       tb.Close
    'pcb dinamico
       K = UBound(OpeIstrRt) + 1
       ReDim Preserve OpeIstrRt(K)
       OpeIstrRt(K).Decodifica = wOpeSave
       OpeIstrRt(K).Istruzione = wIstr
       OpeIstrRt(K).ordPcb = ordPcb
       OpeIstrRt(K).pcbDyn = pcbDynInt
     Else
       wIstrS = tb!Codifica
       nomeroutcompleto = tb!NomeRout
       tb.Close
     End If
     'controllo, da rivedere per ridurre gli accessi, probabilmente si pu� evitare!
     Set tb = m_fun.Open_Recordset("select * from psdli_istrcodifica where idoggetto = " & pIdOggetto & " and riga = " & pRiga & " and codifica = '" & wIstrS & "'")
     If tb.EOF Then
        m_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pRiga & ",'" & wIstrS & "','" & nomeroutcompleto & "')"
     End If
     tb.Close
    End If
    'stefano: sintesi clorofilliana (da rivedere il parser II per le combinazioni "strane")
    'BACO: per ora non viene gestita la molteplicit� sulla ISRT per i livelli qualificati
    ' ma in realt� non ha senso (di solito l'inserimento viene fatto qualificando un
    ' livello precedente per uguale, in modo da puntare un solo parent
    If TipIstr = "GU" Or TipIstr = "GN" Or TipIstr = "GP" Then
     combinazioni = AggiornaCombinazione_ETZ(maxLogicOp)
    Else
     combinazioni = False
    End If
     
     ' aggiornamento collection
     If Not StringToClear = "" Then
      IstrIncaps = Replace(IstrIncaps, StringToClear, "")
      IstrSIncaps = Replace(IstrSIncaps, StringToClear, "")
     End If
     
     If QualAndUnqual Then
       ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
       checkColl = True
       
       For j = 1 To wMoltIstr(3).Count
        
        If wMoltIstr(3).Item(j) = wIstr Then
          checkColl = False
          
          Exit For
        End If
       Next
       If checkColl Then
        wMoltIstr(1).Add IstrIncaps, wDecod
        wMoltIstr(3).Add wIstr, wDecod
        aggiornalistaCodOp (wIstr)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeroutcompleto
       End If
       
       ' 0 e 2  da wOpeS, indice ks dell'array e da IstrSIncaps
       checkColl = True
       checksqual = True
       For j = 1 To wMoltIstr(2).Count
'        If wMoltIstr(2).Item(j) = OpeIstrRt(ks).Istruzione Then
        If wMoltIstr(2).Item(j) = wIstrS Then
          checkColl = False
          checksqual = False
          Exit For
        End If
       Next
       If checkColl Then
        wMoltIstr(0).Add IstrSIncaps, wDecod
        wMoltIstr(2).Add wIstrS, wDecodS
        aggiornalistaCodOp (wIstrS)
        y = y + 1
        ReDim Preserve wNomeRout(y)
        wNomeRout(y) = nomeroutcompleto
       End If
     Else
         ' 1 e 3  da wOpe, indice k dell'array e da IstrIncaps
         checkColl = True
         For j = 1 To wMoltIstr(3).Count
'          If wMoltIstr(3).Item(j) = OpeIstrRt(k).Istruzione Then
          If wMoltIstr(3).Item(j) = wIstr Then
            checkColl = False
            Exit For
          End If
         Next
         If checkColl Then
          wMoltIstr(1).Add IstrIncaps, wDecod
          wMoltIstr(3).Add wIstr, wDecod
          aggiornalistaCodOp (wIstr)
          y = y + 1
          ReDim Preserve wNomeRout(y)
          wNomeRout(y) = nomeroutcompleto
         End If
       
     End If
     ' livello COMBQUAL -- costruisce elemento array combqual
'stefanopul
'''''''      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
'''''''      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\COMBQUAL"
'''''''      If QualAndUnqual Then
'''''''        MavsdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
'''''''        MavsdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
'''''''      Else
'''''''        ' gestisco indentazione move
'''''''        If multiOp Or UBound(cloniIstruzione) > 0 Then ' ne lascio solo una
'''''''          MavsdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
'''''''        End If
'''''''      End If
'''''''
'''''''      'if or end per l'istruzione e relativo end if di chiusura
'''''''      If QualAndUnqual Then
'''''''        MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
'''''''        MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
'''''''      Else
'''''''        MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 2)
'''''''        ' end if
'''''''        If multiOp Or UBound(cloniIstruzione) > 0 Then
'''''''          MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 2)
'''''''        Else
'''''''           MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
'''''''        End If
'''''''      End If
      
'''''''      MavsdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
'''''''      MavsdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
'''''''      MavsdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
'''''''      MavsdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstr
'''''''      MavsdM_GenRout.changeTag Rtb, "<CODICEOPERB>", Replace(wIstr, ".", "")
'''''''
'''''''      If EmbeddedRoutine Then
'''''''        MavsdM_GenRout.changeTag Rtb, "<embedded>", ""
'''''''      Else
'''''''        MavsdM_GenRout.changeTag Rtb, "<standard>", ""
'''''''      End If
      
''''''
''''''      For K = 1 To UBound(CombPLevel)
''''''        MavsdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel(K), True
''''''      Next
      
      
      
''''''      appocomb = Rtb.text
'''''''stefanopul
''''''      'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFQUAL"
''''''      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\IFQUAL"
''''''      MavsdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
''''''      ReDim Preserve CombQual(jk)
''''''      CombQual(jk) = Rtb.text
''''''
''''''      If QualAndUnqual And checksqual Then ' due parti, la seconda � squalificata
''''''        'stefanopul
''''''        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMBQUAL"
''''''        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\COMBQUAL"
''''''        MavsdM_GenRout.changeTag Rtb, "<I%IND-3>", "" ' cancello indentazione non necessaria
''''''        MavsdM_GenRout.changeTag Rtb, "<I2%IND-3>", "" ' cancello indentazione non necessaria
''''''        MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<A%"), GetIdentificator(FindCompleteTag(Rtb.text, "<A%"), 1)
''''''        MavsdM_GenRout.changeTag Rtb, FindCompleteTag(Rtb.text, "<B%"), GetIdentificator(FindCompleteTag(Rtb.text, "<B%"), 1)
''''''        MavsdM_GenRout.changeTag Rtb, "<VARISTR>", varistr, True
''''''        MavsdM_GenRout.changeTag Rtb, "<COMBISTR>", combistr, True
''''''        MavsdM_GenRout.changeTag Rtb, "<PROGRAMMA>", nomeroutcompleto
''''''        MavsdM_GenRout.changeTag Rtb, "<CODICEOPER>", wIstrS
''''''        wIstrsOld = wIstrS
''''''        For K = 1 To UBound(CombPLevel_s)
''''''          MavsdM_GenRout.changeTag Rtb, "<<COMBOPLEVEL(i)>>", CombPLevel_s(K), True
''''''        Next
''''''        appocomb = Rtb.text
''''''        'stefanopul
''''''        'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\IFSQUAL"
''''''        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\IFSQUAL"
''''''        MavsdM_GenRout.changeTag Rtb, "<COMBQUAL>", appocomb
''''''        ReDim Preserve CombSqual(UBound(CombSqual) + 1)
''''''        CombSqual(UBound(CombSqual)) = Rtb.text
''''''      ElseIf Not QualAndUnqual Then
''''''      End If
      jk = jk + 1
      '**********
   Wend
  Next i  'ciclo su molteplicit� istruzioni
  'sostituzioni nel file principale
  
  
   'stefanopul
   'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\MAINCOMB"
''''''   Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\MAINCOMB"
''''''   MavsdM_GenRout.changeTag Rtb, "<SSA-INSPECT>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).SSAName & "(9:" & Len(TokenIesimo(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Search, 1)) + 1 & ")"
''''''   For i = 1 To UBound(CombQual)
''''''     MavsdM_GenRout.changeTag Rtb, "<<IFQUAL(i)>>", CombQual(i)
''''''   Next
''''''   If QualAndUnqual Then
''''''    For i = 1 To UBound(CombSqual)
''''''      MavsdM_GenRout.changeTag Rtb, "<<IFSQUAL(i)>>", CombSqual(i)
''''''    Next
''''''    MavsdM_GenRout.changeTag Rtb, "<condsqual>", "" ' altrimenti istr corrispondenti eliminate
''''''   End If
''''''   appofinalString = Rtb.text
''''''
''''''   While InStr(appofinalString, vbCrLf) > 0
''''''     posendline = InStr(1, appofinalString, vbCrLf)
''''''     insertLine testo, Left(appofinalString, posendline - 1), indentkey
''''''     appofinalString = Right(appofinalString, Len(appofinalString) - posendline - 1)
''''''   Wend
''''''   If Not Trim(appofinalString) = "" Then
''''''     insertLine testo, appofinalString, indentkey
''''''   End If
''''''  Close #nFBms
  'insertLine testo, Rtb.text
  'Resume Next
  getIstruzione_ETZ = wIstr
End Function
Sub CostruisciCommonInitialize()
  If swCall Then
    wIdent = wIdent - 6
  End If
  'SQ
  'testo = vbCrLf & START_TAG & vbCrLf
  testo = vbCrLf
  'testo = testo & CreaSp(0) & vbCrLf
  insertLine testo, ""
  'testo = testo & CreaSp(wIdent) & "INITIALIZE LNKDB-COMMAND " & vbCrLf
  insertLine testo, "INITIALIZE LNKDB-COMMAND ", wIdent
  'testo = testo & START_TAG & vbCrLf
  insertLine testo, ""
  'testo = testo & CreaSp(wIdent) & "MOVE '" & Trim(NomeProg) & "' TO LNKDB-CALLER " & vbCrLf
  insertLine testo, "MOVE '" & Trim(NomeProg) & "' TO LNKDB-CALLER ", wIdent
End Sub
Sub CostruisciCommonInitializeTemplate(rst As Recordset)
 Dim Rtb  As RichTextBox, wPath As String, appofinalString As String, posendline As String
 Dim segLevel() As String, levelKey() As String, appolevelstring As String, K As Integer
 Dim k1 As Integer, i As Integer
 
  Set Rtb = MavsdF_Incapsulatore.RTBR
  wPath = m_fun.FnPathPrj
  'stefanopul
  'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMMONINIT"
  Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\COMMONINIT"
  MavsdM_GenRout.changeTag Rtb, "<NAMEPGM>", Trim(NomeProg)
  appofinalString = Rtb.text
  testo = vbCrLf
  insertLine testo, ""
  
  '************  embedded *********
  If EmbeddedRoutine Then
     MavsdM_GenRout.changeTag Rtb, "<embedded>", ""
    If Not TabIstr.PCB = "" Then
       MavsdM_GenRout.changeTag Rtb, "<PCB>", TabIstr.PCB
    End If
    If Not TabIstr.NumPCB = 0 Then
      MavsdM_GenRout.changeTag Rtb, "<NUMPCB>", TabIstr.NumPCB
    End If
   
  'Salvataggio parziale
    appofinalString = Rtb.text
   
    ReDim segLevel(UBound(TabIstr.BlkIstr))
  
    For K = 1 To UBound(TabIstr.BlkIstr)
      ' GSAM to Sequential: da adeguare per GSAM to RDBMS
      If TabIstr.isGsam Then
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\SEGMENTLV_GSAM"
      Else
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\SEGMENTLV"
      End If
      
      DoEvents
       If TabIstr.BlkIstr(K).Segment = "" Then
          m_fun.WriteLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!riga & " missing segment", "MavsdM_Incapsulatore"
       End If
       
       Dim h As Integer
       If UBound(TabIstr.psb) Then
          h = Restituisci_Livello_Segmento(TabIstr.psb(1).Id, TabIstr.NumPCB, TabIstr.BlkIstr(K).Segment, 0)
       Else
          'stefano: istruzioni senza psb, assegna comunque un default
          'h = -1
          If UBound(TabIstr.dbd) Then
             h = Restituisci_Livello_Segmento_dbd(TabIstr.dbd(1).Id, TabIstr.BlkIstr(K).Segment, 0)
          Else
             h = -1
          End If
       End If
       
       If h >= 0 Then
           MavsdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
       Else
           m_fun.WriteLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!riga & " missing level", "MavsdM_Incapsulatore"
           MavsdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
       End If
       MavsdM_GenRout.changeTag Rtb, "<LEVEL>", K
       
       appolevelstring = Rtb.text
       
       ReDim levelKey(UBound(TabIstr.BlkIstr(K).KeyDef))
  
       For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
         Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\LEVKEY"
         
         If TabIstr.BlkIstr(K).KeyDef(k1).KeyStart <> 0 Then
               MavsdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(K).ssaName
               MavsdM_GenRout.changeTag Rtb, "<KEYSTART>", TabIstr.BlkIstr(K).KeyDef(k1).KeyStart
               MavsdM_GenRout.changeTag Rtb, "<KEYLEN>", TabIstr.BlkIstr(K).KeyDef(k1).KeyLen
               MavsdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(K, "#0")
               MavsdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(k1, "#0")
         End If
         levelKey(k1) = Rtb.text
       Next k1
       
       ' riprendo da SEGMENTLV
       Rtb.text = appolevelstring
       
       For i = 1 To k1 - 1
        MavsdM_GenRout.changeTag Rtb, "<<LEVKEY(i)>>", levelKey(i)
       Next
       
       
       If Len(TabIstr.BlkIstr(K).Record) Then
          MavsdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(K).Record
          MavsdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(K, "#0")
          If TabIstr.isGsam Then
             MavsdM_GenRout.changeTag Rtb, "<RECLEN>", TabIstr.BlkIstr(K).AreaLen
          End If
       End If
       
       If Len(TabIstr.BlkIstr(K).ssaName) Then
          MavsdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(K).ssaName
          MavsdM_GenRout.changeTag Rtb, "<FLEVELSA>", Format(K, "#0")
       End If
       
       segLevel(K) = Rtb.text
       
    Next K
       
       'recuper maincall
       Rtb.text = appofinalString
       'stefanopul
       'Rtb.SaveFile wpath & "\input-prj\Template\INCAPS\logpar ", 1
       Rtb.SaveFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\logpar ", 1
       For K = 1 To UBound(TabIstr.BlkIstr)
        MavsdM_GenRout.changeTag Rtb, "<<SEGMENTLV(i)>>", segLevel(K)
       Next
       'stefanopul
       'Rtb.SaveFile wpath & "\input-prj\Template\INCAPS\logpar ", 1
       Rtb.SaveFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\logpar ", 1
       'SQ 6-10-06
       'MOVE PCB "VERO" nel nostro: (per l'ultimo segmento letto, per es...)
       If swCall Then
          If Len(TabIstr.PCB) Then
            MavsdM_GenRout.changeTag Rtb, "<PCBCALL>", TabIstr.PCB
          End If
       End If
       appofinalString = Rtb.text
  Else
       ' nel caso non embedded rendo disponibili dei tag al programmatore
       ' in questo modo inserendo i tag nel template p�o accedere ai
       ' campi della tabistr
        MavsdM_GenRout.changeTag Rtb, "<XPCB>", TabIstr.PCB
        MavsdM_GenRout.changeTag Rtb, "<XNUMPCB>", TabIstr.NumPCB
        appofinalString = Rtb.text
  End If
  
  While InStr(1, appofinalString, vbCrLf) > 0
    posendline = InStr(1, appofinalString, vbCrLf)
    insertLine testo, Left(appofinalString, posendline - 1), indentkey
    appofinalString = Right(appofinalString, Len(appofinalString) - posendline - 1)
  Wend

  'insertLine testo, ""
  If Not Trim(appofinalString) = "" Then
    insertLine testo, appofinalString, indentkey
  End If
End Sub

Sub CostruisciCommonInitializeTemplateC(rst As Recordset)
 Dim Rtb  As RichTextBox, wPath As String, appofinalString As String, posendline As String
 Dim segLevel() As String, levelKey() As String, appolevelstring As String, K As Integer
 Dim k1 As Integer, i As Integer
 
  Set Rtb = MavsdF_Incapsulatore.RTBR
  wPath = m_fun.FnPathPrj
  'stefanopul
  'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\COMMONINIT"
  Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\COMMONINIT"
  MavsdM_GenRout.changeTag Rtb, "<NAMEPGM>", Trim(NomeProg)
  appofinalString = Rtb.text
  testo = testo & vbCrLf
  insertLine testo, ""
  
  '************  embedded *********
  If EmbeddedRoutine Then
     MavsdM_GenRout.changeTag Rtb, "<embedded>", ""
    If Not TabIstr.PCB = "" Then
       MavsdM_GenRout.changeTag Rtb, "<PCB>", TabIstr.PCB
    End If
    If Not TabIstr.NumPCB = 0 Then
      MavsdM_GenRout.changeTag Rtb, "<NUMPCB>", TabIstr.NumPCB
    End If
   
  'Salvataggio parziale
    
    appofinalString = appofinalString & Rtb.text
   
    ReDim segLevel(UBound(TabIstr.BlkIstr))
  
    For K = 1 To UBound(TabIstr.BlkIstr)
      ' GSAM to Sequential: da adeguare per GSAM to RDBMS
      If TabIstr.isGsam Then
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\SEGMENTLV_GSAM"
      Else
        Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\SEGMENTLV"
      End If
      
      DoEvents
       If TabIstr.BlkIstr(K).Segment = "" Then
          m_fun.WriteLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!riga & " missing segment", "MavsdM_Incapsulatore"
       End If
       
       Dim h As Integer
       If UBound(TabIstr.psb) Then
          h = Restituisci_Livello_Segmento(TabIstr.psb(1).Id, TabIstr.NumPCB, TabIstr.BlkIstr(K).Segment, 0)
       Else
          'stefano: istruzioni senza psb, assegna comunque un default
          'h = -1
          If UBound(TabIstr.dbd) Then
             h = Restituisci_Livello_Segmento_dbd(TabIstr.dbd(1).Id, TabIstr.BlkIstr(K).Segment, 0)
          Else
             h = -1
          End If
       End If
       
       If h >= 0 Then
           MavsdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
       Else
           m_fun.WriteLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!riga & " missing level", "MavsdM_Incapsulatore"
           MavsdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
       End If
       MavsdM_GenRout.changeTag Rtb, "<LEVEL>", K
       
       appolevelstring = Rtb.text
       
       ReDim levelKey(UBound(TabIstr.BlkIstr(K).KeyDef))
  
       For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
         Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\LEVKEY"
         
         If TabIstr.BlkIstr(K).KeyDef(k1).KeyStart <> 0 Then
               MavsdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(K).ssaName
               MavsdM_GenRout.changeTag Rtb, "<KEYSTART>", TabIstr.BlkIstr(K).KeyDef(k1).KeyStart
               MavsdM_GenRout.changeTag Rtb, "<KEYLEN>", TabIstr.BlkIstr(K).KeyDef(k1).KeyLen
               MavsdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(K, "#0")
               MavsdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(k1, "#0")
         End If
         levelKey(k1) = Rtb.text
       Next k1
       
       ' riprendo da SEGMENTLV
       Rtb.text = appolevelstring
       
       For i = 1 To k1 - 1
        MavsdM_GenRout.changeTag Rtb, "<<LEVKEY(i)>>", levelKey(i)
       Next
       
       
       If Len(TabIstr.BlkIstr(K).Record) Then
          MavsdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(K).Record
          MavsdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(K, "#0")
          If TabIstr.isGsam Then
             MavsdM_GenRout.changeTag Rtb, "<RECLEN>", TabIstr.BlkIstr(K).AreaLen
          End If
       End If
       
       If Len(TabIstr.BlkIstr(K).ssaName) Then
          MavsdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(K).ssaName
          MavsdM_GenRout.changeTag Rtb, "<FLEVELSA>", Format(K, "#0")
       End If
       
       segLevel(K) = Rtb.text
       
    Next K
       
       'recuper maincall
       Rtb.text = appofinalString
       'stefanopul
       'Rtb.SaveFile wpath & "\input-prj\Template\INCAPS\logpar ", 1
       'Rtb.SaveFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\logpar ", 1
       For K = 1 To UBound(TabIstr.BlkIstr)
        MavsdM_GenRout.changeTag Rtb, "<<SEGMENTLV(i)>>", segLevel(K)
       Next
       'stefanopul
       'Rtb.SaveFile wpath & "\input-prj\Template\INCAPS\logpar ", 1
       Rtb.SaveFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\logpar ", 1
       'SQ 6-10-06
       'MOVE PCB "VERO" nel nostro: (per l'ultimo segmento letto, per es...)
       If swCall Then
          If Len(TabIstr.PCB) Then
            MavsdM_GenRout.changeTag Rtb, "<PCBCALL>", TabIstr.PCB
          End If
       End If
       appofinalString = Rtb.text
  Else
       ' nel caso non embedded rendo disponibili dei tag al programmatore
       ' in questo modo inserendo i tag nel template p�o accedere ai
       ' campi della tabistr
        MavsdM_GenRout.changeTag Rtb, "<XPCB>", TabIstr.PCB
        MavsdM_GenRout.changeTag Rtb, "<XNUMPCB>", TabIstr.NumPCB
        appofinalString = Rtb.text
  End If
  
  While InStr(1, appofinalString, vbCrLf) > 0
    posendline = InStr(1, appofinalString, vbCrLf)
    insertLine testo, Left(appofinalString, posendline - 1), indentkey
    appofinalString = Right(appofinalString, Len(appofinalString) - posendline - 1)
  Wend

  'insertLine testo, ""
  If Not Trim(appofinalString) = "" Then
    insertLine testo, appofinalString, indentkey
  End If
End Sub
Function Genera_Counter(nomeroutine)
  Dim rsCount As Recordset
  Dim rsRout As Recordset
  Dim K As Integer
  Dim y As Integer
  
  'SQ - 24-07-06
  'Errore impossibile da trovare! Non genera routine e incapsula male se non ritorno un Genera_Counter <> ""!
  'Genera_Counter = ""
  Genera_Counter = nomeroutine
  
  Set rsCount = m_fun.Open_Recordset("select * from bs_parametri where typekey = 'COUNTER'")
  If Not rsCount.EOF Then
  'verifica quanto sono le routine attuali
  'attacca solo se pu�
    If Len(nomeroutine) <= 6 Then
      While Genera_Counter = nomeroutine 'Or rsRout.EOF
        'cerca routine con counter = k
        Set rsRout = m_fun.Open_Recordset("select count(*) as totistr from mgdli_decodificaistr where nomerout = '" & nomeroutine & Format(K, "00") & "'")
        If rsRout!totIstr >= Int(rsCount!ParameterValue) Then
           K = K + 1
        Else
          Genera_Counter = nomeroutine & Format(K, "00")
        End If
      Wend
    End If
  End If
  rsCount.Close

End Function

'SP operatore
Sub CostruisciDecodifiche(pIdOggetto As Long, pIdRiga As Long)
  Dim pmDecIstr As String, pmCallRout As String
  Dim i As Integer, y As Integer
      
 
 ' ciclo su molteplicit� qui fouri
 ' wMoltIstr � funz delle molteplicit�
 
 If QualAndUnqual Then 'entrambe le versioni -> controllo su ultima ssa in posizione 9
     
      
      'insertLine testo, "MOVE ZERO TO REVCOUNT ", wIdent
      'insertLine testo, "INSPECT " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).SSAName & "(9:" & Len(TokenIesimo(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Search, 1)) + 1 & ")", wIdent
      'insertLine testo, " TALLYING REVCOUNT FOR ALL SPACES", wIdent
      
      '******************  inizio parte con ultimo livello qualificato (per gli altri livelli ho preso squal o squal in base a db)
      Dim flg As Boolean
      flg = False
      For i = 1 To wMoltIstr(3).Count
        ' indentazione 3
        Dim windentMul As Integer
        windentMul = 0
        'ultimo
        insertLine testo, "IF REVCOUNT = ZERO", wIdent
        windentMul = 3
        If wMoltIstr(1).Item(i) <> "" Then
           testo = testo & Replace(wMoltIstr(1).Item(i), " IF ", " AND ")
        End If
        
      
          y = y + 1
          nomeroutine = wNomeRout(y)
          pmCallRout = "'" & nomeroutine & "'"
'
          ' indentazione 6
        insertLine testo, "MOVE " & pmCallRout & " TO IMSRTNAME ", wIdent + windentMul
        If (wIdent + windentMul + 34 + Len(wMoltIstr(3).Item(i))) > 72 Then
        'ultimo
           insertLine testo, "MOVE '" & wMoltIstr(3).Item(i) & "' TO ", wIdent + windentMul
           insertLine testo, "     LNKDB-OPERATION", wIdent + windentMul
        Else
           insertLine testo, "MOVE '" & wMoltIstr(3).Item(i) & "' TO LNKDB-OPERATION", wIdent + windentMul
        End If
        
        ' indentazione 3 end if singola combinazione
        If wMoltIstr(1).Item(i) <> "" Then
           flg = True
           insertLine testo, "END-IF", wIdent
        End If
    'SP operatore
'        m_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pIdRiga & ",'" & wMoltIstr(3).Item(i) & "','" & Mid(pmCallRout, 2, Len(pmCallRout) - 2) & "')"
      Next
      '******************  fine parte con ultimo livello qualificato
      'ultimo
      'testo = testo & CreaSp(wIdent) & "ELSE" & vbCrLf
      If Not flg Then
         insertLine testo, "END-IF", wIdent
      End If
      flg = False
      '******************* inizio parte con ultimo livello squalificato
      For i = 1 To wMoltIstr(2).Count
        ' indentazione 4
        'ultimo
        insertLine testo, "IF REVCOUNT > ZERO", wIdent
        'testo = testo & CreaSp(wIdent) & "IF REVCOUNT = ZERO" & vbCrLf
        windentMul = 3
        If wMoltIstr(0).Item(i) <> "" Then
           testo = testo & Replace(wMoltIstr(0).Item(i), " IF ", " AND ")
        End If
        

          y = y + 1
          nomeroutine = wNomeRout(y)
          pmCallRout = "'" & nomeroutine & "'"

          ' indentazione 8
        insertLine testo, "MOVE " & pmCallRout & " TO IMSRTNAME ", wIdent + windentMul
        
        If (wIdent + windentMul + 35 + Len(wMoltIstr(3).Item(i))) > 72 Then
           insertLine testo, "MOVE '" & wMoltIstr(2).Item(i) & "' TO ", wIdent + windentMul
           insertLine testo, "     LNKDB-OPERATION ", wIdent + windentMul
        Else
           insertLine testo, "MOVE '" & wMoltIstr(2).Item(i) & "' TO LNKDB-OPERATION", wIdent + windentMul
        End If
        ' indentazione 4 end if singola combinazione
        If wMoltIstr(0).Item(i) <> "" Then
          flg = True
          insertLine testo, "END-IF", wIdent + windentMul
        End If
    'SP operatore
'         m_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pIdRiga & ",'" & wMoltIstr(2).Item(i) & "','" & Mid(pmCallRout, 2, Len(pmCallRout) - 2) & "')"
      Next
      '******************  fine parte con ultimo livello squalificato
      
      If Not flg Then
        insertLine testo, "END-IF", wIdent
      End If
     
     Else   ' considero livelli squalificati in base a flag
     
      For i = 1 To wMoltIstr(3).Count
        ' indentazione
        windentMul = 0
        If wMoltIstr(1).Item(i) <> "" Then
           windentMul = 3
           'stefano: da rivedere...
           'testo = testo & CreaSp(wIdent) & wMoltIstr(1).Item(i)
           testo = testo & wMoltIstr(1).Item(i)
        End If
        
          y = y + 1
          nomeroutine = wNomeRout(y)
          pmCallRout = "'" & nomeroutine & "'"
'        End If
        ' indentazione 4
       
        insertLine testo, "MOVE " & pmCallRout & " TO IMSRTNAME ", wIdent + windentMul
        If (wIdent + windentMul + 31 + Len(wMoltIstr(3).Item(i))) > 72 Then
          'testo = testo & CreaSp(wIdent + windentMul) & "MOVE '" & wMoltIstr(3).Item(i) & "' TO " & vbCrLf
          insertLine testo, "MOVE '" & wMoltIstr(3).Item(i) & "' TO ", wIdent + windentMul
          'testo = testo & CreaSp(wIdent + windentMul) & "     LNKDB-OPERATION " & vbCrLf
          insertLine testo, "     LNKDB-OPERATION ", wIdent + windentMul
        Else
          'testo = testo & CreaSp(wIdent + windentMul) & "MOVE '" & wMoltIstr(3).Item(i) & "' TO LNKDB-OPERATION" & vbCrLf
          insertLine testo, "MOVE '" & wMoltIstr(3).Item(i) & "' TO LNKDB-OPERATION", wIdent + windentMul
        End If
        ' indentazione  end if singola combinazione
        If Len(wMoltIstr(1).Item(i)) Then
          'testo = testo & CreaSp(wIdent) & "END-IF" & vbCrLf
          insertLine testo, "END-IF", wIdent
        End If
        'SP operatore
        'm_fun.FnConnection.Execute "Insert into PsDLI_IstrCodifica values (" & pIdOggetto & "," & pIdRiga & ",'" & wMoltIstr(3).Item(i) & "','" & Mid(pmCallRout, 2, Len(pmCallRout) - 2) & "')"
    Next
  End If
End Sub

Function CostruisciCall_CALL(pTesto As String, wIstruzione, typeIncaps As String, wIstrDli As String, rst As Recordset) As String
  Dim K As Integer, k1 As Integer, k2 As Integer
  Dim wstr As String, wStr1 As String, nPcb As String, testoCall As String
  Dim s() As String
  Dim wKeyName As String
  'Parametri incapsulatore
  Dim pmNomeDbd As String, pmCallRout As String, pmDecIstr As String
  
  DoEvents
     
  testoCall = testoCall & pTesto
  
  If Not TabIstr.PCB = "" Then
     'testoCall = testoCall & CreaSp(wIdent) & vbCrLf
     insertLine testoCall, "", wIdent
     If (wIdent + 30 + Len(TabIstr.PCB)) > 72 Then
        'testoCall = testoCall & CreaSp(wIdent) & "SET LNKDB-PTRPCB TO" & vbCrLf
        insertLine testoCall, "SET LNKDB-PTRPCB TO", wIdent
        'testoCall = testoCall & CreaSp(wIdent) & "    ADDRESS OF " & TabIstr.PCB & vbCrLf
        insertLine testoCall, "    ADDRESS OF " & TabIstr.PCB, wIdent
     Else
        'testoCall = testoCall & CreaSp(wIdent) & "SET LNKDB-PTRPCB TO ADDRESS OF " & TabIstr.PCB & vbCrLf
        insertLine testoCall, "SET LNKDB-PTRPCB TO ADDRESS OF " & TabIstr.PCB, wIdent
     End If
  End If
  If Not TabIstr.NumPCB = 0 Then
     'testoCall = testoCall & CreaSp(wIdent) & "MOVE " & TabIstr.NumPCB & " TO LNKDB-NUMPCB " & vbCrLf
     insertLine testoCall, "MOVE " & TabIstr.NumPCB & " TO LNKDB-NUMPCB ", wIdent
  End If
   
  For K = 1 To UBound(TabIstr.BlkIstr)
    DoEvents
        
    'testoCall = testoCall & CreaSp(wIdent) & vbCrLf
        
    ' stefano!!!!!! da sostituire salvando solo la variabile senza parentesi
    'stefano: pulizia: da rigestire per l'incapsulamento da EXEC DLI verso routine DLI
'        If TabIstr.BlkIstr(k).IdSegm = -1 Then
'            testoCall = testoCall & CreaSp(wIdent) & "MOVE " & Mid(TabIstr.BlkIstr(k).Segment, 2, Len(TabIstr.BlkIstr(k).Segment) - 2) & " TO LNKDB-SEGNAME(" & k & ")" & vbCrLf
'        ElseIf Trim(TabIstr.BlkIstr(k).Segment) <> "" Then
'            testoCall = testoCall & CreaSp(wIdent) & "MOVE '" & TabIstr.BlkIstr(k).Segment & "' TO LNKDB-SEGNAME(" & k & ")" & vbCrLf
'        End If
     If TabIstr.BlkIstr(K).Segment = "" Then
        m_fun.WriteLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!riga & " missing segment", "MavsdM_Incapsulatore"
     End If
     insertLine testoCall, "", wIdent
     
     Dim h As Integer
     If UBound(TabIstr.psb) Then
        h = Restituisci_Livello_Segmento(TabIstr.psb(1).Id, TabIstr.NumPCB, TabIstr.BlkIstr(K).Segment, 0)
     Else
        'stefano: istruzioni senza psb, assegna comunque un default
        'h = -1
        If UBound(TabIstr.dbd) Then
           h = Restituisci_Livello_Segmento_dbd(TabIstr.dbd(1).Id, TabIstr.BlkIstr(K).Segment, 0)
        Else
           h = -1
        End If
     End If
     
     If h >= 0 Then
         insertLine testoCall, "MOVE '" & Format(h + 1, "00") & "'  TO LNKDB-SEGLEVEL(" & K & ")", wIdent
     Else
         m_fun.WriteLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!riga & " missing level", "MavsdM_Incapsulatore"
         insertLine testoCall, "MOVE [LEVEL NOT FOUND] TO LNKDB-SEGLEVEL(" & K & ")", wIdent
     End If
     
  'stefano: pulizia: da rigestire per l'incapsulamento da EXEC DLI verso routine DLI
'        If TabIstr.BlkIstr(k).SegLen <> "" Then
'           testoCall = testoCall & CreaSp(wIdent) & "MOVE " & TabIstr.BlkIstr(k).SegLen & " TO LNKDB-AREALENGTH(" & k & ")" & vbCrLf
'        End If
     For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
      ' testoCall = testoCall & m_fun.LABEL_INCAPS & vbCrLf
       
  'stefano: pulizia: da rigestire per l'incapsulamento da EXEC DLI verso routine DLI
'          If TabIstr.BlkIstr(k).KeyDef(k1).key <> "" Then
'            testoCall = testoCall & CreaSp(wIdent) & "MOVE '" & TabIstr.BlkIstr(k).KeyDef(k1).key & "' TO LNKDB-KEYNAME(" & Format(k, "#0") & ", " & Format(k1, "#0") & ")" & vbCrLf
'          End If
       
  'stefano: pulizia: da rigestire per l'incapsulamento da EXEC DLI verso routine DLI
'          If TabIstr.BlkIstr(k).KeyDef(k1).KeyLen <> "" And TabIstr.BlkIstr(k).KeyDef(k1).KeyLen <> "0" Then
'             testoCall = testoCall & CreaSp(wIdent) & "MOVE " & TabIstr.BlkIstr(k).KeyDef(k1).KeyLen & " TO LNKDB-KEYLENGTH(" & Format(k, "#0") & ", " & Format(k1, "#0") & ")" & vbCrLf
'          End If
       
       If TabIstr.BlkIstr(K).KeyDef(k1).KeyStart <> 0 Then
          If (wIdent + 34 + Len(TabIstr.BlkIstr(K).ssaName) + Len(TabIstr.BlkIstr(K).KeyDef(k1).KeyStart)) > 72 Then
             insertLine testoCall, "MOVE " & TabIstr.BlkIstr(K).ssaName & "(" & TabIstr.BlkIstr(K).KeyDef(k1).KeyStart & ":" & TabIstr.BlkIstr(K).KeyDef(k1).KeyLen & ")", wIdent
             insertLine testoCall, "     TO LNKDB-KEYVALUE(" & Format(K, "#0") & ", " & Format(k1, "#0") & ")", wIdent
          Else
             insertLine testoCall, "MOVE " & TabIstr.BlkIstr(K).ssaName & "(" & TabIstr.BlkIstr(K).KeyDef(k1).KeyStart & ":" & TabIstr.BlkIstr(K).KeyDef(k1).KeyLen & ") TO LNKDB-KEYVALUE(" & Format(K, "#0") & ", " & Format(k1, "#0") & ")", wIdent
          End If
       End If
       
  'stefano: pulizia: da rigestire per l'incapsulamento da EXEC DLI verso routine DLI
'          If Len(TabIstr.BlkIstr(k).KeyDef(k1).Op) Then
'            testoCall = testoCall & CreaSp(wIdent) & "MOVE '" & TabIstr.BlkIstr(k).KeyDef(k1).Op & "' TO LNKDB-KEYOPER(" & Format(k, "#0") & ", " & Format(k1, "#0") & ")" & vbCrLf
'          End If
       
  'stefano: pulizia: da rigestire per l'incapsulamento da EXEC DLI verso routine DLI
'          If Len(TabIstr.BlkIstr(k).KeyDef(k1).OpLogic) Then
'            testoCall = testoCall & m_fun.LABEL_INCAPS & vbCrLf
'            testoCall = testoCall & CreaSp(wIdent) & "MOVE '" & transcode_booleano(TabIstr.BlkIstr(k).KeyDef(k1).OpLogic) & "' TO LNKDB-OPLOGIC(" & Format(k, "#0") & ", " & Format(k1, "#0") & ")" & vbCrLf
'          End If
       
     Next k1
     
     'testoCall = testoCall & CreaSp(wIdent) & vbCrLf
     
     If Len(TabIstr.BlkIstr(K).Record) Then
        insertLine testoCall, "MOVE " & TabIstr.BlkIstr(K).Record & " TO LNKDB-DATA-AREA(" & Format(K, "#0") & ")", wIdent
     End If
     
     If Len(TabIstr.BlkIstr(K).ssaName) Then
        insertLine testoCall, "MOVE " & TabIstr.BlkIstr(K).ssaName & " TO LNKDB-SSA-AREA(" & Format(K, "#0") & ")", wIdent
     End If
     
'    testoCall = testoCall & CreaSp(wIdent) & vbCrLf
  Next K
     
     'SQ 6-10-06
     'MOVE PCB "VERO" nel nostro: (per l'ultimo segmento letto, per es...)
     If swCall Then
        If Len(TabIstr.PCB) Then
          insertLine testoCall, "MOVE " & TabIstr.PCB & " TO LNKDB-DLZPCB", wIdent
        End If
     End If
     insertLine testoCall, "", wIdent
     If SwTp Then
        If (wIdent + 40) > 72 Then
           insertLine testoCall, "CALL IMSRTNAME USING", wIdent
           insertLine testoCall, "     DFHEIBLK LNKDB-AREA", wIdent
        Else
           insertLine testoCall, "CALL IMSRTNAME USING DFHEIBLK LNKDB-AREA", wIdent
        End If
        insertLine testoCall, "", wIdent
     Else
        If (wIdent + 31) > 72 Then
           insertLine testoCall, "CALL IMSRTNAME USING", wIdent
           insertLine testoCall, "     LNKDB-AREA", wIdent
        Else
           insertLine testoCall, "CALL IMSRTNAME USING LNKDB-AREA", wIdent
        End If
        testoCall = testoCall & CreaSp(wIdent) & vbCrLf
     End If
     
     For K = 1 To UBound(TabIstr.BlkIstr)
     '<NONE>
        If Len(TabIstr.BlkIstr(K).Record) Then
            insertLine testoCall, "MOVE LNKDB-DATA-AREA(" & Format(K, "#0") & ") TO " & TabIstr.BlkIstr(K).Record, wIdent
        End If
     Next K
     
     'testoCall = testoCall & CreaSp(wIdent) & vbCrLf
     
     If TabIstr.keyfeedback <> "" Then
        insertLine testoCall, "MOVE LNKDB-FDBKEY TO " & Trim(TabIstr.keyfeedback), wIdent
     End If
     
     If swCall Then
        If Len(TabIstr.PCB) Then
           insertLine testoCall, "MOVE LNKDB-DLZPCB TO " & TabIstr.PCB, wIdent
        End If
     Else
        insertLine testoCall, "MOVE LNKDB-DIBSTAT TO DIBSTAT", wIdent
     End If
     
     CostruisciCall_CALL = testoCall
     s = Split(CostruisciCall_CALL, vbCrLf)
     
     AddRighe = AddRighe + UBound(s)
End Function
Function CostruisciCall_CALLTemplate(pTesto As String, wIstruzione, typeIncaps As String, wIstrDli As String, rst As Recordset, RTBErr As RichTextBox) As String
  Dim K As Integer, k1 As Integer, k2 As Integer, i As Integer
  Dim wstr As String, wStr1 As String, nPcb As String, testoCall As String
  Dim s() As String
  Dim wKeyName As String
  'Parametri incapsulatore
  Dim pmNomeDbd As String, pmCallRout As String, pmDecIstr As String
  Dim Rtb  As RichTextBox, wPath As String, appofinalString As String, posendline As String
  Dim segLevel() As String, levelKey() As String, appolevelstring As String
  
  testoCall = testoCall & pTesto
  
  Set Rtb = MavsdF_Incapsulatore.RTBR
  
  wPath = m_fun.FnPathPrj & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\MAINCALL" & IIf(EmbeddedRoutine, "_EMBEDDED", "")
  'SQ - 16-11-06: EMBEDDED
  'Gestire TEMPLATE NON ESISTENTI!
  If Len(Dir(wPath)) Then
    Rtb.LoadFile wPath
  Else
    RTBErr.text = RTBErr.text & "* TEMPLATE NOT FOUND: " & wPath & vbCrLf
  End If
  
  wPath = m_fun.FnPathPrj
  DoEvents
'GSAM fa un giro particolare
  If TabIstr.isGsam Then
  ' purtroppo va impostata alla OPEN, mentre la psdli_istrdett_liv non c'� per la open
  ' bisogna fare la seguente forzatura
  ' E' una grossa forzatura, ma evita di cambiare troppe cose
     Dim tb1 As Recordset
     Set tb1 = m_fun.Open_Recordset("select dataarea from psdli_istrdett_liv as a, psdli_istruzioni as b where a.idoggetto = b.idoggetto and a.riga = b.riga and b.idoggetto = " & rst!idOggetto & " and numpcb = '" & TabIstr.PCB & "'")
     If Not tb1.EOF Then
        If Len(tb1!dataarea) Then
           MavsdM_GenRout.changeTag Rtb, "<RECLENGSAM>", tb1!dataarea
        End If
     End If
     tb1.Close
  End If
     
  If Not EmbeddedRoutine Then
  '***** per embedded spostato su commoninit
  
  If Not TabIstr.PCB = "" Then
       MavsdM_GenRout.changeTag Rtb, "<PCB>", TabIstr.PCB
  End If
  If Not TabIstr.NumPCB = 0 Then
     MavsdM_GenRout.changeTag Rtb, "<NUMPCB>", TabIstr.NumPCB
  End If
   
  'Salvataggio parziale
  appofinalString = Rtb.text
   
  ReDim segLevel(UBound(TabIstr.BlkIstr))
  
  For K = 1 To UBound(TabIstr.BlkIstr)
    Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\SEGMENTLV"
    
    DoEvents
     If TabIstr.BlkIstr(K).Segment = "" Then
        m_fun.WriteLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!riga & " missing segment", "MavsdM_Incapsulatore"
     End If
     
     Dim h As Integer
     If UBound(TabIstr.psb) Then
        h = Restituisci_Livello_Segmento(TabIstr.psb(1).Id, TabIstr.NumPCB, TabIstr.BlkIstr(K).Segment, 0)
     Else
        'stefano: istruzioni senza psb, assegna comunque un default
        'h = -1
        If UBound(TabIstr.dbd) Then
           'MG
           'h = Restituisci_Livello_Segmento_dbd(TabIstr.dbd(1).Id, TabIstr.BlkIstr(K).Segment, 0)
           segLev = 0
           getSegLevel (CLng(TabIstr.BlkIstr(K).IdSegm))
           h = segLev - 1
        Else
           h = -1
        End If
     End If
     
     If h >= 0 Then
         MavsdM_GenRout.changeTag Rtb, "<FLEVEL>", Format(h + 1, "00")
     Else
         m_fun.WriteLog "Incaps: Obj(" & rst!idOggetto & ") Row( " & rst!riga & " missing level", "MavsdM_Incapsulatore"
         MavsdM_GenRout.changeTag Rtb, "<FLEVEL>", "[LEVEL NOT FOUND]"
     End If
     MavsdM_GenRout.changeTag Rtb, "<LEVEL>", K
     
     appolevelstring = Rtb.text
     
     ReDim levelKey(UBound(TabIstr.BlkIstr(K).KeyDef))

     For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
'stefanopul
       'Rtb.LoadFile wpath & "\input-prj\Template\INCAPS\LEVKEY"
       Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\LEVKEY"
       
       If TabIstr.BlkIstr(K).KeyDef(k1).KeyStart <> 0 Then
             MavsdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(K).ssaName
             MavsdM_GenRout.changeTag Rtb, "<KEYSTART>", TabIstr.BlkIstr(K).KeyDef(k1).KeyStart
             MavsdM_GenRout.changeTag Rtb, "<KEYLEN>", TabIstr.BlkIstr(K).KeyDef(k1).KeyLen
             MavsdM_GenRout.changeTag Rtb, "<IDXLEVEL>", Format(K, "#0")
             MavsdM_GenRout.changeTag Rtb, "<IDXKEY>", Format(k1, "#0")
       End If
       levelKey(k1) = Rtb.text
     Next k1
     
     ' riprendo da SEGMENTLV
     Rtb.text = appolevelstring
     
     For i = 1 To k1 - 1
      MavsdM_GenRout.changeTag Rtb, "<<LEVKEY(i)>>", levelKey(i)
     Next
     
     
     If Len(TabIstr.BlkIstr(K).Record) Then
        MavsdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(K).Record
        MavsdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(K, "#0")
     End If
     
     If Len(TabIstr.BlkIstr(K).ssaName) Then
        MavsdM_GenRout.changeTag Rtb, "<SSANAME>", TabIstr.BlkIstr(K).ssaName
        MavsdM_GenRout.changeTag Rtb, "<FLEVELSA>", Format(K, "#0")
     End If
     
     segLevel(K) = Rtb.text
     
  Next K
     
     'recuper maincall
     Rtb.text = appofinalString
     For K = 1 To UBound(TabIstr.BlkIstr)
      MavsdM_GenRout.changeTag Rtb, "<<SEGMENTLV(i)>>", segLevel(K)
     Next
     
     'SQ 6-10-06
     'MOVE PCB "VERO" nel nostro: (per l'ultimo segmento letto, per es...)
     If swCall Then
        If Len(TabIstr.PCB) Then
          MavsdM_GenRout.changeTag Rtb, "<PCBCALL>", TabIstr.PCB
        End If
     End If
     
     
     '****** fine parte embedded spostata su COMMONINIT
     End If
     
     ReDim segLevel(UBound(TabIstr.BlkIstr))
     
     'insertLine testoCall, "", wIdent
     If SwTp Then
           MavsdM_GenRout.changeTag Rtb, "<SWTPAREA>", "DFHEIBLK LNKDB-AREA"
        'insertLine testoCall, "", wIdent
     Else
           MavsdM_GenRout.changeTag Rtb, "<SWTPAREA>", "LNKDB-AREA"
        'testoCall = testoCall & CreaSp(wIdent) & vbCrLf
     End If
     
     'Salvataggio parziale
     appofinalString = Rtb.text
     
     For K = 1 To UBound(TabIstr.BlkIstr)
      Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\SEGMENTLVREV"
          
     '<NONE>
        If Len(TabIstr.BlkIstr(K).Record) Then
            'insertLine testoCall, "MOVE LNKDB-DATA-AREA(" & Format(k, "#0") & ") TO " & TabIstr.BlkIstr(k).Record, wIdent
            MavsdM_GenRout.changeTag Rtb, "<FLEVELDA>", Format(K, "#0")
            MavsdM_GenRout.changeTag Rtb, "<RECORD>", TabIstr.BlkIstr(K).Record
            If TabIstr.isGsam Then
               MavsdM_GenRout.changeTag Rtb, "<RECLEN>", TabIstr.BlkIstr(K).AreaLen
            End If
        End If
        segLevel(K) = Rtb.text
     Next K
     Rtb.text = appofinalString
     For K = 1 To UBound(TabIstr.BlkIstr)
      MavsdM_GenRout.changeTag Rtb, "<<SEGMENTLVREV(i)>>", segLevel(K)
     Next
     'testoCall = testoCall & CreaSp(wIdent) & vbCrLf
     
     If TabIstr.keyfeedback <> "" Then
        MavsdM_GenRout.changeTag Rtb, "<KEYFDBCK>", Trim(TabIstr.keyfeedback)
     End If
     
     If swCall Then
        If Len(TabIstr.PCB) Then
           MavsdM_GenRout.changeTag Rtb, "<DLZPCB>", TabIstr.PCB
        End If
     Else
        'se non si pulisce il tag <conditioned> la riga sar� cancellata
        MavsdM_GenRout.changeTag Rtb, "<Conditioned1>", ""
     End If
     ' tag a disposizione in particolare per PLI)
     MavsdM_GenRout.changeTag Rtb, "<XPROGRAMMA>", nomeroutine
     MavsdM_GenRout.changeTag Rtb, "<XREDEFPCB>", TabIstr.PCBRed
     
'stefanopul
     'Rtb.SaveFile wpath & "\input-prj\Template\INCAPS\LOGCALL"
     'Rtb.SaveFile wPath & "\input-prj\Template\imsdb\esternalizzazione\INCAPS\LOGCALL"
     appofinalString = Rtb.text
    ' sostituzione
    
    s = Split(appofinalString, vbCrLf)
    For j = 0 To UBound(s)
      insertLine testoCall, s(j), indentkey
    Next
    
'    While InStr(1, appofinalString, vbCrLf) > 0
'      posendline = InStr(1, appofinalString, vbCrLf)
'      insertLine testo, Left(appofinalString, posendline - 1)
'      appofinalString = Right(appofinalString, Len(appofinalString) - posendline - 1)
'    Wend
'    If Not Trim(appofinalString) = "" Then
'      insertLine testo, appofinalString
'    End If
     CostruisciCall_CALLTemplate = testoCall
     s = Split(CostruisciCall_CALLTemplate, vbCrLf)
     
     AddRighe = AddRighe + UBound(s)
End Function
Public Function FindCompleteTag(target As String, Tag As String) As String
Dim pos As Integer, char As String
    FindCompleteTag = Tag
    pos = InStr(1, target, Tag)
    pos = pos + Len(Tag)
    Do While posizione > 0
      FindCompleteTag = FindCompleteTag & Mid(target, pos, 1)
      If Mid(target, pos, 1) = ">" Then
        Exit Do
      End If
    pos = pos + 1
    Loop
End Function
Public Function Transcode_Istruction_DLI(wIstr As String) As String
    Dim xwIstr As String

    Select Case Left(wIstr, 4)
        Case "GU..", "GHU."
            xwIstr = "GU"
        Case "GN..", "GHN."
          xwIstr = "GN"
        Case "GNP.", "GHNP"
          xwIstr = "GP"
        Case "ISRT", "DLET", "REPL"
          xwIstr = "UP"
        Case "PCB.", "SCH.", "TERM", "CHKP", "SYNC", "QRY.", "QUERY"
          xwIstr = Left(wIstr, 4)
        Case Else
          xwIstr = "UK"
          m_fun.WriteLog "MavsdM_Incapsulatore.Transcode_Istruction_DLI - Function - Istruzione non riconosciuta : " & wIstr, "DR - GenRout"
    End Select

    Transcode_Istruction_DLI = xwIstr
End Function
Public Function getTotalIndent(line As String) As Integer
  Dim posind As Integer, posminor As Integer, posmaior As Integer
  Dim Tag As String
  While InStr(1, line, "%IND") > 0
    posind = InStr(1, line, "%IND")
    posminor = posind - 1
    Do While posind > 0 And posminor > 1
      If Mid(line, posminor, 1) = "<" Then
        Exit Do
      End If
      posminor = posminor - 1
    Loop
    posmaior = posind + 1
    Do While posind > 0 And Not posmaior > Len(line)
      If Mid(line, posmaior, 1) = ">" Then
        Exit Do
      End If
      posmaior = posmaior + 1
    Loop
    Tag = Mid(line, posminor, posmaior - posminor + 1)
    getTotalIndent = getTotalIndent + GetIndent(Tag)
    line = Replace(line, Tag, "")
   Wend
End Function

Public Function getIndentAtEnd(ByVal line As String) As String
Dim lenline As Integer, Tag As String
lenline = Len(line)
While Mid(line, Len(line), 1) = ">"
  lenline = lenline - 1
  Do While lenline
    If Mid(line, lenline, 1) = "<" Then
      Tag = Mid(line, lenline, Len(line) - lenline + 1)
      Exit Do
    End If
    lenline = lenline - 1
  Loop
  If InStr(1, Tag, "IND") > 0 Then
    getIndentAtEnd = Tag & getIndentAtEnd
  End If
  line = Left(line, Len(line) - Len(Tag))
Wend
End Function
Public Function ReplaceTagtoo(inputstr As String, target As String, subst As String) As String
Dim tagind As String, s() As String, i As Integer
  tagind = getIndentAtEnd(inputstr)
  inputstr = Replace(inputstr, target, subst)
  s = Split(inputstr, vbCrLf)
  For i = 0 To UBound(s) - 1
    s(i) = s(i) & tagind
    ReplaceTagtoo = ReplaceTagtoo & s(i) & vbCrLf
  Next
  ReplaceTagtoo = ReplaceTagtoo & s(UBound(s))
End Function

Public Function GetIdentificator(Tag As String, pos As Integer) As String
  Dim posperc As Integer, posslash As Integer, posend As Integer
  posperc = InStr(Tag, "%")
  posslash = InStr(Tag, "/")
  posend = Len(Tag)
  If pos = 1 Then
    GetIdentificator = Mid(Tag, posperc + 1, posslash - posperc - 1)
  Else
    GetIdentificator = Mid(Tag, posslash + 1, posend - posslash - 1)
  End If
End Function
Public Function GetIndent(Tag As String) As Integer
  Dim moltp As Integer, posind As Integer, posclose As Integer
  posind = InStr(Tag, "IND")
  If Mid(Tag, posind + 1, 1) = "+" Then
    moltp = 1
  Else
    moltp = -1
  End If
  GetIndent = moltp * CInt(Mid(Tag, posind + 4, Len(Tag) - posind - 4))
End Function
Public Function TROVA_RIGA(RTBox As RichTextBox, Chiave As String, riga As Long, Optional ByVal StartFind As Long = 0) As Boolean
   Dim FoundPos As Long
   Dim FoundLine As Long
   Dim flag As String
   Dim testo As String

   FoundPos = StartFind
   flag = True  'SQ: facciamo davvero?!

   While flag
     FoundPos = RTBox.find(Trim(Chiave), FoundPos + 1)
'     RTBox.SetFocus
     If FoundPos = -1 Then
        flag = False
     End If

     If FoundPos <> -1 Then

       FoundLine = RTBox.GetLineFromChar(FoundPos) + 1

       If FoundLine = riga Then
            TROVA_RIGA = True
            Exit Function
       End If

       If FoundLine > riga Then
          flag = False
       End If
     End If
   Wend

   FoundPos = 0
   flag = True
   
   'stefano: ma perch�???????????????????????????????????????????
   If Chiave = " EXEC " Then
     Chiave = "CBLTDLI"
   Else
     Chiave = " EXEC "
   End If

   While flag
     FoundPos = RTBox.find(Trim(Chiave), FoundPos + 1)

     If FoundPos = -1 Then
        TROVA_RIGA = False
        Exit Function
     End If

     If FoundPos <> -1 Then

       FoundLine = RTBox.GetLineFromChar(FoundPos) + 1

       If FoundLine = riga Then
            TROVA_RIGA = True
            Exit Function
       End If

       If FoundLine > riga Then
          TROVA_RIGA = False
          Exit Function
       End If
     End If
   Wend
End Function


Function CarNumKey(Ind) As Integer
Dim NumKey As Integer
Dim k1 As Integer
Dim k3 As Integer

NumKey = 0

k3 = UBound(TabIstr.BlkIstr(Ind).KeyDef)

For k1 = 1 To k3
  If TabIstr.BlkIstr(Ind).KeyDef(k1).key = TabIstr.BlkIstr(Ind).KeyDef(k3).key Then
     NumKey = NumKey + 1
  End If
Next k1

CarNumKey = NumKey

End Function

Public Function Open_IstrDett_Liv(IdOgg As Long, riga As Long, Liv As Long) As Recordset
  Open_IstrDett_Liv.Open "Select * From PsDli_IstrDett_Key Where " & _
                         "IdOggetto = " & IdOgg & " And Riga = " & riga & _
                         " And Livello = " & Liv, VSAM2Rdbms.drConnection, adOpenStatic, adLockOptimistic, adCmdText
End Function

Function ControllaUIB(RTBModifica As RichTextBox, typeIncaps As String) As Boolean
  Dim pPos As Variant
  Dim pStart As Variant
  Dim pEnd As Variant
  Dim pInit As Variant
  Dim PFine As Variant
  
  ControllaUIB = True
  
  If typeIncaps <> "DLI" Then
    pEnd = Len(RTBModifica.text)
    pPos = 1
    While pPos > 0
      pPos = RTBModifica.find(" ADDRESS ", pPos + 1, pEnd)
      If pPos > 0 Then
        PFine = RTBModifica.find(vbCrLf, pPos + 1, pEnd)
        For pInit = pPos To pPos - 80 Step -1
          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then
            Exit For
          End If
        Next pInit
        pPos = RTBModifica.find(" SET ", pInit, PFine)
        If pPos > 0 Then
           pPos = RTBModifica.find(" OF ", pInit, PFine)
           If pPos > 0 Then
             pPos = RTBModifica.find(" PCB", pInit, PFine)
             If pPos > 0 Then
                RTBModifica.SelStart = pInit + 7
                RTBModifica.SelLength = 1
                RTBModifica.SelText = "*"
             Else
               pPos = RTBModifica.find(" BPCB", pInit, PFine)
               If pPos > 0 Then
                  RTBModifica.SelStart = pInit + 7
                  RTBModifica.SelLength = 1
                  RTBModifica.SelText = "*"
               End If
             End If
           End If
        End If
        pPos = PFine + 1
      End If
    Wend
End If

If typeIncaps <> "DLI" Then
    pPos = 1
    While pPos > 0
      pPos = RTBModifica.find(" UIBPCBAL ", pPos + 1, pEnd)
      If pPos > 0 Then
        PFine = RTBModifica.find(vbCrLf, pPos + 1, pEnd)
        For pInit = pPos To pPos - 80 Step -1
          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then
             Exit For
          End If
        Next pInit
        pPos = RTBModifica.find(" MOVE ", pInit, PFine)
        If pPos > 0 Then
           RTBModifica.SelStart = pInit + 7
           RTBModifica.SelLength = 1
           RTBModifica.SelText = "*"
        End If
        pPos = PFine + 1
      End If
    Wend
End If

If typeIncaps <> "DLI" Then
    pPos = 1
    While pPos > 0
      pPos = RTBModifica.find(" BPCBPTR", pPos + 1, pEnd)
      If pPos > 0 Then
        PFine = RTBModifica.find(vbCrLf, pPos + 1, pEnd)
        For pInit = pPos To pPos - 80 Step -1
          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then
             Exit For
          End If
        Next pInit
        pPos = RTBModifica.find(" MOVE ", pInit, PFine)
        If pPos > 0 Then
           RTBModifica.SelStart = pInit + 7
           RTBModifica.SelLength = 1
           RTBModifica.SelText = "*"
        End If
        pPos = PFine + 1
      End If
    Wend
End If

If typeIncaps <> "DLI" Then
    pPos = 1
    While pPos > 0
      pPos = RTBModifica.find(" BPCB1PTR", pPos + 1, pEnd)
      If pPos > 0 Then
        PFine = RTBModifica.find(vbCrLf, pPos + 1, pEnd)
        For pInit = pPos To pPos - 80 Step -1
          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then
             Exit For
          End If
        Next pInit
        pPos = RTBModifica.find(" MOVE ", pInit, PFine)
        If pPos > 0 Then
           RTBModifica.SelStart = pInit + 7
           RTBModifica.SelLength = 1
           RTBModifica.SelText = "*"
        End If
        pPos = PFine + 1
      End If
    Wend
End If

If typeIncaps <> "DLI" Then
    pPos = 1
    While pPos > 0
      pPos = RTBModifica.find(" BPCB2PTR", pPos + 1, pEnd)
      If pPos > 0 Then
        PFine = RTBModifica.find(vbCrLf, pPos + 1, pEnd)
        For pInit = pPos To pPos - 80 Step -1
          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then
             Exit For
          End If
        Next pInit
        pPos = RTBModifica.find(" MOVE ", pInit, PFine)
        If pPos > 0 Then
           RTBModifica.SelStart = pInit + 7
           RTBModifica.SelLength = 1
           RTBModifica.SelText = "*"
        End If
        pPos = PFine + 1
      End If
    Wend
End If

If typeIncaps <> "DLI" Then
    pPos = 1
    While pPos > 0
      pPos = RTBModifica.find(" FCNOTOPEN", pPos + 1, pEnd)
      If pPos > 0 Then
        PFine = RTBModifica.find(vbCrLf, pPos + 1, pEnd)
        For pInit = pPos To pPos - 80 Step -1
          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then
             Exit For
          End If
        Next pInit
        pPos = RTBModifica.find(".", pInit, PFine)
        While pPos < 1
           PFine = RTBModifica.find(vbCrLf, PFine + 2, pEnd)
           pPos = RTBModifica.find(".", pInit, PFine)
        Wend
        pPos = RTBModifica.find(" IF ", pInit, PFine)
        If pPos > 0 Then
           PFine = PFine - 2
           pPos = pInit - 4
           pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
           While pPos > 0
             RTBModifica.SelStart = pPos + 8
             RTBModifica.SelLength = 1
             RTBModifica.SelText = "*"
             pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
           Wend
        End If
        pPos = PFine + 1
      End If
    Wend
End If

If typeIncaps <> "DLI" Then
    pPos = 1
    While pPos > 0
      pPos = RTBModifica.find(" FCINVREQ", pPos + 1, pEnd)
      If pPos > 0 Then
        PFine = RTBModifica.find(vbCrLf, pPos + 1, pEnd)
        For pInit = pPos To pPos - 80 Step -1
          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then
             Exit For
          End If
        Next pInit
        pPos = RTBModifica.find(".", pInit, PFine)
        While pPos < 1
           PFine = RTBModifica.find(vbCrLf, PFine + 2, pEnd)
           pPos = RTBModifica.find(".", pInit, PFine)
        Wend
        pPos = RTBModifica.find(" IF ", pInit, PFine)
        If pPos > 0 Then
           PFine = PFine - 2
           pPos = pInit - 4
           pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
           While pPos > 0
             RTBModifica.SelStart = pPos + 8
             RTBModifica.SelLength = 1
             RTBModifica.SelText = "*"
             pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
           Wend
        End If
        pPos = PFine + 1
      End If
    Wend
End If

If typeIncaps <> "DLI" Then
    pPos = 1
    While pPos > 0
      pPos = RTBModifica.find(" FCINVPCB", pPos + 1, pEnd)
      If pPos > 0 Then
        PFine = RTBModifica.find(vbCrLf, pPos + 1, pEnd)
        For pInit = pPos To pPos - 80 Step -1
          If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then
             Exit For
          End If
        Next pInit
        pPos = RTBModifica.find(".", pInit, PFine)
        While pPos < 1
           PFine = RTBModifica.find(vbCrLf, PFine + 2, pEnd)
           pPos = RTBModifica.find(".", pInit, PFine)
        Wend
        pPos = RTBModifica.find(" IF ", pInit, PFine)
        If pPos > 0 Then
           PFine = PFine - 2
           pPos = pInit - 4
           pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
           While pPos > 0
             RTBModifica.SelStart = pPos + 8
             RTBModifica.SelLength = 1
             RTBModifica.SelText = "*"
             pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
           Wend
        End If
        pPos = PFine + 1
      End If
    Wend
End If

pPos = 1
While pPos > 0
  pPos = RTBModifica.find("DLITCBL", pPos + 1, pEnd)
  If pPos > 0 Then
    PFine = RTBModifica.find(vbCrLf, pPos + 1, pEnd)
    For pInit = pPos To pPos - 80 Step -1
      If Mid$(RTBModifica.text, pInit, 2) = vbCrLf Then
         Exit For
      End If
    Next pInit
    pPos = RTBModifica.find(".", pInit, PFine)
    While pPos < 1
       PFine = RTBModifica.find(vbCrLf, PFine + 2, pEnd)
       pPos = RTBModifica.find(".", pInit, PFine)
    Wend
    pPos = RTBModifica.find(" ENTRY ", pInit, PFine)
    If pPos > 0 Then
       PFine = PFine - 2
       pPos = pInit - 4
       pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
       While pPos > 0
         If typeIncaps <> "DLI" Then
           'Fabio : RTBModifica.SelStart = pPos + 8
           'Fabio : RTBModifica.SelLength = 1
           'Fabio : RTBModifica.SelText = "*"
         End If
         pPos = RTBModifica.find(vbCrLf, pPos + 1, PFine)
       Wend
       pPos = RTBModifica.find(vbCrLf, PFine - 5, PFine + 4)
       If typeIncaps <> "DLI" Then
          RTBModifica.SelText = vbCrLf & Space$(11) & "MOVE SPACES TO LNKDB-STATO LNKDB-IMSRTNAME-QUAL2. " & vbCrLf
       Else
          RTBModifica.SelText = vbCrLf & Space$(11) & "MOVE SPACES TO LNKDB-IMSRTNAME-QUAL2.  " & vbCrLf
       End If
       pPos = RTBModifica.find(vbCrLf, pPos + 1, pEnd)
       PFine = pPos
    End If
    pPos = PFine + 1
  End If
Wend
End Function

Function CambiaParole(RTBModifica As RichTextBox, typeIncaps As String, wWhere As String) As Boolean
Dim Ind As Integer
Dim K As Variant

Dim pEnd As Variant
Dim pPos As Variant
Dim pStart As Variant
Dim pInit As Variant
Dim ModSw As Boolean
Dim SwTolta As Boolean
Dim Num01 As Integer
Dim xpos As Variant
Dim xLen As Variant
Dim i As Long
Dim idx01 As Long

Dim pGo As Long

Dim wLine As String

Dim NumApi As Integer
Dim wstr As String

If wWhere = "ALL" Then
   pStart = 1
Else
   pStart = RTBModifica.find(" " & wWhere, 1)
   
   Do While pStart > 0
      pEnd = RTBModifica.find(vbCrLf, pStart) - 1
      
      For i = 1 To 80
         RTBModifica.SelStart = pStart - i
         RTBModifica.SelLength = 2
         
         If RTBModifica.SelText = vbCrLf Then
            pInit = i + 1
            Exit For
         End If
      Next i
      
      RTBModifica.SelStart = pInit
      RTBModifica.SelLength = pEnd - pInit
      
      wLine = RTBModifica.SelText
      
      If Mid(wLine, 7, 1) <> "*" Then
         If wWhere = "PROCEDURE" Then
            idx01 = RTBModifica.find(" DIVISION", pStart)
         End If
         
         If wWhere = "LINKAGE" Then
            idx01 = RTBModifica.find(" SECTION", pStart)
         End If
         
         If wWhere = "WORKING-STORAGE" Then
            idx01 = RTBModifica.find(" SECTION", pStart)
         End If
         
         If idx01 > 0 Then
            pGo = pEnd
            Exit Do
         End If
      End If
      
      pStart = RTBModifica.find(" " & wWhere, pEnd)
   Loop
End If

'SG : Le modifiche devono partire da pGo in poi
'SG : Da completare

End Function

Function CreaSp(NumB, Optional ByVal wLabel As String) As String
   If wLabel = "" Then wLabel = START_TAG
   CreaSp = wLabel & Space(NumB - 7)
End Function

Function CreaNumStack(Stringa) As String
  Dim K As Integer
  
  For K = 1 To UBound(LNKDBNumstack)
    If LNKDBNumstack(K) = Stringa Then
      CreaNumStack = Str(K)
      Exit Function
    End If
  Next K
  
  K = UBound(LNKDBNumstack) + 1
  ReDim Preserve LNKDBNumstack(K)
  LNKDBNumstack(K) = Stringa
  CreaNumStack = Str(K)
End Function

Public Sub Load_StrSegmentoPadre(IdSegmCur As Long, IdDBD As Long, wIdOggettoCur As Long, wnomeSegm As String)
   Dim rs As Recordset
   Dim IdField As Long
   'Carica la struttura riferita al segmento padre
   
   mSegmentoPadre.IdFiglio = IdSegmCur
   mSegmentoPadre.IdDBD = IdDBD
   mSegmentoPadre.NomeFiglio = wnomeSegm
   
   Set rs = m_fun.Open_Recordset("SELECT * FROM PSDLi_segmenti AS T1 " & _
                                          "WHERE T1.Nome = (SELECT DISTINCT parent FROM PsDli_Segmenti as T2 " & _
                                                            "Where T2.IdSegmento = " & IdSegmCur & " " & _
                                                            "and T2.Idoggetto = " & IdDBD & ") " & _
                                          "And T1.IdOggetto = " & IdDBD)
   
   'Ha trovato il segmento padre
   If rs.RecordCount > 0 Then
     mSegmentoPadre.IdParent = rs!idSegmento
     mSegmentoPadre.NomeParent = rs!nome
   End If
   
   rs.Close
   
   'Controllare questa logica :
   'accedendo a una istruzione su quel dbd e di quel segmento trovo la ssa
   'Del padre?!?!?!
   Set rs = m_fun.Open_Recordset("Select * From PSDLI_IstrDett_liv Where IdOggetto = " & wIdOggettoCur & " And Idsegmento = " & mSegmentoPadre.IdParent)
   
   If rs.RecordCount > 0 Then
      mSegmentoPadre.AreaLen = rs!SegLen
      mSegmentoPadre.ssaName = rs!NomeSSA
      mSegmentoPadre.KeyLen = 9
   End If
   
   rs.Close
   
   Set rs = m_fun.Open_Recordset("Select * From PSDLI_Field Where Idsegmento = " & mSegmentoPadre.IdParent & " And Primario = true order By Posizione")
   
   If rs.RecordCount > 0 Then
      mSegmentoPadre.KeyLen = rs!Lunghezza
      mSegmentoPadre.keyName = rs!nome
   End If
End Sub

Public Function Restituisci_Livello_Segmento_dbd(widDbd As Long, wNomeSeg As String, Optional ByVal NON_SETTARE_OptionalInterno As Boolean = True) As Long
  Dim rs As Recordset
  Static wLevel As Long

  If NON_SETTARE_OptionalInterno Then
    wLevel = 0
  End If

  Set rs = m_fun.Open_Recordset("Select * From PsRel_Dliseg Where IdOggetto = " & widDbd & " And Child = '" & wNomeSeg & "'")
  If rs.RecordCount > 0 Then
    If rs!Father <> 0 Then
      wLevel = wLevel + 1
      wLevel = Restituisci_Livello_Segmento_dbd(widDbd, rs!Father, False)
      Restituisci_Livello_Segmento_dbd = wLevel
    Else
      Restituisci_Livello_Segmento_dbd = wLevel
      Exit Function
    End If
  End If
  rs.Close
End Function

Public Function Restituisci_Livello_Segmento(IdPSB As Long, NumPCB As Long, wNomeSeg As String, wLevel As Integer) As Integer
    Dim rs As Recordset
    Dim parent As String
    Set rs = m_fun.Open_Recordset("Select Parent From PsDLI_PsbSeg Where IdOggetto = " & IdPSB & " And NumPcb = " & NumPCB & " And Segmento = '" & wNomeSeg & "'")

    If Not rs.EOF Then
        If Not rs!parent = 0 Then
            wLevel = wLevel + 1
            parent = rs!parent
            rs.Close ' chiudo prima di riaprirne un altro
            'ricorsiva per livelli > 2
            wLevel = Restituisci_Livello_Segmento(IdPSB, NumPCB, parent, wLevel)
        Else
            rs.Close
        End If
    Else
      rs.Close
    End If
    Restituisci_Livello_Segmento = wLevel
    
End Function

Public Function Restituisci_ProcSeq(IdPSB As Long, NumPCB As Long) As String
    Dim rs As Recordset
    Dim wProcSeq As String
    Set rs = m_fun.Open_Recordset("Select ProcSeq From PsDLI_Psb Where IdOggetto = " & IdPSB & " And NumPcb = " & NumPCB)

    If Not rs.EOF Then
    'stefano: ma quando � null e quando invece � una stringa vuota? Fatemelo sapere
        If IsNull(rs!procSeq) Then
            wProcSeq = ""
        Else
            wProcSeq = rs!procSeq
        End If
    Else
        wProcSeq = ""
    End If
    rs.Close
    Restituisci_ProcSeq = wProcSeq
    
End Function

Public Function Restituisci_ProcOpt(IdPSB As Long, NumPCB As Long) As String
    Dim rs As Recordset
    Dim wProcOpt As String
    Set rs = m_fun.Open_Recordset("Select ProcOpt From PsDLI_Psb Where IdOggetto = " & IdPSB & " And NumPcb = " & NumPCB)

    If Not rs.EOF Then
    'stefano: ma quando � null e quando invece � una stringa vuota? Fatemelo sapere
        If IsNull(rs!procOpt) Then
            wProcOpt = ""
        Else
            wProcOpt = rs!procOpt
        End If
    Else
        wProcOpt = ""
    End If
    rs.Close
    Restituisci_ProcOpt = wProcOpt
    
End Function

Public Function Restituisci_varFunz(IdOgg As Long, riga As Long) As String
    Dim rs As Recordset
    Dim wFunz As String
    Set rs = m_fun.Open_Recordset("Select parametri From PsCall Where IdOggetto = " & IdOgg & " And Riga = " & riga)

    If Not rs.EOF Then
        If IsNull(rs!parametri) Then
            wFunz = "[VARIABLE MISSING]"
            m_fun.WriteLog "Incapsulatore - Restituisci_varFunz " & vbCrLf & "Variabile dell'istruzione non trovata " & IdOgg & " " & riga, "DR - Incapsulatore"
        Else
            wFunz = Left(rs!parametri, InStr(rs!parametri, " ") - 1)
        End If
    Else
        wFunz = "[VARIABLE MISSING]"
        m_fun.WriteLog "Incapsulatore - Restituisci_varFunz " & vbCrLf & "Variabile dell'istruzione non trovata " & IdOgg & " " & riga, "DR - Incapsulatore"
    End If
    rs.Close
    Restituisci_varFunz = wFunz
    
End Function

Public Sub CambiaENTRYTDLI(RT As RichTextBox, typeIncaps As String)
    Dim idx01 As Long
    Dim idxStart As Long
    Dim idxStop As Long
    Dim i As Long
    Dim wSpc As Long
    
    'SP 27/9 forzature alpitour
    Dim idxWk As Long, idxProc As Long
    
    'SP 4/9: NON BASTA! Bisogna vedere se � asteriscata! In tutte le ricerche mancava
    Dim entryOk As Boolean, entryKo As Boolean
    entryOk = False
    entryKo = False
    i = 1
    While Not entryOk And Not entryKo
      idx01 = RT.find("ENTRY ", i)
      If idx01 > 0 Then
         For idxStart = idx01 To idx01 - 80 Step -1
              RT.SelStart = idxStart
              RT.SelLength = 2
              If RT.SelText = vbCrLf Then
                
                Exit For
              End If
         Next idxStart
         RT.SelStart = idxStart + 7
         RT.SelLength = 1
         If RT.SelText <> "*" Then
            entryOk = True
         Else
            i = idx01 + 6
         End If
      Else
         entryKo = True
      End If
    Wend
      
    If idx01 > 0 Then
        idx01 = RT.find("'DLITCBL'", idx01)
        
        If idx01 > 0 Then
            'Torna indietro
            
            For idxStart = idx01 To idx01 - 80 Step -1
              RT.SelStart = idxStart
              RT.SelLength = 2
              If RT.SelText = vbCrLf Then
                
                Exit For
              End If
              
            Next idxStart
            
            idxStop = RT.find(".", idx01)
            idxStart = idxStart - 1
            
            Do While idxStart > 0
               If idxStart > 0 And idxStop > 0 Then
                  'asterisca
                  RT.SelStart = idxStart + 2
                  RT.SelLength = 7
                  RT.SelText = m_fun.LABEL_ENTRY & "*"
               End If
               
               idxStart = RT.find(vbCrLf, idxStart + 2)
               
               If idxStart > idxStop Then
                  Exit Do
               End If
            Loop
            
            'SP: 4/9: provo, a volte non funziona....
            'idxStop = RT.Find(vbCrLf, idxStart + 2)
           idxStop = RT.find(vbCrLf, idxStop)
            
           'Si posiziona alla fine
           RT.SelStart = idxStop + 2
           RT.SelLength = 0
           RT.SelText = m_fun.LABEL_ENTRY & "     COPY " & m_fun.COPY_NAME_ENTRYDLI & "." & vbCrLf
           
           'SP 27/9: specifica per Alpitour: istruzione DB2 per problemi di BIND
           'ovviamente da togliere poi
           If Not SwTp Then
              idxStop = RT.find("SQLCA", 1) 'cerco se gi� SQL
              If idxStop < 1 Then
                 idxStop = RT.find("05  IMSRTNAME-QUAL2   PIC X(02)  VALUE SPACES.", 1) 'cerca posizione ws
                 If idxStop > 0 Then
                    idxStop = RT.find(vbCrLf, idxStop)
                    RT.SelStart = idxStop + 2
                    RT.SelLength = 0
                    RT.SelText = m_fun.LABEL_ENTRY & " 01 ITER-BPHX-DATA PIC X(010)." & vbCrLf & m_fun.LABEL_ENTRY & "     EXEC SQL INCLUDE SQLCA END-EXEC." & vbCrLf
                    idxStop = RT.find("COPY ENTRYDLI.", idxStop) 'cerco se gi� SQL
                    If idxStop > 0 Then
                       idxStop = RT.find(vbCrLf, idxStop)
                       RT.SelStart = idxStop + 2
                       RT.SelLength = 0
                       RT.SelText = m_fun.LABEL_ENTRY & "     EXEC SQL SET :ITER-BPHX-DATA = CURRENT DATE END-EXEC." & vbCrLf
                    End If
                 End If
              End If
           End If
        End If
    End If
End Sub

Public Sub SpostaPCB2WorkingStorage(RT As RichTextBox, typeIncaps As String, cPcb As collection)
   Dim idx01 As Long
   Dim idxStart As Long
   Dim idxStop As Long
   Dim i As Long
   Dim wLine As String
   Dim wApp As String
   Dim wWord As String
   
   'SP exec dli
   If Not swCall Then Exit Sub
   idx01 = 1
   idxStop = RT.find(vbCrLf, idx01)
   
   Do While idx01 > 0
      RT.SelStart = idx01 + 2
      RT.SelLength = idxStop - idx01 + 2
      wLine = RT.SelText
      
      If Mid(wLine, 7, 1) <> "*" Then
         If InStr(1, wLine, "WORKING-STORAGE ") > 0 Then
            If InStr(1, wLine, " SECTION") > 0 Then
               'Si sposta alla riga successiva
               idxStop = idxStop + 2
               Exit Do
            End If
         End If
      End If
      
      idx01 = RT.find(vbCrLf, idx01 + 1)
      idxStop = RT.find(vbCrLf, idx01 + 1)
   Loop
   
   RT.SelStart = idxStop
   RT.SelLength = 0
   
   wLine = ""
   For i = 1 To cPcb.Count
      wLine = wLine & m_fun.LABEL_INFO & "     " & cPcb(i) & vbCrLf
   Next
   
   RT.SelText = wLine
   
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 26-07-06
' Caricare qui (UNA VOLTA PER TUTTE) tutti gli "Environment Parameters" necessari
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub loadEnvironmentParameters()
  'Dim parameterName As String
  START_TAG = Left(m_fun.LeggiParam("IMS-INCAPS-START-TAG") & Space(6), 6)
  END_TAG = Left(m_fun.LeggiParam("IMS-INCAPS-END-TAG") & Space(8), 8)
  xCopyParam = m_fun.LeggiParam("X-COPY")
  tCopyParam = m_fun.LeggiParam("T-COPY")
  cCopyParam = m_fun.LeggiParam("C-COPY")
  kCopyParam = m_fun.LeggiParam("K-COPY")
  mCopyParam = m_fun.LeggiParam("MOVE-COPY")
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 26-07-06
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function insertLine(inputText As String, line As String, Optional indent As Integer = 7) As Boolean
  Dim statement As String, extraindent As Integer, Lenbeftrim As Integer, appoindent As Integer
  Dim startTag As String
  insertLine = True
  extraindent = getTotalIndent(line) ' ripulisce anche dai tag indent
  ' se rimangono tag non risolti non inserisco linea
  If InStr(1, line, "<") > 0 And InStr(1, line, ">") > 0 Then
    insertLine = False
    Exit Function
  End If
  If isPLI Then
    statement = Space(indent + extraindent) & line
  Else
    startTag = START_TAG  'Copia: viene modificato
  '  If startTag = "" Then
  '    startTag = "BPHX-P"
  '  End If
    
    ' startTag formattato a sette caratteri
    If Len(startTag) > 7 Then
      startTag = Left(startTag, 7)
    ElseIf Len(startTag) < 7 Then
      startTag = startTag & Space(7 - Len(startTag))
    End If
    ' tolgo spazi davanti a linea e li sommo all'indentazione
    Lenbeftrim = Len(line)
    line = LTrim(line)
    appoindent = indent + (Lenbeftrim - Len(line))
    If Len(line) > Len(startTag) - extraindent Then
      If Not (appoindent - Len(startTag) + extraindent) < 0 Then
        'statement = startTag & Space(indent - 7 + extraindent) & Right(Line, Len(Line) - Len(startTag))
        statement = startTag & Space(appoindent - Len(startTag) + extraindent) & line
      Else
        'statement = startTag & Space(indent - 7) & Right(Line, Len(Line) - Len(startTag) + extraindent)
      End If
    Else
      statement = startTag & Space(appoindent - 7) & line
    End If
  End If
  'stefano: lo ho reinserito, altrimenti considera > 72 anche quelli con spazi in fondo
  statement = RTrim(statement)
  'stefano: sono entrato a gamba tesa, ma non mi � chiaro questo giro...
  If Len(statement) >= 73 Then
    'a capo automatico...
    'stefano: VALE SOLO PER IL COBOL, per ora...
     Dim j, K As Integer
     Dim splitLine As String
     Dim splitLine2 As String
     j = Len(RTrim(statement))
     splitLine = (RTrim(statement))
     K = 72
     splitLine2 = Space(72)
     Do While Mid(splitLine, j, 1) <> " " Or j > 72
      Mid(splitLine2, K, 1) = Mid(splitLine, j, 1)
      Mid(splitLine, j, 1) = " "
      K = K - 1
      j = j - 1
     Loop
     If j > 0 Then
        statement = RTrim(splitLine) & Space(72 - Len(RTrim(splitLine))) & END_TAG
        inputText = inputText & statement & vbCrLf
        'space(20) arbitratrio
        statement = startTag & Space(20) & LTrim(splitLine2) & Space(72 - Len(LTrim(splitLine2))) & END_TAG
        inputText = inputText & statement & vbCrLf
     End If
  Else
    If Not isPLI Then
      statement = statement & Space(72 - Len(statement)) & END_TAG
    End If
    inputText = inputText & statement & vbCrLf
  End If
End Function

Private Sub insertLineOld(inputText As String, line As String, Optional indent As Integer = 7)
  Dim statement As String
  'If wLabel = "" Then wLabel = START_TAG
  statement = START_TAG & Space(indent - 7) & line
  If Len(statement) >= 72 Then
    'a capo automatico...
  Else
    statement = statement & Space(72 - Len(statement)) & END_TAG
    inputText = inputText & statement & vbCrLf
  End If
End Sub

Public Function istrIncopy(pIdOggetto As Long) As Boolean
  Dim rst As Recordset
  
  Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE idPgm =" & pIdOggetto & " AND idOggetto <> " & pIdOggetto)
  If Not rst.EOF Then
    istrIncopy = True
  End If
  rst.Close
End Function

Public Sub InsertMovePgmName(RTBModifica As RichTextBox, namepgm As String)
  Dim pos As Long
  Dim ArrFile() As String, numfile As Integer, riga As Integer, i As Integer
  Dim Index As Integer, bolProc As Boolean, rtbTesto As String


  If InStr(namepgm, ".") Then
    namepgm = Left(namepgm, InStr(namepgm, ".") - 1)
  End If
'  RTBModifica.SaveFile namepgm & "appo", 1

 ' riga = 1
  ArrFile = Split(RTBModifica.text, vbCrLf)
'  numfile = FreeFile
  
'  Open namepgm & "appo" For Input As numfile
'  Line Input #numfile, ArrFile(riga)
'  Do Until EOF(numfile)
'    riga = riga + 1
'    ReDim Preserve ArrFile(riga)
'    Line Input #numfile, ArrFile(riga)
'  Loop
'  Close numfile

  bolProc = False
  Index = 1
  Do While (Index <= UBound(ArrFile)) And Not bolProc
   If InStr(8, ArrFile(Index), "PROCEDURE DIVISION") And Not Mid(ArrFile(Index), 7, 1) = "*" Then
    
    While Not InStr(8, ArrFile(Index), ".") > 0
        Index = Index + 1
    Wend
    
    ArrFile(Index) = ArrFile(Index) & vbCrLf & "BPHX-R     MOVE '" & namepgm & "' TO LNKDB-CALLER."
    bolProc = True
   Else
    Index = Index + 1
   End If
  Loop
'  numfile = FreeFile
'  Open namepgm & "appo" For Output As numfile
'  For i = 1 To UBound(ArrFile)
'    Print #numfile, ArrFile(i)
'  Next i
'  Close numfile
'  RTBModifica.LoadFile namepgm & "appo"

rtbTesto = ArrFile(0)
For Index = 1 To UBound(ArrFile)
  rtbTesto = rtbTesto & vbCrLf & ArrFile(Index)
Next
 RTBModifica.text = rtbTesto
'  Kill namepgm & "appo"
  AddRighe = AddRighe + 1
End Sub


Public Sub insertPLISaveBack(PLIsaveBackKey As String, startpos As Long, endposrighe As Long, RT As RichTextBox)
Dim wresp As Boolean, endpos As Long, find As Boolean, alwaysinsert As Boolean, validpoint As Boolean
Dim w As Long, t As Long, h As Long, s() As String, reservedWord As String, testothen As String
Dim iniziocommento As Long, finecommento As Long, oldiniziocommento As Long
  alwaysinsert = True
  If Not endposrighe = 0 Then
    wresp = TROVA_RIGA(RT, "CALL", endposrighe, RT.SelStart)
    endpos = RT.SelStart
  Else
    endpos = Len(RT.text)
  End If
  While Not w = -1
    validpoint = False
    While Not validpoint
      w = RT.find(PLIsaveBackKey, startpos, endpos)
      If Not w = -1 Then
        ' ciclo per catturare l'inizio commento pi� vicino alla parola chiave
        oldiniziocommento = iniziocommento
        While iniziocommento < w And Not iniziocommento = -1
          oldiniziocommento = iniziocommento
          iniziocommento = RT.find("/*", startpos, endpos)
          startpos = iniziocommento + 2
        Wend
        If Not iniziocommento = -1 Then
          finecommento = RT.find("*/", oldiniziocommento, endpos)
        End If
        If Not (w > oldiniziocommento And w < finecommento) Then
         For t = w To 1 Step -1
          If Mid(RT.text, t, 2) = vbCrLf Then
            validpoint = True
            Exit For
          End If
         Next
        Else
            startpos = finecommento + 1
        End If
      Else
        validpoint = True ' per uscire dal ciclo validpoint con w= -1
      End If
    Wend
    If PLIsaveBackKey = "END " Then
      If InStr(Mid(RT.text, w + 3, 20), NomeProg) > 0 Then
        alwaysinsert = True
      Else
        alwaysinsert = False
        startpos = w + 2
      End If
    End If
    If Not w = -1 And alwaysinsert Then
'      If findReservedWord(t, w, RT, reservedWord) Then
'        testothen = "          " & reservedWord & vbCrLf & testoback
'        h = RT.find(reservedWord, t, w)
'        RT.SelStart = h
'        RT.SelLength = Len(reservedWord)
'        RT.SelText = ""
'        RT.SelStart = t + 1
'        RT.SelLength = 0
'        RT.SelText = testothen
'        s = Split(testothen, vbCrLf)
'        startpos = w + 2 + Len(testothen)
'      Else
'        RT.SelStart = t + 1
'        RT.SelLength = 0
'        RT.SelText = testoback
'        s = Split(testoback, vbCrLf)
'        startpos = w + 2 + Len(testoback)
'      End If
      AddRighe = AddRighe + UBound(s)
      find = True
      iniziocommento = 0
    End If
 Wend
 If Not find Then
  RT.SelStart = RT.SelStart - 2
 End If
End Sub

Function CaricaIstruzioneC(rst As Recordset, idpgm As Long) As Boolean
  Dim tb As Recordset, tb1 As Recordset, rs As Recordset, rsPgm As Recordset
  Dim IndI As Integer
  Dim K As Integer, k1 As Integer, k2 As Integer, k3 As Integer, z As Integer
  
  Dim u As Long
  Dim wXDField As Boolean
  
  Dim vEnd As Double, vPos As Double, vPos1 As Double, vStart As Double
  Dim wstr As String
  Dim TbLenField As Recordset
  
  Dim sDbd() As Long
  Dim sPsb() As Long
  Dim sSegD() As String
  
  Dim TbKey As Recordset
  
  ReDim TabIstr.BlkIstr(0)
  ReDim TabIstr.dbd(0)
  ReDim TabIstr.psb(0)
    
  SwTp = False
  ' Mauro 15/06/2007 : Id del programma chiamante
  'Set tb = m_fun.Open_Recordset("select * from BS_Oggetti where idoggetto = " & rst!IdOggetto)
  Set tb = m_fun.Open_Recordset("select * from BS_Oggetti where idoggetto = " & idpgm)
  If Not tb.EOF Then
     SwTp = tb!Cics
  End If
  tb.Close
  
  'SQ CPY-ISTR
  Set tb = m_fun.Open_Recordset( _
    "SELECT * FROM PsDli_IstrDett_Liv WHERE " & _
    "idPgm=" & idpgm & " AND idoggetto=" & rst!idOggetto & " and riga = " & rst!riga & " order by livello")
             
  '''''''''''''''DEVE CARICARE PI� DBD e pi�PSB
  'sDbd = getDBDByIdOggettoRiga(idpgm, rst!idOggetto, rst!riga)
  'sPsb = getPSBByIdOggettoRiga(idpgm, rst!idOggetto, rst!riga)
  sDbd = getDBDByIdOggettoRiga(rst!idOggetto, rst!riga)
  sPsb = getPSBByIdOggettoRiga(rst!idOggetto, rst!riga)
  
  For u = 1 To UBound(sDbd)
    ReDim Preserve TabIstr.dbd(u)
    TabIstr.dbd(u).Id = sDbd(u)
    TabIstr.dbd(u).nome = Restituisci_NomeOgg_Da_IdOggetto(sDbd(u))
    TabIstr.dbd(u).Tipo = TYPE_DBD
  Next u
'per ora � senza vettore
  TabIstr.isGsam = False
  If UBound(TabIstr.dbd) > 0 Then
    Set tb1 = m_fun.Open_Recordset("select * from BS_Oggetti where tipo_DBD = 'GSA' and nome = '" & TabIstr.dbd(1).nome & "'")
    TabIstr.isGsam = Not tb1.EOF
    tb1.Close
  End If
  For u = 1 To UBound(sPsb)
    ReDim Preserve TabIstr.psb(u)
    TabIstr.psb(u).Id = sPsb(u)
    TabIstr.psb(u).nome = Restituisci_NomeOgg_Da_IdOggetto(sPsb(u))
    TabIstr.psb(u).Tipo = TYPE_PSB
  Next u
  '**********
  'info dalla tabella PsdLi_istruzioni : occorre la riga con l'idpgm corretto
  Set rsPgm = m_fun.Open_Recordset("Select * from PsDLI_Istruzioni WHERE idPgm=" & idpgm & " AND idoggetto=" & rst!idOggetto & _
        " AND riga= " & rst!riga)
  If Not rsPgm.EOF Then
    TabIstr.PCB = rsPgm!NumPCB & ""
 
    If Not IsNull(rsPgm!NumPCB) Then
      TabIstr.PCBRed = getPCDRedFromPCB(rsPgm!NumPCB, rst!idOggetto)
    End If
    If TabIstr.PCBRed = "" Then
      TabIstr.PCBRed = TabIstr.PCB
    End If
    'SQ
    TabIstr.NumPCB = IIf(IsNull(rsPgm!ordPcb), 0, rsPgm!ordPcb)
    If Trim(rsPgm!Istruzione) = "QUERY" Then
      TabIstr.Istr = "QRY"
    Else
      TabIstr.Istr = Trim(rsPgm!Istruzione)
    End If
  End If
  rsPgm.Close
  '*****************
  If UBound(TabIstr.psb) Then
     TabIstr.procSeq = Restituisci_ProcSeq(TabIstr.psb(1).Id, TabIstr.NumPCB)
     TabIstr.procOpt = Restituisci_ProcOpt(TabIstr.psb(1).Id, TabIstr.NumPCB)
  Else
     TabIstr.procSeq = ""
     TabIstr.procOpt = ""
  End If
  
  'ginevra: keyfeedback
  If Trim(rst!keyfeedback) <> "" Then
     TabIstr.keyfeedback = Trim(rst!keyfeedback)
  Else
     TabIstr.keyfeedback = ""
  End If
    
  If TabIstr.Istr = "CHKP" Or TabIstr.Istr = "TERM" Or _
     TabIstr.Istr = "SYNC" Or TabIstr.Istr = "PCB" Or _
     TabIstr.Istr = "QRY" Then
   
   ' Mauro 15/06/2007 : Id del programma chiamante
   'SwTp = m_fun.Is_CICS(rst!IdOggetto)
   'AC 19/06/07
   'spostato sopra per tutte le istruzioni
   'SwTp = m_fun.Is_CICS(rst!idpgm)
       
   ' stefano: ma come fa a funzionare che indi non � impostato? Oppure zero va bene come valore?
   If UBound(TabIstr.dbd) > 0 Then
      TabIstr.BlkIstr(IndI).NomeRout = m_fun.Genera_Nome_Routine(TabIstr.dbd(1).nome, TabIstr.Istr, SwTp)
    Else
      TabIstr.BlkIstr(IndI).NomeRout = m_fun.Genera_Nome_Routine("", TabIstr.Istr, SwTp)
    End If
  End If
  
  If tb.RecordCount > 0 Then
    tb.MoveFirst
    IndI = 0
    
    While Not tb.EOF
       IndI = IndI + 1
       ReDim Preserve TabIstr.BlkIstr(IndI)
       k3 = 0
       ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
       
       ' Mauro 15/06/2007 : Id del programma chiamante
       'SwTp = m_fun.Is_CICS(tb!IdOggetto)
       'SwTp = m_fun.Is_CICS(tb!idpgm)
       
       TabIstr.BlkIstr(IndI).ssaName = Trim(tb!NomeSSA & " ")
       ' lunghezza area (impostat solo per i GSAM per ora)
       If Not IsNull(tb!AreaLen) And tb!AreaLen > 0 Then
          TabIstr.BlkIstr(IndI).AreaLen = tb!AreaLen
       Else
          TabIstr.BlkIstr(IndI).AreaLen = 0
       End If
       '<NONE>
       If Len(TabIstr.BlkIstr(IndI).ssaName) Then
       'stefano: SSA costante
          If InStr(TabIstr.BlkIstr(IndI).ssaName, "'") > 0 Then
             TabIstr.BlkIstr(IndI).SsaLen = Len(TabIstr.BlkIstr(IndI).ssaName) - 2 'toglie gli apici
          Else
            TabIstr.BlkIstr(IndI).SsaLen = m_fun.TrovaLenSSA(TabIstr.BlkIstr(IndI).ssaName, rst!idOggetto)
          End If
       End If
       
       'SQ tmp...
       TabIstr.BlkIstr(IndI).Parentesi = IIf(tb!Qualificazione, "(", " ")
                     
       TabIstr.BlkIstr(IndI).SegLen = tb!SegLen
       TabIstr.BlkIstr(IndI).Search = IIf(IsNull(tb!condizione), "", tb!condizione)
       While InStr(TabIstr.BlkIstr(IndI).Search, ";;") > 0
        TabIstr.BlkIstr(IndI).Search = Replace(TabIstr.BlkIstr(IndI).Search, ";;", ";")
       Wend

       If UBound(TabIstr.dbd) > 0 Then
         TabIstr.BlkIstr(IndI).NomeRout = m_fun.Genera_Nome_Routine(TabIstr.dbd(1).nome, TabIstr.Istr, SwTp)
       Else
         TabIstr.BlkIstr(IndI).NomeRout = m_fun.Genera_Nome_Routine("", TabIstr.Istr, SwTp)
       End If
       'alpitour: 5/8/2005 forzato diversamente; la gestione della data area andava fatta meglio
       ' o perlomeno completata prima di renderla effettiva;
       ' in questo modo si sono aumentate le mancanze, in quanto la generazione routine
       ' faceva un ragionamento diverso
       'If tb!StrIdOggetto > 0 Then
       If Len(tb!dataarea) Then
         TabIstr.BlkIstr(IndI).Record = tb!dataarea & ""
         'TabIstr.BlkIstr(IndI).recordReaddress = findRead(tb!dataarea, rst!idOggetto)
       End If
       
       If tb!idSegmento = -1 Then
          TabIstr.BlkIstr(IndI).Segment = tb!VariabileSegmento
          TabIstr.BlkIstr(IndI).IdSegm = tb!idSegmento
       Else
          If IsNull(tb!idSegmento) Then
            'virgilio
            m_fun.WriteLog "Per l'istruzione " & TabIstr.Istr & " dell'oggetto " & tb!idOggetto & " l'idsegmento � Null", " incapsulatore.caricaistruzione "
            Exit Function
          Else
            Set tb1 = m_fun.Open_Recordset("select * from PsDli_Segmenti where idsegmento = " & tb!idSegmento)
            If Not tb1.EOF Then
               TabIstr.BlkIstr(IndI).Segment = tb1!nome
               TabIstr.BlkIstr(IndI).IdSegm = tb!idSegmento
            End If
            tb1.Close
            If Not IsNull(tb!idSegmentiD) Then
              'STEFANO: verificarla meglio
              'If Not tb!idSegmentiD = "" Then
              If Not tb!idSegmentiD = "" And Not tb!idSegmentiD = "0" Then
                sSegD = Split(tb!idSegmentiD, ";")
                For z = 0 To UBound(sSegD)
                  Set tb1 = m_fun.Open_Recordset("select * from PsDli_Segmenti where idsegmento = " & CLng(sSegD(z)))
                  If Not tb1.EOF Then
                    TabIstr.BlkIstr(IndI).SegmentM = TabIstr.BlkIstr(IndI).SegmentM & tb1!nome & ";"
                  End If
                  tb1.Close
                Next
                TabIstr.BlkIstr(IndI).SegmentM = Left(TabIstr.BlkIstr(IndI).SegmentM, Len(TabIstr.BlkIstr(IndI).SegmentM) - 1)
              Else
                TabIstr.BlkIstr(IndI).SegmentM = ""
              End If
            End If
          End If
        End If
      
       
       Set rs = m_fun.Open_Recordset("select * from DMDB2_Tabelle where idorigine = " & tb!idSegmento)
       If rs.RecordCount > 0 Then
          TabIstr.BlkIstr(IndI).Tavola = rs!nome
       Else
 '         Stop
        'SQ stop cosa?! forse c'� qualche problema!? (� giusto che vada secco sul DB2? verificare)
       End If
       rs.Close
      
       'SQ CPY-ISTR
       Set rs = m_fun.Open_Recordset( _
        "SELECT * FROM PsDli_IstrDett_Key WHERE idPgm=" & idpgm & " AND idoggetto=" & rst!idOggetto & _
        " AND riga= " & rst!riga & " AND livello=" & tb!Livello & " ORDER BY progressivo")
       If rs.RecordCount > 0 Then
          k3 = 0
          While Not rs.EOF
             k3 = k3 + 1
             
             ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyBool = Trim(tb!Qualificazione)
             'TabIstr.BlkIstr(IndI).KeyDef(k3).Op = m_fun.TrattaOp(Tb2!Operatore & "")
             TabIstr.BlkIstr(IndI).KeyDef(k3).Op = rs!Operatore & ""
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal = rs!Valore & ""
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = Trim(rs!KeyLen)
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyStart = rs!KeyStart
             '8-05-06
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVarField = rs!keyvalore
             TabIstr.BlkIstr(IndI).KeyDef(k3).xName = rs!xName
             TabIstr.BlkIstr(IndI).KeyDef(k3).OpLogic = Trim(rs!booleano)
             
             If rs!IdField > 0 Then
                Set tb1 = m_fun.Open_Recordset("select * from PsDLi_Field where idfield = " & Val(rs!IdField & " "))
                wXDField = False
             Else
                Set tb1 = m_fun.Open_Recordset("select * from PsDLi_XDField where idXDField = " & Abs(Val(rs!IdField)))
                wXDField = True
             End If
             
              If tb1.RecordCount > 0 Then
                If Not wXDField Then
                'ginevra: l'ho tolto per il momento: serviva?
                '    If Trim(Tb1!ptr_xNome) = "" Then
                     TabIstr.BlkIstr(IndI).KeyDef(k3).key = tb1!nome
                '    Else
                '       TabIstr.BlkIstr(IndI).KeyDef(k3).key = Tb1!ptr_xNome
                '    End If
                Else
                    TabIstr.BlkIstr(IndI).KeyDef(k3).key = tb1!nome
                End If
                
                'STEFANO TROVA KEYLEN
                '<NONE>
                If Len(TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen) Then
                  If Val(rs!IdField) <> 0 Then
                    'Recupera la lunghezza del field
                    Set TbLenField = m_fun.Open_Recordset("Select * From PsDli_Field Where IdField = " & Val(rs!IdField))
                    
                    If TbLenField.RecordCount > 0 Then
                        TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = TbLenField!Lunghezza
                    End If
                    
                    TbLenField.Close
                  End If
                End If
             Else
                k3 = k3
             End If
             
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyNum = CarNumKey(IndI)
    
             rs.MoveNext
          Wend
      End If
      rs.Close
      tb.MoveNext
    Wend
  End If
  
  CaricaIstruzioneC = True
 
End Function


Public Function FormatTag(line As String, Optional pendtag As String = "") As String
  Dim lenline As Integer
  
  lenline = Len(line)
  If pendtag = "" Then
    line = START_TAG & " " & line & Space(65 - lenline) & END_TAG & vbCrLf
  Else
    line = START_TAG & " " & line & Space(65 - lenline) & pendtag & vbCrLf
  End If
  FormatTag = line
End Function

Public Sub insertExternalEntry(RTBMod As RichTextBox)
'  Dim testoext As String, i As Integer
'  For i = 1 To UBound(routinePLI)
'    testoext = testoext & "     DCL " & routinePLI(i) & " EXTERNAL ENTRY;" & vbCrLf
'  Next
'  'If UBound(routinePLI) > 0 Then
'  'testoext = "              /* " & START_TAG & " START              */" & vbCrLf & testoext _
'  '& "              /* " & START_TAG & " END             */"
'  'End If
'  testoext = Left(testoext, Len(testoext) - 2)
'  MavsdM_GenRout.changeTag RTBMod, "<EXTERNALENTRY>", testoext
End Sub


