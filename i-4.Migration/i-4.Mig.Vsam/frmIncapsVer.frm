VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmIncapsVer 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Verifica sulla Struttura del DLI"
   ClientHeight    =   10395
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   15270
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   10395
   ScaleWidth      =   15270
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox lstPSBAssSegm 
      Height          =   450
      Left            =   180
      TabIndex        =   93
      Top             =   6930
      Width           =   1455
   End
   Begin VB.ListBox ListaOggetti 
      Height          =   840
      Left            =   9660
      TabIndex        =   80
      Top             =   7560
      Visible         =   0   'False
      Width           =   1005
   End
   Begin MSComDlg.CommonDialog ComD 
      Left            =   210
      Top             =   7560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin RichTextLib.RichTextBox RTPsbError 
      Height          =   615
      Left            =   7500
      TabIndex        =   61
      Top             =   7500
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   1085
      _Version        =   393217
      TextRTF         =   $"frmIncapsVer.frx":0000
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ListBox lstId 
      Height          =   450
      Left            =   2670
      TabIndex        =   28
      Top             =   7500
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstidSelez 
      Height          =   450
      Left            =   1260
      TabIndex        =   11
      Top             =   7500
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   3450
      ScaleHeight     =   855
      ScaleWidth      =   3975
      TabIndex        =   7
      Top             =   7470
      Visible         =   0   'False
      Width           =   3975
      Begin VB.FileListBox filList 
         Height          =   675
         Left            =   60
         TabIndex        =   10
         Top             =   90
         Width           =   1815
      End
      Begin VB.DirListBox dirList 
         Height          =   540
         Left            =   1950
         TabIndex        =   9
         Top             =   300
         Width           =   2055
      End
      Begin VB.DriveListBox drvList 
         Height          =   315
         Left            =   1920
         TabIndex        =   8
         Top             =   0
         Width           =   2055
      End
   End
   Begin TabDlg.SSTab SSTabError 
      Height          =   3825
      Left            =   0
      TabIndex        =   2
      Top             =   2985
      Width           =   11925
      _ExtentX        =   21034
      _ExtentY        =   6747
      _Version        =   393216
      TabOrientation  =   1
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   8
      TabHeight       =   520
      ForeColor       =   12582912
      TabCaption(0)   =   "VIE00001 : PSB non associato"
      TabPicture(0)   =   "frmIncapsVer.frx":0135
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame8"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "VIE00002 : Istruzioni non decodificate"
      TabPicture(1)   =   "frmIncapsVer.frx":0151
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame10"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "[PRS00006] : DBD mancanti"
      TabPicture(2)   =   "frmIncapsVer.frx":016D
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame24"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "VIE00004 : Segmenti non riconosciuti"
      TabPicture(3)   =   "frmIncapsVer.frx":0189
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame5"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "Frame6"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).ControlCount=   2
      TabCaption(4)   =   "Strumenti"
      TabPicture(4)   =   "frmIncapsVer.frx":01A5
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "FraRicFile"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "ProgressBar1"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).ControlCount=   2
      Begin VB.Frame Frame6 
         Caption         =   "Confronto PSB dipendenti dai segmenti non riconosciuti e i DBD mancanti"
         ForeColor       =   &H8000000D&
         Height          =   2040
         Left            =   -72345
         TabIndex        =   92
         Top             =   45
         Width           =   5820
         Begin MSComctlLib.ListView lstPSBDBD 
            Height          =   1590
            Left            =   3540
            TabIndex        =   97
            Top             =   300
            Width           =   2175
            _ExtentX        =   3836
            _ExtentY        =   2805
            View            =   3
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            Icons           =   "imlToolbarIcons"
            SmallIcons      =   "imlToolbarIcons"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "nPSB"
               Text            =   "Nome PSB"
               Object.Width           =   3704
            EndProperty
         End
         Begin VB.CommandButton cmdAvvio 
            Height          =   465
            Left            =   2835
            Picture         =   "frmIncapsVer.frx":01C1
            Style           =   1  'Graphical
            TabIndex        =   95
            Top             =   1440
            Width           =   510
         End
         Begin VB.Label Label19 
            Caption         =   "Premere il bottone per avviare la verifica delle associazioni."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   375
            Left            =   135
            TabIndex        =   96
            Top             =   1440
            Width           =   2580
         End
         Begin VB.Label Label18 
            Caption         =   $"frmIncapsVer.frx":030B
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000C0&
            Height          =   1230
            Left            =   135
            TabIndex        =   94
            Top             =   270
            Width           =   3210
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Lista dei Segmenti "
         ForeColor       =   &H8000000D&
         Height          =   2040
         Left            =   -74910
         TabIndex        =   90
         Top             =   45
         Width           =   2445
         Begin MSComctlLib.ListView lswSegm 
            Height          =   1680
            Left            =   135
            TabIndex        =   91
            Top             =   225
            Width           =   2175
            _ExtentX        =   3836
            _ExtentY        =   2963
            View            =   3
            Sorted          =   -1  'True
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            Icons           =   "imlToolbarIcons"
            SmallIcons      =   "imlToolbarIcons"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "Nseg"
               Text            =   "Nome Segmento"
               Object.Width           =   3704
            EndProperty
         End
      End
      Begin VB.Frame Frame24 
         Height          =   3285
         Left            =   -74880
         TabIndex        =   62
         Top             =   60
         Width           =   10155
         Begin VB.Frame Frame26 
            Caption         =   "DBD selezionato"
            ForeColor       =   &H8000000D&
            Height          =   735
            Left            =   150
            TabIndex        =   70
            Top             =   180
            Width           =   3525
            Begin VB.TextBox txtDBD 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   285
               Left            =   1380
               TabIndex        =   72
               Top             =   270
               Width           =   1965
            End
            Begin VB.Label Label13 
               Caption         =   "Nome DBD :"
               ForeColor       =   &H8000000D&
               Height          =   225
               Left            =   150
               TabIndex        =   71
               Top             =   330
               Width           =   1005
            End
         End
         Begin VB.Frame Frame25 
            Caption         =   "Oggetto in relazione con il DBD selezionato"
            ForeColor       =   &H8000000D&
            Height          =   2055
            Left            =   150
            TabIndex        =   63
            Top             =   1050
            Width           =   3555
            Begin VB.TextBox txtDir 
               Height          =   285
               Left            =   960
               TabIndex        =   74
               Top             =   1620
               Width           =   2445
            End
            Begin VB.TextBox txtTipo 
               Height          =   285
               Left            =   1380
               TabIndex        =   69
               Top             =   1170
               Width           =   2025
            End
            Begin VB.TextBox txtNomeOGG 
               Height          =   285
               Left            =   1380
               TabIndex        =   68
               Top             =   720
               Width           =   2025
            End
            Begin VB.TextBox txtID 
               Height          =   285
               Left            =   1380
               TabIndex        =   67
               Top             =   300
               Width           =   2025
            End
            Begin VB.Label Label16 
               Caption         =   "Directory :"
               ForeColor       =   &H8000000D&
               Height          =   225
               Left            =   120
               TabIndex        =   75
               Top             =   1650
               Width           =   1005
            End
            Begin VB.Label Label15 
               Caption         =   "Nome Oggetto :"
               ForeColor       =   &H8000000D&
               Height          =   225
               Left            =   120
               TabIndex        =   66
               Top             =   750
               Width           =   1185
            End
            Begin VB.Label Label14 
               Caption         =   "Tipo :"
               ForeColor       =   &H8000000D&
               Height          =   225
               Left            =   120
               TabIndex        =   65
               Top             =   1200
               Width           =   1005
            End
            Begin VB.Label Label12 
               Caption         =   "ID oggetto :"
               ForeColor       =   &H8000000D&
               Height          =   225
               Left            =   120
               TabIndex        =   64
               Top             =   330
               Width           =   1005
            End
         End
      End
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   165
         Left            =   -74790
         TabIndex        =   53
         Top             =   630
         Visible         =   0   'False
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   291
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin VB.Frame Frame8 
         Height          =   3135
         Left            =   4110
         TabIndex        =   34
         Top             =   30
         Width           =   7605
         Begin VB.Frame Frame21 
            Caption         =   "Monitor Stato associazione PSB nel progetto"
            ForeColor       =   &H8000000D&
            Height          =   2775
            Left            =   2520
            TabIndex        =   59
            Top             =   210
            Width           =   4935
            Begin VB.Frame Frame22 
               Caption         =   "PSB non esistenti"
               ForeColor       =   &H8000000D&
               Height          =   2415
               Left            =   90
               TabIndex        =   60
               Top             =   240
               Width           =   4725
               Begin MSComctlLib.TreeView TreeVPSB 
                  Height          =   2025
                  Left            =   150
                  TabIndex        =   76
                  Top             =   240
                  Width           =   4425
                  _ExtentX        =   7805
                  _ExtentY        =   3572
                  _Version        =   393217
                  LineStyle       =   1
                  Style           =   7
                  ImageList       =   "imlToolbarIcons"
                  Appearance      =   1
               End
            End
         End
         Begin VB.Frame Frame14 
            Caption         =   "Associazione Del PSB"
            ForeColor       =   &H8000000D&
            Height          =   2775
            Left            =   180
            TabIndex        =   35
            Top             =   210
            Width           =   2235
            Begin VB.CommandButton cmdOkAss 
               Caption         =   "Conferma"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Left            =   180
               TabIndex        =   42
               Top             =   2280
               Width           =   1875
            End
            Begin VB.Frame Frame16 
               Caption         =   "File CBL"
               ForeColor       =   &H8000000D&
               Height          =   705
               Left            =   180
               TabIndex        =   40
               Top             =   1410
               Width           =   1875
               Begin VB.TextBox txtCBL 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   41
                  Top             =   240
                  Width           =   1605
               End
            End
            Begin VB.Frame Frame15 
               Height          =   465
               Left            =   180
               TabIndex        =   38
               Top             =   930
               Width           =   1875
               Begin VB.Label Label5 
                  Caption         =   "ASSOCIA A :"
                  ForeColor       =   &H000000FF&
                  Height          =   225
                  Left            =   480
                  TabIndex        =   39
                  Top             =   180
                  Width           =   1065
               End
            End
            Begin VB.Frame Frame13 
               Caption         =   "File PSB "
               ForeColor       =   &H8000000D&
               Height          =   705
               Left            =   180
               TabIndex        =   36
               Top             =   210
               Width           =   1875
               Begin VB.TextBox txtPSB 
                  Height          =   315
                  Left            =   120
                  TabIndex        =   37
                  Top             =   240
                  Width           =   1605
               End
            End
         End
      End
      Begin VB.Frame Frame10 
         Caption         =   "Istruzioni Non Decodificate"
         ForeColor       =   &H8000000D&
         Height          =   3345
         Left            =   -74880
         TabIndex        =   22
         Top             =   30
         Width           =   11655
         Begin VB.ListBox LstInstrERR 
            Height          =   1815
            ItemData        =   "frmIncapsVer.frx":03CF
            Left            =   120
            List            =   "frmIncapsVer.frx":03D1
            TabIndex        =   26
            Top             =   1170
            Width           =   9495
         End
         Begin VB.Frame Frame12 
            Height          =   825
            Left            =   120
            TabIndex        =   23
            Top             =   180
            Width           =   9495
            Begin VB.Frame Frame11 
               Caption         =   "Errori riscontrati Dopo La Verifica :"
               ForeColor       =   &H8000000D&
               Height          =   555
               Left            =   90
               TabIndex        =   24
               Top             =   150
               Width           =   9315
               Begin VB.Label lblInstrErr 
                  ForeColor       =   &H000000FF&
                  Height          =   225
                  Left            =   150
                  TabIndex        =   25
                  Top             =   240
                  Width           =   9015
               End
            End
         End
      End
      Begin VB.Frame FraRicFile 
         Caption         =   "Ricerca File "
         ForeColor       =   &H8000000D&
         Height          =   3390
         Left            =   -74880
         TabIndex        =   12
         Top             =   30
         Width           =   7995
         Begin MSComctlLib.ProgressBar ProgressBar3 
            Height          =   165
            Left            =   90
            TabIndex        =   55
            Top             =   960
            Visible         =   0   'False
            Width           =   1395
            _ExtentX        =   2461
            _ExtentY        =   291
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
         Begin MSComctlLib.ProgressBar ProgressBar2 
            Height          =   165
            Left            =   90
            TabIndex        =   54
            Top             =   780
            Visible         =   0   'False
            Width           =   1395
            _ExtentX        =   2461
            _ExtentY        =   291
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
         Begin VB.ComboBox CboRicFile 
            Height          =   315
            ItemData        =   "frmIncapsVer.frx":03D3
            Left            =   90
            List            =   "frmIncapsVer.frx":03EF
            TabIndex        =   27
            Text            =   "Ricerca File"
            Top             =   210
            Width           =   1425
         End
         Begin VB.Frame Frame9 
            Height          =   555
            Left            =   5910
            TabIndex        =   20
            Top             =   2685
            Width           =   1890
            Begin VB.CommandButton cmdEdit 
               Caption         =   "Edita Listato "
               Height          =   315
               Left            =   480
               TabIndex        =   21
               Top             =   165
               Width           =   1305
            End
            Begin VB.Image Image1 
               Height          =   300
               Left            =   90
               Picture         =   "frmIncapsVer.frx":042B
               Stretch         =   -1  'True
               Top             =   150
               Width           =   300
            End
         End
         Begin VB.CommandButton Command2 
            Height          =   390
            Left            =   5910
            TabIndex        =   19
            Top             =   1020
            Width           =   1875
         End
         Begin VB.CommandButton Command5 
            Height          =   390
            Left            =   5910
            TabIndex        =   18
            Top             =   2295
            Width           =   1875
         End
         Begin VB.CommandButton Command4 
            Height          =   390
            Left            =   5910
            TabIndex        =   17
            Top             =   1875
            Width           =   1875
         End
         Begin VB.CommandButton Command3 
            Height          =   390
            Left            =   5910
            TabIndex        =   16
            Top             =   1440
            Width           =   1875
         End
         Begin VB.CommandButton cmdElimCBL 
            Caption         =   "Elimina File CBL"
            Height          =   390
            Left            =   5910
            TabIndex        =   15
            Top             =   585
            Width           =   1875
         End
         Begin VB.CommandButton cmdInsPSB 
            Caption         =   "Inserisci PSB In Progetto"
            Height          =   390
            Left            =   5910
            TabIndex        =   14
            Top             =   165
            Width           =   1875
         End
         Begin VB.ListBox Lstfoundfiles 
            Height          =   2790
            Left            =   1560
            TabIndex        =   13
            Top             =   210
            Width           =   4245
         End
         Begin VB.Label Label10 
            Height          =   315
            Left            =   300
            TabIndex        =   52
            Top             =   2820
            Width           =   945
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Opzioni Di Correzione Dell'errore :"
         ForeColor       =   &H8000000D&
         Height          =   3135
         Left            =   60
         TabIndex        =   3
         Top             =   30
         Width           =   3945
         Begin MSComctlLib.ListView LswPSBDip 
            Height          =   1635
            Left            =   135
            TabIndex        =   79
            Top             =   1350
            Width           =   3630
            _ExtentX        =   6403
            _ExtentY        =   2884
            View            =   3
            Sorted          =   -1  'True
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "ID"
               Text            =   "ID"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "Nome"
               Text            =   "Nome"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "ch"
               Text            =   "CH"
               Object.Width           =   2471
            EndProperty
         End
         Begin VB.ListBox ListaPSB 
            Height          =   1620
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   5
            Top             =   1380
            Width           =   3675
         End
         Begin VB.Frame Frame4 
            Height          =   885
            Left            =   120
            TabIndex        =   4
            Top             =   180
            Width           =   3675
            Begin VB.OptionButton OptDipendenze 
               Caption         =   "Dipendenze del PSB"
               ForeColor       =   &H8000000D&
               Height          =   255
               Left            =   120
               TabIndex        =   78
               Top             =   540
               Width           =   1815
            End
            Begin VB.OptionButton ChkAggPSB 
               Caption         =   "Inserimento PSB Manuale"
               ForeColor       =   &H8000000D&
               Height          =   255
               Left            =   120
               TabIndex        =   77
               Top             =   210
               Width           =   2235
            End
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Lista PSB:"
            ForeColor       =   &H8000000D&
            Height          =   195
            Left            =   150
            TabIndex        =   6
            Top             =   1140
            Width           =   735
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   15270
      _ExtentX        =   26935
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   20
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Index"
            Object.ToolTipText     =   "Avvia Verifica"
            ImageIndex      =   9
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Edit"
            Object.ToolTipText     =   "Edit Testo"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Help"
            Object.ToolTipText     =   "Help"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Generazione"
            Object.ToolTipText     =   "Genera Routine"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Strumenti"
            Object.ToolTipText     =   "Strumenti"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Verifica"
            Object.ToolTipText     =   "Esegui Nuova Verifica"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Stampe"
            Object.ToolTipText     =   "Stampe di riepilogo"
            ImageIndex      =   10
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   4
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PSBErr"
                  Text            =   "Stampa lista PSB-Error"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "DBDM"
                  Text            =   "Stampa lista DBD mancanti"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "seg"
                  Text            =   "Stampa Segmenti non riconosciuti"
               EndProperty
               BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "LOGElim"
                  Text            =   "Elimina File LOG di stampa"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Analizzatore"
            Object.ToolTipText     =   "Analizzatore Semiassistito"
            ImageIndex      =   14
         EndProperty
         BeginProperty Button17 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button18 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Close"
            Object.ToolTipText     =   "Chiudi"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button19 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button20 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ALL"
            Object.ToolTipText     =   "Tutti i file CBL"
            ImageIndex      =   9
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Errori Riscontrati Dalla Verifica :"
      ForeColor       =   &H8000000D&
      Height          =   2595
      Left            =   0
      TabIndex        =   0
      Top             =   420
      Width           =   11895
      Begin VB.Frame Frame17 
         Caption         =   "Processi"
         ForeColor       =   &H00C00000&
         Height          =   2265
         Left            =   9000
         TabIndex        =   43
         Top             =   180
         Width           =   2775
         Begin VB.Frame Frame19 
            Height          =   555
            Left            =   150
            TabIndex        =   49
            Top             =   1620
            Width           =   2505
            Begin MSComctlLib.ProgressBar ProgTot 
               Height          =   225
               Left            =   120
               TabIndex        =   50
               Top             =   270
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   397
               _Version        =   393216
               BorderStyle     =   1
               Appearance      =   1
               Max             =   1000
               Scrolling       =   1
            End
            Begin VB.Image ImgAnTot 
               BorderStyle     =   1  'Fixed Single
               Height          =   225
               Left            =   2160
               Stretch         =   -1  'True
               Top             =   270
               Width           =   225
            End
            Begin VB.Label Label9 
               Caption         =   "Analisi Totale :"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   225
               Left            =   120
               TabIndex        =   51
               Top             =   120
               Width           =   1815
            End
         End
         Begin VB.Frame Frame18 
            Height          =   1425
            Left            =   150
            TabIndex        =   44
            Top             =   180
            Width           =   2505
            Begin MSComctlLib.ProgressBar ProgPSB 
               Height          =   225
               Left            =   120
               TabIndex        =   45
               Top             =   510
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   397
               _Version        =   393216
               BorderStyle     =   1
               Appearance      =   1
               Min             =   1
               Max             =   1000
               Scrolling       =   1
            End
            Begin MSComctlLib.ProgressBar ProgIstr 
               Height          =   225
               Left            =   120
               TabIndex        =   46
               Top             =   1110
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   397
               _Version        =   393216
               BorderStyle     =   1
               Appearance      =   1
               Min             =   1
               Max             =   1000
               Scrolling       =   1
            End
            Begin VB.Image ImgAnIstr 
               BorderStyle     =   1  'Fixed Single
               Height          =   225
               Left            =   2160
               Stretch         =   -1  'True
               Top             =   1110
               Width           =   225
            End
            Begin VB.Image ImgAnPSB 
               BorderStyle     =   1  'Fixed Single
               Height          =   225
               Left            =   2160
               Stretch         =   -1  'True
               Top             =   510
               Width           =   225
            End
            Begin VB.Label label34 
               Caption         =   "Analisi CBL con PSB non associato :"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   405
               Left            =   120
               TabIndex        =   48
               Top             =   180
               Width           =   1455
            End
            Begin VB.Label Label8 
               Caption         =   "Analisi CBL con istruzioni non decodificate :"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   345
               Left            =   120
               TabIndex        =   47
               Top             =   780
               Width           =   1845
            End
         End
      End
      Begin VB.Frame fraError 
         Caption         =   "Lista Errori"
         ForeColor       =   &H00C00000&
         Height          =   2265
         Left            =   3900
         TabIndex        =   31
         Top             =   180
         Width           =   5055
         Begin MSComctlLib.ListView lswOggetti 
            Height          =   1875
            Left            =   150
            TabIndex        =   81
            Top             =   240
            Width           =   2655
            _ExtentX        =   4683
            _ExtentY        =   3307
            View            =   3
            MultiSelect     =   -1  'True
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            Icons           =   "imlToolbarIcons"
            SmallIcons      =   "imlToolbarIcons"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "Nogg"
               Text            =   "Nome Oggetto"
               Object.Width           =   4586
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Indice"
               Object.Width           =   882
            EndProperty
         End
         Begin VB.Frame Frame20 
            Caption         =   "Totale Oggetti con Errore"
            ForeColor       =   &H8000000D&
            Height          =   1185
            Left            =   2880
            TabIndex        =   56
            Top             =   900
            Width           =   2085
            Begin VB.CommandButton cmdTotErr 
               Caption         =   "Totale "
               Height          =   345
               Left            =   120
               TabIndex        =   58
               Top             =   750
               Width           =   1845
            End
            Begin VB.Label Label11 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               Caption         =   "Lista : 9999 -> Ver: 9999"
               ForeColor       =   &H8000000D&
               Height          =   195
               Left            =   150
               TabIndex        =   57
               Top             =   330
               Width           =   1815
            End
         End
         Begin VB.Frame Frame2 
            Height          =   615
            Left            =   2880
            TabIndex        =   32
            Top             =   180
            Width           =   2085
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               ForeColor       =   &H8000000D&
               Height          =   195
               Left            =   120
               TabIndex        =   33
               Top             =   240
               Width           =   45
            End
         End
      End
      Begin VB.Frame Fram 
         Height          =   2265
         Left            =   90
         TabIndex        =   29
         Top             =   180
         Width           =   3765
         Begin VB.OptionButton OptDBDErr 
            Caption         =   "Programmi senza DBD associato"
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000D&
            Height          =   255
            Left            =   120
            TabIndex        =   99
            Top             =   1890
            Width           =   2535
         End
         Begin VB.OptionButton OptSegm 
            Caption         =   "Segmenti non riconosciuti"
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000D&
            Height          =   255
            Left            =   135
            TabIndex        =   88
            Top             =   1515
            Width           =   2085
         End
         Begin VB.OptionButton OptDBDM 
            Caption         =   "DBD mancanti"
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000D&
            Height          =   255
            Left            =   135
            TabIndex        =   86
            Top             =   1170
            Width           =   2085
         End
         Begin VB.OptionButton OptIstrND 
            Caption         =   "Istruzioni non decodificate"
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000D&
            Height          =   255
            Left            =   135
            TabIndex        =   84
            Top             =   825
            Width           =   2085
         End
         Begin VB.OptionButton OptNoPSB 
            Caption         =   "CBL con PSB non associato"
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000D&
            Height          =   255
            Left            =   135
            TabIndex        =   82
            Top             =   480
            Width           =   2115
         End
         Begin VB.Image ImgSegm 
            Height          =   315
            Left            =   3135
            Stretch         =   -1  'True
            Top             =   1485
            Width           =   315
         End
         Begin VB.Label Label17 
            Caption         =   "STATUS :"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   2325
            TabIndex        =   89
            Top             =   1545
            Width           =   795
         End
         Begin VB.Image ImgDBD 
            Height          =   315
            Left            =   3135
            Stretch         =   -1  'True
            Top             =   1140
            Width           =   315
         End
         Begin VB.Label Label6 
            Caption         =   "STATUS :"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   2325
            TabIndex        =   87
            Top             =   1200
            Width           =   795
         End
         Begin VB.Image ImgIstr 
            Height          =   315
            Left            =   3135
            Stretch         =   -1  'True
            Top             =   795
            Width           =   315
         End
         Begin VB.Label Label3 
            Caption         =   "STATUS :"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   2325
            TabIndex        =   85
            Top             =   855
            Width           =   795
         End
         Begin VB.Image ImgNoPSB 
            Height          =   315
            Left            =   3135
            Stretch         =   -1  'True
            Top             =   450
            Width           =   315
         End
         Begin VB.Label Label2 
            Caption         =   "STATUS :"
            ForeColor       =   &H8000000D&
            Height          =   225
            Left            =   2325
            TabIndex        =   83
            Top             =   510
            Width           =   795
         End
         Begin VB.Label Label7 
            Caption         =   "Scelte di Visualizzazione Errori :"
            ForeColor       =   &H00FF0000&
            Height          =   255
            Left            =   180
            TabIndex        =   30
            Top             =   180
            Width           =   2325
         End
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   2070
      Top             =   7530
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   19
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":086D
            Key             =   "Index"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":0A47
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":1CCB
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":211F
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":2231
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":240B
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":272F
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":2B83
            Key             =   "Chiudi_2"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":2CDF
            Key             =   "Lampo"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":2E3B
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":2F97
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":33EB
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":3553
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":39AB
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":3B07
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":3C6F
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":4203
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":4657
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmIncapsVer.frx":4AAB
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTDbdManc 
      Height          =   615
      Left            =   8610
      TabIndex        =   73
      Top             =   7500
      Width           =   825
      _ExtentX        =   1455
      _ExtentY        =   1085
      _Version        =   393217
      TextRTF         =   $"frmIncapsVer.frx":4EFF
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTSegNonRic 
      Height          =   345
      Left            =   7605
      TabIndex        =   98
      Top             =   8280
      Width           =   1545
      _ExtentX        =   2725
      _ExtentY        =   609
      _Version        =   393217
      TextRTF         =   $"frmIncapsVer.frx":5034
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmIncapsVer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim searchflag As Boolean

Private Sub CboRicFile_Click()
    Toolbar1.Buttons(4).Enabled = False
    Lstfoundfiles.Clear
    
    'IMPOSTA I VALORI ENABLED DEI VARI BOTTONI DELLA FORM
    cmdEdit.Enabled = False
    cmdElimCBL.Enabled = False
    cmdInsPSB.Enabled = False
    
    FraRicFile.ForeColor = vbRed
    FraRicFile.Caption = "Ricerca File In Corso, Attendere..."
    
    ProgressBar1.Visible = True
    ProgressBar2.Visible = True
    ProgressBar3.Visible = True
    
    Select Case CboRicFile.List(CboRicFile.ListIndex)
        Case "*.PSB"
            Call RICERCA_FILE("*.psb")
            cmdInsPSB.Enabled = True
        Case "*.CBL"
            Call RICERCA_FILE("*.cbl")
            cmdElimCBL.Enabled = True
        Case "*.BMS"
            Call RICERCA_FILE("*.bms")
        Case "*.DBD"
            Call RICERCA_FILE("*.dbd")
        Case "*.JCL"
            Call RICERCA_FILE("*.jcl")
        Case "*.CPY"
            Call RICERCA_FILE("*.cpy")
        Case "*.PRC"
            Call RICERCA_FILE("*.prc")
        Case "*.PRM"
            Call RICERCA_FILE("*.prm")
    End Select
    
    FraRicFile.ForeColor = &H8000000D
    FraRicFile.Caption = "Ricerca File "
    
    If Lstfoundfiles.List(1) <> "" Then
        Toolbar1.Buttons(4).Enabled = True
        cmdEdit.Enabled = False
    End If
    
    WaitTime (1)
    ProgressBar1.Visible = False
    ProgressBar2.Visible = False
    ProgressBar3.Visible = False
    ProgressBar1.Value = 0
    ProgressBar2.Value = 0
    ProgressBar3.Value = 0
    
End Sub

Private Sub ChkAggPSB_Click()
'CARICA LA LISTA PSB CON I PSB E SETTA GLI ENABLED DEI VARI BOTTONI
    Dim rs_PSB As Recordset
    Dim i As Variant
    Label4.Visible = True
    LswPSBDip.Visible = False
    ListaPSB.Visible = True
    
    ListaPSB.Clear
    
    Set rs_PSB = DbPrj.OpenRecordset("SELECT * FROM Oggetti WHERE Tipo = 'PSB'")
    rs_PSB.MoveLast
    rs_PSB.MoveFirst

    For i = 1 To rs_PSB.RecordCount
        ListaPSB.AddItem (rs_PSB!nome)
        rs_PSB.MoveNext
    Next
    
    
    If ChkAggPSB.Value = False Then
        ListaPSB.Clear
    End If
    
       
    If ChkAggPSB.Value = True Then
        InfoFlag = 1
    Else
        InfoFlag = 0
    End If
    
    
    Label4.Caption = "Lista PSB del progetto:"
End Sub

Private Sub cmdALL_Click()
     
End Sub

Private Sub cmdAvvio_Click()
        'controlla le associazioni fra le due liste di PSB
        Dim i As Long
        Dim j As Long
        
        Dim NomePSB1 As String
        
        For i = 0 To lstPSBAssSegm.listcount - 1
              NomePSB1 = lstPSBAssSegm.List(i)
              
              For j = 1 To lstPSBDBD.ListItems.Count
                        If NomePSB1 = lstPSBDBD.ListItems(j) Then
                                lstPSBDBD.ListItems(j).ForeColor = vbBlue
                                lstPSBDBD.ListItems(j).Bold = True
                        End If
              Next j
        Next i
End Sub

Private Sub cmdEdit_Click()
    If SSTabError.Tab = 4 Then
        NomeEDitor = Lstfoundfiles.List(Lstfoundfiles.ListIndex)
    End If
    
    frmIncapsEdit.Show
End Sub

Private Sub cmdElimCBL_Click()
    'AGGIUNGERE IL CONTROLLO CHE MI FERMA SE NON HO SELEZIONATO NIENTE
    Call SPOSTA_IN_CESTINO(Lstfoundfiles.List(Lstfoundfiles.ListIndex))
End Sub

Private Sub cmdInsPSB_Click()
    Dim File As String
    
    Dim n, i As Long
    
    percorsoPSB = Lstfoundfiles.List(Lstfoundfiles.ListIndex)
    
    n = InStr(Len(percorsoPSB) - 15, percorsoPSB, "\")
    File = Mid(percorsoPSB, n + 1, 15)
    
    'prende il nome del file e si sposta nella finestra di inserimento del psb
    SSTabError.Tab = 0
    ChkAggPSB.Value = 1
    ChkAggPSB_Click
    For i = 0 To ListaPSB.listcount
        If ListaPSB.List(i) = File Then
            ListaPSB.Selected(i) = True
            Exit For
        End If
    Next
    
    ListaPSB.Enabled = False
    
End Sub

Private Sub cmdTotErr_Click()
    If OGGLista - OGG_Count <= OGG_V_ERR Then
        Label11.ForeColor = &H8000000D
        Label11.Caption = "Lista: " & OGGLista - OGG_Count & " <= Ver: " & OGG_V_ERR
    Else
        Label11.ForeColor = vbRed
        Label11.Caption = "Lista: " & OGGLista - OGG_Count & " => Ver: " & OGG_V_ERR
    End If
End Sub

Private Sub Form_Activate()
    Toolbar1.Buttons(4).Enabled = False
    Toolbar1.Buttons(16).Enabled = False
    Controlla_OPT_Attivi
End Sub

Private Sub Lstfoundfiles_LostFocus()
    Toolbar1.Buttons(4).Enabled = False
End Sub

Private Sub LstPsbManc_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim r As Recordset
    Dim nome As String
    Dim Id As Long
    
    Id = Item
    
    Set r = DbPrj.OpenRecordset("SELECT * FROM Oggetti WHERE IdOggetto = " & Id)
    
    If r.RecordCount = 1 Then
       Call MsgBox("Il PSB mancante selezionato � utilizzato dal Pogramma:" & vbCrLf & "---------------------------------------------------------------------------------------" & vbCrLf & "ID : " & Id & Space(5) & " Nome : " & r!nome & vbCrLf & "---------------------------------------------------------------------------------------", vbInformation, "PSB Info - DLI2RDBMS Client")
    End If
End Sub

Private Sub lswOggetti_GotFocus()
    Toolbar1.Buttons(4).Enabled = True
    Toolbar1.Buttons(16).Enabled = True
End Sub

Private Sub lswOggetti_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim K As Long
    
    Dim nome As String
    
    nome = Item
    If Item = "# NOTHING #" Then Exit Sub
        ListaOggetti.ListIndex = lswOggetti.SelectedItem.ListSubItems(1) - 1
        ListaOggetti.Selected(ListaOggetti.ListIndex) = True
End Sub

Private Sub lswOggetti_LostFocus()
    Toolbar1.Buttons(4).Enabled = False
    Toolbar1.Buttons(16).Enabled = False
End Sub

Private Sub OptDBDErr_Click()
     Call CARICA_LISTA_CBL_SENZA_DBD(lstId, ListaOggetti)
     
     OptDBDErr.Value = False
     
     CARICA_LISTVIEW_OGGETTI ListaOggetti, lswOggetti
     
     fraError.Caption = "Lista Errori file CBL con DBD non associato"
     Label1.Caption = "File CBL N� " & ListaOggetti.listcount
End Sub

Private Sub OptDBDM_Click()
        Dim numeroDBD As Long
        Dim i As Long
        Dim NDBD As String
        Dim Duplicati As Boolean
        
        CARICA_TYPE_DBD_MANCANTI
        CARICA_LISTA_DBD_MANCANTI ListaOggetti, lstId
        fraError.Caption = "Lista Errori: DBD mancanti"
        
        OptIstrND.Value = False
        OptNoPSB.Value = False
    
        For i = 0 To SSTabError.Tabs - 1
            SSTabError.TabEnabled(i) = False
        Next
        SSTabError.TabEnabled(2) = True
        
        SSTabError.Tab = 2
        
        Toolbar1.Buttons(14).Enabled = True
        
        CARICA_LISTVIEW_OGGETTI ListaOggetti, lswOggetti
        
        'controlla quanti dbd sono visualizzati senza i duplicati
        Duplicati = False
        
        For i = 1 To lswOggetti.ListItems.Count
                If lswOggetti.ListItems(i) <> NDBD Then
                        numeroDBD = numeroDBD + 1
                Else
                        Duplicati = True
                End If
                NDBD = lswOggetti.ListItems(i)
        Next i
        
        If Duplicati = True Then
                Label1.Caption = "N� DBD: " & numeroDBD & " (DBD Duplicati)"
        Else
                Label1.Caption = "N� DBD: " & numeroDBD
        End If
        
        frmIncapsVer.Caption = "Verifica sulla struttura del DLI"
        
        Controlla_OPT_Attivi

End Sub

Private Sub OptDipendenze_Click()
    LswPSBDip.Visible = True
    ListaPSB.Visible = False
    Label4.Visible = True
    ListaPSB.Clear
    Label4.Caption = "Relazioni PSB"
    
    CONTROLLA_DIPENDENZE_SULLA_LISTA lswOggetti, LswPSBDip
End Sub


Private Sub OptIstrND_click()
    Call CARICA_LISTA_CBL_ISTR_NON_DECODE(lstId, ListaOggetti)
    fraError.Caption = "Lista Errori file CBL con istruzioni non decodificate"
    Label1.Caption = "File CBL N� " & ListaOggetti.listcount
    
    SSTabError.Tab = 1
    OptNoPSB.Value = False
    OptDBDM.Value = False
    
    Dim i As Long
    
    'indica la percentuale di programmi che hanno istruzioni non decode perch�
    'non hanno il psb associato.
    Dim percentuale As Long
    Dim NumPRG As Long
    
    For i = 0 To SSTabError.Tabs - 1
        SSTabError.TabEnabled(i) = False
    Next
    SSTabError.TabEnabled(1) = True
    
    Toolbar1.Buttons(14).Enabled = True
    
    CARICA_LISTVIEW_OGGETTI ListaOggetti, lswOggetti
    
    percentuale = METCHA_ISTRUZIONI_NON_DEC_CON_CBL_NO_PSB_ASS(lswOggetti)
    
    NumPRG = lswOggetti.ListItems.Count - (lswOggetti.ListItems.Count * percentuale) / 100
    
    frmIncapsVer.Caption = "Verifica sulla struttura del DLI - - - Programmi con Istruzioni Non Codificate (escluse quelle dipendenti dal PSB non associato) " & 100 - percentuale & " % - - - " & NumPRG & " su " & lswOggetti.ListItems.Count & " programmi..."

     Controlla_OPT_Attivi
End Sub

Private Sub optNoPSB_Click()
    Call CARICA_LISTA_CBL_NO_PSB(lstId, ListaOggetti)
    fraError.Caption = "Lista Errori file CBL con PSB non associato"
    Label1.Caption = "File CBL N� " & ListaOggetti.listcount
    
    SSTabError.Tab = 0
    
    Dim i As Long
    
    For i = 0 To SSTabError.Tabs - 1
        SSTabError.TabEnabled(i) = False
    Next
    
    SSTabError.TabEnabled(0) = True
    OptIstrND.Value = False
    OptDBDM.Value = False
    
    Toolbar1.Buttons(14).Enabled = True
    
    CARICA_TREE_VIEW_PSB TreeVPSB
    
    CARICA_LISTVIEW_OGGETTI ListaOggetti, lswOggetti
    
    frmIncapsVer.Caption = "Verifica sulla struttura del DLI"
    
    Controlla_OPT_Attivi
End Sub

Private Sub CmdOkElim_Click()
    'CODICE PER ELIMINARE UN CBL
End Sub


Private Sub ListaErrori_LostFocus()
    Toolbar1.Buttons(4).Enabled = False
End Sub

Private Sub listaIdOgg_LostFocus()
    Toolbar1.Buttons(4).Enabled = False
End Sub


Private Sub cmdOkAss_Click()
  Dim wIdCbl As Variant
  Dim wIdPsb As Variant
  Dim tb As Recordset
  
  Set tb = DbPrj.OpenRecordset("select * from oggetti where tipo = 'CBL' and nome = '" & txtCBL & "'")
  wIdCbl = tb!IdOggetto
  Set tb = DbPrj.OpenRecordset("select * from oggetti where tipo = 'PSB' and nome = '" & txtPSB & "'")
  wIdPsb = tb!IdOggetto
  Set tb = DbPrj.OpenRecordset("relazioni")
  tb.AddNew
  tb!IdOggetto = wIdCbl
  tb!IdOggRich = wIdPsb
  tb!tiporela = "PSB"
  tb.Update
  Set tb = DbPrj.OpenRecordset("select * from segnalazioni where idoggetto = " & wIdCbl & " and codice = 'VIE00001'")
  If tb.RecordCount > 0 Then
     tb.Delete
  End If
  Set tb = DbPrj.OpenRecordset("select * from segnalazioni where idoggetto = " & wIdCbl & " and codice = 'PRS00004'")
  If tb.RecordCount > 0 Then
     tb.Delete
  End If
  tb.Close
  
End Sub

Private Sub ListaOggetti_Click()
    Dim r As Recordset
    Dim rPsb As Recordset
    
    Dim IdOb As Variant
    Dim NumType As Variant
    
    Dim OldNomePSB As String
    
    Set r = DbPrj.OpenRecordset("SELECT * FROM Oggetti WHERE Nome = '" & Trim(ListaOggetti.List(ListaOggetti.ListIndex)) & "'")
    
    If r.RecordCount > 0 Then
        r.MoveLast
        r.MoveFirst
        
        IdOb = r!IdOggetto
    End If
    r.Close
    
    'riempie la lista dei psb con le dipendenze
    If OptDipendenze.Value = True Then
        NumType = CARICA_TYPE_DIPENDENZE_DEI_PSB(IdOb)
        
        If NumType > 0 Then
            CARICA_LISTVIEW_DIPENDENZE_PSB LswPSBDip, NumType
        Else
            CARICA_LISTVIEW_DIPENDENZE_PSB LswPSBDip, "NOTHING"
        End If
    End If
    
    If SSTabError.Tab <> 2 Then
        lstId.ListIndex = ListaOggetti.ListIndex
        LstInstrERR.Clear
        lblInstrErr.Caption = ""
        
        If SSTabError.Tab = 0 Then
            txtCBL.text = ListaOggetti.List(ListaOggetti.ListIndex)
        End If
        
        If SSTabError.Tab = 1 Then
            lblInstrErr = MOSTRA_ISTRUZIONI_NON_DECODE(lstId, LstInstrERR)
        End If
    Else
        If SSTabError.Tab = 2 Then
            
            TxtDBD = DBD_MANCANTI(ListaOggetti.ListIndex).NomeDbD
            txtID = DBD_MANCANTI(ListaOggetti.ListIndex).idOgg
            txtNomeOGG = DBD_MANCANTI(ListaOggetti.ListIndex).NomeOgg
            txtTipo = DBD_MANCANTI(ListaOggetti.ListIndex).TipoOgg
            txtDir = DBD_MANCANTI(ListaOggetti.ListIndex).Directory
        End If
    End If
    
    lswSegm.ListItems.Clear
    If OptSegm.Value = True Then
        'carica la lista dei segmenti mancanti
        Set r = DbPrj.OpenRecordset("SELECT * FROM Segnalazioni WHERE Codice = 'VIE00004' AND IdOggetto = " & IdOb)
        
        If r.RecordCount > 0 Then
                r.MoveLast
                r.MoveFirst
                
                While Not r.EOF
                        lswSegm.ListItems.Add , , Trim(r!var1), , 17
                        r.MoveNext
                Wend
        End If
        lswSegm.Refresh
        r.Close
        
        'controlla il nome del PSB associato all'idoggetto selezionato
        lstPSBAssSegm.Clear
        Set r = DbPrj.OpenRecordset("SELECT DISTINCT idoggrich FROM Relazioni WHERE idoggetto = " & IdOb & " AND TipoRela = 'PSB'")
        
        If r.RecordCount > 0 Then
                r.MoveLast
                r.MoveFirst
                
                While Not r.EOF
                        Set rPsb = DbPrj.OpenRecordset("SELECT * FROM oggetti WHERE idoggetto = " & r!IdOggRich)
                        'riempie la lista
                        lstPSBAssSegm.AddItem Trim(rPsb!nome)
                        rPsb.Close
                        r.MoveNext
                Wend
        End If
        r.Close
        
        'controlla i psb associati ai dbd mancanti
        Set r = DbPrj.OpenRecordset("SELECT * FROM Segnalazioni WHERE Codice = 'PRS00006'")
       lstPSBDBD.ListItems.Clear
       If r.RecordCount > 0 Then
                r.MoveLast
                r.MoveFirst
                
                While Not r.EOF
                        
                        Set rPsb = DbPrj.OpenRecordset("SELECT * FROM oggetti WHERE idoggetto = " & r!IdOggetto)
                        
                        'riempie la lista
                        If Trim(rPsb!Tipo) = "PSB" Then
                                If OldNomePSB <> Trim(rPsb!nome) Then
                                        lstPSBDBD.ListItems.Add , , Trim(rPsb!nome), , 18
                                End If
                                OldNomePSB = Trim(rPsb!nome)
                        End If
                        
                        r.MoveNext
                Wend
        End If
    End If
End Sub

Private Sub ListaPSB_Click()
    'Toolbar1.Buttons(4).Enabled = True
    switch = False
    
    Dim rs As Recordset
    
    Set rs = DbPrj.OpenRecordset("SELECT * FROM Oggetti WHERE Nome = '" & ListaPSB.List(ListaPSB.ListIndex) & "'")
    
    wDirPSB = rs!Directory
    
    
    txtPSB.text = ListaPSB.List(ListaPSB.ListIndex)
End Sub

Private Sub ListaPSB_LostFocus()
    Toolbar1.Buttons(4).Enabled = False
End Sub

Private Sub Lstfoundfiles_Click()
    cmdEdit.Enabled = True
    Toolbar1.Buttons(4).Enabled = False
End Sub

Private Sub LstInstrERR_Click()
    'PRENDE LA STRINGA SELEZIONATA ED ESTRAPOLA I VARI DATI DELLISTRUZIONE
    Dim S As String
    Dim Id As String
    Dim gPos, gpos1, gpos2, gpos3, gpos4, gpos5, gpos6, gpos7 As Variant
    Dim r As String
    Dim sintax As String
    Dim Inst As Variant
    
    'ASSEGNA LA STRINGA
    S = LstInstrERR.List(LstInstrERR.ListIndex)
    
    'RICAVA L'ID
    gPos = InStr(S, ":")
    gpos1 = InStr(S, ";")
    Id = Mid(S, gPos + 1, gpos1 - gPos - 1)
    
    'RICAVA LA RIGA
    gpos2 = InStr(gpos1, S, ":")
    gpos3 = InStr(gpos1 + 1, S, ";")
    r = Mid(S, gpos2 + 1, gpos3 - gpos2 - 1)
    
    'RICAVA LA SINTASSI
    gpos4 = InStr(gpos3, S, ":")
    gpos5 = InStr(gpos3 + 1, S, ";")
    sintax = Mid(S, gpos4 + 1, gpos5 - gpos4 - 1)
    
    'RICAVA L'ISTRUZIONE
    gpos6 = InStr(gpos5, S, ":")
    gpos7 = InStr(gpos5 + 1, S, ";")
    Inst = Mid(S, gpos6 + 1, gpos7 - gpos6 - 1)
    
    'MOSTRA FORM E ASSEGNA LE VARIABILI ALLE TEXT
    frmIncapsErr.Show
    
    frmIncapsErr.txtInstr.text = Inst
    frmIncapsErr.txtID.text = Id
    frmIncapsErr.txtRIga = r
    frmIncapsErr.txtSintax = sintax
    
End Sub

Private Sub OptSegm_Click()
        Dim i As Long
        
        For i = 0 To SSTabError.Tabs - 1
                SSTabError.TabEnabled(i) = False
        Next
        
        SSTabError.Tab = 3
        
        SSTabError.TabEnabled(3) = True
        
        SSTabError.TabEnabled(0) = False
        
        Toolbar1.Buttons(14).Enabled = True
        
        CARICA_LISTA_SEGMENTI_NON_RICONOSCIUTI lstId, ListaOggetti
        
        CARICA_LISTVIEW_OGGETTI ListaOggetti, lswOggetti
        
        Label1.Caption = "N� DBD: " & lswOggetti.ListItems.Count
        
        Controlla_OPT_Attivi
End Sub

Private Sub SSTabError_Click(PreviousTab As Integer)
    'IMPOSTAZIONI DI DEFAULT IN BASE AL TAB CLICCATO
    Dim i As Long
    Dim indx As Integer
    
    If PreviousTab + 1 = 5 Then
        Lstfoundfiles.Clear
        cmdInsPSB.Enabled = False
        cmdElimCBL.Enabled = False
        cmdEdit.Enabled = False
        Toolbar1.Buttons(14).Enabled = False
    End If
    
    For i = 0 To SSTabError.Tab
        If SSTabError.TabEnabled(i) = True Then
            indx = SSTabError.TabIndex
            Exit For
        End If
    Next
    
    For i = 0 To SSTabError.Tab
        SSTabError.TabEnabled(i) = False
    Next
    
    SSTabError.TabEnabled(indx) = True
    
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim NomeEd As String
    Dim edtd As String
    Dim i As Integer
    
    
    On Error Resume Next
    Select Case Button.key
        Case "Index"
            
        Case "Edit"
            
            NomeEDitor = PathPrj & "\cbl\" & ListaOggetti.List(ListaOggetti.ListIndex)
            
            frmIncapsEdit.Show
            
        Case "Help"
            'help da fare
        
        Case "Generazione"
            Unload Me
            frmIncaps.Resize
        
        Case "Close"
            Unload Me
            
        Case "Strumenti"
            SSTabError.Tab = 4
            For i = 0 To SSTabError.Tab
                SSTabError.TabEnabled(i) = False
            Next
    
            SSTabError.TabEnabled(4) = True
            Toolbar1.Buttons(14).Enabled = False
            
            OptDBDM.Value = False
            OptIstrND.Value = False
            OptNoPSB.Value = False
            
            ListaOggetti.Clear
            
        Case "Verifica"
            AVVIA_VERIFICA
            CARICA_TYPE_PSB_NON_ASS
            CARICA_TYPE_PSB_MANCANTI
            CARICA_TYPE_DBD_MANCANTI
            CALCOLA_NUMERO_ERRORI
        
        Case "Analizzatore"
            If lswOggetti.ListItems(1).text = "# NOTHING #" Then
                Exit Sub
            Else
                frmIncapsAUTO.Show vbModal
            End If
            If OptNoPSB Then
               optNoPSB_Click
            End If
            If OptIstrND Then
               OptIstrND_click
            End If
            
          Case "ALL"
               'carica nella lista tutti i CBL
                Dim r As Recordset
     
               Screen.MousePointer = vbHourglass
               
               Set r = DbPrj.OpenRecordset("SELECT * FROM Oggetti WHERE Tipo = 'CBL'")
               
               lstId.Clear
               ListaOggetti.Clear
               
               If r.RecordCount > 0 Then
                    r.MoveFirst
                    
                    While Not r.EOF
                         lstId.AddItem r!IdOggetto
                         ListaOggetti.AddItem r!nome
                         r.MoveNext
                    Wend
               End If
               
               CARICA_LISTVIEW_OGGETTI ListaOggetti, lswOggetti
               
               fraError.Caption = "Lista completa file CBL"
               Label1.Caption = "File CBL N� " & ListaOggetti.listcount
               Screen.MousePointer = vbDefault
    End Select
    
    Controlla_OPT_Attivi
End Sub

Sub Resize()
    On Error Resume Next
    Me.Move GbLeft, GbTop, GbWidth, GbHeight
End Sub

Private Sub Form_Load()
    Resize
    
    ProgIstr.Value = 1
    ProgPSB.Value = 1
    ProgTot.Value = 1
    
    Dim i As Long
    
    For i = 0 To SSTabError.Tabs - 2
        SSTabError.TabEnabled(i) = False
    Next
    
    Toolbar1.Buttons(4).Enabled = False
    Toolbar1.Buttons(14).Enabled = False
    
    Label11.Caption = ""
    
    LswPSBDip.Visible = False
    ListaPSB.Visible = False
    Label4.Visible = False
    
    'carica le variabili dei psb mancanti fisicamente
    CARICA_TYPE_PSB_MANCANTI
    CARICA_TYPE_PSB_NON_ASS
    CARICA_TYPE_DBD_MANCANTI
End Sub

Private Sub Form_Unload(Cancel As Integer)
    GbFinVerif = False
    IncaSwitch = False
End Sub


'*********************************************************************
'*********** ROUTINE PER LA RICERCA DI FILE SU TUTTO C:\  ************
'*********************************************************************

Private Sub RICERCA_FILE(pattern As String)
' Inizializza la ricerca e quindi esegue una ricerca ricorsiva.
Dim FirstPath As String, DirCount As Integer, NumFiles As Integer
Dim result As Integer

    Lstfoundfiles.Clear
    ' Aggiorna dirList.Path se � diverso dalla directory corrente
    ' selezionata, altrimenti esegue la ricerca.
    If dirList.path <> dirList.List(dirList.ListIndex) Then
        dirList.path = dirList.List(dirList.ListIndex)
        Exit Sub         ' Esce in modo che gli utenti possano esminare il risultato
                         ' dell'operazione prima di eseguire la ricerca.
    End If

    ' Continua la ricerca.
    filList.pattern = pattern
    FirstPath = dirList.path
    DirCount = dirList.listcount

    ' Avvia la ricerca ricorsiva nella directory.
    NumFiles = 0 ' Reimposta l'indicatore dei file trovati.
    
    ProgressBar1.Value = 0
    ProgressBar2.Value = 0
    ProgressBar3.Value = 0
    
    result = DirDiver(FirstPath, DirCount, "")
    filList.path = dirList.path
    
End Sub

Private Function DirDiver(NewPath As String, DirCount As Integer, BackUp As String) As Long
'  Esegue la ricerca in modo ricorsivo nelle directory
'  successive a NewPath nella struttura. NewPath viene
'  cercato in questa ricorsione. BackUp � l'origine della
'  ricorsione. DirCount � il numero di sottodirectory in
'  questa directory.
Static FirstErr As Long
Dim DirsToPeek As Long, AbandonSearch As Long, Ind As Long
Dim OldPath As String, ThePath As String, entry As String
Dim retval As Long
    searchflag = True           ' Imposta il flag in modo che l'utente possa interrompere.
    DirDiver = False            ' Imposta su True se c'� un errore.
    retval = DoEvents()         ' Controlla gli eventi (ad esempio, se l'utente sceglie Annulla).
    If searchflag = False Then
        DirDiver = True
        Exit Function
    End If
    
    On Local Error GoTo DirDriverHandler
    DirsToPeek = dirList.listcount                  ' Numero di directory sotto la directory corrente
    Do While DirsToPeek > 0 And searchflag = True
        OldPath = dirList.path                      ' Salva il percorso precedente per la ricorsione successiva.
        dirList.path = NewPath
        If dirList.listcount > 0 Then
            ' Passa alla fine del nodo.
            dirList.path = dirList.List(DirsToPeek - 1)
            AbandonSearch = DirDiver((dirList.path), DirCount%, OldPath)
            Call PROGRESS_BAR_NO_FRAME(ProgressBar1, 800)
            Call PROGRESS_BAR_NO_FRAME(ProgressBar2, 800)
        End If
        ' Sale di un livello nella struttura delle directory.
        DirsToPeek = DirsToPeek - 1
        If AbandonSearch = True Then Exit Function
        
    Loop
    
    ' Richiama la funzione per l'enumerazione dei file.
    If filList.listcount Then
        If Len(dirList.path) <= 3 Then             ' Controlla la presenza di caratteri a 2 byte
            ThePath = dirList.path                 ' Se � il primo livello, non modifica niente...
        Else
            ThePath = dirList.path + "\"           ' Altrimenti inserisce "\" prima del nome del file.
        End If
        For Ind = 0 To filList.listcount - 1       ' Aggiunge nella casella di riepilogo i file corrispondenti in questa directory.
            entry = ThePath + filList.List(Ind)
            Lstfoundfiles.AddItem entry
            Call PROGRESS_BAR_NO_FRAME(ProgressBar3, 100)
            
        Next Ind
        
    End If
    If BackUp <> "" Then        ' Se esiste una directory principale, la sposta.
        dirList.path = BackUp
    End If
    
    Exit Function
    
DirDriverHandler:
    If ERR = 7 Then             ' Se si verifica un errore di memoria esaurita, presuppone che la casella sia piena.
        DirDiver = True         ' Crea Msg e imposta il valore di ritorno AbandonSearch.
        MsgBox "La casella di riepilogo � piena. Interruzione della ricerca..."
        Exit Function           ' Si noti che la routine di uscita reimposta Err su 0.
    Else                        ' Altrimenti visualizza un messaggio di errore ed esce.
        MsgBox ERROR
        End
    End If
End Function

Private Sub DirList_Change()
     ' Aggiorna la casella di riepilogo dei file per sincronizzarla con la casella di riepilogo delle directory.
    filList.path = dirList.path
End Sub

Private Sub DirList_LostFocus()
    dirList.path = dirList.List(dirList.ListIndex)
End Sub

Private Sub DrvList_Change()
    On Error GoTo DriveHandler
    dirList.path = drvList.Drive
    Exit Sub

DriveHandler:
    drvList.Drive = dirList.path
    Exit Sub
End Sub

'*********************************************************************************
'*********** FINE ROURINE PER LA RICERCA DEI FILE IN TUTTO C:\ *******************
'*********************************************************************************


Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    On Error GoTo ERR
    Dim scelta As Integer
    
    Select Case ButtonMenu
        Case "Stampa lista PSB-Error"
            STAMPA_SITUAZIONE_PSB RTPsbError
            'stampa la lista
            STAMPA_DA_RICHTEXTBOX RTPsbError, ComD
        
        Case "Stampa lista DBD mancanti"
            STAMPA_LISTA_DBD_MANCANTI RTDbdManc
            'stampa la lista
            STAMPA_DA_RICHTEXTBOX RTDbdManc, ComD
            
        Case "Stampa Segmenti non riconosciuti"
            STAMPA_LISTA_SEGMENTI_NON_RICONOSCIUTI RTSegNonRic
            'stampa la lista
            STAMPA_DA_RICHTEXTBOX RTSegNonRic, ComD
        
        Case "Elimina File LOG di stampa"
            scelta = MsgBox("Sei Sicuro di eliminare i file log di stampa ?", vbYesNo, "DLI2RDBMS Client")
            
            If scelta = 6 Then
                Kill (app.path & "\stampeuty\dbd*.log")
                Kill (app.path & "\stampeuty\psb*.log")
                Kill (app.path & "\stampeuty\seg*.log")
            End If
    End Select
    
Exit Sub
    
ERR:
    Exit Sub
End Sub

Private Sub TreeVPSB_DblClick()
    Dim r As Recordset
    Dim i As Long
    
    Dim Id As Variant
    Dim nome As Variant
    Dim wpox As Variant
    
    Id = TreeVPSB.SelectedItem.text
    
    wpox = InStr(1, Id, "-")
    
    If wpox <> 0 Then
        Id = Trim(Mid(Id, 4, wpox - 4))
    Else
        Exit Sub
    End If
    
    nome = Trim(Mid(TreeVPSB.SelectedItem.text, wpox + 1))
    
    
    'ricerca il programma nella lista oggetti e se c'� lo mette in evidenza
    For i = 0 To ListaOggetti.listcount - 1
        If ListaOggetti.List(i) = nome Then
            ListaOggetti.Selected(i) = True
            Exit For
        End If
    Next i
End Sub

Sub Controlla_OPT_Attivi()
'serve per permettere all'utente di cliccare l'option button anche quando ne rimane attivo uno solo
        Dim cont As Long
        
        If OptDBDM.Enabled = True Then
             cont = cont + 1
        End If
        
        If OptIstrND.Enabled = True Then
             cont = cont + 1
        End If
        
        If OptNoPSB.Enabled = True Then
             cont = cont + 1
        End If
        
        If OptSegm.Enabled = True Then
             cont = cont + 1
        End If
        
        If cont = 1 Then
               OptDBDM.Value = False
               OptIstrND.Value = False
               OptNoPSB.Value = False
               OptSegm.Value = False
        End If
End Sub

Sub CARICA_LISTA_CBL_SENZA_DBD(ListaID As ListBox, ListaCBL As ListBox)
     Dim r As Recordset
     
     Set r = DbPrj.OpenRecordset("SELECT DISTINCT IdOggetto FROM Istruzioni WHERE NomeDB = '' AND Istruzione <> 'PCB' AND Istruzione <> 'TERM'")
     
     ListaID.Clear
     ListaCBL.Clear
     
     If r.RecordCount > 0 Then
          r.MoveLast
          r.MoveFirst
          Screen.MousePointer = vbHourglass
          While Not r.EOF
               'controlla se � una COPY
               If IsCPY(r!IdOggetto) = False Then
                    ListaID.AddItem r!IdOggetto
                    ListaCBL.AddItem RESTITUISCI_NOMEOGG_DA_ID(r!IdOggetto)
               End If
               r.MoveNext
          Wend
           Screen.MousePointer = vbDefault
     End If
End Sub

Function IsCPY(idObj As Long) As Boolean
     Dim r As Recordset
     
     Set r = DbPrj.OpenRecordset("SELECT * FROM Oggetti WHERE IdOggetto = " & idObj)
     
     If r.RecordCount > 0 Then
          r.MoveFirst
          
          If Trim(r!Tipo) = "CPY" Then
               IsCPY = True
               Exit Function
          Else
               IsCPY = False
               Exit Function
          End If
     End If
End Function
