Attribute VB_Name = "MavsdM_Routine"
'''''Option Explicit
'''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''' carica rtbFile con il testo del template esplodento eventuali
'''''' TEMPLATE-TAG...
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''Public Sub cleanTags(rtbFile As RichTextBox)
'''''  Dim regExpr As Object
'''''  Dim matches As Variant, Match As Variant
'''''
'''''  Set regExpr = CreateObject("VBScript.regExp")
'''''
'''''  'regExpr.Pattern = "<[^>]*(i)>" 'altrimenti mi accorpa tutte quelle di una riga...
'''''
'''''  regExpr.Pattern = "<[^>]*\(i\)>"
'''''  regExpr.Global = True
'''''
'''''  Set matches = regExpr.Execute(rtbFile.text)
'''''
'''''  For Each Match In matches
'''''    changeTag rtbFile, Match & "", ""
'''''  Next
'''''
'''''  Exit Sub
'''''fileErr:
'''''  MsgBox err.Description
'''''End Sub
'''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''' E' un SostWord con indentazione (relativa alla posizione del TAG)
'''''' Tratta i TAG "ciclici": devono terminare con (i)...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''Public Sub changeTag(Rtb As RichTextBox, Tag As String, text As String)
'''''  Dim Posizione As Long
'''''  Dim crLfPosition As Long
'''''  Dim indentedText As String
'''''
'''''
'''''
'''''  If isCiclicTag(Tag) Then
'''''    If Len(text) Then text = text & vbCrLf & Tag 'se text="" e' un clean!...
'''''  End If
'''''  While True
'''''    Posizione = Rtb.Find(Tag, Posizione + Len(indentedText))
'''''    Select Case Posizione
'''''      Case -1
'''''        Exit Sub
'''''      Case 0
'''''        indentedText = text
'''''      Case Else
'''''        crLfPosition = InStrRev(Rtb.text, vbCrLf, Posizione)
'''''        If crLfPosition = 0 Then
'''''          'Linea 1: non ho il fine linea...
'''''          indentedText = Replace(text, vbCrLf, vbCrLf & Space(Posizione))
'''''        Else
'''''          indentedText = Replace(text, vbCrLf, vbCrLf & Space(Posizione - crLfPosition - 1))
'''''        End If
'''''    End Select
'''''    Rtb.SelStart = Posizione
'''''    Rtb.SelLength = Len(Tag)
'''''    Rtb.SelText = indentedText
'''''  Wend
'''''End Sub
'''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''Public Function isCiclicTag(Tag As String) As Boolean
'''''  isCiclicTag = Right(Tag, 4) = "(i)>"
'''''End Function
