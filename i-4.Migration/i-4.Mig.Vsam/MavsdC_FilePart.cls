VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MavsdC_FilePart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'''''''''''''''''''''''''''''''''''''
'classe x gestire l'asteriscamento di
'tutto ci� che � prima della WS
'''''''''''''''''''''''''''''''''''''

Private pFirstSixChar As String     '' primi 6 caratteri della riga
Private pOtherChar As String        '' body (da 7 a 72)
Private pLastChar As String         '' dal 73 alla fine della riga
Private pMultiline As Boolean       '' se continua nella riga successiva
Private pNRows As Integer           '' numero di righe (fino al punto)
Private pNRowsTotal As Integer      '' numero totale di righe lette
Private pIsComment As Boolean       '' se la riga � un commento
Private pUtile As Integer           '' se � 'utile' al fine
                                    '' 0 inutile
                                    '' 1 select
                                    '' 2 fd
Private pIsStruttura As Boolean     '' se � o fa parte di una struttura
Private pIsLast As Boolean          '' serve x chiudere


'''GET
Public Property Get FirstSixChar() As String
  FirstSixChar = pFirstSixChar
End Property
Public Property Get OtherChar() As String
  OtherChar = pOtherChar
End Property
Public Property Get LastChar() As String
  LastChar = pLastChar
End Property
Public Property Get Multiline() As Boolean
  Multiline = pMultiline
End Property
Public Property Get NRows() As Integer
  NRows = pNRows
End Property
Public Property Get NRowsTotal() As Integer
  NRowsTotal = pNRowsTotal
End Property
Public Property Get IsComment() As Boolean
  IsComment = pIsComment
End Property
Public Property Get Utile() As Integer
  Utile = pUtile
End Property
Public Property Get IsStruttura() As Boolean
  IsStruttura = pIsStruttura
End Property

'''LET
Public Property Let FirstSixChar(f As String)
  pFirstSixChar = f
End Property
Public Property Let OtherChar(o As String)
  pOtherChar = o
End Property
Public Property Let LastChar(l As String)
  pLastChar = l
End Property
Public Property Let IsLast(f As Boolean)
  pIsLast = f
End Property
Public Property Let NRowsTotal(i As Integer)
  pNRowsTotal = i
End Property

'***************************************
'input: oggetto tritato precedentemente
'Confronta la riga attuale con quella precedente
'***************************************
Public Sub ContainKeys(oldF As MavsdC_FilePart)
  Dim other As String
On Error GoTo ErrH
  
  
  '1)se il primo carattere � '*' passa oltre
  If Left(pOtherChar, 1) = "*" Then
    pIsComment = True
    pUtile = 0
  Else
    '1) se arriva alla ws chiude
    If InStr(1, pOtherChar, "WORKING-STORAGE") > 0 Then
      pIsLast = True
      pUtile = 0
    End If
    
    '2a) se trova 'SELECT...' o 'FD'  se la cava brillantemente
    If InStr(1, pOtherChar, "SELECT") > 0 Then
      If InStr(1, pOtherChar, ".") = 0 Then
        pMultiline = True
      End If
      If InStr(1, pOtherChar, "ASSIGN TO") > 0 Then
        'beccare il link
      End If
      pUtile = 1
    ElseIf oldF.Utile = 1 Then
      If InStr(1, pOtherChar, "ASSIGN TO") > 0 Then
        'beccare il link
      End If
    End If
    
    '3)FD
    If InStr(1, pOtherChar, "FD") > 0 Then
    
      pUtile = 2
    End If
    
  End If
  
  pNRowsTotal = oldF.NRowsTotal + 1
  
  Exit Sub
ErrH:
  Err.Raise Err.Number, "MavsdC_FilePart.ContainKeys", Err.Description
End Sub
