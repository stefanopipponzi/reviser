VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MavsdF_Verifica 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "VSAM Errors Viewer"
   ClientHeight    =   4245
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   10065
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   4245
   ScaleWidth      =   10065
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdVerifyMoveSSA 
      Caption         =   "Check Move operators on SSA"
      Height          =   525
      Left            =   6720
      TabIndex        =   29
      Top             =   6630
      Visible         =   0   'False
      Width           =   3195
   End
   Begin VB.CommandButton cmLoad 
      Caption         =   "Load Object(s) details to associate PSB"
      Height          =   525
      Left            =   6720
      TabIndex        =   28
      Top             =   6090
      Visible         =   0   'False
      Width           =   3195
   End
   Begin VB.Frame Frame3 
      Caption         =   "PSB Association"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1755
      Left            =   6720
      TabIndex        =   21
      Top             =   4260
      Visible         =   0   'False
      Width           =   3195
      Begin VB.CommandButton cmdAssocia 
         Height          =   435
         Left            =   2610
         Picture         =   "MavsdF_Verifica.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   25
         ToolTipText     =   "Associate PSB"
         Top             =   570
         Width           =   435
      End
      Begin VB.TextBox txtOggetto 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   120
         TabIndex        =   24
         Top             =   1260
         Width           =   2235
      End
      Begin VB.TextBox txtPsb 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   120
         TabIndex        =   23
         Top             =   570
         Width           =   2235
      End
      Begin VB.CommandButton cmdDisass 
         Height          =   435
         Left            =   2610
         Picture         =   "MavsdF_Verifica.frx":0442
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "Unassociate PSB"
         Top             =   1230
         Width           =   435
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "To this Object"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   90
         TabIndex        =   27
         Top             =   990
         Width           =   1245
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Associate this PSB"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   270
         Width           =   1590
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Correlated Programs"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   2895
      Left            =   3480
      TabIndex        =   18
      Top             =   4260
      Visible         =   0   'False
      Width           =   3165
      Begin MSComctlLib.ListView LswCall 
         Height          =   1245
         Left            =   120
         TabIndex        =   19
         Top             =   1530
         Width           =   2925
         _ExtentX        =   5159
         _ExtentY        =   2196
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Program Call"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Relaction"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView LswDep 
         Height          =   1245
         Left            =   120
         TabIndex        =   20
         Top             =   270
         Width           =   2925
         _ExtentX        =   5159
         _ExtentY        =   2196
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "PSB Name"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Calling Level"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame lblPSb 
      Caption         =   "PSB File found in project"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   2895
      Left            =   0
      TabIndex        =   16
      Top             =   4260
      Visible         =   0   'False
      Width           =   3465
      Begin MSComctlLib.ListView LswPSBHD 
         Height          =   2475
         Left            =   120
         TabIndex        =   17
         Top             =   270
         Width           =   3195
         _ExtentX        =   5636
         _ExtentY        =   4366
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         Icons           =   "ImgLista"
         SmallIcons      =   "ImgLista"
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Percorso"
            Object.Width           =   0
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList ImgLista 
      Left            =   8040
      Top             =   7680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":058C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":0B28
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":10C4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":1660
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":17BA
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":1914
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog ComD 
      Left            =   7200
      Top             =   7920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   10065
      _ExtentX        =   17754
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Analizzatore"
            Object.ToolTipText     =   "Automatic istruction corrector"
            ImageIndex      =   14
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Close"
            Object.ToolTipText     =   "Exit"
            ImageIndex      =   8
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Error source summary"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   3765
      Left            =   0
      TabIndex        =   0
      Top             =   450
      Width           =   9915
      Begin VB.Frame Frame2 
         Height          =   3405
         Left            =   180
         TabIndex        =   4
         Top             =   240
         Width           =   3255
         Begin VB.Frame Frame5 
            Caption         =   "Details"
            ForeColor       =   &H00000080&
            Height          =   2565
            Left            =   120
            TabIndex        =   7
            Top             =   720
            Width           =   2985
            Begin VB.CommandButton cmdSeleziona 
               Height          =   315
               Left            =   2550
               Picture         =   "MavsdF_Verifica.frx":1EB0
               Style           =   1  'Graphical
               TabIndex        =   15
               ToolTipText     =   "View selection"
               Top             =   2130
               Width           =   315
            End
            Begin VB.CheckBox ChkSegment 
               Caption         =   "Segment not found"
               Enabled         =   0   'False
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   120
               TabIndex        =   14
               Tag             =   "F10"
               Top             =   2220
               Visible         =   0   'False
               Width           =   2115
            End
            Begin VB.CheckBox ChkDliUnk 
               Caption         =   "VSAM Istructions Unknown"
               Enabled         =   0   'False
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   120
               TabIndex        =   13
               Tag             =   "F09"
               Top             =   1895
               Width           =   2415
            End
            Begin VB.CheckBox ChkNonDec 
               Caption         =   "VSAM Istructions Undecoded"
               Enabled         =   0   'False
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   120
               TabIndex        =   12
               Tag             =   "F08"
               Top             =   1570
               Width           =   2565
            End
            Begin VB.CheckBox ChkPSBAss 
               Caption         =   "PSB association"
               Enabled         =   0   'False
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   120
               TabIndex        =   11
               Tag             =   "F07"
               ToolTipText     =   "Object with no PSB associated"
               Top             =   1230
               Visible         =   0   'False
               Width           =   1845
            End
            Begin VB.CheckBox ChkDBD 
               Caption         =   "DBD Not Loaded"
               Enabled         =   0   'False
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   120
               TabIndex        =   10
               Tag             =   "F06"
               ToolTipText     =   "DBD not loaded into the project"
               Top             =   920
               Visible         =   0   'False
               Width           =   1845
            End
            Begin VB.CheckBox chkPSB 
               Caption         =   "PSB Not loaded"
               Enabled         =   0   'False
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   120
               TabIndex        =   9
               Tag             =   "F04"
               ToolTipText     =   "PSB not loaded into the project"
               Top             =   595
               Visible         =   0   'False
               Width           =   1725
            End
            Begin VB.CheckBox chkInclude 
               Caption         =   "SQL Include"
               Enabled         =   0   'False
               ForeColor       =   &H00C00000&
               Height          =   255
               Left            =   120
               TabIndex        =   8
               Tag             =   "F02"
               ToolTipText     =   "Object of SQL INCLUDE not loaded into the project"
               Top             =   270
               Width           =   1725
            End
         End
         Begin VB.OptionButton OptTotalErr 
            Caption         =   "All Errors Objects (VSAM Errors)"
            ForeColor       =   &H00C00000&
            Height          =   225
            Left            =   120
            TabIndex        =   6
            Top             =   480
            Width           =   2775
         End
         Begin VB.OptionButton OptSelected 
            Caption         =   "Only selected object"
            ForeColor       =   &H00C00000&
            Height          =   225
            Left            =   120
            TabIndex        =   5
            Top             =   210
            Width           =   2115
         End
      End
      Begin MSComctlLib.ListView LswMsg 
         Height          =   3345
         Left            =   5550
         TabIndex        =   3
         Top             =   300
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   5900
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         Icons           =   "ImgLista"
         SmallIcons      =   "ImgLista"
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Selected Object's error (Total)"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView lswOggetti 
         Height          =   3345
         Left            =   3510
         TabIndex        =   2
         Top             =   300
         Width           =   2025
         _ExtentX        =   3572
         _ExtentY        =   5900
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "Nogg"
            Text            =   "Nome Oggetto"
            Object.Width           =   4586
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   9120
      Top             =   7800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   19
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":1FFA
            Key             =   "Index"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":21D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":3458
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":38AC
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":39BE
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":3B98
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":3EBC
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":4310
            Key             =   "Chiudi_2"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":446C
            Key             =   "Lampo"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":45C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":4724
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":4B78
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":4CE0
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":5138
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":5294
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":53FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":5990
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":5DE4
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Verifica.frx":6238
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MavsdF_Verifica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Flag per la ricerca dei file su HD
Public searchflag As Boolean

Private Sub cmdVerifyMoveSSA_Click()
  Screen.MousePointer = vbHourglass
  
  check_Move_Op_SSA VSAM2Rdbms.drObjList.SelectedItem 'rs!IdOggetto
  
  Screen.MousePointer = vbDefault
End Sub

Private Sub cmLoad_Click()
  ReDim Oggetti_ERROR(0)

  DoEvents
  
  'Carica Gli array in memoria degli oggetti e le loro tipologie di errore
  '1) Carica la lista Totale degli oggetti con errore
  'AC SELECT al posto di TOTAL
  Carica_Array_Oggetti_Errori "SELECT"
 
  'Carica la lista con tutti gli oggetti aventi una tipologia di errore
  If UBound(Oggetti_SELECT) > 0 Then
    'Carica solo gli oggetti selezionati
    OptSelected.Value = True
  Else
    'Carica tutti gli oggetti
    OptTotalErr.Value = True
  End If

  'Carica le segnalazioni del 1� Elemento
  If lswOggetti.ListItems.Count > 0 Then
    Screen.MousePointer = vbHourglass

    Carica_Segnalazioni_Da_IdOggetto LswMsg, Val(lswOggetti.ListItems(1).Tag)
    VSAM2Rdbms.drIdOggetto = Val(lswOggetti.ListItems(1).Tag)

    Toolbar1.Buttons(1).Enabled = True

    Screen.MousePointer = vbDefault

    txtOggetto.text = lswOggetti.ListItems(1).text
  End If
  
  'Carica solo i PSB Interni al Project
  Elimina_Flickering Me.hwnd
  Carica_Lista_PSB LswPSBHD, "INTERNAL", lblPSb
  Terminate_Flickering
  
  lblPSb.Caption = "PSB File found in project N� " & LswPSBHD.ListItems.Count
  
  'Chiama funzioni di Resize
  'm_fun.ResizeControls Me
  
  'Resize delle ListView
  lswOggetti.ColumnHeaders(1).Width = lswOggetti.Width - 270
  LswMsg.ColumnHeaders(1).Width = LswMsg.Width
  
  LswPSBHD.ColumnHeaders(1).Width = LswPSBHD.Width - 270
  
  LswDep.ColumnHeaders(1).Width = (LswPSBHD.Width - 270) / 2
  LswDep.ColumnHeaders(2).Width = (LswPSBHD.Width - 270) / 2
  
  'Carica di default le dipendenze del primo elemento
  If lswOggetti.ListItems.Count > 0 Then
    Carica_Dipendenze_PROGRAMMI lswOggetti.ListItems(1).text, Val(lswOggetti.ListItems(1).Tag), LswDep, LswCall
  End If
End Sub

Private Sub Form_Activate()
  'Controlla se la DLL Parser � presente
  If Not Verifica_Esistenza_DLLParser Then
    Unload Me
  End If
End Sub

Private Sub cmdDisass_Click()
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From PsRel_Obj Where IdOggettoC = " & VSAM2Rdbms.drIdOggetto & " And Relazione = 'PSB'")
  While Not r.EOF
    r.Delete adAffectCurrent
    r.Update
    
    'Devo scrivere L'errore
    m_fun.WriteLog "VERIFICA - cmdDisass_Click", "DR - Verifica"
    ' Stop 'Chi lo scrive (il parsing?)
    
    r.MoveNext
  Wend
  r.Close
End Sub

Private Sub Form_Load()
  Dim i As Long
  
  'Setta il parent
  'SetParent Me.hwnd, VSAM2Rdbms.drParent
  
  'm_fun.ResizeControls Me
  
  ReDim Oggetti_SELECT(0)
  ReDim Oggetti_ERROR(0)

  '2) Carica solo gli oggetti selezionati
  Carica_Array_Oggetti_Errori "SELECT"

  'Carica la lista con tutti gli oggetti aventi una tipologia di errore
  If UBound(Oggetti_SELECT) > 0 Then
    'Carica solo gli oggetti selezionati
    OptSelected.Value = True
  Else
    'Carica tutti gli oggetti
    OptTotalErr.Value = True
  End If
  
  
  
  'Carica le segnalazioni del 1� Elemento
  If lswOggetti.ListItems.Count > 0 Then
    Screen.MousePointer = vbHourglass
    
    Carica_Segnalazioni_Da_IdOggetto LswMsg, Val(lswOggetti.ListItems(1).Tag)
    VSAM2Rdbms.drIdOggetto = Val(lswOggetti.ListItems(1).Tag)
    
    Toolbar1.Buttons(1).Enabled = True
    
    Screen.MousePointer = vbDefault
    
    txtOggetto.text = lswOggetti.ListItems(1).text
  End If
  
  'Resize delle ListView
  lswOggetti.ColumnHeaders(1).Width = lswOggetti.Width - 270
  LswMsg.ColumnHeaders(1).Width = LswMsg.Width

  LswPSBHD.ColumnHeaders(1).Width = LswPSBHD.Width - 270

  LswDep.ColumnHeaders(1).Width = (LswPSBHD.Width - 270) / 2
  LswDep.ColumnHeaders(2).Width = (LswPSBHD.Width - 270) / 2
  
  'MF 01/08/07 Mancava AddActiveWindows
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
  
  'Carica le dipendenze
  If Not lswOggetti.ListItems.Count = 0 Then
    Carica_Dipendenze_PROGRAMMILoad lswOggetti.ListItems(1).text, lswOggetti.ListItems(1).Tag, LswDep, LswCall
   Else
    MsgBox "No object selected", vbInformation, "No selection"
   End If
End Sub

Private Sub Form_Resize()
    ResizeForm Me
End Sub

Private Sub LswDep_ItemClick(ByVal Item As MSComctlLib.ListItem)
  If InStr(1, UCase(Item.text), "NO PSB") = 0 Then
    txtPsb.text = Item.text
  End If
End Sub



Private Sub lswOggetti_ItemClick(ByVal Item As MSComctlLib.ListItem)
  'Carica La lista delle segnalazioni
  Screen.MousePointer = vbHourglass
  
  Call Carica_Segnalazioni_Da_IdOggetto(LswMsg, Val(Item.Tag))
  VSAM2Rdbms.drIdOggetto = Val(Item.Tag)
  
  Toolbar1.Buttons(1).Enabled = True
  
  Screen.MousePointer = vbDefault
  
  txtOggetto.text = Item.text
  
  'Carica le dipendenze
  Call Carica_Dipendenze_PROGRAMMI(Item.text, VSAM2Rdbms.drIdOggetto, LswDep, LswCall)
End Sub

Private Sub LswPSBHD_ItemClick(ByVal Item As MSComctlLib.ListItem)
  txtPsb.text = Item.text
  
  DirectoryPSB = Item.Tag
End Sub

Private Sub OptSelected_Click()
  txtPsb.text = ""
  txtOggetto.text = ""
  LswDep.ListItems.Clear
  LswCall.ListItems.Clear
  
  ChkDBD.Value = 0
  ChkDliUnk.Value = 0
  chkInclude.Value = 0
  ChkNonDec.Value = 0
  chkPSB.Value = 0
  ChkPSBAss.Value = 0
  ChkSegment.Value = 0
  
  ChkDBD.Enabled = False
  ChkDliUnk.Enabled = False
  chkInclude.Enabled = False
  ChkNonDec.Enabled = False
  chkPSB.Enabled = False
  ChkPSBAss.Enabled = False
  ChkSegment.Enabled = False
  
  Call Carica_Lista_Oggetti(False, "", lswOggetti, Oggetti_SELECT, True)
End Sub

Private Sub OptTotalErr_Click()
  txtPsb.text = ""
  txtOggetto.text = ""
  LswDep.ListItems.Clear
  LswCall.ListItems.Clear
  
  ChkDBD.Value = 0
  ChkDliUnk.Value = 0
  chkInclude.Value = 0
  ChkNonDec.Value = 0
  chkPSB.Value = 0
  ChkPSBAss.Value = 0
  ChkSegment.Value = 0
  
  ChkDBD.Enabled = True
  ChkDliUnk.Enabled = True
  chkInclude.Enabled = True
  ChkNonDec.Enabled = True
  chkPSB.Enabled = True
  ChkPSBAss.Enabled = True
  ChkSegment.Enabled = True
  
  If UBound(Oggetti_ERROR) = 0 Then cmLoad_Click
  
  Call Carica_Lista_Oggetti(False, "", lswOggetti, Oggetti_ERROR, True)
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Trim(UCase(Button.key))
    Case "ANALIZZATORE" 'Corettore semi automatico
      MavsdF_Corrector.Show
        SetParent MavsdF_Corrector.hwnd, VSAM2Rdbms.drParent
        MavsdF_Corrector.Show
        MavsdF_Corrector.Move 0, 0, VSAM2Rdbms.drWidth, VSAM2Rdbms.drHeight
    
    Case "CLOSE"
      Unload Me
  End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
  ReDim Oggetti_ERROR(0)
  ReDim Oggetti_SELECT(0)
  
  SetFocusWnd m_fun.FnParent
  
  m_fun.FnActiveWindowsBool = False
  m_fun.FnProcessRunning = False
End Sub

Private Sub cmdSeleziona_Click()
  Dim SQL As String
  
  SQL = Crea_SQL
  
  'Carica la lista degli oggetti
  If SQL = "" Then
    Call Carica_Lista_Oggetti(False, SQL, lswOggetti, Oggetti_ERROR, True)
  Else
    Call Carica_Lista_Oggetti(True, SQL, lswOggetti, Oggetti_ERROR, True)
  End If
End Sub

Private Function Crea_SQL() As String
  Dim Texto As String
  Dim App As String
  
  Texto = "Where"
  
  If ChkDBD.Value = 1 Then
    If Trim(UCase(Texto)) = "WHERE" Then
      App = " Codice = 'F06'"
    Else
      App = " Or Codice = 'F06'"
    End If
    
    Texto = Texto & App
  End If
  
  If ChkDliUnk.Value = 1 Then
    If Trim(UCase(Texto)) = "WHERE" Then
      App = " Codice = 'F09'"
    Else
      App = " Or Codice = 'F09'"
    End If
    
    Texto = Texto & App
  End If
  
  If chkInclude.Value = 1 Then
    If Trim(UCase(Texto)) = "WHERE" Then
      App = " Codice = 'F02'"
    Else
      App = " Or Codice = 'F02'"
    End If
    
    Texto = Texto & App
  End If
  
  If ChkNonDec.Value = 1 Then
    If Trim(UCase(Texto)) = "WHERE" Then
      App = " Codice = 'F08'"
    Else
      App = " Or Codice = 'F08'"
    End If
    
    Texto = Texto & App
  End If
  
  If chkPSB.Value = 1 Then
    If Trim(UCase(Texto)) = "WHERE" Then
      App = " Codice = 'F04'"
    Else
      App = " Or Codice = 'F04'"
    End If
    
    Texto = Texto & App
  End If
  
  If ChkPSBAss.Value = 1 Then
    If Trim(UCase(Texto)) = "WHERE" Then
      App = " Codice = 'F07'"
    Else
      App = " Or Codice = 'F07'"
    End If
    
    Texto = Texto & App
  End If
  
  If ChkSegment.Value = 1 Then
    If Trim(UCase(Texto)) = "WHERE" Then
      App = " Codice = 'F10'"
    Else
      App = " Or Codice = 'F10'"
    End If
    
    Texto = Texto & App
  End If
  
  If Trim(UCase(Texto)) = "WHERE" Then
    Crea_SQL = ""
  Else
    Texto = "Select * From BS_Segnalazioni " & Texto
  
    Crea_SQL = Texto
  End If
End Function

Private Sub cmdAssocia_Click()
  Dim DirPsb As String
  Dim OldIdOgg As Long
  
  Dim NRel As String
  Dim Texto As String
  Dim key As String
  Dim LivelloT As String
  
  Dim r As Recordset
  Dim TbOggetti As Recordset
  Dim cParam As collection
  
  IdOgg = VSAM2Rdbms.drIdOggetto
  
  IdPSB = Restituisci_IdOggetto_Da_Nome(NomePsb, "PSB")
  
  NomePsb = Trim(UCase(NomePsb))
    
  If IdPSB <> 0 Then
    '1) Scrive la relazione
    Call Scrivi_Relazione_PSB(IdOgg, IdPSB, NomePsb)
    
    '2) Lancia il parsing
    OldIdOgg = DLLExtParser.PsIdOggetto_Add
    
    Screen.MousePointer = vbHourglass
    
    'Sull'Oggetto
    DLLExtParser.PsIdOggetto_Add = IdOgg
    
    Set cParam = New collection
    cParam.Add IdOgg
    DLLExtParser.AttivaFunzione ("PARSER_DLI"), cParam
    
    Screen.MousePointer = vbDefault
    
    DLLExtParser.PsIdOggetto_Add = OldIdOgg
      
    '3) Elimina le segnalazioni PSB Non associato
    VSAM2Rdbms.drConnection.Execute "Delete * From BS_Segnalazioni Where IdOggetto = " & IdOgg & " And Codice = 'F07'"
  Else
    MsgBox "PSB Find Error... Launch parsing again...", vbExclamation, VSAM2Rdbms.drNomeProdotto
  End If
End Sub

Private Sub txtPsb_Change()
  NomePsb = txtPsb.text
End Sub
