Attribute VB_Name = "MaVSAMM_DLL"
Option Explicit
Public Const LOGPIXELSX = 88
Public Const LOGPIXELSY = 90
Public Const NomeDLLParser As String = "MapsdP_00.dll"

'ferma l'effetto flikering delle form
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hWndLock As Long) As Long
'A.P.I. per il resize dei font
Public Declare Function GetDesktopWindow Lib "user32" () As Long
Public Declare Function GetDeviceCaps Lib "gdi32" (ByVal hdc As Long, ByVal nIndex As Long) As Long
Public Declare Function GetDC Lib "user32" (ByVal hwnd As Long) As Long
Public Declare Function ReleaseDC Lib "user32" (ByVal hwnd As Long, ByVal hdc As Long) As Long
Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long

'Variabile per vedere le funzioni del Parsing
Global DLLExtParser As New MapsdC_Menu

Public Sub Elimina_Flickering(ByVal lHWnd As Long)
    Dim lRet As Long
    'oggetto da 'freezare'
    lRet = LockWindowUpdate(lHWnd)
End Sub

Public Sub Terminate_Flickering()
    Dim lRet As Long
    lRet = LockWindowUpdate(0)
End Sub

Public Function IsScreenFontSmall() As Boolean
    Dim hWndDesk As Long
    Dim hDCDesk As Long
    Dim logPix As Long
    Dim r As Long
    hWndDesk = GetDesktopWindow()
    hDCDesk = GetDC(hWndDesk)
    logPix = GetDeviceCaps(hDCDesk, LOGPIXELSX)
    r = ReleaseDC(hWndDesk, hDCDesk)
    If logPix = 96 Then IsScreenFontSmall = True
    Exit Function
End Function

Sub ResizeControls(frmName As Form, Optional IsMenu As Boolean, Optional TopListView As Boolean)
 m_dllFunctions.ResizeControls frmName
End Sub
'***************************************************************************
'********************* Apre un set di recordset Ado  ***********************
'***************************************************************************

Public Function Carica_Menu() As Boolean
   SwMenu = True
  
  'inizializza la dll rendendo visibili le variabili della classe anche alle form
  Carica_Menu = True
  
  'crea il menu in memoria
  ReDim drMenu(0)
  
    If m_dllFunctions.Type_License <> LIC_REV_NODLIIMS And m_dllFunctions.Type_License <> LIC_REV_NOIMS And m_dllFunctions.Type_License <> LIC_REV_NOAGG Then

         'Primo Bottone del menu a sinistra : VSAM2Rdbms
         ReDim Preserve drMenu(UBound(drMenu) + 1)
         drMenu(UBound(drMenu)).Id = 900
         drMenu(UBound(drMenu)).Label = "VSAM->RDBMS"
         drMenu(UBound(drMenu)).ToolTipText = ""
         drMenu(UBound(drMenu)).Picture = ""
         drMenu(UBound(drMenu)).M1Name = ""
         drMenu(UBound(drMenu)).M1SubName = ""
         drMenu(UBound(drMenu)).M2Name = "<Root>"
         drMenu(UBound(drMenu)).M3Name = ""
         drMenu(UBound(drMenu)).M4Name = ""
         drMenu(UBound(drMenu)).Funzione = ""
         drMenu(UBound(drMenu)).DllName = "MaVSAMP_00"
         drMenu(UBound(drMenu)).TipoFinIm = ""
End If

'    '1) Sottobottone : Verifiche
'    ReDim Preserve drMenu(UBound(drMenu) + 1)
'    drMenu(UBound(drMenu)).Id = 710
'    drMenu(UBound(drMenu)).Label = "Verify's Tools"
'    drMenu(UBound(drMenu)).ToolTipText = ""
'    drMenu(UBound(drMenu)).Picture = "\Immagini\VerifyTools_Disabled.ico"
'    drMenu(UBound(drMenu)).PictureEn = "\Immagini\VerifyTools_Enabled.ico"
'    drMenu(UBound(drMenu)).M1Name = ""
'    drMenu(UBound(drMenu)).M1SubName = ""
'    drMenu(UBound(drMenu)).M2Name = "VSAM2Rdbms"
'    drMenu(UBound(drMenu)).M3Name = ""
'    drMenu(UBound(drMenu)).M4Name = ""
'    drMenu(UBound(drMenu)).Funzione = "Show_Tools"
'    drMenu(UBound(drMenu)).DllName = "MaVSAMP_00"
'    drMenu(UBound(drMenu)).TipoFinIm = ""
    
     '2) Sottobottone : Incapsulatore
    ReDim Preserve drMenu(UBound(drMenu) + 1)
    drMenu(UBound(drMenu)).Id = 915
    drMenu(UBound(drMenu)).Label = "Corrector"
    drMenu(UBound(drMenu)).ToolTipText = ""
    drMenu(UBound(drMenu)).Picture = VSAM2Rdbms.drImgDir & "\Correttore_Disabled.ico"
    drMenu(UBound(drMenu)).PictureEn = VSAM2Rdbms.drImgDir & "\Correttore_Enabled.ico"
    drMenu(UBound(drMenu)).M1Name = ""
    drMenu(UBound(drMenu)).M1SubName = ""
    drMenu(UBound(drMenu)).M2Name = "VSAM->RDBMS"
    drMenu(UBound(drMenu)).M3Name = ""
    drMenu(UBound(drMenu)).M4Name = ""
    drMenu(UBound(drMenu)).Funzione = "Show_Tools"
    drMenu(UBound(drMenu)).DllName = "MaVSAMP_00"
    drMenu(UBound(drMenu)).TipoFinIm = ""
    
  If m_dllFunctions.Type_License <> LIC_REV_NODLIIMS And m_dllFunctions.Type_License <> LIC_REV_NOIMS And m_dllFunctions.Type_License <> LIC_REV_NOAGG Then
         '2) Sottobottone : Incapsulatore
         ReDim Preserve drMenu(UBound(drMenu) + 1)
         drMenu(UBound(drMenu)).Id = 920
         drMenu(UBound(drMenu)).Label = "Encapsulator"
         drMenu(UBound(drMenu)).ToolTipText = ""
         drMenu(UBound(drMenu)).Picture = VSAM2Rdbms.drImgDir & "\Incapsulatore_Disabled.ico"
         drMenu(UBound(drMenu)).PictureEn = VSAM2Rdbms.drImgDir & "\Incapsulatore_Enabled.ico"
         drMenu(UBound(drMenu)).M1Name = ""
         drMenu(UBound(drMenu)).M1SubName = ""
         drMenu(UBound(drMenu)).M2Name = "VSAM->RDBMS"
         drMenu(UBound(drMenu)).M3Name = ""
         drMenu(UBound(drMenu)).M4Name = ""
         drMenu(UBound(drMenu)).Funzione = "Show_Incapsulatore"
         drMenu(UBound(drMenu)).DllName = "MaVSAMP_00"
         drMenu(UBound(drMenu)).TipoFinIm = ""
  
         '3) Sottobottone : VSAM2Rdbms
         ReDim Preserve drMenu(UBound(drMenu) + 1)
         drMenu(UBound(drMenu)).Id = 930
         drMenu(UBound(drMenu)).Label = "I-O Routines Generator"
         drMenu(UBound(drMenu)).ToolTipText = ""
         drMenu(UBound(drMenu)).Picture = VSAM2Rdbms.drImgDir & "\Incapsulatore_Disabled.ico"
         drMenu(UBound(drMenu)).PictureEn = VSAM2Rdbms.drImgDir & "\GeneratoreRoutine_Enabled.ico"
         drMenu(UBound(drMenu)).M1Name = ""
         drMenu(UBound(drMenu)).M1SubName = ""
         drMenu(UBound(drMenu)).M2Name = "VSAM->RDBMS"
         drMenu(UBound(drMenu)).M3Name = ""
         drMenu(UBound(drMenu)).M4Name = ""
         drMenu(UBound(drMenu)).Funzione = "Show_MIGRATOR"
         drMenu(UBound(drMenu)).DllName = "MaVSAMP_00"
         drMenu(UBound(drMenu)).TipoFinIm = ""
  End If

    '4) Sottobottone : Parametri
'    ReDim Preserve drMenu(UBound(drMenu) + 1)
'    drMenu(UBound(drMenu)).Id = 740
'    drMenu(UBound(drMenu)).Label = "VSAM2Rdbms Parameters"
'    drMenu(UBound(drMenu)).ToolTipText = ""
'    drMenu(UBound(drMenu)).Picture = VSAM2Rdbms.drImgDir & "\VSAM2Rdbms_Config_Disabled.ico"
'    drMenu(UBound(drMenu)).PictureEn = VSAM2Rdbms.drImgDir & "\VSAM2Rdbms_Config_Enabled.ico"
'    drMenu(UBound(drMenu)).M1Name = ""
'    drMenu(UBound(drMenu)).M1SubName = ""
'    drMenu(UBound(drMenu)).M2Name = "Config"
'    drMenu(UBound(drMenu)).M3Name = ""
'    drMenu(UBound(drMenu)).M4Name = ""
'    drMenu(UBound(drMenu)).Funzione = "Show_VSAM2Rdbms_Parameter"
'    drMenu(UBound(drMenu)).DllName = "MaVSAMP_00"
'    drMenu(UBound(drMenu)).TipoFinIm = ""

End Function

Public Sub WaitTime(Sec As Long)
  Dim PauseTime, Start, Finish, TotalTime
  
  PauseTime = Sec
  Start = Timer
  Do While Timer < Start + PauseTime
  
  Loop
  Finish = Timer
  TotalTime = Finish - Start
    
End Sub

Public Function Verifica_Esistenza_DLLParser() As Boolean
  Dim exfile As String
  
  exfile = Dir(App.Path & "\" & SYSTEM_DIR & "\" & NomeDLLParser)
  
  If exfile <> "" Then
    Verifica_Esistenza_DLLParser = False
  Else
    Verifica_Esistenza_DLLParser = True
    
    Call Linka_Variabili_Parser
  End If
End Function

Public Sub Linka_Variabili_Parser()
  Set DLLExtParser.PsFinestra = VSAM2Rdbms.drFinestra
  Set DLLExtParser.PsDatabase = VSAM2Rdbms.drDatabase
  Set DLLExtParser.PsConnection = VSAM2Rdbms.drConnection
  DLLExtParser.PsNomeProdotto = VSAM2Rdbms.drNomeProdotto
  
  DLLExtParser.PsPathDef = VSAM2Rdbms.drPathDef
  DLLExtParser.PsUnitDef = VSAM2Rdbms.drUnitDef
  
  Set DLLExtParser.PsObjList = VSAM2Rdbms.drObjList
  
  DLLExtParser.PsTipoMigrazione = VSAM2Rdbms.drTipoMigrazione
  
  DLLExtParser.PsTop = VSAM2Rdbms.drTop
  DLLExtParser.PsLeft = VSAM2Rdbms.drLeft
  DLLExtParser.PsWidth = VSAM2Rdbms.drWidth
  DLLExtParser.PsHeight = VSAM2Rdbms.drHeight
  
End Sub
