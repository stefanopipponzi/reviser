VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form MavsdF_Incapsulatore 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "VSAM - Objects Encapsulator"
   ClientHeight    =   5370
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8895
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   8895
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame6 
      Caption         =   "Encapsulation Source Options"
      ForeColor       =   &H00000080&
      Height          =   612
      Left            =   0
      TabIndex        =   16
      Top             =   3480
      Width           =   3432
      Begin VB.OptionButton OptEncaps 
         Caption         =   "Encapsulated"
         Height          =   192
         Left            =   1800
         TabIndex        =   18
         Top             =   240
         Width           =   1332
      End
      Begin VB.OptionButton Optsource 
         Caption         =   "Native"
         Height          =   192
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Value           =   -1  'True
         Width           =   852
      End
   End
   Begin RichTextLib.RichTextBox RTBModifica 
      Height          =   525
      Left            =   1320
      TabIndex        =   12
      Top             =   5460
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MavsdF_Incapsulatore.frx":0000
   End
   Begin RichTextLib.RichTextBox RTRoutine 
      Height          =   525
      Left            =   690
      TabIndex        =   11
      Top             =   5460
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MavsdF_Incapsulatore.frx":0082
   End
   Begin RichTextLib.RichTextBox RTFile 
      Height          =   525
      Left            =   60
      TabIndex        =   10
      Top             =   5460
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MavsdF_Incapsulatore.frx":0104
   End
   Begin VB.Frame Frame4 
      Caption         =   "Activity Log"
      ForeColor       =   &H00000080&
      Height          =   3135
      Left            =   3480
      TabIndex        =   6
      Top             =   450
      Width           =   5385
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   255
         Left            =   90
         TabIndex        =   8
         Top             =   2835
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ListView LvObject 
         Height          =   2295
         Left            =   90
         TabIndex        =   7
         Top             =   240
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   4048
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "ID"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Object"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Time"
            Object.Width           =   2822
         EndProperty
      End
      Begin MSComctlLib.ProgressBar PbarIstr 
         Height          =   255
         Left            =   90
         TabIndex        =   13
         Top             =   2565
         Width           =   5205
         _ExtentX        =   9181
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Error Log"
      ForeColor       =   &H00000080&
      Height          =   1725
      Left            =   3480
      TabIndex        =   5
      Top             =   3600
      Width           =   5385
      Begin RichTextLib.RichTextBox RTErr 
         Height          =   1365
         Left            =   60
         TabIndex        =   9
         Top             =   270
         Width           =   5235
         _ExtentX        =   9234
         _ExtentY        =   2408
         _Version        =   393217
         BackColor       =   -2147483624
         Enabled         =   -1  'True
         TextRTF         =   $"MavsdF_Incapsulatore.frx":0186
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Analysis Function"
      ForeColor       =   &H00000080&
      Height          =   1248
      Left            =   0
      TabIndex        =   3
      Top             =   4080
      Width           =   3435
      Begin VB.ListBox LstFunction 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   735
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   4
         Top             =   240
         Width           =   3195
      End
      Begin RichTextLib.RichTextBox RTBR 
         Height          =   285
         Left            =   420
         TabIndex        =   15
         Top             =   960
         Visible         =   0   'False
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   503
         _Version        =   393217
         TextRTF         =   $"MavsdF_Incapsulatore.frx":0208
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Select Object for Analysis"
      ForeColor       =   &H00000080&
      Height          =   3012
      Left            =   0
      TabIndex        =   1
      Top             =   450
      Width           =   3435
      Begin MSComctlLib.TreeView TVSelObj 
         Height          =   2745
         Left            =   120
         TabIndex        =   2
         Top             =   270
         Width           =   3195
         _ExtentX        =   5636
         _ExtentY        =   4842
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   529
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   336
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8892
      _ExtentX        =   15690
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Start"
            Object.ToolTipText     =   "Start Encapsulating"
            ImageKey        =   "Attiva"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "Help"
            Object.ToolTipText     =   "Help"
            ImageKey        =   "Help"
         EndProperty
      EndProperty
      Begin VB.TextBox Text1 
         Height          =   315
         Left            =   990
         TabIndex        =   14
         Top             =   30
         Visible         =   0   'False
         Width           =   1425
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   2760
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Incapsulatore.frx":0277
            Key             =   "Attiva"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Incapsulatore.frx":0591
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_Incapsulatore.frx":06A3
            Key             =   "Printer"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "MavsdF_Incapsulatore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'SQ: utilizzare questo metodo:
'Const ..._CHK = 0
'Const ..._CHK = 1
'Const ..._CHK = 2
'Const ..._CHK = 3
Const EMBEDDED_CHK = 4

Dim NumObjInParser As Double
Dim NumFeatures As Integer
'SQ: BUTTARE VIA --^

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub LstFunction_Click()
  Dim k1 As Integer
   
  'ATTENZIONE, POSIZIONALE: pericolosissimo!
  'MavsdM_Incapsulatore.EmbeddedRoutine = LstFunction.Selected(EMBEDDED_CHK)
  If LstFunction.ListCount > EMBEDDED_CHK Then
    EmbeddedRoutine = LstFunction.Selected(EMBEDDED_CHK)
  End If
  
  'SQ - A chi serve 'sto robo!?
  NumFeatures = 0
  For k1 = 0 To LstFunction.ListCount - 1
    If LstFunction.Selected(k1) Then
       NumFeatures = NumFeatures + 1
    End If
  Next k1
End Sub

Private Sub updateEntryOff(pIdOggetto As Long, pRiga As Long, offset As Long)
  Dim rsEntry As Recordset

  Set rsEntry = m_fun.Open_Recordset("Select * from PS_OBJROWOFFSET where idoggetto = " & pIdOggetto & " and riga = " & pRiga)
  If Not rsEntry.EOF Then
    rsEntry!OffsetEncaps = rsEntry!OffsetEncaps + offset
  Else
    rsEntry.AddNew
    rsEntry!idOggetto = pIdOggetto
    rsEntry!riga = pRiga
    rsEntry!OffsetEncaps = offset
  End If
  rsEntry.Update
  rsEntry.Close
End Sub

Private Function namePgmFromId(pIdPGM As Long)
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & pIdPGM)
  If Not r.EOF Then
    If Trim(r!Estensione) = "" Then
       namePgmFromId = r!nome
    Else
       namePgmFromId = r!nome & "." & r!Estensione
    End If
  End If
End Function

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim rst As Recordset, rIst As Recordset, rstCloni As Recordset, rsOffset As Recordset
  Dim typeIncaps As String, wIstr As String, Selezione As String, lastPos As Long
  Dim MaxVal As Long
  Dim nomeroutine As String, Totaloffset As Long, rsEntry As Recordset, rigaentry As Long, upentry As Boolean
  Dim idpgmcpy As Long
  ' AC da questo array discriminiamo copy da programmi
  Dim arrayPGM() As Long
  Dim rsPgm As Recordset, i As Integer, startPLIkey As Long, startPLIkeyRet As Long
  Dim NomeProgappo As String, offsetentry As Long
  Dim tipoFile As String, PLIsaveBackKey As String
  On Error Resume Next
  
  ReDim arrayPGM(0)
  Screen.MousePointer = vbHourglass
  RTErr.text = ""
  'RTB.text = ""
  
  Select Case Button.key
    Case "Start"
      DoEvents
               
      'ora l'incapsulamento � unico: sempre RDBMS, che � quello corretto
      typeIncaps = "RDBMS"
      
      If LstFunction.Selected(2) Then
        swCall = True
      ElseIf LstFunction.Selected(3) Then
        swCall = False
      End If
      
'''che �???
'''      If LstFunction.Selected(1) Then
'''        If MsgBox("Warning. You are resetting all DECODING: you could need to INCAPSULATE the sources again. " & vbCrLf & "Are you sure?", vbYesNo + vbQuestion + vbCritical, "Reviser: Encapsulator") = vbYes Then
'''          m_fun.FnConnection.Execute "DELETE * From MgDli_decodificaIstr Where ImsDB = true"
'''        End If
'''      End If
          
      Selezione = Restituisci_Selezione(TVSelObj)
      
      'CARICAMENTO rIstr
      Dim criterio As String
      Select Case Selezione
        Case "SRC" 'Tutti i programmi
          Set rIst = m_fun.Open_Recordset("Select idoggetto,tipo,cics From BS_Oggetti Where vsam and tipo = 'CBL' Order By IdOggetto")
        Case "SEL" 'Solo i programmi Selezionati
          For i = 1 To VSAM2Rdbms.drObjList.ListItems.Count
            If VSAM2Rdbms.drObjList.ListItems(i).Selected Then
               If Trim(criterio) <> "" Then
                  criterio = criterio & ", " & Val(VSAM2Rdbms.drObjList.ListItems(i).text)
               Else
                  criterio = Val(VSAM2Rdbms.drObjList.ListItems(i).text)
               End If
            End If
          Next
          If Len(Trim(criterio)) = 0 Then
            'dare messaggio
            Exit Sub
          End If
          criterio = " IdOggetto in (" & criterio & ") "
          Set rIst = m_fun.Open_Recordset("Select  idoggetto,tipo,cics From BS_Oggetti Where " & criterio & " And vsam Order By IdOggetto")
      End Select
      
      PBar.Max = rIst.RecordCount
      PBar.Value = 0
      PbarIstr.Max = 1
      PbarIstr.Value = 0
      
''''ok up

      If rIst.RecordCount > 0 Then
        While Not rIst.EOF
          tipoFile = rIst!Tipo
          SwTp = rIst!Cics
          upentry = False
''''''''''ko          startPLIkey = 1
          ReDim routinePLI(0)
          ReDim arrayPGM(0)
          ReDim LNKDBNumstack(0)
          
          'se agisco sul sorgente originale azzero gli offset
          If Optsource Then
            m_fun.FnConnection.Execute "Update Ps_objRowOffset set OffsetEncaps = 0 where idoggetto = " & rIst!idOggetto
          End If
          
          'Analizza e inserisce le varie istruzioni e chiamate nel CBL.
          'Per� non tocca il CBL originale, ma lo copia, modificato, nella directory: CBLOut o CPYOut
          Controlla_CBL_CPY RTBModifica, RTErr, rIst, typeIncaps 'carica o salva il file in base al fatto che sia o meno gi� caricato
          
          Set rsEntry = m_fun.Open_Recordset("Select * from PsPgm where idoggetto = " & rIst!idOggetto & " and comando ='ENTRY'")
          If Not rsEntry.EOF Then
            rigaentry = rsEntry!riga
          End If
          rsEntry.Close
          
          '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' SQ - La gestione della PS_objRowOffset NON deve essere fatta cos�!
          '      Cos� � ingestibile... oltre che scorretta...
          ' Ci vanno le righe aggiunte nella "riga" in cui sono state aggiunte!, non le istruzioni
          ' Cos� devo aggiungere il record della Entry, se non IMSDC sbaglia!!!!!!!!!!!!!!!!!
          '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          'pezza: lo devo fare sempre... pulire sotto
          If istrIncopy(rIst!idOggetto) Then
'''ko            offsetentry = AddRighe + 1
          Else
'''ko            offsetentry = AddRighe
          End If
          If rigaentry Then
'''ko            updateEntryOff rIst!idOggetto, rigaentry, offsetentry
            updateEntryOff rIst!idOggetto, rigaentry, 0 '''offsetentry
            upentry = True
          End If
          '''m_fun.FnConnection.Execute "update ps_objrowoffset set offsetencaps = offsetencaps + " & AddRighe & " Where idoggetto = " & rIst!idOggetto
          'SP 4/9: intabella i PCB
          'CreaTabPCB RTBModifica, rIst!IdOggetto
          
          '''''''''''''''''''''''''''''''''''''''''''''''''
          ' ELIMINAZIONE elementi dalla PsDli_IstrCodifica
          ' !attenzione... rimangono "orfani" nella MgDli_Decodifica...
          '''''''''''''''''''''''''''''''''''''''''''''''''
          m_fun.FnConnection.Execute "DELETE From PsDli_IstrCodifica Where IdOggetto = " & rIst!idOggetto
          
          'SP corso : per ora non escludo quelle IMSDC, quindi le asterisco
          'SQ: NON FUNZIONANO LE OBSOLETE UNKNOWN...
          'Set rst = m_fun.Open_Recordset("Select * from PSDli_Istruzioni where idoggetto = " & rIst!IdOggetto & " And ImsDB order by riga")
          'SQ CPY-ISTR
          'Per le copy: idPgm=0 da scartare: record "matrice"
          Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE idoggetto=" & rIst!idOggetto & " AND idPgm<>0 ORDER BY riga") 'And not ImsDC
          If tipoFile = "CPY" And Not rst.EOF Then
            idpgmcpy = rst!idpgm
            rst.Close
            Set rst = m_fun.Open_Recordset("SELECT * FROM PSDli_Istruzioni WHERE idoggetto=" & rIst!idOggetto & " AND idPgm = " & idpgmcpy & " ORDER BY riga") 'And not ImsDC
            Set rsPgm = m_fun.Open_Recordset("SELECT distinct IdPGM FROM PSDli_Istruzioni WHERE idoggetto=" & rIst!idOggetto)  'And not ImsDC
            While Not rsPgm.EOF
              If Not rsPgm!idpgm = 0 Then
                ReDim Preserve arrayPGM(UBound(arrayPGM) + 1)
                arrayPGM(UBound(arrayPGM)) = rsPgm!idpgm
              End If
              rsPgm.MoveNext
            Wend
          Else
            ' potenziali danni --- serve per move del nome programma nella var WS-Caller
            ' (solo se istruzioni in copy)
            If istrIncopy(rIst!idOggetto) Then
              InsertMovePgmName RTBModifica, namePgmFromId(rIst!idOggetto)
            End If
            'ReDim arrayPGM(1)
          End If
          
          j = 0
    '          3) inserimento delle istruzioni:
          If rst.RecordCount > 0 Then
            PbarIstr.Max = rst.RecordCount
          End If
         
         'pcb dinamico
          ReDim OpeIstrRt(0)
         
          Set distinctCod = New collection
          Set distinctSeg = New collection
          Set distinctDB = New collection
    
          While Not rst.EOF
            Set rsOffset = m_fun.Open_Recordset("Select * from PS_objRowOffset where idoggetto = " & rst!idOggetto & " and riga =" & rst!riga)
            If Not rsOffset.EOF Then
              Totaloffset = IIf(IsNull(rsOffset!offsetMacro), 0, rsOffset!offsetMacro) + rsOffset!OffsetEncaps
            Else
              rsOffset.Close
              Set rsOffset = m_fun.Open_Recordset("Select * from PS_objRowOffset")
              rsOffset.AddNew
              rsOffset!idOggetto = rst!idOggetto
              rsOffset!riga = rst!riga
            End If
            If Not rst!ImsDC Then
              If rst!riga > rigaentry And Not upentry Then
                updateEntryOff rIst!idOggetto, rigaentry, AddRighe
                upentry = True
              End If
              wPunto = False
              'stefano: nuova gestione
              'SQ OK, comunque per Asteriscare leggiamo il file (e l'eventuale punto!)
              'If InStr(rst!Statement, ".") > 0 Then wPunto = True
              Dim rsCall As Recordset
              Set rsCall = m_fun.Open_Recordset("Select * from PSCall where idoggetto = " & rst!idOggetto & " And Riga = " & rst!riga)
              If Not rsCall.EOF Then
                 If InStr(rsCall!parametri, ".") > 0 Then wPunto = True
              'SQ
              'else
                '????????????????
              End If
              'SQ chiudere i recordset!
              rsCall.Close
              
              If rst!to_migrate Then
                Select Case tipoFile
                  Case "EZT"
                    posizione = AsteriscaIstruzione_EZT(Totaloffset, rst, RTBModifica, typeIncaps)
                  Case "PLI"
                    isPLI = True
                    If PLIsaveBackKey = "RET" Then
                      startPLIkeyRet = startPLIkey
                      insertPLISaveBack "RETURN;", startPLIkeyRet, rst!riga + AddRighe + Totaloffset, RTBModifica
                      startPLIkeyRet = startPLIkey
                      insertPLISaveBack "RETURN(", startPLIkeyRet, rst!riga + AddRighe + Totaloffset, RTBModifica
                      startPLIkeyRet = startPLIkey
                      insertPLISaveBack "RETURN (", startPLIkeyRet, rst!riga + AddRighe + Totaloffset, RTBModifica
                    Else
                      insertPLISaveBack PLIsaveBackKey, startPLIkey, rst!riga + AddRighe + Totaloffset, RTBModifica
                    End If
                    posizione = AsteriscaIstruzione_PLI(Totaloffset, rst, RTBModifica, typeIncaps)
                    If posizione > 0 Then
                      startPLIkey = posizione
                    End If
                  Case Else
                    posizione = AsteriscaIstruzione(Totaloffset, rst, RTBModifica, typeIncaps)
                End Select
              Else
                posizione = 0
              End If
              If posizione > 0 Then
                'If rst!Valida Then
                 If rst!valida And Not rst!obsolete Then
                    If Optsource Then
                      rsOffset!OffsetEncaps = AddRighe
                    Else
                      rsOffset!OffsetEncaps = IIf(IsNull(rsOffset!OffsetEncaps), 0, rsOffset!OffsetEncaps) + AddRighe
                    End If
                    
                    'CaricaIstruzione rst lascio dentro asteriscaistruzione
                    If Trim(rst!Istruzione) = "QUERY" Then
                      wIstr = "QRY"
                    Else
                      wIstr = Trim(rst!Istruzione)
                    End If
                    
                    'cappellotto comune  (VECCHIA VERSIONE NO TEMPLATE)
                    If tipoFile <> "EZT" Then
                      If Not UBound(arrayPGM) > 1 Then
                       If UBound(arrayPGM) = 0 Then   ' programma
                        CostruisciCommonInitializeTemplate rst
                        CostruisciOperazioneTemplate Trim(wIstr), 1, rst!idpgm, rst!idOggetto, rst!riga, IIf(IsNull(rst!ordPcb), 0, rst!ordPcb)
                       Else ' copy chiamata da un solo programma
                        ' devo chiamare la stessa funzione COmmonInitialize dei programmi
                        ' ma la riga dove si valorizza il WS-Caller non deve essere scritta
                        ' manometto il nome del programma in modo che diventi un tag e la riga non venga
                        ' scritta, poi lo ripristino
                        NomeProgappo = NomeProg
                        NomeProg = "<todelete>"
                        CostruisciCommonInitializeTemplate rst
                        NomeProg = NomeProgappo
                        CostruisciOperazioneTemplate Trim(wIstr), 1, arrayPGM(1), rst!idOggetto, rst!riga, rst!ordPcb
                       End If
                     
                       'stefano: pcb dinamico, imposta anche il PCB
                       '(VECCHIA VERSIONE NO TEMPLATE)
                       'SQ CPY-ISTR
                       
                    
                       ' PRESENTE NELLA VECCHIA VERSONE
                       'CostruisciDecodifiche rst!IdOggetto, rst!Riga  ' scorporato solo da gruppo Call-Call
                       INSERT_DECOD posizione, RTBModifica, RTErr, rst, typeIncaps
                      Else
                        For i = 1 To UBound(arrayPGM)
                          'rst!idpgm = arrayPGM(i)
                          CaricaIstruzioneC rst, arrayPGM(i)
                          NomeProg = Trim(namePgmFromId(arrayPGM(i)))
                          If InStr(NomeProg, ".") > 0 Then
                           NomeProg = Left(NomeProg, InStr(NomeProg, ".") - 1)
                          End If
                          testo = vbCrLf & FormatTag(Space(indentkey - 7) & "IF LNKDB-CALLER = '" & NomeProg & "'")
                          indentkey = indentkey + 3
                          testo = Left(testo, Len(testo) - 2)
                          CostruisciCommonInitializeTemplateC rst
                          CostruisciOperazioneTemplate Trim(wIstr), 1, arrayPGM(i), rst!idOggetto, rst!riga, rst!ordPcb
                          INSERT_DECODC posizione, RTBModifica, RTErr, rst, typeIncaps
                          indentkey = indentkey - 3
                        Next
                      End If
                    End If
                    lastPos = posizione
                Else
                      If tipoFile <> "EZT" Then
                  INSERT_NODECOD posizione, RTBModifica, rst
                  m_fun.WriteLog "Incaps: Obj(" & rIst!idOggetto & ") Row( " & rst!riga & "non valida", "MadrdF_Incapsulatore"
                  lastPos = posizione
                End If
                    End If
                    
              Else
                'SQ???
                If rst!to_migrate Then
                  m_fun.WriteLog "Instruction not found: Obj(" & rIst!idOggetto & ") Row( " & rst!riga & ")", "MadrdF_Incapsulatore"
                  Debug.Print "Instruction not found: Obj(" & rIst!idOggetto & ") Row(" & rst!riga & ")"
                End If
              End If
            Else
              If Optsource Then
                rsOffset!OffsetEncaps = AddRighe
              Else
                rsOffset!OffsetEncaps = IIf(IsNull(rsOffset!OffsetEncaps), 0, rsOffset!OffsetEncaps) + AddRighe
              End If
            End If
            rsOffset.Update
            rsOffset.Close
            rst.MoveNext
            
            PbarIstr.Value = PbarIstr.Value + 1
          Wend
          If tipoFile = "PLI" Then
    '        If PLIsaveBackKey = "RET" Then
    '          startPLIkeyRet = startPLIkey
    '          insertPLISaveBack "RETURN;", startPLIkeyRet, 0, RTBModifica
    '          insertPLISaveBack "RETURN(", startPLIkeyRet, 0, RTBModifica
    '          insertPLISaveBack "RETURN (", startPLIkeyRet, 0, RTBModifica
    '        Else
    '          insertPLISaveBack PLIsaveBackKey, startPLIkey, 0, RTBModifica
    '        End If
            insertExternalEntry RTBModifica
          End If
          If EmbeddedRoutine Then
              Insert_ProcedureTemplate RTBModifica, RTErr
              Insert_TagRoutine RTBModifica
          End If
          PbarIstr.Value = 0
          
          'CambiaParole RTBModifica, TypeIncaps
          'ControllaUIB RTBModifica, TypeIncaps
          
          'SQ: sospesa gestione PCB...
          'CambiaENTRYTDLI TBModifica, typeIncaps
          'If swCall Then
          ' CambiaPCB RTBModifica, typeIncaps, rIst!IdOggetto
          'End If
           
          RTBModifica.SaveFile OldName, 1
          
          With LvObject.ListItems.Add(, , rIst!idOggetto)
             .SubItems(1) = Replace(OldName, m_fun.FnPathPrj, "...")
             .SubItems(2) = Now
          End With
          
          LvObject.ListItems(LvObject.ListItems.Count).Selected = True
          LvObject.Refresh
          LvObject.SelectedItem.EnsureVisible
          rIst.MoveNext
          
          If PBar.Max < PBar.Value + 1 Then
            PBar.Value = PBar.Max
          Else
            PBar.Value = PBar.Value + 1
          End If
        Wend
        rIst.Close
        
        RTBModifica.SaveFile OldName, 1
      End If
    
      PBar.Value = 0
    Case "Close"
      Unload Me
    Case "Help"
  End Select
  
  Screen.MousePointer = vbDefault
End Sub

Public Sub Resize()
  On Error Resume Next
  Me.Move VSAM2Rdbms.drLeft, VSAM2Rdbms.drTop, VSAM2Rdbms.drWidth, VSAM2Rdbms.drHeight
  Frame2.Move 30, 400, Frame2.Width, (VSAM2Rdbms.drHeight - 400 - Toolbar1.Height) / 3 * 2
  TVSelObj.Move 120, 240, Frame2.Width - 240, Frame2.Height - 360
  Frame1.Move 30, 430 + Frame2.Height, Frame1.Width, ((VSAM2Rdbms.drHeight - 340) / 3) - 60
  LstFunction.Move 120, 240, Frame1.Width - 240, Frame1.Height - 360
  Frame4.Move Frame4.Left, Frame2.Top, VSAM2Rdbms.drWidth - Frame4.Left - 120, Frame2.Height
  LvObject.Move 120, 240, Frame4.Width - 240, Frame4.Height - 420 - PBar.Height - PbarIstr.Height - 60
  PBar.Move LvObject.Left, LvObject.Top + LvObject.Height + 30, LvObject.Width, PBar.Height
  PbarIstr.Move LvObject.Left, PBar.Top + PBar.Height + 30, LvObject.Width, PbarIstr.Height
  Frame3.Move Frame3.Left, Frame1.Top, VSAM2Rdbms.drWidth - Frame3.Left - 120, Frame1.Height
  RTErr.Move 120, 240, Frame3.Width - 240, Frame3.Height - 360
End Sub

Private Sub Form_Activate()
'    If Not VerifyPs Then
'       Unload Me
'    End If
End Sub

Private Sub Form_Load()
  'Setta il parent
  'SetParent Me.hwnd, VSAM2Rdbms.drParent
  'm_fun.ResizeControls Me
  
  '''''''''''''''''''''''''''''''''''''''''''''
  'ATTENZIONE, POSIZIONALE: pericolosissimo!
  ' Usare costanti tipo "EMBEDDED_CHK"
  '''''''''''''''''''''''''''''''''''''''''''''
  LstFunction.AddItem "DLI Features to Rdbms"
  LstFunction.Selected(0) = True
  LstFunction.AddItem "Clear Instructions Key table"
  LstFunction.AddItem "Encapsulate programs for CALL method"
  LstFunction.Selected(2) = True
  LstFunction.AddItem "Encapsulate programs for EXEC method"
  'EMBEDDED_CHK=4
  LstFunction.AddItem "Encapsulate programs for EMBEDDED routines"
  LstFunction.Selected(EMBEDDED_CHK) = False
  EmbeddedRoutine = False

  swCall = True
  
  NumFeatures = 1
  NumObjInParser = 0

  TVSelObj.Nodes.Add , , "SEL", "Only Selected Object"
  TVSelObj.Nodes(1).Checked = True
  TVSelObj.Nodes.Add , , "SRC", "Sources"
  TVSelObj.Nodes.Add "SRC", tvwChild, "CBL", "Cobol Program"
  
  If VSAM2Rdbms.drTipoFinIm <> "" Then
    VSAM2Rdbms.drFinestra.ListItems.Add , , "VSAM2Rdbms: Incasulator Window Loaded at " & Time
  End If
  
  '''''''''''''''''''''''''''''''
  ' SQ 26-07-06
  '''''''''''''''''''''''''''''''
  loadEnvironmentParameters
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
  
End Sub

Private Sub Form_Unload(Cancel As Integer)
  If VSAM2Rdbms.drTipoFinIm <> "" Then
    VSAM2Rdbms.drFinestra.ListItems.Add , , "VSAM2Rdbms: Incapsulator Window Unloaded at " & Time
  End If
  
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False
  
   SetFocusWnd m_fun.FnParent
End Sub

Private Sub TVSelObj_NodeCheck(ByVal Node As MSComctlLib.Node)
Dim k1 As Integer
Dim k2 As Integer
Dim Wkey As String
Dim wCheck As Variant
Dim tb As Recordset

Wkey = Node.key
wCheck = Node.Checked

NumObjInParser = 0

Select Case Node.key
   Case "SEL"
      If Node.Checked = True Then
        For k1 = 1 To VSAM2Rdbms.drObjList.ListItems.Count
          If VSAM2Rdbms.drObjList.ListItems(k1).Selected = True Then
             NumObjInParser = NumObjInParser + 1
          End If
        Next k1
      End If
      
      If TVSelObj.Nodes(Node.Index + 1).Checked = True Then
        TVSelObj.Nodes(Node.Index + 1).Checked = False
        TVSelObj.Nodes(Node.Index + 2).Checked = False
      End If
    
    Case "CBL"
      TVSelObj.Nodes(Node.Index - 1).Checked = Node.Checked
      TVSelObj.Nodes(1).Checked = False
      
    Case Else
      For k1 = Node.Index + 1 To Node.Index + Node.Children
         TVSelObj.Nodes(k1).Checked = Node.Checked
      Next k1
      TVSelObj.Nodes(1).Checked = False
End Select
      
If TVSelObj.Nodes(1).Checked = False Then
   For k1 = 2 To TVSelObj.Nodes.Count
     If TVSelObj.Nodes(k1).Checked = True Then
        Wkey = TVSelObj.Nodes(k1).key
        If Wkey = "MPS" Then
           Wkey = "BMS"
        End If
        Set tb = m_fun.Open_Recordset("Select * from bs_Oggetti where tipo = '" & Wkey & "'")
        If tb.RecordCount > 0 Then
           tb.MoveLast
           NumObjInParser = NumObjInParser + tb.RecordCount
           tb.Close
        End If
     End If
   Next k1
End If

Frame4.Caption = "Activite log for " & NumObjInParser & " Objects"

End Sub
''''''''''
' TMP
''''''''''
Private Sub valueTeam_PCB(Button As MSComctlLib.Button)
  Dim rst As Recordset, rIst As Recordset, rstCloni As Recordset
  Dim typeIncaps As String, wIstr As String, Selezione As String
  Dim MaxVal As Long
  Dim nomeroutine As String
 
  On Error Resume Next
  
  Screen.MousePointer = vbHourglass
  
  Selezione = Restituisci_Selezione(TVSelObj)
  MaxVal = Seleziona_Istruzioni(Selezione, rIst)
  
  If MaxVal = 0 Then
    MaxVal = 1
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
  
  PBar.Max = MaxVal
  PBar.Value = 0
  PbarIstr.Max = 1
  PbarIstr.Value = 0
  
  If rIst.RecordCount Then
    rIst.MoveFirst  'serve!
    While Not rIst.EOF
    
          
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim NCBLcpy As String
  Dim Tip As String
  Dim K As Integer
  Dim w As Long
  Dim ww As Long
  Dim r As Recordset
      
  Set r = m_fun.Open_Recordset("SELECT * FROM BS_Oggetti WHERE Idoggetto = " & rIst!idOggetto)
  If r.RecordCount Then
    If Trim(r!Estensione) = "" Then
       NomeProg = r!nome
    Else
       NomeProg = r!nome & "." & r!Estensione
    End If
    K = InStr(NomeProg, ".")
    If K > 0 Then NomeProg = Trim(Mid$(NomeProg, 1, K - 1))
    DirCBLcpy = Crea_Directory_Progetto(r!Directory_Input, VSAM2Rdbms.drPathDef)
    w = InStr(1, DirCBLcpy, "\")
    While w > 0
      ww = w
      w = InStr(w + 1, DirCBLcpy, "\")
    Wend
    DIROr = Mid(DirCBLcpy, ww + 1)
    If Dir(DirCBLcpy & "\Out", vbDirectory) = "" Then
      MkDir DirCBLcpy & "\Out"
    End If
    OldName = DirCBLcpy & "\Out\" & NomeProg
    OldName1 = NCBLcpy
   End If
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim statement As String, pcbName As String, token As String
  Dim endStatement As Boolean
  Dim fileIN As Long, fileOUT As Long
  Dim line As String
  Dim i As Integer
  
  fileIN = FreeFile
  Open DirCBLcpy & "\" & NomeProg For Input As fileIN
  fileOUT = FreeFile
  Open DirCBLcpy & "\Out\" & NomeProg For Output As fileOUT
  i = 0
  endStatement = False
  While Not EOF(fileIN)
    Line Input #fileIN, line
    'Riscrivo
    ''''Print #fileOUT, line
    'SOLUZIONE 1
    If Mid(line, 7, 9) = " LINKAGE " Then
      'La commento!
      Print #fileOUT, "BPHX-S*" & Mid(line, 7)
      i = 1
    'SOLUZIONE 1
    ElseIf Mid(line, 7, 11) = " PROCEDURE " Then
      Print #fileOUT, "BPHX-S*" & Mid(line, 8)
      If i = 0 Then
        MsgBox "NO LINKAGE??!"
      Else
        i = 0
      End If
      line = RTrim(Mid(line, 8, 65))
       ''''''''''''''''''''''''''''''''''
       ' Becco tutte le linee di call...
       ''''''''''''''''''''''''''''''''''
       statement = line
       If Right(line, 1) <> "." Then
         While Not EOF(fileIN) And Not endStatement
           Line Input #fileIN, line
           Print #fileOUT, "BPHX-S*" & Mid(line, 8)
           If Mid(line, 7, 1) <> "*" Then
             line = Trim(Mid(line, 8, 65))
             ' posso avere una linea vuota? (prob. no...)
             While Len(line) And Not endStatement
                 token = nextToken(line)
                 If Right(token, 1) = "." Then 'If token <> "USING" And SeWordCobol(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Then
                   token = Left(token, Len(token) - 1)
                   endStatement = True
                 End If
                   'linea buona:
                   statement = statement & " " & token '& " " & line
             Wend
             'End If
         End If
         Wend
       Else
         'SQ 14-02-06: se CALL su unica riga, rimane "line" sporca...
         'stefano: vediamo cos�
         statement = Left(statement, Len(statement) - 1)
         line = ""
       End If
       
        i = 1
        Print #fileOUT, "BPHX-S PROCEDURE DIVISION."
'       token = nextToken(statement) 'PROCEDURE
'       token = nextToken(statement) 'DIVISION
'       token = nextToken(statement) 'USING
'       If token = "USING" Then
'          Print #fileOUT, "BPHX-S"
'          statement = Trim(Replace(statement, ",", ""))
'          While Len(statement)
'            i = i + 1
'            pcbName = nextToken(statement)
'            'pcbName = pcbName & "SET ADDRESS OF " & nextToken(statement) & " TO ADDRESS OF REVPCB" & Format(i, "00") & vbCrLf
'            '''''''Print #fileOUT, "BPHX-S" & Space(5) & "SET ADDRESS OF " & pcbName & Space(20 - Len(pcbName)) & " TO ADDRESS OF REVPCB" & Format(i, "00")
'          Wend
'          Print #fileOUT, "BPHX-S"
'       End If
    ElseIf InStr(line, "BPHX-P     SET LNKDB-PTRPCB") Then
      Print #fileOUT, Replace(line, "BPHX-P ", "BPHX-T*")
    Else
      'Riscrivo
      Print #fileOUT, line
    End If
          
'''''''''''''''''''MODO CHE NON FUNZIONA!
''''''    ElseIf Mid(line, 7, 11) = " PROCEDURE " Then
''''''      If i = 0 Then
''''''        MsgBox "NO COPY?!"
''''''      Else
''''''        i = 0
''''''      End If
''''''      line = RTrim(Mid(line, 8, 65))
''''''       ''''''''''''''''''''''''''''''''''
''''''       ' Becco tutte le linee di call...
''''''       ''''''''''''''''''''''''''''''''''
''''''       statement = line
''''''       If Right(line, 1) <> "." Then
''''''         While Not EOF(fileIN) And Not endStatement
''''''           Line Input #fileIN, line
''''''           Print #fileOUT, line
''''''           If Mid(line, 7, 1) <> "*" Then
''''''             line = Trim(Mid(line, 8, 65))
''''''             ' posso avere una linea vuota? (prob. no...)
''''''             While Len(line) And Not endStatement
''''''                 token = nextToken(line)
''''''                 If Right(token, 1) = "." Then 'If token <> "USING" And SeWordCobol(IIf(Right(token, 1) = ".", Left(token, Len(token) - 1), token)) Then
''''''                   token = Left(token, Len(token) - 1)
''''''                   endStatement = True
''''''                 End If
''''''                   'linea buona:
''''''                   statement = statement & " " & token '& " " & line
''''''             Wend
''''''             'End If
''''''         End If
''''''         Wend
''''''       Else
''''''         'SQ 14-02-06: se CALL su unica riga, rimane "line" sporca...
''''''         'stefano: vediamo cos�
''''''         statement = Left(statement, Len(statement) - 1)
''''''         line = ""
''''''       End If
''''''
''''''       token = nextToken(statement) 'PROCEDURE
''''''       token = nextToken(statement) 'DIVISION
''''''       token = nextToken(statement) 'USING
''''''       If token = "USING" Then
''''''          Print #fileOUT, "BPHX-S"
''''''         statement = Trim(Replace(statement, ",", ""))
''''''         While Len(statement)
''''''            i = i + 1
''''''            pcbName = nextToken(statement)
''''''            'pcbName = pcbName & "SET ADDRESS OF " & nextToken(statement) & " TO ADDRESS OF REVPCB" & Format(i, "00") & vbCrLf
''''''            Print #fileOUT, "BPHX-S" & Space(5) & "SET ADDRESS OF " & pcbName & Space(20 - Len(pcbName)) & " TO ADDRESS OF REVPCB" & Format(i, "00")
''''''         Wend
''''''        Print #fileOUT, "BPHX-S"
''''''       End If
''''''    End If
  Wend
  If i = 0 Then
    MsgBox "no PROCEDURE?!"
  End If
  Close fileIN
  Close fileOUT
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          PbarIstr.Value = 0
          With LvObject.ListItems.Add(, , rIst!idOggetto)
             .SubItems(1) = Replace(OldName, m_fun.FnPathPrj, "...")
             .SubItems(2) = Now
          End With
          
          LvObject.ListItems(LvObject.ListItems.Count).Selected = True
          LvObject.Refresh
          LvObject.SelectedItem.EnsureVisible
          
          
          If PBar.Max < PBar.Value + 1 Then
            PBar.Value = PBar.Max
          Else
            PBar.Value = PBar.Value + 1
          End If
        
        rIst.MoveNext
    Wend
  End If
      
  PBar.Value = 0
  Screen.MousePointer = vbDefault
End Sub

