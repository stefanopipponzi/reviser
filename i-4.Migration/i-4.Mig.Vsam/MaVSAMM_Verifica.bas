Attribute VB_Name = "MaVSAMM_Verifica"
Option Explicit

Type Obj
  idOggetto As Long
  nome As String
  Directory As String
End Type

Global Oggetti_ERROR() As Obj
Global Oggetti_SELECT() As Obj

'Type per la lista dei PSB presenti nel progetto fisicamente e non
Type PSBL
  nome As String
  Directory As String
  Esistente As Boolean
End Type

Global PSBList() As PSBL

Global NomePsb As String
Global DirectoryPSB As String
Global IdPSB As Long
Global IdOgg As Long

Public Sub Carica_Array_Oggetti_Errori(TipoErr As String)
  Dim r As ADODB.Recordset
  Dim i As Long
  
  Select Case UCase(Trim(TipoErr))
    Case "TOTAL"
      Set r = m_dllFunctions.Open_Recordset("Select Distinct IdOggetto From BS_Segnalazioni" _
                                          & " Order By IdOggetto")
                           ' Where Codice = 'F02' " & _
                            ' " Or Codice = 'F04'" & _
                            ' " Or Codice = 'F06'" & _
                            ' " Or Codice = 'F07'" & _
                            ' " Or Codice = 'F08'" & _
                            ' " Or Codice = 'F09'" & _
                            ' " Or Codice = 'F10'" & _

      
      If r.RecordCount > 0 Then
        r.MoveFirst
        
        While Not r.EOF
          Screen.MousePointer = vbHourglass
          
          ReDim Preserve Oggetti_ERROR(UBound(Oggetti_ERROR) + 1)
          Oggetti_ERROR(UBound(Oggetti_ERROR)).idOggetto = r!idOggetto
          Oggetti_ERROR(UBound(Oggetti_ERROR)).nome = Restituisci_NomeOgg_Da_IdOggetto(r!idOggetto)
          Oggetti_ERROR(UBound(Oggetti_ERROR)).Directory = Crea_Directory_Progetto(Restituisci_Percorso_Da_IdOggetto(r!idOggetto), VSAM2Rdbms.drPathDef)
          
          r.MoveNext
          
          Screen.MousePointer = vbDefault
        Wend
        
        r.Close
      End If
      
    Case "SELECT"
      For i = 1 To VSAM2Rdbms.drObjList.ListItems.Count
        If VSAM2Rdbms.drObjList.ListItems(i).Selected = True Then
          Screen.MousePointer = vbHourglass
          
          ReDim Preserve Oggetti_SELECT(UBound(Oggetti_SELECT) + 1)
          Oggetti_SELECT(UBound(Oggetti_SELECT)).idOggetto = Val(VSAM2Rdbms.drObjList.ListItems(i).text)
          Oggetti_SELECT(UBound(Oggetti_SELECT)).nome = Restituisci_NomeOgg_Da_IdOggetto(Val(VSAM2Rdbms.drObjList.ListItems(i).text))
          Oggetti_SELECT(UBound(Oggetti_SELECT)).Directory = Crea_Directory_Progetto(Restituisci_Percorso_Da_IdOggetto(Val(VSAM2Rdbms.drObjList.ListItems(i).text)), VSAM2Rdbms.drPathDef)
          
          Screen.MousePointer = vbDefault
        End If
      Next i
  End Select
End Sub

Public Sub Carica_Lista_Oggetti(SQL As Boolean, StringaSQL As String, LSW As ListView, Arr() As Obj, ClearList As Boolean)
  Dim i As Long
  Dim j As Long
  Dim Exist As Boolean
  Dim r As ADODB.Recordset
  Dim NomeOggetto As String
  
  MaVSAMF_Verifica.LswMsg.ListItems.Clear
  MaVSAMF_Verifica.Toolbar1.Buttons(1).Enabled = False
  
  If ClearList = True Then
    LSW.ListItems.Clear
  End If
  
  If SQL = False Then
    For i = 1 To UBound(Arr)
      'Controlla se � gia presente in lista il nome
      For j = 1 To LSW.ListItems.Count
        If Trim(LSW.ListItems(j).text) = Trim(Arr(i).nome) Then
          Exist = True
          Exit For
        End If
      Next j
      
      If Exist = False Then
        LSW.ListItems.Add , , Arr(i).nome
        LSW.ListItems(LSW.ListItems.Count).Tag = Arr(i).idOggetto
      Else
        Exist = False
      End If
    Next i
  Else
    Set r = m_dllFunctions.Open_Recordset(StringaSQL)
    
    If r.RecordCount > 0 Then
      r.MoveFirst
      
      While Not r.EOF
         Screen.MousePointer = vbHourglass
         
         NomeOggetto = Restituisci_NomeOgg_Da_IdOggetto(r!idOggetto)
         
         For j = 1 To LSW.ListItems.Count
            If Trim(LSW.ListItems(j).text) = Trim(NomeOggetto) Then
              Exist = True
              Exit For
            End If
          Next j
      
        If Exist = False Then
          LSW.ListItems.Add , , NomeOggetto
          LSW.ListItems(LSW.ListItems.Count).Tag = r!idOggetto
        Else
          Exist = False
        End If
        
        r.MoveNext
        
        Screen.MousePointer = vbDefault
      Wend
      
      r.Close
    End If
  End If
  
  MaVSAMF_Verifica.Frame1.Caption = "Error Source Summary -- " & LSW.ListItems.Count & " Object(s) in list"
End Sub

Public Sub Carica_Segnalazioni_Da_IdOggetto(LSW As ListView, idOggetto As Long)
  Dim r As ADODB.Recordset
  Dim R1 As ADODB.Recordset
  
  Dim i As Long
  Dim bool As Boolean
  Dim Mess As String
  
  Dim Icona As Long
  
  Elimina_Flickering MaVSAMF_Verifica.hwnd
  
  LSW.ListItems.Clear
  
  Set r = m_dllFunctions.Open_Recordset("Select Distinct IdOggetto,Codice,Riga From BS_Segnalazioni Where IdOggetto = " & idOggetto)
  If r.RecordCount > 0 Then
    
    While Not r.EOF
      bool = False
      
      Mess = Componi_Messaggio_Errore(r!idOggetto, r!Codice, r!riga)
      
      For i = 1 To LSW.ListItems.Count
        If Trim(LSW.ListItems(i).text) = Trim(Mess) Then
          bool = True
          Exit For
        End If
      Next i
      
      If bool = False Then
        Icona = Restituisci_Gravita_Errore(r!idOggetto, r!riga, r!Codice)
        LSW.ListItems.Add , , Mess, , Icona
        LSW.ListItems(LSW.ListItems.Count).Tag = r!Codice
      End If
      
      r.MoveNext
    Wend
    
    r.Close
  End If
  
  Terminate_Flickering
End Sub

Public Function Restituisci_Gravita_Errore(idOggetto As Long, riga As Long, Codice As String) As Long
  'Restituisce il long relativo alla image list
  '1 = Informational;
  '2 = Warning;
  '3 = Several;
  '6 = ERROR;
  
  Dim r As ADODB.Recordset
  
  Set r = m_dllFunctions.Open_Recordset("Select * From BS_Segnalazioni Where IdOggetto = " & idOggetto & " And Riga = " & riga & " And Codice = '" & Codice & "'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    'E' un solo record
    Select Case Trim(UCase(r!Gravita))
      Case "I" 'Information
        Restituisci_Gravita_Errore = 1
      Case "W" 'Warning
        Restituisci_Gravita_Errore = 2
      Case "E"
        Restituisci_Gravita_Errore = 6
      Case "S"
        Restituisci_Gravita_Errore = 3
    End Select
    
    r.Close
  End If
End Function

Public Function Restituisci_Codice_Errore_Da_IdOggetto(idOggetto As Long, riga As Long, Codice As String) As String
  'Restituisce il long relativo alla image list
  '1 = Informational;
  '2 = Warning;
  '3 = Several;
  '6 = ERROR;
  
  Dim r As ADODB.Recordset
  
  Set r = m_dllFunctions.Open_Recordset("Select * From BS_Segnalazioni Where IdOggetto = " & idOggetto & " And Riga = " & riga & " And Codice = '" & Codice & "'")
  
  If r.RecordCount > 0 Then
    r.MoveFirst
    
    'E' un solo record
    Restituisci_Codice_Errore_Da_IdOggetto = r!Codice
    
    r.Close
  End If
End Function

Public Sub Carica_Lista_PSB(LSW As ListView, Tipo As String, lbl As Frame)
  Dim i As Long
  Dim j As Long
  Dim Icona As Long
  Dim rs As ADODB.Recordset
  
  LSW.ListItems.Clear
  
  Select Case Trim(UCase(Tipo))
    Case "ALL"
      For i = 1 To UBound(PSBList)
        If PSBList(i).Esistente = True Then
          Icona = 5
        Else
          Icona = 4
        End If

        LSW.ListItems.Add , , PSBList(i).nome, , Icona
        LSW.ListItems(LSW.ListItems.Count).Tag = PSBList(i).Directory
        
      Next i
       
    Case "INTERNAL"
       Set rs = m_dllFunctions.Open_Recordset("Select * From BS_Oggetti Where Tipo = 'PSB' Order by Nome")
       
       If rs.RecordCount > 0 Then
         While Not rs.EOF
            LSW.ListItems.Add , , rs!nome, , 5
            LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , m_dllFunctions.Crea_Directory_Progetto(rs!Directory_Input, VSAM2Rdbms.drPathDb)
            
            rs.MoveNext
         Wend
       End If
       
       rs.Close
       
    Case "EXTERNAL"
       For i = 1 To UBound(PSBList)
        If PSBList(i).Esistente = False Then
          LSW.ListItems.Add , , PSBList(i).nome, , 4
          LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , PSBList(i).Directory
          
        End If
      Next i
  End Select
  
  lbl.Caption = "PSB File found in project N� " & LSW.ListItems.Count
  
End Sub

Public Sub Carica_Dipendenze_PROGRAMMI(NomeOgg As String, IdOgg As Long, LSW As ListView, LswCal As ListView)
  Dim r As ADODB.Recordset
  Dim R1 As ADODB.Recordset
  Dim c As Long
  Dim i As Long
  
  LSW.ListItems.Clear
  LswCal.ListItems.Clear
  
  'Aggiunge il chiamante primario
  LswCal.ListItems.Add , , NomeOgg
  LswCal.ListItems(LswCal.ListItems.Count).ListSubItems.Add , , "PRIMARY"
  'MaVSAMF_Verifica.txtPsb.Text = ""
  
  Set r = m_dllFunctions.Open_Recordset("Select b.nome as PSB ,a.IdOggettoR as IdOggettoR From PSRel_Obj as a,bs_oggetti as b Where IdOggettoC = " & IdOgg & " And Relazione = 'PSB' and a.IdOggettoR =b.idOggetto")
  If r.RecordCount = 0 Then
    'isDC?
  End If
  If r.RecordCount > 0 Then
    While Not r.EOF
      LSW.ListItems.Add , , r!psb
      
      LSW.ListItems(LSW.ListItems.Count).Tag = r!IdOggettoR
      LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , "0"
      
      'ASSEGNA IL PSB ALLA TEXTBOX
      MaVSAMF_Verifica.txtPsb.text = r!psb
      
      r.Close
      
      Exit Sub
      r.MoveNext
    Wend
  End If
  
  Set r = m_dllFunctions.Open_Recordset("Select a.IdOggettoC as IdOggettoC, b.Nome as Nome From PSRel_Obj as a,Bs_Oggetti as b Where IdOggettoR = " & IdOgg & " and b.IdOggetto = a.IdOggettoC And Relazione = 'CAL'")
  While Not r.EOF
      LswCal.ListItems.Add , , r!nome
      LswCal.ListItems(LswCal.ListItems.Count).ListSubItems.Add , , "CALL"
      
      Trova_PSB_Dipendente LswCal, LSW, r!IdOggettoC, 1
      
      r.MoveNext
      c = c + 1
      DoEvents
  Wend
  r.Close
  
  If MaVSAMF_Verifica.txtPsb.text = "" Then
    
  '*******************  PSB con stesso nome programma
   Set r = m_dllFunctions.Open_Recordset("select idOggetto from bs_oggetti where idOggetto = " & IdOgg & " and Ims")
      
   If Not r.EOF Then
    r.Close
    ReDim psbPgm(0)
    Set r = m_dllFunctions.Open_Recordset("select idOggetto from bs_oggetti where nome = '" & NomeOgg & "' and tipo = 'PSB'")
    If r.RecordCount = 0 Then
      Set r = m_dllFunctions.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.source = '" & NomeOgg & "' and b.load=a.nome and a.tipo = 'PSB'")
    End If
    If r.RecordCount Then
      MaVSAMF_Verifica.txtPsb.text = NomeOgg
    Else
       LSW.ListItems.Add , , "No PSB Linking to this program..."
    End If
    r.Close
   Else
    r.Close
    LSW.ListItems.Add , , "No PSB Linking to this program..."
   End If
    
    
  End If
  
  'Carica le liste con 25 item di default
  For i = 1 To 25 - LSW.ListItems.Count
    LSW.ListItems.Add , , ""
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , ""
    DoEvents
  Next i
  
   For i = 1 To 25 - LswCal.ListItems.Count
    LswCal.ListItems.Add , , ""
    LswCal.ListItems(LswCal.ListItems.Count).ListSubItems.Add , , ""
    DoEvents
   Next i
End Sub
Public Sub Carica_Dipendenze_PROGRAMMILoad(NomeOgg As String, IdOgg As Long, LSW As ListView, LswCal As ListView)
  Dim r As ADODB.Recordset
  Dim R1 As ADODB.Recordset
  Dim c As Long
  Dim i As Long
  
  LSW.ListItems.Clear
  LswCal.ListItems.Clear
  
  'Aggiunge il chiamante primario
  LswCal.ListItems.Add , , NomeOgg
  LswCal.ListItems(LswCal.ListItems.Count).ListSubItems.Add , , "PRIMARY"
  MaVSAMF_Verifica.txtPsb.text = ""
  
  '******** eliminato giro PSRel_Obj
  Set r = m_dllFunctions.Open_Recordset("Select distinct b.nome as PSB ,a.IdPsb as IdPsb From PsDli_IstrPSB as a,bs_oggetti as b Where a.IdOggetto = " & IdOgg & "  and a.IdPsb = b.idOggetto")
  
  
    While Not r.EOF
      LSW.ListItems.Add , , r!psb
      
      LSW.ListItems(LSW.ListItems.Count).Tag = r!IdPSB
      LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , "0"
      
      'ASSEGNA IL PSB ALLA TEXTBOX
      If MaVSAMF_Verifica.txtPsb.text = "" Then
        MaVSAMF_Verifica.txtPsb.text = r!psb
      Else
        MaVSAMF_Verifica.txtPsb.text = MaVSAMF_Verifica.txtPsb.text & ";" & r!psb
      End If
      
      r.Close
      
      Exit Sub
      r.MoveNext
    Wend
  
  
  
  If MaVSAMF_Verifica.txtPsb.text = "" Then
    
  '*******************  PSB con stesso nome programma
   Set r = m_dllFunctions.Open_Recordset("select idOggetto from bs_oggetti where idOggetto = " & IdOgg & " and Ims")
      
   If Not r.EOF Then
    r.Close
    ReDim psbPgm(0)
    Set r = m_dllFunctions.Open_Recordset("select idOggetto from bs_oggetti where nome = '" & NomeOgg & "' and tipo = 'PSB'")
    If r.RecordCount = 0 Then
      Set r = m_dllFunctions.Open_Recordset("select a.idOggetto from bs_oggetti as a,PsLoad as b where b.source = '" & NomeOgg & "' and b.load=a.nome and a.tipo = 'PSB'")
    End If
    If r.RecordCount Then
      MaVSAMF_Verifica.txtPsb.text = NomeOgg
    Else
       LSW.ListItems.Add , , "No PSB Linking to this program..."
    End If
    r.Close
   Else
    r.Close
    LSW.ListItems.Add , , "No PSB Linking to this program..."
   End If
    
    
  End If
  
  'Carica le liste con 25 item di default
  For i = 1 To 25 - LSW.ListItems.Count
    LSW.ListItems.Add , , ""
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , ""
    DoEvents
  Next i
  
   For i = 1 To 25 - LswCal.ListItems.Count
    LswCal.ListItems.Add , , ""
    LswCal.ListItems(LswCal.ListItems.Count).ListSubItems.Add , , ""
    DoEvents
   Next i
End Sub


Public Sub Trova_PSB_Dipendente(LswCal As ListView, LSW As ListView, IdOgg As Long, Contatore As Long)
  Dim r As ADODB.Recordset
  Dim R1 As ADODB.Recordset
  Dim RObj As New ADODB.Recordset
  Dim mycount As Long
  
  Static Count As Long
  
  If Contatore = 0 Then Count = 1
  
  mycount = Contatore + 1
  
  Set r = m_dllFunctions.Open_Recordset("Select a.*,b.Nome From PsRel_Obj as a,Bs_Oggetti as b Where a.IdOggettoC = " & IdOgg & " And a.Relazione = 'PSB' and a.IdOggettoR = b.IdOggetto ")
  
  If r.RecordCount > 0 Then
    
    LSW.ListItems.Add , , r!nome '& ".PSB"
    LSW.ListItems(LSW.ListItems.Count).Tag = r!IdOggettoR
    
    'LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , Count
    LSW.ListItems(LSW.ListItems.Count).ListSubItems.Add , , Contatore
    
    'ASSEGNA IL PSB ALLA TEXTBOX
    'AC
    'MaVSAMF_Verifica.txtPsb.Text = r!NomeComponente & ".PSB"
    
    r.Close
    
  Else
    If Count > 5 Then
      'LSW.ListItems.Add , , "No PSB Linking to this program..."
      'AC
      'MaVSAMF_Verifica.txtPsb.Text = ""
      Exit Sub
    End If
  
    'Cerca Oggetti di tipo CAL e rilancia la funzione
    Set R1 = m_dllFunctions.Open_Recordset("Select a.IdOggettoC as IdOggettoC, b.Nome as Nome From PSRel_Obj as a,Bs_Oggetti as b Where IdOggettoR = " & IdOgg & " and b.IdOggetto = a.IdOggettoC And Relazione = 'CAL'")
  
    If R1.RecordCount > 0 Then
     
      
      While Not R1.EOF
        Count = Count + 1
        'Contatore = Count
        
        LswCal.ListItems.Add , , R1!nome '& ".PSB"
        LswCal.ListItems(LswCal.ListItems.Count).ListSubItems.Add , , "CALL"
        
        'ASSEGNA IL PSB ALLA TEXTBOX
        ''MaVSAMF_Verifica.txtPsb.Text = R1!NomeComponente & ".PSB"  ???
        
        'Trova_PSB_Dipendente LswCal, LSW, R1!IdOggettoC, Contatore
        Trova_PSB_Dipendente LswCal, LSW, R1!IdOggettoC, mycount
        
        R1.MoveNext
        
        If Count > 5 Then Exit Sub
        
      Wend
      
      If Count > 5 Then
        LSW.ListItems.Add , , "No PSB Linking to this program..."
        'MaVSAMF_Verifica.txtPsb.Text = ""
        Exit Sub
      End If
      
      R1.Close
    Else
      LSW.ListItems.Add , , "No PSB Linking to this program..."
      'AC
      'MaVSAMF_Verifica.txtPsb.Text = ""

    End If
  End If
  
  
End Sub

Public Function Conta_Righe_File(percorso As String) As Long
  Dim righe As Long
  Dim numfile As Long
  Dim line As String
  
  numfile = FreeFile
  
  'conta le righe
  Open percorso For Input As #numfile
  
  Do While Not EOF(numfile)
    Line Input #numfile, line
    righe = righe + 1
  Loop
  
  Close #numfile
  
  Conta_Righe_File = righe
End Function

Public Sub Scrivi_Relazione_PSB(idOggetto As Long, IdPSB As Long, NomePsb As String)
  Dim r As ADODB.Recordset
  
  'SG: Elimina l'associazione dei psb
  m_dllFunctions.FnConnection.Execute "delete  * From PsRel_Obj Where IdOggettoC = " & idOggetto & " And relazione = 'PSB'"
  
  Set r = m_dllFunctions.Open_Recordset("Select * From PsRel_Obj Where IdOggettoC = " & idOggetto & " And IdOggettoR = " & IdPSB & " And Relazione = 'PSB'")
  
  If r.RecordCount = 0 Then
    r.AddNew
      r!IdOggettoC = idOggetto
      r!IdOggettoR = IdPSB
      r!Relazione = "PSB"
      r!NomeComponente = NomePsb
      r!Utilizzo = "PSB"
    r.Update
  Else
    'va in edit
      r!IdOggettoC = idOggetto
      r!IdOggettoR = IdPSB
      r!Relazione = "PSB"
      r!NomeComponente = NomePsb
      r!Utilizzo = "PSB"
    r.Update
    r.Close
  End If
End Sub

Public Sub check_Move_Op_SSA(WidOggetto As Long)
   Dim rs As ADODB.Recordset
   Dim rsSSA As ADODB.Recordset
   Dim cSSA As New collection
   Dim rsArea As ADODB.Recordset
   Dim rsCmp As ADODB.Recordset
   Dim wFound As Boolean
   Dim rsLink As ADODB.Recordset
   Dim cValue As String
   Dim i As Long
   
   Set rs = m_dllFunctions.Open_Recordset("Select Distinct NomeSSA From PsDli_IstrDett_liv Where idOggetto = " & WidOggetto & " and nomeSsa <> ''")
   While Not rs.EOF
      'Apre la struttura dell'SSA
      Set rsArea = m_dllFunctions.Open_Recordset("Select * From PsData_Cmp Where Nome = '" & Trim(rs!NomeSSA) & "' And IdOggetto = " & WidOggetto)
      'SG : Ricerca il campo nelle copy linkate da questo id oggetto
      If rsArea.RecordCount = 0 Then
        Set rsLink = m_dllFunctions.Open_Recordset("Select * From PsRel_Obj Where IdOggettoC = " & WidOggetto & " And Relazione = 'CPY'")
        Do While Not rsLink.EOF
          Set rsCmp = m_dllFunctions.Open_Recordset("Select * From PsDATA_Cmp Where Nome = '" & Trim(rs!NomeSSA) & "' And IdOggetto = " & rsLink!IdOggettoR)
          If rsCmp.RecordCount Then
             Set rsArea = rsCmp
             Exit Do
          End If
          rsLink.MoveNext
        Loop
        rsLink.Close
      End If
      
      wFound = False
      
  'Ho la struttura dell'SSA
     If rsArea.RecordCount Then
        Set rsSSA = m_dllFunctions.Open_Recordset("Select * From PsData_Cmp Where IdOggetto = " & rsArea!idOggetto & " And IdArea = " & rsArea!IdArea & " Order by Ordinale")
        While Not rsSSA.EOF
           cValue = Replace(rsSSA!Valore, "'", "")
           wFound = False
           
           If Trim(cValue) = "=" Then
              wFound = True
           ElseIf Trim(cValue) = ">" Then
              wFound = True
           ElseIf Trim(cValue) = ">=" Then
              wFound = True
           ElseIf Trim(cValue) = "=>" Then
              wFound = True
           ElseIf Trim(cValue) = "<" Then
              wFound = True
           ElseIf Trim(cValue) = "<=" Then
              wFound = True
           ElseIf Trim(cValue) = "=<" Then
              wFound = True
           ElseIf Trim(cValue) = "<>" Then
              wFound = True
           End If
           
           If wFound Then
              If Trim(rsSSA!nome) <> "FILLER" Then
                 wFound = False
                 
                 For i = 1 To cSSA.Count
                    If Trim(rsSSA!nome) = Trim(cSSA(i)) Then
                       wFound = True
                       Exit For
                    End If
                 Next i
                 
                 If Not wFound Then
                    cSSA.Add Trim(rsSSA!nome)
                 End If
              End If
           End If
           
           rsSSA.MoveNext
        Wend
        
        
     End If
     
     rsArea.Close
     
     rs.MoveNext
   Wend
   rs.Close
   
   'Legge il file riga a riga
   Dim percorso As String
   Dim nFile As Long
   Dim sLine As String
   Dim fLen As Long
   Dim wWord As String
   Dim NomeFile As String
   
   nFile = FreeFile
   
   percorso = m_dllFunctions.Restituisci_Directory_Da_IdOggetto(WidOggetto)
   NomeFile = m_dllFunctions.Restituisci_NomeOgg_Da_IdOggetto(WidOggetto)
   
   fLen = FileLen(percorso & "\" & NomeFile)
   
   Open percorso & "\" & NomeFile For Input As nFile
   
   Do While Not EOF(nFile)
      Screen.MousePointer = vbHourglass
      
      Line Input #nFile, sLine
      
      If Mid(sLine, 7, 1) <> "*" Then
         sLine = Mid(sLine, 1, 72)
         sLine = Mid(sLine, 8)
         
         If InStr(1, sLine, "MOVE ") > 0 Then
            If InStr(1, sLine, " TO ") > 0 Then
               wWord = Trim(Mid(sLine, InStr(1, sLine, " TO ") + 4)) & " "
               wWord = Trim(Mid(wWord, 1, InStr(1, wWord, " ")))
               wWord = Trim(Replace(wWord, ".", ""))
               
               For i = 1 To cSSA.Count
                  If wWord = cSSA(i) Then
                     'Scrive il file
                     m_dllFunctions.WriteLog NomeFile & " --> " & percorso, , "C:\MoveSSA.txt"
                     Exit Do
                  End If
               Next i
            End If
         End If
         
      End If
      Screen.MousePointer = vbDefault
      DoEvents
   Loop
   
   Close nFile
   
End Sub

