VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form MavsdF_GenRout 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "VSAM - Routine Generator"
   ClientHeight    =   7200
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   11385
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7200
   ScaleWidth      =   11385
   ShowInTaskbar   =   0   'False
   Begin VB.OptionButton optOra 
      Caption         =   "ORACLE"
      ForeColor       =   &H00400000&
      Height          =   255
      Left            =   9600
      TabIndex        =   42
      Top             =   5760
      Width           =   1215
   End
   Begin VB.OptionButton optDB2 
      Caption         =   "DB2"
      ForeColor       =   &H00400000&
      Height          =   255
      Left            =   8280
      TabIndex        =   41
      Top             =   5760
      Width           =   1095
   End
   Begin RichTextLib.RichTextBox Rtb 
      Height          =   375
      Left            =   5880
      TabIndex        =   40
      Top             =   2880
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MavsdF_GenRout.frx":0000
   End
   Begin RichTextLib.RichTextBox RtbOccCopy 
      Height          =   375
      Left            =   1680
      TabIndex        =   39
      Top             =   4560
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MavsdF_GenRout.frx":0082
   End
   Begin RichTextLib.RichTextBox RtbInsert 
      Height          =   375
      Left            =   3480
      TabIndex        =   38
      Top             =   4920
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MavsdF_GenRout.frx":010B
   End
   Begin RichTextLib.RichTextBox RtbSqlAppo 
      Height          =   375
      Left            =   6600
      TabIndex        =   37
      Top             =   4560
      Visible         =   0   'False
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MavsdF_GenRout.frx":0193
   End
   Begin RichTextLib.RichTextBox RtbDisp 
      Height          =   375
      Left            =   480
      TabIndex        =   36
      Top             =   4920
      Visible         =   0   'False
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MavsdF_GenRout.frx":021C
   End
   Begin RichTextLib.RichTextBox RtbUpdate 
      Height          =   495
      Left            =   3480
      TabIndex        =   35
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      _Version        =   393217
      TextRTF         =   $"MavsdF_GenRout.frx":02A2
   End
   Begin RichTextLib.RichTextBox RtbOccurs 
      Height          =   375
      Left            =   1920
      TabIndex        =   34
      Top             =   4080
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MavsdF_GenRout.frx":032A
   End
   Begin RichTextLib.RichTextBox RtbCiclaLivello 
      Height          =   375
      Left            =   5280
      TabIndex        =   33
      Top             =   4560
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MavsdF_GenRout.frx":03B2
   End
   Begin RichTextLib.RichTextBox RtbCiclica 
      Height          =   375
      Left            =   5280
      TabIndex        =   32
      Top             =   3960
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MavsdF_GenRout.frx":0440
   End
   Begin RichTextLib.RichTextBox RtbPerform 
      Height          =   375
      Left            =   120
      TabIndex        =   31
      Top             =   3720
      Visible         =   0   'False
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MavsdF_GenRout.frx":04C9
   End
   Begin RichTextLib.RichTextBox RtbRedCopy 
      Height          =   375
      Left            =   480
      TabIndex        =   30
      Top             =   4560
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MavsdF_GenRout.frx":0552
   End
   Begin RichTextLib.RichTextBox RtbRed 
      Height          =   375
      Left            =   840
      TabIndex        =   29
      Top             =   4080
      Visible         =   0   'False
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MavsdF_GenRout.frx":05DB
   End
   Begin RichTextLib.RichTextBox RtbSql 
      Height          =   375
      Left            =   6600
      TabIndex        =   28
      Top             =   3960
      Visible         =   0   'False
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MavsdF_GenRout.frx":0660
   End
   Begin VB.Frame Frame1 
      Caption         =   "Routine for:"
      ForeColor       =   &H00C00000&
      Height          =   1215
      Left            =   7650
      TabIndex        =   22
      Top             =   330
      Width           =   3675
      Begin VB.OptionButton optType 
         Caption         =   "ALL Instructions"
         ForeColor       =   &H00C00000&
         Height          =   195
         Index           =   2
         Left            =   1230
         TabIndex        =   25
         Top             =   840
         Width           =   1575
      End
      Begin VB.OptionButton optType 
         Caption         =   "Selected PGM"
         ForeColor       =   &H00C00000&
         Height          =   195
         Index           =   1
         Left            =   1230
         TabIndex        =   24
         Top             =   540
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton optType 
         Caption         =   "Selected VSAM"
         ForeColor       =   &H00C00000&
         Height          =   195
         Index           =   0
         Left            =   1230
         TabIndex        =   23
         Top             =   240
         Width           =   1965
      End
   End
   Begin VB.ListBox LstFunction 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   735
      Left            =   7620
      Style           =   1  'Checkbox
      TabIndex        =   19
      Top             =   6420
      Width           =   3705
   End
   Begin RichTextLib.RichTextBox RtbDel 
      Height          =   315
      Left            =   3480
      TabIndex        =   18
      Top             =   4560
      Visible         =   0   'False
      Width           =   1425
      _ExtentX        =   2514
      _ExtentY        =   556
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MavsdF_GenRout.frx":06E5
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   336
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   11388
      _ExtentX        =   20082
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Camera"
            Object.ToolTipText     =   "Prepare Routines migration"
            ImageKey        =   "Camera"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Forward"
            Object.ToolTipText     =   "Start Routines migration"
            ImageKey        =   "Forward"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Close"
            Object.ToolTipText     =   "Exit"
            ImageKey        =   "Close"
         EndProperty
      EndProperty
      Begin VB.TextBox Text2 
         Height          =   315
         Left            =   4680
         TabIndex        =   21
         Top             =   0
         Visible         =   0   'False
         Width           =   1065
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Left            =   3480
         TabIndex        =   20
         Top             =   0
         Visible         =   0   'False
         Width           =   1065
      End
   End
   Begin RichTextLib.RichTextBox RTBErr2 
      Height          =   1695
      Left            =   0
      TabIndex        =   15
      Top             =   5490
      Width           =   7605
      _ExtentX        =   13414
      _ExtentY        =   2990
      _Version        =   393217
      BackColor       =   16777152
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   10000
      TextRTF         =   $"MavsdF_GenRout.frx":0756
   End
   Begin RichTextLib.RichTextBox RTBErr 
      Height          =   1875
      Left            =   0
      TabIndex        =   14
      Top             =   3600
      Width           =   7605
      _ExtentX        =   13414
      _ExtentY        =   3307
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   3
      RightMargin     =   10000
      TextRTF         =   $"MavsdF_GenRout.frx":07D8
   End
   Begin RichTextLib.RichTextBox RtLog 
      Height          =   3255
      Left            =   7650
      TabIndex        =   10
      Top             =   2430
      Width           =   3675
      _ExtentX        =   6482
      _ExtentY        =   5741
      _Version        =   393217
      BackColor       =   -2147483624
      Enabled         =   -1  'True
      ScrollBars      =   3
      TextRTF         =   $"MavsdF_GenRout.frx":085A
   End
   Begin RichTextLib.RichTextBox RTBR 
      Height          =   285
      Left            =   3750
      TabIndex        =   9
      Top             =   2910
      Visible         =   0   'False
      Width           =   1155
      _ExtentX        =   2037
      _ExtentY        =   503
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"MavsdF_GenRout.frx":08DC
   End
   Begin MSComctlLib.TreeView TreeView2 
      Height          =   2745
      Left            =   3480
      TabIndex        =   6
      Top             =   840
      Width           =   4125
      _ExtentX        =   7276
      _ExtentY        =   4842
      _Version        =   393217
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   2745
      Left            =   0
      TabIndex        =   5
      Top             =   840
      Width           =   3465
      _ExtentX        =   6112
      _ExtentY        =   4842
      _Version        =   393217
      Indentation     =   1058
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   5055
      Top             =   3870
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_GenRout.frx":094B
            Key             =   "Camera"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_GenRout.frx":0A5D
            Key             =   "Forward"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MavsdF_GenRout.frx":0B6F
            Key             =   "Close"
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTB_Trig 
      Height          =   375
      Left            =   5820
      TabIndex        =   43
      Top             =   5040
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393217
      TextRTF         =   $"MavsdF_GenRout.frx":0D49
   End
   Begin VB.Label Label13 
      Alignment       =   2  'Center
      Caption         =   "/"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   10500
      TabIndex        =   27
      Top             =   6090
      Width           =   165
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   555
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Total Instructions:"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   7650
      TabIndex        =   17
      Top             =   1560
      Width           =   2415
   End
   Begin VB.Label Label11 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   10740
      TabIndex        =   13
      Top             =   6090
      Width           =   555
   End
   Begin VB.Label Label10 
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Processed Instructions:"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   7650
      TabIndex        =   12
      Top             =   6090
      Width           =   1905
   End
   Begin VB.Label Label9 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   9870
      TabIndex        =   11
      Top             =   6090
      Width           =   555
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      BackColor       =   &H8000000D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Routines - Instructions"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   405
      Left            =   3480
      TabIndex        =   8
      Top             =   450
      Width           =   4125
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      BackColor       =   &H8000000D&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Instructions - Programs"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   375
      Left            =   30
      TabIndex        =   7
      Top             =   450
      Width           =   3435
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   10140
      TabIndex        =   4
      Top             =   2130
      Width           =   1185
   End
   Begin VB.Label Label5 
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Duplicate Instructions:"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   7650
      TabIndex        =   3
      Top             =   2130
      Width           =   2415
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   10140
      TabIndex        =   2
      Top             =   1830
      Width           =   1185
   End
   Begin VB.Label Label3 
      BorderStyle     =   1  'Fixed Single
      Caption         =   " Processed Instructions:"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   7650
      TabIndex        =   1
      Top             =   1830
      Width           =   2415
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   10140
      TabIndex        =   0
      Top             =   1560
      Width           =   1185
   End
End
Attribute VB_Name = "MavsdF_GenRout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'SQ: utilizzare questo metodo:
'Const ..._CHK = 0
'Const ..._CHK = 1
'Const ..._CHK = 2
'Const ..._CHK = 3
Const EMBEDDED_CHK = 4

Private tipoDb As String
'Public setDBName As String

Private Sub Form_Resize()
    ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
   m_fun.RemoveActiveWindows Me
   m_fun.FnActiveWindowsBool = False
   
   SetFocusWnd m_fun.FnParent
End Sub


Private Sub optDB2_Click()
    tipoDb = "DMDB2_"
End Sub

Private Sub optOra_Click()
    tipoDb = "DMORC_"
End Sub

Private Sub Text1_KeyDown(KeyCode As Integer, Shift As Integer)
   m_suffix = Text1.text
End Sub

Private Sub LstFunction_ItemCheck(Item As Integer)
  'ATTENZIONE, POSIZIONALE: pericolosissimo!
  'MavsdM_Incapsulatore.EmbeddedRoutine = LstFunction.Selected(EMBEDDED_CHK)
  'If LstFunction.ListCount > EMBEDDED_CHK Then
  '  EmbeddedRoutine = LstFunction.Selected(EMBEDDED_CHK)
  'End If
   
  If Item = 0 Then LstFunction.Selected(1) = False
  If Item = 1 Then LstFunction.Selected(0) = False
  If Item = 2 Then LstFunction.Selected(3) = False
  If Item = 3 Then LstFunction.Selected(2) = False
End Sub
Private Sub Text2_KeyDown(KeyCode As Integer, Shift As Integer)
   m_author = Text2.text
End Sub
Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Dim K As Integer, y As Integer
  Dim Wkey As String, wIstr As String, Nomedb As String
  Dim idOggettoTxt As String, rigaTxt As String, operator As String
  Dim OldNomeRout As String
  
  On Error GoTo ErrNext
  
  Select Case Button.key
    Case "Camera"
      getPhoto
    Case "Forward" 'Genera le routine
      ReDim tabOk(0)
      ReDim istrOk(0)
      ReDim tabOk2(0)
      ReDim istrOk2(0)
      ReDim istrOkDet2(0)
      idxTabOkMax = 0
      idxTabOkMax2 = 0
      
      'passa l'informazione sul db
      setDBName = tipoDb
      
      totIstr = 0
      RtLog.text = ""
            
      Label11 = Label4
      Label9 = 0
      
      swCall = LstFunction.Selected(2)
      
      AggLog " --> Routines creation has started...", RtLog
      
      For K = 1 To TreeView2.Nodes.Count
        'stefanopippo: treeview2.nodes.count non � il numero totale delle istruzioni
        'contiene anche le righe per i livelli
        'Label9 = k
        'Label9.Refresh
        Wkey = Left(TreeView2.Nodes(K).key, 8)
        
        If Left(Wkey, 1) = "#" And Mid(Wkey, 3, 1) = "#" And Mid(Wkey, 8, 1) = "#" Then
          nomeroutine = Mid(TreeView2.Nodes(K).key, 9)
          'SQ - EMBEDDED
          EmbeddedRoutine = nomeroutine = "<EMBEDDED>"
          Nomedb = TreeView2.Nodes(K).text
          y = InStr(Nomedb, " ")
          Nomedb = Left(Nomedb, y - 1)
          gbCurInstr = Replace(Mid(Wkey, 4, 4), ".", "")
          ' stefano: fa schifo, ma finch� non tiriamo via sta gestione "castrante" della treeview
          ' basata su nomi fissi....
          isGsam = "GS" = Right(TreeView2.Nodes(K).text, 2)
          SwTp = Left(Wkey, 3) = "#T#"
          
          If nomeroutine <> OldNomeRout Then
            If LstFunction.Selected(1) Then
              initRoutinesModule_DLI RTBR, Nomedb
            Else
              'SQ - EMBEDDED
              If Not EmbeddedRoutine Then
                initRoutinesModule_DB2 Nomedb, RTBR
              Else
                If tipoDb = "DMDB2_" Then
                    MavsdM_GenRout.percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\" & IIf(SwTp, "CxRout\", "BtRout\")
                Else
                    MavsdM_GenRout.percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\oracle\" & IIf(SwTp, "CxRout\", "BtRout\")
                End If
              End If
            End If
            
            OldNomeRout = nomeroutine
          End If
        Else
          Wkey = TreeView2.Nodes(K).key
          If InStr(Wkey, "-") = 0 Then
            AggLog "  Instruction: " & Wkey, RtLog
            wIstr = Trim(Wkey)
          Else
            Wkey = TreeView2.Nodes(K).text
            'SQ CPY-ISTR
            'idPgm:
            y = InStr(Wkey, ":")
            idpgm = Mid(Wkey, y + 1, InStr(Wkey, " ") - y)
            'idOggetto:
            'idOggettoTxt = Mid(Wkey, InStr(Wkey, "(") + 4)
            'y = InStr(idOggettoTxt, " ")
            'If y > 0 Then idOggettoTxt = Left(idOggettoTxt, y - 1)
            'mIdOggettoCorrente = Val(idOggettoTxt)
            y = InStr(y + 1, Wkey, ":")
            mIdOggettoCorrente = Mid(Wkey, y + 1, InStr(y, Wkey, " ") - y)
            'Riga:
            'rigaTxt = Mid(Wkey, InStr(Wkey, "Riga:") + 5)
            'y = InStr(rigaTxt, ")")
            'If y > 0 Then rigaTxt = Left(rigaTxt, y - 1)
            'mRiga = Val(rigaTxt)
            y = InStr(y + 1, Wkey, ":")
            mRiga = Mid(Wkey, y + 1, InStr(y, Wkey, ")") - 1 - y)
            
            'operatore:
            'SQ - A cosa serve?!!!!!!
            'y = InStr(Wkey, "Id")
            'y = InStr(Wkey, "IdPgm")
            'If y < 4 Then
            '  operator = ""
            'Else
            '  If InStr(Wkey, "<NOOP>") Then
            '     operator = Left(Wkey, 6)
            '  Else
            '     operator = Left(Wkey, 2)
            '  End If
            'End If
             
            nomeroutine = Mid(TreeView2.Nodes(K).parent.parent.key, 9)
            
            If LstFunction.Selected(1) Then
              'SQ
              createRoutine_DLI RTBR, wIstr
            Else
              'SQ
              createRoutine_DB2 RTBR, wIstr
            End If
            
            Label9 = totIstr
            Label9.Refresh
          End If
        End If
      Next K
       
      'Salva l'ultima routine fuori ciclo
      If Len(RTBR.text) Then
        RTBR.SaveFile RTBR.fileName, 1
        RTBR.text = ""
      End If
      
      'stefano: provo con le copy!!!
      If Len(GbTestoRedOcc) Then
        RTBR.text = GbTestoRedOcc
        Dim pos As Integer
        pos = InStr(VSAM2Rdbms.drNomeDB, ".")
        If tipoDb = "DMDB2_" Then
            RTBR.SaveFile VSAM2Rdbms.drPathDb & "\" & Left(VSAM2Rdbms.drNomeDB, pos - 1) & "\output-prj\db2\cpy\" & GbFileRedOcc, 1
        Else
            RTBR.SaveFile VSAM2Rdbms.drPathDb & "\" & Left(VSAM2Rdbms.drNomeDB, pos - 1) & "\output-prj\oracle\cpy\" & GbFileRedOcc, 1
        End If
    
      End If
'???
'''''      GbTestoRedOcc = ""
'''''      GbFileRedOcc = ""
'''''      RTBR.Text = ""
     
      AggLog " --> Routines has generated.", MavsdF_GenRout.RtLog
    Case "Close" 'Esce
      Unload Me
  End Select
  
  Exit Sub
ErrNext:
  If Err.Number = 35602 Then
    Resume Next
  Else
    'MsgBox err.Description
    AggLog Err.Description, MavsdF_GenRout.RtLog
    Resume Next
  End If
End Sub
Private Sub Form_Load()
  Dim K As Double
  Dim PFreq As String
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
  
  LstFunction.AddItem "Generate RDBMS Routines"
  LstFunction.AddItem "Generate DLI Routines"
  LstFunction.AddItem "Use CALL method into routines"
  LstFunction.AddItem "Use EXEC method into routines"
  'SQ 16-11-04 - EMBEDDED
  'EMBEDDED_CHK=4
  'LstFunction.AddItem "Create EMBEDDED routines"
  'LstFunction.Selected(EMBEDDED_CHK) = False
  'EmbeddedRoutine = False
  
  LstFunction.Selected(0) = True
  LstFunction.Selected(2) = True
  
  
  'setta il default sul database DB2
  optDB2.Value = True
  'optOra.Value = False
  '''''''''''''''''
  ' Tutti i "LeggiParam" necessari: letti una volta sola!
  ' SQ 30-08-06
  '''''''''''''''''
  getEnvironmentParameters
End Sub
'SP 2/6/2006 - festa della repubblica
Sub getPhoto()
  Dim inCondition As String
  Dim rsIstruzioni As Recordset
  Dim testo As String, testo2 As String, testo3 As String
  Dim vPos As Long
  Dim K As Integer, k1 As Integer, k2 As Integer

  Dim wRout As String, wDb As String
  Dim Swoper As Boolean, Swprog As Boolean, SwOpe As Boolean, SwOpx As Boolean, existNode As Boolean
  Dim wIstrDec As String
  Dim wOnum As Integer
  'Dim xwIstr As String, xTrsope As String

  Dim IdField As Long
  Dim sDbd() As Long

  Dim wProgramma As Variant

  Dim wEx As Long
  Dim wFlag As Boolean
  Dim oldIstr As String

  On Error GoTo errRout
  
  RtLog.text = ""
  RTBErr.text = ""
  RTBErr2.text = ""

  wOnum = 0
  TreeView1.Nodes.Clear
  TreeView2.Nodes.Clear
  
  AggLog "Starting Instructions Analysis...", RtLog
  
  'riazzero il flag di generazione
  'serve solo per verifica generazione istruzione, in quanto la
  'lettura ora � ordinata per istruzione; per ora viene lasciato, da togliere
  ' per velocizzare
  'm_fun.FnConnection.Execute "Update MgDli_decodificaIstr Set Generato = false Where ImsDb = true"
  
  Label2 = 0  'Bei nomi!
  Label4 = 0
  Label6 = 0
  
  Screen.MousePointer = vbHourglass
  
  Dim IndOgg As Integer
  If optType.Item(0) Then
  ''''''''''''''''''''''''''''''
  ' DBD:
  ''''''''''''''''''''''''''''''
  For IndOgg = 1 To VSAM2Rdbms.drObjList.ListItems.Count
    If VSAM2Rdbms.drObjList.ListItems(IndOgg).Selected Then
      If VSAM2Rdbms.drObjList.ListItems(IndOgg).ListSubItems(TYPE_OBJ) = "DBD" Then
        inCondition = inCondition & VSAM2Rdbms.drObjList.ListItems(IndOgg) & ","
      Else
        MsgBox VSAM2Rdbms.drObjList.ListItems(IndOgg).ListSubItems(NAME_OBJ) & " is not a DBD", vbExclamation
        Screen.MousePointer = 0
        Exit Sub
      End If
    End If
  Next IndOgg
  If Len(inCondition) Then
    'rimuovo l'ultima virgola:
    inCondition = Left(inCondition, Len(inCondition) - 1)
    'SQ ??? non funge! ha l'ORDER BY SU a.idOggetto,a.riga che non ho nella distinct!!!!
    'Set rsIstruzioni = m_fun.Open_Recordset("select distinct b.*,Codifica,NomeRout from PsDLI_IstrDBD as a,PsDLI_Istruzioni as b,PsDLI_IstrCodifica as c where a.idDBD in (" & inCondition & ") AND a.IdOggetto=b.IdOggetto AND a.riga=b.riga AND a.IdOggetto=c.IdOggetto AND a.riga=c.riga And Valida and ImsDB order by codifica, a.idoggetto, a.riga")
    Set rsIstruzioni = m_fun.Open_Recordset( _
                      "select distinct b.*,Codifica,NomeRout from " & _
                      "PsDLI_IstrDBD as a,PsDLI_Istruzioni as b,PsDLI_IstrCodifica as c where " & _
                      "a.idDBD in (" & inCondition & ") AND a.IdOggetto=b.IdOggetto AND a.riga=b.riga AND " & _
                      "a.IdOggetto=c.IdOggetto AND a.riga=c.riga And Valida and ImsDB order by codifica")
  Else
    MsgBox "No DBDs selected", vbInformation
    Exit Sub
  End If
  ElseIf optType.Item(1) Then
    ''''''''''''''''''''''''''''''
    ' CBL:
    ''''''''''''''''''''''''''''''
    For IndOgg = 1 To VSAM2Rdbms.drObjList.ListItems.Count
      If VSAM2Rdbms.drObjList.ListItems(IndOgg).Selected Then
        'SQ:   SELEZIONE SUI DBD: UTILIZZARE OPT A 3: ALL/SRC/DBD
        If VSAM2Rdbms.drObjList.ListItems(IndOgg).ListSubItems(TYPE_OBJ) = "CBL" Then 'SQ - Tutti i PGM (altri linguaggi...)
          inCondition = inCondition & VSAM2Rdbms.drObjList.ListItems(IndOgg) & ","
        Else
          MsgBox VSAM2Rdbms.drObjList.ListItems(IndOgg).ListSubItems(NAME_OBJ) & " is not a CBL: " & VSAM2Rdbms.drObjList.ListItems(IndOgg).ListSubItems(TYPE_OBJ), vbExclamation
        End If
      End If
    Next
    If Len(inCondition) Then
      'rimuovo l'ultima virgola:
      inCondition = Left(inCondition, Len(inCondition) - 1)
      'SQ CPY-ISTR
      'Set rsIstruzioni = m_fun.Open_Recordset("select a.*,Codifica,NomeRout from PsDli_Istruzioni as a,PsDli_IstrCodifica as b where a.IdOggetto=b.IdOggetto AND a.riga=b.riga AND a.idoggetto in (" & inCondition & ") And Valida and ImsDB order by codifica, a.idoggetto, a.riga")
      Set rsIstruzioni = m_fun.Open_Recordset( _
                        "select a.*,Codifica,NomeRout from PsDli_Istruzioni as a,PsDli_IstrCodifica as b where " & _
                        "a.idPgm=b.idPgm AND a.idOggetto=b.idOggetto AND a.riga=b.riga AND " & _
                        "a.idPgm in (" & inCondition & ") And Valida and ImsDB " & _
                        "order by codifica, a.idPgm, a.idoggetto, a.riga")
    Else
      Exit Sub
    End If
  ElseIf optType.Item(2) Then
    ''''''''''''''''''''''''''''''
    ' ALL:
    ''''''''''''''''''''''''''''''
    'SQ CPY-ISTR
    'Set rsIstruzioni = m_fun.Open_Recordset("select distinct a.idOggetto,a.riga,a.istruzione,a.keyfeedback, Codifica,b.NomeRout from PsDLI_Istruzioni as a, PsDLI_IstrCodifica as b where a.IdOggetto=b.IdOggetto AND a.riga=b.riga and Valida and ImsDB order by codifica, a.idoggetto, a.riga")
    Set rsIstruzioni = m_fun.Open_Recordset( _
                      "select distinct a.idPgm,a.idOggetto,a.riga,a.istruzione,a.keyfeedback,Codifica,b.NomeRout from " & _
                      "PsDLI_Istruzioni as a, PsDLI_IstrCodifica as b where " & _
                      "a.idPgm=b.idPgm AND a.IdOggetto=b.IdOggetto AND a.riga=b.riga and Valida and ImsDB " & _
                      "order by codifica, a.idPgm, a.idoggetto, a.riga")
  Else
    'NO SELEZIONE
    MsgBox "tmp: selezionare DBD/SRC/ALL"
    Exit Sub
  End If
  
  Label2 = Label2 + rsIstruzioni.RecordCount
  ''''''''''''''''''''''''''''''''''
  ' Per ogni singola Istruzione:
  ''''''''''''''''''''''''''''''''''
  K = 0
  Dim oldOggetto As Integer
  Dim wCics As Boolean
  oldOggetto = 0
  'azzera tutto
  ReDim wIstrCod(0)
  While Not rsIstruzioni.EOF
    DoEvents
    
    'estrae alcune informazioni dalla bs_oggetti, ma solo se non cambia il programma
    'si potrebbe mettere in join con la rsistruzioni, ma la join comincerebbe a diventare troppo grande
    
    'stefano: secondo me sarebbe meglio togliere il nome del programma
    ' cos� si evita questa lettura praticamente inutile.
    ' verificare come toglierlo, anche perch� ora legge ogni volta che cambia
    ' l'accoppiata istruzione-idoggetto, cio� quasi sempre
    '   If oldOggetto <> rsIstruzioni!IdOggetto Then
    '      oldOggetto = rsIstruzioni!IdOggetto
    '      Set TbOgg = m_fun.Open_Recordset("select nome from BS_Oggetti where idoggetto = " & rsIstruzioni!IdOggetto)
    '      If Not TbOgg.EOF Then
    '         wProgramma = TbOgg!nome
    '      End If
    '      TbOgg.Close
    '    End If
    
  'ordinato per istruzione, dovrebbe essere pi� veloce
  If rsIstruzioni!Codifica <> oldIstr Then
    'SQ CPY-ISTR
    idpgm = rsIstruzioni!idpgm
    oldIstr = rsIstruzioni!Codifica
    k2 = 0
    'SQ CPY-ISTR aggiornare la funzione con idPgm
    sDbd = m_fun.getDBDByIdOggettoRiga(rsIstruzioni!idOggetto, rsIstruzioni!riga)
    If UBound(sDbd) > 0 Then
      wDb = Restituisci_NomeOgg_Da_IdOggetto(sDbd(1))
    Else
      wDb = ""  'che succede in questo caso?
    End If
    
'    xwIstr = rsIstruzioni!Istruzione
    wRout = rsIstruzioni!NomeRout
             
   Dim TbCodifica As Recordset
   'Set TbCodifica = m_fun.Open_Recordset("select * from MgDLI_DecodificaIstr where codifica = '" & rsIstruzioni!codifica & "' and generato = false")
   Set TbCodifica = m_fun.Open_Recordset("select * from MgDLI_DecodificaIstr where codifica = '" & rsIstruzioni!Codifica & "'")
   If TbCodifica.EOF Then
      MsgBox rsIstruzioni!Codifica & " missing in MGDLI_DECODIFICAISTR", vbExclamation
      Screen.MousePointer = 0
      Exit Sub
   End If
   ' si pu� togliere se rallenta troppo
'     TbCodifica!generato = True
'     TbCodifica.Update
   
     K = K + 1
     ReDim Preserve wIstrCod(K)
    
    'verifica GSAM
     Dim tb As Recordset
     Set tb = m_fun.Open_Recordset("select * from bs_oggetti where tipo_dbd = 'GSA' and nome = '" & wDb & "'")
     wIstrCod(K).isGsam = Not tb.EOF
     tb.Close
    
    wIstrCod(K).Istruzione = rsIstruzioni!Codifica
    wIstrCod(K).Istr = rsIstruzioni!Istruzione
    wIstrCod(K).Routine = wRout
    wIstrCod(K).nDb = wDb
    wIstrCod(K).Decodifica = TbCodifica!Decodifica
    'stefano: inizializza per evitare i soliti errori, non so come fare altrimenti, ci vuole un esperto
    ReDim wIstrCod(K).Liv(0)
    'lasciata per usi futuri, anche se improbabili
    wIstrCod(K).SwTrattata = True
    wIstrCod(K).NumChiamate = 1
    wIstrCod(K).IsCICS = TbCodifica!Cics
    wIstrCod(K).idpgm = rsIstruzioni!idpgm
    wIstrCod(K).IdOgg = rsIstruzioni!idOggetto
    wIstrCod(K).riga = rsIstruzioni!riga
    'da impostare; per ora lascio cos�, tanto non serviva a niente
    wIstrCod(K).Programma = "Generic"
                  
    'stefano: prima faceva un loop per l'operatore dinamico, ora k1 ha preso la molteplicit�
    ' dei livelli, al fine di usare wistrcod nella generazione
    ' DA INSERIRE IN UNA FUNZIONE GENERALE e DA MIGLIORARE (TROPPI INDICI e TROPPO COMPLICATO)
      
    Dim y1, y2, y3, y4, y5, y6 As Integer
    Dim param As String
                  
' Mauro 28-03-2007: Estrapolazione Istr da wistrcod(k).Decodifica
    y1 = InStr(wIstrCod(K).Decodifica, "<istr")
    If y1 > 0 Then
      y2 = InStr(y1, wIstrCod(K).Decodifica & ">", ">") ' inizio param "istr"
      y3 = InStr(y2, wIstrCod(K).Decodifica & "<", "<") ' fine param "istr"
      wIstrCod(K).Istr = Replace(Mid(wIstrCod(K).Decodifica, y2 + 1, y3 - y2 - 1), ".", "")
    End If
    y1 = 0
    y2 = 0
    y3 = 0
                  
    y1 = InStr(wIstrCod(K).Decodifica, "<level")
'   If y1 > 0 Then
      
        While y1 > 0
          y2 = InStr(y1, wIstrCod(K).Decodifica, ">")
          'stefano: vedi commento sopra: il loop � stato mantenuto per caricare wistrcod
          ' per la generazione routine sulle istruzioni dinamiche
          k1 = Int(Mid(wIstrCod(K).Decodifica, y1 + 6, y2 - (y1 + 6)))
          k2 = 0
          ReDim Preserve wIstrCod(K).Liv(k1)
      'stefano: inizializza per evitare i soliti errori, non so come fare altrimenti,
      ' ci vuole un esperto
          ReDim wIstrCod(K).Liv(k1).Chiave(0)
          
          y2 = InStr(y1 + 1, wIstrCod(K).Decodifica, "</level")
          'non funziona con livello > 9
          y4 = y1 + 8
          While y4 < y2 And y4 > 0
             y6 = y4
             y4 = InStr(y6 + 1, wIstrCod(K).Decodifica, ">")
             If Mid(wIstrCod(K).Decodifica, y4 + 1, 1) <> "<" Then
             'stefano: non ce la faccio a farlo meglio,
             ' dal secondo giro in poi, entra sempre nella if
                If Mid(wIstrCod(K).Decodifica, y6 + 1, 1) = "<" Then
                   y6 = y6 + 1
                End If
                param = Mid(wIstrCod(K).Decodifica, y6 + 1, y4 - (y6 + 1))
                y5 = InStr(y4 + 1, wIstrCod(K).Decodifica, "<")
                If param = "segm" Then
                   wIstrCod(K).Liv(k1).Segm = Mid(wIstrCod(K).Decodifica, y4 + 1, y5 - (y4 + 1))
                End If
                If Mid(wIstrCod(K).Decodifica, y4 + 1, y5 - (y4 + 1)) = "AREA" Then
                   wIstrCod(K).Liv(k1).Area = True
                End If
                If param = "ccode" Then
                   wIstrCod(K).Liv(k1).Ccode = Mid(wIstrCod(K).Decodifica, y4 + 1, y5 - (y4 + 1))
                End If
                If param = "key" Then
                   k2 = k2 + 1
                   ReDim Preserve wIstrCod(K).Liv(k1).Chiave(k2)
                   wIstrCod(K).Liv(k1).Chiave(k2).key = Mid(wIstrCod(K).Decodifica, y4 + 1, y5 - (y4 + 1))
                End If
                If param = "oper" Then
                   wIstrCod(K).Liv(k1).Chiave(k2).Oper = Mid(wIstrCod(K).Decodifica, y4 + 1, y5 - (y4 + 1))
                End If
                If param = "bool" Then
                   wIstrCod(K).Liv(k1).Chiave(k2).bool = Mid(wIstrCod(K).Decodifica, y4 + 1, y5 - (y4 + 1))
                End If
             End If
          Wend
          
          y1 = InStr(y1 + 1, wIstrCod(K).Decodifica, "<level")
        Wend
        y1 = InStr(1, wIstrCod(K).Decodifica, "FDBKEY")
        If y1 > 0 Then
           wIstrCod(K).Fdbkey = True
        End If
        y1 = InStr(1, wIstrCod(K).Decodifica, "<procseq>")
        If y1 > 0 Then
           y2 = InStr(y1 + 1, wIstrCod(K).Decodifica, "</procseq>")
           If y2 > 0 Then
              wIstrCod(K).procSeq = Mid(wIstrCod(K).Decodifica, y1 + 9, y2 - (y1 + 9))
           End If
        End If
        y1 = InStr(1, wIstrCod(K).Decodifica, "<procopt>")
        If y1 > 0 Then
           y2 = InStr(y1 + 1, wIstrCod(K).Decodifica, "</procopt>")
           If y2 > 0 Then
              wIstrCod(K).procOpt = Mid(wIstrCod(K).Decodifica, y1 + 9, y2 - (y1 + 9))
           End If
        End If
        y1 = InStr(1, wIstrCod(K).Decodifica, "<pcbseq>")
        If y1 > 0 Then
           y2 = InStr(y1 + 1, wIstrCod(K).Decodifica, "</pcbseq>")
           If y2 > 0 Then
              wIstrCod(K).PcbSeq = Mid(wIstrCod(K).Decodifica, y1 + 8, y2 - (y1 + 8))
           End If
        End If
      Label4 = Label4 + 1
  Else
  'aggiunge i programmi doppi con la stessa istruzione
  ' in realt� � da rivedere, bisognerebbe cambiare wistrcod, in quanto se i programmi
  ' possono essere, giustamente, pi� di uno per ogni istruzione, ci� � vero anche per
  ' l'idoggetto e la riga, altrimenti non ha senso!
  
  'INSERITO L'ORDINAMENTO PER ISTRUZIONE, PER CUI NON SERVE PIU' UN CICLO OGNI
  'VOLTA CHE C'E' L'ISTRUZIONE DOPPIA
'    Dim z1, z2, z3 As Integer
'    For z1 = 1 To UBound(wIstrCod)
'      If wIstrCod(z1).Istruzione = rsIstruzioni!codifica Then
'        For z2 = 1 To UBound(wIstrCod(z1).Oper)
'          Swprog = False
'          For z3 = 1 To UBound(wIstrCod(z1).Oper(z2).Programma)
'            If wIstrCod(z1).Oper(z2).Programma(z3) = wProgramma Then
'               Swprog = True
'               Exit For
'            End If
'          Next
'          If Not Swprog Then
'            z3 = UBound(wIstrCod(z1).Oper(z2).Programma) + 1
'            ReDim Preserve wIstrCod(z1).Oper(z2).Programma(z3)
'            wIstrCod(z1).Oper(z2).Programma(z3) = wProgramma
'            'wIstrCod(z1).IsCICS = wCics       'molto stupido: il programma � sul terzo livello, il flag cics, che � una sua caratteristica, sul primo!
'            wIstrCod(z1).NumChiamate = wIstrCod(z1).NumChiamate + 1
'          End If
'        Next
'        Exit For
'      End If
'    Next
   'programma eliminato: bisogna gestirlo meglio!!!
'    k2 = k2 + 1
    If UBound(wIstrCod) >= K Then
       wIstrCod(K).NumChiamate = wIstrCod(K).NumChiamate + 1
'    ReDim Preserve wIstrCod(k).Oper(k1).Programma(k2)
'    wIstrCod(k).Oper(k1).Programma(k2) = wProgramma
    End If
    Label6 = Label6 + 1
  End If
  rsIstruzioni.MoveNext
  Wend
  Screen.MousePointer = vbDefault
  rsIstruzioni.Close

  For K = 1 To UBound(wIstrCod)

'stefano: per ora lo tolgo, visto che non serve pi�!; da reinserire con lettura
' sulla mgdli_decodificaistr delle istruzioni con flag_generato <> 0
  
'    'SQ 10-08
'    'DISABILITATO LOG SOTTO!!!!!!!!!!!!!!!!!!
'    'SOLO I PRIMI 50!
'    If Not wIstrCod(k).SwTrattata Then
'      If k < 50 Then
'        vPos = Len(RTBErr2.Text)
'        RTBErr2.SelStart = vPos
'        RTBErr2.SelLength = 0
'        RTBErr2.SelText = wIstrCod(k).Istruzione & "-" & wIstrCod(k).Decodifica & vbCrLf
'      ElseIf k = 50 Then
'        vPos = Len(RTBErr2.Text)
'        RTBErr2.SelStart = vPos
'        RTBErr2.SelLength = 0
'        RTBErr2.SelText = "... too many errors (more than 50)..."
'      End If
'    End If
    Dim xsInstr As String
    If Len(wIstrCod(K).Routine) Then

      If wIstrCod(K).IsCICS Then
        xsInstr = "T#" & Left(wIstrCod(K).Istruzione, 4)
      Else
        xsInstr = "B#" & Left(wIstrCod(K).Istruzione, 4)
      End If

      'STEFANO G. Univocit�
      existNode = False
          Dim exKey As String

          'Univocit�
          For wEx = 1 To TreeView2.Nodes.Count
            If TreeView2.Nodes(wEx).text = wIstrCod(K).nDb & " - " & wIstrCod(K).Routine Then
                existNode = True
                exKey = TreeView2.Nodes(wEx).key
                Exit For
            End If
          Next wEx

          If Not existNode Then
              TreeView2.Nodes.Add , , "#" & xsInstr & "#" & wIstrCod(K).Routine, wIstrCod(K).nDb & " - " & wIstrCod(K).Routine
              TreeView2.Nodes.Add "#" & xsInstr & "#" & wIstrCod(K).Routine, tvwChild, wIstrCod(K).Istruzione, wIstrCod(K).Istruzione
          Else
              TreeView2.Nodes.Add exKey, tvwChild, wIstrCod(K).Istruzione, wIstrCod(K).Istruzione
          End If

'          If InStr(1, wIstrCod(k).Routine, "UTIL") > 0 Then
'              TreeView2.Nodes.Add wIstrCod(k).Istruzione, tvwChild, wIstrCod(k).Istruzione & "-", "<NOOP>" & "(Id:-1 Riga: -1)"
'          Else
    'stefano: voleva solo l'ultimo, da reimpostare meglio
'            For k1 = 1 To UBound(wIstrCod(k).Oper)
'              TreeView2.Nodes.Add wIstrCod(k).Istruzione, tvwChild, wIstrCod(k).Istruzione & "-" & wIstrCod(k).Oper(k1).Cod, wIstrCod(k).Oper(k1).Cod & "(Id:" & wIstrCod(k).Oper(k1).IdOgg & " Riga:" & wIstrCod(k).Oper(k1).Riga & ")"
'            Next k1
'          End If
          k1 = UBound(wIstrCod(K).Liv)
          If k1 > 0 Then
             k2 = UBound(wIstrCod(K).Liv(k1).Chiave)
             If k2 > 0 Then
                'SQ CPY-ISTR
                'TreeView2.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "-" & wIstrCod(K).Liv(k1).Chiave(k2).Oper, wIstrCod(K).Liv(k1).Chiave(k2).Oper & "(Id:" & wIstrCod(K).IdOgg & " Riga:" & wIstrCod(K).Riga & ")"
                TreeView2.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "-" & wIstrCod(K).Liv(k1).Chiave(k2).Oper, wIstrCod(K).Liv(k1).Chiave(k2).Oper & "(IdPgm:" & wIstrCod(K).idpgm & " Id:" & wIstrCod(K).IdOgg & " Riga:" & wIstrCod(K).riga & ")"
             Else
                'SQ CPY-ISTR
                'TreeView2.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "-" & "<NOOP>", "<NOOP>" & "(Id:" & wIstrCod(K).IdOgg & " Riga:" & wIstrCod(K).Riga & ")"
                TreeView2.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "-" & "<NOOP>", "<NOOP>" & "(IdPgm:" & wIstrCod(K).idpgm & " Id:" & wIstrCod(K).IdOgg & " Riga:" & wIstrCod(K).riga & ")"
             End If
          Else
            k1 = 1
            'SQ CPY-ISTR
            'TreeView2.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "-" & "<NOLEVEL>", "<NOLEVEL>" & "(Id:" & wIstrCod(K).IdOgg & " Riga:" & wIstrCod(K).Riga & ")"
            TreeView2.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "-" & "<NOLEVEL>", "<NOLEVEL>" & "(IdPgm:" & wIstrCod(K).idpgm & " Id:" & wIstrCod(K).IdOgg & " Riga:" & wIstrCod(K).riga & ")"
          End If
    End If

    If wIstrCod(K).NumChiamate > 0 Then
       TreeView1.Nodes.Add , , wIstrCod(K).Istruzione, wIstrCod(K).Routine & " (" & wIstrCod(K).Istruzione & ") <" & wIstrCod(K).NumChiamate & ">"
       k1 = UBound(wIstrCod(K).Liv)
       If k1 > 0 Then
          k2 = UBound(wIstrCod(K).Liv(k1).Chiave)
          wProgramma = wIstrCod(K).Programma
          If k2 > 0 Then
             TreeView1.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & wIstrCod(K).Liv(k1).Chiave(k2).Oper, wIstrCod(K).Liv(k1).Chiave(k2).Oper
             TreeView1.Nodes.Add wIstrCod(K).Istruzione & wIstrCod(K).Liv(k1).Chiave(k2).Oper, tvwChild, wIstrCod(K).Istruzione & wIstrCod(K).Liv(k1).Chiave(k2).Oper & wProgramma, wProgramma
          Else
             TreeView1.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "<NOOP>", "<NOOP>"
             TreeView1.Nodes.Add wIstrCod(K).Istruzione & "<NOOP>", tvwChild, wIstrCod(K).Istruzione & "<NOOP>" & wProgramma, wProgramma
          End If
       Else
          TreeView1.Nodes.Add wIstrCod(K).Istruzione, tvwChild, wIstrCod(K).Istruzione & "<NOLEVEL>", "<NOLEVEL>"
          TreeView1.Nodes.Add wIstrCod(K).Istruzione & "<NOLEVEL>", tvwChild, wIstrCod(K).Istruzione & "<NOLEVEL>" & "Generic", "Generic"
       End If
    End If
    Me.Refresh
  Next K
      
  AggLog "Instructions Analysis Ended.", RtLog
Exit Sub
errRout:
  If Err.Number = 35602 Then
    Resume Next
  Else
    MsgBox Err.Description
  End If
  Resume Next
End Sub
