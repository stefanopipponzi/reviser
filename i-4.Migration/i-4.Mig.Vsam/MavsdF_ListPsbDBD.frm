VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MavsdF_ListPsbDBD 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choose..."
   ClientHeight    =   2370
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4155
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2370
   ScaleWidth      =   4155
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lswList 
      Height          =   2175
      Left            =   60
      TabIndex        =   2
      Top             =   90
      Width           =   2985
      _ExtentX        =   5265
      _ExtentY        =   3836
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Id"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Name"
         Object.Width           =   3528
      EndProperty
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "&Ok"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   3150
      Picture         =   "MavsdF_ListPsbDBD.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   90
      Width           =   945
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   3150
      Picture         =   "MavsdF_ListPsbDBD.frx":1D42
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1350
      Width           =   945
   End
End
Attribute VB_Name = "MavsdF_ListPsbDBD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private i_IdLista As String
Private i_NomeLista As String

Private Sub cmdCancel_Click()
   i_IdLista = ""
   i_NomeLista = ""
   Unload Me
End Sub

Private Sub cmdOpen_Click()
   Me.Hide
End Sub

Private Sub Form_Load()
   ld_Lista
End Sub

Public Sub ld_Lista()
   Dim rs As Recordset
   Dim wId As String
   Dim wNome As String
   Dim widDbd As String
   Dim i As Long
   
   widDbd = ""
   
   For i = 1 To UBound(Istruzione.Database)
      widDbd = widDbd & " or IdOggetto = " & Istruzione.Database(i).idOggetto
   Next i
   
   widDbd = Mid(widDbd, 5)
   
   Select Case gbTypeToLoad
      Case "PSB"
         Me.Caption = "Choose Psb association..."
         Set rs = m_fun.Open_Recordset("Select * From Bs_Oggetti Where Tipo = '" & gbTypeToLoad & "'")
         wId = "idOggetto"
         wNome = "Nome"
         
      Case "DBD"
         Me.Caption = "Choose Dbd association..."
         Set rs = m_fun.Open_Recordset("Select * From Bs_Oggetti Where Tipo = '" & gbTypeToLoad & "'")
         wId = "idOggetto"
         wNome = "Nome"
         
      Case "SEG" 'segmenti
         If widDbd <> "" Then
            Me.Caption = "Choose Segment association..."
            Set rs = m_fun.Open_Recordset("Select * From psDli_segmenti Where " & widDbd)
            wId = "idSegmento"
            wNome = "Nome"
         Else
            Exit Sub
         End If
   End Select
   
   lswList.ListItems.Clear
   
   If rs.RecordCount > 0 Then
      rs.MoveFirst
      
      While Not rs.EOF
         lswList.ListItems.Add , , Format(rs.Fields(wId), "000000")
         lswList.ListItems(lswList.ListItems.Count).ListSubItems.Add , , rs.Fields(wNome)
         
         rs.MoveNext
      Wend
   End If
   
   rs.Close
End Sub

Property Get Id() As String
   Dim i As Long
   
   For i = 1 To lswList.ListItems.Count
      If lswList.ListItems(i).Selected = True Then
         Id = i_IdLista & "#" & Id
      End If
   Next i
   
   Id = Mid(Id, 2) & "#"
End Property

Property Get SelName() As String
   Dim i As Long
   
   For i = 1 To lswList.ListItems.Count
      If lswList.ListItems(i).Selected = True Then
         SelName = i_NomeLista & "#" & SelName
      End If
   Next i

End Property

Private Sub lswList_ItemClick(ByVal Item As MSComctlLib.ListItem)
   i_IdLista = Item.text
   i_NomeLista = Item.ListSubItems(1).text
End Sub
