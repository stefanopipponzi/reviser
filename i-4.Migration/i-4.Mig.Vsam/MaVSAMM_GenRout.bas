Attribute VB_Name = "MaVSAMM_GenRout"
Option Explicit

Dim nomeTabIndPart As String
Dim idTabIndPart As Long

'''MG 240107
Private pDB As String

Type DefChiavi
  key As String
  Oper As String
  bool As String
End Type

Type DefLivelli
  Segm As String
  Area As Boolean
  Ccode As String
  Chiave() As DefChiavi
End Type

Type DefOperaz
  Istruzione As String
  Istr As String
  Routine As String
  nDb As String
  Decodifica As String
  Liv() As DefLivelli
  SwTrattata As Boolean
  NumChiamate As Integer
  IsCICS As Boolean
  Fdbkey As Boolean
  'per ora lo metto sul primo, prendo solo il primo programma, gli altri non interessano
  idpgm As Long   'SQ CPY-ISTR
  IdOgg As String
  riga As String
  Programma As String
  procSeq As String
  procOpt As String
  PcbSeq As String
  isGsam As Boolean
End Type
Global wIstrCod() As DefOperaz

Enum e_TableLevel
  TABLE_FATHER = 2 ^ 0
  TABLE_CURRENT = 2 ^ 1
  TABLE_CHILD = 2 ^ 2
End Enum

Type PSB_N_A
  nome As String
  Id As Long
End Type

Type BlkIstrKey
  key As String
  xName As Boolean
  NameAlt As String
  Op As String
  KeyVal As String
  KeyStart As Long
  KeyField As String
  KeyLen As String
  KeyBool As String
  KeyNum As Integer
End Type

Type BlkIstrDli
  ssaName As String
  SsaLen As Integer
  Segment As String
  NewNameSeg As String
  Tavola As String
  tableCreator As String  'SQ 28-07-06
  pKey As String
  IdSegm As Double
  'stefano: era ora
  idTable As Long
  Record As String
  SegLen As Long
  CodCom As String
  Parentesi As String
  Search As String
  NomeRout As String
  valida As Boolean
  LParent As String
  LDBD As String
  KeyDef() As BlkIstrKey
End Type

Type IstrDli
  dbd As String
  PCB As String
  ordPcb As Integer
  Istr As String
  PSBName As String
  PsbId As Double
  'ginevra: keyfeedback
  keyfeedback As String
  procSeq As String
  procOpt As String
  BlkIstr() As BlkIstrDli
  isGsam As Boolean
End Type
Global TabIstr As IstrDli
'stefano per delete
Global mIdOggettoCorrente As Long, mRiga As Long, mIdDBDCorrente As Long
'SQ CPY-ISTR
Global idpgm As Long
Global gbCurInstr As String 'Istruzione corrente durante creazione routine (GU,GN...eccc....)
'SQ 26-08 (le calcolo una volta sola)
Global sDbd() As Long, sPsb() As Long
Global suffissiStorici() As String
'SP 12/9 indici particolari
Global indPart As Boolean
'alpitour: 5/8/2005 serve per suffissi nelle routines
Public m_suffix As String
'stefanopippo: 15/8/2005 author tabella (anche questo da mettere meglio)
Public m_author As String
'stefanopippo: indentazione da passare per le where e le order by
Global indentGen As Integer, totIstr As Integer
Global suffDynT As String
Global tabOk() As String, istrOk() As String, istrOkDet() As String
Global tabOk2() As String, istrOk2() As String, istrOkDet2() As String
Global idxTabOkMax2 As Integer, idxTabOkMax As Integer

Global percorso As String
Global SwDigit As Boolean, SwTp As Boolean
Global GbNumIstr As Variant
Global Gistruzione As String
Global GbKeySec As Boolean
Global GbKeySecName As String
Global IstrDb2 As String

Global GbNumDial As Integer, GbAttDial As Integer
Global GbStrDial() As String
Global GbTestoFetch As String, GbTestoRedOcc As String
'SQ D
Global GbFileRedOccs() As String
Global GbFileRedOcc As String 'transitorio: buttare!
Global GbTestoRedOcc2 As String
   
Dim BlkIni As Integer, BlkFin As Integer, BlkStep As Integer
'SQ
Dim orderByColumns As String
Dim level As Integer  'SQ D
Global cCopyParam As String, tCopyParam As String, kCopyParam As String, mCopyParam As String, xCopyParam As String

'GU -tmp... ex locale
Dim testoRed() As String, idRed() As Long
Dim idSave As Long
Dim nomeTRed() As String, nomeSRed() As String, nomeFieldRed() As String, nomeKeyRed() As String
Dim includedMember As String
Dim indentRed As Integer
Global comment As String
Global isGsam As Boolean

'silvia 21/11/2007 : costanti per le intestazioni della lista Oggetti(drObjList)
Global Const NAME_OBJ = 1
Global Const TYPE_OBJ = 2
Global Const DLI_OBJ = 3
Global Const CICS_OBJ = 4
Global Const RC_OBJ = 5
Global Const LOCS_OBJ = 6
Global Const NOTES_OBJ = 7
Global Const PARS_DATE_OBJ = 8
Global Const DIR_OBJ = 9
Global Const IMP_DATE_OBJ = 10


Type cmpCol
  nomeCmp1 As String
  lenCmp1 As Long
  Tipo As String
  Index As Boolean
  nomeCmp2 As String
  lenCmp2 As Long
End Type

'''MG 240107
'let e get del nome del db (se non passato prende db2 come default)
Public Property Let setDBName(dbFromForm As String)
    pDB = dbFromForm
End Property
Property Get DB() As String
    If pDB = "" Then
        DB = "DMDB2_"
    Else
        DB = pDB
    End If
End Property

Public Function isCiclicTag(Tag As String) As Boolean
  'isCiclicTag = Right(Tag, 4) = "(i)>"
  isCiclicTag = Right(Tag, 5) = "(i)>>"
End Function
Public Sub InitIstrTemplate(Rtb As RichTextBox, wIstr As String, wQual As String)
  IstrDb2 = wIstr
    
  changeTag Rtb, "<COMMENT>", comment
  changeTag Rtb, "<NUM-ISTR>", Gistruzione
  changeTag Rtb, "<ISTR>", wIstr
  changeTag Rtb, "<TABLE-NAME>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Tavola
  changeTag Rtb, "<NOME-SEGM>", TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Segment
  changeTag Rtb, "<NOME-DBD>", TabIstr.dbd
End Sub
 Function costruisciOccursIns(idTab As Variant, indentRed As Variant, tipoIstr As String, flagOcc As Boolean, Redef As Boolean, RtbOccurs As RichTextBox, AppoOccurs As String) As String
  Dim tbOcc As Recordset
  Dim testo As String, tabSave As String, idxSave2 As String, istrSave As String
  Dim idSave As Variant, idSave2 As Variant
  Dim keySave() As BlkIstrKey
  Dim testoIndicatori
  Dim tb1 As Recordset
  
  Dim RtbSqlAppo As RichTextBox
  Dim RtbOccCopy As RichTextBox
  
  Dim ScheletonRtbOccurs As String, wPath As String

  wPath = m_dllFunctions.FnPathPrj

  ScheletonRtbOccurs = AppoOccurs
  
'''MG 240107
'''  Set tbOcc = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab,a.creator as creator,b.nome as nomeidx, c.occurs as idxmax, a.condizione as testored, c.id_depending_on as idcmpon" & _
'''      " from dmdb2_tabelle as a, dmdb2_index as b, mgdata_cmp as c" & _
'''      " where a.idtable = b.idtable and a.idtablejoin = " & idTab & _
'''      " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")

  Set tbOcc = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab,a.creator as creator,b.nome as nomeidx, c.occurs as idxmax, a.condizione as testored, c.id_depending_on as idcmpon" & _
      " from " & DB & "tabelle as a, " & DB & "index as b, mgdata_cmp as c" & _
      " where a.idtable = b.idtable and a.idtablejoin = " & idTab & _
      " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")
  'SQ - tmp: cambiare tutto, ATTENZIONE alle ISRT!
  'Set RtbOccCopy = MaVSAMF_GenRout.RtbOccCopy
  
  'RtbOccCopy.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\PIOCCURS"
  RtbOccCopy.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\PIOCCURS"
  
  If tipoIstr <> "REPL" Then
    If Not tbOcc.EOF And Not Redef Then
'''       testo = testo & Space(7) & "9999-" & Replace(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Tavola, "_", "-") & "-" & tipoIstr & " SECTION." & vbCrLf
       'gestione EXIT non funzionante
'''       testo = testo & Space(11 + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
       changeTag RtbOccCopy, "<NOME-TABLE-OCC>", Replace(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Tavola, "_", "-")
    End If
  End If
  
  While Not tbOcc.EOF
     flagOcc = True
     RtbOccurs.text = ScheletonRtbOccurs
     testo = testo & vbCrLf
'''     testo = testo & Space(6) & "*** OCCURS" & vbCrLf
'''     'gestione EXIT non funzionante
'''     testo = testo & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
'''     testo = testo & Space(14 + indentRed) & "MOVE '" & tbOcc!nomeTab & "' TO WK-NAMETABLE " & vbCrLf
'''     testo = testo & Space(14 + indentRed) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
     If tipoIstr = "REPL" Then
        changeTag RtbOccurs, "<TABLE-NAME>", tbOcc!nomeTab
'''       testo = testo & Space(14 + indentRed) & "MOVE KRR-" & Replace(tbOcc!Nomeidx, "_", "-") & " TO K01-" & Replace(tbOcc!Nomeidx, "_", "-") & vbCrLf
'''       testo = testo & Space(14 + indentRed) & "EXEC SQL DELETE " & vbCrLf
       
        changeTag RtbOccurs, "<NOME-IDX>", Replace(tbOcc!Nomeidx, "_", "-")
       
       ' REPLACE sempre livello unico
       idSave = TabIstr.BlkIstr(1).idTable
       tabSave = TabIstr.BlkIstr(1).Tavola
       keySave = TabIstr.BlkIstr(1).KeyDef
     '  RobSave = TabIstr.BlkIstr(1).KeyDef
       istrSave = TabIstr.Istr
       TabIstr.BlkIstr(1).Tavola = tbOcc!nomeTab
       TabIstr.BlkIstr(1).idTable = tbOcc!idTab
       ReDim TabIstr.BlkIstr(1).KeyDef(0)
       TabIstr.Istr = "DUMM"
       
       indentGen = indentRed
'''       testo = testo & Space(15 + indentRed) & "FROM" & vbCrLf & CostruisciFrom(1, 1, True)
'''       testo = testo & vbCrLf & CostruisciWhere(1, 1)
  
       'Set RtbSqlAppo = MaVSAMF_GenRout.RtbSqlAppo
       'RtbSqlAppo.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\REPL\SQLDELETE"
       RtbSqlAppo.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\REPL\SQLDELETE"
     
       changeTag RtbSqlAppo, "<DELETE-SQL-FROM>", CostruisciFrom(1, 1, True)
       changeTag RtbSqlAppo, "<DELETE-SQL-WHERE>", CostruisciWhere(1, 1)
       
       TabIstr.BlkIstr(1).Tavola = tabSave
       TabIstr.BlkIstr(1).idTable = idSave
       TabIstr.BlkIstr(1).KeyDef = keySave
'       TabIstr.BlkIstr(1).KeyDef = RobSave
       TabIstr.Istr = istrSave
'''       testo = testo & Space(14 + indentRed) & "END-EXEC" & vbCrLf
       
       changeTag RtbOccurs, "<<SQLDELETE>>", RtbSqlAppo.text
     
     End If
     
     changeTag RtbOccurs, "<TABLE-NAME>", tbOcc!nomeTab
'''     testo = testo & Space(14 + indentRed) & "PERFORM 9999-SETK-WKEYADL" & vbCrLf
     'gestione EXIT non funzionante
'''     testo = testo & Space(11 + indentRed) & "END-IF" & vbCrLf
      
     Dim flgOn As Boolean
     Dim nomeOn As String
     flgOn = False
     nomeOn = ""
     If tbOcc!idcmpon > 0 Then
        Set tb1 = m_dllFunctions.Open_Recordset("select * from mgdata_cmp where idcmp = " & tbOcc!idcmpon)
        If Not tb1.EOF Then
           flgOn = True
           nomeOn = tb1!nome
           'stefano: verifichiamo se tutte le copy T sono ok per il depending on
           'testo = testo & Space(11 + indentRed) & "MOVE '" & tb1!Nome & "' TO WK-KEYSEQ1X" & vbCrLf
        End If
     End If
'''     testo = testo & Space(11 + indentRed) & "PERFORM VARYING WK-KEYSEQ1 FROM 1 BY 1" & vbCrLf
'''     testo = testo & Space(11 + indentRed) & "  UNTIL WK-KEYSEQ1 > " & IIf(flgOn, nomeOn, tbOcc!idxmax) & vbCrLf
'''     testo = testo & Space(11 + indentRed) & "  OR    WK-SQLCODE NOT = ZERO" & vbCrLf
     
     changeTag RtbOccurs, "<IND-TABLE-OCCURS>", IIf(flgOn, nomeOn, tbOcc!idxmax)
     
     Dim indentOcc As Integer
     indentOcc = 0
     If Len(tbOcc!testoRed) And Not flgOn Then
        indentOcc = 3
''        testo = testo & Space(11 + indentRed + indentOcc) & Replace(getCondizione_Redefines(tbOcc!testoRed), vbCrLf, vbCrLf & Space(14 + indentRed)) & vbCrLf
     
        changeTag RtbOccurs, "<REDEFINE-COND>", getCondizione_Redefines(tbOcc!testoRed)
     
     End If
    'testo = testo & Space(14 + indentRed + indentOcc) & "PERFORM " & SostUnderScore(nomeTRed(k)) & "-TO-RR THRU " & SostUnderScore(nomeTRed(k)) & "-TO-RR-EX" & vbCrLf
'''    testo = testo & Space(14 + indentRed + indentOcc) & "PERFORM 9999-" & Replace(tbOcc!nomeTab, "_", "-") & "-TO-RR" & vbCrLf & vbCrLf
      
     changeTag RtbOccurs, "<TABLE-OCCURS>", Replace(tbOcc!nomeTab, "_", "-")
    
     indentGen = 15 + indentRed
'''    testo = testo & Space(14 + indentRed + indentOcc) & "EXEC SQL " & vbCrLf
    'SQ 1-08-06 CREATOR
    'Buttare via la variabile globale m_author...
''''    testo = testo & Space(14 + indentRed + indentOcc) & "    INSERT INTO " & IIf(Len(tbOcc!creator), tbOcc!creator & ".", "") & tbOcc!nomeTab & vbCrLf
''''    testo = testo & Space(14 + indentRed + indentOcc) & "    (" & vbCrLf
''''    testo = testo & CostruisciSelect("", tbOcc!idTab) & Space(12 + indentRed) & ")" & vbCrLf
''''    testo = testo & Space(14 + indentRed + indentOcc) & "    VALUES (" & vbCrLf
''''    testo = testo & CostruisciInto("T" & UBound(TabIstr.BlkIstr), tbOcc!idTab, tbOcc!nomeTab, "ISRT")
''''    testo = testo & Space(14 + indentRed + indentOcc) & "    )" & vbCrLf
''''    testo = testo & Space(14 + indentRed + indentOcc) & "END-EXEC" & vbCrLf
''''    testo = testo & Space(14 + indentRed + indentOcc) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf

        ' ricaricare template SQLINSERT
     
     'Set RtbSqlAppo = MaVSAMF_GenRout.RtbSqlAppo
     'RtbSqlAppo.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\SQLINSERT"
     RtbSqlAppo.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\SQLINSERT"
     
     changeTag RtbSqlAppo, "<CREATOR>", tbOcc!creator
     changeTag RtbSqlAppo, "<TABLE-NAME>", tbOcc!nomeTab

     changeTag RtbSqlAppo, "<INSERT-SQL-INTO>", CostruisciSelect("", tbOcc!idTab)
     changeTag RtbSqlAppo, "<INSERT-SQL-VALUES>", CostruisciInto("T" & UBound(TabIstr.BlkIstr), tbOcc!idTab, tbOcc!nomeTab, "ISRT")

     changeTag RtbOccurs, "<<SQLINSERT>>", RtbSqlAppo.text
     
     changeTag RtbOccCopy, "<<ISRT-OCCURS(i)>>", RtbOccurs.text
'''     If Len(tbOcc!testoRed) And Not flgOn Then
'''      testo = testo & Space(11 + indentRed + indentOcc) & "END-IF" & vbCrLf
'''     End If
'''
'**** OCCURS DI UN OCCURS!!!!!!!!!!!!!!
' PER ORA GESTITO FISSO, BISOGNERA' FARE UN CICLO
     Dim tbOcc2 As Recordset
     Dim idxOcc2 As Integer
     
'''MG 240107
''''''     Set tbOcc2 = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab, b.nome as nomeidx, c.occurs as idxmax, a.condizione as testored, c.id_depending_on as idcmpon" & _
''''''      " from dmdb2_tabelle as a, dmdb2_index as b, mgdata_cmp as c" & _
''''''      " where a.idtable = b.idtable and a.idtablejoin = " & tbOcc!idTab & _
''''''      " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")

     Set tbOcc2 = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab, b.nome as nomeidx, c.occurs as idxmax, a.condizione as testored, c.id_depending_on as idcmpon" & _
      " from " & DB & "tabelle as a, " & DB & "index as b, mgdata_cmp as c" & _
      " where a.idtable = b.idtable and a.idtablejoin = " & tbOcc!idTab & _
      " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")
     While Not tbOcc2.EOF
'''         testo = testo & vbCrLf
'''         testo = testo & Space(6) & "*** SECOND LEVEL OF OCCURS" & vbCrLf
'''     'gestione EXIT non funzionante
'''         testo = testo & Space(14 + indentRed + indentOcc) & "IF WK-SQLCODE = ZERO" & vbCrLf
'''         testo = testo & Space(17 + indentRed + indentOcc) & "MOVE '" & tbOcc2!nomeTab & "' TO WK-NAMETABLE " & vbCrLf
'''         'testo = testo & Space(17 + indentRed + indentOcc) & "PERFORM SETK-WKEYADL THRU SETK-WKEYADL-EX" & vbCrLf
'''         testo = testo & Space(17 + indentRed + indentOcc) & "PERFORM 9999-SETK-WKEYADL" & vbCrLf
'''     'gestione EXIT non funzionante
'''         testo = testo & Space(14 + indentRed + indentOcc) & "END-IF" & vbCrLf
        
        'Set RtbOccurs = MaVSAMF_GenRout.RtbOccurs
        'RtbOccurs.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\ISRT-OCCURS-SECOND"
        RtbOccurs.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\ISRT-OCCURS-SECOND"
        changeTag RtbOccurs, "<TABLE-NAME>", tbOcc2!nomeTab
          
        flgOn = False
        nomeOn = ""
        If tbOcc2!idcmpon > 0 Then
           Set tb1 = m_dllFunctions.Open_Recordset("select * from mgdata_cmp where idcmp = " & tbOcc2!idcmpon)
           If Not tb1.EOF Then
              flgOn = True
              nomeOn = tb1!nome
           'stefano: verifichiamo se tutte le copy T sono ok per il depending on
           'testo = testo & Space(14 + indentRed + indentOcc) & "MOVE '" & tb1!Nome & "' TO WK-KEYSEQ1X" & vbCrLf
           End If
        End If
          
'''         testo = testo & Space(14 + indentRed + indentOcc) & "PERFORM VARYING WK-KEYSEQ2 FROM 1 BY 1" & vbCrLf
'''         testo = testo & Space(14 + indentRed + indentOcc) & "  UNTIL WK-KEYSEQ2 > " & IIf(flgOn, nomeOn, tbOcc2!idxmax) & vbCrLf
'''         testo = testo & Space(14 + indentRed + indentOcc) & "  OR    WK-SQLCODE NOT = ZERO" & vbCrLf
        changeTag RtbOccurs, "<IND-TABLE-OCCURS>", IIf(flgOn, nomeOn, tbOcc2!idxmax)
        Dim indentOcc2 As Integer
        indentOcc2 = 0
'''         If Len(tbOcc2!testoRed) And Not flgOn Then
'''            indentOcc2 = 3
'''            testo = testo & Space(14 + indentRed + indentOcc + indentOcc2) & Replace(getCondizione_Redefines(tbOcc2!testoRed), vbCrLf, vbCrLf & Space(17 + indentRed)) & vbCrLf
'''         End If
'''          'testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "PERFORM " & SostUnderScore(nomeTRed(k)) & "-TO-RR THRU " & SostUnderScore(nomeTRed(k)) & "-TO-RR-EX" & vbCrLf
'''          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "PERFORM 9999-" & SostUnderScore(tbOcc2!nomeTab) & "-TO-RR" & vbCrLf
'''          testo = testo & vbCrLf
'''          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "EXEC SQL " & vbCrLf
'''          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "    INSERT INTO " & Trim(m_author) & tbOcc2!nomeTab & vbCrLf
'''          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "    (" & vbCrLf & CostruisciSelect("", tbOcc2!idTab) & Space(15 + indentRed) & ")" & vbCrLf
'''          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "    VALUES (" & vbCrLf
'''          testo = testo & CostruisciInto("T" & UBound(TabIstr.BlkIstr), tbOcc2!idTab, tbOcc2!nomeTab, "ISRT")
'''          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "    )" & vbCrLf
'''          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "END-EXEC" & vbCrLf
'''          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
'''          If Len(tbOcc2!testoRed) And Not flgOn Then
'''             testo = testo & Space(14 + indentRed + indentOcc + indentOcc2) & "END-IF" & vbCrLf
'''          End If
'''        testo = testo & Space(14 + indentRed + indentOcc) & "END-PERFORM" & vbCrLf
        
        changeTag RtbOccurs, "<COND-COMPACT>", getCondizione_Redefines(tbOcc2!testoRed)
        changeTag RtbOccurs, "<TABLE-OCCURS>", SostUnderScore(tbOcc2!nomeTab)
        
        
        'Set RtbSqlAppo = MaVSAMF_GenRout.RtbSqlAppo
'        RtbSqlAppo.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\SQLINSERT"
        RtbSqlAppo.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\SQLINSERT"
     
        changeTag RtbSqlAppo, "<CREATOR>", Trim(m_author)
        changeTag RtbSqlAppo, "<TABLE-NAME>", tbOcc2!nomeTab

        changeTag RtbSqlAppo, "<INSERT-SQL-INTO>", CostruisciSelect("", tbOcc2!idTab)
        changeTag RtbSqlAppo, "<INSERT-SQL-VALUES>", CostruisciInto("T" & UBound(TabIstr.BlkIstr), tbOcc2!idTab, tbOcc2!nomeTab, "ISRT")
        
        changeTag RtbOccurs, "<<SQLINSERT>>", RtbSqlAppo.text
        changeTag RtbOccCopy, "<<ISRT-OCCURS-SECOND(i)>>", RtbOccurs.text
        
        tbOcc2.MoveNext
     Wend
   
'''     testo = testo & Space(11 + indentRed) & "END-PERFORM" & vbCrLf
     tbOcc.MoveNext
  Wend
  If Not Redef Then testo = testo & Space(11) & "." & vbCrLf
'''  costruisciOccursIns = testo
  If tbOcc.RecordCount < 1 Then
     costruisciOccursIns = ""
     RtbOccurs.text = ""
  Else
    If tipoIstr = "ISRT" Then
       'Mauro 07-03-2007
       'cleanTags RtbOccCopy
       cleanAllTags RtbOccCopy
       costruisciOccursIns = RtbOccCopy.text
    Else
       costruisciOccursIns = RtbOccurs.text
    End If
  End If
 End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' carica rtbFile con il testo del template esplodento eventuali
' TEMPLATE-TAG...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub cleanTags(rtbFile As RichTextBox)
  Dim regExpr As Object
  Dim matches As Variant, Match As Variant
  
  Set regExpr = CreateObject("VBScript.regExp")
  
  'regExpr.Pattern = "<[^>]*(i)>" 'altrimenti mi accorpa tutte quelle di una riga...
  regExpr.Pattern = "<[^>]*\(i\)>>"
  regExpr.Global = True
  
  Set matches = regExpr.Execute(rtbFile.text)
    
  For Each Match In matches
    changeTag rtbFile, Match & "", ""
  Next
  
  Exit Sub
fileErr:
  MsgBox Err.Description
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' carica rtbFile con il testo del template esplodento eventuali
' TEMPLATE-TAG...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub cleanAllTags(rtbFile As RichTextBox)
  Dim regExpr As Object
  Dim matches As Variant, Match As Variant
  
  Set regExpr = CreateObject("VBScript.regExp")
  
  'regExpr.Pattern = "<[^>]*(i)>" 'altrimenti mi accorpa tutte quelle di una riga...
  'regExpr.Pattern = "<[^>]*\(i\)>>"
  regExpr.Pattern = "<[^>]*>>"
  regExpr.Global = True
  
  Set matches = regExpr.Execute(rtbFile.text)
    
  For Each Match In matches
    changeTag rtbFile, Match & "", ""
  Next
  
  Exit Sub
fileErr:
  MsgBox Err.Description
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' E' un SostWord con indentazione (relativa alla posizione del TAG)
' Tratta i TAG "ciclici": devono terminare con (i)...
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub changeTag(Rtb As RichTextBox, Tag As String, ByVal text As String, Optional chkempty As Boolean = False)
  Dim posizione As Long
  Dim crLfPosition As Long
  Dim indentedText As String
  If chkempty And Trim(text) = "" Then
    Exit Sub
  End If
  If isCiclicTag(Tag) Then
    If Len(text) Then text = text & vbCrLf & Tag 'se text="" e' un clean!...
  End If
  While True
    posizione = Rtb.find(Tag, posizione + Len(indentedText))
    Select Case posizione
      Case -1
        Exit Sub
      Case 0
        indentedText = text
      Case Else
        crLfPosition = InStrRev(Rtb.text, vbCrLf, posizione)
        If crLfPosition = 0 Then
          'Linea 1: non ho il fine linea...
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(posizione))
        Else
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(posizione - crLfPosition - 1))
        End If
    End Select
    Rtb.SelStart = posizione
    Rtb.SelLength = Len(Tag)
    Rtb.SelText = indentedText
  Wend
End Sub
Public Function FindCompleteTag(Rtb As RichTextBox, Tag As String) As String
  Dim pos As Integer, char As String
    
  pos = Rtb.find(Tag, 0)
  While posizione > 0
    pos = pos + 1
    Rtb.SelStart = pos
    Rtb.SelLength = 1
    FindCompleteTag = Tag & Rtb.SelText
    If Rtb.SelText = ">" Then
      pos = -1
    End If
  Wend
End Function

Public Sub changeTagciclico(Rtb As RichTextBox, Tag As String, text As String)
  Dim posizione As Long
  Dim crLfPosition As Long
  Dim indentedText As String
    
  indentedText = ""
  
  If isCiclicTag(Tag) Then
    If Len(text) Then text = text & vbCrLf & Tag 'se text="" e' un clean!...
  End If
  
  While True
    posizione = Rtb.find(Tag, posizione + Len(indentedText))
    Select Case posizione
      Case -1
        Exit Sub
      Case 0
        indentedText = text
      Case Else
        crLfPosition = InStrRev(Rtb.text, vbCrLf, posizione)
        If crLfPosition = 0 Then
          'Linea 1: non ho il fine linea...
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(posizione))
        Else
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(posizione - crLfPosition - 1))
        End If
    End Select
    Rtb.SelStart = posizione
    Rtb.SelLength = Len(Tag)
    Rtb.SelText = Rtb.SelText & indentedText
  Wend
End Sub

Public Function getDLET_DB2() As String
  Dim K As Integer
  Dim testo As String
  Dim idRed() As Variant
  Dim nomeTRed() As String, nomeSRed() As String, testoRed() As String
  Dim nomeKeyRed() As Variant, nomeFieldRed() As Variant
  Dim tbred As Recordset
  Dim indRed As Variant
  Dim indentRed As Variant
  Dim flagSel As Boolean
  Dim tabSave As String
  Dim idSave As Variant
  
  Dim Rtb        As RichTextBox
  Dim RtbRed     As RichTextBox
  Dim RtbRedCopy As RichTextBox
  Dim RtbSql     As RichTextBox
  Dim wPath As String
   
  wPath = m_dllFunctions.FnPathPrj
  
  Set Rtb = MaVSAMF_GenRout.RtbPerform ' rtb conterra il testo della perform della routine
  'Rtb.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\DLET\PERFORM-DLET"
  Rtb.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\DLET\PERFORM-DLET"
  
  InitIstrTemplate Rtb, "DLET", "N"
  
  changeTag Rtb, "<DB2-PK-NAME>", TabIstr.BlkIstr(1).pKey
  
  indentRed = 0
  indRed = 1
  ReDim Preserve testoRed(1)
  ReDim Preserve nomeTRed(1)
  ReDim Preserve nomeSRed(1)
  ReDim Preserve idRed(1)
  ReDim Preserve nomeKeyRed(1)
  ReDim Preserve nomeFieldRed(1)
  idRed(1) = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).idTable
  nomeTRed(1) = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Tavola
  nomeSRed(1) = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Segment

' Rob per le delete secondo me non serve
'  Set tbred = m_dllFunctions.Open_Recordset( _
'  "select condizione, rdbms_tbname from psdli_segmenti where idsegmento = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm)
'  If Len(tbred!condizione) Then
'    testoRed(1) = getCondizione_Redefines(tbred!condizione & "_lett")
'  Else
'    testoRed(1) = ""
'  End If
'  If Len(tbred!rdbms_tbname) Then
'    If Left(tbred!rdbms_tbname, 7) = "Storico" Then
'      'SP: distingue tra cics e batch
'      If SwTp Then
'        testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_cics_repl")
'      Else
'        testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_repl")
'      End If
'    End If
'  End If
  
'''MG 240107
'''''''  Set tbred = m_dllFunctions.Open_Recordset("select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
'''''''  "from mgdli_segmenti as a, dmdb2_tabelle as b, dmdb2_index as c " & _
'''''''  "where a.idsegmentoorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
'''''''  " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")

  Set tbred = m_dllFunctions.Open_Recordset("select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
  "from mgdli_segmenti as a, " & DB & "tabelle as b, " & DB & "index as c " & _
  "where a.idsegmentoorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
  " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
  If tbred.RecordCount Then
    flagSel = False
    ReDim Preserve testoRed(tbred.RecordCount + 1)
    ReDim Preserve nomeTRed(tbred.RecordCount + 1)
    ReDim Preserve nomeSRed(tbred.RecordCount + 1)
    ReDim Preserve idRed(tbred.RecordCount + 1)
    ReDim Preserve nomeKeyRed(tbred.RecordCount + 1)
    ReDim Preserve nomeFieldRed(tbred.RecordCount + 1)
    While Not tbred.EOF
      indRed = indRed + 1
      If Len(tbred!condizione) Then
        If Not flagSel Then
          Dim tbsel As Recordset
'''MG 240107
'''''''          Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
'''''''          "from dmdb2_tabelle " & _
'''''''          "where idorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
'''''''          " and TAG like '%DISPATCHER%'")
          Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
          "from " & DB & "tabelle " & _
          "where idorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
          " and TAG like '%DISPATCHER%'")
          If Not tbsel.EOF Then
            'tabella selettrice: riposiziona e abblenka
            'stefano: non salva pi� la tabella precedente, perch� in caso di dispatcher � una
            ' tabella inutile; sarebbe meglio anche evitare di crearla in conversione...
            testoRed(1) = ""
            idRed(1) = tbsel!idTable
            nomeTRed(1) = tbsel!nome
            nomeSRed(1) = "DISPATCHER"
            indRed = 2
            flagSel = True
            If UBound(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).KeyDef) > 0 Then
              nomeKeyRed(1) = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).KeyDef(1).key
            Else
              nomeKeyRed(1) = ""
            End If
            'stefano: campo per il nome del field; uso l'idfield della redefines perch� sembra
            ' corretto.
            Dim tbred2 As Recordset
            Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
            "from psdli_field where idfield = " & tbred!IdField & " and idsegmento = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm)
            If Not tbred2.EOF Then
              nomeFieldRed(1) = tbred2!Nomeidx
            Else
              m_dllFunctions.WriteLog "Field not found: " & tbred!IdField, "Routines Generation"
              nomeFieldRed(1) = ""
            End If
          End If
        Else
        End If
        If flagSel Then
'          testoRed(indRed) = "IF WK-NAMETABLE = '" & tbred!nomeTab & "'"
          testoRed(indRed) = tbred!nomeTab
'        Else
'          testoRed(indRed) = getCondizione_Redefines(tbred!condizione & "_lett")
        End If
        idRed(indRed) = tbred!idTab
        nomeTRed(indRed) = tbred!nomeTab
        nomeSRed(indRed) = tbred!nomeseg
        nomeKeyRed(indRed) = tbred!nomeindex
        'stefano: campo per il nome del field
        Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
        "from mgdli_field where idfield = " & tbred!IdField & " and idsegmento = " & tbred!idSegmento)
        If Not tbred2.EOF Then
          nomeFieldRed(indRed) = tbred2!Nomeidx
        Else
          m_dllFunctions.WriteLog "Virtual Field not found: " & tbred!IdField, "Routines Generation"
          nomeFieldRed(indRed) = ""
        End If
      Else
        m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
      End If
      tbred.MoveNext
      Wend
    tbred.Close
  End If
  
  Dim Start As Integer
  Start = 1
  
  If flagSel Then ' tabella selettore
  
    'Set RtbRed = MaVSAMF_GenRout.RtbDel ' delete della tabella pilota delle redefine
    'RtbRed.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\DLET\DLET-DISP"
    RtbRed.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\DLET\DLET-DISP"
    
    Start = 2
    changeTag RtbRed, "<DLET-DISP-SQL-SELECT>", CostruisciSelect("T1", idRed(1))
    changeTag RtbRed, "<DLET-DISP-SQL-INTO>", CostruisciInto("T1", idRed(1), nomeTRed(1), "REPL")
    
    'SQ CREATOR (prendo quello della principale...)
    ' Mauro 28-03-2007 : eliminazione CREATOR
    'testo = IIf(Len(TabIstr.BlkIstr(1).tableCreator), TabIstr.BlkIstr(1).tableCreator & ".", "") & nomeTRed(1) & " T1"
    testo = nomeTRed(1) & " T1"
    
    changeTag RtbRed, "<DLET-DISP-SQL-FROM>", testo
    
    tabSave = TabIstr.BlkIstr(1).Tavola
    idSave = TabIstr.BlkIstr(1).idTable
    TabIstr.BlkIstr(1).idTable = idRed(1)
    TabIstr.BlkIstr(1).Tavola = nomeTRed(1)
    indentGen = 0
    
    changeTag RtbRed, "<DLET-DISP-SQL-WHERE>", CostruisciWhere(1, 1)
    
    TabIstr.BlkIstr(1).Tavola = tabSave
    TabIstr.BlkIstr(1).idTable = idSave
    
    Dim tabFound As Boolean
    tabFound = False
    Dim IdxTabOk As Integer
    For IdxTabOk = 1 To idxTabOkMax
      If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "DLET" Then
        tabFound = True
        Exit For
      End If
    Next
    If Not tabFound Then
      idxTabOkMax = idxTabOkMax + 1
      ReDim Preserve tabOk(idxTabOkMax)
      ReDim Preserve istrOk(idxTabOkMax)
      tabOk(idxTabOkMax) = nomeTRed(1)
      istrOk(idxTabOkMax) = "DLET"
    End If
    
    changeTag RtbRed, "<TABLE-NAME-DISP>", SostUnderScore(nomeTRed(1))
    
    'stefano: poi lo aggiustiamo con get environment (bisogna fare il
    ' parametro per le nuove copy P: P[tipo-istr][ALIAS]
    GbFileRedOcc = "PD" & Right(getAlias_tabelle(Int(idRed(1))), 6)
    changeTag RtbRed, "<NOME-INCLUDE-REDEFINE>", GbFileRedOcc
    Rtb.text = Replace(Rtb.text, "<<DLET-DISP>>", RtbRed.text)
    
    'Set RtbRedCopy = MaVSAMF_GenRout.RtbRedCopy ' copy include PD delle delete redefine
    'RtbRedCopy.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\DLET\PDREDEFINE"
    RtbRedCopy.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\DLET\PDREDEFINE"
    
    changeTag RtbRedCopy, "<NOME-TABLE-RED>", SostUnderScore(nomeTRed(1))
    changeTag RtbRedCopy, "<NOME-FIELD-RED>", CStr(nomeFieldRed(1))
    changeTag RtbRedCopy, "<DB2-PK-NAME>", SostUnderScore(TabIstr.BlkIstr(BlkFin).pKey)
  
  Else 'se non c e la tabella selettore rimuovo il tag
    Rtb.text = Replace(Rtb.text, "<<DLET-DISP>>", "")
  End If
  
  ' DELEte normale o interna include di delete redefine
  Dim AppoSql As String
  Dim AppoRed As String
  
  'SQ 2-2-07 - Perch� era stato commentato???????????????
  Set RtbSql = MaVSAMF_GenRout.RtbSql ' delete di una tabella
  'RtbSql.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\DLET\SQLDELETE"
  RtbSql.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\DLET\SQLDELETE"
  'SQ 2-2-07 - Perch� era stato commentato???????????????
  Set RtbRed = MaVSAMF_GenRout.RtbRed ' costruzione dele move antecedenti a una delete di redefine
    ''           <<DLET-REDEFINES(i)>>
  'RtbRed.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\DLET\DLET-REDEFINES"
  RtbRed.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\DLET\DLET-REDEFINES"
  AppoSql = RtbSql.text
  AppoRed = RtbRed.text
  
  For K = Start To indRed
    RtbSql.text = AppoSql
    RtbRed.text = AppoRed
    
    indentRed = 0
    If Len(testoRed(K)) Then
      indentRed = 3
      changeTag RtbRed, "<NOME-TABLE-RED>", Trim(testoRed(K))
      changeTag RtbRed, "<DB2-PK-NAME>", SostUnderScore(TabIstr.BlkIstr(BlkFin).pKey)
      changeTag RtbRed, "<NOME-KEY-RED>", CStr(nomeKeyRed(K))
    End If
    
    idSave = TabIstr.BlkIstr(1).idTable
    tabSave = TabIstr.BlkIstr(1).Tavola
    TabIstr.BlkIstr(1).Tavola = nomeTRed(K)
    TabIstr.BlkIstr(1).idTable = idRed(K)
    indentGen = indentRed
    changeTag RtbSql, "<DELETE-SQL-FROM>", CostruisciFrom(1, 1, True)
    changeTag RtbSql, "<DELETE-SQL-WHERE>", CostruisciWhere(1, 1)
    TabIstr.BlkIstr(1).Tavola = tabSave
    TabIstr.BlkIstr(1).idTable = idSave
    
    If (Not flagSel) Or K = 1 Then  ' delete secca senza redefine
      testo = testo & GbTestoRedOcc
      'Rtb.text = Replace(Rtb.text, "<<SQLDELETE>>", RtbSql.text)
      changeTag Rtb, "<<SQLDELETE>>", RtbSql.text
      GbTestoRedOcc = ""
      Rtb.SaveFile "c:\logt", 1
    Else
      'RtbRed.text = Replace(RtbRed.text, "<<SQLDELETE>>", RtbSql.text)
      changeTag RtbRed, "<<SQLDELETE>>", RtbSql.text
      GbTestoRedOcc = GbTestoRedOcc & RtbRed.text & vbCrLf
      Rtb.text = Replace(Rtb.text, "<<SQLDELETE>>", "") 'elimino il tag per la delete secca senza redefine
    End If
  Next
  
  If flagSel Then
    RtbRedCopy.text = Replace(RtbRedCopy.text, "<<DLET-REDEFINES(i)>>", "")
    GbTestoRedOcc = RtbRedCopy.text & GbTestoRedOcc
  Else
    GbTestoRedOcc = ""
  End If
  getDLET_DB2 = Rtb.text
      
End Function

Public Function getREPL_DB2(rtbFile As RichTextBox) As String
  Dim K As Integer, indRed As Integer, indentRed As Integer, baseMove As Integer
  Dim testo As String, tabSave As String, idSegmSave As String, idTabSave As String
  Dim rs As Recordset, tbred As Recordset
  Dim nomeTRed() As String, nomeSRed() As String, testoRed() As String, testoSel() As String
  Dim flagSel As Boolean, tabFound As Boolean, tabFound2 As Boolean, flagOcc As Boolean
  Dim nomeKeyRed() As Variant, nomeFieldRed() As Variant, idRed() As Variant
'  Dim instrLevel As BlkIstrDli
  
  'SQ D
  Dim testoRedOcc As String, includedMember As String
  
  Dim RtbCiclica As RichTextBox
  Dim RtbPerform As RichTextBox
  Dim RtbRed     As RichTextBox
  Dim RtbRedCopy As RichTextBox
  Dim RtbSql     As RichTextBox
  Dim RtbDisp     As RichTextBox
  Dim RtbOccurs  As RichTextBox
  Dim RtbCiclaLivello  As RichTextBox
  Dim RtbUpdate  As RichTextBox

  
  Dim AppoCiclica As String
  Dim AppoCiclicaLivello As String
  Dim AppoSql As String
  Dim AppoUpdate As String
  Dim AppoRed As String
  Dim AppoOccurs As String
  Dim wPath As String
   
  wPath = m_dllFunctions.FnPathPrj
  indentGen = 0
   
  Set RtbPerform = MaVSAMF_GenRout.RtbPerform ' rtb conterra il testo della perform della routine
'stefanopul
  'RtbPerform.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\REPL\PERFORM-REPL"
  RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\REPL\PERFORM-REPL"
  Set RtbCiclica = MaVSAMF_GenRout.RtbCiclica
'stefanopul
 ' RtbCiclica.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\REPL\PREPL-OCCURS"
  RtbCiclica.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\REPL\PREPL-OCCURS"
  Set RtbOccurs = MaVSAMF_GenRout.RtbOccurs
'stefanopul
'  RtbOccurs.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\REPL\REPL-OCCURS"
  RtbOccurs.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\REPL\REPL-OCCURS"
  Set RtbCiclaLivello = MaVSAMF_GenRout.RtbCiclaLivello
'stefanopul
  'RtbCiclaLivello.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\REPL\REPL-LIVELLI"
  RtbCiclaLivello.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\REPL\REPL-LIVELLI"
  Set RtbRed = MaVSAMF_GenRout.RtbRed
'stefanopul
 ' RtbRed.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\REPL\REPL-REDEFINES"
  RtbRed.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\REPL\REPL-REDEFINES"
  Set RtbDisp = MaVSAMF_GenRout.RtbDisp
'stefanopul
'  RtbDisp.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\REPL\REPL-DISP"
  RtbDisp.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\REPL\REPL-DISP"
  Set RtbRedCopy = MaVSAMF_GenRout.RtbRedCopy
'stefanopul
  'RtbRedCopy.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\REPL\PRREDEFINE"
  RtbRedCopy.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\REPL\PRREDEFINE"
  Set RtbSql = MaVSAMF_GenRout.RtbSql
'stefanopul
 ' RtbSql.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\REPL\SQLUPDATE"
  RtbSql.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\REPL\SQLUPDATE"
  Set RtbUpdate = MaVSAMF_GenRout.RtbUpdate
'stefanopul
'  RtbUpdate.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\REPL\UPDATE"
  RtbUpdate.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\REPL\UPDATE"
    
  AppoSql = RtbSql.text
  AppoRed = RtbRed.text
  AppoOccurs = RtbOccurs.text
  AppoCiclica = RtbCiclica.text
  AppoCiclicaLivello = RtbCiclaLivello.text
  AppoUpdate = RtbUpdate.text
  
  InitIstrTemplate RtbPerform, "REPL", "N"
  
  
'  testo = InitIstr("REPL", "N") & vbCrLf
  'SQ - Posso avere pi� UPDATE: WK-SQLCODE in "OR"
'  testo = testo & Space(11) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
  
  'TMP:
  'For level = 1 To UBound(TabIstr.BlkIstr)
  For level = UBound(TabIstr.BlkIstr) To 1 Step -1
    
    RtbSql.text = AppoSql
    RtbRed.text = AppoRed
    RtbOccurs.text = AppoOccurs
    RtbCiclica.text = AppoCiclica
    RtbCiclaLivello.text = AppoCiclicaLivello
    
    'SQ init - tmp: ora � ciclico... pulire quelle che non servono!
    flagSel = False
    tabFound = False
    tabFound2 = False
    flagOcc = False
    testoRedOcc = ""
    includedMember = ""
    
'''Rob    instrLevel = TabIstr.BlkIstr(level)
 
    
    flagSel = False
    baseMove = baseMove + TabIstr.BlkIstr(level).SegLen
''''''' Rob Non credo che serva
'''''''    If level = UBound(TabIstr.BlkIstr) Then
'''''''      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''      ' SQ - standard o da buttare/sistemare?
'''''''      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''      'stefanopippo: 17/08/2005: gestisce le tabelle gi� esistenti evitando
'''''''      Set rs = m_dllFunctions.Open_Recordset("select * from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(level).IdSegm & " and tag = 'OLD'")
'''''''      If rs.RecordCount Then
'''''''         testo = testo & Space(11) & "MOVE ZERO TO SQLCODE WK-SQLCODE." & vbCrLf
'''''''         getREPL_DB2 = testo
'''''''         Exit Function
'''''''      End If
'''''''    End If
    
'    testo = testo & Space(11) & "MOVE '" & TabIstr.BlkIstr(level).Tavola & "' TO WK-NAMETABLE" & vbCrLf
'    testo = testo & Space(11) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
'    testo = testo & Space(11) & "MOVE KRR-" & Replace(TabIstr.BlkIstr(level).pKey, "_", "-") & " TO K01-" & Replace(TabIstr.BlkIstr(level).pKey, "_", "-") & vbCrLf & vbCrLf
    changeTag RtbCiclaLivello, "<TABLE-NAME>", TabIstr.BlkIstr(level).Tavola
    changeTag RtbCiclaLivello, "<DB2-PK-NAME>", Replace(TabIstr.BlkIstr(level).pKey, "_", "-")
      
    'REDEFINES
    indRed = 1
    ReDim testoRed(1)
    ReDim nomeTRed(1)
    ReDim nomeSRed(1)
    ReDim idRed(1)
    ReDim nomeKeyRed(1)
    ReDim nomeFieldRed(1)
    idRed(1) = TabIstr.BlkIstr(level).idTable
    nomeTRed(1) = TabIstr.BlkIstr(level).Tavola
    nomeSRed(1) = TabIstr.BlkIstr(level).Segment
    
    Set tbred = m_dllFunctions.Open_Recordset( _
      "SELECT condizione,rdbms_tbname FROM psdli_segmenti WHERE idsegmento = " & TabIstr.BlkIstr(level).IdSegm)
    If Len(tbred!condizione) Then
      testoRed(1) = getCondizione_Redefines(tbred!condizione & "_lett")
    Else
      testoRed(1) = ""
    End If
    If Len(tbred!rdbms_tbname) Then
      If Left(tbred!rdbms_tbname, 7) = "Storico" Then
         If SwTp Then
            testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_cics_repl")
         Else
            testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_repl")
         End If
      End If
    End If
    tbred.Close
'''MG 240107
''''''    Set tbred = m_dllFunctions.Open_Recordset( _
''''''      "SELECT a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
''''''      " FROM mgdli_segmenti as a, dmdb2_tabelle as b, dmdb2_index as c " & _
''''''      " WHERE a.idsegmentoorigine = " & TabIstr.BlkIstr(level).IdSegm & _
''''''      " AND a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")

    Set tbred = m_dllFunctions.Open_Recordset( _
      "SELECT a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
      " FROM mgdli_segmenti as a, " & DB & "tabelle as b, " & DB & "index as c " & _
      " WHERE a.idsegmentoorigine = " & TabIstr.BlkIstr(level).IdSegm & _
      " AND a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
    If tbred.RecordCount Then
      ReDim Preserve testoRed(tbred.RecordCount + 1)
      ReDim Preserve nomeTRed(tbred.RecordCount + 1)
      ReDim Preserve nomeSRed(tbred.RecordCount + 1)
      ReDim Preserve idRed(tbred.RecordCount + 1)
      ReDim Preserve nomeKeyRed(tbred.RecordCount + 1)
      ReDim Preserve nomeFieldRed(tbred.RecordCount + 1)
      While Not tbred.EOF
        indRed = indRed + 1
         
        If Len(tbred!condizione) Then
          'If tbred!condizione = "SELETTORE" Then
          If Not flagSel Then
            Dim tbsel As Recordset
'''MG 240107
''''''            Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
''''''              "from dmdb2_tabelle " & _
''''''              "where idorigine = " & TabIstr.BlkIstr(level).IdSegm & _
''''''              " and TAG like '%DISPATCHER%'")

          Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
          "from " & DB & "tabelle " & _
          "where idorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
          " and TAG like '%DISPATCHER%'")
            If Not tbsel.EOF Then
      'tabella selettrice: riposiziona e abblenka
      'stefano: non salva pi� la tabella precedente, perch� in caso di dispatcher � una
      ' tabella inutile; sarebbe meglio anche evitare di crearla in conversione...
  '               idRed(indRed) = idRed(1)
  '               nomeTRed(indRed) = nomeTRed(1)
  '               nomeSRed(indRed) = nomeSRed(1)
  '               testoRed(indRed) = "IF WK-NAMETABLE = '" & nomeTRed(1) & "'"
              testoRed(1) = ""
              idRed(1) = tbsel!idTable
              nomeTRed(1) = tbsel!nome
              nomeSRed(1) = "DISPATCHER"
              indRed = 2
              flagSel = True
              If UBound(TabIstr.BlkIstr(level).KeyDef) > 0 Then
                 nomeKeyRed(1) = TabIstr.BlkIstr(level).KeyDef(1).key
              Else
                 nomeKeyRed(1) = ""
              End If
              'stefano: campo per il nome del field; uso l'idfield della redefines perch� sembra corretto.
              Dim tbred2 As Recordset
              Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
                "FROM psdli_field where idfield = " & tbred!IdField & " and idsegmento = " & TabIstr.BlkIstr(level).IdSegm)
              If Not tbred2.EOF Then
                 nomeFieldRed(1) = tbred2!Nomeidx
              Else
                 m_dllFunctions.WriteLog "Field not found: " & tbred!IdField, "Routines Generation"
                 nomeFieldRed(1) = ""
              End If
            End If
          End If
          If flagSel Then
'''             testoRed(indRed) = "IF WK-NAMETABLE = '" & tbred!nomeTab & "'"
             testoRed(indRed) = tbred!nomeTab
          Else
             testoRed(indRed) = getCondizione_Redefines(tbred!condizione & "_lett")
          End If
          idRed(indRed) = tbred!idTab
          nomeTRed(indRed) = tbred!nomeTab
          nomeSRed(indRed) = tbred!nomeseg
          nomeKeyRed(indRed) = tbred!nomeindex
          'stefano: campo per il nome del field
          Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
           "from mgdli_field where idfield = " & tbred!IdField & " and idsegmento = " & tbred!idSegmento)
          If Not tbred2.EOF Then
              nomeFieldRed(indRed) = tbred2!Nomeidx
          Else
              m_dllFunctions.WriteLog "Virtual Field not found: " & tbred!IdField, "Routines Generation"
              nomeFieldRed(indRed) = ""
          End If
        Else
           m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
        End If
        tbred.MoveNext
      Wend
      tbred.Close
    End If
    
    'selettore
    Dim Start As Integer
    Start = 1
    If flagSel Then  ' Gestione table DISP
      Start = 2
      'testo = testo & getCondizione_Redefines(testoSel(1)) & vbCrLf
      'la REPL � sempre monolivello, almeno per le CALL CBLTDLI
'      testo = testo & Space(11) & "EXEC SQL SELECT " & vbCrLf _
'                 & CostruisciSelect("T1", idRed(1))
'      testo = testo & Space(15) & "INTO " & vbCrLf
'      testo = testo & CostruisciInto("T1", idRed(1), nomeTRed(1), "REPL")
'      testo = testo & Space(14) & "FROM " & vbCrLf
      'fissa: per le REPL non serve altro
      'testo = testo & CostruisciFrom(1, 1, True) & vbCrLf
      'SQ CREATOR (prendo quello della principale...)
      'testo = testo & Space(17) & nomeTRed(1) & " T1" & vbCrLf
'      testo = testo & Space(17) & IIf(Len(TabIstr.BlkIstr(1).tableCreator), TabIstr.BlkIstr(1).tableCreator & ".", "") & nomeTRed(1) & " T1" & vbCrLf
' Rob
      ' Mauro 28-03-2007 :  eliminazione CREATOR
      'testo = IIf(Len(TabIstr.BlkIstr(1).tableCreator), TabIstr.BlkIstr(1).tableCreator & ".", "") & nomeTRed(1) & " T1"
      testo = nomeTRed(1) & " T1"
      tabSave = TabIstr.BlkIstr(1).Tavola
      idTabSave = TabIstr.BlkIstr(1).idTable
      TabIstr.BlkIstr(1).idTable = idRed(1)
      TabIstr.BlkIstr(1).Tavola = nomeTRed(1)
      indentGen = 0
      changeTag RtbDisp, "<REPL-DISP-SQL-SELECT>", CostruisciSelect("T1", idRed(1))
      changeTag RtbDisp, "<REPL-DISP-SQL-INTO>", CostruisciInto("T1", idRed(1), nomeTRed(1), "REPL")
      changeTag RtbDisp, "<REPL-DISP-SQL-FROM>", testo
      changeTag RtbDisp, "<REPL-DISP-SQL-WHERE>", CostruisciWhere(1, 1)
'      testo = testo & Space(14) & CostruisciWhere(1, 1)
      TabIstr.BlkIstr(1).Tavola = tabSave
      TabIstr.BlkIstr(1).idTable = idTabSave
'      testo = testo & Space(11) & "END-EXEC" & vbCrLf
'      testo = testo & Space(11) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
      tabFound = False
    
      
      Dim IdxTabOk As Integer
      For IdxTabOk = 1 To idxTabOkMax
        If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "REPL" Then
          tabFound = True
          Exit For
        End If
      Next
      If Not tabFound Then
         idxTabOkMax = idxTabOkMax + 1
         ReDim Preserve tabOk(idxTabOkMax)
         ReDim Preserve istrOk(idxTabOkMax)
         tabOk(idxTabOkMax) = nomeTRed(1)
         istrOk(idxTabOkMax) = "REPL"
      End If
'''      testo = testo & vbCrLf & _
'''        Space(6) & "*** REDEFINES" & vbCrLf & _
'''        Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
'''        Space(11) & "   PERFORM 9999-" & Replace(nomeTRed(1), "_", "-") & "-REPL" & vbCrLf & _
'''        Space(11) & "END-IF" & vbCrLf & vbCrLf
    
            changeTag RtbDisp, "<TABLE-NAME-DISP>", Replace(nomeTRed(1), "_", "-")
              
            changeTag RtbCiclaLivello, "<<REPL-DISP>>", RtbDisp.text
    
      
'''      testoRedOcc = testoRedOcc & Space(7) & "9999-" & Replace(nomeTRed(1), "_", "-") & "-REPL SECTION." & vbCrLf & vbCrLf
'''      testoRedOcc = testoRedOcc & _
'''        Space(11) & "MOVE " & nomeFieldRed(1) & " OF KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf & _
'''        Space(11) & "  TO KRD-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf & _
'''        Space(6) & "*** REDEFINES" & vbCrLf
    
          changeTag RtbRedCopy, "<NOME-TABLE-RED>", Replace(nomeTRed(1), "_", "-")
          changeTag RtbRedCopy, "<NOME-FIELD-RED>", CStr(nomeFieldRed(1))
          changeTag RtbRedCopy, "<DB2-PK-NAME>", Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-")
      
      'stefano: poi lo aggiustiamo con get environment (bisogna fare il parametro per le nuove copy P: P[tipo-istr][ALIAS]
      includedMember = "PR" & Right(getAlias_tabelle(Int(idRed(1))), 6)
    End If

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    For K = Start To indRed
      indentRed = 0
      RtbRed.text = AppoRed
      RtbUpdate.text = AppoUpdate
      RtbSql.text = AppoSql
      RtbCiclica.text = AppoCiclica
      If Len(testoRed(K)) Then
        indentRed = 3
        'Redefines:
'        testoRedOcc = testoRedOcc & Space(11) & Trim(testoRed(k)) & vbCrLf & _
'          Space(11 + indentRed) & "MOVE KRD-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO" & vbCrLf & _
'          Space(11 + indentRed) & "     KRD-" & nomeKeyRed(k) & vbCrLf & _
'          Space(11 + indentRed) & "MOVE '" & nomeTRed(k) & "' TO WK-NAMETABLE" & vbCrLf & _
'          Space(11 + indentRed) & "MOVE KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO KRR-" & nomeKeyRed(k) & vbCrLf & _
'          Space(11 + indentRed) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf & _
'          Space(11 + indentRed) & "MOVE KRR-" & nomeKeyRed(k) & " TO K01-" & nomeKeyRed(k) & vbCrLf
        changeTag RtbRed, "<NOME-TABLE-RED>", Trim(testoRed(K))
        changeTag RtbRed, "<DB2-PK-NAME>", Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-")
        changeTag RtbRed, "<NOME-KEY-RED>", CStr(nomeKeyRed(K))
      End If
' non so se serve Rob   changeTag RtbRed, "<NOME-SEG-RED>", nomeSRed(k)
      'Comune:
      'Differenziazione: Move posizionale!
      If level = 1 Then
       ''' testoRedOcc = testoRedOcc & Space(11 + indentRed) & _
'''        "MOVE LNKDB-DATA-AREA(1) TO RD-" & nomeSRed(k) & vbCrLf
        changeTag RtbUpdate, "<AREA>", "1"
      Else
        'TMP: AL CONTRARIO non usare baseMove!
        Dim j As Integer
        baseMove = 0
        For j = 1 To level - 1
          baseMove = baseMove + TabIstr.BlkIstr(level).SegLen
        Next
'''        testoRedOcc = testoRedOcc & Space(11 + indentRed) & _
'''          "MOVE LNKDB-DATA-AREA(1)(" & baseMove + 1 & ":" & TabIstr.BlkIstr(level).SegLen & ") TO RD-" & nomeSRed(k) & vbCrLf
        changeTag RtbUpdate, "<AREA>", "1)(" & baseMove + 1 & ":" & TabIstr.BlkIstr(level).SegLen
      End If
      changeTag RtbUpdate, "<NOME-SEGM>", nomeSRed(K)
'''      testoRedOcc = testoRedOcc & Space(11 + indentRed) & "PERFORM 9999-" & Replace(nomeTRed(k), "_", "-") & "-TO-RR" & vbCrLf
      changeTag RtbUpdate, "<TABLE-NAME>", Replace(nomeTRed(K), "_", "-")
      
      'SQ 1-08-06 - Le tabelle con OCCURS possono NON avere colonne nella tabella principale
      Dim setStatement As String
      setStatement = CostruisciSet(idRed(K), nomeTRed(K))
      If Len(setStatement) Then
        tabSave = TabIstr.BlkIstr(1).Tavola
        idTabSave = TabIstr.BlkIstr(1).idTable
        TabIstr.BlkIstr(1).idTable = idRed(K)
        TabIstr.BlkIstr(1).Tavola = nomeTRed(K)
        indentGen = indentRed
'''        testoRedOcc = testoRedOcc & Space(11 + indentRed) & "EXEC SQL UPDATE " & vbCrLf
'''        testoRedOcc = testoRedOcc & CostruisciFrom(1, 1, True)
'''        testoRedOcc = testoRedOcc & Space(11 + indentRed) & "    SET " & vbCrLf
'''        testoRedOcc = testoRedOcc & setStatement
'''        testoRedOcc = testoRedOcc & CostruisciWhere(1, 1)
        changeTag RtbSql, "<UPDATE-SQL-TABLE>", CostruisciFrom(1, 1, True)
        changeTag RtbSql, "<UPDATE-SQL-SET>", setStatement
        changeTag RtbSql, "<UPDATE-SQL-WHERE>", CostruisciWhere(1, 1)
        TabIstr.BlkIstr(1).Tavola = tabSave
        TabIstr.BlkIstr(1).idTable = idTabSave
'''        testoRedOcc = testoRedOcc & Space(11 + indentRed) & "END-EXEC" & vbCrLf
'''        'SQ
'''        'testoRedOcc = testoRedOcc & Space(11 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
'''        testoRedOcc = testoRedOcc & _
'''          Space(11 + indentRed) & "IF SQLCODE NOT = ZERO" & vbCrLf & _
'''          Space(11 + indentRed) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf & _
'''          Space(11 + indentRed) & "END-IF" & vbCrLf
      'SQ
'''      Else
'''        testoRedOcc = testoRedOcc & Space(11 + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
      End If
      ''''''''''''''
      ' OCCURS
      '''''''''''''
      GbTestoRedOcc2 = ""
      If (Not flagSel) Or K = 1 Then
       Dim tbOcc As Recordset
'''MG 240107
'''''       Set tbOcc = m_dllFunctions.Open_Recordset("select *" & _
'''''           " from dmdb2_tabelle as a, dmdb2_index as b" & _
'''''           " where a.idtable = b.idtable and a.idtablejoin = " & idRed(K) & _
'''''           " and a.TAG like '%OCCURS%' and b.tipo = 'P' order by a.nome")

       Set tbOcc = m_dllFunctions.Open_Recordset("select *" & _
           " from " & DB & "tabelle as a, " & DB & "index as b" & _
           " where a.idtable = b.idtable and a.idtablejoin = " & idRed(K) & _
           " and a.TAG like '%OCCURS%' and b.tipo = 'P' order by a.nome")
       If Not tbOcc.EOF Then
         Dim IdxTabOk2 As Integer
         tabFound2 = False
         For IdxTabOk2 = 1 To idxTabOkMax2
            If tabOk2(IdxTabOk2) = idRed(K) And istrOk2(IdxTabOk2) = "REPL" Then
               tabFound2 = True
               Exit For
            End If
         Next
         If Not tabFound2 Then
            idxTabOkMax2 = idxTabOkMax2 + 1
            ReDim Preserve tabOk2(idxTabOkMax2)
            ReDim Preserve istrOk2(idxTabOkMax2)
            tabOk2(idxTabOkMax2) = idRed(K)
            istrOk2(idxTabOkMax2) = "REPL"
         End If
         flagOcc = False
         If Not tabFound2 Then
          'SQ ! controllare!!!!!!!!!!!!
          'GbTestoRedOcc2 = costruisciOccursIns(idRed(k), indentRed, "REPL", flagOcc, False)
'''          GbTestoRedOcc2 = Space(7) & "9999-" & Replace(TabIstr.BlkIstr(level).Tavola, "_", "-") & "-REPL" & " SECTION." & vbCrLf & _
'''                            Space(11 + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf & _
'''                            costruisciOccursIns(idRed(k), indentRed, "REPL", flagOcc, False, RtbOccurs)
           GbTestoRedOcc2 = costruisciOccursIns(idRed(K), indentRed, "REPL", flagOcc, False, RtbOccurs, AppoOccurs)
         End If
         If flagOcc Or tabFound2 Then
'''            testoRedOcc = testoRedOcc & vbCrLf & _
'''              Space(6) & "*** OCCURS" & vbCrLf & _
'''              Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
'''              Space(11) & "   PERFORM 9999-" & Replace(nomeTRed(1), "_", "-") & "-REPL" & vbCrLf & _
'''              Space(11) & "END-IF" & vbCrLf & vbCrLf
            'stefano: poi lo aggiustiamo con get environment (bisogna fare il
            ' parametro per le nuove copy P: P[tipo-istr][ALIAS]
             includedMember = "PR" & Right(getAlias_tabelle(Int(idRed(1))), 6)
             RtbCiclica.text = AppoCiclica
             changeTag RtbCiclica, "<TABLE-NAME>", Replace(nomeTRed(1), "_", "-")
             changeTag RtbCiclica, "<NOME-INCLUDE-OCCURS>", includedMember
             changeTag RtbPerform, "<<PREPL-OCCURS(i)>>", RtbCiclica.text

             changeTag RtbUpdate, "<<SQLUPDATE>>", RtbSql.text
             ' Ripulisco i tag
             changeTag RtbCiclaLivello, "<<REPL-DISP>>", ""
             changeTag RtbCiclaLivello, "<<REPL-REDEFINES(i)>>", ""
             changeTag RtbCiclaLivello, "<<REPL-OCCURS(i)>>", ""
''''  Sposto           ' Risolvo tag PERFORM per il livello corrente
''''             changeTag RtbPerform, "<<REPL-LIVELLI(i)>>", RtbCiclaLivello.text
''''             changeTag RtbPerform, "<<PREPL-OCCURS>>", RtbCiclica.text
''''       '      changeTag RtbPerform, "<<UPDATE>>", RtbUpdate.text
         End If
        End If
      Else
        changeTag RtbUpdate, "<<SQLUPDATE>>", RtbSql.text
        changeTag RtbRed, "<<UPDATE>>", RtbUpdate.text
        
        RtbOccurs.text = AppoOccurs
        testoRedOcc = testoRedOcc & costruisciOccursIns(idRed(K), indentRed, "REPL", flagOcc, True, RtbOccurs, AppoOccurs)
        changeTag RtbRed, "<<REPL-OCCURS(i)>>", RtbOccurs.text
      
      End If
      
'''      If Len(testoRed(k)) Then
'''        testoRedOcc = testoRedOcc & Space(11) & "END-IF" & vbCrLf
'''      End If
      
      If (Not flagSel) Or K = 1 Then
        testo = testo & testoRedOcc
        GbTestoRedOcc = GbTestoRedOcc2
        ' Ripulisco i tag
        changeTag RtbCiclaLivello, "<<REPL-DISP>>", ""
        changeTag RtbCiclaLivello, "<<REPL-REDEFINES(i)>>", ""
        changeTag RtbCiclaLivello, "<<REPL-OCCURS(i)>>", ""
        
        changeTag RtbUpdate, "<<SQLUPDATE>>", RtbSql.text
        changeTag RtbCiclaLivello, "<<UPDATE>>", RtbUpdate.text
'''      Else
'''        GbTestoRedOcc = testoRedOcc & Space(11) & "." & vbCrLf
      End If
      '''''''''''''''''''''''
      ' Scrittura Copy P
      '''''''''''''''''''''''
      If Len(GbTestoRedOcc) Then
        Dim rtbFileSave As String
        If K = Start Then 'SQ tmp... verificare (per non includere N volte in caso di redefines)
          rtbFileSave = rtbFile.text  'salvo
'''          rtbFile.text = GbTestoRedOcc
          rtbFile.text = RtbOccurs.text
' Rob elimino per ora          rtbFile.SaveFile VSAM2Rdbms.drPathDb & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\cpy\" & includedMember, 1
          rtbFile.text = rtbFileSave
          GbFileRedOccs(UBound(GbFileRedOccs)) = includedMember
          ReDim Preserve GbFileRedOccs(UBound(GbFileRedOccs) + 1)
        End If
        GbTestoRedOcc = ""  'flag x chiamante
        'transitorio: buttare
        GbFileRedOcc = ""
      End If
    Next K
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Risolvo tag PERFORM per il livello corrente
    changeTag RtbPerform, "<<REPL-LIVELLI(i)>>", RtbCiclaLivello.text
'             changeTag RtbPerform, "<<PREPL-OCCURS(i)>>", RtbCiclica.text
       '      changeTag RtbPerform, "<<UPDATE>>", RtbUpdate.text
   
   'changeTagciclico RtbPerform, "<<REPL-LIVELLI(i)>>", RtbCiclaLivello.text
   ' changeTag RtbPerform, "<<REPL-LIVELLI(i)>>", RtbCiclaLivello.text
  Next level
 
  cleanTags RtbPerform
  getREPL_DB2 = RtbPerform.text
End Function
Public Function getISRT_GSAM() As String
  
  Dim RtbPerform As RichTextBox
  Dim wPath As String
   
  wPath = m_dllFunctions.FnPathPrj
  
  Set RtbPerform = MaVSAMF_GenRout.RtbPerform ' rtb conterra il testo della perform della routine
  
  RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\GSAM\ISRT-GSAM"
    
  InitIstrTemplate RtbPerform, "ISRT", "N"
  
  cleanTags RtbPerform
  getISRT_GSAM = RtbPerform.text
End Function
Public Function getGN_GSAM() As String
  
  Dim RtbPerform As RichTextBox
  Dim wPath As String
   
  wPath = m_dllFunctions.FnPathPrj
  
  Set RtbPerform = MaVSAMF_GenRout.RtbPerform ' rtb conterra il testo della perform della routine
  
  RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\GSAM\GN-GSAM"
    
  InitIstrTemplate RtbPerform, "GN", "N"
  
  cleanTags RtbPerform
  getGN_GSAM = RtbPerform.text
End Function
Public Function getOPEN_GSAM() As String
  
  Dim RtbPerform As RichTextBox
  Dim wPath As String
  Dim tb1 As Recordset
   
  wPath = m_dllFunctions.FnPathPrj
  
  Set RtbPerform = MaVSAMF_GenRout.RtbPerform ' rtb conterra il testo della perform della routine
  
  If TabIstr.procOpt = "LS" Then 'file GSAM in output
     RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\GSAM\OPEN-GSAM-OUTPUT"
  Else
     RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\GSAM\OPEN-GSAM-INPUT"
  End If
    
  InitIstrTemplate RtbPerform, "OPEN", "N"
  
  cleanTags RtbPerform
  getOPEN_GSAM = RtbPerform.text
End Function
Public Function getCLSE_GSAM() As String
  
  Dim RtbPerform As RichTextBox
  Dim wPath As String
   
  wPath = m_dllFunctions.FnPathPrj
  
  Set RtbPerform = MaVSAMF_GenRout.RtbPerform ' rtb conterra il testo della perform della routine
  
  RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\GSAM\CLSE-GSAM"
    
  InitIstrTemplate RtbPerform, "CLSE", "N"
  
  cleanTags RtbPerform
  getCLSE_GSAM = RtbPerform.text
End Function
Public Function getISRT_DB2(rtbFile As RichTextBox) As String
  Dim K As Integer, isrtlevel As Integer
  Dim rs As ADODB.Recordset, tbred As ADODB.Recordset
  Dim tbsel As ADODB.Recordset
  Dim NomeT2 As String, NomePk As String, nomeKey As String, NOMEKEYDLI As String
  Dim testo As String, NomeT As String
  '''''''''Dim idRed() As Variant 'redefines
  '''''''''Dim nomeTRed() As String, nomeSRed() As String, testoRed() As String
  Dim indRed As Integer, indentRed As Integer
  Dim flagSel As Boolean
    
  Dim RtbPerform As RichTextBox
  Dim RtbCiclaLivello As RichTextBox
  Dim RtbRedCopy As RichTextBox
  Dim RtbOccurs As RichTextBox
  Dim RtbCiclica As RichTextBox
  Dim RtbDisp As RichTextBox
  Dim RtbSql As RichTextBox
  Dim RtbRed As RichTextBox
  Dim RtbInsert As RichTextBox
     
  Dim AppoCiclica As String
  Dim AppoCiclicaLivello As String
  Dim AppoSql As String
  Dim AppoUpdate As String
  Dim AppoRed As String
  Dim AppoOccurs As String
  Dim AppoInsert As String
  Dim wPath As String
   
  wPath = m_dllFunctions.FnPathPrj
  Set RtbPerform = MaVSAMF_GenRout.RtbPerform ' rtb conterra il testo della perform della routine
'stefanopul
  'RtbPerform.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\PERFORM-ISRT"
  RtbPerform.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\PERFORM-ISRT"
  
  Set RtbOccurs = MaVSAMF_GenRout.RtbOccurs ' rtb conterra il testo della perform della routine
'stefanopul
  'RtbOccurs.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\ISRT-OCCURS"
  RtbOccurs.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\ISRT-OCCURS"

  Set RtbCiclaLivello = MaVSAMF_GenRout.RtbCiclaLivello
'stefanopul
  'RtbCiclaLivello.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\ISRT-LIVELLI"
  RtbCiclaLivello.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\ISRT-LIVELLI"

  Set RtbRedCopy = MaVSAMF_GenRout.RtbRedCopy
'stefanopul
  'RtbRedCopy.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\PIREDEFINE"
  RtbRedCopy.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\PIREDEFINE"

  Set RtbOccurs = MaVSAMF_GenRout.RtbOccurs
'stefanopul
  'RtbOccurs.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\ISRT-OCCURS"
  RtbOccurs.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\ISRT-OCCURS"
  
  Set RtbCiclica = MaVSAMF_GenRout.RtbCiclica
'stefanopul
 ' RtbCiclica.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\PISRT-OCCURS"
  RtbCiclica.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\PISRT-OCCURS"

  Set RtbDisp = MaVSAMF_GenRout.RtbDisp
'stefanopul
'  RtbDisp.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\ISRT-DISP"
  RtbDisp.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\ISRT-DISP"

  Set RtbSql = MaVSAMF_GenRout.RtbSql
'stefanopul
  'RtbSql.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\SQLINSERT"
  RtbSql.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\SQLINSERT"

  Set RtbRed = MaVSAMF_GenRout.RtbRed
'stefanopul
 ' RtbRed.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\ISRT-REDEFINES"
  RtbRed.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\ISRT-REDEFINES"

  Set RtbInsert = MaVSAMF_GenRout.RtbInsert
'stefanopul
'  RtbInsert.LoadFile wpath & "\input-prj\Template\ROUTINE\COBOL\ISRT\ISRT-INSERT"
  RtbInsert.LoadFile wPath & "\input-prj\Template\imsdb\esternalizzazione\ROUTINE\COBOL\ISRT\ISRT-INSERT"

  AppoSql = RtbSql.text
  AppoRed = RtbRed.text
  AppoOccurs = RtbOccurs.text
  AppoCiclica = RtbCiclica.text
  AppoCiclicaLivello = RtbCiclaLivello.text
  AppoInsert = RtbInsert.text

  indentGen = 0 '?
  
'''  testo = InitIstr("ISRT", "S") & vbCrLf
  InitIstrTemplate RtbPerform, "ISRT", "N"
 
'''  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''  ' Tabelle "OLD" (gestione standard o pezza?)
'''  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''  'stefanopippo: 17/08/2005: gestisce le tabelle gi� esistenti evitando l'insert; vale solo l'ultimo livello
'''  'spostato dopo la select, in quanto deve comunque salvare la chiave per il posizionamento dei figli
'''  Set rs = m_dllFunctions.Open_Recordset("select * from dmdb2_tabelle where tag = 'OLD' and idOrigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm)
'''  If rs.RecordCount Then
'''    '''flagOld = True
'''    'SP 15/9 vogliono cos�, ripuliamo tutto
'''    testo = testo & Space(11) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
'''    getISRT_DB2 = testo
'''    rs.Close
'''    Exit Function
'''  End If
'''  rs.Close
  
  ''''''''''''''''''''''''''''''''''''''''''
  ' Qualificazione sui livelli precedenti
  ''''''''''''''''''''''''''''''''''''''''''
  'SQ 21-07-06
  'Ex controllo interno alla getGU_DB2: direi che qui � molto meglio
  'P.S.: Dovremmo entrare solo se livelli qualificati...
  '      !!!! CONSIDERARE isrtLevel ("D"), no sempre l'ultimo !!!!
  If UBound(TabIstr.BlkIstr) > 1 Then
    'Posizionamento sul parent: lettura come se fosse GU sui livelli precedenti
  '''  testo = testo & getGU_DB2(rtbFile, "ISRT", False)
      changeTag RtbPerform, "<<ISRT-GU>>", getGU_DB2(rtbFile, "ISRT", False)
  Else
    'Servono?
    GbTestoFetch = ""
    GbTestoRedOcc = ""
    GbFileRedOcc = ""
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' GESTIONE C.C. "D" - "PATH CALL"
  ' Gli inserimenti sono su N livelli
  ' SQ 2-08-06
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'L'ultimo livello � "D" per default (lo appioppo):
  TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).CodCom = "D" & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).CodCom
  'Cerco "isrtLevel": primo livello d'inserimento:
  For isrtlevel = 1 To UBound(TabIstr.BlkIstr)
    If InStr(TabIstr.BlkIstr(isrtlevel).CodCom, "D") Then Exit For
  Next
  
  For isrtlevel = isrtlevel To UBound(TabIstr.BlkIstr)
    
    RtbSql.text = AppoSql
    RtbRed.text = AppoRed
    RtbOccurs.text = AppoOccurs
    RtbCiclica.text = AppoCiclica
    RtbCiclaLivello.text = AppoCiclicaLivello
    RtbInsert.text = AppoInsert
    
    NomeT = TabIstr.BlkIstr(isrtlevel).Tavola
    'SQ 1-08-06 - CREATOR
    m_author = TabIstr.BlkIstr(isrtlevel).tableCreator  'buttare via questa variabile globale...
    If Len(m_author) Then m_author = m_author & "."
    'CHIARIRE
    If isrtlevel = 1 Then
      NomeT2 = getParentTable(CLng(TabIstr.BlkIstr(isrtlevel).IdSegm))  'togliere cast e cambiare function
      If Len(NomeT2) Then
        'SEGMENTO NON RADICE: RESTORE
        testo = testo & Space(11) & "MOVE '" & NomeT2 & "' TO WK-NAMETABLE" & vbCrLf
        'testo = testo & Space(11) & "PERFORM REST-FDBKEY THRU REST-FDBKEY-EX" & vbCrLf & vbCrLf
        testo = testo & Space(11) & "PERFORM 10000-REST-FDBKEY" & vbCrLf & vbCrLf
      End If
    End If
'''    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
'''    testo = testo & Space(11) & "   MOVE '" & NomeT & "' TO WK-NAMETABLE " & vbCrLf
'''    testo = testo & Space(11) & "   PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
'''    testo = testo & Space(11) & "END-IF" & vbCrLf
    changeTag RtbCiclaLivello, "<TABLE-NAME>", NomeT
    '''indentRed = 0
    Set tbred = m_dllFunctions.Open_Recordset( _
      "Select condizione, rdbms_tbname from psdli_segmenti " & _
      " where idsegmento = " & TabIstr.BlkIstr(isrtlevel).IdSegm)
    indRed = 1
    ReDim Preserve testoRed(1)
    ReDim Preserve nomeTRed(1)
    ReDim Preserve nomeSRed(1)
    ReDim Preserve idRed(1)
    'idRed(1) = TabIstr.BlkIstr(isrtLevel).IdSegm
    idRed(1) = TabIstr.BlkIstr(isrtlevel).idTable
    nomeTRed(1) = TabIstr.BlkIstr(isrtlevel).Tavola
    nomeSRed(1) = TabIstr.BlkIstr(isrtlevel).Segment
    If Len(tbred!condizione) Then
      testoRed(1) = getCondizione_Redefines(tbred!condizione)
    Else
      testoRed(1) = ""
    End If
    If Len(tbred!rdbms_tbname) Then
      ''''''''''''''''''''''''''''''''''''
      'SQ - ALTRA GESTIONE NON STANDARD?
      ''''''''''''''''''''''''''''''''''''
      If Left(tbred!rdbms_tbname, 7) = "Storico" Then
         'SP: distingue tra cics e batch
  '      If SwTp And m_dllFunctions.FnNomeDB <> "banca.mty" Then
         If SwTp Then
            testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_cics_isrt")
         Else
            testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_isrt")
         End If
      End If
    End If
    tbred.Close
    ''''''''''''''''''''''
    ' REDEFINES
    ''''''''''''''''''''''
  '   Set tbred = m_dllFunctions.Open_Recordset("select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione " & _
  '               "from psdli_segmenti as a, dmdb2_tabelle as b " & _
  '               "where a.idsegmentoorigine = " & TabIstr.BlkIstr(isrtLevel).IdSegm & _
  '               " and a.idsegmento = b.idorigine order by a.condizione desc")
'''MG 240107
''''''    Set tbred = m_dllFunctions.Open_Recordset( _
''''''      "select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idtab " & _
''''''      " from mgdli_segmenti as a, dmdb2_tabelle as b " & _
''''''      " where a.idsegmentoorigine = " & TabIstr.BlkIstr(isrtLevel).IdSegm & _
''''''      " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' order by b.nome")

          Set tbred = m_dllFunctions.Open_Recordset("select idtable, nome " & _
          "from " & DB & "tabelle " & _
          "where idorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
          " and TAG like '%DISPATCHER%'")
    If tbred.RecordCount Then
      flagSel = False
      ReDim Preserve testoRed(tbred.RecordCount + 1)
      ReDim Preserve nomeTRed(tbred.RecordCount + 1)
      ReDim Preserve nomeSRed(tbred.RecordCount + 1)
      ReDim Preserve idRed(tbred.RecordCount + 1)
      While Not tbred.EOF
        indRed = indRed + 1
        If Len(tbred!condizione) Then
          'stefano: cerca il selettore nel nuovo modo
          'If tbred!condizione = "SELETTORE" Then
          If Not flagSel Then
'''MG 240107
'''''              Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
'''''                 "from dmdb2_tabelle " & _
'''''                 "where idorigine = " & TabIstr.BlkIstr(isrtLevel).IdSegm & _
'''''                 " and TAG like '%DISPATCHER%'")

          Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
          "from " & DB & "tabelle " & _
          "where idorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
          " and TAG like '%DISPATCHER%'")
              If Not tbsel.EOF Then
                'tabella selettrice: riposiziona e abblenka
                'stefano: non salva pi� la tabella precedente, perch� in caso di dispatcher � una
                ' tabella inutile; sarebbe meglio anche evitare di crearla in conversione...
                testoRed(1) = ""
                idRed(1) = tbsel!idTable
                nomeTRed(1) = tbsel!nome
                'nomeSRed(1) = "DISPATCHER"
                nomeSRed(1) = TabIstr.BlkIstr(isrtlevel).Segment
                indRed = 2
                flagSel = True
              End If
          End If
          testoRed(indRed) = getCondizione_Redefines(tbred!condizione)
          'idRed(indRed) = tbred!idSegmento
          idRed(indRed) = tbred!idTab
          nomeTRed(indRed) = tbred!nomeTab
          nomeSRed(indRed) = tbred!nomeseg
          'SP redefines
        Else
          m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
        End If
        tbred.MoveNext
      Wend
      tbred.Close
    End If
    GbTestoRedOcc = ""
    
    For K = 1 To indRed
    
    RtbSql.text = AppoSql
    RtbRed.text = AppoRed
    RtbInsert.text = AppoInsert
    
      indentRed = 0
        
      Dim tabFound As Boolean
      tabFound = False
      If flagSel And K > 1 Then 'solo per le redefines con selettore
        Dim IdxTabOk As Integer
        For IdxTabOk = 1 To idxTabOkMax
          If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "ISRT" Then
            tabFound = True
            Exit For
          End If
        Next
        If Not tabFound Then
          idxTabOkMax = idxTabOkMax + 1
          ReDim Preserve tabOk(idxTabOkMax)
          ReDim Preserve istrOk(idxTabOkMax)
          tabOk(idxTabOkMax) = nomeTRed(1)
          istrOk(idxTabOkMax) = "ISRT"
        End If
        If K = 2 Then 'Primo elemento: 1 � la DISPATHER
'''          GbTestoRedOcc = GbTestoRedOcc & Space(7) & "9999-" & Replace(nomeTRed(1), "_", "-") & "-ISRT SECTION." & vbCrLf & vbCrLf
'''          GbTestoRedOcc = GbTestoRedOcc & Space(6) & "*** REDEFINES" & vbCrLf
           changeTag RtbRedCopy, "<NOME-TABLE-RED>", Replace(nomeTRed(1), "_", "-")
        End If
      End If
      If Len(testoRed(K)) Then
        'Condizione:
'''        GbTestoRedOcc = GbTestoRedOcc & Space(11) & Trim(testoRed(k)) & vbCrLf
        changeTag RtbRed, "<REDEFINE-COND>", Trim(testoRed(K))
        'SQ tmp: sistemare i 14!
         'indentRed = 3
         indentRed = 0
      Else
        'SQ
 '''       GbTestoRedOcc = GbTestoRedOcc & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
 '''  Caso RtbInsert
      End If
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE '" & nomeTRed(k) & "' TO WK-NAMETABLE " & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "PERFORM 9999-SETK-WKEYADL" & vbCrLf
      changeTag RtbRed, "<NOME-TABLE-RED>", nomeTRed(K)
      changeTag RtbInsert, "<TABLE-NAME>", nomeTRed(K)
      
      If K = 1 And Not flagSel Then
   '''     GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE LNKDB-DATA-AREA(" & Format(isrtLevel) & ") TO RD-" & nomeSRed(k) & vbCrLf
        changeTag RtbRed, "<AREA-NAME>", Format(isrtlevel)
        changeTag RtbRed, "<RED-NAME>", nomeSRed(K)
        changeTag RtbInsert, "<AREA-NAME>", Format(isrtlevel)
        changeTag RtbInsert, "<RED-NAME>", nomeSRed(K)
      Else
        If Not flagSel Then
   '''       GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE LNKDB-DATA-AREA(WK-IND2) TO RD-" & nomeSRed(k) & vbCrLf
          changeTag RtbRed, "<AREA-NAME>", "WK-IND2"
          changeTag RtbRed, "<RED-NAME>", nomeSRed(K)
        End If
      End If
      ' Mauro 02-04-2007 : Gestione Trigger INSERT
''''      CreateTrigger isrtlevel
      
      'SQ 'se lo metto in testa lo perde: chi lo resetta?!
      indentGen = 8
      
     ''' GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "PERFORM 9999-" & Replace(nomeTRed(k), "_", "-") & "-TO-RR" & vbCrLf & vbCrLf
      '''If Not flagOld Then
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "EXEC SQL " & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "    INSERT INTO " & m_author & nomeTRed(k) & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "    (" & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & CostruisciSelect("", idRed(k))
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "    )" & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "    VALUES (" & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & CostruisciInto("T" & isrtLevel, idRed(k), nomeTRed(k), "ISRT")
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "    )" & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "END-EXEC" & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
      
      changeTag RtbSql, "<CREATOR>", m_author
      changeTag RtbSql, "<TABLE-NAME>", nomeTRed(K)
      changeTag RtbSql, "<INSERT-SQL-INTO>", CostruisciSelect("", idRed(K))
      changeTag RtbSql, "<INSERT-SQL-VALUES>", CostruisciInto("T" & isrtlevel, idRed(K), nomeTRed(K), "ISRT")
      changeTag RtbRed, "<<SQLINSERT>>", RtbSql.text
      changeTag RtbInsert, "<<SQLINSERT>>", RtbSql.text
      changeTag RtbDisp, "<TABLE-NAME>", nomeTRed(K)
      changeTag RtbDisp, "<<SQLINSERT>>", RtbSql.text
      
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "IF WK-SQLCODE NOT = ZERO" & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "   IF WK-SQLCODE = -803" & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "      MOVE '" & nomeTRed(k) & "' TO WK-NAMETABLE " & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "      PERFORM 9999-SETK-ADLWKEY" & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "      MOVE '" & NomeT & "' TO WK-NAMETABLE " & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "      PERFORM 11000-STORE-FDBKEY" & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "   END-IF" & vbCrLf
'''      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "END-IF" & vbCrLf 'exit non funzionante
      
      changeTag RtbRed, "<TABLE-NAME>", NomeT
      
      'SQ solo se non in copy...
      If Len(testoRed(K)) = 0 Then
'''        GbTestoRedOcc = GbTestoRedOcc & Space(11) & "END-IF" & vbCrLf
      '  If Not flagSel Then changeTag RtbCiclaLivello, "<<ISRT-INSERT>>", RtbInsert.text
          changeTag RtbCiclaLivello, "<<ISRT-INSERT>>", RtbInsert.text
      End If
    
      '''''''''''''''
      ' OCCURS
      '''''''''''''''
      'purtroppo devo fare cos� a quest'ora....
      GbTestoRedOcc2 = ""
      If (Not flagSel) Or K = 1 Then
         Dim tbOcc As Recordset
'''MG240107
''''         Set tbOcc = m_dllFunctions.Open_Recordset("select *" & _
''''             " from dmdb2_tabelle as a, dmdb2_index as b" & _
''''             " where a.idtable = b.idtable and a.idtablejoin = " & idRed(K) & _
''''             " and a.TAG like '%OCCURS%' and b.tipo = 'P' order by a.nome")

         Set tbOcc = m_dllFunctions.Open_Recordset("select *" & _
             " from " & DB & "tabelle as a, " & DB & "index as b" & _
             " where a.idtable = b.idtable and a.idtablejoin = " & idRed(K) & _
             " and a.TAG like '%OCCURS%' and b.tipo = 'P' order by a.nome")
         If Not tbOcc.EOF Then
           Dim tabFound2 As Boolean
           Dim IdxTabOk2 As Integer
           tabFound2 = False
           For IdxTabOk2 = 1 To idxTabOkMax2
              If tabOk2(IdxTabOk2) = idRed(K) And istrOk2(IdxTabOk2) = "ISRT" Then
                 tabFound2 = True
                 Exit For
              End If
           Next
           If Not tabFound2 Then
              idxTabOkMax2 = idxTabOkMax2 + 1
              ReDim Preserve tabOk2(idxTabOkMax2)
              ReDim Preserve istrOk2(idxTabOkMax2)
              tabOk2(idxTabOkMax2) = idRed(K)
              istrOk2(idxTabOkMax2) = "ISRT"
           End If
           Dim flagOcc As Boolean
           flagOcc = False
           If Not tabFound2 Then
              GbTestoRedOcc2 = GbTestoRedOcc2 & costruisciOccursIns(idRed(K), indentRed, "ISRT", flagOcc, False, RtbOccurs, AppoOccurs)
           End If
         End If
      Else
        GbTestoRedOcc = GbTestoRedOcc & costruisciOccursIns(idRed(K), IIf(flagSel, indentRed + 3, indentRed), "ISRT", flagOcc, True, RtbOccurs, AppoOccurs)
      End If
      If Len(testoRed(K)) Then
        'SQ: WK-NAMETABLE valorizzato con la tabella ridefinita serve al chiamante;
        '    se ho OCCURS viene sovrascritto: lo riassegno
'''        GbTestoRedOcc = GbTestoRedOcc & Space(14) & "MOVE '" & nomeTRed(k) & "' TO WK-NAMETABLE" & vbCrLf
'''        GbTestoRedOcc = GbTestoRedOcc & Space(11) & "END-IF" & vbCrLf
      End If
      If (Not flagSel) Or K = 1 Then
         testo = testo & GbTestoRedOcc
         GbTestoRedOcc = GbTestoRedOcc2
      Else
  '''      GbTestoRedOcc = GbTestoRedOcc & Space(11) & "." & vbCrLf
        changeTag RtbRed, "<<ISRT-OCCURS>>", RtbOccurs.text
        changeTag RtbRedCopy, "<<ISRT-REDEFINE(i)>>", RtbRed.text
        GbTestoRedOcc = RtbRedCopy.text
      End If
    Next
    'SP 5/9 keyfeedback per ora non gestita nella insert (gestione diversa dalla lettura)
    '  ...
    '  End If
    If flagSel Then
'''      testo = testo & vbCrLf & _
'''        Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
'''        Space(6) & "*** REDEFINES" & vbCrLf & _
'''        Space(11) & "   MOVE LNKDB-DATA-AREA(" & Format(isrtLevel) & ") TO RD-" & nomeSRed(1) & vbCrLf & _
'''        Space(11) & "   MOVE " & Format(isrtLevel) & " TO WK-IND2" & vbCrLf & _
'''        Space(11) & "   PERFORM 9999-" & Replace(nomeTRed(1), "_", "-") & "-ISRT" & vbCrLf & _
'''        Space(11) & "END-IF" & vbCrLf & vbCrLf
      
      changeTag RtbDisp, "<AREA-NAME>", Format(isrtlevel)
      changeTag RtbDisp, "<SEG-NAME>", nomeSRed(1)
      changeTag RtbDisp, "<TABLE-NAME-PERFORM>", Replace(nomeTRed(1), "_", "-")
      
      changeTag RtbCiclaLivello, "<<ISRT-DISP>>", RtbDisp.text
      
      GbFileRedOcc = "PI" & Right(getAlias_tabelle(Int(idRed(1))), 6)
      changeTag RtbCiclica, "<NOME-INCLUDE-OCCURS>", GbFileRedOcc
    End If 'se corretto qui, accorpare con if sotto
    If (flagOcc Or tabFound2) And Not flagSel Then
'''      testo = testo & vbCrLf & _
'''        Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
'''        Space(6) & "*** OCCURS" & vbCrLf & _
'''        Space(11) & "   PERFORM 9999-" & Replace(nomeTRed(1), "_", "-") & "-ISRT" & vbCrLf & _
'''        Space(11) & "END-IF" & vbCrLf
     
        changeTag RtbCiclica, "<TABLE-NAME>", Replace(nomeTRed(1), "_", "-")
        GbFileRedOcc = "PI" & Right(getAlias_tabelle(Int(idRed(1))), 6) 'fare parametro per le nuove copy P: P[tipo-istr][ALIAS]
        changeTag RtbCiclica, "<NOME-INCLUDE-OCCURS>", GbFileRedOcc
                
        changeTag RtbCiclaLivello, "<<PISRT-OCCURS(i)>>", RtbCiclica.text
    ''    changeTag RtbPerform, "<<ISRT-LIVELLI(i)>>", RtbCiclaLivello.text
      
    End If
'''    testo = testo & vbCrLf & _
'''      Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
'''      Space(11) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf & _
'''      Space(11) & "   MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf & _
'''      Space(11) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf & _
'''      Space(11) & "END-IF" & vbCrLf & vbCrLf
     
    changeTag RtbPerform, "<<ISRT-LIVELLI(i)>>", RtbCiclaLivello.text
  
  Next
  
  cleanAllTags RtbPerform
  getISRT_DB2 = RtbPerform.text
  
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Apertura specifico template, sostituzione TAG, salvataggio file.
' -> verr� riempito da
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub initRoutinesModule_DB2(dbdName As String, RTBR As RichTextBox)
  Dim pos As Long
  Dim i As Integer, indice As Integer
  Dim copyName As String, testo As String, TestoCW As String, TestoCP As String
  Dim wIdDb As Long
  Dim rs As Recordset
  Dim wPath As String, wOpRt As String
  Dim wEnv As collection
  
  'DEFINISCE IL PERCORSO DOVE SALVARE TUTTE LE ROUTINE GENERATE
  If Len(RTBR.text) Then
    RTBR.SaveFile RTBR.fileName, 1
    RTBR.text = ""
  End If
   
  wPath = m_dllFunctions.FnPathPrj
  wOpRt = getOperazioneRoutine()
  
  'SQ - Costruirsi il nome... buttare via 'ste select
  If SwTp Then
   'SQ: togliere tutti i Trim(UCase) inutili...
    Select Case wOpRt
      Case "GN"
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpdb2GN.cbl"
      Case "GU"
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpdb2GU.cbl"
      Case "GP"
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpdb2GP.cbl"
      Case "UP"
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpdb2UP.cbl"
      Case "GS"
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\RoutBTGSAMSEQ.cbl"
      Case Else
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpUTIL.cbl"
      '    Exit Sub
    End Select
  Else
    Select Case wOpRt
      Case "GN"
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtdb2GN.cbl"
      Case "GU"
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtdb2GU.cbl"
      Case "GP"
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtdb2GP.cbl"
      Case "UP"
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtdb2UP.cbl"
      Case "GS"
          RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\RoutBTGSAMSEQ.cbl"
      Case Else
           RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtUTIL.cbl"
      '     Exit Sub
    End Select
  End If
 
  AggLog " --> Load Routine Template...", MaVSAMF_GenRout.RtLog
 
  ''''''''''''''''''''
  ' CHANGE TAG:
  ' Modificare!
  ''''''''''''''''''''
  pos = RTBR.find("#DATA_CREAZ#", 0)
  If pos > -1 Then
    RTBR.SelText = Format(Date, "DD MM YYYY") & "  "
  End If
  pos = RTBR.find("#LABEL_AUTHOR#", 0)
  If pos > -1 Then
    RTBR.SelText = m_dllFunctions.LABEL_AUTHOR
  End If
  pos = RTBR.find("#DATA_MODIF#", 0)
  If pos > -1 Then
    RTBR.SelText = Format(Date, "DD MM YYYY")
  End If
  pos = RTBR.find("#NOME_RTE#", 0)
  If pos > -1 Then
    If InStr(nomeroutine, "UTIL") Then
       RTBR.SelText = nomeroutine
    Else
       RTBR.SelText = nomeroutine & m_suffix
    End If
    pos = RTBR.find("#NOME_RTE#", 0)
    If pos > -1 Then
      If InStr(nomeroutine, "UTIL") Then
         RTBR.SelText = nomeroutine
      Else
         RTBR.SelText = nomeroutine & m_suffix
      End If
    End If
  End If
  pos = RTBR.find("#NOME_RTE#", 0)
  If pos > -1 Then
   'alpitour 5/8/2005 suffisso
   If InStr(nomeroutine, "UTIL") Then
     RTBR.SelText = nomeroutine
   Else
     RTBR.SelText = nomeroutine & m_suffix
   End If
   'tmp: fare changeTag!
   pos = RTBR.find("#NOME_RTE#", 0)
   If pos > -1 Then
     If InStr(nomeroutine, "UTIL") Then
        RTBR.SelText = nomeroutine
     Else
        RTBR.SelText = nomeroutine & m_suffix
     End If
   End If
  End If
  'non � che mi piaccia, ma sar� cambiato tutto insieme....
  pos = RTBR.find("#DB_DLI#", 0)
  If pos > -1 Then
    RTBR.SelText = dbdName
  End If
  pos = RTBR.find("#DB_DLI#", 0)
  If pos > -1 Then
    RTBR.SelText = dbdName
  End If

' GSAM: forse non � la cosa migliore, ma farei cos�
  pos = RTBR.find("#SEG-LEN#", 0)
  Set rs = m_dllFunctions.Open_Recordset("select maxlen from psdli_segmenti as a, bs_oggetti as b where a.idoggetto = b.idoggetto and b.tipo_DBD = 'GSA' and b.nome = '" & dbdName & "'")
  If Not rs.EOF Then
     If pos > 0 Then RTBR.SelText = rs!MaxLen
  End If
  rs.Close
  
  If wOpRt <> "UTIL" Then
    'SQ
    Set rs = m_dllFunctions.Open_Recordset("select IdOggetto from bs_oggetti where tipo = 'DBD' and nome = '" & dbdName & "'")
    If Not rs.EOF Then
      wIdDb = rs!idOggetto
    End If
    rs.Close
'''MG 240107
'''''    Set rs = m_dllFunctions.Open_Recordset("Select * from dmdb2_tabelle where tag not like '%DUMMY%' and idorigine in (select idsegmento from psdli_segmenti where idoggetto = " & wIdDb & ") UNION " & _
'''''                                           "Select * from dmdb2_tabelle where tag not like '%DUMMY%' and idorigine in (select idsegmento from Mgdli_segmenti where idoggetto = " & wIdDb & " AND Correzione='I')" & _
'''''                                           " order by nome")

    'SQ Controllo TAG non sembra sicuro! Deve prendere solo quelle provenienti dal DLI...
    Set rs = m_dllFunctions.Open_Recordset("Select * from " & DB & "tabelle where tag not like '%DUMMY%' and TAG <> 'AUTOMATIC_DB2ORC' and idorigine in (select idsegmento from psdli_segmenti where idoggetto = " & wIdDb & ") UNION " & _
                                           "Select * from " & DB & "tabelle where tag not like '%DUMMY%' and TAG <> 'AUTOMATIC_DB2ORC' and idorigine in (select idsegmento from Mgdli_segmenti where idoggetto = " & wIdDb & " AND Correzione='I')" & _
                                           " order by nome")
    If rs.RecordCount Then
      'K-COPY:
      Set wEnv = New collection
      wEnv.Add dbdName
      TestoCP = Space(5) & "EXEC SQL INCLUDE " & getEnvironmentParam_SQ(kCopyParam, "dbd", wEnv) & " END-EXEC." & vbCrLf & Space(11)
      TestoCW = "     "
      While Not rs.EOF
        If Not IsNull(rs!nome) Then
           'SQ - input per i getEnvironmentParam
           Dim rsSegmenti As ADODB.Recordset
           Set rsSegmenti = m_dllFunctions.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rs!IdOrigine)
           'SQ 23-05-06 (segm virt)
           If rsSegmenti.EOF Then
             Set rsSegmenti = m_dllFunctions.Open_Recordset("select * from MgDLI_Segmenti where IdSegmento = " & rs!IdOrigine)
           End If
           'SQ 16-05-06:
           'TestoCW = TestoCW & "EXEC SQL INCLUDE " & getEnvironmentParam(tCopyParam, "dbd", dbdName, rsSegmenti!nome) & " END-EXEC." & vbCrLf & Space(11)
           Set wEnv = New collection
           wEnv.Add rs
           wEnv.Add rsSegmenti
           'stefano: tabelle senza segmento (occurs)...
           If Not InStr(rs!Tag, "OCCURS") > 0 Then
             TestoCW = TestoCW & "EXEC SQL INCLUDE " & getEnvironmentParam_SQ(tCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf & Space(11)
           End If
           If DB = "DMDB2_" Then
             TestoCW = TestoCW & "EXEC SQL INCLUDE " & getEnvironmentParam_SQ(xCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf & Space(11)
           End If
           TestoCW = TestoCW & "EXEC SQL INCLUDE " & getEnvironmentParam_SQ(cCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf & Space(11)
           TestoCP = TestoCP & "EXEC SQL INCLUDE " & getEnvironmentParam_SQ(mCopyParam, "tabelle", wEnv) & " END-EXEC." & vbCrLf & Space(11)
           rsSegmenti.Close
         End If
         rs.MoveNext
       Wend
       rs.Close
    End If
'stefano: ancora...
    isGsam = wOpRt = "GS"
     
    pos = RTBR.find("*#COPY_WK_DB2#", 0)
    If pos > 0 Then RTBR.SelText = TestoCW
    pos = RTBR.find("*#COPY_PD_DB2#", 0)
    If pos > 0 Then RTBR.SelText = TestoCP
    pos = RTBR.find("*#REST_FDBKEY#", 0)
    If pos > 0 Then RTBR.SelText = TestoRFK(wIdDb, "L")
    pos = RTBR.find("*#SET_FDBKEY#", 0)
    If pos > 0 Then RTBR.SelText = TestoSFK(wIdDb, "L")
    'SP 5/9 non credo serva pi�
    'pos = RTBR.Find("*#SET_WKKEY#", 0)
    'If pos > 0 Then RTBR.SelText = TestoWKA(wIdDb)
    pos = RTBR.find("*#DEL-CHILD#", 0)
    If pos > 0 Then RTBR.SelText = TestoDelChildEx(RTBR)
  Else
   'UTIL : Non deve far niente solo caricare il template
  End If

'''MG 240107
  If DB = "DMDB2_" Then
    percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\" & IIf(SwTp, "CxRout\", "BtRout\")
  Else
    percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\oracle\" & IIf(SwTp, "CxRout\", "BtRout\")
  End If
  
  RTBR.SaveFile percorso & nomeroutine & IIf(InStr(nomeroutine, "UTIL"), "", m_suffix) & ".cbl", 1
  RTBR.LoadFile percorso & nomeroutine & IIf(InStr(nomeroutine, "UTIL"), "", m_suffix) & ".cbl"
  
End Sub
Public Sub initRoutinesModule_DLI(RTBR As RichTextBox, nDb As String)
    Dim wstr As String
    Dim pos As Long
    Dim percorso As String
    
    'carica Template routine
    Load_Template_Routine_DLI RTBR, SwTp
    
    'Modifica l'intestazione
    Insert_Intestazione_Routine RTBR, nomeroutine, nDb
    
    wstr = VSAM2Rdbms.drNomeDB
    pos = InStr(wstr, ".")
    If pos > 0 Then wstr = Mid$(wstr, 1, pos - 1)
    
    percorso = VSAM2Rdbms.drPathDef & "\" & wstr & "\output-prj\dli\"
    
     'alpitour: 5/8/2005 suffisso da gestire moooolto meglio
     ' RTBR.SaveFile Percorso & Trim(nomeroutine) & ".cbl", 1
     ' RTBR.LoadFile Percorso & Trim(nomeroutine) & ".cbl"
     If InStr(nomeroutine, "UTIL") Then
        RTBR.SaveFile percorso & Trim(nomeroutine) & ".cbl", 1
        RTBR.LoadFile percorso & Trim(nomeroutine) & ".cbl"
     Else
        RTBR.SaveFile percorso & Trim(nomeroutine) & m_suffix & ".cbl", 1
        RTBR.LoadFile percorso & Trim(nomeroutine) & m_suffix & ".cbl"
     End If
    
End Sub

Public Sub Load_Template_Routine_DLI(RTBR As RichTextBox, SwTp As Boolean)
    Dim wOpRt As String
    Dim wPath As String
    Dim A As Long
    
    If RTBR.text <> "" Then
       RTBR.SaveFile RTBR.fileName, 1
    End If
    
    wPath = m_dllFunctions.FnPathPrj
    
    If SwTp Then
       Select Case Trim(UCase(gbCurInstr))
            Case "GN", "GHN"
'stefanopul
          '      RTBR.LoadFile wpath & "\input-prj\routbs\routtpDLIGN.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpDLIGN.cbl"
            Case "GU", "GHU"
'stefanopul
         '       RTBR.LoadFile wpath & "\input-prj\routbs\routtpDLIGU.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpDLIGU.cbl"
            Case "UP", "ISRT", "DLET", "REPL"
'stefanopul
        '        RTBR.LoadFile wpath & "\input-prj\routbs\routtpDLIUP.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpDLIUP.cbl"
            Case "GP", "GNP"
'stefanopul
       '         RTBR.LoadFile wpath & "\input-prj\routbs\routtpDLIGP.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpDLIGP.cbl"
            Case "PCB", "TERM", "CHKP", "SYNC", "QRY"
'stefanopul
      '          RTBR.LoadFile wpath & "\input-prj\routbs\routtpUTIL.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpUTIL.cbl"
            Case Else
'stefanopul
     '           RTBR.LoadFile wpath & "\input-prj\routbs\routtpDLI.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routtpDLI.cbl"
       End Select
    Else
       Select Case Trim(UCase(gbCurInstr))
            Case "GN", "GHN"
'stefanopul
     '           RTBR.LoadFile wpath & "\input-prj\routbs\routbtDLIGN.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtDLIGN.cbl"
            Case "GU", "GHU"
'stefanopul
    '            RTBR.LoadFile wpath & "\input-prj\routbs\routbtDLIGU.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtDLIGU.cbl"
            'alpitour 5/8/2005; ahi
            Case "GP", "GNP", "GHNP"
'stefanopul
   '             RTBR.LoadFile wpath & "\input-prj\routbs\routbtDLIGP.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtDLIGP.cbl"
            Case "UP", "ISRT", "DLET", "REPL"
'stefanopul
  '              RTBR.LoadFile wpath & "\input-prj\routbs\routbtDLIUP.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtDLIUP.cbl"
            Case "PCB", "TERM", "CHKP", "SYNC", "QRY"
'stefanopul
 '               RTBR.LoadFile wpath & "\input-prj\routbs\routbtUTIL.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtUTIL.cbl"
            Case Else
'stefanopul
'                RTBR.LoadFile wpath & "\input-prj\routbs\routbtDLI.cbl"
                RTBR.LoadFile wPath & "\input-prj\Template\imsdb\standard\routines\routbtDLI.cbl"
       End Select
    End If

    AggLog " --> Load Routine Template...", MaVSAMF_GenRout.RtLog
End Sub

Public Sub Insert_Intestazione_Routine(RTBR As RichTextBox, nRoutine As String, nDb As String)
  Dim pos2 As Long, pos6 As Long, posFine As Long, posFine1 As Long, pos3 As Long
    
    'INSERISCE LE DATE DI CREAZIONE E DI MODIFICA
    pos2 = RTBR.find("DATA-CREAZ", 0)
    If pos2 > -1 Then
      RTBR.SelText = Format(Date, "DD MM YYYY")
    End If
    
    pos2 = RTBR.find("AUTHOR-RTE", 0)
    If pos2 > -1 Then
      RTBR.SelText = m_dllFunctions.LABEL_AUTHOR
    End If
    
    pos2 = RTBR.find("DATA-MODIF", 0)
    If pos2 > -1 Then
      RTBR.SelText = Format(Date, "DD MM YYYY")
    End If
    
    'INSERISCE IL NOME DELLA ROUTINE ALL'INIZIO
    pos6 = RTBR.find("NOME-RTE", 0)
    If pos6 > -1 Then
      RTBR.SelText = nRoutine & m_suffix
    End If
    
    posFine = RTBR.find("FINE-ROUTINES", 0)
    
    posFine1 = RTBR.find("NOME-RTE", 0)
    
    If posFine1 > -1 Then
      RTBR.SelText = nRoutine & m_suffix
    End If
    
    'INSERISCE IL NOME DEL DATABASE DLI
    
    pos3 = RTBR.find("DB-DLI", 0)
    If pos3 > -1 Then
      RTBR.SelText = nDb
    End If
End Sub

Public Function TipoRoutine(Stringa) As String
    
    Dim Tipo As String
    Dim NomeR As String
    Dim K As Integer
    
    Tipo = "G"   ' generalizzata
    
    If Mid(Stringa, Len(Stringa), 1) = "C" Then
      Tipo = "T"
    End If
    
    If Mid(Stringa, Len(Stringa), 1) = "B" Then
      Tipo = "B"
    End If
    
    TipoRoutine = Tipo
    
End Function
'Function TestoDelChildEx(rtb As RichTextBox, WidOgg) As String
Function TestoDelChildEx(Rtb As RichTextBox) As String
  Dim NomeDelRout As String, testo As String, wPath As String, percorso As String, keyValue As String
  
  Dim RtbDel As RichTextBox
  'SQ 31-08
  Dim tb1 As Recordset, tb2 As Recordset
  
  Dim Pos1 As Variant
  
  Dim K As Integer, k1 As Integer, k2 As Integer, k3 As Integer, Kstart As Integer
  Dim TABCorr() As PSB_N_A
  Dim SwCorr As Boolean
  Dim TestoWhere As String
  
  'SP corso
  TestoDelChildEx = ""
  Exit Function
   
  NomeDelRout = nomeroutine
  Mid$(NomeDelRout, 5, 2) = "DL"

  wPath = m_dllFunctions.FnPathPrj
 
  Set RtbDel = MaVSAMF_GenRout.RtbDel

  RtbDel.text = Rtb.text
  
  'SP: gestione dbd storici
  
  For K = 1 To UBound(suffissiStorici)
  
'stefanopippo: 17/08/2005: acc. ho capito, tirava dentro anche quelli obsoleti!
'e di conseguenza non li trovava nella dmdb2_tabelle
'anche qui c'� un bel giro, sicuramente si pu� fare tutto con una sola query, alla
'prossima...
'Set Tb1 = m_dllFunctions.Open_Recordset("select * from dmdb2_tabelle where idorigine in (select idsegmento from psdli_segmenti where idoggetto = " & WidOgg & ") order by idtable")

'''MG 240107
'''''    Set tb1 = m_dllFunctions.Open_Recordset("select a.nome as nome, a.idorigine as idorigine, a.idobjsource as idobjsource, a.idtable as idtable from dmdb2_tabelle as a, psdli_DBD as b, bs_oggetti as c" & _
'''''             " where b.DBD_name = '" & suffissiStorici(K) & "' and a.idobjsource = c.idoggetto and c.nome = b.DBD_name order by idtable")

    Set tb1 = m_dllFunctions.Open_Recordset("select a.nome as nome, a.idorigine as idorigine, a.idobjsource as idobjsource, a.idtable as idtable from " & DB & "tabelle as a, psdli_DBD as b, bs_oggetti as c" & _
             " where b.DBD_name = '" & suffissiStorici(K) & "' and a.idobjsource = c.idoggetto and c.nome = b.DBD_name order by idtable")
    If tb1.RecordCount > 0 Then
       If K = 1 Then
          testo = testo & Space(5) & "EVALUATE WK-NAMETABLE" & vbCrLf
       End If
       While Not tb1.EOF
          'WidOgg = Tb1!IdObjSource 'idOggetto DBD - serve agli storici...
          Kstart = 0
          k1 = 0
          ReDim TABCorr(k1)
          Set tb2 = m_dllFunctions.Open_Recordset("select * from psdli_segmenti where idsegmento = " & tb1!IdOrigine)
          TABCorr(k1).nome = tb2!nome
          SwCorr = True
          While SwCorr
            SwCorr = False
            k2 = UBound(TABCorr)
            For k3 = Kstart To k2
              'Set tb2 = m_dllFunctions.Open_Recordset("select * from psdli_segmenti where IdOggetto = " & WidOgg & " and parent = '" & TABCorr(k3).nome & "'")
              Set tb2 = m_dllFunctions.Open_Recordset("select * from psdli_segmenti " & _
                  "where IdOggetto = " & tb1!IdObjSource & " and parent = '" & TABCorr(k3).nome & _
                  "' and idsegmento not in (select idsegmento from mgdli_segmenti)")
              While Not tb2.EOF
                  k1 = k1 + 1
                  
                  ReDim Preserve TABCorr(k1)
                  
                  SwCorr = True
                  TABCorr(k1).Id = tb2!idSegmento
                  TABCorr(k1).nome = tb2!nome
                  tb2.MoveNext
              Wend
            Next k3
            Kstart = k3
          Wend
          
          If UBound(TABCorr) > 0 Then
             testo = testo & Space(13) & "WHEN '" & tb1!nome & "'" & vbCrLf
          Else
             testo = testo & Space(13) & "WHEN '" & tb1!nome & "'" & vbCrLf
             'SQ
             'testo = testo & Space(6) & "**************************************" & vbCrLf
             'testo = testo & Space(6) & "* This segment doesn't have children *" & vbCrLf
             'testo = testo & Space(6) & "**************************************" & vbCrLf
             testo = testo & Space(16) & "CONTINUE" & vbCrLf
          End If
          For k3 = UBound(TABCorr) To 1 Step -1
'''MG 240107
'''''            Set tb2 = m_dllFunctions.Open_Recordset("select * from dmdb2_tabelle where idorigine = " & TABCorr(k3).Id)

            Set tb2 = m_dllFunctions.Open_Recordset("select * from " & DB & "tabelle where idorigine = " & TABCorr(k3).Id)
       'stefanopippo: 17/08/2005: gestisce le tabelle gi� esistenti evitando
       'la delete;
            'If tb2.RecordCount > 0 Then
            If tb2.RecordCount > 0 And tb2!Tag <> "OLD" Then
                
  'SP: 3/9 : attenzione che in banca ci sono storici con pi� segmenti del base!!!!
  'quindi il salvataggio della chiave non si pu� fare per questi casi.
  'visto che i segmenti solo sugli storici dovrebbero essere solo sull'ultimo livello,
  'uso per la delchild direttamente la K01 del padre, senza la MOVE
  'modificata la chiamata alla costruisci_where_delchild per gestire questa nuova where
                testo = testo & Space(17) & "MOVE K01-" & Replace(getPrimaryKeyByIdTable(tb1!idTable, TABLE_CURRENT), "_", "-") & " TO K01-" & Replace(getPrimaryKeyByIdTable(tb2!idTable, TABLE_CURRENT), "_", "-") & vbCrLf
                'SQ 1-08-06
                'testo = testo & Space(17) & "EXEC SQL DELETE FROM " & Trim(m_author) & tb2!nome & vbCrLf
                ' Mauro 28-03-2007 : eliminazione CREATOR
                'testo = testo & Space(17) & "EXEC SQL DELETE FROM " & IIf(Len(tb2!creator & ""), "." & tb2!creator, "") & tb2!nome & vbCrLf
                testo = testo & Space(17) & "EXEC SQL DELETE FROM " & tb2!nome & vbCrLf
'                If m_dllFunctions.FnNomeDB <> "banca.mty" Or k = 1 Then
                If K = 1 Then
                   testo = testo & CostruisciWhere_DelChild(tb1!idTable, tb2!idTable)
                Else
                   keyValue = Replace(getPrimaryKeyByIdTable(tb1!idTable, TABLE_CURRENT), "_", "-")
                   testo = testo & CostruisciWhere_DelChild_BPER(tb1!idTable, tb2!idTable, keyValue)
                End If
                testo = testo & Space(17) & "END-EXEC" & vbCrLf
              'End If
            End If
          Next k3
          tb1.MoveNext
       Wend
       If K = UBound(suffissiStorici) Then
          testo = testo & Space(11) & "END-EVALUATE" & vbCrLf
       End If
      End If
         
      Pos1 = RtbDel.find("*#DEL-CHILD#", 0)
      If Pos1 > 0 Then RtbDel.SelText = testo
  Next
  testo = testo & Space(11) & "MOVE SQLCODE TO WK-SQLCODE."

TestoDelChildEx = testo

End Function
Public Function getPrimaryKeyByIdTable(wIdTable As Long, Optional ByVal wLevel As e_TableLevel) As String
    Dim rs As ADODB.Recordset
    Dim rsTb As ADODB.Recordset
    
    If wLevel = TABLE_CURRENT Then
'''MG 240107
'''''        Set rs = m_dllFunctions.Open_Recordset("Select * From DMDB2_Index Where IdTable = " & wIdTable & " And Tipo = 'P'")
        
        Set rs = m_dllFunctions.Open_Recordset("Select * From " & DB & "Index Where IdTable = " & wIdTable & " And Tipo = 'P'")
        
        If rs.RecordCount > 0 Then
            rs.MoveFirst
            
            getPrimaryKeyByIdTable = Trim(rs!nome)
        End If
        
        rs.Close
    End If
    
    If wLevel = TABLE_CHILD Then
'''MG 240107
'''''        Set rsTb = m_dllFunctions.Open_Recordset("Select distinct IdTable From DMDB2_Colonne Where IdTable <> " & wIdTable & " And IdTableJoin = " & wIdTable & " Order by IdTable")
        
        Set rsTb = m_dllFunctions.Open_Recordset("Select distinct IdTable From " & DB & "Colonne Where IdTable <> " & wIdTable & " And IdTableJoin = " & wIdTable & " Order by IdTable")

        
        If rsTb.RecordCount > 0 Then
'''MG 240107
'''''            Set rs = m_dllFunctions.Open_Recordset("Select * From DMDB2_Index Where IdTable = " & rsTb!idTable & " And Tipo = 'P'")

            Set rs = m_dllFunctions.Open_Recordset("Select * From " & DB & "ndex Where IdTable = " & rsTb!idTable & " And Tipo = 'P'")

            If rs.RecordCount > 0 Then
                rs.MoveFirst
                
                getPrimaryKeyByIdTable = Trim(rs!nome)
            End If
            
            rs.Close
        End If
        
        rsTb.Close
    End If
    
    If wLevel = TABLE_FATHER Then
'''''        Set rsTb = m_dllFunctions.Open_Recordset("Select distinct IdTableJoin From DMDB2_Colonne Where IdTable = " & wIdTable & " And IdTableJoin <> " & wIdTable & " Order by IdTableJoin")
        Set rsTb = m_dllFunctions.Open_Recordset("Select distinct IdTableJoin From " & DB & "Colonne Where IdTable = " & wIdTable & " And IdTableJoin <> " & wIdTable & " Order by IdTableJoin")
        
        If rsTb.RecordCount > 0 Then
'''''            Set rs = m_dllFunctions.Open_Recordset("Select * From DMDB2_Index Where IdTable = " & rsTb!idtablejoin & " And Tipo = 'P'")
            Set rs = m_dllFunctions.Open_Recordset("Select * From " & DB & "Index Where IdTable = " & rsTb!idtablejoin & " And Tipo = 'P'")
            
            
            If rs.RecordCount > 0 Then
                rs.MoveFirst
                
                getPrimaryKeyByIdTable = Trim(rs!nome)
            End If
            
            rs.Close
        End If
        
        rsTb.Close
    End If
End Function
Public Function CostruisciWhere_DelChild_BPER(IdTbPadre As Long, IdTbCorrente As Long, keyValue As String)
   Dim tb2 As ADODB.Recordset
   Dim TestoWhere As String
   Dim MaxLen As Long
   Dim k3 As Long
   
'''MG 240107
'''''  Set tb2 = m_dllFunctions.Open_Recordset("select a.nome,c.nome,a.tag,a.etichetta " & _
'''''            "from dmdb2_colonne as a,dmdb2_idxcol as b,dmdb2_index as c " & _
'''''            "where c.tipo='P' and c.idtable = " & IdTbCorrente & " AND " & _
'''''            "b.idIndex=c.idIndex AND a.idcolonna=b.idColonna AND " & _
'''''            "(a.idTablejoin = " & IdTbPadre & " or " & _
'''''            "a.idtablejoin in (select idtablejoin from dmdb2_colonne " & _
'''''            "where idtable = " & IdTbPadre & " and idtablejoin <> idtable)) " & _
'''''            "order by a.ordinale ")
  Set tb2 = m_dllFunctions.Open_Recordset("select a.nome,c.nome,a.tag,a.etichetta " & _
            "from " & DB & "colonne as a," & DB & "idxcol as b," & DB & "index as c " & _
            "where c.tipo='P' and c.idtable = " & IdTbCorrente & " AND " & _
            "b.idIndex=c.idIndex AND a.idcolonna=b.idColonna AND " & _
            "(a.idTablejoin = " & IdTbPadre & " or " & _
            "a.idtablejoin in (select idtablejoin from " & DB & "colonne " & _
            "where idtable = " & IdTbPadre & " and idtablejoin <> idtable)) " & _
            "order by a.ordinale ")
  Do While Not tb2.EOF
    k3 = k3 + 1
    If k3 = 1 Then
       TestoWhere = Space(20) & "WHERE" & Space(4)
    Else
       TestoWhere = TestoWhere & Space(25) & "AND "
    End If
    TestoWhere = TestoWhere & MettiUnderScore(tb2(0)) & " = " & vbCrLf
    'SP 3/9: dovrebbe sempre funzionare con la chiave del padre; da confrontare con
    'versioni precedenti su dbd senza storici (Agend e Aboud, per esempio
    If tb2!Tag = "RED" Then
       TestoWhere = TestoWhere & Space(29) & ":K01-" & Replace(keyValue, "_", "-") & "." & Replace(tb2(3), "_", "-") & vbCrLf
    Else
       TestoWhere = TestoWhere & Space(29) & ":K01-" & Replace(tb2(1), "_", "-") & "." & Replace(tb2(0), "_", "-") & vbCrLf
    End If
    
    tb2.MoveNext
    
  Loop
  CostruisciWhere_DelChild_BPER = TestoWhere
End Function
Public Function CostruisciWhere_DelChild(IdTbPadre As Long, IdTbCorrente As Long)
   Dim tb3 As ADODB.Recordset
   Dim tbIdx As ADODB.Recordset
   Dim tb2 As ADODB.Recordset
   Dim tb4 As ADODB.Recordset
   Dim TestoWhere As String
   Dim MaxLen As Long
   Dim k3 As Long
   
'''MG 240107
'''''  Set tb2 = m_dllFunctions.Open_Recordset("select a.nome,c.nome,a.tag,a.etichetta " & _
'''''            "from dmdb2_colonne as a,dmdb2_idxcol as b,dmdb2_index as c " & _
'''''            "where c.tipo='P' and c.idtable = " & IdTbCorrente & " AND " & _
'''''            "b.idIndex=c.idIndex AND a.idcolonna=b.idColonna AND " & _
'''''            "(a.idTablejoin = " & IdTbPadre & " or " & _
'''''            "a.idtablejoin in (select idtablejoin from dmdb2_colonne " & _
'''''            "where idtable = " & IdTbPadre & " and idtablejoin <> idtable)) " & _
'''''            "order by a.ordinale ")
  Set tb2 = m_dllFunctions.Open_Recordset("select a.nome,c.nome,a.tag,a.etichetta " & _
            "from " & DB & "colonne as a," & DB & "idxcol as b," & DB & "index as c " & _
            "where c.tipo='P' and c.idtable = " & IdTbCorrente & " AND " & _
            "b.idIndex=c.idIndex AND a.idcolonna=b.idColonna AND " & _
            "(a.idTablejoin = " & IdTbPadre & " or " & _
            "a.idtablejoin in (select idtablejoin from " & DB & "colonne " & _
            "where idtable = " & IdTbPadre & " and idtablejoin <> idtable)) " & _
            "order by a.ordinale ")

  Do While Not tb2.EOF
    k3 = k3 + 1
    If k3 = 1 Then
       TestoWhere = Space(20) & "WHERE" & Space(4)
    Else
       TestoWhere = TestoWhere & Space(25) & "AND "
    End If
    TestoWhere = TestoWhere & MettiUnderScore(tb2(0)) & " = " & vbCrLf
    If tb2!Tag = "RED" Then
       TestoWhere = TestoWhere & Space(29) & ":K01-" & Replace(tb2(1), "_", "-") & "." & Replace(tb2(3), "_", "-") & vbCrLf
    Else
       TestoWhere = TestoWhere & Space(29) & ":K01-" & Replace(tb2(1), "_", "-") & "." & Replace(tb2(0), "_", "-") & vbCrLf
    End If
    
    tb2.MoveNext
  Loop
  CostruisciWhere_DelChild = TestoWhere
End Function
Public Function MettiUnderScore(NomeCol) As String
  Dim K As Integer
  Dim testo As String
  
  testo = NomeCol
  
  K = InStr(testo, "-")
  While K > 0
    Mid$(testo, K, 1) = "_"
    K = InStr(testo, "-")
  Wend
  
  MettiUnderScore = testo
End Function
Public Function SostUnderScore(NomeCol) As String
  Dim K As Integer
  Dim testo As String
  
  testo = NomeCol
  
  K = InStr(testo, "_")
  While K > 0
    Mid$(testo, K, 1) = "-"
    K = InStr(testo, "_")
  Wend
  
  SostUnderScore = testo
End Function

Public Function SpezzaData(NomeCol, wIdSegm, wAlias) As String
  Dim K As Integer
  Dim testo As String
  Dim tb1 As Recordset
  Dim wColonna As String
    
  SwDigit = False
  wColonna = Trim(NomeCol)
  
  If wAlias = "FALSE" Then
     testo = wColonna
  Else
     testo = wAlias & "." & wColonna
  End If
  
  Set tb1 = m_dllFunctions.Open_Recordset("select * from dmcolonne where idsegm = " & wIdSegm & " and colonna = '" & wColonna & "' ")
  If tb1.RecordCount > 0 Then
     If UCase(tb1!Tipo) = "DATE" Then
        If wAlias = "FALSE" Then
           testo = "CHAR(" & wColonna & ") "
        Else
           testo = "CHAR(" & wAlias & "." & wColonna & ") "
        End If
     End If
     If UCase(tb1!Tipo) = "TIME" Then
        If wAlias = "FALSE" Then
           testo = "CAR(" & wColonna & ") "
        Else
           testo = "CHAR(" & wAlias & "." & wColonna & ") "
        End If
     End If
     If UCase(tb1!Tipo) = "INTEGER" Or _
        UCase(tb1!Tipo) = "SMALLINT" Or _
        UCase(tb1!Tipo) = "DECIMAL" Then
        SwDigit = True
        If wAlias = "FALSE" Then
           testo = "DIGITS(" & wColonna & ") "
        Else
           testo = "DIGITS(" & wAlias & "." & wColonna & ") "
        End If
     End If

  End If
  K = InStr(testo, "-")
  While K > 0
    Mid$(testo, K, 1) = "_"
    K = InStr(testo, "-")
  Wend
  K = InStr(testo, "#")
  While K > 0
    Mid$(testo, K, 1) = "-"
    K = InStr(testo, "#")
  Wend
  
  SpezzaData = testo
End Function

'SQ
Public Function TestoRFK(WidOgg, Tipo) As String
  Dim testo As String
  Dim rsTabelle As Recordset, rsIndex As Recordset, tb3 As Recordset, rsColonne As Recordset, rsColonneP As Recordset, Tb6 As Recordset, Segm As Recordset
  Dim NumSeg As Integer, NumLiv As Integer
  Dim wIdSegm As Variant
  Dim flag As Boolean
  'alpitour 5 / 8 / 2005
  Dim virt As Boolean

  ' bisogner� rigestire i GSAM to SQL..
  If isGsam Then
     TestoRFK = TestoRFKGSAM
     Exit Function
  End If
  'SQ TAG='AUTOMATIC'
'''MG 240107
'''''  Set rsTabelle = m_dllFunctions.Open_Recordset("select * from dmdb2_tabelle where idorigine in (select idsegmento from psdli_segmenti where idoggetto = " & WidOgg & ") and TAG='AUTOMATIC' order by nome")
  Set rsTabelle = m_dllFunctions.Open_Recordset("select * from " & DB & "tabelle where idorigine in (select idsegmento from psdli_segmenti where idoggetto = " & WidOgg & ") and TAG='AUTOMATIC' order by nome")
  If rsTabelle.RecordCount = 0 Then
  'alpitour: 5/8/2005 mooolto divertente; perch� non andate a giocare da un'altra parte?
     'TestoRFK = "Ma perch� non ci sono Segmenti per il DB?"
     TestoRFK = "             ."
     Exit Function
  End If
  On Error GoTo errRout
  
    'stefanopippo: per ora la tolgo, non dovrebbe mai succedere un errore di non trovato
    'nella restore; se capita, � meglio prendere un abend.
  testo = vbCrLf
'  While Not rsTabelle.EOF
'    virt = False
'    Set segm = m_dllFunctions.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!idorigine & "")
'    If segm.RecordCount > 0 Then
'       If segm!idsegmentoorigine <> 0 Then
'          virt = True
'       End If
'    End If
'    If Not virt Then
'        Set rsIndex = m_dllFunctions.Open_Recordset("select * from dmdb2_index where idtable = " & rsTabelle!idtable & " and tipo = 'P' ")
'        If rsIndex.RecordCount Then
'          'SQ: se ho acceduto per chiave secondaria?!
'          'stefanopippo: per ora recuperiamo sempre e solo la primaria, che salviamo
'          'comunque anche se abbiamo acceduto per chiave secondaria sul livello precedente
'          'alpitour: 5/8/2005 (per modo di dire: sono le 4 del mattino dopo)
'            testo = testo & Space(11) & "INITIALIZE KRR-" & Replace(rsIndex!nome, "_", "-") & vbCrLf
'            Set tb3 = m_dllFunctions.Open_Recordset("select * from dmdb2_idxcol where idindex = " & rsIndex!idIndex)
'            While Not tb3.EOF
'              Set rsColonne = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idcolonna = " & tb3!idColonna)
'              If rsColonne.RecordCount > 0 Then
'                If rsColonne!Tipo = "DATE" Then
'                'alpitour: 5/8/2005 SICURO??????
'                    testo = testo & Space(11) & "MOVE '0001-01-01' TO " & SostUnderScore(rsColonne!nome) & " OF KRR-" & Replace(rsIndex!nome, "_", "-") & vbCrLf
'                End If
'              End If
'              tb3.MoveNext
'            Wend
'        End If
'    End If
'    rsTabelle.MoveNext
'  Wend
  
'  rsTabelle.MoveFirst
  
  'SQ 12-08
  'testo = testo & Space(11) & "MOVE WK-NAMETABLE TO WK-SELTABLE" & vbCrLf
  'If Tipo = "L" Then
  '   testo = testo & Space(11) & "MOVE LNKDB-STKNAME(LNKDB-NUMSTACK) TO WK-NAMETABLE" & vbCrLf
  'End If
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' PORTARE TUTTO IN TEMPLATE...
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  testo = testo & Space(11) & "MOVE ZERO TO WK-IND" & vbCrLf & _
                  Space(11) & "MOVE ZERO TO WINDICE" & vbCrLf & _
                  Space(11) & "PERFORM UNTIL WK-IND > 49" & vbCrLf & _
                  Space(11) & "  ADD 1 TO WK-IND" & vbCrLf & _
                  Space(11) & "  IF LNKDB-NUMPCB = LNKDB-STKNUMPCB(WK-IND) AND" & vbCrLf & _
                  Space(11) & "     WK-NAMETABLE = LNKDB-STKNAME(WK-IND)" & vbCrLf & _
                  Space(11) & "     MOVE WK-IND TO WINDICE" & vbCrLf & _
                  Space(11) & "     MOVE 50 TO WK-IND" & vbCrLf & _
                  Space(11) & "  END-IF" & vbCrLf & _
                  Space(11) & "END-PERFORM" & vbCrLf
                  'EXIT non funzionante
'                  Space(11) & "IF WINDICE = ZERO" & vbCrLf & _
'                  Space(11) & "   EXIT" & vbCrLf & _
'                  Space(11) & "END-IF" & vbCrLf & vbCrLf
'                  Space (11) & "   GO TO REST-FDBKEY-EX" & vbCrLf & _
'                  Space(11) & "END-IF" & vbCrLf & vbCrLf
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'EXIT non funzionante
  testo = testo & Space(11) & "IF WINDICE > ZERO" & vbCrLf
  testo = testo & Space(14) & "EVALUATE WK-NAMETABLE" & vbCrLf
  
  While Not rsTabelle.EOF
    virt = False
    'SP redefines
    'Set Segm = m_dllFunctions.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & "")
    Set Segm = m_dllFunctions.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
    If Segm.RecordCount > 0 Then
       If Segm!IdSegmentoOrigine <> 0 Then
          virt = True
       End If
    End If
    If Not virt Then
'''MG 240107
'''''        Set rsIndex = m_dllFunctions.Open_Recordset("select * from dmdb2_index where idtable = " & rsTabelle!idTable & " and tipo = 'P' ")
        Set rsIndex = m_dllFunctions.Open_Recordset("select * from " & DB & "index where idtable = " & rsTabelle!idTable & " and tipo = 'P' ")

        testo = testo & Space(14) & "WHEN '" & rsTabelle!nome & "'" & vbCrLf
        If Tipo = "L" Then
      'sungard: solo se trovata chiave
            If rsIndex.RecordCount > 0 Then
               'SQ 12-08
               'testo = testo & Space(17) & "MOVE LNKDB-STKFDBKEY(LNKDB-NUMSTACK) " & vbCrLf
               testo = testo & Space(17) & "MOVE LNKDB-STKFDBKEY(WINDICE) " & vbCrLf
               testo = testo & Space(17) & "  TO KRR-" & Replace(rsIndex!nome, "_", "-") & " " & vbCrLf
            Else
               testo = testo & Space(18) & "CONTINUE" & vbCrLf
            End If
        End If
        'stefanopippo: tolto per usare la wkeywkey al suo posto
            
'        Set rsColonne = m_dllFunctions.Open_Recordset("SELECT * FROM DMDB2_COLONNE WHERE idtable = " & getIdTableChildren(rsTabelle!idtable) & " AND IDtablejoin <> " & rsTabelle!idtable)
'        While Not rsColonne.EOF
'          'SQ
'          If rsColonne!idcmp > 0 Then
'            Set rsColonneP = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idCmp = " & rsColonne!idcmp & " and IdTable = " & rsTabelle!idtable)
'            While Not rsColonneP.EOF
'              Set tb3 = m_dllFunctions.Open_Recordset("select * from dmdb2_index where idtable = " & getIdTableChildren(rsColonneP!idtable) & " and tipo = 'P'")
'                If tb3.RecordCount > 0 Then
'                  'SQ - solo commento; non messo in linea perche' dovrebbe andare bene cosi'...
'                  'If m_dllFunctions.FnNomeDB = "banca.mty" Then
'                  '  testo = testo & Space(17) & "MOVE " & Left(rsTabelle!nome, 2) & "N" & Mid(rsTabelle!nome, 10) & "-" & Replace(rsColonne!nome, "_", "-") & vbCrLf & _
'                  '                  Space(18) & "  TO " & Replace(rsColonne!nome, "_", "-") & " OF KRR-" & tb3!nome & " " & vbCrLf
'                  '  Else
'                      testo = testo & Space(17) & "MOVE " & SostUnderScore(Trim(rsColonneP!nome)) & " OF KRR-" & Replace(rsIndex!nome, "_", "-") & " TO " & vbCrLf
'                      testo = testo & Space(17) & "     " & SostUnderScore(Trim(rsColonne!nome)) & " OF KRR-" & SostUnderScore(Trim(tb3!nome)) & vbCrLf
'                  '  End If
'                  End If
'              rsColonneP.MoveNext
'            Wend
'          End If
'          rsColonne.MoveNext
'        Wend
           
'        Set rsColonne = m_dllFunctions.Open_Recordset("SELECT * FROM DMdb2_COLONNE WHERE idtablejoin = " & rsTabelle!idtable & " AND IDtablejoin <> idtable")
'        While Not rsColonne.EOF
'          'alpitour: 5/8/3005 purtroppo devo fare cos� per rimandare la gestione delle redefines
'          'anche senza controlli, non ho tempo; so anche che era meglio farlo fuori ciclo, ormai....
'          Dim segmred As Recordset, tbred As Recordset
'          Set tbred = m_dllFunctions.Open_Recordset("select * from DMDB2_Tabelle where idtable = " & rsColonne!idtable & "")
'          Set segmred = m_dllFunctions.Open_Recordset("select * from psdli_segmenti where idsegmento = " & tbred!idorigine & "")
'          If segmred.RecordCount > 0 Then
'             If segmred!idsegmentoorigine = 0 Then
''alpitour: 5/8/2005 piccola correzione, grosso risultato!!!!
'          'Set rsColonneP = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idCmp = " & rsColonne!idCmp & " and IdTable = " & rsColonne!idtable)
'               Set rsColonneP = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idCmp = " & rsColonne!idcmp & " and IdTable = " & rsColonne!idtablejoin)
'               If rsColonneP.RecordCount > 0 Then
'                  Set tb3 = m_dllFunctions.Open_Recordset("select * from dmdb2_index where idtable = " & rsColonne!idtable & " and tipo = 'P'")
'                  testo = testo & Space(17) & "MOVE " & SostUnderScore(Trim(rsColonneP!nome)) & " OF KRR-" & Replace(rsIndex!nome, "_", "-") & " TO " & vbCrLf
'                  testo = testo & Space(17) & "     " & SostUnderScore(Trim(rsColonne!nome)) & " OF KRR-" & SostUnderScore(Trim(tb3!nome)) & vbCrLf
'               End If
'            End If
'          End If
'          rsColonne.MoveNext
'        Wend
    End If
    rsTabelle.MoveNext
  Wend
    
  testo = testo & Space(14) & "END-EVALUATE" & vbCrLf
    'EXIT non funzionante
  TestoRFK = testo & Space(11) & "END-IF."
  
  Exit Function
errRout:
  MsgBox "TestoRFK - resume next... " & vbCrLf & Err.Description
  Resume Next
End Function

Public Function TestoRFKGSAM() As String
  Dim testo As String
  testo = vbCrLf
  testo = testo & Space(11) & "MOVE ZERO TO WK-IND" & vbCrLf & _
                  Space(11) & "MOVE ZERO TO WINDICE" & vbCrLf & _
                  Space(11) & "PERFORM UNTIL WK-IND > 49" & vbCrLf & _
                  Space(11) & "  ADD 1 TO WK-IND" & vbCrLf & _
                  Space(11) & "  IF LNKDB-NUMPCB = LNKDB-STKNUMPCB(WK-IND)" & vbCrLf & _
                  Space(11) & "     MOVE WK-IND TO WINDICE" & vbCrLf & _
                  Space(11) & "     MOVE 50 TO WK-IND" & vbCrLf & _
                  Space(11) & "  END-IF" & vbCrLf & _
                  Space(11) & "END-PERFORM" & vbCrLf
  testo = testo & Space(11) & "IF WINDICE > ZERO" & vbCrLf
  testo = testo & Space(14) & "MOVE LNKDB-STKFDBKEYGSAM(WINDICE) " & vbCrLf
  testo = testo & Space(14) & "  TO WK-COUNT-XRST" & vbCrLf
    
  TestoRFKGSAM = testo & Space(11) & "END-IF."
  
End Function
Public Function getIdTableChildren(wIdTable As Long) As Long
    Dim rs As ADODB.Recordset
    
'''MG 240107
'''''    Set rs = m_dllFunctions.Open_Recordset("Select Distinct IdTable From DMDB2_Colonne Where IdTableJoin = " & wIdTable & " And IdTable <> " & wIdTable)
    Set rs = m_dllFunctions.Open_Recordset("Select Distinct IdTable From " & DB & "Colonne Where IdTableJoin = " & wIdTable & " And IdTable <> " & wIdTable)
    
    If rs.RecordCount > 0 Then
        rs.MoveFirst
        
        If rs.RecordCount > 1 Then
            'Stop
        End If
        
        getIdTableChildren = rs!idTable
    End If
    
    rs.Close
End Function
'SQ
Public Function TestoSFK(WidOgg, Tipo) As String
  Dim testo As String
  'alpitour: 5/8/2005
  Dim rsTabelle As Recordset, rsColonne As Recordset, tb3 As Recordset, Segm As Recordset
  
  ' bisogner� rigestire i GSAM to SQL..
  If isGsam Then
     TestoSFK = TestoSFKGSAM
     Exit Function
  End If
  'SQ TAG='AUTOMATIC'
'''MG 240107
'''''  Set rsTabelle = m_dllFunctions.Open_Recordset("select * from dmdb2_tabelle where idorigine in (select idsegmento from psdli_segmenti where idoggetto = " & WidOgg & ") and TAG='AUTOMATIC' order by nome")
  Set rsTabelle = m_dllFunctions.Open_Recordset("select * from " & DB & "tabelle where idorigine in (select idsegmento from psdli_segmenti where idoggetto = " & WidOgg & ") and TAG='AUTOMATIC' order by nome")
  
  If rsTabelle.RecordCount = 0 Then
  'alpitour: 5/8/2005 mooolto divertente; perch� non andate a giocare da un'altra parte?
     'TestoSFK = "Ma perch� non ci sono Segmenti per il DB?"
     TestoSFK = "             ."
     Exit Function
  End If

  If Tipo = "L" Then
    testo = Space(5) & "MOVE WK-NAMETABLE TO LNKDB-STKNAME(LNKDB-NUMSTACK) " & vbCrLf & _
            Space(11) & "EVALUATE WK-NAMETABLE" & vbCrLf
  Else
    testo = Space(5) & "EVALUATE WK-NAMETABLE" & vbCrLf
  End If
  While Not rsTabelle.EOF
  'alpitour: 5/8/2005
    Dim virt As Boolean
    virt = False
    'SP redefines
    'Set Segm = m_dllFunctions.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & "")
    Set Segm = m_dllFunctions.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & "")
    If Segm.RecordCount > 0 Then
       If Segm!IdSegmentoOrigine <> 0 Then
          virt = True
       End If
    End If
    If Not virt Then
        testo = testo & Space(14) & "WHEN '" & rsTabelle!nome & "'" & vbCrLf
'''MG 240107
'''''        Set tb3 = m_dllFunctions.Open_Recordset("Select * from dmdb2_index where idtable = " & rsTabelle!idTable & " and tipo = 'P' ")
        Set tb3 = m_dllFunctions.Open_Recordset("Select * from " & DB & "index where idtable = " & rsTabelle!idTable & " and tipo = 'P' ")

        'stefanopippo: tolta qui ed effettuata SEMPRE la adlwkey, che serve allo stesso scopo
'        Set rsColonne = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idtable = " & rsTabelle!idtable & " and idcolonna in (select idcolonna from dmdb2_idxcol where idindex in (select idindex from dmdb2_index where tipo = 'P' ) ) ORDER BY idcolonna")
'        While Not rsColonne.EOF
'          'SQ
'          If m_dllFunctions.FnNomeDB = "banca.mty" Then
'            testo = testo & Space(18) & "MOVE " & Left(rsTabelle!nome, 2) & "N" & Mid(rsTabelle!nome, 10) & "-" & Replace(rsColonne!nome, "_", "-") & vbCrLf & _
'                            Space(18) & "  TO " & Replace(rsColonne!nome, "_", "-") & " OF KRR-" & tb3!nome & " " & vbCrLf
'            'nomeArea = Left(rsTabelle!nome, 2) & "N" & Mid(rsTabelle!nome, 10)
'            'nomeCampo = nomeArea & "-" & Replace(rsColonne!nome, "_", "-")
'          Else
'            testo = testo & Space(18) & "MOVE " & SostUnderScore(Trim(rsColonne!nome)) & " OF " & "RR-" & rsTabelle!nome & vbCrLf & _
'                            Space(18) & "  TO " & SostUnderScore(Trim(rsColonne!nome)) & " OF KRR-" & SostUnderScore(Trim(tb3!nome)) & " " & vbCrLf
'          End If
'          rsColonne.MoveNext
'        Wend
        If Tipo = "L" Then
           'sungard: solo se trovata chiave
          If tb3.RecordCount > 0 Then
             testo = testo & Space(17) & "MOVE KRR-" & SostUnderScore(Trim(tb3!nome)) & " " & vbCrLf
             testo = testo & Space(17) & "  TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
             testo = testo & Space(17) & "MOVE LENGTH OF KRR-" & SostUnderScore(Trim(tb3!nome)) & " " & vbCrLf
             testo = testo & Space(17) & "  TO LNKDB-STKKEYLEN(LNKDB-NUMSTACK) " & vbCrLf
             'SP 5/9 keyfeedback
             'SQ: KRDP??????
             'If m_dllFunctions.FnNomeDB = "banca.mty" Then
             '   'testo = testo & Space(11) & "MOVE W-" & Left(TabIstr.BlkIstr(k1).KeyDef(1).NameAlt, 3) & "-KEY" & Right(TabIstr.BlkIstr(k1).KeyDef(1).NameAlt, 2) & _
             '   '    " TO LNKDB-FDBKEY" & vbCrLf
             'Else
             '   testo = testo & Space(17) & "MOVE KRDP-" & SostUnderScore(Trim(tb3!nome)) & _
             '       " TO LNKDB-PCBFDBKEY" & vbCrLf
             'End If
          Else
             testo = testo & Space(17) & "CONTINUE" & vbCrLf
          End If
        End If
     End If
     rsTabelle.MoveNext
  Wend
  testo = testo & Space(11) & "END-EVALUATE." & vbCrLf

  TestoSFK = testo
End Function
Public Function TestoSFKGSAM() As String
  Dim testo As String
  testo = vbCrLf
  testo = testo & Space(11) & "MOVE WK-COUNT-XRST " & vbCrLf
  testo = testo & Space(11) & "  TO LNKDB-STKFDBKEYGSAM(WINDICE)." & vbCrLf
  TestoSFKGSAM = testo
End Function

'Public Function TestoWKA(WidOgg) As String
'Dim testo As String
'Dim Tb1 As Recordset
'Dim Tb2 As Recordset
'
'' Stop
'm_dllFunctions.WriteLog "TestoWKA", "DR - GENROUT"
'testo = ""
'Set Tb1 = m_dllFunctions.Open_Recordset("select * from dmtavole where idsegm in (select idsegm from segmenti where idoggetto = " & WidOgg & ") order by idsegm")
'If Tb1.RecordCount = 0 Then
'   TestoWKA = "Ma perch� non ci sono Segmenti per il DB?"
'   Exit Function
'End If
'
'Tb1.MoveFirst
'
'testo = Space(8) & "EVALUATE WK-DATASET" & vbCrLf
'
'While Not Tb1.EOF
'   testo = testo & Space(16) & "WHEN '" & Tb1!nome & "'" & vbCrLf
'   Set Tb2 = m_dllFunctions.Open_Recordset("select * from dmcolonne where idsegm = " & Tb1!IdSegm & " and pkey > 0 ORDER BY PKEY")
'   If Tb2.RecordCount > 0 Then
'      Tb2.MoveFirst
'      While Not Tb2.EOF
'         testo = testo & Space(18) & "MOVE " & Tb2!colonna & " OF ADL-" & Tb1!nome & vbCrLf
'         testo = testo & Space(18) & "  TO " & Tb2!colonna & " OF KRR-" & Tb1!nome & " " & vbCrLf
'         Tb2.MoveNext
'      Wend
'   End If
'   Tb1.MoveNext
'Wend
'
'testo = testo & Space(14) & "END-EVALUATE." & vbCrLf
'
'
'TestoWKA = testo
'End Function

'SQ ?
'Public Function createRoutine_DLI(rtbroutine As RichTextBox, NewIstr As String, sOpe As String) As Boolean
Public Function createRoutine_DLI(rtbroutine As RichTextBox, NewIstr As String) As Boolean
    Dim tb As Recordset
    Dim tb1 As Recordset
    Dim tb2 As Recordset
    Dim TbKey As Recordset
    Dim IndI As Integer
    Dim K As Integer
    Dim k1 As Integer
    Dim k2 As Integer
    Dim k3 As Integer
    Dim vEnd As Double
    Dim vPos As Double
    Dim vPos1 As Double
    Dim vStart As Double
    Dim wstr As String
    Dim rst As Recordset
    Dim xrout As String
    Dim TbPrg As Recordset
    Dim sCurIdTable As Long

    wstr = VSAM2Rdbms.drNomeDB
    k1 = InStr(wstr, ".")
    If k1 > 0 Then wstr = Mid$(wstr, 1, k1 - 1)
    
    If InStr(rtbroutine.fileName, nomeroutine) = 0 Then
      rtbroutine.SaveFile rtbroutine.fileName, 1
      rtbroutine.LoadFile VSAM2Rdbms.drPathDb & "\" & wstr & "\output-prj\dli\" & nomeroutine & m_suffix & ".cbl"
    End If

    ReDim TabIstr.BlkIstr(0)
    
    Set rst = m_dllFunctions.Open_Recordset("select * from psdli_istruzioni where idoggetto = " & mIdOggettoCorrente _
            & " and riga = " & mRiga _
            & " order by riga")

    Set tb = m_dllFunctions.Open_Recordset("select * from Psdli_IstrDett_Liv where idoggetto = " & mIdOggettoCorrente _
            & " and riga = " & mRiga _
            & " order by  livello")
    
    sDbd = m_dllFunctions.getDBDByIdOggettoRiga(rst!idOggetto, rst!riga)
    sPsb = m_dllFunctions.getPSBByIdOggettoRiga(rst!idOggetto, rst!riga)
    
    If UBound(sDbd) > 0 Then
      TabIstr.dbd = Trim(Restituisci_NomeOgg_Da_IdOggetto(sDbd(1)))
    Else
      TabIstr.dbd = "[NO DBD WAS ASSOCIATED]"
    End If
    
    TabIstr.Istr = wIstrCod(totIstr).Istr
    'ginevra:keyfeedback
    If Trim(rst!keyfeedback) <> "" Then
      TabIstr.keyfeedback = Trim(rst!keyfeedback)
    Else
      TabIstr.keyfeedback = ""
    End If
    TabIstr.procSeq = wIstrCod(totIstr).procSeq
    TabIstr.procOpt = wIstrCod(totIstr).procOpt
    TabIstr.isGsam = wIstrCod(totIstr).isGsam
    If UBound(sPsb) > 0 Then
      TabIstr.PsbId = sPsb(1)
    Else
      TabIstr.PsbId = -1
    End If

    Set tb1 = m_dllFunctions.Open_Recordset("select * from bs_oggetti where idoggetto = " & TabIstr.PsbId)
    If tb1.RecordCount > 0 Then
      TabIstr.PSBName = tb1!nome
    End If
    TabIstr.PCB = Trim(rst!NumPCB & " ")
    tb1.Close
    If tb.RecordCount > 0 Then
    tb.MoveFirst
    IndI = 0
    
    While Not tb.EOF
      IndI = IndI + 1
       ReDim Preserve TabIstr.BlkIstr(IndI)
       k3 = 0
       ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
       
       TabIstr.BlkIstr(IndI).ssaName = Trim(tb!NomeSSA & "")
      'SQ 8-05-06
      'MODIFICATA STRUTTURA TabIstr in Funzioni: usare quella anche qui!!!
      'In particolare: qualificazione � un boolean...
      'TMP: pezza
      'TabIstr.BlkIstr(IndI).Parentesi = Trim(tb!qualificazione)
      TabIstr.BlkIstr(IndI).Parentesi = IIf(tb!Qualificazione, "(", " ")
       
       TabIstr.BlkIstr(IndI).SegLen = tb!SegLen
       TabIstr.BlkIstr(IndI).Search = tb!condizione
       TabIstr.BlkIstr(IndI).NomeRout = nomeroutine

'       If m_dllFunctions.FnNomeDB = "banca.mty" Then
'
'          If tb!stridoggetto > 0 Then
'             Set Tb1 = m_dllFunctions.Open_Recordset("select * from PsData_Area where IdOggetto = " & tb!stridoggetto & " And IdArea = " & tb!Stridarea)
'             If Tb1.RecordCount > 0 Then
'               TabIstr.BlkIstr(IndI).Record = Tb1!nome
'             End If
'          End If
'       Else
'carica direttamente da liv
          TabIstr.BlkIstr(IndI).Record = tb!dataarea
'       End If
       
       Set tb1 = m_dllFunctions.Open_Recordset("select * from psdli_segmenti where idsegmento = " & tb!idSegmento)
       If tb1.RecordCount > 0 Then
          TabIstr.BlkIstr(IndI).Segment = tb1!nome
'alpitour: 5/8/2005: la tolgo, ma a che serviva la comprtn?? probabilmente a qualcosa
'che abbiamo fatto in altro modo
         ' If UCase(Trim(Tb1!comprtn)) <> "NONE" Then
        '     TabIstr.BlkIstr(IndI).NewNameSeg = Tb1!comprtn
        '  Else
             TabIstr.BlkIstr(IndI).NewNameSeg = tb1!nome
        '  End If
          TabIstr.BlkIstr(IndI).IdSegm = tb!idSegmento
          
'''MG 240107
'''''          Set tb1 = m_dllFunctions.Open_Recordset("select * from DMdb2_Tabelle where idorigine = " & tb!idSegmento)
          
          Set tb1 = m_dllFunctions.Open_Recordset("select * from " & DB & "Tabelle where idorigine = " & tb!idSegmento)

          If tb1.RecordCount > 0 Then
             TabIstr.BlkIstr(IndI).Tavola = tb1!nome
             
             'Stafano:
             sCurIdTable = tb1!idTable
'''MG 240107
'''''             Set tb1 = m_dllFunctions.Open_Recordset("select * from dmdb2_index where tipo = 'P' and idtable = " & tb1!idTable)
             Set tb1 = m_dllFunctions.Open_Recordset("select * from " & DB & "index where tipo = 'P' and idtable = " & tb1!idTable)
             
             
             If tb1.RecordCount > 0 Then
               TabIstr.BlkIstr(IndI).pKey = tb1!nome
             End If
          Else
             'Prende il DL1 : CONTROLLARE
             Set tb1 = m_dllFunctions.Open_Recordset("select * from PSDLI_Field where IdSegmento = " & tb!idSegmento & " And Primario = True")
             
             If tb1.RecordCount > 0 Then
               TabIstr.BlkIstr(IndI).pKey = tb1!nome
             End If
          End If
       End If
       ' stefano!!!!!!!!!!!1
       If tb!idSegmento = -1 Then
          TabIstr.BlkIstr(IndI).IdSegm = tb!idSegmento
       End If
       tb1.Close
       
       Dim tbIdx As New ADODB.Recordset
       
       Set tb2 = m_dllFunctions.Open_Recordset("select * from psdli_istrdett_key where idoggetto = " & rst!idOggetto _
                & " and riga = " & tb!riga _
                & " and livello = " & tb!Livello _
                & " order by riga")
       
       If tb2.RecordCount > 0 Then
          tb2.MoveFirst
          k3 = 0
          
          While Not tb2.EOF
             k3 = k3 + 1
             ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
             TabIstr.BlkIstr(IndI).KeyDef(k3).Op = m_dllFunctions.TrattaOp(Trim(tb2!Operatore & " "))
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal = Trim(tb2!Valore & " ")
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = Trim(tb2!KeyLen)  ' "" ?
             TabIstr.BlkIstr(IndI).KeyDef(k3).xName = tb2!xName
             
             If Val(tb2!IdField) > 0 Then
                Set tb1 = m_dllFunctions.Open_Recordset("select * from psdli_field where idfield = " & Val(tb2!IdField & " And idOggetto = " & rst!idOggetto))
             Else
                Set tb1 = m_dllFunctions.Open_Recordset("select * from psdli_XDfield where idXDfield = " & Abs(Val(tb2!IdField)))
             End If
             
             If tb1.RecordCount > 0 Then
'''MG 240107
'''''                Set TbKey = m_dllFunctions.Open_Recordset("select * from dmdb2_index where Nome = '" & TabIstr.BlkIstr(IndI).pKey & "' ")
                Set TbKey = m_dllFunctions.Open_Recordset("select * from " & DB & "index where Nome = '" & TabIstr.BlkIstr(IndI).pKey & "' ")
                
                
                If TbKey.RecordCount > 0 Then
                     TabIstr.BlkIstr(IndI).KeyDef(k3).key = TbKey!nome
                     TabIstr.BlkIstr(IndI).KeyDef(k3).NameAlt = Trim(tb1!nome)
                Else
                  'Prende il DL1 : CONTROLLARE
                  TabIstr.BlkIstr(IndI).KeyDef(k3).key = tb1!nome
                  TabIstr.BlkIstr(IndI).KeyDef(k3).NameAlt = Trim(tb1!nome)
                End If
             Else
                k3 = k3
             End If
                 
         
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyBool = tb2!booleano
             TabIstr.BlkIstr(IndI).KeyDef(k3).KeyNum = CarNumKey(IndI)
    
             tb2.MoveNext
          Wend
          
          tb2.Close
       End If
       
       tb.MoveNext
     Wend
    End If

    SwTp = False
    'if not Isalw
    Set tb = m_dllFunctions.Open_Recordset("select * from bs_oggetti where idoggetto = " & rst!idOggetto)
    If tb.RecordCount > 0 Then
       SwTp = tb!Cics
    End If
    
    createRoutine_DLI = True
    
    If rst!valida Then
       AggRoutDLI rst, rtbroutine, NewIstr
    End If
End Function

Public Function createRoutine_DB2(rtbroutine As RichTextBox, NewIstr As String) As Boolean
  Dim tb As Recordset, tb1 As Recordset, tb2 As Recordset, rst As Recordset
  Dim IndI As Integer, K As Integer, k1 As Integer, k2 As Integer, k3 As Integer
  Dim vEnd As Double, vPos As Double, vStart As Double
  Dim prjName As String
  Dim sCurIdTable As Long

  'stefano!!!!!!!!!!!!!!!!!!
  'da impostare nelle treeview l'indice k della wistrcod: NON SO COME FARE,
  'ma velocizzerebbe; forse � sempre lo stesso indice dell'operazione,
  'quindi basta un contatore globale, a meno di duplicazioni sulla treeview
  'provo in questo modo, altrimenti bisogna ripristinare il ciclo
  '  indWistr = 0
  '  For k = 1 To UBound(wIstrCod)
  '     If NewIstr = wIstrCod(k).Istruzione Then
  '       indWistr = k
  '       Exit For
  '     End If
  '  Next k
  
  totIstr = totIstr + 1

  prjName = Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1)
  
  'Preparazione RTB:
  If Not EmbeddedRoutine Then
    If InStr(rtbroutine.fileName, nomeroutine) = 0 Then
      'Modulo nuovo: salvo il precedente e ricarico il template
      rtbroutine.SaveFile rtbroutine.fileName, 1
      If Left(nomeroutine, 1) = "T" Then
        If Mid(nomeroutine, 3, 4) = "UTIL" Then
           rtbroutine.LoadFile VSAM2Rdbms.drPathDb & "\" & prjName & "\output-prj\db2\cxrout\" & nomeroutine & ".cbl"
        Else
           rtbroutine.LoadFile VSAM2Rdbms.drPathDb & "\" & prjName & "\output-prj\db2\cxrout\" & nomeroutine & m_suffix & ".cbl"
        End If
      ElseIf Left(nomeroutine, 1) = "P" Then
        If Mid(nomeroutine, 3, 4) = "UTIL" Then
          rtbroutine.LoadFile VSAM2Rdbms.drPathDb & "\" & prjName & "\output-prj\db2\cxrout\" & nomeroutine & ".cbl"
        Else
            If DB = "DMDB2_" Then
                rtbroutine.LoadFile VSAM2Rdbms.drPathDb & "\" & prjName & "\output-prj\db2\btrout\" & nomeroutine & m_suffix & ".cbl"
            Else
                rtbroutine.LoadFile VSAM2Rdbms.drPathDb & "\" & prjName & "\output-prj\ORACLE\btrout\" & nomeroutine & m_suffix & ".cbl"
            End If
        End If
      Else
        'SQ - EMBEDDED
        If Not EmbeddedRoutine Then
        'stefano: default = batch, anche perch� la ricarca sul nome � totalmente falsa, valeva solo per Alpitour!!
          'MsgBox "createRoutine_DB2: cics o batch???"
          If Mid(nomeroutine, 3, 4) = "UTIL" Then
            rtbroutine.LoadFile VSAM2Rdbms.drPathDb & "\" & prjName & "\output-prj\db2\cxrout\" & nomeroutine & ".cbl"
          Else
              If DB = "DMDB2_" Then
                  rtbroutine.LoadFile VSAM2Rdbms.drPathDb & "\" & prjName & "\output-prj\db2\btrout\" & nomeroutine & m_suffix & ".cbl"
              Else
                  rtbroutine.LoadFile VSAM2Rdbms.drPathDb & "\" & prjName & "\output-prj\ORACLE\btrout\" & nomeroutine & m_suffix & ".cbl"
              End If
          End If
        End If
      End If
    Else
      'Modulo gi� iniziato... routine da appendere alla rtb
      DoEvents
    End If
  End If
  
  'stefano: da sostituire con wistrcod: iniziamo a risparmiare letture
  sDbd = m_dllFunctions.getDBDByIdOggettoRiga(mIdOggettoCorrente, mRiga)
  sPsb = m_dllFunctions.getPSBByIdOggettoRiga(mIdOggettoCorrente, mRiga)
  
  ReDim TabIstr.BlkIstr(0)
  If UBound(sDbd) > 0 Then
    mIdDBDCorrente = sDbd(1)
    TabIstr.dbd = Restituisci_NomeOgg_Da_IdOggetto(mIdDBDCorrente)
  End If
  
  If UBound(sPsb) > 0 Then
    TabIstr.PsbId = sPsb(1)
  End If
  TabIstr.Istr = wIstrCod(totIstr).Istr
  TabIstr.procSeq = wIstrCod(totIstr).procSeq
  TabIstr.procOpt = wIstrCod(totIstr).procOpt
  TabIstr.isGsam = wIstrCod(totIstr).isGsam
  
  Set tb1 = m_dllFunctions.Open_Recordset("select * from bs_oggetti where idoggetto = " & TabIstr.PsbId)
  If tb1.RecordCount > 0 Then
    TabIstr.PSBName = tb1!nome
  End If
  tb1.Close
  
  'SQ CPY-ISTR
  Set tb = m_dllFunctions.Open_Recordset( _
    "select * from Psdli_IstrDett_Liv where idPgm=" & idpgm & " AND idoggetto = " & mIdOggettoCorrente & " and riga = " & mRiga & " order by livello")
  While Not tb.EOF
    IndI = IndI + 1
    k3 = 0
    ReDim Preserve TabIstr.BlkIstr(IndI)
    ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
   
    TabIstr.BlkIstr(IndI).ssaName = tb!NomeSSA & ""
    'SQ 8-05-06
    'MODIFICATA STRUTTURA TabIstr in Funzioni: usare quella anche qui!!!
    'In particolare: qualificazione � un boolean...
    'TMP: pezza
    'TabIstr.BlkIstr(IndI).Parentesi = Trim(tb!qualificazione)
    TabIstr.BlkIstr(IndI).Parentesi = IIf(tb!Qualificazione, "(", " ")
    TabIstr.BlkIstr(IndI).SegLen = tb!SegLen
    'A cosa serve la .Search?
    TabIstr.BlkIstr(IndI).Search = tb!condizione & ""  'per i null
    TabIstr.BlkIstr(IndI).NomeRout = nomeroutine
    
    If tb!strIdOggetto > 0 Then  'N.B. Pu� essere null: senza ">0" darebbe errore!
      Set tb1 = m_dllFunctions.Open_Recordset("select * from PsData_Area where IdOggetto = " & tb!strIdOggetto & " And IdArea = " & tb!Stridarea)
      If tb1.RecordCount > 0 Then
         TabIstr.BlkIstr(IndI).Record = tb1!nome
      End If
      tb1.Close
    End If
    Set tb1 = m_dllFunctions.Open_Recordset("select * from PsDli_Segmenti where idsegmento = " & tb!idSegmento)
    If tb1.RecordCount Then
      TabIstr.BlkIstr(IndI).Segment = tb1!nome
      TabIstr.BlkIstr(IndI).NewNameSeg = tb1!nome 'SQ ???????
      TabIstr.BlkIstr(IndI).IdSegm = tb!idSegmento
      ' Mauro 02-04-2007 Inserimento campi LParent
      TabIstr.BlkIstr(IndI).LParent = tb1!LParent
      TabIstr.BlkIstr(IndI).LDBD = tb1!LDBD
      'SQ TAG='AUTOMATIC'
  '    Set Tb1 = m_dllFunctions.Open_Recordset("select * from DMdb2_Tabelle where idorigine = " & tb!idSegmento & " and TAG='AUTOMATIC'")
'''MG 240107
'''''      Set tb1 = m_dllFunctions.Open_Recordset("select * from DMdb2_Tabelle where idorigine = " & tb!idSegmento & " and TAG not like '%DUMMY%' and TAG not like '%OCCURS%'")
      Set tb1 = m_dllFunctions.Open_Recordset("select * from " & DB & "Tabelle where idorigine = " & tb!idSegmento & " and TAG not like '%DUMMY%' and TAG not like '%OCCURS%'")

      If tb1.RecordCount Then
        TabIstr.BlkIstr(IndI).Tavola = tb1!nome
        TabIstr.BlkIstr(IndI).idTable = tb1!idTable
        'SQ 28-07-06
        TabIstr.BlkIstr(IndI).tableCreator = tb1!creator & ""
        sCurIdTable = tb1!idTable
'''MG 240107
'''''        Set tb1 = m_dllFunctions.Open_Recordset("select * from dmdb2_index where tipo = 'P' and idtable = " & tb1!idTable)
        Set tb1 = m_dllFunctions.Open_Recordset("select * from " & DB & "index where tipo = 'P' and idtable = " & tb1!idTable)
        
        If tb1.RecordCount > 0 Then
          TabIstr.BlkIstr(IndI).pKey = tb1!nome
        End If
      End If
    End If
    tb1.Close

    'molteplicit� sulla squalificata
    'stefano: pezza su pezza
    'SQ - La decodifica pu� avere meno livelli rispetto all'istruzione originale
    '     Es.: REPL/DLET hanno significativo solo l'ultimo livello
    If IndI <= UBound(wIstrCod(totIstr).Liv) Then
      'SQ 27-07-06
      TabIstr.BlkIstr(IndI).CodCom = wIstrCod(totIstr).Liv(IndI).Ccode
      If Len(wIstrCod(totIstr).Liv(IndI).Segm) Then
        If UBound(wIstrCod(totIstr).Liv(IndI).Chiave) > 0 Then
          'SQ CPY-ISTR
          Set tb2 = m_dllFunctions.Open_Recordset( _
            "SELECT * FROM psdli_istrdett_key WHERE idPgm=" & idpgm & " AND idoggetto = " & mIdOggettoCorrente & _
            " AND riga = " & tb!riga & " and livello = " & tb!Livello & " order by riga")
          If tb2.RecordCount > 0 Then
            k3 = 0
            While Not tb2.EOF
              k3 = k3 + 1
              ReDim Preserve TabIstr.BlkIstr(IndI).KeyDef(k3)
              'molteplicit�: per ora sostituisco solo quelle gestite (operatore, istruzione,
              'qualificazione, anche se su wistrcod ho tutto
               TabIstr.BlkIstr(IndI).KeyDef(k3).Op = getOperator(wIstrCod(totIstr).Liv(IndI).Chiave(k3).Oper & "")
               TabIstr.BlkIstr(IndI).KeyDef(k3).KeyVal = tb2!Valore & ""
               TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen = tb2!KeyLen  ' "" ?
               TabIstr.BlkIstr(IndI).KeyDef(k3).KeyStart = tb2!KeyStart & ""
               TabIstr.BlkIstr(IndI).KeyDef(k3).KeyField = TabIstr.BlkIstr(IndI).ssaName & "(" & TabIstr.BlkIstr(IndI).KeyDef(k3).KeyStart & ":" & TabIstr.BlkIstr(IndI).KeyDef(k3).KeyLen & ")"
               TabIstr.BlkIstr(IndI).KeyDef(k3).xName = tb2!xName
               
               'OLD: NameAlt: nome DLI / key: nome DB2 primary key (ce l'ho gi� in Pkey!!!)
               'NEW: NameAlt: nome DLI / key: nome DB2 CORRISPONDENTE!  (P.S.: a qualcuno serve il nome DLI?)
               If tb2!IdField > 0 Then
                'FIELD
      '          Set Tb1 = m_dllFunctions.Open_Recordset( _
      '            "select dli.nome as dliName,db2.nome as db2Name from PsDli_Field as dli, DMDB2_Index as db2 " & _
      '            "where idField = " & Tb2!IdField & " and dli.idField=db2.idOrigine")
'''MG 240107
'''''                Set tb1 = m_dllFunctions.Open_Recordset( _
'''''                  "select dli.nome as dliName,db2.nome as db2Name from PsDli_Field as dli, DMDB2_Index as db2 " & _
'''''                  "where idField = " & tb2!IdField & " and dli.idField=db2.idOrigine" & _
'''''                  " and db2.idtable = " & TabIstr.BlkIstr(IndI).idTable)
                Set tb1 = m_dllFunctions.Open_Recordset( _
                  "select dli.nome as dliName,db2.nome as db2Name from PsDli_Field as dli, " & DB & "Index as db2 " & _
                  "where idField = " & tb2!IdField & " and dli.idField=db2.idOrigine" & _
                  " and db2.idtable = " & TabIstr.BlkIstr(IndI).idTable)
                Else
                 'XDFIELD
'''MG 240107
'''''                 Set tb1 = m_dllFunctions.Open_Recordset( _
'''''                   "select dli.nome as dliName,db2.nome as db2Name from PsDli_XDfield as dli, DMDB2_Index as db2 " & _
'''''                   "where dli.idXDfield = " & Abs(tb2!IdField) & " and dli.idXDField=abs(db2.idOrigine)" & _
'''''                   " and db2.idtable = " & TabIstr.BlkIstr(IndI).idTable)
                 Set tb1 = m_dllFunctions.Open_Recordset( _
                   "select dli.nome as dliName,db2.nome as db2Name from PsDli_XDfield as dli, " & DB & "Index as db2 " & _
                   "where dli.idXDfield = " & Abs(tb2!IdField) & " and dli.idXDField=abs(db2.idOrigine)" & _
                   " and db2.idtable = " & TabIstr.BlkIstr(IndI).idTable)
                End If
                If tb1.RecordCount Then
                  TabIstr.BlkIstr(IndI).KeyDef(k3).key = tb1!db2Name
                  TabIstr.BlkIstr(IndI).KeyDef(k3).NameAlt = tb1!dliName
                Else
                  'Gestire...
                  m_dllFunctions.WriteLog "createRoutine_DB2 - No field/index found for idField: " & tb2!IdField
                End If
           
                TabIstr.BlkIstr(IndI).KeyDef(k3).KeyBool = tb2!booleano
                TabIstr.BlkIstr(IndI).KeyDef(k3).KeyNum = CarNumKey(IndI)
    
                tb2.MoveNext
              Wend
              tb2.Close
            End If
          End If
        End If
      End If
      tb.MoveNext
  Wend
  tb.Close
  
  Set tb = m_dllFunctions.Open_Recordset("select * from bs_oggetti where idoggetto = " & mIdOggettoCorrente)
  If tb.RecordCount > 0 Then
    SwTp = tb!Cics
  Else
    SwTp = False
  End If
  tb.Close
  
  'SQ - Aggiungere il PCB alla struttura wistrcod!
  'SQ - CPY-ISTR
  Set rst = m_dllFunctions.Open_Recordset( _
    "SELECT NumPCB,ordPCB FROM psdli_istruzioni WHERE idPgm=" & idpgm & " AND idoggetto = " & mIdOggettoCorrente & " and riga = " & mRiga & " order by riga")
  TabIstr.PCB = Trim(rst!NumPCB & "")
  TabIstr.ordPcb = rst!ordPcb
  rst.Close
  
  If EmbeddedRoutine Then
    getRoutine_DB2_SQ rtbroutine, NewIstr
  Else
    getRoutine_DB2 rtbroutine, NewIstr
  End If
    
End Function

Public Function getRoutine_DB2(RTRoutine As RichTextBox, NewIstr As String) As Boolean
  Dim K As Integer, pXistr As Integer
  Dim IndIstr As Double
  Dim wIstr As String, wIstruzione As String, xIstruzione As String, nIstruzione As String
  Dim vPos As Double, wfine As Double
  Dim testo As String, NumPCB As String
  Dim SwInsIstr As Boolean
  Dim tb1 As Recordset, tb2 As Recordset
  Dim wIdDb As Variant, wIdSeg As Variant
  Dim wXistr As String
  Dim TbPrg As Recordset
  Dim testo1 As String
  
  'SQ - cosa cambia fra wIstr e wIstruzione???
  'Quante variabili uguali abbiamo??
  wIstr = wIstrCod(totIstr).Istr
  wIstruzione = NewIstr
  xIstruzione = wIstrCod(totIstr).Decodifica
  'SQ - buttare via nIstruzione
  Gistruzione = Trim(wIstr) & Mid(wIstruzione, 5)
  GbNumIstr = Mid(wIstruzione, 5)
  
  GbTestoFetch = ""
  GbTestoRedOcc = ""
  
  vPos = RTRoutine.find("#CODE#", 1)
  vPos = RTRoutine.find(vbCrLf, vPos + 1)
  vPos = vPos + 2
  testo = vbCrLf & "      *  " & Left(wIstruzione & Space(4), 10) & " - "
  testo = testo & DecIstr(xIstruzione) & vbCrLf
  RTRoutine.SelStart = vPos
  RTRoutine.SelLength = 0
  RTRoutine.SelText = testo
  
  vPos = RTRoutine.find("#OPE#", 1)
  vPos = RTRoutine.find(vbCrLf, vPos + 1) + 2
  
  testo = Space(11) & "IF LNKDB-OPERATION = '" & wIstruzione & "' " & vbCrLf & _
          Space(11) & "   PERFORM 9999-ROUT-" & Gistruzione & vbCrLf & _
          Space(11) & "   PERFORM 2000-MAIN-001" & vbCrLf & _
          Space(11) & "   PERFORM 3000-MAIN-END" & vbCrLf & _
          Space(11) & "END-IF" & vbCrLf
  
  RTRoutine.SelStart = vPos
  RTRoutine.SelLength = 0
  RTRoutine.SelText = testo
  
  wfine = Len(RTRoutine.text)
  
  BlkIni = 1
  BlkFin = UBound(TabIstr.BlkIstr)
  BlkStep = 1
  
  ''''''''''''''''''''
  ' INIZIO ROUTINE
  ''''''''''''''''''''
  testo = vbCrLf & "      *------------------------------------------------------------*" & vbCrLf & _
                   "      *  Call  :      " & wIstruzione & "  " & vbCrLf & _
                   "      *  Coding:      " & DecIstr(xIstruzione) & vbCrLf & _
                   "      *------------------------------------------------------------*" & vbCrLf
  testo1 = "       9999-ROUT-" & Gistruzione & " SECTION." & vbCrLf
  'SQ D
  comment = testo
  ReDim GbFileRedOccs(0)
    
  Select Case wIstr
    Case "DLET"
      testo = getDLET_DB2()
    Case "ISRT"
      If TabIstr.isGsam Then
        testo = getISRT_GSAM
      Else
        testo = getISRT_DB2(RTRoutine)
      End If
    Case "REPL"
      If TabIstr.BlkIstr(1).idTable Then
        testo = getREPL_DB2(RTRoutine)
      Else
        'm_dllFunctions.WriteLog "Error on istruction: " & TabIstr.Istr & " - riga: " &  & " - IdOggetto: " &
        m_dllFunctions.WriteLog "Error on istruction: " & TabIstr.Istr & " - routine: " & TabIstr.BlkIstr(1).NomeRout
        AggLog "Error on istruction: " & TabIstr.Istr & " - routine: " & TabIstr.BlkIstr(1).NomeRout, MaVSAMF_GenRout.RTBErr2
      End If
    Case "GU"
      testo = testo & testo1 & getGU_DB2(RTRoutine, "GU")
      'testo = testo & Space(11) & "." & vbCrLf
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        testo = testo & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
      End If
    Case "GHU"
      testo = testo & testo1 & getGU_DB2(RTRoutine, "GU", True)
      testo = testo & Space(11) & "." & vbCrLf
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        testo = testo & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
      End If
    Case "GN"
      If TabIstr.isGsam Then
          testo = getGN_GSAM
      Else
      '4-10-06: COMPLETAMENTE SQUALIFICATE
        If UBound(TabIstr.BlkIstr) Then
          'stefano: il procseq ora sar� gestito direttamente nella costruisci_where visto
          ' che dobbiamo fare comunque la ricerca per maggiore della chiave precedente.
          'tmp
          'testo = testo & getGU_DB2_old(RTRoutine, "GN")
          testo = testo & testo1 & getGN_DB2(RTRoutine, "GN")
        Else
          testo = testo & testo1 & getGNsq_DB2(RTRoutine, "GN")
        End If
        testo = testo & Space(11) & "." & vbCrLf
        'SQ D
        If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
          testo = testo & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
        End If
      End If
    Case "GHN"
      '4-10-06: COMPLETAMENTE SQUALIFICATE
      If UBound(TabIstr.BlkIstr) Then
        'tmp
        testo = testo & testo1 & getGN_DB2(RTRoutine, "GN", True)
      Else
        'tmp
        testo = testo & testo1 & getGNsq_DB2(RTRoutine, "GN", True)
      End If
      testo = testo & Space(11) & "." & vbCrLf
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        testo = testo & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
      End If
    Case "GNP"
      testo = testo & testo1 & getGNP_DB2(RTRoutine, "GNP")
      testo = testo & Space(11) & "." & vbCrLf
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        testo = testo & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
      End If
    Case "GHNP"
      testo = testo & testo1 & getGNP_DB2(RTRoutine, "GNP", True)
      testo = testo & Space(11) & "." & vbCrLf
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        testo = testo & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
      End If
    Case "TERM", "PCB", "QRY", "SYNC"
      testo = testo & testo1 & getUTIL_DB2(Trim(UCase(wIstr)))
      testo = testo & Space(11) & "." & vbCrLf
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        testo = testo & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
      End If
    Case "CLSE"
       testo = getCLSE_GSAM
    Case "OPEN"
       testo = getOPEN_GSAM
  End Select
  testo = testo & Space(11) & "." & vbCrLf
  
  If Len(GbFileRedOccs(0)) Then
    For K = 0 To UBound(GbFileRedOccs)
      If Len(GbFileRedOccs(K)) Then
        testo = testo & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOccs(K) & " END-EXEC." & vbCrLf
      End If
    Next
  End If
         
  '''''''''''''''''''''''''''''''''
  ' Inserimento Routine
  '''''''''''''''''''''''''''''''''
  RTRoutine.SelStart = wfine
  RTRoutine.SelLength = 0
  RTRoutine.SelText = testo
  
  ''''''''''''''''''''''''''''''''''
  ' SQ 16-11-06 - EMBEDDED
  ' TMP
  ''''''''''''''''''''''''''''''''''
  Dim routineText As String
  routineText = testo
  testo = RTRoutine.text  'backup
  RTRoutine.text = routineText
' stefano: per ora lo commento; va rimesso un po' meglio per l'embedded definitivo
' DA RESTORARE!!!!!!
'  RTRoutine.SaveFile Percorso & wIstruzione, 1
  RTRoutine.text = testo  'restore
  
  '''''''''''''''''''''''''''''''''
  ' Inserimento Fetch
  '''''''''''''''''''''''''''''''''
  If Len(Trim(GbTestoFetch)) Then
    wfine = Len(RTRoutine.text)
    RTRoutine.SelLength = 0
    RTRoutine.SelStart = wfine
    RTRoutine.SelText = vbCrLf & GbTestoFetch
  End If
   
  'SQ D 4-08-06
  'Portare dentro le routine o... cambiare tutto!
  'Ora sono cicliche... devono ripulire la GbTestoRedOcc
  ' -> da allineare ISRT e DLET
  If Len(GbTestoRedOcc) Then
    testo = RTRoutine.text  'backup
    RTRoutine.text = GbTestoRedOcc
      Dim pos As Integer
    'buttare via Pos
    RTRoutine.SaveFile VSAM2Rdbms.drPathDb & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\cpy\" & GbFileRedOcc, 1
    RTRoutine.text = testo  'restore
    GbTestoRedOcc = ""
  End If
  GbFileRedOcc = ""  'Attenzione a brasare, questo fa da flag...
    
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ TMP:
' GESTIONE ROUTINE EMBEDDED: DIVENTERA' L'UNICO... SOLO PER NON SPACIUGARE...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getRoutine_DB2_SQ(RTRoutine As RichTextBox, NewIstr As String) As Boolean
  Dim K As Integer
  Dim wIstr As String, wIstruzione As String, xIstruzione As String
  Dim vPos As Double, wfine As Double
  Dim testo As String, routineText As String
  
  'SQ - cosa cambia fra wIstr e wIstruzione???
  'Quante variabili uguali abbiamo??
  wIstr = wIstrCod(totIstr).Istr
  wIstruzione = NewIstr
  xIstruzione = wIstrCod(totIstr).Decodifica
  'SQ - buttare via nIstruzione
  Gistruzione = Trim(wIstr) & Mid(wIstruzione, 5)
  GbNumIstr = Mid(wIstruzione, 5)
  
  GbTestoFetch = ""
  GbTestoRedOcc = ""
  
  vPos = RTRoutine.find("#CODE#", 1)
  vPos = RTRoutine.find(vbCrLf, vPos + 1)
  vPos = vPos + 2
  testo = vbCrLf & "      *  " & Left(wIstruzione & Space(4), 10) & " - "
  testo = testo & DecIstr(xIstruzione) & vbCrLf
  RTRoutine.SelStart = vPos
  RTRoutine.SelLength = 0
  RTRoutine.SelText = testo
  
  vPos = RTRoutine.find("#OPE#", 1)
  vPos = RTRoutine.find(vbCrLf, vPos + 1) + 2
  
  testo = Space(11) & "IF LNKDB-OPERATION = '" & wIstruzione & "' " & vbCrLf & _
          Space(11) & "   PERFORM 9999-ROUT-" & Gistruzione & vbCrLf & _
          Space(11) & "   PERFORM 2000-MAIN-001" & vbCrLf & _
          Space(11) & "   PERFORM 3000-MAIN-END" & vbCrLf & _
          Space(11) & "END-IF" & vbCrLf
  
  RTRoutine.SelStart = vPos
  RTRoutine.SelLength = 0
  RTRoutine.SelText = testo
  
  wfine = Len(RTRoutine.text)
  
  BlkIni = 1
  BlkFin = UBound(TabIstr.BlkIstr)
  BlkStep = 1
  
  ''''''''''''''''''''
  ' INIZIO ROUTINE
  ''''''''''''''''''''
  comment = vbCrLf & "      *------------------------------------------------------------*" & vbCrLf & _
                   "      *  Call  :      " & wIstruzione & "  " & vbCrLf & _
                   "      *  Coding:      " & DecIstr(xIstruzione) & vbCrLf & _
                   "      *------------------------------------------------------------*" & vbCrLf
  'rob                 "       9999-ROUT-" & Gistruzione & " SECTION." & vbCrLf
  'SQ D
  'routineText = comment
  ReDim GbFileRedOccs(0)
  
  Select Case wIstr
    Case "DLET"
      routineText = getDLET_DB2()           'TEMPLATE
    Case "ISRT"
      routineText = getISRT_DB2(RTRoutine)  'TEMPLATE
    Case "REPL"
      routineText = getREPL_DB2(RTRoutine)  'TEMPLATE
    Case "GU"
      routineText = comment & "       9999-ROUT-" & Gistruzione & "." & vbCrLf & routineText 'Da buttare con i template
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-CNIT-AREAS" & vbCrLf
      End If
      routineText = routineText & getGU_DB2(RTRoutine, "GU")
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        routineText = routineText & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC" & vbCrLf
      End If
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-PREPARE-EXIT" & vbCrLf
      End If
      routineText = routineText & Space(11) & "." & vbCrLf
      routineText = routineText & "       9999-ROUT-" & Gistruzione & "-EX." & vbCrLf 'Da buttare con i template
      routineText = routineText & Space(11) & "EXIT." 'Da buttare con i template
    Case "GHU"
      routineText = comment & "       9999-ROUT-" & Gistruzione & "." & vbCrLf & routineText 'Da buttare con i template
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-CNIT-AREAS" & vbCrLf
      End If
      routineText = routineText & getGU_DB2(RTRoutine, "GU", True)
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        routineText = routineText & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
      End If
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-PREPARE-EXIT" & vbCrLf
      End If
      routineText = routineText & Space(11) & "." & vbCrLf
      routineText = routineText & "       9999-ROUT-" & Gistruzione & "-EX." & vbCrLf 'Da buttare con i template
      routineText = routineText & Space(11) & "EXIT." 'Da buttare con i template
    Case "GN"
      routineText = comment & "       9999-ROUT-" & Gistruzione & "." & vbCrLf & routineText 'Da buttare con i template
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-CNIT-AREAS" & vbCrLf
      End If
      '4-10-06: COMPLETAMENTE SQUALIFICATE
      If UBound(TabIstr.BlkIstr) Then
        'stefano: il procseq ora sar� gestito direttamente nella costruisci_where visto
        ' che dobbiamo fare comunque la ricerca per maggiore della chiave precedente.
        'tmp
        'routineText = routineText & getGU_DB2_old(RTRoutine, "GN")
        routineText = routineText & getGN_DB2(RTRoutine, "GN")
      Else
        routineText = routineText & getGNsq_DB2(RTRoutine, "GN")
      End If
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        routineText = routineText & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
      End If
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-PREPARE-EXIT" & vbCrLf
      End If
      routineText = routineText & Space(11) & "." & vbCrLf
      routineText = routineText & "       9999-ROUT-" & Gistruzione & "-EX." & vbCrLf 'Da buttare con i template
      routineText = routineText & Space(11) & "EXIT." 'Da buttare con i template
    Case "GHN"
      routineText = comment & "       9999-ROUT-" & Gistruzione & "." & vbCrLf & routineText 'Da buttare con i template
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-CNIT-AREAS" & vbCrLf
      End If
      '4-10-06: COMPLETAMENTE SQUALIFICATE
      If UBound(TabIstr.BlkIstr) Then
        'tmp
        routineText = routineText & getGN_DB2(RTRoutine, "GN", True)
      Else
        'tmp
        routineText = routineText & getGNsq_DB2(RTRoutine, "GN", True)
      End If
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        routineText = routineText & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC" & vbCrLf
      End If
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-PREPARE-EXIT" & vbCrLf
      End If
      routineText = routineText & Space(11) & "." & vbCrLf
      routineText = routineText & "       9999-ROUT-" & Gistruzione & "-EX." & vbCrLf 'Da buttare con i template
      routineText = routineText & Space(11) & "EXIT." 'Da buttare con i template
    Case "GNP"
      routineText = comment & "       9999-ROUT-" & Gistruzione & "." & vbCrLf & routineText 'Da buttare con i template
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-CNIT-AREAS" & vbCrLf
      End If
      routineText = routineText & getGNP_DB2(RTRoutine, "GNP")
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        routineText = routineText & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC" & vbCrLf
      End If
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-PREPARE-EXIT" & vbCrLf
      End If
      routineText = routineText & Space(11) & "." & vbCrLf
      routineText = routineText & "       9999-ROUT-" & Gistruzione & "-EX." & vbCrLf 'Da buttare con i template
      routineText = routineText & Space(11) & "EXIT." 'Da buttare con i template
    Case "GHNP"
      routineText = comment & "       9999-ROUT-" & Gistruzione & "." & vbCrLf & routineText 'Da buttare con i template
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-CNIT-AREAS" & vbCrLf
      End If
      routineText = routineText & getGNP_DB2(RTRoutine, "GNP", True)
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        routineText = routineText & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
      End If
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-PREPARE-EXIT" & vbCrLf
      End If
      routineText = routineText & Space(11) & "." & vbCrLf
      routineText = routineText & "       9999-ROUT-" & Gistruzione & "-EX." & vbCrLf 'Da buttare con i template
      routineText = routineText & Space(11) & "EXIT." 'Da buttare con i template
    Case "TERM", "PCB", "QRY", "SYNC"
      routineText = comment & "       9999-ROUT-" & Gistruzione & "." & vbCrLf & routineText 'Da buttare con i template
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-CNIT-AREAS" & vbCrLf
      End If
      routineText = routineText & getUTIL_DB2(Trim(UCase(wIstr)))
      'SQ D
      If Len(GbFileRedOcc) Then 'Transitorio: buttare! serve solo per le routine che non utilizzano il GbFileRedOccs!
        routineText = routineText & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOcc & " END-EXEC." & vbCrLf
      End If
      If EmbeddedRoutine Then
        routineText = routineText & Space(11) & "PERFORM BPHX-PREPARE-EXIT" & vbCrLf
      End If
      routineText = routineText & Space(11) & "." & vbCrLf
      routineText = routineText & "       9999-ROUT-" & Gistruzione & "-EX." & vbCrLf 'Da buttare con i template
      routineText = routineText & Space(11) & "EXIT." 'Da buttare con i template
  End Select
  'routineText = routineText & Space(11) & "." & vbCrLf
  
  If Len(GbFileRedOccs(0)) Then
    For K = 0 To UBound(GbFileRedOccs)
      If Len(GbFileRedOccs(K)) Then
        routineText = routineText & Space(11) & "EXEC SQL INCLUDE " & GbFileRedOccs(K) & " END-EXEC." & vbCrLf
      End If
    Next
  End If
  
  '''''''''''''''''''''''''''''''''
  ' Inserimento Routine
  '''''''''''''''''''''''''''''''''
  RTRoutine.SelStart = wfine
  RTRoutine.SelLength = 0
  RTRoutine.SelText = routineText
    
  '''''''''''''''''''''''''''''''''
  ' Inserimento Fetch
  '''''''''''''''''''''''''''''''''
  If Len(Trim(GbTestoFetch)) Then
    'Embedded:
    routineText = routineText & vbCrLf & vbCrLf & GbTestoFetch
    wfine = Len(RTRoutine.text)
    RTRoutine.SelLength = 0
    RTRoutine.SelStart = wfine
    RTRoutine.SelText = vbCrLf & GbTestoFetch
  End If
  
  ''''''''''''''''''''''''''''''''''
  ' SQ 16-11-06 - EMBEDDED
  ' TMP
  ''''''''''''''''''''''''''''''''''
  If EmbeddedRoutine Then
    'SCRITTURA COPY-ROUTINE SU DISCO:
    'writeRoutineMember:
    testo = RTRoutine.text  'backup
    RTRoutine.text = routineText
    'Nome copy (fisso: vuoi il sangue?)
    'RT<numeroIstruzione>.cpy
    RTRoutine.SaveFile percorso & "RT" & Right(wIstruzione, 6) & ".cpy", 1
    RTRoutine.text = testo  'restore
    'INSERIMENTO ROUTINE IN INCAPSULATI:
    embedRoutine wIstruzione, routineText, RTRoutine
  End If
   
  'SQ D 4-08-06
  'Portare dentro le routine o... cambiare tutto!
  'Ora sono cicliche... devono ripulire la GbTestoRedOcc
  ' -> da allineare ISRT e DLET
  If Len(GbTestoRedOcc) Then
    testo = RTRoutine.text  'backup
    RTRoutine.text = GbTestoRedOcc
      Dim pos As Integer
    'buttare via Pos
    RTRoutine.SaveFile VSAM2Rdbms.drPathDb & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\cpy\" & GbFileRedOcc, 1
    RTRoutine.text = testo  'restore
    GbTestoRedOcc = ""
  End If
  GbFileRedOcc = ""  'Attenzione a brasare, questo fa da flag...
    
End Function

Public Function AggRoutDLI(rst As Recordset, RTRoutine As RichTextBox, NewIstr As String) As Boolean
   Dim K As Integer
   Dim IndIstr As Double
   Dim wIstr As String
   Dim wIstruzione As String
   Dim xIstruzione As String
   Dim nIstruzione As String
   Dim vPos As Double
   Dim wfine As Double
   Dim testo As String
   Dim SwInsIstr As Boolean
   Dim NumPCB As String
   Dim tb1 As Recordset
   Dim tb2 As Recordset
   Dim wIdDb As Variant
   Dim wIdSeg As Variant
   Dim wXistr As String
   Dim pXistr As Integer
   Dim TbPrg As Recordset
   Dim sDbd() As Long
   Dim xOperatore As String
   Dim l As Long
   
   sDbd = m_dllFunctions.getDBDByIdOggettoRiga(rst!idOggetto, rst!riga)
   
   If UBound(sDbd) > 0 Then
    mIdDBDCorrente = sDbd(1)
   Else
    mIdDBDCorrente = -1
   End If
    
   wIstr = wIstrCod(totIstr).Istr
   wIstruzione = NewIstr
   
   For K = 1 To UBound(wIstrCod)
      If NewIstr = wIstrCod(K).Istruzione Then
        xIstruzione = wIstrCod(K).Decodifica
        xOperatore = ""
        For l = 1 To UBound(wIstrCod(K).Liv)
          'SQ 5-06-06
          'Oper non esiste!?
          MsgBox "tmp: lavori in corso... Oper manca!"
          'xOperatore = xOperatore & wIstrCod(k).Liv(l).Oper(1)
        Next l
        Exit For
      End If
   Next K
   
   Gistruzione = Trim(wIstr) & Mid$(wIstruzione, 5)
   GbNumIstr = Mid(wIstruzione, 5)
   
   wfine = Len(RTRoutine.text)
   
   vPos = RTRoutine.find("#CODE#", 1)
   vPos = RTRoutine.find(vbCrLf, vPos + 1)
   vPos = vPos + 2
   
   If Trim(wIstruzione) <> "" Then
        testo = vbCrLf & "      **  " & Mid$(wIstruzione & Space(4), 1, 10) & " - "
        testo = testo & DecIstr(xIstruzione)
   End If
   
   If Trim(xOperatore) <> "" Then
       testo = testo & "(" & getOperatoreByLiteral(xOperatore) & ")" & vbCrLf
   Else
       testo = testo & vbCrLf & "      *"
   End If
   
   RTRoutine.SelStart = vPos
   RTRoutine.SelLength = 0
   RTRoutine.SelText = testo & vbCrLf
   
     vPos = RTRoutine.find("#OPE#", 1)
     vPos = RTRoutine.find(vbCrLf, vPos + 1)
     vPos = vPos + 2
     testo = Space(12) & "IF LNKDB-OPERATION = '" & wIstruzione & "' " & vbCrLf
     testo = testo & Space(15) & "PERFORM ROUT-" & Gistruzione & vbCrLf
     testo = testo & Space(18) & "THRU ROUT-" & Gistruzione & "-EX" & vbCrLf
     testo = testo & Space(15) & "GO TO MAIN-001." & vbCrLf
     RTRoutine.SelStart = vPos
     RTRoutine.SelLength = 0
     RTRoutine.SelText = testo
     
     testo = vbCrLf & "      *------------------------------------------------------------*" & vbCrLf
     testo = testo & "      *  Operation :     " & wIstruzione & "  " & vbCrLf
     testo = testo & "      *  Coding    :     " & DecIstr(xIstruzione) & vbCrLf
     
     If (xOperatore) <> "" Then
        testo = testo & "      *  Operator  : " & getOperatoreByLiteral(xOperatore) & vbCrLf
     End If
     
     testo = testo & "      *------------------------------------------------------------*" & vbCrLf
     'testo = testo & Space(7) & "ROUT-" & Gistruzione & "." & vbCrLf
     testo = testo & Space(7) & "9999-ROUT-" & Gistruzione & " SECTION." & vbCrLf
 
     wfine = Len(RTRoutine.text)
    
     BlkIni = 1
     BlkFin = UBound(TabIstr.BlkIstr)
     BlkStep = 1
    
     Select Case Trim(UCase(wIstr))
           Case "DLET"
              testo = testo & AggRtDLIGen(Trim(UCase(wIstr)))
           Case "ISRT"
              testo = testo & AggRtDLIGen(Trim(UCase(wIstr)))
           Case "REPL"
              testo = testo & AggRtDLIGen(Trim(UCase(wIstr)))
           Case "GU"
              testo = testo & AggRtDLIGen(Trim(UCase(wIstr)))
           Case "GHU"
              testo = testo & AggRtDLIGen(Trim(UCase(wIstr)))
           Case "GN"
              testo = testo & AggRtDLIGen(Trim(UCase(wIstr)))
           Case "GHN"
              'stefanopippo: � ora di farla
              testo = testo & AggRtDLIGen(Trim(UCase(wIstr)))
              'm_dllFunctions.WriteLog "AggRoutDLI - Unespected GHN Instruction - Generate as Simple 'GN'", "DR - Genrout"
              'testo = testo & AggRtDLIGen(Trim(UCase(wIstr)))
           Case "GNP"
              testo = testo & AggRtDLIGen(Trim(UCase(wIstr)))
           Case "PCB", "TERM", "CHKP", "SYNC", "QRY", "QUERY"
              testo = testo & getUTIL_DB2(Trim(UCase(wIstr)))
           Case "GHNP"
              'stefanopippo: � ora di farla
              testo = testo & AggRtDLIGen(Trim(UCase(wIstr)))
              'm_dllFunctions.WriteLog "AggRoutDLI - Unespected GHNP Instruction", "DR - Genrout"
              'testo = testo & Space(6) & "*" & Space(5) & "GHNP UNESPECTED..." & vbCrLf
     End Select
         
'  testo = testo & Space(7) & "ROUT-" & Gistruzione & "-EX." & vbCrLf
'  testo = testo & Space(11) & "EXIT." & vbCrLf & vbCrLf
  
  RTRoutine.SelLength = 0
  RTRoutine.SelStart = wfine
  RTRoutine.SelText = testo

End Function

Public Function getOperatoreByLiteral(xOpLiteral As String) As String
    Dim wApp As String
    
    wApp = Replace(xOpLiteral, ".", "")
    
    Select Case Trim(UCase(wApp))
        Case "GT"
            getOperatoreByLiteral = ">"
            
        Case "LT"
            getOperatoreByLiteral = "<"
            
        Case "GE"
            getOperatoreByLiteral = ">="
            
        Case "LE"
            getOperatoreByLiteral = "<="
            
        Case "NE"
            getOperatoreByLiteral = "<>"
        
        Case "EQ"
            getOperatoreByLiteral = "="
        Case ""
            getOperatoreByLiteral = ""
            
        Case Else
            getOperatoreByLiteral = "!"
        
    End Select
End Function


Public Function CarNumKey(Ind) As Integer
Dim NumKey As Integer
Dim k1 As Integer
Dim k3 As Integer

NumKey = 0

k3 = UBound(TabIstr.BlkIstr(Ind).KeyDef)

For k1 = 1 To k3
  If TabIstr.BlkIstr(Ind).KeyDef(k1).key = TabIstr.BlkIstr(Ind).KeyDef(k3).key Then
     NumKey = NumKey + 1
  End If
Next k1

CarNumKey = NumKey

End Function
'''''''''''''''''''''''''''''''''''''''''''
'
'''''''''''''''''''''''''''''''''''''''''''
Public Function getDLET_DB2_OLD() As String
  Dim K As Integer
  Dim testo As String
  ''Dim idRed() As Variant
  ''dim nomeTRed() As String, nomeSRed() As String, testoRed() As String
  ''Dim nomeKeyRed() As Variant, nomeFieldRed() As Variant
  Dim tbred As Recordset
  Dim indRed As Variant
  Dim indentRed As Variant
  Dim flagSel As Boolean
  Dim tabSave As String
  Dim idSave As Variant
  
  indentGen = 0
  
  testo = InitIstr("DLET", "N")
  testo = testo & vbCrLf
  testo = testo & Space(11) & "MOVE '" & TabIstr.BlkIstr(1).Tavola & "' TO WK-NAMETABLE" & vbCrLf
  testo = testo & Space(11) & "PERFORM 10000-REST-FDBKEY" & vbCrLf & vbCrLf
  
  If Trim(TabIstr.BlkIstr(1).pKey) <> "" Then
     testo = testo & Space(11) & "MOVE KRR-" & TabIstr.BlkIstr(1).pKey & " TO K01-" & TabIstr.BlkIstr(1).pKey & vbCrLf
     'testo = testo & Space(11) & "PERFORM DEL-CHILD THRU DEL-CHILD-EX" & vbCrLf & vbCrLf
     'stefano: per ora usa integrit� referenziale
     'testo = testo & Space(11) & "PERFORM 9999-DEL-CHILD" & vbCrLf & vbCrLf
  End If
   
   'stefanopippo: 17/08/2005: gestisce le tabelle gi� esistenti evitando
   'la delete; effettua comunque del-child per i figli (solo se a loro volta non
   'provenienti da tabelle gi� esistenti)
  'stefano: per ora usa integrit� referenziale
  '   Set Tb1 = m_dllFunctions.Open_Recordset("select * from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & " and tag = 'OLD'")
  '   If Tb1.RecordCount Then
  '      testo = testo & Space(11) & "MOVE ZERO TO SQLCODE WK-SQLCODE." & vbCrLf
  '      getDLET_DB2 = testo
  '      Exit Function
  '   End If
  '   testo = testo & Space(11) & "IF WK-SQLCODE NOT = ZERO AND +100" & vbCrLf
  '   'testo = testo & Space(11) & "   GO TO " & ExRout & vbCrLf
  '   testo = testo & Space(11) & "   EXIT" & vbCrLf
  '   testo = testo & Space(11) & "END-IF" & vbCrLf & vbCrLf
       
      'stefanopippo: redefines
      
   indentRed = 0
   Set tbred = m_dllFunctions.Open_Recordset( _
    "select condizione, rdbms_tbname from psdli_segmenti where idsegmento = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm)
   indRed = 1
   ReDim Preserve testoRed(1)
   ReDim Preserve nomeTRed(1)
   ReDim Preserve nomeSRed(1)
   ReDim Preserve idRed(1)
   ReDim Preserve nomeKeyRed(1)
   ReDim Preserve nomeFieldRed(1)
   'idRed(1) = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm
   idRed(1) = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).idTable
   nomeTRed(1) = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Tavola
   nomeSRed(1) = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Segment
   If Len(tbred!condizione) Then
      testoRed(1) = getCondizione_Redefines(tbred!condizione & "_lett")
   Else
      testoRed(1) = ""
   End If
   If Len(tbred!rdbms_tbname) Then
      If Left(tbred!rdbms_tbname, 7) = "Storico" Then
         'SP: distingue tra cics e batch
         'SP 15/9 gestione varie replace; da rivedere per banca
  '         If SwTp And m_dllFunctions.FnNomeDB <> "banca.mty" Then
         If SwTp Then
            testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_cics_repl")
         Else
            testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_repl")
         End If
      End If
   End If
'''MG 240107
'''''   Set tbred = m_dllFunctions.Open_Recordset("select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
'''''           "from mgdli_segmenti as a, dmdb2_tabelle as b, dmdb2_index as c " & _
'''''           "where a.idsegmentoorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
'''''           " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
   Set tbred = m_dllFunctions.Open_Recordset("select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
           "from mgdli_segmenti as a, " & DB & "tabelle as b, " & DB & "index as c " & _
           "where a.idsegmentoorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
           " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
   If tbred.RecordCount Then
      flagSel = False
      ReDim Preserve testoRed(tbred.RecordCount + 1)
      ReDim Preserve nomeTRed(tbred.RecordCount + 1)
      ReDim Preserve nomeSRed(tbred.RecordCount + 1)
      ReDim Preserve idRed(tbred.RecordCount + 1)
      ReDim Preserve nomeKeyRed(tbred.RecordCount + 1)
      ReDim Preserve nomeFieldRed(tbred.RecordCount + 1)
      While Not tbred.EOF
         indRed = indRed + 1
         
         If Len(tbred!condizione) Then
           'If tbred!condizione = "SELETTORE" Then
           If Not flagSel Then
             Dim tbsel As Recordset
'''MG 240107
'''''             Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
'''''               "from dmdb2_tabelle " & _
'''''               "where idorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
'''''               " and TAG like '%DISPATCHER%'")
             Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
               "from " & DB & "tabelle " & _
               "where idorigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm & _
               " and TAG like '%DISPATCHER%'")
             If Not tbsel.EOF Then
       'tabella selettrice: riposiziona e abblenka
       'stefano: non salva pi� la tabella precedente, perch� in caso di dispatcher � una
       ' tabella inutile; sarebbe meglio anche evitare di crearla in conversione...
  '               idRed(indRed) = idRed(1)
  '               nomeTRed(indRed) = nomeTRed(1)
  '               nomeSRed(indRed) = nomeSRed(1)
  '               testoRed(indRed) = "IF WK-NAMETABLE = '" & nomeTRed(1) & "'"
               testoRed(1) = ""
               idRed(1) = tbsel!idTable
               nomeTRed(1) = tbsel!nome
               nomeSRed(1) = "DISPATCHER"
               indRed = 2
               flagSel = True
               If UBound(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).KeyDef) > 0 Then
                  nomeKeyRed(1) = TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).KeyDef(1).key
               Else
                  nomeKeyRed(1) = ""
               End If
       'stefano: campo per il nome del field; uso l'idfield della redefines perch� sembra
       ' corretto.
               Dim tbred2 As Recordset
               Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
               "from psdli_field where idfield = " & tbred!IdField & " and idsegmento = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm)
               If Not tbred2.EOF Then
                  nomeFieldRed(1) = tbred2!Nomeidx
               Else
                  m_dllFunctions.WriteLog "Field not found: " & tbred!IdField, "Routines Generation"
                  nomeFieldRed(1) = ""
               End If
             End If
           Else
           End If
           If flagSel Then
              testoRed(indRed) = "IF WK-NAMETABLE = '" & tbred!nomeTab & "'"
           Else
              testoRed(indRed) = getCondizione_Redefines(tbred!condizione & "_lett")
           End If
           idRed(indRed) = tbred!idTab
           nomeTRed(indRed) = tbred!nomeTab
           nomeSRed(indRed) = tbred!nomeseg
           nomeKeyRed(indRed) = tbred!nomeindex
       'stefano: campo per il nome del field
            Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
            "from mgdli_field where idfield = " & tbred!IdField & " and idsegmento = " & tbred!idSegmento)
            If Not tbred2.EOF Then
               nomeFieldRed(indRed) = tbred2!Nomeidx
            Else
               m_dllFunctions.WriteLog "Virtual Field not found: " & tbred!IdField, "Routines Generation"
               nomeFieldRed(indRed) = ""
            End If
         Else
            m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
         End If
         tbred.MoveNext
      Wend
      tbred.Close
   End If
  
  Dim Start As Integer
  Start = 1
  
  If flagSel Then
     Start = 2
     'testo = testo & getCondizione_Redefines(testoSel(1)) & vbCrLf
     'la DLET � sempre monolivello, almeno per le CALL CBLTDLI
     testo = testo & Space(11) & "EXEC SQL SELECT " & vbCrLf _
                & CostruisciSelect("T1", idRed(1))
     testo = testo & Space(15) & "INTO " & vbCrLf
     testo = testo & CostruisciInto("T1", idRed(1), nomeTRed(1), "REPL")
     testo = testo & Space(14) & "FROM " & vbCrLf
     'fissa: per le REPL non serve altro
     'testo = testo & CostruisciFrom(1, 1, True) & vbCrLf
     'SQ CREATOR (prendo quello della principale...)
     'testo = testo & Space(17) & nomeTRed(1) & " T1" & vbCrLf
     ' Mauro 28-03-2007 : Eliminazione CREATOR
     'testo = testo & Space(17) & IIf(Len(TabIstr.BlkIstr(1).tableCreator), TabIstr.BlkIstr(1).tableCreator & ".", "") & nomeTRed(1) & " T1" & vbCrLf
     testo = testo & Space(17) & nomeTRed(1) & " T1" & vbCrLf
     tabSave = TabIstr.BlkIstr(1).Tavola
     idSave = TabIstr.BlkIstr(1).idTable
     TabIstr.BlkIstr(1).idTable = idRed(1)
     TabIstr.BlkIstr(1).Tavola = nomeTRed(1)
     indentGen = 0
     testo = testo & Space(14) & CostruisciWhere(1, 1)
     TabIstr.BlkIstr(1).Tavola = tabSave
     TabIstr.BlkIstr(1).idTable = idSave
     testo = testo & Space(11) & "END-EXEC" & vbCrLf
     testo = testo & Space(11) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
     'EXIT non funzionante
  '     testo = testo & Space(11) & "IF WK-SQLCODE NOT = ZERO" & vbCrLf
  '     'testo = testo & Space(11) & "   GO TO " & ExRout & vbCrLf
  '     'testo = testo & Space(11) & "   EXIT SECTION" & vbCrLf
  '     testo = testo & Space(11) & "   EXIT" & vbCrLf
  '     testo = testo & Space(11) & "END-IF" & vbCrLf
  
      Dim tabFound As Boolean
      tabFound = False
      Dim IdxTabOk As Integer
      For IdxTabOk = 1 To idxTabOkMax
        If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "DLET" Then
          tabFound = True
          Exit For
        End If
      Next
      If Not tabFound Then
         idxTabOkMax = idxTabOkMax + 1
         ReDim Preserve tabOk(idxTabOkMax)
         ReDim Preserve istrOk(idxTabOkMax)
         tabOk(idxTabOkMax) = nomeTRed(1)
         istrOk(idxTabOkMax) = "DLET"
      End If
      testo = testo & vbCrLf
      testo = testo & Space(6) & "*** REDEFINES" & vbCrLf
      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 9999-" & SostUnderScore(nomeTRed(1)) & "-DLET" & vbCrLf
      testo = testo & Space(11) & "END-IF" & vbCrLf
      testo = testo & vbCrLf
    'stefano: poi lo aggiustiamo con get environment (bisogna fare il
    ' parametro per le nuove copy P: P[tipo-istr][ALIAS]
      GbFileRedOcc = "PD" & Right(getAlias_tabelle(Int(idRed(1))), 6)
      'stefano: non � un granch�....
      GbTestoRedOcc = GbTestoRedOcc & Space(7) & "9999-" & SostUnderScore(nomeTRed(1)) & "-DLET SECTION." & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11) & "MOVE " & nomeFieldRed(1) & " OF KRR-" & SostUnderScore(TabIstr.BlkIstr(BlkFin).pKey) & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11) & "  TO KRD-" & SostUnderScore(TabIstr.BlkIstr(BlkFin).pKey) & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(6) & "*** REDEFINES" & vbCrLf
  End If
  
  For K = Start To indRed
     indentRed = 0
     If Len(testoRed(K)) Then
        GbTestoRedOcc = GbTestoRedOcc & Space(11) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
        GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRD-" & SostUnderScore(TabIstr.BlkIstr(BlkFin).pKey) & " TO" & vbCrLf
        'SQ
        'GbTestoRedOcc = GbTestoRedOcc & Space(16 + indentRed) & "KRD-" & nomeFieldRed(k) & vbCrLf
        GbTestoRedOcc = GbTestoRedOcc & Space(16 + indentRed) & "KRD-" & nomeKeyRed(K) & vbCrLf
        GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
        GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRR-" & SostUnderScore(TabIstr.BlkIstr(BlkFin).pKey) & " TO KRR-" & nomeKeyRed(K) & vbCrLf
        GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
        'per ora K01 fisso!!!
        GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRR-" & nomeKeyRed(K) & " TO K01-" & nomeKeyRed(K) & vbCrLf
     End If
    
     GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "EXEC SQL DELETE " & vbCrLf
     idSave = TabIstr.BlkIstr(1).idTable
     tabSave = TabIstr.BlkIstr(1).Tavola
     TabIstr.BlkIstr(1).Tavola = nomeTRed(K)
     TabIstr.BlkIstr(1).idTable = idRed(K)
     indentGen = indentRed
     GbTestoRedOcc = GbTestoRedOcc & Space(12 + indentRed) & "FROM" & vbCrLf & CostruisciFrom(1, 1, True)
     GbTestoRedOcc = GbTestoRedOcc & CostruisciWhere(1, 1)
     TabIstr.BlkIstr(1).Tavola = tabSave
     TabIstr.BlkIstr(1).idTable = idSave
     GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "END-EXEC" & vbCrLf
     GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
  'exit non funzionante
  '     GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
  '     'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "GO TO " & ExRout & vbCrLf
  '     'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "EXIT SECTION" & vbCrLf
  '     GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "EXIT" & vbCrLf
  '     GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "END-IF" & vbCrLf
     If testoRed(K) <> "" Then
        GbTestoRedOcc = GbTestoRedOcc & Space(11) & "END-IF" & vbCrLf
     End If
     'GbTestoRedOcc = GbTestoRedOcc & Space(11) & "." & vbCrLf
  
      If (Not flagSel) Or K = 1 Then
         testo = testo & GbTestoRedOcc
         GbTestoRedOcc = ""
      Else
         GbTestoRedOcc = GbTestoRedOcc & Space(11) & "." & vbCrLf
      End If
  Next
  getDLET_DB2_OLD = testo
End Function
'''''''''''''''''''''''''''''''''''''''''''''''
' Gestione "PATH CALL" (C.C. "D")
' => insert su pi� livelli
'''''''''''''''''''''''''''''''''''''''''''''''
Public Function getISRT_DB2_OLD(rtbFile As RichTextBox) As String
  Dim K As Integer, isrtlevel As Integer
  Dim rs As Recordset, tbred As Recordset
  Dim NomeT2 As String, NomePk As String, nomeKey As String, NOMEKEYDLI As String
  Dim testo As String, NomeT As String
  '''''''''Dim idRed() As Variant 'redefines
  '''''''''Dim nomeTRed() As String, nomeSRed() As String, testoRed() As String
  Dim indRed As Integer, indentRed As Integer
  Dim flagSel As Boolean
  
  indentGen = 0 '?
  
  testo = InitIstr("ISRT", "S") & vbCrLf
   
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Tabelle "OLD" (gestione standard o pezza?)
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'stefanopippo: 17/08/2005: gestisce le tabelle gi� esistenti evitando l'insert; vale solo l'ultimo livello
  'spostato dopo la select, in quanto deve comunque salvare la chiave per il posizionamento dei figli
'''MG 240107
'''''  Set rs = m_dllFunctions.Open_Recordset("select * from dmdb2_tabelle where tag = 'OLD' and idOrigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm)
  Set rs = m_dllFunctions.Open_Recordset("select * from " & DB & "tabelle where tag = 'OLD' and idOrigine = " & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).IdSegm)

  If rs.RecordCount Then
    '''flagOld = True
    'SP 15/9 vogliono cos�, ripuliamo tutto
    testo = testo & Space(11) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
    getISRT_DB2_OLD = testo
    rs.Close
    Exit Function
  End If
  rs.Close
  
  ''''''''''''''''''''''''''''''''''''''''''
  ' Qualificazione sui livelli precedenti
  ''''''''''''''''''''''''''''''''''''''''''
  'SQ 21-07-06
  'Ex controllo interno alla getGU_DB2: direi che qui � molto meglio
  'P.S.: Dovremmo entrare solo se livelli qualificati...
  '      !!!! CONSIDERARE isrtLevel ("D"), no sempre l'ultimo !!!!
  If UBound(TabIstr.BlkIstr) > 1 Then
    'Posizionamento sul parent: lettura come se fosse GU sui livelli precedenti
    testo = testo & getGU_DB2(rtbFile, "ISRT", False)
  Else
    'Servono?
    GbTestoFetch = ""
    GbTestoRedOcc = ""
    GbFileRedOcc = ""
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' GESTIONE C.C. "D" - "PATH CALL"
  ' Gli inserimenti sono su N livelli
  ' SQ 2-08-06
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'L'ultimo livello � "D" per default (lo appioppo):
  TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).CodCom = "D" & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).CodCom
  'Cerco "isrtLevel": primo livello d'inserimento:
  For isrtlevel = 1 To UBound(TabIstr.BlkIstr)
    If InStr(TabIstr.BlkIstr(isrtlevel).CodCom, "D") Then Exit For
  Next
  
  For isrtlevel = isrtlevel To UBound(TabIstr.BlkIstr)
    NomeT = TabIstr.BlkIstr(isrtlevel).Tavola
    'SQ 1-08-06 - CREATOR
    m_author = TabIstr.BlkIstr(isrtlevel).tableCreator  'buttare via questa variabile globale...
    If Len(m_author) Then m_author = m_author & "."
    
    '''wLiv01Copy = getCopyDLISeg(isrtLevel)
    
    'CHIARIRE
    If isrtlevel = 1 Then
      NomeT2 = getParentTable(CLng(TabIstr.BlkIstr(isrtlevel).IdSegm))  'togliere cast e cambiare function
      If Len(NomeT2) Then
        'SEGMENTO NON RADICE: RESTORE
        testo = testo & Space(11) & "MOVE '" & NomeT2 & "' TO WK-NAMETABLE" & vbCrLf
        'testo = testo & Space(11) & "PERFORM REST-FDBKEY THRU REST-FDBKEY-EX" & vbCrLf & vbCrLf
        testo = testo & Space(11) & "PERFORM 10000-REST-FDBKEY" & vbCrLf & vbCrLf
      End If
    End If
    
    
    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    testo = testo & Space(11) & "   MOVE '" & NomeT & "' TO WK-NAMETABLE " & vbCrLf
    testo = testo & Space(11) & "   PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
    testo = testo & Space(11) & "END-IF" & vbCrLf
     
    '''indentRed = 0
    Set tbred = m_dllFunctions.Open_Recordset( _
      "Select condizione, rdbms_tbname from psdli_segmenti " & _
      " where idsegmento = " & TabIstr.BlkIstr(isrtlevel).IdSegm)
    indRed = 1
    ReDim Preserve testoRed(1)
    ReDim Preserve nomeTRed(1)
    ReDim Preserve nomeSRed(1)
    ReDim Preserve idRed(1)
    'idRed(1) = TabIstr.BlkIstr(isrtLevel).IdSegm
    idRed(1) = TabIstr.BlkIstr(isrtlevel).idTable
    nomeTRed(1) = TabIstr.BlkIstr(isrtlevel).Tavola
    nomeSRed(1) = TabIstr.BlkIstr(isrtlevel).Segment
    If Len(tbred!condizione) Then
      testoRed(1) = getCondizione_Redefines(tbred!condizione)
    Else
      testoRed(1) = ""
    End If
     
    If Len(tbred!rdbms_tbname) Then
      ''''''''''''''''''''''''''''''''''''
      'SQ - ALTRA GESTIONE NON STANDARD?
      ''''''''''''''''''''''''''''''''''''
      If Left(tbred!rdbms_tbname, 7) = "Storico" Then
         'SP: distingue tra cics e batch
  '      If SwTp And m_dllFunctions.FnNomeDB <> "banca.mty" Then
         If SwTp Then
            testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_cics_isrt")
         Else
            testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_isrt")
         End If
      End If
    End If
    tbred.Close
    
    ''''''''''''''''''''''
    ' REDEFINES
    ''''''''''''''''''''''
  '   Set tbred = m_dllFunctions.Open_Recordset("select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione " & _
  '               "from psdli_segmenti as a, dmdb2_tabelle as b " & _
  '               "where a.idsegmentoorigine = " & TabIstr.BlkIstr(isrtLevel).IdSegm & _
  '               " and a.idsegmento = b.idorigine order by a.condizione desc")
'''MG 240107
'''''    Set tbred = m_dllFunctions.Open_Recordset( _
'''''      "select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idtab " & _
'''''      " from mgdli_segmenti as a, dmdb2_tabelle as b " & _
'''''      " where a.idsegmentoorigine = " & TabIstr.BlkIstr(isrtLevel).IdSegm & _
'''''      " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' order by b.nome")
    Set tbred = m_dllFunctions.Open_Recordset( _
      "select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idtab " & _
      " from mgdli_segmenti as a, " & DB & "tabelle as b " & _
      " where a.idsegmentoorigine = " & TabIstr.BlkIstr(isrtlevel).IdSegm & _
      " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' order by b.nome")
    If tbred.RecordCount Then
      flagSel = False
      ReDim Preserve testoRed(tbred.RecordCount + 1)
      ReDim Preserve nomeTRed(tbred.RecordCount + 1)
      ReDim Preserve nomeSRed(tbred.RecordCount + 1)
      ReDim Preserve idRed(tbred.RecordCount + 1)
      While Not tbred.EOF
        indRed = indRed + 1
        If Len(tbred!condizione) Then
          'stefano: cerca il selettore nel nuovo modo
          'If tbred!condizione = "SELETTORE" Then
          If Not flagSel Then
               Dim tbsel As Recordset
'''MG 240107
'''''              Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
'''''                 "from dmdb2_tabelle " & _
'''''                 "where idorigine = " & TabIstr.BlkIstr(isrtLevel).IdSegm & _
'''''                 " and TAG like '%DISPATCHER%'")
              Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
                 "from " & DB & "tabelle " & _
                 "where idorigine = " & TabIstr.BlkIstr(isrtlevel).IdSegm & _
                 " and TAG like '%DISPATCHER%'")
              If Not tbsel.EOF Then
                'tabella selettrice: riposiziona e abblenka
                'stefano: non salva pi� la tabella precedente, perch� in caso di dispatcher � una
                ' tabella inutile; sarebbe meglio anche evitare di crearla in conversione...
                testoRed(1) = ""
                idRed(1) = tbsel!idTable
                nomeTRed(1) = tbsel!nome
                'nomeSRed(1) = "DISPATCHER"
                nomeSRed(1) = TabIstr.BlkIstr(isrtlevel).Segment
                indRed = 2
                flagSel = True
              End If
          End If
          testoRed(indRed) = getCondizione_Redefines(tbred!condizione)
          'idRed(indRed) = tbred!idSegmento
          idRed(indRed) = tbred!idTab
          nomeTRed(indRed) = tbred!nomeTab
          nomeSRed(indRed) = tbred!nomeseg
          'SP redefines
        Else
          m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
        End If
        tbred.MoveNext
      Wend
      tbred.Close
    End If
     
    GbTestoRedOcc = ""
    
    'SQ - Richiesta VC: spostato sotto
'    If flagSel Then
'      testo = testo & vbCrLf
'      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
'      testo = testo & Space(6) & "*** REDEFINES" & vbCrLf
'      testo = testo & Space(11) & "   MOVE LNKDB-DATA-AREA(" & Format(isrtLevel) & ") TO RD-" & nomeSRed(1) & vbCrLf
'      testo = testo & Space(11) & "   MOVE " & Format(isrtLevel) & " TO WK-IND2" & vbCrLf
'      testo = testo & Space(11) & "   PERFORM 9999-" & Replace(nomeTRed(1), "_", "-") & "-ISRT" & vbCrLf
'      testo = testo & Space(11) & "END-IF" & vbCrLf
'      testo = testo & vbCrLf
'      GbFileRedOcc = "PI" & Right(getAlias_tabelle(Int(idRed(1))), 6)
'    End If
     
    For K = 1 To indRed
      indentRed = 0
        
      Dim tabFound As Boolean
      tabFound = False
      If flagSel And K > 1 Then 'solo per le redefines con selettore
        Dim IdxTabOk As Integer
        For IdxTabOk = 1 To idxTabOkMax
          If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "ISRT" Then
            tabFound = True
            Exit For
          End If
        Next
        If Not tabFound Then
          idxTabOkMax = idxTabOkMax + 1
          ReDim Preserve tabOk(idxTabOkMax)
          ReDim Preserve istrOk(idxTabOkMax)
          tabOk(idxTabOkMax) = nomeTRed(1)
          istrOk(idxTabOkMax) = "ISRT"
        End If
                
        If K = 2 Then 'Primo elemento: 1 � la DISPATHER
          GbTestoRedOcc = GbTestoRedOcc & Space(7) & "9999-" & Replace(nomeTRed(1), "_", "-") & "-ISRT SECTION." & vbCrLf & vbCrLf
          GbTestoRedOcc = GbTestoRedOcc & Space(6) & "*** REDEFINES" & vbCrLf
        End If
      End If
      
      'SQ 2-08-06 (portato sopra)
      'stefano: non � un granch�....
      'If k = 2 And flagSel Then
      '  GbTestoRedOcc = GbTestoRedOcc & Space(7) & "9999-" & Replace(nomeTRed(1), "_", "-") & "-ISRT SECTION." & vbCrLf & vbCrLf
      '  GbTestoRedOcc = GbTestoRedOcc & Space(6) & "*** REDEFINES" & vbCrLf
      'End If
        
      'SQ dentro la Else: GbTestoRedOcc = GbTestoRedOcc & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      If Len(testoRed(K)) Then
        'Condizione:
        GbTestoRedOcc = GbTestoRedOcc & Space(11) & Trim(testoRed(K)) & vbCrLf
        'SQ tmp: sistemare i 14!
         'indentRed = 3
         indentRed = 0
      Else
        'SQ
        GbTestoRedOcc = GbTestoRedOcc & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      End If
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE " & vbCrLf
      'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "PERFORM SETK-WKEYADL THRU SETK-WKEYADL-EX" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "PERFORM 9999-SETK-WKEYADL" & vbCrLf
      
      'If flagSel Then
        'VERIFICARE!
        'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE LNKDB-DATA-AREA(" & Format(isrtLevel) & ") TO AREA-" & TabIstr.BlkIstr(isrtLevel).Segment & vbCrLf
      'Else
      If K = 1 And Not flagSel Then
        GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE LNKDB-DATA-AREA(" & Format(isrtlevel) & ") TO RD-" & nomeSRed(K) & vbCrLf
      Else
        If Not flagSel Then
          GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE LNKDB-DATA-AREA(WK-IND2) TO RD-" & nomeSRed(K) & vbCrLf
        End If
      End If
      'End If
      
      'SQ 'se lo metto in testa lo perde: chi lo resetta?!
      indentGen = 8
      
      'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "PERFORM " & SostUnderScore(nomeTRed(k)) & "-TO-RR THRU " & SostUnderScore(nomeTRed(k)) & "-TO-RR-EX" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "PERFORM 9999-" & Replace(nomeTRed(K), "_", "-") & "-TO-RR" & vbCrLf & vbCrLf
      '''If Not flagOld Then
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "EXEC SQL " & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "    INSERT INTO " & m_author & nomeTRed(K) & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "    (" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & CostruisciSelect("", idRed(K))
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "    )" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "    VALUES (" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & CostruisciInto("T" & isrtlevel, idRed(K), nomeTRed(K), "ISRT")
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "    )" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "END-EXEC" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "IF WK-SQLCODE NOT = ZERO" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "   IF WK-SQLCODE = -803" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "      MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE " & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "      PERFORM 9999-SETK-ADLWKEY" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "      MOVE '" & NomeT & "' TO WK-NAMETABLE " & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "      PERFORM 11000-STORE-FDBKEY" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "   END-IF" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "END-IF" & vbCrLf 'exit non funzionante
      
      'SQ solo se non in copy...
      If Len(testoRed(K)) = 0 Then
        GbTestoRedOcc = GbTestoRedOcc & Space(11) & "END-IF" & vbCrLf
      End If
    
      '''''''''''''''
      ' OCCURS
      '''''''''''''''
      'purtroppo devo fare cos� a quest'ora....
      GbTestoRedOcc2 = ""
      If (Not flagSel) Or K = 1 Then
         Dim tbOcc As Recordset
'''MG 240107
'''''         Set tbOcc = m_dllFunctions.Open_Recordset("select *" & _
'''''             " from dmdb2_tabelle as a, dmdb2_index as b" & _
'''''             " where a.idtable = b.idtable and a.idtablejoin = " & idRed(K) & _
'''''             " and a.TAG like '%OCCURS%' and b.tipo = 'P' order by a.nome")
         Set tbOcc = m_dllFunctions.Open_Recordset("select *" & _
             " from " & DB & "tabelle as a, " & DB & "index as b" & _
             " where a.idtable = b.idtable and a.idtablejoin = " & idRed(K) & _
             " and a.TAG like '%OCCURS%' and b.tipo = 'P' order by a.nome")

         If Not tbOcc.EOF Then
           Dim tabFound2 As Boolean
           Dim IdxTabOk2 As Integer
           tabFound2 = False
           For IdxTabOk2 = 1 To idxTabOkMax2
              If tabOk2(IdxTabOk2) = idRed(K) And istrOk2(IdxTabOk2) = "ISRT" Then
                 tabFound2 = True
                 Exit For
              End If
           Next
           If Not tabFound2 Then
              idxTabOkMax2 = idxTabOkMax2 + 1
              ReDim Preserve tabOk2(idxTabOkMax2)
              ReDim Preserve istrOk2(idxTabOkMax2)
              tabOk2(idxTabOkMax2) = idRed(K)
              istrOk2(idxTabOkMax2) = "ISRT"
           End If
           Dim flagOcc As Boolean
           flagOcc = False
           If Not tabFound2 Then
              GbTestoRedOcc2 = GbTestoRedOcc2 & costruisciOccursInsOld(idRed(K), indentRed, "ISRT", flagOcc, False)
           End If
  '         If flagOcc Or tabFound2 Then
  '            GbTestoRedOcc = GbTestoRedOcc & vbCrLf
  '            GbTestoRedOcc = GbTestoRedOcc & Space(6) & "*** OCCURS" & vbCrLf
  '            GbTestoRedOcc = GbTestoRedOcc & Space(11) & "PERFORM 9999-" & SostUnderScore(nomeTRed(1)) & "-ISRT" & vbCrLf
  '            GbTestoRedOcc = GbTestoRedOcc & vbCrLf
  '            'stefano: poi lo aggiustiamo con get environment (bisogna fare il
  '            ' parametro per le nuove copy P: P[tipo-istr][ALIAS]
  '            GbFileRedOcc = "PI" & Right(getAlias_tabelle(Int(idRed(1))), 6)
  '         End If
         End If
      Else
        GbTestoRedOcc = GbTestoRedOcc & costruisciOccursInsOld(idRed(K), IIf(flagSel, indentRed + 3, indentRed), "ISRT", flagOcc, True)
      End If
        
      If Len(testoRed(K)) Then
        'SQ: WK-NAMETABLE valorizzato con la tabella ridefinita serve al chiamante;
        '    se ho OCCURS viene sovrascritto: lo riassegno
        GbTestoRedOcc = GbTestoRedOcc & Space(14) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
        GbTestoRedOcc = GbTestoRedOcc & Space(11) & "END-IF" & vbCrLf
      End If
      
      'GbTestoRedOcc = GbTestoRedOcc & Space(11) & "." & vbCrLf
      If (Not flagSel) Or K = 1 Then
         testo = testo & GbTestoRedOcc
         GbTestoRedOcc = GbTestoRedOcc2
      Else
        GbTestoRedOcc = GbTestoRedOcc & Space(11) & "." & vbCrLf
      End If
    Next
         
    'SP 5/9 keyfeedback per ora non gestita nella insert (gestione diversa dalla lettura)
    '  ...
    '  End If
    
    If flagSel Then
      testo = testo & vbCrLf & _
        Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
        Space(6) & "*** REDEFINES" & vbCrLf & _
        Space(11) & "   MOVE LNKDB-DATA-AREA(" & Format(isrtlevel) & ") TO RD-" & nomeSRed(1) & vbCrLf & _
        Space(11) & "   MOVE " & Format(isrtlevel) & " TO WK-IND2" & vbCrLf & _
        Space(11) & "   PERFORM 9999-" & Replace(nomeTRed(1), "_", "-") & "-ISRT" & vbCrLf & _
        Space(11) & "END-IF" & vbCrLf & vbCrLf
      
      GbFileRedOcc = "PI" & Right(getAlias_tabelle(Int(idRed(1))), 6)
    End If 'se corretto qui, accorpare con if sotto
    If (flagOcc Or tabFound2) And Not flagSel Then
      testo = testo & vbCrLf & _
        Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
        Space(6) & "*** OCCURS" & vbCrLf & _
        Space(11) & "   PERFORM 9999-" & Replace(nomeTRed(1), "_", "-") & "-ISRT" & vbCrLf & _
        Space(11) & "END-IF" & vbCrLf
      
      GbFileRedOcc = "PI" & Right(getAlias_tabelle(Int(idRed(1))), 6) 'fare parametro per le nuove copy P: P[tipo-istr][ALIAS]
    End If
    testo = testo & vbCrLf & _
      Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
      Space(11) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf & _
      Space(11) & "   MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf & _
      Space(11) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf & _
      Space(11) & "END-IF" & vbCrLf & vbCrLf
  Next
  
  getISRT_DB2_OLD = testo
  
End Function
'SQ
Public Function getCondizione_Redefines(condizione As String) As String
  Dim line As String
  Dim FD As Long
  
  On Error GoTo loadErr
  
  FD = FreeFile
  'SQ
  'wstr = Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1)
  'Open VSAM2Rdbms.drPathDb & "\" & wstr & "\Input-Prj\redefines\" & condizione For Input As #1
  'Open m_dllFunctions.FnPathPrj & "\Input-Prj\redefines\" & condizione For Input As #FD
  Open m_dllFunctions.FnPathPrj & "\Input-Prj\Template\imsdb\standard\redefines\" & condizione For Input As #FD
  
  'Lettura per linee
  While Not EOF(FD)
    Line Input #FD, line
    getCondizione_Redefines = getCondizione_Redefines & line & vbCrLf
  Wend
  Close FD
    
  Exit Function
loadErr:
  getCondizione_Redefines = ""
  Select Case Err.Number
    Case 53
'      m_dllFunctions.WriteLog "Template """ & m_dllFunctions.FnPathPrj & "\Input-Prj\redefines\" & condizione & """ not found."
      m_dllFunctions.WriteLog "Template """ & m_dllFunctions.FnPathPrj & "\Input-Prj\Template\imsdb\standard\redefines\" & condizione & """ not found."
    Case Else
      MsgBox Err.Description
  End Select
    'Resume
End Function
''''''''''''''''''''''''''''''''''''''''''''''''
' SQ - 3-08-06
' Gestione "PATH-CALL" (C.C. "D")
' ==> Gestire C.C. "N" a run-time
''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getREPL_DB2_OLD(rtbFile As RichTextBox) As String
  Dim K As Integer, indRed As Integer, indentRed As Integer, baseMove As Integer
  Dim testo As String, tabSave As String, idSegmSave As String, idTabSave As String
  Dim rs As Recordset, tbred As Recordset
  Dim nomeTRed() As String, nomeSRed() As String, testoRed() As String, testoSel() As String
  Dim flagSel As Boolean, tabFound As Boolean, tabFound2 As Boolean, flagOcc As Boolean
  Dim nomeKeyRed() As Variant, nomeFieldRed() As Variant, idRed() As Variant
  Dim instrLevel As BlkIstrDli
  'SQ D
  Dim testoRedOcc As String, includedMember As String
  
  indentGen = 0
  
  testo = InitIstr("REPL", "N") & vbCrLf
  'SQ - Posso avere pi� UPDATE: WK-SQLCODE in "OR"
  testo = testo & Space(11) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
    
  'TMP:
  'For level = 1 To UBound(TabIstr.BlkIstr)
  For level = UBound(TabIstr.BlkIstr) To 1 Step -1
    'SQ init - tmp: ora � ciclico... pulire quelle che non servono!
    flagSel = False
    tabFound = False
    tabFound2 = False
    flagOcc = False
    testoRedOcc = ""
    includedMember = ""
    
    instrLevel = TabIstr.BlkIstr(level)
    
    flagSel = False
    baseMove = baseMove + instrLevel.SegLen
    
    If level = UBound(TabIstr.BlkIstr) Then
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' SQ - standard o da buttare/sistemare?
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'stefanopippo: 17/08/2005: gestisce le tabelle gi� esistenti evitando
'''MG 240107
'''''      Set rs = m_dllFunctions.Open_Recordset("select * from dmdb2_tabelle where idorigine = " & instrLevel.IdSegm & " and tag = 'OLD'")
      Set rs = m_dllFunctions.Open_Recordset("select * from " & DB & "tabelle where idorigine = " & instrLevel.IdSegm & " and tag = 'OLD'")
      
      If rs.RecordCount Then
         testo = testo & Space(11) & "MOVE ZERO TO SQLCODE WK-SQLCODE." & vbCrLf
         getREPL_DB2_OLD = testo
         Exit Function
      End If
    End If
    
    testo = testo & Space(11) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
    testo = testo & Space(11) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
    testo = testo & Space(11) & "MOVE KRR-" & Replace(instrLevel.pKey, "_", "-") & " TO K01-" & Replace(instrLevel.pKey, "_", "-") & vbCrLf & vbCrLf
      
    'REDEFINES
    indRed = 1
    ReDim testoRed(1)
    ReDim nomeTRed(1)
    ReDim nomeSRed(1)
    ReDim idRed(1)
    ReDim nomeKeyRed(1)
    ReDim nomeFieldRed(1)
    idRed(1) = instrLevel.idTable
    nomeTRed(1) = instrLevel.Tavola
    nomeSRed(1) = instrLevel.Segment
    
    Set tbred = m_dllFunctions.Open_Recordset( _
      "SELECT condizione,rdbms_tbname FROM psdli_segmenti WHERE idsegmento = " & instrLevel.IdSegm)
    If Len(tbred!condizione) Then
      testoRed(1) = getCondizione_Redefines(tbred!condizione & "_lett")
    Else
      testoRed(1) = ""
    End If
    If Len(tbred!rdbms_tbname) Then
      If Left(tbred!rdbms_tbname, 7) = "Storico" Then
         If SwTp Then
            testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_cics_repl")
         Else
            testo = testo & getCondizione_Redefines(tbred!rdbms_tbname & "_repl")
         End If
      End If
    End If
    tbred.Close
'''MG 240107
'''''    Set tbred = m_dllFunctions.Open_Recordset( _
'''''      "SELECT a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
'''''      " FROM mgdli_segmenti as a, dmdb2_tabelle as b, dmdb2_index as c " & _
'''''      " WHERE a.idsegmentoorigine = " & instrLevel.IdSegm & _
'''''      " AND a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
    Set tbred = m_dllFunctions.Open_Recordset( _
      "SELECT a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
      " FROM mgdli_segmenti as a, " & DB & "tabelle as b, " & DB & "index as c " & _
      " WHERE a.idsegmentoorigine = " & instrLevel.IdSegm & _
      " AND a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")

    If tbred.RecordCount Then
      ReDim Preserve testoRed(tbred.RecordCount + 1)
      ReDim Preserve nomeTRed(tbred.RecordCount + 1)
      ReDim Preserve nomeSRed(tbred.RecordCount + 1)
      ReDim Preserve idRed(tbred.RecordCount + 1)
      ReDim Preserve nomeKeyRed(tbred.RecordCount + 1)
      ReDim Preserve nomeFieldRed(tbred.RecordCount + 1)
      While Not tbred.EOF
        indRed = indRed + 1
         
        If Len(tbred!condizione) Then
          'If tbred!condizione = "SELETTORE" Then
          If Not flagSel Then
            Dim tbsel As Recordset
'''MG 240107
'''''            Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
'''''              "from dmdb2_tabelle " & _
'''''              "where idorigine = " & instrLevel.IdSegm & _
'''''              " and TAG like '%DISPATCHER%'")
            Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
              "from " & DB & "tabelle " & _
              "where idorigine = " & instrLevel.IdSegm & _
              " and TAG like '%DISPATCHER%'")

            If Not tbsel.EOF Then
      'tabella selettrice: riposiziona e abblenka
      'stefano: non salva pi� la tabella precedente, perch� in caso di dispatcher � una
      ' tabella inutile; sarebbe meglio anche evitare di crearla in conversione...
  '               idRed(indRed) = idRed(1)
  '               nomeTRed(indRed) = nomeTRed(1)
  '               nomeSRed(indRed) = nomeSRed(1)
  '               testoRed(indRed) = "IF WK-NAMETABLE = '" & nomeTRed(1) & "'"
              testoRed(1) = ""
              idRed(1) = tbsel!idTable
              nomeTRed(1) = tbsel!nome
              nomeSRed(1) = "DISPATCHER"
              indRed = 2
              flagSel = True
              If UBound(instrLevel.KeyDef) > 0 Then
                 nomeKeyRed(1) = instrLevel.KeyDef(1).key
              Else
                 nomeKeyRed(1) = ""
              End If
              'stefano: campo per il nome del field; uso l'idfield della redefines perch� sembra corretto.
              Dim tbred2 As Recordset
              Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
                "FROM psdli_field where idfield = " & tbred!IdField & " and idsegmento = " & instrLevel.IdSegm)
              If Not tbred2.EOF Then
                 nomeFieldRed(1) = tbred2!Nomeidx
              Else
                 m_dllFunctions.WriteLog "Field not found: " & tbred!IdField, "Routines Generation"
                 nomeFieldRed(1) = ""
              End If
            End If
          End If
          If flagSel Then
             testoRed(indRed) = "IF WK-NAMETABLE = '" & tbred!nomeTab & "'"
          Else
             testoRed(indRed) = getCondizione_Redefines(tbred!condizione & "_lett")
          End If
          idRed(indRed) = tbred!idTab
          nomeTRed(indRed) = tbred!nomeTab
          nomeSRed(indRed) = tbred!nomeseg
          nomeKeyRed(indRed) = tbred!nomeindex
          'stefano: campo per il nome del field
          Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
           "from mgdli_field where idfield = " & tbred!IdField & " and idsegmento = " & tbred!idSegmento)
          If Not tbred2.EOF Then
              nomeFieldRed(indRed) = tbred2!Nomeidx
          Else
              m_dllFunctions.WriteLog "Virtual Field not found: " & tbred!IdField, "Routines Generation"
              nomeFieldRed(indRed) = ""
          End If
        Else
           m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
        End If
        tbred.MoveNext
      Wend
      tbred.Close
    End If
    
    'selettore
    Dim Start As Integer
    Start = 1
    If flagSel Then
      Start = 2
      'testo = testo & getCondizione_Redefines(testoSel(1)) & vbCrLf
      'la REPL � sempre monolivello, almeno per le CALL CBLTDLI
      testo = testo & Space(11) & "EXEC SQL SELECT " & vbCrLf _
                 & CostruisciSelect("T1", idRed(1))
      testo = testo & Space(15) & "INTO " & vbCrLf
      testo = testo & CostruisciInto("T1", idRed(1), nomeTRed(1), "REPL")
      testo = testo & Space(14) & "FROM " & vbCrLf
      'fissa: per le REPL non serve altro
      'testo = testo & CostruisciFrom(1, 1, True) & vbCrLf
      'SQ CREATOR (prendo quello della principale...)
      'testo = testo & Space(17) & nomeTRed(1) & " T1" & vbCrLf
      testo = testo & Space(17) & IIf(Len(TabIstr.BlkIstr(1).tableCreator), TabIstr.BlkIstr(1).tableCreator & ".", "") & nomeTRed(1) & " T1" & vbCrLf
      tabSave = TabIstr.BlkIstr(1).Tavola
      idTabSave = TabIstr.BlkIstr(1).idTable
      TabIstr.BlkIstr(1).idTable = idRed(1)
      TabIstr.BlkIstr(1).Tavola = nomeTRed(1)
      indentGen = 0
      testo = testo & Space(14) & CostruisciWhere(1, 1)
      TabIstr.BlkIstr(1).Tavola = tabSave
      TabIstr.BlkIstr(1).idTable = idTabSave
      testo = testo & Space(11) & "END-EXEC" & vbCrLf
      testo = testo & Space(11) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
                                          'exit non funzionante
                                          '     testo = testo & Space(11) & "IF WK-SQLCODE NOT = ZERO" & vbCrLf
                                          '     'testo = testo & Space(11) & "   GO TO " & ExRout & vbCrLf
                                          '     'testo = testo & Space(11) & "   EXIT SECTION" & vbCrLf
                                          '     testo = testo & Space(11) & "   EXIT" & vbCrLf
                                          '     testo = testo & Space(11) & "END-IF" & vbCrLf
      tabFound = False
      Dim IdxTabOk As Integer
      For IdxTabOk = 1 To idxTabOkMax
        If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "REPL" Then
          tabFound = True
          Exit For
        End If
      Next
      If Not tabFound Then
         idxTabOkMax = idxTabOkMax + 1
         ReDim Preserve tabOk(idxTabOkMax)
         ReDim Preserve istrOk(idxTabOkMax)
         tabOk(idxTabOkMax) = nomeTRed(1)
         istrOk(idxTabOkMax) = "REPL"
      End If
      testo = testo & vbCrLf & _
        Space(6) & "*** REDEFINES" & vbCrLf & _
        Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
        Space(11) & "   PERFORM 9999-" & Replace(nomeTRed(1), "_", "-") & "-REPL" & vbCrLf & _
        Space(11) & "END-IF" & vbCrLf & vbCrLf
      
      testoRedOcc = testoRedOcc & Space(7) & "9999-" & Replace(nomeTRed(1), "_", "-") & "-REPL SECTION." & vbCrLf & vbCrLf
      testoRedOcc = testoRedOcc & _
        Space(11) & "MOVE " & nomeFieldRed(1) & " OF KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf & _
        Space(11) & "  TO KRD-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf & _
        Space(6) & "*** REDEFINES" & vbCrLf
      
      'stefano: poi lo aggiustiamo con get environment (bisogna fare il parametro per le nuove copy P: P[tipo-istr][ALIAS]
      includedMember = "PR" & Right(getAlias_tabelle(Int(idRed(1))), 6)
    End If
      
    For K = Start To indRed
      indentRed = 0
      
      If Len(testoRed(K)) Then
        indentRed = 3
        'Redefines:
        testoRedOcc = testoRedOcc & Space(11) & Trim(testoRed(K)) & vbCrLf & _
          Space(11 + indentRed) & "MOVE KRD-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO" & vbCrLf & _
          Space(11 + indentRed) & "     KRD-" & nomeKeyRed(K) & vbCrLf & _
          Space(11 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf & _
          Space(11 + indentRed) & "MOVE KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO KRR-" & nomeKeyRed(K) & vbCrLf & _
          Space(11 + indentRed) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf & _
          Space(11 + indentRed) & "MOVE KRR-" & nomeKeyRed(K) & " TO K01-" & nomeKeyRed(K) & vbCrLf
      End If
      
      'Comune:
      'Differenziazione: Move posizionale!
      If level = 1 Then
        testoRedOcc = testoRedOcc & Space(11 + indentRed) & _
          "MOVE LNKDB-DATA-AREA(1) TO RD-" & nomeSRed(K) & vbCrLf
      Else
        'TMP: AL CONTRARIO non usare baseMove!
        Dim j As Integer
        baseMove = 0
        For j = 1 To level - 1
          baseMove = baseMove + instrLevel.SegLen
        Next
        testoRedOcc = testoRedOcc & Space(11 + indentRed) & _
          "MOVE LNKDB-DATA-AREA(1)(" & baseMove + 1 & ":" & instrLevel.SegLen & ") TO RD-" & nomeSRed(K) & vbCrLf
      End If
      testoRedOcc = testoRedOcc & Space(11 + indentRed) & "PERFORM 9999-" & Replace(nomeTRed(K), "_", "-") & "-TO-RR" & vbCrLf
      
      'SQ 1-08-06 - Le tabelle con OCCURS possono NON avere colonne nella tabella principale
      Dim setStatement As String
      setStatement = CostruisciSet(idRed(K), nomeTRed(K))
      If Len(setStatement) Then
        tabSave = TabIstr.BlkIstr(1).Tavola
        idTabSave = TabIstr.BlkIstr(1).idTable
        TabIstr.BlkIstr(1).idTable = idRed(K)
        TabIstr.BlkIstr(1).Tavola = nomeTRed(K)
        indentGen = indentRed
        testoRedOcc = testoRedOcc & Space(11 + indentRed) & "EXEC SQL UPDATE " & vbCrLf
        testoRedOcc = testoRedOcc & CostruisciFrom(1, 1, True)
        testoRedOcc = testoRedOcc & Space(11 + indentRed) & "    SET " & vbCrLf
        testoRedOcc = testoRedOcc & setStatement
        testoRedOcc = testoRedOcc & CostruisciWhere(1, 1)
        TabIstr.BlkIstr(1).Tavola = tabSave
        TabIstr.BlkIstr(1).idTable = idTabSave
        testoRedOcc = testoRedOcc & Space(11 + indentRed) & "END-EXEC" & vbCrLf
        'SQ
        'testoRedOcc = testoRedOcc & Space(11 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
        testoRedOcc = testoRedOcc & _
          Space(11 + indentRed) & "IF SQLCODE NOT = ZERO" & vbCrLf & _
          Space(11 + indentRed) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf & _
          Space(11 + indentRed) & "END-IF" & vbCrLf
      'SQ
      'Else
      '  testoRedOcc = testoRedOcc & Space(11 + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
      End If
                                'exit non funzionante
                                '      testoRedOcc = testoRedOcc & Space(11 + indentRed) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
                                '      'testoRedOcc = testoRedOcc & Space(14 + indentRed) & "GO TO " & ExRout & vbCrLf
                                '      'testoRedOcc = testoRedOcc & Space(14 + indentRed) & "EXIT SECTION" & vbCrLf
                                '      testoRedOcc = testoRedOcc & Space(14 + indentRed) & "EXIT" & vbCrLf
                                '      testoRedOcc = testoRedOcc & Space(11 + indentRed) & "END-IF" & vbCrLf
      ''''''''''''''
      ' OCCURS
      '''''''''''''
      GbTestoRedOcc2 = ""
      If (Not flagSel) Or K = 1 Then
       Dim tbOcc As Recordset
'''MG 240107
'''''       Set tbOcc = m_dllFunctions.Open_Recordset("select *" & _
'''''           " from dmdb2_tabelle as a, dmdb2_index as b" & _
'''''           " where a.idtable = b.idtable and a.idtablejoin = " & idRed(K) & _
'''''           " and a.TAG like '%OCCURS%' and b.tipo = 'P' order by a.nome")
       Set tbOcc = m_dllFunctions.Open_Recordset("select *" & _
           " from " & DB & "tabelle as a, " & DB & "index as b" & _
           " where a.idtable = b.idtable and a.idtablejoin = " & idRed(K) & _
           " and a.TAG like '%OCCURS%' and b.tipo = 'P' order by a.nome")

       If Not tbOcc.EOF Then
         Dim IdxTabOk2 As Integer
         tabFound2 = False
         For IdxTabOk2 = 1 To idxTabOkMax2
            If tabOk2(IdxTabOk2) = idRed(K) And istrOk2(IdxTabOk2) = "REPL" Then
               tabFound2 = True
               Exit For
            End If
         Next
         If Not tabFound2 Then
            idxTabOkMax2 = idxTabOkMax2 + 1
            ReDim Preserve tabOk2(idxTabOkMax2)
            ReDim Preserve istrOk2(idxTabOkMax2)
            tabOk2(idxTabOkMax2) = idRed(K)
            istrOk2(idxTabOkMax2) = "REPL"
         End If
         flagOcc = False
         If Not tabFound2 Then
          'SQ ! controllare!!!!!!!!!!!!
          'GbTestoRedOcc2 = costruisciOccursIns(idRed(k), indentRed, "REPL", flagOcc, False)
          GbTestoRedOcc2 = Space(7) & "9999-" & Replace(instrLevel.Tavola, "_", "-") & "-REPL" & " SECTION." & vbCrLf & _
                            Space(11 + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf & _
                            costruisciOccursInsOld(idRed(K), indentRed, "REPL", flagOcc, False)
         End If
         If flagOcc Or tabFound2 Then
            testoRedOcc = testoRedOcc & vbCrLf & _
              Space(6) & "*** OCCURS" & vbCrLf & _
              Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
              Space(11) & "   PERFORM 9999-" & Replace(nomeTRed(1), "_", "-") & "-REPL" & vbCrLf & _
              Space(11) & "END-IF" & vbCrLf & vbCrLf
            'stefano: poi lo aggiustiamo con get environment (bisogna fare il
            ' parametro per le nuove copy P: P[tipo-istr][ALIAS]
            includedMember = "PR" & Right(getAlias_tabelle(Int(idRed(1))), 6)
         End If
        End If
      Else
        testoRedOcc = testoRedOcc & costruisciOccursInsOld(idRed(K), indentRed, "REPL", flagOcc, True)
      End If
      
      If Len(testoRed(K)) Then
        testoRedOcc = testoRedOcc & Space(11) & "END-IF" & vbCrLf
      End If
      
      If (Not flagSel) Or K = 1 Then
        testo = testo & testoRedOcc
        GbTestoRedOcc = GbTestoRedOcc2
      Else
        GbTestoRedOcc = testoRedOcc & Space(11) & "." & vbCrLf
      End If
      
      '''''''''''''''''''''''
      ' Scrittura Copy P
      '''''''''''''''''''''''
      If Len(GbTestoRedOcc) Then
        Dim rtbFileSave As String
        If K = Start Then 'SQ tmp... verificare (per non includere N volte in caso di redefines)
          rtbFileSave = rtbFile.text  'salvo
          rtbFile.text = GbTestoRedOcc
          rtbFile.SaveFile VSAM2Rdbms.drPathDb & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\cpy\" & includedMember, 1
          rtbFile.text = rtbFileSave
          GbFileRedOccs(UBound(GbFileRedOccs)) = includedMember
          ReDim Preserve GbFileRedOccs(UBound(GbFileRedOccs) + 1)
        End If
        GbTestoRedOcc = ""  'flag x chiamante
        'transitorio: buttare
        GbFileRedOcc = ""
      End If
      
    Next K
  Next level
    
 'stefanopippo: 17/08/2005: non serve, in quanto la REPL NON RIPUNTA (COSI' COME LA DLET)
 'testo = testo & Space(14) & "PERFORM STORE-FDBKEY THRU STORE-FDBKEY-EX" & vbCrLf
 
  getREPL_DB2_OLD = testo

End Function

Public Function AggRtDLIGen(Optional ByVal wIstrType As String = "GENERIC") As String
   Dim K As Integer
   Dim k1 As Integer
   Dim tb1 As Recordset
   Dim tb2 As Recordset
   
   Dim NomeT As String
   Dim NomePk As String
   Dim nomeKey As String
   Dim NOMEKEYDLI As String
   Dim Aliast As String
   Dim Kt As Integer
   Dim testo As String
   Dim andOr As String
   
   Dim testoSel As String
   Dim TestoInto As String
   Dim TestoFrom As String
   Dim TestoWhere As String
   Dim wExists As Boolean
   
   Dim arMoveFinale() As Long
   
   Dim Operat As String
  
   ReDim arMoveFinale(0)

' stefano!!!!!
   If Trim(TabIstr.BlkIstr(1).Segment) = "" And Trim(UCase(wIstrType)) <> "GNP" And TabIstr.BlkIstr(1).IdSegm <> -1 Then
      m_dllFunctions.WriteLog "AggRtDLIGen : Segmento non referenziato..."
      Exit Function
   End If
   
   testo = testo & Space(6) & vbCrLf
   
   'SG : Vecchia versione x banca
   If Not swCall Then
      'If Trim(UCase(TabIstr.PCB)) <> "" Then
       '  testo = testo & Space(11) & "MOVE LNKDB-NUMPCB TO WK-NUMPCB." & vbCrLf
      'End If
   End If
   
   If Not swCall Then
      For K = 1 To UBound(TabIstr.BlkIstr)
           wExists = False
           
           If Trim(TabIstr.BlkIstr(K).Record) <> "" Then
               testo = testo & Space(11) & "MOVE LNKDB-DATA-AREA  (" & K & ")   TO WK-DATA-AREA" & K & "." & vbCrLf
               
               ReDim Preserve arMoveFinale(UBound(arMoveFinale) + 1)
               arMoveFinale(UBound(arMoveFinale)) = K
               
               wExists = True
           End If
           
           If Trim(TabIstr.BlkIstr(K).SegLen) <> "" Then
               testo = testo & Space(11) & "MOVE LNKDB-AREALENGTH (" & K & ")   TO WK-AREALENGTH" & K & "." & vbCrLf
               
               wExists = True
           End If
      ' stefano!!!!!
           If TabIstr.BlkIstr(K).IdSegm = -1 Then
              testo = testo & Space(11) & "MOVE LNKDB-SEGNAME   (" & K & ")    TO WK-SEGNAME" & K & "." & vbCrLf
           End If
           If UBound(TabIstr.BlkIstr(K).KeyDef) > 0 Then
               If Trim(TabIstr.BlkIstr(K).KeyDef(1).KeyVal) <> "" Then
                   testo = testo & Space(11) & "MOVE LNKDB-KEYVALUE   (" & K & ",1) TO WK-KEYVALUE" & K & "." & vbCrLf
               End If
               
               If Trim(TabIstr.BlkIstr(K).KeyDef(1).KeyLen) <> "" Then
                   testo = testo & Space(11) & "MOVE LNKDB-KEYLENGTH  (" & K & ",1) TO WK-KEYLENGTH" & K & "." & vbCrLf
               End If
               
               'Implementare per le booleane
               If UBound(TabIstr.BlkIstr(K).KeyDef) > 1 Then
                   
               End If
               
               wExists = True
           End If
           
           If wExists Then
               testo = testo & Space(6) & vbCrLf
           End If
      Next K
   End If
   
   If Not swCall Then
     testo = testo & Space(6) & vbCrLf
     testo = testo & Space(11) & "EXEC DLI " & TabIstr.Istr & " USING PCB (WK-NUMPCB)" & vbCrLf
     
     For K = 1 To UBound(TabIstr.BlkIstr)
           If Trim(TabIstr.BlkIstr(K).Segment) <> "" Then
             testo = testo & Space(19) & "SEGMENT (" & TabIstr.BlkIstr(K).Segment & ")" & vbCrLf
           ElseIf TabIstr.BlkIstr(K).IdSegm = -1 Then
             testo = testo & Space(19) & "SEGMENT ((WK-SEGNAME" & K & "))" & vbCrLf
           End If
           
           If Trim(Replace(Replace(TabIstr.BlkIstr(K).Search, "*", ""), "-", "")) <> "" Then
             'testo = testo & Space(19) & Trim(TabIstr.BlkIstr(k).Search) & vbCrLf
             testo = testo & Space(19) & Trim(Replace(Replace(TabIstr.BlkIstr(K).Search, "*", ""), "-", "")) & vbCrLf
           End If
           
           If Trim(TabIstr.BlkIstr(K).Record) <> "" Then
             Select Case Trim(UCase(wIstrType))
                 Case "GENERIC", "GU", "GN", "GNP"
                     testo = testo & Space(19) & "INTO (WK-DATA-AREA" & (K) & ")" & vbCrLf
                 Case "ISRT", "REPL", "DLET"
                     testo = testo & Space(19) & "FROM (WK-DATA-AREA" & (K) & ")" & vbCrLf
             End Select
           End If
           If Trim(TabIstr.BlkIstr(K).SegLen) <> "" Then
             testo = testo & Space(19) & "SEGLENGTH (WK-AREALENGTH" & (K) & ")" & vbCrLf
           End If
           
           For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
             If Trim(TabIstr.BlkIstr(K).KeyDef(k1).key) <> "" Then
               nomeKey = TabIstr.BlkIstr(K).KeyDef(k1).key
               NOMEKEYDLI = nomeKey
      ' ginevra: non legge pi� le db2
      'vecchia         Set Tb1 = m_dllFunctions.Open_Recordset("select* from dmdb2_index where idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(k).IdSegm & ") and nome = '" & NomeKey & "' order by idindex")
               
      'vecchia         If Tb1.RecordCount = 0 Then
                  If TabIstr.BlkIstr(K).KeyDef(k1).xName Then
                     nomeKey = TabIstr.BlkIstr(K).KeyDef(k1).NameAlt
                     Set tb1 = m_dllFunctions.Open_Recordset("select * from PsDli_Field Where IdSegmento = " & TabIstr.BlkIstr(K).IdSegm & " And Nome = '" & nomeKey & "'")
                  End If
      'vecchia         Else
      'vecchia            Set tb2 = m_dllFunctions.Open_Recordset("select * from psdli_field where idfield = " & Tb1!idorigine)
      'vecchia            If tb2.RecordCount > 0 Then
      'vecchia               NomeKeyDli = tb2!nome
      'vecchia            End If
      'vecchia         End If
               
               '*************************************************************************
               If Trim(nomeKey = TabIstr.BlkIstr(K).KeyDef(k1).key) <> "" Then
                 'Controllare parser e migrazione DLI-->DB2 deve valorizzare xName a true
                 'QUESTO � UNA MODIFICA FUNZIONANTE MA NON PIENAMENTE CORRETTA
                 nomeKey = TabIstr.BlkIstr(K).KeyDef(k1).NameAlt
               End If
               '**************************************************************************
               
               If Trim(nomeKey) <> "" Then
                 testo = testo & Space(19) & "WHERE (" & nomeKey & Trim(TabIstr.BlkIstr(K).KeyDef(k1).Op) & "WK-KEYVALUE" & (K) & ")" & vbCrLf
                 testo = testo & Space(19) & "FIELDLENGTH (WK-KEYLENGTH" & (K) & ")" & vbCrLf
               End If
             End If
           Next k1
      
     Next K
   
     'ginevra: keyfeedback
     If TabIstr.keyfeedback <> "" Then
        testo = testo & Space(19) & "KEYFEEDBACK (WK-FDBKEY)" & vbCrLf
     End If
        
     testo = testo & Space(11) & "END-EXEC." & vbCrLf
    
     testo = testo & Space(6) & vbCrLf
   Else 'SG : Sviluppa in call
      testo = testo & Space(6) & vbCrLf
      
      testo = testo & Space(11) & "MOVE LNKDB-OPERATION(1:4) TO WK-OPERATION." & vbCrLf
      testo = testo & Space(11) & "INSPECT WK-OPERATION REPLACING ALL '.' BY SPACE." & vbCrLf
      
      testo = testo & Space(6) & vbCrLf
     
      For K = 1 To UBound(TabIstr.BlkIstr)
         If Trim(TabIstr.BlkIstr(K).Record) <> "" Then
            testo = testo & Space(11) & "MOVE LNKDB-DATA-AREA(" & K & ") TO WK-DATA-AREA" & K & "." & vbCrLf
            'alpitour: 5/8/2005: chi cavolo l'ha tolta????
            ReDim Preserve arMoveFinale(UBound(arMoveFinale) + 1)
            arMoveFinale(UBound(arMoveFinale)) = K
         End If
         
         'perch� sei tu Romeo?
         'If Trim(TabIstr.BlkIstr(k).SSAName) <> "" Then
         If Trim(TabIstr.BlkIstr(K).ssaName) <> "" And Trim(TabIstr.BlkIstr(K).ssaName) <> "" Then
' alpitour 5/8/2005: ahiahi matteo
            testo = testo & Space(11) & "MOVE LNKDB-SSA-AREA(" & K & ") TO WK-SSA-AREA" & K & vbCrLf
'            testo = testo & Space(11) & "MOVE LNKDB-KEYVALUE(" & k & ", 1) TO WK-SSA-AREA" & k & vbCrLf
         End If
      Next K
      
      testo = testo & Space(6) & vbCrLf
     
      testo = testo & Space(11) & "CALL 'CBLTDLI' USING WK-OPERATION " & vbCrLf
      
      testo = testo & Space(25) & "PCB-AREA" & vbCrLf
      
      For K = 1 To UBound(TabIstr.BlkIstr)
         If Trim(TabIstr.BlkIstr(K).Record) <> "" Then
            testo = testo & Space(25) & "WK-DATA-AREA" & K & vbCrLf
         End If
      Next K
      
      For K = 1 To UBound(TabIstr.BlkIstr)
         'If Trim(TabIstr.BlkIstr(k).SSAName) <> "" Then
         If Trim(TabIstr.BlkIstr(K).ssaName) <> "" And Trim(TabIstr.BlkIstr(K).ssaName) <> "" Then
            testo = testo & Space(25) & "WK-SSA-AREA" & K & vbCrLf
         End If
      Next K
      
'      If wPunto Then
         testo = testo & Space(25) & "." & vbCrLf
 '     End If
      
      testo = testo & Space(6) & vbCrLf
   End If
   
   If UBound(arMoveFinale) > 0 Then
      For K = 1 To UBound(arMoveFinale)
         Select Case Trim(UCase(wIstrType))
            Case "GENERIC", "GU", "GN", "GNP", "GHU", "GHN", "GHNP"
               testo = testo & Space(11) & "MOVE WK-DATA-AREA" & arMoveFinale(K) & " TO LNKDB-DATA-AREA(" & arMoveFinale(K) & ")." & vbCrLf
               testo = testo & Space(6) & vbCrLf
             
         End Select
      Next K
   End If
   
   'ginevra: keyfeedback
   If TabIstr.keyfeedback <> "" Then
      testo = testo & Space(11) & "MOVE WK-FDBKEY TO LNKDB-FDBKEY." & vbCrLf
      testo = testo & Space(6) & vbCrLf
   End If
      
   AggRtDLIGen = testo
End Function

Public Function getUTIL_DB2(Optional ByVal wIstrType As String) As String
   Dim testo As String
   Dim K As Long
   
   If Not swCall Then
      Select Case Trim(UCase(wIstrType))
         Case "PCB"
            testo = testo & Space(11) & "MOVE LNKDB-PSBNAME TO WK-PSBNAME." & vbCrLf
            testo = testo & Space(6) & vbCrLf
            testo = testo & Space(11) & "EXEC DLI SCHEDULE PSB ((WK-PSBNAME))" & vbCrLf
            testo = testo & Space(11) & "END-EXEC." & vbCrLf
            testo = testo & Space(6) & vbCrLf
            'SP exec dli
            testo = testo & Space(11) & "INITIALIZE LNKDB-ACTIVE." & vbCrLf
            testo = testo & Space(6) & vbCrLf
            
            getUTIL_DB2 = testo
         
         Case "TERM"
            testo = testo & Space(11) & "EXEC DLI TERM" & vbCrLf
            testo = testo & Space(11) & "END-EXEC." & vbCrLf
            testo = testo & Space(6) & vbCrLf
            'SP exec dli
            If SwTp Then
               testo = testo & Space(11) & "EXEC CICS SYNCPOINT END-EXEC." & vbCrLf
            Else
               testo = testo & Space(11) & "EXEC SQL COMMIT END-EXEC." & vbCrLf
            End If
            
            testo = testo & Space(6) & vbCrLf
            
            getUTIL_DB2 = testo
            
         Case "SYNC"
         
         Case "QRY", "QUERY"
            testo = testo & Space(11) & "MOVE LNKDB-NUMPCB TO WK-NUMPCB." & vbCrLf
            testo = testo & vbCrLf
            testo = testo & Space(11) & "EXEC DLI QUERY USING PCB (WK-NUMPCB)" & vbCrLf
            testo = testo & Space(11) & "END-EXEC." & vbCrLf
            testo = testo & vbCrLf
            
            getUTIL_DB2 = testo
            
         Case "CHKP"
            testo = testo & Space(11) & "MOVE LNKDB-CHKPID TO WK-CHKPID." & vbCrLf
            testo = testo & vbCrLf
            testo = testo & Space(11) & "EXEC DLI CHKP ID (WK-CHKPID)" & vbCrLf
            testo = testo & Space(11) & "END-EXEC." & vbCrLf
            testo = testo & vbCrLf
            
            getUTIL_DB2 = testo
            
      End Select
   Else 'SG: Sviluppo in call
      
      testo = testo & Space(11) & "MOVE LNKDB-OPERATION(1:4) TO WK-OPERATION." & vbCrLf
      testo = testo & Space(11) & "INSPECT WK-OPERATION REPLACING ALL '.' BY SPACE." & vbCrLf
      
      Select Case Trim(UCase(wIstrType))
         Case "PCB"
            testo = testo & Space(11) & "MOVE LNKDB-PSBNAME TO WK-PSBNAME." & vbCrLf
            testo = testo & Space(6) & vbCrLf
            testo = testo & Space(11) & "CALL 'CBLTDLI' USING WK-OPERATION" & vbCrLf
            testo = testo & Space(11) & "                     WK-PSBNAME" & vbCrLf
            
            testo = testo & Space(11) & "                     ADDRESS OF DLIUIB." & vbCrLf
            testo = testo & Space(6) & vbCrLf
            testo = testo & Space(11) & "SET LNKDB-PTRPCB  TO ADDRESS OF DLIUIB." & vbCrLf
            testo = testo & Space(6) & vbCrLf
            'SG : DB2
            testo = testo & Space(11) & "INITIALIZE LNKDB-ACTIVE." & vbCrLf
            
            testo = testo & Space(6) & vbCrLf
            
            getUTIL_DB2 = testo
         
         Case "TERM"
            testo = testo & Space(11) & "CALL 'CBLTDLI' USING WK-OPERATION." & vbCrLf
            testo = testo & Space(6) & vbCrLf
            
            'SG : DB2
            If SwTp Then
               testo = testo & Space(11) & "EXEC CICS SYNCPOINT END-EXEC." & vbCrLf
            Else
               testo = testo & Space(11) & "EXEC SQL COMMIT END-EXEC." & vbCrLf
            End If
            
            testo = testo & Space(6) & vbCrLf
             
            getUTIL_DB2 = testo
            
         Case "SYNC"
            testo = testo & Space(6) & vbCrLf
            testo = testo & Space(11) & "SET ADDRESS OF PCB-AREA TO LNKDB-PTRPCB" & vbCrLf
            testo = testo & Space(6) & vbCrLf
            testo = testo & Space(11) & "CALL 'CBLTDLI' USING WK-OPERATION" & vbCrLf
            testo = testo & Space(11) & "                     PCB-AREA." & vbCrLf
            
            testo = testo & Space(6) & vbCrLf
            'SG : DB2
            testo = testo & Space(11) & "INITIALIZE LNKDB-ACTIVE." & vbCrLf
            
            testo = testo & Space(6) & vbCrLf
            
            getUTIL_DB2 = testo
            
         Case "QRY", "QUERY"
            Stop
           getUTIL_DB2 = testo
         Case "CHKP"
            testo = testo & Space(6) & vbCrLf
            testo = testo & Space(11) & "INITIALIZE WK-DATA-AREA1" & vbCrLf
            testo = testo & Space(6) & vbCrLf
            testo = testo & Space(11) & "SET ADDRESS OF PCB-AREA TO LNKDB-PTRPCB" & vbCrLf
            testo = testo & Space(11) & "CALL 'CBLTDLI' USING WK-OPERATION" & vbCrLf
            testo = testo & Space(11) & "                     PCB-AREA" & vbCrLf
            testo = testo & Space(11) & "                     WK-DATA-AREA1." & vbCrLf
            testo = testo & Space(6) & vbCrLf
            'SG : DB2
            If SwTp Then
               testo = testo & Space(11) & "EXEC CICS SYNCPOINT END-EXEC." & vbCrLf
            Else
               testo = testo & Space(11) & "EXEC SQL COMMIT END-EXEC." & vbCrLf
            End If
            
            testo = testo & Space(6) & vbCrLf
            
            getUTIL_DB2 = testo
            
      End Select
      
      testo = testo & Space(11) & "SET LNKDB-PTRPCB  TO ADDRESS OF DLIUIB." & vbCrLf
   End If
End Function
'
'Public Function AggRtDb2Gu(Optional ByVal withHold As Boolean = False) As String
'End Function

Public Function getCopyDLISeg(Index As Long) As String
   getCopyDLISeg = TabIstr.BlkIstr(Index).Segment
End Function
'SQ 29-08
Public Function CursorName() As String
  'SP 9/9 pcb dinamico
  CursorName = "C" & GbNumIstr & suffDynT
  If GbNumDial > 1 Then
    CursorName = CursorName & Trim(Str(GbAttDial))
  End If
  'SQ D
  If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then CursorName = CursorName & level
End Function
'SQ
Public Function ExRout() As String
  'SP 9/9 pcb dinamico
  ExRout = "ROUT-" & Gistruzione & suffDynT & "-EX"
  'Dim testo As String
  
  'testo = "ROUT-" & Gistruzione & "-EX"
  'If GbNumDial > 1 Then
  '  testo = testo & Trim(Str(GbAttDial))
  'End If
  
  'ExRout = testo
End Function

'Public Function PuntaRecDb2() As String
'   Dim k As Long
'   Dim k1 As Integer
'   Dim tb1 As Recordset
'   Dim tb2 As Recordset
'
'   Dim NomeT As String
'   Dim NomePk As String
'
'   Dim nomeKey As String
'   Dim NomeKeyDLI As String
'
'   Dim AliasT As String
'   Dim Kt As Integer
'   Dim testo As String
'   Dim andOr As String
'
'   Dim TestoSel As String
'   Dim TestoInto As String
'   Dim TestoFrom As String
'   Dim TestoWhere As String
'
'   Dim Operat As String
'   Dim SwWhere As Boolean
'   Dim wBottom As Integer
'
'   Dim wLiv01Copy As String
'
'   SwWhere = False
'   For k = 1 To UBound(TabIstr.BlkIstr)
'       If UBound(TabIstr.BlkIstr(k).keyDef) > 0 Then
'         If TabIstr.BlkIstr(k).keyDef(1).key <> "" Then
'            SwWhere = True
'            wBottom = k
'         End If
'       End If
'   Next k
'
'   If SwWhere = False Then
'      PuntaRecDb2 = ""
'      Exit Function
'   End If
'
'   TestoSel = TestoSel & Space(6) & "* " & vbCrLf & Space(13) _
'                                  & "EXEC SQL DECLARE " & CursorName & " CURSOR FOR SELECT " & vbCrLf _
'                                  & CostruisciSelect(1, wBottom, True)
'
'   'stefanopippo: 13/08/2005: necessario tipo per gestire isrt diversa, modifica il metodo se vuoi
'   TestoInto = Space(15) & "INTO " & vbCrLf _
'                          & CostruisciInto(1, wBottom, True, "GU")
'
'   TestoFrom = Space(15) & "FROM " & vbCrLf _
'                          & CostruisciFrom(1, wBottom, True)
'
'
'   'stefanopippo: 15/08/2005: ci sto provando....
'   'TestoWhere = Space(15) & "WHERE " & vbCrLf
'   '                        & CostruisciWhere(1, wBottom)
'    TestoWhere = CostruisciWhere(1, wBottom)
'
'   testo = testo & Space(6) & "*" & vbCrLf
'
'   For k = 1 To wBottom
'
'      Kt = k
'      NomeT = TabIstr.BlkIstr(Kt).Tavola
'      NomePk = TabIstr.BlkIstr(Kt).PKey
'
'      testo = testo & Space(11) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
'
'      For k1 = 1 To UBound(TabIstr.BlkIstr(k).keyDef)
'        If Trim(TabIstr.BlkIstr(k).keyDef(k1).key) <> "" Then
'          nomeKey = TabIstr.BlkIstr(k).keyDef(k1).key
'
'          NomeKeyDLI = nomeKey
'
'          Set tb1 = m_dllFunctions.Open_Recordset("select* from dmdb2_index where idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(k).idSegm & ") and nome = '" & nomeKey & "' order by idindex")
'
'          If tb1.RecordCount = 0 Then
'            If TabIstr.BlkIstr(k).keyDef(k1).xName Then
'               nomeKey = TabIstr.BlkIstr(k).keyDef(k1).NameAlt
'               Set tb1 = m_dllFunctions.Open_Recordset("select* from dmdb2_index where idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(k).idSegm & ") and nome = '" & nomeKey & "' order by idindex")
'            End If
'          Else
'            If Not IsNull(tb1!idorigine) Then
'               Set tb2 = m_dllFunctions.Open_Recordset("select * from psdli_field where idfield = " & tb1!idorigine)
'               If tb2.RecordCount > 0 Then
'                  NomeKeyDLI = tb2!nome
'               End If
'            End If
'          End If
'
'          If tb1.RecordCount = 0 Then
'             testo = testo & Space(11) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(k)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
'
'             testo = testo & Space(16) & "K01-" & TabIstr.BlkIstr(k).PKey & vbCrLf
'
'             testo = testo & Space(11) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
'             testo = testo & Space(11) & "PERFORM SETC-WKEYWKEY THRU SETC-WKEYWKEY-EX " & vbCrLf
'             testo = testo & Space(11) & "MOVE KRR-" & NomePk & " TO K" & Format(TabIstr.BlkIstr(k).keyDef(k1).keyNum, "00") & "-" & NomePk & vbCrLf
'          Else
'             testo = testo & Space(11) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(k)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
'
'             testo = testo & Space(16) & "K01-" & TabIstr.BlkIstr(k).PKey & vbCrLf
'
'             testo = testo & Space(11) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
'             testo = testo & Space(11) & "PERFORM SETC-WKEYWKEY THRU SETC-WKEYWKEY-EX " & vbCrLf
'             testo = testo & Space(11) & "MOVE KRR-" & NomePk & " TO K" & Format(TabIstr.BlkIstr(k).keyDef(k1).keyNum, "00") & "-" & NomePk & vbCrLf
'
'          End If
'        End If
'      Next k1
'   Next k
'
'   testo = testo & TestoSel & TestoFrom & TestoWhere
'
'   testo = testo & Space(11) & "END-EXEC." & vbCrLf
'
'   testo = testo & Space(6) & "* " & vbCrLf
'   testo = testo & Space(13) & "EXEC SQL OPEN " & CursorName & vbCrLf
'   testo = testo & Space(13) & "END-EXEC" & vbCrLf
'   testo = testo & Space(6) & "* " & vbCrLf
'
'   testo = testo & Space(13) & "IF SQLCODE = -502" & vbCrLf
'   testo = testo & Space(16) & "EXEC SQL CLOSE " & CursorName & vbCrLf
'   testo = testo & Space(16) & "END-EXEC" & vbCrLf
'   testo = testo & Space(16) & "EXEC SQL OPEN " & CursorName & vbCrLf
'   testo = testo & Space(16) & "END-EXEC" & vbCrLf
'   testo = testo & Space(13) & "END-IF" & vbCrLf
'
'   testo = testo & Space(13) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
'   testo = testo & Space(6) & vbCrLf
'
'   testo = testo & Space(13) & "IF WK-SQLCODE NOT EQUAL ZERO THEN" & vbCrLf
'   testo = testo & Space(16) & "GO TO " & ExRout & vbCrLf
'   testo = testo & Space(13) & "END-IF" & vbCrLf
'
'   testo = testo & Space(6) & vbCrLf
'
'   'Per ALPITOUR filtro REPL
'   If Mid(Gistruzione, 1, 4) <> "REPL" Then
'    GbTestoFetch = Space(6) & "*    FETCH RELATIVA A ROUT-" & Gistruzione & vbCrLf
'    GbTestoFetch = GbTestoFetch & Space(7) & "FETCH-" & CursorName & "." & vbCrLf
'
'    GbTestoFetch = GbTestoFetch & Space(6) & "* " & vbCrLf & Space(11) _
'                                & "EXEC SQL FETCH " & CursorName & vbCrLf
'
'    GbTestoFetch = GbTestoFetch & TestoInto
'    GbTestoFetch = GbTestoFetch & Space(11) & "END-EXEC." & vbCrLf & vbCrLf
'    GbTestoFetch = GbTestoFetch & Space(7) & "FETCH-" & CursorName & "-EX." & vbCrLf
'    GbTestoFetch = GbTestoFetch & Space(11) & "EXIT." & vbCrLf & vbCrLf
'   Else
'    GbTestoFetch = ""
'   End If
'
'   testo = testo & Space(11) & "PERFORM FETCH-" & CursorName & " THRU " & vbCrLf
'   testo = testo & Space(16) & "FETCH-" & CursorName & "-EX." & vbCrLf
'
'   testo = testo & Space(11) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
'   testo = testo & Space(6) & vbCrLf
'
'   testo = testo & Space(11) & "EXEC SQL CLOSE " & CursorName & vbCrLf
'   testo = testo & Space(11) & "END-EXEC" & vbCrLf
'
'   testo = testo & Space(6) & vbCrLf
'
'   testo = testo & Space(11) & "IF WK-SQLCODE NOT EQUAL ZERO THEN" & vbCrLf
'   testo = testo & Space(13) & "GO TO " & ExRout & vbCrLf
'   testo = testo & Space(11) & "END-IF" & vbCrLf
'
'   testo = testo & Space(6) & "* " & vbCrLf
'
'   For k = 1 To UBound(TabIstr.BlkIstr)
'      NomeT = TabIstr.BlkIstr(k).Tavola
'      NomePk = TabIstr.BlkIstr(k).PKey
'
'      If TabIstr.BlkIstr(k).idSegm <> 0 And TabIstr.BlkIstr(k).keyDef(1).key <> "" Then
'         wLiv01Copy = getCopyDLISeg(k)
'         testo = testo & Space(11) & "MOVE '" & NomeT & "' TO WK-NAMETABLE " & vbCrLf
'         testo = testo & Space(11) & "PERFORM " & TabIstr.BlkIstr(k).Segment & "-TO-RD THRU " & TabIstr.BlkIstr(k).Segment & "-TO-RD-EX" & vbCrLf
'         testo = testo & Space(11) & "MOVE RD-" & wLiv01Copy & " TO LNKDB-DATA-AREA(" & Format(k) & ")" & vbCrLf
'         testo = testo & Space(11) & "PERFORM SETK-ADLWKEY THRU SETK-ADLWKEY-EX" & vbCrLf
'         testo = testo & Space(6) & "* " & vbCrLf
'      End If
'   Next k
'   If Trim(TabIstr.BlkIstr(1).keyDef(1).xName) = True Then
'      testo = testo & Space(11) & "PERFORM SETA-" & GbKeySecName & " THRU SETA-" & GbKeySecName & "-EX" & vbCrLf
'      testo = testo & Space(11) & "MOVE KRR-" & GbKeySecName & " TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)" & vbCrLf
'   Else
'      testo = testo & Space(11) & "PERFORM STORE-FDBKEY THRU STORE-FDBKEY-EX." & vbCrLf
'   End If
'   testo = testo & Space(6) & vbCrLf
'
'   PuntaRecDb2 = testo
'
'End Function

Public Function CostruisciFrom(indStart As Integer, indEnd As Integer, SwAlias As Boolean) As String
  Dim K As Integer
  Dim testo As String, Aliast As String
  Dim tb1 As Recordset
  Dim tableOk As Boolean

  For K = indStart To indEnd
    '''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' ULTIMA TABELLA: SERVE SEMPRE
    ' ALTRE: JOIN SE ABBIAMO CHIAVI SECONDARIE
    '''''''''''''''''''''''''''''''''''''''''''''''''''''
    'ATTENZIONE: IGNORIAMO LE BOOLEANE
    If K = indEnd Then
      tableOk = True
    Else
      If UBound(TabIstr.BlkIstr(K).KeyDef) Then
        'SQ
        'tableOk = TabIstr.BlkIstr(k).KeyDef(1).key <> TabIstr.BlkIstr(k).KeyDef(1).NameAlt
        tableOk = TabIstr.BlkIstr(K).KeyDef(1).key <> TabIstr.BlkIstr(K).pKey
      Else
        tableOk = False
      End If
    End If
    If tableOk Then
      Aliast = "T" & Format(K, "#0")
      'SQ 28-07-06
      'Non rileggiamo...
      'Set tb1 = m_dllFunctions.Open_Recordset("select Creator from dmdb2_tabelle where nome = '" & TabIstr.BlkIstr(k).Tavola & "'")
      'm_author = ""
      'If Not tb1.EOF Then
      '   If Len(tb1!Creator) Then
      '      m_author = tb1!Creator & "."
      '   End If
      'End If
      'testo = testo & Space(14 + indentGen) & Trim(m_author) & TabIstr.BlkIstr(k).Tavola
      
      'SQ - TMP PEZZA RAPIDA: IN ALCUNI CASI NON ARRIVA VALORIZZATO "tableCreator" (saranno gli scamuffi vari...)
      ' => rileggo
'''MG 240107
'''''      Set tb1 = m_dllFunctions.Open_Recordset("select Creator from dmdb2_tabelle where nome = '" & TabIstr.BlkIstr(K).Tavola & "'")
      ' Mauro 28-03-2007 : eliminazione CREATOR
''      Set tb1 = m_dllFunctions.Open_Recordset("select Creator from " & DB & "tabelle where nome = '" & TabIstr.BlkIstr(K).Tavola & "'")
''
''      If tb1.RecordCount Then
''        TabIstr.BlkIstr(K).tableCreator = tb1!creator & ""
''      End If
''      tb1.Close
''      testo = testo & Space(14 + indentGen) & IIf(Len(TabIstr.BlkIstr(K).tableCreator), TabIstr.BlkIstr(K).tableCreator & ".", "") & TabIstr.BlkIstr(K).Tavola
      testo = testo & Space(14 + indentGen) & TabIstr.BlkIstr(K).Tavola
      If SwAlias Then
         testo = testo & " " & Aliast
      End If
      If K <> indEnd Then
         testo = testo & " ,"
      End If
      testo = testo & vbCrLf
    End If
  Next K
        
   
  CostruisciFrom = testo
End Function
'''''''''''''''''''''''''''''''''''''''''''
' SQ 27-07-06
' ...
'''''''''''''''''''''''''''''''''''''''''''
Public Function CostruisciFrom_D() As String
  Dim i As Integer
  Dim testo As String, alias As String
  
  For i = 1 To UBound(TabIstr.BlkIstr)
    'tmp
    alias = "T" & i
    'Set tb1 = m_dllFunctions.Open_Recordset("select Creator from dmdb2_tabelle where nome = '" & TabIstr.BlkIstr(k).Tavola & "'")
    ' Mauro 28-03-2007 : Eliminazione CREATOR
    'testo = testo & Space(14 + indentGen) & IIf(Len(TabIstr.BlkIstr(i).tableCreator), TabIstr.BlkIstr(i).tableCreator & ".", "") & TabIstr.BlkIstr(i).Tavola
    testo = testo & Space(14 + indentGen) & TabIstr.BlkIstr(i).Tavola
    testo = testo & " " & alias
    If i < UBound(TabIstr.BlkIstr) Then
      testo = testo & " ,"
    End If
    testo = testo & vbCrLf
  Next
   
  CostruisciFrom_D = testo
End Function

Public Function SeRepSelda() As String
'...
End Function
'stefanopippo: 13/08/2005: necessario tipo per gestire isrt diversa, modifica il metodo se vuoi
'stefanopippo: tolto anche qui indstart ed indend, va di pari passo con costruiscisel
'se necessaria una select su pi� tabelle lo rimettiamo in seguito
Public Function CostruisciInto(Aliast As String, idTab As Variant, nomeTab As String, tipoop As String) As String
  Dim k1 As Integer
  Dim rsColonne As Recordset
  Dim testo As String
  'buttare via:
  Dim nomeCampo As String, nomeArea As String
  
'''MG 240107
'''''  Set rsColonne = m_dllFunctions.Open_Recordset("select nome,[null] from dmdb2_colonne where idtable = " & idTab & " Order By ordinale")
  Set rsColonne = m_dllFunctions.Open_Recordset("select nome,[null] from " & DB & "colonne where idtable = " & idTab & " Order By ordinale")
  
  For k1 = 1 To rsColonne.RecordCount
    'SQ - non ho il parametro d'environment per il campo RR?!
    testo = testo & Space(22) & ":" & "RR-" & Replace(nomeTab, "_", "-") & "." & Replace(rsColonne!nome, "_", "-")
      
    If rsColonne!Null Then
      testo = testo & vbCrLf & Space(27) & ":" & "WK-I-" & Replace(rsColonne!nome, "_", "-")
    End If
      
    '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
    testo = testo & IIf(k1 < rsColonne.RecordCount Or indPart, ",", "") & vbCrLf
    rsColonne.MoveNext
  Next
  rsColonne.Close
  CostruisciInto = testo
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ - Farne una unica (parametrica, con parametri vettoriali...)
' Non ha parametri: l'input � la TabIstr
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function CostruisciInto_D() As String
  Dim rsColonne As Recordset
  Dim testo, nomeCampo As String, nomeArea As String
  Dim i As Integer, j As Integer
  
  For i = 1 To UBound(TabIstr.BlkIstr)
'''MG 240107
'''''    Set rsColonne = m_dllFunctions.Open_Recordset("select nome,[null] from dmdb2_colonne where idtable = " & TabIstr.BlkIstr(i).idTable & " Order By ordinale")
    Set rsColonne = m_dllFunctions.Open_Recordset("select nome,[null] from " & DB & "colonne where idtable = " & TabIstr.BlkIstr(i).idTable & " Order By ordinale")
    If rsColonne.RecordCount Then
      For j = 1 To rsColonne.RecordCount
        nomeCampo = Left(Replace(rsColonne!nome, "_", "-") & Space(20), 19)
        nomeArea = "RR-" & Replace(TabIstr.BlkIstr(i).Tavola, "_", "-")   'SQ - non ho il parametro d'environment?!
        
        testo = testo & Space(20) & ":" & nomeArea & "." & nomeCampo
        If rsColonne!Null Then
          testo = testo & vbCrLf & Space(27) & ":" & "WK-I-" & nomeCampo
        End If
        testo = testo & IIf(i < UBound(TabIstr.BlkIstr) Or j < rsColonne.RecordCount, ",", "") & vbCrLf
        
        rsColonne.MoveNext
      Next
    End If
    rsColonne.Close
  Next
  CostruisciInto_D = testo
End Function
'stefanopippo: tolto indstart ed indend, tanto era stato annullato e comunque veniva
'gestito male: la banca ha 5 istruzioni con doppia area, alpitour 3; meglio farle a mano,
'se dobbiamo rimetterlo, bisogna prima di tutto impostare l'informazione in tabistr,
'ricordarsi la dettliv c'� sempre anche se l'area non � richiesta.
Public Function CostruisciSelect(Aliast As String, idTable As Variant) As String
  Dim rs As Recordset
  Dim testo As String
  Dim i As Integer
  
  'SQ 27-06-06 - Utilizzando idOrigine tiro su occurs/redefines...
  'set Tb1 = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idtable in (select idtable from dmdb2_tabelle where idorigine = " & IdSegm & ") Order by ordinale")
  'stefano: 1/7/6 cambio, visto che la tabella selettore non ha pi� un segmento origine
  'Set Tb1 = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idtable in (select idtable from dmdb2_tabelle where idorigine = " & IdSegm & " and TAG='AUTOMATIC') Order by ordinale")
'''MG 240107
'''''  Set rs = m_dllFunctions.Open_Recordset("select nome from dmdb2_colonne where idtable = " & idTable & " Order by ordinale")
  Set rs = m_dllFunctions.Open_Recordset("select nome from " & DB & "colonne where idtable = " & idTable & " Order by ordinale")
  
  For i = 1 To rs.RecordCount
    testo = testo & Space(14 + indentGen) & _
      IIf(Len(Aliast), Aliast & ".", "") & rs!nome & IIf(i < rs.RecordCount Or indPart, ",", "") & vbCrLf
    rs.MoveNext
  Next
  rs.Close
  
  CostruisciSelect = testo
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 27-07-06
' Vedi commento CostruisciInto_D
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function CostruisciSelect_D() As String
  Dim rsColonne As Recordset
  Dim testo As String, alias As String
  Dim i As Integer, j As Integer
    
  For i = 1 To UBound(TabIstr.BlkIstr)
'''MG 240107
'''''    Set rsColonne = m_dllFunctions.Open_Recordset("select nome from dmdb2_colonne where idtable = " & TabIstr.BlkIstr(i).idTable & " Order By ordinale")
    Set rsColonne = m_dllFunctions.Open_Recordset("select nome from " & DB & "colonne where idtable = " & TabIstr.BlkIstr(i).idTable & " Order By ordinale")
    
    If rsColonne.RecordCount Then
      For j = 1 To rsColonne.RecordCount
        '  If Aliast <> "" Then
        '     testo = testo & Space(14 + indentGen) & Aliast & "." & MettiUnderScore(tb1!nome)
        '  Else
        '     testo = testo & Space(14 + indentGen) & MettiUnderScore(tb1!nome)
        '  End If
        'TMP:
        alias = "T" & i
        testo = testo & Space(14 + indentGen) & alias & IIf(Len(alias), ".", "") & rsColonne!nome
        testo = testo & IIf(i < UBound(TabIstr.BlkIstr) Or j < rsColonne.RecordCount, ",", "") & vbCrLf
        
        rsColonne.MoveNext
      Next
    End If
    rsColonne.Close
  Next
  CostruisciSelect_D = testo
End Function
Public Function CostruisciSet(idTab As Variant, nomeTab As String) As String
  Dim rs As Recordset
  Dim i As Integer
  Dim testo As String

  'For k = IndStart To indEnd
    'Aliast = "T" & Format(k, "#0")
    'Set rs = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(k).idSegm & ")")
    'Set rs = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where tag <> 'ISRT' and idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(k).idSegm & ") order by ordinale")
    'probabilmente al posto del valore "ISRT" nel TAG sarebbe meglio gestire l'idcmp,
    'che dovrebbe contenere -1 proprio negli stessi casi
    'da verificare!!!!!!
'      Set Tb1 = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne " & _
'          "where tag <> 'ISRT' and idtable in (select idtable from dmdb2_tabelle " & _
'          "where idorigine = " & TabIstr.BlkIstr(k).IdSegm & ") " & _
'          "and idcolonna not in (select b.idcolonna from dmdb2_index as a, dmdb2_idxcol as b " & _
'          "where a.idindex=b.idindex and a.tipo='P' and a.idTable=dmdb2_colonne.idTable) " & _
'          "order by ordinale")
      'stefano: semplifichiamo con l'idtable
'''MG 240107
'''''    Set rs = m_dllFunctions.Open_Recordset( _
'''''      "select * from dmdb2_colonne where tag <> 'ISRT' and idtable = " & idTab & _
'''''      " and idcolonna not in (select b.idcolonna from dmdb2_index as a, dmdb2_idxcol as b " & _
'''''      " where a.idindex=b.idindex and a.tipo='P' and a.idTable=dmdb2_colonne.idTable) order by ordinale")
    Set rs = m_dllFunctions.Open_Recordset( _
      "select * from " & DB & "colonne where tag <> 'ISRT' and idtable = " & idTab & _
      " and idcolonna not in (select b.idcolonna from " & DB & "index as a, " & DB & "idxcol as b " & _
      " where a.idindex=b.idindex and a.tipo='P' and a.idTable=" & DB & "colonne.idTable) order by ordinale")

    For i = 1 To rs.RecordCount
      testo = testo & Space(19) & rs!nome & " = " & vbCrLf
      testo = testo & Space(20) & "    :" & "RR-" & Replace(nomeTab, "_", "-") & "." & Replace(rs!nome, "_", "-")
      'SQ 9-08
      If rs!Null Then
         testo = testo & vbCrLf & Space(20) & "    :WK-I-" & Replace(rs!nome, "_", "-")
      End If

      'SQ vedi sopra
      'testo = testo & Space(20) & MettiUnderScore(Tb1!nome) & " = " & vbCrLf
      'testo = testo & Space(24) & ":RR-" & TabIstr.BlkIstr(k).Tavola & "." & Trim(SostUnderScore(Tb1!nome))
      'If Tb1.RecordCount <> i Or k <> indEnd Then
      testo = testo & IIf(i < rs.RecordCount, ",", " ") & vbCrLf
      rs.MoveNext
    Next
    'SQ: sbagliato
    'Else
    '  testo = testo & Space(20) & "* " & vbCrLf
    'End If
    rs.Close
  'Next k
   
  CostruisciSet = testo
End Function
Public Function CostruisciWConc(indStart As Integer, indEnd As Integer, wAlias As Boolean, Optional ByVal yesSelect As Boolean = False) As String
  Dim testo As String, TestoOrder As String, aliasT1 As String, aliasT2 As String
  Dim K As Integer, k1 As Integer, y As Integer, y1 As Integer
  Dim tb1 As Recordset
  Dim wIdSeg As Long, wIdTab As Long
  Dim flagKey As Boolean
  Dim tb2 As Recordset
  Dim andOr As String, sqlCommand As String

  'For k1 = 1 To 100
  '   wColOrder(k1) = ""
  'Next k1
  
  andOr = "    "
  
  'stefanopippo: 15/08/2005 scusate, sicuramente sarebbe pi� giusto dividere le condizioni
  'di join dall'order by
  'stefanopippo: 15/08/2005: fa JOIN solo per secondaria, se non � l'ultima;
  'devono essere gestiti tutti gli altri - eventuali - casi
  For K = indStart To indEnd
    If UBound(TabIstr.BlkIstr(K).KeyDef) Then
      If TabIstr.BlkIstr(K).KeyDef(1).key <> TabIstr.BlkIstr(K).pKey Then
          flagKey = True
          y = K
      End If
    End If
    
    If flagKey And K = indEnd And K <> y Then
      'ULTIMO LIVELLO: QUALIFICAZIONE PER CHIAVE SECONDARIA SUL LIVELLO PRECEDENTE (y)
      wIdTab = TabIstr.BlkIstr(K).idTable
      If indPart Then
        wIdTab = TabIstr.BlkIstr(K - 1).idTable 'SQ non � "y"?
      End If
      aliasT1 = "T" & Format(y, "#0")
      'stefanopippo: 17/08/2005: questa gestione su alpitour non funziona e comunque non
      'ne capisco il senso (ad esempio, perch� differenzia GU dalle altre?
      'eppoi cosa significa fare una condizione in or uguale ad una all'interno della and?);
      'su banca va bene perch� le colonne ereditate mantengono lo stesso nome
      'provo a modificarla (a parte che sarebbe stato molto pi� semplice usare
      'il nomecmp, che � gi� il nome del campo in join, ma questo � vero solo su alpitour; a questo punto join su idcmp)
      'questa nuova versione forse, ma forse, non gestisce join a 3 livelli da rivedere su questi casi (ma ci sono?)
'''MG 240107
'''''      sqlCommand = "select a.nome, b.nome from " & _
'''''                     "dmdb2_colonne as a, dmdb2_colonne as b " & _
'''''                     "Where A.idcmp = b.idcmp and a.idtablejoin = b.idtable " & _
'''''                     "and a.idtable <> a.idtablejoin " & _
'''''                     "and a.idtable = " & wIdTab & " order by a.ordinale"
      sqlCommand = "select a.nome, b.nome from " & _
                     DB & "colonne as a, " & DB & "colonne as b " & _
                     "Where A.idcmp = b.idcmp and a.idtablejoin = b.idtable " & _
                     "and a.idtable <> a.idtablejoin " & _
                     "and a.idtable = " & wIdTab & " order by a.ordinale"
      Set tb1 = m_dllFunctions.Open_Recordset(sqlCommand)
      'Set Tb1 = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idcolonna in (select idcolonna from dmdb2_idxcol where idindex in (select idindex from dmdb2_index where tipo = 'P' and idtable in (select idtable from dmdb2_tabelle where idorigine = " & wIdSeg & "))) order by idcolonna")
      While Not tb1.EOF
         ' For y = k + 1 To IndEnd
         '   Set tb2 = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idcolonna in (select idcolonna from dmdb2_idxcol where idindex in (select idindex from dmdb2_index where tipo = 'P' and idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(y).idSegm & "))) order by idcolonna")
         '   While Not tb2.EOF
         '     If (IstrDb2 = "GU" And (Tb1!idtable <> Tb1!idtablejoin And tb2!nome = Tb1!nome)) Or (tb2!nome = Tb1!nome) Then
              'SQ (Prova) If tb2!nome = Tb1!nome Then
                aliasT2 = "T" & Format(K, "#0")
                testo = testo & Space(11 + indentGen) & andOr
                If wAlias Then
                   testo = testo & aliasT1 & "."
                End If
                testo = testo & tb1(1) & " = " & vbCrLf
                testo = testo & Space(20)
                If wAlias Then
                   testo = testo & aliasT2 & "."
                End If
        '        testo = testo & Trim(MettiUnderScore(Tb1!nome)) & vbCrLf
                testo = testo & tb1(0) & vbCrLf
        '      End If
              If Len(testo) Then andOr = "AND "
        '           End If
        '    tb2.MoveNext
        '   Wend
        ' Next y
          tb1.MoveNext
        Wend
    End If
  Next K

  If Len(Trim(testo)) Then
    testo = Space(16) & "AND ( " & vbCrLf & testo & Space(15 + indentGen) & ")" & vbCrLf
  End If

''''''''''  '''''''''''''''''''''''''''
''''''''''  ' ORDER BY
''''''''''  '''''''''''''''''''''''''''
''''''''''  If IstrDb2 = "GN" Or IstrDb2 = "GNP" Or IstrDb2 = "GU" Or IstrDb2 = "ISRT" Or IstrDb2 = "GHU" Or IstrDb2 = "GHN" Or IstrDb2 = "GHNP" Then
''''''''''    For k = IndStart To indEnd
''''''''''      'SQ 31-07-06
''''''''''      'If Trim(TabIstr.BlkIstr(k).Search) = "LAST" Then wAscending = " DESC "
''''''''''      If InStr(TabIstr.BlkIstr(k).CodCom, "L") Then
''''''''''        wAscending = " DESC "
''''''''''      Else
''''''''''        wAscending = " "
''''''''''      End If
''''''''''
''''''''''      'stefanopippo: 15/08/2005: se � primaria, ma non � l'ultima, non fa ORDER BY
''''''''''      ' (la far� direttamente sulla chiave ereditata); dopo la LAST, perch� questa
''''''''''      ' info ce l'ha sul livello precedente
''''''''''      If k = indEnd Then
''''''''''        flagKey = True
''''''''''      Else
''''''''''        flagKey = False
''''''''''        If UBound(TabIstr.BlkIstr(k).KeyDef) Then
''''''''''          If TabIstr.BlkIstr(k).KeyDef(1).key <> TabIstr.BlkIstr(k).pKey Then
''''''''''            flagKey = True
''''''''''          End If
''''''''''        End If
''''''''''      End If
''''''''''
''''''''''      If flagKey Then
''''''''''        'imposta come nome chiave quella primaria del livello ultimo
'''''''''''       sqlcommand = "select a.nome from dmdb2_index as a, dmdb2_tabelle as b" & _
'''''''''''                           " where b.idorigine = " & TabIstr.BlkIstr(k).IdSegm & " AND b.TAG='AUTOMATIC' and tipo = 'P' and a.idtable = b.idtable"
''''''''''
''''''''''        '+'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''+'
''''''''''        'SQ 21-07-06 - Cos'� 'sto casino qui?
''''''''''        '''sqlcommand = "select nome from dmdb2_index" & _
''''''''''        '''             " where idtable = " & TabIstr.BlkIstr(k).IdTable & " and tipo = 'P'"
''''''''''        If UBound(TabIstr.BlkIstr(k).KeyDef) Then '(� giusta questa if Ubound?)
''''''''''          nomeKey = TabIstr.BlkIstr(k).KeyDef(1).key
''''''''''          '''If Len(TabIstr.BlkIstr(k).KeyDef(1).NameAlt) Then
''''''''''          '''  nomeKey = TabIstr.BlkIstr(k).KeyDef(1).NameAlt
''''''''''          '''Else
''''''''''          '''   Set tb1 = m_dllFunctions.Open_Recordset(sqlcommand)
''''''''''          '''   If tb1.RecordCount Then
''''''''''          '''      nomeKey = tb1(0)
''''''''''          '''   Else
''''''''''          '''    'SP corso
''''''''''          '''    'MsgBox "errore su ricerca chiave: idSegm=" & TabIstr.BlkIstr(k).IdSegm
''''''''''          '''    nomeKey = ""
''''''''''          '''   End If
''''''''''          '''End If
''''''''''        Else
''''''''''          '''Set tb1 = m_dllFunctions.Open_Recordset(sqlcommand)
''''''''''          '''If tb1.RecordCount Then
''''''''''          '''   nomeKey = tb1(0)
''''''''''          ''' Else
''''''''''          ''''SP corso
''''''''''          '''   'MsgBox "errore su ricerca chiave: idSegm=" & TabIstr.BlkIstr(k).IdSegm
''''''''''          '''   nomeKey = ""
''''''''''          '''End If
''''''''''          nomeKey = TabIstr.BlkIstr(k).pKey
''''''''''        End If
''''''''''        '+'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''+'
''''''''''
''''''''''        'stefanopippo: 15/08/2005: imposta un alias diverso se effettua una join,
''''''''''        'considerato che le join si fanno solo in caso di utilizzo di chiave secondaria
''''''''''        'SP 12/9 e per chiavi "particolari2
''''''''''         If wAlias Then
''''''''''            aliasT1 = "T" & Format(indEnd, "#0")
''''''''''            If UBound(TabIstr.BlkIstr(k).KeyDef) Then
''''''''''              If TabIstr.BlkIstr(k).KeyDef(1).key <> TabIstr.BlkIstr(k).pKey And Not indPart Then
''''''''''                aliasT1 = "T" & Format(k, "#0")
''''''''''              End If
''''''''''            End If
''''''''''         Else
''''''''''            aliasT1 = ""
''''''''''         End If
'''''''''''               sqlcommand = "select d.nome from " & _
'''''''''''                    " dmdb2_idxcol as a, dmdb2_index as b, dmdb2_tabelle as c, dmdb2_colonne as d " & _
'''''''''''                    " where "
''''''''''        sqlcommand = "Select d.nome from " & _
''''''''''             " dmdb2_idxcol as a, dmdb2_index as b, dmdb2_colonne as d " & _
''''''''''             " where "
''''''''''        'stefanopippo: 15/08/2005: l'idsegm va preso dall'ultimo livello, cos� come
''''''''''        'il nome key, nel caso l'ultimo sia squalificato
''''''''''        If k < indEnd Then
''''''''''           If UBound(TabIstr.BlkIstr(k + 1).KeyDef) = 0 Then
''''''''''             'wIdSeg = TabIstr.BlkIstr(indEnd).IdSegm
''''''''''             wIdTab = TabIstr.BlkIstr(indEnd).IdTable
''''''''''             sqlcommand = sqlcommand & "b.tipo = 'P'"
''''''''''           Else
''''''''''             'wIdSeg = TabIstr.BlkIstr(k).IdSegm
''''''''''             wIdTab = TabIstr.BlkIstr(k).IdTable
''''''''''             sqlcommand = sqlcommand & " b.nome = '" & nomeKey & "' "
''''''''''           End If
''''''''''        Else
''''''''''           'wIdSeg = TabIstr.BlkIstr(k).IdSegm
''''''''''           wIdTab = TabIstr.BlkIstr(k).IdTable
''''''''''           sqlcommand = sqlcommand & " b.nome = '" & nomeKey & "' "
''''''''''        End If
'''''''''''               sqlcommand = sqlcommand & " and c.idorigine = " & wIdSeg & " AND c.TAG='AUTOMATIC'" & _
'''''''''''                          " and a.idindex = b.idindex and b.idtable = c.idtable " & _
'''''''''''                          " and a.idcolonna = d.idcolonna "
''''''''''               sqlcommand = sqlcommand & " and b.idtable = " & wIdTab & _
''''''''''                          " and a.idindex = b.idindex " & _
''''''''''                          " and a.idcolonna = d.idcolonna "
''''''''''                   'stefanopippo: 15/08/2005: differenzia se � stata utilizzata la chiave
''''''''''                   ' secondaria nel livello precedente: non deve ordinare sulle chiavi ereditate:
''''''''''                   ' in realt� sarebbe ancora pi� complesso: potrebbe essere presente il caso di
''''''''''                   ' 3 livelli: il primo per secondaria, il secondo ed il terzo per primaria;
''''''''''                   ' in questa situazione andrebbe fatta una join tra il primo ed il terzo
''''''''''                   ' con order by sulla secondaria del primo + sulla terza concatenando primaria
''''''''''                   ' del secondo e primaria del terzo, ma per ora lasciamo perdere.....
''''''''''                   ' da rivalutare se togliere anche in caso di squalificata
''''''''''               If k > 1 Then
''''''''''                  If UBound(TabIstr.BlkIstr(k - 1).KeyDef) Then
''''''''''                    'SQ
''''''''''                    ' If TabIstr.BlkIstr(k - 1).KeyDef(1).key <> TabIstr.BlkIstr(k - 1).KeyDef(1).NameAlt Then
''''''''''                     If TabIstr.BlkIstr(k - 1).KeyDef(1).key <> TabIstr.BlkIstr(k - 1).pKey Then
''''''''''                        sqlcommand = sqlcommand & "and d.idtablejoin = d.idtable "
''''''''''                     End If
''''''''''                  End If
''''''''''               End If
''''''''''               sqlcommand = sqlcommand & "order by a.ordinale"
''''''''''
''''''''''               Set tb1 = m_dllFunctions.Open_Recordset(sqlcommand)
''''''''''               While Not tb1.EOF
''''''''''                  If Trim(TestoOrder) = "" Then
''''''''''                    TestoOrder = wPrefOrder & "ORDER BY " & vbCrLf
''''''''''                  End If
''''''''''                  TestoOrder = TestoOrder & wPrefOrder & Space(2) & aliasT1 & "." & tb1(0) & wAscending & "," & vbCrLf
''''''''''                  tb1.MoveNext
''''''''''               Wend
''''''''''               tb1.Close
''''''''''            End If
''''''''''         'End If
''''''''''      'End If
''''''''''    Next k
'''''''''''------------------------------------------------------------------------------------------------
'''''''''''stefanopippo: 15/08/2005: ma perch� aggiunge sempre questo ciclo all'altro?
'''''''''''nel caso delle secondarie aggiunge sempre la primaria sotto, che pu� anche essere corretto,
'''''''''''(cos� se la secondaria non � univoca, le chiavi uguali vengono ordinate per primaria),
'''''''''''ma andrebbe fatto solo se specificato nel parametro SUBSEQ, che al momento non trattiamo
'''''''''''provo ad escluderlo, ho cercato di gestire tutto nel ciclo precedente
'''''''''''   For k = IndStart To IndEnd
'''''''''''...
'''''''''''   Next k
'''''''''''------------------------------------------------------------------------------------------------
''''''''''  End If
''''''''''
''''''''''  If Len(TestoOrder) Then
''''''''''    If Mid(TestoOrder, Len(TestoOrder) - 2, 1) = "," Then 'OCIO, c'� il vbcrlf!
''''''''''      Mid(TestoOrder, Len(TestoOrder) - 2, 1) = " "
''''''''''   End If
''''''''''  End If
  
  'SQ 3-08-06 - Cambio soluzione: non pi� JOIN...
  'SQ 28-07-06
  'If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
  '  testo = testo & getJoin_D()
  'End If
  
  CostruisciWConc = testo

End Function
'''''''''''''''''''''''''''''''''''''''''''
' SQ 1-08-06
' Separato dal CostruisciWconc
'''''''''''''''''''''''''''''''''''''''''''
Private Function getOrderBy(indStart As Integer, indEnd As Integer, wAlias As Boolean) As String
  Dim rs As Recordset
  Dim ascending As String, sqlCommand As String, keyName As String
  Dim aliasT1 As String, aliasT2 As String
  Dim idTable As Long
  Dim orderByKey As Boolean
  Dim i As Integer
  
  On Error GoTo errRout
  
  For i = indStart To indEnd
    'SQ 31-07-06
    'If Trim(TabIstr.BlkIstr(i).Search) = "LAST" Then ascending = " DESC "
    If InStr(TabIstr.BlkIstr(i).CodCom, "L") Then
      ascending = " DESC "
    Else
      ascending = " "
    End If
    
    'ORDER BY su:
    '- livelli con Chiave Secondaria
    '- livello di lettura (ultimo)
    '(dopo la LAST, perch� questa info ce l'ha sul livello precedente)?!
    If i = indEnd Then
      orderByKey = True
      'Chiave di ordinamento:
      If UBound(TabIstr.BlkIstr(i).KeyDef) Then
        keyName = TabIstr.BlkIstr(i).KeyDef(1).key
      Else
        ''''''''''''''''''''''''''''
        ' GESTIONE PROCSEQ
        ' SQ 2-08-06
        ''''''''''''''''''''''''''''
        If Len(TabIstr.procSeq) Then
          keyName = getProcseqField(CLng(TabIstr.BlkIstr(i).IdSegm), TabIstr.procSeq)
          If Len(keyName) = 0 Then
            'Messaggio su log -> Default: Primary Key
            keyName = TabIstr.BlkIstr(i).pKey
          Else
            DoEvents
          End If
        Else
          'Default: Primary Key
          keyName = TabIstr.BlkIstr(i).pKey
        End If
      End If
    Else
      orderByKey = False
      If UBound(TabIstr.BlkIstr(i).KeyDef) Then
        If TabIstr.BlkIstr(i).KeyDef(1).key <> TabIstr.BlkIstr(i).pKey Then
          orderByKey = True
          keyName = TabIstr.BlkIstr(i).KeyDef(1).key  'per ora basta qui dentro...
        End If
      End If
    End If
    
    If orderByKey Then
      'Alias diverso se effettua una join (se chiave secondaria o "particolari")
      If wAlias Then
        aliasT1 = "T" & Format(indEnd, "#0")
        If UBound(TabIstr.BlkIstr(i).KeyDef) Then
          If TabIstr.BlkIstr(i).KeyDef(1).key <> TabIstr.BlkIstr(i).pKey And Not indPart Then
            aliasT1 = "T" & Format(i, "#0")
          End If
        End If
      End If
'''MG 240107
'''''      sqlCommand = "SELECT d.nome from DMDB2_Idxcol as a, DMDB2_Index as b, DMDB2_Colonne as d WHERE "
      sqlCommand = "SELECT d.nome from " & DB & "Idxcol as a, " & DB & "Index as b, " & DB & "Colonne as d WHERE "
      
      'stefanopippo: 15/08/2005
      'l'idsegm va preso dall'ultimo livello, cos� come il nome key, nel caso l'ultimo sia squalificato
      If i < indEnd Then
        If UBound(TabIstr.BlkIstr(i + 1).KeyDef) = 0 Then
          'Livello successivo squalificato (ma non � indEnd)!?
          'SQ 1-08-06 ????????? PROVA - NON DIFFERENZIATO I CASI ????????
          'idTable = TabIstr.BlkIstr(indEnd).idTable
          'sqlCommand = sqlCommand & "b.tipo = 'P'"
          idTable = TabIstr.BlkIstr(i).idTable
          sqlCommand = sqlCommand & "b.nome = '" & keyName & "'"
          'SQ - Debug:
          m_dllFunctions.WriteLog "#### Controllare ORDER BY####" & wIstrCod(totIstr).Istruzione & " - " & wIstrCod(totIstr).Routine
        Else
          idTable = TabIstr.BlkIstr(i).idTable
          sqlCommand = sqlCommand & "b.nome = '" & keyName & "'"
        End If
      Else
         'wIdSeg = TabIstr.BlkIstr(i).IdSegm
         idTable = TabIstr.BlkIstr(i).idTable
         sqlCommand = sqlCommand & "b.nome = '" & keyName & "'"
      End If
      
'     sqlcommand = sqlcommand & " and c.idorigine = " & wIdSeg & " AND c.TAG='AUTOMATIC'" & _
'                          " and a.idindex = b.idindex and b.idtable = c.idtable " & _
'                          " and a.idcolonna = d.idcolonna "
      sqlCommand = sqlCommand & " AND b.idtable = " & idTable & _
                 " AND a.idindex = b.idindex " & _
                 " AND a.idcolonna = d.idcolonna "
                 
                 
      'stefanopippo: 15/08/2005: differenzia se � stata utilizzata la chiave
      ' secondaria nel livello precedente: non deve ordinare sulle chiavi ereditate:
      ' in realt� sarebbe ancora pi� complesso: potrebbe essere presente il caso di
      ' 3 livelli: il primo per secondaria, il secondo ed il terzo per primaria;
      ' in questa situazione andrebbe fatta una join tra il primo ed il terzo
      ' con order by sulla secondaria del primo + sulla terza concatenando primaria
      ' del secondo e primaria del terzo, ma per ora lasciamo perdere.....
      ' da rivalutare se togliere anche in caso di squalificata
      'SQ ######## VERIFICARE QUESTA COSA... NON CREDO VADA BENE COSI'!!!!!!!! ##########
      If i > 1 Then
        If UBound(TabIstr.BlkIstr(i - 1).KeyDef) Then
          'SQ
          ' If TabIstr.BlkIstr(i - 1).KeyDef(1).key <> TabIstr.BlkIstr(i - 1).KeyDef(1).NameAlt Then
           If TabIstr.BlkIstr(i - 1).KeyDef(1).key <> TabIstr.BlkIstr(i - 1).pKey Then
              sqlCommand = sqlCommand & "and d.idtablejoin = d.idtable "
           End If
        End If
      End If
      sqlCommand = sqlCommand & "ORDER BY a.ordinale"
             
      Set rs = m_dllFunctions.Open_Recordset(sqlCommand)
      While Not rs.EOF
        getOrderBy = getOrderBy & Space(12 + indentGen) & Space(2) & aliasT1 & "." & rs(0) & ascending & "," & vbCrLf
        rs.MoveNext
      Wend
      rs.Close
    End If
  Next
  
  If Len(getOrderBy) Then
    If Mid(getOrderBy, Len(getOrderBy) - 2, 1) = "," Then 'OCIO, c'� il vbcrlf!
      Mid(getOrderBy, Len(getOrderBy) - 2, 1) = " "
    End If
    getOrderBy = Space(12 + indentGen) & "ORDER BY " & vbCrLf & getOrderBy
  End If
  Exit Function
errRout:
  m_dllFunctions.WriteLog "##getOrderBy - error## " & wIstrCod(totIstr).Istruzione & " - " & wIstrCod(totIstr).Routine
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Restituisce il nome del XDFIELD corrispondente al procseq (LCHILD)
' Attenzione: non � detto che sia un XDFIELD...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getProcseqField(idSegmento As Long, procSeq As String) As String
  Dim rs As Recordset
  
  Set rs = m_dllFunctions.Open_Recordset( _
    "SELECT nome from PsDLI_XDField where idSegmento=" & idSegmento & " AND Lchild='" & procSeq & "'")
  If rs.RecordCount Then
    getProcseqField = rs!nome
  Else
    'PROCSEQ su qualche livello padre: a noi non serve! (forse)
    'm_dllFunctions.WriteLog "#getProcseqField# " & wIstrCod(totIstr).Istruzione & " - " & wIstrCod(totIstr).Routine & _
    '  "XDField not found: idSegment=" & idSegmento & "-procseq='" & procseq & "'"
  End If
  rs.Close
End Function

Public Function DecIstr(Stringa) As String
  Dim testo As String
  Dim k1, k2, k3, k4 As Integer
  Dim swIn As Boolean
  
  k1 = InStr(Stringa, "<pgm>")
  If k1 > 0 Then
     k2 = InStr(k1 + 1, Stringa, "</pgm>")
     If k2 > 0 Then
        testo = Mid(Stringa, k1 + 5, k2 - (k1 + 5)) & "-"
     End If
  End If
  k1 = InStr(1, Stringa, "<istr>")
  If k1 > 0 Then
     k2 = InStr(k1 + 1, Stringa, "</istr>")
     If k2 > 0 Then
        testo = testo & Mid(Stringa, k1 + 6, k2 - (k1 + 6)) & vbCrLf
     End If
  Else
     testo = testo & vbCrLf
  End If
        
  k1 = InStr(1, Stringa, "<level")
  While k1 > 0
     testo = testo & Space(6) & "*" & Space(15)
     k2 = InStr(k1 + 1, Stringa, "</level")
     k3 = k1 + 9
     While k3 < k2 And k3 > 0
        k3 = InStr(k3 + 1, Stringa, ">")
        If Mid(Stringa, k3 + 1, 1) <> "<" Then
           k4 = InStr(k3 + 1, Stringa, "<")
           testo = testo & Mid(Stringa, k3 + 1, k4 - (k3 + 1)) & "-"
        End If
     Wend
     'toglie l'ultimo trattino
     testo = Left(testo, Len(testo) - 1) & vbCrLf
     k1 = InStr(k2 + 1, Stringa, "<level")
  Wend
  k1 = InStr(1, Stringa, "<fdbkey>")
  If k1 > 0 Then
    k2 = InStr(k1 + 1, Stringa, "</fdbkey>")
    If k2 > 0 Then
      testo = testo & Space(6) & "*" & Space(15) & Mid(Stringa, k1 + 8, k2 - (k1 + 8)) & vbCrLf
    End If
  End If
        
  k1 = InStr(1, Stringa, "<procseq>")
  If k1 > 0 Then
     k2 = InStr(k1 + 1, Stringa, "</procseq>")
     If k2 > 0 Then
        testo = testo & Space(6) & "*" & Space(15) & "PROCSEQ-" & Mid(Stringa, k1 + 9, k2 - (k1 + 9)) & vbCrLf
     End If
  End If
  
  k1 = InStr(1, Stringa, "<procopt>")
  If k1 > 0 Then
     k2 = InStr(k1 + 1, Stringa, "</procopt>")
     If k2 > 0 Then
        testo = testo & Space(6) & "*" & Space(15) & "PROCOPT-" & Mid(Stringa, k1 + 9, k2 - (k1 + 9)) & vbCrLf
     End If
  End If
  
  k1 = InStr(1, Stringa, "<pcbseq>")
  If k1 > 0 Then
     k2 = InStr(k1 + 1, Stringa, "</pcbseq>")
     If k2 > 0 Then
        testo = testo & Space(6) & "*" & Space(15) & "SEQUENCE-" & Mid(Stringa, k1 + 8, k2 - (k1 + 8)) & vbCrLf
     End If
  End If

  DecIstr = Left(testo, Len(testo) - 2)

End Function

Public Function InitIstr(wIstr, wQual) As String
   Dim testo As String
   Dim K As Integer
   Dim k1 As Integer
   
   IstrDb2 = wIstr
   'SQ 30-08 (togliere i punti finali)
   testo = Space(11) & "MOVE '" & wIstr & "' TO WK-ISTR." & vbCrLf
   
   If UBound(TabIstr.BlkIstr) > 0 Then
      testo = testo & Space(11) & "MOVE '" & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Tavola & "' TO WK-NAMETABLE." & vbCrLf
      'SP 5/9 segmento
      testo = testo & Space(11) & "MOVE '" & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Segment & "' TO WK-SEGMENT." & vbCrLf
   End If
   'SQ 30-08 testo = testo & vbCrLf
   
   'testo = testo & Space(11) & "PERFORM MANAGE-PCB THRU MANAGE-PCB-EX." & vbCrLf
   testo = testo & Space(11) & "PERFORM 9000-MANAGE-PCB." & vbCrLf
   
   InitIstr = testo

End Function

Public Function InitIstr_DLI(wIstr, wQual) As String
    Dim testo As String
    Dim K As Integer
    Dim k1 As Integer
    
    IstrDb2 = wIstr
       
    testo = testo & "      *------------------------------------------------------------*" & vbCrLf
    testo = testo & "      *  Call    : " & Gistruzione & "  " & vbCrLf
    testo = testo & "      *  Dialect : " & GbStrDial(GbAttDial) & "  " & vbCrLf
    testo = testo & "      *------------------------------------------------------------*" & vbCrLf
    testo = testo & Space(7) & "ROUT-" & Gistruzione & Trim(Str(GbAttDial)) & "." & vbCrLf
    
    
    InitIstr_DLI = testo

End Function


Public Function Get_IdField_DaIstrLiv(IdOgg As Long, riga As Long, Liv As Long) As Long
  Dim r As ADODB.Recordset
  Dim rf As ADODB.Recordset
  
  Set r = m_dllFunctions.Open_Recordset("Select * From PSDli_IstrDett_Key Where IdOggetto = " & IdOgg & " And Riga = " & riga & " And Livello = " & Liv)
  
  If r.RecordCount > 0 Then
    r.MoveFirst
      If IsNull(r!IdField) Then
        Get_IdField_DaIstrLiv = 0
      Else
        If r!IdField < 0 Then
            Set rf = m_dllFunctions.Open_Recordset("Select * From PsDli_XDField Where idXDField = " & Abs(r!IdField))
         
            If rf.RecordCount > 0 Then
               Get_IdField_DaIstrLiv = Val(r!IdField)
            End If
            
            rf.Close
        Else
            Get_IdField_DaIstrLiv = Val(r!IdField)
        End If
      End If
    r.Close
  End If
End Function

'sungard: copiato da data manager: da fare unico e meglio, cos� ovviamente fa abbastanza schifo
Public Function getEnvironmentParam(ByVal parameter As String, envType As String, dbd As String, tabella As String) As String
  Dim token As String
  Dim subTokens() As String
  
  'intervallo:
  Dim intervallo() As String
  Dim wApp As String
  
  On Error GoTo errdb
  Dim wRout As String
  
  Select Case envType
    Case "dbd"
      'SG: Non so perch� ma a me non funziona, quindi la rsicrivo, appena ti invio tutto la riscrivo meglio
      'environment = nome DBD
'''''''''''''''      While Len(parameter)
'''''''''''''''        token = nextToken(parameter)
'''''''''''''''        If Left(token, 1) <> "(" Then
'''''''''''''''          getEnvironmentParam = getEnvironmentParam & token
'''''''''''''''        Else
'''''''''''''''          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
'''''''''''''''          Select Case subTokens(0)
'''''''''''''''            Case "ROUTE-NAME"
'''''''''''''''              token = getRouteName_dbd(dbd)
'''''''''''''''            Case "TABLE-NAME"
'''''''''''''''              token = tabella
'''''''''''''''            Case "SEGM-NAME"
'''''''''''''''              token = tabella
'''''''''''''''            Case "DBD-NAME"
'''''''''''''''              token = dbd
'''''''''''''''            Case Else
'''''''''''''''              '...
'''''''''''''''          End Select
'''''''''''''''          If UBound(subTokens) Then
'''''''''''''''            intervallo = Split(subTokens(1), "-")
'''''''''''''''            'integrare i casi senza entrambi i valori (es.: #-6#)
'''''''''''''''            If Len(intervallo(0)) = 0 Then
'''''''''''''''              'ULTIMI N CHAR (es.: #-N#)
'''''''''''''''              token = Right(token, CInt(intervallo(1)))
'''''''''''''''            Else
'''''''''''''''              If Len(intervallo(1)) Then
'''''''''''''''                'INTERVALLO ORTODOSSO (es.: #3-4#)
'''''''''''''''                token = Left(token, CInt(intervallo(1)))
'''''''''''''''              'Else
'''''''''''''''                'DAL N-esimo CHAR in poi (es.: #N-#)
'''''''''''''''              End If
'''''''''''''''              token = Mid(token, CInt(intervallo(0)))
'''''''''''''''            End If
'''''''''''''''          End If
'''''''''''''''          getEnvironmentParam = getEnvironmentParam & token
'''''''''''''''        End If
'''''''''''''''      Wend
       'SG : Sto martellando dentro tutti i casi fino ad oggi trovati, bisogner� integrare quella di Matteo, o farne una nuova
       'Per ora funziona
       wRout = nomeroutine
       wApp = Replace(parameter, "[ROUT-NAME]", Mid(wRout, 3, 2))
       wApp = Replace(wApp, "[DBD-NAME#-4]", Mid(dbd, Len(dbd) - 3))
       wApp = Replace(wApp, "[SEGM-NAME#-4]", Mid(tabella, Len(tabella) - 3))
       wApp = Replace(wApp, "[DBD-NAME#1-2#]", Mid(dbd, 1, 2))
       wApp = Replace(wApp, "[SEGM-NAME#2-F#]", Mid(tabella, 2))
       wApp = Replace(parameter, "(ROUT-NAME)", Mid(wRout, 3, 2))
       wApp = Replace(wApp, "(DBD-NAME#-4)", Mid(dbd, Len(dbd) - 3))
       wApp = Replace(wApp, "(SEGM-NAME#-4)", Mid(tabella, Len(tabella) - 3))
       wApp = Replace(wApp, "(DBD-NAME#1-2#)", Mid(dbd, 1, 2))
       wApp = Replace(wApp, "(SEGM-NAME#2-F#)", Mid(tabella, 2))
       'ALPITOUR: 5/8/2005 : SEMPRE PEGGIO, CON SUNGARD AVEVA FUNZIONATO BENE!
       wApp = Replace(wApp, "(ROUTE-NAME)", getRouteName_dbd(dbd))
      
    Case "else"
      '...
  End Select
  
  getEnvironmentParam = wApp & Space(8 - Len(wApp))
  
  Exit Function
errdb:
  If Err.Number = 13 Then
    'sbagliato l'intervallo numerico
    MsgBox "Wrong definition for " & parameter, vbExclamation
  Else
    MsgBox Err.Description
  End If
  
  getEnvironmentParam = ""
End Function
'SQ: provvisorio per "," come separatore... farne uno con argomento opzionale...
'ritorna la parola successiva (space unico separatore)
'Considera come delimitatore di "token" le parentesi e gli apici singoli...
'Effetto Collaterale: mangia...
Function nextToken(inputString As String)
  Dim level, i, j As Integer
  Dim currentChar As String
  
  'Mangio i bianchi davanti (primo giro...)
  inputString = LTrim(inputString)
  If (Len(inputString) > 0) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
     Case "("
        i = 2
        level = 1
        nextToken = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
          Case "("
              level = level + 1
              nextToken = nextToken & currentChar
          Case ")"
              level = level - 1
              nextToken = nextToken & currentChar
          Case Else
              'ATTENZIONE: SE NON HO LA CHIUSA SI INLUPPA!
              If i <= Len(inputString) Then
                nextToken = nextToken & currentChar
              Else
                Err.Raise 123, "nextToken", "syntax error on " & inputString
                Exit Function
              End If
          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
     Case "'"
        inputString = Mid(inputString, 2)
        i = InStr(inputString, "'")
        If i Then
          nextToken = Left(inputString, InStr(inputString, "'") - 1)
          inputString = Mid(inputString, InStr(inputString, "'") + 1)
        Else
          'SQ 9-02-06 (tmp: fare un 125!? e gestirlo dappertutto)
          inputString = "''" & inputString
          Err.Raise 123, "nextToken", "syntax error on " & inputString
          Exit Function
        End If
     Case Else
        i = InStr(inputString, " ")
        j = InStr(inputString, "(")
        If ((i > 0 And j > 0 And j < i) Or (i = 0 And j > 0)) Then i = j
        If (i > 0) Then
            nextToken = Left(inputString, i - 1)
            inputString = Trim(Mid(inputString, i))
        Else
            nextToken = inputString
            inputString = ""
        End If
    End Select
   End If
End Function

Public Function transcode_booleano(wBool As String) As String
   Select Case Trim(wBool)
      Case "*", "&"
         transcode_booleano = "AND"
      Case "+", "!"
         transcode_booleano = "OR"
   End Select
End Function

Public Function is_Qualificata(wNumBlkIstr As Long) As Boolean
   If Trim(UCase(TabIstr.BlkIstr(wNumBlkIstr).ssaName)) = "" Then
      is_Qualificata = False
   Else
      is_Qualificata = True
   End If
   
   If UBound(TabIstr.BlkIstr(wNumBlkIstr).KeyDef) > 0 Then
      is_Qualificata = True
   Else
      is_Qualificata = False
   End If
End Function

'SQ: 28-07-05
Public Function getEnvironmentParam_SQ(ByVal parameter As String, envType As String, environment As collection) As String
  Dim token As String
  Dim subTokens() As String
  'intervallo:
  Dim intervallo() As String
  
  On Error GoTo errdb
  
  Select Case envType
    Case "colonne"
      'environment = rsColonne,rsTabelle
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "GROUP-NAME"
              'token = getGroupName_colonne(environment.Item(1)!IdOggetto)
            Case "COLUMN-NAME"
              token = environment.Item(1)!nome
            Case "ALIAS"
'              If m_dllFunctions.FnNomeDB = "banca.mty" Then
'                token = Left(environment.Item(2)!nome, 2) & "N" & Mid(environment.Item(2)!nome, 10)
'              Else
'                'fare...
'              End If
            Case Else
              MsgBox "Variable unknown: " & subTokens(0)
          End Select
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        End If
      Wend
    Case "tabelle"
      'environment = rsTabelle,rsSegmenti
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "ROUTE-NAME"
              token = getRouteName_tabelle(environment.Item(2)!idOggetto)
            Case "DBD-NAME"
              token = getDbd_tabelle(environment.Item(2)!idOggetto)
            Case "SEGM-PROGR"
              token = getSegProgr_tabelle(environment.Item(2))
            Case "SEGM-NAME"
              token = environment.Item(2)!nome
            Case "TABLE-NAME"
              'SQ 27-06-06
              'token = getTableName_tabelle(environment.Item(2)!IdOggetto)
              token = environment.Item(2)!nome
            Case "ALIAS"
            'stefano: ALIAS (occurs e redefines)
'              If m_dllFunctions.FnNomeDB = "banca.mty" Then
'                token = Left(environment.Item(2)!nome, 2) & "N" & Mid(environment.Item(1)!nome, 10)
'              Else
'                'fare...
'              End If
              token = getAlias_tabelle(environment.Item(1)!idTable)
            Case "NOME-AREA"  'Area Cobol associata al segmento:
              'token = getNomeArea_tabelle(environment.Item(2)!IdOggetto)
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        End If
      Wend
    Case "dbd"
      'environment = nome DBD
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "ROUTE-NAME"
              token = getRouteName_dbd(environment.Item(1))
            Case "TABLE-NAME"
              token = getTableName_dbd(environment.Item(1))
            Case "DBD-NAME"
              token = environment.Item(1)
            'Case "SEGM-PROGR"
            '  token = getSegProgr_tabelle(environment.Item(2))
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam_SQ = getEnvironmentParam_SQ & token
        End If
      Wend
    Case "else"
      '...
  End Select
  Exit Function
errdb:
  If Err.Number = 13 Then
    'sbagliato l'intervallo numerico
    MsgBox "Wrong definition for " & parameter, vbExclamation, "i-4.Migration"
  ElseIf Err.Number = 123 Then 'GESTITO: nextToken
    MsgBox "Wrong definition for " & parameter, vbExclamation, "i-4.Migration"
  Else
    MsgBox Err.Description
    'Resume
  End If
  getEnvironmentParam_SQ = ""
End Function
'SQ
Public Function getRouteName_dbd(NomeDbD As String) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB

  Set r = m_dllFunctions.Open_Recordset("SELECT Route_name from psDli_DBD where DBD_name='" & NomeDbD & "'")
  
  If r.RecordCount Then
    getRouteName_dbd = r(0)
  Else
    getRouteName_dbd = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getRouteName_dbd = ""
End Function
'SQ
Public Function getTableName_dbd(NomeDbD As String) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB

  Set r = m_dllFunctions.Open_Recordset("SELECT Table_name from psDli_DBD where DBD_name='" & NomeDbD & "'")
  
  If r.RecordCount Then
    getTableName_dbd = r(0)
  Else
    getTableName_dbd = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getTableName_dbd = ""
End Function
'SQ
Public Function getSegProgr_tabelle(rs As ADODB.Recordset) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB
  
  If InStr(rs.Source, " MgDLI_Segmenti ") Then
    Set r = m_dllFunctions.Open_Recordset("SELECT count(*) from MgDLI_Segmenti where idOggetto = " & rs!idOggetto & " AND idSegmento <= " & rs!idSegmento)
  Else
    Set r = m_dllFunctions.Open_Recordset("SELECT count(*) from PsDLI_Segmenti where idOggetto = " & rs!idOggetto & " AND idSegmento <= " & rs!idSegmento)
  End If
  If r.RecordCount Then
    getSegProgr_tabelle = Format(r(0), "000")
  Else
    getSegProgr_tabelle = "000"
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getSegProgr_tabelle = "000"
End Function
'SQ
Public Function getRouteName_tabelle(IdDBD As Long) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB

  Set r = m_dllFunctions.Open_Recordset("SELECT Route_name from psDli_DBD,bs_oggetti where bs_oggetti.nome=psDli_DBD.DBD_name AND bs_oggetti.idOggetto = " & IdDBD)

  If r.RecordCount Then
    getRouteName_tabelle = r(0)
  Else
    getRouteName_tabelle = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getRouteName_tabelle = ""
End Function
'SP
Public Function getAlias_tabelle(idTable As Long) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB
'''MG 240107
'''''  Set r = m_dllFunctions.Open_Recordset("SELECT Nome from DMDB2_Alias where idtable = " & idTable)
  Set r = m_dllFunctions.Open_Recordset("SELECT Nome from " & DB & "Alias where idtable = " & idTable)

  If r.RecordCount Then
    getAlias_tabelle = r(0)
  Else
    m_dllFunctions.WriteLog "errore su ricerca alias: idTable=" & idTable
    getAlias_tabelle = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  m_dllFunctions.WriteLog "errore su ricerca alias: idTable=" & idTable
  getAlias_tabelle = ""
End Function
'SQ
Public Function getDliCopies_BPER(rsTabelle As ADODB.Recordset) As String
  Dim inCondition As String
  Dim rsCopies As Recordset
  
  On Error GoTo errdb
  
  inCondition = rsTabelle!IdOrigine
  rsTabelle.MoveNext
  While Not rsTabelle.EOF
    inCondition = inCondition & "," & rsTabelle!IdOrigine
    rsTabelle.MoveNext
  Wend
  'ripristiono per il chiamante
  rsTabelle.MoveFirst
  
  Set rsCopies = m_dllFunctions.Open_Recordset("select distinct nome from bs_oggetti as a,MgRel_SegAree as b where StrIdOggetto=IdOggetto AND " & _
                                                 "IdSegmento IN (" & inCondition & ")")
  getDliCopies_BPER = "COPY " & rsCopies(0) & "."
  rsCopies.MoveNext
  While Not rsCopies.EOF
    getDliCopies_BPER = getDliCopies_BPER & vbCrLf & Space(11) & "COPY " & rsCopies(0) & "."
    rsCopies.MoveNext
  Wend
  rsCopies.Close
  
  Exit Function
errdb:
  MsgBox Err.Description
  getDliCopies_BPER = ""
End Function
'SQ 8-08-05
Public Function getParentTable(idSegmento As Long) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB
'''MG 240107
'''''  Set r = m_dllFunctions.Open_Recordset("SELECT c.nome from PsDLI_Segmenti as a, PsDLI_Segmenti as b,dmdb2_tabelle as c where a.IdOggetto=b.IdOggetto AND a.Nome=b.Parent AND b.IdSegmento=" & idSegmento & " AND a.IdSegmento=c.IdOrigine and c.tag not like '%OCCURS%'")
  Set r = m_dllFunctions.Open_Recordset("SELECT c.nome from PsDLI_Segmenti as a, PsDLI_Segmenti as b," & DB & "tabelle as c where a.IdOggetto=b.IdOggetto AND a.Nome=b.Parent AND b.IdSegmento=" & idSegmento & " AND a.IdSegmento=c.IdOrigine and c.tag not like '%OCCURS%'")
  
  If r.RecordCount Then
    getParentTable = r(0)
  Else
    getParentTable = ""
  End If
  r.Close
  
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getParentTable = ""
End Function

Public Function CostruisciWhere(indStart As Integer, indEnd As Integer, Optional ByVal doppiaDeclare As Boolean = False, Optional ByVal yesSelect As Boolean = False) As String
  Dim K As Integer, k1 As Integer
  Dim testo As String, opeSav As String
  Dim rs As Recordset
  'livelli massimi per DBD: 15...., banca ne ha solo 3, alpitour arriva a 5
  Dim idTab(15) As Long
  Dim tipoKey(15) As String, nomeKey(15) As String
  Dim keyRic As String, segRic As String, opeRic As String, aliasRic As String
  Dim precRic As Boolean, attRic As Boolean, flagFirst As Boolean
  Dim tabRic As String
  Dim KeyNum As Variant, boolEnd As Variant
  
  'SQ
  orderByColumns = "" 'probabilmente non lo user�... brasare
  
  flagFirst = True
  'tutti i livelli
  For K = indStart To indEnd
    'chiavi "booleane" - condizioni multiple sullo stesso livello
    'forzo 1 per entrare nel ciclo quando la chiave � totalmente squalificata
    If UBound(TabIstr.BlkIstr(K).KeyDef) Then
      boolEnd = UBound(TabIstr.BlkIstr(K).KeyDef)
    Else
      boolEnd = 1
    End If
    
    For k1 = 1 To boolEnd
      'il test lo fa solo sulla prima chiave; ovviamente se mai troveremo una "booleana"
      'con test misto tra primaria e secondaria, il sistema non funziona => SECONDO ME SONO COSE CHE NON SI FANNO!
      'SP 9/9 pcb dinamico: l'abbiamo trovato;
      'implementato il test corretto solo per pcb dinamico, in modo da non modificare il resto,
      'da verificare
      If k1 = 1 Then
         'stefano
         'IdSegm(k) = TabIstr.BlkIstr(k).IdSegm
         idTab(K) = TabIstr.BlkIstr(K).idTable
         If UBound(TabIstr.BlkIstr(K).KeyDef) Then
            If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
              'SQ
              'If TabIstr.BlkIstr(k).KeyDef(k1).NameAlt <> TabIstr.BlkIstr(k).KeyDef(k1).key Then
               If TabIstr.BlkIstr(K).pKey <> TabIstr.BlkIstr(K).KeyDef(k1).key Then
                  tipoKey(K) = "SECONDARY"
               Else
                  tipoKey(K) = "PRIMARY"
               End If
            Else
               tipoKey(K) = "NOQUAL"
            End If
         Else
            tipoKey(K) = "NOQUAL"
         End If
         'stefano
         'IdSegm(k) = TabIstr.BlkIstr(k).IdSegm
         idTab(K) = TabIstr.BlkIstr(K).idTable
         If tipoKey(K) = "SECONDARY" Then
            'nome chiave livello corrente
            'SQ
            'nomeKey(k) = TabIstr.BlkIstr(k).KeyDef(k1).NameAlt
            nomeKey(K) = TabIstr.BlkIstr(K).KeyDef(k1).key
         Else
            'forza il nome della chiave prendendolo dall'ultimo livello, per la concatenazione
            'corretta + il segmento
            'IdSegm(k) = TabIstr.BlkIstr(indEnd).IdSegm  'SQ ???
            idTab(K) = TabIstr.BlkIstr(indEnd).idTable
            Dim noQual As Boolean
            noQual = True
            If UBound(TabIstr.BlkIstr(indEnd).KeyDef) Then
              If Len(TabIstr.BlkIstr(indEnd).KeyDef(k1).key) Then
                noQual = False
              End If
            End If
            If Not noQual Then
              'SP 9/9 pcb dinamico
              'SQ
              'nomeKey(k) = TabIstr.BlkIstr(indEnd).KeyDef(1).NameAlt
              nomeKey(K) = TabIstr.BlkIstr(indEnd).KeyDef(k1).key
            Else
              'imposta come nome chiave quella primaria del livello ultimo
'''MG 240107
'''''               Set rs = m_dllFunctions.Open_Recordset( _
'''''                "SELECT nome FROM DMDB2_Index " & _
'''''                "WHERE idtable = " & TabIstr.BlkIstr(indEnd).idTable & " and tipo = 'P'")
               Set rs = m_dllFunctions.Open_Recordset( _
                "SELECT nome FROM " & DB & "Index " & _
                "WHERE idtable = " & TabIstr.BlkIstr(indEnd).idTable & " and tipo = 'P'")

'                "WHERE b.idorigine = " & TabIstr.BlkIstr(indEnd).IdSegm & " AND b.TAG='AUTOMATIC' and tipo = 'P' and a.idtable = b.idtable")
               If rs.RecordCount Then
                  nomeKey(K) = rs(0)
               Else
                m_dllFunctions.WriteLog "errore su ricerca chiave: idTable=" & TabIstr.BlkIstr(indEnd).idTable
                nomeKey(K) = ""
               End If
            End If
         End If
      End If
      Select Case tipoKey(K)
       Case "PRIMARY"
        'inserisce livello attuale
         keyRic = nomeKey(K)
         'segRic = IdSegm(k)
         tabRic = idTab(K)
         opeRic = TabIstr.BlkIstr(K).KeyDef(k1).Op
         'da rivedere
         'If PbLike Then
         '   opeRic = "LIKE"
         'End If
         'da rivedere
         'If Flgwhere Then
         '   If Ope = ">= " Then
         '      Ope = "> "
         '   End If
         'End If
         
         'verifica se concatenare la chiave ereditata o meno:
         'prende solo le chiavi ereditate o solo le chiavi non ereditate;
         'in caso di k = 1 = indend deve fare il doppio giro
         If K = indEnd And K > 1 Then
            precRic = False
         Else
            precRic = True
         End If
         'If k > 1 Then
         '  If tipoKey(k - 1) = "SECONDARY" Then
         '     precRic = True
         '  End If
         'End If
         attRic = False
         'solo l'ultimo
         aliasRic = "T" & Format(indEnd, "#0")
         KeyNum = TabIstr.BlkIstr(K).KeyDef(k1).KeyNum
         'scusate, ma � una condizione moolto forzata: doppia chiave definita
         'genererebbe doppio livello (idtable <> idtablejoin prende tutte le chiavi ereditate
         ' per cui sul terzo livello le prenderebbe due volte tutte e due)
         'quindi fa ripartire da zero; per banca � ok sicuro, da verificare con alpitour
         '(livelli > 3)
         If K > 1 And K < indEnd Then
            If nomeKey(K - 1) = nomeKey(K) Then
               testo = ""
               flagFirst = True
            End If
         End If
         'un po' di forzature da sintetizzare meglio, ma sono stanco
         If K = 1 And indEnd = 1 And Not doppiaDeclare Then
            opeSav = opeRic
            opeRic = "="
         End If
         If Not doppiaDeclare Or K <> indEnd Then
            'testo = testo & addInheritedColumns(keyRic, segRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
            testo = testo & addInheritedColumns(keyRic, tabRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
         End If
         If K = 1 And indEnd = 1 And Not doppiaDeclare Then
            precRic = False
            opeRic = opeSav
            'testo = testo & addInheritedColumns(keyRic, segRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
            testo = testo & addInheritedColumns(keyRic, tabRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
         End If
         'SP 6/9 doppiadeclare qualificata
         If doppiaDeclare And K = indEnd Then
            precRic = False
            'SP 9/9 pcb dinamico: gestione booleana
            'If opeRic = "=" Or opeRic = ">=" Then
            'SP 10/9: per = non entra pi� in doppia declare, comunque � giusto correggere la if
            'If (opeRic = "=" Or opeRic = ">=") And Not (dynPcb And k1 > 1) Then
            'SQ If (opeRic = ">=") And Not (dynPcb And k1 > 1) Then
            If (opeRic = ">=") Then
               opeRic = ">"
            End If
            'testo = testo & addInheritedColumns(keyRic, segRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
            testo = testo & addInheritedColumns(keyRic, tabRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
         End If
       Case "SECONDARY"
       'non recupera livello, imposta solo
         keyRic = nomeKey(K)
         'segRic = IdSegm(k)
         tabRic = idTab(K)
         opeRic = TabIstr.BlkIstr(K).KeyDef(k1).Op
         precRic = False
         attRic = True
         aliasRic = "T" & Format(K, "#0")
         'SP 12/9 indici particolari
         If indPart Then
            aliasRic = "T" & Format(K + 1, "#0")
         End If
         KeyNum = TabIstr.BlkIstr(K).KeyDef(k1).KeyNum
       'doppia declare su chiave secondaria, modifica l'operatore per attuare la fetch
       'a vuoto
       'SP 10/9: equiparato alla primaria il funzionamento sull'operatore per
       ' =, cio� comunque legge in avanti, anche se c'� tale operatore)
       'SP 11/9: rimodificato, era sbagliata la primaria; in ogni caso il flag doppiadeclare
       'viene impostato solo per operatore = ">"
         If doppiaDeclare And K = indEnd And opeRic = ">" Then
            opeRic = ">="
         End If
  'SP 27/9: prova pericolosa!!! confrontare bene!!!!
         Dim saveindpart As Boolean
         saveindpart = indPart
         indPart = True
         'testo = testo & addInheritedColumns(keyRic, segRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
         testo = testo & addInheritedColumns(keyRic, tabRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
         indPart = saveindpart
       Case "NOQUAL"
       'non esiste caso di doppio livello squalificato, per cui sono sicuramente (forse)
       'sull'ultimo; se k > 1 non ci sono problemi, ho gi� impostato i livelli precedenti
       'SP 15/9: provo con la select sulle redefines
         'If k = 1 Then
         If K = 1 Or yesSelect Then
            keyRic = nomeKey(K)
            'segRic = IdSegm(k)
            tabRic = idTab(K)
            opeRic = "="
            precRic = True
            attRic = False
            'stefano: 04/07/2006: provo a cambiare, perch� NON funziona pi�....
            'aliasRic = "T" & Format(k, "#0")
            aliasRic = "T" & Format(indEnd, "#0")
            If yesSelect Then
               KeyNum = 1
            Else
               KeyNum = K
            End If
            'testo = testo & addInheritedColumns(keyRic, segRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
            testo = testo & addInheritedColumns(keyRic, tabRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
         End If
         'recupera anche la chiave non ereditata
         If Trim(TabIstr.Istr) = "DLET" Or Trim(TabIstr.Istr) = "REPL" Then
            precRic = False
            'testo = testo & addInheritedColumns(keyRic, segRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
            testo = testo & addInheritedColumns(keyRic, tabRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
         End If
         'imposta la seconda where in caso di doppiadeclare
         'viene fatta sempre per chiave primaria (per ora, poi da cambiare), con operatore
         ' = ">" sull'ultimo livello
         'SP 11/9: con il pcb dinamico va bene cos�, in quanto la squalificata con procseq
         'su secondaria viene gestita simulandola come qualificata, per cui nel case "SECONDARY"
         ' a bocce ferme toglieremo anche questa forzatura facendola passare per "PRIMARY", in
         'modo da farlo solo se necessario (cio� se l'istruzione � gestita per pcb su primaria)
         If doppiaDeclare Then
            precRic = False
            opeRic = ">"
            'testo = testo & addInheritedColumns(keyRic, segRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
            testo = testo & addInheritedColumns(keyRic, tabRic, opeRic, precRic, attRic, aliasRic, KeyNum, flagFirst)
         End If
      End Select
    Next
  Next
  
'''  ' Mauro 13-04-2007 : Gestione End-Status
'''  If yesSelect Then
'''    Set rsColonne = m_dllFunctions.Open_Recordset("select * from " & DB & "colonne where idtable = " & TabIstr & " and nome = 'DEL_STATUS'")
'''  End If
  'gestione condizioni aggiuntive su template
  
  '''''''''''''''''''''
  ' JOIN
  '''''''''''''''''''''
  CostruisciWhere = testo & CostruisciWConc(indStart, indEnd, True)
  
  '''''''''''''''''''''
  'SQ 1-08-06
  ' ORDER BY (portare fuori anche da qui!)
  '''''''''''''''''''''
  'If Not yesSelect And (IstrDb2 = "GN" Or IstrDb2 = "GNP" Or IstrDb2 = "GU" Or IstrDb2 = "ISRT" Or IstrDb2 = "GHU" Or IstrDb2 = "GHN" Or IstrDb2 = "GHNP") Then
  If Not yesSelect And (IstrDb2 <> "DLET" And IstrDb2 <> "REPL") Then
    CostruisciWhere = CostruisciWhere & getOrderBy(indStart, indEnd, True)
  End If
  
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 28-07-06
' Command Code "D"
' -> Aggiunge alla WHERE CONDITION il join sulle tabelle dei livelli "parent"
''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getJoin_D() As String
  Dim rs As Recordset, rsParent As Recordset
  Dim testo As String, alias As String, join As String
  Dim i As Integer, j As Integer
    
  If UBound(TabIstr.BlkIstr) > 1 Then
    getJoin_D = Space(12 + indentGen) & "AND (" & vbCrLf
    'Colonne ereditate:
'''MG 240107
'''''    Set rs = m_dllFunctions.Open_Recordset( _
'''''      "Select nome,idTableJoin,idCmp from DMDB2_Colonne where " & _
'''''      " idTable=" & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).idTable & " AND idTable <> idTableJoin order by idTableJoin")
    Set rs = m_dllFunctions.Open_Recordset( _
      "Select nome,idTableJoin,idCmp from " & DB & "Colonne where " & _
      " idTable=" & TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).idTable & " AND idTable <> idTableJoin order by idTableJoin")
      
    For i = 1 To UBound(TabIstr.BlkIstr) - 1
      alias = "T" & i
      
      rs.MoveFirst
      For j = 1 To rs.RecordCount
'''MG 240107
'''''        Set rsParent = m_dllFunctions.Open_Recordset( _
'''''          "SELECT nome FROM Dmdb2_Colonne WHERE idTable=" & TabIstr.BlkIstr(i).idTable & " AND idCmp=" & rs!idCmp)
        Set rsParent = m_dllFunctions.Open_Recordset( _
          "SELECT nome FROM " & DB & "Colonne WHERE idTable=" & TabIstr.BlkIstr(i).idTable & " AND idCmp=" & rs!idcmp)
        
        If rsParent.RecordCount Then
          getJoin_D = getJoin_D & IIf(i + j > 2, " AND " & vbCrLf, "") & Space(12 + indentGen + 2) & _
            "T" & UBound(TabIstr.BlkIstr) & "." & rs!nome & " = " & alias & "." & rsParent!nome
        End If
        rsParent.Close
        rs.MoveNext
      Next
    Next
    rs.Close
        
    getJoin_D = getJoin_D & vbCrLf & Space(12 + indentGen) & ")" & vbCrLf
  End If
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''
' Ex CreaConcat_SP
''''''''''''''''''''''''''''''''''''''''''''''''''''
'Public Function addInheritedColumns(keyRic As String, segRic As Variant, opeRic As String, precRic As Boolean, attRic As Boolean, aliasRic As String, KeyNum As Variant, flagFirst As Boolean) As String
Public Function addInheritedColumns(keyRic As String, tabRic As Variant, opeRic As String, precRic As Boolean, attRic As Boolean, aliasRic As String, KeyNum As Variant, flagFirst As Boolean) As String
  Dim sqlCommand As String, testo As String, operator As String
  Dim rs As Recordset
  Dim numCol As Integer, K As Integer
  Dim complexCondition As Boolean
  'accontentiamoci: 30 colonne massimo per singola chiave
  Dim NomeCol(30) As String, nomeColRed(30) As String

  '''''''''''''''''''''''''''''''''''''
  ' 1 FIELD CHIAVE => N COLONNE CHIAVE
  '''''''''''''''''''''''''''''''''''''
'  sqlcommand = "select d.nome as nome, d.tag as tag, d.etichetta as etichetta from " & _
'              " dmdb2_idxcol as a, dmdb2_index as b, dmdb2_tabelle as c, dmdb2_colonne as d " & _
'              " where b.nome = '" & keyRic & "' and c.idorigine = " & segRic & " and c.TAG='AUTOMATIC'" & _
'              " and a.idindex = b.idindex and b.idtable = c.idtable " & _
'              " and a.idcolonna = d.idcolonna "
'''MG 240107
'''''  sqlCommand = "SELECT d.nome as nome, d.tag as tag, d.etichetta as etichetta from " & _
'''''              " DMDB2_Idxcol as a, DMDB2_Index as b, DMDB2_Colonne as d " & _
'''''              " where b.nome = '" & keyRic & "' and b.idtable = " & tabRic & _
'''''              " and a.idindex = b.idindex " & _
'''''              " and a.idcolonna = d.idcolonna "
  sqlCommand = "SELECT d.nome as nome, d.tag as tag, d.etichetta as etichetta from " & _
              DB & "Idxcol as a, " & DB & "Index as b, " & DB & "Colonne as d " & _
              " where b.nome = '" & keyRic & "' and b.idtable = " & tabRic & _
              " and a.idindex = b.idindex " & _
              " and a.idcolonna = d.idcolonna "
  
  'SP 12/9 indici particolari
  If Not indPart Then
    If precRic Then
      sqlCommand = sqlCommand & "and d.idtablejoin <> d.idtable "
    Else
      sqlCommand = sqlCommand & "and d.idtablejoin = d.idtable "
    End If
  End If
  sqlCommand = sqlCommand & "order by a.ordinale"
  
  Set rs = m_dllFunctions.Open_Recordset(sqlCommand)
  
  'imposta il multicolonna per la composizione della where (solo per i casi diversi da op "=")
  complexCondition = rs.RecordCount > 1 And opeRic <> "="
  
  While Not rs.EOF
     numCol = numCol + 1
     If flagFirst Then
        testo = Space(12 + indentGen) & "WHERE" & vbCrLf
        
        ''''''''''''''''''''''''''''''''''''''''''''
        ' condizioni di where "particolari" 'SQ ??
        ''''''''''''''''''''''''''''''''''''''''''''
        Dim whereSpec As Recordset
'        sqlcommand = "select RDBMS_tbname from psdli_segmenti where idsegmento = " & segRic & _
'                     " and RDBMS_tbname like 'Where%'"
'''MG 240107
'''''        sqlCommand = "select RDBMS_tbname from psdli_segmenti as a, dmdb2_tabelle as b where idsegmento = " & _
'''''                     " b.idorigine and b.idtable = " & tabRic & " and RDBMS_tbname like 'Where%'"
        sqlCommand = "select RDBMS_tbname from psdli_segmenti as a, " & DB & "tabelle as b where idsegmento = " & _
                     " b.idorigine and b.idtable = " & tabRic & " and RDBMS_tbname like 'Where%'"

        Set whereSpec = m_dllFunctions.Open_Recordset(sqlCommand)
        If whereSpec.RecordCount Then
           testo = testo & Space(13 + indentGen) & aliasRic & "." & getCondizione_Redefines(whereSpec(0)) & vbCrLf
        End If
        whereSpec.Close
        
        If Not complexCondition Then
           testo = testo & Space(14 + indentGen) & "( "
        End If
        flagFirst = False
     Else
     'per banca � sempre in AND, da gestire le booleane e le multicampo
     'il momento � arrivato
        If numCol = 1 Then
           testo = testo & Space(13 + indentGen) & "AND " & vbCrLf
           If Not complexCondition Then
              testo = testo & Space(14 + indentGen) & "( "
           End If
        Else
           testo = testo & Space(15 + indentGen) & "AND "
        End If
     End If
     
     If complexCondition Then
      ' CASI "COMPLESSI": composizione condizione a fine ciclo...
      'memorizza le colonne per il loop sulle chiavi multicolonna
      NomeCol(numCol) = rs!nome
      If numCol = rs.RecordCount Then
         operator = opeRic
      Else
         operator = "="
      End If
     Else
      operator = opeRic
     End If
     If numCol = 1 Then
        If complexCondition Then
           testo = testo & Space(14 + indentGen) & "(  ( "
        Else
           testo = testo & "   "
        End If
     End If
     
     'AGGIUNTA COLONNA + OPERATORE
     testo = testo & aliasRic & "." & Replace(rs!nome, "-", "_") & " " & operator & vbCrLf
     
     'SQ tmp
     orderByColumns = orderByColumns & Replace(rs!nome, "-", "_") & ","
     
     'AGGIUNTA VARIABILE HOST
     'SQ REDEFINES - SISTEMARE!!!!!!!!!!!!!!!!!!!
     If rs!Tag = "RED" Then
        nomeColRed(numCol) = rs!etichetta
        testo = testo & Space(21 + indentGen) & ":K" & Format(KeyNum, "00") & "-" & Replace(keyRic, "_", "-") & "." & Left(Replace(rs!etichetta, "_", "-"), 19)
     Else
        testo = testo & Space(21 + indentGen) & ":K" & Format(KeyNum, "00") & "-" & Replace(keyRic, "_", "-") & "." & Left(Replace(rs!nome, "_", "-"), 19)
     End If
     If numCol < rs.RecordCount Then
        testo = testo & vbCrLf
     Else
        If complexCondition Then
           testo = testo & " )" & vbCrLf
        Else
           testo = testo & "    )" & vbCrLf
        End If
     End If
     rs.MoveNext
  Wend
  rs.Close
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' COMPOSIZIONE CONDIZIONE PER CASI "COMPLESSI" (N COLONNE + OPERATORE <> "=")
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  If complexCondition Then
     While numCol > 0
        For K = 1 To numCol - 1
           If K = numCol - 1 Then
              If InStr(1, opeRic, ">") > 0 Then
                 operator = ">"
              Else
                 operator = "<"
              End If
           Else
              operator = "="
           End If
           'inizio ciclo (k=1)
           If K = 1 Then
              testo = testo & Space(13 + indentGen) & "OR   ("
           Else
              testo = testo & Space(15 + indentGen) & "AND "
           End If
           testo = testo & aliasRic & "." & Replace(NomeCol(K), "-", "_") & " " & operator & vbCrLf
           If nomeColRed(K) <> "" Then
            testo = testo & Space(21 + indentGen) & ":K" & Format(KeyNum, "00") & "-" & Replace(keyRic, "_", "-") & "." & Mid$(Replace(nomeColRed(K), "_", "-"), 1, 19)
           Else
            testo = testo & Space(21 + indentGen) & ":K" & Format(KeyNum, "00") & "-" & Replace(keyRic, "_", "-") & "." & Mid$(Replace(NomeCol(K), "_", "-"), 1, 19)
           End If
           If K < numCol - 1 Then
              testo = testo & vbCrLf
           Else
              testo = testo & " )" & vbCrLf
           End If
        Next
        numCol = numCol - 1
     Wend
     'stefanopippo: 15/08/2005: mi sono rotto le scatole, so che fa schifo tutta
     'la gestione delle parentesi, ma per ora � cos� ( bisognerebbe fare un ciclo
     'solo anche per le concatenate); tolgo il vbcrlf
     testo = Left(testo, Len(testo) - 2) & "  )" & vbCrLf
  End If
  addInheritedColumns = testo
End Function

'stefanopippo: 15/08/2005: sempre pi� pignolo....
'GN, GNP e GU sono tutte uguali, tranne la if iniziale, ma conviene differenziare solo
'in quel punto; per ora ho fatto cos�, al limite le si pu� riseparare, mantenendo unica tutta la parte uguale
'pensa che pulizia con tre bei template, uno per ogni tipo istruzione.....
'appena possibile lo facciamo, non ci vuole molto, ci saranno si e no 5 variabili
'con i template potremmo sicuramente utilizzare una sola routine anche per le ISRT, DLET, REPL
' e le modifiche ultime ci sarebbero costate un'inezia (modifiche ai template, non al prodotto, c'� una bella differenza)
Public Function getGU_DB2_old(rtbFile As RichTextBox, tipoIstr As String, Optional ByVal withold As Boolean = False) As String
  Dim K As Long
  Dim i As Integer, k1 As Integer, Kt As Integer, indEnd As Integer
  Dim NomeT As String, NomePk As String, nomeKey As String, testo As String, TestoInto As String
  Dim TestoFrom As String, TestoWhere As String, TestoWhere2 As String, wLiv01Copy As String
  Dim swqual As Boolean, keySecFlagPrimoLiv As Boolean, keySecFlagUltimoLiv As Boolean, keyOp As Boolean
  Dim flagSel As Boolean, flagIns As Boolean, keyPrimUguale As Boolean, flagRed As Boolean
  Dim flagOcc As Boolean, tabFound As Boolean, tabFound2 As Boolean, tabFound3 As Boolean
  Dim tb1 As Recordset, tb2 As Recordset, tb3 As Recordset, tbred As Recordset, rsIndPart As Recordset
  Dim indentGN As Variant, indentGNsq As Variant, indentGNqu As Variant, idRed() As Long, nomeKeyRed() As Variant, nomeFieldRed() As Variant
  Dim keyPrim As String, keySec As String, saveStorico As String, saveTemplStorico As String
  Dim nomeTRed() As String, nomeSRed() As String, testoRed() As String, testoSel() As String
  Dim indRed As Variant, indentRed As Variant
  'SP 12/9: cerca gli indici particolari!
  Dim nomeTabIndPart As String
  Dim idTabIndPart As Long
  Dim keySave() As BlkIstrKey
  Dim istrSave As IstrDli
  Dim instrLevel As BlkIstrDli
  Dim includedMember As String
  
  indentGen = 0
  
  If InStr(TabIstr.BlkIstr(1).CodCom, "D") And tipoIstr <> "ISRT" Then
    level = 1
  Else
    level = UBound(TabIstr.BlkIstr)
  End If
  
  If tipoIstr <> "ISRT" Then
    testo = Space(11) & "MOVE '" & tipoIstr & "' TO WK-ISTR" & vbCrLf
  End If
  
  For level = level To UBound(TabIstr.BlkIstr)
    '''''''''''''
    'init:
    '''''''''''''
    'tmp: (ora � ciclico!... togliere quelli che non servono!)
    keySecFlagPrimoLiv = False
    keySecFlagUltimoLiv = False
    keyPrimUguale = False
    swqual = False
    keyOp = False
    flagOcc = False
    flagRed = False
    flagSel = False
    flagIns = False
    tabFound = False
    tabFound2 = False
    tabFound3 = False
    includedMember = ""
      
    If tipoIstr <> "ISRT" Then
      'SQ D
      indEnd = level
      instrLevel = TabIstr.BlkIstr(indEnd)
      'SQ D
      'testo = InitIstr(tipoIstr, "N")
      IstrDb2 = tipoIstr
      
      'SQ D
      testo = testo & Space(11) & "INITIALIZE RD-" & instrLevel.Segment & vbCrLf
      If InStr(TabIstr.BlkIstr(1).CodCom, "D") And level > 1 Then
        'Lettura solo se il precedente livello andato a buon fine
        testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      End If
      
      If UBound(TabIstr.BlkIstr) Then
        testo = testo & Space(11) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
        testo = testo & Space(11) & "MOVE '" & instrLevel.Segment & "' TO WK-SEGMENT" & vbCrLf
      End If
      testo = testo & Space(11) & "PERFORM 9000-MANAGE-PCB" & vbCrLf
  
      flagIns = False
    Else
      'SQ D
      indEnd = UBound(TabIstr.BlkIstr) - 1
      instrLevel = TabIstr.BlkIstr(indEnd)
      'SQ - 21-07-06 (portato nella insert)
      'If indEnd = 0 Then
      '  getGU_DB2 = ""
      '  GbTestoFetch = ""
      '  GbTestoRedOcc = ""
      '  GbFileRedOcc = ""
      '  Exit Function
      'End If
      Dim tipoIstrIniz As String
      tipoIstrIniz = tipoIstr 'SQ - non mi basta il "flagIns"?
      tipoIstr = "GU"
      testo = testo & Space(11) & "INITIALIZE RD-" & instrLevel.Segment & vbCrLf  'SQ - Ci vuole?
      flagIns = True
    End If
    
    If Not indEnd > 0 Then  'SQ - portare fuori
      m_dllFunctions.WriteLog "Instruction: " & GbNumIstr & " - not converted", "Routines Generation"
      Exit Function
    End If
    
    If UBound(instrLevel.KeyDef) Then
      'nuovo sistema: si intende squalificata quando lo � sull'ultimo livello
      swqual = True 'portare dentro la IF
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        keySecFlagUltimoLiv = True
        'SP 12/9: gestione indici particolari (su altri segmenti/tabelle), solo secondari
        'per ora controllo solo il nome, che in alpitour � univoco!
  '      Set rsIndPart = m_dllFunctions.Open_Recordset( _
  '        "SELECT b.nome as nometab, b.idorigine as idsegm from psdli_xdfield as a, dmdb2_tabelle as b " & _
  '        "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine")
  '      If Not rsIndPart.EOF Then
  '        If rsIndPart!nomeTab <> instrLevel.Tavola Then
  '           nomeTabIndPart = rsIndPart!nomeTab
  '           idSegIndPart = rsIndPart!IdSegm
  '        End If
  '      End If
        'SQ - 21-07-06
        'TAG='AUTOMATIC' - IN OGNI CASO NON POSSO CERCARE PER NOME...
        ' => Aggiunto filtro DBD (VERIFICARE)!!!!!!!!!!
'''MG 240107
'''''        Set rsIndPart = m_dllFunctions.Open_Recordset( _
'''''          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, DMDB2_Tabelle as b " & _
'''''          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
'''''          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")
        Set rsIndPart = m_dllFunctions.Open_Recordset( _
          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & DB & "Tabelle as b " & _
          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")

        If Not rsIndPart.EOF Then
          If rsIndPart!nomeTab <> instrLevel.Tavola Then
             nomeTabIndPart = rsIndPart!nomeTab
             idTabIndPart = rsIndPart!idTab
          End If
        End If
        'sulla secondaria, solo con l'operatore per maggiore deve creare la doppia declare
        '(cio� l'esatto contrario della primaria)
        'SP 10/9: modificato, dovrebbe valere anche per l'operatore per =, cos� come
        'fa per la primaria (sulla primaria, se operatore = "=" lo fa diventare ">",
        'per scartare il primo record, sulla secondaria, vista la fetch a vuoto, deve
        'trasformarlo in ">="
        'SP 11/9 : RIMODIFICATO, ERA SBAGLIATA LA PRIMARIA, QUINDI SI FA SOLO PER
        '>!!! (quelli per <, <= li vedremo poi, ma non dovrebbero dare grossi problemi)
        'LA DIFFERENZA TRA PRIMARIA E SECONDARIA E' DOVUTA AL FATTO CHE LA SECONDARIA PUO' AVERE
        'DUPLICATI; COMUNQUE IN FUTURO ATTUEREMO IL SISTEMA DELLA FETCH A VUOTO, CHE ANDREBBE BENE
        'ANCHE PER LA PRIMARIA; PER ORA SI LASCIA COSI' PERCHE' LA PRIMARIA FUNZIONA!!!
        'ANZI, ANCHE LA PRIMARIA, PER =, HA BISOGNO DELLA FETCH A VUOTO, CHE POI SERVE SOLO A
        'DARE "GE" (WHERE PER = SU UNIVOCA, CON SCARTO DEL PRIMO RECORD ==> SQLCODE = 100
        'PER FORZA!
        keyOp = instrLevel.KeyDef(1).Op <> ">"
      Else
        keySecFlagUltimoLiv = False
        'operatore gi� per maggiore, non deve creare la doppia declare
        'SP 11/9 : ANCHE PER UGUALE NON DEVE CREARE LA DOPPIA DECLARE, LO DEVE FARE SOLO PER
        '>=!!! (quelli per <, <= li vedremo poi, ma non dovrebbero dare grossi problemi)
        keyOp = instrLevel.KeyDef(1).Op <> ">="
        'SP 11/9 Flag per forzare fetch a vuoto anche per primaria!!!
        keyPrimUguale = instrLevel.KeyDef(1).Op = "="
      End If
    Else
      swqual = False
    End If
  
  '   If UBound(instrLevel.KeyDef) Then
  '      If Not keySecFlagUltimoLiv Then
  '   'operatore gi� per maggiore, non deve creare la doppia declare
  '   'SP 11/9 : ANCHE PER UGUALE NON DEVE CREARE LA DOPPIA DECLARE, LO DEVE FARE SOLO PER
  '   '>=!!! (quelli per <, <= li vedremo poi, ma non dovrebbero dare grossi problemi)
  '         If Trim(instrLevel.KeyDef(1).Op) <> ">=" Then
  '             keyOp = True
  '         End If
  '   'SP 11/9 Flag per forzare fetch a vuoto anche per primaria!!!
  '         Dim keyPrimUguale As Boolea
  '         keyPrimUguale = False
  '         If Trim(instrLevel.KeyDef(1).Op) = "=" Then
  '            keyPrimUguale = True
  '         End If
  '      Else
  '   'sulla secondaria, solo con l'operatore per maggiore deve creare la doppia declare
  '   '(cio� l'esatto contrario della primaria)
  '   'SP 10/9: modificato, dovrebbe valere anche per l'operatore per =, cos� come
  '   'fa per la primaria (sulla primaria, se operatore = "=" lo fa diventare ">",
  '   'per scartare il primo record, sulla secondaria, vista la fetch a vuoto, deve
  '   'trasformarlo in ">="
  '   'SP 11/9 : RIMODIFICATO, ERA SBAGLIATA LA PRIMARIA, QUINDI SI FA SOLO PER
  '   '>!!! (quelli per <, <= li vedremo poi, ma non dovrebbero dare grossi problemi)
  '   'LA DIFFERENZA TRA PRIMARIA E SECONDARIA E' DOVUTA AL FATTO CHE LA SECONDARIA PUO' AVERE
  '   'DUPLICATI; COMUNQUE IN FUTURO ATTUEREMO IL SISTEMA DELLA FETCH A VUOTO, CHE ANDREBBE BENE
  '   'ANCHE PER LA PRIMARIA; PER ORA SI LASCIA COSI' PERCHE' LA PRIMARIA FUNZIONA!!!
  '   'ANZI, ANCHE LA PRIMARIA, PER =, HA BISOGNO DELLA FETCH A VUOTO, CHE POI SERVE SOLO A
  '   'DARE "GE" (WHERE PER = SU UNIVOCA, CON SCARTO DEL PRIMO RECORD ==> SQLCODE = 100
  '   'PER FORZA!
  '         If Trim(instrLevel.KeyDef(1).Op) <> ">" Then
  '            keyOp = True
  '         End If
  '      End If
  '   End If
         
    'INDENTAZIONE:
    Select Case tipoIstr
      Case "GN"
         indentGN = 3
         If keyOp Then
            indentGNsq = 3
            indentGNqu = 0
         Else
            indentGNsq = 6
            indentGNqu = 3
         End If
      Case "GNP"
         indentGN = 3
         indentGNsq = 3
         indentGNqu = 0
      Case "GU"
         indentGN = 0
         indentGNsq = 0
         indentGNqu = 0
    End Select
    Dim indentGNsqKeyOp
    indentGNsqKeyOp = indentGNsq
    If keyOp And tipoIstr = "GN" Then
      indentGNsqKeyOp = indentGNsq + 3
    End If
    indentRed = 0
    
    'stefanopippo: redefines; per la SELECT IL PROBLEMA E'
    'SOLO SULL'ULTIMO LIVELLO DELLA SELECT, IN QUANTO NON ESISTE JOIN A MENO DI CHIAVE
    'SECONDARIA, ED IN OGNI CASO LA SECONDARIA SAREBBE SULL'ULTIMA!
    Set tbred = m_dllFunctions.Open_Recordset( _
      "select condizione, RDBMS_tbname from PsDli_Segmenti where idSegmento = " & instrLevel.IdSegm)
    indRed = 1
    ReDim Preserve testoRed(1)
    ReDim Preserve nomeTRed(1)
    ReDim Preserve nomeSRed(1)
    ReDim Preserve idRed(1)
    ReDim Preserve nomeKeyRed(1)
    ReDim Preserve nomeFieldRed(1)
    idRed(1) = instrLevel.idTable
    If UBound(instrLevel.KeyDef) Then
      nomeKeyRed(1) = instrLevel.KeyDef(1).key
    Else
      nomeKeyRed(1) = ""
    End If
    Dim tbred2 As Recordset
    Set tbred2 = m_dllFunctions.Open_Recordset( _
      "select nome as nomeidx from psdli_field where unique and idsegmento=" & instrLevel.IdSegm)
    If Not tbred2.EOF Then
       nomeFieldRed(1) = tbred2!Nomeidx
    Else
       m_dllFunctions.WriteLog "No Field found for segment: " & instrLevel.IdSegm, "Routines Generation"
       nomeFieldRed(1) = ""
    End If
    nomeTRed(1) = instrLevel.Tavola
    nomeSRed(1) = instrLevel.Segment
    If Len(tbred!condizione) Then
       testoRed(1) = getCondizione_Redefines(tbred!condizione & "_lett")
    Else
       testoRed(1) = ""
    End If
    
    'STORICI: da buttare?
    saveStorico = ""
    If Len(tbred!rdbms_tbname) Then
       If Left(tbred!rdbms_tbname, 7) = "Storico" Then
          If SwTp Then
             saveStorico = getCondizione_Redefines(tbred!rdbms_tbname & "_cics")
             saveTemplStorico = tbred!rdbms_tbname & "_cics_squal"
          Else
             saveStorico = getCondizione_Redefines(tbred!rdbms_tbname)
             saveTemplStorico = tbred!rdbms_tbname & "_squal"
          End If
       End If
    End If
    
    'solo se non secondaria e non squalificata
    'comunque entra se "SELETTORE" o "Storico" attivo, in modo da effettuare multi-lettura
    'anche per letture squalificate
    flagRed = False
    If Len(tbred!rdbms_tbname) Then
       If tbred!rdbms_tbname = "SELETTORE" Or Left(tbred!rdbms_tbname, 7) = "Storico" Then
          flagRed = True
       End If
    End If
      Dim tbsel As Recordset
'''MG 240107
'''''    Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
'''''        "from dmdb2_tabelle " & _
'''''        "where idorigine = " & instrLevel.IdSegm & _
'''''        " and TAG like '%DISPATCHER%'")
    Set tbsel = m_dllFunctions.Open_Recordset("select idtable, nome " & _
        "from " & DB & "tabelle " & _
        "where idorigine = " & instrLevel.IdSegm & _
        " and TAG like '%DISPATCHER%'")

    If Not tbsel.EOF Then
       flagRed = True
       flagSel = True
    End If
    
    'REDEFINE:
    If flagRed Then
      '       Set tbred = m_dllFunctions.Open_Recordset("select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione " & _
      '           "from psdli_segmenti as a, dmdb2_tabelle as b " & _
      '           "where a.idsegmentoorigine = " & instrLevel.IdSegm & _
      '           " and a.idsegmento = b.idorigine order by a.condizione desc")
'''MG 240107
'''''         Set tbred = m_dllFunctions.Open_Recordset("select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
'''''             "from mgdli_segmenti as a, dmdb2_tabelle as b, dmdb2_index as c " & _
'''''             "where a.idsegmentoorigine = " & instrLevel.IdSegm & _
'''''             " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
         Set tbred = m_dllFunctions.Open_Recordset("select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
             "from mgdli_segmenti as a, " & DB & "tabelle as b, " & DB & "index as c " & _
             "where a.idsegmentoorigine = " & instrLevel.IdSegm & _
             " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
         
         If tbred.RecordCount Then
            ReDim Preserve testoRed(tbred.RecordCount + 1)
            ReDim Preserve nomeTRed(tbred.RecordCount + 1)
            ReDim Preserve nomeSRed(tbred.RecordCount + 1)
            ReDim Preserve idRed(tbred.RecordCount + 1)
            ReDim Preserve nomeKeyRed(tbred.RecordCount + 1)
            ReDim Preserve nomeFieldRed(tbred.RecordCount + 1)
            While Not tbred.EOF
               indRed = indRed + 1
               If Len(tbred!condizione) Then
                  'If tbred!condizione = "SELETTORE" Then
                  If flagSel Then
  '                   testoRed(indRed) = "IF WK-NAMETABLE = '" & nomeTRed(1) & "'"
  '                   idRed(indRed) = idRed(1)
  '                   nomeTRed(indRed) = nomeTRed(1)
  '                   nomeSRed(indRed) = nomeSRed(1)
                     testoRed(1) = ""
                     idRed(1) = tbsel!idTable
                     nomeTRed(1) = tbsel!nome
                     nomeSRed(1) = "DISPATCHER"
                     
                     'SQ - 26-07-06 !?
                     'Se squalificata sbaglio la copy PS?!
                     ' � gi� valorizzato il nomeFieldRed(1)!?...
                     'Comunque non deve prendere la chiave utilizzata dall'istruzione... ma quella primaria!
                     If UBound(instrLevel.KeyDef) > 0 Then
                        nomeKeyRed(1) = instrLevel.KeyDef(1).key
                        nomeFieldRed(1) = instrLevel.KeyDef(1).NameAlt
                     Else
                        nomeKeyRed(1) = ""
                        nomeFieldRed(1) = ""
                        'SQ - 26-07-06 - Pezza per non cambiare cose che non so!
                        nomeKeyRed(1) = Replace(instrLevel.pKey, "_", "-")
                        ' => vado a prendere il nome colonna corretto!
'''MG 240107
'''''                        Set tbred2 = m_dllFunctions.Open_Recordset( _
'''''                           "Select nome from Dmdb2_colonne where idTable=" & instrLevel.idTable & _
'''''                           " and idTable=idTableJoin and idCmp <> -1")
                        Set tbred2 = m_dllFunctions.Open_Recordset( _
                           "Select nome from " & DB & "colonne where idTable=" & instrLevel.idTable & _
                           " and idTable=idTableJoin and idCmp <> -1")

                        If Not tbred2.EOF Then
                          nomeFieldRed(1) = Replace(tbred2!nome, "_", "-")
                        Else
                          m_dllFunctions.WriteLog "No 'field' column found for table: " & instrLevel.idTable, "Routines Generation"
                          nomeFieldRed(1) = ""
                        End If
                        tbred2.Close
                      End If
                  End If
                  If flagSel Then
                    testoRed(indRed) = "IF WK-NAMETABLE = '" & tbred!nomeTab & "'"
                  Else
                    testoRed(indRed) = getCondizione_Redefines(tbred!condizione & "_lett")
                  End If
                  idRed(indRed) = tbred!idTab
                  nomeTRed(indRed) = tbred!nomeTab
                  nomeSRed(indRed) = tbred!nomeseg
                  nomeKeyRed(indRed) = tbred!nomeindex
         'stefano: campo per il nome del field
                  Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
                    "from mgdli_field where idfield = " & tbred!IdField & " and idsegmento = " & tbred!idSegmento)
                  If Not tbred2.EOF Then
                     nomeFieldRed(indRed) = tbred2!Nomeidx
                  Else
                     m_dllFunctions.WriteLog "Virtual Field not found: " & tbred!IdField, "Routines Generation"
                     nomeFieldRed(indRed) = ""
                  End If
           'SP redefines
               Else
                  m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
               End If
               tbred.MoveNext
            Wend
            tbred.Close
         End If
    End If
    If Not flagRed Then
       testoRed(1) = ""
    End If
      Dim indSav As Variant
    If flagSel Then
       indSav = indRed
       indRed = 1
    End If
    ' 6/9 : CAMBIATO TUTTO: doppia declare per tutti i casi di chiave primaria
    'sulla secondaria, recupero chiave + singola declare + fetch a vuoto
    
    'oops: gestione doppia declare ok solo per gu + gn entrambe per >=
    'rispristino gestione con fetch scartata, � pi� coerente, ma devo mantenere anche
    'la doppia declare per i casi di gn squalificata, che possono funzionare solo
    'recuperando l'ultima chiave letta e partendo da quella (comunque rimane da gestire
    'se il puntamento � stato fatto su una secondaria: si verifica con il campo
    'relativo alla chiave secondaria nel numstack, per� non so a priori quante declare
    'devo sviluppare, sono una per ogni gu diversa usata prima della gn squalificata;
    'bisogna verificarlo sui programmi incapsulati e dare questa informazione
    'a Rev, in modo che la possa sviluppare in automatico (� come il multioperatore
    'o il multisegmento, questo � un multikey, da inserire nel corrector quando
    'una chiave/ssa � squalificata.
    'Per ora imposto solo la primaria per > sull'ultimo livello.
    'Quindi, ricapitolando abbiamo due casi, riferendosi all'ultimo livello:
    '1) GN qualificata: fetch in pi� per  primo record in ordine di chiave
    '2) GN squalificata: doppia declare, con la seconda in where per > sulla chiave primaria
    'da implementare in seguito per chiavi secondarie; purtroppo su banca va quasi tutto
    'per secondaria... faremo, basta avere l'informazione nel db (le gn squalilficate sono
    'in buona parte usate in un solo punto e per una sola chiave)
    '...
    
    testo = testo & Space(11) & "MOVE LNKDB-SEGLEVEL(" & indEnd & ") TO LNKDB-STKLEV(LNKDB-NUMSTACK)" & vbCrLf & vbCrLf
     
     Select Case tipoIstr
      Case "GN"
        If swqual Then
          'SQ 31-07-06 - C.C. "F"
          If InStr(instrLevel.CodCom, "F") Then
            'Sistemare INDENT!
          Else
            testo = testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES " & vbCrLf
            'stefanopippo: gestione dell'SSA valido solo per CALL CBLTDLI
            'If m_dllFunctions.FnNomeDB <> "banca.mty" Then
            '  testo = testo & Space(11) & "OR LNKDB-STKSSA(LNKDB-NUMSTACK) < LNKDB-KEYVALUE(1, 1)" & vbCrLf
            'End If
            testo = testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GbNumIstr & vbCrLf
          End If
                           'vedi commentone sopra
                           'Space(11) & "   IF LNKDB-STKSSA(LNKDB-NUMSTACK) < LNKDB-KEYVALUE(1, 1)" & vbCrLf & _
                           'Space(11) & "   OR (LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GbNumIstr & vbCrLf & _
                           'Space(11) & "   AND LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = ZERO)" & vbCrLf
  'SP 6/9: avanti con la doppiadeclare, tranne sulle secondarie, che fanno solo la IF per
  'reimpostare, cos� come l'operatore; MODIFICATO: DOPPIADECLARE ANCHE PER LE SECONDARIE
           'If Not keySecFlagUltimoLiv Then
           'SP 9/9 pcb dinamico
              'SQ If Not dynPcb Then
           'SP 14/9 non funziona con un loop di GHU+REPL+GHN+REPL, ecc., perch� l'ultima
           'istruzione non era una GU, ma doveva considerarla cos�.
                 testo = testo & Space(14) & "IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) NOT = 'GU' AND 'ISRT'" & vbCrLf
                 testo = testo & Space(14) & "                          AND 'GN' AND 'REPL' AND 'DLET'" & vbCrLf
              
           'stefanopippo: gestione dell'SSA valido solo per CALL CBLTDLI
  '            If m_dllFunctions.FnNomeDB <> "banca.mty" Then
                 'testo = testo & Space(14) & "OR LNKDB-STKSSA(LNKDB-NUMSTACK) < LNKDB-KEYVALUE(1, 1)" & vbCrLf
  '            End If
              If Not keyOp Then
  '               If m_dllFunctions.FnNomeDB = "banca.mty" Then
  '                  testo = testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTBNUM(LNKDB-NUMSTACK)" & vbCrLf
  '               Else
                    testo = testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
  '               End If
              End If
           'End If
        Else
          'SQ 31-07-06 - C.C. "F"
          If InStr(instrLevel.CodCom, "F") Then
            'Sistemare INDENT!
          Else
           testo = testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
           testo = testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GbNumIstr & vbCrLf
          End If
           'SP 9/9 pcb dinamico
           'SQ If Not dynPcb Then
           'SP 14/9 problema su GHU+REPL+GHN+REPL
              testo = testo & Space(11) & "   IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) NOT = 'GU' AND 'ISRT'" & vbCrLf
              testo = testo & Space(11) & "                             AND 'GN' AND 'REPL' AND 'DLET'" & vbCrLf
          'purtroppo per non cambiare la cavolo di LNKDBRT di banca; da togliere poi
  '            If m_dllFunctions.FnNomeDB = "banca.mty" Then
  '               testo = testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTBNUM(LNKDB-NUMSTACK)" & vbCrLf
  '            Else
                 testo = testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
  '            End If
           
        End If
      Case "GNP"
        'SQ 31-07-06 - C.C. "F"
        If InStr(instrLevel.CodCom, "F") Then
          'Sistemare INDENT!
        Else
          testo = testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
          testo = testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GbNumIstr & vbCrLf
        End If
        testo = testo & Space(14) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space(14) & "MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
      Case "GU"
     End Select
        
     'SQ 8-08-05
     ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
     ' GESTIONE RESTORE:
     ' Attenzione: per ottimizzare,
     ' posso appoggiare il nome della tabella in TabIstr.BlkIstr(0)! (valutare)
     ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
     'Segmento "parent":
     'stefanopippo: 15/08/2005: se su chiave secondaria (sempre sul primo livello!) non fa restore (� inutile)
     keySecFlagPrimoLiv = False
     If UBound(TabIstr.BlkIstr(1).KeyDef) Then
        keySecFlagPrimoLiv = TabIstr.BlkIstr(1).KeyDef(1).key <> TabIstr.BlkIstr(1).pKey
     End If
     'SP 9/9 pcb dinamico
     If Not keySecFlagPrimoLiv Then
        NomeT = getParentTable(CLng(TabIstr.BlkIstr(1).IdSegm))  'togliere cast e cambiare function
        If Len(NomeT) Then
           'SEGMENTO NON RADICE: RESTORE
           testo = testo & Space(11 + indentGNsq) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
           'testo = testo & Space(11 + indentGNsq) & "PERFORM REST-FDBKEY THRU REST-FDBKEY-EX" & vbCrLf
           testo = testo & Space(11 + indentGNsq) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
           'CONTROLLARE SE CORRETTO: TabIstr.BlkIstr(1).PKey
           'stefanopippo: � inutile, la si fa pi� avanti
           'testo = testo & Space(11 + indentGNsq) & "MOVE KRR-" & TabIstr.BlkIstr(1).PKey & " TO K01-" & TabIstr.BlkIstr(1).PKey & vbCrLf
        End If
     End If
     
     'SP 9/9 pcb dinamico
     'SQ If Not dynPcb Then
      For K = 1 To indEnd
         Kt = K
         NomeT = TabIstr.BlkIstr(Kt).Tavola
         If Trim(NomeT) = "" Then
            NomeT = "[NO-TABLE-FOR:" & TabIstr.BlkIstr(Kt).Segment & "]"
         End If
         NomePk = TabIstr.BlkIstr(Kt).pKey
         testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
         'squalificata solo sull'ultimo livello, imposta la chiave primaria ereditata
         If UBound(TabIstr.BlkIstr(K).KeyDef) = 0 And K = BlkFin Then
            'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETC-WKEYWKEY THRU SETC-WKEYWKEY-EX " & vbCrLf
            testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO K01-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
         End If
         For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
          If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
            nomeKey = Replace(TabIstr.BlkIstr(K).KeyDef(k1).key, "-", "_")
            'SQ
            'NomePk = TabIstr.BlkIstr(k).KeyDef(k1).NameAlt
            NomePk = nomeKey
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(K)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
            'SQ
            'testo = testo & Space(16 + indentGNsqKeyOp) & "KRD-" & TabIstr.BlkIstr(k).KeyDef(k1).NameAlt & vbCrLf
            testo = testo & Space(16 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf
            'SP 12/9: indici particolari
             If nomeTabIndPart <> "" And K = indEnd Then
                testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
             End If
             'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
             testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
             If nomeTabIndPart <> "" And K = indEnd Then
                testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
             End If
             'solo su primaria: wkeywkey eredita la primaria
             'SQ
             'If TabIstr.BlkIstr(k).KeyDef(k1).NameAlt = TabIstr.BlkIstr(k).KeyDef(k1).key Then
             If TabIstr.BlkIstr(K).pKey = TabIstr.BlkIstr(K).KeyDef(k1).key Then
              'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETC-WKEYWKEY THRU SETC-WKEYWKEY-EX " & vbCrLf
              testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
             End If
             'SQ
             testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K" & Format(TabIstr.BlkIstr(K).KeyDef(k1).KeyNum, "00") & "-" & NomePk & vbCrLf
             'testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & nomeKey & " TO K" & Format(TabIstr.BlkIstr(k).KeyDef(k1).KeyNum, "00") & "-" & nomeKey & vbCrLf
           End If
         Next k1
      Next K
     'End If
     
     Select Case tipoIstr
      Case "GN"
         'If Not SwQual Or Not keySecFlagUltimoLiv Then
     'SP 9/9 pcb dinamico
           testo = testo & Space(14) & "ELSE" & vbCrLf
           
     'SP 9/9 pcb dinamico
           If Not keyOp Then
  '            If m_dllFunctions.FnNomeDB = "banca.mty" Then
  '               testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE 'O' TO LNKDB-STKTBNUM(LNKDB-NUMSTACK)" & vbCrLf
  '            Else
                 testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE 'O' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
  '            End If
           End If
           'da impostare diversamente in funzione del tipo lettura (qual/squal, operatore, ecc.)
           'per ora usa la primaria (vedi commentone sopra): CAMBIATO: VALE PER TUTTE LE PRIMARIE
           ' MA NON PER LE SECONDARIE, che fanno la fetch a vuoto
           'MODIFICATO ANCORA: ANCHE PER LE SECONDARIE, CHE FANNO COMUNQUE POI LA FETCH A VUOTO
           testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
           If Not keySecFlagUltimoLiv Then
              'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM REST-FDBKEY THRU REST-FDBKEY-EX" & vbCrLf
              testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
           Else
            'SQ
            'NomePk = instrLevel.KeyDef(1).NameAlt
            NomePk = instrLevel.KeyDef(1).key
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-STKPTGKEY(LNKDB-NUMSTACK) TO " & vbCrLf
            testo = testo & Space(16 + indentGNsqKeyOp) & "KRR-" & NomePk & vbCrLf
           End If
           'SQ 7-07-06 - Controllare... non credo vada bene!
           testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K01-" & NomePk & vbCrLf
     'SP 9/9 pcb dinamico
           Dim j As Integer
           For k1 = 2 To UBound(instrLevel.KeyDef)
            j = k1
            If Trim(instrLevel.KeyDef(k1).key) <> "" Then
              nomeKey = instrLevel.KeyDef(k1).key
              NomePk = instrLevel.KeyDef(k1).NameAlt
              testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(indEnd)) & ", " & Format(j, "#0") & ") TO " & vbCrLf
              'SQ
              'testo = testo & Space(13 + indentGNsqKeyOp) & "KRD-" & instrLevel.KeyDef(k1).NameAlt & vbCrLf
              testo = testo & Space(13 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf
                
             'SP 12/9: indici particolari
              If nomeTabIndPart <> "" Then
                 testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
              End If
              'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
              testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
              If nomeTabIndPart <> "" Then
                 testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
              End If
              'SP 16/9: nella booleana � meglio che non ci sia!!!
              'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETC-WKEYWKEY THRU SETC-WKEYWKEY-EX " & vbCrLf
              ''SQ 7-07-06
              'testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K" & Format(instrLevel.KeyDef(k1).KeyNum, "00") & "-" & NomePk & vbCrLf
              testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & nomeKey & " TO K" & Format(instrLevel.KeyDef(k1).KeyNum, "00") & "-" & nomeKey & vbCrLf
            End If
           Next k1
           
           testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
           If Not keyOp Then
              testo = testo & Space(11 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
           End If
        'End If
      Case "GNP"
      Case "GU"
     End Select
        
     'AGGIUNGE DECLARE 1
     'SP 9/9 pcb dinamico
     If True Then
      For K = 1 To indRed
         indentRed = 0
         If testoRed(K) <> "" Then
            If saveStorico <> "" And K = 1 Then
               'SP: gestione specifica per db storico e squalificata totale
               If UBound(TabIstr.BlkIstr(1).KeyDef) = 0 Then
                  testo = testo & Space(11 + indentGNsq) & Trim(Replace(getCondizione_Redefines(saveTemplStorico), vbCrLf, vbCrLf & Space(indentGNsq)))
               Else
                  testo = testo & Space(11 + indentGNsq) & Trim(Replace(saveStorico, vbCrLf, vbCrLf & Space(indentGNsq)))
               End If
            End If
            testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
            indentRed = 3
         End If
         indentGen = indentGNsq + indentRed
         
         'SELECT
         testo = testo & Space(11 + indentGNsq + indentRed) & _
          "EXEC SQL DECLARE " & CursorName & Format(K, "00") & " CURSOR " & IIf(withold, "WITH HOLD", "") & vbCrLf & _
          Space(11 + indentGNsq + indentRed) & " FOR SELECT " & vbCrLf
         'SQ 27-07-06 - C.C. "D"
         'If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
         '   testo = testo & CostruisciSelect_D()
         'Else
            '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
            indPart = Len(nomeTabIndPart) > 0
            testo = testo & CostruisciSelect("T" & indEnd, idRed(K))
            indPart = False
         'End If
                
         Dim idSave As Variant
         Dim tabSave As String, idxSave As String, idxSave2 As String, opSave As String
         'in realt� potrei farlo sempre, in quanto nel vettore, in caso senza redefines
         'c'� comunque lo stesso segmento/tabella
         If K > 1 Or flagSel Then
            idSave = instrLevel.idTable
            tabSave = instrLevel.Tavola
            instrLevel.Tavola = nomeTRed(K)
            instrLevel.idTable = idRed(K)
         End If
         'SP 12/9: altro gioco di prestigio per gli indici particolari...
         'tutto da mettere meglio nella versione definitiva
         If Len(nomeTabIndPart) Then
            idSave = instrLevel.idTable
            tabSave = instrLevel.Tavola
            indEnd = indEnd + 1
            ReDim Preserve TabIstr.BlkIstr(indEnd)
            TabIstr.BlkIstr(indEnd).idTable = idTabIndPart
            TabIstr.BlkIstr(indEnd).Tavola = nomeTabIndPart
            '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
            testo = testo & CostruisciSelect("T" & indEnd, idTabIndPart)
         End If
         
         'SQ 27-07-06 - C.C. "D"
         'If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
         '   testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom_D()
         'Else
            testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom(1, indEnd, True)
         'End If
         'SP 12/9: altro gioco di prestigio per gli indici particolari...
         'tutto da mettere meglio nella versione definitiva
         indPart = False
         If nomeTabIndPart <> "" Then
            TabIstr.BlkIstr(indEnd - 1).idTable = idTabIndPart
            TabIstr.BlkIstr(indEnd - 1).Tavola = nomeTabIndPart
            TabIstr.BlkIstr(indEnd).idTable = idSave
            TabIstr.BlkIstr(indEnd).Tavola = tabSave
            'proviamo con la squalificata
            ReDim Preserve TabIstr.BlkIstr(indEnd).KeyDef(1)
            indPart = True
         End If
         'SQ La gestione del C.C. "D" � interna alla CostruisciWhere per via dell'ORDER BY...
         testo = testo & CostruisciWhere(1, indEnd)
         
         If nomeTabIndPart <> "" Then
            'rimette a posto
            indEnd = indEnd - 1
            ReDim Preserve TabIstr.BlkIstr(indEnd)
            TabIstr.BlkIstr(indEnd).idTable = idSave
            TabIstr.BlkIstr(indEnd).Tavola = tabSave
            indPart = False
         End If
         If K > 1 Or flagSel Then
            TabIstr.BlkIstr(indEnd).Tavola = tabSave
            TabIstr.BlkIstr(indEnd).idTable = idSave
         End If
       
         testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
         testo = testo & Space(11 + indentGNsq + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
       
         If testoRed(K) <> "" Then
             testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
         End If
      Next
     End If
     'testo = testo & TestoSel & TestoFrom & TestoWhere
     'testo = testo & Space(11 + indentGNsq) & "END-EXEC" & vbCrLf
     
     '''''''''''''''''''''''''''''''''''
     ' SECONDA DECLARE
     '''''''''''''''''''''''''''''''''''
     Select Case tipoIstr
      Case "GN"
        'If (Not SwQual Or Not keySecFlagUltimoLiv) And Not keyOp Then
        If Not keyOp Then
        'SP 9/9 pcb dinamico
          testo = testo & Space(11 + indentGN) & "ELSE" & vbCrLf
  '         testo = testo & TestoSel2 & TestoFrom & TestoWhere2
           For K = 1 To indRed
            indentRed = 0
            If testoRed(K) <> "" Then
               If saveStorico <> "" And K = 1 Then
                  'SP: gestione specifica per db storico e squalificata totale
                  If UBound(TabIstr.BlkIstr(1).KeyDef) = 0 Then
                     testo = testo & Space(11 + indentGNsq) & Trim(Replace(getCondizione_Redefines(saveTemplStorico), vbCrLf, vbCrLf & Space(indentGNsq)))
                  Else
                     testo = testo & Space(11 + indentGNsq) & Trim(Replace(saveStorico, vbCrLf, vbCrLf & Space(indentGNsq)))
                  End If
               End If
               testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
               indentRed = 3
             End If
            '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
             indentGen = indentGNsq + indentRed
             
             testo = testo & Space(11 + indentGNsq + indentRed) _
                             & "EXEC SQL DECLARE " & CursorName & Format(K, "00") & IIf(suffDynT = "", "GU ", "") & " CURSOR " _
                             & IIf(withold, "WITH HOLD", "") & vbCrLf & Space(11 + indentGNsq + indentRed) & " FOR SELECT " & vbCrLf
             
             'SQ 28-07-06 - C.C. "D"
             'If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
             '   testo = testo & CostruisciSelect_D()
             'Else
                indPart = Len(nomeTabIndPart) > 0
                'SP 9/9 pcb dinamico
                testo = testo & CostruisciSelect("T" & indEnd, idRed(K))
                indPart = False
             'End If
             
             If K > 1 Then
               idSave = TabIstr.BlkIstr(indEnd).idTable
               tabSave = TabIstr.BlkIstr(indEnd).Tavola
               TabIstr.BlkIstr(indEnd).Tavola = nomeTRed(K)
               TabIstr.BlkIstr(indEnd).idTable = idRed(K)
             End If
              'SP 12/9: altro gioco di prestigio per gli indici particolari...
              'tutto da mettere meglio nella versione definitiva
              If nomeTabIndPart <> "" Then
                 idSave = TabIstr.BlkIstr(indEnd).idTable
                 tabSave = TabIstr.BlkIstr(indEnd).Tavola
                 indEnd = indEnd + 1
                 ReDim Preserve TabIstr.BlkIstr(indEnd)
                 TabIstr.BlkIstr(indEnd).idTable = idTabIndPart
                 TabIstr.BlkIstr(indEnd).Tavola = nomeTabIndPart
            '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
                 testo = testo & CostruisciSelect("T" & indEnd, idTabIndPart)
              End If
              'SQ 1-08-06 - C.C. "D"
              'If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
              '  testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom_D()
              'Else
                testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom(1, indEnd, True)
              'End If
              'SP 12/9: altro gioco di prestigio per gli indici particolari...
              'tutto da mettere meglio nella versione definitiva
              indPart = False
              If nomeTabIndPart <> "" Then
                 TabIstr.BlkIstr(indEnd - 1).idTable = idTabIndPart
                 TabIstr.BlkIstr(indEnd - 1).Tavola = nomeTabIndPart
                 TabIstr.BlkIstr(indEnd).idTable = idSave
                 TabIstr.BlkIstr(indEnd).Tavola = tabSave
                 'proviamo con la squalificata
                 ReDim Preserve TabIstr.BlkIstr(indEnd).KeyDef(1)
                 indPart = True
              End If
             testo = testo & CostruisciWhere(1, indEnd, True)
              If nomeTabIndPart <> "" Then
                 'rimette a posto
                 indEnd = indEnd - 1
                 ReDim Preserve TabIstr.BlkIstr(indEnd)
                 TabIstr.BlkIstr(indEnd).idTable = idSave
                 TabIstr.BlkIstr(indEnd).Tavola = tabSave
                 indPart = False
              End If
             If K > 1 Then
               TabIstr.BlkIstr(indEnd).Tavola = tabSave
               TabIstr.BlkIstr(indEnd).idTable = idSave
             End If
          
             testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
             testo = testo & Space(11 + indentGNsq + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
          
             If testoRed(K) <> "" Then
                testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
             End If
           Next
           'testo = testo & Space(11 + indentGNsq) & "END-EXEC" & vbCrLf
           'testo = testo & Space(11 + indentGNsq) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
           'SP 9/9 pcb dinamico
              testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
              testo = testo & vbCrLf
  '            If m_dllFunctions.FnNomeDB = "banca.mty" Then
  '               testo = testo & Space(11 + indentGN) & "IF LNKDB-STKTBNUM(LNKDB-NUMSTACK) = 'N'" & vbCrLf
  '            Else
                 testo = testo & Space(11 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
  '            End If

        Else
           testo = testo & vbCrLf
        End If
      Case "GNP"
        testo = testo & vbCrLf
      Case "GU"
        testo = testo & vbCrLf
     End Select
     
     For K = 1 To indRed
        indentRed = 0
        If testoRed(K) <> "" Then
           testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
           indentRed = 3
        End If
        testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & vbCrLf
        testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        If testoRed(K) <> "" Then
           testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
        End If
     Next
  
   '  testo = testo & Space(11 + indentGNsq) & "EXEC SQL OPEN " & CursorName & vbCrLf
   '  testo = testo & Space(11 + indentGNsq) & "END-EXEC" & vbCrLf
     Select Case tipoIstr
      Case "GN"
        'If (Not SwQual Or Not keySecFlagUltimoLiv) And Not keyOp Then
           'SP 9/9 pcb dinamico
        If Not keyOp Then
          testo = testo & Space(11 + indentGN) & "ELSE" & vbCrLf
          For K = 1 To indRed
            indentRed = 0
            If testoRed(K) <> "" Then
               testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
               indentRed = 3
            End If
            testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & "GU" & vbCrLf
            testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
            If testoRed(K) <> "" Then
               testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
            End If
          Next
           'testo = testo & Space(11 + indentGNsq) & "EXEC SQL OPEN " & CursorName & "GU" & vbCrLf
           'testo = testo & Space(11 + indentGNsq) & "END-EXEC" & vbCrLf
           testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
        End If
      Case "GNP"
      Case "GU"
     End Select
     
     testo = testo & Space(11 + indentGN) & "IF SQLCODE = -502" & vbCrLf
     Select Case tipoIstr
      Case "GN"
        'If (Not SwQual Or Not keySecFlagUltimoLiv) And Not keyOp Then
        'SP 9/9 dynpcb
        If Not keyOp Then
  '         If m_dllFunctions.FnNomeDB = "banca.mty" Then
  '            testo = testo & Space(14 + indentGN) & "IF LNKDB-STKTBNUM(LNKDB-NUMSTACK) = 'N'" & vbCrLf
  '         Else
              testo = testo & Space(14 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
  '         End If
        End If
      Case "GNP"
      Case "GU"
     End Select
     For K = 1 To indRed
        indentRed = 0
        If testoRed(K) <> "" Then
           testo = testo & Space(14 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
           indentRed = 3
        End If
        testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & vbCrLf
        testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & vbCrLf
        testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        If testoRed(K) <> "" Then
           testo = testo & Space(14 + indentGNsq) & "END-IF" & vbCrLf
        End If
     Next
     Select Case tipoIstr
      Case "GN"
        'If (Not SwQual Or Not keySecFlagUltimoLiv) And Not keyOp Then
        'SP 9/9 dynpcb
        If Not keyOp Then
          testo = testo & Space(14 + indentGN) & "ELSE" & vbCrLf
          For K = 1 To indRed
            indentRed = 0
            If testoRed(K) <> "" Then
               testo = testo & Space(14 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
               indentRed = 3
            End If
            testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & "GU" & vbCrLf
            testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
            testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & "GU" & vbCrLf
            testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
            If testoRed(K) <> "" Then
               testo = testo & Space(14 + indentGNsq) & "END-IF" & vbCrLf
            End If
          Next
          testo = testo & Space(14 + indentGN) & "END-IF" & vbCrLf
        End If
      Case "GNP"
      Case "GU"
     End Select
     testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
      
     testo = testo & Space(11 + indentGN) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf & vbCrLf
    
    'exit non funzionante
  '   testo = testo & Space(11 + indentGN) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
  '   'testo = testo & Space(14 + indentGN) & "GO TO " & ExRout & vbCrLf
  '   'testo = testo & Space(14 + indentGN) & "EXIT SECTION" & vbCrLf
  '   testo = testo & Space(14 + indentGN) & "EXIT" & vbCrLf
  '   testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
     
     testo = testo & vbCrLf
     
     Select Case tipoIstr
      Case "GN"
        If Not keySecFlagUltimoLiv And Not keyPrimUguale Then
           testo = testo & Space(14) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
           testo = testo & Space(14) & "MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
  '         If m_dllFunctions.FnNomeDB <> "banca.mty" Then
              'testo = testo & Space(14) & "MOVE LNKDB-KEYVALUE(" & indEnd & ", 1) TO LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
  '         End If
          'SQ 31-07-06 - C.C. "F"
          If InStr(instrLevel.CodCom, "F") Then
            'Sistemare INDENT!
          Else
            testo = testo & Space(11) & "END-IF" & vbCrLf
          End If
        Else
          'SQ D
          If level = UBound(TabIstr.BlkIstr) Then
            'fetch in pi�, valida su chiavi secondarie!!
            'SP 11/9: e su primarie per uguale
            'SP 14/9: problema su GHU+REPL+GHN+REPL
            testo = testo & Space(14) & "IF WK-SQLCODE = ZERO" & vbCrLf
            testo = testo & Space(14) & "   IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) = 'GU' OR 'ISRT'" & vbCrLf
            testo = testo & Space(14) & "                           OR 'GN' OR 'REPL' OR 'DLET'" & vbCrLf
            'testo = testo & Space(17) & "AND LNKDB-STKSSA(LNKDB-NUMSTACK) NOT < " & vbCrLf
            'testo = testo & Space(17) & "    LNKDB-KEYVALUE(1, 1)" & vbCrLf
            testo = testo & Space(6) & "***" & " FETCH TO EXCLUDE FIRST RECORD, USED IN LAST GU" & vbCrLf
            'SQ D
            'testo = testo & Space(20) & "PERFORM 9999-FETCH-" & CursorName & "" & vbCrLf
            testo = testo & Space(20) & "PERFORM 9999-FETCH-" & CursorName & vbCrLf
            testo = testo & Space(20) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
            testo = testo & Space(20) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
            testo = testo & Space(20) & "   MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
            'testo = testo & Space(23) & "               LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
            For K = 1 To indRed
              indentRed = 0
              If testoRed(K) <> "" Then
                 testo = testo & Space(23) & Trim(testoRed(K)) & vbCrLf
                 indentRed = 3
              End If
              testo = testo & Space(20 + indentRed) & "   EXEC SQL CLOSE " & CursorName & Format(K, "00") & IIf(keyOp, "", "GU") & vbCrLf
              testo = testo & Space(20 + indentRed) & "   END-EXEC" & vbCrLf
              If testoRed(K) <> "" Then
                testo = testo & Space(23) & "END-IF" & vbCrLf
              End If
            Next
            'testo = testo & Space(23) & "GO TO " & ExRout & vbCrLf
            'testo = testo & Space(23) & "EXIT SECTION" & vbCrLf
            'exit non funzionante
            'testo = testo & Space(23) & "EXIT" & vbCrLf
            testo = testo & Space(14) & "      END-IF" & vbCrLf
            testo = testo & Space(14) & "   END-IF" & vbCrLf
            testo = testo & Space(14) & "END-IF" & vbCrLf
          End If  'D
          
          'testo = testo & Space(17) & "MOVE LNKDB- KEYVALUE(" & indEnd & ", 1) TO LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
          testo = testo & _
            Space(14) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
            Space(14) & "   MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf & _
            Space(14) & "   MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf & _
            Space(14) & "END-IF" & vbCrLf
          'SQ 31-07-06 - C.C. "F"
          If InStr(instrLevel.CodCom, "F") Then
            'Sistemare INDENT!
          Else
            testo = testo & Space(11) & "END-IF" & vbCrLf
          End If
        End If
      Case "GNP"
        'SQ 31-07-06 - C.C. "F"
        If InStr(instrLevel.CodCom, "F") Then
          'Sistemare INDENT!
        Else
          testo = testo & Space(11) & "END-IF" & vbCrLf
        End If
      Case "GU"
     End Select
    
    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    testo = testo & Space(11) & "   PERFORM 9999-FETCH-" & CursorName & vbCrLf
    testo = testo & Space(11) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf  'SQ - 29-08
    testo = testo & Space(11) & "END-IF" & vbCrLf & vbCrLf
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' TESTO FETCH:
    ' Viene scritto in fondo alla routine, in getRoutine_DB2
    ' Se ho pi� livelli, appendo il testo alle fecth dei precedenti
    ' SQ - si pu� isolare?
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    GbTestoFetch = GbTestoFetch & Space(6) & "*    FETCH RELATED TO ROUT-" & Gistruzione & vbCrLf
    GbTestoFetch = GbTestoFetch & Space(7) & "9999-FETCH-" & CursorName & " SECTION." & vbCrLf
     
     Select Case tipoIstr
      Case "GN"
        If Not keyOp Then
          GbTestoFetch = GbTestoFetch & Space(11) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
        End If
      Case "GNP"
      Case "GU"
     End Select
     
     For K = 1 To indRed
        indentRed = 0
        If testoRed(K) <> "" Then
           GbTestoFetch = GbTestoFetch & Space(11 + indentGNqu) & Trim(testoRed(K)) & vbCrLf
           indentRed = 3
        End If
        'stefanopippo: 17/08/2005: initialize area prima della lettura
        GbTestoFetch = GbTestoFetch & Space(11 + indentGNqu + indentRed) & "INITIALIZE "
        'SP 4/9 initialize indicatori di null
         Dim testoIndicatori
'''MG 240107
'''''         Set tb1 = m_dllFunctions.Open_Recordset("select * " & _
'''''             "from dmdb2_tabelle as a, dmdb2_colonne as b " & _
'''''             "where a.idorigine = " & idRed(K) & _
'''''             " and a.idtable = b.idtable and [null] = -1")
         Set tb1 = m_dllFunctions.Open_Recordset("select * " & _
             "from " & DB & "tabelle as a, " & DB & "colonne as b " & _
             "where a.idorigine = " & idRed(K) & _
             " and a.idtable = b.idtable and [null] = -1")

        If tb1.RecordCount > 0 Then
            testoIndicatori = " " & "WK-I-" & SostUnderScore(nomeTRed(K))
        Else
            testoIndicatori = ""
        End If
        GbTestoFetch = GbTestoFetch & "RR-" & SostUnderScore(nomeTRed(K)) & testoIndicatori & vbCrLf
        indentGen = indentGN + indentRed
        GbTestoFetch = GbTestoFetch & vbCrLf & Space(11 + indentGNqu + indentRed) _
                                    & "EXEC SQL FETCH " & CursorName & Format(K, "00") & vbCrLf
           
        'SQ 27-07-06 - C.C. "D"
        'If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
        '  GbTestoFetch = GbTestoFetch & Space(12 + indentGNqu + indentRed) & "INTO " & vbCrLf & CostruisciInto_D()
        'Else
          indPart = Len(nomeTabIndPart) > 0 'SQ - Attenzione: utilizzato dal CostruisciInto!
        
          GbTestoFetch = GbTestoFetch & Space(12 + indentGNqu + indentRed) & "INTO " & vbCrLf _
                               & CostruisciInto("T" & indEnd, idRed(K), nomeTRed(K), "GU")
          indPart = False
        'End If
        
        If Len(nomeTabIndPart) Then
           indEnd = indEnd + 1
           '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
           GbTestoFetch = GbTestoFetch & CostruisciInto("T" & indEnd, idTabIndPart, nomeTabIndPart, "GU")
           indEnd = indEnd - 1
        End If
        GbTestoFetch = GbTestoFetch & Space(11 + indentGNqu + indentRed) & "END-EXEC" & vbCrLf
        
        If testoRed(K) <> "" Then
           GbTestoFetch = GbTestoFetch & Space(11 + indentGNqu) & "END-IF" & vbCrLf
        End If
     Next
     
     Select Case tipoIstr
      Case "GN"
        'If (Not SwQual Or Not keySecFlagUltimoLiv) And Not keyOp Then
        'SP 9/9 dynpcb
        If Not keyOp Then
           GbTestoFetch = GbTestoFetch & Space(11) & "ELSE" & vbCrLf
           For K = 1 To indRed
              indentRed = 0
              If testoRed(K) <> "" Then
                 GbTestoFetch = GbTestoFetch & Space(11 + indentGN) & Trim(testoRed(K)) & vbCrLf
                 indentRed = 3
              End If
        'stefanopippo: 17/08/2005: initialize area prima della lettura
  '            If m_dllFunctions.FnNomeDB = "banca.mty" Then
  '               GbTestoFetch = GbTestoFetch & Space(11 + indentGN + indentRed) & "INITIALIZE "
  '               GbTestoFetch = GbTestoFetch & Left(nomeTRed(k), 2) & "N"
  '               GbTestoFetch = GbTestoFetch & Mid(nomeTRed(k), 10) & " "
  '               GbTestoFetch = GbTestoFetch & Left(nomeTRed(k), 2) & "N"
  '               GbTestoFetch = GbTestoFetch & Mid(nomeTRed(k), 10) & "-N" & vbCrLf
  '            Else
                 GbTestoFetch = GbTestoFetch & Space(11 + indentGN + indentRed) & "INITIALIZE "
       'SP 4/9 initialize indicatori di null
'''MG 240107
'''''                 Set tb1 = m_dllFunctions.Open_Recordset("select * " & _
'''''                  "from dmdb2_tabelle as a, dmdb2_colonne as b " & _
'''''                  "where a.idorigine = " & idRed(K) & _
'''''                  " and a.idtable = b.idtable and [null] = -1")
                 Set tb1 = m_dllFunctions.Open_Recordset("select * " & _
                  "from " & DB & "tabelle as a, " & DB & "colonne as b " & _
                  "where a.idorigine = " & idRed(K) & _
                  " and a.idtable = b.idtable and [null] = -1")

                 If tb1.RecordCount > 0 Then
                    testoIndicatori = " " & "WK-I-" & SostUnderScore(nomeTRed(K))
                 Else
                    testoIndicatori = ""
                 End If
                 GbTestoFetch = GbTestoFetch & "RR-" & SostUnderScore(nomeTRed(K)) & testoIndicatori & vbCrLf
  '            End If
              indentGen = indentGN + indentRed
              GbTestoFetch = GbTestoFetch & Space(11 + indentGN + indentRed) _
                           & "EXEC SQL FETCH " & CursorName & Format(K, "00") & "GU" & vbCrLf
              indPart = False
              If nomeTabIndPart <> "" Then
                 indPart = True
              End If
              GbTestoFetch = GbTestoFetch & Space(12 + indentGN + indentRed) & "INTO " & vbCrLf & CostruisciInto("T" & indEnd, idRed(K), nomeTRed(K), "GU") & vbCrLf
              
              indPart = False
              If nomeTabIndPart <> "" Then
                 indEnd = indEnd + 1
           '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
                 GbTestoFetch = GbTestoFetch & CostruisciInto("T" & indEnd, idTabIndPart, nomeTabIndPart, "GU") & vbCrLf
                 indEnd = indEnd - 1
              End If
              
              GbTestoFetch = GbTestoFetch & Space(11 + indentGN + indentRed) & "END-EXEC" & vbCrLf
         
              If testoRed(K) <> "" Then
                 GbTestoFetch = GbTestoFetch & Space(11 + indentGN) & "END-IF" & vbCrLf
              End If
           Next
           GbTestoFetch = GbTestoFetch & Space(11) & "END-IF" & vbCrLf
        End If
      Case "GNP"
      Case "GU"
     End Select
     
    GbTestoFetch = GbTestoFetch & Space(11) & "." & vbCrLf
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
     
    'OCCURS
    If tipoIstrIniz <> "ISRT" Then
      'purtroppo devo fare cos� a quest'ora....
      Dim tbOcc As Recordset
      
'''''      Set tbOcc = m_dllFunctions.Open_Recordset("select *" & _
'''''          " from dmdb2_tabelle as a, dmdb2_index as b" & _
'''''          " where a.idtable = b.idtable and a.idtablejoin = " & instrLevel.idTable & _
'''''          " and a.TAG like '%OCCURS%' and b.tipo = 'P' order by a.nome")
      Set tbOcc = m_dllFunctions.Open_Recordset("select *" & _
          " from " & DB & "tabelle as a, " & DB & "index as b" & _
          " where a.idtable = b.idtable and a.idtablejoin = " & instrLevel.idTable & _
          " and a.TAG like '%OCCURS%' and b.tipo = 'P' order by a.nome")

      If Not tbOcc.EOF Then
        Dim IdxTabOk2 As Integer
        tabFound2 = False
        For IdxTabOk2 = 1 To idxTabOkMax2
           If tabOk2(IdxTabOk2) = instrLevel.idTable And istrOk2(IdxTabOk2) = "SELT" Then
              tabFound2 = True
              Exit For
           End If
        Next
        If Not tabFound2 Then
           idxTabOkMax2 = idxTabOkMax2 + 1
           ReDim Preserve tabOk2(idxTabOkMax2)
           ReDim Preserve istrOk2(idxTabOkMax2)
           ReDim Preserve istrOkDet2(idxTabOkMax2)
           tabOk2(idxTabOkMax2) = instrLevel.idTable
           istrOk2(idxTabOkMax2) = "SELT"
           istrOkDet2(idxTabOkMax2) = tipoIstr
        End If
         idSave = instrLevel.idTable
         flagOcc = False
         If Not tabFound2 Then
            Dim nomeTab As String
            nomeTab = instrLevel.Tavola
            istrSave = TabIstr
            ReDim TabIstr.BlkIstr(1)
            'withold non serve nelle occurs
            GbTestoRedOcc = GbTestoRedOcc & costruisciOccurs(instrLevel.idTable, instrLevel.pKey, 0, False, flagOcc, False, nomeTab)
            TabIstr = istrSave
            'stefano: poi lo aggiustiamo con get environment (bisogna fare il
            ' parametro per le nuove copy P: P[tipo-istr][ALIAS]
            includedMember = "PS" & Right(getAlias_tabelle(Int(idSave)), 6)
         Else
            GbTestoRedOcc = ""
            includedMember = ""
         End If
  'patch
        tabFound3 = False
        For IdxTabOk2 = 1 To idxTabOkMax2
          'SQ 25-07-06 - pezza su pezza:
          'non so assolutamente cos'� 'sta roba, in ogni caso qualche volta "istrOkDet2" � pi� corto
          'e va in errore!
          'If tabOk2(IdxTabOk2) = idSave And istrOk2(IdxTabOk2) = "SELT" And istrOkDet2(IdxTabOk2) = Right(nomeroutine, 4) Then
          If tabOk2(IdxTabOk2) = idSave And istrOk2(IdxTabOk2) = "SELT" And UBound(istrOkDet2) >= IdxTabOk2 Then
            If istrOkDet2(IdxTabOk2) = Right(nomeroutine, 4) Then
              tabFound3 = True
              Exit For
            End If
          End If
        Next
        If Not tabFound3 Then
          idxTabOkMax2 = idxTabOkMax2 + 1
           ReDim Preserve tabOk2(idxTabOkMax2)
           ReDim Preserve istrOk2(idxTabOkMax2)
           ReDim Preserve istrOkDet2(idxTabOkMax2)
          tabOk2(idxTabOkMax2) = idSave
          istrOk2(idxTabOkMax2) = "SELT"
          istrOkDet2(idxTabOkMax2) = Right(nomeroutine, 4)
          'stefano: poi lo aggiustiamo con get environment (bisogna fare il
          ' parametro per le nuove copy P: P[tipo-istr][ALIAS]
          includedMember = "PS" & Right(getAlias_tabelle(Int(idSave)), 6)
        End If
       End If
     End If
     
     'SQ 29-08 - portato sopra!
     'testo = testo & Space(11) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
     'testo = testo & Space(11) & "END-IF" & vbCrLf & vbCrLf
     
     Select Case tipoIstr
      Case "GN"
        testo = testo & Space(11) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
        testo = testo & Space(11) & "   MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        'testo = testo & Space(14) & "               LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
        If Not keyOp Then
          testo = testo & Space(14) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
        End If
      Case "GNP"
        testo = testo & Space(11) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
        testo = testo & Space(11) & "   MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        'testo = testo & Space(14) & "               LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
      Case "GU"
     End Select
     For K = 1 To indRed
        indentRed = 0
        If testoRed(K) <> "" Then
           testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
           indentRed = 3
        End If
        testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & vbCrLf
        testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        If testoRed(K) <> "" Then
           testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
        End If
     Next
     Select Case tipoIstr
      Case "GN"
        'If (Not SwQual Or Not keySecFlagUltimoLiv) And Not keyOp Then
        'SP 9/9 dynpcb
        If Not keyOp Then
           testo = testo & Space(14) & "ELSE" & vbCrLf
           For K = 1 To indRed
              If testoRed(K) <> "" Then
                 testo = testo & Space(17) & Trim(testoRed(K)) & vbCrLf
                 indentRed = 3
              End If
              testo = testo & Space(17 + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & "GU" & vbCrLf
              testo = testo & Space(17 + indentRed) & "END-EXEC" & vbCrLf
              If testoRed(K) <> "" Then
                 testo = testo & Space(17) & "END-IF" & vbCrLf
              End If
           Next
           testo = testo & Space(14) & "END-IF" & vbCrLf
        End If
       'testo = testo & Space(14) & "GO TO " & ExRout & vbCrLf
       'testo = testo & Space(14) & "EXIT SECTION" & vbCrLf
  '     testo = testo & Space(14) & "EXIT" & vbCrLf
       testo = testo & Space(11) & "END-IF" & vbCrLf
      Case "GNP"
       'testo = testo & Space(14) & "GO TO " & ExRout & vbCrLf
       'testo = testo & Space(14) & "EXIT SECTION" & vbCrLf
  '     testo = testo & Space(14) & "EXIT" & vbCrLf
       testo = testo & Space(11) & "END-IF" & vbCrLf
      Case "GU"
  '     testo = testo & Space(11) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
  '    'testo = testo & Space(14) & "GO TO " & ExRout & vbCrLf
  '    'testo = testo & Space(14) & "EXIT SECTION" & vbCrLf
  '     testo = testo & Space(14) & "EXIT" & vbCrLf
  '     testo = testo & Space(11) & "END-IF" & vbCrLf
     End Select
     testo = testo & vbCrLf
     
     'loop sulle tabelle ridefinite: nome tabella gi� mosso da template in copy move
     'SP 12/9: per ora non gestisce gli indici particolari
     
      'stefano: adesso che funzionava!!!!
      tabFound = False
      If flagSel Then 'solo per le redefines
          Dim IdxTabOk As Integer
          For IdxTabOk = 1 To idxTabOkMax
             If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "SELT" Then
                tabFound = True
                Exit For
             End If
          Next
          If Not tabFound Then
             idxTabOkMax = idxTabOkMax + 1
             ReDim Preserve tabOk(idxTabOkMax)
             ReDim Preserve istrOk(idxTabOkMax)
             ReDim Preserve istrOkDet(idxTabOkMax)
             tabOk(idxTabOkMax) = nomeTRed(1)
             istrOk(idxTabOkMax) = "SELT"
             istrOkDet(idxTabOkMax) = tipoIstr
          End If
      End If
          
      'gestione perform: da provare
     If tabFound Then
      GbTestoRedOcc = ""
      includedMember = ""
     End If
     
     If flagSel Then
        testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
        testo = testo & Space(11) & "   MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
        'testo = testo & Space(14) & "PERFORM SETK-ADLWKEY THRU SETK-ADLWKEY-EX" & vbCrLf
        'testo = testo & Space(14) & "PERFORM " & nomeSRed(1) & "-TO-RD THRU " & nomeSRed(1) & "-TO-RD-EX" & vbCrLf
        testo = testo & Space(11) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf
        'SQ 31-07-06 - C.C. "D"
        'If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
        '  For i = 1 To UBound(TabIstr.BlkIstr)
        '    testo = testo & Space(11) & "   PERFORM 9999-" & Replace(TabIstr.BlkIstr(i).Tavola, "_", "-") & "-TO-RD" & vbCrLf
        '  Next
        'Else
          'testo = testo & Space(14) & "PERFORM 9999-" & nomeSRed(1) & "-TO-RD" & vbCrLf
          testo = testo & Space(11) & "   PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-TO-RD" & vbCrLf
        'End If
        testo = testo & Space(11) & "END-IF" & vbCrLf
        
  '      'testo = testo & Space(11) & "MOVE KRR-" & NomePk & " TO K01" & "-" & NomePk & vbCrLf
  '      testo = testo & vbCrLf
  '      testo = testo & Space(6) & "*** REDEFINES" & vbCrLf
  '      testo = testo & Space(11) & "PERFORM 9999-" & SostUnderScore(instrLevel.Tavola) & "-SELT" & vbCrLf
  '      'testo = testo & vbCrLf
     End If
      
     'If flagSel Then
     If flagSel And Not tabFound Then
        'stefano: poi lo aggiustiamo con get environment (bisogna fare il
        ' parametro per le nuove copy P: P[tipo-istr][ALIAS]
        includedMember = "PS" & Right(getAlias_tabelle(Int(instrLevel.idTable)), 6)
        '...
        'stefano: provo a mettere il giochino sul cambio nome chiave solo qui, dovrebbe servire solo per i casi di dispatcher
        'ATTENZIONE: BISOGNEREBBE SALVARE TUTTA LA STRUTTURA, SI FAREBBE PRIMA,
        ' E SAREBBE PIU' SICURO, MA NON SO COME FARE....
        idSave = instrLevel.idTable
       
       GbTestoRedOcc = GbTestoRedOcc & Space(6) & "*    SELECT RELATED TO " & nomeTRed(1) & vbCrLf
       GbTestoRedOcc = GbTestoRedOcc & Space(6) & " 9999-" & SostUnderScore(nomeTRed(1)) & "-SELT SECTION." & vbCrLf
       GbTestoRedOcc = GbTestoRedOcc & Space(11) & "MOVE " & nomeFieldRed(1) & " OF RR-" & SostUnderScore(nomeTRed(1)) & vbCrLf
       GbTestoRedOcc = GbTestoRedOcc & Space(11) & "  TO KRD-" & SostUnderScore(nomeKeyRed(1)) & vbCrLf
       
       istrSave = TabIstr
       ReDim TabIstr.BlkIstr(1)
       For K = 2 To indSav
          ReDim TabIstr.BlkIstr(1).KeyDef(1)
          TabIstr.BlkIstr(1).KeyDef(1).key = nomeKeyRed(K)
          TabIstr.BlkIstr(1).KeyDef(1).Op = "="
          'SQ 21-07-06 (errore K00 nelle WHERE)
          TabIstr.BlkIstr(1).KeyDef(1).KeyNum = 1
          TabIstr.BlkIstr(1).Tavola = nomeTRed(K)
          TabIstr.BlkIstr(1).idTable = idRed(K)
          TabIstr.BlkIstr(1).pKey = nomeKeyRed(K)
          
          indentRed = 0
          GbTestoRedOcc = GbTestoRedOcc & vbCrLf & Space(6) & "*** REDEFINES" & vbCrLf
             
          If testoRed(K) <> "" Then
            GbTestoRedOcc = GbTestoRedOcc & Space(11) & Trim(testoRed(K)) & vbCrLf
            indentRed = 3
          End If
          'SQ 26-07-06 problemi come sopra...
          'Nella MOVE fuori "IF" viene riempita la KRD-<nomeKeyRed(1)> !...
          'VERIFICARE!!!!!!!!!!!!!!!!!!!!!
          'GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRD-" & nomeKey & " TO" & vbCrLf
          GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRD-" & nomeKeyRed(1) & " TO" & vbCrLf
          'SQ 7-07-06
          'GbTestoRedOcc = GbTestoRedOcc & Space(16 + indentRed) & "KRD-" & nomeFieldRed(k) & vbCrLf
          GbTestoRedOcc = GbTestoRedOcc & Space(16 + indentRed) & "KRD-" & nomeKeyRed(K) & vbCrLf
          GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
          'lo faccio funzionare per forza
          'SQ 26-07-06 problemi come sopra...
          'GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRR-" & nomeKey & " TO KRR-" & nomeKeyRed(k) & vbCrLf
          GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRR-" & nomeKeyRed(1) & " TO KRR-" & nomeKeyRed(K) & vbCrLf
          'GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
          GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
         'per ora K01 fisso!!!
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRR-" & nomeKeyRed(K) & " TO K01-" & nomeKeyRed(K) & vbCrLf
         
         'manca with hold: da verificare come farla, comunque c'� gi� sulla tabella selettore
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "EXEC SQL SELECT " & vbCrLf _
                                                               & CostruisciSelect("T1", idRed(K))
         GbTestoRedOcc = GbTestoRedOcc & Space(15) & "INTO " & vbCrLf
         GbTestoRedOcc = GbTestoRedOcc & CostruisciInto("T1", idRed(K), nomeTRed(K), "GU")
         GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "FROM " & vbCrLf
         indentGen = indentRed
'        GbTestoRedOcc = GbTestoRedOcc & CostruisciFrom(1, indEnd, True) & vbCrLf
'        GbTestoRedOcc = GbTestoRedOcc & CostruisciWhere(1, indEnd, False, True)
         GbTestoRedOcc = GbTestoRedOcc & CostruisciFrom(1, 1, True) & vbCrLf
         GbTestoRedOcc = GbTestoRedOcc & CostruisciWhere(1, 1, False, True)
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "END-EXEC" & vbCrLf & vbCrLf
         
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
'         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
'         'GbTestoredocc = GbTestoredocc & Space(14 + indentRed) & "GO TO " & ExRout & vbCrLf
'         'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "EXIT SECTION " & vbCrLf
'         GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "EXIT" & vbCrLf
'         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "END-IF" & vbCrLf
         'GbTestoredocc = GbTestoredocc & Space(11 + indentRed) & "PERFORM " & nomeSRed(k) & "-TO-RD THRU " & nomeSRed(k) & "-TO-RD-EX" & vbCrLf
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "   PERFORM 9999-" & SostUnderScore(nomeTRed(K)) & "-TO-RD" & vbCrLf
         'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE RD-" & nomeSRed(k) & " TO LNKDB-DATA-AREA(" & Format(k1) & ")" & vbCrLf
         'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE RD-" & nomeSRed(k) & " TO LNKDB-DATA-AREA(WK-IND2)" & vbCrLf
         'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "MOVE '" & nomeTRed(k) & "' TO WK-NAMETABLE " & vbCrLf
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "END-IF" & vbCrLf
         'SP 15/9 feedback migliorato
         'GbTestoredocc = GbTestoredocc & Space(14 + indentRed) & "PERFORM SETK-ADLWKEY THRU SETK-ADLWKEY-EX" & vbCrLf
         'GbTestoredocc = GbTestoredocc & Space(14 + indentRed) & "PERFORM SETK-WKEYWKUT THRU SETK-WKEYWKUT-EX" & vbCrLf
         'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "PERFORM 9999-SETK-ADLWKEY" & vbCrLf
         'GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "PERFORM 9999-SETK-WKEYWKUT" & vbCrLf
           
  'OCCURS
         If tipoIstrIniz <> "ISRT" Then
  '          Dim keysave2() As BlkIstrKey
  '          keysave2 = instrLevel.KeyDef
            ReDim TabIstr.BlkIstr(1).KeyDef(0)
            nomeTab = nomeTRed(K)
            'withold non serve nelle occurs
            'GbTestoRedOcc = GbTestoRedOcc & costruisciOccurs(idRed(k), nomeTRed(k), indentRed, withold, 1, flagOcc, True, nomeTab)
            'SQ - 21-07-06
            'OCCURS DI REDEFINES: Perch� passa nomeTRed(k) e non pkey?
            ' => provo!
            'GbTestoRedOcc = GbTestoRedOcc & costruisciOccurs(idRed(k), nomeTRed(k), indentRed, False, 1, flagOcc, True, nomeTab)
            GbTestoRedOcc = GbTestoRedOcc & costruisciOccurs(idRed(K), nomeKeyRed(K) & "", indentRed, False, flagOcc, True, nomeTab)
  '          instrLevel.KeyDef = keysave2
         End If
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
         'SQ 31-07-06
         'Modifica a causa del CommandCode "D" (mi servirebbe allineata a destra...)
         'GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "   MOVE RD-" & nomeSRed(k) & " TO LNKDB-DATA-AREA(WK-IND2)" & vbCrLf
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "   MOVE RD-" & nomeSRed(K) & " TO WK-DATA-AREA" & vbCrLf
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "   MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE " & vbCrLf
         GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "END-IF" & vbCrLf
         If testoRed(K) <> "" Then
            GbTestoRedOcc = GbTestoRedOcc & Space(11) & "END-IF" & vbCrLf
         End If
        Next
        'If indSav > 1 Then GbTestoRedOcc = GbTestoRedOcc & Space(11) & "." & vbCrLf
        GbTestoRedOcc = GbTestoRedOcc & Space(11) & "." & vbCrLf
  
  '      instrLevel.KeyDef = keySave
  '
  '      instrLevel.Tavola = tabSave
  '      instrLevel.IdTable = idSave
  '      instrLevel.pKey = idxSave2
        TabIstr = istrSave
     End If
     
  'patch
     If flagSel And tabFound Then
        tabFound3 = False
        For IdxTabOk = 1 To idxTabOkMax
          'SQ 25-07-06 - pezza su pezza:
          'non so assolutamente cos'� 'sta roba, in ogni caso qualche volta "istrOkDet2" � pi� corto
          'e va in errore!
          'If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "SELT" And istrOkDet(IdxTabOk) = Right(nomeroutine, 4) Then
          If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "SELT" And UBound(istrOkDet) >= IdxTabOk Then
            If istrOkDet(IdxTabOk) = Right(nomeroutine, 4) Then
              tabFound3 = True
              Exit For
           End If
          End If
        Next
        If Not tabFound3 Then
           idxTabOkMax = idxTabOkMax + 1
           ReDim Preserve tabOk(idxTabOkMax)
           ReDim Preserve istrOk(idxTabOkMax)
           ReDim Preserve istrOkDet(idxTabOkMax)
           tabOk(idxTabOkMax) = nomeTRed(1) ' sulle occurs uso l'id, qui il nome; funziona, ma � da rivedere
           istrOk(idxTabOkMax) = "SELT"
           istrOkDet(idxTabOkMax) = Right(nomeroutine, 4)
            'stefano: poi lo aggiustiamo con get environment (bisogna fare il
            ' parametro per le nuove copy P: P[tipo-istr][ALIAS]
            includedMember = "PS" & Right(getAlias_tabelle(Int(idSave)), 6)
        End If
     End If
     
     'esce se richiamata dalla insert
     If flagIns Then
      'SQ 21-07-06 - Ereditariet� chiavi per la INSERT successiva (il nome della tabella l'ho messo a casaccio: va bene?
      'MANCAVA IL CONTROLLO SUL WK-SQLCODE: PUO' ESSERE?
      'stefano: secondo me questo pezzo non dovrebbe servire, perch� la KRR � gi� impostata
      ' per la lettura di verifica sul padre.
       'SQ - non so se ho capito che hai detto... ma se leggo per chiave secondaria non ho la KRR!
  '    testo = testo & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
  '    testo = testo & Space(14 + indentRed) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE " & vbCrLf
  '    testo = testo & Space(14 + indentRed) & "PERFORM 9999-SETK-ADLWKEY" & vbCrLf & vbCrLf
  '    testo = testo & Space(11 + indentRed) & "END-IF" & vbCrLf
      getGU_DB2_old = testo
      Exit Function
     End If
     
     k1 = indEnd
     
     NomeT = TabIstr.BlkIstr(k1).Tavola
     NomePk = TabIstr.BlkIstr(k1).pKey
     If TabIstr.BlkIstr(k1).IdSegm <> 0 Then
        Dim indGet As Long
        indGet = indEnd
        wLiv01Copy = getCopyDLISeg(indGet)
        
        If Not flagSel Then
           For K = 1 To indRed
              If testoRed(K) <> "" Then
                 testo = testo & Space(11) & Trim(testoRed(K)) & vbCrLf
                 indentRed = 3
              End If
              'SQ 29-08
              'testo = testo & Space(11 + indentRed) & "PERFORM " & nomeSRed(k) & "-TO-RD THRU " & nomeSRed(k) & "-TO-RD-EX" & vbCrLf
              'SQ 29-08
  '           testo = testo & Space(11 + indentRed) & "PERFORM " & nomeSRed(k) & "-TO-RD THRU " & nomeSRed(k) & "-TO-RD-EX" & vbCrLf
              testo = testo & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
              'SQ 31-07-06 - C.C. "D"
              'If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
              '  For i = 1 To UBound(TabIstr.BlkIstr)
              '    testo = testo & Space(14 + indentRed) & "PERFORM 9999-" & Replace(TabIstr.BlkIstr(i).Tavola, "_", "-") & "-TO-RD" & vbCrLf
              '  Next
              'Else
                testo = testo & Space(14 + indentRed) & "PERFORM 9999-" & Replace(nomeTRed(K), "_", "-") & "-TO-RD" & vbCrLf
              'End If
  '            If k = 1 Then
  '              testo = testo & Space(14 + indentRed) & "MOVE RD-" & nomeSRed(k) & " TO LNKDB-DATA-AREA(" & Format(k1) & ")" & vbCrLf
  '            Else
  '              testo = testo & Space(14 + indentRed) & "MOVE RD-" & nomeSRed(k) & " TO LNKDB-DATA-AREA(WK-IND2)" & vbCrLf
  '            End If
              testo = testo & Space(14 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE " & vbCrLf
              'testo = testo & Space(14 + indentRed) & "PERFORM SETK-ADLWKEY THRU SETK-ADLWKEY-EX" & vbCrLf
              testo = testo & Space(14 + indentRed) & "PERFORM 9999-SETK-ADLWKEY" & vbCrLf
  'la chiave deve essere salvata anche se non c'era il blocco chiavi;
  'ad esempio per una GN totalmente squalificata, seguita
  'da una GNP, deve essere comunque recuperata la chiave letta con la GN
  'SP 5/9 keyfeedback; banca lo mettiamo in seguito
  '            If m_dllFunctions.FnNomeDB <> "banca.mty" Then
                'SP 12/9: indici particolari
                 If Len(nomeTabIndPart) Then
                    testo = testo & Space(6) & "*" & Space(7 + indentRed) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
                 Else
                    testo = testo & Space(6) & "*" & Space(7 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
                 End If
                 'SQ tmp: ci vuole? (commentato)
                 'testo = testo & Space(14 + indentRed) & "PERFORM SETK-WKEYWKUT THRU SETK-WKEYWKUT-EX" & vbCrLf
                 'testo = testo & Space(6) & "*" & Space(4 + indentRed) & "PERFORM SETK-WKEYWKUT THRU SETK-WKEYWKUT-EX" & vbCrLf
                 testo = testo & Space(6) & "*" & Space(7 + indentRed) & "PERFORM 9999-SETK-WKEYWKUT" & vbCrLf
                 If Len(nomeTabIndPart) Then
                    testo = testo & Space(14 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
                 End If
                 'SQ D
                 testo = testo & Space(14 + indentRed) & "MOVE '" & Format(level, "00") & "' TO LNKDB-PCBSEGLV" & vbCrLf
                 
  '            Else
  '               testo = testo & Space(11 + indentRed) & "MOVE '" & nomeTRed(k) & "' TO WK-NAMETABLE" & vbCrLf
  '            End If
              testo = testo & Space(11 + indentRed) & "END-IF" & vbCrLf
              If testoRed(K) <> "" Then
                 testo = testo & Space(11) & "END-IF" & vbCrLf
              End If
           Next
        End If
        'mi sembra inutile, � gi� compresa nella STORE-FDBKEY
        'rimessa, in quanto tolta nella STORE-FDBKEY
        testo = testo & vbCrLf
  'la chiave deve essere salvata anche se non c'era il blocco chiavi;
  'ad esempio per una GN totalmente squalificata, seguita
  'da una GNP, deve essere comunque recuperata la chiave letta con la GN
  'SP 5/9 keyfeedback; banca lo mettiamo in seguito
  '      If m_dllFunctions.FnNomeDB <> "banca.mty" Then
  '           'SP 12/9: indici particolari
  '         If nomeTabIndPart <> "" Then
  '            testo = testo & Space(11) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
  '         Else
  '            testo = testo & Space(11) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
  '         End If
  '         testo = testo & Space(11) & "PERFORM SETK-WKEYWKUT THRU SETK-WKEYWKUT-EX" & vbCrLf
  '         If nomeTabIndPart <> "" Then
  '            testo = testo & Space(11) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
  '         End If
  '      Else
  '         testo = testo & Space(11) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
  '      End If
  'SP 18/9: mancava la move della tabella, problema grosso sulle redefines
              
        testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
        testo = testo & Space(11) & "   MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
        testo = testo & Space(11) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf
        If keySecFlagUltimoLiv Then
        'la chiave secondaria viene salvata sempre su un altro campo, in quanto
        'non si conosce, ovviamente, quale sar� l'istruzione successiva, per cui
        'bisogna averle entrambe in linea per usare quella giusta
  '         If m_dllFunctions.FnNomeDB = "banca.mty" Then
  '            testo = testo & Space(11) & "PERFORM SETA-" & "KY-" & Left(TabIstr.BlkIstr(k1).KeyDef(1).NameAlt, 3) & "-" & Right(TabIstr.BlkIstr(k1).KeyDef(1).NameAlt, 2) & " THRU SETA-" & "KY-" & Left(TabIstr.BlkIstr(1).KeyDef(1).NameAlt, 3) & "-" & Right(TabIstr.BlkIstr(k1).KeyDef(1).NameAlt, 2) & "-EX" & vbCrLf
  '            testo = testo & Space(11) & "MOVE KRR-" & "KY-" & _
  '            Left(TabIstr.BlkIstr(k1).KeyDef(1).NameAlt, 3) & "-" & _
  '            Right(TabIstr.BlkIstr(k1).KeyDef(1).NameAlt, 2) & _
  '            " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)" & vbCrLf
  '         Else
              'testo = testo & Space(14) & "PERFORM SETA-" & Replace(TabIstr.BlkIstr(k1).KeyDef(1).NameAlt, "_", "-") & " THRU SETA-" & Replace(TabIstr.BlkIstr(k1).KeyDef(1).NameAlt, "_", "-") & "-EX" & vbCrLf
              'SQ - come per le KRD: nome indice DB2 (prob. lo cambieremo)
              testo = testo & Space(14) & "PERFORM 9999-SETA-" & Replace(TabIstr.BlkIstr(k1).KeyDef(1).key, "_", "-") & vbCrLf
              'SQ 7-07-06
              'testo = testo & Space(14) & "MOVE KRR-" & TabIstr.BlkIstr(k1).KeyDef(1).NameAlt &_
              testo = testo & Space(14) & "MOVE KRR-" & TabIstr.BlkIstr(k1).KeyDef(1).key & _
              " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)" & vbCrLf
              'SP 5/9 keyfeedback
  '            If m_dllFunctions.FnNomeDB <> "banca.mty" Then
            '13/9 keyfeedbackarea per indici particolari
              'SQ - come per le KRD: nome indice DB2 (prob. lo cambieremo)
              testo = testo & Space(14) & "MOVE KRDP-" & TabIstr.BlkIstr(k1).KeyDef(1).key & _
                      "  TO LNKDB-FDBKEY" & vbCrLf
  '            End If
  '         End If
        End If
        testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
     
    'SP 9/9 pcb dinamico: salvataggio SSA formattata a modo nostro!
    ' per ora solo sulla GU, se non ok sar� fatta solo per qualificate e no pcb dinamico
    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    If tipoIstr = "GU" Then
      testo = testo & Space(14) & "MOVE LNKDB-KEYNAME(" & indEnd & ", 1) TO WK-KEYNAME" & vbCrLf
      testo = testo & Space(14) & "MOVE LNKDB-KEYOPER(" & indEnd & ", 1) TO WK-KEYOPER" & vbCrLf
      testo = testo & Space(14) & "MOVE LNKDB-KEYVALUE(" & indEnd & ", 1) TO WK-KEYVALUE" & vbCrLf
      testo = testo & Space(14) & "MOVE WK-SSA TO LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
    End If
     
    If flagOcc Or tabFound2 Then
      'OCCURS:
      testo = testo & vbCrLf
      testo = testo & Space(6) & "*** OCCURS" & vbCrLf
      testo = testo & Space(14) & "MOVE " & indEnd & " TO WK-IND2" & vbCrLf
      testo = testo & Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      testo = testo & vbCrLf
    End If
    
    If flagSel Then
      'REDEFINES:
      testo = testo & Space(6) & "*** REDEFINES" & vbCrLf
      testo = testo & Space(14) & "MOVE " & indEnd & " TO WK-IND2" & vbCrLf
      testo = testo & Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      'SQ 31-07-06
      'Modificata gestione MOVE finale nelle routine di REDEFINES (a causa del CommandCode "D"...)
      'ES: MOVE RD-... TO LNKDB-DATA-AREA(WK-IND2) => MOVE RD-... TO DATA-AREA
      'Ho letto le io-aree di tutti i livelli: tutte (l'ultima letta nella routine in copy) nella LNKDB-DATA-AREA
      testo = testo & Space(14) & "MOVE WK-DATA-AREA TO RD-" & instrLevel.Segment & vbCrLf
      
      'SQ D
      'Tmp: ok, per ora, ripeterla per ogni livello...
      'If level = UBound(TabIstr.BlkIstr) Then
        If InStr(TabIstr.BlkIstr(1).CodCom, "D") And indEnd > 1 Then
          'STRING:
          testo = testo & Space(14) & "STRING  RD-" & TabIstr.BlkIstr(1).Segment & vbCrLf
          For i = 2 To indEnd
            testo = testo & Space(14) & "        RD-" & TabIstr.BlkIstr(i).Segment & vbCrLf
          Next
          testo = testo & Space(14) & "  DELIMITED BY SIZE" & vbCrLf
          testo = testo & Space(14) & "  INTO LNKDB-DATA-AREA(" & indEnd & ")" & vbCrLf
        Else
          'MOVE:
          testo = testo & Space(14) & "MOVE RD-" & instrLevel.Segment & " TO LNKDB-DATA-AREA(" & indEnd & ")" & vbCrLf
        End If
      'End If
    Else
      'SQ 31-07-06 - C.C. "D"
      'SQ D
      'Tmp: ok, per ora, ripeterla per ogni livello...
      'If level = UBound(TabIstr.BlkIstr) Then
        If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
          'Ho letto le io-aree di tutti i livelli: tutte nella LNKDB-DATA-AREA
          testo = testo & Space(14) & "STRING  RD-" & TabIstr.BlkIstr(1).Segment & vbCrLf
          For i = 2 To indEnd
            testo = testo & Space(14) & "        RD-" & TabIstr.BlkIstr(i).Segment & vbCrLf
          Next
          testo = testo & Space(14) & "  DELIMITED BY SIZE" & vbCrLf
          testo = testo & Space(14) & "  INTO LNKDB-DATA-AREA(" & UBound(TabIstr.BlkIstr) & ")" & vbCrLf
        Else
          testo = testo & Space(14) & "MOVE RD-" & instrLevel.Segment & " TO LNKDB-DATA-AREA(" & UBound(TabIstr.BlkIstr) & ")" & vbCrLf
        End If
      'End If
    End If
    testo = testo & Space(11) & "END-IF" & vbCrLf
    
    'SQ D
    If InStr(TabIstr.BlkIstr(1).CodCom, "D") And level > 1 Then
      'Lettura solo se il precedente livello andato a buon fine
      testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
    testo = testo & vbCrLf
  
    '''''''''''''''''''''''
    ' Scrittura Copy P
    '''''''''''''''''''''''
    If Len(GbTestoRedOcc) Then
      Dim rtbFileSave As String
      rtbFileSave = rtbFile.text  'salvo
      rtbFile.text = GbTestoRedOcc
      rtbFile.SaveFile VSAM2Rdbms.drPathDb & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\cpy\" & includedMember, 1
      rtbFile.text = rtbFileSave
      GbTestoRedOcc = ""  'flag x chiamante
      'transitorio: buttare
      GbFileRedOcc = ""
    End If
    If Len(includedMember) Then 'Verificare... se serve utilizzare Collection...
      GbFileRedOccs(UBound(GbFileRedOccs)) = includedMember
      ReDim Preserve GbFileRedOccs(UBound(GbFileRedOccs) + 1)
    End If
  Next level
   
  getGU_DB2_old = testo

End Function

Function costruisciOccurs(idTab As Long, pKey As String, indentRed As Variant, withold As Boolean, flagOcc As Boolean, Redef As Boolean, nomeTab As String) As String
  Dim tbOcc As Recordset
  Dim idxOcc As Integer
  Dim testo As String
  Dim testoIndicatori
  Dim tb1 As Recordset
  
  'SQ 21-07-06 - Problema lunghezza nome cursore (max 18 char)
  Const cursorSuffix = "OC"

'''MG 240107
'''''  Set tbOcc = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab, b.nome as nomeidx, c.id_depending_on as idcmpon" & _
'''''      " from dmdb2_tabelle as a, dmdb2_index as b, mgdata_cmp as c" & _
'''''      " where a.idtable = b.idtable and a.idtablejoin = " & idTab & _
'''''      " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")
  Set tbOcc = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab, b.nome as nomeidx, c.id_depending_on as idcmpon" & _
      " from " & DB & "tabelle as a, " & DB & "index as b, mgdata_cmp as c" & _
      " where a.idtable = b.idtable and a.idtablejoin = " & idTab & _
      " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")
      
  flagOcc = False
  If Not tbOcc.EOF And Not Redef Then
     testo = testo & Space(7) & "9999-" & Replace(nomeTab, "_", "-") & "-SELT SECTION." & vbCrLf
  End If
    
  While Not tbOcc.EOF
     flagOcc = True
     idxOcc = idxOcc + 1
     testo = testo & vbCrLf
     testo = testo & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
     'SQ 24-07 - Modifica Virgilio
     testo = testo & Space(6) & "*** OCCURS" & vbCrLf
     testo = testo & Space(14 + indentRed) & "MOVE '9999-" & Replace(nomeTab, "_", "-") & "' TO WK-LABEL-ERR" & vbCrLf
     testo = testo & Space(14 + indentRed) & "MOVE KRR-" & Replace(pKey, "_", "-") & " TO K01-" & Replace(tbOcc!Nomeidx, "_", "-") & vbCrLf
     testo = testo & Space(14 + indentRed) _
                & "EXEC SQL DECLARE " & Replace(tbOcc!nomeTab, "_", "") & cursorSuffix & " CURSOR " _
                & IIf(withold, "WITH HOLD", "") & vbCrLf & Space(14 + indentRed) & " FOR SELECT " & vbCrLf _
                & CostruisciSelect("T1", tbOcc!idTab)
'                & CostruisciSelect("T" & indEnd, tbOcc!idTab)
''     idSave = TabIstr.BlkIstr(indEnd).IdTable
''     tabSave = TabIstr.BlkIstr(indEnd).Tavola
''     idxSave2 = TabIstr.BlkIstr(indEnd).PKey
'     TabIstr.BlkIstr(indEnd).Tavola = tbOcc!nomeTab
'     TabIstr.BlkIstr(indEnd).IdTable = tbOcc!idTab
''     keySave = TabIstr.BlkIstr(indEnd).KeyDef
'     ReDim TabIstr.BlkIstr(indEnd).KeyDef(0)
     TabIstr.BlkIstr(1).Tavola = tbOcc!nomeTab
     TabIstr.BlkIstr(1).idTable = tbOcc!idTab
     ReDim TabIstr.BlkIstr(1).KeyDef(0)

'     testo = testo & Space(15 + indentRed) & "FROM " & vbCrLf _
'                      & CostruisciFrom(1, indEnd, True)
'     testo = testo & CostruisciWhere(1, indEnd)
     testo = testo & Space(15 + indentRed) & "FROM " & vbCrLf _
                      & CostruisciFrom(1, 1, True)
     testo = testo & CostruisciWhere(1, 1)
     'testo = testo & CostruisciWhere(1, indEnd, False, True)
     
'     TabIstr.BlkIstr(indEnd).Tavola = tabSave
'     TabIstr.BlkIstr(indEnd).IdTable = idSave
'     TabIstr.BlkIstr(indEnd).KeyDef = keySave
  
     testo = testo & Space(14 + indentRed) & "END-EXEC" & vbCrLf
     'testo = testo & Space(14 + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf

     testo = testo & Space(14 + indentRed) & "EXEC SQL OPEN " & Replace(tbOcc!nomeTab, "_", "") & cursorSuffix & vbCrLf
     testo = testo & Space(14 + indentRed) & "END-EXEC" & vbCrLf
     testo = testo & Space(14 + indentRed) & "IF SQLCODE = -502" & vbCrLf
     testo = testo & Space(17 + indentRed) & "EXEC SQL CLOSE " & Replace(tbOcc!nomeTab, "_", "") & cursorSuffix & vbCrLf
     testo = testo & Space(17 + indentRed) & "END-EXEC" & vbCrLf
     testo = testo & Space(17 + indentRed) & "EXEC SQL OPEN " & Replace(tbOcc!nomeTab, "_", "") & cursorSuffix & vbCrLf
     testo = testo & Space(17 + indentRed) & "END-EXEC" & vbCrLf
     testo = testo & Space(14 + indentRed) & "END-IF" & vbCrLf
     'testo = testo & Space(14 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
'     testo = testo & Space(14 + indentRed) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
'     testo = testo & Space(17 + indentRed) & "PERFORM 9999-FETCH-" & Replace(tbOcc!nomeTab, "_", "") & cursorSuffix & vbCrLf
'     testo = testo & Space(14 + indentRed) & "END-IF" & vbCrLf
     
       Dim testoFetch As String
       testoFetch = ""
    
    testoFetch = testoFetch & Space(17 + indentRed) & "INITIALIZE "
      'SP 4/9 initialize indicatori di null
'''MG 240107
'''''    Set tb1 = m_dllFunctions.Open_Recordset("select * " & _
'''''              "from dmdb2_tabelle as a, dmdb2_colonne as b " & _
'''''              "where a.idorigine = " & tbOcc!idTab & _
'''''              " and a.idtable = b.idtable and [null] = -1")
    Set tb1 = m_dllFunctions.Open_Recordset("select * " & _
              "from " & DB & "tabelle as a, " & DB & "colonne as b " & _
              "where a.idorigine = " & tbOcc!idTab & _
              " and a.idtable = b.idtable and [null] = -1")
              
    If tb1.RecordCount > 0 Then
       testoIndicatori = " " & "WK-I-" & SostUnderScore(tbOcc!nomeTab)
    Else
       testoIndicatori = ""
    End If
    testoFetch = testoFetch & "RR-" & SostUnderScore(tbOcc!nomeTab) & testoIndicatori & vbCrLf
    testoFetch = testoFetch & vbCrLf & Space(17 + indentRed) _
             & "EXEC SQL FETCH " & Replace(tbOcc!nomeTab, "_", "") & cursorSuffix & vbCrLf
    testoFetch = testoFetch & Space(17 + indentRed) & "INTO " & vbCrLf _
             & CostruisciInto("T1", tbOcc!idTab, tbOcc!nomeTab, "GU")
'             & CostruisciInto("T" & indEnd, tbOcc!idTab, tbOcc!nomeTab, "GU")
    testoFetch = testoFetch & Space(17 + indentRed) & "END-EXEC" & vbCrLf
     
     testo = testo & Space(14 + indentRed) & "IF SQLCODE EQUAL ZERO" & vbCrLf
     testo = testo & testoFetch
     testo = testo & Space(14 + indentRed) & "END-IF" & vbCrLf
     
     'testo = testo & Space(14 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
     'ciclo campi occurs
     testo = testo & Space(14 + indentRed) & "PERFORM UNTIL SQLCODE NOT = ZERO" & vbCrLf
     
'**** OCCURS DI UN OCCURS!!!!!!!!!!!!!!
' PER ORA GESTITO FISSO, BISOGNERA' FARE UN CICLO
     Dim tbOcc2 As Recordset
     Dim idxOcc2 As Integer
'''MG 240107
'''''     Set tbOcc2 = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab, b.nome as nomeidx, c.id_depending_on as idcmpon" & _
'''''         " from dmdb2_tabelle as a, dmdb2_index as b, mgdata_cmp as c" & _
'''''         " where a.idtable = b.idtable and a.idtablejoin = " & tbOcc!idTab & _
'''''         " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")
     Set tbOcc2 = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab, b.nome as nomeidx, c.id_depending_on as idcmpon" & _
         " from " & DB & "tabelle as a, " & DB & "index as b, mgdata_cmp as c" & _
         " where a.idtable = b.idtable and a.idtablejoin = " & tbOcc!idTab & _
         " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")

     While Not tbOcc2.EOF
        idxOcc2 = idxOcc2 + 1
        testo = testo & vbCrLf
        testo = testo & Space(17 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
        testo = testo & Space(6) & "*** SECOND LEVEL OF OCCURS" & vbCrLf
        testo = testo & Space(20 + indentRed) & "MOVE KRR-" & Replace(pKey, "_", "-") & " TO K01-" & Replace(tbOcc2!Nomeidx, "_", "-") & vbCrLf
        testo = testo & Space(20 + indentRed) _
                   & "EXEC SQL DECLARE " & Replace(tbOcc2!nomeTab, "_", "") & cursorSuffix & " CURSOR " _
                   & IIf(withold, "WITH HOLD", "") & vbCrLf & Space(20 + indentRed) & " FOR SELECT " & vbCrLf _
                   & CostruisciSelect("T1", tbOcc2!idTab)
'                   & CostruisciSelect("T" & indEnd, tbOcc2!idTab)
'        idSave = TabIstr.BlkIstr(indEnd).IdTable
'        tabSave = TabIstr.BlkIstr(indEnd).Tavola
'        idxSave2 = TabIstr.BlkIstr(indEnd).PKey
'        TabIstr.BlkIstr(indEnd).Tavola = tbOcc2!nomeTab
'        TabIstr.BlkIstr(indEnd).IdTable = tbOcc2!idTab
        TabIstr.BlkIstr(1).Tavola = tbOcc2!nomeTab
        TabIstr.BlkIstr(1).idTable = tbOcc2!idTab
        'If UBound(TabIstr.BlkIstr(indEnd).KeyDef) > 0 Then
'          keySave = TabIstr.BlkIstr(indEnd).KeyDef
'          ReDim TabIstr.BlkIstr(indEnd).KeyDef(0)
        'Else
        '  ReDim keySave(0)
        'End If
'        testo = testo & Space(15 + indentRed) & "FROM " & vbCrLf _
'                         & CostruisciFrom(1, indEnd, True)
'        testo = testo & CostruisciWhere(1, indEnd)
        testo = testo & Space(21 + indentRed) & "FROM " & vbCrLf _
                         & CostruisciFrom(1, 1, True)
        testo = testo & CostruisciWhere(1, 1)
        'testo = testo & CostruisciWhere(1, indEnd, False, True)
        
'        TabIstr.BlkIstr(indEnd).Tavola = tabSave
'        TabIstr.BlkIstr(indEnd).IdTable = idSave
'        TabIstr.BlkIstr(indEnd).KeyDef = keySave
     
        testo = testo & Space(20 + indentRed) & "END-EXEC" & vbCrLf
        'testo = testo & Space(20 + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
   
        testo = testo & Space(20 + indentRed) & "EXEC SQL OPEN " & Replace(tbOcc2!nomeTab, "_", "") & cursorSuffix & vbCrLf
        testo = testo & Space(20 + indentRed) & "END-EXEC" & vbCrLf
        testo = testo & Space(20 + indentRed) & "IF SQLCODE = -502" & vbCrLf
        testo = testo & Space(23 + indentRed) & "EXEC SQL CLOSE " & Replace(tbOcc2!nomeTab, "_", "") & cursorSuffix & vbCrLf
        testo = testo & Space(23 + indentRed) & "END-EXEC" & vbCrLf
        testo = testo & Space(23 + indentRed) & "EXEC SQL OPEN " & Replace(tbOcc2!nomeTab, "_", "") & cursorSuffix & vbCrLf
        testo = testo & Space(23 + indentRed) & "END-EXEC" & vbCrLf
        testo = testo & Space(20 + indentRed) & "END-IF" & vbCrLf
        'testo = testo & Space(20 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
'        testo = testo & Space(20 + indentRed) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
'        testo = testo & Space(23 + indentRed) & "PERFORM 9999-FETCH-" & Replace(tbOcc2!nomeTab, "_", "") & cursorSuffix & vbCrLf
'        testo = testo & Space(20 + indentRed) & "END-IF" & vbCrLf
        
'FETCH!!!!!
       Dim testoFetch2 As String
       testoFetch2 = ""
       testoFetch2 = testoFetch2 & Space(23 + indentRed) & "INITIALIZE "
         'SP 4/9 initialize indicatori di null
'''MG 240107
'''''       Set tb1 = m_dllFunctions.Open_Recordset("select * " & _
'''''                 "from dmdb2_tabelle as a, dmdb2_colonne as b " & _
'''''                 "where a.idorigine = " & tbOcc2!idTab & _
'''''                 " and a.idtable = b.idtable and [null] = -1")
       Set tb1 = m_dllFunctions.Open_Recordset("select * " & _
                 "from " & DB & "tabelle as a, " & DB & "colonne as b " & _
                 "where a.idorigine = " & tbOcc2!idTab & _
                 " and a.idtable = b.idtable and [null] = -1")

       If tb1.RecordCount > 0 Then
          testoIndicatori = " " & "WK-I-" & SostUnderScore(tbOcc2!nomeTab)
       Else
          testoIndicatori = ""
       End If
       testoFetch2 = testoFetch2 & "RR-" & SostUnderScore(tbOcc2!nomeTab) & testoIndicatori & vbCrLf
       testoFetch2 = testoFetch2 & vbCrLf & Space(23 + indentRed) _
                & "EXEC SQL FETCH " & Replace(tbOcc2!nomeTab, "_", "") & cursorSuffix & vbCrLf
       testoFetch2 = testoFetch2 & Space(23 + indentRed) & "INTO " & vbCrLf _
                & CostruisciInto("T1", tbOcc2!idTab, tbOcc2!nomeTab, "GU")
'                & CostruisciInto("T" & indEnd, tbOcc2!idTab, tbOcc2!nomeTab, "GU")
       testoFetch2 = testoFetch2 & Space(23 + indentRed) & "END-EXEC" & vbCrLf
        
       testo = testo & Space(20 + indentRed) & "IF SQLCODE EQUAL ZERO" & vbCrLf
       testo = testo & testoFetch2
       testo = testo & Space(20 + indentRed) & "END-IF" & vbCrLf
        
        'testo = testo & Space(20 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
        'ciclo campi occurs
        testo = testo & Space(20 + indentRed) & "PERFORM UNTIL SQLCODE NOT = ZERO" & vbCrLf
        
        testo = testo & Space(23 + indentRed) & "PERFORM 9999-" & SostUnderScore(tbOcc2!nomeTab) & "-TO-RD" & vbCrLf
        
      If tbOcc2!idcmpon > 0 Then
         Set tb1 = m_dllFunctions.Open_Recordset("select * from mgdata_cmp where idcmp = " & tbOcc2!idcmpon)
         If Not tb1.EOF Then
            'stefano: PER ORA FISSO (KEYSEQ PERCHE' GESTISCO LA OCCURS SOLO AL PRIMO LIVELLO
            testo = testo & Space(23 + indentRed) & "MOVE KEYSEQ OF RR-" & SostUnderScore(tbOcc2!nomeTab) & " TO" & vbCrLf
            testo = testo & Space(23 + indentRed) & "    " & tb1!nome & vbCrLf
         End If
      End If
        
        testo = testo & testoFetch2
        'testo = testo & Space(23 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
        testo = testo & Space(20 + indentRed) & "END-PERFORM" & vbCrLf
        testo = testo & Space(20 + indentRed) & "IF SQLCODE NOT = ZERO AND 100" & vbCrLf
        'SQ 24-07 - Modifica Virgilio
        testo = testo & Space(20 + indentRed) & "   MOVE '" & Replace(nomeTab, "_", "-") & "' TO WK-TABLE-SQL-ERR" & vbCrLf
        testo = testo & Space(20 + indentRed) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
        testo = testo & Space(20 + indentRed) & "END-IF" & vbCrLf
        testo = testo & Space(20 + indentRed) & "EXEC SQL CLOSE " & Replace(tbOcc2!nomeTab, "_", "") & cursorSuffix & vbCrLf
        testo = testo & Space(20 + indentRed) & "END-EXEC" & vbCrLf
        testo = testo & Space(17 + indentRed) & "END-IF" & vbCrLf & vbCrLf
       
        tbOcc2.MoveNext
     Wend
        
     testo = testo & Space(17 + indentRed) & "PERFORM 9999-" & SostUnderScore(tbOcc!nomeTab) & "-TO-RD" & vbCrLf
     
      If tbOcc!idcmpon > 0 Then
         Set tb1 = m_dllFunctions.Open_Recordset("select * from mgdata_cmp where idcmp = " & tbOcc!idcmpon)
         If Not tb1.EOF Then
            'stefano: PER ORA FISSO (KEYSEQ PERCHE' GESTISCO LA OCCURS SOLO AL PRIMO LIVELLO
            testo = testo & Space(17 + indentRed) & "MOVE KEYSEQ OF RR-" & SostUnderScore(tbOcc!nomeTab) & " TO" & vbCrLf
            testo = testo & Space(17 + indentRed) & "    " & tb1!nome & vbCrLf
         End If
      End If
     
     testo = testo & testoFetch
     'testo = testo & Space(14 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
  
     testo = testo & Space(14 + indentRed) & "END-PERFORM" & vbCrLf
     
     testo = testo & Space(14 + indentRed) & "IF SQLCODE NOT = ZERO AND 100" & vbCrLf
     'SQ 24-07 - Modifica Virgilio
     testo = testo & Space(14 + indentRed) & "   MOVE '" & Replace(tbOcc!nomeTab, "_", "-") & "' TO WK-TABLE-SQL-ERR" & vbCrLf
     testo = testo & Space(14 + indentRed) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
     testo = testo & Space(14 + indentRed) & "END-IF" & vbCrLf
     
     testo = testo & Space(14 + indentRed) & "EXEC SQL CLOSE " & Replace(tbOcc!nomeTab, "_", "") & cursorSuffix & vbCrLf
     testo = testo & Space(14 + indentRed) & "END-EXEC" & vbCrLf
     testo = testo & Space(11 + indentRed) & "END-IF" & vbCrLf
     testo = testo & vbCrLf
     
     tbOcc.MoveNext

  Wend
  

  If Not Redef Then testo = testo & Space(11) & "." & vbCrLf
  costruisciOccurs = testo
 End Function
 Function costruisciOccursInsOld(idTab As Variant, indentRed As Variant, tipoIstr As String, flagOcc As Boolean, Redef As Boolean) As String
  Dim tbOcc As Recordset
  Dim testo As String, tabSave As String, idxSave2 As String, istrSave As String
  Dim idSave As Variant, idSave2 As Variant
  Dim keySave() As BlkIstrKey
  Dim testoIndicatori
  Dim tb1 As Recordset
  
'''MG 240107
'''''  Set tbOcc = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab,a.creator as creator,b.nome as nomeidx, c.occurs as idxmax, a.condizione as testored, c.id_depending_on as idcmpon" & _
'''''      " from dmdb2_tabelle as a, dmdb2_index as b, mgdata_cmp as c" & _
'''''      " where a.idtable = b.idtable and a.idtablejoin = " & idTab & _
'''''      " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")
  Set tbOcc = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab,a.creator as creator,b.nome as nomeidx, c.occurs as idxmax, a.condizione as testored, c.id_depending_on as idcmpon" & _
      " from " & DB & "tabelle as a, " & DB & "index as b, mgdata_cmp as c" & _
      " where a.idtable = b.idtable and a.idtablejoin = " & idTab & _
      " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")
      
  'SQ - tmp: cambiare tutto, ATTENZIONE alle ISRT!
  If tipoIstr <> "REPL" Then
    If Not tbOcc.EOF And Not Redef Then
       testo = testo & Space(7) & "9999-" & Replace(TabIstr.BlkIstr(UBound(TabIstr.BlkIstr)).Tavola, "_", "-") & "-" & tipoIstr & " SECTION." & vbCrLf
       'gestione EXIT non funzionante
       testo = testo & Space(11 + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
    End If
  End If
  While Not tbOcc.EOF
     flagOcc = True
     testo = testo & vbCrLf
     testo = testo & Space(6) & "*** OCCURS" & vbCrLf
     'gestione EXIT non funzionante
     testo = testo & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
     testo = testo & Space(14 + indentRed) & "MOVE '" & tbOcc!nomeTab & "' TO WK-NAMETABLE " & vbCrLf
     testo = testo & Space(14 + indentRed) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
     'testo = testo & Space(14 + indentRed) & "MOVE KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO KRR-" & Replace(tbOcc!Nomeidx, "_", "-") & vbCrLf
     
     If tipoIstr = "REPL" Then
       testo = testo & Space(14 + indentRed) & "MOVE KRR-" & Replace(tbOcc!Nomeidx, "_", "-") & " TO K01-" & Replace(tbOcc!Nomeidx, "_", "-") & vbCrLf
       testo = testo & Space(14 + indentRed) & "EXEC SQL DELETE " & vbCrLf
       ' REPLACE sempre livello unico
       idSave = TabIstr.BlkIstr(1).idTable
       tabSave = TabIstr.BlkIstr(1).Tavola
       keySave = TabIstr.BlkIstr(1).KeyDef
       istrSave = TabIstr.Istr
       TabIstr.BlkIstr(1).Tavola = tbOcc!nomeTab
       TabIstr.BlkIstr(1).idTable = tbOcc!idTab
       ReDim TabIstr.BlkIstr(1).KeyDef(0)
       TabIstr.Istr = "DUMM"
       
       indentGen = indentRed
       testo = testo & Space(15 + indentRed) & "FROM" & vbCrLf & CostruisciFrom(1, 1, True)
       testo = testo & vbCrLf & CostruisciWhere(1, 1)
       TabIstr.BlkIstr(1).Tavola = tabSave
       TabIstr.BlkIstr(1).idTable = idSave
       TabIstr.BlkIstr(1).KeyDef = keySave
       TabIstr.Istr = istrSave
       testo = testo & Space(14 + indentRed) & "END-EXEC" & vbCrLf
       'testo = testo & Space(14 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
       'testo = testo & Space(14 + indentRed) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
       'testo = testo & Space(17 + indentRed) & "GO TO " & ExRout & vbCrLf
       'testo = testo & Space(17 + indentRed) & "EXIT" & vbCrLf
       'testo = testo & Space(14 + indentRed) & "END-IF" & vbCrLf
     End If
     
     'testo = testo & Space(14 + indentRed) & "PERFORM SETK-WKEYADL THRU SETK-WKEYADL-EX" & vbCrLf
     'testo = testo & Space(14 + indentRed) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
     testo = testo & Space(14 + indentRed) & "PERFORM 9999-SETK-WKEYADL" & vbCrLf
     'gestione EXIT non funzionante
     testo = testo & Space(11 + indentRed) & "END-IF" & vbCrLf
      
     Dim flgOn As Boolean
     Dim nomeOn As String
     flgOn = False
     nomeOn = ""
     If tbOcc!idcmpon > 0 Then
        Set tb1 = m_dllFunctions.Open_Recordset("select * from mgdata_cmp where idcmp = " & tbOcc!idcmpon)
        If Not tb1.EOF Then
           flgOn = True
           nomeOn = tb1!nome
           'stefano: verifichiamo se tutte le copy T sono ok per il depending on
           'testo = testo & Space(11 + indentRed) & "MOVE '" & tb1!Nome & "' TO WK-KEYSEQ1X" & vbCrLf
        End If
     End If
     testo = testo & Space(11 + indentRed) & "PERFORM VARYING WK-KEYSEQ1 FROM 1 BY 1" & vbCrLf
     testo = testo & Space(11 + indentRed) & "  UNTIL WK-KEYSEQ1 > " & IIf(flgOn, nomeOn, tbOcc!idxmax) & vbCrLf
     testo = testo & Space(11 + indentRed) & "  OR    WK-SQLCODE NOT = ZERO" & vbCrLf
     Dim indentOcc As Integer
     indentOcc = 0
     If Len(tbOcc!testoRed) And Not flgOn Then
        indentOcc = 3
        testo = testo & Space(11 + indentRed + indentOcc) & Replace(getCondizione_Redefines(tbOcc!testoRed), vbCrLf, vbCrLf & Space(14 + indentRed)) & vbCrLf
     End If
    'testo = testo & Space(14 + indentRed + indentOcc) & "PERFORM " & SostUnderScore(nomeTRed(k)) & "-TO-RR THRU " & SostUnderScore(nomeTRed(k)) & "-TO-RR-EX" & vbCrLf
    testo = testo & Space(14 + indentRed + indentOcc) & "PERFORM 9999-" & Replace(tbOcc!nomeTab, "_", "-") & "-TO-RR" & vbCrLf & vbCrLf
    indentGen = 15 + indentRed
    testo = testo & Space(14 + indentRed + indentOcc) & "EXEC SQL " & vbCrLf
    'SQ 1-08-06 CREATOR
    'Buttare via la variabile globale m_author...
    ' Mauro 28-03-2007 : Eliminazione CREATOR
    'testo = testo & Space(14 + indentRed + indentOcc) & "    INSERT INTO " & IIf(Len(tbOcc!creator), tbOcc!creator & ".", "") & tbOcc!nomeTab & vbCrLf
    testo = testo & Space(14 + indentRed + indentOcc) & "    INSERT INTO " & tbOcc!nomeTab & vbCrLf
    testo = testo & Space(14 + indentRed + indentOcc) & "    (" & vbCrLf
    testo = testo & CostruisciSelect("", tbOcc!idTab) & Space(12 + indentRed) & ")" & vbCrLf
    testo = testo & Space(14 + indentRed + indentOcc) & "    VALUES (" & vbCrLf
    testo = testo & CostruisciInto("T" & UBound(TabIstr.BlkIstr), tbOcc!idTab, tbOcc!nomeTab, "ISRT")
    testo = testo & Space(14 + indentRed + indentOcc) & "    )" & vbCrLf
    testo = testo & Space(14 + indentRed + indentOcc) & "END-EXEC" & vbCrLf
    testo = testo & Space(14 + indentRed + indentOcc) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
'      testo = testo & Space(14 + indentRed + indentOcc) & "IF WK-SQLCODE NOT = ZERO" & vbCrLf
'      testo = testo & Space(14 + indentRed + indentOcc) & "   EXIT" & vbCrLf
'      testo = testo & Space(14 + indentRed + indentOcc) & "END-IF" & vbCrLf
     If Len(tbOcc!testoRed) And Not flgOn Then
      testo = testo & Space(11 + indentRed + indentOcc) & "END-IF" & vbCrLf
     End If

'**** OCCURS DI UN OCCURS!!!!!!!!!!!!!!
' PER ORA GESTITO FISSO, BISOGNERA' FARE UN CICLO
     
     Dim tbOcc2 As Recordset
     Dim idxOcc2 As Integer
'''MG 240107
'''''     Set tbOcc2 = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab, b.nome as nomeidx, c.occurs as idxmax, a.condizione as testored, c.id_depending_on as idcmpon" & _
'''''      " from dmdb2_tabelle as a, dmdb2_index as b, mgdata_cmp as c" & _
'''''      " where a.idtable = b.idtable and a.idtablejoin = " & tbOcc!idTab & _
'''''      " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")
     Set tbOcc2 = m_dllFunctions.Open_Recordset("select a.idtable as idtab, a.nome as nometab, b.nome as nomeidx, c.occurs as idxmax, a.condizione as testored, c.id_depending_on as idcmpon" & _
      " from " & DB & "tabelle as a, " & DB & "index as b, mgdata_cmp as c" & _
      " where a.idtable = b.idtable and a.idtablejoin = " & tbOcc!idTab & _
      " and a.TAG like '%OCCURS%' and b.tipo = 'P' and a.dimensionamento = c.idcmp order by a.nome")

     While Not tbOcc2.EOF
         testo = testo & vbCrLf
         testo = testo & Space(6) & "*** SECOND LEVEL OF OCCURS" & vbCrLf
     'gestione EXIT non funzionante
         testo = testo & Space(14 + indentRed + indentOcc) & "IF WK-SQLCODE = ZERO" & vbCrLf
         testo = testo & Space(17 + indentRed + indentOcc) & "MOVE '" & tbOcc2!nomeTab & "' TO WK-NAMETABLE " & vbCrLf
         'testo = testo & Space(17 + indentRed + indentOcc) & "PERFORM SETK-WKEYADL THRU SETK-WKEYADL-EX" & vbCrLf
         testo = testo & Space(17 + indentRed + indentOcc) & "PERFORM 9999-SETK-WKEYADL" & vbCrLf
     'gestione EXIT non funzionante
         testo = testo & Space(14 + indentRed + indentOcc) & "END-IF" & vbCrLf
          
     flgOn = False
     nomeOn = ""
     If tbOcc2!idcmpon > 0 Then
        Set tb1 = m_dllFunctions.Open_Recordset("select * from mgdata_cmp where idcmp = " & tbOcc2!idcmpon)
        If Not tb1.EOF Then
           flgOn = True
           nomeOn = tb1!nome
           'stefano: verifichiamo se tutte le copy T sono ok per il depending on
           'testo = testo & Space(14 + indentRed + indentOcc) & "MOVE '" & tb1!Nome & "' TO WK-KEYSEQ1X" & vbCrLf
        End If
     End If
          
         testo = testo & Space(14 + indentRed + indentOcc) & "PERFORM VARYING WK-KEYSEQ2 FROM 1 BY 1" & vbCrLf
         testo = testo & Space(14 + indentRed + indentOcc) & "  UNTIL WK-KEYSEQ2 > " & IIf(flgOn, nomeOn, tbOcc2!idxmax) & vbCrLf
         testo = testo & Space(14 + indentRed + indentOcc) & "  OR    WK-SQLCODE NOT = ZERO" & vbCrLf
         Dim indentOcc2 As Integer
         indentOcc2 = 0
         If Len(tbOcc2!testoRed) And Not flgOn Then
            indentOcc2 = 3
            testo = testo & Space(14 + indentRed + indentOcc + indentOcc2) & Replace(getCondizione_Redefines(tbOcc2!testoRed), vbCrLf, vbCrLf & Space(17 + indentRed)) & vbCrLf
         End If
          'testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "PERFORM " & SostUnderScore(nomeTRed(k)) & "-TO-RR THRU " & SostUnderScore(nomeTRed(k)) & "-TO-RR-EX" & vbCrLf
          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "PERFORM 9999-" & SostUnderScore(tbOcc2!nomeTab) & "-TO-RR" & vbCrLf
          testo = testo & vbCrLf
          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "EXEC SQL " & vbCrLf
          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "    INSERT INTO " & Trim(m_author) & tbOcc2!nomeTab & vbCrLf
          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "    (" & vbCrLf & CostruisciSelect("", tbOcc2!idTab) & Space(15 + indentRed) & ")" & vbCrLf
          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "    VALUES (" & vbCrLf
          testo = testo & CostruisciInto("T" & UBound(TabIstr.BlkIstr), tbOcc2!idTab, tbOcc2!nomeTab, "ISRT")
          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "    )" & vbCrLf
          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "END-EXEC" & vbCrLf
          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
'          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "IF WK-SQLCODE NOT = ZERO" & vbCrLf
'          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "   EXIT" & vbCrLf
'          testo = testo & Space(17 + indentRed + indentOcc + indentOcc2) & "END-IF" & vbCrLf
          If Len(tbOcc2!testoRed) And Not flgOn Then
             testo = testo & Space(14 + indentRed + indentOcc + indentOcc2) & "END-IF" & vbCrLf
          End If
        testo = testo & Space(14 + indentRed + indentOcc) & "END-PERFORM" & vbCrLf
        tbOcc2.MoveNext
     Wend
   
     testo = testo & Space(11 + indentRed) & "END-PERFORM" & vbCrLf
     tbOcc.MoveNext
  Wend
  If Not Redef Then testo = testo & Space(11) & "." & vbCrLf
  costruisciOccursInsOld = testo
 End Function

'SQ: 29-08
'Restituisce l'array di tutti quelli del DBD principale
'Deve corrispondere a quello in: DM...Prj
Function getSuffissiStorici_BPER(dbdName As String) As String()
  Dim r As ADODB.Recordset
  'Dim suffissiStorici() As String
  Dim i As Integer
  
  On Error GoTo errordB
  
  Set r = m_dllFunctions.Open_Recordset("SELECT out.DBD_Name,out.Table_Name from PsDLi_DBD as b,PsDli_DBD as out where b.DBD_Name='" & dbdName & "' AND b.Route_Name=out.Route_Name order by out.Table_Name")
  ReDim suffissiStorici(r.RecordCount)
  For i = 1 To r.RecordCount
    suffissiStorici(i) = r(0)
    r.MoveNext
  Next
  r.Close
  
  getSuffissiStorici_BPER = suffissiStorici
  
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
End Function
'SQ: 29-08
'Accede all'array relativo al DBD costruito da getSuffissiStorici_BPER
'per non fare 40 select...
Function getSuffissoStorico_BPER(dbdName As String) As String
  Dim i As Integer
  
  On Error GoTo errordB
  
'  If UBound(getSuffissiStorici_BPER) Then
'    For i = 1 To UBound(getSuffissiStorici_BPER)
'      If dbdName = suffissiStorici(i) Then
'      getSuffissoStorico_BPER = i 'fa il cast da solo?
'    Next
'  Else
'    getSuffissoStorico_BPER = ""
'  End If
'
'    Do While Not r.EOF
'      'SQ 29-08 (il principale non ha suffisso)
'      If r(0) = dbdName And r(1) > 1 Then
'        getSuffissoStorico_BPER = r(1)
'      End If
'      r.MoveNext
'    Loop
'  End If
'  r.Close
  
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getSuffissoStorico_BPER = ""
End Function

'SP: 9/9 gestione secondaria su chiavi multiple
Function getSecondary(i As Integer) As Boolean
Dim j As Integer
  On Error GoTo errordB
  
  getSecondary = False
  For j = 1 To UBound(TabIstr.BlkIstr(i).KeyDef)
    If TabIstr.BlkIstr(i).KeyDef(j).key <> TabIstr.BlkIstr(i).KeyDef(j).NameAlt Then
       getSecondary = True
       Exit For
    End If
  Next
  
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
End Function

'SP: 9/9 gestione secondaria su chiavi multiple
Function getKeyName(i As Integer) As String
Dim j As Integer
  On Error GoTo errordB
  
  getKeyName = TabIstr.BlkIstr(i).KeyDef(1).NameAlt
  For j = 1 To UBound(TabIstr.BlkIstr(i).KeyDef)
    If TabIstr.BlkIstr(i).KeyDef(j).key <> TabIstr.BlkIstr(i).KeyDef(j).NameAlt Then
       getKeyName = TabIstr.BlkIstr(i).KeyDef(j).NameAlt
       Exit For
    End If
  Next
  
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''
' Arriva l'operatore da DB nella forma "letterale"
' - Ritorna l'operatore "SQL"
''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getOperator(operator As String) As String
  Select Case operator
    Case "GT"
      getOperator = ">"
    Case "GE"
      getOperator = ">="
    Case "LT"
      getOperator = "<"
    Case "LE"
      getOperator = "<="
    Case "EQ"
      getOperator = "="
    Case "NE"
      getOperator = "<>"
    Case Else
      getOperator = operator  'errore!
  End Select
End Function

Public Sub AggLog(Stringa, log As RichTextBox)
  Dim vPos As Variant
  '''vPos = Len(log.Text)
  log.SelLength = 0
  log.SelText = Format(Now, "HH:MM:SS") & " - " & Stringa & vbCrLf
  log.Refresh
End Sub

Public Function getOperazioneRoutine() As String
  'stefano: non funziona, bisogna rileggere un'altra volta il db, tutta colpa di questa gestione del
  ' cavolo, basata su dei valori fittizi
  If isGsam Then
     getOperazioneRoutine = "GS"
  Else
    Select Case gbCurInstr
      Case "GU", "GHU"
        getOperazioneRoutine = "GU"
      Case "GN", "GHN"
        getOperazioneRoutine = "GN"
      Case "GNP", "GHNP"
        getOperazioneRoutine = "GP"
      Case "ISRT", "REPL", "DLET"
        getOperazioneRoutine = "UP"
      Case "CHKP", "QRY", "PCB", "TERM", "SYNC"
        getOperazioneRoutine = "UTIL"
    End Select
  End If
End Function
'SQ 30-08-06
Public Sub getEnvironmentParameters()
  xCopyParam = m_dllFunctions.LeggiParam("X-COPY")
  tCopyParam = m_dllFunctions.LeggiParam("T-COPY")
  cCopyParam = m_dllFunctions.LeggiParam("C-COPY")
  kCopyParam = m_dllFunctions.LeggiParam("K-COPY")
  mCopyParam = m_dllFunctions.LeggiParam("MOVE-COPY")
End Sub

'SQ
'Public Function CostruisciWhere_SQ(IndStart As Integer, IndEnd As Integer) As String
'  Dim k As Integer, k1 As Integer, NumLiv As Integer, wNumCmpKey As Integer
'  Dim testo As String, andOr As String
'  Dim Tb1 As Recordset, tb2 As Recordset
'  Dim AliasT As String, nomeKey As String, ope As String
'  Dim SwConcAll As Boolean, SwCreaConcat As Boolean
'
'  andOr = "(   "
'
'  GbKeySec = False
'  SwConcAll = True
'
'  For k = IndStart To IndEnd
'     '''''''''''''''''''''''''''''''''''''''''''''''''''''
'    ' ULTIMA TABELLA: SERVE SEMPRE
'    ' ALTRE: JOIN SE ABBIAMO CHIAVI SECONDARIE
'    '''''''''''''''''''''''''''''''''''''''''''''''''''''
'    'ATTENZIONE: STO IGNORANDO LE BOOLEANE...
'    'errore: questi livelli NON sono quelli dei segmenti... (solo quelli delle chiavi esplicite...)
'    Dim levelOk As Boolean
'    If k = IndEnd Then
'      levelOk = True
'    Else
'      If UBound(TabIstr.BlkIstr(k).keyDef) Then
'        levelOk = TabIstr.BlkIstr(k).keyDef(1).key <> TabIstr.BlkIstr(k).keyDef(1).NameAlt
'      Else
'        levelOk = False
'        'SQ: Mi serve l'operatore...
'        '...
'      End If
'    End If
'
'    If levelOk Then
'    AliasT = "T" & Format(k, "#0")
'    'PER OGNI CHIAVE DEL LIVELLO: (se OPERATORI RELAZIONALI)
'    For k1 = 1 To UBound(TabIstr.BlkIstr(k).keyDef)
'      SwCreaConcat = False
'      Flgsqual = False
'      If Trim(TabIstr.BlkIstr(k).keyDef(k1).key) <> "" Then
'        'SQ:
'        'NomeKey = ControllaNomeKey(TabIstr.BlkIstr(k).KeyDef(k1).key) 'routine per chissa' quale cliente
'        Dim unusedPrimaryKeys As String
'        Dim secondaryKey As String
'        Dim pippo As Boolean
'        If TabIstr.BlkIstr(k).keyDef(k1).NameAlt <> TabIstr.BlkIstr(k).keyDef(k1).key Then
'          ''''''''''''''''''''''''''''''
'          ' CHIAVE SECONDARIA:
'          ' Cambiano le where:
'          ' - join con le tabelle figlie;
'          ' - eliminazione condizione di where sulla chiave primaria "mancante"
'          ''''''''''''''''''''''''''''''
'          secondaryKey = TabIstr.BlkIstr(k).Segment
'          If m_dllFunctions.FnNomeDB = "banca.mty" Then
'            nomeKey = "KY_" & Left(TabIstr.BlkIstr(k).keyDef(k1).NameAlt, 3) & "_" & Right(TabIstr.BlkIstr(k).keyDef(k1).NameAlt, 2)
'          Else
'            nomeKey = TabIstr.BlkIstr(k).keyDef(k1).NameAlt
'          End If
'          'MI SERVE LA CHIAVE PRIMARIA: DOVRO' IGNORARLA, SUI FIGLI! (tmp: tengo solo l'ultima... verificare...)
'          Set Tb1 = m_dllFunctions.Open_Recordset("select a.nome,a.idCmp from dmdb2_colonne as a,dmdb2_idxcol as b where a.idTable = a.idTableJoin AND b.idindex in (select idindex from dmdb2_index where tipo = 'P' and idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(k).idSegm & ")) and a.idColonna=b.IdColonna")
'          While Not Tb1.EOF
'            unusedPrimaryKeys = Tb1(1) & "," & unusedPrimaryKeys
'            Tb1.MoveNext
'          Wend
'          unusedPrimaryKeys = Left(unusedPrimaryKeys, Len(unusedPrimaryKeys) - 1) 'virgola in piu'...
'        Else
'          'secondaryKey = ""
'          nomeKey = TabIstr.BlkIstr(k).keyDef(k1).key
'        End If
'
'        'SQ PROVA: USO COMUNQUE QUELLE EREDITATE DELLA PRIMARIA...
'        'If Len(secondaryKey) And (IstrDb2 = "GU" Or IstrDb2 = "GNP") Then
'        If Len(secondaryKey) And (IstrDb2 = "GU") Then
'          Set Tb1 = m_dllFunctions.Open_Recordset("select a.*,'CHLD' from dmdb2_idxcol as a where idindex in (select idindex from dmdb2_index where nome = '" & nomeKey & "' and idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(k).idSegm & "))  order by ordinale union select a.*,'PRNT' from dmdb2_idxcol as a where idindex in (select idindex from dmdb2_index where nome = '" & TabIstr.BlkIstr(k).keyDef(k1).key & "' and idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(k).idSegm & "))  order by ordinale")
'        Else
'          Set Tb1 = m_dllFunctions.Open_Recordset("select a.*,'CNCT' from dmdb2_idxcol as a where idindex in (select idindex from dmdb2_index where nome = '" & nomeKey & "' and idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(k).idSegm & "))  order by ordinale")
'        End If
'        'SQ 'sto pezzo non ha piu' senso... eliminare...
''        If Tb1.RecordCount = 0 Then
''...
''        End If
'        If Tb1.RecordCount > 0 Then
'          GbKeySec = True
'          GbKeySecName = nomeKey
'          ''''''''''''''''''''
'          ' Per ogni COLONNA:
'          ''''''''''''''''''''
'          While Not Tb1.EOF
'            '''''''''''''''''''''
'            ' OPERATORE:
'            ' rifare... dentro al ciclo ha senso solo la parte commentata!...
'            '''''''''''''''''''''
'            ope = TabIstr.BlkIstr(k).keyDef(k1).Op
'            If PbLike Then
'               ope = "LIKE"
'            End If
'            Select Case Trim(ope)
'               Case "=>"
'                  ope = ">="
'               Case "=<"
'                  ope = "<="
''               Case ">"
''                  If kNumRec < Tb1.RecordCount Then
''                     Ope = ">="
''                  End If
'            End Select
'            If Flgwhere Then 'pippo... chiarire in gnUnica...
'               If ope = ">= " Then
'                  ope = "> "
'               End If
'            End If
'            'SQ
'            'Attenzione alle catene di chiavi secondarie... (non funzionerebbe!?)
'            'N.B.: gestione singola colonna...
'            If Len(secondaryKey) And (secondaryKey <> TabIstr.BlkIstr(k).Segment Or IstrDb2 = "GU") Then
'              Set tb2 = m_dllFunctions.Open_Recordset("select nome,idTable,idTableJoin from dmdb2_colonne where idColonna = " & Tb1!idColonna & " AND idCmp not in (" & unusedPrimaryKeys & ")")
'            Else
'              Set tb2 = m_dllFunctions.Open_Recordset("select nome,idTable,idTableJoin from dmdb2_colonne where idcolonna = " & Tb1!idColonna)
'            End If
'            If Trim(ope) = "=" Or Tb1.RecordCount = 1 Or m_dllFunctions.FnNomeDB = "banca.mty" Then
'               'SQ (controllare se serve anche per l'else)
'              If tb2.RecordCount Then
'                'SQ: ho la union fra ereditate ('PRNT') e proprie ('CHLD') (se chiave secondaria...):
'                '    tutto il "path" se primaria...
'                Select Case Tb1(8)
'                  Case "CHLD"
'                    'OPERATORE: quello dichiarato...
'                    'AREA:      CHIAVE PRIMARIA
'                    testo = testo & Space(16) & andOr & AliasT & "." & Replace(tb2(0), "-", "_") & " " & ope & vbCrLf
'                    testo = testo & Space(22) & ":K" & Format(TabIstr.BlkIstr(k).keyDef(k1).keyNum, "00") & "-" & Replace(nomeKey, "_", "-") & "." & Mid$(Replace(tb2!nome, "_", "-") & Space(20), 1, 19) & vbCrLf
'                  Case "PRNT"
'                    'OPERATORE: "="
'                    'AREA:      quello della chiave utilizzata
'                    testo = testo & Space(16) & andOr & AliasT & "." & Replace(tb2(0), "-", "_") & " =" & vbCrLf
'                    testo = testo & Space(22) & ":K" & Format(TabIstr.BlkIstr(k).keyDef(k1).keyNum, "00") & "-" & Replace(TabIstr.BlkIstr(k).keyDef(k1).key, "_", "-") & "." & Mid$(Replace(tb2!nome, "_", "-") & Space(20), 1, 19) & vbCrLf
'                  Case "CNCT"
'                    'probabilmente non serve discriminarlo dal CHLD...
'                    'OPERATORE: ognuno ha il suo... ocio
'                    'AREA:      CHIAVE PRIMARIA
'                    If tb2!idtable <> tb2!idtablejoin Then ope = "="
'                    testo = testo & Space(16) & andOr & AliasT & "." & Replace(tb2(0), "-", "_") & " " & ope & vbCrLf
'                    testo = testo & Space(22) & ":K" & Format(TabIstr.BlkIstr(k).keyDef(k1).keyNum, "00") & "-" & Replace(TabIstr.BlkIstr(k).keyDef(k1).key, "_", "-") & "." & Mid$(Replace(tb2!nome, "_", "-") & Space(20), 1, 19) & vbCrLf
'                End Select
'                andOr = "AND "
'              End If
'            Else
'               If Not SwCreaConcat Then
'                  If m_dllFunctions.FnNomeDB = "banca.mty" Then
'                    'tmp:
'                    testo = testo & Space(20) & andOr & CreaConcat(TabIstr.BlkIstr(k).idSegm, True, Trim(ope), "P", nomeKey, k, TabIstr.BlkIstr(k).keyDef(k1).keyNum)
'                  Else
'                    testo = testo & Space(20) & andOr & CreaConcat(TabIstr.BlkIstr(k).idSegm, True, ope, "I", nomeKey, k, TabIstr.BlkIstr(k).keyDef(k1).keyNum)
'                  End If
'                  SwCreaConcat = True
'               End If
'               andOr = Space(11)
'            End If
'            Tb1.MoveNext
'          Wend
'
'          testo = Mid$(testo, 1, Len(testo) - 2) & " ) " & vbCrLf
'          If Trim(TabIstr.BlkIstr(k).keyDef(k1).KeyBool) = "OR" Then
'            andOr = " OR  (     "
'          Else
'            andOr = " AND (     "
'          End If
'        Else
'          '   chiave primaria
'          If k = 1 And IstrDb2 = "GU" Then
'            Set Tb1 = m_dllFunctions.Open_Recordset("select * from psdli_segmenti where idsegmento = " & TabIstr.BlkIstr(k).idSegm)
'            If Tb1!Parent <> "0" Then
'               SwConcAll = False
'            End If
'          Else
'            SwConcAll = True
'          End If
'          Set Tb1 = m_dllFunctions.Open_Recordset("select * from dmdb2_colonne where idcolonna in (select idcolonna from dmdb2_idxcol where idindex in (select idindex from dmdb2_index where tipo = 'P' and idtable in (select idtable from dmdb2_tabelle where idorigine = " & TabIstr.BlkIstr(k).idSegm & "))) order by idcolonna")
'          If Tb1.RecordCount > 0 Then
'            wNumCmpKey = Tb1.RecordCount
'
'              While Not Tb1.EOF
'                ope = TabIstr.BlkIstr(k).keyDef(k1).Op
'                If PbLike Then
'                   ope = "LIKE"
'                End If
'
'                Select Case Trim(ope)
'    '               Case ">"
'    '                  If kNumRec < Tb1.RecordCount Then
'    '                     Ope = ">="
'    '                  End If
'                    Case "=>"
'                      ope = ">="
'
'                    Case "=<"
'                      ope = "<="
'                End Select
'    'per_matteo
'                If Flgwhere Then
'                   If ope = ">= " Then
'                      ope = "> "
'                   End If
'                End If
'    '            If SwConcAll = False And Tb1!IdSegm <> Tb1!IdSegmProv Then
'    '               Ope = "="
'    '               wNumCmpKey = wNumCmpKey - 1
'    '            End If
'                If Trim(ope) = "=" Or wNumCmpKey = 1 Then
'                   testo = testo & Space(20) & andOr & AliasT & "." & MettiUnderScore(Tb1!nome) & " " & ope & vbCrLf
'                   testo = testo & Space(31) & ":K" & Format(TabIstr.BlkIstr(k).keyDef(k1).keyNum, "00") & "-" & TabIstr.BlkIstr(k).PKey & "." & Mid$(Replace(Tb1!nome, "_", "-") & Space(20), 1, 19) & vbCrLf
'                   andOr = "       AND "
'                Else
'                   If Not SwCreaConcat Then
'                       testo = testo & Space(20) & andOr & CreaConcat(TabIstr.BlkIstr(k).idSegm, True, ope, "P", "Primary", k, TabIstr.BlkIstr(k).keyDef(k1).keyNum)
'                       SwCreaConcat = True
'                   End If
'                   andOr = Space(11)
'                End If
'                Tb1.MoveNext
'              Wend
'              testo = Mid$(testo, 1, Len(testo) - 2) & " ) " & vbCrLf
'              If Trim(TabIstr.BlkIstr(k).keyDef(k1).KeyBool) = "OR" Then
'                andOr = " OR  (     "
'              Else
'                andOr = " AND (     "
'              End If
'            End If
'          End If
'        Else
'          Flgsqual = True
'        End If
'      Next k1
'    End If
'  Next k
'
'  If Trim(testo) = "" Then
'     CostruisciWhere_SQ = CostruisciWhereP(IndStart, IndEnd, True)
'  Else
'     'SG : Sembrerebbe non servire nelle GN to  talmente qualificate
'     'stefanopippo: provo ad aggiungere anche GNP
'     If IstrDb2 = "GN" Then
'     'If IstrDb2 = "GN" Then 'Or IstrDb2 = "GNP" Then
'        If Not SwCreaConcat Then
'           If UBound(TabIstr.BlkIstr(k - 1).keyDef) Then
'              testo = testo & Space(20) & andOr & CreaConcatP(TabIstr.BlkIstr(k - 1).idSegm, True, ope, "P", "Primary", k - 1, TabIstr.BlkIstr(k - 1).keyDef(1).keyNum) & Space(20) & " ) " & vbCrLf
'           End If
'        End If
'     End If
'     'SQ
'     'stefanopippo: 15/08/2005: preferisco rischiare....
'     'If Len(secondaryKey) Then
'      'JOIN:
'      CostruisciWhere_SQ = testo & CostruisciWConc(IndStart, IndEnd, True)
'     'Else
'     ' CostruisciWhere_SQ = testo & CostruisciWConc(IndEnd, IndEnd, True)
'     'End If
'  End If
'End Function

''''''''''''''''''''''''''''''
' Solo GU
''''''''''''''''''''''''''''''
Public Function getGU_DB2(rtbFile As RichTextBox, tipoIstr As String, Optional ByVal withold As Boolean = False) As String
  Dim K As Long
  Dim i As Integer, k1 As Integer, indEnd As Integer, indSav As Integer, indentGNsqKeyOp As Integer
  Dim NomeT As String, NomePk As String, nomeKey As String, testo As String
  Dim TestoFrom As String, TestoWhere As String
  Dim keySecFlagPrimoLiv As Boolean, keySecFlagUltimoLiv As Boolean
  Dim flagSel As Boolean, flagIns As Boolean, flagRed As Boolean
  Dim flagOcc As Boolean, isOccurs As Boolean
  Dim tb1 As Recordset, tb2 As Recordset, tbred As Recordset, rsIndPart As Recordset
  Dim indentGN As Integer, indentGNsq As Integer, indentGNqu As Integer
  Dim keyPrim As String, keySec As String
  Dim indRed As Integer
  Dim keySave() As BlkIstrKey
  Dim istrSave As IstrDli
  Dim instrLevel As BlkIstrDli
  Dim nomeTab As String
  'SQ
  Dim testoTmp As String
  
  indentGen = 0
  indentRed = 0
  
  level = 0
  If UBound(TabIstr.BlkIstr) > 0 Then
    If InStr(TabIstr.BlkIstr(1).CodCom, "D") And tipoIstr <> "ISRT" Then
      level = 1
    Else
      level = UBound(TabIstr.BlkIstr)
    End If
  End If
  
  If Not EmbeddedRoutine Then
    testo = Space(11) & "MOVE '" & tipoIstr & "' TO WK-ISTR" & vbCrLf
  End If
  
  For level = level To UBound(TabIstr.BlkIstr)
    '''''''''''''
    'init:
    '''''''''''''
    'tmp: (ora � ciclico!... togliere quelli che non servono!)
    'keySecFlagPrimoLiv = False
    keySecFlagUltimoLiv = False
    flagOcc = False
    ''''flagIns = False
    '''''''''tabFound = False
    isOccurs = False
    'tabFound3 = False
    includedMember = ""
    indentGN = 0
    indentGNsq = 0
    indentGNqu = 0
    indentGNsqKeyOp = 0
    indentRed = 0
      
    If tipoIstr <> "ISRT" Then
      'SQ D
      indEnd = level
      instrLevel = TabIstr.BlkIstr(indEnd)
      'SQ D
      'testo = InitIstr(tipoIstr, "N")
      
      'SQ D
      If Not EmbeddedRoutine Then
        testo = testo & Space(11) & "INITIALIZE RD-" & instrLevel.Segment & vbCrLf
      End If
      If level > 1 Then
        If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
          'Lettura solo se il precedente livello andato a buon fine
          testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
        End If
      End If
      
      If Not EmbeddedRoutine Then
        If UBound(TabIstr.BlkIstr) Then
          testo = testo & Space(11) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
          testo = testo & Space(11) & "MOVE '" & instrLevel.Segment & "' TO WK-SEGMENT" & vbCrLf
        End If
        testo = testo & Space(11) & "PERFORM 9000-MANAGE-PCB" & vbCrLf
      End If
        
      flagIns = False
    Else
      'SQ D
      indEnd = UBound(TabIstr.BlkIstr) - 1
      instrLevel = TabIstr.BlkIstr(indEnd)
      testo = testo & Space(11) & "INITIALIZE RD-" & instrLevel.Segment & vbCrLf  'SQ - Ci vuole?
      flagIns = True
    End If
    
    If Not indEnd > 0 Then  'SQ - portare fuori
      m_dllFunctions.WriteLog "Instruction: " & GbNumIstr & " - not converted", "Routines Generation"
      Exit Function
    End If
        
    testo = testo & Space(11) & "MOVE LNKDB-SEGLEVEL(" & indEnd & ") TO LNKDB-STKLEV(LNKDB-NUMSTACK)" & vbCrLf
    
    
    'SQ - sto pezzo probabilmente � da tenere fuori ciclo... giusto?
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Segmento "parent":
    'stefanopippo: 15/08/2005: se su chiave secondaria (sempre sul primo livello!) non fa restore (� inutile)
    keySecFlagPrimoLiv = False
    If UBound(TabIstr.BlkIstr(1).KeyDef) Then
      keySecFlagPrimoLiv = TabIstr.BlkIstr(1).KeyDef(1).key <> TabIstr.BlkIstr(1).pKey
    End If
    If Not keySecFlagPrimoLiv Then
       NomeT = getParentTable(CLng(TabIstr.BlkIstr(1).IdSegm))  'togliere cast e cambiare function
       If Len(NomeT) Then
          'SEGMENTO NON RADICE: RESTORE
          testo = testo & Space(11 + indentGNsq) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
          testo = testo & Space(11 + indentGNsq) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
       End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If UBound(instrLevel.KeyDef) Then
      'nuovo sistema: si intende squalificata quando lo � sull'ultimo livello
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        keySecFlagUltimoLiv = True
'''MG 240107
'''''        Set rsIndPart = m_dllFunctions.Open_Recordset( _
'''''          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, DMDB2_Tabelle as b " & _
'''''          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
'''''          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")
        Set rsIndPart = m_dllFunctions.Open_Recordset( _
          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & DB & "Tabelle as b " & _
          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")

        If Not rsIndPart.EOF Then
          If rsIndPart!nomeTab <> instrLevel.Tavola Then
             nomeTabIndPart = rsIndPart!nomeTab
             idTabIndPart = rsIndPart!idTab
          End If
        End If
      Else
        keySecFlagUltimoLiv = False
      End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
        
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' RESTORE CHIAVE:
    ' Attenzione: per ottimizzare posso appoggiare il nome della tabella in TabIstr.BlkIstr(0)! (valutare)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    For K = 1 To indEnd
      ''''''''''''''''''''''''''''Kt = k  '??????????????????????????????????????????????????
      NomeT = TabIstr.BlkIstr(K).Tavola
      If Trim(NomeT) = "" Then
        NomeT = "[NO-TABLE-FOR:" & TabIstr.BlkIstr(K).Segment & "]"
      End If
      NomePk = TabIstr.BlkIstr(K).pKey
      If Not EmbeddedRoutine Then
        testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
      End If
      'squalificata solo sull'ultimo livello, imposta la chiave primaria ereditata
      If UBound(TabIstr.BlkIstr(K).KeyDef) = 0 And K = BlkFin Then
        testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
        testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO K01-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
      End If
      For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
        If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
          nomeKey = Replace(TabIstr.BlkIstr(K).KeyDef(k1).key, "-", "_")
          'SQ
          'NomePk = TabIstr.BlkIstr(k).KeyDef(k1).NameAlt
          NomePk = nomeKey
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(K)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
          testo = testo & Space(16 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf
          'SP 12/9: indici particolari
          If Len(nomeTabIndPart) And K = indEnd Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
          testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
          If nomeTabIndPart <> "" And K = indEnd Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
          End If
          If TabIstr.BlkIstr(K).pKey = TabIstr.BlkIstr(K).KeyDef(k1).key Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
          End If
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K" & Format(TabIstr.BlkIstr(K).KeyDef(k1).KeyNum, "00") & "-" & NomePk & vbCrLf
        End If
      Next k1
    Next K
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    'UTILIZZO DI ARRAY ANCHE PER CASI "NORMALI"
    indRed = 1
    ReDim Preserve testoRed(1)
    ReDim Preserve nomeTRed(1)
    ReDim Preserve nomeSRed(1)
    ReDim Preserve idRed(1)
    ReDim Preserve nomeKeyRed(1)
    ReDim Preserve nomeFieldRed(1)
    idRed(1) = instrLevel.idTable
    If UBound(instrLevel.KeyDef) Then
      nomeKeyRed(1) = instrLevel.KeyDef(1).key
    Else
      nomeKeyRed(1) = ""
    End If
    Dim tbred2 As Recordset
    Set tbred2 = m_dllFunctions.Open_Recordset( _
      "select nome as nomeidx from psdli_field where unique and idsegmento=" & instrLevel.IdSegm)
    If Not tbred2.EOF Then
      nomeFieldRed(1) = tbred2!Nomeidx
    Else
      m_dllFunctions.WriteLog "No Field found for segment: " & instrLevel.IdSegm, "Routines Generation"
      nomeFieldRed(1) = ""
    End If
    nomeTRed(1) = instrLevel.Tavola
    nomeSRed(1) = instrLevel.Segment
    
    'stefanopippo: redefines; per la SELECT IL PROBLEMA E' SOLO SULL'ULTIMO LIVELLO DELLA SELECT,
    'IN QUANTO NON ESISTE JOIN A MENO DI CHIAVE SECONDARIA, ED IN OGNI CASO LA SECONDARIA SAREBBE SULL'ULTIMA!
    ''''''''''''''''''''''''''''''''''''
    ' REDEFINES: PREPARAZIONE STRUTTURE
    ''''''''''''''''''''''''''''''''''''
    Set tbred = m_dllFunctions.Open_Recordset( _
      "select condizione, RDBMS_tbname from PsDli_Segmenti where idSegmento = " & instrLevel.IdSegm)
    If Len(tbred!condizione) Then
      testoRed(1) = getCondizione_Redefines(tbred!condizione & "_lett")
    Else
      testoRed(1) = ""
    End If
    tbred.Close
    
    Dim tbsel As Recordset
'''MG 240107
'''''    Set tbsel = m_dllFunctions.Open_Recordset( _
'''''      "select idtable, nome from dmdb2_tabelle where idorigine = " & instrLevel.IdSegm & " and TAG like '%DISPATCHER%'")
    Set tbsel = m_dllFunctions.Open_Recordset( _
      "select idtable, nome from " & DB & "tabelle where idorigine = " & instrLevel.IdSegm & " and TAG like '%DISPATCHER%'")

    If Not tbsel.EOF Then
      flagRed = True
'''MG 2401007
''''''      Set tbred = m_dllFunctions.Open_Recordset( _
''''''        "select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
''''''        "from mgdli_segmenti as a, dmdb2_tabelle as b, dmdb2_index as c where a.idsegmentoorigine = " & instrLevel.IdSegm & _
''''''        " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
      Set tbred = m_dllFunctions.Open_Recordset( _
        "select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
        "from mgdli_segmenti as a, " & DB & "tabelle as b, " & DB & "index as c where a.idsegmentoorigine = " & instrLevel.IdSegm & _
        " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")

      If tbred.RecordCount Then
        ReDim Preserve testoRed(tbred.RecordCount + 1)
        ReDim Preserve nomeTRed(tbred.RecordCount + 1)
        ReDim Preserve nomeSRed(tbred.RecordCount + 1)
        ReDim Preserve idRed(tbred.RecordCount + 1)
        ReDim Preserve nomeKeyRed(tbred.RecordCount + 1)
        ReDim Preserve nomeFieldRed(tbred.RecordCount + 1)
        While Not tbred.EOF
          indRed = indRed + 1
          If Len(tbred!condizione) Then
            If flagRed Then 'SEMPRE VERO (qui)?!
            
              testoRed(1) = ""  '????????????? (non posso farlo fuori ciclo?)
              idRed(1) = tbsel!idTable
              nomeTRed(1) = tbsel!nome
              nomeSRed(1) = "DISPATCHER"
              
              If UBound(instrLevel.KeyDef) > 0 Then   '?? non l'ho gi� fatto sopra? (fuori ciclo?)
                nomeKeyRed(1) = instrLevel.KeyDef(1).key
                nomeFieldRed(1) = instrLevel.KeyDef(1).NameAlt
              Else
                'SQ - 26-07-06 - Pezza per non cambiare cose che non so!
                nomeKeyRed(1) = Replace(instrLevel.pKey, "_", "-")
                ' => vado a prendere il nome colonna corretto!
'''MG 240107
'''''                Set tbred2 = m_dllFunctions.Open_Recordset( _
'''''                   "Select nome from Dmdb2_colonne where idTable=" & instrLevel.idTable & _
'''''                   " and idTable=idTableJoin and idCmp <> -1")
                Set tbred2 = m_dllFunctions.Open_Recordset( _
                   "Select nome from " & DB & "colonne where idTable=" & instrLevel.idTable & _
                   " and idTable=idTableJoin and idCmp <> -1")

                If Not tbred2.EOF Then
                  nomeFieldRed(1) = Replace(tbred2!nome, "_", "-")
                Else
                  nomeFieldRed(1) = ""
                  m_dllFunctions.WriteLog "No 'field' column found for table: " & instrLevel.idTable, "Routines Generation"
                End If
                tbred2.Close
              End If
              testoRed(indRed) = "IF WK-NAMETABLE = '" & tbred!nomeTab & "'"
            Else
              testoRed(indRed) = getCondizione_Redefines(tbred!condizione & "_lett")
            End If
            idRed(indRed) = tbred!idTab
            nomeTRed(indRed) = tbred!nomeTab
            nomeSRed(indRed) = tbred!nomeseg
            nomeKeyRed(indRed) = tbred!nomeindex
            'stefano: campo per il nome del field
            Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
              "from mgdli_field where idfield = " & tbred!IdField & " and idsegmento = " & tbred!idSegmento)
            If Not tbred2.EOF Then
               nomeFieldRed(indRed) = tbred2!Nomeidx
            Else
               nomeFieldRed(indRed) = ""
               m_dllFunctions.WriteLog "Virtual Field not found: " & tbred!IdField, "Routines Generation"
            End If
           'SP redefines
          Else
            m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
          End If
          tbred.MoveNext
        Wend
        tbred.Close
      End If
      indSav = indRed
      indRed = 1
    Else
      flagRed = False
      testoRed(1) = ""
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    '''''''''''''''''''''''''''''''''''
    ' DECLARE CURSORE
    '''''''''''''''''''''''''''''''''''
    For K = 1 To indRed '???????????????? pu� essere maggiore di 1???????????????
      indentRed = 0
      If Len(testoRed(K)) Then  'PU� ENTRARE QUI???
        'CONDIZIONE PER TABELLE RIDEFINITE
        testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      indentGen = indentGNsq + indentRed
      
      testo = testo & Space(11 + indentGNsq + indentRed) & _
        "EXEC SQL DECLARE " & CursorName & Format(K, "00") & " CURSOR " & IIf(withold, "WITH HOLD", "") & vbCrLf & _
        Space(11 + indentGNsq + indentRed) & " FOR SELECT " & vbCrLf
      indPart = Len(nomeTabIndPart) > 0 'SQ - passarlo come parametro!
      testo = testo & CostruisciSelect("T" & indEnd, idRed(K))
      indPart = False
             
      ''Dim idSave As Double
      Dim tabSave As String, idxSave As String, idxSave2 As String, opSave As String
      'in realt� potrei farlo sempre, in quanto nel vettore, in caso senza redefines
      'c'� comunque lo stesso segmento/tabella
      If K > 1 Or flagRed Then
        idSave = instrLevel.idTable
        tabSave = instrLevel.Tavola
        instrLevel.Tavola = nomeTRed(K)
        instrLevel.idTable = idRed(K)
      End If
      'SP 12/9: altro gioco di prestigio per gli indici particolari...
      'tutto da mettere meglio nella versione definitiva
      If Len(nomeTabIndPart) Then
        idSave = instrLevel.idTable
        tabSave = instrLevel.Tavola
        indEnd = indEnd + 1
        ReDim Preserve TabIstr.BlkIstr(indEnd)
        TabIstr.BlkIstr(indEnd).idTable = idTabIndPart
        TabIstr.BlkIstr(indEnd).Tavola = nomeTabIndPart
        '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
        testo = testo & CostruisciSelect("T" & indEnd, idTabIndPart)
      End If
      
      testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom(1, indEnd, True)
     
      If Len(nomeTabIndPart) Then
         TabIstr.BlkIstr(indEnd - 1).idTable = idTabIndPart
         TabIstr.BlkIstr(indEnd - 1).Tavola = nomeTabIndPart
         TabIstr.BlkIstr(indEnd).idTable = idSave
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         'proviamo con la squalificata
         ReDim Preserve TabIstr.BlkIstr(indEnd).KeyDef(1)
         indPart = True
      Else
         indPart = False
      End If
      
      testo = testo & CostruisciWhere(1, indEnd)
      
      If Len(nomeTabIndPart) Then
         'rimette a posto
         indEnd = indEnd - 1
         ReDim Preserve TabIstr.BlkIstr(indEnd)
         TabIstr.BlkIstr(indEnd).idTable = idSave
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         indPart = False
      End If
      If K > 1 Or flagRed Then
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         TabIstr.BlkIstr(indEnd).idTable = idSave
      End If
    
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
    
      If Len(testoRed(K)) Then
        testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next K
    testo = testo & vbCrLf
     
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
         testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
         indentRed = 3
      End If
      testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If Len(testoRed(K)) Then
         testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next
  
    testo = testo & Space(11 + indentGN) & "IF SQLCODE = -502" & vbCrLf
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
         testo = testo & Space(14 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
         indentRed = 3
      End If
      testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If Len(testoRed(K)) Then
        testo = testo & Space(14 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next
     
    testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
    testo = testo & Space(11 + indentGN) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf & vbCrLf & vbCrLf
   
    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    If EmbeddedRoutine Then
      testo = testo & Space(11) & "   PERFORM FETCH-" & CursorName & vbCrLf & _
                      Space(11) & "      THRU " & CursorName & "-EX" & vbCrLf
    Else
      testo = testo & Space(11) & "   PERFORM 9999-FETCH-" & CursorName & vbCrLf
    End If
    testo = testo & Space(11) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf  'SQ - 29-08
    testo = testo & Space(11) & "END-IF" & vbCrLf & vbCrLf
  
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
        testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If testoRed(K) <> "" Then
         testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next
    testo = testo & vbCrLf
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' TESTO FETCH:
    ' Viene scritto in fondo alla routine, in getRoutine_DB2
    ' Se ho pi� livelli, appendo il testo alle fecth dei precedenti
    ' SQ - si pu� isolare?
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'TMP
    GbTestoFetch = GbTestoFetch & getFetch(indRed, indEnd, indentGN, indentGNqu)
    
    ''''''''''''''''''''''''''''''
    ' OCCURS
    ''''''''''''''''''''''''''''''
    If Not flagIns Then
      isOccurs = createOccursRoutine(instrLevel, tipoIstr, flagOcc)
    End If
     
    'loop sulle tabelle ridefinite: nome tabella gi� mosso da template in copy move
    'SP 12/9: per ora non gestisce gli indici particolari
    ''''''''''tabFound = False
    If flagRed Then
      'REDEFINES: CHIAMATA ROUTINE
      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      testo = testo & Space(11) & "   MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-TO-RD" & vbCrLf
      testo = testo & Space(11) & "END-IF" & vbCrLf
          
      createRedefineRoutine instrLevel, tipoIstr, flagOcc, indSav, flagIns
    End If
    
    'esce se richiamata dalla insert: ATTENZIONE: PROBABILMENTE E' UN "NEXT FOR"!!!!!
    If flagIns Then
      getGU_DB2 = testo
      Exit Function '!!!!!!!!
    End If
     
    NomeT = TabIstr.BlkIstr(indEnd).Tavola
    NomePk = TabIstr.BlkIstr(indEnd).pKey
    If TabIstr.BlkIstr(indEnd).IdSegm Then
        Dim indGet As Long
      indGet = indEnd
      '?wLiv01Copy = getCopyDLISeg(indGet)
      
      If Not flagRed Then
        For K = 1 To indRed
          If testoRed(K) <> "" Then
             testo = testo & Space(11) & Trim(testoRed(K)) & vbCrLf
             indentRed = 3
          End If
          testo = testo & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
          testo = testo & Space(11 + indentRed) & "   PERFORM 9999-" & Replace(nomeTRed(K), "_", "-") & "-TO-RD" & vbCrLf
          testo = testo & Space(11 + indentRed) & "   MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE " & vbCrLf
          testo = testo & Space(11 + indentRed) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf
          'la chiave deve essere salvata anche se non c'era il blocco chiavi;
          'ad esempio per una GN totalmente squalificata, seguita
          'da una GNP, deve essere comunque recuperata la chiave letta con la GN
          'SP 5/9 keyfeedback; banca lo mettiamo in seguito
          'SP 12/9: indici particolari
          If Len(nomeTabIndPart) Then
              testo = testo & Space(6) & "*" & Space(7 + indentRed) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          Else
              testo = testo & Space(6) & "*" & Space(7 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'SQ tmp: ci vuole? (commentato)
          'testo = testo & Space(14 + indentRed) & "PERFORM SETK-WKEYWKUT THRU SETK-WKEYWKUT-EX" & vbCrLf
          'testo = testo & Space(6) & "*" & Space(4 + indentRed) & "PERFORM SETK-WKEYWKUT THRU SETK-WKEYWKUT-EX" & vbCrLf
          testo = testo & Space(6) & "*" & Space(7 + indentRed) & "PERFORM 9999-SETK-WKEYWKUT" & vbCrLf
          If Len(nomeTabIndPart) Then
              testo = testo & Space(14 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'SQ D
          testo = testo & Space(14 + indentRed) & "MOVE '" & Format(level, "00") & "' TO LNKDB-PCBSEGLV" & vbCrLf
          testo = testo & Space(11 + indentRed) & "END-IF" & vbCrLf
          If testoRed(K) <> "" Then
             testo = testo & Space(11) & "END-IF" & vbCrLf
          End If
        Next
      End If
      
      testo = testo & vbCrLf
      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      testo = testo & Space(11) & "   MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf
      If keySecFlagUltimoLiv Then
        testo = testo & Space(11) & "   PERFORM 9999-SETA-" & Replace(TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
        testo = testo & Space(11) & "   MOVE KRR-" & TabIstr.BlkIstr(indEnd).KeyDef(1).key & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space(11) & "   MOVE KRDP-" & TabIstr.BlkIstr(indEnd).KeyDef(1).key & "  TO LNKDB-FDBKEY" & vbCrLf
      End If
      testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
     
    'SP 9/9 pcb dinamico: salvataggio SSA formattata a modo nostro!
    ' per ora solo sulla GU, se non ok sar� fatta solo per qualificate e no pcb dinamico
    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    testo = testo & Space(11) & "   MOVE LNKDB-KEYNAME(" & indEnd & ", 1) TO WK-KEYNAME" & vbCrLf
    testo = testo & Space(11) & "   MOVE LNKDB-KEYOPER(" & indEnd & ", 1) TO WK-KEYOPER" & vbCrLf
    testo = testo & Space(11) & "   MOVE LNKDB-KEYVALUE(" & indEnd & ", 1) TO WK-KEYVALUE" & vbCrLf
    testo = testo & Space(11) & "   MOVE WK-SSA TO LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
     
    If flagOcc Or isOccurs Then
      'OCCURS:
      testo = testo & vbCrLf
      testo = testo & Space(6) & "*** OCCURS" & vbCrLf
      testo = testo & Space(14) & "MOVE " & indEnd & " TO WK-IND2" & vbCrLf
      testo = testo & Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      'TMP
      testoTmp = Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      testo = testo & vbCrLf
    End If
    
    If flagRed Then
      'REDEFINES:
      testo = testo & vbCrLf
      testo = testo & Space(6) & "*** REDEFINES" & vbCrLf
      testo = testo & Space(14) & "MOVE " & indEnd & " TO WK-IND2" & vbCrLf
      testo = testo & Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      'TMP
      testoTmp = Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      testo = testo & Space(14) & "MOVE WK-DATA-AREA TO RD-" & instrLevel.Segment & vbCrLf
      
      'SQ D
      'Tmp: ok, per ora, ripeterla per ogni livello...
      'If level = UBound(TabIstr.BlkIstr) Then
        If InStr(TabIstr.BlkIstr(1).CodCom, "D") And indEnd > 1 Then
          'STRING:
          testo = testo & Space(14) & "STRING  RD-" & TabIstr.BlkIstr(1).Segment & vbCrLf
          For i = 2 To indEnd
            testo = testo & Space(14) & "        RD-" & TabIstr.BlkIstr(i).Segment & vbCrLf
          Next
          testo = testo & Space(14) & "  DELIMITED BY SIZE" & vbCrLf
          testo = testo & Space(14) & "  INTO LNKDB-DATA-AREA(" & indEnd & ")" & vbCrLf
        Else
          'MOVE:
          testo = testo & Space(14) & "MOVE RD-" & instrLevel.Segment & " TO LNKDB-DATA-AREA(" & indEnd & ")" & vbCrLf
        End If
      'End If
    Else
      'SQ 31-07-06 - C.C. "D"
      'SQ D
      'Tmp: ok, per ora, ripeterla per ogni livello...
      'If level = UBound(TabIstr.BlkIstr) Then
        If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
          'Ho letto le io-aree di tutti i livelli: tutte nella LNKDB-DATA-AREA
          testo = testo & Space(14) & "STRING  RD-" & TabIstr.BlkIstr(1).Segment & vbCrLf
          For i = 2 To indEnd
            testo = testo & Space(14) & "        RD-" & TabIstr.BlkIstr(i).Segment & vbCrLf
          Next
          testo = testo & Space(14) & "  DELIMITED BY SIZE" & vbCrLf
          testo = testo & Space(14) & "  INTO LNKDB-DATA-AREA(" & UBound(TabIstr.BlkIstr) & ")" & vbCrLf
        Else
          testo = testo & Space(14) & "MOVE RD-" & instrLevel.Segment & " TO LNKDB-DATA-AREA(" & UBound(TabIstr.BlkIstr) & ")" & vbCrLf
        End If
      'End If
    End If
    testo = testo & Space(11) & "END-IF" & vbCrLf
    
    'SQ D
    If InStr(TabIstr.BlkIstr(1).CodCom, "D") And level > 1 Then
      'Lettura solo se il precedente livello andato a buon fine
      testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
    testo = testo & vbCrLf
  
    '''''''''''''''''''''''
    ' Scrittura Copy P
    '''''''''''''''''''''''
    If Len(GbTestoRedOcc) Then
      Dim rtbFileSave As String
      rtbFileSave = rtbFile.text  'salvo
      rtbFile.text = GbTestoRedOcc
      rtbFile.SaveFile VSAM2Rdbms.drPathDb & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\cpy\" & includedMember, 1
      rtbFile.text = rtbFileSave
      GbTestoRedOcc = ""  'flag x chiamante
      'transitorio: buttare
      GbFileRedOcc = ""
    End If
    If Len(includedMember) Then 'Verificare... se serve utilizzare Collection...
      GbFileRedOccs(UBound(GbFileRedOccs)) = includedMember
      ReDim Preserve GbFileRedOccs(UBound(GbFileRedOccs) + 1)
    End If
  Next level
  'TMP
  'getGU_DB2 = testoTmp
  getGU_DB2 = testo

End Function
''''''''''''''''''''''''''''''
' Solo GNP
''''''''''''''''''''''''''''''
Public Function getGNP_DB2(rtbFile As RichTextBox, tipoIstr As String, Optional ByVal withold As Boolean = False) As String
  Dim K As Long
  Dim i As Integer, k1 As Integer, indEnd As Integer, indSav As Integer, indentGNsqKeyOp As Integer
  Dim NomeT As String, NomePk As String, nomeKey As String, testo As String
  Dim TestoFrom As String, TestoWhere As String
  Dim keySecFlagPrimoLiv As Boolean, keySecFlagUltimoLiv As Boolean
  Dim flagSel As Boolean, flagIns As Boolean, flagRed As Boolean
  Dim flagOcc As Boolean, isOccurs As Boolean
  Dim tb1 As Recordset, tb2 As Recordset, tbred As Recordset, rsIndPart As Recordset
  Dim indentGN As Integer, indentGNsq As Integer, indentGNqu As Integer
  Dim keyPrim As String, keySec As String
  Dim indRed As Integer
  Dim keySave() As BlkIstrKey
  Dim istrSave As IstrDli
  Dim instrLevel As BlkIstrDli
  Dim nomeTab As String
  'SQ
  Dim testoTmp As String
  
  indentGen = 0
  indentRed = 0
  
  'TMP:
  If UBound(TabIstr.BlkIstr) Then
    If InStr(TabIstr.BlkIstr(1).CodCom, "D") And tipoIstr <> "ISRT" Then
      level = 1
    Else
      level = UBound(TabIstr.BlkIstr)
    End If
  Else
    level = 0
  End If
  
  If Not EmbeddedRoutine Then
    testo = Space(11) & "MOVE '" & tipoIstr & "' TO WK-ISTR" & vbCrLf
  End If
  
  For level = level To UBound(TabIstr.BlkIstr)
    '''''''''''''
    'init:
    '''''''''''''
    'tmp: (ora � ciclico!... togliere quelli che non servono!)
    'keySecFlagPrimoLiv = False
    keySecFlagUltimoLiv = False
    flagOcc = False
    ''''flagIns = False
    '''''''''tabFound = False
    isOccurs = False
    'tabFound3 = False
    includedMember = ""
    indentGN = 3  'diff
    indentGNsq = 3  'diff
    indentGNqu = 0
    indentGNsqKeyOp = 0
    indentRed = 0
      
    If tipoIstr <> "ISRT" Then
      'SQ D
      indEnd = level
      instrLevel = TabIstr.BlkIstr(indEnd)
      'SQ D
      'testo = InitIstr(tipoIstr, "N")
      
      'TMP
      If UBound(TabIstr.BlkIstr) Then
        If InStr(TabIstr.BlkIstr(1).CodCom, "D") And level > 1 Then
          'Lettura solo se il precedente livello andato a buon fine
          testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
        End If
      End If
                 
      If Not EmbeddedRoutine Then
        If UBound(TabIstr.BlkIstr) Then
          testo = testo & Space(11) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
          testo = testo & Space(11) & "MOVE '" & instrLevel.Segment & "' TO WK-SEGMENT" & vbCrLf
        End If
        testo = testo & Space(11) & "PERFORM 9000-MANAGE-PCB" & vbCrLf
        'SQ D
        testo = testo & Space(11) & "INITIALIZE RD-" & instrLevel.Segment & vbCrLf
      End If
      
      flagIns = False
    Else
      'SQ D
      indEnd = UBound(TabIstr.BlkIstr) - 1
      instrLevel = TabIstr.BlkIstr(indEnd)
      testo = testo & Space(11) & "INITIALIZE RD-" & instrLevel.Segment & vbCrLf  'SQ - Ci vuole?
      flagIns = True
    End If
    
    If Not indEnd > 0 Then  'SQ - portare fuori
      m_dllFunctions.WriteLog "Instruction: " & GbNumIstr & " - not converted", "Routines Generation"
      Exit Function
    End If
        
    testo = testo & Space(11) & "MOVE LNKDB-SEGLEVEL(" & indEnd & ") TO LNKDB-STKLEV(LNKDB-NUMSTACK)" & vbCrLf & vbCrLf
    
    
    'diff
    'SQ 31-07-06 - C.C. "F"
    If InStr(instrLevel.CodCom, "F") Then
      'Sistemare INDENT!
    Else
      testo = testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
      testo = testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GbNumIstr & vbCrLf
    End If
    testo = testo & Space(14) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
    testo = testo & Space(14) & "MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
    
    
    'SQ - sto pezzo probabilmente � da tenere fuori ciclo... giusto?
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Segmento "parent":
    'stefanopippo: 15/08/2005: se su chiave secondaria (sempre sul primo livello!) non fa restore (� inutile)
    keySecFlagPrimoLiv = False
    If UBound(TabIstr.BlkIstr(1).KeyDef) Then
      keySecFlagPrimoLiv = TabIstr.BlkIstr(1).KeyDef(1).key <> TabIstr.BlkIstr(1).pKey
    End If
    If Not keySecFlagPrimoLiv Then
       NomeT = getParentTable(CLng(TabIstr.BlkIstr(1).IdSegm))  'togliere cast e cambiare function
       If Len(NomeT) Then
          'SEGMENTO NON RADICE: RESTORE
          testo = testo & Space(11 + indentGNsq) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
          testo = testo & Space(11 + indentGNsq) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
       End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If UBound(instrLevel.KeyDef) Then
      'nuovo sistema: si intende squalificata quando lo � sull'ultimo livello
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        keySecFlagUltimoLiv = True
'''MG 240107
'''''        Set rsIndPart = m_dllFunctions.Open_Recordset( _
'''''          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, DMDB2_Tabelle as b " & _
'''''          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
'''''          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")
        Set rsIndPart = m_dllFunctions.Open_Recordset( _
          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & DB & "Tabelle as b " & _
          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")

        If Not rsIndPart.EOF Then
          If rsIndPart!nomeTab <> instrLevel.Tavola Then
             nomeTabIndPart = rsIndPart!nomeTab
             idTabIndPart = rsIndPart!idTab
          End If
        End If
      Else
        keySecFlagUltimoLiv = False
      End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' RESTORE CHIAVE:
    ' Attenzione: per ottimizzare posso appoggiare il nome della tabella in TabIstr.BlkIstr(0)! (valutare)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    For K = 1 To indEnd
      ''''''''''''''''''''''''''''Kt = k  '??????????????????????????????????????????????????
      NomeT = TabIstr.BlkIstr(K).Tavola
      If Trim(NomeT) = "" Then
        NomeT = "[NO-TABLE-FOR:" & TabIstr.BlkIstr(K).Segment & "]"
      End If
      NomePk = TabIstr.BlkIstr(K).pKey
      testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
      'squalificata solo sull'ultimo livello, imposta la chiave primaria ereditata
      If UBound(TabIstr.BlkIstr(K).KeyDef) = 0 And K = BlkFin Then
        testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
        testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO K01-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
      End If
      For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
        If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
          nomeKey = Replace(TabIstr.BlkIstr(K).KeyDef(k1).key, "-", "_")
          'SQ
          'NomePk = TabIstr.BlkIstr(k).KeyDef(k1).NameAlt
          NomePk = nomeKey
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(K)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
          testo = testo & Space(16 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf
          'SP 12/9: indici particolari
          If Len(nomeTabIndPart) And K = indEnd Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
          testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
          If nomeTabIndPart <> "" And K = indEnd Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
          End If
          If TabIstr.BlkIstr(K).pKey = TabIstr.BlkIstr(K).KeyDef(k1).key Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
          End If
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K" & Format(TabIstr.BlkIstr(K).KeyDef(k1).KeyNum, "00") & "-" & NomePk & vbCrLf
        End If
      Next k1
    Next K
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    'UTILIZZO DI ARRAY ANCHE PER CASI "NORMALI"
    indRed = 1
    ReDim Preserve testoRed(1)
    ReDim Preserve nomeTRed(1)
    ReDim Preserve nomeSRed(1)
    ReDim Preserve idRed(1)
    ReDim Preserve nomeKeyRed(1)
    ReDim Preserve nomeFieldRed(1)
    idRed(1) = instrLevel.idTable
    If UBound(instrLevel.KeyDef) Then
      nomeKeyRed(1) = instrLevel.KeyDef(1).key
    Else
      nomeKeyRed(1) = ""
    End If
    Dim tbred2 As Recordset
    Set tbred2 = m_dllFunctions.Open_Recordset( _
      "select nome as nomeidx from psdli_field where unique and idsegmento=" & instrLevel.IdSegm)
    If Not tbred2.EOF Then
      nomeFieldRed(1) = tbred2!Nomeidx
    Else
      m_dllFunctions.WriteLog "No Field found for segment: " & instrLevel.IdSegm, "Routines Generation"
      nomeFieldRed(1) = ""
    End If
    nomeTRed(1) = instrLevel.Tavola
    nomeSRed(1) = instrLevel.Segment
    
    'stefanopippo: redefines; per la SELECT IL PROBLEMA E' SOLO SULL'ULTIMO LIVELLO DELLA SELECT,
    'IN QUANTO NON ESISTE JOIN A MENO DI CHIAVE SECONDARIA, ED IN OGNI CASO LA SECONDARIA SAREBBE SULL'ULTIMA!
    ''''''''''''''''''''''''''''''''''''
    ' REDEFINES: PREPARAZIONE STRUTTURE
    ''''''''''''''''''''''''''''''''''''
    Set tbred = m_dllFunctions.Open_Recordset( _
      "select condizione, RDBMS_tbname from PsDli_Segmenti where idSegmento = " & instrLevel.IdSegm)
    If Len(tbred!condizione) Then
      testoRed(1) = getCondizione_Redefines(tbred!condizione & "_lett")
    Else
      testoRed(1) = ""
    End If
    tbred.Close
    
    Dim tbsel As Recordset
'''MG 240107
'''''    Set tbsel = m_dllFunctions.Open_Recordset( _
'''''      "select idtable, nome from dmdb2_tabelle where idorigine = " & instrLevel.IdSegm & " and TAG like '%DISPATCHER%'")
    Set tbsel = m_dllFunctions.Open_Recordset( _
      "select idtable, nome from " & DB & "tabelle where idorigine = " & instrLevel.IdSegm & " and TAG like '%DISPATCHER%'")

    If Not tbsel.EOF Then
      flagRed = True
'''MG 240107
'''''      Set tbred = m_dllFunctions.Open_Recordset( _
'''''        "select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
'''''        "from mgdli_segmenti as a, dmdb2_tabelle as b, dmdb2_index as c where a.idsegmentoorigine = " & instrLevel.IdSegm & _
'''''        " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
      Set tbred = m_dllFunctions.Open_Recordset( _
        "select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
        "from mgdli_segmenti as a, " & DB & "tabelle as b, " & DB & "index as c where a.idsegmentoorigine = " & instrLevel.IdSegm & _
        " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")

      If tbred.RecordCount Then
        ReDim Preserve testoRed(tbred.RecordCount + 1)
        ReDim Preserve nomeTRed(tbred.RecordCount + 1)
        ReDim Preserve nomeSRed(tbred.RecordCount + 1)
        ReDim Preserve idRed(tbred.RecordCount + 1)
        ReDim Preserve nomeKeyRed(tbred.RecordCount + 1)
        ReDim Preserve nomeFieldRed(tbred.RecordCount + 1)
        While Not tbred.EOF
          indRed = indRed + 1
          If Len(tbred!condizione) Then
            If flagRed Then 'SEMPRE VERO (qui)?!
            
              testoRed(1) = ""  '????????????? (non posso farlo fuori ciclo?)
              idRed(1) = tbsel!idTable
              nomeTRed(1) = tbsel!nome
              nomeSRed(1) = "DISPATCHER"
              
              If UBound(instrLevel.KeyDef) > 0 Then   '?? non l'ho gi� fatto sopra? (fuori ciclo?)
                nomeKeyRed(1) = instrLevel.KeyDef(1).key
                nomeFieldRed(1) = instrLevel.KeyDef(1).NameAlt
              Else
                'SQ - 26-07-06 - Pezza per non cambiare cose che non so!
                nomeKeyRed(1) = Replace(instrLevel.pKey, "_", "-")
                ' => vado a prendere il nome colonna corretto!
'''MG 240107
'''''                Set tbred2 = m_dllFunctions.Open_Recordset( _
'''''                   "Select nome from Dmdb2_colonne where idTable=" & instrLevel.idTable & _
'''''                   " and idTable=idTableJoin and idCmp <> -1")
                Set tbred2 = m_dllFunctions.Open_Recordset( _
                   "Select nome from " & DB & "colonne where idTable=" & instrLevel.idTable & _
                   " and idTable=idTableJoin and idCmp <> -1")

                If Not tbred2.EOF Then
                  nomeFieldRed(1) = Replace(tbred2!nome, "_", "-")
                Else
                  nomeFieldRed(1) = ""
                  m_dllFunctions.WriteLog "No 'field' column found for table: " & instrLevel.idTable, "Routines Generation"
                End If
                tbred2.Close
              End If
              testoRed(indRed) = "IF WK-NAMETABLE = '" & tbred!nomeTab & "'"
            Else
              testoRed(indRed) = getCondizione_Redefines(tbred!condizione & "_lett")
            End If
            idRed(indRed) = tbred!idTab
            nomeTRed(indRed) = tbred!nomeTab
            nomeSRed(indRed) = tbred!nomeseg
            nomeKeyRed(indRed) = tbred!nomeindex
            'stefano: campo per il nome del field
            Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
              "from mgdli_field where idfield = " & tbred!IdField & " and idsegmento = " & tbred!idSegmento)
            If Not tbred2.EOF Then
               nomeFieldRed(indRed) = tbred2!Nomeidx
            Else
               nomeFieldRed(indRed) = ""
               m_dllFunctions.WriteLog "Virtual Field not found: " & tbred!IdField, "Routines Generation"
            End If
           'SP redefines
          Else
            m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
          End If
          tbred.MoveNext
        Wend
        tbred.Close
      End If
      indSav = indRed
      indRed = 1
    Else
      flagRed = False
      testoRed(1) = ""
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    '''''''''''''''''''''''''''''''''''
    ' DECLARE CURSORE
    '''''''''''''''''''''''''''''''''''
    For K = 1 To indRed '???????????????? pu� essere maggiore di 1???????????????
      indentRed = 0
      If Len(testoRed(K)) Then  'PU� ENTRARE QUI???
        'CONDIZIONE PER TABELLE RIDEFINITE
        testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      indentGen = indentGNsq + indentRed
      
      testo = testo & Space(11 + indentGNsq + indentRed) & _
        "EXEC SQL DECLARE " & CursorName & Format(K, "00") & " CURSOR " & IIf(withold, "WITH HOLD", "") & vbCrLf & _
        Space(11 + indentGNsq + indentRed) & " FOR SELECT " & vbCrLf
      indPart = Len(nomeTabIndPart) > 0 'SQ - passarlo come parametro!
      testo = testo & CostruisciSelect("T" & indEnd, idRed(K))
      indPart = False
             
      ''Dim idSave As Double
      Dim tabSave As String, idxSave As String, idxSave2 As String, opSave As String
      'in realt� potrei farlo sempre, in quanto nel vettore, in caso senza redefines
      'c'� comunque lo stesso segmento/tabella
      If K > 1 Or flagRed Then
        idSave = instrLevel.idTable
        tabSave = instrLevel.Tavola
        instrLevel.Tavola = nomeTRed(K)
        instrLevel.idTable = idRed(K)
      End If
      'SP 12/9: altro gioco di prestigio per gli indici particolari...
      'tutto da mettere meglio nella versione definitiva
      If Len(nomeTabIndPart) Then
        idSave = instrLevel.idTable
        tabSave = instrLevel.Tavola
        indEnd = indEnd + 1
        ReDim Preserve TabIstr.BlkIstr(indEnd)
        TabIstr.BlkIstr(indEnd).idTable = idTabIndPart
        TabIstr.BlkIstr(indEnd).Tavola = nomeTabIndPart
        '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
        testo = testo & CostruisciSelect("T" & indEnd, idTabIndPart)
      End If
      
      testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom(1, indEnd, True)
     
      If Len(nomeTabIndPart) Then
         TabIstr.BlkIstr(indEnd - 1).idTable = idTabIndPart
         TabIstr.BlkIstr(indEnd - 1).Tavola = nomeTabIndPart
         TabIstr.BlkIstr(indEnd).idTable = idSave
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         'proviamo con la squalificata
         ReDim Preserve TabIstr.BlkIstr(indEnd).KeyDef(1)
         indPart = True
      Else
         indPart = False
      End If
      
      testo = testo & CostruisciWhere(1, indEnd)
      
      If Len(nomeTabIndPart) Then
         'rimette a posto
         indEnd = indEnd - 1
         ReDim Preserve TabIstr.BlkIstr(indEnd)
         TabIstr.BlkIstr(indEnd).idTable = idSave
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         indPart = False
      End If
      If K > 1 Or flagRed Then
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         TabIstr.BlkIstr(indEnd).idTable = idSave
      End If
    
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
    
      If Len(testoRed(K)) Then
        testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next K
    testo = testo & vbCrLf
     
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
         testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
         indentRed = 3
      End If
      testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If Len(testoRed(K)) Then
         testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next
  
    testo = testo & Space(11 + indentGN) & "IF SQLCODE = -502" & vbCrLf
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
         testo = testo & Space(14 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
         indentRed = 3
      End If
      testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If Len(testoRed(K)) Then
        testo = testo & Space(14 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next
     
    testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
    testo = testo & Space(11 + indentGN) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf & vbCrLf & vbCrLf
   
    'diff
    'SQ 31-07-06 - C.C. "F"
    If InStr(instrLevel.CodCom, "F") Then
      'Sistemare INDENT!
    Else
      testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
    
    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    If EmbeddedRoutine Then
      testo = testo & Space(11) & "   PERFORM FETCH-" & CursorName & vbCrLf & _
                      Space(11) & "      THRU " & CursorName & "-EX" & vbCrLf
    Else
      testo = testo & Space(11) & "   PERFORM 9999-FETCH-" & CursorName & vbCrLf
    End If
    testo = testo & Space(11) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf  'SQ - 29-08
    testo = testo & Space(11) & "END-IF" & vbCrLf & vbCrLf
        
    'diff
    testo = testo & Space(11) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
    testo = testo & Space(11) & "   MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
    
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
        testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If testoRed(K) <> "" Then
         testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next
    'diff
    testo = testo & Space(11) & "END-IF" & vbCrLf
    
    testo = testo & vbCrLf
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' TESTO FETCH:
    ' Viene scritto in fondo alla routine, in getRoutine_DB2
    ' Se ho pi� livelli, appendo il testo alle fecth dei precedenti
    ' SQ - si pu� isolare?
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'TMP
    GbTestoFetch = GbTestoFetch & getFetch(indRed, indEnd, indentGN, indentGNqu)
    
    ''''''''''''''''''''''''''''''
    ' OCCURS
    ''''''''''''''''''''''''''''''
    If Not flagIns Then
      isOccurs = createOccursRoutine(instrLevel, tipoIstr, flagOcc)
    End If
     
    'loop sulle tabelle ridefinite: nome tabella gi� mosso da template in copy move
    'SP 12/9: per ora non gestisce gli indici particolari
    ''''''''''tabFound = False
    If flagRed Then
      'REDEFINES: CHIAMATA ROUTINE
      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      testo = testo & Space(11) & "   MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-TO-RD" & vbCrLf
      testo = testo & Space(11) & "END-IF" & vbCrLf
          
      createRedefineRoutine instrLevel, tipoIstr, flagOcc, indSav, flagIns
    End If
    
    'esce se richiamata dalla insert: ATTENZIONE: PROBABILMENTE E' UN "NEXT FOR"!!!!!
    If flagIns Then
      getGNP_DB2 = testo
      Exit Function '!!!!!!!!
    End If
     
    NomeT = TabIstr.BlkIstr(indEnd).Tavola
    NomePk = TabIstr.BlkIstr(indEnd).pKey
    If TabIstr.BlkIstr(indEnd).IdSegm Then
        Dim indGet As Long
      indGet = indEnd
      '?wLiv01Copy = getCopyDLISeg(indGet)
      
      If Not flagRed Then
        For K = 1 To indRed
          If testoRed(K) <> "" Then
             testo = testo & Space(11) & Trim(testoRed(K)) & vbCrLf
             indentRed = 3
          End If
          testo = testo & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
          testo = testo & Space(11 + indentRed) & "   PERFORM 9999-" & Replace(nomeTRed(K), "_", "-") & "-TO-RD" & vbCrLf
          testo = testo & Space(11 + indentRed) & "   MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE " & vbCrLf
          testo = testo & Space(11 + indentRed) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf
          'la chiave deve essere salvata anche se non c'era il blocco chiavi;
          'ad esempio per una GN totalmente squalificata, seguita
          'da una GNP, deve essere comunque recuperata la chiave letta con la GN
          'SP 5/9 keyfeedback; banca lo mettiamo in seguito
          'SP 12/9: indici particolari
          If Len(nomeTabIndPart) Then
              testo = testo & Space(6) & "*" & Space(7 + indentRed) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          Else
              testo = testo & Space(6) & "*" & Space(7 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'SQ tmp: ci vuole? (commentato)
          'testo = testo & Space(14 + indentRed) & "PERFORM SETK-WKEYWKUT THRU SETK-WKEYWKUT-EX" & vbCrLf
          'testo = testo & Space(6) & "*" & Space(4 + indentRed) & "PERFORM SETK-WKEYWKUT THRU SETK-WKEYWKUT-EX" & vbCrLf
          testo = testo & Space(6) & "*" & Space(7 + indentRed) & "PERFORM 9999-SETK-WKEYWKUT" & vbCrLf
          If Len(nomeTabIndPart) Then
              testo = testo & Space(14 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'SQ D
          testo = testo & Space(14 + indentRed) & "MOVE '" & Format(level, "00") & "' TO LNKDB-PCBSEGLV" & vbCrLf
          testo = testo & Space(11 + indentRed) & "END-IF" & vbCrLf
          If testoRed(K) <> "" Then
             testo = testo & Space(11) & "END-IF" & vbCrLf
          End If
        Next
      End If
      
      testo = testo & vbCrLf
      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      testo = testo & Space(11) & "   MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf
      If keySecFlagUltimoLiv Then
        testo = testo & Space(11) & "   PERFORM 9999-SETA-" & Replace(TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
        testo = testo & Space(11) & "   MOVE KRR-" & TabIstr.BlkIstr(indEnd).KeyDef(1).key & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space(11) & "   MOVE KRDP-" & TabIstr.BlkIstr(indEnd).KeyDef(1).key & "  TO LNKDB-FDBKEY" & vbCrLf
      End If
      testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
     
    'SP 9/9 pcb dinamico: salvataggio SSA formattata a modo nostro!
    ' per ora solo sulla GU, se non ok sar� fatta solo per qualificate e no pcb dinamico
    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    
    'diff
    'testo = testo & Space(11) & "   MOVE LNKDB-KEYNAME(" & indEnd & ", 1) TO WK-KEYNAME" & vbCrLf
    'testo = testo & Space(11) & "   MOVE LNKDB-KEYOPER(" & indEnd & ", 1) TO WK-KEYOPER" & vbCrLf
    'testo = testo & Space(11) & "   MOVE LNKDB-KEYVALUE(" & indEnd & ", 1) TO WK-KEYVALUE" & vbCrLf
    'testo = testo & Space(11) & "   MOVE WK-SSA TO LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
     
    If flagOcc Or isOccurs Then
      'OCCURS:
      testo = testo & vbCrLf
      testo = testo & Space(6) & "*** OCCURS" & vbCrLf
      testo = testo & Space(14) & "MOVE " & indEnd & " TO WK-IND2" & vbCrLf
      testo = testo & Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      'TMP
      testoTmp = Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      testo = testo & vbCrLf
    End If
    
    If flagRed Then
      'REDEFINES:
      testo = testo & vbCrLf
      testo = testo & Space(6) & "*** REDEFINES" & vbCrLf
      testo = testo & Space(14) & "MOVE " & indEnd & " TO WK-IND2" & vbCrLf
      testo = testo & Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      'TMP       testoTmp = Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      testo = testo & Space(14) & "MOVE WK-DATA-AREA TO RD-" & instrLevel.Segment & vbCrLf
      
      'SQ D
      'Tmp: ok, per ora, ripeterla per ogni livello...
      'If level = UBound(TabIstr.BlkIstr) Then
        If InStr(TabIstr.BlkIstr(1).CodCom, "D") And indEnd > 1 Then
          'STRING:
          testo = testo & Space(14) & "STRING  RD-" & TabIstr.BlkIstr(1).Segment & vbCrLf
          For i = 2 To indEnd
            testo = testo & Space(14) & "        RD-" & TabIstr.BlkIstr(i).Segment & vbCrLf
          Next
          testo = testo & Space(14) & "  DELIMITED BY SIZE" & vbCrLf
          testo = testo & Space(14) & "  INTO LNKDB-DATA-AREA(" & indEnd & ")" & vbCrLf
        Else
          'MOVE:
          testo = testo & Space(14) & "MOVE RD-" & instrLevel.Segment & " TO LNKDB-DATA-AREA(" & indEnd & ")" & vbCrLf
        End If
      'End If
    Else
      'SQ 31-07-06 - C.C. "D"
      'SQ D
      'Tmp: ok, per ora, ripeterla per ogni livello...
      'If level = UBound(TabIstr.BlkIstr) Then
        If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
          'Ho letto le io-aree di tutti i livelli: tutte nella LNKDB-DATA-AREA
          testo = testo & Space(14) & "STRING  RD-" & TabIstr.BlkIstr(1).Segment & vbCrLf
          For i = 2 To indEnd
            testo = testo & Space(14) & "        RD-" & TabIstr.BlkIstr(i).Segment & vbCrLf
          Next
          testo = testo & Space(14) & "  DELIMITED BY SIZE" & vbCrLf
          testo = testo & Space(14) & "  INTO LNKDB-DATA-AREA(" & UBound(TabIstr.BlkIstr) & ")" & vbCrLf
        Else
          testo = testo & Space(14) & "MOVE RD-" & instrLevel.Segment & " TO LNKDB-DATA-AREA(" & UBound(TabIstr.BlkIstr) & ")" & vbCrLf
        End If
      'End If
    End If
    testo = testo & Space(11) & "END-IF" & vbCrLf
    
    'SQ D
    If InStr(TabIstr.BlkIstr(1).CodCom, "D") And level > 1 Then
      'Lettura solo se il precedente livello andato a buon fine
      testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
    testo = testo & vbCrLf
  
    '''''''''''''''''''''''
    ' Scrittura Copy P
    '''''''''''''''''''''''
    If Len(GbTestoRedOcc) Then
      Dim rtbFileSave As String
      rtbFileSave = rtbFile.text  'salvo
      rtbFile.text = GbTestoRedOcc
      rtbFile.SaveFile VSAM2Rdbms.drPathDb & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\cpy\" & includedMember, 1
      rtbFile.text = rtbFileSave
      GbTestoRedOcc = ""  'flag x chiamante
      'transitorio: buttare
      GbFileRedOcc = ""
    End If
    If Len(includedMember) Then 'Verificare... se serve utilizzare Collection...
      GbFileRedOccs(UBound(GbFileRedOccs)) = includedMember
      ReDim Preserve GbFileRedOccs(UBound(GbFileRedOccs) + 1)
    End If
  Next level
  'TMP
  'getGNP_DB2 = testoTmp
  getGNP_DB2 = testo

End Function
''''''''''''''''''''''''''''''
' Solo GN
''''''''''''''''''''''''''''''
Public Function getGN_DB2(rtbFile As RichTextBox, tipoIstr As String, Optional ByVal withold As Boolean = False) As String
  Dim K As Long
  Dim i As Integer, k1 As Integer, indEnd As Integer, indSav As Integer, indentGNsqKeyOp As Integer
  Dim NomeT As String, NomePk As String, nomeKey As String, testo As String
  Dim TestoFrom As String, TestoWhere As String
  Dim keySecFlagPrimoLiv As Boolean, keySecFlagUltimoLiv As Boolean
  Dim flagSel As Boolean, flagIns As Boolean, flagRed As Boolean
  Dim flagOcc As Boolean, isOccurs As Boolean
  Dim tb1 As Recordset, tb2 As Recordset, tbred As Recordset, rsIndPart As Recordset, tb As Recordset
  Dim indentGN As Integer, indentGNsq As Integer, indentGNqu As Integer
  Dim keyPrim As String, keySec As String
  Dim indRed As Integer
  Dim keySave() As BlkIstrKey
  Dim istrSave As IstrDli
  Dim instrLevel As BlkIstrDli
  Dim nomeTab As String
  'SQ
  Dim testoTmp As String
  'diff
  Dim qualified As Boolean, keyOp As Boolean, keyPrimUguale As Boolean
    
  indentGen = 0
  indentRed = 0
  
  If InStr(TabIstr.BlkIstr(1).CodCom, "D") And tipoIstr <> "ISRT" Then
    level = 1
  Else
    level = UBound(TabIstr.BlkIstr)
  End If
  
  If Not EmbeddedRoutine Then
    testo = Space(11) & "MOVE '" & tipoIstr & "' TO WK-ISTR" & vbCrLf
  End If
  
  For level = level To UBound(TabIstr.BlkIstr)
    '''''''''''''
    'init:
    '''''''''''''
    'tmp: (ora � ciclico!... togliere quelli che non servono!)
    'keySecFlagPrimoLivIf UBound(instrLevel.KeyDef) = False
    keySecFlagUltimoLiv = False
    flagOcc = False
    ''''flagIns = False
    '''''''''tabFound = False
    isOccurs = False
    'tabFound3 = False
    includedMember = ""
        
    If tipoIstr <> "ISRT" Then  'SE RIMARRANNO SEPARATE: BUTTARE!
      'SQ D
      indEnd = level
      instrLevel = TabIstr.BlkIstr(indEnd)
      'SQ D
      'testo = InitIstr(tipoIstr, "N")
      
      If InStr(TabIstr.BlkIstr(1).CodCom, "D") And level > 1 Then
        'Lettura solo se il precedente livello andato a buon fine
        testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      End If
      
      If Not EmbeddedRoutine Then
        If UBound(TabIstr.BlkIstr) Then
          testo = testo & Space(11) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
          testo = testo & Space(11) & "MOVE '" & instrLevel.Segment & "' TO WK-SEGMENT" & vbCrLf
        End If
        testo = testo & Space(11) & "PERFORM 9000-MANAGE-PCB" & vbCrLf
      End If
      
      flagIns = False
    Else
      'SQ D
      indEnd = UBound(TabIstr.BlkIstr) - 1
      instrLevel = TabIstr.BlkIstr(indEnd)
      flagIns = True
    End If
    
    If Not indEnd > 0 Then  'SQ - portare fuori
      m_dllFunctions.WriteLog "Instruction: " & GbNumIstr & " - not converted", "Routines Generation"
      Exit Function
    End If
        
        
    'diff
    If UBound(instrLevel.KeyDef) Then
      'nuovo sistema: si intende squalificata quando lo � sull'ultimo livello
      qualified = True 'portare dentro la IF
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        keySecFlagUltimoLiv = True
        'SQ - 21-07-06
        'TAG='AUTOMATIC' - IN OGNI CASO NON POSSO CERCARE PER NOME...
        ' => Aggiunto filtro DBD (VERIFICARE)!!!!!!!!!!
'''MG 240107
'''''        Set rsIndPart = m_dllFunctions.Open_Recordset( _
'''''          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, DMDB2_Tabelle as b " & _
'''''          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
'''''          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")
        Set rsIndPart = m_dllFunctions.Open_Recordset( _
          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & DB & "Tabelle as b " & _
          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")

        If Not rsIndPart.EOF Then
          If rsIndPart!nomeTab <> instrLevel.Tavola Then
             nomeTabIndPart = rsIndPart!nomeTab
             idTabIndPart = rsIndPart!idTab
          End If
        
       End If
        'sulla secondaria, solo con l'operatore per maggiore deve creare la doppia declare (cio� l'esatto contrario della primaria)
        'SP 10/9: modificato, dovrebbe valere anche per l'operatore per =, cos� come fa per la primaria (sulla primaria, se operatore = "=" lo fa diventare ">",
        'per scartare il primo record, sulla secondaria, vista la fetch a vuoto, deve trasformarlo in ">="
        'SP 11/9 : RIMODIFICATO, ERA SBAGLIATA LA PRIMARIA, QUINDI SI FA SOLO PER >!!! (quelli per <, <= li vedremo poi, ma non dovrebbero dare grossi problemi)
        'LA DIFFERENZA TRA PRIMARIA E SECONDARIA E' DOVUTA AL FATTO CHE LA SECONDARIA PUO' AVERE DUPLICATI; COMUNQUE IN FUTURO ATTUEREMO IL SISTEMA DELLA FETCH A VUOTO, CHE ANDREBBE BENE
        'ANCHE PER LA PRIMARIA; PER ORA SI LASCIA COSI' PERCHE' LA PRIMARIA FUNZIONA!!! ANZI, ANCHE LA PRIMARIA, PER =, HA BISOGNO DELLA FETCH A VUOTO, CHE POI SERVE SOLO A
        'DARE "GE" (WHERE PER = SU UNIVOCA, CON SCARTO DEL PRIMO RECORD ==> SQLCODE = 100 PER FORZA!
        keyOp = instrLevel.KeyDef(1).Op <> ">"
      Else
        keySecFlagUltimoLiv = False
        'operatore gi� per maggiore, non deve creare la doppia declare
        'SP 11/9 : ANCHE PER UGUALE NON DEVE CREARE LA DOPPIA DECLARE, LO DEVE FARE SOLO PER >=!!! (quelli per <, <= li vedremo poi, ma non dovrebbero dare grossi problemi)
        keyOp = instrLevel.KeyDef(1).Op <> ">="
        'SP 11/9 Flag per forzare fetch a vuoto anche per primaria!!!
        keyPrimUguale = instrLevel.KeyDef(1).Op = "="
      End If
    Else
      qualified = False
    End If
        
    
    'diff
    indentGN = 3
    If keyOp Then
       indentGNsq = 3
       indentGNqu = 0
    Else
       indentGNsq = 6
       indentGNqu = 3
    End If
    indentGNsqKeyOp = 6
    indentRed = 0
      
    testo = testo & Space(11) & "INITIALIZE RD-" & instrLevel.Segment & vbCrLf
    testo = testo & Space(11) & "MOVE LNKDB-SEGLEVEL(" & indEnd & ") TO LNKDB-STKLEV(LNKDB-NUMSTACK)" & vbCrLf & vbCrLf
    
    'diff
    If qualified Then
      'SQ 31-07-06 - C.C. "F"
      If InStr(instrLevel.CodCom, "F") Then
        'Sistemare INDENT!
      Else
        testo = testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES " & vbCrLf
        testo = testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GbNumIstr & vbCrLf
      End If

      'SP 6/9: avanti con la doppiadeclare, tranne sulle secondarie, che fanno solo la IF per
      'reimpostare, cos� come l'operatore; MODIFICATO: DOPPIADECLARE ANCHE PER LE SECONDARIE
      'If Not keySecFlagUltimoLiv Then
      'SP 14/9 non funziona con un loop di GHU+REPL+GHN+REPL, ecc., perch� l'ultima
      'istruzione non era una GU, ma doveva considerarla cos�.
       testo = testo & Space(14) & "IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) NOT = 'GU' AND 'ISRT'" & vbCrLf
       testo = testo & Space(14) & "                          AND 'GN' AND 'REPL' AND 'DLET'" & vbCrLf
       If Not keyOp Then
         testo = testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
       End If
    Else
      'SQ 31-07-06 - C.C. "F"
      If InStr(instrLevel.CodCom, "F") Then
        'Sistemare INDENT!
      Else
       testo = testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
       testo = testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GbNumIstr & vbCrLf
      End If
      'SP 14/9 problema su GHU+REPL+GHN+REPL
      testo = testo & Space(11) & "   IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) NOT = 'GU' AND 'ISRT'" & vbCrLf
      testo = testo & Space(11) & "                             AND 'GN' AND 'REPL' AND 'DLET'" & vbCrLf
      testo = testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
    End If
    
    
    'SQ - sto pezzo probabilmente � da tenere fuori ciclo... giusto?
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Segmento "parent":
    'stefanopippo: 15/08/2005: se su chiave secondaria (sempre sul primo livello!) non fa restore (� inutile)
    keySecFlagPrimoLiv = False
    If UBound(TabIstr.BlkIstr(1).KeyDef) Then
      keySecFlagPrimoLiv = TabIstr.BlkIstr(1).KeyDef(1).key <> TabIstr.BlkIstr(1).pKey
    End If
    If Not keySecFlagPrimoLiv Then
       NomeT = getParentTable(CLng(TabIstr.BlkIstr(1).IdSegm))  'togliere cast e cambiare function
       If Len(NomeT) Then
          'SEGMENTO NON RADICE: RESTORE
          testo = testo & Space(11 + indentGNsq) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
          testo = testo & Space(11 + indentGNsq) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
       End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If UBound(instrLevel.KeyDef) Then
      'nuovo sistema: si intende squalificata quando lo � sull'ultimo livello
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        keySecFlagUltimoLiv = True
'''MG 240107
'''''        Set rsIndPart = m_dllFunctions.Open_Recordset( _
'''''          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, DMDB2_Tabelle as b " & _
'''''          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
'''''          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")
        Set rsIndPart = m_dllFunctions.Open_Recordset( _
          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & DB & "Tabelle as b " & _
          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")

        If Not rsIndPart.EOF Then
          If rsIndPart!nomeTab <> instrLevel.Tavola Then
             nomeTabIndPart = rsIndPart!nomeTab
             idTabIndPart = rsIndPart!idTab
          End If
        End If
      Else
        keySecFlagUltimoLiv = False
      End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' RESTORE CHIAVE:
    ' Attenzione: per ottimizzare posso appoggiare il nome della tabella in TabIstr.BlkIstr(0)! (valutare)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    For K = 1 To indEnd
      ''''''''''''''''''''''''''''Kt = k  '??????????????????????????????????????????????????
      NomeT = TabIstr.BlkIstr(K).Tavola
      If Trim(NomeT) = "" Then
        NomeT = "[NO-TABLE-FOR:" & TabIstr.BlkIstr(K).Segment & "]"
      End If
      NomePk = TabIstr.BlkIstr(K).pKey
      testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
      'squalificata solo sull'ultimo livello, imposta la chiave primaria ereditata
      If UBound(TabIstr.BlkIstr(K).KeyDef) = 0 And K = BlkFin Then
        testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
        testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO K01-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
      End If
      For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
        If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
          nomeKey = Replace(TabIstr.BlkIstr(K).KeyDef(k1).key, "-", "_")
          'SQ
          'NomePk = TabIstr.BlkIstr(k).KeyDef(k1).NameAlt
          NomePk = nomeKey
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(K)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
          testo = testo & Space(16 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf
          'SP 12/9: indici particolari
          If Len(nomeTabIndPart) And K = indEnd Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
          testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
          If nomeTabIndPart <> "" And K = indEnd Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
          End If
          If TabIstr.BlkIstr(K).pKey = TabIstr.BlkIstr(K).KeyDef(k1).key Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
          End If
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K" & Format(TabIstr.BlkIstr(K).KeyDef(k1).KeyNum, "00") & "-" & NomePk & vbCrLf
        End If
      Next k1
    Next K
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    'diff
    testo = testo & Space(14) & "ELSE" & vbCrLf
    If Not keyOp Then
      testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE 'O' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
    End If
    'da impostare diversamente in funzione del tipo lettura (qual/squal, operatore, ecc.)
    'per ora usa la primaria (vedi commentone sopra): CAMBIATO: VALE PER TUTTE LE PRIMARIE
    ' MA NON PER LE SECONDARIE, che fanno la fetch a vuoto
    'MODIFICATO ANCORA: ANCHE PER LE SECONDARIE, CHE FANNO COMUNQUE POI LA FETCH A VUOTO
    testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
    If Not keySecFlagUltimoLiv Then
       'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM REST-FDBKEY THRU REST-FDBKEY-EX" & vbCrLf
       testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
    Else
      NomePk = instrLevel.KeyDef(1).key
      testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-STKPTGKEY(LNKDB-NUMSTACK) TO " & vbCrLf
      testo = testo & Space(16 + indentGNsqKeyOp) & "KRR-" & NomePk & vbCrLf
    End If
    'SQ 7-07-06 - Controllare... non credo vada bene!
    testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K01-" & NomePk & vbCrLf
    'SP 9/9 pcb dinamico
    Dim j As Integer
    For k1 = 2 To UBound(instrLevel.KeyDef)
      j = k1

      If Trim(instrLevel.KeyDef(k1).key) <> "" Then
       nomeKey = instrLevel.KeyDef(k1).key
       NomePk = instrLevel.KeyDef(k1).NameAlt
       testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(indEnd)) & ", " & Format(j, "#0") & ") TO " & vbCrLf
       'SQ
       'testo = testo & Space(13 + indentGNsqKeyOp) & "KRD-" & instrLevel.KeyDef(k1).NameAlt & vbCrLf
       testo = testo & Space(13 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf
         
      'SP 12/9: indici particolari
       If nomeTabIndPart <> "" Then
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
       End If
       'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
       testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
       If nomeTabIndPart <> "" Then
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
       End If
       'SP 16/9: nella booleana � meglio che non ci sia!!!
       'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETC-WKEYWKEY THRU SETC-WKEYWKEY-EX " & vbCrLf
       ''SQ 7-07-06
       'testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K" & Format(instrLevel.KeyDef(k1).KeyNum, "00") & "-" & NomePk & vbCrLf
       testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & nomeKey & " TO K" & Format(instrLevel.KeyDef(k1).KeyNum, "00") & "-" & nomeKey & vbCrLf
      End If
    Next k1
    
    testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
    
    If Not keyOp Then
      testo = testo & Space(11 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
    End If
    '...diff
    
    
    'UTILIZZO DI ARRAY ANCHE PER CASI "NORMALI"
    indRed = 1
    ReDim Preserve testoRed(1)
    ReDim Preserve nomeTRed(1)
    ReDim Preserve nomeSRed(1)
    ReDim Preserve idRed(1)
    ReDim Preserve nomeKeyRed(1)
    ReDim Preserve nomeFieldRed(1)
    idRed(1) = instrLevel.idTable
    If UBound(instrLevel.KeyDef) Then
      nomeKeyRed(1) = instrLevel.KeyDef(1).key
    Else
      nomeKeyRed(1) = ""
    End If
    Dim tbred2 As Recordset
    Set tbred2 = m_dllFunctions.Open_Recordset( _
      "select nome as nomeidx from psdli_field where unique and idsegmento=" & instrLevel.IdSegm)
    If Not tbred2.EOF Then
      nomeFieldRed(1) = tbred2!Nomeidx
    Else
      m_dllFunctions.WriteLog "No Field found for segment: " & instrLevel.IdSegm, "Routines Generation"
      nomeFieldRed(1) = ""
    End If
    nomeTRed(1) = instrLevel.Tavola
    nomeSRed(1) = instrLevel.Segment
    
    'stefanopippo: redefines; per la SELECT IL PROBLEMA E' SOLO SULL'ULTIMO LIVELLO DELLA SELECT,
    'IN QUANTO NON ESISTE JOIN A MENO DI CHIAVE SECONDARIA, ED IN OGNI CASO LA SECONDARIA SAREBBE SULL'ULTIMA!
    ''''''''''''''''''''''''''''''''''''
    ' REDEFINES: PREPARAZIONE STRUTTURE
    ''''''''''''''''''''''''''''''''''''
    Set tbred = m_dllFunctions.Open_Recordset( _
      "select condizione, RDBMS_tbname from PsDli_Segmenti where idSegmento = " & instrLevel.IdSegm)
    If Len(tbred!condizione) Then
      testoRed(1) = getCondizione_Redefines(tbred!condizione & "_lett")
    Else
      testoRed(1) = ""
    End If
    tbred.Close
    
    Dim tbsel As Recordset
'''MG 240107
'''''    Set tbsel = m_dllFunctions.Open_Recordset( _
'''''      "select idtable, nome from dmdb2_tabelle where idorigine = " & instrLevel.IdSegm & " and TAG like '%DISPATCHER%'")
    Set tbsel = m_dllFunctions.Open_Recordset( _
      "select idtable, nome from " & DB & "tabelle where idorigine = " & instrLevel.IdSegm & " and TAG like '%DISPATCHER%'")

    If Not tbsel.EOF Then
      flagRed = True
'''MG 240107
'''''      Set tbred = m_dllFunctions.Open_Recordset( _
'''''        "select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
'''''        "from mgdli_segmenti as a, dmdb2_tabelle as b, dmdb2_index as c where a.idsegmentoorigine = " & instrLevel.IdSegm & _
'''''        " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
      Set tbred = m_dllFunctions.Open_Recordset( _
        "select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
        "from mgdli_segmenti as a, " & DB & "tabelle as b, " & DB & "index as c where a.idsegmentoorigine = " & instrLevel.IdSegm & _
        " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")

      If tbred.RecordCount Then
        ReDim Preserve testoRed(tbred.RecordCount + 1)
        ReDim Preserve nomeTRed(tbred.RecordCount + 1)
        ReDim Preserve nomeSRed(tbred.RecordCount + 1)
        ReDim Preserve idRed(tbred.RecordCount + 1)
        ReDim Preserve nomeKeyRed(tbred.RecordCount + 1)
        ReDim Preserve nomeFieldRed(tbred.RecordCount + 1)
        While Not tbred.EOF
          indRed = indRed + 1
          If Len(tbred!condizione) Then
            If flagRed Then 'SEMPRE VERO (qui)?!
            
              testoRed(1) = ""  '????????????? (non posso farlo fuori ciclo?)
              idRed(1) = tbsel!idTable
              nomeTRed(1) = tbsel!nome
              nomeSRed(1) = "DISPATCHER"
              
              If UBound(instrLevel.KeyDef) > 0 Then   '?? non l'ho gi� fatto sopra? (fuori ciclo?)
                nomeKeyRed(1) = instrLevel.KeyDef(1).key
                nomeFieldRed(1) = instrLevel.KeyDef(1).NameAlt
              Else
                'SQ - 26-07-06 - Pezza per non cambiare cose che non so!
                nomeKeyRed(1) = Replace(instrLevel.pKey, "_", "-")
                ' => vado a prendere il nome colonna corretto!
'''MG 240107
'''                Set tbred2 = m_dllFunctions.Open_Recordset( _
'''                   "Select nome from Dmdb2_colonne where idTable=" & instrLevel.idTable & _
'''                   " and idTable=idTableJoin and idCmp <> -1")
                Set tbred2 = m_dllFunctions.Open_Recordset( _
                   "Select nome from " & DB & "colonne where idTable=" & instrLevel.idTable & _
                   " and idTable=idTableJoin and idCmp <> -1")
                   
                If Not tbred2.EOF Then
                  nomeFieldRed(1) = Replace(tbred2!nome, "_", "-")
                Else
                  nomeFieldRed(1) = ""
                  m_dllFunctions.WriteLog "No 'field' column found for table: " & instrLevel.idTable, "Routines Generation"
                End If
                tbred2.Close
              End If
              testoRed(indRed) = "IF WK-NAMETABLE = '" & tbred!nomeTab & "'"
            Else
              testoRed(indRed) = getCondizione_Redefines(tbred!condizione & "_lett")
            End If
            idRed(indRed) = tbred!idTab
            nomeTRed(indRed) = tbred!nomeTab
            nomeSRed(indRed) = tbred!nomeseg
            nomeKeyRed(indRed) = tbred!nomeindex
            'stefano: campo per il nome del field
            Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
              "from mgdli_field where idfield = " & tbred!IdField & " and idsegmento = " & tbred!idSegmento)
            If Not tbred2.EOF Then
               nomeFieldRed(indRed) = tbred2!Nomeidx
            Else
               nomeFieldRed(indRed) = ""
               m_dllFunctions.WriteLog "Virtual Field not found: " & tbred!IdField, "Routines Generation"
            End If
           'SP redefines
          Else
            m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
          End If
          tbred.MoveNext
        Wend
        tbred.Close
      End If
      indSav = indRed
      indRed = 1
    Else
      flagRed = False
      testoRed(1) = ""
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    '''''''''''''''''''''''''''''''''''
    ' DECLARE CURSORE
    '''''''''''''''''''''''''''''''''''
    For K = 1 To indRed '???????????????? pu� essere maggiore di 1???????????????
      indentRed = 0
      If Len(testoRed(K)) Then  'PU� ENTRARE QUI???
        'CONDIZIONE PER TABELLE RIDEFINITE
        testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      indentGen = indentGNsq + indentRed
      
      testo = testo & Space(11 + indentGNsq + indentRed) & _
        "EXEC SQL DECLARE " & CursorName & Format(K, "00") & " CURSOR " & IIf(withold, "WITH HOLD", "") & vbCrLf & _
        Space(11 + indentGNsq + indentRed) & " FOR SELECT " & vbCrLf
      indPart = Len(nomeTabIndPart) > 0 'SQ - passarlo come parametro!
      testo = testo & CostruisciSelect("T" & indEnd, idRed(K))
      indPart = False
             
      ''Dim idSave As Double
      Dim tabSave As String, idxSave As String, idxSave2 As String, opSave As String
      'in realt� potrei farlo sempre, in quanto nel vettore, in caso senza redefines
      'c'� comunque lo stesso segmento/tabella
      If K > 1 Or flagRed Then
        idSave = instrLevel.idTable
        tabSave = instrLevel.Tavola
        instrLevel.Tavola = nomeTRed(K)
        instrLevel.idTable = idRed(K)
      End If
      'SP 12/9: altro gioco di prestigio per gli indici particolari...
      'tutto da mettere meglio nella versione definitiva
      If Len(nomeTabIndPart) Then
        idSave = instrLevel.idTable
        tabSave = instrLevel.Tavola
        indEnd = indEnd + 1
        ReDim Preserve TabIstr.BlkIstr(indEnd)
        TabIstr.BlkIstr(indEnd).idTable = idTabIndPart
        TabIstr.BlkIstr(indEnd).Tavola = nomeTabIndPart
        '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
        testo = testo & CostruisciSelect("T" & indEnd, idTabIndPart)
      End If
      
      testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom(1, indEnd, True)
     
      If Len(nomeTabIndPart) Then
         TabIstr.BlkIstr(indEnd - 1).idTable = idTabIndPart
         TabIstr.BlkIstr(indEnd - 1).Tavola = nomeTabIndPart
         TabIstr.BlkIstr(indEnd).idTable = idSave
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         'proviamo con la squalificata
         ReDim Preserve TabIstr.BlkIstr(indEnd).KeyDef(1)
         indPart = True
      Else
         indPart = False
      End If
      
      testo = testo & CostruisciWhere(1, indEnd)
      
      If Len(nomeTabIndPart) Then
         'rimette a posto
         indEnd = indEnd - 1
         ReDim Preserve TabIstr.BlkIstr(indEnd)
         TabIstr.BlkIstr(indEnd).idTable = idSave
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         indPart = False
      End If
      If K > 1 Or flagRed Then
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         TabIstr.BlkIstr(indEnd).idTable = idSave
      End If
    
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
    
      If Len(testoRed(K)) Then
        testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next K
    
    'diff
    '''''''''''''''''''''''''''''''''''
    ' SECONDA DECLARE
    '''''''''''''''''''''''''''''''''''
    If Not keyOp Then
      testo = testo & Space(11 + indentGN) & "ELSE" & vbCrLf
      For K = 1 To indRed
        indentRed = 0
        If Len(testoRed(K)) Then
          testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
          indentRed = 3
        End If
        '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
        indentGen = indentGNsq + indentRed
        
        testo = testo & Space(11 + indentGNsq + indentRed) _
                     & "EXEC SQL DECLARE " & CursorName & Format(K, "00") & IIf(suffDynT = "", "GU ", "") & " CURSOR " _
                     & IIf(withold, "WITH HOLD", "") & vbCrLf & Space(11 + indentGNsq + indentRed) & " FOR SELECT " & vbCrLf
     
        indPart = Len(nomeTabIndPart) > 0
        'SP 9/9 pcb dinamico
        testo = testo & CostruisciSelect("T" & indEnd, idRed(K))
        indPart = False
         
        If K > 1 Then
          idSave = TabIstr.BlkIstr(indEnd).idTable
          tabSave = TabIstr.BlkIstr(indEnd).Tavola
          TabIstr.BlkIstr(indEnd).Tavola = nomeTRed(K)
          TabIstr.BlkIstr(indEnd).idTable = idRed(K)
        End If
        'SP 12/9: altro gioco di prestigio per gli indici particolari...  tutto da mettere meglio nella versione definitiva
        If nomeTabIndPart <> "" Then
             idSave = TabIstr.BlkIstr(indEnd).idTable
             tabSave = TabIstr.BlkIstr(indEnd).Tavola
             indEnd = indEnd + 1
             ReDim Preserve TabIstr.BlkIstr(indEnd)
             TabIstr.BlkIstr(indEnd).idTable = idTabIndPart
             TabIstr.BlkIstr(indEnd).Tavola = nomeTabIndPart
            '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
             testo = testo & CostruisciSelect("T" & indEnd, idTabIndPart)
        End If
        testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom(1, indEnd, True)
        'SP 12/9: altro gioco di prestigio per gli indici particolari...
        indPart = False
        If nomeTabIndPart <> "" Then
             TabIstr.BlkIstr(indEnd - 1).idTable = idTabIndPart
             TabIstr.BlkIstr(indEnd - 1).Tavola = nomeTabIndPart
             TabIstr.BlkIstr(indEnd).idTable = idSave
             TabIstr.BlkIstr(indEnd).Tavola = tabSave
             'proviamo con la squalificata
             ReDim Preserve TabIstr.BlkIstr(indEnd).KeyDef(1)
             indPart = True
        End If
        testo = testo & CostruisciWhere(1, indEnd, True)
        If nomeTabIndPart <> "" Then
             'rimette a posto
             indEnd = indEnd - 1
             ReDim Preserve TabIstr.BlkIstr(indEnd)
             TabIstr.BlkIstr(indEnd).idTable = idSave
             TabIstr.BlkIstr(indEnd).Tavola = tabSave
             indPart = False
        End If
        If K > 1 Then
           TabIstr.BlkIstr(indEnd).Tavola = tabSave
           TabIstr.BlkIstr(indEnd).idTable = idSave
        End If
      
        testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        testo = testo & Space(11 + indentGNsq + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
      
        If Len(testoRed(K)) Then
          testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
        End If
      Next
      testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf & vbCrLf
      testo = testo & Space(11 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
    Else
       testo = testo & vbCrLf
    End If
    '...diff
     
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
         testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
         indentRed = 3
      End If
      testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If Len(testoRed(K)) Then
         testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next
    
    'SQ? non � qui?!
    'testo = testo & Space(11 + indentGN) & "IF SQLCODE = -502" & vbCrLf
    
    'diff
    If keyOp Then
       'SQ? ...MA qui?!
      testo = testo & Space(11 + indentGN) & "IF SQLCODE = -502" & vbCrLf
    Else
      testo = testo & Space(11 + indentGN) & "ELSE" & vbCrLf
      For K = 1 To indRed
        indentRed = 0
        If Len(testoRed(K)) Then
          testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
          indentRed = 3
        End If
        testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & "GU" & vbCrLf
        testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        If Len(testoRed(K)) Then
          testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
        End If
      Next
      testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
      'SQ? ...MA qui?!
      testo = testo & Space(11 + indentGN) & "IF SQLCODE = -502" & vbCrLf
      testo = testo & Space(14 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
    End If
        
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
        testo = testo & Space(14 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If Len(testoRed(K)) Then
        testo = testo & Space(14 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next

    'diff
    If Not keyOp Then
      testo = testo & Space(14 + indentGN) & "ELSE" & vbCrLf
      For K = 1 To indRed
        indentRed = 0
        If Len(testoRed(K)) Then
           testo = testo & Space(14 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
           indentRed = 3
        End If
        testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & "GU" & vbCrLf
        testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & "GU" & vbCrLf
        testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        If Len(testoRed(K)) Then
           testo = testo & Space(14 + indentGNsq) & "END-IF" & vbCrLf
        End If
      Next
      testo = testo & Space(14 + indentGN) & "END-IF" & vbCrLf
    End If
    
    
    testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
    testo = testo & Space(11 + indentGN) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf & vbCrLf & vbCrLf
   
    'diff
    If Not keySecFlagUltimoLiv And Not keyPrimUguale Then
      testo = testo & Space(14) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
      testo = testo & Space(14) & "MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
      'SQ 31-07-06 - C.C. "F"
      If InStr(instrLevel.CodCom, "F") Then
        'Sistemare INDENT!
      Else
        testo = testo & Space(11) & "END-IF" & vbCrLf
      End If
    Else
      'SQ D
      If level = UBound(TabIstr.BlkIstr) Then
        'fetch in pi�, valida su chiavi secondarie!!
        'SP 11/9: e su primarie per uguale
        'SP 14/9: problema su GHU+REPL+GHN+REPL
        testo = testo & Space(14) & "IF WK-SQLCODE = ZERO" & vbCrLf
        testo = testo & Space(14) & "   IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) = 'GU' OR 'ISRT'" & vbCrLf
        testo = testo & Space(14) & "                           OR 'GN' OR 'REPL' OR 'DLET'" & vbCrLf
        'testo = testo & Space(17) & "AND LNKDB-STKSSA(LNKDB-NUMSTACK) NOT < " & vbCrLf
        'testo = testo & Space(17) & "    LNKDB-KEYVALUE(1, 1)" & vbCrLf
        testo = testo & Space(6) & "***" & " FETCH TO EXCLUDE FIRST RECORD, USED IN LAST GU" & vbCrLf
        'SQ D
        'testo = testo & Space(20) & "PERFORM 9999-FETCH-" & CursorName & "" & vbCrLf
        If EmbeddedRoutine Then
          testo = testo & Space(20) & "PERFORM FETCH-" & CursorName & vbCrLf & _
                          Space(20) & "   THRU " & CursorName & "-EX" & vbCrLf
        Else
          testo = testo & Space(20) & "PERFORM 9999-FETCH-" & CursorName & vbCrLf
        End If
        testo = testo & Space(20) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
        testo = testo & Space(20) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
        testo = testo & Space(20) & "   MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        'testo = testo & Space(23) & "               LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
        For K = 1 To indRed
          indentRed = 0
          If testoRed(K) <> "" Then
            testo = testo & Space(23) & Trim(testoRed(K)) & vbCrLf
            indentRed = 3
          End If
          testo = testo & Space(20 + indentRed) & "   EXEC SQL CLOSE " & CursorName & Format(K, "00") & IIf(keyOp, "", "GU") & vbCrLf
          testo = testo & Space(20 + indentRed) & "   END-EXEC" & vbCrLf
          If testoRed(K) <> "" Then
            testo = testo & Space(23) & "END-IF" & vbCrLf
          End If
        Next
        testo = testo & Space(14) & "      END-IF" & vbCrLf
        testo = testo & Space(14) & "   END-IF" & vbCrLf
        testo = testo & Space(14) & "END-IF" & vbCrLf
      End If  'D
      
'''      'testo = testo & Space(17) & "MOVE LNKDB- KEYVALUE(" & indEnd & ", 1) TO LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
'''      testo = testo & _
'''        Space(14) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
'''        Space(14) & "   MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf & _
'''        Space(14) & "   MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf & _
'''        Space(14) & "END-IF" & vbCrLf
      'SQ 31-07-06 - C.C. "F"
      If InStr(instrLevel.CodCom, "F") Then
        'Sistemare INDENT!
      Else
        testo = testo & Space(11) & "END-IF" & vbCrLf
      End If
    End If
    
    
    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    If EmbeddedRoutine Then
      testo = testo & Space(11) & "   PERFORM FETCH-" & CursorName & vbCrLf & _
                      Space(11) & "      THRU " & CursorName & "-EX" & vbCrLf
    Else
      testo = testo & Space(11) & "   PERFORM 9999-FETCH-" & CursorName & vbCrLf
    End If
    testo = testo & Space(11) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf  'SQ - 29-08
    testo = testo & Space(11) & "END-IF" & vbCrLf & vbCrLf
        
    'diff
    testo = testo & Space(11) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf & _
                    Space(11) & "   MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
    If Not keyOp Then
      testo = testo & Space(14) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
    End If
    
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
        testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If testoRed(K) <> "" Then
         testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next
    'diff
    If Not keyOp Then
      testo = testo & Space(14) & "ELSE" & vbCrLf
      For K = 1 To indRed
         If testoRed(K) <> "" Then
            testo = testo & Space(17) & Trim(testoRed(K)) & vbCrLf
            indentRed = 3
         End If
         testo = testo & Space(17 + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & "GU" & vbCrLf
         testo = testo & Space(17 + indentRed) & "END-EXEC" & vbCrLf
         If testoRed(K) <> "" Then
            testo = testo & Space(17) & "END-IF" & vbCrLf
         End If
      Next
      testo = testo & Space(14) & "END-IF" & vbCrLf
    End If
    testo = testo & Space(11) & "END-IF" & vbCrLf
       
    
    testo = testo & vbCrLf
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' TESTO FETCH:
    ' Viene scritto in fondo alla routine, in getRoutine_DB2
    ' Se ho pi� livelli, appendo il testo alle fecth dei precedenti
    ' SQ - si pu� isolare?
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'TMP
    GbTestoFetch = GbTestoFetch & getFetch(indRed, indEnd, indentGN, indentGNqu, Not keyOp)
    
    ''''''''''''''''''''''''''''''
    ' OCCURS
    ''''''''''''''''''''''''''''''
    If Not flagIns Then
      isOccurs = createOccursRoutine(instrLevel, tipoIstr, flagOcc)
    End If
     
    'loop sulle tabelle ridefinite: nome tabella gi� mosso da template in copy move
    'SP 12/9: per ora non gestisce gli indici particolari
    ''''''''''tabFound = False
    If flagRed Then
      'REDEFINES: CHIAMATA ROUTINE
      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      testo = testo & Space(11) & "   MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-TO-RD" & vbCrLf
      testo = testo & Space(11) & "END-IF" & vbCrLf
          
      createRedefineRoutine instrLevel, tipoIstr, flagOcc, indSav, flagIns
    End If
    
    'esce se richiamata dalla insert: ATTENZIONE: PROBABILMENTE E' UN "NEXT FOR"!!!!!
    If flagIns Then
      getGN_DB2 = testo
      Exit Function '!!!!!!!!
    End If
     
    NomeT = TabIstr.BlkIstr(indEnd).Tavola
    NomePk = TabIstr.BlkIstr(indEnd).pKey
    If TabIstr.BlkIstr(indEnd).IdSegm Then
        Dim indGet As Long
      indGet = indEnd
      '?wLiv01Copy = getCopyDLISeg(indGet)
      
      If Not flagRed Then
        For K = 1 To indRed
          If testoRed(K) <> "" Then
             testo = testo & Space(11) & Trim(testoRed(K)) & vbCrLf
             indentRed = 3
          End If
          testo = testo & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
          testo = testo & Space(11 + indentRed) & "   PERFORM 9999-" & Replace(nomeTRed(K), "_", "-") & "-TO-RD" & vbCrLf
          testo = testo & Space(11 + indentRed) & "   MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE " & vbCrLf
          testo = testo & Space(11 + indentRed) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf
          'la chiave deve essere salvata anche se non c'era il blocco chiavi;
          'ad esempio per una GN totalmente squalificata, seguita
          'da una GNP, deve essere comunque recuperata la chiave letta con la GN
          'SP 5/9 keyfeedback; banca lo mettiamo in seguito
          'SP 12/9: indici particolari
          If Len(nomeTabIndPart) Then
              testo = testo & Space(6) & "*" & Space(7 + indentRed) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          Else
              testo = testo & Space(6) & "*" & Space(7 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'SQ tmp: ci vuole? (commentato)
          'testo = testo & Space(14 + indentRed) & "PERFORM SETK-WKEYWKUT THRU SETK-WKEYWKUT-EX" & vbCrLf
          'testo = testo & Space(6) & "*" & Space(4 + indentRed) & "PERFORM SETK-WKEYWKUT THRU SETK-WKEYWKUT-EX" & vbCrLf
          testo = testo & Space(6) & "*" & Space(7 + indentRed) & "PERFORM 9999-SETK-WKEYWKUT" & vbCrLf
          If Len(nomeTabIndPart) Then
              testo = testo & Space(14 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'SQ D
          testo = testo & Space(14 + indentRed) & "MOVE '" & Format(level, "00") & "' TO LNKDB-PCBSEGLV" & vbCrLf
          testo = testo & Space(11 + indentRed) & "END-IF" & vbCrLf
          If testoRed(K) <> "" Then
             testo = testo & Space(11) & "END-IF" & vbCrLf
          End If
        Next
      End If
      
      testo = testo & vbCrLf
      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      testo = testo & Space(11) & "   MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf
      If keySecFlagUltimoLiv Then
        testo = testo & Space(11) & "   PERFORM 9999-SETA-" & Replace(TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
        testo = testo & Space(11) & "   MOVE KRR-" & TabIstr.BlkIstr(indEnd).KeyDef(1).key & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space(11) & "   MOVE KRDP-" & TabIstr.BlkIstr(indEnd).KeyDef(1).key & "  TO LNKDB-FDBKEY" & vbCrLf
      End If
      testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
     
    'SP 9/9 pcb dinamico: salvataggio SSA formattata a modo nostro!
    ' per ora solo sulla GU, se non ok sar� fatta solo per qualificate e no pcb dinamico
    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    
    'diff
    'testo = testo & Space(11) & "   MOVE LNKDB-KEYNAME(" & indEnd & ", 1) TO WK-KEYNAME" & vbCrLf
    'testo = testo & Space(11) & "   MOVE LNKDB-KEYOPER(" & indEnd & ", 1) TO WK-KEYOPER" & vbCrLf
    'testo = testo & Space(11) & "   MOVE LNKDB-KEYVALUE(" & indEnd & ", 1) TO WK-KEYVALUE" & vbCrLf
    'testo = testo & Space(11) & "   MOVE WK-SSA TO LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
     
    If flagOcc Or isOccurs Then
      'OCCURS:
      testo = testo & vbCrLf
      testo = testo & Space(6) & "*** OCCURS" & vbCrLf
      testo = testo & Space(14) & "MOVE " & indEnd & " TO WK-IND2" & vbCrLf
      testo = testo & Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      'TMP
      testoTmp = Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      testo = testo & vbCrLf
    End If
    
    If flagRed Then
      'REDEFINES:
      testo = testo & vbCrLf
      testo = testo & Space(6) & "*** REDEFINES" & vbCrLf
      testo = testo & Space(14) & "MOVE " & indEnd & " TO WK-IND2" & vbCrLf
      testo = testo & Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      'TMP       testoTmp = Space(14) & "PERFORM 9999-" & Replace(instrLevel.Tavola, "_", "-") & "-SELT" & vbCrLf
      testo = testo & Space(14) & "MOVE WK-DATA-AREA TO RD-" & instrLevel.Segment & vbCrLf
      
      'SQ D
      'Tmp: ok, per ora, ripeterla per ogni livello...
      'If level = UBound(TabIstr.BlkIstr) Then
        If InStr(TabIstr.BlkIstr(1).CodCom, "D") And indEnd > 1 Then
          'STRING:
          testo = testo & Space(14) & "STRING  RD-" & TabIstr.BlkIstr(1).Segment & vbCrLf
          For i = 2 To indEnd
            testo = testo & Space(14) & "        RD-" & TabIstr.BlkIstr(i).Segment & vbCrLf
          Next
          testo = testo & Space(14) & "  DELIMITED BY SIZE" & vbCrLf
          testo = testo & Space(14) & "  INTO LNKDB-DATA-AREA(" & indEnd & ")" & vbCrLf
        Else
          'MOVE:
          testo = testo & Space(14) & "MOVE RD-" & instrLevel.Segment & " TO LNKDB-DATA-AREA(" & indEnd & ")" & vbCrLf
        End If
      'End If
    Else
      'SQ 31-07-06 - C.C. "D"
      'SQ D
      'Tmp: ok, per ora, ripeterla per ogni livello...
      'If level = UBound(TabIstr.BlkIstr) Then
        If InStr(TabIstr.BlkIstr(1).CodCom, "D") Then
          'Ho letto le io-aree di tutti i livelli: tutte nella LNKDB-DATA-AREA
          testo = testo & Space(14) & "STRING  RD-" & TabIstr.BlkIstr(1).Segment & vbCrLf
          For i = 2 To indEnd
            testo = testo & Space(14) & "        RD-" & TabIstr.BlkIstr(i).Segment & vbCrLf
          Next
          testo = testo & Space(14) & "  DELIMITED BY SIZE" & vbCrLf
          testo = testo & Space(14) & "  INTO LNKDB-DATA-AREA(" & UBound(TabIstr.BlkIstr) & ")" & vbCrLf
        Else
          testo = testo & Space(14) & "MOVE RD-" & instrLevel.Segment & " TO LNKDB-DATA-AREA(" & UBound(TabIstr.BlkIstr) & ")" & vbCrLf
        End If
      'End If
    End If
    testo = testo & Space(11) & "END-IF" & vbCrLf
    
    'SQ D
    If InStr(TabIstr.BlkIstr(1).CodCom, "D") And level > 1 Then
      'Lettura solo se il precedente livello andato a buon fine
      testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
    testo = testo & vbCrLf
  
    '''''''''''''''''''''''
    ' Scrittura Copy P
    '''''''''''''''''''''''
    If Len(GbTestoRedOcc) Then
      Dim rtbFileSave As String
      rtbFileSave = rtbFile.text  'salvo
      rtbFile.text = GbTestoRedOcc
      rtbFile.SaveFile VSAM2Rdbms.drPathDb & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\cpy\" & includedMember, 1
      rtbFile.text = rtbFileSave
      GbTestoRedOcc = ""  'flag x chiamante
      'transitorio: buttare
      GbFileRedOcc = ""
    End If
    If Len(includedMember) Then 'Verificare... se serve utilizzare Collection...
      GbFileRedOccs(UBound(GbFileRedOccs)) = includedMember
      ReDim Preserve GbFileRedOccs(UBound(GbFileRedOccs) + 1)
    End If
  Next level
  'TMP
  'getGN_DB2 = testoTmp
  getGN_DB2 = testo

End Function
Public Function getGN_DB2_TOTSQ(NomeSegm As String, jj As Integer) As String
  Dim K As Long
  Dim i As Integer, k1 As Integer, indEnd As Integer, indSav As Integer, indentGNsqKeyOp As Integer
  Dim NomeT As String, NomePk As String, nomeKey As String, testo As String
  Dim TestoFrom As String, TestoWhere As String
  Dim keySecFlagPrimoLiv As Boolean, keySecFlagUltimoLiv As Boolean
  Dim flagSel As Boolean, flagIns As Boolean, flagRed As Boolean
  Dim flagOcc As Boolean, isOccurs As Boolean
  Dim tb1 As Recordset, tb2 As Recordset, tbred As Recordset, rsIndPart As Recordset, tb As Recordset
  Dim indentGN As Integer, indentGNsq As Integer, indentGNqu As Integer
  Dim keyPrim As String, keySec As String
  Dim indRed As Integer
  Dim keySave() As BlkIstrKey
  Dim istrSave As IstrDli
  Dim instrLevel As BlkIstrDli
  Dim nomeTab As String
  'SQ
  Dim testoTmp As String
  'diff
  Dim qualified As Boolean, keyOp As Boolean, keyPrimUguale As Boolean
  
  Dim tipoIstr As String
  Dim withold As Boolean
  
  withold = False
  
  indentGen = 0
  indentRed = 0
  
  testo = vbCrLf & "      *------------------------------------------------------------*" & vbCrLf & _
                   "       9999-ROUT-" & Gistruzione & "-" & NomeSegm & "." & vbCrLf

  tipoIstr = "GN"
  
  If InStr(TabIstr.BlkIstr(jj).CodCom, "D") And tipoIstr <> "ISRT" Then
    level = 1
  Else
    level = UBound(TabIstr.BlkIstr)
  End If
  
  testo = testo & Space(11) & "MOVE 'N' TO WK-GNQUAL" & vbCrLf
  testo = testo & Space(11) & "MOVE '" & tipoIstr & "' TO WK-ISTR" & vbCrLf
  
  
  For level = level To UBound(TabIstr.BlkIstr)
    '''''''''''''
    'init:
    '''''''''''''
    'tmp: (ora � ciclico!... togliere quelli che non servono!)
    'keySecFlagPrimoLivIf UBound(instrLevel.KeyDef) = False
    keySecFlagUltimoLiv = False
    flagOcc = False
    ''''flagIns = False
    '''''''''tabFound = False
    isOccurs = False
    'tabFound3 = False
    includedMember = ""
        
    If tipoIstr <> "ISRT" Then  'SE RIMARRANNO SEPARATE: BUTTARE!
      'SQ D
      indEnd = level
      instrLevel = TabIstr.BlkIstr(indEnd)
      'SQ D
      'testo = InitIstr(tipoIstr, "N")
      
      If InStr(TabIstr.BlkIstr(jj).CodCom, "D") And level > 1 Then
        'Lettura solo se il precedente livello andato a buon fine
        testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      End If
      
      If UBound(TabIstr.BlkIstr) Then
        testo = testo & Space(11) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
        testo = testo & Space(11) & "MOVE '" & instrLevel.Segment & "' TO WK-SEGMENT" & vbCrLf
      End If
      testo = testo & Space(11) & "PERFORM 9000-MANAGE-PCB" & vbCrLf
      flagIns = False
    Else
      'SQ D
      indEnd = UBound(TabIstr.BlkIstr) - 1
      instrLevel = TabIstr.BlkIstr(indEnd)
      flagIns = True
    End If
    
    If Not indEnd > 0 Then  'SQ - portare fuori
      m_dllFunctions.WriteLog "Instruction: " & GbNumIstr & " - not converted", "Routines Generation"
      Exit Function
    End If
        
        
    'diff
    If UBound(instrLevel.KeyDef) Then
      'nuovo sistema: si intende squalificata quando lo � sull'ultimo livello
      qualified = True 'portare dentro la IF
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        keySecFlagUltimoLiv = True
        'SQ - 21-07-06
        'TAG='AUTOMATIC' - IN OGNI CASO NON POSSO CERCARE PER NOME...
        ' => Aggiunto filtro DBD (VERIFICARE)!!!!!!!!!!
'''MG 240107
'''''        Set rsIndPart = m_dllFunctions.Open_Recordset( _
'''''          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, DMDB2_Tabelle as b " & _
'''''          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
'''''          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")
        Set rsIndPart = m_dllFunctions.Open_Recordset( _
          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & DB & "Tabelle as b " & _
          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")

        If Not rsIndPart.EOF Then
          If rsIndPart!nomeTab <> instrLevel.Tavola Then
             nomeTabIndPart = rsIndPart!nomeTab
             idTabIndPart = rsIndPart!idTab
          End If

       End If
        'sulla secondaria, solo con l'operatore per maggiore deve creare la doppia declare (cio� l'esatto contrario della primaria)
        'SP 10/9: modificato, dovrebbe valere anche per l'operatore per =, cos� come fa per la primaria (sulla primaria, se operatore = "=" lo fa diventare ">",
        'per scartare il primo record, sulla secondaria, vista la fetch a vuoto, deve trasformarlo in ">="
        'SP 11/9 : RIMODIFICATO, ERA SBAGLIATA LA PRIMARIA, QUINDI SI FA SOLO PER >!!! (quelli per <, <= li vedremo poi, ma non dovrebbero dare grossi problemi)
        'LA DIFFERENZA TRA PRIMARIA E SECONDARIA E' DOVUTA AL FATTO CHE LA SECONDARIA PUO' AVERE DUPLICATI; COMUNQUE IN FUTURO ATTUEREMO IL SISTEMA DELLA FETCH A VUOTO, CHE ANDREBBE BENE
        'ANCHE PER LA PRIMARIA; PER ORA SI LASCIA COSI' PERCHE' LA PRIMARIA FUNZIONA!!! ANZI, ANCHE LA PRIMARIA, PER =, HA BISOGNO DELLA FETCH A VUOTO, CHE POI SERVE SOLO A
        'DARE "GE" (WHERE PER = SU UNIVOCA, CON SCARTO DEL PRIMO RECORD ==> SQLCODE = 100 PER FORZA!
        keyOp = instrLevel.KeyDef(1).Op <> ">"
      Else
        keySecFlagUltimoLiv = False
        'operatore gi� per maggiore, non deve creare la doppia declare
        'SP 11/9 : ANCHE PER UGUALE NON DEVE CREARE LA DOPPIA DECLARE, LO DEVE FARE SOLO PER >=!!! (quelli per <, <= li vedremo poi, ma non dovrebbero dare grossi problemi)
        keyOp = instrLevel.KeyDef(1).Op <> ">="
        'SP 11/9 Flag per forzare fetch a vuoto anche per primaria!!!
        keyPrimUguale = instrLevel.KeyDef(1).Op = "="
      End If
    Else
      qualified = False
    End If
' rob inserita
'''   qualified = False
    
    'diff
    indentGN = 3
    If keyOp Then
       indentGNsq = 3
       indentGNqu = 0
    Else
       indentGNsq = 6
       indentGNqu = 3
    End If
    indentGNsqKeyOp = 6
    indentRed = 0
      
    testo = testo & Space(11) & "INITIALIZE RD-" & instrLevel.Segment & vbCrLf
    testo = testo & Space(11) & "MOVE LNKDB-SEGLEVEL(" & indEnd & ") TO LNKDB-STKLEV(LNKDB-NUMSTACK)" & vbCrLf & vbCrLf
    
    'diff
    If qualified Then
'''      'SQ 31-07-06 - C.C. "F"
'''      If InStr(instrLevel.CodCom, "F") Then
'''        'Sistemare INDENT!
'''      Else
        testo = testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES " & vbCrLf
        testo = testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GbNumIstr & vbCrLf
'''      End If

      'SP 6/9: avanti con la doppiadeclare, tranne sulle secondarie, che fanno solo la IF per
      'reimpostare, cos� come l'operatore; MODIFICATO: DOPPIADECLARE ANCHE PER LE SECONDARIE
      'If Not keySecFlagUltimoLiv Then
      'SP 14/9 non funziona con un loop di GHU+REPL+GHN+REPL, ecc., perch� l'ultima
      'istruzione non era una GU, ma doveva considerarla cos�.
       testo = testo & Space(14) & "IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) NOT = 'GU' AND 'ISRT'" & vbCrLf
       testo = testo & Space(14) & "                          AND 'GN' AND 'REPL' AND 'DLET'" & vbCrLf
       If Not keyOp Then
         testo = testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
       End If
    Else
      'SQ 31-07-06 - C.C. "F"
'''      If InStr(instrLevel.CodCom, "F") Then
'''        'Sistemare INDENT!
'''      Else
       testo = testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
       testo = testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GbNumIstr & vbCrLf
       
       
'''      End If
      'SP 14/9 problema su GHU+REPL+GHN+REPL
      testo = testo & Space(11) & "   IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) NOT = 'GU' AND 'ISRT'" & vbCrLf
      testo = testo & Space(11) & "                             AND 'GN' AND 'REPL' AND 'DLET'" & vbCrLf
      testo = testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
    End If
    
    
    'SQ - sto pezzo probabilmente � da tenere fuori ciclo... giusto?
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Segmento "parent":
    'stefanopippo: 15/08/2005: se su chiave secondaria (sempre sul primo livello!) non fa restore (� inutile)
    keySecFlagPrimoLiv = False
    If UBound(TabIstr.BlkIstr(jj).KeyDef) Then
      keySecFlagPrimoLiv = TabIstr.BlkIstr(jj).KeyDef(1).key <> TabIstr.BlkIstr(jj).pKey
    End If
    If Not keySecFlagPrimoLiv Then
       NomeT = getParentTable(CLng(TabIstr.BlkIstr(jj).IdSegm))  'togliere cast e cambiare function
       If Len(NomeT) Then
          'SEGMENTO NON RADICE: RESTORE
          testo = testo & Space(11 + indentGNsq) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
          testo = testo & Space(11 + indentGNsq) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
       End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If UBound(instrLevel.KeyDef) Then
      'nuovo sistema: si intende squalificata quando lo � sull'ultimo livello
      If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
        keySecFlagUltimoLiv = True
'''MG 240107
'''''        Set rsIndPart = m_dllFunctions.Open_Recordset( _
'''''          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, DMDB2_Tabelle as b " & _
'''''          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
'''''          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")
        Set rsIndPart = m_dllFunctions.Open_Recordset( _
          "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & DB & "Tabelle as b " & _
          "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
          " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")

        If Not rsIndPart.EOF Then
          If rsIndPart!nomeTab <> instrLevel.Tavola Then
             nomeTabIndPart = rsIndPart!nomeTab
             idTabIndPart = rsIndPart!idTab
          End If
        End If
      Else
        keySecFlagUltimoLiv = False
      End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' RESTORE CHIAVE:
    ' Attenzione: per ottimizzare posso appoggiare il nome della tabella in TabIstr.BlkIstr(0)! (valutare)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    For K = 1 To indEnd
      ''''''''''''''''''''''''''''Kt = k  '??????????????????????????????????????????????????
      NomeT = TabIstr.BlkIstr(K).Tavola
      If Trim(NomeT) = "" Then
        NomeT = "[NO-TABLE-FOR:" & TabIstr.BlkIstr(K).Segment & "]"
      End If
      NomePk = TabIstr.BlkIstr(K).pKey
      testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
      'squalificata solo sull'ultimo livello, imposta la chiave primaria ereditata
      ' Rob
      BlkFin = 1
      If UBound(TabIstr.BlkIstr(K).KeyDef) = 0 And K = BlkFin Then
        testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
        testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO K01-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
      End If
      For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
        If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
          nomeKey = Replace(TabIstr.BlkIstr(K).KeyDef(k1).key, "-", "_")
          'SQ
          'NomePk = TabIstr.BlkIstr(k).KeyDef(k1).NameAlt
          NomePk = nomeKey
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(K)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
          testo = testo & Space(16 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf
          'SP 12/9: indici particolari
          If Len(nomeTabIndPart) And K = indEnd Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
          testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
          If nomeTabIndPart <> "" And K = indEnd Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
          End If
          If TabIstr.BlkIstr(K).pKey = TabIstr.BlkIstr(K).KeyDef(k1).key Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
          End If
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K" & Format(TabIstr.BlkIstr(K).KeyDef(k1).KeyNum, "00") & "-" & NomePk & vbCrLf
        End If
      Next k1
    Next K
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    'diff
    testo = testo & Space(14) & "ELSE" & vbCrLf
    If Not keyOp Then
      testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE 'O' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
    End If
    'da impostare diversamente in funzione del tipo lettura (qual/squal, operatore, ecc.)
    'per ora usa la primaria (vedi commentone sopra): CAMBIATO: VALE PER TUTTE LE PRIMARIE
    ' MA NON PER LE SECONDARIE, che fanno la fetch a vuoto
    'MODIFICATO ANCORA: ANCHE PER LE SECONDARIE, CHE FANNO COMUNQUE POI LA FETCH A VUOTO
    testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
    If Not keySecFlagUltimoLiv Then
       'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM REST-FDBKEY THRU REST-FDBKEY-EX" & vbCrLf
       testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
    Else
      NomePk = instrLevel.KeyDef(1).key
      testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-STKPTGKEY(LNKDB-NUMSTACK) TO " & vbCrLf
      testo = testo & Space(16 + indentGNsqKeyOp) & "KRR-" & NomePk & vbCrLf
    End If
    'SQ 7-07-06 - Controllare... non credo vada bene!
    testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K01-" & NomePk & vbCrLf
    'SP 9/9 pcb dinamico
    Dim j As Integer
    For k1 = 2 To UBound(instrLevel.KeyDef)
      j = k1

      If Trim(instrLevel.KeyDef(k1).key) <> "" Then
       nomeKey = instrLevel.KeyDef(k1).key
       NomePk = instrLevel.KeyDef(k1).NameAlt
       testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(indEnd)) & ", " & Format(j, "#0") & ") TO " & vbCrLf
       'SQ
       'testo = testo & Space(13 + indentGNsqKeyOp) & "KRD-" & instrLevel.KeyDef(k1).NameAlt & vbCrLf
       testo = testo & Space(13 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf

      'SP 12/9: indici particolari
       If nomeTabIndPart <> "" Then
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
       End If
       'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
       testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
       If nomeTabIndPart <> "" Then
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
       End If
       'SP 16/9: nella booleana � meglio che non ci sia!!!
       'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETC-WKEYWKEY THRU SETC-WKEYWKEY-EX " & vbCrLf
       ''SQ 7-07-06
       'testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K" & Format(instrLevel.KeyDef(k1).KeyNum, "00") & "-" & NomePk & vbCrLf
       testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & nomeKey & " TO K" & Format(instrLevel.KeyDef(k1).KeyNum, "00") & "-" & nomeKey & vbCrLf
      End If
    Next k1

    testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf

    If Not keyOp Then
      testo = testo & Space(11 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
    End If
    '...diff
    
    
    'UTILIZZO DI ARRAY ANCHE PER CASI "NORMALI"
    indRed = 1
    ReDim Preserve testoRed(1)
    ReDim Preserve nomeTRed(1)
    ReDim Preserve nomeSRed(1)
    ReDim Preserve idRed(1)
    ReDim Preserve nomeKeyRed(1)
    ReDim Preserve nomeFieldRed(1)
    idRed(1) = instrLevel.idTable
    If UBound(instrLevel.KeyDef) Then
      nomeKeyRed(1) = instrLevel.KeyDef(1).key
    Else
      nomeKeyRed(1) = ""
    End If
    Dim tbred2 As Recordset
    Set tbred2 = m_dllFunctions.Open_Recordset( _
      "select nome as nomeidx from psdli_field where unique and idsegmento=" & instrLevel.IdSegm)
    If Not tbred2.EOF Then
      nomeFieldRed(1) = tbred2!Nomeidx
    Else
      m_dllFunctions.WriteLog "No Field found for segment: " & instrLevel.IdSegm, "Routines Generation"
      nomeFieldRed(1) = ""
    End If
    nomeTRed(1) = instrLevel.Tavola
    nomeSRed(1) = instrLevel.Segment
    
'''    'stefanopippo: redefines; per la SELECT IL PROBLEMA E' SOLO SULL'ULTIMO LIVELLO DELLA SELECT,
'''    'IN QUANTO NON ESISTE JOIN A MENO DI CHIAVE SECONDARIA, ED IN OGNI CASO LA SECONDARIA SAREBBE SULL'ULTIMA!
'''    ''''''''''''''''''''''''''''''''''''
'''    ' REDEFINES: PREPARAZIONE STRUTTURE
'''    ''''''''''''''''''''''''''''''''''''
'''    Set tbred = m_dllFunctions.Open_Recordset( _
'''      "select condizione, RDBMS_tbname from PsDli_Segmenti where idSegmento = " & instrLevel.IdSegm)
'''    If Len(tbred!condizione) Then
'''      testoRed(1) = getCondizione_Redefines(tbred!condizione & "_lett")
'''    Else
'''      testoRed(1) = ""
'''    End If
'''    tbred.Close
    
'''    Dim tbsel As Recordset
'''    Set tbsel = m_dllFunctions.Open_Recordset( _
'''      "select idtable, nome from dmdb2_tabelle where idorigine = " & instrLevel.IdSegm & " and TAG like '%DISPATCHER%'")
'''    If Not tbsel.EOF Then
'''      flagRed = True
'''      Set tbred = m_dllFunctions.Open_Recordset( _
'''        "select a.idsegmento, a.nome as nomeseg, b.nome as nometab, a.condizione, b.idtable as idTab, c.nome as nomeindex, c.idorigine as idfield " & _
'''        "from mgdli_segmenti as a, dmdb2_tabelle as b, dmdb2_index as c where a.idsegmentoorigine = " & instrLevel.IdSegm & _
'''        " and a.idsegmento = b.idorigine and b.TAG not like '%OCCURS%' and b.idtable = c.idtable and c.tipo = 'P' order by b.nome")
'''      If tbred.RecordCount Then
'''        ReDim Preserve testoRed(tbred.RecordCount + 1)
'''        ReDim Preserve nomeTRed(tbred.RecordCount + 1)
'''        ReDim Preserve nomeSRed(tbred.RecordCount + 1)
'''        ReDim Preserve idRed(tbred.RecordCount + 1)
'''        ReDim Preserve nomeKeyRed(tbred.RecordCount + 1)
'''        ReDim Preserve nomeFieldRed(tbred.RecordCount + 1)
'''        While Not tbred.EOF
'''          indRed = indRed + 1
'''          If Len(tbred!condizione) Then
'''            If flagRed Then 'SEMPRE VERO (qui)?!
'''
'''              testoRed(1) = ""  '????????????? (non posso farlo fuori ciclo?)
'''              idRed(1) = tbsel!idTable
'''              nomeTRed(1) = tbsel!nome
'''              nomeSRed(1) = "DISPATCHER"
'''
'''              If UBound(instrLevel.KeyDef) > 0 Then   '?? non l'ho gi� fatto sopra? (fuori ciclo?)
'''                nomeKeyRed(1) = instrLevel.KeyDef(1).key
'''                nomeFieldRed(1) = instrLevel.KeyDef(1).NameAlt
'''              Else
'''                'SQ - 26-07-06 - Pezza per non cambiare cose che non so!
'''                nomeKeyRed(1) = Replace(instrLevel.pKey, "_", "-")
'''                ' => vado a prendere il nome colonna corretto!
'''                Set tbred2 = m_dllFunctions.Open_Recordset( _
'''                   "Select nome from Dmdb2_colonne where idTable=" & instrLevel.idTable & _
'''                   " and idTable=idTableJoin and idCmp <> -1")
'''                If Not tbred2.EOF Then
'''                  nomeFieldRed(1) = Replace(tbred2!nome, "_", "-")
'''                Else
'''                  nomeFieldRed(1) = ""
'''                  m_dllFunctions.WriteLog "No 'field' column found for table: " & instrLevel.idTable, "Routines Generation"
'''                End If
'''                tbred2.Close
'''              End If
'''              testoRed(indRed) = "IF WK-NAMETABLE = '" & tbred!nomeTab & "'"
'''            Else
'''              testoRed(indRed) = getCondizione_Redefines(tbred!condizione & "_lett")
'''            End If
'''            idRed(indRed) = tbred!idTab
'''            nomeTRed(indRed) = tbred!nomeTab
'''            nomeSRed(indRed) = tbred!nomeseg
'''            nomeKeyRed(indRed) = tbred!nomeindex
'''            'stefano: campo per il nome del field
'''            Set tbred2 = m_dllFunctions.Open_Recordset("select nome as nomeidx " & _
'''              "from mgdli_field where idfield = " & tbred!IdField & " and idsegmento = " & tbred!idSegmento)
'''            If Not tbred2.EOF Then
'''               nomeFieldRed(indRed) = tbred2!Nomeidx
'''            Else
'''               nomeFieldRed(indRed) = ""
'''               m_dllFunctions.WriteLog "Virtual Field not found: " & tbred!IdField, "Routines Generation"
'''            End If
'''           'SP redefines
'''          Else
'''            m_dllFunctions.WriteLog "Virtual Segment: " & tbred!nomeseg & " - template missing", "Routines Generation"
'''          End If
'''          tbred.MoveNext
'''        Wend
'''        tbred.Close
'''      End If
'''      indSav = indRed
'''      indRed = 1
'''    Else
'''      flagRed = False
'''      testoRed(1) = ""
'''    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    '''''''''''''''''''''''''''''''''''
    ' DECLARE CURSORE
    '''''''''''''''''''''''''''''''''''
    For K = 1 To indRed '???????????????? pu� essere maggiore di 1???????????????
      indentRed = 0
      If Len(testoRed(K)) Then  'PU� ENTRARE QUI???
        'CONDIZIONE PER TABELLE RIDEFINITE
        testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      indentGen = indentGNsq + indentRed
      
      'rob
      Dim CursorNameTOTSQ As String
      CursorNameTOTSQ = CursorName() & NomeSegm
      
      testo = testo & Space(11 + indentGNsq + indentRed) & _
        "EXEC SQL DECLARE " & CursorNameTOTSQ & Format(K, "00") & " CURSOR " & IIf(withold, "WITH HOLD", "") & vbCrLf & _
        Space(11 + indentGNsq + indentRed) & " FOR SELECT " & vbCrLf
      indPart = Len(nomeTabIndPart) > 0 'SQ - passarlo come parametro!
      testo = testo & CostruisciSelect("T" & indEnd, idRed(K))
      indPart = False
             
      ''Dim idSave As Double
      Dim tabSave As String, idxSave As String, idxSave2 As String, opSave As String
      'in realt� potrei farlo sempre, in quanto nel vettore, in caso senza redefines
      'c'� comunque lo stesso segmento/tabella
      If K > 1 Or flagRed Then
        idSave = instrLevel.idTable
        tabSave = instrLevel.Tavola
        instrLevel.Tavola = nomeTRed(K)
        instrLevel.idTable = idRed(K)
      End If
      'SP 12/9: altro gioco di prestigio per gli indici particolari...
      'tutto da mettere meglio nella versione definitiva
      If Len(nomeTabIndPart) Then
        idSave = instrLevel.idTable
        tabSave = instrLevel.Tavola
        indEnd = indEnd + 1
        ReDim Preserve TabIstr.BlkIstr(indEnd)
        TabIstr.BlkIstr(indEnd).idTable = idTabIndPart
        TabIstr.BlkIstr(indEnd).Tavola = nomeTabIndPart
        '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
        testo = testo & CostruisciSelect("T" & indEnd, idTabIndPart)
      End If
      
      testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom(1, indEnd, True)
     
      If Len(nomeTabIndPart) Then
         TabIstr.BlkIstr(indEnd - 1).idTable = idTabIndPart
         TabIstr.BlkIstr(indEnd - 1).Tavola = nomeTabIndPart
         TabIstr.BlkIstr(indEnd).idTable = idSave
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         'proviamo con la squalificata
         ReDim Preserve TabIstr.BlkIstr(indEnd).KeyDef(1)
         indPart = True
      Else
         indPart = False
      End If
      
      testo = testo & CostruisciWhere(1, indEnd)
      
      If Len(nomeTabIndPart) Then
         'rimette a posto
         indEnd = indEnd - 1
         ReDim Preserve TabIstr.BlkIstr(indEnd)
         TabIstr.BlkIstr(indEnd).idTable = idSave
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         indPart = False
      End If
      If K > 1 Or flagRed Then
         TabIstr.BlkIstr(indEnd).Tavola = tabSave
         TabIstr.BlkIstr(indEnd).idTable = idSave
      End If
    
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
    
      If Len(testoRed(K)) Then
        testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next K
    
    'diff
    '''''''''''''''''''''''''''''''''''
    ' SECONDA DECLARE
    '''''''''''''''''''''''''''''''''''
    If Not keyOp Then
      testo = testo & Space(11 + indentGN) & "ELSE" & vbCrLf
      For K = 1 To indRed
        indentRed = 0
        If Len(testoRed(K)) Then
          testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
          indentRed = 3
        End If
        '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
        indentGen = indentGNsq + indentRed
        
        testo = testo & Space(11 + indentGNsq + indentRed) _
                     & "EXEC SQL DECLARE " & CursorNameTOTSQ & Format(K, "00") & IIf(suffDynT = "", "GU ", "") & " CURSOR " _
                     & IIf(withold, "WITH HOLD", "") & vbCrLf & Space(11 + indentGNsq + indentRed) & " FOR SELECT " & vbCrLf
     
        indPart = Len(nomeTabIndPart) > 0
        'SP 9/9 pcb dinamico
        testo = testo & CostruisciSelect("T" & indEnd, idRed(K))
        indPart = False
         
        If K > 1 Then
          idSave = TabIstr.BlkIstr(indEnd).idTable
          tabSave = TabIstr.BlkIstr(indEnd).Tavola
          TabIstr.BlkIstr(indEnd).Tavola = nomeTRed(K)
          TabIstr.BlkIstr(indEnd).idTable = idRed(K)
        End If
        'SP 12/9: altro gioco di prestigio per gli indici particolari...  tutto da mettere meglio nella versione definitiva
        If nomeTabIndPart <> "" Then
             idSave = TabIstr.BlkIstr(indEnd).idTable
             tabSave = TabIstr.BlkIstr(indEnd).Tavola
             indEnd = indEnd + 1
             ReDim Preserve TabIstr.BlkIstr(indEnd)
             TabIstr.BlkIstr(indEnd).idTable = idTabIndPart
             TabIstr.BlkIstr(indEnd).Tavola = nomeTabIndPart
            '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
             testo = testo & CostruisciSelect("T" & indEnd, idTabIndPart)
        End If
        testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom(1, indEnd, True)
        'SP 12/9: altro gioco di prestigio per gli indici particolari...
        indPart = False
        If nomeTabIndPart <> "" Then
             TabIstr.BlkIstr(indEnd - 1).idTable = idTabIndPart
             TabIstr.BlkIstr(indEnd - 1).Tavola = nomeTabIndPart
             TabIstr.BlkIstr(indEnd).idTable = idSave
             TabIstr.BlkIstr(indEnd).Tavola = tabSave
             'proviamo con la squalificata
             ReDim Preserve TabIstr.BlkIstr(indEnd).KeyDef(1)
             indPart = True
        End If
        testo = testo & CostruisciWhere(1, indEnd, True)
        If nomeTabIndPart <> "" Then
             'rimette a posto
             indEnd = indEnd - 1
             ReDim Preserve TabIstr.BlkIstr(indEnd)
             TabIstr.BlkIstr(indEnd).idTable = idSave
             TabIstr.BlkIstr(indEnd).Tavola = tabSave
             indPart = False
        End If
        If K > 1 Then
           TabIstr.BlkIstr(indEnd).Tavola = tabSave
           TabIstr.BlkIstr(indEnd).idTable = idSave
        End If
      
        testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        testo = testo & Space(11 + indentGNsq + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
      
        If Len(testoRed(K)) Then
          testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
        End If
      Next
      testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf & vbCrLf
      testo = testo & Space(11 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
    Else
       testo = testo & vbCrLf
    End If
    '...diff
     
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
         testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
         indentRed = 3
      End If
      testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorNameTOTSQ & Format(K, "00") & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If Len(testoRed(K)) Then
         testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next
  
    'SQ?? sotto... testo = testo & Space(11 + indentGN) & "IF SQLCODE = -502" & vbCrLf
    
    'diff
    If Not keyOp Then
      testo = testo & Space(11 + indentGN) & "ELSE" & vbCrLf
      For K = 1 To indRed
        indentRed = 0
        If Len(testoRed(K)) Then
          testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
          indentRed = 3
        End If
        testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorNameTOTSQ & Format(K, "00") & "GU" & vbCrLf
        testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        If Len(testoRed(K)) Then
          testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
        End If
      Next
      testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
      'SQ?? sopra?
      testo = testo & Space(11 + indentGN) & "IF SQLCODE = -502" & vbCrLf
      testo = testo & Space(14 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
    End If
    
    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
        testo = testo & Space(14 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorNameTOTSQ & Format(K, "00") & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorNameTOTSQ & Format(K, "00") & vbCrLf
      testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If Len(testoRed(K)) Then
        testo = testo & Space(14 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next

    'diff
    If Not keyOp Then
      testo = testo & Space(14 + indentGN) & "ELSE" & vbCrLf
      For K = 1 To indRed
        indentRed = 0
        If Len(testoRed(K)) Then
           testo = testo & Space(14 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
           indentRed = 3
        End If
        testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorNameTOTSQ & Format(K, "00") & "GU" & vbCrLf
        testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorNameTOTSQ & Format(K, "00") & "GU" & vbCrLf
        testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        If Len(testoRed(K)) Then
           testo = testo & Space(14 + indentGNsq) & "END-IF" & vbCrLf
        End If
      Next
      testo = testo & Space(14 + indentGN) & "END-IF" & vbCrLf
    End If
    
    
    testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
    testo = testo & Space(11 + indentGN) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
   
'''    'diff
'    If Not keySecFlagUltimoLiv And Not keyPrimUguale Then
'      testo = testo & Space(14) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
'      testo = testo & Space(14) & "MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
'      'SQ 31-07-06 - C.C. "F"
'''      If InStr(instrLevel.CodCom, "F") Then
'''        'Sistemare INDENT!
'''      Else
        testo = testo & Space(11) & "END-IF" & vbCrLf
'''     End If
'''    Else
'''      'SQ D
'''      If level = UBound(TabIstr.BlkIstr) Then
'''        'fetch in pi�, valida su chiavi secondarie!!
'''        'SP 11/9: e su primarie per uguale
'''        'SP 14/9: problema su GHU+REPL+GHN+REPL
'''        testo = testo & Space(14) & "IF WK-SQLCODE = ZERO" & vbCrLf
'''        testo = testo & Space(14) & "   IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) = 'GU' OR 'ISRT'" & vbCrLf
'''        testo = testo & Space(14) & "                           OR 'GN' OR 'REPL' OR 'DLET'" & vbCrLf
'''        'testo = testo & Space(17) & "AND LNKDB-STKSSA(LNKDB-NUMSTACK) NOT < " & vbCrLf
'''        'testo = testo & Space(17) & "    LNKDB-KEYVALUE(1, 1)" & vbCrLf
'''        testo = testo & Space(6) & "***" & " FETCH TO EXCLUDE FIRST RECORD, USED IN LAST GU" & vbCrLf
'''        'SQ D
'''        'testo = testo & Space(20) & "PERFORM 9999-FETCH-" & CursorNameTOTSQ & "" & vbCrLf
'''        testo = testo & Space(20) & "PERFORM 9999-FETCH-" & CursorNameTOTSQ & vbCrLf
'''        testo = testo & Space(20) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
'''        testo = testo & Space(20) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf
'''        testo = testo & Space(20) & "   MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
'''        'testo = testo & Space(23) & "               LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
'''        For k = 1 To indRed
'''          indentRed = 0
'''          If testoRed(k) <> "" Then
'''            testo = testo & Space(23) & Trim(testoRed(k)) & vbCrLf
'''            indentRed = 3
'''          End If
'''          testo = testo & Space(20 + indentRed) & "   EXEC SQL CLOSE " & CursorNameTOTSQ & Format(k, "00") & IIf(keyOp, "", "GU") & vbCrLf
'''          testo = testo & Space(20 + indentRed) & "   END-EXEC" & vbCrLf
'''          If testoRed(k) <> "" Then
'''            testo = testo & Space(23) & "END-IF" & vbCrLf
'''          End If
'''        Next
'''        testo = testo & Space(14) & "      END-IF" & vbCrLf
'''        testo = testo & Space(14) & "   END-IF" & vbCrLf
'''        testo = testo & Space(14) & "END-IF" & vbCrLf
'''      End If  'D
'''
'''      'testo = testo & Space(17) & "MOVE LNKDB- KEYVALUE(" & indEnd & ", 1) TO LNKDB-STKSSA(LNKDB-NUMSTACK)" & vbCrLf
'''      testo = testo & _
'''        Space(14) & "IF WK-SQLCODE = ZERO" & vbCrLf & _
'''        Space(14) & "   MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf & _
'''        Space(14) & "   MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf & _
'''        Space(14) & "END-IF" & vbCrLf
'''      'SQ 31-07-06 - C.C. "F"
'''      If InStr(instrLevel.CodCom, "F") Then
'''        'Sistemare INDENT!
'''      Else
'''        testo = testo & Space(11) & "END-IF" & vbCrLf
'''      End If
'''    End If
'''
'''
    testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    testo = testo & Space(11) & "   MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
    testo = testo & Space(11) & "   MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
    If EmbeddedRoutine Then
      testo = testo & Space(11) & "   PERFORM FETCH-" & CursorNameTOTSQ & vbCrLf & _
                      Space(11) & "      THRU FETCH-" & CursorNameTOTSQ & "-EX" & vbCrLf
    Else
      testo = testo & Space(11) & "   PERFORM 9999-FETCH-" & CursorNameTOTSQ & vbCrLf
    End If
    testo = testo & Space(11) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf  'SQ - 29-08
    testo = testo & Space(11) & "END-IF" & vbCrLf & vbCrLf

    'diff
    testo = testo & Space(11) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf & _
                    Space(11) & "   MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
    If Not keyOp Then
      testo = testo & Space(14) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
    End If

    For K = 1 To indRed
      indentRed = 0
      If Len(testoRed(K)) Then
        testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorNameTOTSQ & Format(K, "00") & vbCrLf
      testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
      If testoRed(K) <> "" Then
         testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
      End If
    Next
    'diff
    If Not keyOp Then
      testo = testo & Space(14) & "ELSE" & vbCrLf
      For K = 1 To indRed
         If testoRed(K) <> "" Then
            testo = testo & Space(17) & Trim(testoRed(K)) & vbCrLf
            indentRed = 3
         End If
         testo = testo & Space(17 + indentRed) & "EXEC SQL CLOSE " & CursorNameTOTSQ & Format(K, "00") & "GU" & vbCrLf
         testo = testo & Space(17 + indentRed) & "END-EXEC" & vbCrLf
         If testoRed(K) <> "" Then
            testo = testo & Space(17) & "END-IF" & vbCrLf
         End If
      Next
      testo = testo & Space(14) & "END-IF" & vbCrLf
    End If
    testo = testo & Space(11) & "END-IF" & vbCrLf


    testo = testo & vbCrLf
'''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' TESTO FETCH:
    ' Viene scritto in fondo alla routine, in getRoutine_DB2
    ' Se ho pi� livelli, appendo il testo alle fecth dei precedenti
    ' SQ - si pu� isolare?
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'TMP
    GbTestoFetch = GbTestoFetch & getFetch(indRed, indEnd, indentGN, indentGNqu, Not keyOp)
         
    NomeT = TabIstr.BlkIstr(indEnd).Tavola
    NomePk = TabIstr.BlkIstr(indEnd).pKey
    If TabIstr.BlkIstr(indEnd).IdSegm Then
        Dim indGet As Long
      indGet = indEnd
      '?wLiv01Copy = getCopyDLISeg(indGet)
      
      If Not flagRed Then
        For K = 1 To indRed
          If testoRed(K) <> "" Then
             testo = testo & Space(11) & Trim(testoRed(K)) & vbCrLf
             indentRed = 3
          End If
          testo = testo & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
          testo = testo & Space(11 + indentRed) & "   PERFORM 9999-" & Replace(nomeTRed(K), "_", "-") & "-TO-RD" & vbCrLf
          testo = testo & Space(11 + indentRed) & "   MOVE RD-" & instrLevel.Segment & " TO LNKDB-DATA-AREA(" & UBound(TabIstr.BlkIstr) & ")" & vbCrLf
          testo = testo & Space(11 + indentRed) & "   MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE " & vbCrLf
          testo = testo & Space(11 + indentRed) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf
          If Len(nomeTabIndPart) Then
              testo = testo & Space(14 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
          End If
          'SQ D
          testo = testo & Space(14 + indentRed) & "MOVE '" & Format(level, "00") & "' TO LNKDB-PCBSEGLV" & vbCrLf
          testo = testo & Space(11 + indentRed) & "END-IF" & vbCrLf
          If testoRed(K) <> "" Then
             testo = testo & Space(11) & "END-IF" & vbCrLf
          End If
        Next
      End If
      
      testo = testo & vbCrLf
      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      testo = testo & Space(11) & "   MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
      testo = testo & Space(11) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf
      If keySecFlagUltimoLiv Then
        testo = testo & Space(11) & "   PERFORM 9999-SETA-" & Replace(TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
        testo = testo & Space(11) & "   MOVE KRR-" & TabIstr.BlkIstr(indEnd).KeyDef(1).key & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space(11) & "   MOVE KRDP-" & TabIstr.BlkIstr(indEnd).KeyDef(1).key & "  TO LNKDB-FDBKEY" & vbCrLf
      End If
      testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
  Next level
  
  getGN_DB2_TOTSQ = testo & Space(11) & "." & vbCrLf & Space(7) & "9999-ROUT-" & Gistruzione & "-" & NomeSegm & "-EX." & vbCrLf & vbCrLf & GbTestoFetch

End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' TESTO FETCH:
' Viene scritto in fondo alla routine, in getRoutine_DB2
' Se ho pi� livelli, appendo il testo alle fecth dei precedenti
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getFetch(indRed As Integer, indEnd As Integer, indentGN As Integer, indentGNqu As Integer, Optional gnLike As Boolean = False) As String
  Dim rs As Recordset
  Dim text As String, testoIndicatori As String
  Dim K As Integer, indentRed As Integer
  Dim indPart As Boolean
  
  If EmbeddedRoutine Then
    text = text & Space(6) & "*    FETCH RELATED TO ROUT-" & Gistruzione & vbCrLf & _
                  Space(6) & " FETCH-" & CursorName & "." & vbCrLf
  Else
    text = text & Space(6) & "*    FETCH RELATED TO ROUT-" & Gistruzione & vbCrLf & _
                Space(6) & " 9999-FETCH-" & CursorName & " SECTION." & vbCrLf
  End If
  If gnLike Then
    text = text & Space(11) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
  End If
  
  For K = 1 To indRed
    indentRed = 0
      If Len(testoRed(K)) Then
        text = text & Space(11 + indentGNqu) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      text = text & Space(11 + indentGNqu + indentRed) & "INITIALIZE "
      
      'SP 4/9 initialize indicatori di null
'''MG 240107
'''''      Set rs = m_dllFunctions.Open_Recordset( _
'''''        "SELECT * FROM dmdb2_tabelle as a, dmdb2_colonne as b WHERE a.idorigine = " & idRed(K) & _
'''''        " and a.idtable = b.idtable and [null] = -1")
      Set rs = m_dllFunctions.Open_Recordset( _
        "SELECT * FROM " & DB & "tabelle as a, " & DB & "colonne as b WHERE a.idorigine = " & idRed(K) & _
        " and a.idtable = b.idtable and [null] = -1")

      If rs.RecordCount Then
        testoIndicatori = " " & "WK-I-" & Replace(nomeTRed(K), "_", "-")
      Else
        testoIndicatori = ""
      End If
      rs.Close
      
      text = text & "RR-" & Replace(nomeTRed(K), "_", "-") & testoIndicatori & vbCrLf & vbCrLf
      indentGen = indentGN + indentRed
      text = text & Space(11 + indentGNqu + indentRed) & "EXEC SQL FETCH " & CursorName & Format(K, "00") & vbCrLf
         
      indPart = Len(nomeTabIndPart) > 0 'SQ - Attenzione: utilizzato dal CostruisciInto!
      text = text & Space(12 + indentGNqu + indentRed) & "INTO " & vbCrLf _
                             & CostruisciInto("T" & indEnd, idRed(K), nomeTRed(K), "GU")
      indPart = False
      
      If Len(nomeTabIndPart) Then
         indEnd = indEnd + 1
         '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
         text = text & CostruisciInto("T" & indEnd, idTabIndPart, nomeTabIndPart, "GU")
         indEnd = indEnd - 1
      End If
      text = text & Space(11 + indentGNqu + indentRed) & "END-EXEC" & vbCrLf
      
      If Len(testoRed(K)) Then
         text = text & Space(11 + indentGNqu) & "END-IF" & vbCrLf
      End If
  Next
   
  If gnLike Then
    text = text & Space(11) & "ELSE" & vbCrLf
    For K = 1 To indRed
      indentRed = 0
      If testoRed(K) <> "" Then
        text = text & Space(11 + indentGN) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      text = text & Space(11 + indentGN + indentRed) & "INITIALIZE "
      'SP 4/9 initialize indicatori di null
'''MG 240107
'''''      Set rs = m_dllFunctions.Open_Recordset( _
'''''        "SELECT * FROM dmdb2_tabelle as a, dmdb2_colonne as b where a.idorigine = " & idRed(K) & _
'''''        " and a.idtable = b.idtable and [null] = -1")
      Set rs = m_dllFunctions.Open_Recordset( _
        "SELECT * FROM " & DB & "tabelle as a, " & DB & "colonne as b where a.idorigine = " & idRed(K) & _
        " and a.idtable = b.idtable and [null] = -1")

      If rs.RecordCount Then
         testoIndicatori = " " & "WK-I-" & Replace(nomeTRed(K), "_", "-")
      Else
         testoIndicatori = ""
      End If
      rs.Close
      
      text = text & "RR-" & Replace(nomeTRed(K), "_", "-") & testoIndicatori & vbCrLf
      indentGen = indentGN + indentRed
      text = text & Space(11 + indentGN + indentRed) _
          & "EXEC SQL FETCH " & CursorName & Format(K, "00") & "GU" & vbCrLf
      indPart = False
      If nomeTabIndPart <> "" Then
          indPart = True
      End If
      text = text & Space(12 + indentGN + indentRed) & "INTO " & vbCrLf & CostruisciInto("T" & indEnd, idRed(K), nomeTRed(K), "GU") & vbCrLf
       
      indPart = False
      If Len(nomeTabIndPart) Then
        indEnd = indEnd + 1
        '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
        text = text & CostruisciInto("T" & indEnd, idTabIndPart, nomeTabIndPart, "GU") & vbCrLf
        indEnd = indEnd - 1
      End If
      text = text & Space(11 + indentGN + indentRed) & "END-EXEC" & vbCrLf
  
      If Len(testoRed(K)) Then
        text = text & Space(11 + indentGN) & "END-IF" & vbCrLf
      End If
    Next
    text = text & Space(11) & "END-IF" & vbCrLf
  End If
  text = text & Space(11) & "." & vbCrLf
  If EmbeddedRoutine Then
    text = text & Space(7) & "FETCH-" & CursorName & "EX." & vbCrLf & _
                      Space(11) & "EXIT." & vbCrLf
  End If
  getFetch = text
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 4-09-06
' Setta:
' - includedMember
' - GbTestoRedOcc (aggiorna l'eventuale testo relativo alle redefines...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function createOccursRoutine(instrLevel As BlkIstrDli, tipoIstr As String, flagOcc As Boolean) As Boolean
  Dim rs As Recordset
  Dim tabFound As Boolean
  '''Dim idSave As Double
  
'''MG 240107
'''''  Set rs = m_dllFunctions.Open_Recordset( _
'''''    "SELECT * FROM dmdb2_tabelle as a, dmdb2_index as b WHERE a.idtable = b.idtable " & _
'''''    " AND a.idtablejoin = " & instrLevel.idTable & " AND a.TAG like '%OCCURS%' and b.tipo = 'P' ORDER BY a.nome")
  Set rs = m_dllFunctions.Open_Recordset( _
    "SELECT * FROM " & DB & "tabelle as a, " & DB & "index as b WHERE a.idtable = b.idtable " & _
    " AND a.idtablejoin = " & instrLevel.idTable & " AND a.TAG like '%OCCURS%' and b.tipo = 'P' ORDER BY a.nome")

  If Not rs.EOF Then
    'SEGMENTO OCCURSATO: gi� trattato?
    Dim IdxTabOk2 As Integer
    createOccursRoutine = False
    For IdxTabOk2 = 1 To idxTabOkMax2
       If tabOk2(IdxTabOk2) = instrLevel.idTable And istrOk2(IdxTabOk2) = "SELT" Then
          createOccursRoutine = True
          Exit For
       End If
    Next
    idSave = instrLevel.idTable 'save
    If Not createOccursRoutine Then
      idxTabOkMax2 = idxTabOkMax2 + 1
      ReDim Preserve tabOk2(idxTabOkMax2)
      ReDim Preserve istrOk2(idxTabOkMax2)
      ReDim Preserve istrOkDet2(idxTabOkMax2)
      tabOk2(idxTabOkMax2) = instrLevel.idTable
      istrOk2(idxTabOkMax2) = "SELT"
      istrOkDet2(idxTabOkMax2) = tipoIstr
      Dim istrSave As IstrDli 'buttare via questa modalit�!!
      istrSave = TabIstr
      ReDim TabIstr.BlkIstr(1)
      'withold non serve nelle occurs
      GbTestoRedOcc = GbTestoRedOcc & costruisciOccurs(instrLevel.idTable, instrLevel.pKey, 0, False, flagOcc, False, instrLevel.Tavola)  'setta flagOcc
      TabIstr = istrSave
      includedMember = "PS" & Right(getAlias_tabelle(Int(idSave)), 6)
      'tmp
      m_dllFunctions.WriteLog "--------------" & includedMember
    Else
      flagOcc = False
      GbTestoRedOcc = ""
      includedMember = ""
    End If
    Dim tabFound3 As Boolean
    'SISTEMAZIONE lista OCCURS: tabOk2
    tabFound3 = False
    For IdxTabOk2 = 1 To idxTabOkMax2
      'SQ 25-07-06 - pezza su pezza:
      'non so assolutamente cos'� 'sta roba, in ogni caso qualche volta "istrOkDet2" � pi� corto
      'e va in errore!
      'If tabOk2(IdxTabOk2) = idSave And istrOk2(IdxTabOk2) = "SELT" And istrOkDet2(IdxTabOk2) = Right(nomeroutine, 4) Then
      If tabOk2(IdxTabOk2) = idSave And istrOk2(IdxTabOk2) = "SELT" And UBound(istrOkDet2) >= IdxTabOk2 Then
        If istrOkDet2(IdxTabOk2) = Right(nomeroutine, 4) Then
          tabFound3 = True
          Exit For
        End If
      End If
    Next
    If Not tabFound3 Then
      idxTabOkMax2 = idxTabOkMax2 + 1
      ReDim Preserve tabOk2(idxTabOkMax2)
      ReDim Preserve istrOk2(idxTabOkMax2)
      ReDim Preserve istrOkDet2(idxTabOkMax2)
      tabOk2(idxTabOkMax2) = idSave
      istrOk2(idxTabOkMax2) = "SELT"
      istrOkDet2(idxTabOkMax2) = Right(nomeroutine, 4)
      'SQ gi� calcolato sopra
      If Len(includedMember) = 0 Then includedMember = "PS" & Right(getAlias_tabelle(Int(idSave)), 6)
    End If
  End If
  rs.Close
End Function
''''''''''''''''''''''''''''''''''''''''''''
' SQ 4-08-06
''''''''''''''''''''''''''''''''''''''''''''
Private Sub createRedefineRoutine(instrLevel As BlkIstrDli, tipoIstr As String, flagOcc As Boolean, redefinesNumber As Integer, flagIns As Boolean)
  Dim tabFound As Boolean, tabFound3 As Boolean
  Dim IdxTabOk As Integer, K As Integer
  
  For IdxTabOk = 1 To idxTabOkMax
    If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "SELT" Then
       tabFound = True
       Exit For
    End If
  Next
  If Not tabFound Then
     idxTabOkMax = idxTabOkMax + 1
     ReDim Preserve tabOk(idxTabOkMax)
     ReDim Preserve istrOk(idxTabOkMax)
     ReDim Preserve istrOkDet(idxTabOkMax)
     tabOk(idxTabOkMax) = nomeTRed(1)
     istrOk(idxTabOkMax) = "SELT"
     istrOkDet(idxTabOkMax) = tipoIstr
  Else
    GbTestoRedOcc = ""
    includedMember = ""
  End If
  'REDEFINES: COSTRUZIONE ROUTINE (COPY)
  If Not tabFound Then
    If Len(includedMember) = 0 Then includedMember = "PS" & Right(getAlias_tabelle(Int(instrLevel.idTable)), 6)
    'tmp
    'm_dllFunctions.WriteLog "--------------" & includedMember
    'stefano: provo a mettere il giochino sul cambio nome chiave solo qui, dovrebbe servire solo per i casi di dispatcher
    'ATTENZIONE: BISOGNEREBBE SALVARE TUTTA LA STRUTTURA, SI FAREBBE PRIMA,
    ' E SAREBBE PIU' SICURO, MA NON SO COME FARE....
    idSave = instrLevel.idTable
     
    GbTestoRedOcc = GbTestoRedOcc & Space(6) & "*    SELECT RELATED TO " & nomeTRed(1) & vbCrLf
    GbTestoRedOcc = GbTestoRedOcc & Space(6) & " 9999-" & Replace(nomeTRed(1), "_", "-") & "-SELT SECTION." & vbCrLf
    GbTestoRedOcc = GbTestoRedOcc & Space(11) & "MOVE " & nomeFieldRed(1) & " OF RR-" & SostUnderScore(nomeTRed(1)) & vbCrLf
    GbTestoRedOcc = GbTestoRedOcc & Space(11) & "  TO KRD-" & Replace(nomeKeyRed(1), "_", "-") & vbCrLf
    Dim istrSave As IstrDli 'buttare via questa modalit�!!
    istrSave = TabIstr
    ReDim TabIstr.BlkIstr(1)
    For K = 2 To redefinesNumber
      ReDim TabIstr.BlkIstr(1).KeyDef(1)
      TabIstr.BlkIstr(1).KeyDef(1).key = nomeKeyRed(K)
      TabIstr.BlkIstr(1).KeyDef(1).Op = "="
      'SQ 21-07-06 (errore K00 nelle WHERE)
      TabIstr.BlkIstr(1).KeyDef(1).KeyNum = 1
      TabIstr.BlkIstr(1).Tavola = nomeTRed(K)
      TabIstr.BlkIstr(1).idTable = idRed(K)
      TabIstr.BlkIstr(1).pKey = nomeKeyRed(K)
      
      indentRed = 0
      GbTestoRedOcc = GbTestoRedOcc & vbCrLf & Space(6) & "*** REDEFINES" & vbCrLf
          
      If Len(testoRed(K)) Then
        GbTestoRedOcc = GbTestoRedOcc & Space(11) & Trim(testoRed(K)) & vbCrLf
        indentRed = 3
      End If
      'SQ 26-07-06 problemi come sopra...
      'Nella MOVE fuori "IF" viene riempita la KRD-<nomeKeyRed(1)> !...
      'VERIFICARE!!!!!!!!!!!!!!!!!!!!!
      'GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRD-" & nomeKey & " TO" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRD-" & nomeKeyRed(1) & " TO" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(16 + indentRed) & "KRD-" & nomeKeyRed(K) & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRR-" & nomeKeyRed(1) & " TO KRR-" & nomeKeyRed(K) & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
      'per ora K01 fisso!!!
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE KRR-" & nomeKeyRed(K) & " TO K01-" & nomeKeyRed(K) & vbCrLf
      'manca with hold: da verificare come farla, comunque c'� gi� sulla tabella selettore
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "EXEC SQL SELECT " & vbCrLf _
                                                           & CostruisciSelect("T1", idRed(K))
      GbTestoRedOcc = GbTestoRedOcc & Space(15) & "INTO " & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & CostruisciInto("T1", idRed(K), nomeTRed(K), "GU")
      GbTestoRedOcc = GbTestoRedOcc & Space(14 + indentRed) & "FROM " & vbCrLf
      indentGen = indentRed
      GbTestoRedOcc = GbTestoRedOcc & CostruisciFrom(1, 1, True) & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & CostruisciWhere(1, 1, False, True)
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "END-EXEC" & vbCrLf & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "   PERFORM 9999-" & SostUnderScore(nomeTRed(K)) & "-TO-RD" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "END-IF" & vbCrLf
        
      'OCCURS
      If Not flagIns Then
        ReDim TabIstr.BlkIstr(1).KeyDef(0)
        '''''''''''buttare via! nomeTab =
        GbTestoRedOcc = GbTestoRedOcc & costruisciOccurs(idRed(K), nomeKeyRed(K) & "", indentRed, False, flagOcc, True, nomeTRed(K))
      End If
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "   MOVE RD-" & nomeSRed(K) & " TO WK-DATA-AREA" & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "   MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE " & vbCrLf
      GbTestoRedOcc = GbTestoRedOcc & Space(11 + indentRed) & "END-IF" & vbCrLf
      If Len(testoRed(K)) Then
        GbTestoRedOcc = GbTestoRedOcc & Space(11) & "END-IF" & vbCrLf
      End If
    Next
    GbTestoRedOcc = GbTestoRedOcc & Space(11) & "." & vbCrLf
    TabIstr = istrSave
    ''''''End If
    ''''''If tabFound Then
  Else
    'Aggiornamento lista "tabOk" - REDEFINES!
    tabFound3 = False
    For IdxTabOk = 1 To idxTabOkMax
      If tabOk(IdxTabOk) = nomeTRed(1) And istrOk(IdxTabOk) = "SELT" And UBound(istrOkDet) >= IdxTabOk Then
        If istrOkDet(IdxTabOk) = Right(nomeroutine, 4) Then
          tabFound3 = True
          Exit For
       End If
      End If
    Next
    If Not tabFound3 Then
      idxTabOkMax = idxTabOkMax + 1
       ReDim Preserve tabOk(idxTabOkMax)
       ReDim Preserve istrOk(idxTabOkMax)
       ReDim Preserve istrOkDet(idxTabOkMax)
      tabOk(idxTabOkMax) = nomeTRed(1) ' sulle occurs uso l'id, qui il nome; funziona, ma � da rivedere
      istrOk(idxTabOkMax) = "SELT"
      istrOkDet(idxTabOkMax) = Right(nomeroutine, 4)
      If Len(includedMember) = 0 Then includedMember = "PS" & Right(getAlias_tabelle(Int(idSave)), 6)
    End If
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' GN COMPLETAMENTE SQUALIFICATE
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Function getGNsq_DB2(rtbFile As RichTextBox, tipoIstr As String, Optional ByVal withold As Boolean = False) As String
  Dim rsSenSeg As Recordset, rsTable As Recordset
  Dim testo As String, TestoGNTotSQ As String
  Dim i As Integer
  
  ' costruisce evaluate
  testo = testo & Space(11) & "EVALUATE LNKDB-PCBSEGNM" & vbCrLf & _
                  Space(11) & "    WHEN SPACE" & vbCrLf
  
  Set rsSenSeg = m_dllFunctions.Open_Recordset( _
    "SELECT b.idSegmento as idSegmento,a.Segmento as segName, a.Parent as parent FROM PsDli_PsbSeg as a, PsDLI_Segmenti as b " & _
    " WHERE a.idOggetto=" & TabIstr.PsbId & " AND a.numPcb=" & TabIstr.ordPcb & _
    " AND a.segmento=b.nome and b.idOggetto=" & mIdDBDCorrente & " ORDER BY a.ordinale")
  For i = 1 To rsSenSeg.RecordCount
    ReDim Preserve TabIstr.BlkIstr(i)
    TabIstr.BlkIstr(i) = TabIstr.BlkIstr(0) 'serve?
    TabIstr.BlkIstr(i).Segment = rsSenSeg!segname
    TabIstr.BlkIstr(i).IdSegm = rsSenSeg!idSegmento
    'Non ho voglia di utilizzare un'altra struttura dedicata!!!
    'rapino qualche campo dalla struttura:
    '- NewNameSeg <=> PARENT
    '- NomeRout   <=> CHILD
    '- SSAname   <=> FRATELLO
    TabIstr.BlkIstr(i).NewNameSeg = rsSenSeg!parent
    'Assegnazione primo CHILD al parent:
    Dim j As Integer
    For j = 1 To i
      If TabIstr.BlkIstr(j).Segment = TabIstr.BlkIstr(i).NewNameSeg Then
        If Len(TabIstr.BlkIstr(j).NomeRout) = 0 Then TabIstr.BlkIstr(j).NomeRout = TabIstr.BlkIstr(i).Segment
        Exit For
      End If
    Next
    'Assegnazione primo FRATELLO (vado all'indietro: mi serve il primo!)
    For j = i - 1 To 1 Step -1
      If TabIstr.BlkIstr(j).NewNameSeg = TabIstr.BlkIstr(i).NewNameSeg Then
        If Len(TabIstr.BlkIstr(j).ssaName) = 0 Then TabIstr.BlkIstr(j).ssaName = TabIstr.BlkIstr(i).Segment
        Exit For
      End If
    Next
    'TABELLA:
'''MG 240107
'''''    Set rsTable = m_dllFunctions.Open_Recordset("select idTable,nome,creator from DMdb2_Tabelle where idorigine = " & rsSenSeg!idSegmento & " and TAG not like '%DUMMY%' and TAG not like '%OCCURS%'")
    Set rsTable = m_dllFunctions.Open_Recordset("select idTable,nome,creator from " & DB & "Tabelle where idorigine = " & rsSenSeg!idSegmento & " and TAG not like '%DUMMY%' and TAG not like '%OCCURS%'")

    If rsTable.RecordCount Then
      TabIstr.BlkIstr(i).Tavola = rsTable!nome
      TabIstr.BlkIstr(i).idTable = rsTable!idTable
      TabIstr.BlkIstr(i).tableCreator = rsTable!creator & ""
'''''      Set rsTable = m_dllFunctions.Open_Recordset("select * from dmdb2_index where tipo = 'P' and idtable = " & rsTable!idTable)
      Set rsTable = m_dllFunctions.Open_Recordset("select * from " & DB & "index where tipo = 'P' and idtable = " & rsTable!idTable)

      If rsTable.RecordCount Then
        TabIstr.BlkIstr(i).pKey = rsTable!nome
      End If
    End If
    rsTable.Close
    TabIstr.BlkIstr(i).valida = True
    ReDim TabIstr.BlkIstr(i).KeyDef(0)
    If i > 1 Then TabIstr.BlkIstr(i).KeyDef(0).Op = "="
    rsSenSeg.MoveNext
  Next
  rsSenSeg.Close
  
  '''''''''''''''''''''''''''''''''''''''''''
  ' Routine iniziale: "DISPATCHER"
  '''''''''''''''''''''''''''''''''''''''''''
  For i = 1 To UBound(TabIstr.BlkIstr)
    Dim routineName As String
      
    If Len(TabIstr.BlkIstr(i).NomeRout) Then
      'HA FIGLIO: CALL SEGMENTO FIGLIO
      routineName = "9999-ROUT-" & Gistruzione & "-" & TabIstr.BlkIstr(i).NomeRout
    Else
      routineName = "9999-ROUT-" & Gistruzione & "-" & TabIstr.BlkIstr(i).Segment
      'NON HA FIGLIO: CALL SEGMENTO STESSO
    End If
    testo = testo & Space(15) & "WHEN '" & TabIstr.BlkIstr(i).Segment & "'" & vbCrLf & _
                    Space(15) & "   PERFORM " & routineName & " THRU" & vbCrLf & _
                    Space(15) & "    " & routineName & "-EX" & vbCrLf
  Next
  testo = testo & Space(11) & "END-EVALUATE." & vbCrLf & Space(7) & "9999-ROUT-" & Gistruzione & "-EX." & vbCrLf
  
  Dim jj As Integer
  For jj = 1 To UBound(TabIstr.BlkIstr)
  '  TestoGNTotSQ = TestoGNTotSQ & vbCrLf & getGN_DB2_TOTSQ(TabIstr.BlkIstr(i).Segment, i)
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim K As Integer, k1 As Integer, indEnd As Integer, indSav As Integer, indentGNsqKeyOp As Integer
    Dim NomeT As String, NomePk As String, nomeKey As String
    Dim TestoFrom As String, TestoWhere As String
    Dim keySecFlagPrimoLiv As Boolean, keySecFlagUltimoLiv As Boolean
    Dim flagSel As Boolean, flagIns As Boolean, flagRed As Boolean
    Dim flagOcc As Boolean, isOccurs As Boolean
    Dim tb1 As Recordset, tb2 As Recordset, tbred As Recordset, rsIndPart As Recordset, tb As Recordset
    Dim indentGN As Integer, indentGNsq As Integer, indentGNqu As Integer
    Dim keyPrim As String, keySec As String
    Dim indRed As Integer
    Dim keySave() As BlkIstrKey
    Dim istrSave As IstrDli
    Dim instrLevel As BlkIstrDli
    Dim nomeTab As String
    'SQ
    Dim testoTmp As String
    'diff
    Dim qualified As Boolean, keyOp As Boolean, keyPrimUguale As Boolean
    
    indentGen = 0
    indentRed = 0
    
    level = jj
    testo = testo & vbCrLf & "      *------------------------------------------------------------*" & vbCrLf & _
                     "       9999-ROUT-" & Gistruzione & "-" & TabIstr.BlkIstr(level).Segment & "." & vbCrLf
        
    testo = testo & Space(11) & "MOVE 'N' TO WK-GNQUAL" & vbCrLf
    testo = testo & Space(11) & "MOVE '" & tipoIstr & "' TO WK-ISTR" & vbCrLf
    
    '''''''''''''
    'init:
    '''''''''''''
    keySecFlagUltimoLiv = False
    flagOcc = False
    isOccurs = False
    includedMember = ""
        
    'SQ D
    indEnd = level
    instrLevel = TabIstr.BlkIstr(indEnd)
    
    If InStr(TabIstr.BlkIstr(jj).CodCom, "D") And level > 1 Then
      'Lettura solo se il precedente livello andato a buon fine
      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
    End If
    
    If UBound(TabIstr.BlkIstr) Then
      testo = testo & Space(11) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
      testo = testo & Space(11) & "MOVE '" & instrLevel.Segment & "' TO WK-SEGMENT" & vbCrLf
    End If
      testo = testo & Space(11) & "PERFORM 9000-MANAGE-PCB" & vbCrLf
      flagIns = False
    
      If Not indEnd > 0 Then  'SQ - portare fuori
        m_dllFunctions.WriteLog "Instruction: " & GbNumIstr & " - not converted", "Routines Generation"
        Exit Function
      End If
          
      If UBound(instrLevel.KeyDef) Then
        qualified = True 'portare dentro la IF
        If instrLevel.KeyDef(1).key <> instrLevel.pKey And Len(instrLevel.KeyDef(1).key) Then
          keySecFlagUltimoLiv = True
'''MG 240107
'''''          Set rsIndPart = m_dllFunctions.Open_Recordset( _
'''''            "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, DMDB2_Tabelle as b " & _
'''''            "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
'''''            " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")
          Set rsIndPart = m_dllFunctions.Open_Recordset( _
            "SELECT b.nome as nometab, b.idtable as idTab from PsDli_XDfield as a, " & DB & "Tabelle as b " & _
            "WHERE a.nome = '" & instrLevel.KeyDef(1).NameAlt & "' and a.idsegmento = b.idorigine " & _
            " and b.idObjSource=" & mIdDBDCorrente & " and b.TAG='AUTOMATIC'")

          If Not rsIndPart.EOF Then
            If rsIndPart!nomeTab <> instrLevel.Tavola Then
               nomeTabIndPart = rsIndPart!nomeTab
               idTabIndPart = rsIndPart!idTab
            End If
  
         End If
          keyOp = instrLevel.KeyDef(1).Op <> ">"
        Else
          keySecFlagUltimoLiv = False
          keyOp = instrLevel.KeyDef(1).Op <> ">="
          keyPrimUguale = instrLevel.KeyDef(1).Op = "="
        End If
      Else
        qualified = False
      End If
      
      indentGN = 3
      
      'specifico:
      If level > 1 Then
        keyOp = True
      End If
      
      If keyOp Then
         indentGNsq = 3
         indentGNqu = 0
      Else
         indentGNsq = 6
         indentGNqu = 3
      End If
      indentGNsqKeyOp = 6
      indentRed = 0
      
      testo = testo & Space(11) & "INITIALIZE RD-" & instrLevel.Segment & vbCrLf
      'SQ - Specifico Complet. Squal: SEGLEVEL = 1, SEMPRE!
      testo = testo & Space(11) & "MOVE LNKDB-SEGLEVEL(1) TO LNKDB-STKLEV(LNKDB-NUMSTACK)" & vbCrLf & vbCrLf
      
      testo = testo & Space(11) & "IF LNKDB-STKCURSOR(LNKDB-NUMSTACK) = SPACES" & vbCrLf
      testo = testo & Space(11) & "OR LNKDB-STKNUMISTR(LNKDB-NUMSTACK) NOT = " & GbNumIstr & vbCrLf
      'SQ - Specifico Complet. Squal: la IF interna solo sulla ROOT (level=1)... (VERIFICARE BENE!)
      If level = 1 Then
        testo = testo & Space(11) & "   IF LNKDB-STKLASTOP(LNKDB-NUMSTACK) NOT = 'GU' AND 'ISRT'" & vbCrLf
        testo = testo & Space(11) & "                             AND 'GN' AND 'REPL' AND 'DLET'" & vbCrLf
        testo = testo & Space(11 + indentGNsq) & "MOVE 'N' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
      Else
        testo = testo & Space(11 + indentGNsq) & "MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space(11 + indentGNsq) & "MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
      End If
      
      'SQ - sto pezzo probabilmente � da tenere fuori ciclo... giusto?
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'Segmento "parent":
      'stefanopippo: 15/08/2005: se su chiave secondaria (sempre sul primo livello!) non fa restore (� inutile)
      keySecFlagPrimoLiv = False
      If UBound(TabIstr.BlkIstr(jj).KeyDef) Then
        keySecFlagPrimoLiv = TabIstr.BlkIstr(jj).KeyDef(1).key <> TabIstr.BlkIstr(jj).pKey
      End If
      If Not keySecFlagPrimoLiv Then
         NomeT = getParentTable(CLng(TabIstr.BlkIstr(jj).IdSegm))  'togliere cast e cambiare function
         If Len(NomeT) Then
            'SEGMENTO NON RADICE: RESTORE
            testo = testo & Space(11 + indentGNsq) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
            testo = testo & Space(11 + indentGNsq) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
         End If
      End If
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' RESTORE CHIAVE:
      ' Attenzione: per ottimizzare posso appoggiare il nome della tabella in TabIstr.BlkIstr(0)! (valutare)
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'SQ - Specifico Complet. Squal: Solo sulla ROOT:
      If level = 1 Then
        For K = 1 To indEnd
          NomeT = TabIstr.BlkIstr(K).Tavola
          If Trim(NomeT) = "" Then
            NomeT = "[NO-TABLE-FOR:" & TabIstr.BlkIstr(K).Segment & "]"
          End If
          NomePk = TabIstr.BlkIstr(K).pKey
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
          'squalificata solo sull'ultimo livello, imposta la chiave primaria ereditata
          ' Rob
          BlkFin = 1
          If UBound(TabIstr.BlkIstr(K).KeyDef) = 0 And K = BlkFin Then
            testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
            testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & " TO K01-" & Replace(TabIstr.BlkIstr(BlkFin).pKey, "_", "-") & vbCrLf
          End If
          For k1 = 1 To UBound(TabIstr.BlkIstr(K).KeyDef)
            If Len(TabIstr.BlkIstr(K).KeyDef(k1).key) Then
              nomeKey = Replace(TabIstr.BlkIstr(K).KeyDef(k1).key, "-", "_")
              'SQ
              'NomePk = TabIstr.BlkIstr(k).KeyDef(k1).NameAlt
              NomePk = nomeKey
              testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(K)) & ", " & Format(k1, "#0") & ") TO " & vbCrLf
              testo = testo & Space(16 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf
              'SP 12/9: indici particolari
              If Len(nomeTabIndPart) And K = indEnd Then
                testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
              End If
              'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
              testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
              If nomeTabIndPart <> "" And K = indEnd Then
                testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
              End If
              If TabIstr.BlkIstr(K).pKey = TabIstr.BlkIstr(K).KeyDef(k1).key Then
                testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
              End If
              testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K" & Format(TabIstr.BlkIstr(K).KeyDef(k1).KeyNum, "00") & "-" & NomePk & vbCrLf
            End If
          Next k1
        Next K
        
        'diff
        testo = testo & Space(14) & "ELSE" & vbCrLf
        If Not keyOp Then
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE 'O' TO LNKDB-STKTIPO(LNKDB-NUMSTACK)" & vbCrLf
        End If
        testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
        If Not keySecFlagUltimoLiv Then
           testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 10000-REST-FDBKEY" & vbCrLf
        Else
          NomePk = instrLevel.KeyDef(1).key
          testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-STKPTGKEY(LNKDB-NUMSTACK) TO " & vbCrLf
          testo = testo & Space(16 + indentGNsqKeyOp) & "KRR-" & NomePk & vbCrLf
        End If
        'SQ 7-07-06 - Controllare... non credo vada bene!
        testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & NomePk & " TO K01-" & NomePk & vbCrLf
        'SP 9/9 pcb dinamico
        For k1 = 2 To UBound(instrLevel.KeyDef)
          j = k1
    
          If Trim(instrLevel.KeyDef(k1).key) <> "" Then
           nomeKey = instrLevel.KeyDef(k1).key
           NomePk = instrLevel.KeyDef(k1).NameAlt
           testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE LNKDB-KEYVALUE(" & Trim(Str(indEnd)) & ", " & Format(j, "#0") & ") TO " & vbCrLf
           'SQ
           'testo = testo & Space(13 + indentGNsqKeyOp) & "KRD-" & instrLevel.KeyDef(k1).NameAlt & vbCrLf
           testo = testo & Space(13 + indentGNsqKeyOp) & "KRD-" & nomeKey & vbCrLf
    
          'SP 12/9: indici particolari
           If nomeTabIndPart <> "" Then
              testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & nomeTabIndPart & "' TO WK-NAMETABLE" & vbCrLf
           End If
           'testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM SETK-WKUTWKEY THRU SETK-WKUTWKEY-EX " & vbCrLf
           testo = testo & Space(11 + indentGNsqKeyOp) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
           If nomeTabIndPart <> "" Then
              testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE '" & instrLevel.Tavola & "' TO WK-NAMETABLE" & vbCrLf
           End If
           testo = testo & Space(11 + indentGNsqKeyOp) & "MOVE KRR-" & nomeKey & " TO K" & Format(instrLevel.KeyDef(k1).KeyNum, "00") & "-" & nomeKey & vbCrLf
          End If
        Next k1
    
        testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
      
        If Not keyOp Then
          testo = testo & Space(11 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
        End If
      Else
        testo = testo & Space(11 + indentGNsq) & "PERFORM 9999-SETK-WKUTWKEY" & vbCrLf
        testo = testo & Space(11 + indentGNsq) & "PERFORM 9999-SETC-WKEYWKEY" & vbCrLf
        testo = testo & Space(11 + indentGNsq) & "MOVE KRR-" & NomePk & " TO K01-" & NomePk & vbCrLf
      End If 'level = 1
            
      'UTILIZZO DI ARRAY ANCHE PER CASI "NORMALI"
      indRed = 1
      ReDim Preserve testoRed(1)
      ReDim Preserve nomeTRed(1)
      ReDim Preserve nomeSRed(1)
      ReDim Preserve idRed(1)
      ReDim Preserve nomeKeyRed(1)
      ReDim Preserve nomeFieldRed(1)
      idRed(1) = instrLevel.idTable
      If UBound(instrLevel.KeyDef) Then
        nomeKeyRed(1) = instrLevel.KeyDef(1).key
      Else
        nomeKeyRed(1) = ""
      End If
      Dim tbred2 As Recordset
      Set tbred2 = m_dllFunctions.Open_Recordset( _
        "select nome as nomeidx from psdli_field where unique and idsegmento=" & instrLevel.IdSegm)
      If Not tbred2.EOF Then
        nomeFieldRed(1) = tbred2!Nomeidx
      Else
        m_dllFunctions.WriteLog "No Field found for segment: " & instrLevel.IdSegm, "Routines Generation"
        nomeFieldRed(1) = ""
      End If
      nomeTRed(1) = instrLevel.Tavola
      nomeSRed(1) = instrLevel.Segment
      
      '''''''''''''''''''''''''''''''''''
      ' DECLARE CURSORE
      '''''''''''''''''''''''''''''''''''
      For K = 1 To indRed '???????????????? pu� essere maggiore di 1???????????????
        indentRed = 0
        If Len(testoRed(K)) Then  'PU� ENTRARE QUI???
          'CONDIZIONE PER TABELLE RIDEFINITE
          testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
          indentRed = 3
        End If
        indentGen = indentGNsq + indentRed
        
        'rob
        'Dim CursorNameTOTSQ As String
        'CursorName = CursorName() & TabIstr.BlkIstr(jj).Segment
        suffDynT = Mid(TabIstr.BlkIstr(jj).Segment, 4) 'Serve per costruire il nome del cursore!!
        
        testo = testo & Space(11 + indentGNsq + indentRed) & _
          "EXEC SQL DECLARE " & CursorName & Format(K, "00") & " CURSOR " & IIf(withold, "WITH HOLD", "") & vbCrLf & _
          Space(11 + indentGNsq + indentRed) & " FOR SELECT " & vbCrLf
        indPart = Len(nomeTabIndPart) > 0 'SQ - passarlo come parametro!
        testo = testo & CostruisciSelect("T" & indEnd, idRed(K))
        indPart = False
               
        ''Dim idSave As Double
        Dim tabSave As String, idxSave As String, idxSave2 As String, opSave As String
        'in realt� potrei farlo sempre, in quanto nel vettore, in caso senza redefines
        'c'� comunque lo stesso segmento/tabella
        If K > 1 Or flagRed Then
          idSave = instrLevel.idTable
          tabSave = instrLevel.Tavola
          instrLevel.Tavola = nomeTRed(K)
          instrLevel.idTable = idRed(K)
        End If
        'SP 12/9: altro gioco di prestigio per gli indici particolari...
        'tutto da mettere meglio nella versione definitiva
        If Len(nomeTabIndPart) Then
          idSave = instrLevel.idTable
          tabSave = instrLevel.Tavola
          indEnd = indEnd + 1
          ReDim Preserve TabIstr.BlkIstr(indEnd)
          TabIstr.BlkIstr(indEnd).idTable = idTabIndPart
          TabIstr.BlkIstr(indEnd).Tavola = nomeTabIndPart
          '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
          testo = testo & CostruisciSelect("T" & indEnd, idTabIndPart)
        End If
        
        testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom(1, indEnd, True)
       
        If Len(nomeTabIndPart) Then
           TabIstr.BlkIstr(indEnd - 1).idTable = idTabIndPart
           TabIstr.BlkIstr(indEnd - 1).Tavola = nomeTabIndPart
           TabIstr.BlkIstr(indEnd).idTable = idSave
           TabIstr.BlkIstr(indEnd).Tavola = tabSave
           'proviamo con la squalificata
           ReDim Preserve TabIstr.BlkIstr(indEnd).KeyDef(1)
           indPart = True
        Else
           indPart = False
        End If
        
        testo = testo & CostruisciWhere(1, indEnd)
        
        If Len(nomeTabIndPart) Then
           'rimette a posto
           indEnd = indEnd - 1
           ReDim Preserve TabIstr.BlkIstr(indEnd)
           TabIstr.BlkIstr(indEnd).idTable = idSave
           TabIstr.BlkIstr(indEnd).Tavola = tabSave
           indPart = False
        End If
        If K > 1 Or flagRed Then
           TabIstr.BlkIstr(indEnd).Tavola = tabSave
           TabIstr.BlkIstr(indEnd).idTable = idSave
        End If
      
        testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        testo = testo & Space(11 + indentGNsq + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf & vbCrLf
      
        If Len(testoRed(K)) Then
          testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
        End If
      Next K
      
      'SQ - Specifico Complet. Squal: Solo sulla ROOT:
      If level = 1 Then
        '''''''''''''''''''''''''''''''''''
        ' SECONDA DECLARE
        '''''''''''''''''''''''''''''''''''
        If Not keyOp Then
          testo = testo & Space(11 + indentGN) & "ELSE" & vbCrLf
          For K = 1 To indRed
            indentRed = 0
            If Len(testoRed(K)) Then
              testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
              indentRed = 3
            End If
            '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
            indentGen = indentGNsq + indentRed
            
            testo = testo & Space(11 + indentGNsq + indentRed) _
                       & "EXEC SQL DECLARE " & CursorName & Format(K, "00") & "GU " & " CURSOR " _
                       & IIf(withold, "WITH HOLD", "") & vbCrLf & Space(11 + indentGNsq + indentRed) & " FOR SELECT " & vbCrLf
       
            indPart = Len(nomeTabIndPart) > 0
            'SP 9/9 pcb dinamico
            testo = testo & CostruisciSelect("T" & indEnd, idRed(K))
            indPart = False
             
            If K > 1 Then
              idSave = TabIstr.BlkIstr(indEnd).idTable
              tabSave = TabIstr.BlkIstr(indEnd).Tavola
              TabIstr.BlkIstr(indEnd).Tavola = nomeTRed(K)
              TabIstr.BlkIstr(indEnd).idTable = idRed(K)
            End If
            'SP 12/9: altro gioco di prestigio per gli indici particolari...  tutto da mettere meglio nella versione definitiva
            If nomeTabIndPart <> "" Then
               idSave = TabIstr.BlkIstr(indEnd).idTable
               tabSave = TabIstr.BlkIstr(indEnd).Tavola
               indEnd = indEnd + 1
               ReDim Preserve TabIstr.BlkIstr(indEnd)
               TabIstr.BlkIstr(indEnd).idTable = idTabIndPart
               TabIstr.BlkIstr(indEnd).Tavola = nomeTabIndPart
              '13/9 doppia select per indici particolari, per risolvere la keyfeedbackarea
               testo = testo & CostruisciSelect("T" & indEnd, idTabIndPart)
            End If
            testo = testo & Space(12 + indentGNsq + indentRed) & "FROM " & vbCrLf & CostruisciFrom(1, indEnd, True)
            'SP 12/9: altro gioco di prestigio per gli indici particolari...
            indPart = False
            If nomeTabIndPart <> "" Then
               TabIstr.BlkIstr(indEnd - 1).idTable = idTabIndPart
               TabIstr.BlkIstr(indEnd - 1).Tavola = nomeTabIndPart
               TabIstr.BlkIstr(indEnd).idTable = idSave
               TabIstr.BlkIstr(indEnd).Tavola = tabSave
               'proviamo con la squalificata
               ReDim Preserve TabIstr.BlkIstr(indEnd).KeyDef(1)
               indPart = True
            End If
            testo = testo & CostruisciWhere(1, indEnd, True)
            If nomeTabIndPart <> "" Then
               'rimette a posto
               indEnd = indEnd - 1
               ReDim Preserve TabIstr.BlkIstr(indEnd)
               TabIstr.BlkIstr(indEnd).idTable = idSave
               TabIstr.BlkIstr(indEnd).Tavola = tabSave
               indPart = False
            End If
            If K > 1 Then
               TabIstr.BlkIstr(indEnd).Tavola = tabSave
               TabIstr.BlkIstr(indEnd).idTable = idSave
            End If
          
            testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
            testo = testo & Space(11 + indentGNsq + indentRed) & "MOVE ZERO TO WK-SQLCODE" & vbCrLf
            If Len(testoRed(K)) Then
              testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
            End If
          Next
          testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf & vbCrLf
          testo = testo & Space(11 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
        Else
           testo = testo & vbCrLf
        End If
      End If 'level 1
      
      For K = 1 To indRed
        indentRed = 0
        If Len(testoRed(K)) Then
           testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
           indentRed = 3
        End If
        testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & vbCrLf
        testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        If Len(testoRed(K)) Then
           testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
        End If
      Next
    
      'SQ?? sotto... testo = testo & Space(11 + indentGN) & "IF SQLCODE = -502" & vbCrLf
      
      'SQ - Specifico Complet. Squal: Solo sulla ROOT:
      If level = 1 Then
        If Not keyOp Then
          testo = testo & Space(11 + indentGN) & "ELSE" & vbCrLf
          For K = 1 To indRed
            indentRed = 0
            If Len(testoRed(K)) Then
              testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
              indentRed = 3
            End If
            testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & "GU" & vbCrLf
            testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
            If Len(testoRed(K)) Then
              testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
            End If
          Next
          testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
          'SQ?? sopra?
          testo = testo & Space(11 + indentGN) & "IF SQLCODE = -502" & vbCrLf
          testo = testo & Space(14 + indentGN) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
        End If
              
        For K = 1 To indRed
          indentRed = 0
          If Len(testoRed(K)) Then
            testo = testo & Space(14 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
            indentRed = 3
          End If
          testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & vbCrLf
          testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
          testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & vbCrLf
          testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
          If Len(testoRed(K)) Then
            testo = testo & Space(14 + indentGNsq) & "END-IF" & vbCrLf
          End If
        Next
  
        'diff
        If Not keyOp Then
          testo = testo & Space(14 + indentGN) & "ELSE" & vbCrLf
          For K = 1 To indRed
            indentRed = 0
            If Len(testoRed(K)) Then
               testo = testo & Space(14 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
               indentRed = 3
            End If
            testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & "GU" & vbCrLf
            testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
            testo = testo & Space(14 + indentGNsq + indentRed) & "EXEC SQL OPEN " & CursorName & Format(K, "00") & "GU" & vbCrLf
            testo = testo & Space(14 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
            If Len(testoRed(K)) Then
               testo = testo & Space(14 + indentGNsq) & "END-IF" & vbCrLf
            End If
          Next
          testo = testo & Space(14 + indentGN) & "END-IF" & vbCrLf
        End If
        testo = testo & Space(11 + indentGN) & "END-IF" & vbCrLf
        testo = testo & Space(11 + indentGN) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
      Else
        testo = testo & Space(11 + indentGNsq) & "IF SQLCODE = -502" & vbCrLf
        testo = testo & Space(11 + indentGNsq) & "   EXEC SQL CLOSE " & CursorName & "01" & vbCrLf
        testo = testo & Space(11 + indentGNsq) & "   END-EXEC" & vbCrLf
        testo = testo & Space(11 + indentGNsq) & "   EXEC SQL OPEN " & CursorName & "01" & vbCrLf
        testo = testo & Space(11 + indentGNsq) & "   END-EXEC" & vbCrLf
        testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
        testo = testo & Space(11 + indentGNsq) & "MOVE SQLCODE TO WK-SQLCODE" & vbCrLf
      End If  'LEVEL 1
      
      testo = testo & Space(11) & "END-IF" & vbCrLf
      testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
      If level = 1 Then
        testo = testo & Space(11) & "   MOVE 'S' TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
        testo = testo & Space(11) & "   MOVE " & GbNumIstr & " TO LNKDB-STKNUMISTR(LNKDB-NUMSTACK)" & vbCrLf
      End If
      If EmbeddedRoutine Then
        testo = testo & Space(11) & "   PERFORM FETCH-" & CursorName & vbCrLf & _
                        Space(11) & "      THRU FETCH-" & CursorName & "-EX" & vbCrLf
      Else
        testo = testo & Space(11) & "   PERFORM 9999-FETCH-" & CursorName & vbCrLf
      End If

      testo = testo & Space(11) & "   MOVE SQLCODE TO WK-SQLCODE" & vbCrLf  'SQ - 29-08
      testo = testo & Space(11) & "END-IF" & vbCrLf & vbCrLf
  
      testo = testo & Space(11) & "IF WK-SQLCODE NOT EQUAL ZERO" & vbCrLf & _
                      Space(11) & "   MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
      If level > 1 Then
        'Se ha FRATELLO: uso FRATELLO
        'Se ha PADRE: uso PADRE
        'Se � ROOT: NESSUNO (non sono entrato: level=1)
        If Len(TabIstr.BlkIstr(level).ssaName) Then
          nomeroutine = TabIstr.BlkIstr(level).ssaName
        Else
          nomeroutine = TabIstr.BlkIstr(level).NewNameSeg
        End If
        testo = testo & Space(11) & "   IF WK-SQLCODE  EQUAL 100" & vbCrLf & _
                        Space(11) & "      MOVE '" & nomeroutine & "' TO WK-SEGMENT" & vbCrLf & _
                        Space(11) & "      PERFORM 9999-ROUT-" & Gistruzione & "-" & nomeroutine & " THRU" & vbCrLf & _
                        Space(11) & "        9999-ROUT-" & Gistruzione & "-" & nomeroutine & "-EX" & vbCrLf & _
                        Space(11) & "   ELSE" & vbCrLf
                        indentGNsq = indentGNsq + 3
      End If
      
      'testo = testo & Space(11) & "   MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)" & vbCrLf
      If Not keyOp Then
        testo = testo & Space(14) & "IF LNKDB-STKTIPO(LNKDB-NUMSTACK) = 'N'" & vbCrLf
      End If
  
      For K = 1 To indRed
        indentRed = 0
        If Len(testoRed(K)) Then
          testo = testo & Space(11 + indentGNsq) & Trim(testoRed(K)) & vbCrLf
          indentRed = 3
        End If
        testo = testo & Space(11 + indentGNsq + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & vbCrLf
        testo = testo & Space(11 + indentGNsq + indentRed) & "END-EXEC" & vbCrLf
        If testoRed(K) <> "" Then
           testo = testo & Space(11 + indentGNsq) & "END-IF" & vbCrLf
        End If
      Next
      'diff
      If Not keyOp Then
        testo = testo & Space(14) & "ELSE" & vbCrLf
        For K = 1 To indRed
           If testoRed(K) <> "" Then
              testo = testo & Space(17) & Trim(testoRed(K)) & vbCrLf
              indentRed = 3
           End If
           testo = testo & Space(17 + indentRed) & "EXEC SQL CLOSE " & CursorName & Format(K, "00") & "GU" & vbCrLf
           testo = testo & Space(17 + indentRed) & "END-EXEC" & vbCrLf
           If testoRed(K) <> "" Then
              testo = testo & Space(17) & "END-IF" & vbCrLf
           End If
        Next
        testo = testo & Space(14) & "END-IF" & vbCrLf
      End If
      
      If level > 1 Then
        testo = testo & Space(11) & "   END-IF" & vbCrLf
        indentGNsq = indentGNsq - 3
      End If
      testo = testo & Space(11) & "END-IF" & vbCrLf & vbCrLf
  '''
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' TESTO FETCH:
      ' Viene scritto in fondo alla routine, in getRoutine_DB2
      ' Se ho pi� livelli, appendo il testo alle fecth dei precedenti
      ' SQ - si pu� isolare?
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'TMP
      GbTestoFetch = getFetch(indRed, indEnd, indentGN, indentGNqu, Not keyOp)
           
      NomeT = TabIstr.BlkIstr(indEnd).Tavola
      NomePk = TabIstr.BlkIstr(indEnd).pKey
      If TabIstr.BlkIstr(indEnd).IdSegm Then
          Dim indGet As Long
        indGet = indEnd
        '?wLiv01Copy = getCopyDLISeg(indGet)
        
        If Not flagRed Then
          For K = 1 To indRed
            If testoRed(K) <> "" Then
               testo = testo & Space(11) & Trim(testoRed(K)) & vbCrLf
               indentRed = 3
            End If
            testo = testo & Space(11 + indentRed) & "IF WK-SQLCODE = ZERO" & vbCrLf
            testo = testo & Space(11 + indentRed) & "   PERFORM 9999-" & Replace(nomeTRed(K), "_", "-") & "-TO-RD" & vbCrLf
            'SQ - Specifico Complet. Squal: SEGLEVEL = 1, SEMPRE!
            testo = testo & Space(11 + indentRed) & "   MOVE RD-" & instrLevel.Segment & " TO LNKDB-DATA-AREA(1)" & vbCrLf
            testo = testo & Space(11 + indentRed) & "   MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE " & vbCrLf
            testo = testo & Space(11 + indentRed) & "   PERFORM 9999-SETK-ADLWKEY" & vbCrLf
            If Len(nomeTabIndPart) Then
                testo = testo & Space(14 + indentRed) & "MOVE '" & nomeTRed(K) & "' TO WK-NAMETABLE" & vbCrLf
            End If
            'SQ D
            testo = testo & Space(14 + indentRed) & "MOVE '" & Format(level, "00") & "' TO LNKDB-PCBSEGLV" & vbCrLf
            testo = testo & Space(11 + indentRed) & "END-IF" & vbCrLf
            If testoRed(K) <> "" Then
               testo = testo & Space(11) & "END-IF" & vbCrLf
            End If
          Next
        End If
        
        testo = testo & vbCrLf
        testo = testo & Space(11) & "IF WK-SQLCODE = ZERO" & vbCrLf
        testo = testo & Space(11) & "   MOVE '" & NomeT & "' TO WK-NAMETABLE" & vbCrLf
        testo = testo & Space(11) & "   PERFORM 11000-STORE-FDBKEY" & vbCrLf
        If keySecFlagUltimoLiv Then
          testo = testo & Space(11) & "   PERFORM 9999-SETA-" & Replace(TabIstr.BlkIstr(indEnd).KeyDef(1).key, "_", "-") & vbCrLf
          testo = testo & Space(11) & "   MOVE KRR-" & TabIstr.BlkIstr(indEnd).KeyDef(1).key & " TO LNKDB-STKPTGKEY(LNKDB-NUMSTACK)" & vbCrLf
          testo = testo & Space(11) & "   MOVE KRDP-" & TabIstr.BlkIstr(indEnd).KeyDef(1).key & "  TO LNKDB-FDBKEY" & vbCrLf
        End If
        testo = testo & Space(11) & "END-IF" & vbCrLf
    End If
    
    testo = testo & Space(11) & "." & vbCrLf & Space(7) & "9999-ROUT-" & Gistruzione & "-" & TabIstr.BlkIstr(jj).Segment & "-EX." & vbCrLf & vbCrLf & GbTestoFetch
    GbTestoFetch = ""
  Next jj
  getGNsq_DB2 = testo
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ - EMBEDDED
' Sostituisce a tutti gli incapsulati che utilizzano tale routine,
' ...
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub embedRoutine(routineName As String, routineText As String, Rtb As RichTextBox)
  Dim rs As Recordset
  Dim fileName As String
  
  On Error GoTo catch
  
  Set rs = m_dllFunctions.Open_Recordset("SELECT DISTINCT a.nome,a.directory_input,a.estensione from bs_oggetti as a,PsDli_IstrCodifica as b WHERE b.codifica='" & routineName & "' AND nomeRout='<EMBEDDED>' And A.IdOggetto = b.IdOggetto")
  While Not rs.EOF
    fileName = m_dllFunctions.FnPathDef & Mid(rs!Directory_Input, 2) & "\out\" & rs!nome
    If Len(Trim(rs!Estensione)) Then
      fileName = fileName & "." & rs!Estensione
    End If
    '
    Rtb.LoadFile fileName
    changeTag Rtb, "<" & routineName & ">", routineText
    Rtb.SaveFile fileName, 1
    
    rs.MoveNext
  Wend
  rs.Close
  Exit Sub
catch:
  If Err.Number = 75 Then
    'File not found
    'Scrivere nella finestra!
    m_dllFunctions.WriteLog "##embedRoutine: FILE NOT FOUND: " & fileName
  Else
    m_dllFunctions.WriteLog "##embedRoutine: " & routineName & ": " & Err.Description
    'Scrivere nella finestra!
  End If
End Sub
Private Function getDbd_tabelle(IdDBD As Long) As String
  Dim r As ADODB.Recordset
  On Error GoTo errordB

  Set r = m_dllFunctions.Open_Recordset("SELECT nome from bs_oggetti where idOggetto = " & IdDBD)
  If r.RecordCount Then
    getDbd_tabelle = r(0)
  Else
    getDbd_tabelle = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox Err.Description
  getDbd_tabelle = ""
End Function
'''''
'''''Private Sub createTrigger_ISRT(nomeLChild As String, nomeLParent As String, idLChild As Double, idLParent As Double)
'''''  Dim rsKeyChild As Recordset
'''''  Dim rsKeyParent As Recordset
'''''  Dim RTB_Trig As RichTextBox
'''''  Dim percorso As String
'''''  Dim lp_Key_Col_Dec As String, lp_Key_Column As String, lp_Key_Col_Into As String
'''''  Dim lc_Key_Col_Curs As String, lc_Key_Col_Open As String
'''''  Dim where_LPKey As String, where_LCKey As String, where_Cond As String
'''''  Dim i As Long
'''''  Dim Par_Child As Boolean, Par_Parent As Boolean
'''''
'''''  On Error GoTo ErrorHandler
'''''  ' Logical Parent: S140 - IDTable: 3347
'''''  ' Logical Child:  S139 - IDTable: 3231
'''''
'''''  Set RTB_Trig = MaVSAMF_GenRout.RTB_Trig
'''''  percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\input-prj\template\imsdb\standard\routines\ISRT_LC"
'''''  RTB_Trig.LoadFile percorso
'''''
'''''  ' Creator
'''''  changeTag RTB_Trig, "<creator>", IIf(Len(TabIstr.BlkIstr(1).tableCreator), TabIstr.BlkIstr(1).tableCreator & ".", "")
'''''
'''''  ' nome TbLChild
'''''  changeTag RTB_Trig, "<TABLE-NAME>", nomeLChild ' S139
'''''
'''''  ' nome TbLParent
'''''  changeTag RTB_Trig, "<LP-TABLE-NAME>", nomeLParent ' S140
'''''
'''''  ' Elenco Campi chiave TbLParent
'''''  lp_Key_Col_Dec = ""
'''''  lp_Key_Column = ""
'''''  lp_Key_Col_Into = ""
'''''  where_LPKey = ""
'''''  i = 1
'''''  Par_Parent = True
'''''  Set rsKeyParent = m_dllFunctions.Open_Recordset("select c.nome, c.tipo from " & DB & "Index as a, " & _
'''''                                                                                   DB & "IdxCol as b, " & _
'''''                                                                                   DB & "colonne as c where " & _
'''''                                                  "a.idtable = " & idLParent & " and a.idindex = b.idindex and " & _
'''''                                                  "b.idcolonna = c.idcolonna " & _
'''''                                                  "order by c.ordinale")
'''''  Do Until rsKeyParent.EOF
'''''    If i = rsKeyParent.RecordCount Then
'''''      lp_Key_Col_Dec = lp_Key_Col_Dec & "H_" & rsKeyParent!nome & Space(3) & rsKeyParent!Tipo & ";"
'''''      lp_Key_Column = lp_Key_Column & rsKeyParent!nome
'''''      lp_Key_Col_Into = lp_Key_Col_Into & "H_" & rsKeyParent!nome
'''''      where_LPKey = where_LPKey & rsKeyParent!nome
'''''    Else
'''''      lp_Key_Col_Dec = lp_Key_Col_Dec & "H_" & rsKeyParent!nome & Space(3) & rsKeyParent!Tipo & ";" & vbCrLf
'''''      lp_Key_Column = lp_Key_Column & rsKeyParent!nome & ","
'''''      lp_Key_Col_Into = lp_Key_Col_Into & "H_" & rsKeyParent!nome & ","
'''''      where_LPKey = where_LPKey & rsKeyParent!nome & "||"
'''''    End If
'''''    rsKeyParent.MoveNext
'''''    i = i + 1
'''''  Loop
'''''  If rsKeyParent.RecordCount = 1 Then
'''''    Par_Parent = False
'''''  End If
'''''  rsKeyParent.Close
'''''
'''''  ' Elenco Campi chiave TbLChild
'''''  lc_Key_Col_Curs = ""
'''''  lc_Key_Col_Open = ""
'''''  where_LCKey = ""
'''''  i = 1
'''''  Par_Child = True
'''''  Set rsKeyChild = m_dllFunctions.Open_Recordset("select c.nome, c.tipo from " & DB & "Index as a, " & _
'''''                                                                                   DB & "IdxCol as b, " & _
'''''                                                                                   DB & "colonne as c where " & _
'''''                                                  "a.idtable = " & idLChild & " and a.idindex = b.idindex and " & _
'''''                                                  "b.idcolonna = c.idcolonna and a.tipo = 'P' and " & _
'''''                                                  "c.idtable = c.idtablejoin " & _
'''''                                                  "order by c.ordinale")
'''''  Do Until rsKeyChild.EOF
'''''    If i = rsKeyChild.RecordCount Then
'''''      lc_Key_Col_Curs = lc_Key_Col_Curs & rsKeyChild!nome & " " & rsKeyChild!Tipo
'''''      lc_Key_Col_Open = lc_Key_Col_Open & ":new." & rsKeyChild!nome
'''''      where_LCKey = where_LCKey & rsKeyChild!nome
'''''    Else
'''''      lc_Key_Col_Curs = lc_Key_Col_Curs & rsKeyChild!nome & " " & rsKeyChild!Tipo & ","
'''''      lc_Key_Col_Open = lc_Key_Col_Open & ":new." & rsKeyChild!nome & ","
'''''      where_LCKey = where_LCKey & rsKeyChild!nome & "||"
'''''    End If
'''''    rsKeyChild.MoveNext
'''''    i = i + 1
'''''  Loop
'''''  If rsKeyChild.RecordCount = 1 Then
'''''    Par_Child = False
'''''  End If
'''''  rsKeyChild.Close
'''''
'''''  ' Gestione <WHERE-CONDITION>
'''''  If Par_Parent Then
'''''    where_LPKey = "(" & where_LPKey & ")"
'''''  End If
'''''
'''''  If Par_Child Then
'''''    where_LCKey = "(" & where_LCKey & ")"
'''''  End If
'''''
'''''  where_Cond = where_LPKey & " = " & where_LCKey
'''''
'''''  changeTag RTB_Trig, "<LP-KEY-COLUMNS-DECLARE>", lp_Key_Col_Dec
'''''  changeTag RTB_Trig, "<LC-KEY-COLUMNS-CURS>", lc_Key_Col_Curs
'''''  changeTag RTB_Trig, "<LP-KEY-COLUMNS>", lp_Key_Column
'''''  changeTag RTB_Trig, "<WHERE-CONDITION>", where_Cond
'''''  changeTag RTB_Trig, "<LC-KEY-COLUMNS-OPEN>", lc_Key_Col_Open
'''''  changeTag RTB_Trig, "<LP-KEY-COLUMNS-INTO>", lp_Key_Col_Into
'''''
'''''  If DB = "DMDB2_" Then
'''''    percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\sql\trigger\"
'''''  Else
'''''    percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\oracle\sql\trigger\"
'''''  End If
'''''
'''''  RTB_Trig.SaveFile percorso & "S139_ISRT_LC.sql", 1
'''''
'''''  Exit Sub
'''''ErrorHandler:
'''''  If Err.Number = 75 Then
'''''    MkDir percorso
'''''    Resume
'''''  Else
'''''    Err.Raise Err.Number, Err.Source, Err.Description
'''''  End If
'''''End Sub
'''''
'''''' Routine per confrontare i campi di 2 tabelle
'''''Private Function compareTables(idTb1 As Double, idTb2 As Double) As cmpCol()
'''''  Dim rsTb1 As Recordset, rsTb2 As Recordset
'''''  Dim cmpTb() As cmpCol
'''''  Dim i As Long
'''''  Dim LenRemainCmp1 As Long, LenRemainCmp2 As Long
'''''  Dim ordinaleTb1 As Long, ordinaleTb2 As Long
'''''  Dim wIndex As Boolean
'''''
'''''  On Error GoTo ErrorHandler
'''''
'''''  Dim numFile As Integer
'''''
'''''  numFile = FreeFile
'''''  Open VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\Output-prj\Compare_" & idTb1 & "_" & idTb2 & ".txt" For Output As numFile
'''''
'''''  Set rsTb1 = m_dllFunctions.Open_Recordset("select * from " & DB & "colonne where idtable = " & idTb1 & " order by ordinale")
'''''  Set rsTb2 = m_dllFunctions.Open_Recordset("select * from " & DB & "colonne where idtable = " & idTb2 & " order by ordinale")
'''''
'''''  i = 0
'''''  ReDim cmpTb(i)
'''''  LenRemainCmp1 = 0
'''''  LenRemainCmp2 = 0
'''''  ordinaleTb1 = 0
'''''  ordinaleTb2 = 0
'''''  wIndex = True
'''''  Do Until rsTb2.EOF Or rsTb1!idTable = rsTb1!idtablejoin
'''''
'''''    If rsTb2!idTable = rsTb2!idtablejoin Then
'''''      ' controllo tipo CMP
'''''      If (rsTb1!Tipo = "CHAR" Or rsTb1!Tipo = "VARCHAR") And _
'''''         (rsTb2!Tipo = "CHAR" Or rsTb2!Tipo = "VARCHAR") Then
'''''        If LenRemainCmp1 = 0 And LenRemainCmp2 = 0 Then       ' non ho resto dalla compare precedente
'''''          If rsTb1!Lunghezza = rsTb2!Lunghezza Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza > rsTb2!Lunghezza Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = rsTb1!Lunghezza - rsTb2!Lunghezza
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza < rsTb2!Lunghezza Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = rsTb2!Lunghezza - rsTb1!Lunghezza
'''''            rsTb1.MoveNext
'''''          End If
'''''        ElseIf LenRemainCmp1 > 0 And LenRemainCmp2 = 0 Then       ' il Cmp1 precedente era + grande
'''''          If rsTb2!Lunghezza = LenRemainCmp1 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = 0
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb2!Lunghezza < LenRemainCmp1 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = LenRemainCmp1 - rsTb2!Lunghezza
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb2!Lunghezza > LenRemainCmp1 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = rsTb2!Lunghezza - LenRemainCmp1
'''''            LenRemainCmp1 = 0
'''''            rsTb1.MoveNext
'''''          End If
'''''        ElseIf LenRemainCmp1 = 0 And LenRemainCmp2 > 0 Then       ' il Cmp2 precedente era + grande
'''''          If rsTb1!Lunghezza = LenRemainCmp2 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = 0
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza > LenRemainCmp2 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = rsTb1!Lunghezza - LenRemainCmp2
'''''            LenRemainCmp2 = 0
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza < LenRemainCmp2 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = LenRemainCmp2 - rsTb1!Lunghezza
'''''            rsTb1.MoveNext
'''''          End If
'''''        End If
'''''      Else
'''''        ' controllo che i campi abbiano lo stesso tipo <> da CHAR o VARCHAR
'''''        If rsTb1!Tipo = rsTb2!Tipo Then
'''''          AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''          rsTb1.MoveNext
'''''          rsTb2.MoveNext
'''''        Else
'''''          ' Segnalazione: i tipi  sono diversi
'''''          Print #numFile, "CMP1: " & rsTb1!nome & " from TABLE1: " & idTb1 & "| CMP2: " & rsTb2!nome & " from TABLE2: " & idTb2
'''''          If LenRemainCmp1 > 0 Then
'''''
'''''          ElseIf LenRemainCmp2 > 0 Then
'''''          Else
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          End If
'''''        End If
'''''      End If
'''''    Else
'''''      rsTb2.MoveNext
'''''    End If
'''''  Loop
'''''
'''''  If Not (rsTb2.EOF) Then
'''''    ordinaleTb2 = rsTb2!ordinale
'''''  End If
'''''
'''''  rsTb1.MoveFirst
'''''  rsTb2.MoveFirst
'''''
'''''  Do Until rsTb1.EOF Or rsTb2!idTable = rsTb2!idtablejoin
'''''    If rsTb1!idTable = rsTb1!idtablejoin Then
'''''      ' controllo tipo CMP
'''''      If (rsTb1!Tipo = "CHAR" Or rsTb1!Tipo = "VARCHAR") And _
'''''         (rsTb2!Tipo = "CHAR" Or rsTb2!Tipo = "VARCHAR") Then
'''''        If LenRemainCmp1 = 0 And LenRemainCmp2 = 0 Then       ' non ho resto dalla compare precedente
'''''          If rsTb1!Lunghezza = rsTb2!Lunghezza Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza > rsTb2!Lunghezza Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = rsTb1!Lunghezza - rsTb2!Lunghezza
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza < rsTb2!Lunghezza Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = rsTb2!Lunghezza - rsTb1!Lunghezza
'''''            rsTb1.MoveNext
'''''          End If
'''''        ElseIf LenRemainCmp1 > 0 And LenRemainCmp2 = 0 Then       ' il Cmp1 precedente era + grande
'''''          If rsTb2!Lunghezza = LenRemainCmp1 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = 0
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb2!Lunghezza < LenRemainCmp1 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = LenRemainCmp1 - rsTb2!Lunghezza
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb2!Lunghezza > LenRemainCmp1 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = rsTb2!Lunghezza - LenRemainCmp1
'''''            LenRemainCmp1 = 0
'''''            rsTb1.MoveNext
'''''          End If
'''''        ElseIf LenRemainCmp1 = 0 And LenRemainCmp2 > 0 Then       ' il Cmp2 precedente era + grande
'''''          If rsTb1!Lunghezza = LenRemainCmp2 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = 0
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza > LenRemainCmp2 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = rsTb1!Lunghezza - LenRemainCmp2
'''''            LenRemainCmp2 = 0
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza < LenRemainCmp2 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = LenRemainCmp2 - rsTb1!Lunghezza
'''''            rsTb1.MoveNext
'''''          End If
'''''        End If
'''''      Else
'''''        ' controllo che i campi abbiano lo stesso tipo <> da CHAR o VARCHAR
'''''        If rsTb1!Tipo = rsTb2!Tipo Then
'''''          AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''          rsTb1.MoveNext
'''''          rsTb2.MoveNext
'''''        Else
'''''          ' Segnalazione: i tipi  sono diversi
'''''          Print #numFile, "CMP1: " & rsTb1!nome & " from TABLE1: " & idTb1 & " |CMP2: " & rsTb2!nome & " from TABLE2: " & idTb2
'''''          If LenRemainCmp1 > 0 Then
'''''
'''''          ElseIf LenRemainCmp2 > 0 Then
'''''          Else
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          End If
'''''        End If
'''''      End If
'''''    Else
'''''      rsTb1.MoveNext
'''''    End If
'''''  Loop
'''''
'''''  If Not (rsTb1.EOF) Then
'''''    ordinaleTb1 = rsTb1!ordinale
'''''  End If
'''''
'''''  rsTb1.Close
'''''  rsTb2.Close
'''''
'''''  If ordinaleTb1 > 0 Or ordinaleTb2 > 0 Then
'''''
'''''    Set rsTb1 = m_dllFunctions.Open_Recordset("select * from " & DB & "colonne where " & _
'''''                          "idtable = " & idTb1 & " and ordinale >= " & ordinaleTb1 & _
'''''                          " order by ordinale")
'''''    Set rsTb2 = m_dllFunctions.Open_Recordset("select * from " & DB & "colonne where " & _
'''''                          "idtable = " & idTb2 & " and ordinale >= " & ordinaleTb2 & _
'''''                          " order by ordinale")
'''''
'''''    LenRemainCmp1 = 0
'''''    LenRemainCmp2 = 0
'''''    wIndex = False
'''''    Do Until rsTb2.EOF Or rsTb1.EOF
'''''      ' controllo tipo CMP
'''''      If (rsTb1!Tipo = "CHAR" Or rsTb1!Tipo = "VARCHAR") And _
'''''         (rsTb2!Tipo = "CHAR" Or rsTb2!Tipo = "VARCHAR") Then
'''''        If LenRemainCmp1 = 0 And LenRemainCmp2 = 0 Then       ' non ho resto dalla compare precedente
'''''          If rsTb1!Lunghezza = rsTb2!Lunghezza Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza > rsTb2!Lunghezza Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = rsTb1!Lunghezza - rsTb2!Lunghezza
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza < rsTb2!Lunghezza Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = rsTb2!Lunghezza - rsTb1!Lunghezza
'''''            rsTb1.MoveNext
'''''          End If
'''''        ElseIf LenRemainCmp1 > 0 And LenRemainCmp2 = 0 Then       ' il Cmp1 precedente era + grande
'''''          If rsTb2!Lunghezza = LenRemainCmp1 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = 0
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb2!Lunghezza < LenRemainCmp1 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = LenRemainCmp1 - rsTb2!Lunghezza
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb2!Lunghezza > LenRemainCmp1 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = rsTb2!Lunghezza - LenRemainCmp1
'''''            LenRemainCmp1 = 0
'''''            rsTb1.MoveNext
'''''          End If
'''''        ElseIf LenRemainCmp1 = 0 And LenRemainCmp2 > 0 Then       ' il Cmp2 precedente era + grande
'''''          If rsTb1!Lunghezza = LenRemainCmp2 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = 0
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza > LenRemainCmp2 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp1 = rsTb1!Lunghezza - LenRemainCmp2
'''''            LenRemainCmp2 = 0
'''''            rsTb2.MoveNext
'''''          ElseIf rsTb1!Lunghezza < LenRemainCmp2 Then
'''''            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''            LenRemainCmp2 = LenRemainCmp2 - rsTb1!Lunghezza
'''''            rsTb1.MoveNext
'''''          End If
'''''        End If
'''''      Else
'''''        ' controllo che i campi abbiano lo stesso tipo <> da CHAR o VARCHAR
'''''        If rsTb1!Tipo = rsTb2!Tipo Then
'''''          AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
'''''          rsTb1.MoveNext
'''''          rsTb2.MoveNext
'''''        Else
'''''          ' Segnalazione: i tipi  sono diversi
'''''          Print #numFile, "CMP1: " & rsTb1!nome & " from TABLE1: " & idTb1 & "| CMP2: " & rsTb2!nome & " from TABLE2: " & idTb2
'''''          If LenRemainCmp1 > 0 Then
'''''
'''''          ElseIf LenRemainCmp2 > 0 Then
'''''          Else
'''''            rsTb1.MoveNext
'''''            rsTb2.MoveNext
'''''          End If
'''''        End If
'''''      End If
'''''    Loop
'''''
'''''    rsTb1.Close
'''''    rsTb2.Close
'''''  End If
'''''  Dim compare As String
'''''  For i = 1 To UBound(cmpTb)
'''''    compare = compare & cmpTb(i).nomeCmp1 & Space(30 - Len(cmpTb(i).nomeCmp1)) & " | " & Format(cmpTb(i).lenCmp1, "00") & "|" & _
'''''                        cmpTb(i).nomeCmp2 & Space(30 - Len(cmpTb(i).nomeCmp2)) & " | " & Format(cmpTb(i).lenCmp2, "00") & vbCrLf
'''''  Next i
'''''
'''''  Print #numFile, compare
'''''
'''''  Close numFile
'''''
'''''  compareTables = cmpTb
'''''  Exit Function
'''''ErrorHandler:
'''''  If Err.Number Then
'''''    Close numFile
'''''    Err.Raise Err.Number, Err.Source, Err.Description
'''''  Else
'''''  End If
'''''End Function
'''''
'''''Sub AggiornaCmpTb(rsTb1 As Recordset, rsTb2 As Recordset, cmpTb() As cmpCol, i As Long, wIndex As Boolean)
'''''  i = i + 1
'''''  ReDim Preserve cmpTb(i)
'''''  cmpTb(i).nomeCmp1 = rsTb1!nome
'''''  cmpTb(i).lenCmp1 = rsTb1!Lunghezza
'''''  cmpTb(i).Tipo = rsTb1!Tipo
'''''  cmpTb(i).Index = wIndex
'''''  cmpTb(i).nomeCmp2 = rsTb2!nome
'''''  cmpTb(i).lenCmp2 = rsTb2!Lunghezza
'''''End Sub
'''''
'''''' Mauro 02-04-2007 : Gestione LogicalParent
'''''Private Sub CreateTrigger(isrtlevel As Integer)
'''''  Dim rsLChld As Recordset
'''''  Dim rsLP_DBD As Recordset
'''''  Dim idPairTable As Double
'''''  Dim idLParent As Double
'''''  Dim idTable1 As Double, idTable2 As Double
'''''  Dim CompareCmp() As cmpCol
'''''  If TabIstr.BlkIstr(isrtlevel).LParent <> "" Then
'''''    ' Mauro 02-04-2007 : Cerco il segmento PAIRED associato a questo
'''''    Set rsLP_DBD = m_dllFunctions.Open_Recordset("select idoggetto from bs_oggetti where nome = '" & TabIstr.BlkIstr(isrtlevel).LDBD & "'")
'''''    ' dovrebbe essere solo 1
'''''    If Not rsLP_DBD.EOF Then
'''''      Set rsLChld = m_dllFunctions.Open_Recordset( _
'''''                    "select c.idtable, b.idsegmento from PSDLI_LChild as a, PSDLi_Segmenti as b, " & DB & "tabelle as c where " & _
'''''                    "a.dbdname = '" & TabIstr.BlkIstr(isrtlevel).LDBD & "' and " & _
'''''                    "a.nomesegmento = '" & TabIstr.BlkIstr(isrtlevel).LParent & "' and " & _
'''''                    "a.lc = '" & TabIstr.BlkIstr(isrtlevel).Segment & "' and " & _
'''''                    "a.lc_dbd = '" & TabIstr.dbd & "' and " & _
'''''                    "a.pair = b.nome and " & _
'''''                    "b.idsegmento = c.idorigine and " & _
'''''                    "b.idoggetto = " & rsLP_DBD!idOggetto)
'''''      If Not rsLChld.EOF Then
'''''        idPairTable = rsLChld!idTable
'''''        idLParent = rsLChld!idSegmento
'''''      End If
'''''      rsLChld.Close
'''''    End If
'''''    rsLP_DBD.Close
'''''  End If
'''''  If idPairTable > 0 Then
'''''    ' Mauro 29-03-2007 : Routine per confrontare i campi di 2 tabelle idTb1 e idTb2
'''''    'idTable1 = TabIstr.BlkIstr(isrtlevel).idTable
'''''    'idTable2 = idPairTable
'''''    idTable1 = 3230
'''''    idTable2 = 3263
'''''
'''''    If idTable1 <> 0 And idTable2 <> 0 Then
'''''      CompareCmp() = compareTables(idTable1, idTable2)
'''''    End If
'''''
'''''    ' Mauro 29-03-2007 : Routine per la creazione del trigger d'inserimento
'''''''' createTrigger_ISRT TabIstr.BlkIstr(isrtLevel).Segment, TabIstr.BlkIstr(isrtLevel).LParent, _
''''''''                    TabIstr.BlkIstr(isrtLevel).IdSegm, idLParent
'''''
'''''
'''''    ' Mauro 06-04-2007 : Routine per aggiornare la tabella Paired dell'LChild
'''''    insertPairedTable idTable1, idTable2, CompareCmp()
'''''
'''''    ' Mauro 10-04-2007 : Routine per la creazione del trigger d'aggiornamento
'''''''' createTrigger_ISRT TabIstr.BlkIstr(isrtLevel).Segment, TabIstr.BlkIstr(isrtLevel).LParent, _
''''''''                    TabIstr.BlkIstr(isrtLevel).IdSegm, idLParent
'''''
'''''    ' Mauro 06-04-2007 : Routine per aggiornare la tabella Paired dell'LChild
'''''    updatePairedTable idTable1, idTable2, CompareCmp()
'''''
'''''  End If
'''''End Sub
'''''
'''''Private Sub updatePairedTable(idTb1 As Double, idTb2 As Double, arrCmp() As cmpCol)
'''''Dim RTB_Trig As RichTextBox
'''''Dim percorso As String
'''''Dim rsTableSet As Recordset
'''''Dim nomeTableSet As String
'''''Dim whereCond As String
'''''Dim i As Long
'''''Dim cmpGroup As String, cmpSingle As String
'''''Dim cmpSet As String, cmpCobol As String
'''''
'''''  On Error GoTo ErrorHandler
'''''
'''''  Set RTB_Trig = MaVSAMF_GenRout.RTB_Trig
'''''  percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\input-prj\template\imsdb\standard\routines\UPDT_PAIR"
'''''  RTB_Trig.LoadFile percorso
'''''
'''''  Set rsTableSet = m_dllFunctions.Open_Recordset("select nome from " & DB & "Tabelle where idtable = " & idTb2)
'''''  ' Dovrebbe essere solo 1
'''''  If rsTableSet.RecordCount Then
'''''    nomeTableSet = rsTableSet!nome
'''''  Else
'''''    nomeTableSet = ""
'''''  End If
'''''  rsTableSet.Close
'''''
'''''  changeTag RTB_Trig, "<SET-TABLE>", Space(14) & nomeTableSet
'''''
'''''  ' WHERE CONDITION
'''''  whereCond = ""
'''''  For i = 1 To UBound(arrCmp)
'''''    If arrCmp(i).Index Then
'''''      If arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Then
'''''        whereCond = IIf(Len(Trim(whereCond)), whereCond & " AND " & vbCrLf & Space(14) & arrCmp(i).nomeCmp1 & " = " & arrCmp(i).nomeCmp2, Space(14) & arrCmp(i).nomeCmp1 & " = " & arrCmp(i).nomeCmp2)
'''''      Else
'''''        cmpGroup = ""
'''''        cmpSingle = ""
'''''        If arrCmp(i).lenCmp1 > arrCmp(i).lenCmp2 Then
'''''          cmpSingle = arrCmp(i).nomeCmp1
'''''          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp)
'''''            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & arrCmp(i).nomeCmp2, arrCmp(i).nomeCmp2)
'''''            i = i + 1
'''''          Loop
'''''        Else
'''''          cmpSingle = arrCmp(i).nomeCmp2
'''''          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp)
'''''            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & arrCmp(i).nomeCmp1, arrCmp(i).nomeCmp1)
'''''            i = i + 1
'''''          Loop
'''''        End If
'''''        whereCond = IIf(Len(Trim(whereCond)), whereCond & " AND " & vbCrLf & Space(14) & cmpSingle & " = " & cmpGroup, Space(14) & cmpSingle & " = " & cmpGroup)
'''''        If i < UBound(arrCmp) Then
'''''          whereCond = whereCond & " AND " & vbCrLf & Space(14) & arrCmp(i).nomeCmp1 & " = " & arrCmp(i).nomeCmp2
'''''        End If
'''''      End If
'''''    End If
'''''  Next i
'''''
'''''  changeTag RTB_Trig, "<WHERE-FIELDS>", whereCond
'''''
'''''  cmpSet = ""
'''''  For i = 1 To UBound(arrCmp)
'''''    If Not arrCmp(i).Index Then
'''''      cmpCobol = ":new." & arrCmp(i).nomeCmp1
'''''      If arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Then
'''''        cmpSet = IIf(Len(Trim(cmpSet)), cmpSet & ", " & vbCrLf & Space(14) & cmpCobol & " = " & arrCmp(i).nomeCmp2, Space(14) & cmpCobol & " = " & arrCmp(i).nomeCmp2)
'''''      Else
'''''        cmpGroup = ""
'''''        cmpSingle = ""
'''''        If arrCmp(i).lenCmp1 > arrCmp(i).lenCmp2 Then
'''''          cmpSingle = cmpCobol
'''''          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp)
'''''            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & arrCmp(i).nomeCmp2, arrCmp(i).nomeCmp2)
'''''            i = i + 1
'''''          Loop
'''''        Else
'''''          cmpSingle = arrCmp(i).nomeCmp2
'''''          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp)
'''''            cmpCobol = ":new." & arrCmp(i).nomeCmp1
'''''            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & cmpCobol, cmpCobol)
'''''            i = i + 1
'''''          Loop
'''''        End If
'''''        cmpSet = IIf(Len(Trim(cmpSet)), cmpSet & " AND " & vbCrLf & Space(14) & cmpSingle & " = " & cmpGroup, Space(14) & cmpSingle & " = " & cmpGroup)
'''''        If i < UBound(arrCmp) Then
'''''          cmpCobol = ":new." & arrCmp(i).nomeCmp1
'''''          cmpSet = cmpSet & " AND " & vbCrLf & Space(14) & cmpCobol & " = " & arrCmp(i).nomeCmp2
'''''        End If
'''''      End If
'''''    End If
'''''  Next i
'''''
'''''  changeTag RTB_Trig, "<SET-FIELDS>", cmpSet
'''''
'''''  If DB = "DMDB2_" Then
'''''    percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\sql\trigger\"
'''''  Else
'''''    percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\oracle\sql\trigger\"
'''''  End If
'''''
'''''  RTB_Trig.SaveFile percorso & nomeTableSet & "UPDT_PAIR.sql", 1
'''''
'''''  Exit Sub
'''''ErrorHandler:
'''''  If Err.Number = 75 Then
'''''    MkDir percorso
'''''    Resume
'''''  Else
'''''    Err.Raise Err.Number, Err.Source, Err.Description
'''''  End If
'''''End Sub
'''''
'''''Private Sub insertPairedTable(idTb1 As Double, idTb2 As Double, arrCmp() As cmpCol)
'''''Dim RTB_Trig As RichTextBox
'''''Dim percorso As String
'''''Dim rsTableSet As Recordset
'''''Dim nomeTableSet As String
'''''Dim whereCond As String
'''''Dim i As Long
'''''Dim cmpGroup As String, cmpSingle As String
'''''Dim cmpSet As String, cmpCobol As String
'''''
'''''  On Error GoTo ErrorHandler
'''''
'''''  Set RTB_Trig = MaVSAMF_GenRout.RTB_Trig
'''''  percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\input-prj\template\imsdb\standard\routines\ISRT_PAIR"
'''''  RTB_Trig.LoadFile percorso
'''''
'''''''''  Set rsTableSet = m_dllFunctions.Open_Recordset("select nome from " & DB & "Tabelle where idtable = " & idTb2)
'''''''''  ' Dovrebbe essere solo 1
'''''''''  If rsTableSet.RecordCount Then
'''''''''    nomeTableSet = rsTableSet!nome
'''''''''  Else
'''''''''    nomeTableSet = ""
'''''''''  End If
'''''''''  rsTableSet.Close
'''''''''
'''''''''  changeTag RTB_Trig, "<SET-TABLE>", Space(14) & nomeTableSet
'''''''''
'''''''''  ' WHERE CONDITION
'''''''''  whereCond = ""
'''''''''  For i = 1 To UBound(arrCmp)
'''''''''    If arrCmp(i).Index Then
'''''''''      If arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Then
'''''''''        whereCond = IIf(Len(Trim(whereCond)), whereCond & " AND " & vbCrLf & Space(14) & arrCmp(i).nomeCmp1 & " = " & arrCmp(i).nomeCmp2, Space(14) & arrCmp(i).nomeCmp1 & " = " & arrCmp(i).nomeCmp2)
'''''''''      Else
'''''''''        cmpGroup = ""
'''''''''        cmpSingle = ""
'''''''''        If arrCmp(i).lenCmp1 > arrCmp(i).lenCmp2 Then
'''''''''          cmpSingle = arrCmp(i).nomeCmp1
'''''''''          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp)
'''''''''            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & arrCmp(i).nomeCmp2, arrCmp(i).nomeCmp2)
'''''''''            i = i + 1
'''''''''          Loop
'''''''''        Else
'''''''''          cmpSingle = arrCmp(i).nomeCmp2
'''''''''          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp)
'''''''''            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & arrCmp(i).nomeCmp1, arrCmp(i).nomeCmp1)
'''''''''            i = i + 1
'''''''''          Loop
'''''''''        End If
'''''''''        whereCond = IIf(Len(Trim(whereCond)), whereCond & " AND " & vbCrLf & Space(14) & cmpSingle & " = " & cmpGroup, Space(14) & cmpSingle & " = " & cmpGroup)
'''''''''        If i < UBound(arrCmp) Then
'''''''''          whereCond = whereCond & " AND " & vbCrLf & Space(14) & arrCmp(i).nomeCmp1 & " = " & arrCmp(i).nomeCmp2
'''''''''        End If
'''''''''      End If
'''''''''    End If
'''''''''  Next i
'''''''''
'''''''''  changeTag RTB_Trig, "<WHERE-FIELDS>", whereCond
'''''''''
'''''''''  cmpSet = ""
'''''''''  For i = 1 To UBound(arrCmp)
'''''''''    If Not arrCmp(i).Index Then
'''''''''      cmpCobol = ":new." & arrCmp(i).nomeCmp1
'''''''''      If arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Then
'''''''''        cmpSet = IIf(Len(Trim(cmpSet)), cmpSet & ", " & vbCrLf & Space(14) & cmpCobol & " = " & arrCmp(i).nomeCmp2, Space(14) & cmpCobol & " = " & arrCmp(i).nomeCmp2)
'''''''''      Else
'''''''''        cmpGroup = ""
'''''''''        cmpSingle = ""
'''''''''        If arrCmp(i).lenCmp1 > arrCmp(i).lenCmp2 Then
'''''''''          cmpSingle = cmpCobol
'''''''''          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp)
'''''''''            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & arrCmp(i).nomeCmp2, arrCmp(i).nomeCmp2)
'''''''''            i = i + 1
'''''''''          Loop
'''''''''        Else
'''''''''          cmpSingle = arrCmp(i).nomeCmp2
'''''''''          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp)
'''''''''            cmpCobol = ":new." & arrCmp(i).nomeCmp1
'''''''''            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & cmpCobol, cmpCobol)
'''''''''            i = i + 1
'''''''''          Loop
'''''''''        End If
'''''''''        cmpSet = IIf(Len(Trim(cmpSet)), cmpSet & " AND " & vbCrLf & Space(14) & cmpSingle & " = " & cmpGroup, Space(14) & cmpSingle & " = " & cmpGroup)
'''''''''        If i < UBound(arrCmp) Then
'''''''''          cmpCobol = ":new." & arrCmp(i).nomeCmp1
'''''''''          cmpSet = cmpSet & " AND " & vbCrLf & Space(14) & cmpCobol & " = " & arrCmp(i).nomeCmp2
'''''''''        End If
'''''''''      End If
'''''''''    End If
'''''''''  Next i
'''''''''
'''''''''  changeTag RTB_Trig, "<SET-FIELDS>", cmpSet
'''''
'''''  If DB = "DMDB2_" Then
'''''    percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\db2\sql\trigger\"
'''''  Else
'''''    percorso = VSAM2Rdbms.drPathDef & "\" & Left(VSAM2Rdbms.drNomeDB, InStr(VSAM2Rdbms.drNomeDB, ".") - 1) & "\output-prj\oracle\sql\trigger\"
'''''  End If
'''''
'''''  RTB_Trig.SaveFile percorso & nomeTableSet & "ISRT_PAIR.sql", 1
'''''
'''''  Exit Sub
'''''ErrorHandler:
'''''  If Err.Number = 75 Then
'''''    MkDir percorso
'''''    Resume
'''''  Else
'''''    Err.Raise Err.Number, Err.Source, Err.Description
'''''  End If
'''''End Sub
