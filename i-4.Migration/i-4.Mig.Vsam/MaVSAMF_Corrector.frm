VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form MaVSAMF_Corrector 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "VSAM Instructions corrector"
   ClientHeight    =   7485
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   10995
   Icon            =   "MaVSAMF_Corrector.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7485
   ScaleWidth      =   10995
   ShowInTaskbar   =   0   'False
   Begin TabDlg.SSTab SSTab1 
      Height          =   3495
      Left            =   6690
      TabIndex        =   6
      Top             =   420
      Width           =   4275
      _ExtentX        =   7541
      _ExtentY        =   6165
      _Version        =   393216
      TabOrientation  =   1
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      Enabled         =   0   'False
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Level details"
      TabPicture(0)   =   "MaVSAMF_Corrector.frx":0442
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraSSA"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "CmdAddlev"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Instruction Syntax"
      TabPicture(1)   =   "MaVSAMF_Corrector.frx":045E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FraIstr"
      Tab(1).Control(1)=   "Label3"
      Tab(1).ControlCount=   2
      Begin VB.CommandButton CmdAddlev 
         Caption         =   "Add Level"
         Enabled         =   0   'False
         Height          =   252
         Left            =   3270
         TabIndex        =   72
         Top             =   3240
         Width           =   972
      End
      Begin VB.Frame fraSSA 
         BorderStyle     =   0  'None
         Height          =   3165
         Left            =   30
         TabIndex        =   7
         Top             =   30
         Width           =   4245
         Begin TabDlg.SSTab SSTab3 
            Height          =   3135
            Left            =   0
            TabIndex        =   8
            Top             =   30
            Width           =   4245
            _ExtentX        =   7488
            _ExtentY        =   5530
            _Version        =   393216
            TabOrientation  =   2
            Tabs            =   2
            TabHeight       =   520
            ForeColor       =   128
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Body"
            TabPicture(0)   =   "MaVSAMF_Corrector.frx":047A
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "Label2(0)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblIstrLev(0)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "Label2(3)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "Label2(5)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "Label2(8)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "Label2(10)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "ImgSegmenti"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtComCode"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "cmdKey"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtSSAName"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "cmdSegmFind"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtsegm"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "txtSegLen"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtStrutt"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "ChkQual"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "ChkBoth"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).ControlCount=   16
            TabCaption(1)   =   "Key"
            TabPicture(1)   =   "MaVSAMF_Corrector.frx":0496
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "fraKey"
            Tab(1).ControlCount=   1
            Begin VB.CheckBox ChkBoth 
               Alignment       =   1  'Right Justify
               Caption         =   "Both versions"
               ForeColor       =   &H00C00000&
               Height          =   252
               Left            =   2520
               TabIndex        =   67
               Top             =   2760
               Width           =   1332
            End
            Begin VB.CheckBox ChkQual 
               Alignment       =   1  'Right Justify
               Caption         =   "Qualified"
               ForeColor       =   &H00C00000&
               Height          =   252
               Left            =   585
               TabIndex        =   54
               Top             =   2760
               Width           =   1020
            End
            Begin VB.TextBox txtStrutt 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   360
               Left            =   1410
               TabIndex        =   33
               Top             =   1500
               Width           =   2400
            End
            Begin VB.TextBox txtSegLen 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   360
               Left            =   1410
               TabIndex        =   32
               Top             =   1080
               Width           =   2400
            End
            Begin VB.TextBox txtsegm 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   360
               Left            =   1410
               TabIndex        =   31
               Top             =   660
               Width           =   1830
            End
            Begin VB.CommandButton cmdSegmFind 
               Caption         =   "..."
               Height          =   315
               Left            =   3420
               TabIndex        =   30
               Top             =   696
               Width           =   345
            End
            Begin VB.TextBox txtSSAName 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   360
               Left            =   1410
               TabIndex        =   29
               Top             =   1920
               Width           =   1860
            End
            Begin VB.CommandButton cmdKey 
               Caption         =   "..."
               Height          =   315
               Left            =   3420
               TabIndex        =   28
               Top             =   1956
               Width           =   345
            End
            Begin VB.TextBox txtComCode 
               BackColor       =   &H00FFFFC0&
               ForeColor       =   &H00C00000&
               Height          =   360
               Left            =   1410
               TabIndex        =   27
               Top             =   2376
               Width           =   2400
            End
            Begin VB.Frame fraKey 
               BorderStyle     =   0  'None
               Height          =   2985
               Left            =   -74640
               TabIndex        =   9
               Top             =   60
               Width           =   3795
               Begin VB.TextBox txtKeyMask 
                  BackColor       =   &H00FFFFC0&
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   825
                  TabIndex        =   17
                  Top             =   2280
                  Width           =   2970
               End
               Begin VB.TextBox txtKeyStart 
                  BackColor       =   &H00FFFFC0&
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   810
                  TabIndex        =   16
                  Top             =   1875
                  Width           =   1110
               End
               Begin VB.TextBox txtOp 
                  BackColor       =   &H00FFFFC0&
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   810
                  TabIndex        =   15
                  Top             =   1095
                  Width           =   1110
               End
               Begin VB.TextBox txtValue 
                  BackColor       =   &H00FFFFC0&
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   810
                  TabIndex        =   14
                  Top             =   1470
                  Width           =   2970
               End
               Begin VB.TextBox txtKeyLen 
                  BackColor       =   &H00FFFFC0&
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   2700
                  TabIndex        =   13
                  Top             =   1875
                  Width           =   1080
               End
               Begin VB.TextBox txtOpLogico 
                  Alignment       =   2  'Center
                  BackColor       =   &H00FFFFC0&
                  ForeColor       =   &H00C00000&
                  Height          =   330
                  Left            =   3450
                  TabIndex        =   12
                  Top             =   2670
                  Width           =   330
               End
               Begin VB.TextBox txtKeyName 
                  BackColor       =   &H80000018&
                  ForeColor       =   &H00C00000&
                  Height          =   360
                  Left            =   810
                  TabIndex        =   11
                  Top             =   690
                  Width           =   2970
               End
               Begin VB.CommandButton cmdOpMulti 
                  Caption         =   "..."
                  Height          =   315
                  Left            =   2100
                  TabIndex        =   10
                  ToolTipText     =   "Manage Multi operators for current level istruction..."
                  Top             =   1125
                  Width           =   375
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Logical Operator"
                  ForeColor       =   &H00C00000&
                  Height          =   195
                  Index           =   11
                  Left            =   2010
                  TabIndex        =   26
                  Top             =   2730
                  Width           =   1170
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Key Value"
                  ForeColor       =   &H00C00000&
                  Height          =   195
                  Index           =   13
                  Left            =   60
                  TabIndex        =   25
                  Top             =   2340
                  Width           =   720
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Key start"
                  ForeColor       =   &H00C00000&
                  Height          =   225
                  Index           =   12
                  Left            =   150
                  TabIndex        =   24
                  Top             =   1965
                  Width           =   690
               End
               Begin VB.Label lblIstrLev 
                  Alignment       =   2  'Center
                  BackColor       =   &H8000000D&
                  Caption         =   "Current istruction level: "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00FFFFFF&
                  Height          =   285
                  Index           =   1
                  Left            =   0
                  TabIndex        =   23
                  Top             =   30
                  Width           =   3795
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Key Name"
                  ForeColor       =   &H00C00000&
                  Height          =   195
                  Index           =   4
                  Left            =   30
                  TabIndex        =   22
                  Top             =   780
                  Width           =   735
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "SSA value"
                  ForeColor       =   &H00C00000&
                  Height          =   195
                  Index           =   6
                  Left            =   45
                  TabIndex        =   21
                  Top             =   1545
                  Width           =   750
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Oper."
                  ForeColor       =   &H00C00000&
                  Height          =   225
                  Index           =   7
                  Left            =   360
                  TabIndex        =   20
                  Top             =   1185
                  Width           =   435
               End
               Begin VB.Label Label2 
                  AutoSize        =   -1  'True
                  Caption         =   "Key Len."
                  ForeColor       =   &H00C00000&
                  Height          =   225
                  Index           =   9
                  Left            =   2040
                  TabIndex        =   19
                  Top             =   1965
                  Width           =   645
               End
               Begin VB.Label lblKeyLev 
                  Alignment       =   2  'Center
                  BackColor       =   &H80000003&
                  Caption         =   "Current key level: "
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00FFFFFF&
                  Height          =   285
                  Index           =   0
                  Left            =   0
                  TabIndex        =   18
                  Top             =   360
                  Width           =   3795
               End
            End
            Begin VB.Image ImgSegmenti 
               Height          =   288
               Left            =   3876
               Stretch         =   -1  'True
               Top             =   696
               Width           =   288
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "VSAM. Len."
               ForeColor       =   &H00C00000&
               Height          =   195
               Index           =   10
               Left            =   540
               TabIndex        =   39
               Top             =   1095
               Width           =   855
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Data Area"
               ForeColor       =   &H00C00000&
               Height          =   228
               Index           =   8
               Left            =   576
               TabIndex        =   38
               Top             =   1500
               Width           =   780
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "VSAM"
               ForeColor       =   &H00C00000&
               Height          =   195
               Index           =   5
               Left            =   690
               TabIndex        =   37
               Top             =   690
               Width           =   450
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "SSA Field"
               ForeColor       =   &H00C00000&
               Height          =   192
               Index           =   3
               Left            =   600
               TabIndex        =   36
               Top             =   1950
               Width           =   720
            End
            Begin VB.Label lblIstrLev 
               Alignment       =   2  'Center
               BackColor       =   &H8000000D&
               Caption         =   "Current istruction level : "
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   285
               Index           =   0
               Left            =   360
               TabIndex        =   35
               Top             =   90
               Width           =   3795
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Comm. Codes"
               ForeColor       =   &H00C00000&
               Height          =   192
               Index           =   0
               Left            =   300
               TabIndex        =   34
               Top             =   2350
               Width           =   972
            End
         End
      End
      Begin VB.Frame FraIstr 
         BorderStyle     =   0  'None
         Caption         =   "..."
         Height          =   3075
         Left            =   -74880
         TabIndex        =   40
         Top             =   120
         Width           =   4125
         Begin VB.CommandButton Command1 
            Caption         =   "..."
            Height          =   288
            Left            =   1743
            TabIndex        =   69
            Top             =   30
            Width           =   252
         End
         Begin VB.TextBox TxtInstr 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   288
            Left            =   840
            TabIndex        =   68
            Top             =   30
            Width           =   852
         End
         Begin VB.TextBox txtIstruzione 
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   885
            Left            =   0
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   46
            Top             =   2205
            Width           =   4020
         End
         Begin VB.CommandButton cmdAddPsb 
            Caption         =   "Add"
            Height          =   300
            Left            =   30
            TabIndex        =   45
            Top             =   1800
            Width           =   876
         End
         Begin VB.CommandButton cmdRemovePsb 
            Caption         =   "Remove"
            Height          =   300
            Left            =   1110
            TabIndex        =   44
            Top             =   1800
            Width           =   876
         End
         Begin VB.CommandButton cmdAddDBD 
            Caption         =   "Add"
            Height          =   300
            Left            =   2085
            TabIndex        =   43
            Top             =   1800
            Width           =   876
         End
         Begin VB.CommandButton cmdRemoveDBD 
            Caption         =   "Remove"
            Height          =   300
            Left            =   3150
            TabIndex        =   42
            Top             =   1800
            Width           =   876
         End
         Begin VB.TextBox TxtPCB 
            BackColor       =   &H00FFFFC0&
            ForeColor       =   &H00C00000&
            Height          =   288
            Left            =   3156
            TabIndex        =   41
            Top             =   30
            Width           =   855
         End
         Begin MSComctlLib.ListView lswDBD 
            Height          =   1125
            Left            =   2040
            TabIndex        =   47
            Top             =   630
            Width           =   1995
            _ExtentX        =   3519
            _ExtentY        =   1984
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "PsbName"
               Object.Width           =   2540
            EndProperty
         End
         Begin MSComctlLib.ListView lswPsb 
            Height          =   1125
            Left            =   0
            TabIndex        =   48
            Top             =   630
            Width           =   1995
            _ExtentX        =   3519
            _ExtentY        =   1984
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            HideColumnHeaders=   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   12582912
            BackColor       =   16777152
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "PsbName"
               Object.Width           =   2540
            EndProperty
         End
         Begin VB.Label LblPCB 
            Caption         =   "Instruction"
            ForeColor       =   &H00C00000&
            Height          =   195
            Index           =   0
            Left            =   30
            TabIndex        =   64
            Top             =   60
            Width           =   735
         End
         Begin VB.Label Label2 
            Caption         =   "PSB"
            ForeColor       =   &H00C00000&
            Height          =   195
            Index           =   1
            Left            =   30
            TabIndex        =   51
            Top             =   420
            Width           =   675
         End
         Begin VB.Label Label2 
            Caption         =   "DBD"
            ForeColor       =   &H00C00000&
            Height          =   195
            Index           =   2
            Left            =   2100
            TabIndex        =   50
            Top             =   420
            Width           =   645
         End
         Begin VB.Label LblPCB 
            Caption         =   "PCB Number"
            ForeColor       =   &H00C00000&
            Height          =   195
            Index           =   14
            Left            =   2100
            TabIndex        =   49
            Top             =   60
            Width           =   975
         End
      End
      Begin VB.Label Label3 
         BackColor       =   &H8000000D&
         Caption         =   "ewwe"
         Height          =   252
         Left            =   -74880
         TabIndex        =   63
         Top             =   120
         Width           =   732
      End
   End
   Begin VB.CheckBox CheckToMigrate 
      Alignment       =   1  'Right Justify
      Caption         =   "To migrate"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   8880
      TabIndex        =   71
      Top             =   3960
      Width           =   1095
   End
   Begin VB.CheckBox CheckObsolete 
      Alignment       =   1  'Right Justify
      Caption         =   "Obsolete"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   7680
      TabIndex        =   70
      Top             =   3960
      Width           =   975
   End
   Begin VB.CommandButton cmdConferma 
      Caption         =   "Save"
      Height          =   285
      Left            =   10080
      Picture         =   "MaVSAMF_Corrector.frx":04B2
      TabIndex        =   53
      ToolTipText     =   "Save Corrections"
      Top             =   3960
      Width           =   855
   End
   Begin VB.CheckBox chkIstrFLAG 
      Alignment       =   1  'Right Justify
      Caption         =   "Valid"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   6720
      TabIndex        =   52
      Top             =   3960
      Width           =   735
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   10995
      _ExtentX        =   19394
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Find"
            Object.ToolTipText     =   "Find selected text"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "OPENTEXTED"
            Object.ToolTipText     =   "Open selected source with text editor..."
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Copybooks"
            Object.ToolTipText     =   "Expand/Hide Copybooks"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "Hide_Copybooks"
            Object.ToolTipText     =   "Hide Copybooks"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2415
      Top             =   7860
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   20
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":08F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":0A50
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":0BAC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":1144
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":1460
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":15BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":1A14
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":1B70
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":1FD0
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":2134
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":2594
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":29E8
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":2B44
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":2CA0
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":2DFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":38C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":3D1C
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":416E
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":45C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":4A12
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTSalva 
      Height          =   885
      Left            =   1155
      TabIndex        =   1
      Top             =   7635
      Visible         =   0   'False
      Width           =   1065
      _ExtentX        =   1879
      _ExtentY        =   1561
      _Version        =   393217
      RightMargin     =   60000
      TextRTF         =   $"MaVSAMF_Corrector.frx":4E64
   End
   Begin RichTextLib.RichTextBox RTCpyApp 
      Height          =   885
      Left            =   60
      TabIndex        =   0
      Top             =   7635
      Visible         =   0   'False
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   1561
      _Version        =   393217
      ScrollBars      =   3
      RightMargin     =   60000
      TextRTF         =   $"MaVSAMF_Corrector.frx":4EE6
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Index           =   0
      Left            =   11130
      Top             =   8130
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":4F68
            Key             =   "Checkmrk"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":5282
            Key             =   "New"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":5394
            Key             =   "Misc39b"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":56AE
            Key             =   "Erase02"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":59C8
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":5ADA
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":5CB4
            Key             =   "Find"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":5DC6
            Key             =   "Copy"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Index           =   1
      Left            =   4440
      Top             =   7830
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":5ED8
            Key             =   "Checkmrk"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":61F2
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":6304
            Key             =   "Find"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":6416
            Key             =   "New"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":6528
            Key             =   "Erase02"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":6842
            Key             =   "Misc39b"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":6B5C
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":6C6E
            Key             =   "Close"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":6E48
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":9952
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":9DA4
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MaVSAMF_Corrector.frx":ADF6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTPGMBase 
      Height          =   975
      Left            =   3150
      TabIndex        =   3
      Top             =   7650
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   1720
      _Version        =   393217
      ScrollBars      =   3
      RightMargin     =   60000
      TextRTF         =   $"MaVSAMF_Corrector.frx":B6D0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin RichTextLib.RichTextBox RTIncaps 
      Height          =   3810
      Left            =   30
      TabIndex        =   4
      Top             =   420
      Width           =   6630
      _ExtentX        =   11695
      _ExtentY        =   6720
      _Version        =   393217
      BackColor       =   4210752
      HideSelection   =   0   'False
      ScrollBars      =   3
      RightMargin     =   80000
      TextRTF         =   $"MaVSAMF_Corrector.frx":B750
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TreeView TRWIstruzione 
      Height          =   3180
      Left            =   6690
      TabIndex        =   5
      Top             =   4260
      Width           =   4305
      _ExtentX        =   7594
      _ExtentY        =   5609
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab SSTab2 
      Height          =   3165
      Left            =   30
      TabIndex        =   55
      Top             =   4290
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   5583
      _Version        =   393216
      TabOrientation  =   1
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   128
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Log Viewer"
      TabPicture(0)   =   "MaVSAMF_Corrector.frx":B7D0
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "LswLog"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Instructions list"
      TabPicture(1)   =   "MaVSAMF_Corrector.frx":B7EC
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "lswRighe"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "chkErrInstr"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame1"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Frame2"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      Begin VB.Frame Frame2 
         Caption         =   "Instruction Error"
         Height          =   830
         Left            =   2640
         TabIndex        =   65
         Top             =   1950
         Width           =   3885
         Begin VB.TextBox txtWarning 
            Appearance      =   0  'Flat
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            ForeColor       =   &H00000080&
            Height          =   552
            Left            =   90
            MultiLine       =   -1  'True
            TabIndex        =   66
            Top             =   180
            Width           =   3645
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   855
         Left            =   120
         TabIndex        =   57
         Top             =   1950
         Width           =   1284
         Begin VB.OptionButton optAllInstr 
            Caption         =   "All Instructions"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   50
            TabIndex        =   60
            Top             =   120
            Width           =   1476
         End
         Begin VB.OptionButton optIMSDB 
            Caption         =   "IMS/DB"
            ForeColor       =   &H00C00000&
            Height          =   315
            Left            =   50
            TabIndex        =   59
            Top             =   300
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.OptionButton optIMSDC 
            Caption         =   "IMS/DC"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   50
            TabIndex        =   58
            Top             =   600
            Visible         =   0   'False
            Width           =   885
         End
      End
      Begin VB.CheckBox chkErrInstr 
         Caption         =   "Invalid only"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   1440
         TabIndex        =   56
         Top             =   2520
         Width           =   1092
      End
      Begin MSComctlLib.ListView lswRighe 
         Height          =   1890
         Left            =   60
         TabIndex        =   61
         Top             =   60
         Width           =   6465
         _ExtentX        =   11404
         _ExtentY        =   3334
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "ImageList1"
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "id"
            Text            =   "ID"
            Object.Width           =   1765
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "Riga"
            Text            =   "Line"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "istr"
            Text            =   "Instruction"
            Object.Width           =   1766
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Key             =   "sintax"
            Text            =   "Statement"
            Object.Width           =   31751
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Key             =   "idpsb"
            Text            =   "ID PSB"
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.ListView LswLog 
         Height          =   2625
         Left            =   -74910
         TabIndex        =   62
         Top             =   90
         Width           =   6405
         _ExtentX        =   11298
         _ExtentY        =   4630
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   16777152
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "LogText"
            Object.Width           =   5292
         EndProperty
      End
   End
End
Attribute VB_Name = "MaVSAMF_Corrector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public mSel_Idx_Database As Long
Public mSel_Idx_Psb As Long
Public RtBefore As String

Private Sub loadSelectedKey()
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "KEY", txtKeyName.text
  'Controllo "Istruzione"...
  Associa_Dato_In_Memoria AULivello, "KEY", txtKeyName.text
  
  'SG : Carica i dati riferiti alla chiave
  txtKeyLen = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Valore.Lunghezza
  txtOp = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Operatore
  txtValue = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Valore.FieldValue
  txtKeyLen.text = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Valore.Lunghezza
  txtKeyStart.text = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Valore.KeyStart
  txtKeyMask.text = Istruzione.SSA(AULivello).Chiavi(AUSelIndexKey).Valore.Valore
   
  txtSSAName.text = Istruzione.SSA(AULivello).ssaName
   
  txtValue.SelStart = 0
  txtValue.SelLength = Len(txtValue.text)
  
  'SG : Carica l'operatore logico di congiunzione se c'�
  'attenzione l'operatore logico � riferito al successivo : L'ultimo della catena non avr� operatore
  'Es: FTCHIA>=VALUE & FTCHIA2<=VALUE
  txtOpLogico.text = ""
  If UBound(Istruzione.SSA(AULivello).OpLogico) >= AUSelIndexKey Then
      txtOpLogico.text = Istruzione.SSA(AULivello).OpLogico(AUSelIndexKey)
  End If
End Sub

Private Sub Check1_Click()

End Sub

Private Sub ChkBoth_Click()
  If ChkBoth.Value = 1 Then
    ChkQual.Value = 1
    'Associa_Dato_In_TreeView TRWIstruzione, AULivello, "CKQ" & Format(AULivello, "00"), ChkQual.Value
    Associa_Dato_In_Memoria AULivello, "CKQ", ChkQual.Value
  End If
  'Associa_Dato_In_TreeView TRWIstruzione, AULivello, "CKB" & Format(AULivello, "00"), ChkBoth.Value
  Associa_Dato_In_Memoria AULivello, "CKB", ChkBoth.Value
End Sub

Private Sub ChkQual_Click()
  'Associa_Dato_In_TreeView TRWIstruzione, AULivello, "CKQ" & Format(AULivello, "00"), ChkQual.Value
  Associa_Dato_In_Memoria AULivello, "CKQ", ChkQual.Value
End Sub

Private Sub CmdAddlev_Click()
Dim istrdetlivRec As Recordset
Dim newlwvel As Integer
newlwvel = 1
Set istrdetlivRec = m_dllFunctions.Open_Recordset("Select Max(Livello) as maxl from PsDLI_IstrDett_Liv Where IdOggetto = " & VSAM2Rdbms.drIdOggetto & " and Riga = " & Istruzione.Riga)
If Not istrdetlivRec.EOF Then
  If Not IsNull(istrdetlivRec!maxl) Then
    newlwvel = istrdetlivRec!maxl + 1
  End If
End If
istrdetlivRec.Close
Set istrdetlivRec = m_dllFunctions.Open_Recordset("Select * from PsDLI_IstrDett_Liv")
istrdetlivRec.AddNew
istrdetlivRec!Livello = newlwvel
istrdetlivRec!idOggetto = VSAM2Rdbms.drIdOggetto
istrdetlivRec!idPgm = VSAM2Rdbms.drIdOggetto
istrdetlivRec!Riga = Istruzione.Riga
istrdetlivRec.Update
istrdetlivRec.Close
RefreshRiga
End Sub

Private Sub cmdRemoveDBD_Click()
   Dim i As Long
   
   If lswDBD.ListItems.Count > 0 Then
    'SQ no comment...
    'For i = UBound(Istruzione.Database) To mSel_Idx_Database + 1 Step - 1
    For i = mSel_Idx_Database + 1 To UBound(Istruzione.Database)
      'If i > 1 Then
        Istruzione.Database(i - 1) = Istruzione.Database(i)
      'End If
    Next
      ReDim Preserve Istruzione.Database(UBound(Istruzione.Database) - 1)
    'Remove a video
    lswDBD.ListItems.Remove mSel_Idx_Database
  End If
End Sub

Private Sub chkIstrFLAG_Click()
  Aggiorna_Icone_TreeView_Istruzione chkIstrFLAG.Value, TRWIstruzione
End Sub

Private Sub cmdAddDBD_Click()
   gbTypeToLoad = "DBD"
   
   Dim oj As New MaVSAMF_ListPsbDBD
   Dim i As Long
   Dim s() As String
   Dim Id() As String
   
   oj.Show vbModal
   
   If oj.Id <> "#" Then
      s = Split(oj.SelName, "#")
      Id = Split(oj.Id, "#")
      
      If UBound(s) Then
         For i = 0 To UBound(s) - 1
            lswDBD.ListItems.Add , , s(i)
            lswDBD.ListItems(lswDBD.ListItems.Count).Tag = Format(Id(i), "######")
         Next i
      End If
      
      'associa il dato in memoria
      Associa_Dato_In_Memoria AULivello, "DBD", ""
   End If
   
   Set oj = Nothing
End Sub

Private Sub cmdAddPsb_Click()
   gbTypeToLoad = "PSB"
   
   Dim oj As New MaVSAMF_ListPsbDBD
   Dim i As Long
   Dim s() As String
   Dim Id() As String
   
   oj.Show vbModal
   
   If oj.Id <> "#" Then
      s = Split(oj.SelName, "#")
      Id = Split(oj.Id, "#")
      
      If UBound(s) > 0 Then
         For i = 0 To UBound(s) - 1
            lswPsb.ListItems.Add , , s(i)
            lswPsb.ListItems(lswPsb.ListItems.Count).Tag = Format(Id(i), "######")
         Next i
      End If
      
      
      'associa il dato in memoria
      Associa_Dato_In_Memoria AULivello, "PSB", ""
   End If
   
   Set oj = Nothing
End Sub

Private Sub cmdKey_Click()
   GbIntFormSel = 0
   MaVSAMF_ChiaveViewer.Show
End Sub

Private Sub cmdOpMulti_Click()
   GbIntFormSel = 0
   MaVSAMF_Operatori.Show
End Sub

Private Sub cmdRemovePsb_Click()
   Dim i As Long
   If lswPsb.ListItems.Count > 0 Then
      'SG : Elimina il riferimento al dbd
      For i = UBound(Istruzione.psb) To mSel_Idx_Psb Step -1
         If i > 1 Then
            Istruzione.psb(i - 1) = Istruzione.psb(i)
         End If
      Next i
      
      ReDim Preserve Istruzione.psb(UBound(Istruzione.psb) - 1)
      
      'Remove a video
      lswPsb.ListItems.Remove mSel_Idx_Psb
   End If
End Sub

Private Sub cmdSegmFind_Click()
   gbTypeToLoad = "SEG"
   
   Dim oj As New MaVSAMF_ListPsbDBD
   Dim i As Long
   Dim s() As String
   Dim Id() As String
   
   oj.Show vbModal
   
   If oj.Id <> "#" Then
      s = Split(oj.SelName, "#")
      Id = Split(oj.Id, "#")
      
      If UBound(s) > 0 Then
         For i = 0 To UBound(s) - 1
            txtsegm.text = s(i)
            txtsegm.Tag = Format(Id(i), "######")
         Next i
      End If
      
      txtsegm.Tag = GestDuplicati("SEG", txtsegm.text)
      
      'associa il dato in memoria
      'alpitour: 5/8/2005: ma perch�???????
      'Associa_Dato_In_Memoria AULivello, "SEG", ""
      Associa_Dato_In_Memoria AULivello, "SEG", txtsegm.text
   End If
   
   Set oj = Nothing
End Sub

Private Sub Command1_Click()
  GbIntFormSel = 1
  MaVSAMF_Operatori.Show
End Sub

Private Sub Form_Resize()
    ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
   m_dllFunctions.RemoveActiveWindows Me
   m_dllFunctions.FnActiveWindowsBool = False

   SetFocusWnd m_dllFunctions.FnParent
End Sub

Private Sub lswDBD_DblClick()
  If lswDBD.ListItems.Count > 0 Then
    'MaVSAMF_PSB_DBD_Load.load_psb_dbd lswDBD.SelectedItem, lswDBD.SelectedItem.Tag
    m_dllFunctions.Show_ObjViewer lswDBD.SelectedItem.Tag
  End If
End Sub

Private Sub lswDBD_ItemClick(ByVal Item As MSComctlLib.ListItem)
   mSel_Idx_Database = Item.Index
End Sub

Private Sub lswPsb_DblClick()
  If lswPsb.ListItems.Count > 0 Then
    'MaVSAMF_PSB_DBD_Load.load_psb_dbd lswPsb.SelectedItem, lswPsb.SelectedItem.Tag
    m_dllFunctions.Show_ObjViewer lswPsb.SelectedItem.Tag
  End If
End Sub

Private Sub lswPsb_ItemClick(ByVal Item As MSComctlLib.ListItem)
  mSel_Idx_Psb = Item.Index
End Sub

Private Sub lswRighe_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim key As Long
  Static Order As String
  
  Order = lswRighe.SortOrder
  
  key = ColumnHeader.Index
  
  If UCase(Val(Order)) = 0 Then
     Order = 1
  Else
     Order = 0
  End If
  
  lswRighe.SortOrder = Order
  lswRighe.SortKey = key - 1
  lswRighe.Sorted = True
End Sub

Private Sub lswRighe_DblClick()
  'Si posiziona sulla RichTextBox all'istruzione selezionata
  Dim Riga As String
  Dim i As Long
  Dim K As Long
  Dim x() As String
  Dim y() As String
  
'  riga = Item.Tag ' si tratta del numero di riga scritto in esadecimale
'  i = RTIncaps.Find(riga, 1)
  If Not copyExpanded Then
    'ALE vado al numero di riga scritto nel campo x della lsw:
'    x = Split(arrayRighe(lswRighe.SelectedItem.SubItems(1) - 1), ";")
'    RTIncaps.Find lswRighe.SelectedItem.SubItems(2), x(0), x(1)

    x = Split(arrayRighe(lswRighe.SelectedItem.SubItems(1) - 1), ";")
    y = Split(arrayRighe(lswRighe.SelectedItem.SubItems(1) + lswRighe.SelectedItem.Tag - 2), ";")
    Riga = Mid(RTIncaps.text, x(0) + 1, (y(1) - x(0)))
    'RTIncaps.Find Riga, x(0) - 1, y(1) + 1
    RTIncaps.SelStart = x(0) + 1
    RTIncaps.SelLength = y(1) - x(0)
    RTIncaps.SelColor = vbWhite
    RTIncaps.SelLength = 0
  Else
'    x = Split(arrayRighe(lineeIstr(lswRighe.SelectedItem.Index) - 1), ";")
'    RTIncaps.Find lswRighe.SelectedItem.SubItems(2), x(0), x(1) 'lswRighe.SelectedItem.SubItems(2)
     selRiga RTIncaps, (Val(lineeIstr(lswRighe.SelectedItem.Index)) - 1), lswRighe.SelectedItem.SubItems(2), lswRighe.SelectedItem.Tag
  
  End If
  CmdAddlev.Enabled = True
 'RefreshRiga
End Sub

Private Sub lswRighe_ItemClick(ByVal Item As MSComctlLib.ListItem)
'  'Si posiziona sulla RichTextBox all'istruzione selezionata
'  Dim Riga As String
'  Dim i As Long
'  Dim k As Long
'  Dim x() As String
'  Dim y() As String
'
''  riga = Item.Tag ' si tratta del numero di riga scritto in esadecimale
''  i = RTIncaps.Find(riga, 1)
'  If Not copyExpanded Then
'    'ALE vado al numero di riga scritto nel campo x della lsw:
''    x = Split(arrayRighe(lswRighe.SelectedItem.SubItems(1) - 1), ";")
''    RTIncaps.Find lswRighe.SelectedItem.SubItems(2), x(0), x(1)
'
'    x = Split(arrayRighe(lswRighe.SelectedItem.SubItems(1) - 1), ";")
'    y = Split(arrayRighe(lswRighe.SelectedItem.SubItems(1) + lswRighe.SelectedItem.Tag - 2), ";")
'    Riga = Mid(RTIncaps.Text, x(0) + 1, (y(1) - x(0)))
'    'RTIncaps.Find Riga, x(0) - 1, y(1) + 1
'    RTIncaps.SelStart = x(0) + 1
'    RTIncaps.SelLength = y(1) - x(0)
'    RTIncaps.SelColor = vbWhite
'    RTIncaps.SelLength = 0
'  Else
''    x = Split(arrayRighe(lineeIstr(lswRighe.SelectedItem.Index) - 1), ";")
''    RTIncaps.Find lswRighe.SelectedItem.SubItems(2), x(0), x(1) 'lswRighe.SelectedItem.SubItems(2)
'     selRiga RTIncaps, (Val(lineeIstr(lswRighe.SelectedItem.Index)) - 1), lswRighe.SelectedItem.SubItems(2), lswRighe.SelectedItem.Tag
'
'  End If
  
  CaricaWarnings lswRighe.SelectedItem, lswRighe.SelectedItem.SubItems(1)
  CmdAddlev.Enabled = True
  RefreshRiga
  
End Sub

Public Function selRiga(RT As RichTextBox, numRiga As Long, istruz As String, totRighe As Long)
 'Idx = RT.Find(vbCrLf, OldPos) 'ALE
 'array dalla riga 1 alla fine contenenti gli  indici del carattere capo linea e fine linea
 'es:rt.Find("GDEL",x1,x2)
 Dim Idx As Long
 Dim Riga As Long
 Dim x1 As Long
 Dim x2 As Long
 Dim i As Long
 Dim numRiga2 As Long
  Dim xInit As Long
 Dim xFine As Long
 Dim strRiga As String
 On Error GoTo ErrorHandler
 
 numRiga2 = numRiga + totRighe - 1
  i = 0
  x1 = 1
  Do Until Idx = -1
    Idx = RT.Find(vbCrLf, x1)
    x2 = Idx
    Riga = RT.GetLineFromChar(Idx)
    If Riga = numRiga Then
       xInit = x1
'      RT.Find istruz, x1, x2
'      Exit Do
    End If
    If Riga = numRiga2 Then
       xFine = x2
'      strRiga = Mid(RTIncaps.Text, xInit + 1, (xFine - xInit))
'      RT.Find strRiga, xInit, xFine
      RT.SelStart = xInit
      RT.SelLength = xFine - xInit
      RT.SelColor = vbWhite
      RT.SelLength = 0
      Exit Do
    End If
    x1 = x2 + 2
  Loop
  Exit Function
ErrorHandler:
Resume Next
  
End Function

Private Sub Form_Load()
   'mauro: 15/12/2005: disabilito i tasti di default
   
   copyExpanded = False
   Toolbar1.ImageList = imlToolbarIcons(1)
   Toolbar1.Buttons(1).Image = 3
   Toolbar1.Buttons(2).Image = 9
   Toolbar1.Buttons(3).Image = 11
   'Toolbar1.Buttons(4).Image = 12
   cmdConferma.Enabled = False
   cmdAddDBD.Enabled = False
   cmdRemoveDBD.Enabled = False
   cmdAddPsb.Enabled = False
   cmdRemovePsb.Enabled = False
   cmdOpMulti.Enabled = False
   cmdSegmFind.Enabled = False
   cmdKey.Enabled = False
   
   LswLog.ListItems.Clear
   
   gIdDbd = -1
   gIdPsb = -1
   gIdField = -1
   
   lblIstrLev(0).Caption = "Current Level instruction <not selected>"
   lblIstrLev(1).Caption = "Current Level instruction <not selected>"
   
   RTIncaps.text = "SELCOLOR"
   RTIncaps.SelStart = 1
   RTIncaps.SelLength = Len(RTIncaps.text)
   RTIncaps.SelColor = vbGreen
   
   'Setta il parent
   'SetParent Me.hwnd, VSAM2Rdbms.drParent
   
   'm_dllFunctions.FnHeight = MaVSAMF_Verifica.Height 'ALE?????
   
   ' m_dllFunctions.ResizeControls Me
   
   '2) Carica la lista con tutte le istruzioni
    optAllInstr.Value = True 'settando questo fa scattare il corrispondente evento click
    chkErrInstr.Value = 0
    'ALE**********************
    Elimina_Flickering Me.hwnd
    TRWIstruzione.Nodes.Clear
    'Carica il file nella RichTextBox
    Carica_File_In_RichTextBox LswLog, RTIncaps, VSAM2Rdbms.drIdOggetto
    RTIncaps.SelStart = 1
    RTIncaps.SelLength = Len(RTIncaps.text)
    RTIncaps.SelColor = vbGreen
    Carica_ListView_Istruzioni lswRighe, VSAM2Rdbms.drIdOggetto, 1, RTIncaps
    RTIncaps.SelStart = 0
    RTIncaps.SelLength = 0
    LswLog.ListItems.Add , , "Loading " & LswLog.ListItems.Count & " Instruction(s) in list...."
    Terminate_Flickering
    '*************************
   
  
   
  
   Me.Caption = "IMS/DB/DC Corrector - Object : " & m_dllFunctions.Restituisci_NomeOgg_Da_IdOggetto(VSAM2Rdbms.drIdOggetto)
   
   'Resize delle liste
   LswLog.ColumnHeaders(1).Width = LswLog.Width - 270
   
   'Posiziona la lista sull'ultimo elemento
   LswLog.ListItems(LswLog.ListItems.Count).EnsureVisible
   'ale
   '  FraRighe.Move FraLog.Left, FraLog.Top
   '  FraIstr.Left = fraSSA.Left
   '  FraIstr.Top = fraSSA.Top
   
   m_dllFunctions.AddActiveWindows Me
   m_dllFunctions.FnActiveWindowsBool = True
   
   'resize delle liste
   lswDBD.ColumnHeaders(1).Width = lswDBD.Width - 270
   lswPsb.ColumnHeaders(1).Width = lswPsb.Width - 270
   
   
       'ALE mettere istruzioni click:
   'tmp
   'lswRighe.Visible = true
   
   '  cmdSegmFind.Top = txtsegm.Top + 25
   '  cmdSegmFind.Left = txtsegm.Left + txtsegm.Width - cmdSegmFind.Width
   '  cmdSegmFind.Height = txtsegm.Height - 50
   '  cmdSegmFind.Width = cmdSegmFind.Height
   '
   '  cmdKey.Top = txtSSAName.Top + 25
   '  cmdKey.Left = txtSSAName.Left + txtSSAName.Width - cmdKey.Width
   '  cmdKey.Height = txtSSAName.Height - 50
   '  cmdKey.Width = cmdKey.Height
End Sub

Public Sub CaricaWarnings(idOggetto As Long, Riga As Long)
Dim r As Recordset
Dim Mess As String
Dim i As Long

  txtWarning.text = ""
  Set r = m_dllFunctions.Open_Recordset("Select Distinct IdOggetto,Codice,Riga From BS_Segnalazioni Where IdOggetto = " & idOggetto & " and Riga = " & Riga & " order by codice desc")
    Do Until r.EOF
      Mess = Componi_Messaggio_Errore(r!idOggetto, r!Codice, r!Riga)
      txtWarning = txtWarning & Mess & vbCrLf
      r.MoveNext
    Loop
    r.Close
End Sub

Public Sub optAllInstr_Click()

  Carica_ListView_Istruzioni lswRighe, VSAM2Rdbms.drIdOggetto, 1, RTIncaps
'  Elimina_Flickering Me.hwnd
'  TRWIstruzione.Nodes.Clear
'  'Carica il file nella RichTextBox
'  Carica_File_In_RichTextBox LswLog, RTIncaps, VSAM2Rdbms.drIdOggetto
'  RTIncaps.SelStart = 1
'  RTIncaps.SelLength = Len(RTIncaps.Text)
'  RTIncaps.SelColor = vbGreen
'  Carica_ListView_Istruzioni lswRighe, VSAM2Rdbms.drIdOggetto, 1, RTIncaps
'  RTIncaps.SelStart = 0
'  RTIncaps.SelLength = 0
'  LswLog.ListItems.Add , , "Loading " & LswLog.ListItems.Count & " Istruction(s) in list...."
'  Terminate_Flickering
End Sub

Private Sub optIMSDB_Click()
  Carica_ListView_Istruzioni lswRighe, VSAM2Rdbms.drIdOggetto, 2, RTIncaps
End Sub

Private Sub optIMSDC_Click()
  Carica_ListView_Istruzioni lswRighe, VSAM2Rdbms.drIdOggetto, 3, RTIncaps
End Sub
Private Sub chkErrInstr_Click()

  If optAllInstr Then
    Carica_ListView_Istruzioni lswRighe, VSAM2Rdbms.drIdOggetto, 1, RTIncaps
  ElseIf optIMSDB Then
    Carica_ListView_Istruzioni lswRighe, VSAM2Rdbms.drIdOggetto, 2, RTIncaps
  Else
    Carica_ListView_Istruzioni lswRighe, VSAM2Rdbms.drIdOggetto, 3, RTIncaps
  End If
  

'  Call Elimina_Flickering(Me.hwnd)
'  TRWIstruzione.Nodes.Clear
'  'Carica il file nella RichTextBox
'   Carica_File_In_RichTextBox LswLog, RTIncaps, VSAM2Rdbms.drIdOggetto
'  RTIncaps.SelStart = 1
'  RTIncaps.SelLength = Len(RTIncaps.Text)
'  RTIncaps.SelColor = vbGreen
'  Call Carica_ListView_Istruzioni(lswRighe, VSAM2Rdbms.drIdOggetto, False, RTIncaps)
'  RTIncaps.SelStart = 0
'  RTIncaps.SelLength = 0
'  LswLog.ListItems.Add , , "Loading " & LswLog.ListItems.Count & " Istruction(s) in list...."
'  Call Terminate_Flickering
End Sub

Private Sub LswLog_ItemClick(ByVal Item As MSComctlLib.ListItem)
  Dim Marcatore As String
  
  Select Case Trim(UCase(Mid(Item.Tag, 1, 4)))
    Case "LOAD" ' Ha caricato un file
      'Formalismo : LOAD[Percorso]
    
    Case "EXPD" ' Ha trovato la copy e l'ha espansa
      'Formalismo : EXPD[6byte marcatore][percorso]
      Marcatore = Mid(Item.Tag, 5, 6)
            
    Case "EXPE" ' Non ha espanso la copy
      'Formalismo : EXPD[6byte marcatore][Nome]
      Marcatore = Mid(Item.Tag, 5, 6)
      
    Case "FIND" ' Testo trovato
      'Formalismo : FIND[6byte marcatore]
      Marcatore = Mid(Item.Tag, 5, 6)
    
    Case "SELT" ' Testo selezionato
      'Formalismo : SELT[6byte marcatore]
      Marcatore = Mid(Item.Tag, 5, 6)
      
  End Select
  
  If Trim(Marcatore) <> "" Then
    RTIncaps.Find Marcatore, 1
  End If
End Sub


Public Sub RefreshRiga()
 Dim NumeroLivelli As Long
  Dim ExRiga As Boolean
  Dim istruz As String
  Dim rpos As Long
  Dim rs As New adodb.Recordset
  Dim rsCall As New adodb.Recordset
  Dim Rigaistruzione As String
  Dim IstrValida As Boolean

  Dim i As Long
  
  On Error GoTo subErr
  
  'mauro: 15/12/2005: Abilito i tasti solo se seleziono un'istruzione
   cmdConferma.Enabled = True
   cmdAddDBD.Enabled = True
   cmdRemoveDBD.Enabled = True
   cmdAddPsb.Enabled = True
   cmdRemovePsb.Enabled = True
   cmdOpMulti.Enabled = True
   cmdSegmFind.Enabled = True
   cmdKey.Enabled = True
  
  'pulisce tutte le textbox
  ImgSegmenti.Picture = Nothing
  Cancella_TextBox_Istruzione
  
  'Carica_Array_Istruzione lswRighe.SelectedItem.ListSubItems(1), VSAM2Rdbms.drIdOggetto
  Carica_Istruzione lswRighe.SelectedItem.ListSubItems(1), VSAM2Rdbms.drIdOggetto
  
  'AC
  If UBound(Istruzione.SSA) > 0 Then
  fraSSA.Caption = "Level 01"
    lblIstrLev(0).Caption = "Current Instruction Level 01"
    lblIstrLev(1).Caption = "Current Instruction Level 01"
  lblKeyLev(0).Caption = "Current key level <not selected>"
  AULivello = 1
    Carica_TextBox_Istruzione "LEV01"
    SSTab3.Enabled = True
  Else
    lblIstrLev(0).Caption = "No Current Instruction Level"
    lblIstrLev(1).Caption = "No Current Instruction Level"
    lblKeyLev(0).Caption = "Current key level <not selected>"
    AULivello = 1
    SSTab3.Enabled = False
  End If

  '**************
  IstrValida = Carica_TreeView_Dettaglio_Istruzioni(TRWIstruzione, lswRighe.SelectedItem.ListSubItems(1))
  
  'Load generalit� istruzioni
  lswDBD.ListItems.Clear
  For i = 1 To UBound(Istruzione.Database)
    lswDBD.ListItems.Add , , Istruzione.Database(i).nome
    lswDBD.ListItems(lswDBD.ListItems.Count).Tag = Format(Istruzione.Database(i).idOggetto, "######")
  Next i
  
  lswPsb.ListItems.Clear
  For i = 1 To UBound(Istruzione.psb)
    lswPsb.ListItems.Add , , Istruzione.psb(i).nome
    lswPsb.ListItems(lswPsb.ListItems.Count).Tag = Format(Istruzione.psb(i).idOggetto, "######")
  Next i
  
  'Incolla l'icona giusta sulla TreeView e abilita i bottoni
  'txtIstruzione = Istruzione.Istruzione
  
  '****
  Set rs = m_dllFunctions.Open_Recordset("Select isExec from PsDLI_Istruzioni where IdOggetto = " & VSAM2Rdbms.drIdOggetto & " and riga = " & Istruzione.Riga)
  If Not rs.EOF Then
    If rs!isExec Then
        Set rsCall = m_dllFunctions.Open_Recordset("Select Comando,Istruzione from PsExec where Area = 'DLI' and IdOggetto = " & VSAM2Rdbms.drIdOggetto & " and riga = " & Istruzione.Riga)
        If Not rsCall.EOF Then
            Rigaistruzione = "EXEC DLI " & vbCrLf & _
                             IIf(rsCall!Comando = "SCHD" Or rsCall!Comando = "SCHEDULE", "SCHD PSB", rsCall!Comando & " USING ") & vbCrLf & _
                             Trim(wIstrCod(totIstr).Istr) & vbCrLf & _
                             "END-EXEC"
        End If
        rsCall.Close
    Else
        Set rsCall = m_dllFunctions.Open_Recordset("Select Programma,Parametri from PsCall where IdOggetto = " & VSAM2Rdbms.drIdOggetto & " and riga = " & Istruzione.Riga)
        If Not rsCall.EOF Then
            Rigaistruzione = "CALL " & Trim(rsCall!Programma) & " USING " & Trim(rsCall!parametri)
        End If
        rsCall.Close
    End If
    rs.Close
  End If
  txtIstruzione = Rigaistruzione
 
  'Se l'istruzione � valida chekka la check bo
  chkIstrFLAG.Value = 0
  
  If Istruzione.valida Then
    chkIstrFLAG.Value = 1
  Else
    chkIstrFLAG.Value = 0
  End If
  If Istruzione.obsolete Then
    CheckObsolete.Value = 1
  Else
    CheckObsolete.Value = 0
  End If
  If Istruzione.to_migrate Then
    CheckToMigrate.Value = 1
  Else
    CheckToMigrate.Value = 0
  End If
  Exit Sub
subErr:
  MsgBox "tmp: " & Err.Description
  'Resume
End Sub

Private Sub RTIncaps_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
  Seleziona_Word_E_MarcaWord RTIncaps, LswLog
End Sub

Private Sub SSTab3_Click(PreviousTab As Integer)
   If SSTab3.Tab = 1 Then
'      fraKey.Left = 360
'      fraKey.Top = 60
   End If
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  On Error Resume Next
  Select Case Button.key
    Case "Checkmrk"
      m_dllFunctions.WriteLog "Toolbar1 ButtonClick - Opzione Checkmrk", "DR - AUTO"
    
    Case "OPENTEXTED"
     'Lancia Il text editor
    ' Call Launch_TextEditor(AUSelText)
     
     m_dllFunctions.Show_TextEditor VSAM2Rdbms.drIdOggetto, , , Me
        
    Case "Find"
      'MF 02/08/07
      'Gestione ricerca tramite apertura form TxtFind
      m_dllFunctions.FindTxt AUSelText
      AUSelText = m_dllFunctions.txtModif
      If m_dllFunctions.txtSearch Then
        Call Avvia_Ricerca_Ricorsiva_Parola(RTIncaps, LswLog, AUSelText)
        SSTab2.Tab = 0
      End If
    
    Case "New"
       m_dllFunctions.WriteLog "Toolbar1 ButtonClick - Opzione New", "DR - Auto"
     
    Case "Erase02"
       m_dllFunctions.WriteLog "Toolbar1 ButtonClick - Opzione Erase02", "DR - Auto"

    Case "Help"
       m_dllFunctions.WriteLog "Toolbar1 ButtonClick - Opzione Help", "DR - Auto"

    Case "Close"
      Unload Me
      
    Case "Copybooks"
      If Not copyExpanded Then
        copyExpanded = True
        RtBefore = RTIncaps.text
        If lswRighe.ListItems.Count > 0 Then
           Dim i As Long
           ReDim lineeIstr(lswRighe.ListItems.Count)
           For i = 1 To lswRighe.ListItems.Count
            lineeIstr(i) = lswRighe.ListItems(i).SubItems(1)
           Next
        End If
        Expand_CPY LswLog, RTCpyApp, RTIncaps
      Else
        copyExpanded = False
        RTIncaps.text = ""
        RTIncaps.text = RtBefore
        RTIncaps.SelStart = 1
        RTIncaps.SelLength = Len(RTIncaps.text)
        RTIncaps.SelColor = vbGreen
        RTIncaps.Find " ", 0, 100
      End If

  End Select
End Sub

Private Sub TRWIstruzione_Collapse(ByVal Node As MSComctlLib.Node)
   TRWIstruzione_NodeClick Node
End Sub

Private Sub TRWIstruzione_Expand(ByVal Node As MSComctlLib.Node)
   TRWIstruzione_NodeClick Node
End Sub

Private Sub TRWIstruzione_NodeClick(ByVal Node As MSComctlLib.Node)
  Dim i As Long
  
  Select Case UCase(Mid(Node.key, 1, 3))
    Case "LEV" 'Siamo su un livello 'Carica Le text Box
      'Carica le text Box
      Cancella_TextBox_Istruzione
      
      Carica_TextBox_Istruzione Node.key
      
      AULivello = Val(Mid(Node.key, 4))
      
      lblIstrLev(0).Caption = "Current level instruction : " & Format(AULivello, "00")
      lblIstrLev(1).Caption = "Current level instruction : " & Format(AULivello, "00")
      lblKeyLev(0).Caption = "Current key level : " & Format(AUSelIndexKey, "00")
      
    Case "DBD"
      Cancella_TextBox_Istruzione
    
    Case "PSB"
      Cancella_TextBox_Istruzione
    
    Case "IST"
    'AC
       'Call Cancella_TextBox_Istruzione
    
    Case Else
   
      If Trim(Node.key) <> "" Then
        'Sono in un livello dell'istruzione
        'Torna in su fino al primo nodo
        For i = Node.Index To 1 Step -1
          If Trim(UCase(Mid(TRWIstruzione.Nodes(i).key, 1, 3))) = "LEV" Then
            AULivello = Val(Mid(Node.key, 4))
            
            Carica_TextBox_Istruzione (TRWIstruzione.Nodes(i).key)
            
            lblIstrLev(0).Caption = "Current instruction level : " & Format(AULivello, "00")
            lblIstrLev(1).Caption = "Current instruction level : " & Format(AULivello, "00")
            
            lblKeyLev(0).Caption = "Current key level : " & Format(AUSelIndexKey, "00")
            Exit For
          End If
        Next i
      Else
        'Sono sulle Info dell'istruzione
        'Cancella_TextBox_Istruzione
        Exit Sub
      End If
  End Select
End Sub

Private Sub Carica_TextBox_Istruzione(Nodo As String)
  Dim Liv As Long
  Dim i As Long
  Dim Idx As Long
  Dim mNode As Node
  
  'Prende il nodo selezionato
  Set mNode = TRWIstruzione.SelectedItem
  
  Cancella_TextBox_Istruzione
  
  'Prende il Livello
  Liv = Val(Mid(Nodo, 4))
  AULivello = Liv
  If Not mNode Is Nothing Then
  AUSelIndexKey = Val(mNode.Tag)
  Else
    AUSelIndexKey = 1
  End If
  
  For i = 1 To UBound(Istruzione.SSA)
    If Istruzione.SSA(i).Livello = Liv Then
      Idx = i
    End If
  Next i
 
  txtsegm = Istruzione.SSA(Idx).NomeSegmento
  txtSegLen = Istruzione.SSA(Idx).LenSegmento
  txtStrutt = Istruzione.SSA(Idx).Struttura
  txtSSAName = Istruzione.SSA(Idx).ssaName
  
  ChkQual.Value = IIf(Istruzione.SSA(Idx).Qualificatore, 1, 0)
  ChkBoth.Value = IIf(Istruzione.SSA(Idx).BothQualAndUnqual, 1, 0)
  
  If UBound(Istruzione.SSA(Idx).Chiavi) = 1 Or (UBound(Istruzione.SSA(Idx).Chiavi) > 1 And AUSelIndexKey = 0) Then
      'carica l'unica chiave che esiste
      AUSelIndexKey = 1
  End If
  'SQ - non va bene...
  If AUSelIndexKey > 0 And UBound(Istruzione.SSA(Idx).Chiavi) Then
   txtKeyName = Istruzione.SSA(Idx).Chiavi(AUSelIndexKey).key
   loadSelectedKey
  End If
  
  txtComCode.text = Istruzione.SSA(Idx).CommandCode

  fraSSA.Caption = "Level " & Format(Liv, "00")

  ImgSegmenti.Picture = Nothing
  
  SetFormByIstruzione (Idx)
End Sub

Public Sub SetFormByIstruzione(Index As Long)

End Sub

Private Sub Cancella_TextBox_Istruzione()
  'fraSSA.Caption = "Level [Not Selected]"
  txtSegLen = ""
  txtsegm = ""
  txtStrutt = ""
  txtKeyName.text = ""
  txtKeyLen = ""
  txtValue = ""
  txtComCode = ""
  txtOp = ""
  txtSSAName = ""
  txtKeyMask = ""
  txtKeyStart = ""
  'ChkQual.Value = 0
  'ChkBoth.Value = 0
  txtOpLogico = ""
  
  'lblIstrLev(0).Caption = "Current Level instruction <not selected>"
  'lblIstrLev(1).Caption = "Current Level instruction <not selected>"
  'lblKeyLev(0).Caption = "Current key level <not selected>"
End Sub

Private Sub txtComCode_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "CON" & Format(AUSelIndexKey, "00"), txtComCode.text
  Associa_Dato_In_Memoria AULivello, "COD", txtComCode.text
End Sub

Private Sub txtKeyLen_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "KYL" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtKeyLen.text
  Associa_Dato_In_Memoria AULivello, "KYL", txtKeyLen.text
End Sub

Private Sub txtKeyMask_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "VAL" & Format(AULivello, "00") & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtKeyMask.text
  Associa_Dato_In_Memoria AULivello, "VAL", txtKeyMask.text
End Sub

Private Sub txtKeyName_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "KEY" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtKeyName.text
  Associa_Dato_In_Memoria AULivello, "KEY", txtKeyName.text
End Sub

Private Sub txtKeyStart_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "KYS" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtKeyStart.text
  Associa_Dato_In_Memoria AULivello, "KYS", txtKeyStart.text
End Sub

Private Sub txtOp_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "OPE" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtOp.text
  Associa_Dato_In_Memoria AULivello, "OPE", txtOp.text
End Sub

Private Sub txtOpLogico_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "OPL" & Format(AULivello, "00"), txtOpLogico.text
  Associa_Dato_In_Memoria AULivello, "OPL", txtOpLogico.text
End Sub

Private Sub txtSegLen_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "SGL" & Format(AULivello, "00"), txtSegLen.text
  Associa_Dato_In_Memoria AULivello, "SGL", txtSegLen.text
End Sub

Private Sub txtsegm_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "SEG" & Format(AULivello, "00"), txtsegm.text
 
  txtsegm.Tag = GestDuplicati("SEG", txtsegm.text)
  
  Associa_Dato_In_Memoria AULivello, "SEG", txtsegm.text
End Sub

Private Sub txtSSAName_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "SSA" & Format(AULivello, "00"), txtSSAName.text
  Associa_Dato_In_Memoria AULivello, "SSA", txtSSAName.text
End Sub

Private Sub txtStrutt_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "STR" & Format(AULivello, "00"), txtStrutt.text
  Associa_Dato_In_Memoria AULivello, "STR", txtStrutt.text
End Sub

Private Sub txtValue_KeyUp(KeyCode As Integer, Shift As Integer)
  Associa_Dato_In_TreeView TRWIstruzione, AULivello, "KYV" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), txtValue.text
  Associa_Dato_In_Memoria AULivello, "KYV", txtValue.text
End Sub

Public Function GestDuplicati(wTipo As String, wName As String) As Long
   Dim rs As adodb.Recordset
   Dim sWhere As String
   Dim wNotFound As Boolean
   
   If gIdDbd <> -1 Then
      sWhere = " AND IdOggetto = " & gIdDbd
   End If
   
   Select Case Trim(wTipo)
      Case "SEG" 'Segmento
         Set rs = m_dllFunctions.Open_Recordset("Select * From PSDLI_Segmenti Where Nome = '" & wName & "'" & sWhere)
         
         Select Case rs.RecordCount
            Case 0
               ImgSegmenti.Picture = ImageList1.ListImages.Item(10).Picture
               ImgSegmenti.ToolTipText = "This segment does not exists..."
               GestDuplicati = -1
               wNotFound = True
               
            Case 1
               ImgSegmenti.Picture = ImageList1.ListImages.Item(14).Picture
               GestDuplicati = rs!idSegmento
               ImgSegmenti.ToolTipText = "This segment is unique under this database..."
               txtSegLen.text = rs!MaxLen
               
               Associa_Dato_In_TreeView TRWIstruzione, AULivello, "SGL" & Format(AULivello, "00"), txtSegLen.text
               Associa_Dato_In_Memoria AULivello, "SGL", txtSegLen.text
               
            Case Else '>1
               ImgSegmenti.Picture = ImageList1.ListImages.Item(17).Picture
               GestDuplicati = -1
               ImgSegmenti.ToolTipText = "WARNING! This segment name is duplicated into some databases..."
         
         End Select
         
         rs.Close
   End Select
   
   If wNotFound Then
      wNotFound = False
      Select Case Trim(wTipo)
         Case "SEG" 'Segmento
            Set rs = m_dllFunctions.Open_Recordset("Select * From PSDLI_Segmenti Where Nome = '" & wName & "'")
         
            Select Case rs.RecordCount
               Case 0
                  ImgSegmenti.Picture = ImageList1.ListImages.Item(10).Picture
                  GestDuplicati = -1
                  wNotFound = True
                  
               Case 1
                  ImgSegmenti.Picture = ImageList1.ListImages.Item(14).Picture
                  GestDuplicati = rs!idSegmento
                  
               Case Else '>1
                  ImgSegmenti.Picture = ImageList1.ListImages.Item(17).Picture
                  GestDuplicati = -1
            End Select
         rs.Close
      End Select
   End If
End Function
Private Function IsValidSplit(Source As String) As Boolean
Dim s() As String
Dim i As Integer
  If InStr(1, Source, ",") Or InStr(1, Source, ".") Then
    IsValidSplit = False
    Exit Function
  End If
  IsValidSplit = True
  s = Split(Source, ";")
  For i = 0 To UBound(s)
     If Not IsNumeric(s(i)) Then
      IsValidSplit = False
      Exit For
     End If
  Next
End Function
Private Function IsValidPCB(Source As String) As Boolean

  If InStr(1, Source, ",") Or InStr(1, Source, ".") Then
    IsValidPCB = False
    Exit Function
  Else
     If IsNumeric(Source) Then
        IsValidPCB = True
     Else
        IsValidPCB = False
     End If
  End If
End Function

Private Sub cmdConferma_Click()
   Dim ColRigaOggetto As New Collection
   Screen.MousePointer = vbHourglass
   'If Not Trim(TxtPCB.Text) = "" And Not IsValidSplit(Trim(TxtPCB.Text)) Then
   If Not Trim(TxtPCB.text) = "" And Not IsValidPCB(Trim(TxtPCB.text)) Then
    MsgBox "Invalid PCB Number(s) or PCB numbers not separated with semicolons", vbCritical, "PCB Number"
    TxtPCB.SetFocus
   Else
     'Conferma_Convalida_Istruzione VSAM2Rdbms.drIdOggetto, Istruzione.Riga, CBool(chkIstrFLAG.Value)
     Conferma_Convalida_Istruzione VSAM2Rdbms.drIdOggetto, Istruzione.Riga, CBool(chkIstrFLAG.Value), CBool(CheckObsolete.Value), CBool(CheckToMigrate.Value)
     If Not (UCase(GbPrevIstr) = UCase(FirstElement(Trim(TxtInstr.text)))) Then
        ColRigaOggetto.Add (lswRighe.SelectedItem.SubItems(1))
        ColRigaOggetto.Add (lswRighe.SelectedItem)
        DLLExtParser.AttivaFunzione "PARSER2", ColRigaOggetto
        lswRighe.SelectedItem.SubItems(2) = FirstElement(Trim(TxtInstr.text))
     ElseIf Not (GbPrevPCB = Trim(TxtPCB.text)) And Not Trim(TxtPCB.text) = "" Then
        Riparser_after_PCBChange Trim(TxtPCB.text), Istruzione.pcbName
     End If
     RefreshRiga
   End If
   Screen.MousePointer = vbDefault
End Sub
