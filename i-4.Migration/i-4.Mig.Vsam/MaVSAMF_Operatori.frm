VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MaVSAMF_Operatori 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choose operators"
   ClientHeight    =   3360
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   2400
   Icon            =   "MaVSAMF_Operatori.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3360
   ScaleWidth      =   2400
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   1500
      Picture         =   "MaVSAMF_Operatori.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2460
      Width           =   825
   End
   Begin MSComctlLib.ListView lswOp 
      Height          =   2295
      Left            =   30
      TabIndex        =   0
      Top             =   60
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   4048
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   16777152
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "op"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "MaVSAMF_Operatori"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOk_Click()
   Dim i As Long
   Dim wop As String
   Dim wInstr As String
   Select Case GbIntFormSel
    Case 0  'operatori
      For i = 1 To lswOp.ListItems.Count
         If lswOp.ListItems(i).Checked Then
            wop = wop & ";" & lswOp.ListItems(i).text
         End If
      Next i
      
      wop = Mid(wop, 2)
      
      Associa_Dato_In_TreeView MaVSAMF_Corrector.TRWIstruzione, AULivello, "OPE" & Format(AULivello, "00") & Format(AUSelIndexKey, "00"), wop
      'Controllare "Istruzione"...
      Associa_Dato_In_Memoria AULivello, "OPE", wop
      
      MaVSAMF_Corrector.txtOp.text = wop
   Case 1   'istruzioni
    For i = 1 To lswOp.ListItems.Count
         If lswOp.ListItems(i).Checked Then
            wInstr = wInstr & ";" & lswOp.ListItems(i).text
         End If
    Next i
    
    wInstr = Mid(wInstr, 2)
    
    MaVSAMF_Corrector.TxtInstr.text = wInstr
    
   End Select
   Unload Me
End Sub

Private Sub Form_Load()

 Dim i As Long
 Dim j As Long
 Dim wop As String
 Dim wInstr As String
 Dim s() As String
 
 Select Case GbIntFormSel
    Case 0  'operatori
      Me.Caption = "Choose operetors"
      lswOp.ListItems.Add , , "EQ"
      lswOp.ListItems.Add , , "GT"
      lswOp.ListItems.Add , , "GE"
      lswOp.ListItems.Add , , "LT"
      lswOp.ListItems.Add , , "LE"
      lswOp.ListItems.Add , , "NE"
     
      lswOp.ColumnHeaders(1).Width = lswOp.Width
      wop = Trim(MaVSAMF_Corrector.txtOp.text)
      s = Split(wop, ";")
      
      For i = 0 To UBound(s)
         For j = 1 To lswOp.ListItems.Count
            If Trim(lswOp.ListItems(j).text) = Trim(s(i)) Then
               lswOp.ListItems(j).Checked = True
               Exit For
            End If
         Next j
      Next i
   Case 1
    Me.Caption = "Choose instructions"
    lswOp.ListItems.Add , , "APSB"
    lswOp.ListItems.Add , , "AUTH"
    lswOp.ListItems.Add , , "CHKP"
    lswOp.ListItems.Add , , "CHNG"
    lswOp.ListItems.Add , , "CKPT"
    lswOp.ListItems.Add , , "CLSE"
    lswOp.ListItems.Add , , "DLET"
    lswOp.ListItems.Add , , "DPSB"
    lswOp.ListItems.Add , , "GCMD"
    lswOp.ListItems.Add , , "GHN"
    lswOp.ListItems.Add , , "GHNP"
    lswOp.ListItems.Add , , "GHU"
    lswOp.ListItems.Add , , "GMSG"
    lswOp.ListItems.Add , , "GN"
    lswOp.ListItems.Add , , "GNP"
    lswOp.ListItems.Add , , "GU"
    lswOp.ListItems.Add , , "GSCD"
    lswOp.ListItems.Add , , "ICMD"
    lswOp.ListItems.Add , , "INIT"
    lswOp.ListItems.Add , , "INQY"
    lswOp.ListItems.Add , , "ISRT"
    lswOp.ListItems.Add , , "LOG"
    lswOp.ListItems.Add , , "OPEN"
    lswOp.ListItems.Add , , "PCB"
    lswOp.ListItems.Add , , "RCMD"
    lswOp.ListItems.Add , , "REPL"
    lswOp.ListItems.Add , , "ROLB"
    lswOp.ListItems.Add , , "ROLL"
    lswOp.ListItems.Add , , "ROLS"
    lswOp.ListItems.Add , , "SYNC"
    lswOp.ListItems.Add , , "TERM"
    lswOp.ListItems.Add , , "XRST"
    lswOp.ListItems.Add , , "PURG"
    lswOp.ListItems.Add , , "SETB"
    lswOp.ListItems.Add , , "SETO"
    lswOp.ListItems.Add , , "SETS"
    lswOp.ListItems.Add , , "SETU"
    
    lswOp.ColumnHeaders(1).Width = lswOp.Width
    wInstr = Trim(MaVSAMF_Corrector.TxtInstr.text)
    s = Split(wInstr, ";")
      
      For i = 0 To UBound(s)
         For j = 1 To lswOp.ListItems.Count
            If Trim(lswOp.ListItems(j).text) = Trim(s(i)) Then
               lswOp.ListItems(j).Checked = True
               Exit For
            End If
         Next j
      Next i
   End Select
End Sub
