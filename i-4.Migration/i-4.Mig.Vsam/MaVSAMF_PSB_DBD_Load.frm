VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form MaVSAMF_PSB_DBD_Load 
   Caption         =   "Form1"
   ClientHeight    =   6585
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9420
   LinkTopic       =   "Form1"
   ScaleHeight     =   6585
   ScaleWidth      =   9420
   StartUpPosition =   2  'CenterScreen
   Begin RichTextLib.RichTextBox RTCopyLoad 
      Height          =   6555
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9405
      _ExtentX        =   16589
      _ExtentY        =   11562
      _Version        =   393217
      BackColor       =   16777152
      Enabled         =   -1  'True
      ScrollBars      =   3
      TextRTF         =   $"MaVSAMF_PSB_DBD_Load.frx":0000
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "MaVSAMF_PSB_DBD_Load"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Resize()
   RTCopyLoad.Width = Me.ScaleWidth
   RTCopyLoad.Height = Me.ScaleHeight
   RTCopyLoad.Top = Me.ScaleTop
   RTCopyLoad.Left = Me.ScaleLeft
End Sub

Public Sub load_psb_dbd(objNome As String, Optional objId As Long)
  Dim rs As Recordset
  Dim name As String
  
  Me.Caption = "Selected Object source code - " & objNome
  
  RTCopyLoad.SelStart = 0
  RTCopyLoad.SelLength = 1
  RTCopyLoad.SelColor = vbBlue
  RTCopyLoad.SelLength = 0
  
  Set rs = Open_Recordset("Select Directory_Input,estensione From BS_Oggetti Where IdOggetto = " & objId)
  If rs.RecordCount Then
    name = Crea_Directory_Progetto(rs!Directory_Input, m_dllFunctions.FnPathDef) & "\" & objNome
    If Len(rs!Estensione) Then
      name = name & "." & rs!Estensione
    End If
  End If
  rs.Close
  
  RTCopyLoad.LoadFile name
   
  Me.Show
End Sub
