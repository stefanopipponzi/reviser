       IDENTIFICATION DIVISION.
       PROGRAM-ID.    NOME-RTE.
       AUTHOR.        AUTHOR-RTE.

      *REMARKS.
      **--------------------------------------------------------------**
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  1) GENERALIZED CICS ROUTINE TO MANAGE "SEND MAP"            **
      **     GENERATED FROM IMS/DC                                    **
      **--------------------------------------------------------------**
      
      
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.

      *----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

      *--------------------------------------------------*
      *  MANAGEMENT PARAMETERS FOR TREATMENT ROUTINES    *
      *  SPECIFICALLY FIELDS IMS->CICS AND VICEVERSA     *
      *--------------------------------------------------*
      
       COPY DFHAID.       
       
       01  WK-MAPNAME             PIC X(8). 
       01  WMFS-TIME              PIC X(8).
       01  WMFS-DATE              PIC X(8).
       01 HALF-WORD               PIC S9(4) COMP.
       01 HALF-WORD-R             PIC S9(4) COMP.
       01 CX-ATTRIB               PIC X.
       01 FLG-CURS                PIC X   VALUE 'N'.
       01 RESPONSE                PIC S9(8) COMP.
       01  WS-HOLD-AREAS.
           05  WS-ABSTIME         PIC S9(16) COMP.
           05  WS-SYSDATE         PIC X(08)  VALUE SPACES.
           05  WS-SYSTIME         PIC X(08)  VALUE SPACES.
           05  WS-EIBDATE         PIC 9(05)  VALUE ZERO.
           05  WS-RESP            PIC S9(08) COMP VALUE ZERO.
       01  TSOQ-QUEUE.
          05  TSOQ-TERM           PIC X(4).
          05  FILLER              PIC X(4)  VALUE 'OPER'.
       01  TSOQ-LEN               PIC S9(4) COMP  VALUE +2030.
       01  TSOQ-ITEM.
          05  TSOQ-AREA           PIC X(2030).
       

       01  AREA-LNK-PASS.
           COPY LNKDCRT.

       01  BASE-PAGE              PIC S9(8) COMP.   
       01  LEN-PAGE               PIC S9(8) COMP.         
       01  RESPONSE               PIC S9(8) COMP.

       
       
      *#NROUT# 
       
       
       


       LINKAGE SECTION.
             
       01  DFHCOMMAREA           PIC X(2056).
       
           
      
      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHCOMMAREA.
       
       MAIN-START.

           MOVE DFHCOMMAREA TO AREA-LNK-PASS.   
           IF LNKDC-CHNGDEST NOT EQUAL SPACE
              EXEC CICS XCTL    PROGRAM(LNKDC-CHNGDEST)
	                        RESP(RESPONSE)
	      END-EXEC
	      
	      IF RESPONSE = DFHRESP(INVREQ)
	         MOVE 'ZZ' TO LNKDC-RETCODE
	         GO TO MAIN-END
	      END-IF
	      IF RESPONSE = DFHRESP(NOTAUTH)
	         MOVE 'ZZ' TO LNKDC-RETCODE
	         GO TO MAIN-END
	      END-IF
	      IF RESPONSE = DFHRESP(PGMIDERR)
	         MOVE 'ZZ' TO LNKDC-RETCODE
	         GO TO MAIN-END
	      END-IF

           END-IF
           
      	
      *    #OPE#
       

           MOVE AREA-LNK-PASS TO DFHCOMMAREA.
            
       MAIN-END.
           GOBACK.
      
      

           
