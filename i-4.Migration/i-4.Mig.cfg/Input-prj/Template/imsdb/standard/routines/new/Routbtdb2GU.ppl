 #NOME_RTE#: PROC (LNKDB_AREA) OPTIONS(REENTRANT,FETCHABLE);
 /*-----------------------------------------------------------------*/
 /* AUTHOR        : I-4M                                            */
 /* DATE_WRITEN   : 10/07/2009                                      */
 /*-----------------------------------------------------------------*/
 /* LAST MODIFIED : 10/07/2009        (RELEASE 3.1)                 */
 /*                                                                 */
 /* BATCH IO-Routine for GU and GHU                                 */
 /* --------------------------------------------------------------- */
 /*                                                                 */
 /*           CODED INSTRUCTIONS INTERNAL TO THE MODULE             */
 /*                                                                 */
 /*-----------------------------------------------------------------*/
 /*                                                                 */
 /*  #CODE#
 */
 /* --------------------------------------------------------------- */
 /*                     RECORDS STRUCTURE                           */
 /* --------------------------------------------------------------- */

 /* -------------------------------- INCLUDE  BY COMPILER LINK AREAS*/
  DCL  1  LNKDB_AREA UNALIGNED,
  %INCLUDE LNKDBRT;   /* BUFFER INTERFACE WITH CALLING PROGRAM      */

 DCL  1 LNKDB_PCB_AUX BASED(ADDR(LNKDB_PCB_AREA)) ,
         5   *                     CHAR (12),
         5   LNKDB_PCBPARSEGM      CHAR (4),
         5   LNKDB_PCBEOSEGM       CHAR (1),
         5   LNKDB_PCBPARLV        CHAR (1);
 /* -------------------------------- INCLUDE  OTHER AREAS           */
  %INCLUDE ERRDBRT;
  %INCLUDE FLGDB2I#;         /* GRZ: DB2 INTERFACE */
  %INCLUDE FLCERR0#;         /* GRZ: DB2 INTERFACE */

  EXEC SQL INCLUDE SQLCA;

  *#COPY_WK_DB2#
  *#COPYX_WK_DB2#

  %INCLUDE WKDATERT ;
 /* --------------------------------------------------------------- */
  DCL   WK_GNQUAL          CHAR(001) ;
        WK_GNQUAL = 'S'              ;
  DCL   WK_NAMETABLE       CHAR(010) ;
  DCL   WK_NAMETABLE_GE    CHAR(010) ;
  DCL   WK_SEGMENT         CHAR(008) ;
  DCL   WK_ISTR            CHAR(004) ;

  DCL 1  WK_SSA                      ,
       3 WK_KEYNAME        CHAR(030) ,
       3 WK_KEYOPER        CHAR(002) ,
       3 WK_KEYVALUE       CHAR(100) ;
  DCL 1  WK_SSAR CHAR(132) BASED(ADDR(WK_SSA));

  DCL   WK_SQLCODE         FIXED BIN (31);
  DCL   WK_IND             FIXED BIN (31);
  DCL   WINDICE            FIXED BIN (31);
  DCL   WK_IND2            FIXED BIN (31);
  DCL   WFIXDEC            FIXED DEC (15);
  DCL   WK_REVKEY          FIXED BIN (31);
  DCL   WK_KEYSEQ          FIXED BIN (31);
  DCL   WK_TABLE           CHAR (18);
  DCL   WK_TABLE_SQL_ERR   CHAR (18);
  DCL   WK_LABEL_ERR       CHAR (16);
  DCL   WFIXBIN            FIXED BIN (31) INIT(0);
  DCL   WK_KEYSEQ1         FIXED BIN (31);
  DCL   WK_SEGLEVEL        PIC '99';
  DCL   RC                 FIXED BIN (15);

  DCL YRERRF0 ENTRY  (PTR,CHAR(10))
            RETURNS  (BIN FIXED(15))
            OPTIONS(FETCHABLE);

  *#CURSORS_DB2#

 /* ---------------------------------------------------------------*/
 /*                   M  A  I  N                                   */
 /* ---------------------------------------------------------------*/
  CALL INIT_AREAS ;

  /*    #OPE# */

  LNKDB_CODE_ERROR = 'NOOP';
  CALL ROUT_ERROR;
  RETURN;

 /*----------------------------------------------------------------*/
 /*             A R E A S   I N I T I A L I Z A T I O N            */
 /*----------------------------------------------------------------*/
 INIT_AREAS: PROC;
    LNKDB_ABEND_RTCODE = ' ';
    WK_SQLCODE = '';
    WK_GNQUAL ='S';
    LNKDB_PCBLGKFB = 0;
    LNKDB_PCBDBDNM = '#DBDNAME#';
    IF LNKDB_STATE ^> '' THEN DO;
       LNKDB_ACTIVE = '';
       LNKDB_STATE  = 'A';
       LNKDB_NUMSTACK = 0;
       LNKDB_MAXSTACK = 0;
    END;
 END INIT_AREAS;

 /*----------------------------------------------------------------*/
 /*             P R E P A R E   E X I T   A R E A S                */
 /*            S E T   T H E   R E T U R N   C O D E               */
 /*----------------------------------------------------------------*/
 PREPARE_EXIT: PROC;
    LNKDB_LASTSEGM(LNKDB_NUMPCB) = WK_SEGMENT;
    WK_IND = 1;
    DO UNTIL (WK_IND > MAX_SQLRESP);
       IF WK_SQLCODE = ERRDB_CODRESP(WK_IND) THEN DO;
          LNKDB_PCBSTAT = ERRDB_CODDLI(WK_IND);
          WK_IND = MAX_SQLRESP + 1;
       END;
       ELSE DO;
          IF ERRDB_CODRESP(WK_IND) = -999999 THEN DO;
             LNKDB_PCBSTAT = ERRDB_CODDLI(WK_IND);
             WK_IND = MAX_SQLRESP + 1;
          END;
       END;
       WK_IND = WK_IND + 1;
    END;

    IF LNKDB_PCBSTAT ^= ' ' THEN DO;
       IF LNKDB_PCBSTAT = 'XI' THEN DO;
          CERR_PTR = SQL_ERROR('#NOME_RTE#02',ADDR(SQLCA),
                     '-Instruction: ' !! LNKDB_OPERATION !!
                     '-Table: ' !! WK_NAMETABLE);
          RC = YRERRF0(ADDR(LNKDB_AREA),WK_NAMETABLE);
       END;
       ELSE IF LNKDB_PCBSTAT = 'GE' THEN
          CALL SET_PCBINFO;
    END;
    ELSE
       LNKDB_FLG_XRST(LNKDB_NUMSTACK) = 1;

    LNKDB_PCBSEGNM = WK_SEGMENT;
    LNKDB_PCBPARLV = SUBSTR(LNKDB_PCBSEGLV,2,1); /* PARENTAGE LEVEL */

    CALL SET_LASTOP;
 END PREPARE_EXIT;

 /* ---------------------------------------------------------- */
 /*        BACKUP ROUTINE OF LAST OPER IN LINKAGE AREA         */
 /* ---------------------------------------------------------- */
 SET_LASTOP: PROC;
     IF WK_ISTR = 'GU' THEN DO;
        LNKDB_STKCURSOR(LNKDB_NUMSTACK) = ' ';
     END;
     IF WK_ISTR = 'GU' !
        WK_ISTR = 'GN' !
        WK_ISTR = 'GNP' THEN DO;
        WK_IND = 0;
        DO UNTIL (WK_IND >= LNKDB_MAXSTACK);
           WK_IND = WK_IND + 1;
           IF LNKDB_NUMPCB = LNKDB_STKNUMPCB(WK_IND)
            & LNKDB_STKLEV(LNKDB_NUMSTACK) < LNKDB_STKLEV(WK_IND)
           THEN DO;
              LNKDB_FLG_XRST(WK_IND)  = 0;
              LNKDB_STKCURSOR(WK_IND) = ' ';
              LNKDB_STKNUMPCB(WK_IND) = 0;
           END;
        END;
     END;
 END SET_LASTOP;

 /*----------------------------------------------------------------*/
 /*             E R R O R S   S E C T I O N                        */
 /*----------------------------------------------------------------*/
 ROUT_ERROR: PROC;
     IF LNKDB_CODE_ERROR = 'NOOP' THEN
        MSGDB_DLSA =
          'IMStoDB2 - Instruction not found: ' !! LNKDB_OPERATION;

     CERR_PTR = SYS_ERROR('#NOME_RTE#01',MSGDB_DLSA);
     LNKDB_PCBSTAT = 'XI'; /* CICS */
     RETURN;

 END ROUT_ERROR;

 /*------------------------------------------------------------*/
 /*                      PCB MANAGING                          */
 /*------------------------------------------------------------*/
 MANAGE_PCB: PROC;
    WK_IND = 0;
    LNKDB_NUMSTACK = 0;

    DO UNTIL (WK_IND > LNKDB_MAXSTACK);
       WK_IND = WK_IND + 1;
       IF LNKDB_NUMPCB  = LNKDB_STKNUMPCB(WK_IND) &
          WK_NAMETABLE = LNKDB_STKNAME(WK_IND) THEN DO;
          LNKDB_NUMSTACK = WK_IND;     /* NUMSTACK RETRIEVED */
          WK_IND = LNKDB_MAXSTACK + 1; /* EXIT */
       END;
       ELSE DO;
          IF LNKDB_NUMSTACK = 0 & LNKDB_FLG_XRST(WK_IND) = 0 THEN
             LNKDB_NUMSTACK = WK_IND; /* AVAILABLE POSITION */
       END;
    END;
    IF LNKDB_NUMSTACK > LNKDB_MAXSTACK THEN
       LNKDB_MAXSTACK = LNKDB_NUMSTACK;

    IF LNKDB_NUMSTACK = 0 !
       LNKDB_NUMSTACK > HBOUND(LNKDB_STACKAREA) THEN DO;
       /* NO ROOM IN STACK */
       LNKDB_CODE_ERROR = 'MPCB';
       MSGDB_DLSA = 'IMStoDB2 - Manage_Pcb: no room in stack';
       CALL ROUT_ERROR();
    END;
    ELSE DO;
       /* OK: POSITION IN STACK FOUND */
       LNKDB_STKNUMPCB(LNKDB_NUMSTACK) = LNKDB_NUMPCB;
       LNKDB_STKNAME(LNKDB_NUMSTACK)   = WK_NAMETABLE;
       IF LNKDB_STKLEV(LNKDB_NUMSTACK) = ' ' THEN
          LNKDB_STKLEV(LNKDB_NUMSTACK) = WK_SEGLEVEL;
    END;
 END MANAGE_PCB;

 /*------------------------------------------------------------*/
 /*  AREAS TRANSFER ROUTINES                                   */
 /*------------------------------------------------------------*/

    *#COPY_PD_DB2#

 /*------------------------------------------------------------*/
 /*                   RESTORE LAST READ KEY                    */
 /*------------------------------------------------------------*/
  REST_FDBKEY: PROC;
     WK_IND   = 0;
     WINDICE  = 0;
     DO UNTIL (WK_IND >= LNKDB_MAXSTACK) ;
        WK_IND = WK_IND + 1 ;
        IF LNKDB_FLG_XRST(WK_IND) = 1 &
          LNKDB_NUMPCB = LNKDB_STKNUMPCB(WK_IND) &
          WK_NAMETABLE = LNKDB_STKNAME(WK_IND)   THEN DO;
           WINDICE = WK_IND;
           WK_IND  = 50 ;
        END;
     END;

     *#REST_FDBKEY#

 /*------------------------------------------------------------*/
 /*                     SAVE LAST READ KEY                     */
 /*------------------------------------------------------------*/
 STORE_FDBKEY: PROC;

 *#SET_FDBKEY#
 
 /*****************************************************
 * SET KEYFEEDBACK AREA, SEGMENT, LEVEL, RETCODE      *
 *****************************************************/
  SET_PCBINFO: PROC;
     LNKDB_PCBSEGLV = '00';
     IF WK_GNQUAL = 'N' THEN DO;
        LNKDB_PCBFDBKEY = '';
        LNKDB_PCBLGKFB = 0;
        LNKDB_PCBSEGLV = '00';
     END;
     ELSE DO;
        WK_SEGMENT   = '';
        LNKDB_PCBFDBKEY = '';
     END;
  END SET_PCBINFO;
 /*------------------------------------------------------------*/
 /*                   INCLUDE DATE PROC                        */
 /*------------------------------------------------------------*/

  %INCLUDE PDDATERT ;

#INSTR#
 END #NOME_RTE#;