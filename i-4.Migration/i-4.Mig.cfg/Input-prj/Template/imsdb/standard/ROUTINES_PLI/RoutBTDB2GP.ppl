 #NOME_RTE#: PROC (LNKDB_AREA) ;
 /*-----------------------------------------------------------------*/
 /* AUTHOR        : I-4M                                            */
 /* DATE_WRITEN   : 28/09/2005                                      */
 /*-----------------------------------------------------------------*/
 /* LAST MODIFIED : 22/02/2007        (RELEASE 3.0)                 */
 /*                                                                 */
 /*                 DESCRIPTION                                     */
 /*                 ___________                                     */
 /*   1)Generalized BATCH Routine for GNP and GHNP                  */
 /*                                                                 */
 /*   STANDARD TEMPLATE SOURCE                                      */
 /*                                                                 */
 /*-----------------------------------------------------------------*/
 /*                                                                 */
 /*            CODED INSTRUCTIONS INTERNAL TO THE MODULE            */
 /*                                                                 */
 /*-----------------------------------------------------------------*/

 /* --------------------------------------------------------------- */
 /*                    DICHIARATIVE DEI FILES                       */
 /* --------------------------------------------------------------- */
  DCL SYSPRINT EXTERNAL FILE PRINT;       /* SYSPRINT               */

 /* --------------------------------------------------------------- */
 /*                    BUILTIN FUNCTIONS                            */
 /* --------------------------------------------------------------- */
  DCL (DATE,TIME,DATETIME)                             BUILTIN;
  DCL (STRING,SUBSTR,TRANSLATE,REPEAT,HIGH,LENGTH)     BUILTIN;
  DCL (NULL,ONCODE,MULTIPLY,ROUND,MOD,DIVIDE,VERIFY)   BUILTIN;

 /* --------------------------------------------------------------- */
 /*                     RECORDS STRUCTURE                           */
 /* --------------------------------------------------------------- */

 /* -------------------------------- INCLUDE  BY COMPILER LINK AREAS*/
 DCL 01  LNKDB_AREA,
  %INCLUDE LNKDBRT;   /* BUFFER INTERFACE WITH CALLING PROGRAM      */
 DCL   1      LNKDB_DLZPCB_RED BASED(ADDR(LNKDB_DLZPCB)),
         5   LNKDB_PCBDBDNM        CHAR (8),
         5   LNKDB_PCBSEGLV        CHAR (2),
         5   LNKDB_PCBSTAT         CHAR (2),
         5   LNKDB_PCBPROCO        CHAR (4),
         5   FIL_02                CHAR (4),
         5   LNKDB_PCBSEGNM        CHAR (8),
         5   LNKDB_PCBLGKFB        FIXED BIN (31),
         5   LNKDB_PCBSENSG        FIXED BIN (31),
         5   LNKDB_PCBFDBKEY       CHAR (100),
         5   FIL_03                CHAR(4);

 /* -------------------------------- INCLUDE  OTHER AREAS           */
  %INCLUDE ERRDBRT;


           EXEC SQL INCLUDE SQLCA;

           *#COPY_WK_DB2#

  %INCLUDE WKDATERT ;

 /* --------------------------------------------------------------- */
 /*                                                                 */
 /* --------------------------------------------------------------- */

  DCL   WK_GNQUAL          CHAR(001) ;
        WK_GNQUAL = 'S'              ;
  DCL   WK_NAMETABLE       CHAR(016) ;
  DCL   WK_SEGMENT         CHAR(008) ;
  DCL   WK_ISTR            CHAR(004) ;

  DCL 1  WK_SSA                      ,
       3 WK_KEYNAME        CHAR(030) ,
       3 WK_KEYOPER        CHAR(002) ,
       3 WK_KEYVALUE       CHAR(100) ;

  DCL   WK_SQLCODE         FIXED BIN (31);
  DCL   WK_IND             FIXED BIN (31);
  DCL   WINDICE            FIXED BIN (31);
 /* --------------------------------------------------------------- */
 /*                   ON CONDITONS                                  */
 /* --------------------------------------------------------------- */
   DCL W_CONV_SOURCE   CHAR(15);        
   DCL W_CONV_PIC9     PIC'(15)9';      
   DCL W_CONV_CHAR     CHAR(15) ;       
   DCL W_CONV_FIXBIN   FIXED BIN (31);  
   DCL W_CONV_FIXDEC   FIXED DEC (15);  
   DCL WK_CNT_COND     CHAR(1) INIT('N');
   DCL WK_ERR_COND     CHAR(1) INIT('N');
   DCL W_CONV_FIXDECR  CHAR(8) BASED(ADDR(W_CONV_FIXDEC));
   DCL W_CONV_SEGNO    FIXED DEC(1) UNALIGNED;            
   DCL W_CONV_SEGNOR   CHAR(1) BASED(ADDR(W_CONV_SEGNO)); 
   DCL W_CONV_I        FIXED DEC(2);                      
   DCL W_CONV_II       FIXED DEC(2);                      
   
 /* REV_GESTERR
  ON ERROR BEGIN;
    ON ERROR SYSTEM ;
    IF ONCODE = 54
     & WK_CNT_COND = 'Y' THEN DO;
       WK_ERR_COND = 'Y';
       WK_CNT_COND = 'N';
    END;
    ELSE DO;
     PUT SKIP EDIT ('*********************************')  ( A )  ;
     PUT      EDIT ('*********************************')  ( A )  ;
     PUT SKIP EDIT ('*          ERROR SYSTEM          ')  ( A )  ;
     PUT      EDIT ('                                *')  ( A )  ;
     PUT SKIP EDIT ('*      ONCODE :',ONCODE,'         ') (A,A)  ;
     PUT      EDIT ('                                *')  ( A )  ;
     PUT SKIP EDIT ('*********************************')  ( A )  ;
     PUT      EDIT ('*********************************')  ( A )  ;
     STOP                                                         ;
    END;
  END                                                             ;
  REV_GESTERR */

 /* ---------------------------------------------------------------*/
 /*                   M  A  I  N                                   */
 /* ---------------------------------------------------------------*/

  CALL INIT_AREAS ;

        *    #OPE#

     OTHERWISE DO                    ;
          LNKDB_CODE_ERROR = 'NOOP' ;
          CALL ROUT_ERROR            ;
     END;
  END;
  GOTO Z999_RETURN;

 /*----------------------------------------------------------------*/
 /*             R E T U R N   T O   C A L L E R                    */
 /*----------------------------------------------------------------*/
 /* FINE_MAIN */
 /*----------------------------------------------------------------*/
 /*             A R E A S   I N I T I A L I Z A T I O N            */
 /*----------------------------------------------------------------*/
 INIT_AREAS: PROC;
    LNKDB_ABEND_RTCODE = ' '                                  ;
    WK_SQLCODE         = ''                                   ;
    WK_GNQUAL          ='S'                                   ;
    IF LNKDB_STATE ^> '' THEN DO                              ;
       LNKDB_ACTIVE = ''                                      ;
       LNKDB_STATE  = 'A'                                     ;
       LNKDB_NUMSTACK = 0                                     ;
    END                                                       ;
 END INIT_AREAS;

/*----------------------------------------------------------------*/
/*             P R E P A R E   E X I T   A R E A S                */
/*            S E T   T H E   R E T U R N   C O D E               */
/*----------------------------------------------------------------*/
 PREPARE_EXIT: PROC;
    WK_IND = 1                                                ;
    DO UNTIL (WK_IND > MAX_SQLRESP)                           ;
       IF WK_SQLCODE = ERRDB_CODRESP(WK_IND) THEN DO          ;
          LNKDB_PCBSTAT = ERRDB_CODDLI(WK_IND)                ;
          WK_IND        = 2000                                ;
       END                                                    ;
       ELSE DO                                                ;
          IF ERRDB_CODRESP(WK_IND) = -999999 THEN DO          ;
             LNKDB_PCBSTAT = ERRDB_CODDLI(WK_IND)             ;
             WK_IND        = 2000                             ;
          END                                                 ;
       END                                                    ;
       WK_IND = WK_IND + 1                                    ;
    END                                                       ;
    IF WK_SQLCODE ^= 0 &
       WK_SQLCODE ^= +100 &
       WK_SQLCODE ^= -803 THEN DO ;
       DISPLAY ('************************************************');
       DISPLAY ('***                                          ***');
       DISPLAY ('*** I-4M IMS CONVERSION                      ***');
       DISPLAY ('***   ROUTINE: TEST                          ***');
       DISPLAY ('***   CALLER: ' !! LNKDB_CALLER);
       DISPLAY ('***   OPERATION: ' !! LNKDB_OPERATION);
       DISPLAY ('***                                          ***');
       DISPLAY ('***        NOT MANAGED SQLCODE ');
       DISPLAY ('***                                          ***');
       DISPLAY ('*** SQLCODE = ' !! WK_SQLCODE);
       DISPLAY ('***                                          ***');
       DISPLAY ('*** SQLERRM = ' !! SQLERRM);
       DISPLAY ('***                                          ***');
       DISPLAY ('************************************************');
    END;

    LNKDB_PCBSEGNM = WK_SEGMENT                               ;
    IF LNKDB_PCBSTAT = 'GE' THEN DO                           ;
       IF WK_ISTR = 'GN' &  WK_GNQUAL = 'N' THEN DO      ;
          LNKDB_PCBSTAT = 'GB'                           ;
       END                                               ;
    END                                                       ;
    IF LNKDB_PCBSTAT ^= '  ' THEN DO                       ;
       LNKDB_PCBFDBKEY = ''                                   ;
    END                                                       ;
    CALL SET_LASTOP                                           ;
 END PREPARE_EXIT;

 /* ---------------------------------------------------------- */
 /*        BACKUP ROUTINE OF LAST OPER IN LINKAGE AREA         */
 /* ---------------------------------------------------------- */
 SET_LASTOP: PROC;
     IF WK_ISTR = 'GU' !
        WK_ISTR = 'ISRT' THEN DO                         ;
        LNKDB_STKCURSOR(LNKDB_NUMSTACK) = ''                    ;
     END                                                        ;
     IF WK_ISTR = 'GU' !
        WK_ISTR = 'ISRT' !
        WK_ISTR = 'GN' !
        WK_ISTR = 'GNP' THEN DO          ;
        WK_IND = 0                                              ;
        DO UNTIL (WK_IND > 49)                                  ;
           WK_IND = WK_IND + 1                                  ;
           IF LNKDB_NUMPCB = LNKDB_STKNUMPCB(WK_IND)
            & LNKDB_STKLEV(LNKDB_NUMSTACK) < LNKDB_STKLEV(WK_IND)
           THEN DO                                               ;
              LNKDB_STKCURSOR(WK_IND) = ' '                     ;
              LNKDB_STKLASTOP(WK_IND) = ' '                     ;
              LNKDB_STKSSA(WK_IND)    = ' '                     ;
           END                                                  ;
        END                                                     ;
     END                                                        ;
     LNKDB_STKLASTOP(LNKDB_NUMSTACK)= WK_ISTR                   ;
 END SET_LASTOP;

 /*----------------------------------------------------------------*/
 /*             E R R O R S   S E C T I O N                        */
 /*----------------------------------------------------------------*/

 ROUT_ERROR: PROC;
     IF LNKDB_CODE_ERROR = 'NOOP' THEN DO ;
        LNKDB_RTIOC    = MSGDB_NOOP       ;
        ACTION_ERRORE  = ACTION_NOOP      ;
        GOTO ROUT_ERR01                   ;
     END                                  ;
     IF LNKDB_CODE_ERROR = 'DLSA' THEN DO ;
        LNKDB_RTIOC   = MSGDB_DLSA        ;
        ACTION_ERRORE = ACTION_DLSA       ;
        GOTO ROUT_ERR01                   ;
     END                                  ;
     LNKDB_RTIOC      = MSGDB_NOER        ;
/*   LNKDB_CODE_ERROR = CODDB_NOER        ; */
     ACTION_ERRORE    = ACTION_NOER       ;
   ROUT_ERR01:
     PUT SKIP EDIT (' NAME_RTE ERROR: '
                   !! LNKDB_CODE_ERROR)
                   (A)                    ;
     IF ACTION_ERRORE = 1 THEN LNKDB_ABEND_RTCODE = 'ADL0';
 END ROUT_ERROR;

 ROUT_ABEND: PROC;
    LNKDB_CODE_ERROR = 'ADL1';
    GOTO Z999_RETURN;
 END ROUT_ABEND;

 /*------------------------------------------------------------*/
 /*                      PCB MANAGING                          */
 /*------------------------------------------------------------*/

 MANAGE_PCB: PROC;
     WK_IND           = 0                          ;
     LNKDB_NUMSTACK   = 1                          ;

     DO UNTIL (WK_IND > 49)                        ;
        WK_IND = WK_IND + 1                        ;
        IF LNKDB_NUMPCB  = LNKDB_STKNUMPCB(WK_IND)
         & WK_NAMETABLE = LNKDB_STKNAME(WK_IND)
         ! LNKDB_STKNUMPCB(WK_IND) = 0 THEN DO     ;
           LNKDB_NUMSTACK = WK_IND                 ;
           WK_IND         = 50                     ;
        END                                        ;
     END                                           ;
     LNKDB_STKNUMPCB(LNKDB_NUMSTACK) = LNKDB_NUMPCB;
     LNKDB_STKNAME(LNKDB_NUMSTACK)   = WK_NAMETABLE;
 END MANAGE_PCB;

 /*------------------------------------------------------------*/
 /*  AREAS TRANSFER ROUTINES                                   */
 /*------------------------------------------------------------*/

    *#COPY_PD_DB2#

 /*------------------------------------------------------------*/
 /*                   RESTORE LAST READ KEY                    */
 /*------------------------------------------------------------*/
  REST_FDBKEY: PROC;

 *#REST_FDBKEY#

 /*------------------------------------------------------------*/
 /*                     SAVE LAST READ KEY                     */
 /*------------------------------------------------------------*/
 STORE_FDBKEY: PROC;

 *#SET_FDBKEY#

 /*------------------------------------------------------------*/
 /*                   INCLUDE DATE PROC                        */
 /*------------------------------------------------------------*/

  %INCLUDE PDDATERT ;
  %INCLUDE LCPSEGNO ;

 /*----------------------------------------------------------------*/
 /*             R E T U R N   T O   C A L L E R                    */
 /*----------------------------------------------------------------*/
 Z999_RETURN: ;
 END #NOME_RTE#;