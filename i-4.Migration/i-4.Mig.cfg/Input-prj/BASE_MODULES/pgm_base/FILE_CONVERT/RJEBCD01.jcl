#**********************************************************
#*   PROGRAM RBFUND00 FOR DLI TO RDBMS(ORACLE) DATA LOAD    
#*   INPUT - IMS/DB UNLOAD: MFDFUND                     
#*   OUTPUT SEQUENTIAL FILE FOR LOAD ORACLE
#**********************************************************
#**********************************************************
#*   I-4M
#**********************************************************

BEGINJOB mode='MVS' jobclass='X'

LIBDEF scope='JOB' type='PGM' dataset='BPH.BATCH.LOAD' lib='\$HOME/load/split'

#  *

################################################################################
LABEL name=A1
################################################################################

#  *
ASSGNDD ddname='SYSIN' type='DUMMY'

ASSGNDD ddname='SYSOUT' type='SYSOUT' class='JOBCLASS'

#  *

ASSGNDD ddname='REDFILIN' filename='\$CONVSEQ/redfile/MFCDXRF' disp='i-o' 

ASSGNDD ddname='REDFILOU' filename='\$CONVSEQ/redfile/MFCDXRF.pck' disp='o' normal='k' recfmt='F' recsize='80'

EXECPGM pgmname='ITERFORM' stepname='A1'

################################################################################
LABEL name=A2
################################################################################

#  *
ASSGNDD ddname='SYSIN' type='DUMMY'

ASSGNDD ddname='SYSOUT' type='SYSOUT' class='JOBCLASS'

#  *

ASSGNDD ddname='INPFILE' filename='\$CONVSEQ/ebcdic/INRT7524.DLRTBL' disp='i-o' 

ASSGNDD ddname='OUTFILE' filename='\$CONVSEQ/ascii/MFCDXRF' disp='o' normal='k' recfmt='F' recsize='1'

ASSGNDD ddname='STRUCT' filename='\$CONVSEQ/structure/MFCDXRF' disp='i-o' 

ASSGNDD ddname='REDSTRUC' filename='\$CONVSEQ/structure/RED_MFCDXRF' disp='i-o' 

ASSGNDD ddname='REDFILE' filename='\$CONVSEQ/redfile/MFCDXRF.pck' disp='i-o' 

ASSGNDD ddname='LOGFILE' filename='\$CONVSEQ/log/MFCDXRF.log' disp='o' normal='k' recfmt='F' recsize='80'

EXECPGM pgmname='ITERCONV' stepname='A2'

ENDJOB


