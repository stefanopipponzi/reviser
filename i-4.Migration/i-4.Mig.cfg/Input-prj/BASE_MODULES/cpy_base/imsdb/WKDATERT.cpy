       01  WK-DATE-DLI-Z8.
           05 WK-SS        PIC 99    VALUE ZEROES.
           05 WK-AA        PIC 99    VALUE ZEROES.
           05 WK-MM        PIC 99    VALUE ZEROES.
           05 WK-GG        PIC 99    VALUE ZEROES.
       01  WK-DATE-DLI-98 REDEFINES WK-DATE-DLI-Z8
                           PIC 9(8).

       01  WK-DATE-DLI-P9  PIC S9(9) COMP-3
                                     VALUE ZEROES.

       01  WK-DATE-DB2.
           05 WK-GG        PIC 99    VALUE ZEROES.
           05 FILLER       PIC X     VALUE '.'.
           05 WK-MM        PIC 99    VALUE ZEROES.
           05 FILLER       PIC X     VALUE '.'.
           05 WK-SS        PIC 99    VALUE ZEROES.
           05 WK-AA        PIC 99    VALUE ZEROES.

       01  WK-DATE-DLI-Z10.
           05 WK-GG        PIC 99    VALUE ZEROES.
           05 FILLER       PIC X     VALUE SPACES.
           05 WK-MM        PIC 99    VALUE ZEROES.
           05 FILLER       PIC X     VALUE SPACES.
           05 WK-SS        PIC 99    VALUE ZEROES.
           05 WK-AA        PIC 99    VALUE ZEROES.

       01  WK-DATE-DLI-X10 PIC X(10) VALUE SPACES.

       01  WK-DATE-DLI-Z6.
           05 WK-AA        PIC 99    VALUE ZEROES.
           05 WK-MM        PIC 99    VALUE ZEROES.
           05 WK-GG        PIC 99    VALUE ZEROES.

       01  WK-DATE-DLI-X6  PIC X(6)  VALUE SPACES.

       01  WK-DATE-DLI-X5  PIC X(5)  VALUE SPACES.

       01  WK-SYSTEM-DATE.
           05  WK-AA       PIC XX    VALUE ZEROES.
           05  WK-MM       PIC XX    VALUE ZEROES.
           05  WK-GG       PIC XX    VALUE ZEROES.
           
       01  WK-PRINT-DATE.
           05 WK-GG        PIC 99    VALUE ZEROES.
           05 FILLER       PIC X     VALUE "/".
           05 WK-MM        PIC 99    VALUE ZEROES.
           05 FILLER       PIC X     VALUE "/".
           05 WK-SS        PIC 99    VALUE ZEROES.
           05 WK-AA        PIC 99    VALUE ZEROES.
           
       01  WK-SYSTEM-TIME.
           05  WK-HH       PIC XX    VALUE ZEROES.
           05  WK-MM       PIC XX    VALUE ZEROES.
           05  WK-SS       PIC XX    VALUE ZEROES.

       01  WK-PRINT-TIME.
           05 WK-HH        PIC 99    VALUE ZEROES.
           05 FILLER       PIC X     VALUE ".".
           05 WK-MM        PIC 99    VALUE ZEROES.
           05 FILLER       PIC X     VALUE ".".
           05 WK-SS        PIC 99    VALUE ZEROES.
           
       01  WK-DATE-ERROR   PIC X     VALUE SPACES.

       01  WK-RISULTATO    PIC 9(4)  VALUE ZEROES.
       01  WK-RESTO        PIC 9(4)  VALUE ZEROES.
         
       01  WK-TAB-GIORNI.
           05 FILLER       PIC X(24)
              VALUE '312831303130313130313031'.
       01  FILLER REDEFINES WK-TAB-GIORNI.
           05 WK-GIORNI    PIC 99
                           OCCURS 12.       