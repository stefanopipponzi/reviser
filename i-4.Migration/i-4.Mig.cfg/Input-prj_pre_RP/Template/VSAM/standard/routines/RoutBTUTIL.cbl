       IDENTIFICATION DIVISION.
       PROGRAM-ID.    #NOME_RTE#.
       AUTHOR.        #LABEL_AUTHOR#.
       DATE-WRITTEN.  #DATA_CREAZ#.
       DATE-COMPILED. #DATA_COM#.
       
      *REMARKS.
      **--------------------------------------------------------------**
      **  LAST MODIFICATION DATE:   22 07 2005        (RELEASE 3.0)   **
      **                                                              **
      **  EXECUTED BY           :   Development and support group     **
      **                            Mainframe Affinity                **
      **                                                              **
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  CICS Routine for UTILITY commands                           **
      **                                                              **
      **  DB2 VERSION                                                 ** 
      **                                                              **
      **--------------------------------------------------------------**
      **                                                              **
      **         CODED INSTRUCTIONS INTERNAL TO THE MODULE            ** 
      **                                                              **
      **--------------------------------------------------------------**
      ** 
      **  #CODE#   
      **
      **--------------------------------------------------------------**
 
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.

      /----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.
       
       01 START-WS PIC X(70) 
              VALUE '*** START WORKING STORAGE #NOME_RTE# ***'.

           COPY ERRDBRT.

       01  WK-GNQUAL          PIC X VALUE 'S'.
       
       01  WK-NAMETABLE       PIC X(16).
       01  WK-SEGMENT         PIC X(8).
       01  WK-ISTR            PIC X(4).

       01  WK-SSA.
           03 WK-KEYNAME      PIC X(030).
           03 WK-KEYOPER      PIC X(002).
           03 WK-KEYVALUE     PIC X(100).

           EXEC SQL INCLUDE SQLCA END-EXEC.
 

       01  WK-SQLCODE        PIC S9(8) COMP.
       01  WK-IND            PIC S9(8) COMP.
       01  WINDICE           PIC S9(8) COMP.

       01  WK-OPERATION      PIC X(4)  VALUE SPACES.
       01  WK-PSBNAME        PIC X(8)  VALUE SPACES.       
       01  WK-CHKPID         PIC X(8)  VALUE SPACES.
       01  WK-NUMPCB         PIC S9(4) COMP VALUE ZEROES.

       01  WK-DATA-AREA1     PIC X(100)    VALUE SPACES.
 
           EXEC SQL INCLUDE WKDATERT END-EXEC.

       LINKAGE SECTION.

       01  AREA-CUSTDB2.
           COPY LNKDBRT.
           COPY DLIUIB.
           
       01  PCB-AREA.
         05 DBD-NAME       PIC X(8).                                          
         05 SEG-LEVEL      PIC XX.                                            
         05 ST-CODE        PIC XX.                                            
         05 PROC-OPT       PIC X(4).                                           
         05 R-DLI          PIC S9(5) COMP.                                   
         05 SEG-NAME       PIC X(8).                                          
         05 L-KEY          PIC S9(5) COMP.                            
         05 SEN-SEG        PIC S9(5) COMP.                            
         05 KEFEDBAK       PIC X(100).                               
         
      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-CUSTDB2.

       MAIN-START.
       1000-MAIN-START SECTION.
      *----------------------------------------------------------------*
           PERFORM 4000-INIT-AREAS.
      
      *    COMMAND RECOGNITION AND
      *    RELATIVE SECTION ACTIVATION

      *    #OPE#    
 
           MOVE 'NOOP' TO LNKDB-CODE-ERROR.
           PERFORM 8000-ROUT-ERROR.
           PERFORM 3000-MAIN-END.
                
       2000-MAIN-001 SECTION.
           PERFORM 5000-PREPARE-EXIT.

      *----------------------------------------------------------------*

       3000-MAIN-END SECTION.
           PERFORM 7000-RETURN.

      *----------------------------------------------------------------*
      *             A R E A S   I N I T I A L I Z A T I O N
      *----------------------------------------------------------------*
       4000-INIT-AREAS SECTION.
           
           MOVE SPACES TO LNKDB-ABEND-RTCODE.
           MOVE ZERO TO WK-SQLCODE.

           PERFORM 9999-SYSTEM-DATE

           MOVE "BATCH"       TO  WK-USERID.

      *    MOVE LNKDB-CALLER  TO  WK-PGMCREAZ.       

           MOVE 'S' TO WK-GNQUAL.

           IF LNKDB-STATE NOT GREATER SPACES
              INITIALIZE LNKDB-ACTIVE
              MOVE 'A' TO LNKDB-STATE
              MOVE ZERO TO LNKDB-NUMSTACK
           END-IF.

      *----------------------------------------------------------------*
      *             P R E P A R E   E X I T   A R E A S
      *            S E T   T H E   R E T U R N   C O D E
      *----------------------------------------------------------------*
       5000-PREPARE-EXIT SECTION.
           MOVE 1 TO WK-IND 
           PERFORM UNTIL WK-IND GREATER MAX-SQLRESP
               IF WK-SQLCODE = ERRDB-CODRESP(WK-IND) 
                  MOVE ERRDB-CODDLI(WK-IND) TO LNKDB-PCBSTAT
                  MOVE 2000 TO WK-IND
               ELSE 
                 IF ERRDB-CODRESP(WK-IND) = -999999                  
                    MOVE ERRDB-CODDLI(WK-IND)  TO LNKDB-PCBSTAT
                    MOVE 2000 TO WK-IND
                 END-IF
               END-IF                 
               ADD 1 TO WK-IND
           END-PERFORM

           MOVE WK-SEGMENT TO LNKDB-PCBSEGNM.
           
           EVALUATE LNKDB-PCBSTAT
               WHEN 'GE'
                   IF WK-ISTR = 'GN' AND
                      WK-GNQUAL = 'N'
                         MOVE 'GB' TO LNKDB-PCBSTAT
                   END-IF
           END-EVALUATE.
           
           IF LNKDB-PCBSTAT NOT = '  '
              MOVE SPACES TO LNKDB-FDBKEY
           END-IF.
           
           PERFORM 6000-SET-LASTOP.

      *------------------------------------------------------------*
      *  BACKUP ROUTINE OF LAST OPER IN LINKAGE AREA
      *------------------------------------------------------------*
       SET-LASTOP.

           MOVE WK-ISTR TO LNKDB-STKLASTOP(LNKDB-NUMSTACK).

      *----------------------------------------------------------------*
      *             R E T U R N   T O   C A L L E R
      *----------------------------------------------------------------*
       
       7000-RETURN SECTION.
           GOBACK.
           
      *----------------------------------------------------------------*
      *             E R R O R S   S E C T I O N
      *----------------------------------------------------------------*

       8000-ROUT-ERROR SECTION.
           IF LNKDB-CODE-ERROR = 'NOOP'
              CALL 'NOOP'
           END-IF.
   
           IF LNKDB-CODE-ERROR = 'DLSA'
                   MOVE MSGDB-DLSA TO LNKDB-RTIOC
                   MOVE ACTION-DLSA TO ACTION-ERRORE
           ELSE
                   MOVE MSGDB-NOER TO LNKDB-RTIOC 
                   MOVE CODDB-NOER TO LNKDB-CODE-ERROR 
                   MOVE ACTION-NOER TO ACTION-ERRORE
           END-IF.       
            
           DISPLAY ' NAME-RTE ERROR: ' LNKDB-CODE-ERROR.
           IF ACTION-ERRORE = 1
              MOVE 'ADL0' TO LNKDB-ABEND-RTCODE
           ELSE   
              MOVE 'ADL1' TO LNKDB-CODE-ERROR.
           END-IF.

           PERFORM 7000-RETURN.


       ROUT-ABEND.
           MOVE 'ADL1' TO LNKDB-CODE-ERROR.
           PERFORM Z999-RETURN THRU Z999-RETURN-EX.
       ROUT-ABEND-EX.
           EXIT.
      
