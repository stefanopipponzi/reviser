      *--------------------------------------------------------------*
      *  LAST MODIFICATION DATE:   DATA-MODIF  	      ( RELEASE 2.1 )*
      *                                                              *
      *  EXECUTED BY           :   Development and support group     *
      *                            Mainframe Affinity                *
      *                                                              *
      *--------------------------------------------------------------*
      *                                                              *
      *   		USER PROGRAM INTERFACE AREA                  *
      *             FOR GENERALIZED ROUTINES OF I-O IMS              *
      *                                                              *
      *--------------------------------------------------------------*

           02       LNKDB-ENVIRONMENT.
             05     LNKDB-MODE            PIC X(2).
             05     LNKDB-CALLER          PIC X(8).

      *--------------------------------------------------------------*
           02       LNKDB-COMMAND.
             05     LNKDB-OPERATION       PIC X(50).

             05     LNKDB-PTRPCB          POINTER.
             05     LNKDB-NUMPCB          PIC S9(4) COMP.
             05     LNKDB-PSBNAME         PIC X(8).
             05     LNKDB-CHKPID          PIC X(8).
             05     LNKDB-PARAMETERS      OCCURS 5.
               10   LNKDB-SEGNAME         PIC X(8).  
               10   LNKDB-SEGLEVEL        PIC X(2).  
               10   LNKDB-DATA-AREA       PIC X(12000).
               10   LNKDB-AREALENGTH      PIC S9(4) COMP.
               10   LNKDB-KEYRICH         OCCURS 6.
                 15 LNKDB-KEYNAME         PIC X(30).
                 15 LNKDB-KEYVALUE        PIC X(100).
                 15 LNKDB-KEYLENGTH       PIC S9(4) COMP.
                 15 LNKDB-KEYOPER         PIC X(2).

      *--------------------------------------------------------------*
           02       LNKDB-ACTIVE.
             05     LNKDB-NUMSTACK        PIC 99.
             05     LNKDB-STATE           PIC X.
             05     LNKDB-STACKAREA       OCCURS 50.
               10   LNKDB-STKNAME         PIC X(18).
               10   LNKDB-STKTBNUM        PIC XX.
               10   LNKDB-STKNUMPCB       PIC S9(4) COMP.
               10   LNKDB-STKKEYLEN       PIC S9(4) COMP.
               10   LNKDB-STKLASTOP       PIC X(4).
               10   LNKDB-STKCURSOR       PIC X.
               10   LNKDB-STKNUMISTR      PIC 9(8).
               10   LNKDB-STKFDBKEY       PIC X(100).
               10   LNKDB-STKPTGKEY       PIC X(100).
               10   LNKDB-STKSSA          PIC X(400).
               10   LNKDB-STKLEV          PIC X(2).

      *--------------------------------------------------------------*
           02       LNKDB-FEEDBACK.
             05     LNKDB-RETCODE.
               10   LNKDB-CODE-ERROR      PIC X(4).
               10   FILLER                PIC X(36).

             05     LNKDB-DLZDIB REDEFINES LNKDB-RETCODE.
               10   LNKDB-DIBVER          PIC X(2).
               10   LNKDB-DIBSTAT         PIC X(2).
               10   LNKDB-DIBSEGM         PIC X(8).
               10   LNKDB-DIBFIL01        PIC X(1).
               10   LNKDB-DIBFIL02        PIC X(1).
               10   LNKDB-DIBSEGLV        PIC X(2).
               10   LNKDB-DIBKFBL         PIC S9(4) COMP.
               10   LNKDB-DIBDBDNM        PIC X(8).
               10   LNKDB-DIBDBORG        PIC X(8).
               10   LNKDB-DIBFIL03        PIC X(6).

             05     LNKDB-DLZPCB REDEFINES LNKDB-RETCODE.
               10   FILLER                PIC X(2).
               10   LNKDB-PCBSTAT         PIC X(2).
               10   LNKDB-PCBSEGNM        PIC X(8).
               10   FILLER                PIC X(1).
               10   FILLER                PIC X(1).
               10   LNKDB-PCBSEGLV        PIC X(2).
               10   LNKDB-PCBLGKFB        PIC S9(4) COMP.
               10   LNKDB-PCBDBDNM        PIC X(8).
               10   FILLER                PIC X(8).
               10   LNKDB-PCBPROCO        PIC X(4).
               10   LNKDB-PCBSENSG        PIC S9(4) COMP.

             05     LNKDB-SQLRESP REDEFINES LNKDB-RETCODE.
               10   LNKDB-RTSQL           PIC S9(8) COMP.
               10   LNKDB-MSGSQL          PIC X(36).

             05     LNKDB-ABEND-RTCODE    PIC X(4).
             05     LNKDB-RTIOC           PIC X(56).

             05     LNKDB-FDBKEY          PIC X(100).

      *---------   END COPY -----------------------------------------*
