VERSION 5.00
Begin VB.Form MadmdF_UpdateTab 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   1005
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   7590
   Icon            =   "MadmdF_UpdateTab.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1005
   ScaleWidth      =   7590
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cmbBoolean 
      Height          =   315
      Index           =   0
      ItemData        =   "MadmdF_UpdateTab.frx":000C
      Left            =   2670
      List            =   "MadmdF_UpdateTab.frx":0016
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   150
      Visible         =   0   'False
      Width           =   2085
   End
   Begin VB.TextBox lbField 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      ForeColor       =   &H00C00000&
      Height          =   345
      Index           =   0
      Left            =   60
      MultiLine       =   -1  'True
      TabIndex        =   3
      Top             =   120
      Width           =   2565
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   405
      Left            =   6390
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   540
      Width           =   1155
   End
   Begin VB.CommandButton cmdTab 
      Caption         =   "Confirm"
      Default         =   -1  'True
      Height          =   405
      Left            =   6390
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   90
      Width           =   1155
   End
   Begin VB.TextBox txtNewValue 
      Height          =   345
      Index           =   0
      Left            =   2670
      TabIndex        =   0
      Top             =   120
      Width           =   3585
   End
End
Attribute VB_Name = "MadmdF_UpdateTab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public txtNum As Long
Dim TabName As String
Dim NomiCampi() As String
Dim ValoriCampi() As String
Public update_insert As Integer


Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdTab_Click()
On Error GoTo ErrorHandler
Dim boolInsert As Boolean
Dim boolUpdate As Boolean
Dim varCampi As Variant, varValori As Variant, varUPdate As Variant, varBeforUpdate As Variant
Dim i As Long
'HO PREVISTO SOLO 2 FORMATI DI VARIABILE NEL db...
'TYPE 3 = NUMERICO
'TYPE 202 = TESTO

On Error GoTo ErrorHandler
    If update_insert = 1 Then 'insert
    
        varCampi = FieldNames(0).Name
        For i = 1 To UBound(FieldNames)
                varCampi = varCampi & "," & FieldNames(i).Name
        Next
        
        
        If FieldNames(0).type = "3" Then
                varValori = CInt(txtNewValue(0).text)
        ElseIf FieldNames(0).type = "202" Then
                varValori = "'" & txtNewValue(0) & "'"
        ElseIf FieldNames(0).type = "11" Then
              If cmbBoolean(i).text = "True" Then
                varValori = varValori & ", " & "True"
              Else
                varValori = varValori & ", " & "False"
              End If
        End If
        
        For i = 1 To UBound(FieldValues)
            If FieldNames(i).type = "3" Then
                    varValori = varValori & "," & CInt(txtNewValue(i).text)
            ElseIf FieldNames(i).type = "202" Then
                    varValori = varValori & ", '" & txtNewValue(i) & "'"
            ElseIf FieldNames(i).type = "11" Then
                If cmbBoolean(i).text = "True" Then
                  varValori = varValori & ", " & "True"
                Else
                  varValori = varValori & ", " & "False"
                End If
            End If
            
        Next
    
        m_fun.FnConnection.Execute ("INSERT INTO " & TabName & " ( " & varCampi & " ) VALUES ( " & varValori & " );")

    
    Else 'update

        For i = 0 To UBound(FieldNames) - 1
            If FieldNames(i).type = "3" Then
                varUPdate = varUPdate & FieldNames(i).Name & "=" & txtNewValue(i) & " , " 'presumo che ci siano pi� campi??!!
            ElseIf FieldNames(i).type = "202" Then
                varUPdate = varUPdate & FieldNames(i).Name & checkNull(txtNewValue(i)) & ","      'presumo che ci siano pi� campi??!!
            ElseIf FieldNames(i).type = "11" Then
               varUPdate = varUPdate & FieldNames(i).Name & "= " & IIf(cmbBoolean(i).text = "True", "True", "False") & " , "
            End If
        Next
            If FieldNames(i).type = "3" Then
                varUPdate = varUPdate & FieldNames(UBound(FieldNames)).Name & "=" & txtNewValue(UBound(FieldNames))
            ElseIf FieldNames(i).type = "202" Then
               varUPdate = varUPdate & FieldNames(UBound(FieldNames)).Name & checkNull(txtNewValue(UBound(FieldNames)))
            ElseIf FieldNames(i).type = "11" Then
               varUPdate = varUPdate & FieldNames(UBound(FieldNames)).Name & "=" & IIf(cmbBoolean(UBound(FieldNames)).text = "True", "True", "False")
            End If
        
        For i = 0 To UBound(FieldNames) - 1
            If FieldNames(i).type = "3" Then
                varBeforUpdate = varBeforUpdate & FieldNames(i).Name & "=" & FieldValues(i) & " and "
            ElseIf FieldNames(i).type = "202" Then
                varBeforUpdate = varBeforUpdate & FieldNames(i).Name & checkNull(FieldValues(i)) & " and "
            ElseIf FieldNames(i).type = "11" Then
               varBeforUpdate = varBeforUpdate & FieldNames(i).Name & "=" & IIf(FieldValues(i) = "True", "True", "False") & " and "
            End If
        Next
            If FieldNames(i).type = "3" Then
                varBeforUpdate = varBeforUpdate & FieldNames(UBound(FieldNames)).Name & "=" & FieldValues(UBound(FieldNames))
            ElseIf FieldNames(i).type = "202" Then
                varBeforUpdate = varBeforUpdate & FieldNames(UBound(FieldNames)).Name & checkNull(FieldValues(UBound(FieldNames)))
            ElseIf FieldNames(i).type = "11" Then
                varBeforUpdate = varBeforUpdate & FieldNames(UBound(FieldNames)).Name & "=" & IIf(FieldValues(UBound(FieldNames)) = "True", "True", "False")
            End If
            
'            If Mid(varBeforUpdate, Len(varBeforUpdate) - 3, 3) = "and" Then
'              varBeforUpdate = Mid(varBeforUpdate, 1, Len(varBeforUpdate) - 4)
'            End If
          
'        m_fun.Open_Recordset ("UPDATE " & TabName & " SET " & varUPdate & "  WHERE  " & varBeforUpdate & " ;")
    m_fun.FnConnection.Execute ("UPDATE " & TabName & " SET " & varUPdate & "  WHERE  " & varBeforUpdate & " ;")
    End If
    
     MadmdF_DLI.displayTab True
    
    Unload Me
    Exit Sub
ErrorHandler:
    MsgBox " " & ERR.Description, vbCritical
    Exit Sub
   ' Unload Me
End Sub
Public Function checkNull(Campo As String) As String
If UCase(Campo) = "NULL" Then
  checkNull = " is null"
Else
  checkNull = "= '" & Campo & "'"
End If
End Function

Public Function Start(update_insert As Integer)
Dim i As Long
    TabName = NomeTab
'    NomiCampi() = FieldNames
'    ValoriCampi() = FieldValues
    Me.update_insert = update_insert '1 = Insert
    
    For i = 1 To UBound(FieldNames)
        Load lbField(i)
        lbField(i).Visible = True
        lbField(i) = FieldNames(i).Name & " - Type: " & ConvType(FieldNames(i).type) & " - Len: " & FieldNames(i).len
        lbField(i).Move lbField(i - 1).Left, (lbField(i - 1).Top + lbField(i - 1).Height * 1.3), lbField(i - 1).Width, lbField(i - 1).Height
    Next
    lbField(0) = FieldNames(0).Name & " - Type: " & ConvType(FieldNames(0).type) & " - Len: " & FieldNames(0).len
    
    For i = 1 To UBound(FieldValues)
        Load txtNewValue(i)
        Load cmbBoolean(i)
        cmbBoolean(i).AddItem "True"
        cmbBoolean(i).AddItem "False"
        cmbBoolean(i).ListIndex = 0
        txtNewValue(i).Visible = True
        txtNewValue(i).text = FieldValues(i)
        txtNewValue(i).Move txtNewValue(i - 1).Left, (txtNewValue(i - 1).Top + txtNewValue(i - 1).Height * 1.3), txtNewValue(i - 1).Width, txtNewValue(i - 1).Height
        cmbBoolean(i).Move cmbBoolean(i - 1).Left, (cmbBoolean(i - 1).Top + cmbBoolean(i - 1).Height * 1.3), cmbBoolean(i - 1).Width
        If FieldNames(i).type = 11 Then
          cmbBoolean(i).Visible = True
          If FieldValues(i) = "" Or FieldValues(i) = "True" Then
            cmbBoolean(i).ListIndex = 0
          Else
            cmbBoolean(i).ListIndex = 1
          End If
          
          txtNewValue(i).Visible = False
        End If
    Next
    txtNewValue(0).text = FieldValues(0)
    
    Me.Height = txtNewValue(UBound(FieldValues)).Top + 3 * txtNewValue(UBound(FieldValues)).Height
    
    'CARICARE TANTE TXT QUANTI SONO I CAMPI E RIEMPIRLI CON I VALORI
    Me.Show vbModal
End Function

Private Function ConvType(ByVal TypeVal As Long) As String
  Select Case TypeVal
        Case adBigInt                    ' 20
            ConvType = "Big Integer"
        Case adBinary                    ' 128
            ConvType = "Binary"
        Case adBoolean                   ' 11
            ConvType = "Boolean"
        Case adBSTR                      ' 8 i.e. null terminated string
            ConvType = "Text"
        Case adChar                      ' 129
            ConvType = "Text"
        Case adCurrency                  ' 6
            ConvType = "Currency"
        Case adDate                      ' 7
            ConvType = "Date/Time"
        Case adDBDate                    ' 133
            ConvType = "Date/Time"
        Case adDBTime                    ' 134
            ConvType = "Date/Time"
        Case adDBTimeStamp               ' 135
            ConvType = "Date/Time"
        Case adDecimal                   ' 14
            ConvType = "Float"
        Case adDouble                    ' 5
            ConvType = "Float"
        Case adEmpty                     ' 0
            ConvType = "Empty"
        Case adError                     ' 10
            ConvType = "Error"
        Case adGUID                      ' 72
            ConvType = "GUID"
        Case adIDispatch                 ' 9
            ConvType = "IDispatch"
        Case adInteger                   ' 3
            ConvType = "Integer"
        Case adIUnknown                  ' 13
            ConvType = "Unknown"
        Case adLongVarBinary             ' 205
            ConvType = "Binary"
        Case adLongVarChar               ' 201
            ConvType = "Text"
        Case adLongVarWChar              ' 203
            ConvType = "Text"
        Case adNumeric                  ' 131
            ConvType = "Long"
        Case adSingle                    ' 4
            ConvType = "Single"
        Case adSmallInt                  ' 2
            ConvType = "Small Integer"
        Case adTinyInt                   ' 16
            ConvType = "Tiny Integer"
        Case adUnsignedBigInt            ' 21
            ConvType = "Big Integer"
        Case adUnsignedInt               ' 19
            ConvType = "Integer"
        Case adUnsignedSmallInt          ' 18
            ConvType = "Small Integer"
        Case adUnsignedTinyInt           ' 17
            ConvType = "Timy Integer"
        Case adUserDefined               ' 132
            ConvType = "UserDefined"
        Case adVarNumeric                 ' 139
            ConvType = "Long"
        Case adVarBinary                 ' 204
            ConvType = "Binary"
        Case adVarChar                   ' 200
            ConvType = "Text"
        Case adVariant                   ' 12
            ConvType = "Variant"
        Case adVarWChar                  ' 202
            ConvType = "Text"
        Case adWChar                     ' 130
            ConvType = "Text"
        Case Else
            ConvType = "Unknown"
   End Select
End Function

