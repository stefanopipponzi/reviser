VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MadmdC_CTL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private pFieldName As String
Private pPosition As String
Private pTipo As String
Private pIsVchar As Boolean

Public Property Get FieldName() As String
  FieldName = pFieldName
End Property

Public Property Let FieldName(v As String)
  pFieldName = v
End Property

Public Property Get Position() As String
  Position = pPosition
End Property

Public Property Let Position(p As String)
  pPosition = p
End Property

Public Property Get Tipo() As String
  Tipo = pTipo
End Property

Public Property Let Tipo(t As String)
  pTipo = t
End Property

Public Property Get IsVchar() As Boolean
  IsVchar = pIsVchar
End Property

Public Property Let IsVchar(i As Boolean)
  pIsVchar = i
End Property
