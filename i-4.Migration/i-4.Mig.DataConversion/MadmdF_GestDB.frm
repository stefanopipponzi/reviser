VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadmdF_GestDB 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " Database Tables"
   ClientHeight    =   6765
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7785
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6765
   ScaleWidth      =   7785
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdDOWN 
      Caption         =   "\/"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   7170
      TabIndex        =   22
      Tag             =   "fixed"
      Top             =   1500
      Width           =   420
   End
   Begin VB.CommandButton cmdUP 
      Caption         =   "/\"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   7170
      TabIndex        =   21
      Tag             =   "fixed"
      Top             =   870
      Width           =   420
   End
   Begin VB.Frame Frame1 
      Caption         =   "Table Properties"
      ForeColor       =   &H00000080&
      Height          =   3675
      Left            =   60
      TabIndex        =   2
      Top             =   3030
      Width           =   6975
      Begin VB.TextBox txtLabel 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   615
         Left            =   1155
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   24
         Top             =   2100
         Width           =   5685
      End
      Begin VB.CommandButton cmdTableSpc 
         Caption         =   "..."
         Height          =   345
         Left            =   3510
         TabIndex        =   20
         ToolTipText     =   "Define Tablespaces for table selected..."
         Top             =   1650
         Width           =   345
      End
      Begin VB.TextBox txtTableSpc 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1155
         MaxLength       =   18
         TabIndex        =   18
         Top             =   1620
         Width           =   2295
      End
      Begin VB.CommandButton cmdAlias 
         Caption         =   "..."
         Height          =   345
         Left            =   3510
         TabIndex        =   17
         ToolTipText     =   "Define Alias for table selected..."
         Top             =   1200
         Width           =   345
      End
      Begin VB.TextBox txtAlias 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1155
         MaxLength       =   18
         TabIndex        =   15
         Top             =   1170
         Width           =   2295
      End
      Begin VB.TextBox txtMdDate 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   4815
         MaxLength       =   18
         TabIndex        =   13
         Top             =   720
         Width           =   1995
      End
      Begin VB.TextBox txtCrDate 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   4830
         MaxLength       =   18
         TabIndex        =   11
         Top             =   270
         Width           =   1995
      End
      Begin VB.TextBox txtDescription 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   615
         Left            =   1140
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   9
         Top             =   2910
         Width           =   5685
      End
      Begin VB.TextBox txtLen 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   4815
         MaxLength       =   18
         TabIndex        =   7
         Top             =   1170
         Width           =   1995
      End
      Begin VB.TextBox txtCreator 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1155
         MaxLength       =   18
         TabIndex        =   5
         Top             =   720
         Width           =   2295
      End
      Begin VB.TextBox txtNomeTb 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1155
         MaxLength       =   50
         TabIndex        =   3
         Top             =   270
         Width           =   2295
      End
      Begin VB.Label lblLabel 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Label"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   630
         TabIndex        =   25
         Top             =   2130
         Width           =   390
      End
      Begin VB.Label lblAlias 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Alias"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   690
         TabIndex        =   23
         Top             =   1260
         Width           =   330
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Tablespace"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   150
         TabIndex        =   19
         Top             =   1680
         Width           =   900
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Alias"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   630
         TabIndex        =   16
         Top             =   1230
         Width           =   900
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Update Date"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   3720
         TabIndex        =   14
         Top             =   780
         Width           =   1005
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Creation Date"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   3750
         TabIndex        =   12
         Top             =   330
         Width           =   1005
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Comment"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   360
         TabIndex        =   10
         Top             =   2925
         Width           =   660
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Lenght"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   3720
         TabIndex        =   8
         Top             =   1230
         Width           =   1005
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Creator"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   750
         Width           =   900
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Name"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   150
         TabIndex        =   4
         Top             =   330
         Width           =   900
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   7785
      _ExtentX        =   13732
      _ExtentY        =   635
      ButtonWidth     =   2037
      ButtonHeight    =   582
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Structure"
            Key             =   "STRUCTURE"
            Object.ToolTipText     =   "Create new table"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "New Table"
            Key             =   "NEW"
            Object.ToolTipText     =   "Edit Table Structure"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Erase"
            Key             =   "ERASE"
            Object.ToolTipText     =   "Erase current Table..."
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6360
      Top             =   2340
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_GestDB.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_GestDB.frx":015A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_GestDB.frx":02B4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_GestDB.frx":0706
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_GestDB.frx":0860
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_GestDB.frx":09BA
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lswTabelle 
      Height          =   2595
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   6975
      _ExtentX        =   12303
      _ExtentY        =   4577
      View            =   2
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   12582912
      BackColor       =   16777152
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nome"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "DtCr"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Descr"
         Object.Width           =   0
      EndProperty
   End
End
Attribute VB_Name = "MadmdF_GestDB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdUP_Click()
  Dim app1a As String, app1b As String
  Dim app2a As String, app2b As String
  Dim app3a As String, app3b As String
  Dim app4a As String, app4b As String
  Dim app5a As String, app5b As String
  Dim app6a As String, app7b As String

  If lswTabelle.ListItems.Count Then
    If lswTabelle.ListItems.Count > 1 And lswTabelle.SelectedItem.Index <> 1 Then
      app1a = lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1)
      app1b = lswTabelle.ListItems(lswTabelle.SelectedItem.Index)
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).text = app1a
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1).text = app1b
       
      app2a = lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1).SubItems(1)
      app2b = lswTabelle.ListItems(lswTabelle.SelectedItem.Index).SubItems(1)
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).SubItems(1) = app2a
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1).SubItems(1) = app2b
       
      app3a = lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1).SubItems(2)
      app3b = lswTabelle.ListItems(lswTabelle.SelectedItem.Index).SubItems(2)
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).SubItems(2) = app3a
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1).SubItems(2) = app3b

      app4a = lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1).Tag
      app4b = lswTabelle.ListItems(lswTabelle.SelectedItem.Index).Tag
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).Tag = app4a
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1).Tag = app4b

      app5a = lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1).Key
      app5b = lswTabelle.ListItems(lswTabelle.SelectedItem.Index).Key
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).Key = app5a & "X"
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1).Key = app5b
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).Key = app5a
      
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index - 1).Selected = True
      lswTabelle.Refresh
    End If
  End If
End Sub

Private Sub cmdDOWN_Click()
  Dim app1a As String, app1b As String
  Dim app2a As String, app2b As String
  Dim app3a As String, app3b As String
  Dim app4a As String, app4b As String
  Dim app5a As String, app5b As String
  Dim app6a As String, app7b As String

  If lswTabelle.ListItems.Count Then
    If lswTabelle.ListItems.Count > 1 And lswTabelle.SelectedItem <> lswTabelle.ListItems(lswTabelle.ListItems.Count) Then
      app1a = lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1)
      app1b = lswTabelle.ListItems(lswTabelle.SelectedItem.Index)
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).text = app1a
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1).text = app1b
       
      app2a = lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1).SubItems(1)
      app2b = lswTabelle.ListItems(lswTabelle.SelectedItem.Index).SubItems(1)
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).SubItems(1) = app2a
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1).SubItems(1) = app2b
       
      app3a = lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1).SubItems(2)
      app3b = lswTabelle.ListItems(lswTabelle.SelectedItem.Index).SubItems(2)
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).SubItems(2) = app3a
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1).SubItems(2) = app3b

      app4a = lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1).Tag
      app4b = lswTabelle.ListItems(lswTabelle.SelectedItem.Index).Tag
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).Tag = app4a
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1).Tag = app4b

      app5a = lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1).Key
      app5b = lswTabelle.ListItems(lswTabelle.SelectedItem.Index).Key
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).Key = app5a & "X"
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1).Key = app5b
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index).Key = app5a
         
      lswTabelle.ListItems(lswTabelle.SelectedItem.Index + 1).Selected = True
      lswTabelle.Refresh
    End If
  End If
End Sub
Private Sub cmdAlias_Click()
  'Se il nome � un nome che non � presente nel db:
  'Crea un nuovo alias, setta l'id e permette l'update
  Load MadmdF_Alias
  MadmdF_Alias.Show vbModal
   
'  SetParent MadmdF_Alias.hwnd, formParentResize.hwnd
'  MadmdF_Alias.Show
'  MadmdF_Alias.Move 0, 0, formParentResize.Width, formParentResize.Height
   
  'txtAlias.text = MadmdF_Alias.txtNomeTbAlias
End Sub

Private Sub cmdTableSpc_Click()
  Dim rs As Recordset, rMax As Recordset
  Dim wMax As Long
   
  'Controlla se il tablespace esiste,
  'Se non esiste, ne crea uno e setta gli index
  
  'silvia 13-3-2008: via la BeginTrans...!!!!
  ''''DataMan.DmConnection.BeginTrans
   
  Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_TableSpaces Where " & _
                                "Nome = '" & UCase(Trim(txtTableSpc.text)) & "'")
  If rs.RecordCount = 0 Then
    'Recupera il max id disponibile
    Set rMax = m_fun.Open_Recordset("Select MAX(IdTableSpc) From " & get_PrefixQuery_TipoDB(TipoDB) & "_TableSpaces")
    If rMax.RecordCount Then
      wMax = TN(rMax.fields(0).Value, "LONG") + 1
    Else
      wMax = 1
    End If
    rMax.Close
    
    rs.AddNew
    rs!IdTableSpc = wMax
    rs!Nome = txtTableSpc.text
    rs!idDB = pIndex_DBCorrente
    rs!NomeDB = pNome_DBCorrente
    rs!Data_Creazione = Now
    rs!IdObjSource = -1
    
    rs.Update
  End If
   
  pIndex_TableSpace = rs!IdTableSpc
  pNome_TableSpace = rs!Nome
   
  rs.Close
   
  MadmdF_TableSpaces.Show vbModal
'  SetParent MadmdF_TableSpaces.hwnd, formParentResize.hwnd
'  MadmdF_TableSpaces.Show
'  MadmdF_TableSpaces.Move 0, 0, formParentResize.Width, formParentResize.Height
  txtTableSpc.text = pNome_TableSpace
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  KeyCode = Asc(UCase(Chr(KeyCode)))
End Sub

Private Sub Form_Load()
  'Setta il parent della form
  'SetParent Me.hwnd, DataMan.DmParent

  'ResizeControls Me
 
  'Carica i default della Lista
  lswTabelle.ListItems.Add , "NEWTABLE", "Create new table", , 2
  lswTabelle.ColumnHeaders(1).Width = lswTabelle.Width - 270
 
  'Carica le tabelle esistenti
  Carica_Lista_Tabelle lswTabelle, TipoDB, pIndex_DBCorrente
 
  Screen.MousePointer = vbDefault
 
  Me.Caption = " DATABASE TABLES  [" & TipoDB & " - " & pNome_DBCorrente & "]"
 
  m_fun.AddActiveWindows Me
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
   
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub lswTabelle_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswTabelle.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswTabelle.SortOrder = Order
  lswTabelle.SortKey = Key - 1
  lswTabelle.Sorted = True
End Sub

Private Sub lswTabelle_DblClick()
  If lswTabelle.SelectedItem.Key = "NEWTABLE" Then
    'Aggiunge una tabella vuota nella classe e a video
    Create_New_Table
  Else
    SetParent MadmdF_PropertyTable.hwnd, formParentResize.hwnd
    MadmdF_PropertyTable.Show
    MadmdF_PropertyTable.Move 0, 0, formParentResize.Width, formParentResize.Height
  End If
End Sub

Private Sub lswTabelle_ItemClick(ByVal Item As MSComctlLib.ListItem)
  Dim i As Long
  Dim rs As Recordset, r1 As Recordset
   
  If Trim(UCase(Item.Key)) = "NEWTABLE" Then
    pIndex_TbCorrente = -1
    pNome_TBCorrente = ""
  Else
    pIndex_TbCorrente = Mid(Item.Key, 1, InStr(1, Item.Key, "#ID") - 1)
    pNome_TBCorrente = Item.text
  End If
   
  Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Tabelle Where " & _
                                "IdTable = " & pIndex_TbCorrente)
  If rs.RecordCount Then
    txtNomeTb.text = TN(rs!Nome)
    txtCreator.text = TN(rs!Creator)
    txtCrDate.text = TN(rs!Data_Creazione)
    txtMdDate.text = TN(rs!Data_Modifica)
    txtLen.text = TN(rs!Dimensionamento)
    txtAlias.text = get_Alias_From_IdTable(pIndex_TbCorrente)
    txtDescription.text = rs!Descrizione & ""
    'SQ 21-02-08
    txtLabel.text = rs!LabelOn & ""
    
    'Recupera il tablespace
    Set r1 = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_TableSpaces Where " & _
                                  "IdTableSpc = " & TN(rs!IdTableSpc))
    If r1.RecordCount Then
      txtTableSpc.text = TN(r1!Nome)
    Else
      txtTableSpc.text = ""
    End If
    r1.Close
  Else
    txtNomeTb.text = ""
    txtCreator.text = ""
    txtCrDate.text = ""
    txtMdDate.text = ""
    txtLen.text = ""
    txtAlias.text = ""
    txtDescription.text = ""
    'SQ 21-02-08
    txtLabel.text = ""
    txtTableSpc.text = ""
  End If
  rs.Close
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Trim(UCase(Button.Key))
    Case "STRUCTURE"
      SetParent MadmdF_PropertyTable.hwnd, formParentResize.hwnd
      MadmdF_PropertyTable.Show
      MadmdF_PropertyTable.Move 0, 0, formParentResize.Width, formParentResize.Height
    Case "NEW"
      'Aggiunge una tabella vuota nella classe e a video
      Create_New_Table
       
      pStatus_Table = False
         
      'Lancia la mappa per la creazione delle colonne
      SetParent MadmdF_PropertyTable.hwnd, formParentResize.hwnd
      MadmdF_PropertyTable.Show
      MadmdF_PropertyTable.Move 0, 0, formParentResize.Width, formParentResize.Height
    Case "ERASE"
      'elimina la tabella
      Remove_Tabella
  End Select
End Sub

Public Function get_IdTable_Libero()
  Dim rs As Recordset
  Dim IdFree As Long
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  DataMan.DmConnection.BeginTrans
   
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Tabelle")
  rs.AddNew
  rs!Nome = "GETMAXID"
  rs.Update
  
  IdFree = rs!IdTable
  
  DataMan.DmConnection.RollbackTrans
  
  rs.Close
  
  get_IdTable_Libero = IdFree
End Function

Public Sub Remove_Tabella()
  Dim cDb As MadmdC_Database
  Dim rs As Recordset
  
  If lswTabelle.SelectedItem.Index = 1 Then Exit Sub
  
  If Len(lswTabelle.SelectedItem.Tag) Then
    deleteTable lswTabelle.SelectedItem.Tag, True
    lswTabelle.ListItems.Remove (lswTabelle.SelectedItem.Index)
  
    'Si posiziona sull'ultimo
    lswTabelle.ListItems(lswTabelle.ListItems.Count).Selected = True
    lswTabelle_ItemClick lswTabelle.ListItems(lswTabelle.ListItems.Count)
  End If
End Sub

Public Sub Create_New_Table()
  Dim Adesso As String
  Dim rs As Recordset
  Dim IdTable As Long
  Dim wNomeTb As String, wPref As String
   
  Adesso = Now
  
  'Prende il database corrente
  'Set cDb = m_Database.Item(pIndex_DbClass)
  
  'Salva la tabella nel database
  Select Case Trim(UCase(TipoDB))
    Case "DB2"
      Set rs = m_fun.Open_Recordset("Select * From DMDB2_Tabelle")
    Case "ORACLE"
      Set rs = m_fun.Open_Recordset("Select * From DMORC_Tabelle")
    Case "VSAM"
  End Select
  
  Screen.MousePointer = vbHourglass
  
  rs.AddNew
  wNomeTb = "NEWTABLE" & Format(rs.RecordCount + 1, "0000")
  rs!Nome = wNomeTb
  rs!IdTableSpc = -1
  rs!idTableJoin = -1
  rs!Creator = ""
  rs!Dimensionamento = -1
  rs!NomeDB = pNome_DBCorrente
  rs!idDB = pIndex_DBCorrente
  rs!IdOrigine = -1
  rs!Descrizione = "Put here Table's Description..."
  rs!Tag = ""
  rs!Data_Creazione = Adesso
  rs!Data_Modifica = Adesso
  rs.Update
  
  'Recupera l'id table
  IdTable = rs!IdTable
  
  rs.Close
  
  Screen.MousePointer = vbDefault
   
  'Aggiunge a video
  lswTabelle.ListItems.Add , IdTable & "#ID", wNomeTb, , 1
  lswTabelle.ListItems(lswTabelle.ListItems.Count).ListSubItems.Add , , Adesso
  lswTabelle.ListItems(lswTabelle.ListItems.Count).ListSubItems.Add , , Adesso
  lswTabelle.ListItems(lswTabelle.ListItems.Count).Tag = IdTable
  
  'Prende gli id per puntare la tabella
  pIndex_TbClass = -1 'cDb.TabelleCount
  pIndex_TbCorrente = IdTable
  pIndex_TableJoinCorrente = -1
End Sub

Private Sub txtAlias_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub txtCrDate_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub txtCreator_KeyUp(KeyCode As Integer, Shift As Integer)
  Dim rs As Recordset
  Dim Adesso As String
  
  Adesso = Now
  Select Case KeyCode
    Case 13
    Case Else
      'Va in update sul db
      Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Tabelle Where " & _
                                    "IdTable = " & pIndex_TbCorrente)
      If rs.RecordCount Then
        rs!Creator = txtCreator.text
        rs!Data_Modifica = Adesso
        rs.Update
        rs.Close
        
        txtMdDate.text = Adesso
      Else
        'Errore : Deve essersi perso l'indice del db
        Stop
      End If
  End Select
End Sub

Private Sub txtDescription_KeyUp(KeyCode As Integer, Shift As Integer)
  Dim rs As Recordset
  Dim Adesso As String
   
  Adesso = Now
  Select Case KeyCode
    Case 13
    Case Else
      'Va in update sul db
      Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Tabelle Where " & _
                                    "IdTable = " & pIndex_TbCorrente)
      If rs.RecordCount Then
        rs!Data_Modifica = Adesso
        rs!Descrizione = txtDescription.text
        rs.Update
        rs.Close
        
        txtMdDate.text = Adesso
      Else
        'Errore : Deve essersi perso l'indice del db
        Stop
      End If
  End Select
End Sub

Private Sub txtLen_KeyUp(KeyCode As Integer, Shift As Integer)
  Dim rs As Recordset
  Dim Adesso As String
  
  Adesso = Now
  Select Case KeyCode
    Case 13
    Case Else
      'Va in update sul db
      Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Tabelle Where " & _
                                    "IdTable = " & pIndex_TbCorrente)
      If rs.RecordCount Then
        rs!Dimensionamento = val(txtLen.text)
        rs!Data_Modifica = Adesso
        rs.Update
        rs.Close
  
        txtMdDate.text = Adesso
      Else
        'Errore : Deve essersi perso l'indice del db
        Stop
      End If
  End Select
End Sub

Private Sub txtMdDate_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub txtNomeTb_KeyUp(KeyCode As Integer, Shift As Integer)
  Dim rs As Recordset
  Dim Adesso As String
   
  Adesso = Now
  Select Case KeyCode
    Case 13
    Case Else
      'Va in update sul db
      Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Tabelle Where " & _
                                    "IdTable = " & pIndex_TbCorrente)
      If rs.RecordCount Then
        rs!Nome = txtNomeTb.text
        rs!Data_Modifica = Adesso
        rs.Update
        rs.Close
        
        lswTabelle.ListItems(lswTabelle.SelectedItem.Index).text = txtNomeTb.text
        txtMdDate.text = Adesso
      Else
        'Errore : Deve essersi perso l'indice del db
        Stop
      End If
  End Select
End Sub
