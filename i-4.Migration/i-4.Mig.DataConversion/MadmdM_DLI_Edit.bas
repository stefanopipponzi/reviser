Attribute VB_Name = "MadmdM_DLI_Edit"
Option Explicit

Global GbFinEdit As Boolean

Global Modificato As Boolean

Global EdtNomeFile As String
Global EdtIdFile As Double
Global BufEdit As String
Global EdtSfondo As Variant
Global Liv_Attuale As Integer
Global Statoliv As Integer

Global Virtuale As Boolean

Global ModoCancella As Boolean

Global SfondoGif As String
Global BgColorPg As String
Global BgColorTb As String
Global BgColorTb1 As String
Global BgColorTb2 As String

'*******************************************************************************************************************
'VARIABILI PER MEMORIZZARE IN MANIERA GLOBALE I PARAMETRI DI RICERCA PAROLE NELL'EDITOR
'INDICA LA POSIZIONE e la chiave inserita DOVE � STATO TROVATO IL TESTO NELLA RICHTEXTBOX VEDI FORM frmMigrRicerca
Global sel As Variant
Global Parola, app_parola As String
Global SostText, lunghPar As String
Global app_Pos As Variant
'SERVE A RIINIZIALIZZARE LA VARIABILE STATICA POS
Global pos1 As Variant

'INDICA SE ATTIVARE LA DISTINZIONE FRA MAIUSCOLE E MINUSCOLE
Global Match As Long
'******************************************************************************************************************
   
Dim NomeFileOut As String
Global NFileOut As Variant
Dim NomeFileIn As String
Dim NFileIn As Variant
Dim wRecIn As String
Dim wRecOut As String

Dim wString As String
Dim wNomeGrp As String
Dim wNomeMap As String
Dim wSizeR As Long
Dim wSizeC As Long
Dim TabChr(24, 80) As String

'VARIABILE PER INDICARE LO STATO DELLA FINESTRA ESADECIMALE E DI QUELLA EDITOR
Global FlagHex As Boolean
Global FlagEdit As Boolean

Global FileSel As String
Global PercorsoSel As String

'variabile per indicare su quali routine posso fare delle modifiche
Global Flag_Modifica As Boolean

' Mauro 14/01/2008
Public Const LstFldEdit_LIVELLO = 0
Public Const LstFldEdit_ORDINALE = 1
Public Const LstFldEdit_NOME = 2
Public Const LstFldEdit_BYTE = 3
Public Const LstFldEdit_LENGTH = 4
Public Const LstFldEdit_DEC = 5
Public Const LstFldEdit_START = 6
Public Const LstFldEdit_TYPE = 7
Public Const LstFldEdit_OCCURS = 8
Public Const LstFldEdit_OCCURSFLAT = 9
Public Const LstFldEdit_NOMERED = 10
Public Const LstFldEdit_SELECTOR = 11
Public Const LstFldEdit_IDCMP = 12
Public Const LstFldEdit_DAMIGR = 13


Sub GlobalCheck()
Dim indseg As Integer
Dim IndFld As Integer
Dim indstr As Integer
Dim indcmp As Integer
Dim i As Integer
Dim Lungh As Integer
Dim controllo As Integer
  
' ***********************************************************************************
' *        ROUTINE PER IL CONTROLLO DEGLI ERRORI SU TUTTI I SEGMENTI DEL DB         *
' ***********************************************************************************
  '' Mauro : Se ricordo bene, Squibbo aveva detto che non serviva se � definito nella ruotine
  ''controllo = 0
  'aggiunta per far in modo di scrivere su tutte e quattr le rich
  MadmdF_EditDli.RichTextBox1.text = ""
  For indseg = 1 To UBound(gSegm)
    ' Ma cos'� "IC"?!?! Il campo correzione sul Prj.mty � di un solo byte
    If gSegm(indseg).Correzione <> "IC" And gSegm(indseg).Correzione <> "O" Then
      Nomenodo = gSegm(indseg).Name
      MadmdF_EditDli.ControllaSovrapposizioneCampi
      For indstr = 2 To UBound(gSegm(indseg).Strutture)
        ' Mauro 27/08/2007 : Perch� manca la correzione "P"? Non � compresa anche questa tra le strutture associate?
        'If Trim(gSegm(indseg).Strutture(indstr).Correzione) = "" Or _
        '   Trim(gSegm(indseg).Strutture(indstr).Correzione) = "M" Or _
        '   Trim(gSegm(indseg).Strutture(indstr).Correzione) = "I" Then
        If Trim(gSegm(indseg).Strutture(indstr).Correzione) = "" Or _
           Trim(gSegm(indseg).Strutture(indstr).Correzione) = "M" Or _
           Trim(gSegm(indseg).Strutture(indstr).Correzione) = "P" Or _
           Trim(gSegm(indseg).Strutture(indstr).Correzione) = "I" Then
          Lungh = gSegm(indseg).Strutture(indstr).Campi(1).bytes
          For i = (indstr + 1) To UBound(gSegm(indseg).Strutture)
            If gSegm(indseg).Strutture(i).Correzione = "" And gSegm(indseg).Strutture(i).Campi(1).bytes <> Lungh Then
              AggiungiTB "DMW0001", "There are different structures associated to the segment: " & Nomenodo, MadmdF_EditDli.RichTextBox1
              Nomenodo = MadmdF_EditDli.TreeView1.SelectedItem.text
              Exit For
            End If
          Next i
          'Exit For
        End If
      Next indstr
    End If
  Next indseg
  
  For indseg = 1 To UBound(gSegm)
    controllo = 0
    ' Mauro 27/08/2007 : controllo se il segmento � obsoleto
    If gSegm(indseg).Correzione <> "O" Then
      For indstr = 2 To UBound(gSegm(indseg).Strutture)
        ' Mauro 27/08/2007 : Perch� manca la correzione "P"? Non � compresa anche questa tra le strutture associate?
        'If gSegm(indseg).Strutture(indstr).Correzione = "" Or _
        '   Trim(gSegm(indseg).Strutture(indstr).Correzione) = "M" Or _
        '   Trim(gSegm(indseg).Strutture(indstr).Correzione) = "I" Then
        If Trim(gSegm(indseg).Strutture(indstr).Correzione) = "" Or _
           Trim(gSegm(indseg).Strutture(indstr).Correzione) = "M" Or _
           Trim(gSegm(indseg).Strutture(indstr).Correzione) = "P" Or _
           Trim(gSegm(indseg).Strutture(indstr).Correzione) = "I" Then
          controllo = 1
        End If
      Next indstr
      If controllo = 0 Then
        AggiungiTB "DMW0002", "There aren't any structures associated to the segment " & gSegm(indseg).Name, MadmdF_EditDli.RichTextBox1
      End If
    End If
  Next indseg
  
  MadmdF_EditDli.ControllaSovrapposizioneCampi
  Nomenodo = MadmdF_EditDli.TreeView1.SelectedItem.text
End Sub

'Attenzione... se ne cancello uno poi sbaglia...
Public Function getVirtualSegmentName(segment As wSegm) As String
Dim count As Integer, i As Integer
Dim strOrig As String, strID As String
Dim id As Integer, idMax As Integer
Dim arrayId() As Integer
  
  ReDim arrayId(0)
  count = 1
  For i = 1 To UBound(gSegm)
    If gSegm(i).IdOrigine = segment.IdSegm Then
      strOrig = gSegm(i).IdOrigine
      strID = gSegm(i).IdSegm
      strID = Mid(strID, Len(strOrig) + 1, Len(strID) - Len(strOrig))
      id = val(strID)
      ReDim Preserve arrayId(UBound(arrayId) + 1)
      arrayId(UBound(arrayId)) = id
      If idMax <= id Then
        idMax = id
      End If
    End If
  Next
  
  Dim boolF As Boolean
  
  For id = 1 To idMax
    boolF = False
    For i = 1 To UBound(arrayId)
      If arrayId(i) = id Then
        boolF = True
      End If
    Next
    If Not boolF Then Exit For
  Next
  
  getVirtualSegmentName = segment.Name & Format(id, "00")
'  If id = -1 Then id = 0
'  If id <> idMax Then 'And id <> 0 Then
'    getVirtualSegmentName = segment.Name & Format(id, "00")
'  Else
'    getVirtualSegmentName = segment.Name & Format(idMax + 1, "00")
'  End If
End Function

Public Sub imposta_AreaSel(Lsw As ListView, ListStruct() As wStrutture)
  Dim i As Long, indArea As Long
  
  For i = 1 To UBound(ListStruct)
    If ListStruct(i).Primario Then
      indArea = i
      Exit For
    End If
  Next i
  
  For i = 1 To Lsw.ListItems.count
    If Lsw.ListItems(i) = ListStruct(indArea).StrIdOggetto & "/" & ListStruct(indArea).StrIdArea Then
      Lsw.ListItems(i).Selected = True
      Exit For
    End If
  Next i
  Lsw.Refresh
End Sub
