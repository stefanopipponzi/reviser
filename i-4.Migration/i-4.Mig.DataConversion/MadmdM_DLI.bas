Attribute VB_Name = "MadmdM_DLI"
Option Explicit

'****ale
Public nomeTab As String

'*************************

Type TbSpc
  IdTableSpc As Long
  Nome As String
  Dimensione As Long
  DataC As Date
  DataM As Date
  Descrizione As String
End Type

Type Relazione
  Segmento As String
  IdSegmento As Integer
End Type

'Type Totale per i menu
'Type MaM1
'  id  As Long
'  Label As String
'  ToolTipText As String
'  Picture As String
'  PictureEn As String
'  M1Name As String
'  M1SubName As String
'  M1Level As Long
'  M2Name As String
'  M3Name As String
'  M3ButtonType As String
'  M3SubName As String
'  M4Name As String
'  M4ButtonType As String
'  M4SubName As String
'  Funzione As String
'  DllName As String
'  TipoFinIm As String
'End Type

Public Db As Database
Global Dmmenu() As MaM1
Global DataMan As New MadmdC_Menu
Global SwMenu As Boolean
Global NmSegCpy As String
Global ConnectionState As Boolean

Sub InserisciKeySEC(rtb As RichTextBox, tb As Recordset)
  Dim PsTbField As Recordset
  Dim MgTbField As Recordset
  Dim MgTbCmp As Recordset
  Dim SQL As String
  Dim pos As Integer
  Dim Lun As Integer
  Dim WsTR As String
  Dim NomeCol As String
  Dim Decimali As String
  Dim Inserisci As Boolean
  Dim wLen As Long
  
  Set PsTbField = m_fun.Open_Recordset("Select * from PsDLI_Field where IdSegmento = " & tb!IdSegmento)
  While Not PsTbField.EOF
      Set MgTbField = m_fun.Open_Recordset("Select * from MgDLI_Field where IdSegmento = " & tb!IdSegmento & " and idfield = " & PsTbField!IdField)
      Inserisci = False
      If MgTbField.RecordCount > 0 Then
        Inserisci = False
        If MgTbField!Correzione <> "C" Then
          Inserisci = True
          pos = MgTbField!Posizione
          Lun = MgTbField!lunghezza
        End If
      Else
        Inserisci = True
        pos = PsTbField!Posizione
        Lun = PsTbField!lunghezza
      End If
      
      If Inserisci Then
        If Left(PsTbField!Nome, 1) <> "/" Then
          InsertParola rtb, "<CHIAVI-SECONDARIE>", " 01 KRD-" & PsTbField!Nome & "." & vbCrLf & Space(11) & "<COLONNA-DLI>." & vbCrLf & Space(7) & "<CHIAVI-SECONDARIE>"
          
          Set MgTbCmp = m_fun.Open_Recordset("select * from MgData_Cmp where IdSegmento = " & PsTbField!IdSegmento & _
                                       " and Posizione > " & pos - 1 & " order by ordinale")
          
          If MgTbCmp.RecordCount > 0 Then
            While Not MgTbCmp.EOF
              If MgTbCmp!lunghezza > 0 Then
                'SQ - 28-03-07 MA MONDO FILIPPO... I "BNR" CHI LI GESTISCE?!
                'wLen = MgTbCmp!lunghezza
                'If MgTbCmp!Usage = "PCK" Then
                '  wLen = Int(wLen / 2) + 1
                'End If
                wLen = MgTbCmp!Byte
                If ((MgTbCmp!Posizione + wLen) < (pos + Lun + 1)) Then
                
                  NomeCol = Trim(MgTbCmp!Nome)
                  WsTR = ""
          
                  If MgTbCmp!segno = "S" Then
                     WsTR = "10 " & Trim(MgTbCmp!Nome) & "  PIC S" & MgTbCmp!Tipo & "(" & MgTbCmp!lunghezza & ")"
                  Else
                     WsTR = "10 " & Trim(MgTbCmp!Nome) & "  PIC " & MgTbCmp!Tipo & "(" & MgTbCmp!lunghezza & ")"
                  End If
                  If MgTbCmp!Decimali > 0 Then
                    Decimali = "V9(" & MgTbCmp!Decimali & ")"
                  Else
                    Decimali = ""
                  End If
                  Select Case MgTbCmp!Usage
                     Case "BNR"
                        WsTR = WsTR & Decimali & " COMP"
                     Case "PCK"
                        WsTR = WsTR & Decimali & " COMP-3"
                     Case "ZND"
                       If MgTbCmp!Tipo = "9" Then WsTR = WsTR & Decimali
                  End Select
                  WsTR = WsTR & "." & vbCrLf & Space(11) & "<COLONNA-DLI>."
              
                  InsertParola rtb, "<COLONNA-DLI>.", WsTR
                End If
              End If
              MgTbCmp.MoveNext
            Wend
            DelParola rtb, "<COLONNA-DLI>."
          End If
        End If
      End If
      PsTbField.MoveNext
  Wend
  DelParola rtb, "<CHIAVI-SECONDARIE>"
End Sub

Sub InserisciKeyDLI(rtb As RichTextBox, rsSegmenti As Recordset)
  Dim rsColonne As Recordset, TbRelazioni As Recordset, TbApp As Recordset, TbApp1 As Recordset
  Dim indseg  As Integer, NomeCol As String
  Dim Tipo As String, Usage As String, WsTR As String, Decimali As String, Father As String
  Dim Relazioni() As Relazione
  Dim wNomeSeg As String, wNomeSeg1 As String
  
  wNomeSeg = rsSegmenti!Nome
  'If Trim(rsSegmenti!Comprtn) <> "None" Then wNomeSeg = rsSegmenti!Comprtn
  
  Father = ""
  ReDim Relazioni(0)
  Relazioni(0).Segmento = rsSegmenti!Nome
  Relazioni(0).IdSegmento = rsSegmenti!IdSegmento
  
  Set TbRelazioni = m_fun.Open_Recordset("select * from MgRel_DLISeg where IdOggetto = " & rsSegmenti!IdOggetto & " and Child = '" & rsSegmenti!Nome & "'")
  If TbRelazioni.RecordCount > 0 Then
    Father = TbRelazioni!Father
    While Father <> "0"
      Set TbApp = m_fun.Open_Recordset("select * from PsDLI_Segmenti where IdOggetto = " & rsSegmenti!IdOggetto & " and Nome = '" & Father & "'")
      ReDim Preserve Relazioni(UBound(Relazioni) + 1)
      Relazioni(UBound(Relazioni)).Segmento = Father
      Relazioni(UBound(Relazioni)).IdSegmento = TbApp!IdSegmento
      Set TbApp = m_fun.Open_Recordset("select * from MgRel_DLISeg where IdOggetto = " & rsSegmenti!IdOggetto & " and Child = '" & Father & "'")
      Father = TbApp!Father
    Wend
  Else
    'VC: evitare di richiamare le mappe se non caricate correttamente, andava in errore!
    AggiungiTB "Relations error!!! " & "Segment: " & rsSegmenti!Nome & " Check relations...", vbExclamation, MadmdF_EditDli.RichTextBox1
  End If
  
  For indseg = UBound(Relazioni) To 0 Step -1
    Set rsColonne = m_fun.Open_Recordset("select * from MgData_Cmp where " & _
                                   "IdSegmento= " & Relazioni(indseg).IdSegmento & " and Correzione = 'R' order by Ordinale")
    If rsColonne.RecordCount Then
      If indseg = UBound(Relazioni) Then
        'InsertParola RTB, "<CHIAVI-DLI>.", "01 KRD-" & Relazioni(indseg).Segmento & "." & vbCrLf & Space(9) & "05 WKEYP-" & Relazioni(indseg).Segmento & "." & vbCrLf & Space(12) & "<COLONNA-DLI>." & vbCrLf & Space(9) & "<CHIAVE-SEG>."
        wNomeSeg1 = Relazioni(indseg).Segmento
        Set TbApp1 = m_fun.Open_Recordset("select * from psdli_segmenti where idoggetto = " & rsSegmenti!IdOggetto & " and nome = '" & wNomeSeg1 & "'")
        If TbApp1.RecordCount > 0 Then
          If Trim(TbApp1!Comprtn) <> "None" Then wNomeSeg1 = TbApp1!Comprtn
        End If
        InsertParola rtb, "<CHIAVI-DLI>", " 01 KRD-" & wNomeSeg & "." & vbCrLf & Space(9) & "05 KRDP-" & wNomeSeg1 & "." & vbCrLf & Space(12) & "<COLONNA-DLI>." & vbCrLf & Space(9) & "<CHIAVE-SEG>."
      Else
        'InsertParola RTB, "<CHIAVE-SEG>.", "05 WKEYP-" & Relazioni(indseg).Segmento & "." & vbCrLf & Space(12) & "<COLONNA-DLI>." & vbCrLf & Space(9) & "<CHIAVE-SEG>."
        wNomeSeg1 = Relazioni(indseg).Segmento
        Set TbApp1 = m_fun.Open_Recordset("select * from psdli_segmenti where idoggetto = " & rsSegmenti!IdOggetto & " and nome = '" & wNomeSeg1 & "'")
        If TbApp1.RecordCount > 0 Then
          If Trim(TbApp1!Comprtn) <> "None" Then wNomeSeg1 = TbApp1!Comprtn
        End If
        InsertParola rtb, "<CHIAVE-SEG>.", "05 KRDP-" & wNomeSeg1 & "." & vbCrLf & Space(12) & "<COLONNA-DLI>." & vbCrLf & Space(9) & "<CHIAVE-SEG>."
      End If
      While Not rsColonne.EOF
        NomeCol = Trim(rsColonne!Nome)
        WsTR = ""
        If rsColonne!lunghezza = "0" Then
          WsTR = "<COLONNA-DLI>." ' "10 " & Trim(rsColonne!nome) & "." & vbCrLf & Space(12) & "<COLONNA-DLI>." '"  PIC " & rsColonne!Tipo & "(" & rsColonne!Byte & ")" & "." & vbCrLf & Space(12) & "<COLONNA-DLI>."
        Else
          If rsColonne!segno = "S" Then
             WsTR = "10 " & Trim(rsColonne!Nome) & "  PIC S" & rsColonne!Tipo & "(" & rsColonne!lunghezza & ")"
          Else
             WsTR = "10 " & Trim(rsColonne!Nome) & "  PIC " & rsColonne!Tipo & "(" & rsColonne!lunghezza & ")"
          End If
          If rsColonne!Decimali > 0 Then
            Decimali = "V9(" & rsColonne!Decimali & ")"
          Else
            Decimali = ""
          End If
          Select Case rsColonne!Usage
             Case "BNR"
                WsTR = WsTR & Decimali & " COMP"
             Case "PCK"
                WsTR = WsTR & Decimali & " COMP-3"
             Case "ZND"
              If rsColonne!Tipo = "9" Then WsTR = WsTR & Decimali
          End Select
             WsTR = WsTR & "." & vbCrLf & Space(12) & "<COLONNA-DLI>."
        End If
        InsertParola rtb, "<COLONNA-DLI>.", WsTR
        rsColonne.MoveNext
      Wend
      DelParola rtb, "<COLONNA-DLI>."
    Else
      DelParola rtb, "<CHIAVI-DLI>"
    End If
  Next indseg
  DelParola rtb, "<CHIAVE-SEG>."
End Sub
''''''''''''''''''''''''''''''''''''
' ex InserisciKeyDLI
' Solo BPER!
''''''''''''''''''''''''''''''''''''
Sub getChiaviDli_BPER(rtb As RichTextBox, rsSegmenti As Recordset, IdTable As Long)
  Dim rsIndex As Recordset, rsIdxCol As Recordset
  Dim statement As String, wNomeSeg As String, Decimali As String
  Dim i_index As Integer
  
  On Error GoTo errdb
  
  Set rsIndex = m_fun.Open_Recordset("select idIndex,nome,tipo from DMDB2_index WHERE IdTable= " & IdTable & " and used = true and (tipo = 'I' or tipo = 'P') order by tipo desc,IdIndex")
  While Not rsIndex.EOF
    If m_fun.FnNomeDB = "banca.mty" Then
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Tutte colonne aggiunte; mi basta il nome della colonna...
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Set rsIdxCol = m_fun.Open_Recordset("select a.Nome from DMDB2_Colonne as a, DMDB2_IdxCol as b WHERE a.IdColonna=b.IdColonna and b.IdIndex=" & rsIndex!IdIndex)
      '''While Not rsIdxCol.EOF
      If Not rsIdxCol.EOF Then
        If Left(rsIdxCol!Nome, 3) <> "ID_" Then 'non mi interessano!    P.S.: non partire dagli indici DB2, ma dai Field con correzione...
        'If Left(rsIndex!Nome, 3) <> "ID_" Then 'non mi interessano!    P.S.: non partire dagli indici DB2, ma dai Field con correzione...
          If rsIndex!Tipo = "P" Then  'diverse regole per i nomi:
            statement = "01 KRD-" & rsSegmenti!Nome & "." & vbCrLf
            '"KY_AB_SEG_AB01L"  -> W-AB0-KEY
            statement = statement & getXdfieldStructure_BPER("W-" & Mid(rsIdxCol!Nome, 11, 3) & "-KEY")
            'statement = statement & getXdfieldStructure_BPER("W-" & Mid(rsIndex!Nome, 11, 3) & "-KEY")
          Else
            'SQ 1-08-05: devo cambiare il nome perche' le routine fanno fatica ad utilizzare questo (partono dal DL1)
            'i_index = i_index + 1 'serve per il nome
            'statement = statement & vbCrLf & "01 KRD-" & rsSegmenti!Nome & "-I" & i_index & "." & vbCrLf
            wNomeSeg = Mid(rsIndex!Nome, 4, 3) & "KEY" & Right(rsIndex!Nome, 2) 'nome indice, cambiare variabile!
            statement = statement & vbCrLf & "01 KRD-" & wNomeSeg & "." & vbCrLf
            statement = statement & getXdfieldStructure_BPER(rsIdxCol!Nome)
            'statement = statement & getXdfieldStructure_BPER(rsIndex!Nome)
          End If
        End If
      '''  rsIdxCol.MoveNext
      '''Wend
      End If
    Else
      If rsIndex!Tipo = "P" Then
        statement = "01 KRD-" & rsSegmenti!Nome & "." & vbCrLf
      Else
        i_index = i_index + 1 'serve per il nome
        statement = statement & vbCrLf & "01 KRD-" & rsSegmenti!Nome & "-I" & i_index & "." & vbCrLf
      End If
      'Set rsIdxCol = m_fun.Open_Recordset("select c.idCmp,c.Nome,c.lunghezza,c.segno,c.decimali,c.tipo,c.byte,c.usage from DMDB2_IdxCol as b,mgData_Cmp as c,dmdb2_colonne as d WHERE b.IdIndex=" & rsIndex!IdIndex & " and b.idColonna=d.idColonna AND d.idCmp=c.idCmp order by c.Ordinale")
      Set rsIdxCol = m_fun.Open_Recordset("select c.idCmp,c.Nome,c.lunghezza,c.segno,c.decimali,c.tipo,c.byte,c.[Usage] from DMDB2_IdxCol as b,mgData_Cmp as c,dmdb2_colonne as d WHERE b.IdIndex=" & rsIndex!IdIndex & " and b.idColonna=d.idColonna AND d.idCmp=c.idCmp order by c.Ordinale")
      While Not rsIdxCol.EOF
        If rsIdxCol(2) = 0 Then
          'statement = "10 " & rsIdxCol(1) & "." & vbCrLf & Space(3) & "<COLONNA-DLI>." '"  PIC " & rsIdxCol(5) & "(" & rsIdxCol(6) & ")" & "." & vbCrLf & Space(3) & "<COLONNA-DLI>."
          statement = statement & Space(3) & "10 " & rsIdxCol(1) & "  PIC X(" & rsIdxCol(6) & ")"
        Else
          If rsIdxCol(4) = "S" Then
             statement = statement & Space(3) & "10 " & rsIdxCol(1) & "  PIC S" & rsIdxCol!Tipo & "(" & rsIdxCol(2) & ")"
          Else
             statement = statement & Space(3) & "10 " & rsIdxCol(1) & "  PIC " & rsIdxCol!Tipo & "(" & rsIdxCol(2) & ")"
          End If
          If rsIdxCol(4) > 0 Then
            Decimali = "V9(" & rsIdxCol(4) & ")"
          Else
            Decimali = ""
          End If
          Select Case rsIdxCol(7)
             Case "BNR"
                statement = statement & Decimali & " COMP"
             Case "PCK"
                statement = statement & Decimali & " COMP-3"
             Case "ZND"
              If rsIdxCol(5) = "9" Then statement = statement & Decimali
          End Select
          'statement = statement & "." & vbCrLf & Space(12) & "<COLONNA-DLI>."
        End If
        statement = statement & "." & vbCrLf
        'changeTag RTB, "<CHIAVI-DLI>.", statement
        rsIdxCol.MoveNext
      Wend
    End If
    rsIndex.MoveNext
  Wend
  changeTag rtb, "<CHIAVI-DLI>", statement
  Exit Sub
errdb:
  MsgBox ERR.Description
  'Resume
End Sub

''''''''''''''''''''''''''''''''''''
' ex InserisciKeyDLI
''''''''''''''''''''''''''''''''''''
Sub getChiaviDli(rtb As RichTextBox, rsSegmenti As Recordset, IdTable As Long)
  Dim rsMgField As Recordset, rsField As Recordset, MgTbCmp As Recordset
  Dim i_index As Integer, i As Integer
  Dim statement As String, NomeCol As String, Decimali As String, tracciato As String
  Dim wLen As Long
  Dim usato As Boolean
  Dim tracciatiField() As String
  Dim livello01 As String
  
  On Error GoTo errdb
    
  Set rsField = m_fun.Open_Recordset("Select IdField, Nome, Seq, Posizione, Lunghezza from PsDLI_Field where " & _
                                     "IdSegmento = " & rsSegmenti!IdSegmento)
  If rsField.EOF Then
     Set rsField = m_fun.Open_Recordset("Select IdField, Nome, Seq, Posizione, Lunghezza from mgDLI_Field where " & _
                                        "IdSegmento = " & rsSegmenti!IdSegmento)
  End If
  
  If rsField.RecordCount Then
    ReDim tracciatiField(rsField.RecordCount - 1, 1)
  End If
  For i = 0 To rsField.RecordCount - 1
    'SQ: KRD-indexName!
    Dim rsIndex As Recordset
    Set rsIndex = m_fun.Open_Recordset("SELECT nome from " & PrefDb & "_index where " & _
                                       "idOrigine = " & rsField!IdField & " AND idTable = " & IdTable)
    'CONTROLLO SE USATO: (spostare in fondo)
    usato = False  'init
    livello01 = ""
    ''''''''''''''''''''''''''''''
    'FIELD SEQ e FIELD scelti:
    ''''''''''''''''''''''''''''''
    If rsField!Seq Then
      usato = True
      If rsIndex.EOF Then
        'SQ 30-01-07 - Problema FIELD aggiunti (FIELD MULTIPLI)
        '              => idOrigine NON � idIndice ma idColonna (capire sul DM perch� questo scamuffo!)
        '              ==> PEZZA: faccio una query diversa (sono sul SEQ, quindi � diventato (99%) chiave primaria)
        Set rsIndex = m_fun.Open_Recordset("select nome from " & PrefDb & "_index where tipo = 'P' AND idTable = " & IdTable)
      End If
      If rsIndex.RecordCount Then
        'SQ: KRD-indexName!
        'livello01 = "01 KRD-" & rsField!Nome & "." & vbCrLf
        'VIRGILIO inserita la replace per le REVKEY
        livello01 = "01 KRD-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf
      Else
        'MsgBox "tmp: getChiaviDli - no index for idField: " & rsField!IdField & " - idTable: " & IdTable
        DataMan.DmFinestra.ListItems.Add , , Now & " no index for idField " & rsField!IdField & " - idTable: " & IdTable
        DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      End If
    Else
      'SQ 16-05-07
      'Perch� Correzione <> "M"? Sono i FIELD col baffetto?!
      'Set rsMgField = m_fun.Open_Recordset("Select Correzione from MgDLI_Field where IdSegmento = " & rsSegmenti!IdSegmento & " and idfield = " & rsField!IdField)
      Set rsMgField = m_fun.Open_Recordset("Select idfield from MgDLI_Field WHERE " & _
                                           "IdSegmento = " & rsSegmenti!IdSegmento & " and " & _
                                           "idfield = " & rsField!IdField & " AND correzione = 'M'")
      If rsMgField.RecordCount Then
        'If rsMgField!Correzione <> "M" Then
          usato = True
          i_index = i_index + 1 'serve per il nome
          'SQ: 16-05-07 che nome �? BPER?!
          'livello01 = vbCrLf & "01 KRD-" & rsSegmenti!Nome & "-I" & i_index & "." & vbCrLf
           livello01 = vbCrLf & "01 KRD-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf
        'End If
      End If
      rsMgField.Close
    End If
    rsIndex.Close
    
    'CAMPI:
    tracciato = ""
    'SQ - RIFACCIO LA SELECT PER OGNI FIELD!!!!!!!!!!!!!
    
    'STEFANO: MA LE /CK NON DOVEVA AVERLE GIA' GESTITE QUALCUNO????
'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
'!!!!!!!! COMPLICATINO
    If Left(rsField!Nome, 3) = "/CK" Then
    'crea dei record fittizi prendendoli dai segmenti di livello superiore
      Dim PsSeg As Recordset
      Dim arrSeg(), indseg As Integer, fineSeg As Boolean
      Dim NomeSeg As String
      ReDim arrSeg(0)
      arrSeg(0) = rsSegmenti!IdSegmento
      If rsSegmenti!Parent <> "0" Then
        indseg = 0
        NomeSeg = rsSegmenti!Parent
        fineSeg = False
        While Not fineSeg
          Set PsSeg = m_fun.Open_Recordset("select idSegmento, Parent from PsDLI_Segmenti where " & _
                                           "nome = '" & NomeSeg & "' and idoggetto = " & rsSegmenti!IdOggetto)
          If Not PsSeg.EOF Then
            indseg = indseg + 1
            ReDim Preserve arrSeg(indseg)
            arrSeg(indseg) = PsSeg!IdSegmento
            NomeSeg = PsSeg!Parent
          Else
            fineSeg = True
          End If
        Wend
        'stefano: usare una struttura: come si fa?
        Dim arrLunghezza() As Integer
        Dim arrPosizione() As Integer
        Dim arrUsage() As String
        Dim arrNome() As String
        Dim arrSegno() As String
        Dim arrTipo() As String
        Dim arrDecimali() As Integer
        Dim indArr As Integer
        indArr = 0
        'posizione relativa della chiave concatenata
        Dim relPos As Integer
        relPos = 0
        For indseg = UBound(arrSeg) To 0 Step -1
          Set PsSeg = m_fun.Open_Recordset("Select IdField,Nome,Seq,Posizione,Lunghezza from PsDLI_Field where " & _
                                           "seq = -1 and IdSegmento = " & arrSeg(indseg))
          If PsSeg.EOF Then
             Set PsSeg = m_fun.Open_Recordset("Select IdField,Nome,Seq,Posizione,Lunghezza from mgDLI_Field where " & _
                                              "seq = -1 and IdSegmento = " & arrSeg(indseg))
          End If
          If Not PsSeg.EOF Then
            Set MgTbCmp = m_fun.Open_Recordset("select * from MgData_Cmp where " & _
                                               "IdSegmento = " & arrSeg(indseg) & " and Posizione >= " & PsSeg!Posizione & " and " & _
                                               "Posizione < " & PsSeg!Posizione + PsSeg!lunghezza & " order by ordinale")
            While Not MgTbCmp.EOF
              If MgTbCmp!lunghezza Then
                ReDim Preserve arrLunghezza(indArr)
                ReDim Preserve arrPosizione(indArr)
                ReDim Preserve arrUsage(indArr)
                ReDim Preserve arrNome(indArr)
                ReDim Preserve arrSegno(indArr)
                ReDim Preserve arrTipo(indArr)
                ReDim Preserve arrDecimali(indArr)
                arrLunghezza(indArr) = MgTbCmp!lunghezza
                arrPosizione(indArr) = MgTbCmp!Posizione + relPos
                arrUsage(indArr) = MgTbCmp!Usage
                arrNome(indArr) = Replace(MgTbCmp!Nome, "_", "-")
                arrSegno(indArr) = MgTbCmp!segno
                arrTipo(indArr) = MgTbCmp!Tipo
                arrDecimali(indArr) = MgTbCmp!Decimali
                indArr = indArr + 1
              End If
              MgTbCmp.MoveNext
            Wend
            relPos = relPos + PsSeg!lunghezza ' virgilio
          End If
          'relPos = relPos + PsSeg!lunghezza 'virgilio
        Next indseg
        
        For indseg = 0 To UBound(arrLunghezza)
          If arrPosizione(indseg) >= rsField!Posizione And arrLunghezza(indseg) Then
            wLen = arrLunghezza(indseg)
            If arrUsage(indseg) = "PCK" Then  'SQ i binary?!!!!!
              wLen = Int(wLen / 2) + 1
            End If
            
            If ((arrPosizione(indseg) + wLen) < (rsField!Posizione + rsField!lunghezza + 1)) Then
              NomeCol = arrNome(indseg)
              If arrSegno(indseg) = "S" Then
                tracciato = tracciato & Space(3) & "10 " & arrNome(indseg) & "  PIC S" & arrTipo(indseg) & "(" & arrLunghezza(indseg) & ")"
              Else
                tracciato = tracciato & Space(3) & "10 " & arrNome(indseg) & "  PIC " & arrTipo(indseg) & "(" & arrLunghezza(indseg) & ")"
              End If
              If arrDecimali(indseg) > 0 Then
                Decimali = "V9(" & arrDecimali(indseg) & ")"
              Else
                Decimali = ""
              End If
              Select Case arrUsage(indseg)
                Case "BNR"
                  tracciato = tracciato & Decimali & " COMP"
                Case "PCK"
                  tracciato = tracciato & Decimali & " COMP-3"
                Case "ZND"
                  If arrTipo(indseg) = "9" Then tracciato = tracciato & Decimali
              End Select
              tracciato = tracciato & "." & vbCrLf
            End If
          End If
        Next indseg
      Else
        'MsgBox "tmp: getChiaviDli - no parent on /CK: " & rsField!IdField & " - idTable: " & IdTable
        DataMan.DmFinestra.ListItems.Add , , Now & " no parent on /CK: " & rsField!IdField & " - idTable: " & IdTable
        DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      End If
    Else
      Set MgTbCmp = m_fun.Open_Recordset("SELECT * FROM MgData_Cmp WHERE " & _
                                         "IdSegmento = " & rsSegmenti!IdSegmento & " and Posizione >= " & rsField!Posizione & _
                                         " order by ordinale")
      While Not MgTbCmp.EOF
        If MgTbCmp!lunghezza Then
          wLen = MgTbCmp!lunghezza
          'SQ - 28-03-07 MA MONDO FILIPPO... I "BNR" CHI LI GESTISCE?!
          ' ==> MA SOPRATTUTTO... HO IL CAMPO !byte gi� pronto!!!!!!!!
          'If MgTbCmp!Usage = "PCK" Then
          '  wLen = Int(wLen / 2) + 1
          'End If
          'If ((MgTbCmp!Posizione + wLen) < (rsField!Posizione + rsField!lunghezza + 1)) Then
          If ((MgTbCmp!Posizione + MgTbCmp!Byte) < (rsField!Posizione + rsField!lunghezza + 1)) Then
            NomeCol = Replace(MgTbCmp!Nome, "_", "-")
    
            If MgTbCmp!segno = "S" Then
              tracciato = tracciato & Space(3) & "10 " & NomeCol & "  PIC S" & MgTbCmp!Tipo & "(" & MgTbCmp!lunghezza & ")"
            Else
              tracciato = tracciato & Space(3) & "10 " & NomeCol & "  PIC " & MgTbCmp!Tipo & "(" & MgTbCmp!lunghezza & ")"
            End If
            If MgTbCmp!Decimali Then
              Decimali = "V9(" & MgTbCmp!Decimali & ")"
            Else
              Decimali = ""
            End If
            Select Case MgTbCmp!Usage
              Case "BNR"
                tracciato = tracciato & Decimali & " COMP"
              Case "PCK"
                tracciato = tracciato & Decimali & " COMP-3"
              Case "ZND"
                If MgTbCmp!Tipo = "9" Then tracciato = tracciato & Decimali
            End Select
          tracciato = tracciato & "." & vbCrLf
          End If
        End If
        MgTbCmp.MoveNext
      Wend
      MgTbCmp.Close
    End If
    
    'salvo per gli XDField
    tracciatiField(i, 0) = rsField!Nome
    tracciatiField(i, 1) = tracciato
    
    'alpitour 5/8/2005
'    If usato Then
    If usato And Len(tracciato) Then
'      statement = statement & tracciato
      statement = statement & livello01 & tracciato
    End If
    rsField.MoveNext
  Next i
  rsField.Close
  
  ''''''''''''''''''''''''''''''
  ' XDFIELD:
  ''''''''''''''''''''''''''''''
  Dim fields() As String
  Dim j As Integer
'SP 12/9 provo a gestire gli indici particolari
  Dim tracciatiFieldSave() As String
  tracciatiFieldSave = tracciatiField
  Set rsField = m_fun.Open_Recordset("Select idXDField,nome,srchFields from PsDLI_XDField where " & _
                                     "IdSegmento = " & rsSegmenti!IdSegmento)
  If rsField.EOF Then
    Set rsField = m_fun.Open_Recordset("Select idXDField,nome,srchFields from mgDLI_XDField where " & _
                                       "IdSegmento = " & rsSegmenti!IdSegmento)
  End If
  
  livello01 = ""
  While Not rsField.EOF
    tracciatiField = tracciatiFieldSave
    'SQ: KRD-indexName!
    'livello01 = vbCrLf & "01 KRD-" & rsField!Nome & "." & vbCrLf
    Set rsIndex = m_fun.Open_Recordset("select nome from " & PrefDb & "_index where idOrigine=" & rsField!IdxdField * -1)
    If rsIndex.RecordCount Then
      'livello01 = "01 KRD-" & rsField!Nome & "." & vbCrLf
      livello01 = vbCrLf & "01 KRD-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf
    Else
    'silvia
     '' MsgBox "getChiaviDli: segment '" & rsSegmenti!Nome & "' - no index for idXDField: " & rsField!IdxdField, vbExclamation, DataMan.DmNomeProdotto
    End If
    rsIndex.Close
    
    fields = Split(rsField!srchFields, ",")
    'alpitour 5/8/2005
    tracciato = ""
    For i = 0 To UBound(fields)
      'cerco in tracciatiField il tracciato corrispondente al singolo Field
      For j = 0 To UBound(tracciatiField)
        If tracciatiField(j, 0) = fields(i) Then
'         statement = statement & tracciatiField(j, 1)
          tracciato = tracciato & tracciatiField(j, 1)
          Exit For
        End If
        'controllo se non match...
        'alpitour 5/8/2005
        If Left(tracciatiField(j, 0), 1) <> "/" Then
          If j = UBound(tracciatiField) Then
            ' SILVIA 13-3-2008  SE NON TROVA CAMPI PER LA TABELLA
            DataMan.DmFinestra.ListItems.Add , , Now & " Segment '" & rsSegmenti!Nome & "' - Field not found for: " & fields(i)
            DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
            'MsgBox "Segment '" & rsSegmenti!Nome & "' - Field not found for: " & fields(i), vbExclamation, DataMan.DmNomeProdotto
          End If
        End If
      Next
    Next
    'alpitour 5/8/2005
    If Len(tracciato) Then
      statement = statement & livello01 & tracciato
    End If
    rsField.MoveNext
  Wend
  rsField.Close
  
  changeTag rtb, "<CHIAVI-DLI>", statement
  
  Exit Sub
errdb:
  MsgBox ERR.Description
  'Resume
End Sub

Sub getChiaviDli_PLI(rtb As RichTextBox, rsSegmenti As Recordset, IdTable As Long)
  Dim rsMgField As Recordset, rsField As Recordset, MgTbCmp As Recordset
  Dim i As Integer
  Dim statement As String, tracciato As String
  Dim wLen As Long
  Dim usato As Boolean
  Dim tracciatiField() As String
  Dim livello01 As String
  Dim kind As String
  Dim nomeIndice As String
  Dim totKeyLen As Long
  ' Mauro 06/10/2010
  Dim rsColonne As Recordset, tipoCampo As String
  
  On Error GoTo errdb
    
  Set rsField = m_fun.Open_Recordset("Select IdField,Nome,Seq,Posizione,Lunghezza from PsDLI_Field where " & _
                                     "IdSegmento = " & rsSegmenti!IdSegmento)
  If rsField.EOF Then
     Set rsField = m_fun.Open_Recordset("Select IdField,Nome,Seq,Posizione,Lunghezza from mgDLI_Field where " & _
                                        "IdSegmento = " & rsSegmenti!IdSegmento)
  End If
  
  If rsField.RecordCount Then
    ReDim tracciatiField(rsField.RecordCount - 1, 2)
  End If
  For i = 0 To rsField.RecordCount - 1
    'SQ: KRD-indexName!
    Dim rsIndex As Recordset
    Set rsIndex = m_fun.Open_Recordset("select nome from " & PrefDb & "_index where " & _
                                       "idOrigine = " & rsField!IdField & " AND idTable = " & IdTable)
    usato = False  'init
    livello01 = ""
    If rsField!Seq Then
      usato = True
      If rsIndex.EOF Then
        Set rsIndex = m_fun.Open_Recordset("select nome from " & PrefDb & "_index where tipo = 'P' AND idTable = " & IdTable)
      End If
      If rsIndex.RecordCount Then
        'nomeIndice = rsIndex!Nome
        'livello01 = "DCL 1 KRD_" & nomeIndice & "," & vbCrLf
        livello01 = "DCL 1 KRD_" & rsIndex!Nome & "," & vbCrLf
        nomeIndice = rsIndex!Nome
      Else
        MsgBox "tmp: getChiaviDli - no index for idField: " & rsField!IdField & " - idTable: " & IdTable
      End If
    Else
      ' Mauro 26/09/2007 : Era stato fatto per COBOL, ma non per PLI
      'Perch� Correzione <> "M"? Sono i FIELD col baffetto?!
      'Set rsMgField = m_fun.Open_Recordset("Select Correzione from MgDLI_Field where " & _
                                            "IdSegmento = " & rsSegmenti!IdSegmento & " and idfield = " & rsField!IdField)
      Set rsMgField = m_fun.Open_Recordset("Select idfield from MgDLI_Field WHERE " & _
                                           "IdSegmento = " & rsSegmenti!IdSegmento & " and " & _
                                           "idfield = " & rsField!IdField & " AND correzione='M'")
      If rsMgField.RecordCount Then
        'If rsMgField!Correzione <> "M" Then
          usato = True
          livello01 = vbCrLf & "DCL 1 KRD_" & rsIndex!Nome & "," & vbCrLf
          nomeIndice = rsIndex!Nome
        'End If
      End If
      rsMgField.Close
    End If
    rsIndex.Close
    
    'Mauro : E' una mia impressione, o sto giro non serve veramente a una fava?!?!?!?
'''    'CAMPI:
'''    tracciato = ""
'''    'SQ - RIFACCIO LA SELECT PER OGNI FIELD!!!!!!!!!!!!!
'''    Set MgTbCmp = m_fun.Open_Recordset("select * from MgData_Cmp where IdSegmento = " & rsSegmenti!IdSegmento & _
'''                                       " and Posizione >= " & rsField!Posizione & " order by ordinale")
'''    totKeyLen = 0
'''    While Not MgTbCmp.EOF
'''    Wend
'''    MgTbCmp.Close
    
    ' Mauro 26/09/2007 : Copiato spudoratamente da COBOL
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'CAMPI:
    tracciato = ""
    totKeyLen = 0
    'SQ - RIFACCIO LA SELECT PER OGNI FIELD!!!!!!!!!!!!!
    
    'STEFANO: MA LE /CK NON DOVEVA AVERLE GIA' GESTITE QUALCUNO????
'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
'!!!!!!!! COMPLICATINO
    If Left(rsField!Nome, 3) = "/CK" Then
    'crea dei record fittizi prendendoli dai segmenti di livello superiore
      Dim PsSeg As Recordset
      Dim arrSeg(), indseg As Integer, fineSeg As Boolean
      Dim NomeSeg As String
      ReDim arrSeg(0)
      arrSeg(0) = rsSegmenti!IdSegmento
      If rsSegmenti!Parent <> "0" Then
        indseg = 0
        NomeSeg = rsSegmenti!Parent
        fineSeg = False
        While Not fineSeg
          Set PsSeg = m_fun.Open_Recordset("select idSegmento, Parent from PsDLI_Segmenti where " & _
                                           "nome = '" & NomeSeg & "' and idoggetto = " & rsSegmenti!IdOggetto)
          If Not PsSeg.EOF Then
            indseg = indseg + 1
            ReDim Preserve arrSeg(indseg)
            arrSeg(indseg) = PsSeg!IdSegmento
            NomeSeg = PsSeg!Parent
          Else
            fineSeg = True
          End If
        Wend
        'stefano: usare una struttura: come si fa?
        Dim arrLunghezza() As Integer
        Dim arrPosizione() As Integer
        Dim arrUsage() As String
        Dim arrNome() As String
        Dim arrSegno() As String
        Dim arrTipo() As String
        Dim arrDecimali() As Integer
        Dim arrByte() As Integer
        Dim indArr As Integer
        indArr = 0
        'posizione relativa della chiave concatenata
        Dim relPos As Integer
        relPos = 0
        For indseg = UBound(arrSeg) To 0 Step -1
          Set PsSeg = m_fun.Open_Recordset("Select IdField,Nome,Seq,Posizione,Lunghezza from PsDLI_Field where " & _
                                           "seq = -1 and IdSegmento = " & arrSeg(indseg))
          If PsSeg.EOF Then
            Set PsSeg = m_fun.Open_Recordset("Select IdField,Nome,Seq,Posizione,Lunghezza from mgDLI_Field where " & _
                                             "seq = -1 and IdSegmento = " & arrSeg(indseg))
          End If
          If Not PsSeg.EOF Then
            Set MgTbCmp = m_fun.Open_Recordset("select * from MgData_Cmp where " & _
                                               "IdSegmento = " & arrSeg(indseg) & " and Posizione >= " & PsSeg!Posizione & " and " & _
                                               "Posizione < " & PsSeg!Posizione + PsSeg!lunghezza & " order by ordinale")
            While Not MgTbCmp.EOF
              If MgTbCmp!lunghezza Then
                ReDim Preserve arrLunghezza(indArr)
                ReDim Preserve arrPosizione(indArr)
                ReDim Preserve arrUsage(indArr)
                ReDim Preserve arrNome(indArr)
                ReDim Preserve arrSegno(indArr)
                ReDim Preserve arrTipo(indArr)
                ReDim Preserve arrDecimali(indArr)
                ReDim Preserve arrByte(indArr)
                arrLunghezza(indArr) = MgTbCmp!lunghezza
                arrPosizione(indArr) = MgTbCmp!Posizione + relPos
                arrUsage(indArr) = MgTbCmp!Usage
                arrNome(indArr) = MgTbCmp!Nome
                arrSegno(indArr) = MgTbCmp!segno
                arrTipo(indArr) = MgTbCmp!Tipo
                arrDecimali(indArr) = MgTbCmp!Decimali
                arrByte(indArr) = MgTbCmp!Byte
                indArr = indArr + 1
              End If
              MgTbCmp.MoveNext
            Wend
            relPos = relPos + PsSeg!lunghezza ' virgilio
          End If
          'relPos = relPos + PsSeg!lunghezza 'virgilio
        Next indseg

        For indseg = 0 To UBound(arrLunghezza)
          If arrPosizione(indseg) >= rsField!Posizione And arrLunghezza(indseg) Then
            wLen = arrLunghezza(indseg)
            If arrUsage(indseg) = "PCK" Then  'SQ i binary?!!!!!
              wLen = Int(wLen / 2) + 1
            End If
            
            If ((arrPosizione(indseg) + wLen) < (rsField!Posizione + rsField!lunghezza + 1)) Then
              kind = ""
              Select Case arrUsage(indseg)
                 Case "BNR"
                   kind = " COMP"
                 Case "PCK"
                   kind = " COMP-3"
                 Case "ZND"
                   If arrTipo(indseg) = "9" Then kind = arrDecimali(indseg)
              End Select
              tracciato = tracciato & Space(3) & "2 " & arrNome(indseg) & " " & _
                          getPLIData(arrTipo(indseg), arrLunghezza(indseg), kind, arrDecimali(indseg), arrSegno(indseg))
              totKeyLen = totKeyLen + arrByte(indseg)
            End If
          End If
        Next indseg
      Else
        'MsgBox "tmp: getChiaviDli - no parent on /CK: " & rsField!IdField & " - idTable: " & IdTable
        DataMan.DmFinestra.ListItems.Add , , Now & " no parent on /CK: " & rsField!IdField & " - idTable: " & IdTable
        DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      End If
    Else
      Set MgTbCmp = m_fun.Open_Recordset("select * from MgData_Cmp where " & _
                                         "IdSegmento = " & rsSegmenti!IdSegmento & " and Posizione >= " & rsField!Posizione & _
                                         " order by ordinale")
      While Not MgTbCmp.EOF
        If MgTbCmp!lunghezza Then
          If ((MgTbCmp!Posizione + MgTbCmp!Byte) < (rsField!Posizione + rsField!lunghezza + 1)) Then
            kind = ""
            Select Case MgTbCmp!Usage
              Case "BNR"
                kind = " COMP"
              Case "PCK"
                kind = " COMP-3"
              Case "ZND"
               If MgTbCmp!Tipo = "9" Then kind = MgTbCmp!Decimali
            End Select
            tipoCampo = getPLIData(MgTbCmp!Tipo, MgTbCmp!lunghezza, kind, MgTbCmp!Decimali, MgTbCmp!segno)
            ' Mauro 06/10/2010:
            Set rsColonne = m_fun.Open_Recordset("select a.* from " & PrefDb & "_colonne as a, " & PrefDb & "_tabelle as b " & _
                                                 "where b.idorigine = " & rsSegmenti!IdSegmento & " and a.idtable = b.idtable and " & _
                                                 "a.idcmp = " & MgTbCmp!IdCmp & " and a.nomecmp = '" & MgTbCmp!Nome & "'")
            If rsColonne.RecordCount Then
              If MgTbCmp!Tipo = "9" And (MgTbCmp!Usage = "ZND" Or MgTbCmp!Usage = "") And rsColonne!Tipo = "CHAR" Then
                tipoCampo = Replace(Replace(tipoCampo, "PIC '", "CHAR"), "9'", "")
              End If
            End If
            rsColonne.Close
            
            tracciato = tracciato & Space(3) & "2 " & MgTbCmp!Nome & " " & tipoCampo
            totKeyLen = totKeyLen + MgTbCmp!Byte
            
            tracciato = tracciato & "," & vbCrLf
          End If
        End If
        MgTbCmp.MoveNext
      Wend

      MgTbCmp.Close
    End If
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'salvo per gli XDField
    tracciatiField(i, 0) = rsField!Nome
    tracciatiField(i, 1) = tracciato
    tracciatiField(i, 2) = rsField!lunghezza

    If usato And Len(tracciato) Then
      statement = statement & livello01 & tracciato
      statement = Mid(statement, 1, InStrRev(statement, ",") - 1) & ";"
      'statement = statement & vbCrLf & "  KRD_" & nomeIndice & " = '' ;"
      statement = statement & vbCrLf & "DCL  KRD_" & nomeIndice & "R  CHAR(" & totKeyLen & ") BASED(ADDR(KRD_" & nomeIndice & ")); "
    End If
    rsField.MoveNext
  Next i
  rsField.Close
  
  ''''''''''''''''''''''''''''''
  ' XDFIELD:
  ''''''''''''''''''''''''''''''
  Dim fields() As String
  Dim j As Integer
'SP 12/9 provo a gestire gli indici particolari
  Dim tracciatiFieldSave() As String
  Dim indexXD As String
  tracciatiFieldSave = tracciatiField
  Set rsField = m_fun.Open_Recordset("Select idXDField,nome,srchFields from PsDLI_XDField where IdSegmento = " & rsSegmenti!IdSegmento)
  If rsField.EOF Then
    Set rsField = m_fun.Open_Recordset("Select idXDField,nome,srchFields from mgDLI_XDField where IdSegmento = " & rsSegmenti!IdSegmento)
  End If
  totKeyLen = 0
  livello01 = ""
  While Not rsField.EOF
    tracciatiField = tracciatiFieldSave
    'SQ: KRD-indexName!
    Set rsIndex = m_fun.Open_Recordset("select nome from " & PrefDb & "_index where idOrigine=" & rsField!IdxdField * -1)
    If rsIndex.RecordCount Then
      livello01 = vbCrLf & "DCL 1 KRD_" & rsIndex!Nome & "," & vbCrLf
    Else
      MsgBox "getChiaviDli: segment '" & rsSegmenti!Nome & "' - no index for idXDField: " & rsField!IdxdField, vbExclamation, DataMan.DmNomeProdotto
    End If
    indexXD = rsIndex!Nome
    rsIndex.Close
    
    fields = Split(rsField!srchFields, ",")
    'alpitour 5/8/2005
    tracciato = ""
    For i = 0 To UBound(fields)
      'cerco in tracciatiField il tracciato corrispondente al singolo Field
      For j = 0 To UBound(tracciatiField)
        If tracciatiField(j, 0) = fields(i) Then
          tracciato = tracciato & tracciatiField(j, 1)
          totKeyLen = totKeyLen + tracciatiField(j, 2)
          Exit For
        End If
        'controllo se non match...
        'alpitour 5/8/2005
        If Left(tracciatiField(j, 0), 1) <> "/" Then
          If j = UBound(tracciatiField) Then MsgBox "Segment '" & rsSegmenti!Nome & "' - Field not found for: " & fields(i), vbExclamation, DataMan.DmNomeProdotto
        End If
      Next
    Next
    'alpitour 5/8/2005
    If Len(tracciato) Then
      If Right(Trim(statement), 1) <> ";" Then
        statement = statement & livello01 & tracciato
      End If
    End If
    
    rsField.MoveNext
  Wend
  rsField.Close
  If statement <> "" Then
    If Right(Trim(statement), 1) <> ";" Then
      statement = Mid(statement, 1, InStrRev(statement, ",") - 1) & ";"
      'statement = statement & vbCrLf & "  KRD_" & nomeIndice & " = '' ;"
      statement = statement & vbCrLf & "DCL  KRD_" & nomeIndice & "R  CHAR(" & totKeyLen & ") BASED(ADDR(KRD_" & nomeIndice & ")); "
    End If
  End If
  
  changeTag rtb, "<CHIAVI-DLI>", statement
  
  Exit Sub
errdb:
  MsgBox ERR.Description
  'Resume
End Sub

Sub getChiaviDli_KRDP(rtb As RichTextBox, rsSegmenti As Recordset, IdTable As Long)
  Dim rsField As Recordset, rsIndex As Recordset
  Dim statement As String, NomeCol As String, Decimali As String, tracciato As String
  Dim wLen As Long
  Dim livello01 As String

  On Error GoTo errdb

  statement = ""
  Set rsIndex = m_fun.Open_Recordset("Select * from " & PrefDb & "_Index where " & _
                " tipo <> 'F' and Idtable = " & IdTable & " order by tipo desc, nome")

  While Not rsIndex.EOF
    Set rsField = m_fun.Open_Recordset("Select d.*, b.tipo as tipoidx, a.nome as nomecol " & _
                                       "from " & PrefDb & "_Colonne as a, " & PrefDb & "_index as b," & _
                                       PrefDb & "_idxcol as c, mgdata_cmp as d, " & PrefDb & "_tabelle as e" & _
                                       " where a.Idtable = " & IdTable & " and a.idtable = b.idtable and" & _
                                       " b.idindex = c.idindex and a.idcmp = d.idcmp and b.idindex = " & rsIndex!IdIndex & " and" & _
                                       " e.idtable = a.IdTableJoin and e.idorigine = d.idsegmento and" & _
                                       " a.idcolonna = c.idcolonna order by c.ordinale")
    If Not rsField.EOF Then
      livello01 = "01 KRDP-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf
    End If
    While Not rsField.EOF
      tracciato = ""
      If rsField!lunghezza + rsField!Decimali > 0 Then
        'SQ no!!!!!!!!!!!!!!!!!!!!!!!
         'wLen = rsField!lunghezza
         'If rsField!Usage = "PCK" Then
         '   wLen = Int(wLen / 2) + 1
         'End If
         wLen = rsField!Byte
         
         NomeCol = Replace(rsField!Nome, "_", "-")
         'SP 12/9 forzatura del cavolo per colpa del problema dell'azienda, che � stata
         'portata a volte dal vecchio record, dando al nuovo campo un doppio significato
         'sia come chiave ereditata che come campo gi� presente
         'SP 13/9, ritolta perch� creava altri problemi, per ora si corregge a mano prima
         'di compilare
         'If rsField!Tipoidx = "I" Then
         '   NomeCol = rsField!NomeCol
         'End If
         If rsField!segno = "S" Then
            tracciato = tracciato & Space(3) & "10 " & NomeCol & "  PIC S" & rsField!Tipo & "(" & rsField!lunghezza & ")"
         Else
            tracciato = tracciato & Space(3) & "10 " & NomeCol & "  PIC " & rsField!Tipo & "(" & rsField!lunghezza & ")"
         End If
         If rsField!Decimali > 0 Then
            Decimali = "V9(" & rsField!Decimali & ")"
         Else
            Decimali = ""
         End If
         Select Case rsField!Usage
            Case "BNR"
               tracciato = tracciato & Decimali & " COMP"
            Case "PCK"
               tracciato = tracciato & Decimali & " COMP-3"
            Case "ZND"
               If rsField!Tipo = "9" Then tracciato = tracciato & Decimali
         End Select
         tracciato = tracciato & "." & vbCrLf
      End If
      rsField.MoveNext
      If Len(tracciato) Then
         statement = statement & livello01 & tracciato
        livello01 = ""
      End If
    Wend
    rsField.Close
    statement = statement & vbCrLf
    rsIndex.MoveNext
  Wend
  rsIndex.Close
  changeTag rtb, "<CHIAVI-DLI-KRDP>", statement
  Exit Sub
errdb:
  MsgBox ERR.Description
  'Resume
End Sub

Sub getChiaviDli_KRDP_PLI(rtb As RichTextBox, rsSegmenti As Recordset, IdTable As Long)
  Dim rsField As Recordset, rsIndex As Recordset
  Dim statement As String, NomeCol As String, Decimali As String, tracciato As String
  Dim wLen As Long
  Dim usato As Boolean
  Dim livello01 As String
  Dim totKeyLen  As Long
  Dim kind As String, tipoCampo As String
  
  Dim rsColonne As Recordset
  
  On Error GoTo errdb

  statement = ""
  Set rsIndex = m_fun.Open_Recordset("Select * from " & PrefDb & "_Index where " & _
                "tipo <> 'F' and Idtable = " & IdTable & " order by tipo desc, nome")

  While Not rsIndex.EOF
    Set rsField = m_fun.Open_Recordset("Select d.*, b.tipo as tipoidx, a.nome as nomecol " & _
                                       "from " & PrefDb & "_Colonne as a, " & PrefDb & "_index as b," & _
                                       PrefDb & "_idxcol as c, mgdata_cmp as d, " & PrefDb & "_tabelle as e" & _
                                       " where a.Idtable = " & IdTable & " and a.idtable = b.idtable and" & _
                                       " b.idindex = c.idindex and a.idcmp = d.idcmp and b.idindex = " & rsIndex!IdIndex & " and" & _
                                       " e.idtable = a.IdTableJoin and e.idorigine = d.idsegmento and" & _
                                       " a.idcolonna = c.idcolonna order by c.ordinale")
    If Not rsField.EOF Then
      livello01 = "DCL 1 KRDP_" & rsIndex!Nome & "," & vbCrLf
    End If
    totKeyLen = 0
    While Not rsField.EOF
      tracciato = ""
      If rsField!lunghezza > 0 Then
        wLen = rsField!lunghezza
        NomeCol = rsField!Nome
        kind = ""
        Select Case rsField!Usage
          Case "BNR"
            kind = " COMP"
          Case "PCK"
            kind = " COMP-3"
          Case "ZND"
            If rsField!Tipo = "9" Then kind = Decimali
        End Select
        tipoCampo = getPLIData(rsField!Tipo, rsField!lunghezza, kind, rsField!Decimali, rsField!segno)
        ' Mauro 06/10/2010:
        Set rsColonne = m_fun.Open_Recordset("select a.* from " & PrefDb & "_colonne as a, " & PrefDb & "_tabelle as b " & _
                                             "where b.idorigine = " & rsSegmenti!IdSegmento & " and a.idtable = b.idtable and " & _
                                             "a.idcmp = " & rsField!IdCmp & " and a.nomecmp = '" & rsField!Nome & "'")
        If rsColonne.RecordCount Then
          If rsField!Tipo = "9" And (rsField!Usage = "ZND" Or rsField!Usage = "") And rsColonne!Tipo = "CHAR" Then
            tipoCampo = Replace(Replace(tipoCampo, "PIC '", "CHAR"), "9'", "")
          End If
        End If
        rsColonne.Close
        
        tracciato = tracciato & Space(3) & "2 " & rsField!Nome & Space(10) & tipoCampo & "," & vbCrLf
        totKeyLen = totKeyLen + rsField!Byte
      End If
      
      If Len(tracciato) Then
        statement = statement & livello01 & tracciato
      End If
      livello01 = ""
      rsField.MoveNext
    Wend
    
    If Len(statement) Then
      If Right(statement, 1) <> ";" Then
        statement = Mid(statement, 1, InStrRev(statement, ",") - 1) & ";"
        'statement = statement & vbCrLf & "  KRDP_" & rsIndex!Nome & " = '' ; "
        statement = statement & vbCrLf & "DCL  KRDP_" & rsIndex!Nome & "R  CHAR(" & totKeyLen & ") BASED(ADDR(KRDP_" & rsIndex!Nome & ")); "
      End If
    End If
    statement = statement & vbCrLf
    rsIndex.MoveNext
  Wend
  changeTag rtb, "<CHIAVI-DLI-KRDP>", statement
  Exit Sub
errdb:
  MsgBox ERR.Description
  'Resume
End Sub
