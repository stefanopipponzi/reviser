VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MadmdC_Menu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
'''''''''''''''''
'Array contenente le PK e le SK
Type GbKey
  IdSegmento As Long
  NomeKey As String
  IdCmp As Long
End Type

'Array per contenere I segmenti
Type GbSeg
  IdSegmento As Long
  IdSegmentoOrigine As Long
  Nome As String
  Parent As String
  Prov As String
  'SQ: serve per le PK dei campi figli...
  'ex SQ: IdxcolumnsKey() As GbIdxCol
  SeqKeys() As GbKey
End Type

'Per tenere in memoria i segmenti che creeranno le varie tabelle DB2
Public Type TabDb
  Nome As String
  NomeTb As String
  idDB As Long
  NomeDB As String
  IdOrigine As Long 'Segmento
  IdSegmOR As Long 'Segmento origine � valorizzato <>0 se � un MGSegmento
  AliasSegm As String
End Type
''''''''''''''''''''''''''''
Public Type Field
  IdField As Long
  IdSegm As Long
  Nome As String
  Tipo As String
  Start As Long
  MaxLen As Long
  Seq As String
  Unique As String
  NameIndPunSeq As String
  DBDNome As String
  Pointer As String
  Xname As String
  segment As String
  Srch As String
  SubSeq As String
  NullVal As String
End Type

Type GbIdxCol
  IdIndex As Long
  IdColonna As Long
  IdTable As Long
  Nome As String
End Type

Type XDField
  IdField As Long
  IdSegmento As Long
  Nome As String
  srchFields As String
  '03-05-05
  extRtn As String
End Type

'SQ era nel modulo
Public Type GbField
  IdField As Long
  IdSegmento As Long
  Nome As String
  Posizione As Long
  lunghezza As Long
  Primario As Boolean
  Unique As Boolean
  Prov As String
  'SQ: serve alle XDFld per non rifare tutto il giro o andare sulle tabelle
  idxColumns() As GbIdxCol
End Type

Public Type Campo
  Codice As String
  IdStruttura As Long
  Nome As String
  Ordinale As Long
  NomeRed As String
  livello As Long
  Tipo As String
  Posizione As Long
  bytes As Double
  Occurs As Long
  lunghezza As Long
  Decimali As Long
  segno As String
  Usage As String
  val As String
  Picture As String
  ' Mauro 15/01/2008
  OccursFlat As Boolean
  DaMigrare As Boolean
End Type

Public Type Struttura
  IdSegm As Long
  IdStruttura As Long
  Nome As String
  Tipo As String
  Campi() As Campo
End Type

Public Type Segmento
  IdOggetto As Long
  IdSegmento As Long
  Nome As String
  MaxLen As Long
  MinLen As Long
  Parent As String
  Pointer As String
  Rules As String
  Comprtn As String
  TbName As String
  RoutineIMS As String
  Used As Boolean
  Inserito As Boolean
  fields() As Field
  Strutture() As Struttura
End Type

Public Type CampoRDBMS
  IdChiave As Long
  IdCampo As Long
  Nome As String
  Tipo As String
  lunghezza As Long
  Decimali As Long
  segno As String
  Used As Boolean
End Type

Public Type Chiave
  IdChiave As Long
  IdTavola As Long
  Nome As String
  Campi() As CampoRDBMS
End Type

Public Type Tavola
  IdTavola As Long
  IdTableSpace As Long
  Nome As String
  pkey() As Chiave
  SKey() As Chiave
  Campi() As CampoRDBMS
  Used As Boolean
End Type

Public Type Tablespace
  IdTableSpace As Long
  IdOggetto As Long
  Nome As String
  Used As Boolean
  Tavole() As Tavola
End Type

Public Type Database
  Nome As String
  Tipo As String
  IdOggetto As Long
  Segmenti() As Segmento
  TableSpaces() As Tablespace
End Type

Private m_DmErrCodice As String
Private m_DmErrMsg As String
Private m_DmFinestra As Object
Private m_DmIdOggetto As Long
Private m_DmTop As Long
Private m_DmLeft As Long
Private m_DmWidth As Long
Private m_DmHeight As Long
Private m_DmDatabase As ADOX.Catalog
Private m_DmConnection As ADODB.Connection
Private m_DmConnState As Long
Private m_DmNomeDB As String
Private m_DmPathDb As String
Private m_DmPathDef As String
Private m_DmUnitDef As String
Private m_DmPathPrd As String
Private m_DmTipoDB As String
Private m_DmTipoMigrazione As String
Private m_DmNomeProdotto As String
Private m_DmImgDir As String
Private m_DmParent As Long
Private m_DmMenuIcon As String
Private m_DmId As Long
Private m_DmLabel As String
Private m_DmToolTiptext As String
Private m_DmPicture As String
Private m_DmPictureEn As String
Private m_DmM1Name As String
Private m_DmM1SubName As String
Private m_DmM1Level As Long
Private m_DmM2Name As String
Private m_DmM3Name As String
Private m_DmM3ButtonType As String
Private m_DmM3SubName As String
Private m_DmM4Name As String
Private m_DmM4ButtonType As String
Private m_DmM4SubName As String
Private m_DmFunzione As String
Private m_DmDllName As String
Private m_DmTipoFinIm As String
Private m_DmFunctionCur As String
Private m_DmFunctionStatus As String
Private m_DmProvider As e_Provider
Private m_DmObjList As Object
Private m_DmDli2RdbmsACT As Boolean
Private m_DmParam_MaxSize As Integer
Private m_DmParam_PercOut As String
Private m_DmParam_PercCpyStruc As String
Private m_DmParam_PercCpyField As String
''Private m_DmParam_PercCpyMove As String
''Private m_DmParam_PercCpyProgCob As String
Private m_DmParam_PercCpyExec As String
''Private m_DmParam_PercCpyKey As String
Private m_DmParam_PercSQLExec As String
Private m_DmParam_PercOutField As String
''Private m_DmParam_PercJclLoad As String
Private m_DmParam_OutField_Db As String
Private m_DmParam_OutField_Orc As String
Private m_DmParam_OutField_Vsam As String
Private m_DmParam_PercOutCpy As String
Private m_DmCollectionParam As Collection
Private m_DmAsterixForDelete As String
Private m_DmAsterixForWhere As String

Dim DmIndMnu As Long
Dim DmIndImg As Long

Public Function AttivaFunzione(Funzione As String, cParam As Collection, formParent As Object, Optional IdButton As String) As Boolean
  Dim b As Boolean
  
  On Error GoTo ErrorHandler
  AttivaFunzione = True
  
  Select Case Trim(UCase(Funzione))
    Case "SHOW_DATAMANAGER"
      If DmConnState = adStateOpen Then
        SetParent MadmdF_Start.hwnd, formParent.hwnd
        MadmdF_Start.Show
        MadmdF_Start.Move 0, 0, formParent.Width, formParent.Height
        Set formParentResize = formParent
        
        'Dim myfrm As Form
        'Set myfrm = MadmdF_Start.ShowMe(formParent)
'        MadmdF_Start.Move 0, formParent.Top, formParent.Width, formParent.Height
       'ResizeForm myfrm
       ' MadmdF_Start.Move 0, 0, formParent.Width, formParent.Height
      Else
        MsgBox "You must select a database...", vbExclamation, DataMan.DmNomeProdotto
      End If
    
    Case "SHOW_DM_PARAMETER"
      'Set formParentResize = formParent
    
    Case "SHOW_DLIMIGRATION"
      'Avvia la mappa per la migrazione dei dbd verso DLI
      SetParent MadmdF_DLI2RDBMS.hwnd, formParent.hwnd
      MadmdF_DLI2RDBMS.Show
      MadmdF_DLI2RDBMS.Move 0, 0, formParent.Width, formParent.Height
      Set formParentResize = formParent
   
    Case "SHOW_RDBMS_CONVERTER"
      SetParent MadmdF_RDBMSConv.hwnd, formParent.hwnd
      MadmdF_RDBMSConv.Show
      MadmdF_RDBMSConv.Move 0, 0, formParent.Width, formParent.Height
      Set formParentResize = formParent
      
    Case "SHOW_VSAM_MIGRATION"
      'Avvia la mappa per la migrazione dei dbd verso DLI
      SetParent MadmdF_VSAM2RDBMS.hwnd, formParent.hwnd
      MadmdF_VSAM2RDBMS.Show
      MadmdF_VSAM2RDBMS.Move 0, 0, formParent.Width, formParent.Height
      Set formParentResize = formParent
      
    Case Else
      AttivaFunzione = True
      
  End Select
  
  Exit Function
ErrorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002"
End Function

Public Function Move_To_Button(Direction As String) As Boolean
  'Direction =
  '
  '1)First    : Si muove al primo bottone dell'array
  '2)Next     : Si muove al successivo
  '3)Previous : Si muove al precedente
  '4)Last     : Si muove all'ultimo
  
  If Not SwMenu Then
    Move_To_Button = Carica_Menu
  End If
  
  'seleziona l'indice
  Select Case UCase(Trim(Direction))
    Case "FIRST"
      DmIndMnu = 1
    Case "NEXT"
      DmIndMnu = DmIndMnu + 1
    Case "PREVIOUS"
      DmIndMnu = DmIndMnu - 1
    Case "LAST"
      DmIndMnu = UBound(Dmmenu)
  End Select
    
  If DmIndMnu > UBound(Dmmenu) Then
    Move_To_Button = False
    Exit Function
  End If
  
  'assegna alle variabili public il contenuto dell'elemento puntato dell'array
  DmId = Dmmenu(DmIndMnu).id
  DmLabel = Dmmenu(DmIndMnu).Label
  DmToolTiptext = Dmmenu(DmIndMnu).ToolTipText
  DmPicture = Dmmenu(DmIndMnu).Picture
  DmPictureEn = Dmmenu(DmIndMnu).PictureEn
  DmM1Name = Dmmenu(DmIndMnu).M1Name
  DmM1SubName = Dmmenu(DmIndMnu).M1SubName
  DmM1Level = IIf(Dmmenu(DmIndMnu).M1Level = "", 0, Dmmenu(DmIndMnu).M1Level)
  DmM2Name = Dmmenu(DmIndMnu).M2Name
  DmM3Name = Dmmenu(DmIndMnu).M3Name
  DmM3ButtonType = Dmmenu(DmIndMnu).M3ButtonType
  DmM3SubName = Dmmenu(DmIndMnu).M3SubName
  DmM4Name = Dmmenu(DmIndMnu).M4Name
  DmM4ButtonType = Dmmenu(DmIndMnu).M4ButtonType
  DmM4SubName = Dmmenu(DmIndMnu).M4SubName
  DmFunzione = Dmmenu(DmIndMnu).Funzione
  DmDllName = Dmmenu(DmIndMnu).DllName
  DmTipoFinIm = Dmmenu(DmIndMnu).TipoFinIm
  
  Move_To_Button = True
End Function
Public Sub InitDll(curDllFunzioni As MaFndC_Funzioni)
   Set m_fun = curDllFunzioni
''' Mauro: 06/12/2006: vengono poi valorizzate nella routine SettaFilebase

''' wPref = get_PrefixQuery_TipoDB(TipoDB)
 
''' 'VARIABILI DA RIMUOVERE DOPO AVER FATTO MAPPA PARAMETRI
''' DmParam_MaxSize = 40
''' DmParam_PercCpyStruc = m_fun.FnPathDB & "\" & SYSTEM_DIR & "\CPY_BASE\" & wPref & "_CPY_BASE_CPDD.cpy"
''' DmParam_PercCpyField = DataMan.DmPathPrd & "\" & SYSTEM_DIR & "\CPY_BASE\" & wPref & "_CPY_BASE_CPDR.cpy"
''' DmParam_PercCpyProgCob = DataMan.DmPathPrd & "\" & SYSTEM_DIR & "\CPY_BASE\" & wPref & "_CBL_BASE.cbl"
''' DmParam_PercCpyExec = DataMan.DmPathPrd & "\" & SYSTEM_DIR & "\CPY_BASE\" & wPref & "_CPY_BASE_EXEC.cpy"
''' DmParam_PercJclLoad = DataMan.DmPathPrd & "\" & SYSTEM_DIR & "\CPY_BASE\" & wPref & "_JCL_BASE_LOAD.jcl"
''' DmParam_PercCpyMove = DataMan.DmPathPrd & "\" & SYSTEM_DIR & "\CPY_BASE\" & wPref & "_CPY_BASE_MOVE.cpy"
''' DmParam_PercCpyKey = DataMan.DmPathPrd & "\" & SYSTEM_DIR & "\CPY_BASE\" & wPref & "_CPY_BASE_MOVE_KEY.cpy"
''' DmParam_PercSQLExec = DataMan.DmPathPrd & "\" & SYSTEM_DIR & "\CPY_BASE\" & wPref & "_DEC_BASE_EXEC.sql"
 
 'Altri Percorsi saranno valorizzati a ryn time durante il caricamento del db di progetto
 'oppure al momento della creazione dei file dai template
 Carica_Menu
 
 'Carica la trascodifica db2-->oracle
 Carica_Trascodifica_DB2_To_ORACLE
End Sub

Private Sub Class_Initialize()
  Set DataMan = Me
End Sub

'**************************************************
'****** DICHIARAZIONE PROPERTY GET/LET/SET ********
'**************************************************
'DMERRCODICE
Public Property Let DmErrCodice(mVar As String)
    m_DmErrCodice = mVar
End Property

Public Property Get DmErrCodice() As String
    DmErrCodice = m_DmErrCodice
End Property

'DMERRMSG
Public Property Let DmErrMsg(mVar As String)
    m_DmErrMsg = mVar
End Property

Public Property Get DmErrMsg() As String
    DmErrMsg = m_DmErrMsg
End Property

'DMFINESTRA
Public Property Set DmFinestra(mVar As Object)
   Set m_DmFinestra = mVar
End Property

Public Property Get DmFinestra() As Object
   Set DmFinestra = m_DmFinestra
End Property

'DMIDOGGETTO
Public Property Let DmIdOggetto(mVar As Long)
    m_DmIdOggetto = mVar
End Property

Public Property Get DmIdOggetto() As Long
    DmIdOggetto = m_DmIdOggetto
End Property

'DMTOP
Public Property Let DmTop(mVar As Long)
    m_DmTop = mVar
End Property

Public Property Get DmTop() As Long
    DmTop = m_DmTop
End Property

'DMLEFT
Public Property Let DmLeft(mVar As Long)
    m_DmLeft = mVar
End Property

Public Property Get DmLeft() As Long
    DmLeft = m_DmLeft
End Property

'DMWIDTH
Public Property Let DmWidth(mVar As Long)
    m_DmWidth = mVar
End Property

Public Property Get DmWidth() As Long
    DmWidth = m_DmWidth
End Property

'DMHEIGHT
Public Property Let DmHeight(mVar As Long)
    m_DmHeight = mVar
End Property

Public Property Get DmHeight() As Long
    DmHeight = m_DmHeight
End Property

'DMDATABASE
Public Property Set DmDatabase(mVar As ADOX.Catalog)
   Set m_DmDatabase = mVar
End Property

Public Property Get DmDatabase() As ADOX.Catalog
   Set DmDatabase = m_DmDatabase
End Property

'DMCONNECTION
Public Property Set DmConnection(mVar As ADODB.Connection)
   Set m_DmConnection = mVar
End Property

Public Property Get DmConnection() As ADODB.Connection
   Set DmConnection = m_DmConnection
End Property

'DMCONNSTATE
Public Property Let DmConnState(mVar As Long)
    m_DmConnState = mVar
End Property

Public Property Get DmConnState() As Long
    DmConnState = m_DmConnState
End Property

'DMNOMEDB
Public Property Let DmNomeDB(mVar As String)
    m_DmNomeDB = mVar
End Property

Public Property Get DmNomeDB() As String
    DmNomeDB = m_DmNomeDB
End Property

'DMPATHDB
Public Property Let DmPathDb(mVar As String)
    m_DmPathDb = mVar
End Property

Public Property Get DmPathDb() As String
    DmPathDb = m_DmPathDb
End Property

'DMPATHDEF
Public Property Let DmPathDef(mVar As String)
    m_DmPathDef = mVar
End Property

Public Property Get DmPathDef() As String
    DmPathDef = m_DmPathDef
End Property

'DMUNITDEF
Public Property Let DmUnitDef(mVar As String)
    m_DmUnitDef = mVar
End Property

Public Property Get DmUnitDef() As String
    DmUnitDef = m_DmUnitDef
End Property

'DMPATHPRD
Public Property Let DmPathPrd(mVar As String)
    m_DmPathPrd = mVar
End Property

Public Property Get DmPathPrd() As String
    DmPathPrd = m_DmPathPrd
End Property

'DMTIPODB
Public Property Let DmTipoDB(mVar As String)
    m_DmTipoDB = mVar
End Property

Public Property Get DmTipoDB() As String
    DmTipoDB = m_DmTipoDB
End Property

'DMTIPOMIGRAZIONE
Public Property Let DmTipoMigrazione(mVar As String)
    m_DmTipoMigrazione = mVar
End Property

Public Property Get DmTipoMigrazione() As String
    DmTipoMigrazione = m_DmTipoMigrazione
End Property

'DMNOMEPRODOTTO
Public Property Let DmNomeProdotto(mVar As String)
    m_DmNomeProdotto = mVar
End Property

Public Property Get DmNomeProdotto() As String
    DmNomeProdotto = m_DmNomeProdotto
End Property

'DMIMGDIR
Public Property Let DmImgDir(mVar As String)
    m_DmImgDir = mVar
End Property

Public Property Get DmImgDir() As String
    DmImgDir = m_DmImgDir
End Property

'DMPARENT
Public Property Let DmParent(mVar As Long)
    m_DmParent = mVar
End Property

Public Property Get DmParent() As Long
    DmParent = m_DmParent
End Property

'DMMENUICON
Public Property Let DmMenuIcon(mVar As String)
    m_DmMenuIcon = mVar
End Property

Public Property Get DmMenuIcon() As String
    DmMenuIcon = m_DmMenuIcon
End Property

'DMID
Public Property Let DmId(mVar As Long)
    m_DmId = mVar
End Property

Public Property Get DmId() As Long
    DmId = m_DmId
End Property

'DMLABEL
Public Property Let DmLabel(mVar As String)
    m_DmLabel = mVar
End Property

Public Property Get DmLabel() As String
    DmLabel = m_DmLabel
End Property

'DMTOOLTIPTEXT
Public Property Let DmToolTiptext(mVar As String)
    m_DmToolTiptext = mVar
End Property

Public Property Get DmToolTiptext() As String
    DmToolTiptext = m_DmToolTiptext
End Property

'DMPICTURE
Public Property Let DmPicture(mVar As String)
    m_DmPicture = mVar
End Property

Public Property Get DmPicture() As String
    DmPicture = m_DmPicture
End Property

'DMPICTUREEN
Public Property Let DmPictureEn(mVar As String)
    m_DmPictureEn = mVar
End Property

Public Property Get DmPictureEn() As String
    DmPictureEn = m_DmPictureEn
End Property

'DMM1NAME
Public Property Let DmM1Name(mVar As String)
    m_DmM1Name = mVar
End Property

Public Property Get DmM1Name() As String
    DmM1Name = m_DmM1Name
End Property

'DMM1SUBNAME
Public Property Let DmM1SubName(mVar As String)
    m_DmM1SubName = mVar
End Property

Public Property Get DmM1SubName() As String
    DmM1SubName = m_DmM1SubName
End Property

'DMM1LEVEL
Public Property Let DmM1Level(mVar As Long)
    m_DmM1Level = mVar
End Property

Public Property Get DmM1Level() As Long
    DmM1Level = m_DmM1Level
End Property

'DMM2NAME
Public Property Let DmM2Name(mVar As String)
    m_DmM2Name = mVar
End Property

Public Property Get DmM2Name() As String
    DmM2Name = m_DmM2Name
End Property

'DMM3NAME
Public Property Let DmM3Name(mVar As String)
    m_DmM3Name = mVar
End Property

Public Property Get DmM3Name() As String
    DmM3Name = m_DmM3Name
End Property

'DMM3BUTTONTYPE
Public Property Let DmM3ButtonType(mVar As String)
    m_DmM3ButtonType = mVar
End Property

Public Property Get DmM3ButtonType() As String
    DmM3ButtonType = m_DmM3ButtonType
End Property

'DMM3SUBNAME
Public Property Let DmM3SubName(mVar As String)
    m_DmM3SubName = mVar
End Property

Public Property Get DmM3SubName() As String
    DmM3SubName = m_DmM3SubName
End Property

'DMM4NAME
Public Property Let DmM4Name(mVar As String)
    m_DmM4Name = mVar
End Property

Public Property Get DmM4Name() As String
    DmM4Name = m_DmM4Name
End Property

'DMM4BUTTONTYPE
Public Property Let DmM4ButtonType(mVar As String)
    m_DmM4ButtonType = mVar
End Property

Public Property Get DmM4ButtonType() As String
    DmM4ButtonType = m_DmM4ButtonType
End Property

'DMM4SUBNAME
Public Property Let DmM4SubName(mVar As String)
    m_DmM4SubName = mVar
End Property

Public Property Get DmM4SubName() As String
    DmM4SubName = m_DmM4SubName
End Property

'DMFUNZIONE
Public Property Let DmFunzione(mVar As String)
    m_DmFunzione = mVar
End Property

Public Property Get DmFunzione() As String
    DmFunzione = m_DmFunzione
End Property

'DMDLLNAME
Public Property Let DmDllName(mVar As String)
    m_DmDllName = mVar
End Property

Public Property Get DmDllName() As String
    DmDllName = m_DmDllName
End Property

'DMTIPOFINIM
Public Property Let DmTipoFinIm(mVar As String)
    m_DmTipoFinIm = mVar
End Property

Public Property Get DmTipoFinIm() As String
    DmTipoFinIm = m_DmTipoFinIm
End Property

'DMFUNCTIONCUR
Public Property Let DmFunctionCur(mVar As String)
    m_DmFunctionCur = mVar
End Property

Public Property Get DmFunctionCur() As String
    DmFunctionCur = m_DmFunctionCur
End Property

'DMFUNCTIONSTATUS
Public Property Let DmFunctionStatus(mVar As String)
    m_DmFunctionStatus = mVar
End Property

Public Property Get DmFunctionStatus() As String
    DmFunctionStatus = m_DmFunctionStatus
End Property

'DMPROVIDER
Public Property Let DmProvider(mVar As e_Provider)
    m_DmProvider = mVar
End Property

Public Property Get DmProvider() As e_Provider
    DmProvider = m_DmProvider
End Property

'DMOBJLIST
Public Property Set DmObjList(mVar As Object)
   Set m_DmObjList = mVar
End Property

Public Property Get DmObjList() As Object
   Set DmObjList = m_DmObjList
End Property

'DMDLI2RDBMSACT
Public Property Let DmDli2RdbmsACT(mVar As Boolean)
    m_DmDli2RdbmsACT = mVar
End Property

Public Property Get DmDli2RdbmsACT() As Boolean
    DmDli2RdbmsACT = m_DmDli2RdbmsACT
End Property

'DMPARAM_MAXSIZE
Public Property Let DmParam_MaxSize(mVar As Integer)
    m_DmParam_MaxSize = mVar
End Property

Public Property Get DmParam_MaxSize() As Integer
    DmParam_MaxSize = m_DmParam_MaxSize
End Property

'DMPARAM_PERCOUT
Public Property Let DmParam_PercOut(mVar As String)
    m_DmParam_PercOut = mVar
End Property

Public Property Get DmParam_PercOut() As String
    DmParam_PercOut = m_DmParam_PercOut
End Property

'DMPARAM_PERCCPYSTRUC
Public Property Let DmParam_PercCpyStruc(mVar As String)
    m_DmParam_PercCpyStruc = mVar
End Property

Public Property Get DmParam_PercCpyStruc() As String
    DmParam_PercCpyStruc = m_DmParam_PercCpyStruc
End Property

'DMPARAM_PERCCPYFIELD
Public Property Let DmParam_PercCpyField(mVar As String)
    m_DmParam_PercCpyField = mVar
End Property

Public Property Get DmParam_PercCpyField() As String
    DmParam_PercCpyField = m_DmParam_PercCpyField
End Property

'''DMPARAM_PERCCPYMOVE
''Public Property Let DmParam_PercCpyMove(mVar As String)
''    m_DmParam_PercCpyMove = mVar
''End Property
''
''Public Property Get DmParam_PercCpyMove() As String
''    DmParam_PercCpyMove = m_DmParam_PercCpyMove
''End Property

'''DMPARAM_PERCCPYKEY
''Public Property Let DmParam_PercCpyKey(mVar As String)
''    m_DmParam_PercCpyKey = mVar
''End Property
''
''Public Property Get DmParam_PercCpyKey() As String
''    DmParam_PercCpyKey = m_DmParam_PercCpyKey
''End Property

'''DMPARAM_PERCJCLLOAD
''Public Property Let DmParam_PercJclLoad(mVar As String)
''    m_DmParam_PercJclLoad = mVar
''End Property
''
''Public Property Get DmParam_PercJclLoad() As String
''    DmParam_PercJclLoad = m_DmParam_PercJclLoad
''End Property

'''DMPARAM_PERCCPYPROGCOB
''Public Property Let DmParam_PercCpyProgCob(mVar As String)
''    m_DmParam_PercCpyProgCob = mVar
''End Property

''Public Property Get DmParam_PercCpyProgCob() As String
''    DmParam_PercCpyProgCob = m_DmParam_PercCpyProgCob
''End Property

'DMPARAM_PERCCPYEXEC
Public Property Let DmParam_PercCpyExec(mVar As String)
    m_DmParam_PercCpyExec = mVar
End Property

Public Property Get DmParam_PercCpyExec() As String
    DmParam_PercCpyExec = m_DmParam_PercCpyExec
End Property

'DMPARAM_PERCOUTFIELD
Public Property Let DmParam_PercOutField(mVar As String)
    m_DmParam_PercOutField = mVar
End Property

Public Property Get DmParam_PercOutField() As String
    DmParam_PercOutField = m_DmParam_PercOutField
End Property

'DMPARAM_OUTFIELD_DB
Public Property Let DmParam_OutField_Db(mVar As String)
  m_DmParam_OutField_Db = mVar
End Property

Public Property Get DmParam_OutField_Db() As String
  DmParam_OutField_Db = m_DmParam_OutField_Db
End Property

'DMPARAM_OUTFIELD_ORC
Public Property Let DmParam_OutField_Orc(mVar As String)
  m_DmParam_OutField_Orc = mVar
End Property

Public Property Get DmParam_OutField_Orc() As String
  DmParam_OutField_Orc = m_DmParam_OutField_Orc
End Property

'DMPARAM_PercSQLExec
Public Property Let DmParam_PercSQLExec(mVar As String)
    m_DmParam_PercSQLExec = mVar
End Property

Public Property Get DmParam_PercSQLExec() As String
  DmParam_PercSQLExec = m_DmParam_PercSQLExec
End Property

'DMPARAM_OUTFIELD_VSAM
Public Property Let DmParam_OutField_Vsam(mVar As String)
  m_DmParam_OutField_Vsam = mVar
End Property

Public Property Get DmParam_OutField_Vsam() As String
  DmParam_OutField_Vsam = m_DmParam_OutField_Vsam
End Property

'DMPARAM_PERCOUTCPY
Public Property Let DmParam_PercOutCpy(mVar As String)
  m_DmParam_PercOutCpy = mVar
End Property

Public Property Get DmParam_PercOutCpy() As String
  DmParam_PercOutCpy = m_DmParam_PercOutCpy
End Property

'DMCOLLECTIONPARAM
Public Property Set DmCollectionParam(mVar As Collection)
   Set m_DmCollectionParam = mVar
End Property

Public Property Get DmCollectionParam() As Collection
   Set DmCollectionParam = m_DmCollectionParam
End Property

'DMASTERIXFORDELETE
Public Property Let DmAsterixForDelete(mVar As String)
    m_DmAsterixForDelete = mVar
End Property

Public Property Get DmAsterixForDelete() As String
    DmAsterixForDelete = m_DmAsterixForDelete
End Property

'DMASTERIXFORWHERE
Public Property Let DmAsterixForWhere(mVar As String)
    m_DmAsterixForWhere = mVar
End Property

Public Property Get DmAsterixForWhere() As String
    DmAsterixForWhere = m_DmAsterixForWhere
End Property

Public Sub Carica_Parametri_DataManager()
   Dim wParam() As String
   
   'DATABASE PATTERN
   wParam = m_fun.RetrieveParameterValues("DATABASE_PATTERN_NAME")
   
   If InStr(1, wParam(0), "PARAMETER KEY NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   If InStr(1, wParam(0), "PARAMETER VALUE NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   pn_Databases = wParam(0)
   
   'TABLE PATTERN
   wParam = m_fun.RetrieveParameterValues("TABLE_PATTERN_NAME")
   
   If InStr(1, wParam(0), "PARAMETER KEY NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   If InStr(1, wParam(0), "PARAMETER VALUE NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   pn_Tables = wParam(0)
   
   'CREATOR PATTERN
   wParam = m_fun.RetrieveParameterValues("CREATOR_PATTERN_NAME")
   
   If InStr(1, wParam(0), "PARAMETER KEY NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   If InStr(1, wParam(0), "PARAMETER VALUE NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   pn_Creators = wParam(0)
   
    'CREATOR PATTERN
   wParam = m_fun.RetrieveParameterValues("TABLESPACE_PATTERN_NAME")
   
   If InStr(1, wParam(0), "PARAMETER KEY NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   If InStr(1, wParam(0), "PARAMETER VALUE NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   pn_TableSpaces = wParam(0)
   
    'PRIMARY KEY PATTERN
   wParam = m_fun.RetrieveParameterValues("PRIMARYKEY_PATTERN_NAME")
   
   If InStr(1, wParam(0), "PARAMETER KEY NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   If InStr(1, wParam(0), "PARAMETER VALUE NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   pn_PrimaryKey = wParam(0)
   
    'INDEXES PATTERN
   wParam = m_fun.RetrieveParameterValues("INDEXES_PATTERN_NAME")
   
   If InStr(1, wParam(0), "PARAMETER KEY NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   If InStr(1, wParam(0), "PARAMETER VALUE NOT") > 0 Then
      wParam(0) = "*"
   End If
   
   pn_Indexes = wParam(0)
End Sub
