VERSION 5.00
Begin VB.Form MadmdF_TableSpaces 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "TableSpaces' properties"
   ClientHeight    =   5175
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6705
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5175
   ScaleWidth      =   6705
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton CmdNew 
      Caption         =   "New"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   1440
      Picture         =   "MadmdF_TableSpaces.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   4410
      Width           =   945
   End
   Begin VB.TextBox txtDim 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1380
      MaxLength       =   4
      TabIndex        =   24
      Top             =   2460
      Width           =   1995
   End
   Begin VB.TextBox txtStorageGroup 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1380
      MaxLength       =   18
      TabIndex        =   22
      Top             =   1980
      Width           =   5205
   End
   Begin VB.TextBox txtDsetPass 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   4560
      MaxLength       =   18
      TabIndex        =   20
      Top             =   2460
      Width           =   2025
   End
   Begin VB.CheckBox chkCompress 
      Caption         =   "Compress"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   2430
      TabIndex        =   19
      Top             =   1560
      Width           =   1095
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "Close"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   1410
      TabIndex        =   18
      Top             =   1560
      Width           =   765
   End
   Begin VB.TextBox txtLockSize 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   4770
      MaxLength       =   4
      TabIndex        =   16
      Top             =   1020
      Width           =   1815
   End
   Begin VB.TextBox txtMdDateTbs 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   4575
      MaxLength       =   18
      TabIndex        =   8
      Top             =   2940
      Width           =   2025
   End
   Begin VB.TextBox txtCrDateTbs 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1365
      MaxLength       =   18
      TabIndex        =   7
      Top             =   2940
      Width           =   2025
   End
   Begin VB.TextBox txtExtSize 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1380
      MaxLength       =   4
      TabIndex        =   6
      Top             =   540
      Width           =   1965
   End
   Begin VB.TextBox txtBufferPool 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   1380
      MaxLength       =   18
      TabIndex        =   5
      Top             =   1020
      Width           =   1965
   End
   Begin VB.TextBox txtPreFetchSize 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   4755
      MaxLength       =   4
      TabIndex        =   4
      Top             =   540
      Width           =   1845
   End
   Begin VB.ComboBox cboNomeTbs 
      BackColor       =   &H00FFFFC0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   360
      Left            =   1380
      TabIndex        =   3
      Top             =   90
      Width           =   5205
   End
   Begin VB.TextBox txtDescriptionTbs 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   855
      Left            =   1350
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   3420
      Width           =   5235
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   5670
      Picture         =   "MadmdF_TableSpaces.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   4410
      Width           =   945
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   240
      Picture         =   "MadmdF_TableSpaces.frx":1E74
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   4410
      Width           =   945
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      Caption         =   "Lenght"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   720
      TabIndex        =   25
      Top             =   2520
      Width           =   555
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      Caption         =   "Storage Group"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   90
      TabIndex        =   23
      Top             =   2040
      Width           =   1200
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Caption         =   "DSet Pass"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   3630
      TabIndex        =   21
      Top             =   2520
      Width           =   855
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      Caption         =   "Lock Size"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   3870
      TabIndex        =   17
      Top             =   1080
      Width           =   795
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Update Date"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   3450
      TabIndex        =   15
      Top             =   3000
      Width           =   1050
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Creation Date"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   3000
      Width           =   1170
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Extent Size"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   270
      TabIndex        =   13
      Top             =   630
      Width           =   1005
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Name"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   825
      TabIndex        =   12
      Top             =   150
      Width           =   450
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "PreFetch Size"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   3510
      TabIndex        =   11
      Top             =   600
      Width           =   1155
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Buffer Pool"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   330
      TabIndex        =   10
      Top             =   1080
      Width           =   960
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "Description"
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   300
      TabIndex        =   9
      Top             =   3510
      Width           =   975
   End
End
Attribute VB_Name = "MadmdF_TableSpaces"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim pModify As Boolean
Dim pLoad As Boolean

Private Sub cboNomeTbs_Change()
  Dim rs As Recordset
  
  'Carica le proprietÓ del tablespaces
  Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_TableSpaces Where " & _
                                "IdTableSpc = " & pIndex_TableSpace)
  If rs.RecordCount Then
    rs!Nome = cboNomeTbs.text
    rs!Data_Modifica = Now
    rs.Update
  End If
  rs.Close
  
  pNome_TableSpace = cboNomeTbs.text
End Sub

Private Sub cboNomeTbs_Click()
  Dim rs As Recordset
  
  If Not pLoad Then
    pModify = True
  End If
   
  pIndex_TableSpace = cboNomeTbs.ItemData(cboNomeTbs.ListIndex)
  pNome_TableSpace = cboNomeTbs.list(cboNomeTbs.ListIndex)
  
  'Carica le proprietÓ del tablespaces
  Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_TableSpaces Where " & _
                                "IdTableSpc = " & pIndex_TableSpace)
  If rs.RecordCount Then
    txtBufferPool.text = TN(rs!BufferPool)
    txtCrDateTbs.text = TN(rs!Data_Creazione)
    txtDescriptionTbs.text = TN(rs!Descrizione)
    txtDim.text = IIf(TN(rs!Dimensionamento, "LONG") <> -1, TN(rs!Dimensionamento, "LONG"), "")
    txtDsetPass.text = TN(rs!DSetPass)
    txtExtSize.text = IIf(TN(rs!ExtentSize, "LONG") <> -1, TN(rs!ExtentSize, "LONG"), "")
    txtPreFetchSize.text = IIf(TN(rs!PreFetchSize, "LONG") <> -1, TN(rs!PreFetchSize, "LONG"), "")
    txtStorageGroup.text = TN(rs!StorageGroup)
    If rs!Compress Then
      chkCompress.Value = 1
    Else
      chkCompress.Value = 0
    End If
    If rs!Close Then
      chkClose.Value = 1
    Else
      chkClose.Value = 0
    End If
  End If
  rs.Close
End Sub

Private Sub cboNomeTbs_KeyPress(KeyAscii As Integer)
  'Maiuscolo
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub chkClose_Click()
  If Not pLoad Then
    pModify = True
    txtMdDateTbs.text = Now
  End If
End Sub

Private Sub chkCompress_Click()
  If Not pLoad Then
    pModify = True
    txtMdDateTbs.text = Now
  End If
End Sub

Private Sub cmdAnnulla_Click()
  Dim wRet As Long
  
  wRet = vbYes
  If pModify Then
    wRet = MsgBox("Do you want to close this windows without saving?", vbQuestion + vbYesNo, DataMan.DmNomeProdotto)
  End If
  Unload Me
End Sub

Private Sub CmdNew_Click()
  AggiornaTablespaces
  MsgBox "Warning! this operation change names of TableSpaces for DB!", vbOKOnly + vbInformation, "Attention!"
  Unload Me
End Sub

Private Sub AggiornaTablespaces()
  Dim ts As Recordset, ts1 As Recordset, rs As Recordset
  Dim tbsname As String
  
  tbsname = Trim(cboNomeTbs.text)
  Set ts = m_fun.Open_Recordset("select * from " & PrefDb & "_TableSpaces where nome = '" & tbsname & "'")
  If Not ts.RecordCount Then
    ts.AddNew
    'Nuovo Id:
    Set ts1 = m_fun.Open_Recordset("Select max(IdTableSpc) From " & PrefDb & "_TableSpaces")
    ts!IdTableSpc = IIf(IsNull(ts1(0)), 1, ts1(0) + 1)
  End If
  ts!Nome = tbsname
  ts!StorageGroup = txtStorageGroup.text
  ts!Data_Modifica = Now
  'ts!PRIQTY = "<priqty>"
  'ts!SECQTY = "<secqty>"
  'ts!FREEPAGE = "<freepage>"
  'ts!PCTFREE = "<pctfree>"
  ts!BufferPool = txtBufferPool.text
  ts!Descrizione = txtDescriptionTbs.text
  ts!PreFetchSize = Trim("0" & txtPreFetchSize.text)
  ts!ExtentSize = Trim("0" & txtExtSize.text)
  ts!Dimensionamento = Trim("0" & txtDim.text)
  ts!LockSize = txtLockSize.text
  ts!DSetPass = txtDsetPass.text
  ts!Compress = False
  'CCSID?
  ts1.Close
  
  'Aggiorna l'id del tablespace associato alla tabella
  Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Tabelle Where " & _
                                "IdTable = " & pIndex_TbCorrente)
  If rs.RecordCount Then
    rs!IdTableSpc = ts!IdTableSpc
    rs.Update
  End If
  rs.Close
  
  ts.Update
  ts.Close
End Sub

Private Sub cmdOK_Click()
  MsgBox "Warning! this operation change names of TableSpaces for DB!", vbOKOnly + vbInformation, "Attention!"
  'Screen.MousePointer = vbHourglass
   
  'silvia 13-3-2008: via la BeginTrans...!!!!
'  DataMan.DmConnection.CommitTrans
  'Screen.MousePointer = vbDefault
  AggiornaTablespaces
 
  Unload Me
End Sub

Private Sub Form_Load()
  pLoad = True
  
  'Carica i Tablespace disponibili nella combo
  Carica_TableSpace_Combo cboNomeTbs
   
  Seleziona_TableSpace_Combo
   
  pLoad = False
End Sub

Public Sub Seleziona_TableSpace_Combo()
  'Ricerca nella combo tramite id il table space e lo seleziona
  Dim i As Long
  
  For i = 0 To cboNomeTbs.ListCount
    If cboNomeTbs.ItemData(i) = pIndex_TableSpace Then
      cboNomeTbs.ListIndex = i
      Exit For
    End If
  Next i
End Sub

Private Sub Form_Resize()
'  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub txtBufferPool_KeyPress(KeyAscii As Integer)
  If Not pLoad Then
    pModify = True
    txtMdDateTbs.text = Now
  End If
  
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtCrDateTbs_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub txtDescriptionTbs_KeyPress(KeyAscii As Integer)
  If Not pLoad Then
    pModify = True
  End If
  
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtDim_KeyPress(KeyAscii As Integer)
  If Not pLoad Then
    pModify = True
  End If
  
  If Not IsNumeric(Chr(KeyAscii)) And KeyAscii <> 8 Then
    KeyAscii = 0
    Exit Sub
  End If
End Sub

Private Sub txtDsetPass_KeyPress(KeyAscii As Integer)
  If Not pLoad Then
    pModify = True
  End If
  
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtExtSize_KeyPress(KeyAscii As Integer)
  If Not pLoad Then
    pModify = True
    txtMdDateTbs.text = Now
  End If
  
  If Not IsNumeric(Chr(KeyAscii)) And KeyAscii <> 8 Then
    KeyAscii = 0
    Exit Sub
  End If
End Sub

Private Sub txtLockSize_KeyPress(KeyAscii As Integer)
  If Not pLoad Then
    pModify = True
    txtMdDateTbs.text = Now
  End If
  
  If Not IsNumeric(Chr(KeyAscii)) And KeyAscii <> 8 Then
    KeyAscii = 0
    Exit Sub
  End If
End Sub

Private Sub txtMdDateTbs_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub txtPreFetchSize_KeyPress(KeyAscii As Integer)
  If Not pLoad Then
    pModify = True
    txtMdDateTbs.text = Now
  End If
  
  If Not IsNumeric(Chr(KeyAscii)) And KeyAscii <> 8 Then
    KeyAscii = 0
    Exit Sub
  End If
End Sub

Private Sub txtStorageGroup_KeyPress(KeyAscii As Integer)
  If Not pLoad Then
    pModify = True
  End If
  
  KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
