Attribute VB_Name = "MadmdM_RDBMS"
Option Explicit

'SQ - In modulo di classe... non lo aggiungeva alla collection
'''Type GbIdxCol
'''  IdIndex As Long
'''  IdColonna As Long
'''  IdTable As Long
'''  Nome As String
'''End Type
Global idxcol() As GbIdxCol
'Global parentIdxCol() As GbIdxCol

'''''''Array contenente le PK e le SK
''''''Public Type GbKey
''''''  IdSegmento As Long
''''''  NomeKey As String
''''''  IdCmp As Long
''''''End Type
Global Key() As GbKey

Global parentSeqKeys() As GbKey

'SQ - In modulo di classe... non lo aggiungeva alla collection
'Array per contenere i field
''''Type GbField
''''  IdField As Long
''''  IdSegmento As Long
''''  Nome As String
''''  Posizione As Long
''''  Lunghezza As Long
''''  Primario As Boolean
''''  Unique As Boolean
''''  Prov As String
''''  'SQ: serve alle XDFld per non rifare tutto il giro o andare sulle tabelle
''''  idxColumns() As GbIdxCol
''''End Type
Global Field() As GbField

'''''Array per contenere I segmenti
''''Public Type GbSeg
''''  IdSegmento As Long
''''  IdSegmentoOrigine As Long
''''  Nome As String
''''  Parent As String
''''  Prov As String
''''  'SQ: serve per le PK dei campi figli...
''''  'ex SQ: IdxcolumnsKey() As GbIdxCol
''''  SeqKeys() As GbKey
''''End Type
Global Segmenti() As GbSeg

'Per definire le trascodifiche da DB2 a ORACLE e viceversa (caricato dall'ini)
Type t_TrascDB2ORC
   DB2_Type As String
   ORACLE_Type As String
End Type
Public m_TrsDB2ORC() As t_TrascDB2ORC

Public Tabelle() As TabDb

'SQ
'se non serve toglierlo da Global...
Global XDFields() As XDField

'Variabili che servono per la trascodifica del Tipo COBOL --> DB2
Global DB2Tipo As String
Global DB2Lunghezza As Long
Global DB2Decimali As Long

'DLI -> oracle
Global ORACLETipo As String
Global ORACLELunghezza As Long
Global ORACLEDecimali As Long

'Tiene traccia del Db Selezionato all'interno della classe di tutti i db
Global pIndexDBSelected As String

Global pIndex_NewDB As Long
'TILVIA 04-05-2009
Global pIndex_DBDCorrente As Long
'''
Global pIndex_DBCorrente As Long
Global pNome_DBCorrente As String
Global pOrigin_DBCorrente As String

Global pIndex_TbClass As Long

Global pIndex_TbCorrente As Long
Global pNome_TBCorrente As String

Global pIndex_TableSpace As Long
Global pNome_TableSpace As String

Global pIndex_TableJoinCorrente As Long

Global pIndex_ColonnaCorrente As Long
Global pNome_ColonnaCorrente As String

Global pIndex_IndiceCorrente As Long
Global pNome_IndiceCorrente As String

Global pStatus_Table As Boolean 'true = Da salvare ;False = Non salvare
Global pStatus_Column As Boolean 'true = Da salvare ;False = Non salvare
Global pOrdinal_Column As Boolean 'true = Da salvare ;False = Non salvare
Global pStatus_Index As Boolean  'Vedi come sopra

Global pIndex_ForeignTable As Long
Global pNome_ForeignTable As String
Global pIndex_ForeignColonna As Long
Global pNome_ForeignColonna As String

Global pIndex_IdxColonna As String
Global pNome_IdxColonna As String
Global pIndex_Ordinale_IdxCol As Long

Global pForeign_Colonna_IsUsed  As Boolean 'Serve per dire se una colonna � usata in una chiave foreign
Global pIndexKey_Colonna_IsUsed  As Boolean

'Variabili per definire i nomi di tabelle, db e creator,Tablespaces da migrare da DLI a RDBMS
Global pn_Databases As String
Global pn_Tables As String
Global pn_Creators As String
Global pn_TableSpaces As String
Global pn_PrimaryKey As String
Global pn_Indexes As String

Global GbNomeTbInAdd As String

Sub Drop_Indici_Doppi()
  Dim rsIndexI As Recordset, rsIndexP As Recordset
  
  'ricava tutti gli indici unique
  Set rsIndexI = m_fun.Open_Recordset("Select Idindex, idtable from DMORC_Index where " & _
                                      "Tipo = 'I' order by IdIndex")
  While Not rsIndexI.EOF
    'ricava eventuali pk
    Set rsIndexP = m_fun.Open_Recordset("Select Idindex from DMORC_Index where " & _
                                        "Tipo = 'P' and IdTable = " & rsIndexI!IdTable & " order by IdIndex ")
    'se ci sono
    If rsIndexP.RecordCount Then
      'se esiste verifica i campi.
      calcola rsIndexP!IdIndex, rsIndexI!IdIndex
    End If
    rsIndexP.Close
    rsIndexI.MoveNext
  Wend
  rsIndexI.Close
End Sub

Private Sub calcola(indexP As Long, indexI As Long)
  Dim rrP As New Recordset, rrI As New Recordset
  Dim b As Integer
  
  Set rrP = m_fun.Open_Recordset("Select * from DMORC_IdxCol where IdIndex=" & indexP & " Order by Ordinale")
  Set rrI = m_fun.Open_Recordset("Select * from DMORC_IdxCol where IdIndex=" & indexI & " Order by Ordinale")
  If rrP.RecordCount = rrI.RecordCount Then
    b = 0
    While Not rrP.EOF
      If rrP!IdColonna = rrI!IdColonna Then
        b = b + 1
        rrI.MoveNext
      End If
      rrP.MoveNext
    Wend
        
    If b = rrP.RecordCount Then
      DataMan.DmConnection.Execute "delete from DMORC_Index where IdIndex=" & indexI
      DataMan.DmConnection.Execute "delete from DMORC_IdxCol where IdIndex=" & indexI
    End If
  End If
  rrP.Close
  rrI.Close
End Sub

Public Sub Carica_Combo_Tipo(Tipo As String, cbo As ComboBox)
  cbo.Clear
'  Stop
  Select Case Trim(UCase(Tipo))
    Case "DB2"
      cbo.AddItem Trim(UCase("Char"))
      cbo.AddItem Trim(UCase("VarChar"))
      cbo.AddItem Trim(UCase("Integer"))
      cbo.AddItem Trim(UCase("SmallInt"))
      cbo.AddItem Trim(UCase("BigInt"))
      cbo.AddItem Trim(UCase("Decimal"))
      cbo.AddItem Trim(UCase("Date"))
      cbo.AddItem Trim(UCase("Time"))
      cbo.AddItem Trim(UCase("TimeStamp"))
    Case "ORACLE"
      cbo.AddItem Trim(UCase("Char"))
      cbo.AddItem Trim(UCase("VarChar"))
      cbo.AddItem Trim(UCase("VarChar2"))
      cbo.AddItem Trim(UCase("Number"))
      cbo.AddItem Trim(UCase("Date"))
      cbo.AddItem Trim(UCase("TimeStamp"))
    Case "VSAM"
  End Select
End Sub

Public Function get_PrefixQuery_TipoDB(TipoDB As String) As String
  Select Case Trim(UCase(TipoDB))
    Case "DB2"
      get_PrefixQuery_TipoDB = "DMDB2"
      dbType = "DB2"
    Case "ORACLE"
      get_PrefixQuery_TipoDB = "DMORC"
      dbType = "ORACLE"
    Case "VSAM"
      get_PrefixQuery_TipoDB = "DMVSAM"
      dbType = "VSAM"
    Case "DLI"
  End Select
End Function

Public Sub Carica_Lista_Tabelle(Lsw As ListView, TipoDB As String, Optional idDB As Long)
  Dim rs As Recordset
  Dim SQL As String
  Dim i As Long
   
  'Voce di default
  Lsw.ListItems.Clear
  Lsw.ListItems.Add , "NEWTABLE", "Create new table", , 2

  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Tabelle Where " & _
                                "IdDb = " & pIndex_DBCorrente & " order by nome ")
  While Not rs.EOF
    Lsw.ListItems.Add , Trim(rs!IdTable) & "#ID", rs!Nome, , 1
    Lsw.ListItems(Lsw.ListItems.Count).Tag = rs!IdTable
    Lsw.ListItems(Lsw.ListItems.Count).ListSubItems.Add , , TN(rs!Data_Creazione)
    Lsw.ListItems(Lsw.ListItems.Count).ListSubItems.Add , , TN(rs!Data_Modifica)
     
    rs.MoveNext
  Wend
  rs.Close
End Sub

Public Sub Load_Lista_Tabelle_In_Combo(cbo As ComboBox, TipoDB As String, idDB As Long)
  Dim SQL As String
  Dim i As Long
  Dim rs As Recordset, rs1 As Recordset
   
  cbo.Clear
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs1 = m_fun.Open_Recordset("select ForeignIdTable from " & PrefDb & "_index where tipo  = 'F' and idtable = " & pIndex_TbCorrente)
  Set rs = m_fun.Open_Recordset("Select IdTable, Nome From " & PrefDb & "_Tabelle Where IdDb = " & idDB)
  If rs.RecordCount Then
    'Recupera l'id table
    pIndex_ForeignTable = rs!IdTable
  
    While Not rs.EOF
      cbo.AddItem rs!Nome
      cbo.ItemData(cbo.ListCount - 1) = rs!IdTable
      ' Mauro: imposto nella combo la tabella foreign selezionata
      If Not (rs1.EOF) Then
        If cbo.ItemData(cbo.ListCount - 1) = rs1!ForeignIdTable Then
          cbo.text = cbo.list(cbo.ListCount - 1)
          MadmdF_Index.cboTables.ListIndex = cbo.ListCount - 1
        End If
      End If
      rs.MoveNext
    Wend
  End If
  rs.Close
  rs1.Close
End Sub

Public Function get_Tipo_Key(wTipo As String) As Long
  Select Case Trim(UCase(wTipo))
    Case "I"
      get_Tipo_Key = 3 'Index
    Case "P"
      get_Tipo_Key = 1 'Primary-Key
    Case "F"
      get_Tipo_Key = 2 'Foreign-Key
    Case Else
      get_Tipo_Key = 3 'Index
  End Select
End Function

Public Sub Load_Lista_Colonne_In_Combo(cbo As ComboBox, TipoDB As String, IdTable As Long)
  Dim rs As Recordset
  Dim SQL As String
  Dim i As Long
  
  'Voce di default
  cbo.Clear
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select IdColonna, Nome From " & PrefDb & "_Colonne Where " & _
                                "IdTable = " & IdTable & " order by ordinale")
  While Not rs.EOF
    cbo.AddItem rs!Nome
    cbo.ItemData(cbo.ListCount - 1) = rs!IdColonna
    rs.MoveNext
  Wend
  rs.Close
End Sub

Public Function TN(Dato As Variant, Optional Tipo As String) As Variant
  If IsNull(Dato) Then
    Select Case Tipo
      Case "STRING"
        TN = ""
      Case "LONG"
        TN = -1
      Case "BOOLEAN"
        TN = False
      Case "DECIMAL" 'VIRGILIO 1/3/2007
        TN = 0
      Case Else
        TN = ""
    End Select
  Else
    TN = Dato
  End If
End Function

Function LeggiParam(Wkey As String) As String
  Dim wResp() As String
  
  On Error GoTo ErrResp
  
  wResp = m_fun.RetrieveParameterValues(Wkey)
  If InStr(wResp(0), Wkey) Then
    LeggiParam = ""
  Else
    LeggiParam = wResp(0)
  End If
  Exit Function
ErrResp:
  LeggiParam = ""
End Function

Public Function get_Alias_From_IdTable(cId As Long) As String
  Dim rs As Recordset
   
  'Carica l'alias della tabella corrente se gi� esiste
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select Nome From " & PrefDb & "_Alias Where IdTable = " & cId)
  If rs.RecordCount Then
    get_Alias_From_IdTable = rs!Nome
  End If
  rs.Close
End Function

Public Sub Elimina_Colonna_Indice(wIdColonna As Long)
  Dim rs As Recordset
  Dim IdOrdinale As Long
  Dim wMsg As Long
  Dim i As Long
   
  Screen.MousePointer = vbHourglass
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  'tilvia 06092010
  '  m_fun.FnConnection.Execute
   Set rs = m_fun.Open_Recordset("Select " & m_fun.FnAsterixForDelete & " From " & PrefDb & "_IdxCol Where " & _
                             "IdIndex = " & pIndex_IndiceCorrente & " And " & _
                             "IdTable = " & pIndex_TbCorrente & " And " & _
                             "IdColonna = " & wIdColonna)
   rs.Delete
   rs.Close
   
  'Controlla se ci sono ancora colonne per questo indice
  Set rs = m_fun.Open_Recordset("Select IdIndex From " & PrefDb & "_IdxCol Where " & _
                                "IdIndex = " & pIndex_IndiceCorrente & " And " & _
                                "IdTable = " & pIndex_TbCorrente)
                                          
  If rs.RecordCount = 0 Then
    'Elimina l'indice dalla tabella index
    wMsg = MsgBox("ATTENTION : Current Index [" & pNome_IndiceCorrente & "] does not have associated column indexes." & vbCrLf & _
                  "Do you want to erase it?", vbInformation + vbYesNo, DataMan.DmNomeProdotto)
      
    If wMsg = vbYes Then
      'Dal database
      m_fun.FnConnection.Execute "Delete " & m_fun.FnAsterixForDelete & " From " & PrefDb & "_Index Where " & _
                                 "IdIndex = " & pIndex_IndiceCorrente
      
      'A video
      For i = 1 To MadmdF_Index.lswIndici.ListItems.Count
        If Mid(MadmdF_Index.lswIndici.ListItems(i).Key, 1, InStr(1, MadmdF_Index.lswIndici.ListItems(i).Key, "#IDIDX") - 1) = pIndex_IndiceCorrente Then
          MadmdF_Index.lswIndici.ListItems.Remove i
          Exit For
        End If
      Next i
    End If
  End If
  rs.Close
   
  Screen.MousePointer = vbDefault
End Sub

Public Sub Crea_Nuova_Colonna_Index()
  Dim rs As Recordset
  Dim IdOrdinale As Long
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  'Recupera l'id max
  Set rs = m_fun.Open_Recordset("Select MAX(Ordinale) From " & PrefDb & "_IdxCol Where " & _
                                "IdIndex = " & pIndex_IndiceCorrente & " And IdTable = " & pIndex_TbCorrente)
   
  If IsNull(rs.fields(0).Value) = False Then
    IdOrdinale = rs.fields(0).Value + 1
  Else
    IdOrdinale = 1
  End If
  rs.Close
   
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol Where " & _
                                "IdIndex = " & pIndex_IndiceCorrente & " And IdTable = " & pIndex_TbCorrente & " And " & _
                                "Ordinale = " & IdOrdinale)
  If rs.RecordCount = 0 Then
    rs.AddNew
    
    rs!IdIndex = pIndex_IndiceCorrente
    rs!IdTable = pIndex_TbCorrente
    rs!Ordinale = IdOrdinale
    pIndex_Ordinale_IdxCol = IdOrdinale
    rs!IdColonna = pIndex_IdxColonna
    rs!Order = "ASCENDING" ' Di default
    rs!foridtable = pIndex_ForeignTable
    rs!forIdColonna = -1
    rs.Update
  Else
    'That's impossible
    Stop
  End If
  rs.Close
End Sub

Public Sub Elimina_Indice(IdIndex As Long)
   Dim SQL As String
   Dim rs As Recordset
   
   Screen.MousePointer = vbHourglass
   
   'Elimina gli indici: la EXEC non funzionava
   PrefDb = get_PrefixQuery_TipoDB(TipoDB)
   Set rs = m_fun.Open_Recordset("Select " & DataMan.DmAsterixForDelete & " FROM " & PrefDb & "_Index where " & _
                                 "IdIndex = " & IdIndex)
   rs.Delete
   rs.Close
   
   Screen.MousePointer = vbDefault
   Screen.MousePointer = vbHourglass
   
   'Elimina le associazioni indici colonne
   SQL = "DELETE " & DataMan.DmAsterixForDelete & " FROM " & PrefDb & "_IdxCol where " & _
         "IdIndex = " & IdIndex

   DataMan.DmConnection.Execute (SQL)
   
   Screen.MousePointer = vbDefault
End Sub

Public Sub Elimina_Colonna(IdColonna As Long, IdTable As Long)
   'Questa routine elimina la colonna passata e i riferimenti agli indici
   Dim SQL As String
      
   Screen.MousePointer = vbHourglass
   
   'Elimina La colonna
   PrefDb = get_PrefixQuery_TipoDB(TipoDB)
   SQL = "DELETE " & DataMan.DmAsterixForDelete & " FROM " & PrefDb & "_Colonne where " & _
         "IdColonna = " & IdColonna

   DataMan.DmConnection.Execute (SQL)
   
   'Elimina le associazioni indici colonne
   Screen.MousePointer = vbDefault
   Screen.MousePointer = vbHourglass
   
   SQL = "DELETE " & DataMan.DmAsterixForDelete & " FROM " & PrefDb & "_IdxCol where " & _
         "IdColonna = " & IdColonna & " And IdTable = " & IdTable

   DataMan.DmConnection.Execute (SQL)

   Screen.MousePointer = vbDefault
End Sub

Public Function get_Tipo_Colonna_By_Id(cId As Long) As String
  Dim rs As Recordset
  
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select tipo From " & PrefDb & "_Colonne Where IdColonna = " & cId)
  If rs.RecordCount Then
    get_Tipo_Colonna_By_Id = rs!Tipo
  Else
    get_Tipo_Colonna_By_Id = ""
  End If
  rs.Close
End Function

Public Function get_Nome_Colonna_By_Id(cId As Long) As String
  Dim rs As Recordset
  
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select nome From " & PrefDb & "_Colonne Where IdColonna = " & cId)
  If rs.RecordCount Then
    get_Nome_Colonna_By_Id = rs!Nome
  Else
    get_Nome_Colonna_By_Id = ""
  End If
  rs.Close
End Function

Public Sub Carica_TableSpace_Combo(cbo As ComboBox)
  Dim rs As Recordset
   
  cbo.Clear
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select IdTbaleSpc, Nome From " & PrefDb & "_TableSpaces Order by IdTableSpc")
  While Not rs.EOF
    cbo.AddItem rs!Nome
    cbo.ItemData(cbo.ListCount - 1) = rs!IdTableSpc
    rs.MoveNext
  Wend
  rs.Close
End Sub

Public Sub Carica_Trascodifica_DB2_To_ORACLE()
  Dim wBuff As String, wRet As Long, WsTR As String
  Dim wDB2 As String, wORACLE As String
  Dim Percorso As String, i As Long
  
  Percorso = DataMan.DmPathPrd & FILE_INI
  
  ReDim m_TrsDB2ORC(0)
  'Carica le trascodifiche dei tipi dal file .ini
  For i = 0 To 100 '100 Tipi!!!!
    'formalismo : nn = TipoDB2 # TipoORACLE
    
    wBuff = String(100, Chr(10))
    wRet = GetPrivateProfileString("TRSTYPE", i & "", "", wBuff, 100, Percorso)
    WsTR = Mid(wBuff, 1, wRet)
    
    If Trim(WsTR) <> "" Then
      wDB2 = Mid(WsTR, 1, InStr(1, WsTR, "#") - 1)
      wORACLE = Mid(WsTR, InStr(1, WsTR, "#") + 1)
      
      ReDim Preserve m_TrsDB2ORC(UBound(m_TrsDB2ORC) + 1)
      
      m_TrsDB2ORC(UBound(m_TrsDB2ORC)).DB2_Type = wDB2
      m_TrsDB2ORC(UBound(m_TrsDB2ORC)).ORACLE_Type = wORACLE
    End If
  Next i
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ: C'� nel FILE_INI la voce "TRSTYPE" con le associazioni... caricata
'     in "m_TrsDB2ORC": o la usiamo o buttiamo via tutto!
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function Trascodifica_Tipo_DB2_To_ORACLE(wTipo As String) As String
  'MG 160107 Modificato trascodifica... legge dal db
  'Dim i As Long
  Dim TbTrasCod As Recordset
      
  On Error GoTo ErrorHandler
  Set TbTrasCod = m_fun.Open_Recordset("SELECT OrcField FROM TrascodificaDB2_ORC where " & _
                                       "Db2Field = '" & wTipo & "'")
   
  If TbTrasCod.RecordCount Then
    Trascodifica_Tipo_DB2_To_ORACLE = TbTrasCod!OrcField
  Else
    Trascodifica_Tipo_DB2_To_ORACLE = "NOT-CODED"
  End If
  TbTrasCod.Close
   
  If Trascodifica_Tipo_DB2_To_ORACLE = "NOT-CODED" Then
    DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Automatic TYPE CODING [FAILED : DB2 Type : " & wTipo & "]... Try to DECODE manually"
    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
  End If
  Exit Function
ErrorHandler:
   
End Function
'SQ tmp - lunghezza in tabella - Portare dentro a Trascodifica_Tipo...
Public Function Trascodifica_Len_DB2_To_ORACLE(wTipo As String, wLen As Long) As Long
  'SQ
  Dim rs As Recordset
  Dim oraLen As Integer
  
  On Error GoTo ErrorHandler
  
  Set rs = m_fun.Open_Recordset("SELECT orxcLen FROM TrascodificaDB2_ORC where " & "Db2Field = '" & wTipo & "'")
  If rs.RecordCount Then
    If IsNumeric(rs!orcLen) Then
      oraLen = rs!orcLen
    End If
  End If
  rs.Close
  
  'DEFAULT:
  If oraLen = 0 Then
    oraLen = wLen
    Select Case UCase(wTipo)
      Case "CHAR"
        oraLen = wLen
      Case "VARCHAR"
        oraLen = wLen
      Case "DATE"
        oraLen = wLen
      Case "TIMESTAMP"
        oraLen = wLen
      Case "TIME"
        oraLen = 8
      Case "SMALLINT"
        oraLen = 5
      Case "INTEGER"
        oraLen = 10
      Case "DECIMAL"
        oraLen = wLen
      'SQ
      Case "FLOAT"
        oraLen = 38
    End Select
  End If
  
  Trascodifica_Len_DB2_To_ORACLE = oraLen
    
Exit Function
ErrorHandler:
  
  Resume Next
End Function

Public Function Get_Linked_TableJoin(cIdTable As Long) As Long
  'Cerca di individuare l'id della tabella in join ricercandola per nome fra le nuove Oracle
  Dim rs As Recordset, rsOrc As Recordset
  Dim wNome As String
   
  Get_Linked_TableJoin = -1
  'Recupera il nome del table space
  Set rs = m_fun.Open_Recordset("Select Nome, IdDb From DMDB2_Tabelle Where IdTable = " & cIdTable)
  If rs.RecordCount Then
    wNome = rs!Nome
    Set rsOrc = m_fun.Open_Recordset("Select IdTable From DMORC_Tabelle Where " & _
                                     "Nome = '" & wNome & "' And IdDBOr = " & rs!idDB)
    If rsOrc.RecordCount Then
      Get_Linked_TableJoin = rsOrc!IdTable
    End If
    rsOrc.Close
  Else
    wNome = ""
  End If
  rs.Close
End Function

'Cerca di individuare l'id della tablespace ricercandolo per nome fra quelli Oracle
Public Function Associa_TableSpace(cIdTable As Long) As Long
  Dim rsDB2Tbs As Recordset, rs As Recordset, rsOrc As Recordset
  Dim wNome As String
   
  Associa_TableSpace = -1
   
  Set rsDB2Tbs = m_fun.Open_Recordset("Select * From DMDB2_Tabelle Where IdTable = " & cIdTable)
  If rsDB2Tbs.RecordCount Then
    'Recupera il nome del table space
    Set rs = m_fun.Open_Recordset("Select * From DMDB2_TableSpaces Where IdTableSpc = " & rsDB2Tbs!IdTableSpc)
    If rs.RecordCount Then
      wNome = rs!Nome
      Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_TableSpaces Where " & _
                                       "Nome = '" & wNome & "' And IdDBOr = " & rsDB2Tbs!IdTableSpc)
      If rsOrc.RecordCount Then
        Associa_TableSpace = rsOrc!IdTableSpc
        
        DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Automatic linking of TableSpaces passed..."
        DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Automatic linking of TableSpaces NOT FOUND [DB2:" & rsDB2Tbs!Nome & "]"
        DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      End If
      rsOrc.Close
    Else
      wNome = ""
    End If
    rs.Close
  End If
  rsDB2Tbs.Close
End Function

Public Function AssegnazioneForeignTable(idTabDb2 As String) As Boolean
  Dim rsForTab As Recordset, rsIndexOrc As Recordset, rsIndex As Recordset
  
  ''''''''''''''taborc = tabdb2
  On Error GoTo ErrorHandler

  AssegnazioneForeignTable = True

  'verifica la presenza di fk nella tabella index con l'idtab di db2
  Set rsIndex = m_fun.Open_Recordset("Select * From DMDB2_Index Where IdTable = " & idTabDb2 & " and tipo = 'F'")
  While Not rsIndex.EOF
    'ricava l'id table foreign
    Set rsForTab = m_fun.Open_Recordset("Select * from DMORC_Tabelle where IdDbOr=" & rsIndex!ForeignIdTable)
    If Not rsForTab.EOF Then
      Set rsIndexOrc = m_fun.Open_Recordset("Select * from DMORC_Index where IdDbOr=" & rsIndex!IdIndex & " AND Tipo='F'")
      rsIndexOrc!ForeignIdTable = rsForTab!IdTable
      rsIndexOrc.Update
      rsIndexOrc.Close
    Else
      'scrive sul log
    End If
    rsForTab.Close
    rsIndex.MoveNext
  Wend
  rsIndex.Close
  Exit Function
ErrorHandler:
  m_fun.WriteLog "Errore in [MadmdM_RDBMS.AssegnazioneForeignTable]: " & ERR.Description
  AssegnazioneForeignTable = False
End Function

Public Sub Update_TableJoin_ORACLE(cIdDBdb2 As Long)
  Dim rs As Recordset, rsOrc As Recordset
  Dim cNome As String, lIdTable As Long
   
  Set rs = m_fun.Open_Recordset("Select Nome, IdTableJoin from DMDB2_Tabelle Where " & _
                                "IdDb = " & cIdDBdb2 & " And IdTableJoin <> -1")
  While Not rs.EOF
    cNome = rs!Nome
    lIdTable = Get_Linked_TableJoin(rs!idTableJoin)
     
    'Update sulla tabella oracle
    Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_Tabelle Where " & _
                                     "Nome = '" & cNome & "' And IdDBOr = " & cIdDBdb2)
    If rsOrc.RecordCount Then
      rsOrc!idTableJoin = lIdTable
      rsOrc.Update
      DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Automatic linking of TableJoin passed..."
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    Else
      DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Automatic linking of TableJoin NOT FOUND [DB2:" & rsOrc!Nome & "]"
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    End If
    rsOrc.Close
    rs.MoveNext
  Wend
  rs.Close
End Sub

Public Sub Migra_Indici_DB2_To_ORACLE(cIdTable As Long, cIdTableDb2 As Long, cIdDb2 As Long)
  Dim rsNew As Recordset, rsOrc As Recordset, rs As Recordset
  Dim wIndex As Long
  Dim Adesso As String
  
  Adesso = Now
  Set rs = m_fun.Open_Recordset("Select * From DMDB2_Index Where IdTable = " & cIdTableDb2)
  While Not rs.EOF
    DataMan.DmConnection.Execute "DELETE * From DMORC_Index Where Tag = 'AUTOMATIC_DB2ORC' AND IdDbOr = " & rs!IdIndex
    
    'ID Index:
    Set rsNew = m_fun.Open_Recordset("Select MAX(IdIndex) From DMORC_Index")
    If rsNew.RecordCount Then
      wIndex = TN(rsNew.fields(0).Value, "LONG") + 1
    Else
      wIndex = 1
    End If
    rsNew.Close
      
    'Crea il nuovo indice nel database
    Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_Index")
    rsOrc.AddNew
    rsOrc!IdIndex = wIndex
    rsOrc!IdTable = cIdTable
    rsOrc!iddbor = rs!IdIndex 'cIdDb2
    rsOrc!IdTableSpc = Associa_TableSpace(rs!IdTable)
    rsOrc!Nome = rs!Nome
    rsOrc!Tipo = rs!Tipo
    rsOrc!IdOrigine = rs!IdOrigine
    rsOrc!ForOnCascade = rs!ForOnCascade
    rsOrc!ForOnDelete = rs!ForOnDelete
    rsOrc!ForOnRestrict = rs!ForOnRestrict
    rsOrc!ForOnSetNull = rs!ForOnSetNull
    rsOrc!ForeignIdTable = rs!ForeignIdTable
    rsOrc!ForOnNoAction = rs!ForOnNoAction
    rsOrc!Used = rs!Used
    rsOrc!Unique = rs!Unique
    rsOrc!Descrizione = "Automatic migration from DB2 to ORACLE"
    rsOrc!Data_Creazione = Adesso
    rsOrc!Tag = "AUTOMATIC_DB2ORC"
     
    rsOrc.Update
    rsOrc.Close
    
    DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Add new Index (" & rs!Nome & ")"
    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
     
    rs.MoveNext
  Wend
  rs.Close
End Sub

Public Sub Migra_Check_DB2_To_ORACLE(cIdTable As Long, cIdTableDb2 As Long, cIdDb2 As Long)
   Dim rsNew As Recordset, rsOrc As Recordset
   Dim rs As Recordset
   Dim wIndex As Long
   Dim Adesso As String
   
   On Error GoTo ErrorHandler
   
   Adesso = Now
   
   Set rs = m_fun.Open_Recordset("Select * From DMDB2_Check_Constraint Where IdTable = " & cIdTableDb2 & " order by IdProgressivo ")
   If rs.RecordCount Then
      DataMan.DmConnection.Execute "DELETE " & DataMan.DmAsterixForDelete & " From DMORC_Check_Constraint Where Tag = 'AUTOMATIC_DB2ORC' AND IdDbOr = " & cIdTableDb2
      
      While Not rs.EOF
         
''''         DataMan.DmConnection.Execute "DELETE " & DataMan.DmAsterixForDelete & " From DMORC_Check_Constraint Where Tag = 'AUTOMATIC_DB2ORC' AND " & _
''''                                      " IdDbOr = " & rs!IdTable & " IdProgressivo=" & rs!idProgressivo
         
         Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_Check_Constraint")
         
         'Crea il nuovo indice nel database
         rsOrc.AddNew
            rsOrc!idProgressivo = rs!idProgressivo
            rsOrc!IdTable = cIdTable
            rsOrc!Nome = rs!Nome
            rsOrc!Condizione = Replace(rs!Condizione, "~", "!")
            rsOrc!Data_Creazione = Adesso
            rsOrc!Data_Modifica = Adesso
            rsOrc!Tag = "AUTOMATIC_DB2ORC"
            rsOrc!iddbor = cIdTableDb2 'cIdDb2
         rsOrc.Update
         
         DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Add new Check (" & rs!Nome & ")"
         DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
         
         rs.MoveNext
         
         DoEvents
      Wend
   End If
   
   rs.Close
   Exit Sub
ErrorHandler:
    ERR.Raise ERR.Description
   m_fun.WriteLog "Errore in [MadmdM_SDBMS.Migra_Check_DB2_To_ORACLE]: " & ERR.Description
End Sub

Public Sub Migra_Trigger_DB2_To_ORACLE(cIdTable As Long, cIdTableDb2 As Long, cIdDb2 As Long)
  Dim rsNew As Recordset, rsOrc As Recordset
  Dim rs As Recordset
  Dim wIndex As Long
  Dim Adesso As String
  
  On Error GoTo ErrorHandler
  
  Adesso = Now
  
  Set rs = m_fun.Open_Recordset("Select * From DMDB2_Trigger Where IdTable = " & cIdTableDb2)
  While Not rs.EOF
    DataMan.DmConnection.Execute "DELETE " & DataMan.DmAsterixForDelete & " From DMORC_Trigger Where Tag = 'AUTOMATIC_DB2ORC' AND IdDbOr = " & rs!IdTrigger
    Set rsNew = m_fun.Open_Recordset("Select MAX(IdTrigger) From DMORC_Trigger")
    If rsNew.RecordCount Then
      wIndex = TN(rsNew.fields(0).Value, "LONG") + 1
    Else
      wIndex = 1
    End If
    rsNew.Close
    
    Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_Trigger")
    'Crea il nuovo indice nel database
    rsOrc.AddNew
    rsOrc!IdTrigger = wIndex
    rsOrc!Nome = rs!Nome
    rsOrc!Creator = rs!Creator
    rsOrc!IdTable = cIdTable
    rsOrc!Campi = rs!Campi
    rsOrc!Evento = rs!Evento
    rsOrc!Transazione = rs!Transazione
    rsOrc!Azione = rs!Azione
    rsOrc!Referencing = rs!Referencing
    rsOrc!Condizione = rs!Condizione
    rsOrc!Corpo = rs!Corpo
    rsOrc!Stato = rs!Stato
    rsOrc!Data_Creazione = Adesso
    rsOrc!Data_Modifica = Adesso
    rsOrc!Tag = "AUTOMATIC_DB2ORC"
    rsOrc!iddbor = rs!IdTrigger 'cIdDb2
    rsOrc.Update
    rsOrc.Close
    
    DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Add new Trigger (" & rs!Nome & ")"
    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    
    rs.MoveNext
    DoEvents
  Wend
  rs.Close
  Exit Sub
ErrorHandler:
  m_fun.WriteLog "Errore in [MadmdM_SDBMS.Migra_Trigger_DB2_To_ORACLE]: " & ERR.Description
End Sub

Public Sub Migra_Colonne_DB2_To_ORACLE(cIdTable As Long, IdTableDB2 As Long, cIdDBOr As Long)
  Dim newIdColonna As Long
  Dim rs As Recordset
  Dim rsNew As Recordset, rsOrc As Recordset
  Dim rsParticolari As Recordset
  
  Set rs = m_fun.Open_Recordset("Select * From DMDB2_Colonne Where IdTable = " & IdTableDB2)
  While Not rs.EOF
    DataMan.DmConnection.Execute "DELETE * From DMORC_Colonne Where Tag = 'AUTOMATIC_DB2ORC' AND IdDbOr = " & rs!IdColonna
    
    'Recupera un id libero
    Set rsNew = m_fun.Open_Recordset("Select MAX(IdColonna) From DMORC_Colonne")
    newIdColonna = TN(rsNew.fields(0).Value, "LONG") + 1
    rsNew.Close
       
    Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_Colonne Where " & _
                                     "IdTable = " & cIdTable & " And IdColonna = " & newIdColonna)
    If rsOrc.RecordCount = 0 Then
      rsOrc.AddNew
      'If rs!IdTable = 8662 Then Stop
      rsOrc!IdColonna = newIdColonna
      rsOrc!IdTable = cIdTable
      rsOrc!Ordinale = rs!Ordinale
      rsOrc!iddbor = rs!IdColonna 'cIdDBOr
      rsOrc!idTableJoin = rs!IdTable
      rsOrc!IdCmp = rs!IdCmp
      rsOrc!Nome = rs!Nome
      rsOrc!lunghezza = Trascodifica_Len_DB2_To_ORACLE(rs!Tipo, rs!lunghezza)
      rsOrc!Decimali = rs!Decimali
      'MG 290107
      rsOrc!IsDefault = rs!IsDefault
      If rsOrc!IsDefault Then
        If Trim(rs!defaultValue) <> "" Then
          'prende il valore inserito in db2...
          'tranne il caso in cui si tratti di campi particolari (es. CURRENT SQLID)
          'in quel caso va a leggere il corrispondente oracle
          Set rsParticolari = m_fun.Open_Recordset("Select * from DM_DB2ORC_CAMPI where DB2='" & rs!defaultValue & "'")
          If rsParticolari.RecordCount > 0 Then
            rsOrc!defaultValue = rsParticolari!ORACLE
          ElseIf rs!defaultValue = "NULL" Then
            Select Case UCase(rs!Tipo)
              Case "INTEGER", "SMALLINT", "DECIMAL"
                rsOrc!defaultValue = LeggiParam("DO_DEFVALNUM")
              Case "CHAR", "VARCHAR", "VARCHAR2"
                rsOrc!defaultValue = LeggiParam("DO_DEFVALCHAR")
              Case "DATE"
                rsOrc!defaultValue = LeggiParam("DO_DEFVALDATE")
              Case "TIMESTAMP"
                rsOrc!defaultValue = LeggiParam("DO_DEFVALTIMESTP")
              Case "TIME"
                rsOrc!defaultValue = LeggiParam("DO_DEFVALTIME")
            End Select
          Else
            'prende i valori dalla tabella
            Select Case UCase(rs!Tipo)
              Case "INTEGER", "SMALLINT", "DECIMAL"
                rsOrc!defaultValue = rs!defaultValue
              Case "CHAR", "VARCHAR", "VARCHAR2"
                rsOrc!defaultValue = "'" & rs!defaultValue & "'"
              Case "DATE"
                rsOrc!defaultValue = "'" & rs!defaultValue & "'"
              Case "TIMESTAMP"
                rsOrc!defaultValue = "'" & rs!defaultValue & "'"
              Case "TIME"
                rsOrc!defaultValue = "'" & rs!defaultValue & "'"
            End Select
          End If
          rsParticolari.Close
        Else
          'prende i valori di default
          Select Case UCase(rs!Tipo)
            Case "INTEGER", "SMALLINT", "DECIMAL"
              rsOrc!defaultValue = LeggiParam("DO_DEFVALNUM")
            Case "CHAR", "VARCHAR", "VARCHAR2"
              rsOrc!defaultValue = LeggiParam("DO_DEFVALCHAR")
            Case "DATE"
              rsOrc!defaultValue = LeggiParam("DO_DEFVALDATE")
            Case "TIMESTAMP"
              rsOrc!defaultValue = LeggiParam("DO_DEFVALTIMESTP")
            Case "TIME"
              rsOrc!defaultValue = LeggiParam("DO_DEFVALTIME")
          End Select
        End If
      ElseIf Trim(rs!defaultValue) <> "" Then
        If rs!defaultValue = "NULL" Then
          Select Case UCase(rs!Tipo)
            Case "INTEGER", "SMALLINT", "DECIMAL"
              rsOrc!defaultValue = 0
            Case "CHAR", "VARCHAR", "VARCHAR2"
              rsOrc!defaultValue = "' '"
            Case "DATE"
              rsOrc!defaultValue = "' '"
            Case "TIMESTAMP"
              rsOrc!defaultValue = "' '"
            Case "TIME"
              rsOrc!defaultValue = "' '"
          End Select
        Else
          rsOrc!defaultValue = ""
        End If
      End If
      rsOrc!Formato = TN(rs!Formato)
      rsOrc!Etichetta = TN(rs!Etichetta)
      rsOrc!Null = rs!Null
      rsOrc!Descrizione = "DB2: " & rs!Tipo
      rsOrc!Tag = "AUTOMATIC_DB2ORC"
      rsOrc!Tipo = Trascodifica_Tipo_DB2_To_ORACLE(rs!Tipo)
      rsOrc!RoutLoad = TN(rs!RoutLoad)
      rsOrc!RoutRead = TN(rs!RoutRead)
      rsOrc!RoutWrite = TN(rs!RoutWrite)
      rsOrc!RoutKey = TN(rs!RoutKey)
      
      'MG 160107 aggiunto campo LabelOn
      rsOrc!LabelOn = rs!LabelOn
      'fine
      
      rsOrc.Update
      
      DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Add new column (" & rs!Nome & ")"
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    Else
      ERR.Raise 10000, , "Index column Already exists!!!"
    End If
    rsOrc.Close
    
    rs.MoveNext
    
    DoEvents
  Wend
  rs.Close
End Sub

Public Sub Add_IdxCol_DB2_To_ORACLE(cIdIndexORC As Long, cIndexDb2 As Long, cIdDBdb2 As Long, cIdTBora As Long)
   Dim rs0 As Recordset, rs1 As Recordset, rs2 As Recordset
   
   Dim wNomeCol As String, wNomeColFor As String
   Dim wTabellaFor As String
   
   Dim wIndexCol As Long, wIndexTab As Long
   Dim wIndexColFor As Long, wIndexTabFor As Long
   Dim wNomeTab As String
   'VC: mi serve per separare le tabelle doppie
   Dim wCreator As String
  
  On Error GoTo catch
  
  Set rs0 = m_fun.Open_Recordset("Select * From DMDB2_IdxCol Where IdIndex = " & cIndexDb2)
  While Not rs0.EOF
    'Recupera i nomi colonna, colonna foreign e table foreign
    TipoDB = "DB2"
    
    wNomeCol = Trim(get_Nome_Colonna_By_Id(rs0!IdColonna))
    wNomeTab = Trim(get_NomeTable_ById(TN(rs0!IdTable, "LONG")))
    wNomeColFor = Trim(get_Nome_Colonna_By_Id(TN(rs0!forIdColonna, "LONG")))
    wTabellaFor = Trim(get_NomeTable_ById(TN(rs0!foridtable, "LONG")))
    'VC:
    wCreator = Trim(Get_Creator_Table(TN(rs0!IdTable, "LONG")))
           
    wIndexColFor = -1
    wIndexTabFor = -1
           
    TipoDB = ""
    
    'Cerca queste colonne e tabelle nelle nuove tabelle Oracle per prendere i loro id
   
    'IDTABLE
    'Set rs1 = m_fun.Open_Recordset("Select * From DMORC_Tabelle Where Nome = '" & wNomeTab & "' And IdDBOr = " & cIdDBdb2)
    'VC:
'    Set rs1 = m_fun.Open_Recordset("Select * From DMORC_Tabelle Where IdTable =" & cIdTBora) 'Nome = '" & wNomeTab & "' And IdDBOr = " & cIdDBdb2 & " And Creator = '" & wCreator & "'")
'    If rs1.RecordCount > 0 Then
       wIndexTab = cIdTBora 'rs1!IdTable
'    Else
'       wIndexTab = -1
'    End If
'    rs1.Close

    'IDCOLONNA
    'VC:
    'Set rs1 = m_fun.Open_Recordset("Select * From DMORC_Colonne Where Nome = '" & wNomeCol & "'") And IdTable = " & wIndexTab & " And IdDBOr = " & cIdDBdb2)"
    Set rs1 = m_fun.Open_Recordset("Select * From DMORC_Colonne Where Nome = '" & wNomeCol & "'" & "  And IdTable = " & wIndexTab)  '& " And IdDBOr = " & cIdDBdb2)
    If rs1.RecordCount > 0 Then
       wIndexCol = rs1!IdColonna
    Else
       wIndexCol = -1
    End If
    rs1.Close
    
    'IDCOLONNA FOREIGN
    If wNomeColFor <> "" Then
       Set rs1 = m_fun.Open_Recordset("Select * From DMORC_Colonne Where Nome = '" & wNomeColFor & "'") ' And IdDBOr = " & rs0!ForIdTable) ' & " And IdDBOr = " & cIdDBdb2)
    
       If rs1.RecordCount > 0 Then
          wIndexColFor = rs1!IdColonna
       Else
          wIndexColFor = -1
          
          DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Foreign Column index [" & wNomeColFor & "] LINKING NOT FOUND..."
          DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
       End If
       
       rs1.Close
    End If
    
    'IDTABLE FOREIGN
    If wTabellaFor <> "" Then
       Set rs1 = m_fun.Open_Recordset("Select * From DMORC_Tabelle Where Nome = '" & wTabellaFor & "'") ' And IdDBOr = " & cIdDBdb2)
    
       If rs1.RecordCount > 0 Then
          wIndexTabFor = rs1!IdTable
       Else
          wIndexTabFor = -1
          
          DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Foreign Table index [" & wNomeColFor & "] LINKING NOT FOUND..."
          DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
       End If
       
       rs1.Close
    End If
    
    'Aggiunge le colonne dell'indice
    Set rs2 = m_fun.Open_Recordset("Select * From DMORC_IdxCol Where IdIndex = " & cIdIndexORC & " And Ordinale = " & rs0!Ordinale) ' & " And Idtable = " & cIdTBora) 'VC: inserito IdTable
    
    If rs2.RecordCount = 0 Then
       rs2.AddNew
       
       rs2!IdIndex = cIdIndexORC
       rs2!Ordinale = rs0!Ordinale
       rs2!iddbor = cIndexDb2 'cIdDBdb2
       rs2!IdColonna = wIndexCol
       rs2!IdTable = wIndexTab
       rs2!Order = rs0!Order
       rs2!foridtable = wIndexTabFor
       rs2!forIdColonna = wIndexColFor
       rs2!Tag = "AUTOMATIC_DB2ORC"
       
       rs2.Update
       
       DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Add Column index [" & wNomeCol & "]"
       DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    End If
    rs2.Close
    
    rs0.MoveNext
    
    DoEvents
  Wend
  rs0.Close
  Exit Sub
catch:
  m_fun.WriteLog "#Error Creating Index " & cIndexDb2 & " on Table: " & rs0!IdTable
  'SQ Mettere l'icona 8!
End Sub

Public Sub Migra_TableSpaces_DB2_ORACLE(cNewIdDb As Long, cNewNomeDb As String, cIdDb2 As Long)
  Dim rs As Recordset, rsOrc As Recordset
  Dim MaxId As Long
  Dim rsMax As Recordset
  
  Set rs = m_fun.Open_Recordset("Select * From DMDB2_TableSpaces Where IdDb = " & cIdDb2)
  While Not rs.EOF
    DataMan.DmConnection.Execute "DELETE * From DMORC_Tablespaces Where Tag = 'AUTOMATIC_DB2ORC' AND IdDbOr = " & rs!IdTableSpc
    
    Screen.MousePointer = vbHourglass
    'Recupera il max
    Set rsMax = m_fun.Open_Recordset("Select MAX(IdTableSpc) From DMORC_TableSpaces")
    If rsMax.RecordCount > 0 Then
      MaxId = TN(rsMax.fields(0).Value, "LONG") + 1
    Else
      MaxId = 0
    End If
    rsMax.Close
    
    Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_TableSpaces Where " & _
                                     "IdDBOr = " & rs!IdTableSpc & " And IdDb = " & cNewIdDb & " And Nome = '" & rs!Nome & "'")
    If rsOrc.RecordCount = 0 Then
      rsOrc.AddNew
      
      rsOrc!IdTableSpc = MaxId
      rsOrc!idDB = cNewIdDb
      rsOrc!iddbor = rs!IdTableSpc 'cIdDb2
      rsOrc!Nome = rs!Nome
      rsOrc!ExtentSize = TN(rs!ExtentSize, "LONG")
      rsOrc!PreFetchSize = TN(rs!PreFetchSize, "LONG")
      rsOrc!NomeDB = cNewNomeDb
      rsOrc!BufferPool = rs!BufferPool
      rsOrc!LockSize = TN(rs!LockSize)
      rsOrc!Close = TN(rs!Close, "BOOLEAN")
      rsOrc!DSetPass = TN(rs!DSetPass)
      rsOrc!Compress = TN(rs!Compress, "BOOLEAN")
      rsOrc!StorageGroup = TN(rs!StorageGroup)
      rsOrc!IdObjSource = TN(rs!IdObjSource, "LONG")
      rsOrc!Dimensionamento = TN(rs!Dimensionamento, "LONG")
      rsOrc!Descrizione = TN(rs!Descrizione)
      rsOrc!Data_Creazione = Now
      rsOrc!Tag = "AUTOMATIC_DB2ORC"
      
      rsOrc.Update
      
      DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Add a new Oracle TableSpace [" & rsOrc!Nome & "]"
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    End If
    rsOrc.Close
    
    rs.MoveNext
    
    DoEvents
  Wend
  rs.Close
End Sub

Public Sub Migra_Tabelle_DB2_ORACLE(cNewIdDb As Long, cNomeNewDB As String, cIdDb2 As Long)
  Dim rs As Recordset, rsOrc As Recordset
  Dim retCodeFK As String
  
  Set rs = m_fun.Open_Recordset("Select * From DMDB2_Tabelle Where IdDb = " & cIdDb2)
  While Not rs.EOF
    Screen.MousePointer = vbHourglass
    
    'elimina le tabelle create in precedenza
    DataMan.DmConnection.Execute "DELETE * From DMORC_Tabelle Where Tag = 'AUTOMATIC_DB2ORC' AND IdDbOr = " & rs!IdTable
    
    'VC (SQ): per la gestione di pi� tabelle con creator diversi
    'Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_Tabelle Where IdDBOr = " & cIdDb2 & " And Nome = '" & rs!Nome & "'")
    Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_Tabelle Where " & _
                                     "IdDBOr = " & rs!IdTable & " And Nome = '" & rs!Nome & "'" & " And Creator = '" & rs!Creator & "'")
    If rsOrc.RecordCount = 0 Then
      rsOrc.AddNew
      
      rsOrc!Nome = rs!Nome
      rsOrc!idDB = cNewIdDb
      rsOrc!iddbor = rs!IdTable 'cIdDb2
      rsOrc!IdTableSpc = Associa_TableSpace(rs!IdTable)
      rsOrc!Creator = rs!Creator
      rsOrc!Dimensionamento = TN(rs!Dimensionamento, "LONG")
      rsOrc!NomeDB = cNomeNewDB
      rsOrc!IdOrigine = rs!IdTable
      rsOrc!Descrizione = "Automatic migration from DB2 to ORACLE"
      'MG 160107 inserito label
      rsOrc!LabelOn = rs!LabelOn
      ''''''''''''''''''''
      ' Gran casino:
      ' il parser mette il "COMMENT" dentro Descrizione
      ' e qui (il convertitore) viene perso...
      ' LABEL, invece, � gestito; ora faccio una brutta promisquit�!
      ''
      ''SQ 21-02-08
      ''''''''''''''''''''
      If Len(Trim(rs!Descrizione)) Then
        'se ci sono entrambi? gestire decentemente
        If Len(Trim(rsOrc!LabelOn & "")) = 0 Then rsOrc!LabelOn = Trim(rs!Descrizione)
      End If
      
      rsOrc!Data_Creazione = Now
      rsOrc!Tag = "AUTOMATIC_DB2ORC"
      rsOrc.Update
      
      'Migra gli alias (per tabella)
      Migra_Alias_DB2_ORACLE rsOrc!IdTable, cIdDb2, rs!IdTable
      'Migra le colonne
      Migra_Colonne_DB2_To_ORACLE rsOrc!IdTable, rs!IdTable, cIdDb2
      'Migra gli indici
      Migra_Indici_DB2_To_ORACLE rsOrc!IdTable, rs!IdTable, cIdDb2
      'MG 160107 migra le check
      Migra_Check_DB2_To_ORACLE rsOrc!IdTable, rs!IdTable, cIdDb2
    Else
      '?!?!?!
      ERR.Raise 10000, , "A table already exists !!!! IMPOSSIBLE!!!!"
    End If
    
    rs.MoveNext
    
    DoEvents
    Screen.MousePointer = vbDefault
  Wend
  rs.Close
End Sub

Public Sub Migra_Alias_DB2_ORACLE(cNewIdTable As Long, cIdDb2 As Long, cIdTableDb2 As Long)
  Dim rs As Recordset, rsOrc As Recordset
   
   ''''Set rs = m_fun.Open_Recordset("Select * From DMDB2_Alias Where IdTable = " & cIdTableDb2)
   Set rs = m_fun.Open_Recordset("Select * From DMDB2_Tabelle Where IdTable = " & cIdTableDb2)
  While Not rs.EOF
    Screen.MousePointer = vbHourglass
    
    Set rsOrc = m_fun.Open_Recordset("Select * From DMORC_Alias Where IdDBOr = " & cIdDb2 & " And IdTable = " & cNewIdTable)
    If rsOrc.RecordCount = 0 Then
      rsOrc.AddNew
      
      rsOrc!Nome = Left(rs!Nome, 7)
      rsOrc!iddbor = cIdDb2
      rsOrc!IdTable = cNewIdTable
      rsOrc!Creator = rs!Creator
      rsOrc!Data_Creazione = Now
      rsOrc!Tag = "AUTOMATIC_DB2ORC"
      
      rsOrc.Update
      
      DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Add a new Oracle Alias [" & rsOrc!Nome & "]"
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    End If
    rsOrc.Close
    
    rs.MoveNext
    
    DoEvents
  Wend
  rs.Close
End Sub

'VC (SQ): modificata tutta la gestione degli IdxCol
Public Sub Migra_IdxCol_DB2_To_ORACLE(cIdDBdb2 As Long)
  Dim rsTabDB2 As Recordset, rsIndexDB2 As Recordset, rsIndexOrc As Recordset, rsTabOrc As Recordset
  Dim ix As Integer
   
  'ricava tutti gli id delle tabelle db2
  Set rsTabDB2 = m_fun.Open_Recordset("Select * From DMDB2_Tabelle Where IdDb = " & cIdDBdb2)
  While Not rsTabDB2.EOF
    'x ogni singolo id tabella ricava tutti gli id db2
    Set rsIndexDB2 = m_fun.Open_Recordset("Select * From DMDB2_Index Where IdTable = " & rsTabDB2!IdTable & " Order By IdIndex")
    While Not rsIndexDB2.EOF
      'ciclo e ricavo:
      Set rsIndexOrc = m_fun.Open_Recordset("Select * from DMDB2_IdxCol where IdIndex = " & rsIndexDB2!IdIndex)
      'If rsIndexDB2!IdIndex = 873 Then Stop
      While Not rsIndexOrc.EOF
        InsertIdxCol rsIndexOrc, ix + 1
        rsIndexOrc.MoveNext
      Wend
      rsIndexOrc.Close
      rsIndexDB2.MoveNext
    Wend
    rsIndexDB2.Close
    rsTabDB2.MoveNext
  Wend
  rsTabDB2.Close
End Sub
            
Sub InsertIdxCol(rsDB2 As Recordset, Ordinale As Integer)
  Dim rsTabInOrc As Recordset, rsColInOrc As Recordset, rsTabForOrc As Recordset
  Dim rsColForOrc As Recordset, rsIdxCol As Recordset, rsIndexOrc As Recordset
  Dim IdTable As Long, IdColonna As Long
  Dim idforTable As Long, idforColonna As Long, IdIndex As Long
    
  On Error GoTo ErrHandler

  'idtable corretto
  Set rsTabInOrc = m_fun.Open_Recordset("Select * from DMORC_Tabelle where IdDbOr=" & rsDB2!IdTable)
  If rsTabInOrc.EOF Then
    IdTable = -1
  Else
    IdTable = rsTabInOrc!IdTable
  End If
    
  'idcol corretto
  Set rsColInOrc = m_fun.Open_Recordset("Select * from DMORC_Colonne where IdDbOr=" & rsDB2!IdColonna)
  If rsColInOrc.EOF Then
    IdColonna = -1
  Else
    IdColonna = rsColInOrc!IdColonna
  End If
  rsColInOrc.Close
    
  'idfortab corretto
  If rsDB2!foridtable > 0 Then
    Set rsTabForOrc = m_fun.Open_Recordset("Select * from DMORC_Tabelle where IdDbOr=" & rsDB2!foridtable)
    idforTable = rsTabForOrc!IdTable
  Else
    idforTable = -1
  End If
    
  'idcolfor corretto
  If rsDB2!forIdColonna > 0 Then
    Set rsColForOrc = m_fun.Open_Recordset("Select * from DMORC_Colonne where IdDbOr=" & rsDB2!forIdColonna)
    idforColonna = rsColForOrc!IdColonna
  Else
    idforColonna = -1
  End If
    
  'INDICE:
  Set rsIndexOrc = m_fun.Open_Recordset("Select * from DMORC_Index where IdDbOr=" & rsDB2!IdIndex)
  'SQ se non c'�?!
  'Pezza 22-02-08 (capire il motivo)
  If rsIndexOrc.RecordCount Then
    IdIndex = rsIndexOrc!IdIndex
    
    'SQ che controllo �?!
    Set rsIdxCol = m_fun.Open_Recordset("Select * from DMORC_IdxCol where IdDbOr=" & rsDB2!IdIndex & " and Ordinale=" & rsDB2!Ordinale)
    'Set rsIdxCol = m_fun.Open_Recordset("Select * from DMORC_IdxCol where IdDbOr=" & rsDB2!IdIndex & " and Ordinale=" & Ordinale)
    'SQ cosi' funziona sicuro!... � stato anche testato a sufficienza
    'If rsIndexOrc.RecordCount = 0 Then
    If rsIdxCol.RecordCount = 0 Then
      rsIdxCol.AddNew
  
      rsIdxCol!IdIndex = IdIndex
      rsIdxCol!Ordinale = rsDB2!Ordinale
      rsIdxCol!iddbor = rsDB2!IdIndex
      rsIdxCol!IdColonna = IdColonna
      rsIdxCol!IdTable = IdTable
      rsIdxCol!Order = rsDB2!Order
      rsIdxCol!foridtable = idforTable
      rsIdxCol!forIdColonna = idforColonna
      rsIdxCol!Tag = "AUTOMATIC_DB2ORC"
  
      rsIdxCol.Update
    End If
    rsIdxCol.Close
  Else
    DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Error: No ORACLE index found for table: " & rsTabInOrc!Nome
    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
  End If
  rsIndexOrc.Close
  rsTabInOrc.Close
    
  Exit Sub
ErrHandler:
  MsgBox "Error: " & ERR.Description
  'Resume Next
End Sub

Public Sub Add_Crossing_DB2ORACLE_In_Db(Id_Db2 As Long, NomeDB2 As String, NomeOracle As String)
  Dim rs As Recordset
  
  'Carica l'alias della tabella corrente se gi� esiste
  Set rs = m_fun.Open_Recordset("Select * From DM_DB2ORC Where IdDb_DB2 = " & Id_Db2)
  If rs.RecordCount > 0 Then
    'Edit
    rs!Nome_DB2 = NomeDB2
    rs!Nome_Oracle = NomeOracle
    rs!Migrato = False
    
    DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Update an existing name [" & NomeDB2 & " --> " & NomeOracle & "]"
    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
  Else
    'New
    rs.AddNew
    
    rs!IdDb_DB2 = Id_Db2
    rs!Nome_DB2 = NomeDB2
    rs!Nome_Oracle = NomeOracle
    rs!Migrato = False
    
    DataMan.DmFinestra.ListItems.Add , , Now & " : [DB2 --> ORACLE] Add a new name [" & NomeDB2 & " --> " & NomeOracle & "]"
    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
  End If
  rs.Update
  rs.Close
End Sub
