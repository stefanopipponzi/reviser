VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadmdF_VSAM2RDBMS 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "VSAM to RDBMS Conversion"
   ClientHeight    =   6660
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   8190
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6660
   ScaleWidth      =   8190
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdConvert 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   0
      Picture         =   "MadmdF_VSAM2RDBMS.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   17
      Tag             =   "fixed"
      ToolTipText     =   "Convert"
      Top             =   0
      Width           =   550
   End
   Begin VB.CommandButton CmdErase 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   555
      Picture         =   "MadmdF_VSAM2RDBMS.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   16
      Tag             =   "fixed"
      ToolTipText     =   "Erase"
      Top             =   0
      Width           =   550
   End
   Begin VB.CommandButton CmdChangeAtt 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   1095
      Picture         =   "MadmdF_VSAM2RDBMS.frx":074C
      Style           =   1  'Graphical
      TabIndex        =   15
      Tag             =   "fixed"
      ToolTipText     =   "Change Column Attributes"
      Top             =   0
      Width           =   550
   End
   Begin VB.CommandButton CmdExtraColums 
      Appearance      =   0  'Flat
      Height          =   555
      Left            =   1635
      Picture         =   "MadmdF_VSAM2RDBMS.frx":1416
      Style           =   1  'Graphical
      TabIndex        =   14
      Tag             =   "fixed"
      ToolTipText     =   "Extra Columns"
      Top             =   0
      Visible         =   0   'False
      Width           =   550
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selection"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   975
      Left            =   30
      TabIndex        =   11
      Top             =   630
      Width           =   3885
      Begin VB.OptionButton OptAll 
         Caption         =   "All VSAM Files"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   270
         Width           =   2655
      End
      Begin VB.OptionButton OptSel 
         Caption         =   "Only Selected VSAM File"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Value           =   -1  'True
         Width           =   2655
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Preferences"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1005
      Left            =   3990
      TabIndex        =   8
      Top             =   630
      Width           =   1695
      Begin VB.OptionButton optOracle 
         Caption         =   "Oracle"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   300
         Width           =   915
      End
      Begin VB.OptionButton optDB2 
         Caption         =   "DB2"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Value           =   -1  'True
         Width           =   675
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1005
      Left            =   5700
      TabIndex        =   4
      Top             =   630
      Width           =   2415
      Begin MSComctlLib.ProgressBar PBar 
         Height          =   195
         Left            =   90
         TabIndex        =   5
         Top             =   240
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBarK 
         Height          =   195
         Left            =   90
         TabIndex        =   6
         Top             =   450
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
      Begin MSComctlLib.ProgressBar PBar01 
         Height          =   195
         Left            =   90
         TabIndex        =   7
         Top             =   660
         Width           =   2205
         _ExtentX        =   3889
         _ExtentY        =   344
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "VSAM Selcted"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4935
      Left            =   30
      TabIndex        =   2
      Top             =   1620
      Width           =   3885
      Begin MSComctlLib.ListView lswDBSel 
         Height          =   4515
         Left            =   120
         TabIndex        =   3
         Top             =   270
         Width           =   3645
         _ExtentX        =   6429
         _ExtentY        =   7964
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "VSAM"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Acronym"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   2
            Text            =   "Associated Area"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Log"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   4935
      Left            =   3990
      TabIndex        =   0
      Top             =   1620
      Width           =   4125
      Begin MSComctlLib.ListView LswLog 
         Height          =   4515
         Left            =   150
         TabIndex        =   1
         Top             =   270
         Width           =   3885
         _ExtentX        =   6853
         _ExtentY        =   7964
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   12582912
         BackColor       =   -2147483624
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Log"
            Object.Width           =   26458
         EndProperty
      End
   End
End
Attribute VB_Name = "MadmdF_VSAM2RDBMS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim IdTbinMigr As Long
Dim VSAMinMigr As String
'SQ
Public DEFAULT_NULL As Boolean
Public DEFAULT_DEFAULT As Boolean
Public DEFAULT_CREATOR As String
'SQ
Dim headerColumns As Integer   'solo il numero... diventera' tutta la struttura (recordset)
Dim occursFields() As String
Dim Adesso As String

Private Sub CmdChangeAtt_Click()
  Dim r As Recordset, rsDb2Name As Recordset, rsCobolName As Recordset
  Dim nfile As Integer, ErrFile As Integer
  Dim RigaTxt As String, RecordTxt() As String
  Dim CmpTrovato As Boolean, ColTrovata As Boolean
        
  If MsgBox("Column Exceptions managing: continue?" & vbCrLf & _
            "Input file: 'ColumnExceptions.txt' in 'input-prj' directory.", vbYesNo + vbQuestion, "i-4.Migration - Column Exceptions") = vbNo Then
    Exit Sub
  End If
  
  Screen.MousePointer = vbHourglass
  LswLog.ListItems.Clear
  PBar.MAX = 10
  PBar.Value = 0
  PBarK.MAX = 10
  PBarK.Value = 0
  DoEvents
  
  m_fun.FnProcessRunning = True
  nfile = FreeFile
  Open m_fun.FnPathDB & "\Input-Prj\ColumnExceptions.txt" For Binary As nfile
  ErrFile = FreeFile
  Open m_fun.FnPathDB & "\Input-Prj\ColumnExceptions.log" For Output As ErrFile
  
  Do While Loc(nfile) < FileLen(m_fun.FnPathDB & "\Input-Prj\ColumnExceptions.txt")
    Line Input #nfile, RigaTxt
    If Trim(RigaTxt) = "" Then Exit Do
    ' silvia: fare il ciclo di replace per toglierte gli spazi
    RigaTxt = Replace(RigaTxt, "  ", " ", 1)
    RecordTxt = Split(RigaTxt, " ")
    CmpTrovato = False
    'SILVIA 23-11-2007
    If Left(RigaTxt, 2) <> "##" Then
      Set r = m_fun.Open_Recordset("SELECT IdOggetto from BS_Oggetti where Nome = '" & RecordTxt(0) & "'")
      Do Until r.EOF
        Set rsCobolName = m_fun.Open_Recordset("select * from DMVSM_Data_Cmp where " & _
                                               "Nome = '" & RecordTxt(1) & "' AND IdOggetto = " & r!IdOggetto)
        If rsCobolName.RecordCount Then
          CmpTrovato = True
          ColTrovata = False
          Set rsDb2Name = m_fun.Open_Recordset("select * from DMDB2_Colonne where " & _
                                               "IdCmp = " & rsCobolName!IdCmp)
          'ATTENZIONE, CE NE DOVREBBE ESSERE SOLO UNO!
          Do Until rsDb2Name.EOF
            ColTrovata = True
            If RecordTxt(2) <> "@" Then
              rsDb2Name!Nome = RecordTxt(2)
            End If
            If RecordTxt(3) <> "@" Then
              rsDb2Name!Tipo = RecordTxt(3)
            End If
            If RecordTxt(4) <> "@" Then
              rsDb2Name!lunghezza = RecordTxt(4)
            End If
            If RecordTxt(5) <> "@" Then
              rsDb2Name!Decimali = RecordTxt(5)
            End If
            If RecordTxt(6) <> "@" Then
              rsDb2Name!RoutLoad = RecordTxt(6)
            End If
            If RecordTxt(7) <> "@" Then
              rsDb2Name!RoutWrite = RecordTxt(7)
            End If
            If RecordTxt(8) <> "@" Then
              rsDb2Name!RoutRead = RecordTxt(8)
            End If
            If RecordTxt(9) <> "@" Then
              rsDb2Name!RoutKey = RecordTxt(9)
            End If
            rsDb2Name.Update
            Print #ErrFile, " MODIFICATO: IDCOLONNA: " & rsDb2Name!IdColonna & "; NOME CAMPO: " & rsDb2Name!Nome
            rsDb2Name.MoveNext
          Loop
          rsDb2Name.Close
        End If
        rsCobolName.Close
        r.MoveNext
      Loop
      r.Close
      If Not CmpTrovato Then
        Print #ErrFile, "NO CAMPI - " & RigaTxt
      Else
        If Not ColTrovata Then
          Print #ErrFile, "NO COLONNA - " & RigaTxt
        End If
      End If
    End If
  Loop
  Close nfile
  Close ErrFile
  
  m_fun.FnProcessRunning = False
  Screen.MousePointer = vbDefault
  MsgBox "Process terminated." & vbCrLf & _
         "Log file: 'ColumnExceptions.log' in 'input-prj' directory.", vbInformation, "i-4.Migration - Column Exceptions"
End Sub

Private Sub CmdConvert_Click()
  Dim i As Long
    
  Screen.MousePointer = vbHourglass
  LswLog.ListItems.Clear
  PBar.MAX = 10
  PBar.Value = 0
  PBarK.MAX = 10
  PBarK.Value = 0
  DoEvents
  
  m_fun.FnProcessRunning = True
  
  'Carica per ogni VSAM una tabella DB2/ORACLE in memoria
  LswLog.ListItems.Add , , Time & " - Starting Analysis..."
  MadmdF_VSAM2RDBMS.LswLog.Refresh
  
  LswLog.ListItems.Add , , Time & " - Starting Data Migration..."
  
  ' Mauro 07/09/2007 : Aggiunto per un'unificazione futura delle routine DB2 e ORACLE
  If optDB2.Value Then
    TipoDB = "DB2"
    PrefDb = "DMDB2"
  ElseIf optOracle.Value Then
    TipoDB = "ORACLE"
    PrefDb = "DMORC"
  End If
  
  PBar01.MAX = lswDBSel.ListItems.Count
  For i = 1 To lswDBSel.ListItems.Count
    'VC: coerente con la modifica fatta per i tipi DBD
    'If lswDBSel.ListItems(i).Selected And Is_Hidam(UCase(Trim(lswDBSel.ListItems(i).Text))) Then
    If OptAll.Value Or lswDBSel.ListItems(i).Selected Then
      LswLog.ListItems.Add , , Time & " - Porting VSAM: " & lswDBSel.ListItems(i).text
      MadmdF_VSAM2RDBMS.LswLog.Refresh
      
      If lswDBSel.ListItems(i).SubItems(1) = "" Then
        LswLog.ListItems.Add , , Time & " - No Acronym value for VSAM " & lswDBSel.ListItems(i).text
        LswLog.ListItems.Add , , Time & " - Porting VSAM " & lswDBSel.ListItems(i).text & " not success!"
        MadmdF_VSAM2RDBMS.LswLog.Refresh
      Else
        If lswDBSel.ListItems(i).SubItems(2) = "X" Then
          ' Mauro 01/08/2007 : Unificata la routine DB2/ORACLE
          Translate_VSAM_2_RDBMS lswDBSel.ListItems(i).text, lswDBSel.ListItems(i).SubItems(1)
          Add_VSAM_Key lswDBSel.ListItems(i).text
        Else
          LswLog.ListItems.Add , , Time & " - Porting VSAM " & lswDBSel.ListItems(i).text & " not success!"
          MadmdF_VSAM2RDBMS.LswLog.Refresh
        End If
      End If
    End If
    PBar01.Value = PBar01.Value + 1
  Next i
  
  PBar01.Value = PBar01.MAX

  If optDB2.Value Then
    MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Data Migration from VSAM to DB2 Finished"
  ElseIf optOracle.Value Then
    MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Data Migration from VSAM to ORACLE Finished"
  End If
  
  MadmdF_VSAM2RDBMS.LswLog.Refresh
  LswLog.ListItems.Add , , ""
  MadmdF_VSAM2RDBMS.LswLog.Refresh
  MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
  WaitTime 1
  
  PBar.Value = 0
  PBarK.Value = 0
  PBar01.Value = 0
  
  CLsOn False
  
  m_fun.FnProcessRunning = False
  Screen.MousePointer = vbDefault
End Sub

Public Sub Add_VSAM_Key(nomeVSAM As String)
  Dim rsRT As Recordset, rsTable As Recordset, rsVSAM As Recordset, rsKeys As Recordset, rsIndex As Recordset
  Dim r As Recordset
  Dim IdIndex As Long
  Dim env As Collection
  Dim PKName As String
  
  Set rsRT = m_fun.Open_Recordset("select * from DMVSM_RecordType where vsam = '" & nomeVSAM & "'")
  Do Until rsRT.EOF
    Set rsTable = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "IdOrigine = " & rsRT!IdRecordType)
    Do Until rsTable.EOF
      If Not rsTable!Tag Like "*OCCURS" Then
        Set rsVSAM = m_fun.Open_Recordset("select * from DMVSM_archivio where vsam = '" & rsRT!VSAM & "'")
        If rsVSAM.RecordCount Then
          Set rsKeys = m_fun.Open_Recordset("select * from DMVSM_chiavi where IdArchivio = " & rsVSAM!idArchivio)
          Do Until rsKeys.EOF
            Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_Index where IdTable = " & rsTable!IdTable)
            Set env = New Collection
            env.Add rsRT!Acronimo
            env.Add rsKeys!Nome
            PKName = getEnvironmentParam(VSAM_PKName, "chiaviVSM", env, TipoDB)
            rsIndex.AddNew
            If rsKeys!Primaria Then
              rsIndex!Tipo = "P"
            Else
              rsIndex!Tipo = "I"
            End If
            rsIndex!Unique = True
            IdIndex = getId("IdIndex", PrefDb & "_Index")
            rsIndex!IdIndex = IdIndex
            rsIndex!IdTable = rsTable!IdTable
            rsIndex!IdTableSpc = rsTable!IdTableSpc
            rsIndex!IdOrigine = rsKeys!IdKey
            rsIndex!IdOrigineParent = rsRT!IdRecordType
            rsIndex!Used = True
            rsIndex!Nome = PKName
            rsIndex!Tag = rsKeys!Tag & ""
            rsIndex!Data_Creazione = rsKeys!Data_Creazione & ""
            If IsNull(rsKeys!Data_Modifica) Then
              rsIndex!Data_Modifica = rsIndex!Data_Creazione
            Else
              rsIndex!Data_Modifica = rsKeys!Data_Modifica
            End If
            rsIndex.Update
            rsIndex.Close
            caricaColonne_VSAM rsVSAM!VSAM, rsRT!idArea, rsRT!IdOggetto, rsKeys!keyStart, rsKeys!keyLen, IdIndex, rsTable!IdTable
              
            rsKeys.MoveNext
          Loop
          rsKeys.Close
          
          '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          ' GESTIONE SOTTOTABELLE OCCURS:
          '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
          Set r = m_fun.Open_Recordset("Select idTable,idTableJoin From " & PrefDb & "_Tabelle Where " & _
                                       "IdTableJoin = " & rsTable!IdTable & " AND TAG = 'AUTOMATIC-OCCURS'")
          If r.RecordCount Then
            createOccursIndex r
          End If
          r.Close
        End If
        rsVSAM.Close
      End If
      rsTable.MoveNext
    Loop
    rsTable.Close
    rsRT.MoveNext
  Loop
  rsRT.Close
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' GESTIONE LOOKUP TABLE:
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Set r = m_fun.Open_Recordset("Select idTable, idTableJoin From " & PrefDb & "_Tabelle Where " & _
                               "IdTableJoin = 0")
  Do Until r.EOF
    createLookUpIndex r!IdTable
    r.MoveNext
  Loop
  r.Close
End Sub

Public Sub createLookUpIndex(IdTable As Long)
  Dim env As Collection, PKName As String
  Dim rs As Recordset, rsColonne As Recordset, rsIndex As Recordset
  Dim idColonnaInit As Long
  Dim ordinaleInit As Long
  Dim rsChiavi As Recordset, rsIdxCol As Recordset
  
  VSAM_PKName = LeggiParam("VSAM-PK-NAME")
  Set env = New Collection
  Set rs = m_fun.Open_Recordset("select nome from " & PrefDb & "_tabelle where " & _
                                "idtable = " & IdTable)
  env.Add rs!Nome
  env.Add ""
  PKName = getEnvironmentParam(VSAM_PKName, "chiaviVSM", env, TipoDB)
  
  rs.Close
  'Scrive le colonne-indici nel DB
  
  'IdColonna iniziale
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne order by idColonna")
  If rs.RecordCount Then
    rs.MoveLast
    idColonnaInit = rs!IdColonna
  Else
    idColonnaInit = 0
  End If
  rs.Close
  
  'Mauro 20/09/2007
  'Ordinale Colonna iniziale
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where " & _
                                "idtable = " & IdTable & " order by idColonna")
  If rs.RecordCount Then
    rs.MoveLast
    ordinaleInit = rs!Ordinale
  Else
    ordinaleInit = 0
  End If
  
  Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where idtable = " & IdTable)
  If rsIndex.RecordCount Then
    rsIndex.Delete
  End If
  rsIndex.AddNew
  
  rsIndex!IdIndex = getId("IdIndex", PrefDb & "_Index")
  rsIndex!IdTable = IdTable
  rsIndex!IdTableSpc = -1
  rsIndex!Nome = PKName
  rsIndex!Tipo = "P"
  rsIndex!IdOrigine = 0
  rsIndex!IdOrigineParent = 0
  rsIndex!Used = -1
  rsIndex!Unique = -1
  rsIndex!Descrizione = ""
  rsIndex!ForeignIdTable = 0
  rsIndex!ForOnDelete = 0
  rsIndex!ForOnCascade = 0
  rsIndex!ForOnRestrict = 0
  rsIndex!ForOnSetNull = 0
  rsIndex!ForOnNoAction = 0
  rsIndex!Data_Creazione = Adesso
  rsIndex!Data_Modifica = Adesso
  rsIndex!Tag = "AUTOMATIC-LOOKUP"
  rsIndex.Update
          
  Set rsChiavi = m_fun.Open_Recordset( _
        "select c.keylen from " & PrefDb & "_DB as a, DMVSM_NomiVSAM as b, DMVSM_Archivio as c, " & PrefDb & "_tabelle as d where " & _
        "a.iddb = d.iddb and a.nome = b.acronimo and b.nomeVSAM = c.VSAM and d.idtable = " & IdTable)
  Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where idtable = " & IdTable)
  If rsColonne.RecordCount Then
    rsColonne.Delete
  End If
  rsColonne.AddNew
  rsColonne!IdColonna = getId("IdColonna", PrefDb & "_Colonne")
  rsColonne!IdTable = IdTable
  rsColonne!Ordinale = 1
  rsColonne!idTableJoin = IdTable
  rsColonne!Nome = "PKEY"
  rsColonne!Tipo = "CHAR"
  rsColonne!lunghezza = rsChiavi!keyLen
  rsColonne!Decimali = 0
  rsColonne!Formato = ""
  rsColonne!Etichetta = ""
  rsColonne!Null = 0
  rsColonne!Default = ""
  rsColonne!Commento = ""
  rsColonne!Descrizione = "LookUp Column"
  rsColonne!IdCmp = 0
  rsColonne!NomeCmp = ""
  rsColonne!Tag = "AUTOMATIC-LOOKUP"
  rsColonne!RoutLoad = "load_Field_LOOKUP"
  rsColonne!RoutRead = "rdbms2vsam_field_LOOKUP"
  rsColonne!RoutWrite = "vsam2rdbms_field_LOOKUP"
  rsColonne!RoutKey = "vsam2rdbms_KEY_field_LOOKUP"
  rsColonne!LabelOn = ""
  rsColonne!IsDefault = 0
  rsColonne!defaultValue = ""
  rsColonne!Posizione = 0
  rsColonne.Update
  
  Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_idxcol where idtable = " & IdTable)
  If rsIdxCol.RecordCount Then
    rsIdxCol.Delete
  End If
  rsIdxCol.AddNew
  
  rsIdxCol!IdIndex = rsIndex!IdIndex
  rsIdxCol!Ordinale = 1
  rsIdxCol!IdTable = IdTable
  rsIdxCol!IdColonna = rsColonne!IdColonna
  rsIdxCol!Order = "ASCENDING"
  rsIdxCol!foridtable = 0
  rsIdxCol!forIdColonna = 0
  rsIdxCol!Tag = "AUTOMATIC-LOOKUP"
  rsIdxCol.Update
  
  rsIdxCol.Close
  rsChiavi.Close
  rsColonne.Close
  rsIndex.Close
  
  'AGGIUNTA COLONNA "DATA_TABLE":
  rs.AddNew
  rs!IdColonna = getId("IdColonna", PrefDb & "_Colonne")
  rs!IdTable = IdTable
  rs!idTableJoin = IdTable
  rs!Nome = "DATA_TABLE"
  rs!Ordinale = 2
  rs!IdCmp = -1
  rs!Tipo = "CHAR"
  'SQ - sarebbe 128... fare variabile d'ambiente...
  'rs!lunghezza = 18
  rs!lunghezza = 5
  rs!Decimali = 0
  rs!Descrizione = "LookUp Column"
  rs!Null = False
  
  'MG 130207
  rs!IsDefault = DEFAULT_DEFAULT
  If DEFAULT_DEFAULT Then
    rs!defaultValue = ""
  End If
  
  rs!Tag = "AUTOMATIC-LOOKUP"
  rs!RoutLoad = "load_Table_LOOKUP"
  rs!RoutRead = "rdbms2vsam_Table_LOOKUP"
  rs!RoutWrite = "vsam2rdbms_Table_LOOKUP"
  rs!RoutKey = "vsam2rdbms_KEY_Table_LOOKUP"
  rs.Update
  
  rs.Close
End Sub

Public Sub caricaColonne_VSAM(nomeVSAM As String, idArea As Long, IdOggetto As Long, keyStart As Long, keyLen As Long, IdIndex As Long, IdTable As Long)
  Dim r As Recordset, rs As Recordset
  Dim z As Long, i As Long
  Dim Count As Long
  Dim StPos As Long
  Dim LenPos As Long
  Dim FineCmp As Long
  
  On Error GoTo ErrorHandler
  'Seleziono tutti campi di almeno un byte
  ReDim idxcol(0)
  Set r = m_fun.Open_Recordset("Select * From DMVSM_Data_Cmp Where " & _
                               "vsam = '" & nomeVSAM & "' and idarea = " & idArea & _
                               " and idoggetto = " & IdOggetto & " And Lunghezza + decimali > 0" & _
                               " and damigrare = -1 Order By Ordinale")
  Do Until r.EOF
    StPos = r!Posizione
    LenPos = r!Byte
    FineCmp = LenPos + StPos - 1
    If FineCmp <= keyLen + keyStart - 1 And StPos >= keyStart Then
      Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne Where IdCmp = " & r!IdCmp)
      If rs.RecordCount Then
        'Deve essere un campo solo
        ReDim Preserve idxcol(UBound(idxcol) + 1)
        idxcol(UBound(idxcol)).IdColonna = rs!IdColonna
        idxcol(UBound(idxcol)).IdIndex = IdIndex
        idxcol(UBound(idxcol)).IdTable = IdTable
        idxcol(UBound(idxcol)).Nome = rs!Nome
      End If
      rs.Close
    End If
    r.MoveNext
  Loop
  r.Close
  
  Count = 1
  For z = 1 To UBound(idxcol)
    Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol Where " & _
                                 "IdIndex = " & IdIndex & " And IdColonna = " & idxcol(z).IdColonna & _
                                 " And IdTable = " & IdTable)
    If r.RecordCount = 0 Then
      MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Index Column: " & Get_NomeColonna(idxcol(z).IdColonna)  '?
      MadmdF_VSAM2RDBMS.LswLog.Refresh
      MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
      
      r.AddNew
      r!IdIndex = IdIndex
      r!IdTable = IdTable
      r!IdColonna = idxcol(z).IdColonna
      r!Order = "ASCENDING"
      r!Tag = "AUTOMATIC-XD" 'SQ
      r!Ordinale = Count
      Count = Count + 1
      r.Update
    End If
    r.Close 'ALE
  Next z
  Exit Sub
ErrorHandler:
  MsgBox ERR.Number & " - " & ERR.Description
End Sub

Private Sub cmdErase_Click()
  LswLog.ListItems.Clear
  PBar.MAX = 10
  PBar.Value = 0
  PBarK.MAX = 10
  PBarK.Value = 0
  DoEvents
       
  m_fun.FnProcessRunning = True
  
  If MsgBox("Do you want to delete all migrated data?", vbExclamation + vbYesNo, "i-4.Migration") = vbYes Then
    If MsgBox("Click Yes if you want to continue...", vbExclamation + vbYesNoCancel, "i-4.Migration") = vbYes Then
      CLsOn True
      ' DB2
      LswLog.ListItems.Add , , Time & " - Deleting Old DB2 Data Migration..."
      MadmdF_VSAM2RDBMS.LswLog.Refresh
      DataMan.DmConnection.Execute "DELETE * From DMDB2_DB Where TAG = 'AUTOMATIC-VSAMDB2'"
      DataMan.DmConnection.Execute "DELETE * From DMDB2_TableSpaces Where TAG = 'AUTOMATIC-VSAMDB2'"
      DataMan.DmConnection.Execute "DELETE * From DMDB2_Tabelle Where TAG = 'AUTOMATIC-VSAMDB2'"
      DataMan.DmConnection.Execute "DELETE * From DMDB2_Colonne Where TAG = 'AUTOMATIC-VSAMDB2'"
      DataMan.DmConnection.Execute "DELETE * From DMDB2_Index Where TAG = 'AUTOMATIC-VSAMDB2' or TAG = 'AUTOMATIC-XD'"
      'SQ:
      DataMan.DmConnection.Execute "DELETE * From DMDB2_IdxCol Where TAG = 'AUTOMATIC-VSAMDB2' or TAG = 'AUTOMATIC-XD'"
      'gli alias?
      LswLog.ListItems.Add , , Time & " - Deleting Old DB2 Data Migration [SUCCESSFUL]"
      '''''''''''''''''''''''''''''''''''''''''''''''
      ' ORACLE
      MadmdF_VSAM2RDBMS.LswLog.Refresh
      
      LswLog.ListItems.Add , , Time & " - Deleting Old ORACLE data migration..."
      MadmdF_VSAM2RDBMS.LswLog.Refresh
      DataMan.DmConnection.Execute "DELETE * From DMORC_DB Where TAG = 'AUTOMATIC-VSAMORACLE'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_TableSpaces Where TAG = 'AUTOMATIC-VSAMORACLE'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_Tabelle Where TAG = 'AUTOMATIC-VSAMORACLE'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_Colonne Where TAG = 'AUTOMATIC-VSAMORACLE'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_Index Where TAG = 'AUTOMATIC-VSAMORACLE'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_IdxCol Where TAG = 'AUTOMATIC-VSAMORACLE'"
      DataMan.DmConnection.Execute "DELETE * From DMORC_Trigger Where TAG = 'AUTOMATIC-VSAMORACLE'"
      
      LswLog.ListItems.Add , , Time & " - Deleting Old ORACLE data migration [SUCCESSFUL]"
      '''''''''''''''''''''''''''''''''''''''''''''''
      MadmdF_VSAM2RDBMS.LswLog.Refresh
      CLsOn False
      'Cancella gli archivi VSAM
    End If
  End If
  m_fun.FnProcessRunning = False
End Sub

'� settato da carica_Default... spostarlo!
Private Sub Form_Load()
  'Setta il parent della form
  'SetParent Me.hwnd, DataMan.DmParent
  
  'ResizeControls Me, , True
  
  'SQ OptTBSSingle.Value = True
    
  lswDBSel.ColumnHeaders(1).Width = lswDBSel.Width / 7 * 3
  lswDBSel.ColumnHeaders(2).Width = lswDBSel.Width / 7 * 2
  lswDBSel.ColumnHeaders(3).Width = lswDBSel.Width / 7 * 2

  Load_Array_Char
  
  'Carica la lista completa dei VSAM
  Carica_Lista_VSAM
  
  'Carica i parametri
  DataMan.Carica_Parametri_DataManager
  
  'SQ
  Carica_Default
  
  '''''''''''''''''
  ' Tutti i "LeggiParam" necessari: letti una volta sola!
  ' Mauro: 14/09/2007
  '''''''''''''''''
  MadmdM_VSAM.getEnvironmentParameters
  
  m_fun.AddActiveWindows Me
  m_fun.FnActiveWindowsBool = True
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ
' Accede alle tabelle (FINIRE) di "properties" che definiscono
' alcuni parametri di configurazione iniziale:
' - DEFAULT_NULL
' - DEFAULT_DEFAULT
' - INTEGRITA REFERENZIALE
' - (COLONNE DI HEADER)
' - ...?
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub Carica_Default()
  '''''''''''''
  ' TMP:
  '''''''''''''
  'Select Case m_fun.FnNomeDB
  '  Case "banca.mty"
  '    DEFAULT_NULL = True
  '    DEFAULT_DEFAULT = False
  '  Case Else
  DEFAULT_NULL = False
  DEFAULT_DEFAULT = False
  'End Select
  DEFAULT_CREATOR = LeggiParam("DM_CREATOR")
  
  Dim r As Recordset
  Set r = m_fun.Open_Recordset("Select count(*) From DMheader_Columns")
  headerColumns = IIf(r.EOF, 0, r(0))
  r.Close
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Dim wScelta As Long
  
  If m_fun.FnProcessRunning Then
    wScelta = m_fun.Show_MsgBoxError("FB01I")
    
    If wScelta = vbNo Then
      Cancel = 0 'No unload
      m_fun.FnStopProcess = False
    Else
      Cancel = 1 'Unload
      m_fun.FnStopProcess = True
    End If
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
  lswDBSel.ColumnHeaders(1).Width = lswDBSel.Width / 7 * 3
  lswDBSel.ColumnHeaders(2).Width = lswDBSel.Width / 7 * 2
  lswDBSel.ColumnHeaders(3).Width = lswDBSel.Width / 7 * 2
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
  m_fun.FnActiveWindowsBool = False

  SetFocusWnd m_fun.FnParent
End Sub

' Mauro 07/09/2007 : Unificate le routine DB2-ORACLE
'Public Sub Translate_DLI_2_DB2(nomeDbd As String, idDbd As Long)
Public Sub Translate_VSAM_2_RDBMS(nomeVSAM As String, AcronimoVSAM As String)
  Dim rsVSAM As Recordset
  Dim dbName As String
  Dim env As Collection
  
  Adesso = Now
  
  LswLog.ListItems.Add , , Time & " - Deleting Old Data Migration... " & nomeVSAM
  MadmdF_VSAM2RDBMS.LswLog.Refresh
  
  Set rsVSAM = m_fun.Open_Recordset("select idarchivio from DMVSM_Archivio where vsam = '" & nomeVSAM & "'")
  Do Until rsVSAM.EOF
    clearTables rsVSAM!idArchivio
    rsVSAM.MoveNext
  Loop
  rsVSAM.Close
  
  If Len(dbName) Then
    Set env = New Collection  'resetto;
    env.Add "" ' Nome DB
    env.Add rsVSAM
    dbName = getEnvironmentParam(dbName, "dbVSM", env, TipoDB)
  End If
  If Len(dbName) = 0 Then
    'SQ tmp!
    'dbName = "DBVSAM"
    dbName = AcronimoVSAM
  End If
  dbName = Replace(dbName, "-", "_")
  
  MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Database: " & dbName
  MadmdF_VSAM2RDBMS.LswLog.Refresh
  MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
  Add_Database nomeVSAM, AcronimoVSAM, dbName
End Sub

Public Sub clearTables(idArchivio As Long)
  Dim wTb As Recordset
  Dim idDB As Long

  Set wTb = m_fun.Open_Recordset("select iddb, idtable from " & PrefDb & "_Tabelle where idobjsource = " & idArchivio)
  If wTb.RecordCount Then
    idDB = wTb!idDB
    Do Until wTb.EOF
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Alias Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Check_Constraint Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Colonne Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_IdxCol Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Index Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Trigger Where idtable = " & wTb!IdTable
      wTb.MoveNext
    Loop
  End If
  wTb.Close
  DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_tabelle Where idobjsource = " & idArchivio
  Set wTb = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & idDB)
  If wTb.RecordCount = 0 Then
    DataMan.DmConnection.Execute "DELETE * from " & PrefDb & "_DB where idDb = " & idDB
  End If
  wTb.Close
End Sub

' Mauro 07/09/2007 : Unificate le routine DB2-ORACLE
'Public Sub Add_Database_DB2(nomeVsam As String, IdDBDOr As Long)
Public Sub Add_Database(nomeVSAM As String, AcronimoVSAM As String, NomeDB As String)
  Dim r As Recordset
  Dim IdDatabase As Long
  Dim env As Collection
  Dim tNome As String
  
  VSAMinMigr = nomeVSAM
  Set r = m_fun.Open_Recordset("select idarchivio from DMVSM_Archivio where VSAM = '" & nomeVSAM & "'")
  If r.RecordCount Then
    IdTbinMigr = r!idArchivio
  Else
    IdTbinMigr = 0
  End If
  r.Close
  
  ''''''''''''''''''''''''''''''''''
  ' CONTROLLO DB:
  '...FARE...
  ''''''''''''''''''''''''''''''''''
  Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where Nome = '" & NomeDB & "'")
  If r.RecordCount = 0 Then
    r.AddNew
    r!iddbor = 0
    r!IdObjSource = 0
    r!Nome = NomeDB
    r!Data_Creazione = Now
    r!Data_Modifica = r!Data_Creazione
    r!Descrizione = "VSAM To " & TipoDB & " Database Automatic DATA MIGRATION..."
    r!Tag = "AUTOMATIC-VSAM" & TipoDB
    r.Update
  End If
  IdDatabase = r!idDB
  r.Close
  
  ''''''''''PBar.MAX = UBound(Tabelle)
  ''''''''''PBar.Value = 0
  '''''''''''''''''''''''''''''''''
  'AGGIUNTA TABELLE:
  '''''''''''''''''''''''''''''''''
  'Aggiunge Le tabelle
  MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding Tables..."
  MadmdF_VSAM2RDBMS.LswLog.Refresh
  MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
   
  ''''''''''''''''''
  'GESTIONE NOMI:
  ''''''''''''''''''
  'SQ - Fare parametro!!
  'tNome = Applica_PatternString(nomeVsam, pn_Tables)
  ' Mauro 10/09/2007 : Non creo pi� una tabella per ogni Archivio, ma per ogni RecordType
  '                    e se i RecordType sono pi� di uno, creo anche la tabella LookUp (Dispatcher)
  Set r = m_fun.Open_Recordset("select * from DMVSM_Recordtype where vsam = '" & nomeVSAM & "'")
  If r.RecordCount > 1 Then
    ' Creare la LookUp table
    Set env = New Collection  'resetto;
    env.Add AcronimoVSAM
    env.Add r!Acronimo
    env.Add r!Nome
    tNome = getEnvironmentParam(VSAM_LookUpName, "tabelleVSM", env, TipoDB)
    createLookUpTable IdDatabase, 0, tNome, NomeDB, -1
  End If
  
  Do Until r.EOF
    Set env = New Collection  'resetto;
    env.Add AcronimoVSAM
    env.Add r
    env.Add UCase(r!Nome)
    tNome = getEnvironmentParam(VSAM_TbName, "tabelleVSM", env, TipoDB)
    
    MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding Table: " & tNome
    MadmdF_VSAM2RDBMS.LswLog.Refresh
    MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
    Add_Table IdDatabase, r!IdRecordType, tNome, NomeDB, -1
    r.MoveNext
  Loop
  r.Close
  
  PBar.Value = 0
End Sub

' Mauro 07/09/2007 : Unificate le routine DB2-ORACLE
'Public Sub Add_Table_DB2(idDB As Long, IdOr As Long, NomeTb As String, NomeDB As String, IdTbs As Long)
Public Sub Add_Table(idDB As Long, IdOr As Long, NomeTb As String, NomeDB As String, IdTbs As Long)
  Dim rsTable As Recordset
  Dim IdTable As Long
  Dim i As Long
  Dim tableName As String
    
  GbNomeTbInAdd = NomeTb
  
  Set rsTable = m_fun.Open_Recordset("Select * From " & PrefDb & "_Tabelle Where " & _
                               "IdDB = " & idDB & " And IdOrigine = " & IdOr & " And Nome = '" & NomeTb & "' And " & _
                               "NomeDB ='" & NomeDB & "'")
  'Recupera il maxId
  If rsTable.RecordCount = 0 Then
    rsTable.AddNew
    
    rsTable!idDB = idDB
 '   rsTable!IdTable = IdTable
    rsTable!Nome = NomeTb
    rsTable!IdTableSpc = IdTbs
    rsTable!NomeDB = NomeDB
    rsTable!IdOrigine = IdOr ' IdRecordType
    rsTable!IdObjSource = IdTbinMigr ' IDArchivio del VSAM
    rsTable!Descrizione = "VSAM to " & TipoDB & " Automatic Migration"
    rsTable!Data_Creazione = Adesso
    rsTable!Data_Modifica = Adesso
    rsTable!Tag = "AUTOMATIC-VSAM" & TipoDB
    'SQ
    If Len(DEFAULT_CREATOR) Then rsTable!Creator = DEFAULT_CREATOR
    rsTable.Update
  End If
  rsTable.Close
  
  IdTable = getId("idtable", PrefDb & "_Tabelle")
  getOccursFields IdOr
  
  Add_Colonne IdTable, IdOr, NomeTb, NomeDB 'GLI ULTIMI DUE PARAMETRI MI POSSONO SERVIRE PER LE "EXTRA COLUMNS"...
  
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' GESTIONE OCCURS:
  ' - CREAZIONE SOTTOTABELLA OCCURS
  ' - TABELLA PRINCIPALE SENZA COLONNE OCCURSATE
  ' SQ - 21-06-06
  ''''''''''''''''''''''''''''''''''''''''''''''''''''
  'SQ
  Set rsTable = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle")
  For i = 1 To UBound(occursFields)
    'TMP: Fare parametro!
    tableName = NomeTb & "_" & Format(i, "00")   'TMP name: ma univoco!
    
    MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding 'OCCURS SubTable': " & tableName
    MadmdF_VSAM2RDBMS.LswLog.Refresh
    MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
    rsTable.AddNew
    rsTable!idDB = idDB
    rsTable!Nome = tableName
    rsTable!IdTableSpc = IdTbs
    rsTable!NomeDB = NomeDB
    rsTable!IdOrigine = IdOr
    rsTable!idTableJoin = IdTable
    '''''''''''''''''''''''''''''''''''''
    ' Origine occurs! campo provvisorio
    '''''''''''''''''''''''''''''''''''''
    rsTable!Dimensionamento = i
    rsTable!IdObjSource = IdTbinMigr
    rsTable!Descrizione = "VSAM to " & TipoDB & " Automatic Migration"
    rsTable!Data_Creazione = Adesso
    rsTable!Data_Modifica = Adesso
    rsTable!Tag = "AUTOMATIC-OCCURS"
    If Len(DEFAULT_CREATOR) Then rsTable!Creator = DEFAULT_CREATOR
    'IdTable = rsTable!IdTable
    rsTable.Update
    
    addOccursColumns rsTable!IdTable, IdTable, IdOr, occursFields(i)  'per ciclicit�
  Next i
  rsTable.Close
End Sub

Public Sub createLookUpTable(idDB As Long, IdOr As Long, NomeTb As String, NomeDB As String, IdTbs As Long)
  Dim rsTable As Recordset
    
  GbNomeTbInAdd = NomeTb
  
  Set rsTable = m_fun.Open_Recordset("Select * From " & PrefDb & "_Tabelle Where " & _
                                     "IdDB = " & idDB & " And IdOrigine = " & IdOr & " And Nome = '" & NomeTb & "' And " & _
                                     "NomeDB ='" & NomeDB & "'")
  If rsTable.RecordCount = 0 Then
    rsTable.AddNew
    
    rsTable!idDB = idDB
    rsTable!Nome = NomeTb
    rsTable!IdTableSpc = IdTbs
    rsTable!NomeDB = NomeDB
    rsTable!idTableJoin = 0
    rsTable!IdOrigine = IdOr
    rsTable!IdObjSource = IdTbinMigr
    rsTable!Descrizione = "VSAM to " & TipoDB & " Automatic Migration"
    rsTable!Data_Creazione = Adesso
    rsTable!Data_Modifica = Adesso
    rsTable!Tag = "AUTOMATIC-LOOKUP"
    'SQ
    If Len(DEFAULT_CREATOR) Then rsTable!Creator = DEFAULT_CREATOR
    rsTable.Update
  End If
  rsTable.Close
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Valorizza "occursFields", array di "liste" di idcmp
' relativi ai campi di una occurs
''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub getOccursFields(idRecType As Long)
  Dim rs As Recordset, rsRT As Recordset
  Dim occursLevel As Integer
  Dim i As Integer
  
  ReDim occursFields(0)
  i = 1
  Set rsRT = m_fun.Open_Recordset("select * from DMVSM_RecordType where " & _
                                  "idrecordtype = " & idRecType)
  If rsRT.RecordCount Then
    Set rs = m_fun.Open_Recordset("select idCmp,livello,occurs,occursflat from DMVSM_Data_cmp where " & _
                                  "vsam = '" & rsRT!VSAM & "' and idarea = " & rsRT!idArea & _
                                  " and idoggetto = " & rsRT!IdOggetto & " order by ordinale")
    Do Until rs.EOF
      If rs!Occurs And Not rs!OccursFlat Then
        ReDim Preserve occursFields(i)
        'Serve? � il primo della lista!
        occursFields(i) = occursFields(i) & rs!IdCmp & ","
        occursLevel = rs!livello
        rs.MoveNext
        Do Until rs.EOF
          If rs!livello > occursLevel Then
            occursFields(i) = occursFields(i) & rs!IdCmp & ","
            rs.MoveNext
          Else
            i = i + 1
            Exit Do
          End If
        Loop
      Else
        rs.MoveNext
      End If
    Loop
    rs.Close
  End If
  rsRT.Close
  
  For i = 1 To UBound(occursFields)
    ' elimino l'ultima virgola
    If Len(occursFields(i)) Then
      occursFields(i) = Left(occursFields(i), Len(occursFields(i)) - 1)
    End If
  Next i
End Sub

Public Sub addOccursColumns(IdTable As Long, idTableJoin As Long, idRT As Long, listaOccurs As String)
  Dim rsColonne As Recordset, rsCmp As Recordset, rsRT As Recordset
  Dim NomeNorm As String
  Dim IdColonna As Long, Ordinale As Integer
  Dim env As Collection
  
  On Error GoTo errdb
  
  'OCCURS_I
  MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: OCCURS_I"
  MadmdF_VSAM2RDBMS.LswLog.Refresh
  MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
  Ordinale = 1
  IdColonna = getId("idColonna", PrefDb & "_Colonne") 'getId ritorna -1 in caso d'errore!
  Set rsColonne = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne")
  rsColonne.AddNew
  rsColonne!IdColonna = IdColonna
  rsColonne!IdTable = IdTable
  'SQ 19-07-06 - Puntavo alla tabella "origine"... modificato per le WHERE condition...
  rsColonne!idTableJoin = IdTable 'IdTableJoin
  rsColonne!Nome = "KEYSEQ"
  rsColonne!Ordinale = Ordinale
  rsColonne!IdCmp = -1
  rsColonne!NomeCmp = ""
  If TipoDB = "DB2" Then
    rsColonne!Tipo = "SMALLINT"
    rsColonne!lunghezza = 2
  ElseIf TipoDB = "ORACLE" Then
    rsColonne!Tipo = "NUMBER"
    rsColonne!lunghezza = 5
  End If
  rsColonne!Decimali = 0
  rsColonne!Descrizione = "Occurs Index Column"
  rsColonne!Null = False
  rsColonne!IsDefault = DEFAULT_DEFAULT
  If DEFAULT_DEFAULT Then
    rsColonne!defaultValue = "" '?? che metto???
  Else
    rsColonne!defaultValue = ""
  End If
  rsColonne!Tag = "AUTOMATIC-OCCURS"
  rsColonne!Commento = ""
  rsColonne!RoutLoad = "load_OCCURS_I"
  rsColonne!RoutRead = "rdbms2vsam_OCCURS_I"
  rsColonne!RoutWrite = "vsam2rdbms_OCCURS_I"
  rsColonne!RoutKey = "vsam2rdbms_KEY_OCCURS_I"
  rsColonne.Update
  
  IdColonna = IdColonna + 1
  
  Set rsRT = m_fun.Open_Recordset("select * from DMVSM_Recordtype where " & _
                                  "idrecordtype = " & idRT)
  If rsRT.RecordCount Then
    Set rsCmp = m_fun.Open_Recordset("Select * from DMVSM_Data_Cmp where " & _
                                     "idCmp in (" & listaOccurs & ") and vsam = '" & rsRT!VSAM & "'" & _
                                     " and idoggetto = " & rsRT!IdOggetto & " and idarea = " & rsRT!idArea & _
                                     "And lunghezza > 0 And Nome <> 'FILLER' And Nome <> 'FILLER-REVISER' order by IdCmp")
    Do Until rsCmp.EOF
      Set env = New Collection
      env.Add rsCmp
      NomeNorm = getEnvironmentParam(VSAM_ColName, "colonneVSM", env, TipoDB)
      
      MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: " & NomeNorm
      MadmdF_VSAM2RDBMS.LswLog.Refresh
      MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
      
      Ordinale = Ordinale + 1
      
      If TipoDB = "DB2" Then
        Trascodifica_Tipo_VSAM_DB2 rsCmp!Tipo, rsCmp!Usage, rsCmp!lunghezza, rsCmp!Decimali, NomeNorm
      ElseIf TipoDB = "ORACLE" Then
        Trascodifica_Tipo_VSAM_ORACLE rsCmp!Tipo, rsCmp!Usage, rsCmp!lunghezza, rsCmp!Decimali, NomeNorm
      End If
              
      rsColonne.AddNew
      rsColonne!IdColonna = IdColonna
      rsColonne!IdTable = IdTable
      'SQ 19-07-06 - Puntavo alla tabella "origine"... modificato per le WHERE condition...
      rsColonne!idTableJoin = IdTable 'IdTableJoin
      rsColonne!Nome = NomeNorm
      rsColonne!Ordinale = Ordinale
      rsColonne!IdCmp = rsCmp!IdCmp
      rsColonne!NomeCmp = rsCmp!Nome
      If TipoDB = "DB2" Then
        rsColonne!Tipo = DB2Tipo
        rsColonne!lunghezza = DB2Lunghezza
        rsColonne!Decimali = DB2Decimali
      ElseIf TipoDB = "ORACLE" Then
        rsColonne!Tipo = ORACLETipo
        rsColonne!lunghezza = ORACLELunghezza
        rsColonne!Decimali = ORACLEDecimali
      End If
      rsColonne!Descrizione = "Column migrated from VSAM"
      rsColonne!Null = DEFAULT_NULL
      'MG 130207 Modificato default
      rsColonne!IsDefault = DEFAULT_DEFAULT
      If DEFAULT_DEFAULT Then
        rsColonne!defaultValue = "" '?? che metto???
      Else
        rsColonne!defaultValue = ""
      End If
      rsColonne!Tag = "AUTOMATIC-OCCURS"
      rsColonne!Commento = ""
      If TipoDB = "DB2" Then
        rsColonne!RoutLoad = "load_" & DB2Tipo & "_OCCURS"
        rsColonne!RoutRead = "rdbms2vsam_" & DB2Tipo & "_OCCURS"
        rsColonne!RoutWrite = "vsam2rdbms_" & DB2Tipo & "_OCCURS"
        rsColonne!RoutKey = "vsam2rdbms_KEY_" & DB2Tipo & "_OCCURS"
      ElseIf TipoDB = "ORACLE" Then
        rsColonne!RoutLoad = "load_" & ORACLETipo & "_OCCURS"
        rsColonne!RoutRead = "rdbms2vsam_" & ORACLETipo & "_OCCURS"
        rsColonne!RoutWrite = "vsam2rdbms_" & ORACLETipo & "_OCCURS"
        rsColonne!RoutKey = "vsam2rdbms_KEY_" & ORACLETipo & "_OCCURS"
      End If
      rsColonne.Update
      
      IdColonna = IdColonna + 1
          
      rsCmp.MoveNext
    Loop
    rsCmp.Close
  End If
  rsRT.Close
  rsColonne.Close
  
  Exit Sub
errdb:
  'gestire:
  MsgBox ERR.Description
  'Resume
End Sub

'GLI ULTIMI DUE PARAMETRI MI POSSONO SERVIRE PER LE "EXTRA COLUMNS"...
' Mauro 07/09/2007 : Unificate le routine DB2-ORACLE
'Public Sub Add_Colonne_DB2(IdTable As Long, idSeg As Long, NomeTb As String, NomeDB As String)
Public Sub Add_Colonne(IdTable As Long, idRT As Long, NomeTb As String, NomeDB As String)
  Dim r As Recordset, rc As Recordset, rsRT As Recordset
  Dim NomeNorm As String
  Dim IdColonna As Long, Ordinale As Long
  Dim k As Integer, i As Long
  Dim rsOccursCmp As Recordset
  Dim OccursFld() As Long, Occurs As Long
  Dim occursLevel As Integer
  Dim env As Collection
  
  'IdColonna:
  IdColonna = getId("idColonna", PrefDb & "_Colonne") 'getId ritorna -1 in caso d'errore!
  If IdColonna < 0 Then
    'gestire
    MsgBox "Warning! " & ERR.Description & " Impossible to add columns!"
    Exit Sub
  End If
  
  'PS: UTILIZZARE UN SOLO unica open e close di un unico rc...
  
  Set rc = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne")
  ''''''''''''''''''''''''''''''''''''''
  ' AGGIUNTA COLONNE HEADER
  ''''''''''''''''''''''''''''''''''''''
  On Error GoTo errdb
  Set r = m_fun.Open_Recordset("Select * from DMheader_columns order by ordinale")
  Do Until r.EOF
    MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: " & r!Nome
    MadmdF_VSAM2RDBMS.LswLog.Refresh
    MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
  
    Ordinale = Ordinale + 1
    rc.AddNew
    rc!IdColonna = IdColonna
    rc!IdTable = IdTable
    rc!idTableJoin = IdTable
    rc!Nome = r!Nome
    rc!Ordinale = Ordinale
    rc!IdCmp = -1
    rc!Tipo = r!Tipo
    rc!lunghezza = r!lunghezza
    rc!Decimali = r!Decimali
    rc!Descrizione = "Header Column"
    rc!Null = r!Null
    rc!Default = r!Default
    rc!Tag = "AUTOMATIC-VSAM" & TipoDB
    rc!RoutLoad = r!TemplateLoad
    rc!RoutRead = r!TemplateRead
    rc!RoutWrite = r!TemplateWrite
    rc!RoutKey = r!TemplateKey
    rc.Update
    
    r.MoveNext
    IdColonna = IdColonna + 1
  Loop
  r.Close
  rc.Close
  
  'ATTENZIONE, sarebbe una count(*)...
  'da qui per i VSAM fare il delete di questa tabella x idtable ALE
  DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Colonne where IdTable = " & IdTable
  ' Mauro 10/09/2007 : I campi per la where me li ricavo dalla DMVSM_RecordType
  Set rsRT = m_fun.Open_Recordset("Select * from DMVSM_RecordType where idrecordtype = " & idRT)
  If rsRT.RecordCount Then
    Set r = m_fun.Open_Recordset("Select * from  DMVSM_Data_Cmp where " & _
                                 "vsam = '" & rsRT!VSAM & "' and idarea = " & rsRT!idArea & _
                                 " and idoggetto = " & rsRT!IdOggetto & " and (lunghezza + decimali > 0 or occurs > 0)" & _
                                 "and damigrare = -1 order by ordinale")
      Do Until r.EOF
        Set env = New Collection
        env.Add r
        NomeNorm = getEnvironmentParam(VSAM_ColName, "colonneVSM", env, TipoDB)
        If NomeNorm <> "FILLER" And NomeNorm <> "FILLER_REVISER" Then  'PARAMETRIZZARE IL CONTROLLO!
          If r!Occurs = 0 Then
            Ordinale = Ordinale + 1
            If TipoDB = "DB2" Then
              Trascodifica_Tipo_VSAM_DB2 r!Tipo, r!Usage & "", r!lunghezza, r!Decimali, NomeNorm
            ElseIf TipoDB = "ORACLE" Then
              Trascodifica_Tipo_VSAM_ORACLE r!Tipo, r!Usage & "", r!lunghezza, r!Decimali, NomeNorm
            End If
            NomeNorm = Replace(NomeNorm, "-", "_")
            Set rc = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne Where " & _
                                          "IdColonna = " & IdColonna & " And IdTable = " & IdTable)
            If rc.RecordCount = 0 Then  'la open l'ho fatta 10 operazioni fa!?
              MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: " & NomeNorm
              MadmdF_VSAM2RDBMS.LswLog.Refresh
              MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
            
              rc.AddNew
              rc!IdColonna = IdColonna
              rc!IdTable = IdTable
              rc!idTableJoin = IdTable
              rc!Nome = NomeNorm
              rc!Ordinale = Ordinale
              rc!IdCmp = r!IdCmp
              rc!NomeCmp = r!Nome
              If TipoDB = "DB2" Then
                rc!Tipo = DB2Tipo
                rc!lunghezza = DB2Lunghezza
                rc!Decimali = DB2Decimali
              ElseIf TipoDB = "ORACLE" Then
                rc!Tipo = ORACLETipo
                rc!lunghezza = ORACLELunghezza
                rc!Decimali = ORACLEDecimali
              End If
              rc!Descrizione = "Column migrated from VSAM"
              rc!Null = DEFAULT_NULL
              'ATTENZIONE, SISTEMARE: DEFAULT NON E' BOOLEAN...
              rc!Default = DEFAULT_DEFAULT
              rc!Tag = "AUTOMATIC-VSAM" & TipoDB
              rc!Commento = ""
              If TipoDB = "DB2" Then
                rc!RoutLoad = "load_" & DB2Tipo
                rc!RoutRead = "rdbms2vsam_" & DB2Tipo
                rc!RoutWrite = "vsam2rdbms_" & DB2Tipo
                rc!RoutKey = "vsam2rdbms_KEY_" & DB2Tipo
              ElseIf TipoDB = "ORACLE" Then
                rc!RoutLoad = "load_" & ORACLETipo
                rc!RoutRead = "rdbms2vsam_" & ORACLETipo
                rc!RoutWrite = "vsam2rdbms_" & ORACLETipo
                rc!RoutKey = "vsam2rdbms_KEY_" & ORACLETipo
              End If
              rc.Update
              IdColonna = IdColonna + 1
            End If
            rc.Close
          Else
            If r!OccursFlat Then
              ' Espando l'occurs
              Occurs = r!Occurs
              i = 1
              ReDim OccursFld(i)
              OccursFld(i) = r!IdCmp
              i = i + 1
              occursLevel = r!livello
              r.MoveNext
              Do Until r.EOF
                If r!livello > occursLevel Then
                  ReDim Preserve OccursFld(i)
                  OccursFld(i) = r!IdCmp
                  i = i + 1
                  r.MoveNext
                Else
                  r.MovePrevious
                  Exit Do
                End If
              Loop
              For i = 1 To Occurs
                For k = 1 To UBound(OccursFld)
                  Set rsOccursCmp = m_fun.Open_Recordset("select * from DMVSM_Data_Cmp where " & _
                                                         "idcmp = " & OccursFld(k))
                  If rsOccursCmp.RecordCount Then
                    Ordinale = Ordinale + 1
                    Set env = New Collection
                    env.Add rsOccursCmp
                    NomeNorm = getEnvironmentParam(VSAM_ColName, "colonneVSM", env, TipoDB)
                    NomeNorm = Replace(NomeNorm, "-", "_")
                    If TipoDB = "DB2" Then
                      Trascodifica_Tipo_VSAM_DB2 rsOccursCmp!Tipo, rsOccursCmp!Usage & "", _
                                                 rsOccursCmp!lunghezza, rsOccursCmp!Decimali, NomeNorm
                    ElseIf TipoDB = "ORACLE" Then
                      Trascodifica_Tipo_VSAM_ORACLE rsOccursCmp!Tipo, rsOccursCmp!Usage & "", _
                                                 rsOccursCmp!lunghezza, rsOccursCmp!Decimali, NomeNorm
                    End If
                    NomeNorm = Replace(NomeNorm, "-", "_")
                    Set rc = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne Where " & _
                                                  "IdColonna = " & IdColonna & " And IdTable = " & IdTable)
                    If rc.RecordCount = 0 Then  'la open l'ho fatta 10 operazioni fa!?
                      MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: " & NomeNorm & "_" & i
                      MadmdF_VSAM2RDBMS.LswLog.Refresh
                      MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
                    
                      rc.AddNew
                      rc!IdColonna = IdColonna
                      rc!IdTable = IdTable
                      rc!idTableJoin = IdTable
                      rc!Nome = NomeNorm & "_" & i
                      rc!Ordinale = Ordinale
                      rc!IdCmp = rsOccursCmp!IdCmp
                      rc!NomeCmp = rsOccursCmp!Nome
                      If TipoDB = "DB2" Then
                        rc!Tipo = DB2Tipo
                        rc!lunghezza = DB2Lunghezza
                        rc!Decimali = DB2Decimali
                      ElseIf TipoDB = "ORACLE" Then
                        rc!Tipo = ORACLETipo
                        rc!lunghezza = ORACLELunghezza
                        rc!Decimali = ORACLEDecimali
                      End If
                      rc!Descrizione = "Column migrated from VSAM"
                      rc!Null = DEFAULT_NULL
                      'ATTENZIONE, SISTEMARE: DEFAULT NON E' BOOLEAN...
                      rc!Default = DEFAULT_DEFAULT
                      rc!Tag = "AUTOMATIC-VSAM" & TipoDB
                      rc!Commento = ""
                      If TipoDB = "DB2" Then
                        rc!RoutLoad = "load_" & DB2Tipo
                        rc!RoutRead = "rdbms2vsam_" & DB2Tipo
                        rc!RoutWrite = "vsam2rdbms_" & DB2Tipo
                        rc!RoutKey = "vsam2rdbms_KEY_" & DB2Tipo
                      ElseIf TipoDB = "ORACLE" Then
                        rc!RoutLoad = "load_" & ORACLETipo
                        rc!RoutRead = "rdbms2vsam_" & ORACLETipo
                        rc!RoutWrite = "vsam2rdbms_" & ORACLETipo
                        rc!RoutKey = "vsam2rdbms_KEY_" & ORACLETipo
                      End If
                      rc.Update
                      IdColonna = IdColonna + 1
                    End If
                    rc.Close
                  End If
                  rsOccursCmp.Close
                Next k
              Next i
            End If
          End If
        End If
        If Not r.EOF Then
          r.MoveNext
        End If
        DoEvents
      Loop
      r.Close
    End If
    rsRT.Close
    ''''''''''''''''''''''''''''''''''''''
    ' AGGIUNTA COLONNE FOOTER
    ''''''''''''''''''''''''''''''''''''''
    Set rc = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne")
    Set r = m_fun.Open_Recordset("Select * from DMfooter_columns order by ordinale")
    Do Until r.EOF
      'fase_2, ma sara' ok per tutti...
      NomeNorm = r!Nome
      MadmdF_VSAM2RDBMS.LswLog.ListItems.Add , , Time & " - Adding New Column: " & NomeNorm
      MadmdF_VSAM2RDBMS.LswLog.Refresh
      MadmdF_VSAM2RDBMS.LswLog.ListItems(MadmdF_VSAM2RDBMS.LswLog.ListItems.Count).EnsureVisible
      Ordinale = Ordinale + 1
      rc.AddNew
      rc!IdColonna = IdColonna
      rc!IdTable = IdTable
      rc!idTableJoin = IdTable
      rc!Nome = NomeNorm
      rc!Ordinale = Ordinale
      rc!IdCmp = -1
      rc!Tipo = r!Tipo
      rc!lunghezza = r!lunghezza
      rc!Decimali = r!Decimali
      rc!Descrizione = "Footer Column"
      rc!Null = r!Null
      rc!Default = r!Default
      rc!Tag = "AUTOMATIC-VSAM" & TipoDB
      rc!RoutLoad = r!TemplateLoad
      rc!RoutRead = r!TemplateRead
      rc!RoutWrite = r!TemplateWrite
      rc!RoutKey = r!TemplateKey
              
      rc.Update
      r.MoveNext
      IdColonna = IdColonna + 1
    Loop
    r.Close
    rc.Close
  Exit Sub
errdb:
  'gestire:
  MsgBox ERR.Description
  'Resume
End Sub

Public Function Trascodifica_Tipo_VSAM_DB2(Tipo As String, Usage As String, lunghezza As Long, Dec As Long, NomeCol As String)
  
  Select Case Trim(UCase(Tipo))
    Case "X"
      Select Case Trim(UCase(Usage))
        Case "ZND"
          If lunghezza >= 1 And lunghezza <= 255 Then
            DB2Tipo = "CHAR"
            DB2Lunghezza = lunghezza
            DB2Decimali = 0
          End If
          If lunghezza > 255 Then
            DB2Tipo = "VARCHAR"
            DB2Lunghezza = lunghezza
            DB2Decimali = 0
          End If
        Case "EDT"
          If lunghezza >= 1 Then
            DB2Tipo = "DECIMAL"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
          End If
      End Select
    Case "A"
      If Trim(UCase(Usage)) = "ZND" Then
        If lunghezza >= 1 And lunghezza <= 255 Then
          DB2Tipo = "CHAR"
          DB2Lunghezza = lunghezza
          DB2Decimali = 0
        End If
        If lunghezza > 255 Then
          DB2Tipo = "VARCHAR"
          DB2Lunghezza = lunghezza
          DB2Decimali = 0
        End If
      End If
    Case "9"
      Select Case Trim(UCase(Usage))
        Case "EDT", "ZND", "PCK"
          If lunghezza + Dec >= 1 Then
            DB2Tipo = "DECIMAL"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
          End If
        Case "BNR"
          If lunghezza + Dec >= 1 And lunghezza + Dec <= 4 Then
            DB2Tipo = "SMALLINT"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
          End If
          If lunghezza + Dec >= 5 And lunghezza + Dec <= 8 Then
            DB2Tipo = "INTEGER"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
          End If
          If lunghezza + Dec >= 9 Then
            DB2Tipo = "LONG"
            DB2Lunghezza = lunghezza + Dec
            DB2Decimali = Dec
          End If
      End Select
  End Select
  'analisi casi particolari
  'eliminato...
End Function

Public Function Trascodifica_Tipo_VSAM_ORACLE(Tipo As String, Usage As String, lunghezza As Long, Dec As Long, NomeCol As String)
  
  Select Case Trim(UCase(Tipo))
    Case "X"
      Select Case Trim(UCase(Usage))
        Case "ZND"
          If lunghezza >= 1 And lunghezza <= 255 Then
            ORACLETipo = "CHAR"
            ORACLELunghezza = lunghezza
            ORACLEDecimali = 0
          End If
          If lunghezza > 255 Then
            ORACLETipo = "VARCHAR"
            ORACLELunghezza = lunghezza
            ORACLEDecimali = 0
          End If
        Case "EDT"
          If lunghezza + Dec >= 1 Then
            ORACLETipo = "DECIMAL"
            ORACLELunghezza = lunghezza + Dec
            ORACLEDecimali = Dec
          End If
      End Select
    Case "A"
      If Trim(UCase(Usage)) = "ZND" Then
        If lunghezza >= 1 And lunghezza <= 255 Then
          ORACLETipo = "CHAR"
          ORACLELunghezza = lunghezza
          ORACLEDecimali = 0
        End If
        If lunghezza > 255 Then
          ORACLETipo = "VARCHAR"
          ORACLELunghezza = lunghezza
          ORACLEDecimali = 0
        End If
      End If
    Case "9"
      Select Case Trim(UCase(Usage))
        Case "EDT", "ZND", "PCK"
          If lunghezza + Dec >= 1 Then
            ORACLETipo = "DECIMAL"
            ORACLELunghezza = lunghezza + Dec
            ORACLEDecimali = Dec
          End If
        Case "BNR"
          If lunghezza + Dec >= 1 And lunghezza + Dec <= 4 Then
            ORACLETipo = "SMALLINT"
            ORACLELunghezza = lunghezza + Dec
            ORACLEDecimali = Dec
          End If
          If lunghezza + Dec >= 5 And lunghezza + Dec <= 8 Then
            ORACLETipo = "INTEGER"
            ORACLELunghezza = lunghezza + Dec
            ORACLEDecimali = Dec
          End If
          If lunghezza + Dec >= 9 Then
            ORACLETipo = "LONG"
            ORACLELunghezza = lunghezza + Dec
            ORACLEDecimali = Dec
          End If
      End Select
  End Select
   
  'analisi casi particolari
  If InStr(NomeCol, "DT") > 0 And lunghezza = 8 Then ' considero campo data
    ORACLETipo = "DATE"
    ORACLELunghezza = 10
    ORACLEDecimali = 0
    Exit Function
  End If
  
  If lunghezza = 1 Or lunghezza = 2 Then  ' considero campi flag
    If Tipo = "9" And Dec = 0 Then
      ORACLETipo = "CHAR"
      ORACLELunghezza = lunghezza
      ORACLEDecimali = 0
      Exit Function
    End If
  End If
  
  If lunghezza > 8 And Dec = 2 Then  ' considero campo importo
    ORACLETipo = "DECIMAL"
    ORACLELunghezza = 15
    ORACLEDecimali = 2
    Exit Function
  End If
  
  If InStr(NomeCol, "CAP") And lunghezza = 5 Then ' codice C.A.P.
    ORACLETipo = "CHAR"
    ORACLELunghezza = 5
    ORACLEDecimali = 0
    Exit Function
  End If
  If InStr(NomeCol, "ABI") And lunghezza = 5 Then ' codice Banca ABI
    ORACLETipo = "CHAR"
    ORACLELunghezza = 5
    ORACLEDecimali = 0
    Exit Function
  End If
  If InStr(NomeCol, "CAB") And lunghezza = 4 Then ' codice Banca CAB
    ORACLETipo = "CHAR"
    ORACLELunghezza = 4
    ORACLEDecimali = 0
    Exit Function
  End If
  If InStr(NomeCol, "PIVA") And lunghezza = 11 Then ' Partita IVA
    ORACLETipo = "CHAR"
    ORACLELunghezza = 11
    ORACLEDecimali = 0
    Exit Function
  End If
  If InStr(NomeCol, "CDEN") And lunghezza = 7 Then ' Codice Dir.
    ORACLETipo = "CHAR"
    ORACLELunghezza = 7
    ORACLEDecimali = 0
    Exit Function
  End If
End Function

' Mauro 07/09/2007 : Unificate le routine DB2-ORACLE
'Public Function Get_NomeColonna_DB2(IdCol As Long) As String
Public Function Get_NomeColonna(IdCol As Long) As String
  Dim r As Recordset
  
  'Prende L'ultimo ID disponibile
  Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne Where IdColonna = " & IdCol)
  If r.RecordCount Then
    Get_NomeColonna = r!Nome
  Else
    Get_NomeColonna = ""
  End If
  r.Close
End Function

Public Sub Carica_Lista_VSAM()
  Dim rs As Recordset, rs2 As Recordset
  Dim listx As ListItem
   
  lswDBSel.ListItems.Clear
  
  'Selezione tutti i VSAM ('%SDS')
  'Set rs = m_fun.Open_Recordset("select IdArchivio, Nome FROM DMVSM_Archivio WHERE TYPE LIKE '%%DS'")
  Set rs = m_fun.Open_Recordset("select NomeVsam, Acronimo FROM DMVSM_nomivsam")
  Do Until rs.EOF
    Set listx = lswDBSel.ListItems.Add(, , rs!nomeVSAM)
    listx.SubItems(1) = rs!Acronimo
    'Associazione aree
    Set rs2 = m_fun.Open_Recordset("SELECT * FROM DMVSM_RecordType WHERE " & _
                                   "vsam = '" & rs!nomeVSAM & "' AND idoggetto > 0")
    listx.SubItems(2) = IIf(rs2.RecordCount, "X", "")
    rs2.Close
    rs.MoveNext
  Loop
  rs.Close
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' GESTIONE OCCURS:
' - SOTTOTABELLA OCCURS (PK + OCCURS_I + <COLONNE>)
'   -> crea PK e inserisce le colonne ereditate
'   -> crea FK
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub createOccursIndex(rsTable As Recordset)
  Dim rsOrig As Recordset, rsNew As Recordset, rOrig As Recordset, rNew As Recordset, rsIndexOrig As Recordset
  Dim i As Integer, j As Integer, columnsCount As Integer
  Dim IdColonna As Long, IdIndex As Long, idIndexOrig As Long
  Dim idColonnaInit As Long
  
  On Error GoTo errdb
  
  IdColonna = getId("idColonna", PrefDb & "_Colonne") 'getId ritorna -1 in caso d'errore!
  IdIndex = getId("IdIndex", PrefDb & "_Index")
  Do Until rsTable.EOF
    'INDICE
    Set rsIndexOrig = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index where " & _
                                           "idTable = " & rsTable!idTableJoin & " and Tipo = 'P'")
    If Not rsIndexOrig.EOF Then
      idIndexOrig = rsIndexOrig!IdIndex
      Set rsNew = rsIndexOrig.Clone
      rsNew.AddNew
      For i = 1 To rsIndexOrig.fields.Count - 1
        If Not IsNull(rsIndexOrig.fields.Item(i)) Then
          rsNew.fields.Item(i) = rsIndexOrig.fields.Item(i)
        End If
      Next i
      rsNew!IdIndex = IdIndex
      rsNew!IdTable = rsTable!IdTable
      rsNew!Tag = "AUTOMATIC-OCCURS"
      rsNew.Update
      rsNew.Close
      
      'IDXCOL
      Set rsOrig = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol where " & _
                                        "idIndex=" & idIndexOrig)
      columnsCount = rsOrig.RecordCount  'lo devo bloccare, perch� con le clone aumenta!
            
      'Update Ordinale Colonne Sottotabella Occurs
      DataMan.DmConnection.Execute "Update " & PrefDb & "_Colonne set Ordinale = Ordinale + " & rsOrig.RecordCount & _
                                   " where idTable=" & rsTable!IdTable
      
      Set rsNew = rsOrig.Clone
      
      idColonnaInit = IdColonna
      For j = 1 To columnsCount
        'COLONNA
        Set rOrig = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where " & _
                                         "idColonna=" & rsOrig!IdColonna)
        Set rNew = rOrig.Clone
        rNew.AddNew
        For i = 1 To rOrig.fields.Count - 1
          If Not IsNull(rOrig.fields.Item(i)) Then
            rNew.fields.Item(i) = rOrig.fields.Item(i)
          End If
        Next i
        rNew!IdColonna = IdColonna
        rNew!IdTable = rsTable!IdTable
        'SQ - 19-07-06
        'rNew!IdTableJoin = rsOrig!IdTableJoin
        rNew!Tag = "AUTOMATIC-OCCURS"
        rNew.Update
        rOrig.Close
        rNew.Close
        
        'Colonna di IDXCOL
        rsNew.AddNew
        For i = 1 To rsOrig.fields.Count - 1
          If Not IsNull(rsOrig.fields.Item(i)) Then
            rsNew.fields.Item(i) = rsOrig.fields.Item(i)
          End If
        Next i
        rsNew!IdIndex = IdIndex
        rsNew!IdTable = rsTable!IdTable
        rsNew!IdColonna = IdColonna
        rsNew!Tag = "AUTOMATIC-OCCURS"
        rsNew.Update
        
        IdColonna = IdColonna + 1
        
        rsOrig.MoveNext
      Next j
      rsOrig.Close
            
      'Colonna di IDXCOL: OCCURS_I
      'Mi serve l'idColonna:
      Set rOrig = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where " & _
                                       "idTable = " & rsTable!IdTable & " and ordinale = " & columnsCount + 1)
      rsNew.AddNew
      rsNew!IdIndex = IdIndex
      rsNew!Ordinale = columnsCount + 1
      rsNew!IdTable = rsTable!IdTable
      rsNew!IdColonna = rOrig!IdColonna
      rsNew!Order = "ASCENDING"
      rsNew!Tag = "AUTOMATIC-OCCURS"
      rsNew.Update
      rsNew.Close
      rOrig.Close
    End If
    rsIndexOrig.Close
    
    IdIndex = IdIndex + 1
    
    Set rsIndexOrig = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index where " & _
                                           "idTable = " & rsTable!idTableJoin & " and Tipo = 'F'")
    If rsIndexOrig.RecordCount Then
      idIndexOrig = rsIndexOrig!IdIndex
      Set rsNew = rsIndexOrig.Clone
      rsNew.AddNew
      For i = 1 To rsIndexOrig.fields.Count - 1
        If Not IsNull(rsIndexOrig.fields.Item(i)) Then
          rsNew.fields.Item(i) = rsIndexOrig.fields.Item(i)
        End If
      Next i
      rsNew!IdIndex = IdIndex
      rsNew!IdTable = rsTable!IdTable
      rsNew!Tag = "AUTOMATIC-OCCURS"
      rsNew.Update
      rsNew.Close
      
      'IDXCOL
      Set rsOrig = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol where " & _
                                        "idIndex = " & idIndexOrig)
      columnsCount = rsOrig.RecordCount  'lo devo bloccare, perch� con le clone aumenta!
      
      Set rsNew = rsOrig.Clone
      For j = 1 To columnsCount
        'COLONNA
        Set rOrig = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where " & _
                                         "idColonna = " & rsOrig!IdColonna)
        
        'Colonna di IDXCOL
        rsNew.AddNew
        For i = 1 To rsOrig.fields.Count - 1
          If Not IsNull(rsOrig.fields.Item(i)) Then
            rsNew.fields.Item(i) = rsOrig.fields.Item(i)
          End If
        Next i
        rsNew!IdIndex = IdIndex
        rsNew!IdTable = rsTable!IdTable
        rsNew!IdColonna = idColonnaInit + j - 1
        rsNew!Tag = "AUTOMATIC-OCCURS"
        rsNew.Update
        
        rsOrig.MoveNext
      Next j
      rsOrig.Close
      
      IdIndex = IdIndex + 1
    End If
    rsIndexOrig.Close
    
    rsTable.MoveNext
  Loop
  'rsTable.Close  'chiudo fuori!
  Exit Sub
errdb:
  'gestire:
  MsgBox ERR.Number & " - " & ERR.Description
  'Resume
End Sub

Private Sub lswDBSel_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswDBSel.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswDBSel.SortOrder = Order
  lswDBSel.SortKey = Key - 1
  lswDBSel.Sorted = True
End Sub

Private Sub LswLog_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = LswLog.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  LswLog.SortOrder = Order
  LswLog.SortKey = Key - 1
  LswLog.Sorted = True
End Sub
