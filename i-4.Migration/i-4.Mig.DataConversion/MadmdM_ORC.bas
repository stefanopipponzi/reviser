Attribute VB_Name = "MadmdM_ORC"
Option Explicit

Global SegSelX As Long
Global SegSelY As Long
Global IdTbs As Long
Global IdTav As Long
Global IdCmp As Long
Global IdKey As Long

Public TBSMode As String

' Variabili che tengono conto dell' elemento cliccato in una Listview
Public ItemX As Long
Public Itemy As Long

' Variabile che tiene conto del fatto che la colonna che aggiungo � la prima o successiva
Public First_Col As Boolean

' Variabile che tiene conto del fatto che l' indice che aggiungo � il primo o successivo
Public First_Index As Boolean

' Variabile che tiene conto del fatto che l' indice che aggiungo � il primo o successivo
Public First_IndXCol As Boolean

Type Col_REL
  IdColonna As Long
  Ordinale As Long
  Nome As String
  Tipo As String
  lunghezza As Long
  Decimali As Long
  Formato As String
  Etichetta As String
  Null As Boolean
  Order As String
  VincoloVal As String
  Descrizione As String
  TableJoin As Long
  IdCmp As Long
  RoutLoad As String
  RoutRead As String
  RoutWrite As String
  RoutKey As String
End Type

Type Idx_REL
  IdIndex As Long
  IdTable As Long
  IdTableSpc As Long
  NomeKey As String
  Tipo As String
  Unique As Boolean
  DataC As Date
  DataM As Date
  Descrizione As String
  Colonne() As Col_REL
End Type

Type Tb_REL
  IdTable As Long
  IdTableSpc As Long
  Nome As String
  Creator As String
  Cyl As Long
  DataC As Date
  Descrizione As String
  Colonne() As Col_REL
End Type

Type TbSpc_REL
  IdTableSpc As Long
  Nome As String
  Dimensione As Long
  DataC As Date
  DataM As Date
  Descrizione As String
End Type

Type Trigger
  IdTrigger As Long
  IdTable As Long
  Corpo As String
  Nome As String
  Evento As String
  livello As String
  Transazione As String
  Azione As String
  Referencing As String
  Condizione As String
  Stato As String
  Descrizione As String
  DataC As Date
  DataM As Date
End Type

Type idxcol
  IdIndex As Long
  IdColonna As Long
  IdTable As Long
  Ordinale As Long
  Order As String
End Type

Global REL_TableSpc As TbSpc_REL
Global REL_Table As Tb_REL
Global REL_index() As Idx_REL
Global REL_Trigger() As Trigger
Global Rel_IdXCol() As idxcol
Public NomeTBS As String

Type TabRelations
  Name As String ' Tabella
  IdTable As Long ' ID Tabella
  IdSegmento As Long ' ID origine tabella
  LC() As String ' Logical Childs
  idLC() As Long ' ID LChilds
  LP As String ' Logical Parent
  idLP As Long ' ID LParent
  PC() As String ' Phisical Childs
  idPC() As Long ' ID PChilds
  paired As String ' Tabella gemellata
  idPaired As Long ' ID tabella gemellata
  PP As String ' Phisical Parent
  idPP As Long ' ID PParent
End Type
Public arrRelations() As TabRelations

Type cmpCol
  nomeCmp1 As String
  lenCmp1 As Long
  Tipo As String
  Index As Boolean
  nomeCmp2 As String
  lenCmp2 As Long
End Type
Public CompareCmp() As cmpCol

' Variabili per Tag dei TRIGGER
Public Creator As String
Public nomeLChild As String
Public nomeLParent As String
Public Declare_PK_LP As String
Public Cursor_PK_LC As String
Public Column_PK_LP As String
Public where_ISRT As String
Public Open_PK_LC As String
Public Into_PK_LP As String
Public nomePaired As String
Public where_Cond_UPDT As String
Public set_Fields_UPDT As String
Public where_PK_LC As String
Public where_PK_LP As String
Public set_ISRT_Pair As String
Public value_ISRT_Pair As String
'''Public set_Pair_ISRT As String
'''Public value_Pair_ISRT As String

Type Trig_DLET
  idTb As Long
  NomeTb As String
  Where_DLET As String
End Type

Public PChild_DLET() As Trig_DLET
Public varPref As String ' Pu� essere ":new." o ":old.". Viene usata per i trigger

Sub InserisciTrigger_ORA(rtb As RichTextBox, IdTable As Long, nomeTable As String, alias As String)
  Dim rsTrigger As Recordset
  Dim strTrigger As String
  
  On Error GoTo ErrorHandler

  strTrigger = "Select * from DMORC_Trigger where IdTable=" & IdTable
  Set rsTrigger = m_fun.Open_Recordset(strTrigger)
    
  If Not rsTrigger.EOF Then
    DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & TipoDB & "\Sql\"
  
  
    rtb.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & TipoDB & "\CREATE_TRG"
  
    
    changeTag rtb, "<TRG-NAME>", nomeTable & "_TRG"
    
    changeTag rtb, "<EVENTO>", rsTrigger!Evento
  
    changeTag rtb, "<CAMPI>", rsTrigger!Campi
    
    changeTag rtb, "<TB-NAME>", nomeTable
    
    changeTag rtb, "<REFERENCING>", rsTrigger!Referencing
    
    changeTag rtb, "<CORPO>", rsTrigger!Corpo
    changeTag rtb, "<CONDIZIONE>", rsTrigger!Condizione
    
    changeTag rtb, "<AZIONE>", rsTrigger!Azione
    
    rtb.SaveFile DataMan.DmParam_OutField_Db & "TRG\" & nomeTable & "_TRG.sql", 1

  End If
  Exit Sub
ErrorHandler:
  
End Sub

Sub SaveTable()
  ' funzione per il salvataggio delle tabelle
  Dim TbTable As Recordset, TbApp As Recordset
  
  Select Case TipoDB
    Case "DB2"
      Set TbTable = m_fun.Open_Recordset("select * from DMDB2_Tabelle where IdTable= " & REL_Table.IdTable)
      If TbTable.RecordCount = 0 Then
        ' Aggiungo la tabella
        TbTable.AddNew
        Set TbApp = m_fun.Open_Recordset("select * from DMDB2_Tabelle order by IdTable")
        TbTable!IdTable = Get_First_Free_ID(TbApp, "IdTable")
        TbApp.Close
      End If
    Case "ORACLE"
      Set TbTable = m_fun.Open_Recordset("select * from DMORC_Tabelle where IdTable= " & REL_Table.IdTable)
      If TbTable.RecordCount = 0 Then
        ' Aggiungo la tabella
        TbTable.AddNew
        Set TbApp = m_fun.Open_Recordset("select * from DMORC_Tabelle order by IdTable")
        TbTable!IdTable = Get_First_Free_ID(TbApp, "IdTable")
        TbApp.Close
      End If
  End Select
        
  ' Faccio l' update sulla tabella se non � stata aggiunta
  
  TbTable!IdTableSpc = Trim(REL_Table.IdTableSpc)
  TbTable!Nome = Trim(UCase(REL_Table.Nome))
  '****************************************
  ' TbTable!Dimensionamento = Da Verificare
  '****************************************
  TbTable!Descrizione = Trim(UCase(REL_Table.Descrizione)) & " "
  TbTable!Data_Creazione = Now
  TbTable!Data_Modifica = Now
  'TbTable!NomeDB = Trim(UCase(NomeDatabaseCorrente))
  TbTable!idDB = IDDatabaseCorrente
  REL_Table.IdTable = TbTable!IdTable
  TbTable.Update
  TbTable.Close
  
End Sub

Sub Carica_Lista_TableSpace(Lista As ListView, TipoDB As String)
  ' Carica i Tablespaces disponibili in base al tipo di database scelto:
  ' ORC = ORACLE
  ' DB2 = DB2
  
  Dim Tbs As Recordset
  
  Select Case TipoDB
    Case "ORACLE"
      Set Tbs = m_fun.Open_Recordset("select * from DMORC_Tablespaces where Nomedb='" & Trim(NomeDatabaseCorrente) & "'")
    Case "DB2"
      Set Tbs = m_fun.Open_Recordset("select * from DMDB2_Tablespaces where Nomedb='" & Trim(NomeDatabaseCorrente) & "'")
      ' da implementare
  End Select
  
  If Tbs.RecordCount Then
    Lista.ListItems.Clear
    While Tbs.EOF = False
      Lista.ListItems.Add , , Tbs!Nome
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Tbs!Data_Creazione
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Tbs!Data_Modifica
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Tbs!NomeDB
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Tbs!DefaultStorage
      Tbs.MoveNext
    Wend
  End If
  Tbs.Close
End Sub

Public Function CaricaDatabase(IdOgg As Long, MyForm As Form) As Boolean
  CaricaDatabase = True
End Function

Function Get_First_Free_ID(Tabella As Recordset, NomeCampo As String) As Long
  On Error Resume Next
  If Tabella.RecordCount <> 0 Then
    If Not IsNull(Tabella.fields(0).Value) Then
      Tabella.MoveFirst
      Tabella.MoveLast
      Get_First_Free_ID = Tabella.fields(0).Value + 1
    Else
      Get_First_Free_ID = 1
    End If
  Else
    Get_First_Free_ID = 1
  End If
End Function

Public Sub AggiungiElemento(Db As Database, tb As Recordset, elemento As String, Optional indSeg As Integer, Optional IndFld As Integer, Optional indStr As Integer, Optional indCmp As Integer, Optional nomeStruttura As String, Optional IndTbs As Integer, Optional IndTab As Integer, Optional IndIndex As Integer)

  Select Case elemento
    Case "Segmento"
      ReDim Preserve Db.Segmenti(indSeg)
      Db.Segmenti(indSeg).Comprtn = tb!Comprtn
      Db.Segmenti(indSeg).TbName = tb!rdbms_tbname
      Db.Segmenti(indSeg).IdOggetto = tb!IdOggetto
      Db.Segmenti(indSeg).IdSegmento = tb!IdSegm
      Db.Segmenti(indSeg).MaxLen = tb!MaxLen
      Db.Segmenti(indSeg).MinLen = tb!MinLen
      Db.Segmenti(indSeg).Nome = tb!Nome
      Db.Segmenti(indSeg).Parent = tb!Parent
      Db.Segmenti(indSeg).Pointer = tb!Pointer
      Db.Segmenti(indSeg).RoutineIMS = tb!RoutineIMS
      Db.Segmenti(indSeg).Rules = tb!Rules
      Db.Segmenti(indSeg).Used = tb!Used
    
    Case "Field"
      ReDim Preserve Db.Segmenti(indSeg).fields(IndFld)
      Db.Segmenti(indSeg).fields(IndFld).DBDNome = tb!DBDNome
      Db.Segmenti(indSeg).fields(IndFld).IdField = tb!IdField
      Db.Segmenti(indSeg).fields(IndFld).IdSegm = tb!IdSegm
      Db.Segmenti(indSeg).fields(IndFld).MaxLen = tb!MaxLen
      Db.Segmenti(indSeg).fields(IndFld).NameIndPunSeq = tb!NameIndPunSeq
      Db.Segmenti(indSeg).fields(IndFld).Nome = tb!Nome
      Db.Segmenti(indSeg).fields(IndFld).NullVal = tb!NullVal
      Db.Segmenti(indSeg).fields(IndFld).Pointer = tb!Pointer
      Db.Segmenti(indSeg).fields(IndFld).segment = tb!segment
      Db.Segmenti(indSeg).fields(IndFld).Seq = tb!Seq
      Db.Segmenti(indSeg).fields(IndFld).Srch = tb!Srch
      Db.Segmenti(indSeg).fields(IndFld).Start = tb!Start
      Db.Segmenti(indSeg).fields(IndFld).SubSeq = tb!SubSeq
      Db.Segmenti(indSeg).fields(IndFld).Tipo = tb!type
      Db.Segmenti(indSeg).fields(IndFld).Unique = tb!Unique
      Db.Segmenti(indSeg).fields(IndFld).Xname = tb!Xname

    Case "Struttura"
      ReDim Preserve Db.Segmenti(indSeg).Strutture(indStr)
      'Db.Segmenti(IndSeg).Strutture(IndStr).IdOggetto = tb!IdOggetto
      Db.Segmenti(indSeg).Strutture(indStr).IdSegm = tb!IdSegm
      Db.Segmenti(indSeg).Strutture(indStr).IdStruttura = tb!IdStruttura
      Db.Segmenti(indSeg).Strutture(indStr).Nome = nomeStruttura
      'Db.Segmenti(IndSeg).Strutture(IndStr).Tipo = tb!Tipo

    Case "Campo"
      ReDim Preserve Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp)
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).Codice = tb!Codice
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).IdStruttura = tb!IdStruttura
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).Nome = tb!NomeCmp
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).Ordinale = tb!Ordinale
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).NomeRed = tb!NomeRed
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).livello = tb!livello
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).Tipo = tb!Tipo
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).Posizione = tb!Posizione
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).bytes = tb!bytes
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).Occurs = tb!Occurs
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).lunghezza = tb!lunghezza
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).Decimali = tb!Decimali
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).segno = Trim(tb!segno)
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).Usage = tb!Usage
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).val = tb!val
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).Picture = tb!Picture
      ' Mauro 15/01/2008
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).OccursFlat = tb!OccursFlat
      Db.Segmenti(indSeg).Strutture(indStr).Campi(indCmp).DaMigrare = tb!DaMigrare
     
    Case "TableSpace"
      ReDim Preserve Db.TableSpaces(IndTbs)
      Db.TableSpaces(IndTbs).IdOggetto = tb!IdOggetto
      Db.TableSpaces(IndTbs).IdTableSpace = tb!IdTableSpace
      Db.TableSpaces(IndTbs).Nome = tb!Nome
      
    Case "Table"
      ReDim Preserve Db.TableSpaces(IndTbs).Tavole(IndTab)
      Db.TableSpaces(IndTbs).Tavole(IndTab).IdTableSpace = tb!IdTableSpace
      Db.TableSpaces(IndTbs).Tavole(IndTab).IdTavola = tb!IdTavola
      Db.TableSpaces(IndTbs).Tavole(IndTab).Nome = tb!Nome
      Db.TableSpaces(IndTbs).Tavole(IndTab).Used = tb!Used
      
    Case "PKey"
      ReDim Preserve Db.TableSpaces(IndTbs).Tavole(IndTab).pkey(IndIndex)
      Db.TableSpaces(IndTbs).Tavole(IndTab).pkey(IndIndex).IdChiave = tb!IdChiave
      Db.TableSpaces(IndTbs).Tavole(IndTab).pkey(IndIndex).IdTavola = tb!IdTavola
      Db.TableSpaces(IndTbs).Tavole(IndTab).pkey(IndIndex).Nome = tb!Nome
      
    Case "SKey"
      ReDim Preserve Db.TableSpaces(IndTbs).Tavole(IndTab).SKey(IndIndex)
      Db.TableSpaces(IndTbs).Tavole(IndTab).SKey(IndIndex).IdChiave = tb!IdChiave
      Db.TableSpaces(IndTbs).Tavole(IndTab).SKey(IndIndex).IdTavola = tb!IdTavola
      Db.TableSpaces(IndTbs).Tavole(IndTab).SKey(IndIndex).Nome = tb!Nome
      
    Case Else
      ' Istruzione errata
      Stop
  
  End Select
End Sub

Public Function Esistenza_Tabella(nomeTabella As String) As Boolean
  Dim Tables As ADOX.Tables
  Dim IndTab As Integer
  
  Set Tables = DataMan.DmDatabase.Tables
  
  For IndTab = 0 To Tables.Count - 1
    If Tables(IndTab).Name = nomeTabella Then
      Esistenza_Tabella = True
      Exit Function
    End If
  Next IndTab
  
  Esistenza_Tabella = False
  
End Function

Sub SelectAction(Lista As ListView, Action As String)
   
  Dim IdSegmento As Long
  Dim IndTbs As Integer
  Dim IndTav As Integer
  Dim indCmp As Integer
  
  Select Case Action
    
'    Case "SaveSegments"
'       SalvaDatabase
    
    Case "AddSegments"
      IsArray (Db.Segmenti)
      Lista.ListItems.Add Lista.ListItems.Count + 1, , IdSegmento
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.IdOggetto
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add = "NEW"
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add = "0"
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add = "0"
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add = "None"
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add = "None"
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add = "None"
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add = "None"
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add = "None"
      Lista.Refresh
      ReDim Preserve Db.Segmenti(UBound(Db.Segmenti) + 1)
      Db.Segmenti(UBound(Db.Segmenti)).IdSegmento = AssegnaId("TBS")
      Db.Segmenti(UBound(Db.Segmenti)).IdOggetto = Db.IdOggetto
      Db.Segmenti(UBound(Db.Segmenti)).Nome = "NEW"
      Db.Segmenti(UBound(Db.Segmenti)).MaxLen = "0"
      Db.Segmenti(UBound(Db.Segmenti)).MinLen = "0"
      Db.Segmenti(UBound(Db.Segmenti)).Pointer = "None"
      Db.Segmenti(UBound(Db.Segmenti)).Parent = "None"
      Db.Segmenti(UBound(Db.Segmenti)).Comprtn = "None"
      Db.Segmenti(UBound(Db.Segmenti)).RoutineIMS = "None"
      Db.Segmenti(UBound(Db.Segmenti)).Rules = "None"
      Db.Segmenti(UBound(Db.Segmenti)).Used = True
      Db.Segmenti(UBound(Db.Segmenti)).Inserito = True
    
    Case "RemoveSegments"
      Lista.ListItems.Remove Lista.SelectedItem.Index
      Db.Segmenti(UBound(Db.Segmenti)).Used = False
    
    Case "SaveStructures"
      
    Case "AddStructures"
    
    Case "RemoveStructures"
    
    Case "AddTBS"
      ReDim Preserve Db.TableSpaces(UBound(Db.TableSpaces) + 1)
      ReDim Preserve Db.TableSpaces(UBound(Db.TableSpaces)).Tavole(0)
      Db.TableSpaces(UBound(Db.TableSpaces)).IdTableSpace = AssegnaId("TBS")
      Db.TableSpaces(UBound(Db.TableSpaces)).IdOggetto = Db.IdOggetto
      Db.TableSpaces(UBound(Db.TableSpaces)).Nome = "New"
      Db.TableSpaces(UBound(Db.TableSpaces)).Used = True
      
      Lista.ListItems.Add , , Db.TableSpaces(UBound(Db.TableSpaces)).IdTableSpace
      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add = "New"

    Case "AddField"
      IsArray (Db.TableSpaces)
      If UBound(Db.TableSpaces) <> -1 Then
        For IndTbs = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
            IsArray (Db.TableSpaces(IndTbs).Tavole)
            For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
              If Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola = IdTav Then
                IsArray (Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)
                ReDim Preserve Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi) + 1)
                ReDim Preserve Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(0)
                ReDim Preserve Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(0)
                Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).IdCampo = AssegnaId("Field", IndTbs, IndTav)
                Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).IdChiave = 0
                Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).Nome = "New"
                Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).Tipo = "None"
                Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).lunghezza = 0
                Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).Decimali = 0
                Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).segno = True
                Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).Used = True
                
                Lista.ListItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).IdCampo
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).Nome
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).Tipo
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).lunghezza
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).Decimali
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).segno
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , CStr(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)).Used)
                
                Exit Sub
              End If
            Next IndTav
          End If
        Next IndTbs
      Else
        MsgBox "You must create a Table...", vbExclamation, DataMan.DmNomeProdotto
      End If
      
    Case "RemoveField"
    
    Case "AddPKey"
      IsArray (Db.TableSpaces)
      If UBound(Db.TableSpaces) <> -1 Then
        For IndTbs = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
            IsArray (Db.TableSpaces(IndTbs).Tavole)
            For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
              If Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola = IdTav Then
                IsArray (Db.TableSpaces(IndTbs).Tavole(IndTav).pkey)
                ReDim Preserve Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(1)
                ReDim Preserve Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(1).Campi(0)
                Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(1).IdChiave = AssegnaId("PKey")
                Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(1).IdTavola = IdTav
                Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(1).Nome = "New"
                Lista.ListItems.Add 1, , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(1).IdChiave
                Lista.ListItems(1).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(1).Nome
                Lista.ListItems(1).ForeColor = vbRed
                Lista.ListItems(1).ListSubItems(1).ForeColor = vbRed
                Exit Sub
              End If
            Next IndTav
          End If
        Next IndTbs
      End If
      
      Case "AddSKey"
      IsArray (Db.TableSpaces)
      If UBound(Db.TableSpaces) <> -1 Then
        For IndTbs = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
            IsArray (Db.TableSpaces(IndTbs).Tavole)
            For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
              If Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola = IdTav Then
                IsArray (Db.TableSpaces(IndTbs).Tavole(IndTav).SKey)
                ReDim Preserve Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey) + 1)
                ReDim Preserve Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey) + 1).Campi(0)
                Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey)).IdChiave = AssegnaId("SKey", IndTbs, IndTav)
                Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey)).IdTavola = IdTav
                Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey)).Nome = "New"
                Lista.ListItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey)).IdChiave
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey)).Nome
                Lista.ListItems(Lista.ListItems.Count).ForeColor = vbBlue
                Lista.ListItems(Lista.ListItems.Count).ListSubItems(1).ForeColor = vbBlue
                Exit Sub
              End If
            Next IndTav
          End If
        Next IndTbs
      End If
      
  End Select
End Sub

Public Function AssegnaId(Tipo As String, Optional Ind1 As Integer, Optional Ind2 As Integer) As Long
  Dim Ind As Integer
  Dim id As Long
  Dim check As Boolean
  
  Select Case Tipo
    
    Case "DB"
      AssegnaId = 1
      
    Case "TBS"
      id = 1
      check = True
      While check = True
        For Ind = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(Ind).IdTableSpace <> id Then
            check = False
          Else
            check = True
            Exit For
          End If
        Next Ind
        If check = False Then
          AssegnaId = id
          Exit Function
        End If
        id = id + 1
      Wend
      
    Case "TAB"
      id = 1
      check = True
      While check = True
        For Ind = 1 To UBound(Db.TableSpaces(Ind1).Tavole)
          If Db.TableSpaces(Ind1).Tavole(Ind).IdTavola <> id Then
            check = False
          Else
            check = True
            Exit For
          End If
        Next Ind
        If check = False Then
          AssegnaId = id
          Exit Function
        End If
        id = id + 1
      Wend
      
    Case "Field"
      id = 1
      check = True
      While check = True
        For Ind = 1 To UBound(Db.TableSpaces(Ind1).Tavole(Ind2).Campi)
          If Db.TableSpaces(Ind1).Tavole(Ind2).Campi(Ind).IdCampo <> id Then
            check = False
          Else
            check = True
            Exit For
          End If
        Next Ind
        If check = False Then
          AssegnaId = id
          Exit Function
        End If
        id = id + 1
      Wend
      
    Case "PKey"
      AssegnaId = 1
    
    Case "SKey"
      id = 1
      check = True
      While check = True
        For Ind = 1 To UBound(Db.TableSpaces(Ind1).Tavole(Ind2).SKey)
          If Db.TableSpaces(Ind1).Tavole(Ind2).SKey(Ind).IdChiave <> id Then
            check = False
          Else
            check = True
            Exit For
          End If
        Next Ind
        If check = False Then
          AssegnaId = id
          Exit Function
        End If
        id = id + 1
      Wend
  End Select

End Function

Sub AggiornaStruttura(Tipo As String, Lista As ListView, Txt As TextBox, SegSelX As Long, SegSelY As Long, Optional idSeg As Long)
  Dim indSeg As Integer
  Dim IndFld As Integer
  Dim IndTbs As Integer
  Dim IndTav As Integer
  Dim indCmp As Integer
  Dim IndKey As Integer
  
  Select Case Tipo
    Case "Segmenti"
      For indSeg = 1 To UBound(Db.Segmenti)
        If CStr(Db.Segmenti(indSeg).IdSegmento) = Lista.SelectedItem.text Then
          Select Case Lista.ColumnHeaders(SegSelX + 1)
            Case "IdSegm"
              Txt.Visible = False
              MsgBox "Update not possible", vbExclamation, DataMan.DmNomeProdotto
              Exit Sub
            Case "IdOggetto"
              Txt.Visible = False
              MsgBox "Update not possible", vbExclamation, DataMan.DmNomeProdotto
              Exit Sub
            Case "Nome"
              Db.Segmenti(indSeg).Nome = UCase(Txt.text)
            Case "MaxLen"
              Db.Segmenti(indSeg).MaxLen = UCase(Txt.text)
            Case "MinLen"
              Db.Segmenti(indSeg).MinLen = UCase(Txt.text)
            Case "Parent"
              Db.Segmenti(indSeg).Parent = UCase(Txt.text)
            Case "Pointer"
              Db.Segmenti(indSeg).Pointer = UCase(Txt.text)
            Case "Rules"
              Db.Segmenti(indSeg).Rules = UCase(Txt.text)
            Case "Comprtn"
              Db.Segmenti(indSeg).Comprtn = UCase(Txt.text)
            Case "RoutineIMS"
              Db.Segmenti(indSeg).RoutineIMS = UCase(Txt.text)
          End Select
          Exit For
        End If
      Next indSeg
      
      If Trim(Txt.text) <> "" Then
        Lista.ListItems(SegSelY).SubItems(SegSelX) = UCase(Txt.text)
      End If
      Txt.Visible = False
      
    Case "Fields"
      For indSeg = 1 To UBound(Db.Segmenti)
        If Db.Segmenti(indSeg).IdSegmento = idSeg Then
          For IndFld = 1 To UBound(Db.Segmenti(indSeg).fields)
            If CStr(Db.Segmenti(indSeg).fields(IndFld).IdField) = Lista.SelectedItem.text Then
              Select Case Lista.ColumnHeaders(SegSelX + 1)
                Case "IdField"
                  Txt.Visible = False
                  MsgBox "Update not possible", vbExclamation, DataMan.DmNomeProdotto
                  Exit Sub
                Case "IdSegm"
                  Txt.Visible = False
                  MsgBox "Update not possible", vbExclamation, DataMan.DmNomeProdotto
                  Exit Sub
                Case "Nome"
                  Db.Segmenti(indSeg).fields(IndFld).Nome = UCase(Txt.text)
                Case "Start"
                  Db.Segmenti(indSeg).fields(IndFld).Start = UCase(Txt.text)
                Case "Length"
                  Db.Segmenti(indSeg).fields(IndFld).MaxLen = UCase(Txt.text)
                Case "Seq."
                  Db.Segmenti(indSeg).fields(IndFld).Seq = UCase(Txt.text)
                Case "Unique"
                  Db.Segmenti(indSeg).fields(IndFld).Unique = UCase(Txt.text)
              End Select
              Exit For
            End If
          Next IndFld
          Exit For
        End If
      Next indSeg
      
      Case "Tablespace"
        For IndTbs = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
            Select Case Lista.ColumnHeaders(SegSelX + 1)
              Case "Id Tablespace"
                Txt.Visible = False
                MsgBox "Update not possible", vbExclamation, DataMan.DmNomeProdotto
                Exit Sub
              Case "Name"
                Db.TableSpaces(IndTbs).Nome = UCase(Txt.text)
            End Select
            Exit For
          End If
        Next IndTbs
      
      Case "Tavole"
        For IndTbs = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
            For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
              If Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola = IdTav Then
                Select Case Lista.ColumnHeaders(SegSelX + 1)
                  Case "IdTable"
                    Txt.Visible = False
                    MsgBox "Update not possible", vbExclamation, DataMan.DmNomeProdotto
                    Exit Sub
                  Case "Name"
                    Db.TableSpaces(IndTbs).Tavole(IndTav).Nome = UCase(Txt.text)
                  Case "Used"
                    If UCase(Trim(Txt.text)) = "TRUE" Or UCase(Trim(Txt.text)) = "FALSE" Then
                      Db.TableSpaces(IndTbs).Tavole(IndTav).Used = CBool(UCase(Txt.text))
                    Else
                      MsgBox "Update not possible, value not accepted!!!", vbExclamation, DataMan.DmNomeProdotto
                      Txt.Visible = False
                      Exit Sub
                    End If
                End Select
              End If
            Next IndTav
            Exit For
          End If
        Next IndTbs
        
      Case "FieldDB2"
        For IndTbs = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
            For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
              If Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola = IdTav Then
                For indCmp = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)
                  If Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).IdCampo = IdCmp Then
                    Select Case Lista.ColumnHeaders(SegSelX + 1)
                      Case "IdField"
                        Txt.Visible = False
                        MsgBox "Update not possible", vbExclamation, DataMan.DmNomeProdotto
                        Exit Sub
                      Case "Name"
                        Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Nome = UCase(Txt.text)
                      Case "Type"
                        Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Tipo = UCase(Txt.text)
                      Case "Lenght"
                        Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).lunghezza = CInt(UCase(Txt.text))
                      Case "Decimals"
                        Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Decimali = CInt(UCase(Txt.text))
                      Case "Signed"
                        Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).segno = CBool((Txt.text))
                      Case "Used"
                      If UCase(Trim(Txt.text)) = "TRUE" Or UCase(Trim(Txt.text)) = "FALSE" Then
                        Db.TableSpaces(IndTbs).Tavole(IndTav).Used = CBool(UCase(Txt.text))
                      Else
                        MsgBox "Update not possible, value not accepted!!!", vbExclamation, DataMan.DmNomeProdotto
                        Txt.Visible = False
                        Exit Sub
                      End If
                      Exit For
                End Select
                Exit For
                  End If
                Next indCmp
              End If
            Next IndTav
          End If
        Next IndTbs
        
      Case "Key"
        For IndTbs = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
            For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
              If Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola = IdTav Then
                If Txt.Tag = "PKey" Then
                  For IndKey = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).pkey)
                    If Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).IdChiave = IdKey Then
                      Select Case Lista.ColumnHeaders(SegSelX + 1)
                        Case "IdKey"
                          Txt.Visible = False
                          MsgBox "Update not possible", vbExclamation, DataMan.DmNomeProdotto
                          Exit Sub
                        Case "Name"
                          Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Nome = UCase(Txt.text)
                        Exit For
                      End Select
                      Exit For
                    End If
                  Next IndKey
                Else
                  For IndKey = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey)
                    If Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).IdChiave = IdKey Then
                      Select Case Lista.ColumnHeaders(SegSelX + 1)
                        Case "IdKey"
                          Txt.Visible = False
                          MsgBox "Update not possible", vbExclamation, DataMan.DmNomeProdotto
                          Exit Sub
                        Case "Name"
                          Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).Nome = UCase(Txt.text)
                        Exit For
                      End Select
                      Exit For
                    End If
                  Next IndKey
                End If
              End If
            Next IndTav
          End If
        Next IndTbs
  End Select
  
  If Trim(Txt.text) <> "" Then
    Lista.ListItems(SegSelY).SubItems(SegSelX) = UCase(Txt.text)
  End If
  If Txt.Tag = "PKey" Then Lista.ListItems(SegSelY).ListSubItems(1).ForeColor = vbRed
  If Txt.Tag = "SKey" Then Lista.ListItems(SegSelY).ListSubItems(1).ForeColor = vbBlue
  Txt.Visible = False
End Sub

Sub PopolaLista(Tipo As String, Lista As ListView, Optional id As Long)
  Dim indSeg As Integer, IndFld As Integer, indStr As Integer, IndTbs As Integer
  Dim IndTav As Integer, indCmp As Integer, IndKey As Integer
  
  Select Case Tipo
    Case "Segmenti"
      For indSeg = 1 To UBound(Db.Segmenti)
        Lista.ListItems.Add , , Db.Segmenti(indSeg).IdSegmento
        Lista.ListItems(Lista.ListItems.Count).SubItems(1) = Db.Segmenti(indSeg).IdOggetto
        Lista.ListItems(Lista.ListItems.Count).SubItems(2) = Db.Segmenti(indSeg).Nome
        Lista.ListItems(Lista.ListItems.Count).SubItems(3) = Db.Segmenti(indSeg).MaxLen
        Lista.ListItems(Lista.ListItems.Count).SubItems(4) = Db.Segmenti(indSeg).MinLen
        Lista.ListItems(Lista.ListItems.Count).SubItems(5) = Db.Segmenti(indSeg).Parent
        Lista.ListItems(Lista.ListItems.Count).SubItems(6) = Db.Segmenti(indSeg).Pointer
        Lista.ListItems(Lista.ListItems.Count).SubItems(7) = Db.Segmenti(indSeg).Rules
        Lista.ListItems(Lista.ListItems.Count).SubItems(8) = Db.Segmenti(indSeg).Comprtn
        Lista.ListItems(Lista.ListItems.Count).SubItems(9) = Db.Segmenti(indSeg).RoutineIMS
      Next indSeg
      
    Case "Fields"
      Lista.ListItems.Clear
      For indSeg = 1 To UBound(Db.Segmenti)
        If Db.Segmenti(indSeg).IdSegmento = id Then
          If IsArray(Db.Segmenti(indSeg).fields) Then
            For IndFld = 1 To UBound(Db.Segmenti(indSeg).fields)
              Lista.ListItems.Add , , Db.Segmenti(indSeg).fields(IndFld).IdField
              Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.Segmenti(indSeg).fields(IndFld).IdSegm
              Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.Segmenti(indSeg).fields(IndFld).Nome
              Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.Segmenti(indSeg).fields(IndFld).Start
              Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.Segmenti(indSeg).fields(IndFld).MaxLen
              Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.Segmenti(indSeg).fields(IndFld).Seq
              Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.Segmenti(indSeg).fields(IndFld).Unique
            Next IndFld
            Exit For
          End If
        End If
      Next indSeg
      
    Case "Strutture"
      Lista.ListItems.Clear
      For indSeg = 1 To UBound(Db.Segmenti)
        If Db.Segmenti(indSeg).IdSegmento = id Then
          For indStr = 1 To UBound(Db.Segmenti(indSeg).Strutture)
            Lista.ListItems.Add , , Db.Segmenti(indSeg).Strutture(indStr).IdStruttura
            Lista.ListItems(Lista.ListItems.Count).SubItems(1) = Db.Segmenti(indSeg).Strutture(indStr).IdSegm
            Lista.ListItems(Lista.ListItems.Count).SubItems(2) = Db.Segmenti(indSeg).Strutture(indStr).Nome
          Next indStr
          Exit For
        End If
      Next indSeg
        
    Case "Tablespaces"
      For IndTbs = 1 To UBound(Db.TableSpaces)
       If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
       
       End If
      Next IndTbs
      
    Case "Tables"
      Lista.ListItems.Clear
      For IndTbs = 1 To UBound(Db.TableSpaces)
       If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
         IsArray (Db.TableSpaces(IndTbs).Tavole)
         For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
           Lista.ListItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola
           Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Nome
           If CStr(Db.TableSpaces(IndTbs).Tavole(IndTav).Used) = "Vero" Then
             Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , "TRUE"
           Else
             Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , "FALSE"
           End If
         Next IndTav
       End If
      Next IndTbs
      
    Case "FieldsDB2"
      Lista.ListItems.Clear
      IsArray (Db.TableSpaces)
      For IndTbs = 1 To UBound(Db.TableSpaces)
        If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
          IsArray (Db.TableSpaces(IndTbs).Tavole)
          For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
            If Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola = IdTav Then
              IsArray (Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)
              For indCmp = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)
                Lista.ListItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).IdCampo
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Nome
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Tipo
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).lunghezza
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Decimali
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).segno
                Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Used
              Next indCmp
            End If
          Next IndTav
        End If
      Next IndTbs
      
      Case "Key"
        Lista.ListItems.Clear
        IsArray (Db.TableSpaces)
        For IndTbs = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
            IsArray (Db.TableSpaces(IndTbs).Tavole)
            For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
              If Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola = IdTav Then
                IsArray (Db.TableSpaces(IndTbs).Tavole(IndTav).pkey)
                If UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).pkey) <> -1 Then
                  For IndKey = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).pkey)
                    Lista.ListItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).IdChiave
                    Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Nome
                    Lista.ListItems(Lista.ListItems.Count).ForeColor = vbRed
                  Next IndKey
                End If
                IsArray (Db.TableSpaces(IndTbs).Tavole(IndTav).SKey)
                If UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey) <> -1 Then
                  For IndKey = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey)
                    Lista.ListItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).IdChiave
                    Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).Nome
                    Lista.ListItems(Lista.ListItems.Count).ForeColor = vbBlue
                  Next IndKey
                End If
              End If
            Next IndTav
          End If
        Next IndTbs
        
      Case "FieldDispo"
        Lista.ListItems.Clear
        IsArray (Db.TableSpaces)
        For IndTbs = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
            IsArray (Db.TableSpaces(IndTbs).Tavole)
            For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
              If Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola = IdTav Then
                IsArray (Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)
                For indCmp = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).Campi)
                  Lista.ListItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).IdCampo
                  Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Nome
                  Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Tipo
                  Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).lunghezza
                  Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Decimali
                  Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).segno
                  Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).Campi(indCmp).Used
                Next indCmp
              End If
            Next IndTav
          End If
        Next IndTbs
        
      Case "FieldKey"
        Lista.ListItems.Clear
        For IndTbs = 1 To UBound(Db.TableSpaces)
          If Db.TableSpaces(IndTbs).IdTableSpace = IdTbs Then
            For IndTav = 1 To UBound(Db.TableSpaces(IndTbs).Tavole)
              If Db.TableSpaces(IndTbs).Tavole(IndTav).IdTavola = IdTav Then
                For IndKey = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).pkey)
                  If Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).IdChiave = IdKey Then
                    For indCmp = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Campi)
                      Lista.ListItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Campi(indCmp).IdChiave
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Campi(indCmp).Nome
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Campi(indCmp).Tipo
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Campi(indCmp).lunghezza
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Campi(indCmp).Decimali
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Campi(indCmp).segno
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Campi(indCmp).Used
                    Next indCmp
                    Exit Sub
                  End If
                Next IndKey
                For IndKey = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey)
                  If Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).IdChiave = IdKey Then
                    IsArray (Db.TableSpaces(IndTbs).Tavole(IndTav).pkey(IndKey).Campi)
                    For indCmp = 1 To UBound(Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).Campi)
                      Lista.ListItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).Campi(indCmp).IdChiave
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).Campi(indCmp).Nome
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).Campi(indCmp).Tipo
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).Campi(indCmp).lunghezza
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).Campi(indCmp).Decimali
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).Campi(indCmp).segno
                      Lista.ListItems(Lista.ListItems.Count).ListSubItems.Add , , Db.TableSpaces(IndTbs).Tavole(IndTav).SKey(IndKey).Campi(indCmp).Used
                    Next indCmp
                    Exit Sub
                  End If
                Next IndKey
              End If
            Next IndTav
          End If
        Next IndTbs
  End Select
  
End Sub

Sub Check_Lista(Lista As ListView)
  Dim INDCOL As Integer
  Dim LenCols As Long
  
  LenCols = 0
  
  For INDCOL = 1 To Lista.ColumnHeaders.Count
    LenCols = LenCols + Lista.ColumnHeaders(INDCOL).Width
  Next INDCOL
  
  If LenCols > Lista.Width Then
    For INDCOL = 1 To Lista.ColumnHeaders.Count
      Lista.ColumnHeaders(INDCOL).Width = (Lista.Width - 80) / Lista.ColumnHeaders.Count
    Next INDCOL
  End If
  
End Sub

Sub SaveColonne()
  Dim TbColonne As Recordset
  Dim INDCOL As Integer
  Dim MaxCol As Variant
  
  Select Case TipoDB
    Case "DB2"
      Set TbColonne = m_fun.Open_Recordset("select idcolonna from dmdb2_colonne order by idcolonna")
      If TbColonne.RecordCount > 0 Then
         TbColonne.MoveLast
         MaxCol = TbColonne!IdColonna
      Else
         MaxCol = 0
      End If
      Set TbColonne = m_fun.Open_Recordset("select * from DMDB2_Colonne where IdTable =" & REL_Table.IdTable)
      If TbColonne.RecordCount <> 0 Then
        TbColonne.MoveLast
        TbColonne.MoveFirst
        While TbColonne.EOF = False
          TbColonne.Delete adAffectCurrent
          TbColonne.MoveNext
        Wend
      End If
    Case "ORACLE"
      Set TbColonne = m_fun.Open_Recordset("select idcolonna from dmdb2_colonne order by idcolonna")
      If TbColonne.RecordCount > 0 Then
         TbColonne.MoveLast
         MaxCol = TbColonne!IdColonna
      Else
         MaxCol = 0
      End If
      Set TbColonne = m_fun.Open_Recordset("select * from DMORC_Colonne where IdTable =" & REL_Table.IdTable)
      If TbColonne.RecordCount <> 0 Then
        TbColonne.MoveFirst
        While TbColonne.EOF = False
          TbColonne.Delete adAffectCurrent
          TbColonne.MoveNext
        Wend
      End If
  End Select
  
  For INDCOL = 1 To UBound(REL_Table.Colonne)
    If Trim(REL_Table.Colonne(INDCOL).Nome) <> "" Then
      TbColonne.AddNew
      If val(REL_Table.Colonne(INDCOL).IdColonna) > 0 Then
        TbColonne!IdColonna = Trim(UCase(REL_Table.Colonne(INDCOL).IdColonna)) & ""
      Else
        MaxCol = MaxCol + 1
        TbColonne!IdColonna = MaxCol
      End If
      TbColonne!IdTable = Trim(UCase(REL_Table.IdTable)) & ""
      TbColonne!idTableJoin = Trim(UCase(REL_Table.Colonne(INDCOL).TableJoin)) & ""
      TbColonne!RoutLoad = Trim(UCase(REL_Table.Colonne(INDCOL).RoutLoad)) & ""
      TbColonne!RoutRead = Trim(UCase(REL_Table.Colonne(INDCOL).RoutRead)) & ""
      TbColonne!RoutWrite = Trim(UCase(REL_Table.Colonne(INDCOL).RoutWrite)) & ""
      TbColonne!RoutKey = Trim(UCase(REL_Table.Colonne(INDCOL).RoutKey)) & ""
      If val(REL_Table.Colonne(INDCOL).IdCmp) > 0 Then
        TbColonne!IdCmp = Trim(UCase(REL_Table.Colonne(INDCOL).IdCmp)) & ""
      End If
      TbColonne!Ordinale = Trim(UCase(REL_Table.Colonne(INDCOL).Ordinale)) & ""
      TbColonne!Nome = Trim(UCase(REL_Table.Colonne(INDCOL).Nome)) & ""
      TbColonne!Tipo = Trim(UCase(REL_Table.Colonne(INDCOL).Tipo)) & ""
      TbColonne!lunghezza = Trim(UCase(REL_Table.Colonne(INDCOL).lunghezza)) & ""
      TbColonne!Decimali = Trim(UCase(REL_Table.Colonne(INDCOL).Decimali)) & ""
      TbColonne!Formato = Trim(UCase(REL_Table.Colonne(INDCOL).Formato)) & " "
      TbColonne!Etichetta = Trim(UCase(REL_Table.Colonne(INDCOL).Etichetta)) & " "
      TbColonne!Null = REL_Table.Colonne(INDCOL).Null
      TbColonne!Descrizione = Trim(UCase(REL_Table.Colonne(INDCOL).Descrizione)) & " "
      If TipoDB = "ORACLE" Then TbColonne!VincoloVal = Trim(UCase(REL_Table.Colonne(INDCOL).VincoloVal)) & " "
      TbColonne.Update
    End If
  Next INDCOL
  TbColonne.Close
End Sub

Function CheckItemClick(MyLista As ListView, TxtApp As TextBox, cmb As ComboBox) As Boolean
  On Error GoTo ERR
  
  If MyLista.SelectedItem.Index <> 1 Then
    If Trim(MyLista.ListItems(MyLista.SelectedItem.Index - 1).ListSubItems(1).text) = "" Then
      cmb.Visible = False
      TxtApp.Visible = False
      'MsgBox "<Add action> is not possible!!!", vbExclamation, DataMan.DMNomeProdotto
      CheckItemClick = False
      Exit Function
    ElseIf Trim(MyLista.SelectedItem.ListSubItems(1).text) = "" And (ItemX = 2 Or ItemX = 3) Then
      cmb.Visible = False
      TxtApp.Visible = False
      'MsgBox "<Add action> is not possible!!!", vbExclamation, DataMan.DMNomeProdotto
      CheckItemClick = False
      Exit Function
    End If
  End If
  CheckItemClick = True
  Exit Function
ERR:
  If ERR.Number = 91 Then
  End If
End Function

Sub CaricaIndex(MyLista As ListView)
  Dim Ind As Integer
  Dim INDCOL As Integer
  Dim Nome As String
  Dim TbIndex As Recordset
  Dim TbIndexXCol As Recordset
  Dim TbColonne As Recordset
  
  ReDim REL_index(0)
  
  ' Carico gli indici relativi alla tabella selezionata
  Select Case TipoDB
    Case "DB2"
      Set TbIndex = m_fun.Open_Recordset("select * from DMDB2_Index where IdTable= " & REL_Table.IdTable & " order by IdIndex")
    Case "ORACLE"
      Set TbIndex = m_fun.Open_Recordset("select * from DMORC_Index where IdTable= " & REL_Table.IdTable & " order by IdIndex")
  End Select
  
  If TbIndex.RecordCount <> 0 Then
    TbIndex.MoveFirst
    While TbIndex.EOF = False
      ReDim Preserve REL_index(UBound(REL_index) + 1)
      Ind = UBound(REL_index)
      REL_index(Ind).DataC = TbIndex!Data_Creazione
      REL_index(Ind).DataM = TbIndex!Data_Modifica
      REL_index(Ind).Descrizione = TbIndex!Descrizione
      REL_index(Ind).IdIndex = TbIndex!IdIndex
      REL_index(Ind).IdTable = TbIndex!IdTable
      REL_index(Ind).IdTableSpc = TbIndex!IdTableSpc
      REL_index(Ind).NomeKey = TbIndex!Nome
      REL_index(Ind).Tipo = TbIndex!Tipo
      Select Case Trim(UCase(TipoDB))
        Case "DB2"
          If Trim(UCase(REL_index(Ind).Tipo)) = "CLUSTERING" Then
            REL_index(Ind).Unique = True
          Else
            If TbIndex!Unique = True Then
              REL_index(Ind).Unique = True
            Else
              REL_index(Ind).Unique = False
            End If
          End If
        Case "ORACLE"
      End Select
      'scrive le colonne che fanno perte dei vari indici
      ReDim REL_index(Ind).Colonne(0)
      Select Case TipoDB
        Case "DB2"
          Set TbIndexXCol = m_fun.Open_Recordset("SELECT * FROM DMDB2_IDXCOL WHERE IDINDEX= " & TbIndex!IdIndex & " ORDER BY IDINDEX,ORDINALE")
        Case "ORACLE"
          Set TbIndexXCol = m_fun.Open_Recordset("SELECT * FROM DMORC_IDXCOL WHERE IDINDEX= " & TbIndex!IdIndex & " ORDER BY IDINDEX,ORDINALE")
      End Select
      If TbIndexXCol.RecordCount <> 0 Then
        TbIndexXCol.MoveFirst
        While TbIndexXCol.EOF = False
          ReDim Preserve REL_index(Ind).Colonne(UBound(REL_index(Ind).Colonne) + 1)
          INDCOL = UBound(REL_index(Ind).Colonne)
          REL_index(Ind).Colonne(INDCOL).IdColonna = TbIndexXCol!IdColonna
          Select Case TipoDB
            Case "DB2"
              Set TbColonne = m_fun.Open_Recordset("SELECT * FROM DMDB2_COLONNE WHERE IDCOLONNA= " & TbIndexXCol!IdColonna & " and IdTable= " & TbIndexXCol!IdTable)
            Case "ORACLE"
              Set TbColonne = m_fun.Open_Recordset("SELECT * FROM DMORC_COLONNE WHERE IDCOLONNA= " & TbIndexXCol!IdColonna & " and IdTable= " & TbIndexXCol!IdTable)
          End Select
          If TbColonne.RecordCount <> 0 Then
            REL_index(Ind).Colonne(INDCOL).Nome = TbColonne!Nome
            REL_index(Ind).Colonne(INDCOL).Tipo = TbColonne!Tipo
          End If
          REL_index(Ind).Colonne(INDCOL).Order = TbIndexXCol!Order & ""
          REL_index(Ind).Colonne(INDCOL).Ordinale = INDCOL
          TbIndexXCol.MoveNext
        Wend
      End If
      TbIndex.MoveNext
    Wend
  End If
  
  MyLista.ListItems.Clear
  Nome = ""
  For Ind = 1 To UBound(REL_index)
    If Nome <> Trim(REL_index(Ind).NomeKey) Then
      MyLista.ListItems.Add
      MyLista.ListItems(MyLista.ListItems.Count).Tag = REL_index(Ind).IdIndex
      MyLista.ListItems(MyLista.ListItems.Count).ListSubItems.Add , , REL_index(Ind).NomeKey
      MyLista.ListItems(MyLista.ListItems.Count).ListSubItems.Add , , REL_index(Ind).Tipo
      Select Case Trim(UCase(TipoDB))
        Case "DB2"
          If REL_index(Ind).Tipo = "CLUSTERING" Then
            MyLista.ListItems(MyLista.ListItems.Count).SmallIcon = 8
          ElseIf REL_index(Ind).Tipo = "PRIMARY-KEY" Then
            MyLista.ListItems(MyLista.ListItems.Count).SmallIcon = 13
          ElseIf REL_index(Ind).Tipo = "KEY" Then
            If REL_index(Ind).Unique = False Then
              MyLista.ListItems(MyLista.ListItems.Count).SmallIcon = 7
            Else
              MyLista.ListItems(MyLista.ListItems.Count).SmallIcon = 12
            End If
          ElseIf REL_index(Ind).Tipo = "" Then
            MyLista.ListItems(MyLista.ListItems.Count).SmallIcon = 0
          End If
        Case "ORACLE"
          If REL_index(Ind).Tipo = "PK" Then
            MyLista.ListItems(MyLista.ListItems.Count).SmallIcon = 8
          ElseIf REL_index(Ind).Tipo = "SK" Then
            MyLista.ListItems(MyLista.ListItems.Count).SmallIcon = 7
          ElseIf REL_index(Ind).Tipo = "" Then
            MyLista.ListItems(MyLista.ListItems.Count).SmallIcon = 0
          End If
      End Select
      Nome = Trim(REL_index(Ind).NomeKey)
    End If
  Next Ind
End Sub

Sub Carica_Col_Indice(MyLista As ListView, Index As Integer)
  Dim Ind As Integer
  Dim cont As Integer
  
  MyLista.ListItems.Clear
  
  For Ind = 1 To UBound(REL_index(Index).Colonne)
    MyLista.ListItems.Add , , REL_index(Index).Colonne(Ind).Nome
    MyLista.ListItems(MyLista.ListItems.Count).ListSubItems.Add , , REL_index(Index).Colonne(Ind).Tipo
    MyLista.ListItems(MyLista.ListItems.Count).ListSubItems.Add , , REL_index(Index).Colonne(Ind).Order
  Next Ind
  
  MyLista.Refresh
End Sub

Public Sub Declare_TableORA(RtbFile As RichTextBox)
  Dim TbTable As Recordset, rs As Recordset
  Dim Creator As String
  Dim wDbName As String
  Dim wNomeStg As String
  Dim RecSeg As Recordset
  Dim rsAlias As Recordset
  Dim strAlias As String
  Dim NomeFile As String
  Dim wIdDB As Double
  Dim TbLogicSeg As Recordset
  Dim TbDBD As Recordset, TbSeg As Recordset
  Dim tbTabella As Recordset
  Dim idTables() As Trig_DLET
  
  On Error GoTo ErrH
  
  'VC (SQ):
  Dim alterTableStatements As String
  
  'Chiedere se posso andare con id database piuttosto che con nome db
  
  Set TbTable = m_fun.Open_Recordset("select * from " & PrefDb & "_db where nome = '" & pNome_DBCorrente & "'")
  
  If TbTable.RecordCount > 0 Then
    wIdDB = TbTable!idDB
  End If

  RtbFile.text = ""

''''  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & DbType & "\Sql\SQL\"

  Set TbTable = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & wIdDB)
  While Not TbTable.EOF
    
    DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Sql\SQL\"
  
    'ricava l'alias della tabella
    Set rsAlias = m_fun.Open_Recordset("Select * from " & PrefDb & "_Alias where idTable=" & TbTable!IdTable)
    strAlias = rsAlias!Nome
  
    ' Mauro 02-05-2007 : Nuova gestione per Viste di DB Logici
    Set TbLogicSeg = m_fun.Open_Recordset("select * from psdli_Segmenti_Source where idsegmento = " & TbTable!IdOrigine)
    If TbLogicSeg.RecordCount = 0 Then
  
      'VC (SQ): per prendere il template modificato ad hoc
      'rtbFile.LoadFile DataMan.DmParam_PercSQLExec
      'rtbFile.LoadFile m_fun.FnPathDB & "\Input-Prj\CPY_BASE\" & DbType & "\DEC_BASE_EXEC.sql"
      RtbFile.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\CREATE_TABLE"
      
      Creator = LeggiParam("DM_CREATOR")
      If Trim(Creator) = "" Then Creator = "%CREATOR%"
      'Controlla se c'� un creator definito
      If Trim(TbTable!Creator) <> "" Then
         'Controllare anche sugli alias
         Creator = UCase(Trim(TbTable!Creator))
      End If
      
      'Mauro 09/01/2007
      '''''''''''''''
      ''MG Inserito direttamente nel template
      ''''changeTag rtbFile, "<ALTR-SESS>", "'DD.MM.YYYY'"
      '''''''''''''''
      If Trim(Creator) = "BLANK" Or Trim(Creator) = "BLANKS" Then
        changeTag RtbFile, "<CRT-NAME>.", " "
      Else
        changeTag RtbFile, "<CRT-NAME>.", Creator
      End If
      changeTag RtbFile, "<CREATOR>", IIf(Len(Trim(Creator)), Creator & ".", "")
      changeTag RtbFile, "<NOME-TABELLA>", TbTable!Nome
      changeTag RtbFile, "<TB-DECLARE>", TbTable!Nome
  
      'commento sulla tabella
      If Len(TbTable!LabelOn) > 0 Then
      changeTag RtbFile, "<TB-COMMENTO>", "COMMENT ON TABLE " & TbTable!Nome & " IS '" & TbTable!LabelOn & "';"
      Else
        changeTag RtbFile, "<TB-COMMENTO>", ""
      End If
      
      Set rs = m_fun.Open_Recordset("select * from " & PrefDb & "_tableSpaces where IdTableSpc = " & TbTable!IdTableSpc)
      If rs.RecordCount > 0 Then
         changeTag RtbFile, "<TBS-NAME>", rs!Nome
         NomeTBS = rs!Nome
      End If
      rs.Close
      Set rs = m_fun.Open_Recordset("select * from " & PrefDb & "_DB where Iddb = " & TbTable!idDB)
      If rs.RecordCount > 0 Then
        changeTag RtbFile, "<DB-NAME>", rs!Nome
        wDbName = rs!Nome
        wNomeStg = rs!Nome & "STG"
      Else
        'Gestire?!
        wDbName = ""
        wNomeStg = ""
      End If
      rs.Close
      
      changeTag RtbFile, "<STG-NAME>", wNomeStg
  
      InsertDichiarativa RtbFile, TbTable, strAlias
  
      'ricarica il recordset x i commenti sui campi....
      Dim i As Integer
      Set rs = m_fun.Open_Recordset("Select Nome, LabelOn from " & PrefDb & "_Colonne where IdTable=" & TbTable!IdTable)
      i = 1
      If Not rs.EOF Then
        While Not rs.EOF
          If Len(rs!LabelOn) > 0 Then
            changeTag RtbFile, "<CAMPI-COMMENTO(i)>", "COMMENT ON COLUMN " & TbTable!Nome & "." & rs!Nome & " IS '" & rs!LabelOn & "';"
            i = i + 1
          End If
          rs.MoveNext
          
        Wend
        
        changeTag RtbFile, "<CAMPI-COMMENTO(i)>", ""
      End If
  
      NomeFile = TbTable!Nome
  
      'Salva il file
      RtbFile.SaveFile DataMan.DmParam_OutField_Db & NomeFile & ".sql", 1
  
      InsertCheckConstraint RtbFile, TbTable!IdTable, strAlias, NomeFile
  
      InserisciKeyDef_ORA RtbFile, TbTable(0), TbTable(1), 1, Creator, wDbName, nomeDbd, alterTableStatements, strAlias
      InserisciKeyDef_ORA RtbFile, TbTable(0), TbTable(1), 3, Creator, wDbName, nomeDbd, alterTableStatements, strAlias
      InserisciKeyDef_ORA RtbFile, TbTable(0), TbTable(1), 4, Creator, wDbName, nomeDbd, alterTableStatements, strAlias
      
      InserisciTrigger_ORA RtbFile, TbTable(0), TbTable(1), strAlias
  
    Else
      If TbLogicSeg.RecordCount = 1 Then
        ' Viste singole per DB Logici
        RtbFile.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\CREATE_VIEW"
      Else
        ' Viste concatenate per DB Logici
        RtbFile.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\CREATE_VIEW_JOIN"
      End If
      Creator = LeggiParam("DM_CREATOR")
      If Trim(Creator) = "" Then Creator = "%CREATOR%"
      'Controlla se c'� un creator definito
      If Trim(TbTable!Creator) <> "" Then
         'Controllare anche sugli alias
         Creator = UCase(Trim(TbTable!Creator))
      End If
      changeTag RtbFile, "<CREATOR>", IIf(Len(Trim(Creator)), Creator & ".", "")
      changeTag RtbFile, "<NOME-VIEW>", TbTable!Nome
      
      i = 1
      Do Until TbLogicSeg.EOF
        Set TbDBD = m_fun.Open_Recordset("select idoggetto from bs_oggetti where nome = '" & TbLogicSeg!DBD & "'")
        If Not TbDBD.EOF Then
          Set TbSeg = m_fun.Open_Recordset("select idsegmento from psdli_segmenti where " & _
                                           "idoggetto = " & TbDBD!IdOggetto & " and " & _
                                           "nome = '" & TbLogicSeg!Segmento & "'")
          If Not TbSeg.EOF Then
            Set tbTabella = m_fun.Open_Recordset("select nome, idtable from " & PrefDb & "_tabelle where idorigine = " & TbSeg!IdSegmento)
            If Not tbTabella.EOF Then
              changeTag RtbFile, "<NOME-TABLE" & i & ">", tbTabella!Nome
              
              changeTag RtbFile, "<CAMPI-TABLE" & i & ">", getCampiTable(tbTabella!IdTable, tbTabella!Nome, Creator, i)
            End If
            ReDim Preserve idTables(i)
            idTables(i).idTb = tbTabella!IdTable
            idTables(i).NomeTb = tbTabella!Nome
            tbTabella.Close
          End If
          TbSeg.Close
        End If
        TbDBD.Close
        TbLogicSeg.MoveNext
        i = i + 1
      Loop
      
      If UBound(idTables) = 2 Then
        changeTag RtbFile, "<WHERE-COND-VIEW>", getWhereJOIN(idTables(1).idTb, idTables(2).idTb, idTables(1).NomeTb, idTables(2).NomeTb, Creator)
      End If
      RtbFile.SaveFile DataMan.DmParam_OutField_Db & "V_" & TbTable!Nome & ".sql", 1
    End If
    TbLogicSeg.Close
    
    TbTable.MoveNext
  Wend
  TbTable.Close
  
  Exit Sub
ErrH:
  If ERR.Number = 75 Then
    MkDir DataMan.DmParam_OutField_Db
    RtbFile.SaveFile DataMan.DmParam_OutField_Db & NomeFile & ".sql", 1
    Resume Next
  End If
End Sub

Public Function getCampiTable(IdTable As Long, tableName As String, ByVal Creator As String, i As Integer) As String
  Dim TbColonne As Recordset
  
  getCampiTable = ""
  If Len(Creator) Then Creator = Creator & "."
  If Len(tableName) Then tableName = tableName & "."
  Set TbColonne = m_fun.Open_Recordset("select nome from " & PrefDb & "_colonne where idtable = " & IdTable)
  Do Until TbColonne.EOF
    If i = 1 Then
      getCampiTable = getCampiTable & IIf(Len(Trim(getCampiTable)), "," & vbCrLf, "") & _
                      Creator & tableName & TbColonne!Nome
    Else
      getCampiTable = getCampiTable & IIf(Len(Trim(getCampiTable)), "," & vbCrLf, "") & _
                      Creator & tableName & TbColonne!Nome & " as C_" & TbColonne!Nome
    End If
    TbColonne.MoveNext
  Loop
End Function
' IdTable1: Id Logical Child
' IdTable2: Id Logical Parent
Private Function getWhereJOIN(idLChild As Long, idLParent As Long, ByVal nomeLChild As String, _
                              ByVal nomeLParent As String, ByVal Creator As String) As String
  Dim rsKeyChild As Recordset, rsKeyParent As Recordset
  Dim TbCmp() As cmpCol
  Dim cmpGroup As String, LenCmp As Long
  
  getWhereJOIN = ""
  If Len(Creator) Then Creator = Creator & "."
  If Len(nomeLChild) Then nomeLChild = nomeLChild & "."
  If Len(nomeLParent) Then nomeLParent = nomeLParent & "."
  
  Set rsKeyChild = m_fun.Open_Recordset( _
                   "select c.* from " & PrefDb & "_Index as a, " & _
                                                  PrefDb & "_IdxCol as b, " & _
                                                  PrefDb & "_colonne as c where " & _
                   "a.idtable = " & idLChild & " and a.idindex = b.idindex and " & _
                   "b.idcolonna = c.idcolonna and a.tipo = 'P' and " & _
                   "c.idtable = c.idtablejoin " & _
                   "order by c.ordinale")
  Set rsKeyParent = m_fun.Open_Recordset( _
                    "select distinct c.ordinale as cont, c.* from " & PrefDb & "_Index as a, " & _
                                                   PrefDb & "_IdxCol as b, " & _
                                                   PrefDb & "_colonne as c where " & _
                    "a.idtable = " & idLParent & " and a.idindex = b.idindex and " & _
                    "b.idcolonna = c.idcolonna " & _
                    "order by c.ordinale")
                    
  Do Until rsKeyChild.EOF Or rsKeyParent.EOF
    ' Se i campi sono uguali
    If rsKeyParent!lunghezza = rsKeyChild!lunghezza Then
       getWhereJOIN = getWhereJOIN & _
                      IIf(Len(Trim(getWhereJOIN)), " AND " & vbCrLf, "") & _
                      Creator & nomeLParent & rsKeyParent!Nome & " = " & Creator & nomeLChild & rsKeyChild!Nome
    ' Se i campi hanno lunghezza diversa: creo un gruppo di campi separati da "||"
    ElseIf rsKeyParent!lunghezza > rsKeyChild!lunghezza Then
      LenCmp = 0
      Do Until LenCmp = rsKeyParent!lunghezza Or rsKeyChild.EOF
        LenCmp = LenCmp + rsKeyChild!lunghezza
        cmpGroup = cmpGroup & _
                   IIf(Len(Trim(cmpGroup)), "||", "") & _
                   Creator & nomeLChild & rsKeyChild!Nome
        rsKeyChild.MoveNext
      Loop
      If Not rsKeyChild.EOF Then
        cmpGroup = cmpGroup & _
                   IIf(Len(Trim(cmpGroup)), "||", "") & _
                   Creator & nomeLChild & rsKeyChild!Nome
      End If
      getWhereJOIN = getWhereJOIN & _
                     IIf(Len(Trim(getWhereJOIN)), " AND " & vbCrLf, "") & _
                     Creator & nomeLParent & rsKeyParent!Nome & " = " & cmpGroup
    Else
      LenCmp = 0
      Do Until LenCmp = rsKeyChild!lunghezza Or rsKeyParent.EOF
        LenCmp = LenCmp + rsKeyParent!lunghezza
        cmpGroup = cmpGroup & _
                   IIf(Len(Trim(cmpGroup)), "||", "") & _
                   Creator & nomeLParent & rsKeyParent!Nome
        rsKeyParent.MoveNext
      Loop
      If Not rsKeyParent.EOF Then
        cmpGroup = cmpGroup & _
                   IIf(Len(Trim(cmpGroup)), "||", "") & _
                   Creator & nomeLParent & rsKeyParent!Nome
      End If
      getWhereJOIN = getWhereJOIN & _
                     IIf(Len(Trim(getWhereJOIN)), " AND " & vbCrLf, "") & _
                     cmpGroup & " = " & Creator & nomeLChild & rsKeyChild!Nome
    End If
    If Not rsKeyParent.EOF Then
      rsKeyParent.MoveNext
    End If
    If Not rsKeyChild.EOF Then
      rsKeyChild.MoveNext
    End If
  Loop
  rsKeyParent.Close
  rsKeyChild.Close
End Function

'MG 180107 Creato funzione x inserire le check constraint
Function InsertCheckConstraint(rtb As RichTextBox, IdTable As Long, alias As String, nomeTabella As String) As Boolean
  Dim TbCheck As Recordset
  Dim strCheck As String
  Dim IdCampo As Long
  Dim strTempCondizione
  Dim strResto() As String
  Dim rsColonnaOracle As Recordset
  Dim ix As Integer, i As Integer
  Dim nomeComposto As String
    
  On Error GoTo ErrorHandler
    
    Set TbCheck = m_fun.Open_Recordset("select * from DMORC_Check_Constraint where IdTable= " & IdTable & " order by IdProgressivo")
    If Not TbCheck.EOF Then
        DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Sql\CK\"
        rtb.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\CREATE_CK"
     
        i = 1
        While Not TbCheck.EOF
            nomeComposto = alias & "_" & TbCheck!Nome
            strTempCondizione = TbCheck!Condizione
            strResto = Split(strTempCondizione, "#")
            For ix = 0 To UBound(strResto)
                If IsNumeric(Left(strResto(ix), 7)) Then
                    Set rsColonnaOracle = m_fun.Open_Recordset("Select Nome from DMORC_Colonne where IdDbOr=" & Left(strResto(ix), 7))
                    If Not rsColonnaOracle.EOF Then
                        strTempCondizione = Replace(strTempCondizione, "#" & Left(strResto(ix), 7), rsColonnaOracle!Nome)
                    End If
                    rsColonnaOracle.Close
                End If
            Next
            Dim Campi As String
            If Right(Trim(strTempCondizione), 1) <> "," Then
              Campi = strTempCondizione
            Else
              Campi = Mid(Trim(strTempCondizione), 1, Len(strTempCondizione) - 1)
            End If
            
            If i = 1 Then
              changeTag rtb, "<TB-DECLARE>", nomeTabella
              changeTag rtb, "<CST-DECLARE>", nomeComposto
              changeTag rtb, "<DICHIARAZIONE-CAMPI(i)>", Campi & ";"
            Else
              rtb.SelText = vbCrLf & vbCrLf & Space(4) & " ALTER TABLE " & nomeTabella & vbCrLf _
                            & Space(4) & " ADD CONSTRAINT " & nomeComposto & vbCrLf _
                            & Space(4) & " CHECK " & vbCrLf _
                            & Space(8) & Campi & ";"
            End If
            
            
            'strCheck = strCheck & vbCrLf & Space(15) & "CONSTRAINT " & TbCheck!Nome & vbCrLf & Space(15) & "CHECK " & strTempCondizione
            
            
            
            i = i + 1
            TbCheck.MoveNext
        Wend
        
        
    rtb.SaveFile DataMan.DmParam_OutField_Db & nomeTabella & ".sql", 1
      
    End If
    
  Exit Function
ErrorHandler:
End Function

'VC (SQ): l'ho creata io per l'ORACLE
Private Sub InserisciKeyDef_ORA(rtb As RichTextBox, IdTable As Long, nomeTable As String, _
                                bol As Integer, Creator As String, wDbName As String, _
                                nomeDbd As String, alterTableStatements As String, _
                                alias As String)
  Dim TbColonne As Recordset, TbIndex As Recordset, r As Recordset
  Dim Colonne As String
  Dim ColonneParent As String
  Dim i As Integer, j As Integer
  Dim wPref As String, indexName As String
  Dim ParentTable As String
  Dim rsTableORC As Recordset
  
  If TipoDB = "DB2" Then
    wPref = "DMDB2"
  ElseIf TipoDB = "ORACLE" Then
    wPref = "DMORC"
  End If
  
On Error GoTo ErrorHandler
  
  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & TipoDB & "\Sql\"
  
  
  Select Case bol
    Case 1
      Set TbIndex = m_fun.Open_Recordset("select * from " & wPref & "_Index where " & _
                                         "idtable = " & IdTable & " and tipo='P' and used = true")
    Case 3
      Set TbIndex = m_fun.Open_Recordset("select * from " & wPref & "_Index where " & _
                                         "idtable = " & IdTable & " and tipo='I' and used = True ")
    Case 4
      Set TbIndex = m_fun.Open_Recordset("select * from " & wPref & "_Index where " & _
                                         "idtable = " & IdTable & " and tipo='F' and used = True ")
  End Select

  For i = 1 To TbIndex.RecordCount
    'Preparo la ex <COLONNA>
      Set TbColonne = m_fun.Open_Recordset( _
          "select nome, order from " & wPref & "_IdxCol as a, " & wPref & "_Colonne as b where " & _
          "a.idColonna = b.idColonna AND a.idtable = " & IdTable & " and a.idindex = " & TbIndex!IdIndex & _
          " order by a.ordinale")
      'ATTENZIONE: MODIFICA TEMPORANEA (solo per le FK) ANCHE PER LA BANCA... RIPRISTINARE ! (devo lasciare indietro l'ultima chiave: quella locale!
      If bol = 4 Then
        For j = 1 To TbColonne.RecordCount
          Colonne = IIf(j = 1, TbColonne!Nome, Colonne & "," & TbColonne!Nome)
          TbColonne.MoveNext
        Next j
      Else
        For j = 1 To TbColonne.RecordCount
          'SQ 2-10-2010 Oracle non vuole ASCENDING nella definizione del constraint
          '-> non so se per� da altre parti serve! controllare...
          'Colonne = IIf(j = 1, TbColonne!Nome & "    " & TbColonne!Order, Colonne & "," & TbColonne!Nome & "    " & TbColonne!Order)
          Colonne = IIf(j = 1, TbColonne!Nome, Colonne & "," & TbColonne!Nome)
          TbColonne.MoveNext
        Next j
      End If
      TbColonne.Close
    Select Case bol
      Case 1
        ''''''''''''''''''''''''''''''''''''''''
        ' Ma sta costruendo la DDL! Il nome dell'indice ce l'ha gi�,
        ' -> cosa ci dobbiamo inventare?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ' (non c'� neanche eventualmente il parametro...
        ''
        ' SQ 24-02-08
        ''''''''''''''''''''''''''''''''''''''''
        indexName = TbIndex!Nome
        'If Len(alias) Then
        '  indexName = alias & "_PK"
        'Else
        '  indexName = nomeTable & "_PK"
        'End If
        If i = 1 Then
          rtb.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & TipoDB & "\CREATE_PK"
          
          
          'solo x sungard
          changeTag rtb, "<CRT-NAME>.", Creator
          
          'NOME CHIAVE
          changeTag rtb, "<CST-DECLARE-PK>", indexName
          
          'nome tabella
          changeTag rtb, "<TB-DECLARE>", nomeTable
                                        
          'CAMPI
          changeTag rtb, "<DICHIARAZIONE-CAMPI(i)>", Replace(Colonne, ",", "," & vbCrLf & Space$(12))
                                      
          cleanTags rtb
        Else
          rtb.SelText = ") USING INDEX TABLESPACE INDEX_TS;" & vbCrLf
          rtb.SelText = vbCrLf & Space(4) & "ALTER TABLE " & nomeTable _
                         & vbCrLf & Space(4) & " " & "ADD CONSTRAINT " & indexName & " PRIMARY KEY " _
                         & vbCrLf & Space(4) & " " & "( " _
                         & vbCrLf & Space(4) & Space(4) & Replace(Colonne, ",", "," & vbCrLf & Space(4)) _
                         & vbCrLf & Space(4) & ")  "
        End If
                                      
        'SALVA IL FILE
        If i = TbIndex.RecordCount Then
          rtb.SaveFile DataMan.DmParam_OutField_Db & "PK\" & nomeTable & "_PK.sql", 1
        End If
        
      Case 3
        ''''''''''''''''''''''''''''''''''''''''
        ' Ma sta costruendo la DDL! Il nome dell'indice ce l'ha gi�,
        ' -> cosa ci dobbiamo inventare?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ' (non c'� neanche eventualmente il parametro...
        ''
        ' SQ 24-02-08
        ''''''''''''''''''''''''''''''''''''''''
        'indexName = alias & "_" & i & "_IX"
        indexName = TbIndex!Nome
        If i = 1 Then
          rtb.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & TipoDB & "\CREATE_IX"
         
          '''''''''''''''''''''''''
          'UNIQUE CHI LO METTEVA, MIO NONNO?!
          'SQ 24-02-08
          '''''''''''''''''''''''''
          changeTag rtb, "<INDEX-TYPE>", IIf(TbIndex!Unique, "UNIQUE INDEX", "INDEX")
                    
          'solo x sungard
          changeTag rtb, "<CRT-NAME>.", Creator
        
          'NOME CHIAVE
          changeTag rtb, "<CST-DECLARE-IX>", indexName
          
          'nome tabella
          changeTag rtb, "<TB-DECLARE>", nomeTable
          
          'CAMPI
          changeTag rtb, "<DICHIARAZIONE-CAMPI(i)>", Replace(Colonne, ",", "," & vbCrLf)
            
          cleanTags rtb
        Else
          'SQ NON CI CREDO... SOLO IL PRIMO INDICE E' SU TEMPLATE... GLI ALTRI LI SCRIVE QUI!!!!!!!!!!
          rtb.SelText = ") TABLESPACE INDEX_TS;" & vbCrLf
          
          
          If TbIndex!Unique Then
            rtb.SelText = vbCrLf & Space(4) & "CREATE UNIQUE INDEX " & indexName _
                         & vbCrLf & Space(4) & " " & "ON " & nomeTable _
                         & vbCrLf & Space(4) & " " & "( " _
                         & vbCrLf & Space(4) & Space(4) & Replace(Colonne, ",", "," & vbCrLf & Space(4)) '_
                         '& vbCrLf & space(4) & " "
          
          Else
            rtb.SelText = vbCrLf & Space(4) & "CREATE INDEX " & indexName _
                         & vbCrLf & Space(4) & " " & "ON " & nomeTable _
                         & vbCrLf & Space(4) & " " & "( " _
                         & vbCrLf & Space(4) & Space(4) & Replace(Colonne, ",", "," & vbCrLf & Space(4)) ' _
                         '& vbCrLf & space(4) & " "
          End If
        
        End If
        'SALVA IL FILE
        If i = TbIndex.RecordCount Then
          rtb.SaveFile DataMan.DmParam_OutField_Db & "IX\" & nomeTable & "_IX.sql", 1
        End If
      Case 4
        'MG 190107 inserimento fk
        InserisciFK rtb, IdTable, wPref, Creator, alias
    End Select
    TbIndex.MoveNext
  Next i
  TbIndex.Close

  Exit Sub
ErrorHandler:
  m_fun.WriteLog "Errore in MadmdM_ORC.InserisciKeyDef_ORA]. Errore: " & ERR.Description
End Sub

Public Function InserisciFK(rtb As RichTextBox, idParentTable As Long, _
                            wPref As String, Creator As String, alias As String) As Boolean
    Dim rsQuery As Recordset
    Dim rsIndex As Recordset
    Dim rsForTable As Recordset
    Dim SQL As String
    Dim strIndexName As String, strForTable As String
    Dim lIdIndex As Long
    Dim lIdTableFor As Long
    Dim strTableInit As String
    Dim strTableEnd As String
    Dim strFieldInit As String
    Dim strFieldEnd As String
    Dim i As Integer
    
On Error GoTo ErrorHandler

    'ricava gli indici x le fk
    Set rsIndex = m_fun.Open_Recordset("select distinct * from " & wPref & "_index where IdTable = " & idParentTable & " and tipo = 'F'")


    If Not rsIndex.EOF Then
      i = 1
      rtb.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & TipoDB & "\CREATE_FK"
      While Not rsIndex.EOF
            
            strIndexName = rsIndex!Nome
            
            SQL = "  SELECT " & _
                  " T1.Nome as tInit, C1.Nome as cInit, T2.Nome as tDest, " & _
                  " C2.Nome as cDest,  IDX.Ordinale, IX.ForeignIdTable " & _
                  " From " & _
                  " DMORC_INDEX IX, " & _
                  " DMORC_TABELLE T1, " & _
                  " DMORC_TABELLE T2, " & _
                  " DMORC_IDXCOL IDX, " & _
                  " DMORC_COLONNE C1, " & _
                  " DMORC_COLONNE C2 " & _
                  " WHERE IX.IDTABLE = T1.IDTABLE AND " & _
                  " IX.FOREIGNIDTABLE = T2.IDTABLE AND " & _
                  " IX.IDINDEX = IDX.IDINDEX and " & _
                  " C1.IDCOLONNA = IDX.IDCOLONNA and " & _
                  " C2.IDCOLONNA = IDX.FORIDCOLONNA and " & _
                  " idx.IdIndex = " & rsIndex!IdIndex & _
                  " ORDER BY IDX.Ordinale; "
                  
            Set rsQuery = m_fun.Open_Recordset(SQL)
            
            'salva i nomi dei campi e i nomi delle tabelle
            strTableInit = ""
            strTableEnd = ""
            strFieldInit = ""
            strFieldEnd = ""
            lIdTableFor = -1
            If Not rsQuery.EOF Then
                lIdTableFor = rsQuery!ForeignIdTable
                strTableInit = rsQuery!tInit
                strTableEnd = rsQuery!tDest
                While Not rsQuery.EOF
                    strFieldInit = strFieldInit & "," & rsQuery!cInit
                    strFieldEnd = strFieldEnd & "," & rsQuery!cDest
                    rsQuery.MoveNext
                Wend
                strFieldInit = Mid(strFieldInit, 2)
                strFieldEnd = Mid(strFieldEnd, 2)
            End If
            

            Dim stringaDelete As String
            stringaDelete = ""
            If rsIndex!ForOnDelete Then
              stringaDelete = "ON DELETE"
              If rsIndex!ForOnCascade Then
                stringaDelete = stringaDelete & " CASCADE"
              ElseIf rsIndex!ForOnRestrict Then
                '� il default
                stringaDelete = ""
              Else
                stringaDelete = stringaDelete & " SET NULL"
              End If
              stringaDelete = stringaDelete & " ENABLE NOVALIDATE"
            Else
              stringaDelete = stringaDelete & " ENABLE NOVALIDATE"
            End If

            '''strIndexName = Left(strTableEnd, 7) & "_" & Left(strTableInit, 7) & "_" & i & "_FK"
            Set rsForTable = m_fun.Open_Recordset("Select * from DMORC_Alias where IdTable=" & lIdTableFor)
            If rsForTable.RecordCount > 0 Then
                strForTable = rsForTable!Nome
            End If
            rsForTable.Close
            'SQ TMP - MA CHI L'HA DETTO?!
            'strIndexName = strForTable & "_" & alias & "_" & i & "_FK"
            
            If i = 1 Then
              'solo x sungard
              changeTag rtb, "<CRT-NAME>.", ""
            
              'NOME CHIAVE
              changeTag rtb, "<CST-DECLARE-FK>", strIndexName
              
              'nome tabella IN
              changeTag rtb, "<TB-DECLARE-IN>", strTableInit
              
              'nome tabella OUT
              changeTag rtb, "<TB-DECLARE-OUT>", strTableEnd
              
              'CAMPI IN
              changeTag rtb, "<DICHIARAZIONE-CAMPI-IN>.", Replace(strFieldInit, ",", "," & vbCrLf & Space(4) & Space(4))
              
              'CAMPI OUT
              changeTag rtb, "<DICHIARAZIONE-CAMPI-OUT>.", Replace(strFieldEnd, ",", "," & vbCrLf & Space(4) & Space(4))

              'ON DELETE
              changeTag rtb, "<TIPO-DELETE>.", stringaDelete
              
            Else
              rtb.SelText = " ;" & vbCrLf
              rtb.SelText = vbCrLf & Space(4) & "ALTER TABLE " & strTableInit _
                          & vbCrLf & Space(4) & "ADD CONSTRAINT " & strIndexName _
                          & vbCrLf & Space(4) & "FOREIGN KEY " _
                          & vbCrLf & Space(4) & "( " _
                          & vbCrLf & Space(4) & Space(4) & Replace(strFieldInit, ",", "," & vbCrLf & Space(4) & Space(4)) _
                          & vbCrLf & Space(4) & ") " _
                          & vbCrLf & Space(4) & "REFERENCES " & strTableEnd _
                          & vbCrLf & Space(4) & "( " _
                          & vbCrLf & Space(4) & Space(4) & Replace(strFieldEnd, ",", "," & vbCrLf & Space(4) & Space(4)) _
                          & vbCrLf & Space(4) & ") " _
                          & vbCrLf & Space(4) & Space(4) & stringaDelete '& " ;"
            End If
            rsIndex.MoveNext
            i = i + 1
        Wend
        'SALVA IL FILE
       rtb.SaveFile DataMan.DmParam_OutField_Db & "FK\" & strTableInit & "_FK.sql", 1
    End If
    
    Exit Function
    
ErrorHandler:
    
End Function

Public Function TrovaColonneParent(idParentTable As Long, wPref As String) As String
  Dim rsIndex As Recordset
  Dim rsIdxCol As Recordset
  Dim rsColonne As Recordset
  
  TrovaColonneParent = ""

  Set rsIndex = m_fun.Open_Recordset("select IdIndex from " & wPref & "_index where IdTable = " & idParentTable & " and nome = 'PRIMARY_KEY'")

  If rsIndex.RecordCount > 0 Then
    Set rsColonne = m_fun.Open_Recordset("select B.Nome from " & wPref & "_idxcol as A, " & wPref & "_colonne as B " & _
                                         "where A.IdColonna = B.IdColonna and A.IdTable = " & idParentTable & " and IdIndex = " & rsIndex!IdIndex)
    Do Until rsColonne.EOF
      TrovaColonneParent = TrovaColonneParent & rsColonne!Nome & ","
      rsColonne.MoveNext
    Loop
    If Not (TrovaColonneParent = "") Then
      TrovaColonneParent = Mid(TrovaColonneParent, 1, Len(TrovaColonneParent) - 1)
    End If
    rsColonne.Close
  End If
 rsIndex.Close
End Function
'restituisce l'id tablespace dall'id della tabella
Public Function Get_IdColonna_ORACLE(IdTable As Long, NomeCol As String) As Long
  Dim rs As Recordset
  Dim wNomeCol
  Dim k As Integer
  
  wNomeCol = NomeCol
  k = InStr(wNomeCol, "-")
  While k > 0
    Mid$(wNomeCol, k, 1) = "_"
    k = InStr(wNomeCol, "-")
  Wend
  Set rs = m_fun.Open_Recordset("Select * From DMORC_Colonne Where IdTable = " & IdTable & " And Nome = '" & wNomeCol & "'")
  If rs.RecordCount > 0 Then
    'deve essere solo uno
    Get_IdColonna_ORACLE = rs!IdColonna
    rs.Close
  Else
    Get_IdColonna_ORACLE = 0
  End If
  rs.Close
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''' INTEGRITA' REFERENZIALE "APPLICATIVA"
''' ELIMINA ALL/AUTOMATIC ITEMS
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''Public Sub deleteTableORACLE(IdTable As Long, Optional eraseAll As Boolean)
''  Dim queryFilter As String
''
''  On Error GoTo errdb
''
''  If Not eraseAll Then queryFilter = " and tag like 'AUTOMATIC%'"
''
''  DataMan.DmConnection.Execute "DELETE * From DMORC_Tabelle Where idtable = " & IdTable & queryFilter
''  DataMan.DmConnection.Execute "DELETE * From DMORC_Alias Where idtable = " & IdTable & queryFilter
''  DataMan.DmConnection.Execute "DELETE * From DMORC_Colonne Where idtable = " & IdTable & queryFilter
''  DataMan.DmConnection.Execute "DELETE * From DMORC_Index Where idtable = " & IdTable & queryFilter
''  DataMan.DmConnection.Execute "DELETE * From DMORC_IdxCol Where idtable = " & IdTable & queryFilter
''
''  Exit Sub
''errdb:
''  MsgBox "tmp: " & ERR.Description
''  Resume Next
''End Sub

Sub Carica_ArrRelations(dbsel As Long)
Dim rsTable As Recordset, rsSegment As Recordset
Dim rsLSegm As Recordset, rsPSegm As Recordset
Dim rsTbPaired As Recordset, rsTbPairSegm As Recordset
Dim i As Long, a As Long
Dim LParent As Boolean

  i = 0
  ReDim arrRelations(i)
  Set rsTable = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where iddb = " & dbsel)
  Do Until rsTable.EOF
    i = i + 1
    ReDim Preserve arrRelations(i)
    arrRelations(i).Name = rsTable!Nome
    arrRelations(i).IdTable = rsTable!IdTable
    arrRelations(i).IdSegmento = rsTable!IdOrigine
    ReDim arrRelations(i).idLC(0)
    ReDim arrRelations(i).LC(0)
    ReDim arrRelations(i).idPC(0)
    ReDim arrRelations(i).PC(0)
    Set rsSegment = m_fun.Open_Recordset("select * from psdli_Segmenti where idsegmento = " & rsTable!IdOrigine)
    If Not rsSegment.EOF Then
      ' La tabella ha un Logical Parent?
      If Len(Trim(rsSegment!LParent)) Then
        LParent = True
        Set rsLSegm = m_fun.Open_Recordset("select c.idtable, c.nome from psdli_segmenti as a, bs_oggetti as b, " & PrefDb & "_tabelle as c where " & _
                                           "a.nome = '" & rsSegment!LParent & "' AND " & _
                                           "a.idoggetto = b.idoggetto AND " & _
                                           "b.nome = '" & rsSegment!LDBD & "' and " & _
                                           "c.idorigine = a.idsegmento")
        If Not rsLSegm.EOF Then
          arrRelations(i).idLP = rsLSegm!IdTable
          arrRelations(i).LP = rsLSegm!Nome
        End If
        rsLSegm.Close
        ' Cerca la Paired Table
        Set rsTbPaired = m_fun.Open_Recordset("select c.* " & _
                                          "from psdli_segmenti as a, bs_oggetti as b, psdli_lchild as c where " & _
                                          "b.idoggetto = a.idoggetto and " & _
                                          "a.idsegmento = " & rsTable!IdOrigine & " and " & _
                                          "c.dbdname = '" & rsSegment!LDBD & "' and " & _
                                          "c.nomesegmento = '" & rsSegment!LParent & "' and " & _
                                          "c.lc = a.nome and " & _
                                          "c.lc_dbd = b.nome")
        If Not rsTbPaired.EOF Then
          arrRelations(i).paired = rsTbPaired!pair
          Set rsTbPairSegm = m_fun.Open_Recordset( _
                             "select c.idtable from PSDLi_Segmenti as a, bs_oggetti as b, " & PrefDb & "_tabelle as c where " & _
                             "a.nome = '" & rsTbPaired!pair & "' and " & _
                             "a.idoggetto = b.idoggetto and " & _
                             "b.nome = '" & rsSegment!LDBD & "' and " & _
                             "a.idsegmento = c.idorigine")
                             
          If Not rsTbPairSegm.EOF Then
            arrRelations(i).idPaired = rsTbPairSegm!IdTable
          End If
        End If
        rsTbPaired.Close
      Else
        LParent = False
      End If
      ' La tabella ha un Phisical Parent?
      If rsSegment!Parent <> 0 Then
        Set rsPSegm = m_fun.Open_Recordset("select b.idtable, b.nome from psdli_segmenti as a, " & PrefDb & "_tabelle as b where " & _
                                           "a.nome = '" & rsSegment!Parent & "' AND " & _
                                           "a.idoggetto = " & rsSegment!IdOggetto & " and " & _
                                           "a.idsegmento = b.idorigine")
        If Not rsPSegm.EOF Then
          arrRelations(i).idPP = rsPSegm!IdTable
          arrRelations(i).PP = rsPSegm!Nome
        End If
        rsPSegm.Close
      End If
    End If
    rsSegment.Close
    ' Non essendo Logical Parent, La tabella ha dei Logical Childs?
    If Not LParent Then
      a = 0
      ReDim arrRelations(i).idLC(a)
      ReDim arrRelations(i).LC(a)
      Set rsSegment = m_fun.Open_Recordset( _
                      "select c.nome, c.idtable from psdli_Segmenti as a, psdli_segmenti as b, " & PrefDb & "_tabelle as c where " & _
                      "a.lparent = b.nome and " & _
                      "b.idsegmento = " & rsTable!IdOrigine & " and " & _
                      "a.idsegmento = c.idorigine " & _
                      "order by a.idsegmento")
      Do Until rsSegment.EOF
        a = a + 1
        ReDim Preserve arrRelations(i).idLC(a)
        ReDim Preserve arrRelations(i).LC(a)
        arrRelations(i).idLC(a) = rsSegment!IdTable
        arrRelations(i).LC(a) = rsSegment!Nome
        rsSegment.MoveNext
      Loop
      rsSegment.Close
    End If
    ' La tabella ha dei Phisical Childs?
    a = 0
    ReDim arrRelations(i).idPC(a)
    ReDim arrRelations(i).PC(a)
    Set rsSegment = m_fun.Open_Recordset( _
                    "select c.nome, c.idtable from psdli_Segmenti as a, psdli_segmenti as b, " & PrefDb & "_tabelle as c where " & _
                    "a.parent = b.nome and " & _
                    "a.idoggetto = b.idoggetto and " & _
                    "b.idsegmento = " & rsTable!IdOrigine & " and " & _
                    "a.idsegmento = c.idorigine " & _
                    "order by a.idsegmento")
    Do Until rsSegment.EOF
      a = a + 1
      ReDim Preserve arrRelations(i).idPC(a)
      ReDim Preserve arrRelations(i).PC(a)
      arrRelations(i).idPC(a) = rsSegment!IdTable
      arrRelations(i).PC(a) = rsSegment!Nome
      rsSegment.MoveNext
    Loop
    rsSegment.Close
    rsTable.MoveNext
  Loop
  rsTable.Close
  
End Sub

Sub CreaTrigger()
Dim template As String
Dim Percorso As String
Dim fileOut As String, fileIn As String
Dim RTB_File As RichTextBox
Dim i As Long, a As Long, w As Long
Dim cmpIndex As Boolean
Dim flgPP As Boolean, flgLP As Boolean
Dim flgPC As Boolean, flgLC As Boolean
' Sono 3 flag che indicano le regole di INSERT, DELETE e REPLACE
Dim ruleSegm As String ' Rule: P,L,V,X.
Dim rsSegmento As Recordset
Dim flgOK As Boolean
Dim typeIstr As String, rulesIstr As String, tableIstr As String

  On Error GoTo ErrorHandler
  For i = 1 To UBound(arrRelations)
    
    ReDim CompareCmp(0)
    If Len(arrRelations(i).paired) Then
      ' confronto le tabelle gemellate: da controllare
      If arrRelations(i).IdTable <> 0 And arrRelations(i).idPaired <> 0 Then
        CompareCmp() = compareTables(arrRelations(i).IdTable, arrRelations(i).idPaired)
      Else
        'Segnalare un errore
      End If
    End If
''    ' Caricamento variabili-Tag
''    getTagsTrigger arrRelations(i)
    ReDim PChild_DLET(0)
    If UBound(arrRelations(i).idPC) > 0 Then
      searchPC_DLET arrRelations(i)
    End If
    
    ' setto i flag per gestire poi le condizioni di crezione trigger
    Set rsSegmento = m_fun.Open_Recordset("select a.Rules from psdli_segmenti as a, " & PrefDb & "_tabelle as b where " & _
                                          "b.idtable = " & arrRelations(i).IdTable & " and " & _
                                          "b.idorigine = a.idsegmento")
    If Not rsSegmento.EOF Then
      ruleSegm = rsSegmento!Rules
    Else
      ruleSegm = ""
    End If
    rsSegmento.Close
    
    ' Se ha un Logical Parent, allora la tabella � un Logical Child
    If arrRelations(i).idLP Then
      flgLC = True
    Else
      flgLC = False
    End If
    ' Se ha un Phisical Parent, allora la tabella � un Phisical Child
    If arrRelations(i).idPP Then
      flgPC = True
    Else
      flgPC = False
    End If
    ' Se ha dei Logical Child, allora la tabella � un Logical Parent
    If UBound(arrRelations(i).idLC) Then
      flgLP = True
    Else
      flgLP = False
    End If
    ' Se ha Dei Phisical Child , allora la tabella � un Phisical Parent
    If UBound(arrRelations(i).idPC) Then
      flgPP = True
    Else
      flgPP = False
    End If
    
    template = ""
    Percorso = DataMan.DmPathDb & "\input-prj\template\imsdb\standard\trigger\"
    template = Dir(Percorso & "*")
    Do Until template = ""
      ' la tabella ha solo campi chiave?
      cmpIndex = True
      For a = 1 To UBound(CompareCmp())
        If CompareCmp(a).Index = False Then
          cmpIndex = False
          Exit For
        End If
      Next a
            
      ' Nome Template Trigger: AAAA_B_CC
      ' A = 4 Caratteri per identificare il tipo d'istruzione ISRT,UPDT,DLET
      ' B = 1 carattere per identificare la Rule
      '                                           X: Generale
      '                                           L: Logico
      '                                           V: Virtuale
      '                                           P: Fisico
      ' C = 2 caratteri per identificare il tipo di tabella interessata
      '                                                                  PP: Phisical Parent
      '                                                                  LP: Logical Parent
      '                                                                  PC: Phisical Child
      '                                                                  LC: Logical Child
      typeIstr = Left(template, 4)
      rulesIstr = Mid(Right(template, 4), 1, 1)
      tableIstr = Right(template, 2)
      
      If (flgLC And tableIstr = "LC") Or _
         (flgLP And tableIstr = "LP") Or _
         (flgPC And tableIstr = "PC") Or _
         (flgPP And tableIstr = "PP" And UBound(PChild_DLET) > 0) Then
        Select Case typeIstr
          Case "ISRT"
            flgOK = False
            If rulesIstr = "X" Or _
               rulesIstr = Mid(ruleSegm, 1, 1) Then
              varPref = ":new."
              flgOK = True
            End If
          Case "DLET"
            flgOK = False
            If rulesIstr = "X" Or _
               rulesIstr = Mid(ruleSegm, 2, 1) Then
              varPref = ":old."
              flgOK = True
            End If
          Case "UPDT"
            flgOK = False
            If Not cmpIndex Then
              If rulesIstr = "X" Or _
                 rulesIstr = Mid(ruleSegm, 3, 1) Then
                varPref = ":new."
                flgOK = True
              End If
            End If
          Case Else
            ' Per ora niente
        End Select
          
        If flgOK Then
          ' Caricamento variabili-Tag
          getTagsTrigger arrRelations(i)
          getTagDLET arrRelations(i)
          Set RTB_File = MadmdF_Start.RtbFile
          fileIn = Percorso & template
          
          If tableIstr = "PP" Then
            ' Mauro 30-05-2007 : Gestione anomala per i Phisical Parent!
            ' Bisogna creare un trigger per ogni Phisical Child!
            For w = 1 To UBound(PChild_DLET)
              RTB_File.LoadFile fileIn
            
              changeTag RTB_File, "<creator>", getCreator(PChild_DLET(w).idTb)
              changeTag RTB_File, "<TABLE-NAME>", nomeLChild
              changeTag RTB_File, "<TABLE-DLET>", PChild_DLET(w).NomeTb
              changeTag RTB_File, "<WHERE-DLET>", PChild_DLET(w).Where_DLET & ";"
              
              fileOut = DataMan.DmPathDb & "\output-prj\" & dbType & "\sql\trigger\" & arrRelations(i).Name & "_" & template & "_" & PChild_DLET(w).NomeTb & ".sql"
              RTB_File.SaveFile fileOut, 1
            Next w
          Else
            RTB_File.LoadFile fileIn
            
            changeTagsTrigger RTB_File, arrRelations(i)
          
            fileOut = DataMan.DmPathDb & "\output-prj\" & dbType & "\sql\trigger\" & arrRelations(i).Name & "_" & template & ".sql"
            RTB_File.SaveFile fileOut, 1
          End If
        End If
      End If
    
      template = Dir
    Loop
  Next i
  
  Exit Sub
ErrorHandler:
  If ERR.Number = 75 Then
    MkDir DataMan.DmPathDb & "\output-prj\" & dbType & "\sql\trigger\"
    Resume
  Else
    ERR.Raise ERR.Number, ERR.Source, ERR.Description
  End If
End Sub

' Routine per confrontare i campi di 2 tabelle
Private Function compareTables(idTb1 As Long, idTb2 As Long) As cmpCol()
  Dim rsTb1 As Recordset, rsTb2 As Recordset
  Dim cmpTb() As cmpCol
  Dim i As Long
  Dim LenRemainCmp1 As Long, LenRemainCmp2 As Long
  Dim ordinaleTb1 As Long, ordinaleTb2 As Long
  Dim wIndex As Boolean
  
  On Error GoTo ErrorHandler
  
  Dim NumFile As Integer
  
  NumFile = FreeFile
  'Open Dli2Rdbms.drPathDef & "\" & Left(Dli2Rdbms.drNomeDB, InStr(Dli2Rdbms.drNomeDB, ".") - 1) & "\Output-prj\Compare_" & idTb1 & "_" & idTb2 & ".txt" For Output As numFile
  
  Set rsTb1 = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                   "idtable = " & idTb1 & " and idcmp > 0 " & _
                                   "order by ordinale")
  Set rsTb2 = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                   "idtable = " & idTb2 & " and idcmp > 0 " & _
                                   "order by ordinale")
  
  i = 0
  ReDim cmpTb(i)
  LenRemainCmp1 = 0
  LenRemainCmp2 = 0
  ordinaleTb1 = 0
  ordinaleTb2 = 0
  wIndex = True
  Do Until rsTb2.EOF Or rsTb1!IdTable = rsTb1!idTableJoin
  
    If rsTb2!IdTable = rsTb2!idTableJoin Then
      ' controllo tipo CMP
      If (rsTb1!Tipo = "CHAR" Or rsTb1!Tipo = "VARCHAR") And _
         (rsTb2!Tipo = "CHAR" Or rsTb2!Tipo = "VARCHAR") Then
        If LenRemainCmp1 = 0 And LenRemainCmp2 = 0 Then       ' non ho resto dalla compare precedente
          If rsTb1!lunghezza = rsTb2!lunghezza Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            rsTb1.MoveNext
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza > rsTb2!lunghezza Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = rsTb1!lunghezza - rsTb2!lunghezza
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza < rsTb2!lunghezza Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = rsTb2!lunghezza - rsTb1!lunghezza
            rsTb1.MoveNext
          End If
        ElseIf LenRemainCmp1 > 0 And LenRemainCmp2 = 0 Then       ' il Cmp1 precedente era + grande
          If rsTb2!lunghezza = LenRemainCmp1 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = 0
            rsTb1.MoveNext
            rsTb2.MoveNext
          ElseIf rsTb2!lunghezza < LenRemainCmp1 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = LenRemainCmp1 - rsTb2!lunghezza
            rsTb2.MoveNext
          ElseIf rsTb2!lunghezza > LenRemainCmp1 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = rsTb2!lunghezza - LenRemainCmp1
            LenRemainCmp1 = 0
            rsTb1.MoveNext
          End If
        ElseIf LenRemainCmp1 = 0 And LenRemainCmp2 > 0 Then       ' il Cmp2 precedente era + grande
          If rsTb1!lunghezza = LenRemainCmp2 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = 0
            rsTb1.MoveNext
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza > LenRemainCmp2 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = rsTb1!lunghezza - LenRemainCmp2
            LenRemainCmp2 = 0
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza < LenRemainCmp2 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = LenRemainCmp2 - rsTb1!lunghezza
            rsTb1.MoveNext
          End If
        End If
      Else
        ' controllo che i campi abbiano lo stesso tipo <> da CHAR o VARCHAR
        If rsTb1!Tipo = rsTb2!Tipo Then
          AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
          rsTb1.MoveNext
          rsTb2.MoveNext
        Else
          ' Segnalazione: i tipi  sono diversi
          'Print #numFile, "CMP1: " & rsTb1!Nome & " from TABLE1: " & idTb1 & "| CMP2: " & rsTb2!Nome & " from TABLE2: " & idTb2
          If LenRemainCmp1 > 0 Then
            
          ElseIf LenRemainCmp2 > 0 Then
          Else
            rsTb1.MoveNext
            rsTb2.MoveNext
          End If
        End If
      End If
    Else
      rsTb2.MoveNext
    End If
  Loop
  
  If Not (rsTb2.EOF) Then
    ordinaleTb2 = rsTb2!Ordinale
  End If
  
  rsTb1.MoveFirst
  rsTb2.MoveFirst
  
  Do Until rsTb1.EOF Or rsTb2!IdTable = rsTb2!idTableJoin
    If rsTb1!IdTable = rsTb1!idTableJoin Then
      ' controllo tipo CMP
      If (rsTb1!Tipo = "CHAR" Or rsTb1!Tipo = "VARCHAR") And _
         (rsTb2!Tipo = "CHAR" Or rsTb2!Tipo = "VARCHAR") Then
        If LenRemainCmp1 = 0 And LenRemainCmp2 = 0 Then       ' non ho resto dalla compare precedente
          If rsTb1!lunghezza = rsTb2!lunghezza Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            rsTb1.MoveNext
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza > rsTb2!lunghezza Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = rsTb1!lunghezza - rsTb2!lunghezza
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza < rsTb2!lunghezza Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = rsTb2!lunghezza - rsTb1!lunghezza
            rsTb1.MoveNext
          End If
        ElseIf LenRemainCmp1 > 0 And LenRemainCmp2 = 0 Then       ' il Cmp1 precedente era + grande
          If rsTb2!lunghezza = LenRemainCmp1 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = 0
            rsTb1.MoveNext
            rsTb2.MoveNext
          ElseIf rsTb2!lunghezza < LenRemainCmp1 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = LenRemainCmp1 - rsTb2!lunghezza
            rsTb2.MoveNext
          ElseIf rsTb2!lunghezza > LenRemainCmp1 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = rsTb2!lunghezza - LenRemainCmp1
            LenRemainCmp1 = 0
            rsTb1.MoveNext
          End If
        ElseIf LenRemainCmp1 = 0 And LenRemainCmp2 > 0 Then       ' il Cmp2 precedente era + grande
          If rsTb1!lunghezza = LenRemainCmp2 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = 0
            rsTb1.MoveNext
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza > LenRemainCmp2 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = rsTb1!lunghezza - LenRemainCmp2
            LenRemainCmp2 = 0
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza < LenRemainCmp2 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = LenRemainCmp2 - rsTb1!lunghezza
            rsTb1.MoveNext
          End If
        End If
      Else
        ' controllo che i campi abbiano lo stesso tipo <> da CHAR o VARCHAR
        If rsTb1!Tipo = rsTb2!Tipo Then
          AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
          rsTb1.MoveNext
          rsTb2.MoveNext
        Else
          ' Segnalazione: i tipi  sono diversi
          'Print #numFile, "CMP1: " & rsTb1!Nome & " from TABLE1: " & idTb1 & " |CMP2: " & rsTb2!Nome & " from TABLE2: " & idTb2
          If LenRemainCmp1 > 0 Then
            
          ElseIf LenRemainCmp2 > 0 Then
          
          Else
            rsTb1.MoveNext
            rsTb2.MoveNext
          End If
        End If
      End If
    Else
      rsTb1.MoveNext
    End If
  Loop
  
  If Not (rsTb1.EOF) Then
    ordinaleTb1 = rsTb1!Ordinale
  End If
  
  rsTb1.Close
  rsTb2.Close
  
  If ordinaleTb1 > 0 Or ordinaleTb2 > 0 Then
    
    Set rsTb1 = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                          "idtable = " & idTb1 & " and ordinale >= " & ordinaleTb1 & " and " & _
                          "idcmp > 0 " & _
                          " order by ordinale")
    Set rsTb2 = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                          "idtable = " & idTb2 & " and ordinale >= " & ordinaleTb2 & " and " & _
                          "idcmp > 0 " & _
                          " order by ordinale")
    
    LenRemainCmp1 = 0
    LenRemainCmp2 = 0
    wIndex = False
    Do Until rsTb2.EOF Or rsTb1.EOF
      ' controllo tipo CMP
      If (rsTb1!Tipo = "CHAR" Or rsTb1!Tipo = "VARCHAR") And _
         (rsTb2!Tipo = "CHAR" Or rsTb2!Tipo = "VARCHAR") Then
        If LenRemainCmp1 = 0 And LenRemainCmp2 = 0 Then       ' non ho resto dalla compare precedente
          If rsTb1!lunghezza = rsTb2!lunghezza Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            rsTb1.MoveNext
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza > rsTb2!lunghezza Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = rsTb1!lunghezza - rsTb2!lunghezza
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza < rsTb2!lunghezza Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = rsTb2!lunghezza - rsTb1!lunghezza
            rsTb1.MoveNext
          End If
        ElseIf LenRemainCmp1 > 0 And LenRemainCmp2 = 0 Then       ' il Cmp1 precedente era + grande
          If rsTb2!lunghezza = LenRemainCmp1 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = 0
            rsTb1.MoveNext
            rsTb2.MoveNext
          ElseIf rsTb2!lunghezza < LenRemainCmp1 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = LenRemainCmp1 - rsTb2!lunghezza
            rsTb2.MoveNext
          ElseIf rsTb2!lunghezza > LenRemainCmp1 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = rsTb2!lunghezza - LenRemainCmp1
            LenRemainCmp1 = 0
            rsTb1.MoveNext
          End If
        ElseIf LenRemainCmp1 = 0 And LenRemainCmp2 > 0 Then       ' il Cmp2 precedente era + grande
          If rsTb1!lunghezza = LenRemainCmp2 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = 0
            rsTb1.MoveNext
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza > LenRemainCmp2 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp1 = rsTb1!lunghezza - LenRemainCmp2
            LenRemainCmp2 = 0
            rsTb2.MoveNext
          ElseIf rsTb1!lunghezza < LenRemainCmp2 Then
            AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
            LenRemainCmp2 = LenRemainCmp2 - rsTb1!lunghezza
            rsTb1.MoveNext
          End If
        End If
      Else
        ' controllo che i campi abbiano lo stesso tipo <> da CHAR o VARCHAR
        If rsTb1!Tipo = rsTb2!Tipo Then
          AggiornaCmpTb rsTb1, rsTb2, cmpTb(), i, wIndex
          rsTb1.MoveNext
          rsTb2.MoveNext
        Else
          ' Segnalazione: i tipi  sono diversi
          'Print #numFile, "CMP1: " & rsTb1!Nome & " from TABLE1: " & idTb1 & "| CMP2: " & rsTb2!Nome & " from TABLE2: " & idTb2
          If LenRemainCmp1 > 0 Then
            
          ElseIf LenRemainCmp2 > 0 Then
          Else
            rsTb1.MoveNext
            rsTb2.MoveNext
          End If
        End If
      End If
    Loop
    
    rsTb1.Close
    rsTb2.Close
  End If
  Dim compare As String
  For i = 1 To UBound(cmpTb)
    compare = compare & cmpTb(i).nomeCmp1 & Space(30 - Len(cmpTb(i).nomeCmp1)) & " | " & Format(cmpTb(i).lenCmp1, "00") & "|" & _
                        cmpTb(i).nomeCmp2 & Space(30 - Len(cmpTb(i).nomeCmp2)) & " | " & Format(cmpTb(i).lenCmp2, "00") & vbCrLf
  Next i
      
  'Print #numFile, compare
      
  'Close NumFile
        
  compareTables = cmpTb
  Exit Function
ErrorHandler:
  If ERR.Number Then
    Close NumFile
    ERR.Raise ERR.Number, ERR.Source, ERR.Description
  Else
  End If
End Function

Sub AggiornaCmpTb(rsTb1 As Recordset, rsTb2 As Recordset, cmpTb() As cmpCol, i As Long, wIndex As Boolean)
  i = i + 1
  ReDim Preserve cmpTb(i)
  cmpTb(i).nomeCmp1 = rsTb1!Nome
  cmpTb(i).lenCmp1 = rsTb1!lunghezza
  cmpTb(i).Tipo = rsTb1!Tipo
  cmpTb(i).Index = wIndex
  cmpTb(i).nomeCmp2 = rsTb2!Nome
  cmpTb(i).lenCmp2 = rsTb2!lunghezza
End Sub

Public Sub getTagsTrigger(tbRel As TabRelations)
  Creator = getCreator(tbRel.IdTable)
  nomeLChild = tbRel.Name 'getLChild(tbRel)
  nomeLParent = tbRel.LP 'getLParent(tbRel)
  Declare_PK_LP = getDeclPKLP(tbRel.idLP)
  Cursor_PK_LC = getCursPKLC(tbRel.IdTable)
  Column_PK_LP = getColumnPKLP(tbRel.idLP)
  where_ISRT = getWhereISRT(tbRel.idLP, tbRel.IdTable)
  Open_PK_LC = getOpenPKLC(tbRel.IdTable)
  Into_PK_LP = getIntoPKLP(tbRel.idLP)
  nomePaired = tbRel.paired 'getPaired(tbrel)
  where_Cond_UPDT = getWhereCondUPDT(CompareCmp())
  set_Fields_UPDT = getFieldsUPDT(CompareCmp())
  where_PK_LC = getWherePKLC(tbRel.IdTable)
  set_ISRT_Pair = getSetISRTPair(CompareCmp())
  value_ISRT_Pair = getValueISRTPair(CompareCmp())
  
''  set_Pair_ISRT = getSetPairISRT(CompareCmp())
''  value_Pair_ISRT = getValuePairISRT(CompareCmp())
End Sub

Public Sub changeTagsTrigger(RtbFile As RichTextBox, tbRel As TabRelations)
Dim i As Long
  ' Insert LChild
  changeTag RtbFile, "<creator>", Creator
  changeTag RtbFile, "<TABLE-NAME>", nomeLChild
  changeTag RtbFile, "<LP-TABLE-NAME>", nomeLParent
  changeTag RtbFile, "<LP-KEY-COLUMNS-DECLARE>", Declare_PK_LP
  changeTag RtbFile, "<LC-KEY-COLUMNS-CURS>", Cursor_PK_LC
  changeTag RtbFile, "<LP-KEY-COLUMNS>", Column_PK_LP
  changeTag RtbFile, "<WHERE-CONDITION>", where_ISRT
  changeTag RtbFile, "<LC-KEY-COLUMNS-OPEN>", Open_PK_LC
  changeTag RtbFile, "<LP-KEY-COLUMNS-INTO>", Into_PK_LP
  changeTag RtbFile, "<WHERE-PK-LC>", where_PK_LC
  changeTag RtbFile, "<SET-ISRT-PAIR>", set_ISRT_Pair
  changeTag RtbFile, "<VALUE-ISRT-PAIR>", value_ISRT_Pair
  ' Update Paired
  changeTag RtbFile, "<PAIRED-NAME>", nomePaired
  changeTag RtbFile, "<SET-UPDT-PAIR>", set_Fields_UPDT
  changeTag RtbFile, "<WHERE-PK-PAIR>", where_Cond_UPDT
  ' Delete PChild
''  For i = 1 To UBound(PChild_DLET)
''    changeTag rtbFile, "<DLET-PCHILD>", "DELETE" & vbCrLf _
''                                        & Space(1) & Creator & PChild_DLET(i).NomeTb & vbCrLf & _
''                                        "WHERE" & vbCrLf _
''                                        & Space(1) & PChild_DLET(i).Where_DLET & ";" & vbCrLf & _
''                                        "DBMS_OUTPUT.PUT_LINE('DELETE ON " & PChild_DLET(i).NomeTb & "');" & vbCrLf & _
''                                        "<DLET-PCHILD>"
''  Next i
''  changeTag rtbFile, "<DLET-PCHILD>", ""
End Sub

Public Function getCreator(IdTable As Long) As String
Dim rsTable As Recordset

  Set rsTable = m_fun.Open_Recordset("select creator from " & PrefDb & "_tabelle where idtable = " & IdTable)
  If Not rsTable.EOF Then
    getCreator = IIf(Len(Trim(rsTable!Creator)), rsTable!Creator & ".", "")
  End If
  rsTable.Close
End Function

Private Function getDeclPKLP(idLParent As Long) As String
  Dim rsKeyParent As Recordset
  
  Set rsKeyParent = m_fun.Open_Recordset( _
                    "select distinct c.ordinale, c.nome, c.tipo, c.lunghezza, c.decimali from " & PrefDb & "_Index as a, " & _
                                                   PrefDb & "_IdxCol as b, " & _
                                                   PrefDb & "_colonne as c where " & _
                    "a.idtable = " & idLParent & " and a.idindex = b.idindex and " & _
                    "b.idcolonna = c.idcolonna " & _
                    "order by c.ordinale")
  Do Until rsKeyParent.EOF
    getDeclPKLP = getDeclPKLP & IIf(Len(Trim(getDeclPKLP)), vbCrLf, "") & _
                  "H_" & rsKeyParent!Nome & Space(3) & _
                  rsKeyParent!Tipo & "(" & rsKeyParent!lunghezza & _
                  IIf(rsKeyParent!Decimali = 0, "", "," & rsKeyParent!Decimali) & ");"
    rsKeyParent.MoveNext
  Loop
  rsKeyParent.Close
End Function

Private Function getCursPKLC(idLChild As Long) As String
  Dim rsKeyChild As Recordset
  
  Set rsKeyChild = m_fun.Open_Recordset( _
                   "select c.nome, c.tipo from " & PrefDb & "_Index as a, " & _
                                                  PrefDb & "_IdxCol as b, " & _
                                                 PrefDb & "_colonne as c where " & _
                   "a.idtable = " & idLChild & " and a.idindex = b.idindex and " & _
                   "b.idcolonna = c.idcolonna and a.tipo = 'P' and " & _
                   "c.idtable = c.idtablejoin " & _
                   "order by c.ordinale")
  Do Until rsKeyChild.EOF
    getCursPKLC = getCursPKLC & IIf(Len(Trim(getCursPKLC)), ",", "") & rsKeyChild!Nome & " " & rsKeyChild!Tipo
    rsKeyChild.MoveNext
  Loop
  rsKeyChild.Close
End Function

Private Function getColumnPKLP(idLParent As Long) As String
  Dim rsKeyParent As Recordset
  
  Set rsKeyParent = m_fun.Open_Recordset( _
                    "select c.nome, c.tipo from " & PrefDb & "_Index as a, " & _
                                                   PrefDb & "_IdxCol as b, " & _
                                                  PrefDb & "_colonne as c where " & _
                    "a.idtable = " & idLParent & " and a.tipo = 'P' And " & _
                    "a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
                    "order by c.ordinale")
  Do Until rsKeyParent.EOF
    getColumnPKLP = getColumnPKLP & IIf(Len(Trim(getColumnPKLP)), ",", "") & rsKeyParent!Nome
    rsKeyParent.MoveNext
  Loop
  rsKeyParent.Close
End Function

Private Function getWhereISRT(idLParent As Long, idLChild As Long) As String
  Dim rsKeyParent As Recordset, rsKeyChild As Recordset
  Dim TbCmp() As cmpCol
  Dim cmpGroup As String, LenCmp As Long
  
  getWhereISRT = ""
  Set rsKeyParent = m_fun.Open_Recordset( _
                    "select distinct c.ordinale as cont, c.* from " & PrefDb & "_Index as a, " & _
                                                   PrefDb & "_IdxCol as b, " & _
                                                  PrefDb & "_colonne as c where " & _
                    "a.idtable = " & idLParent & " and a.idindex = b.idindex and " & _
                    "b.idcolonna = c.idcolonna " & _
                    "order by c.ordinale")
  Set rsKeyChild = m_fun.Open_Recordset( _
                   "select c.* from " & PrefDb & "_Index as a, " & _
                                                  PrefDb & "_IdxCol as b, " & _
                                                 PrefDb & "_colonne as c where " & _
                   "a.idtable = " & idLChild & " and a.idindex = b.idindex and " & _
                   "b.idcolonna = c.idcolonna and a.tipo = 'P' and " & _
                   "c.idtable = c.idtablejoin " & _
                   "order by c.ordinale")
                    
  Do Until rsKeyParent.EOF Or rsKeyChild.EOF
    ' Se i campi sono uguali
    If rsKeyParent!lunghezza = rsKeyChild!lunghezza Then
       getWhereISRT = getWhereISRT & _
                      IIf(Len(Trim(getWhereISRT)), " AND " & vbCrLf, "") & _
                      rsKeyParent!Nome & " = " & varPref & rsKeyChild!Nome
    ' Se i campi hanno lunghezza diversa: creo un gruppo di campi separati da "||"
      If Not rsKeyParent.EOF Then
        rsKeyParent.MoveNext
      End If
      If Not rsKeyChild.EOF Then
        rsKeyChild.MoveNext
      End If
    ElseIf rsKeyParent!lunghezza > rsKeyChild!lunghezza Then
      LenCmp = 0
      Do Until LenCmp = rsKeyParent!lunghezza Or rsKeyChild.EOF
        LenCmp = LenCmp + rsKeyChild!lunghezza
        cmpGroup = cmpGroup & _
                   IIf(Len(Trim(cmpGroup)), "||", "") & _
                   varPref & rsKeyChild!Nome
        rsKeyChild.MoveNext
      Loop
      getWhereISRT = getWhereISRT & _
                     IIf(Len(Trim(getWhereISRT)), " AND " & vbCrLf, "") & _
                     rsKeyParent!Nome & " = " & cmpGroup
      rsKeyParent.MoveNext
    Else
      LenCmp = 0
      Do Until LenCmp = rsKeyChild!lunghezza Or rsKeyParent.EOF
        LenCmp = LenCmp + rsKeyParent!lunghezza
        cmpGroup = cmpGroup & _
                   IIf(Len(Trim(cmpGroup)), "||", "") & _
                   rsKeyParent!Nome
        rsKeyParent.MoveNext
      Loop
      getWhereISRT = getWhereISRT & _
                     IIf(Len(Trim(getWhereISRT)), " AND " & vbCrLf, "") & _
                     cmpGroup & " = " & varPref & rsKeyChild!Nome
      rsKeyChild.MoveNext
    End If
  Loop
  rsKeyParent.Close
  rsKeyChild.Close
End Function

Private Function getOpenPKLC(idLChild As Long) As String
Dim rsKeyChild As Recordset

  Set rsKeyChild = m_fun.Open_Recordset( _
                   "select c.nome, c.tipo from " & PrefDb & "_Index as a, " & _
                                                  PrefDb & "_IdxCol as b, " & _
                                                 PrefDb & "_colonne as c where " & _
                   "a.idtable = " & idLChild & " and a.idindex = b.idindex and " & _
                   "b.idcolonna = c.idcolonna and a.tipo = 'P' and " & _
                   "c.idtable = c.idtablejoin " & _
                   "order by c.ordinale")
  Do Until rsKeyChild.EOF
    getOpenPKLC = getOpenPKLC & IIf(Len(Trim(getOpenPKLC)), ",", "") & varPref & rsKeyChild!Nome
    rsKeyChild.MoveNext
  Loop
  rsKeyChild.Close

End Function

Private Function getIntoPKLP(idLParent As Long) As String
  Dim rsKeyParent As Recordset
  
  Set rsKeyParent = m_fun.Open_Recordset( _
                    "select c.nome, c.tipo from " & PrefDb & "_Index as a, " & _
                                                   PrefDb & "_IdxCol as b, " & _
                                                  PrefDb & "_colonne as c where " & _
                    "a.idtable = " & idLParent & " and a.tipo = 'P' and " & _
                    "a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
                    "order by c.ordinale")
  Do Until rsKeyParent.EOF
    getIntoPKLP = getIntoPKLP & IIf(Len(Trim(getIntoPKLP)), ",", "") & "H_" & rsKeyParent!Nome
    rsKeyParent.MoveNext
  Loop
  rsKeyParent.Close
End Function

Private Function getWhereCondUPDT(arrCmp() As cmpCol) As String
Dim cmpGroup As String
Dim cmpSingle As String
Dim i As Long

  For i = 1 To UBound(arrCmp)
    If arrCmp(i).Index Then
      If arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Then
        getWhereCondUPDT = IIf(Len(Trim(getWhereCondUPDT)), _
                             getWhereCondUPDT & " AND " & vbCrLf & Space(1) & arrCmp(i).nomeCmp2 & " = " & varPref & arrCmp(i).nomeCmp1, _
                             Space(1) & arrCmp(i).nomeCmp2 & " = " & varPref & arrCmp(i).nomeCmp1)
      Else
        cmpGroup = ""
        cmpSingle = ""
        If arrCmp(i).lenCmp1 > arrCmp(i).lenCmp2 Then
          cmpSingle = varPref & arrCmp(i).nomeCmp1
          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp) Or Not arrCmp(i).Index
            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & arrCmp(i).nomeCmp2, arrCmp(i).nomeCmp2)
            i = i + 1
          Loop
        Else
          cmpSingle = arrCmp(i).nomeCmp2
          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp) Or Not arrCmp(i).Index
            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & varPref & arrCmp(i).nomeCmp1, varPref & arrCmp(i).nomeCmp1)
            i = i + 1
          Loop
        End If
        getWhereCondUPDT = IIf(Len(Trim(getWhereCondUPDT)), _
                             getWhereCondUPDT & " AND " & vbCrLf & Space(1) & cmpSingle & " = " & cmpGroup, _
                             Space(1) & cmpSingle & " = " & cmpGroup)
        If i < UBound(arrCmp) And arrCmp(i).Index Then
          getWhereCondUPDT = getWhereCondUPDT & " AND " & vbCrLf & Space(1) & arrCmp(i).nomeCmp2 & " = " & varPref & arrCmp(i).nomeCmp1
        End If
      End If
    End If
  Next i
End Function

Private Function getFieldsUPDT(arrCmp() As cmpCol) As String
Dim cmpCobol As String
Dim cmpGroup As String
Dim cmpSingle As String
Dim LenCmp As Long
Dim i As Long
  
  getFieldsUPDT = ""
  For i = 1 To UBound(arrCmp)
    If Not arrCmp(i).Index Then
      cmpCobol = varPref & arrCmp(i).nomeCmp1
      If arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Then
        getFieldsUPDT = getFieldsUPDT & _
                        IIf(Len(Trim(getFieldsUPDT)), " , " & vbCrLf, "") & _
                        Space(1) & arrCmp(i).nomeCmp2 & " = " & cmpCobol
      Else
        cmpGroup = ""
        cmpSingle = ""
        If arrCmp(i).lenCmp1 > arrCmp(i).lenCmp2 Then
          ' Scomporre con SUBSTR
          LenCmp = 1
          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp)
            getFieldsUPDT = getFieldsUPDT & _
                            IIf(Len(Trim(getFieldsUPDT)), " , " & vbCrLf, "") & _
                            Space(1) & arrCmp(i).nomeCmp2 & " = SUBSTR(" & varPref & arrCmp(i).nomeCmp1 & "," & LenCmp & "," & arrCmp(i).lenCmp2 & ")"
            LenCmp = LenCmp + arrCmp(i).lenCmp2
            i = i + 1
          Loop
        Else
          ' Basta associarlo con "||"
          cmpSingle = arrCmp(i).nomeCmp2
          Do Until arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Or i = UBound(arrCmp)
            cmpCobol = varPref & arrCmp(i).nomeCmp1
            cmpGroup = IIf(Len(Trim(cmpGroup)), cmpGroup & "||" & cmpCobol, cmpCobol)
            i = i + 1
          Loop
          getFieldsUPDT = getFieldsUPDT & _
                          IIf(Len(Trim(getFieldsUPDT)), " , " & vbCrLf, "") & _
                          Space(1) & cmpSingle & " = " & cmpGroup
        End If
        If i < UBound(arrCmp) Then
          cmpCobol = varPref & arrCmp(i).nomeCmp1
          getFieldsUPDT = getFieldsUPDT & " , " & vbCrLf & Space(1) & arrCmp(i).nomeCmp2 & " = " & cmpCobol
        End If
      End If
    End If
  Next i
End Function

Private Function getSetISRTPair(arrCmp() As cmpCol) As String
Dim i As Long
  
  getSetISRTPair = ""
  For i = 1 To UBound(arrCmp)
    If arrCmp(i).nomeCmp2 <> arrCmp(i - 1).nomeCmp2 Then
      getSetISRTPair = getSetISRTPair & _
                       IIf(Len(Trim(getSetISRTPair)), "," & vbCrLf, "") & _
                        Space(1) & arrCmp(i).nomeCmp2
    End If
  Next i
End Function

Private Function getValueISRTPair(arrCmp() As cmpCol) As String
Dim i As Long
Dim LenCmp As Long
  
  getValueISRTPair = ""
  For i = 1 To UBound(arrCmp)
    If arrCmp(i).lenCmp1 <= arrCmp(i).lenCmp2 Then
      LenCmp = 1
      If arrCmp(i).nomeCmp2 <> arrCmp(i - 1).nomeCmp2 Then
        getValueISRTPair = getValueISRTPair & _
                           IIf(Len(Trim(getValueISRTPair)), "," & vbCrLf, "") & _
                           Space(1) & varPref & arrCmp(i).nomeCmp1
      Else
        getValueISRTPair = getValueISRTPair & _
                           IIf(Len(Trim(getValueISRTPair)), "||" & varPref & arrCmp(i).nomeCmp1, _
                           Space(1) & varPref & arrCmp(i).nomeCmp1)

      End If
    Else
      If arrCmp(i).nomeCmp1 <> arrCmp(i - 1).nomeCmp1 Then
        LenCmp = 1
      End If
      getValueISRTPair = getValueISRTPair & _
                         IIf(Len(Trim(getValueISRTPair)), "," & vbCrLf, "") & _
                          Space(1) & "SUBSTR(" & varPref & arrCmp(i).nomeCmp1 & "," & LenCmp & "," & arrCmp(i).lenCmp2 & ")"
      LenCmp = LenCmp + arrCmp(i).lenCmp2
    End If
  Next i
End Function

Private Sub searchPC_DLET(tbRel As TabRelations)
Dim rsLChild As Recordset
Dim i As Long, a As Long
Dim LogicRel As Boolean
Dim rsSegment As Recordset

  ReDim PChild_DLET(0)
  a = 0
  For i = 1 To UBound(tbRel.idPC)
    LogicRel = False
    Set rsLChild = m_fun.Open_Recordset("select a.nome, a.idsegmento, a.lparent from psdli_segmenti as a, " & PrefDb & "_tabelle  as b where " & _
                                        "b.idtable = " & tbRel.idPC(i) & " and " & _
                                        "b.idorigine = a.idsegmento")
    ' Dovrebbe essercene solo 1
    If Not rsLChild.EOF Then
      ' Se � un segmento LChild
      If Len(Trim(rsLChild!LParent)) > 0 Then
        LogicRel = True
      End If
      ' Se � un segmento LParent
      Set rsSegment = m_fun.Open_Recordset("select nome from PSDli_segmenti where LParent = '" & rsLChild!Nome & "'")
      If Not rsSegment.EOF Then
        LogicRel = True
      End If
      rsSegment.Close
    End If
    If LogicRel Then
      a = a + 1
      ReDim Preserve PChild_DLET(a)
      PChild_DLET(a).NomeTb = tbRel.PC(i)
      PChild_DLET(a).idTb = tbRel.idPC(i)
    End If
    rsLChild.Close
  Next i

  'getTagDLET tbRel
End Sub

Private Sub getTagDLET(tbRel As TabRelations)
Dim rsPChild As Recordset, rsPParent As Recordset
Dim i As Long

  For i = 1 To UBound(PChild_DLET)
    Set rsPChild = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                        "idtable <> idtablejoin and " & _
                                        "idtable = " & PChild_DLET(i).idTb & " and " & _
                                        "idcmp > 0 " & _
                                        "order by ordinale")
    Do Until rsPChild.EOF
      Set rsPParent = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                           "idtable = " & tbRel.IdTable & " and " & _
                                           "idtablejoin = " & rsPChild!idTableJoin & " and " & _
                                           "idcmp = " & rsPChild!IdCmp)
      ' Dovrebbe essercene solo 1
      If Not rsPParent.EOF Then
        PChild_DLET(i).Where_DLET = PChild_DLET(i).Where_DLET & _
                                    IIf(Len(Trim(PChild_DLET(i).Where_DLET)), " AND " & vbCrLf & Space(1), "") _
                                    & rsPChild!Nome & " = " & varPref & rsPParent!Nome
      End If
      rsPParent.Close
      rsPChild.MoveNext
    Loop
    rsPChild.Close
  Next i
End Sub

Private Function getWherePKLC(idTb As Long) As String
Dim rsIndex As Recordset, rsColonne As Recordset
  
  getWherePKLC = ""
  Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where idtable = " & idTb)
  Do Until rsIndex.EOF
    Set rsColonne = m_fun.Open_Recordset("select a.nome from " & PrefDb & "_colonne as a, " & PrefDb & "_idxcol as b where " & _
                                         "b.idindex = " & rsIndex!IdIndex & " and " & _
                                         "b.idtable = " & idTb & " and " & _
                                         "a.idcolonna = b.idcolonna " & _
                                         "order by a.ordinale")
    Do Until rsColonne.EOF
      getWherePKLC = getWherePKLC & _
                     IIf(Len(Trim(getWherePKLC)), " AND " & vbCrLf, "") & _
                     Space(1) & rsColonne!Nome & " = " & varPref & rsColonne!Nome
      rsColonne.MoveNext
    Loop
    rsColonne.Close
    rsIndex.MoveNext
  Loop
  rsIndex.Close
End Function
'''
'''Private Function getSetISRTPair(arrCmp() As cmpCol) As String
'''Dim i As Long
'''
'''  getSetISRTPair = ""
'''  For i = 1 To UBound(arrCmp)
'''    If arrCmp(i).nomeCmp2 <> arrCmp(i - 1).nomeCmp2 Then
'''      getSetISRTPair = getSetISRTPair & IIf(Len(Trim(getSetISRTPair)), ", ", "") & arrCmp(i).nomeCmp2
'''    End If
'''  Next i
'''End Function
'''
'''Private Function getValueISRTPair(arrCmp() As cmpCol) As String
'''Dim i As Long
'''
'''  getValueISRTPair = ""
'''  For i = 1 To UBound(arrCmp)
'''    If arrCmp(i).lenCmp1 = arrCmp(i).lenCmp2 Then
'''      getValueISRTPair = getValueISRTPair & IIf(Len(Trim(getValueISRTPair)), ", ", "") & varPref & arrCmp(i).nomeCmp1
'''    ElseIf arrCmp(i).lenCmp1 > arrCmp(i).lenCmp2 Then
'''      ' Divido i campi con la SUBSTR
'''
'''    Else
'''
'''    End If
'''  Next i
'''
'''End Function
