VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadmdF_Index 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Index Tools :"
   ClientHeight    =   7020
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   10455
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7020
   ScaleWidth      =   10455
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.OptionButton optDescending 
      Caption         =   "Descending"
      ForeColor       =   &H00C00000&
      Height          =   195
      Left            =   9180
      TabIndex        =   53
      Top             =   3480
      Width           =   1215
   End
   Begin VB.OptionButton optAscending 
      Caption         =   "Ascending"
      ForeColor       =   &H00C00000&
      Height          =   195
      Left            =   9180
      TabIndex        =   52
      Top             =   3240
      Width           =   1095
   End
   Begin VB.ComboBox cboColumns 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      ItemData        =   "MadmdF_Index.frx":0000
      Left            =   7950
      List            =   "MadmdF_Index.frx":0002
      TabIndex        =   46
      Top             =   30
      Visible         =   0   'False
      Width           =   2445
   End
   Begin VB.CommandButton cmdEraseColumn 
      Height          =   345
      Left            =   5430
      Picture         =   "MadmdF_Index.frx":0004
      Style           =   1  'Graphical
      TabIndex        =   45
      Top             =   60
      Width           =   375
   End
   Begin VB.CommandButton cmdEraseIndex 
      Height          =   345
      Left            =   480
      Picture         =   "MadmdF_Index.frx":014E
      Style           =   1  'Graphical
      TabIndex        =   44
      Top             =   60
      Width           =   375
   End
   Begin VB.CommandButton cmdNewColumn 
      Height          =   345
      Left            =   5010
      Picture         =   "MadmdF_Index.frx":0298
      Style           =   1  'Graphical
      TabIndex        =   43
      Top             =   60
      Width           =   375
   End
   Begin VB.CommandButton cmdNewIndex 
      Height          =   345
      Left            =   60
      Picture         =   "MadmdF_Index.frx":03E2
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   60
      Width           =   375
   End
   Begin VB.TextBox txtToForeign 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      Left            =   4980
      TabIndex        =   40
      Top             =   2850
      Visible         =   0   'False
      Width           =   2325
   End
   Begin VB.ComboBox cboForeignCol 
      BackColor       =   &H00FFFFC0&
      ForeColor       =   &H00C00000&
      Height          =   375
      ItemData        =   "MadmdF_Index.frx":052C
      Left            =   7950
      List            =   "MadmdF_Index.frx":052E
      TabIndex        =   39
      Top             =   2850
      Visible         =   0   'False
      Width           =   2445
   End
   Begin VB.Frame Frame2 
      Caption         =   "Column Details"
      ForeColor       =   &H00000080&
      Height          =   3285
      Left            =   4980
      TabIndex        =   16
      Top             =   3660
      Width           =   5415
      Begin VB.TextBox TxtKeyTemplate 
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   4080
         TabIndex        =   48
         Top             =   2040
         Width           =   1155
      End
      Begin VB.TextBox txtNomeCol 
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1245
         TabIndex        =   27
         Top             =   300
         Width           =   2265
      End
      Begin VB.TextBox TxtWrite 
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   4095
         TabIndex        =   26
         Top             =   1140
         Width           =   1155
      End
      Begin VB.TextBox TxtRead 
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   4110
         TabIndex        =   25
         Top             =   1590
         Width           =   1155
      End
      Begin VB.TextBox TxTLoad 
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   4095
         TabIndex        =   24
         Top             =   690
         Width           =   1155
      End
      Begin VB.ComboBox CmbNull 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   360
         ItemData        =   "MadmdF_Index.frx":0530
         Left            =   4095
         List            =   "MadmdF_Index.frx":053A
         TabIndex        =   23
         Text            =   "CmbNull"
         Top             =   270
         Width           =   1155
      End
      Begin VB.TextBox txtDescrizioneCol 
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H00C00000&
         Height          =   615
         Left            =   1245
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   22
         Top             =   2520
         Width           =   4035
      End
      Begin VB.TextBox txtLabel 
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1245
         MaxLength       =   255
         TabIndex        =   21
         Text            =   " "
         Top             =   2040
         Width           =   2265
      End
      Begin VB.TextBox txtDim 
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1245
         MaxLength       =   9
         TabIndex        =   20
         Text            =   " "
         Top             =   1170
         Width           =   945
      End
      Begin VB.TextBox txtDecimal 
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   2895
         MaxLength       =   1
         TabIndex        =   19
         Text            =   " "
         Top             =   1170
         Width           =   615
      End
      Begin VB.ComboBox CmbFormat 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         ForeColor       =   &H00C00000&
         Height          =   315
         ItemData        =   "MadmdF_Index.frx":0547
         Left            =   1245
         List            =   "MadmdF_Index.frx":0549
         TabIndex        =   18
         Text            =   "CmbFormat"
         Top             =   1620
         Width           =   2265
      End
      Begin VB.ComboBox cboType 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   360
         ItemData        =   "MadmdF_Index.frx":054B
         Left            =   1230
         List            =   "MadmdF_Index.frx":054D
         TabIndex        =   17
         Top             =   750
         Width           =   2265
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Description"
         ForeColor       =   &H00C00000&
         Height          =   315
         Left            =   120
         TabIndex        =   50
         Top             =   2520
         Width           =   975
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Tr. K"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   3615
         TabIndex        =   49
         Top             =   2070
         Width           =   345
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Tr. R"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   3600
         TabIndex        =   37
         Top             =   1620
         Width           =   360
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Field Name"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   180
         TabIndex        =   36
         Top             =   360
         Width           =   915
      End
      Begin VB.Label Label41 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Load"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   3585
         TabIndex        =   35
         Top             =   780
         Width           =   375
      End
      Begin VB.Label Label33 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Label"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   570
         TabIndex        =   34
         Top             =   2145
         Width           =   525
      End
      Begin VB.Label Label30 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Null"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   3600
         TabIndex        =   33
         Top             =   330
         Width           =   360
      End
      Begin VB.Label Label29 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Length"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   540
         TabIndex        =   32
         Top             =   1230
         Width           =   555
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Decimal"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   2235
         TabIndex        =   31
         Top             =   1230
         Width           =   645
      End
      Begin VB.Label Label28 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Type"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   705
         TabIndex        =   30
         Top             =   780
         Width           =   390
      End
      Begin VB.Label Label31 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Format"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   510
         TabIndex        =   29
         Top             =   1710
         Width           =   585
      End
      Begin VB.Label Label42 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Tr. W"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   3555
         TabIndex        =   28
         Top             =   1230
         Width           =   405
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Index Details"
      ForeColor       =   &H00000080&
      Height          =   4095
      Left            =   60
      TabIndex        =   1
      Top             =   2850
      Width           =   4815
      Begin VB.ComboBox cboTables 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   360
         ItemData        =   "MadmdF_Index.frx":054F
         Left            =   2520
         List            =   "MadmdF_Index.frx":055C
         TabIndex        =   14
         Top             =   1050
         Visible         =   0   'False
         Width           =   2205
      End
      Begin VB.ComboBox cboTypeIndex 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   360
         ItemData        =   "MadmdF_Index.frx":0581
         Left            =   630
         List            =   "MadmdF_Index.frx":058E
         TabIndex        =   13
         Top             =   1050
         Width           =   1875
      End
      Begin VB.TextBox txtNomeIdx 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   630
         MaxLength       =   18
         TabIndex        =   7
         Top             =   330
         Width           =   4065
      End
      Begin VB.TextBox txtDescrizioneIdx 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   1185
         Left            =   1380
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   6
         Top             =   2760
         Width           =   3315
      End
      Begin VB.TextBox txtMdDateIdx 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1395
         MaxLength       =   18
         TabIndex        =   5
         Top             =   2310
         Width           =   3315
      End
      Begin VB.TextBox txtCrDateIdx 
         BackColor       =   &H00FFFFC0&
         ForeColor       =   &H00C00000&
         Height          =   375
         Left            =   1380
         MaxLength       =   18
         TabIndex        =   4
         Top             =   1860
         Width           =   3315
      End
      Begin VB.CheckBox chkUsed 
         Caption         =   "Used"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   1380
         TabIndex        =   3
         Top             =   1530
         Width           =   885
      End
      Begin VB.CheckBox chkUnique 
         Caption         =   "Unique"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   2280
         TabIndex        =   2
         Top             =   1530
         Width           =   885
      End
      Begin VB.Label lblTables 
         AutoSize        =   -1  'True
         Caption         =   "Select Foreign Table..."
         ForeColor       =   &H00C00000&
         Height          =   225
         Left            =   2550
         TabIndex        =   15
         Top             =   780
         Visible         =   0   'False
         Width           =   1725
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Name"
         ForeColor       =   &H00C00000&
         Height          =   225
         Left            =   150
         TabIndex        =   12
         Top             =   420
         Width           =   435
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Type"
         ForeColor       =   &H00C00000&
         Height          =   225
         Left            =   150
         TabIndex        =   11
         Top             =   1140
         Width           =   375
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Description"
         ForeColor       =   &H00C00000&
         Height          =   225
         Left            =   420
         TabIndex        =   10
         Top             =   2820
         Width           =   900
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Update Date"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   270
         TabIndex        =   9
         Top             =   2370
         Width           =   1050
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Creation Date"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   150
         TabIndex        =   8
         Top             =   1920
         Width           =   1170
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8730
      Top             =   2100
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Index.frx":05B3
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Index.frx":070D
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Index.frx":0867
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Index.frx":09C1
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Index.frx":0B1B
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_Index.frx":0C75
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lswIndici 
      Height          =   2325
      Left            =   30
      TabIndex        =   0
      Top             =   450
      Width           =   4845
      _ExtentX        =   8546
      _ExtentY        =   4101
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   12582912
      BackColor       =   16777152
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Name"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Type"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Unique"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Used"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView lswColonne 
      Height          =   2325
      Left            =   4980
      TabIndex        =   38
      Top             =   450
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   4101
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   16777152
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Column Name"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Foreign Column"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label32 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Order By:"
      ForeColor       =   &H00C00000&
      Height          =   195
      Left            =   8400
      TabIndex        =   51
      Top             =   3360
      Width           =   720
   End
   Begin VB.Label lblChoseCol 
      AutoSize        =   -1  'True
      Caption         =   "Chose a Indexes' column"
      ForeColor       =   &H00C00000&
      Height          =   225
      Left            =   6030
      TabIndex        =   47
      Top             =   120
      Visible         =   0   'False
      Width           =   1890
   End
   Begin VB.Label lblForeign 
      AutoSize        =   -1  'True
      Caption         =   "<-->"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   240
      Left            =   7410
      TabIndex        =   41
      Top             =   2940
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "MadmdF_Index"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public CurIndexIdClass As Long
Public fLoad As Boolean

Private Sub cboColumns_Click()
  Dim wTipo As String
  Dim wIdx As Long
   
  'recupera l'index della colonna
  wIdx = cboColumns.ItemData(cboColumns.ListIndex)
   
  pIndex_IdxColonna = wIdx
  pNome_IdxColonna = cboColumns.list(cboColumns.ListIndex)
   
  'Recupera il tipo della colonna
  wTipo = get_Tipo_Colonna_By_Id(wIdx)
   
  'Aggiunge la colonna alla lista delle colonne dell'indice
  lswColonne.ListItems.Add , wIdx & "#IDCOL@" & pIndex_Ordinale_IdxCol, cboColumns.list(cboColumns.ListIndex)
   
  If UCase(Trim(cboTypeIndex.text)) <> "FOREIGN-KEY" Then
    lswColonne.ListItems(lswColonne.ListItems.count).ListSubItems.Add , , wTipo
  Else
    lswColonne.ListItems(lswColonne.ListItems.count).ListSubItems.Add , , ""
  End If
   
  'Crea una nuova colonna associata all'indice
  Crea_Nuova_Colonna_Index
    
  lblChoseCol.Visible = False
  cboColumns.Visible = False
End Sub

Private Sub cboForeignCol_Click()
  'Inserisce in lista la colonna foreign associata all'item selezionato
  If lswColonne.ListItems.count > 0 Then
    lswColonne.SelectedItem.ListSubItems(1).text = cboForeignCol.text
  End If
   
  'Update in memoria dell' indice con la colonna Foreign
  Insert_Colonna_Foreign cboForeignCol.ItemData(cboForeignCol.ListIndex)
End Sub

Public Sub Insert_Colonna_Foreign(IdColonnaForeign As Long)
  Dim rs As Recordset
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Idxcol where " & _
                                "IdIndex = " & pIndex_IndiceCorrente & " and " & _
                                "Ordinale = " & pIndex_Ordinale_IdxCol & " and " & _
                                "IdTable = " & pIndex_TbCorrente & " and " & _
                                "IdColonna = " & pIndex_IdxColonna)
  If rs.RecordCount Then
    rs!forIdColonna = IdColonnaForeign
    rs.Update
  Else
    'Errore : Deve essersi perso l'indice del db
    m_fun.WriteLog "+------------------------------------------------------------+" & vbCrLf & Space(22) & _
                   "|                                                            |" & vbCrLf & Space(22) & _
                   "| Data Manager - Critical Error: No Id index Found...        |" & vbCrLf & Space(22) & _
                   "|                                                            |" & vbCrLf & Space(22) & _
                   "| Routine : MadmdP_00.MadmdF_Index.                          |" & vbCrLf & Space(22) & _
                   "|           Insert_Colonna_Foreign(IdColonnaForeign As Long) |" & vbCrLf & Space(22) & _
                   "|                                                            |" & vbCrLf & Space(22) & _
                   "+------------------------------------------------------------+"
  End If
  rs.Close
End Sub

Private Sub cboTables_Click()
  Dim rs As Recordset
  
  'Memorizza l'id della tabella in Join
  pIndex_ForeignTable = cboTables.ItemData(cboTables.ListIndex)
  pNome_ForeignTable = cboTables.list(cboTables.ListIndex)
     
  Set rs = m_fun.Open_Recordset("select foreignidtable from " & get_PrefixQuery_TipoDB(TipoDB) & "_index where " & _
                                "idindex = " & pIndex_IndiceCorrente & " and idtable = " & pIndex_TbCorrente)
  If rs.RecordCount Then
    rs!ForeignIdTable = pIndex_ForeignTable
    rs.Update
  End If
  rs.Close
  
  'Carica le colonne correlate a questa tabella
  Load_Lista_Colonne_In_Combo cboForeignCol, TipoDB, pIndex_ForeignTable
End Sub

Private Sub cboTypeIndex_Click()
  Dim rs As Recordset
  Dim Adesso As String
   
  If Not fLoad Then pStatus_Index = True
   
  If Trim(UCase(cboTypeIndex.text)) = "FOREIGN-KEY" Then
    cboTables.Visible = True
    lblTables.Visible = True
     
    lswColonne.ColumnHeaders(2).text = "Foreign Column"
     
    'Carica la lista delle tabelle disponibili sotto il database
    Load_Lista_Tabelle_In_Combo cboTables, TipoDB, pIndex_DBCorrente
     
    lblForeign.Visible = True
    txtToForeign.Visible = True
     
    cboForeignCol.Visible = True
  Else
    cboTables.Visible = False
    lblTables.Visible = False
    cboForeignCol.Visible = False
    lblForeign.Visible = False
    txtToForeign.Visible = False
     
    lswColonne.ColumnHeaders(2).text = "Type"
     
    'Deve cancellare le colonne foreign e tutti i riferimenti
    'Forse;perch� in un secondo momento rivalorizzando la chiave a Foreign potrebbe
    'venir comodo riavere tutte le colonne gi� impostate in precedenza
  End If
  
  'Update nel DB
  Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Index where " & _
                                "idIndex = " & pIndex_IndiceCorrente)
  If rs.RecordCount Then
    Adesso = Now
     
    Select Case Trim(UCase(cboTypeIndex.text))
      Case "FOREIGN-KEY"
        rs!Tipo = "F"
      Case "PRIMARY-KEY"
        rs!Tipo = "P"
      Case "INDEX"
        rs!Tipo = "I"
    End Select
    
    rs!Data_Modifica = Adesso
    rs.Update
    
    txtMdDateIdx.text = Adesso
  Else
    'Errore : Deve essersi perso l'indice del db
    m_fun.WriteLog "+---------------------------------------------------------+" & vbCrLf & Space(22) & _
                   "|                                                         |" & vbCrLf & Space(22) & _
                   "| Data Manager - Critical Error : No Id index Found...    |" & vbCrLf & Space(22) & _
                   "|                                                         |" & vbCrLf & Space(22) & _
                   "| Routine : MadmdP_00.MadmdF_Index.cboTypeIndex_Click()   |" & vbCrLf & Space(22) & _
                   "|                                                         |" & vbCrLf & Space(22) & _
                   "+---------------------------------------------------------+"
  End If
  rs.Close
End Sub

Private Sub chkUnique_Click()
  Dim rs As Recordset
  Dim Adesso As String
  
  If Not fLoad Then
    pStatus_Index = True
  End If
   
  'Va in update sul db
  Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Index Where " & _
                                "IdIndex = " & pIndex_IndiceCorrente)
  If rs.RecordCount Then
    Adesso = Now
    
    rs!Used = CBool(chkUnique.Value)
    rs!Data_Modifica = Adesso
    rs.Update
     
    txtMdDateIdx.text = Adesso
  Else
     'Errore : Deve essersi perso l'indice del db
     m_fun.WriteLog "+------------------------------------------------------+" & vbCrLf & Space(22) & _
                    "|                                                      |" & vbCrLf & Space(22) & _
                    "| Data Manager - Critical Error : No Id index Found... |" & vbCrLf & Space(22) & _
                    "|                                                      |" & vbCrLf & Space(22) & _
                    "| Routine : MadmdP_00.MadmdF_Index.chkUnique_Click()   |" & vbCrLf & Space(22) & _
                    "|                                                      |" & vbCrLf & Space(22) & _
                    "+------------------------------------------------------+"
  End If
  rs.Close
End Sub

Private Sub chkUsed_Click()
  Dim rs As Recordset
  Dim Adesso As String
   
  If Not fLoad Then
    pStatus_Index = True
  End If
   
  'Va in update sul db
  Set rs = m_fun.Open_Recordset("Select * From " & get_PrefixQuery_TipoDB(TipoDB) & "_Index " & _
                                "Where IdIndex = " & pIndex_IndiceCorrente)
  If rs.RecordCount Then
    Adesso = Now
    
    rs!Used = CBool(chkUsed.Value)
    rs!Data_Modifica = Adesso
    
    rs.Update
    
    txtMdDateIdx.text = Adesso
  Else
    'Errore : Deve essersi perso l'indice del db
    m_fun.WriteLog "+------------------------------------------------------+" & vbCrLf & Space(22) & _
                   "|                                                      |" & vbCrLf & Space(22) & _
                   "| Data Manager - Critical Error : No Id index Found... |" & vbCrLf & Space(22) & _
                   "|                                                      |" & vbCrLf & Space(22) & _
                   "| Routine : MadmdP_00.MadmdF_Index.chkUsed_Click()     |" & vbCrLf & Space(22) & _
                   "|                                                      |" & vbCrLf & Space(22) & _
                   "+------------------------------------------------------+"
  End If
  rs.Close
End Sub

Private Sub cmdEraseColumn_Click()
  Dim i As Long, iSelected As Boolean
   
  If fLoad = False Then
    pStatus_Index = True
  End If
   
  '****************************************************************************'
  '*****                                                                  *****'
  '***** 1) Elimina la colonna riferita all'indice (nella tabella idxcol) *****'
  '*****                                                                  *****'
  '***** 2) Prende l'id index corrente e se non � pi� presente in idxcol  *****'
  '*****    Elimina anche l'indice nella tabella Index                    *****'
  '*****                                                                  *****'
  '****************************************************************************'
   
  For i = 1 To lswColonne.ListItems.count
    If lswColonne.ListItems(i).Selected Then
      iSelected = True
      Exit For
    End If
  Next i
   
  If iSelected Then
    'Nel database
    Elimina_Colonna_Indice Mid(lswColonne.SelectedItem.Key, 1, InStr(1, lswColonne.SelectedItem.Key, "#IDCOL@") - 1)
    
    'a video
    lswColonne.ListItems.Remove lswColonne.SelectedItem.Index
  End If
End Sub

Private Sub cmdEraseIndex_Click()
  Dim wScelta As Long
   
  If Not fLoad Then
    pStatus_Index = True
  End If
   
  wScelta = MsgBox("Do you want to erase current Index?", vbQuestion + vbYesNo, DataMan.DmNomeProdotto)
  If wScelta = vbYes Then
    'Elimina nel database
    Elimina_Indice pIndex_IndiceCorrente
     
    'Elimina a video
    lswColonne.ListItems.Clear
    lswIndici.ListItems.Remove lswIndici.SelectedItem.Index
  End If
End Sub

Private Sub cmdNewColumn_Click()
  If Not fLoad Then
    pStatus_Index = True
  End If
   
  lblChoseCol.Visible = True
  cboColumns.Visible = True
   
  If Trim(UCase(cboTypeIndex.text)) = "FOREIGN_KEY" Then
    lblForeign.Visible = True
    txtToForeign.Visible = True
    
    cboForeignCol.Visible = True
  End If
End Sub

Private Sub cmdNewIndex_Click()
  'Crea un nuovo indice
  If Not fLoad Then
    pStatus_Index = True
  End If
  
  Crea_Nuovo_Indice
End Sub

Private Sub Form_Load()
   fLoad = True
   
   pStatus_Index = False
   
   'Setta il parent della form
   'SetParent Me.hwnd, DataMan.DmParent
  
   'ResizeControls Me
   
   Me.Caption = "DB-Engine: " & TipoDB & " - Indexes Tools - Table : [" & pNome_TBCorrente & "] - DB : [" & pNome_DBCorrente & "]"

   lswColonne.ColumnHeaders(1).Width = lswColonne.Width / 2
   lswColonne.ColumnHeaders(2).Width = lswColonne.Width / 2
   
   'Carica Lalista degli indici riferiti alla tabella selezionata
   Load_Indici_In_Lista
   
   'Carico Colonne relative all'indice:
   If lswIndici.ListItems.count > 0 Then
   'SQ: ripristinare...
'      lswIndici_ItemClick lswIndici.ListItems(1)
'
'      If lswColonne.ListItems.Count > 0 Then
'         lswColonne_ItemClick lswColonne.ListItems(1)
'      End If
   End If
   
   'Carica le colonne della tabella corrente nella combo
   Load_Lista_Colonne_In_Combo cboColumns, TipoDB, pIndex_TbCorrente
   
   'Carica Combo tipo secondo il tipo DB
   Carica_Combo_Tipo TipoDB, cboType
   
   fLoad = False
   
   m_fun.AddActiveWindows Me
End Sub

Public Sub Load_Indici_In_Lista()
  Dim i As Long, j As Long
  Dim ImgIndex As Long
  Dim wTipo As String
  Dim rs As Recordset
   
  lswIndici.ListItems.Clear
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index where " & _
                                "IdTable = " & pIndex_TbCorrente)
  While Not rs.EOF
    Select Case Trim(UCase(rs!Tipo))
      Case "P"
        ImgIndex = 1
        wTipo = "PRIMARY-KEY"
      Case "F"
        ImgIndex = 2
        wTipo = "FOREIGN-KEY"
      Case "I"
        ImgIndex = 3
        wTipo = "INDEX"
      Case Else
        ImgIndex = -1
        wTipo = "UNDEFINED"
    End Select
    
    If ImgIndex <> -1 Then
      lswIndici.ListItems.Add , rs!IdIndex & "#IDIDX", rs!Nome, , ImgIndex
    Else
      lswIndici.ListItems.Add , rs!IdIndex & "#IDIDX", rs!Nome
    End If
    
    lswIndici.ListItems(lswIndici.ListItems.count).ListSubItems.Add , , wTipo
    
    If rs!Unique Then
      lswIndici.ListItems(lswIndici.ListItems.count).ListSubItems.Add , , "Yes"
    Else
      lswIndici.ListItems(lswIndici.ListItems.count).ListSubItems.Add , , "No"
    End If
    
    If rs!Used = True Then
      lswIndici.ListItems(lswIndici.ListItems.count).ListSubItems.Add , , "Yes"
    Else
      lswIndici.ListItems(lswIndici.ListItems.count).ListSubItems.Add , , "No"
    End If
    rs.MoveNext
  Wend
  rs.Close
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  'Richiesta di salvataggio
  Dim wScelta As Long
   
  If pStatus_Index = True Then
    wScelta = MsgBox("Do you want to save this Indexes' structure?", vbQuestion + vbYesNo, DataMan.DmNomeProdotto)
    If wScelta = vbYes Then
      DataMan.DmConnection.CommitTrans
      'Setta a false il flag per il salvataggio delle colonne
      'Perch� in questo caso � stato tutto salvato
      pStatus_Column = False
      'Ricarica la struttura della tabella
      'potrebbe essere cambiata (es. Nomi colonne)
      MadmdF_PropertyTable.Load_Lista_Colonne
    Else
      DataMan.DmConnection.RollbackTrans
    End If
    'Reinit transazione
    DataMan.DmConnection.BeginTrans
  End If
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  m_fun.RemoveActiveWindows Me
   
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub lswColonne_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswColonne.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswColonne.SortOrder = Order
  lswColonne.SortKey = Key - 1
  lswColonne.Sorted = True
End Sub

Private Sub lswColonne_ItemClick(ByVal Item As MSComctlLib.ListItem)
  Dim rs As Recordset
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where " & _
                                "IdColonna = " & Mid(Item.Key, 1, InStr(1, Item.Key, "#") - 1))
  If rs.RecordCount Then
    txtDecimal = TN(rs!Decimali)
    txtDescrizioneCol = TN(rs!Descrizione)
    txtDim = TN(rs!lunghezza)
    txtLabel = TN(rs!Etichetta)
    TxTLoad = TN(rs!RoutLoad)
    txtNomeCol = TN(rs!Nome)
    TxtRead = TN(rs!RoutRead)
    TxtWrite = TN(rs!RoutWrite)
    TxtKeyTemplate = TN(rs!RoutKey)
    CmbFormat.text = TN(rs!Formato)
    cboType.text = TN(rs!Tipo)
      
    If TN(rs!Null) Then
      CmbNull.ListIndex = 0
    Else
      CmbNull.ListIndex = 1
    End If
  End If
  rs.Close
  
  'Incolla la combobox per la foreign key se serve
  If Trim(UCase(cboTypeIndex.text)) = "FOREIGN-KEY" Then
    cboForeignCol.Visible = True
    txtToForeign.text = txtNomeCol.text
  Else
    cboForeignCol.Visible = False
  End If
   
  'Recupera l'id della colonna
  pIndex_IdxColonna = Mid(lswColonne.SelectedItem.Key, 1, InStr(1, lswColonne.SelectedItem.Key, "#IDCOL") - 1)
  pIndex_Ordinale_IdxCol = Mid(lswColonne.SelectedItem.Key, InStr(1, lswColonne.SelectedItem.Key, "@") + 1)
  ' Mauro 20/10/2009 : Mi tocca farlo per leggere l'Order By della colonna
  Dim isAscending As Boolean
  isAscending = True
  Set rs = m_fun.Open_Recordset("select order from " & PrefDb & "_idxcol where " & _
                                "IdIndex = " & pIndex_IndiceCorrente & " And " & _
                                "Ordinale = " & pIndex_Ordinale_IdxCol)
  If rs.RecordCount Then
    If InStr(TN(rs!Order), "DESC") Then
      isAscending = False
    End If
  End If
  rs.Close
  
  If isAscending Then
    optAscending.Value = True
    optDescending.Value = False
  Else
    optAscending.Value = False
    optDescending.Value = True
  End If
End Sub

Private Sub lswIndici_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswIndici.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswIndici.SortOrder = Order
  lswIndici.SortKey = Key - 1
  lswIndici.Sorted = True
End Sub

Private Sub lswIndici_ItemClick(ByVal Item As MSComctlLib.ListItem)
  'Prende il nome e l'id dell'indice selezionato
  pIndex_IndiceCorrente = Mid(Item.Key, 1, InStr(1, Item.Key, "#") - 1)
  pNome_IndiceCorrente = Item.text
   
  'Carica le colonne dell'indice selezionato
  Load_Colonne_Indice pIndex_IndiceCorrente
End Sub

Public Sub Load_Colonne_Indice(idIdx As Long)
  Dim rs As Recordset, rc As Recordset ', rf As Recordset
  Dim wTipo As String, cc As Long
   
  lswColonne.ListItems.Clear
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index where " & _
                                "IdTable = " & pIndex_TbCorrente & " " & _
                                "And IdIndex = " & pIndex_IndiceCorrente & " order by idIndex")  'serve?
  If rs.RecordCount Then
    'Riempie anche le textbox
    txtCrDateIdx = TN(rs!Data_Creazione)
    txtMdDateIdx = TN(rs!Data_Modifica)
    txtDescrizioneIdx = rs!Descrizione & ""
    txtNomeIdx = rs!Nome
    
    Select Case Trim(UCase(rs!Tipo))
      Case "P"
        wTipo = "PRIMARY-KEY"
        cboTypeIndex.ListIndex = 0
      Case "F"
        wTipo = "FOREIGN-KEY"
        cboTypeIndex.ListIndex = 1
      Case "I"
        wTipo = "INDEX"
        cboTypeIndex.ListIndex = 2
      Case Else
        wTipo = "UNDEFINED"
        cboTypeIndex.text = wTipo
    End Select
    
    If rs!Used Then
      chkUsed.Value = 1
    Else
      chkUsed.Value = 0
    End If
    
    If rs!Unique Then
      chkUnique.Value = 1
    Else
      chkUnique.Value = 0
    End If
   
    'Prende le colonne
    'SQ
    Set rc = m_fun.Open_Recordset("Select a.idcolonna, a.ordinale, b.tipo, a.foridcolonna, b.nome From " & PrefDb & "_IdxCol as a, " & PrefDb & "_Colonne as b where " & _
                                  "a.idColonna = b.idColonna And b.IdTable = " & pIndex_TbCorrente & _
                                  " And IdIndex = " & pIndex_IndiceCorrente & _
                                  " Order By a.Ordinale")
    While Not rc.EOF
      'Aggiunge in lista
      'Recupera le info della colonna
      'SQ: idColonna e' gia' chiave!...
      'Set rf = m_fun.Open_Recordset("Select * From " & wPref & "_Colonne Where IdColonna = " & rc!IdColonna & " And IdTable = " & pIndex_TbCorrente)
      'If rf.RecordCount > 0 Then
      'rf!IdColonna & "#IDCOL@" & rc!Ordinale, rf!Nome
      lswColonne.ListItems.Add , rc!IdColonna & "#IDCOL@" & rc!Ordinale, rc!Nome
      If wTipo <> "FOREIGN-KEY" Then
        lswColonne.ListItems(lswColonne.ListItems.count).ListSubItems.Add , , rc!Tipo
      Else
        lswColonne.ListItems(lswColonne.ListItems.count).ListSubItems.Add , , get_Nome_Colonna_By_Id(IIf(IsNull(rc!forIdColonna), 0, rc!forIdColonna))
      End If

      ' Mauro 20/10/2009 : A che serve? Non viene utilizzato da nessuna parte!
      'cc = cc + 1
      'SQ Else
      '  If lswColonne.ListItems.Count > 0 Then
      '    lswColonne.ListItems(lswColonne.ListItems.Count).ListSubItems.Add , , ""
      '  End If
      'End If

      rc.MoveNext
    Wend
    'SQ End If
    rc.Close
  End If
  rs.Close
End Sub

Public Sub Crea_Nuovo_Indice()
  Dim rs As Recordset, wIndex As Long, Adesso As String
  
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select MAX(IdIndex) From " & PrefDb & "_Index")
  If rs.RecordCount Then
    wIndex = TN(rs.fields(0).Value, "LONG") + 1
  Else
    wIndex = 1
  End If
  rs.Close
   
  'Memorizza L'index corrente dell'indice
  pIndex_IndiceCorrente = wIndex
  Adesso = Now
   
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index")
  'Crea il nuovo indice nel database
  rs.AddNew
  rs!IdIndex = wIndex
  rs!IdTable = pIndex_TbCorrente
  rs!IdTableSpc = -1
  rs!Nome = "NEWINDEX" & Format(wIndex, "00000")
  rs!Tipo = "I" 'Di default Index
  rs!IdOrigine = -1
  rs!Used = False
  rs!Unique = True
  rs!Descrizione = "Put here Index Description"
  rs!Data_Creazione = Adesso
  rs!Data_Modifica = Adesso
  rs!Tag = ""
   
  rs.Update
  rs.Close
      
  'Aggiunge a Video Il nuovo indice
  lswIndici.ListItems.Add , wIndex & "#ID", "NEWINDEX" & Format(wIndex, "00000")
  lswIndici.ListItems(lswIndici.ListItems.count).ListSubItems.Add , , "INDEX"
  lswIndici.ListItems(lswIndici.ListItems.count).ListSubItems.Add , , "Yes"
  lswIndici.ListItems(lswIndici.ListItems.count).ListSubItems.Add , , "No"
End Sub

Private Sub optAscending_Click()
  Dim rsIdxCol As Recordset
  
  ' Aggiorno l'Option Button
  optDescending.Value = False
  
  ' Aggiorno il db
  If fLoad Then Exit Sub
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rsIdxCol = m_fun.Open_Recordset("select order from " & PrefDb & "_idxcol where " & _
                                      "IdIndex = " & pIndex_IndiceCorrente & " And " & _
                                      "Ordinale = " & pIndex_Ordinale_IdxCol)
  If rsIdxCol.RecordCount Then
    rsIdxCol!Order = "ASCENDING"
    rsIdxCol.Update
  Else
    'Errore Grave
    m_fun.WriteLog "+-------------------------------------------------------------+" & vbCrLf & _
       Space(22) & "|                                                             |" & vbCrLf & _
       Space(22) & "| Data Manager - Critical Error : No Id index Found...        |" & vbCrLf & _
       Space(22) & "|                                                             |" & vbCrLf & _
       Space(22) & "| Routine : MadmdP_00.MadmdF_Index.optAscending_Click()       |" & vbCrLf & _
       Space(22) & "|                                                             |" & vbCrLf & _
       Space(22) & "+-------------------------------------------------------------+"
  End If
  rsIdxCol.Close
End Sub

Private Sub optDescending_Click()
  Dim rsIdxCol As Recordset
    
  ' Aggiorno l'Option Button
  optAscending.Value = False
  
  ' Aggiorno il db
  If fLoad Then Exit Sub
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rsIdxCol = m_fun.Open_Recordset("select order from " & PrefDb & "_idxcol where " & _
                                      "IdIndex = " & pIndex_IndiceCorrente & " And " & _
                                      "Ordinale = " & pIndex_Ordinale_IdxCol)
  If rsIdxCol.RecordCount Then
    rsIdxCol!Order = "DESCENDING"
    rsIdxCol.Update
  Else
    'Errore Grave
    m_fun.WriteLog "+-------------------------------------------------------------+" & vbCrLf & _
       Space(22) & "|                                                             |" & vbCrLf & _
       Space(22) & "| Data Manager - Critical Error : No Id index Found...        |" & vbCrLf & _
       Space(22) & "|                                                             |" & vbCrLf & _
       Space(22) & "| Routine : MadmdP_00.MadmdF_Index.optDescending_Click()      |" & vbCrLf & _
       Space(22) & "|                                                             |" & vbCrLf & _
       Space(22) & "+-------------------------------------------------------------+"
  End If
  rsIdxCol.Close
End Sub

Private Sub txtCrDateIdx_KeyPress(KeyAscii As Integer)
   KeyAscii = 0
End Sub

Private Sub txtDecimal_KeyPress(KeyAscii As Integer)
   KeyAscii = 0
End Sub

Private Sub txtDescrizioneCol_KeyPress(KeyAscii As Integer)
   KeyAscii = 0
End Sub

Private Sub txtDescrizioneIdx_Change()
  'Va in update sul nome
  Dim rs As Recordset
  
  If fLoad Then Exit Sub
  
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index Where idIndex = " & pIndex_IndiceCorrente)
  If rs.RecordCount Then
    rs!Descrizione = txtDescrizioneIdx.text
    rs.Update
  Else
    'Errore Grave
    m_fun.WriteLog "+-------------------------------------------------------------+" & vbCrLf & _
       Space(22) & "|                                                             |" & vbCrLf & _
       Space(22) & "| Data Manager - Critical Error : No Id index Found...        |" & vbCrLf & _
       Space(22) & "|                                                             |" & vbCrLf & _
       Space(22) & "| Routine : MadmdP_00.MadmdF_Index.txtDescrizioneIdx_Change() |" & vbCrLf & _
       Space(22) & "|                                                             |" & vbCrLf & _
       Space(22) & "+-------------------------------------------------------------+"
  End If
  rs.Close
End Sub

Private Sub txtDim_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub txtLabel_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub TxTLoad_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub txtMdDateIdx_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub txtNomeIdx_Change()
  'Va in update sul nome
  Dim rs As Recordset
   
  If fLoad Then Exit Sub
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_Index Where idIndex = " & pIndex_IndiceCorrente)
  If rs.RecordCount Then
    rs!Nome = txtNomeIdx.text
    rs.Update
  Else
    'Errore Grave
    m_fun.WriteLog "+------------------------------------------------------+" & vbCrLf & _
       Space(22) & "|                                                      |" & vbCrLf & _
       Space(22) & "| Data Manager - Critical Error : No Id index Found... |" & vbCrLf & _
       Space(22) & "|                                                      |" & vbCrLf & _
       Space(22) & "| Routine : MadmdP_00.MadmdF_Index.txtNomeIdx_Change() |" & vbCrLf & _
       Space(22) & "|                                                      |" & vbCrLf & _
       Space(22) & "+------------------------------------------------------+"
  End If
  rs.Close
End Sub

Private Sub TxtRead_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub TxtWrite_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub

Private Sub TxtKeyTemplate_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
End Sub
