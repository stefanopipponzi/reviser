VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MadmdC_Tabella"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_IdTable As Long
Private m_IdOrigine As Long
Private m_IdTableJoin As Long
Private m_Nome As String
Private m_Creator As String
Private m_Dimensionamento As Long
Private m_Descrizione As String
Private m_DataCreazione As String
Private m_DataModifica As String
Private m_Tags As String
Private m_Colonne As New Collection
Private m_Indici As New Collection

'ID TABLE
Public Property Get IdTable() As Long
   IdTable = m_IdTable
End Property

Public Property Let IdTable(cIdTable As Long)
   m_IdTable = cIdTable
End Property

'ID TABLEJOIN
Public Property Get IdTableJoin() As Long
   IdTableJoin = m_IdTableJoin
End Property

Public Property Let IdTableJoin(cIdTableJoin As Long)
   m_IdTableJoin = cIdTableJoin
End Property

'ID ORIGINE
Public Property Get IdOrigine() As Long
   IdOrigine = m_IdOrigine
End Property

Public Property Let IdOrigine(cIdOr As Long)
   m_IdOrigine = cIdOr
End Property

'NOME
Public Property Get Nome() As String
   Nome = m_Nome
End Property

Public Property Let Nome(cNome As String)
   m_Nome = cNome
End Property

'DIMENSIONAMENTO
Public Property Get Dimensionamento() As Long
  Dimensionamento = m_Dimensionamento
End Property

Public Property Let Dimensionamento(cDimensionamento As Long)
   m_Dimensionamento = cDimensionamento
End Property

'CREATOR
Public Property Get Creator() As String
  Creator = m_Creator
End Property

Public Property Let Creator(cCreator As String)
   m_Creator = cCreator
End Property

'DATA CR
Public Property Get DataCreazione() As String
   DataCreazione = m_DataCreazione
End Property

Public Property Let DataCreazione(cDataCreazione As String)
   m_DataCreazione = cDataCreazione
End Property

'DATA MOD
Public Property Get DataModifica() As String
   DataModifica = m_DataModifica
End Property

Public Property Let DataModifica(cDataModifica As String)
   m_DataModifica = cDataModifica
End Property

'DESCRIZIONE
Public Property Get Descrizione() As String
   Descrizione = m_Descrizione
End Property

Public Property Let Descrizione(cDescrizione As String)
   m_Descrizione = cDescrizione
End Property

'TAG
Public Property Get Tags() As String
   Tags = m_Tags
End Property

Public Property Let Tags(cTags As String)
   m_Tags = cTags
End Property

'COLONNE
Public Sub AddColonna(colonna As MadmdC_Colonne)
   m_Colonne.Add colonna
End Sub

Public Function ColonneCount() As Long
   ColonneCount = m_Colonne.Count
End Function

Public Sub RemoveColonna(Index As Long)
   m_Colonne.Remove (Index)
End Sub

Public Function ItemColonna(Index As Long) As MadmdC_Colonne
   Set ItemColonna = m_Colonne.Item(Index)
End Function

Public Sub ClearColonne()
   Set m_Colonne = New Collection
End Sub

'INDICI
Public Sub AddIndice(idx As MadmdC_Indici)
   m_Indici.Add idx
End Sub

Public Function IndiciCount() As Long
   IndiciCount = m_Indici.Count
End Function

Public Sub RemoveIndice(Index As Long)
   m_Indici.Remove (Index)
End Sub

Public Function ItemIndice(Index As Long) As MadmdC_Indici
   Set ItemIndice = m_Indici.Item(Index)
End Function

Public Sub ClearIndici()
   Set m_Indici = New Collection
End Sub
