VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadmdF_IdxColumn 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Column Index References..."
   ClientHeight    =   5055
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   4830
   Icon            =   "MadmdF_IdxColumn.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   4830
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   480
      Top             =   4440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MadmdF_IdxColumn.frx":030A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lswIdxCol 
      Height          =   4245
      Left            =   90
      TabIndex        =   1
      Top             =   120
      Width           =   4635
      _ExtentX        =   8176
      _ExtentY        =   7488
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Table Reference"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Used As"
         Object.Width           =   3528
      EndProperty
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   3750
      Picture         =   "MadmdF_IdxColumn.frx":0464
      Style           =   1  'Graphical
      TabIndex        =   0
      Tag             =   "fixed"
      Top             =   4440
      Width           =   945
   End
End
Attribute VB_Name = "MadmdF_IdxColumn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAnnulla_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  Me.Caption = "DB-Engine: " & TipoDB & " - Column Index References [" & pNome_ColonnaCorrente & "]"
  
  'Carica le tabelle di reference per questa colonna
  Load_Tabelle_Reference pIndex_ColonnaCorrente
End Sub

Public Sub Load_Tabelle_Reference(IdColonna As Long)
  Dim rs As Recordset
  Dim wNomeTable As String
   
  lswIdxCol.ListItems.Clear
   
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol Where " & _
                                "ForIdColonna = " & IdColonna)
  If rs.RecordCount Then
    wNomeTable = get_NomeTable_ById(rs!IdTable)
    lswIdxCol.ListItems.Add , , wNomeTable
    lswIdxCol.ListItems(lswIdxCol.ListItems.Count).ListSubItems.Add , , "Foreign Table"
    lswIdxCol.ListItems(lswIdxCol.ListItems.Count).ListSubItems(1).ForeColor = vbRed
    lswIdxCol.ListItems(lswIdxCol.ListItems.Count).ListSubItems(1).Bold = True
  End If
  rs.Close
  
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_IdxCol Where " & _
                                "IdColonna = " & IdColonna)
  If rs.RecordCount Then
     wNomeTable = get_NomeTable_ById(rs!IdTable)
     lswIdxCol.ListItems.Add , , wNomeTable
     lswIdxCol.ListItems(lswIdxCol.ListItems.Count).ListSubItems.Add , , "Local Table"
     lswIdxCol.ListItems(lswIdxCol.ListItems.Count).ListSubItems(1).ForeColor = vbBlue
     lswIdxCol.ListItems(lswIdxCol.ListItems.Count).ListSubItems(1).Bold = True
  End If
  rs.Close
End Sub

Private Sub Form_Resize()
  ResizeForm Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  SetFocusWnd m_fun.FnParent
End Sub

Private Sub lswIdxCol_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
  'ordina la lista
  Dim Key As Long
  Static Order As String
  
  Order = lswIdxCol.SortOrder
  
  Key = ColumnHeader.Index
  
  If UCase(val(Order)) = 0 Then
    Order = 1
  Else
    Order = 0
  End If
  
  lswIdxCol.SortOrder = Order
  lswIdxCol.SortKey = Key - 1
  lswIdxCol.Sorted = True
End Sub
