Attribute VB_Name = "MadmdM_VSAM"
Option Explicit
Global VSAM_dbName As String, VSAM_LookUpName As String, VSAM_TbName As String, VSAM_ColName As String
Global VSAM_PKName As String, VSAM_TbsName As String, VSAM_UKName As String, VSAM_FKName As String

Public Sub generaCpyRR_VSAM(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset
  Dim Creator As String
  Dim fileName As String
  'Dim cont As String
  Dim env As Collection
      
  On Error GoTo ErrorHandler
  
  RtbFile.text = ""
  
  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
  
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "idDB = " & pIndex_DBCorrente & " order by idTable")
  Do Until rsTabelle.EOF
    'Set rsSegmenti = m_fun.Open_Recordset("select * from DMVSM_Archivio where IdArchivio = " & rsTabelle!IdOrigine)
    Set rsSegmenti = m_fun.Open_Recordset("select * from DMVSM_RecordType where IdRecordType = " & rsTabelle!IdOrigine)
    ' La variabile cont mi serve come progressivo per i segmenti virtuali
'''    If rsSegmenti.RecordCount Then
'''      ' Postfisso per il nome dell'area RR
'''      cont = Right(rsSegmenti!Nome, 2)
'''    Else
'''      cont = ""
'''    End If
    If rsSegmenti.RecordCount Then
      'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load cos� non va in eoe!
      If Dir(DataMan.DmPathDb & "\Input-Prj\TEMPLATE\" & pOrigin_DBCorrente & "\standard\fields\" & dbType & "\CPY_BASE_RR") <> "" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\TEMPLATE\" & pOrigin_DBCorrente & "\standard\fields\" & dbType & "\CPY_BASE_RR"
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\TEMPLATE\" & pOrigin_DBCorrente & "\standard\fields\" & dbType & "\CPY_BASE_RR"" not found."
        Exit Sub
      End If
      
      changeTag RtbFile, "<DBD-NAME>", rsSegmenti!VSAM
      changeTag RtbFile, "<TABLE-NAME>", Replace(rsTabelle!Nome, "_", "-")
      ' Inserisco le colonne
      changeTag RtbFile, "<LISTA-CAMPI>", getListaCampi(rsTabelle!Nome, rsTabelle!IdTable)
      
      'SEGMENTI VIRTUALI: non utilizziamo KRR/KRD
      InserisciKeyVSAM RtbFile, rsTabelle, 1, "" 'cont
      InserisciSecondaryKeyVSAM RtbFile, rsTabelle

      If Trim(rsTabelle!Creator) <> "" Then
         Creator = Trim(rsTabelle!Creator) 'Controllare se esiste un alias!!!!!!!!
      Else
        Creator = Trim(LeggiParam("DM_CREATOR"))
        If Trim(Creator) <> "" Then
           Creator = UCase(Trim(Creator))
        End If
      End If
      
      'fileName = LeggiParam("X-COPY")
      fileName = LeggiParam("C-COPY")
      If Len(fileName) Then
        Set env = New Collection  'resetto;
        env.Add rsTabelle
        env.Add rsSegmenti
        If Right(rsTabelle!Tag, 6) = "OCCURS" Then ' potrei avere "AUTOMATIC-OCCURS"
          env.Add Format(rsTabelle!Dimensionamento, "00")
        Else
          env.Add ""
        End If
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(fileName, "tabelleVSM", env), 1
      Else
        'MsgBox "No value found for 'X-COPY' environment parameter.", vbExclamation, DataMan.DmNomeProdotto
        MsgBox "No value found for 'C-COPY' environment parameter.", vbExclamation, DataMan.DmNomeProdotto
        Exit Sub
      End If
    Else
      MsgBox "No origin segment found for table " & rsTabelle!Nome, vbExclamation, DataMan.DmNomeProdotto
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Loop
  rsTabelle.Close
  Exit Sub
ErrorHandler:
  Select Case ERR.Number
    Case 75
      If Dir(DataMan.DmPathDb & "\Input-Prj\TEMPLATE\" & pOrigin_DBCorrente & "\standard\fields\" & dbType & "\CPY_BASE_RR") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_EXEC"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
    'resume
  End Select
End Sub

Sub GeneraCpyRD_VSAM(RtbFile As RichTextBox)
  Dim rsSegmenti As Recordset, rsTabelle As Recordset
  Dim fileName As String
  Dim env As Collection
      
  On Error GoTo errdb
    
  If dbType = "VSAM" Then
    Stop
  End If
  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
  
  Set rsTabelle = m_fun.Open_Recordset("select idTable,nome,idOrigine,TAG from " & PrefDb & "_Tabelle where " & _
                                       "idDB= " & pIndex_DBCorrente & " and TAG not like '%OCCURS' order by idTable")
  'Virtuale = False  'non utilizzato!?
  Do Until rsTabelle.EOF
    '''''''''''''''''''''''''''''''''''''
    ' Per ogni TABELLA associata al DBD:
    '''''''''''''''''''''''''''''''''''''
    'Set rsSegmenti = m_fun.Open_Recordset("select * from DMVSM_Archivio where IdArchivio = " & rsTabelle!IdOrigine)
    Set rsSegmenti = m_fun.Open_Recordset("select * from DMVSM_RecordType where IdRecordType = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount Then
      'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load cos� non va in eoe!
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_RD") <> "" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_RD"
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_RD"" not found."
        Exit Sub
      End If
          
      changeTag RtbFile, "<VSAM-NAME>", rsSegmenti!VSAM
      
      '<STRUTTURA-ASSOCIATA>
      InserisciAreaVSAM rsSegmenti, RtbFile
      
      getChiaviVSAM RtbFile, rsSegmenti, rsTabelle!IdTable
      '''getChiaviDli_KRDP RtbFile, rsSegmenti, rsTabelle!IdTable
    
      fileName = LeggiParam("T-COPY")
      If Len(fileName) Then
        Set env = New Collection  'resetto;
        env.Add rsTabelle
        env.Add rsSegmenti
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(fileName, "tabelleVSM", env), 1
      Else
        MsgBox "No value found for 'T-COPY' environment parameter.", vbExclamation
        Exit Sub
      End If
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Loop
  rsTabelle.Close
  Exit Sub
errdb:
  Select Case ERR.Number
    Case 75
'      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_RD"" not found."
    Case 7676 '
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Sub

Sub InserisciAreaVSAM(rsSegmenti As Recordset, RtbFile As RichTextBox)
  Dim TbAreaSeg As Recordset
  Dim testo As String, Struttura As String
  Dim str As String, Str1 As String, Field As String
  Dim LenCmp As Integer
  Dim Decimali As String
  Dim LenRiga As Integer, k As Integer, x As Integer, MaxLen As Integer
  Dim bol As Boolean
  Dim env As New Collection
  
  MaxLen = 0
  ' Tratto i segmenti reali
  ReDim Ind_Liv(0)
  LenRiga = 72
  Set TbAreaSeg = m_fun.Open_Recordset("select * from DMVSM_Data_Cmp where " & _
                                       "VSAM = '" & rsSegmenti!VSAM & "' and idoggetto = " & rsSegmenti!IdOggetto & " and " & _
                                       "idarea = " & rsSegmenti!idArea & " order by ordinale")
  If TbAreaSeg.RecordCount Then
    '''''''''''''''''''''''''''''''
    'Calcolo indentazione: una per livello
    '''''''''''''''''''''''''''''''
    For k = 1 To TbAreaSeg.RecordCount
      If Len(TbAreaSeg!Nome) > MaxLen Then
        MaxLen = Len(TbAreaSeg!Nome)
        testo = TbAreaSeg!Nome
      End If
      If UBound(Ind_Liv) = 0 Then
        ReDim Preserve Ind_Liv(UBound(Ind_Liv) + 1)
        Ind_Liv(UBound(Ind_Liv)).livello = Int(TbAreaSeg!livello)
        Ind_Liv(UBound(Ind_Liv)).Indet = 0
      Else
        bol = False
        For x = 1 To UBound(Ind_Liv)
          If Ind_Liv(x).livello = Int(TbAreaSeg!livello) Then
            bol = True
            Exit For
          End If
        Next x
        If Not bol Then
          ReDim Preserve Ind_Liv(UBound(Ind_Liv) + 1)
          Ind_Liv(UBound(Ind_Liv)).livello = Int(TbAreaSeg!livello)
          Ind_Liv(UBound(Ind_Liv)).Indet = Ind_Liv(UBound(Ind_Liv) - 1).Indet + 3
        End If
      End If
      bol = False
      TbAreaSeg.MoveNext
    Next k
    
    TbAreaSeg.MoveFirst
    NmSegCpy = rsSegmenti!Nome
    '''''''''''''''''''''
    ' Livelli 01:
    '''''''''''''''''''''
    'Nome parametrico:
    str = LeggiParam("T-COPY-AREA")
    If Len(str) Then
      env.Add Null
      env.Add rsSegmenti
    Else
      'esco!
      ERR.Raise 7676, , "No valid value for ""T-COPY-AREA"" environment parameter."
    End If
    
'    If Not Virtuale Then
'      Dim rsTemp As Recordset
'      Set rsTemp = m_fun.Open_Recordset("select maxlen from DMVSM_Archivio where VSAM = '" & rsSegmenti!VSAM & "'")
'      If rsTemp.RecordCount Then
'        MaxLen = rsTemp!MaxLen
'      Else
'        MaxLen = 0
'      End If
'      rsTemp.Close
'      Struttura = "01 AREA-" & Replace(rsSegmenti!Nome, "_", "-") & "  PIC X(" & MaxLen & ")." & vbCrLf '& _
'                  '"01 RD-" & rsSegmenti!Nome & " REDEFINES AREA-" & rsSegmenti!Nome & "." & vbCrLf
'    Else
'      Struttura = "01 RD-" & getEnvironmentParam(str, "tabelleVSM", env) & "." & vbCrLf
'    End If
    Struttura = "01 RD-" & Replace(rsSegmenti!Nome, "_", "-") & "." & vbCrLf
    
    'salto il vero livello 01:
    TbAreaSeg.MoveNext
    Do Until TbAreaSeg.EOF
      LenCmp = TbAreaSeg!lunghezza
      If TbAreaSeg!Decimali <> 0 Then
        Decimali = "V9(" & TbAreaSeg!Decimali & ")"
      Else
        Decimali = ""
      End If
      If LenCmp + TbAreaSeg!Decimali = "0" Then
        If TbAreaSeg!Occurs = "1" Or TbAreaSeg!Occurs = "0" Then
          If Trim(TbAreaSeg!NomeRed) <> "" Then
            Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & " REDEFINES " & TbAreaSeg!NomeRed & "."
          Else
            Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & "."
          End If
        Else
          Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & " OCCURS " & TbAreaSeg!Occurs & " TIMES."
        End If
      Else
        If TbAreaSeg!segno = "S" Then
          If Len(TbAreaSeg!Nome) + Indenta(Int(TbAreaSeg!livello)) > 30 Then
            Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & "  PIC S" & IIf(LenCmp > 0, TbAreaSeg!Tipo & "(" & LenCmp & ")", "")
          Else
            Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & Space(31 - Len(TbAreaSeg!Nome) - Indenta(Int(TbAreaSeg!livello))) & "  PIC S" & IIf(LenCmp > 0, TbAreaSeg!Tipo & "(" & LenCmp & ")", "")
          End If
        Else
          If Len(TbAreaSeg!Nome) + Indenta(Int(TbAreaSeg!livello)) > 30 Then
            Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & "  PIC " & IIf(LenCmp > 0, TbAreaSeg!Tipo & "(" & LenCmp & ")", "")
          Else
            Field = Space(Indenta(Int(TbAreaSeg!livello))) & Format(TbAreaSeg!livello, "00") & " " & TbAreaSeg!Nome & Space(31 - Len(TbAreaSeg!Nome) - Indenta(Int(TbAreaSeg!livello))) & "  PIC " & IIf(LenCmp > 0, TbAreaSeg!Tipo & "(" & LenCmp & ")", "")
          End If
        End If
        Select Case TbAreaSeg!Usage
          Case "BNR"
            Field = Field & Decimali & " COMP"
          Case "PCK"
            Field = Field & Decimali & " COMP-3"
          Case "ZND"
            If Trim(TbAreaSeg!Tipo) = "9" Then Field = Field & Decimali
        End Select
        If TbAreaSeg!Occurs = "0" Or TbAreaSeg!Occurs = "1" Then
          Field = Field & "."
        Else
          Field = Field & " OCCURS " & TbAreaSeg!Occurs & " TIMES." & vbCrLf
        End If
      End If
      If Len(Field) > LenRiga Then
        k = InStr(Field, "PIC")
        If k <> 0 Then
         str = RTrim(Mid(Field, 1, k - 1))
         Str1 = RTrim(Mid(Field, k, Len(Field)))
         Field = str & Space(LenRiga - Len(str)) & vbCrLf
         For x = 1 To Len(str)
          If Mid(str, 1, 1) <> " " Then
            Str1 = Space(x + 2) & Str1
            Field = Field & Str1 & Space(LenRiga - Len(Str1)) & vbCrLf
            Exit For
          Else
            str = Mid(str, 2)
          End If
         Next x
        End If
      Else
        Struttura = Struttura & Field & vbCrLf
      End If
      TbAreaSeg.MoveNext
    Loop
    changeTag RtbFile, "<STRUTTURA-ASSOCIATA>", Struttura
    changeTag RtbFile, "<NOME-SEGM>", rsSegmenti!Nome
  End If
  TbAreaSeg.Close
End Sub

''''''''''''''''''''''''''''''''''''
' ex InserisciKeyDLI
'
''''''''''''''''''''''''''''''''''''
Sub getChiaviVSAM(rtb As RichTextBox, rsSegmenti As Recordset, IdTable As Long)
  Dim rsField As Recordset, MgTbCmp As Recordset
  Dim i As Integer
  Dim statement As String, NomeCol As String, Decimali As String, tracciato As String
  Dim wLen As Long
  'Dim usato As Boolean
  Dim tracciatiField() As String
  Dim livello01 As String
  Dim rsTemp As Recordset
  Dim rsIndex As Recordset
  
  On Error GoTo errdb
    
  Set rsTemp = m_fun.Open_Recordset("Select idarchivio from DMVSM_archivio where " & _
                                     "VSAM = '" & rsSegmenti!VSAM & "'")
  If rsTemp.RecordCount Then
    Set rsField = m_fun.Open_Recordset("Select IdKey,Nome,primaria,KeyStart,KeyLen from DMVSM_Chiavi where " & _
                                       "idarchivio = " & rsTemp!idArchivio)
    If rsField.RecordCount Then
      ReDim tracciatiField(rsField.RecordCount - 1, 1)
    End If
    For i = 0 To rsField.RecordCount - 1
      'SQ: KRD-indexName!
      Set rsIndex = m_fun.Open_Recordset("select nome from " & PrefDb & "_index where " & _
                                         "idOrigine = " & rsField!IdKey & " AND idTable = " & IdTable)
      
      'CONTROLLO SE USATO: (spostare in fondo)
      livello01 = ""
      ''''''''''''''''''''''''''''''
      'FIELD SEQ e FIELD scelti:
      ''''''''''''''''''''''''''''''
      If rsIndex.EOF Then
        'SQ 30-01-07 - Problema FIELD aggiunti (FIELD MULTIPLI)
        '              => idOrigine NON � idIndice ma idColonna (capire sul DM perch� questo scamuffo!)
        '              ==> PEZZA: faccio una query diversa (sono sul SEQ, quindi � diventato (99%) chiave primaria)
        Set rsIndex = m_fun.Open_Recordset("select nome from " & PrefDb & "_index where tipo='P' AND idTable=" & IdTable)
      End If
      If rsIndex.RecordCount Then
        'SQ: KRD-indexName!
        'livello01 = "01 KRD-" & rsField!Nome & "." & vbCrLf
        'VIRGILIO inserita la replace per le REVKEY
        livello01 = "01 KRD-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf
      Else
        MsgBox "tmp: no index for idField: " & rsField!IdKey & " - idTable: " & IdTable
      End If
      rsIndex.Close
      
      'CAMPI:
      tracciato = ""
      Set MgTbCmp = m_fun.Open_Recordset("select * from DMVSM_Data_Cmp where " & _
                                         "VSAM = '" & rsSegmenti!VSAM & "' and idoggetto = " & rsSegmenti!IdOggetto & " and " & _
                                         "idarea = " & rsSegmenti!idArea & " and Posizione >= " & rsField!keyStart & _
                                         " order by ordinale")
      Do Until MgTbCmp.EOF
        If MgTbCmp!lunghezza > 0 Then
          wLen = MgTbCmp!lunghezza
          If ((MgTbCmp!Posizione + MgTbCmp!Byte) < (rsField!keyStart + rsField!keyLen + 1)) Then
            NomeCol = MgTbCmp!Nome
            If MgTbCmp!segno = "S" Then
              tracciato = tracciato & Space(3) & "10 " & MgTbCmp!Nome & "  PIC S" & MgTbCmp!Tipo & "(" & MgTbCmp!lunghezza & ")"
            Else
              tracciato = tracciato & Space(3) & "10 " & MgTbCmp!Nome & "  PIC " & MgTbCmp!Tipo & "(" & MgTbCmp!lunghezza & ")"
            End If
            If MgTbCmp!Decimali > 0 Then
              Decimali = "V9(" & MgTbCmp!Decimali & ")"
            Else
              Decimali = ""
            End If
            Select Case MgTbCmp!Usage
              Case "BNR"
                tracciato = tracciato & Decimali & " COMP"
              Case "PCK"
                tracciato = tracciato & Decimali & " COMP-3"
              Case "ZND"
                If MgTbCmp!Tipo = "9" Then tracciato = tracciato & Decimali
            End Select
            tracciato = tracciato & "." & vbCrLf
          End If
        End If
        MgTbCmp.MoveNext
      Loop
      MgTbCmp.Close
      
      'salvo per gli XDField
      tracciatiField(i, 0) = rsField!Nome
      tracciatiField(i, 1) = tracciato
      
      'alpitour 5/8/2005
  '    If usato Then
      If Len(tracciato) Then
  '      statement = statement & tracciato
        statement = statement & livello01 & tracciato
      End If
      rsField.MoveNext
    Next i
    rsField.Close
    
    changeTag rtb, "<CHIAVI-VSAM>", statement
  End If
  rsTemp.Close
  Exit Sub
errdb:
  MsgBox ERR.Number & " - " & ERR.Description
  'Resume
End Sub

Public Sub InserisciKeyVSAM(rtb As RichTextBox, tb As Recordset, bol As Integer, cont As String)
  Dim rsIndex As Recordset, rsIdxCol As Recordset, r As Recordset
  Dim indseg As Integer, Ind As Integer, sp As Integer, indice As Integer
  Dim colonna As String, chiave_i As String, NomeCol As String, Tipo As String, Usage As String
  
  On Error GoTo ERR
  
  Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_Index where idtable = " & tb!IdTable & " and tipo = 'P'")
  If rsIndex.RecordCount Then
    Select Case bol
      Case 1
        InsertParola rtb, "<CHIAVI-PRIMARIE>", "01 KRR-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & _
                                   Space(10) & "<COLONNA>." & vbCrLf & _
                                    Space(7) & "01 K01-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & _
                                   Space(10) & "<CHIAVE1>."
'''      Case 2
'''        InsertParola rtb, "<CHIAVI-CLUSTERING>.", "01 KRR-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & _
'''                                      Space(10) & "<CLUCOLONNA>." & _
'''                                       Space(7) & "01 K01-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & _
'''                                      Space(10) & "<CLUCHIAVE>."
    End Select

    Set rsIdxCol = m_fun.Open_Recordset("select *  from " & PrefDb & "_IdxCol where " & _
                                        "idtable = " & tb!IdTable & " and idindex = " & rsIndex!IdIndex & " order by ordinale")
    If rsIdxCol.RecordCount Then
      For Ind = 0 To rsIdxCol.RecordCount - 1
        sp = 10
        rsIndex.MoveFirst
        indseg = rsIndex.RecordCount
        Do Until rsIndex.EOF
          Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where idcolonna = " & rsIdxCol!IdColonna)
          If r.RecordCount Then
            NomeCol = Replace(r!Nome, "_", "-")
            'NomeCol = r!nome
            Select Case bol
              Case 1
                Select Case r!Tipo
                  Case "CHAR"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(" & r!lunghezza & ")." & vbCrLf & Space(sp) & "<COLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(" & r!lunghezza & ")." & vbCrLf & Space(sp) & "<CHIAVE_i>."
                  Case "TIME"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(8)." & vbCrLf & Space(sp) & "<COLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(8)." & vbCrLf & Space(sp) & "<CHIAVE_i>."
                  Case "DATE"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(10)." & vbCrLf & Space(sp) & "<COLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(10)." & vbCrLf & Space(sp) & "<CHIAVE_i>."
                  Case "INTEGER"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(9) COMP." & vbCrLf & Space(sp) & "<COLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(9) COMP." & vbCrLf & Space(sp) & "<CHIAVE_i>."
                  Case "LONG"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(18) COMP." & vbCrLf & Space(sp) & "<COLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(18) COMP." & vbCrLf & Space(sp) & "<CHIAVE_i>."
                  Case "DECIMAL"
                    If IsNull(r!Decimali) Then r!Decimali = 0
                    Tipo = "PIC S9" & "(" & (CInt(r!lunghezza) - r!Decimali) & ")"
                    If r!Decimali > 0 Then
                      Tipo = Tipo & "V9(" & r!Decimali & ") "
                    End If
                    Usage = " COMP-3."
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & Tipo & Usage & vbCrLf & Space(sp) & "<COLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & Tipo & Usage & vbCrLf & Space(sp) & "<CHIAVE_i>."
                  Case "SMALLINT"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(4) COMP." & vbCrLf & Space(sp) & "<COLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(4) COMP." & vbCrLf & Space(sp) & "<CHIAVE_i>."
                  Case "VARCHAR"
                    'colonna = "10  " & nomecol & "." & vbCrLf & Space(15) & "15  ADL-" & nomecol & "-LEN" & Space(20 - (Len(nomecol) + Len(nomecol & "-LEN"))) & "PIC S9(4) COMP." & vbCrLf & Space(15) & "15  ADL-" & nomecol & "-TEXT" & Space(20 - (Len(nomecol) + Len(nomecol & "-TEXT"))) & "PIC X(" & TbColRel!Lunghezza & ")." & vbCrLf & Space(11) & "<COLONNA>."
                  Case "VARCHAR2"
                  'stefano: 18/7/2006
                  Case "TIMESTAMP"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(26)." & vbCrLf & Space(sp) & "<COLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(26)." & vbCrLf & Space(sp) & "<CHIAVE_i>."
                  Case "NUMBER"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(" & r!lunghezza & ") COMP-3." & vbCrLf & Space(sp) & "<COLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(" & r!lunghezza & ") COMP-3." & vbCrLf & Space(sp) & "<CHIAVE_i>."
                End Select
              Case 2
                Select Case r!Tipo
                  Case "CHAR"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(" & r!lunghezza & ")." & vbCrLf & Space(sp) & "<CLUCOLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(" & r!lunghezza & ")." & vbCrLf & Space(sp) & "<CLUCHIAVE>."
                  Case "TIME"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(8)." & vbCrLf & Space(sp) & "<CLUCOLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(8)." & vbCrLf & Space(sp) & "<CLUCHIAVE>."
                  Case "DATE"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(10)." & vbCrLf & Space(sp) & "<CLUCOLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(10)." & vbCrLf & Space(sp) & "<CLUCHIAVE>."
                  Case "INTEGER"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(9) COMP." & vbCrLf & Space(sp) & "<CLUCOLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(9) COMP." & vbCrLf & Space(sp) & "<CLUCHIAVE>."
                  Case "LONG"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(18) COMP." & vbCrLf & Space(sp) & "<CLUCOLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(18) COMP." & vbCrLf & Space(sp) & "<CLUCHIAVE>."
                  Case "DECIMAL"
                    Tipo = "PIC S9" & "(" & (CInt(r!lunghezza) - CInt(r!Decimali)) & ")"
                    If r!Decimali = 0 Then
                      Tipo = Tipo & ""
                    Else
                      Tipo = Tipo & "V9(" & r!Decimali & ") "
                    End If
                    Usage = " COMP-3."
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & Tipo & Usage & vbCrLf & Space(sp) & "<CLUCOLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & Tipo & Usage & vbCrLf & Space(sp) & "<CLUCHIAVE>."
                  Case "SMALLINT"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(4) COMP." & vbCrLf & Space(sp) & "<CLUCOLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(4) COMP." & vbCrLf & Space(sp) & "<CLUCHIAVE>."
                  Case "VARCHAR"
                    'colonna = "10  " & nomecol & "." & vbCrLf & Space(15) & "15  ADL-" & nomecol & "-LEN" & Space(20 - (Len(nomecol) + Len(nomecol & "-LEN"))) & "PIC S9(4) COMP." & vbCrLf & Space(15) & "15  ADL-" & nomecol & "-TEXT" & Space(20 - (Len(nomecol) + Len(nomecol & "-TEXT"))) & "PIC X(" & TbColRel!Lunghezza & ")." & vbCrLf & Space(11) & "<COLONNA>."
                  Case "VARCHAR2"
                  'stefano: 18/7/2006
                  Case "TIMESTAMP"
                    colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(26)." & vbCrLf & Space(sp) & "<CLUCOLONNA>."
                    chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC x(26)." & vbCrLf & Space(sp) & "<CLUCHIAVE>."
                End Select
            End Select

            Select Case bol
              Case 1
                InsertParola rtb, "<COLONNA>.", colonna
                For indice = 1 To 6
                  InsertParola rtb, "<CHIAVE" & indice & ">.", Replace(chiave_i, "_i", indice)
                Next indice
              Case 2
                InsertParola rtb, "<CLUCOLONNA>.", colonna
                InsertParola rtb, "<CLUCHIAVE>.", chiave_i
            End Select
          End If
          rsIndex.MoveNext
        Loop
        rsIdxCol.MoveNext
      Next Ind
      rsIdxCol.Close
      
      Select Case bol
        Case 1
          DelParola rtb, "<COLONNA>."
          For indice = 1 To 6
            DelParola rtb, "<CHIAVE" & indice & ">."
          Next
        Case 2
          DelParola rtb, "<CLUCOLONNA>."
          DelParola rtb, "<CLUCHIAVE>."
          DelParola rtb, "<CLUCHIAVE1>."
      End Select
    Else
      Select Case bol
        Case 1
          colonna = vbCrLf & Space(6) & "*NO PRIMARY KEY'COLUMNS"
          InsertParola rtb, "<COLONNA>.", colonna
          InsertParola rtb, "<CHIAVE>.", colonna
          InsertParola rtb, "<CHIAVE_i>.", colonna
        Case 2
          colonna = vbCrLf & Space(6) & "*NO CLUSTERING KEY'COLUMNS"
          InsertParola rtb, "<CLUCOLONNA>.", colonna
          InsertParola rtb, "<CLUCHIAVE>.", colonna
          InsertParola rtb, "<CLUCHIAVE1>.", colonna
      End Select
    End If
  Else
    Select Case bol
      Case 1
        colonna = vbCrLf & Space(6) & "*NO PRIMARY KEY'COLUMNS"
        InsertParola rtb, "<CHIAVI-PRIMARIE>", colonna
      Case 2
        colonna = vbCrLf & Space(6) & "*NO CLUSTERING KEY'COLUMNS"
        InsertParola rtb, "<CHIAVI-CLUSTERING>.", colonna
    End Select
  End If
  Exit Sub
ERR:
  Select Case ERR.Number
    Case 380
      Beep
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
  End Select
  'Resume
End Sub

Public Sub InserisciSecondaryKeyVSAM(rtb As RichTextBox, tb As Recordset)
  Dim rsIdxCol As Recordset, TbColRel As Recordset, TbIndex As Recordset, TbColonne As Recordset
  Dim Ind As Integer, i As Integer
  Dim colonna As String, NomeCol As String, Tipo As String, Usage As String
  
  On Error GoTo ERR
  
  Set TbIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_Index where " & _
                                     "idtable = " & tb!IdTable & " and tipo = 'I' order by idIndex")
  Do Until TbIndex.EOF
    Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IdxCol where " & _
                                        "idtable = " & tb!IdTable & " and idindex = " & TbIndex!IdIndex & " order by ordinale")
    If Not rsIdxCol.EOF Then
      InsertParola rtb, "<CHIAVI-SECONDARIE>", "01 KRR-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf & _
                                   Space(10) & "<COLONNA0>." & vbCrLf & _
                                    Space(7) & "01 K01-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf & _
                                   Space(10) & "<COLONNA1>." & vbCrLf & _
                                    Space(7) & "<CHIAVI-SECONDARIE>"
      
      For i = 1 To rsIdxCol.RecordCount
        Set TbColonne = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where idcolonna = " & rsIdxCol!IdColonna)
        If TbColonne.RecordCount Then
          NomeCol = Replace(TbColonne!Nome, "_", "-")
          Select Case TbColonne!Tipo
            Case "CHAR"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(" & TbColonne!lunghezza & ")." & vbCrLf & "<COLONNA_i>."
            Case "TIME"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(8)." & vbCrLf & "<COLONNA_i>."
            Case "DATE"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(10)." & vbCrLf & "<COLONNA_i>."
            Case "INTEGER"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(9) COMP." & vbCrLf & "<COLONNA_i>."
            Case "LONG"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(18) COMP." & vbCrLf & "<COLONNA_i>."
            Case "DECIMAL"
              Tipo = "PIC S9" & "(" & (CInt(TbColonne!lunghezza) - CInt(TbColonne!Decimali)) & ")"
              If TbColonne!Decimali = 0 Then
                Tipo = Tipo & ""
              Else
                Tipo = Tipo & "V9(" & TbColonne!Decimali & ") "
              End If
              Usage = " COMP-3."
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & Tipo & Usage & vbCrLf & "<COLONNA_i>."
            Case "SMALLINT"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(4) COMP." & vbCrLf & "<COLONNA_i>."
            Case "VARCHAR"
            Case "VARCHAR2"
            Case "TIMESTAMP"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(26)." & vbCrLf & "<COLONNA_i>."
            Case "NUMBER"
              Tipo = "PIC S9" & "(" & (CInt(TbColonne!lunghezza) - CInt(TbColonne!Decimali)) & ")"
              If TbColonne!Decimali = 0 Then
                Tipo = Tipo & ""
              Else
                Tipo = Tipo & "V9(" & TbColonne!Decimali & ") "
              End If
              Usage = " COMP-3."
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & Tipo & Usage & vbCrLf & "<COLONNA_i>."
          End Select
          
          For Ind = 0 To 6
            InsertParola rtb, "<COLONNA" & Ind & ">.", IIf(i > 1, Space(10), "") & Replace(colonna, "_i", Ind)
          Next Ind
        End If
        rsIdxCol.MoveNext
      Next i
      rsIdxCol.Close
      
      For Ind = 0 To 6
        DelParola rtb, "<COLONNA" & Ind & ">."
      Next Ind
    Else
      'ANOMALIA: no colonne appartenenti all'indice
      'per il momento segnaliamo solo, proseguendo...
      MsgBox "No columns for index " & TbIndex!Nome, vbExclamation
    End If
    TbIndex.MoveNext
  Loop
  TbIndex.Close
  DelParola rtb, "<CHIAVI-SECONDARIE>"
  Exit Sub
ERR:
  Select Case ERR.Number
    Case 380
      MsgBox "Creation copy interrupted", vbInformation, DataMan.DmNomeProdotto
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
      Resume Next
  End Select
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' "Riempie" il template "CPY_BASE_MOVE"
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub GeneraCpyMoveVSAM(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset, rsColonne As Recordset
  Dim env As Collection
  Dim suffissoStorico As String
  
  On Error GoTo errdb
  
  Screen.MousePointer = vbHourglass
  
  Set rsTabelle = m_fun.Open_Recordset("select idTable,nome,idOrigine from " & PrefDb & "_Tabelle where idDB = " & pIndex_DBCorrente)
  Do Until rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select idoggetto,nome,idArchivio,VSAM from DMVSM_Archivio where IdArchivio = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount Then
      'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load cos� non va in eoe!
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_MOVE") <> "" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_MOVE"
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_MOVE"" not found."
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
      
      changeTag RtbFile, "<VSAM-NAME>", rsSegmenti!VSAM
      changeTag RtbFile, "<NOME-SEGMENTO>", rsSegmenti!Nome & suffissoStorico
      changeTag RtbFile, "<NOME-TABELLA>", Replace(rsTabelle!Nome, "_", "-")
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Trattamento Campi:
      ' Ex soluzione: solo colonne "originali": con idTable=idTableJoin (no ereditate) e idCmp > 0 (no extra-columns)
      ' Nuova: tutte colonne (si controlla se ho il con template associato nelle specifiche routine...)
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where IdTable = " & rsTabelle!IdTable & " order by ordinale")
      If rsColonne.RecordCount Then
        'SQ: 28-08
        changeTag RtbFile, "<MOVE-VSAM2RDBMS>", getMoveVSAM2Rdbms(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome & suffissoStorico) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        rsColonne.MoveFirst
        changeTag RtbFile, "<MOVE-RDBMS2VSAM>", getMoveRdbms2VSAM(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome & suffissoStorico) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
      End If
            
      Set env = New Collection  'resetto;
      env.Add rsTabelle
      env.Add rsSegmenti
      If Len(MoveCopyParam) Then
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(MoveCopyParam, "tabelleVSM", env), 1
      Else
        MsgBox "No value found for 'MOVE-COPY' environment parameter.", vbExclamation
      End If
      rsColonne.Close
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Loop
  rsTabelle.Close
  
  Screen.MousePointer = vbDefault
  Exit Sub
errdb:
  Screen.MousePointer = vbDefault
  Select Case ERR.Number
    Case 75
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_MOVE") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case 7575
      ' non ha trovato il file (err.number "forzato" da noi...)
      ERR.Raise 75, , ERR.Description
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
  End Select
End Sub

Public Sub GeneraCpyMoveKeyVSAM(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset
  Dim env As Collection
   
  On Error GoTo errLoad
  
  Screen.MousePointer = vbHourglass
  
  Set rsTabelle = m_fun.Open_Recordset("select idTable,nome,idOrigine from " & PrefDb & "_Tabelle where idDB = " & pIndex_DBCorrente)
  Do Until rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select nome,idArchivio from DMVSM_Archivio where IdArchivio = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount Then
      'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load cos� non va in eoe!
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_MOVE_KEY") <> "" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_MOVE_KEY"
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_MOVE_KEY"" not found."
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
       
      changeTag RtbFile, "<VSAM-NAME>", rsSegmenti!VSAM
      changeTag RtbFile, "<MOVE-SETK-WKUTWKEY>", getSetkWkutwkeyVSAM(RtbFile)
      If Len(KCopyParam) Then
        Set env = New Collection  'resetto;
        env.Add pIndex_DBCorrente
        RtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\" & dbType & "\Cpy\" & getEnvironmentParam(KCopyParam, "dbd", env), 1
      Else
        MsgBox "tmp: problems on K-COPY parameter!!!!!!!!", vbExclamation
      End If
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Loop
  rsTabelle.Close
  Screen.MousePointer = vbDefault
  Exit Sub
errLoad:
  Screen.MousePointer = vbDefault
  Select Case ERR.Number
    Case 75
'      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_MOVE_KEY"" not found."
    Case 7575
      ERR.Raise 75, , ERR.Description
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
     'Resume
  End Select
End Sub

Function getMoveVSAM2Rdbms(RtbFile As RichTextBox, rsColonne As Recordset, nomeTable As String, nomeSegmento As String) As String
  Dim rtbString As String
  Dim rsCmp As Recordset, daCamb As Recordset
  
  rtbString = RtbFile.text
  
  On Error GoTo loadErr
  Do Until rsColonne.EOF
    If Not IsNull(rsColonne!RoutWrite) And Len(rsColonne!RoutWrite) Then
      RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\" & rsColonne!RoutWrite
      '''''''''''''''''''''''''''''''''
      ' RIEMPIO IL TEMPLATE!
      '''''''''''''''''''''''''''''''''
      'usare un tag piu' appropriato... (per replace...)
      changeTag RtbFile, "<NOME-COL>", Replace(rsColonne!Nome, "_", "-")
      changeTag RtbFile, "<LEN-COL>", rsColonne!lunghezza
      changeTag RtbFile, "<NOME-CAMPO>", IIf(IsNull(rsColonne!NomeCmp), "", rsColonne!NomeCmp)
      changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "_", "-")
      changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
           
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' SQ 14-06-06
      ' Gestione Chiavi ereditate
      ' SQ - ATTENZIONE! - UNIFORMARE LA GESTIONE CON LE NUOVE VARIABILI GLOBALI
      '                    idSegmentoParent,idTableParent,...
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If Right(rsColonne!Tag, 6) = "OCCURS" Then ' (potrei avere "AUTOMATIC-OCCURS")
        Set daCamb = m_fun.Open_Recordset("select Nome from " & PrefDb & "_Colonne where " & _
                                          "idcmp = " & rsColonne!IdCmp & " and IdTable = " & rsColonne!idTableJoin)
        changeTag RtbFile, "<NOME-COL-PARENT>", Replace(daCamb!Nome, "_", "-")
        daCamb.Close
        Set rsCmp = m_fun.Open_Recordset("select Nome from " & PrefDb & "_Tabelle where IdTable = " & rsColonne!idTableJoin)
        If rsCmp.RecordCount Then
          changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Nome, "_", "-")
        End If
        rsCmp.Close
      Else
        If Right(rsColonne!Tag, 10) = "DISPATCHER" Then ' (potrei avere "AUTOMATIC-DISPATCHER")
          'Gestione colonna "FIELD"
          If rsColonne!IdCmp > 0 Then '� l'idField!
            'FIELD
            Set rsCmp = m_fun.Open_Recordset("Select posizione,lunghezza from PsDli_Field where idField = " & rsColonne!IdCmp)
            If rsCmp.RecordCount Then
              'Attenzione, entra anche per le ereditate... non sono distinguibili!
              changeTag RtbFile, "<POS-SEGM-FIELD>", rsCmp!Posizione
              changeTag RtbFile, "<LEN-SEGM-FIELD>", rsCmp!lunghezza
            End If
          Else
            'XDFIELD: gestire!
            Set rsCmp = m_fun.Open_Recordset("Select srchFields from PsDli_XDField where idXDField = " & rsColonne!IdCmp)
          End If
          rsCmp.Close
        End If
        If rsColonne!idTableJoin <> rsColonne!IdTable Then
          Set rsCmp = m_fun.Open_Recordset("select b.nome as nome from MgData_Cmp as a,PsDli_Segmenti as b where " & _
                                           "a.IdCmp = " & rsColonne!IdCmp & " And a.IdSegmento = b.IdSegmento")
          If rsCmp.RecordCount Then
            changeTag RtbFile, "<NOME-SEGM-PARENT>", rsCmp!Nome
          End If
          rsCmp.Close
          
          If rsColonne!idTableJoin Then
            'Template di tipo "INHERITED
            Set rsCmp = m_fun.Open_Recordset("select a.Nome as colonna,b.nome as tabella from " & PrefDb & "_Colonne as a," & PrefDb & "_Tabelle as b where " & _
                                             "a.IdTable = b.IdTable And a.IdTable = " & rsColonne!idTableJoin & " And " & _
                                             "a.IdCmp = " & rsColonne!IdCmp)
            If rsCmp.RecordCount Then
              changeTag RtbFile, "<NOME-COL-PARENT>", Replace(rsCmp!colonna, "_", "-")
              changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Tabella, "_", "-")
            End If
            rsCmp.Close
          End If
        End If
      End If
      getMoveVSAM2Rdbms = getMoveVSAM2Rdbms & RtbFile.text & vbCrLf 'formattare il primo...
    End If
    rsColonne.MoveNext
  Loop
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\" & rsColonne!RoutWrite & """ not found."
    Case 7575
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
      'Resume
  End Select
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Uguale alla duale, cambia solo RoutWrite/RoutRead (farne una sola, volendo)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function getMoveRdbms2VSAM(RtbFile As RichTextBox, rsColonne As Recordset, nomeTable As String, nomeSegmento As String) As String
  Dim rtbString As String
  
  On Error GoTo loadErr
  
  rtbString = RtbFile.text
    
  Do Until rsColonne.EOF
    If Not IsNull(rsColonne!RoutRead) And Len(rsColonne!RoutRead) Then
      RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\" & rsColonne!RoutRead
      '''''''''''''''''''''''''''''''''
      ' RIEMPIO IL TEMPLATE!
      '''''''''''''''''''''''''''''''''
      'usare un tag piu' appropriato... (per replace...)
      changeTag RtbFile, "<NOME-COL>", Replace(rsColonne!Nome, "_", "-")
      changeTag RtbFile, "<NOME-CAMPO>", IIf(IsNull(rsColonne!NomeCmp), "", rsColonne!NomeCmp)
      changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "_", "-")
      changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
      changeTag RtbFile, "<LEN-COL>", rsColonne!lunghezza
   
      getMoveRdbms2VSAM = getMoveRdbms2VSAM & RtbFile.text & vbCrLf 'formattare il primo...
    End If
    rsColonne.MoveNext
  Loop
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\" & rsColonne!RoutRead & """ not found."
    Case 7575
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
      'resume
  End Select
End Function

Function getSetkWkutwkeyVSAM(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, rs As Recordset, rsColonne As Recordset, TbCmp As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  Dim statement As String, rrName As String, rdName As String, krdName As String, cCopyArea As String
  Dim continue As Boolean
  Dim env As Collection
  Dim pos As Integer, Lun As Integer
  Dim oldKey As String
  
  statement = "EVALUATE WK-NAMETABLE " & vbCrLf
  
  'TUTTE LE TABELLE TRANNE QUELLE DI OCCURS. POI PRENDE SOLO QUELLO CON SEGMENTO... PERDE LE "VIRTUALI"
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where " & _
                                       "idOrigine > 0 and TAG not like '%OCCURS' and iddb = " & pIndex_DBCorrente & " order by idTable")
  Do Until rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from DMVSM_Archivio where idarchivio = " & rsTabelle!IdOrigine)
    'stefano: ancora le redefines.. servono
    If rsSegmenti.RecordCount Then
      statement = statement & Space(3) & "WHEN '" & Replace(rsTabelle!Nome, "-", "_") & "' " & vbCrLf
      continue = False
      Set rs = m_fun.Open_Recordset("SELECT * from " & PrefDb & "_INDEX as a, " & PrefDb & "_IDXCOL as b, " & PrefDb & "_COLONNE as c where " & _
                                    "a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & " and " & _
                                    "a.idindex = b.idindex and b.idcolonna = c.idcolonna and " & _
                                    "c.idtable = c.idtablejoin and c.idcmp <> -1")
      If rs.RecordCount = 0 Then
        continue = True
      End If
      rs.Close
      
      If continue Then
        statement = statement & Space(6) & "CONTINUE" & vbCrLf
      Else
        Set env = New Collection  'resetto;
        env.Add rsColonne
        env.Add rsTabelle
        If Len(cCopyArea) Then
          cCopyArea = Replace(getEnvironmentParam(cCopyAreaParam, "colonne", env), "_", "-")
        Else
          'segnalare errore
          cCopyArea = "RR-" & Replace(rsTabelle!Nome, "_", "-")
        End If
        
        '''''''''''''''''''
        'CHIAVI PRIMARIE:
        '''''''''''''''''''
        Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_INDEX where tipo = 'P' and idtable = " & rsTabelle!IdTable)
        If rsIndex.RecordCount Then
          Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
          Do Until rsIdxCol.EOF
            Set rsColonne = m_fun.Open_Recordset("Select * from " & PrefDb & "_COLONNE where " & _
                                                 "idcolonna = " & rsIdxCol!IdColonna & " and idTable=idTableJoin")
            If rsColonne.RecordCount Then
              'Nome = "KRD-" & RsSegmenti!Nome
              krdName = "KRD-" & Replace(rsIndex!Nome, "_", "-")
              If rsColonne!IdCmp > 0 Then
                rrName = Replace(rsColonne!Nome, "_", "-")
                If Right(rsTabelle!Tag, 10) = "DISPATCHER" Then
                  'UNICA MOVE: COLONNA  "FIELD"
                  statement = statement & Space(5) & "MOVE " & rrName & " OF " & krdName & " TO " & vbCrLf & _
                                          Space(5) & "     " & rrName & " OF KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                Else
                  'SQ Ho gi� il nome campo nella DMDB2_Colonne...
                  'controllare che sia sempre coerente ed utilizzarlo!!!!!!
                  'idcmp non � pi� univoco
                  Set TbCmp = m_fun.Open_Recordset("select * from DMVSM_Data_cmp where " & _
                                                   "idcmp = " & rsColonne!IdCmp & " and idarchivio = " & rsTabelle!IdOrigine)
                  If TbCmp.RecordCount Then
                    rdName = TbCmp!Nome 'SQ - non abbiamo parametri di modifica nome?
                  Else
                    MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                    TbCmp.Close
                    Exit Function
                  End If
                  If pos = 0 Then pos = 1
                  If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                    pos = 1
                    oldKey = Replace(rsIndex!Nome, "_", "-")
                  End If
                  Lun = TbCmp!Byte
                  If Len(rsColonne!RoutKey) = 0 Then
                    statement = statement & CreaMvCol_VSAM(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & "", rdName, krdName, TbCmp!Usage, True, pos, Lun)
                  Else
                    statement = statement & CreaMvCol_VSAM(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutKey & "", rdName, krdName, TbCmp!Usage, True, pos, Lun)
                  End If
                  pos = pos + TbCmp!Byte
                End If  'dispatcher
              End If  'idCmp > 0
            Else
              m_fun.WriteLog "#####getSetkWkutwkey: column " & rsIdxCol!IdColonna & " not found for index " & rsIndex!IdIndex
            End If
            rsIdxCol.MoveNext
          Loop
        End If 'end "CHIAVI PRIMARIE"
        ''''''''''''''''''''''
        ' CHIAVI SECONDARIE:
        ''''''''''''''''''''''
        Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where " & _
                                           "tipo = 'I' and used = true and idtable = " & rsTabelle!IdTable & _
                                           " order by idIndex")  'ordine necessario per nome...
        Do Until rsIndex.EOF
          Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where " & _
                                              "idindex = " & rsIndex!IdIndex & " order by ordinale ")
          Do Until rsIdxCol.EOF
            Set rsColonne = m_fun.Open_Recordset("Select * from " & PrefDb & "_COLONNE where " & _
                                                 "idcolonna = " & rsIdxCol!IdColonna & " and idTable = idTableJoin")
            If rsColonne.RecordCount Then
              If rsColonne!IdCmp > 0 Then
                rrName = Replace(rsColonne!Nome, "_", "-")
                Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where " & _
                                                 "idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                If TbCmp.RecordCount Then
                  '????
                  If rsColonne!Nome = rsColonne!NomeCmp Then
                    rdName = TbCmp!Nome
                  Else
                    rdName = Replace(rsColonne!Nome, "_", "-")
                  End If
                  krdName = "KRD-" & Replace(rsIndex!Nome, "_", "-")
                  'newTempl = rsColonne!RoutWrite
                  If pos = 0 Then pos = 1
                  If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                    pos = 1
                    oldKey = Replace(rsIndex!Nome, "_", "-")
                  End If
                  Lun = TbCmp!Byte
                  If Len(rsColonne!RoutKey) = 0 Then
                    statement = statement & CreaMvCol_VSAM(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & " ", rdName, krdName, TbCmp!Usage, False, pos, Lun)
                  Else
                    statement = statement & CreaMvCol_VSAM(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutKey & " ", rdName, krdName, TbCmp!Usage, False, pos, Lun)
                  End If
                  pos = pos + TbCmp!Byte
                Else
                  MsgBox "No cobol field found for " & rsColonne!Nome & " column.", vbExclamation
                  TbCmp.Close
                  rsIdxCol.Close
                  rsIndex.Close
                  rsSegmenti.Close
                  Exit Function
                End If
                TbCmp.Close
              End If
            Else
              m_fun.WriteLog "#####getSetkWkutwkeyVSAM: column " & rsIdxCol!IdColonna & " not found for index " & rsIndex!IdIndex
            End If
            rsIdxCol.MoveNext
          Loop
          rsIdxCol.Close
          rsIndex.MoveNext
        Loop
        rsIndex.Close
      End If  'continue
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Loop
  rsTabelle.Close
  
  statement = statement & "END-EVALUATE. "
  getSetkWkutwkeyVSAM = statement
End Function

Public Function CreaMvCol_VSAM(RtbFile As RichTextBox, nomeSegmento As String, nomeTable As String, NomeCol As String, NomeStr1 As String, TipoCol As String, template As String, NomeCmp As String, nomeArea As String, tipoCmp As String, wKeyPrimary As Boolean, pos As Integer, Lun As Integer) As String
  Dim statement As String, rtbString As String
  
  On Error GoTo loadErr
  
  If Len(Trim(template)) Then 'togliere trim e pulire le colonne!!!!!!!!!!!!!!!!!!!!!!!
    'salvo
    rtbString = RtbFile.text
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\" & template
    
    changeTag RtbFile, "RD-<NOME-SEGM>", nomeArea
    changeTag RtbFile, "RR-<NOME-TAB>", NomeStr1
    'ATTENZIONE: DEVONO STARE QUI SOTTO!
    changeTag RtbFile, "<NOME-TAB>", nomeTable
    changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
    
    changeTag RtbFile, "<NOME-CAMPO>", NomeCmp
    changeTag RtbFile, "<NOME-COL>", NomeCol
    changeTag RtbFile, "<POS-FIELD>", Format(pos)
    changeTag RtbFile, "<LEN-FIELD>", Format(Lun)
    'rapina:
    changeTag RtbFile, " RD-", " KRD-"
    'SP 14/9 doppia rapina, se gli passa rr usa direttamente rr: perch� non usa direttamente il dato passato?
    If Left(NomeStr1, 3) = "KRR" Then
      changeTag RtbFile, " RR-", " KRR-"
    End If
    RtbFile.text = Space(5) & RtbFile.text
    RtbFile.text = Replace(RtbFile.text, vbCrLf, vbCrLf & Space(5))
    
    statement = RtbFile.text & vbCrLf
    'restore:
    RtbFile.text = rtbString
  Else
    'TEMPLATE REVISERIANO!... gestire tutto da fuori (solo template!)
    Select Case TipoCol
      Case "DATE"
        If tipoCmp = "PCK" Then
          statement = statement & Space(6) & "MOVE " & NomeCmp & " OF " & nomeArea & " TO DATA-8" & vbCrLf & _
                                  Space(6) & "MOVE 'DTD2' TO WF-FUNZIONE" & vbCrLf & _
                                  Space(6) & "CALL WF-NOMEROUT USING" & vbCrLf & _
                                  Space(9) & "DATA-8" & vbCrLf & _
                                  Space(9) & NomeCol & " OF " & NomeStr1 & vbCrLf & _
                                  Space(9) & "WF-PARAMETRI" & vbCrLf
        Else
          statement = statement & Space(6) & "MOVE 'DTD2' TO WF-FUNZIONE" & vbCrLf & _
                                  Space(6) & "CALL WF-NOMEROUT USING" & vbCrLf & _
                                  Space(9) & NomeCmp & " OF " & nomeArea & vbCrLf & _
                                  Space(9) & NomeCol & " OF " & NomeStr1 & vbCrLf & _
                                  Space(9) & "WF-PARAMETRI" & vbCrLf
        End If
      Case "TIME"
        statement = statement & Space(6) & "MOVE 'TMT2' TO WF-FUNZIONE" & vbCrLf & _
                                Space(6) & "CALL WF-NOMEROUT USING" & vbCrLf & _
                                Space(9) & NomeCmp & " OF RD-" & nomeArea & vbCrLf & _
                                Space(9) & NomeCol & " OF RR-" & NomeStr1 & vbCrLf & _
                                Space(9) & "WF-PARAMETRI" & vbCrLf
      Case Else
        statement = statement & Space(6) & "MOVE " & NomeCmp & " OF " & nomeArea & " TO " & vbCrLf & _
                                Space(9) & NomeCol & " OF " & NomeStr1 & vbCrLf
        If TipoCol = "DECIMAL" Then
           statement = statement & Space(6) & "IF " & NomeCol & " OF " & NomeStr1 & vbCrLf & _
                                   Space(18) & "NOT NUMERIC " & vbCrLf & _
                                   Space(9) & "MOVE ZEROES TO " & NomeCol & " OF " & NomeStr1 & vbCrLf & _
                                   Space(6) & "END-IF" & vbCrLf
        End If
    End Select
  End If
  CreaMvCol_VSAM = statement
  
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\" & template & " not found."
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
      'resume
  End Select
End Function

'Mauro 14/09/2007
Public Sub getEnvironmentParameters()
  VSAM_dbName = LeggiParam("VSAM_DBNAME")
  VSAM_LookUpName = LeggiParam("VSAM_LOOKUPNAME")
  VSAM_TbName = LeggiParam("VSAM_TBNAME")
  VSAM_ColName = LeggiParam("VSAM_COLNAME")
  VSAM_PKName = LeggiParam("VSAM-PK-NAME")
  VSAM_TbsName = LeggiParam("VSAM_TBSNAME")
  VSAM_UKName = LeggiParam("VSAM-UK-NAME")
  VSAM_FKName = LeggiParam("VSAM-FK-NAME")
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''
' "Riempie" il template "DMDB2_CPY_BASE_MOVE.cpy"
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub GeneraCpyLoadVSAM(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset, rsColonne As Recordset
  Dim env As Collection
  
  On Error GoTo errdb
  
  Screen.MousePointer = vbHourglass
  
  RtbFile.text = ""
  
  Set rsTabelle = m_fun.Open_Recordset("select idTable,nome, idOrigine from " & PrefDb & "_Tabelle where idDB = " & pIndex_DBCorrente)
  Do Until rsTabelle.EOF
    '''''''''''''''''''''''''''''''''''''
    ' Per ogni TABELLA associata al DBD:
    '''''''''''''''''''''''''''''''''''''
    Set rsSegmenti = m_fun.Open_Recordset("select * from DMVSM_RecordType where IdRecordType = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount Then
      'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load cos� non va in eoe!
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_LOAD") <> "" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_LOAD"
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\CPY_BASE_LOAD"" not found."
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
      
      changeTag RtbFile, "<DBD-NAME>", rsSegmenti!VSAM 'pNome_DBCorrente
      changeTag RtbFile, "<NOME-SEGMENTO>", Replace(rsSegmenti!Nome, "_", "-")
      changeTag RtbFile, "<NOME-TABELLA>", Replace(rsTabelle!Nome, "_", "-")
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Trattamento Campi:
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where " & _
                                           "IdTable = " & rsTabelle!IdTable & " AND routLoad <> null AND routLoad <> '' order by ordinale")
      If rsColonne.RecordCount Then
        'SP exec dli
        'changeTag rtbFile, "<MOVE-DLI2RDBMS>.", getMoveLoad(rtbFile, rsColonne, RsTabelle!Nome, rsSegmenti!Nome) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        changeTag RtbFile, "<MOVE-VSAM2RDBMS>", getMoveLoadVSAM(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome, rsSegmenti!IdRecordType, rsSegmenti!VSAM) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
      End If
            
      If Len(LoadProgramParam) Then
        Set env = New Collection  'resetto;
        'env.Add rsTabelle
        env.Add rsSegmenti!VSAM
        changeTag RtbFile, "<PGM-NAME>", getEnvironmentParam(LoadProgramParam, "tabelleVSM", env)
      Else
        MsgBox "tmp: problems on LOAD-PROGRAM parameter!!!!!!!!", vbExclamation
      End If
                    
      If Len(LoadCopyParam) Then
        Set env = New Collection  'resetto;
        env.Add rsTabelle
        env.Add rsSegmenti
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(LoadCopyParam, "tabelleVSM", env), 1
      Else
        MsgBox "No value found for 'LOAD-COPY' environment parameter.", vbExclamation
      End If
      rsColonne.Close
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Loop
  rsTabelle.Close
  Screen.MousePointer = vbDefault
  Exit Sub
errdb:
  Screen.MousePointer = vbDefault
  Select Case ERR.Number
    Case 75
      'VB non ha trovato il file
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_LOAD") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_LOAD"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case 7575
      ' non ha trovato il file (err.number "forzato" da noi...)
      ERR.Raise 75, , ERR.Description
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
      'resume
  End Select
End Sub

Function getMoveLoadVSAM(RtbFile As RichTextBox, rsColonne As Recordset, _
                         nomeTable As String, nomeSegmento As String, IdSegmento As Long, VSAM As String) As String
  Dim rsCmp As Recordset, rsCmp2 As Recordset
  Dim rtbString As String
  
  On Error GoTo loadErr
  
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
  Do Until rsColonne.EOF
    'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & rsColonne!RoutLoad
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\" & rsColonne!RoutLoad
    '''''''''''''''''''''''''''''''''
    ' RIEMPIO IL TEMPLATE!
    '''''''''''''''''''''''''''''''''
    changeTag RtbFile, "<NOME-COL>", Replace(rsColonne!Nome, "_", "-")
    changeTag RtbFile, "<LEN-COL>", rsColonne!lunghezza
    changeTag RtbFile, "<NOME-CAMPO>", IIf(IsNull(rsColonne!NomeCmp), "", rsColonne!NomeCmp)
    changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "_", "-")
    changeTag RtbFile, "<NOME-SEGM>", Replace(nomeSegmento, "_", "-")
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 14-06-06
    ' Gestione Chiavi ereditate
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Right(rsColonne!Tag, 6) = "OCCURS" Then ' (potrei avere "AUTOMATIC-OCCURS")
      'SQ - TMP: questa gestione particolare per le OCCURS � un po' forzata... meglio cambiare template/tag!
      changeTag RtbFile, "<NOME-COL-PARENT>", Replace(rsColonne!Nome, "_", "-") 'vincolo: stesso nome (accettabile)
      Set rsCmp = m_fun.Open_Recordset("select Nome from " & PrefDb & "_Tabelle where IdTable=" & rsColonne!idTableJoin)
      If rsCmp.RecordCount Then
        changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Nome, "_", "-")
      End If
      rsCmp.Close
    Else
      If Right(rsColonne!Tag, 10) = "DISPATCHER" Then ' (potrei avere "AUTOMATIC-DISPATCHER")
        'SQ - TMP: questa gestione particolare per le OCCURS � un po' forzata... meglio cambiare template/tag!
        'changeTag rtbFile, "<NOME-COL-PARENT>", Replace(rsColonne!Nome, "_", "-") 'vincolo: stesso nome (accettabile)
        'Set rsCmp = m_fun.Open_Recordset("select Nome from DMDB2_Tabelle where IdTable=" & rsColonne!IdTableJoin)
        'If rsCmp.RecordCount Then
        '  changeTag rtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Nome, "_", "-")
        'End If
        'rsCmp.Close
        'Gestione colonna "FIELD"
        If rsColonne!IdCmp > 0 Then '� l'idField!
          'FIELD
          Set rsCmp = m_fun.Open_Recordset("Select posizione,lunghezza from PsDli_Field where " & _
                                           "idField = " & rsColonne!IdCmp & " AND IdSegmento = " & IdSegmento)
          If rsCmp.RecordCount Then
            'Attenzione, entra anche per le ereditate... non sono distinguibili!
            changeTag RtbFile, "<POS-SEGM-FIELD>", rsCmp!Posizione
            changeTag RtbFile, "<LEN-SEGM-FIELD>", rsCmp!lunghezza
          End If
        Else
          'XDFIELD: gestire!
          Set rsCmp = m_fun.Open_Recordset("Select srchFields from PsDli_XDField where idXDField = " & rsColonne!IdCmp)
        End If
        rsCmp.Close
      End If
      If rsColonne!idTableJoin <> rsColonne!IdTable Then
       'Set rsCmp = m_fun.Open_Recordset("select b.nome as nome from DMDB2_Tabelle as a,PsDli_Segmenti as b where a.IdTable = " & rsColonne!IdTableJoin & " And a.IdOrigine = b.IdSegmento")
       Set rsCmp = m_fun.Open_Recordset("select b.nome as nome from DMVSM_Data_Cmp as a,DMVSM_RecordType as b where " & _
                                        "a.IdCmp = " & rsColonne!IdCmp & " And a.VSAM = b.VSAM and " & _
                                        "a.idoggetto = b.idoggetto and a.idarea = b.idarea")
       If rsCmp.RecordCount Then
         changeTag RtbFile, "<NOME-SEGM-PARENT>", rsCmp!Nome
       End If
       rsCmp.Close
       
       If rsColonne!idTableJoin Then
         'Template di tipo "INHERITED
         Set rsCmp = m_fun.Open_Recordset("select a.Nome as colonna,b.nome as tabella from " & PrefDb & "_Colonne as a," & PrefDb & "_Tabelle as b where " & _
                                          "a.IdTable = b.IdTable AND a.IdTable = " & rsColonne!idTableJoin & " AND a.idCmp = " & rsColonne!IdCmp)
         If rsCmp.RecordCount Then
           '"MOVE " & Replace(rsOrigine(0), "_", "-") & " OF RR-" & rsOrigine(1) & " TO " & vbCrLf
           changeTag RtbFile, "<NOME-COL-PARENT>", Replace(rsCmp!colonna, "_", "-")
           changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Tabella, "_", "-")
         End If
         rsCmp.Close
       End If
      End If
    End If

    If Not IsNull(rsColonne!NomeCmp) Then
      Set rsCmp = m_fun.Open_Recordset("select VSAM,Byte,Ordinale,posizione,livello from DMVSM_Data_Cmp where " & _
                                       "IdCmp = " & rsColonne!IdCmp & " and VSAM = '" & VSAM & "'")
      'stefano: evito il loop
      If Not rsCmp.EOF Then
        changeTag RtbFile, "<LEN-FIELD>", IIf(IsNull(rsCmp!Byte), "", rsCmp!Byte)
        changeTag RtbFile, "<POS-FIELD>", rsCmp!Posizione
    
        'Set rsCmp2 = m_fun.Open_Recordset("select Nome,LIVELLO from MgData_Cmp where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!Livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 and decimali = 0 order by Ordinale desc")
        Set rsCmp2 = m_fun.Open_Recordset("select Nome,LIVELLO from DMVSM_Data_Cmp where " & _
                                          "VSAM = '" & rsCmp!VSAM & "' AND livello < " & rsCmp!livello & _
                                          " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 and decimali = 0 order by Ordinale desc")
        If rsCmp2.RecordCount Then
          'SQ 10-07-06
          'SE IL GRUPPO E' IL LIVELLO 01, NON CE L'ABBIAMO NELLA COPY T! E' LO "RD-..."
          If rsCmp2!livello = 1 Then
            changeTag RtbFile, "<GROUP-NAME>", "RD-" & Replace(nomeSegmento, "_", "-") 'UTILIZZARE IL PARAMETRO DI SISTEMA!!!!!
          Else
            changeTag RtbFile, "<GROUP-NAME>", rsCmp2!Nome
          End If
        Else
          'SQ 05-08-05
          m_fun.WriteLog "getMoveLoadVSAM: tmp-error: no group field found!"
        End If
        rsCmp2.Close
      End If
      rsCmp.Close
    End If
          
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' GESTIONE XDFIELD... generalizzare... solo banca...
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'changeTag rtbFile, "<XDFIELD-MOVE>", getXdfieldMove_BPER(rsColonne!Nome)
    'If Right(rsColonne!RoutLoad, 4) = "EXIT" Then
    'changeTag rtbFile, "<CONDIZIONE>", getCondizione_BPER(rtbFile, rsColonne!Nome)
    
    getMoveLoadVSAM = getMoveLoadVSAM & RtbFile.text & vbCrLf  'formattare il primo...
                    
    rsColonne.MoveNext
  Loop
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\fields\" & dbType & "\" & rsColonne!RoutLoad & """ not found."
    Case 7575
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
      'Resume
  End Select
End Function

Public Sub GeneraProgrammaLoadVSAM(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset, rsTabelleOccurs As Recordset
  Dim maxLenSeg As Long, dclgenLen As Long
  Dim copyname As String, cCopyParam As String, tCopyParam As String
  Dim env As Collection
  Dim originTable As Boolean, isDispatcher As Boolean, ok As Boolean
  Dim rsAlias As Recordset
  Dim NomeTb As String
  Dim tbOccurs As String
  Dim AcronymVSAM As String, rsVSAM As Recordset
  Dim assignName As String
  Dim rsArchivio As Recordset
  
  On Error GoTo errLoad
  
  'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load cos� non va in eoe!
  If Dir(DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\dcp\" & dbType & "\LOAD_PROGRAM") <> "" Then
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\dcp\" & dbType & "\LOAD_PROGRAM"
  Else
    DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\dcp\" & dbType & "\LOAD_PROGRAM"" not found."
    Exit Sub
  End If
  
  cCopyParam = LeggiParam("C-COPY")
  tCopyParam = LeggiParam("T-COPY")
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' SCARTO LE OCCURS... non � vero!...
  ' -> ogni tabella "origine" gestir� le sottotabelle in fondo
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "idDB = " & pIndex_DBCorrente & " order by idOrigine,idTable")
  ' Mauro 23/11/2007 : Che roba assurda! E tutto solo per portarmi fuori l'acronimo del VSAM  per il nome della DCP
  AcronymVSAM = ""
  Do Until rsTabelle.EOF
    If rsTabelle!IdOrigine Then
      Set rsVSAM = m_fun.Open_Recordset("select a.Acronimo from DMVSM_NomiVSAM as a, DMVSM_RecordType as b where " & _
                                        "b.idrecordtype = " & rsTabelle!IdOrigine & " and b.VSAM = a.NomeVSAM")
      If rsVSAM.RecordCount Then
        AcronymVSAM = rsVSAM!Acronimo
      End If
      rsVSAM.Close
      Exit Do
    End If
    rsTabelle.MoveNext
  Loop
  rsTabelle.MoveFirst
  
  Do Until rsTabelle.EOF
    Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
    If rsAlias.EOF Then
      NomeTb = rsTabelle!Nome
    Else
      NomeTb = rsAlias!Nome
    End If
    rsAlias.Close
    
    isDispatcher = False  'init
    ok = False            'init
    'gestione PILOTA+REDEFINES
    If Right(rsTabelle!Tag, 6) = "LOOKUP" Then 'normalmente ho AUTOMATIC-LOOKUP
      DoEvents
      'tmp:
      'DISPATCHER da gestire...
      'Scarto quella che per ora � la principale: di troppo!
      'virgilio: ora non la scarto pi� perch� mi serve!
      'rsTabelle.MoveNext
      isDispatcher = True
      'Set rsSegmenti = m_fun.Open_Recordset("select * from DMVSM_RecordType where IdRecordType = " & rsTabelle!IdOrigine)
    ElseIf Right(rsTabelle!Tag, 6) <> "OCCURS" Then
      '''''''''''''''''''''''''''''''''''''
      ' TABELLE "OCCURS"
      '''''''''''''''''''''''''''''''''''''
      Set rsTabelleOccurs = m_fun.Open_Recordset("select idTable,nome,idOrigine,Dimensionamento from " & PrefDb & "_Tabelle where " & _
                                                 "idOrigine = " & rsTabelle!IdOrigine & " and TAG like '%OCCURS' order by idTable")
      originTable = Not rsTabelleOccurs.EOF
      '''''''''''''''''''''''''''''''''''''
      ' Per ogni TABELLA associata al DBD:
      '''''''''''''''''''''''''''''''''''''
      Set rsSegmenti = m_fun.Open_Recordset("select * from DMVSM_RecordType where IdRecordType = " & rsTabelle!IdOrigine)
    End If
    
    'SQ  - Incredibile... cosa mi tocca fare!
    If Not rsSegmenti Is Nothing Then
      ok = rsSegmenti.RecordCount
    End If
    If isDispatcher Then ok = True
    
    If Right(rsTabelle!Tag, 6) = "OCCURS" Or ok Then
      ' Mauro 28/11/2007 : Il nome esterno non dev'essere pi� lungo di 8 caratteri
      'changeTag RtbFile, "<SELECT-TABLES(i)>", "SELECT F" & Replace(NomeTb, "_", "") & vbCrLf & _
      '                                         "   ASSIGN TO " & Replace(NomeTb, "_", "") & "."
      assignName = Replace(NomeTb, "_", "")
      If Len(assignName) > 8 Then
        MsgBox "External file name too long: '" & assignName & "' (MAX 8 CHAR)", vbExclamation, DataMan.DmNomeProdotto
      End If
      changeTag RtbFile, "<SELECT-TABLES(i)>", "SELECT F" & assignName & vbCrLf & _
                                               "   ASSIGN TO " & assignName & "."
      
      dclgenLen = getDclgenLen(rsTabelle!IdTable)
      changeTag RtbFile, "<FD-TABLES(i)>", "FD F" & Replace(NomeTb, "_", "") & vbCrLf & _
                                  Space(4) & "RECORD CONTAINS " & dclgenLen & " CHARACTERS" & vbCrLf & _
                                  Space(4) & "LABEL RECORD STANDARD" & vbCrLf & _
                                  Space(4) & "DATA RECORD REC-F" & Replace(NomeTb, "_", "") & "." & vbCrLf & _
                                             "01  REC-F" & Replace(NomeTb, "_", "") & Space(5) & "PIC X(" & dclgenLen & ")."
      If Not isDispatcher Then
        Set env = New Collection  'resetto;
        env.Add rsTabelle
        env.Add rsSegmenti
        copyname = getEnvironmentParam(LoadCopyParam, "tabelleVSM", env)
        changeTag RtbFile, "<LOAD-COPIES(i)>", "COPY " & copyname & "."
      End If
      
      'OPEN FILE
      changeTag RtbFile, "<OPEN-FILE(i)>", "OPEN OUTPUT F" & Replace(NomeTb, "_", "") & "."
      changeTag RtbFile, "<CLOSE-FILE(i)>", "CLOSE F" & Replace(NomeTb, "_", "")

      'DISPLAY CONTATORI
      changeTag RtbFile, "<DISPL-OUTPUT(i)>", "DISPLAY 'Record  F" & Replace(NomeTb, "_", "") & " : '" & vbCrLf & _
                                   Space(3) & "CTR-" & Replace(NomeTb, "_", "")
      changeTag RtbFile, "<CTR-OUTPUT(i)>", "01  CTR-" & Replace(NomeTb, "_", "") & " PIC 9(8) VALUE ZERO."
  
      ' Virtuale = False  'Non utilizzato
  
      If Not isDispatcher Then
        If Len(cCopyParam) Then
          If Right(rsTabelle!Tag, 6) = "OCCURS" Then ' potrei avere "AUTOMATIC-OCCURS"
            env.Add Format(rsTabelle!Dimensionamento, "00")
          Else
            env.Add ""
          End If
          changeTag RtbFile, "<C-COPIES(i)>", "COPY " & getEnvironmentParam(cCopyParam, "tabelleVSM", env) & "."
        Else
          m_fun.WriteLog "Parametro C-COPY non definito."
        End If
      End If
      
      'SQ 21-11-07 Problema INITIALIZE dentro la COPY MOVE:
      'La facciamo una volta per tutte alla INIT e lo togliamo dal ...-TO-RR (soluzione definitiva?)
      changeTag RtbFile, "<INITIALIZE-RR(i)>", "INITIALIZE RR-" & Replace(NomeTb, "_", "-")
      If Right(rsTabelle!Tag, 6) <> "OCCURS" Then 'es: "AUTOMATIC-OCCURS"
        If Not isDispatcher Then
          If Len(tCopyParam) Then
            changeTag RtbFile, "<T-COPIES(i)>", "COPY " & getEnvironmentParam(tCopyParam, "tabelleVSM", env) & "."
          Else
            m_fun.WriteLog "Parametro T-COPY non definito."
          End If
        End If
        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Segmento di lunghezza max. Serve per l'unica area host di lettura
        ' SQ: a chi serve?
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If isDispatcher Then
          Set rsArchivio = m_fun.Open_Recordset("select c.* from " & PrefDb & "_DB as a, DMVSM_NomiVSAM as b, DMVSM_Archivio as c, " & PrefDb & "_tabelle as d where " & _
                                                "a.iddb = d.iddb and a.nome = b.acronimo and b.nomeVSAM = c.VSAM and d.idtable = " & rsTabelle!IdTable)
        Else
          Set rsArchivio = m_fun.Open_Recordset("select * from DMVSM_Archivio where VSAM = '" & rsSegmenti!VSAM & "'")
        End If
        If rsArchivio.RecordCount Then
          If rsArchivio!MaxLen > maxLenSeg Then
            maxLenSeg = rsArchivio!MaxLen
          End If
        End If
        rsArchivio.Close
        
        If isDispatcher Then
          InserisciChiavePrimariaRedefinesVSAM RtbFile, rsTabelle!IdTable, PrefDb 'virgilio: vogliamo metterli su tutti gli accessi in comune (mezzo gaudio)
        Else
          'changeTag RtbFile, "<AREA-COPY>", "RD-" & Replace(rsSegmenti!Nome, "_", "-")
          changeTag RtbFile, "<MOVE-AREA-COPY(i)>", "MOVE REC-<VSAM-NAME> TO RD-" & Replace(rsSegmenti!Nome, "_", "-")
          'GESTIONE OCCURS
          InserisciChiavePrimariaVSAM RtbFile, rsSegmenti, rsTabelle, originTable
        End If
        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' OCCURS: GESTIONE SOTTOTABELLE
        ' SONO SULLA TABELLA ORIGINE: SOPRA HO PREPARATO I TAG CICLICI PER LE SOTTOTABELLE
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If originTable Then
          Do Until rsTabelleOccurs.EOF
            Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelleOccurs!IdTable)
            If rsAlias.EOF Then
              tbOccurs = rsTabelleOccurs!Nome
            Else
              tbOccurs = rsAlias!Nome
            End If
            rsAlias.Close

            changeTag RtbFile, "<OCCURS-MOVE(i)>", "MOVE '" & tbOccurs & "' TO WK-TABLE-OCCURS" & vbCrLf & _
                                                   "PERFORM SCRIVI-OUT-OCCURS"
            changeTag RtbFile, "<WHEN-TABLE-OCCURS(i)>", "WHEN '" & tbOccurs & "'" & vbCrLf _
                                            & Space(4) & "PERFORM VARYING WINDICE FROM 1 BY 1" & vbCrLf _
                                            & Space(4) & "   UNTIL WINDICE > " & getOccursLen(rsTabelleOccurs!Dimensionamento, rsTabelleOccurs!IdOrigine) & vbCrLf _
                                            & Space(4) & "   MOVE WINDICE TO WK-KEYSEQ" & vbCrLf _
                                            & Space(4) & "   PERFORM 9999-" & Replace(tbOccurs, "_", "-") & "-TO-RR" & vbCrLf _
                                            & Space(4) & "   ADD 1 TO CTR-" & Replace(tbOccurs, "_", "") & vbCrLf _
                                            & Space(4) & "   WRITE  REC-F" & Replace(rsTabelle!Nome, "_", "") & vbCrLf _
                                            & Space(4) & "      FROM RR-" & Replace(rsTabelle!Nome, "_", "-") & vbCrLf _
                                            & Space(4) & "END-PERFORM"
            rsTabelleOccurs.MoveNext
          Loop
          '<OCCURS-MOVE(i)> � "ciclico parziale": muore a fine ciclo interno
          DelParola RtbFile, "<OCCURS-MOVE(i)>"
        End If
      End If
    End If
    rsTabelle.MoveNext
  Loop
    
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  '  Set rsSegmenti = m_fun.Open_Recordset("SELECT * FROM mgDLI_Segmenti WHERE idoggetto=" & TbApp!Iddbor)
  '  If rsSegmenti.RecordCount <> 0 Then
  '... SQ: c'era un sacco di roba...
  ' End If
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  changeTag RtbFile, "<VSAM-NAME>", pNome_DBCorrente
  changeTag RtbFile, "<DATA-CREAZ>", Date
  changeTag RtbFile, "<DATA-MODIF>", Date
  changeTag RtbFile, "<VSAM-LEN>", maxLenSeg & ""
     
  cleanTags RtbFile
  'Mauro 27/11/2007 : Temporaneo - La CleanTags non ripulisce i tag singoli, ma solo i ciclici (i)
  ' Se il VSAM ha solo un RecordType, questo tag non viene scritto, e la DCP rimane sporca
  changeTag RtbFile, "<WRITE-DISP>", ""
  
  ''''''''''''''''''''''''''''''''''''''''''''''''
  ' NOME PROGRAMMA:
  ''''''''''''''''''''''''''''''''''''''''''''''''
  Set env = New Collection 'resetto
  'env.Add ""
  env.Add AcronymVSAM
  copyname = getEnvironmentParam(LoadProgramParam, "tabelleVSM", env)
  changeTag RtbFile, "<NOME-PGM>", copyname

  RtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\" & dbType & "\VSAM\" & copyname, 1

  rsTabelle.Close
  Exit Sub
errLoad:
  Select Case ERR.Number
    Case 75
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\dcp\" & dbType & "\LOAD_PROGRAM") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\" & dbType & "\LOAD_PROGRAM"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
      'Resume
  End Select
End Sub

Sub InserisciChiavePrimariaRedefinesVSAM(rtb As RichTextBox, IdTable As Long, PrefDb As String)
  'virgilio inserito il PrefDb sulle query per gestire anche ORACLE
  Dim rsOrigine As Recordset, rsTabelle As Recordset
  Dim wNomeSeg As String
  Dim tableName As String
  Dim NomeTb As String
  Dim rsAlias As Recordset
  Dim nomeTabella As String
  Dim rsSegmenti As Recordset
  
  Set rsTabelle = m_fun.Open_Recordset("select nome from " & PrefDb & "_tabelle where idtable = " & IdTable)
  If rsTabelle.RecordCount Then
    Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & IdTable)
    If rsAlias.EOF Then
      NomeTb = rsTabelle!Nome
    Else
      NomeTb = rsAlias!Nome
    End If
    rsAlias.Close
  
    changeTag rtb, "<WRITE-DISP>", "PERFORM " & Replace(NomeTb, "_", "-") & "-TO-RR" & vbCrLf _
                                 & "ADD 1 TO CTR-" & Replace(NomeTb, "_", "") & vbCrLf _
                                 & "WRITE  REC-F" & Replace(NomeTb, "_", "") & vbCrLf _
                                 & "   FROM  RR-" & Replace(rsTabelle!Nome, "_", "-")
    
  End If
  rsTabelle.Close
  
  Set rsOrigine = m_fun.Open_Recordset("select b.* from " & PrefDb & "_tabelle as a, " & PrefDb & "_tabelle as b where " & _
                                       "a.idtable = " & IdTable & " and a.iddb = b.iddb and b.tag not like '%OCCURS'")
  Do Until rsOrigine.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from DMVSM_RecordType where IdRecordType = " & rsOrigine!IdOrigine)
    If rsSegmenti.RecordCount Then
      wNomeSeg = rsSegmenti!Nome
      
      Set rsTabelle = m_fun.Open_Recordset("select idTable,Nome, condizione from " & PrefDb & "_Tabelle where " & _
                                           "IdOrigine = " & rsSegmenti!IdRecordType)
      nomeTabella = Replace(rsTabelle!Nome, "_", "-")
      If rsTabelle.RecordCount Then
        Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
        If rsAlias.EOF Then
          NomeTb = rsTabelle!Nome
        Else
          NomeTb = rsAlias!Nome
        End If
        rsAlias.Close
      
        tableName = Replace(NomeTb, "_", "-")
        If Len(rsTabelle!Condizione & "") Then
          changeTag rtb, "<REC-TYPE-CONDITION(i)>", "IF <CONDIZIONE>" & vbCrLf & _
                                                    "   MOVE '<RECTYPE-NAME>' TO WK-TABLE" & vbCrLf & _
                                                    "END-IF"
          changeTag rtb, "<CONDIZIONE>", getCondizione_RedefinesVSAM(rtb, IIf(IsNull(rsTabelle!Condizione), "", rsTabelle!Condizione))
        Else
          changeTag rtb, "<REC-TYPE-CONDITION(i)>", "MOVE '<RECTYPE-NAME>' TO WK-TABLE"
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Pi� lungo/lento ma modulare:
        ' - gi� pronto per Template innestati...
        ' - A posto per indentazione
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        changeTag rtb, "<RECTYPE-NAME>", rsSegmenti!Nome
      End If
      rsTabelle.Close
    End If
    rsSegmenti.Close
    rsOrigine.MoveNext
  Loop
  rsOrigine.Close
End Sub

Sub InserisciChiavePrimariaVSAM(rtb As RichTextBox, rsSegmenti As Recordset, rsTabelle As Recordset, originTable As Boolean)
  Dim NomeTb As String, rsAlias As Recordset
  
  Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
  If rsAlias.EOF Then
    NomeTb = rsTabelle!Nome
  Else
    NomeTb = rsAlias!Nome
  End If
  rsAlias.Close
                     
  If originTable Then
    changeTag rtb, "<WHEN-TABLE(i)>", "WHEN '" & rsSegmenti!Nome & "'" & vbCrLf _
                         & Space(4) & "PERFORM " & Replace(rsSegmenti!Nome, "_", "-") & "-TO-RR" & vbCrLf _
                         & Space(4) & "<OCCURS-MOVE(i)>" & vbCrLf _
                         & Space(4) & "ADD 1 TO CTR-" & rsSegmenti!Nome & vbCrLf _
                         & Space(4) & "WRITE  REC-F" & Replace(NomeTb, "_", "-") & vbCrLf _
                         & Space(4) & "   FROM  RR-" & Replace(rsTabelle!Nome, "_", "-") & vbCrLf _
                         & Space(4) & "<INIT-REVKEY(i)>"
  Else
    'virgilio gestione section
    changeTag rtb, "<WHEN-TABLE(i)>", "WHEN '" & rsSegmenti!Nome & "'" & vbCrLf _
                         & Space(4) & "PERFORM " & Replace(rsSegmenti!Nome, "_", "-") & "-TO-RR" & vbCrLf _
                         & Space(4) & "ADD 1 TO CTR-" & Replace(NomeTb, "_", "") & vbCrLf _
                         & Space(4) & "WRITE  REC-F" & Replace(NomeTb, "_", "") & vbCrLf _
                         & Space(4) & "   FROM  RR-" & Replace(rsTabelle!Nome, "_", "-") & vbCrLf _
                         & Space(4) & "<INIT-REVKEY(i)>"   'virgilio
  End If
  changeTag rtb, "<REC-TYPE-CONDITION(i)>", "MOVE '" & rsSegmenti!Nome & "' TO WK-TABLE"
End Sub

Public Function getCondizione_RedefinesVSAM(RtbFile As RichTextBox, Condizione As String) As String
  Dim rtbString As String
  
  On Error GoTo loadErr
   
  'Portare controllo fuori
  If Len(Condizione & "") Then
    'salvo:
    rtbString = RtbFile.text
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\redefines\" & Condizione
    getCondizione_RedefinesVSAM = RtbFile.text
    
    'ripristino
    RtbFile.text = rtbString
  End If
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\VSAM\standard\redefines\" & Condizione & """ not found."
      getCondizione_RedefinesVSAM = ""
    Case Else
      MsgBox ERR.Number & " - " & ERR.Description
      'resume
  End Select
End Function

''''''''''''''''''''''
' SILVIA 21-03-2008 : add structure from file
'''''''''''''
Public Sub AddStructure_From_List()
  Dim i As Long
  Dim dialogFileName As String
  Dim Riga() As String
  Dim objList() As String 'solo il nome � pericoloso: dovr� avere anche la directory
  Dim rs As Recordset
  Dim IdOggetto As Long, idArea As Long
  Dim strIns As String, strCpynf As String, strAreanf As String
  Dim strlog As String
  Dim strlog1 As String
  
  On Error GoTo ErrorHandler
  
  If m_fun.FnProcessRunning Then
    m_fun.Show_MsgBoxError "FB01I"
    Exit Sub
  End If
  
  ''''''''''''''''''''''''''''''''''''''''''''''
  'Gestione inserimento lista oggetti da file
  ''''''''''''''''''''''''''''''''''''''''''''''
  MadmdF_Start.ComD.ShowOpen

  dialogFileName = MadmdF_Start.ComD.fileName
  If Len(dialogFileName) > 0 Then
    objList = readFile(dialogFileName)
    Screen.MousePointer = vbHourglass
    m_fun.FnActiveWindowsBool = True
    m_fun.FnProcessRunning = True
    
    If MsgBox("Do you want to insert the list of " & UBound(objList) & " objects?", vbQuestion + vbYesNo, "Add Structures") = vbYes Then
      For i = 1 To UBound(objList)
        Riga() = Split(objList(i), " ")
        If UBound(Riga()) = 4 Then
          m_fun.FnActiveWindowsBool = False
          
          Set rs = m_fun.Open_Recordset("SELECT * FROM Bs_Oggetti WHERE tipo = 'CPY' AND Nome = '" & Trim(Riga(0)) & "'")
          If rs.RecordCount Then
            IdOggetto = rs!IdOggetto
          Else
            IdOggetto = 0
            strCpynf = strCpynf & Riga(0) & vbCrLf
          End If
          rs.Close
          
          Set rs = m_fun.Open_Recordset("SELECT * FROM PsData_Area WHERE idOggetto = " & IdOggetto & " AND Nome = '" & Trim(Riga(1)) & "'")
          If rs.RecordCount Then
            idArea = rs!idArea
          Else
            idArea = 0
            strAreanf = strAreanf & Riga(1) & vbCrLf
          End If
          rs.Close
          
          If IdOggetto > 0 And idArea > 0 Then
            SaveRT Trim(Riga(0)), Trim(Riga(1)), IdOggetto, idArea
            AddArea_fromlist IdOggetto, idArea, Trim(Riga(0))
          Else
            strlog = strlog & "WRONG LINE : " & objList(i) & vbCrLf
          End If
        Else
          strlog = strlog & "WRONG LINE : " & objList(i) & vbCrLf
        End If
      Next i
      If strCpynf <> "" Then
        strCpynf = "---COPYBOOK NOT FOUND---" & vbCrLf & _
                   strCpynf
      End If
      If strAreanf <> "" Then
        strAreanf = "---AREA NOT FOUND---" & vbCrLf & _
                    strAreanf
      End If
      If strIns <> "" Then
        strIns = "INSERT RELATION AREA - COPYBOOK: " & vbCrLf & _
                 strIns
      End If
      If strlog1 <> "" Then
        strlog1 = "Relation already exist from Area - Copybook" & vbCrLf & _
                  strlog1
      End If
    
      Screen.MousePointer = vbDefault
      m_fun.Show_ShowText strCpynf & vbCrLf & _
                          strAreanf & vbCrLf & _
                          strIns & vbCrLf & _
                          strlog1 & vbCrLf & _
                          strlog, "Add Structure Log"
      m_fun.FnActiveWindowsBool = True
      
      m_fun.FnActiveWindowsBool = False
      m_fun.FnProcessRunning = False
      
      Screen.MousePointer = vbDefault
    End If
  End If
  
  Exit Sub
ErrorHandler:
  Screen.MousePointer = vbDefault
  m_fun.FnProcessRunning = False
  m_fun.Show_MsgBoxError "RC0002", "Add Structures", "AddStructure_from_List", ERR.Number, ERR.Description, True
End Sub

Public Sub Refresh_Areas()
  Dim rs As Recordset
  Dim wScelta As Integer
  
  Set rs = m_fun.Open_Recordset("select count(*) from DMVSM_Data_Cmp")
  If rs.RecordCount Then
    wScelta = MsgBox("There are some areas associated to files. Do you want to delete all?", vbYesNoCancel, DataMan.DmNomeProdotto)
  End If
  rs.Close
  If wScelta <> vbCancel Then
    Screen.MousePointer = vbHourglass
    If wScelta = vbYes Then
      DataMan.DmConnection.Execute "delete * from DMVSM_Data_Cmp"
      DataMan.DmConnection.Execute "update DMVSM_Recordtype set idarea = 0, idoggetto = 0"
    End If
    Set rs = m_fun.Open_Recordset("select vsam, idoggetto, idarea from DMVSM_Tracciato where vsam <> ''")
    Do Until rs.EOF
      aggiorna_Data_Cmp rs!IdOggetto, rs!idArea, rs!VSAM & ""
      rs.MoveNext
    Loop
    rs.Close
    Screen.MousePointer = vbDefault
  End If
End Sub

Public Sub aggiorna_Data_Cmp(idObj As Long, idArea As Long, VName As String)
  Dim rsCmp As Recordset, rs As Recordset
  Dim i As Long
  
  Set rsCmp = m_fun.Open_Recordset("select * from DMVSM_Data_Cmp where " & _
                                   "vsam = '" & VName & "' and idoggetto = " & idObj & " and idarea = " & idArea)
  If rsCmp.RecordCount = 0 Then
    i = getId("idcmp", "DMVSM_Data_Cmp") - 1
    Set rs = m_fun.Open_Recordset("select * from PSData_Cmp where " & _
                                  "IdOggetto = " & idObj & " and IdArea = " & idArea & _
                                  " order by ordinale")
    Do Until rs.EOF
      i = i + 1
      'completare
      rsCmp.AddNew
      rsCmp!IdCmp = i
      rsCmp!VSAM = UCase(VName) & ""
      rsCmp!IdOggetto = rs!IdOggetto
      rsCmp!idArea = rs!idArea
      rsCmp!Ordinale = rs!Ordinale
      rsCmp!Nome = rs!Nome
      rsCmp!livello = rs!livello
      rsCmp!Tipo = rs!Tipo
      rsCmp!Posizione = rs!Posizione
      rsCmp!Byte = rs!Byte
      rsCmp!lunghezza = rs!lunghezza
      rsCmp!Decimali = rs!Decimali
      rsCmp!segno = rs!segno
      rsCmp!Usage = rs!Usage
      rsCmp!Occurs = rs!Occurs
      rsCmp!OccursFlat = False
      rsCmp!NomeRed = rs!NomeRed
      rsCmp!condizionered = ""
      rsCmp!DaMigrare = True

      rsCmp.Update
      rs.MoveNext
    Loop
    rs.Close
  End If
  rsCmp.Close
End Sub

Public Sub SaveRT(varVSAM As String, varRT As String, varIdOggetto As Long, varIdArea As Long)
  Dim tbRecordT As Recordset
  
  Set tbRecordT = m_fun.Open_Recordset("select * from dmvsm_recordtype where " & _
                                       "vsam = '" & varVSAM & "' and nome = '" & varRT & "'")
  If tbRecordT.RecordCount = 0 Then
    ' Se non c'� nessun Record Type, ne creo uno di default
    tbRecordT.AddNew
    tbRecordT!IdRecordType = getId("idRecordType", "DMVSM_RecordType")
  Else
    tbRecordT!VSAM = varVSAM & ""
    tbRecordT!Nome = varRT
    tbRecordT!Acronimo = varRT
    tbRecordT!idArea = varIdArea
    tbRecordT!IdOggetto = varIdOggetto
    tbRecordT!Condizione = ""
    tbRecordT.Update
  End If
  tbRecordT.Close
End Sub

Public Sub AddArea_fromlist(varIdOggetto As Long, varIdArea As Long, varVSAM As String)
  Dim rsAdd As Recordset, rs As Recordset, rsDMVSM As Recordset
  Dim i As Long
  
  Set rsAdd = m_fun.Open_Recordset("select * from dmvsm_tracciato where " & _
                                   "vsam = '" & varVSAM & "' and IdOggetto = " & varIdOggetto & " and IdArea = " & varIdArea)
  If rsAdd.RecordCount = 0 Then
    rsAdd.AddNew
    rsAdd!VSAM = varVSAM
    rsAdd!Ordinale = getId("Ordinale", "DMVSM_tracciato") ' � sbagliato!!!!!!!!!!!!!!!!!
    rsAdd!IdOggetto = varIdOggetto
    rsAdd!idArea = varIdArea
    rsAdd!Tag = "Added"
    rsAdd.Update
    
    ' Update DMVSM_Data_Cmp
    DataMan.DmConnection.Execute ("delete * from DMVSM_Data_Cmp where " & _
                                  "vsam = '" & varVSAM & "' and idoggetto = " & varIdOggetto & " and idarea = " & varIdArea)
    i = getId("idcmp", "DMVSM_Data_Cmp") - 1
    Set rs = m_fun.Open_Recordset("select * from PSData_Cmp where " & _
                                  "IdArea = " & varIdArea & " and Idoggetto = " & varIdOggetto)
    Set rsDMVSM = m_fun.Open_Recordset("select * from DMVSM_Data_Cmp")
    Do Until rs.EOF
      'completare
      i = i + 1
      rsDMVSM.AddNew
      rsDMVSM!IdCmp = i
      rsDMVSM!VSAM = rsAdd!VSAM
      rsDMVSM!IdOggetto = rs!IdOggetto
      rsDMVSM!idArea = rs!idArea
      rsDMVSM!Ordinale = rs!Ordinale
      rsDMVSM!Nome = rs!Nome
      rsDMVSM!livello = rs!livello
      rsDMVSM!Tipo = rs!Tipo
      rsDMVSM!Posizione = rs!Posizione
      rsDMVSM!Byte = rs!Byte
      rsDMVSM!lunghezza = rs!lunghezza
      rsDMVSM!Decimali = rs!Decimali
      rsDMVSM!segno = rs!segno
      rsDMVSM!Usage = rs!Usage
      rsDMVSM!Occurs = rs!Occurs
      rsDMVSM!OccursFlat = False
      rsDMVSM!NomeRed = rs!NomeRed
      rsDMVSM!condizionered = ""
      rsDMVSM!DaMigrare = True

      rsDMVSM.Update
      rs.MoveNext
    Loop
    rsDMVSM.Close
    rs.Close
  Else
    MsgBox "The record is already existing, the new record can not be saved", vbOKOnly + vbExclamation, DataMan.DmNomeProdotto
  End If
  rsAdd.Close
End Sub

Public Function readFile(dialogFileName As String) As String()
  Dim values() As String
  Dim FD As Long
  Dim Line As String
  
  ReDim values(0)
  
  FD = FreeFile
  Open dialogFileName For Input As FD
  Do Until EOF(FD)
    Line Input #FD, Line
    If Len(Trim(Line)) Then
      ReDim Preserve values(UBound(values) + 1)
      values(UBound(values)) = Trim(Line)
    Else
      'riga vuota: buttiamo via
    End If
  Loop
  Close FD
    
  readFile = values
End Function
