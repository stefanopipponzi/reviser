Attribute VB_Name = "MadmdM_DB2"
Option Explicit

'contengono i dati del tablespace,tabella e indici correnti (Selezionati)
Global CURIdCol As Long
'serve per sapere se sto andando in edit di un tablespace o i creazione
Global CreateTbSpc As Boolean
'serve per sapere se sto andando in edit di un tablespace o i creazione di una tabella
Global Edit_New_TbSpc As Boolean
'serve per sapere su quale lista sono :
'1) TB = Lista delle Tabelle
'2) TBSPC = Lista TableSpaces
Global TipoLista As String
'serve per tenere traccia dell'id selezionato del TBSpace
Global IdTbSpc As Long
Global IndexTbSpc As Long
'serve per tenere traccia della tabella che si � appena creata
Global GbIdTb As Long
Global GbTbNome As String
'serve a sapere se sono in modalit� NEW o EXISTINGOBJ nel modulo di Start
Global OpenMode As Long    '1=NEW ----- 2=EXIST
Global OpenScelta As Long  '1=Global Group ----- 2=Single DB Selected
Global TipoDB As String
Global EditTabMode As Boolean 'True : Sto editando la tabella -- False : Non sto editando la tabella
'indica se l'utente ha selezionato una tabella per editarla
Global Good As Boolean
'serve a memorizzare il vecchio nome della tabella
Global OldNomeTabella As String
'index della lista
Global IndexRiga As Long
'serve per definire lo stato di creazione delle colonne (progressivo)
Global SwCont As Boolean
'variabili per sapere se il tbspc, la tabella e il nome del DB sono stati inseriti
Global TbSpcOK As Boolean
Global TbOK As Boolean
Global DbOK As Boolean
'serve a tenere traccia del database corrente su cui si sta lavorando
Global NomeDatabaseCorrente As String
Global IDDatabaseCorrente As Long
'serve per sapere quale tabella ho selezionato sulla lista
Public IndexTabList As Double
Global SwColMod As Boolean
Dim columnComments() As String
'SQ - 10-07-06
Dim idSegmentoParent As Long, idTableParent As Long
Dim nomeSegmentoParent As String, nomeTableParent As String
Global cCopyColumnParam As String, KCopyParam As String, cCopyParam As String, cCopyAreaParam As String
Global LoadProgramParam As String, LoadCopyParam As String, MoveCopyParam As String
Global PkNameParam As String, JclLoadParam As String, JclTranParam As String
' Mauro 2014 Naming convention per Quadratura
Global JclQuadParam As String, QuadProgramParam As String, QuadPSBParam As String

Public Function withTot(Lsw As ListView) As Long
  Dim i As Long
  
  For i = 1 To Lsw.ColumnHeaders.Count - 1
    withTot = withTot + Lsw.ColumnHeaders(i).Width
  Next i
  
  If withTot >= Lsw.Width Then
    i = withTot - Lsw.Width
    withTot = withTot - i
  Else
    withTot = Lsw.Width - withTot
  End If
End Function

Public Function checkOriginColumn(Lsw As ListView) As ListView
  Dim i As Long
  Dim boolF As Boolean
  
  For i = 1 To Lsw.ColumnHeaders.Count
    If Lsw.ColumnHeaders(i).text = "Origin" Then
      boolF = True
      Exit For
    End If
  Next i
  
  If boolF Then
    Lsw.ColumnHeaders.Remove (i)
  End If
  Set checkOriginColumn = Lsw
End Function

Public Function get_NomeTable_ById(IdTable As Long) As String
  Dim r As Recordset
  
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Tabelle Where IdTable = " & IdTable)
  If r.RecordCount Then
    get_NomeTable_ById = r!Nome
  Else
    get_NomeTable_ById = ""
  End If
  r.Close
End Function

Public Sub CLsOn(Optional val As Boolean)
  If val Then
    Screen.MousePointer = vbHourglass
  Else
    Screen.MousePointer = vbDefault
  End If
End Sub

Public Function Get_Creator_Table(idTb As Long) As String
  Dim r As Recordset
  
  Set r = m_fun.Open_Recordset("Select * From DMDB2_Tabelle Where IdTable = " & idTb)
  If r.RecordCount Then
    If r.RecordCount = 1 Then 'deve essere solo un record
      Get_Creator_Table = Trim(r!Creator) & ""
    Else
      Stop
    End If
  Else
  '  Stop 'impossibile !!!
  End If
  r.Close
End Function

Public Sub GeneraCpyRR(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset
  Dim fileName As String
  Dim env As Collection
  Dim cont As String
  Dim k As Integer, idtablej As Integer
  k = 1
  idtablej = 0
  On Error GoTo errdb
  
  'PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  If dbType = "VSAM" Then
    Stop
  End If
  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
   
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB = " & pIndex_DBCorrente)
  While Not rsTabelle.EOF
    If idtablej <> rsTabelle!idTableJoin Or IsNull(rsTabelle!idTableJoin) Then
      k = 1
    End If

    If rsTabelle!idTableJoin <> 0 Then
      k = k + 1
      idtablej = rsTabelle!idTableJoin
    End If
    
    Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto, idsegmentoorigine from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto, idsegmentoorigine from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
      If rsSegmenti.RecordCount Then
        ' Postfisso per il nome dell'area RR
        cont = Right(rsSegmenti!Nome, 2)
      End If
    Else
      cont = ""
    End If
    If rsSegmenti.RecordCount > 0 Then
    
     'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
      '****************
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RR") <> "" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RR"
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RR"" not found."
        Exit Sub
      End If
     
'      RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RR"
    '*************************
    
      changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
      changeTag RtbFile, "<TABLE-NAME>", Replace(rsTabelle!Nome, "_", "-")
      changeTag RtbFile, "<SEGM-NAME>", Replace(rsSegmenti!Nome, "_", "-")
      ' Inserisco le colonne
      changeTag RtbFile, "<LISTA-CAMPI>", getListaCampi(rsTabelle!Nome, rsTabelle!IdTable)
      'getListaCampi rtbFile, rsTabelle
      
      'SEGMENTI VIRTUALI: non utilizziamo KRR/KRD
      'If rsSegmenti!IdSegmentoOrigine = 0 Then
      InserisciKey RtbFile, rsTabelle
      InserisciSecondaryKey RtbFile, rsTabelle
      'tilvia 2014 inserimento indicatori di null nella copy C invece che nella copy X
      InsertIndicatori RtbFile, rsTabelle
      'Else
      '   DelParola rtbFile, "<CHIAVI-PRIMARIE>"
      '   DelParola rtbFile, "<CHIAVI-SECONDARIE>"
      'End If
      DelParola RtbFile, "<LISTA-CAMPI-INDICATORE>"
      
      If Len(cCopyParam) Then
        Set env = New Collection  'resetto;
        Dim rsAlias As Recordset
        Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
        If rsAlias.RecordCount Then
          env.Add rsAlias
        Else
          env.Add rsTabelle
        End If
        env.Add rsSegmenti
        env.Add k
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(cCopyParam, "tabelle", env), 1
        rsAlias.Close
      Else
        MsgBox "No value found for 'C-COPY' environment parameter.", vbExclamation, DataMan.DmNomeProdotto
        Exit Sub
      End If
    Else
      MsgBox "No origin segment found for table " & rsTabelle!Nome, vbExclamation, DataMan.DmNomeProdotto
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  Exit Sub
errdb:
  Select Case ERR.Number
    Case 75
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RR") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RR"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Sub

Public Sub GeneraCpyRR_PLI(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset
  Dim fileName As String
  Dim env As Collection
  Dim cont As String
  Dim dataString As String
  Dim tabString As String
  Dim rs As Recordset
  
  On Error GoTo errdb
  'tilvia 04-05-2009
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
  pIndex_DBDCorrente = rs!iddbor
  rs.Close

  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Include\"
   
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB = " & pIndex_DBCorrente)
  While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto, idsegmentoorigine from PSDLI_Segmenti where " & _
                                          "IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto, idsegmentoorigine from MGDLI_Segmenti where " & _
                                            "IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      If rsSegmenti.RecordCount Then
        cont = Right(rsSegmenti!Nome, 2)
      End If
    Else
      cont = ""
    End If
    If rsSegmenti.RecordCount Then
       'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
      '****************
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RR") <> "" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RR"
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RR"" not found."
        Exit Sub
      End If
      
      'RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RR"
      '*****************
      
      'inserisce il nome formattato del db
      'dataString = "/*     TYPE: RDBMS          By DBD Name = " & pNome_DBCorrente
      'changeTag RtbFile, "<DBD-NAME>", dataString & Space(68 - Len(dataString)) & "*/"
      changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
      changeTag RtbFile, "<SEGM-NAME>", rsSegmenti!Nome
      
      'inserisce il nome formattato della tabella
      tabString = "/* C-COPY for Table: " & Replace(rsTabelle!Nome, "-", "_")
      changeTag RtbFile, "<TABLE-NAME>", tabString & Space(60 - Len(tabString)) & "*/"
      
      ' Inserisco le colonne
      changeTag RtbFile, "<LISTA-CAMPI>", getListaCampi_PLI(rsTabelle!Nome, rsTabelle!IdTable)
      
      InserisciKey_PLI RtbFile, rsTabelle, 1, cont
      InserisciSecondaryKey_PLI RtbFile, rsTabelle
      
      If Len(cCopyParam) Then
        Set env = New Collection  'resetto;
        env.Add rsTabelle
        env.Add rsSegmenti
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(cCopyParam, "tabelle", env), 1
      Else
        MsgBox "No value found for 'C-COPY' environment parameter.", vbExclamation, DataMan.DmNomeProdotto
        Exit Sub
      End If
'    Else
'      MsgBox "No origin segment found for table " & rsTabelle!Nome, vbExclamation, DataMan.DmNomeProdotto
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  Exit Sub
errdb:
  Select Case ERR.Number
    Case 75
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RR") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RR"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Sub

Public Sub generaCpyX(RtbFile As RichTextBox, Optional pgtype = "")
  'alpitour 5/8/2005: cambiata praticamente tutta
  Dim rsTabelle As Recordset, rsSegmenti As Recordset
  Dim Creator As String, fileName As String
  Dim wIdDB As Double
  Dim rs As Recordset
    
  Screen.MousePointer = vbHourglass
  
  'tilvia 04-05-2009
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
  pIndex_DBDCorrente = rs!iddbor
  rs.Close

  RtbFile.text = ""
  DataMan.DmParam_PercCpyExec = DataMan.DmPathDb & "\input-prj\Template\imsdb\standard\fields\" & IIf(pgtype = "PLI", "PLI\", "") & dbType & "\CPY_BASE_EXEC"
  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & IIf(pgtype = "PLI", "\Include\", "\Cpy\")

  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & wIdDB)
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & pIndex_DBCorrente)
  On Error Resume Next
  While Not rsTabelle.EOF
    'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
    '****************
    If Dir(DataMan.DmParam_PercCpyExec) <> "" Then
      RtbFile.LoadFile DataMan.DmParam_PercCpyExec
    Else
      DataMan.DmFinestra.ListItems.Add , , Now & " " & DataMan.DmParam_PercCpyExec
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
    'RtbFile.LoadFile DataMan.DmParam_PercCpyExec

    '*****************
       
    If Trim(rsTabelle!Creator) <> "" Then
      Creator = Trim(rsTabelle!Creator) 'Controllare se esiste un alias!!!!!!!!
    Else
      Creator = Trim(LeggiParam("DM_CREATOR"))
      'virgilio inizio: definire dove e come usare blank/s sul creator
      If Trim(Creator) <> "" Then
        Creator = UCase(Trim(Creator))
      End If
''''      If Len(Creator) = 0 Then
''''        Creator = "%CREATOR%"
''''      ElseIf Creator = "BLANK" Or Creator = "BLANKS" Then
''''        Creator = ""
''''      End If
      'virgilio fine:
    End If
      
    ' Inserisco il nome del database
    InsertDbName RtbFile, rsTabelle
    InsertParola RtbFile, "<DB2NAME>", pNome_DBCorrente

    If TipoDB <> "VSAM" Then
      ' Inserisco il nome della tabella
      InsertTableNameDeclare RtbFile, rsTabelle, Creator
      ' Inserisco i campi e le caratteristiche
      InsertDichiarativa RtbFile, rsTabelle
      'Inserisco gli indicatori
      'tilvia 2014 inserisco gli indicatori di null nella copy C
      'InsertIndicatori RtbFile, rsTabelle
    End If

    'NomeFile = "EXEC" & Format(TbTable!IdSegm, "000")
    'alpitour: 5/8/2005 nome copy X
    DelParola RtbFile, "<LISTA-CAMPI-INDICATORE>"
    
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    End If
    If rsSegmenti.RecordCount Then
      fileName = LeggiParam("X-COPY")
      Dim env As Collection
      If Len(fileName) Then
        Set env = New Collection  'resetto;
        env.Add rsTabelle
        env.Add rsSegmenti
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(fileName, "tabelle", env), 1
      Else
        MsgBox "No value found for 'X-COPY' environment parameter.", vbExclamation, DataMan.DmNomeProdotto
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
    End If
   
    rsTabelle.MoveNext
  Wend
  Screen.MousePointer = vbDefault
End Sub

Public Sub GeneraTabOrg()
  Dim TbTable As Recordset
  Dim TbIndex As Recordset
  Dim Creator As String
  Dim RecSeg As Recordset
  Dim NomeFile As String
  Dim NumFile As Variant
  Dim NumTab As Integer
  Dim NumElem As Integer
  Dim wIdDB As Double
  Dim wRec As String
  
  NumFile = FreeFile
  
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
  NomeFile = DataMan.DmParam_OutField_Db & "VTABORG.CPY"
  
'''  If TipoDB = "DB2" Then
'''     PrefDb = "DMDB2"
'''     DataMan.DmParam_OutField_Db2 = DataMan.DmPathDb & "\OutPut-Prj\DB2\Cpy\"
'''  End If
'''  If TipoDB = "ORACLE" Then
'''     PrefDb = "DMORC"
'''     DataMan.DmParam_OutField_Db2 = DataMan.DmPathDb & "\OutPut-Prj\ORA\Cpy\"
'''  End If
'''  NomeFile = DataMan.DmParam_OutField_Db2 & "VTABORG.CPY"

  Open NomeFile For Output As #NumFile

  wRec = Space(7) & "01 TABORG. "
  Print #NumFile, wRec

  NumElem = 0
  NumTab = 0

  Set TbTable = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle order by idtable")

  If TbTable.RecordCount > 0 Then
    TbTable.MoveFirst

    While TbTable.EOF = False
      If TbTable!IdOrigine > 0 Then

         Print #NumFile, " "

         NumTab = NumTab + 1
         NumElem = NumElem + 1
         wRec = Space(11) & "05  FILLER          PIC X(18) VALUE '" & Trim(TbTable!Nome) & "'."
         Print #NumFile, wRec
         wRec = Space(11) & "05  FILLER          PIC 9(4) VALUE " & NumTab & "."
         Print #NumFile, wRec
         wRec = Space(11) & "05  FILLER          PIC 9(2) VALUE " & CercaNumLiv(TbTable!IdOrigine) & "."
         Print #NumFile, wRec
         wRec = Space(11) & "05  FILLER          PIC X    VALUE 'C'."
         Print #NumFile, wRec
         wRec = Space(11) & "05  FILLER          PIC 9(4) VALUE " & CercaLenPk(TbTable!IdTable) & "."
         Print #NumFile, wRec
         Set TbIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where idtable = " & TbTable!IdTable & " and tipo <> 'P' ")
         If TbIndex.RecordCount > 0 Then
            TbIndex.MoveFirst
            While Not TbIndex.EOF

                Print #NumFile, " "
                NumElem = NumElem + 1
                wRec = Space(11) & "05  FILLER          PIC X(18) VALUE '" & Trim(TbIndex!Nome) & "'."
                Print #NumFile, wRec
                wRec = Space(11) & "05  FILLER          PIC 9(4) VALUE " & NumTab & "."
                Print #NumFile, wRec
                wRec = Space(11) & "05  FILLER          PIC 9(2) VALUE 0."
                Print #NumFile, wRec
                wRec = Space(11) & "05  FILLER          PIC X    VALUE 'A'."
                Print #NumFile, wRec
                wRec = Space(11) & "05  FILLER          PIC 9(4) VALUE 0."
                Print #NumFile, wRec
                TbIndex.MoveNext
            Wend
         End If
      End If
      TbTable.MoveNext
    Wend
  End If

  wRec = Space(7) & "01 TABORG-RED REDEFINES TABORG. "
  Print #NumFile, wRec
  wRec = Space(11) & "05 LNKDB-TABDEF      OCCURS " & NumElem & "."
  Print #NumFile, wRec
  wRec = Space(13) & "10 LNKDB-TABNAME         PIC X(18)."
  Print #NumFile, wRec
  wRec = Space(13) & "10 LNKDB-TABNUM          PIC 9(4)."
  Print #NumFile, wRec
  wRec = Space(13) & "10 LNKDB-TABLIV          PIC 9(2)."
  Print #NumFile, wRec
  wRec = Space(13) & "10 LNKDB-TABORG          PIC X."
  Print #NumFile, wRec
  wRec = Space(13) & "10 LNKDB-TABKEYLEN       PIC 9(4)."
  Print #NumFile, wRec
  Print #NumFile, " "

  wRec = Space(7) & "01 IND-TABORG                  PIC 9(4)."
  Print #NumFile, wRec
  wRec = Space(7) & "01 MAX-TABORG                  PIC 9(4)  VALUE " & NumElem & "."
  Print #NumFile, wRec


  Close #NumFile
End Sub

Function CercaLenPk(wIdTable) As Integer
  Dim wLenKey As Integer, wIdIndex As Long
  Dim Tb1 As Recordset, Tb2 As Recordset
  
  wLenKey = 0
  Set Tb1 = m_fun.Open_Recordset("select * from dmdb2_index where " & _
                                 "idtable = " & wIdTable & " and tipo = 'P'")
  If Tb1.RecordCount Then
    Set Tb2 = m_fun.Open_Recordset("select * from dmdb2_idxcol where " & _
                                   "idindex = " & Tb1!IdIndex & " and idtable = " & wIdTable & " order by ordinale")
    While Not Tb2.EOF
      Set Tb1 = m_fun.Open_Recordset("select * from dmdb2_colonne where idcolonna = " & Tb2!IdColonna)
      If Tb1!Tipo = "DECIMAL" Then
        wLenKey = wLenKey + Int((Tb1!lunghezza + IIf(IsNull(Tb1!Decimali), 0, Tb1!Decimali)) / 2) + 1
      Else
        wLenKey = wLenKey + Tb1!lunghezza + IIf(IsNull(Tb1!Decimali), 0, Tb1!Decimali)
      End If
      Tb2.MoveNext
    Wend
    Tb2.Close
  End If
  Tb1.Close
  
  CercaLenPk = wLenKey
End Function

Public Function CercaNumLiv(wIdTable As Long) As Integer
  Dim wNumLiv As Integer
  Dim wChild As String
  Dim Tb1 As Recordset
  Dim wIdOgg As Long
  
  wNumLiv = 0
  Set Tb1 = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & wIdTable)
  If Tb1.RecordCount Then
    wChild = Tb1!Nome
    wIdOgg = Tb1!IdOggetto
  Else
    Tb1.Close
    Set Tb1 = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & Int(val(wIdTable) / 100))
    wChild = Tb1!Nome
    wIdOgg = Tb1!IdOggetto
    'Stop
  End If
  Tb1.Close
  
  While Trim(wChild) <> "0"
    wNumLiv = wNumLiv + 1
    Set Tb1 = m_fun.Open_Recordset("select * from psrel_dliseg where idoggetto = " & wIdOgg & " and child = '" & wChild & "' ")
    If Tb1.RecordCount Then
      wChild = Tb1!Father
    Else
      Stop
    End If
    Tb1.Close
  Wend
  
  CercaNumLiv = wNumLiv
End Function

' Mauro : Non serve a niente e non viene pi� utilizzata
''Sub GeneraJclLoad(rtbFile As RichTextBox)
''  Dim rsTabelle As Recordset, rsSegmenti As Recordset
''  Dim wLen As Long, maxLenSeg As Long
''  Dim fileName As String
''  Dim rsAlias As Recordset
''  Dim NomeTb As String
''
''  Dim env As Collection
''
''  On Error GoTo errLoad
''
''  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & pIndex_DBCorrente)
''  While Not rsTabelle.EOF
''    Set rsAlias = m_fun.Open_Recordset("select nome from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
''    If rsAlias.EOF Then
''      NomeTb = rsTabelle!Nome
''    Else
''      NomeTb = rsAlias!Nome
''    End If
''    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
''    If rsSegmenti.RecordCount = 0 Then
''      Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
''    End If
''    If rsSegmenti.RecordCount > 0 Then
''      'Carica il template
''      'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
''      '****************
''      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_LOAD") <> "" Then
''        rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_LOAD"
''      Else
''        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_LOAD"" not found."
''        Exit Sub
''      End If
''
'''      RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_LOAD"
''      '***********
''
''      'tmp:
''      ''''''''changeTag rtbFile, "<ALIAS>", Left(rsTabelle!Nome, 2) & "N" & Mid(rsTabelle!Nome, 10)
''
''      'changeTag rtbFile, "<TABLE-NAME>", Replace(rsTabelle!Nome, "_", "")
''      changeTag rtbFile, "<TABLE-NAME>", Replace(NomeTb, "_", "")
''
''      Set env = New Collection  'resetto;
''      env.Add NomeTb
''      env.Add rsSegmenti
'''      If TipoDB = "DB2" Then
''        rtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\" & dbType & "\Jcl\" & getEnvironmentParam(JclLoadParam, "tabelle", env), 1
'''      ElseIf TipoDB = "ORACLE" Then
'''        rtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\ORA\Jcl\" & getEnvironmentParam(fileName, "tabelle", env), 1
'''      End If
''    Else
''      '
''    End If
''    rsTabelle.MoveNext
''
''  Wend
''
''  Exit Sub
''errLoad:
''  Select Case ERR.Number
''    Case 75
'''      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_LOAD"" not found."
''    Case Else
''      MsgBox ERR.Description
''      'Resume
''  End Select
''End Sub

Sub GeneraJclTran(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset
  Dim maxLenSeg As Long
  Dim fileName As String, Stringa As String
  Dim env As Collection
  Dim rsAlias As Recordset
  Dim NomeTb As String

  On Error GoTo errLoad

    'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
  '****************
  If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN") <> "" Then
     RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN"
  Else
     DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN"" not found."
     Exit Sub
  End If

  'TEMPLATE:
  'RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN"
  '********************
  'TILVIA 04-05-2009
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & pIndex_DBCorrente)
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "idDB IN (select IdDB from " & PrefDb & "_DB where " & _
                                       "idDBOr = " & pIndex_DBDCorrente & ") ")

  While Not rsTabelle.EOF
    Set rsAlias = m_fun.Open_Recordset("select nome from " & PrefDb & "_Alias where idtable = " & rsTabelle!IdTable)
    If rsAlias.EOF Then
      NomeTb = rsTabelle!Nome
    Else
      NomeTb = rsAlias!Nome
    End If
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    End If
    If rsSegmenti.RecordCount > 0 Then
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Segmento di lunghezza max. Serve per l'unica area host di lettura
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If rsSegmenti!MaxLen > maxLenSeg Then
        maxLenSeg = rsSegmenti!MaxLen
      End If
      
      InsertParola RtbFile, "<<LOAD_JCL_DD(i)>>", getLoadJclDD(RtbFile, rsTabelle, rsSegmenti) & vbCrLf & "<<LOAD_JCL_DD(i)>>"
      'SQ
      'InsertParola RtbFile, "<LOAD_JCL_DEL(i)>", getLoadJclDEL(RtbFile, rsTabelle, rsSegmenti) & vbCrLf & "<LOAD_JCL_DEL(i)>"
      changeTag RtbFile, "<<LOAD_JCL_DEL(i)>>", getLoadJclDEL(RtbFile, rsTabelle, rsSegmenti)
      
      InsertParola RtbFile, "<<LOAD_JCL_CTL(i)>>", getLoadJclCTL(RtbFile, rsTabelle, rsSegmenti) & vbCrLf & "<<LOAD_JCL_CTL(i)>>"
      
      Stringa = Space(7) & "SCRATCH  DSNAME=<DBD-NAME>." & Replace(NomeTb, "_", "") & ",VOL=3390=<VOL-SER>"
      
      InsertParola RtbFile, "<LISTA-SCRATCH>", Stringa & vbCrLf & "<LISTA-SCRATCH>"
  
      'changeTag rtbFile, "<ALIAS>", Left(RsTabelle!Nome, 2) & "N" & Mid(RsTabelle!Nome, 10)
      changeTag RtbFile, "<TABLE-NAME>", Replace(NomeTb, "_", "-")
    Else
      '
    End If
    rsTabelle.MoveNext
  Wend
    
  changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
  changeTag RtbFile, "<LISTA-SCRATCH>", "//*"
  changeTag RtbFile, "<VOL-SER>", "AL3ADA"
  changeTag RtbFile, "<REC-SEQDBDLI-LEN>", maxLenSeg & ""
  changeTag RtbFile, vbCrLf & "<<LOAD_JCL_DD(i)>>", ""
  changeTag RtbFile, vbCrLf & "<<LOAD_JCL_CTL(i)>>", ""
  changeTag RtbFile, vbCrLf & "<<LOAD_JCL_DEL(i)>>", ""
    
  'DCP name:
  Set env = New Collection  'resetto;
  env.Add NomeTb
  env.Add rsSegmenti
  fileName = getEnvironmentParam(LoadProgramParam, "tabelle", env)
  changeTag RtbFile, "<PGM-NAME>", fileName
  
  Set env = New Collection
  'SQ 22-11-07
  'env.Add pNome_DBCorrente
  env.Add NomeTb
  env.Add rsSegmenti
  'fileName = getEnvironmentParam(fileName, "dbd", env)
  fileName = getEnvironmentParam(JclTranParam, "tabelle", env)
  changeTag RtbFile, "<JOB-N>", fileName
  
  cleanTags RtbFile
  
  RtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\" & dbType & "\Jcl\" & fileName, 1

  Exit Sub
errLoad:
  Select Case ERR.Number
    Case 75
'      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN"" not found."
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Sub

Sub GeneraJclLoadUrgio(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset
  Dim maxLenSeg As Long
  Dim fileName As String, Stringa As String
  Dim env As Collection
  Dim rsAlias As Recordset, rsRoute As Recordset
  Dim NomeTb As String
  Dim progr As Integer
  
  On Error GoTo errLoad
  
  progr = 1
  'TEMPLATE:
  'RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN"
  '********************
  'TILVIA 04-05-2009
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & pIndex_DBCorrente)
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "idDB IN (select IdDB from " & PrefDb & "_DB where " & _
                                       "idDBOr = " & pIndex_DBDCorrente & ") ")

  While Not rsTabelle.EOF
    Set rsAlias = m_fun.Open_Recordset("select nome from " & PrefDb & "_Alias where idtable = " & rsTabelle!IdTable)
    If rsAlias.EOF Then
      NomeTb = rsTabelle!Nome
    Else
      NomeTb = rsAlias!Nome
    End If
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    End If
    If rsSegmenti.RecordCount > 0 Then
      'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
      '****************
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_LOAD_NEW") <> "" Then
         RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_LOAD_NEW"
      Else
         DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_LOAD_NEW"" not found."
         Exit Sub
      End If
      Set rsRoute = m_fun.Open_Recordset("select a.route_name from psdli_dbd as a, bs_oggetti as b where a.dbd_name = b.nome and b.idoggetto = " & pIndex_DBDCorrente)
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Segmento di lunghezza max. Serve per l'unica area host di lettura
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      
      changeTag RtbFile, "<STEPNAME>", "STP" & Mid(rsSegmenti!Nome, 4)
      changeTag RtbFile, "<UTLNAME>", "UNLTHA" & progr
      changeTag RtbFile, "<DBNAME>", pNome_DBCorrente
      changeTag RtbFile, "<TBNAME>", NomeTb
      changeTag RtbFile, "<CREATOR>", rsTabelle!Creator
      changeTag RtbFile, "<LOADNAME>", "LOA" & Mid(rsSegmenti!Nome, 4)
      fileName = "JC" & rsRoute!route_name & Mid(rsSegmenti!Nome, 4)
      changeTag RtbFile, "<JCLNAME>", fileName
      changeTag RtbFile, "<SEGNAME>", Mid(rsSegmenti!Nome, 4)
      cleanTags RtbFile
      
      RtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\" & dbType & "\Jcl\" & fileName, 1
      progr = progr + 1
      rsRoute.Close
    Else
      '
    End If
    rsTabelle.MoveNext
  Wend
    
  

  Exit Sub
errLoad:
  Select Case ERR.Number
    Case 75
'      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN"" not found."
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Sub

Sub GeneraJclTran_PLI(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset
  Dim maxLenSeg As Long
  Dim fileName As String, Stringa As String
  Dim env As Collection

  On Error GoTo errLoad

  'Carica il template

    'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
  '****************
  If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN") <> "" Then
     RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN"
  Else
     DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN"" not found."
     Exit Sub
  End If
   
  'RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN"
  '*****************
  
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & pIndex_DBCorrente)
  While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    End If
    If rsSegmenti.RecordCount > 0 Then
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Segmento di lunghezza max. Serve per l'unica area host di lettura
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If rsSegmenti!MaxLen > maxLenSeg Then
        maxLenSeg = rsSegmenti!MaxLen
      End If
      
      InsertParola RtbFile, "<LOAD_JCL_DD(i)>", getLoadJclDD(RtbFile, rsTabelle, rsSegmenti) & vbCrLf & "<LOAD_JCL_DD(i)>"
    
      InsertParola RtbFile, "<LOAD_JCL_DEL(i)>", getLoadJclDEL(RtbFile, rsTabelle, rsSegmenti) & vbCrLf & "<LOAD_JCL_DEL(i)>"
      
      InsertParola RtbFile, "<LOAD_JCL_CTL(i)>", getLoadJclCTL(RtbFile, rsTabelle, rsSegmenti) & vbCrLf & "<LOAD_JCL_CTL(i)>"
    
      Stringa = Space(7) & "SCRATCH  DSNAME=<DBD-NAME>." & Replace(rsTabelle!Nome, "_", "") & ",VOL=3390=<VOL-SER>"
      
      InsertParola RtbFile, "<LISTA-SCRATCH>", Stringa & vbCrLf & "<LISTA-SCRATCH>"
  
      'changeTag rtbFile, "<ALIAS>", Left(RsTabelle!Nome, 2) & "N" & Mid(RsTabelle!Nome, 10)
      changeTag RtbFile, "<TABLE-NAME>", Replace(rsTabelle!Nome, "_", "-")
    Else
      '
    End If
    rsTabelle.MoveNext
  Wend
    
  changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
  changeTag RtbFile, "<LISTA-SCRATCH>", "//*"
  changeTag RtbFile, "<VOL-SER>", "AL3ADA"
  changeTag RtbFile, "<REC-SEQDBDLI-LEN>", maxLenSeg & ""
  changeTag RtbFile, vbCrLf & "<LOAD_JCL_DD(i)>", ""
  changeTag RtbFile, vbCrLf & "<LOAD_JCL_CTL(i)>", ""
  changeTag RtbFile, vbCrLf & "<LOAD_JCL_DEL(i)>", ""
    
  'DCP name:
  Set env = New Collection  'resetto;
  env.Add rsTabelle
  env.Add rsSegmenti
  fileName = getEnvironmentParam(LoadProgramParam, "tabelle", env)
  changeTag RtbFile, "<PGM-NAME>", fileName
  
  Set env = New Collection
  'SQ 22-11-07
  'env.Add pNome_DBCorrente
  env.Add rsTabelle
  env.Add rsSegmenti
  'fileName = getEnvironmentParam(fileName, "dbd", env)
  fileName = getEnvironmentParam(JclTranParam, "tabelle", env)
  changeTag RtbFile, "<JOB-N>", fileName
  
  
  RtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\" & dbType & "\Jcl\" & fileName, 1

  Exit Sub
errLoad:
  Select Case ERR.Number
    Case 75
'      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\JCL_TRAN"" not found."
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Sub

Sub GeneraCtlCopy(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset
  Dim fileName As String
  Dim env As Collection

  On Error GoTo errLoad
  
  fileName = LeggiParam("D-COPY")
  If Len(fileName) = 0 Then
    MsgBox "tmp: problemi sul paramentro CTL-FILE!!!!!!!!", vbExclamation
    Exit Sub
  End If
  
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & pIndex_DBCorrente)
  While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    End If
    If rsSegmenti.RecordCount > 0 Then
      'Carica il template
       'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
      '****************
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CTL_COPY") <> "" Then
         RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CTL_COPY"
      Else
         DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CTL_COPY"" not found."
         Exit Sub
      End If
      
      'RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CTL_COPY"
      '*****************
      
      'SP corso
      changeTag RtbFile, "<CTL-COPY-COLUMN(i)>", getCtlCopyColumn(RtbFile, rsTabelle)
      'changeTag rtbFile, "<CTL-COPY-COLUMN>", getCtlCopyColumn(rtbFile, RsTabelle)
      
      changeTag RtbFile, "<NOME-SEGM>", rsSegmenti!Nome
      
      ''''''''''''''''''''''''''''''''''''''''
      ' NOME CTL:
      ''''''''''''''''''''''''''''''''''''''''
      Set env = New Collection  'resetto;
      env.Add rsTabelle
      env.Add rsSegmenti
      RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(fileName, "tabelle", env), 1
    Else
      '
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  
  Exit Sub
errLoad:
  Select Case ERR.Number
    Case 75
'      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CTL_COPY"" not found."
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Sub

'SQ
Public Sub Declare_Table(RtbFile As RichTextBox)
  Dim TbTable As Recordset
  Dim Creator As String
  Dim wDbName As String
  Dim wNomeStg As String
  Dim RecSeg As Recordset
  Dim Tb1 As Recordset
  
  On Error GoTo errors
  RtbFile.text = ""

  Select Case TipoDB
   Case "DB2"
    DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\Db2\Sql\"
    'DataMan.DmParam_PercSQLExec = DataMan.DmPathPrd & "\System\CPY_BASE\DEC_BASE_EXEC.sql"
    'SQ: attenzione: obbligatorio tablespace!!!!!!!!!!!!!!!!!
    'Set TbTable = m_fun.Open_Recordset("select * from DMDB2_DB,DMDB2_Tabelle,DMDB2_tableSpaces where DMDB2_Tabelle.idDB=DMDB2_DB.idDB AND DMDB2_tableSpaces.IdTableSpc=DMDB2_Tabelle.IdTableSpc AND DMDB2_DB.nome = '" & pNome_DBCorrente & "'")
    Set TbTable = m_fun.Open_Recordset("select * from DMDB2_DB,DMDB2_Tabelle where DMDB2_Tabelle.idDB=DMDB2_DB.idDB AND DMDB2_DB.nome = '" & pNome_DBCorrente & "'")
   Case "ORACLE"
    Stop  '???????????????????????
   Case "VSAM"
    Stop
  End Select

  While Not TbTable.EOF
      ''' VIRGILIO 12/5/2006
    ''Creator = IIf(IsNull(TbTable!Creator), "%CREATOR%", TbTable!Creator)
    ''rtbFile.LoadFile DataMan.DmParam_PercSQLExec
    'Creator = IIf(IsNull(TbTable!Creator), "<creator>", TbTable!Creator)
    'rtbFile.LoadFile m_fun.FnPathDB & "\Input-Prj\CPY_BASE\DMDB2\DEC_BASE_EXEC.sql"
    
    Creator = IIf(IsNull(TbTable!Creator), "%CREATOR%", TbTable!Creator)
    
    RtbFile.LoadFile DataMan.DmParam_PercSQLExec

    changeTag RtbFile, "<CRT-NAME>", Creator 'Controllare anche sugli alias
    
    changeTag RtbFile, "<NOME-TABELLA>", TbTable(9)
    
    'LUNGHEZZA TABELLA
    'fare...
    
    'TABLESPACE:
    If TbTable(11) > 0 Then
      'TBS associato; cerco il nome
      Set Tb1 = m_fun.Open_Recordset("select nome from DMDB2_TableSpaces where idTableSpc = " & TbTable(11))
      If Tb1.RecordCount Then
        changeTag RtbFile, "<TBS-NAME>", Tb1!Nome
        'ALIAS - il nome lo prendo, per far prima, da quello del tbs...
        'changeTag rtbFile, "<ALIAS>", Left(Tb1!Nome, 6) 'Left(TbTable(9), 2) & "N" & Mid(TbTable(9), 10)
      'Else
      '  MsgBox "TMP; idTableSpace not found: " & TbTable(11)
      End If
    Else
      'no TBS associati
    End If
      
    changeTag RtbFile, "<DB-NAME>", pNome_DBCorrente
    changeTag RtbFile, "<STG-NAME>", pNome_DBCorrente & "STG"

    InsertDichiarativa RtbFile, TbTable
    
    ''' VIRGILIO 12/5/2006
    'changeTag rtbFile, "<COMMENTI-COLONNE>", getCommentiColonne(TbTable!IdTable, "<TB-DECLARE>")
    
                                                        'TIPO CHIAVE? UN PELO PIU' LEGGIBILE?
'        InserisciKeyDef rtbFile, TbTable(8), TbTable(9), 1, Creator, pNome_DBCorrente, pNome_DBCorrente & "STG"
'        InserisciKeyDef rtbFile, TbTable(8), TbTable(9), 2, Creator, pNome_DBCorrente, pNome_DBCorrente & "STG"
'        InserisciKeyDef rtbFile, TbTable(8), TbTable(9), 3, Creator, pNome_DBCorrente, pNome_DBCorrente & "STG"
    'effetto collaterale: mi riempiono la alterTableStatements
    'InserisciKeyDef_Fase2 rtbFile, TbTable(8), TbTable(9), 1, Creator, pNome_DBCorrente, pNome_DBCorrente, alterTableStatements
    'InserisciKeyDef_Fase2 rtbFile, TbTable(8), TbTable(9), 3, Creator, pNome_DBCorrente, pNome_DBCorrente, alterTableStatements
    
        ''' VIRGILIO 12/5/2006
    'InserisciKeyDef_Fase2 rtbFile, TbTable(8), TbTable(9), 1, Creator, pNome_DBCorrente, pNome_DBCorrente, ""
    'InserisciKeyDef_Fase2 rtbFile, TbTable(8), TbTable(9), 3, Creator, pNome_DBCorrente, pNome_DBCorrente, ""
    
    changeTag RtbFile, "<TB-DECLARE>", TbTable(9)  'alla fine... quelli sopra possono introdurre tag...
    
    RtbFile.SaveFile DataMan.DmParam_OutField_Db & TbTable(9) & ".sql", 1

    TbTable.MoveNext
  Wend
  
  Exit Sub
errors:
  'gestire:
  MsgBox ERR.Description
  'Resume
End Sub

Public Sub createDDL_BPERmodel(RtbFile As RichTextBox)
  Dim i As Integer, nfile As Integer
  Dim ddlFile As Variant
  Dim TbTable As Recordset, Tb1 As Recordset
  Dim alterTableStatements As String
  Dim Creator As String

  On Error GoTo errors
  
  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\Db2\Sql\"
  ddlFile = FreeFile
  Open DataMan.DmParam_OutField_Db & pNome_DBCorrente For Output As ddlFile
  
  ''''''''''''''''''''''''''''''''''''''
  ' HEADER DBD:
  ''''''''''''''''''''''''''''''''''''''
  Print #ddlFile, "-- #DB:" & pNome_DBCorrente & vbCrLf
  
  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\Db2\Sql\"
  
  Set TbTable = m_fun.Open_Recordset("select * from DMDB2_DB as a,DMDB2_Tabelle as b where " & _
                                     "b.idDB = a.idDB AND a.nome = '" & pNome_DBCorrente & "' ORDER BY b.IdTable")
  
  alterTableStatements = ""
  For i = 0 To TbTable.RecordCount - 1
    RtbFile.text = ""
    Creator = IIf(IsNull(TbTable!Creator), "%CREATOR%", TbTable!Creator)
    
    'rtbFile.LoadFile m_fun.FnPathDB & "\Input-Prj\CPY_BASE\" & DbType & "\DEC_BASE_EXEC.sql"
    ' Mauro 08/02/2008 : Nuova gestione per creazione DDL con Template
    'RtbFile.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\CREATE_TABLE"
    RtbFile.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\DDL"
    
    changeTag RtbFile, "<CRT-NAME>", Creator 'Controllare anche sugli alias
    
    changeTag RtbFile, "<NOME-TABELLA>", TbTable.fields("b.Nome")  'TbTable(9)
    
    'LUNGHEZZA TABELLA
    changeTag RtbFile, "<TB-LEN>", getDclgenLen_BPER(TbTable!IdTable)
    
    'TABLESPACE:
    If TbTable!IdTableSpc > 0 Then
      'TBS associato; cerco il nome
      Set Tb1 = m_fun.Open_Recordset("select * from DMDB2_TableSpaces where idTableSpc = " & TbTable!IdTableSpc)
      If Tb1.RecordCount Then
        changeTag RtbFile, "<TBS-NAME>", Tb1!Nome
        NomeTBS = Tb1!Nome
        'changeTag RtbFile, "<ALIAS>", Left(Tb1!Nome, 6) 'Left(TbTable.fields("DMDB2_Tabelle.Nome"), 2) & "N" & Mid(TbTable.fields("DMDB2_Tabelle.Nome"), 10)
        ' Mauro 13-04-2007 : Gestione Storage Group e BufferPool
        changeTag RtbFile, "<stogroup>", IIf(IsNull(Tb1!StorageGroup), "", Tb1!StorageGroup)
        changeTag RtbFile, "<bufferpool>", IIf(IsNull(Tb1!BufferPool), "", Tb1!BufferPool)
      'Else
      '  MsgBox "TMP; idTableSpace not found: " & TbTable!IdTableSpc
      End If
    Else
      'no TBS associati
    End If
    
    changeTag RtbFile, "<DB-NAME>", pNome_DBCorrente
    changeTag RtbFile, "<STG-NAME>", pNome_DBCorrente & "STG"

    InsertDichiarativa RtbFile, TbTable
    
    'changeTag rtbFile, "<COMMENTI-COLONNE>", getCommentiColonne_BPER
    'VIRGILIO 12/5/2006
    changeTag RtbFile, "<COMMENTI-COLONNE>", getCommentiColonne(TbTable!IdTable, TbTable.fields("b.nome"), Creator)
    'Mauro : 16/10/2009
    changeTag RtbFile, "<COMMENTI-TABELLA>", "COMMENT ON TABLE " & IIf(Len(Creator), Creator & ".", "") & TbTable.fields("b.nome") & vbCrLf & _
                                             Space(8) & "IS '" & TbTable!Commento & "';" & vbCrLf & vbCrLf
    
    ' CHIAVI PRIMARIE
    InserisciPrimKeyDef RtbFile, TbTable!IdTable, TbTable.fields("b.Nome"), Creator
    ' INDICI SECONDARI
    InserisciSecIdxDef RtbFile, TbTable!IdTable, TbTable.fields("b.Nome"), Creator
    ' FOREIGN KEY
    InserisciForKeyDef TbTable!IdTable, TbTable.fields("b.Nome"), Creator, alterTableStatements
    
    changeTag RtbFile, "<TB-DECLARE>", TbTable.fields("b.Nome")  'alla fine... quelli sopra possono introdurre tag...
    
    Print #ddlFile, RtbFile.text
    'singleDDL(i) = rtbFile.Text
    'rtbFile.SaveFile DataMan.DmParam_OutField_Db2 & TbTable.fields("DMDB2_Tabelle.Nome") & ".sql", 1
    
    TbTable.MoveNext
  Next i
  
  TbTable.Close
  '''''''''''''''''''''''''''''''''''''''
  ' ALTER TABLE: generalizzare...
  '''''''''''''''''''''''''''''''''''''''
  'Print #ddlFile, "--#KEN:" _
  '                & alterTableStatements & vbCrLf _
  '                & "--#KEF:"
  Print #ddlFile, alterTableStatements
  
  Close #ddlFile
  
  'rtbFile. .SaveFile DataMan.DmParam_OutField_Db2 & TbTable.fields("DMDB2_Tabelle.Nome") & ".sql", 1
  Exit Sub
errors:
  'gestire:
  If ERR.Number = 76 Then
    m_fun.MkDir_API DataMan.DmPathDb & "\OutPut-Prj\Db2\Sql\"
    Resume
  ElseIf ERR.Number = 75 Then
    ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\DDL*"" not found."
  Else
    MsgBox ERR.Description
    'Resume
  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Commenti Colonne:
''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function getCommentiColonne(IdTable As Long, nomeTable As String, Creator As String) As String
  Dim rs As Recordset
  Dim i As Integer
  
  On Error GoTo errdb
  
  Set rs = m_fun.Open_Recordset("select nome,commento from " & PrefDb & "_Colonne where " & _
                                "IdTable= " & IdTable & " AND commento <> '' order by ordinale")
  If rs.RecordCount Then
    'separato per formattazione...
    getCommentiColonne = "COMMENT ON COLUMN " & IIf(Len(Creator), Creator & ".", "") & nomeTable & "." & rs!Nome & vbCrLf & _
                         Space(8) & "IS '" & rs!Commento & "';" & vbCrLf & vbCrLf
    rs.MoveNext
    While Not rs.EOF
      getCommentiColonne = getCommentiColonne & _
                           "COMMENT ON COLUMN " & IIf(Len(Creator), Creator & ".", "") & nomeTable & "." & rs!Nome & vbCrLf & _
                           Space(8) & "IS '" & rs!Commento & "';" & vbCrLf & vbCrLf
      rs.MoveNext
    Wend
  Else
    getCommentiColonne = ""
  End If
  rs.Close
  Exit Function
errdb:
  MsgBox ERR.Description
  getCommentiColonne = ""
End Function
''Function getCommentiColonne_BPER() As String
''  Dim i As Integer
''  'per formattare...
''  getCommentiColonne_BPER = "COMMENT ON COLUMN" & vbCrLf & _
''                            Space(7) & columnComments(i) & ";" & vbCrLf & vbCrLf
''
''  For i = 1 To UBound(columnComments) - 1
''    getCommentiColonne_BPER = getCommentiColonne_BPER & Space(7) & "COMMENT ON COLUMN" & vbCrLf & _
''                              Space(7) & columnComments(i) & ";" & vbCrLf & vbCrLf
''  Next i
''End Function

Public Sub Declare_Load_ORA(RtbFile As RichTextBox)
  Dim TbTable As Recordset
  Dim wUser As String
  Dim wPassword As String
  Dim wPathLog As String
  Dim wPathDati As String
  Dim RecSeg As Recordset
  Dim NomeFile As String
  Dim wIdDB As Double
  Dim Tb1 As Recordset
  
On Error GoTo ErrorHandler
  'Chiedere se posso andare con id database piuttosto che con nome db
  
  Set TbTable = m_fun.Open_Recordset("select * from " & PrefDb & "_db where nome = '" & pNome_DBCorrente & "'")
  
  If TbTable.RecordCount > 0 Then
    wIdDB = TbTable!idDB
  End If

  RtbFile.text = ""


  Set TbTable = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & wIdDB)
    
  If TbTable.RecordCount > 0 Then
    TbTable.MoveFirst

    While TbTable.EOF = False
      
      DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Sql\LOAD\"

      'VIRGILIO
      If TbTable!Tag = "AUTOMATIC_DB2ORC" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-prj\Template\imsdb\standard\rdbms\" & dbType & "\DB2\SCRIPT.load"
      Else
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-prj\Template\imsdb\standard\rdbms\" & dbType & "\IMS\SCRIPT.load"
      End If

      wUser = LeggiParam("DM_USER")
      wPassword = LeggiParam("DM_PASSWORD")
      wPathDati = LeggiParam("DO_PATH_DATA")
      wPathDati = Replace(wPathDati, "$", DataMan.DmPathDb)
      If Trim(wUser) = "" Then wUser = "$1"
      If Trim(wPassword) = "" Then wPassword = "$2"
      If Trim(wPathDati) = "" Then wPathDati = "$3"
      
      changeTag RtbFile, "#USER#", wUser
      changeTag RtbFile, "#TABLENAME#", TbTable!Nome
      changeTag RtbFile, "#PASSWORD#", wPassword
      changeTag RtbFile, "#PATHDATA#", wPathDati
      
      NomeFile = TbTable!Nome
      
      'Salva il file
      RtbFile.SaveFile DataMan.DmParam_OutField_Db & NomeFile & ".load", 1

      TbTable.MoveNext
    Wend
  End If
  
  Exit Sub
ErrorHandler:
  If ERR.Number = 75 Then
    MkDir DataMan.DmParam_OutField_Db
    RtbFile.SaveFile DataMan.DmParam_OutField_Db & NomeFile & ".load", 1
    Resume Next
  Else
    m_fun.WriteLog "Errore in [MadmdM_DB2.Declare_Load_ORA]. Errore: " & ERR.Description
  End If
End Sub

Function getField_CTL(rs As Recordset, wLength As Integer, bIsVarChar As Boolean, Tag As String, Optional cls As MadmdC_CTL) As String
'modificato funzione:
'aggiunto parametro optional 'cls'
'prende gli stessi dati e li aggiunge alla collection nel chiamante
                      
  Dim stringaCTL As String
  Dim wTipo As String
  Dim wLen As Integer
  Dim WsTR As String
  Dim rsColonneORA As Recordset
  Dim TipoDB2 As String
  Dim rsTransCod As Recordset
  
  On Error GoTo ErrHandler
  
  'SQ 20141118 - Manca controllo su nome >= 25 --> errore non gestito space
  If Len(rs!Nome) < 25 Then
    stringaCTL = rs!Nome & Space(25 - Len(rs!Nome)) & "POSITION(" & Trim(str(wLength + 1)) & ":"
  Else
    'SQ tmp
    MsgBox "TMP ERROR: field longer than 24 characters: '" & rs!Nome & "'"
    Exit Function
  End If
  
  cls.FieldName = rs!Nome
  cls.Position = Trim(str(wLength + 1)) & ":"
  
  ' Mauro 28/02/2011: Nuova gestione per tipi in CTL
  Set rsTransCod = m_fun.Open_Recordset("Select * From TrascodificaDB2_ORC Where DB2Field = '" & rs!Tipo & "'")
  If rsTransCod.RecordCount = 0 Then
    TipoDB2 = rsTransCod!db2field
  Else
    If IsNull(rsTransCod!CTL_DB2) Then
      TipoDB2 = rsTransCod!db2field
    Else
      TipoDB2 = rsTransCod!CTL_DB2
    End If
  End If
  rsTransCod.Close
  
  Select Case TipoDB2
    Case "INTEGER"
      wTipo = "INTEGER"
      wLen = 4
    Case "SMALLINT"
      wTipo = "SMALLINT"
      wLen = 2
    Case "DECIMAL", "NUMBER"
      wTipo = "DECIMAL(" & Trim(str(rs!lunghezza)) & "," & Format(rs!Decimali, "0") & ")"
      wLen = Int((rs!lunghezza) / 2) + 1
    Case "FLOAT"
      wTipo = "FLOAT"
      wLen = 8
    Case "CHAR"
      wTipo = TipoDB2 & "(" & Trim(str(rs!lunghezza)) & ")"
      wLen = rs!lunghezza
    Case "VARCHAR", "VARCHAR2"
      bIsVarChar = True
      wTipo = "VARCHAR(" & Trim(str(rs!lunghezza)) & ")"
      wLen = rs!lunghezza + 2
    Case "DATE"
      wTipo = "DATE"
      WsTR = LeggiParam("DO_FORDATE_ORA")
      If Trim(WsTR) <> "" Then wTipo = wTipo & " " & """" & WsTR & """"
      wLen = rs!lunghezza
    Case "TIME"
      wTipo = "CHAR"
      wLen = 8
    Case "TIMESTAMP"
      wTipo = "TIMESTAMP"
      WsTR = LeggiParam("DO_FORTIMESTAMP_ORA")
      If Trim(WsTR) <> "" Then wTipo = wTipo & " " & """" & WsTR & """"
      wLen = rs!lunghezza
    Case Else
      m_fun.WriteLog "#Declare_Clt_ORA: incorrect data type in table " & rs!Nome
  End Select
  
  wLength = wLength + wLen
  stringaCTL = stringaCTL & Trim(str(wLength)) & ")     " & wTipo
  
  'MG 260207
  cls.Position = cls.Position & wLength
  cls.Tipo = wTipo
  
  If InStr(rs!Tag, "DB2ORC") Then
    Set rsColonneORA = m_fun.Open_Recordset("SELECT C.* from DMORC_Tabelle as TORA, DMDB2_Tabelle as TDB2, DMORC_Colonne as c where " & _
                                            "TORA.IdOrigine = TDB2.IdTable and C.IdTable = TORA.IdTable and " & _
                                            "TDB2.IdTable = " & rs!IdTable & " and C.Nome ='" & rs!Nome & "' ")
  Else
    Set rsColonneORA = rs
  End If
  
  If rsColonneORA.RecordCount > 0 Then
    If rsColonneORA!Null = True Then
      wLength = wLength + 1
      stringaCTL = stringaCTL & vbCrLf & _
                   Space(35) & "NULLIF(" & Trim(str(wLength)) & ")='" & LeggiParam("DO_NULL_VALUE") & "' "
      'MG 260207
      cls.Tipo = cls.Tipo & vbCrLf & _
                 Space(35) & "NULLIF(" & Trim(str(wLength)) & ")='" & LeggiParam("DO_NULL_VALUE") & "' "
    End If
    'MG 260207
    If rsColonneORA!Tipo = "VARCHAR2" And rsColonneORA!Null = 0 Then
      cls.IsVchar = True
    Else
      cls.IsVchar = False
    End If
  End If
  
  getField_CTL = stringaCTL & ","
  Exit Function
  
ErrHandler:
  MsgBox "Errore in creazione campi in CTL " & ERR.Description
  m_fun.WriteLog "Errore in [MadmdM_DB2.getField_CTL]. Errore : " & ERR.Description
  getField_CTL = ""
End Function

Public Sub Declare_Ctl_ORA(RtbFile As RichTextBox)
  Dim rs As Recordset, rsColonneORA As Recordset
  Dim rsColonne As Recordset, rsColonneDLI As Recordset
  Dim rsColonneORC As Recordset, rsCountVC As Recordset
  Dim wLength As Integer, templateText As String
  Dim wTipo As String, WsTR As String
  Dim wNRec As Long, wTotRec As Long, wLen As Long
  Dim wIdDB As Double
  Dim Tb1 As Recordset
  Dim wArrayPos() As String
  Dim k As Long
  Dim strCountVC As String, strCampi As String, strWhen As String
  Dim arWhen() As String
  Dim j As Integer
  Dim collCTL As Collection
  Dim conta As Integer
  Dim clsCTL As MadmdC_CTL
  'array con all'interno le combinazioni
  Dim ar() As String
  'variabile x verifica campi 'VARCHAR'
  Dim bIsVarChar As Boolean
  
  On Error GoTo ErrorHandler
  'Chiedere se posso andare con id database piuttosto che con nome db
  
  Set rs = m_fun.Open_Recordset("select * from " & PrefDb & "_db where nome = '" & pNome_DBCorrente & "'")
  If rs.RecordCount Then
    wIdDB = rs!idDB
  End If
  rs.Close
  RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\CTL_BASE_LOAD.ctl"
  templateText = RtbFile.text

  Set rs = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & wIdDB)
  While Not rs.EOF
    DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Sql\CTL\"

    'SQ 14-11-06
    'Tutte le volte si deve rileggere il TEMPLATE!!????????????????????
    RtbFile.text = templateText
    
    changeTag RtbFile, "<TABLE-NAME>", Replace(rs!Nome, "-", "_")
    
    k = 0
    ReDim wArrayPos(k)
    If InStr(rs!Tag, "DB2ORC") Then
      Set rsColonne = m_fun.Open_Recordset("select * from DMDB2_Colonne where idtable = " & rs!IdOrigine & " order by ordinale ")
    Else
      Set rsColonne = m_fun.Open_Recordset("select * from DMORC_Colonne where idtable = " & rs!IdTable & " order by ordinale ")
    End If

    'setto la variabile x il varchar
    bIsVarChar = False
          
    'setto la lunghezza a 0
    wLength = 0
    
    'ciclo sui campi
    Set collCTL = New Collection
    While Not rsColonne.EOF
      'contatore x array dove inserisco i campi
      k = k + 1
      ReDim Preserve wArrayPos(k)
      'carica i campi
      'all'interno dell'array c'� tutta la struttura dei campi
      Set clsCTL = New MadmdC_CTL
      wArrayPos(k) = getField_CTL(rsColonne, wLength, bIsVarChar, rs!Tag, clsCTL)
      collCTL.Add clsCTL
      rsColonne.MoveNext
    Wend
    
    'toglie l'ultima virgola
    If Len(wArrayPos(k)) Then
      wArrayPos(k) = Mid(wArrayPos(k), 1, Len(wArrayPos(k)) - 1)
    End If
    
    'inserisce la lunghezza
    changeTag RtbFile, "<TOTAL-LEN>", "0" & wLength
    
    '1) quanti sono?
    strCountVC = " SELECT COUNT(TIPO) as TIPI from dmorc_colonne " & _
                 " where tipo='VARCHAR2' and IdTable=" & rs!IdTable & " and [Null] = 0 "
    Set rsCountVC = m_fun.Open_Recordset(strCountVC)
    
    'adesso devo verificare se ci sono campi 'VARCHAR'
    If bIsVarChar And rsCountVC!TIPI > 0 Then
      'visto ce ci sono tipi 'varchar' ricavo...
      '2) in base a quanti sono costruisco 2^quanti sono condizioni di when
      
      'crea l'array con all'interno le combinazioni
      caricaArray ar, rsCountVC!TIPI
      
      If collCTL.Count > 0 Then   'inutile ma non si sa mai
        'svuota la stringa
        strCampi = ""
        'cicla su tutte le righe e le combinazioni
        conta = 0
        For j = 0 To UBound(ar)
          If conta = 0 Then
            changeTag RtbFile, "<WHEN>", getWhen(ar(j), collCTL)
            changeTag RtbFile, "<FIELD-POS(i)>", getFieldModified(ar(j), collCTL)
          Else
            RtbFile.SelText = vbCrLf & ")" & vbCrLf & " INTO TABLE " & vbCrLf & Replace(rs!Nome, "-", "_") & vbCrLf & _
                              getWhen(ar(j), collCTL) & vbCrLf & _
                              "(" & vbCrLf & _
                              getFieldModified(ar(j), collCTL) & vbCrLf
          End If
          conta = conta + 1
        Next
      End If
      
    Else
      'nel caso non ci siano campi 'VARCHAR' scrive il template
      strCampi = ""
      For k = 0 To UBound(wArrayPos)
        strCampi = strCampi & wArrayPos(k) & vbCrLf
      Next
      changeTag RtbFile, "<WHEN>", ""
      changeTag RtbFile, "<FIELD-POS(i)>", strCampi
    End If

    cleanTags RtbFile
    
    'Salva il file
    RtbFile.SaveFile DataMan.DmParam_OutField_Db & rs!Nome & ".ctl", 1

    rs.MoveNext
  Wend
  rs.Close
  
  Exit Sub
ErrorHandler:
  If ERR.Number = 75 Then
    MkDir DataMan.DmParam_OutField_Db
    RtbFile.SaveFile DataMan.DmParam_OutField_Db & rs!Nome & ".ctl", 1
    Resume Next
  Else
    m_fun.WriteLog "Errore in [MadmdM_DB2.DeclareCtl_ORA]. Errore : " & ERR.Description
    MsgBox ERR.Description
  End If
End Sub

Function getFieldModified(Riga As String, coll As Collection) As String
  Dim Stringa As String
  Dim cls As MadmdC_CTL
  Dim righe() As String
  Dim i As Integer
    
On Error GoTo ErrorHandler

  If UBound(Split(Riga, ",")) > 0 Then
    righe = Split(Riga, ",")
  Else
    ReDim Preserve righe(0)
    righe(0) = Riga
  End If
  i = 0
  Stringa = ""
  For Each cls In coll
    If cls.IsVchar Then
      If righe(i) = "-1" Then
        Stringa = Stringa & cls.FieldName & Space(25 - Len(cls.FieldName)) & "CONSTANT ' '," & vbCrLf
      Else
        Stringa = Stringa & cls.FieldName & Space(25 - Len(cls.FieldName)) & "POSITION(" & cls.Position & ")" & Space(8) & cls.Tipo & "," & vbCrLf
      End If
      If UBound(righe) = i Then i = 100 'imposta un valore finto x non uscire dal ciclo... possono esserci altri campi dopo
      i = i + 1
    Else
      Stringa = Stringa & cls.FieldName & Space(25 - Len(cls.FieldName)) & "POSITION(" & cls.Position & ")" & Space(8) & cls.Tipo & "," & vbCrLf
    End If
  Next
  
  Stringa = Mid(Stringa, 1, InStrRev(Stringa, ",") - 1)

  getFieldModified = Stringa
  Exit Function
ErrorHandler:
  MsgBox ERR.Description

End Function

'prese in considerazione solo le prime 4 (2^4)
'pensare a come farla meglio!!!
Sub caricaArray(ar() As String, condizioni As Integer)

  If condizioni = 1 Then
    ReDim Preserve ar(1)
  Else
    ReDim Preserve ar((2 ^ condizioni) - 1)
  End If
  
  Select Case condizioni
    Case 1
      ar(0) = "-1"
      ar(1) = "0"
    Case 2
      ar(0) = "-1,-1"
      ar(1) = "-1,0"
      ar(2) = "0,-1"
      ar(3) = "0,0"
    Case 3
      ar(0) = "-1,-1,-1"
      ar(1) = "-1,-1,0"
      ar(2) = "-1,0,-1"
      ar(3) = "-1,0,0"
      ar(4) = "0,-1,-1"
      ar(5) = "0,-1,0"
      ar(6) = "0,0,-1"
      ar(7) = "0,0,0"
    Case 4
      ar(0) = "-1,-1,-1,-1"
      ar(1) = "-1,-1,-1,0"
      ar(2) = "-1,-1,0,-1"
      ar(3) = "-1,-1,0,0"
      ar(4) = "-1,0,-1,-1"
      ar(5) = "-1,0,-1,0"
      ar(6) = "-1,0,0,-1"
      ar(7) = "-1,0,0,0"
      ar(8) = "0,-1,-1,-1"
      ar(9) = "0,-1,-1,0"
      ar(10) = "0,-1,0,-1"
      ar(11) = "0,-1,0,0"
      ar(12) = "0,0,-1,-1"
      ar(13) = "0,0,-1,0"
      ar(14) = "0,0,0,-1"
      ar(15) = "0,0,0,0"
    Case 5
      ar(0) = "-1,-1,-1,-1,-1"
      ar(1) = "-1,-1,-1,-1,0"
      ar(2) = "-1,-1,-1,0,-1"
      ar(3) = "-1,-1,-1,0,0"
      ar(4) = "-1,-1,0,-1,-1"
      ar(5) = "-1,-1,0,-1,0"
      ar(6) = "-1,-1,0,0,-1"
      ar(7) = "-1,-1,0,0,0"
      ar(8) = "-1,0,-1,-1,-1"
      ar(9) = "-1,0,-1,-1,0"
      ar(10) = "-1,0,-1,0,-1"
      ar(11) = "-1,0,-1,0,0"
      ar(12) = "-1,0,0,-1,-1"
      ar(13) = "-1,0,0,-1,0"
      ar(14) = "-1,0,0,0,-1"
      ar(15) = "-1,0,0,0,0"
      ar(16) = "0,-1,-1,-1,-1"
      ar(17) = "0,-1,-1,-1,0"
      ar(18) = "0,-1,-1,0,-1"
      ar(19) = "0,-1,-1,0,0"
      ar(20) = "0,-1,0,-1,-1"
      ar(21) = "0,-1,0,-1,0"
      ar(22) = "0,-1,0,0,-1"
      ar(23) = "0,-1,0,0,0"
      ar(24) = "0,0,-1,-1,-1"
      ar(25) = "0,0,-1,-1,0"
      ar(26) = "0,0,-1,0,-1"
      ar(27) = "0,0,-1,0,0"
      ar(28) = "0,0,0,-1,-1"
      ar(29) = "0,0,0,-1,0"
      ar(30) = "0,0,0,0,-1"
      ar(31) = "0,0,0,0,0"
  End Select
  
End Sub
'deve ritornare una stringa con:
'WHEN (x:y) <>(=) X'000'  AND .....
Function getWhen(Riga As String, coll As Collection) As String
  Dim Stringa As String
  Dim cls As MadmdC_CTL
  Dim righe() As String
  Dim i As Integer
    
  On Error GoTo ErrorHandler

  righe = Split(Riga, ",")
    
  i = 0
  For Each cls In coll
    If cls.IsVchar Then
      If righe(i) = "-1" Then
        Stringa = Stringa & "(" & cls.Position & ")" & " = " & "X'0000' AND "
      Else
        Stringa = Stringa & "(" & cls.Position & ")" & " <> " & "X'0000' AND "
      End If
      If UBound(righe) = i Then Exit For
      i = i + 1
    End If
  Next
  
  Stringa = "WHEN " & Mid(Stringa, 1, Len(Stringa) - 4)

  getWhen = Stringa

  Exit Function
ErrorHandler:
  'MsgBox "Errore in caricamento 'WHEN' :" & ERR.Description
  m_fun.WriteLog "Errore in [MadmdM_DB2.getWhen]. Errore : " & ERR.Description

End Function


Public Sub SostWord(rtb As RichTextBox, StrDa As String, StrA As String)
  Dim wPos As Double
  
  wPos = rtb.Find(StrDa, 1)
  While wPos > 0
     rtb.SelStart = wPos
     rtb.SelLength = Len(StrDa)
     rtb.SelText = StrA
     wPos = rtb.Find(StrDa, wPos + 1)
  Wend
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' E' un SostWord con indentazione (relativa alla posizione del TAG)
' Tratta i TAG "ciclici": devono terminare con (i)...
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub changeTag(rtb As RichTextBox, Tag As String, text As String)
  Dim Posizione As Long
  Dim crLfPosition As Long
  Dim indentedText As String
    
  If isCiclicTag(Tag) Then
    If Len(text) Then text = text & vbCrLf & Tag 'se text="" e' un clean!...
  End If
  While True
    Posizione = rtb.Find(Tag, Posizione + Len(indentedText))
    Select Case Posizione
      Case -1
        Exit Sub
      Case 0
        indentedText = text
      Case Else
        crLfPosition = InStrRev(rtb.text, vbCrLf, Posizione)
        If crLfPosition = 0 Then
          'Linea 1: non ho il fine linea...
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(Posizione))
        Else
          indentedText = Replace(text, vbCrLf, vbCrLf & Space(Posizione - crLfPosition - 1))
        End If
    End Select
    rtb.SelStart = Posizione
    rtb.SelLength = Len(Tag)
    rtb.SelText = indentedText
  Wend
End Sub

Public Sub InsertTableSpace(rtb As RichTextBox, tb As Recordset, Stgroup As String, Tablespace As String, NDb As String)
  InsertParola rtb, "<CREATE-TABLESPACE>.", "CREATE TABLESPACE " & Tablespace & vbCrLf & Space(9) & "IN " & NDb & vbCrLf & Space(9) & "USING STOGROUP " & Stgroup & vbCrLf & Space(11) & "PRIQTY XXX" & vbCrLf & Space(11) & "SECQTY XXX" & vbCrLf & Space(11) & "FREEPAGE 0 PCTFREE 5" & vbCrLf & Space(9) & "LOCKSIZE ANY" & vbCrLf & Space(9) & "BUFFERPOOL BP0" & vbCrLf & Space(9) & "CLOSE NO;"
End Sub

Public Sub InsertTableName(rtb As RichTextBox, tb As Recordset, Creator As String)
  InsertParola rtb, "<TABLE>", Creator & "." & tb!Nome
End Sub

Public Sub InsertIndicatori_BPER(rtb As RichTextBox, tb As Recordset)
  Dim TbColonne As Recordset
  Dim NotNull As String
  Dim Tipo As String
  Dim indice As Integer
  Dim indSeg As Integer
  Dim bol As Boolean
  Dim NomeCampo As String
  Dim alias As String
  
  On Error GoTo errdb
  
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  indice = 1
  alias = Mid(tb!Nome, 8, 2) & "N" & Right(tb!Nome, 3) 'ALIAS: CONTROLLARE
  Set TbColonne = m_fun.Open_Recordset("select nome from " & PrefDb & "_Colonne where Idtable= " & tb!IdTable & " AND [NULL]=true " & " order by ordinale")
  While Not TbColonne.EOF
    DelParola rtb, "<LISTA-CAMPI-INDICATORE>."
    NomeCampo = alias & "-" & Replace(TbColonne!Nome, "_", "-") & "-N"
    If indice = 1 Then
      rtb.SelText = "01 " & alias & "-N." & vbCrLf & Space(7) & _
                    "  10 " & NomeCampo & Space(28 - Len(NomeCampo)) & "PIC S9(4) USAGE COMP VALUE +0." & vbCrLf & Space(7) & "<LISTA-CAMPI-INDICATORE>."
    ElseIf (TbColonne.RecordCount) <> indice Then
        rtb.SelText = "  10 " & NomeCampo & Space(28 - Len(NomeCampo)) & "PIC S9(4) USAGE COMP VALUE +0." & vbCrLf & Space(7) & "<LISTA-CAMPI-INDICATORE>."
      Else
        rtb.SelText = "  10 " & NomeCampo & Space(28 - Len(NomeCampo)) & "PIC S9(4) USAGE COMP VALUE +0." & vbCrLf & vbCrLf & Space(6)
    End If
    TbColonne.MoveNext
    indice = indice + 1
  Wend
  Exit Sub
errdb:
  'gestire
  MsgBox ERR.Description
  Resume
End Sub

Public Sub InsertIndicatori(rtb As RichTextBox, tb As Recordset)
  Dim TbColonne As Recordset
  Dim NotNull As String
  Dim Tipo As String
  Dim indice As Integer
  Dim indSeg As Integer
  Dim bol As Boolean
  Dim NomeCol As String
  
  'PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  indice = 1
  If dbType = "VSAM" Then
    Stop
  End If
  'Set TbColonne = DbPrj.OpenRecordset("select * from DMColonne where IdSegm= " & tb!IdSegm & " order by ordinale")
'  Select Case TipoDB
'  Case "DB2"
'  Case "ORACLE"
' '   Stop
'  Case "VSAM"
'    Stop
'  End Select
  
  Set TbColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where Idtable= " & tb!IdTable & " AND [NULL]=true order by ordinale")
  
  While Not TbColonne.EOF
  '   If TbColonne!IdTable = TbColonne!IdTableJoin Then
  'stefanopippo: 17/08/2005
       'DelParola RTB, "<LISTA-CAMPI-INDICATORE>."
       DelParola rtb, "<LISTA-CAMPI-INDICATORE>"
       NotNull = " COMP."
       Tipo = "PIC S9(4)"
       NomeCol = Replace(TbColonne!Nome, "_", "-")
       'SP 9/4 indicatori di Null in gruppo per initialize
       If indice = 1 Then
          rtb.SelText = "01 WK-I-" & Replace(tb!Nome, "_", "-") & "." & vbCrLf & Space(7) & _
                        "   10 WK-I-" & Trim(NomeCol) & Space(25 - Len(Trim(Replace(NomeCol, "_", "-")))) & Tipo & NotNull & vbCrLf & Space(7) & "<LISTA-CAMPI-INDICATORE>"
       ElseIf (TbColonne.RecordCount) <> indice Then
         rtb.SelText = "   10 WK-I-" & Trim(NomeCol) & Space(25 - Len(Trim(Replace(NomeCol, "_", "-")))) & Tipo & NotNull & vbCrLf & Space(7) & "<LISTA-CAMPI-INDICATORE>"
       Else
         rtb.SelText = "   10 WK-I-" & Trim(NomeCol) & Space(25 - Len(Trim(Replace(NomeCol, "_", "-")))) & Tipo & NotNull & vbCrLf & vbCrLf & Space(6)
       End If
   '  End If
   TbColonne.MoveNext
   indice = indice + 1
  Wend
  TbColonne.Close
End Sub

Public Sub InsertTableNameDeclare(rtb As RichTextBox, tb As Recordset, Creator As String)
'''  If Creator = "" Or Creator = "BLANK" Or Creator = "BLANKS" Then
'''      InsertParola rtb, "<TABLE-DECLARE>", tb!Nome
'''  Else
'''      InsertParola rtb, "<TABLE-DECLARE>", Creator & "." & tb!Nome
'''  End If
  'TILVIA
  InsertParola rtb, "<CREATOR>", Creator
  InsertParola rtb, "<TABLE-DECLARE>", tb!Nome
  ' creator & "." & "F" & Mid(SelectedDb, 4, 2) & "C" & Mid(Trim(tb!nome), 5, 4)
End Sub

'SQ-01 (tmp 22-10-09)
Public Sub InsertDichiarativa(rtb As RichTextBox, tb As Recordset, Optional alias As String = "")
  Dim TbColonne As Recordset
  Dim NotNull As String
  Dim indice As Integer, indexColonne As Integer
  Dim Decimali As String
  Dim lunghezza As String
  Dim NomeCol As String
  'SQ-02
  Dim indentDataType As Integer
  ''SQ-02: calcolare il max(len) una volta sola a inizio sessione...
  indentDataType = 16
  
  On Error GoTo errdb
  
  Set TbColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where " & _
                                       "IdTable= " & tb!IdTable & " order by ordinale")
  If m_fun.FnNomeDB = "banca.mty" Then
    ReDim columnComments(TbColonne.RecordCount)
  End If
  indexColonne = 0
  If TbColonne.RecordCount Then
    indice = 1
    While Not TbColonne.EOF
      'DelParola rtb, "<DICHIARAZIONE-CAMPI>."
      NomeCol = TbColonne!Nome
      'SQ-02  TMP (buttare col controllo sopra)
      If Len(TbColonne!Nome) >= indentDataType Then
        indentDataType = Len(TbColonne!Nome) + 1
      End If
      If Len(TbColonne!Nome) > 30 Then
        MsgBox "Column Name '" & TbColonne!Nome & "' too long!", vbExclamation
        indentDataType = Len(TbColonne!Nome)
      End If
      If TipoDB = "DB2" Then
        If TbColonne!Null Then
          NotNull = " "
        Else
          NotNull = " NOT NULL"
        End If
        If TbColonne!IsDefault Then
          NotNull = NotNull & " WITH DEFAULT "
        End If
        If Trim(TbColonne!defaultValue) <> "" Then
          NotNull = NotNull & TbColonne!defaultValue
        End If
      Else
        NotNull = ""
        'DEFAULT USER NOT NULL
        If TbColonne!defaultValue <> "" Then
          NotNull = NotNull & " DEFAULT "
          NotNull = NotNull & TbColonne!defaultValue
'          If TbColonne!Null Then
'            NotNull = " NULL"
'          Else
'            NotNull = NotNull & " NOT NULL"
'          End If
        End If
        If TbColonne!Null Then
          NotNull = NotNull & " NULL"
        Else
          NotNull = NotNull & " NOT NULL"
        End If
      End If
      
      Decimali = ""
      lunghezza = " " & (TbColonne!lunghezza)
      If TbColonne!Tipo = "DECIMAL" Or TbColonne!Tipo = "NUMBER" Then
        If TbColonne!Decimali > 0 Then
          Decimali = ", " & TbColonne!Decimali
        End If
      End If
      If TbColonne!Tipo <> "DATE" And TbColonne!Tipo <> "TIME" And TbColonne!Tipo <> "SMALLINT" And TbColonne!Tipo <> "INTEGER" And TbColonne!Tipo <> "TIMESTAMP" Then
        If (TbColonne.RecordCount) <> indice Then
          'rtb.SelText = NomeCol & Space(indentDataType - Len(NomeCol)) & UCase(TbColonne!Tipo) & " (" & Trim((lunghezza)) & Trim(Decimali) & ") " & NotNull & "," & vbCrLf & _
          '              Space(13) & "<DICHIARAZIONE-CAMPI>."
          changeTag rtb, "<DICHIARAZIONE-CAMPI(i)>", NomeCol & Space(indentDataType - Len(NomeCol)) & UCase(TbColonne!Tipo) & " (" & Trim((lunghezza)) & Trim(Decimali) & ") " & NotNull & ","
        Else
          'rtb.SelText = NomeCol & Space(indentDataType - Len(NomeCol)) & UCase(TbColonne!Tipo) & " (" & Trim((lunghezza)) & Trim(Decimali) & ") " & NotNull & InsDecMod(Mid$(NomeCol, 1, 4))
          changeTag rtb, "<DICHIARAZIONE-CAMPI(i)>", NomeCol & Space(indentDataType - Len(NomeCol)) & UCase(TbColonne!Tipo) & " (" & Trim((lunghezza)) & Trim(Decimali) & ") " & NotNull & InsDecMod(Mid$(NomeCol, 1, 4))
        End If
      Else
        If (TbColonne.RecordCount) <> indice Then
          'rtb.SelText = NomeCol & Space(indentDataType - Len(NomeCol)) & UCase(TbColonne!Tipo) & " " & NotNull & "," & vbCrLf & _
          '              Space(13) & "<DICHIARAZIONE-CAMPI>."
          changeTag rtb, "<DICHIARAZIONE-CAMPI(i)>", NomeCol & Space(indentDataType - Len(NomeCol)) & UCase(TbColonne!Tipo) & " " & NotNull & ","
        Else
          'rtb.SelText = NomeCol & Space(indentDataType - Len(NomeCol)) & UCase(TbColonne!Tipo) & " " & NotNull & InsDecMod(Mid$(NomeCol, 1, 4))
          changeTag rtb, "<DICHIARAZIONE-CAMPI(i)>", NomeCol & Space(indentDataType - Len(NomeCol)) & UCase(TbColonne!Tipo) & " " & NotNull & InsDecMod(Mid$(NomeCol, 1, 4))
        End If
      End If
      
'''      'TMP: realizzare template con TAG indicizzati... generalizzare!
'''      'COMMENT ON COLUMN
'''      Select Case m_fun.FnNomeDB
'''        Case "banca.mty"
'''          columnComments(indice - 1) = "<x2cccc>.<TB-DECLARE>." & NomeCol & " IS '" & IIf(IsNull(TbColonne!NomeCmp), "EXTRA COLUMN", TbColonne!NomeCmp) & "@'"
'''          'MsgBox NomeCol
'''        Case "Fase_2.mty"
'''          If Len(TbColonne!Commento) Then
'''            ReDim Preserve columnComments(indexColonne)
'''            columnComments(indexColonne) = "<TB-DECLARE>." & NomeCol & " IS '" & TbColonne!Commento & "'"
'''            indexColonne = indexColonne + 1
'''          End If
'''        Case Else
'''          '
'''      End Select
      TbColonne.MoveNext
      indice = indice + 1   'usare il FOR!
    Wend
  Else
    ' SILVIA 13-3-2008  SE NON TROVA CAMPI PER LA TABELLA
    DataMan.DmFinestra.ListItems.Add , , Now & " No columns for IdTable = " & tb!IdTable & " - DBName = " & tb!NomeDB
    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
  End If
  cleanTags rtb
  TbColonne.Close
  Exit Sub
errdb:
  MsgBox ERR.Description
  Resume
End Sub

Public Function InsDecMod(wPref As String) As String
  If SwColMod Then
    InsDecMod = "," & vbCrLf & _
                Space(11) & wPref & "DTAMOD                    DATE NOT NULL ," & vbCrLf & _
                Space(11) & wPref & "TMEMOD                    TIME NOT NULL ," & vbCrLf & _
                Space(11) & wPref & "PGMMOD                    CHAR(8) ," & vbCrLf & _
                Space(11) & wPref & "TRNMOD                    CHAR(4) ," & vbCrLf & _
                Space(11) & wPref & "OPEMOD                    CHAR(4) ," & vbCrLf & _
                Space(11) & wPref & "USRMOD                    CHAR(8)"
  Else
    InsDecMod = ""
  End If
End Function

Public Sub AggiungiPKey(rtb As RichTextBox, tb As Recordset)
  Dim pkey As String
  Dim TbColonne As Recordset
  Dim TbKey As Recordset
  Dim indice As Integer
  Dim NomeCol As String
  Dim Nomecolapp As String
  Dim pos As Integer

  Set TbColonne = m_fun.Open_Recordset("select * from DMDB2_Colonne where IdTable= " & tb!IdTable & " order by ordinale")
  If TbColonne.RecordCount <> 0 Then
     TbColonne.MoveLast
     TbColonne.MoveFirst

     If TipoDB = "DB2" Then
        Set TbKey = m_fun.Open_Recordset("select * from DMDB2_Index where IdTable= " & tb!IdTable & " and Tipo = 'P' order by IdIndex")
        If TbKey.RecordCount <> 0 Then
          TbKey.MoveLast
          TbKey.MoveFirst
          indice = 1

          While TbKey.EOF = False
            pos = 1
            Nomecolapp = ""
            If Len(Trim(TbColonne!Nome)) > 18 Then
              NomeCol = Mid(Trim(TbColonne!Nome), 1, 18)
            Else
              NomeCol = Trim(TbKey!Nome)
            End If
            While InStr(pos, NomeCol, "-")
              Nomecolapp = Nomecolapp & Mid(NomeCol, pos, InStr(pos, NomeCol, "-") - pos) & "_"
              pos = InStr(pos, NomeCol, "-") + 1
            Wend
            NomeCol = Trim(Nomecolapp & Mid(NomeCol, pos))
            If indice = TbKey.RecordCount Then
              pkey = ")"
            Else
              pkey = "," & vbCrLf & Space(19) & "<PRIMARY-KEY>."
            End If
            If indice = 1 Then
               InsertParola rtb, "<PRIMARY-KEY>.", "PRIMARY KEY(" & Trim(NomeCol) & pkey
           Else
             InsertParola rtb, "<PRIMARY-KEY>.", Trim(NomeCol) & pkey
           End If

            indice = indice + 1
            TbKey.MoveNext
          Wend
        Else
          DelParola rtb, "<PRIMARY-KEY>."
        End If
     End If
  End If

End Sub

Sub CreatePKey(rtb As RichTextBox, tb As Recordset)
  Dim TbCol As Recordset
  Dim Index As Integer
  Dim NomeCol As String
  Dim Nomecolapp As String
  Dim pos As Integer

  Set TbCol = m_fun.Open_Recordset("select * from DMDB2_Index where IdTable= " & tb!IdTable & " and Tipo ='P' order by IdIndex")
  If TbCol.RecordCount <> 0 Then
    Index = 1

    While TbCol.EOF = False
      pos = 1
      Nomecolapp = ""
      While InStr(pos, TbCol!Nome, "-")
        Nomecolapp = Nomecolapp & Mid(TbCol!colonna, pos, InStr(pos, TbCol!Nome, "-") - pos) & "_"
        pos = InStr(pos, TbCol!Nome, "-") + 1
      Wend
      Nomecolapp = Trim(Nomecolapp & Mid(TbCol!Nome, pos))
      If Index <> TbCol.RecordCount Then
        NomeCol = Trim(Nomecolapp) & "," & vbCrLf & Space(Len("ON FSRVZ." & tb!Nome) + 8) & "<CREATE-PRIMARY-KEY>."
      Else
        NomeCol = Trim(Nomecolapp)
      End If
      InsertParola rtb, "<CREATE-PRIMARY-KEY>.", NomeCol
      TbCol.MoveNext
      Index = Index + 1
    Wend
  End If
End Sub

Public Sub InserisciKey(rtb As RichTextBox, tb As Recordset)
  Dim rsIndex As Recordset, rsIdxCol As Recordset, r As Recordset
  Dim Ind As Integer, sp As Integer, indice As Integer
  Dim colonna As String, chiave_i As String, NomeCol As String, Tipo As String
  Dim Usage As String

  On Error GoTo ERR
  
  Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_Index where idtable = " & tb!IdTable & " and tipo='P'")
  If rsIndex.RecordCount Then
    If UCase(m_fun.FnNomeDB) = "AMPERT.MTY" Then
      InsertParola rtb, "<CHIAVI-PRIMARIE>", "01 KRR-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<COLONNA>." & vbCrLf & _
                                  Space(7) & "01 K02-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<CHIAVE2>."
    Else
      InsertParola rtb, "<CHIAVI-PRIMARIE>", "01 KRR-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<COLONNA>." & vbCrLf & _
                                  Space(7) & "01 K01-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<CHIAVE1>." & vbCrLf & _
                                  Space(7) & "01 K02-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<CHIAVE2>." & vbCrLf & _
                                  Space(7) & "01 K03-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<CHIAVE3>." & vbCrLf & _
                                  Space(7) & "01 K04-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<CHIAVE4>." & vbCrLf & _
                                  Space(7) & "01 K05-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<CHIAVE5>." & vbCrLf & _
                                  Space(7) & "01 K06-" & Replace(rsIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<CHIAVE6>."
    End If
    Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IdxCol where " & _
                                        "idtable = " & tb!IdTable & " and idindex = " & rsIndex!IdIndex & " order by ordinale")
    If rsIdxCol.RecordCount Then
      For Ind = 0 To rsIdxCol.RecordCount - 1
        sp = 10
        rsIndex.MoveFirst
        While Not rsIndex.EOF
          Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where idcolonna=" & rsIdxCol!IdColonna)
          If r.RecordCount Then
            NomeCol = Replace(r!Nome, "_", "-")
            'NomeCol = r!nome
            Select Case r!Tipo
              Case "CHAR"
                colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(" & r!lunghezza & ")." & vbCrLf & Space(sp) & "<COLONNA>."
                chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(" & r!lunghezza & ")." & vbCrLf & Space(sp) & "<CHIAVE_i>."
              Case "TIME"
                colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(8)." & vbCrLf & Space(sp) & "<COLONNA>."
                chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(8)." & vbCrLf & Space(sp) & "<CHIAVE_i>."
              Case "DATE"
                colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(10)." & vbCrLf & Space(sp) & "<COLONNA>."
                chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(10)." & vbCrLf & Space(sp) & "<CHIAVE_i>."
              Case "INTEGER"
                colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(9) COMP." & vbCrLf & Space(sp) & "<COLONNA>."
                chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(9) COMP." & vbCrLf & Space(sp) & "<CHIAVE_i>."
              Case "LONG"
                colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(18) COMP." & vbCrLf & Space(sp) & "<COLONNA>."
                chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(18) COMP." & vbCrLf & Space(sp) & "<CHIAVE_i>."
              Case "DECIMAL"
                If IsNull(r!Decimali) Then r!Decimali = 0
                Tipo = "PIC S9" & "(" & (CInt(r!lunghezza) - r!Decimali) & ")"
                If r!Decimali > 0 Then
                  Tipo = Tipo & "V9(" & r!Decimali & ") "
                End If
                Usage = " COMP-3."
                colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & Tipo & Usage & vbCrLf & Space(sp) & "<COLONNA>."
                chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & Tipo & Usage & vbCrLf & Space(sp) & "<CHIAVE_i>."
              Case "SMALLINT"
                colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(4) COMP." & vbCrLf & Space(sp) & "<COLONNA>."
                chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(4) COMP." & vbCrLf & Space(sp) & "<CHIAVE_i>."
              Case "VARCHAR"
                'colonna = "10  " & nomecol & "." & vbCrLf & Space(15) & "15  ADL-" & nomecol & "-LEN" & Space(20 - (Len(nomecol) + Len(nomecol & "-LEN"))) & "PIC S9(4) COMP." & vbCrLf & Space(15) & "15  ADL-" & nomecol & "-TEXT" & Space(20 - (Len(nomecol) + Len(nomecol & "-TEXT"))) & "PIC X(" & TbColRel!Lunghezza & ")." & vbCrLf & Space(11) & "<COLONNA>."
              Case "VARCHAR2"
              'stefano: 18/7/2006
              Case "TIMESTAMP"
                colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(26)." & vbCrLf & Space(sp) & "<COLONNA>."
                chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(26)." & vbCrLf & Space(sp) & "<CHIAVE_i>."
              Case "NUMBER"
                colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(" & r!lunghezza & ") COMP-3." & vbCrLf & Space(sp) & "<COLONNA>."
                chiave_i = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(" & r!lunghezza & ") COMP-3." & vbCrLf & Space(sp) & "<CHIAVE_i>."
            End Select
            InsertParola rtb, "<COLONNA>.", colonna
            For indice = 1 To 6
              InsertParola rtb, "<CHIAVE" & indice & ">.", Replace(chiave_i, "_i", indice)
            Next indice
          End If
          r.Close
          rsIndex.MoveNext
        Wend
        rsIdxCol.MoveNext
      Next Ind
      DelParola rtb, "<COLONNA>."
      For indice = 1 To 6
        DelParola rtb, "<CHIAVE" & indice & ">."
      Next indice
    Else
      colonna = vbCrLf & Space(6) & "*NO PRIMARY KEY'COLUMNS"
      InsertParola rtb, "<COLONNA>.", colonna
      InsertParola rtb, "<CHIAVE>.", colonna
      InsertParola rtb, "<CHIAVE_i>.", colonna
    End If
    rsIdxCol.Close
  Else
    colonna = vbCrLf & Space(6) & "*NO PRIMARY KEY'COLUMNS"
    InsertParola rtb, "<CHIAVI-PRIMARIE>", colonna
  End If
  rsIndex.Close
  
  Exit Sub
ERR:
  Select Case ERR.Number
    Case 380
      Beep
    Case Else
      MsgBox ERR.Number & ERR.Description
  End Select
  'Resume
End Sub

Function GetLenKey(lunghezza As Integer, Tipo As String) As Integer
  On Error GoTo errdb
  GetLenKey = 0
  
  'Parametrizzare il log: � molto lento!
  ' m_fun.WriteLog "Debug: colonna=" & TbColonne!Nome & " - totLen=" & getDclgenLen
  Select Case Tipo
    Case "CHAR"
      GetLenKey = lunghezza
    Case "TIME"
      GetLenKey = 8
    Case "DATE"
      GetLenKey = 10
    Case "TIMESTAMP"
      GetLenKey = 26
    Case "INTEGER"
      GetLenKey = 4
    Case "LONG"
      GetLenKey = 8
    Case "DECIMAL"
      GetLenKey = CInt((lunghezza + 1.1) / 2)
    Case "SMALLINT"
      GetLenKey = 2
    Case "VARCHAR", "VARCHAR2"
      GetLenKey = 2 + lunghezza
    'SQ 15-
    Case "NUMBER" 'ORACLE
      GetLenKey = CInt((lunghezza + 1.1) / 2)
    Case Else
      'm_fun.WriteLog "##gGetLenKey: type unknown=" & Tipo & " - idTable: " & IdTable
  End Select
  'X(1) in piu' per "indicatore di null"
  Exit Function
errdb:
  MsgBox ERR.Description
  GetLenKey = -1
End Function

Public Sub InserisciKey_PLI(rtb As RichTextBox, tb As Recordset, bol As Integer, cont As String)
  Dim rsIndex As Recordset, rsIdxCol As Recordset, tbField As Recordset, TbApp As Recordset, r As Recordset
  Dim indSeg As Integer, Ind As Integer, sp As Integer, indice As Integer
  Dim colonna As String, chiave_i As String, NomeCol As String, TipoCol As String, Col As String, Tipo As String
  Dim Usage As String, testop As String
  Dim nomeIndice As String
  'AC
  Dim totKeyLen As Long
  
  On Error GoTo ERR
  
  Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_Index where idtable = " & tb!IdTable & " and tipo = 'P'")
  
  If rsIndex.RecordCount Then
    'SQ 25-05-06
    Dim krrFieldName As String
    krrFieldName = krrFieldName
    nomeIndice = Replace(rsIndex!Nome, "-", "_")
    InsertParola rtb, "<CHIAVI-PRIMARIE>", "DCL 1 KRR_" & nomeIndice & "," & vbCrLf & _
                      "<COLONNA>." & vbCrLf & _
                      "  DCL  KRR_" & nomeIndice & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(KRR_" & nomeIndice & ")); " & vbCrLf & _
                      Space(2) & "DCL 1 K01_" & nomeIndice & "," & vbCrLf & _
                      "<COLONNA>." & vbCrLf & _
                      "  DCL  K01_" & nomeIndice & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K01_" & nomeIndice & ")); " & vbCrLf & _
                      Space(2) & "DCL 1 K02_" & nomeIndice & "," & vbCrLf & _
                      "<COLONNA>." & vbCrLf & _
                      "  DCL  K02_" & nomeIndice & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K02_" & nomeIndice & ")); " & vbCrLf & _
                      Space(2) & "DCL 1 K03_" & nomeIndice & "," & vbCrLf & _
                      "<COLONNA>." & vbCrLf & _
                      "  DCL  K03_" & nomeIndice & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K03_" & nomeIndice & ")); " & vbCrLf & _
                      Space(2) & "DCL 1 K04_" & nomeIndice & "," & vbCrLf & _
                      "<COLONNA>." & vbCrLf & _
                      "  DCL  K04_" & nomeIndice & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K04_" & nomeIndice & ")); " & vbCrLf & _
                      Space(2) & "DCL 1 K05_" & nomeIndice & "," & vbCrLf & _
                      "<COLONNA>." & vbCrLf & _
                      "  DCL  K05_" & nomeIndice & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K05_" & nomeIndice & ")); " & vbCrLf & _
                      Space(2) & "DCL 1 K06_" & nomeIndice & "," & vbCrLf & _
                      "<COLONNA>." & vbCrLf & _
                      "  DCL  K06_" & nomeIndice & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K06_" & nomeIndice & ")); " & vbCrLf

    colonna = ""
    Set rsIdxCol = m_fun.Open_Recordset("select *  from " & PrefDb & "_IdxCol where " & _
                                        "idtable = " & tb!IdTable & " and idindex = " & rsIndex!IdIndex & " order by ordinale")
    If rsIdxCol.RecordCount Then
      totKeyLen = 0
      For Ind = 0 To rsIdxCol.RecordCount - 1
        sp = 4
        rsIndex.MoveFirst
        indSeg = rsIndex.RecordCount
        While Not rsIndex.EOF
          Set r = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where idcolonna=" & rsIdxCol!IdColonna)
          If r.RecordCount > 0 Then
            totKeyLen = totKeyLen + GetLenKey(r!lunghezza, r!Tipo)
            NomeCol = Replace(r!Nome, "-", "_")
            'NomeCol = r!nome
            Select Case bol
              Case 1
                Select Case r!Tipo
                  Case "CHAR", "DATE"
                    colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "CHAR(" & r!lunghezza & ")," & vbCrLf  '& "<COLONNA>."
                    chiave_i = chiave_i & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "CHAR(" & r!lunghezza & ")," & vbCrLf
                  Case "DECIMAL", "NUMBER"
                    If r!Decimali <> "0" Then
                      colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED DEC(" & r!lunghezza & "," & r!Decimali & ")," & vbCrLf
                      chiave_i = chiave_i & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED DEC(" & r!lunghezza & "," & r!Decimali & ")," & vbCrLf
                    Else
                      colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED DEC(" & r!lunghezza & ")," & vbCrLf
                      chiave_i = chiave_i & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED DEC(" & r!lunghezza & ")," & vbCrLf
                    End If
                  'SILVIA 27-03-2009
                  Case "SMALLINT", "INTEGER"
                    If r!lunghezza <= 4 Then
                      colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED BIN(15)," & vbCrLf
                      chiave_i = chiave_i & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED BIN(15)," & vbCrLf
                    Else
                      colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED BIN(31)," & vbCrLf
                      chiave_i = chiave_i & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED BIN(31)," & vbCrLf
                    End If
                  Case "VARCHAR"
                    colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "CHAR(" & r!lunghezza & ") VAR," & vbCrLf
                    chiave_i = chiave_i & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "VARCHAR(" & r!lunghezza & ")," & vbCrLf
                End Select
            End Select

            Select Case bol
              Case 1
                For indice = 1 To 6
                  InsertParola rtb, "<CHIAVE" & indice & ">.", Replace(chiave_i, "_i", indice)
                Next indice
              Case 2
                InsertParola rtb, "<CLUCOLONNA>.", colonna
                InsertParola rtb, "<CLUCHIAVE>.", chiave_i
            End Select
          End If
          rsIndex.MoveNext
        Wend
        rsIdxCol.MoveNext
      Next Ind
      colonna = Mid(colonna, 1, InStrRev(colonna, ",") - 1) & ";" & "<COLONNA>."
      'colonna = colonna & vbCrLf & "  KRR_" & nomeIndice & " = '' ; "

      For indice = 1 To 7
        InsertParola rtb, "<COLONNA>.", colonna
        InsertParola rtb, "<TOTKEYLEN>", CStr(totKeyLen)
        DelParola rtb, "<COLONNA>."
      Next indice
      Select Case bol
        Case 1
          For indice = 1 To 6
            DelParola rtb, "<CHIAVE" & indice & ">."
          Next indice
        Case 2
          DelParola rtb, "<CLUCOLONNA>."
          DelParola rtb, "<CLUCHIAVE>."
          DelParola rtb, "<CLUCHIAVE1>."
      End Select
    Else
      Select Case bol
        Case 1
          colonna = vbCrLf & Space(6) & "*NO PRIMARY KEY'COLUMNS"
          InsertParola rtb, "<COLONNA>.", colonna
          InsertParola rtb, "<CHIAVE>.", colonna
          InsertParola rtb, "<CHIAVE_i>.", colonna
        Case 2
          colonna = vbCrLf & Space(6) & "*NO CLUSTERING KEY'COLUMNS"
          InsertParola rtb, "<CLUCOLONNA>.", colonna
          InsertParola rtb, "<CLUCHIAVE>.", colonna
          InsertParola rtb, "<CLUCHIAVE1>.", colonna
      End Select
    End If
  Else
    Select Case bol
      Case 1
        colonna = vbCrLf & Space(6) & "*NO PRIMARY KEY'COLUMNS"
        InsertParola rtb, "<CHIAVI-PRIMARIE>", colonna
      Case 2
        colonna = vbCrLf & Space(6) & "*NO CLUSTERING KEY'COLUMNS"
        InsertParola rtb, "<CHIAVI-CLUSTERING>.", colonna
    End Select
  End If
  rsIndex.Close
  rsIdxCol.Close
  Exit Sub
ERR:
  Select Case ERR.Number
    Case 380
      Beep
    Case Else
      MsgBox ERR.Number & ERR.Description
  End Select
  'Resume
End Sub

Public Sub InserisciPrimKeyDef(rtb As RichTextBox, IdTable As Long, nomeTable As String, Creator As String)
  Dim TbColonne As Recordset, TbIndex As Recordset
  Dim Colonne As String, ColonneCreate As String
  Dim i As Integer, j As Integer
  Dim indexName As String
  Dim rtbKey As RichTextBox
  
  On Error GoTo ErrorHandler
  Set rtbKey = MadmdF_Start.rtbKey
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  
  Set TbIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_Index where " & _
                                     "idtable = " & IdTable & " and tipo='P' and used = true")

  For i = 1 To TbIndex.RecordCount
    rtbKey.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\DDL-CHIAVI-PRIMARIE"
    
    'Preparo la ex <COLONNA>
    Set TbColonne = m_fun.Open_Recordset("select b.nome, a.order from " & PrefDb & "_IdxCol as a," & PrefDb & "_Colonne as b where " & _
                                         "a.idColonna=b.idColonna AND a.idtable= " & IdTable & " and " & _
                                         "idindex=" & TbIndex!IdIndex & " order by a.ordinale")
                                         
    'SILVIA 12-3-2008 segnalazione se non trova colonne per questo indice
    If TbColonne.RecordCount Then
 
      For j = 1 To TbColonne.RecordCount
        Colonne = Colonne & IIf(Len(Colonne), "," & vbCrLf, "") & TbColonne!Nome
        ColonneCreate = ColonneCreate & IIf(Len(ColonneCreate), "," & vbCrLf, "") & TbColonne!Nome & IIf(InStr(TbColonne!Order, "DESC"), " DESC ", "")
        TbColonne.MoveNext
      Next j
      TbColonne.Close
        
      indexName = TbIndex!Nome
      
      changeTag rtbKey, "<Creator>", Creator
      changeTag rtbKey, "<indexName>", indexName
      If InStr(DataMan.DmNomeDB, "prj-GRZ") Then
        changeTag rtbKey, "<indexNameU>", "U" & Mid(indexName, 2)
      End If
      changeTag rtbKey, "<Colonne>", Colonne
      changeTag rtbKey, "<Colonne-Create>", ColonneCreate
      changeTag rtbKey, "<nomeTable>", nomeTable
      
      'SILVIA 31-03-2009 PER CLIENTE GRZ
      changeTag rtbKey, "<DB-Name>", pNome_DBCorrente
      changeTag rtbKey, "<TBS-NAME>", NomeTBS
      
      changeTag rtb, "<<DDL-CHIAVI-PRIMARIE>>", rtbKey.text
      rtb.text = rtb.text & vbCrLf & _
                 Space(8) & "<<DDL-CHIAVI-PRIMARIE>>"
      TbIndex.MoveNext
    Else
      'ANOMALIA: no colonne appartenenti all'indice
      'per il momento segnaliamo solo, proseguendo...
     ' SILVIA 13-3-2008  SE NON TROVA CAMPI PER LA TABELLA
      DataMan.DmFinestra.ListItems.Add , , Now & " No columns for index " & TbIndex!Nome & "  And IdTable = " & TbIndex!IdTable
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
      '-------
      'SILVIA 1-4-2009 X GRZ
      changeTag rtb, "<<DDL-CHIAVI-PRIMARIE>>", vbCrLf & Space(12) & ") IN " & pNome_DBCorrente & "." & NomeTBS & " CCSID EBCDIC; " & vbCrLf
      rtb.text = rtb.text & vbCrLf & _
                 Space(8) & "<<DDL-CHIAVI-PRIMARIE>>"
      '---------
    End If
  Next i
  TbIndex.Close
  
  DelParola rtb, "<<DDL-CHIAVI-PRIMARIE>>"
  Exit Sub
ErrorHandler:
  If ERR.Number = 75 Then
    ERR.Raise 75, , ""
  Else
    MsgBox ERR.Description
  End If
End Sub

Public Sub InserisciSecIdxDef(rtb As RichTextBox, IdTable As Long, nomeTable As String, Creator As String)
  Dim TbColonne As Recordset, TbIndex As Recordset
  Dim Colonne As String
  Dim i As Integer, j As Integer
  Dim indexName As String
  Dim rtbKey As RichTextBox
  
  On Error GoTo ErrorHandler
  Set rtbKey = MadmdF_Start.rtbKey
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  
  Set TbIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_Index where " & _
                                     "idtable = " & IdTable & " and tipo='I' and used = True ")

  For i = 1 To TbIndex.RecordCount
    rtbKey.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\DDL-CHIAVI-SECONDARIE"
    'Preparo la ex <COLONNA>
    Set TbColonne = m_fun.Open_Recordset("select b.nome, a.order from " & PrefDb & "_IdxCol as a," & PrefDb & "_Colonne as b where " & _
                                         "a.idColonna=b.idColonna AND a.idtable= " & IdTable & " and " & _
                                         "idindex=" & TbIndex!IdIndex & " order by a.ordinale")
    Colonne = ""
    
    'SILVIA 12-3-2008 segnalazione se non trova colonne per questo indice
    If TbColonne.RecordCount Then
      
      For j = 1 To TbColonne.RecordCount
        Colonne = Colonne & IIf(Len(Colonne), "," & vbCrLf, "") & TbColonne!Nome & IIf(InStr(TbColonne!Order, "DESC"), " DESC ", "")
        TbColonne.MoveNext
      Next j
      TbColonne.Close
      indexName = TbIndex!Nome
      
      changeTag rtbKey, "<UNIQUE>", IIf(TbIndex!Unique, " UNIQUE", "")
      changeTag rtbKey, "<Creator>", Creator
      changeTag rtbKey, "<indexName>", indexName
      changeTag rtbKey, "<nomeTable>", nomeTable
      changeTag rtbKey, "<Colonne>", Colonne
      
      changeTag rtb, "<<DDL-CHIAVI-SECONDARIE>>", rtbKey.text
      rtb.text = rtb.text & vbCrLf & Space(8) & "<<DDL-CHIAVI-SECONDARIE>>"
      
  ''    'SQ 4-12-07 aggiunto BUFFERPOOL (richiesta RealeMutua)
  ''    InsertParola rtb, "<CHIAVI-SECONDARIE>", " " & "CREATE" & IIf(TbIndex!Unique, " UNIQUE ", " ") & "INDEX " & Creator & "." & indexName _
  ''                         & vbCrLf & Space(10) & " " & "ON " & Creator & "." & nomeTable _
  ''                         & vbCrLf & Space(10) & " " & "( " _
  ''                         & vbCrLf & Space(12) & Replace(Colonne, ",", "," & vbCrLf & Space(12)) _
  ''                         & vbCrLf & Space(10) & " " & ") " _
  ''                         & vbCrLf & Space(10) & " " & "USING STOGROUP <storageGroup-i>" _
  ''                         & vbCrLf & Space(10) & " " & "PRIQTY <priqty-i>" _
  ''                         & vbCrLf & Space(10) & " " & "SECQTY <secqty-i>" _
  ''                         & vbCrLf & Space(10) & " " & "FREEPAGE 0" _
  ''                         & vbCrLf & Space(10) & " " & "BUFFERPOOL <bufferpool-i>" _
  ''                         & vbCrLf & Space(10) & " " & "PCTFREE 10" _
  ''                         & vbCrLf & Space(10) & " " & "COPY NO;"

    Else
      'ANOMALIA: no colonne appartenenti all'indice
      'per il momento segnaliamo solo, proseguendo...
     ' SILVIA 13-3-2008  SE NON TROVA CAMPI PER LA TABELLA
      DataMan.DmFinestra.ListItems.Add , , Now & " No columns for index " & TbIndex!Nome & "  And IdTable = " & TbIndex!IdTable
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
 
    End If

    TbIndex.MoveNext
  Next i
  TbIndex.Close
  
  DelParola rtb, "<<DDL-CHIAVI-SECONDARIE>>"
  Exit Sub
ErrorHandler:
  If ERR.Number = 75 Then
    ERR.Raise 75, , ""
  Else
    MsgBox ERR.Description
  End If
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Effetto collaterale: riempiono la alterTableStatements
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub InserisciForKeyDef(IdTable As Long, nomeTable As String, Creator As String, alterTableStatements As String)
  Dim TbColonne As Recordset, TbIndex As Recordset
  Dim Colonne As String
  Dim i As Integer, j As Integer
  Dim indexName As String
  Dim rsQuery As Recordset
  Dim ColonneRef As String
  Dim rtbKey As RichTextBox
  
  On Error GoTo ErrorHandler
  Set rtbKey = MadmdF_Start.rtbKey
  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  
  'ATTENZIONE: MODIFICA TEMPORANEA ANCHE PER LA BANCA... RIPRISTINARE!
  'Devo prendere le colonne della chiave (composta) del parent
  Set TbIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_Index where idtable = " & IdTable & " and tipo='F'")
  If TbIndex.RecordCount Then
    Set TbColonne = m_fun.Open_Recordset("select idtable, nome from " & PrefDb & "_tabelle where " & _
                                         "IdTable =" & TbIndex!ForeignIdTable)
    Dim ParentTable As String
    If TbColonne.RecordCount Then
      ParentTable = TbColonne!Nome
      Set rsQuery = m_fun.Open_Recordset("SELECT C2.Nome as cDest, IDX.Ordinale, IX.ForeignIdTable From " & _
                                         "DMDB2_INDEX as IX, DMDB2_IDXCOL as IDX, DMDB2_COLONNE as C2 WHERE " & _
                                         "IX.IDINDEX = IDX.IDINDEX and C2.IDCOLONNA = IDX.FORIDCOLONNA and " & _
                                         "IDX.IDINDEX = " & TbIndex!IdIndex & _
                                         " ORDER BY IDX.Ordinale;")
      ColonneRef = ""
      While Not rsQuery.EOF
        ColonneRef = ColonneRef & IIf(Len(ColonneRef), "," & vbCrLf, "") & rsQuery!cDest
        rsQuery.MoveNext
      Wend
    End If
    TbColonne.Close
  End If

  For i = 1 To TbIndex.RecordCount
    rtbKey.LoadFile m_fun.FnPathDB & "\Input-Prj\Template\imsdb\standard\rdbms\" & dbType & "\DDL-CHIAVI-FOREIGN"
    'Preparo la ex <COLONNA>
    Set TbColonne = m_fun.Open_Recordset("select b.nome, a.order from " & PrefDb & "_IdxCol as a," & PrefDb & "_Colonne as b where " & _
                                         "a.idColonna=b.idColonna AND a.idtable= " & IdTable & " and " & _
                                         "idindex=" & TbIndex!IdIndex & " order by a.ordinale")
    For j = 1 To TbColonne.RecordCount
      Colonne = Colonne & IIf(Len(Colonne), "," & vbCrLf, "") & TbColonne!Nome & IIf(InStr(TbColonne!Order, "DESC"), " DESC ", "")
      TbColonne.MoveNext
    Next j
    TbColonne.Close
    'Preparo la ex <COLONNA>
    indexName = TbIndex!Nome
    'ALTER TABLE va a parte!
    
    changeTag rtbKey, "<Creator>", Creator
    changeTag rtbKey, "<nomeTable>", nomeTable
    changeTag rtbKey, "<indexName>", indexName
    changeTag rtbKey, "<Colonne>", Colonne
    changeTag rtbKey, "<ParentTable>", ParentTable
    changeTag rtbKey, "<ColonneRef>", ColonneRef
    
    alterTableStatements = alterTableStatements & vbCrLf & rtbKey.text
    TbIndex.MoveNext
  Next i
  TbIndex.Close
  Exit Sub
ErrorHandler:
  If ERR.Number = 75 Then
    ERR.Raise 75, , ""
  Else
    MsgBox ERR.Description
  End If
End Sub

Public Sub InserisciSecondaryKey(rtb As RichTextBox, tb As Recordset)
  Dim rsIdxCol As Recordset, TbColRel As Recordset, TbIndex As Recordset, TbColonne As Recordset
  Dim Ind As Integer, i As Integer
  Dim colonna As String, NomeCol As String, Tipo As String, Usage As String
  
'  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  
  On Error GoTo ERR
  
  Set TbIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_Index where " & _
                                     "idtable = " & tb!IdTable & " and tipo ='I' order by idIndex")
  Do While Not TbIndex.EOF
    Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IdxCol where " & _
                                        "idtable= " & tb!IdTable & " and idindex=" & TbIndex!IdIndex & " order by ordinale")
    If Not rsIdxCol.EOF Then
      InsertParola rtb, "<CHIAVI-SECONDARIE>", "01 KRR-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<COLONNA0>." & vbCrLf & Space(7) & _
                                               "01 K01-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<COLONNA1>." & vbCrLf & Space(7) & _
                                               "01 K02-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<COLONNA2>." & vbCrLf & Space(7) & _
                                               "01 K03-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<COLONNA3>." & vbCrLf & Space(7) & _
                                               "01 K04-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<COLONNA4>." & vbCrLf & Space(7) & _
                                               "01 K05-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<COLONNA5>." & vbCrLf & Space(7) & _
                                               "01 K06-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf & Space(10) & "<COLONNA6>." & vbCrLf & Space(7) & _
                                               "<CHIAVI-SECONDARIE>"
      
      For i = 1 To rsIdxCol.RecordCount
        Set TbColonne = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where idcolonna=" & rsIdxCol!IdColonna)
        If TbColonne.RecordCount Then
          NomeCol = Replace(TbColonne!Nome, "_", "-")
          Select Case TbColonne!Tipo
            Case "CHAR"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(" & TbColonne!lunghezza & ")." & vbCrLf & "<COLONNA_i>."
            Case "TIME"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(8)." & vbCrLf & "<COLONNA_i>."
            Case "DATE"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(10)." & vbCrLf & "<COLONNA_i>."
            Case "INTEGER"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(9) COMP." & vbCrLf & "<COLONNA_i>."
            Case "LONG"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(18) COMP." & vbCrLf & "<COLONNA_i>."
            Case "DECIMAL"
              Tipo = "PIC S9" & "(" & (CInt(TbColonne!lunghezza) - CInt(TbColonne!Decimali)) & ")"
              If TbColonne!Decimali = 0 Then
                Tipo = Tipo & ""
              Else
                Tipo = Tipo & "V9(" & TbColonne!Decimali & ") "
              End If
              Usage = " COMP-3."
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & Tipo & Usage & vbCrLf & "<COLONNA_i>."
            Case "SMALLINT"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC S9(4) COMP." & vbCrLf & "<COLONNA_i>."
            Case "VARCHAR"
            '  stringa = "10  " & nomecol & "." & vbCrLf & Space(15) & "15  ADL-" & nomecol & "-LEN" & Space(20 - (Len(nomecol) + Len(nomecol & "-LEN"))) & "PIC S9(4) COMP." & vbCrLf & Space(15) & "15  ADL-" & nomecol & "-TEXT" & Space(20 - (Len(nomecol) + Len(nomecol & "-TEXT"))) & "PIC X(" & TbColRel!Lunghezza & ")." & vbCrLf & Space(10) & vbCrLf & "<COLONNA_i>."
            Case "VARCHAR2"
'stefano: 18/7/2006
            Case "TIMESTAMP"
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & "PIC X(26)." & vbCrLf & "<COLONNA_i>."
            'VIRGILIO 1/3/2007
            Case "NUMBER"
              Tipo = "PIC S9" & "(" & (CInt(TbColonne!lunghezza) - CInt(TbColonne!Decimali)) & ")"
              If TbColonne!Decimali = 0 Then
                Tipo = Tipo & ""
              Else
                Tipo = Tipo & "V9(" & TbColonne!Decimali & ") "
              End If
              Usage = " COMP-3."
              colonna = "10  " & NomeCol & Space(25 - Len(NomeCol)) & Tipo & Usage & vbCrLf & "<COLONNA_i>."
              
          End Select
          
          For Ind = 0 To 6
            InsertParola rtb, "<COLONNA" & Ind & ">.", IIf(i > 1, Space(10), "") & Replace(colonna, "_i", Ind)
          Next Ind
        End If
        rsIdxCol.MoveNext
      Next i
      
      For Ind = 0 To 6
        DelParola rtb, "<COLONNA" & Ind & ">."
      Next Ind
    Else
      'ANOMALIA: no colonne appartenenti all'indice
      'per il momento segnaliamo solo, proseguendo...
      'MsgBox "No columns for index " & TbIndex!Nome & " - DBName = " & tb!NomeDB & " And IdTable = " & TbIndex!IdTable, vbExclamation
       ' SILVIA 13-3-2008  SE NON TROVA CAMPI PER LA TABELLA
      DataMan.DmFinestra.ListItems.Add , , Now & " No columns for index " & TbIndex!Nome & " - DBName = " & tb!NomeDB & " And IdTable = " & TbIndex!IdTable
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible

    End If
    TbIndex.MoveNext
  Loop
  DelParola rtb, "<CHIAVI-SECONDARIE>"
  Exit Sub
ERR:
  Select Case ERR.Number
  Case 380
    MsgBox "Creation copy interrupted", vbInformation, DataMan.DmNomeProdotto
  Case Else
    MsgBox ERR.Number & ERR.Description
    Resume Next
  End Select
End Sub

Public Sub InserisciSecondaryKey_PLI(rtb As RichTextBox, tb As Recordset)
  Dim rsIdxCol As Recordset, TbColRel As Recordset, TbIndex As Recordset, TbColonne As Recordset
  Dim Ind As Integer, i As Integer
  Dim colonna As String, NomeCol As String, Tipo As String, Usage As String
  Dim totKeyLen As Long
'  PrefDb = get_PrefixQuery_TipoDB(TipoDB)
  
  On Error GoTo ERR
  Dim sp As Integer
  sp = 4
  Set TbIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_Index where idtable = " & tb!IdTable & " and tipo ='I' order by idIndex")
  Do While Not TbIndex.EOF
    
    Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IdxCol where idtable= " & tb!IdTable & " and idindex=" & TbIndex!IdIndex & " order by ordinale")
    If Not rsIdxCol.EOF Then
'      InsertParola rtb, "<CHIAVI-SECONDARIE>", "DCL 1 KRR_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & _
'                                                Space(2) & "DCL 1 K01_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & _
'                                                Space(2) & "DCL 1 K02_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & _
'                                                Space(2) & "DCL 1 K03_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & _
'                                                Space(2) & "DCL 1 K04_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & _
'                                                Space(2) & "DCL 1 K05_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & _
'                                                Space(2) & "DCL 1 K06_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & _
'                                                "<CHIAVI-SECONDARIE>"
      InsertParola rtb, "<CHIAVI-SECONDARIE>", "DCL 1 KRR_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & "  DCL  KRR_" _
                    & Replace(TbIndex!Nome, "-", "_") & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(KRR_" & Replace(TbIndex!Nome, "-", "_") & ")); " & vbCrLf & _
                                     Space(2) & "DCL 1 K01_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & "  DCL  K01_" _
                    & Replace(TbIndex!Nome, "-", "_") & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K01_" & Replace(TbIndex!Nome, "-", "_") & ")); " & vbCrLf & _
                                     Space(2) & "DCL 1 K02_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & "  DCL  K02_" _
                    & Replace(TbIndex!Nome, "-", "_") & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K02_" & Replace(TbIndex!Nome, "-", "_") & ")); " & vbCrLf & _
                                     Space(2) & "DCL 1 K03_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & "  DCL  K03_" _
                    & Replace(TbIndex!Nome, "-", "_") & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K03_" & Replace(TbIndex!Nome, "-", "_") & ")); " & vbCrLf & _
                                     Space(2) & "DCL 1 K04_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & "  DCL  K04_" _
                    & Replace(TbIndex!Nome, "-", "_") & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K04_" & Replace(TbIndex!Nome, "-", "_") & ")); " & vbCrLf & _
                                     Space(2) & "DCL 1 K05_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & "  DCL  K05_" _
                    & Replace(TbIndex!Nome, "-", "_") & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K05_" & Replace(TbIndex!Nome, "-", "_") & ")); " & vbCrLf & _
                                     Space(2) & "DCL 1 K06_" & Replace(TbIndex!Nome, "-", "_") & "," & vbCrLf & "<COLONNA>." & vbCrLf & "  DCL  K06_" _
                    & Replace(TbIndex!Nome, "-", "_") & "R  CHAR(<TOTKEYLEN>) BASED(ADDR(K06_" & Replace(TbIndex!Nome, "-", "_") & ")); " & vbCrLf & "<CHIAVI-SECONDARIE>"

      colonna = ""
      totKeyLen = 0
      For i = 1 To rsIdxCol.RecordCount
        Set TbColonne = m_fun.Open_Recordset("Select * From " & PrefDb & "_Colonne where idcolonna=" & rsIdxCol!IdColonna)
        If TbColonne.RecordCount Then
          totKeyLen = totKeyLen + GetLenKey(TbColonne!lunghezza, TbColonne!Tipo)
          NomeCol = Replace(TbColonne!Nome, "_", "-")
          Select Case TbColonne!Tipo
            Case "CHAR"
              colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "CHAR(" & TbColonne!lunghezza & ")," & vbCrLf
            Case "DECIMAL", "NUMBER"
              If TbColonne!Decimali <> "0" Then
                colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED DEC(" & TbColonne!lunghezza & "," & TbColonne!Decimali & "),"
              Else
                colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED DEC(" & TbColonne!lunghezza & ")," & vbCrLf
              End If
            Case "SMALLINT", "INTEGER"
              If TbColonne!lunghezza <= 4 Then
                colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED BIN(15)," & vbCrLf
              Else
                colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "FIXED BIN(31)," & vbCrLf
              End If
            Case "VARCHAR"
              colonna = colonna & Space(sp) & "2  " & NomeCol & Space(25 - Len(NomeCol)) & "CHAR(" & TbColonne!lunghezza & ") VAR," & vbCrLf
          End Select
          
        End If
        rsIdxCol.MoveNext
      Next i
      
      colonna = Mid(colonna, 1, InStrRev(colonna, ",") - 1) & ";" & "<COLONNA>."
      For i = 1 To 7
        InsertParola rtb, "<COLONNA>.", colonna
        InsertParola rtb, "<TOTKEYLEN>", CStr(totKeyLen)
        DelParola rtb, "<COLONNA>."
      Next i
    Else
      'ANOMALIA: no colonne appartenenti all'indice
      'per il momento segnaliamo solo, proseguendo...
      'MsgBox "No columns for index " & TbIndex!Nome, vbExclamation
      ' SILVIA 13-3-2008  SE NON TROVA CAMPI PER LA TABELLA
      DataMan.DmFinestra.ListItems.Add , , Now & " No columns for index " & TbIndex!Nome
      DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
    End If
    TbIndex.MoveNext
  Loop
  TbIndex.Close
  DelParola rtb, "<CHIAVI-SECONDARIE>"
  Exit Sub
ERR:
  Select Case ERR.Number
    Case 380
      MsgBox "Creation copy interrupted", vbInformation, DataMan.DmNomeProdotto
    Case Else
      MsgBox ERR.Number & ERR.Description
      Resume Next
  End Select
End Sub

Function getListaCampi(tableName As String, IdTable As Long)
  Dim rsColonne As Recordset, rsTabelle As Recordset
  Dim k As Integer, x As Integer, LenRiga As Integer, indice As Integer
  Dim cont As String, NotNull As String, Tipo As String, Usage As String
  Dim NomeCol As String
  Dim ListaCampi As String, Str1 As String, str2 As String
  Dim bol As Boolean
  Dim cCopyArea As String, env As Collection
  Dim ListArr() As String
  Dim i As Integer

  On Error GoTo errSub
  
  'PrefDb = get_PrefixQuery_TipoDB(TipoDB)

  LenRiga = 72
  indice = 1
  Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where IdTable= " & IdTable & " order by ordinale")
  If rsColonne.RecordCount Then
    Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idtable = " & IdTable)
    cCopyArea = LeggiParam("C-COPY-AREA")
    Set env = New Collection  'resetto;
    env.Add rsColonne
    env.Add rsTabelle
    If Len(cCopyArea) Then
      cCopyArea = Replace(getEnvironmentParam(cCopyArea, "colonne", env), "_", "-")
    Else
      'segnalare errore
      cCopyArea = "DCL-" & Replace(rsTabelle!Nome, "_", "-")
    End If
    rsTabelle.Close
    getListaCampi = "01  " & cCopyArea & "." & vbCrLf
    
    While Not rsColonne.EOF
      'SQ 20141118
      'NomeCol = Replace(rsColonne!Nome, "_", "-")
      NomeCol = "DCL-" & Replace(rsColonne!Nome, "_", "-")
      
      Select Case TipoDB
        Case "VSAM"
          '
        Case "ORACLE"
          Select Case rsColonne!Tipo
            Case "CHAR"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC X(" & rsColonne!lunghezza & ")."
            Case "DATE"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC X(10)."
            Case "LONG"
  '            Stop 'CONTROLLA SE VA BENE
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC S9(18) COMP."
            Case "NUMBER"
              Tipo = "PIC S9" & "(" & (CInt(rsColonne!lunghezza) - CInt(rsColonne!Decimali)) & ")"
              If rsColonne!Decimali > 0 Then
                Tipo = Tipo & "V9(" & rsColonne!Decimali & ") "
              End If
              Usage = " COMP-3."
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " " & Tipo & Usage
            Case "VARCHAR"
              ListaCampi = Space(2) & "10 " & NomeCol & "." & vbCrLf & _
                           Space(5) & "49  " & NomeCol & "-LEN" & Space(31 - (Len(NomeCol) + Len(NomeCol & "-LEN"))) & " PIC S9(4) COMP." & vbCrLf & _
                           Space(5) & "49  " & NomeCol & "-TEXT" & Space(31 - (Len(NomeCol) + Len(NomeCol & "-TEXT"))) & " PIC X(" & rsColonne!lunghezza & ")."
            Case "VARCHAR2"
              ListaCampi = Space(2) & "10 " & NomeCol & "." & vbCrLf & _
                           Space(5) & "49  " & NomeCol & "-LEN" & Space(41 - (Len(NomeCol) + Len(NomeCol & "-LEN"))) & " PIC S9(4) COMP." & vbCrLf & _
                           Space(5) & "49  " & NomeCol & "-TEXT" & Space(41 - (Len(NomeCol) + Len(NomeCol & "-TEXT"))) & " PIC X(" & rsColonne!lunghezza & ")."
            Case "SMALLINT"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC S9(4) COMP."
            Case "TIMESTAMP"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC X(26)."
            Case "INTEGER"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC S9(9) COMP."
          End Select
          'VIRGILIO 26/2/2007 INIZIO
          ListArr = Split(ListaCampi, vbCrLf)
          For i = 0 To UBound(ListArr)
            If Len(ListArr(i)) > 72 Then
              k = InStr(ListArr(i), "PIC")
              If k <> 0 Then
                str2 = RTrim(Mid(ListArr(i), 1, k - 1))
                'str2 = RTrim(Replace(str2, vbCrLf, ""))
                Str1 = RTrim(Mid(ListArr(i), k, Len(ListArr(i))))
                ListArr(i) = str2 & Space(LenRiga - Len(str2)) & vbCrLf
                For x = 1 To Len(str2)
                  If Mid(str2, 1, 1) <> " " Then
                    Str1 = Space(x + 2) & Str1
                    'ListArr(i) = ListArr(i) & Str1 & Space(LenRiga - Len(Str1)) & cont & vbCrLf
                    ListArr(i) = ListArr(i) & Str1
                    If Len(Str1) < 72 Then
                      ListArr(i) = ListArr(i) & Space(LenRiga - Len(Str1)) & vbCrLf
                    Else
                      ListArr(i) = ListArr(i) & vbCrLf
                    End If
                    Exit For
                  Else
                    str2 = Mid(str2, 2)
                  End If
                Next x
              End If
            Else
              ListArr(i) = ListArr(i) & Space(LenRiga - Len(ListArr(i))) & vbCrLf
            End If
          Next i
          ListaCampi = ""
          For i = 0 To UBound(ListArr)
            ListaCampi = ListaCampi & ListArr(i)
          Next i
          'VIRGILIO 26/2/2007 FINE
        Case "DB2"
          Select Case rsColonne!Tipo
            Case "CHAR"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC X(" & rsColonne!lunghezza & ")."
            Case "TIME"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC X(8)."
            Case "DATE"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC X(10)."
            Case "TIMESTAMP"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC X(26)."
            Case "INTEGER"
              'SQ - 19-07-05:
              'ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC S9(8) COMP."
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC S9(9) COMP."
            Case "LONG"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC S9(18) COMP."
            Case "DECIMAL"
              If IsNull(rsColonne!Decimali) Then rsColonne!Decimali = 0
              'PIPPO
              Tipo = "PIC S"
              If (CInt(rsColonne!lunghezza) - rsColonne!Decimali) > 0 Then
                Tipo = Tipo & "9(" & (CInt(rsColonne!lunghezza) - rsColonne!Decimali) & ")"
              End If
              'Tipo = "PIC S9" & "(" & (CInt(rsColonne!Lunghezza) - rsColonne!Decimali) & ")"
              If rsColonne!Decimali > 0 Then
                Tipo = Tipo & "V9(" & rsColonne!Decimali & ") "
              End If
              Usage = " COMP-3."
              If Len(NomeCol) < 31 Then
                ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & Tipo & Usage
              Else
                ListaCampi = Space(2) & "10 " & NomeCol & "  " & Tipo & Usage
              End If
            Case "SMALLINT"
              ListaCampi = Space(2) & "10 " & NomeCol & Space(31 - Len(NomeCol)) & " PIC S9(4) COMP."
            Case "VARCHAR"
              'attento alla mozzatura della riga
              ListaCampi = Space(2) & "10 " & NomeCol & "."
              If m_fun.FnNomeDB = "banca.mty" Then
                If (29 > Len(NomeCol & "-TEXT")) Then
                  Str1 = Space(4) & "49 " & NomeCol & "-LEN" & Space(31 - Len(NomeCol & "-LEN")) & " PIC S9(4) COMP."
                  str2 = Space(4) & "49 " & NomeCol & "-TEXT" & Space(31 - Len(NomeCol & "-TEXT")) & " PIC X(" & rsColonne!lunghezza & ")."
                Else
                  Str1 = Space(4) & "49 " & NomeCol & "-LEN   " & " PIC S9(4) COMP."
                  str2 = Space(4) & "49 " & NomeCol & "-TEXT  " & " PIC X(" & rsColonne!lunghezza & ")."
                End If
              Else
                Str1 = Space(4) & "49 " & NomeCol & "-LEN" & Space(40 - (Len(NomeCol) + Len(NomeCol & "-LEN"))) & " PIC S9(4) COMP."
                str2 = Space(4) & "49 " & NomeCol & "-TEXT" & Space(40 - (Len(NomeCol) + Len(NomeCol & "-TEXT"))) & " PIC X(" & rsColonne!lunghezza & ")."
              End If
              bol = True
          End Select
          'cont = Format(val(cont) + 10, "00000000")
          If Not bol Then
            If Len(ListaCampi) > LenRiga Then
              'MsgBox listaCampi
              k = InStr(ListaCampi, "PIC")
              If k <> 0 Then
                str2 = RTrim(Mid(ListaCampi, 1, k - 1))
                Str1 = RTrim(Mid(ListaCampi, k, Len(ListaCampi)))
                'listaCampi = str2 & Space(LenRiga - Len(str2)) & cont & vbCrLf
                'SQ-tmp:
                If LenRiga > Len(str2) Then
                  ListaCampi = str2 & Space(LenRiga - Len(str2)) & vbCrLf
                Else
                  ListaCampi = str2 & " " & vbCrLf
                End If
                'cont = Format(val(cont) + 10, "00000000")
                For x = 1 To Len(str2)
                  If Mid(str2, 1, 1) <> " " Then
                    Str1 = Space(x + 2) & Str1
                    If LenRiga > Len(Str1) Then
                      ListaCampi = ListaCampi & Str1 & Space(LenRiga - Len(Str1)) & vbCrLf
                    Else
                      ListaCampi = ListaCampi & Str1 & " " & vbCrLf
                    End If
                    Exit For
                  Else
                    str2 = Mid(str2, 2)
                  End If
                Next x
              End If
            Else
              'listaCampi = listaCampi & Space(LenRiga - Len(listaCampi)) & cont & vbCrLf
              ListaCampi = ListaCampi & Space(LenRiga - Len(ListaCampi)) & vbCrLf
            End If
          Else
            If Trim(ListaCampi) <> "" Then
              ListaCampi = ListaCampi & Space(LenRiga - Len(ListaCampi)) & vbCrLf
            End If
            If Trim(Str1) <> "" Then
              'tmp... dava errore
              If LenRiga > Len(Str1) Then
                ListaCampi = ListaCampi & Str1 & Space(LenRiga - Len(Str1)) & vbCrLf
              Else
                ListaCampi = ListaCampi & Str1 & vbCrLf
              End If
            End If
            If Trim(ListaCampi) <> "" Then
              ListaCampi = ListaCampi & str2 & Space(LenRiga - Len(str2)) & vbCrLf
            End If
            bol = False
          End If
          '''''''''''''''''''''''''''''''''''''''''''''''''
          ' CAMPI NULL:
          ' AGGIUNGIAMO UN PIC X(1)
          '''''''''''''''''''''''''''''''''''''''''''''''''
          If rsColonne!Null Then
            If m_fun.FnNomeDB = "banca.mty" Then
              'PRIMO BYTE!
              'stefano: ci sono nomi pi� lunghi di 23!!!!
              'tmp: ListaCampi = Space(2) & "10 " & NomeCol & "-N" & Space(29 - Len(NomeCol)) & "PIC X(1)." & vbCrLf & ListaCampi
            Else
              'stefanopippo: 17/08/2005: matteo, matteo.... direi di togliere tutto!
              ListaCampi = ListaCampi & Space(2) & "10 " & NomeCol & "-N" & Space(29 - Len(NomeCol)) & "PIC X(1)." & vbCrLf
            End If
          End If
      End Select
      getListaCampi = getListaCampi & ListaCampi
      rsColonne.MoveNext
    Wend
  End If
  rsColonne.Close
Exit Function
errSub:
  MsgBox ERR.Description
  Resume
End Function

Function getListaCampi_PLI(tableName As String, IdTable As Long)
  Dim rsColonne As Recordset
  Dim k As Integer, x As Integer, LenRiga As Integer, indice As Integer
  Dim cont As String, NotNull As String, Tipo As String, Usage As String
  Dim NomeCol As String
  Dim ListaCampi As String, Str1 As String, str2 As String
  Dim bol As Boolean
  Dim dclgenLen As Integer
  Dim xa As String
  
  On Error GoTo errSub
  
  LenRiga = 72
  
  dclgenLen = getDclgenLen(IdTable)
  
  getListaCampi_PLI = " DCL    RA_" & Replace(tableName, "-", "_") & "  CHAR(" & dclgenLen & ") ;"
  getListaCampi_PLI = getListaCampi_PLI & vbCrLf & " DCL 1  RR_" & Replace(tableName, "-", "_") & " BASED(ADDR(RA_" & Replace(tableName, "-", "_") & ")) ," & vbCrLf
  
  indice = 1
  Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where IdTable= " & IdTable & " order by ordinale")
  While Not rsColonne.EOF
    NomeCol = Replace(rsColonne!Nome, "-", "_")
    
    Select Case TipoDB
      ''''''''''''''''''''''''''''''''''''''''''''''''''''
      'attualmente solo DB2
      'solo CHAR, SMALLINT, DECIMAL e VARCHAR
      'SQ tmp 2-10-10 - aggiunto ORACLE
      Case "DB2", "ORACLE"
      ''''''''''''''''''''''''''''''''''''''''''''''''''''
        Select Case rsColonne!Tipo
          Case "CHAR", "DATE"
            ListaCampi = Space(2) & "2 " & NomeCol & Space(31 - Len(NomeCol)) & "CHAR(" & rsColonne!lunghezza & "),"
          'SQ tmp 2-10-10
          Case "DECIMAL", "NUMBER"
            If rsColonne!Decimali <> "0" Then
              ListaCampi = Space(2) & "2 " & NomeCol & Space(31 - Len(NomeCol)) & "FIXED DEC(" & rsColonne!lunghezza & "," & rsColonne!Decimali & "),"
            Else
              ListaCampi = Space(2) & "2 " & NomeCol & Space(31 - Len(NomeCol)) & "FIXED DEC(" & rsColonne!lunghezza & "),"
            End If
          Case "SMALLINT", "INTEGER"
            If rsColonne!lunghezza <= 4 Then
              ListaCampi = Space(2) & "2 " & NomeCol & Space(31 - Len(NomeCol)) & "FIXED BIN(15),"
            Else
              ListaCampi = Space(2) & "2 " & NomeCol & Space(31 - Len(NomeCol)) & "FIXED BIN(31),"
            End If
          Case "VARCHAR"
            ListaCampi = Space(2) & "2 " & NomeCol & Space(31 - Len(NomeCol)) & "CHAR(" & rsColonne!lunghezza & ") VAR,"
          'Case "DATE"
          '  ListaCampi = Space(2) & "2 " & NomeCol & Space(31 - Len(NomeCol)) & "PIC X(10)."
          Case Else
            'SQ tmp
            MsgBox "tmp: unexpected column type - " & rsColonne!Tipo, vbInformation, "i-4.Migration"
        End Select
        If Not bol Then
          If Len(ListaCampi) > LenRiga Then
            'MsgBox listaCampi
            k = InStr(ListaCampi, "PIC")
            If k <> 0 Then
              str2 = RTrim(Mid(ListaCampi, 1, k - 1))
              Str1 = RTrim(Mid(ListaCampi, k, Len(ListaCampi)))
              If LenRiga > Len(str2) Then
                ListaCampi = str2 & Space(LenRiga - Len(str2)) & vbCrLf
              Else
                ListaCampi = str2 & " " & vbCrLf
              End If
             
              For x = 1 To Len(str2)
                If Mid(str2, 1, 1) <> " " Then
                  Str1 = Space(x + 2) & Str1
                  If LenRiga > Len(Str1) Then
                    ListaCampi = ListaCampi & Str1 & Space(LenRiga - Len(Str1)) & vbCrLf
                  Else
                    ListaCampi = ListaCampi & Str1 & " " & vbCrLf
                  End If
                  Exit For
                Else
                  str2 = Mid(str2, 2)
                End If
              Next x
            End If
          Else
            ListaCampi = ListaCampi & Space(LenRiga - Len(ListaCampi)) & vbCrLf
          End If
        Else
          If Trim(ListaCampi) <> "" Then
            ListaCampi = ListaCampi & Space(LenRiga - Len(ListaCampi)) & vbCrLf
          End If
          If Trim(Str1) <> "" Then
            If LenRiga > Len(Str1) Then
              ListaCampi = ListaCampi & Str1 & Space(LenRiga - Len(Str1)) & vbCrLf
            Else
              ListaCampi = ListaCampi & Str1 & vbCrLf
            End If
          End If
          If Trim(ListaCampi) <> "" Then
            ListaCampi = ListaCampi & str2 & Space(LenRiga - Len(str2)) & vbCrLf
          End If
          bol = False
        End If
      'SQ tmp 2-10-10
      'Case "ORACLE"
      ' Da gestire
    End Select
    getListaCampi_PLI = getListaCampi_PLI & ListaCampi
    rsColonne.MoveNext
  Wend
  xa = getListaCampi_PLI
  getListaCampi_PLI = Mid(getListaCampi_PLI, 1, InStrRev(getListaCampi_PLI, ",") - 1) & ";"
  'getListaCampi_PLI = getListaCampi_PLI & vbCrLf & "RR_" & Replace(tableName, "-", "_") & " = '' ;"
  Exit Function
errSub:
  MsgBox ERR.Description
  Resume
End Function

Public Sub InsertDbName(rtb As RichTextBox, TbTavole As Recordset)
  Dim TbDBD As Recordset, tbSegm As Recordset
  Dim NomeDB As String

  Select Case m_fun.FnNomeDB
    Case "banca.mty"
      InsertParola rtb, "<DATABASE>", TbTavole!NomeDB
      Exit Sub
    Case Else
      '...
  End Select
  
  If TbTavole!IdOrigine > 0 Then
    Set tbSegm = m_fun.Open_Recordset("select * from PsDLI_Segmenti where IdSegmento= " & TbTavole!IdOrigine)
    If tbSegm.RecordCount Then
      Set TbDBD = m_fun.Open_Recordset("select * from Bs_Oggetti where IdOggetto= " & tbSegm!IdOggetto)
      'NomeDB = Mid$(TbDBD!Nome, 1, Len(TbDBD!Nome) - 4) 'SQ: cos'e' sta roba???????????????
      NomeDB = TbDBD!Nome
      TbDBD.Close
    Else
      NomeDB = TbTavole!NomeDB
    End If
    tbSegm.Close
  Else
    NomeDB = TbTavole!NomeDB
  End If
  InsertParola rtb, "<DATABASE>", NomeDB
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' "Riempie" il template "CPY_BASE_MOVE"
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub GeneraCpyMove(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset, rsColonne As Recordset
  Dim tbxd As Recordset, xdnomeseg As String, xdKeyField As String, xdsrchFields As String, xdDb As String
  Dim ddata As String
  Dim env As Collection
  'SQ: 28-08 (servira' a tutti?!)
  Dim suffissoStorico As String
  Dim isDbindex As Boolean, dbindex As String, iddborigine As Long
  Dim isdispacher As Boolean
  Dim k As Integer, idtablej As Integer
  On Error GoTo errdb
  
  Screen.MousePointer = vbHourglass
  
  '----------------db index
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where IdDB = " & pIndex_DBCorrente)
  If Not rsTabelle.EOF Then
    iddborigine = rsTabelle!IdObjSource
  End If
  rsTabelle.Close
  Set rsTabelle = m_fun.Open_Recordset("select * from bs_oggetti where tipo_dbd = 'IND' and IdOggetto = " & iddborigine)
  If Not rsTabelle.EOF Then
    isDbindex = True
    dbindex = rsTabelle!Nome
  End If
  rsTabelle.Close
  
  Dim rs As Recordset, rsXd As Recordset
 
  If isDbindex Then
    Set rsTabelle = m_fun.Open_Recordset("select s.nome as nomeseg,s.IdOggetto,x.srchFields,x.ddatafields,x.nome as nomexd from PsDli_Segmenti as s,PsDli_XDField as x where x.Lchild = '" & dbindex & "' and s.IdSegmento = x.Idsegmento")
    If Not rsTabelle.EOF Then
      xdnomeseg = rsTabelle!NomeSeg
      xdKeyField = rsTabelle!nomexd
      xdsrchFields = rsTabelle!srchFields
      ddata = rsTabelle!ddatafields
      Set tbxd = m_fun.Open_Recordset("select nome from Bs_Oggetti where IdOggetto = " & rsTabelle!IdOggetto)
      If Not tbxd.EOF Then
        xdDb = tbxd!Nome
      End If
      tbxd.Close
    End If
    iddborigine = rsTabelle!IdOggetto
    rsTabelle.Close
    
    Set rsTabelle = m_fun.Open_Recordset("select idDB,NomeDb from " & PrefDb & "_Tabelle where idObjSource = " & iddborigine)
    If Not rsTabelle.EOF Then
      pIndex_DBCorrente = rsTabelle!idDB
      pNome_DBCorrente = rsTabelle!NomeDB
    End If
    rsTabelle.Close
    pNome_DBCorrente = xdDb
  End If
  '-------------- dbindex
  k = 1
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB= " & pIndex_DBCorrente)
  While Not rsTabelle.EOF
    If idtablej <> rsTabelle!idTableJoin Or IsNull(rsTabelle!idTableJoin) Then
      k = 1
    End If
    If rsTabelle!idTableJoin <> 0 Then
      k = k + 1
      idtablej = rsTabelle!idTableJoin
    End If

    '''''''''''''''''''''''''''''''''''''
    ' Per ogni TABELLA associata al DBD:
    '''''''''''''''''''''''''''''''''''''
    Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto,Parent from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto,Parent from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    End If
    If rsSegmenti.RecordCount > 0 Then
     'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
      '****************
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE") <> "" Then
         RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE"
      Else
         DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE"" not found."
         Screen.MousePointer = vbDefault
         Exit Sub
      End If

      'RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE"
      '*****************
      
      changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
      If Not isDbindex Then
        'SQ 2-10-07 tmp: nome "LABEL" - serve un EnvParam o qualcosa di diverso: fra dispatcher/occurs ci sono duplicati
        'Per le DISP, le routine usano la "<TABLE-NAME-PERFORM>"
        If InStr(rsTabelle!Tag, "DISPATCHER") Then
          changeTag RtbFile, "<NOME-SEGMENTO>", Replace(rsTabelle!Nome, "_", "-")
        Else
          changeTag RtbFile, "<NOME-SEGMENTO>", rsSegmenti!Nome & suffissoStorico
        End If
      Else
         changeTag RtbFile, "<NOME-SEGMENTO>", rsSegmenti!Nome & "-" & xdKeyField & suffissoStorico
      End If
      changeTag RtbFile, "<NOME-TABELLA>", Replace(rsTabelle!Nome, "_", "-")
      
      'SQ 10-07-06 SEGMENTO/TABELLA "PADRE"
      setParentInfo rsSegmenti!Parent, rsSegmenti!IdOggetto
      
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Trattamento Campi:
      ' Ex soluzione: solo colonne "originali": con idTable=idTableJoin (no ereditate) e idCmp > 0 (no extra-columns)
      ' Nuova: tutte colonne (si controlla se ho il con template associato nelle specifiche routine...)
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'Set rsColonne = m_fun.Open_Recordset("select * from DMDB2_Colonne where IdTable= " & rsTabelle!IdTable & " AND idTableJoin = idTable AND idCmp > 0 order by ordinale")
      'stefanopippo: 17/08/2005: il nuovo commento era giusto, per� poi hai fatto diversamente; perch�?
      'cos� va bene per banca????
      'Set rsColonne = m_fun.Open_Recordset("select * from DMDB2_Colonne where IdTable= " & RsTabelle!IdTable & " AND idTableJoin = idTable order by ordinale")
      Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where IdTable= " & rsTabelle!IdTable & " order by ordinale")
      If rsColonne.RecordCount Then
        'SQ: 28-08
        'silvia 6-5-2008
        '''changeTag RtbFile, "<MOVE-DLI2RDBMS>", getMoveDli2Rdbms(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome & suffissoStorico) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        changeTag RtbFile, "<MOVE-DLI2RDBMS>", getMoveDli2Rdbms(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome & suffissoStorico, rsSegmenti!IdSegmento) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        rsColonne.MoveFirst
        'SQ: 28-08 AC 030807
        If Not isDbindex Then
          changeTag RtbFile, "<MOVE-RDBMS2DLI>", getMoveRdbms2Dli(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome & suffissoStorico, rsSegmenti!IdSegmento) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        Else
          changeTag RtbFile, "<MOVE-RDBMS2DLI>", getMoveRdbms2DliXD(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome & suffissoStorico, ddata, xdKeyField)
        End If
        ' stefano da migliorare con una funzione ad hoc, che serve anche per il programma
        Dim rsArea As Recordset
        Set rsArea = m_fun.Open_Recordset("select NomeCmp from MgRel_SegAree where Stridarea = 1 and idSegmento = " & rsSegmenti!IdSegmento)
        If rsArea.RecordCount > 0 Then
          changeTag RtbFile, "<NOME-AREA>", rsArea!NomeCmp
        End If
        rsArea.Close
      End If
            
      Set env = New Collection  'resetto;
      env.Add rsTabelle
      env.Add rsSegmenti
      env.Add k
      If Len(MoveCopyParam) Then
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(MoveCopyParam, "tabelle", env), 1
      Else
        MsgBox "No value found for 'MOVE-COPY' environment parameter.", vbExclamation
      End If
      rsColonne.Close
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  
  Screen.MousePointer = vbDefault
  Exit Sub
errdb:
    Screen.MousePointer = vbDefault
    Select Case ERR.Number
    Case 75
      'VB non ha trovato il file
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case 7575
      ' non ha trovato il file (err.number "forzato" da noi...)
      ERR.Raise 75, , ERR.Description
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Sub
Public Sub GeneraCpyMove_PLI(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset, rsColonne As Recordset
  Dim env As Collection
  'SQ: 28-08 (servira' a tutti?!)
  Dim suffissoStorico As String
  Dim nomeSegm As String
  Dim rs As Recordset
  On Error GoTo errdb
  
  Screen.MousePointer = vbHourglass
  
  'tilvia 04-05-2009
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
  pIndex_DBDCorrente = rs!iddbor
  rs.Close

  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB= " & pIndex_DBCorrente)
  While Not rsTabelle.EOF
    '''''''''''''''''''''''''''''''''''''
    ' Per ogni TABELLA associata al DBD:
    '''''''''''''''''''''''''''''''''''''
    Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto,Parent from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    If rsSegmenti.RecordCount = 0 Then
      'TILVIA
      Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto,Parent from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      nomeSegm = rsSegmenti!Nome
    Else
      nomeSegm = rsSegmenti!Nome
    End If
    If rsSegmenti.RecordCount > 0 Then
       'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
      '****************
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE") <> "" Then
         RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE"
      Else
         DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE"" not found."
         Screen.MousePointer = vbDefault
         Exit Sub
      End If
      '*****************
'      RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE"
      
      changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
      changeTag RtbFile, "<NOME-SEGMENTO>", Replace(rsSegmenti!Nome, "-", "_") & suffissoStorico
      changeTag RtbFile, "<NOME-TABELLA>", rsTabelle!Nome
      
      'SQ 10-07-06 SEGMENTO/TABELLA "PADRE"
      setParentInfo rsSegmenti!Parent, rsSegmenti!IdOggetto
      
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Trattamento Campi:
      ' Ex soluzione: solo colonne "originali": con idTable=idTableJoin (no ereditate) e idCmp > 0 (no extra-columns)
      ' Nuova: tutte colonne (si controlla se ho il con template associato nelle specifiche routine...)
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'Set rsColonne = m_fun.Open_Recordset("select * from DMDB2_Colonne where IdTable= " & rsTabelle!IdTable & " AND idTableJoin = idTable AND idCmp > 0 order by ordinale")
      'stefanopippo: 17/08/2005: il nuovo commento era giusto, per� poi hai fatto diversamente; perch�?
      'cos� va bene per banca????
      'Set rsColonne = m_fun.Open_Recordset("select * from DMDB2_Colonne where IdTable= " & RsTabelle!IdTable & " AND idTableJoin = idTable order by ordinale")
      Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where IdTable= " & rsTabelle!IdTable & " order by ordinale")
      If rsColonne.RecordCount Then
        'SQ: 28-08
        changeTag RtbFile, "<MOVE-DLI2RDBMS>", getMoveDli2Rdbms_PLI(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome & suffissoStorico, rsSegmenti!IdSegmento, nomeSegm) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        rsColonne.MoveFirst
        'SQ: 28-08
        changeTag RtbFile, "<MOVE-RDBMS2DLI>", getMoveRdbms2Dli(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome & suffissoStorico, rsSegmenti!IdSegmento, "PLI")      'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        
        ' stefano da migliorare con una funzione ad hoc, che serve anche per il programma
        Dim rsArea As Recordset
        Set rsArea = m_fun.Open_Recordset("select NomeCmp from MgRel_SegAree where Stridarea = 1 and idSegmento = " & rsSegmenti!IdSegmento)
        If rsArea.RecordCount > 0 Then
          changeTag RtbFile, "<NOME-AREA>", Replace(rsArea!NomeCmp, "-", "_")
        End If
        rsArea.Close
      End If
            
      Set env = New Collection  'resetto;
      env.Add rsTabelle
      env.Add rsSegmenti
      If Len(MoveCopyParam) Then
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(MoveCopyParam, "tabelle", env), 1
      Else
        MsgBox "No value found for 'MOVE-COPY' environment parameter.", vbExclamation
      End If
      rsColonne.Close
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  
  Screen.MousePointer = vbDefault
  Exit Sub
errdb:
    Screen.MousePointer = vbDefault
    Select Case ERR.Number
    Case 75
      'VB non ha trovato il file
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case 7575
      ' non ha trovato il file (err.number "forzato" da noi...)
      ERR.Raise 75, , ERR.Description
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''
' "Riempie" il template "DMDB2_CPY_BASE_MOVE.cpy"
'''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub GeneraCpyLoad(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset, rsColonne As Recordset
  Dim fileName As String
  Dim env As Collection
  Dim k As Integer, idtablej As Integer
  k = 1
  On Error GoTo errdb
  
  Screen.MousePointer = vbHourglass
  
  RtbFile.text = ""
  
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB= " & pIndex_DBCorrente)
  While Not rsTabelle.EOF
    If idtablej <> rsTabelle!idTableJoin Or IsNull(rsTabelle!idTableJoin) Then
      k = 1
    End If
    If rsTabelle!idTableJoin <> 0 Then
      k = k + 1
      idtablej = rsTabelle!idTableJoin
    End If

    '''''''''''''''''''''''''''''''''''''
    ' Per ogni TABELLA associata al DBD:
    '''''''''''''''''''''''''''''''''''''
    Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    End If
    If rsSegmenti.RecordCount > 0 Then
      
       'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
      '****************
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_LOAD") <> "" Then
         RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_LOAD"
      Else
         DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_LOAD"" not found."
         Screen.MousePointer = vbDefault
         Exit Sub
      End If
      
'      RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_LOAD"
      
      '*****************
      changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
      changeTag RtbFile, "<NOME-SEGMENTO>", rsSegmenti!Nome
      changeTag RtbFile, "<NOME-TABELLA>", Replace(rsTabelle!Nome, "_", "-")
      
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Trattamento Campi:
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where IdTable= " & rsTabelle!IdTable & " AND routLoad <> null AND routLoad <> '' order by ordinale")
      If rsColonne.RecordCount Then
        Do While Not rsColonne.EOF
          If rsColonne!Tipo = "TIMESTAMP" And InStr(rsColonne!Nome, "TMS_FINE") Then
            changeTag RtbFile, "<TMS-FINE-VALID>", rsColonne!NomeCmp & " OF RD-" & rsSegmenti!Nome
            Exit Do
'          Else
'            changeTag RtbFile, "<TMS-FINE-VALID>", "SPACE"
          End If
          rsColonne.MoveNext
          If rsColonne.EOF Then
            changeTag RtbFile, "<TMS-FINE-VALID>", "SPACE"
          End If
        Loop
        rsColonne.Close
      End If
      Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where IdTable= " & rsTabelle!IdTable & " AND routLoad <> null AND routLoad <> '' order by ordinale")
      If rsColonne.RecordCount Then
      'SP exec dli
        'changeTag rtbFile, "<MOVE-DLI2RDBMS>.", getMoveLoad(rtbFile, rsColonne, RsTabelle!Nome, rsSegmenti!Nome) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        changeTag RtbFile, "<MOVE-DLI2RDBMS>", getMoveLoad(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome, rsSegmenti!IdSegmento) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        ' stefano da migliorare con una funzione ad hoc, che serve anche per il programma
        Dim rsArea As Recordset
        Set rsArea = m_fun.Open_Recordset("select NomeCmp from MgRel_SegAree where Stridarea = 1 and idSegmento = " & rsSegmenti!IdSegmento)
        If rsArea.RecordCount Then
          changeTag RtbFile, "<NOME-AREA>", rsArea!NomeCmp
        End If
        rsArea.Close
      End If
      
      'Nome DCP
      Set env = New Collection  'resetto;
      env.Add rsTabelle!Nome
      env.Add rsSegmenti
      changeTag RtbFile, "<PGM-NAME>", getEnvironmentParam(LoadProgramParam, "tabelle", env)
      RtbFile.text = Replace(RtbFile.text, "_", "-")
                    
      'Nome copy L
      Set env = New Collection  'resetto;
      env.Add rsTabelle
      env.Add rsSegmenti
      env.Add k
      RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(LoadCopyParam, "tabelle", env), 1
      rsColonne.Close
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  Screen.MousePointer = vbDefault
  Exit Sub
errdb:
  Screen.MousePointer = vbDefault
  Select Case ERR.Number
    Case 75
      'VB non ha trovato il file

      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_LOAD") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_LOAD"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case 7575
      'non ha trovato il file (err.number "forzato" da noi...)
      ERR.Raise 75, , ERR.Description
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Sub

Function getMoveLoad(RtbFile As RichTextBox, rsColonne As Recordset, nomeTable As String, nomeSegmento As String, IdSegmento As Long) As String
  Dim rsCmp As Recordset, rsCmp2 As Recordset
  Dim rtbString As String
  Dim NomeCampo As String
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
  
  On Error GoTo loadErr
  While Not rsColonne.EOF
    'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & rsColonne!RoutLoad
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & rsColonne!RoutLoad
    '''''''''''''''''''''''''''''''''
    ' RIEMPIO IL TEMPLATE!
    '''''''''''''''''''''''''''''''''
    changeTag RtbFile, "<NOME-COL>", Replace(rsColonne!Nome, "_", "-")
    changeTag RtbFile, "<LEN-COL>", rsColonne!lunghezza
    NomeCampo = IIf(IsNull(rsColonne!NomeCmp), "", rsColonne!NomeCmp)
    changeTag RtbFile, "<NOME-CAMPO>", Replace(NomeCampo, "_", "-")
    changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "_", "-")
    changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 14-06-06
    ' Gestione Chiavi ereditate
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Right(rsColonne!Tag, 6) = "OCCURS" Then ' (potrei avere "AUTOMATIC-OCCURS")
      'SQ - TMP: questa gestione particolare per le OCCURS � un po' forzata... meglio cambiare template/tag!
      changeTag RtbFile, "<NOME-COL-PARENT>", Replace(rsColonne!Nome, "_", "-") 'vincolo: stesso nome (accettabile)
      'VIRGILIO 26/2/2007
''      If dbType = "DB2" Then
''        Set rsCmp = m_fun.Open_Recordset("select Nome from DMDB2_Tabelle where IdTable=" & rsColonne!idTableJoin)
''      Else
''        Set rsCmp = m_fun.Open_Recordset("select Nome from DMORC_Tabelle where IdTable=" & rsColonne!idTableJoin)
''      End If
      Set rsCmp = m_fun.Open_Recordset("select Nome from " & PrefDb & "_Tabelle where IdTable = " & rsColonne!idTableJoin)
      If rsCmp.RecordCount Then
        changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Nome, "_", "-")
      End If
      rsCmp.Close
    Else
      If Right(rsColonne!Tag, 10) = "DISPATCHER" Then ' (potrei avere "AUTOMATIC-DISPATCHER")
        'SQ - TMP: questa gestione particolare per le OCCURS � un po' forzata... meglio cambiare template/tag!
        'changeTag rtbFile, "<NOME-COL-PARENT>", Replace(rsColonne!Nome, "_", "-") 'vincolo: stesso nome (accettabile)
        'Set rsCmp = m_fun.Open_Recordset("select Nome from DMDB2_Tabelle where IdTable=" & rsColonne!IdTableJoin)
        'If rsCmp.RecordCount Then
        '  changeTag rtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Nome, "_", "-")
        'End If
        'rsCmp.Close
        'Gestione colonna "FIELD"
        If rsColonne!IdCmp > 0 Then '� l'idField!
          'FIELD
          Set rsCmp = m_fun.Open_Recordset("Select posizione,lunghezza from PsDli_Field where idField = " & rsColonne!IdCmp & " AND IdSegmento=" & IdSegmento)
          If rsCmp.RecordCount Then
            'Attenzione, entra anche per le ereditate... non sono distinguibili!
            changeTag RtbFile, "<POS-SEGM-FIELD>", rsCmp!Posizione
            changeTag RtbFile, "<LEN-SEGM-FIELD>", rsCmp!lunghezza
          End If
        Else
          'XDFIELD: gestire!
          Set rsCmp = m_fun.Open_Recordset("Select srchFields from PsDli_XDField where idXDField = " & rsColonne!IdCmp)
        End If
        rsCmp.Close
      End If
      If rsColonne!idTableJoin <> rsColonne!IdTable Then
        'Set rsCmp = m_fun.Open_Recordset("select b.nome as nome from DMDB2_Tabelle as a,PsDli_Segmenti as b where a.IdTable = " & rsColonne!IdTableJoin & " And a.IdOrigine = b.IdSegmento")
        Set rsCmp = m_fun.Open_Recordset("select b.nome as nome from MgData_Cmp as a,PsDli_Segmenti as b where a.IdCmp = " & rsColonne!IdCmp & " And a.IdSegmento = b.IdSegmento")
        If rsCmp.RecordCount Then
          changeTag RtbFile, "<NOME-SEGM-PARENT>", rsCmp!Nome
        End If
        rsCmp.Close
        
        If rsColonne!idTableJoin Then
          'Template di tipo "INHERITED
                   'SQ 4-4-08 - COME FA AD ESSERCI ANCORA QUESTO PROBLEMA (idCmp non univoco)?!. Aggiunto "a.IdTable=a.idTableJoin"
          Set rsCmp = m_fun.Open_Recordset("select a.Nome as colonna, b.nome as tabella " & _
                    "from " & PrefDb & "_Colonne as a," & PrefDb & "_Tabelle as b where " & _
                    "a.IdTable = b.IdTable AND " & _
                    "a.IdTable = " & rsColonne!idTableJoin & " And a.IdTable = a.idTableJoin And " & _
                    "a.IdCmp = " & rsColonne!IdCmp & " and a.routkey <> null")
          If rsCmp.RecordCount Then
            '"MOVE " & Replace(rsOrigine(0), "_", "-") & " OF RR-" & rsOrigine(1) & " TO " & vbCrLf
            changeTag RtbFile, "<NOME-COL-PARENT>", Replace(rsCmp!colonna, "_", "-")
            changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Tabella, "_", "-")
          End If
          rsCmp.Close
        End If
      End If
    End If

    If Not IsNull(rsColonne!NomeCmp) Then
      Set rsCmp = m_fun.Open_Recordset("select IdSegmento,Byte,Ordinale,posizione,livello from MgData_Cmp where IdCmp = " & rsColonne!IdCmp & " and IdSegmento=" & IdSegmento)
      
      'stefano: evito il loop
      If Not rsCmp.EOF Then
        changeTag RtbFile, "<LEN-FIELD>", IIf(IsNull(rsCmp!Byte), "", rsCmp!Byte)
        changeTag RtbFile, "<POS-FIELD>", rsCmp!Posizione
          
        ' Mauro 27/11/2008
        'Set rsCmp2 = m_fun.Open_Recordset("select Nome,LIVELLO from MgData_Cmp where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!Livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 and decimali = 0 order by Ordinale desc")
        Set rsCmp2 = m_fun.Open_Recordset("select Nome,LIVELLO, posizione from MgData_Cmp " & _
                      "where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!livello & " AND " & _
                      "ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 and decimali = 0 order by Ordinale desc")
        If rsCmp2.RecordCount Then
          'SQ 10-07-06
          'SE IL GRUPPO E' IL LIVELLO 01, NON CE L'ABBIAMO NELLA COPY T! E' LO "RD-..."
          If rsCmp2!livello = 1 Then
            changeTag RtbFile, "<GROUP-NAME>", "RD-" & nomeSegmento 'UTILIZZARE IL PARAMETRO DI SISTEMA!!!!!
          Else
            changeTag RtbFile, "<GROUP-NAME>", rsCmp2!Nome
          End If
          ' Mauro 27/11/2008
          changeTag RtbFile, "<POS-FIELD-SUB>", (rsCmp!Posizione - rsCmp2!Posizione) + 1
        Else
        'SQ 05-08-05
           m_fun.WriteLog "getMoveLoad: tmp-error: no group field found!"
        End If
        rsCmp2.Close
      End If
      rsCmp.Close
    End If
          
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' GESTIONE XDFIELD... generalizzare... solo banca...
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'changeTag rtbFile, "<XDFIELD-MOVE>", getXdfieldMove_BPER(rsColonne!Nome)
    'If Right(rsColonne!RoutLoad, 4) = "EXIT" Then
    'changeTag rtbFile, "<CONDIZIONE>", getCondizione_BPER(rtbFile, rsColonne!Nome)
    
    getMoveLoad = getMoveLoad & RtbFile.text & vbCrLf  'formattare il primo...
                    
    rsColonne.MoveNext
  Wend
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & rsColonne!RoutLoad & """ not found."
    Case 7575
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Function

Public Sub GeneraCpyLoad_PLI(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset, rsColonne As Recordset
  Dim fileName As String
  Dim env As Collection
  Dim dbString As String
  Dim nomeSegm As String
  Dim rs As Recordset
  On Error GoTo errdb
  
  Screen.MousePointer = vbHourglass
  
  'tilvia 04-05-2009
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
  pIndex_DBDCorrente = rs!iddbor
  rs.Close

  RtbFile.text = ""
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB = " & pIndex_DBCorrente)
  While Not rsTabelle.EOF
    '''''''''''''''''''''''''''''''''''''
    ' Per ogni TABELLA associata al DBD:
    '''''''''''''''''''''''''''''''''''''
    Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select nome,idSegmento,idOggetto from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      nomeSegm = rsSegmenti!Nome
    Else
      nomeSegm = rsSegmenti!Nome
    End If
    
    If rsSegmenti.RecordCount Then
       'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
      '****************
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_LOAD") <> "" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_LOAD"
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_LOAD"" not found."
        Screen.MousePointer = vbDefault
        Exit Sub
      End If
      
    '  RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_LOAD"
      '*****************
    
      'dbString = "/*    " & pNome_DBCorrente
      'changeTag RtbFile, "<DBD-NAME>", dbString & Space(68 - Len(dbString)) & "*/"
      changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
      
      changeTag RtbFile, "<NOME-SEGMENTO>", Replace(rsSegmenti!Nome, "-", "_")
      changeTag RtbFile, "<NOME-TABELLA>", Replace(rsTabelle!Nome, "_", "-")

      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Trattamento Campi:
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_Colonne where IdTable= " & rsTabelle!IdTable & " AND routLoad <> null AND routLoad <> '' order by ordinale")
      If rsColonne.RecordCount Then
      'SP exec dli
        'changeTag rtbFile, "<MOVE-DLI2RDBMS>.", getMoveLoad(rtbFile, rsColonne, RsTabelle!Nome, rsSegmenti!Nome) 'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        changeTag RtbFile, "<MOVE-DLI2RDBMS>", getMoveLoad_PLI(RtbFile, rsColonne, rsTabelle!Nome, rsSegmenti!Nome, rsSegmenti!IdSegmento, nomeSegm)          'attenzione: sarebbe suff. rsColonne... ma velocizziamo un po'...
        ' stefano da migliorare con una funzione ad hoc, che serve anche per il programma
        Dim rsArea As Recordset
        Set rsArea = m_fun.Open_Recordset("select NomeCmp from MgRel_SegAree where Stridarea = 1 and idSegmento = " & rsSegmenti!IdSegmento)
        If rsArea.RecordCount Then
          changeTag RtbFile, "<NOME-AREA>", rsArea!NomeCmp
        End If
        rsArea.Close
      End If
      'DCP name:
      Set env = New Collection  'resetto;
      env.Add rsTabelle!Nome
      env.Add rsSegmenti
      changeTag RtbFile, "<PGM-NAME>", getEnvironmentParam(LoadProgramParam, "tabelle", env)
      'Copy L:
      Set env = New Collection  'resetto;
      env.Add rsTabelle
      env.Add rsSegmenti
      On Error GoTo errdb
      RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(LoadCopyParam, "tabelle", env), 1
      rsColonne.Close
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  Screen.MousePointer = vbDefault
  Exit Sub
errdb:
  Screen.MousePointer = vbDefault
  Select Case ERR.Number
    Case 75
      'VB non ha trovato il file
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_LOAD") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_LOAD"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case 7575
      ' non ha trovato il file (err.number "forzato" da noi...)
      ERR.Raise 75, , ERR.Description
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Sub

'silvia 6-5-2008 aggiunto idsegmento
''Function getMoveDli2Rdbms(RtbFile As RichTextBox, rsColonne As Recordset, nomeTable As String, nomeSegmento As String,  Optional pgtype = "") As String
Function getMoveDli2Rdbms(RtbFile As RichTextBox, rsColonne As Recordset, nomeTable As String, nomeSegmento As String, IdSegmento As Long, Optional pgtype = "") As String
  Dim rtbString As String ', parameter As String
  Dim rsCmp As Recordset, rsCmp2 As Recordset
  Dim i As Integer
  'Dim env As Collection
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
  
  On Error GoTo loadErr
  While Not rsColonne.EOF
    If Not IsNull(rsColonne!RoutWrite) And Len(rsColonne!RoutWrite) Then
      'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & rsColonne!RoutWrite
      If pgtype = "PLI" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\" & rsColonne!RoutWrite
      Else
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & rsColonne!RoutWrite
      End If
      '''''''''''''''''''''''''''''''''
      ' RIEMPIO IL TEMPLATE!
      '''''''''''''''''''''''''''''''''
      'usare un tag piu' appropriato... (per replace...)
      If Not pgtype = "PLI" Then
       changeTag RtbFile, "<NOME-COL>", Replace(rsColonne!Nome, "_", "-")
       changeTag RtbFile, "<REVKEY>", rsColonne!Nome
       changeTag RtbFile, "<LEN-COL>", rsColonne!lunghezza
       changeTag RtbFile, "<NOME-CAMPO>", IIf(IsNull(rsColonne!NomeCmp), "", rsColonne!NomeCmp)
       changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "_", "-")
       changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
      Else
       changeTag RtbFile, "<NOME-COL>", rsColonne!Nome
       changeTag RtbFile, "<LEN-COL>", rsColonne!lunghezza
       'changeTag rtbFile, "<NOME-CAMPO>", IIf(IsNull(rsColonne!NomeCmp), "", Replace(rsColonne!NomeCmp, "-", "_"))
       changeTag RtbFile, "<NOME-CAMPO>", Replace(rsColonne!NomeCmp & "", "-", "_")
       changeTag RtbFile, "<NOME-TAB>", nomeTable
       changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
      End If
      changeTag RtbFile, "<NOME-TABELLA>", nomeTable
'        Dim rsArea As Recordset
'        Set rsArea = m_fun.Open_Recordset("select NomeCmp from MgRel_SegAree where Stridarea = 1 and idSegmento = " & rsSegmenti!IdSegmento)
'        If rsArea.RecordCount > 0 Then
'          changeTag rtbFile, "<NOME-AREA>", rsArea!NomeCmp
'        End If
'        rsArea.close
      
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 14-06-06
    ' Gestione Chiavi ereditate
    ' SQ - ATTENZIONE! - UNIFORMARE LA GESTIONE CON LE NUOVE VARIABILI GLOBALI
    '                    idSegmentoParent,idTableParent,...
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If Right(rsColonne!Tag, 6) = "OCCURS" Then ' (potrei avere "AUTOMATIC-OCCURS")
        'SQ - TMP: questa gestione particolare per le OCCURS � un po' forzata... meglio cambiare template/tag!
        'stefano: 16/7/2006 si chiama diverso....
        'changeTag rtbFile, "<NOME-COL-PARENT>", Replace(rsColonne!Nome, "_", "-") 'vincolo: stesso nome (accettabile)
        Dim daCamb As Recordset
        Set daCamb = m_fun.Open_Recordset("select Nome from " & PrefDb & "_Colonne where idcmp = " & rsColonne!IdCmp & " and IdTable=" & rsColonne!idTableJoin)
        changeTag RtbFile, "<NOME-COL-PARENT>", Replace(daCamb!Nome, "_", "-")
        daCamb.Close
        Set rsCmp = m_fun.Open_Recordset("select Nome from " & PrefDb & "_Tabelle where IdTable=" & rsColonne!idTableJoin)
        If rsCmp.RecordCount Then
          changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Nome, "_", "-")
        End If
        rsCmp.Close
        
        'tilvia 2014
          If rsColonne!IdCmp = -1 Then
            changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(nomeTableParent, "_", "-")
            If rsColonne!RoutWrite = "dli2rdbms_REVKEY" Then
                Set rsCmp = m_fun.Open_Recordset("select a.Nome as colonna from " & PrefDb & "_Colonne as a where " & _
                                                 "a.IdTable <> a.Idtablejoin  and a.IdTable = " & rsColonne!IdTable)
                If rsCmp.RecordCount Then
                  'rsCmp.MoveFirst
                  While Not rsCmp.EOF

                  '"MOVE " & Replace(rsOrigine(0), "_", "-") & " OF RR-" & rsOrigine(1) & " TO " & vbCrLf
                    '<COLINERIT(i)>     = :RR-<NOME-TAB>.<FIELDINERIT(i)>
                    If rsCmp.EOF Then
                      changeTag RtbFile, "<COLINERIT(i)>", rsCmp!colonna & " =:RR-<NOME-TAB>." & Replace(rsCmp!colonna, "_", "-")
                    Else
                      changeTag RtbFile, "<COLINERIT(i)>", rsCmp!colonna & " =:RR-<NOME-TAB>." & Replace(rsCmp!colonna, "_", "-") & " AND "
                    End If
                    'changeTag RtbFile, "<FIELDINERIT(i)>", Replace(rsCmp!colonna, "_", "-")
                    changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "_", "-")
                    rsCmp.MoveNext
                    
                  Wend
                End If
                changeTag RtbFile, "AND " & vbCrLf & "    <COLINERIT(i)>", ""
                rsCmp.Close
            
            End If
            
          End If
        '''
      Else
        If Right(rsColonne!Tag, 10) = "DISPATCHER" Then ' (potrei avere "AUTOMATIC-DISPATCHER")
          'SQ - TMP: questa gestione particolare per le OCCURS � un po' forzata... meglio cambiare template/tag!
          'changeTag rtbFile, "<NOME-COL-PARENT>", Replace(rsColonne!Nome, "_", "-") 'vincolo: stesso nome (accettabile)
          'Set rsCmp = m_fun.Open_Recordset("select Nome from DMDB2_Tabelle where IdTable=" & rsColonne!IdTableJoin)
          'If rsCmp.RecordCount Then
          '  changeTag rtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Nome, "_", "-")
          'End If
          'rsCmp.Close
          'Gestione colonna "FIELD"
          If rsColonne!IdCmp > 0 Then '� l'idField!
            'FIELD
            Set rsCmp = m_fun.Open_Recordset("Select posizione,lunghezza from PsDli_Field where idField = " & rsColonne!IdCmp)
            If rsCmp.RecordCount Then
              'Attenzione, entra anche per le ereditate... non sono distinguibili!
              changeTag RtbFile, "<POS-SEGM-FIELD>", rsCmp!Posizione
              changeTag RtbFile, "<LEN-SEGM-FIELD>", rsCmp!lunghezza
            End If
          Else
            'XDFIELD: gestire!
            Set rsCmp = m_fun.Open_Recordset("Select srchFields from PsDli_XDField where idXDField = " & rsColonne!IdCmp)
          End If
          rsCmp.Close
        End If
        If rsColonne!idTableJoin <> rsColonne!IdTable Then
          Set rsCmp = m_fun.Open_Recordset("select b.nome as nome from MgData_Cmp as a,PsDli_Segmenti as b where a.IdCmp = " & rsColonne!IdCmp & " And a.IdSegmento = b.IdSegmento")
          If rsCmp.RecordCount Then
            changeTag RtbFile, "<NOME-SEGM-PARENT>", rsCmp!Nome
          End If
          rsCmp.Close
          
          If rsColonne!idTableJoin Then
            'Template di tipo "INHERITED
            Set rsCmp = m_fun.Open_Recordset("select a.Nome as colonna,b.nome as tabella from " & PrefDb & "_Colonne as a," & PrefDb & "_Tabelle as b where " & _
                                             "a.IdTable = b.IdTable And a.IdTable = " & rsColonne!idTableJoin & " And " & _
                                             "a.IdCmp = " & rsColonne!IdCmp)
            If rsCmp.RecordCount Then
              '"MOVE " & Replace(rsOrigine(0), "_", "-") & " OF RR-" & rsOrigine(1) & " TO " & vbCrLf
              changeTag RtbFile, "<NOME-COL-PARENT>", Replace(rsCmp!colonna, "_", "-")
              changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Tabella, "_", "-")
            End If
            rsCmp.Close
          End If
        Else
          'Colonne Aggiunte (Es. contatore...) - FARE "TAG" DEDICATO!
          If rsColonne!IdCmp = -1 Then
            changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(nomeTableParent, "_", "-")
            If rsColonne!RoutWrite = "dli2rdbms_REVKEY" Then
                Set rsCmp = m_fun.Open_Recordset("select a.Nome as colonna from " & PrefDb & "_Colonne as a where " & _
                                                 "a.IdTable <> a.Idtablejoin  and a.IdTable = " & rsColonne!IdTable)
                If rsCmp.RecordCount Then
                  'rsCmp.MoveFirst
                  While Not rsCmp.EOF

                  '"MOVE " & Replace(rsOrigine(0), "_", "-") & " OF RR-" & rsOrigine(1) & " TO " & vbCrLf
                    '<COLINERIT(i)>     = :RR-<NOME-TAB>.<FIELDINERIT(i)>
                    If rsCmp.EOF Then
                      changeTag RtbFile, "<COLINERIT(i)>", rsCmp!colonna & " =:RR-<NOME-TAB>." & Replace(rsCmp!colonna, "_", "-")
                    Else
                      changeTag RtbFile, "<COLINERIT(i)>", rsCmp!colonna & " =:RR-<NOME-TAB>." & Replace(rsCmp!colonna, "_", "-") & " AND "
                    End If
                    'changeTag RtbFile, "<FIELDINERIT(i)>", Replace(rsCmp!colonna, "_", "-")
                    changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "_", "-")
                    rsCmp.MoveNext
                    
                  Wend
                End If
                changeTag RtbFile, "AND " & vbCrLf & "    <COLINERIT(i)>", ""
                rsCmp.Close
            End If
          End If
        End If
      End If
      
      If Not IsNull(rsColonne!NomeCmp) Then
        'silvia 6-5-2008: ci vuole anche l'idsegmento
        'Set rsCmp = m_fun.Open_Recordset("select IdSegmento,Byte,ordinale,Posizione,livello from MgData_Cmp where IdCmp = " & rsColonne!IdCmp)
        Set rsCmp = m_fun.Open_Recordset("select IdSegmento,Byte,ordinale,Posizione,livello from MgData_Cmp where IdCmp = " & rsColonne!IdCmp & " and IdSegmento=" & IdSegmento)

        If rsCmp.RecordCount Then
          changeTag RtbFile, "<LEN-FIELD>", IIf(IsNull(rsCmp!Byte), "", rsCmp!Byte)
          changeTag RtbFile, "<POS-FIELD>", rsCmp!Posizione
          'SQ: 25-08
          'Set rsCmp2 = m_fun.Open_Recordset("select Nome from MgData_Cmp where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!Livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 order by Ordinale desc")
          Set rsCmp2 = m_fun.Open_Recordset("select Nome,livello,ordinale from MgData_Cmp where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 and decimali = 0 order by Ordinale desc")
          If rsCmp2.RecordCount Then
            'SQ 10-07-06
            'SE IL GRUPPO E' IL LIVELLO 01, NON CE L'ABBIAMO NELLA COPY T! E' LO "RD-..."
            If rsCmp2!livello = 1 Then
              changeTag RtbFile, "<GROUP-NAME>", "RD-" & nomeSegmento 'UTILIZZARE IL PARAMETRO DI SISTEMA!!!!!
            Else
              changeTag RtbFile, "<GROUP-NAME>", rsCmp2!Nome
            End If
            'SQ: 24-08
            If m_fun.FnNomeDB = "banca.mty" Then
              If rsCmp2!livello > 1 Then  'probabilmente si puo' aggiungere alla query sopra...
                'controllare prima di invocare...
                changeTag RtbFile, "<MOVE-SPACE-BROTHERS>", getMoveSpaceBrothers_BPER(getBrotherColumns_BPER(rsCmp, rsColonne!IdTable, rsCmp2!Ordinale))
              End If
            End If
          Else
            m_fun.WriteLog "getMoveDli2Rdbms: tmp-error: no group field found!"
          End If
          rsCmp2.Close
        Else
          '...
        End If
        rsCmp.Close
      End If
      
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' GESTIONE XDFIELD... generalizzare... solo banca...
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If m_fun.FnNomeDB = "banca.mty" Then
        If Left(rsColonne!Nome, 3) = "KY_" And rsColonne!IdCmp = -1 Then 'scremo un po' di lavoro... hanno -1 (per la banca)
          changeTag RtbFile, "<XDFIELD-MOVE>", getXdfieldMove_BPER(rsColonne!Nome)
          'gestione "forzata" (orrenda)...
          If Right(rsColonne!RoutWrite, 4) = "EXIT" Then
            changeTag RtbFile, "<CONDIZIONE>", getCondizione_BPER(RtbFile, rsColonne!Nome)
          End If
        End If
        'ALIAS: [ab]+'N'+[c]+M+g:
        changeTag RtbFile, "<ALIAS>", Left(nomeTable, 2) & "N" & Mid(nomeTable, 10)
      End If

      getMoveDli2Rdbms = getMoveDli2Rdbms & RtbFile.text & vbCrLf 'formattare il primo...
                      
    'Else
      'colonna ignorata
    End If
    rsColonne.MoveNext
  Wend
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & rsColonne!RoutWrite & """ not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & rsColonne!RoutWrite & """ not found."
    Case 7575
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Function

Function getMoveDli2Rdbms_PLI(RtbFile As RichTextBox, rsColonne As Recordset, nomeTable As String, nomeSegmento As String, pidsegmento As Long, nomesegmor As String) As String
  Dim rtbString As String ', parameter As String
  Dim rsCmp As Recordset, rsCmp2 As Recordset
  
  'Dim env As Collection
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
  
  On Error GoTo loadErr
  While Not rsColonne.EOF
    If Not IsNull(rsColonne!RoutWrite) And Len(rsColonne!RoutWrite) Then
      'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & rsColonne!RoutWrite
      
      RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\" & rsColonne!RoutWrite
      
      '''''''''''''''''''''''''''''''''
      ' RIEMPIO IL TEMPLATE!
      '''''''''''''''''''''''''''''''''
      'usare un tag piu' appropriato... (per replace...)
      
       changeTag RtbFile, "<NOME-COL>", rsColonne!Nome
       changeTag RtbFile, "<LEN-COL>", rsColonne!lunghezza
       'changeTag rtbFile, "<NOME-CAMPO>", IIf(IsNull(rsColonne!NomeCmp), "", Replace(rsColonne!NomeCmp, "-", "_"))
       changeTag RtbFile, "<NOME-CAMPO>", Replace(rsColonne!NomeCmp & "", "-", "_")
       changeTag RtbFile, "<NOME-TAB>", nomeTable
       changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
       'TILVIA
       changeTag RtbFile, "<NOME-SEGMOR>", nomesegmor

     
    ' SQ 14-06-06
    ' Gestione Chiavi ereditate
    ' SQ - ATTENZIONE! - UNIFORMARE LA GESTIONE CON LE NUOVE VARIABILI GLOBALI
    '                    idSegmentoParent,idTableParent,...
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If Right(rsColonne!Tag, 6) = "OCCURS" Then ' (potrei avere "AUTOMATIC-OCCURS")
        'SQ - TMP: questa gestione particolare per le OCCURS � un po' forzata... meglio cambiare template/tag!
        'stefano: 16/7/2006 si chiama diverso....
        'changeTag rtbFile, "<NOME-COL-PARENT>", Replace(rsColonne!Nome, "_", "-") 'vincolo: stesso nome (accettabile)
        Dim daCamb As Recordset
        Set daCamb = m_fun.Open_Recordset("select Nome from " & PrefDb & "_Colonne where idcmp = " & rsColonne!IdCmp & " and IdTable=" & rsColonne!idTableJoin)
        changeTag RtbFile, "<NOME-COL-PARENT>", Replace(daCamb!Nome, "_", "-")
        daCamb.Close
        Set rsCmp = m_fun.Open_Recordset("select Nome from " & PrefDb & "_Tabelle where IdTable=" & rsColonne!idTableJoin)
        If rsCmp.RecordCount Then
          changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Nome, "_", "-")
        End If
        rsCmp.Close
      Else
        If Right(rsColonne!Tag, 10) = "DISPATCHER" Then ' (potrei avere "AUTOMATIC-DISPATCHER")
          
          If rsColonne!IdCmp > 0 Then '� l'idField!
            'FIELD
            Set rsCmp = m_fun.Open_Recordset("Select posizione,lunghezza from PsDli_Field where idField = " & rsColonne!IdCmp)
            If rsCmp.RecordCount Then
              'Attenzione, entra anche per le ereditate... non sono distinguibili!
              changeTag RtbFile, "<POS-SEGM-FIELD>", rsCmp!Posizione
              changeTag RtbFile, "<LEN-SEGM-FIELD>", rsCmp!lunghezza
            End If
          Else
            'XDFIELD: gestire!
            Set rsCmp = m_fun.Open_Recordset("Select srchFields from PsDli_XDField where idXDField = " & rsColonne!IdCmp)
          End If
          rsCmp.Close
        End If
        If rsColonne!idTableJoin <> rsColonne!IdTable Then
          Set rsCmp = m_fun.Open_Recordset("select b.nome as nome from MgData_Cmp as a,PsDli_Segmenti as b where a.IdCmp = " & rsColonne!IdCmp & " And a.IdSegmento = b.IdSegmento")
          If rsCmp.RecordCount Then
            changeTag RtbFile, "<NOME-SEGM-PARENT>", rsCmp!Nome
          End If
          rsCmp.Close
          
          If rsColonne!idTableJoin Then
            'Template di tipo "INHERITED
            Set rsCmp = m_fun.Open_Recordset("select a.Nome as colonna,b.nome as tabella from " & PrefDb & "_Colonne as a," & PrefDb & "_Tabelle as b where " & _
                                             "a.IdTable = b.IdTable And a.IdTable = " & rsColonne!idTableJoin & " And " & _
                                             "a.IdCmp = " & rsColonne!IdCmp)
            If rsCmp.RecordCount Then
              '"MOVE " & Replace(rsOrigine(0), "_", "-") & " OF RR-" & rsOrigine(1) & " TO " & vbCrLf
              changeTag RtbFile, "<NOME-COL-PARENT>", Replace(rsCmp!colonna, "_", "-")
              changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Tabella, "_", "-")
            End If
            rsCmp.Close
          End If
        Else
          'Colonne Aggiunte (Es. contatore...) - FARE "TAG" DEDICATO!
          If rsColonne!IdCmp = -1 Then
            changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(nomeTableParent, "_", "-")
          End If
        End If
      End If
      
      If Not IsNull(rsColonne!NomeCmp) Then
        Set rsCmp = m_fun.Open_Recordset("select IdSegmento,Byte,ordinale,Posizione,livello from MgData_Cmp where IdCmp = " & rsColonne!IdCmp & " and IdSegmento=" & pidsegmento)
        If rsCmp.RecordCount Then
          changeTag RtbFile, "<LEN-FIELD>", IIf(IsNull(rsCmp!Byte), "", rsCmp!Byte)
          changeTag RtbFile, "<POS-FIELD>", rsCmp!Posizione
          'SQ: 25-08
          'Set rsCmp2 = m_fun.Open_Recordset("select Nome from MgData_Cmp where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!Livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 order by Ordinale desc")
          Set rsCmp2 = m_fun.Open_Recordset("select Nome,livello,ordinale from MgData_Cmp where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 and decimali = 0 order by Ordinale desc")
          If rsCmp2.RecordCount Then
            'SQ 10-07-06
            'SE IL GRUPPO E' IL LIVELLO 01, NON CE L'ABBIAMO NELLA COPY T! E' LO "RD-..."
            If rsCmp2!livello = 1 Then
              changeTag RtbFile, "<GROUP-NAME>", "RD_" & nomeSegmento 'UTILIZZARE IL PARAMETRO DI SISTEMA!!!!!
            Else
              changeTag RtbFile, "<GROUP-NAME>", rsCmp2!Nome
            End If
            'SQ: 24-08
            If m_fun.FnNomeDB = "banca.mty" Then
              If rsCmp2!livello > 1 Then  'probabilmente si puo' aggiungere alla query sopra...
                'controllare prima di invocare...
                changeTag RtbFile, "<MOVE-SPACE-BROTHERS>", getMoveSpaceBrothers_BPER(getBrotherColumns_BPER(rsCmp, rsColonne!IdTable, rsCmp2!Ordinale))
              End If
            End If
          Else
            m_fun.WriteLog "getMoveDli2Rdbms: tmp-error: no group field found!"
          End If
          rsCmp2.Close
        Else
          '...
        End If
        rsCmp.Close
      End If
      
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' GESTIONE XDFIELD... generalizzare... solo banca...
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If m_fun.FnNomeDB = "banca.mty" Then
        If Left(rsColonne!Nome, 3) = "KY_" And rsColonne!IdCmp = -1 Then 'scremo un po' di lavoro... hanno -1 (per la banca)
          changeTag RtbFile, "<XDFIELD-MOVE>", getXdfieldMove_BPER(rsColonne!Nome)
          'gestione "forzata" (orrenda)...
          If Right(rsColonne!RoutWrite, 4) = "EXIT" Then
            changeTag RtbFile, "<CONDIZIONE>", getCondizione_BPER(RtbFile, rsColonne!Nome)
          End If
        End If
        'ALIAS: [ab]+'N'+[c]+M+g:
        changeTag RtbFile, "<ALIAS>", Left(nomeTable, 2) & "N" & Mid(nomeTable, 10)
      End If

      getMoveDli2Rdbms_PLI = getMoveDli2Rdbms_PLI & RtbFile.text & vbCrLf 'formattare il primo...
                      
    'Else
      'colonna ignorata
    End If
    rsColonne.MoveNext
  Wend
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & rsColonne!RoutWrite & """ not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & rsColonne!RoutWrite & """ not found."
    Case 7575
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Uguale alla duale, cambia solo RoutWrite/RoutRead (farne una sola, volendo)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'silvia 6-5-2008 : affiunto idsegmento
''Function getMoveRdbms2Dli(RtbFile As RichTextBox, rsColonne As Recordset, nomeTable As String, nomeSegmento As String, idsegmento As Long, Optional pgtype = "") As String
Function getMoveRdbms2Dli(RtbFile As RichTextBox, rsColonne As Recordset, nomeTable As String, nomeSegmento As String, IdSegmento As Long, Optional pgtype = "") As String
  Dim rtbString As String
  Dim rsCmp As Recordset, rsCmp2 As Recordset
  
  On Error GoTo loadErr
  
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
    
  While Not rsColonne.EOF
    If Not IsNull(rsColonne!RoutRead) And Len(rsColonne!RoutRead) Then
      'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & rsColonne!RoutRead
      If pgtype = "PLI" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\" & rsColonne!RoutRead
      Else
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & rsColonne!RoutRead
      End If
      '''''''''''''''''''''''''''''''''
      ' RIEMPIO IL TEMPLATE!
      '''''''''''''''''''''''''''''''''
      'usare un tag piu' appropriato... (per replace...)
      If Not pgtype = "PLI" Then
       changeTag RtbFile, "<NOME-COL>", Replace(rsColonne!Nome, "_", "-")
       changeTag RtbFile, "<NOME-CAMPO>", IIf(IsNull(rsColonne!NomeCmp), "", rsColonne!NomeCmp)
       changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "_", "-")
       changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
       changeTag RtbFile, "<LEN-COL>", rsColonne!lunghezza
      Else
       changeTag RtbFile, "<NOME-COL>", rsColonne!Nome
       rsColonne!NomeCmp = IIf(IsNull(rsColonne!NomeCmp), "", rsColonne!NomeCmp)
       changeTag RtbFile, "<NOME-CAMPO>", Replace(rsColonne!NomeCmp, "-", "_")
       changeTag RtbFile, "<NOME-TAB>", nomeTable
       changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
       changeTag RtbFile, "<LEN-COL>", rsColonne!lunghezza
      End If
      
      'If Right(rsColonne!Tag, 6) = "OCCURS" Then ' (potrei avere "AUTOMATIC-OCCURS")
      If Right(rsColonne!Tag, 10) = "DISPATCHER" Then ' (potrei avere "AUTOMATIC-DISPATCHER")
        If rsColonne!IdCmp > 0 Then '� l'idField!
          'FIELD
          Set rsCmp = m_fun.Open_Recordset("Select posizione,lunghezza from PsDli_Field where idField = " & rsColonne!IdCmp)
          If rsCmp.RecordCount Then
            'Attenzione, entra anche per le ereditate... non sono distinguibili!
            changeTag RtbFile, "<POS-SEGM-FIELD>", rsCmp!Posizione
            changeTag RtbFile, "<LEN-SEGM-FIELD>", rsCmp!lunghezza
          End If
        End If
      End If
      If Len(nomeTableParent) Then changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(nomeTableParent, "_", "-")
      
      If Not IsNull(rsColonne!NomeCmp) Then
        'silvia 6-5-2008 aggiunto idsegmento
        '' Set rsCmp = m_fun.Open_Recordset("select IdSegmento,Byte,ordinale,Posizione,livello from MgData_Cmp where IdCmp = " & rsColonne!IdCmp)
        Set rsCmp = m_fun.Open_Recordset("select IdSegmento,Byte,ordinale,Posizione,livello from MgData_Cmp where IdCmp = " & rsColonne!IdCmp & " and IdSegmento=" & IdSegmento)
 
        If rsCmp.RecordCount Then
          changeTag RtbFile, "<LEN-FIELD>", IIf(IsNull(rsCmp!Byte), "", rsCmp!Byte)
          changeTag RtbFile, "<POS-FIELD>", rsCmp!Posizione
          
          'SQ: 24-08
          Set rsCmp2 = m_fun.Open_Recordset("select Nome,livello,ordinale from MgData_Cmp where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 and decimali = 0 order by Ordinale desc")
          'Set rsCmp2 = m_fun.Open_Recordset("select Nome from MgData_Cmp where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!Livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 order by Ordinale desc")
          If rsCmp2.RecordCount Then
            'SQ 10-07-06
            'SE IL GRUPPO E' IL LIVELLO 01, NON CE L'ABBIAMO NELLA COPY T! E' LO "RD-..."
            If rsCmp2!livello = 1 Then
              changeTag RtbFile, "<GROUP-NAME>", "RD-" & nomeSegmento 'UTILIZZARE IL PARAMETRO DI SISTEMA!!!!!
            Else
              changeTag RtbFile, "<GROUP-NAME>", rsCmp2!Nome
            End If

            'Banca: potrebbe servire?!
            'If rsCmp2!Livello > 1 Then  'probabilmente si puo' aggiungere alla query sopra...
            '    brotherColumns = getBrotherColumns_BPER(rsCmp, rsColonne!IdTable, rsCmp2!Ordinale)
            '    'controllare prima di invocare...
            '    changeTag rtbFile, "<CHECK-NULL-BROTHERS>", getCheckNullBrothers_BPER(brotherColumns)
            '    changeTag rtbFile, "<CHECK-SPACE-BROTHERS>", getCheckSpaceBrothers_BPER(brotherColumns)
            'End If
          Else
            m_fun.WriteLog "getMoveRdbms2Dli: tmp-error: no group field found!"
          End If
          rsCmp2.Close
        Else
          '...
        End If
        rsCmp.Close
      End If
      
      'ALIAS: gestire
      'changeTag rtbFile, "<ALIAS>", Left(nomeTable, 2) & "N" & Mid(nomeTable, 10)
      
      getMoveRdbms2Dli = getMoveRdbms2Dli & RtbFile.text & vbCrLf 'formattare il primo...
                      
    'Else
      'colonna ignorata
    End If
    rsColonne.MoveNext
  Wend
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & rsColonne!RoutRead & """ not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & rsColonne!RoutRead & """ not found."
    Case 7575
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Function

Function GetLenByType(Tipo As String, lunghezza As Integer) As Integer
  If Not lunghezza = 0 Then
    GetLenByType = lunghezza
  Else
    Select Case Tipo
      Case "INTEGER"
        GetLenByType = 5
      Case "DATE"
        GetLenByType = 5
      Case "SMALLINT"
        GetLenByType = 5
    End Select
  End If
End Function

Function getCmpFromField(IdSegmento As Double, nameField As String, IdTable As Long, LenCmp As String, campiCMP As String, RoutRead As String) As String
  Dim rs As Recordset, rsColonne As Recordset
  Dim fldStart As Integer, fldLen As Integer, complete As Boolean, totLen As Integer
  Dim addCmp As Boolean, lenColumn As Integer
  
  Set rs = m_fun.Open_Recordset("Select Posizione,Lunghezza from PsDli_Field where " & _
                                "IdSegmento = " & IdSegmento & " and Nome = '" & nameField & "'")
  If Not rs.EOF Then
    fldStart = rs!Posizione
    fldLen = rs!lunghezza
  Else
    rs.Close
    Exit Function
  End If
  rs.Close
  
  addCmp = True
  Set rs = m_fun.Open_Recordset("Select d.Nome,d.Posizione,d.Byte from PsData_Cmp as d,MgRel_SegAree as r where " & _
                                "r.IdSegmento = " & IdSegmento & " and r.StrIdOggetto = d.IdOggetto and r.StrIdArea = d.IdArea and " & _
                                "d.Posizione >= " & fldStart & " and d.livello > 1 order by ordinale")
  While Not rs.EOF And Not complete
    Set rsColonne = m_fun.Open_Recordset("Select Nome,Tipo,Lunghezza,NomeCmp,RoutRead from DMDB2_Colonne where " & _
                                         "IdTable = " & IdTable & " and NomeCmp = '" & rs!Nome & "'")
    If Not rsColonne.EOF Then
      lenColumn = GetLenByType(rsColonne!Tipo, rsColonne!lunghezza)
      totLen = totLen + lenColumn
      If totLen >= CInt(fldLen) Then
        complete = True
        If totLen > CInt(fldLen) + 1 Then
          addCmp = False
        End If
      End If
      If addCmp Then
        getCmpFromField = getCmpFromField & rsColonne!Nome & ","
        LenCmp = LenCmp & CStr(lenColumn) & ","
        campiCMP = campiCMP & rsColonne!NomeCmp & ","
        RoutRead = RoutRead & rsColonne!RoutRead & ","
      End If
    End If
    rs.MoveNext
  Wend
  If Right(LenCmp, 1) = "," Then
    LenCmp = Left(LenCmp, Len(LenCmp) - 1)
  End If
  If Right(NomeCmp, 1) = "," Then
    campiCMP = Left(campiCMP, Len(campiCMP) - 1)
  End If
  If Right(RoutRead, 1) = "," Then
    RoutRead = Left(RoutRead, Len(RoutRead) - 1)
  End If
  If Right(getCmpFromField, 1) = "," Then
    getCmpFromField = Left(getCmpFromField, Len(getCmpFromField) - 1)
  End If
End Function

Function getMoveRdbms2DliXD(RtbFile As RichTextBox, rsColonne As Recordset, nomeTable As String, nomeSegmento As String, ddata As String, Key As String, Optional pgtype = "") As String
  Dim rtbString As String
  'Dim rsCmp As Recordset, rsCmp2 As Recordset
  ' var ddata
  Dim splitDData() As String, splitcampi() As String, Campi() As String, idOriginel As Double, appoCampi As String
  Dim splitlunghezze() As String, splitCampiCmp() As String
  Dim campiCMP() As String, lunghezze() As String, pLunghezze As String, pCampiCmp As String
  Dim RoutRead() As String, pRoutRead As String, splitRoutR() As String
  Dim rs As Recordset, rs2 As Recordset
  Dim testo As String
  Dim i As Integer, j As Integer, pSrch As String, pddata As String
    
  On Error GoTo loadErr
  
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
  ReDim Campi(0)
  ReDim campiCMP(0)
  ReDim lunghezze(0)
  ReDim RoutRead(0)
  
  Set rs2 = m_fun.Open_Recordset("select idindex from " & PrefDb & "_index as a, psdli_xdfield as b where " & _
                                 "(a.idorigine * -1) = b.idxdfield and b.nome = '" & Key & "' and a.idtable = " & rsColonne!IdTable)
  
  If Not rs2.EOF Then
    Set rs = m_fun.Open_Recordset("select nome,NomeCmp,lunghezza,routread,Tipo from " & PrefDb & "_colonne as a, " & PrefDb & "_idxcol as b where " & _
                                  "b.idindex = " & rs2!IdIndex & " and a.idtable = " & rsColonne!IdTable & " and a.idtable = b.idtable and " & _
                                  "a.idcolonna = b.idcolonna Order by b.ordinale")
    For i = 1 To rs.RecordCount
      ReDim Preserve Campi(UBound(Campi) + 1)
      Campi(UBound(Campi)) = rs!Nome
      ReDim Preserve campiCMP(UBound(campiCMP) + 1)
      campiCMP(UBound(campiCMP)) = rs!NomeCmp
      ReDim Preserve lunghezze(UBound(lunghezze) + 1)
      lunghezze(UBound(lunghezze)) = GetLenByType(rs!Tipo, rs!lunghezza)
      ReDim Preserve RoutRead(UBound(RoutRead) + 1)
      RoutRead(UBound(RoutRead)) = rs!RoutRead
      rs.MoveNext
    Next
    rs.Close
  End If
  rs2.Close
  
  ' gestione ddata
  If Len(ddata) Then
    Set rs2 = m_fun.Open_Recordset("select idorigine from " & PrefDb & "_tabelle where idtable = " & rsColonne!IdTable)
    If Not rs2.EOF Then
      idOriginel = rs2!IdOrigine
    End If
    splitDData = Split(ddata, ",")
    For i = 0 To UBound(splitDData)
      pLunghezze = ""
      pCampiCmp = ""
      pRoutRead = ""
      appoCampi = getCmpFromField(idOriginel, splitDData(i), rsColonne!IdTable, pLunghezze, pCampiCmp, pRoutRead)
      If Len(appoCampi) Then
        splitcampi() = Split(appoCampi, ",")
        For j = 0 To UBound(splitcampi)
          ReDim Preserve Campi(UBound(Campi) + 1)
          Campi(UBound(Campi)) = splitcampi(j)
        Next j
        splitCampiCmp() = Split(pCampiCmp, ",")
        For j = 0 To UBound(splitCampiCmp)
          ReDim Preserve campiCMP(UBound(campiCMP) + 1)
          campiCMP(UBound(campiCMP)) = splitCampiCmp(j)
        Next j
        splitlunghezze() = Split(pLunghezze, ",")
        For j = 0 To UBound(splitlunghezze)
          ReDim Preserve lunghezze(UBound(lunghezze) + 1)
          lunghezze(UBound(lunghezze)) = splitlunghezze(j)
        Next j
        splitRoutR() = Split(pRoutRead, ",")
        For j = 0 To UBound(splitRoutR)
          ReDim Preserve RoutRead(UBound(RoutRead) + 1)
          RoutRead(UBound(RoutRead)) = splitRoutR(j)
        Next j
      End If
    Next i
  End If
        
  For i = 1 To UBound(Campi)
    If Not IsNull(rsColonne!RoutRead) And Len(rsColonne!RoutRead) Then
      'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & rsColonne!RoutRead
      If pgtype = "PLI" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\" & RoutRead(i)
      Else
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & RoutRead(i)
      End If
      '''''''''''''''''''''''''''''''''
      ' RIEMPIO IL TEMPLATE!
      '''''''''''''''''''''''''''''''''
      'usare un tag piu' appropriato... (per replace...)
      If Not pgtype = "PLI" Then
        changeTag RtbFile, "<NOME-COL>", Replace(Campi(i), "_", "-")
        changeTag RtbFile, "<NOME-CAMPO>", campiCMP(i)
        changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "_", "-")
        changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
        changeTag RtbFile, "<LEN-COL>", lunghezze(i)
      Else
        changeTag RtbFile, "<NOME-COL>", Campi(i)
        changeTag RtbFile, "<NOME-CAMPO>", Replace(campiCMP(i), "-", "_")
        changeTag RtbFile, "<NOME-TAB>", nomeTable
        changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
        changeTag RtbFile, "<LEN-COL>", lunghezze(i)
      End If
    End If
  Next i
   
   '--- parte dispactcher non gestita
'      'If Right(rsColonne!Tag, 6) = "OCCURS" Then ' (potrei avere "AUTOMATIC-OCCURS")
'      If Right(rscolonne!Tag, 10) = "DISPATCHER" Then ' (potrei avere "AUTOMATIC-DISPATCHER")
'        If rscolonne!IdCmp > 0 Then '� l'idField!
'          'FIELD
'          Set rsCmp = m_fun.Open_Recordset("Select posizione,lunghezza from PsDli_Field where idField = " & rscolonne!IdCmp)
'          If rsCmp.RecordCount Then
'            'Attenzione, entra anche per le ereditate... non sono distinguibili!
'            changeTag rtbFile, "<POS-SEGM-FIELD>", rsCmp!Posizione
'            changeTag rtbFile, "<LEN-SEGM-FIELD>", rsCmp!lunghezza
'          End If
'        End If
'      End If
'      If Len(nomeTableParent) Then changeTag rtbFile, "<NOME-TAB-PARENT>", Replace(nomeTableParent, "_", "-")
'
'      If Not IsNull(rscolonne!NomeCmp) Then
'        Set rsCmp = m_fun.Open_Recordset("select IdSegmento,Byte,ordinale,Posizione,livello from MgData_Cmp where IdCmp = " & rscolonne!IdCmp)
'        If rsCmp.RecordCount Then
'          changeTag rtbFile, "<LEN-FIELD>", IIf(IsNull(rsCmp!Byte), "", rsCmp!Byte)
'          changeTag rtbFile, "<POS-FIELD>", rsCmp!Posizione
'
'          'SQ: 24-08
'          Set rsCmp2 = m_fun.Open_Recordset("select Nome,livello,ordinale from MgData_Cmp where IdSegmento=" & rsCmp!idSegmento & " AND livello < " & rsCmp!Livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 and decimali = 0 order by Ordinale desc")
'          'Set rsCmp2 = m_fun.Open_Recordset("select Nome from MgData_Cmp where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!Livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 order by Ordinale desc")
'          If rsCmp2.RecordCount Then
'            'SQ 10-07-06
'            'SE IL GRUPPO E' IL LIVELLO 01, NON CE L'ABBIAMO NELLA COPY T! E' LO "RD-..."
'            If rsCmp2!Livello = 1 Then
'              changeTag rtbFile, "<GROUP-NAME>", "RD-" & nomeSegmento 'UTILIZZARE IL PARAMETRO DI SISTEMA!!!!!
'            Else
'              changeTag rtbFile, "<GROUP-NAME>", rsCmp2!Nome
'            End If
'
'          Else
'            m_fun.WriteLog "getMoveRdbms2Dli: tmp-error: no group field found!"
'          End If
'          rsCmp2.Close
'        Else
'          '...
'        End If
'        rsCmp.Close
'      End If
'
'      'ALIAS: gestire
'      'changeTag rtbFile, "<ALIAS>", Left(nomeTable, 2) & "N" & Mid(nomeTable, 10)
'
'      getMoveRdbms2Dli = getMoveRdbms2Dli & rtbFile.text & vbCrLf 'formattare il primo...
'
'    'Else
'      'colonna ignorata
'    End If
'    rscolonne.MoveNext
'  Wend
'  'ripristino
'  rtbFile.text = rtbString
  getMoveRdbms2DliXD = getMoveRdbms2DliXD & RtbFile.text & vbCrLf 'formattare il primo...
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & rsColonne!RoutRead & """ not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & rsColonne!RoutRead & """ not found."
    Case 7575
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Function

Function getMoveLoad_PLI(RtbFile As RichTextBox, rsColonne As Recordset, nomeTable As String, _
                         nomeSegmento As String, IdSegmento As Long, nomesegmor As String) As String
  Dim rsCmp As Recordset, rsCmp2 As Recordset
  Dim rtbString As String
  
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
  
  On Error GoTo loadErr
  While Not rsColonne.EOF
    'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & rsColonne!RoutLoad
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\" & rsColonne!RoutLoad
    '''''''''''''''''''''''''''''''''
    ' RIEMPIO IL TEMPLATE!
    '''''''''''''''''''''''''''''''''
    changeTag RtbFile, "<NOME-COL>", Replace(rsColonne!Nome, "-", "_")
    changeTag RtbFile, "<LEN-COL>", rsColonne!lunghezza
    changeTag RtbFile, "<NOME-CAMPO>", IIf(IsNull(rsColonne!NomeCmp), "", rsColonne!NomeCmp)
    changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "-", "_")
    'tilvia
    changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
    changeTag RtbFile, "<NOME-SEGMOR>", nomesegmor
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' SQ 14-06-06
    ' Gestione Chiavi ereditate
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Right(rsColonne!Tag, 6) = "OCCURS" Then ' (potrei avere "AUTOMATIC-OCCURS")
      'SQ - TMP: questa gestione particolare per le OCCURS � un po' forzata... meglio cambiare template/tag!
      changeTag RtbFile, "<NOME-COL-PARENT>", rsColonne!Nome 'vincolo: stesso nome (accettabile)
      Set rsCmp = m_fun.Open_Recordset("select Nome from " & PrefDb & "_Tabelle where IdTable=" & rsColonne!idTableJoin)
      If rsCmp.RecordCount Then
        changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Nome, "-", "_")
      End If
      rsCmp.Close
    Else
      If Right(rsColonne!Tag, 10) = "DISPATCHER" Then ' (potrei avere "AUTOMATIC-DISPATCHER")
        If rsColonne!IdCmp > 0 Then '� l'idField!
        
          
          'FIELD
          Set rsCmp = m_fun.Open_Recordset("Select posizione,lunghezza from PsDli_Field where idField = " & rsColonne!IdCmp & " and IdSegmento=" & IdSegmento)
          If rsCmp.RecordCount Then
            'Attenzione, entra anche per le ereditate... non sono distinguibili!
            changeTag RtbFile, "<POS-SEGM-FIELD>", rsCmp!Posizione
            changeTag RtbFile, "<LEN-SEGM-FIELD>", rsCmp!lunghezza
          End If
        Else
          'XDFIELD: gestire!
          Set rsCmp = m_fun.Open_Recordset("Select srchFields from PsDli_XDField where idXDField = " & rsColonne!IdCmp)
        End If
        rsCmp.Close
      End If
      If rsColonne!idTableJoin <> rsColonne!IdTable Then
       'Set rsCmp = m_fun.Open_Recordset("select b.nome as nome from DMDB2_Tabelle as a,PsDli_Segmenti as b where a.IdTable = " & rsColonne!IdTableJoin & " And a.IdOrigine = b.IdSegmento")
       Set rsCmp = m_fun.Open_Recordset("select b.nome as nome from MgData_Cmp as a,PsDli_Segmenti as b where a.IdCmp = " & rsColonne!IdCmp & " And a.IdSegmento = b.IdSegmento")
       If rsCmp.RecordCount Then
         changeTag RtbFile, "<NOME-SEGM-PARENT>", rsCmp!Nome
       End If
       rsCmp.Close
       
       If rsColonne!idTableJoin Then
         'Template di tipo "INHERITED
         'SQ 4-4-08 - COME FA AD ESSERCI ANCORA QUESTO PROBLEMA (idCmp non univoco)?!. Aggiunto "a.IdTable=a.idTableJoin"
         Set rsCmp = m_fun.Open_Recordset( _
          "select a.Nome as colonna,b.nome as tabella from " & PrefDb & "_Colonne as a," & PrefDb & "_Tabelle as b where " & _
          "a.IdTable = b.IdTable AND " & _
          "a.IdTable = " & rsColonne!idTableJoin & " And a.IdTable=a.idTableJoin And " & _
          "a.IdCmp = " & rsColonne!IdCmp)
         If rsCmp.RecordCount Then
           '"MOVE " & Replace(rsOrigine(0), "_", "-") & " OF RR-" & rsOrigine(1) & " TO " & vbCrLf
           changeTag RtbFile, "<NOME-COL-PARENT>", Replace(rsCmp!colonna, "-", "_")
           changeTag RtbFile, "<NOME-TAB-PARENT>", Replace(rsCmp!Tabella, "-", "_")
         End If
         rsCmp.Close
       End If
      End If
    End If

    If Not IsNull(rsColonne!NomeCmp) Then
      '''MG 130207 aggiunto id segmento come filtro
      Set rsCmp = m_fun.Open_Recordset("select IdSegmento, Byte, Ordinale, posizione, livello from MgData_Cmp where " & _
                                       "IdCmp = " & rsColonne!IdCmp & " and IdSegmento=" & IdSegmento)
      
      'stefano: evito il loop
      If Not rsCmp.EOF Then
       
       changeTag RtbFile, "<LEN-FIELD>", IIf(IsNull(rsCmp!Byte), "", rsCmp!Byte)
       changeTag RtbFile, "<POS-FIELD>", rsCmp!Posizione
    
    
       Set rsCmp2 = m_fun.Open_Recordset("select Nome,LIVELLO from MgData_Cmp where IdSegmento=" & rsCmp!IdSegmento & " AND livello < " & rsCmp!livello & " AND ordinale < " & rsCmp!Ordinale & " AND lunghezza = 0 and decimali = 0 order by Ordinale desc")
       If rsCmp2.RecordCount Then
          'SQ 10-07-06
          'SE IL GRUPPO E' IL LIVELLO 01, NON CE L'ABBIAMO NELLA COPY T! E' LO "RD-..."
          If rsCmp2!livello = 1 Then
            changeTag RtbFile, "<GROUP-NAME>", "RD_" & nomeSegmento 'UTILIZZARE IL PARAMETRO DI SISTEMA!!!!!
          Else
            changeTag RtbFile, "<GROUP-NAME>", rsCmp2!Nome
          End If
       Else
      'SQ 05-08-05
          m_fun.WriteLog "getMoveLoad: tmp-error: no group field found!"
       End If
       rsCmp2.Close
      End If
      rsCmp.Close
    End If
          
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' GESTIONE XDFIELD... generalizzare... solo banca...
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'changeTag rtbFile, "<XDFIELD-MOVE>", getXdfieldMove_BPER(rsColonne!Nome)
    'If Right(rsColonne!RoutLoad, 4) = "EXIT" Then
    'changeTag rtbFile, "<CONDIZIONE>", getCondizione_BPER(rtbFile, rsColonne!Nome)
    
    getMoveLoad_PLI = getMoveLoad_PLI & RtbFile.text & vbCrLf  'formattare il primo...
                    
    rsColonne.MoveNext
  Wend
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & rsColonne!RoutLoad & """ not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & rsColonne!RoutLoad & """ not found."
    Case 7575
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Function

Sub GeneraCpyMoveKey(RtbFile As RichTextBox)
  Dim TbOrigine As Recordset, tbSegm As Recordset
  Dim copyname As String
  Dim env As Collection
  Dim rs As Recordset
  On Error GoTo errLoad
  
  'tilvia 04-05-2009
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
  pIndex_DBDCorrente = rs!iddbor
  rs.Close
   

  Screen.MousePointer = vbHourglass
   'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
  '****************
  If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE_KEY") <> "" Then
     RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE_KEY"
  Else
     DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE_KEY"" not found."
     Screen.MousePointer = vbDefault
     Exit Sub
  End If
  '*****************
'  RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE_KEY"
   
  changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
  
  changeTag RtbFile, "<MOVE-ADLWKEY>", getSetkAdlwkey(RtbFile)
  changeTag RtbFile, "<MOVE-WKEYADL>", getSetkWkeyadl(RtbFile)
  changeTag RtbFile, "<MOVE-SETC-WKEYWKEY>", getSetcWkeywkey(RtbFile)
  changeTag RtbFile, "<MOVE-SETK-WKUTWKEY>", getSetkWkutwkey(RtbFile)
  ' Mauro 2014 :  Nuova gestione WKUTWKEY separata per SEGMENTO/FIELD
  getSetkSegments RtbFile
  'Gestione keyfeedbackarea
  changeTag RtbFile, "<MOVE-SETK-WKEYWKUT>", getSetkWkeywkut(RtbFile)
  changeTag RtbFile, "<KEYSEC-ROUTINES>", getKeysecRoutines(RtbFile)
  changeTag RtbFile, "<AKEYSEC-ROUTINES>", getAKeysecRoutines()
  
  ''''''''''''''''''''''''''''''''''''''''''''''''
  ' NOME COPY:
  ''''''''''''''''''''''''''''''''''''''''''''''''
  If Len(KCopyParam) Then
    Set env = New Collection  'resetto;
    env.Add pNome_DBCorrente
    'SQ 31-08 (banca)
    'non genero le copy K per gli storici:
    '...
    RtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\" & dbType & "\Cpy\" & getEnvironmentParam(KCopyParam, "dbd", env), 1
  Else
    MsgBox "tmp: problemi sul paramentro K-COPY!!!!!!!!", vbExclamation
  End If
  
  Screen.MousePointer = vbDefault
  Exit Sub
errLoad:
  Screen.MousePointer = vbDefault
  Select Case ERR.Number
    Case 75
      'VB non ha trovato il file
'      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_MOVE_KEY"" not found."
    Case 7575
      ' non ha trovato il file (err.number "forzato" da noi...)
      ERR.Raise 75, , ERR.Description
    Case Else
      MsgBox ERR.Description
     'Resume
  End Select
End Sub

Sub GeneraCpyMoveKey_PLI(RtbFile As RichTextBox)
  Dim env As Collection, rs As Recordset
  On Error GoTo errLoad
  
  Screen.MousePointer = vbHourglass
  
  'tilvia 04-05-2009
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
  pIndex_DBDCorrente = rs!iddbor
  rs.Close
   
   'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
  '****************
  If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE_KEY") <> "" Then
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE_KEY"
  Else
    DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE_KEY"" not found."
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
  
'  RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE_KEY"
  '*****************
  changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
  
  changeTag RtbFile, "<MOVE-ADLWKEY>", getSetkAdlwkey_PLI(RtbFile)
  changeTag RtbFile, "<MOVE-WKEYADL>", getSetkWkeyadl_PLI(RtbFile)
  changeTag RtbFile, "<MOVE-SETC-WKEYWKEY>", getSetcWkeywkey_PLI(RtbFile)
  changeTag RtbFile, "<MOVE-SETK-WKUTWKEY>", getSetkWkutwkey_PLI(RtbFile)
  'Gestione keyfeedbackarea
  changeTag RtbFile, "<MOVE-SETK-WKEYWKUT>", getSetkWkeywkut_PLI(RtbFile)
  changeTag RtbFile, "<KEYSEC-ROUTINES>", getKeysecRoutines_PLI(RtbFile)
  changeTag RtbFile, "<AKEYSEC-ROUTINES>", getAKeysecRoutines_PLI()
  
  ''''''''''''''''''''''''''''''''''''''''''''''''
  ' NOME COPY:
  ''''''''''''''''''''''''''''''''''''''''''''''''
  If Len(KCopyParam) Then
    Set env = New Collection  'resetto;
    env.Add pNome_DBCorrente
    'SQ 31-08 (banca)
    'non genero le copy K per gli storici:
    '...
    RtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\" & dbType & "\Include\" & getEnvironmentParam(KCopyParam, "dbd", env), 1
  Else
    MsgBox "tmp: problemi sul paramentro K-COPY!!!!!!!!", vbExclamation
  End If
  
  Screen.MousePointer = vbDefault
  Exit Sub
errLoad:
  Screen.MousePointer = vbDefault
  Select Case ERR.Number
    Case 75
      'VB non ha trovato il file
'      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_MOVE_KEY"" not found."
    Case 7575
      ' non ha trovato il file (err.number "forzato" da noi...)
      ERR.Raise 75, , ERR.Description
    Case Else
      MsgBox ERR.Description
     'Resume
  End Select
End Sub

Function getSetkWkeywkut(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, rsColonne As Recordset, TbCmp As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  'silvia
  Dim rsindexdis As Recordset
  Dim statement As String, rrName As String, rdName As String, Nome As String
  Dim cCopyArea As String, oldKey As String
  Dim i_index As Long
  Dim pos As Integer, Lun As Integer
  Dim env As Collection
  Dim rsTbParent As Recordset
  Dim continue As Boolean
  Dim rsebasta As Recordset
  Dim newTempl As String

  statement = "EVALUATE WK-NAMETABLE " & vbCrLf
  
  'SQ TMP - Lasciamo da parte le occurs...
  'stefano: invece no
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB IN  (select IdDB from " & PrefDb & "_DB where idDBOr = " & pIndex_DBDCorrente & ") AND idorigine > 0  and TAG not like '%OCCURS' order by idTable")
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB IN  (select IdDB from " & PrefDb & "_DB where idDBOr = " & pIndex_DBDCorrente & ") AND idorigine > 0  and TAG not like '%OCCURS' order by idTable")
  
  While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
    'SQ 26-05-06 - Segmenti virtuali: NON USIAMO LE KRR...
    'stefano: invece s�
    'stefano: per il momento no, tanto la wkeywkut non la usiamo.
'    If rsSegmenti.RecordCount = 0 Then
'       Set rsSegmenti = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
'    End If
    If rsSegmenti.RecordCount Then
      'SQ: 26-08
      statement = statement & Space(3) & "WHEN '" & Replace(rsTabelle!Nome, "-", "_") & "' " & vbCrLf
      continue = True
      cCopyArea = LeggiParam("C-COPY-AREA")
      Set env = New Collection  'resetto;
      env.Add rsColonne
      env.Add rsTabelle
      If Len(cCopyArea) Then
        cCopyArea = Replace(getEnvironmentParam(cCopyArea, "colonne", env), "_", "-")
      Else
        'segnalare errore
        cCopyArea = "RR-" & Replace(rsTabelle!Nome, "_", "-")
      End If
      
      '''''''''''''''''''
      'CHIAVI PRIMARIE:
      '''''''''''''''''''
      Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where " & _
                                         "tipo = 'P' and idtable = " & rsTabelle!IdTable)
      If rsIndex.RecordCount > 0 Then
        Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where " & _
                                            "idindex = " & rsIndex!IdIndex & " order by ordinale ")
        Do While Not rsIdxCol.EOF
          Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where " & _
                                               "idcolonna = " & rsIdxCol!IdColonna)
          If rsColonne.RecordCount Then
            Set env = New Collection  'resetto;
            env.Add rsColonne
            env.Add rsTabelle
            If Len(cCopyColumnParam) Then
              rrName = getEnvironmentParam(cCopyColumnParam, "colonne", env)
            Else
              'segnalare errore
              rrName = rsColonne!Nome
            End If
            rrName = Replace(rrName, "_", "-")
            If rsColonne!IdCmp > 0 Then
              'stefano: dava errore sulle dispatcher, tanto la perform non serve per niente....
              ' per cui � una versione "draft"
              'silvia 13-5-2008
              ''If Right(rsTabelle!Tag, 10) = "DISPATCHER"
              If Right(rsTabelle!Tag, 10) = "DISPATCHER" And rsColonne!IdTable <> rsColonne!idTableJoin Then
                'UNICA MOVE: COLONNA  "FIELD"
                statement = statement & Space(5) & "MOVE " & rrName & " OF " & cCopyArea & " TO " & vbCrLf & _
                                        Space(5) & "     " & rrName & " OF KRDP-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                continue = False
              Else
                'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                If rsColonne!IdTable = rsColonne!idTableJoin Then
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp " & _
                                                   "where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                Else
                  Set rsTbParent = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idtable = " & rsColonne!idTableJoin)
                  If rsTbParent.RecordCount Then
                    'silvia 13-5-2008
''                    If Right(rsTbParent!Tag, 10) = "DISPATCHER" Then
''                      Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where ordinale = " & rsColonne!Ordinale & " and idsegmento = " & rsTbParent!IdOrigine)
''                    Else
                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTbParent!IdOrigine)
''                    End If
                  'SQ 21-09-07 pezza: chiarire:
                  Else
                    ' SILVIA 13-3-2008  SE NON TROVA CAMPI PER LA TABELLA
                    DataMan.DmFinestra.ListItems.Add , , Now & " Table" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column."
                    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
                    'MsgBox "Table" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                    
                    rsTbParent.Close
                    Exit Function
                  End If
                  rsTbParent.Close
                End If
                If TbCmp.RecordCount Then
                  rdName = TbCmp!Nome
                Else
                  'MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                  DataMan.DmFinestra.ListItems.Add , , Now & " Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column."
                  DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.Count).EnsureVisible
                  TbCmp.Close
                  Exit Do
                  'Exit Function
                End If
                'SP 5/9 per ora non imposto la posizione/lunghezza: se la trova da errore...
                Nome = "KRDP-" & Replace(rsIndex!Nome, "_", "-")  '? KRDP?
                If pos = 0 Then pos = 1
                If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                  pos = 1
                  oldKey = Replace(rsIndex!Nome, "_", "-")
                End If
                Lun = TbCmp!Byte
                
                newTempl = rsColonne!RoutRead & ""
                'SP 12/9: correzione per casi di azienda fissa su template
                'If newTempl = "" Then
                If newTempl = "" Or rsColonne!IdTable <> rsColonne!idTableJoin Then
                  'silvia caso dispatcher
                  If Right(rsColonne!Tag, 10) = "DISPATCHER" Then
                    Set rsindexdis = m_fun.Open_Recordset("select * from " & PrefDb & "_index where " & _
                                                          "tipo = 'P' and idtable = " & rsColonne!idTableJoin)
                                         
                    statement = statement & Space(5) & "MOVE " & rrName & " OF " & cCopyArea & " TO " & vbCrLf & _
                                            Space(5) & "     " & rrName & " OF KRDP-" & Replace(rsindexdis!Nome, "_", "-") & vbCrLf
                    rsindexdis.Close
                    continue = False
                  Else
                    Set rsebasta = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                                        "idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                    If Not rsebasta.EOF Then
                      newTempl = rsebasta!RoutRead & ""
                    Else
                      newTempl = ""
                    End If
                    rsebasta.Close
                    ''End If
                    '14/9 RR invece di KRR per la keyfeedbackarea
                    'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, RsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, True, pos, Lun)
                    If rsSegmenti!IdSegmentoOrigine <> 0 Then
                      rdName = Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "_", "-")
                    End If
                    statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR-" & Replace(rsTabelle!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, True, pos, Lun)
                    pos = pos + TbCmp!Byte
                    continue = False
                  End If
                ' Mauro 14/10/2008 : Muoveva solo i campi delle chiavi ereditate
                Else
                  Set rsebasta = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                                      "idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                  If Not rsebasta.EOF Then
                    newTempl = rsebasta!RoutRead & ""
                  Else
                    newTempl = ""
                  End If
                  rsebasta.Close
                  ''End If
                  '14/9 RR invece di KRR per la keyfeedbackarea
                  'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, RsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, True, pos, Lun)
                  If rsSegmenti!IdSegmentoOrigine <> 0 Then
                    rdName = Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "_", "-")
                  End If
                  statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR-" & Replace(rsTabelle!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, True, pos, Lun)
                  pos = pos + TbCmp!Byte
                  continue = False
                End If
                ''''''''''''''''''''''''''''''
              End If
            End If  'rsColonne!IdCmp > 0 Then
          Else
            m_fun.WriteLog "#####getSetkWkeywkut: column " & rsIdxCol!IdColonna & " not found for index " & rsIndex!IdIndex
          End If
          rsIdxCol.MoveNext
        Loop
      End If 'rsIndex.RecordCount > 0
      
      'SP 5/9: keyfeedback
      i_index = 0
      ''''''''''''''''''''''
      ' CHIAVI SECONDARIE:
      ''''''''''''''''''''''
      Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where " & _
                                         "tipo = 'I' and used = true and idtable = " & rsTabelle!IdTable & " order by idIndex")  'ordine necessario per nome...
      While Not rsIndex.EOF
        'stefano: provo
        Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
        i_index = i_index + 1  'nome campo: KRD_<nome-segm>_I<i_index> <-- SQ non � pi� vero?!
        Do While Not rsIdxCol.EOF
          Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
          If rsColonne!IdCmp > 0 Then
            rrName = Replace(rsColonne!Nome, "_", "-")
            'silvia 13-5-2008
            ''If Right(rsTabelle!Tag, 10) = "DISPATCHER"
            If Right(rsTabelle!Tag, 10) = "DISPATCHER" And rsColonne!IdTable <> rsColonne!idTableJoin Then
              statement = statement & Space(5) & "MOVE " & rrName & " OF " & cCopyArea & " TO " & vbCrLf & _
                                      Space(5) & "     " & rrName & " OF KRDP-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
              continue = False
            Else
              'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
              If rsColonne!IdTable = rsColonne!idTableJoin Then
                Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp " & _
                                                 "where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
              Else
                Set rsTbParent = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idtable = " & rsColonne!idTableJoin)
                If rsTbParent.RecordCount Then
'                  'silvia 13-5-2008
''                  If Right(rsTbParent!Tag, 10) = "DISPATCHER" Then
''                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where ordinale = " & rsColonne!Ordinale & " and idsegmento = " & rsTbParent!IdOrigine)
''                  Else
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTbParent!IdOrigine)
''                  End If
                End If
                rsTbParent.Close
              End If
  
              If TbCmp.RecordCount Then
                '???????
                If rsColonne!Nome = rsColonne!NomeCmp Then
                  rdName = TbCmp!Nome
                Else
                  rdName = rsColonne!Nome
                End If
                Nome = "KRDP-" & Replace(rsIndex!Nome, "_", "-")
                  
                newTempl = rsColonne!RoutRead
                If newTempl = "" Then
                  'silvia caso dispatcher
                  If Right(rsColonne!Tag, 10) = "DISPATCHER" Then
                    Set rsindexdis = m_fun.Open_Recordset("select * from " & PrefDb & "_index where " & _
                                                          "tipo = 'P' and idtable = " & rsColonne!idTableJoin)
                    statement = statement & Space(5) & "MOVE " & rrName & " OF " & cCopyArea & " TO " & vbCrLf & _
                                            Space(5) & "     " & rrName & " OF KRDP-" & Replace(rsindexdis!Nome, "_", "-") & vbCrLf
                    rsindexdis.Close
                    continue = False
                  Else
                    Set rsebasta = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                                        "idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                    If Not rsebasta.EOF Then
                      newTempl = rsebasta!RoutRead
                    Else
                      newTempl = ""
                    End If
                    rsebasta.Close
                    'End If
                    If pos = 0 Then pos = 1
                    If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                      pos = 1
                      oldKey = Replace(rsIndex!Nome, "_", "-")
                    End If
                    Lun = TbCmp!Byte
                    '14/9 RR invece di KRR per la keyfeedbackarea
                    statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR-" & Replace(rsTabelle!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, False, pos, Lun)
                    pos = pos + TbCmp!Byte
                    continue = False
                  End If
                ' Mauro 14/10/2008 : Muoveva solo i campi delle chiavi ereditate
                Else
                  Set rsebasta = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                                      "idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                  If Not rsebasta.EOF Then
                    newTempl = rsebasta!RoutRead
                  Else
                    newTempl = ""
                  End If
                  rsebasta.Close
                  'End If
                    
                  If pos = 0 Then pos = 1
                  If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                    pos = 1
                    oldKey = Replace(rsIndex!Nome, "_", "-")
                  End If
                  Lun = TbCmp!Byte
                  '14/9 RR invece di KRR per la keyfeedbackarea
                  statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR-" & Replace(rsTabelle!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, False, pos, Lun)
                  pos = pos + TbCmp!Byte
                  continue = False
                End If
              Else
                DataMan.DmFinestra.ListItems.Add , , Now & " No cobol field found for " & rsColonne!Nome & " column."
                DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
                'MsgBox "No cobol field found for " & rsColonne!Nome & " column.", vbExclamation
                '''   Exit Function
                Exit Do
              End If
            End If
          End If
          rsIdxCol.MoveNext
        Loop
        rsIdxCol.Close
        rsIndex.MoveNext
      Wend
      rsIndex.Close
      If continue Then
        statement = statement & Space(6) & "CONTINUE" & vbCrLf
      End If  'continue

    End If 'rsSegmenti > 0
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  
  getSetkWkeywkut = statement & "END-EVALUATE. "
End Function

Function getSetkWkeywkut_PLI(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, rsColonne As Recordset, TbCmp As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  'silvia
  Dim rsindexdis As Recordset
  Dim statement As String, rrName As String, rdName As String, Nome As String
  Dim cCopyArea As String, oldKey As String
  Dim i_index As Long
  Dim pos As Integer, Lun As Integer
  Dim env As Collection
  Dim rsTbParent As Recordset
  Dim rsebasta As Recordset
  Dim newTempl As String
  statement = "SELECT (WK_NAMETABLE);" & vbCrLf
  
  'SQ TMP - Lasciamo da parte le occurs...
  'stefano: invece no
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idOrigine > 0 and iddb=" & pIndex_DBCorrente & " order by idTable")
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "idDB IN (select IdDB from " & PrefDb & "_DB where " & _
                                       "idDBOr = " & pIndex_DBDCorrente & ") AND idorigine > 0  and " & _
                                       "TAG not like '%OCCURS' order by idTable")
  While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    If rsSegmenti.RecordCount Then
      'SQ: 26-08
      statement = statement & Space(3) & "WHEN ('" & rsTabelle!Nome & "') DO;" & vbCrLf
         
      cCopyArea = LeggiParam("C-COPY-AREA")
      Set env = New Collection  'resetto;
      env.Add rsColonne
      env.Add rsTabelle
      If Len(cCopyArea) Then
        cCopyArea = Replace(getEnvironmentParam(cCopyArea, "colonne", env), "-", "_")
      Else
        'segnalare errore
        cCopyArea = "RR_" & rsTabelle!Nome
      End If
      
      '''''''''''''''''''
      'CHIAVI PRIMARIE:
      '''''''''''''''''''
      Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & rsTabelle!IdTable)
      If rsIndex.RecordCount > 0 Then
        Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
        Do While Not rsIdxCol.EOF
          Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
          If rsColonne.RecordCount Then
            Set env = New Collection  'resetto;
            env.Add rsColonne
            env.Add rsTabelle
            If Len(cCopyColumnParam) Then
              rrName = getEnvironmentParam(cCopyColumnParam, "colonne", env)
            Else
              'segnalare errore
              rrName = rsColonne!Nome
            End If
            If rsColonne!IdCmp > 0 Then
              'stefano: dava errore sulle dispatcher, tanto la perform non serve per niente....
              ' per cui � una versione "draft"
              '''''''If Right(rsTabelle!Tag, 10) = "DISPATCHER" Then
              If Right(rsTabelle!Tag, 10) = "DISPATCHER" And rsColonne!IdTable <> rsColonne!idTableJoin Then
               'UNICA MOVE: COLONNA  "FIELD"
                statement = statement & Space(6) & "KRDP_" & rsIndex!Nome & "." & rrName & " = " & vbCrLf & _
                Space(6) & cCopyArea & "." & rrName & ";" & vbCrLf
              Else
                'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                If rsColonne!IdTable = rsColonne!idTableJoin Then
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                Else
                  Set rsTbParent = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idtable = " & rsColonne!idTableJoin)
                  If rsTbParent.RecordCount Then
''                    If Right(rsTbParent!Tag, 10) = "DISPATCHER" Then
''                      Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where ordinale = " & rsColonne!Ordinale & " and idsegmento = " & rsTbParent!IdOrigine)
''                    Else
                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTbParent!IdOrigine)
''                    End If
                  Else
                    ' SILVIA 13-3-2008  SE NON TROVA CAMPI PER LA TABELLA
                    DataMan.DmFinestra.ListItems.Add , , Now & " Table" & rsTabelle!Nome & "': no field found for '" & rsColonne!Nome & "' column."
                    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
                    'MsgBox "Table" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                    rsTbParent.Close
                    Exit Function
                  End If
                  rsTbParent.Close
                End If
                If TbCmp.RecordCount Then
                  rdName = TbCmp!Nome
                Else
                  DataMan.DmFinestra.ListItems.Add , , Now & " Table '" & rsTabelle!Nome & "': no  field found for '" & rsColonne!Nome & "' column."
                  DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
                  'MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                  TbCmp.Close
                  Exit Do
                  'Exit Function
                End If
                'SP 5/9 per ora non imposto la posizione/lunghezza: se la trova da errore...
                Nome = "KRDP_" & rsIndex!Nome  '? KRDP?
                If pos = 0 Then pos = 1
                If rsIndex!Nome <> oldKey Then
                  pos = 1
                  oldKey = rsIndex!Nome
                End If
                Lun = TbCmp!Byte
                newTempl = rsColonne!RoutRead & ""
                'SP 12/9: correzione per casi di azienda fissa su template
                'If newTempl = "" Then
                If newTempl = "" Or rsColonne!IdTable <> rsColonne!idTableJoin Then
                  'silvia caso dispatcher
                  If Right(rsColonne!Tag, 10) = "DISPATCHER" Then
                    Set rsindexdis = m_fun.Open_Recordset("select * from " & PrefDb & "_index where " & _
                                                          "tipo = 'P' and idtable = " & rsColonne!idTableJoin)
                    statement = statement & Space(6) & "KRDP_" & rsindexdis!Nome & "." & rrName & " = " & vbCrLf & _
                                            Space(6) & cCopyArea & "." & rrName & ";" & vbCrLf
                    rsindexdis.Close
                  Else
                    If newTempl = "" Then
                      Set rsebasta = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                                          "idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                      If Not rsebasta.EOF Then
                        newTempl = rsebasta!RoutRead & ""
                      Else
                        newTempl = ""
                      End If
                      rsebasta.Close
                    End If
                    '14/9 RR invece di KRR per la keyfeedbackarea
                    'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, RsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, True, pos, Lun)
                    If rsSegmenti!IdSegmentoOrigine <> 0 Then
                      rdName = Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "_", "-")
                    End If
                    statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR-" & Replace(rsTabelle!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, True, pos, Lun)
                    pos = pos + TbCmp!Byte
                  End If
                ' Mauro 14/10/2008 : Muoveva solo i campi delle chiavi ereditate
                Else
                  If newTempl = "" Then
                    Set rsebasta = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                                        "idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                    If Not rsebasta.EOF Then
                      newTempl = rsebasta!RoutRead & ""
                    Else
                      newTempl = ""
                    End If
                    rsebasta.Close
                  End If
                '14/9 RR invece di KRR per la keyfeedbackarea
                  If rsSegmenti!IdSegmentoOrigine <> 0 Then
                    rdName = IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta)
                  End If
                  statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR_" & Replace(rsTabelle!Nome, "-", "_"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, True, pos, Lun)
                  pos = pos + TbCmp!Byte
                End If
              End If
            End If  'rsColonne!IdCmp > 0 Then
'        Else
'            m_fun.WriteLog "#####getSetkWkeywkut: column " & rsIdxCol!IdColonna & " not found for index " & rsIndex!IdIndex
          End If
          rsIdxCol.MoveNext
        Loop
      End If 'rsIndex.RecordCount > 0
      
      'SP 5/9: keyfeedback
      i_index = 0
      ''''''''''''''''''''''
      ' CHIAVI SECONDARIE:
      ''''''''''''''''''''''
      Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where " & _
                                         "tipo = 'I' and used = true and idtable = " & rsTabelle!IdTable & " order by idIndex")  'ordine necessario per nome...
      While Not rsIndex.EOF
        'stefano: provo
        Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where " & _
                                            "idindex = " & rsIndex!IdIndex & " order by ordinale ")
        i_index = i_index + 1  'nome campo: KRD_<nome-segm>_I<i_index> <-- SQ non � pi� vero?!
        Do While Not rsIdxCol.EOF
          Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
          If rsColonne!IdCmp > 0 Then
            rrName = rsColonne!Nome
            If Right(rsTabelle!Tag, 10) = "DISPATCHER" And rsColonne!IdTable <> rsColonne!idTableJoin Then
              statement = statement & Space(6) & "KRDP_" & rsIndex!Nome & "." & rrName & " = " & vbCrLf & _
              Space(6) & cCopyArea & "." & rrName & ";" & vbCrLf
            Else
              'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
              If rsColonne!IdTable = rsColonne!idTableJoin Then
                Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where " & _
                                                 "idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
              Else
                Set rsTbParent = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where " & _
                                                      "idtable = " & rsColonne!idTableJoin)
                If rsTbParent.RecordCount Then
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where " & _
                                                   "idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTbParent!IdOrigine)
                End If
                rsTbParent.Close
              End If

              If TbCmp.RecordCount Then
                '???????
                If rsColonne!Nome = rsColonne!NomeCmp Then
                  rdName = TbCmp!Nome
                Else
                  rdName = rsColonne!Nome
                End If
                Nome = "KRDP_" & rsIndex!Nome
                  
                newTempl = rsColonne!RoutRead
                If newTempl = "" Then
                  If Right(rsColonne!Tag, 10) = "DISPATCHER" Then
                    Set rsindexdis = m_fun.Open_Recordset("select * from " & PrefDb & "_index where " & _
                                                          "tipo = 'P' and idtable = " & rsColonne!idTableJoin)
                    
                    statement = statement & Space(6) & "KRDP_" & rsindexdis!Nome & "." & rrName & " = " & vbCrLf & _
                    Space(6) & cCopyArea & "." & rrName & ";" & vbCrLf
                    
                    rsindexdis.Close
                  Else
                    Set rsebasta = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                    If Not rsebasta.EOF Then
                      newTempl = rsebasta!RoutRead
                    Else
                      newTempl = ""
                    End If
                    rsebasta.Close
                    'End If
                    If pos = 0 Then pos = 1
                    If rsIndex!Nome <> oldKey Then
                      pos = 1
                      oldKey = rsIndex!Nome
                    End If
                    Lun = TbCmp!Byte
                    '14/9 RR invece di KRR per la keyfeedbackarea
                    statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR_" & Replace(rsTabelle!Nome, "-", "_"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, False, pos, Lun)
                    pos = pos + TbCmp!Byte
                  End If
                Else
                  Set rsebasta = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where " & _
                                                      "idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                  If Not rsebasta.EOF Then
                    newTempl = rsebasta!RoutRead
                  Else
                    newTempl = ""
                  End If
                  rsebasta.Close
                  If pos = 0 Then pos = 1
                  If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                    pos = 1
                    oldKey = Replace(rsIndex!Nome, "_", "-")
                  End If
                  Lun = TbCmp!Byte
                  '14/9 RR invece di KRR per la keyfeedbackarea
                  statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR-" & Replace(rsTabelle!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", rdName, Nome, TbCmp!Usage, False, pos, Lun)
                  pos = pos + TbCmp!Byte
                End If
              Else
                DataMan.DmFinestra.ListItems.Add , , Now & " No cobol field found for " & rsColonne!Nome & " column."
                DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
                Exit Do
              End If
            End If
          End If
          rsIdxCol.MoveNext
        Loop
        rsIdxCol.Close
        rsIndex.MoveNext
      Wend
      rsIndex.Close
      statement = statement & Space(3) & "END;" & vbCrLf
    End If 'rsSegmenti > 0
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  statement = statement & Space(3) & "OTHERWISE DO;" & vbCrLf
  statement = statement & Space(3) & "END;" & vbCrLf
  getSetkWkeywkut_PLI = statement & "END;"
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Separato dal matroccolo; riorganizzare il tutto...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function getSetkWkutwkey(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, rs As Recordset, Tb2 As Recordset, rsColonne As Recordset, TbCmp As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  Dim statement As String, rrName As String, rdName As String, krdName As String, cCopyArea As String
  Dim continue As Boolean
  Dim env As Collection
  
  statement = "EVALUATE WK-NAMETABLE " & vbCrLf
  
  'TUTTE LE TABELLE TRANNE QUELLE DI OCCURS. POI PRENDE SOLO QUELLO CON SEGMENTO... PERDE LE "VIRTUALI"
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idOrigine > 0 and TAG not like '%OCCURS' and iddb=" & pIndex_DBCorrente & " order by idTable")
   Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB IN  (select IdDB from " & PrefDb & "_DB where idDBOr = " & pIndex_DBDCorrente & ") AND idorigine > 0 and TAG not like '%OCCURS'  order by idTable")

  Do While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
    'stefano: ancora le redefines.. servono
    If rsSegmenti.EOF Then
       Set rsSegmenti = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
    End If
    If rsSegmenti.RecordCount Then
      statement = statement & Space(3) & "WHEN '" & Replace(rsTabelle!Nome, "-", "_") & "' " & vbCrLf
      
      'stefano: aha fedigrafo...
      'continue = rsSegmenti!IdSegmentoOrigine <> 0
      'stefano 25/8/07: ci sono anche casi in cui non c'� la chiave primaria, ma le move le deve
      ' fare lo stesso
'      continue = False
       continue = True
       
      'Controllino... modificare!
'      Set rs = m_fun.Open_Recordset( _
'        "SELECT * from " & PrefDb & "_INDEX as a, " & PrefDb & "_IDXCOL as b, " & PrefDb & "_COLONNE as c " & _
'          " where a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & _
'          " and a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
'          " and c.idtable = c.idtablejoin and c.idcmp <> -1")
'      If rs.RecordCount = 0 Then
'        continue = True
'      End If
'      rs.Close
      
'      If continue Then
'        statement = statement & Space(6) & "CONTINUE" & vbCrLf
'      Else
        Set env = New Collection  'resetto;
        env.Add rsColonne
        env.Add rsTabelle
        If Len(cCopyArea) Then
          cCopyArea = Replace(getEnvironmentParam(cCopyAreaParam, "colonne", env), "_", "-")
        Else
          'segnalare errore
          cCopyArea = "RR-" & Replace(rsTabelle!Nome, "_", "-")
        End If
        
        '''''''''''''''''''
        'CHIAVI PRIMARIE:
        '''''''''''''''''''
        Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_INDEX where tipo = 'P' and idtable = " & rsTabelle!IdTable)
        If rsIndex.RecordCount > 0 Then
          Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
          Do While Not rsIdxCol.EOF
            Set rsColonne = m_fun.Open_Recordset( _
              "Select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna & _
              " and idTable=idTableJoin")
            If rsColonne.RecordCount Then
              'Nome = "KRD-" & RsSegmenti!Nome
              krdName = "KRD-" & Replace(rsIndex!Nome, "_", "-")
              If rsColonne!IdCmp > 0 Then
                rrName = Replace(rsColonne!Nome, "_", "-")
                'silvia
                rdName = rrName
                If Right(rsTabelle!Tag, 10) = "DISPATCHER" Then
                  'UNICA MOVE: COLONNA  "FIELD"
                  'SILVIA 12-5-2008 : caso del "FIELDONE"!!!
                  statement = statement & Space(5) & "MOVE " & krdName & " TO " & vbCrLf & _
                                          Space(5) & "      KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                  
''                  statement = statement & Space(5) & "MOVE " & rrName & " OF " & krdName & " TO " & vbCrLf & _
''                                          Space(5) & "     " & rrName & " OF KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                  continue = False
                Else
                  'SQ Ho gi� il nome campo nella DMDB2_Colonne...
                  'controllare che sia sempre coerente ed utilizzarlo!!!!!!
                  'idcmp non � pi� univoco
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                  If TbCmp.RecordCount Then
                    rdName = TbCmp!Nome 'SQ - non abbiamo parametri di modifica nome?
                  Else
                    'MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                    DataMan.DmFinestra.ListItems.Add , , Now & " Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column."
                    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
                    TbCmp.Close
                    Exit Function
                  End If
                  'VC
                  'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, rsTabelle!Nome, nomeCampo, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & " ", rdName, krdName, TbCmp!Usage)
                  Dim pos As Integer
                  Dim Lun As Integer
                  Dim oldKey As String
                  If pos = 0 Then pos = 1
                  If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                     pos = 1
                     oldKey = Replace(rsIndex!Nome, "_", "-")
                  End If
                  Lun = TbCmp!Byte
                  If Len(rsColonne!RoutKey) = 0 Then
                    statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & "", rdName, krdName, TbCmp!Usage, True, pos, Lun)
                  Else
                    statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutKey & "", rdName, krdName, TbCmp!Usage, True, pos, Lun)
                  End If
                  continue = False
                  pos = pos + TbCmp!Byte
                End If  'dispatcher
              End If  'idCmp > 0
            Else
              m_fun.WriteLog "#####getSetkWkutwkey: column " & rsIdxCol!IdColonna & " not found for index " & rsIndex!IdIndex
            End If
            rsIdxCol.MoveNext
          Loop
        End If 'end "CHIAVI PRIMARIE"
        ''''''''''''''''''''''
        ' CHIAVI SECONDARIE:
        ''''''''''''''''''''''
        Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where " & _
                                           "tipo = 'I' and used = true and idtable = " & rsTabelle!IdTable & " order by idIndex")  'ordine necessario per nome...
        While Not rsIndex.EOF
          Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
          While Not rsIdxCol.EOF
            Set rsColonne = m_fun.Open_Recordset("Select * from " & PrefDb & "_COLONNE where " & _
                                                 "idcolonna = " & rsIdxCol!IdColonna & " and idTable=idTableJoin")
            If rsColonne.RecordCount Then
            'silvia 12-5-2008
            krdName = "KRD-" & Replace(rsIndex!Nome, "_", "-")
            If rsColonne!IdCmp > 0 Then
              rrName = Replace(rsColonne!Nome, "_", "-")
                  'silvia
                rdName = rrName
            If Right(rsTabelle!Tag, 10) = "DISPATCHER" Then
                'UNICA MOVE: COLONNA  "FIELD"
              'SILVIA 12-5-2008 : caso del "FIELDONE"!!!
               statement = statement & Space(5) & "MOVE " & krdName & " TO " & vbCrLf & _
                                        Space(5) & "     KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
''                statement = statement & Space(5) & "MOVE " & rrName & " OF " & krdName & " TO " & vbCrLf & _
''                                        Space(5) & "     " & rrName & " OF KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                continue = False
              Else
    
                Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                If TbCmp.RecordCount Then
                  '????
                  If rsColonne!Nome = rsColonne!NomeCmp Then
                    rdName = TbCmp!Nome
                  Else
                    rdName = Replace(rsColonne!Nome, "_", "-")
                  End If
                  krdName = "KRD-" & Replace(rsIndex!Nome, "_", "-")
                  'newTempl = rsColonne!RoutWrite
                  If pos = 0 Then pos = 1
                  If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                     pos = 1
                     oldKey = Replace(rsIndex!Nome, "_", "-")
                  End If
                  Lun = TbCmp!Byte
                  If Len(rsColonne!RoutKey) = 0 Then
                    statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & " ", rdName, krdName, TbCmp!Usage, False, pos, Lun)
                  Else
                    statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutKey & " ", rdName, krdName, TbCmp!Usage, False, pos, Lun)
                  End If
                  continue = False
                  pos = pos + TbCmp!Byte
                Else
                  'MsgBox "No cobol field found for " & rsColonne!Nome & " column.", vbExclamation
                  DataMan.DmFinestra.ListItems.Add , , Now & " No cobol field found for " & rsColonne!Nome & " column."
                  DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
                  Exit Function
                TbCmp.Close
              End If
            End If
            Else
              m_fun.WriteLog "#####getSetkWkutwkey: column " & rsIdxCol!IdColonna & " not found for index " & rsIndex!IdIndex
            End If
          End If
          rsIdxCol.MoveNext
          Wend
          rsIdxCol.Close
          rsIndex.MoveNext
        Wend
        rsIndex.Close
      If continue Then
        statement = statement & Space(6) & "CONTINUE" & vbCrLf
      End If  'continue
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Loop
  rsTabelle.Close
  
  statement = statement & "END-EVALUATE. "
  getSetkWkutwkey = statement
End Function

Function getSetkWkutwkey_PLI(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, rs As Recordset, Tb2 As Recordset, rsColonne As Recordset, TbCmp As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  Dim statement As String, rrName As String, rdName As String, krdName As String, cCopyArea As String
  Dim continue As Boolean
  Dim env As Collection
  
  statement = "SELECT (WK_NAMETABLE);" & vbCrLf
  
  'TUTTE LE TABELLE TRANNE QUELLE DI OCCURS. POI PRENDE SOLO QUELLO CON SEGMENTO... PERDE LE "VIRTUALI"
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where " & _
  '                                     "idOrigine > 0 and TAG not like '%OCCURS' and iddb=" & pIndex_DBCorrente & " order by idTable")
    
    Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB IN  (select IdDB from " & PrefDb & "_DB where idDBOr = " & pIndex_DBDCorrente & ") AND idorigine > 0 and TAG not like '%OCCURS'  order by idTable")

  
  Do While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    'stefano: ancora le redefines.. servono
    If rsSegmenti.EOF Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    End If
    If rsSegmenti.RecordCount Then
      statement = statement & Space(3) & "WHEN ('" & rsTabelle!Nome & "') DO;" & vbCrLf
      
      'stefano: aha fedigrafo...
      'continue = rsSegmenti!IdSegmentoOrigine <> 0
      continue = False
      
      'Controllino... modificare!
      Set rs = m_fun.Open_Recordset( _
          "SELECT * from " & PrefDb & "_INDEX as a, " & PrefDb & "_IDXCOL as b, " & PrefDb & "_COLONNE as c " & _
          " where a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & _
          " and a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
          " and c.idtable = c.idtablejoin and c.idcmp <> -1")
      If rs.RecordCount = 0 Then
        continue = True
      End If
      rs.Close
      
      If continue Then
        statement = statement & vbCrLf
      Else
        Set env = New Collection  'resetto;
        env.Add rsColonne
        env.Add rsTabelle
        If Len(cCopyArea) Then
          cCopyArea = Replace(getEnvironmentParam(cCopyAreaParam, "colonne", env), "-", "_")
        Else
          'segnalare errore
          cCopyArea = "RR_" & rsTabelle!Nome
        End If
        
        '''''''''''''''''''
        'CHIAVI PRIMARIE:
        '''''''''''''''''''
        Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_INDEX where tipo = 'P' and idtable = " & rsTabelle!IdTable)
        If rsIndex.RecordCount Then
          Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
          Do While Not rsIdxCol.EOF
            Set rsColonne = m_fun.Open_Recordset( _
              "Select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna & _
              " and idTable=idTableJoin")
            If rsColonne.RecordCount Then
              'Nome = "KRD-" & RsSegmenti!Nome
              krdName = "KRD_" & rsIndex!Nome
              If rsColonne!IdCmp > 0 Then
                rrName = rsColonne!Nome
                If Right(rsTabelle!Tag, 10) = "DISPATCHER" Then
                  'tilvia move del campo ridefinito
'''                  'UNICA MOVE: COLONNA  "FIELD"
'''                  statement = statement & Space(6) & "KRR_" & rsIndex!Nome & "." & rrName & " = " & vbCrLf & _
'''                  Space(6) & krdName & "." & rrName & ";" & vbCrLf
                  statement = statement & Space(6) & "KRR_" & rsIndex!Nome & "R  = " & vbCrLf & _
                  Space(6) & krdName & "R ;" & vbCrLf
                Else
                  'SQ Ho gi� il nome campo nella DMDB2_Colonne...
                  'controllare che sia sempre coerente ed utilizzarlo!!!!!!
                  'idcmp non � pi� univoco
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                  If TbCmp.RecordCount Then
                    rdName = Replace(TbCmp!Nome, "-", "_") 'SQ - non abbiamo parametri di modifica nome?
                  Else
                    'MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                    DataMan.DmFinestra.ListItems.Add , , Now & " Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column."
                    DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
                    TbCmp.Close
                    Exit Function
                  End If
                  'VC
                  'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, rsTabelle!Nome, nomeCampo, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & " ", rdName, krdName, TbCmp!Usage)
                  Dim pos As Integer
                  Dim Lun As Integer
                  Dim oldKey As String
                  If pos = 0 Then pos = 1
                  If rsIndex!Nome <> oldKey Then
                    pos = 1
                    oldKey = rsIndex!Nome
                  End If
                  Lun = TbCmp!Byte
                  If Len(rsColonne!RoutKey) = 0 Then
                    statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR_" & Replace(rsIndex!Nome, "-", "_"), rsColonne!Tipo, rsColonne!RoutWrite & "", rdName, krdName, TbCmp!Usage, True, pos, Lun)
                  Else
                    statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR_" & Replace(rsIndex!Nome, "-", "_"), rsColonne!Tipo, rsColonne!RoutKey & "", rdName, krdName, TbCmp!Usage, True, pos, Lun)
                  End If
                  pos = pos + TbCmp!Byte
                End If  'dispatcher
              End If  'idCmp > 0
            Else
              m_fun.WriteLog "#####getSetkWkutwkey: column " & rsIdxCol!IdColonna & " not found for index " & rsIndex!IdIndex
            End If
            rsIdxCol.MoveNext
          Loop
        End If 'end "CHIAVI PRIMARIE"
        ''''''''''''''''''''''
        ' CHIAVI SECONDARIE:
        ''''''''''''''''''''''
        Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where " & _
                                           "tipo = 'I' and used = true and idtable = " & rsTabelle!IdTable & " order by idIndex")  'ordine necessario per nome...
        While Not rsIndex.EOF
          Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
          While Not rsIdxCol.EOF
            Set rsColonne = m_fun.Open_Recordset( _
                "Select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna & _
                " and idTable=idTableJoin")
            If rsColonne.RecordCount Then
              If rsColonne!IdCmp Then
                rrName = rsColonne!Nome
                
                Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where " & _
                                                 "idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                If TbCmp.RecordCount Then
                  '????
                  If rsColonne!Nome = rsColonne!NomeCmp Then
                    rdName = Replace(TbCmp!Nome, "-", "_")
                  Else
                    rdName = rsColonne!Nome
                  End If
                  krdName = "KRD_" & rsIndex!Nome
                  'newTempl = rsColonne!RoutWrite
                  If pos = 0 Then pos = 1
                  If rsIndex!Nome <> oldKey Then
                    pos = 1
                    oldKey = rsIndex!Nome
                  End If
                  Lun = TbCmp!Byte
                  If Len(rsColonne!RoutKey) = 0 Then
                    statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR_" & Replace(rsIndex!Nome, "-", "_"), rsColonne!Tipo, rsColonne!RoutWrite & " ", rdName, krdName, TbCmp!Usage, False, pos, Lun)
                  Else
                    statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR_" & Replace(rsIndex!Nome, "-", "_"), rsColonne!Tipo, rsColonne!RoutKey & " ", rdName, krdName, TbCmp!Usage, False, pos, Lun)
                  End If
                  pos = pos + TbCmp!Byte
                Else
                  'MsgBox "No cobol field found for " & rsColonne!Nome & " column.", vbExclamation
                  DataMan.DmFinestra.ListItems.Add , , Now & " No cobol field found for " & rsColonne!Nome & " column."
                  DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
                  Exit Function
                End If
                TbCmp.Close
              End If
            Else
              m_fun.WriteLog "#####getSetkWkutwkey: column " & rsIdxCol!IdColonna & " not found for index " & rsIndex!IdIndex
            End If
            rsIdxCol.MoveNext
          Wend
          rsIdxCol.Close
          rsIndex.MoveNext
        Wend
        rsIndex.Close
      End If  'continue
      statement = statement & Space(3) & "END;" & vbCrLf
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Loop
  rsTabelle.Close
  statement = statement & Space(3) & "OTHERWISE DO;" & vbCrLf
  statement = statement & Space(3) & "END;" & vbCrLf
  statement = statement & "END;"
  getSetkWkutwkey_PLI = statement
End Function
'Ex CreaKeySec
Public Function getAKeysecRoutines() As String
  Dim Stringa As String, cCopyArea As String, cCopyColumn As String
  Dim rsTabelle As Recordset, Tb1 As Recordset, Tb2 As Recordset, rsColonne As Recordset, TbCmp As Recordset, TbIndex As Recordset, TbIdxCol As Recordset, TbSeg As Recordset
  Dim env As Collection

  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where iddb = " & pIndex_DBCorrente)
  While Not rsTabelle.EOF
    If rsTabelle!IdOrigine > 0 Then
      Set TbSeg = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
      If TbSeg.RecordCount = 0 Then
        Set TbSeg = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
      End If
      
      Set env = New Collection  'resetto;
      env.Add rsColonne
      env.Add rsTabelle
      If Len(cCopyAreaParam) Then
        cCopyArea = Replace(getEnvironmentParam(cCopyAreaParam, "colonne", env), "_", "-")
      Else
        'segnalare errore
        cCopyArea = "RR-" & Replace(rsTabelle!Nome, "_", "-")
      End If
      
      'SQ 7-06-05
      'tmp?
      'Tolte SETA per FK
      Set TbIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_INDEX where tipo = 'I' and used and idtable = " & rsTabelle!IdTable)
      While Not TbIndex.EOF
        'If m_fun.FnNomeDB = "Toyota.mty" Then
          Stringa = Stringa & "9999-SETA-" & Replace(TbIndex!Nome, "_", "-") & " SECTION." & vbCrLf
        'Else
        '  Stringa = Stringa & "SETA-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf
        'End If
        
        Set TbIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & TbIndex!IdIndex & " order by ordinale")
        While Not TbIdxCol.EOF
          Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & TbIdxCol!IdColonna)
          'SILVIA 16-04-2008
          If rsColonne.RecordCount Then
            Set env = New Collection  'resetto;
            env.Add rsColonne
            env.Add rsTabelle
            If Len(cCopyColumnParam) Then
              cCopyColumn = getEnvironmentParam(cCopyColumnParam, "colonne", env)
            Else
              'segnalare errore
              cCopyColumn = rsColonne!Nome
            End If
            cCopyColumn = Replace(cCopyColumn, "_", "-")
            
            Stringa = Stringa & Space(4) & "MOVE " & cCopyColumn & " OF " & cCopyArea & vbCrLf & _
                                Space(4) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(TbIndex!Nome, "_", "-") & "." & vbCrLf
  '                                 Space(3) & "  TO " & wNomeCol & " OF " & "KRR-" & Replace(rsTabelle!Nome, "_", "-") & "." & vbCrLf
            Else
              'ANOMALIA: no colonna appartenente all'indice
              'per il momento segnaliamo solo, proseguendo...
              ' SILVIA 16-4-2008  SE NON TROVA CAMPI PER LA TABELLA
              DataMan.DmFinestra.ListItems.Add , , Now & " No column " & TbIdxCol!IdColonna & " for index " & TbIndex!Nome & "  And IdTable = " & TbIndex!IdTable
              DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
            End If
            TbIdxCol.MoveNext
        Wend
        'SQ tmp - per toyota
        'If m_fun.FnNomeDB = "Toyota.mty" Then
          Stringa = Stringa & vbCrLf
        'Else
        '  Stringa = Stringa & "SETA-" & Replace(TbIndex!Nome, "_", "-") & "-EX. EXIT. " & vbCrLf & vbCrLf
        'End If
                
        TbIndex.MoveNext
      Wend
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  
  getAKeysecRoutines = Stringa
End Function
'Ex CreaKeySec
Public Function getAKeysecRoutines_PLI() As String
  Dim Stringa As String, cCopyArea As String, cCopyColumn As String
  Dim rsTabelle As Recordset, Tb1 As Recordset, Tb2 As Recordset, rsColonne As Recordset, TbCmp As Recordset, TbIndex As Recordset, TbIdxCol As Recordset, TbSeg As Recordset
  Dim env As Collection

  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where iddb = " & pIndex_DBCorrente)
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "idDB IN (select IdDB from " & PrefDb & "_DB where " & _
                                       "idDBOr = " & pIndex_DBDCorrente & ") ORDER by idTable")
  While Not rsTabelle.EOF
    If rsTabelle!IdOrigine > 0 Then
      Set TbSeg = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      If TbSeg.RecordCount = 0 Then
        Set TbSeg = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      End If
      
      Set env = New Collection  'resetto;
      env.Add rsColonne
      env.Add rsTabelle
      If Len(cCopyAreaParam) Then
        cCopyArea = Replace(getEnvironmentParam(cCopyAreaParam, "colonne", env), "-", "_")
      Else
        'segnalare errore
        cCopyArea = "RR_" & rsTabelle!Nome
      End If
      
      'SQ 7-06-05
      'tmp?
      'Tolte SETA per FK
      Set TbIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_INDEX where tipo = 'I' and used and idtable = " & rsTabelle!IdTable)
      While Not TbIndex.EOF
        Stringa = Stringa & "SETA_" & TbIndex!Nome & ": PROC;" & vbCrLf
        
        Set TbIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & TbIndex!IdIndex & " order by ordinale")
        While Not TbIdxCol.EOF
          Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & TbIdxCol!IdColonna)
          
          Set env = New Collection  'resetto;
          env.Add rsColonne
          env.Add rsTabelle
          If Len(cCopyColumnParam) Then
            cCopyColumn = getEnvironmentParam(cCopyColumnParam, "colonne", env)
          Else
            cCopyColumn = rsColonne!Nome
          End If
          cCopyColumn = Replace(cCopyColumn, "-", "_")
          
          Stringa = Stringa & Space(6) & "KRR_" & TbIndex!Nome & "." & rsColonne!Nome & " = " & vbCrLf & _
                    Space(6) & cCopyArea & "." & cCopyColumn & ";" & vbCrLf
          TbIdxCol.MoveNext
        Wend
        'SQ tmp - per toyota
        'If m_fun.FnNomeDB = "Toyota.mty" Then
          Stringa = Stringa & vbCrLf
        'Else
        '  Stringa = Stringa & "SETA-" & Replace(TbIndex!Nome, "_", "-") & "-EX. EXIT. " & vbCrLf & vbCrLf
        'End If
        Stringa = Stringa & "END SETA_" & TbIndex!Nome & ";" & vbCrLf
                
        TbIndex.MoveNext
      Wend
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  
  getAKeysecRoutines_PLI = Stringa
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Utilizza a sua volta un template (ciclico)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getKeysecRoutines(RtbFile As RichTextBox) As String
  Dim moveKeysec As String, wNomeCol As String, rtbString As String
  Dim rsTabelle As Recordset, Tb1 As Recordset, TbCol As Recordset, TbCmp As Recordset, rsIndex As Recordset, TbIdxCol As Recordset, TbSeg As Recordset

  On Error GoTo errdb
  
  'salvo:
  rtbString = RtbFile.text
  
  'SQ tmp: lasciamo indietro le OCCURS
  'Set RsTabelle = m_fun.Open_Recordset("select * from dmdb2_tabelle where iddb = " & pIndex_DBCorrente)
  Set rsTabelle = m_fun.Open_Recordset( _
    "select * from " & PrefDb & "_tabelle where iddb = " & pIndex_DBCorrente & " and TAG not like '%OCCURS'")
  While Not rsTabelle.EOF
    If rsTabelle!IdOrigine > 0 Then
      Set TbSeg = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
      If TbSeg.RecordCount = 0 Then
        Set TbSeg = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
      End If
    
      Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_INDEX where tipo <> 'P' and used and idtable = " & rsTabelle!IdTable)
      While Not rsIndex.EOF
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Singolo Indice: template "MOVE_KEY_KEYSEC"
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Set TbIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
        While Not TbIdxCol.EOF
          Set TbCol = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & TbIdxCol!IdColonna)
          'silvia
          If TbCol.RecordCount Then
            wNomeCol = Replace(TbCol!Nome, "_", "-")
            
            If m_fun.FnNomeDB = "banca.mty" Then
              'Set TbCmp = m_fun.Open_Recordset("select * from MgData_Cmp where idcmp = " & TbCol!IdCmp)
              Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & TbCol!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
              
              If TbCmp.RecordCount Then
                getKeysecRoutines = getKeysecRoutines & getMoveKeysec(wNomeCol, "KRR-" & Replace(rsIndex!Nome, "_", "-"), TbCol!Tipo, TbCmp!Nome, "KRD-" & Replace(rsIndex!Nome, "_", "-"), TbCmp!Usage)
              End If
              TbCmp.Close
            Else
              If Not IsNull(rsIndex!IdOrigine) Then
                'Set TbCmp = m_fun.Open_Recordset("select * from MgData_Cmp where idcmp = " & TbCol!IdCmp)
                Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & TbCol!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                'SQ 21-06-06
                If TbCmp.RecordCount Then
                  Set Tb1 = m_fun.Open_Recordset("select * from psdli_field where idfield = " & rsIndex!IdOrigine)
                  getKeysecRoutines = getKeysecRoutines & CreaMvCol(wNomeCol, "KRR-" & Replace(rsIndex!Nome, "_", "-"), TbCol!Tipo, TbCol!RoutRead & " ", TbCmp!Nome, "KRD-" & Replace(Tb1!Nome, "_", "-"), TbCmp!Usage) & "."
                  Tb1.Close
                Else
                  'MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & TbCol!Nome & "' column.", vbExclamation
                  DataMan.DmFinestra.ListItems.Add , , Now & " Table '" & rsTabelle!Nome & "': no cobol field found for '" & TbCol!Nome & "' column."
                  DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
                  TbCmp.Close
                  'ripristino
                  RtbFile.text = rtbString
                  Exit Function
                End If
                TbCmp.Close
              End If
            End If
          Else
            'ANOMALIA: no colonna appartenente all'indice
            'per il momento segnaliamo solo, proseguendo...
            ' SILVIA 16-4-2008  SE NON TROVA CAMPI PER LA TABELLA
            DataMan.DmFinestra.ListItems.Add , , Now & " No column " & TbIdxCol!IdColonna & " for index " & rsIndex!Nome & "  And IdTable = " & rsIndex!IdTable
            DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
          End If
          TbCol.Close
          TbIdxCol.MoveNext
        Wend
        TbIdxCol.Close
        'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\MOVE_KEY_KEYSEC"
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\MOVE_KEY_KEYSEC"
        changeTag RtbFile, "<INDEX-NAME>", Replace(rsIndex!Nome, "_", "-")
        changeTag RtbFile, "<MOVE-KEYSEC>", moveKeysec
        
        getKeysecRoutines = getKeysecRoutines & RtbFile.text & vbCrLf & vbCrLf
              
        rsIndex.MoveNext
      Wend
      rsIndex.Close
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  
  'salvo:
  RtbFile.text = rtbString
  
  Exit Function
errdb:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\MOVE_KEY_KEYSEC not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\MOVE_KEY_KEYSEC not found."
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Utilizza a sua volta un template (ciclico)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getKeysecRoutines_PLI(RtbFile As RichTextBox) As String
  Dim moveKeysec As String, wNomeCol As String, rtbString As String
  Dim rsTabelle As Recordset, Tb1 As Recordset, TbCol As Recordset, TbCmp As Recordset, rsIndex As Recordset, TbIdxCol As Recordset, TbSeg As Recordset

  On Error GoTo errdb
  
'stefano: tutta la routine � obsoleta, per ora la tengo....
  
  'salvo:
  rtbString = RtbFile.text
  
  'SQ tmp: lasciamo indietro le OCCURS
  'Set RsTabelle = m_fun.Open_Recordset("select * from dmdb2_tabelle where iddb = " & pIndex_DBCorrente)
  
   Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle " & _
                                        "where idDB IN (select IdDB from " & PrefDb & "_DB where " & _
                                        "idDBOr = " & pIndex_DBDCorrente & ") and TAG not like '%OCCURS' order by idTable")
  
  'Set rsTabelle = m_fun.Open_Recordset( _
  '  "select * from " & PrefDb & "_tabelle where iddb = " & pIndex_DBCorrente & " and TAG not like '%OCCURS'")
  While Not rsTabelle.EOF
    If rsTabelle!IdOrigine > 0 Then
      Set TbSeg = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      If TbSeg.RecordCount = 0 Then
        Set TbSeg = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      End If
    
      Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_INDEX where tipo <> 'P' and used and idtable = " & rsTabelle!IdTable)
      While Not rsIndex.EOF
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Singolo Indice: template "MOVE_KEY_KEYSEC"
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Set TbIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
        While Not TbIdxCol.EOF
          Set TbCol = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & TbIdxCol!IdColonna)
          wNomeCol = TbCol!Nome
          
          If Not IsNull(rsIndex!IdOrigine) Then
            'Set TbCmp = m_fun.Open_Recordset("select * from MgData_Cmp where idcmp = " & TbCol!IdCmp)
            Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & TbCol!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
            'SQ 21-06-06
            If TbCmp.RecordCount Then
              Set Tb1 = m_fun.Open_Recordset("select * from psdli_field where idfield = " & rsIndex!IdOrigine)
              'stefano: questa in realt� � obsoleta e non capisco perch� ci sia ancora
              '  nella parte COBOL
              'getKeysecRoutines = getKeysecRoutines & CreaMvCol(wNomeCol, "KRR_" & rsIndex!Nome, TbCol!Tipo, TbCol!RoutRead & " ", TbCmp!Nome, "KRD_" & Tb1!Nome, TbCmp!Usage) & "."
              Tb1.Close
            Else
              DataMan.DmFinestra.ListItems.Add , , Now & " Table '" & rsTabelle!Nome & "': no cobol field found for '" & TbCol!Nome & "' column."
              DataMan.DmFinestra.ListItems(DataMan.DmFinestra.ListItems.count).EnsureVisible
              'MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & TbCol!Nome & "' column.", vbExclamation
              TbCmp.Close
              'ripristino
              RtbFile.text = rtbString
              Exit Function
            End If
            TbCmp.Close
          End If
          TbIdxCol.MoveNext
        Wend
        'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\MOVE_KEY_KEYSEC"
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\MOVE_KEY_KEYSEC"
        changeTag RtbFile, "<INDEX-NAME>", rsIndex!Nome
        changeTag RtbFile, "<MOVE-KEYSEC>", moveKeysec
        
        getKeysecRoutines_PLI = getKeysecRoutines_PLI & RtbFile.text & vbCrLf & vbCrLf
              
        rsIndex.MoveNext
      Wend
      rsIndex.Close
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  
  'salvo:
  RtbFile.text = rtbString
  
  Exit Function
errdb:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\MOVE_KEY_KEYSEC not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\MOVE_KEY_KEYSEC not found."
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Function

Public Function CreaMvCmp(NomeCol, NomeStr1, TipoCol, RoutX, NomeCmp, NomeStr2, tipoCmp) As String

Dim Stringa As String
Dim wStrD As String

Stringa = ""
          
If Trim(RoutX) <> "" Then
   If TipoCol = "DATE" And tipoCmp = "PCK" Then
     wStrD = "DATA-8"
     If RoutX = "D2DG" Then
        wStrD = "DATA-7"
     End If
     Stringa = Stringa & Space(16) _
        & "MOVE '" & Trim(RoutX) & "' TO WF-FUNZIONE" _
        & vbCrLf & Space(16) & "CALL WF-NOMEROUT USING" _
        & vbCrLf & Space(20) & wStrD _
        & vbCrLf & Space(20) & NomeCol & " OF " & NomeStr1 _
        & vbCrLf & Space(20) & "WF-PARAMETRI" _
        & vbCrLf & Space(16) & "MOVE " & wStrD & " TO " & NomeCmp & " OF " & NomeStr2 _
        & vbCrLf
   Else
      Stringa = Stringa & Space(16) _
        & "MOVE '" & Trim(RoutX) & "' TO WF-FUNZIONE" _
        & vbCrLf & Space(16) & "CALL WF-NOMEROUT USING" _
        & vbCrLf & Space(20) & NomeCmp & " OF " & NomeStr2 _
        & vbCrLf & Space(20) & NomeCol & " OF " & NomeStr1 _
        & vbCrLf & Space(20) & "WF-PARAMETRI" _
        & vbCrLf
  End If
Else
   Select Case TipoCol
     Case "DATE"
       If tipoCmp = "PCK" Then
          Stringa = Stringa & Space(16) _
          & "MOVE 'D2DT' TO WF-FUNZIONE" _
          & vbCrLf & Space(16) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(20) & "DATA-8" _
          & vbCrLf & Space(20) & NomeCol & " OF " & NomeStr1 _
          & vbCrLf & Space(20) & "WF-PARAMETRI" _
          & vbCrLf & Space(16) & "MOVE DATA-8 TO " & NomeCmp & " OF " & NomeStr2 _
          & vbCrLf
       Else
          Stringa = Stringa & Space(16) _
          & "MOVE 'D2DT' TO WF-FUNZIONE" _
          & vbCrLf & Space(16) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(20) & NomeCmp & " OF " & NomeStr2 _
          & vbCrLf & Space(20) & NomeCol & " OF " & NomeStr1 _
          & vbCrLf & Space(20) & "WF-PARAMETRI" _
          & vbCrLf
       End If
     Case "TIME"
       Stringa = Stringa & Space(16) _
          & "MOVE 'T2TM' TO WF-FUNZIONE" _
          & vbCrLf & Space(16) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(20) & NomeCmp & " OF " & NomeStr2 _
          & vbCrLf & Space(20) & NomeCol & " OF " & NomeStr1 _
          & vbCrLf & Space(20) & "WF-PARAMETRI" _
          & vbCrLf
     Case Else
        Stringa = Stringa & Space(16) _
                    & "MOVE " & NomeCol & " OF " & NomeStr1 & " TO " _
                    & vbCrLf & Space(20) & NomeCmp & " OF " & NomeStr2 _
                    & vbCrLf
   End Select
End If

CreaMvCmp = Stringa
End Function
Public Function getMoveKeysec(NomeCol, NomeStr1, TipoCol, NomeCmp, NomeStr2, tipoCmp) As String
  
  Dim statement As String
  Dim wStrD As String
  
  Select Case TipoCol
     Case "DATE"
       If tipoCmp = "PCK" Then
          statement = statement & Space(6) _
          & "MOVE " & NomeCmp & " OF " & NomeStr2 & " TO DATA-8" _
          & vbCrLf & Space(6) & "MOVE 'DTD2' TO WF-FUNZIONE" _
          & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(9) & "DATA-8" _
          & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
          & vbCrLf & Space(9) & "WF-PARAMETRI" _
          & vbCrLf
       Else
          statement = statement & Space(6) _
          & "MOVE 'DTD2' TO WF-FUNZIONE" _
          & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(9) & NomeCmp & " OF " & NomeStr2 _
          & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
          & vbCrLf & Space(9) & "WF-PARAMETRI" _
          & vbCrLf
       End If
     Case "TIME"
       statement = statement & Space(6) _
          & "MOVE 'TMT2' TO WF-FUNZIONE" _
          & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(9) & NomeCmp & " OF RD-" & NomeStr2 _
          & vbCrLf & Space(9) & NomeCol & " OF RR-" & NomeStr1 _
          & vbCrLf & Space(9) & "WF-PARAMETRI" _
          & vbCrLf
     Case Else
        statement = statement & Space(6) _
                    & "MOVE " & NomeCmp & " OF " & NomeStr2 & " TO " _
                    & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
                    & vbCrLf
        If TipoCol = "DECIMAL" Then
           statement = statement & Space(6) _
                     & "IF " & NomeCol & " OF " & NomeStr1 _
                     & vbCrLf & Space(18) & "NOT NUMERIC " _
                     & vbCrLf & Space(9) & "MOVE ZEROES TO " & NomeCol & " OF " & NomeStr1 _
                     & vbCrLf & Space(6) & "END-IF" & vbCrLf
        End If
    End Select
  getMoveKeysec = statement
End Function
Public Function CreaMvCol(NomeCol, NomeStr1, TipoCol, RoutX, NomeCmp, NomeStr2, tipoCmp) As String
  
  Dim statement As String
  Dim wStrD As String
  
  If Len(Trim(RoutX)) Then
    If TipoCol = "DATE" And tipoCmp = "PCK" Then
       wStrD = "DATA-8"
       If RoutX = "DGD2" Then
          wStrD = "DATA-7"
       End If
       statement = statement & Space(6) _
       & "MOVE " & NomeCmp & " OF " & NomeStr2 & " TO " & wStrD _
       & vbCrLf & Space(6) & "MOVE '" & Trim(RoutX) & "' TO WF-FUNZIONE" _
       & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
       & vbCrLf & Space(9) & wStrD _
       & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
       & vbCrLf & Space(9) & "WF-PARAMETRI" _
       & vbCrLf
    Else
       statement = statement & Space(6) _
       & "MOVE '" & Trim(RoutX) & "' TO WF-FUNZIONE" _
       & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
       & vbCrLf & Space(9) & NomeCmp & " OF " & NomeStr2 _
       & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
       & vbCrLf & Space(9) & "WF-PARAMETRI" _
       & vbCrLf
    End If
  Else
    Select Case TipoCol
     Case "DATE"
       If tipoCmp = "PCK" Then
          statement = statement & Space(6) _
          & "MOVE " & NomeCmp & " OF " & NomeStr2 & " TO DATA-8" _
          & vbCrLf & Space(6) & "MOVE 'DTD2' TO WF-FUNZIONE" _
          & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(9) & "DATA-8" _
          & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
          & vbCrLf & Space(9) & "WF-PARAMETRI" _
          & vbCrLf
       Else
          statement = statement & Space(6) _
          & "MOVE 'DTD2' TO WF-FUNZIONE" _
          & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(9) & NomeCmp & " OF " & NomeStr2 _
          & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
          & vbCrLf & Space(9) & "WF-PARAMETRI" _
          & vbCrLf
       End If
     Case "TIME"
       statement = statement & Space(6) _
          & "MOVE 'TMT2' TO WF-FUNZIONE" _
          & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(9) & NomeCmp & " OF RD-" & NomeStr2 _
          & vbCrLf & Space(9) & NomeCol & " OF RR-" & NomeStr1 _
          & vbCrLf & Space(9) & "WF-PARAMETRI" _
          & vbCrLf
     Case Else
        statement = statement & Space(6) _
                    & "MOVE " & NomeCmp & " OF " & NomeStr2 & " TO " _
                    & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
                    & vbCrLf
        If TipoCol = "DECIMAL" Then
           statement = statement & Space(6) _
                     & "IF " & NomeCol & " OF " & NomeStr1 _
                     & vbCrLf & Space(18) & "NOT NUMERIC " _
                     & vbCrLf & Space(9) & "MOVE ZEROES TO " & NomeCol & " OF " & NomeStr1 _
                     & vbCrLf & Space(6) & "END-IF" & vbCrLf
        End If
    End Select
  End If
  CreaMvCol = statement
End Function

Public Function CreaMvCol_TMP(RtbFile As RichTextBox, nomeSegmento As String, nomeTable As String, NomeCol As String, NomeStr1 As String, TipoCol As String, template As String, NomeCmp As String, nomeArea As String, tipoCmp As String, wKeyPrimary As Boolean, pos As Integer, Lun As Integer) As String
  Dim statement As String
  Dim wStrD As String
  Dim rtbString As String ', parameter As String
  
  On Error GoTo loadErr
  
  If Len(Trim(template)) Then 'togliere trim e pulire le colonne!!!!!!!!!!!!!!!!!!!!!!!
    'salvo
    rtbString = RtbFile.text
   ' rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & template
    
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & template
    
    changeTag RtbFile, "RD-<NOME-SEGM>", nomeArea
    changeTag RtbFile, "RR-<NOME-TAB>", NomeStr1
    'ATTENZIONE: DEVONO STARE QUI SOTTO!
    changeTag RtbFile, "<NOME-TAB>", nomeTable
    changeTag RtbFile, "<NOME-SEGM>", nomeSegmento
    
    changeTag RtbFile, "<NOME-CAMPO>", NomeCmp
    changeTag RtbFile, "<NOME-COL>", NomeCol
    changeTag RtbFile, "<POS-FIELD>", Format(pos)
    changeTag RtbFile, "<LEN-FIELD>", Format(Lun)
    'rapina:
    changeTag RtbFile, " RD-", " KRD-"
    'SP 14/9 doppia rapina, se gli passa rr usa direttamente rr: perch� non usa direttamente
    'il dato passato?
    If Left(NomeStr1, 3) = "KRR" Then
       changeTag RtbFile, " RR-", " KRR-"
    End If
    
    'VC: seguo lo standard di modifiche sui progetti in corso Agosto a Riccione
    'stefanopippo: toglieva anche i punti delle date: modificati i template
    'If m_fun.FnNomeDB = "Fase_2.mty" Then
    '  rtbFile.text = Replace(rtbFile.text, ".", "")
    'Else
      'torrido, ma ci tocca:
      RtbFile.text = Space(5) & RtbFile.text
      RtbFile.text = Replace(RtbFile.text, vbCrLf, vbCrLf & Space(5))
    'End If
    
    statement = RtbFile.text & vbCrLf
    'restore:
    RtbFile.text = rtbString
  Else
    'TEMPLATE REVISERIANO!... gestire tutto da fuori (solo template!)
    Select Case TipoCol
     Case "DATE"
       If tipoCmp = "PCK" Then
          statement = statement & Space(6) _
          & "MOVE " & NomeCmp & " OF " & nomeArea & " TO DATA-8" _
          & vbCrLf & Space(6) & "MOVE 'DTD2' TO WF-FUNZIONE" _
          & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(9) & "DATA-8" _
          & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
          & vbCrLf & Space(9) & "WF-PARAMETRI" _
          & vbCrLf
       Else
          statement = statement & Space(6) _
          & "MOVE 'DTD2' TO WF-FUNZIONE" _
          & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(9) & NomeCmp & " OF " & nomeArea _
          & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
          & vbCrLf & Space(9) & "WF-PARAMETRI" _
          & vbCrLf
       End If
     Case "TIME"
       statement = statement & Space(6) _
          & "MOVE 'TMT2' TO WF-FUNZIONE" _
          & vbCrLf & Space(6) & "CALL WF-NOMEROUT USING" _
          & vbCrLf & Space(9) & NomeCmp & " OF RD-" & nomeArea _
          & vbCrLf & Space(9) & NomeCol & " OF RR-" & NomeStr1 _
          & vbCrLf & Space(9) & "WF-PARAMETRI" _
          & vbCrLf
     Case Else
        statement = statement & Space(6) _
                    & "MOVE " & NomeCmp & " OF " & nomeArea & " TO " _
                    & vbCrLf & Space(9) & NomeCol & " OF " & NomeStr1 _
                    & vbCrLf
        If TipoCol = "DECIMAL" Then
           statement = statement & Space(6) _
                     & "IF " & NomeCol & " OF " & NomeStr1 _
                     & vbCrLf & Space(18) & "NOT NUMERIC " _
                     & vbCrLf & Space(9) & "MOVE ZEROES TO " & NomeCol & " OF " & NomeStr1 _
                     & vbCrLf & Space(6) & "END-IF" & vbCrLf
        End If
    End Select
  End If
  CreaMvCol_TMP = statement
  
  Exit Function
loadErr:
Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & template & " not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & template & " not found."
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Function

Public Function CreaMvCol_TMP_PLI(RtbFile As RichTextBox, nomeSegmento As String, nomeTable As String, NomeCol As String, NomeStr1 As String, TipoCol As String, template As String, NomeCmp As String, nomeArea As String, tipoCmp As String, wKeyPrimary As Boolean, pos As Integer, Lun As Integer) As String
  Dim statement As String
  Dim wStrD As String
  Dim rtbString As String ', parameter As String
  
  On Error GoTo loadErr
  
  If Len(Trim(template)) Then 'togliere trim e pulire le colonne!!!!!!!!!!!!!!!!!!!!!!!
    'salvo
    rtbString = RtbFile.text
   ' rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & template
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\" & template
    
    
    changeTag RtbFile, "RD_<NOME-SEGM>", Replace(nomeArea, "-", "_")
    changeTag RtbFile, "RR_<NOME-TAB>", Replace(NomeStr1, "-", "_")
    'ATTENZIONE: DEVONO STARE QUI SOTTO!
    changeTag RtbFile, "<NOME-TAB>", Replace(nomeTable, "-", "_")
    'changeTag RtbFile, "<NOME-SEGM>", Replace(nomeSegmento, "-", "_")
    changeTag RtbFile, "<NOME-SEGM>", Replace(nomeArea, "-", "_")
    
    changeTag RtbFile, "<NOME-CAMPO>", Replace(NomeCmp, "-", "_")
    changeTag RtbFile, "<NOME-COL>", Replace(NomeCol, "-", "_")
    changeTag RtbFile, "<POS-FIELD>", Format(pos)
    changeTag RtbFile, "<LEN-FIELD>", Format(Lun)
    'rapina:
    changeTag RtbFile, " RD_", " KRD_"
    'SP 14/9 doppia rapina, se gli passa rr usa direttamente rr: perch� non usa direttamente
    'il dato passato?
    If Left(NomeStr1, 3) = "KRR" Then
       changeTag RtbFile, " RR_", " KRR_"
    End If
    
    'VC: seguo lo standard di modifiche sui progetti in corso Agosto a Riccione
    'stefanopippo: toglieva anche i punti delle date: modificati i template
    'If m_fun.FnNomeDB = "Fase_2.mty" Then
    '  rtbFile.text = Replace(rtbFile.text, ".", "")
    'Else
      'torrido, ma ci tocca:
      RtbFile.text = Space(5) & RtbFile.text
      RtbFile.text = Replace(RtbFile.text, vbCrLf, vbCrLf & Space(5))
    'End If
    
    statement = RtbFile.text & vbCrLf
    'restore:
    RtbFile.text = rtbString
  Else
    'TEMPLATE REVISERIANO!... gestire tutto da fuori (solo template!)
    'stefano: non aveva senso per il pli
  End If
  CreaMvCol_TMP_PLI = statement
  
  Exit Function
loadErr:
Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\" & template & " not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\" & template & " not found."
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Function

'SQ a chi serviva?/serve?!
Public Sub InserisciTavole(rtb As RichTextBox, segmentName As String)
'  Dim TbTavole As Recordset
'  Dim TbColonne As Recordset
'  Dim IndOcc As Integer
'  Dim PKeyLen As Integer
'  'Dim PKeyName As String
'  Dim INDCOL As Integer
'  Dim TbColRel As Recordset
'  Dim TbApp As Recordset
'  Dim NomeCmp As String
'  Dim NomeCol As String
'  Dim TbAreaSeg As Recordset
'  Dim moveString As String
'  Dim wStrD As String
'  Dim wLen As Integer
'  Dim wPref As String
'  'Dim segmentName As String
'
'  INDCOL = 0
'  'SQ: tmp...
'  Dim tbTav As Recordset
'  Set TbTavole = m_fun.Open_Recordset("select * from DMDB2_Tabelle where IdTable= " & tbTav!IdTable)
'  If TbTavole.RecordCount = 0 Then
'     Exit Sub
'  End If
'
'  Set TbColonne = m_fun.Open_Recordset("select * from DMDB2_Colonne where IdTable= " & tbTav!IdTable & " order by ordinale")
'  If TbColonne.RecordCount > 0 Then
'    moveString = ""
'    While Not TbColonne.EOF
'      If TbColonne!IdTable = TbColonne!idTableJoin Then 'SQ: else?
'        If TbColonne!IdCmp > 0 Then
'          Set TbColRel = m_fun.Open_Recordset("select * from MgData_Cmp where IdCmp= " & TbColonne!IdCmp)
'          If TbColRel.RecordCount > 0 Then
'            'NomeCmp = TbColRel!Nome
'            'NomeCol = TbColonne!Nome
'          End If
'         End If
'      End If
'      TbColonne.MoveNext
'    Wend
'
'    ''''''''''''''''''''''''''''''''''''''''''''''''
'    ' INSERIMENTO PACCONE
'    ''''''''''''''''''''''''''''''''''''''''''''''''
'    changeTag rtb, "<MOVE-WK-TO-ADL>.", moveString
'
'    TbColonne.MoveFirst
'    moveString = ""
'
'    While TbColonne.EOF = False
'      If TbColonne!IdTable = TbColonne!idTableJoin Then
'       If val(TbColonne!IdCmp & "") > 0 Then
'        Set TbColRel = m_fun.Open_Recordset("select * from MgData_Cmp where IdCmp= " & TbColonne!IdCmp)
'        If TbColRel.RecordCount > 0 Then
'          NomeCmp = Trim(TbColRel!Nome)
'          NomeCol = Replace(TbColonne!Nome, "_", "-")
'          wPref = Mid$(NomeCol, 1, 4)
'
'          moveString = moveString & vbCrLf & Space(6) & "*  " _
'                    & vbCrLf & Space(11) & "MOVE '" & NomeCol & "' TO WF-NOMECOL" _
'                    & vbCrLf & Space(11)
'
'          If Trim(TbColonne!RoutWrite) <> "" Then
'             If TbColonne!Tipo = "DATE" And TbColRel!Usage = "PCK" Then
'               wStrD = "DATA-8"
'               If TbColonne!RoutWrite = "D2DG" Then
'                  wStrD = "DATA-7"
'               End If
'               moveString = moveString _
'                  & "MOVE '" & Trim(TbColonne!RoutWrite) & "' TO WF-FUNZIONE" _
'                  & vbCrLf & Space(11) & "CALL WF-NOMEROUT USING" _
'                  & vbCrLf & Space(16) & wStrD _
'                  & vbCrLf & Space(16) & NomeCol & " OF RR-" & TbTavole!Nome _
'                  & vbCrLf & Space(16) & "WF-PARAMETRI." _
'                  & vbCrLf & Space(11) & "MOVE " & wStrD & " TO " & NomeCmp & " OF RD-" & segmentName & "." _
'                  & vbCrLf & Space(11)
'             Else
'                moveString = moveString _
'                  & "MOVE '" & Trim(TbColonne!RoutWrite) & "' TO WF-FUNZIONE" _
'                  & vbCrLf & Space(11) & "CALL WF-NOMEROUT USING" _
'                  & vbCrLf & Space(16) & NomeCmp & " OF RD-" & segmentName _
'                  & vbCrLf & Space(16) & NomeCol & " OF RR-" & TbTavole!Nome _
'                  & vbCrLf & Space(16) & "WF-PARAMETRI." _
'                  & vbCrLf & Space(11)
'            End If
'          Else
'             Select Case TbColonne!Tipo
'               Case "DATE"
'                 If TbColRel!Usage = "PCK" Then
'                    moveString = moveString _
'                    & "MOVE 'D2DT' TO WF-FUNZIONE" _
'                    & vbCrLf & Space(11) & "CALL WF-NOMEROUT USING" _
'                    & vbCrLf & Space(16) & "DATA-8" _
'                    & vbCrLf & Space(16) & NomeCol & " OF RR-" & TbTavole!Nome _
'                    & vbCrLf & Space(16) & "WF-PARAMETRI." _
'                    & vbCrLf & Space(11) & "MOVE DATA-8 TO " & NomeCmp & " OF RD-" & segmentName & "." _
'                    & vbCrLf & Space(11)
'                 Else
'                    moveString = moveString _
'                    & "MOVE 'D2DT' TO WF-FUNZIONE" _
'                    & vbCrLf & Space(11) & "CALL WF-NOMEROUT USING" _
'                    & vbCrLf & Space(16) & NomeCmp & " OF RD-" & segmentName _
'                    & vbCrLf & Space(16) & NomeCol & " OF RR-" & TbTavole!Nome _
'                    & vbCrLf & Space(16) & "WF-PARAMETRI." _
'                    & vbCrLf & Space(11)
'                 End If
'               Case "TIME"
'                 moveString = moveString _
'                    & "MOVE 'T2TM' TO WF-FUNZIONE" _
'                    & vbCrLf & Space(11) & "CALL WF-NOMEROUT USING" _
'                    & vbCrLf & Space(16) & NomeCmp & " OF RD-" & segmentName _
'                    & vbCrLf & Space(16) & NomeCol & " OF RR-" & TbTavole!Nome _
'                    & vbCrLf & Space(16) & "WF-PARAMETRI." _
'                    & vbCrLf & Space(11)
'               Case Else
'                  moveString = moveString _
'                              & "MOVE " & NomeCol & " OF RR-" & TbTavole!Nome & " TO " _
'                              & vbCrLf & Space(16) & NomeCmp & " OF RD-" & segmentName & "." _
'                              & vbCrLf & Space(11)
'             End Select
'          End If
'         End If
'        End If
'      End If
'      TbColonne.MoveNext
'    Wend
'    changeTag rtb, "<MOVE-ADL-TO-WK>.", moveString
'
'  End If
End Sub
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' SQ 9-06-06
' GESTIONE OCCURS (TAG: "OCCURS"/"AUTOMATIC-OCCURS")
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub GeneraProgrammaLoad(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset, rsTabelleOccurs As Recordset
  Dim maxLenSeg As Long, dclgenLen As Long
  Dim copyname As String, cCopyParam As String, tCopyParam As String
  Dim env As Collection
  Dim originTable As Boolean, isRedefine As Boolean, isDispatcher As Boolean, ok As Boolean
  Dim FbkLen As Integer
  Dim rsAlias As Recordset
  Dim NomeTb As String
  Dim tbOccurs As String
  'silvia 29-04-2008 selettore-valore
  Dim strSelVal As String
  Dim spselval() As String
  Dim i As Integer, k As Integer
  On Error GoTo errLoad
  
 'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load cos� non va in eoe!
  '****************
  If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\" & dbType & "\LOAD_PROGRAM") <> "" Then
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\" & dbType & "\LOAD_PROGRAM"
  Else
    DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\" & dbType & "\LOAD_PROGRAM"" not found."
    Exit Sub
  End If
  '*****************
  
  cCopyParam = LeggiParam("C-COPY")
  tCopyParam = LeggiParam("T-COPY")
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' SCARTO LE OCCURS... non � vero!...
  ' -> ogni tabella "origine" gestir� le sottotabelle in fondo
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'Set RsTabelle = m_fun.Open_Recordset("select idTable,nome,idOrigine from DMDB2_Tabelle where idDB= " & pIndex_DBCorrente)
  'Set rsTabelle = m_fun.Open_Recordset("select idTable,nome,idOrigine from DMDB2_Tabelle where idDB= " & pIndex_DBCorrente & " and TAG <> 'OCCURS' order by idOrigine")
  
  'tilvia 04-05-2009
  'Set rsTabelle = m_fun.Open_Recordset("select idTable,nome,idOrigine,TAG from " & PrefDb & "_Tabelle where idDB= " & pIndex_DBCorrente & " order by idOrigine,idTable")
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB IN (select IdDB from " & PrefDb & "_DB where idDBOr = " & pIndex_DBDCorrente & ")  order by idOrigine,idTable")

  While Not rsTabelle.EOF
    Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
    If rsAlias.EOF Then
      NomeTb = rsTabelle!Nome
    Else
      NomeTb = rsAlias!Nome
    End If
    rsAlias.Close
    
    isRedefine = False    'init
    isDispatcher = False  'init
    ok = False            'init
    
    'gestione PILOTA+REDEFINES
    If Right(rsTabelle!Tag, 10) = "DISPATCHER" Then 'normalmente ho AUTOMATIC-DISPACTHER
      DoEvents
      'tmp:
      'DISPATCHER da gestire...
      'Scarto quella che per ora � la principale: di troppo!
      'virgilio: ora non la scarto pi� perch� mi serve!
      'rsTabelle.MoveNext
      isDispatcher = True
      'Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & RsTabelle!IdOrigine & " AND Correzione='I'")
      Set rsSegmenti = m_fun.Open_Recordset("select * from PsDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    ElseIf Right(rsTabelle!Tag, 6) <> "OCCURS" Then
      '''''''''''''''''''''''''''''''''''''
      ' TABELLE "OCCURS"
      '''''''''''''''''''''''''''''''''''''
      Set rsTabelleOccurs = m_fun.Open_Recordset("select idTable,nome,idOrigine,Dimensionamento from " & PrefDb & "_Tabelle where idOrigine = " & rsTabelle!IdOrigine & " and TAG like '%OCCURS' order by idTable")
      originTable = Not rsTabelleOccurs.EOF
      
      '''''''''''''''''''''''''''''''''''''
      ' Per ogni TABELLA associata al DBD:
      '''''''''''''''''''''''''''''''''''''
      Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
      If rsSegmenti.RecordCount = 0 Then
        Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
        'tmp
        isRedefine = rsSegmenti!Correzione = "I"
      End If
    End If
    
    'SQ  - Incredibile... cosa mi tocca fare!
    If Not rsSegmenti Is Nothing Then
      ok = rsSegmenti.RecordCount
    End If
    
    If Right(rsTabelle!Tag, 6) = "OCCURS" Or ok Then
      
      'SQ 8-06-06
      'changeTag rtbFile, "<SELECT-SEG(i)>", "SELECT F" & rsSegmenti!Nome & " ASSIGN TO " & rsSegmenti!Nome & "."
      'changeTag rtbFile, "<SELECT-SEG(i)>", "SELECT F" & Replace(rsTabelle!Nome, "_", "") & " ASSIGN TO " & Replace(rsTabelle!Nome, "_", "") & "."
      changeTag RtbFile, "<SELECT-SEG(i)>", "SELECT F" & Replace(NomeTb, "_", "") & " ASSIGN TO " & Replace(NomeTb, "_", "") & "."
      
      'SQ 8-06-06
      'changeTag rtbFile, "<SELECT-CTL(i)>", "SELECT C" & rsSegmenti!Nome & " ASSIGN TO C<ALIAS>."
      'changeTag rtbFile, "<SELECT-CTL(i)>", "SELECT C" & Replace(rsTabelle!Nome, "_", "") & " ASSIGN TO C<ALIAS>."
      changeTag RtbFile, "<SELECT-CTL(i)>", "SELECT C" & Replace(NomeTb, "_", "") & " ASSIGN TO C<ALIAS>."
      
      'changeTag rtbFile, "<ALIAS>", Left(RsTabelle!Nome, 2) & "N" & Mid(RsTabelle!Nome, 10)
      
      dclgenLen = getDclgenLen(rsTabelle!IdTable)
'      changeTag rtbFile, "<FD-SEGMENTO(i)>", "FD F" & Replace(rsTabelle!Nome, "_", "") & vbCrLf & _
'                                  Space(4) & "RECORD CONTAINS " & dclgenLen & " CHARACTERS" & vbCrLf & _
'                                  Space(4) & "LABEL RECORD STANDARD" & vbCrLf & _
'                                  Space(4) & "DATA RECORD REC-F" & Replace(rsTabelle!Nome, "_", "") & "." & vbCrLf & _
'                                             "01  REC-F" & Replace(rsTabelle!Nome, "_", "") & Space(5) & "PIC X(" & dclgenLen & ")."
      changeTag RtbFile, "<FD-SEGMENTO(i)>", "FD F" & Replace(NomeTb, "_", "") & vbCrLf & _
                                  Space(4) & "RECORD CONTAINS " & dclgenLen & " CHARACTERS" & vbCrLf & _
                                  Space(4) & "LABEL RECORD STANDARD" & vbCrLf & _
                                  Space(4) & "DATA RECORD REC-F" & Replace(NomeTb, "_", "") & "." & vbCrLf & _
                                             "01  REC-F" & Replace(NomeTb, "_", "") & Space(5) & "PIC X(" & dclgenLen & ")."
      
'      changeTag rtbFile, "<FD-CTL(i)>", "FD C" & Replace(rsTabelle!Nome, "_", "") & vbCrLf & _
'                                  Space(4) & "RECORD CONTAINS 80 CHARACTERS" & vbCrLf & _
'                                  Space(4) & "LABEL RECORD STANDARD" & vbCrLf & _
'                                  Space(4) & "DATA RECORD REC-C" & Replace(rsTabelle!Nome, "_", "") & "." & vbCrLf & _
'                                        "01  REC-C" & Replace(rsTabelle!Nome, "_", "") & Space(5) & "PIC X(80)."
      changeTag RtbFile, "<FD-CTL(i)>", "FD C" & Replace(NomeTb, "_", "") & vbCrLf & _
                                  Space(4) & "RECORD CONTAINS 80 CHARACTERS" & vbCrLf & _
                                  Space(4) & "LABEL RECORD STANDARD" & vbCrLf & _
                                  Space(4) & "DATA RECORD REC-C" & Replace(NomeTb, "_", "") & "." & vbCrLf & _
                                        "01  REC-C" & Replace(NomeTb, "_", "") & Space(5) & "PIC X(80)."

'      changeTag rtbFile, "<CALL-CTL(i)>", "PERFORM CTL-" & Replace(rsTabelle!Nome, "_", "") & " THRU CTL-" & Replace(rsTabelle!Nome, "_", "") & "-EX."

      ' Mauro 23-04-2007 : Eliminato per ripristinare la chiamata a SECTION
      changeTag RtbFile, "<CALL-CTL(i)>", "PERFORM CTL-" & Replace(NomeTb, "_", "") '& " THRU CTL-" & Replace(NomeTb, "_", "") & "-EX."
      
      
      Set env = New Collection  'resetto;
      Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
      If rsAlias.RecordCount Then
        env.Add rsAlias
      Else
        env.Add rsTabelle
      End If
      env.Add rsSegmenti
      copyname = getEnvironmentParam(LoadCopyParam, "tabelle", env)
      changeTag RtbFile, "<LOAD-COPIES(i)>", "COPY " & copyname & "."
      
      'SQ 22-06-06: A CHI SERVONO?
'''      copyname = LeggiParam("D-COPY")
'''      If Len(copyname) Then
'''        copyname = getEnvironmentParam(copyname, "tabelle", env)
'''        changeTag rtbFile, "<CTL-COPIES(i)>", "COPY " & copyname & "."
'''      Else
'''        MsgBox "tmp: problemi sul paramentro CTL-COPY!!!!!!!!", vbExclamation
'''      End If
                  
      'OPEN FILE
'      changeTag rtbFile, "<OPEN-FILE(i)>", "OPEN OUTPUT F" & Replace(rsTabelle!Nome, "_", "") & "."
'      changeTag rtbFile, "<CLOSE-FILE(i)>", "CLOSE F" & Replace(rsTabelle!Nome, "_", "")
      changeTag RtbFile, "<OPEN-FILE(i)>", "OPEN OUTPUT F" & Replace(NomeTb, "_", "") & "."
      changeTag RtbFile, "<CLOSE-FILE(i)>", "CLOSE F" & Replace(NomeTb, "_", "")

      'DISPLAY CONTATORI
'      changeTag rtbFile, "<DISPL-OUTPUT(i)>", "DISPLAY 'Record  F" & Replace(rsTabelle!Nome, "_", "") & " : ' CTR-" & Replace(rsTabelle!Nome, "_", "")
'      changeTag rtbFile, "<CTR-OUTPUT(i)>", "01  CTR-" & Replace(rsTabelle!Nome, "_", "") & " PIC 9(8) VALUE ZERO."
      changeTag RtbFile, "<DISPL-OUTPUT(i)>", "DISPLAY 'Table  F" & Replace(NomeTb, "_", "") & " : ' CTR-" & Replace(NomeTb, "_", "")
      changeTag RtbFile, "<CTR-OUTPUT(i)>", "01  CTR-" & Replace(NomeTb, "_", "") & " PIC 9(8) VALUE ZERO."
  
      Virtuale = False  '??
  
      'Case "banca.mty"
      'Set rsArea = m_fun.Open_Recordset("select NomeCmp from MgRel_SegAree where Stridarea = 1 and idSegmento = " & RsTabelle!IdOrigine)
      '...
      'SQ (Roby)
      'wNomeArea = "AREA-" & Replace(RsTabelle!Nome, "_", "")
            
'      copyName = LeggiParam("DLI-COPY")
'      If Len(copyName) Then
'        changeTag rtbFile, "<DLI-COPIES(i)>", "COPY " & getEnvironmentParam(copyName, "tabelle", env) & "."
'      Else
'        m_fun.WriteLog "Parametro DLI-COPY non definito."
'      End If
      'changeTag rtbFile, "<INIT-C-COPIES>", "INITIALIZE " & getAreaCcopy(rsTabelle!Nome) & "." & vbCrLf & "<INIT-C-COPIES>"
      
      If Len(cCopyParam) Then
        changeTag RtbFile, "<C-COPIES(i)>", "COPY " & getEnvironmentParam(cCopyParam, "tabelle", env) & "."
      Else
        m_fun.WriteLog "Parametro C-COPY non definito."
      End If
      'SQ 21-11-07 Problema INITIALIZE dentro la COPY MOVE:
      'La facciamo una volta per tutte alla INIT e lo togliamo dal ...-TO-RR (soluzione definitiva?)
      changeTag RtbFile, "<INITIALIZE-RR(i)>", "INITIALIZE RR-" & Replace(NomeTb, "_", "-")
                
      If Right(rsTabelle!Tag, 6) <> "OCCURS" Then 'es: "AUTOMATIC-OCCURS"
      
        If Len(tCopyParam) Then
          changeTag RtbFile, "<T-COPIES(i)>", "COPY " & getEnvironmentParam(tCopyParam, "tabelle", env) & "."
        Else
          m_fun.WriteLog "Parametro T-COPY non definito."
        End If
        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Segmento di lunghezza max. Serve per l'unica area host di lettura
        ' SQ: a chi serve?
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If rsSegmenti!MaxLen > maxLenSeg Then
          maxLenSeg = rsSegmenti!MaxLen
        End If
        
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' VIRGILIO 28/2/2007
        ' SELETTORE-IMS dovrebbe essere per eventuali Segmenti virtuali... chiarire quando si gestiranno
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If rsSegmenti!IdSegmentoOrigine = 0 Then
          FbkLen = Calcola_FBK_Len(TN(rsSegmenti!IdSegmento, "LONG"), FbkLen)
          changeTag RtbFile, "<WHENFBK(i)>", "WHEN '" & rsSegmenti!Nome & "'" & vbCrLf & _
                             Space(4) & "MOVE " & FbkLen & " TO FBK-LEN "
        End If
        
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' MOVE DI LEGGI-IMS
        ' SELETTORE-IMS dovrebbe essere per eventuali Segmenti virtuali... chiarire quando si gestiranno
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If rsSegmenti!IdSegmentoOrigine = 0 Then
          ''''''''''''''''''''''''''''''''''''''''''
          ' SQ - 8-01-07
          ' Gestione Segmenti a lunghezza variabile: inizializzazione + move posizionale (aggiunta variabile LEN-SEGM a DCP)
          ''''''''''''''''''''''''''''''''''''''''''
          changeTag RtbFile, "<WHEN(i)>", _
            "WHEN '" & rsSegmenti!Nome & "'" & vbCrLf & _
            "    MOVE SPACES TO AREA-" & rsSegmenti!Nome & vbCrLf & _
            "    MOVE AREA-DATI(1:" & rsSegmenti!MaxLen & ") TO AREA-" & rsSegmenti!Nome & vbCrLf & _
            "    MOVE '" & rsSegmenti!Nome & "' TO WF-SEGMENTO " & vbCrLf & _
            "    IF AREA-" & rsSegmenti!Nome & " = ALL '9' THEN " & vbCrLf & _
            "       MOVE 1 TO SW-SCARTO " & vbCrLf & _
            "    END-IF "
        End If
  
        If rsSegmenti!IdSegmentoOrigine = 0 Then
          'changeTag rtbFile, "<INIT-REVKEY(i)>", "grifa faggianoooo"
          If isDispatcher Then
            InserisciChiavePrimariaRedefines RtbFile, rsSegmenti, PrefDb 'virgilio: vogliamo metterli su tutti gli accessi in comune (mezzo gaudio)
          Else
            'GESTIONE OCCURS
            InserisciChiavePrimaria RtbFile, rsSegmenti, rsTabelle, originTable
          End If
          
        End If
        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' OCCURS: GESTIONE SOTTOTABELLE
        ' SONO SULLA TABELLA ORIGINE: SOPRA HO PREPARATO I TAG CICLICI PER LE SOTTOTABELLE
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If originTable Then
          While Not rsTabelleOccurs.EOF
            Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelleOccurs!IdTable)
            'SILVIA nome delle child
            If Left(m_fun.FnNomeDB, 14) = "prj-RealeMutua" Then
              tbOccurs = rsTabelleOccurs!Nome
            Else
              If rsAlias.EOF Then
                tbOccurs = rsTabelle!Nome
              Else
                tbOccurs = rsAlias!Nome
              End If
            End If
            rsAlias.Close
            changeTag RtbFile, "<OCCURS-MOVE(i)>", "MOVE '" & tbOccurs & "' TO WK-TABLE-OCCURS" & vbCrLf & _
                                          "PERFORM SCRIVI-OUT-OCCURS"
                      
            'silvia 1-4-2008 : cambiata la funzione getOccursLen
'''            changeTag RtbFile, "<WHEN-TABLE-OCCURS(i)>", "WHEN '" & tbOccurs & "'" & vbCrLf _
'''                                            & Space(4) & "PERFORM VARYING WINDICE FROM 1 BY 1" & vbCrLf _
'''                                            & Space(4) & "   UNTIL WINDICE > " & getOccursLen(rsTabelleOccurs!Dimensionamento) & vbCrLf _
'''                                            & Space(4) & "   MOVE WINDICE TO WK-KEYSEQ" & vbCrLf _
'''                                            & Space(4) & "   PERFORM " & Replace(tbOccurs, "_", "-") & "-TO-RR" & vbCrLf _
'''                                            & Space(4) & "   ADD 1 TO CTR-" & Replace(tbOccurs, "_", "") & vbCrLf _
'''                                            & Space(4) & "   WRITE  REC-F" & Replace(rsTabelle!Nome, "_", "") & " FROM RR-" & Replace(rsTabelle!Nome, "_", "-") & vbCrLf _
'''                                            & Space(4) & "END-PERFORM"
            
            'silvia 29-04-2008 gestione condizione campi occurs
            strSelVal = getOccurExit(rsTabelleOccurs!Dimensionamento, rsTabelleOccurs!IdOrigine)
            i = IIf(strSelVal <> "", 3, 0)
            spselval = Split(strSelVal, vbCrLf)
            If UBound(spselval) > 0 Then
              strSelVal = ""
              For k = 0 To UBound(spselval)
                strSelVal = strSelVal & spselval(k) & vbCrLf & Space(9)
              Next
            End If
            changeTag RtbFile, "<WHEN-TABLE-OCCURS(i)>", "WHEN '" & tbOccurs & "'" & vbCrLf _
                                            & Space(4) & "PERFORM VARYING WINDICE FROM 1 BY 1" & vbCrLf _
                                            & Space(4) & "   UNTIL WINDICE > " & getOccursLen(rsTabelleOccurs!Dimensionamento, rsTabelleOccurs!IdOrigine) & vbCrLf _
                                            & IIf(strSelVal <> "", Space(7) & "IF " & Trim(strSelVal), "") _
                                            & Space(4 + i) & "   MOVE WINDICE TO WK-KEYSEQ" & vbCrLf _
                                            & Space(4 + i) & "   PERFORM " & Replace(tbOccurs, "_", "-") & "-TO-RR" & vbCrLf _
                                            & Space(4 + i) & "   ADD 1 TO CTR-" & Replace(tbOccurs, "_", "") & vbCrLf _
                                            & Space(4 + i) & "   WRITE  REC-F" & Replace(tbOccurs, "_", "") & " FROM RR-" & Replace(tbOccurs, "_", "-") & vbCrLf _
                                            & IIf(strSelVal <> "", Space(4) & Space(3) & "END-IF" & vbCrLf, "") _
                                            & Space(4) & "END-PERFORM"
                        
            rsTabelleOccurs.MoveNext
          Wend
          '<OCCURS-MOVE(i)> � "ciclico parziale": muore a fine ciclo interno
          DelParola RtbFile, "<OCCURS-MOVE(i)>"
        End If
      End If
    End If
    rsTabelle.MoveNext
  Wend
    
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  '  Set rsSegmenti = m_fun.Open_Recordset("SELECT * FROM mgDLI_Segmenti WHERE idoggetto=" & TbApp!Iddbor)
  '  If rsSegmenti.RecordCount <> 0 Then
  '... SQ: c'era un sacco di roba...
  ' End If
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
  
  changeTag RtbFile, "<DATA-CREAZ>", Date
  
  changeTag RtbFile, "<DATA-MODIF>", Date
  
  changeTag RtbFile, "<MAX-LEN-SEG>", maxLenSeg & ""
   
  'GenFileGuida (RtbFile, RecOgg!IdOggetto)
  
  'Aggiornamento PrgMigr, LblMigr
  
  cleanTags RtbFile
  
  ''''''''''''''''''''''''''''''''''''''''''''''''
  ' NOME PROGRAMMA:
  ''''''''''''''''''''''''''''''''''''''''''''''''
  Set env = New Collection 'resetto
  Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias ")
  env.Add NomeTb
  env.Add rsSegmenti
  copyname = getEnvironmentParam(LoadProgramParam, "tabelle", env)
  changeTag RtbFile, "<NOME-CBL>", copyname

  RtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\" & dbType & "\Cbl\" & copyname, 1
  rsAlias.Close
  rsTabelle.Close
  
  Exit Sub
errLoad:
  Select Case ERR.Number
    Case 75
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\" & dbType & "\LOAD_PROGRAM") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\" & dbType & "\LOAD_PROGRAM"" not found."
      Else
      
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Sub

'VIRGILIO 28/2/2007
Public Function Calcola_FBK_Len(idSeg As Long, FbkLen As Integer) As Integer
  Dim rs As Recordset, rP As Recordset
  Dim Length As Integer, WnomeParent As String
  
  Set rs = m_fun.Open_Recordset("Select * From psdli_field as a, PSDli_Segmenti as b Where " & _
                                "b.idsegmento = " & idSeg & " and a.idsegmento = b.idsegmento and seq = true ")
  If Not rs.EOF Then
    WnomeParent = rs!Parent
    Length = rs!lunghezza

    If WnomeParent = "0" Then
      Calcola_FBK_Len = rs!lunghezza
    Else
      Calcola_FBK_Len = Length + Get_Parent_Len(WnomeParent, 0)
    End If
  Else
    Set rP = m_fun.Open_Recordset("Select * From PSDli_Segmenti as b Where b.idsegmento = " & idSeg)
    If Not rP.EOF Then
      WnomeParent = rP!Parent
      If WnomeParent = "0" Then
        Stop
      Else
        Calcola_FBK_Len = Length + Get_Parent_Len(WnomeParent, 0)
      End If
    End If
    rP.Close
  End If
  rs.Close
   
End Function

'VIRGILIO 28/2/2007
Public Function Get_Parent_Len(NomeSeg As String, Wpar As Integer) As Integer
  Dim rs As Recordset, rP As Recordset
  
  Set rP = m_fun.Open_Recordset("Select * From psdli_segmenti as a, bs_oggetti as b Where " & _
                                "a.Nome = '" & NomeSeg & "' and a.idoggetto = b.idoggetto and " & _
                                "b.tipo = 'DBD' and b.tipo_dbd <> 'ind' and b.tipo_dbd <> 'Log'")
  If Not rP.EOF Then
    Set rs = m_fun.Open_Recordset("Select a.lunghezza, b.parent From psdli_field as a, PSDli_Segmenti as b Where " & _
                                  "b.idsegmento = " & rP!IdSegmento & " and a.idsegmento = b.idsegmento and a.seq = true")
    If Not rs.EOF Then
      If rs!Parent = "0" Then
        Get_Parent_Len = rs!lunghezza + Wpar
      Else
        Get_Parent_Len = Get_Parent_Len + Get_Parent_Len(rs!Parent, rs!lunghezza)
      End If
    Else
      'Stop
      Get_Parent_Len = 0
    End If
    rs.Close
  End If
  rP.Close
End Function

Public Sub GeneraProgrammaLoad_PLI(RtbFile As RichTextBox)
  Dim rsTabelle As Recordset, rsSegmenti As Recordset, rsTabelleOccurs As Recordset, rsSegmDisp As Recordset
  Dim maxLenSeg As Long, dclgenLen As Integer
  Dim copyname As String, cCopyParam As String, tCopyParam As String, stareaDisp As String
  Dim env As Collection
  Dim originTable As Boolean, isRedefine As Boolean, isDispatcher As Boolean, ok As Boolean
  'tilvia 2-4-2009
  Dim rsAlias As Recordset
  Dim NomeTb As String
  Dim tbOccurs As String
  Dim strSelVal As String
  Dim spselval() As String
  Dim i As Integer, k As Integer
    
  On Error GoTo errLoad
  
   'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
  '****************
  If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\PLI\" & dbType & "\LOAD_PROGRAM") <> "" Then
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\PLI\" & dbType & "\LOAD_PROGRAM"
  Else
    DataMan.DmFinestra.ListItems.Add , , Now & "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\PLI\" & dbType & "\LOAD_PROGRAM"" not found."
    Exit Sub
  End If
  
  'RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\PLI\" & dbType & "\LOAD_PROGRAM"
  '*****************
  
  cCopyParam = LeggiParam("C-COPY")
  tCopyParam = LeggiParam("T-COPY")
  
  'tilvia 04-05-2009
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB= " & pIndex_DBCorrente & " order by idOrigine,idTable")
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "idDB IN (select IdDB from " & PrefDb & "_DB where " & _
                                       "idDBOr = " & pIndex_DBDCorrente & ")  order by idOrigine,idTable")
  While Not rsTabelle.EOF
    stareaDisp = ""

    Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
    If rsAlias.EOF Then
      NomeTb = rsTabelle!Nome
    Else
      NomeTb = rsAlias!Nome
    End If
    rsAlias.Close
    
    isRedefine = False    'init
    isDispatcher = False  'init
    ok = False            'init
    
    'gestione PILOTA+REDEFINES
    If Right(rsTabelle!Tag, 10) = "DISPATCHER" Then 'normalmente ho AUTOMATIC-DISPACTHER
      DoEvents
      'tmp:
      'DISPATCHER da gestire...
      'Scarto quella che per ora � la principale: di troppo!
      'rsTabelle.MoveNext
      isDispatcher = True
      'Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & RsTabelle!IdOrigine & " AND Correzione='I'")
      Set rsSegmenti = m_fun.Open_Recordset("select * from PsDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      'TILVIA : Area Dispatcher
      Set rsSegmDisp = m_fun.Open_Recordset("select Nome from MGDLI_Segmenti where IdSegmentoOrigine = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      While Not rsSegmDisp.EOF
        stareaDisp = stareaDisp & IIf(Len(stareaDisp) > 50, vbCrLf, "") & "AREA_" & rsSegmDisp!Nome & ","
        rsSegmDisp.MoveNext
      Wend
      If Len(stareaDisp) Then
        stareaDisp = Left(stareaDisp, Len(stareaDisp) - 1)
      End If
      rsSegmDisp.Close
    ElseIf Right(rsTabelle!Tag, 6) <> "OCCURS" Then
      '''''''''''''''''''''''''''''''''''''
      ' TABELLE "OCCURS"
      '''''''''''''''''''''''''''''''''''''
      Set rsTabelleOccurs = m_fun.Open_Recordset("select idTable,nome,idOrigine,Dimensionamento from " & PrefDb & "_Tabelle where " & _
                                                 "idOrigine = " & rsTabelle!IdOrigine & " and TAG like '%OCCURS' order by idTable")
      originTable = Not rsTabelleOccurs.EOF
      
      '''''''''''''''''''''''''''''''''''''
      ' Per ogni TABELLA associata al DBD:
      '''''''''''''''''''''''''''''''''''''
      Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      If rsSegmenti.RecordCount = 0 Then
        Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
        'tmp
        If rsSegmenti.RecordCount Then
          isRedefine = rsSegmenti!Correzione = "I"
        End If
      End If
    End If
    
    'SQ  - Incredibile... cosa mi tocca fare!
    If Not rsSegmenti Is Nothing Then
      ok = rsSegmenti.RecordCount
    End If
    
    If Right(rsTabelle!Tag, 6) = "OCCURS" Or ok Then
      
      'MG 130207
      'tilvia 2-4-2009
      ''changeTag RtbFile, "<SELECT-SEG(i)>", "DCL F" & rsSegmenti!Nome & " FILE RECORD OUTPUT ;"
      changeTag RtbFile, "<SELECT-SEG(i)>", "DCL " & NomeTb & " FILE RECORD OUTPUT ;"
      
      
      dclgenLen = getDclgenLen(rsTabelle!IdTable)
      'tilvia 2-4-2009
''      changeTag RtbFile, "<FD-SEGMENTO(i)>", "DCL  REC_" & Replace(rsSegmenti!Nome, "_", "") & vbCrLf & _
''                                  Space(4) & "CHAR(" & dclgenLen & ") INIT('');"
      changeTag RtbFile, "<FD-SEGMENTO(i)>", "DCL  REC_" & NomeTb & vbCrLf & _
                                  Space(4) & "CHAR(" & dclgenLen & ") INIT('');"
      
      Set env = New Collection  'resetto;
      env.Add rsTabelle
      env.Add rsSegmenti
      copyname = getEnvironmentParam(LoadCopyParam, "tabelle", env)
      changeTag RtbFile, "<LOAD-COPIES(i)>", "% INCLUDE " & copyname & ";"
                  
      'OPEN FILE
      changeTag RtbFile, "<OPEN-FILE(i)>", "OPEN FILE(" & NomeTb & ") ;"
      changeTag RtbFile, "<CLOSE-FILE(i)>", "CLOSE FILE(" & NomeTb & ") ;"

      'DISPLAY CONTATORI
      changeTag RtbFile, "<DISPL-OUTPUT(i)>", "PUT SKIP EDIT ('Table  " & NomeTb & " : ' !! CTR_" & NomeTb & ") (A)  ;"
      changeTag RtbFile, "<CTR-OUTPUT(i)>", "DCL CTR_" & NomeTb & " PIC  '(8)9' INIT (0);"
  
      Virtuale = False  '??
  
      
      If Len(cCopyParam) Then
        changeTag RtbFile, "<C-COPIES(i)>", "% INCLUDE " & getEnvironmentParam(cCopyParam, "tabelle", env) & ";"
      Else
        m_fun.WriteLog "Parametro C-COPY non definito."
      End If
                
      If Right(rsTabelle!Tag, 6) <> "OCCURS" Then 'es: "AUTOMATIC-OCCURS"
      
        If Len(tCopyParam) Then
          changeTag RtbFile, "<T-COPIES(i)>", "% INCLUDE " & getEnvironmentParam(tCopyParam, "tabelle", env) & ";"
        Else
          m_fun.WriteLog "Parametro T-COPY non definito."
        End If
        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Segmento di lunghezza max. Serve per l'unica area host di lettura
        ' SQ: a chi serve?
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If rsSegmenti!MaxLen > maxLenSeg Then
          maxLenSeg = rsSegmenti!MaxLen
        End If
        
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' MOVE DI LEGGI-IMS
        ' SELETTORE-IMS dovrebbe essere per eventuali Segmenti virtuali... chiarire quando si gestiranno
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If rsSegmenti!IdSegmentoOrigine = 0 Then
          changeTag RtbFile, "<WHEN(i)>", Space(4) & "WHEN ('" & rsSegmenti!Nome & "') DO ; " & vbCrLf _
                              & Space(4) & "   AREA_" & rsSegmenti!Nome & " = SUBSTR(AREA_DATI,1,SEGM_LEN) ; " & vbCrLf _
                              & Space(4) & IIf(stareaDisp <> "", "   " & stareaDisp & " = " & "AREA_" & rsSegmenti!Nome & " ;" & vbCrLf, "") _
                              & Space(4) & "   WF_SEGMENTO = '" & rsSegmenti!Nome & "' ; " & vbCrLf _
                              & Space(4) & "   I=VERIFY(AREA_" & rsSegmenti!Nome & ",'9') ;" & vbCrLf _
                              & Space(4) & "   IF I = 0 THEN DO ;" & vbCrLf _
                              & Space(4) & "      SW_SCARTO = 1 ;" & vbCrLf _
                              & Space(4) & "   END ;" & vbCrLf _
                              & Space(4) & "END ;"
        End If
  
        If rsSegmenti!IdSegmentoOrigine = 0 Then
          If isDispatcher Then
            InserisciChiavePrimariaRedefines_PLI RtbFile, rsSegmenti
          Else
            'GESTIONE OCCURS
            InserisciChiavePrimaria_PLI RtbFile, rsSegmenti, rsTabelle, originTable
          End If
        End If
        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' OCCURS: GESTIONE SOTTOTABELLE
        ' SONO SULLA TABELLA ORIGINE: SOPRA HO PREPARATO I TAG CICLICI PER LE SOTTOTABELLE
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If originTable Then
          While Not rsTabelleOccurs.EOF
            Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelleOccurs!IdTable)
            'SILVIA nome delle child
            If rsAlias.EOF Then
              tbOccurs = rsTabelle!Nome
            Else
              tbOccurs = rsAlias!Nome
            End If
            rsAlias.Close
          
            
            changeTag RtbFile, "<OCCURS-MOVE(i)>", "WK_TABLE_OCCURS = '" & rsTabelleOccurs!Nome & "' ;" & vbCrLf & _
                                          "CALL SCRIVI_OUT_OCCURS ;"
         
''            changeTag RtbFile, "<WHEN-TABLE-OCCURS(i)>", "WHEN '" & rsTabelleOccurs!Nome & "'" & vbCrLf _
''                                            & Space(4) & "PERFORM VARYING WINDICE FROM 1 BY 1" & vbCrLf _
''                                            & Space(4) & "   UNTIL WINDICE > " & getOccursLen(rsTabelleOccurs!Dimensionamento) & vbCrLf _
''                                            & Space(4) & "   MOVE WINDICE TO WK-KEYSEQ" & vbCrLf _
''                                            & Space(4) & "   PERFORM 9999-" & Replace(rsTabelleOccurs!Nome, "_", "-") & "-TO-RR" & vbCrLf _
''                                            & Space(4) & "   ADD 1 TO CTR-" & Replace(rsTabelleOccurs!Nome, "_", "") & vbCrLf _
''                                            & Space(4) & "   WRITE  REC-F" & Replace(rsTabelleOccurs!Nome, "_", "") & " FROM RR-" & Replace(rsTabelleOccurs!Nome, "_", "-") & vbCrLf _
''                                            & Space(4) & "END-PERFORM"
                        
            'silvia 29-04-2008 gestione condizione campi occurs
            strSelVal = getOccurExit(rsTabelleOccurs!Dimensionamento, rsTabelleOccurs!IdOrigine)
            i = IIf(strSelVal <> "", 3, 0)
            spselval = Split(strSelVal, vbCrLf)
            If UBound(spselval) > 0 Then
              strSelVal = ""
              For k = 0 To UBound(spselval)
                strSelVal = strSelVal & spselval(k) & vbCrLf & Space(9)
              Next
            End If
            
            changeTag RtbFile, "<WHEN-TABLE-OCCURS(i)>", "WHEN ('" & rsTabelleOccurs!Nome & "') DO ; " & vbCrLf _
                                            & Space(4) & "DO WINDICE = 1 TO " & getOccursLen(rsTabelleOccurs!Dimensionamento, rsTabelleOccurs!IdOrigine) & ";" & vbCrLf _
                                            & IIf(strSelVal <> "", Space(7) & "IF " & Trim(strSelVal) & " THEN DO;", "") & vbCrLf _
                                            & Space(4 + i) & "   WK_KEYSEQ = WINDICE;" & vbCrLf _
                                            & Space(4 + i) & "   CALL " & rsTabelleOccurs!Nome & "_TO_RR;" & vbCrLf _
                                            & Space(4 + i) & "   CTR_" & rsTabelleOccurs!Nome & " = CTR_" & rsTabelleOccurs!Nome & " + 1 ;" & vbCrLf _
                                            & Space(4 + i) & "   WRITE FILE (" & rsTabelleOccurs!Nome & ") FROM (RR_" & rsTabelleOccurs!Nome & ");" & vbCrLf _
                                            & IIf(strSelVal <> "", Space(4) & Space(3) & "END;" & vbCrLf, "") _
                                            & Space(4) & "END;" & vbCrLf _
                                            & "END;"
            
            
            rsTabelleOccurs.MoveNext
          Wend
          '<OCCURS-MOVE(i)> � "ciclico parziale": muore a fine ciclo interno
          DelParola RtbFile, "<OCCURS-MOVE(i)>"
        End If
      End If
    End If
    rsTabelle.MoveNext
  Wend
    
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'  Set rsSegmenti = m_fun.Open_Recordset("SELECT * FROM mgDLI_Segmenti WHERE idoggetto=" & TbApp!Iddbor)
'  If rsSegmenti.RecordCount <> 0 Then
'... SQ: c'era un sacco di roba...
' End If
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
  
  changeTag RtbFile, "<DATA-CREAZ>", Date
  
  changeTag RtbFile, "<DATA-MODIF>", Date
  
  changeTag RtbFile, "<MAX-LEN-SEG>", maxLenSeg & ""
   
  'GenFileGuida (RtbFile, RecOgg!IdOggetto)
  
  'Aggiornamento PrgMigr, LblMigr
  
  cleanTags RtbFile
  
  'DCP name
  Set env = New Collection 'resetto
  env.Add NomeTb
  env.Add rsSegmenti
  Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias ")
  copyname = getEnvironmentParam(LoadProgramParam, "tabelle", env)
  changeTag RtbFile, "<NOME-CBL>", copyname
  
  RtbFile.SaveFile DataMan.DmPathDb & "\Output-Prj\" & dbType & "\PLI\" & copyname, 1
  rsAlias.Close
  rsTabelle.Close
  Exit Sub
errLoad:
  Select Case ERR.Number
    Case 75
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\" & dbType & "\LOAD_PROGRAM") = "" Then
'        ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\dcp\" & dbType & "\LOAD_PROGRAM"" not found."
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\DB2\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\BtRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\CxRout"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cbl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Cpy"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Jcl"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\PLI"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Include"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\VSAM"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\PK"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\IX"
        m_fun.MkDir_API DataMan.DmPathDb & "\Output-Prj\ORACLE\Sql\FK"
        Resume
      End If
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Sub
' Non viene mai utilizzata
''Public Sub CreaDirDli()
''  Dim wPath As String
''
''  On Error Resume Next
''  wPath = DataMan.DmParam_PercOut
''  MkDir wPath
''  If TipoDB = "DB2" Then
''    wPath = DataMan.DmPathDb & "\Output-Prj\DB2"
''  End If
''  If TipoDB = "ORACLE" Then
''    wPath = DataMan.DmPathDb & "\Output-Prj\ORACLE"
''  End If
''  MkDir wPath
''  MkDir wPath & "\Cpy"
''  MkDir wPath & "\Cbl"
''  MkDir wPath & "\Sql"
''  MkDir wPath & "\Jcl"
''  MkDir wPath & "\BtRout"
''  MkDir wPath & "\CxRout"
''End Sub
Sub CancellaParoleChiave(rtb As RichTextBox)
  DelParola rtb, "<SELECT-SEG>."
  DelParola rtb, "<SELECT-CTL>."
  'DelParola RTB, "<FD-SEGMENTO>."
  DelParola rtb, "<FD-CTL>."
  DelParola rtb, "<C-COPIES>"
  DelParola rtb, "<T-COPIES>"
  DelParola rtb, "<DLI-COPIES>"
  DelParola rtb, "<CALL-CTL>."
  DelParola rtb, "<CTL-COPY>."
  DelParola rtb, "<LOAD-COPY>."
  DelParola rtb, "<OPEN-FILE>."
  DelParola rtb, "<CLOSE-FILE>."
  DelParola rtb, "<STRUTTURA-ASSOCIATA>"
  DelParola rtb, "<WHEN>."
  DelParola rtb, "<WHEN-TABLE>."
  DelParola rtb, "<CTR-OUTPUT>"
  DelParola rtb, "<DISPL-OUTPUT>"
  DelParola rtb, "<INIT-C-COPIES>"
  DelParola rtb, "<WK-ID_BPER(i)>"
End Sub
Sub InserisciCopyTable(RtbFile As RichTextBox, tb As Recordset, FWord As String, Tipo As String)
  Dim TbTable As Recordset
  Dim TbOgg As Recordset
  Dim NomeCopy0 As String
  Dim NomeCopy1 As String
  
  Set TbTable = m_fun.Open_Recordset("select * from DMDB2_Tabelle where IdOrigine= " & tb!IdSegmento)
  If TbTable.RecordCount > 0 Then
    Set TbOgg = m_fun.Open_Recordset("select * from bs_oggetti where idoggetto = " & tb!IdOggetto)
    If TipoDB = "DB2" Then
      Select Case Tipo
        Case "CPLD"
          NomeCopy0 = "TD" & Mid$(TbOgg!Nome, 1, 2) & Mid$(tb!Nome, 5)
          NomeCopy1 = "C" & Mid$(TbTable!Nome, 2)
          InsertParola RtbFile, FWord, "COPY " & NomeCopy0 & "." _
                       & vbCrLf & Space(11) & "COPY " & NomeCopy1 & "." _
                       & vbCrLf & Space(11) & FWord
        Case "CPMV"
          NomeCopy0 = "M" & Mid$(TbTable!Nome, 2)
          InsertParola RtbFile, FWord, "COPY " & NomeCopy0 & "." & vbCrLf & Space(11) & FWord
      End Select
    End If
  End If
End Sub

Sub InserisciChiavePrimaria(rtb As RichTextBox, rsSegmenti As Recordset, rsTabelle As Recordset, originTable As Boolean)
  Dim rsOrigine As Recordset, rsColonne As Recordset
  Dim NomeTb As String
  Dim rsAlias As Recordset
  
  Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
  If rsAlias.EOF Then
    NomeTb = rsTabelle!Nome
  Else
    NomeTb = rsAlias!Nome
  End If
  rsAlias.Close
  
  ''''''''''''''''''''''''''''''''''
  ' GESTIONE REVKEY (chiavi non univoche)
  ' SQ 3-01-08
  ' -> variabile d'appoggio WK-REVKEY (perch� c'� INITIALIZE RR)
  ''''''''''''''''''''''''''''''''''
  Dim withREVKEY As Boolean
  Set rsColonne = m_fun.Open_Recordset("SELECT count(*) as revkeyNumber FROM " & PrefDb & "_Colonne where IdTable=" & rsTabelle!IdTable & " and Nome='" & PkNameParam & "' and idCmp=-1")
  withREVKEY = rsColonne!revkeyNumber > 0
  rsColonne.Close
  
  Dim when_Table As String
  'utilizzare --^ !!
  
  If originTable Then
    ' SILVIA 10-03-2008
    changeTag rtb, "<WHEN-TABLE(i)>", "WHEN '" & rsSegmenti!Nome & "'" & vbCrLf _
      & IIf(withREVKEY, Space(4) & "COMPUTE WK-REVKEY = " & PkNameParam & " OF RR-" & Replace(rsTabelle!Nome, "_", "-") & " + 1" & vbCrLf, "") _
      & Space(4) & "PERFORM " & Replace(NomeTb, "_", "-") & "-TO-RR" & vbCrLf _
      & Space(4) & "<OCCURS-MOVE(i)>" & vbCrLf _
      & Space(4) & "ADD 1 TO CTR-" & Replace(NomeTb, "_", "") & vbCrLf _
      & Space(4) & "WRITE  REC-F" & Replace(NomeTb, "_", "-") & "  FROM  RR-" & Replace(rsTabelle!Nome, "_", "-") & vbCrLf _
      & Space(4) & "<INIT-REVKEY(i)>"   'virgilio
      '& Space(4) & "<AGGIORNA-CHIAVE>." & vbCrLf _
      & Space(4) & "WRITE  REC-F" & rsSegmenti!Nome & "  FROM  RR-" & rsTabelle!Nome
  Else
    ' SQ 3-01-08
    changeTag rtb, "<WHEN-TABLE(i)>", "WHEN '" & rsSegmenti!Nome & "'" & vbCrLf _
      & IIf(withREVKEY, Space(4) & "COMPUTE WK-REVKEY = " & PkNameParam & " OF RR-" & Replace(rsTabelle!Nome, "_", "-") & " + 1" & vbCrLf, "") _
      & Space(4) & "PERFORM " & Replace(NomeTb, "_", "-") & "-TO-RR" & vbCrLf _
      & Space(4) & "ADD 1 TO CTR-" & Replace(NomeTb, "_", "") & vbCrLf _
      & Space(4) & "WRITE  REC-F" & Replace(NomeTb, "_", "") & "  FROM  RR-" & Replace(rsTabelle!Nome, "_", "-") & vbCrLf _
      & Space(4) & "<INIT-REVKEY(i)>"   'virgilio
  End If
  
'''' 'virgilio
  getRevKey rsSegmenti!IdSegmento, rtb

End Sub

Sub InserisciChiavePrimaria_PLI(rtb As RichTextBox, rsSegmenti As Recordset, rsTabelle As Recordset, originTable As Boolean)
  Dim rsOrigine As Recordset, rsColonne As Recordset
  Dim NomeTb As String
  Dim rsAlias As Recordset
  
  Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
  If rsAlias.EOF Then
    NomeTb = rsTabelle!Nome
  Else
    NomeTb = rsAlias!Nome
  End If
  rsAlias.Close
  On Error GoTo ErrorHandler
  Dim withREVKEY As Boolean
  Set rsColonne = m_fun.Open_Recordset("SELECT count(*) as revkeyNumber FROM " & PrefDb & "_Colonne where IdTable=" & rsTabelle!IdTable & " and Nome='" & PkNameParam & "' and idCmp=-1")
  withREVKEY = rsColonne!revkeyNumber > 0
  rsColonne.Close
                      
 'TILVIA 05-05-2009
  If originTable Then
    changeTag rtb, "<WHEN-TABLE(i)>", "WHEN ('" & rsSegmenti!Nome & "') DO ;" & vbCrLf _
    & IIf(withREVKEY, Space(4) & " WK_REVKEY = RR_" & rsTabelle!Nome & "." & PkNameParam & " + 1 ;" & vbCrLf, "") _
                    & Space(4) & "CALL " & NomeTb & "_TO_RR ;" & vbCrLf _
                    & Space(4) & "<OCCURS-MOVE(i)>" & vbCrLf _
                    & Space(4) & "CTR_" & NomeTb & " = CTR_" & NomeTb & " + 1 ;" & vbCrLf _
                    & Space(4) & "REC_" & NomeTb & " = RA_" & NomeTb & " ;" & vbCrLf _
                    & Space(4) & "WRITE FILE (" & NomeTb & ") FROM (REC_" & NomeTb & ") ;" & vbCrLf _
                    & Space(4) & "<INIT-REVKEY(i)>" & vbCrLf _
                    & "END ;"
  Else
    changeTag rtb, "<WHEN-TABLE(i)>", "WHEN ('" & rsSegmenti!Nome & "') DO ;" & vbCrLf _
    & IIf(withREVKEY, Space(4) & " WK_REVKEY = RR_" & rsTabelle!Nome & "." & PkNameParam & " + 1 ;" & vbCrLf, "") _
                    & Space(4) & "CALL " & NomeTb & "_TO_RR; " & vbCrLf _
                    & Space(4) & "CTR_" & NomeTb & " = CTR_" & NomeTb & " + 1 ;" & vbCrLf _
                    & Space(4) & "REC_" & NomeTb & " = RA_" & NomeTb & " ;" & vbCrLf _
                    & Space(4) & "WRITE FILE (" & NomeTb & ") FROM (REC_" & NomeTb & ") ;" & vbCrLf _
                    & Space(4) & "<INIT-REVKEY(i)>" & vbCrLf _
                    & "END ;"
  End If

'''' 'virgilio
  getRevKey rsSegmenti!IdSegmento, rtb, "PLI"

Exit Sub
ErrorHandler:
  m_fun.WriteLog "Errore in MadmdM_DB2.InserisciChiavePrimaria_PLI. Errore: " & ERR.Description
End Sub

Sub InserisciChiavePrimariaRedefines(rtb As RichTextBox, rsSegmenti As Recordset, PrefDb As String)
  
  'virgilio inserito il PrefDb sulle query per gestire anche ORACLE
  Dim rsOrigine As Recordset, rsTabelle As Recordset, rsColonne As Recordset
  Dim rsRedefine As Recordset
  Dim wNomeSeg As String
  Dim spazi As Integer
  Dim tableName As String
  Dim NomeTb As String
  Dim rsAlias As Recordset
  Dim nomeTabella As String
  Dim occursTable As Boolean
  
  wNomeSeg = rsSegmenti!Nome
  
  Set rsTabelle = m_fun.Open_Recordset("select idTable,Nome from " & PrefDb & "_Tabelle where IdOrigine = " & rsSegmenti!IdSegmento)
  nomeTabella = Replace(rsTabelle!Nome, "_", "-")
  If rsTabelle.RecordCount Then
    Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
    If rsAlias.EOF Then
      NomeTb = rsTabelle!Nome
    Else
      NomeTb = rsAlias!Nome
    End If
    rsAlias.Close
  
    tableName = Replace(NomeTb, "_", "-")
    changeTag rtb, "<WHEN-TABLE(i)>", "WHEN '" & rsSegmenti!Nome & "'" & vbCrLf & _
                                      "    <WHEN-PILOTA(i)>" & vbCrLf & _
                                      "    <SCRIVI-PILOTA>" & vbCrLf & _
                                      "    <INIT-REVKEY(i)>"   'virgilio
  End If
  
  ''''''''''''''''''''''''''''''''''''''
  ' Sostituzione "<AGGIORNA-CHIAVE>."

  rsTabelle.Close
  
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' ???
 
  ''''''''''''''''''''''''''''''''''''''
  ' Sostituzione "<WHEN-PILOTA>."
  ''''''''''''''''''''''''''''''''''''''
  Set rsRedefine = m_fun.Open_Recordset("select * from Mgdli_Segmenti where IdSegmentoOrigine = " & rsSegmenti!IdSegmento)
  While Not rsRedefine.EOF
    Set rsTabelle = m_fun.Open_Recordset("select idTable,Nome,Condizione from " & PrefDb & "_Tabelle where IdOrigine = " & rsRedefine!IdSegmento)
    ' Mauro 28/10/2009 : Se c'� pi� di un record, ci sono delle tabelle OCCURS.
    ' L'unica tabella senza "Dimensionamento" � il padre!
    occursTable = False
    If rsTabelle.RecordCount > 1 Then
      occursTable = True
      Do Until rsTabelle.EOF
        If IsNull(rsTabelle!Dimensionamento) Then
          Exit Do
        End If
        rsTabelle.MoveNext
      Loop
    End If
    If rsTabelle.RecordCount Then
      Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
      If rsAlias.EOF Then
        NomeTb = rsTabelle!Nome
      Else
        NomeTb = rsAlias!Nome
      End If
      rsAlias.Close

      changeTag rtb, "<WHEN-PILOTA(i)>", "IF <CONDIZIONE>" & vbCrLf & _
                                         "   <SCRIVI-REDEFINES>" & vbCrLf & _
                                         "END-IF"
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Pi� lungo/lento ma modulare:
      ' - gi� pronto per Template innestati...
      ' - A posto per indentazione
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'SILVIA 07-04-2008 : La condizione di Redefine � sul segmento!!
      changeTag rtb, "<CONDIZIONE>", getCondizione_Redefines(rtb, IIf(IsNull(rsRedefine!Condizione), "", rsRedefine!Condizione))
      ' Mauro 28/10/2009 :  Gestione tabelle Occurs in Redefines
      If occursTable Then
        changeTag rtb, "<SCRIVI-REDEFINES>", _
                       "MOVE AREA-" & rsSegmenti!Nome & " TO AREA-" & Replace(rsRedefine!Nome, "_", "-") & vbCrLf & _
                       "PERFORM " & Replace(NomeTb, "_", "-") & "-TO-RR " & vbCrLf & _
                       "<OCCURS-MOVE(i)>" & vbCrLf & _
                       "ADD 1 TO CTR-" & Replace(NomeTb, "_", "-") & vbCrLf & _
                       "WRITE REC-F" & Replace(NomeTb, "_", "-") & " FROM RR-" & Replace(rsTabelle!Nome, "_", "-") & vbCrLf & _
                       "MOVE '" & rsTabelle!Nome & "' TO WK-TABLE"
      Else
        changeTag rtb, "<SCRIVI-REDEFINES>", _
                       "MOVE AREA-" & rsSegmenti!Nome & " TO AREA-" & Replace(rsRedefine!Nome, "_", "-") & vbCrLf & _
                       "PERFORM " & Replace(NomeTb, "_", "-") & "-TO-RR " & vbCrLf & _
                       "ADD 1 TO CTR-" & Replace(NomeTb, "_", "-") & vbCrLf & _
                       "WRITE REC-F" & Replace(NomeTb, "_", "-") & " FROM RR-" & Replace(rsTabelle!Nome, "_", "-") & vbCrLf & _
                       "MOVE '" & rsTabelle!Nome & "' TO WK-TABLE"
      End If
    End If
    rsRedefine.MoveNext
  Wend
  rsRedefine.Close
  
  ''''''''''''''''''''''''''''''''''''''
  ' Sostituzione "<SCRIVI-PILOTA>."
  ''''''''''''''''''''''''''''''''''''''
''''  changeTag RTB, "<SCRIVI-PILOTA>", _
''''                 "MOVE AREA-" & rsSegmenti!Nome & " TO AREA-" & tableName & vbCrLf & _
''''                 "PERFORM " & rsSegmenti!Nome & "-TO-RR THRU " & rsSegmenti!Nome & "-TO-RR-EX" & vbCrLf & _
''''                 "ADD 1 TO CTR-" & rsSegmenti!Nome & vbCrLf & _
''''                 "WRITE REC-F" & rsSegmenti!Nome & " FROM RR-" & tableName
                 
  'virgilio section
 '''   SILVIA 19-03-2008 : il nome delle section -TO-RR � sempre formato dal nome tabella non dal nome segmento....

'''  changeTag rtb, "<SCRIVI-PILOTA>", _
'''                 "PERFORM " & rsSegmenti!Nome & "-TO-RR " & vbCrLf & _
'''                 "ADD 1 TO CTR-" & Replace(tableName, "-", "") & vbCrLf & _
'''                 "WRITE REC-F" & Replace(tableName, "-", "") & " FROM RR-" & nomeTabella
                 

  changeTag rtb, "<SCRIVI-PILOTA>", _
                 "PERFORM " & Replace(tableName, "-", "") & "-TO-RR " & vbCrLf & _
                 "ADD 1 TO CTR-" & Replace(tableName, "-", "") & vbCrLf & _
                 "WRITE REC-F" & Replace(tableName, "-", "") & " FROM RR-" & nomeTabella
  
''''
  getRevKey rsSegmenti!IdSegmento, rtb
''''  changeTag rtb, "<INIT-REVKEY(i)>", _
''''                 "________ inserire template _________"
                 
'''''  DelParola rtb, "<INIT-REVKEY(i)>"
  DelParola rtb, "<WHEN-PILOTA(i)>"
End Sub

Private Sub getRevKey(ids As Long, rtbI As RichTextBox, Optional tipoconv As String)
  Dim rsSeg As Recordset, rsSegParent As Recordset, rsTabelle As Recordset, rsColonne As Recordset
  Dim strSegParent As String, strTabelle As String, strParent As String, dbType As String
  Dim lIdOggetto As Long, lIdSegmento As Long, lIdTabella As Long
  
  On Error GoTo ErrH

  If TipoDB = "ORACLE" Then
    dbType = "DMORC"
  Else
    dbType = "DMDB2"
  End If

  'prende il nome e l'id oggetto
  Set rsSeg = m_fun.Open_Recordset(" select * from PSDLI_Segmenti where IdSegmento=" & ids)
  While Not rsSeg.EOF
      strParent = rsSeg!Nome
      lIdOggetto = rsSeg!IdOggetto
      strSegParent = " select * from PSDLI_Segmenti " & _
                     " where Parent='" & strParent & "' and " & _
                     " IdOggetto=" & lIdOggetto
      Set rsSegParent = m_fun.Open_Recordset(strSegParent)
      If Not rsSegParent.EOF Then
        While Not rsSegParent.EOF
          lIdSegmento = rsSegParent!IdSegmento
          strTabelle = "select * from " & dbType & "_Tabelle where IdOrigine=" & lIdSegmento
          Set rsTabelle = m_fun.Open_Recordset(strTabelle)
          If Not rsTabelle.EOF Then
            While Not rsTabelle.EOF
              lIdTabella = rsTabelle!IdTable
              'SQ 21-11-07 NO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              'appena modificato il tipo non ha pi� funzionato (c'era REVKEY, SMALLINT e 4, nella query)
              Set rsColonne = m_fun.Open_Recordset( _
                "SELECT * FROM " & dbType & "_colonne WHERE " & _
                " IdTable=" & lIdTabella & " and Nome='" & PkNameParam & "' and idCmp=-1")
              If Not rsColonne.EOF Then
                'TILVIA
                changeTag rtbI, "<INIT-REVKEY(i)>", _
                IIf(tipoconv = "PLI", "RR_" & rsTabelle!Nome & "." & PkNameParam & " = 0;", "MOVE ZERO TO " & PkNameParam & " OF RR-" & Replace(rsTabelle!Nome, "_", "-")) & _
                 "<INIT-REVKEY(i)>"
                 DelParola rtbI, "<INIT-REVKEY(i)>"
              End If
              rsColonne.Close
              rsTabelle.MoveNext
            Wend
            rsTabelle.Close
          End If
          rsSegParent.MoveNext
        Wend
        rsSegParent.Close
      End If
      rsSeg.MoveNext
  Wend
  rsSeg.Close
  
  DelParola rtbI, "<INIT-REVKEY(i)>"
  Exit Sub
ErrH:
  Resume Next
End Sub

Sub InserisciChiavePrimariaRedefines_PLI(rtb As RichTextBox, rsSegmenti As Recordset)
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  '''' MG 130207 INSERITO NUOVO METODO MA NON VERIFICATO'''
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim rsOrigine As Recordset, rsTabelle As Recordset, rsColonne As Recordset
  Dim rsRedefine As Recordset
  Dim wNomeSeg As String
  Dim spazi As Integer
  Dim tableName As String
  Dim occursTable As Boolean
  
  wNomeSeg = rsSegmenti!Nome
  
  Set rsTabelle = m_fun.Open_Recordset("select idTable,Nome from DMDB2_Tabelle where IdOrigine = " & rsSegmenti!IdSegmento)
  If rsTabelle.RecordCount Then
    tableName = Replace(rsTabelle!Nome, "_", "-")
    
     changeTag rtb, "<WHEN-TABLE(i)>", "WHEN ('" & rsSegmenti!Nome & "') DO ;" & vbCrLf & _
                                      "    <WHEN-PILOTA(i)>" & vbCrLf & _
                                      "    <SCRIVI-PILOTA>" & vbCrLf & _
                                      "    <INIT-REVKEY(i)>" & vbCrLf & _
                                      "END; " & vbCrLf
  End If

  rsTabelle.Close
  
  ''''''''''''''''''''''''''''''''''''''
  ' Sostituzione "<WHEN-PILOTA>."
  ''''''''''''''''''''''''''''''''''''''
  Set rsRedefine = m_fun.Open_Recordset("select * from Mgdli_Segmenti where IdSegmentoOrigine = " & rsSegmenti!IdSegmento)
  While Not rsRedefine.EOF
    Set rsTabelle = m_fun.Open_Recordset("select idTable,Nome,dimensionamento from DMDB2_Tabelle where IdOrigine = " & rsRedefine!IdSegmento)
    ' Mauro 28/10/2009 : Se c'� pi� di un record, ci sono delle tabelle OCCURS.
    ' L'unica tabella senza "Dimensionamento" � il padre!
    occursTable = False
    If rsTabelle.RecordCount > 1 Then
      occursTable = True
      Do Until rsTabelle.EOF
        If IsNull(rsTabelle!Dimensionamento) Then
          Exit Do
        End If
        rsTabelle.MoveNext
      Loop
    End If
    If rsTabelle.RecordCount Then
      'SILVIA 1-4-2009
      changeTag rtb, "<WHEN-PILOTA(i)>", "IF <CONDIZIONE> THEN DO; " & vbCrLf & _
                                         " <SCRIVI-REDEFINES>" & vbCrLf & _
                                         "END;"
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      ' Pi� lungo/lento ma modulare:
      ' - gi� pronto per Template innestati...
      ' - A posto per indentazione
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'SILVIA 07-04-2008 : La condizione di Redefine � sul segmento!!
      changeTag rtb, "<CONDIZIONE>", getCondizione_Redefines(rtb, IIf(IsNull(rsRedefine!Condizione), "", rsRedefine!Condizione))
      ' Mauro 23-04-2007 : Eliminato per ripristinare la chiamata a SECTION
'''      changeTag rtb, "<SCRIVI-REDEFINES>", _
'''                     "MOVE AREA-" & rsSegmenti!Nome & " TO AREA-" & rsRedefine!Nome & vbCrLf & _
'''                     "PERFORM " & rsRedefine!Nome & "-TO-RR THRU " & rsRedefine!Nome & "-TO-RR-EX" & vbCrLf & _
'''                     "ADD 1 TO CTR-" & rsRedefine!Nome & vbCrLf & _
'''                     "WRITE REC-F" & rsRedefine!Nome & " FROM RR-" & Replace(rsTabelle!Nome, "_", "-") & vbCrLf & _
'''                     "MOVE '" & rsTabelle!Nome & "' TO WK-TABLE"
      'SILVIA 1-4-2009
'''      changeTag rtb, "<SCRIVI-REDEFINES>", _
'''                     "AREA_" & rsRedefine!Nome & " = AREA_" & rsSegmenti!Nome & " ;" & vbCrLf & _
'''                     "CALL " & rsRedefine!Nome & "_TO_RR;" & vbCrLf & _
'''                     "CTR_" & rsRedefine!Nome & "= CTR_" & rsRedefine!Nome & "+ 1;" & vbCrLf & _
'''                     "WRITE FILE (" & rsRedefine!Nome & ") FROM (RR_" & rsTabelle!Nome & ") ;" & vbCrLf & _
'''                     "WK_TABLE =" & rsTabelle!Nome & ";"
      ' Mauro 28/10/2009 : Gestione Tabelle Occurs
      If occursTable Then
        changeTag rtb, "<SCRIVI-REDEFINES>", _
                       "CALL " & rsTabelle!Nome & "_TO_RR;" & vbCrLf & _
                       "<OCCURS-MOVE(i)>" & vbCrLf & _
                       "CTR_" & rsTabelle!Nome & "= CTR_" & rsTabelle!Nome & "+ 1;" & vbCrLf & _
                       "WRITE FILE (" & rsTabelle!Nome & ") FROM (RR_" & rsTabelle!Nome & ") ;" & vbCrLf & _
                       "WK_TABLE = '" & rsTabelle!Nome & "' ;"
      Else
        changeTag rtb, "<SCRIVI-REDEFINES>", _
                       "CALL " & rsTabelle!Nome & "_TO_RR;" & vbCrLf & _
                       "CTR_" & rsTabelle!Nome & "= CTR_" & rsTabelle!Nome & "+ 1;" & vbCrLf & _
                       "WRITE FILE (" & rsTabelle!Nome & ") FROM (RR_" & rsTabelle!Nome & ") ;" & vbCrLf & _
                       "WK_TABLE = '" & rsTabelle!Nome & "' ;"
      End If
    
    End If
    rsRedefine.MoveNext
  Wend
  rsRedefine.Close
  
  ''''''''''''''''''''''''''''''''''''''
  ' Sostituzione "<SCRIVI-PILOTA>."
  ''''''''''''''''''''''''''''''''''''''
  ' Mauro 23-04-2007 : Eliminato per ripristinare la chiamata a SECTION
''  changeTag rtb, "<SCRIVI-PILOTA>", _
''                 "MOVE AREA-" & rsSegmenti!Nome & " TO AREA-" & tableName & vbCrLf & _
''                 "PERFORM " & rsSegmenti!Nome & "-TO-RR THRU " & rsSegmenti!Nome & "-TO-RR-EX" & vbCrLf & _
''                 "ADD 1 TO CTR-" & rsSegmenti!Nome & vbCrLf & _
''                 "WRITE REC-F" & rsSegmenti!Nome & " FROM RR-" & tableName
  'SILVIA 1-4-2009
''  changeTag rtb, "<SCRIVI-PILOTA>", _
''                 "AREA_" & tableName & " = AREA_" & rsSegmenti!Nome & " ;" & vbCrLf & _
''                 "CALL " & rsSegmenti!Nome & "_TO_RR;" & vbCrLf & _
''                 "CTR_" & rsSegmenti!Nome & "= CTR_" & rsSegmenti!Nome & "+ 1;" & vbCrLf & _
''                 "WRITE FILE (" & rsSegmenti!Nome & ") FROM (RR_" & tableName & ") ;"
  
  changeTag rtb, "<SCRIVI-PILOTA>", _
                 "CALL " & tableName & "_TO_RR;" & vbCrLf & _
                 "CTR_" & tableName & "= CTR_" & tableName & "+ 1;" & vbCrLf & _
                 "WRITE FILE (" & tableName & ") FROM (RR_" & tableName & ") ;"
  
  DelParola rtb, "<WHEN-PILOTA(i)>"

''''
  getRevKey rsSegmenti!IdSegmento, rtb, "PLI"
''''  changeTag rtb, "<INIT-REVKEY(i)>", _
''''                 "________ inserire template _________"
                 
'''''  DelParola rtb, "<INIT-REVKEY(i)>"
  DelParola rtb, "<WHEN-PILOTA(i)>"

End Sub

Public Function getDclgenLen_BPER(IdTable As Long) As Integer
  Dim TbColonne As Recordset
   
  On Error GoTo errdb
  getDclgenLen_BPER = 0
  Set TbColonne = m_fun.Open_Recordset("select nome, Tipo, Lunghezza, Decimali from DMDB2_Colonne where idTable = " & IdTable & " order by ordinale") 'nome + order by solo per debug...
  Do Until TbColonne.EOF
      m_fun.WriteLog "Debug: colonna=" & TbColonne!Nome & " - totLen=" & getDclgenLen_BPER
     Select Case TbColonne!Tipo
        Case "CHAR"
           getDclgenLen_BPER = getDclgenLen_BPER + TbColonne!lunghezza
        Case "TIME"
           getDclgenLen_BPER = getDclgenLen_BPER + 8
        Case "DATE"
           getDclgenLen_BPER = getDclgenLen_BPER + 10
        Case "TIMESTAMP"
           getDclgenLen_BPER = getDclgenLen_BPER + 26
        Case "INTEGER"
           getDclgenLen_BPER = getDclgenLen_BPER + 4
        Case "LONG"
           getDclgenLen_BPER = getDclgenLen_BPER + 8
        Case "DECIMAL"
           'PIPPO:
           'getDclgenLen_BPER = getDclgenLen_BPER + CInt((TbColonne!Lunghezza + 1) / 2)
           getDclgenLen_BPER = getDclgenLen_BPER + CInt((TbColonne!lunghezza + 1.1) / 2)
        Case "SMALLINT"
           getDclgenLen_BPER = getDclgenLen_BPER + 2
        Case "VARCHAR"
           'getDclgenLen_BPER = getDclgenLen_BPER + 2 + tbColonne!Lunghezza
           getDclgenLen_BPER = getDclgenLen_BPER + TbColonne!lunghezza
     End Select
     TbColonne.MoveNext
  Loop
  TbColonne.Close
  Exit Function
errdb:
  MsgBox ERR.Description
  getDclgenLen_BPER = -1
End Function
''''''''''''''''''''''''''''''''''''''''''''''
' SQ - 17-06-05
' Era nella "Create" del DM gestione "DLI"...
''''''''''''''''''''''''''''''''''''''''''''''
Sub GeneraCpyRD(RtbFile As RichTextBox)
  Dim rsSegmenti As Recordset, rsTabelle As Recordset
  Dim fileName As String
  Dim wNomeSeg As String
  Dim env As Collection
      
  On Error GoTo errdb
  
  If dbType = "VSAM" Then
    Stop
  End If
  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Cpy\"
  
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "idDB = " & pIndex_DBCorrente & " and TAG not like '%OCCURS' order by idTable")
  Virtuale = False  'non utilizzato!?
  While Not rsTabelle.EOF
    '''''''''''''''''''''''''''''''''''''
    ' Per ogni TABELLA associata al DBD:
    '''''''''''''''''''''''''''''''''''''
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine)
    End If
    
    If rsSegmenti.RecordCount > 0 Then
      
      'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
      '****************
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RD") <> "" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RD"
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RD"" not found."
        Exit Sub
      End If
     
'      RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RD"
      '*****************
           
      changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
      changeTag RtbFile, "<TABLE-NAME>", Replace(rsTabelle!Nome, "_", "-")
      changeTag RtbFile, "<SEGM-NAME>", Replace(rsSegmenti!Nome, "_", "-")
      
      '<STRUTTURA-ASSOCIATA>
      InserisciArea rsSegmenti, RtbFile
      
      getChiaviDli RtbFile, rsSegmenti, rsTabelle!IdTable
      getChiaviDli_KRDP RtbFile, rsSegmenti, rsTabelle!IdTable
    
      fileName = LeggiParam("T-COPY")
      If Len(fileName) Then
        Set env = New Collection  'resetto;
        Dim rsAlias As Recordset
        Set rsAlias = m_fun.Open_Recordset("select * from " & PrefDb & "_alias where idtable = " & rsTabelle!IdTable)
        If rsAlias.RecordCount Then
          env.Add rsAlias
        Else
          env.Add rsTabelle
        End If
        env.Add rsSegmenti
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(fileName, "tabelle", env), 1
        rsAlias.Close
      Else
        MsgBox "No value found for 'T-COPY' environment parameter.", vbExclamation
        Exit Sub
      End If
    End If
    rsTabelle.MoveNext
  Wend
  
  Exit Sub
errdb:
  Select Case ERR.Number
    Case 75
'      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CPY_BASE_RD"" not found."
    Case 7676 '
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Sub

Sub GeneraCpyRD_PLI(RtbFile As RichTextBox)
  Dim rsSegmenti As Recordset, rsTabelle As Recordset
  Dim fileName As String
  Dim wNomeSeg As String
  Dim env As Collection
  Dim dbString As String
  Dim rs As Recordset
  
  On Error GoTo errdb
  
  If dbType = "VSAM" Then
    Stop
  End If
  
  'tilvia 04-05-2009
  Set rs = m_fun.Open_Recordset("Select * From " & PrefDb & "_DB Where IdDB = " & pIndex_DBCorrente)
  pIndex_DBDCorrente = rs!iddbor
  rs.Close
  
  DataMan.DmParam_OutField_Db = DataMan.DmPathDb & "\OutPut-Prj\" & dbType & "\Include\"
  
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB= " & pIndex_DBCorrente & " and TAG not like '%OCCURS' order by idTable")
  Virtuale = False  'non utilizzato!?
  While Not rsTabelle.EOF
    '''''''''''''''''''''''''''''''''''''
    ' Per ogni TABELLA associata al DBD:
    '''''''''''''''''''''''''''''''''''''
    Set rsSegmenti = m_fun.Open_Recordset("select * from PSDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from MGDLI_Segmenti where IdSegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    End If
    If rsSegmenti.RecordCount Then
      'gloria 30/04/2009: controllo l'esistenza del file prima di fare la load co�' non va in eoe!
      '****************
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RD") <> "" Then
        RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RD"
      Else
        DataMan.DmFinestra.ListItems.Add , , Now & " Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RD"" not found."
        Exit Sub
      End If
      
      'RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RD"
      '*****************
            
      ''dbString = "/*     TYPE: IMS/DB       Nome DBD = " & pNome_DBCorrente
      changeTag RtbFile, "<DBD-NAME>", pNome_DBCorrente
      changeTag RtbFile, "<SEGM-NAME>", rsSegmenti!Nome
      changeTag RtbFile, "<TABLE-NAME>", rsTabelle!Nome
      
      '<STRUTTURA-ASSOCIATA>
      InserisciArea_PLI rsSegmenti, RtbFile
      
      'chiavi
      getChiaviDli_PLI RtbFile, rsSegmenti, rsTabelle!IdTable
      getChiaviDli_KRDP_PLI RtbFile, rsSegmenti, rsTabelle!IdTable
    
      fileName = LeggiParam("T-COPY")
      If Len(fileName) Then
        Set env = New Collection  'resetto;
        env.Add rsTabelle
        env.Add rsSegmenti
        RtbFile.SaveFile DataMan.DmParam_OutField_Db & getEnvironmentParam(fileName, "tabelle", env), 1
      Else
        MsgBox "No value found for 'T-COPY' environment parameter.", vbExclamation
        Exit Sub
      End If
    End If
    rsSegmenti.Close
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  Exit Sub
errdb:
  Select Case ERR.Number
    Case 75
'      ERR.Raise 75, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\PLI\" & dbType & "\CPY_BASE_RD"" not found."
    Case 7676 '
      ERR.Raise ERR
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Sub

Function getLoadJclDD(RtbFile As RichTextBox, rsTabelle As Recordset, rsSegmenti As Recordset) As String
  Dim rtbString As String
  Dim lRecl As Integer
  Dim rsAlias As Recordset, rsTbs As Recordset
  Dim NomeTb As String
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
  
  On Error GoTo loadErr
  
  Set rsAlias = m_fun.Open_Recordset("select nome from " & PrefDb & "_Alias where idtable = " & rsTabelle!IdTable)
  If rsAlias.EOF Then
    NomeTb = rsTabelle!Nome
  Else
    NomeTb = rsAlias!Nome
  End If
  'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\LOAD_JCL_DD"
  RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\LOAD_JCL_DD"
  
  'changeTag RtbFile, "<TABLE-NAME>", Replace(rsTabelle!Nome, "_", "")
  changeTag RtbFile, "<TABLE-NAME>", Replace(NomeTb, "_", "")
  'virgilio per pippo
  'changeTag RtbFile, "<DD-NAME>", Replace(rsTabelle!Nome, "_", "")
  changeTag RtbFile, "<DD-NAME>", Replace(NomeTb, "_", "")
  
  'SQ RAGAZZI, BASTA DROGARSI! IL TAG SI CHIAMA <SEGM-NAME>!!!!!!!!!!!!!!!!!!
  'VIRGILIO 1/3/2007
  'changeTag rtbFile, "<SEGM-NAME>", Replace(rsTabelle!Nome, "_", "")
  'changeTag RtbFile, "<SEGM-NAME>", rsTabelle!Nome
  changeTag RtbFile, "<SEGM-NAME>", rsSegmenti!Nome
  
  lRecl = getDclgenLen(rsTabelle!IdTable)
        
  changeTag RtbFile, "<LRECL>", lRecl & ""
      
  changeTag RtbFile, "<BLKSIZE>", lRecl & ""
  
  '<TBS-NAME>
  If rsTabelle!IdTableSpc Then
    'TBS associato; cerco il nome
    Set rsTbs = m_fun.Open_Recordset("select nome from " & PrefDb & "_TableSpaces where idTableSpc = " & rsTabelle!IdTableSpc)
    If rsTbs.RecordCount Then
      changeTag RtbFile, "<TBS-NAME>", rsTbs!Nome
    'Else
    '  MsgBox "TMP; idTableSpace not found: " & rsTabelle!IdTableSpc
    End If
    rsTbs.Close
  Else
    'no TBS associati
  End If
        
  getLoadJclDD = RtbFile.text
  
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\LOAD_JCL_DD not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\LOAD_JCL_DD not found."
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Function

Function getCtlCopyColumn(RtbFile As RichTextBox, rsTabelle As Recordset) As String
  Dim rtbString As String, rsColonne As Recordset
  Dim base As Integer, offset As Integer
  Dim nullIf As String, columnType As String
  Dim swVirgola As Boolean
  Dim ColFill As Integer
  
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
  
  On Error GoTo loadErr
  
  '''''''''''''''''''''''''
  ' CICLO COLONNE:
  '''''''''''''''''''''''''
  base = 1
  'If primi 2 byte: X'0019'
  'base = 3
  swVirgola = True
  Set rsColonne = m_fun.Open_Recordset("select nome,tipo,lunghezza,decimali,[null] from " & PrefDb & "_Colonne where IdTable = " & rsTabelle!IdTable & " order by ordinale")
  While Not rsColonne.EOF
    'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\CTL_COPY_COLUMN"
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CTL_COPY_COLUMN"
    'usare il format(...)
    ColFill = 18 - Len(rsColonne!Nome)
    changeTag RtbFile, "<COLUMN-NAME>", rsColonne!Nome & Space(ColFill)
    
    If swVirgola Then
       changeTag RtbFile, "<VIRGOLA>", " "
       swVirgola = False
    Else
       changeTag RtbFile, "<VIRGOLA>", ","
    End If
    ''''''''''
    '<OFFSET>
    ''''''''''
    If TipoDB = "DB2" Then
      Select Case rsColonne!Tipo
        Case "CHAR"
           offset = rsColonne!lunghezza
           columnType = rsColonne!Tipo & "(" & Format(rsColonne!lunghezza, "00000") & ")"
        Case "TIME"
           offset = 8
           columnType = rsColonne!Tipo & " EXTERNAL"
        Case "DATE"
           offset = 10
           columnType = rsColonne!Tipo & " EXTERNAL"
        Case "TIMESTAMP"
           offset = 26
           columnType = rsColonne!Tipo & " EXTERNAL"
        Case "INTEGER"
           offset = 4
           columnType = rsColonne!Tipo
        Case "LONG"
           offset = 8
        Case "DECIMAL"
           offset = CInt((rsColonne!lunghezza + 1.1) / 2)
           columnType = rsColonne!Tipo
        Case "SMALLINT"
           offset = 2
           columnType = rsColonne!Tipo
        Case "VARCHAR", "VARCHAR2"
           offset = 2 + rsColonne!lunghezza
           columnType = rsColonne!Tipo
      End Select
    ElseIf TipoDB = "ORACLE" Then
      Select Case rsColonne!Tipo
        Case "CHAR"
           offset = rsColonne!lunghezza
           columnType = rsColonne!Tipo & "(" & Format(rsColonne!lunghezza, "00000") & ")"
        Case "DATE"
           offset = 10
           columnType = rsColonne!Tipo & " EXTERNAL"
        Case "NUMBER"
           offset = rsColonne!lunghezza + rsColonne!Decimali
           columnType = rsColonne!Tipo
        Case "VARCHAR2"
           offset = 2 + rsColonne!lunghezza
           columnType = rsColonne!Tipo
      End Select
    End If
    'X(1) in piu' per "indicatore di null"
    If rsColonne!Null Then
      base = base + 1
      nullIf = "NULLIF(" & Format(base - 1, "00000") & ")=X''FF''"
    Else
      nullIf = ""
    End If
    
    '''''''''
    '<BASE>
    '''''''''
    changeTag RtbFile, "<BASE>", Format(base, "00000")
    
    base = base + offset
    
    changeTag RtbFile, "<OFFSET>", Format(base - 1, "00000")
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''
    '<COLUMN-TYPE>
    ' Aggiunge EXTERNAL per i tipi di dati "temporali"
    ''''''''''''''''''''''''''''''''''''''''''''''''''
    changeTag RtbFile, "<COLUMN-TYPE>", columnType
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''
    '<NULLIF>
    ''''''''''''''''''''''''''''''''''''''''''''''''''
    changeTag RtbFile, "<NULLIF>", nullIf
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''
    '<COLUMN-TYPE>
    ' Aggiunge EXTERNAL per i tipi di dati "temporali"
    ''''''''''''''''''''''''''''''''''''''''''''''''''
    getCtlCopyColumn = getCtlCopyColumn & vbCrLf & vbCrLf & RtbFile.text
    
    rsColonne.MoveNext
  Wend
  rsColonne.Close
  
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\CTL_COPY_COLUMN not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\fields\" & dbType & "\CTL_COPY_COLUMN not found."
    Case Else
      MsgBox ERR.Description
      'Resume
  End Select
End Function

Function getLoadJclCTL(RtbFile As RichTextBox, rsTabelle As Recordset, rsSegmenti As Recordset) As String
  Dim rtbString As String
  Dim lRecl As Integer
  Dim rsAlias As Recordset
  Dim NomeTb As String
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
  
  On Error GoTo loadErr
  
  Set rsAlias = m_fun.Open_Recordset("select nome from " & PrefDb & "_Alias where idtable = " & rsTabelle!IdTable)
  If rsAlias.EOF Then
    NomeTb = rsTabelle!Nome
  Else
    NomeTb = rsAlias!Nome
  End If
  
  'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\LOAD_JCL_CTL"
  RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\LOAD_JCL_CTL"
  
  changeTag RtbFile, "<SEGM-NAME>", rsSegmenti!Nome
  'changeTag RtbFile, "<TABLE-NAME>", Replace(rsTabelle!Nome, "_", "")
  changeTag RtbFile, "<TABLE-NAME>", Replace(NomeTb, "_", "")
  
  lRecl = getDclgenLen(rsTabelle!IdTable)
        
  changeTag RtbFile, "<LRECL>", lRecl & ""
      
  changeTag RtbFile, "<BLKSIZE>", lRecl & ""
  
  '<TBS-NAME>
  If rsTabelle!IdTableSpc > 0 Then
    'TBS associato; cerco il nome
    Dim rsTbs As Recordset
    Set rsTbs = m_fun.Open_Recordset("select nome from " & PrefDb & "_TableSpaces where idTableSpc = " & rsTabelle!IdTableSpc)
    If rsTbs.RecordCount Then
      changeTag RtbFile, "<TBS-NAME>", rsTbs!Nome
    Else
      ' TMP!!!
      MsgBox "Tabella: " & NomeTb & " - idTableSpace not found: " & rsTabelle!IdTableSpc, vbExclamation + vbOKOnly, DataMan.DmNomeProdotto
    End If
    rsTbs.Close
  Else
    'no TBS associati
  End If
        
  getLoadJclCTL = RtbFile.text
  
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\LOAD_JCL_CTL not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\LOAD_JCL_CTL not found."
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Function

Function getLoadJclDEL(RtbFile As RichTextBox, rsTabelle As Recordset, rsSegmenti As Recordset) As String
  Dim rtbString As String
  Dim lRecl As Integer
  Dim rsAlias As Recordset
  Dim NomeTb As String
  'usarne un'altra!
  'salvo:
  rtbString = RtbFile.text
  
  On Error GoTo loadErr
  
  Set rsAlias = m_fun.Open_Recordset("select nome from " & PrefDb & "_Alias where idtable = " & rsTabelle!IdTable)
  If rsAlias.EOF Then
    NomeTb = rsTabelle!Nome
  Else
    NomeTb = rsAlias!Nome
  End If

  'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\LOAD_JCL_DEL"
  RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\LOAD_JCL_DEL"
  
  'changeTag RtbFile, "<NOME-TABELLA>", Replace(rsTabelle!Nome, "_", "-")
  changeTag RtbFile, "<NOME-TABELLA>", Replace(NomeTb, "_", "-")
  
  changeTag RtbFile, "<SEGM-NAME>", rsSegmenti!Nome
  
  lRecl = getDclgenLen(rsTabelle!IdTable)
        
  changeTag RtbFile, "<LRECL>", lRecl & ""
      
  changeTag RtbFile, "<BLKSIZE>", lRecl & ""
  
  '<TBS-NAME>
  If rsTabelle!IdTableSpc > 0 Then
  'TBS associato; cerco il nome
    Dim rsTbs As Recordset
      Set rsTbs = m_fun.Open_Recordset("select nome from " & PrefDb & "_TableSpaces where idTableSpc = " & rsTabelle!IdTableSpc)
      If rsTbs.RecordCount Then
        changeTag RtbFile, "<TBS-NAME>", rsTbs!Nome
      'Else
      '  MsgBox "TMP; idTableSpace not found: " & rsTabelle!IdTableSpc
      End If
    Else
      'no TBS associati
    End If
        
  getLoadJclDEL = RtbFile.text
  
  'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\CPY_BASE\" & DbType & "\LOAD_JCL_DEL not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\jcl\" & dbType & "\LOAD_JCL_DEL not found."
    Case Else
      MsgBox ERR.Description
    'resume
  End Select
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' MOVE KRR PADRE -> KRR FIGLIO (COLONNE EREDITATE)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getSetcWkeywkey(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, rs As Recordset, rsIndexParent As Recordset, rsColonne As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  Dim statement As String, rrName As String, rrNameParent, cCopyArea As String
  Dim idParent As Long
  Dim env As Collection
  
  statement = "EVALUATE WK-NAMETABLE " & vbCrLf
  
  'stefano: a me piacciono anche le OCCURS...
  'Set rsTabelle = m_fun.Open_Recordset("select * from dmdb2_tabelle where idOrigine > 0 and TAG not like '%OCCURS' and iddb=" & pIndex_DBCorrente & " order by idTable")
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idOrigine > 0 and iddb=" & pIndex_DBCorrente & " order by idTable")
  While Not rsTabelle.EOF
    If rsTabelle!IdOrigine > 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
      If rsSegmenti.RecordCount = 0 Then
        Set rsSegmenti = m_fun.Open_Recordset("select Correzione from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
        If rsSegmenti!Correzione = "I" Then
          'Segmento VIRTUALE: nop
        Else
          m_fun.WriteLog "getSetcWkewkey: no origin segment for table: " & rsTabelle!Nome
          MsgBox "getSetcWkewkey: no origin segment for table: " & rsTabelle!Nome, vbExclamation, DataMan.DmNomeProdotto
        End If
      Else
        statement = statement & Space(3) & "WHEN '" & Replace(rsTabelle!Nome, "-", "_") & "' " & vbCrLf
        
        'stefano: cerco di ripristinare la gestione delle redefines
        If (rsSegmenti!Parent = "0" Or rsSegmenti!IdSegmentoOrigine <> 0) And IsNull(rsTabelle!idTableJoin) Then
          statement = statement & Space(6) & "CONTINUE" & vbCrLf
        Else
          'stefano:
          'idParent = getParentTable(rsTabelle!IdTable)
          idParent = getParentTable(rsTabelle!IdTable, IIf(IsNull(rsTabelle!idTableJoin), 0, rsTabelle!idTableJoin))
          '''''''''''''''''''
          'CHIAVI PRIMARIE:
          '''''''''''''''''''
          Set env = New Collection  'resetto;
          env.Add rsColonne
          env.Add rsTabelle
          If Len(cCopyArea) Then
            cCopyArea = Replace(getEnvironmentParam(cCopyAreaParam, "colonne", env), "_", "-")
          Else
            'segnalare errore
            cCopyArea = "RR-" & Replace(rsTabelle!Nome, "_", "-")
          End If
          
          Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & rsTabelle!IdTable)
          If rsIndex.RecordCount > 0 Then
            Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
            Do While Not rsIdxCol.EOF
              Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
              
              Set env = New Collection  'resetto;
              env.Add rsColonne
              env.Add rsTabelle
              If Len(cCopyColumnParam) Then
                rrName = getEnvironmentParam(cCopyColumnParam, "colonne", env)
              Else
                'segnalare errore
                rrName = rsColonne!Nome
              End If
              rrName = Replace(rrName, "_", "-")
              
              'SOLO COLONNE EREDITATE
              If rsColonne!IdTable <> rsColonne!idTableJoin Then
                Set rsIndexParent = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & idParent)
              'If rsColonne!IdTableJoin = idParent Then
                ' Mauro 16/01/2008: Se l'idcmp arrivava dalla tabella "nonno" il giro non funzionava!
'                If rsColonne!IdCmp = -1 Then
'                  'REV-FIELD: CHIAVE AGGIUNTA PER UNIVOCITA'  (verificare: idtable = idtablejoin)
'                  Set rs = m_fun.Open_Recordset( _
'                    "select nome from " & PrefDb & "_colonne where idtable = " & idParent & _
'                    " and idtable = idtablejoin and idcmp = " & rsColonne!IdCmp)
'                  If rs.RecordCount Then
'                    rrNameParent = rs!Nome
'                  Else
'                    MsgBox "tmp: rrNameParent not found for column " & rsColonne!Nome & " - Table: " & rsTabelle!Nome
'                  End If
'                  rs.Close
'                'Else
'                '  rrNameParent = Replace(rsColonne!NomeCmp, "_", "-")
'                'End If
'                Else
                'stefano: che bel CASINO!!! idcmp non � pi� univoco, per cui una tabella al terzo
                ' livello ha come idcmp ripetuto quelli ereditati da padre e nonno e fa un casino
                ' a questo punto devo tirare dentro altre condizioni, spero che lo si rimetta a posto
                ' prossimamente, cos� � un casino
                  Set rs = m_fun.Open_Recordset( _
                    "select nome from " & PrefDb & "_colonne where " & _
                    "idtable = " & idParent & " and idtablejoin = " & rsColonne!idTableJoin & " and idcmp = " & rsColonne!IdCmp)
                  If rs.RecordCount Then
                    rrNameParent = Replace(rs!Nome, "_", "-")
                  Else
                    MsgBox "tmp: rrNameParent not found for column " & rsColonne!Nome & " - Table: " & rsTabelle!Nome
                  End If
                  rs.Close
'                End If
                'MOVE
                statement = statement & Space(6) & _
                "MOVE " & rrNameParent & " OF " & "KRR-" & Replace(rsIndexParent!Nome, "_", "-") & vbCrLf & _
                Space(6) & "  TO " & rrName & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                rsIndexParent.Close
              End If  'end rsColonne!IdTable <> rsColonne!IdTableJoin
              rsIdxCol.MoveNext
            Loop
          End If 'end "CHIAVI PRIMARIE"
        End If 'end "CONTINUE"
      End If 'end "ok segmento"
    Else
      'no idOrigine
      m_fun.WriteLog "getSetcWkewkey: no origin segment for table: " & rsTabelle!Nome
      MsgBox "getSetcWkewkey: no origin segment for table: " & rsTabelle!Nome, vbExclamation, DataMan.DmNomeProdotto
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
    
  getSetcWkeywkey = statement & "END-EVALUATE. "
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' MOVE KRR PADRE -> KRR FIGLIO (COLONNE EREDITATE)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getSetcWkeywkey_PLI(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, rs As Recordset, rsIndexParent As Recordset, rsColonne As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  Dim statement As String, rrName As String, rrNameParent, cCopyArea As String
  Dim idParent As Long
  Dim env As Collection
  
  statement = "SELECT (WK_NAMETABLE);" & vbCrLf
  
  'stefano: a me piacciono anche le OCCURS...
  'Set rsTabelle = m_fun.Open_Recordset("select * from dmdb2_tabelle where idOrigine > 0 and TAG not like '%OCCURS' and iddb=" & pIndex_DBCorrente & " order by idTable")
  
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idOrigine > 0 and iddb=" & pIndex_DBCorrente & " order by idTable")
  
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB IN  (select IdDB from " & PrefDb & "_DB where idDBOr = " & pIndex_DBDCorrente & ")  order by idTable")
 
  While Not rsTabelle.EOF
    If rsTabelle!IdOrigine > 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
      If rsSegmenti.RecordCount = 0 Then
        Set rsSegmenti = m_fun.Open_Recordset("select Correzione from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
        If rsSegmenti.RecordCount Then
          If rsSegmenti!Correzione = "I" Then
            'Segmento VIRTUALE: nop
          Else
            m_fun.WriteLog "getSetcWkewkey: no origin segment for table: " & rsTabelle!Nome
            MsgBox "getSetcWkewkey: no origin segment for table: " & rsTabelle!Nome, vbExclamation, DataMan.DmNomeProdotto
          End If
        End If
      Else
        statement = statement & Space(3) & "WHEN ('" & rsTabelle!Nome & "') DO;" & vbCrLf
        
        'stefano: cerco di ripristinare la gestione delle redefines
        If (rsSegmenti!Parent = "0" Or rsSegmenti!IdSegmentoOrigine <> 0) And IsNull(rsTabelle!idTableJoin) Then
          statement = statement & vbCrLf
        Else
          'stefano:
          'idParent = getParentTable(rsTabelle!IdTable)
          idParent = getParentTable(rsTabelle!IdTable, IIf(IsNull(rsTabelle!idTableJoin), 0, rsTabelle!idTableJoin))
          '''''''''''''''''''
          'CHIAVI PRIMARIE:
          '''''''''''''''''''
          Set env = New Collection  'resetto;
          env.Add rsColonne
          env.Add rsTabelle
          If Len(cCopyArea) Then
            cCopyArea = getEnvironmentParam(cCopyAreaParam, "colonne", env)
          Else
            'segnalare errore
            cCopyArea = "RR_" & rsTabelle!Nome
          End If
          
          Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & rsTabelle!IdTable)
          If rsIndex.RecordCount > 0 Then
            Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
            Do While Not rsIdxCol.EOF
              
              Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
              If rsColonne.RecordCount Then
                Set env = New Collection  'resetto;
                env.Add rsColonne
                env.Add rsTabelle
                If Len(cCopyColumnParam) Then
                  rrName = getEnvironmentParam(cCopyColumnParam, "colonne", env)
                Else
                  'segnalare errore
                  rrName = rsColonne!Nome
                End If
                
                'SOLO COLONNE EREDITATE
                If rsColonne!IdTable <> rsColonne!idTableJoin Then
                  Set rsIndexParent = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & idParent)
                'If rsColonne!IdTableJoin = idParent Then
                  If rsColonne!IdCmp = -1 Then
                    'REV-FIELD: CHIAVE AGGIUNTA PER UNIVOCITA'  (verificare: idtable = idtablejoin)
                    Set rs = m_fun.Open_Recordset( _
                      "select nome from " & PrefDb & "_colonne where idtable=" & idParent & _
                      " and idtable = idtablejoin " & " and idcmp = " & rsColonne!IdCmp)
                    If rs.RecordCount Then
                      rrNameParent = rs!Nome
                    Else
                      'MsgBox "tmp: rrNameParent not found for column " & rsColonne!Nome & " - Table: " & rsTabelle!Nome
                      DataMan.DmFinestra.ListItems.Add , , Now & " rrNameParent not found for column " & rsColonne!Nome & " - Table: " & rsTabelle!Nome
                    End If
                    rs.Close
                  'Else
                  '  rrNameParent = Replace(rsColonne!NomeCmp, "_", "-")
                  'End If
                  Else
                  'stefano: che bel CASINO!!! idcmp non � pi� univoco, per cui una tabella al terzo
                  ' livello ha come idcmp ripetuto quelli ereditati da padre e nonno e fa un casino
                  ' a questo punto devo tirare dentro altre condizioni, spero che lo si rimetta a posto
                  ' prossimamente, cos� � un casino
                    Set rs = m_fun.Open_Recordset("select nome from " & PrefDb & "_colonne where idtable = " & idParent & " and idtablejoin = " & rsColonne!idTableJoin & " and idcmp = " & rsColonne!IdCmp)
                    If rs.RecordCount Then
                      rrNameParent = rs!Nome
                    Else
                      'MsgBox "tmp: rrNameParent not found for column " & rsColonne!Nome & " - Table: " & rsTabelle!Nome
                      DataMan.DmFinestra.ListItems.Add , , Now & " rrNameParent not found for column " & rsColonne!Nome & " - Table: " & rsTabelle!Nome
                    End If
                    rs.Close
                  End If
                  'MOVE
                  statement = statement & Space(6) & "KRR_" & rsIndex!Nome & "." & rrName & " =" & vbCrLf _
                      & Space(6) & "KRR_" & rsIndexParent!Nome & "." & rrNameParent & ";" & vbCrLf
  
                  rsIndexParent.Close
                End If  'end rsColonne!IdTable <> rsColonne!IdTableJoin
              Else
                DataMan.DmFinestra.ListItems.Add , , Now & " No column " & rsIdxCol!IdColonna & " for index " & rsIndex!Nome & "  And IdTable = " & rsTabelle!IdTable
            End If

              rsIdxCol.MoveNext
            Loop
          End If 'end "CHIAVI PRIMARIE"
        End If 'end "CONTINUE"
        statement = statement & Space(3) & "END;" & vbCrLf
      End If 'end "ok segmento"
    Else
      'no idOrigine
      m_fun.WriteLog "getSetcWkewkey: no origin segment for table: " & rsTabelle!Nome
      MsgBox "getSetcWkewkey: no origin segment for table: " & rsTabelle!Nome, vbExclamation, DataMan.DmNomeProdotto
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  statement = statement & Space(3) & "OTHERWISE DO;" & vbCrLf
  statement = statement & Space(3) & "END;" & vbCrLf
  getSetcWkeywkey_PLI = statement & "END;"
End Function

'stefano
'Public Function getParentTable(idtable As Long) As Long
Public Function getParentTable(IdTable As Long, idTableJoin As Long) As Long
  Dim r As Recordset
  On Error GoTo errordB
  
  'SQ TAG='AUTOMATIC'
  'stefano: le voglio tutte, anche OCCURS e REDEFINES
'  Set r = m_fun.Open_Recordset("SELECT d.idtable as parent from dmdb2_tabelle as a, " & _
'          "psdli_segmenti as b, psdli_segmenti as c, dmdb2_tabelle as d " & _
'          "where a.idtable = " & IdTable & " and a.idorigine = b.idsegmento " & _
'          "and b.parent = c.nome and b.IdOggetto=c.IdOggetto and c.idsegmento = d.idorigine and d.TAG='AUTOMATIC'")
  'SQ ?
  If Len(idTableJoin) = 0 Or idTableJoin = 0 Then
     'stefano: modifica temporanea: bisogna mettere tutti gli idtablejoin nella dmbd2_tabelle
     ' in modo da evitare questo giro, per ora non lo faccio perch� � troppo utilizzato
     Set r = m_fun.Open_Recordset("SELECT d.idtable as parent from " & PrefDb & "_tabelle as a, " & _
          "psdli_segmenti as b, psdli_segmenti as c, " & PrefDb & "_tabelle as d " & _
          "where a.idtable = " & IdTable & " and a.idorigine = b.idsegmento " & _
          "and b.parent = c.nome and b.IdOggetto=c.IdOggetto and c.idsegmento = d.idorigine and d.tag not like '%OCCURS%' and d.tag not like '%DB2%'")
     If r.RecordCount Then
       getParentTable = r!Parent
     Else
       getParentTable = 0
     End If
     r.Close
  Else
     getParentTable = idTableJoin
  End If
  
  
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getParentTable = ""
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''' INTEGRITA' REFERENZIALE "APPLICATIVA"
''' ELIMINA ALL/AUTOMATIC ITEMS
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''Public Sub deleteTableDB2(IdTable As Long, Optional eraseAll As Boolean)
''  Dim queryFilter As String
''
''  On Error GoTo errdb
''
''  If Not eraseAll Then queryFilter = " and tag like 'AUTOMATIC%'"
''
''  DataMan.DmConnection.Execute "DELETE * From DMDB2_Tabelle Where idtable = " & IdTable & queryFilter
''  DataMan.DmConnection.Execute "DELETE * From DMDB2_Alias Where idtable = " & IdTable & queryFilter
''  DataMan.DmConnection.Execute "DELETE * From DMDB2_Colonne Where idtable = " & IdTable & queryFilter
''  DataMan.DmConnection.Execute "DELETE * From DMDB2_Index Where idtable = " & IdTable & queryFilter
''  DataMan.DmConnection.Execute "DELETE * From DMDB2_IdxCol Where idtable = " & IdTable & queryFilter
''
''  Exit Sub
''errdb:
''  MsgBox "tmp: " & ERR.Description
''  Resume Next
''End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Valorizza le variabili globali:
' - idSegmentoParent, nomeSegmentoParent, idTableParent, nomeTableParent
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub setParentInfo(segmentName As String, idDbd As Long)
  Dim rs As Recordset
  
  Set rs = m_fun.Open_Recordset("select idSegmento,nome from PsDLI_Segmenti where " & _
                                         " nome='" & segmentName & "' AND idOggetto=" & idDbd)
  If rs.RecordCount Then
    idSegmentoParent = rs!IdSegmento
    nomeSegmentoParent = rs!Nome
    rs.Close
    Set rs = m_fun.Open_Recordset("select idTable,nome from " & PrefDb & "_Tabelle where " & _
                                           " idOrigine=" & idSegmentoParent & " and TAG='AUTOMATIC'")
    If rs.RecordCount Then
      idTableParent = rs!IdTable
      nomeTableParent = rs!Nome
    End If
  Else
    idTableParent = 0
    idSegmentoParent = 0
    nomeSegmentoParent = ""
    nomeTableParent = ""
  End If
  rs.Close
End Sub

'RKP
' SQ - tmp: ripulire il tutto!
Function getSetkAdlwkey(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, Tb1 As Recordset, Tb2 As Recordset, rsColonne As Recordset, TbCmp As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  Dim statement As String, rrName As String, wNomeCmp As String, Nome As String, cCopyArea As String
  Dim i_index As Long
  Dim env As Collection
  
  statement = "EVALUATE WK-NAMETABLE " & vbCrLf
  
  'tmp
  Dim wTipo As String
  wTipo = "RKP"
  
  'SQ TMP - Lasciamo da parte le occurs...
  'stefano: a me piacciono le occurs...
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idOrigine > 0" & _
  '                                     " and iddb=" & pIndex_DBCorrente & " order by idTable")
   Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB IN  (select IdDB from " & PrefDb & "_DB where idDBOr = " & pIndex_DBDCorrente & ") AND idorigine > 0 order by idTable")
                                     
  While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
    'SQ 26-05-06 - Segmenti virtuali: NON USIAMO LE KRR...
    'stefano: dobbiamo usarli...
    If rsSegmenti.RecordCount = 0 Then
       Set rsSegmenti = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
    End If
    If rsSegmenti.RecordCount Then
      'SQ: 26-08
      statement = statement & Space(3) & "WHEN '" & Replace(rsTabelle!Nome, "-", "_") & "' " & vbCrLf
      
      'stefanopippo: cambieremo quando rifaremo la routine completamente
      If wTipo = "KPR" Then
        Dim sqlcommand As String
        Dim daRifare As Recordset
        sqlcommand = "select * from " & PrefDb & "_index as a, " & PrefDb & "_idxcol as b, " & PrefDb & "_colonne as c " & _
            " where a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & _
            " and a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
            " and c.idtable <> c.idtablejoin"
        Set daRifare = m_fun.Open_Recordset(sqlcommand)
        If daRifare.RecordCount = 0 Then
          statement = statement & Space(6) & "CONTINUE" & vbCrLf
        End If
      End If
         
      'stefano: provo
      'If wTipo = "KUKP" Or wTipo = "KPKU" Then
      If wTipo = "KUKP" Then
'        sqlcommand = "SELECT * from DMDB2_INDEX as a, dmdb2_idxcol as b, dmdb2_colonne as c " & _
'            " where a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & _
'            " and a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
'            " and c.idtable = c.idtablejoin"
        sqlcommand = "SELECT * from " & PrefDb & "_INDEX as a, " & PrefDb & "_idxcol as b, " & PrefDb & "_colonne as c " & _
            " where a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & _
            " and a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
            " and c.idtable = c.idtablejoin and c.idcmp <> -1"
        Set daRifare = m_fun.Open_Recordset(sqlcommand)
        If daRifare.RecordCount = 0 Then
          statement = statement & Space(6) & "CONTINUE" & vbCrLf
        End If
      End If
         
      'stefanopippo: verifico se reinserire le redefines solo su WKEYADL e ADWLKEY
      'SP 15/9 e su WKEYWKUT
      If (wTipo = "KPKP" And rsSegmenti!Parent = "0") _
      Or ((rsSegmenti!IdSegmentoOrigine <> 0) And (wTipo <> "RKP" And wTipo <> "KPR" And wTipo <> "KPKU")) Then
          statement = statement & Space(6) & "CONTINUE" & vbCrLf
      Else
        '''''''''''''''''''
        'CHIAVI PRIMARIE:
        '''''''''''''''''''
        cCopyArea = LeggiParam("C-COPY-AREA")
        Set env = New Collection  'resetto;
        env.Add rsColonne
        env.Add rsTabelle
        If Len(cCopyArea) Then
          cCopyArea = Replace(getEnvironmentParam(cCopyArea, "colonne", env), "_", "-")
        Else
          'segnalare errore
          cCopyArea = "RR-" & Replace(rsTabelle!Nome, "_", "-")
        End If
        Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & rsTabelle!IdTable)
        If rsIndex.RecordCount > 0 Then
          Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
          Do While Not rsIdxCol.EOF
            Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
            rsColonne!Etichetta = rsColonne!Etichetta & ""
            '''''''''''''''''''''''
            'stefanopippo: controlla recordcount
            Set env = New Collection  'resetto;
            env.Add rsColonne
            env.Add rsTabelle
            If Len(cCopyColumnParam) Then
              rrName = getEnvironmentParam(cCopyColumnParam, "colonne", env)
            Else
              'segnalare errore
              rrName = rsColonne!Nome
            End If
            rrName = Replace(rrName, "_", "-")
            '
            Select Case wTipo
              Case "RD"
                If rsColonne!IdCmp > 0 Then
                  'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                  wNomeCmp = TbCmp!Nome
                End If
                If rsColonne!IdTable = rsColonne!idTableJoin Then
                  Nome = "RD-" & rsSegmenti!Nome
                  'If UCase(rsSegmenti!Comprtn) <> "NONE" Then Nome = "RD-" & rsSegmenti!Comprtn
                  If val(rsColonne!IdCmp & "") > 0 Then
                    statement = statement & CreaMvCmp(rrName, cCopyArea, rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage)
                  End If
                End If
              Case "DR"
                If rsColonne!IdCmp > 0 Then
                  'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                  wNomeCmp = TbCmp!Nome
                End If
                If rsColonne!IdTable = rsColonne!idTableJoin Then
                  Nome = "RD-" & rsSegmenti!Nome
                  'If UCase(rsSegmenti!Comprtn) <> "NONE" Then Nome = "RD-" & rsSegmenti!Comprtn
                  statement = statement & CreaMvCol(rrName, cCopyArea, rsColonne!Tipo, rsColonne!RoutRead & " ", wNomeCmp, Nome, TbCmp!Usage)
                End If
              Case "RKP"
                'stefanopippo: redefines
                'If rsSegmenti!IdSegmentoOrigine = 0 Or m_fun.FnNomeDB = "banca.mty" Then
                If rsSegmenti!IdSegmentoOrigine = 0 Then
                  statement = statement & Space(6) & "MOVE " & rrName & " OF " & cCopyArea & vbCrLf & _
                                          Space(6) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                Else
                  statement = statement & Space(6) & "MOVE " & rrName & " OF " & cCopyArea & vbCrLf & _
                                        Space(6) & "  TO " & Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                End If
              Case "KPR"
          'Stefanopippo: tolte le chiavi non ereditate per la WKEYADL: davano abend asra
          'tanto non le deve recuperare (servono solo le chiavi ereditate per la
          'insert)
                If rsColonne!IdTable <> rsColonne!idTableJoin Then
                  'stefanopippo: redefines
                  If rsSegmenti!IdSegmentoOrigine = 0 Then
                    statement = statement & Space(6) & "MOVE " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf & _
                                        Space(6) & "  TO " & rrName & " OF " & cCopyArea & vbCrLf
                  Else
                    statement = statement & Space(6) & "MOVE " & Replace(rsColonne!Etichetta, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf & _
                                        Space(6) & "  TO " & rrName & " OF " & cCopyArea & vbCrLf
                  End If
                End If
              Case "CRKP"
                If rsColonne!IdTable <> rsColonne!idTableJoin Then
                  Set Tb1 = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idtable = " & rsColonne!idTableJoin)
                  Nome = "KRR-" & Replace(rsIndex!Nome, "_", "-")
                  statement = statement & Space(6) & "MOVE " & rrName & " OF " & "RR-" & Replace(Tb1!Nome, "_", "-") & vbCrLf & _
                                          Space(6) & "  TO " & rrName & " OF " & Nome & vbCrLf
                End If
              Case "KPKP"
               If rsColonne!IdTable <> rsColonne!idTableJoin Then
              'SP: anche per alpitour non funzionava: l'ho cambiato io:
                  'Set Tb1 = m_fun.Open_Recordset("select * from dmdb2_tabelle where idtable = " & rsColonne!IdTableJoin)
                  'Set Tb2 = m_fun.Open_Recordset("select * from dmdb2_index where tipo = 'P' and idtable = " & Tb1!IdTable)
                  Dim idParent As Long
                  'SQ 7-07-06
                  'stefano: momentaneo
                  'idParent = getParentTable(rsColonne!IdTable)
                  idParent = getParentTable(rsColonne!IdTable, rsColonne!idTableJoin)
                  Set Tb2 = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & idParent)
                  'If rsColonne!IdTableJoin = idParent Then
                  '  'SQ 22-06-06 CONTROLLARE!
                  '  If Len(rsColonne!NomeCmp) Then
                  '    statement = statement & Space(6) & "MOVE " & Replace(rsColonne!NomeCmp, "_", "-") & " OF " & "KRR-" & Tb2!Nome & vbCrLf & _
                  '                      Space(6) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                  '  Else
                  '    'chiarire!!!!
                  '  End If
                  'Else
                  '   Set Tb1 = m_fun.Open_Recordset("select * from dmdb2_colonne where idtable = " & idParent & " and idtable <> idtablejoin and idcmp = " & rsColonne!IdCmp)
                  '   statement = statement & Space(6) & "MOVE " & Replace(Tb1!Nome, "_", "-") & " OF " & "KRR-" & Tb2!Nome & vbCrLf & _
                  '                      Space(6) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                  'End If
                  Set Tb1 = m_fun.Open_Recordset( _
                    "select * from " & PrefDb & "_colonne where idtable=" & rsColonne!idTableJoin & " and idcmp = " & rsColonne!IdCmp)
                  statement = statement & Space(6) & "MOVE " & Replace(Tb1!Nome, "_", "-") & " OF " & "KRR-" & Tb2!Nome & vbCrLf & _
                                        Space(6) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
               End If
              Case "KUKP"
                If rsColonne!IdCmp > 0 Then
                  'Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp)
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                  If TbCmp.RecordCount Then
                    wNomeCmp = TbCmp!Nome
                  Else
                    MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                    TbCmp.Close
                    Exit Function
                  End If
                  'stefano: mah
                  'End If
                  If rsColonne!IdTable = rsColonne!idTableJoin Then
                    If m_fun.FnNomeDB = "banca.mty" Then
                      '"KY_AB_SEG_AB01L"  -> W-AB0-KEY
                      'statement = statement & getKeyMove_BPER("W-" & Mid(rsColonne!Nome, 11, 3) & "-KEY", "KRD-" & rsSegmenti!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-")) & vbCrLf & vbCrLf
                      'statement = statement & Space(6) & "MOVE KRD-" & rsSegmenti!Nome & " TO KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                      'SQ: DL1
                      If rsColonne!IdCmp > 0 Then 'NBSEQ
                        'SQ 29-08
                        'statement = statement & Space(6) & "MOVE W-" & Left(rsSegmenti!Nome, 3) & "-KEY" & " TO KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                        statement = statement & Space(6) & "MOVE W-" & Left(rsSegmenti!Nome, 3) & "-KEY" & vbCrLf & _
                                                Space(6) & "  TO KY-" & Replace(rsTabelle!Nome, "_", "-") & " OF KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                      Else
                        statement = statement & Space(6) & "CONTINUE" & vbCrLf
                      End If
                    Exit Do
                  Else
                  'alpitour: 5/8/2005 � tardi....
                    '  Nome = "KRD-" & RsSegmenti!Nome
                     Nome = "KRD-" & Replace(rsIndex!Nome, "_", "-")
                    'If UCase(rsSegmenti!Comprtn) <> "NONE" Then Nome = "KRD-" & rsSegmenti!Comprtn
                    'statement = statement & CreaMvCol(rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutRead & " ", wNomeCmp, Nome, TbCmp!Usage)
                    'VC
                    'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, rsTabelle!Nome, nomeCampo, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage)
                    'alpitour: 5/8/2005 mancano tag
                    'statement = statement & CreaMvCol_TMP(rtbFile, RsSegmenti!Nome, RsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage, True)
                    Dim pos As Integer
                    Dim Lun As Integer
                    Dim oldKey As String
                    If pos = 0 Then pos = 1
                    If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                       pos = 1
                       oldKey = Replace(rsIndex!Nome, "_", "-")
                    End If
                    Lun = TbCmp!Byte
                    If Len(rsColonne!RoutKey) = 0 Then
                      statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    Else
                      statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutKey & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    End If
                    pos = pos + TbCmp!Byte
                  'stefano: mah
                  End If
                  End If
                End If
            'SP 5/9 gestione MOVE da KRR a KRD per keyfeedbackarea: da verificare bene!!!!
              Case "KPKU"
                If rsColonne!IdCmp > 0 Then
                  'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                  If TbCmp.RecordCount Then
                    wNomeCmp = TbCmp!Nome
                  Else
                    MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                    TbCmp.Close
                    Exit Function
                  End If
                'stefano: mah
                'End If
                'SP 5/9 da KRR a KRD tratta anche le chiavi ereditate
                'If rsColonne!IdTable = rsColonne!IdTableJoin Then
                  If m_fun.FnNomeDB = "banca.mty" Then
                    If rsColonne!IdCmp > 0 Then 'NBSEQ
                      statement = statement & Space(6) & "MOVE KY-" & Replace(rsTabelle!Nome, "_", "-") & " OF KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf & _
                                              Space(6) & "  TO W-" & Left(rsSegmenti!Nome, 3) & "-KEY" & vbCrLf
                    Else
                      statement = statement & Space(6) & "CONTINUE" & vbCrLf
                    End If
                    Exit Do
                  Else
                    'SP 5/9 per ora non imposto la posizione/lunghezza: se la trova da errore...
                    Nome = "KRDP-" & Replace(rsIndex!Nome, "_", "-")
                    If pos = 0 Then pos = 1
                    If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                       pos = 1
                       oldKey = Replace(rsIndex!Nome, "_", "-")
                    End If
                    Lun = TbCmp!Byte
                    Dim newTempl As String
                    'stefano: no template
                    If IsNull(rsColonne!RoutRead) Then
                      newTempl = ""
                    Else
                      newTempl = rsColonne!RoutRead & ""
                    End If
                    'SP 12/9: correzione per casi di azienda fissa su template
                    'If newTempl = "" Then
                    If newTempl = "" Or rsColonne!IdTable <> rsColonne!idTableJoin Then
                      Dim rsebasta As Recordset
                      Set rsebasta = m_fun.Open_Recordset("select * from dmdb2_colonne where idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                      If Not rsebasta.EOF Then
                        newTempl = rsebasta!RoutRead & ""
                      Else
                        newTempl = ""
                      End If
                    End If
                    '14/9 RR invece di KRR per la keyfeedbackarea
                    'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, RsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    If rsSegmenti!IdSegmentoOrigine <> 0 Then
                       wNomeCmp = Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "_", "-")
                    End If
                    statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR-" & Replace(rsTabelle!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    pos = pos + TbCmp!Byte
                  End If
                  'stefano
                 End If
                'End If
            End Select
            rsIdxCol.MoveNext
          Loop
        End If 'end "CHIAVI PRIMARIE"
        '23-07-05
        'SP 5/9: keyfeedback
        If wTipo = "KUKP" Or wTipo = "KPKU" Then
          i_index = 0
          ''''''''''''''''''''''
          ' CHIAVI SECONDARIE:
          ''''''''''''''''''''''
          Set rsIndex = m_fun.Open_Recordset("select * from dmdb2_index where tipo = 'I' and used = true and idtable = " & rsTabelle!IdTable & " order by idIndex")  'ordine necessario per nome...
          If m_fun.FnNomeDB = "banca.mty" Then
            While Not rsIndex.EOF
              '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
              ' Tutte colonne aggiunte; mi basta il nome della colonna...
              '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
              Set rsIdxCol = m_fun.Open_Recordset("select a.Nome from DMDB2_Colonne as a, DMDB2_IdxCol as b WHERE a.IdColonna=b.IdColonna and b.IdIndex=" & rsIndex!IdIndex)
              While Not rsIdxCol.EOF
                If Left(rsIdxCol!Nome, 3) <> "ID_" Then 'non mi interessano!    P.S.: non partire dagli indici DB2, ma dai Field con correzione...
                    'i_index = i_index + 1 'serve per il nome
                    'statement = statement & vbCrLf & "01 KRD-" & rsSegmenti!Nome & "-I" & i_index & "." & vbCrLf
                    'SQ DL1
                    'statement = statement & Space(6) & "MOVE KRD-" & Mid(rsIndex!Nome, 4, 3) & "KEY" & Right(rsIndex!Nome, 2) & " TO KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                    statement = statement & Space(6) & "MOVE W-" & Mid(rsIndex!Nome, 4, 3) & "-KEY" & Right(rsIndex!Nome, 2) & " TO KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                End If
                rsIdxCol.MoveNext
              Wend
              rsIndex.MoveNext
            Wend
          Else
            While Not rsIndex.EOF
            'stefano: provo
              Set rsIdxCol = m_fun.Open_Recordset("select * from DMDB2_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
              i_index = i_index + 1  'nome campo: KRD_<nome-segm>_I<i_index>
              While Not rsIdxCol.EOF
                Set rsColonne = m_fun.Open_Recordset("select * from DMDB2_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
                If rsColonne!IdCmp > 0 Then
                  rrName = Replace(rsColonne!Nome, "_", "-")
                  'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                    If TbCmp.RecordCount Then
                      If rsColonne!Nome = rsColonne!NomeCmp Then
                        wNomeCmp = TbCmp!Nome
                      Else
                        wNomeCmp = rsColonne!Nome
                      End If
                      If wTipo = "KPKU" Then
                        Nome = "KRDP-" & Replace(rsIndex!Nome, "_", "-")
                      Else
                        Nome = "KRD-" & Replace(rsIndex!Nome, "_", "-")
                      End If

                      newTempl = rsColonne!RoutWrite
                      If wTipo = "KPKU" Then
                        newTempl = rsColonne!RoutRead
                        If newTempl = "" Then
                          Set rsebasta = m_fun.Open_Recordset("select * from dmdb2_colonne where idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                          If Not rsebasta.EOF Then
                            newTempl = rsebasta!RoutRead
                          Else
                            newTempl = ""
                          End If
                        End If
                      End If
                      If pos = 0 Then pos = 1
                      If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                        pos = 1
                        oldKey = Replace(rsIndex!Nome, "_", "-")
                      End If
                      Lun = TbCmp!Byte
                    '14/9 RR invece di KRR per la keyfeedbackarea
                      If wTipo = "KPKU" Then
                        statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR-" & Replace(rsTabelle!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, False, pos, Lun)
                      Else
                        If Len(rsColonne!RoutKey) = 0 Then
                          statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, False, pos, Lun)
                        Else
                          statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutKey & "", wNomeCmp, Nome, TbCmp!Usage, False, pos, Lun)
                        End If
                      End If
                      pos = pos + TbCmp!Byte
                    Else
                      MsgBox "No cobol field found for " & rsColonne!Nome & " column.", vbExclamation
                      Exit Function
                    End If
                End If
                rsIdxCol.MoveNext
              Wend
              rsIdxCol.Close
              rsIndex.MoveNext
            Wend
          End If
        End If
      End If 'end "CONTINUE"
    'SQ 26-05-06
    'Else
    '  statement = statement & Space(6) & "* NON ESISTE CHIAVE PRIMARIA " & vbCrLf
    End If
    rsTabelle.MoveNext
  Wend
  statement = statement & "END-EVALUATE. "
  getSetkAdlwkey = statement
End Function

Function getSetkAdlwkey_PLI(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, Tb1 As Recordset, Tb2 As Recordset, rsColonne As Recordset, TbCmp As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  Dim statement As String, rrName As String, wNomeCmp As String, Nome As String, cCopyArea As String
  Dim i_index As Long
  Dim env As Collection
  
  statement = "SELECT (WK_NAMETABLE);" & vbCrLf
  
  'tmp
  Dim wTipo As String
  wTipo = "RKP"
  
  'SQ TMP - Lasciamo da parte le occurs...
  'stefano: a me piacciono le occurs...
  
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "idDB IN  (select IdDB from " & PrefDb & "_DB where " & _
                                       "idDBOr = " & pIndex_DBDCorrente & ") AND idorigine > 0 order by idTable")
  While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    'SQ 26-05-06 - Segmenti virtuali: NON USIAMO LE KRR...
    'stefano: dobbiamo usarli...
    If rsSegmenti.RecordCount = 0 Then
       Set rsSegmenti = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    End If
    If rsSegmenti.RecordCount Then
      'SQ: 26-08
      statement = statement & Space(3) & "WHEN ('" & rsTabelle!Nome & "') DO;" & vbCrLf
      
      'stefanopippo: cambieremo quando rifaremo la routine completamente
      If wTipo = "KPR" Then
        Dim sqlcommand As String
        Dim daRifare As Recordset
        sqlcommand = "select * from " & PrefDb & "_index as a, " & PrefDb & "_idxcol as b, " & PrefDb & "_colonne as c " & _
            " where a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & _
            " and a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
            " and c.idtable <> c.idtablejoin"
        Set daRifare = m_fun.Open_Recordset(sqlcommand)
        If daRifare.RecordCount = 0 Then
          statement = statement & vbCrLf
        End If
      End If
         
      'stefano: provo
      'If wTipo = "KUKP" Or wTipo = "KPKU" Then
      If wTipo = "KUKP" Then
'        sqlcommand = "SELECT * from DMDB2_INDEX as a, dmdb2_idxcol as b, dmdb2_colonne as c " & _
'            " where a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & _
'            " and a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
'            " and c.idtable = c.idtablejoin"
        sqlcommand = "SELECT * from " & PrefDb & "_INDEX as a, " & PrefDb & "_idxcol as b, " & PrefDb & "_colonne as c " & _
            " where a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & _
            " and a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
            " and c.idtable = c.idtablejoin and c.idcmp <> -1"
        Set daRifare = m_fun.Open_Recordset(sqlcommand)
        If daRifare.RecordCount = 0 Then
          statement = statement & Space(6) & ";" & vbCrLf
        End If
      End If
         
      'stefanopippo: verifico se reinserire le redefines solo su WKEYADL e ADWLKEY
      'SP 15/9 e su WKEYWKUT
      If (wTipo = "KPKP" And rsSegmenti!Parent = "0") _
      Or ((rsSegmenti!IdSegmentoOrigine <> 0) And (wTipo <> "RKP" And wTipo <> "KPR" And wTipo <> "KPKU")) Then
          statement = statement & Space(6) & ";" & vbCrLf
      Else
        '''''''''''''''''''
        'CHIAVI PRIMARIE:
        '''''''''''''''''''
        cCopyArea = LeggiParam("C-COPY-AREA")
        Set env = New Collection  'resetto;
        env.Add rsColonne
        env.Add rsTabelle
        If Len(cCopyArea) Then
          cCopyArea = Replace(getEnvironmentParam(cCopyArea, "colonne", env), "-", "_")
        Else
          'segnalare errore
          cCopyArea = "RR_" & rsTabelle!Nome
        End If
        Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & rsTabelle!IdTable)
        If rsIndex.RecordCount > 0 Then
          Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
          Do While Not rsIdxCol.EOF
            Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
            If rsColonne.RecordCount Then
              rsColonne!Etichetta = rsColonne!Etichetta & ""
              '''''''''''''''''''''''
              'stefanopippo: controlla recordcount
              Set env = New Collection  'resetto;
              env.Add rsColonne
              env.Add rsTabelle
              If Len(cCopyColumnParam) Then
                rrName = getEnvironmentParam(cCopyColumnParam, "colonne", env)
              Else
                'segnalare errore
                rrName = rsColonne!Nome
              End If
              rrName = Replace(rrName, "-", "_")
              '
              Select Case wTipo
                Case "RD"
                  If rsColonne!IdCmp > 0 Then
                    'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                    wNomeCmp = TbCmp!Nome
                  End If
                  If rsColonne!IdTable = rsColonne!idTableJoin Then
                     Nome = "RD_" & rsSegmenti!Nome
                     '    If UCase(rsSegmenti!Comprtn) <> "NONE" Then Nome = "RD-" & rsSegmenti!Comprtn
                     If val(rsColonne!IdCmp & "") > 0 Then
                        statement = statement & CreaMvCmp(rrName, cCopyArea, rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage)
                     End If
                  End If
                Case "DR"
                  If rsColonne!IdCmp > 0 Then
                    'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                    wNomeCmp = TbCmp!Nome
                  End If
                  If rsColonne!IdTable = rsColonne!idTableJoin Then
                     Nome = "RD_" & rsSegmenti!Nome
                     '     If UCase(rsSegmenti!Comprtn) <> "NONE" Then Nome = "RD-" & rsSegmenti!Comprtn
                    statement = statement & CreaMvCol(rrName, cCopyArea, rsColonne!Tipo, rsColonne!RoutRead & " ", wNomeCmp, Nome, TbCmp!Usage)
                  End If
                Case "RKP"
                  If rsSegmenti!IdSegmentoOrigine = 0 Then
                      statement = statement & Space(6) & "KRR_" & rsIndex!Nome & "." & rsColonne!Nome & " =" & vbCrLf _
                      & Space(6) & cCopyArea & "." & rrName & ";" & vbCrLf
                  Else
                    statement = statement & Space(6) & "KRR_" & rsIndex!Nome & "." & Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "-", "_") & " =" & vbCrLf _
                      & Space(6) & cCopyArea & "." & rrName & ";" & vbCrLf
                  End If
                Case "KPR"
            'Stefanopippo: tolte le chiavi non ereditate per la WKEYADL: davano abend asra
            'tanto non le deve recuperare (servono solo le chiavi ereditate per la
            'insert)
                  If rsColonne!IdTable <> rsColonne!idTableJoin Then
                    If rsSegmenti!IdSegmentoOrigine = 0 Then
                      statement = statement & Space(6) & "cCopyArea" & "." & rrName & " =" & vbCrLf _
                          & Space(6) & "KRR_" & rsIndex!Nome & "." & rsColonne!Nome & ";" & vbCrLf
                    Else
                      statement = statement & Space(6) & cCopyArea & "." & rrName & " =" & vbCrLf _
                          & Space(6) & "KRR_" & rsIndex!Nome & "." & Replace(rsColonne!Etichetta, "-", "_") & ";" & vbCrLf
                    End If
                  End If
                Case "CRKP"
                  If rsColonne!IdTable <> rsColonne!idTableJoin Then
                    Set Tb1 = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idtable = " & rsColonne!idTableJoin)
                    Nome = "KRR_" & rsIndex!Nome
                    statement = statement & Space(6) & Nome & "." & rrName & " =" & vbCrLf _
                                           & Space(6) & "RR_" & Tb1!Nome & "." & rrName & ";" & vbCrLf
                  End If
                Case "KPKP"
                 If rsColonne!IdTable <> rsColonne!idTableJoin Then
               
                    Dim idParent As Long
                    
                    idParent = getParentTable(rsColonne!IdTable, rsColonne!idTableJoin)
                    Set Tb2 = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & idParent)
                    
                    Set Tb1 = m_fun.Open_Recordset( _
                      "select * from " & PrefDb & "_colonne where idtable=" & rsColonne!idTableJoin & " and idcmp = " & rsColonne!IdCmp)
                     statement = statement & Space(6) & "KRR_" & rsIndex!Nome & "." & rsColonne!Nome & " =" & vbCrLf _
                                           & Space(6) & "KRR_" & Tb2!Nome & "." & Tb1!Nome & ";" & vbCrLf
                 End If
                Case "KUKP"
                  If rsColonne!IdCmp > 0 Then
                    
                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                    If TbCmp.RecordCount Then
                      wNomeCmp = TbCmp!Nome
                    Else
                      MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                      TbCmp.Close
                      Exit Function
                    End If
                    If rsColonne!IdTable = rsColonne!idTableJoin Then
                      Nome = "KRD_" & rsIndex!Nome
                      Dim pos As Integer
                      Dim Lun As Integer
                      Dim oldKey As String
                      If pos = 0 Then pos = 1
                      If rsIndex!Nome <> oldKey Then
                         pos = 1
                         oldKey = rsIndex!Nome
                      End If
                      Lun = TbCmp!Byte
                      If Len(rsColonne!RoutKey) = 0 Then
                        statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR_" & Replace(rsIndex!Nome, "-", "_"), rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                      Else
                        statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR_" & Replace(rsIndex!Nome, "-", "_"), rsColonne!Tipo, rsColonne!RoutKey & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                      End If
                      pos = pos + TbCmp!Byte
                    'stefano: mah
                    End If
                  End If
              'SP 5/9 gestione MOVE da KRR a KRD per keyfeedbackarea: da verificare bene!!!!
                Case "KPKU"
                  If rsColonne!IdCmp > 0 Then
                    'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                    If TbCmp.RecordCount Then
                      wNomeCmp = TbCmp!Nome
                    Else
                      MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                      TbCmp.Close
                      Exit Function
                    End If
                    'SP 5/9 per ora non imposto la posizione/lunghezza: se la trova da errore...
                    Nome = "KRDP_" & rsIndex!Nome
                    If pos = 0 Then pos = 1
                    If rsIndex!Nome <> oldKey Then
                       pos = 1
                       oldKey = Replace(rsIndex!Nome, "-", "_")
                    End If
                    Lun = TbCmp!Byte
                    Dim newTempl As String
                    'stefano: no template
                    If IsNull(rsColonne!RoutRead) Then
                       newTempl = ""
                    Else
                       newTempl = rsColonne!RoutRead & ""
                    End If
                    'SP 12/9: correzione per casi di azienda fissa su template
                    If newTempl = "" Or rsColonne!IdTable <> rsColonne!idTableJoin Then
                       Dim rsebasta As Recordset
                       Set rsebasta = m_fun.Open_Recordset("select * from dmdb2_colonne where idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                       If Not rsebasta.EOF Then
                         newTempl = rsebasta!RoutRead & ""
                       Else
                         newTempl = ""
                       End If
                    End If
                    '14/9 RR invece di KRR per la keyfeedbackarea
                    'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, RsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    If rsSegmenti!IdSegmentoOrigine <> 0 Then
                       wNomeCmp = Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "-", "_")
                    End If
                    statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR_" & Replace(rsTabelle!Nome, "-", "_"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    pos = pos + TbCmp!Byte
                  End If
              End Select
            Else
              DataMan.DmFinestra.ListItems.Add , , Now & " No column " & rsIdxCol!IdColonna & " for index " & rsIndex!Nome & "  And IdTable = " & rsTabelle!IdTable
            End If
            rsIdxCol.MoveNext
          Loop
        End If 'end "CHIAVI PRIMARIE"
        '23-07-05
        'SP 5/9: keyfeedback
        If wTipo = "KUKP" Or wTipo = "KPKU" Then
          i_index = 0
          ''''''''''''''''''''''
          ' CHIAVI SECONDARIE:
          ''''''''''''''''''''''
          Set rsIndex = m_fun.Open_Recordset("select * from dmdb2_index where tipo = 'I' and used = true and idtable = " & rsTabelle!IdTable & " order by idIndex")  'ordine necessario per nome...
          While Not rsIndex.EOF
            'stefano: provo
              Set rsIdxCol = m_fun.Open_Recordset("select * from DMDB2_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
              i_index = i_index + 1  'nome campo: KRD_<nome-segm>_I<i_index>
              While Not rsIdxCol.EOF
                Set rsColonne = m_fun.Open_Recordset("select * from DMDB2_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
                If rsColonne!IdCmp > 0 Then
                  rrName = rsColonne!Nome
                  'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                    If TbCmp.RecordCount Then
                      If rsColonne!Nome = rsColonne!NomeCmp Then
                         wNomeCmp = TbCmp!Nome
                      Else
                         wNomeCmp = rsColonne!Nome
                      End If
                      If wTipo = "KPKU" Then
                         Nome = "KRDP_" & rsIndex!Nome
                      Else
                         Nome = "KRD_" & rsIndex!Nome
                      End If

                      If Len(rsColonne!RoutKey) = 0 Then
                        newTempl = rsColonne!RoutWrite
                      Else
                        newTempl = rsColonne!RoutKey
                      End If
                      If wTipo = "KPKU" Then
                         newTempl = rsColonne!RoutRead
                        If newTempl = "" Then
                           Set rsebasta = m_fun.Open_Recordset("select * from dmdb2_colonne where idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                           If Not rsebasta.EOF Then
                              newTempl = rsebasta!RoutRead
                           Else
                              newTempl = ""
                           End If
                        End If
                      End If
                      If pos = 0 Then pos = 1
                      If rsIndex!Nome <> oldKey Then
                         pos = 1
                         oldKey = rsIndex!Nome
                      End If
                      Lun = TbCmp!Byte
                    '14/9 RR invece di KRR per la keyfeedbackarea
                      If wTipo = "KPKU" Then
                         statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR_" & Replace(rsTabelle!Nome, "-", "_"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, False, pos, Lun)
                      Else
                         statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR_" & Replace(rsIndex!Nome, "-", "_"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, False, pos, Lun)
                      End If
                      pos = pos + TbCmp!Byte
                    Else
                      MsgBox "No cobol field found for " & rsColonne!Nome & " column.", vbExclamation
                      Exit Function
                    End If
                End If
                rsIdxCol.MoveNext
              Wend
              rsIdxCol.Close
              rsIndex.MoveNext
            Wend
        End If
      End If 'end ";"
      statement = statement & Space(3) & "END;" & vbCrLf
    End If
    rsTabelle.MoveNext
  Wend
  statement = statement & Space(3) & "OTHERWISE DO;" & vbCrLf
  statement = statement & Space(3) & "END;" & vbCrLf
  statement = statement & "END;"
  getSetkAdlwkey_PLI = statement
End Function
'KPR
' SQ - tmp: ripulire il tutto!
Function getSetkWkeyadl(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, Tb1 As Recordset, Tb2 As Recordset, rsColonne As Recordset, TbCmp As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  Dim statement As String, rrName As String, wNomeCmp As String, Nome As String, cCopyArea As String
  Dim i_index As Long
  Dim env As Collection
  
  statement = "EVALUATE WK-NAMETABLE " & vbCrLf
  
  'tmp
  Dim wTipo As String
  wTipo = "KPR"
  
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idOrigine > 0 and iddb=" & pIndex_DBCorrente & " order by idTable")
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where idDB IN  (select IdDB from " & PrefDb & "_DB where idDBOr = " & pIndex_DBDCorrente & ") AND idorigine > 0 order by idTable")
  
  While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine)
    End If
    If rsSegmenti.RecordCount Then
      'SQ: 26-08
      statement = statement & Space(3) & "WHEN '" & Replace(rsTabelle!Nome, "-", "_") & "' " & vbCrLf
      
      'stefanopippo: cambieremo quando rifaremo la routine completamente
        Dim sqlcommand As String
        Dim daRifare As Recordset
      sqlcommand = "select * from " & PrefDb & "_index as a, " & PrefDb & "_idxcol as b, " & PrefDb & "_colonne as c " & _
          " where a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & _
          " and a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
          " and c.idtable <> c.idtablejoin"
      Set daRifare = m_fun.Open_Recordset(sqlcommand)
      If daRifare.RecordCount = 0 Then
        statement = statement & Space(6) & "CONTINUE" & vbCrLf
      End If
         
      'stefanopippo: verifico se reinserire le redefines solo su WKEYADL e ADWLKEY
      'SP 15/9 e su WKEYWKUT
      If (False) _
      Or False Then
          statement = statement & Space(6) & "CONTINUE" & vbCrLf
      Else
        '''''''''''''''''''
        'CHIAVI PRIMARIE:
        '''''''''''''''''''
        cCopyArea = LeggiParam("C-COPY-AREA")
        Set env = New Collection  'resetto;
        env.Add rsColonne
        env.Add rsTabelle
        If Len(cCopyArea) Then
          cCopyArea = Replace(getEnvironmentParam(cCopyArea, "colonne", env), "_", "-")
        Else
          'segnalare errore
          cCopyArea = "RR-" & Replace(rsTabelle!Nome, "_", "-")
        End If
        Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & rsTabelle!IdTable)
        If rsIndex.RecordCount > 0 Then
          Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
          Do While Not rsIdxCol.EOF
            Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
            rsColonne!Etichetta = rsColonne!Etichetta & ""
            '''''''''''''''''''''''
            'stefanopippo: controlla recordcount
            Set env = New Collection  'resetto;
            env.Add rsColonne
            env.Add rsTabelle
            If Len(cCopyColumnParam) Then
              rrName = getEnvironmentParam(cCopyColumnParam, "colonne", env)
            Else
              'segnalare errore
              rrName = rsColonne!Nome
            End If
            rrName = Replace(rrName, "_", "-")
            '
            Select Case wTipo
              Case "RD"
                If rsColonne!IdCmp > 0 Then
                  'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                  wNomeCmp = TbCmp!Nome
                End If
                If rsColonne!IdTable = rsColonne!idTableJoin Then
                   Nome = "RD-" & rsSegmenti!Nome
                   '    If UCase(rsSegmenti!Comprtn) <> "NONE" Then Nome = "RD-" & rsSegmenti!Comprtn
                   If val(rsColonne!IdCmp & "") > 0 Then
                      statement = statement & CreaMvCmp(rrName, cCopyArea, rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage)
                   End If
                End If
              Case "DR"
                If rsColonne!IdCmp > 0 Then
                  'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                  wNomeCmp = TbCmp!Nome
                End If
                If rsColonne!IdTable = rsColonne!idTableJoin Then
                   Nome = "RD-" & rsSegmenti!Nome
                   '     If UCase(rsSegmenti!Comprtn) <> "NONE" Then Nome = "RD-" & rsSegmenti!Comprtn
                  statement = statement & CreaMvCol(rrName, cCopyArea, rsColonne!Tipo, rsColonne!RoutRead & " ", wNomeCmp, Nome, TbCmp!Usage)
                End If
              Case "RKP"
                'stefanopippo: redefines
                'If rsSegmenti!IdSegmentoOrigine = 0 Or m_fun.FnNomeDB = "banca.mty" Then
                If rsSegmenti!IdSegmentoOrigine = 0 Then
                   statement = statement & Space(6) & "MOVE " & rrName & " OF " & cCopyArea & vbCrLf & _
                                        Space(6) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                Else
                  statement = statement & Space(6) & "MOVE " & rrName & " OF " & cCopyArea & vbCrLf & _
                                        Space(6) & "  TO " & Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                End If
              Case "KPR"
                'Stefanopippo: tolte le chiavi non ereditate per la WKEYADL: davano abend asra
                'tanto non le deve recuperare (servono solo le chiavi ereditate per la
                'insert)
                If rsColonne!IdTable <> rsColonne!idTableJoin Then
                  'stefanopippo: redefines
                  If rsSegmenti!IdSegmentoOrigine = 0 Then
                    statement = statement & Space(6) & "MOVE " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf & _
                                        Space(6) & "  TO " & rrName & " OF " & cCopyArea & vbCrLf
                  Else
                    'SQ - Chiarire l'utilizzo di "etichetta"!!
                    statement = statement & Space(6) & "MOVE " & IIf(Len(rsColonne!Etichetta), rsColonne!Etichetta, rrName) & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf & _
                                        Space(6) & "  TO " & rrName & " OF " & cCopyArea & vbCrLf
                  End If
                End If
              Case "CRKP"
                If rsColonne!IdTable <> rsColonne!idTableJoin Then
                  Set Tb1 = m_fun.Open_Recordset("select * from dmdb2_tabelle where idtable = " & rsColonne!idTableJoin)
                  Nome = "KRR-" & Replace(rsIndex!Nome, "_", "-")
                  statement = statement & Space(6) & "MOVE " & rrName & " OF " & "RR-" & Replace(Tb1!Nome, "_", "-") & vbCrLf & _
                                          Space(6) & "  TO " & rrName & " OF " & Nome & vbCrLf
                End If
              Case "KPKP"
               If rsColonne!IdTable <> rsColonne!idTableJoin Then
              'SP: anche per alpitour non funzionava: l'ho cambiato io:
                  'Set Tb1 = m_fun.Open_Recordset("select * from dmdb2_tabelle where idtable = " & rsColonne!IdTableJoin)
                  'Set Tb2 = m_fun.Open_Recordset("select * from dmdb2_index where tipo = 'P' and idtable = " & Tb1!IdTable)
                  Dim idParent As Long
                  'SQ 7-07-06
                  'stefano:
                  'idParent = getParentTable(rsColonne!IdTable)
                  idParent = getParentTable(rsColonne!IdTable, rsColonne!idTableJoin)
                  Set Tb2 = m_fun.Open_Recordset("select * from dmdb2_index where tipo = 'P' and idtable = " & idParent)
                  'If rsColonne!IdTableJoin = idParent Then
                  '  'SQ 22-06-06 CONTROLLARE!
                  '  If Len(rsColonne!NomeCmp) Then
                  '    statement = statement & Space(6) & "MOVE " & Replace(rsColonne!NomeCmp, "_", "-") & " OF " & "KRR-" & Tb2!Nome & vbCrLf & _
                  '                      Space(6) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                  '  Else
                  '    'chiarire!!!!
                  '  End If
                  'Else
                  '   Set Tb1 = m_fun.Open_Recordset("select * from dmdb2_colonne where idtable = " & idParent & " and idtable <> idtablejoin and idcmp = " & rsColonne!IdCmp)
                  '   statement = statement & Space(6) & "MOVE " & Replace(Tb1!Nome, "_", "-") & " OF " & "KRR-" & Tb2!Nome & vbCrLf & _
                  '                      Space(6) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                  'End If
                  Set Tb1 = m_fun.Open_Recordset( _
                    "select * from dmdb2_colonne where idtable=" & rsColonne!idTableJoin & " and idcmp = " & rsColonne!IdCmp)
                  statement = statement & Space(6) & "MOVE " & Replace(Tb1!Nome, "_", "-") & " OF " & "KRR-" & Tb2!Nome & vbCrLf & _
                                        Space(6) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
               End If
              Case "KUKP"
                If rsColonne!IdCmp > 0 Then
                  'Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp)
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                  If TbCmp.RecordCount Then
                    wNomeCmp = TbCmp!Nome
                  Else
                    MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                    TbCmp.Close
                    Exit Function
                  End If
                  'stefano: mah
                  'End If
                  If rsColonne!IdTable = rsColonne!idTableJoin Then
                    If m_fun.FnNomeDB = "banca.mty" Then
                      '"KY_AB_SEG_AB01L"  -> W-AB0-KEY
                      'statement = statement & getKeyMove_BPER("W-" & Mid(rsColonne!Nome, 11, 3) & "-KEY", "KRD-" & rsSegmenti!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-")) & vbCrLf & vbCrLf
                      'statement = statement & Space(6) & "MOVE KRD-" & rsSegmenti!Nome & " TO KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                      'SQ: DL1
                      If rsColonne!IdCmp > 0 Then 'NBSEQ
                        'SQ 29-08
                        'statement = statement & Space(6) & "MOVE W-" & Left(rsSegmenti!Nome, 3) & "-KEY" & " TO KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                        statement = statement & Space(6) & "MOVE W-" & Left(rsSegmenti!Nome, 3) & "-KEY" & vbCrLf & _
                                                Space(6) & "  TO KY-" & Replace(rsTabelle!Nome, "_", "-") & " OF KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                      Else
                        statement = statement & Space(6) & "CONTINUE" & vbCrLf
                      End If
                    Exit Do
                  Else
                  'alpitour: 5/8/2005 � tardi....
                    '  Nome = "KRD-" & RsSegmenti!Nome
                     Nome = "KRD-" & Replace(rsIndex!Nome, "_", "-")
                    'If UCase(rsSegmenti!Comprtn) <> "NONE" Then Nome = "KRD-" & rsSegmenti!Comprtn
                    'statement = statement & CreaMvCol(rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutRead & " ", wNomeCmp, Nome, TbCmp!Usage)
                    'VC
                    'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, rsTabelle!Nome, nomeCampo, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage)
                    'alpitour: 5/8/2005 mancano tag
                    'statement = statement & CreaMvCol_TMP(rtbFile, RsSegmenti!Nome, RsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage, True)
                    Dim pos As Integer
                    Dim Lun As Integer
                    Dim oldKey As String
                    If pos = 0 Then pos = 1
                    If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                       pos = 1
                       oldKey = Replace(rsIndex!Nome, "_", "-")
                    End If
                    Lun = TbCmp!Byte
                    If Len(rsColonne!RoutKey) = 0 Then
                      statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    Else
                      statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, rsColonne!RoutKey & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    End If
                    pos = pos + TbCmp!Byte
                  'stefano: mah
                  End If
                  End If
                End If
            'SP 5/9 gestione MOVE da KRR a KRD per keyfeedbackarea: da verificare bene!!!!
              Case "KPKU"
                If rsColonne!IdCmp > 0 Then
                  'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                  Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                  If TbCmp.RecordCount Then
                    wNomeCmp = TbCmp!Nome
                  Else
                    MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                    TbCmp.Close
                    Exit Function
                  End If
                'stefano: mah
                'End If
                'SP 5/9 da KRR a KRD tratta anche le chiavi ereditate
                'If rsColonne!IdTable = rsColonne!IdTableJoin Then
                  If m_fun.FnNomeDB = "banca.mty" Then
                    If rsColonne!IdCmp > 0 Then 'NBSEQ
                      statement = statement & Space(6) & "MOVE KY-" & Replace(rsTabelle!Nome, "_", "-") & " OF KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf & _
                                              Space(6) & "  TO W-" & Left(rsSegmenti!Nome, 3) & "-KEY" & vbCrLf
                    Else
                      statement = statement & Space(6) & "CONTINUE" & vbCrLf
                    End If
                    Exit Do
                  Else
                    'SP 5/9 per ora non imposto la posizione/lunghezza: se la trova da errore...
                    Nome = "KRDP-" & Replace(rsIndex!Nome, "_", "-")
                    If pos = 0 Then pos = 1
                    If Replace(rsIndex!Nome, "_", "-") <> oldKey Then
                       pos = 1
                       oldKey = Replace(rsIndex!Nome, "_", "-")
                    End If
                    Lun = TbCmp!Byte
                    Dim newTempl As String
                    'stefano: no template
                    If IsNull(rsColonne!RoutRead) Then
                       newTempl = ""
                    Else
                       newTempl = rsColonne!RoutRead & ""
                    End If
                    'SP 12/9: correzione per casi di azienda fissa su template
                    'If newTempl = "" Then
                    If newTempl = "" Or rsColonne!IdTable <> rsColonne!idTableJoin Then
                       Dim rsebasta As Recordset
                       Set rsebasta = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                       If Not rsebasta.EOF Then
                         newTempl = rsebasta!RoutRead & ""
                       Else
                         newTempl = ""
                       End If
                    End If
                    '14/9 RR invece di KRR per la keyfeedbackarea
                    'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, RsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    If rsSegmenti!IdSegmentoOrigine <> 0 Then
                       wNomeCmp = Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "_", "-")
                    End If
                    statement = statement & CreaMvCol_TMP(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR-" & Replace(rsTabelle!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    pos = pos + TbCmp!Byte
                  End If
                  'stefano
                 End If
                'End If
            End Select
            rsIdxCol.MoveNext
          Loop
        End If 'end "CHIAVI PRIMARIE"
      End If 'end "CONTINUE"
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  
  statement = statement & "END-EVALUATE. "
  getSetkWkeyadl = statement
End Function

Function getSetkWkeyadl_PLI(RtbFile As RichTextBox) As String
  Dim rsTabelle As Recordset, Tb1 As Recordset, Tb2 As Recordset, rsColonne As Recordset, TbCmp As Recordset
  Dim rsIndex As Recordset, rsIdxCol As Recordset, rsSegmenti As Recordset
  Dim statement As String, rrName As String, wNomeCmp As String, Nome As String, cCopyArea As String
  Dim i_index As Long
  Dim env As Collection
  Dim sqlcommand As String
  Dim daRifare As Recordset
  
  statement = "SELECT (WK_NAMETABLE);" & vbCrLf
  
  'tmp
  Dim wTipo As String
  wTipo = "KPR"
  
  'Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_tabelle where idOrigine > 0 and iddb=" & pIndex_DBCorrente & " order by idTable")
  Set rsTabelle = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where " & _
                                       "idDB IN  (select IdDB from " & PrefDb & "_DB where " & _
                                       "idDBOr = " & pIndex_DBDCorrente & ") AND idorigine > 0 order by idTable")
  While Not rsTabelle.EOF
    Set rsSegmenti = m_fun.Open_Recordset("select * from psdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    If rsSegmenti.RecordCount = 0 Then
      Set rsSegmenti = m_fun.Open_Recordset("select * from mgdli_segmenti where idsegmento = " & rsTabelle!IdOrigine & " and idoggetto = " & pIndex_DBDCorrente)
    End If
    If rsSegmenti.RecordCount Then
      'SQ: 26-08
      statement = statement & Space(3) & "WHEN ('" & rsTabelle!Nome & "') DO;" & vbCrLf
      
      'stefanopippo: cambieremo quando rifaremo la routine completamente
      sqlcommand = "select * from " & PrefDb & "_index as a, " & PrefDb & "_idxcol as b, " & PrefDb & "_colonne as c " & _
          " where a.tipo = 'P' and a.idtable = " & rsTabelle!IdTable & _
          " and a.idindex = b.idindex and b.idcolonna = c.idcolonna " & _
          " and c.idtable <> c.idtablejoin"
      Set daRifare = m_fun.Open_Recordset(sqlcommand)
      If daRifare.RecordCount = 0 Then
        statement = statement & vbCrLf
      End If
         
      'stefano: ????? cosa vuol dire questa if????
      If (False) _
      Or False Then
          statement = statement & Space(6) & ";" & vbCrLf
      Else
        '''''''''''''''''''
        'CHIAVI PRIMARIE:
        '''''''''''''''''''
        cCopyArea = LeggiParam("C-COPY-AREA")
        Set env = New Collection  'resetto;
        env.Add rsColonne
        env.Add rsTabelle
        If Len(cCopyArea) Then
          cCopyArea = Replace(getEnvironmentParam(cCopyArea, "colonne", env), "-", "_")
        Else
          'segnalare errore
          cCopyArea = "RR-" & Replace(rsTabelle!Nome, "-", "_")
        End If
        Set rsIndex = m_fun.Open_Recordset("select * from " & PrefDb & "_index where tipo = 'P' and idtable = " & rsTabelle!IdTable)
        If rsIndex.RecordCount > 0 Then
          Set rsIdxCol = m_fun.Open_Recordset("select * from " & PrefDb & "_IDXCOL where idindex = " & rsIndex!IdIndex & " order by ordinale ")
          Do While Not rsIdxCol.EOF
            Set rsColonne = m_fun.Open_Recordset("select * from " & PrefDb & "_COLONNE where idcolonna = " & rsIdxCol!IdColonna)
            If rsColonne.RecordCount Then
              rsColonne!Etichetta = rsColonne!Etichetta & ""
              '''''''''''''''''''''''
              'stefanopippo: controlla recordcount
              Set env = New Collection  'resetto;
              env.Add rsColonne
              env.Add rsTabelle
              If Len(cCopyColumnParam) Then
                rrName = getEnvironmentParam(cCopyColumnParam, "colonne", env)
              Else
                'segnalare errore
                rrName = rsColonne!Nome
              End If
              rrName = Replace(rrName, "-", "_")
              '
              Select Case wTipo
                Case "RD"
                  If rsColonne!IdCmp > 0 Then
                    'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                    wNomeCmp = TbCmp!Nome
                  End If
                  If rsColonne!IdTable = rsColonne!idTableJoin Then
                     Nome = "RD_" & rsSegmenti!Nome
                     '    If UCase(rsSegmenti!Comprtn) <> "NONE" Then Nome = "RD-" & rsSegmenti!Comprtn
                     If val(rsColonne!IdCmp & "") > 0 Then
                        statement = statement & CreaMvCmp(rrName, cCopyArea, rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage)
                     End If
                  End If
                Case "DR"
                  If rsColonne!IdCmp > 0 Then
                    'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                    wNomeCmp = TbCmp!Nome
                  End If
                  If rsColonne!IdTable = rsColonne!idTableJoin Then
                     Nome = "RD_" & rsSegmenti!Nome
                    statement = statement & CreaMvCol(rrName, cCopyArea, rsColonne!Tipo, rsColonne!RoutRead & " ", wNomeCmp, Nome, TbCmp!Usage)
                  End If
                Case "RKP"
                  
                  If rsSegmenti!IdSegmentoOrigine = 0 Then
  '                   statement = statement & Space(6) & "MOVE " & rrName & " OF " & cCopyArea & vbCrLf & _
  '                                        Space(6) & "  TO " & Replace(rsColonne!Nome, "-", "_") & " OF " & "KRR-" & Replace(rsIndex!Nome, "-", "_") & vbCrLf
                      statement = statement & Space(6) & "KRR_" & Replace(rsIndex!Nome, "-", "_") & "." & Replace(rsColonne!Nome, "-", "_") & " = " & vbCrLf & _
                      Space(6) & cCopyArea & "." & rrName & ";" & vbCrLf
                  Else
  '                  statement = statement & Space(6) & "MOVE " & rrName & " OF " & cCopyArea & vbCrLf & _
  '                                        Space(6) & "  TO " & Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "-", "_") & " OF " & "KRR-" & Replace(rsIndex!Nome, "-", "_") & vbCrLf
                    statement = statement & Space(6) & "KRR_" & Replace(rsIndex!Nome, "-", "_") & "." & Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "-", "_") & " = " & vbCrLf & _
                      Space(6) & cCopyArea & "." & rrName & ";" & vbCrLf
                  End If
                Case "KPR"
                  
                  If rsColonne!IdTable <> rsColonne!idTableJoin Then
                    If rsSegmenti!IdSegmentoOrigine = 0 Then
  '                    statement = statement & Space(6) & "MOVE " & Replace(rsColonne!Nome, "-", "_") & " OF " & "KRR-" & Replace(rsIndex!Nome, "-", "_") & vbCrLf & _
  '                                        Space(6) & "  TO " & rrName & " OF " & cCopyArea & vbCrLf
                      statement = statement & Space(6) & cCopyArea & "." & rrName & " = " & vbCrLf & _
                                          Space(6) & "KRR_" & Replace(rsIndex!Nome, "-", "_") & "." & Replace(rsColonne!Nome, "-", "_") & ";" & vbCrLf
                    Else
                      If False Then
                        If InStr(rsIndex!Nome, "_NBSEQ") Then
                          'statement = statement & Space(6) & "MOVE " & Replace(rsColonne!Etichetta, "-", "_") & " OF " & "KRR-" & Left(rsIndex!Nome, 4) & "1" & Mid(Replace(rsIndex!Nome, "-", "_"), 6) & vbCrLf & Space(6) & "  TO " & rrName & " OF " & cCopyArea & vbCrLf
                          statement = statement & Space(6) & cCopyArea & "." & rrName & " = " & vbCrLf & Space(6) & "KRR_" & Left(rsIndex!Nome, 4) & "1" & Mid(Replace(rsIndex!Nome, "-", "_"), 6) & "." & Replace(rsColonne!Etichetta, "-", "_") & ";" & vbCrLf
                        Else
  '                        statement = statement & Space(6) & "MOVE " & Replace(rsColonne!Etichetta, "-", "_") & " OF " & "KRR-" & Replace(rsIndex!Nome, "-", "_") & vbCrLf & Space(6) & "  TO " & rrName & " OF " & cCopyArea & vbCrLf
                          statement = statement & Space(6) & cCopyArea & "." & rrName & " = " & vbCrLf & Space(6) & "KRR_" & Replace(rsIndex!Nome, "-", "_") & "." & Replace(rsColonne!Etichetta, "-", "_") & ";" & vbCrLf
                        End If
                      Else
  '                      statement = statement & Space(6) & "MOVE " & IIf(Len(rsColonne!Etichetta), rsColonne!Etichetta, rrName) & " OF " & "KRR-" & Replace(rsIndex!Nome, "-", "_") & vbCrLf & _
  '                                        Space(6) & "  TO " & rrName & " OF " & cCopyArea & vbCrLf
                       statement = statement & Space(6) & cCopyArea & "." & rrName & " = " & vbCrLf & _
                                          Space(6) & "KRR_" & Replace(rsIndex!Nome, "-", "_") & "." & IIf(Len(rsColonne!Etichetta), rsColonne!Etichetta, rrName) & ";" & vbCrLf
                      End If
                    End If
                  End If
                ' SONO ARRIVATO QUI  <----------------------------------------------------
                
                ' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                
                
                '----------------------------------------------------------------------
                Case "CRKP"
                  If rsColonne!IdTable <> rsColonne!idTableJoin Then
                    Set Tb1 = m_fun.Open_Recordset("select * from dmdb2_tabelle where idtable = " & rsColonne!idTableJoin)
                    Nome = "KRR_" & rsIndex!Nome
                    statement = statement & Space(6) & Nome & "." & rrName & " = " & vbCrLf & _
                                          Space(6) & "RR_" & Tb1!Nome & "." & rrName & ";" & vbCrLf
                  End If
                Case "KPKP"
                 If rsColonne!IdTable <> rsColonne!idTableJoin Then
                'SP: anche per alpitour non funzionava: l'ho cambiato io:
                    'Set Tb1 = m_fun.Open_Recordset("select * from dmdb2_tabelle where idtable = " & rsColonne!IdTableJoin)
                    'Set Tb2 = m_fun.Open_Recordset("select * from dmdb2_index where tipo = 'P' and idtable = " & Tb1!IdTable)
                    Dim idParent As Long
                    'SQ 7-07-06
                    'stefano:
                    'idParent = getParentTable(rsColonne!IdTable)
                    idParent = getParentTable(rsColonne!IdTable, rsColonne!idTableJoin)
                    Set Tb2 = m_fun.Open_Recordset("select * from dmdb2_index where tipo = 'P' and idtable = " & idParent)
                    'If rsColonne!IdTableJoin = idParent Then
                    '  'SQ 22-06-06 CONTROLLARE!
                    '  If Len(rsColonne!NomeCmp) Then
                    '    statement = statement & Space(6) & "MOVE " & Replace(rsColonne!NomeCmp, "_", "-") & " OF " & "KRR-" & Tb2!Nome & vbCrLf & _
                    '                      Space(6) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                    '  Else
                    '    'chiarire!!!!
                    '  End If
                    'Else
                    '   Set Tb1 = m_fun.Open_Recordset("select * from dmdb2_colonne where idtable = " & idParent & " and idtable <> idtablejoin and idcmp = " & rsColonne!IdCmp)
                    '   statement = statement & Space(6) & "MOVE " & Replace(Tb1!Nome, "_", "-") & " OF " & "KRR-" & Tb2!Nome & vbCrLf & _
                    '                      Space(6) & "  TO " & Replace(rsColonne!Nome, "_", "-") & " OF " & "KRR-" & Replace(rsIndex!Nome, "_", "-") & vbCrLf
                    'End If
                    Set Tb1 = m_fun.Open_Recordset( _
                      "select * from dmdb2_colonne where idtable=" & rsColonne!idTableJoin & " and idcmp = " & rsColonne!IdCmp)
                    statement = statement & Space(6) & "KRR_" & rsIndex!Nome & "." & rsColonne!Nome & " = " & vbCrLf & _
                                          Space(6) & "KRR_" & Tb2!Nome & "." & rsColonne!Nome & ";" & vbCrLf
                 End If
                 
                Case "KUKP"
                  If rsColonne!IdCmp > 0 Then
                    'Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp)
                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                    If TbCmp.RecordCount Then
                      wNomeCmp = TbCmp!Nome
                    Else
                      MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                      TbCmp.Close
                      Exit Function
                    End If
                    'stefano: mah
                    'End If
                    If rsColonne!IdTable = rsColonne!idTableJoin Then
                    'alpitour: 5/8/2005 � tardi....
                      '  Nome = "KRD-" & RsSegmenti!Nome
                       Nome = "KRD_" & rsIndex!Nome
                      Dim pos As Integer
                      Dim Lun As Integer
                      Dim oldKey As String
                      If pos = 0 Then pos = 1
                      If Replace(rsIndex!Nome, "-", "_") <> oldKey Then
                         pos = 1
                         oldKey = Replace(rsIndex!Nome, "-", "_")
                      End If
                      Lun = TbCmp!Byte
                      If Len(rsColonne!RoutKey) = 0 Then
                        statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR_" & Replace(rsIndex!Nome, "-", "_"), rsColonne!Tipo, rsColonne!RoutWrite & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                      Else
                        statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "KRR_" & Replace(rsIndex!Nome, "-", "_"), rsColonne!Tipo, rsColonne!RoutKey & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                      End If
                      pos = pos + TbCmp!Byte
                    End If
                  End If
              'SP 5/9 gestione MOVE da KRR a KRD per keyfeedbackarea: da verificare bene!!!!
                Case "KPKU"
                  If rsColonne!IdCmp > 0 Then
                    'Set TbCmp = m_fun.Open_Recordset("select * from MGDATA_CMP where idcmp = " & rsColonne!IdCmp)
                    Set TbCmp = m_fun.Open_Recordset("select * from MgData_cmp where idcmp = " & rsColonne!IdCmp & " and idsegmento = " & rsTabelle!IdOrigine)
                    If TbCmp.RecordCount Then
                      wNomeCmp = TbCmp!Nome
                    Else
                      MsgBox "Table '" & rsTabelle!Nome & "': no cobol field found for '" & rsColonne!Nome & "' column.", vbExclamation
                      TbCmp.Close
                      Exit Function
                    End If
                  'stefano: mah
                  'End If
                  'SP 5/9 da KRR a KRD tratta anche le chiavi ereditate
                  'If rsColonne!IdTable = rsColonne!IdTableJoin Then
                      'SP 5/9 per ora non imposto la posizione/lunghezza: se la trova da errore...
                    Nome = "KRDP-" & Replace(rsIndex!Nome, "-", "_")
                    If pos = 0 Then pos = 1
                    If Replace(rsIndex!Nome, "-", "_") <> oldKey Then
                       pos = 1
                       oldKey = Replace(rsIndex!Nome, "-", "_")
                    End If
                    Lun = TbCmp!Byte
                    Dim newTempl As String
                    'stefano: no template
                    If IsNull(rsColonne!RoutRead) Then
                       newTempl = ""
                    Else
                       newTempl = rsColonne!RoutRead & ""
                    End If
                    'SP 12/9: correzione per casi di azienda fissa su template
                    'If newTempl = "" Then
                    If newTempl = "" Or rsColonne!IdTable <> rsColonne!idTableJoin Then
                       Dim rsebasta As Recordset
                       Set rsebasta = m_fun.Open_Recordset("select * from " & PrefDb & "_colonne where idcmp = " & rsColonne!IdCmp & " and idtable = idtablejoin")
                       If Not rsebasta.EOF Then
                         newTempl = rsebasta!RoutRead & ""
                       Else
                         newTempl = ""
                       End If
                    End If
                    '14/9 RR invece di KRR per la keyfeedbackarea
                    'statement = statement & CreaMvCol_TMP(rtbFile, rsSegmenti!Nome, RsTabelle!Nome, rrName, "KRR-" & Replace(rsIndex!Nome, "_", "-"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    If rsSegmenti!IdSegmentoOrigine <> 0 Then
                       wNomeCmp = Replace(IIf(IsNull(rsColonne!Etichetta) Or Len(rsColonne!Etichetta) = 0, rsColonne!Nome, rsColonne!Etichetta), "-", "_")
                    End If
                    statement = statement & CreaMvCol_TMP_PLI(RtbFile, rsSegmenti!Nome, rsTabelle!Nome, rrName, "RR_" & Replace(rsTabelle!Nome, "-", "_"), rsColonne!Tipo, newTempl & " ", wNomeCmp, Nome, TbCmp!Usage, True, pos, Lun)
                    pos = pos + TbCmp!Byte
                  End If
              End Select
            Else
              DataMan.DmFinestra.ListItems.Add , , Now & " No column " & rsIdxCol!IdColonna & " for index " & rsIndex!Nome & "  And IdTable = " & rsTabelle!IdTable
            End If
            rsIdxCol.MoveNext
          Loop
        End If 'end "CHIAVI PRIMARIE"
      End If 'end "CONTINUE"
      statement = statement & Space(3) & "END;" & vbCrLf
    End If
    rsTabelle.MoveNext
  Wend
  rsTabelle.Close
  statement = statement & Space(3) & "OTHERWISE DO;" & vbCrLf
  statement = statement & Space(3) & "END;" & vbCrLf
  statement = statement & "END;"
  getSetkWkeyadl_PLI = statement
End Function
