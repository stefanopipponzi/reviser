VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MadmdC_IdxCol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_IdColonna As Long
Private m_Order As String
Private m_Tag As String

'ID COLONNA
Public Property Get IdColonna() As Long
   IdColonna = m_IdColonna
End Property

Public Property Let IdColonna(cIdColonna As Long)
   m_IdColonna = cIdColonna
End Property

'ORDER
Public Property Get Order() As String
   Order = m_Order
End Property

Public Property Let Order(cOrder As String)
   m_Order = Order
End Property

'TAG
Public Property Get Tags() As String
   Tags = m_Tag
End Property

Public Property Let Tags(cTag As String)
   m_Tag = cTag
End Property

