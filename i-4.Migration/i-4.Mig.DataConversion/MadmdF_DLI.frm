VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MadmdF_DLI 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   " DBD details"
   ClientHeight    =   5685
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   7560
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5685
   ScaleWidth      =   7560
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Delete"
      Height          =   405
      Left            =   4710
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   5190
      Width           =   1845
   End
   Begin VB.CommandButton cmdInsert 
      Caption         =   "Insert "
      Height          =   405
      Left            =   2790
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   5190
      Width           =   1845
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "Update"
      Height          =   405
      Left            =   870
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   5190
      Width           =   1845
   End
   Begin MSComctlLib.ListView lswTab 
      Height          =   5055
      Left            =   30
      TabIndex        =   3
      Top             =   60
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   8916
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   12582912
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
End
Attribute VB_Name = "MadmdF_DLI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public txtNum As Long
Dim TabName As String
Dim NomiCampi() As String
Dim ValoriCampi() As String
Public update_insert As Integer

Private Sub cmdCancel_Click()
'stop
On Error GoTo ErrorHandler
Dim varBeforUpdate As Variant
Dim i As Long

  If MsgBox("Do you confirm the cancellation of the selected record", vbOKCancel + vbExclamation, "Reviser") = vbOK Then

        ReDim FieldValues(lswTab.ColumnHeaders.count - 1)
        If lswTab.ListItems.count > 0 Then
            FieldValues(0) = lswTab.SelectedItem
            For i = 1 To lswTab.ColumnHeaders.count - 1
                FieldValues(i) = lswTab.SelectedItem.SubItems(i)
            Next
        End If

'******************
        For i = 0 To UBound(FieldNames) - 1
            If FieldNames(i).type = "3" Then
                varBeforUpdate = varBeforUpdate & FieldNames(i).Name & "=" & FieldValues(i) & " and "
            ElseIf FieldNames(i).type = "202" Then
                varBeforUpdate = varBeforUpdate & FieldNames(i).Name & checkNull(FieldValues(i)) & " and "
            ElseIf FieldNames(i).type = "11" Then
               varBeforUpdate = varBeforUpdate & FieldNames(i).Name & "= " & IIf(FieldValues(i), "True", "False") & " and "
            End If
            
        Next
            If FieldNames(i).type = "3" Then
                varBeforUpdate = varBeforUpdate & FieldNames(UBound(FieldNames)).Name & "=" & FieldValues(UBound(FieldNames))
            ElseIf FieldNames(i).type = "202" Then
                varBeforUpdate = varBeforUpdate & FieldNames(UBound(FieldNames)).Name & checkNull(FieldValues(UBound(FieldNames)))
            ElseIf FieldNames(i).type = "11" Then
               varBeforUpdate = varBeforUpdate & FieldNames(UBound(FieldNames)).Name & "=" & IIf(FieldValues(UBound(FieldNames)), "True", "False")
            End If
            
        m_fun.FnConnection.Execute ("DELETE FROM " & NomeTab & " WHERE  " & varBeforUpdate & " ;")

        displayTab True
  End If
        Exit Sub
ErrorHandler:
      MsgBox ERR.Number & " " & ERR.Description
End Sub

Public Function checkNull(Campo As String) As String
If UCase(Campo) = "NULL" Then
  checkNull = " is null"
Else
  checkNull = "= '" & Campo & "'"
End If
End Function
Private Sub cmdInsert_Click()
'stop
Dim i As Long
        ReDim FieldValues(lswTab.ColumnHeaders.count - 1)
        If lswTab.ListItems.count > 0 Then
            FieldValues(0) = ""
            For i = 1 To lswTab.ColumnHeaders.count - 1
                FieldValues(i) = ""
            Next
        End If
        
        MadmdF_UpdateTab.Start 1
End Sub

Private Sub cmdUpdate_Click()
'stop
Dim i As Long

        'FieldValues(i)  = inserire i valori del record selezionati
        ReDim FieldValues(lswTab.ColumnHeaders.count - 1)
        If lswTab.ListItems.count > 0 Then
            FieldValues(0) = lswTab.SelectedItem
            For i = 1 To lswTab.ColumnHeaders.count - 1
                FieldValues(i) = lswTab.SelectedItem.SubItems(i)
            Next
        End If
        
        MadmdF_UpdateTab.Start 0

End Sub



Private Sub Form_Load()
displayTab False
End Sub

Public Sub displayTab(boolRefresh As Boolean)
  Dim rs As Recordset
  Dim listx As ListItem
  Dim i As Long
    
  Set rs = m_fun.Open_Recordset("Select * from PsDli_DBD")
  If Not rs Is Nothing Then
    For i = 0 To rs.fields.count - 1
      If Not boolRefresh Then
        lswTab.ColumnHeaders.Add , , rs.fields(i).Name
        lswTab.ColumnHeaders(i + 1).Width = lswTab.Width / rs.fields.count * 0.99
      End If
      lswTab.ListItems.Clear
      ReDim Preserve FieldNames(rs.fields.count - 1)
      FieldNames(i).Name = rs.fields(i).Name
      FieldNames(i).type = rs.fields(i).type
      FieldNames(i).len = rs.fields(i).DefinedSize
      'SQ - run-time error se non ho il default impostato!
      'Proviamo almeno una volta le modifiche...
      If rs.RecordCount > 0 Then
        If Not IsNull(rs.fields(i).OriginalValue) Then
          FieldNames(i).default = rs.fields(i).OriginalValue
        End If
      End If
    Next
    Do Until rs.EOF
      For i = 0 To rs.fields.count - 1
          If i = 0 Then
            Set listx = lswTab.ListItems.Add(, , TN(rs.fields(i)))
          Else
              If IsNull(rs.fields(i)) Then
                  listx.SubItems(i) = "null"
              Else
                If rs.fields(i).type = 11 Then
                  If rs.fields(i) Then
                    listx.SubItems(i) = "True"
                  Else
                    listx.SubItems(i) = "False"
                  End If
                Else
                  listx.SubItems(i) = rs.fields(i)
                End If
                  
              End If
          End If
      Next
      rs.MoveNext
    Loop
  End If
'        frmTab.Caption = " " & lswTables.SelectedItem
   NomeTab = "PsDli_DBD"
  'Me.Show
End Sub

