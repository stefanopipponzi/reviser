Attribute VB_Name = "MadmdM_Prj"
Option Explicit

'Dll Puntate
Global NormNomeDB As String
Public m_fun As New MaFndC_Funzioni
Public m_HeightBefore As Long
'SQ farne solo una
Global Const SYSTEM_DIR = "i-4.Mig.cfg"
Global Const FILE_INI = "\" & SYSTEM_DIR & "\i-4.Migration.ini"
Global PrefDb As String
Global dbType As String
Global Stgroup As String
'variabile per tenere traccia della colonna selezionata sulla lista di filtro selezione
Global Filter_List_Column As Long
'variabile come la precedente, ma riguarda la lista totale degli oggetti
Global ListObjCol As Long

Public Function getId(column As String, table As String) As Long
  Dim r As Recordset
  On Error GoTo errdb
  
  Set r = m_fun.Open_Recordset("Select max(" & column & ") From " & table)
  getId = IIf(IsNull(r(0)), 1, r(0) + 1)
  r.Close
  Exit Function
errdb:
  getId = -1
End Function
'''''''''''''''''''''''''''''''''''
'PULISCO in cascata:
' - (db e tbs)
' - DMDB2_Tabelle
' - DMDB2_Alias
' - DMDB2_Colonne
' - DMDB2_Index
' - DMDB2_IdxCol
'''''''''''''''''''''''''''''''''''
' Mauro 02/08/2007 : Unificate le routine DB2-ORACLE
'Public Sub deleteDBDTablesDB2(idDbd As Long)
Public Sub deleteDBDTables(idDbd As Long)
  Dim wTb As Recordset
  Dim idDB_DBD As Long
  
  'Decidere la gestione del DB eventualmente rimasto "vuoto"
  '...
  'Decidere la gestione del TableSpace eventualmente rimasto "vuoto"
  '...
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Per non rivoluzionare tutto, teniamo la _DB cosi', ma con altro significato!
  ' In realta' deve essere una ...DB2/ORACLE_DBD
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Set wTb = m_fun.Open_Recordset("select * from " & PrefDb & "_DB where idDB = " & idDbd)
  'DM*_DB.IdDB e' la chiave di accesso per la cascata di tutti gli altri oggetti
  If wTb.RecordCount Then
    idDB_DBD = wTb!idDB
    Set wTb = m_fun.Open_Recordset("select * from " & PrefDb & "_Tabelle where iddb = " & idDB_DBD)
    While Not wTb.EOF
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Alias Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Colonne Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Index Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_IdxCol Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Trigger Where idtable = " & wTb!IdTable
      DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Check_Constraint Where idtable = " & wTb!IdTable
      wTb.Delete
      wTb.MoveNext
    Wend
    wTb.Close
    DataMan.DmConnection.Execute "DELETE * from " & PrefDb & "_DB where idDB = " & idDbd
    DataMan.DmConnection.Execute "DELETE * from " & PrefDb & "_Tablespaces where idDB = " & idDbd
  End If
End Sub

' Mauro 01/08/2007 : Unificate le routine DB2 e ORACLE
''Public Function getTableName_tabellaDB2(IdTable As Long) As String
Public Function getTableName_tabella(IdTable As Long, TipoDB As String) As String
  Dim r As Recordset
  On Error GoTo errordB
  
  If TipoDB = "DB2" Then
    Set r = m_fun.Open_Recordset("SELECT nome from DMDB2_Tabelle where idTable=" & IdTable)
  ElseIf TipoDB = "ORACLE" Then
    Set r = m_fun.Open_Recordset("SELECT nome from DMORC_Tabelle where idTable=" & IdTable)
  End If
  
  If r.RecordCount Then
    'getTableName_tabellaDB2 = r(0)
    getTableName_tabella = r!Nome
  Else
    getTableName_tabella = ""
  End If
  r.Close
  
  Exit Function
errordB:
  'gestire...
  'MsgBox ERR.Description
  getTableName_tabella = ""
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' - Crea il nome del tablespace
' - Aggiorna la tabella dei tablespace
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Mauro 01/08/2007 : Unificate le routine DB2-ORACLE
'Function getIdTableSpcDB2(table As TabDb) As Long
Function getIdTableSpc(table As TabDb, Optional k As Integer) As Long
  Dim ts As Recordset, ts1 As Recordset
  Dim tbsname As String

  ''''''''''''''''''''''''
  'TBS CRITERIA
  'SQ 8-06-06
  ''''''''''''''''''''''''
  Dim env As Collection
  tbsname = LeggiParam("IMSDB_TBSNAME")
  If Len(tbsname) Then
    Set env = New Collection  'resetto;
    env.Add table
    env.Add k
    tbsname = getEnvironmentParam(tbsname, "TabelleDM", env, TipoDB)
  End If
  If Len(tbsname) = 0 Then
    tbsname = table.Nome
  End If
  tbsname = Replace(tbsname, "-", "_")
  Set ts = m_fun.Open_Recordset("select * from " & PrefDb & "_TableSpaces where nome = '" & tbsname & "'")
  If ts.RecordCount Then
    'table space gia' presente:
    getIdTableSpc = ts!IdTableSpc
  Else
    ts.AddNew
    ts!Nome = tbsname
    'Nuovo Id:
    Set ts1 = m_fun.Open_Recordset("Select max(IdTableSpc) From " & PrefDb & "_TableSpaces")
    ts!IdTableSpc = IIf(IsNull(ts1(0)), 1, ts1(0) + 1)
    getIdTableSpc = ts!IdTableSpc
    ts!StorageGroup = Stgroup
    'ts!PRIQTY = "<priqty>"
    'ts!SECQTY = "<secqty>"
    'ts!FREEPAGE = "<freepage>"
    'ts!PCTFREE = "<pctfree>"
    ts!BufferPool = "<bufferPool>"
    ts!Compress = False
    'CCSID?
    ts1.Close
        
    ts.Update
    ts.Close
  End If
  k = k + 1
End Function

'ritorna la parola successiva (space unico separatore)
'Considera come delimitatore di "token" le parentesi e gli apici singoli...
'Effetto Collaterale: si mangia ...
Function nextToken(inputString As String)
  Dim level, i, j As Integer
  Dim currentChar As String
    
  If (Len(inputString) > 0) Then
    currentChar = Left(inputString, 1)
    Select Case currentChar
      Case "("
        i = 2
        level = 1
        nextToken = "("
        While level > 0
          currentChar = Mid(inputString, i, 1)
          Select Case currentChar
            Case "("
              level = level + 1
              nextToken = nextToken & currentChar
            Case ")"
              level = level - 1
              nextToken = nextToken & currentChar
            Case Else
              nextToken = nextToken & currentChar
          End Select
          i = i + 1
        Wend
        inputString = Trim(Mid(inputString, i))
      Case "'"
        inputString = Mid(inputString, 2)
        nextToken = Left(inputString, InStr(inputString, "'") - 1)
        inputString = Mid(inputString, InStr(inputString, "'") + 1)
      Case Else
        i = InStr(inputString, " ")
        j = InStr(inputString, "(")
        If ((i > 0 And j > 0 And j < i) Or (i = 0 And j > 0)) Then i = j
        If (i > 0) Then
          nextToken = Left(inputString, i - 1)
          inputString = Trim(Mid(inputString, i))
        Else
          nextToken = inputString
          inputString = ""
        End If
    End Select
  End If
End Function

Public Function getSegProgr_tabelle(rs As Recordset) As String
  Dim r As Recordset
  On Error GoTo errordB
  
  If InStr(rs.Source, " MgDLI_Segmenti ") Then
    Set r = m_fun.Open_Recordset("SELECT count(*) from MgDLI_Segmenti where " & _
                                 "idOggetto = " & rs!IdOggetto & " AND idSegmento <= " & rs!IdSegmento)
  Else
    Set r = m_fun.Open_Recordset("SELECT count(*) from PsDLI_Segmenti where " & _
                                 "idOggetto = " & rs!IdOggetto & " AND idSegmento <= " & rs!IdSegmento)
  End If
  If r.RecordCount Then
    getSegProgr_tabelle = Format(r(0), "000")
  Else
    getSegProgr_tabelle = "000"
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getSegProgr_tabelle = "000"
End Function
'silvia 1-4-2009
Public Function getAliasSegm(rs As Recordset) As String
  Dim r As Recordset
  On Error GoTo errordB
  
  Set r = m_fun.Open_Recordset("SELECT * from MgData_Area where idSegmento = " & rs!IdSegmento)
  
  If r.RecordCount Then
    getAliasSegm = IIf(IsNull(r!Alias_Segm), "", r!Alias_Segm)
  Else
    getAliasSegm = ""
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getAliasSegm = ""
End Function

Public Function getRouteName_tabelle(idDbd As Long) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("SELECT Route_name from psDli_DBD as a, bs_oggetti as b where " & _
                               "b.nome = a.DBD_name AND b.idOggetto = " & idDbd)
  If r.RecordCount Then
    getRouteName_tabelle = r(0) & ""
  Else
    getRouteName_tabelle = ""
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getRouteName_tabelle = ""
End Function

Public Function getTableName_tabelle(idDbd As Long) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("SELECT Table_name from psDli_DBD as a, bs_oggetti as b where " & _
                               "b.nome = a.DBD_name AND b.idOggetto = " & idDbd)
  If r.RecordCount Then
    getTableName_tabelle = r(0)
  Else
    getTableName_tabelle = ""
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getTableName_tabelle = ""
End Function

Public Function getDbd_tabelle(idDbd As Long) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("SELECT nome from bs_oggetti where idOggetto = " & idDbd)
  If r.RecordCount Then
    getDbd_tabelle = r(0)
  Else
    getDbd_tabelle = ""
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getDbd_tabelle = ""
End Function

Public Function getNomeArea_tabelle(IdSegmento As Long) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("select NomeCmp from MgRel_SegAree where Stridarea = 1 and idSegmento = " & IdSegmento)
  If r.RecordCount Then
    getNomeArea_tabelle = Trim(r(0))  'ci sono bianchi in fondo! toglierli dalle insert...
  Else
    getNomeArea_tabelle = ""
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getNomeArea_tabelle = ""
End Function

Public Function getRouteName_dbd(nomeDbd As String) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("SELECT Route_name from psDli_DBD where DBD_name='" & nomeDbd & "'")
  If r.RecordCount Then
    getRouteName_dbd = r(0) & ""
  Else
    getRouteName_dbd = ""
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getRouteName_dbd = ""
End Function

Public Function getTableName_dbd(nomeDbd As String) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("SELECT Table_name from psDli_DBD where DBD_name = '" & nomeDbd & "'")
  If r.RecordCount Then
    getTableName_dbd = r(0)
  Else
    getTableName_dbd = ""
  End If
  r.Close
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getTableName_dbd = ""
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Unica DB2/ORACLE
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getDclgenLen(IdTable As Long) As Integer
  Dim TbColonne As Recordset
   
  On Error GoTo errdb
  getDclgenLen = 0
  Set TbColonne = m_fun.Open_Recordset("select nome, Tipo, Lunghezza, Decimali,[null] from " & PrefDb & "_Colonne where idTable = " & IdTable & " order by ordinale") 'nome + order by solo per debug...
  Do Until TbColonne.EOF
    'Parametrizzare il log: � molto lento!
    ' m_fun.WriteLog "Debug: colonna=" & TbColonne!Nome & " - totLen=" & getDclgenLen
    Select Case TbColonne!Tipo
      Case "CHAR"
        getDclgenLen = getDclgenLen + TbColonne!lunghezza
      Case "TIME"
        getDclgenLen = getDclgenLen + 8
      Case "DATE"
        getDclgenLen = getDclgenLen + 10
      Case "TIMESTAMP"
        getDclgenLen = getDclgenLen + 26
      Case "INTEGER"
        getDclgenLen = getDclgenLen + 4
      Case "LONG"
        getDclgenLen = getDclgenLen + 8
      Case "DECIMAL"
        getDclgenLen = getDclgenLen + CInt((TbColonne!lunghezza + 1.1) / 2)
      Case "SMALLINT"
        getDclgenLen = getDclgenLen + 2
      Case "VARCHAR", "VARCHAR2"
        getDclgenLen = getDclgenLen + 2 + TbColonne!lunghezza
      Case "NUMBER" 'ORACLE
        getDclgenLen = getDclgenLen + CInt((TbColonne!lunghezza + 1.1) / 2)
      Case Else
        m_fun.WriteLog "##getDclgenLen: type unknown=" & TbColonne!Tipo & " - idTable: " & IdTable
    End Select
    'X(1) in piu' per "indicatore di null"
    If TbColonne!Null Then getDclgenLen = getDclgenLen + 1
           
    TbColonne.MoveNext
  Loop
  'If DUE BYTE IN TESTA CON X'0019': getDclgenLen + 2
  TbColonne.Close
  Exit Function
errdb:
  MsgBox ERR.Description
  getDclgenLen = -1
End Function
'''''''''''''''''''''''''''''''''''''''''''
'Nome colonna: KY_AG0_I2
'Nome livello 01 copy DL1...: W-AG0-KEYI2
'''''''''''''''''''''''''''''''''''''''''''
Public Function getXdfieldMove_BPER(Nome As String) As String
  Dim rsCmpIn As Recordset, rsCmp As Recordset
  Dim nomeArea As String
  
  On Error GoTo errdb
  
  nomeArea = Replace(Replace(Nome, "KY_", "W-"), "_I", "-KEYI")
  '4-07-05: MAGAGNA NOMI "FARLOCCHI", SENZA "_I" finale:
  nomeArea = Replace(nomeArea, "_", "-KEYI")
  
  'select Nome from Psdata_cmp where IdOggetto & IdArea  IN (SELECT distinct IdOggetto & IdArea from psData_cmp where Nome='W-AG0-KEY');
  'IN ACCESS NON POSSO FARE LA "IN" SU PIU' CAMPI e l'append ci mette una vita!
  Set rsCmpIn = m_fun.Open_Recordset("select distinct IdOggetto,IdArea from PsData_Cmp where nome='" & nomeArea & "'")
  If rsCmpIn.RecordCount Then
    'posso averne piu' di uno: tengo l'ultimo
    rsCmpIn.MoveLast
    Set rsCmp = m_fun.Open_Recordset("select nome from PsData_Cmp where IdOggetto=" & rsCmpIn!IdOggetto & " AND IdArea=" & rsCmpIn!idArea & " AND livello > 1")
    rsCmpIn.Close
    While Not rsCmp.EOF
      'attenzione se ci sono piu' di 9 indici...
      'NOME LIVELLO 01: W-I<N>-AG0-SOCSUC...
      getXdfieldMove_BPER = getXdfieldMove_BPER & "MOVE " & Mid(rsCmp!Nome, 6) & " TO " & rsCmp!Nome & vbCrLf
      rsCmp.MoveNext
    Wend
    'stefano: attenzione: tolto il carriage-return; decide il template....
    getXdfieldMove_BPER = getXdfieldMove_BPER & "MOVE " & nomeArea & " TO <ALIAS>-" & Replace(Nome, "_", "-")
    rsCmp.Close
  Else
    getXdfieldMove_BPER = ""
  End If
  Exit Function
errdb:
  MsgBox ERR.Description
  'resume
  getXdfieldMove_BPER = ""
End Function

Public Function getKeyMove_BPER(Nome As String, areaRD As String, NomeCol As String, areaRR As String) As String
  Dim rsCmpIn As Recordset, rsCmp As Recordset
  Dim nomeArea As String
  
  On Error GoTo errdb
  
  nomeArea = Replace(Replace(Nome, "KY_", "W-"), "_I", "-KEYI")
  '4-07-05: MAGAGNA NOMI "FARLOCCHI", SENZA "_I" finale:
  nomeArea = Replace(nomeArea, "_", "-KEYI")
  
  'select Nome from Psdata_cmp where IdOggetto & IdArea  IN (SELECT distinct IdOggetto & IdArea from psData_cmp where Nome='W-AG0-KEY');
  'IN ACCESS NON POSSO FARE LA "IN" SU PIU' CAMPI e l'append ci mette una vita!
  Set rsCmpIn = m_fun.Open_Recordset("select distinct IdOggetto,IdArea from PsData_Cmp where nome='" & nomeArea & "'")
  If rsCmpIn.RecordCount Then
    'posso averne piu' di uno: tengo l'ultimo
    rsCmpIn.MoveLast
    Set rsCmp = m_fun.Open_Recordset("select nome from PsData_Cmp where IdOggetto=" & rsCmpIn!IdOggetto & " AND IdArea=" & rsCmpIn!idArea & " AND livello > 1")
    rsCmpIn.Close
    While Not rsCmp.EOF
      'attenzione se ci sono piu' di 9 indici...
      'NOME LIVELLO 01: W-I<N>-AG0-SOCSUC...
      getKeyMove_BPER = getKeyMove_BPER & "MOVE " & rsCmp!Nome & " OF " & areaRD & " TO " & vbCrLf & _
                                          Space(3) & NomeCol & " OF " & areaRR & vbCrLf
      rsCmp.MoveNext
    Wend
    rsCmp.Close
  Else
    getKeyMove_BPER = ""
  End If
  Exit Function
errdb:
  MsgBox ERR.Description
  'resume
  getKeyMove_BPER = ""
End Function

Public Function getCondizione_BPER(RtbFile As RichTextBox, Nome As String) As String
  Dim rtbString As String, extRoutine As String
  
  On Error GoTo loadErr
  
  extRoutine = Replace(Replace(Nome, "KY_", "EXTR-"), "_I", "-KEYI")
  
  'salvo:
  rtbString = RtbFile.text
  RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\extRoutines\" & extRoutine
  getCondizione_BPER = RtbFile.text
  
'ripristino
  RtbFile.text = rtbString
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\extRoutines\" & extRoutine & """ not found."
      getCondizione_BPER = ""
    Case Else
      MsgBox ERR.Description
      'resume
  End Select
End Function

Public Function getCondizione_Redefines(RtbFile As RichTextBox, Condizione As String) As String
  Dim rtbString As String
  
  On Error GoTo loadErr
   
  'Portare controllo fuori
  If Len(Condizione & "") Then
    'salvo:
    rtbString = RtbFile.text
    'rtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\redefines\" & Condizione
    RtbFile.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\redefines\" & Condizione
    getCondizione_Redefines = RtbFile.text
  'ripristino
    RtbFile.text = rtbString
  End If
  Exit Function
loadErr:
  Select Case ERR.Number
    Case 75
      'ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\redefines\" & Condizione & """ not found."
      ERR.Raise 7575, , "Template """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\redefines\" & Condizione & """ not found."
      getCondizione_Redefines = ""
    Case Else
      MsgBox ERR.Description
      'resume
  End Select
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Da generalizzare quando il template per C-COPY sara' rifatto...
' Vedi getListaCampi...
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function getAreaCcopy(tableName As String) As String
  'pezzo di getListaCampi...
  If m_fun.FnNomeDB = "banca.mty" Then
    getAreaCcopy = Mid(tableName, 8, 2) & "N" & Right(tableName, 3)  'ALIAS
  Else
''    getAreaCcopy = "RR-" & Normalizza_Nome(tableName, "RCL")
    If Len(tableName) > 18 Then
      'Tronca a 18
      tableName = Left(tableName, 18)
    End If
    
    '2) Controlla i caratteri (sostituisce con underscore)
'    K = InStr(Nome, "_")
'    If K Then
'      Ch = Mid$(Nome, 1, K - 1) & "-" & Trim(Mid$(Nome, K + 1, 30))
'    Else
'      Ch = Nome
'    End If
    tableName = Replace(tableName, "_", "-")
    
    getAreaCcopy = "RR-" & tableName
  End If
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function isCiclicTag(Tag As String) As String
  isCiclicTag = Right(Tag, 4) = "(i)>"
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' carica rtbFile con il testo del template esplodento eventuali
' TEMPLATE-TAG...
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub cleanTags(RtbFile As RichTextBox)
  Dim regExpr As Object
  Dim matches As Variant, Match As Variant
  
  Set regExpr = CreateObject("VBScript.regExp")
  
  'regExpr.Pattern = "<[^>]*(i)>" 'altrimenti mi accorpa tutte quelle di una riga...
  regExpr.Pattern = "<[^>]*\(i\)>"
  regExpr.Global = True
  
  Set matches = regExpr.Execute(RtbFile.text)
  For Each Match In matches
    changeTag RtbFile, Match & "", ""
  Next
  
  Exit Sub
fileErr:
  MsgBox ERR.Description
End Sub

'''''''''''''''''''''''''''''''''''''''''
' FARE TUTTO STO DISCORSO FATTO BENE...
'''''''''''''''''''''''''''''''''''''''''
Public Function getEnvironmentParam(ByVal parameter As String, envType As String, environment As Collection, Optional TipoDB As String) As String
  Dim token As String
  Dim subTokens() As String
  'intervallo:
  Dim intervallo() As String
  
  On Error GoTo errdb
  
  Select Case envType
    Case "colonne"
      'environment = rsColonne,rsTabelle
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = getEnvironmentParam & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "GROUP-NAME"
              'token = getGroupName_colonne(environment.Item(1)!IdOggetto)
            Case "COLUMN-NAME"
              token = environment.Item(1)!Nome
'alpitour 5/8/2005
            Case "PARENT-COLUMN-NAME"
              token = environment.Item(1)!NomeCmp
            Case "ALIAS"
'              If m_fun.FnNomeDB = "banca.mty" Then
'                token = Left(environment.Item(2)!Nome, 2) & "N" & Mid(environment.Item(2)!Nome, 10)
'              Else
'                'fare...
'              End If
              token = getAlias_tabelle(environment.Item(1)!IdTable)
'stefanopippo: 17/08/2005: e basta!
            Case "TABLE-NAME"
              token = environment.Item(2)!Nome
              'stefano: schifezza + schifezza
            Case Else
              MsgBox "Variable unknown: " & subTokens(0)
          End Select
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    Case "colonneVSM"
      'environment = rsColonne,rsTabelle
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = getEnvironmentParam & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "CMP-NAME"
              token = Replace(environment.Item(1)!Nome, "-", "_")
            Case Else
              MsgBox "Variable unknown: " & subTokens(0)
          End Select
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    Case "chiaviVSM"
      'environment = rsColonne,rsTabelle
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = getEnvironmentParam & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "TABLE-NAME"
              token = environment.Item(1)
            Case "CMP-NAME"
              token = Replace(environment.Item(2), "-", "_")
            Case Else
              MsgBox "Variable unknown: " & subTokens(0)
          End Select
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    Case "CmpName"
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = getEnvironmentParam & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "CMP-NAME"
              token = environment.Item(1)!Nome
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    'SQ 13-11-07
    'Case "Field"
    Case "Index"  'Per: PK, AK, FK
      'environment = GbField/GbXDField  (elemento di tipo " (Field(i)/XDField(i),idTable,progressivo
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = getEnvironmentParam & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "FIELD-NAME"
              token = environment.Item(1).Nome
            Case "TABLE-NAME"
              token = getTableName_tabella(environment.Item(2), TipoDB)
            Case "IDX-NUMBER" 'n.b.: formattato a 2 char
              token = Format(environment.Item(3), "00")
            Case "CHLD-NUMBER"
              token = Format(environment.Item(3), "0")
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    Case "TabelleDM"
      'environment = Tabelle(i) (elemento di tipo "TabDb")
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          If token = "00" Then
            token = Format(environment.Item(2), "00")
          End If
          getEnvironmentParam = getEnvironmentParam & token
        Else
            subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
            Select Case subTokens(0)
            Case "SEGM-NAME"
              token = environment.Item(1).Nome
            Case "TABLE-NAME"
              token = environment.Item(1).NomeTb
            Case "DBD-NAME"
              token = environment.Item(1).NomeDB
            'SILVIA : GESTIONE NOMI TABELLE CHILD
            Case "CHLD-NUMBER"
              token = Format(environment.Item(2), "0")
            'SILVIA 20-03-2009 aggiungo Alias_Segm
            Case "ALIAS-SEGM"
              token = environment.Item(1).AliasSegm
            Case Else
              '...
            End Select
          
            If UBound(subTokens) Then
              intervallo = Split(subTokens(1), "-")
              'integrare i casi senza entrambi i valori (es.: #-6#)
              If Len(intervallo(0)) = 0 Then
               'ULTIMI N CHAR (es.: #-N#)
                token = Right(token, CInt(intervallo(1)))
              Else
                If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
                End If
                token = Mid(token, CInt(intervallo(0)))
              End If
            End If
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    Case "tabelle"
      'environment = rsTabelle,rsSegmenti
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = getEnvironmentParam & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "ROUTE-NAME", "ROUT-NAME" 'SQ in giro ci sono "ROUTE": sbagliati!
              token = getRouteName_tabelle(environment.Item(2)!IdOggetto)
            Case "SEGM-PROGR"
              token = getSegProgr_tabelle(environment.Item(2))
            'SILVIA 1-4-2009
            Case "ALIAS-SEGM"
              token = getAliasSegm(environment.Item(2))
            Case "SEGM-NAME"
              token = environment.Item(2)!Nome
            Case "TABLE-NAME"
              'SQ - 6-06-05
              'token = getTableName_tabelle(environment.Item(2)!IdOggetto)
              'VIRGILIO 26/2/2007
              'token = environment.Item(1)!Nome
              token = Replace(environment.Item(1)!Nome, "_", "")
            'stefano: schifezza + schifezza
            Case "DBD-NAME"
              token = getDbd_tabelle(environment.Item(2)!IdOggetto)
            Case "ALIAS"
              'If m_fun.FnNomeDB = "banca.mty" Then
'                token = Left(environment.Item(2)!Nome, 2) & "N" & Mid(environment.Item(1)!Nome, 10)
              'Else
                'fare...
              'End If
              token = getAlias_tabelle(environment.Item(1)!IdTable)
            Case "NOME-AREA"  'Area Cobol associata al segmento:
              token = getNomeArea_tabelle(environment.Item(2)!IdOggetto)
            'TILVIA
            Case "CHLD-NUMBER"
              token = Format(environment.Item(2), "0")
            Case "DB-NAME"
              token = environment.Item(1)
            
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            token = Replace(token, "_", "")
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    Case "tabelleVSM"
      'environment = rsTabelle,rsSegmenti
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = getEnvironmentParam & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "VSAM-ACRONYM"
              token = environment.Item(1)
            Case "RECTYPE-ACRONYM"
              token = environment.Item(2)!Acronimo
            Case "RECTYPE-NAME"
              token = Replace(environment.Item(3), "-", "_")
            Case "ROUTE-NAME", "ROUT-NAME" 'SQ in giro ci sono "ROUTE": sbagliati!
              token = getRouteName_tabelle(environment.Item(2)!IdOggetto)
            Case "SEGM-NAME"
              token = environment.Item(2)!Acronimo
            Case "IDXNUMBER"
              token = environment.Item(3)
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    Case "dbVSM"
      'environment = rsTabelle,rsSegmenti 'SQ non � vero!
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = getEnvironmentParam & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "DB-NAME"
              token = environment.Item(1)
            Case "VSAM-ACRONYM" 'SQ Sistemare!
              token = environment.Item(1)
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    Case "tabella"
      'environment = idTable
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = getEnvironmentParam & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "TABLE-NAME"
              
''              If TipoDB = "DB2" Then
              token = getTableName_tabella(environment.Item(1), TipoDB)
''              ElseIf TipoDB = "ORACLE" Then
''                token = getTableName_tabellaORACLE(environment.Item(1))
''              End If
            Case "IDX-NUMBER"
              token = Format(environment.Item(2), "0")
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    Case "dbd"
      'environment = nome DBD
      While Len(parameter)
        token = nextToken(parameter)
        If Left(token, 1) <> "(" Then
          getEnvironmentParam = getEnvironmentParam & token
        Else
          subTokens = Split(Mid(token, 2, Len(token) - 2), "#")
          Select Case subTokens(0)
            Case "ROUTE-NAME", "ROUT-NAME"
              token = getRouteName_dbd(environment.Item(1))
            Case "TABLE-NAME"
              token = getTableName_dbd(environment.Item(1))
            Case "DBD-NAME"
              token = getDbdByNomeDB2(environment.Item(1))
            Case "DB-NAME"
              token = environment.Item(1)
            'Case "SEGM-PROGR"
            '  token = getSegProgr_tabelle(environment.Item(2))
            Case Else
              '...
          End Select
          If UBound(subTokens) Then
            intervallo = Split(subTokens(1), "-")
            'integrare i casi senza entrambi i valori (es.: #-6#)
            If Len(intervallo(0)) = 0 Then
              'ULTIMI N CHAR (es.: #-N#)
              token = Right(token, CInt(intervallo(1)))
            Else
              If Len(intervallo(1)) Then
                'INTERVALLO ORTODOSSO (es.: #3-4#)
                token = Left(token, CInt(intervallo(1)))
              'Else
                'DAL N-esimo CHAR in poi (es.: #N-#)
              End If
              token = Mid(token, CInt(intervallo(0)))
            End If
          End If
          getEnvironmentParam = getEnvironmentParam & token
        End If
      Wend
    Case "else"
      '...
  End Select
  Exit Function
errdb:
  If ERR.Number = 13 Then
    'sbagliato l'intervallo numerico
    MsgBox "Wrong definition for " & parameter, vbExclamation
  Else
    MsgBox ERR.Description
    'Resume
  End If
  getEnvironmentParam = ""
End Function

Public Function getDbdByNomeDB2(NomeDB2 As String) As String
  Dim rsDbd As Recordset
  
  getDbdByNomeDB2 = ""
  Set rsDbd = m_fun.Open_Recordset("select a.nome from bs_oggetti as a, dmdb2_db as b where b.nome = '" & NomeDB2 & "' and b.idDBOr = a.idoggetto")
  If rsDbd.RecordCount Then
    getDbdByNomeDB2 = rsDbd!Nome
  End If
  rsDbd.Close
End Function

Public Function getXdfieldStructure_BPER(Nome As String) As String
  Dim rsCmpIn As Recordset, rsCmp As Recordset
  Dim nomeArea As String, statement As String, Decimali As String
  
  On Error GoTo errdb
  
  nomeArea = Replace(Replace(Nome, "KY_", "W-"), "_I", "-KEYI")
  '4-07-05: MAGAGNA NOMI "FARLOCCHI", SENZA "_I" finale:
  nomeArea = Replace(nomeArea, "_", "-KEYI")
  
  'IN ACCESS NON POSSO FARE LA "IN" SU PIU' CAMPI e l'append ci mette una vita!
  Set rsCmpIn = m_fun.Open_Recordset("select distinct IdOggetto,IdArea from PsData_Cmp where nome='" & nomeArea & "'")
  If rsCmpIn.RecordCount Then
    'posso averne piu' di uno: tengo l'ultimo
    rsCmpIn.MoveLast
    Set rsCmp = m_fun.Open_Recordset("select c.idOggetto,c.Nome,c.lunghezza,c.segno,c.decimali,c.tipo,c.byte,c.[Usage] FROM PsData_Cmp as c where IdOggetto=" & rsCmpIn!IdOggetto & " AND IdArea=" & rsCmpIn!idArea & " AND livello > 1")
    rsCmpIn.Close
    While Not rsCmp.EOF
      'attenzione se ci sono piu' di 9 indici...
      'NOME LIVELLO 01: W-I<N>-AG0-SOCSUC...
      'getXdfieldStructure_BPER = getXdfieldStructure_BPER & "MOVE " & Mid(rsCmp!Nome, 6) & " TO " & rsCmp!Nome & vbCrLf
      ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      If rsCmp(2) = 0 Then
        statement = statement & Space(3) & "10 " & rsCmp(1) & "    PIC X(" & rsCmp(6) & ")"
      Else
        If rsCmp(3) = "S" Then
           statement = statement & Space(3) & "10 " & rsCmp(1) & "    PIC S" & rsCmp!Tipo & "(" & rsCmp(2) & ")"
        Else
           statement = statement & Space(3) & "10 " & rsCmp(1) & "   PIC " & rsCmp!Tipo & "(" & rsCmp(2) & ")"
        End If
        If rsCmp(4) > 0 Then
          Decimali = "V9(" & rsCmp(4) & ")"
        Else
          Decimali = ""
        End If
        Select Case rsCmp(7)
           Case "BNR"
              statement = statement & Decimali & " COMP"
           Case "PCK"
              statement = statement & Decimali & " COMP-3"
           Case "ZND"
            If rsCmp(5) = "9" Then statement = statement & Decimali
        End Select
      End If
      statement = statement & "." & vbCrLf
      rsCmp.MoveNext
    Wend
    'getXdfieldStructure_BPER = getXdfieldStructure_BPER & "MOVE " & nomeArea & " TO <ALIAS>-" & Replace(Nome, "_", "-")
    getXdfieldStructure_BPER = statement
    rsCmp.Close
  Else
    getXdfieldStructure_BPER = ""
  End If
  Exit Function
errdb:
  MsgBox ERR.Description
  'resume
  getXdfieldStructure_BPER = ""
End Function
'SQ: 25-08
'Ritorna IND-NULL1 = -1 AND IND-NULL2 = -1 ...
Function getCheckNullBrothers_BPER(columns As Variant) As String
  Dim i As Integer
  
  getCheckNullBrothers_BPER = columns(0) & "-N = -1 AND"
  For i = 1 To UBound(columns)
    getCheckNullBrothers_BPER = getCheckNullBrothers_BPER & vbCrLf & columns(i) & "-N = -1 AND"
  Next
  'Ho un "AND" in coda...
  getCheckNullBrothers_BPER = Left(getCheckNullBrothers_BPER, Len(getCheckNullBrothers_BPER) - 3)
End Function
'SQ: 25-08
Function getCheckSpaceBrothers_BPER(columns As Variant) As String
  Dim i As Integer
  
  getCheckSpaceBrothers_BPER = "  = " & columns(0)
  For i = 1 To UBound(columns)
    getCheckSpaceBrothers_BPER = getCheckSpaceBrothers_BPER & vbCrLf & "AND " & columns(i)
  Next
End Function
'SQ: 25-08
Function getMoveSpaceBrothers_BPER(columns As Variant) As String
  Dim i As Integer
  
  getMoveSpaceBrothers_BPER = "MOVE SPACES TO " & columns(0)
  For i = 1 To UBound(columns)
    getMoveSpaceBrothers_BPER = getMoveSpaceBrothers_BPER & vbCrLf & "MOVE SPACES TO " & columns(i)
  Next
End Function

'SQ: 25-08
''''''''''''''''''''''''''''''''''''''''''''''''''''
' Utile per tutti, ma parametrizzare nome colonna...
''''''''''''''''''''''''''''''''''''''''''''''''''''
Function getBrotherColumns_BPER(rsCmp As Recordset, IdTable As String, ordinaleGruppo As Integer) As String()
  Dim rsColonne As Recordset
  Dim ordinalePrecedente As String
  Dim results() As String
  
  On Error GoTo errdb
  
  Set rsColonne = m_fun.Open_Recordset("select a.Nome,b.ordinale from dmdb2_colonne as a,MgData_Cmp as b where a.IdCmp=b.IdCmp AND a.idTable=" & IdTable & " AND b.idSegmento=" & rsCmp!IdSegmento & " AND b.livello = " & rsCmp!livello & " AND b.ordinale > " & ordinaleGruppo & " AND b.lunghezza > 0 order by b.Ordinale")
  'probabilmente non serve spezzare il ciclo... adesso non importa!
  
  If rsColonne.RecordCount Then
    ReDim results(0)
    results(0) = "<ALIAS>-" & Replace(rsColonne(0), "_", "-")
    ordinalePrecedente = rsColonne(1)
    rsColonne.MoveNext
    Do While Not rsColonne.EOF
      If rsColonne(1) = ordinalePrecedente + 1 Then 'successivi: devo evitare figli intrusi dello stesso livello!
        ReDim Preserve results(UBound(results) + 1)
        results(UBound(results)) = "<ALIAS>-" & Replace(rsColonne(0), "_", "-")
        ordinalePrecedente = rsColonne(1)
      Else
        Exit Do
      End If
      rsColonne.MoveNext
    Loop
    getBrotherColumns_BPER = results
  End If
  rsColonne.Close
  Exit Function
errdb:
  MsgBox ERR.Description
  'resume
  getBrotherColumns_BPER = Null
End Function
'SQ: 28-08
Function getSuffissoStorico_BPER(dbdName As String)
  Dim r As Recordset
  On Error GoTo errordB
  
  Set r = m_fun.Open_Recordset("SELECT out.DBD_Name,out.Table_Name from PsDLi_DBD as b,PsDli_DBD as out where b.DBD_Name='" & dbdName & "' AND b.Route_Name=out.Route_Name")
  
  If r.RecordCount > 1 Then
    Do While Not r.EOF
      'SQ 29-08 (il principale non ha suffisso)
      If r(0) = dbdName And r(1) > 1 Then
        getSuffissoStorico_BPER = r(1)
      End If
      r.MoveNext
    Loop
  End If
  r.Close
  
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getSuffissoStorico_BPER = ""

End Function
'SQ: 28-08
Function getDbdStorici_BPER(DBD As String) As String
  Dim r As Recordset
  
  On Error GoTo errordB
  
  Set r = m_fun.Open_Recordset("SELECT c.idDB from PsDLi_DBD as b,PsDli_DBD as out,bs_oggetti as a,dmdb2_db as c where b.DBD_Name='" & DBD & "' AND b.Route_Name=out.Route_Name AND a.nome=out.dbd_name AND a.IdOggetto=c.IdDBOr order by c.idDB")
  Do While Not r.EOF
    getDbdStorici_BPER = getDbdStorici_BPER & r(0) & ","
    r.MoveNext
  Loop
  r.Close
  getDbdStorici_BPER = Left(getDbdStorici_BPER, Len(getDbdStorici_BPER) - 1)
  
  Exit Function
errordB:
  'gestire...
  MsgBox ERR.Description
  getDbdStorici_BPER = ""

End Function

Public Function Char_Is_Valid(Char As String) As Boolean
  Select Case Trim(UCase(Char))
    Case "\", "|", "!", "�", "$", "%", "&", "/", "(", ")", "=", "?", "^", "'", "�"
      Char_Is_Valid = False
    Case "�", "�", "[", "+", "*", "]", "�", "�", "@", "�", "�", "#", "�", "�"
      Char_Is_Valid = False
    'Case ",", ";", ".", ":", "-", "<", ">", "{", "}", "~"
    '  Char_Is_Valid = False
    Case "-"
      Char_Is_Valid = True
    Case ",", ";", ".", ":", "<", ">", "{", "}", "~"
      Char_Is_Valid = False
    Case "A", "B", "C", "D", "E", "F", "G", "H", "I", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "Z", "X", "Y", "W", "J", "K"
      Char_Is_Valid = True
    Case "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"
      Char_Is_Valid = True
    Case Else
      Char_Is_Valid = False
  End Select
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Cardinalit� del campo di occurs (dalla MgData_Cmp)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'SILVIA 1-4-2008 PESCE D'APRILE come fa a funzionare con il solo idcmp ? aggiungo anche l'idsegmento
''Public Function getOccursLen(IdCmp As Long) As Integer
Public Function getOccursLen(IdCmp As Long, IdSegmento As Long) As Integer
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("Select occurs from MgData_cmp where " & _
                                "idCmp = " & IdCmp & " and idsegmento = " & IdSegmento)
  ''Set r = m_fun.Open_Recordset("Select occurs from MgData_cmp where idCmp = " & IdCmp)
  If r.RecordCount Then
    getOccursLen = r(0)
  Else
    getOccursLen = 0
  End If

  r.Close
  Exit Function
errordB:
  getOccursLen = 0
End Function

'SILVIA 2-4-2008 : condizione di uscita aggiuntiva della PERFORM VARYING WINDICE... nella routine  SCRIVI-OUT-OCCURS

Public Function getOccurExit(Ordinale As Long, IdSegmento As Long) As String
  Dim r As Recordset
  Dim rtb As RichTextBox
  Dim selval As String
  On Error GoTo errordB
  
  If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\Occurs") = "" Then
    m_fun.MkDir_API DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\Occurs"
  End If
  Set rtb = MadmdF_Start.RtbFile
  Set r = m_fun.Open_Recordset("Select Nome from MgData_cmp where " & _
                                "Ordinale = 1 and idsegmento = " & IdSegmento)
  If r.RecordCount Then
    If r(0) <> "" Then
       'getOccurExit = r(0)
      selval = Trim(r(0))
    Else
      selval = ""
    End If
  Else
    selval = ""
  End If
  r.Close

  Set r = m_fun.Open_Recordset("Select Nome from MgData_cmp where " & _
                                "Ordinale = " & Ordinale & " and idsegmento = " & IdSegmento)
  ''Set r = m_fun.Open_Recordset("Select occurs from MgData_cmp where idCmp = " & IdCmp)
  If r.RecordCount Then
    If r(0) <> "" Then
       'getOccurExit = r(0)
      selval = selval & "_" & Trim(r(0))
    Else
      selval = ""
    End If
  Else
    selval = ""
  End If
  r.Close
  rtb.LoadFile DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\Occurs\" & selval
  getOccurExit = rtb.text
  If getOccurExit = "" Then
    'MsgBox "File """ & DataMan.DmPathDb & "\Input-Prj\Occurs\" & selval & " is empty."
    MadmdF_DLI2RDBMS.LswLog.ListItems.Add , , Time & "### " & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\Occurs\" & selval & " is empty."
    MadmdF_DLI2RDBMS.LswLog.Refresh
    MadmdF_DLI2RDBMS.LswLog.ListItems(MadmdF_DLI2RDBMS.LswLog.ListItems.count).EnsureVisible
  End If
  Exit Function
errordB:
  
  Select Case ERR.Number
    Case 75
      If Dir(DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\Occurs\" & selval & "") = "" Then
        MsgBox "File """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\Occurs\" & selval & " not found."
        getOccurExit = ""
      Else
        m_fun.MkDir_API DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\Occurs"
        getOccurExit = ""
      End If
    Case Else
      MsgBox "File """ & DataMan.DmPathDb & "\Input-Prj\Template\imsdb\standard\Occurs\" & selval & " not found."
      getOccurExit = ""
  End Select
End Function

Public Function getSegmFieldName(IdField As Long) As String
  Dim r As Recordset
  On Error GoTo errordB
  
  If IdField < 0 Then
    Set r = m_fun.Open_Recordset("Select nome from PsDli_XDField where idXDField = " & IdField)
  Else
    Set r = m_fun.Open_Recordset("Select nome from PsDli_Field where idField = " & IdField)
  End If
  If r.RecordCount Then
    getSegmFieldName = r(0)
  Else
    getSegmFieldName = ""
  End If

  r.Close
  Exit Function
errordB:
  getSegmFieldName = ""
End Function
'SP
Public Function getAlias_tabelle(IdTable As Long) As String
  Dim r As Recordset
  On Error GoTo errordB

  Set r = m_fun.Open_Recordset("SELECT Nome from " & PrefDb & "_Alias where idtable = " & IdTable)
  If r.RecordCount Then
    getAlias_tabelle = r!Nome
  Else
    m_fun.WriteLog "errore su ricerca alias: idTable=" & IdTable
    r.Close
    Set r = m_fun.Open_Recordset("select nome from " & PrefDb & "_tabelle where idtable = " & IdTable)
    getAlias_tabelle = r!Nome
    'getAlias_tabelle = ""
  End If

  r.Close
  Exit Function
errordB:
  'gestire...
  m_fun.WriteLog "errore su ricerca alias: idTable=" & IdTable
  getAlias_tabelle = ""
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' INTEGRITA' REFERENZIALE "APPLICATIVA"
' ELIMINA ALL/AUTOMATIC ITEMS
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Mauro 01/08/2007 : Unificate le routine DB2-ORACLE
Public Sub deleteTable(IdTable As Long, Optional eraseAll As Boolean)
  Dim queryFilter As String
  
  On Error GoTo errdb
  
  If Not eraseAll Then queryFilter = " and tag like 'AUTOMATIC%'"
    
  DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Tabelle Where idtable = " & IdTable & queryFilter
  DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Alias Where idtable = " & IdTable & queryFilter
  DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Colonne Where idtable = " & IdTable & queryFilter
  DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_Index Where idtable = " & IdTable & queryFilter
  DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_IdxCol Where idtable = " & IdTable & queryFilter
  
  Exit Sub
errdb:
  MsgBox "tmp: " & ERR.Description
  Resume Next
End Sub

' SILVIA 11-03-2008 : DELETE DEI TABLESPACES
Public Sub deleteTableSpace(IdTableSpc As Long)
  Dim queryFilter As String
  
  On Error GoTo errdb
      
  DataMan.DmConnection.Execute "DELETE * From " & PrefDb & "_TableSpaces Where IdTableSpc = " & IdTableSpc
  
  Exit Sub
errdb:
  MsgBox "tmp: " & ERR.Description
  Resume Next
End Sub

Public Sub Carica_Lista_Oggetti_Esistenti(Lsw As ListView, Tipo As String, Optional SQL As String = "")
  Dim r As Recordset, rsOr As Recordset
  Dim k As Integer
  
  Lsw.ListItems.Clear
  Set Lsw = checkOriginColumn(Lsw)
  Select Case UCase(Trim(Tipo))
    Case "DB2"
      Lsw.ColumnHeaders(1).Width = "2000"
      Lsw.ColumnHeaders.Add 3, , "Origin"
      Lsw.ColumnHeaders(5).text = "Last Modify Date"
      Lsw.ColumnHeaders(6).text = "Description"
      Lsw.ColumnHeaders(6).Width = withTot(Lsw)
      'Set r = m_fun.Open_Recordset("Select Distinct NomeDB From DMDB2_TableSpaces")
      'alpitour 5/8/2005
      'Set r = m_fun.Open_Recordset("Select Distinct IdDb,Nome,Descrizione,Data_Creazione,Data_Modifica From DMDB2_DB")
      Set r = m_fun.Open_Recordset("Select Distinct IdDb,Nome,Descrizione,Data_Creazione,Data_Modifica,Tag,IdDBOr From " & PrefDb & "_DB order by nome")
      
      While Not r.EOF
            With Lsw.ListItems.Add(, , Trim(UCase(r!Nome)))
            .Tag = r!idDB
            If Trim(r!Tag) = "Da DDL" Or Trim(r!Tag) = "Default" Then
              .SubItems(1) = Tipo 'Type
              .SubItems(2) = "" 'Origin
            ElseIf InStr(r!Tag, "-VSAMDB2") Then
              .SubItems(1) = "VSAM" 'Type
              Set rsOr = m_fun.Open_Recordset("select nome from bs_oggetti where idoggetto = " & r!iddbor)
              If rsOr.RecordCount Then
                .SubItems(2) = rsOr!Nome 'Origin
              Else
                .SubItems(2) = "" 'Origin
              End If
              rsOr.Close
            Else
              .SubItems(1) = "DLI" 'Type
              Set rsOr = m_fun.Open_Recordset("select nome from bs_oggetti where idoggetto = " & r!iddbor)
              If rsOr.RecordCount Then
                .SubItems(2) = rsOr!Nome 'Origin
              Else
                .SubItems(2) = "" 'Origin
              End If
              rsOr.Close
            End If
            .SubItems(3) = r!Data_Creazione & " "
            .SubItems(4) = r!Data_Modifica & " "
            .SubItems(5) = r!Descrizione & " "
          End With
          r.MoveNext
      Wend
      r.Close
    Case "DLI"
      Lsw.ColumnHeaders(1).Width = "2000"
      Lsw.ColumnHeaders(4).text = "Parsing Date"
      Lsw.ColumnHeaders(5).text = "Parsing Level"
      Lsw.ColumnHeaders(5).Width = withTot(Lsw)
      'SQ
      'Set r = m_fun.Open_Recordset("Select Distinct Nome,ParsingLevel,Tipo_dbd, idoggetto,DtImport,DtParsing From BS_Oggetti where Tipo_DBD in ('HID', 'GSA', 'HDA', 'LOG', 'HIS', 'SHI') ")
      Set r = m_fun.Open_Recordset("Select Distinct Nome,ParsingLevel,Tipo_dbd, idoggetto,DtImport,DtParsing From BS_Oggetti where Tipo_DBD not in ('IND')")
      While Not r.EOF
          With Lsw.ListItems.Add(, , Trim(UCase(r!Nome)))
            .SubItems(1) = r!Tipo_DBD
            .Tag = r!IdOggetto
            .SubItems(2) = r!DtImport & " "
            .SubItems(3) = r!DtParsing & " "
            .SubItems(4) = r!ParsingLevel & " "
          End With
          r.MoveNext
      Wend
      r.Close
      
    Case "ORACLE"
       'Set r = m_fun.Open_Recordset("Select Distinct NomeDB From DMORC_TableSpaces")
      Lsw.ColumnHeaders(1).Width = "2000"
      Lsw.ColumnHeaders.Add 3, , "Origin"
      Lsw.ColumnHeaders(3).Width = "3000"
      Lsw.ColumnHeaders(5).text = "Last Modify Date"
      Lsw.ColumnHeaders(6).text = "Description"
      Lsw.ColumnHeaders(6).Width = withTot(Lsw)
      Set r = m_fun.Open_Recordset("Select Distinct IdDb,Nome,Descrizione,Data_Creazione,Data_Modifica,Tag,IdDBOr From " & PrefDb & "_DB")
      While Not r.EOF
          With Lsw.ListItems.Add(, , Trim(UCase(r!Nome)))
            .Tag = r!idDB
            'SQ 25-06-07 e questo?! (testare!)
            If Trim(r!Tag) = "Da DDL" Or Trim(r!Tag) = "Default" Then
              .SubItems(1) = Tipo 'Type
              .SubItems(2) = "" 'Origin
            ElseIf InStr(r!Tag, "_DB2ORC") Then
              .SubItems(1) = "DB2" 'Type
              Set rsOr = m_fun.Open_Recordset("select nome from DMDB2_DB where IdDB = " & r!iddbor)
              If rsOr.RecordCount Then
                .SubItems(2) = Trim(UCase(rsOr!Nome)) 'Origin
              Else
                .SubItems(2) = "" 'Origin
              End If
              rsOr.Close
            ElseIf InStr(r!Tag, "-VSAMORA") Then
              .SubItems(1) = "VSAM" 'Type
              Set rsOr = m_fun.Open_Recordset("select nome from BS_Oggetti where idoggetto = " & r!iddbor)
              If rsOr.RecordCount Then
                .SubItems(2) = rsOr!Nome 'Origin
              Else
                .SubItems(2) = "" 'Origin
              End If
              rsOr.Close
            Else
              .SubItems(1) = "DLI" 'Type
              Set rsOr = m_fun.Open_Recordset("select nome from BS_Oggetti where idoggetto = " & r!iddbor)
              If rsOr.RecordCount Then
                .SubItems(2) = rsOr!Nome 'Origin
              Else
                .SubItems(2) = "" 'Origin
              End If
              rsOr.Close
            End If
            .SubItems(3) = r!Data_Creazione & " "
            .SubItems(4) = r!Data_Modifica & " "
            .SubItems(5) = r!Descrizione & " "
          End With
          r.MoveNext
      Wend
      r.Close
    Case "VSAM"
      Lsw.ColumnHeaders(1).Width = "4000"
      Lsw.ColumnHeaders(4).text = "Last Modify Date"
      Lsw.ColumnHeaders(5).text = "TAG"
      If SQL <> "" Then
        Set r = m_fun.Open_Recordset(SQL)
      Else
        Set r = m_fun.Open_Recordset("Select * from DMVSM_ARCHIVIO order by nome")
      End If
      While Not r.EOF
          With Lsw.ListItems.Add(, , Trim(UCase(r!Nome)))
            .Tag = r!idArchivio
            ' Mauro 06/08/2007 : Basta "UNKNOWN"!!!!
            '.SubItems(1) = "UNKNOWN"
            .SubItems(1) = ""
            
            ' Mauro 06/08/2007 : cambio i flag con un campo Type
            'If r!esds = True Then .SubItems(1) = "ESDS"
            'If r!ksds = True Then .SubItems(1) = "KSDS"
            'If r!rrds = True Then .SubItems(1) = "RRDS"
            'If r!Seq = True Then .SubItems(1) = "Sequential"
            If r!type = "Seq" Then
              .SubItems(1) = "Sequential"
            ElseIf Len(r!type) Then
              .SubItems(1) = r!type & ""
            End If
            If r!gdg = True Then .SubItems(1) = "GDG"
            
            .SubItems(2) = r!Data_Creazione & " "
            .SubItems(3) = r!Data_Modifica & " "
            .SubItems(4) = r!Tag & " "
          End With
          r.MoveNext
      Wend
      r.Close
  End Select
  
  For k = 1 To Lsw.ListItems.count
    Lsw.ListItems(k).Selected = False
  Next k
End Sub

Public Sub Seleziona_Filtro_Selezione(Lista As ListView, Filter As MSFlexGrid, Stato As Long, Index As Integer)
  Select Case Stato
    Case tbrPressed
      Lista.Visible = False
      Filter.Visible = True
      Filter.Height = 915
      MadmdF_Start.Toolbar1.Buttons(Index + 1).Enabled = True
    Case tbrUnpressed
      Lista.Visible = True
      Filter.Visible = False
      MadmdF_Start.Toolbar1.Buttons(Index + 1).Enabled = False
      MadmdF_Start.txtFilter.Visible = False
      'ricarica la lista oggetti precedente
      'MabseF_List.TotalObj.Text = Carica_Lista_Oggetti(Lista, SQL_Selection_List, MabseF_List.PBar)
  End Select

  'imposta la riga di intestazione
  Filter.Row = 0
  'imposta le colonne
  Filter.Col = 0
  Filter.text = "Name"
  Filter.ColWidth(0) = MadmdF_Start.LswExistObj.ColumnHeaders(1).Width
  
  Filter.Col = 1
  Filter.text = "Type"
  Filter.ColWidth(1) = MadmdF_Start.LswExistObj.ColumnHeaders(2).Width
  
  Filter.Col = 2
  Filter.text = "Creation Date"
  Filter.ColWidth(2) = MadmdF_Start.LswExistObj.ColumnHeaders(3).Width
  
  Filter.Col = 3
  Filter.text = "Last Modify Date"
  Filter.ColWidth(3) = MadmdF_Start.LswExistObj.ColumnHeaders(4).Width
  
  Filter.Col = 4
  Filter.text = "Tag"
  Filter.ColWidth(4) = MadmdF_Start.LswExistObj.ColumnHeaders(5).Width
End Sub

Public Sub Execute_Filter(list As MSFlexGrid, LObj As ListView, Stato As Long)
  Dim Prop() As String, SQL As String, AppStr As String
  Dim cont As Long, i As Long, FinalS As String
  
  Screen.MousePointer = vbHourglass
  If Stato = 0 Then
    MadmdF_Start.Toolbar1.Buttons(11).Value = tbrUnpressed
    Carica_Lista_Oggetti_Esistenti LObj, TipoDB
  Else
    SQL = ""
    cont = 0
    AppStr = ""
    FinalS = ""
    
    ReDim Prop(0)
    'memorizza le variabili di query
    list.Row = 1
    For i = 0 To list.Cols - 1
      list.Col = i
      ReDim Preserve Prop(UBound(Prop) + 1)
      Prop(UBound(Prop)) = list.text
    Next i
    
    'crea la query
    For i = 1 To UBound(Prop)
      If Prop(i) <> "" Then
        Prop(i) = Replace(Prop(i), "*", "%")
        Select Case i
          Case 1
            If InStr(1, Prop(i), "%") <> 0 Then
              SQL = "Nome Like '" & Prop(i) & "'"
            Else
              SQL = "Nome = '" & Prop(i) & "'"
            End If
            cont = cont + 1
            
          Case 2
            If InStr(1, Prop(i), "%") <> 0 Then
              SQL = "Type Like '" & Prop(i) & "'"
            Else
              SQL = "Type = '" & Prop(i) & "'"
            End If
            cont = cont + 1
                      
          Case 3
            If InStr(1, Prop(i), "%") <> 0 Then
              SQL = "Data_Creazione Like '" & Prop(i) & "'"
            Else
              SQL = "Data_Creazione = '" & Prop(i) & "'"
            End If
            cont = cont + 1
          
          Case 4
            If InStr(1, Prop(i), "%") <> 0 Then
              SQL = "Data_Modifica Like '" & Prop(i) & "'"
            Else
              SQL = "Data_Modifica = '" & Prop(i) & "'"
            End If
            cont = cont + 1
          
          Case 5
            If InStr(1, Prop(i), "%") <> 0 Then
              SQL = "TAG Like '" & Prop(i) & "'"
            Else
              SQL = "TAG = '" & Prop(i) & "'"
            End If
            cont = cont + 1
        End Select
        
        If SQL <> "" Then
          If cont > 1 Then
            AppStr = " And " & SQL
          Else
            AppStr = SQL
          End If
        End If
        
        FinalS = FinalS & AppStr
        AppStr = ""
        SQL = ""
      End If
    Next i
    If FinalS <> "" Then
      FinalS = "Select * from DMVSM_ARCHIVIO Where " & FinalS & " Order By Nome"
    End If
    
    'visualizza la lista oggetti
    list.Visible = True
    
    'disabilita il bottone di filtro
    MadmdF_Start.Toolbar1.Buttons(10).Value = tbrUnpressed
    MadmdF_Start.Toolbar1.Buttons(11).Value = tbrPressed
    MadmdF_Start.txtFilter.Visible = False
    
    LObj.Visible = True
    
    'carica la lista oggetti
    Carica_Lista_Oggetti_Esistenti LObj, TipoDB, FinalS
    FinalS = ""
  End If
  Screen.MousePointer = vbDefault
End Sub
